//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_MP_ARC_CAB_MANAGER.sc																				//
// Description: This is the implementation of arcade cabinet manager. This script will create & manage arcade cabinet   //
//				props and start any child script for arcade games if required.							 				//
// Written by:  Online Technical Design: Ata Tabrizi																	//
// Date:  		28/08/2019																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_wait_zero.sch"
USING "net_common_functions.sch"
USING "net_simple_interior.sch"
USING "net_render_targets.sch"

#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF

#IF FEATURE_CASINO_HEIST
USING "net_arcade_cabinet.sch"
USING "net_arcade_cabinet_management_menu.sch"
USING "net_hacking_fingerprint_minigame.sch"
USING "net_hacking_order_unlock.sch"
USING "net_voltage_hack.sch"

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════════════╡ ENUM ╞══════════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛


//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ DATA STRUCTURES ╞═════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

STRUCT PlayerBroadcastData
	INT iLocalBS
ENDSTRUCT

#IF IS_DEBUG_BUILD
STRUCT ARCADE_CABINET_DEBUG_DATA
	BOOL bDisplaySlotNumber
	BOOL bTerminateAllChildScripts
	BOOL bClearSaveSlots
	BOOL bHasHackingDevice = FALSE
ENDSTRUCT
#ENDIF

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡    CONSTS    ╞════════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

// sACMData.iBS 
CONST_INT ARCADE_CABINET_MANAGER_STARTED_CHILD_SCRIPT							0
CONST_INT ARCADE_CABINET_MANAGER_START_ENTER_ANIM								1
CONST_INT ARCADE_CABINET_MANAGER_ASSIGN_SUB_PROP_OBJ_INDEX						2
CONST_INT ARCADE_CABINET_MANAGER_EXIT_ARCADE_CABINET							3
CONST_INT ARCADE_CABINET_MANAGER_EXIT_ARCADE_CABINET_ANIM_STARTED				4
CONST_INT ARCADE_CABINET_MANAGER_TERMINATE_CHILD_SCRIPT							5
CONST_INT ARCADE_CABINET_MANAGER_HIDE_CABINETS_FOR_NET_CUT_SCENE				6
CONST_INT ARCADE_CABINET_MANAGER_FORCE_TO_ROOM									7
CONST_INT ARCADE_CABINET_MANAGER_HIDE_CABINETS_NEXT_TO_PILLAR					8
CONST_INT ARCADE_CABINET_MANAGER_HIDE_CABINETS_FOR_FINAL_MOCAP					9

// playerBD.iLocalBS
CONST_INT  ARCADE_CABINET_MANAGER_PLAYER_BD_SET_IN_LOCATE						0
CONST_INT  ARCADE_CABINET_MANAGER_PLAYER_BD_REQUEST_TO_USE						1
CONST_INT  ARCADE_CABINET_MANAGER_PLAYER_BD_MOVE_OUT_OF_WAY						2
CONST_INT  ARCADE_CABINET_MANAGER_PLAYER_BD_VAULT_DRILL_ASSETS_LOADED			3
CONST_INT  ARCADE_CABINET_MANAGER_PLAYER_BD_RENOVATE_ARCADE						4

CONST_FLOAT ARCADE_CABINET_LOC_DIMENSIONS_X										0.45 
CONST_FLOAT ARCADE_CABINET_LOC_DIMENSIONS_Y										0.45
CONST_FLOAT ARCADE_CABINET_LOC_DIMENSIONS_Z										2.0

CONST_INT ARCADE_MINI_GAME_PASSED 												0
CONST_INT ARCADE_MINI_GAME_FAILED 												1
CONST_INT ARCADE_MINI_GAME_QUIT 												2

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡    VARIABLES    ╞═════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]
ARCADE_CABINET_MANAGER_SERVER_BD serverBD
ARCADE_CABINET_DETAILS sACMData

// expensive struct which bumpped the stack 
FINGERPRINT_MINIGAME_GAMEPLAY_DATA fingerPrintCloneData
ORDER_UNLOCK_GAMEPLAY_DATA orderUnlockData

#IF IS_DEBUG_BUILD
ARCADE_CABINET_DEBUG_DATA sACMDebugData
#ENDIF

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡     DEBUG     ╞══════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛
#IF IS_DEBUG_BUILD
FUNC STRING GET_ARCADE_CABINET_STATE_NAME(ARCADE_CABINET_MANAGER_STATE eState)
	
	SWITCH eState
		CASE ARCADE_CABINET_INIT_PROPS_STATE			RETURN "ARCADE_CABINET_INIT_PROPS_STATE"
		CASE ARCADE_CABINET_INIT_CHILD_SCRIPT_STATE		RETURN "ARCADE_CABINET_INIT_CHILD_SCRIPT_STATE"
		CASE ARCADE_CABINET_IDLE_STATE					RETURN "ARCADE_CABINET_IDLE_STATE"
		CASE ARCADE_CABINET_DRAW_STARTUP_MENU_STATE		RETURN "ARCADE_CABINET_DRAW_STARTUP_MENU_STATE"
		CASE ARCADE_CABINET_GO_TO_INITAL_COORDS_STATE	RETURN "ARCADE_CABINET_GO_TO_INITAL_COORDS_STATE"
		CASE ARCADE_CABINET_TAKE_MONEY_STATE			RETURN "ARCADE_CABINET_TAKE_MONEY_STATE"
		CASE ARCADE_CABINET_ENTRY_ANIM_STATE			RETURN "ARCADE_CABINET_ENTRY_ANIM_STATE"
		CASE ARCADE_CABINET_LAUNCH_GAME_SCRIPT_STATE	RETURN "ARCADE_CABINET_LAUNCH_GAME_SCRIPT_STATE"
		CASE ARCADE_CABINET_IN_USE_ANIM_STATE			RETURN "ARCADE_CABINET_IN_USE_ANIM_STATE"
		CASE ARCADE_CABINET_CLEANUP_EXIT_STATE			RETURN "ARCADE_CABINET_CLEANUP_EXIT_STATE"
		CASE ARCADE_CABINET_CLEANUP_GAME_ASSETS_STATE	RETURN "ARCADE_CABINET_CLEANUP_GAME_ASSETS_STATE"
		CASE ARCADE_CABINET_CLEANUP_STATE				RETURN "ARCADE_CABINET_CLEANUP_STATE"
	ENDSWITCH
	
	RETURN "INVALID STATE"
	
ENDFUNC

FUNC STRING GET_ARCADE_CABINET_SERVER_STATE_NAME(ARCADE_CABINET_SERVER_STATE eState)
	SWITCH eState
		CASE ARCADE_CABINET_SERVER_INIT_STATE		RETURN "ARCADE_CABINET_SERVER_INIT_STATE"
		CASE ARCADE_CABINET_SERVER_IDLE_STATE		RETURN "ARCADE_CABINET_SERVER_IDLE_STATE"
		CASE ARCADE_CABINET_SERVER_CLEARNUP_STATE	RETURN "ARCADE_CABINET_SERVER_CLEARNUP_STATE"
	ENDSWITCH
	RETURN ""
ENDFUNC 

FUNC STRING GET_ARRAY_BIT_SET_NAME(INT iBit)
	SWITCH iBit
		CASE ARCADE_CABINET_MANAGER_STARTED_CHILD_SCRIPT			RETURN "ARCADE_CABINET_MANAGER_STARTED_CHILD_SCRIPT"
		CASE ARCADE_CABINET_MANAGER_START_ENTER_ANIM				RETURN "ARCADE_CABINET_MANAGER_START_ENTER_ANIM"
		CASE ARCADE_CABINET_MANAGER_ASSIGN_SUB_PROP_OBJ_INDEX		RETURN "ARCADE_CABINET_MANAGER_ASSIGN_SUB_PROP_OBJ_INDEX"
		CASE ARCADE_CABINET_MANAGER_EXIT_ARCADE_CABINET				RETURN "ARCADE_CABINET_MANAGER_EXIT_ARCADE_CABINET"
		CASE ARCADE_CABINET_MANAGER_TERMINATE_CHILD_SCRIPT			RETURN "ARCADE_CABINET_MANAGER_TERMINATE_CHILD_SCRIPT"
		CASE ARCADE_CABINET_MANAGER_HIDE_CABINETS_FOR_NET_CUT_SCENE	RETURN "ARCADE_CABINET_MANAGER_HIDE_CABINETS_FOR_NET_CUT_SCENE"
		CASE ARCADE_CABINET_MANAGER_FORCE_TO_ROOM					RETURN "ARCADE_CABINET_MANAGER_FORCE_TO_ROOM"
		CASE ARCADE_CABINET_MANAGER_HIDE_CABINETS_NEXT_TO_PILLAR	RETURN "ARCADE_CABINET_MANAGER_HIDE_CABINETS_NEXT_TO_PILLAR"
		CASE ARCADE_CABINET_MANAGER_HIDE_CABINETS_FOR_FINAL_MOCAP	RETURN "ARCADE_CABINET_MANAGER_HIDE_CABINETS_FOR_FINAL_MOCAP"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_BIT_SET_NAME(INT iBit)
	SWITCH iBit
		CASE ARCADE_CABINET_MANAGER_LOCAL_BS_USE_ENTER_B				RETURN "ARCADE_CABINET_MANAGER_LOCAL_BS_USE_ENTER_B"
		CASE ARCADE_CABINET_MANAGER_LOCAL_BS_FORCE_FPP_CAM				RETURN "ARCADE_CABINET_MANAGER_LOCAL_BS_FORCE_FPP_CAM"
		CASE ARCADE_CABINET_MANAGER_LOCAL_BS_STOP_ALL_CABINET_SOUNDS	RETURN "ARCADE_CABINET_MANAGER_LOCAL_BS_STOP_ALL_CABINET_SOUNDS"
		CASE ARCADE_CABINET_MANAGER_LOCAL_BS_PLAY_KICK_SOUND			RETURN "ARCADE_CABINET_MANAGER_LOCAL_BS_PLAY_KICK_SOUND"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

PROC CREATE_DEBUG_WIDGET()
	START_WIDGET_GROUP("AM_MP_ARC_CAB_MANAGER")
		START_WIDGET_GROUP("Cabinet info")
			ADD_WIDGET_BOOL("Display slot number", sACMDebugData.bDisplaySlotNumber)
			ADD_WIDGET_BOOL("Render finish phase info", g_DebugArcadeCabinetManagerData.bAnimFinishDebugInfo)
			ADD_WIDGET_BOOL("Set all screen props as in use", g_DebugArcadeCabinetManagerData.bSpawnScreenProp)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Terminate all scripts")
			ADD_WIDGET_BOOL("Terminate", sACMDebugData.bTerminateAllChildScripts)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Randomise Cabinets")
			ADD_WIDGET_BOOL("Randomise and Reset", g_DebugArcadeCabinetManagerData.bRandomiseCabinets)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Mini game")
			ADD_WIDGET_BOOL("Force laser drilling", g_DebugArcadeCabinetManagerData.bForceLaser)
			ADD_WIDGET_BOOL("Force drilling", g_DebugArcadeCabinetManagerData.bForceDrill)
			ADD_WIDGET_BOOL("Add hacking devices", sACMDebugData.bHasHackingDevice)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Save slots")
			ADD_WIDGET_BOOL("Clear all save display slots", sACMDebugData.bClearSaveSlots)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Minimize game V1")
			ADD_WIDGET_BOOL("Minimize arcade game", g_bMinimizeArcadeCabinetGame)
			ADD_WIDGET_FLOAT_SLIDER("CentreX", fTWEAK_ARCADE_CABINET_MINIMIZE_CENTREX,  -1.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("CentreY", fTWEAK_ARCADE_CABINET_MINIMIZE_CENTREY,  -1.0, 1.0, 0.005)
			ADD_WIDGET_FLOAT_SLIDER("Scale", fTWEAK_ARCADE_CABINET_MINIMIZE_SCALE,		0.0, 1.0, 0.001)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Minimize game V2")
			ADD_WIDGET_BOOL("Minimize arcade game", g_bMinimizeArcadeCabinetDrawing)
			ADD_WIDGET_FLOAT_SLIDER("CentreX", g_fMinimizeArcadeCabinetCentreX,  -1.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("CentreY", g_fMinimizeArcadeCabinetCentreY,  -1.0, 1.0, 0.005)
			ADD_WIDGET_FLOAT_SLIDER("Scale", g_fMinimizeArcadeCabinetCentreScale,		0.0, 1.0, 0.001)
		STOP_WIDGET_GROUP()		
	STOP_WIDGET_GROUP()
ENDPROC
#ENDIF

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════╡     GETTER & SETTER     ╞══════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

PROC SET_ARCADE_CABINET_MANAGER_SERVER_STATE(ARCADE_CABINET_SERVER_STATE eState)
	IF serverBD.eServerState != eState
		PRINTLN("[AM_ARC_CAB] - SET_ARCADE_CABINET_MANAGER_SERVER_STATE: ", GET_ARCADE_CABINET_SERVER_STATE_NAME(serverBD.eServerState), " TO: ", GET_ARCADE_CABINET_SERVER_STATE_NAME(eState))
		serverBD.eServerState = eState
	ENDIF
ENDPROC

PROC SET_ARCADE_CABINET_MANAGER_STATE(ARCADE_CABINET_MANAGER_STATE eState)
	IF sACMData.eArcadeCabinetManagerState != eState
		PRINTLN("[AM_ARC_CAB] - SET_ARCADE_CABINET_MANAGER_STATE: ", GET_ARCADE_CABINET_STATE_NAME(sACMData.eArcadeCabinetManagerState), " TO: ", GET_ARCADE_CABINET_STATE_NAME(eState))
		DEBUG_PRINTCALLSTACK()
		sACMData.eArcadeCabinetManagerState = eState
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_IN_ARCADE_CABINET_LOCATE(PLAYER_INDEX playerToCheck)
	IF playerToCheck != INVALID_PLAYER_INDEX()
		RETURN IS_BIT_SET(playerBD[NATIVE_TO_INT(playerToCheck)].iLocalBS, ARCADE_CABINET_MANAGER_PLAYER_BD_SET_IN_LOCATE)
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Set true when player in arcade cabine locate
PROC SET_PLAYER_IN_ARCADE_CABINET_LOCATE(BOOL bInLocate, INT iArcadeDisplaySlot, INT iPlayerSlot)
	IF PLAYER_ID() = INVALID_PLAYER_INDEX()
		PRINTLN("[AM_ARC_CAB] - SET_PLAYER_IN_ARCADE_CABINET_LOCATE invalid player index")
		EXIT
	ENDIF
	
	IF bInLocate
		IF !IS_PLAYER_IN_ARCADE_CABINET_LOCATE(PLAYER_ID())
		OR GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID()) != iArcadeDisplaySlot
		OR GET_ARCADE_CABINET_LOCATE_PLAYER_SLOT_PLAYER_IS_IN(PLAYER_ID()) != iPlayerSlot
			SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, ARCADE_CABINET_MANAGER_PLAYER_BD_SET_IN_LOCATE)
			globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].sArcadeManagerGlobalPlayerBD.iArcadeDisplaySlot 		= iArcadeDisplaySlot
			globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].sArcadeManagerGlobalPlayerBD.iArcadePlayerSlot		= iPlayerSlot
			PRINTLN("[AM_ARC_CAB] - SET_PLAYER_IN_ARCADE_CABINET_LOCATE Display slot: ", iArcadeDisplaySlot, " iPlayerSlot: ", iPlayerSlot, " TRUE")
		ENDIF
	ELSE
		IF IS_PLAYER_IN_ARCADE_CABINET_LOCATE(PLAYER_ID())
			CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, ARCADE_CABINET_MANAGER_PLAYER_BD_SET_IN_LOCATE)
			globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].sArcadeManagerGlobalPlayerBD.iArcadeDisplaySlot 		= -1
			globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].sArcadeManagerGlobalPlayerBD.iArcadePlayerSlot		= -1
			PRINTLN("[AM_ARC_CAB] - SET_PLAYER_IN_ARCADE_CABINET_LOCATE Display slot: ", iArcadeDisplaySlot,  " FALSE")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_REQUESTING_TO_USE_CABINET(PLAYER_INDEX playerToCheck)
	IF playerToCheck != INVALID_PLAYER_INDEX()
		RETURN IS_BIT_SET(playerBD[NATIVE_TO_INT(playerToCheck)].iLocalBS, ARCADE_CABINET_MANAGER_PLAYER_BD_REQUEST_TO_USE)
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_PLAYER_REQUESTING_TO_USE_CABINET(BOOL bRequest)
	IF bRequest
		IF !IS_PLAYER_REQUESTING_TO_USE_CABINET(PLAYER_ID())
			SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, ARCADE_CABINET_MANAGER_PLAYER_BD_REQUEST_TO_USE)
			PRINTLN("[AM_ARC_CAB] - SET_PLAYER_REQUESTING_TO_USE_CABINET TRUE")
		ENDIF
	ELSE
		IF IS_PLAYER_REQUESTING_TO_USE_CABINET(PLAYER_ID())
			CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, ARCADE_CABINET_MANAGER_PLAYER_BD_REQUEST_TO_USE)
			PRINTLN("[AM_ARC_CAB] - SET_PLAYER_REQUESTING_TO_USE_CABINET FALSE")
		ENDIF
	ENDIF
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ QUERY BITS ╞══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    set array bit set
PROC SET_ARCADE_CABINET_MANAGER_BIT(INT iArcadeCabinetSlot, INT iBit)
	IF NOT IS_BIT_SET(sACMData.iBS[iArcadeCabinetSlot], iBit)
		SET_BIT(sACMData.iBS[iArcadeCabinetSlot], iBit)
		PRINTLN("[AM_ARC_CAB] SET_ARCADE_CABINET_MANAGER_BIT iArcadeCabinetSlot: ", iArcadeCabinetSlot, " bit ", GET_ARRAY_BIT_SET_NAME(iBit) , " TRUE")
	ENDIF
ENDPROC

/// PURPOSE:
///    Get array bit set
FUNC BOOL IS_ARCADE_CABINET_MANAGER_BIT_SET(INT iArcadeCabinetSlot, INT iBit)
	RETURN IS_BIT_SET(sACMData.iBS[iArcadeCabinetSlot], iBit)
ENDFUNC

/// PURPOSE:
///    Clear array bit set
PROC CLEAR_ARCADE_CABINET_MANAGER_BIT(INT iArcadeCabinetSlot, INT iBit)
	IF IS_BIT_SET(sACMData.iBS[iArcadeCabinetSlot], iBit)
		CLEAR_BIT(sACMData.iBS[iArcadeCabinetSlot], iBit)
		PRINTLN("[AM_ARC_CAB] CLEAR_ARCADE_CABINET_MANAGER_BIT iArcadeCabinetSlot ", iArcadeCabinetSlot, " bit ", GET_ARRAY_BIT_SET_NAME(iBit) , " FALSE")
	ENDIF
ENDPROC

/// PURPOSE:
///    Set local bit set
PROC SET_ARCADE_CABINET_MANAGER_LOCAL_BIT(INT iBit)
	IF NOT IS_BIT_SET(sACMData.iLocalBS, iBit)
		SET_BIT(sACMData.iLocalBS, iBit)
		PRINTLN("[AM_ARC_CAB] SET_ARCADE_CABINET_MANAGER_LOCAL_BIT bit ", GET_BIT_SET_NAME(iBit) , " TRUE")
	ENDIF
ENDPROC

/// PURPOSE:
///    Get local bit set
FUNC BOOL IS_LOCAL_ARCADE_CABINET_MANAGER_BIT_SET(INT iBit)
	RETURN IS_BIT_SET(sACMData.iLocalBS, iBit)
ENDFUNC

/// PURPOSE:
///    Clear local bit set
PROC CLEAR_ARCADE_CABINET_MANAGER_LOCAL_BIT(INT iBit)
	IF IS_BIT_SET(sACMData.iLocalBS, iBit)
		CLEAR_BIT(sACMData.iLocalBS, iBit)
		PRINTLN("[AM_ARC_CAB] CLEAR_ARCADE_CABINET_MANAGER_LOCAL_BIT bit ", GET_BIT_SET_NAME(iBit) , " FALSE")
	ENDIF
ENDPROC

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡     CLEANUP     ╞══════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

PROC CLEAR_TRIGGER_HELP()
	IF !IS_STRING_NULL_OR_EMPTY(sACMData.strTriggerHelp)
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sACMData.strTriggerHelp)
			CLEAR_HELP()
		ENDIF
		sACMData.strTriggerHelp = ""
	ENDIF
ENDPROC

/// PURPOSE:
///    Clear global flags
PROC CLEAR_GLOBAL_VARIABLES()
	SET_PLAYER_USING_ARCADE_CABINET(FALSE)
	SET_PLAYER_READY_TO_PLAY_ARCADE_GAME(FALSE)
	CLEAR_BIT(g_ArcadeCabinetManagerData.iBS, ARC_CAB_MANAGER_GLOBAL_BIT_SET_UPDATE_PROPS)
ENDPROC

PROC REMOVE_ANIM_DICTIONARY()
	IF NOT IS_STRING_NULL_OR_EMPTY(g_ArcadeCabinetManagerData.strArcCabinetAnimDic)
		REMOVE_ANIM_DICT(g_ArcadeCabinetManagerData.strArcCabinetAnimDic)
		g_ArcadeCabinetManagerData.strArcCabinetAnimDic = ""
		PRINTLN("[AM_ARC_CAB] - REMOVE_ANIM_DICTIONARY g_ArcadeCabinetManagerData.strArcCabinetAnimDic = NULL ")
	ENDIF
ENDPROC

/// PURPOSE:
///    Get number of arcade cabinets
FUNC INT GET_NUM_ARCADE_CABINETS()
	RETURN sACMData.iNumOfArcadeCabinets
ENDFUNC 

/// PURPOSE:
///    Remove all arcade cabinet props
PROC REMOVE_ALL_CABINET_PROPS()
	INT iPlayerSlot
	INT iDisplaySlot
	FOR iPlayerSlot = 0 TO GET_NUM_ARCADE_CABINETS() - 1
		REMOVE_ARCADE_CABINETS(sACMData, iPlayerSlot)
		FOR iDisplaySlot = 0 TO ENUM_TO_INT(ARCADE_CAB_MAX_NUM_PLAYERS) - 1
			REMOVE_ARCADE_CABINETS_SCREEN_PROP(sACMData, iPlayerSlot, iDisplaySlot)
		ENDFOR
	ENDFOR 
	#IF IS_DEBUG_BUILD
	sACMDebugData.bTerminateAllChildScripts = FALSE
	#ENDIF
ENDPROC

/// PURPOSE:
///    Checks if game has audio scene and stops one
PROC STOP_GAME_AUDIO_SCENE()
	IF NOT IS_STRING_NULL_OR_EMPTY(sACMData.tlAudioSceneName)
		STOP_AUDIO_SCENE(sACMData.tlAudioSceneName)
		PRINTLN("[AM_ARC_CAB] - STOP_GAME_AUDIO_SCENE ", sACMData.tlAudioSceneName)
	ELSE
		PRINTLN("[AM_ARC_CAB] - STOP_GAME_AUDIO_SCENE is null")
	ENDIF
ENDPROC

/// PURPOSE:
///    request save if display slots are updated
PROC SAVE_ARCADE_CABINET_DISPLAY_SLOT(BOOL bForce = FALSE)
	IF IS_BIT_SET(g_ArcadeCabinetManagerData.iBS, ARC_CAB_MANAGER_GLOBAL_BIT_SET_REQUEST_SAVE)
		IF !IS_LOCAL_PLAYER_USING_ARCADE_MANAGEMENT_MENU()
			IF NOT HAS_NET_TIMER_STARTED(sACMData.tSaveTimer)
			OR bForce
				REQUEST_SAVE(SSR_REASON_ARCADE_CABINET_MANAGEMENT, STAT_SAVETYPE_SCRIPT_MP_GLOBALS)
				START_NET_TIMER(sACMData.tSaveTimer)
				CLEAR_BIT(g_ArcadeCabinetManagerData.iBS, ARC_CAB_MANAGER_GLOBAL_BIT_SET_REQUEST_SAVE)
				PRINTLN("[AM_ARC_CAB] - SAVE_ARCADE_CABINET_DISPLAY_SLOT REQUEST_SAVE set ARC_CAB_MANAGER_GLOBAL_BIT_SET_REQUEST_SAVE to false bForce: ", bForce)
			ELSE
				// Reset every two min
				IF HAS_NET_TIMER_EXPIRED(sACMData.tSaveTimer, 120000)
					RESET_NET_TIMER(sACMData.tSaveTimer)
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

PROC CLEANUP_STARTUP_MENU()
	IF IS_ARCADE_CABINET_MENU_BS(sACMData, ARCADE_CABINET_MANAGER_MENU_BS_EXIT_MENU)
		CLEANUP_MENU_ASSETS()
		ARCADE_CABINET_MENU_STRUCT sMenu
		sACMData.sArcadeCabinetMenu = sMenu
	ENDIF	
ENDPROC

FUNC BOOL SHOULD_CLEANUP_ARCADE_CABINET_USING_FLAG()
	ARCADE_CABINETS cabinetPlayerUsing = GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID())
	IF cabinetPlayerUsing != ARCADE_CABINET_INVALID
		IF cabinetPlayerUsing = ARCADE_CABINET_CH_LOVE_METER
			RETURN FALSE
		ENDIF 
	ENDIF
	RETURN TRUE
ENDFUNC

PROC CLEANUP_ARCADE_CABINET_BIT_SETS()
	INT i
	FOR i = 0 TO GET_ARCADE_SLOT_COUNT_FOR_PROPERTY(sACMData.eArcadeCabinetPropertyType) - 1
		CLEAR_ARCADE_CABINET_MANAGER_BIT(i, ARCADE_CABINET_MANAGER_START_ENTER_ANIM)
		CLEAR_ARCADE_CABINET_MANAGER_BIT(i, ARCADE_CABINET_MANAGER_EXIT_ARCADE_CABINET_ANIM_STARTED)
		CLEAR_ARCADE_CABINET_MANAGER_BIT(i, ARCADE_CABINET_MANAGER_EXIT_ARCADE_CABINET)
	ENDFOR
	
	CLEAR_ARCADE_CABINET_MANAGER_LOCAL_BIT(ARCADE_CABINET_MANAGER_LOCAL_BS_USE_ENTER_B)
ENDPROC

PROC CLEANUP_GAME_VARIABLES()
	g_sCasinoArcadeInvadeAndPersuadeVars.ePlayerState = INT_TO_ENUM(CASINO_ARCADE_INVADE_PERSUADE_PLAYER_STATE, -1)
	g_sCasinoArcadeStreetCrimeVars.ePlayerState = INT_TO_ENUM(CASINO_ARCADE_STREET_CRIME_PLAYER_STATE, -1)
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].sArcadeManagerGlobalPlayerBD.eStreetCrimePlayerState = INT_TO_ENUM(CASINO_ARCADE_STREET_CRIME_PLAYER_STATE, -1)
	ENDIF	
ENDPROC

/// PURPOSE:
///    Clean up mini game props
FUNC BOOL CLEANUP_MINIGAME_ANIM_PROPS()
	ACM_SUB_PROP_DETAILS eArcadeCabSubPropDetail
	VECTOR vCoord, vRotation
	FLOAT fHeading
	
	INT iDisplaySlot = GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID())
	GET_ARCADE_CABINET_SUB_PROP_DETAIL(sACMData.eArcadeCab[iDisplaySlot], eArcadeCabSubPropDetail)
	
	IF iDisplaySlot != -1
		INT iSubPropIndex
			
		IF eArcadeCabSubPropDetail.iNumProp > 0
		
			STOP_CURRENT_ARCADE_CABINET_PROP_SYNCHED_SCENE()
			STOP_CURRENT_ARCADE_CABINET_SYNCHED_SCENE()
			
			IF DOES_ARCADE_CABINET_PROP_SYNCHED_SCENE_RUNNING()
			OR DOES_ARCADE_CABINET_SYNCHED_SCENE_RUNNING()
				RETURN FALSE
			ENDIF	
			
			FOR iSubPropIndex = 0 TO eArcadeCabSubPropDetail.iNumProp - 1
				IF DOES_ENTITY_EXIST(sACMData.arcadeCabinetSubObj[iDisplaySlot][iSubPropIndex])
					IF !TAKE_CONTROL_OF_ENTITY(sACMData.arcadeCabinetSubObj[iDisplaySlot][iSubPropIndex])
						RETURN FALSE
					ENDIF
					
					IF IS_ENTITY_ATTACHED(sACMData.arcadeCabinetSubObj[iDisplaySlot][iSubPropIndex])
						DETACH_ENTITY(sACMData.arcadeCabinetSubObj[iDisplaySlot][iSubPropIndex])
					ENDIF
					
					FREEZE_ENTITY_POSITION(sACMData.arcadeCabinetSubObj[iDisplaySlot][iSubPropIndex], TRUE)
					SET_ENTITY_VISIBLE(sACMData.arcadeCabinetSubObj[iDisplaySlot][iSubPropIndex], FALSE)
					
					vCoord = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sACMData.arcadeCabinetProp[iDisplaySlot], eArcadeCabSubPropDetail.vOffset[iSubPropIndex])
					fHeading = GET_ENTITY_HEADING(sACMData.arcadeCabinetProp[iDisplaySlot])
					vRotation = eArcadeCabSubPropDetail.vRotation[iSubPropIndex]
					SET_ENTITY_COORDS_NO_OFFSET(sACMData.arcadeCabinetSubObj[iDisplaySlot][iSubPropIndex], vCoord)			
					SET_ENTITY_ROTATION(sACMData.arcadeCabinetSubObj[iDisplaySlot][iSubPropIndex], <<vRotation.x, vRotation.y, fHeading>>)
					PRINTLN("[AM_ARC_CAB] - CLEANUP_MINIGAME_ANIM_PROPS iSubPropIndex: ", iSubPropIndex)	
				ENDIF
			ENDFOR
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC CLEANUP_MINIGAME_STRUCTS()
	ARCADE_CABINETS arcadeCabinet = GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID())
	SWITCH arcadeCabinet
		CASE ARCADE_CABINET_CH_DRILLING_LASER_MINI_G
			S_VAULT_DRILL_DATA newDrillData
			sACMData.sArcadeDrillPractice = newDrillData

			HG_CONTROL_STRUCT newDrillControlData
			sACMData.sArcadeHackingPractice = newDrillControlData
		BREAK
		CASE ARCADE_CABINET_CH_HACKING_MINI_G
			CLEANUP_MINIGAME_ANIM_PROPS()
			
			FINGERPRINT_MINIGAME_GAMEPLAY_DATA newFingerPrintData
			fingerPrintCloneData = newFingerPrintData
			
			ORDER_UNLOCK_GAMEPLAY_DATA newOrderUnlockData
			orderUnlockData = newOrderUnlockData
			
			HG_CONTROL_STRUCT newHackingControlData
			sACMData.sArcadeHackingPractice = newHackingControlData
		BREAK	
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Play idle sound after game ends only call it once 
PROC PLAY_ARCADE_CABINET_IDLE_SOUND()
	IF IS_LOCAL_PLAYER_PED_IN_ARCADE_ROOM(ARC_RM_BASEMENT)
	AND IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
		EXIT
	ENDIF
	
	TEXT_LABEL_63 tlSoundSet, tlSoundName
	INT i
	FOR i = 0 TO GET_ARCADE_SLOT_COUNT_FOR_PROPERTY(sACMData.eArcadeCabinetPropertyType) - 1
		IF sACMData.eArcadeCab[i] != ARCADE_CABINET_INVALID
			IF sACMData.iArcadeTypeIndex[ENUM_TO_INT(GET_ARCADE_CABINET_SOUND_AREA_IN_SLOT(INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, i)))] = i	
				GET_ARCADE_CABINET_SOUND_DETAIL(sACMData.eArcadeCab[i], tlSoundSet, tlSoundName, FALSE)
				IF NOT IS_STRING_NULL_OR_EMPTY(tlSoundName)
					IF sACMData.iIdleSoundID[i] = -1
						
						sACMData.iIdleSoundID[i] = GET_SOUND_ID()
						PLAY_SOUND_FROM_COORD(sACMData.iIdleSoundID[i], tlSoundName, sACMData.vArcadeCabCoords[i], tlSoundSet, FALSE, 20)
						PRINTLN("[AM_ARC_CAB] - PLAY_ARCADE_CABINET_IDLE_SOUND - play sound from ", sACMData.vArcadeCabCoords[i], ":", sACMData.iIdleSoundID[i]
									, ", \"", tlSoundName, "\", \"dlc_vw_am_cabinet_sounds\"")
					ELSE
						IF HAS_SOUND_FINISHED(sACMData.iIdleSoundID[i])
							RELEASE_SOUND_ID(sACMData.iIdleSoundID[i])
							sACMData.iIdleSoundID[i] = -1
							CLEAR_BIT(sACMData.iSoundIndexBS, ENUM_TO_INT(GET_ARCADE_CABINET_SOUND_AREA_IN_SLOT(INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, i))))
							CLEAR_BIT(sACMData.iArcadeTypeSound, ENUM_TO_INT(sACMData.eArcadeCab[i]))
							PRINTLN("[AM_ARC_CAB] - PLAY_ARCADE_CABINET_IDLE_SOUND UPDATE_ARCADE_SOUND_INDEX sACMData.iSoundIndexBS "
									, GET_ARCADE_CABINET_SOUND_AREA_NAME(GET_ARCADE_CABINET_SOUND_AREA_IN_SLOT(INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, i))), " FALSE stagger: ", i)
						ENDIF
					ENDIF
				ENDIF
			ENDIF	
		ENDIF	
	ENDFOR	
ENDPROC

/// PURPOSE:
///    Maintain arcade cabinet clean up state
PROC MAINTAIN_ACM_CLEANUP_STATE()
	CLEANUP_ARCADE_CABINET_BIT_SETS()
	CLEANUP_STARTUP_MENU()
	CLEANUP_GAME_VARIABLES()
	
	STOP_GAME_AUDIO_SCENE()
	RESET_ALL_ARCADE_SOUND_INDEX_BIT_SETS(sACMData)
	REMOVE_ANIM_DICTIONARY()
	
	IF SHOULD_CLEANUP_ARCADE_CABINET_USING_FLAG()
		IF IS_PLAYER_USING_ARCADE_CABINET(PLAYER_ID())
			IF sACMData.startCamViewMode != CAM_VIEW_MODE_FIRST_PERSON
				IF IS_LOCAL_ARCADE_CABINET_MANAGER_BIT_SET(ARCADE_CABINET_MANAGER_LOCAL_BS_FORCE_FPP_CAM)
					SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT, sACMData.startCamViewMode)
				ENDIF
			ENDIF	
			
			CLEANUP_MINIGAME_STRUCTS()
			IF !IS_PLAYER_CONTROL_ON(PLAYER_ID())
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				CLEAR_PED_TASKS(PLAYER_PED_ID())
			ENDIF	
			
			IF IS_GAMEPLAY_HINT_ACTIVE()
				STOP_GAMEPLAY_HINT()
			ENDIF
			
			SET_PLAYER_CONFIG_FLAG_KINEMATIC_MODE_WHEN_STATIONARY(FALSE)
			SET_PLAYER_USING_ARCADE_CABINET(FALSE)
		ENDIF	
	ENDIF
	
	sACMData.iLocalBS = 0
	g_ArcadeCabinetManagerData.iClawPrizeIndex = -1
	RESET_NET_TIMER(sACMData.tExitAnimSafeTimer)
	SET_PLAYER_REQUESTING_TO_USE_CABINET(FALSE)
	SET_ARCADE_CABINET_GLITCHED(FALSE)
	SET_PLAYER_READY_TO_PLAY_ARCADE_GAME(FALSE)
	SET_BAIL_OUT_ARCADE_CABINET(FALSE)
	SET_PLAYER_IN_ARCADE_CABINET_LOCATE(FALSE, -1, -1)
	SET_PLAYER_IN_ARCADE_CABINET_LOCATE_TYPE()
	SET_ARCADE_CABINET_MANAGER_STATE(ARCADE_CABINET_IDLE_STATE)
ENDPROC

/// PURPOSE:
///    Script cleanup
PROC SCRIPT_CLEANUP()
	PRINTLN("[AM_ARC_CAB] - SCRIPT_CLEANUP called")	
	SET_KILL_ARCADE_CABINET_MANAGER_SCRIPT(TRUE)
	CLEAR_GLOBAL_VARIABLES()	
	CLEAR_TRIGGER_HELP()
	STOP_ALL_ARCADE_CABINET_SOUNDS(sACMData)
	RESET_ALL_ARCADE_SOUND_INDEX_BIT_SETS(sACMData)
	
	SAVE_ARCADE_CABINET_DISPLAY_SLOT(TRUE)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			MAINTAIN_ARCADE_CABINET_MANAGEMENT_CLEANUP_STATE(sACMData, serverBD)
		ENDIF
	ENDIF
	
	g_ArcadeCabinetManagerData.eArcadeCabinetManagerState = ARCADE_CABINET_INIT_PROPS_STATE
	
	SET_TERMINATE_PHYSICAL_ARCADE_GAME_FLAG(ARCADE_CABINET_CH_LOVE_METER, FALSE)
	SET_TERMINATE_PHYSICAL_ARCADE_GAME_FLAG(ARCADE_CABINET_CH_FORTUNE_TELLER, FALSE)
	SET_TERMINATE_PHYSICAL_ARCADE_GAME_FLAG(ARCADE_CABINET_CH_CLAW_CRANE, FALSE)
	#IF FEATURE_SUMMER_2020
	SET_TERMINATE_PHYSICAL_ARCADE_GAME_FLAG(ARCADE_CABINET_SUM_STRENGTH_TEST, FALSE)
	#ENDIF
	
	HIDE_ARCADE_CABINETS_NEXT_TO_PILLAR(FALSE)
 	HIDE_ARCADE_CABINETS_FOR_FINAL_MOCAP(FALSE)
	SET_BAIL_OUT_ARCADE_CABINET(FALSE)
	
	STOP_GAME_AUDIO_SCENE()
	MAINTAIN_ACM_CLEANUP_STATE()
	REMOVE_ALL_CABINET_PROPS()
	REMOVE_ANIM_DICTIONARY()
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════╡     INITIALISE     ╞════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///     Get next free display slot 
FUNC INT GET_LOCAL_FREE_DISPLAY_SLOT()
	INT i
	FOR i = 0 TO GET_ARCADE_SLOT_COUNT_FOR_PROPERTY(sACMData.eArcadeCabinetPropertyType) - 1
		IF sACMData.eArcadeCab[i] = ARCADE_CABINET_INVALID
			RETURN i
		ENDIF
	ENDFOR
	RETURN -1
ENDFUNC

/// PURPOSE:
///    set all the local variables to invalid
PROC SET_ALL_VAIABLES_TO_INVALID()
	INT i//, iNumPlayer 
	FOR i = 0 TO GET_ARCADE_SLOT_COUNT_FOR_PROPERTY(sACMData.eArcadeCabinetPropertyType) - 1
		sACMData.eArcadeCab[i] = ARCADE_CABINET_INVALID
		sACMData.iIdleSoundID[i] = -1
		sACMData.iInUseSoundID[i] = -1
//		FOR iNumPlayer = 0 TO ENUM_TO_INT(ARCADE_CAB_MAX_NUM_PLAYERS) - 1
//			sACMData.iMonitorsRenderTargetID[i][iNumPlayer] = -1
//		ENDFOR
	ENDFOR 
ENDPROC

/// PURPOSE:
///    Initials all local vaibales using pars data from script struct
PROC INITIALISE_ACM_LOCAL_VARIABLES(ACM_LAUNCHER_STRUCT missionScriptArgs)
	#IF IS_DEBUG_BUILD
	ARCADE_CABINETS_SAVE_SLOT_TYPE eSaveSlotType
	#ENDIF
	
	sACMData.eArcadeCabinetPropertyType = missionScriptArgs.eArcadeCabinetPropertyType
	
	INT i 
	FOR i = 0 TO GET_ARCADE_SLOT_COUNT_FOR_PROPERTY(sACMData.eArcadeCabinetPropertyType) - 1
		sACMData.eArcadeCab[i] 							= missionScriptArgs.eArcadeCab[i]
		
		#IF IS_DEBUG_BUILD
		IF g_DebugArcadeCabinetManagerData.bRandomiseCabinets
			eSaveSlotType =  GET_ARCADE_CABINET_SAVE_SLOT_TYPE(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner, INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, i), sACMData.eArcadeCabinetPropertyType)
			sACMData.eArcadeCab[i] = GET_ARCADE_CABINET_FROM_DISPLAY_SLOT_TYPE(eSaveSlotType)
		ENDIF
		#ENDIF
		
		g_ArcadeCabinetManagerData.iAnimVariation[i]	= missionScriptArgs.iAnimVariation[i]
		
		sACMData.iNumOfArcadeCabinets++
		PRINTLN("[AM_ARC_CAB] - INITIALISE_ACM_LOCAL_VARIABLES GET_NUM_ARCADE_CABINETS(): ", GET_NUM_ARCADE_CABINETS(), " Type: ", GET_ARCADE_CABINET_NAME(sACMData.eArcadeCab[i]), " display slot: ", i)
	ENDFOR
	
	#IF IS_DEBUG_BUILD
	g_DebugArcadeCabinetManagerData.bRandomiseCabinets = FALSE
	#ENDIF
	
	RESET_ALL_ARCADE_SOUND_INDEX_BIT_SETS(sACMData, TRUE)
	
	sACMData.iRoomKey = missionScriptArgs.iRoomKey
	
	g_sCasinoArcadeInvadeAndPersuadeVars.ePlayerState = INT_TO_ENUM(CASINO_ARCADE_INVADE_PERSUADE_PLAYER_STATE, -1)
	g_sCasinoArcadeStreetCrimeVars.ePlayerState = INT_TO_ENUM(CASINO_ARCADE_STREET_CRIME_PLAYER_STATE, -1)
ENDPROC

/// PURPOSE:
///    Sets all the used server display slots to -1 
PROC INITIALISE_SERVER_DISPLAY_SLOT_USING_DATA()
	INT iDisplaySlot, iPlayerSlot
	FOR iDisplaySlot = 0 TO GET_ARCADE_SLOT_COUNT_FOR_PROPERTY(sACMData.eArcadeCabinetPropertyType) - 1
		FOR iPlayerSlot = 0 TO ENUM_TO_INT(ARCADE_CAB_MAX_NUM_PLAYERS) - 1
			serverBD.iDisplaySlotUsedByPlayer[iDisplaySlot][iPlayerSlot] = -1
		ENDFOR	
	ENDFOR
ENDPROC

PROC INITIALISE_SERVER_DATA()
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		INITIALISE_SERVER_DISPLAY_SLOT_USING_DATA()
	ENDIF
ENDPROC

PROC INITIALISE(ACM_LAUNCHER_STRUCT missionScriptArgs)
	SET_ALL_VAIABLES_TO_INVALID()
	INITIALISE_ACM_LOCAL_VARIABLES(missionScriptArgs)
	IF GET_NUM_ARCADE_CABINETS() > 0
		SET_ARCADE_CABINET_MANAGER_STATE(ARCADE_CABINET_INIT_PROPS_STATE)
	ELSE
		PRINTLN("[AM_ARC_CAB] - INITIALISE GET_NUM_ARCADE_CABINETS(): ", GET_NUM_ARCADE_CABINETS())	
	ENDIF
ENDPROC

/// PURPOSE:
///    Initialise script data
PROC SCRIPT_INITIALISE(ACM_LAUNCHER_STRUCT missionScriptArgs)
	#IF IS_DEBUG_BUILD
	PRINTLN("[AM_ARC_CAB] - SCRIPT_INITIALISE - Launching instance: ", missionScriptArgs.iInstanceId)
	#ENDIF
		
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, missionScriptArgs.iInstanceId)
	
	// This makes sure the net script is active, waits untill it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	//Set all sound IDs to -1
	SET_ALL_VAIABLES_TO_INVALID()

	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		PRINTLN("[AM_ARC_CAB] - SCRIPT_INITIALISE FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF

	IF NETWORK_IS_GAME_IN_PROGRESS()
		PRINTLN("[AM_ARC_CAB] INITIALISED arcade cabinet manager script")
		
		// This script will not be paused if another script calls PAUSE_GAME
		PRINTLN("[AM_ARC_CAB] - INITIALISED")
	ELSE
		PRINTLN("[AM_ARC_CAB] - INITIALISED NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
	INITIALISE(missionScriptArgs)
	INITIALISE_SERVER_DATA()
	
	#IF IS_DEBUG_BUILD
	CREATE_DEBUG_WIDGET()
	#ENDIF
ENDPROC

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════╡     FUNCS & PROC     ╞══════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Server assings player index to display slot
///    iSlotToCheck = display slot 
///    iAssignPlayerIndex = player index assigned to slot
///    iPlayerSlot = player slot max ARCADE_CAB_MAX_NUM_PLAYERS (Some arcade machines can have more than 1 players)
PROC SET_PLAYER_USING_THIS_DISPLAY_SLOT(INT iSlotToCheck, INT iPlayerSlot, INT iAssignPlayerIndex)
	IF iSlotToCheck > -1 
	AND iSlotToCheck < GET_ARCADE_SLOT_COUNT_FOR_PROPERTY(sACMData.eArcadeCabinetPropertyType)
		IF iPlayerSlot > -1
		AND iPlayerSlot < ENUM_TO_INT(ARCADE_CAB_MAX_NUM_PLAYERS)
			IF serverBD.iDisplaySlotUsedByPlayer[iSlotToCheck][iPlayerSlot] != iAssignPlayerIndex
				serverBD.iDisplaySlotUsedByPlayer[iSlotToCheck][iPlayerSlot] = iAssignPlayerIndex
				PRINTLN("[AM_ARC_CAB] - SET_PLAYER_USING_THIS_DISPLAY_SLOT - iSlotToCheck: ", iSlotToCheck , " iPlayerSlot: ", iPlayerSlot, " iAssignPlayerIndex: "
						, iAssignPlayerIndex , " player name: ", iAssignPlayerIndex)
			ENDIF	
		ELSE
			PRINTLN("[AM_ARC_CAB] - SET_PLAYER_USING_THIS_DISPLAY_SLOT - INVALID iPlayerSlot: ", iPlayerSlot)
		ENDIF
	ELSE	
		PRINTLN("[AM_ARC_CAB] - SET_PLAYER_USING_THIS_DISPLAY_SLOT - INVALID iSlotToCheck: ", iSlotToCheck)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_KILL_THIS_SCRIPT()
	IF SHOULD_KILL_ACM_SCRIPT()
		PRINTLN("[AM_ARC_CAB] - SHOULD_KILL_THIS_SCRIPT - SHOULD_KILL_DRONE_SCRIPT TRUE")
		RETURN TRUE
	ENDIF
	
	IF NOT IS_SKYSWOOP_AT_GROUND()
		PRINTLN("[AM_ARC_CAB] - SHOULD_KILL_THIS_SCRIPT - IS_SKYSWOOP_AT_GROUND is false - TRUE")
		RETURN TRUE
	ENDIF
	
	IF PLAYER_ID() != INVALID_PLAYER_INDEX()
		IF IS_PLAYER_SPECTATING(PLAYER_ID())
		AND NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
		AND NOT g_bMPTVplayerWatchingTV
			PRINTLN("[AM_ARC_CAB] - SHOULD_KILL_THIS_SCRIPT - IS_PLAYER_SPECTATING is true - TRUE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		PRINTLN("[AM_ARC_CAB] - SHOULD_KILL_THIS_SCRIPT - IS_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR is true - TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
		PRINTLN("[AM_ARC_CAB] - SHOULD_KILL_THIS_SCRIPT - IS_PLAYER_ENTERING_OR_EXITING_PROPERTY is true - TRUE")
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC UPDATE_ARCADE_CABINET_STAGGER()
	sACMData.iArcadeCabinetStagger = (sACMData.iArcadeCabinetStagger + 1) % GET_NUM_ARCADE_CABINETS()
ENDPROC

/// PURPOSE:
///    Has all arcade cabinet props created
FUNC BOOL HAS_ALL_PROPS_CREATED()
	INT i 
	
	FOR i = 0 TO GET_NUM_ARCADE_CABINETS() - 1
		IF NOT DOES_ENTITY_EXIST(sACMData.arcadeCabinetProp[i])
		AND sACMData.eArcadeCab[i] != ARCADE_CABINET_INVALID
		AND SHOULD_SPAWN_THIS_ARCADE_CABINET(sACMData.eArcadeCab[i])
			PRINTLN("[AM_ARC_CAB] - HAS_ALL_PROPS_CREATED - waiting on ", i)
			RETURN FALSE
		ENDIF
	ENDFOR 
	RETURN TRUE
ENDFUNC 

/// PURPOSE:
///    Update arcade cabinet model stagger (some arcade cabinets comes as set of 4 e.g. street crime)
PROC UPDATE_PROP_MODEL_VARIATION()
	INT iPreviousArcCabStagger = sACMData.iArcadeCabinetStagger - 1
	
	IF sACMData.eArcadeCab[sACMData.iArcadeCabinetStagger] = ARCADE_CABINET_CA_STREET_CRIMES
		
		IF iPreviousArcCabStagger > -1 
			IF sACMData.eArcadeCab[iPreviousArcCabStagger] != ARCADE_CABINET_CA_STREET_CRIMES
				sACMData.iPropModelVar = 0
			ENDIF
		ENDIF	
		
		IF sACMData.iPropModelVar >= ARCADE_CABINET_MAX_PROP_MODEL_VAR
			sACMData.iPropModelVar = 0
		ENDIF
		
		sACMData.iPropModelVar++
	ELSE
		sACMData.iPropModelVar = 0
	ENDIF
ENDPROC

FUNC INT GET_RANDOM_SLOT_TO_PLAY_SOUND_FOR_THIS_ARCADE_CABINET_TYPE(ARCADE_CABINET_SOUND_AREA arcadeCabinetSoundArea)
	PLAYER_INDEX pOwner = GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner
	
	IF pOwner = INVALID_PLAYER_INDEX()
		RETURN -1
	ENDIF	
	
	INT iCabinetOfType[ACM_SLOT_COUNT]
	INT iNumCabinet = 0
	INT iSlot, iRandomSlot
	ARCADE_CABINETS eArcadeCabinet
	TEXT_LABEL_63 tlSoundSet, tlSoundName
	
	FOR iSlot = 0 TO GET_ARCADE_SLOT_COUNT_FOR_PROPERTY(sACMData.eArcadeCabinetPropertyType) - 1
		IF IS_ARCADE_CABINET_SLOT_IN_THIS_SOUND_AREA(arcadeCabinetSoundArea, INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, iSlot))
			eArcadeCabinet = GET_ARCADE_CABINET_FROM_DISPLAY_SLOT_TYPE(GET_ARCADE_CABINET_SAVE_SLOT_TYPE(pOwner, INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, iSlot), sACMData.eArcadeCabinetPropertyType))
			IF eArcadeCabinet != ARCADE_CABINET_INVALID
				GET_ARCADE_CABINET_SOUND_DETAIL(eArcadeCabinet, tlSoundSet, tlSoundName, FALSE)
				IF NOT IS_STRING_NULL_OR_EMPTY(tlSoundName)
					IF NOT IS_BIT_SET(sACMData.iArcadeTypeSound, ENUM_TO_INT(eArcadeCabinet))
						iCabinetOfType[iNumCabinet] = iSlot
						iNumCabinet++	
						PRINTLN("[AM_ARC_CAB] - GET_RANDOM_SLOT_TO_PLAY_SOUND_FOR_THIS_ARCADE_CABINET_TYPE iNumCabinet: ", iNumCabinet, " cabinet area : ", GET_ARCADE_CABINET_SOUND_AREA_NAME(arcadeCabinetSoundArea))
					ENDIF	
				ENDIF	
			ENDIF
		ENDIF
	ENDFOR 
	
	IF iNumCabinet > 0 
		iRandomSlot = GET_RANDOM_INT_IN_RANGE(1, iNumCabinet)
		iRandomSlot --
		IF iRandomSlot > -1
			IF sACMData.eArcadeCab[iCabinetOfType[iRandomSlot]] != ARCADE_CABINET_INVALID
				PRINTLN("[AM_ARC_CAB] - GET_RANDOM_SLOT_TO_PLAY_SOUND_FOR_THIS_ARCADE_CABINET_TYPE siRandomSlot: ", iRandomSlot, " found type: "
						, GET_ARCADE_CABINET_NAME(sACMData.eArcadeCab[iCabinetOfType[iRandomSlot]]), " selected slot: ", iCabinetOfType[iRandomSlot])
				
				SET_BIT(sACMData.iArcadeTypeSound, ENUM_TO_INT(sACMData.eArcadeCab[iCabinetOfType[iRandomSlot]]))
				RETURN  iCabinetOfType[iRandomSlot]
			ELSE
				PRINTLN("[AM_ARC_CAB] - GET_RANDOM_SLOT_TO_PLAY_SOUND_FOR_THIS_ARCADE_CABINET_TYPE sACMData.eArcadeCab[iCabinetOfType[iRandomSlot]]: ARCADE_CABINET_INVALID")
			ENDIF
		ENDIF	
	ENDIF
	
	RETURN -1
ENDFUNC

/// PURPOSE:
///    Updates arcade cabinet index which we want to play the attract loop sound
PROC UPDATE_ARCADE_SOUND_INDEX()
	IF IS_LOCAL_PLAYER_USING_ARCADE_MANAGEMENT_MENU()
		EXIT
	ENDIF	
	
	IF sACMData.eArcadeCabinetManagerState = ARCADE_CABINET_INIT_PROPS_STATE
	OR sACMData.eArcadeCabinetManagerState = ARCADE_CABINET_INIT_CHILD_SCRIPT_STATE
	OR sACMData.eArcadeCabinetManagerState = ARCADE_CABINET_CLEANUP_STATE
		EXIT
	ENDIF	
	
	IF !IS_BIT_SET(sACMData.iSoundIndexBS, ENUM_TO_INT(ARCADE_CABINET_SOUND_ENTRANCE_AREA_1))
		sACMData.iArcadeTypeIndex[ENUM_TO_INT(ARCADE_CABINET_SOUND_ENTRANCE_AREA_1)] = GET_RANDOM_SLOT_TO_PLAY_SOUND_FOR_THIS_ARCADE_CABINET_TYPE(ARCADE_CABINET_SOUND_ENTRANCE_AREA_1)
		SET_BIT(sACMData.iSoundIndexBS, ENUM_TO_INT(ARCADE_CABINET_SOUND_ENTRANCE_AREA_1))
		PRINTLN("[AM_ARC_CAB] - UPDATE_ARCADE_SOUND_INDEX ARCADE_CABINET_SOUND_ENTRANCE_AREA_1 TRUE")
	ENDIF
	
	IF !IS_BIT_SET(sACMData.iSoundIndexBS, ENUM_TO_INT(ARCADE_CABINET_SOUND_ENTRANCE_AREA_2))
		sACMData.iArcadeTypeIndex[ENUM_TO_INT(ARCADE_CABINET_SOUND_ENTRANCE_AREA_2)] = GET_RANDOM_SLOT_TO_PLAY_SOUND_FOR_THIS_ARCADE_CABINET_TYPE(ARCADE_CABINET_SOUND_ENTRANCE_AREA_2)
		SET_BIT(sACMData.iSoundIndexBS, ENUM_TO_INT(ARCADE_CABINET_SOUND_ENTRANCE_AREA_2))
		PRINTLN("[AM_ARC_CAB] - UPDATE_ARCADE_SOUND_INDEX ARCADE_CABINET_SOUND_ENTRANCE_AREA_2 TRUE")
	ENDIF
	
	IF !IS_BIT_SET(sACMData.iSoundIndexBS, ENUM_TO_INT(ARCADE_CABINET_SOUND_UPPER_MACHINE_1))
		sACMData.iArcadeTypeIndex[ENUM_TO_INT(ARCADE_CABINET_SOUND_UPPER_MACHINE_1)] = GET_RANDOM_SLOT_TO_PLAY_SOUND_FOR_THIS_ARCADE_CABINET_TYPE(ARCADE_CABINET_SOUND_UPPER_MACHINE_1)
		SET_BIT(sACMData.iSoundIndexBS, ENUM_TO_INT(ARCADE_CABINET_SOUND_UPPER_MACHINE_1))
		PRINTLN("[AM_ARC_CAB] - UPDATE_ARCADE_SOUND_INDEX ARCADE_CABINET_SOUND_UPPER_MACHINE_1 TRUE")
	ENDIF
	
	IF !IS_BIT_SET(sACMData.iSoundIndexBS, ENUM_TO_INT(ARCADE_CABINET_SOUND_UPPER_MACHINE_2))
		sACMData.iArcadeTypeIndex[ENUM_TO_INT(ARCADE_CABINET_SOUND_UPPER_MACHINE_2)] = GET_RANDOM_SLOT_TO_PLAY_SOUND_FOR_THIS_ARCADE_CABINET_TYPE(ARCADE_CABINET_SOUND_UPPER_MACHINE_2)
		SET_BIT(sACMData.iSoundIndexBS, ENUM_TO_INT(ARCADE_CABINET_SOUND_UPPER_MACHINE_2))
		PRINTLN("[AM_ARC_CAB] - UPDATE_ARCADE_SOUND_INDEX ARCADE_CABINET_SOUND_UPPER_MACHINE_2 TRUE")
	ENDIF
	
	IF !IS_BIT_SET(sACMData.iSoundIndexBS, ENUM_TO_INT(ARCADE_CABINET_SOUND_WALL_OPPOSITE_BAR))
		sACMData.iArcadeTypeIndex[ENUM_TO_INT(ARCADE_CABINET_SOUND_WALL_OPPOSITE_BAR)] = GET_RANDOM_SLOT_TO_PLAY_SOUND_FOR_THIS_ARCADE_CABINET_TYPE(ARCADE_CABINET_SOUND_WALL_OPPOSITE_BAR)
		SET_BIT(sACMData.iSoundIndexBS, ENUM_TO_INT(ARCADE_CABINET_SOUND_WALL_OPPOSITE_BAR))
		PRINTLN("[AM_ARC_CAB] - UPDATE_ARCADE_SOUND_INDEX ARCADE_CABINET_SOUND_WALL_OPPOSITE_BAR TRUE")
	ENDIF
	
	IF !IS_BIT_SET(sACMData.iSoundIndexBS, ENUM_TO_INT(ARCADE_CABINET_SOUND_CENTRAL_PILLAR))
		sACMData.iArcadeTypeIndex[ENUM_TO_INT(ARCADE_CABINET_SOUND_CENTRAL_PILLAR)] = GET_RANDOM_SLOT_TO_PLAY_SOUND_FOR_THIS_ARCADE_CABINET_TYPE(ARCADE_CABINET_SOUND_CENTRAL_PILLAR)
		SET_BIT(sACMData.iSoundIndexBS, ENUM_TO_INT(ARCADE_CABINET_SOUND_CENTRAL_PILLAR))
		PRINTLN("[AM_ARC_CAB] - UPDATE_ARCADE_SOUND_INDEX ARCADE_CABINET_SOUND_CENTRAL_PILLAR TRUE")
	ENDIF
	
	IF !IS_BIT_SET(sACMData.iSoundIndexBS, ENUM_TO_INT(ARCADE_CABINET_SOUND_LOWER_FLOOR_BARRIER_EDGE))
		sACMData.iArcadeTypeIndex[ENUM_TO_INT(ARCADE_CABINET_SOUND_LOWER_FLOOR_BARRIER_EDGE)] = GET_RANDOM_SLOT_TO_PLAY_SOUND_FOR_THIS_ARCADE_CABINET_TYPE(ARCADE_CABINET_SOUND_LOWER_FLOOR_BARRIER_EDGE)
		SET_BIT(sACMData.iSoundIndexBS, ENUM_TO_INT(ARCADE_CABINET_SOUND_LOWER_FLOOR_BARRIER_EDGE))
		PRINTLN("[AM_ARC_CAB] - UPDATE_ARCADE_SOUND_INDEX ARCADE_CABINET_SOUND_LOWER_FLOOR_BARRIER_EDGE TRUE")
	ENDIF
ENDPROC

/// PURPOSE:
///    Process arcade cabinet prop creation
PROC MAINTAIN_ACM_INIT_PROP_STATE()
	IF GET_NUM_ARCADE_CABINETS() > 0 
		IF !HAS_ALL_PROPS_CREATED()
			CREATE_ARCADE_CABINETS(sACMData, sACMData.iArcadeCabinetStagger, sACMData.iPropModelVar)
		ELSE
			// Wait for host to create sub props 
			IF serverBD.eServerState = ARCADE_CABINET_SERVER_IDLE_STATE
				SET_ARCADE_CABINET_MANAGER_STATE(ARCADE_CABINET_INIT_CHILD_SCRIPT_STATE)
			ELSE
				PRINTLN("[AM_ARC_CAB] - MAINTAIN_ACM_INIT_PROP_STATE - Wait for host to create sub props. serverBD.eServerState: ", serverBD.eServerState)
			ENDIF	
		ENDIF	
	ENDIF	
ENDPROC

/// PURPOSE:
///    Has all child scripts started
FUNC BOOL HAS_ALL_CHILD_SCRIPT_LAUNCHED()
	INT i 
	FOR i = 0 TO GET_NUM_ARCADE_CABINETS() - 1
		IF NOT IS_ARCADE_CABINET_MANAGER_BIT_SET(i, ARCADE_CABINET_MANAGER_STARTED_CHILD_SCRIPT)
		AND sACMData.eArcadeCab[i] != ARCADE_CABINET_INVALID
			PRINTLN("[AM_ARC_CAB] - HAS_ALL_CHILD_SCRIPT_LAUNCHED - waiting on ", i , " cabinet type: ", GET_ARCADE_CABINET_NAME(sACMData.eArcadeCab[i]))
			RETURN FALSE
		ENDIF
	ENDFOR 
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Start child scripts if stagger arcade cabinet has one.
FUNC BOOL  MAINTAIN_ACM_INIT_CHILD_SCRIPT_STATE()
	IF sACMData.eArcadeCab[sACMData.iArcadeCabinetStagger] != ARCADE_CABINET_INVALID
		IF IS_THIS_PHYSICAL_ARCADE_CABINET(sACMData.eArcadeCab[sACMData.iArcadeCabinetStagger])
			IF SHOULD_TERMINATE_PHYSICAL_ARDCADE_GAME(sACMData.eArcadeCab[sACMData.iArcadeCabinetStagger])
				RETURN FALSE
			ENDIF	
		ENDIF	
	ENDIF
	
	IF !HAS_ALL_CHILD_SCRIPT_LAUNCHED()
		IF sACMData.eArcadeCab[sACMData.iArcadeCabinetStagger] != ARCADE_CABINET_INVALID
			IF DOES_ARCADE_CABINET_USE_CHILD_SCRIPT(sACMData.eArcadeCab[sACMData.iArcadeCabinetStagger], sACMData.strCabinetChildScript, FALSE)
				IF NOT IS_STRING_NULL_OR_EMPTY(sACMData.strCabinetChildScript)
					IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(sACMData.strCabinetChildScript)) = 0
						IF NOT NETWORK_IS_SCRIPT_ACTIVE(sACMData.strCabinetChildScript, NETWORK_GET_INSTANCE_ID_OF_THIS_SCRIPT(), TRUE)
							REQUEST_SCRIPT(sACMData.strCabinetChildScript)
							
							IF HAS_SCRIPT_LOADED(sACMData.strCabinetChildScript)
								
								INT iSubPropIndex
								ACM_SUB_PROP_DETAILS eArcadeCabSubPropDetail
								ACM_CHILD_SCRIPT_LAUNCHER_STRUCT sMPMissionData
								sMPMissionData.iInstanceId = NETWORK_GET_INSTANCE_ID_OF_THIS_SCRIPT()
								sMPMissionData.cabinetObj = sACMData.arcadeCabinetProp[sACMData.iArcadeCabinetStagger]
								sMPMissionData.eArcadeCabinetLocateType = sACMData.eArcadeCab[sACMData.iArcadeCabinetStagger]
								
								GET_ARCADE_CABINET_SUB_PROP_DETAIL(sACMData.eArcadeCab[sACMData.iArcadeCabinetStagger], eArcadeCabSubPropDetail)
								
								IF eArcadeCabSubPropDetail.iNumProp > 0 
									FOR iSubPropIndex = 0 TO eArcadeCabSubPropDetail.iNumProp - 1
										IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.serverArcadeCabinetSubObj[sACMData.iArcadeCabinetStagger][iSubPropIndex])
											sMPMissionData.cabinetSubObj[iSubPropIndex] = NET_TO_OBJ(serverBD.serverArcadeCabinetSubObj[sACMData.iArcadeCabinetStagger][iSubPropIndex])
										ELSE
											PRINTLN("[AM_ARC_CAB] - MAINTAIN_ACM_INIT_CHILD_SCRIPT_STATE wait for sub prop to create arcade cabinet stagger: ", sACMData.iArcadeCabinetStagger, " prop index: ", iSubPropIndex)
											RETURN FALSE
										ENDIF
									ENDFOR
								ENDIF

								START_NEW_SCRIPT_WITH_ARGS(sACMData.strCabinetChildScript, sMPMissionData, SIZE_OF(sMPMissionData), DEFAULT_STACK_SIZE)
									
								SET_SCRIPT_AS_NO_LONGER_NEEDED(sACMData.strCabinetChildScript)
									
								PRINTLN("[AM_ARC_CAB] - MAINTAIN_ACM_INIT_CHILD_SCRIPT_STATE start ", sACMData.strCabinetChildScript , " Instance: ", sMPMissionData.iInstanceId)
								SET_ARCADE_CABINET_MANAGER_BIT(sACMData.iArcadeCabinetStagger, ARCADE_CABINET_MANAGER_STARTED_CHILD_SCRIPT)	
							ENDIF
						ENDIF	
					ELSE
						SET_ARCADE_CABINET_MANAGER_BIT(sACMData.iArcadeCabinetStagger, ARCADE_CABINET_MANAGER_STARTED_CHILD_SCRIPT)
					ENDIF
				ENDIF
			ELSE
				SET_ARCADE_CABINET_MANAGER_BIT(sACMData.iArcadeCabinetStagger, ARCADE_CABINET_MANAGER_STARTED_CHILD_SCRIPT)
			ENDIF
		ENDIF	
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_PLAYING_ARCADE_EXIT_ANIM()
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		INT iLocatePlayerIsIn = GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID())
		IF iLocatePlayerIsIn > -1
			ARCADE_CABINET_ANIM_DETAIL eAnimDetails
			IF sACMData.eArcadeCab[iLocatePlayerIsIn] != ARCADE_CABINET_INVALID
				GET_ARCADE_CABINET_ANIMATION_DETAIL(sACMData.eArcadeCab[iLocatePlayerIsIn], ARCADE_CABINET_ANIM_CLIP_EXIT, GET_ARCADE_CABINET_LOCATE_PLAYER_SLOT_PLAYER_IS_IN(PLAYER_ID()), eAnimDetails)
				IF !IS_STRING_NULL_OR_EMPTY(eAnimDetails.sAnimClipName)
					RETURN IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), eAnimDetails.strArcCabinetAnimDic, eAnimDetails.sAnimClipName)
				ENDIF	
			ENDIF	
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_ARCADE_CABINET_EXIT_ANIMATION_CLIP_FINISHED()
	IF IS_LOCAL_PLAYER_PLAYING_ARCADE_EXIT_ANIM()
		IF HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(ARCADE_CABINET_ANIM_CLIP_EXIT)
			RETURN TRUE
		ENDIF	
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Can player start arcade game
FUNC BOOL IS_SAFE_TO_START_ARCADE_GAME(TEXT_LABEL_63 &sBlockReason, TEXT_LABEL_63 &sBlockReasonStringTwo, BOOL bCheckCabinetUsage = FALSE, BOOL bSafeForLocateCheck = FALSE)
	IF IS_LOCAL_PLAYER_USING_ARCADE_MANAGEMENT_MENU()
		#IF IS_DEBUG_BUILD
		IF GET_FRAME_COUNT() % 60 = 0
			PRINTLN("[AM_ARC_CAB] IS_SAFE_TO_START_ARCADE_GAME ARC_CAB_MANAGER_GLOBAL_BIT_SET_TRIGGER_ARCADE_MANAGMENT_MENU true")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF !IS_PLAYER_IN_ARCADE_CABINET_LOCATE(PLAYER_ID())
	AND !bSafeForLocateCheck
		RETURN FALSE
	ENDIF	
	
	INT iDisplayLocatePlayerIn = GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID())
	INT iplayerSlotPlayerIn = GET_ARCADE_CABINET_LOCATE_PLAYER_SLOT_PLAYER_IS_IN(PLAYER_ID())
	
	IF iDisplayLocatePlayerIn = -1
	AND !bSafeForLocateCheck
		PRINTLN("[AM_ARC_CAB] IS_SAFE_TO_START_ARCADE_GAME display locate is -1")
		RETURN FALSE
	ENDIF
	
	INT iPlayerUsingSlot = GET_PLAYER_INDEX_USING_THIS_ARCADE_CABINET_DISPLAY_SLOT(serverBD, iDisplayLocatePlayerIn, iplayerSlotPlayerIn, sACMData.eArcadeCabinetPropertyType)
	IF iPlayerUsingSlot != -1
	AND iPlayerUsingSlot != NATIVE_TO_INT(PLAYER_ID())
	AND !bSafeForLocateCheck
		PRINTLN("[AM_ARC_CAB] IS_SAFE_TO_START_ARCADE_GAME display slot not free player ", iDisplayLocatePlayerIn , " is using arcade in: " , iDisplayLocatePlayerIn
				, " using player: ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iPlayerUsingSlot)))
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_USING_ARCADE_CABINET(PLAYER_ID())	
	AND !bSafeForLocateCheck
		PRINTLN("[AM_ARC_CAB] IS_SAFE_TO_START_ARCADE_GAME player is using cabinet")
		RETURN FALSE
	ENDIF	
	
	IF bCheckCabinetUsage
	AND !bSafeForLocateCheck
		IF !IS_PLAYER_REQUESTING_TO_USE_CABINET(PLAYER_ID())
			#IF IS_DEBUG_BUILD
			IF GET_FRAME_COUNT() % 60 = 0
				PRINTLN("[AM_ARC_CAB] IS_SAFE_TO_START_ARCADE_GAME player not requesting to use cabinet")
			ENDIF	
			#ENDIF
			RETURN FALSE
		ENDIF	
	ENDIF
	
	IF IS_BIT_SET(g_ArcadeCabinetManagerData.iBS, ARC_CAB_MANAGER_GLOBAL_BIT_SET_UPDATE_PROPS)	
		PRINTLN("[AM_ARC_CAB] IS_SAFE_TO_START_ARCADE_GAME player update prop")
		RETURN FALSE
	ENDIF
	
	ARCADE_CABINETS eCabinetPlayerIsIn = GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID())
	
	// Check if there is on going street crime game
	IF eCabinetPlayerIsIn = ARCADE_CABINET_CA_STREET_CRIMES
		INT iStartLoop, iEndLoop, iLoop
		GET_FOUR_PLAYER_ARCADE_CABINET_START_AND_END_SLOT(INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, iDisplayLocatePlayerIn), iStartLoop, iEndLoop)
		
		FOR iLoop = iStartLoop TO iEndLoop
			INT iPlayerUsingIndex = GET_PLAYER_INDEX_USING_THIS_ARCADE_CABINET_DISPLAY_SLOT(serverBD, iLoop, 0, sACMData.eArcadeCabinetPropertyType)
			PLAYER_INDEX playerUsingIndex 
			IF iPlayerUsingIndex != -1
				playerUsingIndex = INT_TO_PLAYERINDEX(iPlayerUsingIndex)
				IF playerUsingIndex != PLAYER_ID()
					IF ENUM_TO_INT(globalPlayerBD[NATIVE_TO_INT(playerUsingIndex)].sArcadeManagerGlobalPlayerBD.eStreetCrimePlayerState) > ENUM_TO_INT(CASINO_ARCADE_STREET_CRIME_PLAYER_STATE_READY_UP)
						PRINTLN("[AM_ARC_CAB] - IS_SAFE_TO_START_ARCADE_GAME - some one using street crime cabinet")
						sBlockReason = "ARC_BLOCK_R_5"
						RETURN FALSE
					ENDIF	
				
					IF IS_PLAYER_PLAYING_ARCADE_GAME_WITH_AI(playerUsingIndex)
						PRINTLN("[AM_ARC_CAB] - IS_SAFE_TO_START_ARCADE_GAME - some one using street crime cabinet with AI")
						RETURN FALSE
					ENDIF 
				ENDIF	
			ENDIF
		ENDFOR	
	ENDIF
	
	IF IS_CELLPHONE_CAMERA_IN_USE()
		RETURN FALSE
	ENDIF	
	
	IF IS_PHONE_ONSCREEN()
		RETURN FALSE
	ENDIF	
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		RETURN FALSE
	ENDIF
	
	IF Get_Peds_Drunk_Alcohol_Hit_Count(PLAYER_PED_ID()) >= 10
		RETURN FALSE
	ENDIF
	
	IF g_SpawnData.bPassedOutDrunk 
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_INSTANT_PASS_OUT)
		RETURN FALSE
	ENDIF
	
	IF IS_PED_RAGDOLL(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_PAUSE_MENU_ACTIVE()
		PRINTLN("[AM_ARC_CAB] IS_SAFE_TO_START_ARCADE_GAME pause menu active")
		RETURN FALSE
	ENDIF
	
	IF IS_BROWSER_OPEN()
		PRINTLN("[AM_ARC_CAB] IS_SAFE_TO_START_ARCADE_GAME browser open")
		RETURN FALSE
	ENDIF
	
	IF IS_INTERACTION_MENU_OPEN()
		PRINTLN("[AM_ARC_CAB] IS_SAFE_TO_START_ARCADE_GAME interaction menu is open")
		RETURN FALSE
	ENDIF
	
	IF IS_SELECTOR_ONSCREEN()
		PRINTLN("[AM_ARC_CAB] IS_SAFE_TO_START_ARCADE_GAME selector is open")
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_PLAYING_ARCADE_EXIT_ANIM()
		PRINTLN("[AM_ARC_CAB] IS_SAFE_TO_START_ARCADE_GAME local player is playing exit anim")
		RETURN FALSE
	ENDIF
	
	IF !bSafeForLocateCheck
	AND IS_ARCADE_CABINET_MANAGER_BIT_SET(iDisplayLocatePlayerIn, ARCADE_CABINET_MANAGER_EXIT_ARCADE_CABINET)
		PRINTLN("[AM_ARC_CAB] IS_SAFE_TO_START_ARCADE_GAME ARCADE_CABINET_MANAGER_EXIT_ARCADE_CABINET TRUE")
		RETURN FALSE	
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBS, ARCADE_CABINET_MANAGER_SERVER_BD_MANAGEMENT_MENU_IN_USE)
		sBlockReason = "ARC_BLOCK_R_1"
		sBlockReasonStringTwo = ""
		PRINTLN("[AM_ARC_CAB] IS_SAFE_TO_START_ARCADE_GAME owner is modifying this slot")
		RETURN FALSE
	ENDIF
	
	IF !IS_ARCADE_CABINET_FREE_TO_USE(eCabinetPlayerIsIn)
		IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner != PLAYER_ID()
			IF !NETWORK_CAN_SPEND_MONEY(1, FALSE, FALSE, TRUE)
				sBlockReason = "ARC_BLOCK_R_2"
				sBlockReasonStringTwo = ""
				PRINTLN("[AM_ARC_CAB] IS_SAFE_TO_START_ARCADE_GAME not enough money")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF	

	IF IS_MP_PASSIVE_MODE_ENABLED()
		PRINTLN("[AM_ARC_CAB] IS_SAFE_TO_START_ARCADE_GAME passive mode is active")
		sBlockReason = "MPCT_UNVLPASS"
		RETURN FALSE
	ENDIF
	
	IF GB_IS_LOCAL_PLAYER_PERMANENT_TO_GANG_BOSS_MISSION()
		PRINTLN("[AM_ARC_CAB] IS_SAFE_TO_START_ARCADE_GAME GB_IS_LOCAL_PLAYER_PERMANENT_TO_GANG_BOSS_MISSION true")
		RETURN FALSE
	ENDIF
	
	SWITCH eCabinetPlayerIsIn
		CASE ARCADE_CABINET_CH_DRONE_MINI_G
			IF IS_LOCAL_DRONE_IN_COOL_DOWN()
			AND !IS_DRONE_USE_FROM_PI_MENU_ENABLED_IN_MISSION()
				INT iCD
				iCD = GET_HACKER_TRUCK_DRONE_SEAT_COOLDOWN(PLAYER_ID())
				IF iCD  != -1
					iCD = GET_DRONE_COOL_DOWN_TIMER() - iCD
					IF iCD < 0
						iCD = 0
					ENDIF
				ENDIF
				sBlockReasonStringTwo = GET_TEXT_OF_TIME(iCD, FALSE, TRUE)
				sBlockReason = "PIM_DRONAVT"
				PRINTLN("[AM_ARC_CAB] IS_SAFE_TO_START_ARCADE_GAME drone in cool down")
				RETURN FALSE
			ENDIF
		BREAK
		CASE ARCADE_CABINET_CH_DRILLING_LASER_MINI_G
			IF SHOULD_BLOCK_LASER_OPTION(sBlockReason)
			AND SHOULD_BLOCK_DRILL_OPTION(sBlockReason)
				sBlockReason = "ARC_BLOCK_R_4"
				sBlockReasonStringTwo = ""
				PRINTLN("[AM_ARC_CAB] IS_SAFE_TO_START_ARCADE_GAME no drill or laser")
				RETURN FALSE
			ENDIF
		BREAK
		CASE ARCADE_CABINET_CH_CLAW_CRANE
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.serverArcadeCabinetSubObj[iDisplayLocatePlayerIn][ARCADE_CAB_OBJECT_3])
				IF IS_ENTITY_PLAYING_ANIM(NET_TO_ENT(serverBD.serverArcadeCabinetSubObj[iDisplayLocatePlayerIn][ARCADE_CAB_OBJECT_3]), "anim_heist@arcade@claw@male@", "prop_claw_win")
				OR IS_ENTITY_PLAYING_ANIM(NET_TO_ENT(serverBD.serverArcadeCabinetSubObj[iDisplayLocatePlayerIn][ARCADE_CAB_OBJECT_3]), "anim_heist@arcade@claw@female@", "prop_claw_win")	
					PRINTLN("[AM_ARC_CAB] IS_SAFE_TO_START_ARCADE_GAME claw playing win anim")
					RETURN FALSE
				ENDIF
				
				VECTOR vOffset, vCoordToCheck
				vOffset = <<CLAW_CRAN_MIN_X_AXIS, CLAW_CRAN_MIN_Y_AXIS, CLAW_CRAN_MAX_Z_AXIS>>
				vCoordToCheck = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sACMData.arcadeCabinetProp[iDisplayLocatePlayerIn], vOffset)
				FLOAT fDistance 
				fDistance = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(NET_TO_ENT(serverBD.serverArcadeCabinetSubObj[iDisplayLocatePlayerIn][ARCADE_CAB_OBJECT_3]), vCoordToCheck)
				IF fDistance > 0.03
					PRINTLN("[AM_ARC_CAB] IS_SAFE_TO_START_ARCADE_GAME claw is far fDistance: ", fDistance)
					RETURN FALSE
				ENDIF
				
				IF IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_MISS)
				OR IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_PRIZE_WIN)
					PRINTLN("[AM_ARCADE_CLAW_CRANE] IS_SAFE_TO_START_ARCADE_GAME player playing miss or win anim")
					RETURN FALSE
				ENDIF
			ENDIF
		BREAK
		CASE ARCADE_CABINET_CH_HACKING_MINI_G
			IF !HAS_PLAYER_ACQUIRED_CASINO_HEIST_HACKING_DEVICE(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner)
			#IF IS_DEBUG_BUILD
			AND !sACMDebugData.bHasHackingDevice
			#ENDIF
				sBlockReason = "ARC_BLOCK_R_6"
				PRINTLN("[AM_ARCADE_CLAW_CRANE] IS_SAFE_TO_START_ARCADE_GAME HAS_PLAYER_ACQUIRED_CASINO_HEIST_HACKING_DEVICE false")
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH

	RETURN TRUE
ENDFUNC 

PROC CLEAR_ARCADE_CABINET_BLOCK_HELP(TEXT_LABEL_63 tlBlockReason, TEXT_LABEL_63 sBlockReasonStringTwo)
	IF !IS_STRING_NULL_OR_EMPTY(tlBlockReason)
	AND IS_STRING_NULL_OR_EMPTY(sBlockReasonStringTwo)
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(tlBlockReason)
			CLEAR_HELP()
		ENDIF
	ELIF !IS_STRING_NULL_OR_EMPTY(tlBlockReason)
	AND !IS_STRING_NULL_OR_EMPTY(sBlockReasonStringTwo)
		IF IS_THIS_HELP_MESSAGE_WITH_PLAYER_NAME_BEING_DISPLAYED(tlBlockReason, sBlockReasonStringTwo)
			CLEAR_HELP()
		ENDIF
	ENDIF	
ENDPROC

/// PURPOSE:
///    Check if player is in arcade cabinet locate
PROC MAINTAIN_ARCADE_CABINET_LOCATE()
	
	IF IS_PLAYER_REQUESTING_TO_USE_CABINET(PLAYER_ID())
		PRINTLN("[AM_ARC_CAB] - MAINTAIN_ARCADE_CABINET_LOCATE player requesting to use the arcade stop locate check ")
		EXIT
	ENDIF
	
	IF IS_PLAYER_USING_ARCADE_CABINET(PLAYER_ID())
		PRINTLN("[AM_ARC_CAB] - MAINTAIN_ARCADE_CABINET_LOCATE player is using a arcade cabinet ")
		EXIT
	ENDIF
	
	TEXT_LABEL_63 tlBlockReason, sBlockReasonStringTwo
	FLOAT fLocateXMultiplier = 1.0
	FLOAT fLocateYMultiplier = 1.0
	INT iArcadeCabinetSlotToCheck = sACMData.iArcadeCabinetStagger
	INT iArcadeCabinetSlotPlayerIsIn = GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID())
	
	IF iArcadeCabinetSlotPlayerIsIn != -1
		iArcadeCabinetSlotToCheck = iArcadeCabinetSlotPlayerIsIn
	ENDIF	
	
	IF DOES_ENTITY_EXIST(sACMData.arcadeCabinetProp[iArcadeCabinetSlotToCheck])
	OR DOES_THIS_ARCADE_CABINETS_PROP_INCLUDED_IN_INTERIOR(sACMData.eArcadeCab[iArcadeCabinetSlotToCheck])
		VECTOR vOffset[ARCADE_CAB_MAX_NUM_PLAYERS], vLocateCoord, vPropCoords
		FLOAT fHeading
		FLOAT fHeadingLeeway = 80.0
		IF sACMData.eArcadeCab[iArcadeCabinetSlotToCheck] != ARCADE_CABINET_INVALID
			GET_ARCADE_CABINET_TRIGGER_LOCATE_DATA(sACMData.eArcadeCab[iArcadeCabinetSlotToCheck], vOffset)
			INT iNumPlayers = GET_NUM_PLAYERS_ARCADE_CABINET_CAN_HAVE(sACMData.eArcadeCab[iArcadeCabinetSlotToCheck])
			INT iPlayerSlot
			FOR iPlayerSlot = 0 TO iNumPlayers
						
				GET_ARCADE_CABINET_DISPLAY_SLOT_COORD_AND_HEADING(sACMData.eArcadeCabinetPropertyType, INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, iArcadeCabinetSlotToCheck), sACMData.eArcadeCab[iArcadeCabinetSlotToCheck], vPropCoords, fHeading)
				vLocateCoord = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPropCoords, fHeading, vOffset[iPlayerSlot])
				
				IF SHOULD_DOUBLE_LOCATE_X_AXIS_FOR_THIS_ARCADE_CABINET(sACMData.eArcadeCab[iArcadeCabinetSlotToCheck])
					fLocateXMultiplier = 2.0
					IF fHeading = 180
						fLocateYMultiplier = 2.0
						fLocateXMultiplier = 1.0
					ENDIF	
					
					IF iPlayerSlot = 0
						fHeading -= 45
					ELSE
						fHeading += 45
					ENDIF
				ENDIF
				
				IF sACMData.eArcadeCab[iArcadeCabinetSlotToCheck] = ARCADE_CABINET_CH_LOVE_METER
					fLocateXMultiplier = 0.7
				#IF FEATURE_SUMMER_2020
				ELIF sACMData.eArcadeCab[iArcadeCabinetSlotToCheck] = ARCADE_CABINET_SUM_STRENGTH_TEST
					fLocateXMultiplier = 1.75
					fLocateYMultiplier = 1.75
					fHeadingLeeway = 50.0
				#ENDIF
				ENDIF
				
				IF IS_SAFE_TO_START_ARCADE_GAME(tlBlockReason, sBlockReasonStringTwo, FALSE, TRUE)
				AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vLocateCoord, <<ARCADE_CABINET_LOC_DIMENSIONS_X * fLocateXMultiplier, ARCADE_CABINET_LOC_DIMENSIONS_Y * fLocateYMultiplier, ARCADE_CABINET_LOC_DIMENSIONS_Z>>
										, FALSE, TRUE, TM_ON_FOOT)
				AND IS_HEADING_ACCEPTABLE_CORRECTED(GET_ENTITY_HEADING(PLAYER_PED_ID()), fHeading, fHeadingLeeway)
					IF !IS_STRING_NULL_OR_EMPTY(tlBlockReason)
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(tlBlockReason)
							CLEAR_HELP()
						ENDIF
					ENDIF
					
					IF GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID()) != sACMData.eArcadeCab[iArcadeCabinetSlotToCheck]
						CLEAR_TRIGGER_HELP()
					ENDIF	
					
					SET_PLAYER_IN_ARCADE_CABINET_LOCATE(TRUE, iArcadeCabinetSlotToCheck, iPlayerSlot)
					SET_PLAYER_IN_ARCADE_CABINET_LOCATE_TYPE(sACMData.eArcadeCab[iArcadeCabinetSlotToCheck])
				
					IF IS_STRING_NULL_OR_EMPTY(sACMData.strTriggerHelp)
						GET_ARCADE_CABINET_TRIGGER_HELP_TEXT(sACMData.eArcadeCab[iArcadeCabinetSlotToCheck], sACMData)	
					ENDIF
				ELSE
					IF !IS_SAFE_TO_START_ARCADE_GAME(tlBlockReason, sBlockReasonStringTwo, FALSE, TRUE)
						IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vLocateCoord, <<ARCADE_CABINET_LOC_DIMENSIONS_X * fLocateXMultiplier, ARCADE_CABINET_LOC_DIMENSIONS_Y * fLocateYMultiplier, ARCADE_CABINET_LOC_DIMENSIONS_Z>>, FALSE, TRUE, TM_ON_FOOT)
							IF !IS_STRING_NULL_OR_EMPTY(tlBlockReason)
							AND IS_STRING_NULL_OR_EMPTY(sBlockReasonStringTwo)
								IF !IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(tlBlockReason)
								AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
									PRINT_HELP(tlBlockReason)
								ENDIF	
							ELIF !IS_STRING_NULL_OR_EMPTY(tlBlockReason)
							AND !IS_STRING_NULL_OR_EMPTY(sBlockReasonStringTwo)
								IF !IS_THIS_HELP_MESSAGE_WITH_PLAYER_NAME_BEING_DISPLAYED(tlBlockReason, sBlockReasonStringTwo)
								AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
									PRINT_HELP_WITH_PLAYER_NAME(tlBlockReason, sBlockReasonStringTwo, HUD_COLOUR_WHITE)
								ENDIF
							ENDIF
							
							IF NOT IS_PLAYER_USING_ARCADE_CABINET(PLAYER_ID())
								SET_PLAYER_IN_ARCADE_CABINET_LOCATE(FALSE, -1, -1)	
								SET_PLAYER_IN_ARCADE_CABINET_LOCATE_TYPE()
							ENDIF	
						ELSE
							IF iArcadeCabinetSlotToCheck = GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID())
							AND iPlayerSlot = GET_ARCADE_CABINET_LOCATE_PLAYER_SLOT_PLAYER_IS_IN(PLAYER_ID())
								IF NOT IS_PLAYER_USING_ARCADE_CABINET(PLAYER_ID())
									// player no longer in locate
									SET_PLAYER_IN_ARCADE_CABINET_LOCATE(FALSE, -1, -1)	
									SET_PLAYER_IN_ARCADE_CABINET_LOCATE_TYPE()
									CLEAR_TRIGGER_HELP()
									CLEAR_ARCADE_CABINET_BLOCK_HELP(tlBlockReason, sBlockReasonStringTwo)
								ENDIF	
							ENDIF	
						ENDIF
					ELSE
						IF iArcadeCabinetSlotToCheck = GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID())
						AND iPlayerSlot = GET_ARCADE_CABINET_LOCATE_PLAYER_SLOT_PLAYER_IS_IN(PLAYER_ID())
							IF NOT IS_PLAYER_USING_ARCADE_CABINET(PLAYER_ID())
								// player no longer in locate
								SET_PLAYER_IN_ARCADE_CABINET_LOCATE(FALSE, -1, -1)	
								SET_PLAYER_IN_ARCADE_CABINET_LOCATE_TYPE()
								CLEAR_TRIGGER_HELP()
								CLEAR_ARCADE_CABINET_BLOCK_HELP(tlBlockReason, sBlockReasonStringTwo)
							ENDIF	
						ENDIF
					ENDIF
				ENDIF
			ENDFOR							
		ENDIF	
	ENDIF
ENDPROC

/// PURPOSE:
///    Check if owner is using arcade managment option to modify the cabinets 
FUNC BOOL IS_OWNER_MODIFYING_THIS_ARECADE_CABINET_SLOT(INT iDisplayLocatePlayerIn)
	IF IS_BIT_SET(serverBD.iServerBS, ARCADE_CABINET_MANAGER_SERVER_BD_MANAGEMENT_MENU_IN_USE)
		ARCADE_CAB_MANAGER_SLOT cabinetSlotPlayerIsIn = INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, iDisplayLocatePlayerIn)
		INT iCabinetNeighbour = ENUM_TO_INT(GET_ARCADE_CABINET_NEIGHBOUR_SLOT(cabinetSlotPlayerIsIn))
		INT iStartLoop, iEndLoop, iLoop
		IF iDisplayLocatePlayerIn > -1
			IF GET_NUM_SURROUNDED_CABINETS(cabinetSlotPlayerIsIn) = 3
				GET_FOUR_PLAYER_ARCADE_CABINET_START_AND_END_SLOT(cabinetSlotPlayerIsIn, iStartLoop, iEndLoop)
				FOR iLoop = iStartLoop TO iEndLoop
					IF serverBD.eServerArcadeCabinetManagementMenuType = INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, iLoop)
						PRINTLN("[AM_ARC_CAB] - IS_OWNER_MODIFYING_THIS_ARECADE_CABINET_SLOT - some one using street crime cabinet slot: ", iLoop)
						RETURN TRUE
					ENDIF
				ENDFOR	
			ELIF GET_NUM_SURROUNDED_CABINETS(cabinetSlotPlayerIsIn) = 1
				IF iCabinetNeighbour > iDisplayLocatePlayerIn
					iStartLoop = iDisplayLocatePlayerIn
					iEndLoop = iCabinetNeighbour
				ELSE
					iStartLoop = iCabinetNeighbour
					iEndLoop = iDisplayLocatePlayerIn
				ENDIF

				FOR iLoop = iStartLoop TO iEndLoop
					IF sACMData.eArcadeCab[iLoop] != ARCADE_CABINET_INVALID
						IF serverBD.eServerArcadeCabinetManagementMenuType = INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, iLoop)
							PRINTLN("[AM_ARC_CAB] - DOES_ANY_PLAYER_USING_ARCADE_CABINET_IN_CURRENT_SLOT - some one using 2 slot machine: ", iLoop)
							RETURN TRUE
						ENDIF
					ENDIF	
				ENDFOR
			ELSE
				IF sACMData.eArcadeCab[iDisplayLocatePlayerIn] != ARCADE_CABINET_INVALID
					IF serverBD.eServerArcadeCabinetManagementMenuType = cabinetSlotPlayerIsIn
						PRINTLN("[AM_ARC_CAB] - DOES_ANY_PLAYER_USING_ARCADE_CABINET_IN_CURRENT_SLOT - some one using this slot iDisplayLocatePlayerIn: ", iDisplayLocatePlayerIn)
						RETURN TRUE
					ENDIF
				ENDIF	
			ENDIF
		ENDIF	
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Move players out of way if owner is 
PROC MAINTAIN_MOVE_PLAYER_OUT_OF_WAY()
	IF IS_BIT_SET(serverBD.iServerBS, ARCADE_CABINET_MANAGER_SERVER_BD_MANAGEMENT_MENU_IN_USE)
		IF !IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, ARCADE_CABINET_MANAGER_PLAYER_BD_MOVE_OUT_OF_WAY)
			INT iStartLoop, iEndLoop, iLoop
			VECTOR vCoord
			FLOAT fHeading
			
			INT iCabinetNeighbour = ENUM_TO_INT(GET_ARCADE_CABINET_NEIGHBOUR_SLOT(serverBD.eServerArcadeCabinetManagementMenuType))
			
			IF GET_NUM_SURROUNDED_CABINETS(serverBD.eServerArcadeCabinetManagementMenuType) = 3
				GET_FOUR_PLAYER_ARCADE_CABINET_START_AND_END_SLOT(serverBD.eServerArcadeCabinetManagementMenuType, iStartLoop, iEndLoop)
				FOR iLoop = iStartLoop TO iEndLoop
					GET_ARCADE_CABINET_DISPLAY_SLOT_COORD_AND_HEADING(sACMData.eArcadeCabinetPropertyType, INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, iLoop), ARCADE_CABINET_INVALID, vCoord, fHeading)
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vCoord) < 1.5
					AND NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, ARCADE_CABINET_MANAGER_PLAYER_BD_MOVE_OUT_OF_WAY)
						VECTOR vTargetCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoord, fHeading, <<0, -1.5, 0>>)
						IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
							SET_PLAYER_CONFIG_FLAG_KINEMATIC_MODE_WHEN_STATIONARY(FALSE)
						ENDIF	
						TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), vTargetCoords, PEDMOVEBLENDRATIO_WALK, 7000, DEFAULT, 0.01)
						SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, ARCADE_CABINET_MANAGER_PLAYER_BD_MOVE_OUT_OF_WAY)
						PRINTLN("[AM_ARC_CAB] - MAINTAIN_MOVE_PLAYER_OUT_OF_WAY - ARCADE_CABINET_MANAGER_PLAYER_BD_MOVE_OUT_OF_WAY case 1 TRUE")
					ENDIF
				ENDFOR	
			ELIF GET_NUM_SURROUNDED_CABINETS(serverBD.eServerArcadeCabinetManagementMenuType) = 1
				IF iCabinetNeighbour > ENUM_TO_INT(serverBD.eServerArcadeCabinetManagementMenuType)
					iStartLoop = ENUM_TO_INT(serverBD.eServerArcadeCabinetManagementMenuType)
					iEndLoop = iCabinetNeighbour
				ELSE
					iStartLoop = iCabinetNeighbour
					iEndLoop = ENUM_TO_INT(serverBD.eServerArcadeCabinetManagementMenuType)
				ENDIF
				FOR iLoop = iStartLoop TO iEndLoop
					// get the coord for bigest prop ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK
					GET_ARCADE_CABINET_DISPLAY_SLOT_COORD_AND_HEADING(sACMData.eArcadeCabinetPropertyType, INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, iLoop), ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK, vCoord, fHeading)
					
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vCoord) < 1.5
					AND NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, ARCADE_CABINET_MANAGER_PLAYER_BD_MOVE_OUT_OF_WAY)
						VECTOR vTargetCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoord, fHeading, <<0, -1.5, 0>>)
						IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
							SET_PLAYER_CONFIG_FLAG_KINEMATIC_MODE_WHEN_STATIONARY(FALSE)
						ENDIF	
						TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), vTargetCoords, PEDMOVEBLENDRATIO_WALK, 7000, DEFAULT, 0.01)
						SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, ARCADE_CABINET_MANAGER_PLAYER_BD_MOVE_OUT_OF_WAY)
						PRINTLN("[AM_ARC_CAB] - MAINTAIN_MOVE_PLAYER_OUT_OF_WAY - ARCADE_CABINET_MANAGER_PLAYER_BD_MOVE_OUT_OF_WAY case 2 TRUE")
					ENDIF
				ENDFOR
			ELSE	
				GET_ARCADE_CABINET_DISPLAY_SLOT_COORD_AND_HEADING(sACMData.eArcadeCabinetPropertyType, serverBD.eServerArcadeCabinetManagementMenuType, ARCADE_CABINET_INVALID, vCoord, fHeading)
				
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vCoord) < 1.5
					VECTOR vTargetCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoord, fHeading, <<0, -1.5, 0>>)
					IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
						SET_PLAYER_CONFIG_FLAG_KINEMATIC_MODE_WHEN_STATIONARY(FALSE)
					ENDIF	
					TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), vTargetCoords, PEDMOVEBLENDRATIO_WALK, 7000, DEFAULT, 0.01)
					SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, ARCADE_CABINET_MANAGER_PLAYER_BD_MOVE_OUT_OF_WAY)
					PRINTLN("[AM_ARC_CAB] - MAINTAIN_MOVE_PLAYER_OUT_OF_WAY - ARCADE_CABINET_MANAGER_PLAYER_BD_MOVE_OUT_OF_WAY case 3 TRUE")
				ENDIF
			ENDIF	
		ELSE	
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> PERFORMING_TASK
			AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> WAITING_TO_START_TASK
				IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
					SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_UseKinematicModeWhenStationary, TRUE)
				ENDIF
				CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, ARCADE_CABINET_MANAGER_PLAYER_BD_MOVE_OUT_OF_WAY)
				PRINTLN("[AM_ARC_CAB] - MAINTAIN_MOVE_PLAYER_OUT_OF_WAY - ARCADE_CABINET_MANAGER_PLAYER_BD_MOVE_OUT_OF_WAY task is done FALSE")
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, ARCADE_CABINET_MANAGER_PLAYER_BD_MOVE_OUT_OF_WAY)
			CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, ARCADE_CABINET_MANAGER_PLAYER_BD_MOVE_OUT_OF_WAY)
			PRINTLN("[AM_ARC_CAB] - MAINTAIN_MOVE_PLAYER_OUT_OF_WAY - ARCADE_CABINET_MANAGER_PLAYER_BD_MOVE_OUT_OF_WAY owner no longer usign menu FALSE")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Blocks the trigger help text if an arcade cabinet block condition is met.
/// PARAMS:
///    eCabinet - Cabinet to check.
/// RETURNS: TRUE if a block condition is met, FALSE otherwise.
FUNC BOOL BLOCK_ARCADE_CABINET_TRIGGER_HELP(ARCADE_CABINETS eCabinet)

	UNUSED_PARAMETER(eCabinet)

	#IF FEATURE_SUMMER_2020
	SWITCH eCabinet
		CASE ARCADE_CABINET_SUM_STRENGTH_TEST
			RETURN IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARD_CAB_AOF_TROP1")
		BREAK
	ENDSWITCH
	#ENDIF
	
	RETURN FALSE
ENDFUNC

//// PURPOSE:
 ///    Display trigger help text
PROC MAINTAIN_ACM_TRIGGER_HELP()
	TEXT_LABEL_63 tlBlockReason, sBlockReasonStringTwo
	
	IF IS_SAFE_TO_START_ARCADE_GAME(tlBlockReason, sBlockReasonStringTwo)
		IF !IS_STRING_NULL_OR_EMPTY(sACMData.strTriggerHelp)
			IF !IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sACMData.strTriggerHelp)
			AND !BLOCK_ARCADE_CABINET_TRIGGER_HELP(GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID()))
				PRINT_HELP_FOREVER(sACMData.strTriggerHelp)	
			ENDIF	
		ENDIF
	ENDIF	
ENDPROC

PROC INITALISE_ENTRY_ANIMS()
	IF !IS_STRING_NULL_OR_EMPTY(g_ArcadeCabinetManagerData.strArcCabinetAnimDic)
		REQUEST_ANIM_DICT(g_ArcadeCabinetManagerData.strArcCabinetAnimDic)
		PRINTLN("[AM_ARC_CAB] - INITALISE_ENTRY_ANIMS REQUEST_ANIM_DICT: ", g_ArcadeCabinetManagerData.strArcCabinetAnimDic)
	ENDIF	
ENDPROC

PROC TRIGGER_CABINET_DIALOGUE(ARCADE_CABINETS eCabinet, TEXT_LABEL_63 tlContext, BOOL bOverNetwork, BOOL bUseConvo)
	ARCADE_CABINET_AI_CONTEXT_STRUCT eAIContext
	INT iDisplaySlot = GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID())
	eAIContext.eArcadeCabinet = eCabinet
	eAIContext.sCabinetSlot = INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, iDisplaySlot)
	eAIContext.stContext = tlContext
	eAIContext.bUseConvo = bUseConvo
	eAIContext.spParams = SPEECH_PARAMS_FORCE
	PLAY_ARCADE_CABINET_AI_CONTEXT(eAIContext, bOverNetwork)
ENDPROC

PROC PLAY_CABINET_INTRO_AUDIO(ARCADE_CABINETS eCabinet)
	TEXT_LABEL_63 tlApproachSpeech
	SWITCH eCabinet
		CASE ARCADE_CABINET_CH_FORTUNE_TELLER
			tlApproachSpeech = "HS3MN_NEW"
			TRIGGER_CABINET_DIALOGUE(ARCADE_CABINET_CH_FORTUNE_TELLER, tlApproachSpeech, TRUE, TRUE)
		BREAK
		#IF FEATURE_SUMMER_2020
		CASE ARCADE_CABINET_SUM_STRENGTH_TEST
			tlApproachSpeech = "STRENGTH_START_PLAY"
			TRIGGER_CABINET_DIALOGUE(ARCADE_CABINET_SUM_STRENGTH_TEST, tlApproachSpeech, TRUE, FALSE)
		BREAK
		#ENDIF
	ENDSWITCH
ENDPROC

FUNC BOOL SHOULD_BLOCK_ARCADE_CABINETS_AUDIO_SCENES()
	IF IS_LOCAL_PLAYER_USING_DRONE()
		RETURN TRUE
	ENDIF	
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if game has audio scene and starts one
PROC START_GAME_AUDIO_SCENE()
	ARCADE_CABINETS eCabinetLocatePlayerIsIn = GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID())
	IF eCabinetLocatePlayerIsIn != ARCADE_CABINET_INVALID
		GET_ARCADE_CABINET_DETAILS(eCabinetLocatePlayerIsIn, sACMData)
		IF NOT IS_STRING_NULL_OR_EMPTY(sACMData.tlAudioSceneName)
			IF !IS_AUDIO_SCENE_ACTIVE(sACMData.tlAudioSceneName)
				START_AUDIO_SCENE(sACMData.tlAudioSceneName)
				PRINTLN("[AM_ARC_CAB] - START_AUDIO_SCENE ", sACMData.tlAudioSceneName)
			ENDIF	
		ENDIF
	ENDIF	
ENDPROC

FUNC INT GET_ARCADE_CABINET_ANIMATION_VARIATION(ARCADE_CABINETS eCabinetLocatePlayerIsIn, INT iDisplaySlot, INT iPlayerIndex)
	IF GET_NUM_PLAYERS_ARCADE_CABINET_CAN_HAVE(eCabinetLocatePlayerIsIn) > ENUM_TO_INT(ARCADE_CAB_PLAYER_1)
		RETURN iPlayerIndex
	ELIF IS_THIS_PHYSICAL_ARCADE_CABINET(eCabinetLocatePlayerIsIn)
	AND GET_NUM_PLAYERS_ARCADE_CABINET_CAN_HAVE(eCabinetLocatePlayerIsIn) = ENUM_TO_INT(ARCADE_CAB_PLAYER_1)
		RETURN 0
	ELIF IS_THIS_FOUR_PLAYERS_ARCADE_CABINET(eCabinetLocatePlayerIsIn)	
		SWITCH GET_ARCADE_CABINET_SAVE_SLOT_TYPE(PLAYER_ID(), INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, iDisplaySlot), sACMData.eArcadeCabinetPropertyType)
			CASE ARCADE_CABINET_SAVE_SLOT_CA_STREET_CRIMES_PROP_B			RETURN 1
			CASE ARCADE_CABINET_SAVE_SLOT_CA_STREET_CRIMES_PROP_C			RETURN 2
			CASE ARCADE_CABINET_SAVE_SLOT_CA_STREET_CRIMES_PROP_D			RETURN 3
			DEFAULT 														RETURN 0
		ENDSWITCH
	ELSE
		SWITCH eCabinetLocatePlayerIsIn
			CASE ARCADE_CABINET_CH_DEFENDER_OF_THE_FAITH
			CASE ARCADE_CABINET_CH_MONKEYS_PARADISE	
			CASE ARCADE_CABINET_CH_PENETRATOR	
			CASE ARCADE_CABINET_CH_GG_SPACE_MONKEY
			CASE ARCADE_CABINET_CH_WIZARDS_SLEVE	
			#IF FEATURE_SUMMER_2020
			CASE ARCADE_CABINET_SUM_QUB3D
			#ENDIF
				RETURN GET_RANDOM_INT_IN_RANGE(0, 2)
		ENDSWITCH	
	ENDIF
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Task player to go to anim's first frame coords
PROC MAINTAIN_ACM_TASK_GO_TO_INITAL_COORDS()
	IF !IS_STRING_NULL_OR_EMPTY(g_ArcadeCabinetManagerData.strArcCabinetAnimDic)
		REQUEST_ANIM_DICT(g_ArcadeCabinetManagerData.strArcCabinetAnimDic)
		IF HAS_ANIM_DICT_LOADED(g_ArcadeCabinetManagerData.strArcCabinetAnimDic)
			INT iDisplaySlot =  GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID())
			INT iPlayerIndex = GET_ARCADE_CABINET_LOCATE_PLAYER_SLOT_PLAYER_IS_IN(PLAYER_ID())
			IF iDisplaySlot != -1
				IF IS_ARCADE_CABINET_MENU_BS(sACMData, ARCADE_CABINET_MANAGER_MENU_BS_EXIT_MENU)
					CLEANUP_MENU_ASSETS()
					CLEAR_ARCADE_CABINET_MENU_BS(sACMData, ARCADE_CABINET_MANAGER_MENU_BS_EXIT_MENU)
				ENDIF
				
				ARCADE_CABINETS eCabinetLocatePlayerIsIn = GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID())
				
				VECTOR vLocateOffset[ARCADE_CAB_MAX_NUM_PLAYERS], vLocateCoord, vPropCoords
				FLOAT fHeading
				
				IF DOES_THIS_ARCADE_CABINETS_PROP_INCLUDED_IN_INTERIOR(eCabinetLocatePlayerIsIn)
					GET_ARCADE_CABINET_DISPLAY_SLOT_COORD_AND_HEADING(sACMData.eArcadeCabinetPropertyType, INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, iDisplaySlot), eCabinetLocatePlayerIsIn
																	, sACMData.vArcadeCabCoords[iDisplaySlot], sACMData.fArcadeCabHeading[iDisplaySlot])
				ENDIF
				
				// Check if we should use further enter animation
				IF DOES_THIS_ARCADE_CABINET_HAVE_TWO_ENTER_ANIM_CLIP(eCabinetLocatePlayerIsIn)
					IF iPlayerIndex != -1
						GET_ARCADE_CABINET_TRIGGER_LOCATE_DATA(eCabinetLocatePlayerIsIn, vLocateOffset)
						vLocateOffset[iPlayerIndex] = <<vLocateOffset[iPlayerIndex].x, vLocateOffset[iPlayerIndex].y * 2, vLocateOffset[iPlayerIndex].z>>
						
						GET_ARCADE_CABINET_DISPLAY_SLOT_COORD_AND_HEADING(sACMData.eArcadeCabinetPropertyType, INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, iDisplaySlot), eCabinetLocatePlayerIsIn, vPropCoords, fHeading)
						vLocateCoord = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPropCoords, fHeading, vLocateOffset[iPlayerIndex])
						
						IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vLocateCoord, <<ARCADE_CABINET_LOC_DIMENSIONS_X/2, ARCADE_CABINET_LOC_DIMENSIONS_Y, ARCADE_CABINET_LOC_DIMENSIONS_Z>>, FALSE, TRUE, TM_ON_FOOT)
							SET_ARCADE_CABINET_MANAGER_LOCAL_BIT(ARCADE_CABINET_MANAGER_LOCAL_BS_USE_ENTER_B)
						ENDIF
					ENDIF
				ENDIF
				
				ARCADE_CABINET_ANIM_DETAIL eAnimDetails
				ARCADE_CABINET_ANIM_CLIPS eClip = ARCADE_CABINET_ANIM_CLIP_ENTRY
				
				IF IS_LOCAL_ARCADE_CABINET_MANAGER_BIT_SET(ARCADE_CABINET_MANAGER_LOCAL_BS_USE_ENTER_B)
					eClip = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_ENTER_B
				ENDIF	
				
				GET_ARCADE_CABINET_ANIMATION_DETAIL(eCabinetLocatePlayerIsIn, eClip, g_ArcadeCabinetManagerData.iAnimVariation[iDisplaySlot], eAnimDetails)
				g_ArcadeCabinetManagerData.strArcCabinetAnimDic = eAnimDetails.strArcCabinetAnimDic
				
				FLOAT fTargetRadius	 = 0.1
				VECTOR vGotoPos		 = GET_ANIM_INITIAL_OFFSET_POSITION(g_ArcadeCabinetManagerData.strArcCabinetAnimDic, eAnimDetails.sAnimClipName, sACMData.vArcadeCabCoords[iDisplaySlot], <<0, 0, sACMData.fArcadeCabHeading[iDisplaySlot]>>)
				VECTOR vOffset		 = GET_ARCADE_CABINET_ANIMATION_OFFSET(eCabinetLocatePlayerIsIn, eClip, iPlayerIndex, FALSE)
				VECTOR vGotoRot 	 = GET_ANIM_INITIAL_OFFSET_ROTATION(g_ArcadeCabinetManagerData.strArcCabinetAnimDic, eAnimDetails.sAnimClipName, sACMData.vArcadeCabCoords[iDisplaySlot], <<0, 0, sACMData.fArcadeCabHeading[iDisplaySlot]>>)
				VECTOR vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
				vGotoPos.z = vPlayerCoords.z
				
				IF !IS_VECTOR_ZERO(vOffset)
					vGotoPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vGotoPos, sACMData.fArcadeCabHeading[iDisplaySlot], vOffset)
				ENDIF
				
				#IF FEATURE_SUMMER_2020
				IF eCabinetLocatePlayerIsIn = ARCADE_CABINET_SUM_STRENGTH_TEST
					fTargetRadius = 0.01
				ENDIF
				#ENDIF
				
				IF !IS_VECTOR_ZERO(vGotoPos)
					IF !ARE_VECTORS_ALMOST_EQUAL(vPlayerCoords, vGotoPos, 0.01)
						TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), vGotoPos, PEDMOVEBLENDRATIO_WALK, 4000, vGotoRot.z, fTargetRadius)
						PRINTLN("[AM_ARC_CAB] - MAINTAIN_ACM_TASK_GO_TO_INITAL_COORDS TASK_GO_STRAIGHT_TO_COORD vGotoPos: ", vGotoPos)
					ELSE
						TASK_ACHIEVE_HEADING(PLAYER_PED_ID(), vGotoRot.z)
						PRINTLN("[AM_ARC_CAB] - MAINTAIN_ACM_TASK_GO_TO_INITAL_COORDS TASK_ACHIEVE_HEADING vGotoRot.z: ", vGotoRot.z)
					ENDIF	
				ELSE
					PRINTLN("[AM_ARC_CAB] - MAINTAIN_ACM_TASK_GO_TO_INITAL_COORDS vGotoPos = 0")
				ENDIF
				
				#IF FEATURE_SUMMER_2020
				IF eCabinetLocatePlayerIsIn = ARCADE_CABINET_SUM_QUB3D
					IF Is_Ped_Drunk(PLAYER_PED_ID())
						PRINTLN("[AM_ARC_CAB] - MAINTAIN_ACM_TASK_GO_TO_INITAL_COORDS - Sober player up.")
						Quit_Drunk_Camera_Gradually(1000)
						Make_Ped_Sober(PLAYER_PED_ID())
					ENDIF
				ENDIF
				#ENDIF
				
				IF sACMData.eArcadeCab[iDisplaySlot] = ARCADE_CABINET_CH_DRILLING_LASER_MINI_G
					START_GAME_AUDIO_SCENE()
					SET_PLAYER_READY_TO_PLAY_ARCADE_GAME(TRUE)
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_PREVENT_VISIBILITY_CHANGES | NSPC_LEAVE_CAMERA_CONTROL_ON)
					SET_PLAYER_CONFIG_FLAG_KINEMATIC_MODE_WHEN_STATIONARY(TRUE)
					SET_ARCADE_CABINET_MANAGER_STATE(ARCADE_CABINET_IN_USE_ANIM_STATE)
				ELSE	
					SET_ARCADE_CABINET_MANAGER_STATE(ARCADE_CABINET_ENTRY_ANIM_STATE)
				ENDIF	
			ELSE
				PRINTLN("[AM_ARC_CAB] - MAINTAIN_ACM_TASK_GO_TO_INITAL_COORDS iDisplaySlot = -1")
			ENDIF
		ELSE
			PRINTLN("[AM_ARC_CAB] - MAINTAIN_ACM_TASK_GO_TO_INITAL_COORDS anim dic: ", g_ArcadeCabinetManagerData.strArcCabinetAnimDic, " not loaded yet")
		ENDIF
	ELSE
		PRINTLN("[AM_ARC_CAB] - MAINTAIN_ACM_TASK_GO_TO_INITAL_COORDS anim dic NULL")
	ENDIF	
ENDPROC

/// PURPOSE:
///    If cabinet is free and player control is pressed, player will send a request to server. If server authorise the usage, player will do entry animation and gets ready to play a game
PROC MAINTAIN_ACM_TRIGGER_GAME()
	TEXT_LABEL_63 tlBlockReason
	TEXT_LABEL_63 sBlockReasonStringTwo
	
	ARCADE_CABINETS eCabinetLocatePlayerIsIn = GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID())
	
	IF !IS_STRING_NULL_OR_EMPTY(sACMData.strTriggerHelp)
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sACMData.strTriggerHelp)
		OR BLOCK_ARCADE_CABINET_TRIGGER_HELP(eCabinetLocatePlayerIsIn)
			IF IS_SAFE_TO_START_ARCADE_GAME(tlBlockReason, sBlockReasonStringTwo)
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
					SET_PLAYER_REQUESTING_TO_USE_CABINET(TRUE)
				ENDIF
				IF DOES_THIS_ARCADE_CABINET_HAS_GLITCH_MODE(eCabinetLocatePlayerIsIn)
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
						SET_ARCADE_CABINET_GLITCHED(TRUE)
						SET_PLAYER_REQUESTING_TO_USE_CABINET(TRUE)
					ENDIF
				ENDIF	
			ELSE
				CLEAR_HELP()
			ENDIF		
		ENDIF
	ENDIF
	
	IF IS_SAFE_TO_START_ARCADE_GAME(tlBlockReason, sBlockReasonStringTwo, TRUE)
		IF GET_PLAYER_INDEX_USING_THIS_ARCADE_CABINET_DISPLAY_SLOT(serverBD, GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID()), GET_ARCADE_CABINET_LOCATE_PLAYER_SLOT_PLAYER_IS_IN(PLAYER_ID()), sACMData.eArcadeCabinetPropertyType) = NATIVE_TO_INT(PLAYER_ID())
			CLEAR_TRIGGER_HELP()
			
			INT iDisplaySlot = GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID())
			IF iDisplaySlot != -1 
				IF DOES_THIS_ARCADE_CABINET_HAVE_HINT_CAMERA(eCabinetLocatePlayerIsIn)
					IF DOES_ENTITY_EXIST(sACMData.arcadeCabinetProp[iDisplaySlot])
						SET_GAMEPLAY_ENTITY_HINT(sACMData.arcadeCabinetProp[iDisplaySlot], <<-1, 2.5, 1.0>>, DEFAULT, 10000)
					ENDIF
				ENDIF	
				
				IF SHOULD_ALLOW_CAMERA_CONTROLS_ON_ARCADE_CABINET_TRIGGER(eCabinetLocatePlayerIsIn)
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_PREVENT_VISIBILITY_CHANGES | NSPC_LEAVE_CAMERA_CONTROL_ON)
				ELSE
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_PREVENT_VISIBILITY_CHANGES)
				ENDIF
				
				SET_PLAYER_USING_ARCADE_CABINET(TRUE)
				SET_PLAYER_REQUESTING_TO_USE_CABINET(FALSE)
				
				IF DOES_ARCADE_CABINET_HAVE_STARTUP_MENU(eCabinetLocatePlayerIsIn)
					SET_ARCADE_CABINET_MENU_BS(sACMData, ARCADE_CABINET_MANAGER_MENU_BS_BUILD_MENU)
					SET_ARCADE_CABINET_MANAGER_STATE(ARCADE_CABINET_DRAW_STARTUP_MENU_STATE)
				ELSE
					// Get Animation dic name
					g_ArcadeCabinetManagerData.iAnimVariation[iDisplaySlot] = GET_ARCADE_CABINET_ANIMATION_VARIATION(eCabinetLocatePlayerIsIn, iDisplaySlot, GET_ARCADE_CABINET_LOCATE_PLAYER_SLOT_PLAYER_IS_IN(PLAYER_ID()))
					
					ARCADE_CABINET_ANIM_DETAIL eAnimDetails
					GET_ARCADE_CABINET_ANIMATION_DETAIL(eCabinetLocatePlayerIsIn, ARCADE_CABINET_ANIM_CLIP_INVALID, g_ArcadeCabinetManagerData.iAnimVariation[iDisplaySlot], eAnimDetails)
					g_ArcadeCabinetManagerData.strArcCabinetAnimDic = eAnimDetails.strArcCabinetAnimDic
					PRINTLN("[AM_ARC_CAB] - MAINTAIN_ACM_TRIGGER_GAME anim dic: ", g_ArcadeCabinetManagerData.strArcCabinetAnimDic, " anim variation: ", g_ArcadeCabinetManagerData.iAnimVariation[iDisplaySlot])
					
					INITALISE_ENTRY_ANIMS()
					PLAY_CABINET_INTRO_AUDIO(eCabinetLocatePlayerIsIn)
					
					IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner != PLAYER_ID()
						IF !IS_ARCADE_CABINET_FREE_TO_USE(eCabinetLocatePlayerIsIn)
							SET_ARCADE_CABINET_MANAGER_STATE(ARCADE_CABINET_TAKE_MONEY_STATE)
						ELSE
							SET_ARCADE_CABINET_MANAGER_STATE(ARCADE_CABINET_GO_TO_INITAL_COORDS_STATE)
						ENDIF	
					ELSE
						SET_ARCADE_CABINET_MANAGER_STATE(ARCADE_CABINET_GO_TO_INITAL_COORDS_STATE)
					ENDIF
				ENDIF	
			ELSE
				PRINTLN("[AM_ARC_CAB] - MAINTAIN_ACM_TRIGGER_GAME iDisplaySlot = -1")
			ENDIF
		ENDIF
	ELSE
		SET_PLAYER_REQUESTING_TO_USE_CABINET(FALSE)
		SET_ARCADE_CABINET_GLITCHED(FALSE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintain arcade cabinet manager idle state
PROC MAINTAIN_ACM_IDLE_STATE()
	MAINTAIN_ARCADE_CABINET_LOCATE()
	MAINTAIN_ACM_TRIGGER_HELP()
	MAINTAIN_ACM_TRIGGER_GAME()
	MAINTAIN_MOVE_PLAYER_OUT_OF_WAY()
ENDPROC

PROC ATTACH_PROP_TO_PLAYER_FOR_ARCADE_CABINET_ANIM_CLIP_START(ENTITY_INDEX propToAttach, ARCADE_CABINETS eCabinetLocatePlayerIsIn , ARCADE_CABINET_ANIM_CLIPS eClip)
	UNUSED_PARAMETER(eClip)
	
	SWITCH eCabinetLocatePlayerIsIn
		CASE ARCADE_CABINET_CH_HACKING_MINI_G
			MODEL_NAMES propModel
			propModel = GET_ENTITY_MODEL(propToAttach)
			IF propModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_usb_drive01x"))
				//ATTACH_ENTITY_TO_ENTITY(propToAttach, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_L_HAND), <<0,0,0>>, <<0,0,0>>)
			ELSE
				//ATTACH_ENTITY_TO_ENTITY(propToAttach, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC MAINTAIN_ARCADE_CABINET_SUB_PROP_ANIMATION_ATTACHMENT()
	IF IS_PLAYER_USING_ARCADE_CABINET(PLAYER_ID())
		INT iDisplaySlot = GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID())
		ARCADE_CAB_NUM_PLAYERS playerIndex = INT_TO_ENUM(ARCADE_CAB_NUM_PLAYERS, GET_ARCADE_CABINET_LOCATE_PLAYER_SLOT_PLAYER_IS_IN(PLAYER_ID()))
		ARCADE_CABINETS eCabinetLocatePlayerIsIn = GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID())
		SWITCH eCabinetLocatePlayerIsIn
			CASE ARCADE_CABINET_CH_LAST_GUNSLINGERS
			#IF FEATURE_TUNER
			CASE ARCADE_CABINET_SE_CAMHEDZ
			#ENDIF
				IF IS_ARCADE_CABINET_SYNC_SCENE_PHASE_PASSED(ARCADE_CABINET_ANIM_CLIP_ENTRY, GET_ARCADE_CABINET_SUB_PROP_ATTACHMENT_PHASE(eCabinetLocatePlayerIsIn, ARCADE_CABINET_ANIM_CLIP_ENTRY, playerIndex, TRUE))	
					IF DOES_ENTITY_EXIST(sACMData.arcadeCabinetSubObj[iDisplaySlot][playerIndex])
						IF NOT IS_ENTITY_ATTACHED(sACMData.arcadeCabinetSubObj[iDisplaySlot][playerIndex])
							IF TAKE_CONTROL_OF_ENTITY(sACMData.arcadeCabinetSubObj[iDisplaySlot][playerIndex])
								ATTACH_ENTITY_TO_ENTITY(sACMData.arcadeCabinetSubObj[iDisplaySlot][playerIndex], PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>)
							ENDIF
						ENDIF	
					ENDIF	
				ENDIF	
				IF IS_ARCADE_CABINET_SYNC_SCENE_PHASE_PASSED(ARCADE_CABINET_ANIM_CLIP_LAST_GUNSLINGERS_EXIT_FROM_AIM, GET_ARCADE_CABINET_SUB_PROP_ATTACHMENT_PHASE(eCabinetLocatePlayerIsIn, ARCADE_CABINET_ANIM_CLIP_LAST_GUNSLINGERS_EXIT_FROM_AIM, playerIndex, FALSE))	
				OR IS_ARCADE_CABINET_SYNC_SCENE_PHASE_PASSED(ARCADE_CABINET_ANIM_CLIP_EXIT, GET_ARCADE_CABINET_SUB_PROP_ATTACHMENT_PHASE(eCabinetLocatePlayerIsIn, ARCADE_CABINET_ANIM_CLIP_EXIT, playerIndex, FALSE))	
					IF DOES_ENTITY_EXIST(sACMData.arcadeCabinetSubObj[iDisplaySlot][playerIndex])
						IF IS_ENTITY_ATTACHED(sACMData.arcadeCabinetSubObj[iDisplaySlot][playerIndex])
							IF TAKE_CONTROL_OF_ENTITY(sACMData.arcadeCabinetSubObj[iDisplaySlot][playerIndex])
								DETACH_ENTITY(sACMData.arcadeCabinetSubObj[iDisplaySlot][playerIndex], FALSE)
							ENDIF
						ENDIF	
					ENDIF	
				ENDIF	
			BREAK
		ENDSWITCH
	ENDIF	
ENDPROC

/// PURPOSE:
///    start arcade cabinet synced scene 
/// PARAMS:
///    sAnimClip - anim clip to play
///    bHoldLastFrame - hold animation on last frame
///    bLoopAnim - loop animation
PROC START_ARCADE_CABINET_SYNC_SCENE(ARCADE_CABINET_ANIM_CLIPS eClip, BOOL bHoldLastFrame = FALSE, BOOL bLoopAnim = FALSE, FLOAT blendInDelta = REALLY_SLOW_BLEND_IN, FLOAT blendOutDelta = REALLY_SLOW_BLEND_OUT
									, BOOL bFacialAnim = FALSE, ENTITY_INDEX enToPlayAnim = NULL)
	
	INT iDisplaySlot =  GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID())
	INT iPlayerIndex = GET_ARCADE_CABINET_LOCATE_PLAYER_SLOT_PLAYER_IS_IN(PLAYER_ID())
	ARCADE_CABINETS eCabinetLocatePlayerIsIn = GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID())
	ARCADE_CABINET_ANIM_DETAIL eAnimDetails
	
	IF iDisplaySlot > -1
		GET_ARCADE_CABINET_ANIMATION_DETAIL(eCabinetLocatePlayerIsIn, eClip, g_ArcadeCabinetManagerData.iAnimVariation[iDisplaySlot], eAnimDetails)
		PRINTLN("[AM_ARC_CAB] - START_ARCADE_CABINET_SYNC_SCENE anim dic: ", g_ArcadeCabinetManagerData.strArcCabinetAnimDic, " anim variation: ", g_ArcadeCabinetManagerData.iAnimVariation[iDisplaySlot])
	ELSE
		PRINTLN("[AM_ARC_CAB] - START_ARCADE_CABINET_SYNC_SCENE iDisplaySlot = -1")
	ENDIF
	
	IF SHOULD_STOP_ARCADE_CABINET_SYNCED_SCENE_BEFORE_STARTING_NEW_CLIP(eClip)
		STOP_CURRENT_ARCADE_CABINET_SYNCHED_SCENE()
	ENDIF
	
	VECTOR vAnimCoords = sACMData.vArcadeCabCoords[iDisplaySlot]
	VECTOR vOffset = GET_ARCADE_CABINET_ANIMATION_OFFSET(eCabinetLocatePlayerIsIn, eClip, iPlayerIndex, FALSE)

	IF !IS_VECTOR_ZERO(vOffset)
		vAnimCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sACMData.arcadeCabinetProp[iDisplaySlot], vOffset)
	ENDIF
	
	IF !IS_STRING_NULL_OR_EMPTY(eAnimDetails.sAnimClipName)
	AND !IS_STRING_NULL_OR_EMPTY(g_ArcadeCabinetManagerData.strArcCabinetAnimDic)
		sACMData.sArcadeCabinetAnimEvent.ePreviousAnimClip = eClip
		
		g_ArcadeCabinetManagerData.iSyncSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(vAnimCoords, <<0, 0, sACMData.fArcadeCabHeading[iDisplaySlot]>>, DEFAULT, bHoldLastFrame, bLoopAnim)
		NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), GET_ARCADE_CABINET_SYNC_SCENE_ID(), g_ArcadeCabinetManagerData.strArcCabinetAnimDic, eAnimDetails.sAnimClipName,
												blendInDelta, blendOutDelta, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
		
		NETWORK_START_SYNCHRONISED_SCENE(GET_ARCADE_CABINET_SYNC_SCENE_ID())
		PRINTLN("[AM_ARC_CAB] - START_ARCADE_CABINET_SYNC_SCENE ", GET_ARCADE_CABINET_SYNC_SCENE_ID(), " \"", eAnimDetails.sAnimClipName, "\", \"", g_ArcadeCabinetManagerData.strArcCabinetAnimDic, "\"")
		
		IF SHOULD_CLEANUP_ARCADE_CABINET_MANAGER_WITH_THIS_ANIM(eClip)		
			SET_ARCADE_CABINET_MANAGER_BIT(iDisplaySlot, ARCADE_CABINET_MANAGER_EXIT_ARCADE_CABINET_ANIM_STARTED)
		ENDIF
		
		IF bFacialAnim
			TEXT_LABEL_63 tl63FaceClipName = ""
			tl63FaceClipName  = eAnimDetails.sAnimClipName
			tl63FaceClipName += "_Facial"
			
			PLAY_FACIAL_ANIM(PLAYER_PED_ID(), tl63FaceClipName, g_ArcadeCabinetManagerData.strArcCabinetAnimDic)
			PRINTLN("[AM_ARC_CAB] - START_ARCADE_CABINET_SYNC_SCENE - Play Facial Anim \"", tl63FaceClipName, "\", \"", g_ArcadeCabinetManagerData.strArcCabinetAnimDic, "\"")
		ENDIF
		
		INT iSubPropIndex
		ACM_SUB_PROP_DETAILS eArcadeCabSubPropDetail
		GET_ARCADE_CABINET_SUB_PROP_DETAIL(eCabinetLocatePlayerIsIn, eArcadeCabSubPropDetail)
		
		IF enToPlayAnim = NULL
			IF eAnimDetails.iNumPropClips > 0
				
				vAnimCoords = sACMData.vArcadeCabCoords[iDisplaySlot]
				vOffset = GET_ARCADE_CABINET_ANIMATION_OFFSET(eCabinetLocatePlayerIsIn, eClip, iPlayerIndex, TRUE)
				IF !IS_VECTOR_ZERO(vOffset)
					vAnimCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sACMData.arcadeCabinetProp[iDisplaySlot], vOffset)
				ENDIF
				
				IF !SHOULD_ONLY_ATTACH_SUB_PROP_IN_ANIMS(eCabinetLocatePlayerIsIn)
					IF DOES_ARCADE_CABINET_HAVE_VALID_SUB_PROP_ANIM_CLIP(eCabinetLocatePlayerIsIn, eAnimDetails)
						g_ArcadeCabinetManagerData.iPropSyncSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(vAnimCoords, <<0, 0, sACMData.fArcadeCabHeading[iDisplaySlot]>>, DEFAULT, bHoldLastFrame, bLoopAnim)
						PRINTLN("[AM_ARC_CAB] - START_ARCADE_CABINET_SYNC_SCENE ", GET_ARCADE_CABINET_SYNC_SCENE_ID(), " \ prop vAnimCoords: ", vAnimCoords)
					ENDIF	
				ENDIF
				
				FOR iSubPropIndex = 0 TO eArcadeCabSubPropDetail.iNumProp - 1
					IF !IS_STRING_NULL_OR_EMPTY(eAnimDetails.sPropAnimClip[iSubPropIndex])
						IF iPlayerIndex = ENUM_TO_INT(ARCADE_CAB_PLAYER_1)	
							IF (GET_NUM_PLAYERS_GENERIC_ARCADE_CAN_HAVE(eCabinetLocatePlayerIsIn) = ENUM_TO_INT(ARCADE_CAB_PLAYER_2) AND iSubPropIndex = 0)
							OR GET_NUM_PLAYERS_GENERIC_ARCADE_CAN_HAVE(eCabinetLocatePlayerIsIn) = ENUM_TO_INT(ARCADE_CAB_PLAYER_1)
								enToPlayAnim = sACMData.arcadeCabinetSubObj[iDisplaySlot][iSubPropIndex]
								IF DOES_ENTITY_EXIST(enToPlayAnim)
									ATTACH_PROP_TO_PLAYER_FOR_ARCADE_CABINET_ANIM_CLIP_START(enToPlayAnim, eCabinetLocatePlayerIsIn, eClip)
									IF !SHOULD_ONLY_ATTACH_SUB_PROP_IN_ANIMS(eCabinetLocatePlayerIsIn)
										NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(enToPlayAnim, GET_ARCADE_CABINET_PROP_SYNC_SCENE_ID(), g_ArcadeCabinetManagerData.strArcCabinetAnimDic, eAnimDetails.sPropAnimClip[iSubPropIndex],
													blendInDelta, blendOutDelta, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
									ENDIF
									PRINTLN("[AM_ARC_CAB] - START_ARCADE_CABINET_SYNC_SCENE - ARCADE_CAB_PLAYER_1 play anim for entity iDisplaySlot: ", iDisplaySlot, " prop: ", iSubPropIndex, " clip: ", eAnimDetails.sPropAnimClip[iSubPropIndex])
								ENDIF	
							ENDIF	
						ELIF iPlayerIndex = ENUM_TO_INT(ARCADE_CAB_PLAYER_2)
						IF iSubPropIndex = 1
								enToPlayAnim = sACMData.arcadeCabinetSubObj[iDisplaySlot][iSubPropIndex]
								IF DOES_ENTITY_EXIST(enToPlayAnim)
									ATTACH_PROP_TO_PLAYER_FOR_ARCADE_CABINET_ANIM_CLIP_START(enToPlayAnim, eCabinetLocatePlayerIsIn, eClip)
									IF !SHOULD_ONLY_ATTACH_SUB_PROP_IN_ANIMS(eCabinetLocatePlayerIsIn)
										NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(enToPlayAnim, GET_ARCADE_CABINET_PROP_SYNC_SCENE_ID(), g_ArcadeCabinetManagerData.strArcCabinetAnimDic, eAnimDetails.sPropAnimClip[iSubPropIndex],
												blendInDelta, blendOutDelta, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)						
									ENDIF
									PRINTLN("[AM_ARC_CAB] - START_ARCADE_CABINET_SYNC_SCENE - ARCADE_CAB_PLAYER_2 play anim for entity iDisplaySlot: ", iDisplaySlot, " prop: ", iSubPropIndex, " clip: ", eAnimDetails.sPropAnimClip[iSubPropIndex])
								ELSE
									PRINTLN("[AM_ARC_CAB] - START_ARCADE_CABINET_SYNC_SCENE - iDisplaySlot: ", iDisplaySlot, " prop: ", iSubPropIndex, " doesn't exist")
								ENDIF
							ENDIF	
						ELSE
							PRINTLN("[AM_ARC_CAB] - START_ARCADE_CABINET_SYNC_SCENE - iPlayerIndex = -1")
						ENDIF
					ELSE
						PRINTLN("[AM_ARC_CAB] - START_ARCADE_CABINET_SYNC_SCENE - prop anim clip is null")
					ENDIF
				ENDFOR	
				
				IF !SHOULD_ONLY_ATTACH_SUB_PROP_IN_ANIMS(eCabinetLocatePlayerIsIn)
					IF DOES_ARCADE_CABINET_HAVE_VALID_SUB_PROP_ANIM_CLIP(eCabinetLocatePlayerIsIn, eAnimDetails)
						NETWORK_START_SYNCHRONISED_SCENE(GET_ARCADE_CABINET_PROP_SYNC_SCENE_ID())
					ENDIF	
				ENDIF	
			ENDIF	
		ELSE
			IF DOES_ENTITY_EXIST(enToPlayAnim)
			AND !IS_STRING_NULL_OR_EMPTY(eAnimDetails.sPropAnimClip[ENUM_TO_INT(ARCADE_CAB_PLAYER_1)])
				ATTACH_PROP_TO_PLAYER_FOR_ARCADE_CABINET_ANIM_CLIP_START(enToPlayAnim, eCabinetLocatePlayerIsIn, eClip)
				
				vAnimCoords = sACMData.vArcadeCabCoords[iDisplaySlot]
				vOffset = GET_ARCADE_CABINET_ANIMATION_OFFSET(eCabinetLocatePlayerIsIn, eClip, iPlayerIndex, TRUE)
				IF !IS_VECTOR_ZERO(vOffset)
					vAnimCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sACMData.arcadeCabinetProp[iDisplaySlot], vOffset)
				ENDIF
				
				IF !SHOULD_ONLY_ATTACH_SUB_PROP_IN_ANIMS(eCabinetLocatePlayerIsIn)
					g_ArcadeCabinetManagerData.iPropSyncSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(vAnimCoords, <<0, 0, sACMData.fArcadeCabHeading[iDisplaySlot]>>, DEFAULT, bHoldLastFrame, bLoopAnim)
					
					NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(enToPlayAnim, GET_ARCADE_CABINET_PROP_SYNC_SCENE_ID(), g_ArcadeCabinetManagerData.strArcCabinetAnimDic, eAnimDetails.sPropAnimClip[ENUM_TO_INT(ARCADE_CAB_PLAYER_1)],
													blendInDelta, blendOutDelta, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
													
					NETWORK_START_SYNCHRONISED_SCENE(GET_ARCADE_CABINET_PROP_SYNC_SCENE_ID())
					
					PRINTLN("[AM_ARC_CAB] - START_ARCADE_CABINET_SYNC_SCENE - entity assigned from event PLAY_ENTITY_ANIM sPropAnimClip: ", eAnimDetails.sPropAnimClip[ENUM_TO_INT(ARCADE_CAB_PLAYER_1)])
				ENDIF	
			ELSE
				PRINTLN("[AM_ARC_CAB] - START_ARCADE_CABINET_SYNC_SCENE - prop anim entity is null " , GET_ARCADE_CABINET_NAME(eCabinetLocatePlayerIsIn) 
						, " sPropAnimClip: ", eAnimDetails.sPropAnimClip[ENUM_TO_INT(ARCADE_CAB_PLAYER_1)])
			ENDIF	
		ENDIF
		
	ELSE
		IF IS_STRING_NULL_OR_EMPTY(g_ArcadeCabinetManagerData.strArcCabinetAnimDic)
			PRINTLN("[AM_ARC_CAB] - START_ARCADE_CABINET_SYNC_SCENE - g_ArcadeCabinetManagerData.strArcCabinetAnimDic is null")
		ELSE
			PRINTLN("[AM_ARC_CAB] - START_ARCADE_CABINET_SYNC_SCENE - sAnimClipName is null")
		ENDIF	
	ENDIF
ENDPROC

PROC BUILD_ARCADE_CABINET_STARTUP_MENU()
	CLEAR_MENU_DATA()
	ARCADE_CABINETS eCabinetLocatePlayerIsIn = GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID())
	IF eCabinetLocatePlayerIsIn != ARCADE_CABINET_INVALID
		TEXT_LABEL_63 txtTitle
		STRING strOptions[ARCADE_CABINET_MAX_STARTUP_MENU_ITEMS]
		STRING strToggleableOptions[ARCADE_CABINET_MAX_STARTUP_MENU_ITEMS]
		GET_ARCADE_CABINET_STARTUP_MENU_DETAIL(eCabinetLocatePlayerIsIn, txtTitle, strOptions, strToggleableOptions, sACMData.sArcadeCabinetMenu)
		
		SET_MENU_TITLE(txtTitle)
		SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
		SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)

		INT i
		FOR i = 0 TO sACMData.sArcadeCabinetMenu.iNumItemInMenu - 1
			ADD_MENU_ITEM_TEXT(i, strOptions[i], DEFAULT, IS_BIT_SET(sACMData.sArcadeCabinetMenu.iMenuAvailable, i))
			
			IF IS_BIT_SET(sACMData.sArcadeCabinetMenu.iMenuToggleable, i)
				ADD_MENU_ITEM_TEXT(i, strToggleableOptions[sACMData.sArcadeCabinetMenu.iNumToggleable[i]], DEFAULT, IS_BIT_SET(sACMData.sArcadeCabinetMenu.iMenuAvailable, i))
				IF IS_BIT_SET(sACMData.sArcadeCabinetMenu.iMenuAvailable, i)
					SET_MENU_ITEM_TOGGLEABLE(FALSE, TRUE)
				ENDIF	
			ENDIF
		ENDFOR
		
		SET_TOP_MENU_ITEM(sACMData.sArcadeCabinetMenu.iTopMenuItem)
		SET_CURRENT_MENU_ITEM(sACMData.sArcadeCabinetMenu.iMenuCurrentItem)
		
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
	ELSE
		PRINTLN("[AM_ARC_CAB] - BUILD_ARCADE_CABINET_STARTUP_MENU invalid arcade cabinet")
	ENDIF
ENDPROC

FUNC BOOL SHOULD_BAIL_PLAYER_FROM_ARCADE_CABINET(ARCADE_CABINETS eArcadeCabinet)
	IF eArcadeCabinet != ARCADE_CABINET_CH_DRONE_MINI_G
		IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
			PRINTLN("[AM_ARC_CAB] SHOULD_BAIL_PLAYER_FROM_ARCADE_CABINET - IS_PLAYER_CONTROL_ON true")
			RETURN TRUE
		ENDIF
	ENDIF	
	
	RETURN FALSE
ENDFUNC

PROC NAVIGATE_ARCADE_CABINET_STARTUP_MENU()
	ARCADE_CABINETS eCabinetLocatePlayerIsIn = GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID())
	
	// Navigate menu
	IF IS_ARCADE_CABINET_MENU_BS(sACMData, ARCADE_CABINET_MANAGER_MENU_BS_PRESS_ACCEPT)
		IF IS_BIT_SET(sACMData.sArcadeCabinetMenu.iMenuAvailable, sACMData.sArcadeCabinetMenu.iMenuCurrentItem)
			INT iDisplaySlot = GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID())
			// Get Animation dic name
			g_ArcadeCabinetManagerData.iAnimVariation[iDisplaySlot] = GET_ARCADE_CABINET_ANIMATION_VARIATION(eCabinetLocatePlayerIsIn, iDisplaySlot, GET_ARCADE_CABINET_LOCATE_PLAYER_SLOT_PLAYER_IS_IN(PLAYER_ID()))
			ARCADE_CABINET_ANIM_DETAIL eAnimDetails
			GET_ARCADE_CABINET_ANIMATION_DETAIL(eCabinetLocatePlayerIsIn, ARCADE_CABINET_ANIM_CLIP_INVALID, g_ArcadeCabinetManagerData.iAnimVariation[iDisplaySlot], eAnimDetails)
			g_ArcadeCabinetManagerData.strArcCabinetAnimDic = eAnimDetails.strArcCabinetAnimDic
			PRINTLN("[AM_ARC_CAB] - NAVIGATE_ARCADE_CABINET_STARTUP_MENU anim dic: ", g_ArcadeCabinetManagerData.strArcCabinetAnimDic)
			INITALISE_ENTRY_ANIMS()
			SET_ARCADE_CABINET_MENU_BS(sACMData, ARCADE_CABINET_MANAGER_MENU_BS_EXIT_MENU)
			SET_ARCADE_CABINET_MANAGER_STATE(ARCADE_CABINET_GO_TO_INITAL_COORDS_STATE)
		ENDIF
	ELIF (IS_ARCADE_CABINET_MENU_BS(sACMData, ARCADE_CABINET_MANAGER_MENU_BS_PRESS_UP) AND ALLOW_ANALOGUE_MOVEMENT(sACMData.sArcadeCabinetMenu.scrollDelay, sACMData.sArcadeCabinetMenu.iMoveUp))
		IF sACMData.sArcadeCabinetMenu.iMoveUp > 0
			sACMData.sArcadeCabinetMenu.iMenuCurrentItem--
			IF sACMData.sArcadeCabinetMenu.iMenuCurrentItem < 0
				sACMData.sArcadeCabinetMenu.iMenuCurrentItem = sACMData.sArcadeCabinetMenu.iNumItemInMenu - 1
			ENDIF
			IF IS_BIT_SET(sACMData.sArcadeCabinetMenu.iMenuToggleable, sACMData.sArcadeCabinetMenu.iMenuCurrentItem)
				SET_MENU_ITEM_TOGGLEABLE(FALSE, TRUE)
			ENDIF
			SET_CURRENT_MENU_ITEM(sACMData.sArcadeCabinetMenu.iMenuCurrentItem)	
			PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		ENDIF	
	ELIF (IS_ARCADE_CABINET_MENU_BS(sACMData, ARCADE_CABINET_MANAGER_MENU_BS_PRESS_DOWN) AND ALLOW_ANALOGUE_MOVEMENT(sACMData.sArcadeCabinetMenu.scrollDelay, sACMData.sArcadeCabinetMenu.iMoveUp))
		IF sACMData.sArcadeCabinetMenu.iMoveUp < 0
			sACMData.sArcadeCabinetMenu.iMenuCurrentItem++
			IF sACMData.sArcadeCabinetMenu.iMenuCurrentItem > sACMData.sArcadeCabinetMenu.iNumItemInMenu - 1
				sACMData.sArcadeCabinetMenu.iMenuCurrentItem = 0
			ENDIF
			IF IS_BIT_SET(sACMData.sArcadeCabinetMenu.iMenuToggleable, sACMData.sArcadeCabinetMenu.iMenuCurrentItem)
				SET_MENU_ITEM_TOGGLEABLE(FALSE, TRUE)
			ENDIF
			SET_CURRENT_MENU_ITEM(sACMData.sArcadeCabinetMenu.iMenuCurrentItem)
			PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		ENDIF
	ELIF ((IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_RIGHT) AND ALLOW_ANALOGUE_MOVEMENT(sACMData.sArcadeCabinetMenu.scrollDelay, sACMData.sArcadeCabinetMenu.iMoveUp, FALSE))
	OR (sACMData.sArcadeCabinetMenu.iCursorValue = -1 ))
	AND IS_BIT_SET(sACMData.sArcadeCabinetMenu.iMenuToggleable, sACMData.sArcadeCabinetMenu.iMenuCurrentItem)
	AND IS_BIT_SET(sACMData.sArcadeCabinetMenu.iMenuAvailable, sACMData.sArcadeCabinetMenu.iMenuCurrentItem)
		IF sACMData.sArcadeCabinetMenu.iMoveUp > 0
		OR sACMData.sArcadeCabinetMenu.iCursorValue = -1
			sACMData.sArcadeCabinetMenu.iNumToggleable[sACMData.sArcadeCabinetMenu.iMenuCurrentItem] ++
			IF sACMData.sArcadeCabinetMenu.iNumToggleable[sACMData.sArcadeCabinetMenu.iMenuCurrentItem]  > sACMData.sArcadeCabinetMenu.iMaxNumToggleable - 1
				sACMData.sArcadeCabinetMenu.iNumToggleable[sACMData.sArcadeCabinetMenu.iMenuCurrentItem]  = 0
			ENDIF
			PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			SET_CURRENT_MENU_ITEM(sACMData.sArcadeCabinetMenu.iMenuCurrentItem)
			SET_MENU_ITEM_TOGGLEABLE(FALSE, TRUE)
			SET_ARCADE_CABINET_MENU_BS(sACMData, ARCADE_CABINET_MANAGER_MENU_BS_BUILD_MENU)
			PRINTLN("[AM_ARC_CAB] - NAVIGATE_ARCADE_CABINET_STARTUP_MENU toggleable index: ", sACMData.sArcadeCabinetMenu.iNumToggleable[sACMData.sArcadeCabinetMenu.iMenuCurrentItem])
		ENDIF	
	ELIF ((IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_LEFT) AND ALLOW_ANALOGUE_MOVEMENT(sACMData.sArcadeCabinetMenu.scrollDelay, sACMData.sArcadeCabinetMenu.iMoveUp, FALSE))
	OR (sACMData.sArcadeCabinetMenu.iCursorValue = 1))
	AND IS_BIT_SET(sACMData.sArcadeCabinetMenu.iMenuToggleable, sACMData.sArcadeCabinetMenu.iMenuCurrentItem)
	AND IS_BIT_SET(sACMData.sArcadeCabinetMenu.iMenuAvailable, sACMData.sArcadeCabinetMenu.iMenuCurrentItem)
		IF sACMData.sArcadeCabinetMenu.iMoveUp < 0
		OR sACMData.sArcadeCabinetMenu.iCursorValue = 1
			sACMData.sArcadeCabinetMenu.iNumToggleable[sACMData.sArcadeCabinetMenu.iMenuCurrentItem] --
			IF sACMData.sArcadeCabinetMenu.iNumToggleable[sACMData.sArcadeCabinetMenu.iMenuCurrentItem]  < 0
				sACMData.sArcadeCabinetMenu.iNumToggleable[sACMData.sArcadeCabinetMenu.iMenuCurrentItem] = sACMData.sArcadeCabinetMenu.iMaxNumToggleable - 1
			ENDIF
			PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			SET_CURRENT_MENU_ITEM(sACMData.sArcadeCabinetMenu.iMenuCurrentItem)
			SET_MENU_ITEM_TOGGLEABLE(FALSE, TRUE)
			SET_ARCADE_CABINET_MENU_BS(sACMData, ARCADE_CABINET_MANAGER_MENU_BS_BUILD_MENU)
			PRINTLN("[AM_ARC_CAB] - NAVIGATE_ARCADE_CABINET_STARTUP_MENU toggleable index: ", sACMData.sArcadeCabinetMenu.iNumToggleable[sACMData.sArcadeCabinetMenu.iMenuCurrentItem])
		ENDIF	
	ELIF IS_ARCADE_CABINET_MENU_BS(sACMData, ARCADE_CABINET_MANAGER_MENU_BS_PRESS_BACK)
	OR SHOULD_BAIL_PLAYER_FROM_ARCADE_CABINET(eCabinetLocatePlayerIsIn)	
		SET_ARCADE_CABINET_MENU_BS(sACMData, ARCADE_CABINET_MANAGER_MENU_BS_EXIT_MENU)
		PLAY_SOUND_FRONTEND(-1, "CANCEL", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		STOP_ALL_ARCADE_CABINET_SOUNDS(sACMData)
		STOP_GAME_AUDIO_SCENE()
		PLAY_ARCADE_CABINET_IDLE_SOUND()
		SET_ARCADE_CABINET_MANAGER_STATE(ARCADE_CABINET_CLEANUP_EXIT_STATE)
	ENDIF	
ENDPROC

PROC UPDATE_ARCADE_CABINET_STARTUP_MENU()
	IF !IS_PAUSE_MENU_ACTIVE()
		NAVIGATE_ARCADE_CABINET_STARTUP_MENU()
	ENDIF	
	
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
	HIDE_HUD_AND_RADAR_THIS_FRAME()
ENDPROC

PROC MAINTAIN_STARTUP_MENU_ARROW_CLICK()
	
	sACMData.sArcadeCabinetMenu.iCursorValue = 0
	
	IF IS_BIT_SET(sACMData.sArcadeCabinetMenu.iMenuAvailable, sACMData.sArcadeCabinetMenu.iMenuCurrentItem)
	AND IS_BIT_SET(sACMData.sArcadeCabinetMenu.iMenuToggleable, sACMData.sArcadeCabinetMenu.iMenuCurrentItem)
		// Cursor is inside the menu
		IF g_iMenuCursorItem > MENU_CURSOR_NO_ITEM	
			// Clicking on an item
			IF IS_ARCADE_CABINET_MENU_BS(sACMData, ARCADE_CABINET_MANAGER_MENU_BS_PRESS_ACCEPT)
				// Clicking on currently selected item
				IF g_iMenuCursorItem = sACMData.sArcadeCabinetMenu.iMenuCurrentItem
					// Item is a < item > style 
					IF g_iMenuCursorItem = sACMData.sArcadeCabinetMenu.iMenuCurrentItem
						sACMData.sArcadeCabinetMenu.iCursorValue = GET_CURSOR_MENU_ITEM_VALUE_CHANGE()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
ENDPROC

PROC HANDLE_ARCADE_CABINET_STARTUP_MENU_DESCRIPTION()
	ARCADE_CABINETS eCabinetLocatePlayerIsIn = GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID())
	TEXT_LABEL_63 blockReason
	
	SWITCH eCabinetLocatePlayerIsIn
		CASE ARCADE_CABINET_CH_DRONE_MINI_G
			IF SHOULD_BLOCK_DRONE_MINI_GAME_MENU_OPTION(blockReason)
				IF NOT IS_STRING_NULL_OR_EMPTY(blockReason)
					SET_CURRENT_MENU_ITEM_DESCRIPTION(blockReason)
				ENDIF	
			ENDIF	
		BREAK	
		CASE ARCADE_CABINET_CH_DRILLING_LASER_MINI_G
			SWITCH sACMData.sArcadeCabinetMenu.iMenuCurrentItem
				CASE 0
					IF SHOULD_BLOCK_DRILL_OPTION(blockReason)
						IF NOT IS_STRING_NULL_OR_EMPTY(blockReason)
							SET_CURRENT_MENU_ITEM_DESCRIPTION(blockReason)
						ENDIF	
					ENDIF
				BREAK	
				CASE 1
					IF SHOULD_BLOCK_LASER_OPTION(blockReason)
						IF NOT IS_STRING_NULL_OR_EMPTY(blockReason)
							SET_CURRENT_MENU_ITEM_DESCRIPTION(blockReason)
						ENDIF	
					ENDIF
				BREAK	
			ENDSWITCH 	
		BREAK	
	ENDSWITCH 
ENDPROC

PROC MAINTAIN_DRAW_STARTUP_MENU()
	SETUP_ARCADE_CABINET_MANAGEMENT_MENU_CONTROLS(sACMData, serverBD)
	MAINTAIN_STARTUP_MENU_ARROW_CLICK()
	IF LOAD_MENU_ASSETS()
		IF IS_ARCADE_CABINET_MENU_BS(sACMData, ARCADE_CABINET_MANAGER_MENU_BS_BUILD_MENU)
			BUILD_ARCADE_CABINET_STARTUP_MENU()
			CLEAR_ARCADE_CABINET_MENU_BS(sACMData, ARCADE_CABINET_MANAGER_MENU_BS_BUILD_MENU)
		ELSE
			SET_CURRENT_MENU_ITEM(sACMData.sArcadeCabinetMenu.iMenuCurrentItem)
		ENDIF
		
		UPDATE_ARCADE_CABINET_STARTUP_MENU()
		HANDLE_ARCADE_CABINET_STARTUP_MENU_DESCRIPTION()
		
		DRAW_MENU()
	ENDIF

ENDPROC

/// PURPOSE:
///    Maintain arcade cabinet entry animation if manager has control of animations = SHOULD_ALL_ARCADE_CABINET_ANIMATION_HANDLE_BY_CHILD_SCRIPT
PROC MAINTAIN_ACM_ENTRY_ANIM_STATE()
	DISABLE_FRONTEND_THIS_FRAME()
	
	ARCADE_CABINETS eCabinetLocatePlayerIsIn = GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID())
	INT iDisplaySlot = GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID())
	IF !IS_ARCADE_CABINET_MANAGER_BIT_SET(GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID()), ARCADE_CABINET_MANAGER_START_ENTER_ANIM)	
		IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) != WAITING_TO_START_TASK
		AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) != PERFORMING_TASK
		AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ACHIEVE_HEADING) != WAITING_TO_START_TASK
		AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ACHIEVE_HEADING) != PERFORMING_TASK
			
			IF SHOULD_STOP_ARCADE_CABINET_HINT_CAMERA(eCabinetLocatePlayerIsIn)
				IF IS_GAMEPLAY_HINT_ACTIVE()
					STOP_GAMEPLAY_HINT()
				ENDIF	
			ENDIF
			
			IF !SHOULD_ALL_ARCADE_CABINET_ANIMATION_HANDLE_BY_CHILD_SCRIPT(eCabinetLocatePlayerIsIn)
				IF iDisplaySlot != -1
					ARCADE_CABINET_ANIM_CLIPS eClip = ARCADE_CABINET_ANIM_CLIP_ENTRY
				
					IF IS_LOCAL_ARCADE_CABINET_MANAGER_BIT_SET(ARCADE_CABINET_MANAGER_LOCAL_BS_USE_ENTER_B)
						eClip = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_ENTER_B
					ENDIF
					
					FLOAT blendInDelta = SLOW_BLEND_IN
					FLOAT blendOutDelta = SLOW_BLEND_OUT
					
					START_ARCADE_CABINET_SYNC_SCENE(eClip, DEFAULT, DEFAULT, blendInDelta, blendOutDelta)	
					
					IF SHOULD_FORCE_FP_CAMERA_FOR_ARCADE_CABINET_ENTER_ANIM(eCabinetLocatePlayerIsIn)
						sACMData.startCamViewMode = GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT)
						IF sACMData.startCamViewMode != CAM_VIEW_MODE_FIRST_PERSON
							SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT, CAM_VIEW_MODE_FIRST_PERSON)
							SET_ARCADE_CABINET_MANAGER_LOCAL_BIT(ARCADE_CABINET_MANAGER_LOCAL_BS_FORCE_FPP_CAM)
						ENDIF	
					ENDIF
					
					SET_ARCADE_CABINET_MANAGER_BIT(GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID()), ARCADE_CABINET_MANAGER_START_ENTER_ANIM)
				ELSE
					PRINTLN("[AM_ARC_CAB] - MAINTAIN_ACM_ENTRY_ANIM_STATE ARCADE_CABINET_MANAGER_START_ENTER_ANIM false iDisplaySlot = -1")
				ENDIF	
			ELSE
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_PREVENT_VISIBILITY_CHANGES | NSPC_LEAVE_CAMERA_CONTROL_ON)
				SET_PLAYER_READY_TO_PLAY_ARCADE_GAME(TRUE)
				START_GAME_AUDIO_SCENE()
				SET_PLAYER_CONFIG_FLAG_KINEMATIC_MODE_WHEN_STATIONARY(TRUE)
				SET_ARCADE_CABINET_MANAGER_STATE(ARCADE_CABINET_IN_USE_ANIM_STATE)
				IF NOT IS_THIS_ARCADE_FOR_A_MINI_GAME(eCabinetLocatePlayerIsIn)
					SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_PLAY_ARCADE_GAME)
				ENDIF
			ENDIF	
		ENDIF
	ELSE
		IF iDisplaySlot != -1
			// Get Animation detail
			ARCADE_CABINET_ANIM_DETAIL eAnimDetails
			ARCADE_CABINET_ANIM_CLIPS eClip = ARCADE_CABINET_ANIM_CLIP_ENTRY
				
			IF IS_LOCAL_ARCADE_CABINET_MANAGER_BIT_SET(ARCADE_CABINET_MANAGER_LOCAL_BS_USE_ENTER_B)
				eClip = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_ENTER_B
			ENDIF	
			
			GET_ARCADE_CABINET_ANIMATION_DETAIL(eCabinetLocatePlayerIsIn, eClip, g_ArcadeCabinetManagerData.iAnimVariation[iDisplaySlot], eAnimDetails)
			g_ArcadeCabinetManagerData.strArcCabinetAnimDic = eAnimDetails.strArcCabinetAnimDic
			PRINTLN("[AM_ARC_CAB] - MAINTAIN_ACM_ENTRY_ANIM_STATE anim dic: ", g_ArcadeCabinetManagerData.strArcCabinetAnimDic)
			
			IF DOES_THIS_ARCADE_CABINET_HAS_GLITCH_MODE(eCabinetLocatePlayerIsIn)
				IF IS_ARCADE_CABINET_GLITCHED()
					IF IS_ARCADE_CABINET_SYNC_SCENE_PHASE_PASSED(eClip, 0.6)
						IF !IS_LOCAL_ARCADE_CABINET_MANAGER_BIT_SET(ARCADE_CABINET_MANAGER_LOCAL_BS_PLAY_KICK_SOUND)
							PLAY_SOUND_FROM_COORD(-1, "Kick_Machine", sACMData.vArcadeCabCoords[iDisplaySlot], "DLC_H3_Arc_Mac_Degen_DotF_Sounds", TRUE, 20)
							SET_ARCADE_CABINET_MANAGER_LOCAL_BIT(ARCADE_CABINET_MANAGER_LOCAL_BS_PLAY_KICK_SOUND)
						ENDIF	
					ENDIF
				ENDIF
			ENDIF
			
			IF HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(eClip)
				START_ARCADE_CABINET_SYNC_SCENE(ARCADE_CABINET_ANIM_CLIP_IDLE, DEFAULT, TRUE)
				IF sACMData.eArcadeCab[iDisplaySlot] != ARCADE_CABINET_INVALID
					IF DOES_ARCADE_CABINET_USE_CHILD_SCRIPT(sACMData.eArcadeCab[iDisplaySlot], sACMData.strCabinetChildScript, TRUE)
						SET_ARCADE_CABINET_MANAGER_STATE(ARCADE_CABINET_LAUNCH_GAME_SCRIPT_STATE)
					ELSE
						SET_PLAYER_READY_TO_PLAY_ARCADE_GAME(TRUE)
						START_GAME_AUDIO_SCENE()
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_PREVENT_VISIBILITY_CHANGES | NSPC_LEAVE_CAMERA_CONTROL_ON)
						SET_PLAYER_CONFIG_FLAG_KINEMATIC_MODE_WHEN_STATIONARY(TRUE)
						SET_ARCADE_CABINET_MANAGER_STATE(ARCADE_CABINET_IN_USE_ANIM_STATE)
						IF NOT IS_THIS_ARCADE_FOR_A_MINI_GAME(sACMData.eArcadeCab[iDisplaySlot])
							SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_PLAY_ARCADE_GAME)
						ENDIF
					ENDIF	
				ENDIF	
			ENDIF
		ELSE
			PRINTLN("[AM_ARC_CAB] - MAINTAIN_ACM_ENTRY_ANIM_STATE iDisplaySlot = -1")
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_HACKING_MINI_GAME_SUB_PROP_VISIBILITY(INT iDisplaySlot)
	//Hacking device visibility
	IF DOES_ENTITY_EXIST(sACMData.arcadeCabinetSubObj[iDisplaySlot][ARCADE_CAB_OBJECT_1])
		IF IS_ARCADE_CABINET_SYNC_SCENE_PHASE_PASSED(ARCADE_CABINET_ANIM_CLIP_ENTRY, 0.1)
		//IF IS_ARCADE_CABINET_SYNC_SCENE_EVENT_FIRED_ON_PROP_ANIM(sACMData, ARCADE_CABINET_ANIM_CLIP_ENTRY, iDisplaySlot, ENUM_TO_INT(ARCADE_CAB_OBJECT_1), "CREATE")
			IF NOT IS_ENTITY_VISIBLE_TO_SCRIPT(sACMData.arcadeCabinetSubObj[iDisplaySlot][ARCADE_CAB_OBJECT_1])
				SET_ENTITY_VISIBLE(sACMData.arcadeCabinetSubObj[iDisplaySlot][ARCADE_CAB_OBJECT_1], TRUE)
				PRINTLN("[AM_ARC_CAB] MAINTAIN_HACKING_MINI_GAME_SUB_PROP_VISIBILITY SET_ENTITY_VISIBLE ARCADE_CAB_OBJECT_1 TRUE")
			ENDIF	
		ELIF IS_ARCADE_CABINET_SYNC_SCENE_PHASE_PASSED(ARCADE_CABINET_ANIM_CLIP_HACKING_PASS, 0.80)
		OR IS_ARCADE_CABINET_SYNC_SCENE_PHASE_PASSED(ARCADE_CABINET_ANIM_CLIP_EXIT, 0.98)
			IF IS_ENTITY_VISIBLE_TO_SCRIPT(sACMData.arcadeCabinetSubObj[iDisplaySlot][ARCADE_CAB_OBJECT_1])
				SET_ENTITY_VISIBLE(sACMData.arcadeCabinetSubObj[iDisplaySlot][ARCADE_CAB_OBJECT_1], FALSE)
				PRINTLN("[AM_ARC_CAB] MAINTAIN_HACKING_MINI_GAME_SUB_PROP_VISIBILITY SET_ENTITY_VISIBLE ARCADE_CAB_OBJECT_1 FALSE")
			ENDIF	
		ENDIF
	ENDIF
	
	//Fake phone visibility
	IF DOES_ENTITY_EXIST(sACMData.arcadeCabinetSubObj[iDisplaySlot][ARCADE_CAB_OBJECT_2])
		IF IS_ARCADE_CABINET_SYNC_SCENE_PHASE_PASSED(ARCADE_CABINET_ANIM_CLIP_ENTRY, 0.72)	
		//IF IS_ARCADE_CABINET_SYNC_SCENE_EVENT_FIRED_ON_PROP_ANIM(sACMData, ARCADE_CABINET_ANIM_CLIP_ENTRY, iDisplaySlot, ENUM_TO_INT(ARCADE_CAB_OBJECT_2), "CREATE")
			IF NOT IS_ENTITY_VISIBLE_TO_SCRIPT(sACMData.arcadeCabinetSubObj[iDisplaySlot][ARCADE_CAB_OBJECT_2])
				SET_ENTITY_VISIBLE(sACMData.arcadeCabinetSubObj[iDisplaySlot][ARCADE_CAB_OBJECT_2], TRUE)
				PRINTLN("[AM_ARC_CAB] MAINTAIN_HACKING_MINI_GAME_SUB_PROP_VISIBILITY SET_ENTITY_VISIBLE ARCADE_CAB_OBJECT_2 TRUE")
			ENDIF	
		ELIF IS_ARCADE_CABINET_SYNC_SCENE_PHASE_PASSED(ARCADE_CABINET_ANIM_CLIP_HACKING_PASS, 0.24)	
		OR IS_ARCADE_CABINET_SYNC_SCENE_PHASE_PASSED(ARCADE_CABINET_ANIM_CLIP_EXIT, 0.98)
			IF IS_ENTITY_VISIBLE_TO_SCRIPT(sACMData.arcadeCabinetSubObj[iDisplaySlot][ARCADE_CAB_OBJECT_2])
				SET_ENTITY_VISIBLE(sACMData.arcadeCabinetSubObj[iDisplaySlot][ARCADE_CAB_OBJECT_2], FALSE)
				PRINTLN("[AM_ARC_CAB] MAINTAIN_HACKING_MINI_GAME_SUB_PROP_VISIBILITY SET_ENTITY_VISIBLE ARCADE_CAB_OBJECT_2 FALSE")
			ENDIF					
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_ARCADE_CABINET_SUB_PROP_VISIBILITY_IN_ANIM()
	ARCADE_CABINETS eCabinetLocatePlayerIsIn = GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID())
	IF eCabinetLocatePlayerIsIn != ARCADE_CABINET_INVALID
		INT iDisplaySlot = GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID())
		IF iDisplaySlot != -1
			SWITCH	eCabinetLocatePlayerIsIn
				CASE ARCADE_CABINET_CH_HACKING_MINI_G
					MAINTAIN_HACKING_MINI_GAME_SUB_PROP_VISIBILITY(iDisplaySlot)
				BREAK	
			ENDSWITCH
		ENDIF	
	ENDIF	
ENDPROC

PROC MAINTAIN_HACKING_MINI_GAME_IN_USE_STATE()
	SWITCH sACMData.sArcadeCabinetMenu.iMenuCurrentItem
		CASE 0
			IF sACMData.sArcadeCabinetAnimEvent.ePreviousAnimClip != ARCADE_CABINET_ANIM_CLIP_HACKING_FAIL
			AND sACMData.sArcadeCabinetAnimEvent.ePreviousAnimClip != ARCADE_CABINET_ANIM_CLIP_HACKING_WAIT_IDLE
			AND sACMData.sArcadeCabinetAnimEvent.ePreviousAnimClip != ARCADE_CABINET_ANIM_CLIP_EXIT
				PROCESS_FINGERPRINT_CLONE_MINIGAME(fingerPrintCloneData, sACMData.sArcadeHackingPractice, ciFC_NUMBER_OF_PRINTS_INGAME)
			ENDIF
			
			IF IS_BIT_SET(sACMData.sArcadeHackingPractice.iBS, ARCADE_MINI_GAME_PASSED)
				CLEAN_UP_FINGERPRINT_CLONE(fingerPrintCloneData, sACMData.sArcadeHackingPractice)
				sACMData.sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
				sACMData.sArcadeCabinetAnimEvent.bLoopAnim = FALSE
				PLAY_ARCADE_CABINET_ANIMATION(sACMData.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_HACKING_PASS)
			ELIF IS_BIT_SET(sACMData.sArcadeHackingPractice.iBS, ARCADE_MINI_GAME_QUIT)
			OR IS_BIT_SET(sACMData.sArcadeHackingPractice.iBS, ARCADE_MINI_GAME_FAILED)
				CLEAN_UP_FINGERPRINT_CLONE(fingerPrintCloneData, sACMData.sArcadeHackingPractice)
				sACMData.sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
				sACMData.sArcadeCabinetAnimEvent.bLoopAnim = FALSE
				PLAY_ARCADE_CABINET_ANIMATION(sACMData.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_HACKING_FAIL)	
			ENDIF
			
			IF sACMData.sArcadeCabinetAnimEvent.ePreviousAnimClip = ARCADE_CABINET_ANIM_CLIP_HACKING_FAIL
			AND IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(ARCADE_CABINET_ANIM_CLIP_HACKING_FAIL)
			AND HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(ARCADE_CABINET_ANIM_CLIP_HACKING_FAIL)
				PLAY_ARCADE_CABINET_ANIMATION(sACMData.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_EXIT)		
			ENDIF
		BREAK
		CASE 1
			IF sACMData.sArcadeCabinetAnimEvent.ePreviousAnimClip != ARCADE_CABINET_ANIM_CLIP_HACKING_FAIL
			AND sACMData.sArcadeCabinetAnimEvent.ePreviousAnimClip != ARCADE_CABINET_ANIM_CLIP_HACKING_WAIT_IDLE
			AND sACMData.sArcadeCabinetAnimEvent.ePreviousAnimClip != ARCADE_CABINET_ANIM_CLIP_EXIT
				PROCESS_ORDER_UNLOCK_MINIGAME(orderUnlockData, sACMData.sArcadeHackingPractice, ciOU_NUMBER_OF_PATTERNS)
			ENDIF
			
			IF IS_BIT_SET(sACMData.sArcadeHackingPractice.iBS, ARCADE_MINI_GAME_PASSED)
				CLEAN_UP_ORDER_UNLOCK(orderUnlockData, sACMData.sArcadeHackingPractice)
				sACMData.sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
				sACMData.sArcadeCabinetAnimEvent.bLoopAnim = FALSE
				PLAY_ARCADE_CABINET_ANIMATION(sACMData.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_HACKING_PASS)
			ELIF IS_BIT_SET(sACMData.sArcadeHackingPractice.iBS, ARCADE_MINI_GAME_QUIT)
			OR IS_BIT_SET(sACMData.sArcadeHackingPractice.iBS, ARCADE_MINI_GAME_FAILED)
				CLEAN_UP_ORDER_UNLOCK(orderUnlockData, sACMData.sArcadeHackingPractice)
				sACMData.sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
				sACMData.sArcadeCabinetAnimEvent.bLoopAnim = FALSE
				PLAY_ARCADE_CABINET_ANIMATION(sACMData.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_HACKING_FAIL)
			ENDIF
			
			IF sACMData.sArcadeCabinetAnimEvent.ePreviousAnimClip = ARCADE_CABINET_ANIM_CLIP_HACKING_FAIL
			AND IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(ARCADE_CABINET_ANIM_CLIP_HACKING_FAIL)
			AND HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(ARCADE_CABINET_ANIM_CLIP_HACKING_FAIL)
				PLAY_ARCADE_CABINET_ANIMATION(sACMData.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_EXIT)
			ENDIF
		BREAK
		CASE 2
			IF sACMData.sArcadeCabinetAnimEvent.ePreviousAnimClip != ARCADE_CABINET_ANIM_CLIP_HACKING_FAIL
			AND sACMData.sArcadeCabinetAnimEvent.ePreviousAnimClip != ARCADE_CABINET_ANIM_CLIP_HACKING_WAIT_IDLE
			AND sACMData.sArcadeCabinetAnimEvent.ePreviousAnimClip != ARCADE_CABINET_ANIM_CLIP_EXIT
				PROCESS_FINGERPRINT_RETRO_MINIGAME(fingerPrintCloneData, sACMData.sArcadeHackingPractice, ciFR_NUMBER_OF_PRINTS_INGAME, DEFAULT)
			ENDIF
			
			IF IS_BIT_SET(sACMData.sArcadeHackingPractice.iBS, ARCADE_MINI_GAME_PASSED)
				CLEAN_UP_FINGERPRINT_RETRO(fingerPrintCloneData, sACMData.sArcadeHackingPractice, DEFAULT, DEFAULT, GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST4_SESSION_ID_POSTIME))
				sACMData.sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
				sACMData.sArcadeCabinetAnimEvent.bLoopAnim = FALSE
				PLAY_ARCADE_CABINET_ANIMATION(sACMData.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_HACKING_PASS)
			ELIF IS_BIT_SET(sACMData.sArcadeHackingPractice.iBS, ARCADE_MINI_GAME_QUIT)
			OR IS_BIT_SET(sACMData.sArcadeHackingPractice.iBS, ARCADE_MINI_GAME_FAILED)
				CLEAN_UP_FINGERPRINT_RETRO(fingerPrintCloneData, sACMData.sArcadeHackingPractice, DEFAULT, DEFAULT, GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST4_SESSION_ID_POSTIME))
				sACMData.sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
				sACMData.sArcadeCabinetAnimEvent.bLoopAnim = FALSE
				PLAY_ARCADE_CABINET_ANIMATION(sACMData.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_HACKING_FAIL)	
			ENDIF
			
			IF sACMData.sArcadeCabinetAnimEvent.ePreviousAnimClip = ARCADE_CABINET_ANIM_CLIP_HACKING_FAIL
			AND IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(ARCADE_CABINET_ANIM_CLIP_HACKING_FAIL)
			AND HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(ARCADE_CABINET_ANIM_CLIP_HACKING_FAIL)
				PLAY_ARCADE_CABINET_ANIMATION(sACMData.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_EXIT)		
			ENDIF
		BREAK
		CASE 3
			IF sACMData.sArcadeCabinetAnimEvent.ePreviousAnimClip != ARCADE_CABINET_ANIM_CLIP_HACKING_FAIL
			AND sACMData.sArcadeCabinetAnimEvent.ePreviousAnimClip != ARCADE_CABINET_ANIM_CLIP_HACKING_WAIT_IDLE
			AND sACMData.sArcadeCabinetAnimEvent.ePreviousAnimClip != ARCADE_CABINET_ANIM_CLIP_EXIT									
				PROCESS_VOLTAGE_GAME(sACMData.sArcadeHackingPractice, fingerPrintCloneData.sBaseStruct)
			ENDIF
			
			IF IS_BIT_SET(sACMData.sArcadeHackingPractice.iBS, ARCADE_MINI_GAME_PASSED)
				VOLTAGE_CLEANUP(sACMData.sArcadeHackingPractice, fingerPrintCloneData.sBaseStruct)
				sACMData.sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
				sACMData.sArcadeCabinetAnimEvent.bLoopAnim = FALSE
				PLAY_ARCADE_CABINET_ANIMATION(sACMData.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_HACKING_PASS)
			ELIF IS_BIT_SET(sACMData.sArcadeHackingPractice.iBS, ARCADE_MINI_GAME_QUIT)
			OR IS_BIT_SET(sACMData.sArcadeHackingPractice.iBS, ARCADE_MINI_GAME_FAILED)
				VOLTAGE_CLEANUP(sACMData.sArcadeHackingPractice, fingerPrintCloneData.sBaseStruct)
				sACMData.sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
				sACMData.sArcadeCabinetAnimEvent.bLoopAnim = FALSE
				PLAY_ARCADE_CABINET_ANIMATION(sACMData.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_HACKING_FAIL)	
			ENDIF
			
			IF sACMData.sArcadeCabinetAnimEvent.ePreviousAnimClip = ARCADE_CABINET_ANIM_CLIP_HACKING_FAIL
			AND IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(ARCADE_CABINET_ANIM_CLIP_HACKING_FAIL)
			AND HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(ARCADE_CABINET_ANIM_CLIP_HACKING_FAIL)
				PLAY_ARCADE_CABINET_ANIMATION(sACMData.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_EXIT)		
			ENDIF
			
		BREAK
	ENDSWITCH	
ENDPROC

/// PURPOSE:
///    Maintain arcade cabinet while player is using(playing a game)
PROC MAINTAIN_ACM_IN_USE_ANIM_STATE()
	DISABLE_FRONTEND_THIS_FRAME()
	
	ARCADE_CABINETS eCabinetLocatePlayerIsIn = GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID())

	IF eCabinetLocatePlayerIsIn != ARCADE_CABINET_INVALID
		IF SHOULD_HIDE_HUD_AND_RADAR_WHILE_USING_ARCADE_CABINET(eCabinetLocatePlayerIsIn)
			HIDE_HUD_AND_RADAR_THIS_FRAME()
		ENDIF	
		
		MAINTAIN_ARCADE_CABINET_IN_USE_ANIMATIONS(eCabinetLocatePlayerIsIn, sACMData)
		
		SWITCH eCabinetLocatePlayerIsIn 
			CASE ARCADE_CABINET_CH_HACKING_MINI_G
				MAINTAIN_HACKING_MINI_GAME_IN_USE_STATE()
			BREAK
			#IF FEATURE_SUMMER_2020
			CASE ARCADE_CABINET_SUM_STRENGTH_TEST
				IF HAS_PLAYER_FINISHED_STRENGTH_TEST_ARCADE_GAME(PLAYER_ID())
					SET_ARCADE_CABINET_MANAGER_STATE(ARCADE_CABINET_CLEANUP_STATE)
					EXIT
				ENDIF
			BREAK
			#ENDIF
		ENDSWITCH
		
		sACMData.sControlTimer.control = FRONTEND_CONTROL
		sACMData.sControlTimer.action = INPUT_FRONTEND_CANCEL
		IF IS_CONTROL_HELD(sACMData.sControlTimer)		
			SET_ARCADE_CABINET_MENU_BS(sACMData, ARCADE_CABINET_MANAGER_MENU_BS_INPUT_BLOCK_TIMER_SET)
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			sACMData.sArcadeCabinetMenu.tdInputBlockTimer = GET_NETWORK_TIME()
		ENDIF
		
		IF SHOULD_BAIL_PLAYER_FROM_ARCADE_CABINET(eCabinetLocatePlayerIsIn)
			IF sACMData.sArcadeCabinetAnimEvent.ePreviousAnimClip != ARCADE_CABINET_ANIM_CLIP_EXIT
				PLAY_ARCADE_CABINET_ANIMATION(sACMData.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_EXIT)
				PRINTLN("[AM_ARC_CAB] MAINTAIN_ACM_IN_USE_ANIM_STATE - bail true")
			ENDIF	
		ENDIF	
	ELSE
		SET_ARCADE_CABINET_MANAGER_STATE(ARCADE_CABINET_CLEANUP_STATE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Process arcade cabinet animation events
PROC PROCESS_EVENT_ARCADE_CABINET_ANIMATION(INT iCount)
	SCRIPT_EVENT_DATA_ARCADE_CABINET_ANIM sEventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sEventData, SIZE_OF(sEventData))
		CDEBUG1LN(DEBUG_MINIGAME, "[AM_ARC_CAB] PROCESS_EVENT_ARCADE_CABINET_ANIMATION - 0")
		ARCADE_CABINETS eCabinetLocatePlayerIsIn = GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID())
		
		IF eCabinetLocatePlayerIsIn != ARCADE_CABINET_INVALID
			CDEBUG1LN(DEBUG_MINIGAME, "[AM_ARC_CAB] PROCESS_EVENT_ARCADE_CABINET_ANIMATION - 1")
			IF !SHOULD_ALL_ARCADE_CABINET_ANIMATION_HANDLE_BY_CHILD_SCRIPT(eCabinetLocatePlayerIsIn)
				CDEBUG1LN(DEBUG_MINIGAME, "[AM_ARC_CAB] PROCESS_EVENT_ARCADE_CABINET_ANIMATION - 2")
				START_ARCADE_CABINET_SYNC_SCENE(sEventData.eAnimToPlay,	sEventData.bHoldLastFrame, sEventData.bLoopAnim, sEventData.fBlendInDelta, sEventData.fBlendOutDelta, DEFAULT, sEventData.enToPlayAnim)
			ENDIF	
		ENDIF
		
		IF SHOULD_CLEANUP_ARCADE_CABINET_MANAGER_WITH_THIS_ANIM(sEventData.eAnimToPlay)	
			CDEBUG1LN(DEBUG_MINIGAME, "[AM_ARC_CAB] PROCESS_EVENT_ARCADE_CABINET_ANIMATION - 3")
			IF GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID()) != -1
				CDEBUG1LN(DEBUG_MINIGAME, "[AM_ARC_CAB] PROCESS_EVENT_ARCADE_CABINET_ANIMATION - 4")
				SET_ARCADE_CABINET_MANAGER_BIT(GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID()), ARCADE_CABINET_MANAGER_EXIT_ARCADE_CABINET)
			ENDIF
			
			IF !SHOULD_CLEANUP_ARCADE_CABINET_SOUND_AFTER_EXIT_ANIM_ENDS(sEventData.eAnimToPlay)
				STOP_ALL_ARCADE_CABINET_SOUNDS(sACMData)
				STOP_GAME_AUDIO_SCENE()
				PLAY_ARCADE_CABINET_IDLE_SOUND()
			ENDIF	
			SET_ARCADE_CABINET_MANAGER_STATE(ARCADE_CABINET_CLEANUP_EXIT_STATE)
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_MINIGAME, "[AM_ARC_CAB] PROCESS_EVENT_ARCADE_CABINET_ANIMATION - 5")
		SCRIPT_ASSERT("[AM_ARC_CAB] - PROCESS_EVENT_ARCADE_CABINET_ANIMATION - could not retrieve data.")
	ENDIF
ENDPROC

PROC CLEAR_FLAGS_FOR_UPDATE()
	INT i
	FOR i = 0 TO GET_ARCADE_SLOT_COUNT_FOR_PROPERTY(sACMData.eArcadeCabinetPropertyType) - 1
		CLEAR_ARCADE_CABINET_MANAGER_BIT(i, ARCADE_CABINET_MANAGER_ASSIGN_SUB_PROP_OBJ_INDEX)
		CLEAR_ARCADE_CABINET_MANAGER_BIT(i, ARCADE_CABINET_MANAGER_STARTED_CHILD_SCRIPT)
		CLEAR_ARCADE_CABINET_MANAGER_BIT(i, ARCADE_CABINET_MANAGER_FORCE_TO_ROOM)
	ENDFOR
	
	sACMData.iSoundIndexBS = 0
	sACMData.iArcadeTypeSound = 0
ENDPROC

/// PURPOSE:
///    Process update arcade cabinet props
PROC PROCESS_EVENT_UPADTE_ARCADE_CABINET_PROPS(INT iCount)
	STRUCT_EVENT_COMMON_DETAILS sEventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sEventData, SIZE_OF(sEventData))
		IF NETWORK_IS_PLAYER_A_PARTICIPANT(sEventData.FromPlayerIndex)
			CLEAR_FLAGS_FOR_UPDATE()
			SET_BIT(g_ArcadeCabinetManagerData.iBS, ARC_CAB_MANAGER_GLOBAL_BIT_SET_UPDATE_PROPS)
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				SET_ARCADE_CABINET_MANAGER_SERVER_STATE(ARCADE_CABINET_SERVER_CLEARNUP_STATE)
			ENDIF
			IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = PLAYER_ID()
				SET_BIT(g_ArcadeCabinetManagerData.iBS, ARC_CAB_MANAGER_GLOBAL_BIT_SET_REQUEST_SAVE)
				PRINTLN("[AM_ARC_CAB] - PROCESS_EVENT_UPADTE_ARCADE_CABINET_PROPS - ARC_CAB_MANAGER_GLOBAL_BIT_SET_REQUEST_SAVE TRUE")
			ENDIF	
			PRINTLN("[AM_ARC_CAB] - PROCESS_EVENT_UPADTE_ARCADE_CABINET_PROPS - ARC_CAB_MANAGER_GLOBAL_BIT_SET_UPDATE_PROPS TRUE")	
		ELSE
			PRINTLN("[AM_ARC_CAB] - PROCESS_EVENT_UPADTE_ARCADE_CABINET_PROPS - sender is not a participant")
		ENDIF	
	ELSE
		SCRIPT_ASSERT("[AM_ARC_CAB] - PROCESS_EVENT_UPADTE_ARCADE_CABINET_PROPS - could not retrieve data.")
	ENDIF
ENDPROC

FUNC BOOL IS_SAFE_TO_PLAY_ARCADE_CABINET_DIALOGUE(ARCADE_CABINETS eArcadeCabinet)
	IF IS_SCREEN_FADING_OUT()
	OR IS_SCREEN_FADED_OUT()
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_USING_ARCADE_CABINET(PLAYER_ID())
	AND GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID()) != eArcadeCabinet
		RETURN FALSE
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(sACMData.tSpeechCoolDown)
		IF !HAS_NET_TIMER_EXPIRED(sACMData.tSpeechCoolDown, 500)
			RETURN FALSE
		ELSE
			RESET_NET_TIMER(sACMData.tSpeechCoolDown)
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC 

PROC PLAY_CARMOD_PED_AMBIENT_SPEECH(ARCADE_CABINETS eArcadeCabinet, ARCADE_CAB_MANAGER_SLOT sSlot, STRING Context, BOOL bUseConvo, SPEECH_PARAMS Params = SPEECH_PARAMS_FORCE)

	PRINTLN("PLAY_CARMOD_PED_AMBIENT_SPEECH Context:" , Context , " Params: " , Params, " Use Convo: ", bUseConvo)

	IF IS_STRING_NULL_OR_EMPTY(Context)
	OR !IS_SAFE_TO_PLAY_ARCADE_CABINET_DIALOGUE(eArcadeCabinet)
		PRINTLN("PLAY_CARMOD_PED_AMBIENT_SPEECH not safe")
		EXIT
	ENDIF
	
	STRING sVoiceName
	VECTOR vCoord 
	FLOAT fHeading
	STRING sConvoTextBlock
	INT iConvoSpeakerID
	
	SWITCH eArcadeCabinet 
		CASE ARCADE_CABINET_CH_CLAW_CRANE
			sVoiceName = "CLAW_MACHINE"
			Params = SPEECH_PARAMS_FORCE
		BREAK		
		#IF FEATURE_SUMMER_2020
		CASE ARCADE_CABINET_SUM_STRENGTH_TEST
			sVoiceName = "STRENGTH_TEST"
		BREAK	
		#ENDIF
		CASE ARCADE_CABINET_CH_LOVE_METER
			sVoiceName = "LOVE_MACHINE"
			Params = SPEECH_PARAMS_FORCE
		BREAK
		CASE ARCADE_CABINET_CH_FORTUNE_TELLER
			sVoiceName = "MADAM_NAZAR"
			Params = SPEECH_PARAMS_FORCE
		BREAK
	ENDSWITCH
	
	IF IS_STRING_NULL_OR_EMPTY(sVoiceName)
		PRINTLN("PLAY_CARMOD_PED_AMBIENT_SPEECH - Bail: sVoiceName is null")
		EXIT
	ELSE
		PRINTLN("PLAY_CARMOD_PED_AMBIENT_SPEECH sVoiceName is: ", sVoiceName)
	ENDIF
	
	GET_ARCADE_CABINET_DISPLAY_SLOT_COORD_AND_HEADING(sACMData.eArcadeCabinetPropertyType, sSlot, eArcadeCabinet, vCoord, fHeading)
	
	IF NOT IS_PLAYER_WITHIN_CABINET_COORDS_SPEECH_LISTEN_DISTANCE(PLAYER_ID(), vCoord)
		PRINTLN("PLAY_CARMOD_PED_AMBIENT_SPEECH - Bail: Local player is too far from cabinet coords")
		EXIT
	ENDIF
	
	IF bUseConvo
		sConvoTextBlock = GET_CONVO_SPEECH_TEXT_BLOCK(eArcadeCabinet)
		iConvoSpeakerID = GET_CONVO_SPEECH_SPEAKER_ID(eArcadeCabinet)
		
		IF IS_STRING_NULL_OR_EMPTY(sConvoTextBlock)
			PRINTLN("PLAY_CARMOD_PED_AMBIENT_SPEECH - Bail: sConvoTextBlock is null")
			EXIT
		ELSE
			PRINTLN("PLAY_CARMOD_PED_AMBIENT_SPEECH sConvoTextBlock is: ", sConvoTextBlock)
		ENDIF
		
		IF (iConvoSpeakerID = -1)
			PRINTLN("PLAY_CARMOD_PED_AMBIENT_SPEECH - Bail: iConvoSpeakerID is -1")
			EXIT
		ELSE
			PRINTLN("PLAY_CARMOD_PED_AMBIENT_SPEECH iConvoSpeakerID is: ", iConvoSpeakerID)
		ENDIF
		
		structPedsForConversation dialogueStruct
		ADD_PED_FOR_DIALOGUE(dialogueStruct, iConvoSpeakerID, NULL, sVoiceName)
		SET_VOICE_LOCATION_FOR_NULL_PED(dialogueStruct, iConvoSpeakerID, vCoord)
		
		enumSubtitlesState eSubtitles = DO_NOT_DISPLAY_SUBTITLES
		IF SHOULD_CONVO_SPEECH_HAVE_SUBTITLES(eArcadeCabinet)
			eSubtitles = DISPLAY_SUBTITLES
			PRINTLN("PLAY_CARMOD_PED_AMBIENT_SPEECH eSubtitles: DISPLAY_SUBTITLES")
		ELSE
			PRINTLN("PLAY_CARMOD_PED_AMBIENT_SPEECH eSubtitles: DO_NOT_DISPLAY_SUBTITLES")
		ENDIF
		
		CREATE_CONVERSATION(dialogueStruct, sConvoTextBlock, Context, CONV_PRIORITY_MEDIUM, eSubtitles)
	ELSE
		PLAY_AMBIENT_SPEECH_FROM_POSITION(Context, sVoiceName, vCoord, Params)
		START_NET_TIMER(sACMData.tSpeechCoolDown)
	ENDIF
	
	PRINTLN("PLAY_CARMOD_PED_AMBIENT_SPEECH PLAYED")
ENDPROC

/// PURPOSE:
///    Process update arcade cabinet AI context event
PROC PROCESS_EVENT_ARCADE_CABINET_AI_CONTEXT(INT iCount)
	SCRIPT_EVENT_DATA_ARCADE_CABINET_AI_CONTEXT sEventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sEventData, SIZE_OF(sEventData))
		IF NETWORK_IS_PLAYER_A_PARTICIPANT(sEventData.Details.FromPlayerIndex)
			PLAY_CARMOD_PED_AMBIENT_SPEECH(sEventData.eArcadeCabinet, sEventData.sCabinetSlot, sEventData.stContext, sEventData.bUseConvo, sEventData.spParams)
		ELSE
			PRINTLN("[AM_ARC_CAB] - PROCESS_EVENT_ARCADE_CABINET_AI_CONTEXT - sender is not a participant")
		ENDIF
	ELSE
		SCRIPT_ASSERT("[AM_ARC_CAB] - PROCESS_EVENT_ARCADE_CABINET_AI_CONTEXT - could not retrieve data.")
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_VAULT_DRILL_ASSET_ARCADE(INT iEventID)
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
	
		SCRIPT_EVENT_DATA_VAULT_DRILL_ASSET Event
		
		PRINTLN("[FM ARCADE] - PROCESS_SCRIPT_EVENT_VAULT_DRILL_ASSET - has been received.")

		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
			IF Event.iEventType = ciEVENT_VAULT_DRILL_ASSET_REQUEST
			OR Event.iEventType = ciEVENT_VAULT_DRILL_ASSET_REQUEST_LASER	 
				SET_BIT(serverBD.iServerBS, ARCADE_CABINET_MANAGER_SERVER_BD_REQUEST_VAULT_DRILL_ASSETS)
				
				IF Event.iEventType = ciEVENT_VAULT_DRILL_ASSET_REQUEST_LASER
					SET_BIT(serverBD.iServerBS, ARCADE_CABINET_MANAGER_SERVER_BD_REQUEST_VAULT_DRILL_ASSETS_LASER)
				ENDIF
			ELSE
				CLEAR_BIT(serverBD.iServerBS, ARCADE_CABINET_MANAGER_SERVER_BD_REQUEST_VAULT_DRILL_ASSETS)
				CLEAR_BIT(serverBD.iServerBS, ARCADE_CABINET_MANAGER_SERVER_BD_REQUEST_VAULT_DRILL_ASSETS_LASER)
			ENDIF

			PRINTLN("[FM ARCADE] - PROCESS_SCRIPT_EVENT_VAULT_DRILL_ASSET - iEventType =  ", Event.iEventType)
		ENDIF
	ENDIF
ENDPROC

///PURPOSE: 2107736 - Requests the drilling audio banks for non-drilling players so they can hear the drilling sound effects.
///2152186 - Use the flag to request other drilling assets too.
PROC MANAGE_VAULT_DRILLING_ASSET_REQUESTS__ARCADE()
	IF IS_BIT_SET(serverBD.iServerBS, ARCADE_CABINET_MANAGER_SERVER_BD_REQUEST_VAULT_DRILL_ASSETS)
		IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, ARCADE_CABINET_MANAGER_PLAYER_BD_VAULT_DRILL_ASSETS_LOADED)
			IF REMOTE_ASSETS_REQUEST_AND_LOAD(IS_BIT_SET(serverBD.iServerBS, ARCADE_CABINET_MANAGER_SERVER_BD_REQUEST_VAULT_DRILL_ASSETS_LASER))
				SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, ARCADE_CABINET_MANAGER_PLAYER_BD_VAULT_DRILL_ASSETS_LOADED)
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, ARCADE_CABINET_MANAGER_PLAYER_BD_VAULT_DRILL_ASSETS_LOADED)
			REMOTE_CLEAR_ASSETS(IS_BIT_SET(serverBD.iServerBS, ARCADE_CABINET_MANAGER_SERVER_BD_REQUEST_VAULT_DRILL_ASSETS_LASER))
			CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, ARCADE_CABINET_MANAGER_PLAYER_BD_VAULT_DRILL_ASSETS_LOADED)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL ARE_ALL_ARCADE_CABINET_PROPS_UPDATED()
	ARCADE_CABINETS_SAVE_SLOT_TYPE eSaveSlotType
	ARCADE_CABINETS eArcadeCabinetType
	
	INT i = 0
	FOR i = 0 TO GET_ARCADE_SLOT_COUNT_FOR_PROPERTY(sACMData.eArcadeCabinetPropertyType) - 1
		eSaveSlotType =  GET_ARCADE_CABINET_SAVE_SLOT_TYPE(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner, INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, i), sACMData.eArcadeCabinetPropertyType)
		eArcadeCabinetType = GET_ARCADE_CABINET_FROM_DISPLAY_SLOT_TYPE(eSaveSlotType)
		IF eArcadeCabinetType != ARCADE_CABINET_INVALID
			IF (eArcadeCabinetType != sACMData.eArcadeCab[i]
			OR !DOES_ENTITY_EXIST(sACMData.arcadeCabinetProp[i]))
			AND !DOES_THIS_ARCADE_CABINETS_PROP_INCLUDED_IN_INTERIOR(eArcadeCabinetType)
				RETURN FALSE
			ENDIF
		ELSE
			IF eArcadeCabinetType != sACMData.eArcadeCab[i]
				RETURN FALSE
			ENDIF	
		ENDIF
	ENDFOR
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF NOT IS_BIT_SET(serverBD.iServerBS, ARCADE_CABINET_MANAGER_SERVER_BD_UPDATE_SUB_PROPS)
			SET_BIT(serverBD.iServerBS, ARCADE_CABINET_MANAGER_SERVER_BD_UPDATE_SUB_PROPS)
		ENDIF
	ENDIF	
	RETURN TRUE
ENDFUNC

PROC CLEAR_TERMINATE_FLAGS()
	INT i
	FOR i = 0 TO GET_ARCADE_SLOT_COUNT_FOR_PROPERTY(sACMData.eArcadeCabinetPropertyType) - 1
		CLEAR_ARCADE_CABINET_MANAGER_BIT(i, ARCADE_CABINET_MANAGER_TERMINATE_CHILD_SCRIPT)
	ENDFOR
ENDPROC

FUNC BOOL SHOULD_SKIP_ARCADE_CABINET_PROP_UPDATE(INT iArcadeCabinetSlot)
	IF IS_PLAYER_USING_ARCADE_CABINET(PLAYER_ID())
		IF GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID()) = iArcadeCabinetSlot
			RETURN TRUE
		ENDIF
		
		IF GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID()) = ENUM_TO_INT(GET_ARCADE_CABINET_NEIGHBOUR_SLOT(INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, iArcadeCabinetSlot)))
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PROCESS_UPDATE_ARCADE_CABINET_PROPS()
	IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = INVALID_PLAYER_INDEX()
		EXIT
	ENDIF	

	IF IS_BIT_SET(g_ArcadeCabinetManagerData.iBS, ARC_CAB_MANAGER_GLOBAL_BIT_SET_UPDATE_PROPS)	
		ARCADE_CABINETS_SAVE_SLOT_TYPE eSaveSlotType
		ARCADE_CABINETS eArcadeCabinetType
		VECTOR vCoord
		FLOAT fHeading
		
		BOOL bTerminateScript = FALSE
		INT iStartLoop, iEndLoop, iLoop, iStreetCrimeIndex
		INT i = 0
		FOR i = 0 TO GET_ARCADE_SLOT_COUNT_FOR_PROPERTY(sACMData.eArcadeCabinetPropertyType) - 1
			eSaveSlotType =  GET_ARCADE_CABINET_SAVE_SLOT_TYPE(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner, INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, i), sACMData.eArcadeCabinetPropertyType)
			eArcadeCabinetType = GET_ARCADE_CABINET_FROM_DISPLAY_SLOT_TYPE(eSaveSlotType)
			PRINTLN("[AM_ARC_CAB] PROCESS_UPDATE_ARCADE_CABINET_PROPS Slot: ", i, "Type : ", GET_ARCADE_CABINET_NAME(eArcadeCabinetType), " save slot type: ", GET_ARCADE_CABINET_SAVE_SLOT_TYPE_NAME(eSaveSlotType)
					, " local cabinet type: ", GET_ARCADE_CABINET_NAME(sACMData.eArcadeCab[i]))
			
			sACMData.strCabinetChildScript = ""
			
			
			IF SHOULD_SKIP_ARCADE_CABINET_PROP_UPDATE(i)
				// skip
			ELSE	
			
				IF sACMData.eArcadeCab[i] != ARCADE_CABINET_INVALID
					IF DOES_ARCADE_CABINET_USE_CHILD_SCRIPT(sACMData.eArcadeCab[i], sACMData.strCabinetChildScript, FALSE)
						IF NOT IS_STRING_NULL_OR_EMPTY(sACMData.strCabinetChildScript)
							bTerminateScript = TRUE
							PRINTLN("[AM_ARC_CAB] PROCESS_UPDATE_ARCADE_CABINET_PROPS bTerminateScript true ", i)
						ENDIF
						
						IF IS_PLAYER_USING_ARCADE_CABINET(PLAYER_ID())
							IF GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID()) = sACMData.eArcadeCab[i]
								bTerminateScript = FALSE
								PRINTLN("[AM_ARC_CAB] PROCESS_UPDATE_ARCADE_CABINET_PROPS bTerminateScript false player is using this cabinet: ", i)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF bTerminateScript
					IF NOT IS_ARCADE_CABINET_MANAGER_BIT_SET(i, ARCADE_CABINET_MANAGER_TERMINATE_CHILD_SCRIPT)
						
						IF NOT IS_STRING_NULL_OR_EMPTY(sACMData.strCabinetChildScript)
							IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(sACMData.strCabinetChildScript)) > 0
								IF IS_THIS_PHYSICAL_ARCADE_CABINET(sACMData.eArcadeCab[i])
									IF NOT SHOULD_TERMINATE_PHYSICAL_ARDCADE_GAME(sACMData.eArcadeCab[i])
										CLEAR_ARCADE_CABINET_MANAGER_BIT(i, ARCADE_CABINET_MANAGER_STARTED_CHILD_SCRIPT)
										SET_ARCADE_CABINET_MANAGER_BIT(i, ARCADE_CABINET_MANAGER_TERMINATE_CHILD_SCRIPT)
										SET_TERMINATE_PHYSICAL_ARCADE_GAME_FLAG(sACMData.eArcadeCab[i], TRUE)
										PRINTLN("[AM_ARC_CAB] PROCESS_UPDATE_ARCADE_CABINET_PROPS SET ", GET_ARCADE_CABINET_TERMINATE_BIT_NAME(sACMData.eArcadeCab[i]))
									ENDIF	
								ELSE
									TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME(sACMData.strCabinetChildScript)
									CLEAR_ARCADE_CABINET_MANAGER_BIT(i, ARCADE_CABINET_MANAGER_STARTED_CHILD_SCRIPT)
									SET_ARCADE_CABINET_MANAGER_BIT(i, ARCADE_CABINET_MANAGER_TERMINATE_CHILD_SCRIPT)
								ENDIF	
							ENDIF	
						ENDIF	
					ELSE
						IF NOT IS_STRING_NULL_OR_EMPTY(sACMData.strCabinetChildScript)
							IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(sACMData.strCabinetChildScript)) < 1
							AND NOT NETWORK_IS_SCRIPT_ACTIVE(sACMData.strCabinetChildScript, NETWORK_GET_INSTANCE_ID_OF_THIS_SCRIPT(), TRUE)
								IF IS_THIS_PHYSICAL_ARCADE_CABINET(sACMData.eArcadeCab[i])
									IF SHOULD_TERMINATE_PHYSICAL_ARDCADE_GAME(sACMData.eArcadeCab[i])
										SET_TERMINATE_PHYSICAL_ARCADE_GAME_FLAG(sACMData.eArcadeCab[i], FALSE)
										PRINTLN("[AM_ARC_CAB] PROCESS_UPDATE_ARCADE_CABINET_PROPS CLEAR ", GET_ARCADE_CABINET_TERMINATE_BIT_NAME(sACMData.eArcadeCab[i]))
									ENDIF	
								ENDIF
							ENDIF
						ENDIF	
					ENDIF	
				ENDIF
				
				IF eArcadeCabinetType != ARCADE_CABINET_CA_STREET_CRIMES
					IF eArcadeCabinetType != ARCADE_CABINET_INVALID
					OR !DOES_ENTITY_EXIST(sACMData.arcadeCabinetProp[i])
						IF eArcadeCabinetType != sACMData.eArcadeCab[i]
						OR !DOES_ENTITY_EXIST(sACMData.arcadeCabinetProp[i])
							IF DOES_ENTITY_EXIST(sACMData.arcadeCabinetProp[i])	
								DELETE_OBJECT(sACMData.arcadeCabinetProp[i])
								sACMData.eArcadeCab[i] = ARCADE_CABINET_INVALID
							ELSE
								sACMData.eArcadeCab[i] = eArcadeCabinetType
								GET_ARCADE_CABINET_DISPLAY_SLOT_COORD_AND_HEADING(sACMData.eArcadeCabinetPropertyType, INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, i), ARCADE_CABINET_INVALID, vCoord, fHeading)
					
								IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vCoord) < 1.5
									IF !IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, ARCADE_CABINET_MANAGER_PLAYER_BD_MOVE_OUT_OF_WAY)
										VECTOR vTargetCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoord, fHeading, <<0, -1.5, 0>>)
										TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), vTargetCoords, PEDMOVEBLENDRATIO_WALK, 7000, DEFAULT, 0.01)
										SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, ARCADE_CABINET_MANAGER_PLAYER_BD_MOVE_OUT_OF_WAY)
										PRINTLN("[AM_ARC_CAB] - PROCESS_UPDATE_ARCADE_CABINET_PROPS - ARCADE_CABINET_MANAGER_PLAYER_BD_MOVE_OUT_OF_WAY")
									ENDIF	
								ELSE
									CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, ARCADE_CABINET_MANAGER_PLAYER_BD_MOVE_OUT_OF_WAY)
								ENDIF
								
								IF !IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, ARCADE_CABINET_MANAGER_PLAYER_BD_MOVE_OUT_OF_WAY)
									IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> PERFORMING_TASK
									AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> WAITING_TO_START_TASK
										CREATE_ARCADE_CABINETS(sACMData, i)
									ENDIF	
								ENDIF	
							ENDIF
						ELSE
							PRINTLN("[AM_ARC_CAB] PROCESS_UPDATE_ARCADE_CABINET_PROPS Slot: ", i, " equal")
						ENDIF
					ELSE
						IF DOES_ENTITY_EXIST(sACMData.arcadeCabinetProp[i])
							DELETE_OBJECT(sACMData.arcadeCabinetProp[i])
							sACMData.eArcadeCab[i] = ARCADE_CABINET_INVALID
						ENDIF	
					ENDIF	
				ELSE
					IF eArcadeCabinetType != sACMData.eArcadeCab[i]
					OR (!DOES_ENTITY_EXIST(sACMData.arcadeCabinetProp[i]) AND eArcadeCabinetType = sACMData.eArcadeCab[i]) 
						GET_FOUR_PLAYER_ARCADE_CABINET_START_AND_END_SLOT(INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, i), iStartLoop, iEndLoop)
						FOR iLoop = iStartLoop TO iEndLoop

							IF eArcadeCabinetType = ARCADE_CABINET_CA_STREET_CRIMES
								iStreetCrimeIndex++
								IF DOES_ENTITY_EXIST(sACMData.arcadeCabinetProp[iLoop])
								AND sACMData.eArcadeCab[iLoop] != ARCADE_CABINET_CA_STREET_CRIMES
									DELETE_OBJECT(sACMData.arcadeCabinetProp[iLoop])
								ENDIF
								
								sACMData.eArcadeCab[iLoop] = ARCADE_CABINET_CA_STREET_CRIMES
							
								IF !DOES_ENTITY_EXIST(sACMData.arcadeCabinetProp[iLoop])
								AND sACMData.eArcadeCab[iLoop] = ARCADE_CABINET_CA_STREET_CRIMES
								
									GET_ARCADE_CABINET_DISPLAY_SLOT_COORD_AND_HEADING(sACMData.eArcadeCabinetPropertyType, INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, iLoop), ARCADE_CABINET_INVALID, vCoord, fHeading)
									
									IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vCoord) < 1.5
										IF !IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, ARCADE_CABINET_MANAGER_PLAYER_BD_MOVE_OUT_OF_WAY)
											VECTOR vTargetCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoord, fHeading, <<0, -1.5, 0>>)
											TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), vTargetCoords, PEDMOVEBLENDRATIO_WALK, 7000, DEFAULT, 0.01)
											SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, ARCADE_CABINET_MANAGER_PLAYER_BD_MOVE_OUT_OF_WAY)
											PRINTLN("[AM_ARC_CAB] - PROCESS_UPDATE_ARCADE_CABINET_PROPS - ARCADE_CABINET_MANAGER_PLAYER_BD_MOVE_OUT_OF_WAY street crime")
										ENDIF	
									ELSE
										CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, ARCADE_CABINET_MANAGER_PLAYER_BD_MOVE_OUT_OF_WAY)
									ENDIF
									
									IF !IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, ARCADE_CABINET_MANAGER_PLAYER_BD_MOVE_OUT_OF_WAY)
										IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> PERFORMING_TASK
										AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> WAITING_TO_START_TASK
											CREATE_ARCADE_CABINETS(sACMData, iLoop, iStreetCrimeIndex)
										ENDIF	
									ENDIF	
								ENDIF	
							ENDIF
						ENDFOR
					ENDIF	
				ENDIF	
			ENDIF	
		ENDFOR

		IF ARE_ALL_ARCADE_CABINET_PROPS_UPDATED()
		AND MAINTAIN_ACM_INIT_CHILD_SCRIPT_STATE()
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				IF NOT IS_BIT_SET(serverBD.iServerBS, ARCADE_CABINET_MANAGER_SERVER_BD_SET_SUB_PROP_TO_VISIBLE)
					SET_BIT(serverBD.iServerBS, ARCADE_CABINET_MANAGER_SERVER_BD_SET_SUB_PROP_TO_VISIBLE)
					PRINTLN("[AM_ARC_CAB] - PROCESS_UPDATE_ARCADE_CABINET_PROPS ARCADE_CABINET_MANAGER_SERVER_BD_SET_SUB_PROP_TO_VISIBLE true")
				ENDIF
			ENDIF	
			CLEAR_TERMINATE_FLAGS()
			CLEAR_BIT(g_ArcadeCabinetManagerData.iBS, ARC_CAB_MANAGER_GLOBAL_BIT_SET_UPDATE_PROPS)	
			PRINTLN("[AM_ARC_CAB] - PROCESS_UPDATE_ARCADE_CABINET_PROPS ARC_CAB_MANAGER_GLOBAL_BIT_SET_UPDATE_PROPS FALSE")
		ENDIF	
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintain arcade cabinet manager events 
PROC MAINTAIN_ARCADE_CABINET_EVENTS()
	INT iCount
	EVENT_NAMES ThisScriptEvent
	STRUCT_EVENT_COMMON_DETAILS Details
	
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount

	    ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)
	   	
	    SWITCH ThisScriptEvent
			CASE EVENT_NETWORK_SCRIPT_EVENT
				
				GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Details, SIZE_OF(Details))
				
				SWITCH Details.Type
					CASE SCRIPT_EVENT_PLAY_ARCADE_CABINET_ANIM
						IF g_sBlockedEvents.bSCRIPT_EVENT_PLAY_ARCADE_CABINET_ANIM
							EXIT
						ENDIF
						PROCESS_EVENT_ARCADE_CABINET_ANIMATION(iCount)
					BREAK
					CASE SCRIPT_EVENT_UPDATE_ARCADE_CABINET_PROPS
						IF g_sBlockedEvents.bSCRIPT_EVENT_UPDATE_ARCADE_CABINET_PROPS
							EXIT
						ENDIF
						PROCESS_EVENT_UPADTE_ARCADE_CABINET_PROPS(iCount)
					BREAK	
					CASE SCRIPT_EVENT_PLAY_ARCADE_CABINET_AI_CONTEXT
						IF g_sBlockedEvents.bSCRIPT_EVENT_PLAY_ARCADE_CABINET_AI_CONTEXT
							EXIT
						ENDIF
						PROCESS_EVENT_ARCADE_CABINET_AI_CONTEXT(iCount)
					BREAK
					CASE SCRIPT_EVENT_VAULT_DRILL_ASSET_REQUEST_EVENT
						PRINTLN("[AM_ARC_CAB] - SCRIPT_EVENT_VAULT_DRILL_ASSET_REQUEST_EVENT Has been called")
						IF g_sBlockedEvents.bSCRIPT_EVENT_VAULT_DRILL_ASSET_BLOCKED
							PRINTLN("[AM_ARC_CAB] - SCRIPT_EVENT_VAULT_DRILL_ASSET_REQUEST_EVENT BLOCKED")
							EXIT
						ENDIF
						PROCESS_SCRIPT_EVENT_VAULT_DRILL_ASSET_ARCADE(iCount)
					BREAK
					CASE SCRIPT_EVENT_VAULT_DRILL_EFFECT_EVENT
						IF g_sBlockedEvents.bSCRIPT_EVENT_VAULT_DRILL_EFFECT_EVENT_BLOCKED
							EXIT
						ENDIF
						PROCESS_SCRIPT_EVENT_VAULT_DRILL_EFFECT_EVENT__ARCADE(iCount)	
					BREAK
				ENDSWITCH	
			BREAK
	    ENDSWITCH
    ENDREPEAT
ENDPROC

/// PURPOSE:
///   update participants stagger
PROC UPDATE_ARCADE_CABINET_PLAYER_STAGGER()
	sACMData.iPlayerStagger = (sACMData.iPlayerStagger + 1) % NUM_NETWORK_PLAYERS
ENDPROC

/// PURPOSE:
///    update stagger number of player for each arcade cabinet - some of arcade cabinets have more than 1 player e.g. night drive
PROC UPDATE_ARCADE_CABINET_NUM_PLAYER_ALLOWED_STAGGER()
	sACMData.iArcadeCabinetNumPlayerStagger = (sACMData.iArcadeCabinetNumPlayerStagger + 1) % ENUM_TO_INT(ARCADE_CAB_MAX_NUM_PLAYERS)
ENDPROC

/// PURPOSE:
///    Start game scripts
PROC MAINTAIN_ACM_LAUNCH_GAME_SCRIPT_STATE()
	DISABLE_FRONTEND_THIS_FRAME()
	
	INT iDisplaySlotLocatePlayerIsIn 
	iDisplaySlotLocatePlayerIsIn = GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID())
	IF iDisplaySlotLocatePlayerIsIn != -1 
		IF MAINTAIN_LAUNCH_GAME_SCRIPT(sACMData.eArcadeCab[iDisplaySlotLocatePlayerIsIn], GET_ARCADE_CABINET_GROUP(INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, iDisplaySlotLocatePlayerIsIn)), sACMData.eArcadeCabinetPropertyType)
			SET_PLAYER_READY_TO_PLAY_ARCADE_GAME(TRUE)
			START_GAME_AUDIO_SCENE()
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_PREVENT_VISIBILITY_CHANGES | NSPC_LEAVE_CAMERA_CONTROL_ON)
			SET_PLAYER_CONFIG_FLAG_KINEMATIC_MODE_WHEN_STATIONARY(TRUE)
			SET_ARCADE_CABINET_MANAGER_STATE(ARCADE_CABINET_IN_USE_ANIM_STATE)
			IF NOT IS_THIS_ARCADE_FOR_A_MINI_GAME(sACMData.eArcadeCab[iDisplaySlotLocatePlayerIsIn])
				SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_PLAY_ARCADE_GAME)
			ENDIF
		ENDIF
	ENDIF	
ENDPROC

/// PURPOSE:
///    Start arcade sounds
PROC START_ARCADE_CABINET_SOUND(BOOL bInUse)
	TEXT_LABEL_63 tlSoundSet, tlSoundName
	IF bInUse
		IF sACMData.iArcadeTypeIndex[ENUM_TO_INT(GET_ARCADE_CABINET_SOUND_AREA_IN_SLOT(INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, sACMData.iArcadeCabinetStagger)))] = sACMData.iArcadeCabinetStagger
		//OR IS_THIS_PHYSICAL_ARCADE_CABINET(sACMData.eArcadeCab[sACMData.iArcadeCabinetStagger])
			GET_ARCADE_CABINET_SOUND_DETAIL(sACMData.eArcadeCab[sACMData.iArcadeCabinetStagger], tlSoundSet, tlSoundName, FALSE)
			IF sACMData.iIdleSoundID[sACMData.iArcadeCabinetStagger] != -1
				STOP_SOUND(sACMData.iIdleSoundID[sACMData.iArcadeCabinetStagger])
				RELEASE_SOUND_ID(sACMData.iIdleSoundID[sACMData.iArcadeCabinetStagger])
				sACMData.iIdleSoundID[sACMData.iArcadeCabinetStagger] = -1
				CLEAR_BIT(sACMData.iSoundIndexBS, ENUM_TO_INT(GET_ARCADE_CABINET_SOUND_AREA_IN_SLOT(INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, sACMData.iArcadeCabinetStagger))))
				CLEAR_BIT(sACMData.iArcadeTypeSound, ENUM_TO_INT(sACMData.eArcadeCab[sACMData.iArcadeCabinetStagger]))
			ENDIF
		ENDIF	
			
		GET_ARCADE_CABINET_SOUND_DETAIL(sACMData.eArcadeCab[sACMData.iArcadeCabinetStagger], tlSoundSet, tlSoundName, TRUE)
		IF sACMData.iInUseSoundID[sACMData.iArcadeCabinetStagger] = -1
		AND NOT IS_STRING_NULL_OR_EMPTY(tlSoundName)
			sACMData.iInUseSoundID[sACMData.iArcadeCabinetStagger] = GET_SOUND_ID()
			PLAY_SOUND_FROM_COORD(sACMData.iInUseSoundID[sACMData.iArcadeCabinetStagger], tlSoundName, sACMData.vArcadeCabCoords[sACMData.iArcadeCabinetStagger], tlSoundSet)
			PRINTLN("[AM_ARC_CAB] - MAINTAIN_ARCADE_CABINET_SOUNDS - play using sound from ", sACMData.vArcadeCabCoords[sACMData.iArcadeCabinetStagger], ":", sACMData.iInUseSoundID[sACMData.iArcadeCabinetStagger]
						, ", \"", tlSoundName, "\", \"dlc_vw_am_cabinet_sounds\"")
		ENDIF	
	ELSE
		GET_ARCADE_CABINET_SOUND_DETAIL(sACMData.eArcadeCab[sACMData.iArcadeCabinetStagger], tlSoundSet, tlSoundName, TRUE)
		IF sACMData.iInUseSoundID[sACMData.iArcadeCabinetStagger] != -1
			STOP_SOUND(sACMData.iInUseSoundID[sACMData.iArcadeCabinetStagger])
			RELEASE_SOUND_ID(sACMData.iInUseSoundID[sACMData.iArcadeCabinetStagger])
			sACMData.iInUseSoundID[sACMData.iArcadeCabinetStagger] = -1
		ENDIF
		
		IF sACMData.iArcadeTypeIndex[ENUM_TO_INT(GET_ARCADE_CABINET_SOUND_AREA_IN_SLOT(INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, sACMData.iArcadeCabinetStagger)))] = sACMData.iArcadeCabinetStagger	
		//OR IS_THIS_PHYSICAL_ARCADE_CABINET(sACMData.eArcadeCab[sACMData.iArcadeCabinetStagger])
			GET_ARCADE_CABINET_SOUND_DETAIL(sACMData.eArcadeCab[sACMData.iArcadeCabinetStagger], tlSoundSet, tlSoundName, FALSE)
			IF NOT IS_STRING_NULL_OR_EMPTY(tlSoundName)
				IF sACMData.iIdleSoundID[sACMData.iArcadeCabinetStagger] = -1
					
					sACMData.iIdleSoundID[sACMData.iArcadeCabinetStagger] = GET_SOUND_ID()
					PLAY_SOUND_FROM_COORD(sACMData.iIdleSoundID[sACMData.iArcadeCabinetStagger], tlSoundName, sACMData.vArcadeCabCoords[sACMData.iArcadeCabinetStagger], tlSoundSet, FALSE, 20)
					PRINTLN("[AM_ARC_CAB] - MAINTAIN_ARCADE_CABINET_SOUNDS - play not in use sound from ", sACMData.vArcadeCabCoords[sACMData.iArcadeCabinetStagger], ":", sACMData.iIdleSoundID[sACMData.iArcadeCabinetStagger]
								, ", \"", tlSoundName, "\", \"dlc_vw_am_cabinet_sounds\"")
				ELSE
					IF HAS_SOUND_FINISHED(sACMData.iIdleSoundID[sACMData.iArcadeCabinetStagger])
						RELEASE_SOUND_ID(sACMData.iIdleSoundID[sACMData.iArcadeCabinetStagger])
						sACMData.iIdleSoundID[sACMData.iArcadeCabinetStagger] = -1
						CLEAR_BIT(sACMData.iSoundIndexBS, ENUM_TO_INT(GET_ARCADE_CABINET_SOUND_AREA_IN_SLOT(INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, sACMData.iArcadeCabinetStagger))))
						CLEAR_BIT(sACMData.iArcadeTypeSound, ENUM_TO_INT(sACMData.eArcadeCab[sACMData.iArcadeCabinetStagger]))
						PRINTLN("[AM_ARC_CAB] - START_ARCADE_CABINET_SOUND UPDATE_ARCADE_SOUND_INDEX sACMData.iSoundIndexBS "
								, GET_ARCADE_CABINET_SOUND_AREA_NAME(GET_ARCADE_CABINET_SOUND_AREA_IN_SLOT(INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, sACMData.iArcadeCabinetStagger))), " FALSE stagger: ", sACMData.iArcadeCabinetStagger)
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDIF	
ENDPROC

/// PURPOSE:
///    Depending on arcade cabinet game state start idle / in use sound
PROC MAINTAIN_ARCADE_CABINET_SOUND()
	IF IS_LOCAL_PLAYER_USING_ARCADE_MANAGEMENT_MENU()
		EXIT
	ENDIF	
	
	IF IS_LOCAL_PLAYER_PED_IN_ARCADE_ROOM(ARC_RM_BASEMENT)
	AND IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
		IF !IS_LOCAL_ARCADE_CABINET_MANAGER_BIT_SET(ARCADE_CABINET_MANAGER_LOCAL_BS_STOP_ALL_CABINET_SOUNDS)
			STOP_ALL_ARCADE_CABINET_SOUNDS(sACMData)
			SET_ARCADE_CABINET_MANAGER_LOCAL_BIT(ARCADE_CABINET_MANAGER_LOCAL_BS_STOP_ALL_CABINET_SOUNDS)
		ENDIF	
		EXIT
	ELSE
		IF IS_LOCAL_ARCADE_CABINET_MANAGER_BIT_SET(ARCADE_CABINET_MANAGER_LOCAL_BS_STOP_ALL_CABINET_SOUNDS)
			CLEAR_ARCADE_CABINET_MANAGER_LOCAL_BIT(ARCADE_CABINET_MANAGER_LOCAL_BS_STOP_ALL_CABINET_SOUNDS)
		ENDIF	
	ENDIF
	
	IF sACMData.eArcadeCab[sACMData.iArcadeCabinetStagger] != ARCADE_CABINET_INVALID
	AND DOES_ENTITY_EXIST(sACMData.arcadeCabinetProp[sACMData.iArcadeCabinetStagger])
		INT iNumPlayers = GET_NUM_PLAYERS_ARCADE_CABINET_CAN_HAVE(sACMData.eArcadeCab[sACMData.iArcadeCabinetStagger])
		INT iPlayerSlot, iPlayerUsingCabinet
		BOOL bInUse = TRUE
		FOR iPlayerSlot = 0 TO iNumPlayers
			iPlayerUsingCabinet = GET_PLAYER_INDEX_USING_THIS_ARCADE_CABINET_DISPLAY_SLOT(serverBD, sACMData.iArcadeCabinetStagger, iPlayerSlot, sACMData.eArcadeCabinetPropertyType)
			
			IF sACMData.eArcadeCabinetManagerState = ARCADE_CABINET_CLEANUP_EXIT_STATE 
			OR sACMData.eArcadeCabinetManagerState = ARCADE_CABINET_CLEANUP_STATE
				bInUse = FALSE
				BREAKLOOP
			ENDIF	
			
			IF IS_PLAYER_READY_TO_PLAY_ARCADE_GAME(INT_TO_PLAYERINDEX(iPlayerUsingCabinet))
				bInUse = TRUE
				BREAKLOOP
			ELSE
				bInUse = FALSE
			ENDIF	
		ENDFOR
		
		START_ARCADE_CABINET_SOUND(bInUse)
	ENDIF
ENDPROC

PROC MAINTAIN_ARCADE_CABINETS_IN_USE_SCREENS()
	IF sACMData.eArcadeCab[sACMData.iArcadeCabinetStagger] != ARCADE_CABINET_INVALID
		BOOL bInUse = TRUE
		INT iNumPlayers = GET_NUM_PLAYERS_ARCADE_CABINET_CAN_HAVE(sACMData.eArcadeCab[sACMData.iArcadeCabinetStagger])
		INT iPlayerSlot
		FOR iPlayerSlot = 0 TO iNumPlayers
			bInUse = TRUE
			IF GET_PLAYER_INDEX_USING_THIS_ARCADE_CABINET_DISPLAY_SLOT(serverBD, sACMData.iArcadeCabinetStagger, iPlayerSlot, sACMData.eArcadeCabinetPropertyType) = -1
			OR IS_LOCAL_PLAYER_USING_ARCADE_MANAGEMENT_MENU()
			OR IS_PLAYER_USING_ARCADE_CABINET(PLAYER_ID())
				bInUse = FALSE
			ENDIF	
			
			#IF IS_DEBUG_BUILD
			IF g_DebugArcadeCabinetManagerData.bSpawnScreenProp
				bInUse = TRUE
			ENDIF
			#ENDIF
			
			IF bInUse
				CREATE_ARCADE_CABINETS_SCREENS(sACMData, sACMData.iArcadeCabinetStagger, iPlayerSlot)
			ELSE
				REMOVE_ARCADE_CABINETS_SCREEN_PROP(sACMData, sACMData.iArcadeCabinetStagger, iPlayerSlot)
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

FUNC INT GET_ARCADE_ROOM_KEY(ARCADE_CABINETS arcadeCabinet, ARCADE_CABINET_PROPERTY eProperty)
	#IF FEATURE_FIXER
	IF eProperty = ACP_FIXER_HQ
		SWITCH arcadeCabinet
			CASE ARCADE_CABINET_CH_GG_SPACE_MONKEY
				RETURN 1122875644
			BREAK
			CASE ARCADE_CABINET_CH_WIZARDS_SLEVE
				RETURN -111647487
			BREAK
		ENDSWITCH
	ENDIF
	#ENDIF

    RETURN sACMData.iRoomKey
ENDFUNC

FUNC BOOL SHOULD_FORCE_ROOM_FOR_THIS_ARCADE_CABINET(ARCADE_CABINETS arcadeCabinet, ARCADE_CABINET_PROPERTY eProperty)
	#IF FEATURE_FIXER
	IF eProperty = ACP_FIXER_HQ
		SWITCH arcadeCabinet
			CASE ARCADE_CABINET_CH_GG_SPACE_MONKEY
			CASE ARCADE_CABINET_CH_WIZARDS_SLEVE
				IF sACMData.iRoomKey != g_sShopSettings.playerRoom
					RETURN TRUE
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	#ENDIF

	SWITCH arcadeCabinet
		CASE ARCADE_CABINET_CH_DRILLING_LASER_MINI_G
			RETURN FALSE
		BREAK	
	ENDSWITCH
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Force cabinet props to room
PROC FORCE_ROOM_FOR_ARCADE_CABINET_PROPS()
	IF sACMData.eArcadeCab[sACMData.iArcadeCabinetStagger] != ARCADE_CABINET_INVALID
	AND DOES_ENTITY_EXIST(sACMData.arcadeCabinetProp[sACMData.iArcadeCabinetStagger])
		IF SHOULD_FORCE_ROOM_FOR_THIS_ARCADE_CABINET(sACMData.eArcadeCab[sACMData.iArcadeCabinetStagger], sACMData.eArcadeCabinetPropertyType)
			IF IS_VALID_INTERIOR(GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()))
			AND IS_INTERIOR_READY(GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()))
				IF sACMData.iRoomKey = GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID())
					IF IS_SIMPLE_INTERIOR_CHILD_SCRIPT_READY()
						IF NOT IS_ARCADE_CABINET_MANAGER_BIT_SET(sACMData.iArcadeCabinetStagger, ARCADE_CABINET_MANAGER_FORCE_TO_ROOM)
							FORCE_ROOM_FOR_ENTITY(sACMData.arcadeCabinetProp[sACMData.iArcadeCabinetStagger], GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()), GET_ARCADE_ROOM_KEY(sACMData.eArcadeCab[sACMData.iArcadeCabinetStagger], sACMData.eArcadeCabinetPropertyType))
							SET_ARCADE_CABINET_MANAGER_BIT(sACMData.iArcadeCabinetStagger, ARCADE_CABINET_MANAGER_FORCE_TO_ROOM)
						ENDIF
					ENDIF
				ENDIF	
			ENDIF
		ENDIF	
	ENDIF	
	
	IF IS_PLAYER_ARCADE_PROPERTY_RENOVATION_ACTIVE(PLAYER_ID())
		IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, ARCADE_CABINET_MANAGER_PLAYER_BD_RENOVATE_ARCADE)
			SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, ARCADE_CABINET_MANAGER_PLAYER_BD_RENOVATE_ARCADE)
			PRINTLN("[AM_ARC_CAB] - FORCE_ROOM_FOR_ARCADE_CABINET_PROPS ARCADE_CABINET_MANAGER_PLAYER_BD_RENOVATE_ARCADE TRUE")
		ENDIF
	ELSE
		IF IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, ARCADE_CABINET_MANAGER_PLAYER_BD_RENOVATE_ARCADE)
			INT i
			FOR i = 0 TO GET_ARCADE_SLOT_COUNT_FOR_PROPERTY(sACMData.eArcadeCabinetPropertyType) - 1
				CLEAR_ARCADE_CABINET_MANAGER_BIT(i, ARCADE_CABINET_MANAGER_FORCE_TO_ROOM)
			ENDFOR
			CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, ARCADE_CABINET_MANAGER_PLAYER_BD_RENOVATE_ARCADE)
			PRINTLN("[AM_ARC_CAB] - FORCE_ROOM_FOR_ARCADE_CABINET_PROPS ARCADE_CABINET_MANAGER_PLAYER_BD_RENOVATE_ARCADE FALSE")
		ENDIF
	ENDIF
ENDPROC

// Hide local props when player is in cutscene
PROC HIDE_ARCADE_CABINETS_FOR_NET_CUTSCENE()
	IF sACMData.eArcadeCab[sACMData.iArcadeCabinetStagger] != ARCADE_CABINET_INVALID
		IF NOT IS_BIT_SET(g_SimpleInteriorData.iSeventhBS, BS7_SIMPLE_INTERIOR_ENTERING_INTERIOR_FOR_POST_HEIST_MOCAP)
		AND NOT IS_BIT_SET(g_SimpleInteriorData.iSeventhBS, BS7_SIMPLE_INTERIOR_ENTERING_PLAYING_POST_HEIST_MOCAP)
		AND NOT IS_BIT_SET(g_SimpleInteriorData.iTenthBS, BS10_SIMPLE_INTERIOR_LOCAL_PLAYER_RUNNING_FIXER_HQ_MOCAP)
		AND NOT NETWORK_IS_ACTIVITY_SESSION()
			IF NOT IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
			OR IS_PLAYER_LOCALLY_RUNNING_ARCADE_MOCAP()
				IF NETWORK_IS_IN_MP_CUTSCENE()
					IF DOES_ENTITY_EXIST(sACMData.arcadeCabinetProp[sACMData.iArcadeCabinetStagger])
						IF NOT IS_ARCADE_CABINET_MANAGER_BIT_SET(sACMData.iArcadeCabinetStagger, ARCADE_CABINET_MANAGER_HIDE_CABINETS_FOR_NET_CUT_SCENE)
							IF IS_ENTITY_VISIBLE(sACMData.arcadeCabinetProp[sACMData.iArcadeCabinetStagger])
								SET_ENTITY_VISIBLE(sACMData.arcadeCabinetProp[sACMData.iArcadeCabinetStagger], FALSE)
								SET_ARCADE_CABINET_MANAGER_BIT(sACMData.iArcadeCabinetStagger, ARCADE_CABINET_MANAGER_HIDE_CABINETS_FOR_NET_CUT_SCENE)
							ENDIF
						ENDIF
					ELSE
						CLEAR_ARCADE_CABINET_MANAGER_BIT(sACMData.iArcadeCabinetStagger, ARCADE_CABINET_MANAGER_HIDE_CABINETS_FOR_NET_CUT_SCENE)
					ENDIF
				ELSE
					IF IS_ARCADE_CABINET_MANAGER_BIT_SET(sACMData.iArcadeCabinetStagger, ARCADE_CABINET_MANAGER_HIDE_CABINETS_FOR_NET_CUT_SCENE)
						IF DOES_ENTITY_EXIST(sACMData.arcadeCabinetProp[sACMData.iArcadeCabinetStagger])
							IF !IS_ENTITY_VISIBLE(sACMData.arcadeCabinetProp[sACMData.iArcadeCabinetStagger])
								SET_ENTITY_VISIBLE(sACMData.arcadeCabinetProp[sACMData.iArcadeCabinetStagger], TRUE)
								CLEAR_ARCADE_CABINET_MANAGER_BIT(sACMData.iArcadeCabinetStagger, ARCADE_CABINET_MANAGER_HIDE_CABINETS_FOR_NET_CUT_SCENE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF	
		ENDIF	
		
		IF IS_BIT_SET(g_ArcadeCabinetManagerData.iBS, ARC_CAB_MANAGER_GLOBAL_BIT_SET_HIDE_CABINETS_NEXT_TO_PILLAR)
			ARCADE_CAB_MANAGER_SLOT eSlot = INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, sACMData.iArcadeCabinetStagger)
			IF eSlot = ACM_SLOT_20
			OR eSlot = ACM_SLOT_21
				IF DOES_ENTITY_EXIST(sACMData.arcadeCabinetProp[sACMData.iArcadeCabinetStagger])
					IF NOT IS_ARCADE_CABINET_MANAGER_BIT_SET(sACMData.iArcadeCabinetStagger, ARCADE_CABINET_MANAGER_HIDE_CABINETS_NEXT_TO_PILLAR)
						IF IS_ENTITY_VISIBLE(sACMData.arcadeCabinetProp[sACMData.iArcadeCabinetStagger])
							SET_ENTITY_VISIBLE(sACMData.arcadeCabinetProp[sACMData.iArcadeCabinetStagger], FALSE)
							SET_ARCADE_CABINET_MANAGER_BIT(sACMData.iArcadeCabinetStagger, ARCADE_CABINET_MANAGER_HIDE_CABINETS_NEXT_TO_PILLAR)
						ENDIF
					ENDIF
				ELSE
					CLEAR_ARCADE_CABINET_MANAGER_BIT(sACMData.iArcadeCabinetStagger, ARCADE_CABINET_MANAGER_HIDE_CABINETS_NEXT_TO_PILLAR)
				ENDIF
			ENDIF
		ELSE
			IF IS_ARCADE_CABINET_MANAGER_BIT_SET(ENUM_TO_INT(ACM_SLOT_20), ARCADE_CABINET_MANAGER_HIDE_CABINETS_NEXT_TO_PILLAR)
				IF DOES_ENTITY_EXIST(sACMData.arcadeCabinetProp[ENUM_TO_INT(ACM_SLOT_20)])
					IF !IS_ENTITY_VISIBLE(sACMData.arcadeCabinetProp[ENUM_TO_INT(ACM_SLOT_20)])
						SET_ENTITY_VISIBLE(sACMData.arcadeCabinetProp[ENUM_TO_INT(ACM_SLOT_20)], TRUE)
						CLEAR_ARCADE_CABINET_MANAGER_BIT(ENUM_TO_INT(ACM_SLOT_20), ARCADE_CABINET_MANAGER_HIDE_CABINETS_NEXT_TO_PILLAR)
					ENDIF
				ENDIF
			ENDIF
			IF IS_ARCADE_CABINET_MANAGER_BIT_SET(ENUM_TO_INT(ACM_SLOT_21), ARCADE_CABINET_MANAGER_HIDE_CABINETS_NEXT_TO_PILLAR)
				IF DOES_ENTITY_EXIST(sACMData.arcadeCabinetProp[ENUM_TO_INT(ACM_SLOT_21)])
					IF !IS_ENTITY_VISIBLE(sACMData.arcadeCabinetProp[ENUM_TO_INT(ACM_SLOT_21)])
						SET_ENTITY_VISIBLE(sACMData.arcadeCabinetProp[ENUM_TO_INT(ACM_SLOT_21)], TRUE)
						CLEAR_ARCADE_CABINET_MANAGER_BIT(ENUM_TO_INT(ACM_SLOT_21), ARCADE_CABINET_MANAGER_HIDE_CABINETS_NEXT_TO_PILLAR)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_ArcadeCabinetManagerData.iBS, ARC_CAB_MANAGER_GLOBAL_BIT_SET_HIDE_CABINETS_FOR_FINAL_MOCAP)	
			ARCADE_CAB_MANAGER_SLOT eSlot = INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, sACMData.iArcadeCabinetStagger)
			IF eSlot = ACM_SLOT_31
			OR eSlot = ACM_SLOT_32
				IF DOES_ENTITY_EXIST(sACMData.arcadeCabinetProp[sACMData.iArcadeCabinetStagger])
					IF sACMData.eArcadeCab[sACMData.iArcadeCabinetStagger] = ARCADE_CABINET_CH_NIGHT_DRIVE_CAR
					OR sACMData.eArcadeCab[sACMData.iArcadeCabinetStagger] = ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK
					OR sACMData.eArcadeCab[sACMData.iArcadeCabinetStagger] = ARCADE_CABINET_CH_NIGHT_DRIVE_BIKE
						IF NOT IS_ARCADE_CABINET_MANAGER_BIT_SET(sACMData.iArcadeCabinetStagger, ARCADE_CABINET_MANAGER_HIDE_CABINETS_FOR_FINAL_MOCAP)
							IF IS_ENTITY_VISIBLE(sACMData.arcadeCabinetProp[sACMData.iArcadeCabinetStagger])
								SET_ENTITY_VISIBLE(sACMData.arcadeCabinetProp[sACMData.iArcadeCabinetStagger], FALSE)
								SET_ARCADE_CABINET_MANAGER_BIT(sACMData.iArcadeCabinetStagger, ARCADE_CABINET_MANAGER_HIDE_CABINETS_FOR_FINAL_MOCAP)
							ENDIF
						ENDIF
					ENDIF	
				ELSE
					CLEAR_ARCADE_CABINET_MANAGER_BIT(sACMData.iArcadeCabinetStagger, ARCADE_CABINET_MANAGER_HIDE_CABINETS_FOR_FINAL_MOCAP)
				ENDIF
			ENDIF
		ELSE
			IF IS_ARCADE_CABINET_MANAGER_BIT_SET(ENUM_TO_INT(ACM_SLOT_31), ARCADE_CABINET_MANAGER_HIDE_CABINETS_FOR_FINAL_MOCAP)
				IF DOES_ENTITY_EXIST(sACMData.arcadeCabinetProp[ENUM_TO_INT(ACM_SLOT_31)])
					IF !IS_ENTITY_VISIBLE(sACMData.arcadeCabinetProp[ENUM_TO_INT(ACM_SLOT_31)])
						SET_ENTITY_VISIBLE(sACMData.arcadeCabinetProp[ENUM_TO_INT(ACM_SLOT_31)], TRUE)
						CLEAR_ARCADE_CABINET_MANAGER_BIT(ENUM_TO_INT(ACM_SLOT_31), ARCADE_CABINET_MANAGER_HIDE_CABINETS_FOR_FINAL_MOCAP)
					ENDIF
				ENDIF
			ENDIF
			IF IS_ARCADE_CABINET_MANAGER_BIT_SET(ENUM_TO_INT(ACM_SLOT_32), ARCADE_CABINET_MANAGER_HIDE_CABINETS_FOR_FINAL_MOCAP)
				IF DOES_ENTITY_EXIST(sACMData.arcadeCabinetProp[ENUM_TO_INT(ACM_SLOT_32)])
					IF !IS_ENTITY_VISIBLE(sACMData.arcadeCabinetProp[ENUM_TO_INT(ACM_SLOT_32)])
						SET_ENTITY_VISIBLE(sACMData.arcadeCabinetProp[ENUM_TO_INT(ACM_SLOT_32)], TRUE)
						CLEAR_ARCADE_CABINET_MANAGER_BIT(ENUM_TO_INT(ACM_SLOT_32), ARCADE_CABINET_MANAGER_HIDE_CABINETS_FOR_FINAL_MOCAP)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
ENDPROC


/// PURPOSE:
///    Assing server sub prop objects to local variables
PROC MAINTAIN_ASSING_LOCAL_SUB_PROP_OBJECTS()
	IF !IS_BIT_SET(sACMData.iBS[sACMData.iArcadeCabinetStagger], ARCADE_CABINET_MANAGER_ASSIGN_SUB_PROP_OBJ_INDEX)
		IF sACMData.eArcadeCab[sACMData.iArcadeCabinetStagger] != ARCADE_CABINET_INVALID
			IF serverBD.eServerState = ARCADE_CABINET_SERVER_IDLE_STATE
				INT iSubPropIndex
				ACM_SUB_PROP_DETAILS eArcadeCabSubPropDetail
				
				GET_ARCADE_CABINET_SUB_PROP_DETAIL(sACMData.eArcadeCab[sACMData.iArcadeCabinetStagger], eArcadeCabSubPropDetail)
				
				IF eArcadeCabSubPropDetail.iNumProp > 0 
					FOR iSubPropIndex = 0 TO eArcadeCabSubPropDetail.iNumProp - 1
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.serverArcadeCabinetSubObj[sACMData.iArcadeCabinetStagger][iSubPropIndex])
							sACMData.arcadeCabinetSubObj[sACMData.iArcadeCabinetStagger][iSubPropIndex] = NET_TO_OBJ(serverBD.serverArcadeCabinetSubObj[sACMData.iArcadeCabinetStagger][iSubPropIndex])
						ENDIF
					ENDFOR
				ENDIF
					
				SET_BIT(sACMData.iBS[sACMData.iArcadeCabinetStagger], ARCADE_CABINET_MANAGER_ASSIGN_SUB_PROP_OBJ_INDEX)
			ENDIF	
		ENDIF	
	ENDIF
ENDPROC

/// PURPOSE:
///    Owner of player request to be a host
PROC MAINTAIN_OWNER_AS_HOST()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = PLAYER_ID()
			IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
				IF NOT HAS_NET_TIMER_STARTED(sACMData.tHostRequestTimer)
					IF NETWORK_GET_HOST_OF_THIS_SCRIPT() != INVALID_PARTICIPANT_INDEX()
						
						NETWORK_REQUEST_TO_BE_HOST_OF_THIS_SCRIPT()
						
						START_NET_TIMER(sACMData.tHostRequestTimer, TRUE)
						
						PRINTLN("[AM_ARC_CAB] - MAINTAIN_OWNER_AS_HOST - Requesting to be host.")
					ENDIF
				ELSE
					IF HAS_NET_TIMER_EXPIRED(sACMData.tHostRequestTimer, 1000, TRUE)
						RESET_NET_TIMER(sACMData.tHostRequestTimer)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
ENDPROC

PROC MAINTAIN_ACM_CLEANUP_EXIT_ANIM_STATE()	
	DISABLE_FRONTEND_THIS_FRAME()
	
	INT iLocatePlayerIsIn = GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID())
	IF iLocatePlayerIsIn > -1
		IF IS_ARCADE_CABINET_MANAGER_BIT_SET(iLocatePlayerIsIn, ARCADE_CABINET_MANAGER_EXIT_ARCADE_CABINET_ANIM_STARTED)
			IF GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID()) = ARCADE_CABINET_CH_LOVE_METER
				IF !IS_LOCAL_PLAYER_ARCADE_LOVE_METER_ANIM_PLAYING()
					PRINTLN("[AM_ARC_CAB] MAINTAIN_ACM_CLEANUP_EXIT_ANIM_STATE love meter exit anim finished")
					SET_ARCADE_CABINET_MANAGER_STATE(ARCADE_CABINET_CLEANUP_STATE)
				ENDIF
			ELSE
				IF IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(sACMData.sArcadeCabinetAnimEvent.ePreviousAnimClip)
				AND HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(sACMData.sArcadeCabinetAnimEvent.ePreviousAnimClip)
					IF IS_ANY_DISABLED_ANALOGUE_PRESSED()	
					OR IS_ARCADE_CABINET_SYNC_SCENE_PHASE_PASSED(sACMData.sArcadeCabinetAnimEvent.ePreviousAnimClip, 0.98)
						PRINTLN("[AM_ARC_CAB] MAINTAIN_ACM_CLEANUP_EXIT_ANIM_STATE exit animation finished")
						IF SHOULD_CLEANUP_THIS_ARCADE_MINI_GAME_ASSETS(GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID()))
							SET_ARCADE_CABINET_MANAGER_STATE(ARCADE_CABINET_CLEANUP_GAME_ASSETS_STATE)
						ELSE
							IF SHOULD_CLEANUP_ARCADE_CABINET_USING_FLAG()
								STOP_CURRENT_ARCADE_CABINET_SYNCHED_SCENE()
								STOP_CURRENT_ARCADE_CABINET_PROP_SYNCHED_SCENE()
								
								IF IS_GAMEPLAY_HINT_ACTIVE()
									STOP_GAMEPLAY_HINT()
								ENDIF
													
								// neil f. - fix for url:bugstar:6598580 - Local Player gets stuck on "Downloading" when attempting to join a UGC Job invite from inside the Arcade, after quitting Qub3d mid-game
								// switching the player control back on just after the player has accepted an invite can cause it to get stuck.
								IF NOT IS_TRANSITION_FADE_OUT_FOR_INTERIOR_SWITCH()					
									NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
								ENDIF
								
								CLEAR_PED_TASKS(PLAYER_PED_ID())
								SET_PLAYER_CONFIG_FLAG_KINEMATIC_MODE_WHEN_STATIONARY(FALSE)
							ENDIF	
							SET_ARCADE_CABINET_MANAGER_STATE(ARCADE_CABINET_CLEANUP_STATE)
						ENDIF	
					ENDIF
				ELSE
					IF NOT HAS_NET_TIMER_STARTED(sACMData.tExitAnimSafeTimer)
						START_NET_TIMER(sACMData.tExitAnimSafeTimer)
					ELSE
						IF HAS_NET_TIMER_EXPIRED(sACMData.tExitAnimSafeTimer, 10000)
							RESET_NET_TIMER(sACMData.tExitAnimSafeTimer)
							PRINTLN("[AM_ARC_CAB] MAINTAIN_ACM_CLEANUP_EXIT_ANIM_STATE hit fail safe timer is anim playing: "
									, IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(sACMData.sArcadeCabinetAnimEvent.ePreviousAnimClip))
							IF SHOULD_CLEANUP_THIS_ARCADE_MINI_GAME_ASSETS(GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID()))
								SET_ARCADE_CABINET_MANAGER_STATE(ARCADE_CABINET_CLEANUP_GAME_ASSETS_STATE)
							ELSE
								SET_ARCADE_CABINET_MANAGER_STATE(ARCADE_CABINET_CLEANUP_STATE)
							ENDIF	
						ENDIF
					ENDIF	
				ENDIF
			ENDIF
		ELSE
			IF SHOULD_CLEANUP_THIS_ARCADE_MINI_GAME_ASSETS(GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID()))
				SET_ARCADE_CABINET_MANAGER_STATE(ARCADE_CABINET_CLEANUP_GAME_ASSETS_STATE)
			ELSE
				SET_ARCADE_CABINET_MANAGER_STATE(ARCADE_CABINET_CLEANUP_STATE)
			ENDIF
			PRINTLN("[AM_ARC_CAB] MAINTAIN_ACM_CLEANUP_EXIT_ANIM_STATE ARCADE_CABINET_MANAGER_EXIT_ARCADE_CABINET_ANIM_STARTED is not set")
		ENDIF	
	ELSE
		IF SHOULD_CLEANUP_THIS_ARCADE_MINI_GAME_ASSETS(GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID()))
			SET_ARCADE_CABINET_MANAGER_STATE(ARCADE_CABINET_CLEANUP_GAME_ASSETS_STATE)
		ELSE
			SET_ARCADE_CABINET_MANAGER_STATE(ARCADE_CABINET_CLEANUP_STATE)
		ENDIF	
	ENDIF
ENDPROC

PROC MAINTAIN_ACM_CLEANUP_GAME_ASSETS_STATE()
	IF CLEANUP_MINIGAME_ANIM_PROPS()
		IF SHOULD_CLEANUP_ARCADE_CABINET_USING_FLAG()
			IF !IS_PLAYER_CONTROL_ON(PLAYER_ID())
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				CLEAR_PED_TASKS(PLAYER_PED_ID())
			ENDIF	
			SET_PLAYER_CONFIG_FLAG_KINEMATIC_MODE_WHEN_STATIONARY(FALSE)
		ENDIF	
		SET_ARCADE_CABINET_MANAGER_STATE(ARCADE_CABINET_CLEANUP_STATE)
	ELSE
		PRINTLN("[AM_ARC_CAB] MAINTAIN_ACM_CLEANUP_GAME_ASSETS_STATE waiting on CLEANUP_MINIGAME_ANIM_PROPS")
	ENDIF
ENDPROC

PROC UPDATE_GLOBAL_VAIABLES()
	g_ArcadeCabinetManagerData.eArcadeCabinetManagerState = sACMData.eArcadeCabinetManagerState
ENDPROC

PROC MAINTAIN_ARCADE_CABINET_TRANSACTION()
	IF NETWORK_CAN_SPEND_MONEY(1, FALSE, TRUE, FALSE)
		ARCADE_CABINETS eArcadeCaninet = GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID())
		TEXT_LABEL_15 tlArcadeMachineName = GET_ARCADE_MACHINE_LABEL(eArcadeCaninet, FALSE, TRUE)
		
		IF USE_SERVER_TRANSACTIONS()
			INT iTransactionID
	     	g_cashTransactionData[iTransactionID].cashInfo.iItemHash = GET_HASH_KEY(tlArcadeMachineName)
			TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_ARCADE_GAME, 1, iTransactionID, FALSE, TRUE)
		ELSE
			NETWORK_SPEND_PLAY_ARCADE(1, FALSE, TRUE, GET_HASH_KEY(tlArcadeMachineName), 1)
		ENDIF
		
		USE_FAKE_MP_CASH(FALSE)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
	    PRINTLN("[AM_ARC_CAB] MAINTAIN_ARCADE_CABINET_TRANSACTION success!!")
		SET_ARCADE_CABINET_MANAGER_STATE(ARCADE_CABINET_GO_TO_INITAL_COORDS_STATE)
	ELSE
		PRINTLN("[AM_ARC_CAB] MAINTAIN_ARCADE_CABINET_TRANSACTION not enough money ")
		SET_ARCADE_CABINET_MANAGER_STATE(ARCADE_CABINET_CLEANUP_STATE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Update arcade cabinet camera while cabinet is in use
PROC UPDATE_ARCADE_CABINET_CUSTOM_CAMERA()
	ARCADE_CABINETS eArcadeCaninet = GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID())
	
	IF DOES_THIS_ARCADE_CABINET_HAVE_CUSTOM_CAMERA(eArcadeCaninet)
		IF sACMData.eArcadeCabinetManagerState >= ARCADE_CABINET_GO_TO_INITAL_COORDS_STATE
		AND sACMData.eArcadeCabinetManagerState <= ARCADE_CABINET_IN_USE_ANIM_STATE
			SET_TABLE_GAMES_CAMERA_THIS_UPDATE(GET_ARCADE_CABINET_CAMERA_TYPE(eArcadeCaninet))
		ENDIF	
	ENDIF
ENDPROC

PROC MAINTAIN_ARCADE_CABINET_SUB_PROP_VISIBALE_LOCALLY()
	IF IS_BIT_SET(serverBD.iServerBS, ARCADE_CABINET_MANAGER_SERVER_BD_SET_SUB_PROP_TO_VISIBLE)
	OR IS_LOCAL_PLAYER_USING_ARCADE_MANAGEMENT_MENU()
	OR IS_BIT_SET(g_ArcadeCabinetManagerData.iBS, ARC_CAB_MANAGER_GLOBAL_BIT_SET_UPDATE_PROPS)	
		ACM_SUB_PROP_DETAILS eArcadeCabSubPropDetail
		INT i
		FOR i = 0 TO GET_ARCADE_SLOT_COUNT_FOR_PROPERTY(sACMData.eArcadeCabinetPropertyType) - (GET_NUMBER_OF_ARCADE_MINI_GAME(sACMData.eArcadeCabinetPropertyType) + 1)
			IF sACMData.eArcadeCab[i] != ARCADE_CABINET_INVALID
				IF DOES_ENTITY_EXIST(sACMData.arcadeCabinetProp[i])
					GET_ARCADE_CABINET_SUB_PROP_DETAIL(sACMData.eArcadeCab[i], eArcadeCabSubPropDetail)
					
					INT iSubPropIndex
					
					IF eArcadeCabSubPropDetail.iNumProp > 0
						FOR iSubPropIndex = 0 TO eArcadeCabSubPropDetail.iNumProp - 1
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.serverArcadeCabinetSubObj[i][iSubPropIndex])
								SET_ENTITY_LOCALLY_VISIBLE(NET_TO_ENT(serverBD.serverArcadeCabinetSubObj[i][iSubPropIndex]))
							ENDIF
						ENDFOR
					ENDIF
				ENDIF	
			ENDIF
		ENDFOR	
	ENDIF
ENDPROC

PROC MAINTAIN_CONTROL_OF_IN_USE_ARCADE_CABINET_SUB_PROP()
	IF IS_PLAYER_USING_ARCADE_CABINET(PLAYER_ID())
		
		INT iDisplaySlot = GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID())
		INT iPlayerIndex = GET_ARCADE_CABINET_LOCATE_PLAYER_SLOT_PLAYER_IS_IN(PLAYER_ID())
		INT iSubPropIndex
		
		ARCADE_CABINETS eCabinetLocatePlayerIsIn = GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID())
		
		IF iDisplaySlot != -1
			ACM_SUB_PROP_DETAILS eArcadeCabSubPropDetail
			IF sACMData.eArcadeCab[iDisplaySlot] != ARCADE_CABINET_INVALID
			AND sACMData.eArcadeCab[iDisplaySlot] != ARCADE_CABINET_CH_CLAW_CRANE		// This is done in child script already
				IF DOES_ENTITY_EXIST(sACMData.arcadeCabinetProp[iDisplaySlot])
					GET_ARCADE_CABINET_SUB_PROP_DETAIL(sACMData.eArcadeCab[iDisplaySlot], eArcadeCabSubPropDetail)
					
					IF eArcadeCabSubPropDetail.iNumProp > 0 
						FOR iSubPropIndex = 0 TO eArcadeCabSubPropDetail.iNumProp - 1
							
							IF iPlayerIndex = ENUM_TO_INT(ARCADE_CAB_PLAYER_1)	
								IF (GET_NUM_PLAYERS_GENERIC_ARCADE_CAN_HAVE(eCabinetLocatePlayerIsIn) = ENUM_TO_INT(ARCADE_CAB_PLAYER_2) AND iSubPropIndex = 0)
								OR GET_NUM_PLAYERS_GENERIC_ARCADE_CAN_HAVE(eCabinetLocatePlayerIsIn) = ENUM_TO_INT(ARCADE_CAB_PLAYER_1)
									IF DOES_ENTITY_EXIST(sACMData.arcadeCabinetSubObj[iDisplaySlot][iSubPropIndex])
										TAKE_CONTROL_OF_ENTITY(sACMData.arcadeCabinetSubObj[iDisplaySlot][iSubPropIndex])
									ENDIF
								ENDIF	
							ELIF iPlayerIndex = ENUM_TO_INT(ARCADE_CAB_PLAYER_2)
								IF iSubPropIndex = 1
									IF DOES_ENTITY_EXIST(sACMData.arcadeCabinetSubObj[iDisplaySlot][iSubPropIndex])
										TAKE_CONTROL_OF_ENTITY(sACMData.arcadeCabinetSubObj[iDisplaySlot][iSubPropIndex])
									ENDIF
								ENDIF	
							ENDIF
						ENDFOR	
					ENDIF	
				ENDIF					
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

PROC UPDATE_ARCADE_CABINET_DRILL_MINI_GAME_PFX()
	
	IF !IS_LOCAL_PLAYER_PED_IN_ARCADE_ROOM(ARC_RM_BASEMENT)
	AND IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
		EXIT
	ENDIF
	
	IF HAS_PLAYER_PURCHASED_CASINO_VAULT_DOOR(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner)
		INT iDrillMiniGameSlot = ENUM_TO_INT(ACM_SLOT_37)
		MANAGE_VAULT_DRILLING_ASSET_REQUESTS__ARCADE()
		IF sACMData.eArcadeCab[iDrillMiniGameSlot] != ARCADE_CABINET_INVALID
			INT iNumPlayers = GET_NUM_PLAYERS_ARCADE_CABINET_CAN_HAVE(sACMData.eArcadeCab[iDrillMiniGameSlot])
			INT iPlayerSlot
			OBJECT_INDEX keyPadObject
			INT iPlayerPlayingMiniGame = -1
			FOR iPlayerSlot = 0 TO iNumPlayers
				keyPadObject = sACMData.arcadeCabinetProp[iDrillMiniGameSlot]
				iPlayerPlayingMiniGame = GET_PLAYER_INDEX_USING_THIS_ARCADE_CABINET_DISPLAY_SLOT(serverBD, iDrillMiniGameSlot, iPlayerSlot, sACMData.eArcadeCabinetPropertyType)
				IF iPlayerPlayingMiniGame != -1
					IF globalPlayerBD[iPlayerPlayingMiniGame].sArcadeManagerGlobalPlayerBD.iArcadePlayerSlot = ENUM_TO_INT(ARCADE_CAB_PLAYER_2)
						keyPadObject = sACMData.arcadeCabinetSubObj[iDrillMiniGameSlot][ARCADE_CAB_OBJECT_1]
					ENDIF
					UPDATE_LOCAL_VAULT_DRILL_PTFX_ARCADE(sACMData.sArcadeDrillPractice , PARTICIPANT_ID_TO_INT(), keyPadObject, iPlayerPlayingMiniGame)
				ENDIF	
			ENDFOR
		ENDIF	
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintain arcade cabinet manager main client logics
PROC RUN_MAIN_CLIENT_LOGIC()
	UPDATE_PROP_MODEL_VARIATION()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_PROP_MODEL_VARIATION")
	#ENDIF	
	#ENDIF
		
	UPDATE_ARCADE_SOUND_INDEX()	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_ARCADE_SOUND_INDEX")
	#ENDIF	
	#ENDIF
	
	FORCE_ROOM_FOR_ARCADE_CABINET_PROPS()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("FORCE_ROOM_FOR_ARCADE_CABINET_PROPS")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_ASSING_LOCAL_SUB_PROP_OBJECTS()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ASSING_LOCAL_SUB_PROP_OBJECTS")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_CONTROL_OF_IN_USE_ARCADE_CABINET_SUB_PROP()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ASSING_LOCAL_SUB_PROP_OBJECTS")
	#ENDIF	
	#ENDIF

	SWITCH sACMData.eArcadeCabinetManagerState
		CASE ARCADE_CABINET_INIT_PROPS_STATE		
			MAINTAIN_ACM_INIT_PROP_STATE()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ACM_INIT_PROP_STATE")
			#ENDIF	
			#ENDIF
		BREAK
		CASE ARCADE_CABINET_INIT_CHILD_SCRIPT_STATE	
			IF MAINTAIN_ACM_INIT_CHILD_SCRIPT_STATE()
				SET_ARCADE_CABINET_MANAGER_STATE(ARCADE_CABINET_IDLE_STATE)
			ENDIF	
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ACM_INIT_CHILD_SCRIPT_STATE")
			#ENDIF	
			#ENDIF
		BREAK
		CASE ARCADE_CABINET_IDLE_STATE				
			MAINTAIN_ACM_IDLE_STATE()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ACM_IDLE_STATE")
			#ENDIF	
			#ENDIF
		BREAK
		CASE ARCADE_CABINET_DRAW_STARTUP_MENU_STATE
			MAINTAIN_DRAW_STARTUP_MENU()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_DRAW_STARTUP_MENU")
			#ENDIF	
			#ENDIF
		BREAK
		CASE ARCADE_CABINET_TAKE_MONEY_STATE
			MAINTAIN_ARCADE_CABINET_TRANSACTION()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ARCADE_CABINET_TRANSACTION")
			#ENDIF	
			#ENDIF
		BREAK
		CASE ARCADE_CABINET_GO_TO_INITAL_COORDS_STATE
			MAINTAIN_ACM_TASK_GO_TO_INITAL_COORDS()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ACM_TASK_GO_TO_INITAL_COORDS")
			#ENDIF	
			#ENDIF
		BREAK
		CASE ARCADE_CABINET_ENTRY_ANIM_STATE				
			MAINTAIN_ACM_ENTRY_ANIM_STATE()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ACM_ENTRY_ANIM_STATE")
			#ENDIF	
			#ENDIF
		BREAK
		CASE ARCADE_CABINET_LAUNCH_GAME_SCRIPT_STATE
			MAINTAIN_ACM_LAUNCH_GAME_SCRIPT_STATE()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ACM_LAUNCH_GAME_SCRIPT_STATE")
			#ENDIF	
			#ENDIF
		BREAK
		CASE ARCADE_CABINET_IN_USE_ANIM_STATE		
			MAINTAIN_ACM_IN_USE_ANIM_STATE()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ACM_IN_USE_ANIM_STATE")
			#ENDIF	
			#ENDIF
		BREAK
		CASE ARCADE_CABINET_CLEANUP_EXIT_STATE
			MAINTAIN_ACM_CLEANUP_EXIT_ANIM_STATE()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ACM_CLEANUP_EXIT_ANIM_STATE")
			#ENDIF	
			#ENDIF
		BREAK
		CASE ARCADE_CABINET_CLEANUP_GAME_ASSETS_STATE
			MAINTAIN_ACM_CLEANUP_GAME_ASSETS_STATE()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ACM_CLEANUP_GAME_ASSETS_STATE")
			#ENDIF	
			#ENDIF
		BREAK
		CASE ARCADE_CABINET_CLEANUP_STATE					
			MAINTAIN_ACM_CLEANUP_STATE()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ACM_CLEANUP_STATE")
			#ENDIF	
			#ENDIF
		BREAK
	ENDSWITCH
	
	MAINTAIN_ARCADE_CABINET_EVENTS()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ARCADE_CABINET_EVENTS")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_ARCADE_CABINETS_IN_USE_SCREENS()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ARCADE_CABINETS_IN_USE_SCREENS")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_OWNER_AS_HOST()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_OWNER_AS_HOST")
	#ENDIF	
	#ENDIF
	
	UPDATE_ARCADE_CABINET_CUSTOM_CAMERA()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_ARCADE_CABINET_CUSTOM_CAMERA")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_ARCADE_CABINET_SOUND()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ARCADE_CABINET_SOUND")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_ARCADE_CABINET_MANAGEMENT(sACMData, serverBD)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ARCADE_CABINET_MANGMENT")
	#ENDIF	
	#ENDIF
	
	HIDE_ARCADE_CABINETS_FOR_NET_CUTSCENE()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("HIDE_ARCADE_CABINETS_FOR_NET_CUTSCENE")
	#ENDIF	
	#ENDIF
	
	UPDATE_ARCADE_CABINET_STAGGER()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_ARCADE_CABINET_STAGGER")
	#ENDIF	
	#ENDIF
	
	UPDATE_ARCADE_CABINET_PLAYER_STAGGER()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_ARCADE_CABINET_PLAYER_STAGGER")
	#ENDIF	
	#ENDIF
	
	UPDATE_ARCADE_CABINET_NUM_PLAYER_ALLOWED_STAGGER()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_ARCADE_CABINET_NUM_PLAYER_ALLOWED_STAGGER")
	#ENDIF	
	#ENDIF

	PROCESS_UPDATE_ARCADE_CABINET_PROPS()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("PROCESS_UPDATE_ARCADE_CABINET_PROPS")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_ARCADE_CABINET_SUB_PROP_VISIBALE_LOCALLY()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ARCADE_CABINET_SUB_PROP_VISIBALE_LOCALLY")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_ARCADE_CABINET_SUB_PROP_VISIBILITY_IN_ANIM()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ARCADE_CABINET_SUB_PROP_VISIBILITY_IN_ANIM")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_ARCADE_CABINET_SUB_PROP_ANIMATION_ATTACHMENT()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ARCADE_CABINET_SUB_PROP_VISIBILITY_IN_ANIM")
	#ENDIF	
	#ENDIF
	
	SAVE_ARCADE_CABINET_DISPLAY_SLOT()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("SAVE_ARCADE_CABINET_DISPLAY_SLOT")
	#ENDIF	
	#ENDIF
	
	UPDATE_GLOBAL_VAIABLES()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_GLOBAL_VAIABLES")
	#ENDIF	
	#ENDIF
	
	UPDATE_ARCADE_CABINET_DRILL_MINI_GAME_PFX()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_ARCADE_CABINET_DRILL_MINI_GAME_PFX")
	#ENDIF	
	#ENDIF
ENDPROC

/// PURPOSE:
///    Host will check if locate is not in use and assings the requested player to that locate 
PROC MAINTAIN_SERVER_ASSIGN_PLAYER_USAGE_REQUEST()
	PLAYER_INDEX piPlayer = INT_TO_NATIVE(PLAYER_INDEX, sACMData.iPlayerStagger)
		
	IF NETWORK_IS_PLAYER_ACTIVE(piPlayer)
	AND NETWORK_IS_PLAYER_A_PARTICIPANT(piPlayer)
		IF IS_PLAYER_REQUESTING_TO_USE_CABINET(piPlayer)
		AND !IS_PLAYER_USING_ARCADE_CABINET(piPlayer)	
			INT iDisplaySlotLocatePlayerIsIn = GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(piPlayer)
			INT iPlayerSlotPlayerIsIn = GET_ARCADE_CABINET_LOCATE_PLAYER_SLOT_PLAYER_IS_IN(piPlayer)
			IF GET_PLAYER_INDEX_USING_THIS_ARCADE_CABINET_DISPLAY_SLOT(serverBD, iDisplaySlotLocatePlayerIsIn, iPlayerSlotPlayerIsIn, sACMData.eArcadeCabinetPropertyType) = -1
				SET_PLAYER_USING_THIS_DISPLAY_SLOT(iDisplaySlotLocatePlayerIsIn, iPlayerSlotPlayerIsIn, sACMData.iPlayerStagger)
			ENDIF
			PRINTLN("[AM_ARC_CAB] - MAINTAIN_SERVER_ASSIGN_PLAYER_USAGE_REQUEST sACMData.iPlayerStagger: ", sACMData.iPlayerStagger, " iDisplaySlotLocatePlayerIsIn: ", iDisplaySlotLocatePlayerIsIn, 
					" using index: ", GET_PLAYER_INDEX_USING_THIS_ARCADE_CABINET_DISPLAY_SLOT(serverBD, iDisplaySlotLocatePlayerIsIn, iPlayerSlotPlayerIsIn, sACMData.eArcadeCabinetPropertyType))
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Check if host should reset server usage flat
FUNC BOOL SHOULD_RESET_ARCADE_CABINET_USAGE_FLAG(INT iPlayerIndex)
	PLAYER_INDEX piPlayer = INT_TO_NATIVE(PLAYER_INDEX, iPlayerIndex)
	IF !IS_NET_PLAYER_OK(piPlayer)
		PRINTLN("[AM_ARC_CAB] SHOULD_RESET_ARCADE_CABINET_USAGE_FLAG player not okay ", iPlayerIndex)
		RETURN TRUE
	ENDIF	
	
	IF !NETWORK_IS_PLAYER_ACTIVE(piPlayer)
	OR !NETWORK_IS_PLAYER_A_PARTICIPANT(piPlayer)
		PRINTLN("[AM_ARC_CAB] SHOULD_RESET_ARCADE_CABINET_USAGE_FLAG player not active or not participant ", iPlayerIndex)
		RETURN TRUE
	ENDIF
	
	IF !IS_PLAYER_REQUESTING_TO_USE_CABINET(piPlayer)
	AND !IS_PLAYER_USING_ARCADE_CABINET(piPlayer)	
		PRINTLN("[AM_ARC_CAB] SHOULD_RESET_ARCADE_CABINET_USAGE_FLAG player not using & not requesting ", iPlayerIndex)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Server check if should clear usage flags
PROC MAINTAIN_SERVER_CLEAR_USAGE_FLAG()
	INT piPlayerUsing, iPlayerIndex
	IF sACMData.eArcadeCab[sACMData.iArcadeCabinetStagger] != ARCADE_CABINET_INVALID
		INT iMaxNumPlayer = GET_NUM_PLAYERS_ARCADE_CABINET_CAN_HAVE(sACMData.eArcadeCab[sACMData.iArcadeCabinetStagger])

		FOR iPlayerIndex = ENUM_TO_INT(ARCADE_CAB_PLAYER_1) TO iMaxNumPlayer
			piPlayerUsing = GET_PLAYER_INDEX_USING_THIS_ARCADE_CABINET_DISPLAY_SLOT(serverBD, sACMData.iArcadeCabinetStagger, iPlayerIndex, sACMData.eArcadeCabinetPropertyType)
			IF piPlayerUsing != -1	
				IF SHOULD_RESET_ARCADE_CABINET_USAGE_FLAG(piPlayerUsing)
					SET_PLAYER_USING_THIS_DISPLAY_SLOT(sACMData.iArcadeCabinetStagger, iPlayerIndex, -1)
				ENDIF
			ENDIF
		ENDFOR	
	ENDIF	
ENDPROC

FUNC INT GET_TOTAL_NUM_SUB_PROPS()
	RETURN serverBD.iTotalNumSubProp
ENDFUNC

/// PURPOSE:
///    Has all arcade cabinet props created
FUNC BOOL CREATE_ARCADE_CABINET_SUB_PROPS()
	INT i
	FOR i = 0 TO GET_NUM_ARCADE_CABINETS() - 1
		IF !SERVER_CREATE_THIS_ARCADE_CABINET_SUB_PROPS(sACMData, serverBD, i)
			RETURN FALSE
		ENDIF	
	ENDFOR 
	RETURN TRUE
ENDFUNC 

PROC SET_ARCADE_CABINET_TOTAL_PROP_NUM()
	IF NOT IS_BIT_SET(serverBD.iServerBS, ARCADE_CABINET_MANAGER_SERVER_BD_SET_TOTAL_NUM_SUB_PROP)
		IF HAS_ALL_PROPS_CREATED()
			ACM_SUB_PROP_DETAILS eArcadeCabSubPropDetail
			
			INT i
			FOR i = 0 TO GET_NUM_ARCADE_CABINETS() - 1
				IF sACMData.eArcadeCab[i] != ARCADE_CABINET_INVALID
					GET_ARCADE_CABINET_SUB_PROP_DETAIL(sACMData.eArcadeCab[i], eArcadeCabSubPropDetail)
					serverBD.iTotalNumSubProp = serverBD.iTotalNumSubProp + eArcadeCabSubPropDetail.iNumProp
					PRINTLN("[AM_ARC_CAB] - SET_ARCADE_CABINET_TOTAL_PROP_NUM serverBD.iTotalNumSubProp: ", serverBD.iTotalNumSubProp)	
				ENDIF	
			ENDFOR
			SET_BIT(serverBD.iServerBS, ARCADE_CABINET_MANAGER_SERVER_BD_SET_TOTAL_NUM_SUB_PROP)
			PRINTLN("[AM_ARC_CAB] - SET_ARCADE_CABINET_TOTAL_PROP_NUM ARCADE_CABINET_MANAGER_SERVER_BD_SET_TOTAL_NUM_SUB_PROP TRUE")	
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_SERVER_INIT_STATE()
	IF NOT IS_BIT_SET(serverBD.iServerBS, ARCADE_CABINET_MANAGER_SERVER_BD_SET_TOTAL_NUM_SUB_PROP)
		SET_ARCADE_CABINET_TOTAL_PROP_NUM()
	ELSE
		IF GET_TOTAL_NUM_SUB_PROPS() > 0
			IF CREATE_ARCADE_CABINET_SUB_PROPS()
				CLEAR_BIT(serverBD.iServerBS, ARCADE_CABINET_MANAGER_SERVER_BD_UPDATE_SUB_PROPS)
				SET_ARCADE_CABINET_MANAGER_SERVER_STATE(ARCADE_CABINET_SERVER_IDLE_STATE)
			ELSE
				PRINTLN("[AM_ARC_CAB] - MAINTAIN_SERVER_INIT_STATE waiting on sub props to created")
			ENDIF
		ELSE
			SET_ARCADE_CABINET_MANAGER_SERVER_STATE(ARCADE_CABINET_SERVER_IDLE_STATE)	
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Check if sub prop exist then set to visible
PROC MAINTAIN_SUB_PROP_VISIBILITY()
	IF IS_BIT_SET(serverBD.iServerBS, ARCADE_CABINET_MANAGER_SERVER_BD_SET_SUB_PROP_TO_VISIBLE)
		INT i, iSubPropIndex, iNumInvisible, iNumVisible
		ACM_SUB_PROP_DETAILS eArcadeCabSubPropDetail
		FOR i = 0 TO GET_NUM_ARCADE_CABINETS() - 1
			IF sACMData.eArcadeCab[i] != ARCADE_CABINET_INVALID
				IF DOES_ENTITY_EXIST(sACMData.arcadeCabinetProp[i])

					GET_ARCADE_CABINET_SUB_PROP_DETAIL(sACMData.eArcadeCab[i], eArcadeCabSubPropDetail)
					
					IF eArcadeCabSubPropDetail.iNumProp > 0 
						FOR iSubPropIndex = 0 TO eArcadeCabSubPropDetail.iNumProp - 1
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.serverArcadeCabinetSubObj[i][iSubPropIndex])
								IF !IS_ENTITY_VISIBLE_TO_SCRIPT(NET_TO_OBJ(serverBD.serverArcadeCabinetSubObj[i][iSubPropIndex]))
									iNumInvisible++
									IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.serverArcadeCabinetSubObj[i][iSubPropIndex])
										iNumVisible++
										SET_ENTITY_VISIBLE(NET_TO_OBJ(serverBD.serverArcadeCabinetSubObj[i][iSubPropIndex]), TRUE)
										PRINTLN("[AM_ARC_CAB] - MAINTAIN_SUB_PROP_VISIBILITY set to visible i: ", i , " iSubPropIndex: ", iSubPropIndex)
									ELSE
										NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.serverArcadeCabinetSubObj[i][iSubPropIndex])
										PRINTLN("[AM_ARC_CAB] - MAINTAIN_SUB_PROP_VISIBILITY request control of i: ", i , " iSubPropIndex: ", iSubPropIndex)
									ENDIF
								ENDIF		
							ENDIF
						ENDFOR
					ENDIF
				ENDIF
			ENDIF	
		ENDFOR
		
		IF iNumVisible = iNumInvisible
			CLEAR_BIT(serverBD.iServerBS, ARCADE_CABINET_MANAGER_SERVER_BD_SET_SUB_PROP_TO_VISIBLE)
			PRINTLN("[AM_ARC_CAB] - MAINTAIN_SUB_PROP_VISIBILITY ARCADE_CABINET_MANAGER_SERVER_BD_SET_SUB_PROP_TO_VISIBLE FALSE")
		ENDIF	
	ENDIF
ENDPROC


/// PURPOSE:
///    Maintain server idle state
PROC MAINTAIN_SERVER_IDLE_STATE()
	MAINTAIN_SERVER_ASSIGN_PLAYER_USAGE_REQUEST()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SERVER_ASSIGN_PLAYER_USAGE_REQUEST")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_SERVER_CLEAR_USAGE_FLAG()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SERVER_CLEAR_USAGE_FLAG")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_SUB_PROP_VISIBILITY()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SUB_PROP_VISIBILITY")
	#ENDIF	
	#ENDIF
ENDPROC

PROC MAINTAIN_ARCADE_CABINET_SERVER_CLEANUP()
	IF IS_BIT_SET(g_ArcadeCabinetManagerData.iBS, ARC_CAB_MANAGER_GLOBAL_BIT_SET_UPDATE_PROPS)
	AND !IS_BIT_SET(serverBD.iServerBS, ARCADE_CABINET_MANAGER_SERVER_BD_UPDATE_SUB_PROPS)
		// Skip
	ELSE
		serverBD.iServerBS = 0
		SET_ARCADE_CABINET_MANAGER_SERVER_STATE(ARCADE_CABINET_SERVER_INIT_STATE)
	ENDIF	
ENDPROC

/// PURPOSE:
///    Maintain server main logic
PROC RUN_MAIN_SERVER_LOGIC()
	SWITCH serverBD.eServerState
		CASE ARCADE_CABINET_SERVER_INIT_STATE
			MAINTAIN_SERVER_INIT_STATE()
		BREAK
		CASE ARCADE_CABINET_SERVER_IDLE_STATE
			MAINTAIN_SERVER_IDLE_STATE()
		BREAK
		CASE ARCADE_CABINET_SERVER_CLEARNUP_STATE
			MAINTAIN_ARCADE_CABINET_SERVER_CLEANUP()
		BREAK
	ENDSWITCH	
ENDPROC

#IF IS_DEBUG_BUILD
PROC DEBUG_CLEAR_SAVE_SLOTS()
	IF sACMDebugData.bClearSaveSlots
		INT iSaveSlot 
		FOR iSaveSlot = 0 TO GET_ARCADE_SLOT_COUNT_FOR_PROPERTY(sACMData.eArcadeCabinetPropertyType) - 1
			IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
			#IF FEATURE_TUNER OR IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
			#ENDIF
				SET_ARCADE_CABINET_TO_DISPLAY_SAVE_SLOT(INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, iSaveSlot), ARCADE_CABINET_SAVE_SLOT_INVALID, sACMData.eArcadeCabinetPropertyType)
			ENDIF
		ENDFOR 
		
		sACMDebugData.bClearSaveSlots = FALSE
	ENDIF
ENDPROC

PROC DEBUG_DISPLAY_ARCADE_CABINET_SLOT_NUMBER()
 	IF sACMDebugData.bDisplaySlotNumber
		INT i 
		FOR i = 0 TO GET_NUM_ARCADE_CABINETS() - 1
			IF DOES_ENTITY_EXIST(sACMData.arcadeCabinetProp[i])
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(sACMData.arcadeCabinetProp[i], GET_STRING_FROM_INT(i + 1), 2.0)
			ENDIF
		ENDFOR 
	ENDIF
ENDPROC

PROC DEBUG_ARCADE_CABINET_TERMINATE_ALL_CHILD_SCRIPTS()
	IF sACMDebugData.bTerminateAllChildScripts
		SET_KILL_ARCADE_CABINET_MANAGER_SCRIPT(TRUE)
	ENDIF
ENDPROC

FUNC INT GET_SAVE_SLOT_WITH_SAME_PHYSICAL_GAME(ARCADE_CABINETS arcadeCabinet, INT iCurrentSlot)
	
	ARCADE_CABINETS_SAVE_SLOT_TYPE eSaveSlotType
	ARCADE_CABINETS eArcadeCabinetType
	
	IF IS_THIS_PHYSICAL_ARCADE_CABINET(arcadeCabinet)
		INT i
		FOR i = 0 TO GET_ARCADE_SLOT_COUNT_FOR_PROPERTY(sACMData.eArcadeCabinetPropertyType) - 1
			
			eSaveSlotType =  GET_ARCADE_CABINET_SAVE_SLOT_TYPE(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner, INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, i), sACMData.eArcadeCabinetPropertyType)
			eArcadeCabinetType = GET_ARCADE_CABINET_FROM_DISPLAY_SLOT_TYPE(eSaveSlotType)
			
			IF i != iCurrentSlot
				IF arcadeCabinet != ARCADE_CABINET_INVALID
					IF arcadeCabinet = eArcadeCabinetType
						RETURN i
					ENDIF
				ENDIF	
			ENDIF
		ENDFOR	
	ENDIF
	
	RETURN -1
ENDFUNC

PROC DEBUG_RANDOMISE_ARCADE_CABINETS()
	IF sACMData.eArcadeCabinetManagerState = ARCADE_CABINET_IDLE_STATE
		IF g_DebugArcadeCabinetManagerData.bRandomiseCabinets
			SET_PLAYER_PURCHASED_ARCADE_MACHINE(ARCADE_CABINET_CA_INVADE_AND_PERSUADE, TRUE)
			SET_PLAYER_PURCHASED_ARCADE_MACHINE(ARCADE_CABINET_CH_GG_SPACE_MONKEY, TRUE)
			SET_PLAYER_PURCHASED_ARCADE_MACHINE(ARCADE_CABINET_CH_LAST_GUNSLINGERS, TRUE)
			SET_PLAYER_PURCHASED_ARCADE_MACHINE(ARCADE_CABINET_CH_NIGHT_DRIVE_BIKE, TRUE)
			SET_PLAYER_PURCHASED_ARCADE_MACHINE(ARCADE_CABINET_CH_NIGHT_DRIVE_CAR, TRUE)
			SET_PLAYER_PURCHASED_ARCADE_MACHINE(ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK, TRUE)
			SET_PLAYER_PURCHASED_ARCADE_MACHINE(ARCADE_CABINET_CH_WIZARDS_SLEVE, TRUE)
			SET_PLAYER_PURCHASED_ARCADE_MACHINE(ARCADE_CABINET_CH_DEFENDER_OF_THE_FAITH, TRUE)
			SET_PLAYER_PURCHASED_ARCADE_MACHINE(ARCADE_CABINET_CH_MONKEYS_PARADISE, TRUE)
			SET_PLAYER_PURCHASED_ARCADE_MACHINE(ARCADE_CABINET_CH_PENETRATOR, TRUE)
			SET_PLAYER_PURCHASED_ARCADE_MACHINE(ARCADE_CABINET_CH_CLAW_CRANE, TRUE)
			SET_PLAYER_PURCHASED_ARCADE_MACHINE(ARCADE_CABINET_CH_FORTUNE_TELLER, TRUE)
			SET_PLAYER_PURCHASED_ARCADE_MACHINE(ARCADE_CABINET_CH_LOVE_METER, TRUE)
			#IF FEATURE_SUMMER_2020
			SET_PLAYER_PURCHASED_ARCADE_MACHINE(ARCADE_CABINET_SUM_QUB3D, TRUE)
			SET_PLAYER_PURCHASED_ARCADE_MACHINE(ARCADE_CABINET_SUM_STRENGTH_TEST, TRUE)
			#ENDIF
			
			SET_PLAYER_PURCHASED_ARCADE_MACHINE_DELIVERED(ARCADE_CABINET_CA_INVADE_AND_PERSUADE, TRUE)
			SET_PLAYER_PURCHASED_ARCADE_MACHINE_DELIVERED(ARCADE_CABINET_CH_GG_SPACE_MONKEY, TRUE)
			SET_PLAYER_PURCHASED_ARCADE_MACHINE_DELIVERED(ARCADE_CABINET_CH_LAST_GUNSLINGERS, TRUE)
			SET_PLAYER_PURCHASED_ARCADE_MACHINE_DELIVERED(ARCADE_CABINET_CH_NIGHT_DRIVE_BIKE, TRUE)
			SET_PLAYER_PURCHASED_ARCADE_MACHINE_DELIVERED(ARCADE_CABINET_CH_NIGHT_DRIVE_CAR, TRUE)
			SET_PLAYER_PURCHASED_ARCADE_MACHINE_DELIVERED(ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK, TRUE)
			SET_PLAYER_PURCHASED_ARCADE_MACHINE_DELIVERED(ARCADE_CABINET_CH_WIZARDS_SLEVE, TRUE)
			SET_PLAYER_PURCHASED_ARCADE_MACHINE_DELIVERED(ARCADE_CABINET_CH_DEFENDER_OF_THE_FAITH, TRUE)
			SET_PLAYER_PURCHASED_ARCADE_MACHINE_DELIVERED(ARCADE_CABINET_CH_MONKEYS_PARADISE, TRUE)
			SET_PLAYER_PURCHASED_ARCADE_MACHINE_DELIVERED(ARCADE_CABINET_CH_PENETRATOR, TRUE)
			SET_PLAYER_PURCHASED_ARCADE_MACHINE_DELIVERED(ARCADE_CABINET_CH_CLAW_CRANE, TRUE)
			SET_PLAYER_PURCHASED_ARCADE_MACHINE_DELIVERED(ARCADE_CABINET_CH_FORTUNE_TELLER, TRUE)
			SET_PLAYER_PURCHASED_ARCADE_MACHINE_DELIVERED(ARCADE_CABINET_CH_LOVE_METER, TRUE)
			#IF FEATURE_SUMMER_2020
			SET_PLAYER_PURCHASED_ARCADE_MACHINE_DELIVERED(ARCADE_CABINET_SUM_QUB3D, TRUE)
			SET_PLAYER_PURCHASED_ARCADE_MACHINE_DELIVERED(ARCADE_CABINET_SUM_STRENGTH_TEST, TRUE)
			#ENDIF

			DEBUG_CLEAR_SAVE_SLOTS()
			
			BOOL bAllow = FALSE
			INT i
			FOR i = 0 TO GET_ARCADE_SLOT_COUNT_FOR_PROPERTY(sACMData.eArcadeCabinetPropertyType) - (GET_NUMBER_OF_ARCADE_MINI_GAME(sACMData.eArcadeCabinetPropertyType) + 1)
				INT iRandomCabinet
				
				WHILE !bAllow
					iRandomCabinet = GET_RANDOM_INT_IN_RANGE(ENUM_TO_INT(ARCADE_CABINET_CH_GG_SPACE_MONKEY), ENUM_TO_INT(ARCADE_CABINET_CA_STREET_CRIMES) + 1)	
				
					IF !IS_THIS_DISPLAY_SLOT_SUITABLE_FOR_ARCADE_CABINET(INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, i), INT_TO_ENUM(ARCADE_CABINETS, iRandomCabinet))
					OR GET_SAVE_SLOT_WITH_SAME_PHYSICAL_GAME(INT_TO_ENUM(ARCADE_CABINETS, iRandomCabinet), i) != -1
					OR ((GET_NUM_SLOT_SPACE_ARCADE_CABINET_TAKE(INT_TO_ENUM(ARCADE_CABINETS, iRandomCabinet), TRUE) =  2)
						AND IS_THIS_SLOT_SUITABLE_FOR_TWO_PLAYER_ARCADE_CABINET(INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, i))
						AND (GET_NUM_SLOT_SPACE_ARCADE_CABINET_TAKE(GET_ARCADE_CABINET_FROM_DISPLAY_SLOT_TYPE(GET_LOCAL_PLAYER_ARCADE_CABINET_SAVE_SLOT_TYPE(GET_ARCADE_CABINET_NEIGHBOUR_SLOT(INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, i)), sACMData.eArcadeCabinetPropertyType)), TRUE)) = 2
						AND IS_THIS_SLOT_SUITABLE_FOR_TWO_PLAYER_ARCADE_CABINET(INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, i)))
						bAllow = FALSE
					ELSE
						bAllow = TRUE
					ENDIF
					WAIT(0)
				ENDWHILE
				
				IF IS_THIS_SLOT_SUITABLE_FOR_TWO_PLAYER_ARCADE_CABINET(INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, i))
					BOOL bDoesNeighbourTakeTwo = FALSE
					IF GET_NUM_SLOT_SPACE_ARCADE_CABINET_TAKE(GET_ARCADE_CABINET_FROM_DISPLAY_SLOT_TYPE(GET_LOCAL_PLAYER_ARCADE_CABINET_SAVE_SLOT_TYPE(GET_ARCADE_CABINET_NEIGHBOUR_SLOT(INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, i)), sACMData.eArcadeCabinetPropertyType)), TRUE) = 2
						bDoesNeighbourTakeTwo = TRUE
					ENDIF	
					
					IF GET_NUM_SLOT_SPACE_ARCADE_CABINET_TAKE(INT_TO_ENUM(ARCADE_CABINETS, iRandomCabinet), TRUE) =  2
					AND bDoesNeighbourTakeTwo
						SET_ARCADE_CABINET_TO_DISPLAY_SAVE_SLOT(INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, i), 
														GET_ARCADE_CABINET_TYPE_FROM_DISPLAY_SLOT(INT_TO_ENUM(ARCADE_CABINETS, iRandomCabinet), 1), sACMData.eArcadeCabinetPropertyType)
														
						SET_ARCADE_CABINET_TO_DISPLAY_SAVE_SLOT(GET_ARCADE_CABINET_NEIGHBOUR_SLOT(INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, i)), ARCADE_CABINET_SAVE_SLOT_INVALID, sACMData.eArcadeCabinetPropertyType)								
					
					ELIF GET_NUM_SLOT_SPACE_ARCADE_CABINET_TAKE(INT_TO_ENUM(ARCADE_CABINETS, iRandomCabinet), TRUE) =  2
					AND !bDoesNeighbourTakeTwo
						SET_ARCADE_CABINET_TO_DISPLAY_SAVE_SLOT(INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, i), 
														GET_ARCADE_CABINET_TYPE_FROM_DISPLAY_SLOT(INT_TO_ENUM(ARCADE_CABINETS, iRandomCabinet), 1), sACMData.eArcadeCabinetPropertyType)
														
						SET_ARCADE_CABINET_TO_DISPLAY_SAVE_SLOT(GET_ARCADE_CABINET_NEIGHBOUR_SLOT(INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, i)), ARCADE_CABINET_SAVE_SLOT_INVALID, sACMData.eArcadeCabinetPropertyType)	
					
					ELIF bDoesNeighbourTakeTwo
					AND GET_NUM_SLOT_SPACE_ARCADE_CABINET_TAKE(INT_TO_ENUM(ARCADE_CABINETS, iRandomCabinet), TRUE) !=  2
					
						SET_ARCADE_CABINET_TO_DISPLAY_SAVE_SLOT(INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, i), ARCADE_CABINET_SAVE_SLOT_INVALID, sACMData.eArcadeCabinetPropertyType)
														
						//SET_ARCADE_CABINET_TO_DISPLAY_SAVE_SLOT(GET_ARCADE_CABINET_NEIGHBOUR_SLOT(INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, i)), ARCADE_CABINET_SAVE_SLOT_INVALID)
					
					ELSE
						
						SET_ARCADE_CABINET_TO_DISPLAY_SAVE_SLOT(INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, i), 
														GET_ARCADE_CABINET_TYPE_FROM_DISPLAY_SLOT(INT_TO_ENUM(ARCADE_CABINETS, iRandomCabinet), 1), sACMData.eArcadeCabinetPropertyType)
					
					ENDIF
					
				ELSE										
					SET_ARCADE_CABINET_TO_DISPLAY_SAVE_SLOT(INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, i), 
														GET_ARCADE_CABINET_TYPE_FROM_DISPLAY_SLOT(INT_TO_ENUM(ARCADE_CABINETS, iRandomCabinet), 1), sACMData.eArcadeCabinetPropertyType)
				ENDIF
				
				bAllow = FALSE										
			ENDFOR
			
			sACMDebugData.bTerminateAllChildScripts = TRUE
		ENDIF
	ENDIF	
ENDPROC

PROC UPDATE_DEBUG_WIDGETS()
	DEBUG_DISPLAY_ARCADE_CABINET_SLOT_NUMBER()
	DEBUG_ARCADE_CABINET_TERMINATE_ALL_CHILD_SCRIPTS()
	DEBUG_CLEAR_SAVE_SLOTS()
	DEBUG_RANDOMISE_ARCADE_CABINETS()
ENDPROC
#ENDIF

SCRIPT (ACM_LAUNCHER_STRUCT missionScriptArgs)

	IF NETWORK_IS_GAME_IN_PROGRESS() 
		SCRIPT_INITIALISE(missionScriptArgs)	
	ELSE
		SCRIPT_CLEANUP()
	ENDIF

	WHILE TRUE
		MP_LOOP_WAIT_ZERO()
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_START_OF_FRAME() 
		#ENDIF
		#ENDIF	
			
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("[AM_ARC_CAB] - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_KILL_THIS_SCRIPT()
			PRINTLN("[AM_ARC_CAB] - SHOULD_KILL_THIS_SCRIPT = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		RUN_MAIN_CLIENT_LOGIC()

		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			RUN_MAIN_SERVER_LOGIC()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		UPDATE_DEBUG_WIDGETS()
		
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_END_OF_FRAME() 
		#ENDIF
		#ENDIF

	ENDWHILE

#ENDIF	
	
#IF NOT FEATURE_CASINO_HEIST
SCRIPT
#ENDIF
	
ENDSCRIPT 


