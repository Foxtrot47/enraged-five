USING "globals.sch"
USING "rage_builtins.sch"

CONST_INT ENABLE_CONTENT	FEATURE_DLC_2_2022

#IF ENABLE_CONTENT
USING "Variations/fm_content_drug_lab_work_crop_dusting.sch"
USING "Variations/fm_content_drug_lab_work_drug_lord.sch"
USING "Variations/fm_content_drug_lab_work_supply_line.sch"
USING "Variations/fm_content_drug_lab_work_vehicle_bomb.sch"
USING "Variations/fm_content_drug_lab_work_weed_farm.sch"

/// PURPOSE: Use to set up the logic pointers for your mission
///    
PROC SETUP_MISSION_LOGIC()
	SWITCH GET_VARIATION()

		CASE DLWV_INVALID BREAK
		CASE DLWV_CROP_DUSTING 	CD_SETUP_LOGIC() 		BREAK
		CASE DLWV_WEED_FARM		WEED_SETUP_LOGIC()		BREAK
		
		DEFAULT
			#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("Please populate SETUP_MISSION_LOGIC in fm_content_<filename>_variations.sch")
			#ENDIF
		BREAK
	ENDSWITCH
ENDPROC
#ENDIF
