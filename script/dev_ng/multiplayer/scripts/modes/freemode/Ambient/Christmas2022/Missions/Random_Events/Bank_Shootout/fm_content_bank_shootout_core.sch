USING "globals.sch"
USING "rage_builtins.sch"

CONST_INT ENABLE_CONTENT									FEATURE_DLC_2_2022

#IF ENABLE_CONTENT
USING "fm_content_bank_shootout_structs.sch"
USING "fm_content_gb_mission_structs.sch"
USING "fm_content_generic_structs.sch"

//////////////////////////
///    HELPERS
//////////////////////////

FUNC BANK_SHOOTOUT_VARIATION GET_VARIATION()
	RETURN serverBD.sMissionData.eMissionVariation
ENDFUNC

FUNC BANK_SHOOTOUT_SUBVARIATION GET_SUBVARIATION()
	RETURN serverBD.sMissionData.eMissionSubvariation
ENDFUNC

//////////////////////////
///    OVERRIDES
//////////////////////////

FUNC STRING GET_MISSION_PLACEMENT_DATA_FILENAME()
	RETURN ""
ENDFUNC

#IF IS_DEBUG_BUILD
FUNC STRING GET_VARIATION_NAME_FOR_DEBUG()
	RETURN ""
ENDFUNC

FUNC STRING GET_SUBVARIATION_NAME_FOR_DEBUG()
	RETURN ""
ENDFUNC
#ENDIF

FUNC BOOL SETUP_VARIATION(INT iVariation, INT iSubvariation)
	serverBD.sMissionData.eMissionVariation = INT_TO_ENUM(BANK_SHOOTOUT_VARIATION, iVariation)
	serverBD.sMissionData.eMissionSubvariation = INT_TO_ENUM(BANK_SHOOTOUT_SUBVARIATION, iSubvariation)
	
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] SETUP_VARIATION - Variation: ", GET_VARIATION_NAME_FOR_DEBUG())
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] SETUP_VARIATION - Subvariation: ", GET_SUBVARIATION_NAME_FOR_DEBUG())
	
	RETURN TRUE
ENDFUNC

/// PURPOSE: Use to set up the logic pointers for your mission
///    
PROC SETUP_MISSION_LOGIC()

ENDPROC
#ENDIF
