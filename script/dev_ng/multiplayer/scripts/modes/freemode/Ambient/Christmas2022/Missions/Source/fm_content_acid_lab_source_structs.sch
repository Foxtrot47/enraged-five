USING "globals.sch"
USING "rage_builtins.sch"
USING "fm_content_includes.sch"

//----------------------
//	CONSTANTS
//----------------------
CONST_INT MAX_NUM_STATES 					6
CONST_INT MAX_NUM_CLIENT_STATES				9
CONST_INT MAX_NUM_TRANSITIONS				6
CONST_INT MAX_NUM_PEDS						20
CONST_INT MAX_NUM_PROPS						30
CONST_INT MAX_NUM_VEHICLES					13
CONST_INT MAX_NUM_TRIGGER_AREAS				6
CONST_INT MAX_NUM_SCENARIOS					16
CONST_INT MAX_NUM_GOTO_LOCATIONS			2
CONST_INT MAX_NUM_SEARCH_AREAS				1
CONST_INT MAX_NUM_TAGS						11
CONST_INT MAX_NUM_HELP_TEXTS				3
CONST_INT MAX_NUM_PED_GROUP_TRIGGERS		2
CONST_INT MAX_NUM_AMBUSH_VEHICLES			3
CONST_INT MAX_NUM_PATROLS					4
CONST_INT MAX_NUM_PED_GROUPS				21
CONST_INT MAX_NUM_PED_BEHAVIOURS 			2
CONST_INT MAX_NUM_ATTACH_PROPS				2
CONST_INT MAX_NUM_PED_TRIGGERS				7
CONST_INT MAX_NUM_PATROL_NODES				10
CONST_INT MAX_NUM_MISSION_ENTITIES			3
CONST_INT MAX_NUM_LEAVE_AREAS 				1
CONST_INT MAX_NUM_POCKET_ITEMS 				1
CONST_INT MAX_NUM_FOCUS_CAMS				2

CONST_INT ENABLE_LOSE_THE_COPS				1

// --- Mission Specific ---
CONST_INT MAD_SUPPLY_COUNT					3
CONST_FLOAT MAD_SUPPLY_COLLECT_RADIUS 		5.0

//----------------------
//	SERVER VARIABLES
//----------------------

STRUCT MISSION_SPECIFIC_SERVER_DATA
	
	ACID_LAB_SOURCE_VARIATION eMissionVariation = ASOV_INVALID
	ACID_LAB_SOURCE_SUBVARIATION eMissionSubvariation = ASOS_INVALID
	SCRIPT_TIMER stTimer
	
	NETWORK_INDEX niVeh 
	INT iVehId = -1
	
	INT iVanCheckedServerBitSet
ENDSTRUCT

STRUCT MISSION_SPECIFIC_CLIENT_DATA
	INT iCheckedVan = -1
ENDSTRUCT

//----------------------
//	LOCAL VARIABLES
//----------------------

STRUCT MISSION_SPECIFIC_LOCAL_VARIABLES
	INT iVehicleCheck
	BLIP_INDEX blipDropoff
ENDSTRUCT
MISSION_SPECIFIC_LOCAL_VARIABLES sLocalVariables

//----------------------
//	BITSET WRAPPERS
//----------------------

ENUM eMISSION_SERVER_BITSET
	eMISSIONSERVERBITSET_SHOW_TRACTOR,
	eMISSIONSERVERBITSET_RIGHT_VAN_FOUND,
	eMISSIONSERVERBITSET_SUPPLIES_COLLECTED,
	eMISSIONSERVERBITSET_SUPPLIES_DELIVERED,
	eMISSIONSERVERBITSET_SUPPLIES_UNLOCKED,
	eMISSIONSERVERBITSET_PLANE_START,
	eMISSIONSERVERBITSET_PLANE_COMPLETE,
	eMISSIONSERVERBITSET_PLANE_FADING_OUT,
	eMISSIONSERVERBITSET_PLANE_CLEANED_UP,
	
	eMISSIONSERVERBITSET_END
ENDENUM

ENUM eMISSION_CLIENT_BITSET
	eMISSIONCLIENTBITSET_SHOW_TRACTOR,
	eMISSIONCLIENTBITSET_I_HAVE_FOUND_THE_VAN,
	eMISSIONCLIENTBITSET_I_HAVE_COLLECTED_SUPPLIES,
	eMISSIONCLIENTBITSET_I_HAVE_DELIVERED_SUPPLIES,
	eMISSIONCLIENTBITSET_I_HAVE_UNLOCKED_SUPPLIES,
	eMISSIONCLIENTBITSET_PLANE_START,
	eMISSIONCLIENTBITSET_PLANE_COMPLETE,
	eMISSIONCLIENTBITSET_PLANE_FADING_OUT,
	eMISSIONCLIENTBITSET_PLANE_CLEANED_UP,
	
	eMISSIONCLIENTBITSET_END
ENDENUM

FUNC eMISSION_SERVER_BITSET GET_MISSION_SERVER_BIT_FOR_CLIENT_BIT(eMISSION_CLIENT_BITSET eClientBit)
	SWITCH eClientBit
		CASE eMISSIONCLIENTBITSET_SHOW_TRACTOR							RETURN eMISSIONSERVERBITSET_SHOW_TRACTOR
		CASE eMISSIONCLIENTBITSET_I_HAVE_FOUND_THE_VAN					RETURN eMISSIONSERVERBITSET_RIGHT_VAN_FOUND
		CASE eMISSIONCLIENTBITSET_I_HAVE_COLLECTED_SUPPLIES				RETURN eMISSIONSERVERBITSET_SUPPLIES_COLLECTED
		CASE eMISSIONCLIENTBITSET_I_HAVE_DELIVERED_SUPPLIES 			RETURN eMISSIONSERVERBITSET_SUPPLIES_DELIVERED
		CASE eMISSIONCLIENTBITSET_I_HAVE_UNLOCKED_SUPPLIES				RETURN eMISSIONSERVERBITSET_SUPPLIES_UNLOCKED
		CASE eMISSIONCLIENTBITSET_PLANE_START 							RETURN eMISSIONSERVERBITSET_PLANE_START
		CASE eMISSIONCLIENTBITSET_PLANE_COMPLETE 						RETURN eMISSIONSERVERBITSET_PLANE_COMPLETE
		CASE eMISSIONCLIENTBITSET_PLANE_FADING_OUT						RETURN eMISSIONSERVERBITSET_PLANE_FADING_OUT
		CASE eMISSIONCLIENTBITSET_PLANE_CLEANED_UP 						RETURN eMISSIONSERVERBITSET_PLANE_CLEANED_UP
		
		CASE eMISSIONCLIENTBITSET_END									RETURN eMISSIONSERVERBITSET_END
	ENDSWITCH
	
	RETURN eMISSIONSERVERBITSET_END
ENDFUNC

ENUM eMISSION_LOCAL_BITSET
	eMISSIONLOCALBITSET_VEHICLE_DOORS_SET,
	eMISSIONLOCALBITSET_WANTED_LEVEL_SET,
	eMISSIONLOCALBITSET_ADDED_PLANE_TO_MIX_GROUP,
	
	eMISSIONLOCALBITSET_END
ENDENUM

#IF IS_DEBUG_BUILD
FUNC STRING GET_MISSION_SERVER_BIT_NAME(eMISSION_SERVER_BITSET eBit)
	SWITCH eBit
		CASE eMISSIONSERVERBITSET_SHOW_TRACTOR							RETURN "eMISSIONSERVERBITSET_SHOW_TRACTOR"
		CASE eMISSIONSERVERBITSET_RIGHT_VAN_FOUND						RETURN "eMISSIONSERVERBITSET_RIGHT_VAN_FOUND"
		CASE eMISSIONSERVERBITSET_SUPPLIES_COLLECTED					RETURN "eMISSIONSERVERBITSET_SUPPLIES_COLLECTED"
		CASE eMISSIONSERVERBITSET_SUPPLIES_UNLOCKED 					RETURN "eMISSIONSERVERBITSET_SUPPLIES_UNLOCKED"
		CASE eMISSIONSERVERBITSET_PLANE_START 							RETURN "eMISSIONSERVERBITSET_PLANE_START"
		CASE eMISSIONSERVERBITSET_PLANE_COMPLETE 						RETURN "eMISSIONSERVERBITSET_PLANE_COMPLETE"
		CASE eMISSIONSERVERBITSET_PLANE_FADING_OUT 						RETURN "eMISSIONSERVERBITSET_PLANE_FADING_OUT"
		CASE eMISSIONSERVERBITSET_PLANE_CLEANED_UP						RETURN "eMISSIONSERVERBITSET_PLANE_CLEANED_UP"
		
		CASE eMISSIONSERVERBITSET_END									RETURN "eMISSIONSERVERBITSET_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
ENDFUNC

FUNC STRING GET_MISSION_CLIENT_BIT_NAME(eMISSION_CLIENT_BITSET eBit)
	SWITCH eBit
		CASE eMISSIONCLIENTBITSET_SHOW_TRACTOR						RETURN "eMISSIONCLIENTBITSET_SHOW_TRACTOR"
		CASE eMISSIONCLIENTBITSET_I_HAVE_FOUND_THE_VAN				RETURN "eMISSIONCLIENTBITSET_I_HAVE_FOUND_THE_VAN"
		CASE eMISSIONCLIENTBITSET_I_HAVE_COLLECTED_SUPPLIES			RETURN "eMISSIONCLIENTBITSET_I_HAVE_COLLECTED_SUPPLIES"
		CASE eMISSIONCLIENTBITSET_I_HAVE_UNLOCKED_SUPPLIES 			RETURN "eMISSIONCLIENTBITSET_I_HAVE_UNLOCKED_SUPPLIES"
		CASE eMISSIONCLIENTBITSET_PLANE_START						RETURN "eMISSIONCLIENTBITSET_PLANE_START"
		CASE eMISSIONCLIENTBITSET_PLANE_COMPLETE					RETURN "eMISSIONCLIENTBITSET_PLANE_COMPLETE"
		CASE eMISSIONCLIENTBITSET_PLANE_FADING_OUT 					RETURN "eMISSIONCLIENTBITSET_PLANE_FADING_OUT"
		CASE eMISSIONCLIENTBITSET_PLANE_CLEANED_UP					RETURN "eMISSIONCLIENTBITSET_PLANE_CLEANED_UP" 
		
		CASE eMISSIONCLIENTBITSET_END								RETURN "eMISSIONCLIENTBITSET_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
ENDFUNC

FUNC STRING GET_MISSION_LOCAL_BIT_NAME(eMISSION_LOCAL_BITSET eBit)
	SWITCH eBit
		CASE eMISSIONLOCALBITSET_VEHICLE_DOORS_SET						RETURN "eMISSIONLOCALBITSET_VEHICLE_DOORS_SET"
		CASE eMISSIONLOCALBITSET_WANTED_LEVEL_SET						RETURN "eMISSIONLOCALBITSET_WANTED_LEVEL_SET"
		CASE eMISSIONLOCALBITSET_ADDED_PLANE_TO_MIX_GROUP 				RETURN "eMISSIONLOCALBITSET_ADDED_PLANE_TO_MIX_GROUP"
		
		CASE eMISSIONLOCALBITSET_END									RETURN "eMISSIONLOCALBITSET_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
ENDFUNC
#ENDIF

