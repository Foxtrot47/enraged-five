USING "globals.sch"
USING "rage_builtins.sch"

CONST_INT ENABLE_CONTENT	FEATURE_DLC_2_2022

#IF ENABLE_CONTENT
USING "fm_content_convoy_structs.sch"
USING "fm_content_random_events_structs.sch"
USING "fm_content_generic_structs.sch"

//----------------------
//	Enums
//----------------------

ENUM eGC_MODE_STATES
	eGC_MODESTATE_STEAL_VEHICLE,
	eGC_MODESTATE_LOSE_THE_COPS,
	eGC_MODESTATE_DELIVER_VEHICLE,
	eGC_MODESTATE_END
ENDENUM

ENUM eGC_CLIENT_MODE_STATES
	eGC_CLIENTMODESTATE_STEAL_VEHICLE,
	eGC_CLIENTMODESTATE_LOSE_THE_COPS,
	eGC_CLIENTMODESTATE_DELIVER_VEHICLE,
	eGC_CLIENTMODESTATE_END
ENDENUM

ENUM eGC_TAGS
	eGC_TAG_MISSION_VEHICLE,
	eGC_TAG_DELIVERABLE
ENDENUM

ENUM eGC_PED_DRIVER_TASKS
	eGC_PEDDRIVER_DRIVE,
	eGC_PEDDRIVER_ATTACK_HATED_TARGETS,
	eGC_PEDDRIVER_WANDER
ENDENUM

ENUM eGC_PED_PASSENGER_TASKS
	eGC_PEDPASSENGER_IDLE,
	eGC_PEDPASSENGER_EXIT_VEH,
	eGC_PEDPASSENGER_ATTACK_HATED_TARGETS
ENDENUM

ENUM eGC_HELP_TEXTS
	eGC_HELPTEXT_PLAYER_OWNS_FACILITY,
	eGC_HELPTEXT_PLAYER_DOESNT_OWN_ANY_FACILITY
ENDENUM

ENUM eGC_TEXT_MESSAGES
	eGC_TXTMSG_BUNKER_FACILITY,
	eGC_TXTMSG_BIKER_FACILITIES,
	eGC_TXTMSG_NO_BUNKER_OWNED,
	eGC_TXTMSG_NO_BIKER_OWNED
ENDENUM

//----------------------
//	VARIATIONS
//----------------------

FUNC GANG_CONVOY_VARIATION GET_VARIATION()
	RETURN serverBD.sMissionData.eMissionVariation
ENDFUNC

FUNC GANG_CONVOY_SUBVARIATION GET_SUBVARIATION()
	RETURN serverBD.sMissionData.eMissionSubvariation
ENDFUNC

FUNC STRING GET_VARIATION_NAME_FOR_DEBUG()
	RETURN GANG_CONVOY_GET_VARIATION_NAME_FOR_DEBUG(GET_VARIATION())
ENDFUNC

FUNC STRING GET_SUBVARIATION_NAME_FOR_DEBUG()
	RETURN GANG_CONVOY_GET_SUBVARIATION_NAME_FOR_DEBUG(GET_SUBVARIATION())
ENDFUNC

FUNC BOOL SETUP_VARIATION(INT iVariation, INT iSubvariation)

	serverBD.sMissionData.eMissionVariation = INT_TO_ENUM(GANG_CONVOY_VARIATION, iVariation)
	serverBD.sMissionData.eMissionSubvariation = INT_TO_ENUM(GANG_CONVOY_SUBVARIATION, iSubvariation)
	
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] SETUP_VARIATION - Variation: ", GET_VARIATION_NAME_FOR_DEBUG())
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] SETUP_VARIATION - Subvariation: ", GET_SUBVARIATION_NAME_FOR_DEBUG())
	
	
	RETURN TRUE
ENDFUNC

FUNC STRING GET_MISSION_PLACEMENT_DATA_FILENAME()
	RETURN GANG_CONVOY_GET_MISSION_PLACEMENT_DATA_FILENAME(GET_VARIATION(), GET_SUBVARIATION())
ENDFUNC

////////////////////////////////////
///    CONSTANTS AND LOCAL VARIABLES 
////////////////////////////////////
CONST_INT ciTIME_BEFORE_HELP_TEXT				10000 // 10 sec
CONST_INT ciTIME_BEFORE_TEXT_MESSAGE			10000 // 10 sec

CONST_INT ciFACTORY_MATERIALS_AMOUNT_REWARD		2 // 1 crate = 20% materials

CONST_INT ciMISSION_ENTITY_INDEX				0

//////////////////////////
///    HELPERS 
//////////////////////////

FUNC eGC_MODE_STATES GC_GET_MODE_STATE()
	RETURN INT_TO_ENUM(eGC_MODE_STATES, GET_MODE_STATE_ID())
ENDFUNC

FUNC eGC_CLIENT_MODE_STATES GC_GET_CLIENT_MODE_STATE()
	RETURN INT_TO_ENUM(eGC_CLIENT_MODE_STATES, GET_CLIENT_MODE_STATE_ID())
ENDFUNC

FUNC INT GC_GET_MISSION_VEHICLE_INDEX()
	RETURN GET_TAG_ID(eGC_TAG_MISSION_VEHICLE)
ENDFUNC

FUNC NETWORK_INDEX GC_GET_MISSION_VEHICLE_NETWORK_INDEX()
	RETURN serverBD.sVehicle[GC_GET_MISSION_VEHICLE_INDEX()].netId
ENDFUNC

FUNC BOOL GC_FIND_MISSION_VEHICLE_ID(VEHICLE_INDEX &vehIndex)

	NETWORK_INDEX netVehicle = GC_GET_MISSION_VEHICLE_NETWORK_INDEX()
	IF NETWORK_DOES_NETWORK_ID_EXIST(netVehicle)
		vehIndex = NET_TO_VEH(netVehicle)
		IF DOES_ENTITY_EXIST(vehIndex)
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC NETWORK_INDEX GC_GET_MISSION_DELIVERABLE_NETWORK_INDEX()
	RETURN serverBD.sProp[ciMISSION_ENTITY_INDEX].netID
ENDFUNC

FUNC INT GC_GET_MISSION_ENTITY_INDEX()
	RETURN GET_TAG_ID(eGC_TAG_DELIVERABLE)
ENDFUNC

FUNC BOOL GC_IS_PED_A_DRIVER(INT iPed)
	IF data.Ped.Peds[iPed].eSeat = VS_DRIVER
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// This does not include subvariation 1
FUNC FACTORY_TYPE GC_GET_FACTORY_TYPE_FROM_SUBVARIATION()
	SWITCH GET_SUBVARIATION()
		CASE GCS_LOCATION_2
			RETURN FACTORY_TYPE_METH
		BREAK
		CASE GCS_LOCATION_3
			RETURN FACTORY_TYPE_WEED
		BREAK
		CASE GCS_LOCATION_4
			RETURN FACTORY_TYPE_CRACK
		BREAK
		CASE GCS_LOCATION_5
			RETURN FACTORY_TYPE_FAKE_MONEY
		BREAK
		CASE GCS_LOCATION_6
			RETURN FACTORY_TYPE_FAKE_IDS
		BREAK
	ENDSWITCH

	RETURN FACTORY_TYPE_INVALID
ENDFUNC

FUNC BOOL GC_DOES_LOCAL_PLAYER_OWN_THE_RIGHT_FACILITIES()
	IF GET_SUBVARIATION() = GCS_LOCATION_1
		IF DOES_PLAYER_OWN_A_BUNKER(GB_GET_LOCAL_PLAYER_GANG_BOSS())
			RETURN TRUE
		ENDIF
	ELSE
		IF DOES_PLAYER_OWN_FACTORY_OF_TYPE(LOCAL_PLAYER_INDEX, GC_GET_FACTORY_TYPE_FROM_SUBVARIATION())
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

///////////////////////
///    OBJECTIVE TEXT
///////////////////////

PROC GC_OBJECTIVE_TEXT_STEAL_VEHICLE()
	Print_Objective_Text_With_Coloured_Text_Label("GC_STEAL", "GC_VEH", HUD_COLOUR_BLUE)	
ENDPROC

PROC GC_OBJECTIVE_TEXT_DELIVER()
	STRING sLocation = "GC_GARAGE" // Default for subvariations 2,3,4 and 5.

	SWITCH GET_SUBVARIATION()
		CASE GCS_LOCATION_1
			IF DOES_PLAYER_OWN_A_BUNKER(GB_GET_THIS_PLAYER_GANG_BOSS(LOCAL_PLAYER_INDEX))
				sLocation = "GC_BUNKER"
			ELSE
				sLocation = "GC_AMMU_N"
			ENDIF
		BREAK
		CASE GCS_LOCATION_2
			IF DOES_PLAYER_OWN_FACTORY_OF_TYPE(LOCAL_PLAYER_INDEX, FACTORY_TYPE_METH)
				sLocation = "GC_METH_LAB"
			ENDIF
		BREAK
		CASE GCS_LOCATION_3
			IF DOES_PLAYER_OWN_FACTORY_OF_TYPE(LOCAL_PLAYER_INDEX, FACTORY_TYPE_WEED)
				sLocation = "GC_W_FARM"
			ENDIF
		BREAK
		CASE GCS_LOCATION_4
			IF DOES_PLAYER_OWN_FACTORY_OF_TYPE(LOCAL_PLAYER_INDEX, FACTORY_TYPE_CRACK)
				sLocation = "GC_C_LOCKUP"
			ENDIF
		BREAK
		CASE GCS_LOCATION_5
			IF DOES_PLAYER_OWN_FACTORY_OF_TYPE(LOCAL_PLAYER_INDEX, FACTORY_TYPE_FAKE_MONEY)
				sLocation = "GC_C_CASH"
			ENDIF
		BREAK
		CASE GCS_LOCATION_6
			IF DOES_PLAYER_OWN_FACTORY_OF_TYPE(LOCAL_PLAYER_INDEX, FACTORY_TYPE_FAKE_IDS)
				sLocation = "GC_DOC_OFFICE"
			ENDIF
		BREAK
	ENDSWITCH
	
	VEHICLE_INDEX vehIndex
	IF GC_FIND_MISSION_VEHICLE_ID(vehIndex)
	AND IS_PED_SITTING_IN_VEHICLE_SEAT(LOCAL_PED_INDEX, vehIndex, VS_DRIVER)
		// Driver
		Print_Objective_Text_With_Two_Strings_Coloured("GC_DELIVER", "GC_VEH", sLocation, HUD_COLOUR_WHITE, HUD_COLOUR_YELLOW)
	ELSE
		// Other Participants
		Print_Objective_Text_With_Two_Strings_Coloured("GC_DELIVER_H", "GC_VEH", sLocation, HUD_COLOUR_WHITE, HUD_COLOUR_YELLOW)
	ENDIF
	
ENDPROC

PROC GC_OBJECTIVE_TEXT_LOSE_COPS()
	Print_Objective_Text("GC_LOSE_COPS")
ENDPROC

////////////////////////////////
///  MODE STATES CONDITIONS ///
//////////////////////////////

FUNC BOOL GC_SHOULD_MOVE_TO_LOSE_THE_COPS()
	IF GET_SUBVARIATION() = GCS_LOCATION_1
		IF IS_VEHICLE_BIT_SET(GC_GET_MISSION_VEHICLE_INDEX(), eVEHICLEBITSET_VEHICLE_HAS_BEEN_ENTERED)
		OR IS_MISSION_SERVER_BIT_SET(eMISSIONSERVERBITSET_VEHICLE_HAS_BEEN_DAMAGED)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GC_SHOULD_GO_STRAIGHT_TO_DELIVER()
	
	IF GET_SUBVARIATION() != GCS_LOCATION_1
	AND IS_VEHICLE_BIT_SET(GC_GET_MISSION_VEHICLE_INDEX(), eVEHICLEBITSET_VEHICLE_HAS_BEEN_ENTERED)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC GC_SETUP_MODE_STATES()
	ADD_MODE_STATE(eGC_MODESTATE_STEAL_VEHICLE, eMODESTATE_CUSTOM) // To match better with the client
		ADD_MODE_STATE_TRANSITION_CUSTOM(eGC_MODESTATE_STEAL_VEHICLE, eGC_MODESTATE_LOSE_THE_COPS, &GC_SHOULD_MOVE_TO_LOSE_THE_COPS)
		ADD_MODE_STATE_TRANSITION_CUSTOM(eGC_MODESTATE_STEAL_VEHICLE, eGC_MODESTATE_DELIVER_VEHICLE, &GC_SHOULD_GO_STRAIGHT_TO_DELIVER)
		
	ADD_MODE_STATE(eGC_MODESTATE_LOSE_THE_COPS, eMODESTATE_LOSE_THE_COPS)
		ADD_MODE_STATE_TRANSITION_DEFAULT(eGC_MODESTATE_LOSE_THE_COPS, eGC_MODESTATE_DELIVER_VEHICLE)
	
	ADD_MODE_STATE(eGC_MODESTATE_DELIVER_VEHICLE, eMODESTATE_DELIVER_MISSION_ENTITY)
		ADD_MODE_STATE_TRANSITION_DEFAULT(eGC_MODESTATE_DELIVER_VEHICLE, eGC_MODESTATE_END)
		
	ADD_MODE_STATE(eGC_MODESTATE_END, eMODESTATE_END)
ENDPROC

PROC GC_SETUP_CLIENT_MODE_STATES()
	
	ADD_CLIENT_MODE_STATE(eGC_CLIENTMODESTATE_STEAL_VEHICLE, eMODESTATE_CUSTOM, &GC_OBJECTIVE_TEXT_STEAL_VEHICLE)
		ADD_CLIENT_MODE_STATE_TRANSITION_LINKED(eGC_CLIENTMODESTATE_STEAL_VEHICLE, eGC_CLIENTMODESTATE_LOSE_THE_COPS, eGC_MODESTATE_LOSE_THE_COPS)
		ADD_CLIENT_MODE_STATE_TRANSITION_LINKED(eGC_CLIENTMODESTATE_STEAL_VEHICLE, eGC_CLIENTMODESTATE_DELIVER_VEHICLE, eGC_MODESTATE_DELIVER_VEHICLE)
		
	ADD_CLIENT_MODE_STATE(eGC_CLIENTMODESTATE_LOSE_THE_COPS, eMODESTATE_LOSE_THE_COPS, &GC_OBJECTIVE_TEXT_LOSE_COPS)
		ADD_CLIENT_MODE_STATE_TRANSITION_LINKED(eGC_CLIENTMODESTATE_LOSE_THE_COPS, eGC_CLIENTMODESTATE_DELIVER_VEHICLE, eGC_MODESTATE_DELIVER_VEHICLE)
	
	ADD_CLIENT_MODE_STATE(eGC_CLIENTMODESTATE_DELIVER_VEHICLE, eMODESTATE_DELIVER_MISSION_ENTITY, &GC_OBJECTIVE_TEXT_DELIVER)
		ADD_CLIENT_MODE_STATE_TRANSITION_LINKED(eGC_CLIENTMODESTATE_DELIVER_VEHICLE, eGC_CLIENTMODESTATE_END, eGC_MODESTATE_END)
		
	ADD_CLIENT_MODE_STATE(eGC_CLIENTMODESTATE_END, eMODESTATE_END, &EMPTY)
ENDPROC

///////////////////////
///    VEHICLE
///////////////////////

PROC GC_VEHICLE_ON_DAMAGED(INT iVehicle, STRUCT_ENTITY_DAMAGE_EVENT sEvent, PLAYER_INDEX damager)
	
	UNUSED_PARAMETER(sEvent)
	UNUSED_PARAMETER(damager)
	
	IF iVehicle = GC_GET_MISSION_VEHICLE_INDEX()
	AND NOT IS_MISSION_SERVER_BIT_SET(eMISSIONSERVERBITSET_VEHICLE_HAS_BEEN_DAMAGED)
		SET_MISSION_CLIENT_BIT(eMISSIONCLIENTBITSET_VEHICLE_HAS_BEEN_DAMAGED)
	ENDIF
	
ENDPROC

FUNC BOOL GC_VEHICLE_BLIP_ENABLE(INT iVehicle)
	IF iVehicle = GC_GET_MISSION_VEHICLE_INDEX()
		
		VEHICLE_INDEX vehId = GET_VEHICLE_INDEX_FROM_TAG(eGC_TAG_MISSION_VEHICLE)
		IF DOES_ENTITY_EXIST(vehId)
		AND NOT IS_ANY_PLAYER_IN_VEHICLE(vehId) // This includes the dead checks already.
			RETURN TRUE
		ENDIF
		
	ENDIF
	
	
	RETURN FALSE
ENDFUNC

////////////////////////////////////
///    WANTED / LOSE THE COPS (LTC)
////////////////////////////////////

FUNC BOOL GC_LTC_SHOULD_TRIGGER()
	RETURN GC_GET_MODE_STATE() = eGC_MODESTATE_LOSE_THE_COPS
ENDFUNC

FUNC BOOL GC_WANTED_SHOULD_FAKE()
	RETURN GC_LTC_SHOULD_TRIGGER() AND GET_PLAYER_WANTED_LEVEL(LOCAL_PLAYER_INDEX) > 0
ENDFUNC

///////////////////
///    AMBUSH
///////////////////

FUNC BOOL GC_AMBUSH_SHOULD_ACTIVATE()
	IF GET_SUBVARIATION() = GCS_LOCATION_1
		RETURN GC_LTC_SHOULD_TRIGGER() // To activate it as soon as player enters the vehicle = lose the cops stage.
	ENDIF
	
	RETURN GC_GET_MODE_STATE() = eGC_MODESTATE_DELIVER_VEHICLE
ENDFUNC

///////////////////////////
///    DROPOFF SETTINGS
///////////////////////////

FUNC VECTOR GC_DELIVERY_DROPOFF_COORDS()
	
	IF GET_SUBVARIATION() = GCS_LOCATION_1
	AND DOES_PLAYER_OWN_A_BUNKER(GB_GET_LOCAL_PLAYER_GANG_BOSS())
		RETURN GET_BUNKER_COORDS(GET_OWNED_BUNKER(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
	ENDIF
	
	IF DOES_PLAYER_OWN_FACTORY_OF_TYPE(LOCAL_PLAYER_INDEX, GC_GET_FACTORY_TYPE_FROM_SUBVARIATION())
		FACTORY_ID eFactoryID = GET_FACTORY_ID_OF_OWNED_FACTORY_TYPE(LOCAL_PLAYER_INDEX, GC_GET_FACTORY_TYPE_FROM_SUBVARIATION())
		//GET_FACTORY_ENTRY_LOCATE(GET_SIMPLE_INTERIOR_ID_FROM_FACTORY_ID(eFactoryID), vFactoryCoords)
		RETURN GET_FACTORY_DROP_OFF_COORDS(eFactoryID)
	ENDIF
	
	//FREEMODE_DELIVERY_DROPOFF_DETAILS_BS_IGNORE_PRIMARY_DROPOFF
	
	IF GET_SUBVARIATION() = GCS_LOCATION_1
		RETURN <<822.5, -2143.1, 28.8>> // Ammu-Nation (Default of Subvariation 1)
	ENDIF
	
	RETURN <<854.9, -2120.2, 30.6>> // Garage (Default)
ENDFUNC

FUNC FREEMODE_DELIVERY_DROPOFFS GC_DELIVERY_DROPOFF_SETUP(INT iDropoff)
	UNUSED_PARAMETER(iDropoff)
	
	IF GET_SUBVARIATION() = GCS_LOCATION_1
	AND DOES_PLAYER_OWN_A_BUNKER(GB_GET_LOCAL_PLAYER_GANG_BOSS())
		RETURN GET_FREEMODE_DELIVERY_DROPOFF_FROM_FACTORY_ID(GET_OWNED_BUNKER(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
	ENDIF
	
	IF GET_SUBVARIATION() != GCS_LOCATION_1
	AND DOES_PLAYER_OWN_FACTORY_OF_TYPE(LOCAL_PLAYER_INDEX, GC_GET_FACTORY_TYPE_FROM_SUBVARIATION())
		FACTORY_ID eFactoryID = GET_FACTORY_ID_OF_OWNED_FACTORY_TYPE(LOCAL_PLAYER_INDEX, GC_GET_FACTORY_TYPE_FROM_SUBVARIATION())
		RETURN GET_FREEMODE_DELIVERY_DROPOFF_FROM_FACTORY_ID(eFactoryID)
	ENDIF
	
	IF GET_SUBVARIATION() = GCS_LOCATION_1
		RETURN FMC_DROPOFF_AMMU_NATION_CYPRESS
	ENDIF
	
	RETURN FMC_DROPOFF_GANG_CONVOY_GARAGE_CYPRESS
ENDFUNC

PROC ACTIVATE_FREEMODE_DELIVERY_DROPOFF(BOOL bActive)
	IF bActive
		IF FREEMODE_DELIVERY_GET_ACTIVE_LOCAL_DROPOFF() = FREEMODE_DELIVERY_DROPOFF_INVALID
			FREEMODE_DELIVERY_ACTIVATE_DROPOFF_FOR_FM_EVENT(GET_DROPOFF(), serverBD.sMissionID)
		ENDIF
	ELSE
		FREEMODE_DELIVERY_DEACTIVATE_DROPOFF_FOR_RIVAL_PACKAGE_FOR_MISSION(serverBD.sMissionID)
	ENDIF
ENDPROC

FUNC FREEMODE_DELIVERABLE_TYPE GET_MISSION_ENTITY_DELIVERABLE_TYPE()
	RETURN FMC_DELIVERABLE
ENDFUNC

///////////////////////////
///    HELP TEXT
///////////////////////////

FUNC STRING GC_HELP_TEXT(INT iHelpText)
	
	SWITCH INT_TO_ENUM(eGC_HELP_TEXTS, iHelpText)
		CASE eGC_HELPTEXT_PLAYER_OWNS_FACILITY 				RETURN "GC_HT_1"
		CASE eGC_HELPTEXT_PLAYER_DOESNT_OWN_ANY_FACILITY	RETURN "GC_HT_2"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC BOOL GC_HELP_TEXT_TRIGGER(INT iHelpText)
	
	IF GC_GET_MODE_STATE() = eGC_MODESTATE_DELIVER_VEHICLE // If the player has stolen the vehicle.
		SWITCH INT_TO_ENUM(eGC_HELP_TEXTS, iHelpText)
			CASE eGC_HELPTEXT_PLAYER_OWNS_FACILITY
				IF NOT GC_DOES_LOCAL_PLAYER_OWN_THE_RIGHT_FACILITIES() 
					RETURN FALSE
				ELSE
					RETURN HAS_NET_TIMER_EXPIRED(sLocalVariables.sTimer, ciTIME_BEFORE_HELP_TEXT)
				ENDIF
			BREAK
			CASE eGC_HELPTEXT_PLAYER_DOESNT_OWN_ANY_FACILITY
				IF GC_DOES_LOCAL_PLAYER_OWN_THE_RIGHT_FACILITIES() 
					RETURN FALSE
				ELSE
					RETURN HAS_NET_TIMER_EXPIRED(sLocalVariables.sTimer, ciTIME_BEFORE_HELP_TEXT)
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN FALSE
ENDFUNC

///////////////////////////
///    PEDS
///////////////////////////

FUNC BOOL GC_SHOULD_PED_ATTACK(INT iPed, PED_INDEX pedId)
	UNUSED_PARAMETER(pedId)
	
	RETURN HAS_PED_BEEN_TRIGGERED(iPed)
ENDFUNC

FUNC BOOL GC_SHOULD_PED_WANDER(INT iPed, PED_INDEX pedId)
	UNUSED_PARAMETER(pedId)
	
	RETURN IS_PED_BIT_SET(iPed, ePEDBITSET_I_HAVE_REACHED_FINAL_CHECKPOINT)
ENDFUNC

PROC GC_PED_BEHAVIOUR_SETUP(INT iBehaviour)
	SWITCH iBehaviour
		CASE 0
			ADD_PED_TASK(iBehaviour, eGC_PEDDRIVER_DRIVE, ePEDTASK_VEHICLE_GO_TO_POINT)
				ADD_PED_TASK_TRANSITION(iBehaviour, eGC_PEDDRIVER_DRIVE, eGC_PEDDRIVER_ATTACK_HATED_TARGETS, &GC_SHOULD_PED_ATTACK)
				ADD_PED_TASK_TRANSITION(iBehaviour, eGC_PEDDRIVER_DRIVE, eGC_PEDDRIVER_WANDER, &GC_SHOULD_PED_WANDER)
		
			ADD_PED_TASK(iBehaviour, eGC_PEDDRIVER_WANDER, ePEDTASK_WANDER_IN_VEHICLE)
				ADD_PED_TASK_TRANSITION(iBehaviour, eGC_PEDDRIVER_WANDER, eGC_PEDDRIVER_ATTACK_HATED_TARGETS, &GC_SHOULD_PED_ATTACK)
				
			ADD_PED_TASK(iBehaviour, eGC_PEDDRIVER_ATTACK_HATED_TARGETS, ePEDTASK_ATTACK_HATED_TARGETS)
		BREAK
		
		CASE 1
			ADD_PED_TASK(iBehaviour, eGC_PEDPASSENGER_IDLE, ePEDTASK_IDLE)
				ADD_PED_TASK_TRANSITION(iBehaviour, eGC_PEDPASSENGER_IDLE, eGC_PEDPASSENGER_ATTACK_HATED_TARGETS, &GC_SHOULD_PED_ATTACK)
			
			ADD_PED_TASK(iBehaviour, eGC_PEDPASSENGER_ATTACK_HATED_TARGETS, ePEDTASK_ATTACK_HATED_TARGETS)
		BREAK

	ENDSWITCH
ENDPROC

FUNC INT GC_PED_BEHAVIOUR_GET(INT iPed)
	
	IF GC_IS_PED_A_DRIVER(iPed)
		RETURN 0
	ENDIF
	
	RETURN 1
ENDFUNC

PROC GC_PED_TRIGGERS(INT iPed)
	ADD_PED_TRIGGER(iPed, &PED_TRIGGER_HAS_PED_BEEN_DAMAGED)
	ADD_PED_TRIGGER(iPed, &PED_TRIGGER_HAS_PED_VEHICLE_BEEN_DAMAGED)
ENDPROC

FUNC VECTOR GC_PED_TASK_VGOTO_COORDS(INT iPed, INT iCheckPoint)
	UNUSED_PARAMETER(iPed)
	
	SWITCH GET_SUBVARIATION()
		CASE GCS_LOCATION_1
			SWITCH iCheckPoint
				CASE 0					RETURN <<-581.7, -2236.2, 6.0>>
				CASE 1					RETURN <<-506.9, -2150.7, 9.0>>
				CASE 2					RETURN <<-476.1, -2153.1, 9.4>>
				CASE 3					RETURN <<-187.9, -2191.9, 10.3>>
				CASE 4					RETURN <<-237.6, -1849.3, 29.1>>
			ENDSWITCH
		BREAK
		CASE GCS_LOCATION_2
			SWITCH iCheckPoint
				CASE 0					RETURN <<-15.9, 6509.1, 31.3>>
				CASE 1					RETURN <<-6.6, 6524.1, 31.3>>
				CASE 2					RETURN <<66.1, 6598.0, 31.4>>
				CASE 3					RETURN <<139.3, 6538.6, 31.6>>
			ENDSWITCH
		BREAK
		CASE GCS_LOCATION_3
			SWITCH iCheckPoint
				CASE 0					RETURN <<996.7, -1774.0, 31.9>>
				CASE 1					RETURN <<1053.4, -1764.6, 35.7>>
				CASE 2					RETURN <<1124.0, -1739.9, 35.7>>
			ENDSWITCH
		BREAK
		CASE GCS_LOCATION_4
			SWITCH iCheckPoint
				CASE 0					RETURN <<1301.9, 1125.3, 105.7>>
				CASE 1					RETURN <<1292.8, 1329.8, 106.2>>
				CASE 2					RETURN <<1271.0, 1649.8, 87.9>>
			ENDSWITCH
		BREAK
		CASE GCS_LOCATION_5
			SWITCH iCheckPoint
				CASE 0					RETURN <<-478.1, -829.7, 30.4>>
				CASE 1					RETURN <<-530.2, -829.6, 29.8>>
				CASE 2					RETURN <<-710.9, -828.7, 23.5>>
			ENDSWITCH
		BREAK
		CASE GCS_LOCATION_6
			SWITCH iCheckPoint
				CASE 0					RETURN <<-1363.5, -690.6, 25.1>>
				CASE 1					RETURN <<-1439.5, -725.6, 23.6>>
				CASE 2					RETURN <<-1520.5, -686.2, 28.6>>
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN <<0, 0, 0>>
ENDFUNC	

FUNC INT GC_PED_TASK_VGOTO_NUM_COORDS(INT iPed)
	UNUSED_PARAMETER(iPed)
	
	SWITCH GET_SUBVARIATION()
		CASE GCS_LOCATION_1			RETURN	5
		CASE GCS_LOCATION_2			RETURN	4
		CASE GCS_LOCATION_3			RETURN	3
		CASE GCS_LOCATION_4			RETURN	3
		CASE GCS_LOCATION_5			RETURN	3
		CASE GCS_LOCATION_6			RETURN	3
	ENDSWITCH
	
	RETURN 0
ENDFUNC

///////////////////////////
///    REWARDS
///////////////////////////

PROC _DEPRECATED_GIVE_REWARD()
	IF IS_GENERIC_CLIENT_BIT_SET(LOCAL_PARTICIPANT_INDEX, eGENERICCLIENTBITSET_COMPLETED_REWARDS)
		EXIT
	ENDIF
	
	IF NOT GC_DOES_LOCAL_PLAYER_OWN_THE_RIGHT_FACILITIES() // To avoid getting stuck here if the player doesn't own a facility.
	OR GB_IS_PLAYER_MEMBER_OF_A_GANG(LOCAL_PLAYER_INDEX, FALSE) // To run this only on BOSS or Not in a gang.
		SET_GENERIC_CLIENT_BIT(eGENERICCLIENTBITSET_COMPLETED_REWARDS)
		EXIT
	ENDIF
	
	FACTORY_ID eFactoryID
	CONTRABAND_TRANSACTION_STATE eTransactionState
	
	IF GET_SUBVARIATION() = GCS_LOCATION_1 // ~ Bunker variation. ~
	AND GC_DOES_LOCAL_PLAYER_OWN_THE_RIGHT_FACILITIES()
		//INT iStatMaterialsTotal 					= GET_MP_INT_CHARACTER_STAT(GET_STAT_ENUM_FOR_FACTORY_MATERIALS_TOTAL_SLOT(BUNKER_SAVE_SLOT))	
		eFactoryID 									= GET_OWNED_BUNKER(LOCAL_PLAYER_INDEX)
		
		IF eFactoryID != FACTORY_ID_INVALID
			ADD_MATERIALS_TO_FACTORY(eFactoryID, eTransactionState, ciFACTORY_MATERIALS_AMOUNT_REWARD)
		ENDIF
		
	ELSE // ~ Biker business variation ~
		IF GC_DOES_LOCAL_PLAYER_OWN_THE_RIGHT_FACILITIES()

			eFactoryID 								= GET_FACTORY_ID_OF_OWNED_FACTORY_TYPE(LOCAL_PLAYER_INDEX, GC_GET_FACTORY_TYPE_FROM_SUBVARIATION())
			
			IF eFactoryID != FACTORY_ID_INVALID
				
				//INT iSaveSlot					= GET_SAVE_SLOT_FOR_FACTORY(eFactoryID)	
				
				ADD_MATERIALS_TO_FACTORY(eFactoryID, eTransactionState, ciFACTORY_MATERIALS_AMOUNT_REWARD)
				
			ENDIF
		ENDIF
	ENDIF
	
	// Set as complete once the transaction is complete.
	IF eTransactionState != CONTRABAND_TRANSACTION_STATE_DEFAULT
	AND eTransactionState != CONTRABAND_TRANSACTION_STATE_PENDING
		SET_GENERIC_CLIENT_BIT(eGENERICCLIENTBITSET_COMPLETED_REWARDS)
		PRINTLN("[DAVOR-TEST] - GIVE_REWARD - Transaction finished.")
	ENDIF

ENDPROC

PROC PROCESS_ON_DELIVERY_CASH_REWARD(INT iCashToGive)
	IF iCashToGive > 0
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] PROCESS_ON_DELIVERY_CASH_REWARD - iCashToGive = ", iCashToGive)
	
		IF USE_SERVER_TRANSACTIONS()
			INT iTransactionIndex
			TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_JOBS, iCashToGive, iTransactionIndex, FALSE, TRUE)
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] PROCESS_ON_DELIVERY_CASH_REWARD - TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION, SERVICE_EARN_JOBS - Cash $: ", iCashToGive)
		ELSE
			AMBIENT_JOB_DATA sData
			NETWORK_EARN_FROM_AMBIENT_JOB(iCashToGive, "RANDOM_EVENT_GANG_CONVOY", sData)
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] PROCESS_ON_DELIVERY_CASH_REWARD - NETWORK_EARN_FROM_AMBIENT_JOB - Cash $: ", iCashToGive)
		ENDIF
		
		sTelemetry.iCashEarned += iCashToGive
	ELSE
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] PROCESS_ON_DELIVERY_CASH_REWARD - iCashToGive <= 0")
	ENDIF
ENDPROC

PROC PROCESS_ON_DELIVERY_RP_REWARD(INT iRpToGive)
	IF iRpToGive > 0
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] PROCESS_ON_DELIVERY_RP_REWARD - iRpToGive = ", iRpToGive)
		NEXT_RP_ADDITION_SHOW_RANKBAR()
		GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "", XPTYPE_COMPLETE,XPCATEGORY_COMPLETE_MISSION_PASS, iRpToGive)
		
		sTelemetry.iRpEarned += iRpToGive
	ELSE
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] PROCESS_ON_DELIVERY_RP_REWARD - iRpToGive <= 0")
	ENDIF
ENDPROC

FUNC INT GC_ON_DELIVERY_CASH(BOOL bLocalDelivered)
	
	IF bLocalDelivered
		PRINTLN("[DAVOR-TEST] - GC_ON_DELIVERY_CASH - Giving $10,000 for delivering the vehicle.")
		RETURN 25000
	ENDIF
	RETURN 0
ENDFUNC

FUNC INT GC_ON_DELIVERY_RP(BOOL bLocalDelivered)
	IF bLocalDelivered
		PRINTLN("[DAVOR-TEST] - GC_ON_DELIVERY_RP - Giving 2,000 RP for delivering the vehicle.")
		RETURN 2000
	ENDIF
	RETURN 0
ENDFUNC

///////////////////////////
///    TEXT MESSAGES
///////////////////////////

FUNC enumCharacterList GC_TXT_MSG_CHARACTER(INT iText)
	
	SWITCH INT_TO_ENUM(eGC_TEXT_MESSAGES, iText)
		
		CASE eGC_TXTMSG_NO_BIKER_OWNED
		CASE eGC_TXTMSG_BIKER_FACILITIES
			RETURN CHAR_BIKER_CH2 // CHAR_LJT
			
		CASE eGC_TXTMSG_BUNKER_FACILITY
			RETURN CHAR_MP_AGENT_14 //CHAR_AGENT14
		
		CASE eGC_TXTMSG_NO_BUNKER_OWNED
			RETURN CHAR_AMMUNATION
	ENDSWITCH

	
	RETURN CHAR_BIKER_CH2 // CHAR_LJT
ENDFUNC

FUNC STRING GC_TXT_MSG_LABEL(INT iText)
	SWITCH INT_TO_ENUM(eGC_TEXT_MESSAGES, iText)
		CASE eGC_TXTMSG_BUNKER_FACILITY					RETURN "GC_TXT_1"
		CASE eGC_TXTMSG_BIKER_FACILITIES				RETURN "GC_TXT_2"
		CASE eGC_TXTMSG_NO_BUNKER_OWNED					RETURN "GC_TXT_3"
		CASE eGC_TXTMSG_NO_BIKER_OWNED					RETURN "GC_TXT_4"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC BOOL GC_TXT_MSG_SHOULD_SEND(INT iText)
	
	IF GC_GET_MODE_STATE() = eGC_MODESTATE_DELIVER_VEHICLE
		IF GC_DOES_LOCAL_PLAYER_OWN_THE_RIGHT_FACILITIES()
			
			IF iText = ENUM_TO_INT(eGC_TXTMSG_BUNKER_FACILITY)
				RETURN GET_SUBVARIATION() = GCS_LOCATION_1 AND HAS_NET_TIMER_EXPIRED(sLocalVariables.sTimer2, ciTIME_BEFORE_TEXT_MESSAGE)
			ENDIF
			
			IF iText = ENUM_TO_INT(eGC_TXTMSG_BIKER_FACILITIES)
				RETURN GET_SUBVARIATION() != GCS_LOCATION_1 AND HAS_NET_TIMER_EXPIRED(sLocalVariables.sTimer2, ciTIME_BEFORE_TEXT_MESSAGE)
			ENDIF
		
		ELSE
			IF iText = ENUM_TO_INT(eGC_TXTMSG_NO_BUNKER_OWNED)
				RETURN GET_SUBVARIATION() = GCS_LOCATION_1 AND HAS_NET_TIMER_EXPIRED(sLocalVariables.sTimer2, ciTIME_BEFORE_TEXT_MESSAGE)
			ENDIF
			
			IF iText = ENUM_TO_INT(eGC_TXTMSG_NO_BIKER_OWNED)
				RETURN GET_SUBVARIATION() != GCS_LOCATION_1 AND HAS_NET_TIMER_EXPIRED(sLocalVariables.sTimer2, ciTIME_BEFORE_TEXT_MESSAGE)
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

///////////////////////////
///    MISSION
///////////////////////////

// So dead bodies that were left do not just drop out of the air when the vehicle gets cleaned up.
PROC GC_DELETE_DEAD_PEDS_IN_VEH(VEHICLE_INDEX vehId)
	IF NOT IS_VEHICLE_SEAT_FREE(vehId, VS_FRONT_RIGHT, TRUE)
		PED_INDEX pedId = GET_PED_IN_VEHICLE_SEAT(vehId, VS_FRONT_RIGHT, TRUE)
		
		IF DOES_ENTITY_EXIST(pedId)
		AND NOT IS_PED_A_PLAYER(pedId)
			IF IS_PED_INJURED(pedId)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(pedId)
					DELETE_PED(pedId)
				ELSE
					TAKE_CONTROL_OF_ENTITY(pedId)
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
ENDPROC

// For subvariations where we don't enter the location we deliver to.
FUNC BOOL GC_MISSION_HOLD_END()
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GC_GET_MISSION_VEHICLE_NETWORK_INDEX())
	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GC_GET_MISSION_VEHICLE_NETWORK_INDEX())
		
		VEHICLE_INDEX vehId = GET_VEHICLE_INDEX_FROM_TAG(eGC_TAG_MISSION_VEHICLE)
		
		// So even if the mission fails, we still lock the vehicle. (Doesn't bother to call this first)
		IF IS_ENTITY_ALIVE(vehId)
		AND GET_VEHICLE_DOOR_LOCK_STATUS(vehId) != VEHICLELOCK_LOCKED
		AND MAINTAIN_CONTROL_OF_NETWORK_ID(GC_GET_MISSION_VEHICLE_NETWORK_INDEX())
			SET_VEHICLE_DOORS_LOCKED(vehId, VEHICLELOCK_LOCKED)
			PRINTLN("[GANG-CONVOY] GC_MISSION_HOLD_END - Vehicle doors locked.")
		ENDIF
		
		//  >>>> DON'T WRITE BELOW THIS LINE <<<<
		//  -------------------------------------
		
		// Using this condition so if mission failed, player is still tasked to leave veh without this blocking it.
		IF IS_MISSION_ENTITY_BIT_SET(ciMISSION_ENTITY_INDEX, eMISSIONENTITYBITSET_DELIVERED) 
			
			// These conditions need to be before tasking the player with leave vehicle so we don't interrupt the locations that got a cutscene
			IF GET_SUBVARIATION() = GCS_LOCATION_1
			AND GC_DOES_LOCAL_PLAYER_OWN_THE_RIGHT_FACILITIES()
				RETURN FALSE // To avoid glitching out the bunker cutscene
			ENDIF
		
			IF NOT GC_DOES_LOCAL_PLAYER_OWN_THE_RIGHT_FACILITIES()
				RETURN FALSE // Since we deliver to ammu-nation or garage and both got cutscenes.
			ENDIF
		ENDIF
		
		
		//  >>>> DON'T WRITE ABOVE THIS LINE <<<<
		//  -------------------------------------
		
		IF IS_ENTITY_VISIBLE(vehId)
		AND IS_ENTITY_VISIBLE(NET_TO_ENT(GC_GET_MISSION_DELIVERABLE_NETWORK_INDEX()))
			IF NOT IS_ANY_PLAYER_IN_VEHICLE(vehId)
				
				GC_DELETE_DEAD_PEDS_IN_VEH(vehId)
				
				IF MAINTAIN_CONTROL_OF_NETWORK_ID(GC_GET_MISSION_VEHICLE_NETWORK_INDEX())
				AND MAINTAIN_CONTROL_OF_NETWORK_ID(GC_GET_MISSION_DELIVERABLE_NETWORK_INDEX())
					NETWORK_FADE_OUT_ENTITY(vehId, TRUE, TRUE)
					NETWORK_FADE_OUT_ENTITY(NET_TO_ENT(GC_GET_MISSION_DELIVERABLE_NETWORK_INDEX()), TRUE, TRUE)
					PRINTLN("[DAVOR-TEST] Fading out both veh and mission ent.")
				ENDIF
			ENDIF
		ELSE
			RETURN FALSE // End mission once the veh is no longer visible.
		ENDIF
		
		IF IS_PED_IN_VEHICLE(LOCAL_PED_INDEX, vehId, TRUE)
			IF IS_LOCAL_ALIVE
				TASK_LEAVE_VEHICLE(LOCAL_PED_INDEX, vehId)
			ENDIF
		ENDIF
		
	ELSE
		RETURN FALSE // Veh doesn't exist.
	ENDIF
	
	RETURN TRUE
ENDFUNC

///////////////////////////
///    DEBUG
///////////////////////////

#IF IS_DEBUG_BUILD
PROC GC_DEBUG_ADDITIONAL_DEBUG()
	IF g_sRandomEventDebug.bWarpToGangConvoy
		NET_WARP_TO_COORD(GET_ENTITY_COORDS(GET_VEHICLE_INDEX_FROM_TAG(eGC_TAG_MISSION_VEHICLE)) + <<2.0, 1.0, 0.0>>, GET_ENTITY_HEADING(LOCAL_PED_INDEX))
		PRINTLN("[GANG-CONVOY] - GC_DEBUG_ADDITIONAL_DEBUG - Warped to Gang Convoy")
		g_sRandomEventDebug.bWarpToGangConvoy = FALSE
	ENDIF
	
	IF g_sRandomEventDebug.bWarpToAmmuNation
		NET_WARP_TO_COORD(<<822.5, -2123.1, 28.8>>, GET_ENTITY_HEADING(LOCAL_PED_INDEX), TRUE)
		PRINTLN("[GANG-CONVOY] - GC_DEBUG_ADDITIONAL_DEBUG - Warped to Ammu-Nation")
		g_sRandomEventDebug.bWarpToAmmuNation = FALSE
	ENDIF
	
ENDPROC
#ENDIF

////////////////////////////////////
///    PLACEMENT DATA
////////////////////////////////////

PROC GC_SET_INIT_PROP_POSITION_ROT_DATA()
	IF GET_SUBVARIATION() = GCS_LOCATION_1
		//data.Prop.AttachedProp[GET_TAG_ID(eGC_TAG_DELIVERABLE)].vOffset 	= <<0.0000,-2.2800,0.0000>>
		
		data.Prop.Props[GET_TAG_ID(eGC_TAG_DELIVERABLE)].vRotation 		= <<0.0000,0.0000,94.0000>>
		//data.Prop.Props[GET_TAG_ID(eGC_TAG_DELIVERABLE_1)].vRotation 		= <<0.0000,0.0000,94.0000>>
		
	ENDIF
	
	IF GET_SUBVARIATION() = GCS_LOCATION_2
		data.Prop.AttachedProp[GET_TAG_ID(eGC_TAG_DELIVERABLE)].vOffset 	= <<-0.7200,-1.1800,-0.3100>>
		//data.Prop.AttachedProp[GET_TAG_ID(eGC_TAG_DELIVERABLE_1)].vOffset 	= <<0.7300,-1.1800,-0.3100>>
		//data.Prop.AttachedProp[GET_TAG_ID(eGC_TAG_DELIVERABLE_2)].vOffset 	= <<0.0100,-1.5900,-0.3000>>
		//data.Prop.AttachedProp[GET_TAG_ID(eGC_TAG_DELIVERABLE_3)].vOffset 	= <<0.1800,-0.6200,-0.3000>>

		//data.Prop.Props[GET_TAG_ID(eGC_TAG_DELIVERABLE_1)].vRotation 		= <<0.0000,0.0000,-20.0000>>
		//data.Prop.Props[GET_TAG_ID(eGC_TAG_DELIVERABLE_2)].vRotation 		= <<0.0000,0.0000,14.0000>>
		//data.Prop.Props[GET_TAG_ID(eGC_TAG_DELIVERABLE_3)].vRotation 		= <<0.0000,0.0000,-17.0000>>
		
	ENDIF
	
	IF GET_SUBVARIATION() = GCS_LOCATION_3
		data.Prop.AttachedProp[GET_TAG_ID(eGC_TAG_DELIVERABLE)].vOffset 	= <<0.2000,0.0000,-0.4800>>
		//data.Prop.AttachedProp[GET_TAG_ID(eGC_TAG_DELIVERABLE_1)].vOffset 	= <<-0.5100,0.0200,-0.4800>>
		//data.Prop.AttachedProp[GET_TAG_ID(eGC_TAG_DELIVERABLE_2)].vOffset 	= <<-0.3600,-0.6800,-0.4800>>
		//data.Prop.AttachedProp[GET_TAG_ID(eGC_TAG_DELIVERABLE_3)].vOffset 	= <<0.2700,-0.7800,-0.4800>>
		//data.Prop.AttachedProp[GET_TAG_ID(eGC_TAG_DELIVERABLE_4)].vOffset 	= <<0.2600,-1.9200,-0.4700>>
		//data.Prop.AttachedProp[GET_TAG_ID(eGC_TAG_DELIVERABLE_5)].vOffset 	= <<-0.2100,-1.7600,-0.4400>>
	ENDIF
	
	IF GET_SUBVARIATION() = GCS_LOCATION_4
		data.Prop.AttachedProp[GET_TAG_ID(eGC_TAG_DELIVERABLE)].vOffset 	= <<0.0700,-1.8900,0.2700>>
		//data.Prop.AttachedProp[GET_TAG_ID(eGC_TAG_DELIVERABLE_1)].vOffset 	= <<0.0000,-2.6400,0.5500>>
		
		data.Prop.Props[GET_TAG_ID(eGC_TAG_DELIVERABLE)].vRotation  		= <<0.0000,0.0000,92.0000>>
		//data.Prop.Props[GET_TAG_ID(eGC_TAG_DELIVERABLE_1)].vRotation 		= <<25.0000,0.0000,0.0000>>
	ENDIF
	
	
	IF GET_SUBVARIATION() = GCS_LOCATION_5
		data.Prop.AttachedProp[GET_TAG_ID(eGC_TAG_DELIVERABLE)].vOffset 	= <<-0.1700,-0.7700,0.2500>>
		//data.Prop.AttachedProp[GET_TAG_ID(eGC_TAG_DELIVERABLE_1)].vOffset 	= <<-0.2300,-0.7600,0.3900>>
		//data.Prop.AttachedProp[GET_TAG_ID(eGC_TAG_DELIVERABLE_2)].vOffset 	= <<0.2400,-0.7600,0.3200>>
		//data.Prop.AttachedProp[GET_TAG_ID(eGC_TAG_DELIVERABLE_3)].vOffset 	= <<0.0000,-1.9600,0.1100>>
		
		data.Prop.Props[GET_TAG_ID(eGC_TAG_DELIVERABLE)].vRotation  		= <<4.0000,0.0000,0.0000>>
		//data.Prop.Props[GET_TAG_ID(eGC_TAG_DELIVERABLE_1)].vRotation 		= <<3.0000,0.0000,0.0000>>
		//data.Prop.Props[GET_TAG_ID(eGC_TAG_DELIVERABLE_2)].vRotation 		= <<0.0000,25.0000,0.0000>>
		//data.Prop.Props[GET_TAG_ID(eGC_TAG_DELIVERABLE_3)].vRotation 		= <<0.0000,0.0000,83.0000>>
	ENDIF
	
	IF GET_SUBVARIATION() = GCS_LOCATION_6
		data.Prop.AttachedProp[GET_TAG_ID(eGC_TAG_DELIVERABLE)].vOffset 	= <<-0.2900,-1.8000,0.1800>>
		//data.Prop.AttachedProp[GET_TAG_ID(eGC_TAG_DELIVERABLE_1)].vOffset 	= <<0.3800,-1.7200,0.1800>>
		//data.Prop.AttachedProp[GET_TAG_ID(eGC_TAG_DELIVERABLE_2)].vOffset 	= <<0.4000,-1.6900,0.4400>>
		
		//data.Prop.Props[GET_TAG_ID(eGC_TAG_DELIVERABLE_1)].vRotation  		= <<0.0000,0.0000,41.0000>>
	ENDIF
	
ENDPROC

PROC GC_SET_INIT_PROP_STATS_DATA()
	
	INT iProp = GC_GET_MISSION_ENTITY_INDEX()
	
	// Setting up the mission entity
	data.MissionEntity.iCount = 1
	data.MissionEntity.MissionEntities[ciMISSION_ENTITY_INDEX].iCarrierVehicle 	= GC_GET_MISSION_VEHICLE_INDEX()
	data.MissionEntity.MissionEntities[ciMISSION_ENTITY_INDEX].model			= data.Prop.Props[iProp].model
	
	// Attach the prop to the vehicle
	data.Prop.AttachedProp[0].iPropIndex = iProp
	data.Prop.AttachedProp[0].eParentType = ET_VEHICLE
	data.Prop.AttachedProp[0].iParentIndex = GC_GET_MISSION_VEHICLE_INDEX()
		
	// Bitset data below this line
	SET_PROP_DATA_BIT(iProp, ePROPDATABITSET_SPAWN_INVINCIBLE)
	SET_PROP_DATA_BIT(iProp, ePROPDATABITSET_FREEZE_PROP)
	//SET_PROP_DATA_BIT(iProp, ePROPDATABITSET_VISIBLE_IN_CUTSCENES) // Commented out - they don't seem to follow the vehicle on the cutscene.
	SET_PROP_DATA_BIT(iProp, ePROPDATABITSET_DISABLE_COLLISION)
ENDPROC

PROC GC_SET_PLACEMENT_DATA()
	PRINTLN("[DAVOR-TEST] ##### Executing SETUP_MISSION_LOGIC #####")
	
	GC_SET_INIT_PROP_STATS_DATA()
	GC_SET_INIT_PROP_POSITION_ROT_DATA()
	
ENDPROC

//----------------------
//	MISSION LOGIC
//----------------------

/// PURPOSE: Use to set up the logic pointers for your mission
PROC SETUP_MISSION_LOGIC()

	// Placement Data
	logic.Data.Setup 							=	&GC_SET_PLACEMENT_DATA
	
	logic.StateMachine.SetupStates				= 	&GC_SETUP_MODE_STATES
	logic.StateMachine.SetupClientStates		=	&GC_SETUP_CLIENT_MODE_STATES
	
	logic.Vehicle.OnDamaged 					= 	&GC_VEHICLE_ON_DAMAGED
	
	logic.Vehicle.Blip.Enable					= 	&GC_VEHICLE_BLIP_ENABLE
	
	logic.LoseTheCops.ShouldTriggerWantedLevel	=	&GC_LTC_SHOULD_TRIGGER
	logic.Wanted.ShouldFake						= 	&GC_WANTED_SHOULD_FAKE
	
	logic.Ambush.ShouldActivate					=	&GC_AMBUSH_SHOULD_ACTIVATE
	
	logic.Delivery.Dropoff.OverrideCoords		=	&GC_DELIVERY_DROPOFF_COORDS
	logic.Delivery.Dropoff.Setup				=	&GC_DELIVERY_DROPOFF_SETUP
	
	logic.HelpText.Text							=	&GC_HELP_TEXT
	logic.HelpText.Trigger						=	&GC_HELP_TEXT_TRIGGER
	
	logic.Ped.Behaviour.Setup					=	&GC_PED_BEHAVIOUR_SETUP
	logic.Ped.Behaviour.Get						=	&GC_PED_BEHAVIOUR_GET
	
	logic.Ped.Triggers.Setup					=	&GC_PED_TRIGGERS
	
	logic.Ped.Task.VehicleGoToPoint.Coords		=	&GC_PED_TASK_VGOTO_COORDS
	logic.Ped.Task.VehicleGoToPoint.NumCoords	=	&GC_PED_TASK_VGOTO_NUM_COORDS
	
	logic.Reward.OnDeliveryCash					=	&GC_ON_DELIVERY_CASH
	logic.Reward.OnDeliveryRp					=	&GC_ON_DELIVERY_RP
	
	logic.TextMessages.Character				=	&GC_TXT_MSG_CHARACTER
	logic.TextMessages.Label					=	&GC_TXT_MSG_LABEL
	logic.TextMessages.ShouldSend				=	&GC_TXT_MSG_SHOULD_SEND
	
	logic.Mission.HoldEnd						=	&GC_MISSION_HOLD_END
	
	#IF IS_DEBUG_BUILD
	logic.Debug.AdditionalDebug					=	&GC_DEBUG_ADDITIONAL_DEBUG
	#ENDIF
	
ENDPROC

#ENDIF
