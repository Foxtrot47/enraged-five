USING "globals.sch"
USING "rage_builtins.sch"

USING "fm_content_acid_lab_sell_structs.sch"
USING "fm_content_gb_mission_structs.sch"
USING "fm_content_generic_structs.sch"

//////////////////////////
///    HELPERS
//////////////////////////

FUNC ACID_LAB_SELL_VARIATION GET_VARIATION()
	RETURN serverBD.sMissionData.eMissionVariation
ENDFUNC

FUNC ACID_LAB_SELL_SUBVARIATION GET_SUBVARIATION()
	RETURN serverBD.sMissionData.eMissionSubvariation
ENDFUNC

// --- HUD ---

FUNC HUD_COLOURS ACID_LAB_SELL_MISSION_GET_PRODUCT_BLIP_HUD_COLOUR()
	RETURN HUD_COLOUR_GREEN
ENDFUNC

FUNC STRING ACID_LAB_SELL_MISSION_GET_PRODUCT_NAME()
	RETURN "Custom Acid" // TODO: Add User String for Acid Product
ENDFUNC

//////////////////////////
///  BIG MESSAGE
//////////////////////////

// --- START ---
FUNC STRING GET_START_BIG_MESSAGE_TITLE()
	RETURN "ASEV_BM_S"	
ENDFUNC

FUNC STRING GET_START_BIG_MESSAGE_STRAPLINE()

	SWITCH GET_VARIATION()
	CASE ASEV_POLICE_STING 	RETURN "ASEV_BM_S_SL01"
	DEFAULT 				RETURN "ASEV_BM_S_SL02"
	ENDSWITCH
	
ENDFUNC

// --- PASSED ---

FUNC STRING GET_END_BIG_MESSAGE_SUCCESS_TITLE()
	RETURN "ASEV_BM_P"
ENDFUNC

FUNC STRING GET_END_BIG_MESSAGE_SUCCESS_STRAPLINE()
	RETURN "ASEV_BM_P_SL"	
ENDFUNC

// --- FAILED ---

FUNC STRING GET_END_BIG_MESSAGE_FAIL_TITLE()
	RETURN "ASEV_BM_F"
ENDFUNC

FUNC STRING GET_END_BIG_MESSAGE_FAIL_STRAPLINE()
	RETURN "ASEV_BM_F_SL"	
ENDFUNC

// --- GENERAL ---

PROC DISPLAY_START_BIG_MESSAGE()
	SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_GB_START_OF_JOB, GET_START_BIG_MESSAGE_STRAPLINE(), ACID_LAB_SELL_MISSION_GET_PRODUCT_NAME(), DEFAULT, DEFAULT, GET_START_BIG_MESSAGE_TITLE(), DEFAULT)
ENDPROC

PROC DISPLAY_END_BIG_MESSAGE()
	IF IS_GENERIC_BIT_SET(eGENERICBITSET_I_WON)
		SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_GB_BUY_END_OF_JOB_SUCCESS, GET_END_BIG_MESSAGE_SUCCESS_STRAPLINE(), ACID_LAB_SELL_MISSION_GET_PRODUCT_NAME(), DEFAULT, DEFAULT, GET_END_BIG_MESSAGE_SUCCESS_TITLE(), DEFAULT)
	ELSE
		SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_GB_END_OF_JOB_FAIL, GET_END_BIG_MESSAGE_FAIL_STRAPLINE(), ACID_LAB_SELL_MISSION_GET_PRODUCT_NAME(), DEFAULT, DEFAULT, GET_END_BIG_MESSAGE_FAIL_TITLE(), DEFAULT)
	ENDIF
ENDPROC

//////////////////////////
///  SHARED FUNCTIONS
//////////////////////////

// --- REWARD ---

PROC ASEV_REWARD_GIVE_REWARD(INT iRp)

	// --- ADD RP ---
	IF iRp > 0
		
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [", GET_CURRENT_SUBVARIATION_STRING(), "] [GIVE_REWARD] - Giving player ", iRp, "RP")
		
		IF data.Reward.bShowRankbarOnRPEarn
			NEXT_RP_ADDITION_SHOW_RANKBAR()
		ENDIF
		
		GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, LOCAL_PED_INDEX, "", XPTYPE_ACTION, XPCATEGORY_FM_CONTENT_COMPLETE_TASK, iRp)

		sTelemetry.iRpEarned += iRp
	ENDIF
	
ENDPROC

//////////////////////////
///    OVERRIDES
//////////////////////////

FUNC STRING GET_MISSION_PLACEMENT_DATA_FILENAME()
	RETURN ACID_LAB_SELL_GET_MISSION_PLACEMENT_DATA_FILENAME(GET_VARIATION(), GET_SUBVARIATION())
ENDFUNC

#IF IS_DEBUG_BUILD
FUNC STRING GET_VARIATION_NAME_FOR_DEBUG()
	
	SWITCH GET_VARIATION()
		CASE ASEV_INVALID			RETURN "ASEV_INVALID"
		
		CASE ASEV_PAPERBOY			RETURN "ASEV_PAPERBOY"
		CASE ASEV_POLICE_STING		RETURN "ASEV_POLICE_STING"
		CASE ASEV_STASH_DELIVERY 	RETURN "ASEV_STASH_DELIVERY"
		
		CASE ASEV_MAX				RETURN "ASEV_MAX"
	ENDSWITCH
	
	RETURN "INVALID VARIATION"

ENDFUNC

FUNC STRING GET_SUBVARIATION_NAME_FOR_DEBUG()

	SWITCH GET_SUBVARIATION()
		CASE ASES_INVALID			RETURN "ASES_INVALID"
		
		CASE ASES_PB_LOCATION_1		RETURN "ASES_PB_LOCATION_1"
		CASE ASES_PB_LOCATION_2		RETURN "ASES_PB_LOCATION_2"
		CASE ASES_PB_LOCATION_3		RETURN "ASES_PB_LOCATION_3"
		CASE ASES_PB_LOCATION_4		RETURN "ASES_PB_LOCATION_4"
		CASE ASES_PB_LOCATION_5		RETURN "ASES_PB_LOCATION_5"
		
		CASE ASES_PS_LOCATION_1		RETURN "ASES_PS_LOCATION_1"
		CASE ASES_PS_LOCATION_2		RETURN "ASES_PS_LOCATION_2"
		CASE ASES_PS_LOCATION_3		RETURN "ASES_PS_LOCATION_3"
		CASE ASES_PS_LOCATION_4		RETURN "ASES_PS_LOCATION_4"
		CASE ASES_PS_LOCATION_5		RETURN "ASES_PS_LOCATION_5"

		CASE ASES_SD_LOCATION_1		RETURN "ASES_SD_LOCATION_1"
		CASE ASES_SD_LOCATION_2		RETURN "ASES_SD_LOCATION_2"
		CASE ASES_SD_LOCATION_3		RETURN "ASES_SD_LOCATION_3"
		CASE ASES_SD_LOCATION_4		RETURN "ASES_SD_LOCATION_4"
		CASE ASES_SD_LOCATION_5		RETURN "ASES_SD_LOCATION_5"
		
		CASE ASES_MAX				RETURN "ASES_MAX"
	ENDSWITCH
	
	RETURN "INVALID VARIATION"
	
ENDFUNC
#ENDIF

FUNC BOOL SETUP_VARIATION(INT iVariation, INT iSubvariation)
	serverBD.sMissionData.eMissionVariation = INT_TO_ENUM(ACID_LAB_SELL_VARIATION, iVariation)
	serverBD.sMissionData.eMissionSubvariation = INT_TO_ENUM(ACID_LAB_SELL_SUBVARIATION, iSubvariation)
	
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] SETUP_VARIATION - Variation: ", GET_VARIATION_NAME_FOR_DEBUG())
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] SETUP_VARIATION - Subvariation: ", GET_SUBVARIATION_NAME_FOR_DEBUG())
	
	RETURN TRUE
ENDFUNC

FUNC BOOL HOLD_END()

//	// Run the transaction stuff from technical to remove the product from the acid lab here!!!
//	IF NOT REMOVE_STUFF_FROM_ACID_LAB()
//		RETURN TRUE
//	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC FLOAT GET_DROPOFF_CORONA_Z_OVERRIDE()
	SWITCH GET_VARIATION()
		CASE ASEV_POLICE_STING		RETURN -1.0
	ENDSWITCH
	RETURN 0.7
ENDFUNC

FUNC FLOAT GET_DROPOFF_CORONA_SCALE_Z()
	SWITCH GET_VARIATION()
		CASE ASEV_POLICE_STING		RETURN 1.5
	ENDSWITCH
	RETURN 2.5
ENDFUNC

FUNC FLOAT GET_DROPOFF_CORONA_RADIUS()
	SWITCH GET_VARIATION()
		CASE ASEV_POLICE_STING		RETURN 0.5
	ENDSWITCH
	
	IF IS_PED_IN_ANY_VEHICLE(LOCAL_PED_INDEX)
		RETURN 1.5
	ENDIF

	RETURN 0.5
ENDFUNC

///////////////////////////////////
///    BIKE COUNT AND START WARP
//////////////////////////////////

FUNC INT GET_NUM_BIKES_FOR_SELL()
	#IF IS_DEBUG_BUILD
	IF MPGlobalsAmbience.sMagnateGangBossData.iWidgetRandomModeInt0 != (-1)
		RETURN CLAMP_INT(MPGlobalsAmbience.sMagnateGangBossData.iWidgetRandomModeInt0, 0, 4)
	ENDIF
	#ENDIF
	
	INT iNumGoons = GB_GET_NUM_GOONS_IN_PLAYER_GANG(GB_GET_LOCAL_PLAYER_MISSION_HOST())
	
	SWITCH iNumGoons
		CASE 0			RETURN 1
		CASE 1			RETURN 2
		CASE 2			RETURN 3
	ENDSWITCH
	
	RETURN 4
ENDFUNC

FUNC INT GET_SELL_BIKE_FOR_PLAYER_WARP()
	IF GB_GET_LOCAL_PLAYER_MISSION_HOST() = LOCAL_PLAYER_INDEX
		RETURN 0
	ELSE
		INT iGoonIndex = GB_GET_INDEX_OF_GOON(GB_GET_LOCAL_PLAYER_MISSION_HOST(), LOCAL_PLAYER_INDEX)
		IF iGoonIndex != (-1)
			RETURN iGoonIndex+1
		ENDIF
	ENDIF
	
	RETURN -1
ENDFUNC

FUNC VEHICLE_SPAWN_LOCATION_PARAMS GET_SELL_BIKE_SPAWN_PARAMS()
	VEHICLE_SPAWN_LOCATION_PARAMS Params
	Params.bConsiderHighways = TRUE
	Params.bConsiderOnlyActiveNodes = FALSE
	Params.bAvoidSpawningInExclusionZones = TRUE
	Params.bAllowFallbackToInactiveNodes = TRUE
	Params.bCheckEntityArea = TRUE
	Params.fMaxDistance = 200.0
	Params.fMinDistFromCoords = 10.0
	Params.bIgnoreCustomNodesForArea = FALSE
	Params.bCheckOwnVisibility = FALSE
	Params.fVisibleDistance = 0.0
	RETURN Params
ENDFUNC

FUNC VECTOR GET_ACID_LAB_COORD_FOR_START()
	IF DOES_ENTITY_EXIST(g_vehAcidLab[NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_MISSION_HOST())])
		RETURN GET_ENTITY_COORDS(g_vehAcidLab[NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_MISSION_HOST())], FALSE)
	ELSE
		PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] GET_ACID_LAB_COORD_FOR_START - Vehicle doesn't exist.")
	ENDIF
	RETURN <<0, 0, 0>>
ENDFUNC

PROC CLEANUP_SELL_BIKE_WARP()
	FADE_SCREEN_IN()
	DISABLE_PLAYER_CONTROL(FALSE)
	SET_MISSION_LOCAL_BIT(eMISSIONLOCALBITSET_COMPLETED_START_WARP)
	CLEAR_MISSION_LOCAL_BIT(eMISSIONLOCALBITSET_STARTED_START_WARP)
ENDPROC

FUNC BOOL SHOULD_WARP_LOCAL_ONTO_SELL_BIKE(INT iVehicle)
	IF NOT IS_MISSION_LOCAL_BIT_SET(eMISSIONLOCALBITSET_STARTED_START_WARP)
		IF GET_SIMPLE_INTERIOR_PLAYER_IS_IN(LOCAL_PLAYER_INDEX) != SIMPLE_INTERIOR_ACID_LAB
			PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] [SELL_WARP] HANDLE_ON_START_SELL_BIKE_WARP - Local isn't inside acid lab, not warping.")
			RETURN FALSE
		ENDIF

		IF iVehicle = -1
			PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] [SELL_WARP] HANDLE_ON_START_SELL_BIKE_WARP - Vehicle is -1, local doesn't have a bike assigned to them for warp.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	SET_MISSION_LOCAL_BIT(eMISSIONLOCALBITSET_STARTED_START_WARP)
	RETURN TRUE
ENDFUNC

PROC HANDLE_ON_START_SELL_BIKE_WARP(INT iVehicle)
	IF IS_MISSION_LOCAL_BIT_SET(eMISSIONLOCALBITSET_COMPLETED_START_WARP)
		EXIT
	ENDIF
	
	IF NOT SHOULD_WARP_LOCAL_ONTO_SELL_BIKE(iVehicle)
		CLEANUP_SELL_BIKE_WARP()
		PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] [SELL_WARP] HANDLE_ON_START_SELL_BIKE_WARP - Player not eligible for sell bike warp, skipping.")
		EXIT
	ENDIF
	
	IF FADE_SCREEN_OUT()
		IF NOT IS_PLAYER_CONTROL_DISABLED_BY_MISSION()
			DISABLE_PLAYER_CONTROL(TRUE, NSPC_SET_INVISIBLE)
			PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] [SELL_WARP] HANDLE_ON_START_SELL_BIKE_WARP - Removing player control for warp start.")
		ENDIF
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
		AND HAVE_ALL_MISSION_ENTITIES_BEEN_CREATED()
			VEHICLE_INDEX vehIndex = NET_TO_VEH(serverBD.sVehicle[iVehicle].netId)
			IF NOT IS_PED_IN_VEHICLE(LOCAL_PED_INDEX, NET_TO_VEH(serverBD.sVehicle[iVehicle].netId), TRUE)
				IF IS_ENTITY_ALIVE(vehIndex)
				AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(LOCAL_PED_INDEX, SCRIPT_TASK_ENTER_VEHICLE)
					TASK_ENTER_VEHICLE(LOCAL_PED_INDEX, vehIndex, DEFAULT, VS_DRIVER, DEFAULT, ECF_WARP_PED)
					PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] [SELL_WARP] HANDLE_ON_START_SELL_BIKE_WARP - Player not in vehicle #", iVehicle, ", warping to driver seat.")
				ENDIF
			ELSE
				CLEANUP_SELL_BIKE_WARP()
				PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] [SELL_WARP] HANDLE_ON_START_SELL_BIKE_WARP - Player in vehicle #", iVehicle, ", setting warp complete.")
			ENDIF
		ELSE
			IF NOT HAVE_ALL_MISSION_ENTITIES_BEEN_CREATED()
				PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] [SELL_WARP] HANDLE_ON_START_SELL_BIKE_WARP - Waiting on all mission entities to be created before warp.")
			ELSE
				PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] [SELL_WARP] HANDLE_ON_START_SELL_BIKE_WARP - Vehicle #", iVehicle, " does not exist yet.")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//////////////////////////
///    REWARDS
//////////////////////////

FUNC INT GET_DEFAULT_WINNER_CASH_REWARD()

	PLAYER_INDEX piBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
	BOOL bBoss = (piBoss = LOCAL_PLAYER_INDEX)
	INT iCashToGive, iNumUnitsSold

	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] GET_DEFAULT_WINNER_CASH_REWARD - Product taken at start: ", sLocalVariables.sRewards.iUnitsTakenAtStart)
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] GET_DEFAULT_WINNER_CASH_REWARD - Product left in the acid lab: ", sLocalVariables.sRewards.iUnitsRemaining)
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] GET_DEFAULT_WINNER_CASH_REWARD - Product total: ", (sLocalVariables.sRewards.iUnitsTakenAtStart + sLocalVariables.sRewards.iUnitsRemaining))
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] GET_DEFAULT_WINNER_CASH_REWARD - Total drops: ", sLocalVariables.sRewards.iTotalDropoffs)
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] GET_DEFAULT_WINNER_CASH_REWARD - Num drops made: ", sLocalVariables.sRewards.iDropOffCount)
	
	// Calculate the value of the contraband
	IF sLocalVariables.sRewards.iDropOffCount = sLocalVariables.sRewards.iTotalDropoffs
		iNumUnitsSold = sLocalVariables.sRewards.iUnitsTakenAtStart + sLocalVariables.sRewards.iUnitsRemaining
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] GET_DEFAULT_WINNER_CASH_REWARD - Contraband units sold: ALL - ", iNumUnitsSold)
	ELSE
		FLOAT fDropsMade = TO_FLOAT(sLocalVariables.sRewards.iDropOffCount)
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] GET_DEFAULT_WINNER_CASH_REWARD - fDropsMade: ", fDropsMade)
		FLOAT fTotalDrops = TO_FLOAT(sLocalVariables.sRewards.iTotalDropoffs)
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] GET_DEFAULT_WINNER_CASH_REWARD - fTotalDrops: ", fTotalDrops)
		FLOAT fPercentUnitsSold = ((fDropsMade / fTotalDrops) * 100.0)
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] GET_DEFAULT_WINNER_CASH_REWARD - fPercentUnitsSold: ", fPercentUnitsSold)
		INT iTotalUnits = sLocalVariables.sRewards.iUnitsTakenAtStart + sLocalVariables.sRewards.iUnitsRemaining
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] GET_DEFAULT_WINNER_CASH_REWARD - iTotalUnits: ", iTotalUnits)
		FLOAT fUnitsToBePaidFor = ((iTotalUnits / 100.0) * fPercentUnitsSold)
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] GET_DEFAULT_WINNER_CASH_REWARD - fUnitsToBePaidFor: ", fUnitsToBePaidFor)
		iNumUnitsSold = CEIL(fUnitsToBePaidFor)
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] GET_DEFAULT_WINNER_CASH_REWARD - Contraband units sold (CEIL(fUnitsToBePaidFor)): ", iNumUnitsSold, " out of ", iTotalUnits)
	ENDIF
	
	iCashToGive = 10000 //TODO: GET_PLAYER_ACID_LAB_PRODUCT_VALUE(piBoss, iNumUnitsSold)
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] GET_DEFAULT_WINNER_CASH_REWARD - Value of product successfully sold: ", iCashToGive)
	
	IF bBoss
	
		INT iNumSpectators
		INT iNumActivePlayers = GET_NUM_ACTIVE_SESSION_PLAYERS_FOR_HIGH_DEMAND_BONUS(iNumSpectators)
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] GET_DEFAULT_WINNER_CASH_REWARD - High Demand Bonus - number of players considered active ", iNumActivePlayers, "; spectating players: ", iNumSpectators)
		
		IF iNumActivePlayers > g_sMPTunables.iXM22_SELL_HIGH_DEMAND_BONUS_PLAYER_CAP
			iNumActivePlayers = g_sMPTunables.iXM22_SELL_HIGH_DEMAND_BONUS_PLAYER_CAP
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] GET_DEFAULT_WINNER_CASH_REWARD - High Demand Bonus - iNumActivePlayers is above ", g_sMPTunables.iXM22_SELL_HIGH_DEMAND_BONUS_PLAYER_CAP, "; capping at this value")
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] GET_DEFAULT_WINNER_CASH_REWARD - High Demand Bonus - new number of players considered active after cap", iNumActivePlayers, "; spectating players: ", iNumSpectators)
		ENDIF
		
		INT iHighDemandBonusPercentage = CEIL((iNumActivePlayers * g_sMPTunables.fXM22_SELL_HIGH_DEMAND_BONUS))
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] GET_DEFAULT_WINNER_CASH_REWARD - High Demand Bonus - % additional  is ", iHighDemandBonusPercentage, "%")
		
		INT iHighDemandBonusValue = (iCashToGive /100) * iHighDemandBonusPercentage
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] GET_DEFAULT_WINNER_CASH_REWARD - High Demand Bonus value is $", iHighDemandBonusValue)

		iCashToGive += iHighDemandBonusValue
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] GET_DEFAULT_WINNER_CASH_REWARD - new iCashToGive total  (iCashToGive + iHighDemandBonusValue) = ", iCashToGive)
		
	ELSE
	
		FLOAT fPercentage = g_sMPTunables.fXM22_SELL_MEMBER_REWARD_PERCENTAGE
		INT iGoonPayout = CEIL(iCashToGive * fPercentage)
		PRINTLN("[MAGNATE_GANG_BOSS] GANG_BOSS_MANAGE_REWARDS [GB$] - HANDLE_ILLICIT_GOODS_MISSION_REWARDS [GOON] - iGoonPayout = iCashToGive ($", iCashToGive, ") * fPercentage (",fPercentage, ") = $", iGoonPayout)
		
		iCashToGive = iGoonPayout
		PRINTLN("[MAGNATE_GANG_BOSS] GANG_BOSS_MANAGE_REWARDS [GB$] - HANDLE_ILLICIT_GOODS_MISSION_REWARDS [GOON] - iCashToGive = $", iCashToGive)
		
		INT iGoonPayoutCap = g_sMPTunables.iXM22_SELL_MEMBER_REWARD_CAP
		IF iCashToGive > iGoonPayoutCap
			iCashToGive = iGoonPayoutCap
			PRINTLN("[MAGNATE_GANG_BOSS] GANG_BOSS_MANAGE_REWARDS [GB$] - HANDLE_ILLICIT_GOODS_MISSION_REWARDS [GOON] - iCashToGive > iGoonPayoutCap of $", iGoonPayoutCap, ", setting iCashToGive to $", iCashToGive)
		ENDIF
	
	ENDIF
	
	IF sLocalVariables.sRewards.iExtraCash > 0
		iCashToGive += sLocalVariables.sRewards.iExtraCash
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] GET_DEFAULT_WINNER_CASH_REWARD - extra cash: ", sLocalVariables.sRewards.iExtraCash)
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] GET_DEFAULT_WINNER_CASH_REWARD - new total: ", iCashToGive)
	ENDIF

	RETURN iCashToGive
ENDFUNC

