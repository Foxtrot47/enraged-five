USING "globals.sch"
USING "rage_builtins.sch"

CONST_INT ENABLE_CONTENT	FEATURE_DLC_2_2022

#IF ENABLE_CONTENT
USING "Variations/fm_content_acid_lab_sell_paperboy.sch"
USING "Variations/fm_content_acid_lab_sell_police_sting.sch"
USING "Variations/fm_content_acid_lab_sell_stash_delivery.sch"

/// PURPOSE: Use to set up the logic pointers for your mission
///    
PROC SETUP_MISSION_LOGIC()
	SWITCH GET_VARIATION()

		CASE ASEV_PAPERBOY	 	PB_SETUP_LOGIC()	BREAK
		CASE ASEV_POLICE_STING	PS_SETUP_LOGIC()	BREAK

		CASE ASEV_INVALID
		
		BREAK
		
		DEFAULT
			#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("Please populate SETUP_MISSION_LOGIC in fm_content_acid_lab_sell_variations.sch")
			#ENDIF
		BREAK
	ENDSWITCH
ENDPROC
#ENDIF
