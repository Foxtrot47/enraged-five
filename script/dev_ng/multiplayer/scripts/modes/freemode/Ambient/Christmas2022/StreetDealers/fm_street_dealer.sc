USING "globals.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"
USING "net_wait_zero.sch"

#IF FEATURE_DLC_2_2022

USING "net_street_dealers_launch.sch"

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ VARIABLES ╞═════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

//////////////////////////////////////////////////////////////
// ENUMS

ENUM STREET_DEALERS_SERVER_STATE
	STREET_DEALERS_SERVER_STATE_LOADING,
	STREET_DEALERS_SERVER_STATE_IDLE
ENDENUM

ENUM STREET_DEALERS_CLIENT_STATE
	STREET_DEALERS_STATE_LOADING,
	STREET_DEALERS_STATE_IDLE
ENDENUM

//////////////////////////////////////////////////////////////
// STRUCTS

STRUCT STREET_DEALER_DATA
	INT iScriptBS
	
	VECTOR vDealerPedLocation
	FLOAT fDealerPedHeading
	MODEL_NAMES mDealerPedModel
	
	BLIP_INDEX blipDealer
	SCRIPT_TIMER tiDealerBlipFlashTimer
	
	STREET_DEALERS_CLIENT_STATE eState
ENDSTRUCT

STRUCT ServerBroadcastData
	STREET_DEALERS_SERVER_STATE eState = STREET_DEALERS_SERVER_STATE_LOADING
	
	INT iServerBS
	
	NETWORK_INDEX niDealerPed
ENDSTRUCT

STRUCT PlayerBroadcastData
	INT iBS
ENDSTRUCT

//////////////////////////////////////////////////////////////
// LOCAL SCRIPT BIT SETS

CONST_INT BS_STREET_DEALER_STARTED_LOADING_PED_MODEL	0

//////////////////////////////////////////////////////////////
// SERVER BIT SETS

CONST_INT SERVER_BS_RESERVE_PED_INDEX_FOR_DEALER		0
CONST_INT SERVER_BS_MIGRATE_HOST						1

/// End bit set const INTs
//////////////////////////////////////////////////////////////

CONST_INT ciDEALER_BLIP_FLASHING_TIME_MS	10000

STREET_DEALER_DATA m_StreetDealerData
ServerBroadcastData m_ServerBD
PlayerBroadcastData m_PlayerBD[NUM_NETWORK_PLAYERS]

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ FUNCTIONS ╞═════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC SCRIPT_CLEANUP()
	CDEBUG1LN(DEBUG_DEBUG_MENU, "FM_STREET_DEALER - SCRIPT_CLEANUP")
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF (NETWORK_GET_NUM_SCRIPT_PARTICIPANTS(GET_STREET_DEALER_SCRIPT_NAME()) - 1) = 0
			CLEANUP_NET_ID(m_ServerBD.niDealerPed)
			
			IF IS_BIT_SET(m_StreetDealerData.iScriptBS, BS_STREET_DEALER_STARTED_LOADING_PED_MODEL)
				SET_MODEL_AS_NO_LONGER_NEEDED(m_StreetDealerData.mDealerPedModel)
			ENDIF
			
			CDEBUG1LN(DEBUG_DEBUG_MENU, "FM_STREET_DEALER - SCRIPT_CLEANUP - Cleaned up dealer")
		ELSE
			SET_BIT(m_ServerBD.iServerBS, SERVER_BS_MIGRATE_HOST)
			
			CDEBUG1LN(DEBUG_DEBUG_MENU, "FM_STREET_DEALER - SCRIPT_CLEANUP - Setting host migration server bit")
		ENDIF
	ENDIF
	
	SAFE_REMOVE_BLIP(m_StreetDealerData.blipDealer)
	
	// Reset script launching system
	MPGlobalsAmbience.sStreetDealers.iActiveScriptLocation = -1
	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

PROC INITIALISE()
//	#IF IS_DEBUG_BUILD
//	CREATE_DEBUG_WIDGETS()
//	#ENDIF

	INT iLocation = MPGlobalsAmbience.sStreetDealers.iActiveScriptLocation
	
	m_StreetDealerData.vDealerPedLocation = GET_STREET_DEALER_LOCATION_COORD(iLocation)
	m_StreetDealerData.fDealerPedHeading = 0.0
	m_StreetDealerData.mDealerPedModel = G_M_Y_BALLAEAST_01
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, RELGROUPHASH_PLAYER)
	
	#IF IS_DEBUG_BUILD
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		CDEBUG1LN(DEBUG_DEBUG_MENU, "FM_STREET_DEALER - INITIALISE - ", GET_PLAYER_NAME(PLAYER_ID()), " is the host")
	ELSE
		CDEBUG1LN(DEBUG_DEBUG_MENU, "FM_STREET_DEALER - INITIALISE - ", GET_PLAYER_NAME(PLAYER_ID()), " is a client")
	ENDIF
	#ENDIF
ENDPROC

PROC SCRIPT_INITIALISE()
	
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, -1)
	
	// Ensures net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE, RESERVATION_LOCAL_ONLY) + 1)
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(m_ServerBD, SIZE_OF(m_ServerBD))
	
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(m_PlayerBD, SIZE_OF(m_PlayerBD))
	
	IF NOT Wait_For_First_Network_Broadcast() 
		CDEBUG1LN(DEBUG_DEBUG_MENU, "FM_STREET_DEALER - SCRIPT_INITIALISE - Failed to receive initial network broadcast.")
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		CDEBUG1LN(DEBUG_DEBUG_MENU, "FM_STREET_DEALER - SCRIPT_INITIALISE - Initialised")
	ELSE
		CDEBUG1LN(DEBUG_DEBUG_MENU, "FM_STREET_DEALER - SCRIPT_INITIALISE - NETWORK_IS_GAME_IN_PROGRESS = FALSE")
		SCRIPT_CLEANUP()
	ENDIF
	
	INITIALISE()
ENDPROC

//PURPOSE: Sets or clears a bit in the bit set if bSetBit is different from the current value
//bSetBit - TRUE sets the bit and FALSE clears it
PROC UPDATE_BIT(INT iBS, INT iBit, BOOL bSetBit)
	IF bSetBit
		IF NOT IS_BIT_SET(iBS, iBit)
			SET_BIT(iBS, iBit)
		ENDIF
	ELSE
		IF IS_BIT_SET(iBS, iBit)
			CLEAR_BIT(iBS, iBit)
		ENDIF
	ENDIF
ENDPROC

PROC SETUP_DEALER_BLIP()
	IF DOES_BLIP_EXIST(m_StreetDealerData.blipDealer)
		EXIT
	ENDIF
	
	m_StreetDealerData.blipDealer = ADD_BLIP_FOR_COORD(m_StreetDealerData.vDealerPedLocation)
	SET_BLIP_SPRITE(m_StreetDealerData.blipDealer, GET_RANDOM_EVENT_BLIP_SPRITE())
	SET_BLIP_COLOUR_FROM_HUD_COLOUR(m_StreetDealerData.blipDealer, GET_RANDOM_EVENT_BLIP_COLOUR())
	SET_BLIP_SCALE(m_StreetDealerData.blipDealer, GET_RANDOM_EVENT_BLIP_SCALE())
	SET_BLIP_AS_SHORT_RANGE(m_StreetDealerData.blipDealer, TRUE)
	SET_BLIP_PRIORITY(m_StreetDealerData.blipDealer, BLIPPRIORITY_HIGH)
	
	START_NET_TIMER(m_StreetDealerData.tiDealerBlipFlashTimer, TRUE)
	SET_BLIP_FLASHES(m_StreetDealerData.blipDealer, TRUE)
	
	CDEBUG1LN(DEBUG_DEBUG_MENU, "FM_STREET_DEALER - SETUP_DEALER_BLIP - Created dealer blip and started flashing")
ENDPROC

FUNC BOOL SETUP_DEALER_PED()
	IF NOT IS_BIT_SET(m_ServerBD.iServerBS, SERVER_BS_RESERVE_PED_INDEX_FOR_DEALER)
		IF CAN_RESERVE_NETWORK_PEDS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_PEDS() + 1, FALSE, TRUE)
			RESERVE_NETWORK_MISSION_PEDS(GET_NUM_RESERVED_MISSION_PEDS() + 1)
			SET_BIT(m_ServerBD.iServerBS, SERVER_BS_RESERVE_PED_INDEX_FOR_DEALER)
			
			CPRINTLN(DEBUG_DEBUG_MENU, "FM_STREET_DEALER - SETUP_DEALER_PED - SERVER_BS_RESERVE_PED_INDEX_FOR_DEALER true")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT CAN_REGISTER_MISSION_ENTITIES(1, 0, 0, 0)
		CPRINTLN(DEBUG_DEBUG_MENU, "FM_STREET_DEALER - SETUP_DEALER_PED - Cannot register mission entities")
		RETURN FALSE
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(m_ServerBD.niDealerPed)
		CDEBUG1LN(DEBUG_DEBUG_MENU, "FM_STREET_DEALER - SETUP_DEALER_PED - Ped already exists")
		RETURN TRUE
	ENDIF
	
	VECTOR vSpawnPos = m_StreetDealerData.vDealerPedLocation
	FLOAT fSpawnHeading = m_StreetDealerData.fDealerPedHeading
	MODEL_NAMES mDealerPedModel = m_StreetDealerData.mDealerPedModel
	
	IF NOT REQUEST_LOAD_MODEL(mDealerPedModel)
		UPDATE_BIT(m_StreetDealerData.iScriptBS, BS_STREET_DEALER_STARTED_LOADING_PED_MODEL, TRUE)
		RETURN FALSE
	ENDIF
	
	PED_INDEX pedDealer = CREATE_PED(PEDTYPE_MISSION, mDealerPedModel, vSpawnPos, fSpawnHeading)
	
	IF DOES_ENTITY_EXIST(pedDealer)
	AND NOT IS_PED_INJURED(pedDealer)
		SET_ENTITY_INVINCIBLE(pedDealer, TRUE)
		SET_ENTITY_CAN_BE_DAMAGED(pedDealer, FALSE)
		SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedDealer, FALSE)
		SET_PED_CONFIG_FLAG(pedDealer, PCF_DontActivateRagdollFromVehicleImpact, TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedDealer, RELGROUPHASH_PLAYER)
		
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDealer, TRUE)
		TASK_START_SCENARIO_IN_PLACE(pedDealer, "WORLD_HUMAN_STAND_IMPATIENT")
		FREEZE_ENTITY_POSITION(pedDealer, TRUE)
		
		SET_MODEL_AS_NO_LONGER_NEEDED(mDealerPedModel)
		UPDATE_BIT(m_StreetDealerData.iScriptBS, BS_STREET_DEALER_STARTED_LOADING_PED_MODEL, FALSE)
		
		m_ServerBD.niDealerPed = PED_TO_NET(pedDealer)
		
		CDEBUG1LN(DEBUG_DEBUG_MENU, "FM_STREET_DEALER - SETUP_DEALER_PED - Created dealer ped at ", vSpawnPos)
		RETURN TRUE
	ENDIF
	
	CDEBUG1LN(DEBUG_DEBUG_MENU, "FM_STREET_DEALER - SETUP_DEALER_PED - Failed to create dealer ped")
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
FUNC STRING GET_SERVER_STATE_STRING(STREET_DEALERS_SERVER_STATE eState)
	SWITCH eState
		CASE STREET_DEALERS_SERVER_STATE_LOADING	RETURN "STREET_DEALERS_SERVER_STATE_LOADING"
		CASE STREET_DEALERS_SERVER_STATE_IDLE		RETURN "STREET_DEALERS_SERVER_STATE_IDLE"
	ENDSWITCH
	
	RETURN ""
ENDFUNC
#ENDIF

PROC SET_SERVER_STATE(STREET_DEALERS_SERVER_STATE newServerState)
	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_DEBUG_MENU, "FM_STREET_DEALER - Changing server state from ", GET_SERVER_STATE_STRING(m_ServerBD.eState), " to ", GET_SERVER_STATE_STRING(newServerState))
	#ENDIF
	
	m_ServerBD.eState = newServerState
ENDPROC

FUNC BOOL HAS_PLAYER_LEFT_DEALER_SCRIPT_RANGE()
	FLOAT fDist = VDIST(MPGlobals.sFreemodeCache.vPlayersLocation, m_StreetDealerData.vDealerPedLocation)
	
	IF fDist > SDL_GET_STREET_DEALER_LAUNCH_RANGE()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_HOST_MIGRATION()
	IF IS_BIT_SET(m_ServerBD.iServerBS, SERVER_BS_MIGRATE_HOST)
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			CLEAR_BIT(m_ServerBD.iServerBS, SERVER_BS_MIGRATE_HOST)
			
			CDEBUG1LN(DEBUG_DEBUG_MENU, "FM_STREET_DEALER - MAINTAIN_HOST_MIGRATION - ", GET_PLAYER_NAME(PLAYER_ID()), " is already the host of this script")
			EXIT
		ENDIF
		
		// Finds the first active participant from all participants, if that is us then we become the host, otherwise exits the function. This ensures that only one participant requests to be the host.
		INT i
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
				PLAYER_INDEX playerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
				IF IS_NET_PLAYER_OK(playerId, FALSE)
					CDEBUG1LN(DEBUG_DEBUG_MENU, "FM_STREET_DEALER - MAINTAIN_HOST_MIGRATION - Next host will be ", GET_PLAYER_NAME(playerId), ". Current player is ", GET_PLAYER_NAME(PLAYER_ID()))
					
					IF playerId = PLAYER_ID()
						NETWORK_REQUEST_TO_BE_HOST_OF_THIS_SCRIPT()
						CLEAR_BIT(m_ServerBD.iServerBS, SERVER_BS_MIGRATE_HOST)
						
						CDEBUG1LN(DEBUG_DEBUG_MENU, "FM_STREET_DEALER - MAINTAIN_HOST_MIGRATION - Setting ", GET_PLAYER_NAME(PLAYER_ID()), " as the host of this script")
					ENDIF
					
					EXIT
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ MAIN LOGIC PROC ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC RUN_MAIN_CLIENT_LOGIC()
	SETUP_DEALER_BLIP()
	
	IF HAS_NET_TIMER_STARTED_AND_EXPIRED(m_StreetDealerData.tiDealerBlipFlashTimer, ciDEALER_BLIP_FLASHING_TIME_MS, TRUE)
		SET_BLIP_FLASHES(m_StreetDealerData.blipDealer, FALSE)
		RESET_NET_TIMER(m_StreetDealerData.tiDealerBlipFlashTimer)
		
		CDEBUG1LN(DEBUG_DEBUG_MENU, "FM_STREET_DEALER - Stopped flashing dealer blip")
	ENDIF
ENDPROC

PROC RUN_MAIN_SERVER_LOGIC()
	SWITCH m_ServerBD.eState
		CASE STREET_DEALERS_SERVER_STATE_LOADING
			IF SETUP_DEALER_PED()
				SET_SERVER_STATE(STREET_DEALERS_SERVER_STATE_IDLE)
			ENDIF
		BREAK
		
		CASE STREET_DEALERS_SERVER_STATE_IDLE
			
		BREAK
	ENDSWITCH
ENDPROC
#ENDIF

SCRIPT
	#IF FEATURE_DLC_2_2022
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		SCRIPT_INITIALISE()
	ELSE
		SCRIPT_CLEANUP()
	ENDIF
	
	WHILE TRUE
		MP_LOOP_WAIT_ZERO()
				
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_DEBUG_MENU, "FM_STREET_DEALER - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
			#ENDIF
			SCRIPT_CLEANUP()
		ENDIF
		IF HAS_PLAYER_LEFT_DEALER_SCRIPT_RANGE()
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_DEBUG_MENU, "FM_STREET_DEALER - HAS_PLAYER_LEFT_DEALER_SCRIPT_RANGE = TRUE")
			#ENDIF
			SCRIPT_CLEANUP()
		ENDIF
		
		RUN_MAIN_CLIENT_LOGIC()
		
		MAINTAIN_HOST_MIGRATION()
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			RUN_MAIN_SERVER_LOGIC()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_F, KEYBOARD_MODIFIER_NONE, "Mission Fail")
			SCRIPT_CLEANUP()
		ENDIF
		#ENDIF
		
	ENDWHILE
	
	#ENDIF	// FEATURE_DLC_2_2022
ENDSCRIPT