USING "globals.sch"
USING "rage_builtins.sch"

CONST_INT ENABLE_CONTENT	FEATURE_DLC_2_2022

#IF ENABLE_CONTENT
USING "Variations/fm_content_acid_lab_source_chemical_spill.sch"
USING "Variations/fm_content_acid_lab_source_deludomal_vans.sch"
USING "Variations/fm_content_acid_lab_source_missing_air_drop.sch"
USING "Variations/fm_content_acid_lab_source_stealing_grain.sch"
USING "Variations/fm_content_acid_lab_source_volatile_chemicals.sch"

/// PURPOSE: Use to set up the logic pointers for your mission
///    
PROC SETUP_MISSION_LOGIC()
	SWITCH GET_VARIATION()
		CASE ASOV_STEALING_GRAIN		SG_SETUP_LOGIC()		BREAK
		CASE ASOV_DELUDOMAL_VANS		VANS_SETUP_LOGIC()		BREAK
//		CASE ASOV_VOLATILE_CHEMICALS	VC_SETUP_LOGIC()		BREAK
		CASE ASOV_MISSING_AIR_DROP 		MAD_SETUP_LOGIC() 		BREAK
		CASE ASOV_INVALID
		
		BREAK
		
		DEFAULT
			#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("Please populate SETUP_MISSION_LOGIC in fm_content_<filename>_variations.sch")
			#ENDIF
		BREAK
	ENDSWITCH
ENDPROC
#ENDIF
