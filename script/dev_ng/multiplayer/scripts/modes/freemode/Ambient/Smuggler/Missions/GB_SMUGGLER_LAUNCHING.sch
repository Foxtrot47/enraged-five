USING "globals.sch"
USING "net_prints.sch"
USING "net_include.sch"
USING "commands_misc.sch"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Written by:  William Kennedy, Ryan Elliott & Martin McMillan.														//
// Date: 		25/05/2017																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "GB_SMUGGLER_COORDS.sch"
USING "GB_SMUGGLER_DROPOFFS.sch"

//////////////////////////////////////
/// SMUGGLER LOCATION SPAWN CHECKS ///
//////////////////////////////////////

FUNC INT GET_MINIMUM_DISTANCE_FROM_HANGAR_FOR_SMUGGLER_MISSION(SMUGGLER_VARIATION eVariation)
	SWITCH eVariation
		
		CASE SMV_SELL_CONTESTED						
		CASE SMV_SELL_AGILE_DELIVERY				
		CASE SMV_SELL_PRECISION_DELIVERY			
		CASE SMV_SELL_FLY_LOW						
		CASE SMV_SELL_AIR_DELIVERY					
		CASE SMV_SELL_AIR_POLICE					
		CASE SMV_SELL_UNDER_THE_RADAR				
			RETURN g_sMPTunables.iSMUG_SELL_LAUNCH_DISTANCE_CHECK_SELL_MULTIPLE_MISSIONS
		
		CASE SMV_SETUP_RON							RETURN g_sMPTunables.iSMUG_SETUP_LAUNCH_DISTANCE_CHECK_SETUP
	ENDSWITCH
	
	IF IS_SMUGGLER_VARIATION_A_SELL_MISSION(eVariation)
		RETURN g_sMPTunables.iSMUG_SELL_LAUNCH_DISTANCE_CHECK_SELL_SINGLE_MISSIONS
	ENDIF
	RETURN g_sMPTunables.iSMUG_STEAL_LAUNCH_DISTANCE_CHECK_STEAL_SINGLE_MISSIONS
ENDFUNC

FUNC INT GET_MAXIMUM_DISTANCE_FROM_HANGAR_FOR_SMUGGLER_MISSION()

	RETURN 10000

ENDFUNC
 
FUNC VECTOR GET_SPAWN_COORD_FOR_SMUGGLER_DISTANCE_CHECK(SMUGGLER_VARIATION eVariation, SMUGGLER_LOCATION eLocation)
	SWITCH eVariation
		CASE SMV_DOGFIGHT 		RETURN GET_NON_SMUGGLER_VEHICLE_SPAWN_COORDS(eVariation, eLocation, 0, DEFAULT)
		CASE SMV_ROOF_ATTACK 	RETURN GET_SMUGGLER_PED_SPAWN_COORDS(0, eLocation, eVariation)
		CASE SMV_CARGO_PLANE	RETURN GET_SMV_CARGO_PLANE_VEHICLE_SPAWN_COORDS(0, eLocation)
	ENDSWITCH
	
	RETURN GET_SMUGGLER_ENTITY_SPAWN_COORDS(eVariation, eLocation, 0, TRUE)
ENDFUNC

FUNC BOOL IS_SMUGGLER_LOCATION_IN_SAFE_RANGE(SMUGGLER_LOCATION eLocation, SMUGGLER_VARIATION eVariation, PLAYER_INDEX playerId, BOOL bIgnoreDistanceCheck = FALSE)
	IF bIgnoreDistanceCheck
	#IF IS_DEBUG_BUILD
	OR GET_COMMANDLINE_PARAM_EXISTS("sc_SmugglerFlowPlayVariations")
	#ENDIF
		PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] IS_SMUGGLER_LOCATION_IN_SAFE_RANGE - RETURN TRUE - Ignoring distance check.")
		RETURN TRUE
	ENDIF
	
	VECTOR vDistanceCheckLocation = GET_SPAWN_COORD_FOR_SMUGGLER_DISTANCE_CHECK(eVariation, eLocation)
	 
	IF NOT IS_VECTOR_ZERO(vDistanceCheckLocation)
		
		IF GET_SIMPLE_INTERIOR_ID_FROM_HANGAR_ID(GET_PLAYERS_OWNED_HANGAR(playerId)) != SIMPLE_INTERIOR_INVALID
			FLOAT fDistance = VDIST(g_SimpleInteriorData.vMidPoints[GET_SIMPLE_INTERIOR_ID_FROM_HANGAR_ID(GET_PLAYERS_OWNED_HANGAR(playerId))], vDistanceCheckLocation)
			IF fDistance < GET_MINIMUM_DISTANCE_FROM_HANGAR_FOR_SMUGGLER_MISSION(eVariation) OR fDistance > GET_MAXIMUM_DISTANCE_FROM_HANGAR_FOR_SMUGGLER_MISSION()
				PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] IS_SMUGGLER_LOCATION_IN_SAFE_RANGE - Distance between hangar and location is outside range (", GET_MINIMUM_DISTANCE_FROM_HANGAR_FOR_SMUGGLER_MISSION(eVariation) ," - ",GET_MAXIMUM_DISTANCE_FROM_HANGAR_FOR_SMUGGLER_MISSION() ,") - Location ", GET_SMUGGLER_LOCATION_NAME(eLocation), " not valid.")
				RETURN FALSE
			ENDIF
		ELSE
			PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] IS_SMUGGLER_LOCATION_IN_SAFE_RANGE - RETURN TRUE - No hangar detected.")
		ENDIF
	ELSE
		PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] IS_SMUGGLER_LOCATION_IN_SAFE_RANGE - RETURN TRUE - Smuggler entity spawn coords is <<0.0, 0.0, 0.0>>. Probably a moving vehicle.")
	ENDIF
	
	UNUSED_PARAMETER(playerId)
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_SMUGGLER_LOCATION_IN_USE(INT iLocation, PLAYER_INDEX playerID)

	INT iPlayer
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		IF iPlayer != NATIVE_TO_INT(playerID)
			IF GB_GET_LOCATION_RESERVATION_FOR_MISSION(FMMC_TYPE_SMUGGLER_BUY, iPlayer) = iLocation
			OR GB_GET_LOCATION_RESERVATION_FOR_MISSION(FMMC_TYPE_SMUGGLER_SELL, iPlayer) = iLocation
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_SMUGGLER_LOCATION_SAFE_TO_SPAWN_ENTITIES(SMUGGLER_LOCATION eLocation, SMUGGLER_VARIATION eVariation, SMUGGLER_SUBVARIATION eSubvariation, PLAYER_INDEX playerID)

	MODEL_NAMES model
	FLOAT fRadius
	
	INT iNumEntities = GB_GET_NUM_SMUGGLER_ENTITIES_FOR_VARIATION(eVariation,GB_GET_NUM_GOONS_IN_PLAYER_GANG(playerID)+1,playerID)

	// Smuggler Mission Entities
	INT iMissionEntity	
	REPEAT iNumEntities iMissionEntity

		model = GET_SMUGGLER_ENTITY_MODEL(eVariation, eSubvariation, eSGT_MIXED)
		fRadius = GET_RADIUS_FROM_CONTRABAND_ENTITY(model)
		
		VECTOR vMissionEntityCoords = GET_SMUGGLER_LOCATION_ENTITY_SPAWN_COORDS(eLocation, iMissionEntity, TRUE) 

		PRINTLN("[SMUGGLER] - IS_SMUGGLER_LOCATION_SAFE_TO_SPAWN_ENTITIES - Checking - Mission entity #",iMissionEntity," at ",vMissionEntityCoords," with radius ",fRadius, " with model ",GET_MODEL_NAME_FOR_DEBUG(model))
		IF NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION(vMissionEntityCoords,fRadius,fRadius,fRadius,0.0,FALSE,FALSE,FALSE,0.0,FALSE,DEFAULT,FALSE,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			PRINTLN("[SMUGGLER] - IS_SMUGGLER_LOCATION_SAFE_TO_SPAWN_ENTITIES - FALSE - Mission entity #",iMissionEntity," spawn point is not safe.")
			RETURN FALSE
		ENDIF
		
	ENDREPEAT
	
	// Non-mission vehicles
	INT iNonMissionVehicle
	WHILE ( NOT IS_VECTOR_ZERO(GET_NON_SMUGGLER_VEHICLE_SPAWN_COORDS(eVariation, eLocation, iNonMissionVehicle)) )
	
		VECTOR vNonCoords = GET_NON_SMUGGLER_VEHICLE_SPAWN_COORDS(eVariation, eLocation, iNonMissionVehicle)
		model = GET_NON_SMUGGLER_VEHICLE_MODEL(eVariation, eSubvariation, iNonMissionVehicle)
		fRadius = GET_RADIUS_FROM_CONTRABAND_ENTITY(model)
		
		PRINTLN("[SMUGGLER] - IS_SMUGGLER_LOCATION_SAFE_TO_SPAWN_ENTITIES - Checking - Non-mission vehicle #",iNonMissionVehicle," at ",vNonCoords," with radius ",fRadius, " with model ",GET_MODEL_NAME_FOR_DEBUG(model))
		IF NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION(vNonCoords,fRadius,fRadius,fRadius,0.0,FALSE,FALSE,FALSE,0.0,FALSE,DEFAULT,FALSE,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			PRINTLN("[SMUGGLER] - IS_SMUGGLER_LOCATION_SAFE_TO_SPAWN_ENTITIES - FALSE - Non-mission vehicle ",iNonMissionVehicle," not safe.")
			RETURN FALSE
		ENDIF
		
		iNonMissionVehicle++
	ENDWHILE
	
	//Mission peds
	INT iPed
	WHILE (NOT IS_VECTOR_ZERO(GET_SMUGGLER_PED_SPAWN_COORDS(iPed, eLocation, eVariation)))
		
		VECTOR vPedCoords = GET_SMUGGLER_PED_SPAWN_COORDS(iPed, eLocation, eVariation)
		model = mp_m_freemode_01
		fRadius = 2
		
		PRINTLN("[SMUGGLER] - IS_SMUGGLER_LOCATION_SAFE_TO_SPAWN_ENTITIES - Checking - Ped #",iPed," at ",vPedCoords," with radius ",fRadius)
		IF NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION(vPedCoords,fRadius,fRadius,fRadius,0.0,FALSE,FALSE,FALSE,0.0,FALSE,DEFAULT,FALSE,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			PRINTLN("[SMUGGLER] - IS_SMUGGLER_LOCATION_SAFE_TO_SPAWN_ENTITIES - FALSE - Ped #",iPed," is not safe.")
			RETURN FALSE
		ENDIF
		
		iPed++
	ENDWHILE
	
	RETURN TRUE
	
	UNUSED_PARAMETER(playerId)
	UNUSED_PARAMETER(eSubvariation)
ENDFUNC

/// Returns false if the variation does not require checks against entity spawn coords to launch.
FUNC BOOL DOES_SMUGGLER_VARIATION_REQUIRE_ENTITY_COORD_LAUNCHING_CHECK(SMUGGLER_VARIATION eVariation)
	
//	SWITCH eVariation
//		CASE SMV_DESTROY_TRUCKS
//		CASE SMV_FLY_SWATTER
//			RETURN FALSE
//	ENDSWITCH

	UNUSED_PARAMETER(eVariation)
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_SMUGGLER_LOCATION_SAFE(SMUGGLER_LOCATION eLocation, SMUGGLER_VARIATION eVariation, SMUGGLER_SUBVARIATION eSubvariation, PLAYER_INDEX playerID)
	
	// Specific variations do not require spawn checks since they are not fixed entity locations
	IF NOT DOES_SMUGGLER_VARIATION_REQUIRE_ENTITY_COORD_LAUNCHING_CHECK(eVariation)
		RETURN TRUE
	ENDIF
	
	VECTOR vMissionEntityCoords
	vMissionEntityCoords = GET_SMUGGLER_LOCATION_ENTITY_SPAWN_COORDS(eLocation, 0, TRUE)

	//Check if in view of any players
	IF NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION(vMissionEntityCoords,0,0,0,DEFAULT,TRUE,TRUE,DEFAULT,DEFAULT,DEFAULT,DEFAULT,FALSE)
		PRINTLN("[SMUGGLER] - IS_SMUGGLER_LOCATION_SAFE - location in view of player, location not valid.")
		RETURN FALSE
	ENDIF
	
	//Add entity coords checks here
	IF NOT IS_SMUGGLER_LOCATION_SAFE_TO_SPAWN_ENTITIES(eLocation, eVariation, eSubvariation, playerID)
		PRINTLN("[SMUGGLER] - IS_SMUGGLER_LOCATION_SAFE - location not valid to spawn entities")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

////////////////////////////////////////
/// SMUGGLER LAUNCH LOCATION SELECTION ///
////////////////////////////////////////

FUNC BOOL IS_SMUGGLER_LOCATION_DISABLED(SMUGGLER_LOCATION eLocation)
	SWITCH eLocation
		CASE SmugglerLocation_BeaconGrab_Location1				RETURN FALSE
		CASE SmugglerLocation_BombRoof_Route1					RETURN TRUE
		CASE SmugglerLocation_BombRoof_Route3					RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_SMUGGLER_LOCATION_A_GLOBAL_RESUPPLY_LOCATION(SMUGGLER_LOCATION eLocation)
//	SWITCH eLocation
//
//			RETURN TRUE
//	ENDSWITCH
	
	UNUSED_PARAMETER(eLocation)
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_SMUGGLER_LOCATION_SUITABLE_FOR_SELL_VARIATION(SMUGGLER_LOCATION eLocation, SMUGGLER_VARIATION eVariation)
	UNUSED_PARAMETER(eLocation)
	UNUSED_PARAMETER(eVariation)
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_SMUGGLER_LOCATION_SUITABLE_FOR_VARIATION_AND_SUBVARIATION(SMUGGLER_LOCATION eLocation, SMUGGLER_VARIATION eVariation, SMUGGLER_SUBVARIATION eSubvariation)
	
	// SELL
	IF IS_SMUGGLER_VARIATION_A_SELL_MISSION(eVariation)
		RETURN IS_SMUGGLER_LOCATION_SUITABLE_FOR_SELL_VARIATION(eLocation, eVariation)
	ENDIF

	SWITCH eVariation
		
		// RESUPPLY MISSIONS
		
		// Steal Aircraft
		CASE SMV_STEAL_AIRCRAFT
			SWITCH eSubvariation
				CASE SMS_SA_SANDY_SHORES			RETURN (eLocation = SmugglerLocation_StealAircraft_SandyShores)
				CASE SMS_SA_MCKENZIE_AIRFIELD		RETURN (eLocation = SmugglerLocation_StealAircraft_McKenzieAirfield)
				CASE SMS_SA_EPSILON_HQ				RETURN (eLocation = SmugglerLocation_StealAircraft_EpsilonHQ)
			ENDSWITCH
		BREAK
		
		CASE SMV_SETUP_RON
			SWITCH eSubvariation
				CASE SMS_SETUP_DELPERROBEACH			RETURN (eLocation = SmugglerLocation_Setup_DelPerroBeach)
				CASE SMS_SETUP_LSRIVER			RETURN (eLocation = SmugglerLocation_Setup_LSRiver)
				CASE SMS_SETUP_ELBURRO			RETURN (eLocation = SmugglerLocation_Setup_ElBurro)
				CASE SMS_SETUP_CHUMASH			RETURN (eLocation = SmugglerLocation_Setup_Chumash)
				CASE SMS_SETUP_TATAVIUM			RETURN (eLocation = SmugglerLocation_Setup_Tutuavium)
				CASE SMS_SETUP_RONSWINDFARM			RETURN (eLocation = SmugglerLocation_Setup_RonsWindFarm)
				CASE SMS_SETUP_GRANDSENORA1			RETURN (eLocation = SmugglerLocation_Setup_SenoraDesert1)
				CASE SMS_SETUP_GRANDSENORA2			RETURN (eLocation = SmugglerLocation_Setup_SenoraDesert2)
				CASE SMS_SETUP_SANDY			RETURN (eLocation = SmugglerLocation_Setup_SandyShores)
				CASE SMS_SETUP_STAB			RETURN (eLocation = SmugglerLocation_Setup_StabCity)
				CASE SMS_SETUP_CALAFIA		RETURN (eLocation = SmugglerLocation_Setup_NCalafiaWay)
				CASE SMS_SETUP_MCKENZIE		RETURN (eLocation = SmugglerLocation_Setup_McKenzie)
				CASE SMS_SETUP_GRAPESEED		RETURN (eLocation = SmugglerLocation_Setup_Grapeseed)
				CASE SMS_SETUP_PORCOPIO		RETURN (eLocation = SmugglerLocation_Setup_ProcopioBeach)
				CASE SMS_SETUP_PALETO		RETURN (eLocation = SmugglerLocation_Setup_Paleto)
			ENDSWITCH
		BREAK
		
		// Thermal Scope
		CASE SMV_THERMAL_SCOPE
			SWITCH eSubvariation
				CASE SMS_TS_QUARRY					RETURN (eLocation = SmugglerLocation_ThermalScope_Quarry)
				CASE SMS_TS_POWER_STATION			RETURN (eLocation = SmugglerLocation_ThermalScope_PowerStation)
				CASE SMS_TS_PLAYBOY_MANSION			RETURN (eLocation = SmugglerLocation_ThermalScope_PlayboyMansion)
			ENDSWITCH
		BREAK
		
		// Infiltration
		CASE SMV_INFILTRATION
			SWITCH eSubvariation
				CASE SMS_INF_HUMANE_LABS			RETURN (eLocation = SmugglerLocation_Infiltration_HumaneLabs)
				CASE SMS_INF_BOLINGBROKE_PRISON		RETURN (eLocation = SmugglerLocation_Infiltration_BolingbrokePrison)
				CASE SMS_INF_ZANCUDO				RETURN (eLocation = SmugglerLocation_Infiltration_Zancudo)
			ENDSWITCH
		BREAK
		
		// Stunt Pilot
		CASE SMV_STUNT_PILOT
			SWITCH eSubvariation
				CASE SMS_SP_LOCATION_1				RETURN (eLocation = SmugglerLocation_StuntPilot_Location1)
				CASE SMS_SP_LOCATION_2              RETURN (eLocation = SmugglerLocation_StuntPilot_Location2)
				CASE SMS_SP_LOCATION_3              RETURN (eLocation = SmugglerLocation_StuntPilot_Location3)
				CASE SMS_SP_LOCATION_4              RETURN (eLocation = SmugglerLocation_StuntPilot_Location4)
				CASE SMS_SP_LOCATION_5              RETURN (eLocation = SmugglerLocation_StuntPilot_Location5)
				CASE SMS_SP_LOCATION_6              RETURN (eLocation = SmugglerLocation_StuntPilot_Location6)
				CASE SMS_SP_LOCATION_7              RETURN (eLocation = SmugglerLocation_StuntPilot_Location7)
				CASE SMS_SP_LOCATION_8              RETURN (eLocation = SmugglerLocation_StuntPilot_Location8)
				CASE SMS_SP_LOCATION_9              RETURN (eLocation = SmugglerLocation_StuntPilot_Location9)
				CASE SMS_SP_LOCATION_10             RETURN (eLocation = SmugglerLocation_StuntPilot_Location10)
			ENDSWITCH
		BREAK
		
		// Bomb Base
		CASE SMV_BOMB_BASE
			SWITCH eSubvariation
				CASE SMS_BB_TERMINAL				RETURN (eLocation = SmugglerLocation_BombBase_Terminal)
				CASE SMS_BB_GRAPESEED_AUTOYARD		RETURN (eLocation = SmugglerLocation_BombBase_GrapeseedAutoyard)
				CASE SMS_BB_PLANE_SCRAPYARD			RETURN (eLocation = SmugglerLocation_BombBase_PlaneScrapyard)
			ENDSWITCH
		BREAK
		
		// Splash Landing
		CASE SMV_SPLASH_LANDING
			SWITCH eSubvariation
				CASE SMS_SL_LAGO_ZANCUDO			RETURN (eLocation = SmugglerLocation_SplashLanding_LagoZancudo)
				CASE SMS_SL_PACIFIC_OCEAN_E			RETURN (eLocation = SmugglerLocation_SplashLanding_PacificOceanE)
				CASE SMS_SL_PACIFIC_OCEAN_NE		RETURN (eLocation = SmugglerLocation_SplashLanding_PacificOceanNE)
			ENDSWITCH
		BREAK
		
		// Blackbox
		CASE SMV_BLACK_BOX
			SWITCH eSubvariation
				CASE SMS_BLB_MOUNT_GORDO			RETURN (eLocation = SmugglerLocation_Blackbox_MountGordo)
				CASE SMS_BLB_PALETO_FOREST			RETURN (eLocation = SmugglerLocation_Blackbox_PaletoForest)
				CASE SMS_BLB_ISLAND		RETURN (eLocation = SmugglerLocation_Blackbox_Island)
			ENDSWITCH
		BREAK
		
		// Dogfight
		CASE SMV_DOGFIGHT
			SWITCH eSubvariation
				CASE SMS_DF_PLACEHOLDER_1			RETURN (eLocation = SmugglerLocation_Dogfight_Placeholder1)
				CASE SMS_DF_PLACEHOLDER_2			RETURN (eLocation = SmugglerLocation_Dogfight_Placeholder2)
				CASE SMS_DF_PLACEHOLDER_3			RETURN (eLocation = SmugglerLocation_Dogfight_Placeholder3)
			ENDSWITCH
		BREAK
		
		// Cargo Plane
		CASE SMV_CARGO_PLANE
			SWITCH eSubvariation
				CASE SMS_CP_PLACEHOLDER_1			RETURN (eLocation = SmugglerLocation_CargoPlane_Placeholder1)
				CASE SMS_CP_PLACEHOLDER_2			RETURN (eLocation = SmugglerLocation_CargoPlane_Placeholder2)
				CASE SMS_CP_PLACEHOLDER_3			RETURN (eLocation = SmugglerLocation_CargoPlane_Placeholder3)
			ENDSWITCH
		BREAK
		
		// Roof Attack
		CASE SMV_ROOF_ATTACK
			SWITCH eSubvariation
				CASE SMS_RA_CONSTRUCTION_SITE		RETURN (eLocation = SmugglerLocation_RoofAttack_ConstructionSite)
				CASE SMS_RA_ALTA_HOTEL				RETURN (eLocation = SmugglerLocation_RoofAttack_AltaHotel)
				CASE SMS_RA_PACIFIC_STANDARD		RETURN (eLocation = SmugglerLocation_RoofAttack_PacificStandard)
			ENDSWITCH
		BREAK
		
		// Bombing Run
		CASE SMV_BOMBING_RUN
			SWITCH eSubvariation
				CASE SMS_BR_PLACEHOLDER_1			RETURN (eLocation = SmugglerLocation_BombingRun_Placeholder1)
				CASE SMS_BR_PLACEHOLDER_2			RETURN (eLocation = SmugglerLocation_BombingRun_Placeholder2)
				CASE SMS_BR_PLACEHOLDER_3			RETURN (eLocation = SmugglerLocation_BombingRun_Placeholder3)
			ENDSWITCH
		BREAK
		
		// Escort
		CASE SMV_ESCORT
			SWITCH eSubvariation
				CASE SMS_ESC_PLACEHOLDER_1			RETURN (eLocation = SmugglerLocation_Escort_Placeholder1)
				CASE SMS_ESC_PLACEHOLDER_2			RETURN (eLocation = SmugglerLocation_Escort_Placeholder2)
				CASE SMS_ESC_PLACEHOLDER_3			RETURN (eLocation = SmugglerLocation_Escort_Placeholder3)
			ENDSWITCH
		BREAK
		
		// Beacon Grab
		CASE SMV_BEACON_GRAB
			SWITCH eSubvariation
				CASE SMS_BG_LOCATION_1			RETURN (eLocation = SmugglerLocation_BeaconGrab_Location1)
				CASE SMS_BG_LOCATION_2			RETURN (eLocation = SmugglerLocation_BeaconGrab_Location2)
				CASE SMS_BG_LOCATION_3			RETURN (eLocation = SmugglerLocation_BeaconGrab_Location3)
				CASE SMS_BG_LOCATION_4			RETURN (eLocation = SmugglerLocation_BeaconGrab_Location4)
				CASE SMS_BG_LOCATION_5			RETURN (eLocation = SmugglerLocation_BeaconGrab_Location5)
			ENDSWITCH
		BREAK
		
		CASE SMV_BOMB_ROOF
			SWITCH eSubvariation
				CASE SMS_BRO_ROUTE_1			RETURN (eLocation = SmugglerLocation_BombRoof_Route1)
				CASE SMS_BRO_ROUTE_2			RETURN (eLocation = SmugglerLocation_BombRoof_Route2)
				CASE SMS_BRO_ROUTE_3			RETURN (eLocation = SmugglerLocation_BombRoof_Route3)
			ENDSWITCH
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_SMUGGLER_LOCATION_VALID_FOR_THIS_VARIATION(SMUGGLER_LOCATION eLocation, SMUGGLER_VARIATION eVariation, SMUGGLER_SUBVARIATION eSubvariation)

	IF eLocation = SmugglerLocation_Max
		RETURN FALSE
	ENDIF
	
	IF NOT IS_SMUGGLER_LOCATION_SUITABLE_FOR_VARIATION_AND_SUBVARIATION(eLocation, eVariation, eSubvariation)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE	
ENDFUNC

FUNC SMUGGLER_LOCATION GB_GET_RANDOM_SMUGGLER_LOCATION(SMUGGLER_VARIATION eVariation, SMUGGLER_SUBVARIATION eSubvariation, PLAYER_INDEX playerID, BOOL bIgnoreDistanceChecks = FALSE)

	
	PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] -----------------------------------------------------------------------------------------------------------------")
	
	INT iLoop, iSuitableLocationCount, iNumMissionEntities
	SMUGGLER_LOCATION eLocation[COUNT_OF(SMUGGLER_LOCATION)]
	SMUGGLER_LOCATION eTemp
	BOOL bShouldIgnoreDistanceChecks
	
	IF bIgnoreDistanceChecks 
	#IF IS_DEBUG_BUILD
	OR GET_COMMANDLINE_PARAM_EXISTS("sc_SmugglerFlowPlaySubvariations")
	#ENDIF
		bShouldIgnoreDistanceChecks = TRUE
	ENDIF
	
	iNumMissionEntities = GB_GET_NUM_SMUGGLER_ENTITIES_FOR_VARIATION(eVariation, (GB_GET_NUM_GOONS_IN_PLAYER_GANG(playerID) + 1),playerID)
	
	REPEAT COUNT_OF(SMUGGLER_LOCATION) iLoop
		eTemp = INT_TO_ENUM(SMUGGLER_LOCATION, iLoop)
		
		IF eTemp = SmugglerLocation_Max
			BREAKLOOP
		ENDIF
		
		#IF IS_DEBUG_BUILD
		STRING sTemp = GET_SMUGGLER_LOCATION_NAME(eTemp)
		PRINTLN("[SMUGGLER] GB_GET_RANDOM_SMUGGLER_LOCATION - ", sTemp, " is being checked for suitability.")
		#ENDIF
		
		IF IS_SMUGGLER_LOCATION_VALID_FOR_THIS_VARIATION(eTemp, eVariation, eSubvariation)
			PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] GB_GET_RANDOM_SMUGGLER_LOCATION - ", sTemp, " is valid for this variation.")
			IF NOT IS_SMUGGLER_LOCATION_DISABLED(eTemp)
				IF NOT IS_SMUGGLER_LOCATION_IN_USE(iLoop, playerID)
					IF IS_SMUGGLER_LOCATION_IN_SAFE_RANGE(eTemp, eVariation, playerID, bShouldIgnoreDistanceChecks)
						IF IS_SMUGGLER_LOCATION_SAFE(eTemp, eVariation, eSubvariation, playerID)
						OR bShouldIgnoreDistanceChecks
							IF ARE_NETWORK_ENTITIES_AVAILABLE_FOR_SCRIPT_LAUNCH(GB_GET_SMUGGLER_NUM_PEDS_REQUIRED(eVariation, eSubvariation, iNumMissionEntities), GB_GET_SMUGGLER_NUM_VEH_REQUIRED(eVariation, eSubvariation, iNumMissionEntities, playerID), GB_GET_SMUGGLER_NUM_OBJ_REQUIRED(eVariation, eSubvariation, iNumMissionEntities,playerID),FALSE,TRUE)
								eLocation[iSuitableLocationCount] = eTemp
								iSuitableLocationCount++
								PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] GB_GET_RANDOM_SMUGGLER_LOCATION - PASS - ", sTemp, " is suitable. Current number of suitable locations: ", iSuitableLocationCount)
							ENDIF
						ELSE
							PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] GB_GET_RANDOM_SMUGGLER_LOCATION - FAIL - ", sTemp, " is not safe to spawn entities.")
						ENDIF
					ELSE
						PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] GB_GET_RANDOM_SMUGGLER_LOCATION - FAIL - ", sTemp, " is not in safe range to launch.")
					ENDIF
				ELSE
					PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] GB_GET_RANDOM_SMUGGLER_LOCATION - FAIL - ", sTemp, " is currently in use by another player.")
				ENDIF
			ELSE
				PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] - GB_GET_RANDOM_SMUGGLER_LOCATION - FAIL - ", sTemp, " is currently disabled.")
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF iSuitableLocationCount < 1
		PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] GB_GET_RANDOM_SMUGGLER_LOCATION - No suitable locations. Returning eVEHICLEEXPORTLOCATION_MAX to start search again.")
		RETURN SmugglerLocation_Max
	ENDIF
	
	eTemp = eLocation[GET_RANDOM_INT_IN_RANGE(0, iSuitableLocationCount)]
	PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] GB_GET_RANDOM_SMUGGLER_LOCATION - Number of Possible Locations: ", iSuitableLocationCount, ". Selected location: ", GET_SMUGGLER_LOCATION_NAME(eTemp))
	PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] -----------------------------------------------------------------------------------------------------------------")
	
	RETURN eTemp
ENDFUNC

FUNC FLOAT GET_SMUGGLER_VARIATION_WEIGHTING(SMUGGLER_VARIATION eVariation)

	SWITCH eVariation
		// Resupply
		CASE SMV_STEAL_AIRCRAFT						RETURN g_sMPTunables.fSMUG_STEAL_STEAL_AIRCRAFT_WEIGHTING
		CASE SMV_BLACK_BOX							RETURN g_sMPTunables.fSMUG_STEAL_BLACKBOX_WEIGHTING
		CASE SMV_BOMB_BASE							RETURN g_sMPTunables.fSMUG_STEAL_BOMB_BASE_WEIGHTING
		CASE SMV_THERMAL_SCOPE						RETURN g_sMPTunables.fSMUG_STEAL_THERMAL_SCOPE_WEIGHTING
		CASE SMV_DOGFIGHT							RETURN g_sMPTunables.fSMUG_STEAL_DOGFIGHT_WEIGHTING
		CASE SMV_SPLASH_LANDING						RETURN g_sMPTunables.fSMUG_STEAL_SPLASH_LANDING_WEIGHTING
		CASE SMV_ROOF_ATTACK						RETURN g_sMPTunables.fSMUG_STEAL_ROOF_ATTACK_WEIGHTING
		CASE SMV_CARGO_PLANE						RETURN g_sMPTunables.fSMUG_STEAL_CARGO_PLANE_WEIGHTING
		CASE SMV_BOMB_ROOF							RETURN g_sMPTunables.fSMUG_STEAL_BOMB_ROOF_WEIGHTING
		CASE SMV_BEACON_GRAB						RETURN g_sMPTunables.fSMUG_STEAL_BEACON_GRAB_WEIGHTING
		CASE SMV_BOMBING_RUN						RETURN g_sMPTunables.fSMUG_STEAL_BOMBING_RUN_WEIGHTING
		CASE SMV_INFILTRATION						RETURN g_sMPTunables.fSMUG_STEAL_INFILTRATION_WEIGHTING
		CASE SMV_ESCORT								RETURN g_sMPTunables.fSMUG_STEAL_ESCORT_WEIGHTING
		CASE SMV_STUNT_PILOT						RETURN g_sMPTunables.fSMUG_STEAL_STUNT_PILOT_WEIGHTING
		
		// Sell
		CASE SMV_SELL_HEAVY_LIFTING					RETURN g_sMPTunables.fSMUG_SELL_HEAVY_LIFTING_WEIGHTING
		CASE SMV_SELL_CONTESTED						RETURN g_sMPTunables.fSMUG_SELL_CONTESTED_WEIGHTING
		CASE SMV_SELL_AGILE_DELIVERY				RETURN g_sMPTunables.fSMUG_SELL_AGILE_DELIVERY_WEIGHTING
		CASE SMV_SELL_PRECISION_DELIVERY			RETURN g_sMPTunables.fSMUG_SELL_PRECISION_DELIVERY_WEIGHTING
		CASE SMV_SELL_FLYING_FORTRESS				RETURN g_sMPTunables.fSMUG_SELL_FLYING_FORTRESS_WEIGHTING
		CASE SMV_SELL_FLY_LOW						RETURN g_sMPTunables.fSMUG_SELL_FLY_LOW_WEIGHTING
		CASE SMV_SELL_AIR_DELIVERY					RETURN g_sMPTunables.fSMUG_SELL_AIR_DELIVERY_WEIGHTING
		CASE SMV_SELL_AIR_POLICE					RETURN g_sMPTunables.fSMUG_SELL_AIR_POLICE_WEIGHTING
		CASE SMV_SELL_UNDER_THE_RADAR				RETURN g_sMPTunables.fSMUG_SELL_UNDER_THE_RADAR_WEIGHTING
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

/////////////////////////////////////////
/// SMUGGLER LAUNCH VARIATION SELECTION ///
/////////////////////////////////////////
FUNC BOOL IS_SMUGGLER_SUBVARIATION_DISABLED(SMUGGLER_SUBVARIATION eSubvariation)

	SWITCH eSubvariation
		CASE SMS_SETUP_DELPERROBEACH		RETURN g_sMPTunables.bSMUG_SETUP_DISABLE_LOCATION_0
		CASE SMS_SETUP_LSRIVER				RETURN g_sMPTunables.bSMUG_SETUP_DISABLE_LOCATION_1
		CASE SMS_SETUP_ELBURRO				RETURN g_sMPTunables.bSMUG_SETUP_DISABLE_LOCATION_2
		CASE SMS_SETUP_CHUMASH				RETURN g_sMPTunables.bSMUG_SETUP_DISABLE_LOCATION_3
		CASE SMS_SETUP_TATAVIUM				RETURN g_sMPTunables.bSMUG_SETUP_DISABLE_LOCATION_4
		CASE SMS_SETUP_RONSWINDFARM			RETURN g_sMPTunables.bSMUG_SETUP_DISABLE_LOCATION_5
		CASE SMS_SETUP_GRANDSENORA1			RETURN g_sMPTunables.bSMUG_SETUP_DISABLE_LOCATION_6
		CASE SMS_SETUP_GRANDSENORA2			RETURN g_sMPTunables.bSMUG_SETUP_DISABLE_LOCATION_7
		CASE SMS_SETUP_SANDY				RETURN g_sMPTunables.bSMUG_SETUP_DISABLE_LOCATION_8
		CASE SMS_SETUP_STAB					RETURN g_sMPTunables.bSMUG_SETUP_DISABLE_LOCATION_9
		CASE SMS_SETUP_CALAFIA				RETURN g_sMPTunables.bSMUG_SETUP_DISABLE_LOCATION_10
		CASE SMS_SETUP_MCKENZIE				RETURN g_sMPTunables.bSMUG_SETUP_DISABLE_LOCATION_11
		CASE SMS_SETUP_GRAPESEED			RETURN g_sMPTunables.bSMUG_SETUP_DISABLE_LOCATION_12
		CASE SMS_SETUP_PORCOPIO				RETURN g_sMPTunables.bSMUG_SETUP_DISABLE_LOCATION_13
		CASE SMS_SETUP_PALETO				RETURN g_sMPTunables.bSMUG_SETUP_DISABLE_LOCATION_14
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_SMUGGLER_SUBVARIATION_SUITABLE(SMUGGLER_SUBVARIATION eSubvariation #IF IS_DEBUG_BUILD , SMUGGLER_VARIATION eVariation #ENDIF )
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_SmugglerFlowPlaySubvariations")
		IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_SmugglerFlowPlaySubvariations_alt")
			SWITCH eVariation
				CASE SMV_INFILTRATION		RETURN (eSubvariation = SMS_INF_HUMANE_LABS)
				CASE SMV_SPLASH_LANDING		RETURN (eSubvariation = SMS_SL_PACIFIC_OCEAN_NE)
				CASE SMV_BOMB_BASE			RETURN (eSubvariation = SMS_BB_GRAPESEED_AUTOYARD)
				CASE SMV_BLACK_BOX			RETURN (eSubvariation = SMS_BLB_PALETO_FOREST)
				CASE SMV_THERMAL_SCOPE		RETURN (eSubvariation = SMS_TS_POWER_STATION)
			ENDSWITCH
		ELSE
//			SWITCH eVariation
//				CASE SMV_INFILTRATION		RETURN (eSubvariation = SMS_INF_BOLINGBROKE_PRISON)
//				CASE SMV_ROOF_ATTACK		RETURN (eSubvariation = SMS_RA_ALTA_HOTEL)
//				CASE SMV_THERMAL_SCOPE		RETURN (eSubvariation = SMS_TS_POWER_STATION)
//				CASE SMV_BOMB_BASE			RETURN (eSubvariation = SMS_BB_PLANE_SCRAPYARD)
//				CASE SMV_STEAL_AIRCRAFT		RETURN (eSubvariation = SMS_SA_EPSILON_HQ)
//			ENDSWITCH
		ENDIF
	ENDIF
	#ENDIF

	UNUSED_PARAMETER(eSubvariation)

	RETURN TRUE
ENDFUNC

/// PURPOSE: Returns the number of subvariations a variation has.
/// PARAMS: iVariation - The variation that is being checked
/// RETURNS: The current number of subvariations the passed in variation has.
/// THIS HAS TO BE UPDATED WHEN ADDING NEW SUBVARIATIONS
FUNC INT GET_MAX_SMUGGLER_SUBVARIATION_FOR_VARIATION(SMUGGLER_VARIATION eVariation)

	SWITCH eVariation
		CASE SMV_STEAL_AIRCRAFT				RETURN 3
		CASE SMV_THERMAL_SCOPE				RETURN 3
		CASE SMV_INFILTRATION				RETURN 3
		CASE SMV_STUNT_PILOT				RETURN 3
		CASE SMV_BOMB_BASE					RETURN 3
		CASE SMV_SPLASH_LANDING				RETURN 3
		CASE SMV_BLACK_BOX					RETURN 3
		CASE SMV_DOGFIGHT					RETURN 3
		CASE SMV_CARGO_PLANE				RETURN 3
		CASE SMV_ROOF_ATTACK				RETURN 3
		CASE SMV_BOMBING_RUN				RETURN 3
		CASE SMV_ESCORT						RETURN 3
		CASE SMV_BEACON_GRAB				RETURN 5
		CASE SMV_BOMB_ROOF					RETURN 3
		CASE SMV_SETUP_RON					RETURN 15
	ENDSWITCH
	
	PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] GET_MAX_SMUGGLER_SUBVARIATION_FOR_VARIATION - Variation is invalid. Variation = ", GET_SMUGGLER_VARIATION_NAME_FOR_DEBUG(eVariation))
	RETURN 0
ENDFUNC

/// PURPOSE: Returns the start point of subvariations for a variation in the subvariation enum.
/// PARAMS: iVariation - The variation that is being checked
/// RETURNS: the start point of subvariations for a variation in the subvariation enum.
/// This shouldn't need updated as long as all new subvariations are added after the existing subvariations for that variation
FUNC INT GET_START_SMUGGLER_SUBVARIATION_FOR_VARIATION(SMUGGLER_VARIATION eVariation)

	SWITCH eVariation
		CASE SMV_STEAL_AIRCRAFT			RETURN ENUM_TO_INT(SMS_SA_SANDY_SHORES)
		CASE SMV_THERMAL_SCOPE			RETURN ENUM_TO_INT(SMS_TS_QUARRY)
		CASE SMV_INFILTRATION			RETURN ENUM_TO_INT(SMS_INF_HUMANE_LABS)
		CASE SMV_STUNT_PILOT			RETURN ENUM_TO_INT(SMS_SP_LOCATION_1)
		CASE SMV_BOMB_BASE				RETURN ENUM_TO_INT(SMS_BB_TERMINAL)
		CASE SMV_SPLASH_LANDING			RETURN ENUM_TO_INT(SMS_SL_LAGO_ZANCUDO)
		CASE SMV_BLACK_BOX				RETURN ENUM_TO_INT(SMS_BLB_MOUNT_GORDO)
		CASE SMV_DOGFIGHT				RETURN ENUM_TO_INT(SMS_DF_PLACEHOLDER_1)
		CASE SMV_CARGO_PLANE			RETURN ENUM_TO_INT(SMS_CP_PLACEHOLDER_1)
		CASE SMV_ROOF_ATTACK			RETURN ENUM_TO_INT(SMS_RA_CONSTRUCTION_SITE)
		CASE SMV_BOMBING_RUN			RETURN ENUM_TO_INT(SMS_BR_PLACEHOLDER_1)
		CASE SMV_ESCORT					RETURN ENUM_TO_INT(SMS_ESC_PLACEHOLDER_1)
		CASE SMV_BEACON_GRAB			RETURN ENUM_TO_INT(SMS_BG_LOCATION_1)
		CASE SMV_SETUP_RON				RETURN ENUM_TO_INT(SMS_SETUP_DELPERROBEACH)
		CASE SMV_BOMB_ROOF				RETURN ENUM_TO_INT(SMS_BRO_ROUTE_1)
	ENDSWITCH
	
	PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] GET_START_SMUGGLER_SUBVARIATION_FOR_VARIATION - Variation is invalid. Variation = ", GET_SMUGGLER_VARIATION_NAME_FOR_DEBUG(eVariation))
	RETURN ENUM_TO_INT(SMS_INVALID)
ENDFUNC
    
/// Select a random subvariation for the supplied variation
FUNC INT GB_GET_RANDOM_SMUGGLER_SUBVARIATION(SMUGGLER_VARIATION eVariation)
	INT iSelectedSubvariation
	
	// Get the start and end point for the RNG, so as only to check subvariations for the supplied variation
	INT iStartSubvariation = GET_START_SMUGGLER_SUBVARIATION_FOR_VARIATION(eVariation)
	INT iEndSubvariation = iStartSubvariation + GET_MAX_SMUGGLER_SUBVARIATION_FOR_VARIATION(eVariation)
	
	PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] -----------------------------------------------------------------------------------------------------------------")
	
	PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] GB_GET_RANDOM_SMUGGLER_SUBVARIATION - iStartSubvariation = ", iStartSubvariation, " - iEndSubvariation = ", iEndSubvariation)
	
	IF iStartSubvariation = iEndSubvariation
		PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] GB_GET_RANDOM_SMUGGLER_SUBVARIATION - Variation has no subvariations. Returning -1.")
		RETURN -1
	ENDIF
	
	FLOAT fWeightings[SMS_MAX]
	FLOAT fTotalWeight, fCurrentRange
	
	//Assign weightings to each available subvariation
	INT iLoop
	FOR iLoop = iStartSubvariation TO (iEndSubvariation-1)
		SMUGGLER_SUBVARIATION eTemp = INT_TO_ENUM(SMUGGLER_SUBVARIATION, iLoop)
		IF IS_SMUGGLER_SUBVARIATION_DISABLED(eTemp)
		OR NOT IS_SMUGGLER_SUBVARIATION_SUITABLE(eTemp #IF IS_DEBUG_BUILD , eVariation #ENDIF )
			fWeightings[iLoop] = 0.0
		ELSE
			fWeightings[iLoop] = 1.0
		ENDIF
	ENDFOR
	
	//Calculate total of all weightings
	FOR iLoop = iStartSubvariation TO (iEndSubvariation-1)
		fTotalWeight += fWeightings[iLoop]
		PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] GET_RANDOM_SMUGGLER_SUBVARIATION - ", GET_SMUGGLER_SUBVARIATION_NAME_FOR_DEBUG(INT_TO_ENUM(SMUGGLER_SUBVARIATION, iLoop)), " has a weighting of ", fWeightings[iLoop])
	ENDFOR
	
	//Pick a position between 0 and total of all weightings
	FLOAT fSelection = GET_RANDOM_FLOAT_IN_RANGE(0, fTotalWeight)
	PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] GB_GET_RANDOM_SMUGGLER_SUBVARIATION - fSelection = ", fSelection)
	
	//Find the variation at that position
	FOR iLoop = iStartSubvariation TO (iEndSubvariation-1)
		fCurrentRange += fWeightings[iLoop]
		IF fSelection < fCurrentRange
			iSelectedSubvariation = iLoop
			BREAKLOOP
		ENDIF
	ENDFOR
	
	PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] GB_GET_RANDOM_SMUGGLER_SUBVARIATION - Selected subvariation ", GET_SMUGGLER_SUBVARIATION_NAME_FOR_DEBUG(INT_TO_ENUM(SMUGGLER_SUBVARIATION, iSelectedSubvariation)), " for variation ", GET_SMUGGLER_VARIATION_NAME_FOR_DEBUG(eVariation))
	PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] -----------------------------------------------------------------------------------------------------------------")

	RETURN iSelectedSubvariation

ENDFUNC

#IF IS_DEBUG_BUILD
CONST_INT MAX_SMUGGLER_RESUPPLY_FLOW_PLAY_VARIATIONS		14
CONST_INT MAX_SMUGGLER_RESUPPLY_FLOW_PLAY_VARIATIONS_ALT	8
FUNC SMUGGLER_VARIATION GET_NEXT_SMUGGLER_RESUPPLY_FLOW_PLAY_VARIATION(INT iNextBuyStealFlowPlayVariation)
	
	IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_SmugglerFlowPlayVariations_alt")
		SWITCH iNextBuyStealFlowPlayVariation
			CASE 0		RETURN SMV_BOMB_ROOF
			CASE 1		RETURN SMV_INFILTRATION
			CASE 2		RETURN SMV_DOGFIGHT
			CASE 3		RETURN SMV_SPLASH_LANDING
			CASE 4		RETURN SMV_CARGO_PLANE
			CASE 5		RETURN SMV_STUNT_PILOT
			CASE 6		RETURN SMV_BEACON_GRAB
			CASE 7		RETURN SMV_BOMB_BASE
			CASE 8		RETURN SMV_BLACK_BOX
			CASE 9		RETURN SMV_BOMBING_RUN
			CASE 10		RETURN SMV_THERMAL_SCOPE
			CASE 11		RETURN SMV_ROOF_ATTACK
			CASE 12		RETURN SMV_ESCORT
			CASE 13		RETURN SMV_STEAL_AIRCRAFT
		ENDSWITCH
	ELSE
		SWITCH iNextBuyStealFlowPlayVariation
			CASE 0		RETURN SMV_BOMBING_RUN
			CASE 1		RETURN SMV_CARGO_PLANE
			CASE 2		RETURN SMV_BLACK_BOX
			CASE 3		RETURN SMV_ROOF_ATTACK
			CASE 4		RETURN SMV_ESCORT
			CASE 5		RETURN SMV_BOMB_BASE
			CASE 6		RETURN SMV_THERMAL_SCOPE
			CASE 7		RETURN SMV_BEACON_GRAB
		ENDSWITCH
	ENDIF
	
	RETURN SMV_MAX
ENDFUNC

FUNC BOOL IS_SMUGGLER_RESUPPLY_VARIATION_IN_FLOW_PLAY(SMUGGLER_VARIATION eVariation, INT iNextBuyStealFlowPlayVariation)
	RETURN GET_NEXT_SMUGGLER_RESUPPLY_FLOW_PLAY_VARIATION(iNextBuyStealFlowPlayVariation) = eVariation
ENDFUNC

CONST_INT MAX_SMUGGLER_SELL_FLOW_PLAY_VARIATIONS		9
CONST_INT MAX_SMUGGLER_SELL_FLOW_PLAY_VARIATIONS_ALT	6
FUNC SMUGGLER_VARIATION GET_NEXT_SMUGGLER_SELL_FLOW_PLAY_VARIATION(INT iNextSellFlowPlayVariation)

	IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_SmugglerFlowPlayVariations_alt")
		SWITCH iNextSellFlowPlayVariation
			CASE 0		RETURN SMV_SELL_UNDER_THE_RADAR
			CASE 1		RETURN SMV_SELL_FLYING_FORTRESS
			CASE 2		RETURN SMV_SELL_AGILE_DELIVERY
			CASE 3		RETURN SMV_SELL_AIR_POLICE
			CASE 4		RETURN SMV_SELL_FLY_LOW
			CASE 5		RETURN SMV_SELL_PRECISION_DELIVERY
			CASE 6		RETURN SMV_SELL_HEAVY_LIFTING
			CASE 7		RETURN SMV_SELL_CONTESTED
			CASE 8		RETURN SMV_SELL_AIR_DELIVERY
		ENDSWITCH
	ELSE
		SWITCH iNextSellFlowPlayVariation
			CASE 0		RETURN SMV_SELL_HEAVY_LIFTING
			CASE 1		RETURN SMV_SELL_CONTESTED
			CASE 2		RETURN SMV_SELL_AIR_POLICE
			CASE 3		RETURN SMV_SELL_PRECISION_DELIVERY
			CASE 4		RETURN SMV_SELL_FLY_LOW
			CASE 5		RETURN SMV_SELL_AIR_DELIVERY
		ENDSWITCH
	ENDIF
	
	RETURN SMV_MAX
ENDFUNC

FUNC BOOL IS_SMUGGLER_SELL_VARIATION_IN_FLOW_PLAY(SMUGGLER_VARIATION eVariation, INT iNextSellFlowPlayVariation)
	RETURN GET_NEXT_SMUGGLER_SELL_FLOW_PLAY_VARIATION(iNextSellFlowPlayVariation) = eVariation
ENDFUNC
#ENDIF

PROC GB_COPY_SMUGGLER_HISTORY_FROM_TRANSITION()
	INT iSlot
	
	// Resupply
	REPEAT GET_SMUGGLER_RESUPPLY_HISTORY_LIST_TUNABLE() iSlot
		GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iSmugglerResupplyHistory[iSlot] = g_TransitionSessionNonResetVars.sMagnateGangBossData.iSmugglerResupplyHistory[iSlot]
		PRINTLN("[GB] - GB_COPY_SMUGGLER_HISTORY_FROM_TRANSITION - Resupply History Slot #", iSlot, ": ", GET_SMUGGLER_VARIATION_NAME_FOR_DEBUG(INT_TO_ENUM(SMUGGLER_VARIATION,GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iSmugglerResupplyHistory[iSlot])))
	ENDREPEAT
	
	// Sell
	REPEAT GET_SMUGGLER_SELL_HISTORY_LIST_TUNABLE() iSlot
		GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iSmugglerSellHistory[iSlot] = g_TransitionSessionNonResetVars.sMagnateGangBossData.iSmugglerSellHistory[iSlot]
		PRINTLN("[GB] - GB_COPY_SMUGGLER_HISTORY_FROM_TRANSITION - Sell History Slot #", iSlot, ": ", GET_SMUGGLER_VARIATION_NAME_FOR_DEBUG(INT_TO_ENUM(SMUGGLER_VARIATION,GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iSmugglerSellHistory[iSlot])))
	ENDREPEAT
	
	PRINTLN("[GB] - GB_COPY_SMUGGLER_HISTORY_FROM_TRANSITION ")
ENDPROC

FUNC BOOL GB_IS_SMUGGLER_VARIATION_IN_PLAYERS_HISTORY_LIST(SMUGGLER_VARIATION eVariation, PLAYER_INDEX playerId, INT iFmmcType)
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_SmugglerFlowPlayVariations")
		RETURN FALSE
	ENDIF
	#ENDIF
	
	INT iPlayer = NATIVE_TO_INT(playerId)
	IF iPlayer = -1
		RETURN FALSE
	ENDIF
	
	INT iSlot
	
	// RESUPPLY
	IF iFmmcType = FMMC_TYPE_SMUGGLER_BUY
		REPEAT GET_SMUGGLER_RESUPPLY_HISTORY_LIST_TUNABLE() iSlot
			IF GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.iSmugglerResupplyHistory[iSlot] = ENUM_TO_INT(eVariation)
				PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] GB_IS_SMUGGLER_VARIATION_IN_PLAYERS_HISTORY_LIST - TRUE - ", GET_SMUGGLER_VARIATION_NAME_FOR_DEBUG(eVariation), " is in slot ", iSlot, " in the history list.")
				RETURN TRUE
			ENDIF
		ENDREPEAT
	// SELL
	ELIF iFmmcType = FMMC_TYPE_SMUGGLER_SELL
		REPEAT GET_SMUGGLER_SELL_HISTORY_LIST_TUNABLE() iSlot
			IF GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.iSmugglerSellHistory[iSlot] = ENUM_TO_INT(eVariation)
				PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] GB_IS_SMUGGLER_VARIATION_IN_PLAYERS_HISTORY_LIST - TRUE - ", GET_SMUGGLER_VARIATION_NAME_FOR_DEBUG(eVariation), " is in slot ", iSlot, " in the history list.")
				RETURN TRUE
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_TIME_OF_DAY_SUITABLE_FOR_SMUGGLER_VARIATION(SMUGGLER_VARIATION eVariation)
	TIMEOFDAY tofTime = GET_CURRENT_TIMEOFDAY() 
	INT iHour = GET_TIMEOFDAY_HOUR(tofTime)
	
	SWITCH eVariation
		CASE SMV_INFILTRATION
			RETURN (iHour >= 19 OR iHour <= 2)
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_SMUGGLER_VARIATION_DISABLED(SMUGGLER_VARIATION eVariation, PLAYER_INDEX playerID #IF IS_DEBUG_BUILD , INT iNextResupplyFlowPlayVariation, INT iNextSellFlowPlayVariation, INT iFMMC #ENDIF )
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_SmugglerFlowPlayVariations")
		IF iFMMC = FMMC_TYPE_SMUGGLER_SELL
			IF IS_SMUGGLER_VARIATION_A_SELL_MISSION(eVariation)
				IF NOT IS_SMUGGLER_SELL_VARIATION_IN_FLOW_PLAY(eVariation, iNextSellFlowPlayVariation)
					RETURN TRUE
				ELSE
					PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] [FPLAY_SELL] IS_SMUGGLER_VARIATION_DISABLED - ", GET_SMUGGLER_VARIATION_NAME_FOR_DEBUG(eVariation), " is the next Sell in the flow play.")
					RETURN FALSE
				ENDIF
			ENDIF
		ELIF iFMMC = FMMC_TYPE_SMUGGLER_BUY
			IF NOT IS_SMUGGLER_VARIATION_A_SELL_MISSION(eVariation)
				IF NOT IS_SMUGGLER_RESUPPLY_VARIATION_IN_FLOW_PLAY(eVariation, iNextResupplyFlowPlayVariation)
					RETURN TRUE
				ELSE
					PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] [FPLAY_RESUP] IS_SMUGGLER_VARIATION_DISABLED - ", GET_SMUGGLER_VARIATION_NAME_FOR_DEBUG(eVariation), " is next Resupply in the flow play.")
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	#ENDIF
	
	IF NOT IS_TIME_OF_DAY_SUITABLE_FOR_SMUGGLER_VARIATION(eVariation)
		PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] IS_SMUGGLER_VARIATION_DISABLED - Time of day is not suitable for ", GET_SMUGGLER_VARIATION_NAME_FOR_DEBUG(eVariation), " to launch. Disabling variation.")
		RETURN TRUE
	ENDIF
	
	IF NOT FREEMODE_CONTENT_CAN_REGISTER_DELIVERABLES(GB_GET_NUM_SMUGGLER_ENTITIES_FOR_VARIATION(eVariation, GB_GET_NUM_GOONS_IN_PLAYER_GANG(playerID)+1, playerID))
		RETURN TRUE
	ENDIF
	
	// Launch Restrictions (Gang Size, Product %)
	SWITCH(eVariation)

		CASE SMV_SELL_FLYING_FORTRESS
		CASE SMV_BOMB_ROOF
		CASE SMV_THERMAL_SCOPE
		CASE SMV_SELL_UNDER_THE_RADAR
			IF GB_GET_NUM_GOONS_IN_PLAYER_GANG(playerID) = 0
				PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] IS_SMUGGLER_VARIATION_DISABLED - ", GET_SMUGGLER_VARIATION_NAME_FOR_DEBUG(eVariation), " requires at least 2 players in gang. Disabling variation.")
				RETURN TRUE
			ENDIF
		BREAK
		
	ENDSWITCH
	
	SMUGGLER_MISSION_DATA data = GB_GET_PLAYER_SMUGGLER_MISSION_DATA()
	SMUGGLER_SELL_SIZES eSellSize = GET_SMUGGLER_SELL_SIZE_FROM_COUNT(GET_COUNT_OF_CONTRABAND_TYPE_IN_HANGAR(data.eType))
	
	SWITCH(eVariation)

		CASE SMV_SELL_FLYING_FORTRESS
			IF eSellSize != eSMUGGLER_SELL_SIZE_MEDIUM
			AND eSellSize != eSMUGGLER_SELL_SIZE_LARGE
				PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] IS_SMUGGLER_VARIATION_DISABLED - ", GET_SMUGGLER_VARIATION_NAME_FOR_DEBUG(eVariation), " requires medium or large shipment.")
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE SMV_SELL_UNDER_THE_RADAR
			IF eSellSize != eSMUGGLER_SELL_SIZE_MEDIUM
			AND eSellSize != eSMUGGLER_SELL_SIZE_SMALL
				PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] IS_SMUGGLER_VARIATION_DISABLED - ", GET_SMUGGLER_VARIATION_NAME_FOR_DEBUG(eVariation), " requires medium or small shipment.")
				RETURN TRUE
			ENDIF
		BREAK
		
	ENDSWITCH
	
//	Hook up disable mode tunables here
//	SWITCH(eVariation)
//		CASE SMV_ALTRUISTS		
//		CASE SMV_DESTROY_TRUCKS	
//		CASE SMV_FLASHLIGHT		
//		CASE SMV_RIVAL_OPERATION
//		CASE SMV_STEAL_APC		
//		CASE SMV_STEAL_MINIGUNS	
//		CASE SMV_STEAL_RAILGUNS					   
//		CASE SMV_STEAL_RHINO					   
//		CASE SMV_STEAL_TECHNICAL				   
//		CASE SMV_YACHT_SEARCH					   			   
//	ENDSWITCH
	
	RETURN FALSE
	UNUSED_PARAMETER(eVariation)
	UNUSED_PARAMETER(playerID)
ENDFUNC

PROC GB_GET_RANDOM_SMUGGLER_VARIATION(INT& iVariation, INT& iSubvariation, PLAYER_INDEX playerId, INT iFMMCType)

	INT iTemp
	
	PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] -----------------------------------------------------------------------------------------------------------------")

	IF iVariation > -1
		PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] GB_GET_RANDOM_SMUGGLER_VARIATION - Forced variation: ", GET_SMUGGLER_VARIATION_NAME_FOR_DEBUG(INT_TO_ENUM(SMUGGLER_VARIATION, iVariation)))
		IF DOES_SMUGGLER_VARIATION_HAVE_SUBVARIATIONS(INT_TO_ENUM(SMUGGLER_VARIATION, iVariation))
			IF iSubvariation > -1
				PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] GB_GET_RANDOM_SMUGGLER_VARIATION - Forced subvariation: ", GET_SMUGGLER_SUBVARIATION_NAME_FOR_DEBUG(INT_TO_ENUM(SMUGGLER_SUBVARIATION, iSubvariation)))
			ELSE
				iSubvariation = GB_GET_RANDOM_SMUGGLER_SUBVARIATION(INT_TO_ENUM(SMUGGLER_VARIATION, iVariation))
				PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] GB_GET_RANDOM_SMUGGLER_VARIATION - Forced variation without subvariation. Subvariation selected: ", GET_SMUGGLER_SUBVARIATION_NAME_FOR_DEBUG(INT_TO_ENUM(SMUGGLER_SUBVARIATION, iSubvariation)))
			ENDIF
			
		ENDIF
		EXIT
	ENDIF

	FLOAT fWeightings[SMV_MAX]
	FLOAT fTotalWeight, fCurrentRange
	
	//Assign weightings to each variation
	INT iVar
	REPEAT ENUM_TO_INT(SMV_MAX) iVar
		SMUGGLER_VARIATION eTemp = INT_TO_ENUM(SMUGGLER_VARIATION,iVar)
		IF IS_SMUGGLER_VARIATION_DISABLED(eTemp, playerID #IF IS_DEBUG_BUILD , GlobalplayerBD_FM_3[NATIVE_TO_INT(playerID)].sMagnateGangBossData.iNextBuyStealFlowPlayVariation, GlobalplayerBD_FM_3[NATIVE_TO_INT(playerID)].sMagnateGangBossData.iNextSellFlowPlayVariation, iFMMCType #ENDIF )
		OR GB_IS_SMUGGLER_VARIATION_IN_PLAYERS_HISTORY_LIST(eTemp, playerID, iFMMCType)
		OR eTemp = SMV_SETUP_RON
			fWeightings[iVar] = 0.0
		ELSE
			IF NOT IS_SMUGGLER_VARIATION_A_SELL_MISSION(eTemp)
				IF iFMMCType = FMMC_TYPE_SMUGGLER_BUY
					fWeightings[iVar] = GET_SMUGGLER_VARIATION_WEIGHTING(eTemp)
				ELSE
					fWeightings[iVar] = 0.0
				ENDIF
			ELSE
				IF iFMMCType = FMMC_TYPE_SMUGGLER_SELL
					fWeightings[iVar] = GET_SMUGGLER_VARIATION_WEIGHTING(eTemp)
				ELSE
					fWeightings[iVar] = 0.0
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	//Calculate total of all weightings
	REPEAT SMV_MAX iTemp
		fTotalWeight += fWeightings[iTemp]
		PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] GB_GET_RANDOM_SMUGGLER_VARIATION - ", GET_SMUGGLER_VARIATION_NAME_FOR_DEBUG(INT_TO_ENUM(SMUGGLER_VARIATION, iTemp)), " has a weighting of ", fWeightings[iTemp])
	ENDREPEAT
	
	//Pick a position between 0 and total of all weightings
	FLOAT fSelection = GET_RANDOM_FLOAT_IN_RANGE(0,fTotalWeight)
	PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] GB_GET_RANDOM_SMUGGLER_VARIATION - fSelection = ", fSelection)
	
	//Find the variation at that position
	REPEAT SMV_MAX iTemp
		fCurrentRange += fWeightings[iTemp]
		PRINTLN("[MAGNATE_GANG_BOSS] [SMUGGLER] GB_GET_RANDOM_SMUGGLER_VARIATION - fCurrentRange = ", fCurrentRange)
		IF fSelection < fCurrentRange
			IF NOT DOES_SMUGGLER_VARIATION_HAVE_SUBVARIATIONS(INT_TO_ENUM(SMUGGLER_VARIATION, iTemp))
				iVariation = iTemp
				EXIT
			ELSE
				iSubvariation = GB_GET_RANDOM_SMUGGLER_SUBVARIATION(INT_TO_ENUM(SMUGGLER_VARIATION, iTemp))
				iVariation = iTemp
				EXIT
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

/////////////////////////////
/// SMUGGLER LAUNCH DATA  ///
/////////////////////////////

FUNC BOOL GB_GET_RANDOM_SMUGGLER_VARIATION_DATA(INT& iVariation, INT& iSubvariation, INT& iLocation, PLAYER_INDEX playerID, INT iFMMCType, FREEMODE_DELIVERY_ACTIVE_DROPOFF_PROPERTIES &sDropOffProperties)
	INT iTempVar = iVariation
	INT iTempSub = iSubvariation
	INT iTempLoc = iLocation
	GB_GET_RANDOM_SMUGGLER_VARIATION(iTempVar, iTempSub, playerID, iFMMCType)
	
	IF iTempVar != (-1)
	
		#IF IS_DEBUG_BUILD
		IF iTempLoc > -1
			PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] GB_GET_RANDOM_SMUGGLER_VARIATION_DATA - Used debug location: ", GET_SMUGGLER_LOCATION_NAME(INT_TO_ENUM(SMUGGLER_LOCATION, iTempLoc)))
		ELSE
		#ENDIF
	
		IF NOT IS_SMUGGLER_VARIATION_A_SELL_MISSION(INT_TO_ENUM(SMUGGLER_VARIATION,iTempVar))
			BOOL bIgnoreDistanceChecks
			IF iSubvariation != -1
				bIgnoreDistanceChecks = TRUE
			ENDIF
			
			iTempLoc = ENUM_TO_INT(GB_GET_RANDOM_SMUGGLER_LOCATION(INT_TO_ENUM(SMUGGLER_VARIATION, iTempVar), INT_TO_ENUM(SMUGGLER_SUBVARIATION, iTempSub),playerID, bIgnoreDistanceChecks))
			
		ENDIF
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		IF iTempLoc != ENUM_TO_INT(SmugglerLocation_Max)
			iVariation = iTempVar
			iSubvariation = iTempSub
			iLocation = iTempLoc
			PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] -----------------------------------------------------------------------------------------------------------------")
			PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] GB_GET_RANDOM_SMUGGLER_VARIATION_DATA - Selected variation: ", GET_SMUGGLER_VARIATION_NAME_FOR_DEBUG(INT_TO_ENUM(SMUGGLER_VARIATION, iVariation)))
			PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] GB_GET_RANDOM_SMUGGLER_VARIATION_DATA - Selected subvariation: ", GET_SMUGGLER_SUBVARIATION_NAME_FOR_DEBUG(INT_TO_ENUM(SMUGGLER_SUBVARIATION, iSubvariation)))
			PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] GB_GET_RANDOM_SMUGGLER_VARIATION_DATA - Selected location: ", GET_SMUGGLER_LOCATION_NAME(INT_TO_ENUM(SMUGGLER_LOCATION, iLocation)))
			PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] -----------------------------------------------------------------------------------------------------------------")
			
			//Find dropoff
			IF GET_DROP_OFF_FOR_SMUGGLER_VARIATION(playerID,GET_PLAYERS_OWNED_HANGAR(playerID),INT_TO_ENUM(SMUGGLER_VARIATION, iTempVar),sDropOffProperties)
				RESERVE_DROPOFFS_ON_SERVER(sDropOffProperties.eDropoffList, playerID)
				PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] GB_GET_RANDOM_SMUGGLER_VARIATION_DATA - Found dropoff starting at ",sDropOffProperties.eDropoffList.eDropoffID[0])
			ELSE
				PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] GB_GET_RANDOM_SMUGGLER_VARIATION_DATA - Couldn't find a valid dropoff.")
				RETURN FALSE
			ENDIF
			
			RETURN TRUE
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] GB_GET_RANDOM_SMUGGLER_VARIATION_DATA - Couldn't find a valid location.")
		#ENDIF
		ENDIF
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [SMUGGLER] GB_GET_RANDOM_SMUGGLER_VARIATION_DATA - Couldn't find a valid variation.")
	#ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

