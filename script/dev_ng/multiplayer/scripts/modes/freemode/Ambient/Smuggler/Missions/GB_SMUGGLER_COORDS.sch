USING "globals.sch"
USING "net_prints.sch"
USING "net_include.sch"
USING "commands_misc.sch"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Written by:  William Kennedy, Ryan Elliott & Martin McMillan.														//
// Date: 		25/05/2017																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////
/// SMUGGLER ENTITY SPAWN COORDS ///
////////////////////////////////////

FUNC MODEL_NAMES GET_SMUGGLER_MISSION_CRATE_MODEL(eSMUGGLED_GOODS_TYPE eGoodsType)
	SWITCH eGoodsType
		CASE eSGT_ANIMAL_MATERIALS					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_Prop_Smug_Crate_S_Bones"))
		CASE eSGT_ART_AND_ANTIQUES					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_Prop_Smug_Crate_S_Antiques"))
		CASE eSGT_CHEMICALS							RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_Prop_Smug_Crate_S_Hazard"))
		CASE eSGT_COUNTERFEIT_GOODS					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_Prop_Smug_Crate_S_Fake"))
		CASE eSGT_JEWELRY_AND_GEMSTONES				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_Prop_Smug_Crate_S_Jewellery"))
		CASE eSGT_MEDICAL_SUPPLIES					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_Prop_Smug_Crate_S_Medical"))
		CASE eSGT_NARCOTICS							RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_Prop_Smug_Crate_S_Narc"))
		CASE eSGT_TOBACCO_AND_ALCOHOL				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_Prop_Smug_Crate_S_Tobacco"))
	ENDSWITCH
	RETURN ex_prop_adv_case_sm
ENDFUNC

FUNC MODEL_NAMES GET_SMUGGLER_ENTITY_MODEL(SMUGGLER_VARIATION eVariation, SMUGGLER_SUBVARIATION eSubvariation, eSMUGGLED_GOODS_TYPE eGoodsType)
	
	UNUSED_PARAMETER(eSubvariation)
	
	IF NOT DOES_SMUGGLER_VARIATION_USE_VEHICLES_AS_MISSION_ENTITIES(eVariation)
		RETURN GET_SMUGGLER_MISSION_CRATE_MODEL(eGoodsType)
	ELSE
		SWITCH eVariation
			
			// Sell
			CASE SMV_SELL_HEAVY_LIFTING				RETURN SKYLIFT
			CASE SMV_SELL_FLYING_FORTRESS			RETURN BOMBUSHKA
			CASE SMV_SELL_AGILE_DELIVERY			RETURN HAVOK
			CASE SMV_SELL_CONTESTED					RETURN HUNTER
			CASE SMV_SELL_FLY_LOW					RETURN ALPHAZ1
			CASE SMV_SELL_UNDER_THE_RADAR			RETURN MICROLIGHT
			CASE SMV_SELL_PRECISION_DELIVERY		RETURN CARGOBOB2
			CASE SMV_SETUP_RON						RETURN MOGUL
			CASE SMV_SELL_AIR_DELIVERY				RETURN SEABREEZE
			CASE SMV_SELL_AIR_POLICE				RETURN MOGUL
		ENDSWITCH
	ENDIF
	
	SCRIPT_ASSERT("GET_SMUGGLER_ENTITY_MODEL returning DUMMY_MODEL_FOR_SCRIPT - invalid data passed or mission not setup correctly")
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC VECTOR GET_SMUGGLER_LOCATION_ENTITY_SPAWN_COORDS(SMUGGLER_LOCATION eLocation, INT iMissionEntity, BOOL bForLaunchingChecks = FALSE)
	
	SWITCH eLocation
		
		// STEAL MISSION LOCATIONS
		// Steal Aircraft
		CASE SmugglerLocation_StealAircraft_SandyShores
			SWITCH iMissionEntity
				CASE 0			RETURN <<1733.5162, 3320.3188, 40.2235>>
				CASE 1			RETURN <<1733.5162, 3320.3188, 40.2235>>
				CASE 2			RETURN <<1725.2155, 3312.6453, 40.2235>>
				CASE 3			RETURN <<1725.2155, 3312.6453, 40.2235>>
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_StealAircraft_McKenzieAirfield
			SWITCH iMissionEntity
				CASE 0			RETURN <<2133.5850, 4778.6807, 39.9703>>
				CASE 1			RETURN <<2133.5850, 4778.6807, 39.9703>>
				CASE 2			RETURN <<2131.6846, 4793.2813, 40.1269>>
				CASE 3			RETURN <<2131.6846, 4793.2813, 40.1269>>
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_StealAircraft_EpsilonHQ
			SWITCH iMissionEntity
				CASE 0			RETURN <<-680.2657, 74.6550, 68.6856>>
				CASE 1			RETURN <<-680.2657, 74.6550, 68.6856>>
				CASE 2			RETURN <<-676.2980, 101.5110, 54.8550>>
				CASE 3			RETURN <<-676.2980, 101.5110, 54.8550>>
			ENDSWITCH
		BREAK
		
		// Bomb Base
		CASE SmugglerLocation_BombBase_Terminal
			SWITCH iMissionEntity
				CASE 0			RETURN <<744.2346, -3176.6636, 4.9005>>
				CASE 1			RETURN <<744.2346, -3176.6636, 4.9005>>
				CASE 2			RETURN <<735.2852, -3166.1895, 4.9005>>
				CASE 3			RETURN <<735.2852, -3166.1895, 4.9005>>
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_BombBase_GrapeseedAutoyard
			SWITCH iMissionEntity
				CASE 0			RETURN <<2002.1001, 4712.4922, 40.6149>>
				CASE 1			RETURN <<2002.1001, 4712.4922, 40.6149>>
				CASE 2			RETURN <<1988.9009, 4705.6772, 40.5671>>
				CASE 3			RETURN <<1988.9009, 4705.6772, 40.5671>>
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_BombBase_PlaneScrapyard
			SWITCH iMissionEntity
				CASE 0			RETURN <<2408.4583, 3324.2749, 46.6277>>
				CASE 1			RETURN <<2408.4583, 3324.2749, 46.6277>>
				CASE 2			RETURN <<2411.6665, 3336.4106, 45.9022>>
				CASE 3			RETURN <<2411.6665, 3336.4106, 45.9022>>
			ENDSWITCH
		BREAK
	
		// Splash Landing
		CASE SmugglerLocation_SplashLanding_LagoZancudo
			SWITCH iMissionEntity
				CASE 0			RETURN <<-3181.7529, 3012.0950, -41.0300>>
				CASE 1			RETURN <<-3176.1108, 3038.2063, -36.0347>>
				CASE 2			RETURN <<-3185.6123, 3056.5857, -41.0703>>
				CASE 3			RETURN <<-3199.9934, 3034.8774, -37.7143>>
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_SplashLanding_PacificOceanE
			SWITCH iMissionEntity
				CASE 0			RETURN <<3901.1228, 3055.8547, -26.3330>>
				CASE 1			RETURN <<3891.9363, 3045.5801, -22.1210>>
				CASE 2			RETURN <<3933.6370, 3015.3191, -40.2210>>
				CASE 3			RETURN <<3916.3210, 3052.3850, -30.7540>>
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_SplashLanding_PacificOceanNE
			SWITCH iMissionEntity
				CASE 0			RETURN <<3401.1912, 6334.4941, -59.0909>>
				CASE 1			RETURN <<3398.5493, 6316.6348, -52.3259>>
				CASE 2			RETURN <<3424.2070, 6320.5630, -64.4400>>
				CASE 3			RETURN <<3390.3345, 6333.8452, -56.7310>>
			ENDSWITCH
		BREAK
		
		// Thermal Scope
		CASE SmugglerLocation_ThermalScope_Quarry			
			SWITCH iMissionEntity
				CASE 0			RETURN <<1091.1672, 2307.5627, 44.5130>>
				CASE 1			RETURN <<1091.1672, 2307.5627, 44.5130>>
				CASE 2			RETURN <<1081.6643, 2300.5225, 44.5138>>
				CASE 3			RETURN <<1081.6643, 2300.5225, 44.5138>>
			ENDSWITCH
		BREAK
		
		CASE SmugglerLocation_ThermalScope_PowerStation		
			SWITCH iMissionEntity
				CASE 0			RETURN <<2736.5688, 1416.5463, 23.4710>>
				CASE 1			RETURN <<2736.5688, 1416.5463, 23.4710>>
				CASE 2			RETURN <<2723.4700, 1420.8550, 23.4810>>
				CASE 3			RETURN <<2723.4700, 1420.8550, 23.4810>>
			ENDSWITCH
		BREAK
		
		CASE SmugglerLocation_ThermalScope_PlayboyMansion	
			SWITCH iMissionEntity
				CASE 0			RETURN <<-1567.7841, 100.1070, 56.9240>>
				CASE 1			RETURN <<-1567.7841, 100.1070, 56.9240>>
				CASE 2			RETURN <<-1473.6180, 151.2950, 53.8570>>
				CASE 3			RETURN <<-1473.6180, 151.2950, 53.8570>>
			ENDSWITCH
		BREAK
		
		// Black Box
		CASE SmugglerLocation_Blackbox_MountGordo			
			SWITCH iMissionEntity
				CASE 0			RETURN <<2578.2488, 6182.8936, 162.8385>>
				CASE 1			RETURN <<2541.9963, 6146.4771, 157.9084>>
				CASE 2			RETURN <<2604.1382, 6159.5049, 170.5552>>
				CASE 3			RETURN <<2514.8909, 6139.3218, 158.3921>>
				CASE 4			RETURN <<2575.6704, 6204.9824, 155.3048>>
				CASE 5			RETURN <<2584.5884, 6142.2822, 160.7758>>
				CASE 6			RETURN <<2492.0808, 6106.2852, 173.9649>>
				CASE 7			RETURN <<2576.0100, 6133.0854, 169.4876>>
			ENDSWITCH 
		BREAK
		CASE SmugglerLocation_Blackbox_PaletoForest
			SWITCH iMissionEntity
				CASE 0			RETURN <<-401.8941, 5843.5874, 45.4500>>
				CASE 1			RETURN <<-456.1789, 5750.7310, 66.1382>>
				CASE 2			RETURN <<-469.9607, 5810.1777, 44.3801>>
				CASE 3			RETURN <<-437.2340, 5861.1865, 36.8436>>
				CASE 4			RETURN <<-448.1917, 5807.2085, 49.9880>>
				CASE 5			RETURN <<-422.1481, 5770.3457, 57.1516>>
				CASE 6			RETURN <<-460.2819, 5780.0073, 52.7017>>
				CASE 7			RETURN <<-428.5784, 5843.0962, 44.3902>>
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_Blackbox_Island
			SWITCH iMissionEntity
				CASE 0			RETURN <<2815.3391, -1462.8577, 8.9045>>
				CASE 1			RETURN <<2837.5449, -1557.0084, -1.7513>>
				CASE 2			RETURN <<2765.3931, -1587.7749, 1.2766>>
				CASE 3			RETURN <<2754.6213, -1465.6897, 0.8046>>
				CASE 4			RETURN <<2842.0610, -1513.0660, 1.2421>>
				CASE 5			RETURN <<2762.3472, -1548.3162, 0.3614>>
				CASE 6			RETURN <<2747.5979, -1593.2408, 3.5502>>
				CASE 7			RETURN <<2809.7202, -1491.8540, 9.8297>>
			ENDSWITCH
		BREAK
		
		// Infiltration
		CASE SmugglerLocation_Infiltration_HumaneLabs
			SWITCH iMissionEntity
				CASE 0			RETURN <<3576.5911, 3656.3601, 32.8990>>
				CASE 1			RETURN <<3576.5911, 3656.3601, 32.8990>>
				CASE 2			RETURN <<3573.4509, 3667.0017, 32.8886>>
				CASE 3			RETURN <<3573.4509, 3667.0017, 32.8886>>
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_Infiltration_BolingbrokePrison
			SWITCH iMissionEntity
				CASE 0			RETURN <<1673.2805, 2607.9761, 44.5649>>
				CASE 1			RETURN <<1673.2805, 2607.9761, 44.5649>>
				CASE 2			RETURN <<1673.4609, 2601.0017, 44.5649>>
				CASE 3			RETURN <<1673.4609, 2601.0017, 44.5649>>
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_Infiltration_Zancudo
			SWITCH iMissionEntity
				CASE 0			RETURN <<-1951.6100, 3335.5020, 31.9600>>
				CASE 1			RETURN <<-1951.6100, 3335.5020, 31.9600>>
				CASE 2			RETURN <<-1955.7740, 3324.8369, 31.9600>>
				CASE 3			RETURN <<-1955.7740, 3324.8369, 31.9600>>
			ENDSWITCH
		BREAK
		
		// Bomb Roof
		CASE SmugglerLocation_BombRoof_Route1
			SWITCH iMissionEntity
				CASE 0			RETURN <<-1937.0310, 479.9040, 101.6910>>
				CASE 1			RETURN <<-1937.0310, 479.9040, 101.6910>>
				CASE 2			RETURN <<-1952.5520, 477.1580, 101.6910>>
				CASE 3			RETURN <<-1952.5520, 477.1580, 101.6910>>
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_BombRoof_Route2
			SWITCH iMissionEntity
				CASE 0			RETURN <<-786.1140, 866.8280, 202.1720>>
				CASE 1			RETURN <<-786.1140, 866.8280, 202.1720>>
				CASE 2			RETURN <<-787.9610, 866.5110, 202.1710>>
				CASE 3			RETURN <<-787.9610, 866.5110, 202.1710>>
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_BombRoof_Route3
			SWITCH iMissionEntity
				CASE 0			RETURN <<198.8830, 108.9680, 105.0700>>
				CASE 1			RETURN <<198.8830, 108.9680, 105.0700>>
				CASE 2			RETURN <<210.3820, 107.1570, 105.0700>>
				CASE 3			RETURN <<210.3820, 107.1570, 105.0700>>
			ENDSWITCH
		BREAK
		
		// Dogfight
		CASE SmugglerLocation_Dogfight_Placeholder1			
		CASE SmugglerLocation_Dogfight_Placeholder2
		CASE SmugglerLocation_Dogfight_Placeholder3
			RETURN <<0.0, 0.0, 0.0>>
			
		// Bombing Run
		CASE SmugglerLocation_BombingRun_Placeholder1	
		CASE SmugglerLocation_BombingRun_Placeholder2
		CASE SmugglerLocation_BombingRun_Placeholder3
			RETURN <<0.0, 0.0, 0.0>>
		
		// Roof Attack
		CASE SmugglerLocation_RoofAttack_ConstructionSite
		CASE SmugglerLocation_RoofAttack_AltaHotel
		CASE SmugglerLocation_RoofAttack_PacificStandard
			RETURN <<0.0, 0.0, 0.0>>
			
		// Stunt Pilot
		CASE SmugglerLocation_StuntPilot_Location1
		CASE SmugglerLocation_StuntPilot_Location2
		CASE SmugglerLocation_StuntPilot_Location3
		CASE SmugglerLocation_StuntPilot_Location4
		CASE SmugglerLocation_StuntPilot_Location5
		CASE SmugglerLocation_StuntPilot_Location6
		CASE SmugglerLocation_StuntPilot_Location7
		CASE SmugglerLocation_StuntPilot_Location8
		CASE SmugglerLocation_StuntPilot_Location9
		CASE SmugglerLocation_StuntPilot_Location10
			RETURN <<0.0, 0.0, 0.0>>
			
		// Beacon Grab
		CASE SmugglerLocation_BeaconGrab_Location1
		CASE SmugglerLocation_BeaconGrab_Location2
		CASE SmugglerLocation_BeaconGrab_Location3
		CASE SmugglerLocation_BeaconGrab_Location4
		CASE SmugglerLocation_BeaconGrab_Location5
			RETURN <<0.0, 0.0, 0.0>>
		
		// Escort
		CASE SmugglerLocation_Escort_Placeholder1
		CASE SmugglerLocation_Escort_Placeholder2
		CASE SmugglerLocation_Escort_Placeholder3
			RETURN <<0.0, 0.0, 0.0>>
			
		CASE SmugglerLocation_Setup_DelPerroBeach
			SWITCH iMissionEntity
				CASE 0  RETURN <<-1788.0525, -842.3671, 6.8088>>
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_Setup_LSRiver
			SWITCH iMissionEntity
				CASE 0 RETURN <<607.5912, -545.4429, 14.2422>>
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_Setup_ElBurro
			SWITCH iMissionEntity
				CASE 0 RETURN <<1750.3967, -1551.8258, 111.6305>>
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_Setup_Chumash
			SWITCH iMissionEntity
				CASE 0 RETURN <<-3283.8772, 998.4238, 2.6219>>
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_Setup_Tutuavium
			SWITCH iMissionEntity
				CASE 0 RETURN <<2517.4741, 1030.2629, 76.8540>>
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_Setup_RonsWindFarm
			SWITCH iMissionEntity
				CASE 0 RETURN <<2437.9949, 1461.5397, 36.8019>>
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_Setup_SenoraDesert1
			SWITCH iMissionEntity
				 CASE 0 RETURN <<1369.4000, 1412.8090, 102.5004>>
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_Setup_SenoraDesert2
			SWITCH iMissionEntity
				CASE 0 RETURN <<321.9248, 2423.0657, 47.4936>>
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_Setup_SandyShores
			SWITCH iMissionEntity
				CASE 0 RETURN <<1182.1031, 3078.3088, 39.9078>>
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_Setup_StabCity
			SWITCH iMissionEntity
				CASE 0 RETURN <<107.2016, 3651.5603, 38.5678>>
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_Setup_NCalafiaWay
			SWITCH iMissionEntity
				CASE 0 RETURN <<1005.6583, 4346.6050, 42.8017>>
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_Setup_McKenzie
			SWITCH iMissionEntity
				CASE 0 RETURN <<2017.9810, 4789.2378, 40.8519>>
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_Setup_Grapeseed
			SWITCH iMissionEntity
				CASE 0 RETURN <<2650.9424, 4225.5522, 42.4054>>
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_Setup_ProcopioBeach
			SWITCH iMissionEntity
				CASE 0 RETURN <<758.5976, 6591.7734, 1.3513>>
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_Setup_Paleto
			SWITCH iMissionEntity
				CASE 0 RETURN <<-341.3064, 6469.6650, 1.3353>>
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	IF NOT bForLaunchingChecks
		SCRIPT_ASSERT("GET_SMUGGLER_LOCATION_ENTITY_SPAWN_COORDS - Invalid location passed! Further details in logs.")
		PRINTLN("[SMUGGLER] GET_SMUGGLER_LOCATION_ENTITY_SPAWN_COORDS - Invalid location passed - iMissionEntity: ", iMissionEntity, " - eLocation: ", GET_SMUGGLER_LOCATION_NAME(eLocation))
	ENDIF
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC FLOAT GET_SMUGGLER_LOCATION_ENTITY_SPAWN_HEADING(INT iMissionEntity, SMUGGLER_LOCATION eLocation, BOOL bForLaunchingChecks = FALSE)

	SWITCH eLocation
		
		// Steal Aircraft					- Martin 				- Vehicle
		CASE SmugglerLocation_StealAircraft_SandyShores
			SWITCH iMissionEntity
				CASE 0			RETURN 180.2000
				CASE 1			RETURN 180.2000
				CASE 2			RETURN 222.8002
				CASE 3			RETURN 222.8002
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_StealAircraft_McKenzieAirfield
			SWITCH iMissionEntity
				CASE 0			RETURN 16.9998
				CASE 1			RETURN 16.9998
				CASE 2			RETURN 49.7998
				CASE 3			RETURN 49.7998
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_StealAircraft_EpsilonHQ
			SWITCH iMissionEntity
				CASE 0			RETURN 205.8001
				CASE 1			RETURN 205.8001
				CASE 2			RETURN 6.1990
				CASE 3			RETURN 6.1990
			ENDSWITCH
		BREAK
		
		// Splash Landing
		CASE SmugglerLocation_SplashLanding_LagoZancudo
			SWITCH iMissionEntity
				CASE 0			RETURN 332.8000
				CASE 1			RETURN 229.3000
				CASE 2			RETURN 229.3000
				CASE 3			RETURN 229.3000
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_SplashLanding_PacificOceanE
			SWITCH iMissionEntity
				CASE 0			RETURN 22.5000
				CASE 1			RETURN 170.8990
				CASE 2			RETURN 334.5990
				CASE 3			RETURN 263.2990
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_SplashLanding_PacificOceanNE
			SWITCH iMissionEntity
				CASE 0			RETURN 359.2000
				CASE 1			RETURN 332.0000
				CASE 2			RETURN 280.6990
				CASE 3			RETURN 340.5990
			ENDSWITCH
		BREAK
		
		// Black Box
		CASE SmugglerLocation_Blackbox_MountGordo			
			SWITCH iMissionEntity
				CASE 0			RETURN 31.1987
				CASE 1			RETURN 71.1988
				CASE 2			RETURN 95.3987
				CASE 3			RETURN 10.1982
				CASE 4			RETURN 22.1982
				CASE 5			RETURN 334.9981
				CASE 6			RETURN 80.5981
				CASE 7			RETURN 352.3980
			ENDSWITCH 
		BREAK
		CASE SmugglerLocation_Blackbox_PaletoForest
			SWITCH iMissionEntity
				CASE 0			RETURN 160.1997
				CASE 1			RETURN 101.9992
				CASE 2			RETURN 63.9990
				CASE 3			RETURN 298.9990
				CASE 4			RETURN 14.5988
				CASE 5			RETURN 310.7982
				CASE 6			RETURN 11.5980
				CASE 7			RETURN 57.7980
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_Blackbox_Island
			SWITCH iMissionEntity
				CASE 0			RETURN 218.9969
				CASE 1			RETURN 233.5971
				CASE 2			RETURN 284.1970
				CASE 3			RETURN 266.9967
				CASE 4			RETURN 210.1967
				CASE 5			RETURN 291.3967
				CASE 6			RETURN 158.1965
				CASE 7			RETURN 111.9964
			ENDSWITCH
		BREAK
		
		// Thermal Scope
		CASE SmugglerLocation_ThermalScope_Quarry			
			SWITCH iMissionEntity
				CASE 0			RETURN 210.7950
				CASE 1			RETURN 210.7950
				CASE 2			RETURN 275.5950
				CASE 3			RETURN 275.5950
			ENDSWITCH
		BREAK
		
		CASE SmugglerLocation_ThermalScope_PowerStation		
			SWITCH iMissionEntity
				CASE 0			RETURN 210.8000
				CASE 1			RETURN 210.8000
				CASE 2			RETURN 329.8000
				CASE 3			RETURN 329.8000
			ENDSWITCH
		BREAK
		
		CASE SmugglerLocation_ThermalScope_PlayboyMansion	
			SWITCH iMissionEntity
				CASE 0			RETURN 316.6000
				CASE 1			RETURN 316.6000
				CASE 2			RETURN 178.7990
				CASE 3			RETURN 178.7990
			ENDSWITCH
		BREAK
		
		// Infiltration
		CASE SmugglerLocation_Infiltration_HumaneLabs
		CASE SmugglerLocation_Infiltration_BolingbrokePrison
		CASE SmugglerLocation_Infiltration_Zancudo
			RETURN 47.6000
			
		// Bomb Roof
		CASE SmugglerLocation_BombRoof_Route1
		CASE SmugglerLocation_BombRoof_Route2
		CASE SmugglerLocation_BombRoof_Route3
			RETURN 98.3800
		
		// Bomb Base
		CASE SmugglerLocation_BombBase_Terminal
		CASE SmugglerLocation_BombBase_GrapeseedAutoyard
		CASE SmugglerLocation_BombBase_PlaneScrapyard
			RETURN 118.3960
			
		// Roof Attack
		CASE SmugglerLocation_RoofAttack_ConstructionSite
		CASE SmugglerLocation_RoofAttack_AltaHotel
		CASE SmugglerLocation_RoofAttack_PacificStandard
			RETURN 0.0
			
		// Stunt Pilot
		CASE SmugglerLocation_StuntPilot_Location1
		CASE SmugglerLocation_StuntPilot_Location2
		CASE SmugglerLocation_StuntPilot_Location3
		CASE SmugglerLocation_StuntPilot_Location4
		CASE SmugglerLocation_StuntPilot_Location5
		CASE SmugglerLocation_StuntPilot_Location6
		CASE SmugglerLocation_StuntPilot_Location7
		CASE SmugglerLocation_StuntPilot_Location8
		CASE SmugglerLocation_StuntPilot_Location9
		CASE SmugglerLocation_StuntPilot_Location10
			RETURN 0.0
			
		// Beacon Grab
		CASE SmugglerLocation_BeaconGrab_Location1
		CASE SmugglerLocation_BeaconGrab_Location2
		CASE SmugglerLocation_BeaconGrab_Location3
		CASE SmugglerLocation_BeaconGrab_Location4
		CASE SmugglerLocation_BeaconGrab_Location5
			RETURN 0.0
		
		// Escort
		CASE SmugglerLocation_Escort_Placeholder1
		CASE SmugglerLocation_Escort_Placeholder2
		CASE SmugglerLocation_Escort_Placeholder3
			SWITCH iMissionEntity
				CASE 0 RETURN 0.0
				CASE 1 RETURN 135.0
				CASE 2 RETURN 225.0
				CASE 3 RETURN 270.0
			ENDSWITCH
		BREAK
		
		CASE SmugglerLocation_Setup_DelPerroBeach
			SWITCH iMissionEntity
				CASE 0 RETURN 0.0003
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_Setup_LSRiver
			SWITCH iMissionEntity
				CASE 0 RETURN 151.5308
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_Setup_ElBurro
			SWITCH iMissionEntity
				CASE 0 RETURN 339.7852
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_Setup_Chumash
			SWITCH iMissionEntity
				CASE 0 RETURN 353.8231
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_Setup_Tutuavium
			SWITCH iMissionEntity
				CASE 0 RETURN 28.5109
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_Setup_RonsWindFarm
			SWITCH iMissionEntity
				CASE 0 RETURN 182.7954
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_Setup_SenoraDesert1
			SWITCH iMissionEntity
				CASE 0 RETURN 315.6512
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_Setup_SenoraDesert2
			SWITCH iMissionEntity
				CASE 0 RETURN 336.7934
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_Setup_SandyShores
			SWITCH iMissionEntity
				CASE 0 RETURN 92.1980
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_Setup_StabCity
			SWITCH iMissionEntity
				CASE 0 RETURN 209.1977
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_Setup_NCalafiaWay
			SWITCH iMissionEntity
				CASE 0 RETURN 202.1409
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_Setup_McKenzie
			SWITCH iMissionEntity
				CASE 0 RETURN 141.9814
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_Setup_Grapeseed
			SWITCH iMissionEntity
				CASE 0 RETURN 125.6944
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_Setup_ProcopioBeach
			SWITCH iMissionEntity
				CASE 0 RETURN 65.9329
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_Setup_Paleto
			SWITCH iMissionEntity
				CASE 0 RETURN 94.3976
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	IF NOT bForLaunchingChecks
		SCRIPT_ASSERT("GET_SMUGGLER_LOCATION_ENTITY_SPAWN_HEADING - Invalid location passed! Further details in logs.")
		PRINTLN("[SMUGGLER] GET_SMUGGLER_LOCATION_ENTITY_SPAWN_HEADING - Invalid location passed - iMissionEntity: ", iMissionEntity, " - eLocation: ", GET_SMUGGLER_LOCATION_NAME(eLocation))
	ENDIF
	RETURN 0.0
	
ENDFUNC

FUNC VECTOR GET_SMUGGLER_ENTITY_SPAWN_COORDS(SMUGGLER_VARIATION eVariation, SMUGGLER_LOCATION eLocation, INT iMissionEntity, BOOL bForLaunchingChecks = FALSE)

	UNUSED_PARAMETER(eVariation)

	RETURN GET_SMUGGLER_LOCATION_ENTITY_SPAWN_COORDS(eLocation, iMissionEntity, bForLaunchingChecks)
	
ENDFUNC

FUNC FLOAT GET_SMUGGLER_ENTITY_SPAWN_HEADING(SMUGGLER_VARIATION eVariation, SMUGGLER_LOCATION eLocation, BOOL bDoingDriveToLocation, INT iMissionEntity, BOOL bForLaunchingChecks = FALSE)

	UNUSED_PARAMETER(eVariation)
	UNUSED_PARAMETER(bDoingDriveToLocation)

	RETURN GET_SMUGGLER_LOCATION_ENTITY_SPAWN_HEADING(iMissionEntity, eLocation, bForLaunchingChecks)
	
ENDFUNC

FUNC VECTOR GET_SMUGGLER_ENTITY_PICKUP_ROTATION(INT iMissionEntity, SMUGGLER_LOCATION eLocation)

	SWITCH eLocation
	
		// Beacon Grab							- ??					- ??
		CASE SmugglerLocation_BeaconGrab_Location1
			SWITCH iMissionEntity
				CASE 0			RETURN <<0.0000, 0.0000, 0.0000>>
				CASE 1			RETURN <<0.0000, 0.0000, 0.0000>>
				CASE 2			RETURN <<0.0000, 0.0000, 0.0000>>
				CASE 3			RETURN <<0.0000, 0.0000, 0.0000>>
				CASE 4			RETURN <<0.0000, 0.0000, 0.0000>>
				CASE 5			RETURN <<0.0000, 0.0000, 0.0000>>
				CASE 6			RETURN <<0.0000, 0.0000, 0.0000>>
				CASE 7			RETURN <<0.0000, 0.0000, 0.0000>>
			ENDSWITCH
		BREAK
		
		// Splash Landing
		CASE SmugglerLocation_SplashLanding_LagoZancudo
			SWITCH iMissionEntity
				CASE 0			RETURN <<3.7000, 0.0000, -27.2000>>
				CASE 1			RETURN <<-6.2001, -20.6000, -130.7000>>
				CASE 2			RETURN <<-2.8002, -4.3003, -130.7000>>
				CASE 3			RETURN <<-1.1001, -21.4001, -130.7000>>
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_SplashLanding_PacificOceanE
			SWITCH iMissionEntity
				CASE 0			RETURN <<-14.7000, -6.6011, 22.5000>>
				CASE 1			RETURN <<16.7000, 13.4000, 170.8990>>
				CASE 2			RETURN <<-4.7000, 6.3000, -25.4010>>
				CASE 3			RETURN <<8.0000, 11.3000, -96.7010>>
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_SplashLanding_PacificOceanNE
			SWITCH iMissionEntity
				CASE 0			RETURN <<0.0000, 14.0000, -0.8000>>
				CASE 1			RETURN <<23.7000, -12.1000, -28.0000>>
				CASE 2			RETURN <<-25.5000, -1.3000, -79.3010>>
				CASE 3			RETURN <<-7.0000, 7.1000, -19.4010>>
			ENDSWITCH
		BREAK
		
		// Black Box
		CASE SmugglerLocation_Blackbox_MountGordo			
			SWITCH iMissionEntity
				CASE 0			RETURN <<-6.3103, -1.6122, 31.1098>>
				CASE 1			RETURN <<0.0000, 0.0000, 71.1988>>
				CASE 2			RETURN <<-28.1876, -3.8981, 94.4196>>
				CASE 3			RETURN <<0.0000, 0.0000, 10.1982>>
				CASE 4			RETURN <<-26.9908, 11.0987, 24.8697>>
				CASE 5			RETURN <<-25.2639, -2.5614, -25.5760>>
				CASE 6			RETURN <<-8.3072, -12.8070, 79.6642>>
				CASE 7			RETURN <<-19.2242, 0.6666, -7.4891>>
			ENDSWITCH 
		BREAK
		CASE SmugglerLocation_Blackbox_PaletoForest
			SWITCH iMissionEntity
				CASE 0			RETURN <<-19.5269, 0.5486, 160.2941>>
				CASE 1			RETURN <<-8.9841, 21.9745, 103.7469>>
				CASE 2			RETURN <<-15.7877, 4.6880, 64.6494>>
				CASE 3			RETURN <<3.3605, 12.7279, -61.3760>>
				CASE 4			RETURN <<-3.7256, -13.1379, 14.1696>>
				CASE 5			RETURN <<9.5470, -19.6599, -47.5440>>
				CASE 6			RETURN <<-9.3716, -10.4775, 10.7369>>
				CASE 7			RETURN <<-8.8267, 0.5460, 57.8401>>
			ENDSWITCH
		BREAK
		CASE SmugglerLocation_Blackbox_Island
			SWITCH iMissionEntity
				CASE 0			RETURN <<9.7028, 7.5993, -141.6491>>
				CASE 1			RETURN <<0.0000, -0.0000, -126.4029>>
				CASE 2			RETURN <<0.0000, 0.0000, -75.8030>>
				CASE 3			RETURN <<-8.6228, -9.3771, -93.7118>>
				CASE 4			RETURN <<0.0000, -0.0000, -149.8033>>
				CASE 5			RETURN <<0.0000, 0.0000, -68.6033>>
				CASE 6			RETURN <<3.4839, 1.4729, 158.1517>>
				CASE 7			RETURN <<-8.0258, 4.0912, 112.2835>>
			ENDSWITCH
		BREAK
		
	ENDSWITCH
	
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

/////////////////////////////////////////////////
/// SMUGGLER NON-MISSION VEHICLE SPAWN COORDS ///
/////////////////////////////////////////////////

FUNC VECTOR GET_SMV_CARGO_PLANE_VEHICLE_SPAWN_COORDS(INT iVehicle, SMUGGLER_LOCATION eLocation, BOOL bReturnDestination = FALSE)

	VECTOR vSpawnCoord, vDestinationCoord
	FLOAT fHeadingBetween

	SWITCH eLocation
		CASE SmugglerLocation_CargoPlane_Placeholder1
			vSpawnCoord = <<3100.000, -3200.000, 1000.00>>
			vDestinationCoord = <<-3000.000, 7000.000, 1200.00>>
		BREAK
		CASE SmugglerLocation_CargoPlane_Placeholder2
			vSpawnCoord = <<3600.000, 7000.000, 1000.00>>
			vDestinationCoord = <<-3000.000, -3800.000, 1200.00>>
		BREAK
		CASE SmugglerLocation_CargoPlane_Placeholder3
			vSpawnCoord = <<4000.000, 681.8651, 1000.00>>
			vDestinationCoord = <<-4000.000, 681.8651, 1200.00>>
		BREAK
	ENDSWITCH
	
	fHeadingBetween = GET_HEADING_BETWEEN_VECTORS_2D(vSpawnCoord, vDestinationCoord)
	
	IF bReturnDestination
		SWITCH iVehicle
			CASE 0			RETURN vDestinationCoord
			CASE 1			RETURN GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vDestinationCoord, fHeadingBetween, <<-400.0, -300.0, 0.0>>)
			CASE 2			RETURN GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vDestinationCoord, fHeadingBetween, <<400.0, -600.0, 0.0>>)
		ENDSWITCH
	ELSE
		SWITCH iVehicle
			CASE 0			RETURN vSpawnCoord
			CASE 1			RETURN GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vSpawnCoord, fHeadingBetween, <<-400.0, -300.0, 0.0>>)
			CASE 2			RETURN GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vSpawnCoord, fHeadingBetween, <<400.0, -600.0, 0.0>>)
		ENDSWITCH
	ENDIF
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC FLOAT GET_SMV_CARGO_PLANE_VEHICLE_SPAWN_HEADING(INT iVehicle, SMUGGLER_LOCATION eLocation)
	RETURN GET_HEADING_BETWEEN_VECTORS_2D(GET_SMV_CARGO_PLANE_VEHICLE_SPAWN_COORDS(iVehicle, eLocation), GET_SMV_CARGO_PLANE_VEHICLE_SPAWN_COORDS(iVehicle, eLocation, TRUE))
ENDFUNC

FUNC VECTOR GET_SMV_CARGO_PLANE_SUPPORT_JET_SPAWN_COORDS(INT iVehicle, SMUGGLER_LOCATION eLocation, BOOL bReturnDestination = FALSE)
	SWITCH iVehicle
		CASE 3	RETURN (GET_SMV_CARGO_PLANE_VEHICLE_SPAWN_COORDS(0, eLocation, bReturnDestination) + <<-100.0, -75.0, 75.0>>)
		CASE 4	RETURN (GET_SMV_CARGO_PLANE_VEHICLE_SPAWN_COORDS(0, eLocation, bReturnDestination) + <<100.0, -75.0, 75.0>>)
		CASE 5	RETURN (GET_SMV_CARGO_PLANE_VEHICLE_SPAWN_COORDS(1, eLocation, bReturnDestination) + <<-100.0, -75.0, 75.0>>)
		CASE 6	RETURN (GET_SMV_CARGO_PLANE_VEHICLE_SPAWN_COORDS(1, eLocation, bReturnDestination) + <<100.0, -75.0, 75.0>>)
		CASE 7	RETURN (GET_SMV_CARGO_PLANE_VEHICLE_SPAWN_COORDS(2, eLocation, bReturnDestination) + <<-100.0, -75.0, 75.0>>)
		CASE 8	RETURN (GET_SMV_CARGO_PLANE_VEHICLE_SPAWN_COORDS(2, eLocation, bReturnDestination) + <<100.0, -75.0, 75.0>>)
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC FLOAT GET_SMV_CARGO_PLANE_SUPPORT_JET_SPAWN_HEADING(INT iVehicle, SMUGGLER_LOCATION eLocation)
	RETURN GET_HEADING_BETWEEN_VECTORS_2D(GET_SMV_CARGO_PLANE_SUPPORT_JET_SPAWN_COORDS(iVehicle, eLocation), GET_SMV_CARGO_PLANE_SUPPORT_JET_SPAWN_COORDS(iVehicle, eLocation, TRUE))
ENDFUNC

FUNC VECTOR GET_NON_SMUGGLER_VEHICLE_SPAWN_COORDS(SMUGGLER_VARIATION eVariation, SMUGGLER_LOCATION eLocation, INT iVehicle, FREEMODE_DELIVERY_DROPOFFS eDropOff = FREEMODE_DELIVERY_DROPOFF_INVALID, INT iRandomSelection = -1, INT iRespawnLocation = 0)

	SWITCH eVariation
		CASE SMV_STEAL_AIRCRAFT			
			SWITCH eLocation
				CASE SmugglerLocation_StealAircraft_SandyShores
					SWITCH iVehicle
						CASE 0			RETURN <<1726.7080, 3283.2710, 40.0670>>
						CASE 1			RETURN <<1733.3347, 3320.6499, 40.2235>>
						CASE 2			RETURN <<1724.4729, 3313.0564, 40.2235>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_StealAircraft_McKenzieAirfield
					SWITCH iVehicle
						CASE 0			RETURN <<2149.0581, 4797.2710, 40.1360>>
						CASE 1			RETURN <<2133.5850, 4778.6807, 39.9703>>
						CASE 2			RETURN <<2131.6846, 4793.2813, 40.1269>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_StealAircraft_EpsilonHQ
					SWITCH iVehicle
						CASE 0			RETURN <<-680.2657, 74.6550, 68.6856>>
						CASE 1			RETURN <<-676.2980, 101.5110, 54.8550>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE SMV_BEACON_GRAB					
			SWITCH iRandomSelection
				CASE 0
					SWITCH iVehicle
						CASE 0			RETURN <<-1391.7581, 195.8287, 57.6872>>
						CASE 1			RETURN <<-1391.7581, 195.8287, 57.6872>>
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH iVehicle
						CASE 0			RETURN <<1066.8962, -519.0798, 61.4449>>
						CASE 1			RETURN <<1066.8962, -519.0798, 61.4449>>
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iVehicle
						CASE 0			RETURN <<1785.2509, 3685.7651, 33.2577>>
						CASE 1			RETURN <<1785.2509, 3685.7651, 33.2577>>
					ENDSWITCH
				BREAK
				CASE 3
					SWITCH iVehicle
						CASE 0			RETURN <<2390.7458, 4880.3086, 40.0175>>
						CASE 1			RETURN <<2390.7458, 4880.3086, 40.0175>>
					ENDSWITCH
				BREAK
				CASE 4
					SWITCH iVehicle
						CASE 0			RETURN <<-175.0695, 6359.5708, 30.5224>>
						CASE 1			RETURN <<-175.0695, 6359.5708, 30.5224>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_SPLASH_LANDING
			SWITCH eLocation
				CASE SmugglerLocation_SplashLanding_LagoZancudo
					SWITCH iVehicle
						CASE 0			RETURN <<-3224.9509, 3030.6321, -0.1880>>
						CASE 1			RETURN <<-3177.2129, 3017.1799, 0.0000>>
						CASE 2			RETURN <<-3198.5410, 3050.2588, 0.0001>>
						CASE 3			RETURN <<-3209.3210, 3000.0488, -0.1333>>
						CASE 4			RETURN <<-3204.2336, 3025.1838, -0.7500>>
						CASE 5			RETURN <<-3199.0186, 3008.5759, -0.1876>>
						CASE 6			RETURN <<-3239.5442, 3016.6643, 1.000>>
						CASE 7			RETURN <<-3249.7681, 3021.1711, 1.000>>
						CASE 8			RETURN <<-3243.6560, 3017.9814, 33.8841>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_SplashLanding_PacificOceanE
					SWITCH iVehicle
						CASE 0			RETURN <<3905.3750, 3022.7539, 0.0000>>
						CASE 1			RETURN <<3853.0090, 3019.2129, -0.0000>>
						CASE 2			RETURN <<3883.7744, 3059.9897, 0.9743>>
						CASE 3			RETURN <<3869.8308, 2984.1692, 0.5626>>
						CASE 4			RETURN <<3873.8062, 3029.2185, -0.1875>>
						CASE 5			RETURN <<3886.6416, 3000.9807, 0.7493>>
						CASE 6			RETURN <<3847.8335, 3043.4338, 1.000>>
						CASE 7			RETURN <<3858.3618, 3040.0496, 1.000>>
						CASE 8			RETURN <<3852.7705, 3043.3779, 33.6913>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_SplashLanding_PacificOceanNE
					SWITCH iVehicle
						CASE 0			RETURN <<3400.2690, 6304.7900, -0.0000>>
						CASE 1			RETURN <<3382.7429, 6361.9858, -0.1880>>
						CASE 2			RETURN <<3417.6838, 6340.3457, 0.0001>>
						CASE 3			RETURN <<3372.5913, 6328.2109, 0.1875>>
						CASE 4			RETURN <<3396.8887, 6331.0449, 1.3126>>
						CASE 5			RETURN <<3387.4465, 6316.5381, 0.9375>>
						CASE 6			RETURN <<3368.7874, 6292.1450, 1.000>>
						CASE 7			RETURN <<3362.1069, 6300.2681, 1.000>>
						CASE 8			RETURN <<3366.3618, 6296.5488, 33.5670>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_DOGFIGHT
			SWITCH eLocation
				CASE SmugglerLocation_Dogfight_Placeholder1			
					SWITCH iVehicle
						CASE 0			RETURN <<-2200.5774, -384.6226, 600.00>>
						CASE 1			RETURN <<-2250.5774, -384.6226, 600.00>>
						CASE 2			RETURN <<-2300.5774, -384.6226, 600.00>>
						CASE 3			RETURN <<-2350.5774, -384.6226, 600.00>>
						CASE 4			RETURN <<-2400.5774, -384.6226, 600.00>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Dogfight_Placeholder2	
					SWITCH iVehicle
						CASE 0			RETURN <<2380.1345, 1280.5082, 600.00>>
						CASE 1			RETURN <<2430.1345, 1280.5082, 600.00>>
						CASE 2			RETURN <<2480.1345, 1280.5082, 600.00>>
						CASE 3			RETURN <<2530.1345, 1280.5082, 600.00>>
						CASE 4			RETURN <<2580.1345, 1280.5082, 600.00>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Dogfight_Placeholder3		
					SWITCH iVehicle
						CASE 0			RETURN <<720.6462, 2531.4600, 600.00>>
						CASE 1			RETURN <<770.6462, 2531.4600, 600.00>>
						CASE 2			RETURN <<820.6462, 2531.4600, 600.00>>
						CASE 3			RETURN <<870.6462, 2531.4600, 600.00>>
						CASE 4			RETURN <<920.6462, 2531.4600, 600.00>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_BOMB_BASE
			SWITCH eLocation
				CASE SmugglerLocation_BombBase_Terminal
					SWITCH iVehicle
						CASE 0			RETURN <<1289.0820, -3329.9131, 4.9420>>
						CASE 1			RETURN <<1245.6910, -3298.6111, 4.8160>>
						CASE 2			RETURN <<1245.8380, -3289.0540, 4.7450>>
						CASE 3			RETURN <<1245.6150, -3262.9641, 4.7300>>
						CASE 4			RETURN <<1275.9170, -3269.0200, 4.9020>>
						CASE 5			RETURN <<1292.8110, -3243.0310, 4.9063>>
						CASE 6			RETURN <<1293.5330, -3261.2900, 4.9070>>
						CASE 7			RETURN <<1275.8040, -3240.0820, 4.9020>>
						CASE 8			RETURN <<1306.2466, -3245.7712, 1.5000>>
						CASE 9			RETURN <<1263.3330, -3356.0310, 0.0000>>
						CASE 10			RETURN <<1332.9474, -3334.1150, 0.1875>>
						CASE 11			RETURN <<1272.9399, -3315.9919, 4.9020>>
						CASE 12			RETURN <<744.2346, -3176.6636, 4.9005>>
						CASE 13			RETURN <<735.2852, -3166.1895, 4.9005>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_BombBase_GrapeseedAutoyard
					SWITCH iVehicle
						CASE 0			RETURN <<2353.2939, 4875.8618, 40.8260>>
						CASE 1			RETURN <<2346.7241, 4882.2139, 40.8260>>
						CASE 2			RETURN <<2336.4231, 4845.0952, 40.1300>>
						CASE 3			RETURN <<2330.0779, 4838.8921, 40.0370>>
						CASE 4			RETURN <<2270.4160, 4842.0708, 39.5420>>
						CASE 5			RETURN <<2262.7129, 4850.4702, 39.6590>>
						CASE 6			RETURN <<2266.5139, 4920.7490, 39.9530>>
						CASE 7			RETURN <<2250.6741, 4941.7261, 40.0990>>
						CASE 8			RETURN <<2268.3091, 4962.3232, 40.5700>>
						CASE 9			RETURN <<2270.1689, 4975.3750, 41.1720>>
						CASE 10			RETURN <<2281.6560, 4883.9258, 39.9940>>
						CASE 11			RETURN <<2301.1450, 4897.7290, 40.2890>>
						CASE 12			RETURN <<2002.1001, 4712.4922, 40.6149>>
						CASE 13			RETURN <<1988.9009, 4705.6772, 40.5671>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_BombBase_PlaneScrapyard
					SWITCH iVehicle
						CASE 0			RETURN <<2352.3921, 3041.4519, 47.1520>>
						CASE 1			RETURN <<2377.8540, 3058.2729, 47.1530>>
						CASE 2			RETURN <<2350.7051, 3107.2051, 47.2300>>
						CASE 3			RETURN <<2346.4580, 3108.0681, 47.2200>>
						CASE 4			RETURN <<2423.2361, 3132.7720, 47.1820>>
						CASE 5			RETURN <<2412.9031, 3136.7400, 47.2090>>
						CASE 6			RETURN <<2394.2830, 3100.4800, 47.1520>>
						CASE 7			RETURN <<2406.0039, 3097.6599, 47.1530>>
						CASE 8			RETURN <<2422.8540, 3110.1819, 47.1530>>
						CASE 9			RETURN <<2369.6873, 3119.7300, 47.2706>>
						CASE 10			RETURN <<2367.4871, 3106.1641, 46.9270>>
						CASE 11			RETURN <<2439.7964, 3110.0530, 46.6891>>
						CASE 12			RETURN <<2408.4583, 3324.2749, 46.6277>>
						CASE 13			RETURN <<2411.6665, 3336.4106, 45.9022>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		
		CASE SMV_THERMAL_SCOPE
			SWITCH eLocation
				CASE SmugglerLocation_ThermalScope_Quarry
					SWITCH iVehicle
						CASE 0			RETURN <<1076.6550, 2191.1040, 47.0850>>
						CASE 1			RETURN <<1104.6290, 2209.5581, 47.5260>>
						CASE 2			RETURN <<1162.0389, 2130.1499, 54.1510>>
						CASE 3			RETURN <<1146.4680, 2099.0371, 54.8000>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_ThermalScope_PowerStation
					SWITCH iVehicle
						CASE 0			RETURN <<2748.9199, 1442.4780, 23.4890>>
						CASE 1			RETURN <<2765.9590, 1439.8640, 23.4770>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_ThermalScope_PlayboyMansion
					SWITCH iVehicle
						CASE 0			RETURN <<-1558.4170, 110.3480, 55.7800>>
						CASE 1			RETURN <<-1525.6040, 91.6750, 55.5380>>
						CASE 2			RETURN <<-1523.9060, 97.2490, 55.6720>>
						CASE 3			RETURN <<-1575.0680, 92.2600, 57.6750>>
						CASE 4			RETURN <<-1524.3650, 74.0450, 60.3140>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK		
		
		CASE SMV_CARGO_PLANE
			SWITCH iVehicle
				CASE 0
				CASE 1
				CASE 2
					RETURN GET_SMV_CARGO_PLANE_VEHICLE_SPAWN_COORDS(iVehicle, eLocation)
			ENDSWITCH
			RETURN GET_SMV_CARGO_PLANE_SUPPORT_JET_SPAWN_COORDS(iVehicle, eLocation)
		
		CASE SMV_ROOF_ATTACK
			SWITCH eLocation
				CASE SmugglerLocation_RoofAttack_ConstructionSite
					SWITCH iVehicle
						CASE 0			RETURN <<-164.1431, -997.6091, 253.1314>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_BOMBING_RUN
			SWITCH eLocation
				CASE SmugglerLocation_BombingRun_Placeholder1
					SWITCH iVehicle
						CASE 0			RETURN <<1707.0422, -2820.6853, 0.1875>>
						CASE 1			RETURN <<-3295.1150, 1558.6477, -1.6875>>
						CASE 2			RETURN <<3519.4211, 5210.8936, 1.1249>>
						CASE 3			RETURN <<1745.8531, 3755.4138, 32.9299>>
						CASE 4			RETURN <<-302.6415, 6229.3120, 30.4542>>
						CASE 5			RETURN <<987.3046, -657.9983, 56.6113>>
						CASE 6			RETURN <<310.8066, 2577.6968, 43.1253>>
						CASE 7			RETURN <<-1437.4830, 262.3509, 59.9146>>
						CASE 8			RETURN <<110.2688, -1865.4857, 23.4505>>
						CASE 9			RETURN <<-548.0477, -658.3134, 32.2713>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_BombingRun_Placeholder2
					SWITCH iVehicle
						CASE 0			RETURN <<3136.3308, -392.7423, 0.3750>>
						CASE 1			RETURN <<1040.3850, 4050.6101, 30.7500>>
						CASE 2			RETURN <<-2239.2461, 4972.6401, 0.3751>>
						CASE 3			RETURN <<2067.4177, 5013.0254, 40.0294>>
						CASE 4			RETURN <<-3175.1011, 910.9316, 13.5213>>
						CASE 5			RETURN <<123.3600, -26.8273, 66.7593>>
						CASE 6			RETURN <<1212.2954, -1764.0031, 38.9814>>
						CASE 7			RETURN <<2554.1746, 1641.6437, 28.0813>>
						CASE 8			RETURN <<1173.3838, 1815.5480, 73.6187>>
						CASE 9			RETURN <<197.6640, 6574.6973, 30.8134>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_BombingRun_Placeholder3
					SWITCH iVehicle
						CASE 0			RETURN <<571.7643, 6911.3447, 1.8749>>
						CASE 1			RETURN <<3157.6143, 1536.3463, 0.1875>>
						CASE 2			RETURN <<126.9530, -2746.4829, 0.9370>>
						CASE 3			RETURN <<978.0015, -3104.9851, 4.9007>>
						CASE 4			RETURN <<302.0589, -1067.9849, 28.4224>>
						CASE 5			RETURN <<-1506.2325, -697.7436, 26.6401>>
						CASE 6			RETURN <<316.5249, 1002.4792, 209.5461>>
						CASE 7			RETURN <<416.6307, 3479.7122, 33.6590>>
						CASE 8			RETURN <<-1550.3002, 1405.9318, 123.8970>>
						CASE 9			RETURN <<2510.5288, 4131.2876, 37.5870>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_BOMB_ROOF
			SWITCH eLocation
				CASE SmugglerLocation_BombRoof_Route1
					SWITCH iRespawnLocation
						CASE 0
							SWITCH iVehicle
								CASE 0			RETURN <<984.5420, 67.0510, 106.7680>>
								CASE 1			RETURN <<982.9010, 42.8780, 103.0180>>
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH iVehicle
								CASE 0			RETURN <<-1009.9680, -756.6220, 78.8580>>
								CASE 1			RETURN <<-997.6030, -730.9060, 75.5370>>
							ENDSWITCH
						BREAK
						CASE 2
							SWITCH iVehicle
								CASE 0			RETURN <<-770.5180, 769.0880, 212.1990>>
								CASE 1			RETURN <<-752.5950, 798.5830, 219.1450>>
							ENDSWITCH
						BREAK
						CASE 3
							SWITCH iVehicle
								CASE 0			RETURN <<766.0810, -1707.1920, 43.1470>>
								CASE 1			RETURN <<784.3550, -1691.7550, 43.1470>>
							ENDSWITCH
						BREAK
						CASE 4
							SWITCH iVehicle
								CASE 0			RETURN <<-2211.0020, 257.6060, 197.1090>>
								CASE 1			RETURN <<-2210.5100, 275.9050, 197.1090>>
							ENDSWITCH
						BREAK
					ENDSWITCH
					SWITCH iVehicle
						CASE 2			RETURN <<-448.7445, 294.5321, 82.2397>>
						CASE 3			RETURN <<-436.7970, 293.0325, 90.2198>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_BombRoof_Route2
					SWITCH iRespawnLocation
						CASE 0
							SWITCH iVehicle
								CASE 0			RETURN <<37.5320, 181.2760, 125.2160>>
								CASE 1			RETURN <<42.6190, 158.5800, 125.2160>>
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH iVehicle
								CASE 0			RETURN <<51.8560, -1014.2780, 82.4060>>
								CASE 1			RETURN <<64.8340, -1029.5460, 78.7520>>
							ENDSWITCH
						BREAK
						CASE 2
							SWITCH iVehicle
								CASE 0			RETURN <<-1646.9821, -507.2890, 77.5070>>
								CASE 1			RETURN <<-1657.1689, -527.5870, 74.0570>>
							ENDSWITCH
						BREAK
						CASE 3
							SWITCH iVehicle
								CASE 0			RETURN <<-35.8980, -603.8240, 111.0040>>
								CASE 1			RETURN <<-22.3550, -585.6070, 105.3280>>
							ENDSWITCH
						BREAK
						CASE 4
							SWITCH iVehicle
								CASE 0			RETURN <<-697.5300, 681.4570, 157.5870>>
								CASE 1			RETURN <<-704.0520, 697.1560, 169.0360>>
							ENDSWITCH
						BREAK
					ENDSWITCH
					SWITCH iVehicle
						CASE 2			RETURN <<-567.9655, 324.5697, 83.4512>>
						CASE 3			RETURN <<-575.3657, 290.7993, 93.0922>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_BombRoof_Route3
					SWITCH iRespawnLocation
						CASE 0
							SWITCH iVehicle
								CASE 0			RETURN <<-659.8480, 325.6990, 139.1520>>
								CASE 1			RETURN <<-642.3170, 311.6370, 139.1520>>
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH iVehicle
								CASE 0			RETURN <<-225.6320, -845.7730, 127.8910>>
								CASE 1			RETURN <<-226.1500, -826.3990, 125.7930>>
							ENDSWITCH
						BREAK
						CASE 2
							SWITCH iVehicle
								CASE 0			RETURN <<766.5200, -1772.6379, 52.2920>>
								CASE 1			RETURN <<760.6560, -1794.7080, 48.2920>>
							ENDSWITCH
						BREAK
						CASE 3
							SWITCH iVehicle
								CASE 0			RETURN <<-836.8330, -718.5930, 120.2730>>
								CASE 1			RETURN <<-817.8400, -710.6750, 123.2380>>
							ENDSWITCH
						BREAK
						CASE 4
							SWITCH iVehicle
								CASE 0			RETURN <<328.4170, 105.2700, 144.1690>>
								CASE 1			RETURN <<313.8520, 126.1560, 144.1690>>
							ENDSWITCH
						BREAK
					ENDSWITCH
					SWITCH iVehicle
						CASE 2			RETURN <<-1449.9022, -235.8300, 60.1281>>
						CASE 3			RETURN <<-1456.7855, -243.3493, 60.1281>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_STUNT_PILOT
			SWITCH eLocation
				CASE SmugglerLocation_StuntPilot_Location1
					SWITCH iVehicle
						CASE 0			RETURN <<-401.8466, 6379.9121, 12.9703>>
						CASE 1			RETURN <<-391.3758, 6381.6948, 13.1631>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_StuntPilot_Location2
					SWITCH iVehicle
						CASE 0			RETURN <<1307.9705, 6510.4595, 18.9334>>
						CASE 1			RETURN <<1313.2063, 6512.0244, 18.7545>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_StuntPilot_Location3
					SWITCH iVehicle
						CASE 0			RETURN <<2042.0511, 4725.8940, 40.4531>>
						CASE 1			RETURN <<2033.4088, 4721.5313, 40.4334>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_StuntPilot_Location4
					SWITCH iVehicle
						CASE 0			RETURN <<-2270.6379, 4283.8081, 44.8770>>
						CASE 1			RETURN <<-2268.0857, 4287.9604, 45.1511>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_StuntPilot_Location5
					SWITCH iVehicle
						CASE 0			RETURN <<1274.4612, 3160.5254, 39.8807>>
						CASE 1			RETURN <<1265.5695, 3164.4351, 39.7985>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_StuntPilot_Location6
					SWITCH iVehicle
						CASE 0			RETURN <<1304.9149, 1451.2892, 98.2596>>
						CASE 1			RETURN <<1296.1133, 1432.9905, 99.1033>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_StuntPilot_Location7
					SWITCH iVehicle
						CASE 0			RETURN <<-3174.8477, 1205.0298, 8.5478>>
						CASE 1			RETURN <<-3173.6492, 1214.3809, 8.7268>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_StuntPilot_Location8
					SWITCH iVehicle
						CASE 0			RETURN <<648.6086, -1044.4624, 21.1075>>
						CASE 1			RETURN <<652.8605, -1046.0258, 21.3577>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_StuntPilot_Location9
					SWITCH iVehicle
						CASE 0			RETURN <<605.8329, -1709.2822, 21.0144>>
						CASE 1			RETURN <<604.9275, -1721.1699, 20.3649>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_StuntPilot_Location10
					SWITCH iVehicle
						CASE 0			RETURN <<1286.0010, -2561.8174, 43.1652>>
						CASE 1			RETURN <<1294.5327, -2548.0015, 42.6570>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_SELL_CONTESTED
			SWITCH eDropOff
				CASE SMUGGLER_DROPOFF_CONTESTED_1
					RETURN <<-1251.2791, -845.5270, 67.8580>>				
				CASE SMUGGLER_DROPOFF_CONTESTED_2
					RETURN <<237.8204, -3191.3250, 39.5347>>				
				CASE SMUGGLER_DROPOFF_CONTESTED_3
					RETURN <<-133.7180, -1281.3500, 46.8980>>				
				CASE SMUGGLER_DROPOFF_CONTESTED_4
					RETURN <<911.9180, -1670.7280, 50.1320>>				
				CASE SMUGGLER_DROPOFF_CONTESTED_5
					RETURN <<948.4640, -936.2100, 58.0920>>				
				CASE SMUGGLER_DROPOFF_CONTESTED_6
					RETURN <<72.8840, -347.5020, 66.2020>>				
				CASE SMUGGLER_DROPOFF_CONTESTED_7
					RETURN <<-1571.4490, 203.0380, 73.3380>>				
				CASE SMUGGLER_DROPOFF_CONTESTED_8
					RETURN <<-2256.1790, 360.8610, 187.6020>>				
				CASE SMUGGLER_DROPOFF_CONTESTED_9
					RETURN <<-734.6190, -222.8170, 47.5190>>				
				CASE SMUGGLER_DROPOFF_CONTESTED_10
					RETURN <<48.8285, -1769.8120, 46.7003>>				
				CASE SMUGGLER_DROPOFF_CONTESTED_11
					RETURN <<841.9620, -2527.7190, 39.5250>>				
				CASE SMUGGLER_DROPOFF_CONTESTED_12
					RETURN <<-408.9670, -349.7540, 75.8030>>				
				CASE SMUGGLER_DROPOFF_CONTESTED_13
					RETURN <<89.0880, -925.7630, 84.2890>>				
				CASE SMUGGLER_DROPOFF_CONTESTED_14
					RETURN <<188.0810, 230.3190, 142.6650>>				
				CASE SMUGGLER_DROPOFF_CONTESTED_15
					RETURN <<1243.4680, -2979.8455, 13.9850>>				
				CASE SMUGGLER_DROPOFF_CONTESTED_16
					RETURN <<-135.6190, 6167.3242, 39.1650>>				
				CASE SMUGGLER_DROPOFF_CONTESTED_17
					RETURN <<2731.8601, 3480.6841, 72.7040>>				
				CASE SMUGGLER_DROPOFF_CONTESTED_18
					RETURN <<1715.2800, 4772.0151, 46.5820>>				
				CASE SMUGGLER_DROPOFF_CONTESTED_19
					RETURN <<590.0570, 2752.2510, 50.1450>>				
				CASE SMUGGLER_DROPOFF_CONTESTED_20
					RETURN <<742.5180, 1297.1750, 359.2970>>				
				CASE SMUGGLER_DROPOFF_CONTESTED_21
					RETURN <<-1115.5110, 2711.7451, 22.8410>>				
				CASE SMUGGLER_DROPOFF_CONTESTED_22
					RETURN <<1529.0128, 3580.2485, 40.9926>>				
				CASE SMUGGLER_DROPOFF_CONTESTED_23
					RETURN <<-1588.2789, 756.2960, 188.1942>>				
				CASE SMUGGLER_DROPOFF_CONTESTED_24
					RETURN <<249.0290, 3599.3049, 33.2930>>				
				CASE SMUGGLER_DROPOFF_CONTESTED_25
					RETURN <<164.3110, 7362.3691, 11.2700>>				
				CASE SMUGGLER_DROPOFF_CONTESTED_26
					RETURN <<2940.9600, 770.9070, 24.8180>>
				CASE SMUGGLER_DROPOFF_CONTESTED_27
					RETURN <<3632.5129, 5672.2651, 7.6460>>
				CASE SMUGGLER_DROPOFF_CONTESTED_28
					RETURN <<2378.8960, 6622.1479, 1.7470>>
				CASE SMUGGLER_DROPOFF_CONTESTED_29
					RETURN <<-2596.1321, 1890.6530, 170.4910>>
				CASE SMUGGLER_DROPOFF_CONTESTED_30
					RETURN <<-509.8790, 5268.6929, 79.6030>>
			ENDSWITCH
		BREAK
		
		CASE SMV_SELL_FLY_LOW
			SWITCH eDropOff
				CASE SMUGGLER_DROPOFF_FLY_LOW_1
					RETURN <<-230.2547, -2453.7703, 5.0014>>
				CASE SMUGGLER_DROPOFF_FLY_LOW_2
					RETURN <<611.9923, -847.6424, 9.9094>>
				CASE SMUGGLER_DROPOFF_FLY_LOW_3
					RETURN <<2912.1411, 763.9382, 20.6162>>
				CASE SMUGGLER_DROPOFF_FLY_LOW_4
					RETURN <<-174.2268, 2858.1177, 31.2110>>
				CASE SMUGGLER_DROPOFF_FLY_LOW_5
					RETURN <<-1406.8820, 2630.7510, 0.0000>>
				CASE SMUGGLER_DROPOFF_FLY_LOW_6
					RETURN <<-2662.5410, 2605.0330, -0.0890>>
				CASE SMUGGLER_DROPOFF_FLY_LOW_7
					RETURN <<-1947.8680, 4582.1606, 1.5397>>
				CASE SMUGGLER_DROPOFF_FLY_LOW_8
					RETURN <<-508.3870, 4411.8662, 30.0000>>
				CASE SMUGGLER_DROPOFF_FLY_LOW_9
					RETURN <<798.3570, -2640.5359, 0.5630>>
				CASE SMUGGLER_DROPOFF_FLY_LOW_10
					RETURN <<2265.8169, 2043.2170, 127.7000>>
				CASE SMUGGLER_DROPOFF_FLY_LOW_11
					RETURN <<1666.7827, -1634.0100, 111.3332>>
				CASE SMUGGLER_DROPOFF_FLY_LOW_12
					RETURN <<-1856.1910, -332.2533, 56.0769>>
				CASE SMUGGLER_DROPOFF_FLY_LOW_13
					RETURN <<1982.5414, 711.3438, 164.0339>>
				CASE SMUGGLER_DROPOFF_FLY_LOW_14
					RETURN <<-26.2064, 657.7755, 196.5965>>
				CASE SMUGGLER_DROPOFF_FLY_LOW_15
					RETURN <<-209.8049, -1809.7168, 0.4592>>
				CASE SMUGGLER_DROPOFF_FLY_LOW_16
					RETURN <<321.1330, 2207.8049, 75.0600>>
				CASE SMUGGLER_DROPOFF_FLY_LOW_17
					RETURN <<1968.7180, 5041.6421, 40.0924>>
				CASE SMUGGLER_DROPOFF_FLY_LOW_18
					RETURN <<2032.4050, 2937.8069, 46.8413>>
				CASE SMUGGLER_DROPOFF_FLY_LOW_19
					RETURN <<81.7330, 7079.2402, 0.9710>>
				CASE SMUGGLER_DROPOFF_FLY_LOW_20
					RETURN <<3856.6345, 4367.8115, 5.5632>>
				CASE SMUGGLER_DROPOFF_FLY_LOW_21
					RETURN <<2916.9482, 5309.2437, 95.1478>>
				CASE SMUGGLER_DROPOFF_FLY_LOW_22
					RETURN <<203.8774, -732.2269, 46.0770>>
				CASE SMUGGLER_DROPOFF_FLY_LOW_23
					RETURN <<709.7780, 4109.8618, 29.8120>>
				CASE SMUGGLER_DROPOFF_FLY_LOW_24
					RETURN <<1091.6940, 62.6200, 79.8910>>
				CASE SMUGGLER_DROPOFF_FLY_LOW_25
					RETURN <<-2831.1919, 1419.7640, 99.9110>>
				CASE SMUGGLER_DROPOFF_FLY_LOW_26
					RETURN <<-1375.9344, 5349.7334, 1.9698>>
				CASE SMUGGLER_DROPOFF_FLY_LOW_27
					RETURN <<-1341.2570, -1432.9086, 3.3139>>
				CASE SMUGGLER_DROPOFF_FLY_LOW_28
					RETURN <<2468.2170, 3770.1841, 40.3540>>
				CASE SMUGGLER_DROPOFF_FLY_LOW_29
					RETURN <<-965.4330, -972.6680, 2.8350>>
				CASE SMUGGLER_DROPOFF_FLY_LOW_30
					RETURN <<631.2976, 631.4233, 127.9110>>
				CASE SMUGGLER_DROPOFF_FLY_LOW_31
					RETURN <<-1047.0857, 923.3252, 168.8197>>
				CASE SMUGGLER_DROPOFF_FLY_LOW_32
					RETURN <<-1183.3816, 45.4623, 51.9519>>
				CASE SMUGGLER_DROPOFF_FLY_LOW_33
					RETURN <<1450.9657, 6349.4390, 22.7797>>
			ENDSWITCH
		BREAK
		
		CASE SMV_SELL_PRECISION_DELIVERY
			SWITCH eDropOff
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_1
					RETURN <<1349.4340, -2211.2009, 59.1850>>
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_2
					RETURN <<244.6660, -3086.7620, 4.6340>>
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_3
					RETURN <<374.0230, -1845.6420, 26.7070>>
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_4
					RETURN <<2566.5339, -678.9420, 53.1690>>
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_5
					RETURN <<1147.3770, -1479.2490, 33.6850>>
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_6
					RETURN <<-1204.0870, -1340.3330, 3.7260>>
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_7
					RETURN <<736.9614, -328.0111, 49.2329>>
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_8
					RETURN <<1025.3444, 955.4961, 221.1720>>
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_9
					RETURN <<298.8370, 16.6260, 82.0750>>
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_10
					RETURN <<-301.3760, -1106.5800, 22.0260>>
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_11
					RETURN <<1321.4170, -725.5130, 64.5790>>
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_12
					RETURN <<2611.8259, 469.9974, 104.6810>>
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_13
					RETURN <<-808.0940, -396.2500, 36.0310>>
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_14
					RETURN <<150.6398, 758.6245, 208.3275>>
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_15
					RETURN <<-1619.5630, 391.3410, 85.6970>>

				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_16
					RETURN <<1756.9448, 6395.2021, 35.4139>>
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_17
					RETURN <<56.7549, 6521.5981, 30.4565>>
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_18
					RETURN <<-596.0260, 5753.0562, 34.0110>>
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_19
					RETURN <<-2259.7710, 4341.0762, 42.8490>>
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_20
					RETURN <<135.1260, 4447.8882, 79.1670>>
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_21
					RETURN <<1440.9668, 4470.1299, 48.4830>>
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_22
					RETURN <<1963.2120, 5083.1138, 39.6153>>
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_23
					RETURN <<2679.7600, 4341.2632, 44.7960>>
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_24
					RETURN <<2183.0061, 3399.3420, 44.5270>>
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_25
					RETURN <<1472.3240, 3658.7549, 33.2850>>
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_26
					RETURN <<1275.3380, 2629.9661, 36.7080>>
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_27
					RETURN <<170.0100, 3214.7490, 41.2990>>
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_28
					RETURN <<-1700.2831, 2371.0303, 48.2861>>
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_29
					RETURN <<-281.7410, 1958.1182, 162.3620>>
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_30
					RETURN <<2513.3630, 1969.4479, 18.8880>>
			ENDSWITCH
		BREAK
		
		CASE SMV_SELL_AIR_DELIVERY
			SWITCH eDropOff
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_1			RETURN <<-1406.8375, -997.6846, 18.3804>>
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_2			RETURN <<-60.1701, 807.2293, 225.9786>>
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_3			RETURN <<-172.9176, -158.3714, 92.7024>>
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_4			RETURN <<83.1513, -1592.0226, 37.5587>>
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_5			RETURN <<1160.1012, -423.1530, 70.9262>>
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_6			RETURN <<-299.4515, 2792.2117, 59.0160>>
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_7			RETURN <<-2230.2104, 213.9279, 193.6117>>
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_8			RETURN <<-201.3390, 6275.1978, 30.4902>>
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_9			RETURN <<492.6814, 5602.8184, 793.1485>>
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_10			RETURN <<2557.0598, 4280.3511, 43.6242>>
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_11			RETURN <<2073.5581, 1878.8500, 92.0380>>
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_12			RETURN <<1443.0680, -2110.1270, 54.8390>>
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_13			RETURN <<538.6540, -2832.8181, 5.0450>>
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_14			RETURN <<1410.9130, 3798.9709, 31.1610>>
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_15			RETURN <<-1228.5640, 4465.3682, 28.7530>>
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_16			RETURN <<1935.4659, 4980.3389, 43.0690>>
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_17			RETURN <<-428.1220, 1573.9360, 356.0900>>
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_18			RETURN <<-333.7750, -751.5120, 52.2470>>
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_19			RETURN <<2803.1951, -661.2320, 1.1770>>
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_20			RETURN <<1600.2100, 6560.3809, 12.7010>>
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_21			RETURN <<-1791.1810, 5312.3521, 1.5000>>
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_22			RETURN <<91.4460, 7553.3989, -0.5630>>
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_23			RETURN <<4368.4790, 4206.2358, 0.5620>>
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_24			RETURN <<3346.7310, 1285.3400, 0.3750>>
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_25			RETURN <<43.9570, -3663.7261, 0.5630>>
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_26			RETURN <<-3483.8589, 1513.5341, 0.9370>>
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_27			RETURN <<3142.5139, 6772.5762, 0.3750>>
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_28			RETURN <<3651.1550, -1497.2480, 0.9100>>
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_29			RETURN <<-2238.2119, -1635.3380, 0.9380>>
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_30			RETURN <<2587.3350, -2492.8640, 1.5000>>
			ENDSWITCH
		BREAK
		
		CASE SMV_SELL_AIR_POLICE
			SWITCH eDropOff
				CASE SMUGGLER_DROPOFF_AIR_POLICE_1				RETURN <<-1136.5844, 4897.4565, 218.2406>>
				CASE SMUGGLER_DROPOFF_AIR_POLICE_2 				RETURN <<-1271.8770, 4413.1362, 10.3100>>
				CASE SMUGGLER_DROPOFF_AIR_POLICE_3				RETURN <<2468.9663, -387.9986, 108.5770>>
				CASE SMUGGLER_DROPOFF_AIR_POLICE_4				RETURN <<3705.6272, 3799.9370, 10.9323>>
				CASE SMUGGLER_DROPOFF_AIR_POLICE_5				RETURN <<488.9060, 5588.8584, 793.1848>>
				CASE SMUGGLER_DROPOFF_AIR_POLICE_6				RETURN <<-331.1526, -1970.4769, 65.5680>>
				CASE SMUGGLER_DROPOFF_AIR_POLICE_7				RETURN <<1347.7137, 4349.9097, 42.5016>>
				CASE SMUGGLER_DROPOFF_AIR_POLICE_8				RETURN <<-197.1085, -171.9029, 84.2247>>
				CASE SMUGGLER_DROPOFF_AIR_POLICE_9 				RETURN <<2342.6169, -2172.4021, 0.1870>>
				CASE SMUGGLER_DROPOFF_AIR_POLICE_10				RETURN <<-915.7610, 6159.6631, 3.7790>>
				CASE SMUGGLER_DROPOFF_AIR_POLICE_11				RETURN <<660.3950, -731.2900, 23.1492>>
				CASE SMUGGLER_DROPOFF_AIR_POLICE_12				RETURN <<-3247.7070, 781.1210, 0.1880>>
				CASE SMUGGLER_DROPOFF_AIR_POLICE_13				RETURN <<3742.2600, 1525.9960, 0.3750>>
				CASE SMUGGLER_DROPOFF_AIR_POLICE_14				RETURN <<4360.6689, 5409.2139, -0.7500>>
				CASE SMUGGLER_DROPOFF_AIR_POLICE_15				RETURN <<1489.0510, 7359.2041, -0.5620>>
				CASE SMUGGLER_DROPOFF_AIR_POLICE_16				RETURN <<1117.6010, -3883.0750, 0.9380>>
				CASE SMUGGLER_DROPOFF_AIR_POLICE_17				RETURN <<-2810.3130, -1331.4720, -0.3750>>
				CASE SMUGGLER_DROPOFF_AIR_POLICE_18				RETURN <<-3747.0000, 2432.5930, -0.1870>>
				CASE SMUGGLER_DROPOFF_AIR_POLICE_19				RETURN <<2831.6282, 5965.1299, 350.5634>>
				CASE SMUGGLER_DROPOFF_AIR_POLICE_20				RETURN <<-1212.9952, 502.7975, 102.8198>>
				CASE SMUGGLER_DROPOFF_AIR_POLICE_21				RETURN <<1449.1532, 1118.6444, 113.3340>>
				CASE SMUGGLER_DROPOFF_AIR_POLICE_22				RETURN <<2468.7212, 3432.0540, 49.0871>>
				CASE SMUGGLER_DROPOFF_AIR_POLICE_23				RETURN <<-64.8453, 1907.6471, 194.9479>>
				CASE SMUGGLER_DROPOFF_AIR_POLICE_24				RETURN <<-1755.6960, 2002.0800, 116.0470>>
				CASE SMUGGLER_DROPOFF_AIR_POLICE_25				RETURN <<2476.2546, 4940.1162, 43.8339>>
				CASE SMUGGLER_DROPOFF_AIR_POLICE_26				RETURN <<286.9490, 6491.8359, 28.9600>>
				CASE SMUGGLER_DROPOFF_AIR_POLICE_27				RETURN <<1175.9698, -2274.7993, 29.4231>>
				CASE SMUGGLER_DROPOFF_AIR_POLICE_28				RETURN <<710.8422, 3129.5354, 42.8720>>
				CASE SMUGGLER_DROPOFF_AIR_POLICE_29				RETURN <<2202.7170, 1823.7540, 106.3350>>
				CASE SMUGGLER_DROPOFF_AIR_POLICE_30				RETURN <<1082.0820, -5.1660, 79.7640>>
			ENDSWITCH
		BREAK
		
		CASE SMV_INFILTRATION
			SWITCH eLocation
				CASE SmugglerLocation_Infiltration_HumaneLabs
					SWITCH iVehicle
						CASE 0			RETURN <<3557.5320, 3668.0330, 32.8890>>
						CASE 1			RETURN <<3576.5911, 3656.3601, 32.8990>>
						CASE 2			RETURN <<3573.4509, 3667.0017, 32.8886>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Infiltration_BolingbrokePrison
					SWITCH iVehicle
						CASE 0			RETURN <<1804.9080, 2611.8140, 44.6220>>
						CASE 1			RETURN <<1706.6010, 2610.1021, 44.5650>>
						CASE 2			RETURN <<1736.9670, 2604.4509, 44.5650>>
						CASE 3			RETURN <<1654.3361, 2604.4839, 44.5650>>
						CASE 4			RETURN <<1673.2805, 2607.9761, 44.5649>>
						CASE 5			RETURN <<1673.4609, 2601.0017, 44.5649>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Infiltration_Zancudo
					SWITCH iVehicle
						CASE 0			RETURN <<-1945.0726, 3361.6580, 31.8569>>
						CASE 1			RETURN <<-1945.9630, 3317.1421, 31.9600>>
						CASE 2			RETURN <<-1951.6100, 3335.5020, 31.9600>>
						CASE 3			RETURN <<-1955.7740, 3324.8369, 31.9600>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_ESCORT
			SWITCH eLocation
				CASE SmugglerLocation_Escort_Placeholder1
					SWITCH iVehicle
						CASE 0			RETURN <<2162.0940, -4399.9351, 700.00>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Escort_Placeholder2
					SWITCH iVehicle
						CASE 0			RETURN <<-5746.7671, -771.9508, 700.00>>	
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Escort_Placeholder3
					SWITCH iVehicle
						CASE 0			RETURN <<-4836.3115, 8009.9873, 700.00>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_SETUP_RON
			SWITCH eLocation
				CASE SmugglerLocation_Setup_DelPerroBeach 
					SWITCH iVehicle
						CASE 0 RETURN <<-1783.8888, -854.4708, 6.8299>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_LSRiver
					SWITCH iVehicle
						CASE 0 RETURN <<616.0158, -536.3337, 14.2045>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_ElBurro
					SWITCH iVehicle
						CASE 0 RETURN <<1738.9270, -1558.6775, 111.6570>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_Chumash
					SWITCH iVehicle
						CASE 0 RETURN <<-3289.2593, 986.4471, 2.5968>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_Tutuavium
					SWITCH iVehicle
						CASE 0 RETURN <<2529.6299, 1026.5852, 76.1964>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_RonsWindFarm
					SWITCH iVehicle
						CASE 0 RETURN <<2428.6138, 1469.3000, 37.8038>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_SenoraDesert1
					SWITCH iVehicle
						CASE 0 RETURN <<1357.5342, 1408.5576, 102.2637>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_SenoraDesert2
					SWITCH iVehicle
						CASE 0 RETURN <<308.9504, 2425.6035, 46.4931>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_SandyShores
					SWITCH iVehicle
						CASE 0 RETURN <<1198.3041, 3074.8479, 39.9078>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_StabCity
					SWITCH iVehicle
						CASE 0 RETURN <<98.7975, 3660.0198, 38.7550>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_NCalafiaWay
					SWITCH iVehicle
						CASE 0 RETURN <<1010.7133, 4354.9756, 42.2786>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_McKenzie
					SWITCH iVehicle
						CASE 0 RETURN <<2031.4851, 4787.4937, 40.7912>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_Grapeseed
					SWITCH iVehicle
						CASE 0 RETURN <<2649.1694, 4236.3599, 43.3915>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_ProcopioBeach
					SWITCH iVehicle
						CASE 0 RETURN <<769.3394, 6585.5645, 1.3929>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_Paleto
					SWITCH iVehicle
						CASE 0 RETURN <<-339.9335, 6457.4297, 1.0282>>
					ENDSWITCH
				BREAK

			ENDSWITCH
		BREAK
		
	ENDSWITCH

	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC FLOAT GET_NON_SMUGGLER_VEHICLE_SPAWN_HEADING(SMUGGLER_VARIATION eVariation, SMUGGLER_LOCATION eLocation, INT iVehicle, FREEMODE_DELIVERY_DROPOFFS eDropOff = FREEMODE_DELIVERY_DROPOFF_INVALID, INT iRandomSelection = -1, INT iRespawnLocation = 0)

	SWITCH eVariation
		CASE SMV_STEAL_AIRCRAFT			
			SWITCH eLocation
				CASE SmugglerLocation_StealAircraft_SandyShores
					SWITCH iVehicle
						CASE 0			RETURN 331.3970
						CASE 1			RETURN 177.8002
						CASE 2			RETURN 220.4002
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_StealAircraft_McKenzieAirfield
					SWITCH iVehicle
						CASE 0			RETURN 58.1970
						CASE 1			RETURN 16.9998
						CASE 2			RETURN 49.7998
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_StealAircraft_EpsilonHQ
					SWITCH iVehicle
						CASE 0			RETURN 205.8001
						CASE 1			RETURN 6.1990
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE SMV_BEACON_GRAB					
			SWITCH iRandomSelection
				CASE 0
					SWITCH iVehicle
						CASE 0			RETURN 0.0
						CASE 1			RETURN 0.0
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH iVehicle
						CASE 0			RETURN 0.0
						CASE 1			RETURN 0.0
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iVehicle
						CASE 0			RETURN 0.0
						CASE 1			RETURN 0.0
					ENDSWITCH
				BREAK
				CASE 3
					SWITCH iVehicle
						CASE 0			RETURN 0.0
						CASE 1			RETURN 0.0
					ENDSWITCH
				BREAK
				CASE 4
					SWITCH iVehicle
						CASE 0			RETURN 0.0
						CASE 1			RETURN 0.0
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_SPLASH_LANDING
			SWITCH eLocation
				CASE SmugglerLocation_SplashLanding_LagoZancudo
					SWITCH iVehicle
						CASE 0			RETURN 135.0990
						CASE 1			RETURN -5.5010
						CASE 2			RETURN 10.6990
						CASE 3			RETURN 258.4989
						CASE 4			RETURN 211.0987
						CASE 5			RETURN 98.8988
						CASE 6			RETURN 306.3990
						CASE 7			RETURN 126.1985
						CASE 8			RETURN 313.5976
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_SplashLanding_PacificOceanE
					SWITCH iVehicle
						CASE 0			RETURN 362.9990
						CASE 1			RETURN 160.5990
						CASE 2			RETURN 325.5988
						CASE 3			RETURN 252.3984
						CASE 4			RETURN 219.3983
						CASE 5			RETURN 306.3982
						CASE 6			RETURN 12.6000
						CASE 7			RETURN 193.0000
						CASE 8			RETURN 197.1989
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_SplashLanding_PacificOceanNE
					SWITCH iVehicle
						CASE 0			RETURN 104.9990
						CASE 1			RETURN 277.1990
						CASE 2			RETURN 212.2000
						CASE 3			RETURN 317.2001
						CASE 4			RETURN 246.9999
						CASE 5			RETURN 34.9991
						CASE 6			RETURN 279.5998
						CASE 7			RETURN 99.7996
						CASE 8			RETURN 280.3986
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_BOMB_BASE
			SWITCH eLocation
				CASE SmugglerLocation_BombBase_Terminal
					SWITCH iVehicle
						CASE 0			RETURN 105.7990
						CASE 1			RETURN 268.7970
						CASE 2			RETURN 268.9970
						CASE 3			RETURN 268.9970
						CASE 4			RETURN 285.9970
						CASE 5			RETURN 225.9970
						CASE 6			RETURN 201.7970
						CASE 7			RETURN 271.7960
						CASE 8			RETURN 180.7958
						CASE 9			RETURN 90.3960
						CASE 10			RETURN 218.7957
						CASE 11			RETURN 207.7960
						CASE 12			RETURN 118.3960
						CASE 13			RETURN 339.7958
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_BombBase_GrapeseedAutoyard
					SWITCH iVehicle
						CASE 0			RETURN 326.5990
						CASE 1			RETURN 326.5990
						CASE 2			RETURN 43.5990
						CASE 3			RETURN 43.5990
						CASE 4			RETURN 225.9990
						CASE 5			RETURN 266.1990
						CASE 6			RETURN 129.1980
						CASE 7			RETURN 228.9980
						CASE 8			RETURN 149.5990
						CASE 9			RETURN 243.3980
						CASE 10			RETURN 301.1980
						CASE 11			RETURN 68.9980
						CASE 12			RETURN 329.1977
						CASE 13			RETURN 255.3974
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_BombBase_PlaneScrapyard
					SWITCH iVehicle
						CASE 0			RETURN 295.5980
						CASE 1			RETURN 6.9980
						CASE 2			RETURN 155.9970
						CASE 3			RETURN 155.9970
						CASE 4			RETURN 252.1960
						CASE 5			RETURN 209.9960
						CASE 6			RETURN 332.7970
						CASE 7			RETURN 211.5970
						CASE 8			RETURN 110.7970
						CASE 9			RETURN 2.5960
						CASE 10			RETURN 245.5960
						CASE 11			RETURN 264.7957
						CASE 12			RETURN 285.7957
						CASE 13			RETURN 51.7955
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_THERMAL_SCOPE
			SWITCH eLocation
				CASE SmugglerLocation_ThermalScope_Quarry
					SWITCH iVehicle
						CASE 0			RETURN 340.7960
						CASE 1			RETURN 87.7950
						CASE 2			RETURN 179.5950
						CASE 3			RETURN 202.5450
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_ThermalScope_PowerStation
					SWITCH iVehicle
						CASE 0			RETURN 294.5950
						CASE 1			RETURN 321.9950
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_ThermalScope_PlayboyMansion
					SWITCH iVehicle
						CASE 0			RETURN 90.3980
						CASE 1			RETURN 246.5980
						CASE 2			RETURN 29.1980
						CASE 3			RETURN 316.7950
						CASE 4			RETURN 277.7950
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_CARGO_PLANE
			SWITCH iVehicle
				CASE 0
				CASE 1
				CASE 2
					RETURN GET_SMV_CARGO_PLANE_VEHICLE_SPAWN_HEADING(iVehicle, eLocation)
				
			ENDSWITCH
			
			RETURN GET_SMV_CARGO_PLANE_SUPPORT_JET_SPAWN_HEADING(iVehicle, eLocation)
		BREAK
		
		CASE SMV_ROOF_ATTACK
			SWITCH eLocation
				CASE SmugglerLocation_RoofAttack_ConstructionSite
					SWITCH iVehicle
						CASE 0			RETURN 209.3968
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_BOMBING_RUN
			SWITCH eLocation
				CASE SmugglerLocation_BombingRun_Placeholder1
					SWITCH iVehicle
						CASE 0			RETURN 305.3998
						CASE 1			RETURN 176.1992
						CASE 2			RETURN 176.1992
						CASE 3			RETURN 208.5994
						CASE 4			RETURN 136.9994
						CASE 5			RETURN 115.7992
						CASE 6			RETURN 214.5991
						CASE 7			RETURN 38.7990
						CASE 8			RETURN 65.9990
						CASE 9			RETURN 90.7484
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_BombingRun_Placeholder2
					SWITCH iVehicle
						CASE 0			RETURN 353.4967
						CASE 1			RETURN 143.4953
						CASE 2			RETURN 211.4949
						CASE 3			RETURN 225.4949
						CASE 4			RETURN 267.4948
						CASE 5			RETURN 162.6947
						CASE 6			RETURN 81.6943
						CASE 7			RETURN 91.2942
						CASE 8			RETURN 128.6939
						CASE 9			RETURN 185.0938
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_BombingRun_Placeholder3
					SWITCH iVehicle
						CASE 0			RETURN 55.4935
						CASE 1			RETURN 340.2933
						CASE 2			RETURN 136.0929
						CASE 3			RETURN 180.8930
						CASE 4			RETURN 358.8927
						CASE 5			RETURN 48.8925
						CASE 6			RETURN -0.3075
						CASE 7			RETURN 288.4923
						CASE 8			RETURN 313.2919
						CASE 9			RETURN 330.8920
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_BOMB_ROOF
			SWITCH eLocation
				CASE SmugglerLocation_BombRoof_Route1
					SWITCH iRespawnLocation
						CASE 0
							SWITCH iVehicle
								CASE 0			RETURN 172.3850
								CASE 1			RETURN 327.9850
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH iVehicle
								CASE 0			RETURN 331.9980
								CASE 1			RETURN 216.9980
							ENDSWITCH
						BREAK
						CASE 2
							SWITCH iVehicle
								CASE 0			RETURN 344.7940
								CASE 1			RETURN 85.1940
							ENDSWITCH
						BREAK
						CASE 3
							SWITCH iVehicle
								CASE 0			RETURN 220.5880
								CASE 1			RETURN 111.9870
							ENDSWITCH
						BREAK
						CASE 4
							SWITCH iVehicle
								CASE 0			RETURN 34.9830
								CASE 1			RETURN 108.5820
							ENDSWITCH
						BREAK
					ENDSWITCH
					SWITCH iVehicle
						CASE 2			RETURN 281.9994
						CASE 3			RETURN 14.9992
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_BombRoof_Route2
					SWITCH iRespawnLocation
						CASE 0
							SWITCH iVehicle
								CASE 0			RETURN 251.4000
								CASE 1			RETURN 70.3990
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH iVehicle
								CASE 0			RETURN 248.1970
								CASE 1			RETURN 11.9970
							ENDSWITCH
						BREAK
						CASE 2
							SWITCH iVehicle
								CASE 0			RETURN 233.9910
								CASE 1			RETURN 322.7920
							ENDSWITCH
						BREAK
						CASE 3
							SWITCH iVehicle
								CASE 0			RETURN 71.5980
								CASE 1			RETURN 204.7970
							ENDSWITCH
						BREAK
						CASE 4
							SWITCH iVehicle
								CASE 0			RETURN 118.7940
								CASE 1			RETURN 221.3930
							ENDSWITCH
						BREAK
					ENDSWITCH
					SWITCH iVehicle
						CASE 2			RETURN 318.2000
						CASE 3			RETURN 242.5998
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_BombRoof_Route3
					SWITCH iRespawnLocation
						CASE 0
							SWITCH iVehicle
								CASE 0			RETURN 198.6000
								CASE 1			RETURN 81.1990
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH iVehicle
								CASE 0			RETURN 71.1930
								CASE 1			RETURN 159.1930
							ENDSWITCH
						BREAK
						CASE 2
							SWITCH iVehicle
								CASE 0			RETURN 174.5970
								CASE 1			RETURN 33.9960
							ENDSWITCH
						BREAK
						CASE 3
							SWITCH iVehicle
								CASE 0			RETURN 272.7920
								CASE 1			RETURN 139.5920
							ENDSWITCH
						BREAK
						CASE 4
							SWITCH iVehicle
								CASE 0			RETURN 34.2000
								CASE 1			RETURN 160.7980
							ENDSWITCH
						BREAK
					ENDSWITCH
					SWITCH iVehicle
						CASE 2			RETURN 199.9988
						CASE 3			RETURN 337.3987
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_STUNT_PILOT
			SWITCH eLocation
				CASE SmugglerLocation_StuntPilot_Location1
					SWITCH iVehicle
						CASE 0			RETURN 287.5984
						CASE 1			RETURN 38.7981
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_StuntPilot_Location2
					SWITCH iVehicle
						CASE 0			RETURN 155.5974
						CASE 1			RETURN 171.1973
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_StuntPilot_Location3
					SWITCH iVehicle
						CASE 0			RETURN 117.1967
						CASE 1			RETURN 115.3968
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_StuntPilot_Location4
					SWITCH iVehicle
						CASE 0			RETURN 48.7958
						CASE 1			RETURN 238.1951
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_StuntPilot_Location5
					SWITCH iVehicle
						CASE 0			RETURN 253.5953
						CASE 1			RETURN 240.3952
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_StuntPilot_Location6
					SWITCH iVehicle
						CASE 0			RETURN 183.3948
						CASE 1			RETURN 175.1951
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_StuntPilot_Location7
					SWITCH iVehicle
						CASE 0			RETURN 354.1935
						CASE 1			RETURN 354.5936
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_StuntPilot_Location8
					SWITCH iVehicle
						CASE 0			RETURN 178.9931
						CASE 1			RETURN 184.5931
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_StuntPilot_Location9
					SWITCH iVehicle
						CASE 0			RETURN 355.5930
						CASE 1			RETURN 356.3931
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_StuntPilot_Location10
					SWITCH iVehicle
						CASE 0			RETURN 325.3924
						CASE 1			RETURN 176.5919
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_SELL_CONTESTED
			SWITCH eDropOff
				CASE SMUGGLER_DROPOFF_CONTESTED_1
					RETURN 15.2000
				CASE SMUGGLER_DROPOFF_CONTESTED_2
					RETURN 112.9969
				CASE SMUGGLER_DROPOFF_CONTESTED_3
					RETURN 161.9940
				CASE SMUGGLER_DROPOFF_CONTESTED_4
					RETURN 61.1920
				CASE SMUGGLER_DROPOFF_CONTESTED_5
					RETURN 33.9890
				CASE SMUGGLER_DROPOFF_CONTESTED_6
					RETURN 315.8000
				CASE SMUGGLER_DROPOFF_CONTESTED_7
					RETURN 272.9950
				CASE SMUGGLER_DROPOFF_CONTESTED_8
					RETURN 83.7920
				CASE SMUGGLER_DROPOFF_CONTESTED_9
					RETURN 84.9890
				CASE SMUGGLER_DROPOFF_CONTESTED_10
					RETURN 193.1869
				CASE SMUGGLER_DROPOFF_CONTESTED_11
					RETURN 211.8000
				CASE SMUGGLER_DROPOFF_CONTESTED_12
					RETURN 230.0000
				CASE SMUGGLER_DROPOFF_CONTESTED_13
					RETURN 46.2000
				CASE SMUGGLER_DROPOFF_CONTESTED_14
					RETURN 23.8000
				CASE SMUGGLER_DROPOFF_CONTESTED_15
					RETURN 123.1999
				CASE SMUGGLER_DROPOFF_CONTESTED_16
					RETURN 80.4000
				CASE SMUGGLER_DROPOFF_CONTESTED_17
					RETURN 197.5990
				CASE SMUGGLER_DROPOFF_CONTESTED_18
					RETURN 31.7980
				CASE SMUGGLER_DROPOFF_CONTESTED_19
					RETURN 73.3970
				CASE SMUGGLER_DROPOFF_CONTESTED_20
					RETURN 135.5940
				CASE SMUGGLER_DROPOFF_CONTESTED_21
					RETURN 257.2000
				CASE SMUGGLER_DROPOFF_CONTESTED_22
					RETURN 351.1999
				CASE SMUGGLER_DROPOFF_CONTESTED_23
					RETURN 324.5988
				CASE SMUGGLER_DROPOFF_CONTESTED_24
					RETURN 318.6000
				CASE SMUGGLER_DROPOFF_CONTESTED_25
					RETURN 83.8000
				CASE SMUGGLER_DROPOFF_CONTESTED_26
					RETURN 328.4000
				CASE SMUGGLER_DROPOFF_CONTESTED_27
					RETURN 21.0000
				CASE SMUGGLER_DROPOFF_CONTESTED_28
					RETURN 111.8000
				CASE SMUGGLER_DROPOFF_CONTESTED_29
					RETURN 159.3990
				CASE SMUGGLER_DROPOFF_CONTESTED_30
					RETURN 130.2000
			ENDSWITCH
		BREAK
		
		CASE SMV_SELL_FLY_LOW
			SWITCH eDropOff
				CASE SMUGGLER_DROPOFF_FLY_LOW_1
					RETURN 89.9919
				CASE SMUGGLER_DROPOFF_FLY_LOW_2
					RETURN 23.1910
				CASE SMUGGLER_DROPOFF_FLY_LOW_3
					RETURN 353.2000
				CASE SMUGGLER_DROPOFF_FLY_LOW_4
					RETURN 350.3900
				CASE SMUGGLER_DROPOFF_FLY_LOW_5
					RETURN 111.2000
				CASE SMUGGLER_DROPOFF_FLY_LOW_6
					RETURN 111.2000
				CASE SMUGGLER_DROPOFF_FLY_LOW_7
					RETURN 21.5998
				CASE SMUGGLER_DROPOFF_FLY_LOW_8
					RETURN 72.0000
				CASE SMUGGLER_DROPOFF_FLY_LOW_9
					RETURN 258.4000
				CASE SMUGGLER_DROPOFF_FLY_LOW_10
					RETURN 144.9990
				CASE SMUGGLER_DROPOFF_FLY_LOW_11
					RETURN 172.3989
				CASE SMUGGLER_DROPOFF_FLY_LOW_12
					RETURN 308.7478
				CASE SMUGGLER_DROPOFF_FLY_LOW_13
					RETURN 133.9999
				CASE SMUGGLER_DROPOFF_FLY_LOW_14
					RETURN 329.4000
				CASE SMUGGLER_DROPOFF_FLY_LOW_15
					RETURN 358.8000
				CASE SMUGGLER_DROPOFF_FLY_LOW_16
					RETURN 285.3990
				CASE SMUGGLER_DROPOFF_FLY_LOW_17
					RETURN 73.3988
				CASE SMUGGLER_DROPOFF_FLY_LOW_18
					RETURN 163.7990
				CASE SMUGGLER_DROPOFF_FLY_LOW_19
					RETURN 48.5990
				CASE SMUGGLER_DROPOFF_FLY_LOW_20
					RETURN 245.7974
				CASE SMUGGLER_DROPOFF_FLY_LOW_21
					RETURN 273.3979
				CASE SMUGGLER_DROPOFF_FLY_LOW_22
					RETURN 233.1976
				CASE SMUGGLER_DROPOFF_FLY_LOW_23
					RETURN 338.3950
				CASE SMUGGLER_DROPOFF_FLY_LOW_24
					RETURN 177.1950
				CASE SMUGGLER_DROPOFF_FLY_LOW_25
					RETURN 15.7940
				CASE SMUGGLER_DROPOFF_FLY_LOW_26
					RETURN 102.3938
				CASE SMUGGLER_DROPOFF_FLY_LOW_27
					RETURN 176.7928
				CASE SMUGGLER_DROPOFF_FLY_LOW_28
					RETURN 270.7930
				CASE SMUGGLER_DROPOFF_FLY_LOW_29
					RETURN 94.3930
				CASE SMUGGLER_DROPOFF_FLY_LOW_30
					RETURN 229.9999
				CASE SMUGGLER_DROPOFF_FLY_LOW_31
					RETURN 223.1995
				CASE SMUGGLER_DROPOFF_FLY_LOW_32
					RETURN 197.5988
				CASE SMUGGLER_DROPOFF_FLY_LOW_33
					RETURN 73.1999
			ENDSWITCH
		BREAK
		
		CASE SMV_SELL_PRECISION_DELIVERY
			SWITCH eDropOff
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_1
					RETURN 12.5990
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_2
					RETURN 44.3990
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_3
					RETURN 315.3990
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_4
					RETURN 114.5980
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_5
					RETURN 358.9980
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_6
					RETURN 204.5980
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_7
					RETURN 232.1974
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_8
					RETURN 192.1991
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_9
					RETURN 250.7990
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_10
					RETURN 249.9980
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_11
					RETURN 71.3980
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_12
					RETURN 1.9980
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_13
					RETURN 50.1980
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_14
					RETURN 338.1979
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_15
					RETURN 231.6000

				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_16
					RETURN 99.7967
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_17
					RETURN 44.3960
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_18
					RETURN 179.3970
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_19
					RETURN 191.3980
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_20
					RETURN 229.9970
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_21
					RETURN 335.3970
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_22
					RETURN 312.5970
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_23
					RETURN 41.3960
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_24
					RETURN 20.3960
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_25
					RETURN 108.7980
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_26
					RETURN 210.1960
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_27
					RETURN 345.3960
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_28
					RETURN 57.9942
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_29
					RETURN 11.1959
				CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_30
					RETURN 223.7961
			ENDSWITCH
		BREAK
		
		CASE SMV_SELL_AIR_DELIVERY
			SWITCH eDropOff
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_1			RETURN 101.5997
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_2			RETURN 122.5996
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_3			RETURN 184.5995
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_4			RETURN 3.3997
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_5			RETURN 102.9991
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_6			RETURN 11.7990
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_7			RETURN 360.1984
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_8			RETURN 78.9982
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_9			RETURN 206.7983
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_10			RETURN 300.9982
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_11			RETURN 285.2000
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_12			RETURN 344.5990
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_13			RETURN 188.4000
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_14			RETURN 164.3990
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_15			RETURN 13.5990
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_16			RETURN 148.4000
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_17			RETURN 76.5990
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_18			RETURN 202.3990
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_19			RETURN 304.2000
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_20			RETURN 189.2000
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_21			RETURN 329.8000
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_22			RETURN 137.0000
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_23			RETURN 234.1990
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_24			RETURN 315.3990
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_25			RETURN 315.3990
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_26			RETURN 127.3990
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_27			RETURN 267.1990
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_28			RETURN 109.9980
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_29			RETURN 336.7980
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_30			RETURN 174.6000
			ENDSWITCH
		BREAK
		
		CASE SMV_SELL_AIR_POLICE
			SWITCH eDropOff
				CASE SMUGGLER_DROPOFF_AIR_POLICE_1				RETURN 284.1998
				CASE SMUGGLER_DROPOFF_AIR_POLICE_2 				RETURN 341.2000
				CASE SMUGGLER_DROPOFF_AIR_POLICE_3				RETURN 43.1993
				CASE SMUGGLER_DROPOFF_AIR_POLICE_4				RETURN 162.1990
				CASE SMUGGLER_DROPOFF_AIR_POLICE_5				RETURN 295.1985
				CASE SMUGGLER_DROPOFF_AIR_POLICE_6				RETURN 265.7982
				CASE SMUGGLER_DROPOFF_AIR_POLICE_7				RETURN 306.1980
				CASE SMUGGLER_DROPOFF_AIR_POLICE_8				RETURN 196.8000
				CASE SMUGGLER_DROPOFF_AIR_POLICE_9 				RETURN 259.1960
				CASE SMUGGLER_DROPOFF_AIR_POLICE_10				RETURN 279.2000
				CASE SMUGGLER_DROPOFF_AIR_POLICE_11				RETURN 229.9992
				CASE SMUGGLER_DROPOFF_AIR_POLICE_12				RETURN 160.2000
				CASE SMUGGLER_DROPOFF_AIR_POLICE_13				RETURN 318.2000
				CASE SMUGGLER_DROPOFF_AIR_POLICE_14				RETURN 231.2000
				CASE SMUGGLER_DROPOFF_AIR_POLICE_15				RETURN 51.7990
				CASE SMUGGLER_DROPOFF_AIR_POLICE_16				RETURN 234.0000
				CASE SMUGGLER_DROPOFF_AIR_POLICE_17				RETURN 33.2000
				CASE SMUGGLER_DROPOFF_AIR_POLICE_18				RETURN 33.2000
				CASE SMUGGLER_DROPOFF_AIR_POLICE_19				RETURN 71.7988
				CASE SMUGGLER_DROPOFF_AIR_POLICE_20				RETURN 233.7986
				CASE SMUGGLER_DROPOFF_AIR_POLICE_21				RETURN 233.7986
				CASE SMUGGLER_DROPOFF_AIR_POLICE_22				RETURN 212.7985
				CASE SMUGGLER_DROPOFF_AIR_POLICE_23				RETURN 324.9983
				CASE SMUGGLER_DROPOFF_AIR_POLICE_24				RETURN 44.5980
				CASE SMUGGLER_DROPOFF_AIR_POLICE_25				RETURN 359.9981
				CASE SMUGGLER_DROPOFF_AIR_POLICE_26				RETURN 93.9970
				CASE SMUGGLER_DROPOFF_AIR_POLICE_27				RETURN 321.9981
				CASE SMUGGLER_DROPOFF_AIR_POLICE_28				RETURN 51.7977
				CASE SMUGGLER_DROPOFF_AIR_POLICE_29				RETURN 134.9960
				CASE SMUGGLER_DROPOFF_AIR_POLICE_30				RETURN 161.3960
			ENDSWITCH
		BREAK
		
		CASE SMV_INFILTRATION
			SWITCH eLocation
				CASE SmugglerLocation_Infiltration_HumaneLabs
					SWITCH iVehicle
						CASE 0			RETURN 104.5970
						CASE 1			RETURN 47.6000
						CASE 2			RETURN 103.7997
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Infiltration_BolingbrokePrison
					SWITCH iVehicle
						CASE 0			RETURN 43.9980
						CASE 1			RETURN 268.9980
						CASE 2			RETURN 90.1990
						CASE 3			RETURN 89.7980
						CASE 4			RETURN 90.7998
						CASE 5			RETURN 286.7996
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Infiltration_Zancudo
					SWITCH iVehicle
						CASE 0			RETURN 157.5989
						CASE 1			RETURN 328.7490
						CASE 2			RETURN 69.1490
						CASE 3			RETURN 259.7480
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_ESCORT
			SWITCH eLocation
				CASE SmugglerLocation_Escort_Placeholder1
					SWITCH iVehicle
						CASE 0			RETURN 0.0000	
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Escort_Placeholder2
					SWITCH iVehicle
						CASE 0			RETURN 300.3990	
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Escort_Placeholder3
					SWITCH iVehicle
						CASE 0			RETURN 228.7984
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_SETUP_RON
			SWITCH eLocation
				CASE SmugglerLocation_Setup_DelPerroBeach 
					SWITCH iVehicle
						CASE 0 RETURN 234.5997
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_LSRiver
					SWITCH iVehicle
						CASE 0 RETURN 347.7990
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_ElBurro
					SWITCH iVehicle
						CASE 0 RETURN 140.5976
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_Chumash
					SWITCH iVehicle
						CASE 0 RETURN 145.5968
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_Tutuavium
					SWITCH iVehicle
						CASE 0 RETURN 352.9966
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_RonsWindFarm
					SWITCH iVehicle
						CASE 0 RETURN 39.3946
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_SenoraDesert1
					SWITCH iVehicle
						CASE 0 RETURN 118.7941
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_SenoraDesert2
					SWITCH iVehicle
						CASE 0 RETURN 124.9933
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_SandyShores
					SWITCH iVehicle
						CASE 0 RETURN 248.8422
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_StabCity
					SWITCH iVehicle
						CASE 0 RETURN 77.7349
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_NCalafiaWay
					SWITCH iVehicle
						CASE 0 RETURN 239.7339
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_McKenzie
					SWITCH iVehicle
						CASE 0 RETURN 267.9337
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_Grapeseed
					SWITCH iVehicle
						CASE 0 RETURN 128.9334
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_ProcopioBeach
					SWITCH iVehicle
						CASE 0 RETURN 276.5425
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_Paleto
					SWITCH iVehicle
						CASE 0 RETURN 130.3411
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK

	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

////////////////////////////////////////
/// SMUGGLER NON-MISSION VEHICLE MODEL ///
////////////////////////////////////////
  
FUNC MODEL_NAMES GET_NON_SMUGGLER_VEHICLE_MODEL(SMUGGLER_VARIATION eVariation, SMUGGLER_SUBVARIATION eSubvariation1, INT iVehicle, FREEMODE_DELIVERY_DROPOFFS eDropOff = FREEMODE_DELIVERY_DROPOFF_INVALID)
	BOOL bBikerGang = GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
	UNUSED_PARAMETER(bBikerGang)
	
	SWITCH eVariation
		// Steal
		CASE SMV_STEAL_AIRCRAFT
			SWITCH eSubvariation1
				CASE SMS_SA_SANDY_SHORES
					SWITCH iVehicle
						CASE 0			RETURN BUZZARD
						CASE 1			RETURN PYRO
						CASE 2			RETURN PYRO
						CASE 3			RETURN LAZER	// Ambush
						CASE 4			RETURN LAZER	// Ambush
					ENDSWITCH
				BREAK
				CASE SMS_SA_MCKENZIE_AIRFIELD
					SWITCH iVehicle
						CASE 0			RETURN FROGGER
						CASE 1			RETURN rogue
						CASE 2			RETURN rogue
						CASE 3			RETURN rogue	// Ambush
						CASE 4			RETURN rogue	// Ambush
					ENDSWITCH
				BREAK
				CASE SMS_SA_EPSILON_HQ
					SWITCH iVehicle
						CASE 0			RETURN BUZZARD2
						CASE 1			RETURN BUZZARD2
						CASE 2			RETURN BUZZARD2	// Ambush
						CASE 3			RETURN BUZZARD2	// Ambush
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_SPLASH_LANDING
			SWITCH iVehicle
				CASE 0			RETURN DINGHY2
				CASE 1			RETURN DINGHY2
				CASE 2			RETURN tug
				CASE 3			RETURN tug
				CASE 4			RETURN seashark
				CASE 5			RETURN seashark
				CASE 6			RETURN SUBMERSIBLE2
				CASE 7			RETURN SUBMERSIBLE2
				CASE 8			RETURN CARGOBOB2
				CASE 9			RETURN TULA
				CASE 10			RETURN TULA
				CASE 11			RETURN TULA
				CASE 12			RETURN TULA
				CASE 13			RETURN FROGGER
				CASE 14			RETURN FROGGER
			ENDSWITCH
		BREAK
		
		CASE SMV_BLACK_BOX
			SWITCH iVehicle
				CASE 0			RETURN MAVERICK		// Ambush
				CASE 1			RETURN MAVERICK		// Ambush
				CASE 2			RETURN BUZZARD2		// Support
				CASE 3			RETURN BUZZARD2		// Support
			ENDSWITCH
		BREAK
		
		CASE SMV_THERMAL_SCOPE 
		
			SWITCH eSubvariation1
				CASE SMS_TS_QUARRY
					SWITCH iVehicle
						CASE 0			RETURN BUZZARD2
						CASE 1			RETURN speedo
						CASE 2			RETURN speedo
						CASE 3			RETURN speedo
						
						CASE 4			RETURN BUZZARD // ambush
						CASE 5			RETURN BUZZARD // ambush
						
						CASE 6			RETURN POLMAV // SUPPORT
						CASE 7			RETURN POLMAV // SUPPORT
					ENDSWITCH
				BREAK
				
				CASE SMS_TS_POWER_STATION
					SWITCH iVehicle
						CASE 0			RETURN BUZZARD2
						CASE 1			RETURN speedo
						
						CASE 2			RETURN BUZZARD // ambush
						CASE 3			RETURN BUZZARD // ambush
						
						CASE 4			RETURN POLMAV // SUPPORT
						CASE 5			RETURN POLMAV // SUPPORT
					ENDSWITCH
				BREAK
				
				CASE SMS_TS_PLAYBOY_MANSION
					SWITCH iVehicle
						CASE 0			RETURN kuruma
						CASE 1			RETURN kuruma
						CASE 2			RETURN kuruma
						CASE 3			RETURN SUPERVOLITO
						CASE 4			RETURN SUPERVOLITO
						
						CASE 5			RETURN BUZZARD // ambush
						CASE 6			RETURN BUZZARD // ambush
						
						CASE 7			RETURN POLMAV // SUPPORT
						CASE 8			RETURN POLMAV // SUPPORT
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
				
		CASE SMV_DOGFIGHT
			SWITCH iVehicle
				CASE 0			RETURN LAZER		
				CASE 1			RETURN LAZER		
				CASE 2			RETURN LAZER		
				CASE 3			RETURN LAZER
				CASE 4			RETURN LAZER
				
				CASE 10			RETURN nokota		// Support
				CASE 11			RETURN nokota		// Support
				CASE 12			RETURN nokota		// Support
				CASE 13			RETURN nokota		// Support
				
				CASE 14			RETURN LAZER		// Ambush
				CASE 15			RETURN LAZER		// Ambush
			ENDSWITCH
		BREAK
		
		CASE SMV_BOMB_BASE
			SWITCH eSubvariation1
				CASE SMS_BB_TERMINAL
					SWITCH iVehicle
						CASE 0			RETURN FROGGER
						CASE 1			RETURN MULE
						CASE 2			RETURN MULE
						CASE 3			RETURN MULE
						CASE 4			RETURN FROGGER
						CASE 5			RETURN FROGGER
						CASE 6			RETURN MULE
						CASE 7			RETURN MULE
						CASE 8			RETURN tug
						CASE 9			RETURN tug
						CASE 10			RETURN tug
						CASE 11			RETURN FROGGER
						CASE 12			RETURN FROGGER	// Carrier
						CASE 13			RETURN FROGGER	// Carrier
						CASE 14			RETURN FROGGER	// Ambush
						CASE 15			RETURN FROGGER	// Ambush
						CASE 16			RETURN STARLING	// Support
						CASE 17			RETURN STARLING	// Support
						CASE 18			RETURN STARLING	// Support
						CASE 19			RETURN STARLING	// Support
					ENDSWITCH
				BREAK
				CASE SMS_BB_GRAPESEED_AUTOYARD
					SWITCH iVehicle
						CASE 0			RETURN MULE
						CASE 1			RETURN MULE
						CASE 2			RETURN MULE
						CASE 3			RETURN MULE
						CASE 4			RETURN MULE
						CASE 5			RETURN FROGGER
						CASE 6			RETURN FROGGER
						CASE 7			RETURN FROGGER
						CASE 8			RETURN MULE
						CASE 9			RETURN MULE
						CASE 10			RETURN FROGGER
						CASE 11			RETURN FROGGER
						CASE 12			RETURN FROGGER	// Carrier
						CASE 13			RETURN FROGGER  // Carrier
						CASE 14			RETURN FROGGER	// Ambush
						CASE 15			RETURN FROGGER	// Ambush
						CASE 16			RETURN STARLING	// Support
						CASE 17			RETURN STARLING	// Support
						CASE 18			RETURN STARLING	// Support
						CASE 19			RETURN STARLING	// Support
					ENDSWITCH
				BREAK
				CASE SMS_BB_PLANE_SCRAPYARD
					SWITCH iVehicle
						CASE 0			RETURN MULE
						CASE 1			RETURN MULE
						CASE 2			RETURN MULE
						CASE 3			RETURN MULE
						CASE 4			RETURN MULE
						CASE 5			RETURN BUZZARD
						CASE 6			RETURN MULE
						CASE 7			RETURN MULE
						CASE 8			RETURN MULE
						CASE 9			RETURN BUZZARD
						CASE 10			RETURN BUZZARD
						CASE 11			RETURN BUZZARD
						CASE 12			RETURN BUZZARD	// Carrier
						CASE 13			RETURN BUZZARD  // Carrier
						CASE 14			RETURN BUZZARD	// Ambush
						CASE 15			RETURN BUZZARD	// Ambush
						CASE 16			RETURN STARLING	// Support
						CASE 17			RETURN STARLING	// Support
						CASE 18			RETURN STARLING	// Support
						CASE 19			RETURN STARLING	// Support
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_CARGO_PLANE
			SWITCH iVehicle
				CASE 0			RETURN CARGOPLANE
				CASE 1			RETURN CARGOPLANE
				CASE 2			RETURN CARGOPLANE
				CASE 3			RETURN LAZER
				CASE 4			RETURN LAZER
				CASE 5			RETURN LAZER
				CASE 6			RETURN LAZER
				CASE 7			RETURN LAZER
				CASE 8			RETURN LAZER
				CASE 9			RETURN LAZER	// Ambush
				CASE 10			RETURN LAZER	// Ambush
				CASE 11			RETURN MOLOTOK	// Support
				CASE 12			RETURN MOLOTOK	// Support
				CASE 13			RETURN MOLOTOK	// Support
				CASE 14			RETURN MOLOTOK	// Support
			ENDSWITCH
		BREAK
		
		CASE SMV_ROOF_ATTACK
			SWITCH eSubvariation1
				CASE SMS_RA_CONSTRUCTION_SITE
					SWITCH iVehicle
						CASE 0			RETURN FROGGER
						CASE 1			RETURN FROGGER	// Ambush
						CASE 2			RETURN FROGGER  // Ambush
						CASE 3			RETURN BUZZARD2 // Support
						CASE 4			RETURN BUZZARD2 // Support
					ENDSWITCH
				BREAK
				CASE SMS_RA_ALTA_HOTEL
					SWITCH iVehicle
						CASE 0			RETURN SUPERVOLITO2	// Ambush
						CASE 1			RETURN SUPERVOLITO2	// Ambush
						CASE 2			RETURN BUZZARD2 	// Support
						CASE 3			RETURN BUZZARD2 	// Support
					ENDSWITCH
				BREAK
				CASE SMS_RA_PACIFIC_STANDARD
					SWITCH iVehicle
						CASE 0			RETURN BUZZARD	// Ambush
						CASE 1			RETURN BUZZARD	// Ambush
						CASE 2			RETURN BUZZARD2 // Support
						CASE 3			RETURN BUZZARD2 // Support
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_BOMBING_RUN
			SWITCH iVehicle
				CASE 0			RETURN tug
				CASE 1			RETURN tug
				CASE 2			RETURN tug
				CASE 3			RETURN trash2
				CASE 4			RETURN trash2
				CASE 5			RETURN trash2
				CASE 6			RETURN trash2
				CASE 7			RETURN trash2
				CASE 8			RETURN trash2
				CASE 9			RETURN trash2
				CASE 10			RETURN SEABREEZE		// Support
				CASE 11			RETURN SEABREEZE		// Support
				CASE 12			RETURN SEABREEZE		// Support
				CASE 13			RETURN SEABREEZE		// Support
				CASE 14			RETURN rogue			// Ambush
				CASE 15			RETURN rogue			// Ambush
			ENDSWITCH
		BREAK
		
		CASE SMV_INFILTRATION
			SWITCH eSubvariation1
				CASE SMS_INF_HUMANE_LABS
					SWITCH iVehicle
						CASE 0			RETURN INSURGENT2
						CASE 1			RETURN BUZZARD		// Carrier
						CASE 2			RETURN BUZZARD		// Carrier
						CASE 3			RETURN CARGOBOB2 	// Support
					ENDSWITCH
				BREAK
				CASE SMS_INF_BOLINGBROKE_PRISON
					SWITCH iVehicle
						CASE 0			RETURN GRANGER
						CASE 1			RETURN PRIMO
						CASE 2			RETURN PBUS
						CASE 3			RETURN PBUS
						CASE 4			RETURN riot			// Carrier
						CASE 5			RETURN riot			// Carrier
						CASE 6			RETURN CARGOBOB2	// Support
					ENDSWITCH
				BREAK
				CASE SMS_INF_ZANCUDO
					SWITCH iVehicle
						CASE 0			RETURN RHINO
						CASE 1			RETURN cargobob
						CASE 2			RETURN BUZZARD		// Carrier
						CASE 3			RETURN BUZZARD		// Carrier
						CASE 4			RETURN CARGOBOB2	// Support
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_BEACON_GRAB
			SWITCH iVehicle
				CASE 0		RETURN BRICKADE
				CASE 1		RETURN BRICKADE
				CASE 2		RETURN hunter			// Support
				CASE 3		RETURN hunter			// Support
				CASE 4		RETURN BUZZARD			// Ambush
				CASE 5		RETURN BUZZARD			// Ambush
			ENDSWITCH
		BREAK
		
		CASE SMV_ESCORT
			SWITCH iVehicle
				CASE 0		RETURN TITAN
				CASE 1		RETURN BUZZARD			// Ambush
				CASE 2		RETURN BUZZARD			// Ambush
				CASE 3		RETURN MOGUL		// Support
				CASE 4		RETURN MOGUL		// Support
				CASE 5		RETURN MOGUL		// Support
				CASE 6		RETURN MOGUL		// Support
			ENDSWITCH
		BREAK
		
		CASE SMV_BOMB_ROOF
			SWITCH iVehicle
				CASE 0		RETURN BUZZARD2
				CASE 1		RETURN BUZZARD2		
				CASE 2		RETURN BUZZARD2		// Carrier
				CASE 3		RETURN BUZZARD2		// Carrier
				CASE 4		RETURN ROGUE		// Support
				CASE 5		RETURN ROGUE		// Support
				CASE 6		RETURN BUZZARD		// Ambush
				CASE 7		RETURN BUZZARD		// Ambush
			ENDSWITCH
		BREAK

		CASE SMV_STUNT_PILOT
			SWITCH iVehicle
				CASE 0		RETURN MULE			// Carrier
				CASE 1		RETURN MULE			// Carrier
				CASE 2		RETURN HOWARD		// Support
				CASE 3		RETURN HOWARD		// Support
				CASE 4		RETURN HOWARD		// Support
				CASE 5		RETURN HOWARD		// Support
			ENDSWITCH
		BREAK

		// Sell
		CASE SMV_SELL_HEAVY_LIFTING
			SWITCH iVehicle
				CASE 0			RETURN TUG
				CASE 1			RETURN TUG
			ENDSWITCH
		BREAK
		
		CASE SMV_SELL_FLYING_FORTRESS
			SWITCH iVehicle
				CASE 0			RETURN BUZZARD
				CASE 1			RETURN BUZZARD
			ENDSWITCH
		BREAK
		
		CASE SMV_SELL_CONTESTED
			RETURN BUZZARD
			
		CASE SMV_SELL_FLY_LOW
			SWITCH eDropOff
				CASE SMUGGLER_DROPOFF_FLY_LOW_5
				CASE SMUGGLER_DROPOFF_FLY_LOW_6
				CASE SMUGGLER_DROPOFF_FLY_LOW_8
				CASE SMUGGLER_DROPOFF_FLY_LOW_9
				CASE SMUGGLER_DROPOFF_FLY_LOW_14
				CASE SMUGGLER_DROPOFF_FLY_LOW_23
					RETURN SEASHARK
			ENDSWITCH
			RETURN BLAZER
		
		CASE SMV_SELL_PRECISION_DELIVERY
			RETURN FLATBED
			
		CASE SMV_SELL_AIR_DELIVERY
			SWITCH iVehicle
				CASE 15
				CASE 16
					RETURN rogue
			ENDSWITCH
			
			SWITCH eDropOff
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_1
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_2
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_3
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_4
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_5
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_6
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_7
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_8
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_9
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_10
					RETURN HAVOK
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_11
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_12
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_13
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_14
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_15
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_16
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_17
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_18
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_19
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_20
					RETURN POUNDER
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_21
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_22
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_23
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_24
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_25
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_26
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_27
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_28
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_29
				CASE SMUGGLER_DROPOFF_AIR_DELIVERY_30
					RETURN TUG
			ENDSWITCH
		BREAK
		
		CASE SMV_SELL_AIR_POLICE
			SWITCH iVehicle
				CASE 5
				CASE 6
					RETURN POLMAV
			ENDSWITCH
			
			SWITCH eDropOff
				CASE SMUGGLER_DROPOFF_AIR_POLICE_1
				CASE SMUGGLER_DROPOFF_AIR_POLICE_3
				CASE SMUGGLER_DROPOFF_AIR_POLICE_4
				CASE SMUGGLER_DROPOFF_AIR_POLICE_5
				CASE SMUGGLER_DROPOFF_AIR_POLICE_6
				CASE SMUGGLER_DROPOFF_AIR_POLICE_7
				CASE SMUGGLER_DROPOFF_AIR_POLICE_8
				CASE SMUGGLER_DROPOFF_AIR_POLICE_11
				CASE SMUGGLER_DROPOFF_AIR_POLICE_19
				CASE SMUGGLER_DROPOFF_AIR_POLICE_20
				CASE SMUGGLER_DROPOFF_AIR_POLICE_21
				CASE SMUGGLER_DROPOFF_AIR_POLICE_22
				CASE SMUGGLER_DROPOFF_AIR_POLICE_23
				CASE SMUGGLER_DROPOFF_AIR_POLICE_25
				CASE SMUGGLER_DROPOFF_AIR_POLICE_27
				CASE SMUGGLER_DROPOFF_AIR_POLICE_28
					RETURN HAVOK
				CASE SMUGGLER_DROPOFF_AIR_POLICE_2
				CASE SMUGGLER_DROPOFF_AIR_POLICE_10
				CASE SMUGGLER_DROPOFF_AIR_POLICE_24
				CASE SMUGGLER_DROPOFF_AIR_POLICE_26
				CASE SMUGGLER_DROPOFF_AIR_POLICE_29
				CASE SMUGGLER_DROPOFF_AIR_POLICE_30
					RETURN POUNDER
				CASE SMUGGLER_DROPOFF_AIR_POLICE_9
				CASE SMUGGLER_DROPOFF_AIR_POLICE_12
				CASE SMUGGLER_DROPOFF_AIR_POLICE_13
				CASE SMUGGLER_DROPOFF_AIR_POLICE_14
				CASE SMUGGLER_DROPOFF_AIR_POLICE_15
				CASE SMUGGLER_DROPOFF_AIR_POLICE_16
				CASE SMUGGLER_DROPOFF_AIR_POLICE_17
				CASE SMUGGLER_DROPOFF_AIR_POLICE_18
					RETURN TUG
			ENDSWITCH
		BREAK
		
		CASE SMV_SETUP_RON
			SWITCH eSubvariation1
				CASE SMS_SETUP_DELPERROBEACH 
				CASE SMS_SETUP_LSRIVER
				CASE SMS_SETUP_ELBURRO
				CASE SMS_SETUP_CHUMASH
				CASE SMS_SETUP_TATAVIUM
				CASE SMS_SETUP_RONSWINDFARM
				CASE SMS_SETUP_GRANDSENORA1
				CASE SMS_SETUP_GRANDSENORA2
				CASE SMS_SETUP_SANDY
				CASE SMS_SETUP_STAB
				CASE SMS_SETUP_CALAFIA
				CASE SMS_SETUP_MCKENZIE
				CASE SMS_SETUP_GRAPESEED
				CASE SMS_SETUP_PORCOPIO
				CASE SMS_SETUP_PALETO
					RETURN SLAMVAN2
				BREAK
			ENDSWITCH
		BREAK
		
		
		
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

/////////////////////////////////
/// SMUGGLER PED SPAWN COORDS ///
/////////////////////////////////
   
FUNC VECTOR GET_SMUGGLER_PED_SPAWN_COORDS(INT iPed, SMUGGLER_LOCATION eLocation, SMUGGLER_VARIATION eVariation, FREEMODE_DELIVERY_DROPOFFS eDropOff = FREEMODE_DELIVERY_DROPOFF_INVALID, INT iRespawnLocation = 0)
	SWITCH eVariation
		CASE SMV_STEAL_AIRCRAFT			
			SWITCH eLocation
				CASE SmugglerLocation_StealAircraft_SandyShores
					SWITCH iPed
						CASE 0			RETURN <<1743.8560, 3296.6101, 40.1170>>
						CASE 1			RETURN <<1721.7970, 3304.4299, 40.2230>>
						CASE 2			RETURN <<1721.6071, 3320.0171, 40.2230>>
						CASE 3			RETURN <<1739.9030, 3310.8230, 40.2230>>
						CASE 4			RETURN <<1739.0291, 3311.9131, 40.2230>>
						CASE 5			RETURN <<1728.0270, 3313.4131, 40.2230>>
						CASE 6			RETURN <<1728.6219, 3314.6550, 40.2230>>
						CASE 7			RETURN <<1728.3190, 3296.1021, 40.2230>>
						CASE 8			RETURN <<1728.9990, 3297.4260, 40.2230>>
						CASE 9			RETURN <<1716.8959, 3317.1418, 40.2235>>
						CASE 10			RETURN <<1724.4331, 3287.9612, 40.1522>>
						CASE 11			RETURN <<1724.5315, 3289.6213, 40.1871>>
						CASE 12			RETURN <<1756.3555, 3297.5012, 40.1540>>
						CASE 13			RETURN <<1757.8159, 3297.6514, 40.1514>>
						CASE 14			RETURN <<1727.0168, 3326.0593, 40.2235>>
						CASE 15			RETURN <<1736.5751, 3328.0259, 40.2235>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_StealAircraft_McKenzieAirfield
					SWITCH iPed
						CASE 0			RETURN <<2138.0271, 4795.7979, 40.1110>>
						CASE 1			RETURN <<2124.8960, 4785.8301, 39.9700>>
						CASE 2			RETURN <<2125.7109, 4786.7822, 39.9700>>
						CASE 3			RETURN <<2136.8159, 4785.3232, 39.9700>>
						CASE 4			RETURN <<2135.8301, 4786.4102, 39.9700>>
						CASE 5			RETURN <<2126.0000, 4775.5200, 39.9700>>
						CASE 6			RETURN <<2137.5630, 4775.1001, 39.9700>>
						CASE 7			RETURN <<2138.5459, 4776.2080, 39.9700>>
						CASE 8			RETURN <<2145.8230, 4775.5869, 39.9980>>
						CASE 9			RETURN <<2142.6499, 4786.0649, 39.9700>>
						CASE 10			RETURN <<2145.5415, 4793.8145, 40.1418>>
						CASE 11			RETURN <<2146.8486, 4794.5269, 40.1416>>
						CASE 12			RETURN <<2116.4878, 4784.1382, 40.0225>>
						CASE 13			RETURN <<2143.0986, 4777.2690, 39.9703>>
						CASE 14			RETURN <<2128.0420, 4769.7456, 39.9703>>
						CASE 15			RETURN <<2137.1440, 4770.1353, 40.0288>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_StealAircraft_EpsilonHQ
					SWITCH iPed
						CASE 0			RETURN <<-681.9910, 72.3300, 68.6860>>
						CASE 1			RETURN <<-701.9560, 65.4400, 68.6860>>
						CASE 2			RETURN <<-685.3880, 59.9060, 68.6860>>
						CASE 3			RETURN <<-680.0960, 101.0550, 54.8550>>
						CASE 4			RETURN <<-683.3560, 72.0420, 68.6860>>
						CASE 5			RETURN <<-680.3070, 102.3540, 54.8550>>
						CASE 6			RETURN <<-681.4200, 82.9880, 68.6860>>
						CASE 7			RETURN <<-697.7540, 57.8210, 68.6860>>
						CASE 8			RETURN <<-708.5700, 73.3490, 68.6860>>
						CASE 9			RETURN <<-708.5760, 72.2880, 68.6860>>
						CASE 10			RETURN <<-690.4533, 76.3671, 54.8554>>
						CASE 11			RETURN <<-697.0634, 71.0390, 58.8734>>
						CASE 12			RETURN <<-666.7314, 98.2226, 54.8554>>
						CASE 13			RETURN <<-670.3982, 92.1167, 54.8554>>
						CASE 14			RETURN <<-711.2704, 56.4375, 68.6856>>
						CASE 15			RETURN <<-696.6024, 56.8736, 68.6856>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE SMV_BEACON_GRAB				
			SWITCH eLocation
				CASE SmugglerLocation_BeaconGrab_Location1
					SWITCH iPed
						CASE 0			RETURN <<0.0000, 0.0000, 0.0000>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_SPLASH_LANDING
			SWITCH eLocation
				CASE SmugglerLocation_SplashLanding_LagoZancudo
					SWITCH iPed
						// Peds 0-5 are created in vehicles		
						
						// Peds 6 and 7 are on the barge
						CASE 6			RETURN <<-3244.1345, 3020.2588, 1.0000>>
						CASE 7			RETURN <<-3244.7468, 3018.8955, 1.0000>>
						
						// Peds 8-10 are created in vehicles	
						CASE 11			RETURN <<-2.5000, -14.2000, 1.2000>>
						CASE 12			RETURN <<3.2000, -10.4500, 0.7000>>
						CASE 13			RETURN <<-2.4000, -6.7000, 3.1000>>
						CASE 14			RETURN <<-1.6000, 11.5000, 1.3000>>
						CASE 15			RETURN <<2.2000, 7.1500, 3.1000>>
						CASE 16			RETURN <<-2.5000, -14.2000, 1.2000>>
						CASE 17			RETURN <<3.2000, -10.4500, 0.7000>>
						CASE 18			RETURN <<-2.4000, -6.7000, 3.1000>>
						CASE 19			RETURN <<-1.6000, 11.5000, 1.3000>>
						CASE 20			RETURN <<2.2000, 7.1500, 3.1000>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_SplashLanding_PacificOceanE
					SWITCH iPed
						// Peds 0-5 are created in vehicles		
						
						// Peds 6 and 7 are on the barge
						CASE 6			RETURN <<3853.1433, 3044.2876, 1.0>>
						CASE 7			RETURN <<3852.5837, 3042.8889, 1.0>>
						
						// Peds 8-10 are created in vehicles	
						CASE 11			RETURN <<-2.5000, -14.2000, 1.2000>>
						CASE 12			RETURN <<3.2000, -10.4500, 0.7000>>
						CASE 13			RETURN <<-2.4000, -6.7000, 3.1000>>
						CASE 14			RETURN <<-1.6000, 11.5000, 1.3000>>
						CASE 15			RETURN <<2.2000, 7.1500, 3.1000>>
						CASE 16			RETURN <<-2.5000, -14.2000, 1.2000>>
						CASE 17			RETURN <<3.2000, -10.4500, 0.7000>>
						CASE 18			RETURN <<-2.4000, -6.7000, 3.1000>>
						CASE 19			RETURN <<-1.6000, 11.5000, 1.3000>>
						CASE 20			RETURN <<2.2000, 7.1500, 3.1000>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_SplashLanding_PacificOceanNE
					SWITCH iPed
						// Peds 0-5 are created in vehicles		
						
						// Peds 6 and 7 are on the barge
						CASE 6			RETURN <<3363.8655, 6295.7754, 1.0>>
						CASE 7			RETURN <<3365.0669, 6296.5205, 1.0>>

						// Peds 8-10 are created in vehicles	
						CASE 11			RETURN <<-2.5000, -14.2000, 1.2000>>
						CASE 12			RETURN <<3.2000, -10.4500, 0.7000>>
						CASE 13			RETURN <<-2.4000, -6.7000, 3.1000>>
						CASE 14			RETURN <<-1.6000, 11.5000, 1.3000>>
						CASE 15			RETURN <<2.2000, 7.1500, 3.1000>>
						CASE 16			RETURN <<-2.5000, -14.2000, 1.2000>>
						CASE 17			RETURN <<3.2000, -10.4500, 0.7000>>
						CASE 18			RETURN <<-2.4000, -6.7000, 3.1000>>
						CASE 19			RETURN <<-1.6000, 11.5000, 1.3000>>
						CASE 20			RETURN <<2.2000, 7.1500, 3.1000>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		
			
		BREAK
		
		CASE SMV_BLACK_BOX
			SWITCH eLocation
				CASE SmugglerLocation_Blackbox_MountGordo
					SWITCH iPed
						CASE 0			RETURN <<2573.1538, 6181.2007, 162.4240>>
						CASE 1			RETURN <<2572.9150, 6175.1938, 162.9417>>
						CASE 2			RETURN <<2579.1563, 6174.7520, 163.3838>>
						CASE 3			RETURN <<2562.8848, 6177.6328, 162.5126>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Blackbox_PaletoForest
					SWITCH iPed
						CASE 0			RETURN <<-399.4203, 5842.6362, 45.4566>>
						CASE 1			RETURN <<-408.8625, 5813.5020, 53.2642>>
						CASE 2			RETURN <<-401.2626, 5811.1968, 53.2378>>
						CASE 3			RETURN <<-407.1889, 5802.8032, 54.5755>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Blackbox_Island
					SWITCH iPed
						CASE 0			RETURN <<2817.8054, -1461.0920, 9.0637>>
						CASE 1			RETURN <<2786.5566, -1505.1354, 7.6518>>
						CASE 2			RETURN <<2799.5942, -1517.7476, 6.9735>>
						CASE 3			RETURN <<2805.5874, -1510.6223, 8.2916>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK

		CASE SMV_THERMAL_SCOPE	
		
			SWITCH eLocation
				CASE SmugglerLocation_ThermalScope_Quarry
					SWITCH iPed
						CASE 0			RETURN <<1093.7800, 2140.3269, 56.6950>>
						CASE 1			RETURN <<1130.8130, 2124.7920, 54.5560>>
						CASE 2			RETURN <<1127.5200, 2101.5000, 54.7720>>
						CASE 3			RETURN <<1113.3730, 2182.4929, 44.5350>>
						CASE 4			RETURN <<1112.5330, 2183.4170, 44.5680>>
						CASE 5			RETURN <<1075.1600, 2193.2361, 47.2590>>
						CASE 6			RETURN <<1135.3350, 2191.6931, 78.3300>>
						CASE 7			RETURN <<1123.0110, 2126.8630, 78.2720>>
						CASE 8			RETURN <<1145.0760, 2102.4810, 54.8000>>
						CASE 9			RETURN <<1108.0830, 2209.3750, 47.9070>>
						CASE 10			RETURN <<1100.4020, 2161.8940, 52.4150>>
						CASE 11			RETURN <<1131.6360, 2179.6509, 47.9660>>
						CASE 12			RETURN <<1101.5710, 2096.3921, 53.1580>>
						CASE 13			RETURN <<1126.1840, 2100.9570, 54.7720>>
						CASE 14			RETURN <<1163.8530, 2129.0171, 54.2110>>
					ENDSWITCH
				BREAK

				CASE SmugglerLocation_ThermalScope_PowerStation
					SWITCH iPed
						CASE 0			RETURN <<2715.2710, 1480.3230, 43.5610>>
						CASE 1			RETURN <<2733.8679, 1424.1110, 23.4890>>
						CASE 2			RETURN <<2732.9570, 1424.4969, 23.4890>>
						CASE 3			RETURN <<2716.8311, 1502.5660, 41.2530>>
						CASE 4			RETURN <<2715.2881, 1462.3070, 23.5010>>
						CASE 5			RETURN <<2716.2520, 1462.3030, 23.5010>>
						CASE 6			RETURN <<2722.4131, 1510.5870, 43.5610>>
						CASE 7			RETURN <<2741.3401, 1556.5820, 39.3380>>
						CASE 8			RETURN <<2719.4551, 1539.6021, 49.5360>>
						CASE 9			RETURN <<2729.2971, 1529.2430, 39.3370>>
						CASE 10			RETURN <<2733.2190, 1580.9189, 65.5430>>
						CASE 11			RETURN <<2729.4751, 1574.6906, 49.5362>>
						CASE 12			RETURN <<2740.0049, 1543.6230, 49.6920>>
						CASE 13			RETURN <<2749.4641, 1445.5610, 23.4890>>
						CASE 14			RETURN <<2764.9399, 1441.2340, 23.4490>>
					ENDSWITCH
				BREAK

				CASE SmugglerLocation_ThermalScope_PlayboyMansion
					SWITCH iPed
						CASE 0			RETURN <<-1558.8030, 108.0160, 55.7800>>
						CASE 1			RETURN <<-1560.0341, 108.3450, 55.7800>>
						CASE 2			RETURN <<-1547.3370, 115.7810, 55.7800>>
						CASE 3			RETURN <<-1584.3180, 81.0960, 58.2100>>
						CASE 4			RETURN <<-1538.7190, 126.8250, 55.7800>>
						CASE 5			RETURN <<-1537.6340, 122.5830, 63.5440>>
						CASE 6			RETURN <<-1525.2184, 75.4085, 60.3140>>
						CASE 7			RETURN <<-1570.8080, 114.9730, 58.1790>>
						CASE 8			RETURN <<-1541.0206, 66.5022, 56.5391>>
						CASE 9			RETURN <<-1570.8781, 126.9690, 57.2810>>
						CASE 10			RETURN <<-1541.3140, 65.5921, 56.5391>>
						CASE 11			RETURN <<-1583.7889, 82.0040, 58.2100>>
						CASE 12			RETURN <<-1552.5229, 84.4120, 60.3130>>
						CASE 13			RETURN <<-1575.1400, 94.8190, 57.5680>>
						CASE 14			RETURN <<-1499.4680, 102.2780, 54.5740>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_BOMB_BASE			
			SWITCH eLocation
				CASE SmugglerLocation_BombBase_Terminal
					SWITCH iPed
						CASE 0			RETURN <<1292.8650, -3333.4971, 4.9040>>
						CASE 1			RETURN <<1292.0240, -3335.0791, 4.9030>>
						CASE 2			RETURN <<1269.8988, -3327.7295, 4.9016>>
						CASE 3			RETURN <<1245.5840, -3332.2661, 5.0290>>
						CASE 4			RETURN <<1274.3810, -3245.8240, 7.7120>>
						CASE 5			RETURN <<1289.8740, -3301.7471, 23.3930>>
						CASE 6			RETURN <<1277.9070, -3258.9600, 7.7120>>
						CASE 7			RETURN <<1296.4520, -3266.7490, 4.9070>>
						CASE 8			RETURN <<1295.1000, -3267.3911, 4.9070>>
						CASE 9			RETURN <<1288.6592, -3234.7737, 4.9046>>
						CASE 10			RETURN <<1286.9646, -3234.1013, 4.9039>>
						CASE 11			RETURN <<1244.9700, -3267.9451, 4.6840>>
						CASE 12			RETURN <<1246.0360, -3284.0220, 5.0290>>
						CASE 13			RETURN <<1277.7520, -3270.1799, 4.9020>>
						CASE 14			RETURN <<1271.6851, -3316.9199, 4.9020>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_BombBase_GrapeseedAutoyard
					SWITCH iPed
						CASE 0			RETURN <<2334.5891, 4860.6382, 51.1640>>
						CASE 1			RETURN <<2311.1951, 4884.1909, 51.1700>>
						CASE 2			RETURN <<2333.8779, 4890.0640, 40.8110>>
						CASE 3			RETURN <<2333.3040, 4891.5078, 40.8130>>
						CASE 4			RETURN <<2253.4150, 4941.4629, 40.2160>>
						CASE 5			RETURN <<2279.5459, 4914.0869, 40.0340>>
						CASE 6			RETURN <<2270.7671, 4966.5898, 40.7290>>
						CASE 7			RETURN <<2293.6250, 4933.8672, 40.4210>>
						CASE 8			RETURN <<2280.2461, 4947.1191, 40.3310>>
						CASE 9			RETURN <<2267.7351, 4853.6880, 39.6710>>
						CASE 10			RETURN <<2266.4280, 4853.8940, 39.6690>>
						CASE 11			RETURN <<2324.5459, 4928.3120, 40.5120>>
						CASE 12			RETURN <<2305.9761, 4863.0439, 40.8080>>
						CASE 13			RETURN <<2305.5730, 4864.4131, 40.8080>>
						CASE 14			RETURN <<2256.1289, 4957.8662, 43.3730>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_BombBase_PlaneScrapyard
					SWITCH iPed
						CASE 0			RETURN <<2357.0720, 3072.4700, 47.1660>>
						CASE 1			RETURN <<2400.9319, 3127.1960, 51.0460>>
						CASE 2			RETURN <<2372.0659, 3061.2180, 49.9380>>
						CASE 3			RETURN <<2375.4761, 3030.5310, 52.6780>>
						CASE 4			RETURN <<2403.0869, 3101.7620, 47.1530>>
						CASE 5			RETURN <<2427.6421, 3112.1389, 47.1760>>
						CASE 6			RETURN <<2366.2849, 3115.2810, 47.3260>>
						CASE 7			RETURN <<2366.5979, 3116.8010, 47.3890>>
						CASE 8			RETURN <<2440.7229, 3106.4810, 46.5690>>
						CASE 9			RETURN <<2439.3821, 3107.0840, 46.7700>>
						CASE 10			RETURN <<2415.8660, 3133.1030, 47.1740>>
						CASE 11			RETURN <<2414.3159, 3131.9629, 47.1660>>
						CASE 12			RETURN <<2411.8989, 3043.1509, 47.1520>>
						CASE 13			RETURN <<2380.0940, 3040.8320, 47.1530>>
						CASE 14			RETURN <<2356.2141, 3043.3660, 47.1520>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_ROOF_ATTACK		
			SWITCH eLocation
				CASE SmugglerLocation_RoofAttack_ConstructionSite
					SWITCH iPed
						CASE 0			RETURN <<-180.8231, -992.3263, 253.3570>>
						CASE 1			RETURN <<-160.3267, -989.1707, 253.1315>>
						CASE 2			RETURN <<-171.4300, -963.7650, 253.3570>>
						CASE 3			RETURN <<-162.1393, -993.4059, 253.1315>>
						CASE 4			RETURN <<-161.3740, -994.5391, 253.1315>>
						CASE 5			RETURN <<-176.8964, -1005.3085, 253.1315>>
						CASE 6			RETURN <<-159.4269, -1014.7977, 253.1315>>
						CASE 7			RETURN <<-160.9709, -1015.3647, 253.1315>>
						CASE 8			RETURN <<-142.0740, -966.1560, 253.1310>>
						CASE 9			RETURN <<-142.9340, -964.5970, 253.1310>>
						CASE 10			RETURN <<-151.7730, -942.1410, 253.1310>>
						CASE 11			RETURN <<-152.8010, -960.1270, 253.1310>>
						CASE 12			RETURN <<-143.8030, -974.0040, 253.1310>>
						CASE 13			RETURN <<-152.2680, -1004.2540, 253.1310>>
						CASE 14			RETURN <<-147.1000, -994.4900, 260.8520>>
						CASE 15			RETURN <<-152.8190, -985.9580, 253.1310>>
						CASE 16			RETURN <<-148.6190, -951.6810, 253.1310>>
						CASE 17			RETURN <<-147.1330, -951.7520, 253.1310>>
						CASE 18			RETURN <<-170.7410, -980.0190, 253.1310>>
						CASE 19			RETURN <<-154.0760, -984.8700, 253.1310>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_RoofAttack_AltaHotel
					SWITCH iPed
						CASE 0			RETURN <<449.4620, -262.5200, 70.2380>>
						CASE 1			RETURN <<442.6510, -261.7820, 70.2470>>
						CASE 2			RETURN <<423.1600, -259.2550, 70.2500>>
						CASE 3			RETURN <<422.5160, -258.4250, 70.2510>>
						CASE 4			RETURN <<414.2870, -236.0190, 73.2510>>
						CASE 5			RETURN <<442.5470, -260.5690, 70.2470>>
						CASE 6			RETURN <<453.5840, -259.0890, 73.8070>>
						CASE 7			RETURN <<401.5210, -242.6960, 73.2520>>
						CASE 8			RETURN <<439.3910, -268.2460, 70.2450>>
						CASE 9			RETURN <<431.6110, -266.8630, 70.2490>>
						CASE 10			RETURN <<407.7993, -248.8011, 71.4987>>
						CASE 11			RETURN <<446.3050, -267.2810, 70.2380>>
						CASE 12			RETURN <<447.5420, -266.9960, 70.2380>>
						CASE 13			RETURN <<412.9120, -236.3450, 73.2510>>
						CASE 14			RETURN <<446.8600, -272.3260, 73.8070>>
						CASE 15			RETURN <<402.3630, -243.7970, 73.2520>>
						CASE 16			RETURN <<421.3790, -249.5220, 73.2510>>
						CASE 17			RETURN <<406.6690, -258.0850, 70.2400>>
						CASE 18			RETURN <<430.7440, -253.7620, 70.2530>>
						CASE 19			RETURN <<399.4490, -248.9340, 73.2520>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_RoofAttack_PacificStandard
					SWITCH iPed
						CASE 0			RETURN <<273.6900, 218.6980, 136.7570>>
						CASE 1			RETURN <<275.3510, 219.2480, 136.7570>>
						CASE 2			RETURN <<306.2930, 294.2270, 131.0910>>
						CASE 3			RETURN <<300.3320, 268.4050, 131.0910>>
						CASE 4			RETURN <<284.6860, 283.3260, 131.0910>>
						CASE 5			RETURN <<283.7280, 284.5280, 131.0910>>
						CASE 6			RETURN <<254.6654, 265.5165, 136.7574>>
						CASE 7			RETURN <<282.5490, 294.1870, 131.0910>>
						CASE 8			RETURN <<260.6010, 256.8650, 136.7570>>
						CASE 9			RETURN <<258.8170, 256.4140, 136.7570>>
						CASE 10			RETURN <<261.2070, 212.9670, 136.7570>>
						CASE 11			RETURN <<275.3700, 265.6570, 136.7570>>
						CASE 12			RETURN <<290.7030, 247.5230, 122.8190>>
						CASE 13			RETURN <<289.8780, 249.0240, 122.8190>>
						CASE 14			RETURN <<283.1380, 240.1390, 127.3980>>
						CASE 15			RETURN <<277.1760, 201.0760, 136.7570>>
						CASE 16			RETURN <<261.9570, 251.7110, 136.7570>>
						CASE 17			RETURN <<286.2430, 228.2710, 136.7570>>
						CASE 18			RETURN <<296.8090, 305.1520, 131.0910>>
						CASE 19			RETURN <<304.9360, 293.5030, 131.0910>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_BOMB_ROOF		
			SWITCH eLocation
				CASE SmugglerLocation_BombRoof_Route1
					SWITCH iRespawnLocation
						CASE 0
							SWITCH iPed
								CASE 0			RETURN <<993.8390, 63.4330, 103.0180>>
								CASE 1			RETURN <<995.8010, 54.9040, 103.0180>>
								CASE 2			RETURN <<988.8500, 68.9390, 106.7680>>
								CASE 3			RETURN <<998.4170, 62.4560, 103.0180>>
								CASE 4			RETURN <<982.6440, 66.5080, 106.7680>>
								CASE 5			RETURN <<986.0960, 61.7130, 106.7680>>
								CASE 6			RETURN <<988.6430, 70.5100, 106.7680>>
								CASE 7			RETURN <<986.1900, 54.5020, 103.0180>>
								CASE 8			RETURN <<996.0200, 56.2620, 103.0180>>
								CASE 9			RETURN <<990.2490, 50.8450, 103.0180>>
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH iPed
								CASE 0			RETURN <<-1002.3240, -760.7070, 78.8580>>
								CASE 1			RETURN <<-1001.4450, -759.3050, 78.8580>>
								CASE 2			RETURN <<-1008.2880, -757.2500, 78.8580>>
								CASE 3			RETURN <<-1014.9080, -754.3710, 78.8570>>
								CASE 4			RETURN <<-1013.5810, -753.6550, 78.8570>>
								CASE 5			RETURN <<-1005.0100, -741.2650, 75.5370>>
								CASE 6			RETURN <<-1013.1900, -738.3270, 75.5370>>
								CASE 7			RETURN <<-1000.4860, -733.2550, 75.5370>>
								CASE 8			RETURN <<-998.9900, -734.2700, 75.5370>>
								CASE 9			RETURN <<-993.6510, -748.1350, 75.5370>>
							ENDSWITCH
						BREAK
						CASE 2
							SWITCH iPed
								CASE 0			RETURN <<-773.1000, 780.8600, 212.9630>>
								CASE 1			RETURN <<-759.6810, 788.6010, 213.5760>>
								CASE 2			RETURN <<-758.1780, 788.2050, 213.5760>>
								CASE 3			RETURN <<-771.9670, 769.7210, 212.1990>>
								CASE 4			RETURN <<-735.3720, 786.2940, 212.1990>>
								CASE 5			RETURN <<-735.5200, 784.6940, 212.1990>>
								CASE 6			RETURN <<-752.6240, 797.0150, 219.1450>>
								CASE 7			RETURN <<-751.7340, 770.0250, 212.1990>>
								CASE 8			RETURN <<-778.5270, 783.8350, 212.9620>>
								CASE 9			RETURN <<-762.1570, 769.6090, 212.1990>>
							ENDSWITCH
						BREAK
						CASE 3
							SWITCH iPed
								CASE 0			RETURN <<768.7300, -1703.7330, 43.1470>>
								CASE 1			RETURN <<769.3030, -1705.1880, 43.1470>>
								CASE 2			RETURN <<781.6200, -1718.2280, 43.1470>>
								CASE 3			RETURN <<782.8960, -1717.4280, 43.1470>>
								CASE 4			RETURN <<784.5840, -1693.3910, 43.1470>>
								CASE 5			RETURN <<782.4550, -1703.4041, 43.1470>>
								CASE 6			RETURN <<787.9750, -1708.3510, 43.1470>>
								CASE 7			RETURN <<765.3990, -1709.1030, 43.1470>>
								CASE 8			RETURN <<779.6720, -1711.3590, 43.1470>>
								CASE 9			RETURN <<784.5700, -1694.7910, 43.1470>>
							ENDSWITCH
						BREAK
						CASE 4
							SWITCH iPed
								CASE 0			RETURN <<-2210.4060, 269.1210, 197.1090>>
								CASE 1			RETURN <<-2211.0110, 267.5040, 197.1090>>
								CASE 2			RETURN <<-2213.1941, 255.3740, 197.1090>>
								CASE 3			RETURN <<-2212.1340, 254.5830, 197.1090>>
								CASE 4			RETURN <<-2205.0391, 268.1540, 197.1090>>
								CASE 5			RETURN <<-2203.7029, 260.7860, 197.1090>>
								CASE 6			RETURN <<-2216.6260, 270.5640, 197.1090>>
								CASE 7			RETURN <<-2211.2471, 277.5440, 197.1090>>
								CASE 8			RETURN <<-2218.3059, 262.5950, 197.1090>>
								CASE 9			RETURN <<-2209.4490, 258.3000, 197.1090>>
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_BombRoof_Route2
					SWITCH iRespawnLocation
						CASE 0
							SWITCH iPed
								CASE 0			RETURN <<36.3350, 160.1400, 125.2160>>
								CASE 1			RETURN <<36.8100, 161.4890, 125.2160>>
								CASE 2			RETURN <<40.9530, 178.2070, 125.2160>>
								CASE 3			RETURN <<42.2040, 178.4190, 125.2160>>
								CASE 4			RETURN <<32.2150, 173.3530, 125.2160>>
								CASE 5			RETURN <<43.5570, 160.0050, 125.2160>>
								CASE 6			RETURN <<44.0300, 169.2370, 125.2160>>
								CASE 7			RETURN <<51.2680, 163.1510, 125.2970>>
								CASE 8			RETURN <<33.4520, 172.8680, 125.2160>>
								CASE 9			RETURN <<35.9570, 167.0110, 125.2160>>
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH iPed
								CASE 0			RETURN <<61.0770, -1019.2720, 78.7800>>
								CASE 1			RETURN <<52.4060, -1016.3740, 82.4060>>
								CASE 2			RETURN <<61.9140, -1017.9940, 78.7870>>
								CASE 3			RETURN <<59.3110, -1010.1070, 78.8200>>
								CASE 4			RETURN <<68.5990, -1023.2030, 78.7690>>
								CASE 5			RETURN <<61.9140, -1029.6570, 78.7480>>
								CASE 6			RETURN <<63.1090, -1029.7600, 78.7500>>
								CASE 7			RETURN <<53.8740, -1023.2600, 78.7460>>
								CASE 8			RETURN <<55.6040, -1017.2010, 78.7710>>
								CASE 9			RETURN <<53.0670, -1022.3410, 78.7450>>
							ENDSWITCH
						BREAK
						CASE 2
							SWITCH iPed
								CASE 0			RETURN <<-1660.6530, -525.3390, 74.0570>>
								CASE 1			RETURN <<-1659.4570, -524.3190, 74.0570>>
								CASE 2			RETURN <<-1653.8340, -521.1790, 74.0580>>
								CASE 3			RETURN <<-1649.0520, -517.6470, 77.5070>>
								CASE 4			RETURN <<-1647.7560, -509.0180, 77.5070>>
								CASE 5			RETURN <<-1636.6010, -516.3880, 74.0580>>
								CASE 6			RETURN <<-1637.4740, -517.6260, 74.0580>>
								CASE 7			RETURN <<-1647.5280, -525.3600, 74.0580>>
								CASE 8			RETURN <<-1633.9220, -506.2300, 74.0590>>
								CASE 9			RETURN <<-1633.5601, -508.0890, 74.0590>>
							ENDSWITCH
						BREAK
						CASE 3
							SWITCH iPed
								CASE 0			RETURN <<-28.8850, -608.4240, 111.0040>>
								CASE 1			RETURN <<-27.7850, -607.2350, 111.0040>>
								CASE 2			RETURN <<-39.1180, -605.6990, 111.0040>>
								CASE 3			RETURN <<-37.0830, -589.0250, 105.3280>>
								CASE 4			RETURN <<-29.8330, -595.6880, 105.3280>>
								CASE 5			RETURN <<-23.7870, -593.0530, 105.3280>>
								CASE 6			RETURN <<-23.5170, -594.4440, 105.3280>>
								CASE 7			RETURN <<-31.0170, -582.1130, 105.3280>>
								CASE 8			RETURN <<-29.9310, -581.3490, 105.3280>>
								CASE 9			RETURN <<-20.8560, -585.1500, 105.3280>>
							ENDSWITCH
						BREAK
						CASE 4
							SWITCH iPed
								CASE 0			RETURN <<-712.7160, 699.5360, 169.0360>>
								CASE 1			RETURN <<-711.2120, 700.3310, 169.0360>>
								CASE 2			RETURN <<-703.3630, 692.5110, 169.0360>>
								CASE 3			RETURN <<-704.3230, 683.6450, 157.5870>>
								CASE 4			RETURN <<-704.5070, 682.0160, 157.5870>>
								CASE 5			RETURN <<-713.1810, 679.2910, 157.5870>>
								CASE 6			RETURN <<-708.1120, 672.4760, 157.5870>>
								CASE 7			RETURN <<-712.4990, 690.3010, 157.6360>>
								CASE 8			RETURN <<-713.8660, 691.4050, 157.6360>>
								CASE 9			RETURN <<-696.8790, 679.5350, 157.5870>>
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_BombRoof_Route3
					SWITCH iRespawnLocation
						CASE 0
							SWITCH iPed
								CASE 0			RETURN <<-640.5370, 322.2460, 139.1520>>
								CASE 1			RETURN <<-639.7630, 320.9360, 139.1520>>
								CASE 2			RETURN <<-653.3260, 317.2020, 139.1520>>
								CASE 3			RETURN <<-652.3020, 318.3850, 139.1520>>
								CASE 4			RETURN <<-661.1200, 312.3040, 139.1520>>
								CASE 5			RETURN <<-642.8670, 309.8920, 139.1520>>
								CASE 6			RETURN <<-645.2230, 316.6890, 139.1520>>
								CASE 7			RETURN <<-661.1570, 320.9580, 139.1520>>
								CASE 8			RETURN <<-661.5450, 322.1420, 139.1520>>
								CASE 9			RETURN <<-648.3120, 324.5960, 139.1520>>
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH iPed
								CASE 0			RETURN <<-235.7610, -836.6810, 124.2570>>
								CASE 1			RETURN <<-235.0480, -837.7940, 124.2570>>
								CASE 2			RETURN <<-218.4390, -837.9150, 124.2570>>
								CASE 3			RETURN <<-228.8140, -843.4180, 127.8910>>
								CASE 4			RETURN <<-229.3000, -844.7880, 127.8910>>
								CASE 5			RETURN <<-225.5910, -841.0450, 124.2570>>
								CASE 6			RETURN <<-219.3750, -830.1230, 125.7930>>
								CASE 7			RETURN <<-226.8360, -824.6290, 125.7930>>
								CASE 8			RETURN <<-226.0030, -835.9780, 124.2570>>
								CASE 9			RETURN <<-227.4870, -835.9100, 124.2570>>
							ENDSWITCH
						BREAK
						CASE 2
							SWITCH iPed
								CASE 0			RETURN <<767.0980, -1778.0200, 52.2920>>
								CASE 1			RETURN <<767.9400, -1776.8890, 52.2920>>
								CASE 2			RETURN <<753.8990, -1778.2070, 48.2920>>
								CASE 3			RETURN <<753.3490, -1785.9120, 48.2920>>
								CASE 4			RETURN <<765.6240, -1785.8420, 48.2920>>
								CASE 5			RETURN <<762.3230, -1789.1689, 48.2920>>
								CASE 6			RETURN <<761.5970, -1787.7140, 48.2920>>
								CASE 7			RETURN <<764.8460, -1772.5920, 52.2920>>
								CASE 8			RETURN <<757.1930, -1797.5830, 48.2920>>
								CASE 9			RETURN <<757.3550, -1795.9890, 48.2920>>
							ENDSWITCH
						BREAK
						CASE 3
							SWITCH iPed
								CASE 0			RETURN <<-817.8490, -716.6380, 123.2380>>
								CASE 1			RETURN <<-817.1430, -715.2660, 123.2380>>
								CASE 2			RETURN <<-818.9860, -709.2560, 123.2380>>
								CASE 3			RETURN <<-823.1450, -701.3550, 120.2730>>
								CASE 4			RETURN <<-820.5850, -722.4240, 120.2730>>
								CASE 5			RETURN <<-826.5810, -727.2540, 120.2730>>
								CASE 6			RETURN <<-836.3860, -720.4850, 120.2730>>
								CASE 7			RETURN <<-830.6270, -717.6020, 120.2720>>
								CASE 8			RETURN <<-829.2000, -717.8490, 120.2720>>
								CASE 9			RETURN <<-829.9290, -722.2610, 120.2730>>
							ENDSWITCH
						BREAK
						CASE 4
							SWITCH iPed
								CASE 0			RETURN <<304.5450, 121.9920, 144.1690>>
								CASE 1			RETURN <<307.5300, 118.2990, 144.1690>>
								CASE 2			RETURN <<303.5790, 112.9230, 144.1690>>
								CASE 3			RETURN <<311.1100, 111.1700, 144.1690>>
								CASE 4			RETURN <<312.1070, 112.2450, 144.1690>>
								CASE 5			RETURN <<318.9310, 107.0320, 144.1690>>
								CASE 6			RETURN <<306.0230, 130.0310, 144.1690>>
								CASE 7			RETURN <<307.4520, 129.5530, 144.1690>>
								CASE 8			RETURN <<312.9660, 123.3540, 144.1690>>
								CASE 9			RETURN <<326.7820, 104.7620, 144.1690>>
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_SELL_CONTESTED
			SWITCH eDropOff
				CASE SMUGGLER_DROPOFF_CONTESTED_1
					SWITCH iPed
						CASE 0			RETURN <<-1241.2450, -860.8860, 64.3310>>
						CASE 1			RETURN <<-1247.1670, -852.0880, 67.8580>>
						CASE 2			RETURN <<-1243.5970, -847.4130, 76.7200>>
						CASE 3			RETURN <<-1234.7520, -844.0440, 76.5370>>
						CASE 4			RETURN <<-1238.9580, -849.5620, 84.0960>>
						CASE 5			RETURN <<-1253.9480, -840.2360, 64.3310>>
						CASE 6			RETURN <<-1253.7310, -841.3550, 64.3310>>
						CASE 7			RETURN <<-1259.9250, -828.4940, 64.3310>>
						CASE 8			RETURN <<-1248.3831, -831.5640, 64.3310>>
						CASE 9			RETURN <<-1259.4940, -842.0320, 64.3310>>
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_2
					SWITCH iPed
						CASE 0			RETURN <<221.5159, -3177.2446, 39.5351>>
						CASE 1			RETURN <<229.0691, -3159.9866, 49.0724>>
						CASE 2			RETURN <<257.9539, -3167.6272, 43.7787>>
						CASE 3			RETURN <<238.3749, -3193.6406, 39.5346>>
						CASE 4			RETURN <<195.9496, -3165.0339, 42.2666>>
						CASE 5			RETURN <<193.1859, -3187.7744, 42.3897>>
						CASE 6			RETURN <<224.9519, -3155.6655, 64.0225>>
						CASE 7			RETURN <<263.1786, -3176.3186, 42.2673>>
						CASE 8			RETURN <<225.5325, -3193.2581, 39.5357>>
						CASE 9			RETURN <<224.0226, -3191.8992, 39.5358>>
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_3
					SWITCH iPed
						CASE 0			RETURN <<-148.0120, -1291.2531, 49.2980>>
						CASE 1			RETURN <<-143.0010, -1286.5510, 46.8980>>
						CASE 2			RETURN <<-135.5930, -1256.9010, 47.8840>>
						CASE 3			RETURN <<-138.8190, -1270.8101, 46.8980>>
						CASE 4			RETURN <<-151.2480, -1270.1300, 46.8980>>
						CASE 5			RETURN <<-153.8620, -1278.0510, 46.8980>>
						CASE 6			RETURN <<-153.7460, -1279.3990, 46.8980>>
						CASE 7			RETURN <<-122.8970, -1259.8130, 49.6320>>
						CASE 8			RETURN <<-142.4550, -1291.3910, 51.2080>>
						CASE 9			RETURN <<-130.4250, -1283.5010, 46.8980>>
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_4
					SWITCH iPed
						CASE 0			RETURN <<911.6420, -1703.2610, 56.1340>>
						CASE 1			RETURN <<904.0347, -1698.9255, 50.1350>>
						CASE 2			RETURN <<918.6627, -1681.3534, 50.1266>>
						CASE 3			RETURN <<919.4840, -1667.9310, 50.1280>>
						CASE 4			RETURN <<909.8690, -1688.6460, 50.1310>>
						CASE 5			RETURN <<911.6820, -1688.1830, 50.1340>>
						CASE 6			RETURN <<905.2239, -1667.8531, 50.1239>>
						CASE 7			RETURN <<902.2250, -1688.5007, 50.1280>>
						CASE 8			RETURN <<917.1311, -1698.5657, 50.1266>>
						CASE 9			RETURN <<902.4190, -1679.3010, 50.1270>>
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_5
					SWITCH iPed
						CASE 0			RETURN <<919.4190, -936.7380, 58.0920>>
						CASE 1			RETURN <<921.7120, -922.2790, 58.0910>>
						CASE 2			RETURN <<931.6100, -915.9850, 67.9830>>
						CASE 3			RETURN <<941.1240, -923.6700, 67.9830>>
						CASE 4			RETURN <<945.9230, -939.4610, 58.0920>>
						CASE 5			RETURN <<945.5520, -938.0200, 58.0920>>
						CASE 6			RETURN <<953.1410, -915.5680, 58.0890>>
						CASE 7			RETURN <<960.6750, -942.4800, 58.0920>>
						CASE 8			RETURN <<943.2290, -948.0660, 58.0930>>
						CASE 9			RETURN <<932.9600, -948.1690, 58.0930>>
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_6
					SWITCH iPed
						CASE 0			RETURN <<86.3750, -356.2980, 66.2020>>
						CASE 1			RETURN <<87.3440, -355.1820, 66.2020>>
						CASE 2			RETURN <<77.0990, -344.4160, 66.2020>>
						CASE 3			RETURN <<78.1250, -344.3410, 66.2020>>
						CASE 4			RETURN <<91.4120, -344.5040, 74.5670>>
						CASE 5			RETURN <<84.4040, -342.0190, 74.5670>>
						CASE 6			RETURN <<95.2960, -332.6120, 74.5670>>
						CASE 7			RETURN <<70.6190, -353.0660, 66.2020>>
						CASE 8			RETURN <<91.6250, -350.2230, 66.2020>>
						CASE 9			RETURN <<86.5850, -328.4040, 74.5660>>
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_7
					SWITCH iPed
						CASE 0			RETURN <<-1554.7970, 206.3110, 74.5010>>
						CASE 1			RETURN <<-1571.7880, 232.0510, 74.5010>>
						CASE 2			RETURN <<-1602.0640, 208.6970, 74.5010>>
						CASE 3			RETURN <<-1580.3781, 185.4620, 74.5010>>
						CASE 4			RETURN <<-1557.9297, 220.2588, 73.3379>>
						CASE 5			RETURN <<-1592.2454, 191.2693, 73.3379>>
						CASE 6			RETURN <<-1570.8700, 209.6450, 73.3380>>
						CASE 7			RETURN <<-1569.8020, 210.3210, 73.3380>>
						CASE 8			RETURN <<-1569.4164, 195.1552, 74.5253>>
						CASE 9			RETURN <<-1587.0170, 223.8120, 73.3380>>
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_8
					SWITCH iPed
						CASE 0			RETURN <<-2257.5444, 404.0342, 187.6023>>
						CASE 1			RETURN <<-2274.2856, 388.3651, 187.6023>>
						CASE 2			RETURN <<-2249.0339, 359.2610, 187.6020>>
						CASE 3			RETURN <<-2269.9080, 369.7770, 187.6020>>
						CASE 4			RETURN <<-2269.9890, 371.0300, 187.6020>>
						CASE 5			RETURN <<-2260.0691, 347.7140, 191.6020>>
						CASE 6			RETURN <<-2267.6899, 359.8530, 191.6010>>
						CASE 7			RETURN <<-2280.9900, 364.9280, 191.6010>>
						CASE 8			RETURN <<-2249.9670, 385.7570, 187.6020>>
						CASE 9			RETURN <<-2263.6326, 380.5144, 187.6023>>
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_9
					SWITCH iPed
						CASE 0			RETURN <<-729.5340, -225.2520, 47.5190>>
						CASE 1			RETURN <<-737.2550, -228.4090, 47.5190>>
						CASE 2			RETURN <<-760.6150, -222.6280, 52.0930>>
						CASE 3			RETURN <<-753.9800, -233.3790, 50.1030>>
						CASE 4			RETURN <<-746.8280, -227.3640, 50.7190>>
						CASE 5			RETURN <<-751.0930, -217.1870, 52.0930>>
						CASE 6			RETURN <<-748.8170, -217.2570, 47.5190>>
						CASE 7			RETURN <<-736.5020, -213.1700, 47.5190>>
						CASE 8			RETURN <<-743.0710, -227.3390, 47.5190>>
						CASE 9			RETURN <<-741.9290, -227.0000, 47.5190>>
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_10
					SWITCH iPed
						CASE 0			RETURN <<43.4380, -1770.5031, 46.7003>>
						CASE 1			RETURN <<41.8568, -1770.1423, 46.7003>>
						CASE 2			RETURN <<57.6980, -1756.6964, 46.7003>>
						CASE 3			RETURN <<55.3729, -1781.2354, 46.7003>>
						CASE 4			RETURN <<44.8820, -1794.0090, 46.7000>>
						CASE 5			RETURN <<38.2896, -1762.8362, 46.7003>>
						CASE 6			RETURN <<43.1372, -1755.6052, 46.7003>>
						CASE 7			RETURN <<66.5130, -1766.5549, 46.7003>>
						CASE 8			RETURN <<25.3873, -1778.4771, 46.7003>>
						CASE 9			RETURN <<35.9339, -1788.0292, 46.7003>>
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_11
					SWITCH iPed
						CASE 0			RETURN <<807.3306, -2506.7488, 43.5287>>
						CASE 1			RETURN <<805.1691, -2530.1609, 43.6459>>
						CASE 2			RETURN <<852.7040, -2527.8660, 39.5250>>
						CASE 3			RETURN <<853.9690, -2527.1560, 39.5250>>
						CASE 4			RETURN <<829.8645, -2530.7913, 39.5252>>
						CASE 5			RETURN <<815.3445, -2511.8596, 39.5252>>
						CASE 6			RETURN <<858.3340, -2535.0129, 51.3040>>
						CASE 7			RETURN <<859.4110, -2512.6270, 51.3020>>
						CASE 8			RETURN <<842.6375, -2525.0676, 39.5252>>
						CASE 9			RETURN <<845.2700, -2506.0930, 45.6290>>
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_12
					SWITCH iPed
						CASE 0			RETURN <<-415.9530, -351.5470, 75.8030>>
						CASE 1			RETURN <<-412.4012, -331.4430, 74.5519>>
						CASE 2			RETURN <<-378.2829, -362.2560, 75.8033>>
						CASE 3			RETURN <<-369.0065, -352.5716, 75.8033>>
						CASE 4			RETURN <<-410.6800, -318.1270, 75.8030>>
						CASE 5			RETURN <<-394.7360, -327.9260, 74.5520>>
						CASE 6			RETURN <<-398.1195, -358.1539, 74.5519>>
						CASE 7			RETURN <<-371.8193, -342.3575, 75.8033>>
						CASE 8			RETURN <<-393.9800, -347.8469, 69.9677>>
						CASE 9			RETURN <<-392.8957, -346.4381, 69.9677>>
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_13
					SWITCH iPed
						CASE 0			RETURN <<84.6820, -926.1440, 84.2890>>
						CASE 1			RETURN <<84.4750, -924.8010, 84.2890>>
						CASE 2			RETURN <<68.6492, -933.1027, 86.6149>>
						CASE 3			RETURN <<70.0330, -913.3860, 90.2240>>
						CASE 4			RETURN <<59.5230, -909.8080, 90.2240>>
						CASE 5			RETURN <<73.2373, -882.8674, 87.2874>>
						CASE 6			RETURN <<40.3994, -953.5738, 86.6149>>
						CASE 7			RETURN <<114.4019, -910.8043, 86.6149>>
						CASE 8			RETURN <<84.1356, -964.3251, 87.2874>>
						CASE 9			RETURN <<55.8870, -922.1110, 90.2240>>
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_14
					SWITCH iPed
						CASE 0			RETURN <<200.2620, 249.8170, 140.4780>>
						CASE 1			RETURN <<193.7570, 256.7710, 140.4780>>
						CASE 2			RETURN <<192.9930, 257.9080, 140.4780>>
						CASE 3			RETURN <<178.7133, 241.7415, 140.4780>>
						CASE 4			RETURN <<195.7806, 265.6559, 140.4780>>
						CASE 5			RETURN <<210.8017, 280.0156, 140.4780>>
						CASE 6			RETURN <<196.6735, 278.4822, 144.2149>>
						CASE 7			RETURN <<193.1881, 218.8174, 142.9514>>
						CASE 8			RETURN <<167.5761, 232.6193, 140.4780>>
						CASE 9			RETURN <<178.8973, 264.8475, 140.6698>>
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_15
					SWITCH iPed
						CASE 0			RETURN <<1233.3771, -2992.5781, 11.1643>>
						CASE 1			RETURN <<1234.2971, -2991.5215, 11.1643>>
						CASE 2			RETURN <<1252.4178, -2976.9160, 14.0164>>
						CASE 3			RETURN <<1229.1168, -2973.5205, 14.0068>>
						CASE 4			RETURN <<1228.2269, -2999.2625, 11.1892>>
						CASE 5			RETURN <<1252.7341, -2989.4634, 11.1892>>
						CASE 6			RETURN <<1220.3927, -2950.7197, 23.3933>>
						CASE 7			RETURN <<1220.5634, -2943.9790, 19.2356>>
						CASE 8			RETURN <<1242.5059, -2972.6089, 13.9850>>
						CASE 9			RETURN <<1249.1902, -2999.9133, 11.1643>>
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_16
					SWITCH iPed
						CASE 0			RETURN <<-150.0460, 6181.6782, 39.1640>>
						CASE 1			RETURN <<-136.6140, 6196.4082, 41.7450>>
						CASE 2			RETURN <<-133.2960, 6182.8818, 39.1650>>
						CASE 3			RETURN <<-134.8720, 6183.4761, 39.1650>>
						CASE 4			RETURN <<-132.9980, 6157.5000, 39.1650>>
						CASE 5			RETURN <<-118.2430, 6172.2549, 39.1660>>
						CASE 6			RETURN <<-117.9770, 6186.2271, 41.7450>>
						CASE 7			RETURN <<-150.2630, 6159.2842, 45.0330>>
						CASE 8			RETURN <<-169.0510, 6179.5000, 42.0150>>
						CASE 9			RETURN <<-126.2756, 6206.0332, 39.1642>>
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_17
					SWITCH iPed
						CASE 0			RETURN <<2752.1589, 3480.2920, 72.7040>>
						CASE 1			RETURN <<2740.5820, 3453.2129, 72.7040>>
						CASE 2			RETURN <<2734.3450, 3439.8611, 72.7040>>
						CASE 3			RETURN <<2722.4629, 3476.8899, 72.7040>>
						CASE 4			RETURN <<2736.8201, 3506.8779, 72.7040>>
						CASE 5			RETURN <<2758.3779, 3498.6121, 72.7040>>
						CASE 6			RETURN <<2736.8459, 3468.0559, 72.7040>>
						CASE 7			RETURN <<2738.5659, 3468.1580, 72.7040>>
						CASE 8			RETURN <<2720.7820, 3444.3821, 72.7040>>
						CASE 9			RETURN <<2750.8391, 3510.9719, 72.7040>>
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_18
					SWITCH iPed
						CASE 0			RETURN <<1717.5820, 4779.5640, 46.5800>>
						CASE 1			RETURN <<1718.0590, 4778.1250, 46.5800>>
						CASE 2			RETURN <<1715.7280, 4817.1851, 57.5300>>
						CASE 3			RETURN <<1697.6570, 4774.6958, 46.5920>>
						CASE 4			RETURN <<1699.4700, 4788.4492, 46.5910>>
						CASE 5			RETURN <<1724.2830, 4787.6519, 46.5760>>
						CASE 6			RETURN <<1722.7950, 4762.0708, 46.5770>>
						CASE 7			RETURN <<1724.2460, 4777.1538, 46.5760>>
						CASE 8			RETURN <<1713.7650, 4788.5742, 46.5830>>
						CASE 9			RETURN <<1710.5400, 4762.2729, 46.5850>>
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_19
					SWITCH iPed
						CASE 0			RETURN <<594.7780, 2759.0649, 50.1450>>
						CASE 1			RETURN <<593.6650, 2757.8669, 50.1450>>
						CASE 2			RETURN <<615.9730, 2746.3740, 47.5290>>
						CASE 3			RETURN <<599.4030, 2768.5400, 50.1450>>
						CASE 4			RETURN <<609.3880, 2782.1841, 47.5290>>
						CASE 5			RETURN <<587.2140, 2775.9839, 50.1450>>
						CASE 6			RETURN <<574.0570, 2788.0090, 47.5290>>
						CASE 7			RETURN <<581.4450, 2765.3420, 50.1450>>
						CASE 8			RETURN <<576.3750, 2744.0601, 47.5290>>
						CASE 9			RETURN <<593.9830, 2745.4890, 50.1450>>
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_20
					SWITCH iPed
						CASE 0			RETURN <<740.7170, 1272.8621, 382.1710>>
						CASE 1			RETURN <<717.5440, 1294.9091, 362.3350>>
						CASE 2			RETURN <<742.7160, 1269.4620, 382.1710>>
						CASE 3			RETURN <<756.1300, 1315.8190, 361.8970>>
						CASE 4			RETURN <<739.4110, 1286.9670, 359.2970>>
						CASE 5			RETURN <<738.3280, 1286.4270, 359.2970>>
						CASE 6			RETURN <<755.0700, 1286.4790, 359.2970>>
						CASE 7			RETURN <<733.5750, 1270.7930, 359.2960>>
						CASE 8			RETURN <<721.3450, 1280.2480, 359.2960>>
						CASE 9			RETURN <<756.4850, 1295.4230, 365.6420>>
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_21
					SWITCH iPed
						CASE 0			RETURN <<-1121.4900, 2709.7520, 22.8410>>
						CASE 1			RETURN <<-1129.7930, 2702.5391, 21.7270>>
						CASE 2			RETURN <<-1105.1920, 2702.1841, 22.8410>>
						CASE 3			RETURN <<-1104.7850, 2703.8689, 22.8410>>
						CASE 4			RETURN <<-1123.8910, 2683.2371, 22.8620>>
						CASE 5			RETURN <<-1108.9871, 2695.7319, 22.8410>>
						CASE 6			RETURN <<-1108.0280, 2720.4780, 21.9870>>
						CASE 7			RETURN <<-1094.7670, 2714.7590, 22.8920>>
						CASE 8			RETURN <<-1110.7111, 2711.2781, 22.8410>>
						CASE 9			RETURN <<-1133.4900, 2683.8979, 22.8620>>
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_22
					SWITCH iPed
						CASE 0			RETURN <<1512.0715, 3578.0125, 41.1119>>
						CASE 1			RETURN <<1520.8658, 3569.8887, 41.1119>>
						CASE 2			RETURN <<1536.5065, 3579.2239, 40.9293>>
						CASE 3			RETURN <<1505.9810, 3589.0903, 37.9928>>
						CASE 4			RETURN <<1495.3202, 3591.0564, 37.9223>>
						CASE 5			RETURN <<1502.4833, 3577.8499, 41.1119>>
						CASE 6			RETURN <<1527.5436, 3582.1616, 41.0019>>
						CASE 7			RETURN <<1512.2028, 3579.5725, 41.1050>>
						CASE 8			RETURN <<1523.7448, 3589.8330, 41.1119>>
						CASE 9			RETURN <<1509.0139, 3566.4512, 41.1167>>
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_23
					SWITCH iPed
						CASE 0			RETURN <<-1584.4879, 767.3894, 193.7737>>
						CASE 1			RETURN <<-1594.2323, 754.8381, 188.1942>>
						CASE 2			RETURN <<-1596.0197, 755.4628, 188.2431>>
						CASE 3			RETURN <<-1606.0719, 778.0377, 188.2431>>
						CASE 4			RETURN <<-1617.6896, 769.1016, 188.2431>>
						CASE 5			RETURN <<-1591.1614, 784.3525, 188.1931>>
						CASE 6			RETURN <<-1610.7322, 746.0071, 188.1942>>
						CASE 7			RETURN <<-1568.5365, 792.2070, 192.7230>>
						CASE 8			RETURN <<-1612.5568, 775.4239, 188.2431>>
						CASE 9			RETURN <<-1615.6300, 756.1089, 188.2431>>
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_24
					SWITCH iPed
						CASE 0			RETURN <<256.2220, 3614.6331, 32.8060>>
						CASE 1			RETURN <<257.0080, 3613.4080, 32.8750>>
						CASE 2			RETURN <<242.4580, 3579.4929, 32.9510>>
						CASE 3			RETURN <<272.8060, 3595.4790, 33.4780>>
						CASE 4			RETURN <<276.3350, 3614.8911, 32.3530>>
						CASE 5			RETURN <<264.6960, 3629.1040, 32.6940>>
						CASE 6			RETURN <<242.2770, 3627.2590, 32.2670>>
						CASE 7			RETURN <<230.2780, 3608.5649, 31.9840>>
						CASE 8			RETURN <<246.4110, 3600.2920, 33.1570>>
						CASE 9			RETURN <<254.9440, 3587.2520, 33.3670>>
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_25
					SWITCH iPed
						CASE 0			RETURN <<163.1630, 7351.4009, 10.1810>>
						CASE 1			RETURN <<163.1610, 7352.5928, 10.3720>>
						CASE 2			RETURN <<139.3410, 7364.2700, 10.9840>>
						CASE 3			RETURN <<124.1560, 7344.5518, 7.1910>>
						CASE 4			RETURN <<150.2510, 7345.7090, 9.4360>>
						CASE 5			RETURN <<161.0940, 7362.2422, 11.3160>>
						CASE 6			RETURN <<180.9120, 7349.2578, 9.3800>>
						CASE 7			RETURN <<165.4470, 7394.6309, 12.2330>>
						CASE 8			RETURN <<179.5530, 7370.3262, 10.2570>>
						CASE 9			RETURN <<151.6780, 7323.9878, 7.2470>>
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_26
					SWITCH iPed
						CASE 0			RETURN <<2942.8711, 781.6350, 25.2720>>
						CASE 1			RETURN <<2940.3560, 781.6290, 25.3870>>
						CASE 2			RETURN <<2933.5161, 750.9500, 20.3790>>
						CASE 3			RETURN <<2955.5911, 775.5110, 22.5170>>
						CASE 4			RETURN <<2969.7839, 802.1480, 22.6180>>
						CASE 5			RETURN <<2950.6670, 817.0280, 21.9710>>
						CASE 6			RETURN <<2934.5559, 802.0340, 23.7710>>
						CASE 7			RETURN <<2912.8230, 790.9460, 24.6690>>
						CASE 8			RETURN <<2911.8940, 767.5180, 21.2630>>
						CASE 9			RETURN <<2974.7390, 818.7870, 19.3590>>
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_27
					SWITCH iPed
						CASE 0			RETURN <<3630.8130, 5684.4521, 6.8130>>
						CASE 1			RETURN <<3664.8511, 5640.5059, 10.0010>>
						CASE 2			RETURN <<3673.7100, 5654.5239, 9.0560>>
						CASE 3			RETURN <<3610.1741, 5693.5898, 4.5650>>
						CASE 4			RETURN <<3658.0098, 5659.6875, 9.5492>>
						CASE 5			RETURN <<3632.1897, 5662.7036, 7.2817>>
						CASE 6			RETURN <<3633.7815, 5662.2163, 7.4439>>
						CASE 7			RETURN <<3618.0222, 5671.4199, 5.7899>>
						CASE 8			RETURN <<3597.4146, 5685.0889, 2.8724>>
						CASE 9			RETURN <<3605.3198, 5668.0527, 3.2150>>
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_28
					SWITCH iPed
						CASE 0			RETURN <<2372.7710, 6623.1138, 2.0380>>
						CASE 1			RETURN <<2371.6089, 6621.9302, 2.0980>>
						CASE 2			RETURN <<2366.8330, 6597.8271, 0.5810>>
						CASE 3			RETURN <<2342.0190, 6615.3921, 15.9280>>
						CASE 4			RETURN <<2424.1780, 6599.4941, 30.8030>>
						CASE 5			RETURN <<2365.5310, 6660.9482, 0.8210>>
						CASE 6			RETURN <<2399.2991, 6634.8232, 7.3900>>
						CASE 7			RETURN <<2421.9070, 6631.2148, 6.7800>>
						CASE 8			RETURN <<2382.4961, 6635.9619, 1.4410>>
						CASE 9			RETURN <<2345.3181, 6641.1089, 4.5030>>
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_29
					SWITCH iPed
						CASE 0			RETURN <<-2583.9700, 1884.8400, 170.4910>>
						CASE 1			RETURN <<-2584.6970, 1886.4010, 170.4910>>
						CASE 2			RETURN <<-2602.4619, 1880.0100, 170.4930>>
						CASE 3			RETURN <<-2591.2839, 1868.0770, 170.4930>>
						CASE 4			RETURN <<-2572.8979, 1883.8770, 170.4930>>
						CASE 5			RETURN <<-2604.5540, 1923.3680, 171.5970>>
						CASE 6			RETURN <<-2585.3220, 1927.5040, 170.4930>>
						CASE 7			RETURN <<-2581.8191, 1897.2020, 170.4920>>
						CASE 8			RETURN <<-2601.1899, 1895.4600, 170.4930>>
						CASE 9			RETURN <<-2597.7019, 1933.2860, 169.7240>>
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_30
					SWITCH iPed
						CASE 0			RETURN <<-498.2310, 5258.7661, 91.6630>>
						CASE 1			RETURN <<-521.2550, 5258.9722, 79.6000>>
						CASE 2			RETURN <<-510.7480, 5261.9521, 79.6100>>
						CASE 3			RETURN <<-489.6850, 5264.7021, 85.8860>>
						CASE 4			RETURN <<-496.1210, 5248.5000, 85.7860>>
						CASE 5			RETURN <<-484.8160, 5278.1768, 85.8640>>
						CASE 6			RETURN <<-488.3310, 5308.8721, 88.6480>>
						CASE 7			RETURN <<-512.6040, 5303.4468, 79.2450>>
						CASE 8			RETURN <<-537.6410, 5289.3550, 89.6510>>
						CASE 9			RETURN <<-504.6730, 5292.4189, 81.7350>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_SELL_FLY_LOW
			SWITCH eDropOff
				CASE SMUGGLER_DROPOFF_FLY_LOW_1
					RETURN <<-230.1460, -2452.3789, 5.0010>>
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_2
					RETURN <<612.5220, -846.2160, 9.9140>>
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_3
					RETURN <<2910.9844, 764.6005, 20.4908>>
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_4
					RETURN <<-175.1900, 2859.2920, 31.0940>>
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_5
					RETURN <<-1407.5260, 2630.1870, 0.6680>>
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_6
					RETURN <<-2663.9419, 2604.5039, 0.5220>>
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_7
					RETURN <<-1949.0031, 4581.4048, 1.6290>>
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_8
					RETURN <<-508.9590, 4411.9512, 30.7830>>
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_9
					RETURN <<798.8030, -2640.9871, 0.4730>>
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_10
					RETURN <<2267.0640, 2042.6880, 127.7470>>
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_11
					RETURN <<1665.6000, -1632.7920, 111.3540>>
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_12
					RETURN <<-1855.7061, -333.4560, 56.0770>>
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_13
					RETURN <<1981.7710, 712.4120, 164.1020>>
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_14
					RETURN <<-26.1960, 657.5670, 197.2900>>
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_15
					RETURN <<-211.4950, -1807.9620, 0.4620>>
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_16
					RETURN <<320.9580, 2209.0220, 74.7520>>
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_17
					RETURN <<1969.1880, 5042.9600, 40.0540>>
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_18
					RETURN <<2031.2939, 2938.9451, 46.7980>>
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_19
					RETURN <<80.4080, 7078.6221, 0.9720>>
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_20
					RETURN <<3855.7849, 4366.4238, 5.5760>>
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_21
					RETURN <<2916.9431, 5310.4971, 95.1440>>
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_22
					RETURN <<202.0375, -733.0214, 46.0770>>
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_23
					RETURN <<709.6810, 4110.0239, 30.5270>>
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_24
					RETURN <<1090.6970, 62.8450, 79.8910>>
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_25
					RETURN <<-2829.1689, 1420.0820, 99.9070>>
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_26
					RETURN <<-1376.0450, 5351.4922, 1.9170>>
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_27
					RETURN <<-1342.7710, -1431.6281, 3.3150>>
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_28
					RETURN <<2468.3069, 3769.0681, 40.4290>>
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_29
					RETURN <<-965.2510, -974.5110, 2.8570>>
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_30
					RETURN <<630.5140, 630.2900, 127.9110>>
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_31
					RETURN <<-1045.1750, 924.0750, 168.5500>>
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_32
					RETURN <<-1185.3340, 45.4540, 52.0470>>
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_33
					RETURN <<1450.3260, 6347.3862, 22.7830>>
				
			ENDSWITCH
		BREAK
		
		CASE SMV_INFILTRATION		
			SWITCH eLocation
				CASE SmugglerLocation_Infiltration_HumaneLabs
					SWITCH iPed
						CASE 0			RETURN <<3554.9690, 3656.6201, 32.8890>>
						CASE 1			RETURN <<3554.4209, 3658.5649, 32.8890>>
						CASE 2			RETURN <<3546.9280, 3673.2490, 35.1770>>
						CASE 3			RETURN <<3570.4399, 3677.3379, 40.0070>>
						CASE 4			RETURN <<3564.6079, 3668.0720, 32.9270>>
						CASE 5			RETURN <<3584.5146, 3696.2656, 35.6428>>
						CASE 6			RETURN <<3541.9380, 3670.8311, 32.8890>>
						CASE 7			RETURN <<3555.6230, 3649.0239, 45.0330>>
						CASE 8			RETURN <<3615.5022, 3705.7405, 42.4911>>
						CASE 9			RETURN <<3537.7185, 3714.2944, 35.6426>>
						CASE 10			RETURN <<3527.6599, 3737.2776, 35.7308>>
						CASE 11			RETURN <<3577.1479, 3658.6460, 32.8990>>
						CASE 12			RETURN <<3559.5249, 3646.3789, 40.3400>>
						CASE 13			RETURN <<3540.8010, 3658.9509, 32.8890>>
						CASE 14			RETURN <<3584.1880, 3681.2681, 40.0070>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Infiltration_BolingbrokePrison
					SWITCH iPed
						CASE 0			RETURN <<1803.6864, 2610.1611, 44.5703>>
						CASE 1			RETURN <<1803.0200, 2610.9141, 44.5720>>
						CASE 2			RETURN <<1816.4915, 2602.6572, 44.5967>>
						CASE 3			RETURN <<1703.2910, 2610.0920, 44.5650>>
						CASE 4			RETURN <<1678.1202, 2607.7178, 44.5649>>
						CASE 5			RETURN <<1718.6387, 2615.9607, 44.5649>>
						CASE 6			RETURN <<1721.7976, 2595.4514, 54.4480>>
						CASE 7			RETURN <<1748.2090, 2613.9309, 54.4480>>
						CASE 8			RETURN <<1758.9060, 2615.0330, 44.5650>>
						CASE 9			RETURN <<1757.8870, 2614.0840, 44.5650>>
						CASE 10			RETURN <<1776.0560, 2600.1670, 44.5650>>
						CASE 11			RETURN <<1758.8451, 2599.0313, 54.4479>>
						CASE 12			RETURN <<1732.3331, 2603.1353, 44.5649>>
						CASE 13			RETURN <<1732.5511, 2604.1782, 44.5649>>
						CASE 14			RETURN <<1656.7780, 2601.6609, 44.5650>>
						CASE 15			RETURN <<1655.2925, 2601.1982, 44.5649>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Infiltration_Zancudo
					SWITCH iPed
						CASE 0			RETURN <<-1977.7560, 3295.1550, 39.2160>>
						CASE 1			RETURN <<-1932.1890, 3343.3870, 39.2270>>
						CASE 2			RETURN <<-1936.3400, 3332.0950, 34.4160>>
						CASE 3			RETURN <<-1956.4280, 3298.4851, 31.9600>>
						CASE 4			RETURN <<-1952.4753, 3331.7998, 31.9602>>
						CASE 5			RETURN <<-1953.7706, 3333.0857, 31.9602>>
						CASE 6			RETURN <<-1970.2975, 3319.2671, 31.9603>>
						CASE 7			RETURN <<-1970.1691, 3321.6311, 31.9603>>
						CASE 8			RETURN <<-1960.4150, 3356.9502, 31.9603>>
						CASE 9			RETURN <<-1962.6656, 3312.2817, 31.9602>>
						CASE 10			RETURN <<-1946.5280, 3313.0161, 31.9600>>
						CASE 11			RETURN <<-1921.9680, 3346.3940, 39.1610>>
						CASE 12			RETURN <<-1946.1185, 3357.3948, 31.9371>>
						CASE 13			RETURN <<-1975.6790, 3343.3333, 32.0184>>
						CASE 14			RETURN <<-1981.7000, 3304.7920, 39.1610>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_SETUP_RON
			SWITCH eLocation
				CASE SmugglerLocation_Setup_DelPerroBeach 
					SWITCH iPed
						CASE 0			RETURN <<-1790.9301, -850.3152, 6.7606>>
						CASE 1			RETURN <<-1789.3066, -851.5523, 6.8008>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_LSRiver
					SWITCH iPed
						CASE 0			RETURN <<612.6701, -542.7713, 14.2353>>
						CASE 1			RETURN <<614.1082, -542.3696, 14.2096>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_ElBurro
					SWITCH iPed
						CASE 0			RETURN <<1744.5270, -1552.6510, 111.6410>>
						CASE 1			RETURN <<1744.6090, -1553.9030, 111.6450>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_Chumash
					SWITCH iPed
						CASE 0			RETURN <<-3286.9302, 994.4673, 2.4803>>
						CASE 1			RETURN <<-3288.4001, 994.0409, 2.3186>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_Tutuavium
					SWITCH iPed
						CASE 0			RETURN <<2525.7617, 1027.0326, 76.4320>>
						CASE 1			RETURN <<2526.5225, 1025.3094, 76.4609>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_RonsWindFarm
					SWITCH iPed
						CASE 0			RETURN <<2432.6914, 1466.1274, 37.3969>>
						CASE 1			RETURN <<2432.2397, 1464.6962, 37.5130>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_SenoraDesert1
					SWITCH iPed
						CASE 0			RETURN <<1362.6135, 1411.7999, 102.3359>>
						CASE 1			RETURN <<1363.9839, 1411.3595, 102.3687>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_SenoraDesert2
					SWITCH iPed
						CASE 0			RETURN <<315.1063, 2424.0776, 47.2845>>
						CASE 1			RETURN <<316.2569, 2424.4243, 47.3108>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_SandyShores
					SWITCH iPed
						CASE 0			RETURN <<1191.4181, 3078.1978, 39.9078>>
						CASE 1			RETURN <<1192.7694, 3078.7400, 39.9078>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_StabCity
					SWITCH iPed
						CASE 0			RETURN <<107.5576, 3655.8435, 38.7549>>
						CASE 1			RETURN <<108.8687, 3656.7449, 38.7072>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_NCalafiaWay
					SWITCH iPed
						CASE 0			RETURN <<1006.0247, 4355.0151, 42.9170>>
						CASE 1			RETURN <<1006.4970, 4356.0508, 42.8266>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_McKenzie
					SWITCH iPed
						CASE 0			RETURN <<2025.5170, 4787.8208, 40.7012>>
						CASE 1			RETURN <<2026.6233, 4788.7798, 40.7303>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_Grapeseed
					SWITCH iPed
						CASE 0			RETURN <<2653.9087, 4232.7793, 42.3191>>
						CASE 1			RETURN <<2655.4548, 4232.9009, 42.2673>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_ProcopioBeach
					SWITCH iPed
						CASE 0			RETURN <<758.6412, 6588.3745, 1.3421>>
						CASE 1			RETURN <<759.8186, 6587.4229, 1.3243>>
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_Paleto
					SWITCH iPed
						CASE 0			RETURN <<-335.4713, 6464.4692, 1.0265>>
						CASE 1			RETURN <<-336.8973, 6464.6035, 1.0551>>
					ENDSWITCH
				BREAK

			ENDSWITCH
		BREAK		
		
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC FLOAT GET_SMUGGLER_PED_SPAWN_HEADING(INT iPed, SMUGGLER_VARIATION eVariation, SMUGGLER_LOCATION eLocation, FREEMODE_DELIVERY_DROPOFFS eDropOff = FREEMODE_DELIVERY_DROPOFF_INVALID, INT iRespawnLocation = 0)
	SWITCH eVariation
		CASE SMV_STEAL_AIRCRAFT			
			SWITCH eLocation
				CASE SmugglerLocation_StealAircraft_SandyShores
					SWITCH iPed
						CASE 0			RETURN 193.9980
						CASE 1			RETURN 114.7980
						CASE 2			RETURN 33.5980
						CASE 3			RETURN 41.9980
						CASE 4			RETURN 212.7980
						CASE 5			RETURN 323.7970
						CASE 6			RETURN 141.9970
						CASE 7			RETURN 328.7980
						CASE 8			RETURN 150.3980
						CASE 9			RETURN 183.9960
						CASE 10			RETURN 351.9976
						CASE 11			RETURN 171.5968
						CASE 12			RETURN 290.3968
						CASE 13			RETURN 105.5965
						CASE 14			RETURN 355.1956
						CASE 15			RETURN 249.5953
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_StealAircraft_McKenzieAirfield
					SWITCH iPed
						CASE 0			RETURN 25.9970
						CASE 1			RETURN 326.5960
						CASE 2			RETURN 134.1960
						CASE 3			RETURN 39.7990
						CASE 4			RETURN 219.1990
						CASE 5			RETURN 118.7990
						CASE 6			RETURN 310.5980
						CASE 7			RETURN 133.7970
						CASE 8			RETURN 281.9970
						CASE 9			RETURN 284.9970
						CASE 10			RETURN 290.9969
						CASE 11			RETURN 118.1959
						CASE 12			RETURN 73.9966
						CASE 13			RETURN 110.3964
						CASE 14			RETURN 86.1963
						CASE 15			RETURN 265.1954
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_StealAircraft_EpsilonHQ
					SWITCH iPed
						CASE 0			RETURN 101.7990
						CASE 1			RETURN 308.5990
						CASE 2			RETURN 227.7980
						CASE 3			RETURN 9.9980
						CASE 4			RETURN 268.7980
						CASE 5			RETURN 179.7980
						CASE 6			RETURN 169.3970
						CASE 7			RETURN 231.3970
						CASE 8			RETURN 176.5960
						CASE 9			RETURN 354.5960
						CASE 10			RETURN 23.7977
						CASE 11			RETURN 359.1973
						CASE 12			RETURN 202.7967
						CASE 13			RETURN 352.3976
						CASE 14			RETURN 31.1986
						CASE 15			RETURN 52.1986
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE SMV_BEACON_GRAB				
			SWITCH eLocation
				CASE SmugglerLocation_BeaconGrab_Location1
					SWITCH iPed
						CASE 0			RETURN 0.0
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_SPLASH_LANDING
			SWITCH eLocation
				CASE SmugglerLocation_SplashLanding_LagoZancudo
					SWITCH iPed
						// Peds 0-5 are created in vehicles		
						
						// Peds 6 and 7 are on the barge
						CASE 6			RETURN 153.3984
						CASE 7			RETURN 336.9981
						
						// Peds 8-10 are created in vehicles	
						CASE 11			RETURN 135.2581
						CASE 12			RETURN 989.4000
						CASE 13			RETURN 139.4581
						CASE 14			RETURN 46.7581
						CASE 15			RETURN -21.0419
						CASE 16			RETURN 135.2581
						CASE 17			RETURN 989.4000
						CASE 18			RETURN 139.4581
						CASE 19			RETURN 46.7581
						CASE 20			RETURN -21.0419
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_SplashLanding_PacificOceanE
					SWITCH iPed
						// Peds 0-5 are created in vehicles		
						
						// Peds 6 and 7 are on the barge
						CASE 6			RETURN 156.3995
						CASE 7			RETURN 334.3990
						
						// Peds 8-10 are created in vehicles	
						CASE 11			RETURN 135.2581
						CASE 12			RETURN 989.4000
						CASE 13			RETURN 139.4581
						CASE 14			RETURN 46.7581
						CASE 15			RETURN -21.0419
						CASE 16			RETURN 135.2581
						CASE 17			RETURN 989.4000
						CASE 18			RETURN 139.4581
						CASE 19			RETURN 46.7581
						CASE 20			RETURN -21.0419
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_SplashLanding_PacificOceanNE
					SWITCH iPed
						// Peds 0-5 are created in vehicles		
						
						// Peds 6 and 7 are on the barge
						CASE 6			RETURN 299.5987
						CASE 7			RETURN 113.5985
						
						// Peds 8-10 are created in vehicles	
						CASE 11			RETURN 135.2581
						CASE 12			RETURN 989.4000
						CASE 13			RETURN 139.4581
						CASE 14			RETURN 46.7581
						CASE 15			RETURN -21.0419
						CASE 16			RETURN 135.2581
						CASE 17			RETURN 989.4000
						CASE 18			RETURN 139.4581
						CASE 19			RETURN 46.7581
						CASE 20			RETURN -21.0419
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_BLACK_BOX
			SWITCH eLocation
				CASE SmugglerLocation_Blackbox_MountGordo
					SWITCH iPed
						CASE 0			RETURN 116.9998
						CASE 1			RETURN 167.6000
						CASE 2			RETURN 356.7990
						CASE 3			RETURN 267.3990
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Blackbox_PaletoForest
					SWITCH iPed
						CASE 0			RETURN 168.1990
						CASE 1			RETURN 13.2000
						CASE 2			RETURN 13.2000
						CASE 3			RETURN 139.1999
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Blackbox_Island
					SWITCH iPed
						CASE 0			RETURN 209.2971
						CASE 1			RETURN 352.5977
						CASE 2			RETURN 111.5975
						CASE 3			RETURN 244.7971
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK

		CASE SMV_THERMAL_SCOPE	
		
			SWITCH eLocation
				CASE SmugglerLocation_ThermalScope_Quarry
					SWITCH iPed
						CASE 0			RETURN 251.1950
						CASE 1			RETURN 268.5950
						CASE 2			RETURN 107.1950
						CASE 3			RETURN 38.1970
						CASE 4			RETURN 216.7970
						CASE 5			RETURN 37.7950
						CASE 6			RETURN 353.1950
						CASE 7			RETURN 86.1950
						CASE 8			RETURN 198.3940
						CASE 9			RETURN 82.5940
						CASE 10			RETURN 292.3940
						CASE 11			RETURN 0.3930
						CASE 12			RETURN 336.1920
						CASE 13			RETURN 293.7920
						CASE 14			RETURN 237.7950
					ENDSWITCH
				BREAK

				CASE SmugglerLocation_ThermalScope_PowerStation
					SWITCH iPed
						CASE 0			RETURN 71.5990
						CASE 1			RETURN 52.7980
						CASE 2			RETURN 238.5970
						CASE 3			RETURN 160.9970
						CASE 4			RETURN 272.3970
						CASE 5			RETURN 94.9950
						CASE 6			RETURN 135.7950
						CASE 7			RETURN 249.5940
						CASE 8			RETURN 107.3950
						CASE 9			RETURN 70.1950
						CASE 10			RETURN 345.1970
						CASE 11			RETURN 73.9960
						CASE 12			RETURN 339.1960
						CASE 13			RETURN 147.3950
						CASE 14			RETURN 260.7950
					ENDSWITCH
				BREAK

				CASE SmugglerLocation_ThermalScope_PlayboyMansion
					SWITCH iPed
						CASE 0			RETURN 75.1990
						CASE 1			RETURN 269.7980
						CASE 2			RETURN 49.1980
						CASE 3			RETURN 331.5970
						CASE 4			RETURN 66.9980
						CASE 5			RETURN 90.3980
						CASE 6			RETURN 178.5978
						CASE 7			RETURN 134.9980
						CASE 8			RETURN 147.9965
						CASE 9			RETURN 165.9980
						CASE 10			RETURN 332.9967
						CASE 11			RETURN 148.5970
						CASE 12			RETURN 138.7950
						CASE 13			RETURN 218.5950
						CASE 14			RETURN 226.1980
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_BOMB_BASE			
			SWITCH eLocation
				CASE SmugglerLocation_BombBase_Terminal
					SWITCH iPed
						CASE 0			RETURN 168.1970
						CASE 1			RETURN 343.3960
						CASE 2			RETURN 333.5960
						CASE 3			RETURN 54.7960
						CASE 4			RETURN 338.7960
						CASE 5			RETURN 149.5950
						CASE 6			RETURN 162.7950
						CASE 7			RETURN 111.7950
						CASE 8			RETURN 295.3950
						CASE 9			RETURN 47.7940
						CASE 10			RETURN 245.3940
						CASE 11			RETURN 122.1940
						CASE 12			RETURN 80.1930
						CASE 13			RETURN 206.1930
						CASE 14			RETURN 296.1930
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_BombBase_GrapeseedAutoyard
					SWITCH iPed
						CASE 0			RETURN 229.1940
						CASE 1			RETURN 52.3940
						CASE 2			RETURN 10.9940
						CASE 3			RETURN 190.7930
						CASE 4			RETURN 302.5930
						CASE 5			RETURN 176.5930
						CASE 6			RETURN 151.3920
						CASE 7			RETURN 208.9920
						CASE 8			RETURN 129.7920
						CASE 9			RETURN 91.7920
						CASE 10			RETURN 262.5920
						CASE 11			RETURN 309.7910
						CASE 12			RETURN 12.7910
						CASE 13			RETURN 185.9910
						CASE 14			RETURN 218.1910
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_BombBase_PlaneScrapyard
					SWITCH iPed
						CASE 0			RETURN 85.5970
						CASE 1			RETURN 206.1970
						CASE 2			RETURN 304.5960
						CASE 3			RETURN 210.3960
						CASE 4			RETURN 215.5960
						CASE 5			RETURN 120.1950
						CASE 6			RETURN 343.5950
						CASE 7			RETURN 180.3950
						CASE 8			RETURN 59.9950
						CASE 9			RETURN 244.9950
						CASE 10			RETURN 136.7940
						CASE 11			RETURN 306.3940
						CASE 12			RETURN 114.3930
						CASE 13			RETURN 195.5930
						CASE 14			RETURN 116.9930
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_ROOF_ATTACK		
			SWITCH eLocation
				CASE SmugglerLocation_RoofAttack_ConstructionSite
					SWITCH iPed
						CASE 0			RETURN 271.7958
						CASE 1			RETURN 157.9980
						CASE 2			RETURN 341.7970
						CASE 3			RETURN 200.7970
						CASE 4			RETURN 27.7970
						CASE 5			RETURN 72.1969
						CASE 6			RETURN 212.7957
						CASE 7			RETURN 246.1948
						CASE 8			RETURN 23.9960
						CASE 9			RETURN 205.1960
						CASE 10			RETURN 201.1950
						CASE 11			RETURN 107.9950
						CASE 12			RETURN 257.1930
						CASE 13			RETURN 313.9930
						CASE 14			RETURN 161.1930
						CASE 15			RETURN 46.9920
						CASE 16			RETURN 269.5920
						CASE 17			RETURN 80.3920
						CASE 18			RETURN 196.1910
						CASE 19			RETURN 226.7920
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_RoofAttack_AltaHotel
					SWITCH iPed
						CASE 0			RETURN 161.5980
						CASE 1			RETURN 357.1970
						CASE 2			RETURN 20.3970
						CASE 3			RETURN 211.1970
						CASE 4			RETURN 101.5950
						CASE 5			RETURN 173.1950
						CASE 6			RETURN 114.7960
						CASE 7			RETURN 206.1950
						CASE 8			RETURN 204.9960
						CASE 9			RETURN 157.9960
						CASE 10			RETURN 247.3960
						CASE 11			RETURN 286.5950
						CASE 12			RETURN 103.9950
						CASE 13			RETURN 281.9940
						CASE 14			RETURN 13.9940
						CASE 15			RETURN 27.5930
						CASE 16			RETURN 175.5950
						CASE 17			RETURN 335.1940
						CASE 18			RETURN 166.5950
						CASE 19			RETURN 105.9940
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_RoofAttack_PacificStandard
					SWITCH iPed
						CASE 0			RETURN 286.3950
						CASE 1			RETURN 106.1950
						CASE 2			RETURN 109.3940
						CASE 3			RETURN 340.9940
						CASE 4			RETURN 37.7940
						CASE 5			RETURN 215.5930
						CASE 6			RETURN 173.1915
						CASE 7			RETURN 15.5930
						CASE 8			RETURN 98.7920
						CASE 9			RETURN 296.1920
						CASE 10			RETURN 66.3920
						CASE 11			RETURN 333.9910
						CASE 12			RETURN 16.1900
						CASE 13			RETURN 204.5900
						CASE 14			RETURN 287.7900
						CASE 15			RETURN 234.3900
						CASE 16			RETURN 167.5920
						CASE 17			RETURN 247.1920
						CASE 18			RETURN 158.7920
						CASE 19			RETURN 286.5900
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_BOMB_ROOF		
			SWITCH eLocation
				CASE SmugglerLocation_BombRoof_Route1
					SWITCH iRespawnLocation
						CASE 0
							SWITCH iPed
								CASE 0			RETURN 236.1840
								CASE 1			RETURN 348.3840
								CASE 2			RETURN 4.5850
								CASE 3			RETURN 292.1840
								CASE 4			RETURN 64.5840
								CASE 5			RETURN 235.7830
								CASE 6			RETURN 190.3830
								CASE 7			RETURN 332.3830
								CASE 8			RETURN 184.7830
								CASE 9			RETURN 14.9820
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH iPed
								CASE 0			RETURN 329.3930
								CASE 1			RETURN 147.3920
								CASE 2			RETURN 234.3920
								CASE 3			RETURN 286.5920
								CASE 4			RETURN 115.5910
								CASE 5			RETURN 56.7910
								CASE 6			RETURN 319.5910
								CASE 7			RETURN 236.3910
								CASE 8			RETURN 61.1910
								CASE 9			RETURN 267.9900
							ENDSWITCH
						BREAK
						CASE 2
							SWITCH iPed
								CASE 0			RETURN 330.3900
								CASE 1			RETURN 249.1890
								CASE 2			RETURN 79.5890
								CASE 3			RETURN 85.1890
								CASE 4			RETURN 173.5890
								CASE 5			RETURN 352.3890
								CASE 6			RETURN 204.1880
								CASE 7			RETURN 219.1880
								CASE 8			RETURN 250.3880
								CASE 9			RETURN 146.9880
							ENDSWITCH
						BREAK
						CASE 3
							SWITCH iPed
								CASE 0			RETURN 202.1870
								CASE 1			RETURN 27.9870
								CASE 2			RETURN 300.9870
								CASE 3			RETURN 122.7860
								CASE 4			RETURN 192.9860
								CASE 5			RETURN 211.1860
								CASE 6			RETURN 108.9860
								CASE 7			RETURN 140.1850
								CASE 8			RETURN 320.7850
								CASE 9			RETURN 354.9850
							ENDSWITCH
						BREAK
						CASE 4
							SWITCH iPed
								CASE 0			RETURN 171.9820
								CASE 1			RETURN 322.9820
								CASE 2			RETURN 238.3820
								CASE 3			RETURN 55.1810
								CASE 4			RETURN 206.9810
								CASE 5			RETURN 324.3810
								CASE 6			RETURN 210.1810
								CASE 7			RETURN 31.5800
								CASE 8			RETURN 119.1800
								CASE 9			RETURN 294.3800
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_BombRoof_Route2
					SWITCH iRespawnLocation
						CASE 0
							SWITCH iPed
								CASE 0			RETURN 335.7990
								CASE 1			RETURN 160.7990
								CASE 2			RETURN 265.9980
								CASE 3			RETURN 113.5980
								CASE 4			RETURN 239.5970
								CASE 5			RETURN 327.9970
								CASE 6			RETURN 1.1970
								CASE 7			RETURN 265.1970
								CASE 8			RETURN 59.3970
								CASE 9			RETURN 349.3950
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH iPed
								CASE 0			RETURN 322.1950
								CASE 1			RETURN 182.9950
								CASE 2			RETURN 141.1940
								CASE 3			RETURN 195.7940
								CASE 4			RETURN 25.5930
								CASE 5			RETURN 270.1930
								CASE 6			RETURN 97.5920
								CASE 7			RETURN 31.3920
								CASE 8			RETURN 249.9920
								CASE 9			RETURN 213.9940
							ENDSWITCH
						BREAK
						CASE 2
							SWITCH iPed
								CASE 0			RETURN 298.1910
								CASE 1			RETURN 121.3900
								CASE 2			RETURN 137.5900
								CASE 3			RETURN -3.0100
								CASE 4			RETURN 144.3890
								CASE 5			RETURN 138.9890
								CASE 6			RETURN 316.5890
								CASE 7			RETURN 240.9880
								CASE 8			RETURN 195.5880
								CASE 9			RETURN 7.7880
							ENDSWITCH
						BREAK
						CASE 3
							SWITCH iPed
								CASE 0			RETURN 305.1930
								CASE 1			RETURN 132.9930
								CASE 2			RETURN 153.9930
								CASE 3			RETURN 184.5920
								CASE 4			RETURN 299.7920
								CASE 5			RETURN 188.5920
								CASE 6			RETURN 7.7920
								CASE 7			RETURN 312.5910
								CASE 8			RETURN 123.3910
								CASE 9			RETURN 308.1900
							ENDSWITCH
						BREAK
						CASE 4
							SWITCH iPed
								CASE 0			RETURN 295.3900
								CASE 1			RETURN 118.3900
								CASE 2			RETURN 221.5890
								CASE 3			RETURN 175.3890
								CASE 4			RETURN 10.3890
								CASE 5			RETURN 227.1890
								CASE 6			RETURN 352.1890
								CASE 7			RETURN 47.3890
								CASE 8			RETURN 218.1890
								CASE 9			RETURN 211.9890
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_BombRoof_Route3
					SWITCH iRespawnLocation
						CASE 0
							SWITCH iPed
								CASE 0			RETURN 202.8000
								CASE 1			RETURN 30.5990
								CASE 2			RETURN 319.9990
								CASE 3			RETURN 142.3990
								CASE 4			RETURN 307.5990
								CASE 5			RETURN 166.9980
								CASE 6			RETURN 29.5980
								CASE 7			RETURN 16.1990
								CASE 8			RETURN 204.9990
								CASE 9			RETURN 161.1980
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH iPed
								CASE 0			RETURN 213.5920
								CASE 1			RETURN 23.1910
								CASE 2			RETURN 212.1910
								CASE 3			RETURN 151.5930
								CASE 4			RETURN 331.1920
								CASE 5			RETURN 342.7920
								CASE 6			RETURN 345.7920
								CASE 7			RETURN 34.9920
								CASE 8			RETURN 83.9920
								CASE 9			RETURN 267.9910
							ENDSWITCH
						BREAK
						CASE 2
							SWITCH iPed
								CASE 0			RETURN 326.5960
								CASE 1			RETURN 141.5960
								CASE 2			RETURN 202.3950
								CASE 3			RETURN 281.9950
								CASE 4			RETURN 334.7950
								CASE 5			RETURN 16.7950
								CASE 6			RETURN 216.5940
								CASE 7			RETURN 83.3940
								CASE 8			RETURN 345.1940
								CASE 9			RETURN 166.3930
							ENDSWITCH
						BREAK
						CASE 3
							SWITCH iPed
								CASE 0			RETURN 334.5910
								CASE 1			RETURN 163.9900
								CASE 2			RETURN 53.7900
								CASE 3			RETURN 343.5900
								CASE 4			RETURN 188.1900
								CASE 5			RETURN 307.3890
								CASE 6			RETURN 166.7890
								CASE 7			RETURN 255.5890
								CASE 8			RETURN 57.5880
								CASE 9			RETURN 263.7880
							ENDSWITCH
						BREAK
						CASE 4
							SWITCH iPed
								CASE 0			RETURN 161.7970
								CASE 1			RETURN 203.7970
								CASE 2			RETURN 302.9970
								CASE 3			RETURN 304.9970
								CASE 4			RETURN 133.9960
								CASE 5			RETURN 353.5960
								CASE 6			RETURN 249.5960
								CASE 7			RETURN 69.9950
								CASE 8			RETURN 153.1950
								CASE 9			RETURN 132.7950
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_SELL_CONTESTED
			SWITCH eDropOff
				CASE SMUGGLER_DROPOFF_CONTESTED_1
					SWITCH iPed
						CASE 0			RETURN 214.8000
						CASE 1			RETURN 186.0000
						CASE 2			RETURN 77.4000
						CASE 3			RETURN 302.7990
						CASE 4			RETURN 185.3990
						CASE 5			RETURN 190.1990
						CASE 6			RETURN 8.1990
						CASE 7			RETURN 31.5980
						CASE 8			RETURN 305.3980
						CASE 9			RETURN 127.5970
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_2
					SWITCH iPed
						CASE 0			RETURN 179.7970
						CASE 1			RETURN 234.3957
						CASE 2			RETURN 312.3958
						CASE 3			RETURN 210.7955
						CASE 4			RETURN 47.5959
						CASE 5			RETURN 87.1959
						CASE 6			RETURN 356.1945
						CASE 7			RETURN 240.1943
						CASE 8			RETURN 55.3949
						CASE 9			RETURN 224.3950
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_3
					SWITCH iPed
						CASE 0			RETURN 161.9940
						CASE 1			RETURN 1.7940
						CASE 2			RETURN 1.7940
						CASE 3			RETURN 7.9930
						CASE 4			RETURN 18.5920
						CASE 5			RETURN 190.5930
						CASE 6			RETURN 9.9930
						CASE 7			RETURN 285.7920
						CASE 8			RETURN 221.9920
						CASE 9			RETURN 218.7920
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_4
					SWITCH iPed
						CASE 0			RETURN 131.9920
						CASE 1			RETURN 126.3918
						CASE 2			RETURN 268.3910
						CASE 3			RETURN 268.3910
						CASE 4			RETURN 272.5910
						CASE 5			RETURN 88.3900
						CASE 6			RETURN 40.1899
						CASE 7			RETURN 149.3889
						CASE 8			RETURN 237.9884
						CASE 9			RETURN 89.1890
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_5
					SWITCH iPed
						CASE 0			RETURN 70.1890
						CASE 1			RETURN 350.3890
						CASE 2			RETURN 46.3880
						CASE 3			RETURN 280.1880
						CASE 4			RETURN 10.3870
						CASE 5			RETURN 192.1870
						CASE 6			RETURN 304.9870
						CASE 7			RETURN 277.1870
						CASE 8			RETURN 203.9870
						CASE 9			RETURN 169.7870
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_6
					SWITCH iPed
						CASE 0			RETURN 308.9990
						CASE 1			RETURN 129.7980
						CASE 2			RETURN 270.9980
						CASE 3			RETURN 95.7980
						CASE 4			RETURN 183.7970
						CASE 5			RETURN 162.7970
						CASE 6			RETURN 305.3970
						CASE 7			RETURN 149.9960
						CASE 8			RETURN 93.3950
						CASE 9			RETURN 27.1950
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_7
					SWITCH iPed
						CASE 0			RETURN 224.7950
						CASE 1			RETURN 365.3950
						CASE 2			RETURN 80.7940
						CASE 3			RETURN 177.9940
						CASE 4			RETURN 296.9937
						CASE 5			RETURN 111.9939
						CASE 6			RETURN 300.7930
						CASE 7			RETURN 131.5930
						CASE 8			RETURN 217.5919
						CASE 9			RETURN 38.5920
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_8
					SWITCH iPed
						CASE 0			RETURN 329.5919
						CASE 1			RETURN 76.5920
						CASE 2			RETURN 235.1920
						CASE 3			RETURN 358.3910
						CASE 4			RETURN 190.3910
						CASE 5			RETURN 245.5910
						CASE 6			RETURN 299.9900
						CASE 7			RETURN 41.1900
						CASE 8			RETURN 299.1900
						CASE 9			RETURN 293.3886
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_9
					SWITCH iPed
						CASE 0			RETURN 243.7890
						CASE 1			RETURN 203.7880
						CASE 2			RETURN 57.7880
						CASE 3			RETURN 158.7880
						CASE 4			RETURN 228.9870
						CASE 5			RETURN 311.3870
						CASE 6			RETURN 296.1870
						CASE 7			RETURN 310.7870
						CASE 8			RETURN 284.1880
						CASE 9			RETURN 114.5870
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_10
					SWITCH iPed
						CASE 0			RETURN 78.9869
						CASE 1			RETURN 262.1867
						CASE 2			RETURN 8.7869
						CASE 3			RETURN 222.1870
						CASE 4			RETURN 191.5870
						CASE 5			RETURN 9.5868
						CASE 6			RETURN 58.7857
						CASE 7			RETURN 276.3859
						CASE 8			RETURN 97.1859
						CASE 9			RETURN 134.1858
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_11
					SWITCH iPed
						CASE 0			RETURN 18.9997
						CASE 1			RETURN 115.3998
						CASE 2			RETURN 293.1990
						CASE 3			RETURN 115.7990
						CASE 4			RETURN 153.5977
						CASE 5			RETURN 263.3975
						CASE 6			RETURN 150.9990
						CASE 7			RETURN 57.9990
						CASE 8			RETURN 332.1976
						CASE 9			RETURN 69.7980
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_12
					SWITCH iPed
						CASE 0			RETURN 122.3490
						CASE 1			RETURN 70.5488
						CASE 2			RETURN 244.7487
						CASE 3			RETURN 272.7478
						CASE 4			RETURN 18.9490
						CASE 5			RETURN 321.7490
						CASE 6			RETURN 181.7485
						CASE 7			RETURN 276.9487
						CASE 8			RETURN 308.1487
						CASE 9			RETURN 126.5477
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_13
					SWITCH iPed
						CASE 0			RETURN 15.6000
						CASE 1			RETURN 207.6000
						CASE 2			RETURN 221.9989
						CASE 3			RETURN 272.3990
						CASE 4			RETURN 40.1990
						CASE 5			RETURN 28.9987
						CASE 6			RETURN 94.5478
						CASE 7			RETURN 207.1477
						CASE 8			RETURN 177.5468
						CASE 9			RETURN 137.9470
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_14
					SWITCH iPed
						CASE 0			RETURN 260.6000
						CASE 1			RETURN 26.5990
						CASE 2			RETURN 211.3990
						CASE 3			RETURN 152.1988
						CASE 4			RETURN 195.3988
						CASE 5			RETURN 283.3977
						CASE 6			RETURN 39.9977
						CASE 7			RETURN 229.1977
						CASE 8			RETURN 151.5965
						CASE 9			RETURN 20.9967
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_15
					SWITCH iPed
						CASE 0			RETURN 310.9996
						CASE 1			RETURN 121.7993
						CASE 2			RETURN 310.9989
						CASE 3			RETURN 76.9986
						CASE 4			RETURN 105.5985
						CASE 5			RETURN 254.3979
						CASE 6			RETURN 187.1980
						CASE 7			RETURN 325.7974
						CASE 8			RETURN 347.7972
						CASE 9			RETURN 199.5969
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_16
					SWITCH iPed
						CASE 0			RETURN 46.0000
						CASE 1			RETURN 37.8000
						CASE 2			RETURN 62.2000
						CASE 3			RETURN 255.2000
						CASE 4			RETURN 191.3990
						CASE 5			RETURN 215.5990
						CASE 6			RETURN 265.3990
						CASE 7			RETURN 314.6000
						CASE 8			RETURN 104.5990
						CASE 9			RETURN 14.1988
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_17
					SWITCH iPed
						CASE 0			RETURN 244.7990
						CASE 1			RETURN 239.1990
						CASE 2			RETURN 200.3980
						CASE 3			RETURN 57.9980
						CASE 4			RETURN 60.7980
						CASE 5			RETURN 291.1980
						CASE 6			RETURN 277.9980
						CASE 7			RETURN 98.5980
						CASE 8			RETURN 149.1980
						CASE 9			RETURN 319.3980
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_18
					SWITCH iPed
						CASE 0			RETURN 201.3980
						CASE 1			RETURN 20.7980
						CASE 2			RETURN 176.7970
						CASE 3			RETURN 95.5970
						CASE 4			RETURN 39.3970
						CASE 5			RETURN 312.7970
						CASE 6			RETURN 200.7960
						CASE 7			RETURN 251.3960
						CASE 8			RETURN 354.3960
						CASE 9			RETURN 167.7970
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_19
					SWITCH iPed
						CASE 0			RETURN 120.1970
						CASE 1			RETURN 307.1960
						CASE 2			RETURN 181.1960
						CASE 3			RETURN 266.9950
						CASE 4			RETURN 353.9950
						CASE 5			RETURN 353.9950
						CASE 6			RETURN -1.0050
						CASE 7			RETURN 88.1950
						CASE 8			RETURN 183.1950
						CASE 9			RETURN 195.1940
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_20
					SWITCH iPed
						CASE 0			RETURN 37.5940
						CASE 1			RETURN 40.1940
						CASE 2			RETURN 190.6440
						CASE 3			RETURN 350.2430
						CASE 4			RETURN 128.0430
						CASE 5			RETURN 308.6430
						CASE 6			RETURN 268.6420
						CASE 7			RETURN 156.4420
						CASE 8			RETURN 118.6420
						CASE 9			RETURN 266.0400
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_21
					SWITCH iPed
						CASE 0			RETURN 61.2000
						CASE 1			RETURN 79.4000
						CASE 2			RETURN 345.5990
						CASE 3			RETURN 165.7990
						CASE 4			RETURN 215.7990
						CASE 5			RETURN 218.1990
						CASE 6			RETURN 22.1980
						CASE 7			RETURN 237.9980
						CASE 8			RETURN 296.1960
						CASE 9			RETURN 152.7970
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_22
					SWITCH iPed
						CASE 0			RETURN 351.7997
						CASE 1			RETURN 206.7988
						CASE 2			RETURN 253.5990
						CASE 3			RETURN 317.1985
						CASE 4			RETURN 89.1986
						CASE 5			RETURN 97.5976
						CASE 6			RETURN 91.3979
						CASE 7			RETURN 170.9977
						CASE 8			RETURN 342.5976
						CASE 9			RETURN 131.1979
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_23
					SWITCH iPed
						CASE 0			RETURN 104.9987
						CASE 1			RETURN 60.5969
						CASE 2			RETURN 241.9968
						CASE 3			RETURN 139.5969
						CASE 4			RETURN 70.3970
						CASE 5			RETURN 333.1966
						CASE 6			RETURN 137.9967
						CASE 7			RETURN 334.7957
						CASE 8			RETURN 219.9954
						CASE 9			RETURN 113.9966
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_24
					SWITCH iPed
						CASE 0			RETURN 222.5990
						CASE 1			RETURN 50.3990
						CASE 2			RETURN 151.5990
						CASE 3			RETURN 254.7990
						CASE 4			RETURN 297.9990
						CASE 5			RETURN 323.3990
						CASE 6			RETURN 25.5980
						CASE 7			RETURN 74.5980
						CASE 8			RETURN 86.9980
						CASE 9			RETURN 194.3990
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_25
					SWITCH iPed
						CASE 0			RETURN 2.6000
						CASE 1			RETURN 185.7990
						CASE 2			RETURN 45.1990
						CASE 3			RETURN 90.3990
						CASE 4			RETURN 128.1990
						CASE 5			RETURN 106.3980
						CASE 6			RETURN 232.7980
						CASE 7			RETURN 32.5980
						CASE 8			RETURN 293.3980
						CASE 9			RETURN 194.1970
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_26
					SWITCH iPed
						CASE 0			RETURN 81.4000
						CASE 1			RETURN 248.6000
						CASE 2			RETURN 198.0000
						CASE 3			RETURN 239.9990
						CASE 4			RETURN 267.1990
						CASE 5			RETURN 348.3990
						CASE 6			RETURN 49.5990
						CASE 7			RETURN 62.7990
						CASE 8			RETURN 108.9990
						CASE 9			RETURN 302.7980
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_27
					SWITCH iPed
						CASE 0			RETURN -1.3740
						CASE 1			RETURN 168.8260
						CASE 2			RETURN 273.0250
						CASE 3			RETURN 335.4240
						CASE 4			RETURN 9.3995
						CASE 5			RETURN 175.7992
						CASE 6			RETURN 168.7990
						CASE 7			RETURN 144.3986
						CASE 8			RETURN 99.3983
						CASE 9			RETURN 161.1980
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_28
					SWITCH iPed
						CASE 0			RETURN 126.4000
						CASE 1			RETURN 306.1990
						CASE 2			RETURN 153.5980
						CASE 3			RETURN 91.5980
						CASE 4			RETURN 290.5980
						CASE 5			RETURN 320.7970
						CASE 6			RETURN 308.1970
						CASE 7			RETURN 302.5970
						CASE 8			RETURN 343.9960
						CASE 9			RETURN 61.9960
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_29
					SWITCH iPed
						CASE 0			RETURN 26.1990
						CASE 1			RETURN 205.5990
						CASE 2			RETURN 93.3980
						CASE 3			RETURN 168.9980
						CASE 4			RETURN 224.1980
						CASE 5			RETURN 56.3970
						CASE 6			RETURN -0.8030
						CASE 7			RETURN 328.5970
						CASE 8			RETURN 98.9970
						CASE 9			RETURN 23.1960
					ENDSWITCH
				BREAK
				CASE SMUGGLER_DROPOFF_CONTESTED_30
					SWITCH iPed
						CASE 0			RETURN 206.9990
						CASE 1			RETURN 77.7990
						CASE 2			RETURN 161.1990
						CASE 3			RETURN 71.9990
						CASE 4			RETURN 205.1990
						CASE 5			RETURN 345.7990
						CASE 6			RETURN 161.7990
						CASE 7			RETURN 170.1990
						CASE 8			RETURN 240.9990
						CASE 9			RETURN 164.1980
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_SELL_FLY_LOW
			SWITCH eDropOff
				CASE SMUGGLER_DROPOFF_FLY_LOW_1
					RETURN 353.5910
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_2
					RETURN 333.3910
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_3
					RETURN 23.7999
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_4
					RETURN 26.7910
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_5
					RETURN 111.2000
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_6
					RETURN 111.2000
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_7
					RETURN 85.2000
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_8
					RETURN 72.0000
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_9
					RETURN 258.4000
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_10
					RETURN 169.5990
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_11
					RETURN 116.7990
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_12
					RETURN 268.5480
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_13
					RETURN 18.6000
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_14
					RETURN 329.4000
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_15
					RETURN 20.4000
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_16
					RETURN 322.7990
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_17
					RETURN 284.7990
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_18
					RETURN 67.7990
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_19
					RETURN 91.7990
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_20
					RETURN 216.7980
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_21
					RETURN 325.3980
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_22
					RETURN 11.1976
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_23
					RETURN 338.3950
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_24
					RETURN 142.9940
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_25
					RETURN 317.1940
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_26
					RETURN 344.1940
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_27
					RETURN 101.9940
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_28
					RETURN 203.5930
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_29
					RETURN 46.1930
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_30
					RETURN 149.2000
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_31
					RETURN 326.1990
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_32
					RETURN 188.5990
				
				CASE SMUGGLER_DROPOFF_FLY_LOW_33
					RETURN 82.5990
				
			ENDSWITCH
		BREAK
		
		CASE SMV_INFILTRATION		
			SWITCH eLocation
				CASE SmugglerLocation_Infiltration_HumaneLabs
					SWITCH iPed
						CASE 0			RETURN 10.9990
						CASE 1			RETURN 201.3990
						CASE 2			RETURN 169.3990
						CASE 3			RETURN 166.3990
						CASE 4			RETURN 104.5990
						CASE 5			RETURN 166.9990
						CASE 6			RETURN 81.1990
						CASE 7			RETURN 349.7980
						CASE 8			RETURN 263.7979
						CASE 9			RETURN 85.7481
						CASE 10			RETURN 142.1965
						CASE 11			RETURN 173.9970
						CASE 12			RETURN 264.3980
						CASE 13			RETURN 79.9980
						CASE 14			RETURN 164.5970
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Infiltration_BolingbrokePrison
					SWITCH iPed
						CASE 0			RETURN 40.9986
						CASE 1			RETURN 218.9990
						CASE 2			RETURN 271.9946
						CASE 3			RETURN 274.7950
						CASE 4			RETURN 75.3451
						CASE 5			RETURN 268.1928
						CASE 6			RETURN 342.5938
						CASE 7			RETURN 177.5940
						CASE 8			RETURN 117.5930
						CASE 9			RETURN 313.5930
						CASE 10			RETURN 0.1930
						CASE 11			RETURN 87.3930
						CASE 12			RETURN 341.7934
						CASE 13			RETURN 160.9928
						CASE 14			RETURN 104.5930
						CASE 15			RETURN 285.5923
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Infiltration_Zancudo
					SWITCH iPed
						CASE 0			RETURN 192.7990
						CASE 1			RETURN 70.7990
						CASE 2			RETURN 272.7990
						CASE 3			RETURN 148.7990
						CASE 4			RETURN 37.7990
						CASE 5			RETURN 214.7980
						CASE 6			RETURN 323.5974
						CASE 7			RETURN 167.5974
						CASE 8			RETURN 152.7975
						CASE 9			RETURN 147.7980
						CASE 10			RETURN 46.5970
						CASE 11			RETURN 325.7960
						CASE 12			RETURN 334.9957
						CASE 13			RETURN 56.9967
						CASE 14			RETURN 87.1960
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_SETUP_RON
			SWITCH eLocation
				CASE SmugglerLocation_Setup_DelPerroBeach 
					SWITCH iPed
						CASE 0			RETURN 230.7424
						CASE 1			RETURN 50.3422
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_LSRiver
					SWITCH iPed
						CASE 0			RETURN 287.7988
						CASE 1			RETURN 98.5984
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_ElBurro
					SWITCH iPed
						CASE 0			RETURN 177.1990
						CASE 1			RETURN 357.7990
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_Chumash
					SWITCH iPed
						CASE 0			RETURN 103.3963
						CASE 1			RETURN 291.1960
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_Tutuavium
					SWITCH iPed
						CASE 0			RETURN 202.1120
						CASE 1			RETURN 24.5119
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_RonsWindFarm
					SWITCH iPed
						CASE 0			RETURN 148.5952
						CASE 1			RETURN 343.3949
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_SenoraDesert1
					SWITCH iPed
						CASE 0			RETURN 252.3936
						CASE 1			RETURN 71.7935
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_SenoraDesert2
					SWITCH iPed
						CASE 0			RETURN 280.3930
						CASE 1			RETURN 111.9926
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_SandyShores
					SWITCH iPed
						CASE 0			RETURN 292.0419
						CASE 1			RETURN 112.0414
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_StabCity
					SWITCH iPed
						CASE 0			RETURN 308.1346
						CASE 1			RETURN 127.5342
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_NCalafiaWay
					SWITCH iPed
						CASE 0			RETURN 340.1346
						CASE 1			RETURN 150.9342
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_McKenzie
					SWITCH iPed
						CASE 0			RETURN 313.9330
						CASE 1			RETURN 131.5326
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_Grapeseed
					SWITCH iPed
						CASE 0			RETURN 281.9332
						CASE 1			RETURN 91.1332
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_ProcopioBeach
					SWITCH iPed
						CASE 0			RETURN 225.5420
						CASE 1			RETURN 46.5419
					ENDSWITCH
				BREAK
				CASE SmugglerLocation_Setup_Paleto
					SWITCH iPed
						CASE 0			RETURN 64.7406
						CASE 1			RETURN 255.5403
					ENDSWITCH
				BREAK

			ENDSWITCH
		BREAK	
		
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC MODEL_NAMES GET_SMUGGLER_PED_MODEL(INT iPed, SMUGGLER_VARIATION eVariation, SMUGGLER_SUBVARIATION eSubvariation, SMUGGLER_LOCATION eLocation)
	BOOL bBikerGang = GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
	UNUSED_PARAMETER(bBikerGang)
	
	SWITCH eVariation
		CASE SMV_STEAL_AIRCRAFT
			SWITCH eSubvariation
				CASE SMS_SA_SANDY_SHORES
					RETURN mp_g_m_pros_01
				CASE SMS_SA_MCKENZIE_AIRFIELD
					RETURN g_m_y_mexgang_01
				CASE SMS_SA_EPSILON_HQ
					SWITCH iPed
						CASE 0			RETURN S_M_M_HighSec_01
						CASE 1			RETURN S_M_M_HighSec_01
						CASE 2			RETURN S_M_M_HighSec_01
						CASE 3			RETURN S_M_M_HighSec_01
						CASE 4			RETURN A_M_Y_Epsilon_01
						CASE 5			RETURN A_M_Y_Epsilon_01
						CASE 6			RETURN A_M_Y_Epsilon_01
						CASE 7			RETURN A_M_Y_Epsilon_01
						CASE 8			RETURN A_M_Y_Epsilon_01
						CASE 9			RETURN A_M_Y_Epsilon_01
						CASE 10			RETURN A_M_Y_Epsilon_01
						CASE 11			RETURN S_M_M_HighSec_01
						CASE 12			RETURN S_M_M_HighSec_01
						CASE 13			RETURN A_M_Y_Epsilon_01
						CASE 14			RETURN S_M_M_HighSec_01
						CASE 15			RETURN S_M_M_HighSec_01
						CASE 16			RETURN S_M_M_HighSec_01		// Ambush
						CASE 17			RETURN S_M_M_HighSec_01
						CASE 18			RETURN S_M_M_HighSec_01
						CASE 19			RETURN S_M_M_HighSec_01
					ENDSWITCH
				BREAK
				
				
				
			ENDSWITCH
		BREAK
		
		
		CASE SMV_SPLASH_LANDING
			RETURN G_M_M_ChiGoon_02
		
		CASE SMV_BLACK_BOX
			RETURN g_m_y_mexgang_01
		
		CASE SMV_THERMAL_SCOPE	
		
			SWITCH eLocation
				CASE SmugglerLocation_ThermalScope_Quarry
					SWITCH iPed
						CASE 0			RETURN mp_g_m_pros_01
						CASE 1			RETURN mp_g_m_pros_01
						CASE 2			RETURN mp_g_m_pros_01
						CASE 3			RETURN mp_g_m_pros_01
						CASE 4			RETURN mp_g_m_pros_01
						CASE 5			RETURN mp_g_m_pros_01
						CASE 6			RETURN mp_g_m_pros_01
						CASE 7			RETURN mp_g_m_pros_01
						CASE 8			RETURN mp_g_m_pros_01
						CASE 9			RETURN mp_g_m_pros_01
						CASE 10			RETURN mp_g_m_pros_01
						CASE 11			RETURN mp_g_m_pros_01
						CASE 12			RETURN mp_g_m_pros_01
						CASE 13			RETURN mp_g_m_pros_01
						CASE 14			RETURN mp_g_m_pros_01
						
						CASE 15			RETURN mp_g_m_pros_01 // AMBUSH
						CASE 16			RETURN mp_g_m_pros_01 // AMBUSH
						CASE 17			RETURN mp_g_m_pros_01 // AMBUSH
						CASE 18			RETURN mp_g_m_pros_01 // AMBUSH
					ENDSWITCH
				BREAK

				CASE SmugglerLocation_ThermalScope_PowerStation
					SWITCH iPed
						CASE 0			RETURN mp_g_m_pros_01
						CASE 1			RETURN mp_g_m_pros_01
						CASE 2			RETURN mp_g_m_pros_01
						CASE 3			RETURN mp_g_m_pros_01
						CASE 4			RETURN mp_g_m_pros_01
						CASE 5			RETURN mp_g_m_pros_01
						CASE 6			RETURN mp_g_m_pros_01
						CASE 7			RETURN mp_g_m_pros_01
						CASE 8			RETURN mp_g_m_pros_01
						CASE 9			RETURN mp_g_m_pros_01
						CASE 10			RETURN mp_g_m_pros_01
						CASE 11			RETURN mp_g_m_pros_01
						CASE 12			RETURN mp_g_m_pros_01
						CASE 13			RETURN mp_g_m_pros_01
						CASE 14			RETURN mp_g_m_pros_01
						
						CASE 15			RETURN mp_g_m_pros_01 // AMBUSH
						CASE 16			RETURN mp_g_m_pros_01 // AMBUSH
						CASE 17			RETURN mp_g_m_pros_01 // AMBUSH
						CASE 18			RETURN mp_g_m_pros_01 // AMBUSH
					ENDSWITCH
				BREAK

				CASE SmugglerLocation_ThermalScope_PlayboyMansion
					SWITCH iPed
						CASE 0			RETURN G_M_M_Korboss_01
						CASE 1			RETURN G_M_M_Korboss_01
						CASE 2			RETURN G_M_M_Korboss_01
						CASE 3			RETURN G_M_M_Korboss_01
						CASE 4			RETURN G_M_M_Korboss_01
						CASE 5			RETURN G_M_M_Korboss_01
						CASE 6			RETURN G_M_M_Korboss_01
						CASE 7			RETURN G_M_M_Korboss_01
						CASE 8			RETURN G_M_M_Korboss_01
						CASE 9			RETURN G_M_M_Korboss_01
						CASE 10			RETURN G_M_M_Korboss_01
						CASE 11			RETURN G_M_M_Korboss_01
						CASE 12			RETURN G_M_M_Korboss_01
						CASE 13			RETURN G_M_M_Korboss_01
						CASE 14			RETURN G_M_M_Korboss_01
						
						CASE 15			RETURN G_M_M_Korboss_01 // AMBUSH
						CASE 16			RETURN G_M_M_Korboss_01 // AMBUSH
						CASE 17			RETURN G_M_M_Korboss_01 // AMBUSH
						CASE 18			RETURN G_M_M_Korboss_01 // AMBUSH
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_DOGFIGHT						RETURN S_M_M_HighSec_01
		
		CASE SMV_BOMB_BASE
			SWITCH eSubvariation
				CASE SMS_BB_TERMINAL			RETURN G_M_M_ChiGoon_02
				CASE SMS_BB_GRAPESEED_AUTOYARD	RETURN G_M_M_ArmGoon_01
				CASE SMS_BB_PLANE_SCRAPYARD		RETURN mp_g_m_pros_01
			ENDSWITCH
		BREAK
		
		CASE SMV_CARGO_PLANE
			RETURN S_M_Y_BLACKOPS_01
			
		CASE SMV_ROOF_ATTACK
			SWITCH eSubvariation
				CASE SMS_RA_CONSTRUCTION_SITE		RETURN G_M_M_ArmGoon_01
				CASE SMS_RA_ALTA_HOTEL				RETURN G_M_Y_KorLieut_01
				CASE SMS_RA_PACIFIC_STANDARD		RETURN mp_g_m_pros_01
			ENDSWITCH
		BREAK
		
		CASE SMV_INFILTRATION
			SWITCH eSubvariation
				CASE SMS_INF_HUMANE_LABS			RETURN S_M_Y_BLACKOPS_01
				CASE SMS_INF_BOLINGBROKE_PRISON		
					SWITCH iPed
						CASE 0			RETURN S_M_M_Prisguard_01
						CASE 1			RETURN S_M_M_Prisguard_01
						CASE 2			RETURN S_M_Y_Swat_01
						CASE 3			RETURN S_M_M_Prisguard_01
						CASE 4			RETURN S_M_Y_Swat_01
						CASE 5			RETURN S_M_M_Prisguard_01
						CASE 6			RETURN S_M_M_Prisguard_01
						CASE 7			RETURN S_M_M_Prisguard_01
						CASE 8			RETURN S_M_M_Prisguard_01
						CASE 9			RETURN S_M_M_Prisguard_01
						CASE 10			RETURN S_M_M_Prisguard_01
						CASE 11			RETURN S_M_M_Prisguard_01
						CASE 12			RETURN S_M_M_Prisguard_01
						CASE 13			RETURN S_M_M_Prisguard_01
						CASE 14			RETURN S_M_M_Prisguard_01
						CASE 15			RETURN S_M_M_Prisguard_01
						CASE 16			RETURN S_M_M_Prisguard_01
					ENDSWITCH
				BREAK
				CASE SMS_INF_ZANCUDO				RETURN S_M_Y_BLACKOPS_01
			ENDSWITCH
		BREAK
		
		CASE SMV_BOMB_ROOF						RETURN mp_g_m_pros_01
		CASE SMV_BOMBING_RUN					RETURN G_M_M_ArmGoon_01
		CASE SMV_BEACON_GRAB					RETURN S_M_Y_BLACKOPS_01
		CASE SMV_ESCORT							
			SWITCH iPed
				CASE 0					RETURN mp_m_exarmy_01
				CASE 1					RETURN mp_m_exarmy_01
				DEFAULT					RETURN MP_G_M_PROS_01
			ENDSWITCH
		BREAK
		
		// Sell
		CASE SMV_SELL_HEAVY_LIFTING				RETURN G_M_M_Korboss_01
		CASE SMV_SELL_FLYING_FORTRESS			RETURN S_M_Y_BLACKOPS_01
		CASE SMV_SELL_CONTESTED					RETURN MP_G_M_PROS_01
		CASE SMV_SELL_FLY_LOW					RETURN S_M_Y_XMech_01
		CASE SMV_SELL_PRECISION_DELIVERY		RETURN S_M_Y_DEALER_01
		CASE SMV_SELL_AIR_DELIVERY				RETURN g_m_y_mexgang_01
		CASE SMV_SELL_AIR_POLICE				RETURN S_M_Y_Swat_01
		
		CASE SMV_SETUP_RON						RETURN G_M_Y_Lost_01
 
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC VECTOR GET_SMUGGLER_PED_RESPAWN_COORDS(INT iPed, SMUGGLER_LOCATION eLocation, SMUGGLER_VARIATION eVariation, FREEMODE_DELIVERY_DROPOFFS eDropOff = FREEMODE_DELIVERY_DROPOFF_INVALID, INT iRespawnLocation = 0)
	SWITCH eVariation
		
		CASE SMV_BEACON_GRAB
			SWITCH eLocation
				CASE SmugglerLocation_BeaconGrab_Location1
					SWITCH iPed						
						CASE 0			RETURN <<0.0000, 0.0000, 0.0000>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_SELL_CONTESTED
			RETURN GET_SMUGGLER_PED_SPAWN_COORDS(iPed, eLocation, eVariation, eDropOff)
		BREAK
		
		CASE SMV_BOMB_ROOF		RETURN GET_SMUGGLER_PED_SPAWN_COORDS(iPed, eLocation, eVariation, eDropOff, iRespawnLocation)
		
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC FLOAT GET_SMUGGLER_PED_RESPAWN_HEADING(INT iPed, SMUGGLER_VARIATION eVariation, SMUGGLER_LOCATION eLocation, FREEMODE_DELIVERY_DROPOFFS eDropOff = FREEMODE_DELIVERY_DROPOFF_INVALID, INT iRespawnLocation = 0)
	SWITCH eVariation

		CASE SMV_BEACON_GRAB
			SWITCH eLocation
				CASE SmugglerLocation_BeaconGrab_Location1
					SWITCH iPed
						CASE 0		RETURN 0.0
					ENDSWITCH
				BREAK 
			ENDSWITCH
		BREAK
		
		CASE SMV_SELL_CONTESTED
			RETURN GET_SMUGGLER_PED_SPAWN_HEADING(iPed, eVariation, eLocation, eDropOff)
		BREAK
		
		CASE SMV_BOMB_ROOF		RETURN GET_SMUGGLER_PED_SPAWN_HEADING(iPed, eVariation, eLocation, eDropOff, iRespawnLocation)
		
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC MODEL_NAMES GET_SMUGGLER_VARIATION_PROP_MODEL(INT iProp, SMUGGLER_VARIATION eVariation, SMUGGLER_SUBVARIATION eSubvariation)
	
	SWITCH eVariation
	
		CASE SMV_STEAL_AIRCRAFT
			SWITCH eSubvariation
				CASE SMS_SA_SANDY_SHORES
					SWITCH iProp
						CASE 0			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
						CASE 1			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
						CASE 2			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
					ENDSWITCH
				BREAK
				CASE SMS_SA_MCKENZIE_AIRFIELD
					SWITCH iProp
						CASE 0			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
						CASE 1			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
						CASE 2			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
					ENDSWITCH
				BREAK
				CASE SMS_SA_EPSILON_HQ
					SWITCH iProp
						CASE 0			RETURN prop_elecbox_15_cr
						CASE 1			RETURN prop_elecbox_14
						CASE 2			RETURN prop_worklight_04d
						CASE 3			RETURN prop_worklight_04d
						CASE 4			RETURN prop_worklight_03b
						CASE 5			RETURN prop_worklight_03b
						CASE 6			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_lights_02b"))
						CASE 7			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_lights_02b"))
						CASE 8			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_lights_02b"))
						CASE 9			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_lights_02b"))
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_BLACK_BOX
			SWITCH eSubvariation
				CASE SMS_BLB_MOUNT_GORDO
					SWITCH iProp
						CASE 0			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_heli"))
					ENDSWITCH
				BREAK
				CASE SMS_BLB_PALETO_FOREST
					SWITCH iProp
						CASE 0			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_heli"))
					ENDSWITCH
				BREAK
				CASE SMS_BLB_ISLAND
					SWITCH iProp
						CASE 0			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_heli"))
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_BOMB_BASE
			SWITCH eSubvariation
				CASE SMS_BB_TERMINAL
					SWITCH iProp
						CASE 0			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a")) 
						CASE 1			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a"))
						CASE 2			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a"))
						CASE 3			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a"))
						CASE 4			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a"))
						CASE 5			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a"))
						CASE 6			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a"))
						CASE 7			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a"))
						CASE 8			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a"))
						CASE 9			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a"))
						CASE 10			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a"))
						CASE 11			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a"))
					ENDSWITCH
				BREAK
				CASE SMS_BB_GRAPESEED_AUTOYARD
					SWITCH iProp
						CASE 0			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a"))
						CASE 1			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a"))
						CASE 2			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a"))
						CASE 3			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a"))
						CASE 4			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a"))
						CASE 5			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a"))
						CASE 6			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a"))
						CASE 7			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a"))
						CASE 8			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a"))
						CASE 9			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a"))
						CASE 10			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a"))
						CASE 11			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a"))
					ENDSWITCH
				BREAK
				CASE SMS_BB_PLANE_SCRAPYARD
					SWITCH iProp
						CASE 0			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a"))
						CASE 1			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a"))
						CASE 2			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a"))
						CASE 3			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a"))
						CASE 4			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a"))
						CASE 5			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a"))
						CASE 6			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a"))
						CASE 7			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a"))
						CASE 8			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a"))
						CASE 9			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a"))
						CASE 10			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a"))
						CASE 11			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a"))
						CASE 12			RETURN prop_roadcone01a
						CASE 13			RETURN prop_roadcone01a
						CASE 14			RETURN prop_roadcone01a
						CASE 15			RETURN prop_roadcone01a
						CASE 16			RETURN prop_roadcone01a
						CASE 17			RETURN prop_roadcone01a
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_SPLASH_LANDING
			SWITCH iProp
				CASE 0			RETURN prop_ind_barge_01_cr
			ENDSWITCH
		BREAK

		CASE SMV_ROOF_ATTACK
			SWITCH eSubvariation
				CASE SMS_RA_CONSTRUCTION_SITE
					SWITCH iProp
						CASE 0			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_crate_01a"))
						CASE 1			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_crate_01a"))
						CASE 2			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_crate_01a"))
						CASE 3			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_crate_01a"))
						CASE 4			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_crate_01a"))
						CASE 5			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_crate_01a"))
						CASE 6			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_crate_01a"))
						CASE 7			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_crate_01a"))
						CASE 8			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_crate_01a"))
						CASE 9			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_crate_01a"))
						CASE 10			RETURN prop_woodpile_01c
						CASE 11			RETURN prop_woodpile_01c
						CASE 12			RETURN prop_woodpile_01c
						CASE 13			RETURN prop_cementbags01
						CASE 14			RETURN prop_cementbags01
						CASE 15			RETURN prop_cementbags01
						CASE 16			RETURN prop_cementbags01
						CASE 17			RETURN prop_cementbags01
					ENDSWITCH
				BREAK
				CASE SMS_RA_ALTA_HOTEL
					SWITCH iProp
						CASE 0			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_crate_01a"))
						CASE 1			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_crate_01a"))
						CASE 2			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_crate_01a"))
						CASE 3			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_crate_01a"))
						CASE 4			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_crate_01a"))
						CASE 5			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_crate_01a"))
						CASE 6			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_crate_01a"))
						CASE 7			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_crate_01a"))
						CASE 8			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_crate_01a"))
						CASE 9			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_crate_01a"))
						CASE 10			RETURN prop_box_wood04a
						CASE 11			RETURN prop_box_wood04a
						CASE 12			RETURN prop_box_wood04a
						CASE 13			RETURN prop_box_wood04a
						CASE 14			RETURN prop_box_wood04a
						CASE 15			RETURN prop_box_wood04a
						CASE 16			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("bkr_prop_biker_bblock_lrg3"))
						CASE 17			RETURN prop_worklight_03b
						CASE 18			RETURN prop_worklight_03b
						CASE 19			RETURN prop_worklight_03b
					ENDSWITCH
				BREAK
				CASE SMS_RA_PACIFIC_STANDARD
					SWITCH iProp
						CASE 0			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_crate_01a"))
						CASE 1			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_crate_01a"))
						CASE 2			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_crate_01a"))
						CASE 3			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_crate_01a"))
						CASE 4			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_crate_01a"))
						CASE 5			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_crate_01a"))
						CASE 6			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_crate_01a"))
						CASE 7			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_crate_01a"))
						CASE 8			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_crate_01a"))
						CASE 9			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_crate_01a"))
						CASE 10			RETURN prop_box_wood04a
						CASE 11			RETURN prop_box_wood04a
						CASE 12			RETURN prop_box_wood04a
						CASE 13			RETURN prop_box_wood04a
						CASE 14			RETURN prop_worklight_03b
						CASE 15			RETURN prop_worklight_03b
						CASE 16			RETURN prop_shuttering03
						CASE 17			RETURN prop_shuttering03
						CASE 18			RETURN prop_shuttering03
						CASE 19			RETURN prop_shuttering03
						CASE 20			RETURN prop_worklight_03b
						CASE 21			RETURN prop_worklight_03b
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_INFILTRATION
			SWITCH eSubvariation
				CASE SMS_INF_HUMANE_LABS
					SWITCH iProp
						CASE 0			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
						CASE 1			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
						CASE 2			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
						CASE 3			RETURN prop_box_wood04a
						CASE 4			RETURN prop_box_wood04a
						CASE 5			RETURN prop_elecbox_17_cr
						CASE 6			RETURN prop_plas_barier_01a
						CASE 7			RETURN prop_plas_barier_01a
						CASE 8			RETURN prop_plas_barier_01a
					ENDSWITCH
				BREAK
				CASE SMS_INF_ZANCUDO
					SWITCH iProp
						CASE 0			RETURN prop_elecbox_24
						CASE 1			RETURN prop_worklight_03b
						CASE 2			RETURN prop_worklight_03b
						CASE 3			RETURN prop_worklight_03b
						CASE 4			RETURN prop_worklight_03b
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_BOMB_ROOF
			SWITCH iProp
				CASE 0			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a"))
				CASE 1			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a"))
				CASE 2			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_cratespile_01a"))
				CASE 3			RETURN prop_barrel_exp_01c
				CASE 4			RETURN prop_barrel_exp_01c
				CASE 5			RETURN prop_barrel_exp_01c
				CASE 6			RETURN prop_barrel_exp_01c
				CASE 7			RETURN prop_barrel_exp_01c
				CASE 8			RETURN prop_barrel_exp_01c
				CASE 9			RETURN prop_barrel_exp_01c
			ENDSWITCH
		BREAK
		
		CASE SMV_BEACON_GRAB			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_Prop_Smug_Jammer"))
		
		// Sell
		CASE SMV_SELL_HEAVY_LIFTING
			RETURN prop_ind_barge_01_cr
			
		CASE SMV_SELL_PRECISION_DELIVERY
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_cont_01a"))
//			RETURN PROP_CONTR_03B_LD
			
		CASE SMV_SETUP_RON
			SWITCH eSubvariation
				CASE SMS_SETUP_DELPERROBEACH 
				CASE SMS_SETUP_LSRIVER
				CASE SMS_SETUP_ELBURRO
				CASE SMS_SETUP_CHUMASH
				CASE SMS_SETUP_TATAVIUM
				CASE SMS_SETUP_RONSWINDFARM
				CASE SMS_SETUP_GRANDSENORA1
				CASE SMS_SETUP_GRANDSENORA2
				CASE SMS_SETUP_SANDY
				CASE SMS_SETUP_STAB
				CASE SMS_SETUP_CALAFIA
				CASE SMS_SETUP_MCKENZIE
				CASE SMS_SETUP_GRAPESEED
				CASE SMS_SETUP_PORCOPIO
				CASE SMS_SETUP_PALETO
					SWITCH iProp
						CASE 0			RETURN prop_box_wood01a
						CASE 1			RETURN prop_box_wood02a
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK	
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC VECTOR GET_SMUGGLER_VARIATION_PROP_LOCATION(INT iProp, SMUGGLER_VARIATION eVariation, SMUGGLER_SUBVARIATION eSubvariation, INT iExtraParam = -1, HANGAR_ID eHangar = HANGAR_ID_INVALID, INT iRespawnLocation = 0)
	
	SWITCH eVariation
	
		CASE SMV_STEAL_AIRCRAFT
			SWITCH eSubvariation
				CASE SMS_SA_SANDY_SHORES
					SWITCH iProp
						CASE 0			RETURN <<1726.5070, 3288.5317, 40.1832>>
						CASE 1			RETURN <<1747.5190, 3294.1489, 40.0950>>
						CASE 2			RETURN <<1751.4196, 3294.1899, 40.1339>>
					ENDSWITCH
				BREAK
				CASE SMS_SA_MCKENZIE_AIRFIELD
					SWITCH iProp
						CASE 0			RETURN <<2119.6475, 4788.5083, 40.1355>>
						CASE 1			RETURN <<2138.1533, 4799.0483, 40.1440>>
						CASE 2			RETURN <<2141.5098, 4800.0190, 40.1179>>
					ENDSWITCH
				BREAK
				CASE SMS_SA_EPSILON_HQ
					SWITCH iProp
						CASE 0			RETURN <<-706.0370, 71.9896, 68.6906>>
						CASE 1			RETURN <<-698.7879, 59.4819, 68.6906>>
						CASE 2			RETURN <<-694.0869, 71.4397, 59.3617>>
						CASE 3			RETURN <<-688.0873, 80.3286, 55.8244>>
						CASE 4			RETURN <<-704.8350, 70.1889, 68.6813>>
						CASE 5			RETURN <<-697.4631, 53.1259, 68.6815>>
						CASE 6			RETURN <<-675.4540, 72.4730, 68.6850>>
						CASE 7			RETURN <<-679.5390, 81.1610, 68.6850>>
						CASE 8			RETURN <<-685.6540, 78.2520, 68.6850>>
						CASE 9			RETURN <<-681.6310, 69.5910, 68.6850>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_BLACK_BOX
			SWITCH eSubvariation
				CASE SMS_BLB_MOUNT_GORDO
					SWITCH iProp
						CASE 0			RETURN <<2568.9060, 6169.3970, 159.7700>>
					ENDSWITCH
				BREAK
				CASE SMS_BLB_PALETO_FOREST
					SWITCH iProp
						CASE 0			RETURN <<-405.8150, 5808.4219, 53.8150>>
					ENDSWITCH
				BREAK
				CASE SMS_BLB_ISLAND
					SWITCH iProp
						CASE 0			RETURN <<2791.9495, -1512.5062, 16.0495>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_BOMB_BASE
			SWITCH eSubvariation
				CASE SMS_BB_TERMINAL
					SWITCH iProp
						CASE 0			RETURN <<1295.7380, -3345.3030, 5.0310>>
						CASE 1			RETURN <<1293.0229, -3348.0620, 4.9030>>
						CASE 2			RETURN <<1289.9940, -3343.0210, 4.9030>>
						CASE 3			RETURN <<1275.6763, -3325.0552, 4.9028>>
						CASE 4			RETURN <<1273.7234, -3329.7141, 4.9028>>
						CASE 5			RETURN <<1270.7594, -3325.7864, 4.9124>>
						CASE 6			RETURN <<1294.7412, -3229.9390, 4.9082>>
						CASE 7			RETURN <<1293.5248, -3234.3545, 4.9172>>
						CASE 8			RETURN <<1289.1074, -3230.6519, 4.9060>>
						CASE 9			RETURN <<1242.1250, -3331.7261, 5.0300>>
						CASE 10			RETURN <<1244.3560, -3324.7690, 5.0300>>
						CASE 11			RETURN <<1245.2410, -3329.0049, 5.0300>>
					ENDSWITCH
				BREAK
				CASE SMS_BB_GRAPESEED_AUTOYARD
					SWITCH iProp
						CASE 0			RETURN <<2297.0190, 4869.8599, 40.8090>>
						CASE 1			RETURN <<2296.5640, 4874.4609, 40.8090>>
						CASE 2			RETURN <<2301.1030, 4867.2930, 40.8090>>
						CASE 3			RETURN <<2322.9961, 4905.4580, 40.8260>>
						CASE 4			RETURN <<2322.4861, 4900.1011, 40.8090>>
						CASE 5			RETURN <<2325.4590, 4908.1821, 40.8410>>
						CASE 6			RETURN <<2301.0540, 4929.4580, 40.4240>>
						CASE 7			RETURN <<2297.8870, 4926.3042, 40.4290>>
						CASE 8			RETURN <<2294.8860, 4931.8872, 40.4370>>
						CASE 9			RETURN <<2281.0830, 4940.2710, 40.4350>>
						CASE 10			RETURN <<2277.9670, 4944.7158, 40.2960>>
						CASE 11			RETURN <<2282.4509, 4944.7002, 40.4010>>
					ENDSWITCH
				BREAK
				CASE SMS_BB_PLANE_SCRAPYARD
					SWITCH iProp
						CASE 0			RETURN <<2381.6689, 3033.2710, 47.1540>>
						CASE 1			RETURN <<2382.7510, 3039.0100, 47.1540>>
						CASE 2			RETURN <<2379.2129, 3037.1089, 47.1540>>
						CASE 3			RETURN <<2405.4609, 3040.1650, 47.1540>>
						CASE 4			RETURN <<2410.3701, 3037.6360, 47.1540>>
						CASE 5			RETURN <<2409.5750, 3043.0100, 47.1540>>
						CASE 6			RETURN <<2349.9080, 3071.2529, 47.1530>>
						CASE 7			RETURN <<2351.6121, 3065.8931, 47.1700>>
						CASE 8			RETURN <<2355.1370, 3071.7319, 47.1720>>
						CASE 9			RETURN <<2373.4309, 3149.7729, 47.2100>>
						CASE 10			RETURN <<2374.5991, 3145.4160, 47.3430>>
						CASE 11			RETURN <<2378.3230, 3149.8220, 47.0900>>
						CASE 12			RETURN <<2440.2271, 3118.4109, 46.6030>>
						CASE 13			RETURN <<2450.2991, 3118.3369, 46.8400>>
						CASE 14			RETURN <<2460.7810, 3118.4080, 47.6250>>
						CASE 15			RETURN <<2461.1980, 3104.4309, 47.5730>>
						CASE 16			RETURN <<2450.7351, 3103.8411, 46.7620>>
						CASE 17			RETURN <<2440.7881, 3103.4041, 46.5140>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_SPLASH_LANDING
			SWITCH eSubvariation
				CASE SMS_SL_LAGO_ZANCUDO
					SWITCH iProp
						CASE 0			RETURN <<-3244.6755, 3019.3257, -1.0>>
					ENDSWITCH
				BREAK
				CASE SMS_SL_PACIFIC_OCEAN_E
					SWITCH iProp
						CASE 0			RETURN <<3853.2813, 3041.8894, -1.0>>
					ENDSWITCH
				BREAK
				CASE SMS_SL_PACIFIC_OCEAN_NE
					SWITCH iProp
						CASE 0			RETURN <<3365.5244, 6295.7349, -1.0>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_ROOF_ATTACK
			SWITCH eSubvariation
				CASE SMS_RA_CONSTRUCTION_SITE
					SWITCH iProp
						CASE 0			RETURN <<-177.9828, -1005.0557, 253.1313>>
						CASE 1			RETURN <<-159.7218, -1016.6276, 253.1313>>
						CASE 2			RETURN <<-162.8824, -991.1367, 253.1313>>
						CASE 3			RETURN <<-168.6418, -979.2914, 253.1313>>
						CASE 4			RETURN <<-156.0735, -962.7341, 253.1313>>
						CASE 5			RETURN <<-154.6575, -955.2259, 253.1313>>
						CASE 6			RETURN <<-140.4603, -953.1813, 253.1313>>
						CASE 7			RETURN <<-140.0991, -972.8619, 253.1313>>
						CASE 8			RETURN <<-145.4883, -965.3784, 253.1313>>
						CASE 9			RETURN <<-153.2574, -948.6885, 253.1313>>
						CASE 10			RETURN <<-161.8032, -990.0978, 253.1371>>
						CASE 11			RETURN <<-159.5047, -994.0595, 253.1371>>
						CASE 12			RETURN <<-144.0970, -965.6150, 253.1370>>
						CASE 13			RETURN <<-152.9420, -987.5710, 253.1320>>
						CASE 14			RETURN <<-154.7210, -986.8570, 253.1320>>
						CASE 15			RETURN <<-150.3827, -971.1959, 253.1320>>
						CASE 16			RETURN <<-151.7968, -970.4783, 253.1320>>
						CASE 17			RETURN <<-148.2075, -954.0214, 253.1320>>
					ENDSWITCH
				BREAK
				CASE SMS_RA_ALTA_HOTEL
					SWITCH iProp
						CASE 0			RETURN <<448.6235, -263.3119, 70.2381>>
						CASE 1			RETURN <<418.2304, -261.1306, 70.2553>>
						CASE 2			RETURN <<419.9077, -245.4991, 73.2511>>
						CASE 3			RETURN <<399.3304, -251.4556, 73.2518>>
						CASE 4			RETURN <<410.1058, -258.8917, 70.2394>>
						CASE 5			RETURN <<441.1131, -268.1046, 70.2421>>
						CASE 6			RETURN <<398.8911, -243.8948, 73.2518>>
						CASE 7			RETURN <<434.3204, -255.3302, 70.2515>>
						CASE 8			RETURN <<405.0283, -254.0187, 70.2394>>
						CASE 9			RETURN <<444.2440, -259.4925, 70.2470>>
						CASE 10			RETURN <<440.8640, -260.1800, 70.2510>>
						CASE 11			RETURN <<441.1350, -262.7400, 70.2500>>
						CASE 12			RETURN <<413.3410, -234.8810, 73.2540>>
						CASE 13			RETURN <<440.0820, -269.7170, 70.2450>>
						CASE 14			RETURN <<422.9870, -256.8270, 70.2540>>
						CASE 15			RETURN <<398.2010, -249.5330, 73.2550>>
						CASE 16			RETURN <<460.5201, -183.7146, 69.0002>>
						CASE 17			RETURN <<402.0390, -237.5760, 73.2476>>
						CASE 18			RETURN <<450.7440, -257.9403, 70.2338>>
						CASE 19			RETURN <<421.5104, -262.2006, 70.2467>>
					ENDSWITCH
				BREAK
				CASE SMS_RA_PACIFIC_STANDARD
					SWITCH iProp
						CASE 0			RETURN <<274.8170, 221.7910, 136.7570>>
						CASE 1			RETURN <<260.3500, 215.6050, 136.7570>>
						CASE 2			RETURN <<289.6048, 252.6826, 122.8194>>
						CASE 3			RETURN <<254.2712, 254.6904, 136.7573>>
						CASE 4			RETURN <<263.6000, 262.9600, 136.7570>>
						CASE 5			RETURN <<281.9490, 295.8370, 131.0910>>
						CASE 6			RETURN <<296.3520, 303.8110, 131.0910>>
						CASE 7			RETURN <<306.2100, 296.9870, 131.0910>>
						CASE 8			RETURN <<289.8440, 254.9680, 122.8190>>
						CASE 9			RETURN <<286.4490, 289.1100, 131.0910>>
						CASE 10			RETURN <<254.7686, 264.1141, 136.7601>>
						CASE 11			RETURN <<261.6655, 250.5158, 136.7601>>
						CASE 12			RETURN <<280.7200, 293.3000, 131.0940>>
						CASE 13			RETURN <<272.4819, 220.9961, 136.7601>>
						CASE 14			RETURN <<256.5048, 251.0138, 136.7531>>
						CASE 15			RETURN <<275.6640, 299.6690, 131.0870>>
						CASE 16			RETURN <<276.3950, 270.0450, 135.4540>>
						CASE 17			RETURN <<277.7870, 274.0530, 132.9230>>
						CASE 18			RETURN <<274.3690, 270.7390, 135.5030>>
						CASE 19			RETURN <<275.8000, 274.7130, 132.9930>>
						CASE 20			RETURN <<269.6210, 216.3370, 136.7530>>
						CASE 21			RETURN <<290.7770, 256.7770, 122.8150>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_INFILTRATION
			SWITCH eSubvariation
				CASE SMS_INF_HUMANE_LABS
					SWITCH iProp
						CASE 0			RETURN <<3562.5210, 3667.9221, 32.9170>>
						CASE 1			RETURN <<3556.6060, 3658.0669, 32.8790>>
						CASE 2			RETURN <<3579.7051, 3662.2209, 32.8890>>
						CASE 3			RETURN <<3564.5520, 3670.8240, 32.8910>>
						CASE 4			RETURN <<3561.9050, 3670.8569, 32.8910>>
						CASE 5			RETURN <<3582.1331, 3696.3430, 35.6480>>
						CASE 6			RETURN <<3540.4170, 3661.9121, 32.8930>>
						CASE 7			RETURN <<3540.0210, 3658.6140, 32.8930>>
						CASE 8			RETURN <<3541.3330, 3670.9199, 32.8930>>
					ENDSWITCH
				BREAK
				CASE SMS_INF_ZANCUDO
					SWITCH iProp
						CASE 0			RETURN <<-1931.3080, 3318.9790, 31.9600>>
						CASE 1			RETURN <<-1955.6147, 3316.6489, 31.9559>>
						CASE 2			RETURN <<-1966.4412, 3322.7671, 31.9559>>
						CASE 3			RETURN <<-1953.1134, 3344.6846, 31.9559>>
						CASE 4			RETURN <<-1940.2501, 3337.7549, 31.9559>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_BEACON_GRAB
			SWITCH eSubvariation
				CASE SMS_BG_LOCATION_1
					SWITCH iProp
						CASE 0			RETURN <<105.2605, 6618.7871, 38.0079>>
						CASE 1			RETURN <<-1001.4290, 4851.4175, 273.6111>>
						CASE 2			RETURN <<2808.1067, 5979.9316, 354.1196>>
						CASE 3			RETURN <<1990.2489, 5027.0791, 60.6082>>
						CASE 4			RETURN <<1548.6837, 3797.9263, 37.1352>>
						CASE 5			RETURN <<-1955.7577, 1771.1031, 181.9124>>
						CASE 6			RETURN <<-274.3548, 2196.4780, 132.1872>>
						CASE 7			RETURN <<2165.4888, 3325.8943, 50.5579>>
						CASE 8			RETURN <<2569.9148, 1273.2241, 51.0510>>
						CASE 9			RETURN <<1494.8678, 1106.4763, 119.5029>>
						CASE 10			RETURN <<754.4335, 564.9361, 141.9693>>
						CASE 11			RETURN <<-1211.8850, -1273.7335, 20.8495>>
						CASE 12			RETURN <<-554.6368, -107.1006, 53.7405>>
						CASE 13			RETURN <<428.9457, 17.7107, 151.9554>>
						CASE 14			RETURN <<1739.0208, -1459.5750, 119.0625>>
						CASE 15			RETURN <<852.1704, -3189.9153, 13.5022>>
						CASE 16			RETURN <<555.1158, -2210.6919, 68.2929>>
						CASE 17			RETURN <<-610.8616, -1612.4015, 44.5379>>
						CASE 18			RETURN <<268.0793, -1115.0341, 87.8146>>
						CASE 19			RETURN <<-272.2942, -955.3922, 143.6003>>
					ENDSWITCH
				BREAK
				CASE SMS_BG_LOCATION_2
					SWITCH iProp
						CASE 0			RETURN <<-71.6530, 6213.4316, 41.2798>>
						CASE 1			RETURN <<-1852.9340, 4656.8457, 45.5719>>
						CASE 2			RETURN <<3419.2332, 5164.7622, 4.8611>>
						CASE 3			RETURN <<1302.2863, 4344.3760, 46.6279>>
						CASE 4			RETURN <<-1820.2080, 793.3373, 141.6514>>
						CASE 5			RETURN <<-2623.7766, 1901.2100, 162.4561>>
						CASE 6			RETURN <<1244.9802, 1876.7743, 95.1041>>
						CASE 7			RETURN <<2659.3381, 3491.2949, 58.2301>>
						CASE 8			RETURN <<2804.4150, 1453.8107, 33.3519>>
						CASE 9			RETURN <<669.5452, 1279.9999, 359.2960>>
						CASE 10			RETURN <<-1809.1782, -357.8807, 94.9921>>
						CASE 11			RETURN <<-1827.2637, -1191.8013, 27.1609>>
						CASE 12			RETURN <<-306.5563, 202.4166, 174.4954>>
						CASE 13			RETURN <<472.0296, -99.1670, 122.7075>>
						CASE 14			RETURN <<1612.1294, -2243.5249, 131.7942>>
						CASE 15			RETURN <<258.4716, -3303.9326, 44.2467>>
						CASE 16			RETURN <<1008.3665, -2429.3113, 42.8303>>
						CASE 17			RETURN <<-353.4998, -1390.9008, 42.7370>>
						CASE 18			RETURN <<86.6910, -1113.3568, 67.5423>>
						CASE 19			RETURN <<-196.1527, -741.4399, 218.9722>>
					ENDSWITCH
				BREAK
				CASE SMS_BG_LOCATION_3
					SWITCH iProp
						CASE 0			RETURN <<-362.3446, 6105.7686, 45.3743>>
						CASE 1			RETURN <<-2181.5464, 4296.8340, 52.8154>>
						CASE 2			RETURN <<3718.9036, 4538.2734, 24.9775>>
						CASE 3			RETURN <<-212.7213, 3667.6877, 63.4125>>
						CASE 4			RETURN <<-2295.4121, 258.4507, 193.6115>>
						CASE 5			RETURN <<-2526.5012, 2315.1108, 38.4921>>
						CASE 6			RETURN <<717.2358, 2535.1946, 77.9927>>
						CASE 7			RETURN <<3610.5981, 3640.3933, 51.2199>>
						CASE 8			RETURN <<2588.2087, 482.9003, 113.9744>>
						CASE 9			RETURN <<182.5070, 1110.6672, 235.7942>>
						CASE 10			RETURN <<-1367.0612, 129.4187, 59.6439>>
						CASE 11			RETURN <<-1414.9575, -663.7548, 38.1736>>
						CASE 12			RETURN <<-164.0682, -146.7017, 96.1529>>
						CASE 13			RETURN <<305.8657, -299.6016, 68.4284>>
						CASE 14			RETURN <<1234.3201, -1903.3467, 46.0905>>
						CASE 15			RETURN <<338.3328, -2724.4680, 44.6585>>
						CASE 16			RETURN <<877.6901, -1825.2241, 80.2021>>
						CASE 17			RETURN <<111.3095, -1768.6198, 34.3035>>
						CASE 18			RETURN <<99.9969, -866.6687, 136.9540>>
						CASE 19			RETURN <<-292.1132, -578.0060, 55.9135>>
					ENDSWITCH
				BREAK
				CASE SMS_BG_LOCATION_4
					SWITCH iProp
						CASE 0			RETURN <<-737.7020, 5588.1904, 50.5891>>
						CASE 1			RETURN <<442.6481, 5579.5020, 792.9248>>
						CASE 2			RETURN <<2866.4644, 4397.3828, 79.0733>>
						CASE 3			RETURN <<347.8882, 3401.1887, 41.2786>>
						CASE 4			RETURN <<-3002.4478, 65.6132, 20.6524>>
						CASE 5			RETURN <<-2655.1526, 2596.8320, 12.4445>>
						CASE 6			RETURN <<264.9839, 2867.0339, 73.1801>>
						CASE 7			RETURN <<2553.0566, 2594.7456, 41.9158>>
						CASE 8			RETURN <<2457.1997, -379.6754, 113.4318>>
						CASE 9			RETURN <<-446.9813, 1109.9910, 331.5306>>
						CASE 10			RETURN <<-936.2590, 461.0022, 97.1313>>
						CASE 11			RETURN <<-1014.2420, -767.1638, 78.8578>>
						CASE 12			RETURN <<210.4721, 281.5426, 140.4780>>
						CASE 13			RETURN <<935.1641, -207.8281, 81.7359>>
						CASE 14			RETURN <<1136.3298, -1384.6687, 53.3262>>
						CASE 15			RETURN <<55.2206, -2385.7004, 5.0000>>
						CASE 16			RETURN <<-543.8771, -2225.4829, 121.3706>>
						CASE 17			RETURN <<337.9226, -1624.3281, 97.4960>>
						CASE 18			RETURN <<-53.2512, -808.7504, 247.6936>>
						CASE 19			RETURN <<-674.7872, -1128.0010, 50.8586>>
					ENDSWITCH
				BREAK
				CASE SMS_BG_LOCATION_5
					SWITCH iProp
						CASE 0			RETURN <<-557.5379, 5328.3086, 87.5094>>
						CASE 1			RETURN <<1698.6150, 6417.2773, 37.1664>>
						CASE 2			RETURN <<2234.1179, 5614.5967, 57.2814>>
						CASE 3			RETURN <<907.1216, 3654.2922, 35.1605>>
						CASE 4			RETURN <<-3422.4680, 977.7936, 10.9079>>
						CASE 5			RETURN <<-1128.7351, 2680.6321, 22.8649>>
						CASE 6			RETURN <<1583.5087, 2914.9895, 60.1446>>
						CASE 7			RETURN <<2368.9951, 2184.8794, 139.3186>>
						CASE 8			RETURN <<1661.6116, 22.3937, 179.8814>>
						CASE 9			RETURN <<83.7169, 813.7457, 213.2910>>
						CASE 10			RETURN <<-1195.6333, -1788.1890, 18.4886>>
						CASE 11			RETURN <<-896.8290, -445.6172, 170.8292>>
						CASE 12			RETURN <<761.0652, 230.5637, 145.1288>>
						CASE 13			RETURN <<1217.2682, -456.5120, 77.8981>>
						CASE 14			RETURN <<1236.0145, -2916.3372, 28.6855>>
						CASE 15			RETURN <<225.1796, -2283.3074, 71.6990>>
						CASE 16			RETURN <<-398.8979, -1905.5012, 56.4266>>
						CASE 17			RETURN <<704.0060, -794.8538, 48.4017>>
						CASE 18			RETURN <<-94.4651, -812.0129, 294.7189>>
						CASE 19			RETURN <<765.2806, -1291.2601, 46.1139>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_BOMB_ROOF
			SWITCH eSubvariation
				CASE SMS_BRO_ROUTE_1
					SWITCH iRespawnLocation
						CASE 0
							SWITCH iProp
								CASE 0			RETURN <<1000.5460, 62.7700, 103.0190>>
								CASE 1			RETURN <<988.5330, 56.2800, 103.0190>>
								CASE 2			RETURN <<989.6040, 52.8310, 103.0190>>
								CASE 3			RETURN <<981.3890, 68.5700, 107.2180>>
								CASE 4			RETURN <<1002.7370, 63.2220, 103.4680>>
								CASE 5			RETURN <<984.0940, 39.8570, 103.4680>>
								CASE 6			RETURN <<994.4930, 56.0490, 103.4680>>
								CASE 7			RETURN <<995.1980, 64.9580, 103.4680>>
								CASE 8			RETURN <<990.3970, 69.9020, 107.2180>>
								CASE 9			RETURN <<985.7290, 52.4130, 103.4680>>
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH iProp
								CASE 0			RETURN <<-1011.0560, -736.0200, 75.5380>>
								CASE 1			RETURN <<-1008.0470, -740.4210, 75.5380>>
								CASE 2			RETURN <<-1006.3310, -736.1820, 75.5380>>
								CASE 3			RETURN <<-993.4340, -747.0310, 75.9790>>
								CASE 4			RETURN <<-995.5090, -729.1990, 75.9790>>
								CASE 5			RETURN <<-1008.8250, -743.2780, 75.9790>>
								CASE 6			RETURN <<-1000.7640, -760.7060, 79.3000>>
								CASE 7			RETURN <<-1014.6600, -752.6120, 79.2990>>
								CASE 8			RETURN <<-1011.4520, -762.5660, 79.3000>>
								CASE 9			RETURN <<-998.9900, -738.8620, 75.9790>>
							ENDSWITCH
						BREAK
						CASE 2													
							SWITCH iProp									
								CASE 0			RETURN <<-776.5440, 783.1520, 212.9640>>
								CASE 1			RETURN <<-772.0280, 782.7950, 212.9640>>
								CASE 2			RETURN <<-764.5980, 767.2120, 212.2000>>
								CASE 3			RETURN <<-768.9330, 764.6890, 212.6410>>
								CASE 4			RETURN <<-774.4430, 781.4140, 213.4050>>
								CASE 5			RETURN <<-758.5160, 789.8210, 214.0190>>
								CASE 6			RETURN <<-754.2170, 801.9650, 219.5880>>
								CASE 7			RETURN <<-736.4400, 785.7890, 212.6410>>
								CASE 8			RETURN <<-750.6700, 770.7940, 212.6410>>
								CASE 9			RETURN <<-771.7600, 772.4840, 212.6410>>
							ENDSWITCH									
						BREAK										
						CASE 3										
							SWITCH iProp									
								CASE 0			RETURN <<781.7780, -1710.7841, 43.1480>>
								CASE 1			RETURN <<783.8160, -1705.8180, 43.1480>>
								CASE 2			RETURN <<785.7920, -1709.6830, 43.1480>>
								CASE 3			RETURN <<767.2380, -1711.1740, 43.5890>>
								CASE 4			RETURN <<778.7480, -1702.9490, 43.5890>>
								CASE 5			RETURN <<778.3250, -1712.1210, 43.5890>>
								CASE 6			RETURN <<782.7480, -1719.2340, 43.5890>>
								CASE 7			RETURN <<788.6400, -1692.7310, 43.5890>>
								CASE 8			RETURN <<789.2580, -1705.0300, 43.5890>>
								CASE 9			RETURN <<781.5380, -1688.8400, 43.6470>>
							ENDSWITCH									
						BREAK										
						CASE 4										
							SWITCH iProp									
								CASE 0			RETURN <<-2202.3491, 262.1050, 197.1100>>
								CASE 1			RETURN <<-2202.7500, 266.4160, 197.1100>>
								CASE 2			RETURN <<-2215.4919, 267.8820, 197.1100>>
								CASE 3			RETURN <<-2215.4751, 271.8140, 197.5590>>
								CASE 4			RETURN <<-2218.7939, 264.3470, 197.5590>>
								CASE 5			RETURN <<-2206.4661, 255.5370, 197.5590>>
								CASE 6			RETURN <<-2200.4590, 268.3480, 197.5590>>
								CASE 7			RETURN <<-2208.8989, 279.0910, 197.5590>>
								CASE 8			RETURN <<-2209.5481, 267.7590, 197.5590>>
								CASE 9			RETURN <<-2213.4819, 253.7650, 197.5590>>
							ENDSWITCH									
						BREAK										
					ENDSWITCH											
				BREAK												
				CASE SMS_BRO_ROUTE_2												
					SWITCH iRespawnLocation											
						CASE 0										
							SWITCH iProp									
								CASE 0			RETURN <<36.4830, 170.1960, 125.2180>>
								CASE 1			RETURN <<41.9380, 171.9710, 125.2180>>
								CASE 2			RETURN <<39.9690, 166.6530, 125.2180>>
								CASE 3			RETURN <<35.4060, 161.7060, 125.6590>>
								CASE 4			RETURN <<51.3950, 164.9750, 125.6590>>
								CASE 5			RETURN <<46.2740, 156.0050, 125.6590>>
								CASE 6			RETURN <<38.8750, 171.0140, 125.6590>>
								CASE 7			RETURN <<41.8890, 180.7470, 125.6590>>
								CASE 8			RETURN <<30.6530, 172.7830, 125.6590>>
								CASE 9			RETURN <<45.9481, 171.3494, 125.6590>>
							ENDSWITCH									
						BREAK										
						CASE 1										
							SWITCH iProp									
								CASE 0			RETURN <<63.4940, -1012.4970, 78.7990>>
								CASE 1			RETURN <<67.2300, -1021.1340, 78.7710>>
								CASE 2			RETURN <<59.7080, -1013.3090, 78.8020>>
								CASE 3			RETURN <<54.2040, -1021.8200, 79.1880>>
								CASE 4			RETURN <<61.7830, -1009.2580, 79.1880>>
								CASE 5			RETURN <<56.0680, -1015.9750, 79.1880>>
								CASE 6			RETURN <<66.8010, -1024.3340, 79.1880>>
								CASE 7			RETURN <<49.2970, -1015.4410, 82.8480>>
								CASE 8			RETURN <<62.8970, -1019.0760, 79.1880>>
								CASE 9			RETURN <<62.2330, -1031.5350, 79.1880>>
							ENDSWITCH									
						BREAK										
						CASE 2										
							SWITCH iProp									
								CASE 0			RETURN <<-1649.1591, -514.5010, 77.5090>>
								CASE 1			RETURN <<-1649.3970, -528.4010, 74.0590>>
								CASE 2			RETURN <<-1645.3929, -526.5630, 74.0590>>
								CASE 3			RETURN <<-1634.9540, -507.1400, 74.5040>>
								CASE 4			RETURN <<-1636.0570, -517.7710, 74.5030>>
								CASE 5			RETURN <<-1647.4919, -510.7560, 77.9520>>
								CASE 6			RETURN <<-1652.5470, -521.9690, 74.5030>>
								CASE 7			RETURN <<-1646.4410, -529.6430, 74.5030>>
								CASE 8			RETURN <<-1660.8130, -528.6970, 74.5020>>
								CASE 9			RETURN <<-1651.6899, -516.6290, 77.9520>>
							ENDSWITCH									
						BREAK										
						CASE 3										
							SWITCH iProp									
								CASE 0			RETURN <<-35.7470, -591.4020, 105.3290>>
								CASE 1			RETURN <<-32.8200, -587.9840, 105.3290>>
								CASE 2			RETURN <<-27.2540, -595.2820, 105.3290>>
								CASE 3			RETURN <<-22.1110, -593.5810, 105.7780>>
								CASE 4			RETURN <<-26.2170, -609.3490, 111.4540>>
								CASE 5			RETURN <<-39.9900, -602.1990, 111.4540>>
								CASE 6			RETURN <<-35.3160, -593.7270, 105.7780>>
								CASE 7			RETURN <<-31.1050, -580.5660, 105.7780>>
								CASE 8			RETURN <<-28.5410, -589.4490, 105.7780>>
								CASE 9			RETURN <<-18.2970, -585.7130, 105.7780>>
							ENDSWITCH									
						BREAK										
						CASE 4										
							SWITCH iProp									
								CASE 0			RETURN <<-711.2740, 673.1450, 157.5880>>
								CASE 1			RETURN <<-711.0010, 678.2000, 157.5880>>
								CASE 2			RETURN <<-707.1990, 675.2280, 157.5880>>
								CASE 3			RETURN <<-700.1850, 696.0940, 169.4860>>
								CASE 4			RETURN <<-712.4590, 701.6050, 169.4860>>
								CASE 5			RETURN <<-713.9840, 676.2280, 158.0370>>
								CASE 6			RETURN <<-714.9090, 690.0760, 158.0860>>
								CASE 7			RETURN <<-705.6000, 683.5890, 158.0370>>
								CASE 8			RETURN <<-694.1980, 680.8630, 158.0370>>
								CASE 9			RETURN <<-706.6810, 671.8390, 158.0370>>
							ENDSWITCH									
						BREAK										
					ENDSWITCH											
				BREAK												
				CASE SMS_BRO_ROUTE_3												
					SWITCH iRespawnLocation											
						CASE 0										
							SWITCH iProp									
								CASE 0			RETURN <<-648.5130, 322.3500, 139.1530>>
								CASE 1			RETURN <<-646.8940, 318.1440, 139.1530>>
								CASE 2			RETURN <<-659.3800, 313.8170, 139.1530>>
								CASE 3			RETURN <<-651.6380, 316.8340, 139.5940>>
								CASE 4			RETURN <<-662.9710, 321.4470, 139.5940>>
								CASE 5			RETURN <<-659.4970, 311.0080, 139.5940>>
								CASE 6			RETURN <<-639.4550, 309.8520, 139.5940>>
								CASE 7			RETURN <<-638.4010, 322.7300, 139.5940>>
								CASE 8			RETURN <<-644.8520, 321.8720, 139.5940>>
								CASE 9			RETURN <<-655.2630, 325.7180, 139.5940>>
							ENDSWITCH									
						BREAK										
						CASE 1										
							SWITCH iProp									
								CASE 0			RETURN <<-216.6390, -840.6790, 124.2580>>
								CASE 1			RETURN <<-220.8950, -840.2100, 124.2580>>
								CASE 2			RETURN <<-220.1350, -828.0730, 125.7940>>
								CASE 3			RETURN <<-226.5000, -822.4090, 126.2930>>
								CASE 4			RETURN <<-216.7480, -830.4740, 126.2930>>
								CASE 5			RETURN <<-217.9230, -843.8570, 124.7570>>
								CASE 6			RETURN <<-226.8290, -840.5040, 124.7570>>
								CASE 7			RETURN <<-226.1950, -834.6380, 124.7570>>
								CASE 8			RETURN <<-236.4840, -837.8820, 124.7570>>
								CASE 9			RETURN <<-230.4180, -843.7190, 128.3910>>
							ENDSWITCH									
						BREAK										
						CASE 2										
							SWITCH iProp									
								CASE 0			RETURN <<754.7770, -1780.6670, 48.2930>>
								CASE 1			RETURN <<755.6300, -1785.6820, 48.2930>>
								CASE 2			RETURN <<766.5400, -1784.1370, 48.2930>>
								CASE 3			RETURN <<756.2030, -1796.3390, 48.7340>>
								CASE 4			RETURN <<762.8970, -1787.6340, 48.7340>>
								CASE 5			RETURN <<752.3660, -1783.7100, 48.7340>>
								CASE 6			RETURN <<764.2920, -1775.8750, 52.7350>>
								CASE 7			RETURN <<769.7870, -1783.1010, 48.7340>>
								CASE 8			RETURN <<765.0240, -1796.3700, 48.7340>>
								CASE 9			RETURN <<759.4460, -1779.0330, 48.7340>>
							ENDSWITCH									
						BREAK										
						CASE 3										
							SWITCH iProp									
								CASE 0			RETURN <<-827.9370, -723.2580, 120.2740>>
								CASE 1			RETURN <<-821.9760, -699.4200, 120.2740>>
								CASE 2			RETURN <<-824.4100, -725.9690, 120.2740>>
								CASE 3			RETURN <<-839.7690, -719.9890, 120.7230>>
								CASE 4			RETURN <<-829.5000, -716.2100, 120.7230>>
								CASE 5			RETURN <<-829.1980, -726.4960, 120.7230>>
								CASE 6			RETURN <<-824.6910, -721.5120, 120.7230>>
								CASE 7			RETURN <<-819.0220, -722.5330, 120.7230>>
								CASE 8			RETURN <<-818.7690, -714.3640, 123.6880>>
								CASE 9			RETURN <<-818.8460, -700.3870, 120.7230>>
							ENDSWITCH									
						BREAK										
						CASE 4										
							SWITCH iProp									
								CASE 0			RETURN <<305.4260, 113.9260, 144.1700>>
								CASE 1			RETURN <<308.8740, 116.5150, 144.1700>>
								CASE 2			RETURN <<318.8910, 108.9930, 144.1700>>
								CASE 3			RETURN <<305.9510, 121.4930, 144.6190>>
								CASE 4			RETURN <<302.0440, 114.1070, 144.6190>>
								CASE 5			RETURN <<310.8460, 113.1440, 144.6190>>
								CASE 6			RETURN <<307.3910, 131.1780, 144.6190>>
								CASE 7			RETURN <<315.8480, 124.0800, 144.6190>>
								CASE 8			RETURN <<331.0890, 106.6580, 144.6190>>
								CASE 9			RETURN <<318.9350, 112.0060, 144.6190>>
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_SELL_PRECISION_DELIVERY
			SWITCH eHangar
				CASE LSIA_HANGAR_1
					SWITCH iExtraParam
						CASE 0
							SWITCH iProp
								CASE 0			RETURN <<-993.5030, -3298.9609, 13.0460>>
								CASE 1			RETURN <<-986.6900, -3302.6489, 13.0460>>
								CASE 2			RETURN <<-979.6090, -3306.5139, 13.0460>>
								CASE 3			RETURN <<-989.3930, -3291.4851, 13.0460>>
								CASE 4			RETURN <<-982.5890, -3295.0669, 13.0460>>
								CASE 5			RETURN <<-975.4690, -3299.0459, 13.0460>>
								CASE 6			RETURN <<-984.8000, -3283.4231, 13.0460>>
								CASE 7			RETURN <<-978.1520, -3287.0449, 13.0460>>
								CASE 8			RETURN <<-971.1170, -3291.2180, 13.0460>>
								CASE 9			RETURN <<-973.6890, -3310.2090, 13.0460>>
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH iProp
								CASE 0			RETURN <<-967.1040, -3313.9241, 13.0460>>
								CASE 1			RETURN <<-960.3720, -3317.5339, 13.0460>>
								CASE 2			RETURN <<-969.6580, -3302.7361, 13.0460>>
								CASE 3			RETURN <<-962.9290, -3306.4480, 13.0460>>
								CASE 4			RETURN <<-956.5810, -3310.1050, 13.0460>>
								CASE 5			RETURN <<-965.4960, -3295.0400, 13.0460>>
								CASE 6			RETURN <<-958.7760, -3298.7610, 13.0460>>
								CASE 7			RETURN <<-952.4380, -3302.3369, 13.0460>>
								CASE 8			RETURN <<-1019.0587, -3284.2649, 13.2242>>
								CASE 9			RETURN <<-1012.4613, -3288.3845, 13.3121>>
							ENDSWITCH
						BREAK
						CASE 2
							SWITCH iProp
								CASE 0			RETURN <<-1005.3160, -3292.3826, 13.3121>>
								CASE 1			RETURN <<-1014.3723, -3276.2334, 13.0457>>
								CASE 2			RETURN <<-1007.7657, -3280.2078, 13.0457>>
								CASE 3			RETURN <<-1000.7748, -3284.5615, 13.0457>>
								CASE 4			RETURN <<-1009.8502, -3268.4309, 13.2346>>
								CASE 5			RETURN <<-1003.2211, -3272.5862, 13.3121>>
								CASE 6			RETURN <<-996.2583, -3277.0015, 13.3121>>
								CASE 7			RETURN <<-1038.3351, -3273.0159, 13.2475>>
								CASE 8			RETURN <<-1045.0386, -3269.2649, 13.2873>>
								CASE 9			RETURN <<-1051.6494, -3265.5806, 13.2842>>
							ENDSWITCH
						BREAK
						CASE 3
							SWITCH iProp
								CASE 0			RETURN <<-1033.9274, -3265.1826, 13.2525>>
								CASE 1			RETURN <<-1040.8551, -3261.4194, 13.2918>>
								CASE 2			RETURN <<-1047.3328, -3257.8076, 13.2917>>
								CASE 3			RETURN <<-1029.5009, -3257.3057, 13.2577>>
								CASE 4			RETURN <<-1036.4540, -3253.4148, 13.2994>>
								CASE 5			RETURN <<-1043.2061, -3249.6504, 13.2994>>
								CASE 6			RETURN <<-1059.3986, -3260.4902, 13.2770>>
								CASE 7			RETURN <<-1066.0569, -3256.5137, 13.2755>>
								CASE 8			RETURN <<-1073.0608, -3252.4019, 13.2709>>
								CASE 9			RETURN <<-1055.2484, -3253.2014, 13.2842>>
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				CASE LSIA_HANGAR_A17
					SWITCH iExtraParam
						CASE 0
							SWITCH iProp
								CASE 0			RETURN <<-1285.4220, -3113.2700, 13.2730>>
								CASE 1			RETURN <<-1278.3051, -3117.9709, 13.2810>>
								CASE 2			RETURN <<-1270.6670, -3122.7000, 13.2870>>
								CASE 3			RETURN <<-1279.8760, -3104.0859, 13.3050>>
								CASE 4			RETURN <<-1272.7570, -3108.4460, 13.3060>>
								CASE 5			RETURN <<-1265.1450, -3113.1621, 13.3050>>
								CASE 6			RETURN <<-1274.4720, -3094.7439, 13.2530>>
								CASE 7			RETURN <<-1267.2209, -3099.2771, 13.2690>>
								CASE 8			RETURN <<-1259.5699, -3103.7749, 13.2750>>
								CASE 9			RETURN <<-1263.9630, -3126.6079, 13.2940>>
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH iProp
								CASE 0			RETURN <<-1256.6450, -3130.7500, 13.3130>>
								CASE 1			RETURN <<-1249.4780, -3134.7439, 13.3180>>
								CASE 2			RETURN <<-1258.5930, -3117.2571, 13.3090>>
								CASE 3			RETURN <<-1251.4740, -3121.3650, 13.3140>>
								CASE 4			RETURN <<-1244.4290, -3125.4331, 13.3130>>
								CASE 5			RETURN <<-1253.3060, -3107.6411, 13.2890>>
								CASE 6			RETURN <<-1246.0500, -3111.8279, 13.2980>>
								CASE 7			RETURN <<-1238.9821, -3115.9509, 13.3010>>
								CASE 8			RETURN <<-1241.3800, -3139.5095, 13.3130>>
								CASE 9			RETURN <<-1234.1476, -3143.7485, 13.3121>>
							ENDSWITCH
						BREAK
						CASE 2
							SWITCH iProp
								CASE 0			RETURN <<-1226.5262, -3148.0864, 13.3121>>
								CASE 1			RETURN <<-1236.5515, -3130.7415, 13.3121>>
								CASE 2			RETURN <<-1229.3147, -3134.9431, 13.3121>>
								CASE 3			RETURN <<-1221.5803, -3139.2251, 13.3121>>
								CASE 4			RETURN <<-1232.1230, -3122.5078, 13.3147>>
								CASE 5			RETURN <<-1224.8868, -3126.8003, 13.3121>>
								CASE 6			RETURN <<-1217.2740, -3131.1624, 13.3121>>
								CASE 7			RETURN <<-1218.1603, -3152.8499, 13.3121>>
								CASE 8			RETURN <<-1210.6515, -3157.0261, 13.3121>>
								CASE 9			RETURN <<-1203.7496, -3160.8445, 13.3121>>
							ENDSWITCH
						BREAK
						CASE 3
							SWITCH iProp
								CASE 0			RETURN <<-1213.7836, -3143.8950, 13.3121>>
								CASE 1			RETURN <<-1206.0491, -3148.0518, 13.3121>>
								CASE 2			RETURN <<-1198.5797, -3152.4121, 13.3121>>
								CASE 3			RETURN <<-1209.3182, -3135.3384, 13.3121>>
								CASE 4			RETURN <<-1201.7565, -3139.7361, 13.3121>>
								CASE 5			RETURN <<-1194.2974, -3143.9148, 13.3121>>
								CASE 6			RETURN <<-1223.0336, -3181.5918, 13.2818>>
								CASE 7			RETURN <<-1215.8398, -3185.5737, 13.3121>>
								CASE 8			RETURN <<-1208.5059, -3189.8125, 13.3121>>
								CASE 9			RETURN <<-1218.8635, -3173.7695, 13.3121>>
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				CASE ZANCUDO_HANGAR_A2
					SWITCH iExtraParam
						CASE 0
							SWITCH iProp
								CASE 0			RETURN <<-2154.8989, 3051.9221, 31.9760>>
								CASE 1			RETURN <<-2148.3750, 3047.7891, 31.9580>>
								CASE 2			RETURN <<-2161.9951, 3056.3960, 31.9270>>
								CASE 3			RETURN <<-2153.1201, 3040.0210, 31.8090>>
								CASE 4			RETURN <<-2159.4839, 3044.1079, 31.8910>>
								CASE 5			RETURN <<-2166.2539, 3048.4270, 31.8840>>
								CASE 6			RETURN <<-2157.7610, 3031.7351, 31.8090>>
								CASE 7			RETURN <<-2164.2600, 3036.0149, 31.8390>>
								CASE 8			RETURN <<-2170.8091, 3040.5259, 31.8830>>
								CASE 9			RETURN <<-2116.6489, 3028.8259, 32.0230>>
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH iProp
								CASE 0			RETURN <<-2110.2339, 3024.9031, 32.0360>>
								CASE 1			RETURN <<-2103.9619, 3021.1990, 32.0500>>
								CASE 2			RETURN <<-2122.5859, 3020.6650, 31.8090>>
								CASE 3			RETURN <<-2115.1299, 3016.4971, 32.0340>>
								CASE 4			RETURN <<-2108.6160, 3012.6919, 31.9800>>
								CASE 5			RETURN <<-2127.3669, 3012.6340, 31.8090>>
								CASE 6			RETURN <<-2120.3020, 3008.6731, 31.7590>>
								CASE 7			RETURN <<-2112.6040, 3004.1970, 32.0720>>
								CASE 8			RETURN <<-2221.6448, 3089.8789, 31.8519>>
								CASE 9			RETURN <<-2228.2480, 3093.8513, 31.9257>>
							ENDSWITCH
						BREAK
						CASE 2
							SWITCH iProp
								CASE 0			RETURN <<-2235.4688, 3098.1086, 31.9343>>
								CASE 1			RETURN <<-2242.7517, 3102.5864, 31.9688>>
								CASE 2			RETURN <<-2250.0469, 3106.8218, 31.9354>>
								CASE 3			RETURN <<-2232.9788, 3086.0891, 31.9683>>
								CASE 4			RETURN <<-2239.9749, 3090.0356, 31.9606>>
								CASE 5			RETURN <<-2247.2000, 3094.6167, 32.0167>>
								CASE 6			RETURN <<-2254.6248, 3098.9534, 31.9645>>
								CASE 7			RETURN <<-2236.2373, 3071.4202, 31.8257>>
								CASE 8			RETURN <<-2243.2451, 3076.1340, 31.9834>>
								CASE 9			RETURN <<-2250.2080, 3080.7039, 31.9763>>
							ENDSWITCH
						BREAK
						CASE 3
							SWITCH iProp
								CASE 0			RETURN <<-2257.0698, 3084.8481, 31.9935>>
								CASE 1			RETURN <<-2264.5454, 3088.8733, 31.9009>>
								CASE 2			RETURN <<-2247.8281, 3067.9282, 31.7967>>
								CASE 3			RETURN <<-2254.6873, 3072.1694, 31.9606>>
								CASE 4			RETURN <<-2261.6829, 3076.4067, 31.9602>>
								CASE 5			RETURN <<-2268.9609, 3080.1863, 31.9039>>
								CASE 6			RETURN <<-2188.8557, 3050.5032, 31.9926>>
								CASE 7			RETURN <<-2195.3704, 3054.5676, 31.9649>>
								CASE 8			RETURN <<-2202.0107, 3058.7683, 31.9318>>
								CASE 9			RETURN <<-2208.6858, 3062.9458, 31.8891>>
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				CASE ZANCUDO_HANGAR_3497
					SWITCH iExtraParam
						CASE 0
							SWITCH iProp
								CASE 0			RETURN <<-1949.5100, 2937.6140, 31.8650>>
								CASE 1			RETURN <<-1956.1740, 2941.4270, 32.1160>>
								CASE 2			RETURN <<-1963.0880, 2945.5400, 32.0650>>
								CASE 3			RETURN <<-1953.7920, 2929.1089, 32.0810>>
								CASE 4			RETURN <<-1961.1890, 2932.9241, 32.0390>>
								CASE 5			RETURN <<-1967.9180, 2937.0620, 32.0590>>
								CASE 6			RETURN <<-1959.0500, 2920.1179, 32.0630>>
								CASE 7			RETURN <<-1966.4189, 2923.9871, 32.0460>>
								CASE 8			RETURN <<-1973.1851, 2927.9060, 32.0760>>
								CASE 9			RETURN <<-1978.0389, 2919.2429, 31.8760>>
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH iProp
								CASE 0			RETURN <<-1971.1560, 2915.3269, 32.0850>>
								CASE 1			RETURN <<-1963.9830, 2911.0129, 32.0390>>
								CASE 2			RETURN <<-1982.8340, 2911.1670, 31.8600>>
								CASE 3			RETURN <<-1975.8680, 2907.2361, 31.8130>>
								CASE 4			RETURN <<-1968.4550, 2903.1050, 31.8620>>
								CASE 5			RETURN <<-1987.3320, 2903.4041, 31.8370>>
								CASE 6			RETURN <<-1980.2700, 2899.4961, 32.0150>>
								CASE 7			RETURN <<-1973.0800, 2895.3560, 31.9390>>
								CASE 8			RETURN <<-1989.5701, 2915.4890, 32.0200>>
								CASE 9			RETURN <<-2004.4744, 2925.2383, 31.8138>>
							ENDSWITCH
						BREAK
						CASE 2
							SWITCH iProp
								CASE 0			RETURN <<-1997.1904, 2920.8564, 31.9560>>
								CASE 1			RETURN <<-1994.0802, 2908.2742, 32.0085>>
								CASE 2			RETURN <<-2019.8479, 2913.6714, 31.7925>>
								CASE 3			RETURN <<-2001.3705, 2912.9771, 32.0170>>
								CASE 4			RETURN <<-1997.7922, 2900.4194, 31.8263>>
								CASE 5			RETURN <<-2005.2316, 2905.4116, 31.8512>>
								CASE 6			RETURN <<-2012.5551, 2909.7334, 31.8394>>
								CASE 7			RETURN <<-1977.5050, 2947.8760, 32.0305>>
								CASE 8			RETURN <<-1984.8192, 2951.9087, 32.0583>>
								CASE 9			RETURN <<-1992.6486, 2956.9158, 31.7616>>
							ENDSWITCH
						BREAK
						CASE 3
							SWITCH iProp
								CASE 0			RETURN <<-1985.7407, 2940.8618, 32.0186>>
								CASE 1			RETURN <<-1991.9114, 2945.9260, 31.8125>>
								CASE 2			RETURN <<-1978.7491, 2934.5859, 32.0634>>
								CASE 3			RETURN <<-1990.1840, 2931.7454, 31.8100>>
								CASE 4			RETURN <<-1997.6185, 2936.1716, 31.8145>>
								CASE 5			RETURN <<-1983.4780, 2926.5874, 31.8338>>
								CASE 6			RETURN <<-2051.0579, 2954.5947, 31.8348>>
								CASE 7			RETURN <<-2057.5083, 2958.9663, 31.8655>>
								CASE 8			RETURN <<-2064.5874, 2963.5889, 31.8350>>
								CASE 9			RETURN <<-2072.2222, 2967.3042, 32.1030>>
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				CASE ZANCUDO_HANGAR_3499
					SWITCH iExtraParam
						CASE 0
							SWITCH iProp
								CASE 0			RETURN <<-2533.6140, 3121.4829, 31.8190>>
								CASE 1			RETURN <<-2542.1621, 3126.1819, 31.8180>>
								CASE 2			RETURN <<-2550.2620, 3131.3369, 31.8180>>
								CASE 3			RETURN <<-2554.5120, 3123.9141, 31.9680>>
								CASE 4			RETURN <<-2546.1770, 3119.1350, 31.9470>>
								CASE 5			RETURN <<-2537.7629, 3114.3240, 31.9360>>
								CASE 6			RETURN <<-2558.5959, 3116.8740, 31.8550>>
								CASE 7			RETURN <<-2550.5020, 3112.2229, 31.9130>>
								CASE 8			RETURN <<-2541.9861, 3107.4089, 31.8470>>
								CASE 9			RETURN <<-2517.1160, 3112.0400, 31.8190>>
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH iProp
								CASE 0			RETURN <<-2509.4570, 3107.6919, 31.8190>>
								CASE 1			RETURN <<-2502.2981, 3103.7739, 31.8190>>
								CASE 2			RETURN <<-2521.2520, 3104.7600, 31.9450>>
								CASE 3			RETURN <<-2513.5471, 3100.4700, 31.9040>>
								CASE 4			RETURN <<-2506.1460, 3096.8159, 31.9490>>
								CASE 5			RETURN <<-2524.9180, 3098.1589, 31.8780>>
								CASE 6			RETURN <<-2517.3979, 3094.1370, 31.9380>>
								CASE 7			RETURN <<-2509.9519, 3090.1509, 31.8730>>
								CASE 8			RETURN <<-2462.6763, 3080.7605, 31.8181>>
								CASE 9			RETURN <<-2455.4331, 3076.4512, 31.8180>>
							ENDSWITCH
						BREAK
						CASE 2
							SWITCH iProp
								CASE 0			RETURN <<-2448.2239, 3072.1216, 31.8179>>
								CASE 1			RETURN <<-2466.6621, 3073.6882, 32.0216>>
								CASE 2			RETURN <<-2459.4338, 3069.3401, 32.0043>>
								CASE 3			RETURN <<-2452.1953, 3064.9878, 32.0061>>
								CASE 4			RETURN <<-2470.4270, 3066.9861, 31.8947>>
								CASE 5			RETURN <<-2463.2075, 3062.6448, 31.8885>>
								CASE 6			RETURN <<-2455.9512, 3058.5425, 31.8343>>
								CASE 7			RETURN <<-2478.8889, 3075.3467, 31.9992>>
								CASE 8			RETURN <<-2484.9629, 3078.9451, 31.9284>>
								CASE 9			RETURN <<-2491.3804, 3082.6816, 31.9166>>
							ENDSWITCH
						BREAK
						CASE 3
							SWITCH iProp
								CASE 0			RETURN <<-2497.7378, 3086.6736, 31.9468>>
								CASE 1			RETURN <<-2471.5090, 3079.9111, 31.9225>>
								CASE 2			RETURN <<-2478.4258, 3083.9148, 31.9020>>
								CASE 3			RETURN <<-2485.1187, 3087.6567, 31.9319>>
								CASE 4			RETURN <<-2491.5398, 3091.3813, 31.9612>>
								CASE 5			RETURN <<-2498.0469, 3094.9978, 31.9925>>
								CASE 6			RETURN <<-2481.7041, 3036.2446, 31.8264>>
								CASE 7			RETURN <<-2488.5144, 3040.0603, 31.8257>>
								CASE 8			RETURN <<-2494.9429, 3043.7913, 31.8208>>
								CASE 9			RETURN <<-2502.2329, 3048.0178, 31.8218>>
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_SETUP_RON
			SWITCH eSubvariation
				CASE SMS_SETUP_DELPERROBEACH 
					SWITCH iProp
						CASE 0			RETURN <<-1784.0635, -850.1891, 6.8085>>
						CASE 1			RETURN <<-1789.1035, -853.5162, 6.7837>>
					ENDSWITCH
				BREAK
				CASE SMS_SETUP_LSRIVER
					SWITCH iProp
						CASE 0			RETURN <<612.7700, -538.2505, 14.2585>>
						CASE 1			RETURN <<616.9852, -540.8159, 14.1693>>
					ENDSWITCH
				BREAK
				CASE SMS_SETUP_ELBURRO
					SWITCH iProp
						CASE 0			RETURN <<1743.5668, -1556.5587, 111.6542>>
						CASE 1			RETURN <<1742.1045, -1553.3013, 111.6492>>
					ENDSWITCH
				BREAK
				CASE SMS_SETUP_CHUMASH
					SWITCH iProp
						CASE 0			RETURN <<-3285.7971, 989.0798, 3.0015>>
						CASE 1			RETURN <<-3284.0188, 989.4413, 3.2194>>
					ENDSWITCH
				BREAK
				CASE SMS_SETUP_TATAVIUM
					SWITCH iProp
						CASE 0			RETURN <<2527.5244, 1022.5158, 76.5200>>
						CASE 1			RETURN <<2523.1150, 1023.6847, 76.7563>>
					ENDSWITCH
				BREAK
				CASE SMS_SETUP_RONSWINDFARM
					SWITCH iProp
						CASE 0			RETURN <<2429.7095, 1464.3807, 37.8687>>
						CASE 1			RETURN <<2434.1326, 1467.6989, 37.1543>>
					ENDSWITCH
				BREAK
				CASE SMS_SETUP_GRANDSENORA1
					SWITCH iProp
						CASE 0			RETURN <<1362.0426, 1408.2795, 102.3434>>
						CASE 1			RETURN <<1360.7832, 1412.7390, 102.2822>>
					ENDSWITCH
				BREAK
				CASE SMS_SETUP_GRANDSENORA2
					SWITCH iProp
						CASE 0			RETURN <<312.7105, 2428.3181, 46.6684>>
						CASE 1			RETURN <<315.7248, 2421.8354, 47.6608>>
					ENDSWITCH
				BREAK
				CASE SMS_SETUP_SANDY
					SWITCH iProp
						CASE 0			RETURN <<1195.0182, 3078.1521, 39.9090>>
						CASE 1			RETURN <<1192.8680, 3075.7070, 39.9090>>
					ENDSWITCH
				BREAK
				CASE SMS_SETUP_STAB
					SWITCH iProp
						CASE 0			RETURN <<103.6748, 3658.6477, 38.7560>>
						CASE 1			RETURN <<108.3548, 3659.7373, 38.7445>>
					ENDSWITCH
				BREAK
				CASE SMS_SETUP_CALAFIA
					SWITCH iProp
						CASE 0			RETURN <<1006.9199, 4352.6758, 42.8155>>
						CASE 1			RETURN <<1005.2342, 4357.5771, 42.9967>>
					ENDSWITCH
				BREAK
				CASE SMS_SETUP_MCKENZIE
					SWITCH iProp
						CASE 0			RETURN <<2023.9475, 4788.6230, 40.6078>>
						CASE 1			RETURN <<2026.5940, 4791.5732, 40.6569>>
					ENDSWITCH
				BREAK
				CASE SMS_SETUP_GRAPESEED
					SWITCH iProp
						CASE 0			RETURN <<2652.6838, 4238.9658, 43.3375>>
						CASE 1			RETURN <<2654.4851, 4235.0063, 42.4772>>
					ENDSWITCH
				BREAK
				CASE SMS_SETUP_PORCOPIO
					SWITCH iProp
						CASE 0			RETURN <<764.2339, 6586.0654, 1.3567>>
						CASE 1			RETURN <<763.3056, 6583.9229, 1.3745>>
					ENDSWITCH
				BREAK
				CASE SMS_SETUP_PALETO
					SWITCH iProp
						CASE 0			RETURN <<-334.1346, 6462.8032, 1.0304>>
						CASE 1			RETURN <<-336.6118, 6460.7490, 0.9864>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC FLOAT GET_SMUGGLER_VARIATION_PROP_HEADING(INT iProp, SMUGGLER_VARIATION eVariation, SMUGGLER_SUBVARIATION eSubvariation, INT iExtraParam = -1, HANGAR_ID eHangar = HANGAR_ID_INVALID, INT iRespawnLocation = 0)
	
	SWITCH eVariation
	
		CASE SMV_STEAL_AIRCRAFT
			SWITCH eSubvariation
				CASE SMS_SA_SANDY_SHORES
					SWITCH iProp
						CASE 0			RETURN 134.3950
						CASE 1			RETURN 124.9940
						CASE 2			RETURN 249.3993
					ENDSWITCH
				BREAK
				CASE SMS_SA_MCKENZIE_AIRFIELD
					SWITCH iProp
						CASE 0			RETURN 238.4000
						CASE 1			RETURN 138.9992
						CASE 2			RETURN 205.5990
					ENDSWITCH
				BREAK
				CASE SMS_SA_EPSILON_HQ
					SWITCH iProp
						CASE 0			RETURN 294.7979
						CASE 1			RETURN 24.1977
						CASE 2			RETURN 285.9994
						CASE 3			RETURN 22.1990
						CASE 4			RETURN 83.7972
						CASE 5			RETURN 154.9973
						CASE 6			RETURN 205.2000
						CASE 7			RETURN 205.2000
						CASE 8			RETURN 205.2000
						CASE 9			RETURN 205.2000
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_BLACK_BOX
			SWITCH eSubvariation
				CASE SMS_BLB_MOUNT_GORDO
					SWITCH iProp
						CASE 0			RETURN 337.8000
					ENDSWITCH
				BREAK
				CASE SMS_BLB_PALETO_FOREST
					SWITCH iProp
						CASE 0			RETURN 22.9990
					ENDSWITCH
				BREAK
				CASE SMS_BLB_ISLAND
					SWITCH iProp
						CASE 0			RETURN 352.5977
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_BOMB_BASE
			SWITCH eSubvariation
				CASE SMS_BB_TERMINAL
					SWITCH iProp
						CASE 0			RETURN 299.7990
						CASE 1			RETURN 269.5980
						CASE 2			RETURN 251.3980
						CASE 3			RETURN 131.5978
						CASE 4			RETURN 23.3979
						CASE 5			RETURN 166.1979
						CASE 6			RETURN 169.9980
						CASE 7			RETURN 180.9980
						CASE 8			RETURN 138.9980
						CASE 9			RETURN 180.9980
						CASE 10			RETURN 159.9970
						CASE 11			RETURN 159.9970
					ENDSWITCH
				BREAK
				CASE SMS_BB_GRAPESEED_AUTOYARD
					SWITCH iProp
						CASE 0			RETURN 317.3990
						CASE 1			RETURN 156.3980
						CASE 2			RETURN 61.3970
						CASE 3			RETURN 139.1470
						CASE 4			RETURN 150.9960
						CASE 5			RETURN 138.9960
						CASE 6			RETURN 333.5960
						CASE 7			RETURN 317.9960
						CASE 8			RETURN 224.9950
						CASE 9			RETURN 315.5950
						CASE 10			RETURN 250.3950
						CASE 11			RETURN 295.3940
					ENDSWITCH
				BREAK
				CASE SMS_BB_PLANE_SCRAPYARD
					SWITCH iProp
						CASE 0			RETURN 32.3970
						CASE 1			RETURN 267.5970
						CASE 2			RETURN 276.5970
						CASE 3			RETURN 242.1970
						CASE 4			RETURN 179.7970
						CASE 5			RETURN 85.9960
						CASE 6			RETURN 260.1970
						CASE 7			RETURN 214.7970
						CASE 8			RETURN 109.1970
						CASE 9			RETURN 41.7970
						CASE 10			RETURN 6.9970
						CASE 11			RETURN 279.9970
						CASE 12			RETURN 5.3950
						CASE 13			RETURN 5.3950
						CASE 14			RETURN 5.3950
						CASE 15			RETURN 5.3950
						CASE 16			RETURN 5.3950
						CASE 17			RETURN 5.3950
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_SPLASH_LANDING
			SWITCH eSubvariation
				CASE SMS_SL_LAGO_ZANCUDO
					SWITCH iProp
						CASE 0			RETURN 36.1993
					ENDSWITCH
				BREAK
				CASE SMS_SL_PACIFIC_OCEAN_E
					SWITCH iProp
						CASE 0			RETURN 102.5999
					ENDSWITCH
				BREAK
				CASE SMS_SL_PACIFIC_OCEAN_NE
					SWITCH iProp
						CASE 0			RETURN 188.9994
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_ROOF_ATTACK
			SWITCH eSubvariation
				CASE SMS_RA_CONSTRUCTION_SITE
					SWITCH iProp
						CASE 0			RETURN 75.5967
						CASE 1			RETURN 260.1968
						CASE 2			RETURN 311.7967
						CASE 3			RETURN 190.3967
						CASE 4			RETURN 358.1966
						CASE 5			RETURN 73.7965
						CASE 6			RETURN 161.9960
						CASE 7			RETURN 164.1957
						CASE 8			RETURN 89.1958
						CASE 9			RETURN 157.7953
						CASE 10			RETURN 41.9990
						CASE 11			RETURN 11.3980
						CASE 12			RETURN -0.2010
						CASE 13			RETURN 351.5990
						CASE 14			RETURN 316.7990
						CASE 15			RETURN 69.7990
						CASE 16			RETURN 322.3982
						CASE 17			RETURN 279.1980
					ENDSWITCH
				BREAK
				CASE SMS_RA_ALTA_HOTEL
					SWITCH iProp
						CASE 0			RETURN 160.7999
						CASE 1			RETURN 70.9996
						CASE 2			RETURN 70.3994
						CASE 3			RETURN 119.1993
						CASE 4			RETURN 159.5989
						CASE 5			RETURN 249.7986
						CASE 6			RETURN 70.1983
						CASE 7			RETURN 339.1982
						CASE 8			RETURN 70.1980
						CASE 9			RETURN 250.5978
						CASE 10			RETURN 54.5990
						CASE 11			RETURN 105.1980
						CASE 12			RETURN 193.5970
						CASE 13			RETURN 339.9960
						CASE 14			RETURN 194.7960
						CASE 15			RETURN 119.9970
						CASE 16			RETURN 70.2000
						CASE 17			RETURN 17.7993
						CASE 18			RETURN 329.7979
						CASE 19			RETURN 102.9978
					ENDSWITCH
				BREAK
				CASE SMS_RA_PACIFIC_STANDARD
					SWITCH iProp
						CASE 0			RETURN 340.3930	
						CASE 1			RETURN 70.1930
						CASE 2			RETURN 312.9930
						CASE 3			RETURN 152.3927
						CASE 4			RETURN 339.7920
						CASE 5			RETURN 25.9920
						CASE 6			RETURN 159.9920
						CASE 7			RETURN 339.9920
						CASE 8			RETURN 69.5930
						CASE 9			RETURN 69.9930
						CASE 10			RETURN 185.5990
						CASE 11			RETURN 170.1979
						CASE 12			RETURN 342.5980
						CASE 13			RETURN 58.7980
						CASE 14			RETURN 127.1928
						CASE 15			RETURN 66.7930
						CASE 16			RETURN 160.3970
						CASE 17			RETURN 160.9970
						CASE 18			RETURN 160.3970
						CASE 19			RETURN 160.5970
						CASE 20			RETURN 118.5930
						CASE 21	        RETURN 359.3930
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_INFILTRATION
			SWITCH eSubvariation
				CASE SMS_INF_HUMANE_LABS
					SWITCH iProp
						CASE 0			RETURN 3.7980
						CASE 1			RETURN 11.9980
						CASE 2			RETURN 259.1980
						CASE 3			RETURN 52.9980
						CASE 4			RETURN 161.3970
						CASE 5			RETURN 259.9970
						CASE 6			RETURN 81.9970
						CASE 7			RETURN 82.5970
						CASE 8			RETURN 82.5970
					ENDSWITCH
				BREAK
				CASE SMS_INF_ZANCUDO
					SWITCH iProp
						CASE 0			RETURN 149.7480
						CASE 1			RETURN 169.5989
						CASE 2			RETURN 123.3988
						CASE 3			RETURN 350.1985
						CASE 4			RETURN 299.7994
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_BEACON_GRAB
			SWITCH eSubvariation
				CASE SMS_BG_LOCATION_1
					SWITCH iProp
						CASE 0			RETURN 42.5999
						CASE 1			RETURN 71.3987
						CASE 2			RETURN 45.5977
						CASE 3			RETURN 122.7969
						CASE 4			RETURN 205.3958
						CASE 5			RETURN -0.2547
						CASE 6			RETURN 60.3449
						CASE 7			RETURN 120.1446
						CASE 8			RETURN 279.9436
						CASE 9			RETURN 89.9427
						CASE 10			RETURN 160.1408
						CASE 11			RETURN 289.3397
						CASE 12			RETURN 133.9391
						CASE 13			RETURN 149.7377
						CASE 14			RETURN 159.9366
						CASE 15			RETURN 179.3361
						CASE 16			RETURN 85.3355
						CASE 17			RETURN 174.9350
						CASE 18			RETURN 359.1339
						CASE 19			RETURN 251.1327
					ENDSWITCH
				BREAK
				CASE SMS_BG_LOCATION_2
					SWITCH iProp
						CASE 0			RETURN 127.1998
						CASE 1			RETURN 45.8986
						CASE 2			RETURN 205.3975
						CASE 3			RETURN 164.5966
						CASE 4			RETURN 220.9959
						CASE 5			RETURN 317.7452
						CASE 6			RETURN 40.5447
						CASE 7			RETURN 336.9444
						CASE 8			RETURN 77.9434
						CASE 9			RETURN 1.9425
						CASE 10			RETURN 89.9405
						CASE 11			RETURN 60.3396
						CASE 12			RETURN 100.7389
						CASE 13			RETURN 156.9377
						CASE 14			RETURN 261.1365
						CASE 15			RETURN 179.3361
						CASE 16			RETURN 85.3355
						CASE 17			RETURN 271.1348
						CASE 18			RETURN 306.7338
						CASE 19			RETURN 251.1327
					ENDSWITCH
				BREAK
				CASE SMS_BG_LOCATION_3
					SWITCH iProp
						CASE 0			RETURN 43.9995
						CASE 1			RETURN 148.1985
						CASE 2			RETURN 174.5975
						CASE 3			RETURN 131.9964
						CASE 4			RETURN 113.3958
						CASE 5			RETURN 4.7452
						CASE 6			RETURN 180.1444
						CASE 7			RETURN 258.5443
						CASE 8			RETURN 89.9433
						CASE 9			RETURN 138.1424
						CASE 10			RETURN 95.5402
						CASE 11			RETURN 216.1395
						CASE 12			RETURN 73.5388
						CASE 13			RETURN 69.3373
						CASE 14			RETURN 259.9365
						CASE 15			RETURN 359.1358
						CASE 16			RETURN 265.5351
						CASE 17			RETURN 141.3346
						CASE 18			RETURN 159.5335
						CASE 19			RETURN 269.9325
					ENDSWITCH
				BREAK
				CASE SMS_BG_LOCATION_4
					SWITCH iProp
						CASE 0			RETURN 179.7990
						CASE 1			RETURN 270.3980
						CASE 2			RETURN 113.1972
						CASE 3			RETURN 111.1961
						CASE 4			RETURN 328.9956
						CASE 5			RETURN 4.7452
						CASE 6			RETURN 210.5447
						CASE 7			RETURN 108.9441
						CASE 8			RETURN 270.5430
						CASE 9			RETURN 164.5416
						CASE 10			RETURN 110.5401
						CASE 11			RETURN 269.1396
						CASE 12			RETURN 159.1384
						CASE 13			RETURN 238.7370
						CASE 14			RETURN 265.5365
						CASE 15			RETURN 1.1358
						CASE 16			RETURN 232.7349
						CASE 17			RETURN 236.7343
						CASE 18			RETURN 294.9330
						CASE 19			RETURN 244.9322
					ENDSWITCH
				BREAK
				CASE SMS_BG_LOCATION_5
					SWITCH iProp
						CASE 0			RETURN 72.5987
						CASE 1			RETURN 248.1979
						CASE 2			RETURN 107.5971
						CASE 3			RETURN 90.5959
						CASE 4			RETURN 179.1455
						CASE 5			RETURN 310.5451
						CASE 6			RETURN 214.3447
						CASE 7			RETURN 90.1439
						CASE 8			RETURN 90.5427
						CASE 9			RETURN 47.3411
						CASE 10			RETURN 92.5399
						CASE 11			RETURN 63.9394
						CASE 12			RETURN 58.7380
						CASE 13			RETURN 254.3369
						CASE 14			RETURN 360.5364
						CASE 15			RETURN 88.7356
						CASE 16			RETURN 232.1349
						CASE 17			RETURN 89.1340
						CASE 18			RETURN 252.3327
						CASE 19			RETURN 271.1318
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_BOMB_ROOF
			SWITCH eSubvariation
				CASE SMS_BRO_ROUTE_1
					SWITCH iRespawnLocation
						CASE 0
							SWITCH iProp
								CASE 0			RETURN 287.1850
								CASE 1			RETURN 161.7850
								CASE 2			RETURN 14.3840
								CASE 3			RETURN 236.5940
								CASE 4			RETURN 268.7950
								CASE 5			RETURN 268.7950
								CASE 6			RETURN 268.7950
								CASE 7			RETURN 268.7950
								CASE 8			RETURN 268.7950
								CASE 9			RETURN 268.7950
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH iProp
								CASE 0			RETURN 218.9980
								CASE 1			RETURN 162.7980
								CASE 2			RETURN 97.7980
								CASE 3			RETURN 236.5940
								CASE 4			RETURN 268.7950
								CASE 5			RETURN 268.7950
								CASE 6			RETURN 268.7950
								CASE 7			RETURN 268.7950
								CASE 8			RETURN 268.7950
								CASE 9			RETURN 268.7950
							ENDSWITCH
						BREAK
						CASE 2
							SWITCH iProp
								CASE 0			RETURN 85.1940
								CASE 1			RETURN 141.5930
								CASE 2			RETURN 219.1930
								CASE 3			RETURN 100.1880
								CASE 4			RETURN 226.1880
								CASE 5			RETURN 128.1880
								CASE 6			RETURN 128.1880
								CASE 7			RETURN 41.1880
								CASE 8			RETURN 41.1880
								CASE 9			RETURN 220.5880
							ENDSWITCH
						BREAK
						CASE 3
							SWITCH iProp
								CASE 0			RETURN 111.9870
								CASE 1			RETURN 35.1870
								CASE 2			RETURN 100.1870
								CASE 3			RETURN 354.9850
								CASE 4			RETURN 354.9850
								CASE 5			RETURN 354.9850
								CASE 6			RETURN 354.9850
								CASE 7			RETURN 354.9850
								CASE 8			RETURN 354.9850
								CASE 9			RETURN 0.0000
							ENDSWITCH
						BREAK
						CASE 4
							SWITCH iProp
								CASE 0			RETURN 123.5820
								CASE 1			RETURN 333.3820
								CASE 2			RETURN 113.1820
								CASE 3			RETURN 354.9850
								CASE 4			RETURN 354.9850
								CASE 5			RETURN 354.9850
								CASE 6			RETURN 354.9850
								CASE 7			RETURN 354.9850
								CASE 8			RETURN 354.9850
								CASE 9			RETURN 0.0000
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				CASE SMS_BRO_ROUTE_2
					SWITCH iRespawnLocation
						CASE 0
							SWITCH iProp
								CASE 0			RETURN 70.3990
								CASE 1			RETURN 349.1990
								CASE 2			RETURN 262.7990
								CASE 3			RETURN 59.3970
								CASE 4			RETURN 59.3970
								CASE 5			RETURN 59.3970
								CASE 6			RETURN 59.3970
								CASE 7			RETURN 59.3970
								CASE 8			RETURN 59.3970
								CASE 9			RETURN 59.3970
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH iProp
								CASE 0			RETURN 249.9970
								CASE 1			RETURN 178.5970
								CASE 2			RETURN 278.7970
								CASE 3			RETURN 270.1930
								CASE 4			RETURN 270.1930
								CASE 5			RETURN 270.1930
								CASE 6			RETURN 270.1930
								CASE 7			RETURN 270.1930
								CASE 8			RETURN 270.1930
								CASE 9			RETURN 270.1930
							ENDSWITCH
						BREAK
						CASE 2
							SWITCH iProp
								CASE 0			RETURN 281.7910
								CASE 1			RETURN 274.5910
								CASE 2			RETURN 233.7910
								CASE 3			RETURN 7.7880
								CASE 4			RETURN 7.7880
								CASE 5			RETURN 7.7880
								CASE 6			RETURN 7.7880
								CASE 7			RETURN 7.7880
								CASE 8			RETURN 7.7880
								CASE 9			RETURN 7.7880
							ENDSWITCH
						BREAK
						CASE 3
							SWITCH iProp
								CASE 0			RETURN 189.1980
								CASE 1			RETURN 151.1980
								CASE 2			RETURN 310.5970
								CASE 3			RETURN 7.7880
								CASE 4			RETURN 7.7880
								CASE 5			RETURN 7.7880
								CASE 6			RETURN 7.7880
								CASE 7			RETURN 7.7880
								CASE 8			RETURN 7.7880
								CASE 9			RETURN 7.7880
							ENDSWITCH
						BREAK
						CASE 4
							SWITCH iProp
								CASE 0			RETURN 170.9930
								CASE 1			RETURN 65.3930
								CASE 2			RETURN 305.1930
								CASE 3			RETURN 7.7880
								CASE 4			RETURN 7.7880
								CASE 5			RETURN 7.7880
								CASE 6			RETURN 7.7880
								CASE 7			RETURN 7.7880
								CASE 8			RETURN 7.7880
								CASE 9			RETURN 7.7880
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				CASE SMS_BRO_ROUTE_3
					SWITCH iRespawnLocation
						CASE 0
							SWITCH iProp
								CASE 0			RETURN 177.6000
								CASE 1			RETURN 219.6000
								CASE 2			RETURN 142.8000
								CASE 3			RETURN 161.1980
								CASE 4			RETURN 161.1980
								CASE 5			RETURN 161.1980
								CASE 6			RETURN 161.1980
								CASE 7			RETURN 161.1980
								CASE 8			RETURN 161.1980
								CASE 9			RETURN 161.1980
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH iProp
								CASE 0			RETURN 111.5920
								CASE 1			RETURN 135.5920
								CASE 2			RETURN 362.3920
								CASE 3			RETURN 161.1980
								CASE 4			RETURN 161.1980
								CASE 5			RETURN 161.1980
								CASE 6			RETURN 161.1980
								CASE 7			RETURN 161.1980
								CASE 8			RETURN 161.1980
								CASE 9			RETURN 161.1980
							ENDSWITCH
						BREAK
						CASE 2
							SWITCH iProp
								CASE 0			RETURN 33.9960
								CASE 1			RETURN 71.9960
								CASE 2			RETURN 326.5960
								CASE 3			RETURN 166.3930
								CASE 4			RETURN 166.3930
								CASE 5			RETURN 166.3930
								CASE 6			RETURN 166.3930
								CASE 7			RETURN 166.3930
								CASE 8			RETURN 166.3930
								CASE 9			RETURN 166.3930
							ENDSWITCH
						BREAK
						CASE 3
							SWITCH iProp
								CASE 0			RETURN 80.1910
								CASE 1			RETURN 335.3910
								CASE 2			RETURN 126.5910
								CASE 3			RETURN 166.3930
								CASE 4			RETURN 166.3930
								CASE 5			RETURN 166.3930
								CASE 6			RETURN 166.3930
								CASE 7			RETURN 166.3930
								CASE 8			RETURN 166.3930
								CASE 9			RETURN 166.3930
							ENDSWITCH
						BREAK
						CASE 4
							SWITCH iProp
								CASE 0			RETURN 308.9980
								CASE 1			RETURN 242.9980
								CASE 2			RETURN 178.1980
								CASE 3			RETURN 166.3930
								CASE 4			RETURN 166.3930
								CASE 5			RETURN 166.3930
								CASE 6			RETURN 166.3930
								CASE 7			RETURN 166.3930
								CASE 8			RETURN 166.3930
								CASE 9			RETURN 166.3930
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_SELL_PRECISION_DELIVERY
			UNUSED_PARAMETER(eHangar)
			UNUSED_PARAMETER(iExtraParam)
			SWITCH iProp
				CASE 0			RETURN 330.0000
				CASE 1			RETURN 330.0000
				CASE 2			RETURN 330.0000
				CASE 3			RETURN 330.0000
				CASE 4			RETURN 330.0000
				CASE 5			RETURN 330.0000
				CASE 6			RETURN 330.0000
				CASE 7			RETURN 330.0000
				CASE 8			RETURN 330.0000
				CASE 9			RETURN 330.0000
			ENDSWITCH
		BREAK

		CASE SMV_SETUP_RON
			SWITCH eSubvariation
				CASE SMS_SETUP_DELPERROBEACH 
					SWITCH iProp
						CASE 0			RETURN 188.3997
						CASE 1			RETURN 183.7991
					ENDSWITCH
				BREAK
				CASE SMS_SETUP_LSRIVER
					SWITCH iProp
						CASE 0			RETURN 65.9983
						CASE 1			RETURN 211.5987
					ENDSWITCH
				BREAK
				CASE SMS_SETUP_ELBURRO
					SWITCH iProp
						CASE 0			RETURN 133.3976
						CASE 1			RETURN 78.1974
					ENDSWITCH
				BREAK
				CASE SMS_SETUP_CHUMASH
					SWITCH iProp
						CASE 0			RETURN 291.1960
						CASE 1			RETURN 267.1959
					ENDSWITCH
				BREAK
				CASE SMS_SETUP_TATAVIUM
					SWITCH iProp
						CASE 0			RETURN 82.7961
						CASE 1			RETURN 116.9963
					ENDSWITCH
				BREAK
				CASE SMS_SETUP_RONSWINDFARM
					SWITCH iProp
						CASE 0			RETURN 2.7945
						CASE 1			RETURN 316.5945
					ENDSWITCH
				BREAK
				CASE SMS_SETUP_GRANDSENORA1
					SWITCH iProp
						CASE 0			RETURN 71.7935
						CASE 1			RETURN 250.5931
					ENDSWITCH
				BREAK
				CASE SMS_SETUP_GRANDSENORA2
					SWITCH iProp
						CASE 0			RETURN 111.7932
						CASE 1			RETURN 174.7932
					ENDSWITCH
				BREAK
				CASE SMS_SETUP_SANDY
					SWITCH iProp
						CASE 0			RETURN 32.1979
						CASE 1			RETURN 158.1978
					ENDSWITCH
				BREAK
				CASE SMS_SETUP_STAB
					SWITCH iProp
						CASE 0			RETURN 62.1348
						CASE 1			RETURN 329.1348
					ENDSWITCH
				BREAK
				CASE SMS_SETUP_CALAFIA
					SWITCH iProp
						CASE 0			RETURN 174.9353
						CASE 1			RETURN 34.3350
					ENDSWITCH
				BREAK
				CASE SMS_SETUP_MCKENZIE
					SWITCH iProp
						CASE 0			RETURN 100.1324
						CASE 1			RETURN 352.5320
					ENDSWITCH
				BREAK
				CASE SMS_SETUP_GRAPESEED
					SWITCH iProp
						CASE 0			RETURN 128.9334
						CASE 1			RETURN 188.9330
					ENDSWITCH
				BREAK
				CASE SMS_SETUP_PORCOPIO
					SWITCH iProp
						CASE 0			RETURN 232.5425
						CASE 1			RETURN 168.7425
					ENDSWITCH
				BREAK
				CASE SMS_SETUP_PALETO
					SWITCH iProp
						CASE 0			RETURN 58.9409
						CASE 1			RETURN 337.7408
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC VECTOR GET_SMUGGLER_VARIATION_PROP_ROTATION(INT iProp, SMUGGLER_VARIATION eVariation, SMUGGLER_SUBVARIATION eSubvariation, INT iExtraParam = -1, HANGAR_ID eHangar = HANGAR_ID_INVALID, INT iRespawnLocation = 0)
	
	SWITCH eVariation
		
		CASE SMV_STEAL_AIRCRAFT
			SWITCH eSubvariation
				CASE SMS_SA_SANDY_SHORES
					SWITCH iProp
						CASE 0			RETURN <<0.0000, -0.0000, 134.3950>>
						CASE 1			RETURN <<0.0000, 0.0000, 124.9940>>
						CASE 2			RETURN <<0.0000, -0.0000, -110.6007>>
					ENDSWITCH
				BREAK
				CASE SMS_SA_MCKENZIE_AIRFIELD
					SWITCH iProp
						CASE 0			RETURN <<0.0000, -0.0000, -121.6000>>
						CASE 1			RETURN <<0.0000, -0.0000, 138.9991>>
						CASE 2			RETURN <<0.0000, -0.0000, -154.4010>>
					ENDSWITCH
				BREAK
				CASE SMS_SA_EPSILON_HQ
					SWITCH iProp
						CASE 0			RETURN <<0.0000, 0.0000, -65.2022>>		
						CASE 1			RETURN <<0.0000, 0.0000, 24.1977>>		
						CASE 2			RETURN <<0.0000, 0.0000, -74.0006>>		
						CASE 3			RETURN <<0.0000, 0.0000, 22.1990>>		
						CASE 4			RETURN <<0.0000, 0.0000, 83.7972>>		
						CASE 5			RETURN <<0.0000, -0.0000, 154.9973>>		
						CASE 6			RETURN <<0.0000, 0.0000, -154.8000>>
						CASE 7			RETURN <<0.0000, 0.0000, -154.8000>>
						CASE 8			RETURN <<0.0000, 0.0000, -154.8000>>
						CASE 9			RETURN <<0.0000, 0.0000, -154.8000>>				
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_BLACK_BOX
			SWITCH eSubvariation
				CASE SMS_BLB_MOUNT_GORDO
					SWITCH iProp
						CASE 0			RETURN <<17.8560, -15.3130, -19.7810>>
					ENDSWITCH
				BREAK
				CASE SMS_BLB_PALETO_FOREST
					SWITCH iProp
						CASE 0			RETURN <<-3.0000, 3.0000, 22.9990>>
					ENDSWITCH
				BREAK
				CASE SMS_BLB_ISLAND
					SWITCH iProp
						CASE 0			RETURN <<-31.9008, -1.2001, -7.4023>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_BOMB_BASE
			SWITCH eSubvariation
				CASE SMS_BB_TERMINAL
					SWITCH iProp
						CASE 0			RETURN <<0.0000, 0.0000, -60.2010>>
						CASE 1			RETURN <<0.0000, 0.0000, -90.4020>>
						CASE 2			RETURN <<0.0000, 0.0000, -108.6020>>
						CASE 3			RETURN <<0.0000, -0.0000, 131.5977>>
						CASE 4			RETURN <<0.0000, 0.0000, 23.3979>>
						CASE 5			RETURN <<0.0000, -0.0000, 166.1979>>
						CASE 6			RETURN <<0.0000, -0.0000, 169.9980>>
						CASE 7			RETURN <<0.0000, -0.0000, -179.0020>>
						CASE 8			RETURN <<0.0000, -0.0000, 138.9980>>
						CASE 9			RETURN <<0.0000, 0.0000, -179.0020>>
						CASE 10			RETURN <<0.0000, 0.0000, 159.9970>>
						CASE 11			RETURN <<0.0000, 0.0000, 159.9970>>
					ENDSWITCH
				BREAK
				CASE SMS_BB_GRAPESEED_AUTOYARD
					SWITCH iProp
						CASE 0			RETURN <<0.0000, 0.0000, -42.6010>>
						CASE 1			RETURN <<0.0000, 0.0000, 156.3980>>
						CASE 2			RETURN <<0.0000, 0.0000, 61.3970>>
						CASE 3			RETURN <<0.0000, 0.0000, 139.1470>>
						CASE 4			RETURN <<0.0000, 0.0000, 150.9960>>
						CASE 5			RETURN <<0.0000, 0.0000, 138.9960>>
						CASE 6			RETURN <<0.0000, 0.0000, -26.4040>>
						CASE 7			RETURN <<0.0000, 0.0000, -42.0040>>
						CASE 8			RETURN <<0.0000, 0.0000, -135.0050>>
						CASE 9			RETURN <<0.0000, 0.0000, -44.4050>>
						CASE 10			RETURN <<0.0000, 0.0000, -109.6050>>
						CASE 11			RETURN <<0.0000, 0.0000, -64.6060>>
					ENDSWITCH
				BREAK
				CASE SMS_BB_PLANE_SCRAPYARD
					SWITCH iProp
						CASE 0			RETURN <<0.0000, 0.0000, 32.3970>>
						CASE 1			RETURN <<0.0000, 0.0000, -92.4030>>
						CASE 2			RETURN <<0.0000, 0.0000, -83.4030>>
						CASE 3			RETURN <<0.0000, 0.0000, -117.8030>>
						CASE 4			RETURN <<0.0000, 0.0000, 179.7970>>
						CASE 5			RETURN <<0.0000, 0.0000, 85.9960>>
						CASE 6			RETURN <<0.0000, 0.0000, -99.8030>>
						CASE 7			RETURN <<0.0000, 0.0000, -145.2030>>
						CASE 8			RETURN <<0.0000, 0.0000, 109.1970>>
						CASE 9			RETURN <<0.0000, 0.0000, 41.7970>>
						CASE 10			RETURN <<0.0000, 0.0000, 6.9970>>
						CASE 11			RETURN <<-7.1920, 4.4140, -79.7260>>
						CASE 12			RETURN <<-2.3120, 5.9690, 5.5160>>
						CASE 13			RETURN <<1.6680, -7.0050, 5.4970>>
						CASE 14			RETURN <<0.0000, 0.0000, 5.3950>>
						CASE 15			RETURN <<0.0000, 0.0000, 5.3950>>
						CASE 16			RETURN <<5.0580, 1.9260, 5.3100>>
						CASE 17			RETURN <<0.0000, 0.0000, 5.3950>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_SPLASH_LANDING
			SWITCH eSubvariation
				CASE SMS_SL_LAGO_ZANCUDO
					SWITCH iProp
						CASE 0			RETURN <<0.0000, 0.0000, 36.1993>>

					ENDSWITCH
				BREAK
				CASE SMS_SL_PACIFIC_OCEAN_E
					SWITCH iProp
						CASE 0			RETURN <<0.0000, -0.0000, 102.5999>>

					ENDSWITCH
				BREAK
				CASE SMS_SL_PACIFIC_OCEAN_NE
					SWITCH iProp
						CASE 0			RETURN <<0.0000, -0.0000, -171.0006>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_ROOF_ATTACK
			SWITCH eSubvariation
				CASE SMS_RA_CONSTRUCTION_SITE
					SWITCH iProp
						CASE 0			RETURN <<0.0000, 0.0000, 75.5967>>		
						CASE 1			RETURN <<0.0000, -0.0000, -99.8032>>
						CASE 2			RETURN <<-0.0000, -0.0000, -48.2033>>
						CASE 3			RETURN <<0.0000, -0.0000, -169.6033>>
						CASE 4			RETURN <<-0.0000, -0.0000, -1.8034>>
						CASE 5			RETURN <<0.0000, 0.0000, 73.7965>>
						CASE 6			RETURN <<0.0000, -0.0000, 161.9960>>
						CASE 7			RETURN <<0.0000, -0.0000, 164.1957>>
						CASE 8			RETURN <<0.0000, 0.0000, 89.1958>>
						CASE 9			RETURN <<0.0000, -0.0000, 157.7953>>
						CASE 10			RETURN <<0.0000, 0.0000, 41.9990>>
						CASE 11			RETURN <<0.0000, 0.0000, 11.3980>>
						CASE 12			RETURN <<0.0000, 0.0000, -0.2010>>
						CASE 13			RETURN <<0.0000, 0.0000, -8.4010>>
						CASE 14			RETURN <<0.0000, 0.0000, -43.2010>>
						CASE 15			RETURN <<0.0000, 0.0000, 69.7990>>
						CASE 16			RETURN <<0.0000, 0.0000, -37.6018>>
						CASE 17         RETURN <<0.0000, 0.0000, -80.8020>>
					ENDSWITCH
				BREAK
				CASE SMS_RA_ALTA_HOTEL
					SWITCH iProp
						CASE 0			RETURN <<0.0000, -0.0000, 160.7998>>	
						CASE 1			RETURN <<0.0000, 0.0000, 70.9996>>		
						CASE 2			RETURN <<0.0000, 0.0000, 70.3994>>
						CASE 3			RETURN <<0.0000, -0.0000, 119.1993>>
						CASE 4			RETURN <<0.0000, -0.0000, 159.5989>>
						CASE 5			RETURN <<0.0000, -0.0000, -110.2014>>
						CASE 6			RETURN <<0.0000, 0.0000, 70.1983>>
						CASE 7			RETURN <<-0.0000, -0.0000, -20.8018>>
						CASE 8			RETURN <<0.0000, 0.0000, 70.1980>>
						CASE 9			RETURN <<0.0000, -0.0000, -109.4022>>
						CASE 10			RETURN <<0.0000, 0.0000, 54.5990>>
						CASE 11			RETURN <<0.0000, 0.0000, 105.1980>>
						CASE 12			RETURN <<0.0000, 0.0000, -166.4030>>
						CASE 13			RETURN <<0.0000, 0.0000, -20.0040>>
						CASE 14			RETURN <<0.0000, 0.0000, -165.2040>>
						CASE 15			RETURN <<0.0000, 0.0000, 119.9970>>
						CASE 16			RETURN <<0.0000, 0.0000, 70.2000>>
						CASE 17			RETURN <<0.0000, 0.0000, 17.7993>>
						CASE 18			RETURN <<0.0000, 0.0000, -30.2022>>
						CASE 19		    RETURN <<0.0000, -0.0000, 102.9978>>
					ENDSWITCH
				BREAK
				CASE SMS_RA_PACIFIC_STANDARD
					SWITCH iProp
						CASE 0			RETURN <<0.0000, 0.0000, -19.6070>>			
						CASE 1			RETURN <<0.0000, 0.0000, 70.1930>>
						CASE 2			RETURN <<-0.0000, -0.0000, -47.0070>>
						CASE 3			RETURN <<0.0000, -0.0000, 152.3927>>
						CASE 4			RETURN <<0.0000, 0.0000, -20.2080>>
						CASE 5			RETURN <<0.0000, 0.0000, 25.9920>>
						CASE 6			RETURN <<0.0000, 0.0000, 159.9920>>
						CASE 7			RETURN <<0.0000, 0.0000, -20.0080>>
						CASE 8			RETURN <<0.0000, 0.0000, 69.5930>>
						CASE 9			RETURN <<0.0000, 0.0000, 69.9930>>
						CASE 10			RETURN <<0.0000, -0.0000, -174.4010>>
						CASE 11			RETURN <<0.0000, -0.0000, 170.1979>>
						CASE 12			RETURN <<0.0000, 0.0000, -17.4020>>
						CASE 13			RETURN <<0.0000, 0.0000, 58.7979>>
						CASE 14			RETURN <<0.0000, -0.0000, 127.1928>>
						CASE 15			RETURN <<0.0000, 0.0000, 66.7930>>
						CASE 16			RETURN <<30.8000, 89.2000, 160.3970>>
						CASE 17			RETURN <<30.9000, 88.8000, 160.9970>>
						CASE 18			RETURN <<30.6000, 90.1000, 160.3970>>
						CASE 19			RETURN <<31.0000, 90.0000, 160.5970>>
						CASE 20			RETURN <<0.0000, 0.0000, 118.5930>>
						CASE 21	        RETURN <<0.0000, 0.0000, -0.6070>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_INFILTRATION
			SWITCH eSubvariation
				CASE SMS_INF_HUMANE_LABS
					SWITCH iProp
						CASE 0			RETURN <<0.0000, 0.0000, 3.7980>>
						CASE 1			RETURN <<0.0000, 0.0000, 11.9980>>
						CASE 2			RETURN <<0.0000, 0.0000, -100.8020>>
						CASE 3			RETURN <<0.0000, 0.0000, 52.9980>>
						CASE 4			RETURN <<0.0000, 0.0000, 161.3970>>
						CASE 5			RETURN <<0.0000, 0.0000, -100.0030>>
						CASE 6			RETURN <<0.0000, 0.0000, 81.9970>>
						CASE 7			RETURN <<0.0000, 0.0000, 82.5970>>
						CASE 8			RETURN <<0.0000, 0.0000, 82.5970>>
					ENDSWITCH
				BREAK
				CASE SMS_INF_ZANCUDO
					SWITCH iProp
						CASE 0			RETURN <<0.0000, 0.0000, 149.7480>>
						CASE 1			RETURN <<0.0000, -0.0000, 169.5989>>
						CASE 2			RETURN <<0.0000, -0.0000, 123.3987>>
						CASE 3			RETURN <<0.0000, 0.0000, -9.8016>>
						CASE 4			RETURN <<0.0000, 0.0000, -60.2006>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_BEACON_GRAB
			SWITCH eSubvariation
				CASE SMS_BG_LOCATION_1
					SWITCH iProp
						CASE 0			RETURN <<-0.3173, 8.9685, 42.6248>>
						CASE 1			RETURN <<0.0000, 0.0000, 71.3987>>
						CASE 2			RETURN <<0.0000, 0.0000, 45.5977>>
						CASE 3			RETURN <<0.0000, -0.0000, 122.7969>>
						CASE 4			RETURN <<0.0000, -0.0000, -154.6042>>
						CASE 5			RETURN <<4.9295, -0.4083, -0.2372>>
						CASE 6			RETURN <<0.0000, 0.0000, 60.3449>>
						CASE 7			RETURN <<0.0000, -0.0000, 120.1446>>
						CASE 8			RETURN <<4.8708, 0.8467, -80.0924>>
						CASE 9			RETURN <<6.9782, 0.0070, 89.9422>>
						CASE 10			RETURN <<0.0000, -0.0000, 160.1408>>
						CASE 11			RETURN <<0.0000, 0.0000, -70.6603>>
						CASE 12			RETURN <<0.0000, -0.0000, 133.9391>>
						CASE 13			RETURN <<0.0000, -0.0000, 149.7377>>
						CASE 14			RETURN <<0.0000, -0.0000, 159.9366>>
						CASE 15			RETURN <<0.0000, -0.0000, 179.3361>>
						CASE 16			RETURN <<0.0000, 0.0000, 85.3355>>
						CASE 17			RETURN <<0.0000, -0.0000, 174.9350>>
						CASE 18			RETURN <<0.0000, 0.0000, -0.8661>>
						CASE 19			RETURN <<0.0000, -0.0000, -108.8673>>
					ENDSWITCH
				BREAK
				CASE SMS_BG_LOCATION_2
					SWITCH iProp
						CASE 0			RETURN <<0.0000, -0.0000, 127.1997>>
						CASE 1			RETURN <<0.0000, 179.9998, 45.8986>>
						CASE 2			RETURN <<0.0000, -0.0000, -154.6025>>
						CASE 3			RETURN <<0.0000, -0.0000, 164.5966>>
						CASE 4			RETURN <<0.0000, -0.0000, -139.0041>>
						CASE 5			RETURN <<0.0000, 0.0000, -42.2548>>
						CASE 6			RETURN <<20.3980, -0.2014, 40.5809>>
						CASE 7			RETURN <<0.0000, 0.0000, -23.0556>>
						CASE 8			RETURN <<0.0000, 0.0000, 77.9434>>
						CASE 9			RETURN <<0.0000, 0.0000, 1.9425>>
						CASE 10			RETURN <<0.0000, 0.0000, 89.9405>>
						CASE 11			RETURN <<0.0000, 0.0000, 60.3396>>
						CASE 12			RETURN <<0.0000, -0.0000, 100.7389>>
						CASE 13			RETURN <<0.0000, -0.0000, 156.9376>>
						CASE 14			RETURN <<0.0000, -0.0000, -98.8635>>
						CASE 15			RETURN <<0.0000, -0.0000, 179.3361>>
						CASE 16			RETURN <<0.0000, 0.0000, 85.3355>>
						CASE 17			RETURN <<0.0000, 0.0000, -88.8652>>
						CASE 18			RETURN <<0.0000, 0.0000, -53.2662>>
						CASE 19			RETURN <<0.0000, -0.0000, -108.8673>>
					ENDSWITCH
				BREAK
				CASE SMS_BG_LOCATION_3
					SWITCH iProp
						CASE 0			RETURN <<0.0000, 0.0000, 43.9995>>
						CASE 1			RETURN <<0.0000, -0.0000, 148.1984>>
						CASE 2			RETURN <<0.0000, -0.0000, 174.5975>>
						CASE 3			RETURN <<0.0000, -0.0000, 131.9964>>
						CASE 4			RETURN <<0.0000, -0.0000, 113.3958>>
						CASE 5			RETURN <<0.0000, 0.0000, 4.7452>>
						CASE 6			RETURN <<0.0000, -0.0000, -179.8556>>
						CASE 7			RETURN <<0.0000, -0.0000, -101.4557>>
						CASE 8			RETURN <<0.0000, 0.0000, 89.9433>>
						CASE 9			RETURN <<0.0000, -0.0000, 138.1424>>
						CASE 10			RETURN <<0.0000, -0.0000, 95.5402>>
						CASE 11			RETURN <<0.0000, -0.0000, -143.8605>>
						CASE 12			RETURN <<0.0000, 0.0000, 73.5388>>
						CASE 13			RETURN <<0.0000, 0.0000, 69.3373>>
						CASE 14			RETURN <<0.0000, -0.0000, -100.0636>>
						CASE 15			RETURN <<-0.1016, 8.3779, -0.8568>>
						CASE 16			RETURN <<0.0000, -0.0000, -94.4649>>
						CASE 17			RETURN <<0.0000, -0.0000, 141.3346>>
						CASE 18			RETURN <<0.0000, -0.0000, 159.5335>>
						CASE 19			RETURN <<0.0000, -0.0000, -90.0675>>
					ENDSWITCH
				BREAK
				CASE SMS_BG_LOCATION_4
					SWITCH iProp
						CASE 0			RETURN <<-0.0419, 11.9223, 179.8034>>
						CASE 1			RETURN <<18.1904, -0.1271, -89.5817>>
						CASE 2			RETURN <<0.0000, -0.0000, 113.1972>>
						CASE 3			RETURN <<0.0000, -0.0000, 111.1961>>
						CASE 4			RETURN <<0.0000, 0.0000, -31.0044>>
						CASE 5			RETURN <<0.0000, 179.9998, 4.7452>>
						CASE 6			RETURN <<0.0000, -0.0000, -149.4553>>
						CASE 7			RETURN <<0.0000, -0.0000, 108.9441>>
						CASE 8			RETURN <<0.0000, 0.0000, -89.4570>>
						CASE 9			RETURN <<0.0000, -0.0000, 164.5416>>
						CASE 10			RETURN <<0.0000, -0.0000, 110.5401>>
						CASE 11			RETURN <<0.0000, -0.0000, -90.8604>>
						CASE 12			RETURN <<0.0000, -0.0000, 159.1384>>
						CASE 13			RETURN <<0.0000, -0.0000, -121.2630>>
						CASE 14			RETURN <<0.0000, -0.0000, -94.4635>>
						CASE 15			RETURN <<0.0000, 0.0000, 1.1358>>
						CASE 16			RETURN <<0.0000, -0.0000, -127.2651>>
						CASE 17			RETURN <<0.0000, -0.0000, -123.2657>>
						CASE 18			RETURN <<0.0000, 0.0000, -65.0670>>
						CASE 19			RETURN <<0.0000, -0.0000, -115.0678>>
					ENDSWITCH
				BREAK
				CASE SMS_BG_LOCATION_5
					SWITCH iProp
						CASE 0			RETURN <<-28.1625, 3.8146, 73.5558>>
						CASE 1			RETURN <<0.0000, -0.0000, -111.8021>>
						CASE 2			RETURN <<5.7493, -0.0040, 107.5973>>
						CASE 3			RETURN <<0.0000, -0.0000, 90.5959>>
						CASE 4			RETURN <<0.0000, -0.0000, 179.1455>>
						CASE 5			RETURN <<0.0000, 0.0000, -49.4549>>
						CASE 6			RETURN <<0.0000, -0.0000, -145.6553>>
						CASE 7			RETURN <<0.0000, -0.0000, 90.1439>>
						CASE 8			RETURN <<0.0000, -0.0000, 90.5427>>
						CASE 9			RETURN <<0.0000, 0.0000, 47.3411>>
						CASE 10			RETURN <<0.0000, -0.0000, 92.5399>>
						CASE 11			RETURN <<0.0000, 0.0000, 63.9394>>
						CASE 12			RETURN <<0.0000, 0.0000, 58.7380>>
						CASE 13			RETURN <<0.0000, -0.0000, -105.6631>>
						CASE 14			RETURN <<0.0000, 0.0000, 0.5364>>
						CASE 15			RETURN <<0.0000, 0.0000, 88.7356>>
						CASE 16			RETURN <<0.0000, -0.0000, -127.8651>>
						CASE 17			RETURN <<0.0000, 0.0000, 89.1340>>
						CASE 18			RETURN <<0.0000, -0.0000, -107.6673>>
						CASE 19			RETURN <<0.0000, 0.0000, -88.8682>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_BOMB_ROOF
			SWITCH eSubvariation
				CASE SMS_BRO_ROUTE_1
					SWITCH iRespawnLocation
						CASE 0
							SWITCH iProp
								CASE 0			RETURN <<0.0000, 0.0000, -72.8150>>
								CASE 1			RETURN <<0.0000, 0.0000, 161.7850>>
								CASE 2			RETURN <<0.0000, 0.0000, 14.3840>>
								CASE 3			RETURN <<0.0000, 0.0000, -123.4060>>
								CASE 4			RETURN <<0.0000, 0.0000, -91.2050>>
								CASE 5			RETURN <<0.0000, 0.0000, -91.2050>>
								CASE 6			RETURN <<0.0000, 0.0000, -91.2050>>
								CASE 7			RETURN <<0.0000, 0.0000, -91.2050>>
								CASE 8			RETURN <<0.0000, 0.0000, -91.2050>>
								CASE 9			RETURN <<0.0000, 0.0000, -91.2050>>
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH iProp
								CASE 0			RETURN <<0.0000, 0.0000, -141.0020>>
								CASE 1			RETURN <<0.0000, 0.0000, 162.7980>>
								CASE 2			RETURN <<0.0000, 0.0000, 97.7980>>
								CASE 3			RETURN <<0.0000, 0.0000, -123.4060>>
								CASE 4			RETURN <<0.0000, 0.0000, -91.2050>>
								CASE 5			RETURN <<0.0000, 0.0000, -91.2050>>
								CASE 6			RETURN <<0.0000, 0.0000, -91.2050>>
								CASE 7			RETURN <<0.0000, 0.0000, -91.2050>>
								CASE 8			RETURN <<0.0000, 0.0000, -91.2050>>
								CASE 9			RETURN <<0.0000, 0.0000, -91.2050>>
							ENDSWITCH
						BREAK
						CASE 2
							SWITCH iProp
								CASE 0			RETURN <<0.0000, 0.0000, 85.1940>>
								CASE 1			RETURN <<0.0000, 0.0000, 141.5930>>
								CASE 2			RETURN <<0.0000, 0.0000, -140.8070>>
								CASE 3			RETURN <<0.0000, 0.0000, 100.1880>>
								CASE 4			RETURN <<0.0000, 0.0000, -133.8120>>
								CASE 5			RETURN <<0.0000, 0.0000, 128.1880>>
								CASE 6			RETURN <<0.0000, 0.0000, 128.1880>>
								CASE 7			RETURN <<0.0000, 0.0000, 41.1880>>
								CASE 8			RETURN <<0.0000, 0.0000, 41.1880>>
								CASE 9			RETURN <<0.0000, 0.0000, -139.4120>>
							ENDSWITCH
						BREAK
						CASE 3
							SWITCH iProp
								CASE 0			RETURN <<0.0000, 0.0000, 111.9870>>
								CASE 1			RETURN <<0.0000, 0.0000, 35.1870>>
								CASE 2			RETURN <<0.0000, 0.0000, 100.1870>>
								CASE 3			RETURN <<0.0000, 0.0000, -5.0150>>
								CASE 4			RETURN <<0.0000, 0.0000, -5.0150>>
								CASE 5			RETURN <<0.0000, 0.0000, -5.0150>>
								CASE 6			RETURN <<0.0000, 0.0000, -5.0150>>
								CASE 7			RETURN <<0.0000, 0.0000, -5.0150>>
								CASE 8			RETURN <<0.0000, 0.0000, -5.0150>>
								CASE 9			RETURN <<0.0000, 0.0000, -5.0150>>
							ENDSWITCH
						BREAK
						CASE 4
							SWITCH iProp
								CASE 0			RETURN <<0.0000, 0.0000, 123.5820>>
								CASE 1			RETURN <<0.0000, 0.0000, -26.6180>>
								CASE 2			RETURN <<0.0000, 0.0000, 113.1820>>
								CASE 3			RETURN <<0.0000, 0.0000, -5.0150>>
								CASE 4			RETURN <<0.0000, 0.0000, -5.0150>>
								CASE 5			RETURN <<0.0000, 0.0000, -5.0150>>
								CASE 6			RETURN <<0.0000, 0.0000, -5.0150>>
								CASE 7			RETURN <<0.0000, 0.0000, -5.0150>>
								CASE 8			RETURN <<0.0000, 0.0000, -5.0150>>
								CASE 9			RETURN <<0.0000, 0.0000, -5.0150>>
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				CASE SMS_BRO_ROUTE_2
					SWITCH iRespawnLocation
						CASE 0
							SWITCH iProp
								CASE 0			RETURN <<0.0000, 0.0000, 70.3990>>
								CASE 1			RETURN <<0.0000, 0.0000, -10.8010>>
								CASE 2			RETURN <<0.0000, 0.0000, -97.2010>>
								CASE 3			RETURN <<0.0000, 0.0000, 59.3970>>
								CASE 4			RETURN <<0.0000, 0.0000, 59.3970>>
								CASE 5			RETURN <<0.0000, 0.0000, 59.3970>>
								CASE 6			RETURN <<0.0000, 0.0000, 59.3970>>
								CASE 7			RETURN <<0.0000, 0.0000, 59.3970>>
								CASE 8			RETURN <<0.0000, 0.0000, 59.3970>>
								CASE 9			RETURN <<0.0000, 0.0000, 59.3970>>
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH iProp
								CASE 0			RETURN <<0.0000, 0.0000, -110.0030>>
								CASE 1			RETURN <<0.0000, 0.0000, 178.5970>>
								CASE 2			RETURN <<0.0000, 0.0000, -81.2030>>
								CASE 3			RETURN <<0.0000, 0.0000, -89.8070>>
								CASE 4			RETURN <<0.0000, 0.0000, -89.8070>>
								CASE 5			RETURN <<0.0000, 0.0000, -89.8070>>
								CASE 6			RETURN <<0.0000, 0.0000, -89.8070>>
								CASE 7			RETURN <<0.0000, 0.0000, -89.8070>>
								CASE 8			RETURN <<0.0000, 0.0000, -89.8070>>
								CASE 9			RETURN <<0.0000, 0.0000, -89.8070>>
							ENDSWITCH
						BREAK
						CASE 2
							SWITCH iProp
								CASE 0			RETURN <<0.0000, 0.0000, -78.2090>>
								CASE 1			RETURN <<0.0000, 0.0000, -85.4090>>
								CASE 2			RETURN <<0.0000, 0.0000, -126.2090>>
								CASE 3			RETURN <<0.0000, 0.0000, 7.7880>>
								CASE 4			RETURN <<0.0000, 0.0000, 7.7880>>
								CASE 5			RETURN <<0.0000, 0.0000, 7.7880>>
								CASE 6			RETURN <<0.0000, 0.0000, 7.7880>>
								CASE 7			RETURN <<0.0000, 0.0000, 7.7880>>
								CASE 8			RETURN <<0.0000, 0.0000, 7.7880>>
								CASE 9			RETURN <<0.0000, 0.0000, 7.7880>>
							ENDSWITCH
						BREAK
						CASE 3
							SWITCH iProp
								CASE 0			RETURN <<0.0000, 0.0000, -170.8020>>
								CASE 1			RETURN <<0.0000, 0.0000, 151.1980>>
								CASE 2			RETURN <<0.0000, 0.0000, -49.4030>>
								CASE 3			RETURN <<0.0000, 0.0000, 7.7880>>
								CASE 4			RETURN <<0.0000, 0.0000, 7.7880>>
								CASE 5			RETURN <<0.0000, 0.0000, 7.7880>>
								CASE 6			RETURN <<0.0000, 0.0000, 7.7880>>
								CASE 7			RETURN <<0.0000, 0.0000, 7.7880>>
								CASE 8			RETURN <<0.0000, 0.0000, 7.7880>>
								CASE 9			RETURN <<0.0000, 0.0000, 7.7880>>
							ENDSWITCH
						BREAK
						CASE 4
							SWITCH iProp
								CASE 0			RETURN <<0.0000, 0.0000, 170.9930>>
								CASE 1			RETURN <<0.0000, 0.0000, 65.3930>>
								CASE 2			RETURN <<0.0000, 0.0000, -54.8070>>
								CASE 3			RETURN <<0.0000, 0.0000, 7.7880>>
								CASE 4			RETURN <<0.0000, 0.0000, 7.7880>>
								CASE 5			RETURN <<0.0000, 0.0000, 7.7880>>
								CASE 6			RETURN <<0.0000, 0.0000, 7.7880>>
								CASE 7			RETURN <<0.0000, 0.0000, 7.7880>>
								CASE 8			RETURN <<0.0000, 0.0000, 7.7880>>
								CASE 9			RETURN <<0.0000, 0.0000, 7.7880>>
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				CASE SMS_BRO_ROUTE_3
					SWITCH iRespawnLocation
						CASE 0
							SWITCH iProp
								CASE 0			RETURN <<0.0000, 0.0000, 177.6000>>
								CASE 1			RETURN <<0.0000, 0.0000, -140.4000>>
								CASE 2			RETURN <<0.0000, 0.0000, 142.8000>>
								CASE 3			RETURN <<0.0000, 0.0000, 161.1980>>
								CASE 4			RETURN <<0.0000, 0.0000, 161.1980>>
								CASE 5			RETURN <<0.0000, 0.0000, 161.1980>>
								CASE 6			RETURN <<0.0000, 0.0000, 161.1980>>
								CASE 7			RETURN <<0.0000, 0.0000, 161.1980>>
								CASE 8			RETURN <<0.0000, 0.0000, 161.1980>>
								CASE 9			RETURN <<0.0000, 0.0000, 161.1980>>
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH iProp
								CASE 0			RETURN <<0.0000, 0.0000, 111.5920>>
								CASE 1			RETURN <<0.0000, 0.0000, 135.5920>>
								CASE 2			RETURN <<0.0000, 0.0000, 2.3920>>
								CASE 3			RETURN <<0.0000, 0.0000, 161.1980>>
								CASE 4			RETURN <<0.0000, 0.0000, 161.1980>>
								CASE 5			RETURN <<0.0000, 0.0000, 161.1980>>
								CASE 6			RETURN <<0.0000, 0.0000, 161.1980>>
								CASE 7			RETURN <<0.0000, 0.0000, 161.1980>>
								CASE 8			RETURN <<0.0000, 0.0000, 161.1980>>
								CASE 9			RETURN <<0.0000, 0.0000, 161.1980>>
							ENDSWITCH
						BREAK
						CASE 2
							SWITCH iProp
								CASE 0			RETURN <<0.0000, 0.0000, 33.9960>>
								CASE 1			RETURN <<0.0000, 0.0000, 71.9960>>
								CASE 2			RETURN <<0.0000, 0.0000, -33.4040>>
								CASE 3			RETURN <<0.0000, 0.0000, 166.3930>>
								CASE 4			RETURN <<0.0000, 0.0000, 166.3930>>
								CASE 5			RETURN <<0.0000, 0.0000, 166.3930>>
								CASE 6			RETURN <<0.0000, 0.0000, 166.3930>>
								CASE 7			RETURN <<0.0000, 0.0000, 166.3930>>
								CASE 8			RETURN <<0.0000, 0.0000, 166.3930>>
								CASE 9			RETURN <<0.0000, 0.0000, 166.3930>>
							ENDSWITCH
						BREAK
						CASE 3
							SWITCH iProp
								CASE 0			RETURN <<0.0000, 0.0000, 80.1910>>
								CASE 1			RETURN <<0.0000, 0.0000, -24.6090>>
								CASE 2			RETURN <<0.0000, 0.0000, 126.5910>>
								CASE 3			RETURN <<0.0000, 0.0000, 166.3930>>
								CASE 4			RETURN <<0.0000, 0.0000, 166.3930>>
								CASE 5			RETURN <<0.0000, 0.0000, 166.3930>>
								CASE 6			RETURN <<0.0000, 0.0000, 166.3930>>
								CASE 7			RETURN <<0.0000, 0.0000, 166.3930>>
								CASE 8			RETURN <<0.0000, 0.0000, 166.3930>>
								CASE 9			RETURN <<0.0000, 0.0000, 166.3930>>
							ENDSWITCH
						BREAK
						CASE 4
							SWITCH iProp
								CASE 0			RETURN <<0.0000, 0.0000, -51.0020>>
								CASE 1			RETURN <<0.0000, 0.0000, -117.0020>>
								CASE 2			RETURN <<0.0000, 0.0000, 178.1970>>
								CASE 3			RETURN <<0.0000, 0.0000, 166.3930>>
								CASE 4			RETURN <<0.0000, 0.0000, 166.3930>>
								CASE 5			RETURN <<0.0000, 0.0000, 166.3930>>
								CASE 6			RETURN <<0.0000, 0.0000, 166.3930>>
								CASE 7			RETURN <<0.0000, 0.0000, 166.3930>>
								CASE 8			RETURN <<0.0000, 0.0000, 166.3930>>
								CASE 9			RETURN <<0.0000, 0.0000, 166.3930>>
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_SELL_PRECISION_DELIVERY
			SWITCH eHangar
				CASE LSIA_HANGAR_1
					SWITCH iExtraParam
						CASE 0
							SWITCH iProp
								CASE 0			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 1			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 2			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 3			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 4			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 5			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 6			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 7			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 8			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 9			RETURN <<0.0000, 0.0000, -29.0000>>
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH iProp
								CASE 0			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 1			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 2			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 3			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 4			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 5			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 6			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 7			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 8			RETURN <<0.0000, 0.0000, -30.6000>>
								CASE 9			RETURN <<0.0000, 0.0000, -30.6000>>
							ENDSWITCH
						BREAK
						CASE 2
							SWITCH iProp
								CASE 0			RETURN <<0.0000, 0.0000, -30.6000>>
								CASE 1			RETURN <<0.0000, 0.0000, -30.6000>>
								CASE 2			RETURN <<0.0000, 0.0000, -30.6000>>
								CASE 3			RETURN <<0.0000, 0.0000, -30.6000>>
								CASE 4			RETURN <<0.0000, 0.0000, -30.6000>>
								CASE 5			RETURN <<0.0000, 0.0000, -30.6000>>
								CASE 6			RETURN <<0.0000, 0.0000, -30.6000>>
								CASE 7			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 8			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 9			RETURN <<0.0000, 0.0000, -30.0000>>
							ENDSWITCH
						BREAK
						CASE 3
							SWITCH iProp
								CASE 0			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 1			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 2			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 3			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 4			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 5			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 6			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 7			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 8			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 9			RETURN <<0.0000, 0.0000, -30.0000>>
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				CASE LSIA_HANGAR_A17
					SWITCH iExtraParam
						CASE 0
							SWITCH iProp
								CASE 0			RETURN <<0.0000, 0.0000, -30.6000>>
								CASE 1			RETURN <<0.0000, 0.0000, -30.6000>>
								CASE 2			RETURN <<0.0000, 0.0000, -30.6000>>
								CASE 3			RETURN <<0.0000, 0.0000, -30.6000>>
								CASE 4			RETURN <<0.0000, 0.0000, -30.6000>>
								CASE 5			RETURN <<0.0000, 0.0000, -30.6000>>
								CASE 6			RETURN <<0.0000, 0.0000, -30.6000>>
								CASE 7			RETURN <<0.0000, 0.0000, -30.6000>>
								CASE 8			RETURN <<0.0000, 0.0000, -30.6000>>
								CASE 9			RETURN <<0.0000, 0.0000, -30.6000>>
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH iProp
								CASE 0			RETURN <<0.0000, 0.0000, -30.6000>>
								CASE 1			RETURN <<0.0000, 0.0000, -30.6000>>
								CASE 2			RETURN <<0.0000, 0.0000, -30.6000>>
								CASE 3			RETURN <<0.0000, 0.0000, -30.6000>>
								CASE 4			RETURN <<0.0000, 0.0000, -30.6000>>
								CASE 5			RETURN <<0.0000, 0.0000, -30.6000>>
								CASE 6			RETURN <<0.0000, 0.0000, -30.6000>>
								CASE 7			RETURN <<0.0000, 0.0000, -30.6000>>
								CASE 8			RETURN <<0.0000, 0.0000, -29.6000>>
								CASE 9			RETURN <<0.0000, 0.0000, -29.6000>>
							ENDSWITCH
						BREAK
						CASE 2
							SWITCH iProp
								CASE 0			RETURN <<0.0000, 0.0000, -29.6000>>
								CASE 1			RETURN <<0.0000, 0.0000, -29.6000>>
								CASE 2			RETURN <<0.0000, 0.0000, -29.6000>>
								CASE 3			RETURN <<0.0000, 0.0000, -29.6000>>
								CASE 4			RETURN <<0.0000, 0.0000, -29.6000>>
								CASE 5			RETURN <<0.0000, 0.0000, -29.6000>>
								CASE 6			RETURN <<0.0000, 0.0000, -29.6000>>
								CASE 7			RETURN <<0.0000, 0.0000, -29.6000>>
								CASE 8			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 9			RETURN <<0.0000, 0.0000, -29.0000>>
							ENDSWITCH
						BREAK
						CASE 3
							SWITCH iProp
								CASE 0			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 1			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 2			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 3			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 4			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 5			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 6			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 7			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 8			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 9			RETURN <<0.0000, 0.0000, -29.0000>>
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				CASE ZANCUDO_HANGAR_A2
					SWITCH iExtraParam
						CASE 0
							SWITCH iProp
								CASE 0			RETURN <<0.0000, 0.0000, -29.4000>>
								CASE 1			RETURN <<0.0000, 0.0000, -30.6000>>
								CASE 2			RETURN <<0.0000, 0.0000, -29.4000>>
								CASE 3			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 4			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 5			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 6			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 7			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 8			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 9			RETURN <<0.0000, 0.0000, -30.0000>>
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH iProp
								CASE 0			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 1			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 2			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 3			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 4			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 5			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 6			RETURN <<0.0550, 5.9780, -30.0030>>
								CASE 7			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 8			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 9			RETURN <<0.0000, 0.0000, -30.0000>>
							ENDSWITCH
						BREAK
						CASE 2
							SWITCH iProp
								CASE 0			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 1			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 2			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 3			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 4			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 5			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 6			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 7			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 8			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 9			RETURN <<0.0000, 0.0000, -30.0000>>
							ENDSWITCH
						BREAK
						CASE 3
							SWITCH iProp
								CASE 0			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 1			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 2			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 3			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 4			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 5			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 6			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 7			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 8			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 9			RETURN <<0.0000, 0.0000, -30.0000>>
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				CASE ZANCUDO_HANGAR_3497
					SWITCH iExtraParam
						CASE 0
							SWITCH iProp
								CASE 0			RETURN <<-4.2410, 2.9380, -30.8910>>
								CASE 1			RETURN <<0.0000, 0.0000, -31.0000>>
								CASE 2			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 3			RETURN <<0.0000, 0.0000, -31.0000>>
								CASE 4			RETURN <<0.0000, 0.0000, -31.0000>>
								CASE 5			RETURN <<0.0000, 0.0000, -31.0000>>
								CASE 6			RETURN <<-0.7550, 3.9730, -30.9740>>
								CASE 7			RETURN <<0.0000, 0.0000, -31.0000>>
								CASE 8			RETURN <<0.0000, 0.0000, -31.0000>>
								CASE 9			RETURN <<0.0000, 0.0000, -31.0000>>
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH iProp
								CASE 0			RETURN <<0.0000, 0.0000, -31.0000>>
								CASE 1			RETURN <<2.1990, 5.8170, -31.1120>>
								CASE 2			RETURN <<0.0000, 0.0000, -31.0000>>
								CASE 3			RETURN <<0.0000, 0.0000, -31.0000>>
								CASE 4			RETURN <<0.0000, 0.0000, -31.0000>>
								CASE 5			RETURN <<0.0000, 0.0000, -31.0000>>
								CASE 6			RETURN <<0.0000, 0.0000, -31.0000>>
								CASE 7			RETURN <<0.0000, 0.0000, -31.0000>>
								CASE 8			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 9			RETURN <<0.0000, 0.0000, -30.0000>>
							ENDSWITCH
						BREAK
						CASE 2
							SWITCH iProp
								CASE 0			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 1			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 2			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 3			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 4			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 5			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 6			RETURN <<5.3915, -1.6982, -29.9200>>
								CASE 7			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 8			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 9			RETURN <<0.0000, 0.0000, -30.0000>>
							ENDSWITCH
						BREAK
						CASE 3
							SWITCH iProp
								CASE 0			RETURN <<2.8665, -2.3018, -29.9424>>
								CASE 1			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 2			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 3			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 4			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 5			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 6			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 7			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 8			RETURN <<0.0000, 0.0000, -30.0000>>
								CASE 9			RETURN <<0.0000, 0.0000, -30.0000>>
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				CASE ZANCUDO_HANGAR_3499
					SWITCH iExtraParam
						CASE 0
							SWITCH iProp
								CASE 0			RETURN <<0.0000, 0.0000, -29.6000>>
								CASE 1			RETURN <<0.0000, 0.0000, -29.6000>>
								CASE 2			RETURN <<0.0000, 0.0000, -29.6000>>
								CASE 3			RETURN <<0.0000, 0.0000, -29.6000>>
								CASE 4			RETURN <<0.0000, 0.0000, -29.6000>>
								CASE 5			RETURN <<0.0000, 0.0000, -29.6000>>
								CASE 6			RETURN <<0.0000, 0.0000, -29.6000>>
								CASE 7			RETURN <<0.0000, 0.0000, -29.6000>>
								CASE 8			RETURN <<4.1110, -0.8320, -29.5700>>
								CASE 9			RETURN <<0.0000, 0.0000, -29.6000>>
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH iProp
								CASE 0			RETURN <<0.0000, 0.0000, -29.6000>>
								CASE 1			RETURN <<0.0000, 0.0000, -29.6000>>
								CASE 2			RETURN <<0.0000, 0.0000, -29.6000>>
								CASE 3			RETURN <<0.0000, 0.0000, -29.6000>>
								CASE 4			RETURN <<0.0000, 0.0000, -29.6000>>
								CASE 5			RETURN <<0.0000, 0.0000, -29.6000>>
								CASE 6			RETURN <<0.0000, 0.0000, -29.6000>>
								CASE 7			RETURN <<0.0000, 0.0000, -29.6000>>
								CASE 8			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 9			RETURN <<0.0000, 0.0000, -29.0000>>
							ENDSWITCH
						BREAK
						CASE 2
							SWITCH iProp
								CASE 0			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 1			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 2			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 3			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 4			RETURN <<5.4602, 0.0633, -29.0031>>
								CASE 5			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 6			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 7			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 8			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 9			RETURN <<0.0000, 0.0000, -29.0000>>
							ENDSWITCH
						BREAK
						CASE 3
							SWITCH iProp
								CASE 0			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 1			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 2			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 3			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 4			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 5			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 6			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 7			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 8			RETURN <<0.0000, 0.0000, -29.0000>>
								CASE 9			RETURN <<0.0000, 0.0000, -29.0000>>
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SMV_SETUP_RON
			SWITCH eSubvariation
				CASE SMS_SETUP_DELPERROBEACH 
					SWITCH iProp
						CASE 0						RETURN <<0.0000, -0.0000, -171.6003>>
						CASE 1						RETURN <<0.0000, -0.0000, -176.2009>>
					ENDSWITCH						
				BREAK								
				CASE SMS_SETUP_LSRIVER				
					SWITCH iProp					
						CASE 0						RETURN <<0.0000, 0.0000, 65.9983>>
						CASE 1						RETURN <<0.0000, -0.0000, -148.4013>>
					ENDSWITCH						
				BREAK								
				CASE SMS_SETUP_ELBURRO				
					SWITCH iProp					
						CASE 0						RETURN <<0.0000, -0.0000, 133.3976>>
						CASE 1						RETURN <<0.0000, 0.0000, 78.1974>>
					ENDSWITCH						
				BREAK								
				CASE SMS_SETUP_CHUMASH				
					SWITCH iProp					
						CASE 0						RETURN <<6.4329, -5.9298, -68.4704>>
						CASE 1						RETURN <<7.6215, -3.4004, -92.5776>>
					ENDSWITCH						
				BREAK								
				CASE SMS_SETUP_TATAVIUM				
					SWITCH iProp					
						CASE 0						RETURN <<6.2037, -13.9837, 176.3571>>
						CASE 1						RETURN <<-5.4711, 14.2811, -0.7189>>
					ENDSWITCH						
				BREAK								
				CASE SMS_SETUP_RONSWINDFARM			
					SWITCH iProp					
						CASE 0						RETURN <<3.3779, 2.8417, 82.7123>>
						CASE 1						RETURN <<4.4738, 0.6099, 116.9724>>
					ENDSWITCH	
				BREAK								
				CASE SMS_SETUP_GRANDSENORA1			
					SWITCH iProp					
						CASE 0						RETURN <<0.0000, 0.0000, 71.7935>>
						CASE 1						RETURN <<0.0000, -0.0000, -109.4069>>
					ENDSWITCH						
				BREAK								
				CASE SMS_SETUP_GRANDSENORA2			
					SWITCH iProp					
						CASE 0						RETURN <<0.0000, -0.0000, 111.7932>>
						CASE 1						RETURN <<7.9562, 3.6829, 174.5370>>
					ENDSWITCH						
				BREAK								
				CASE SMS_SETUP_SANDY				
					SWITCH iProp					
						CASE 0						RETURN <<0.0000, 0.0000, 32.1979>>
						CASE 1						RETURN <<0.0000, -0.0000, 158.1977>>
					ENDSWITCH						
				BREAK								
				CASE SMS_SETUP_STAB					
					SWITCH iProp					
						CASE 0						RETURN <<0.0000, 0.0000, 62.1348>>
						CASE 1						RETURN <<2.8993, 5.2114, -30.9972>>
					ENDSWITCH						
				BREAK								
				CASE SMS_SETUP_CALAFIA				
					SWITCH iProp					
						CASE 0						RETURN <<0.2553, -7.5261, 174.9521>>
						CASE 1						RETURN <<4.1365, 7.7901, 34.0532>>
					ENDSWITCH						
				BREAK								
				CASE SMS_SETUP_MCKENZIE				
					SWITCH iProp					
						CASE 0						RETURN <<0.0000, -0.0000, 100.1324>>
						CASE 1						RETURN <<-0.0000, -0.0000, -7.4680>>
					ENDSWITCH						
				BREAK								
				CASE SMS_SETUP_GRAPESEED			
					SWITCH iProp					
						CASE 0						RETURN <<0.9602, -4.6612, 128.9724>>
						CASE 1						RETURN <<-4.6705, -1.7128, -171.1368>>
					ENDSWITCH						
				BREAK								
				CASE SMS_SETUP_PORCOPIO				
					SWITCH iProp					
						CASE 0						RETURN <<0.0000, -0.0000, -127.4575>>
						CASE 1						RETURN <<1.2151, 3.5738, 168.7046>>
					ENDSWITCH						
				BREAK								
				CASE SMS_SETUP_PALETO				
					SWITCH iProp					
						CASE 0						RETURN <<0.0000, 0.0000, 58.9409>>
						CASE 1						RETURN <<-0.0000, -0.0000, -22.2592>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

