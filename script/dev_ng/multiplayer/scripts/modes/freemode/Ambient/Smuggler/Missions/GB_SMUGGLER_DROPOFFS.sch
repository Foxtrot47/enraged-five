USING "globals.sch"
USING "net_include.sch"
USING "net_freemode_delivery_common.sch"


FUNC BOOL SMUGGLER_DROPOFF_IS_VALID(FREEMODE_DELIVERY_DROPOFFS &eDropoffID)
	RETURN ( (ENUM_TO_INT(eDropoffID) >= ENUM_TO_INT(SMUGGLER_DROPOFF_LSIA_HANGAR_1)) AND (ENUM_TO_INT(eDropoffID) <= ENUM_TO_INT(SMUGGLER_DROPOFF_UNDER_RADAR_30)) )
ENDFUNC

FUNC SMUGGLER_VARIATION SMUGGLER_GET_MISSION_VARIATION_FROM_DROPOFF(FREEMODE_DELIVERY_DROPOFFS &eDropoffID)
	IF SMUGGLER_DROPOFF_IS_VALID(eDropoffID)
		IF  ( (ENUM_TO_INT(eDropoffID) >= ENUM_TO_INT(SMUGGLER_DROPOFF_HEAVY_LIFTING_1)) AND (ENUM_TO_INT(eDropoffID) <= ENUM_TO_INT(SMUGGLER_DROPOFF_HEAVY_LIFTING_20)) )
			RETURN SMV_SELL_HEAVY_LIFTING
		ENDIF
		
		IF  ( (ENUM_TO_INT(eDropoffID) >= ENUM_TO_INT(SMUGGLER_DROPOFF_CONTESTED_1)) AND (ENUM_TO_INT(eDropoffID) <= ENUM_TO_INT(SMUGGLER_DROPOFF_CONTESTED_30)) )
			RETURN SMV_SELL_CONTESTED
		ENDIF
		
		IF  ( (ENUM_TO_INT(eDropoffID) >= ENUM_TO_INT(SMUGGLER_DROPOFF_AGILE_DELIVERY_1)) AND (ENUM_TO_INT(eDropoffID) <= ENUM_TO_INT(SMUGGLER_DROPOFF_AGILE_DELIVERY_60)) )
			RETURN SMV_SELL_AGILE_DELIVERY
		ENDIF
		
		IF  ( (ENUM_TO_INT(eDropoffID) >= ENUM_TO_INT(SMUGGLER_DROPOFF_PRECISION_DELIVERY_1)) AND (ENUM_TO_INT(eDropoffID) <= ENUM_TO_INT(SMUGGLER_DROPOFF_PRECISION_DELIVERY_30)) )
			RETURN SMV_SELL_PRECISION_DELIVERY
		ENDIF
		
		IF  ( (ENUM_TO_INT(eDropoffID) >= ENUM_TO_INT(SMUGGLER_DROPOFF_FLYING_FORTRESS_1)) AND (ENUM_TO_INT(eDropoffID) <= ENUM_TO_INT(SMUGGLER_DROPOFF_FLYING_FORTRESS_30)) )
			RETURN SMV_SELL_FLYING_FORTRESS
		ENDIF
		
		IF  ( (ENUM_TO_INT(eDropoffID) >= ENUM_TO_INT(SMUGGLER_DROPOFF_FLY_LOW_1)) AND (ENUM_TO_INT(eDropoffID) <= ENUM_TO_INT(SMUGGLER_DROPOFF_FLY_LOW_33)) )
			RETURN SMV_SELL_FLY_LOW
		ENDIF
		
		IF  ( (ENUM_TO_INT(eDropoffID) >= ENUM_TO_INT(SMUGGLER_DROPOFF_AIR_DELIVERY_1)) AND (ENUM_TO_INT(eDropoffID) <= ENUM_TO_INT(SMUGGLER_DROPOFF_AIR_DELIVERY_30)) )
			RETURN SMV_SELL_AIR_DELIVERY
		ENDIF
		
		IF  ( (ENUM_TO_INT(eDropoffID) >= ENUM_TO_INT(SMUGGLER_DROPOFF_AIR_POLICE_1)) AND (ENUM_TO_INT(eDropoffID) <= ENUM_TO_INT(SMUGGLER_DROPOFF_AIR_POLICE_30)) )
			RETURN SMV_SELL_AIR_POLICE
		ENDIF
		
		IF  ( (ENUM_TO_INT(eDropoffID) >= ENUM_TO_INT(SMUGGLER_DROPOFF_UNDER_RADAR_1)) AND (ENUM_TO_INT(eDropoffID) <= ENUM_TO_INT(SMUGGLER_DROPOFF_UNDER_RADAR_30)) )
			RETURN SMV_SELL_UNDER_THE_RADAR
		ENDIF
		
	ENDIF
	
	RETURN SMV_INVALID
ENDFUNC

FUNC BOOL SMUGGLER_DROPOFF_SHOULD_RUN_DELIVERY_SCRIPT(FREEMODE_DELIVERY_DROPOFFS eDropoffID)	
	SWITCH SMUGGLER_GET_MISSION_VARIATION_FROM_DROPOFF(eDropoffID)
		CASE SMV_SELL_PRECISION_DELIVERY 
		RETURN TRUE
	ENDSWITCH
	
	IF SMUGGLER_DROPOFF_IS_PLAYER_HANGAR(eDropoffID)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC VECTOR SMUGGLER_DROPOFF_GET_LOCATE_SIZE_MULTIPLIER(SMUGGLER_VARIATION eMissionVar)
	UNUSED_PARAMETER(eMissionVar)
	VECTOR vReturn
		
		//SWITCH eMissionVar			
		//	DEFAULT
				vReturn = <<1.0, 1.0, 1.0>>
		//ENDSWITCH
		
	RETURN vReturn
ENDFUNC

PROC SMUGGLER_DROPOFF_WILL_ACCEPT_TYPE_OF_DELIVERY(FREEMODE_DELIVERY_DROPOFFS eDropoffID, FREEMODE_DELIVERABLE_TYPE eType, BOOL &bWillAcceptOnFoot, BOOL &bWillAcceptInCar)
	UNUSED_PARAMETER(eDropoffID)
	IF eType = SMUGGLER_DELIVERABLE_VEHICLE
		bWillAcceptOnFoot = FALSE
		bWillAcceptInCar = TRUE
	ELIF eType = SMUGGLER_DELIVERABLE_ART_AND_ANTIQUES
	OR eType = SMUGGLER_DELIVERABLE_ANIMAL_MATERIALS
	OR eType = SMUGGLER_DELIVERABLE_CHEMICALS
	OR eType = SMUGGLER_DELIVERABLE_COUNTERFEIT_GOODS
	OR eType = SMUGGLER_DELIVERABLE_JEWELRY_AND_GEMSTONES
	OR eType = SMUGGLER_DELIVERABLE_MEDICAL_SUPPLIES
	OR eType = SMUGGLER_DELIVERABLE_NARCOTICS
	OR eType = SMUGGLER_DELIVERABLE_TOBACCO_AND_ALCOHOL
		bWillAcceptOnFoot = TRUE
		bWillAcceptInCar = TRUE
	ENDIF
ENDPROC

FUNC INT SMUGGLER_GET_MAX_DROPOFFS_ON_ROUTE()	
	RETURN 3//FREEMODE_DELIVERY_MAX_ACTIVE_DROPOFFS
ENDFUNC

/// PURPOSE:
///    Finds the next freemode delivery dropoff in a delivery routes sequence
/// PARAMS:
///    eRoute - The current FREEMODE_DELIVERY_ROUTES in use
///    iDropoff - The next dropoff to find. Expects 1 - Max dropoffs for the given route
FUNC FREEMODE_DELIVERY_DROPOFFS SMUGGLER_GET_DROPOFF_ON_ROUTE(FREEMODE_DELIVERY_ROUTES eRoute, INT iDropoff)
	
	FREEMODE_DELIVERY_DROPOFFS eDropoff = FREEMODE_DELIVERY_DROPOFF_INVALID
	INT iSelectedDropoff
	
	IF iDropoff > SMUGGLER_GET_MAX_DROPOFFS_ON_ROUTE()
		CASSERTLN(DEBUG_AMBIENT, "SMUGGLER_GET_DROPOFF_ON_ROUTE invalid dropoff requested: ", iDropoff)
		RETURN eDropoff
	ENDIF
	
	CDEBUG1LN(DEBUG_AMBIENT, "[FM_DO_RES] SMUGGLER_GET_DROPOFF_ON_ROUTE looking for dropoff: ", iDropoff, " on route: ", eRoute)
	
	SWITCH eRoute
		///////////////////////////////////
		///      Flying Fortress Routes
		CASE FREEMODE_DELIVERY_FLYING_FORTRESS_1
		CASE FREEMODE_DELIVERY_FLYING_FORTRESS_2
		CASE FREEMODE_DELIVERY_FLYING_FORTRESS_3
		CASE FREEMODE_DELIVERY_FLYING_FORTRESS_4
		CASE FREEMODE_DELIVERY_FLYING_FORTRESS_5
		CASE FREEMODE_DELIVERY_FLYING_FORTRESS_6
		CASE FREEMODE_DELIVERY_FLYING_FORTRESS_7
		CASE FREEMODE_DELIVERY_FLYING_FORTRESS_8
		CASE FREEMODE_DELIVERY_FLYING_FORTRESS_9
		CASE FREEMODE_DELIVERY_FLYING_FORTRESS_10
			iSelectedDropoff = ((ENUM_TO_INT(eRoute) - ENUM_TO_INT(FREEMODE_DELIVERY_FLYING_FORTRESS_1)) + 1)
			eDropoff = SMUGGLER_DROPOFF_FLYING_FORTRESS_1
		BREAK
		
	ENDSWITCH
	
	//Find the last dropoff on the given route
	iSelectedDropoff *= SMUGGLER_GET_MAX_DROPOFFS_ON_ROUTE()
	//Work back to the first dropoff on that route
	iSelectedDropoff -= SMUGGLER_GET_MAX_DROPOFFS_ON_ROUTE()
	//Add the calculated value to the first dropoff of route 1
	iSelectedDropoff += ENUM_TO_INT(eDropoff)
	//Work out the correct first dropoff from given route
	iSelectedDropoff += (iDropoff - 1)
	//Assign the dropoff ID
	eDropoff = INT_TO_ENUM(FREEMODE_DELIVERY_DROPOFFS, iSelectedDropoff)	
	
	//CDEBUG1LN(DEBUG_AMBIENT, "[FM_DO_RES] SMUGGLER_GET_DROPOFF_ON_ROUTE returning ", FREEMODE_DELIVERY_GET_DROPOFF_DEBUG_NAME(eDropoff), " iSelectedDropoff = ", iSelectedDropoff)
	
	RETURN eDropoff
ENDFUNC

FUNC VECTOR SMUGGLER_GET_DROPOFF_LOCATE_SIZE(FREEMODE_DELIVERY_DROPOFFS eDropoffID, BOOL bOnFoot)
	
	UNUSED_PARAMETER(eDropoffID)

	VECTOR vReturnVector = <<1.0, 1.0, 1.0>>
	
	IF bOnFoot
		vReturnVector = <<1.0, 1.0, 1.0>>
	ENDIF
	
	RETURN vReturnVector
ENDFUNC

PROC SMUGGLER_GET_HEAVY_LIFTING_DROPOFF_COORDS(FREEMODE_DELIVERY_DROPOFFS eDropoffID, FREEMODE_DELIVERY_DROPOFF_DETAILS &sDetails)
	
	SET_BIT(sDetails.iBS, FREEMODE_DELIVERY_DROPOFF_DETAILS_BS_DROPOFF_HAS_DROP_ZONE)
	SET_BIT(sDetails.iBS, FREEMODE_DELIVERY_DROPOFF_DETAILS_BS_DROPOFF_DROPZONE_USES_AA)
	
	SWITCH eDropoffID
		//LSIA HANGARS
		CASE SMUGGLER_DROPOFF_HEAVY_LIFTING_1
			sDetails.vInCarCorona = <<874.7770, 2344.2939, 50.6860>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<861.025146,2349.383789,49.647938>>	
			sDetails.vAirDrop2 = <<881.173401,2349.657471,70.690796>>
			sDetails.fAirDropAreaWidth = 30.000000
		BREAK
		CASE SMUGGLER_DROPOFF_HEAVY_LIFTING_2
			sDetails.vInCarCorona = <<-7.2040, 6199.6748, 38.7550>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-3.588748,6194.030762,30.691616>>	
			sDetails.vAirDrop2 = <<-18.894112,6213.560059,70.391876>>
			sDetails.fAirDropAreaWidth = 45.000000
		BREAK
		CASE SMUGGLER_DROPOFF_HEAVY_LIFTING_3
			sDetails.vInCarCorona = <<1991.2960, 5002.7710, 40.4100>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1982.497192,5013.054199,37.349178>>	
			sDetails.vAirDrop2 = <<2002.316895,4990.891602,70.407791>>
			sDetails.fAirDropAreaWidth = 40.000000
		BREAK
		CASE SMUGGLER_DROPOFF_HEAVY_LIFTING_4
			sDetails.vInCarCorona = <<1787.8630, 3397.8989, 39.7210>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1779.740356,3394.033203,36.238174>>	
			sDetails.vAirDrop2 = <<1803.400757,3407.282959,69.571098>>
			sDetails.fAirDropAreaWidth = 40.000000
		BREAK
		CASE SMUGGLER_DROPOFF_HEAVY_LIFTING_5
			sDetails.vInCarCorona = <<-2022.1130, 561.0860, 106.9500>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-2024.853149,543.642090,106.167572>>	
			sDetails.vAirDrop2 = <<-2018.601929,575.488647,139.334946>>
			sDetails.fAirDropAreaWidth = 32.000000
		BREAK
		CASE SMUGGLER_DROPOFF_HEAVY_LIFTING_6
			sDetails.vInCarCorona = <<3848.9487, 4749.0522, 4.2995>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<3857.232910,4736.315430,2.018371>>
			sDetails.vAirDrop2 = <<3841.511963,4761.041016,30.149063>>
			sDetails.fAirDropAreaWidth = 25.000000
		BREAK
		CASE SMUGGLER_DROPOFF_HEAVY_LIFTING_7
			sDetails.vInCarCorona = <<942.1198, 6940.5366, 4.2995>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<939.618958,6925.514160,1.983978>>	
			sDetails.vAirDrop2 = <<944.592041,6954.485840,29.812134>>
			sDetails.fAirDropAreaWidth = 25.000000
		BREAK
		CASE SMUGGLER_DROPOFF_HEAVY_LIFTING_8
			sDetails.vInCarCorona = <<-757.7433, 6175.5415, 4.1283>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-771.907410,6176.818848,2.186861>>	
			sDetails.vAirDrop2 = <<-743.027893,6174.760742,30.680489>>
			sDetails.fAirDropAreaWidth = 25.000000
		BREAK
		CASE SMUGGLER_DROPOFF_HEAVY_LIFTING_9
			sDetails.vInCarCorona = <<-1807.1444, 5279.7256, 4.2995>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-1812.923340,5266.890137,2.169169>>	
			sDetails.vAirDrop2 = <<-1800.996094,5293.814453,30.289665>>
			sDetails.fAirDropAreaWidth = 25.000000
		BREAK
		CASE SMUGGLER_DROPOFF_HEAVY_LIFTING_10
			sDetails.vInCarCorona = <<-3414.8931, 1565.5381, 4.2995>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-3428.291504,1561.292236,2.136420>>	
			sDetails.vAirDrop2 = <<-3400.508545,1570.362427,30.369434>>
			sDetails.fAirDropAreaWidth = 25.000000
		BREAK
		
		//ZANCUDO HANGARS
		CASE SMUGGLER_DROPOFF_HEAVY_LIFTING_11
			sDetails.vInCarCorona = <<-347.5480, -2421.6860, 1.6520>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-359.114868,-2435.098145,-1.131242>>
			sDetails.vAirDrop2 = <<-336.071320,-2407.628662,26.869577>>
			sDetails.fAirDropAreaWidth = 9.000000
		BREAK
		CASE SMUGGLER_DROPOFF_HEAVY_LIFTING_12
			sDetails.vInCarCorona = <<1043.3719, -2881.1660, 18.0180>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1024.188477,-2880.971680,15.035818>>	
			sDetails.vAirDrop2 = <<1063.540649,-2881.016113,43.035820>> 
			sDetails.fAirDropAreaWidth = 30.000000
		BREAK
		CASE SMUGGLER_DROPOFF_HEAVY_LIFTING_13
			sDetails.vInCarCorona = <<924.8960, -1240.7469, 24.5240>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<932.994629,-1260.574463,22.685463>>	
			sDetails.vAirDrop2 = <<912.221436,-1232.266235,49.511959>>
			sDetails.fAirDropAreaWidth = 30.000000
		BREAK
		CASE SMUGGLER_DROPOFF_HEAVY_LIFTING_14
			sDetails.vInCarCorona = <<-451.9960, -918.8160, 46.9850>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-451.651123,-935.543762,43.984642>> 	
			sDetails.vAirDrop2 = <<-452.487640,-897.917725,71.988937>>
			sDetails.fAirDropAreaWidth = 25.000000
		BREAK
		CASE SMUGGLER_DROPOFF_HEAVY_LIFTING_15
			sDetails.vInCarCorona = <<1147.5970, 124.9690, 80.9670>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1133.213989,100.168526,77.023537>>	
			sDetails.vAirDrop2 = <<1163.710815,149.814911,104.885765>>
			sDetails.fAirDropAreaWidth = 60.000000
		BREAK
		CASE SMUGGLER_DROPOFF_HEAVY_LIFTING_16
			sDetails.vInCarCorona = <<673.8410, 1285.4800, 362.1030>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<675.871643,1288.756958,356.295990>>
			sDetails.vAirDrop2 = <<676.097900,1276.259521,384.295990>>
			sDetails.fAirDropAreaWidth = 30.000000
		BREAK
		CASE SMUGGLER_DROPOFF_HEAVY_LIFTING_17
			sDetails.vInCarCorona = <<-2316.1685, -1328.5548, 4.2995>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-2322.771973,-1342.041748,2.358489>>	
			sDetails.vAirDrop2 = <<-2309.929443,-1315.937012,30.416939>>
			sDetails.fAirDropAreaWidth = 25.000000
		BREAK
		CASE SMUGGLER_DROPOFF_HEAVY_LIFTING_18
			sDetails.vInCarCorona = <<2288.2617, -2547.0991, -24.1587>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<2278.694824,-2557.841064,2.410532>>	
			sDetails.vAirDrop2 = <<2298.014648,-2536.507324,30.381504>>
			sDetails.fAirDropAreaWidth = 25.000000
		BREAK
		CASE SMUGGLER_DROPOFF_HEAVY_LIFTING_19
			sDetails.vInCarCorona = <<3107.6399, 822.0205, -5.6705>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<3117.856689,811.942200,2.199003>>	
			sDetails.vAirDrop2 =<<3097.106934,831.822815,30.358337>>
			sDetails.fAirDropAreaWidth = 25.000000
		BREAK
		CASE SMUGGLER_DROPOFF_HEAVY_LIFTING_20
			sDetails.vInCarCorona = <<3430.3401, 2275.2437, -21.7691>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<3422.722412,2263.081055,2.113419>>	
			sDetails.vAirDrop2 = <<3438.165283,2287.442871,30.348886>>
			sDetails.fAirDropAreaWidth = 25.000000
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL SMUGGLER_GET_CONTESTED_ALTERNATE_AIR_DROP_AREA_COORDS(FREEMODE_DELIVERY_DROPOFFS eDropoffID, VECTOR &vec1, VECTOR &vec2, FLOAT &fWidth)	
	SWITCH eDropoffID
		CASE SMUGGLER_DROPOFF_CONTESTED_8 			
			vec1 = <<-2267.026367,402.225677,183.667038>>
			vec2 = <<-2257.724365,380.619598,267.602356>>
			fWidth = 20.0
			RETURN TRUE
		CASE SMUGGLER_DROPOFF_CONTESTED_9		
			vec1 = <<-757.326355,-192.947479,43.208218>>	
			vec2 = <<-745.316833,-213.009674,128.118546>>
			fWidth = 18.0
			RETURN TRUE
		CASE SMUGGLER_DROPOFF_CONTESTED_12	
			vec1 = <<-410.776306,-319.030029,70.803314>>
			vec2 = <<-380.264008,-340.973602,155.803314>>
			fWidth = 16.0
			RETURN TRUE
		CASE SMUGGLER_DROPOFF_CONTESTED_20 			
			vec1 = <<740.653687,1287.007080,353.296661>>
			vec2 = <<716.817322,1286.362671,439.295990>>
			fWidth = 30.0
			RETURN TRUE
		CASE SMUGGLER_DROPOFF_CONTESTED_29		
			vec1 = <<-2595.064453,1927.041138,167.046326>>	
			vec2 = <<-2589.626221,1882.499756,250.492676>>
			fWidth = 22.0
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC SMUGGLER_GET_CONTESTED_DROPOFF_COORDS(FREEMODE_DELIVERY_DROPOFFS eDropoffID, FREEMODE_DELIVERY_DROPOFF_DETAILS &sDetails)
	
	SET_BIT(sDetails.iBS, FREEMODE_DELIVERY_DROPOFF_DETAILS_BS_DROPOFF_HAS_DROP_ZONE)
	SET_BIT(sDetails.iBS, FREEMODE_DELIVERY_DROPOFF_DETAILS_BS_DROPOFF_DROPZONE_USES_AA)
	
	SWITCH eDropoffID
		CASE SMUGGLER_DROPOFF_CONTESTED_1
			sDetails.vInCarCorona = <<-1256.4780, -834.6710, 64.3310>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-1260.381104,-827.613403,64.139160>>	
			sDetails.vAirDrop2 = <<-1238.341064,-860.158875,145.122498>> 
			sDetails.fAirDropAreaWidth = 17.00
		BREAK
		CASE SMUGGLER_DROPOFF_CONTESTED_2
			sDetails.vInCarCorona = <<228.3380, -3185.3669, 39.4690>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<230.021637,-3137.968506,37.165092>>	
			sDetails.vAirDrop2 = <<230.204910,-3248.130615,119.602890>>
			sDetails.fAirDropAreaWidth = 67.00
		BREAK
		CASE SMUGGLER_DROPOFF_CONTESTED_3
			sDetails.vInCarCorona = <<-147.5700, -1279.3280, 46.8980>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-122.062286,-1274.618042,44.342434>>	
			sDetails.vAirDrop2 = <<-173.799133,-1275.016113,126.898102>>
			sDetails.fAirDropAreaWidth = 35.00
		BREAK
		CASE SMUGGLER_DROPOFF_CONTESTED_4
			sDetails.vInCarCorona = <<910.3700, -1681.3560, 50.1320>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<941.959595,-1687.260132,36.061821>>	
			sDetails.vAirDrop2 = <<881.283081,-1681.779663,126.141640>>
			sDetails.fAirDropAreaWidth = 30.00
		BREAK
		CASE SMUGGLER_DROPOFF_CONTESTED_5
			sDetails.vInCarCorona = <<934.2630, -938.1370, 58.0920>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<927.352112,-914.684631,53.090317>>	
			sDetails.vAirDrop2 = <<927.365234,-980.604797,138.095673>>
			sDetails.fAirDropAreaWidth = 55.00
		BREAK
		CASE SMUGGLER_DROPOFF_CONTESTED_6
			sDetails.vInCarCorona = <<84.2060, -350.9300, 66.2020>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<56.185104,-333.029633,61.144379>>	
			sDetails.vAirDrop2 = <<96.682510,-347.644806,146.144379>>
			sDetails.fAirDropAreaWidth = 30.00
		BREAK
		CASE SMUGGLER_DROPOFF_CONTESTED_7
			sDetails.vInCarCorona = <<-1575.9210, 214.7390, 73.3380>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-1596.796875,198.902847,68.838554>>
			sDetails.vAirDrop2 = <<-1556.020874,217.965469,153.838562>>
			sDetails.fAirDropAreaWidth = 39.00
		BREAK
		CASE SMUGGLER_DROPOFF_CONTESTED_8
			sDetails.vInCarCorona = <<-2262.5320, 368.6930, 187.6020>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-2261.236084,353.308350,183.667038>>	
			sDetails.vAirDrop2 = <<-2272.480469,378.959778,267.602356>>
			sDetails.fAirDropAreaWidth = 30.00 
			SET_BIT(sDetails.iBS, FREEMODE_DELIVERY_DROPOFF_DETAILS_BS_DROPOFF_MULTI_DROP_ZONE)
		BREAK
		CASE SMUGGLER_DROPOFF_CONTESTED_9
			sDetails.vInCarCorona = <<-743.2130, -221.3890, 47.5190>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-759.461548,-226.510986,43.118549>>
			sDetails.vAirDrop2 = <<-733.825684,-215.857727,128.118561>>
			sDetails.fAirDropAreaWidth = 23.00
			SET_BIT(sDetails.iBS, FREEMODE_DELIVERY_DROPOFF_DETAILS_BS_DROPOFF_MULTI_DROP_ZONE)
		BREAK
		CASE SMUGGLER_DROPOFF_CONTESTED_10
			sDetails.vInCarCorona = <<40.7000, -1777.0830, 46.7000>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<33.932240,-1787.517578,41.512081>>	
			sDetails.vAirDrop2 = <<80.863388,-1730.698975,126.700302>>
			sDetails.fAirDropAreaWidth = 27.00
		BREAK
		CASE SMUGGLER_DROPOFF_CONTESTED_11
			sDetails.vInCarCorona = <<847.5530, -2523.7400, 39.5250>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<806.315735,-2519.168457,36.535500>>
			sDetails.vAirDrop2 = <<881.936035,-2524.729004,127.297119>>
			sDetails.fAirDropAreaWidth = 25.00
		BREAK
		CASE SMUGGLER_DROPOFF_CONTESTED_12
			sDetails.vInCarCorona = <<-398.9756, -343.3768, 69.9677>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-416.543121,-346.082123,71.054367>>	
			sDetails.vAirDrop2 = <<-375.170044,-351.419067,155.803314>>
			sDetails.fAirDropAreaWidth = 25.00
			SET_BIT(sDetails.iBS, FREEMODE_DELIVERY_DROPOFF_DETAILS_BS_DROPOFF_MULTI_DROP_ZONE)
		BREAK
		CASE SMUGGLER_DROPOFF_CONTESTED_13
			sDetails.vInCarCorona = <<77.9442, -922.8723, 84.2884>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<107.646149,-933.997009,44.614899>>
			sDetails.vAirDrop2 = <<54.397060,-914.745117,166.614899>>
			sDetails.fAirDropAreaWidth = 85.00
		BREAK
		CASE SMUGGLER_DROPOFF_CONTESTED_14
			sDetails.vInCarCorona = <<185.7760, 255.6760, 140.4780>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<179.023529,227.560181,135.928207>>
			sDetails.vAirDrop2 = <<200.980133,288.239716,220.928177>>
			sDetails.fAirDropAreaWidth = 28.00
		BREAK
		CASE SMUGGLER_DROPOFF_CONTESTED_15
			sDetails.vInCarCorona = <<1240.4080, -2994.6899, 11.1640>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1240.565918,-3002.223877,3.319258>>	
			sDetails.vAirDrop2 = <<1240.272827,-2950.455811,88.319260>>
			sDetails.fAirDropAreaWidth = 27.00
		BREAK
		CASE SMUGGLER_DROPOFF_CONTESTED_16
			sDetails.vInCarCorona = <<-137.6750, 6176.4771, 39.1650>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-149.822311,6160.892090,35.822548>>
			sDetails.vAirDrop2 = <<-115.877419,6195.788574,121.921494>>
			sDetails.fAirDropAreaWidth = 32.00
		BREAK
		CASE SMUGGLER_DROPOFF_CONTESTED_17
			sDetails.vInCarCorona = <<2740.5100, 3474.7290, 72.7040>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<2722.110840,3442.815918,66.517601>>	
			sDetails.vAirDrop2 = <<2751.009766,3512.018555,152.518097>>
			sDetails.fAirDropAreaWidth = 27.00
		BREAK
		CASE SMUGGLER_DROPOFF_CONTESTED_18
			sDetails.vInCarCorona = <<1710.5861, 4779.9839, 46.5850>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1711.055054,4760.764648,41.411781>>
			sDetails.vAirDrop2 = <<1711.034058,4790.054688,127.412865>>
			sDetails.fAirDropAreaWidth = 30.00
		BREAK
		CASE SMUGGLER_DROPOFF_CONTESTED_19
			sDetails.vInCarCorona = <<590.2340, 2763.8511, 50.1450>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<598.392822,2744.509277,44.435173>>	
			sDetails.vAirDrop2 = <<595.883545,2782.834717,127.998497>>
			sDetails.fAirDropAreaWidth = 52.00
		BREAK
		CASE SMUGGLER_DROPOFF_CONTESTED_20
			sDetails.vInCarCorona = <<734.5100, 1292.1560, 359.2960>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<739.744629,1288.609131,353.296661>>
			sDetails.vAirDrop2 = <<769.988892,1287.509399,439.296661>> 
			sDetails.fAirDropAreaWidth = 55.00
			SET_BIT(sDetails.iBS, FREEMODE_DELIVERY_DROPOFF_DETAILS_BS_DROPOFF_MULTI_DROP_ZONE)
		BREAK
		CASE SMUGGLER_DROPOFF_CONTESTED_21
			sDetails.vInCarCorona = <<-1111.5740, 2704.8701, 22.8410>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-1123.026855,2693.358154,17.492786>> 	
			sDetails.vAirDrop2 = <<-1098.111816,2716.403076,102.483894>>
			sDetails.fAirDropAreaWidth = 22.00
		BREAK
		CASE SMUGGLER_DROPOFF_CONTESTED_22
			sDetails.vInCarCorona = <<1518.6439, 3578.3745, 41.0232>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1542.940918,3591.817871,35.109852>> 	
			sDetails.vAirDrop2 = <<1506.197632,3570.450195,121.111908>>
			sDetails.fAirDropAreaWidth = 18.00
		BREAK
		CASE SMUGGLER_DROPOFF_CONTESTED_23
			sDetails.vInCarCorona = <<-1594.4871, 763.1670, 188.1940>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-1613.787354,760.437683,182.243103>>	
			sDetails.vAirDrop2 = <<-1563.256958,784.199646,275.949615>>
			sDetails.fAirDropAreaWidth = 45.00
		BREAK
		CASE SMUGGLER_DROPOFF_CONTESTED_24
			sDetails.vInCarCorona = <<250.0210, 3610.6770, 32.9310>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<239.750565,3564.669189,27.761375>>	
			sDetails.vAirDrop2 = <<249.512482,3627.260986,112.543686>>
			sDetails.fAirDropAreaWidth = 48.00
		BREAK
		CASE SMUGGLER_DROPOFF_CONTESTED_25
			sDetails.vInCarCorona = <<156.1960, 7353.5791, 10.5210>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<137.437424,7337.536621,2.219269>>
			sDetails.vAirDrop2 = <<177.068985,7369.921875,90.698853>>
			sDetails.fAirDropAreaWidth = 48.00
		BREAK
		CASE SMUGGLER_DROPOFF_CONTESTED_26
			sDetails.vInCarCorona = <<2936.5010, 777.4730, 25.6800>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<2904.621826,760.860229,12.970413>>
			sDetails.vAirDrop2 = <<2958.999756,809.292847,102.531136>>
			sDetails.fAirDropAreaWidth = 48.00
		BREAK
		CASE SMUGGLER_DROPOFF_CONTESTED_27
			sDetails.vInCarCorona = <<3623.0090, 5676.7261, 6.7720>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<3596.059814,5674.643066,1.261269>>	
			sDetails.vAirDrop2 = <<3674.869141,5651.379883,102.201447>>
			sDetails.fAirDropAreaWidth = 39.250000
		BREAK
		CASE SMUGGLER_DROPOFF_CONTESTED_28
			sDetails.vInCarCorona = <<2373.8376, 6615.3403, 1.1413>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<2385.574951,6643.938477,-11.103401>>	
			sDetails.vAirDrop2 = <<2372.584229,6580.025879,87.708778>>
			sDetails.fAirDropAreaWidth = 60.00
		BREAK
		CASE SMUGGLER_DROPOFF_CONTESTED_29
			sDetails.vInCarCorona = <<-2589.9417, 1884.4567, 170.4919>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-2597.960693,1873.451416,165.679886>>
			sDetails.vAirDrop2 = <<-2576.077148,1892.282837,250.493042>>
			sDetails.fAirDropAreaWidth = 20.0
			SET_BIT(sDetails.iBS, FREEMODE_DELIVERY_DROPOFF_DETAILS_BS_DROPOFF_MULTI_DROP_ZONE)
		BREAK
		CASE SMUGGLER_DROPOFF_CONTESTED_30
			sDetails.vInCarCorona = <<-494.2820, 5289.9458, 79.6100>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-512.150635,5246.959473,74.319946>>
			sDetails.vAirDrop2 = <<-486.862671,5314.745605,166.480286>>
			sDetails.fAirDropAreaWidth = 35.0
		BREAK
	ENDSWITCH
ENDPROC

PROC SMUGGLER_GET_AGILE_DELIVERY_DROPOFF_COORDS(FREEMODE_DELIVERY_DROPOFFS eDropoffID, FREEMODE_DELIVERY_DROPOFF_DETAILS &sDetails)
	
	SET_BIT(sDetails.iBS, FREEMODE_DELIVERY_DROPOFF_DETAILS_BS_DROPOFF_HAS_DROP_ZONE)
	SET_BIT(sDetails.iBS, FREEMODE_DELIVERY_DROPOFF_DETAILS_BS_IGNORE_PRIMARY_DROPOFF)
	
	SWITCH eDropoffID
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_1
			sDetails.vInCarCorona = <<930.9380, -2985.8650, 4.9020>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<930.905090,-2985.834473,7.117765>>
			sDetails.vAirDrop2 = <<6.000000,6.000000,6.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_2
			sDetails.vInCarCorona = <<686.2710, 577.6860, 129.4620>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<683.853210,571.334412,126.461395>>	
			sDetails.vAirDrop2 = <<687.778076,581.945068,138.461395>>
			sDetails.fAirDropAreaWidth = 26.0
			SET_BIT(sDetails.iBS, FREEMODE_DELIVERY_DROPOFF_DETAILS_BS_DROPOFF_DROPZONE_USES_AA)
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_3
			sDetails.vInCarCorona = <<-383.9390, -2672.4661, 5.0010>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-375.713745,-2665.312988,2.000296>>	
			sDetails.vAirDrop2 = <<-389.192413,-2679.025146,16.000217>>
			sDetails.fAirDropAreaWidth = 14.00
			SET_BIT(sDetails.iBS, FREEMODE_DELIVERY_DROPOFF_DETAILS_BS_DROPOFF_DROPZONE_USES_AA)
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_4
			sDetails.vInCarCorona = <<-529.0860, -1684.2220, 18.1620>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-529.187195,-1684.504150,24.377766>>
			sDetails.vAirDrop2 = <<10.000000,10.000000,10.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_5
			sDetails.vInCarCorona = <<-971.1970, -1305.3710, 19.8050>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-971.146973,-1305.277466,26.023878>>
			sDetails.vAirDrop2 = <<10.000000,10.000000,10.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_6
			sDetails.vInCarCorona = <<-1479.1200, -663.1780, 27.9440>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-1479.092896,-663.180908,30.159763>>
			sDetails.vAirDrop2 = <<8.000000,8.000000,6.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_7
			sDetails.vInCarCorona = <<-606.7500, -127.3000, 38.0090>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-606.718567,-127.388260,42.224766>>
			sDetails.vAirDrop2 = <<10.000000,10.000000,8.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_8
			sDetails.vInCarCorona = <<-489.1280, 187.4300, 82.1630>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-489.080536,188.414566,84.662712>>
			sDetails.vAirDrop2 = <<9.000000,9.000000,7.500000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_9
			sDetails.vInCarCorona = <<-121.6420, -1000.8410, 53.2640>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-121.596199,-1000.927673,59.479767>>
			sDetails.vAirDrop2 = <<10.000000,10.000000,10.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_10
			sDetails.vInCarCorona = <<-477.0120, -1048.3000, 28.1110>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-476.203369,-1048.321289,33.150620>>
			sDetails.vAirDrop2 = <<8.000000,8.000000,6.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_11
			sDetails.vInCarCorona = <<-2210.6140, 201.9900, 173.6020>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-2210.535156,202.004028,177.817764>>
			sDetails.vAirDrop2 = <<8.000000,6.000000,6.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_12
			sDetails.vInCarCorona = <<-3266.0010, 955.4270, 7.3530>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-3265.769287,956.671448,8.852164>>
			sDetails.vAirDrop2 = <<4.000000,5.000000,4.500000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_13
			sDetails.vInCarCorona = <<203.2230, 1198.4120, 230.2910>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<204.198380,1198.679932,232.290359>>
			sDetails.vAirDrop2 = <<6.000000,5.000000,5.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_14
			sDetails.vInCarCorona = <<1129.0310, -2074.7539, 30.0090>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1128.883423,-2074.139404,32.112324>>
			sDetails.vAirDrop2 = <<8.000000,10.000000,5.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_15
			sDetails.vInCarCorona = <<710.0510, -1221.8149, 23.6620>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<709.998413,-1221.837646,31.877766>>
			sDetails.vAirDrop2 = <<12.000000,12.000000,10.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_16
			sDetails.vInCarCorona = <<-773.2300, -1044.8290, 22.1030>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-772.990540,-1045.432861,25.032133>>
			sDetails.vAirDrop2 = <<8.000000,8.000000,5.500000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_17
			sDetails.vInCarCorona = <<-945.8510, -1422.6910, 6.6800>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-945.865356,-1422.654541,9.895765>>
			sDetails.vAirDrop2 = <<10.000000,10.000000,5.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_18
			sDetails.vInCarCorona = <<-1680.9600, -1144.2340, 12.0180>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-1680.959106,-1144.147583,16.233765>>
			sDetails.vAirDrop2 = <<10.000000,10.000000,6.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_19
			sDetails.vInCarCorona = <<-1500.0220, -333.2210, 44.9000>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-1500.079956,-333.299896,49.115765>>
			sDetails.vAirDrop2 = <<10.000000,10.000000,6.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_20
			sDetails.vInCarCorona = <<-1860.2419, 222.5270, 83.2940>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-1861.147339,221.583557,84.885529>>
			sDetails.vAirDrop2 = <<10.000000,10.000000,4.500000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_21
			sDetails.vInCarCorona = <<413.1160, -29.2350, 113.5450>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<413.668213,-29.542959,120.630730>>
			sDetails.vAirDrop2 = <<12.000000,10.000000,10.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_22
			sDetails.vInCarCorona = <<34.8300, -383.4500, 63.8060>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<35.541451,-383.324829,67.845612>>
			sDetails.vAirDrop2 = <<7.500000,7.500000,5.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_23
			sDetails.vInCarCorona = <<255.4160, -710.3540, 43.6860>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<255.453323,-710.272949,53.901764>>
			sDetails.vAirDrop2 = <<10.000000,10.000000,10.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_24
			sDetails.vInCarCorona = <<592.1800, -507.2940, 23.7490>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<592.288330,-507.121307,27.964766>>
			sDetails.vAirDrop2 = <<10.000000,10.000000,6.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_25
			sDetails.vInCarCorona = <<1076.1320, -685.8560, 56.6440>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1076.657349,-686.303101,60.683617>>
			sDetails.vAirDrop2 = <<8.000000,8.000000,7.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_26
			sDetails.vInCarCorona = <<471.3170, -2645.5120, 4.7120>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<471.191376,-2645.628418,7.927763>>
			sDetails.vAirDrop2 = <<8.000000,8.000000,5.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_27
			sDetails.vInCarCorona = <<-129.3160, -2223.4009, 6.8120>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-129.375198,-2223.396729,11.027765>>
			sDetails.vAirDrop2 = <<10.000000,10.000000,6.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_28
			sDetails.vInCarCorona = <<-89.9780, -1619.2760, 28.5610>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-90.022072,-1619.193604,31.776768>>
			sDetails.vAirDrop2 = <<10.000000,10.000000,5.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_29
			sDetails.vInCarCorona = <<276.0080, -1094.4650, 51.5770>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<276.078491,-1094.376709,59.792763>>
			sDetails.vAirDrop2 = <<10.000000,10.000000,10.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_30
			sDetails.vInCarCorona = <<-549.7460, -790.4590, 28.3270>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-549.791016,-790.437866,34.542763>>
			sDetails.vAirDrop2 = <<10.000000,10.000000,8.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK		
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_31
			sDetails.vInCarCorona = <<-822.6800, -595.0520, 36.1970>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-823.320618,-595.473206,41.196659>>
			sDetails.vAirDrop2 = <<8.000000,8.000000,8.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_32
			sDetails.vInCarCorona = <<-1220.7620, -498.0940, 30.3000>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-1220.794922,-498.237244,35.508194>>
			sDetails.vAirDrop2 = <<8.000000,8.000000,7.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_33
			sDetails.vInCarCorona = <<-178.6720, 53.3820, 66.7020>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-177.878098,53.518211,69.700592>>
			sDetails.vAirDrop2 = <<10.000000,10.000000,6.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_34
			sDetails.vInCarCorona = <<852.8610, -1902.5510, 36.0900>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<852.877197,-1902.477783,42.805763>>
			sDetails.vAirDrop2 = <<10.000000,10.000000,8.500000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_35
			sDetails.vInCarCorona = <<-510.3530, -2030.1730, 11.4330>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-510.260162,-2029.906250,17.694872>>
			sDetails.vAirDrop2 = <<6.000000,6.000000,8.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_36
			sDetails.vInCarCorona = <<62.6580, -814.9160, 50.8860>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<62.394981,-815.689026,57.885918>>
			sDetails.vAirDrop2 = <<8.000000,8.000000,10.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_37
			sDetails.vInCarCorona = <<-325.8310, -595.9410, 32.5590>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-325.952972,-595.974487,37.774761>>
			sDetails.vAirDrop2 = <<8.000000,8.000000,7.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_38
			sDetails.vInCarCorona = <<1083.0710, -231.4080, 56.4150>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1078.683228,-239.198242,53.728657>>
			sDetails.vAirDrop2 = <<1089.179321,-221.789780,67.012024>>
			sDetails.fAirDropAreaWidth = 11.0
			SET_BIT(sDetails.iBS, FREEMODE_DELIVERY_DROPOFF_DETAILS_BS_DROPOFF_DROPZONE_USES_AA)
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_39
			sDetails.vInCarCorona = <<210.4130, -3323.2620, 4.7940>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<210.314850,-3323.419922,9.009765>>
			sDetails.vAirDrop2 = <<10.000000,10.000000,6.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_40
			sDetails.vInCarCorona = <<-1275.7791, 140.1900, 57.3300>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-1275.382202,139.881470,61.545769>>
			sDetails.vAirDrop2 = <<6.000000,6.000000,6.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_41
			sDetails.vInCarCorona = <<1306.2595, -1114.4169, 38.5697>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1306.245605,-1114.322266,42.785500>>
			sDetails.vAirDrop2 = <<8.000000,8.000000,6.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_42
			sDetails.vInCarCorona = <<1430.6550, -965.0710, 46.7990>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1431.873169,-967.040527,49.820496>>
			sDetails.vAirDrop2 = <<8.000000,8.000000,6.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_43
			sDetails.vInCarCorona = <<939.3190, -2368.1001, 29.5320>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<939.369995,-2368.138428,32.247765>>
			sDetails.vAirDrop2 = <<10.000000,10.000000,4.500000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_44
			sDetails.vInCarCorona = <<-466.4220, -902.7170, 37.6890>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-464.989563,-901.940247,41.188736>>
			sDetails.vAirDrop2 = <<6.000000,6.000000,4.500000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_45
			sDetails.vInCarCorona = <<-3106.2991, 1226.4500, 9.7020>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-3106.248291,1226.497437,14.417765>>
			sDetails.vAirDrop2 = <<10.000000,10.000000,4.500000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_46
			sDetails.vInCarCorona = <<593.0290, -2094.0359, 4.7530>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<592.520386,-2093.163574,7.952190>>
			sDetails.vAirDrop2 = <<6.000000,9.000000,4.200000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_47
			sDetails.vInCarCorona = <<-153.3100, -1098.8979, 12.1180>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-153.379349,-1098.834106,22.333763>>
			sDetails.vAirDrop2 = <<7.000000,7.000000,10.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_48
			sDetails.vInCarCorona = <<-45.0454, -343.4405, 42.8071>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-45.014374,-343.361420,49.022865>>
			sDetails.vAirDrop2 = <<6.000000,6.000000,6.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_49
			sDetails.vInCarCorona = <<-891.3830, -1259.0710, 19.8750>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-891.298889,-1258.869263,28.090763>>
			sDetails.vAirDrop2 = <<10.000000,10.000000,10.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_50
			sDetails.vInCarCorona = <<-423.7030, -330.0120, 41.2220>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-423.745972,-330.020050,51.371574>>
			sDetails.vAirDrop2 = <<10.000000,10.000000,10.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_51
			sDetails.vInCarCorona = <<730.9880, -2650.4761, 5.1780>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<731.440186,-2650.377441,7.383765>>
			sDetails.vAirDrop2 = <<8.000000,8.000000,4.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_52
			sDetails.vInCarCorona = <<900.1840, -1436.4540, 11.1740>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<899.598206,-1435.845337,18.160830>>
			sDetails.vAirDrop2 = <<6.000000,6.000000,10.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_53
			sDetails.vInCarCorona = <<-136.2430, -943.8270, 113.2520>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-136.915771,-946.368469,116.136627>>
			sDetails.vAirDrop2 = <<6.000000,6.000000,6.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_54
			sDetails.vInCarCorona = <<-1468.5730, -176.1020, 47.8200>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-1468.537842,-176.300049,53.035763>>
			sDetails.vAirDrop2 = <<10.000000,10.000000,7.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_55
			sDetails.vInCarCorona = <<472.4260, -850.5490, 25.3150>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<472.554535,-850.252502,28.511568>>
			sDetails.vAirDrop2 = <<10.000000,10.000000,5.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_56
			sDetails.vInCarCorona = <<692.8470, -1009.2010, 21.7740>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<692.744202,-1009.965820,23.760040>>
			sDetails.vAirDrop2 = <<9.000000,9.000000,5.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_57
			sDetails.vInCarCorona = <<2523.0300, -191.9970, 74.3130>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<2523.233887,-191.922058,79.001686>>
			sDetails.vAirDrop2 = <<6.000000,6.000000,6.500000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_58
			sDetails.vInCarCorona = <<837.8650, -844.4490, 25.3360>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<837.762390,-844.503723,31.551765>>
			sDetails.vAirDrop2 = <<10.000000,10.000000,8.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_59
			sDetails.vInCarCorona = <<-119.0040, -1470.7290, 32.8220>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-118.999008,-1470.700806,34.925766>>
			sDetails.vAirDrop2 = <<10.000000,10.000000,4.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AGILE_DELIVERY_60
			sDetails.vInCarCorona = <<10.9498, 57.9502, 70.8541>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<10.989043,58.020485,78.863525>>
			sDetails.vAirDrop2 = <<10.000000,10.000000,10.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
	ENDSWITCH
ENDPROC

PROC SMUGGLER_GET_PRECISION_DELIVERY_DROPOFF_COORDS(FREEMODE_DELIVERY_DROPOFFS eDropoffID, FREEMODE_DELIVERY_DROPOFF_DETAILS &sDetails)
	
	SET_BIT(sDetails.iBS, FREEMODE_DELIVERY_DROPOFF_DETAILS_BS_DROPOFF_HAS_DROP_ZONE)
	
	SWITCH eDropoffID
		//LSIA Hangars
		CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_1
			sDetails.vInCarCorona = <<1349.4340, -2211.2009, 59.1850>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1349.4340, -2211.2009, 59.1850>>
			sDetails.vAirDrop2 = <<10.0, 10.0, 20.0>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_2
			sDetails.vInCarCorona = <<244.6660, -3086.7620, 4.6340>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<244.6660, -3086.7620, 4.6340>>
			sDetails.vAirDrop2 = <<10.0, 10.0, 20.0>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_3
			sDetails.vInCarCorona = <<374.0230, -1845.6420, 26.7070>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<374.0230, -1845.6420, 26.7070>>
			sDetails.vAirDrop2 = <<10.0, 10.0, 20.0>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_4
			sDetails.vInCarCorona = <<2566.5339, -678.9420, 53.1690>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<2566.5339, -678.9420, 53.1690>>
			sDetails.vAirDrop2 = <<10.0, 10.0, 20.0>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_5
			sDetails.vInCarCorona = <<1147.3770, -1479.2490, 33.6850>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1147.3770, -1479.2490, 33.6850>>
			sDetails.vAirDrop2 = <<10.0, 10.0, 20.0>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_6
			sDetails.vInCarCorona = <<-1204.0870, -1340.3330, 3.7260>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-1204.0870, -1340.3330, 3.7260>>
			sDetails.vAirDrop2 = <<10.0, 10.0, 20.0>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_7
			sDetails.vInCarCorona = <<736.9614, -328.0111, 49.2329>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<736.9614, -328.0111, 49.2329>>
			sDetails.vAirDrop2 = <<10.0, 10.0, 20.0>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_8
			sDetails.vInCarCorona = <<1025.3444, 955.4961, 221.1720>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1025.3444, 955.4961, 221.1720>>
			sDetails.vAirDrop2 = <<10.0, 10.0, 20.0>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_9
			sDetails.vInCarCorona = <<298.8370, 16.6260, 82.0750>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<298.8370, 16.6260, 82.0750>>
			sDetails.vAirDrop2 = <<10.0, 10.0, 20.0>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_10
			sDetails.vInCarCorona = <<-301.3760, -1106.5800, 22.0260>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-301.3760, -1106.5800, 22.0260>>
			sDetails.vAirDrop2 = <<10.0, 10.0, 20.0>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_11
			sDetails.vInCarCorona = <<1321.4170, -725.5130, 64.5790>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1321.4170, -725.5130, 64.5790>>
			sDetails.vAirDrop2 = <<10.0, 10.0, 20.0>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_12
			sDetails.vInCarCorona = <<2611.8259, 469.9974, 104.6810>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<2611.8259, 469.9974, 104.6810>>
			sDetails.vAirDrop2 = <<10.0, 10.0, 20.0>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_13
			sDetails.vInCarCorona = <<-808.0940, -396.2500, 36.0310>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-808.0940, -396.2500, 36.0310>>
			sDetails.vAirDrop2 = <<10.0, 10.0, 20.0>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_14
			sDetails.vInCarCorona = <<150.6398, 758.6245, 208.3275>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<150.6398, 758.6245, 208.3275>>
			sDetails.vAirDrop2 = <<10.0, 10.0, 20.0>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_15
			sDetails.vInCarCorona = <<-1619.5630, 391.3410, 85.6970>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-1619.5630, 391.3410, 85.6970>>
			sDetails.vAirDrop2 = <<10.0, 10.0, 20.0>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		
		//Zancudo Hangars
		CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_16
			sDetails.vInCarCorona = <<1756.9448, 6395.2021, 35.4139>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1756.9448, 6395.2021, 35.4139>>
			sDetails.vAirDrop2 = <<10.0, 10.0, 20.0>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_17
			sDetails.vInCarCorona = <<56.7549, 6521.5981, 30.4565>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<56.7549, 6521.5981, 30.4565>>
			sDetails.vAirDrop2 = <<10.0, 10.0, 20.0>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_18
			sDetails.vInCarCorona = <<-596.0260, 5753.0562, 34.0110>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-596.0260, 5753.0562, 34.0110>>
			sDetails.vAirDrop2 = <<10.0, 10.0, 20.0>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_19
			sDetails.vInCarCorona = <<-2259.7710, 4341.0762, 42.8490>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-2259.7710, 4341.0762, 42.8490>>
			sDetails.vAirDrop2 = <<10.0, 10.0, 20.0>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_20
			sDetails.vInCarCorona = <<135.1260, 4447.8882, 79.1670>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<135.1260, 4447.8882, 79.1670>>
			sDetails.vAirDrop2 = <<10.0, 10.0, 20.0>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_21
			sDetails.vInCarCorona = <<1440.9668, 4470.1299, 48.4830>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1440.9668, 4470.1299, 48.4830>>
			sDetails.vAirDrop2 = <<10.0, 10.0, 20.0>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_22
			sDetails.vInCarCorona = <<1963.2120, 5083.1138, 39.6153>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1963.2120, 5083.1138, 39.6153>>
			sDetails.vAirDrop2 = <<10.0, 10.0, 20.0>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_23
			sDetails.vInCarCorona = <<2679.7600, 4341.2632, 44.7960>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<2679.7600, 4341.2632, 44.7960>>
			sDetails.vAirDrop2 = <<10.0, 10.0, 20.0>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_24
			sDetails.vInCarCorona = <<2183.0061, 3399.3420, 44.5270>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<2183.0061, 3399.3420, 44.5270>>
			sDetails.vAirDrop2 = <<10.0, 10.0, 20.0>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_25
			sDetails.vInCarCorona = <<1472.3240, 3658.7549, 33.2850>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1472.3240, 3658.7549, 33.2850>>
			sDetails.vAirDrop2 = <<10.0, 10.0, 20.0>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_26
			sDetails.vInCarCorona = <<1275.3380, 2629.9661, 36.7080>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1275.3380, 2629.9661, 36.7080>>
			sDetails.vAirDrop2 = <<10.0, 10.0, 20.0>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_27
			sDetails.vInCarCorona = <<170.0100, 3214.7490, 41.2990>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<170.0100, 3214.7490, 41.2990>>
			sDetails.vAirDrop2 = <<10.0, 10.0, 20.0>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_28
			sDetails.vInCarCorona = <<-1700.2831, 2371.0303, 48.2861>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-1700.2831, 2371.0303, 48.2861>>
			sDetails.vAirDrop2 = <<10.0, 10.0, 20.0>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_29
			sDetails.vInCarCorona = <<-281.7410, 1958.1182, 162.3620>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-281.7410, 1958.1182, 162.3620>>
			sDetails.vAirDrop2 = <<10.0, 10.0, 20.0>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_PRECISION_DELIVERY_30
			sDetails.vInCarCorona = <<2513.3630, 1969.4479, 18.8880>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<2513.3630, 1969.4479, 18.8880>>
			sDetails.vAirDrop2 = <<10.0, 10.0, 20.0>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
	ENDSWITCH
ENDPROC

PROC SMUGGLER_GET_FLYING_FORTRESS_DROPOFF_COORDS(FREEMODE_DELIVERY_DROPOFFS eDropoffID, FREEMODE_DELIVERY_DROPOFF_DETAILS &sDetails)
	
	SET_BIT(sDetails.iBS, FREEMODE_DELIVERY_DROPOFF_DETAILS_BS_DROPOFF_HAS_DROP_ZONE)
	
	SWITCH eDropoffID
		//LSIA hangar dropoffs
		CASE SMUGGLER_DROPOFF_FLYING_FORTRESS_1
			sDetails.vInCarCorona = <<1628.2531, -1885.0110, 104.2440>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1628.258545,-1885.123535,249.235855>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLYING_FORTRESS_2
			sDetails.vInCarCorona = <<2663.6990, 1395.7440, 23.4610>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<2663.730469,1395.770386,168.464661>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLYING_FORTRESS_3
			sDetails.vInCarCorona = <<-681.0220, 5798.6689, 16.3310>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-680.947876,5798.406738,161.330963>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLYING_FORTRESS_4
			sDetails.vInCarCorona = <<-1046.6750, -997.8320, 8.6180>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-1046.516235,-997.762329,153.618271>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLYING_FORTRESS_5
			sDetails.vInCarCorona = <<2361.1931, 2563.8701, 50.6530>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<2361.377197,2563.769775,195.650421>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLYING_FORTRESS_6
			sDetails.vInCarCorona = <<2228.4390, 5595.1099, 53.0470>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<2228.511475,5594.972656,198.043091>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLYING_FORTRESS_7
			sDetails.vInCarCorona = <<1041.2930, -2980.3831, 4.9010>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1041.384766,-2980.493408,149.901047>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLYING_FORTRESS_8
			sDetails.vInCarCorona = <<-2975.9529, 728.3860, 28.3400>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-2975.979980,728.330933,173.339920>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLYING_FORTRESS_9
			sDetails.vInCarCorona = <<1564.0930, 3572.5869, 32.7450>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1564.143433,3572.575928,177.740128>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLYING_FORTRESS_10
			sDetails.vInCarCorona = <<-1376.6219, -1398.9351, 4.6280>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-1376.794556,-1398.847412,150.843765>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLYING_FORTRESS_11
			sDetails.vInCarCorona = <<-1833.1130, 2154.4641, 114.6460>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-1833.026489,2154.511475,260.861755>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLYING_FORTRESS_12
			sDetails.vInCarCorona = <<1730.5720, 4856.5381, 39.6280>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1730.669678,4856.528320,185.842163>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLYING_FORTRESS_13
			sDetails.vInCarCorona = <<418.1920, -1331.3220, 45.0540>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<418.205170,-1331.247681,191.269775>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLYING_FORTRESS_14
			sDetails.vInCarCorona = <<248.5220, 3107.3359, 44.9240>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<248.510956,3107.331543,190.933395>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLYING_FORTRESS_15
			sDetails.vInCarCorona = <<3294.0759, 5188.6792, 17.4160>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<3294.169678,5188.586914,163.425400>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		
		//Zancudo Hangars
		CASE SMUGGLER_DROPOFF_FLYING_FORTRESS_16
			sDetails.vInCarCorona = <<-987.0500, 4397.2832, 14.9450>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-986.881470,4396.998047,159.871201>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLYING_FORTRESS_17
			sDetails.vInCarCorona = <<2702.9939, 3503.4661, 60.5260>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<2703.048096,3503.432861,205.525787>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLYING_FORTRESS_18
			sDetails.vInCarCorona = <<985.2860, -2244.7500, 29.5570>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<985.302307,-2244.743652,174.556808>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLYING_FORTRESS_19
			sDetails.vInCarCorona = <<2257.6489, 5157.1992, 56.8250>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<2257.497559,5157.227539,201.831787>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLYING_FORTRESS_20
			sDetails.vInCarCorona = <<-1057.0811, 834.8380, 165.7290>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-1057.117310,834.900085,310.729340>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLYING_FORTRESS_21
			sDetails.vInCarCorona = <<473.8190, -2589.0120, 13.4610>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<473.679962,-2588.863525,158.461044>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLYING_FORTRESS_22
			sDetails.vInCarCorona = <<69.5550, 6526.3862, 30.5760>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<69.583099,6526.562988,176.791763>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLYING_FORTRESS_23
			sDetails.vInCarCorona = <<1390.8920, 2172.0391, 100.5320>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1390.970337,2171.999268,246.747772>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLYING_FORTRESS_24
			sDetails.vInCarCorona = <<-238.2900, -1530.3070, 30.4860>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-238.336502,-1530.406494,176.701767>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLYING_FORTRESS_25
			sDetails.vInCarCorona = <<1350.8580, 6372.6050, 32.2100>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1350.847290,6372.572266,178.425766>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLYING_FORTRESS_26
			sDetails.vInCarCorona = <<-289.7990, 2526.0610, 73.6130>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-289.818787,2526.114990,219.828766>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLYING_FORTRESS_27
			sDetails.vInCarCorona = <<384.7900, -1798.9080, 32.0150>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<384.762421,-1798.774414,178.230774>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLYING_FORTRESS_28
			sDetails.vInCarCorona = <<1300.6350, 6611.2769, 1.2150>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1300.758301,6611.331543,147.430756>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLYING_FORTRESS_29
			sDetails.vInCarCorona = <<2688.6980, 2878.0000, 35.0410>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<2688.731445,2878.050537,181.256775>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLYING_FORTRESS_30
			sDetails.vInCarCorona = <<-482.3020, -428.9430, 56.2400>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-482.304688,-428.811554,202.455765>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
	ENDSWITCH
ENDPROC

PROC SMUGGLER_GET_FLY_LOW_DROPOFF_COORDS(FREEMODE_DELIVERY_DROPOFFS eDropoffID, FREEMODE_DELIVERY_DROPOFF_DETAILS &sDetails)
	
	SET_BIT(sDetails.iBS, FREEMODE_DELIVERY_DROPOFF_DETAILS_BS_DROPOFF_HAS_DROP_ZONE)
	SET_BIT(sDetails.iBS, FREEMODE_DELIVERY_DROPOFF_DETAILS_BS_AIR_DROP_IGNORE_DPAD_RIGHT)
	
	SWITCH eDropoffID				
		CASE SMUGGLER_DROPOFF_FLY_LOW_1
			sDetails.vInCarCorona = <<-228.6900, -2445.7000, 19.0000>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-228.448502,-2445.367188,16.001398>>
			sDetails.vAirDrop2 = <<25.000000,25.000000,11.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLY_LOW_2
			sDetails.vInCarCorona = <<617.2860, -840.5870, 24.0720>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<617.347412,-840.502686,21.929361>>
			sDetails.vAirDrop2 = <<25.000000,25.000000,12.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLY_LOW_3
			sDetails.vInCarCorona = <<2908.7280, 770.1070, 35.1210>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<2908.174561,769.732422,33.864594>>
			sDetails.vAirDrop2 = <<25.000000,25.000000,13.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLY_LOW_4
			sDetails.vInCarCorona = <<-180.0580, 2863.6230, 37.5280>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-180.133438,2863.537109,37.456291>>
			sDetails.vAirDrop2 = <<25.000000,25.000000,7.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLY_LOW_5
			sDetails.vInCarCorona = <<-1414.1630, 2636.9509, 8.0050>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-1413.618164,2636.493164,7.268405>>
			sDetails.vAirDrop2 = <<25.000000,25.000000,8.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLY_LOW_6
			sDetails.vInCarCorona = <<-2665.4509, 2595.6960, 6.0080>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-2665.379883,2595.290771,5.917889>>
			sDetails.vAirDrop2 = <<25.000000,25.000000,7.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLY_LOW_7
			sDetails.vInCarCorona = <<-1955.1639, 4579.2632, 16.2120>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-1954.962769,4579.154297,13.939919>>
			sDetails.vAirDrop2 = <<25.000000,25.000000,12.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLY_LOW_8
			sDetails.vInCarCorona = <<-509.5570, 4421.5239, 44.4000>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-510.915405,4421.008789,39.482529>>
			sDetails.vAirDrop2 = <<25.000000,25.000000,13.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLY_LOW_9
			sDetails.vInCarCorona = <<806.0340, -2635.4651, 15.2930>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<806.775574,-2636.127441,4.788301>>
			sDetails.vAirDrop2 = <<25.000000,25.000000,19.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLY_LOW_10
			sDetails.vInCarCorona = <<2265.8540, 2036.0170, 142.1970>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<2265.952148,2036.054321,139.336029>>
			sDetails.vAirDrop2 = <<25.000000,25.000000,11.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLY_LOW_11
			sDetails.vInCarCorona = <<1659.9500, -1638.8650, 125.2830>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1659.895996,-1638.868286,122.252693>>
			sDetails.vAirDrop2 = <<25.000000,25.000000,11.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLY_LOW_12
			sDetails.vInCarCorona = <<-1850.9430, -334.0370, 70.2370>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-1850.938110,-334.012024,67.087456>>
			sDetails.vAirDrop2 = <<25.000000,25.000000,11.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLY_LOW_13
			sDetails.vInCarCorona = <<1976.2159, 717.2310, 178.6130>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1976.275269,717.414917,175.297180>>
			sDetails.vAirDrop2 = <<25.000000,25.000000,11.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLY_LOW_14
			sDetails.vInCarCorona = <<-24.0430, 668.6900, 211.0030>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-23.553850,668.961426,202.526581>>
			sDetails.vAirDrop2 = <<25.000000,25.000000,18.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLY_LOW_15
			sDetails.vInCarCorona = <<-211.0010, -1800.3180, 14.0440>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-210.773788,-1800.151611,11.554911>>
			sDetails.vAirDrop2 = <<25.000000,25.000000,12.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLY_LOW_16
			sDetails.vInCarCorona = <<321.8560, 2214.7610, 87.7700>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<321.913910,2214.874023,85.225891>>
			sDetails.vAirDrop2 = <<25.000000,25.000000,11.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLY_LOW_17
			sDetails.vInCarCorona = <<1973.5140, 5042.1040, 51.5050>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1973.452515,5042.029297,49.965775>>
			sDetails.vAirDrop2 = <<25.000000,25.000000,10.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLY_LOW_18
			sDetails.vInCarCorona = <<2025.1320, 2941.1790, 56.5550>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<2025.185547,2941.274902,55.597359>>
			sDetails.vAirDrop2 = <<25.000000,25.000000,9.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLY_LOW_19
			sDetails.vInCarCorona = <<77.0010, 7075.8940, 13.2500>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<76.955872,7075.935547,10.972376>>
			sDetails.vAirDrop2 = <<25.000000,25.000000,10.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLY_LOW_20
			sDetails.vInCarCorona = <<3857.7170, 4362.6729, 17.5030>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<3857.716309,4362.641113,15.640448>>
			sDetails.vAirDrop2 = <<25.000000,25.000000,10.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLY_LOW_21
			sDetails.vInCarCorona = <<2920.7510, 5314.0430, 109.4040>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<2920.730225,5314.069336,106.134117>>
			sDetails.vAirDrop2 = <<25.000000,25.000000,11.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLY_LOW_22
			sDetails.vInCarCorona = <<201.4460, -725.1520, 60.0020>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<201.354614,-725.228882,57.076965>>
			sDetails.vAirDrop2 = <<25.000000,25.000000,11.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLY_LOW_23
			sDetails.vInCarCorona = <<714.2620, 4115.0088, 45.0060>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<714.308044,4115.062500,43.814262>>
			sDetails.vAirDrop2 = <<25.000000,25.000000,9.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLY_LOW_24
			sDetails.vInCarCorona = <<1090.1730, 61.7330, 92.0590>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1090.208008,61.795120,89.890907>>
			sDetails.vAirDrop2 = <<25.000000,25.000000,10.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLY_LOW_25
			sDetails.vInCarCorona = <<-2823.0959, 1423.1290, 109.9820>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-2823.114014,1423.143066,108.808380>>
			sDetails.vAirDrop2 = <<25.000000,25.000000,9.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLY_LOW_26
			sDetails.vInCarCorona = <<-1373.5530, 5352.6948, 14.9570>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-1373.512085,5352.808594,13.162935>>
			sDetails.vAirDrop2 = <<25.000000,25.000000,11.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLY_LOW_27
			sDetails.vInCarCorona = <<-1350.4630, -1434.6429, 17.1180>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-1350.429565,-1434.646851,14.324188>>
			sDetails.vAirDrop2 = <<25.000000,25.000000,11.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLY_LOW_28
			sDetails.vInCarCorona = <<2471.1260, 3763.6680, 55.0250>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<2471.111084,3763.622559,52.047554>>
			sDetails.vAirDrop2 = <<25.000000,25.000000,11.500000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLY_LOW_29
			sDetails.vInCarCorona = <<-967.1260, -974.1660, 15.0020>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-967.161560,-974.150696,12.865185>>
			sDetails.vAirDrop2 = <<25.000000,25.000000,10.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLY_LOW_30
			sDetails.vInCarCorona = <<626.7140, 623.5640, 142.1660>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<626.707520,623.609680,138.911011>>
			sDetails.vAirDrop2 = <<25.000000,25.000000,11.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLY_LOW_31
			sDetails.vInCarCorona = <<-1038.2061, 929.8860, 182.0090>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-1038.277100,929.907532,178.716919>>
			sDetails.vAirDrop2 = <<25.000000,25.000000,11.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLY_LOW_32
			sDetails.vInCarCorona = <<-1186.1230, 38.3090, 65.0000>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-1186.142822,38.233978,62.726677>>
			sDetails.vAirDrop2 = <<25.000000,25.000000,11.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_FLY_LOW_33
			sDetails.vInCarCorona = <<1441.2111, 6346.6021, 37.0180>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1441.136963,6346.372559,33.954910>>
			sDetails.vAirDrop2 = <<25.000000,25.000000,11.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
	ENDSWITCH
ENDPROC

PROC SMUGGLER_GET_AIR_DELIVERY_DROPOFF_COORDS(FREEMODE_DELIVERY_DROPOFFS eDropoffID, FREEMODE_DELIVERY_DROPOFF_DETAILS &sDetails)	
	
	SET_BIT(sDetails.iBS, FREEMODE_DELIVERY_DROPOFF_DETAILS_BS_DROPOFF_HAS_DROP_ZONE)
	
	SWITCH eDropoffID				
		CASE SMUGGLER_DROPOFF_AIR_DELIVERY_1
			sDetails.vInCarCorona = <<-1410.1300, -993.2890, 18.3800>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-1410.102173,-993.234497,168.380463>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_DELIVERY_2
			sDetails.vInCarCorona = <<-71.1690, 807.4150, 226.2500>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-71.134308,807.328735,376.249756>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_DELIVERY_3
			sDetails.vInCarCorona = <<-163.2980, -160.2120, 92.7020>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-163.231934,-160.041382,242.702408>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_DELIVERY_4
			sDetails.vInCarCorona = <<78.7310, -1587.6350, 37.5590>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<78.705742,-1587.401123,187.558716>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_DELIVERY_5
			sDetails.vInCarCorona = <<1145.4200, -431.0920, 66.1690>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1145.483032,-431.072418,216.164413>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_DELIVERY_6
			sDetails.vInCarCorona = <<-293.6180, 2798.6001, 58.4760>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-293.561523,2798.717773,208.459991>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_DELIVERY_7
			sDetails.vInCarCorona = <<-2233.7859, 222.9360, 193.6120>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-2234.007813,223.109818,343.611694>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_DELIVERY_8
			sDetails.vInCarCorona = <<-211.0030, 6270.1650, 30.4890>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-211.302353,6270.386230,180.489349>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_DELIVERY_9
			sDetails.vInCarCorona = <<501.3060, 5597.5840, 794.9210>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<501.070160,5597.814941,944.908813>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_DELIVERY_10
			sDetails.vInCarCorona = <<2559.6570, 4288.5630, 40.5900>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<2559.550781,4288.573242,190.590332>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_DELIVERY_11
			sDetails.vInCarCorona = <<2061.7871, 1876.1570, 92.1470>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<2061.820801,1876.255249,242.096069>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_DELIVERY_12
			sDetails.vInCarCorona = <<1439.3979, -2123.2500, 55.7580>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1439.464233,-2123.064453,205.738571>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_DELIVERY_13
			sDetails.vInCarCorona = <<536.5550, -2817.6960, 5.0420>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<536.701599,-2817.774902,155.042084>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_DELIVERY_14
			sDetails.vInCarCorona = <<1414.7090, 3812.3960, 31.2850>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1414.680542,3812.424316,181.285843>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_DELIVERY_15
			sDetails.vInCarCorona = <<-1224.8710, 4451.7178, 29.1230>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-1224.648926,4451.726563,179.127106>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_DELIVERY_16
			sDetails.vInCarCorona = <<1942.5699, 4992.0752, 41.6220>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1942.686035,4992.245117,191.604996>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_DELIVERY_17
			sDetails.vInCarCorona = <<-413.8390, 1570.6490, 354.0090>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-413.764160,1570.748535,504.001068>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_DELIVERY_18
			sDetails.vInCarCorona = <<-338.9770, -738.6340, 52.2470>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-338.825653,-739.076721,202.246704>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_DELIVERY_19
			sDetails.vInCarCorona = <<2785.3391, -671.2020, 3.1290>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<2785.372314,-671.132874,153.117645>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_DELIVERY_20
			sDetails.vInCarCorona = <<1597.8530, 6574.4302, 13.0570>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1597.943115,6574.681641,163.095612>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_DELIVERY_21
			sDetails.vInCarCorona = <<-1788.9530, 5296.7808, 0.9370>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-1794.736572,5302.018066,156.046326>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_DELIVERY_22
			sDetails.vInCarCorona = <<107.9290, 7556.9321, 0.3750>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<102.067551,7561.386719,154.287079>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_DELIVERY_23
			sDetails.vInCarCorona = <<4348.7002, 4204.6479, 0.3750>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<4356.095703,4211.247070,155.399277>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_DELIVERY_24
			sDetails.vInCarCorona = <<3350.9141, 1271.1060, -0.3750>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<3344.999023,1277.366455,155.482056>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_DELIVERY_25
			sDetails.vInCarCorona = <<25.1820, -3666.9419, 1.3120>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<32.641727,-3669.944336,155.938644>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_DELIVERY_26
			sDetails.vInCarCorona = <<-3462.2229, 1513.8000, 0.9380>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-3470.700928,1519.476074,155.749146>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_DELIVERY_27
			sDetails.vInCarCorona = <<3123.2500, 6765.6519, 0.5630>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<3127.177002,6770.721680,155.951187>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_DELIVERY_28
			sDetails.vInCarCorona = <<3666.0449, -1481.3199, 1.1250>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<3664.705078,-1489.470093,155.779373>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_DELIVERY_29
			sDetails.vInCarCorona = <<-2261.4790, -1643.8320, 0.3750>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-2246.729736,-1646.456299,155.437851>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_DELIVERY_30
			sDetails.vInCarCorona = <<2602.5811, -2480.5210, 0.1880>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<2592.320557,-2481.460205,156.492661>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
	ENDSWITCH
ENDPROC

PROC SMUGGLER_GET_AIR_POLICE_DROPOFF_COORDS(FREEMODE_DELIVERY_DROPOFFS eDropoffID, FREEMODE_DELIVERY_DROPOFF_DETAILS &sDetails)	
	
	SET_BIT(sDetails.iBS, FREEMODE_DELIVERY_DROPOFF_DETAILS_BS_DROPOFF_HAS_DROP_ZONE)
	
	SWITCH eDropoffID
		CASE SMUGGLER_DROPOFF_AIR_POLICE_1
			sDetails.vInCarCorona = <<-1129.8870, 4908.9502, 218.1760>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-1129.951782,4908.958984,367.263855>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_POLICE_2
			sDetails.vInCarCorona = <<-1278.6790, 4397.9492, 8.7970>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-1278.799805,4398.199219,157.885391>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_POLICE_3
			sDetails.vInCarCorona = <<2472.1189, -383.7140, 108.6330>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<2472.226563,-383.716675,257.634064>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_POLICE_4
			sDetails.vInCarCorona = <<3704.9099, 3792.8081, 10.3440>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<3704.832520,3792.771729,159.380661>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_POLICE_5
			sDetails.vInCarCorona = <<501.4910, 5604.6841, 796.9150>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<501.428497,5604.606445,945.914795>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_POLICE_6
			sDetails.vInCarCorona = <<-324.3640, -1968.7090, 65.9940>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-324.398102,-1968.648804,214.993591>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_POLICE_7
			sDetails.vInCarCorona = <<1352.1530, 4355.0322, 42.7200>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1352.193970,4355.175781,191.719818>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_POLICE_8
			sDetails.vInCarCorona = <<-188.8220, -176.6330, 84.2250>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-188.810226,-176.546570,233.224670>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_POLICE_9
			sDetails.vInCarCorona = <<2327.5911, -2106.1021, 4.0420>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<2327.513916,-2106.169434,153.031601>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_POLICE_10
			sDetails.vInCarCorona = <<-930.6260, 6157.1001, 3.9350>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-930.682434,6157.203125,152.928879>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_POLICE_11
			sDetails.vInCarCorona = <<685.8250, -744.6800, 33.9680>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<685.780396,-744.555176,183.025055>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_POLICE_12
			sDetails.vInCarCorona = <<-3222.7019, 835.2790, 1.3890>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-3222.961914,835.331909,150.392487>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_POLICE_13
			sDetails.vInCarCorona = <<3722.3931, 1525.7360, 0.1880>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<3731.081787,1518.727173,154.992111>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_POLICE_14
			sDetails.vInCarCorona = <<4343.1709, 5407.7422, 0.5630>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<4349.430176,5413.023438,153.488647>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_POLICE_15
			sDetails.vInCarCorona = <<1493.3870, 7343.3418, -0.5630>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1495.308838,7349.076660,153.353683>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_POLICE_16
			sDetails.vInCarCorona = <<1108.5520, -3865.8311, -0.1450>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1108.740112,-3865.596680,149.253159>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_POLICE_17
			sDetails.vInCarCorona = <<-2794.2061, -1340.1281, -0.8570>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-2794.710693,-1341.127197,150.900497>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_POLICE_18
			sDetails.vInCarCorona = <<-3753.3330, 2421.2971, 0.1340>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-3753.042480,2421.227295,151.579239>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_POLICE_19
			sDetails.vInCarCorona = <<2820.6731, 5970.3491, 351.4000>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<2819.887695,5971.665527,498.531128>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_POLICE_20
			sDetails.vInCarCorona = <<-1207.2820, 508.5250, 104.6990>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-1208.541382,508.314423,251.819824>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_POLICE_21
			sDetails.vInCarCorona = <<1458.0880, 1113.1210, 115.2130>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1458.243408,1111.970947,262.333984>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_POLICE_22
			sDetails.vInCarCorona = <<2472.1709, 3423.7290, 50.7130>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<2472.184814,3423.361328,200.951248>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_POLICE_23
			sDetails.vInCarCorona = <<-57.0980, 1915.8101, 196.2400>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-56.979012,1914.767334,343.682281>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_POLICE_24
			sDetails.vInCarCorona = <<-1747.2230, 1991.3180, 118.1050>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-1748.071655,1992.806030,265.120483>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_POLICE_25
			sDetails.vInCarCorona = <<2480.0911, 4951.5361, 45.8720>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<2480.827393,4952.608887,193.007751>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_POLICE_26
			sDetails.vInCarCorona = <<306.2070, 6493.0112, 30.2670>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<304.851227,6493.137207,177.406998>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_POLICE_27
			sDetails.vInCarCorona = <<1184.2531, -2272.9829, 31.2120>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1183.060425,-2273.078369,178.333084>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_POLICE_28
			sDetails.vInCarCorona = <<704.8450, 3122.8479, 44.7280>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<704.983215,3121.728760,191.853897>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_POLICE_29
			sDetails.vInCarCorona = <<2213.4131, 1834.7120, 108.6100>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<2214.312500,1835.359863,255.756439>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_AIR_POLICE_30
			sDetails.vInCarCorona = <<1087.7260, 9.9470, 81.5900>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1087.424561,8.901700,228.847260>>
			sDetails.vAirDrop2 = <<80.000000,80.000000,300.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
	ENDSWITCH
ENDPROC

PROC SMUGGLER_GET_UNDER_RADAR_DROPOFF_COORDS(FREEMODE_DELIVERY_DROPOFFS eDropoffID, FREEMODE_DELIVERY_DROPOFF_DETAILS &sDetails)
	
	SET_BIT(sDetails.iBS, FREEMODE_DELIVERY_DROPOFF_DETAILS_BS_DROPOFF_HAS_DROP_ZONE)
	
	SWITCH eDropoffID
		//LSIA hangars
		CASE SMUGGLER_DROPOFF_UNDER_RADAR_1
			sDetails.vInCarCorona = <<-1009.3990, -1510.9020, 10.7870>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-1009.357300,-1510.772583,30.786808>>
			sDetails.vAirDrop2 = <<45.000000,45.000000,100.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_UNDER_RADAR_2
			sDetails.vInCarCorona = <<488.1490, 193.5760, 110.5210>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<488.338776,193.449310,130.520996>>
			sDetails.vAirDrop2 = <<45.000000,45.000000,100.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_UNDER_RADAR_3
			sDetails.vInCarCorona = <<1136.6360, -1545.0900, 47.1450>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1136.649414,-1545.196167,67.145065>>
			sDetails.vAirDrop2 = <<45.000000,45.000000,100.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_UNDER_RADAR_4
			sDetails.vInCarCorona = <<287.1840, -2430.1260, 7.0420>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<287.095398,-2430.257813,27.041580>>
			sDetails.vAirDrop2 = <<45.000000,45.000000,100.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_UNDER_RADAR_5
			sDetails.vInCarCorona = <<-1273.4919, 613.7950, 138.3030>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-1273.409302,613.635742,158.302734>>
			sDetails.vAirDrop2 = <<45.000000,45.000000,100.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_UNDER_RADAR_6
			sDetails.vInCarCorona = <<1152.2900, -411.7090, 73.4470>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1152.218872,-411.743835,93.447098>>
			sDetails.vAirDrop2 = <<45.000000,45.000000,100.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_UNDER_RADAR_7
			sDetails.vInCarCorona = <<154.7100, 1154.9050, 225.7940>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<154.739182,1154.618896,245.794083>>
			sDetails.vAirDrop2 = <<45.000000,45.000000,100.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_UNDER_RADAR_8
			sDetails.vInCarCorona = <<-3102.6531, 405.0200, 15.2650>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-3102.914063,404.939728,35.265068>>
			sDetails.vAirDrop2 = <<45.000000,45.000000,100.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_UNDER_RADAR_9
			sDetails.vInCarCorona = <<-1568.8170, -416.3710, 58.7550>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-1568.900146,-416.293365,78.754639>>
			sDetails.vAirDrop2 = <<45.000000,45.000000,100.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_UNDER_RADAR_10
			sDetails.vInCarCorona = <<-697.7400, 167.4380, 72.2420>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-697.737610,167.341370,92.242470>>
			sDetails.vAirDrop2 = <<45.000000,45.000000,100.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_UNDER_RADAR_11
			sDetails.vInCarCorona = <<-707.3040, -896.9650, 29.3430>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-707.384216,-896.798706,49.342945>>
			sDetails.vAirDrop2 = <<45.000000,45.000000,100.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_UNDER_RADAR_12
			sDetails.vInCarCorona = <<129.1930, -1504.0460, 35.3500>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<129.280838,-1504.015869,55.350025>>
			sDetails.vAirDrop2 = <<45.000000,45.000000,100.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_UNDER_RADAR_13
			sDetails.vInCarCorona = <<705.9420, -898.6810, 31.1620>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<706.122314,-898.761780,51.161652>>
			sDetails.vAirDrop2 = <<45.000000,45.000000,100.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_UNDER_RADAR_14
			sDetails.vInCarCorona = <<-54.8680, -320.5340, 55.5710>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-54.924786,-320.594635,75.571259>>
			sDetails.vAirDrop2 = <<45.000000,45.000000,100.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_UNDER_RADAR_15
			sDetails.vInCarCorona = <<960.6000, -2310.0020, 38.8360>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<960.729248,-2310.002441,58.835842>>
			sDetails.vAirDrop2 = <<45.000000,45.000000,100.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		
		//Zancudo hangars
		CASE SMUGGLER_DROPOFF_UNDER_RADAR_16
			sDetails.vInCarCorona = <<2475.5801, 3442.3020, 52.9420>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<2475.584229,3442.381592,74.157768>>
			sDetails.vAirDrop2 = <<45.000000,45.000000,100.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_UNDER_RADAR_17
			sDetails.vInCarCorona = <<-71.6990, 6380.3901, 34.0040>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-71.589607,6380.448730,55.219765>>
			sDetails.vAirDrop2 = <<45.000000,45.000000,100.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_UNDER_RADAR_18
			sDetails.vInCarCorona = <<2576.5901, 4648.2651, 35.6550>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<2576.536621,4648.232910,56.870766>>
			sDetails.vAirDrop2 = <<45.000000,45.000000,100.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_UNDER_RADAR_19
			sDetails.vInCarCorona = <<1383.0880, 2164.9009, 100.2210>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1383.052979,2164.876709,121.436768>>
			sDetails.vAirDrop2 = <<45.000000,45.000000,100.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_UNDER_RADAR_20
			sDetails.vInCarCorona = <<-52.2040, 2874.0610, 57.8800>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-52.198067,2874.119141,79.095764>>
			sDetails.vAirDrop2 = <<45.000000,45.000000,100.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_UNDER_RADAR_21
			sDetails.vInCarCorona = <<-2606.0449, 1874.7061, 166.3200>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-2605.906738,1874.732666,187.535767>>
			sDetails.vAirDrop2 = <<45.000000,45.000000,100.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_UNDER_RADAR_22
			sDetails.vInCarCorona = <<2232.6089, 5612.7598, 57.4790>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<2232.607422,5612.829102,78.699219>>
			sDetails.vAirDrop2 = <<45.000000,45.000000,100.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_UNDER_RADAR_23
			sDetails.vInCarCorona = <<-98.9060, 1909.6880, 196.0530>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-98.887901,1909.770020,217.266479>>
			sDetails.vAirDrop2 = <<45.000000,45.000000,100.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_UNDER_RADAR_24
			sDetails.vInCarCorona = <<1338.3840, 4353.1489, 46.2150>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1338.347290,4353.255859,67.430763>>
			sDetails.vAirDrop2 = <<45.000000,45.000000,100.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_UNDER_RADAR_25
			sDetails.vInCarCorona = <<-533.2170, 4196.7212, 191.9910>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-533.309937,4196.712402,213.206039>>
			sDetails.vAirDrop2 = <<45.000000,45.000000,100.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_UNDER_RADAR_26
			sDetails.vInCarCorona = <<-1514.5341, 1524.4220, 117.1120>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-1514.497559,1524.455444,138.327759>>
			sDetails.vAirDrop2 = <<45.000000,45.000000,100.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_UNDER_RADAR_27
			sDetails.vInCarCorona = <<1224.7800, 2735.0911, 41.0890>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1224.727295,2735.129883,62.304764>>
			sDetails.vAirDrop2 = <<45.000000,45.000000,100.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_UNDER_RADAR_28
			sDetails.vInCarCorona = <<1430.9440, 6354.6611, 27.3500>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1430.842529,6354.548340,48.565765>>
			sDetails.vAirDrop2 = <<45.000000,45.000000,100.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_UNDER_RADAR_29
			sDetails.vInCarCorona = <<-746.8320, 5579.2378, 40.5960>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<-746.896423,5579.122070,61.814705>>
			sDetails.vAirDrop2 = <<45.000000,45.000000,100.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
		CASE SMUGGLER_DROPOFF_UNDER_RADAR_30
			sDetails.vInCarCorona = <<1950.5959, 3831.2241, 38.3940>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.vAirDrop1 = <<1950.513184,3831.260498,59.609768>>
			sDetails.vAirDrop2 = <<45.000000,45.000000,100.000000>>
			sDetails.fAirDropAreaWidth = 0.0
		BREAK
	ENDSWITCH
ENDPROC

FUNC INT GET_CUSTOM_SPAWN_POINT_COUNT(FREEMODE_DELIVERY_DROPOFFS eDropoffID)
	UNUSED_PARAMETER(eDropoffID)
	RETURN 8
ENDFUNC

FUNC VECTOR SMUGGLER_GET_HANGAR_ON_FOOT_DROPOFF_COORDS(HANGAR_ID eHangar)
	VECTOR vReturn = <<0.0, 0.0, 0.0>>
	
	SWITCH eHangar
		CASE LSIA_HANGAR_1 			vReturn = <<-1152.4313, -3410.4202, 12.9450>> 	BREAK
		CASE LSIA_HANGAR_A17 		vReturn = <<-1395.3582, -3266.3525, 12.9448>> 	BREAK
		CASE ZANCUDO_HANGAR_A2 		vReturn = <<-2020.9714, 3157.2930, 31.8103>>	BREAK
		CASE ZANCUDO_HANGAR_3497 	vReturn = <<-1877.7657, 3108.9211, 31.8103>>	BREAK
		CASE ZANCUDO_HANGAR_3499 	vReturn = <<-2469.7173, 3275.2539, 31.8325>> 	BREAK
	ENDSWITCH
	
	RETURN vReturn
ENDFUNC

FUNC VECTOR SMUGGLER_GET_HANGAR_VEHICLE_DROPOFF_COORDS(HANGAR_ID eHangar)
	VECTOR vReturn = <<0.0, 0.0, 0.0>>
	
	SWITCH eHangar
		CASE LSIA_HANGAR_1 			vReturn = <<-1148.6279, -3403.8931, 12.9450>>	BREAK
		CASE LSIA_HANGAR_A17 		vReturn = <<-1391.4531, -3259.5642, 12.9448>>	BREAK
		CASE ZANCUDO_HANGAR_A2 		vReturn = <<-2025.2025, 3150.5723, 31.8103>>	BREAK
		CASE ZANCUDO_HANGAR_3497 	vReturn = <<-1881.5845, 3102.0073, 31.8103>> 	BREAK
		CASE ZANCUDO_HANGAR_3499 	vReturn = <<-2473.6804, 3267.9905, 31.8461>> 	BREAK
	ENDSWITCH
	
	RETURN vReturn
ENDFUNC

PROC SMUGGLER_GET_HANGAR_AIR_DROP_AREA_COORDS(HANGAR_ID eHangar, VECTOR &vec1, VECTOR &vec2, FLOAT &fWidth)
	
	SWITCH eHangar
		CASE LSIA_HANGAR_1 			
			vec1 = <<-1132.018677,-3481.439453,36.943932>>	
			vec2 = <<-1227.920898,-3426.592041,136.807266>>
			fWidth = 100.0
		BREAK
		CASE LSIA_HANGAR_A17 		
			vec1 = <<-1381.350220,-3303.019775,29.996233>>	
			vec2 = <<-1434.088257,-3272.819092,130.993393>>
			fWidth = 45.0
		BREAK
		CASE ZANCUDO_HANGAR_A2 		
			vec1 = <<-2019.949463,3159.680420,48.588032>>	
			vec2 = <<-1979.777100,3229.237549,156.589111>>
			fWidth = 70.0
		BREAK
		CASE ZANCUDO_HANGAR_3497 	
			vec1 = <<-1876.744141,3111.080811,43.719162>>	
			vec2 = <<-1850.548950,3156.451172,148.722473>>
			fWidth = 33.0
		BREAK
		CASE ZANCUDO_HANGAR_3499 	
			vec1 = <<-2468.442139,3278.073486,43.713802>> 	
			vec2 = <<-2442.203369,3323.375977,148.730942>>
			fWidth = 33.0
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC FREEMODE_DELIVERY_DROPOFF_DETAILS SMUGGLER_DROPOFF_GET_DETAILS(FREEMODE_DELIVERY_DROPOFFS eDropoffID)
	FREEMODE_DELIVERY_DROPOFF_DETAILS details
	SMUGGLER_VARIATION eMissionVariation = SMUGGLER_GET_MISSION_VARIATION_FROM_DROPOFF(eDropoffID)
	
	IF SMUGGLER_DROPOFF_IS_VALID(eDropoffID)
		details.txtScriptName = "GB_DELIVERY"
		details.shouldDropoffLaunchDeliveryScript = &SMUGGLER_DROPOFF_SHOULD_RUN_DELIVERY_SCRIPT
		details.willDropoffAcceptTypeOfDelivery = &SMUGGLER_DROPOFF_WILL_ACCEPT_TYPE_OF_DELIVERY
		details.fOnFootCoronaRadius = 2.0
		details.fInCarCoronaRadius = 10.0
		
		IF SMUGGLER_DROPOFF_IS_PLAYER_HANGAR(eDropoffID)
			
			SET_BIT(details.iBS, FREEMODE_DELIVERY_DROPOFF_DETAILS_BS_DROPOFF_HAS_DROP_ZONE)
			SET_BIT(details.iBS, FREEMODE_DELIVERY_DROPOFF_DETAILS_BS_DROPOFF_DROPZONE_USES_AA)
			
			HANGAR_ID eHangar 		= GET_SMUGGLER_DROPOFF_HANGAR_ID(eDropoffID)			
			details.vOnFootCorona 	= SMUGGLER_GET_HANGAR_ON_FOOT_DROPOFF_COORDS(eHangar)
			details.vInCarCorona 	= SMUGGLER_GET_HANGAR_VEHICLE_DROPOFF_COORDS(eHangar)
			
			SMUGGLER_GET_HANGAR_AIR_DROP_AREA_COORDS(eHangar, details.vAirDrop1, details.vAirDrop2, details.fAirDropAreaWidth)

			details.cutsceneToPlay = SMUGGLER_CUTSCENE_HANGAR_BRING_EVERYTHING
			
//			SWITCH eMissionVariation
//				CASE SMV_BLACK_BOX
//					details.cutsceneToPlay = SMUGGLER_CUTSCENE_HANGAR_FOOT
//				BREAK
//			ENDSWITCH

		ELSE
			SWITCH eMissionVariation			
				CASE SMV_SELL_HEAVY_LIFTING
					details.cutsceneToPlay = SMUGGLER_CUTSCENE_HEAVY_LIFTING
					SMUGGLER_GET_HEAVY_LIFTING_DROPOFF_COORDS(eDropoffID, details)
				BREAK
								
				CASE SMV_SELL_CONTESTED
					details.cutsceneToPlay = SMUGGLER_CUTSCENE_CONTESTED
					SMUGGLER_GET_CONTESTED_DROPOFF_COORDS(eDropoffID, details)
					details.fInCarCoronaRadius = 15.0
				BREAK
				
				CASE SMV_SELL_AGILE_DELIVERY
					details.cutsceneToPlay = SMUGGLER_CUTSCENE_AGILE_DELIVERY
					SMUGGLER_GET_AGILE_DELIVERY_DROPOFF_COORDS(eDropoffID, details)
					details.fInCarCoronaRadius = 7.5
				BREAK
				
				CASE SMV_SELL_PRECISION_DELIVERY
					SET_BIT(details.iBS, FREEMODE_DELIVERY_DROPOFF_DETAILS_BS_IGNORE_DELIVERABLE_COUNT)
					details.cutsceneToPlay = SMUGGLER_CUTSCENE_PRECISION_DELIVERY
					SMUGGLER_GET_PRECISION_DELIVERY_DROPOFF_COORDS(eDropoffID, details)
				BREAK
				
				CASE SMV_SELL_FLYING_FORTRESS
					details.cutsceneToPlay = SMUGGLER_CUTSCENE_FLYING_FORTRESS
					SMUGGLER_GET_FLYING_FORTRESS_DROPOFF_COORDS(eDropoffID, details)
				BREAK
							
				CASE SMV_SELL_FLY_LOW
					details.cutsceneToPlay = SMUGGLER_CUTSCENE_FLY_LOW
					SMUGGLER_GET_FLY_LOW_DROPOFF_COORDS(eDropoffID, details)
				BREAK
				
				CASE SMV_SELL_AIR_DELIVERY			
					details.cutsceneToPlay = SMUGGLER_CUTSCENE_AIR_DELIVERY
					SMUGGLER_GET_AIR_DELIVERY_DROPOFF_COORDS(eDropoffID, details)
				BREAK
				
				CASE SMV_SELL_AIR_POLICE			
					details.cutsceneToPlay = SMUGGLER_CUTSCENE_AIR_POLICE
					SMUGGLER_GET_AIR_POLICE_DROPOFF_COORDS(eDropoffID, details)
				BREAK
				
				CASE SMV_SELL_UNDER_THE_RADAR
					details.cutsceneToPlay = SMUGGLER_CUTSCENE_UNDER_THE_RADAR
					SMUGGLER_GET_UNDER_RADAR_DROPOFF_COORDS(eDropoffID, details)
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
	
	RETURN details
ENDFUNC

CONST_INT MAX_SMUGGLER_DROPOFF_POOL 60	//The maximum pool size to be used when selecting dropoffs

/// PURPOSE:
///    Used to fill a stuct with MAX_SMUGGLER_DROPOFF_POOL dropoffs in a random order
/// PARAMS:
///    eVariation - The mission variation
///    eDropoffCandidates - The struct to fill
/// RETURNS:
///    True if we are able to successfully fill the struct
FUNC BOOL GENERATE_RANDOM_LIST_OF_SMUG_DROPOFFS(SMUGGLER_VARIATION eVariation, FREEMODE_DELIVERY_DROPOFF_SELECTION_DATA &eDropoffCandidates[MAX_SMUGGLER_DROPOFF_POOL], HANGAR_ID eHanagar)
	
	CONST_INT FREEMODE_DELIVERY_DROPOFF_POOL_SIZE 30
	
	INT i, iMinimum, iMaximum
	FREEMODE_DELIVERY_ROUTES eStartingRoute
	
	SWITCH eVariation
		// Sell mission variations		
		CASE SMV_SELL_HEAVY_LIFTING
			IF IS_HANGAR_IN_LSIA(eHanagar)
				iMinimum = ENUM_TO_INT(SMUGGLER_DROPOFF_HEAVY_LIFTING_1)
				iMaximum = ENUM_TO_INT(SMUGGLER_DROPOFF_HEAVY_LIFTING_10)
			ELSE
				iMinimum = ENUM_TO_INT(SMUGGLER_DROPOFF_HEAVY_LIFTING_11)
				iMaximum = ENUM_TO_INT(SMUGGLER_DROPOFF_HEAVY_LIFTING_20)
			ENDIF
		BREAK
						
		CASE SMV_SELL_CONTESTED
			iMinimum = ENUM_TO_INT(SMUGGLER_DROPOFF_CONTESTED_1)
			iMaximum = ENUM_TO_INT(SMUGGLER_DROPOFF_CONTESTED_30)
		BREAK
		
		CASE SMV_SELL_AGILE_DELIVERY
			iMinimum = ENUM_TO_INT(SMUGGLER_DROPOFF_AGILE_DELIVERY_1)
			iMaximum = ENUM_TO_INT(SMUGGLER_DROPOFF_AGILE_DELIVERY_60)
		BREAK
		
		CASE SMV_SELL_PRECISION_DELIVERY
			IF IS_HANGAR_IN_LSIA(eHanagar)
				iMinimum = ENUM_TO_INT(SMUGGLER_DROPOFF_PRECISION_DELIVERY_1)
				iMaximum = ENUM_TO_INT(SMUGGLER_DROPOFF_PRECISION_DELIVERY_15)
			ELSE
				iMinimum = ENUM_TO_INT(SMUGGLER_DROPOFF_PRECISION_DELIVERY_16)
				iMaximum = ENUM_TO_INT(SMUGGLER_DROPOFF_PRECISION_DELIVERY_30)
			ENDIF
		BREAK
		
		CASE SMV_SELL_FLYING_FORTRESS
			IF IS_HANGAR_IN_LSIA(eHanagar)
				eStartingRoute = FREEMODE_DELIVERY_FLYING_FORTRESS_1
				iMinimum = ENUM_TO_INT(SMUGGLER_DROPOFF_FLYING_FORTRESS_1)
			ELSE
				eStartingRoute = FREEMODE_DELIVERY_FLYING_FORTRESS_6
				iMinimum = ENUM_TO_INT(SMUGGLER_DROPOFF_FLYING_FORTRESS_16)
			ENDIF
		BREAK
					
		CASE SMV_SELL_FLY_LOW
			iMinimum = ENUM_TO_INT(SMUGGLER_DROPOFF_FLY_LOW_1)
			iMaximum = ENUM_TO_INT(SMUGGLER_DROPOFF_FLY_LOW_33)
		BREAK
		
		CASE SMV_SELL_AIR_DELIVERY
			iMinimum = ENUM_TO_INT(SMUGGLER_DROPOFF_AIR_DELIVERY_1)
			iMaximum = ENUM_TO_INT(SMUGGLER_DROPOFF_AIR_DELIVERY_30)
		BREAK
		
		CASE SMV_SELL_AIR_POLICE
			iMinimum = ENUM_TO_INT(SMUGGLER_DROPOFF_AIR_POLICE_1)
			iMaximum = ENUM_TO_INT(SMUGGLER_DROPOFF_AIR_POLICE_30)
		BREAK
		
		CASE SMV_SELL_UNDER_THE_RADAR
			IF IS_HANGAR_IN_LSIA(eHanagar)
				iMinimum = ENUM_TO_INT(SMUGGLER_DROPOFF_UNDER_RADAR_1)
				iMaximum = ENUM_TO_INT(SMUGGLER_DROPOFF_UNDER_RADAR_15)
			ELSE
				iMinimum = ENUM_TO_INT(SMUGGLER_DROPOFF_UNDER_RADAR_16)
				iMaximum = ENUM_TO_INT(SMUGGLER_DROPOFF_UNDER_RADAR_30)
			ENDIF
		BREAK
		
		DEFAULT 
			CASSERTLN("GENERATE_RANDOM_LIST_OF_SMUG_DROPOFFS passed invalid mission variation: ", eVariation)
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	INT iMaxDropoffsForMissionType = ((iMaximum - iMinimum) + 1)
	
	IF eVariation = SMV_SELL_FLYING_FORTRESS
		iMaxDropoffsForMissionType = 5
	ENDIF
		
	//Repeat once through to assign the iMaxDropoffsForMissionType dropoff locations
	FOR i = 0 TO (iMaxDropoffsForMissionType - 1)
		IF eVariation = SMV_SELL_FLYING_FORTRESS
			
			FREEMODE_DELIVERY_ROUTES eRoute = eStartingRoute + INT_TO_ENUM(FREEMODE_DELIVERY_ROUTES, i)
			
			eDropoffCandidates[i].eDropoffID 	= SMUGGLER_GET_DROPOFF_ON_ROUTE(eRoute, 1)
			eDropoffCandidates[i].eRoute 		= eRoute
		ELSE
			eDropoffCandidates[i].eDropoffID 	= INT_TO_ENUM(FREEMODE_DELIVERY_DROPOFFS, (i + iMinimum))
			eDropoffCandidates[i].eRoute		= FREEMODE_DELIVERY_ROUTE_NO_ROUTE
		ENDIF
	ENDFOR
	
	//Repeat once more to shuffle the list
	FOR i = 0 TO (iMaxDropoffsForMissionType - 1)
		FREEMODE_DELIVERY_ROUTES 	eSwapRoute
		FREEMODE_DELIVERY_DROPOFFS 	eSwapDropoff
		
		INT iRand = GET_RANDOM_INT_IN_RANGE(0, iMaxDropoffsForMissionType)
		
		eSwapDropoff 							= eDropoffCandidates[i].eDropoffID
		eSwapRoute								= eDropoffCandidates[i].eRoute
		eDropoffCandidates[i].eDropoffID 		= eDropoffCandidates[iRand].eDropoffID
		eDropoffCandidates[i].eRoute 			= eDropoffCandidates[iRand].eRoute
		eDropoffCandidates[iRand].eDropoffID	= eSwapDropoff
		eDropoffCandidates[iRand].eRoute		= eSwapRoute
	ENDFOR
	
	FOR i = 0 TO (iMaxDropoffsForMissionType - 1)		
		CDEBUG1LN(DEBUG_AMBIENT, "[FM_DO_RES] GENERATE_RANDOM_LIST_OF_SMUG_DROPOFFS slot: ", i, " dropoff: ", FREEMODE_DELIVERY_GET_DROPOFF_DEBUG_NAME(eDropoffCandidates[i].eDropoffID))
	ENDFOR
	
	RETURN TRUE
ENDFUNC

FUNC INT SMUGGLER_GET_MAX_DROPOFFS_FOR_MISSION_VARIATION(SMUGGLER_VARIATION eVariation)
	SWITCH eVariation
		//Resupply missions
		CASE SMV_BEACON_GRAB
		CASE SMV_BLACK_BOX
		CASE SMV_BOMB_BASE
		CASE SMV_BOMBING_RUN	
		CASE SMV_CARGO_PLANE
		CASE SMV_BOMB_ROOF
		CASE SMV_CRATE_CHASE	
		CASE SMV_DOGFIGHT
		CASE SMV_ESCORT
		CASE SMV_INFILTRATION	
		CASE SMV_ROOF_ATTACK
		CASE SMV_SPLASH_LANDING
		CASE SMV_STEAL_AIRCRAFT
		CASE SMV_STUNT_PILOT
		CASE SMV_THERMAL_SCOPE
			RETURN 1
		BREAK
		
		//Sell missions
		CASE SMV_SELL_AGILE_DELIVERY		RETURN 18
		CASE SMV_SELL_AIR_DELIVERY			RETURN 15
		CASE SMV_SELL_AIR_POLICE			RETURN 5
		CASE SMV_SELL_CONTESTED				RETURN 5
		CASE SMV_SELL_FLYING_FORTRESS		RETURN 3
		CASE SMV_SELL_FLY_LOW				RETURN 15
		CASE SMV_SELL_HEAVY_LIFTING			RETURN 2
		CASE SMV_SELL_PRECISION_DELIVERY	RETURN 9
		CASE SMV_SELL_UNDER_THE_RADAR		RETURN 5
	ENDSWITCH
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Sets the data that describes how a dropoff or group of dropoffs should be used 
PROC SMUGGLER_SET_DROPOFF_PROPERTIES(SMUGGLER_VARIATION eVariation, FREEMODE_DELIVERY_ACTIVE_DROPOFF_PROPERTIES &sData)
	
	UNUSED_PARAMETER(eVariation)
	UNUSED_PARAMETER(sData)
	
//	SWITCH eVariation
//		CASE GRV_SELL_HILL_CLIMB		FREEMODE_FELIVERY_SET_DROPOFF_PROPERTIES(sData, DROPOFF_PROPERTY_ANY_ORDER)					BREAK
//		CASE GRV_SELL_ROUGH_TERRAIN		FREEMODE_FELIVERY_SET_DROPOFF_PROPERTIES(sData, DROPOFF_PROPERTY_FINAL_DROPOFF_ACTIVE_LAST)	BREAK
//		CASE GRV_SELL_PHANTOM			FREEMODE_FELIVERY_SET_DROPOFF_PROPERTIES(sData, DROPOFF_PROPERTY_SIMPLE)					BREAK
//		CASE GRV_SELL_AMBUSHED			FREEMODE_FELIVERY_SET_DROPOFF_PROPERTIES(sData, DROPOFF_PROPERTY_SEQUENTIAL_DELIVERY)		BREAK
//		CASE GRV_SELL_FOLLOW_THE_LEADER	FREEMODE_FELIVERY_SET_DROPOFF_PROPERTIES(sData, DROPOFF_PROPERTY_VEHICLE_SPECIFIC)			BREAK
//		CASE GRV_SELL_MOVE_WEAPONS		FREEMODE_FELIVERY_SET_DROPOFF_PROPERTIES(sData, DROPOFF_PROPERTY_SIMPLE_MULTIPLE)			BREAK
//		
//		DEFAULT
//			FREEMODE_FELIVERY_SET_DROPOFF_PROPERTIES(sData, DROPOFF_PROPERTY_HANGAR)
//		BREAK
//	ENDSWITCH
	
ENDPROC

FUNC BOOL SHOULD_SMUGGLER_DROPOFF_SPAWN_PLAYER_AFTER_DELIVERY(FREEMODE_DELIVERY_DROPOFFS eDropoffID) 
	IF eDropoffID >= SMUGGLER_DROPOFF_LSIA_HANGAR_1
	AND eDropoffID <= SMUGGLER_DROPOFF_ZANCUDO_HANGAR_3499
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Returns the model name of prop required by a dropoff
/// PARAMS:
///    eDropoff - The dropoff
///    iPropNumber - prop 0, 1 or 2
FUNC MODEL_NAMES SMUGGLER_GET_DROPOFF_EXTRA_MODEL(FREEMODE_DELIVERY_DROPOFFS eDropoff, INT iPropNumber)
	UNUSED_PARAMETER(eDropoff)
	UNUSED_PARAMETER(iPropNumber)
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC VECTOR SMUGGLER_GET_DROPOFF_EXTRA_MODEL_POSITION(FREEMODE_DELIVERY_DROPOFFS eDropoff, INT iPropNumber)
	UNUSED_PARAMETER(eDropoff)
	UNUSED_PARAMETER(iPropNumber)	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC VECTOR SMUGGLER_GET_DROPOFF_EXTRA_MODEL_ROTATION(FREEMODE_DELIVERY_DROPOFFS eDropoff, INT iPropNumber)
	UNUSED_PARAMETER(eDropoff)
	UNUSED_PARAMETER(iPropNumber)	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

/// PURPOSE:
///    Returns any extra information required by the smuggler mission scripts 
///    e.g. Model names of extra props to be created at the given dropoff points
/// PARAMS:
///    eRoutes - The dropoff to check
FUNC FREEMODE_DELIVERY_DROPOFF_PROPS SMUGGLER_GET_DROPOFF_EXTRA_DATA(FREEMODE_DELIVERY_DROPOFFS eDropoff)
	
	INT i
	FREEMODE_DELIVERY_DROPOFF_PROPS sData
	
	REPEAT FREEMODE_DELIVERY_MAX_EXTRA_PROPS_PER_DROPOFF i
		sData.ePropModels[i]	= SMUGGLER_GET_DROPOFF_EXTRA_MODEL(eDropoff, i)
		sData.vPropPosition[i]	= SMUGGLER_GET_DROPOFF_EXTRA_MODEL_POSITION(eDropoff, i)
		sData.vPropRotation[i]	= SMUGGLER_GET_DROPOFF_EXTRA_MODEL_ROTATION(eDropoff, i)
	ENDREPEAT
	
	RETURN sData
ENDFUNC

FUNC INT SMUGGLER_GET_DROPOFF_COUNT_OF_PROPS_REQUIRED(FREEMODE_DELIVERY_DROPOFFS eDropoff)
	UNUSED_PARAMETER(eDropoff)
	RETURN 0
ENDFUNC

FUNC BOOL SMUGGLER_CAN_WE_RESERVE_ALL_REQUIRED_NETWORK_ENTITIES(INT iNumMissionEntities, FREEMODE_DELIVERY_DROPOFFS eDropoff, SMUGGLER_VARIATION eVariation)
	
	UNUSED_PARAMETER(iNumMissionEntities)
	UNUSED_PARAMETER(eVariation)
	
	INT iDropoffProps	= SMUGGLER_GET_DROPOFF_COUNT_OF_PROPS_REQUIRED(eDropoff)
	INT iNumVehicles 	= 0//GB_GET_SMUGGLER_NUM_VEH_REQUIRED(eVariation, SMV_INVALID, iNumMissionEntities)
	INT iNumProps 		= 0//GB_GET_SMUGGLER_NUM_OBJ_REQUIRED(eVariation, SMV_INVALID, iNumMissionEntities, iDropoffProps)
	
	PRINTLN("[FM_DO_RES] SMUGGLER_CAN_WE_RESERVE_ALL_REQUIRED_NETWORK_ENTITIES - iDropoffProps = ", iDropoffProps, " iNumVehicles = ", iNumVehicles, " iNumProps = ", iNumProps)
	
	RETURN CAN_REGISTER_MISSION_ENTITIES(0, iNumVehicles, iNumProps, 0)
ENDFUNC

FUNC BOOL SMUGGLER_DO_ALL_DROPOFFS_ON_ROUTE_MEET_DISTANCE_REQUIREMENTS(FREEMODE_DELIVERY_ROUTES eRoute, FLOAT fMinDistance, VECTOR vhangarCoords)
	INT i
	VECTOR vDropoffCoords
	FREEMODE_DELIVERY_DROPOFFS eDropoff
	FREEMODE_DELIVERY_DROPOFF_DETAILS eDetails
	
	REPEAT SMUGGLER_GET_MAX_DROPOFFS_ON_ROUTE() i
		eDropoff 		= SMUGGLER_GET_DROPOFF_ON_ROUTE(eRoute, (i + 1))
		eDetails 		= SMUGGLER_DROPOFF_GET_DETAILS(eDropoff)
		vDropoffCoords 	= eDetails.vInCarCorona
		
		IF eDropoff != FREEMODE_DELIVERY_DROPOFF_INVALID
		AND fMinDistance > VDIST2(vDropoffCoords, vhangarCoords)
			PRINTLN("[FM_DO_RES] SMUGGLER_DO_ALL_DROPOFFS_ON_ROUTE_MEET_DISTANCE_REQUIREMENTS - Dropoff ", 
						FREEMODE_DELIVERY_GET_DROPOFF_DEBUG_NAME(eDropoff), " on route: ", 
						FREEMODE_DELIVERY_GET_ROUTE_DEBUG_NAME(eRoute), " failed the distance requirement. Distance is: ",
						VDIST2(vDropoffCoords, vhangarCoords), " minimum distance is: ", fMinDistance)
			RETURN FALSE
		ENDIF		
		
	ENDREPEAT
	
	PRINTLN("[FM_DO_RES] SMUGGLER_DO_ALL_DROPOFFS_ON_ROUTE_MEET_DISTANCE_REQUIREMENTS - All dropoffs are all further away than the minimum distance of: ", SQRT(fMinDistance))
	
	RETURN TRUE
ENDFUNC

FUNC FLOAT SMUGGLER_GET_DROPOFF_MIN_DISTANCE(SMUGGLER_VARIATION eVariation)
	SWITCH eVariation		
		//Sell missions
		CASE SMV_SELL_AGILE_DELIVERY		RETURN 2250000.0	//(1500 * 1500)
		CASE SMV_SELL_AIR_DELIVERY			RETURN 2250000.0	//(1500 * 1500)
		CASE SMV_SELL_AIR_POLICE			RETURN 2250000.0	//(1500 * 1500)
		CASE SMV_SELL_CONTESTED				RETURN 2250000.0	//(1500 * 1500)
		CASE SMV_SELL_FLYING_FORTRESS		RETURN 2250000.0	//(1500 * 1500)
		CASE SMV_SELL_FLY_LOW				RETURN 2250000.0	//(1500 * 1500)
		CASE SMV_SELL_HEAVY_LIFTING			RETURN 9000000.0	//(3000 * 3000)
		CASE SMV_SELL_PRECISION_DELIVERY	RETURN 2250000.0	//(1500 * 1500)
		CASE SMV_SELL_UNDER_THE_RADAR		RETURN 2250000.0	//(1500 * 1500)
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

/// PURPOSE:
///    Selects a freemode delivery dropoff based on the mission variation.
///    Can only be called by the host of freemode
/// PARAMS:
///    piGangBoss - The id of the gang boss starting a mission
///    eOwnedHanagr - The ID of the players owned hangar
///    eVariation - The mission variation selected
///    sData - The data struct to fill (FREEMODE_DELIVERY_ACTIVE_DROPOFF_LIST)
///    eLocation - city or countryside dropoff?
/// RETURNS:
///    True if a dropoff or multiple dropoffs were successfully selected and reserved
FUNC BOOL GET_DROP_OFF_FOR_SMUGGLER_VARIATION(PLAYER_INDEX piGangBoss, HANGAR_ID eOwnedHanagr, SMUGGLER_VARIATION eVariation, FREEMODE_DELIVERY_ACTIVE_DROPOFF_PROPERTIES &sData)
	
	SIMPLE_INTERIORS eSimpleInteriorID = GET_SIMPLE_INTERIOR_ID_FROM_HANGAR_ID(eOwnedHanagr)
	//Array of dropoffs that we select from
	FREEMODE_DELIVERY_DROPOFF_SELECTION_DATA eDropoffCandidates[MAX_SMUGGLER_DROPOFF_POOL]
	//Dropoff details
	FREEMODE_DELIVERY_DROPOFF_DETAILS eDetails
	//Minimum distance the first dropoff must be from the hangar
	FLOAT fMinDistFromHangarRoute, fMinDistFromHangarSingleDropoff
	FLOAT fTempHangarDist
	VECTOR vDropoffCoords, vhangarCoords
	INT i, j, iMaxSmugglerEnitiesForMission, iNumGoons, iProductTotal
	
	IF piGangBoss = INVALID_PLAYER_INDEX()
		CASSERTLN(DEBUG_AMBIENT, "GET_DROP_OFF_FOR_SMUGGLER_VARIATION passed invalid boss id.")
		RETURN FALSE
	ELIF NOT GB_IS_PLAYER_BOSS_OF_A_GANG(piGangBoss)
		CASSERTLN(DEBUG_AMBIENT, "GET_DROP_OFF_FOR_SMUGGLER_VARIATION passed non boss player.")
		RETURN FALSE
	ELIF NOT IS_HANGAR_ID_VALID(eOwnedHanagr)
		CASSERTLN(DEBUG_AMBIENT, "GET_DROP_OFF_FOR_SMUGGLER_VARIATION passed invalid owned hangar.")
		RETURN FALSE
	ELIF NETWORK_GET_HOST_OF_SCRIPT(FREEMODE_SCRIPT()) != PLAYER_ID()
		CASSERTLN(DEBUG_AMBIENT, "GET_DROP_OFF_FOR_SMUGGLER_VARIATION needs to be called by the host of freemode.")
		RETURN FALSE
	ENDIF
	
	vhangarCoords					= g_SimpleInteriorData.vMidPoints[eSimpleInteriorID]
	fMinDistFromHangarRoute 		= SMUGGLER_GET_DROPOFF_MIN_DISTANCE(eVariation)
	fMinDistFromHangarSingleDropoff = fMinDistFromHangarRoute //TODO update
	
	//Initalise the struct
	INIT_FREEMODE_ACTIVE_DROPOFF_PROPERTIES(sData)
	//Set the properties of the dropoff
	SMUGGLER_SET_DROPOFF_PROPERTIES(eVariation, sData)
	
	iNumGoons 						= (GB_GET_NUM_GOONS_IN_PLAYER_GANG(piGangBoss) + 1)
	iProductTotal 					= GET_PLAYERS_CONTRABAND_UNITS_TOTAL_FOR_HANGAR(piGangBoss)
	iMaxSmugglerEnitiesForMission 	= GB_GET_NUM_SMUGGLER_ENTITIES_FOR_VARIATION(eVariation, iNumGoons, piGangBoss, iProductTotal, DEFAULT #IF IS_DEBUG_BUILD , FALSE, NATIVE_TO_INT(piGangBoss) #ENDIF) * GET_SMUGGLER_DELIVERABLE_QUANTITY(eVariation)
		
	#IF IS_DEBUG_BUILD
	IF g_FreemodeDeliveryData.eDbgSelectedDropoff[0] != FREEMODE_DELIVERY_DROPOFF_INVALID
	AND g_FreemodeDeliveryData.eDbgSelectedDropoff[0] >= SMUGGLER_DROPOFF_LSIA_HANGAR_1
		IF eVariation = SMUGGLER_GET_MISSION_VARIATION_FROM_DROPOFF(g_FreemodeDeliveryData.eDbgSelectedDropoff[0])
		
			CDEBUG1LN(DEBUG_AMBIENT, "[FM_DO_RES] GET_DROP_OFF_FOR_SMUGGLER_VARIATION *DEBUG* Reserving dropoffs")
			
			REPEAT 3 i
				
				IF g_FreemodeDeliveryData.eDbgSelectedDropoff[i] != FREEMODE_DELIVERY_DROPOFF_INVALID
					//Select the required number of dropoffs for this variation
					sData.eDropoffList.eDropoffID[sData.eDropoffList.iCount] = g_FreemodeDeliveryData.eDbgSelectedDropoff[i]					
					sData.eDropoffList.iCount ++
				
					eDetails = SMUGGLER_DROPOFF_GET_DETAILS(g_FreemodeDeliveryData.eDbgSelectedDropoff[i])
					CDEBUG1LN(DEBUG_AMBIENT, "[FM_DO_RES] GET_DROP_OFF_FOR_SMUGGLER_VARIATION Dropoff coord = ", eDetails.vInCarCorona, " for dropoff with ID: ", g_FreemodeDeliveryData.eDbgSelectedDropoff[i])
				ENDIF
				
				//Exit when we have the number of dropoffs we need
				IF sData.eDropoffList.iCount >= iMaxSmugglerEnitiesForMission
				OR sData.eDropoffList.iCount >= SMUGGLER_GET_MAX_DROPOFFS_FOR_MISSION_VARIATION(eVariation)
					FREEMODE_DELIVERY_DEBUG_PRINT_FREEMODE_DELIVERY_DROPOFF_PROPERTIES(sData)		
					RETURN TRUE
				ENDIF
				
			ENDREPEAT
		ENDIF
	ENDIF
	
	g_FreemodeDeliveryData.eDbgSelectedDropoff[0]	= FREEMODE_DELIVERY_DROPOFF_INVALID
	g_FreemodeDeliveryData.eDbgSelectedDropoff[1]	= FREEMODE_DELIVERY_DROPOFF_INVALID
	g_FreemodeDeliveryData.eDbgSelectedDropoff[2]	= FREEMODE_DELIVERY_DROPOFF_INVALID
	
	IF g_FreemodeDeliveryData.eDbgSelectedRoute	!= FREEMODE_DELIVERY_ROUTE_NO_ROUTE
	AND g_FreemodeDeliveryData.eDbgSelectedRoute >= FREEMODE_DELIVERY_FLYING_FORTRESS_1
		FREEMODE_DELIVERY_DROPOFFS eDropoff = SMUGGLER_GET_DROPOFF_ON_ROUTE(g_FreemodeDeliveryData.eDbgSelectedRoute, 1)
		IF eVariation = SMUGGLER_GET_MISSION_VARIATION_FROM_DROPOFF(eDropoff)
			sData.eDropoffList.iCount = SMUGGLER_GET_MAX_DROPOFFS_ON_ROUTE()
			CDEBUG1LN(DEBUG_AMBIENT, "[FM_DO_RES] GET_DROP_OFF_FOR_SMUGGLER_VARIATION *DEBUG* Reserving dropoffs on route: ", FREEMODE_DELIVERY_GET_ROUTE_DEBUG_NAME(g_FreemodeDeliveryData.eDbgSelectedRoute), ". Starting with dropoff: ", FREEMODE_DELIVERY_GET_DROPOFF_DEBUG_NAME(eDropoff), " . Absolute maximum no. dropoffs: ", sData.eDropoffList.iCount, " will reserve: ", iMaxSmugglerEnitiesForMission)
					
			REPEAT sData.eDropoffList.iCount j
				sData.eDropoffList.eDropoffID[j] = SMUGGLER_GET_DROPOFF_ON_ROUTE(g_FreemodeDeliveryData.eDbgSelectedRoute, (j + 1))
				
				IF (j + 1) >= iMaxSmugglerEnitiesForMission
				AND sData.eDropoffList.iCount < FREEMODE_DELIVERY_MAX_ACTIVE_DROPOFFS
					//For missions that need dropoffs based on the number of vehicles being created
					//Currently all missions with 5 dropoffs need all of them regardless of the number of vehicles
					sData.eDropoffList.iCount = (j + 1)
					BREAKLOOP
				ENDIF				
			ENDREPEAT
			
			FREEMODE_DELIVERY_DEBUG_PRINT_FREEMODE_DELIVERY_DROPOFF_PROPERTIES(sData)
			
			g_FreemodeDeliveryData.eDbgSelectedRoute = FREEMODE_DELIVERY_ROUTE_NO_ROUTE
			
			RETURN TRUE	
		ENDIF
	ENDIF
	#ENDIF
	
	SWITCH eVariation
		CASE SMV_BEACON_GRAB
		CASE SMV_BLACK_BOX
		CASE SMV_BOMB_BASE
		CASE SMV_BOMBING_RUN	
		CASE SMV_CARGO_PLANE
		CASE SMV_BOMB_ROOF
		CASE SMV_CRATE_CHASE	
		CASE SMV_DOGFIGHT
		CASE SMV_ESCORT
		CASE SMV_INFILTRATION	
		CASE SMV_ROOF_ATTACK
		CASE SMV_SPLASH_LANDING
		CASE SMV_STEAL_AIRCRAFT
		CASE SMV_STUNT_PILOT
		CASE SMV_THERMAL_SCOPE
		CASE SMV_SETUP_RON
			
			//We're not checking if we can reserve entities for this one as theese missions all use the hangar which does not reserve any props
			//Set the struct data - For resupply missions the final dropoff will always be the players hangar
			sData.eDropoffList.eDropoffID[0] = GET_SMUGGLER_DROPOFF_SIMPLE_INTERIOR(eSimpleInteriorID)
			sData.eDropoffList.iCount = 1
			
			CDEBUG1LN(DEBUG_AMBIENT, "[FM_DO_RES] GET_DROP_OFF_FOR_SMUGGLER_VARIATION Reserving hangar dropoff: ", sData.eDropoffList.eDropoffID[0], " for player: ", GET_PLAYER_NAME(piGangBoss), " owned hangar ID: ", eOwnedHanagr, " simple interior: ", eSimpleInteriorID)
			//Always return true for the hangars as multiple players could have the same hangar
			RETURN TRUE
			
		BREAK
		
		// Sell mission variations
		CASE SMV_SELL_AGILE_DELIVERY
		CASE SMV_SELL_AIR_DELIVERY
		CASE SMV_SELL_AIR_POLICE
		CASE SMV_SELL_CONTESTED
		CASE SMV_SELL_FLYING_FORTRESS
		CASE SMV_SELL_FLY_LOW
		CASE SMV_SELL_HEAVY_LIFTING	
		CASE SMV_SELL_PRECISION_DELIVERY
		CASE SMV_SELL_UNDER_THE_RADAR
			GENERATE_RANDOM_LIST_OF_SMUG_DROPOFFS(eVariation, eDropoffCandidates, eOwnedHanagr)
			
			REPEAT MAX_SMUGGLER_DROPOFF_POOL i
				eDetails = SMUGGLER_DROPOFF_GET_DETAILS(eDropoffCandidates[i].eDropoffID)
				
				vDropoffCoords = eDetails.vInCarCorona
				fTempHangarDist = VDIST2(vDropoffCoords, vhangarCoords)
				
				IF SMUGGLER_DROPOFF_IS_VALID(eDropoffCandidates[i].eDropoffID)
				AND NOT SMUGGLER_DROPOFF_IS_PLAYER_HANGAR(eDropoffCandidates[i].eDropoffID)
				AND NOT IS_DROPOFF_RESERVED_ON_SERVER(eDropoffCandidates[i].eDropoffID)
					IF SMUGGLER_CAN_WE_RESERVE_ALL_REQUIRED_NETWORK_ENTITIES(iMaxSmugglerEnitiesForMission, eDropoffCandidates[i].eDropoffID, eVariation)
						//If it's a route the fill the list of dropoffs
						IF eVariation = SMV_SELL_FLYING_FORTRESS
							
							IF SMUGGLER_DO_ALL_DROPOFFS_ON_ROUTE_MEET_DISTANCE_REQUIREMENTS(eDropoffCandidates[i].eRoute, fMinDistFromHangarRoute, vhangarCoords)
								sData.eDropoffList.iCount = SMUGGLER_GET_MAX_DROPOFFS_ON_ROUTE()
								
								REPEAT sData.eDropoffList.iCount j
									sData.eDropoffList.eDropoffID[j] = SMUGGLER_GET_DROPOFF_ON_ROUTE(eDropoffCandidates[i].eRoute, (j + 1))
									
									IF (j + 1) >= sData.eDropoffList.iCount
									AND sData.eDropoffList.iCount < FREEMODE_DELIVERY_MAX_ACTIVE_DROPOFFS							
										//For missions that need dropoffs based on the number of vehicles being created
										//Currently all missions with 5 dropoffs need all of them regardless of the number of vehicles
										sData.eDropoffList.iCount = (j + 1)
										BREAKLOOP
									ENDIF
								ENDREPEAT
								
								#IF IS_DEBUG_BUILD
									FREEMODE_DELIVERY_DEBUG_PRINT_FREEMODE_DELIVERY_DROPOFF_PROPERTIES(sData)
								#ENDIF
								
								RETURN TRUE
							ELSE
								PRINTLN("[FM_DO_RES] GET_DROP_OFF_FOR_SMUGGLER_VARIATION Route ", i, " has a dropoff that is too close to be selected for hangar: ", eOwnedHanagr)
							ENDIF
						ELSE
							IF fTempHangarDist > fMinDistFromHangarSingleDropoff
								//Select the required number of dropoffs for this variation
								sData.eDropoffList.eDropoffID[sData.eDropoffList.iCount] = eDropoffCandidates[i].eDropoffID						
								sData.eDropoffList.iCount ++
								
								//Exit when we have the number of dropoffs we need
								IF sData.eDropoffList.iCount >= iMaxSmugglerEnitiesForMission
								OR sData.eDropoffList.iCount >= SMUGGLER_GET_MAX_DROPOFFS_FOR_MISSION_VARIATION(eVariation)
									#IF IS_DEBUG_BUILD
										FREEMODE_DELIVERY_DEBUG_PRINT_FREEMODE_DELIVERY_DROPOFF_PROPERTIES(sData)
									#ENDIF
									RETURN TRUE
								ENDIF
							ELSE
								PRINTLN("[FM_DO_RES] GET_DROP_OFF_FOR_SMUGGLER_VARIATION Dropoff ", i, " is too close to be selected for hanagr: ", eOwnedHanagr, " distance is: ", fTempHangarDist)
							ENDIF
							
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_AMBIENT, "[FM_DO_RES] GET_DROP_OFF_FOR_SMUGGLER_VARIATION dropoff ", i, " failed checks as we can't reserve the number of entities required")
					ENDIF
					
				#IF IS_DEBUG_BUILD
				ELIF IS_DROPOFF_RESERVED_ON_SERVER(eDropoffCandidates[i].eDropoffID)
					CDEBUG1LN(DEBUG_AMBIENT, "[FM_DO_RES] GET_DROP_OFF_FOR_SMUGGLER_VARIATION dropoff ", i, " failed checks as it is already active")
				ELSE 
					CDEBUG1LN(DEBUG_AMBIENT, "[FM_DO_RES] GET_DROP_OFF_FOR_SMUGGLER_VARIATION dropoff ", i, " failed checks as it is too close to the hanagar")
				#ENDIF
				ENDIF
				
			ENDREPEAT
			
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

