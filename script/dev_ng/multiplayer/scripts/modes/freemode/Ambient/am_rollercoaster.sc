// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	am_rollercoaster.sc
//		AUTHOR			:	Martin McMillan
//		DESCRIPTION		:   Rollercoaster ride for multiplayer
//
// *****************************************************************************************
// *****************************************************************************************

//----------------------
//	INCLUDES
//----------------------
USING "rage_builtins.sch"
USING "globals.sch"
USING "rgeneral_include.sch"
USING "net_include.sch"
USING "net_fps_cam.sch"
USING "net_wait_zero.sch"
USING "net_mission.sch"
USING "shared_hud_displays.sch"
USING "mp_scaleform_functions.sch"
USING "commands_money.sch"
USING "net_common_functions.sch"
USING "net_transition_sessions.sch"

#IF IS_DEBUG_BUILD 
	USING "shared_debug.sch"
#ENDIF

//----------------------
//	CONSTANTS
//----------------------

TWEAK_FLOAT GRAV_VALUE  			10.0
TWEAK_FLOAT CLIMB_SPEED 			3.0
//TWEAK_FLOAT CLIMB_ACCEL			0.3
TWEAK_FLOAT NULL_VALUE 				-0.0000000001
CONST_FLOAT COASTER_START_POSITION	24.086

CONST_INT MAX_CAR_OCCUPANTS			2
CONST_INT RIDE_ID					1
CONST_INT MAX_TRACK_NODES 			225
CONST_INT NO_OF_LAPS				1
CONST_INT MAX_SEATS					8
TWEAK_INT COASTER_TIME				30000	//30 Secs
CONST_INT BAR_SOUND_DELAY			1000
CONST_INT MAX_SCREAM_LEVEL			300

//----------------------
//	Local Bitset
//----------------------

INT iLocalBitSet
CONST_INT	HAS_LOCAL_PLAYER_BECOME_HOST	0
CONST_INT	DO_MIGRATION					1
CONST_INT	BUTTONS_REQUESTED				2
CONST_INT	HELP_SHOWN						3
CONST_INT	ENTER_BUTTON_SHOWN				4
CONST_INT	PLAYER_LOCATED_SEAT				5 
CONST_INT	STORE_ACTIVE					6
CONST_INT 	ACTIVATED_PASSIVE				7


//----------------------
//	STRUCT
//----------------------

STRUCT COASTER_CAR_STRUCT
	VECTOR m_vPos
	FLOAT m_fSpeed
	INT iLastNodePassed
ENDSTRUCT

STRUCT LOCAL_POSITION_STRUCT
	INT iCurrentSeat
	VECTOR vCarPlayerOffset
	VECTOR vLocalExitPoint
ENDSTRUCT

STRUCT ROLLERCOASTER_SEATS
	VECTOR vBackSeat
	VECTOR vEntryPoint
	VECTOR vExitPoint
	INT iNumOccupants
ENDSTRUCT

STRUCT TRACK_NODE_STRUCT
	VECTOR m_vPos			//Position if node
	FLOAT m_fDist			//Distance of this node along track
	FLOAT m_fSpeed			//Recommended speed at this node
ENDSTRUCT

//----------------------
//	VARIABLES
//----------------------

INT iCurrentScreamDuration = 0
INT iCurrentCoasterTimer = COASTER_TIME
FLOAT fVolume

INT iScreamXpThisRide

FLOAT fCarSeperation = 2.55

VECTOR vNorms[MAX_TRACK_NODES]
VECTOR vCarPosition
INT iNumberOfNodes
#IF IS_DEBUG_BUILD
BOOL bUseConstSpeed
FLOAT fRaiseVal = 0.0
#ENDIF

TIME_DATATYPE previousNetworkTime
FLOAT fTimeStep

INT coasterRunningTime
INT iTempNode = -1
//VECTOR vFrontOfCar

OBJECT_INDEX objCars[4]

VEHICLE_INDEX cameraCar
MODEL_NAMES cameraCarModel = SANCHEZ

BOOL bWarp
BOOL bRefreshButtons = TRUE

//Instructional button variables
SCALEFORM_INDEX buttonMovie
SCALEFORM_INSTRUCTIONAL_BUTTONS instructionalButtons
SPRITE_PLACEMENT aSprite

COASTER_CAR_STRUCT coasterCar

LOCAL_POSITION_STRUCT coasterPositions

TRACK_NODE_STRUCT trackNodes[MAX_TRACK_NODES]

STRING sFW_AnimDict = "anim@mp_rollarcoaster"

TEXT_LABEL_23 sFW_AnimPlayer
TEXT_LABEL_23 sFW_AnimPlayer2

TEXT_LABEL_31 sFW_AnimCarPlayer
TEXT_LABEL_63 sFW_AnimBarPlayer

ENUM ROLLERCOASTER_ENTER_ANIM_STAGE
	ENTER_WALK_TO_POINT	= 0,
	ENTER_PLAY_ANIM,
	ENTER_ANIM_PLAYING,
	ENTER_IN_CAR
ENDENUM

ENUM ROLLERCOASTER_EXIT_ANIM_STAGE
	EXIT_BAR_RAISE_ANIM = 0,
	EXIT_PLAY_ANIM,
	EXIT_OUT_OF_CAR
ENDENUM

ENUM ROLLERCOASTER_BAR_ANIM_STAGE
	BAR_PLAY_LOWER_ANIM	= 0,
	BAR_PLAYER_IDLE
ENDENUM

ENUM ROLLERCOASTER_HANDS_RAISE_STAGE
	HANDS_RAISE = 0,
	HANDS_IDLE
ENDENUM

ROLLERCOASTER_ENTER_ANIM_STAGE eEnterAnimStage = ENTER_WALK_TO_POINT
ROLLERCOASTER_EXIT_ANIM_STAGE eExitAnimStage = EXIT_BAR_RAISE_ANIM
ROLLERCOASTER_BAR_ANIM_STAGE eBarAnimStage = BAR_PLAY_LOWER_ANIM
ROLLERCOASTER_HANDS_RAISE_STAGE eHandsAnimStage = HANDS_RAISE

INT iEnterScene
INT iBarPlayerScene

SCRIPT_TIMER barSoundTimer

CONST_INT COASTER_BS_LOADED_PLAYER_RIDE			0
CONST_INT COASTER_BS_LOADED_AMBIENT_RIDE		1
INT iCoasterBS	

SCRIPT_TIMER soundLoadFailsafe
							
//----------------------	
//	GAME STATE				
//----------------------

CONST_INT	GAME_STATE_AWAIT_ACTIVATION		0 
CONST_INT	GAME_STATE_INIT					1
CONST_INT	GAME_STATE_READY				2


ENUM ROLLER_COASTER_RUN_STAGE
	ROLLER_COASTER_IN_STATION = 0,
	ROLLER_COASTER_BOARDING,
	ROLLER_COASTER_PREPARING,
	ROLLER_COASTER_RUNNING,
	ROLLER_COASTER_UNLOADING,
	ROLLER_COASTER_BROKEN
ENDENUM

//----------------------
//	BROADCAST VARIABLES
//----------------------

// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData

	ROLLER_COASTER_RUN_STAGE eRollerCoasterStage = ROLLER_COASTER_BOARDING

	INT iServerGameState = GAME_STATE_AWAIT_ACTIVATION
	
	INT iServerBitSet
	
//	COASTER_STRUCT theRollerCoaster
	
	BOOL bSeatIsOccupied[MAX_SEATS]
	
	ROLLERCOASTER_SEATS rollercoasterPositions[MAX_SEATS]
	
	SCRIPT_TIMER iCoasterTimer
	
ENDSTRUCT

ServerBroadcastData serverBD

CONST_INT ROLLERCOASTER_CONTROL_OF_CAR		0
CONST_INT ROLLERCOASTER_HAS_BEEN_BOARDED	1
CONST_INT ROLLERCOASTER_HAS_BEEN_UNLOADED	2
CONST_INT TIMER_RESET						3
CONST_INT UNLOAD							4
CONST_INT DRAW_CLIENT_TIMER					5
CONST_INT BLOCK_PLAYER_ENTRY				6

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	
	INT iGameState = GAME_STATE_AWAIT_ACTIVATION
	
	ROLLER_COASTER_RUN_STAGE eRollerCoasterStage = ROLLER_COASTER_BOARDING
	
	int iClientBitSet
	
	INT iLapsDone
	
	INT iCarOrder
	
ENDSTRUCT

PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

CONST_INT	PLAYER_IN_CAR					0
CONST_INT	PLAYER_HANDS_RAISED				1
CONST_INT 	LAP_COUNTED						2
CONST_INT	READY_FOR_UNLOADING				3
CONST_INT	CLIENT_COASTER_STOPPED			4
CONST_INT	LOAD_AND_PLAY_SOUND				5
CONST_INT 	PUTTING_PLAYER_IN_CAR			6
CONST_INT 	REMOVING_PLAYER_FROM_CAR		7
CONST_INT	PLAYER_PREPARED					8
CONST_INT	REFRESH_ANIMATION				9
CONST_INT	RAISED_BAR						10
CONST_INT	BAR_SOUND_PLAYED				11
CONST_INT	FREEZE_OCCURRED					12

//----------------------
//	FUNCTIONS
//----------------------

PROC SET_ROLLERCOASTER_PASSIVE_MODE(BOOL bOn)

	IF bOn
		IF NOT IS_BIT_SET(iLocalBitset,ACTIVATED_PASSIVE)
			SET_TEMP_PASSIVE_MODE()
			SET_BIT(iLocalBitset,ACTIVATED_PASSIVE)
			println("am_rollercoaster - SET_ROLLERCOASTER_PASSIVE_MODE - TRUE")
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBitset,ACTIVATED_PASSIVE)
			CLEANUP_TEMP_PASSIVE_MODE()
			CLEAR_BIT(iLocalBitset,ACTIVATED_PASSIVE)
			println("am_rollercoaster - SET_ROLLERCOASTER_PASSIVE_MODE - FALSE")
		ELSE
			println("am_rollercoaster - SET_ROLLERCOASTER_PASSIVE_MODE - not cleaning up temp passive mode")
		ENDIF
	ENDIF

ENDPROC

//Cleanup Mission Data
PROC SCRIPT_CLEANUP()
	INT iTemp
	
	REPEAT COUNT_OF(objCars) iTemp
		DELETE_OBJECT(objCars[iTemp])
	ENDREPEAT
	
	STOP_STREAM()
	
	//Clear globals
	g_fCoasterDist = 0
	g_fCoasterSpeed = 0
	
	// Temp fix for url:bugstar:2454570
	MPGlobalsAmbience.bPossibleToBoardRollerCoaster = FALSE
	
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(buttonMovie)
	
	//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].g_bUsingRollerCoaster = FALSE
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bUsingRollerCoaster)
	PRINTLN("am_rollercoaster - g_bUsingRollerCoaster = FALSE")	
									
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_NET_PLAYER_OK(PLAYER_ID(),TRUE,TRUE)
			NETWORK_ALLOW_REMOTE_ATTACHMENT_MODIFICATION(PLAYER_PED_ID(),FALSE)
		ENDIF
		
		DELETE_VEHICLE(cameraCar)
			
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_IN_CAR)
			CLEAR_PED_TASKS(PLAYER_PED_ID())
		ENDIF
			//SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
		SET_ROLLERCOASTER_PASSIVE_MODE(FALSE)
	ENDIF
	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

FUNC INT GET_NEXT_NODE(INT iNode)
	INT iNext = iNode+1
	IF iNext >= iNumberOfNodes
		iNext = 1
	ENDIF
	RETURN iNext
ENDFUNC

FUNC INT GET_PREVIOUS_NODE(INT iNode)
	INT iPrev = iNode-1
	IF iPrev < 0
		iPrev = iNumberOfNodes-2
	ENDIF
	RETURN iPrev
ENDFUNC

FUNC VECTOR RAISE_NODE_BY_FLOAT_VAL(INT iNode, FLOAT fVal)
	VECTOR vRet
	vRet = vNorms[iNode] * fVal
	RETURN vRet
ENDFUNC

FUNC VECTOR GET_TRACK_NODE_POSITION(INT iNodeIndex)
	RETURN trackNodes[iNodeIndex].m_vPos #IF IS_DEBUG_BUILD  + RAISE_NODE_BY_FLOAT_VAL(iNodeIndex, fRaiseVal) #ENDIF
ENDFUNC

PROC SET_TRACK_NODE_POSITION(INT iNodeIndex, VECTOR vPos)
	IF iNodeIndex >=0 AND iNodeIndex <= MAX_TRACK_NODES
		trackNodes[iNodeIndex].m_vPos = vPos
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD

	WIDGET_GROUP_ID wTrackEditor
	INT iSelectedKnot = 0
	INT iPreviousKnot = -1
	INT iCurrentNormal
	INT iWidgetKnot
	BOOL bAddNodePressed
	BOOL bRemoveNodePressed
	BOOL bEditorWidgetBuilt = TRUE
	BOOL bPrintNodes
	BOOL bRunRide
	BOOL bEndScript
	BOOL bStep
	BOOL bEditorActive
	BOOL bNormalsReady
	BOOL bKillSpeed
	BOOL bExtraDebug
	
	FLOAT fConstSpeed = 1.0
	FLOAT fSpeedlimit = 50.0
	
	INT iNodeInfo[4][4]
	
	VECTOR vSelectedNodePos
	VECTOR vCurrentPoints[3]
#ENDIF	
	
	FUNC BOOL IS_VECTOR_NULL(VECTOR vTemp)
		RETURN ARE_VECTORS_EQUAL(vTemp, <<NULL_VALUE, NULL_VALUE, NULL_VALUE>>)
	ENDFUNC
	
	FUNC BOOL IS_TRACK_NODE_NULL(INT iNodeIndex)
		RETURN IS_VECTOR_NULL(trackNodes[iNodeIndex].m_vPos)
	ENDFUNC
	PROC SET_VECTOR_NULL(VECTOR &vRef)
		vRef = <<NULL_VALUE, NULL_VALUE, NULL_VALUE>>
	ENDPROC
	
	PROC ERASE_NODE_DATA(INT iNodeIndex)
		SET_VECTOR_NULL(trackNodes[iNodeIndex].m_vPos)
		trackNodes[iNodeIndex].m_fDist = 0.0
	ENDPROC
#IF IS_DEBUG_BUILD	
	FUNC VECTOR GET_SIDE_VECTOR(VECTOR vLine)
		VECTOR vRet
		SET_VECTOR_NULL(vRet)
		IF NOT IS_VECTOR_ZERO(vLine)
			vRet = <<-vLine.y, vLine.x,0>>
			//if we define dx=x2-x1 and dy=y2-y1, then the normals are (-dy, dx) and (dy, -dx).
		ENDIF
		RETURN vRet
	ENDFUNC
	
	//gets line normal in z direction
	FUNC VECTOR GET_LINE_NORMAL(VECTOR vLine)
		VECTOR vRet
		SET_VECTOR_NULL(vRet)
		IF NOT IS_VECTOR_ZERO(vLine)
			VECTOR vSide = GET_SIDE_VECTOR(vLine)
			vRet = CROSS_PRODUCT(vLine, vSide)
		ENDIF
		RETURN vRet
	ENDFUNC
	
	FUNC VECTOR GET_NODE_SMOOTHED_NORMAL(INT iNode)
		VECTOR vRet
		SET_VECTOR_NULL(vRet)
		IF NOT IS_TRACK_NODE_NULL(iNode)
			INT iNext = GET_NEXT_NODE(iNode)
			INT iPrev = iNode - 1
			IF iPrev < 0
				iPrev = iNumberOfNodes - 2
			ENDIF
			//vector from previous to Next
			vRet = GET_LINE_NORMAL(NORMALISE_VECTOR( trackNodes[iNext].m_vPos - trackNodes[iPrev].m_vPos))
		ENDIF
		RETURN vRet
	ENDFUNC
	
	PROC BUILD_NORMALS()
		IF NOT bNormalsReady
			vNorms[iCurrentNormal] = GET_NODE_SMOOTHED_NORMAL(iCurrentNormal)
			iCurrentNormal++
			IF iCurrentNormal >= iNumberOfNodes
				bNormalsReady = TRUE
				iCurrentNormal = 0
			ENDIF
		ENDIF
	ENDPROC
#ENDIF	
	PROC DELETE_TRACK()
		INT iTemp
		REPEAT COUNT_OF(trackNodes) iTemp
			ERASE_NODE_DATA(iTemp)
		ENDREPEAT
	ENDPROC
#IF IS_DEBUG_BUILD	
	
	PROC MOVE_TRACK_NODES_LEFT()
		INT iTemp
		REPEAT COUNT_OF(trackNodes) iTemp
			IF IS_TRACK_NODE_NULL(iTemp)
				INT iTemp2
				FOR iTemp2 = iTemp+1 TO MAX_TRACK_NODES-1
					IF NOT IS_TRACK_NODE_NULL(iTemp2)	
						trackNodes[iTemp] = trackNodes[iTemp2]
						ERASE_NODE_DATA(iTemp2)
						iTemp2 = MAX_TRACK_NODES
						PRINTLN("AM_ROLLER - Moving data from node ", iTemp2, " into node ", iTemp)
					ENDIF
					IF iTemp2 = MAX_TRACK_NODES-1
						EXIT //all remaining nodes empty
					ENDIF
				ENDFOR
			ENDIF
		ENDREPEAT
	ENDPROC
	
	PROC DELETE_TRACK_NODE(INT iNodeIndex)
		ERASE_NODE_DATA(iNodeIndex)
		MOVE_TRACK_NODES_LEFT()
	ENDPROC
	
	FUNC INT GET_NUMBER_OF_TRACK_NODES_IN_TRACK()
		INT iTemp
		REPEAT COUNT_OF(trackNodes) iTemp
			IF IS_TRACK_NODE_NULL(iTemp)
				RETURN iTemp
			ENDIF
		ENDREPEAT
		RETURN MAX_TRACK_NODES
	ENDFUNC
	
	FUNC BOOL CAN_TRACK_NODE_BE_ADDED_TO_TRACK()
		RETURN iNumberOfNodes < MAX_TRACK_NODES
	ENDFUNC
	
	PROC SET_CURRENT_NODE(INT iNode)
		IF iNode >= 0
		AND iNode < iNumberOfNodes
			vSelectedNodePos = trackNodes[iNode].m_vPos
			#IF IS_DEBUG_BUILD
			iWidgetKnot = iNode
			#ENDIF
		ENDIF
	ENDPROC
	
	FUNC BOOL IS_THIS_TRACK_NODE_ENDPOINT(INT iNodeIndex)
		IF iNodeIndex = MAX_TRACK_NODES-1
			RETURN TRUE
		ENDIF
		IF iNodeIndex < MAX_TRACK_NODES
		AND iNodeIndex >= 0
		AND IS_TRACK_NODE_NULL(iNodeIndex+1)
			RETURN TRUE
		ENDIF
		RETURN FALSE
	ENDFUNC
	
	PROC MOVE_TRACK_NODES_RIGHT(INT iStartPoint)
		INT iTemp
		IF iStartPoint > 0 AND iStartPoint < MAX_TRACK_NODES-1
			VECTOR vTemp1
			SET_VECTOR_NULL(vTemp1)
			FOR iTemp = iStartPoint TO MAX_TRACK_NODES-1  		//	repeat through all knots
				
			 	VECTOR vTemp2 = trackNodes[iTemp].m_vPos			//	Save current value to vTemp2
				trackNodes[iTemp].m_vPos = vTemp1					//	Set current value to vTemp1
				vTemp1 = vTemp2									//	Save vTemp2  value (The value of this node when we arrived) to vTemp1 for next node
				IF IS_VECTOR_NULL(vTemp2)						//	If vTemp2 is Null value then we have reached the end of the track.
					EXIT
				ENDIF
			ENDFOR
		ENDIF
	ENDPROC
	
	//adds a knot to a track after the index provided
	//if in between two existing knots will shift all knots right
	PROC ADD_TRACK_NODE_TO_TRACK(INT iNodeIndex)
		IF iNodeIndex < MAX_TRACK_NODES-1
		AND CAN_TRACK_NODE_BE_ADDED_TO_TRACK()
			IF IS_THIS_TRACK_NODE_ENDPOINT(iNodeIndex)
				IF iNumberOfNodes > 1
					trackNodes[iNodeIndex+1].m_vPos = trackNodes[iNodeIndex].m_vPos + (NORMALISE_VECTOR(trackNodes[iNodeIndex].m_vPos - trackNodes[iNodeIndex-1].m_vPos))
				ELIF iNumberOfNodes = 1
					trackNodes[iNodeIndex+1].m_vPos = trackNodes[iNodeIndex].m_vPos + <<0,1,0>>
				ENDIF
				PRINTLN("AM_ROLLER - adding a node to the end of the array at index ", iNodeIndex+1)
			ELSE
				MOVE_TRACK_NODES_RIGHT(iNodeIndex+1)
				trackNodes[iNodeIndex+1].m_vPos = trackNodes[iNodeIndex].m_vPos + 0.5*(trackNodes[iNodeIndex+2].m_vPos - trackNodes[iNodeIndex].m_vPos)
				PRINTLN("AM_ROLLER - adding a node to middle of the array at node ", iNodeIndex+1)
			ENDIF
			PRINTLN("AM_ROLLER - added new node with index ", iNodeIndex+1, " at position ", trackNodes[iNodeIndex+1].m_vPos)
		ELSE
			SCRIPT_ASSERT("Track is full, no further nodes can be added")
		ENDIF
	ENDPROC
	
	PROC DRAW_TRACK()
	
		INT iTemp
		
		DRAW_DEBUG_LINE(vCurrentPoints[0], vCurrentPoints[0]+<<0,0,2>>, 255, 0, 0)
		DRAW_DEBUG_TEXT(GET_STRING_FROM_INT(coasterCar.iLastNodePassed+1), vCurrentPoints[0]+<<0,0,2>>)
		DRAW_DEBUG_LINE(vCurrentPoints[1], vCurrentPoints[1]+<<0,0,2>>, 0, 255, 0)
		DRAW_DEBUG_TEXT(GET_STRING_FROM_INT(coasterCar.iLastNodePassed), vCurrentPoints[1]+<<0,0,2>>)
		DRAW_DEBUG_LINE_WITH_TWO_COLOURS(vCurrentPoints[1] + <<0,0,1.5>>, vCurrentPoints[1] + <<0,0,1.5>> + vCurrentPoints[2] , 0, 255, 0, 255, 255, 0, 0, 255)
		
		REPEAT COUNT_OF(objCars) iTemp
			IF iTemp < COUNT_OF(objCars)-1
			AND DOES_ENTITY_EXIST(objCars[iTemp])
			AND DOES_ENTITY_EXIST(objCars[iTemp+1])
				DRAW_DEBUG_LINE(GET_ENTITY_COORDS(objCars[iTemp]), GET_ENTITY_COORDS(objCars[iTemp+1]))
			ENDIF
		ENDREPEAT
		
		REPEAT COUNT_OF(trackNodes) iTemp
		
			IF NOT IS_TRACK_NODE_NULL(iTemp)
			
				DRAW_DEBUG_SPHERE(GET_TRACK_NODE_POSITION(iTemp) , 0.025, 255, 0 , 0, 175)
				
				IF iTemp > 0
				AND NOT IS_TRACK_NODE_NULL(iTemp-1)
					DRAW_DEBUG_LINE(GET_TRACK_NODE_POSITION(iTemp), GET_TRACK_NODE_POSITION(iTemp-1), 255, 0, 0)
				ELIF iTemp = 0
					DRAW_DEBUG_LINE(GET_TRACK_NODE_POSITION(0), GET_TRACK_NODE_POSITION(iNumberOfNodes-1), 255, 0, 0)
				ENDIF
				
				DRAW_DEBUG_TEXT(GET_STRING_FROM_INT(iTemp), GET_TRACK_NODE_POSITION(iTemp)+<<0,0,1>>)
				DRAW_DEBUG_LINE(GET_TRACK_NODE_POSITION(iTemp), GET_TRACK_NODE_POSITION(iTemp) + GET_NODE_SMOOTHED_NORMAL(iTemp))
				
				IF iTemp = iSelectedKnot
					DRAW_DEBUG_SPHERE(GET_TRACK_NODE_POSITION(iTemp), 0.075, 0, 255 , 0)
				ENDIF
				
			ELSE
			
				iTemp = MAX_TRACK_NODES
				
			ENDIF
			
		ENDREPEAT
		
	ENDPROC
	
	PROC PRINT_TRACK_NODES()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("||*******************  - NODE LIST - *******************||")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		INT iTemp
		REPEAT COUNT_OF(trackNodes) iTemp
			IF NOT IS_TRACK_NODE_NULL(iTemp)
				SAVE_STRING_TO_DEBUG_FILE("PUSH_TRACK_NODE_WITH_POSITION(")
				SAVE_VECTOR_TO_DEBUG_FILE(GET_TRACK_NODE_POSITION(iTemp))
				SAVE_STRING_TO_DEBUG_FILE(")")
				SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDIF
		ENDREPEAT
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("||******************  - END OF NODES - ******************||")
		SAVE_NEWLINE_TO_DEBUG_FILE()
	ENDPROC
	
	PROC BUILD_TRACK_EDITOR_WIDGET()
	
		wTrackEditor = START_WIDGET_GROUP("Rollercoaster")
		IF bEditorActive
			ADD_WIDGET_BOOL("Stop editor", bEditorActive)
			ADD_WIDGET_INT_SLIDER("Selected knot", iWidgetKnot, 0, MAX_TRACK_NODES-1, 1)
			ADD_WIDGET_FLOAT_SLIDER("Gravity value", GRAV_VALUE, 0, 20, 0.1)
			ADD_WIDGET_BOOL("Add node", bAddNodePressed)
			ADD_WIDGET_BOOL("Remove node", bRemoveNodePressed)
			ADD_WIDGET_FLOAT_SLIDER("Raise val", fRaiseVal, 0.0, 2.0, 0.001)
			ADD_WIDGET_VECTOR_SLIDER("Current node position", vSelectedNodePos, -2000.0, 2000.0, 0.001)
			ADD_WIDGET_INT_READ_ONLY("Number of nodes in track", iNumberOfNodes)
			ADD_WIDGET_BOOL("Use const speed", bUseConstSpeed)
			ADD_WIDGET_FLOAT_SLIDER("Speed", fConstSpeed, -50.0, 50.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Car seperation", fCarSeperation, 0.0, 5.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Max speed", fSpeedlimit, 0.0, 50.0, 0.1)
			
			START_WIDGET_GROUP("Car info")
			
				INT iTemp
				TEXT_LABEL_63 sTemp
				FOR iTemp = 0 TO 3
					sTemp = "Car "
					sTemp += iTemp
					START_WIDGET_GROUP(stemp)
						ADD_WIDGET_INT_READ_ONLY("prev node", iNodeInfo[iTemp][0])
						ADD_WIDGET_INT_READ_ONLY("last node", iNodeInfo[iTemp][1])
						ADD_WIDGET_INT_READ_ONLY("this node", iNodeInfo[iTemp][2])
						ADD_WIDGET_INT_READ_ONLY("next node", iNodeInfo[iTemp][3])
					STOP_WIDGET_GROUP()
				ENDFOR
				
			STOP_WIDGET_GROUP()
			
			ADD_WIDGET_BOOL("Print node list", bPrintNodes)
			ADD_WIDGET_BOOL("Step", bStep)
			ADD_WIDGET_BOOL("Run", bRunRide)
			ADD_WIDGET_BOOL("End",bEndScript)
			ADD_WIDGET_BOOL("STOP!", bKillSpeed)
			
		ELSE
			ADD_WIDGET_BOOL("Start editor", bEditorActive)
			ADD_WIDGET_BOOL("Step", bStep)
			ADD_WIDGET_BOOL("Run", bRunRide)
			ADD_WIDGET_BOOL("End",bEndScript)
			ADD_WIDGET_BOOL("More Prints",bExtraDebug)
			ADD_WIDGET_BOOL("Warp To Rollercoaster",bWarp)
			ADD_WIDGET_FLOAT_READ_ONLY("DIST",g_fCoasterDist)
			ADD_WIDGET_FLOAT_READ_ONLY("SPEED",g_fCoasterSpeed)
			ADD_WIDGET_VECTOR_SLIDER("POS",coasterCar.m_vPos,-2000,2000,1)
			ADD_WIDGET_INT_READ_ONLY("LAST NODE",coasterCar.iLastNodePassed)
		ENDIF
		STOP_WIDGET_GROUP()
		
	ENDPROC
	
	PROC RUN_TRACK_WIDGET()
	
		iNumberOfNodes = GET_NUMBER_OF_TRACK_NODES_IN_TRACK()
		
		IF bEditorActive
		
			DRAW_TRACK()
			
			IF iSelectedKnot != iWidgetKnot
				IF iWidgetKnot >= 0 
				AND iWidgetKnot < iNumberOfNodes
					iSelectedKnot = iWidgetKnot
				ELSE
					iWidgetKnot = iSelectedKnot
				ENDIF
			ENDIF
			
			IF iSelectedKnot != iPreviousKnot
				SET_CURRENT_NODE(iSelectedKnot)
			ENDIF
			
			IF NOT ARE_VECTORS_EQUAL(GET_TRACK_NODE_POSITION(iSelectedKnot), vSelectedNodePos)
				SET_TRACK_NODE_POSITION(iSelectedKnot, vSelectedNodePos)
				bNormalsReady = FALSE
			ENDIF
			
			IF NOT bEditorWidgetBuilt
				IF DOES_WIDGET_GROUP_EXIST(wTrackEditor)
					DELETE_WIDGET_GROUP(wTrackEditor)
				ENDIF
				BUILD_TRACK_EDITOR_WIDGET()
				bEditorWidgetBuilt = TRUE
			ENDIF
			
			IF bAddNodePressed
				ADD_TRACK_NODE_TO_TRACK(iSelectedKnot)
				SET_CURRENT_NODE(iSelectedKnot+1)
				bNormalsReady = FALSE
				bAddNodePressed = FALSE
			ENDIF
			
			IF bRemoveNodePressed
				DELETE_TRACK_NODE(iSelectedKnot)
				SET_CURRENT_NODE(iSelectedKnot)
				IF iWidgetKnot >= iNumberOfNodes
					iWidgetKnot--
				ENDIF
				bNormalsReady = FALSE
				bRemoveNodePressed = FALSE
			ENDIF
			
			IF bPrintNodes
				WAIT(0)
				PRINT_TRACK_NODES()
				bPrintNodes = FALSE
			ENDIF
			
			IF bKillSpeed
				g_fCoasterSpeed = 0.0
				bKillSpeed = FALSE
			ENDIF
			
			BUILD_NORMALS()
			iPreviousKnot = iSelectedKnot
			
		ELSE
		
			IF bEditorWidgetBuilt
				IF DOES_WIDGET_GROUP_EXIST(wTrackEditor)
					DELETE_WIDGET_GROUP(wTrackEditor)
				ENDIF
				BUILD_TRACK_EDITOR_WIDGET()
				bEditorWidgetBuilt = FALSE
			ENDIF
						
		ENDIF
		
	ENDPROC
	
#ENDIF

PROC CALCULATE_NODE_DISTANCES()
	INT iTemp
	FLOAT fDist = 0.0
	REPEAT COUNT_OF(trackNodes) iTemp
		IF NOT IS_TRACK_NODE_NULL(iTemp)
			trackNodes[iTemp].m_fDist = fDist
			IF iTemp < MAX_TRACK_NODES-1
				fDist += VDIST(GET_TRACK_NODE_POSITION(iTemp), GET_TRACK_NODE_POSITION(iTemp+1))
			ENDIF
		ELSE
			EXIT
		ENDIF
	ENDREPEAT
ENDPROC

//function used to build track path using positions grabbed from the widget
PROC PUSH_TRACK_NODE_WITH_POSITION(VECTOR vPos)
	IF iNumberOfNodes < MAX_TRACK_NODES
	AND iNumberOfNodes >= 0
		SET_TRACK_NODE_POSITION(iNumberOfNodes, vPos)
		iNumberOfNodes++
		#IF IS_DEBUG_BUILD
		iWidgetKnot++
		#ENDIF
	ELSE
		SCRIPT_ASSERT("Track is full, no further nodes can be added")
	ENDIF
ENDPROC

FUNC VECTOR GET_POINT_OF_DISTANCE_ALONG_TRACK(FLOAT fDist, INT iLastNode)

	VECTOR vPoint
	SET_VECTOR_NULL(vPoint)
	INT iNode1, iNode2
	
	IF fDist < 0
		fDist += trackNodes[iNumberOfNodes-1].m_fDist
	ENDIF
	
	IF g_fCoasterSpeed >= 0.0
		iNode1 = iLastNode
		iNode2 = GET_NEXT_NODE(iLastNode)
	ELSE
		iNode1 = GET_NEXT_NODE(iLastNode)
		iNode2 = iLastNode
	ENDIF
	
	FLOAT fDistBetween = ABSF(trackNodes[iNode2].m_fDist - trackNodes[iNode1].m_fDist)
	FLOAT fFurtherDist = fDist - trackNodes[iNode1].m_fDist
	FLOAT fMag = fFurtherDist/fDistBetween
	VECTOR vDir = GET_TRACK_NODE_POSITION(iNode2) - GET_TRACK_NODE_POSITION(iNode1)
	
	#IF IS_DEBUG_BUILD
		vCurrentPoints[0] = GET_TRACK_NODE_POSITION(iNode2)
		vCurrentPoints[1] = GET_TRACK_NODE_POSITION(iNode1)
		vCurrentPoints[2] = vDir
	#ENDIF
	
	IF g_fCoasterSpeed >= 0
		vPoint =  GET_TRACK_NODE_POSITION(iNode1)+ (vDir*fMag)
	ELSE
		vPoint =  GET_TRACK_NODE_POSITION(iNode1) - (vDir*fMag)
	ENDIF
	
	RETURN vPoint
	
ENDFUNC

FUNC FLOAT GET_ACCELERATION_FROM_TRACK_POSITION()

	//grab the next node index
	INT iNext = GET_NEXT_NODE(coasterCar.iLastNodePassed)
	
	//figure out the drop or rise of the track
	FLOAT fHeightDiff = trackNodes[coasterCar.iLastNodePassed].m_vPos.z - trackNodes[iNext].m_vPos.z
	
	//then the length of the track section
	FLOAT fLength = trackNodes[iNext].m_fDist - trackNodes[coasterCar.iLastNodePassed].m_fDist
	IF fLength < 0
		fLength += trackNodes[MAX_TRACK_NODES-1].m_fDist
	ENDIF
	
	FLOAT fIncline = ASIN(fHeightDiff/fLength)
	RETURN GRAV_VALUE*SIN(fIncline)
	
ENDFUNC

FUNC VECTOR GET_ROTATION_BETWEEN_TRACK_NODES(INT iNode1, INT iNode2)
	VECTOR vDir
	vDir = NORMALISE_VECTOR(trackNodes[iNode2].m_vPos - trackNodes[iNode1].m_vPos)
	FLOAT yaw = ATAN2(vDir.X, vDir.Y)
	FLOAT pitch = ATAN2(vDir.Z, SQRT((vDir.X * vDir.X) + (vDir.Y * vDir.Y)))
	RETURN <<-pitch, 0, -yaw-180>>
ENDFUNC

//cycles back from the passed point to find the closest node behind the distance. Needs to be used properly, always pass in the lead car's last node as the start point.
FUNC INT GET_LAST_NODE_PASSED_FROM_DISTANCE(FLOAT fDist, INT iStartPoint)
	INT iTemp
	IF fDist <= 0
		fDist += trackNodes[iNumberOfNodes-1].m_fDist
		iStartPoint = iNumberOfNodes-1
	ENDIF
	FOR iTemp = iStartPoint TO 0 STEP -1
		IF trackNodes[iTemp].m_fDist < fDist
			
			RETURN iTemp
		ENDIF
	ENDFOR
	RETURN 0
ENDFUNC

PROC SET_ROTATION_FOR_TRACK_PROGRESS(INT iCar, INT iLastNode, FLOAT fDist)

	//			iLastNode
	// a			b			c			d
	// *------------*--||||-----*-----------*
	
	INT iNodeA = GET_PREVIOUS_NODE(iLastNode)
	INT iNodeB = iLastNode
	INT iNodeC = GET_NEXT_NODE(iLastNode)
	INT iNodeD = GET_NEXT_NODE(iNodeC)
	
	#IF IS_DEBUG_BUILD
		iNodeInfo[iCar][0] = iNodeA
		iNodeInfo[iCar][1] = iNodeB
		iNodeInfo[iCar][2] = iNodeC
		iNodeInfo[iCar][3] = iNodeD
	#ENDIF
	
	IF fDist < 0
		fDist += trackNodes[iNumberOfNodes-1].m_fDist
	ENDIF
	
	FLOAT fTime
	FLOAT fQuat1[4], fQuat2[4], fQuatEnd[4]
	FLOAT fProg = (fDist-trackNodes[iNodeB].m_fDist)/(trackNodes[iNodeC].m_fDist - trackNodes[iNodeB].m_fDist)
	
	IF fProg < 0.5
		fTime = fProg + 0.5
		GET_ROTATION_QUATERNION(GET_ROTATION_BETWEEN_TRACK_NODES(iNodeA, iNodeB), fQuat1[0], fQuat1[1], fQuat1[2], fQuat1[3])
		GET_ROTATION_QUATERNION(GET_ROTATION_BETWEEN_TRACK_NODES(iNodeB, iNodeC), fQuat2[0], fQuat2[1], fQuat2[2], fQuat2[3])	
	ELSE
		fTime = fProg - 0.5
		GET_ROTATION_QUATERNION(GET_ROTATION_BETWEEN_TRACK_NODES(iNodeB, iNodeC), fQuat1[0], fQuat1[1], fQuat1[2], fQuat1[3])
		GET_ROTATION_QUATERNION(GET_ROTATION_BETWEEN_TRACK_NODES(iNodeC, iNodeD), fQuat2[0], fQuat2[1], fQuat2[2], fQuat2[3])	
	ENDIF
	
	SLERP_NEAR_QUATERNION(fTime, fQuat1[0], fQuat1[1], fQuat1[2], fQuat1[3], fQuat2[0], fQuat2[1], fQuat2[2], fQuat2[3], fQuatEnd[0], fQuatEnd[1], fQuatEnd[2], fQuatEnd[3])
	SET_ENTITY_QUATERNION(objCars[iCar], fQuatEnd[0], fQuatEnd[1], fQuatEnd[2], fQuatEnd[3])
	
ENDPROC

//Draw time until next rollercoaster ride
PROC DRAW_TIMER()
	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<<-1652.7169, -1089.2167, 12.1492>>,<<40, 40, 40>>,FALSE)
	AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bUsingFerrisWheel)
	AND NOT IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
		IF (COASTER_TIME-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBd.iCoasterTimer)) >= 0
			DRAW_GENERIC_TIMER((COASTER_TIME-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBd.iCoasterTimer)), "AMRC_TIMER", 0, TIMER_STYLE_DONTUSEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_TOP)
		ELSE
			DRAW_GENERIC_TIMER(0, "AMRC_TIMER", 0, TIMER_STYLE_DONTUSEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_TOP)
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Sets up the Control Icons to be displayed in the bottom right of the screen
PROC PROCESS_INSTRUCTIONAL_BUTTONS()	
	IF NOT (serverBD.eRollerCoasterStage >= ROLLER_COASTER_UNLOADING)
		IF IS_BIT_SET(iLocalBitSet,BUTTONS_REQUESTED)
			IF HAS_SCALEFORM_MOVIE_LOADED(buttonMovie)
				IF bRefreshButtons
					aSprite = GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
					REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(instructionalButtons)
					
					IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(),sFW_AnimDict,"hands_up_enter_player_one")
					AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(),sFW_AnimDict,"hands_up_enter_player_two")
					AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(),sFW_AnimDict,"hands_up_exit_player_one")
					AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(),sFW_AnimDict,"hands_up_exit_player_two")	
						IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_HANDS_RAISED)
							#IF IS_DEBUG_BUILD
								println("am_rollercoaster - HANDS ARE RAISED")
							#ENDIF
							
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_X), "AMRC_HANDSLOWER", instructionalButtons)
						ELSE
							#IF IS_DEBUG_BUILD
								println("am_rollercoaster - HANDS ARE LOWERED")
							#ENDIF
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_X), "AMRC_HANDSRAISE", instructionalButtons)

						ENDIF
					ENDIF
					
					#IF NOT IS_JAPANESE_BUILD
						IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_VEH_CIN_CAM), "AMRC_CINEMATIC", instructionalButtons)
						ELSE
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL), "AMRC_CINEMATIC", instructionalButtons)
						ENDIF
					#ENDIF
					#IF IS_JAPANESE_BUILD
						IF IS_PLAYSTATION_PLATFORM()
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT), "AMRC_CINEMATIC", instructionalButtons)
						ELSE
							IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
								ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_VEH_CIN_CAM), "AMRC_CINEMATIC", instructionalButtons)
							ELSE
								ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL), "AMRC_CINEMATIC", instructionalButtons)
							ENDIF
						ENDIF	
					#ENDIF
					
					IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
						ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_NEXT_CAMERA), "AMRC_CHANGEVIEW", instructionalButtons)
					ELSE
						ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT), "AMRC_CHANGEVIEW", instructionalButtons)
					ENDIF
										
					bRefreshButtons = FALSE
					println("am_rollercoaster - PROCESS_INSTRUCTIONAL_BUTTONS - Refreshed buttons.")
				ENDIF	
				SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
				
				SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(FALSE)
				
				SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
				SET_LOADING_ICON_SUBTITLES_OFFSET_SHIFT_THIS_FRAME()
				
				RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(buttonMovie, aSprite, instructionalButtons, SHOULD_REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(instructionalButtons))
				//println("am_rollercoaster - PROCESS_INSTRUCTIONAL_BUTTONS - RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS")
			ENDIF
		ELSE
			buttonMovie = REQUEST_SCALEFORM_MOVIE("instructional_buttons")
			SET_BIT(iLocalBitSet,BUTTONS_REQUESTED)
			println("am_rollercoaster - PROCESS_INSTRUCTIONAL_BUTTONS - SET_BIT(iLocalBitSet,BUTTONS_REQUESTED)")
		ENDIF
	ELSE
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(buttonMovie)
		CLEAR_BIT(iLocalBitSet,BUTTONS_REQUESTED)
		bRefreshButtons = TRUE
		println("am_rollercoaster - PROCESS_INSTRUCTIONAL_BUTTONS - CLEAR_BIT(iLocalBitSet,BUTTONS_REQUESTED)")
	ENDIF
ENDPROC

//take care of switching host
PROC CHECK_FOR_MIGRATE()

	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF NOT IS_BIT_SET(iLocalBitSet,HAS_LOCAL_PLAYER_BECOME_HOST) AND NOT IS_BIT_SET(iLocalBitSet,DO_MIGRATION)
			SET_BIT(iLocalBitSet,HAS_LOCAL_PLAYER_BECOME_HOST)
			SET_BIT(iLocalBitSet,DO_MIGRATION)
		ENDIF
	ELSE
		CLEAR_BIT(iLocalBitSet,HAS_LOCAL_PLAYER_BECOME_HOST)
	ENDIF
ENDPROC

PROC SET_POSITION_OF_CARS()
	INT iTemp
	
	REPEAT COUNT_OF(objCars) iTemp
		//IF IS_BIT_SET(serverBD.iServerBitSet,ROLLERCOASTER_CONTROL_OF_CAR)
			INT iNode = GET_LAST_NODE_PASSED_FROM_DISTANCE(g_fCoasterDist - fCarSeperation*iTemp, coasterCar.iLastNodePassed)
			VECTOR vPoint = GET_POINT_OF_DISTANCE_ALONG_TRACK(g_fCoasterDist - fCarSeperation*iTemp, iNode)
			SET_ENTITY_COORDS(objCars[iTemp], vPoint)
			SET_ROTATION_FOR_TRACK_PROGRESS(iTemp, iNode, g_fCoasterDist - fCarSeperation*iTemp)
		//ENDIF
	ENDREPEAT
ENDPROC

PROC UPDATE_COASTER_CAR(BOOL bSetTimeStep)
	//INT iTemp2
	//FLOAT fDistFromLastPoint
	
	SET_BIT(serverBD.iServerBitSet,ROLLERCOASTER_CONTROL_OF_CAR)
	
	IF bSetTimeStep
		IF previousNetworkTime != null		
			fTimeStep = GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(),previousNetworkTime) /1000.0
		ENDIF
		previousNetworkTime = GET_NETWORK_TIME_ACCURATE()
	ENDIF
	
	IF fTimeStep > 0.5
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,FREEZE_OCCURRED)
	ENDIF
	
	coasterCar.m_vPos = GET_POINT_OF_DISTANCE_ALONG_TRACK(g_fCoasterDist, coasterCar.iLastNodePassed) 	//grab current position based on distance
	
	FLOAT accel = GET_ACCELERATION_FROM_TRACK_POSITION()													//calc acceleration based on drop
	
	#IF IS_DEBUG_BUILD
	IF NOT bUseConstSpeed														//constant speed switch for debug
	#ENDIF
	
	IF coasterCar.iLastNodePassed < 20										
		IF g_fCoasterSpeed < CLIMB_SPEED
			g_fCoasterSpeed += 0.3
		ELSE
			g_fCoasterSpeed -= 0.3
		ENDIF
		IF ABSF(g_fCoasterSpeed-CLIMB_SPEED) < 0.3
			g_fCoasterSpeed = CLIMB_SPEED
		ENDIF
	ELSE
		g_fCoasterSpeed += (accel * fTimeStep)
		g_fCoasterSpeed *= 0.999999999										//throw on a little friction
	ENDIF
	
	#IF IS_DEBUG_BUILD
	ELSE
		g_fCoasterSpeed = fConstSpeed
	ENDIF
	#ENDIF
	
	println("am_rollercoaster - IS ",g_fCoasterDist," < ",trackNodes[1].m_fDist)
	println("am_rollercoaster - NEXT STEP IS ",(g_fCoasterDist + (g_fCoasterSpeed*fTimeStep)))
	IF (g_fCoasterDist < trackNodes[1].m_fDist)
	AND (g_fCoasterDist + (g_fCoasterSpeed*fTimeStep)) >= trackNodes[1].m_fDist
	AND playerBD[PARTICIPANT_ID_TO_INT()].iLapsDone >= NO_OF_LAPS
		println("am_rollercoaster - STOPPED")
		g_fCoasterDist = trackNodes[1].m_fDist
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_COASTER_STOPPED)
		println("am_rollercoaster - RESETTING START POSITION: ",g_fCoasterDist)
	ELSE
		g_fCoasterDist += g_fCoasterSpeed*fTimeStep
	ENDIF
	
	
	BOOL bLastNodeFound = FALSE
	INT iTemp

	IF g_fCoasterSpeed >=0													//update car moving forwards
		IF g_fCoasterDist >= trackNodes[iNumberOfNodes-1].m_fDist				//skip the car over the end start point
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,FREEZE_OCCURRED)
				g_fCoasterDist -= trackNodes[iNumberOfNodes-1].m_fDist				//Take out the full track distance from the coaster distance
			ELSE
				g_fCoasterDist = trackNodes[1].m_fDist
			ENDIF
			coasterCar.iLastNodePassed = 0											//Set the last node passed to 0
		ENDIF
		iTemp = GET_NEXT_NODE(coasterCar.iLastNodePassed)							//set first node to node infront of last node
		WHILE NOT bLastNodeFound													//loop all node
			#IF IS_DEBUG_BUILD
			IF bExtraDebug
				println("am_rollercoaster - g_fCoasterDist < trackNodes[iTemp].m_fDist ",g_fCoasterDist," < ",trackNodes[iTemp].m_fDist)
			ENDIF
			#ENDIF
			IF g_fCoasterDist < trackNodes[iTemp].m_fDist						//if nodes distance is further along than the coaster distance
				bLastNodeFound = TRUE												
				IF coasterCar.iLastNodePassed != iTemp-1
					IF trackNodes[iTemp-1].m_fSpeed != g_fCoasterSpeed
						g_fCoasterSpeed = trackNodes[iTemp-1].m_fSpeed
					ENDIF
				ENDIF
				coasterCar.iLastNodePassed = iTemp-1								//then the node just before this one was the last node passed
			ENDIF	
			iTemp = GET_NEXT_NODE(iTemp)											//if not then go to the next node
		ENDWHILE
	ELSE																		//update car moving backwards
		println("am_rollercoaster - Going backwards!!")
		IF g_fCoasterDist < 0													//skip car from 0th to last node
			g_fCoasterDist += trackNodes[iNumberOfNodes-1].m_fDist				//add the full distance on the track to the distance covered by the coaster if it goes back past 0
			coasterCar.iLastNodePassed = iNumberOfNodes-2							//set the last node to the node beofre the last one
		ENDIF
		iTemp = coasterCar.iLastNodePassed											//set first node to check to last one passed (as if going forwards)
		WHILE NOT bLastNodeFound													//loop all nodes
			IF  trackNodes[iTemp].m_fDist < g_fCoasterDist						//If track node distance being checked is less than the coaster distance
				bLastNodeFound = TRUE
				coasterCar.iLastNodePassed = iTemp									//then that node would have been the last one passed if going forward (as last node always refers to last one passed if car was going forwards)

			ENDIF
			iTemp = GET_PREVIOUS_NODE(iTemp)										//if not check the previous node
		ENDWHILE
	ENDIF
	
	SET_POSITION_OF_CARS()															//sets the position of the car objects based on their distance along the track
	
	CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,FREEZE_OCCURRED)
ENDPROC

PROC INIT_ROLLERCOASTER_SEATS()

	serverBD.rollercoasterPositions[0].vEntryPoint = <<-1644.3155, -1123.5304, 17.3447>>
	serverBD.rollercoasterPositions[1].vEntryPoint = <<-1644.9202, -1124.2810, 17.3447>>
	serverBD.rollercoasterPositions[2].vEntryPoint = <<-1645.8455, -1125.4132, 17.3447>>
	serverBD.rollercoasterPositions[3].vEntryPoint = <<-1646.5617, -1126.3025, 17.3447>>
	serverBD.rollercoasterPositions[4].vEntryPoint = <<-1647.4975, -1127.4381, 17.3447>>
	serverBD.rollercoasterPositions[5].vEntryPoint = <<-1648.2300, -1128.1843, 17.3447>>
	serverBD.rollercoasterPositions[6].vEntryPoint = <<-1649.2331, -1129.3989, 17.3447>>
	serverBD.rollercoasterPositions[7].vEntryPoint = <<-1649.9373, -1130.2032, 17.3447>>
	
	serverBD.rollercoasterPositions[0].vExitPoint = <<-1641.9138, -1125.2683, 17.3424>>
	serverBD.rollercoasterPositions[1].vExitPoint = <<-1642.6060, -1126.2400, 17.3424>>
	serverBD.rollercoasterPositions[2].vExitPoint = <<-1643.5735, -1127.3903, 17.3424>>
	serverBD.rollercoasterPositions[3].vExitPoint = <<-1644.2715, -1128.1997, 17.3424>>
	serverBD.rollercoasterPositions[4].vExitPoint = <<-1645.3427, -1129.3135, 17.3424>>
	serverBD.rollercoasterPositions[5].vExitPoint = <<-1645.9661, -1130.0671, 17.3424>>
	serverBD.rollercoasterPositions[6].vExitPoint = <<-1647.0225, -1131.2905, 17.3424>>
	serverBD.rollercoasterPositions[7].vExitPoint = <<-1647.6445, -1132.0157, 17.3424>>
	
	serverBD.rollercoasterPositions[0].vBackSeat = <<-1644.1531, -1125.4327, 18.3447>>
	serverBD.rollercoasterPositions[1].vBackSeat = <<-1645.7225, -1127.4076, 18.3447>>
	serverBD.rollercoasterPositions[2].vBackSeat = <<-1647.3153, -1129.3744, 18.3447>>
	serverBD.rollercoasterPositions[3].vBackSeat = <<-1648.9503, -1131.2986, 18.3447>>
	
ENDPROC


/////////////----------------------------------------------------------------------------------------------------------------------------
//////-----------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------- Queuing VVVV
//
//FUNC VECTOR GET_QUEUE_POSITION_VETOR(INT iPlaceInQueue)
//	
//	VECTOR vRet
//	
//	SWITCH iPlaceInQueue
//	
//		CASE 0 	RETURN <<-1651.911377,-1131.198730,17.342411>> BREAK
//		CASE 1 	RETURN <<-1652.373413,-1132.079590,16.879433>> BREAK
//		CASE 2 	RETURN <<-1653.028687,-1132.524414,16.419022>> BREAK
//		CASE 3 	RETURN <<-1653.510010,-1133.199951,15.954121>> BREAK
//		CASE 4 	RETURN <<-1654.312012,-1133.675903,15.493713>> BREAK
//		CASE 5 	RETURN <<-1654.527832,-1134.520264,15.028172>> BREAK
//		CASE 6 	RETURN <<-1655.336426,-1135.028320,14.796998>> BREAK
//		CASE 7 	RETURN <<-1656.297241,-1135.134033,14.796999>> BREAK
//		CASE 8 	RETURN <<-1656.581421,-1134.213623,14.796999>> BREAK
//		CASE 9 	RETURN <<-1657.081909,-1133.595337,14.796998>> BREAK
//		CASE 10 RETURN <<-1656.218140,-1133.088867,14.565842>> BREAK
//		CASE 11 RETURN <<-1655.930298,-1132.275024,14.105428>> BREAK
//		CASE 12 RETURN <<-1655.139404,-1131.819336,13.639887>> BREAK
//		CASE 13 RETURN <<-1654.587280,-1131.277344,13.180108>> BREAK
//		CASE 14 RETURN <<-1654.137817,-1130.517578,12.714576>> BREAK
//		CASE 15 RETURN <<-1653.625244,-1129.852539,12.248407>> BREAK
//		
//	ENDSWITCH
//	
//	RETURN vRet
//	
//ENDFUNC
//
//FUNC FLOAT GET_QUEUE_POSITION_HEADING(INT iPlaceInQueue)
//
//	VECTOR vRet
//	
//	SWITCH iPlaceInQueue
//	
//		CASE 0 	RETURN -32.085640 	BREAK
//		CASE 1 	RETURN -32.085636 	BREAK
//		CASE 2 	RETURN -29.220848 	BREAK
//		CASE 3 	RETURN -30.939720 	BREAK
//		CASE 4 	RETURN -28.074932 	BREAK
//		CASE 5 	RETURN -23.491268 	BREAK
//		CASE 6 	RETURN -64.171272 	BREAK
//		CASE 7 	RETURN -112.872681 	BREAK
//		CASE 8 	RETURN -131.207321 	BREAK
//		CASE 9 	RETURN 178.372391 	BREAK
//		CASE 10 RETURN 152.979721 	BREAK
//		CASE 11 RETURN 150.687897 	BREAK
//		CASE 12 RETURN 148.396072 	BREAK
//		CASE 13 RETURN 159.855225 	BREAK
//		CASE 14 RETURN 149.541962 	BREAK
//		CASE 15 RETURN 148.578552 	BREAK
//		
//	ENDSWITCH
//	
//	RETURN vRet
//	
//ENDFUNC
//
//FUNC INT GET_NUMBER_OF_PEDS_IN_QUEUE()
//	INT iTemp
//	INT iCount
//	REPEAT COUNT_OF(serverBD.theRollerCoaster.iPedsInQueue) iTemp
//		IF IS_NET_PLAYER_OK(theRollerCoaster.iPedsOnBoard[iTemp])
//			iCount++
//		ENDIF
//	ENDREPEAT
//	RETURN iCount
//ENDFUNC
//
//PROC JOIN_QUEUE(PLAYER_ID playerID)
//	IF IS_NET_PLAYER_OK(playerID)
//		INT iNextSpace = GET_NUMBER_OF_PEDS_IN_QUEUE()
//		IF iNextSpace < NUM_NETWORK_PLAYERS
//		AND NOT DOES_ENTITY_EXIST(GET_PLAYER_PED(theRollerCoaster.iPedsInQueue[iNextSpace]))
//			theRollerCoaster.iPedsInQueue[iNextSpace] = playerID
//		ENDIF
//	ENDIF
//ENDPROC
//
//FUNC BOOL IS_QUEUE_SPOT_FREE(INT iSpot)
//	IF NOT DOES_ENTITY_EXIST(GET_PLAYER_PED(theRollerCoaster.iPedsInQueue[iSpot]))
//	OR IS_ENTITY_DEAD(GET_PLAYER_PED(theRollerCoaster.iPedsInQueue[iSpot]))
//		RETURN FALSE
//	ENDIF
//	RETURN TRUE
//ENDFUNC
//
//PROC SHUFFLE_ALL_PEDS_ALONG_QUEUE()
//	INT iTemp
//	REPEAT COUNT_OF(theRollerCoaster.iPedsInQueue) iTemp
//		IF iTemp < NUM_NETWORK_PLAYERS-1
//			IF IS_QUEUE_SPOT_FREE(iTemp)
//			AND NOT IS_QUEUE_SPOT_FREE(iTemp+1)
//				theRollerCoaster.iPedsInQueue[iTemp] = theRollerCoaster.iPedsInQueue[iTemp+1]
//				theRollerCoaster.iPedsInQueue[iTemp]  = NULL
//			ENDIF
//		ENDIF
//	ENDREPEAT
//ENDPROC
//
//PROC UPDATE_QUEUE()
//
//	IF eRollerCoasterStage != ROLLER_COASTER_BOARDING
//	
//		SHUFFLE_ALL_PEDS_ALONG_QUEUE()
//		
//		INT iTemp
//		REPEAT COUNT_OF(theRollerCoaster.iPedsInQueue) iTemp
//			IF DOES_ENTITY_EXIST(GET_PLAYER_PED(theRollerCoaster.iPedsInQueue[iTemp]))
//				IF IS_NET_PLAYER_OK(theRollerCoaster.iPedsInQueue[iTemp])
//					IF IS_ENTITY_AT_COORD(theRollerCoaster.iPedsInQueue[iTemp], <<-1659.0100, -1143.1289, 17.4192>>, <<10, 10, 10>>)
//					
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDREPEAT
//		
//	ENDIF
//	
//ENDPROC

//---------------------------------------------------------------------------------------- Queuing ^^^^
//////-----------------------------------------------------------------------------------------------------
/////////////----------------------------------------------------------------------------------------------------------------------------


FUNC BOOL INIT_COASTER()
	RETURN TRUE
ENDFUNC

FUNC BOOL INIT_COASTER_CLIENT()
	int iTemp

	
	REQUEST_MODEL(cameraCarModel)
	REQUEST_MODEL(IND_PROP_DLC_ROLLER_CAR)
	REQUEST_MODEL(IND_PROP_DLC_ROLLER_CAR_02)
	REQUEST_ANIM_DICT(sFW_AnimDict)
	IF NOT HAS_MODEL_LOADED(cameraCarModel)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_ANIM_DICT_LOADED(sFW_AnimDict)
		RETURN FALSE
	ENDIF
	
	BROADCAST_SET_ROLLERCOASTER_HIDDEN(ALL_PLAYERS(TRUE))
	
	IF HAS_MODEL_LOADED(IND_PROP_DLC_ROLLER_CAR)
	AND HAS_MODEL_LOADED(IND_PROP_DLC_ROLLER_CAR_02)
		REPEAT COUNT_OF(objCars) iTemp
			INT iNode = GET_LAST_NODE_PASSED_FROM_DISTANCE(g_fCoasterDist - fCarSeperation*iTemp, coasterCar.iLastNodePassed)
			VECTOR vPoint = GET_POINT_OF_DISTANCE_ALONG_TRACK(g_fCoasterDist - fCarSeperation*iTemp, iNode)
			IF iTemp = 0
				objCars[0] = CREATE_OBJECT(IND_PROP_DLC_ROLLER_CAR,GET_TRACK_NODE_POSITION(1),FALSE,FALSE)
			ELSE
				objCars[iTemp] = CREATE_OBJECT(IND_PROP_DLC_ROLLER_CAR_02,vPoint,FALSE,FALSE)
				SET_ROTATION_FOR_TRACK_PROGRESS(iTemp, iNode, g_fCoasterDist - fCarSeperation*iTemp)
			ENDIF
			FREEZE_ENTITY_POSITION(objCars[iTemp], TRUE)
			SET_ENTITY_LOD_DIST(objCars[iTemp], 300)
			SET_ENTITY_INVINCIBLE(objCars[iTemp],TRUE)
			
//			iBarCarScene[0] = iBarCarScene[0]
//			sFW_AnimCarPlayer = sFW_AnimCarPlayer

			
		ENDREPEAT
		g_fCoasterDist = trackNodes[1].m_fDist
		UPDATE_COASTER_CAR(FALSE)
		REPEAT COUNT_OF(objCars) iTemp
			sFW_AnimCarPlayer = "idle_a_roller_car"
			IF IS_ENTITY_OK(objCars[iTemp])
				PLAY_ENTITY_ANIM(objCars[iTemp],sFW_AnimCarPlayer,sFW_AnimDict,NORMAL_BLEND_IN,TRUE,FALSE)
				println("am_rollercoaster -PLAY_ENTITY_ANIM - On Init")
			ENDIF
			println("am_rollercoaster - iTemp is", iTemp)
		ENDREPEAT
		
		INIT_ROLLERCOASTER_SEATS()
		SET_BIT(serverBD.iServerBitSet,DRAW_CLIENT_TIMER)
		previousNetworkTime = null 																		//to prevent jump when restarting coaster
		BROADCAST_REQUEST_ATTACH_REMOTE_PLAYER_TO_COASTER(ALL_PLAYERS_ON_SCRIPT(FALSE))
		BROADCAST_REQUEST_ROLLERCOASTER_DIST(ALL_PLAYERS_ON_SCRIPT(FALSE))	
		SET_POSITION_OF_CARS()
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


//height diff = 0.5548

PROC INIT_COASTER_PATH()

	PUSH_TRACK_NODE_WITH_POSITION(<<-1659.0100, -1143.1289, 17.4192>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1643.5242, -1124.6805, 17.4326>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1639.6210, -1120.0211, 17.6357>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1638.1987, -1118.3164, 17.9966>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1637.0107, -1116.8962, 18.5407>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1635.7722, -1115.4171, 19.2558>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1634.2271, -1113.5690, 20.1725>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1632.6921, -1111.7339, 21.0835>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1631.1788, -1109.9218, 21.9826>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1629.6918, -1108.1448, 22.8650>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1628.2432, -1106.4114, 23.7252>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1626.8402, -1104.7330, 24.5580>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1625.4911, -1103.1199, 25.3588>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1624.2059, -1101.5819, 26.1218>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1622.9917, -1100.1300, 26.8424>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1620.7211, -1097.4156, 28.1892>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1618.8663, -1095.1962, 29.2895>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1617.5326, -1093.6029, 30.0795>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1616.7781, -1092.6991, 30.5401>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1615.6774, -1091.3878, 30.9156>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1614.8295, -1090.3774, 31.0008>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1614.0109, -1089.4058, 30.9417>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1612.6145, -1087.7469, 30.3463>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1610.9922, -1085.8203, 29.1724>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1609.2275, -1083.7252, 27.9490>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1608.2947, -1082.6155, 27.4861>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1606.9371, -1081.0024, 27.4328>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1605.4711, -1079.2581, 27.5762>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1604.1593, -1077.7012, 28.0216>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1602.5111, -1075.7494, 28.5244>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1600.9319, -1073.8729, 28.9813>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1599.3423, -1071.9829, 29.1756>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1597.8506, -1070.0671, 29.1552>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1596.7230, -1067.9955, 29.0611>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1596.1230, -1065.7084, 28.9503>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1595.9913, -1063.3540, 28.8316>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1596.3649, -1061.0410, 28.7074>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1597.2539, -1058.8568, 28.5770>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1598.5615, -1056.8939, 28.4423>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1600.2698, -1055.2915, 28.3045>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1602.2876, -1054.0770, 28.1630>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1604.4968, -1053.2946, 28.0190>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1606.8450, -1053.0630, 27.8712>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1609.1934, -1053.2997, 27.7214>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1611.4155, -1054.0293, 27.5695>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1613.4320, -1055.2484, 27.4148>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1615.1665, -1056.8436, 27.2581>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1616.4861, -1058.7821, 27.0998>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1617.3711, -1060.9636, 26.9395>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1617.8032, -1063.2813, 26.7771>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1617.6693, -1065.6246, 26.6138>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1617.0708, -1067.9028, 26.4484>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1616.0057, -1069.9943, 26.2817>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1614.4891, -1071.7976, 26.1132>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1612.6460, -1073.2648, 25.9435>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1610.5234, -1074.2725, 25.7722>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1608.2314, -1074.8075, 25.5996>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1605.8754, -1074.8770, 25.4258>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1603.5756, -1074.3848, 25.2510>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1601.4167, -1073.4410, 25.0748>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1599.5076, -1072.0667, 24.8974>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1597.9614, -1070.2892, 24.7188>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1596.7981, -1068.2411, 24.5393>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1596.1215, -1065.9871, 24.3586>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1595.9456, -1063.6371, 24.1770>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1596.2417, -1061.3008, 23.9942>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1597.0789, -1059.0972, 23.8103>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1598.3450, -1057.1089, 23.6258>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1599.9965, -1055.4258, 23.4400>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1601.9915, -1054.1725, 23.2534>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1604.1952, -1053.3387, 23.0661>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1606.5326, -1053.0104, 22.8773>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1608.8809, -1053.1993, 22.6882>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1611.1442, -1053.8503, 22.4984>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1613.1995, -1055.0148, 22.3068>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1614.9824, -1056.5808, 22.1126>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1616.5448, -1058.3978, 21.7888>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1618.0975, -1060.2607, 21.3373>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1619.5831, -1062.0428, 20.7536>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1621.0585, -1063.8126, 20.1778>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1622.5345, -1065.5818, 19.6021>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1624.0093, -1067.3519, 19.0262>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1625.4822, -1069.1185, 18.4527>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1626.8800, -1070.8055, 17.9515>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1628.2180, -1072.4259, 17.7058>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1629.5092, -1073.9755, 17.7076>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1631.0459, -1075.8159, 17.7079>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1632.3602, -1077.3932, 17.7079>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1633.8970, -1079.2336, 17.7081>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1635.3969, -1080.9722, 17.7074>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1636.9238, -1082.8009, 17.7074>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1638.3828, -1084.5349, 17.8395>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1639.6444, -1086.0052, 18.3624>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1640.9854, -1087.5626, 19.3675>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1642.4822, -1089.2756, 20.5799>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1644.1083, -1091.0959, 21.5914>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1645.8442, -1092.9700, 21.9359>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1647.5609, -1094.7814, 21.6225>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1649.2391, -1096.5061, 20.9627>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1650.8937, -1098.1479, 20.1969>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1652.5349, -1099.7041, 19.5250>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1654.2482, -1101.2473, 18.9923>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1656.0504, -1102.7939, 18.5631>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1657.9113, -1104.3146, 18.2393>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1659.7985, -1105.7816, 18.0219>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1661.6807, -1107.1676, 17.9110>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1663.5251, -1108.4448, 17.9064>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1665.2932, -1109.5818, 18.0057>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1667.3165, -1110.7728, 18.2989>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1669.2629, -1111.8361, 18.8213>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1671.1436, -1112.7867, 19.5262>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1673.0222, -1113.6846, 20.3691>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1674.9579, -1114.5823, 21.2989>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1676.9952, -1115.5343, 22.2490>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1679.0840, -1116.4778, 23.1368>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1681.2194, -1117.3893, 23.9532>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1683.3738, -1118.2900, 24.6845>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1685.5168, -1119.2075, 25.3158>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1687.6200, -1120.1670, 25.8329>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1689.7048, -1121.1885, 26.2178>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1691.7719, -1122.3146, 26.5427>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1693.6351, -1123.7498, 26.8450>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1695.2539, -1125.4745, 27.1175>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1696.5809, -1127.4436, 27.3373>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1697.5736, -1129.6018, 27.5003>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1698.2067, -1131.8818, 27.6230>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1698.4651, -1134.2345, 27.7231>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1698.3444, -1136.6016, 27.7949>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1697.8413, -1138.9208, 27.8328>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1696.9724, -1141.1312, 27.8321>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1695.7592, -1143.1737, 27.7892>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1694.2338, -1144.9945, 27.7019>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1692.4349, -1146.5442, 27.5687>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1690.4153, -1147.7841, 27.3900>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1688.2257, -1148.6819, 27.1671>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1685.9261, -1149.2175, 26.9025>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1683.5758, -1149.3762, 26.5994>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1681.2373, -1149.1605, 26.2631>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1678.9684, -1148.5776, 25.8985>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1676.8254, -1147.6453, 25.5118>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1674.8622, -1146.3881, 25.1091>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1673.1237, -1144.8394, 24.6968>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1671.6537, -1143.0388, 24.2822>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1670.4862, -1141.0310, 23.8716>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1669.6494, -1138.8652, 23.4720>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1669.1626, -1136.5946, 23.0898>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1669.0399, -1134.2744, 22.7307>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1669.2836, -1131.9617, 22.4006>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1669.8879, -1129.7131, 22.1045>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1670.8413, -1127.5852, 21.8463>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1672.1199, -1125.6320, 21.6294>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1673.6940, -1123.9036, 21.4559>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1675.5234, -1122.4438, 21.3270>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1677.5629, -1121.2904, 21.2429>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1679.7617, -1120.4758, 21.2021>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1682.0636, -1120.0193, 21.2025>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1684.4103, -1119.9332, 21.2408>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1686.7423, -1120.2213, 21.3125>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1689.0004, -1120.8771, 21.4123>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1691.1276, -1121.8840, 21.5343>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1693.0688, -1123.2177, 21.6720>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1694.7787, -1124.8489, 21.8191>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1695.9298, -1126.3239, 21.9029>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1696.8777, -1127.9899, 21.9873>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1697.6743, -1129.8783, 22.0778>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1698.2920, -1131.9603, 22.1685>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1698.6986, -1134.2059, 22.2533>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1698.8660, -1136.5867, 22.3261>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1698.7638, -1139.0724, 22.3807>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1698.3630, -1141.6333, 22.4112>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1697.6333, -1144.2400, 22.4113>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1696.5461, -1146.8627, 22.3751>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1695.0607, -1149.4835, 22.2950>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1693.2386, -1151.8811, 22.1555>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1691.2246, -1153.8721, 21.9650>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1689.0741, -1155.4827, 21.7370>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1686.8419, -1156.7402, 21.4850>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1684.5834, -1157.6742, 21.2224>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1682.3508, -1158.3105, 20.9625>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1680.1613, -1158.6699, 20.7161>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1678.1755, -1158.8018, 20.5023>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1676.3438, -1158.7122, 20.3287>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1674.7552, -1158.4371, 20.2097>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1672.6060, -1157.6576, 20.0965>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1670.5844, -1156.4423, 19.9803>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1668.8311, -1154.8657, 19.8642>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1667.4183, -1152.9860, 19.7482>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1666.4520, -1150.8329, 19.6319>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1665.9114, -1148.5381, 19.5158>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1665.8174, -1146.1813, 19.3996>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1666.2070, -1143.8623, 19.2836>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1667.0726, -1141.6678, 19.1674>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1668.3390, -1139.6786, 19.0512>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1669.9618, -1137.9653, 18.9350>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1671.9126, -1136.6497, 18.8189>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1674.0874, -1135.7374, 18.7028>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1676.3966, -1135.2559, 18.5866>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1678.7510, -1135.2368, 18.4705>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1681.0581, -1135.7300, 18.3543>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1683.2302, -1136.6499, 18.2382>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1685.1871, -1137.9679, 18.1219>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1686.8236, -1139.6614, 18.0059>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1688.0814, -1141.6572, 17.8896>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1688.9384, -1143.8553, 17.7735>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1689.3594, -1146.1771, 17.6571>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1689.2595, -1148.5319, 17.5411>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1688.7097, -1150.8262, 17.4250>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1687.7325, -1152.9760, 17.3087>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1686.3417, -1154.8873, 17.1759>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1684.5730, -1156.4615, 17.0021>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1682.5402, -1157.6687, 16.7987>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1680.3135, -1158.4659, 16.5838>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1677.9725, -1158.7699, 16.3415>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1675.6257, -1158.6013, 16.0948>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1673.3607, -1157.9943, 15.9205>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1671.2545, -1156.9663, 15.8075>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1669.4353, -1155.5109, 15.7719>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1667.8477, -1153.6604, 15.7660>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1666.3301, -1151.8516, 15.7703>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1664.8750, -1150.1171, 15.8984>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1663.4597, -1148.4312, 16.1980>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1662.0331, -1146.7307, 16.6412>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1660.5557, -1144.9701, 17.1643>>)
	PUSH_TRACK_NODE_WITH_POSITION(<<-1659.0100, -1143.1289, 17.4192>>)
	
	SET_TRACK_NODE_POSITION(iNumberOfNodes-1, trackNodes[0].m_vPos)
	
	CALCULATE_NODE_DISTANCES()
	
	trackNodes[1].m_fSpeed = 0.3000
	trackNodes[2].m_fSpeed = 0.3000
	trackNodes[3].m_fSpeed = 3.0000
	trackNodes[4].m_fSpeed = 3.0000
	trackNodes[5].m_fSpeed = 3.0000
	trackNodes[6].m_fSpeed = 3.0000
	trackNodes[7].m_fSpeed = 3.0000
	trackNodes[8].m_fSpeed = 3.0000
	trackNodes[9].m_fSpeed = 3.0000
	trackNodes[10].m_fSpeed = 3.0000
	trackNodes[11].m_fSpeed = 3.0000
	trackNodes[12].m_fSpeed = 3.0000
	trackNodes[13].m_fSpeed = 3.0000
	trackNodes[14].m_fSpeed = 3.0000
	trackNodes[15].m_fSpeed = 3.0000
	trackNodes[16].m_fSpeed = 3.0000
	trackNodes[17].m_fSpeed = 3.0000
	trackNodes[18].m_fSpeed = 3.0000
	trackNodes[19].m_fSpeed = 3.0000
	trackNodes[20].m_fSpeed = 3.0000
	trackNodes[21].m_fSpeed = 3.0000
	trackNodes[22].m_fSpeed = 3.1794
	trackNodes[23].m_fSpeed = 4.8025
	trackNodes[24].m_fSpeed = 6.7585
	trackNodes[25].m_fSpeed = 8.3448
	trackNodes[26].m_fSpeed = 8.8436
	trackNodes[27].m_fSpeed = 8.9045
	trackNodes[28].m_fSpeed = 8.7073
	trackNodes[29].m_fSpeed = 8.1965
	trackNodes[30].m_fSpeed = 7.5921
	trackNodes[31].m_fSpeed = 7.0097
	trackNodes[32].m_fSpeed = 6.6959
	trackNodes[33].m_fSpeed = 6.7221
	trackNodes[34].m_fSpeed = 6.8771
	trackNodes[35].m_fSpeed = 7.0232
	trackNodes[36].m_fSpeed = 7.1800
	trackNodes[37].m_fSpeed = 7.3457
	trackNodes[38].m_fSpeed = 7.5605
	trackNodes[39].m_fSpeed = 7.6956
	trackNodes[40].m_fSpeed = 7.8806
	trackNodes[41].m_fSpeed = 8.0659
	trackNodes[42].m_fSpeed = 8.2597
	trackNodes[43].m_fSpeed = 8.4085
	trackNodes[44].m_fSpeed = 8.6081
	trackNodes[45].m_fSpeed = 8.7629
	trackNodes[46].m_fSpeed = 8.9647
	trackNodes[47].m_fSpeed = 9.1286
	trackNodes[48].m_fSpeed = 9.2889
	trackNodes[49].m_fSpeed = 9.4513
	trackNodes[50].m_fSpeed = 9.6107
	trackNodes[51].m_fSpeed = 9.8224
	trackNodes[52].m_fSpeed = 9.9849
	trackNodes[53].m_fSpeed = 10.1485
	trackNodes[54].m_fSpeed = 10.3112
	trackNodes[55].m_fSpeed = 10.4793
	trackNodes[56].m_fSpeed = 10.6480
	trackNodes[57].m_fSpeed = 10.7657
	trackNodes[58].m_fSpeed = 10.9378
	trackNodes[59].m_fSpeed = 11.1113
	trackNodes[60].m_fSpeed = 11.2850
	trackNodes[61].m_fSpeed = 11.4038
	trackNodes[62].m_fSpeed = 11.5785
	trackNodes[63].m_fSpeed = 11.7563
	trackNodes[64].m_fSpeed = 11.8826
	trackNodes[65].m_fSpeed = 12.0063
	trackNodes[66].m_fSpeed = 12.1858
	trackNodes[67].m_fSpeed = 12.3110
	trackNodes[68].m_fSpeed = 12.4905
	trackNodes[69].m_fSpeed = 12.6186
	trackNodes[70].m_fSpeed = 12.7520
	trackNodes[71].m_fSpeed = 12.9366
	trackNodes[72].m_fSpeed = 13.0690
	trackNodes[73].m_fSpeed = 13.2002
	trackNodes[74].m_fSpeed = 13.3271
	trackNodes[75].m_fSpeed = 13.5131
	trackNodes[76].m_fSpeed = 13.6428
	trackNodes[77].m_fSpeed = 13.8557
	trackNodes[78].m_fSpeed = 14.1467
	trackNodes[79].m_fSpeed = 14.7078
	trackNodes[80].m_fSpeed = 15.0933
	trackNodes[81].m_fSpeed = 15.4812
	trackNodes[82].m_fSpeed = 15.6897
	trackNodes[83].m_fSpeed = 16.0720
	trackNodes[84].m_fSpeed = 16.4288
	trackNodes[85].m_fSpeed = 16.6158
	trackNodes[86].m_fSpeed = 16.6150
	trackNodes[87].m_fSpeed = 16.6148
	trackNodes[88].m_fSpeed = 16.6148
	trackNodes[89].m_fSpeed = 16.6147
	trackNodes[90].m_fSpeed = 16.6151
	trackNodes[91].m_fSpeed = 16.6151
	trackNodes[92].m_fSpeed = 16.5645
	trackNodes[93].m_fSpeed = 16.1450
	trackNodes[94].m_fSpeed = 15.4464
	trackNodes[95].m_fSpeed = 14.6939
	trackNodes[96].m_fSpeed = 14.0775
	trackNodes[97].m_fSpeed = 13.7741
	trackNodes[98].m_fSpeed = 13.9723
	trackNodes[99].m_fSpeed = 14.3900
	trackNodes[100].m_fSpeed = 14.8988
	trackNodes[101].m_fSpeed = 15.3516
	trackNodes[102].m_fSpeed = 15.7118
	trackNodes[103].m_fSpeed = 15.9945
	trackNodes[104].m_fSpeed = 16.2069
	trackNodes[105].m_fSpeed = 16.3518
	trackNodes[106].m_fSpeed = 16.3944
	trackNodes[107].m_fSpeed = 16.3977
	trackNodes[108].m_fSpeed = 16.3236
	trackNodes[109].m_fSpeed = 16.1241
	trackNodes[110].m_fSpeed = 15.9292
	trackNodes[111].m_fSpeed = 15.4375
	trackNodes[112].m_fSpeed = 14.8409
	trackNodes[113].m_fSpeed = 14.2057
	trackNodes[114].m_fSpeed = 13.5752
	trackNodes[115].m_fSpeed = 12.7336
	trackNodes[116].m_fSpeed = 12.2095
	trackNodes[117].m_fSpeed = 11.5221
	trackNodes[118].m_fSpeed = 11.0986
	trackNodes[119].m_fSpeed = 10.6031
	trackNodes[120].m_fSpeed = 10.2269
	trackNodes[121].m_fSpeed = 9.9111
	trackNodes[122].m_fSpeed = 9.5337
	trackNodes[123].m_fSpeed = 9.2694
	trackNodes[124].m_fSpeed = 9.0583
	trackNodes[125].m_fSpeed = 8.8516
	trackNodes[126].m_fSpeed = 8.7288
	trackNodes[127].m_fSpeed = 8.6021
	trackNodes[128].m_fSpeed = 8.5100
	trackNodes[129].m_fSpeed = 8.4733
	trackNodes[130].m_fSpeed = 8.4742
	trackNodes[131].m_fSpeed = 8.5294
	trackNodes[132].m_fSpeed = 8.6157
	trackNodes[133].m_fSpeed = 8.7849
	trackNodes[134].m_fSpeed = 8.9639
	trackNodes[135].m_fSpeed = 9.2546
	trackNodes[136].m_fSpeed = 9.5140
	trackNodes[137].m_fSpeed = 9.8293
	trackNodes[138].m_fSpeed = 10.1114
	trackNodes[139].m_fSpeed = 10.5312
	trackNodes[140].m_fSpeed = 10.9067
	trackNodes[141].m_fSpeed = 11.1773
	trackNodes[142].m_fSpeed = 11.5836
	trackNodes[143].m_fSpeed = 11.9962
	trackNodes[144].m_fSpeed = 12.2748
	trackNodes[145].m_fSpeed = 12.6632
	trackNodes[146].m_fSpeed = 12.9328
	trackNodes[147].m_fSpeed = 13.1711
	trackNodes[148].m_fSpeed = 13.3959
	trackNodes[149].m_fSpeed = 13.6873
	trackNodes[150].m_fSpeed = 13.8612
	trackNodes[151].m_fSpeed = 14.0083
	trackNodes[152].m_fSpeed = 14.1297
	trackNodes[153].m_fSpeed = 14.2155
	trackNodes[154].m_fSpeed = 14.2732
	trackNodes[155].m_fSpeed = 14.3011
	trackNodes[156].m_fSpeed = 14.3008
	trackNodes[157].m_fSpeed = 14.2746
	trackNodes[158].m_fSpeed = 14.2251
	trackNodes[159].m_fSpeed = 14.1577
	trackNodes[160].m_fSpeed = 14.0733
	trackNodes[161].m_fSpeed = 13.9385
	trackNodes[162].m_fSpeed = 13.8385
	trackNodes[163].m_fSpeed = 13.7996
	trackNodes[164].m_fSpeed = 13.7301
	trackNodes[165].m_fSpeed = 13.6586
	trackNodes[166].m_fSpeed = 13.5902
	trackNodes[167].m_fSpeed = 13.5308
	trackNodes[168].m_fSpeed = 13.4820
	trackNodes[169].m_fSpeed = 13.4313
	trackNodes[170].m_fSpeed = 13.4127
	trackNodes[171].m_fSpeed = 13.4126
	trackNodes[172].m_fSpeed = 13.4330
	trackNodes[173].m_fSpeed = 13.4949
	trackNodes[174].m_fSpeed = 13.6046
	trackNodes[175].m_fSpeed = 13.7610
	trackNodes[176].m_fSpeed = 13.8937
	trackNodes[177].m_fSpeed = 14.1199
	trackNodes[178].m_fSpeed = 14.2919
	trackNodes[179].m_fSpeed = 14.4699
	trackNodes[180].m_fSpeed = 14.6443
	trackNodes[181].m_fSpeed = 14.7383
	trackNodes[182].m_fSpeed = 14.8890
	trackNodes[183].m_fSpeed = 14.9538
	trackNodes[184].m_fSpeed = 15.0334
	trackNodes[185].m_fSpeed = 15.1141
	trackNodes[186].m_fSpeed = 15.1923
	trackNodes[187].m_fSpeed = 15.2712
	trackNodes[188].m_fSpeed = 15.3499
	trackNodes[189].m_fSpeed = 15.4286
	trackNodes[190].m_fSpeed = 15.5073
	trackNodes[191].m_fSpeed = 15.5512
	trackNodes[192].m_fSpeed = 15.6299
	trackNodes[193].m_fSpeed = 15.7082
	trackNodes[194].m_fSpeed = 15.7859
	trackNodes[195].m_fSpeed = 15.8647
	trackNodes[196].m_fSpeed = 15.9434
	trackNodes[197].m_fSpeed = 16.0226
	trackNodes[198].m_fSpeed = 16.0645
	trackNodes[199].m_fSpeed = 16.1422
	trackNodes[200].m_fSpeed = 16.2213
	trackNodes[201].m_fSpeed = 16.2996
	trackNodes[202].m_fSpeed = 16.3783
	trackNodes[203].m_fSpeed = 16.4226
	trackNodes[204].m_fSpeed = 16.5003
	trackNodes[205].m_fSpeed = 16.5796
	trackNodes[206].m_fSpeed = 16.6622
	trackNodes[207].m_fSpeed = 16.7055
	trackNodes[208].m_fSpeed = 16.7837
	trackNodes[209].m_fSpeed = 16.8729
	trackNodes[210].m_fSpeed = 16.9907
	trackNodes[211].m_fSpeed = 17.0661
	trackNodes[212].m_fSpeed = 17.2127
	trackNodes[213].m_fSpeed = 17.3761
	trackNodes[214].m_fSpeed = 17.4689
	trackNodes[215].m_fSpeed = 17.5883
	trackNodes[216].m_fSpeed = 17.6648
	trackNodes[217].m_fSpeed = 17.6783
	trackNodes[218].m_fSpeed = 17.6822
	trackNodes[219].m_fSpeed = 17.6806
	trackNodes[220].m_fSpeed = 17.5908
	trackNodes[221].m_fSpeed = 17.3710
	trackNodes[222].m_fSpeed = 17.1986
	trackNodes[223].m_fSpeed = 16.8458

	
ENDPROC

FUNC BOOL IS_ROLLERCOASTER_UNLOADED()
	INT iTemp
	
	FOR iTemp = NUM_NETWORK_REAL_PLAYERS() TO 0 STEP -1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iTemp))
			IF playerBD[iTemp].eRollerCoasterStage != ROLLER_COASTER_UNLOADING
			OR IS_BIT_SET(playerBD[iTemp].iClientBitSet,PLAYER_IN_CAR)
			OR NOT IS_BIT_SET(playerBD[iTemp].iClientBitSet,RAISED_BAR)
			OR NOT IS_BIT_SET(playerBD[iTemp].iClientBitSet,BAR_SOUND_PLAYED)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN TRUE
	
ENDFUNC

PROC MAINTAIN_PLAYER_IN_SEAT()
	INT iTemp
	IF NOT IS_BIT_SET(iLocalBitSet,PLAYER_LOCATED_SEAT)
		REPEAT COUNT_OF(serverBD.rollercoasterPositions) iTemp
			println("am_rollercoaster - checking point: ",serverBD.rollercoasterPositions[iTemp].vEntryPoint)
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),serverBD.rollercoasterPositions[iTemp].vEntryPoint,<<0.3,0.3,1.5>>,FALSE)	
				IF NOT serverBD.bSeatIsOccupied[iTemp]
					coasterPositions.iCurrentSeat = iTemp
					SET_BIT(iLocalBitSet,PLAYER_LOCATED_SEAT)
					serverBd.bSeatIsOccupied[iTemp] = TRUE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

PROC CHECK_FOR_SCREAMING()
	
	IF iScreamXpThisRide < g_sMPTunables.irollercoasterridescreamrewardcap
		IF NETWORK_PLAYER_HAS_HEADSET(PLAYER_ID())
		
			DRAW_GENERIC_METER(iCurrentScreamDuration, MAX_SCREAM_LEVEL, "AMRC_SCRM_L")
			
			fVolume = NETWORK_GET_PLAYER_LOUDNESS(PLAYER_ID())
			
			CPRINTLN(DEBUG_AMBIENT, "Player volume is ", fVolume)
			
			IF fVolume > 0.0

				iCurrentScreamDuration++

				IF iCurrentScreamDuration >=  MAX_SCREAM_LEVEL
					// Award RP
					SET_XP_ANIM_TO_TRACK_PLAYER(TRUE)
					GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "AMRC_SCRM_R", XPTYPE_SKILL, XPCATEGORY_COMPLETED_ROLLERCOASTER, g_sMPTunables.irollercoasterridescreamrewardxp)
					iScreamXpThisRide += g_sMPTunables.irollercoasterridescreamrewardxp
					iCurrentScreamDuration = 0
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL LOAD_COASTER_SOUNDS(INT iStartOffset=0)
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_IN_CAR)
		IF IS_BIT_SET(iCoasterBS,COASTER_BS_LOADED_AMBIENT_RIDE)
			PRINTLN("LOAD_COASTER_SOUNDS clearing COASTER_BS_LOADED_AMBIENT_RIDE , before loading again")
			STOP_STREAM()
			CLEAR_BIT(iCoasterBS,COASTER_BS_LOADED_AMBIENT_RIDE)
		ENDIF
		IF NOT IS_BIT_SET(iCoasterBS,COASTER_BS_LOADED_PLAYER_RIDE)
			IF LOAD_STREAM_WITH_START_OFFSET("Player_Ride",0,"DLC_IND_ROLLERCOASTER_SOUNDS")
				SET_BIT(iCoasterBS,COASTER_BS_LOADED_PLAYER_RIDE)
				RESET_NET_TIMER(soundLoadFailsafe)
				PRINTLN("LOAD_COASTER_SOUNDS loaded Player_Ride stream")
			ELSE
				IF NOT HAS_NET_TIMER_STARTED(soundLoadFailsafe)
					START_NET_TIMER(soundLoadFailsafe,TRUE)
				ELIF HAS_NET_TIMER_EXPIRED(soundLoadFailsafe,5000,TRUE)
					SET_BIT(iCoasterBS,COASTER_BS_LOADED_PLAYER_RIDE)
					RESET_NET_TIMER(soundLoadFailsafe)
					PRINTLN("LOAD_COASTER_SOUNDS waiting for Player_Ride stream TIMEOUT!!")
				ENDIF
				PRINTLN("LOAD_COASTER_SOUNDS waiting for Player_Ride stream")
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ELSE
		IF IS_BIT_SET(iCoasterBS,COASTER_BS_LOADED_PLAYER_RIDE)
			PRINTLN("LOAD_COASTER_SOUNDS clearing COASTER_BS_LOADED_PLAYER_RIDE, before loading again")
			STOP_STREAM()
			CLEAR_BIT(iCoasterBS,COASTER_BS_LOADED_PLAYER_RIDE)
		ENDIF
		IF NOT IS_BIT_SET(iCoasterBS,COASTER_BS_LOADED_AMBIENT_RIDE)
			IF LOAD_STREAM_WITH_START_OFFSET("Ambient_Ride",iStartOffset,"DLC_IND_ROLLERCOASTER_SOUNDS")
				SET_BIT(iCoasterBS,COASTER_BS_LOADED_AMBIENT_RIDE)
				RESET_NET_TIMER(soundLoadFailsafe)
				PRINTLN("LOAD_COASTER_SOUNDS loaded Ambient_Ride stream")
			ELSE
				IF NOT HAS_NET_TIMER_STARTED(soundLoadFailsafe)
					START_NET_TIMER(soundLoadFailsafe,TRUE)
				ELIF HAS_NET_TIMER_EXPIRED(soundLoadFailsafe,5000,TRUE)
					RESET_NET_TIMER(soundLoadFailsafe)
					SET_BIT(iCoasterBS,COASTER_BS_LOADED_AMBIENT_RIDE)
					PRINTLN("LOAD_COASTER_SOUNDS waiting for Ambient_Ridestream TIMEOUT!!")
				ENDIF
				PRINTLN("LOAD_COASTER_SOUNDS waiting for Ambient_Ride stream")
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	println("am_rollercoaster - LOADING SOUND at offset: ",coasterRunningTime)
	RETURN FALSE
ENDFUNC

PROC SET_ROLLERCOASTER_CAM_ENABLED(BOOL bActive=TRUE)
	IF bActive
		cameraCar = CREATE_VEHICLE(cameraCarModel,<<-1622.1541, -1089.8063, 4.3360>>, 57.5266,FALSE,FALSE)
		SET_ENTITY_VISIBLE(cameraCar,FALSE)
		SET_ENTITY_COLLISION(cameraCar,FALSE)
		SET_ENTITY_INVINCIBLE(cameraCar,TRUE)
		IF IS_VEHICLE_DRIVEABLE(cameraCar)
			ATTACH_ENTITY_TO_ENTITY(cameraCar,objCars[coasterPositions.iCurrentSeat/2],0,coasterPositions.vCarPlayerOffset,<<0.0, 0.0, 180.0>>)
		ENDIF
	ELSE
		DELETE_VEHICLE(cameraCar)
	ENDIF
ENDPROC

PROC MAINTAIN_ROLLERCOASTER_CAM()
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	IF IS_VEHICLE_DRIVEABLE(cameraCar)
		SET_IN_VEHICLE_CAM_STATE_THIS_UPDATE(cameraCar,CAM_INSIDE_VEHICLE)
	ENDIF
ENDPROC

PROC SET_PLAYER_IN_COASTER_CAR()
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_IN_CAR)
		VECTOR vCarOffset
		INT iLocalSceneID
		FLOAT vSeatOffseat
		
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
		
		IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
			CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PUTTING_PLAYER_IN_CAR)
			EXIT
		ENDIF
		
		SWITCH(eEnterAnimStage)
			CASE ENTER_WALK_TO_POINT
				println("am_rollercoaster - ENTER_WALK_TO_POINT")
				
				println("am_rollercoaster - current seat: ",coasterPositions.iCurrentSeat)
				IF coasterPositions.iCurrentSeat % 2 != 0												//Player in back seat of car
					coasterPositions.vCarPlayerOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(objCars[coasterPositions.iCurrentSeat/2],serverBD.rollercoasterPositions[coasterPositions.iCurrentSeat/2].vBackSeat)
					vSeatOffseat = 0.0
				ELSE
					coasterPositions.vCarPlayerOffset = <<0.0,0.0,1.0>>
					vSeatOffseat = -1.0170
					println("am_rollercoaster - ADDED FRONT OFFSET ",vCarPosition.y)
				ENDIF
				
				vCarPosition = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objCars[coasterPositions.iCurrentSeat/2],<<0.0,vSeatOffseat,0.0>>)
				
				NET_SET_PLAYER_CONTROL(PLAYER_ID(),FALSE, NSPC_CLEAR_TASKS  | NSPC_LEAVE_CAMERA_CONTROL_ON )
				//SET_PLAYER_CONTROL(PLAYER_ID(),FALSE,SPC_LEAVE_CAMERA_CONTROL_ON|SPC_REMOVE_EXPLOSIONS|SPC_DEACTIVATE_GADGETS)
				SET_ROLLERCOASTER_PASSIVE_MODE(TRUE)

			
				playerBD[PARTICIPANT_ID_TO_INT()].iCarOrder = serverBD.rollercoasterPositions[coasterPositions.iCurrentSeat].iNumOccupants
			
				sFW_AnimPlayer = "enter_player_"
				sFW_AnimPlayer2 = "idle_a_player_"
				IF playerBD[PARTICIPANT_ID_TO_INT()].iCarOrder = 0
					sFW_AnimPlayer += "one"
					sFW_AnimPlayer2+= "one"
				ELSE
					sFW_AnimPlayer += "two"
					sFW_AnimPlayer2+= "two"
				ENDIF
				
				BROADCAST_MODIFY_COASTER_OCCUPANTS(ALL_PLAYERS_ON_SCRIPT(FALSE),coasterPositions.iCurrentSeat)
				
				TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(),serverBD.rollercoasterPositions[coasterPositions.iCurrentSeat].vEntryPoint, PEDMOVE_WALK, -1, 229.3511, 0.2)
				println("am_rollercoaster - WALKING TO COORD")
				
				//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].g_bUsingRollerCoaster = TRUE
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bUsingRollerCoaster)
				PRINTLN("am_rollercoaster - g_bUsingRollerCoaster = TRUE")
				NETWORK_DISABLE_LEAVE_REMOTE_PED_BEHIND(TRUE)
				
				SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW_IF_NEEDED()
				
				eEnterAnimStage = ENTER_PLAY_ANIM
				
			BREAK
			CASE ENTER_PLAY_ANIM
				println("am_rollercoaster - ENTER_PLAY_ANIM")
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_GO_STRAIGHT_TO_COORD) != WAITING_TO_START_TASK
				AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_GO_STRAIGHT_TO_COORD) != PERFORMING_TASK
					iEnterScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCarPosition,<<0.0,0.0,139.96>>,DEFAULT,TRUE)
					println("am_rollercoaster - ENTER_PLAY_ANIM - NETWORK_CREATE_SYNCHRONISED_SCENE")
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(),iEnterScene,sFW_AnimDict,sFW_AnimPlayer,NORMAL_BLEND_IN,NORMAL_BLEND_OUT,SYNCED_SCENE_NET_DISREGARD_ATTACHMENT_CHECKS)
					println("am_rollercoaster - ENTER_PLAY_ANIM - NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE")
					NETWORK_START_SYNCHRONISED_SCENE(iEnterScene)
					println("am_rollercoaster - ENTER_PLAY_ANIM - NETWORK_START_SYNCHRONISED_SCENE")
					
					eEnterAnimStage = ENTER_ANIM_PLAYING
				ENDIF
			BREAK
			CASE ENTER_ANIM_PLAYING
				sFW_AnimPlayer2 = sFW_AnimPlayer2
				println("am_rollercoaster - ENTER_ANIM_PLAYING")
				iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iEnterScene)
				IF iLocalSceneID != -1
					IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) > 0.99
						iEnterScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCarPosition,<<0.0,0.0,139.96>>,DEFAULT,DEFAULT,TRUE)
						println("am_rollercoaster - ENTER_ANIM_PLAYING - NETWORK_CREATE_SYNCHRONISED_SCENE")
						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(),iEnterScene,sFW_AnimDict,sFW_AnimPlayer2,NORMAL_BLEND_IN,NORMAL_BLEND_OUT,SYNCED_SCENE_NET_DISREGARD_ATTACHMENT_CHECKS)
						println("am_rollercoaster - ENTER_ANIM_PLAYING - NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE")
						NETWORK_START_SYNCHRONISED_SCENE(iEnterScene)
						println("am_rollercoaster - ENTER_ANIM_PLAYING - NETWORK_START_SYNCHRONISED_SCENE")
						
						eEnterAnimStage = ENTER_IN_CAR
					ENDIF
				ENDIF
			BREAK
			CASE ENTER_IN_CAR
				println("am_rollercoaster - ENTER_IN_CAR ",coasterPositions.iCurrentSeat)
				vCarOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(objCars[coasterPositions.iCurrentSeat/2],GET_ENTITY_COORDS(PLAYER_PED_ID()))
				ATTACH_ENTITY_TO_ENTITY(PLAYER_PED_ID(),objCars[coasterPositions.iCurrentSeat/2],0,vCarOffset,<<0.0, 0.0, GET_ENTITY_HEADING(PLAYER_PED_ID())-139.96>>)
				NETWORK_ALLOW_REMOTE_ATTACHMENT_MODIFICATION(PLAYER_PED_ID(),TRUE)
				BROADCAST_ATTACH_REMOTE_PLAYER_TO_COASTER(ALL_PLAYERS_ON_SCRIPT(FALSE),coasterPositions.iCurrentSeat/2,vCarOffset,<<0.0, 0.0, 180.0>>)
				SET_ROLLERCOASTER_CAM_ENABLED(TRUE)
				println("am_rollercoaster - SET_ROLLERCOASTER_CAM_ENABLED(TRUE) ",coasterPositions.iCurrentSeat)
				ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_X)
				coasterPositions.vLocalExitPoint = serverBD.rollercoasterPositions[coasterPositions.iCurrentSeat].vExitPoint
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_IN_CAR)
				CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PUTTING_PLAYER_IN_CAR)
				
				eEnterAnimStage = ENTER_WALK_TO_POINT
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC REMOVE_PLAYER_FROM_COASTER_CAR()
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_IN_CAR)
		IF IS_ENTITY_OK(PLAYER_PED_ID())
			INT iLocalSceneID
			
			SWITCH(eExitAnimStage)
				CASE EXIT_BAR_RAISE_ANIM
					println("am_rollercoaster - EXIT_BAR_RAISE_ANIM")
				
					IF IS_ENTITY_ATTACHED(PLAYER_PED_ID())
						DETACH_ENTITY(PLAYER_PED_ID())
					ENDIF
					BROADCAST_DETACH_REMOTE_PLAYER_FROM_COASTER(ALL_PLAYERS_ON_SCRIPT(FALSE))
					
					sFW_AnimBarPlayer = "safety_bar_exit_player_"
					IF playerBD[PARTICIPANT_ID_TO_INT()].iCarOrder = 0
						sFW_AnimBarPlayer += "one"
					ELSE
						sFW_AnimBarPlayer +="two"
					ENDIF
					
					BROADCAST_MODIFY_COASTER_OCCUPANTS(ALL_PLAYERS_ON_SCRIPT(FALSE),coasterPositions.iCurrentSeat)
					
					iBarPlayerScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCarPosition,<<0.0,0.0,139.96>>,DEFAULT,TRUE)
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(),iBarPlayerScene,sFW_AnimDict,sFW_AnimBarPlayer,NORMAL_BLEND_IN,NORMAL_BLEND_OUT,SYNCED_SCENE_NET_DISREGARD_ATTACHMENT_CHECKS)
					NETWORK_START_SYNCHRONISED_SCENE(iBarPlayerScene)
				
					eExitAnimStage = EXIT_PLAY_ANIM
				BREAK
				CASE EXIT_PLAY_ANIM
					println("am_rollercoaster - EXIT_PLAY_ANIM")
					iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iBarPlayerScene)
					IF iLocalSceneID != -1
						IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) > 0.99
							
							sFW_AnimPlayer = "exit_player_"
							IF playerBD[PARTICIPANT_ID_TO_INT()].iCarOrder = 0
								sFW_AnimPlayer += "one"
							ELSE
								sFW_AnimPlayer += "two"
							ENDIF
								
							iEnterScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCarPosition,<<0.0,0.0,139.96>>,DEFAULT,TRUE)
							NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(),iEnterScene,sFW_AnimDict,sFW_AnimPlayer,NORMAL_BLEND_IN,NORMAL_BLEND_OUT,SYNCED_SCENE_NET_DISREGARD_ATTACHMENT_CHECKS)
							NETWORK_START_SYNCHRONISED_SCENE(iEnterScene)
							
							eExitAnimStage = EXIT_OUT_OF_CAR
						ENDIF
					ENDIF
				BREAK
				
				CASE EXIT_OUT_OF_CAR
					println("am_rollercoaster - EXIT_OUT_OF_CAR")
					iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iEnterScene)
					IF iLocalSceneID != -1
						IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) > 0.99

							CLEAR_PED_TASKS(PLAYER_PED_ID()) 
							IF IS_ENTITY_OK(PLAYER_PED_ID()) 
								NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
							ENDIF
							
							SET_ROLLERCOASTER_PASSIVE_MODE(FALSE)
							SET_ROLLERCOASTER_CAM_ENABLED(FALSE)
							CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_IN_CAR)
							CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,REMOVING_PLAYER_FROM_CAR)
							CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bUsingRollerCoaster)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].g_bUsingRollerCoaster = FALSE
							PRINTLN("am_rollercoaster - g_bUsingRollerCoaster = FALSE")
							NETWORK_DISABLE_LEAVE_REMOTE_PED_BEHIND(FALSE)
							
							SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(buttonMovie)
							CLEAR_BIT(iLocalBitSet,BUTTONS_REQUESTED)
							bRefreshButtons = TRUE
							
							eExitAnimStage = EXIT_BAR_RAISE_ANIM
							SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_RIDE_FAIRGROUND_RIDE)
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_BAR_ANIMATIONS()
	//INT iLocalSceneID,
	INT iTemp
	SWITCH (eBarAnimStage)
		CASE BAR_PLAY_LOWER_ANIM
			sFW_AnimCarPlayer = "safety_bar_enter_roller_car"
			REPEAT COUNT_OF(objCars) iTemp
				PLAY_ENTITY_ANIM(objCars[iTemp],sFW_AnimCarPlayer,sFW_AnimDict,NORMAL_BLEND_IN,FALSE,TRUE)
				println("am_rollercoaster -PLAY_ENTITY_ANIM - On Start")
			ENDREPEAT
			PLAY_SOUND_FROM_ENTITY(-1,"Bar_Lower_And_Lock",objCars[1],"DLC_IND_ROLLERCOASTER_SOUNDS")
			
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_IN_CAR)
				println("am_rollercoaster - sFW_AnimBarPlayer: ",sFW_AnimBarPlayer)
				sFW_AnimBarPlayer = "safety_bar_enter_player_"
				IF playerBD[PARTICIPANT_ID_TO_INT()].iCarOrder = 0
					sFW_AnimBarPlayer += "one"
				ELSE
					sFW_AnimBarPlayer += "two"
				ENDIF
				println("am_rollercoaster - sFW_AnimBarPlayer: ",sFW_AnimBarPlayer)
								
				TASK_PLAY_ANIM(PLAYER_PED_ID(),sFW_AnimDict,sFW_AnimBarPlayer,NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_HOLD_LAST_FRAME)
			ENDIF
			
			eBarAnimStage = BAR_PLAYER_IDLE
		BREAK
		CASE BAR_PLAYER_IDLE
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_IN_CAR)
				IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(),sFW_AnimDict,sFW_AnimBarPlayer)
					IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(),sFW_AnimDict,sFW_AnimBarPlayer) > 0.2
					
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_PREPARED)
						eBarAnimStage = BAR_PLAY_LOWER_ANIM
					ENDIF
				ENDIF
			ELSE
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_PREPARED)
				eBarAnimStage = BAR_PLAY_LOWER_ANIM
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC PROCESS_RUNNING_ANIMATIONS
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,REFRESH_ANIMATION)
		println("am_rollercoaster - REFRESHING RUNNING ANIMS")
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_HANDS_RAISED)
			IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(),sFW_AnimDict,"hands_up_idle_a_player_one")
			AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(),sFW_AnimDict,"hands_up_idle_a_player_two")
				SWITCH(eHandsAnimStage)
					CASE HANDS_RAISE
						sFW_AnimBarPlayer = "hands_up_enter_player_"
						IF playerBD[PARTICIPANT_ID_TO_INT()].iCarOrder = 0
							sFW_AnimBarPlayer += "one"
						ELSE
							sFW_AnimBarPlayer += "two"
						ENDIF
						
						TASK_PLAY_ANIM(PLAYER_PED_ID(),sFW_AnimDict,sFW_AnimBarPlayer,NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_HOLD_LAST_FRAME)
						
						eHandsAnimStage = HANDS_IDLE
					BREAK
					CASE HANDS_IDLE
						println("am_rollercoaster - CASE HANDS IDLE")
						IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(),sFW_AnimDict,sFW_AnimBarPlayer)
							println("am_rollercoaster - PLAYING HANDS IDLE - got localsceneid")
							IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(),sFW_AnimDict,sFW_AnimBarPlayer) > 0.99
								println("am_rollercoaster - PLAYING HANDS IDLE - previous anim ended")
								sFW_AnimBarPlayer = "hands_up_idle_a_player_"
								IF playerBD[PARTICIPANT_ID_TO_INT()].iCarOrder = 0
									sFW_AnimBarPlayer += "one"
								ELSE
									sFW_AnimBarPlayer += "two"
								ENDIF
							
								println("am_rollercoaster - PLAYING HANDS IDLE -",sFW_AnimBarPlayer)

								TASK_PLAY_ANIM(PLAYER_PED_ID(),sFW_AnimDict,sFW_AnimBarPlayer,NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
								
								eHandsAnimStage = HANDS_RAISE
								CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,REFRESH_ANIMATION)
								bRefreshButtons = TRUE
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		ELSE
			IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(),sFW_AnimDict,"hands_up_idle_a_player_one")
			OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(),sFW_AnimDict,"hands_up_idle_a_player_two")
				sFW_AnimBarPlayer = "hands_up_exit_player_"
				IF playerBD[PARTICIPANT_ID_TO_INT()].iCarOrder = 0
					sFW_AnimBarPlayer += "one"
				ELSE
					sFW_AnimBarPlayer += "two"
				ENDIF
			
				TASK_PLAY_ANIM(PLAYER_PED_ID(),sFW_AnimDict,sFW_AnimBarPlayer,NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_HOLD_LAST_FRAME)
			ELSE
				IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(),sFW_AnimDict,sFW_AnimBarPlayer)
					IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(),sFW_AnimDict,sFW_AnimBarPlayer) > 0.99
						sFW_AnimBarPlayer = "safety_bar_grip_move_a_player_"
						IF playerBD[PARTICIPANT_ID_TO_INT()].iCarOrder = 0
							sFW_AnimBarPlayer += "one"
						ELSE
							sFW_AnimBarPlayer += "two"
						ENDIF

						TASK_PLAY_ANIM(PLAYER_PED_ID(),sFW_AnimDict,sFW_AnimBarPlayer,NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
						CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,REFRESH_ANIMATION)
						bRefreshButtons = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_HANDS()
	IF NOT IS_PAUSE_MENU_ACTIVE()
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_X) //CONTROL_ACTION
		AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(),sFW_AnimDict,"hands_up_enter_player_one")
		AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(),sFW_AnimDict,"hands_up_enter_player_two")
		AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(),sFW_AnimDict,"hands_up_exit_player_one")
		AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(),sFW_AnimDict,"hands_up_exit_player_two")					
			#IF IS_DEBUG_BUILD
				println("am_rollercoaster - IS_CONTROL_JUST_PRESSED")
			#ENDIF
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_HANDS_RAISED)
				CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_HANDS_RAISED)
				println("am_rollercoaster - CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_HANDS_RAISED)")
			ELSE
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_HANDS_RAISED)
				println("am_rollercoaster - SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_HANDS_RAISED)")
			ENDIF
			bRefreshButtons = TRUE
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,REFRESH_ANIMATION)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL ARE_PLAYERS_READY()
	INT iTemp
	
	FOR iTemp = NUM_NETWORK_REAL_PLAYERS() TO 0 STEP -1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iTemp))
			IF IS_BIT_SET(playerBD[iTemp].iClientBitSet,PUTTING_PLAYER_IN_CAR)
			OR IS_BIT_SET(playerBD[iTemp].iClientBitSet,REMOVING_PLAYER_FROM_CAR)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL ARE_PLAYERS_PREPARED()
	INT iTemp
	
	FOR iTemp = NUM_NETWORK_REAL_PLAYERS() TO 0 STEP -1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iTemp))
			IF NOT IS_BIT_SET(playerBD[iTemp].iClientBitSet,PLAYER_PREPARED)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL SHOULD_COASTER_SCRIPT_RUN()
	INT iTemp
	REPEAT NUM_NETWORK_PLAYERS iTemp
		PLAYER_INDEX playerID = INT_TO_PLAYERINDEX(iTemp)
		IF IS_NET_PLAYER_OK(playerID, FALSE, TRUE)
			IF IS_ENTITY_AT_COORD(GET_PLAYER_PED(playerID), <<-1659.0100, -1143.1289, 17.4192>>, <<500, 500, 500>>)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

//Helper function to get a clients game/mission state
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iGameState
ENDFUNC

//Helper function to get the servers game/mission state
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC

//----------------------
//	EVENT PROCESSING
//----------------------

//Purpose: Syncs the current distance of the rollercoaster with the server
PROC PROCESS_SYNC_ROLLERCOASTER_DIST(INT iEventID)
	NET_NL() NET_PRINT("     ----->    PROCESS_SYNC_ROLLERCOASTER_DIST( - called...") NET_NL()

	SCRIPT_EVENT_DATA_SYNC_ROLLERCOASTER_DIST Event	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
		g_fCoasterDist = Event.fDist
		g_fCoasterSpeed = Event.fSpeed
		coasterRunningTime = Event.iTime
		playerBD[PARTICIPANT_ID_TO_INT()].iLapsDone = Event.iLapsDone
		println("am_rollercoaster - SETTING SOUND at offset: ",coasterRunningTime)
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,LOAD_AND_PLAY_SOUND)
		PRINTLN("     ----->    PROCESS_SYNC_ROLLERCOASTER_DIST ",Event.fDist,"( - SENT FROM ", GET_PLAYER_NAME(Event.Details.FromPlayerIndex)," at ",GET_CLOUD_TIME_AS_INT())
	ELSE
		SCRIPT_ASSERT("PROCESS_SYNC_ROLLERCOASTER_DIST( - could not retreive data.")		
	ENDIF
ENDPROC

//Purpose: Syncs the current distance of the rollercoaster with the server
PROC PROCESS_REQUEST_ROLLERCOASTER_DIST(INT iEventID)

	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()

		NET_NL() NET_PRINT("     ----->    PROCESS_REQUEST_ROLLERCOASTER_DIST( - called...") NET_NL()

		SCRIPT_EVENT_DATA_REQUEST_ROLLERCOASTER_DIST Event	
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
			BROADCAST_SYNC_ROLLERCOASTER_DIST(SPECIFIC_PLAYER(Event.Details.FromPlayerIndex),playerBD[PARTICIPANT_ID_TO_INT()].iLapsDone)
			PRINTLN("     ----->    PROCESS_REQUEST_ROLLERCOASTER_DIST ( - SENT FROM ", GET_PLAYER_NAME(Event.Details.FromPlayerIndex)," at ",GET_CLOUD_TIME_AS_INT())
		ELSE
			SCRIPT_ASSERT("PROCESS_REQUEST_ROLLERCOASTER_DIST( - could not retreive data.")		
		ENDIF
		
	ENDIF
ENDPROC

//Purpose: Request all players send an attach event
PROC PROCESS_REQUEST_ATTACH_REMOTE_PLAYER_TO_COASTER(INT iEventID)
	NET_NL() NET_PRINT("     ----->    PROCESS_REQUEST_ATTACH_REMOTE_PLAYER_TO_COASTER( - called...") NET_NL()

	SCRIPT_EVENT_DATA_REQUEST_ATTACH_REMOTE_PLAYER_TO_COASTER Event	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_IN_CAR)
			NETWORK_ALLOW_REMOTE_ATTACHMENT_MODIFICATION(PLAYER_PED_ID(),TRUE)
			BROADCAST_ATTACH_REMOTE_PLAYER_TO_COASTER(SPECIFIC_PLAYER(Event.Details.FromPlayerIndex),coasterPositions.iCurrentSeat/2,coasterPositions.vCarPlayerOffset,<<0.0, 0.0, 180.0>>)
			PRINTLN("     ----->    PROCESS_REQUEST_ATTACH_REMOTE_PLAYER_TO_COASTER ( - SENT FROM ", GET_PLAYER_NAME(Event.Details.FromPlayerIndex)," at ",GET_CLOUD_TIME_AS_INT())
		ENDIF
	ELSE
		SCRIPT_ASSERT("PROCESS_REQUEST_ATTACH_REMOTE_PLAYER_TO_COASTER( - could not retreive data.")		
	ENDIF
ENDPROC

//Purpose: Attach a remote player to the local rollercoaster car
PROC PROCESS_ATTACH_REMOTE_PLAYER_TO_COASTER(INT iEventID)
	NET_NL() NET_PRINT("     ----->    PROCESS_ATTACH_REMOTE_PLAYER_TO_COASTER( - called...") NET_NL()

	SCRIPT_EVENT_DATA_ATTACH_REMOTE_PLAYER_TO_COASTER Event	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
		PED_INDEX playerPed = GET_PLAYER_PED(Event.Details.FromPlayerIndex)
		IF IS_NET_PLAYER_OK(Event.Details.FromPlayerIndex,TRUE,TRUE)
			NETWORK_ALLOW_REMOTE_ATTACHMENT_MODIFICATION(playerPed,TRUE)
			ATTACH_ENTITY_TO_ENTITY(playerPed, objCars[Event.iCarIndex], 0, Event.vOffset, Event.vRotation)
			PRINTLN("     ----->    PROCESS_ATTACH_REMOTE_PLAYER_TO_COASTER ( - SENT FROM ", GET_PLAYER_NAME(Event.Details.FromPlayerIndex)," at ",GET_CLOUD_TIME_AS_INT())
		ENDIF
	ELSE
		SCRIPT_ASSERT("PROCESS_ATTACH_REMOTE_PLAYER_TO_COASTER( - could not retreive data.")		
	ENDIF
ENDPROC

//Purpose: Detach a remote player from the local Ferris wheel car
PROC PROCESS_DETACH_REMOTE_PLAYER_FROM_COASTER(INT iEventID)
	NET_NL() NET_PRINT("     ----->    PROCESS_DETACH_REMOTE_PLAYER_FROM_COASTER( - called...") NET_NL()

	SCRIPT_EVENT_DATA_DETACH_REMOTE_PLAYER_FROM_COASTER Event	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
		PED_INDEX playerPed = GET_PLAYER_PED(Event.Details.FromPlayerIndex)
		IF IS_ENTITY_OK(playerPed)
			DETACH_ENTITY(playerPed,FALSE)
			//NETWORK_ALLOW_REMOTE_ATTACHMENT_MODIFICATION(playerPed,FALSE)
			PRINTLN("     ----->    PROCESS_DETACH_REMOTE_PLAYER_FROM_COASTER ( - SENT FROM ", GET_PLAYER_NAME(Event.Details.FromPlayerIndex)," at ",GET_CLOUD_TIME_AS_INT())
		ENDIF
	ELSE
		SCRIPT_ASSERT("PROCESS_DETACH_REMOTE_PLAYER_FROM_COASTER( - could not retreive data.")		
	ENDIF
ENDPROC

//Purpose: Tell the server when a player enter/exits coaster
PROC PROCESS_MODIFY_COASTER_OCCUPANTS(INT iEventID)

	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()

		NET_NL() NET_PRINT("     ----->    PROCESS_MODIFY_COASTER_OCCUPANTS( - called...") NET_NL()

		SCRIPT_EVENT_DATA_MODIFY_COASTER_OCCUPANTS Event	
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
			IF IS_BIT_SET(playerBD[NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(Event.Details.FromPlayerIndex))].iClientBitSet,PLAYER_IN_CAR)
				serverBD.rollercoasterPositions[event.iSeatIndex].iNumOccupants--
			ELSE	
				serverBD.rollercoasterPositions[event.iSeatIndex].iNumOccupants++
			ENDIF
			PRINTLN("     ----->    PROCESS_MODIFY_COASTER_OCCUPANTS ( - SENT FROM ", GET_PLAYER_NAME(Event.Details.FromPlayerIndex)," at ",GET_CLOUD_TIME_AS_INT())

		ELSE
			SCRIPT_ASSERT("PROCESS_MODIFY_COASTER_OCCUPANTS( - could not retreive data.")		
		ENDIF
	
	ENDIF
ENDPROC

PROC PROCESS_ROLLERCOASTER_EVENTS()

	INT iEventID
	EVENT_NAMES ThisScriptEvent
	
	STRUCT_EVENT_COMMON_DETAILS Details


		REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iEventID
		
			ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iEventID)
						
			SWITCH ThisScriptEvent			
			
				CASE EVENT_NETWORK_SCRIPT_EVENT	
					GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Details, SIZE_OF(Details))	
					IF IS_NET_PLAYER_OK(Details.FromPlayerIndex, FALSE)
						IF NETWORK_IS_PLAYER_A_PARTICIPANT(Details.FromPlayerIndex)
							SWITCH Details.Type
								CASE SCRIPT_EVENT_REQUEST_ATTACH_REMOTE_PLAYER_TO_COASTER
									NET_PRINT("PROCESS_SCRIPT_EVENT - received SCRIPT_EVENT_REQUEST_ATTACH_REMOTE_PLAYER_TO_COASTER") NET_NL()
									PROCESS_REQUEST_ATTACH_REMOTE_PLAYER_TO_COASTER(iEventID)				
								BREAK							
								CASE SCRIPT_EVENT_ATTACH_REMOTE_PLAYER_TO_COASTER
									NET_PRINT("PROCESS_SCRIPT_EVENT - received SCRIPT_EVENT_ATTACH_REMOTE_PLAYER_TO_COASTER") NET_NL()
									PROCESS_ATTACH_REMOTE_PLAYER_TO_COASTER(iEventID)				
								BREAK	
								CASE SCRIPT_EVENT_DETACH_REMOTE_PLAYER_FROM_COASTER
									NET_PRINT("PROCESS_SCRIPT_EVENT - received SCRIPT_EVENT_DETACH_REMOTE_PLAYER_TO_COASTER") NET_NL()
									PROCESS_DETACH_REMOTE_PLAYER_FROM_COASTER(iEventID)				
								BREAK									
								CASE SCRIPT_EVENT_SYNC_ROLLERCOASTER_DIST
									NET_PRINT("PROCESS_SCRIPT_EVENT - received SCRIPT_EVENT_SYNC_ROLLERCOASTER_DIST") NET_NL()
									PROCESS_SYNC_ROLLERCOASTER_DIST(iEventID)				
								BREAK
								CASE SCRIPT_EVENT_REQUEST_ROLLERCOASTER_DIST
									NET_PRINT("PROCESS_SCRIPT_EVENT - received SCRIPT_EVENT_REQUEST_ROLLERCOASTER_DIST") NET_NL()
									PROCESS_REQUEST_ROLLERCOASTER_DIST(iEventID)				
								BREAK
								CASE SCRIPT_EVENT_MODIFY_COASTER_OCCUPANTS
									NET_PRINT("PROCESS_SCRIPT_EVENT - received SCRIPT_EVENT_MODIFY_COASTER_OCCUPANTS") NET_NL()
									PROCESS_MODIFY_COASTER_OCCUPANTS(iEventID)				
								BREAK	
							ENDSWITCH
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		ENDREPEAT
ENDPROC


FUNC BOOL PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)
	
	println("[MJM] - am_rollercoaster START")
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission), missionScriptArgs)
	
	//Reserve Entities
	RESERVE_NETWORK_MISSION_OBJECTS(1)
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD,SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD,SIZE_OF(playerBD))
	
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	g_fCoasterDist = 0
	g_fCoasterSpeed = 0
	
	playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_AWAIT_ACTIVATION
	
	RETURN TRUE
	
ENDFUNC

PROC PROCESS_ROLLERCOASTER_CLIENT
	
	#IF IS_DEBUG_BUILD 
		RUN_TRACK_WIDGET()
	#ENDIF
	
	// Temp fix for url:bugstar:2454570
	MPGlobalsAmbience.bPossibleToBoardRollerCoaster = FALSE
	
	SWITCH playerBD[PARTICIPANT_ID_TO_INT()].eRollerCoasterStage		
	
		CASE ROLLER_COASTER_IN_STATION
			IF serverBD.eRollerCoasterStage >= ROLLER_COASTER_BOARDING								//If server on next stage, move on
					playerBD[PARTICIPANT_ID_TO_INT()].eRollerCoasterStage = ROLLER_COASTER_BOARDING
					println("am_rollercoaster - STAGE: BOARDING")
			ENDIF
		BREAK
		
		CASE ROLLER_COASTER_BOARDING
			IF serverBD.eRollerCoasterStage >= ROLLER_COASTER_PREPARING								//If server on next stage, move on
				playerBD[PARTICIPANT_ID_TO_INT()].eRollerCoasterStage = ROLLER_COASTER_PREPARING
				IF IS_BIT_SET(iCoasterBS,COASTER_BS_LOADED_PLAYER_RIDE)
				OR IS_BIT_SET(iCoasterBS,COASTER_BS_LOADED_AMBIENT_RIDE)
					PRINTLN("PROCESS_ROLLERCOASTER_CLIENT clearing stream before loading again")
					STOP_STREAM()
					CLEAR_BIT(iCoasterBS,COASTER_BS_LOADED_PLAYER_RIDE)
					CLEAR_BIT(iCoasterBS,COASTER_BS_LOADED_AMBIENT_RIDE)
				ENDIF
				println("am_rollercoaster - STAGE: PREPARING")
			ELSE
				IF IS_BIT_SET(iLocalBitSet,PLAYER_LOCATED_SEAT)
					IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PUTTING_PLAYER_IN_CAR)
						SET_PLAYER_IN_COASTER_CAR()
					ENDIF		
					
					IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(),serverBD.rollercoasterPositions[coasterPositions.iCurrentSeat].vEntryPoint,<<0.3,0.3,1.5>>,FALSE)
						IF IS_HELP_MESSAGE_BEING_DISPLAYED()
							CLEAR_HELP()
						ENDIF
						IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PUTTING_PLAYER_IN_CAR)
						AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_IN_CAR)
							serverBd.bSeatIsOccupied[coasterPositions.iCurrentSeat] = FALSE
							CLEAR_BIT(iLocalBitSet,PLAYER_LOCATED_SEAT)
							coasterPositions.iCurrentSeat = -1
						ENDIF
					ELSE
						IF NOT IS_PHONE_ONSCREEN()
						AND NOT IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
						AND NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_TIME_TRIAL)
						AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
						AND NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
						AND NOT IS_BIT_SET(serverBD.iServerBitSet,BLOCK_PLAYER_ENTRY)
						AND NOT IS_PED_WEARING_JUGGERNAUT_SUIT(PLAYER_PED_ID())
						AND NOT IS_PLAYER_AN_ANIMAL(PLAYER_ID())
							IF NOT IS_PLAYER_ACTIONING_ANY_CORONA_INVITE()
								IF serverBD.rollercoasterPositions[coasterPositions.iCurrentSeat].iNumOccupants < MAX_CAR_OCCUPANTS
								
									IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
										PRINT_HELP_WITH_NUMBER("AMRC_ENTER",g_sMptunables.irollercoasterridecost)
									ENDIF
									
									// Temp fix for url:bugstar:2454570
									MPGlobalsAmbience.bPossibleToBoardRollerCoaster = TRUE
									
									IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL,INPUT_CONTEXT)	
										IF GET_LOCAL_PLAYER_VC_AMOUNT(TRUE) >= g_sMptunables.irollercoasterridecost
										OR NETWORK_CAN_SPEND_MONEY(g_sMptunables.irollercoasterridecost,FALSE,FALSE,TRUE) //Use OR to catch cash values over SCRIPT_MAX_INT32.
											
											IF USE_SERVER_TRANSACTIONS()
												// Trigger a cash transaction for PC Build (2150728)
												INT iScriptTransactionIndex
												TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_FAIRGROUND, g_sMPTunables.irollercoasterridecost, iScriptTransactionIndex, FALSE, TRUE)
												g_cashTransactionData[iScriptTransactionIndex].cashInfo.iLocation = RIDE_ID
											ELSE
												NETWORK_BUY_FAIRGROUND_RIDE(g_sMptunables.irollercoasterridecost,RIDE_ID,FALSE,TRUE)
											ENDIF
											SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PUTTING_PLAYER_IN_CAR)
										ELSE
											IF NOT IS_BIT_SET(iLocalBitSet, STORE_ACTIVE)
												LAUNCH_STORE_CASH_ALERT()
												SET_BIT(iLocalBitSet, STORE_ACTIVE)
												//NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, TRUE, FALSE)
												PRINTLN("am_rollercoaster - LAUNCH_STORE_CASH_ALERT - CALLED")
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF (GET_GAME_TIMER() % 1000) < 50
									println("am_rollercoaster - IS_PLAYER_ACTIONING_ANY_CORONA_INVITE - Access Blocked.")
								ENDIF
							ENDIF
						ELSE
							IF IS_HELP_MESSAGE_BEING_DISPLAYED()
								CLEAR_HELP()
							ENDIF
						ENDIF
					ENDIF	
				ENDIF
				IF IS_BIT_SET(serverBD.iServerBitSet,DRAW_CLIENT_TIMER)
					println("am_rollercoaster - IF IS_BIT_SET(serverBD.iServerBitSet,DRAW_CLIENT_TIMER)")
					DRAW_TIMER()
				ENDIF
				MAINTAIN_PLAYER_IN_SEAT()
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,LOAD_AND_PLAY_SOUND)	
			ENDIF
		BREAK
		
		CASE ROLLER_COASTER_PREPARING
			IF serverBD.eRollerCoasterStage >= ROLLER_COASTER_RUNNING							//If server on next stage, move on
				IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,LOAD_AND_PLAY_SOUND)
					IF LOAD_COASTER_SOUNDS(coasterRunningTime)
						IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_IN_CAR)
							PLAY_STREAM_FROM_PED(PLAYER_PED_ID())
							bRefreshButtons = TRUE
						ELSE
							PLAY_STREAM_FROM_OBJECT(objCars[2])
						ENDIF
						coasterRunningTime = 0
						#IF IS_DEBUG_BUILD
						bRunRide = TRUE
						#ENDIF
						CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_PREPARED)
						playerBD[PARTICIPANT_ID_TO_INT()].eRollerCoasterStage = ROLLER_COASTER_RUNNING
						println("am_rollercoaster - STAGE: RUNNING")
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_PREPARED)
					PROCESS_BAR_ANIMATIONS()
				ENDIF
			ENDIF
		BREAK
		
		CASE ROLLER_COASTER_RUNNING
			IF serverBD.eRollerCoasterStage = ROLLER_COASTER_UNLOADING
				PLAY_SOUND_FROM_ENTITY(-1,"Ride_Stop",objCars[1],"DLC_IND_ROLLERCOASTER_SOUNDS")
				previousNetworkTime = null 																		//to prevent jump when restarting coaster
				g_fCoasterSpeed = 0.3
				iTempNode = -1
				iCurrentScreamDuration = 0
				iScreamXpThisRide = 0
				playerBD[PARTICIPANT_ID_TO_INT()].iLapsDone = 0
				CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,READY_FOR_UNLOADING)
				CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_COASTER_STOPPED)
				CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,REFRESH_ANIMATION)
				IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_IN_CAR)
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,REMOVING_PLAYER_FROM_CAR)
				ENDIF
				SET_POSITION_OF_CARS() // Set twice to disable push physics
				SET_POSITION_OF_CARS()
				playerBD[PARTICIPANT_ID_TO_INT()].eRollerCoasterStage = ROLLER_COASTER_UNLOADING
				println("am_rollercoaster - STAGE: UNLOADING")
			ELSE
				IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_IN_CAR)
					IF iTempNode!= -1
						IF coasterCar.iLastNodePassed != iTempNode
						AND coasterCar.iLastNodePassed = 22	//Just over the initial slope
							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,REFRESH_ANIMATION)
						ENDIF
					ENDIF
					iTempNode = coasterCar.iLastNodePassed

					MAINTAIN_ROLLERCOASTER_CAM()
					PROCESS_INSTRUCTIONAL_BUTTONS()	
					CHECK_FOR_SCREAMING()
					PROCESS_RUNNING_ANIMATIONS()

					IF NOT IS_BIT_SET(iLocalBitSet,HELP_SHOWN)
						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
							PRINT_HELP("AMRC_HELPTEXT")
							SET_BIT(iLocalBitSet,HELP_SHOWN)
						ENDIF
					ENDIF
					
					PROCESS_HANDS()

					IF coasterCar.iLastNodePassed = 0
						IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_HANDS_RAISED)
							CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_HANDS_RAISED)
							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,REFRESH_ANIMATION)
							bRefreshButtons = TRUE
						ENDIF
						DISABLE_CONTROL_ACTION(FRONTEND_CONTROL,INPUT_FRONTEND_X)
					ENDIF
				ENDIF
				
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,READY_FOR_UNLOADING)
					IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_COASTER_STOPPED)		//Don't move anymore once stopped
						UPDATE_COASTER_CAR(TRUE)
					ELSE
						CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,LAP_COUNTED)
						println("am_rollercoaster - CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,LAP_COUNTED)")
					ENDIF
					IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,LAP_COUNTED)	//If this lap hasn't been counted already
						println("am_rollercoaster - NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,LAP_COUNTED")
						IF coasterCar.iLastNodePassed = 1 
							println("am_rollercoaster - coasterCar.iLastNodePassed = 1")
							IF playerBD[PARTICIPANT_ID_TO_INT()].iLapsDone >= NO_OF_LAPS 			//Done required lap(s)
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,READY_FOR_UNLOADING)
								println("am_rollercoaster - SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,READY_FOR_UNLOADING")
								#IF IS_DEBUG_BUILD 
									bRunRide = FALSE
								#ENDIF
							ELSE
								playerBD[PARTICIPANT_ID_TO_INT()].iLapsDone++						//Count the current lap
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,LAP_COUNTED)
								println("am_rollercoaster - playerBD[PARTICIPANT_ID_TO_INT()].iLapsDone = ",playerBD[PARTICIPANT_ID_TO_INT()].iLapsDone)
							ENDIF
						ENDIF
					ELSE
						IF coasterCar.iLastNodePassed > 1
							CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,LAP_COUNTED)
							println("am_rollercoaster - CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,LAP_COUNTED)")
						ENDIF
					ENDIF

				ENDIF
			ENDIF
			
//			#IF IS_DEBUG_BUILD 
//				IF bStep
//				OR bRunRide
//														//Maintain the ride movement					
//					IF bStep
//						bStep = FALSE
//					ENDIF
//				ENDIF				
//			#ENDIF

		BREAK
		
		CASE ROLLER_COASTER_UNLOADING
			IF serverBD.eRollerCoasterStage = ROLLER_COASTER_BOARDING
				CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,RAISED_BAR)
				CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,BAR_SOUND_PLAYED)
				playerBD[PARTICIPANT_ID_TO_INT()].eRollerCoasterStage = ROLLER_COASTER_BOARDING
				#IF IS_DEBUG_BUILD
					println("am_rollercoaster - STAGE: BOARDING")
				#ENDIF
			ELSE 
				IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,REMOVING_PLAYER_FROM_CAR)
					REMOVE_PLAYER_FROM_COASTER_CAR()
				ENDIF
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,RAISED_BAR)
					sFW_AnimCarPlayer = "safety_bar_exit_roller_car"
					INT iTemp
					REPEAT COUNT_OF(objCars) iTemp
						PLAY_ENTITY_ANIM(objCars[iTemp],sFW_AnimCarPlayer,sFW_AnimDict,NORMAL_BLEND_IN,FALSE,TRUE)
						println("am_rollercoaster -PLAY_ENTITY_ANIM - On End")
					ENDREPEAT
					RESET_NET_TIMER(barSoundTimer)
					PLAY_SOUND_FROM_ENTITY(-1,"Bar_Unlock_And_Raise",objCars[1],"DLC_IND_ROLLERCOASTER_SOUNDS")
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,RAISED_BAR)
				ENDIF
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,RAISED_BAR)
			AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,BAR_SOUND_PLAYED)
				IF HAS_NET_TIMER_EXPIRED(barSoundTimer,BAR_SOUND_DELAY)
					PLAY_SOUND_FROM_ENTITY(-1,"Bar_Unlock_And_Raise",objCars[1],"DLC_IND_ROLLERCOASTER_SOUNDS")
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,BAR_SOUND_PLAYED)
				ENDIF
			ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC PROCESS_ROLLERCOASTER_SERVER


	SWITCH serverBD.eRollerCoasterStage
		CASE ROLLER_COASTER_IN_STATION
			println("am_rollercoaster - SET_BIT(serverBD.iServerBitSet,DRAW_CLIENT_TIMER)")
			serverBD.eRollerCoasterStage = ROLLER_COASTER_BOARDING
		BREAK
		
		CASE ROLLER_COASTER_BOARDING
			IF IS_BIT_SET(serverBD.iServerBitSet,ROLLERCOASTER_HAS_BEEN_BOARDED)	//Timer run out, start the ride
				iCurrentCoasterTimer = COASTER_TIME
				SET_BIT(serverBD.iServerBitSet,DRAW_CLIENT_TIMER)
				println("am_rollercoaster - SET_BIT(serverBD.iServerBitSet,DRAW_CLIENT_TIMER)")
				CLEAR_BIT(serverBD.iServerBitSet,ROLLERCOASTER_HAS_BEEN_BOARDED)
				CLEAR_BIT(serverBD.iServerBitSet,TIMER_RESET)
				CLEAR_BIT(serverBD.iServerBitSet,BLOCK_PLAYER_ENTRY)
				serverBD.eRollerCoasterStage = ROLLER_COASTER_PREPARING
			ELSE
				IF NOT IS_BIT_SET(serverBD.iServerBitSet,TIMER_RESET)				//Initialising
					RESET_NET_TIMER(serverBd.iCoasterTimer)
					SET_BIT(serverBD.iServerBitSet,TIMER_RESET)
				ELSE
					println("Current time is :", COASTER_TIME-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBd.iCoasterTimer))
					IF (COASTER_TIME-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBd.iCoasterTimer)) < 3000
					AND NOT((COASTER_TIME-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBd.iCoasterTimer)) < 0)
						SET_BIT(serverBD.iServerBitSet,BLOCK_PLAYER_ENTRY)
						println("am_rollercoaster - SET_BIT(serverBD.iServerBitSet,BLOCK_PLAYER_ENTRY)")
					ENDIF
					
					IF HAS_NET_TIMER_EXPIRED(serverBd.iCoasterTimer, iCurrentCoasterTimer)
						IF ARE_PLAYERS_READY()
							println("am_rollercoaster - Players are ready")
							SET_BIT(serverBD.iServerBitSet,DRAW_CLIENT_TIMER)
							SET_BIT(serverBD.iServerBitSet,ROLLERCOASTER_HAS_BEEN_BOARDED)
						ELSE
							println("am_rollercoaster - Players are NOT ready")
							iCurrentCoasterTimer = 1000
							CLEAR_BIT(serverBD.iServerBitSet,DRAW_CLIENT_TIMER)
							CLEAR_BIT(serverBD.iServerBitSet,TIMER_RESET)
							println("am_rollercoaster - CLEAR_BIT(serverBD.iServerBitSet,DRAW_CLIENT_TIMER)")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
		BREAK
		
		CASE ROLLER_COASTER_PREPARING
			IF ARE_PLAYERS_PREPARED()
				serverBD.eRollerCoasterStage = ROLLER_COASTER_RUNNING
			ENDIF
		BREAK
		
		CASE ROLLER_COASTER_RUNNING
			INT iTemp
			
			IF coasterCar.iLastNodePassed = 1
			
				SET_BIT(serverBD.iServerBitSet,UNLOAD)
			
				FOR iTemp = NUM_NETWORK_REAL_PLAYERS() TO 0 STEP -1
					println("am_rollercoaster - IN PARTICIPANT LOOP: ",iTemp)
					IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iTemp))
						println("am_rollercoaster - NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iTemp))")
						println("am_rollercoaster - PLAYER NAME: ",GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iTemp))))
						println("am_rollercoaster - SCTV: ",IS_PLAYER_SCTV(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iTemp))))
						println("am_rollercoaster - PLAYER IN CAR: ",IS_BIT_SET(playerBD[iTemp].iClientBitSet,PLAYER_IN_CAR))
						println("am_rollercoaster - READY_FOR_UNLOADING: ",IS_BIT_SET(playerBD[iTemp].iClientBitSet,READY_FOR_UNLOADING))
						IF NOT IS_BIT_SET(playerBD[iTemp].iClientBitSet,READY_FOR_UNLOADING)
							CLEAR_BIT(serverBD.iServerBitSet,UNLOAD)
							println("am_rollercoaster - CLEAR_BIT(serverBD.iServerBitSet,UNLOAD)")
						ENDIF
					ENDIF
				ENDFOR
				
				IF IS_BIT_SET(serverBD.iServerBitSet,UNLOAD)
					println("am_rollercoaster - IS_BIT_SET(serverBD.iServerBitSet,UNLOAD)")
					serverBD.eRollerCoasterStage = ROLLER_COASTER_UNLOADING
					CLEAR_BIT(serverBD.iServerBitSet,UNLOAD)
				ENDIF
			ENDIF
		BREAK
		
		CASE ROLLER_COASTER_UNLOADING
			IF IS_ROLLERCOASTER_UNLOADED()
				serverBD.eRollerCoasterStage = ROLLER_COASTER_BOARDING
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC


SCRIPT(MP_MISSION_DATA missionScriptArgs)

	BOOL isInMultiplayer = NETWORK_IS_GAME_IN_PROGRESS()
	
	IF(isInMultiplayer)
		
		// Carry out all the initial game starting duties. 
		IF NOT PROCESS_PRE_GAME(missionScriptArgs)
			SCRIPT_CLEANUP()
		ENDIF
		
		DELETE_TRACK()
		INIT_COASTER_PATH()

	ENDIF
	
	WHILE(TRUE)
	
		MP_LOOP_WAIT_ZERO()
	
	
		// If we have a match end event, bail
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			SCRIPT_CLEANUP()
			PRINTLN("AM_ROLLER - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
		ENDIF
		IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<<-1645.5549, -1123.8719, 17.3436>>,<<ciFAIRGROUND_END_DISTANCE, ciFAIRGROUND_END_DISTANCE, ciFAIRGROUND_END_DISTANCE>>,FALSE)
		OR IS_PLAYER_IN_CORONA()
		OR NETWORK_IS_ACTIVITY_SESSION()
		OR (AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE() )
		#IF IS_DEBUG_BUILD OR bEndScript #ENDIF
			PRINTLN("AM_ROLLER - DO CLEANUP")
			SCRIPT_CLEANUP()
		ENDIF

		// -----------------------------------
		// Process your game logic.....
		
		PROCESS_ROLLERCOASTER_EVENTS()
		
		IF IS_BIT_SET(iLocalBitSet, STORE_ACTIVE)
			IF NOT g_sShopSettings.bProcessStoreAlert
				CLEAR_BIT(iLocalBitSet, STORE_ACTIVE)
				//NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				PRINTLN("am_rollercoaster - LAUNCH_STORE_CASH_ALERT - CLEARED")
			ENDIF
		ENDIF
		
//		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,LOAD_AND_PLAY_SOUND)
//			println("am_rollercoaster - LOADING SOUND at offset: ",coasterRunningTime)
//			IF LOAD_COASTER_SOUNDS(coasterRunningTime)
//				PLAY_STREAM_FROM_OBJECT(objCars[1])
//				println("am_rollercoaster - PLAYING STREAM NOW at offset: ",coasterRunningTime)
//				CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,LOAD_AND_PLAY_SOUND)
//			ENDIF
//		ENDIF
		
		IF(bWarp)
			bWarp = FALSE
			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1649.6675, -1129.0533, 17.3438>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 332.3180)
		ENDIF

		SWITCH GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT())	
		
			CASE GAME_STATE_AWAIT_ACTIVATION
				IF GET_SERVER_MISSION_STATE() > GAME_STATE_AWAIT_ACTIVATION
						playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_INIT
				ENDIF
			BREAK
			
			CASE GAME_STATE_INIT
				IF GET_SERVER_MISSION_STATE() > GAME_STATE_INIT
					IF INIT_COASTER_CLIENT()
						playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_READY
					ENDIF
				ENDIF
			BREAK
			
			CASE GAME_STATE_READY
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_READY
				
					PROCESS_ROLLERCOASTER_CLIENT()
					
				ENDIF
			BREAK
		ENDSWITCH		
		
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
			SWITCH GET_SERVER_MISSION_STATE()
			
				CASE GAME_STATE_AWAIT_ACTIVATION
					IF SHOULD_COASTER_SCRIPT_RUN()
						serverBD.iServerGameState = GAME_STATE_INIT
					ENDIF
				BREAK
				
				CASE GAME_STATE_INIT
					IF INIT_COASTER()
						serverBD.iServerGameState = GAME_STATE_READY
					ENDIF
				BREAK
				
				CASE GAME_STATE_READY
				
					PROCESS_ROLLERCOASTER_SERVER()
									
				BREAK
				
			ENDSWITCH
		
		ENDIF
		
		
	ENDWHILE
	
ENDSCRIPT
