//////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_GA_PICKUPS.sc														//
// Description: Maintains the Gang Attack pickups once the Gang Attack has ended.		//
// Written by:  Ryan Baker																//
// Date: 30/07/2013																		//
//////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_event.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"
USING "net_mission.sch"
USING "net_scoring_common.sch"
USING "net_ambience.sch"
USING "commands_object.sch"
USING "net_xp_animation.sch"
USING "net_hideout.sch"

USING "net_wait_zero.sch"

USING "net_synced_ambient_pickups.sch"

#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF

// Game States. 
CONST_INT GAME_STATE_INI 				0
CONST_INT GAME_STATE_RUNNING			1
CONST_INT GAME_STATE_LEAVE				2
CONST_INT GAME_STATE_TERMINATE_DELAY	3
CONST_INT GAME_STATE_END				4

//CONSTANTS
CONST_INT	LEAVE_RANGE		200

//Server Bitset Data
CONST_INT biS_AnyCrewArrested		0

// The server broadcast data.
// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData
	INT iServerGameState
		
	INT iServerBitSet
	
	DESTRUCTIBLE_CRATE_DATA DCrateData[MAX_NUM_DESTRUCTIBLE_CRATES]
		
	VECTOR vLocation
		
	INT iInstance
		
	SCRIPT_TIMER MissionTerminateDelayTimer
ENDSTRUCT
ServerBroadcastData serverBD


CONST_INT biP_PickedUpBox			0
CONST_INT biP_FinishedRage			1

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	INT iGameState
	
	INT iPlayerBitSet
		
	#IF IS_DEBUG_BUILD
		INT iDebugPassFail = 0
	#ENDIF
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

///// MISSION VARIABLES /////
//INT iBoolsBitSet
//CONST_INT biInitialSetup			0


// Widgets & Debug.
#IF IS_DEBUG_BUILD
	BOOL bHostEndMissionNow
#ENDIF

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MAIN PROCS                            ////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Do necessary pre game start ini.
// Returns FALSE if the script fails to receive an initial network broadcast.
FUNC BOOL PROCESS_PRE_GAME( GANG_ATTACK_PICKUPS_DATA missionScriptArgs )
	
	// This marks the script as a net script, and handles any instancing setup.
	MP_MISSION_DATA MpMissionData
	MpMissionData.mdID.idMission = missionScriptArgs.idMission
	MpMissionData.iInstanceId = missionScriptArgs.iInstanceId
	
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.idMission) ,  MpMissionData)	//NUM_NETWORK_PLAYERS, missionScriptArgs)	//
	//NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, PARTICIPANT_ID_TO_INT())  
	
	//Reserve Entities
	//RESERVE_NETWORK_MISSION_PEDS(1)
	//RESERVE_NETWORK_MISSION_VEHICLES(1)
	//RESERVE_NETWORK_MISSION_OBJECTS(missionScriptArgs.iNumPickups)
	
	// This makes sure the net script is active, waits untill it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		RETURN FALSE
	ENDIF
			
	// This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		serverBD.iInstance = missionScriptArgs.iInstanceID
		NET_PRINT_TIME() NET_PRINT("     ---------->     GA PICKUPS - iInstanceID = ") NET_PRINT_INT(serverBD.iInstance) NET_NL()
		serverBD.vLocation = missionScriptArgs.vGangAttackLocation
		NET_PRINT_TIME() NET_PRINT("     ---------->     GA PICKUPS - vLocation = ") NET_PRINT_VECTOR(serverBD.vLocation) NET_NL()
		
		INT i
		REPEAT MAX_NUM_DESTRUCTIBLE_CRATES i
			IF NOT IS_BIT_SET(missionScriptArgs.iBitSet, iBS_GAP_Valid)
				SET_BIT(serverBD.DCrateData[i].iBitSet, iBS_DC_CrateDestroyed)
			ENDIF
			
			serverBD.DCrateData[i].vCoord			=	missionScriptArgs.vCoord[i]
			serverBD.DCrateData[i].fHeading			=	missionScriptArgs.fHeading[i]
			
			serverBD.DCrateData[i].iContents 		= 	missionScriptArgs.iContents[i]
			serverBD.DCrateData[i].ContentsWeapon 	= 	missionScriptArgs.ContentsWeapon[i]
			serverBD.DCrateData[i].iContentsAmount 	= 	missionScriptArgs.iContentsAmount[i]
		ENDREPEAT
		
	ENDIF
		
	playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_INI
	NET_PRINT_TIME() NET_PRINT("     ---------->     GA PICKUPS - PRE_GAME DONE - AA     <----------     ") NET_NL()
	
	RETURN TRUE
ENDFUNC

//Player leaves area
FUNC BOOL MISSION_END_CHECK()
		
	/*IF NO PICKUPS LEFT
		NET_PRINT_TIME() NET_PRINT("     ---------->     GA PICKUPS - MISSION END - NO PICKUPS LEFT     <----------     ") NET_NL()
		RETURN TRUE
	ENDIF*/
	
	RETURN FALSE
ENDFUNC

//Server only function that checks to see if the game mode should now end. For this test script, it if time has passed.
FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET()
	
	#IF IS_DEBUG_BUILD
		IF bHostEndMissionNow
			RETURN TRUE
		ENDIF
	#ENDIF	
	
	IF MISSION_END_CHECK()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


//Helper function to get a clients game/mission state
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iGameState
ENDFUNC

//Helper function to get the servers game/mission state
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC

#IF IS_DEBUG_BUILD
PROC CREATE_WIDGETS()

	START_WIDGET_GROUP("GANG ATTACK PICKUPS")  
			
		// Gameplay debug, sever only.
		START_WIDGET_GROUP("Server Only Gameplay") 
			ADD_WIDGET_BOOL("End mission", bHostEndMissionNow)			
		STOP_WIDGET_GROUP()		
		
		// Data about the server
		START_WIDGET_GROUP("Server BD") 
				ADD_WIDGET_INT_SLIDER("S. Game state", serverBD.iServerGameState,-1, HIGHEST_INT,1)
		STOP_WIDGET_GROUP()	
		
	STOP_WIDGET_GROUP()
ENDPROC		

PROC UPDATE_WIDGETS()
	
ENDPROC

#ENDIF

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  ENTITY CREATION                  /////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

//PURPOSE: Creates a Cash pickup that should be in every crate
FUNC BOOL CREATE_BONUS_CASH_PICKUP(INT iCrate, INT iPickup)
	VECTOR vPickupPos
	INT iPlacementFlags = 0
	//SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_UPRIGHT))
	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
	//SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_PLAYER_GIFT))
	//SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ROTATE)
	
	IF CAN_REGISTER_MISSION_ENTITIES(0, 0, 0, 1)
		IF NOT DOES_ENTITY_EXIST(serverBD.DCrateData[iCrate].Pickup[iPickup].oiPickup)
			vPickupPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(serverBD.DCrateData[iCrate].vCoord, serverBD.DCrateData[iCrate].fHeading, <<0, 0.25-(iPickup*0.25), 0.1>>)
			serverBD.DCrateData[iCrate].Pickup[iPickup] = CREATE_AMBIENT_PICKUP_WITH_SAP_PICKUP_ID(PICKUP_GANG_ATTACK_MONEY, vPickupPos, iPlacementFlags, g_sMPTunables.icash_modifier_crate_drop_gang_attack, DEFAULT, TRUE)
			PRINTLN("     ---------->     GA PICKUPS - CREATE_BONUS_CASH_PICKUP ", iCrate, " : ", iPickup , " : CASH AMOUNT = ", g_sMPTunables.icash_modifier_crate_drop_gang_attack)
		ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Creates the contents of the passed in Crate.
FUNC BOOL CREATE_DESTRUCTIBLE_CRATE_CONTENTS(INT i)
	
	IF IS_VECTOR_ZERO(serverBD.DCrateData[i].vCoord)
	OR IS_BIT_SET(serverBD.DCrateData[i].iBitSet, iBS_DC_CrateDestroyed)
		PRINTLN("     ---------->     GA PICKUPS - CREATE_DESTRUCTIBLE_CRATE_CONTENTS - INVALID DATA ", i)
		RETURN TRUE
	ENDIF
	
	INT k
	VECTOR vPickupPos
	INT iPlacementFlags = 0
	//SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_UPRIGHT))
	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))

	SWITCH serverBD.DCrateData[i].iContents
		CASE ciCRATE_CONTENTS_CASH
			//REQUEST_MODEL(PROP_LD_HEALTH_PACK)
			//IF HAS_MODEL_LOADED(PROP_LD_HEALTH_PACK)
			IF CAN_REGISTER_MISSION_ENTITIES(0, 0, 0, MAX_NUM_CASH_PICKUPS)
				REPEAT MAX_NUM_CASH_PICKUPS k
					IF NOT DOES_ENTITY_EXIST(serverBD.DCrateData[i].Pickup[k].oiPickup)
						vPickupPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(serverBD.DCrateData[i].vCoord, serverBD.DCrateData[i].fHeading, <<0.6-(k*0.4), 0, 0.1>>)
						serverBD.DCrateData[i].Pickup[k] = CREATE_AMBIENT_PICKUP_WITH_SAP_PICKUP_ID(PICKUP_GANG_ATTACK_MONEY, vPickupPos, iPlacementFlags, serverBD.DCrateData[i].iContentsAmount, DEFAULT, TRUE)
						PRINTLN("     ---------->     GA PICKUPS - CREATE_DESTRUCTIBLE_CRATE_CONTENTS - CASH ", i, " : ", k)
					ENDIF
				ENDREPEAT
				RETURN TRUE
			ENDIF
		BREAK
		
		/*CASE ciCRATE_CONTENTS_DRUGS
			REQUEST_MODEL(PROP_DRUG_PACKAGE_02)
			IF HAS_MODEL_LOADED(PROP_DRUG_PACKAGE_02)
			AND CAN_REGISTER_MISSION_ENTITIES(0, 0, 0, MAX_NUM_DRUG_PICKUPS)
				REPEAT MAX_NUM_DRUG_PICKUPS k
					IF NOT DOES_PICKUP_EXIST(serverBD.DCrateData[i].Pickup[k])
						vPickupPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(serverBD.DCrateData[i].vCoord, serverBD.DCrateData[i].fHeading, <<0.6-(k*0.4), 0, 0.1>>)
						serverBD.DCrateData[i].Pickup[k] = CREATE_PICKUP(PICKUP_MONEY_MED_BAG, vPickupPos, iPlacementFlags, serverBD.DCrateData[i].iContentsAmount, TRUE, PROP_DRUG_PACKAGE_02)
						PRINTLN("     ---------->     GA PICKUPS - CREATE_DESTRUCTIBLE_CRATE_CONTENTS - DRUGS ", i)
					ENDIF
				ENDREPEAT
				RETURN TRUE
			ENDIF
		BREAK*/
		
		CASE ciCRATE_CONTENTS_WEAPON
			IF CAN_REGISTER_MISSION_ENTITIES(0, 0, 0, HIDEOUT_MAX_NUM_WEAPON_PICKUPS)
				//SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_PLAYER_GIFT))
				REPEAT HIDEOUT_MAX_NUM_WEAPON_PICKUPS k
					IF NOT DOES_ENTITY_EXIST(serverBD.DCrateData[i].Pickup[k].oiPickup)
						vPickupPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(serverBD.DCrateData[i].vCoord, serverBD.DCrateData[i].fHeading, <<0, 0.25-(k*0.25), 0.1>>)
						serverBD.DCrateData[i].Pickup[k] = CREATE_AMBIENT_PICKUP_WITH_SAP_PICKUP_ID(serverBD.DCrateData[i].ContentsWeapon, vPickupPos, iPlacementFlags, DEFAULT, DEFAULT, TRUE)
						PRINTLN("     ---------->     GA PICKUPS - CREATE_DESTRUCTIBLE_CRATE_CONTENTS - WEAPON ", i, " : ", k)
					ENDIF
				ENDREPEAT
				IF CREATE_BONUS_CASH_PICKUP(i, HIDEOUT_MAX_NUM_WEAPON_PICKUPS)
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE ciCRATE_CONTENTS_AMMO
			IF CAN_REGISTER_MISSION_ENTITIES(0, 0, 0, MAX_NUM_AMMO_PICKUPS)
				//SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_PLAYER_GIFT))
				REPEAT MAX_NUM_AMMO_PICKUPS k
					IF NOT DOES_ENTITY_EXIST(serverBD.DCrateData[i].Pickup[k].oiPickup)
						vPickupPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(serverBD.DCrateData[i].vCoord, serverBD.DCrateData[i].fHeading, <<0, 0.25-(k*0.25), 0.2>>)
						serverBD.DCrateData[i].Pickup[k] = CREATE_AMBIENT_PICKUP_WITH_SAP_PICKUP_ID(PICKUP_AMMO_BULLET_MP, vPickupPos, iPlacementFlags, serverBD.DCrateData[i].iContentsAmount, DEFAULT, TRUE)
						PRINTLN("     ---------->     GA PICKUPS - CREATE_DESTRUCTIBLE_CRATE_CONTENTS - AMMO ", i, " : ", k)
					ENDIF
				ENDREPEAT
				IF CREATE_BONUS_CASH_PICKUP(i, MAX_NUM_AMMO_PICKUPS)
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Loops through and creates all the Pickups
FUNC BOOL CREATE_ALL_PICKUPS()
	INT i
	BOOL bFinished = TRUE
	REPEAT MAX_NUM_DESTRUCTIBLE_CRATES i
		IF NOT CREATE_DESTRUCTIBLE_CRATE_CONTENTS(i)
			bFinished = FALSE
		ENDIF
	ENDREPEAT
	
	RETURN bFinished
ENDFUNC

//PURPOSE: Moves the pickups up slightly, due to the snap to ground
PROC MOVE_DESTRUCTIBLE_CRATE_CONTENTS_UP(INT i)
	INT k
	REPEAT (MAX_NUM_CASH_PICKUPS+1) k	//ADD ONE ON FOR BONUS CASH
		IF DOES_ENTITY_EXIST(serverBD.DCrateData[i].Pickup[k].oiPickup)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(serverBD.DCrateData[i].Pickup[k].oiPickup)
				SET_AMBIENT_PICKUP_COORDS_WITH_SAP_PICKUP_ID(serverBD.DCrateData[i].Pickup[k], GET_ENTITY_COORDS(serverBD.DCrateData[i].Pickup[k].oiPickup) + <<0, 0, 0.046*1.5>>)
				PRINTLN("     ---------->     GA PICKUPS - MOVE_DESTRUCTIBLE_CRATE_CONTENTS_UP - ", i, " : ", k)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

//PURPOSE: Move Crates Up (so they don't fall below crate)
PROC MAINTAIN_CRATES()
	INT i
	REPEAT MAX_NUM_DESTRUCTIBLE_CRATES i
		IF NOT IS_BIT_SET(serverBD.DCrateData[i].iBitSet, iBS_DC_CratePickupsMoveUp)
			MOVE_DESTRUCTIBLE_CRATE_CONTENTS_UP(i)
			IF serverBD.DCrateData[i].iMoveCounter > TIMES_TO_MOVE_UP
				SET_BIT(serverBD.DCrateData[i].iBitSet, iBS_DC_CratePickupsMoveUp)
			ENDIF
			serverBD.DCrateData[i].iMoveCounter++
		ENDIF
	ENDREPEAT
ENDPROC

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MISSION PROCS                  //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Cleanup Mission Data
PROC SCRIPT_CLEANUP()
		
	NET_PRINT_TIME() NET_PRINT("     ---------->     GA PICKUPS - CLEANUP MISSION      <----------     ") NET_NL()
	#IF IS_DEBUG_BUILD NET_LOG("SCRIPT_CLEANUP")	#ENDIF
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                      //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT(GANG_ATTACK_PICKUPS_DATA missionScriptArgs)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		#IF IS_DEBUG_BUILD	
			NET_PRINT_TIME() NET_PRINT("MP: Starting mission ") NET_NL()
		#ENDIF
				
		// Carry out all the initial game starting duties. 
		IF NOT PROCESS_PRE_GAME(missionScriptArgs)
			#IF IS_DEBUG_BUILD NET_LOG("FAILED TO RECEIVE INITIAL NETWORK BROADCAST")	#ENDIF
			NET_PRINT_TIME() NET_PRINT("     ---------->     GA PICKUPS - MISSION END - FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP C     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		
		// Setup widgets. Make sure this is done after PROCESS_PRE_GAME()
		#IF IS_DEBUG_BUILD
			CREATE_WIDGETS()
		#ENDIF
	ENDIF
	
	// Main loop
	WHILE TRUE

		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
				
		// If we have a match end event, bail
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			#IF IS_DEBUG_BUILD NET_LOG("SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")	#ENDIF
			NET_PRINT_TIME() NET_PRINT("     ---------->     GA PICKUPS - MISSION END - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE - SCRIPT CLEANUP B     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		
		// Deal with the debug
		#IF IS_DEBUG_BUILD		
			UPDATE_WIDGETS()
		#ENDIF		
	
		// If we need to leave the mission, make sure we just straight to the end stage and terminate the script - DON'T NEED FOR AMBIENT MISSION
		/*IF SHOULD_PLAYER_LEAVE_MP_MISSION() 
			#IF IS_DEBUG_BUILD
			NET_PRINT_TIME() NET_PRINT( GET_MP_MISSION_NAME(thisMission), " Leave mission = TRUE \n")
			#ENDIF
			playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_LEAVE
			#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_LEAVE")	#ENDIF
		ENDIF*/
		
		// -----------------------------------
		// Process your game logic.....
		SWITCH GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT())
			
			// Wait untill the server gives the all go before moving on
			CASE GAME_STATE_INI
				//IF LOAD_BOX_ASSETS()
					IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
						playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING
						NET_PRINT_TIME() NET_PRINT("     ---------->     GA PICKUPS - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING    <----------     ") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_RUNNING")	#ENDIF
					ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
						playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
						NET_PRINT_TIME() NET_PRINT("     ---------->     GA PICKUPS - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 1    <----------     ") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY")	#ENDIF
					ENDIF
				//ENDIF
			BREAK
			
			// Main gameplay state.
			CASE GAME_STATE_RUNNING
				
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					NET_PRINT_TIME() NET_PRINT("     ---------->     GA PICKUPS - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 2   <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY")	#ENDIF
				ENDIF
				
				// Make the player end the mission if they leave the Area
				IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), serverBD.vLocation, <<LEAVE_RANGE, LEAVE_RANGE, LEAVE_RANGE>>)
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					NET_PRINT_TIME() NET_PRINT("     ---------->     GA PICKUPS - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 6 - MISSION END - PLAYER NOT IN AREA ") NET_PRINT_VECTOR(serverBD.vLocation) NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 6")	#ENDIF
				ENDIF
			BREAK
			
			CASE GAME_STATE_TERMINATE_DELAY 
				SET_MISSION_FINISHED(serverBD.MissionTerminateDelayTimer)
		      	IF IS_MISSION_READY_TO_CLEANUP(serverBD.MissionTerminateDelayTimer)
		            playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					NET_PRINT_TIME() NET_PRINT("     ---------->     GA PICKUPS - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 3    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 3")	#ENDIF
		      	ENDIF
			BREAK
			
			// The game stage the local player is placed when we want him to leave a mission
			CASE GAME_STATE_LEAVE
				//LEAVE_MY_MP_MISSION(thisMission)
				playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
				NET_PRINT_TIME() NET_PRINT("     ---------->     GA PICKUPS - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 2    <----------     ") NET_NL()
				#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 2")	#ENDIF
			FALLTHRU
			
			//Cleans up then terminates the mission
			CASE GAME_STATE_END
				NET_PRINT_TIME() NET_PRINT("     ---------->     GA PICKUPS - SCRIPT CLEANUP A     <----------     ") NET_NL()
				SCRIPT_CLEANUP()
			BREAK

		ENDSWITCH
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			
			SWITCH GET_SERVER_MISSION_STATE()
				
				// For CnC group missions, we may want to wait for all players to have launched the script
				CASE GAME_STATE_INI
					IF CREATE_ALL_PICKUPS()
						serverBD.iServerGameState = GAME_STATE_RUNNING
						NET_PRINT_TIME() NET_PRINT("     ---------->     GA PICKUPS - serverBD.iServerGameState = GAME_STATE_RUNNING    <----------     ") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_RUNNING")	#ENDIF
					#IF IS_DEBUG_BUILD 
					ELSE
						NET_PRINT_TIME() NET_PRINT("     ---------->     GA PICKUPS - WAITING ON - CREATE_ALL_PICKUPS    <----------     ") NET_NL()
					#ENDIF
					ENDIF
				BREAK
				
				// Look for game end conditions
				CASE GAME_STATE_RUNNING
					
					//CHECK_IF_ALL_PICKUPS_GONE() //STAGGERED
					
					MAINTAIN_CRATES()
					
					#IF IS_DEBUG_BUILD
						IF bHostEndMissionNow
							serverBD.iServerGameState = GAME_STATE_END
							NET_PRINT_TIME() NET_PRINT("     ---------->     GA PICKUPS - serverBD.iServerGameState = GAME_STATE_END 1    <----------     ") NET_NL()
							#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 1")	#ENDIF
						ENDIF
					#ENDIF	
					
					IF HAVE_MISSION_END_CONDITIONS_BEEN_MET()
						serverBD.iServerGameState = GAME_STATE_END
						NET_PRINT_TIME() NET_PRINT("     ---------->     GA PICKUPS - serverBD.iServerGameState = GAME_STATE_END 4    <----------     ") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 4")	#ENDIF
					ENDIF
					
				BREAK
				
				CASE GAME_STATE_END
				BREAK
				
			ENDSWITCH
			
		ENDIF
		
	ENDWHILE
	
ENDSCRIPT
