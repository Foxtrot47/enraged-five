//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_MP_ARMORY_AIRCRAFT.sc																				//
// Description: Script for managing the interior of a AM_MP_ARMORY_AIRCRAFT. AM_MP_ARMORY_AIRCRAFT access, spawning etc.	//
//				is managed by AM_MP_SMPL_INTERIOR_* script while this script is launched by simple interior			//
//				script to handle anything specific to AM_MP_ARMORY_AIRCRAFT.											//
// Written by:  Online Technical Team: Leo Goldsmith																//
// Date:  		27/06/2017																							//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"

USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_event.sch"

USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"

USING "net_wait_zero.sch"
USING "freemode_events_header.sch"


USING "net_MP_Radio.sch"
USING "net_activity_creator_activities.sch"
USING "net_include.sch"
USING "net_simple_interior.sch"
USING "net_realty_armory_aircraft.sch"
USING "net_mp_cctv.sch"
USING "net_gun_locker.sch"
#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF


//Non-turret seats
CONST_INT MAX_NUM_SEATS_FOR_SITTING_ACTIVITY 5
USING "defunct_base_sitting_activities_public.sch"
USING "defunct_base_bench_helper.sch"

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ VARIABLES ╞═════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

STRUCT ServerBroadcastData
	INT iBS
	MP_RADIO_SERVER_DATA_STRUCT MPRadioServer
	MP_PROP_ACT_SERVER_CONTROL_STRUCT activityControl
	SERVER_CREATOR_ACTIVITY_PROPS activityProps[iMaxCreatorActivities]
ENDSTRUCT
ServerBroadcastData serverBD

STRUCT PlayerBroadcastData
	INT iBS
	MP_RADIO_CLIENT_DATA_STRUCT MPRadioClient
	INT iActivityRequested
	MP_CCTV_CLIENT_DATA_STRUCT MPCCTVClient
	INT iRadioStation
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

ENUM ARMORY_AIRCRAFT_SCRIPT_STATE
	ARMORY_AIRCRAFT_STATE_LOADING,
	ARMORY_AIRCRAFT_STATE_IDLE,
	ARMORY_AIRCRAFT_STATE_PLAYING_INTRO,
	ARMORY_AIRCRAFT_STATE_EXIT
ENDENUM

STRUCT ARMORY_AIRCRAFT_DATA
	INT iBS
	ARMORY_AIRCRAFT_SCRIPT_STATE eState
	INT iID // ID of this ArmoryAircraft
	SIMPLE_INTERIORS eSimpleInteriorID
	INT iScriptInstance
	INT iInvitingPlayer
	INT iBikerSaveSlot
	
	PLAYER_INDEX pOwner
	SIMPLE_INTERIOR_DETAILS interiorDetails
	VEHICLE_INDEX ownerVeh
	
	STRING sChildOfChildScript	
	THREADID CarModThread
	
	SCRIPT_TIMER tOwnerNotOk
	SCRIPT_TIMER sCarModScriptRunTimer
	SCRIPT_TIMER tPinInMemTimer
	
	INTERIOR_INSTANCE_INDEX interiorIndex
	OBJECT_INDEX oXmasTinsel
	BOOL bScriptRelaunched
	
	BLIP_INDEX bWeaponWorkshopBlip
	
	MP_CCTV_LOCAL_DATA_STRUCT MPCCTVLocal
	SEATS_LOCAL_DATA seatData
	INT iLocalRadioStation
ENDSTRUCT
ARMORY_AIRCRAFT_DATA thisArmoryAircraft

MODEL_NAMES eXmasTinselProp = INT_TO_ENUM(MODEL_NAMES, HASH("xm_Int_Prop_Tinsel_Aven_01a"))
VAULT_WEAPON_LOADOUT_CUSTOMIZATION sAvengerGunLocker

#IF IS_DEBUG_BUILD
STRUCT ARMORY_AIRCRAFT_DEBUG_DATA
	STRING sWidgetName
	
	BOOL bUpdateSeatData = FALSE
ENDSTRUCT
ARMORY_AIRCRAFT_DEBUG_DATA debugData
#ENDIF

SERVER_CREATOR_ACTIVITY_PEDS activityPeds[iMaxCreatorActivities]
ACTIVITY_INTERIOR_STRUCT interiorStruct
CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck[iMaxCreatorActivities]
ACTIVITY_CONTROLLER_STRUCT activityControllerStruct
SCRIPT_TIMER pauseMenuInteractionsDelay

// PlayerBroadcastData iBS
CONST_INT BS_ARMORY_AIRCRAFT_READY_TO_WARP_OUT_W_OWNER					0

// thisArmoryAircraft.iBS
CONST_INT BS_ARMORY_AIRCRAFT_IS_CAR_MOD_SCRIPT_READY 					0
CONST_INT BS_ARMORY_AIRCRAFT_CALLED_CLEAR_HELP							1
CONST_INT BS_ARMORY_AIRCRAFT_INTERIOR_REFRESHED_ON_INIT					2
CONST_INT BS_ARMORY_AIRCRAFT_KILL_LOAD_SCENE_WHEN_INSIDE				3
CONST_INT BS_ARMORY_AIRCRAFT_DATA_LOAD_SCENE_STARTED					4
CONST_INT BS_ARMORY_AIRCRAFT_COCKPIT_PASSENGER_ACCESS_EVERYONE			5
CONST_INT BS_ARMORY_AIRCRAFT_COCKPIT_PASSENGER_ACCESS_NO_ONE			6
CONST_INT BS_ARMORY_AIRCRAFT_CAN_LEAVE_TO_COCKPIT_PASSENGER				7
CONST_INT BS_ARMORY_AIRCRAFT_CAN_NOT_LEAVE_TO_COCKPIT_PASSENGER			8
CONST_INT BS_ARMORY_AIRCRAFT_UNFREEZE_PLAYER_VEHICLE					9
CONST_INT BS_ARMORY_AIRCRAFT_PHONE_ON_SCREEN							10
CONST_INT BS_ARMORY_AIRCRAFT_BLOCK_EXIT									11
CONST_INT BS_ARMORY_AIRCRAFT_SET_WEAPON_WORK_BENCH_TIMECYCLE_MOD		12
CONST_INT BS_ARMORY_AIRCRAFT_INIT_FADE_IN_TIMER							13
CONST_INT BS_ARMORY_AIRCRAFT_RELOAD_VEHICLE_WEAPONS						14
CONST_INT BS_ARMORY_AIRCRAFT_NEED_TO_SET_VEHICLE_INDEX_EXISTENCE		15

// AOC Entity Sets
CONST_INT BS_ARMORY_AIRCRAFT_GUN_TURRET_PURCHASED						0
CONST_INT BS_ARMORY_AIRCRAFT_WEAPONS_WORKSHOP_PURCHASED					1
CONST_INT BS_ARMORY_AIRCRAFT_VEHICLE_WORKSHOP_PURCHASED					2

TWEAK_FLOAT m_vehicle_zoomed_in_value									1.0
TWEAK_FLOAT m_vehicle_zoomed_out_value									0.0

SCRIPT_TIMER sFadeInTimer

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ PROCEDURES ╞═════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL IS_ARMORY_AIRCRAFT_STATE(ARMORY_AIRCRAFT_SCRIPT_STATE eState)
	RETURN thisArmoryAircraft.eState = eState
ENDFUNC

PROC SET_ARMORY_AIRCRAFT_STATE(ARMORY_AIRCRAFT_SCRIPT_STATE eState)
	CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] AM_MP_ARMORY_AIRCRAFT - SET_ARMORY_AIRCRAFT_STATE - New state: ", ENUM_TO_INT(eState))
	thisArmoryAircraft.eState = eState
ENDPROC

#IF IS_DEBUG_BUILD
FUNC STRING DEBUG_GET_PLAYER_BROADCAST_BIT_NAME(INT iBit)
	SWITCH iBit 
		CASE BS_ARMORY_AIRCRAFT_GUN_TURRET_PURCHASED			RETURN "BS_ARMORY_AIRCRAFT_GUN_TURRET_PURCHASED"
		CASE BS_ARMORY_AIRCRAFT_WEAPONS_WORKSHOP_PURCHASED		RETURN "BS_ARMORY_AIRCRAFT_WEAPONS_WORKSHOP_PURCHASED"
		CASE BS_ARMORY_AIRCRAFT_VEHICLE_WORKSHOP_PURCHASED		RETURN "BS_ARMORY_AIRCRAFT_VEHICLE_WORKSHOP_PURCHASED"
	ENDSWITCH
	
	RETURN "NULL"
ENDFUNC
#ENDIF

PROC SET_LOCAL_PLAYER_BROADCAST_BIT(INT iBit, BOOL bSet)
	IF bSet
		#IF IS_DEBUG_BUILD
		IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, iBit)
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_ARMORY_AIRCRAFT - SET_LOCAL_PLAYER_BROADCAST_BIT - Setting bit ", DEBUG_GET_PLAYER_BROADCAST_BIT_NAME(iBit))
		#ENDIF
			SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, iBit)
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		IF IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, iBit)	
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_ARMORY_AIRCRAFT - SET_LOCAL_PLAYER_BROADCAST_BIT - Clearing bit ", DEBUG_GET_PLAYER_BROADCAST_BIT_NAME(iBit))
		#ENDIF	
			CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, iBit)
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_CCTV(BOOL bReturnControl)
	CLEANUP_MP_CCTV_CLIENT(playerBD[NATIVE_TO_INT(PLAYER_ID())].MPCCTVClient, thisArmoryAircraft.MPCCTVLocal, bReturnControl)
ENDPROC

PROC CLEAN_UP_PERSONAL_CAR_MOD()
	PRINTLN("CLEAN_UP_PERSONAL_CAR_MOD (AOC) - called")
	g_bCleanUpCarmodShop = TRUE
	g_sShopSettings.bShopScriptLaunched[ENUM_TO_INT(CARMOD_SHOP_PERSONALMOD)] = FALSE
	CLEAR_BIT(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_IS_CAR_MOD_SCRIPT_READY)
	RESET_NET_TIMER(thisArmoryAircraft.sCarModScriptRunTimer)
	IF NOT IS_STRING_NULL_OR_EMPTY(thisArmoryAircraft.sChildOfChildScript)
		SET_SCRIPT_AS_NO_LONGER_NEEDED(thisArmoryAircraft.sChildOfChildScript)
	ENDIF	
ENDPROC

PROC STORE_SEAT_DATA()
	INT i
	REPEAT MAX_NUM_SEATS_FOR_SITTING_ACTIVITY i
		SETUP_NAMED_FACILITY_AOC_SEAT(INT_TO_ENUM(NAMED_AOC_SEATS, i), thisArmoryAircraft.seatData, i)
	ENDREPEAT
ENDPROC

PROC CLEANUP_ARMORY_AIRCRAFT_VEHICLE_FLAGS()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND g_iSimpleInteriorState != SIMPLE_INT_STATE_IDLE
	AND NOT HAS_PLAYER_ACCEPTED_SIMPLE_INTERIOR_INVITE()
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		VEHICLE_INDEX vTemp = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
		
		IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
		
			IF !IS_VEHICLE_MODEL(vTemp , AVENGER)
				SET_ENTITY_COLLISION(vTemp, TRUE)
				SET_ENTITY_VISIBLE(vTemp, TRUE)
				IF NOT IS_ENTER_BASE_FROM_AIRCRAFT_WITH_VEH_TRIGGERED_IN_PROGRESS()
					FREEZE_ENTITY_POSITION(vTemp, FALSE)
				ENDIF	
				SET_VEHICLE_LIGHTS(vTemp, NO_VEHICLE_LIGHT_OVERRIDE)
				SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vTemp, FALSE)
				SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(vTemp, FALSE)
				SET_VEHICLE_IS_STOLEN(vTemp, FALSE)
				SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(vTemp, FALSE)
				SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(vTemp, FALSE)
				SET_ENTITY_CAN_BE_DAMAGED(vTemp, TRUE)
				SET_ENTITY_INVINCIBLE(vTemp, FALSE)
				SET_CAN_USE_HYDRAULICS(vTemp, TRUE)
				NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(vTemp, FALSE)
			ENDIF
		ENDIF
	
	ENDIF
ENDPROC

PROC CLEANUP_ARMORY_AIRCRAFT_BLIPS()
	IF DOES_BLIP_EXIST(thisArmoryAircraft.bWeaponWorkshopBlip)
		REMOVE_BLIP(thisArmoryAircraft.bWeaponWorkshopBlip)
	ENDIF
ENDPROC

PROC CLEAN_UP_AVENGER_GUN_LOCKER(VAULT_WEAPON_LOADOUT_CUSTOMIZATION& sWLoadoutCustomization,BOOL bForceCleanUp = FALSE)
	PRINTLN("Vault weapon loadout - CLEAN_UP_AVENGER_GUN_LOCKER")
	
	RELEASE_CONTEXT_INTENTION(sWLoadoutCustomization.iVaultWeaponContext)
	sWLoadoutCustomization.iVaultWeaponContext = NEW_CONTEXT_INTENTION
	sWLoadoutCustomization.iWeaponMenuCurrentItem = 0
	sWLoadoutCustomization.iWeaponMenuTopItem = 0
	sWLoadoutCustomization.iNumAvailableWeaponGroup = 0
	sWLoadoutCustomization.eWeaponCurrentMenu = VM_MAIN_MENU
	sWLoadoutCustomization.bMenuInitialised = FALSE
	sWLoadoutCustomization.bReBuildMenu = FALSE
	
	CLEAR_BIT(sWLoadoutCustomization.iLocalBS, VAULT_DOOR_ANIM_STAGE_FINISHED)
	CLEAR_BIT(sWLoadoutCustomization.iLocalBS, VAULT_GUN_DOOR_ANIM_RUNNING)
	CLEAR_BIT(sWLoadoutCustomization.iLocalBS, BS_GET_NUM_AVAILABLE_WEAPON_GROUP)
	
	INT iNumSubMenu
	FOR iNumSubMenu = 0 TO WEAPON_VAULT_TOTAL - 1
		CLEAR_BIT(sWLoadoutCustomization.iVaultWeaponBS, iNumSubMenu)
		CLEAR_BIT(sWLoadoutCustomization.iAvailableWeaponBS, iNumSubMenu)
		CLEAR_BIT(sWLoadoutCustomization.iHideAllWeaponBS, iNumSubMenu)
	ENDFOR
	
	IF bForceCleanUp
		INT iNumWeaponModels
		FOR iNumWeaponModels = 0 TO MAX_NUMBER_WEAPON_MODELS -1
			IF DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons[iNumWeaponModels])
				DELETE_OBJECT(sWLoadoutCustomization.weapons[iNumWeaponModels])
			ENDIF
			
			IF IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS, iNumWeaponModels)
				CLEAR_BIT(sWLoadoutCustomization.iWeaponModelBS, iNumWeaponModels)
			ENDIF
			
			IF DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons2[iNumWeaponModels])
				DELETE_OBJECT(sWLoadoutCustomization.weapons2[iNumWeaponModels])
			ENDIF
			
			IF IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS2, iNumWeaponModels)
				CLEAR_BIT(sWLoadoutCustomization.iWeaponModelBS2, iNumWeaponModels)
			ENDIF
		ENDFOR
		
		CLEAR_BIT(sWLoadoutCustomization.iLocalBS, VAULT_GUN_DOOR_ANIM_INITIALISED)
		CLEAR_BIT(sWLoadoutCustomization.iLocalBS, VAULT_GUN_DOOR_NET_RESERVED)
		CLEAR_BIT(sWLoadoutCustomization.iLocalBS, VAULT_DOOR_ANIM_STAGE_FINISHED)
		CLEAR_BIT(sWLoadoutCustomization.iLocalBS, OFFICE_GUN_VAULT_DOOR_CREATED)
		
		sWLoadoutCustomization.eCustomizationStage = VMC_STAGE_INIT
	ELSE
		sWLoadoutCustomization.eCustomizationStage = VMC_STAGE_INIT
	ENDIF
ENDPROC


PROC SCRIPT_CLEANUP()	
	#IF IS_DEBUG_BUILD //Is this directive necessary? CDEBUG1LN is declared as debugonly.
	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_ARMORY_AIRCRAFT - SCRIPT_CLEANUP")
	#ENDIF
	
	IF thisArmoryAircraft.pOwner != PLAYER_ID()
		BROADCAST_REQUEST_VEHICLE_EXISTENCE(thisArmoryAircraft.pOwner, FALSE, FALSE, TRUE, FALSE #IF FEATURE_HEIST_ISLAND , FALSE #ENDIF )
	ENDIF
	
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		IF GET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableTakeOffParachutePack)
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableTakeOffParachutePack, FALSE)
		ENDIF
	ENDIF	
	
	REACTIVATE_NAMED_WORLD_BRAINS_WAITING_TILL_OUT_OF_RANGE("AM_MP_PROPERTY_EXT")
	STOP_AUDIO_SCENE("dlc_xm_avenger_interior_scene")
	SET_AMBIENT_ZONE_LIST_STATE("azl_dlc_xm_int_01_avngr_zones", FALSE, FALSE)
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableTurretOrRearSeatPreference, FALSE)
	ENDIF
	CLEAR_TIMECYCLE_MODIFIER()
	IF thisArmoryAircraft.pOwner = PLAYER_ID()
		SET_FORCE_CONCEAL_PERSONAL_VEHICLE_EVENT_FLAG(PLAYER_ID(), FALSE)
	ENDIF	
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PlayersDontDragMeOutOfCar, FALSE)
	IF IS_BIT_SET(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_BLOCK_EXIT)
		BLOCK_SIMPLE_INTERIOR_EXIT(FALSE)
		CLEAR_BIT(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_BLOCK_EXIT)
	ENDIF
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableStartEngine, FALSE)
	ENDIF
	SET_VEHICLE_SEAT_UNAVAILABLE(FALSE)
	CLEANUP_CCTV(TRUE)
	CLEAN_UP_PERSONAL_CAR_MOD()
	CLEANUP_ARMORY_AIRCRAFT_VEHICLE_FLAGS()
	CLEANUP_ARMORY_AIRCRAFT_BLIPS()
	CLEAN_UP_AVENGER_GUN_LOCKER(sAvengerGunLocker, TRUE)
	CLEANUP_SEATS_ACTIVITY(thisArmoryAircraft.seatData)
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════════╡ DEBUG ╞═══════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
PROC CREATE_DEBUG_WIDGETS()
	debugData.sWidgetName = "AM_MP_ARMORY_AIRCRAFT"
	
	START_WIDGET_GROUP(debugData.sWidgetName)
		CREATE_MPCCTV_WIDGETS()
		
		ADD_WIDGET_FLOAT_SLIDER("m_vehicle_zoomed_in_value", m_vehicle_zoomed_in_value, 000.0, 100.0, 1.0)
		ADD_WIDGET_FLOAT_SLIDER("m_vehicle_zoomed_out_value", m_vehicle_zoomed_out_value, 000.0, 100.0, 1.0)
		
		ADD_WIDGET_FLOAT_SLIDER("cfAOC_ANIM_X", cfAOC_ANIM_X, -5000, 5000, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("cfAOC_ANIM_Y", cfAOC_ANIM_Y, -5000, 5000, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("cfAOC_ANIM_Z", cfAOC_ANIM_Z, -5000, 5000, 0.01)
		ADD_WIDGET_BOOL("Force update seat", debugData.bUpdateSeatData)
		ADD_WIDGET_FLOAT_SLIDER("cfAOC_SEAT_ANIM_ROT_OFFSET", cfAOC_SEAT_ANIM_ROT_OFFSET, -180, 180, 1)
		ADD_WIDGET_INT_SLIDER("FORCED_ANIM_VARIATION", thisArmoryAircraft.seatData.debug.iNextForcedAnimVar, 0, 20, 1)
		ADD_WIDGET_BOOL("FORCE_NOW", thisArmoryAircraft.seatData.debug.bForceAnimVarNow)	
	STOP_WIDGET_GROUP()
ENDPROC

PROC UPDATE_DEBUG_WIDGETS()
	IF debugData.bUpdateSeatData
		STORE_SEAT_DATA()
	ENDIF
ENDPROC
#ENDIF

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ INITIALIZE ╞═════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC INITIALISE_ENTITY_SETS()
	VECTOR vInteriorPosition
	FLOAT fInteriorHeading
	TEXT_LABEL_63 sInteriorType
	
	GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION(thisArmoryAircraft.eSimpleInteriorID, sInteriorType, vInteriorPosition, fInteriorHeading)
	thisArmoryAircraft.interiorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(vInteriorPosition, sInteriorType)
	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_ARMORY_AIRCRAFT - INITIALISE_ENTITY_SETS - thisArmoryAircraft.interiorIndex = ", NATIVE_TO_INT(thisArmoryAircraft.interiorIndex))
	
	IF IS_VALID_INTERIOR(thisArmoryAircraft.interiorIndex)
		CONFIGURE_ARMORY_AIRCRAFT_ENTITY_SET(thisArmoryAircraft.pOwner)
	ENDIF
	
ENDPROC 

PROC INITIALISE()
	g_bPlayerLeavingCurrentInteriorInVeh = FALSE
	
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_ARMORY_AIRCRAFT_GUN_TURRET_PURCHASED, IS_PLAYER_ARMORY_AIRCRAFT_GUN_TURRET_PURCHASED(thisArmoryAircraft.pOwner))
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_ARMORY_AIRCRAFT_WEAPONS_WORKSHOP_PURCHASED, IS_PLAYER_ARMORY_AIRCRAFT_WEAPON_WORKSHOP_PURCHASED(thisArmoryAircraft.pOwner))
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_ARMORY_AIRCRAFT_VEHICLE_WORKSHOP_PURCHASED, IS_PLAYER_ARMORY_AIRCRAFT_VEHICLE_WORKSHOP_PURCHASED(thisArmoryAircraft.pOwner))
	
	// Get simple interior details
	GET_SIMPLE_INTERIOR_DETAILS(thisArmoryAircraft.eSimpleInteriorID, thisArmoryAircraft.interiorDetails)
	
	INITIALISE_ENTITY_SETS()
	SET_START_BASE_VEHICLE_EXIT_TO_ENTER_AIRCRAFT(FALSE)
	START_AUDIO_SCENE("dlc_xm_avenger_interior_scene")
	SET_AMBIENT_ZONE_LIST_STATE("azl_dlc_xm_int_01_avngr_zones", TRUE, FALSE)
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
			SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
		ENDIF
		IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE, 1, FALSE, FALSE)
		ENDIF
	ENDIF	
	RESET_ADAPTATION(1)
	#IF IS_DEBUG_BUILD
	CREATE_DEBUG_WIDGETS()
	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_ARMORY_AIRCRAFT - INITIALISE - Done.")
	#ENDIF
ENDPROC

FUNC BOOL DETERMINE_INTERIOR_OWNER()
	// We have to search for the owner as we can't rely on gang membership.
	IF IS_LOCAL_PLAYER_IN_ARMORY_AIRCRAFT_THEY_OWN()
		thisArmoryAircraft.pOwner = PLAYER_ID()
		g_ownerOfArmoryAircraftPropertyIAmIn = thisArmoryAircraft.pOwner
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_ARMORY_AIRCRAFT - DETERMINE_INTERIOR_OWNER - I ", GET_PLAYER_NAME(thisArmoryAircraft.pOwner), " am the owner. Access BS = ", g_SimpleInteriorData.iAccessBS)
		SET_LOCAL_PLAYER_CHECK_FOR_NEARBY_PLAYERS_IN_BASE(FALSE)
		RETURN TRUE
	ELIF g_SimpleInteriorData.iAccessBS > 0
	OR IS_PLAYER_MOVING_FROM_BASE_TO_AIRCRAFT()
		IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner != INVALID_PLAYER_INDEX()
			
			IF IS_NET_PLAYER_OK(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner, FALSE)
				thisArmoryAircraft.pOwner = GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner	
				g_ownerOfArmoryAircraftPropertyIAmIn = thisArmoryAircraft.pOwner
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_ARMORY_AIRCRAFT - DETERMINE_INTERIOR_OWNER - I am not the owner. It is: ", GET_PLAYER_NAME(thisArmoryAircraft.pOwner))
				RETURN TRUE
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_ARMORY_AIRCRAFT - DETERMINE_INTERIOR_OWNER - I am not the owner. It is: ", GET_PLAYER_NAME(thisArmoryAircraft.pOwner), " holding up the script due to IS_NET_PLAYER_OK")
				
				IF NOT HAS_NET_TIMER_STARTED(thisArmoryAircraft.tOwnerNotOk)
					START_NET_TIMER(thisArmoryAircraft.tOwnerNotOk)
				ENDIF
				
				IF HAS_NET_TIMER_EXPIRED(thisArmoryAircraft.tOwnerNotOk, 5000)
					// If the owner is not ok after 5 seconds just move on. The interior script will run a check on this and kick us once we're in anyway.
					thisArmoryAircraft.pOwner = GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner
					g_ownerOfArmoryAircraftPropertyIAmIn = thisArmoryAircraft.pOwner
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_ARMORY_AIRCRAFT - DETERMINE_INTERIOR_OWNER - I am not the owner. It is: ", GET_PLAYER_NAME(thisArmoryAircraft.pOwner), " timer expired for IS_NET_PLAYER_OK. Continuing regardless.")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		IF IS_PLAYER_MOVING_FROM_BASE_TO_AIRCRAFT()
			IF IS_PLAYER_NEED_TO_CHECK_FOR_NEARBY_PLAYERS_IN_BASE(PLAYER_ID())
				thisArmoryAircraft.pOwner = PLAYER_ID()
				g_ownerOfArmoryAircraftPropertyIAmIn = PLAYER_ID()
				SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_ARMORY_AIRCRAFT_ACCESS_BS_OWNER)
				SET_LOCAL_PLAYER_CHECK_FOR_NEARBY_PLAYERS_IN_BASE(FALSE)
				PRINTLN("AM_MP_ARMORY_TRUCK - DETERMINE_INTERIOR_OWNER - IS_PLAYER_MOVING_FROM_BASE_TO_AIRCRAFT I'm owner.")
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		IF (GET_FRAME_COUNT() % 60) = 0
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_ARMORY_AIRCRAFT - DETERMINE_INTERIOR_OWNER - Unable to determine interior owner, g_SimpleInteriorData.iAccessBS = ", g_SimpleInteriorData.iAccessBS)
		ENDIF
		
		IF NOT HAS_NET_TIMER_STARTED(g_SimpleInteriorData.tIntAccessFallback)
			START_NET_TIMER(g_SimpleInteriorData.tIntAccessFallback)
		ENDIF
		
		IF HAS_NET_TIMER_EXPIRED(g_SimpleInteriorData.tIntAccessFallback, g_SimpleInteriorData.iIntAccessFallbackTime)
			thisArmoryAircraft.pOwner = INVALID_PLAYER_INDEX()
			RESET_NET_TIMER(g_SimpleInteriorData.tIntAccessFallback)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_ARMORY_AIRCRAFT - DETERMINE_INTERIOR_OWNER - Unable to determine interior owner, returning TRUE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_ARMORY_AIRCRAFT - DETERMINE_INTERIOR_OWNER - g_SimpleInteriorData.iAccessBS = ", g_SimpleInteriorData.iAccessBS)
	#ENDIF
	
	IF (GET_FRAME_COUNT() % 60) = 0
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_ARMORY_AIRCRAFT - DETERMINE_INTERIOR_OWNER - Trying to find the owner.")
	ENDIF
	
	RETURN FALSE
ENDFUNC 

PROC SCRIPT_INITIALISE(SIMPLE_INTERIOR_CHILD_SCRIPT_DATA& scriptData)
	
	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][ARMORY_AIRCRAFT] AM_MP_ARMORY_AIRCRAFT - SCRIPT_INITIALISE - Launching instance ", thisArmoryAircraft.iScriptInstance)
	#ENDIF
	
	SET_EMPTY_AVENGER_HOLD(FALSE)
	SET_PLAYER_NEEDS_TO_BE_CONCAEL_FOR_BASE_AIRCRAFT_TRANS(FALSE)
	SET_START_EXIT_TO_BASE_FROM_AIRCRAFT(FALSE)
	SET_EXIT_TO_BASE_FROM_AIRCRAFT_TRIGGERED(FALSE)
	
	g_bExitingDefuncBaseInVehIgnoreEntryLocate = FALSE
	
	IF IS_PHONE_ONSCREEN()
		HANG_UP_AND_PUT_AWAY_PHONE()
	ENDIF
	
	WHILE NOT DETERMINE_INTERIOR_OWNER()
		IF NOT NETWORK_IS_GAME_IN_PROGRESS()
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_ARMORY_AIRCRAFT - SCRIPT_INITIALISE - NETWORK_IS_GAME_IN_PROGRESS = FALSE")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_ARMORY_AIRCRAFT - SCRIPT_INITIALISE - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE(scriptData.iScriptInstance, scriptData.eSimpleInteriorID)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_ARMORY_AIRCRAFT - SCRIPT_INITIALISE - SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_ARMORY_AIRCRAFT_ACCESS_BS_OWNER_LEFT_GAME)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_ARMORY_AIRCRAFT - SCRIPT_INITIALISE - SIMPLE_INTERIOR_ARMORY_AIRCRAFT_ACCESS_BS_OWNER_LEFT_GAME = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		WAIT(0)
	ENDWHILE
	
	BROADCAST_REQUEST_VEHICLE_EXISTENCE(thisArmoryAircraft.pOwner, TRUE, FALSE, TRUE, FALSE #IF FEATURE_HEIST_ISLAND , FALSE #ENDIF )
	thisArmoryAircraft.eSimpleInteriorID 	= scriptData.eSimpleInteriorID
	thisArmoryAircraft.iScriptInstance 	= scriptData.iScriptInstance
	thisArmoryAircraft.iInvitingPlayer 	= scriptData.iInvitingPlayer
	
	g_iGunModInstance = thisArmoryAircraft.iScriptInstance
	
	IF thisArmoryAircraft.pOwner = INVALID_PLAYER_INDEX()
		#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][ARMORY_AIRCRAFT] AM_MP_ARMORY_AIRCRAFT - SCRIPT_INITIALISE - Owner of this ARMORY_AIRCRAFT is invalid, exiting...")
		#ENDIF
		SET_SIMPLE_INTERIOR_CHILD_SCRIPT_READY(TRUE)
	ENDIF
	
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, thisArmoryAircraft.iScriptInstance)
	
	// This makes sure the net script is active, waits untill it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	STORE_SEAT_DATA()
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		PRINTLN("AM_MP_ARMORY_AIRCRAFT - FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		// This script will not be paused if another script calls PAUSE_GAME
		PRINTLN("AM_MP_ARMORY_AIRCRAFT - INITIALISED")
	ELSE
		PRINTLN("AM_MP_ARMORY_AIRCRAFT - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
	INITIALISE()
	
ENDPROC

FUNC BOOL MAINTAIN_LAUNCHING_CARMOD_SCRIPT()
	
	IF thisArmoryAircraft.pOwner != INVALID_PLAYER_INDEX()
		IF IS_PLAYER_ARMORY_AIRCRAFT_VEHICLE_WORKSHOP_PURCHASED(thisArmoryAircraft.pOwner)
			//Make sure the car mod script is not started too early after being killed
			//Make sure the car mod script cleanup flag is false
			IF g_bCleanUpCarmodShop = TRUE
				IF NOT HAS_NET_TIMER_STARTED(thisArmoryAircraft.sCarModScriptRunTimer)
					START_NET_TIMER(thisArmoryAircraft.sCarModScriptRunTimer)
				ELSE
					IF HAS_NET_TIMER_EXPIRED(thisArmoryAircraft.sCarModScriptRunTimer, 8000)
						g_bCleanUpCarmodShop = FALSE
						RESET_NET_TIMER(thisArmoryAircraft.sCarModScriptRunTimer)
						#IF IS_DEBUG_BUILD
						PRINTLN("MAINTAIN_LAUNCHING_CARMOD_SCRIPT - Setting g_bCleanUpCarmodShop To FALSE")
						#ENDIF
					ENDIF	
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(thisArmoryAircraft.iBS,BS_ARMORY_AIRCRAFT_IS_CAR_MOD_SCRIPT_READY)
				thisArmoryAircraft.sChildOfChildScript = "armory_aircraft_carmod"
				REQUEST_SCRIPT(thisArmoryAircraft.sChildOfChildScript)
				IF HAS_SCRIPT_LOADED(thisArmoryAircraft.sChildOfChildScript)
				AND NOT IS_THREAD_ACTIVE(thisArmoryAircraft.CarModThread)
				AND !g_bCleanUpCarmodShop
					
					g_iCarModInstance = thisArmoryAircraft.iScriptInstance
					
					IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(thisArmoryAircraft.sChildOfChildScript)) < 1
						IF NOT NETWORK_IS_SCRIPT_ACTIVE(thisArmoryAircraft.sChildOfChildScript, g_iCarModInstance, TRUE)
							SHOP_LAUNCHER_STRUCT sShopLauncherData
							sShopLauncherData.bLinkedShop = FALSE
							sShopLauncherData.eShop = CARMOD_SHOP_PERSONALMOD
							sShopLauncherData.iNetInstanceID = g_iCarModInstance
							sShopLauncherData.ePersonalCarModVariation = PERSONAL_CAR_MOD_VARIATION_AOC
							
							g_iPersonalCarModVariation = ENUM_TO_INT(sShopLauncherData.ePersonalCarModVariation)
							thisArmoryAircraft.CarModThread = START_NEW_SCRIPT_WITH_ARGS(thisArmoryAircraft.sChildOfChildScript, sShopLauncherData, SIZE_OF(sShopLauncherData), CAR_MOD_SHOP_STACK_SIZE)
							SET_SCRIPT_AS_NO_LONGER_NEEDED(thisArmoryAircraft.sChildOfChildScript)
							SET_BIT(thisArmoryAircraft.iBS,BS_ARMORY_AIRCRAFT_IS_CAR_MOD_SCRIPT_READY)
							g_sShopSettings.bShopScriptLaunched[ENUM_TO_INT(CARMOD_SHOP_PERSONALMOD)] = TRUE
							g_sShopSettings.bShopScriptLaunchedInMP[ENUM_TO_INT(CARMOD_SHOP_PERSONALMOD)] = NETWORK_IS_GAME_IN_PROGRESS()
							PRINTLN("MAINTAIN_LAUNCHING_CARMOD_SCRIPT - TRUE sShopLauncherData.ePersonalCarModVariation: ", sShopLauncherData.ePersonalCarModVariation)
							RETURN TRUE
						ELSE
							#IF IS_DEBUG_BUILD
								PRINTLN("MAINTAIN_LAUNCHING_CARMOD_SCRIPT: Unable to start shop script for personal_car_mod_shop - last instance still active")
							#ENDIF					
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
							CLEAN_UP_PERSONAL_CAR_MOD()
							TERMINATE_THREAD(thisArmoryAircraft.CarModThread)
						ENDIF
						#ENDIF
						
						PRINTLN("MAINTAIN_LAUNCHING_CARMOD_SCRIPT - carmod is already running")
						RETURN TRUE 
					ENDIF
				ELSE
					PRINTLN("MAINTAIN_LAUNCHING_CARMOD_SCRIPT - is car mod thread active: " , IS_THREAD_ACTIVE(thisArmoryAircraft.CarModThread))
					PRINTLN("MAINTAIN_LAUNCHING_CARMOD_SCRIPT - is car mod loaded: " , HAS_SCRIPT_LOADED(thisArmoryAircraft.sChildOfChildScript))
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
					CLEAN_UP_PERSONAL_CAR_MOD()
					TERMINATE_THREAD(thisArmoryAircraft.CarModThread)
				ENDIF
				#ENDIF
				
				RETURN TRUE
			ENDIF
		ELSE
			IF IS_BIT_SET(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_IS_CAR_MOD_SCRIPT_READY)
				g_bCleanUpCarmodShop = TRUE
				CLEAR_BIT(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_IS_CAR_MOD_SCRIPT_READY)
			ENDIF	
			
			RETURN TRUE
		ENDIF
	ELSE
		PRINTLN("MAINTAIN_LAUNCHING_CARMOD_SCRIPT - thisArmoryAircraft.pOwner is invalid")
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("MAINTAIN_LAUNCHING_CARMOD_SCRIPT - return false")
	#ENDIF
	RETURN FALSE
ENDFUNC

PROC KICK_PLAYERS_OUT_OF_VEHICLE_IN_AIRCRAFT_AFTER_AUTOWARP()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
		AND IS_DRIVER_ENTERING_SIMPLE_INTERIOR(PLAYER_ID())
		AND thisArmoryAircraft.pOwner != INVALID_PLAYER_INDEX()
			IF GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) = HASH("GtaMloRoom001")
				IF IS_SCREEN_FADED_IN()		
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER)
							thisArmoryAircraft.ownerVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
							IF IS_VEHICLE_A_PERSONAL_VEHICLE(thisArmoryAircraft.ownerVeh)
								SET_PERSONAL_VEHICLE_IN_AVENGER(TRUE)
							ELIF IS_VEHICLE_UPGRADABLE_PEGASUS_VEHICLE(thisArmoryAircraft.ownerVeh)
								SET_PEGASUS_VEHICLE_IN_AVENGER(TRUE)
							ENDIF	
						ENDIF
						IF NOT IS_PLAYER_ARMORY_AIRCRAFT_VEHICLE_WORKSHOP_PURCHASED(thisArmoryAircraft.pOwner)
							TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
						ENDIF	
					ENDIF	
					SET_DRIVER_ENTERING_SIMPLE_INTERIOR(FALSE)
					SET_DRIVER_ENTERING_ARMORY_AIRCRAFT(FALSE)
				ENDIF
			ENDIF
		ENDIF	
	ENDIF	
ENDPROC

FUNC BOOL ARE_ALL_PEDS_IN_VEHICLE_READY_FOR_EXIT_ARMORY_AIRCRAFT()
	INT i
	PLAYER_INDEX thePlayer
	REPEAT NUM_NETWORK_PLAYERS i
		thePlayer = INT_TO_PLAYERINDEX(i)
		
		IF thePlayer != PLAYER_ID()
		AND thePlayer != INVALID_PLAYER_INDEX()
		AND NETWORK_IS_PLAYER_A_PARTICIPANT(thePlayer)
			IF IS_NET_PLAYER_OK(thePlayer)
				IF DOES_ENTITY_EXIST(thisArmoryAircraft.ownerVeh)
					IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(thePlayer), thisArmoryAircraft.ownerVeh, FALSE)
						IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(thePlayer)].iBS, BS_ARMORY_AIRCRAFT_READY_TO_WARP_OUT_W_OWNER)
							#IF IS_DEBUG_BUILD
								PRINTLN("ARE_ALL_PEDS_IN_VEHICLE_READY_FOR_EXIT_ARMORY_AIRCRAFT: ", GET_PLAYER_NAME(thePlayer)," not ready not set flag")
							#ENDIF
							
							RETURN FALSE
						ENDIF
					ELSE
						IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(thePlayer), thisArmoryAircraft.ownerVeh, TRUE)
						OR IS_ENTITY_ATTACHED_TO_ENTITY(GET_PLAYER_PED(thePlayer), thisArmoryAircraft.ownerVeh)
							#IF IS_DEBUG_BUILD
								PRINTLN("ARE_ALL_PEDS_IN_VEHICLE_READY_FOR_EXIT_ARMORY_AIRCRAFT: ", GET_PLAYER_NAME(thePlayer)," entering or attached")
							#ENDIF
							
							RETURN FALSE
						ENDIF
					ENDIF
				ENDIF	
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_CAN_DRIVE_OUT_OF_ARMORY_AIRCRAFT()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND NOT g_MultiplayerSettings.g_bSuicide
	AND NOT IS_PASSENGER_READY_TO_ENTER_AVENGER_WITH_DRIVER(PLAYER_ID())
	AND GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) = HASH("GtaMloRoom001")
	AND NOT IS_LOCAL_PLAYER_WALKING_INTO_SIMPLE_INTERIOR()
	AND g_iSimpleInteriorState = SIMPLE_INT_STATE_IDLE
		DISABLE_SELECTOR_THIS_FRAME()
		IF NOT IS_WARNING_MESSAGE_ACTIVE()
		AND NOT IS_BIT_SET(g_iPersonalCarMod_BS, SHOP_PERSONAL_CAR_MOD_IMPOUNDED_PV_WARNING)
			SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_ARMORY_AIRCRAFT_READY_TO_WARP_OUT_W_OWNER)
		ELSE
			IF !IS_PLAYER_STARTED_TO_USE_PERSONAL_CAR_MOD(PLAYER_ID())
			AND NOT IS_PLAYER_TRIGGERED_ENTER_TO_BASE_FROM_AIRCRAFT_WITH_VEH(g_ownerOfArmoryAircraftPropertyIAmIn)
			AND NOT g_bPlayerLeavingCurrentInteriorInVeh
			AND NOT IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
			AND NOT IS_PLAYER_LEAVING_AVENGER_FROM_MOD_SHOP(PLAYER_ID())
				TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
				CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_ARMORY_AIRCRAFT_READY_TO_WARP_OUT_W_OWNER)
			ENDIF	
		ENDIF
	ELSE
		CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_ARMORY_AIRCRAFT_READY_TO_WARP_OUT_W_OWNER)
	ENDIF
ENDPROC

FUNC BOOL IS_PED_ENTER_OR_EXIT_VEHICLE(PED_INDEX pedToCheck, BOOL bCheckPedIsAlive = TRUE)
	IF bCheckPedIsAlive
	AND (NOT DOES_ENTITY_EXIST(pedToCheck) 
	OR IS_ENTITY_DEAD(pedToCheck))
		RETURN FALSE
	ENDIF
	
	IF GET_IS_TASK_ACTIVE(pedToCheck, CODE_TASK_EXIT_VEHICLE)
		RETURN TRUE
	ENDIF
	
	IF GET_VEHICLE_PED_IS_ENTERING(pedToCheck) != NULL
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANY_PARTICIPANT_TRYING_TO_ENTER_OR_EXIT_MY_VEHICLE()
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND NOT IS_PED_TRYING_TO_ENTER_OR_EXIT_VEHICLE(PLAYER_PED_ID())
			INT i
			REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
					IF i <> PARTICIPANT_ID_TO_INT()
						IF IS_NET_PLAYER_OK(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i)))
							IF IS_PED_ENTER_OR_EXIT_VEHICLE(GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))))
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

SCRIPT_TIMER sPhoneOnScreenTimer
FUNC BOOL CAN_PLAYER_DRIVE_OUT_OF_ARMORY_AIRCRAFT()
	IF NOT ARE_ALL_PEDS_IN_VEHICLE_READY_FOR_EXIT_ARMORY_AIRCRAFT()
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_ARMORY_AIRCRAFT - False NOT ARE_ALL_PEDS_IN_VEHICLE_READY_FOR_EXIT_ARMORY_AIRCRAFT")
		RETURN FALSE
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(pauseMenuInteractionsDelay)
	AND NOT HAS_NET_TIMER_EXPIRED(pauseMenuInteractionsDelay,1000,TRUE)
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_ARMORY_AIRCRAFT- False pause menu timer delay active")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_STARTED_TO_USE_PERSONAL_CAR_MOD(PLAYER_ID())
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_ARMORY_AIRCRAFT - False IS_PLAYER_STARTED_TO_USE_PERSONAL_CAR_MOD true")
		RETURN FALSE
	ENDIF
	
	IF PROPERTY_HAS_JUST_ACCEPTED_A_MISSION()
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_ARMORY_AIRCRAFT- False just accepted a mission")
		RETURN FALSE
	ENDIF
	
	IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) = PERFORMING_TASK
	OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) = WAITING_TO_START_TASK
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_ARMORY_AIRCRAFT - False leaving a vehicle")
		RETURN FALSE
	ENDIF
	
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		IF GET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_IsSeatShuffling)
			PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_ARMORY_AIRCRAFT False player shuffling")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP() 
	AND NOT IS_PLAYER_LEAVING_AVENGER_FROM_MOD_SHOP(PLAYER_ID())
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_ARMORY_AIRCRAFT- False IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP true")
		RETURN FALSE
	ENDIF
	
	IF IS_CUSTOM_MENU_ON_SCREEN()
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_ARMORY_AIRCRAFT False IS_CUSTOM_MENU_ON_SCREEN true")
		RETURN FALSE
	ENDIF
	
	IF IS_PHONE_ONSCREEN()
		IF NOT IS_BIT_SET(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_PHONE_ON_SCREEN)
			SET_BIT(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_PHONE_ON_SCREEN)
		ENDIF	
	ELSE
		IF IS_BIT_SET(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_PHONE_ON_SCREEN)
			IF !HAS_NET_TIMER_STARTED(sPhoneOnScreenTimer)
				START_NET_TIMER(sPhoneOnScreenTimer)
			ELSE
				IF HAS_NET_TIMER_EXPIRED(sPhoneOnScreenTimer, 2000)
					RESET_NET_TIMER(sPhoneOnScreenTimer)
					CLEAR_BIT(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_PHONE_ON_SCREEN)
				ELSE
					PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_ARMORY_AIRCRAFT False IS_PHONE_ONSCREEN waiting for timer")
					RETURN FALSE
				ENDIF
			ENDIF	
		ENDIF	
	ENDIF	
	
	IF IS_PHONE_ONSCREEN()
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_ARMORY_AIRCRAFT False IS_PHONE_ONSCREEN true")
		RETURN FALSE
	ENDIF
	
	IF IS_ANY_PARTICIPANT_TRYING_TO_ENTER_OR_EXIT_MY_VEHICLE()
		PRINTLN("HAS_ALL_PLAYERS_READY_TO_START_PERSONAL_CAR_MOD False IS_ANY_PARTICIPANT_TRYING_TO_ENTER_OR_EXIT_MY_VEHICLE false")
		RETURN FALSE
	ENDIF	
	
	IF IS_PLAYER_IN_CORONA()
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_ARMORY_AIRCRAFT False IS_PLAYER_IN_CORONA true")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

VEHICLE_INDEX lastVeh
PROC MAINTAIN_PLAYER_VEHICLE_LOCKS()
	IF IS_PAUSE_MENU_ACTIVE_EX()
	OR IS_SYSTEM_UI_BEING_DISPLAYED()
	OR IS_INTERACTION_MENU_OPEN()
	OR  IS_BROWSER_OPEN()
		REINIT_NET_TIMER(pauseMenuInteractionsDelay,TRUE)
	ENDIF

	g_bPlayerLeavingCurrentInteriorInVeh = FALSE
	IF thisArmoryAircraft.pOwner = PLAYER_ID()
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER)

				IF NOT IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), AVENGER)
					thisArmoryAircraft.ownerVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)
					
					IF IS_VEHICLE_MODEL(thisArmoryAircraft.ownerVeh, OPPRESSOR2)
						IF GET_IS_VEHICLE_ENGINE_RUNNING(thisArmoryAircraft.ownerVeh)
						AND g_iSimpleInteriorState = SIMPLE_INT_STATE_IDLE
							SET_VEHICLE_ENGINE_ON(thisArmoryAircraft.ownerVeh, FALSE, TRUE)
						ENDIF
						
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableStartEngine, TRUE)
					ELSE
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableStartEngine, FALSE)
					ENDIF
					
					IF thisArmoryAircraft.ownerVeh != lastVeh
						IF NETWORK_HAS_CONTROL_OF_ENTITY(thisArmoryAircraft.ownerVeh)
							lastVeh = thisArmoryAircraft.ownerVeh
							SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(thisArmoryAircraft.ownerVeh, FALSE)
							SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(thisArmoryAircraft.ownerVeh, FALSE)
							SET_VEHICLE_DOORS_LOCKED(thisArmoryAircraft.ownerVeh,VEHICLELOCK_UNLOCKED)
							PRINTLN("[SIMPLE_INTERIOR] - Aircraft MAINTAIN_PLAYER_VEHICLE_LOCKS: unlocking doors for remote players")
						ELSE
							NETWORK_REQUEST_CONTROL_OF_ENTITY(thisArmoryAircraft.ownerVeh)
						ENDIF
					ENDIF

					IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF (GET_IS_VEHICLE_ENGINE_RUNNING(thisArmoryAircraft.ownerVeh) OR IS_PLAYER_LEAVING_AVENGER_FROM_MOD_SHOP(PLAYER_ID()) OR IS_VEHICLE_MODEL(thisArmoryAircraft.ownerVeh, OPPRESSOR2))
						AND PLAYER_PED_ID() = GET_PED_IN_VEHICLE_SEAT(thisArmoryAircraft.ownerVeh)
							IF CAN_PLAYER_DRIVE_OUT_OF_ARMORY_AIRCRAFT()
								IF (GET_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_VEH_ACCELERATE) != 0
								OR GET_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_VEH_BRAKE) != 0
								OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
								OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
								OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
								OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
								OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
								OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
								OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
								OR IS_PLAYER_LEAVING_AVENGER_FROM_MOD_SHOP(PLAYER_ID()))
								AND NOT g_MultiplayerSettings.g_bSuicide	
								AND NOT IS_SELECTOR_ONSCREEN()
									
									IF IS_VEHICLE_MODEL(thisArmoryAircraft.ownerVeh, OPPRESSOR2)
										SET_VEHICLE_ENGINE_ON(thisArmoryAircraft.ownerVeh, TRUE, TRUE)
									ENDIF
							
									g_bPlayerLeavingCurrentInteriorInVeh = TRUE
									PRINTLN("MAINTAIN_PLAYER_VEHICLE_LOCKS - g_bPlayerLeavingCurrentInteriorInVeh = TRUE")
									IF NETWORK_HAS_CONTROL_OF_ENTITY(thisArmoryAircraft.ownerVeh)
										SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(thisArmoryAircraft.ownerVeh, TRUE) 
										SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(thisArmoryAircraft.ownerVeh, TRUE)
										
										DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
									ELSE
										NETWORK_REQUEST_CONTROL_OF_ENTITY(thisArmoryAircraft.ownerVeh)
									ENDIF
								ENDIF
							ELSE
								DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
								DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
							ENDIF
						ELSE
							IF  !GET_IS_VEHICLE_ENGINE_RUNNING(thisArmoryAircraft.ownerVeh)
								PRINTLN("MAINTAIN_PLAYER_VEHICLE_LOCKS - vehicle engine is not running")
							ENDIF	
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
						ENDIF
					ENDIF
				ENDIF	
			ENDIF	
		ELSE
			IF IS_VEHICLE_DRIVEABLE(lastVeh)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(lastVeh)
					IF NOT IS_VEHICLE_MODEL(lastVeh, AVENGER)
						SET_VEHICLE_DOORS_LOCKED(lastVeh,VEHICLELOCK_UNLOCKED)
						SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(lastVeh, TRUE)
				    	SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(lastVeh, PLAYER_ID(), FALSE)
						lastVeh = NULL
					ENDIF	
				ELSE
					NETWORK_REQUEST_CONTROL_OF_ENTITY(lastVeh)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND NOT IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), AVENGER)
			IF IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER)
				TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
			ENDIF
		ELSE
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				IF GET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PreventAutoShuffleToDriversSeat)
					SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PreventAutoShuffleToDriversSeat, FALSE)
				ENDIF	
			ENDIF	
		ENDIF	
	ENDIF
ENDPROC


SCRIPT_TIMER sTimerToLoadInterior
PROC KICK_PLAYERS_OUT_OF_VEHICLE_IN_AIRCRAFT_AFTER_AUTOWARP_AS_PASSENGER()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF IS_PASSENGER_READY_TO_ENTER_AVENGER_WITH_DRIVER(PLAYER_ID())
		//AND IS_PLAYER_IN_ARMORY_AIRCRAFT(thisArmoryAircraft.pOwner)
		AND IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
			IF GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) = HASH("GtaMloRoom001")

				IF NOT HAS_NET_TIMER_STARTED(sTimerToLoadInterior)
					START_NET_TIMER(sTimerToLoadInterior)	
				ELSE
					IF HAS_NET_TIMER_EXPIRED(sTimerToLoadInterior, 5000)
						IF IS_SCREEN_FADED_OUT()
							DO_SCREEN_FADE_IN(500)
						ELIF IS_SCREEN_FADED_IN()
							IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
								NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
							ENDIF	
							
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
								AND NOT IS_ENTITY_DEAD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))							
									IF NOT IS_PLAYER_ARMORY_AIRCRAFT_VEHICLE_WORKSHOP_PURCHASED(thisArmoryAircraft.pOwner)
										TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
									ENDIF	
									PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] KICK_PLAYERS_OUT_OF_VEHICLE_IN_AIRCRAFT_AFTER_AUTOWARP_AS_PASSENGER - true")
								ENDIF	
							ENDIF
							SET_DRIVER_ENTERING_SIMPLE_INTERIOR(FALSE)
							SET_PASSENGER_ENTERING_WITH_DRIVER_TO_AVENGER(FALSE)
							SET_PASSENGER_READY_TO_ENTER_AVENGER_WITH_DRIVER(FALSE)
							SET_DRIVER_ENTERING_ARMORY_AIRCRAFT(FALSE)
							RESET_NET_TIMER(sTimerToLoadInterior)		
							PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] KICK_PLAYERS_OUT_OF_VEHICLE_IN_AIRCRAFT_AFTER_AUTOWARP_AS_PASSENGER -  BS_SIMPLE_INTERIOR_GLOBAL_PLAYER_BD_DRIVER_ENTERING FALSE")
						ENDIF	
						
					ENDIF
				ENDIF
				
				IF IS_SCREEN_FADED_IN()
					IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
						NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
					ENDIF	
					
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						AND NOT IS_ENTITY_DEAD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))							
							IF NOT IS_PLAYER_ARMORY_AIRCRAFT_VEHICLE_WORKSHOP_PURCHASED(thisArmoryAircraft.pOwner)
								TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
							ENDIF	
							PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] KICK_PLAYERS_OUT_OF_VEHICLE_IN_AIRCRAFT_AFTER_AUTOWARP_AS_PASSENGER - screen already faded in true")
						ENDIF	
					ENDIF
					SET_DRIVER_ENTERING_SIMPLE_INTERIOR(FALSE)
					SET_PASSENGER_ENTERING_WITH_DRIVER_TO_AVENGER(FALSE)
					SET_PASSENGER_READY_TO_ENTER_AVENGER_WITH_DRIVER(FALSE)
					SET_DRIVER_ENTERING_ARMORY_AIRCRAFT(FALSE)
					RESET_NET_TIMER(sTimerToLoadInterior)		
					PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] KICK_PLAYERS_OUT_OF_VEHICLE_IN_AIRCRAFT_AFTER_AUTOWARP_AS_PASSENGER -  BS_SIMPLE_INTERIOR_GLOBAL_PLAYER_BD_DRIVER_ENTERING screen already faded in FALSE")	
				ENDIF		
				
			ENDIF
		ENDIF	
	ENDIF	
ENDPROC

PROC MAINTAIN_AVENGER_CCTV()
	IF g_SimpleInteriorData.bForceCCTVCleanup
		CLEANUP_CCTV(FALSE)
		g_SimpleInteriorData.bForceCCTVCleanup = FALSE
		EXIT
	ENDIF
	
	BOOL bBlocked = FALSE
	
	IF IS_OWNERS_ARMORY_AIRCRAFT_INSIDE_DEFUNCT_BASE(g_ownerOfArmoryAircraftPropertyIAmIn)
	OR (IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(g_ownerOfArmoryAircraftPropertyIAmIn)
	AND IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(g_ownerOfArmoryAircraftPropertyIAmIn))
	AND IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(g_ownerOfArmoryAircraftPropertyIAmIn)), AVENGER))
		bBlocked = TRUE
	ENDIF
	
	SET_CCTV_ACTIVITY_BLOCKED(thisArmoryAircraft.MPCCTVLocal, bBlocked)
	CLIENT_MAINTAIN_MP_CCTV(playerBD[NATIVE_TO_INT(PLAYER_ID())].MPCCTVClient, thisArmoryAircraft.MPCCTVLocal, thisArmoryAircraft.eSimpleInteriorID, DEFAULT, DEFAULT, DEFAULT)
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════╡ ARMORY AIRCRAFT BLIPS ╞══════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC MAINTAIN_ARMORY_AIRCRAFT_BLIPS()
	
	IF thisArmoryAircraft.pOwner = PLAYER_ID()
	AND NOT IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())
	AND IS_PLAYER_ARMORY_AIRCRAFT_WEAPON_WORKSHOP_PURCHASED(thisArmoryAircraft.pOwner)
	AND NOT g_bPlayerViewingTurretHud
		IF NOT DOES_BLIP_EXIST(thisArmoryAircraft.bWeaponWorkshopBlip)
			thisArmoryAircraft.bWeaponWorkshopBlip = CREATE_BLIP_FOR_COORD(<<517.6, 4752.2, -68.0>>)
			SET_BLIP_SPRITE(thisArmoryAircraft.bWeaponWorkshopBlip, RADAR_TRACE_GR_W_UPGRADE)
			SET_BLIP_NAME_FROM_TEXT_FILE(thisArmoryAircraft.bWeaponWorkshopBlip, "BLIP_567")
			SET_BLIP_DISPLAY(thisArmoryAircraft.bWeaponWorkshopBlip, DISPLAY_BOTH)
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(thisArmoryAircraft.bWeaponWorkshopBlip)
			REMOVE_BLIP(thisArmoryAircraft.bWeaponWorkshopBlip)
		ENDIF
	ENDIF
	
ENDPROC


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ MAIN LOGIC PROC ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL IS_LOCAL_PLAYER_WITHIN_ARMORY_AIRCRAFT_BOUNDS() 
	SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX bounds
	GET_SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX(thisArmoryAircraft.eSimpleInteriorID, bounds)
		
	BOOL bInBounds = IS_ENTITY_IN_AREA(PLAYER_PED_ID(), bounds.vInsideBBoxMin, bounds.vInsideBBoxMax)
	
	#IF IS_DEBUG_BUILD
	VECTOR vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
	CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_ARMORY_AIRCRAFT - IS_LOCAL_PLAYER_WITHIN_ARMORY_AIRCRAFT_BOUNDS - Player pos: ", vPlayerCoords)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_ARMORY_AIRCRAFT - IS_LOCAL_PLAYER_WITHIN_ARMORY_AIRCRAFT_BOUNDS - Bounds: ", bounds.vInsideBBoxMin, bounds.vInsideBBoxMax)
	#ENDIF
	
	RETURN bInBounds
ENDFUNC


OBJECT_INDEX oChairOne
OBJECT_INDEX oChairTwo
OBJECT_INDEX oChairThree
SCRIPT_TIMER sTintFailTimer
FUNC BOOL LOAD_INTERIOR_TINTS(BOOL bChairsOnly = FALSE)
	INT iTint = 0
	
	IF thisArmoryAircraft.pOwner = INVALID_PLAYER_INDEX()
		RETURN FALSE
	ENDIF
	
	IF !bChairsOnly
		IF NOT HAS_NET_TIMER_STARTED(sTintFailTimer)
			START_NET_TIMER(sTintFailTimer)
		ENDIF	
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(thisArmoryAircraft.pOwner)].propertyDetails.iArmourAircraftModBS,(PROPERTY_BROADCAST_ARMOUR_AIRCRAFT_BS_INTERIOR_0))
		iTint = 1
	ELIF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(thisArmoryAircraft.pOwner)].propertyDetails.iArmourAircraftModBS,(PROPERTY_BROADCAST_ARMOUR_AIRCRAFT_BS_INTERIOR_1))
		iTint = 2
	ELIF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(thisArmoryAircraft.pOwner)].propertyDetails.iArmourAircraftModBS,(PROPERTY_BROADCAST_ARMOUR_AIRCRAFT_BS_INTERIOR_2))
		iTint = 3
	ELIF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(thisArmoryAircraft.pOwner)].propertyDetails.iArmourAircraftModBS,(PROPERTY_BROADCAST_ARMOUR_AIRCRAFT_BS_INTERIOR_3))
		iTint = 4
	ELIF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(thisArmoryAircraft.pOwner)].propertyDetails.iArmourAircraftModBS,(PROPERTY_BROADCAST_ARMOUR_AIRCRAFT_BS_INTERIOR_4))
		iTint = 5
	ELIF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(thisArmoryAircraft.pOwner)].propertyDetails.iArmourAircraftModBS,(PROPERTY_BROADCAST_ARMOUR_AIRCRAFT_BS_INTERIOR_5))
		iTint = 6
	ELIF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(thisArmoryAircraft.pOwner)].propertyDetails.iArmourAircraftModBS,(PROPERTY_BROADCAST_ARMOUR_AIRCRAFT_BS_INTERIOR_6))
		iTint = 7
	ELIF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(thisArmoryAircraft.pOwner)].propertyDetails.iArmourAircraftModBS,(PROPERTY_BROADCAST_ARMOUR_AIRCRAFT_BS_INTERIOR_7))
		iTint = 8
	ELIF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(thisArmoryAircraft.pOwner)].propertyDetails.iArmourAircraftModBS,(PROPERTY_BROADCAST_ARMOUR_AIRCRAFT_BS_INTERIOR_8))
		iTint = 9
	ENDIF
	
	BOOL bReady = TRUE
	
	IF NOT bChairsOnly
		PRINTLN("LOAD_INTERIOR_TINTS iTint: ", iTint)
		
		IF !SET_TINT_INDEX_CLOSEST_BUILDING_OF_TYPE(<<523.0380,4750.5483,-67.6263>>, 50 , INT_TO_ENUM(MODEL_NAMES, HASH("xm_int_x17DLC_Tint_Ribs_01a")), iTint )
			PRINTLN("LOAD_INTERIOR_TINTS xm_int_x17DLC_Tint_Ribs_01a false")
			bReady = FALSE
		ENDIF
		IF !SET_TINT_INDEX_CLOSEST_BUILDING_OF_TYPE(<<523.0380,4750.5483,-67.6263>>, 50 , INT_TO_ENUM(MODEL_NAMES, HASH("xm_int_x17DLC_Tint_Fabric_01a")), iTint )
			PRINTLN("LOAD_INTERIOR_TINTS xm_int_x17DLC_Tint_Fabric_01a false")
			bReady = FALSE
		ENDIF
		IF !SET_TINT_INDEX_CLOSEST_BUILDING_OF_TYPE(<<523.0380,4750.5483,-67.6263>>, 50 , INT_TO_ENUM(MODEL_NAMES, HASH("xm_Int_x17DLC_Tint_Ribs_02a")), iTint )
			PRINTLN("LOAD_INTERIOR_TINTS xm_Int_x17DLC_Tint_Ribs_02a false")
			bReady = FALSE
		ENDIF
		IF !SET_TINT_INDEX_CLOSEST_BUILDING_OF_TYPE(<<523.0380,4750.5483,-67.6263>>, 50 , INT_TO_ENUM(MODEL_NAMES, HASH("xm_Int_x17DLC_Tint_Strut_01a")), iTint )
			PRINTLN("LOAD_INTERIOR_TINTS xm_Int_x17DLC_Tint_Strut_01a false")
			bReady = FALSE
		ENDIF
//		IF !SET_TINT_INDEX_CLOSEST_BUILDING_OF_TYPE(<<523.0380,4750.5483,-67.6263>>, 50 , INT_TO_ENUM(MODEL_NAMES, HASH("xm_Int_x17DLC_Tint_Pipes_02a")), iTint )
//			PRINTLN("LOAD_INTERIOR_TINTS xm_Int_x17DLC_Tint_Pipes_02a false")
//			bReady = FALSE
//		ENDIF
		IF !SET_TINT_INDEX_CLOSEST_BUILDING_OF_TYPE(<<523.0380,4750.5483,-67.6263>>, 50 , INT_TO_ENUM(MODEL_NAMES, HASH("xm_Int_x17DLC_Tint_Runners_01a")), iTint )
			PRINTLN("LOAD_INTERIOR_TINTS xm_Int_x17DLC_Tint_Runners_01a false")
			bReady = FALSE
		ENDIF
		IF !SET_TINT_INDEX_CLOSEST_BUILDING_OF_TYPE(<<523.0380,4750.5483,-67.6263>>, 50 , INT_TO_ENUM(MODEL_NAMES, HASH("xm_Int_x17DLC_Tint_Shell_01a")), iTint )
			PRINTLN("LOAD_INTERIOR_TINTS xm_Int_x17DLC_Tint_Shell_01a false")
			bReady = FALSE
		ENDIF
		IF !SET_TINT_INDEX_CLOSEST_BUILDING_OF_TYPE(<<514.8364,4750.5464,-69.3338>>, 50 , INT_TO_ENUM(MODEL_NAMES, HASH("xm_Int_x17DLC_Tint_seat_01b")), iTint )
			PRINTLN("LOAD_INTERIOR_TINTS xm_Int_x17DLC_Tint_Shell_01a false")
			bReady = FALSE
		ENDIF
		IF !SET_TINT_INDEX_CLOSEST_BUILDING_OF_TYPE(<<514.8749,4748.1060,-70.0003>>, 50 , INT_TO_ENUM(MODEL_NAMES, HASH("xm_Int_x17DLC_cctv_unit")), iTint )
			PRINTLN("LOAD_INTERIOR_TINTS xm_Int_x17DLC_Tint_Shell_01a false")
			bReady = FALSE
		ENDIF
		IF !SET_TINT_INDEX_CLOSEST_BUILDING_OF_TYPE(<<513.3058,4750.4287,-68.2650>>, 50 , INT_TO_ENUM(MODEL_NAMES, HASH("xm_Int_x17DLC_tint_netting_01a")), iTint )
			PRINTLN("LOAD_INTERIOR_TINTS xm_Int_x17DLC_Tint_Shell_01a false")
			bReady = FALSE
		ENDIF
		
		
		VEHICLE_INDEX avengerVeh
		IF g_OwnerOfArmoryAircraftPropertyIAmIn != INVALID_PLAYER_INDEX()
			avengerVeh =  g_vehOspreyLocal[NATIVE_TO_INT(g_OwnerOfArmoryAircraftPropertyIAmIn)]
		ENDIF	
		
		IF IS_PLAYER_ARMORY_AIRCRAFT_GUN_TURRET_PURCHASED(thisArmoryAircraft.pOwner)
			IF DOES_ENTITY_EXIST(avengerVeh)
			AND NOT IS_ENTITY_DEAD(avengerVeh)
				IF GET_VEHICLE_MOD(avengerVeh, MOD_ROOF) = 0
					PRINTLN("LOAD_INTERIOR_TINTS apply tint on front turret")
					IF !SET_TINT_INDEX_CLOSEST_BUILDING_OF_TYPE(<<523.0380,4750.5483,-67.6263>>, 50 , INT_TO_ENUM(MODEL_NAMES, HASH("xm_OSP_Control_Station_01b")), iTint )
						PRINTLN("LOAD_INTERIOR_TINTS xm_OSP_Control_Station_01b false")
						bReady = FALSE
					ENDIF
				ELSE
					PRINTLN("LOAD_INTERIOR_TINTS apply tint on all turrets")
					IF !SET_TINT_INDEX_CLOSEST_BUILDING_OF_TYPE(<<523.0380,4750.5483,-67.6263>>, 50 , INT_TO_ENUM(MODEL_NAMES, HASH("xm_OSP_Control_Station_01a")), iTint )
						PRINTLN("LOAD_INTERIOR_TINTS xm_OSP_Control_Station_01a false")
						bReady = FALSE
					ENDIF
					IF !SET_TINT_INDEX_CLOSEST_BUILDING_OF_TYPE(<<523.0380,4750.5483,-67.6263>>, 50 , INT_TO_ENUM(MODEL_NAMES, HASH("xm_OSP_Control_Station_01b")), iTint )
						PRINTLN("LOAD_INTERIOR_TINTS xm_OSP_Control_Station_01b false")
						bReady = FALSE
					ENDIF
					IF !SET_TINT_INDEX_CLOSEST_BUILDING_OF_TYPE(<<523.0380,4750.5483,-67.6263>>, 50 , INT_TO_ENUM(MODEL_NAMES, HASH("xm_OSP_Control_Station_01c")), iTint )
						PRINTLN("LOAD_INTERIOR_TINTS xm_OSP_Control_Station_01c false")
						bReady = FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
		
		IF IS_PLAYER_ARMORY_AIRCRAFT_WEAPON_WORKSHOP_PURCHASED(thisArmoryAircraft.pOwner)
			IF !SET_TINT_INDEX_CLOSEST_BUILDING_OF_TYPE(<<523.0380,4750.5483,-67.6263>>, 50 , INT_TO_ENUM(MODEL_NAMES, HASH("xm_OSP_Gun_Mod_Station")), iTint )
				PRINTLN("LOAD_INTERIOR_TINTS xm_OSP_Gun_Mod_Station false")
				bReady = FALSE
			ENDIF
		ENDIF
		
		IF IS_PLAYER_ARMORY_AIRCRAFT_VEHICLE_WORKSHOP_PURCHASED(thisArmoryAircraft.pOwner)
			IF !SET_TINT_INDEX_CLOSEST_BUILDING_OF_TYPE(<<523.0380,4750.5483,-67.6263>>, 50 , INT_TO_ENUM(MODEL_NAMES, HASH("xm_OSP_Vehicle_Mod_Station")), iTint )
				PRINTLN("LOAD_INTERIOR_TINTS xm_OSP_Vehicle_Mod_Station false")
				bReady = FALSE
			ENDIF
		ENDIF	
		
		IF NOT IS_PLAYER_BROWSING_ITEMS_IN_SHOP(GUN_SHOP_ARMORY_AVENGER)
			IF IS_BIT_SET(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_SET_WEAPON_WORK_BENCH_TIMECYCLE_MOD)
				CLEAR_TIMECYCLE_MODIFIER()
				CLEAR_BIT(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_SET_WEAPON_WORK_BENCH_TIMECYCLE_MOD)
			ENDIF
			
			IF iTint != 0
				TEXT_LABEL_31 sTimerCycle = "mp_x17dlc_int_01_tint"
				sTimerCycle += iTint
				PRINTLN("LOAD_INTERIOR_TINTS time cycle: ", sTimerCycle)
				SET_TIMECYCLE_MODIFIER(sTimerCycle)
			ENDIF
		ENDIF
		
	ELSE
		IF IS_PLAYER_ARMORY_AIRCRAFT_GUN_TURRET_PURCHASED(thisArmoryAircraft.pOwner)
			
			IF DOES_ENTITY_EXIST(oChairOne)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(oChairOne)
					IF GET_OBJECT_TINT_INDEX(oChairOne) != iTint
						SET_OBJECT_TINT_INDEX(oChairOne, iTint)
					ENDIF	
				ENDIF	
			ELSE
				oChairOne = GET_CLOSEST_OBJECT_OF_TYPE(<<513.2440,4749.9961,-69.3940>>, 5 ,INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_avengerchair")), FALSE, FALSE, FALSE)
			ENDIF
			
			IF DOES_ENTITY_EXIST(oChairTwo)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(oChairTwo)
					IF GET_OBJECT_TINT_INDEX(oChairTwo) != iTint
						SET_OBJECT_TINT_INDEX(oChairTwo, iTint)
					ENDIF	
				ENDIF	
			ELSE
				oChairTwo = GET_CLOSEST_OBJECT_OF_TYPE(<<512.4060,4749.7319,-69.3940>>, 5 ,INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_avengerchair")), FALSE, FALSE, FALSE)
			ENDIF
			
			IF DOES_ENTITY_EXIST(oChairThree)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(oChairThree)
					IF GET_OBJECT_TINT_INDEX(oChairThree) != iTint
						SET_OBJECT_TINT_INDEX(oChairThree, iTint)
					ENDIF	
				ENDIF	
			ELSE
				oChairThree = GET_CLOSEST_OBJECT_OF_TYPE(<<515.9560,4751.0449,-69.3940>>, 5 ,INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_avengerchair")), FALSE, FALSE, FALSE)
			ENDIF
		ENDIF	
	ENDIF
	
	IF !bChairsOnly
		IF HAS_NET_TIMER_STARTED(sTintFailTimer)
			IF HAS_NET_TIMER_EXPIRED(sTintFailTimer, 10000)
				IF !bReady 
					bReady = TRUE
					PRINTLN("LOAD_INTERIOR_TINTS - hitting fail timer...")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bReady
ENDFUNC	

#IF IS_DEBUG_BUILD
SCRIPT_TIMER sEntryFadeTimer
#ENDIF

PROC MAINTAIN_INTERIOR_LOAD()

	VECTOR vInteriorPosition
	FLOAT fInteriorHeading
	TEXT_LABEL_63 sInteriorType
	
	IF NOT IS_BIT_SET(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_INTERIOR_REFRESHED_ON_INIT)
		IF NOT thisArmoryAircraft.bScriptRelaunched
			
			GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION(thisArmoryAircraft.eSimpleInteriorID, sInteriorType, vInteriorPosition, fInteriorHeading)
			thisArmoryAircraft.interiorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(vInteriorPosition, sInteriorType)
			
			IF IS_VALID_INTERIOR(thisArmoryAircraft.interiorIndex) AND IS_INTERIOR_READY(thisArmoryAircraft.interiorIndex)
				BOOL bReady = TRUE
				
				IF bReady
					IF IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
						FORCE_ROOM_FOR_ENTITY(PLAYER_PED_ID(), thisArmoryAircraft.interiorIndex, ARMORY_AIRCRAFT_ROOM_KEY)
						PRINTLN("[SIMPLE_INTERIOR] AM_MP_ARMORY_AIRCRAFT - MAINTAIN_INTERIOR_LOAD - Forcing room for player, we are inside the AOC.")
					ELSE
						PRINTLN("[SIMPLE_INTERIOR] AM_MP_ARMORY_AIRCRAFT - MAINTAIN_INTERIOR_LOAD - Preventing room force, not inside the AOC yet.")
					ENDIF
					
					REPIN_AND_REFRESH_SIMPLE_INTERIOR()
					
					SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
					SET_BIT(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_INTERIOR_REFRESHED_ON_INIT)
					CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] AM_MP_ARMORY_AIRCRAFT - MAINTAIN_INTERIOR_LOAD - Setting BS_ARMORY_AIRCRAFT_INTERIOR_REFRESHED_ON_INIT")
					
					RESET_NET_TIMER(thisArmoryAircraft.tPinInMemTimer)
				ENDIF
			ELSE
				IF NOT HAS_NET_TIMER_STARTED(thisArmoryAircraft.tPinInMemTimer)
					PRINTLN("[SIMPLE_INTERIOR] AM_MP_ARMORY_AIRCRAFT - MAINTAIN_INTERIOR_LOAD - Pinning interior with ID: ", NATIVE_TO_INT(thisArmoryAircraft.interiorIndex))
					PIN_INTERIOR_IN_MEMORY_VALIDATE(thisArmoryAircraft.interiorIndex)
					START_NET_TIMER(thisArmoryAircraft.tPinInMemTimer)
				ENDIF
				
				IF HAS_NET_TIMER_EXPIRED(thisArmoryAircraft.tPinInMemTimer, 12500)
					RESET_NET_TIMER(thisArmoryAircraft.tPinInMemTimer)
				ENDIF
				
				PRINTLN("[SIMPLE_INTERIOR] AM_MP_ARMORY_AIRCRAFT - MAINTAIN_INTERIOR_LOAD - Waiting for a valid and ready interior with ID: ", NATIVE_TO_INT(thisArmoryAircraft.interiorIndex), " valid? ", 
					IS_VALID_INTERIOR(thisArmoryAircraft.interiorIndex), " ready? ", IS_INTERIOR_READY(thisArmoryAircraft.interiorIndex), " disabled? ", IS_INTERIOR_DISABLED(thisArmoryAircraft.interiorIndex))
			ENDIF
		ELSE
			SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
			SET_BIT(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_INTERIOR_REFRESHED_ON_INIT)
			CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] AM_MP_ARMORY_AIRCRAFT - MAINTAIN_INTERIOR_LOAD - Setting BS_ARMORY_AIRCRAFT_INTERIOR_REFRESHED_ON_INIT")
		ENDIF	
	ENDIF
	
	IF CAN_SIMPLE_INTERIOR_CHILD_SCRIPT_TAKE_CONTROL()	
	AND IS_BIT_SET(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_INTERIOR_REFRESHED_ON_INIT)
		BOOL bReady = TRUE

		IF NOT IS_BIT_SET(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_DATA_LOAD_SCENE_STARTED)
			VECTOR vBaseCoords
			FLOAT fBaseHeading
			
			SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX bounds
			GET_SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX(thisArmoryAircraft.eSimpleInteriorID, bounds)
			GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION(thisArmoryAircraft.eSimpleInteriorID, sInteriorType, vBaseCoords, fBaseHeading)			
			vBaseCoords.z = vBaseCoords.z + 1
			//NEW_LOAD_SCENE_START_SPHERE(vBaseCoords , 30.0)
			SET_BIT(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_DATA_LOAD_SCENE_STARTED)
			PRINTLN("[SIMPLE_INTERIOR] AM_MP_ARMORY_AIRCRAFT - MAINTAIN_INTERIOR_LOAD - Setting BS_ARMORY_AIRCRAFT_DATA_LOAD_SCENE_STARTED")
			
			bReady = FALSE
		ELSE
			IF IS_NEW_LOAD_SCENE_ACTIVE()
				IF NOT IS_NEW_LOAD_SCENE_LOADED()
					PRINTLN("[SIMPLE_INTERIOR] AM_MP_ARMORY_AIRCRAFT - MAINTAIN_INTERIOR_LOAD - BS_ARMORY_AIRCRAFT_DATA_LOAD_SCENE_STARTED - IS_NEW_LOAD_SCENE_LOADED = FALSE")
					bReady = FALSE
				ELSE
					GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION(thisArmoryAircraft.eSimpleInteriorID, sInteriorType, vInteriorPosition, fInteriorHeading)
					thisArmoryAircraft.interiorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(vInteriorPosition, sInteriorType)
					
					IF IS_VALID_INTERIOR(thisArmoryAircraft.interiorIndex)
					AND IS_INTERIOR_READY(thisArmoryAircraft.interiorIndex)
					AND IS_LOCAL_PLAYER_WITHIN_ARMORY_AIRCRAFT_BOUNDS()
						PRINTLN("[SIMPLE_INTERIOR] AM_MP_ARMORY_AIRCRAFT - MAINTAIN_INTERIOR_LOAD - Inside base bounds, load scene is ready.")
						NEW_LOAD_SCENE_STOP()
					ELSE
						SET_BIT(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_KILL_LOAD_SCENE_WHEN_INSIDE)
						PRINTLN("[SIMPLE_INTERIOR] AM_MP_ARMORY_AIRCRAFT - MAINTAIN_INTERIOR_LOAD - Setting BS_ARMORY_AIRCRAFT_KILL_LOAD_SCENE_WHEN_INSIDE")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_VALID_INTERIOR(thisArmoryAircraft.interiorIndex)
			IF !IS_INTERIOR_READY(thisArmoryAircraft.interiorIndex)
				PRINTLN("[SIMPLE_INTERIOR] AM_MP_ARMORY_AIRCRAFT - MAINTAIN_INTERIOR_LOAD - interior is not ready yet")
				bReady = FALSE
			ENDIF
		ENDIF	
		
		IF NOT LOAD_INTERIOR_TINTS()
			bReady = FALSE
			PRINTLN("[SIMPLE_INTERIOR] AM_MP_ARMORY_AIRCRAFT - MAINTAIN_INTERIOR_LOAD - waiting on LOAD_INTERIOR_TINTS")
		ENDIF
		
		IF g_SimpleInteriorData.bForceInteriorScriptToReloadInterior
			bReady = FALSE
			PRINTLN("[SIMPLE_INTERIOR] AM_MP_ARMORY_AIRCRAFT - bForceInteriorScriptToReloadInterior is true")
		ENDIF
		
		IF IS_PLAYER_ARMORY_AIRCRAFT_VEHICLE_WORKSHOP_PURCHASED(thisArmoryAircraft.pOwner)
			IF !MAINTAIN_LAUNCHING_CARMOD_SCRIPT()
				bReady = FALSE
				PRINTLN("[SIMPLE_INTERIOR] AM_MP_ARMORY_AIRCRAFT - MAINTAIN_LAUNCHING_CARMOD_SCRIPT - false")
			ENDIF
		ENDIF
		
		IF bReady
			IF NOT IS_BIT_SET(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_INIT_FADE_IN_TIMER)
				PRINTLN("[SIMPLE_INTERIOR] AM_MP_ARMORY_AIRCRAFT - start the timer next frame")
				SET_BIT(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_INIT_FADE_IN_TIMER)
				bReady = FALSE
			ENDIF
		ENDIF	
		
		IF IS_BIT_SET(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_INIT_FADE_IN_TIMER)
			IF NOT HAS_NET_TIMER_STARTED(sFadeInTimer)
				START_NET_TIMER(sFadeInTimer)
			ELSE	
				IF HAS_NET_TIMER_EXPIRED(sFadeInTimer, 1000)
					PRINTLN("[SIMPLE_INTERIOR] AM_MP_ARMORY_AIRCRAFT - we hit 1 seconds")
				ELSE
					PRINTLN("[SIMPLE_INTERIOR] AM_MP_ARMORY_AIRCRAFT - waiting on start timer")
					RESET_ADAPTATION(1)
					bReady = FALSE
				ENDIF
			ENDIF	
		ENDIF
				
		IF bReady
			
			#IF IS_DEBUG_BUILD
			IF g_bAddFadeDelay
				IF NOT HAS_NET_TIMER_STARTED(sEntryFadeTimer)
					START_NET_TIMER(sEntryFadeTimer)
					EXIT
				ELSE
					IF HAS_NET_TIMER_EXPIRED(sEntryFadeTimer, 25000)
						// This is important, let internal script know that its child script is ready.
						IF IS_NET_PLAYER_OK(PLAYER_ID())
							FORCE_ROOM_FOR_ENTITY(PLAYER_PED_ID(), thisArmoryAircraft.interiorIndex, ARMORY_AIRCRAFT_ROOM_KEY)
							PRINTLN("[SIMPLE_INTERIOR] AM_MP_ARMORY_AIRCRAFT - force player to room")
						ENDIF	
						SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
						SET_SIMPLE_INTERIOR_CHILD_SCRIPT_READY(TRUE) 
						SET_LOCAL_PLAYER_IN_SIMPLE_INTERIOR(TRUE, thisArmoryAircraft.eSimpleInteriorID)
						SET_CONCEAL_PLAYER_FOR_PILOT_SEAT_TRANS(FALSE)
						IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
							#IF IS_DEBUG_BUILD
								PRINTLN("[SIMPLE_INTERIOR][ARMORY_AIRCRAFT] AM_MP_ARMORY_AIRCRAFT - Cleared Wanted Level.")
							#ENDIF
							SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
						ENDIF
						SET_ARMORY_AIRCRAFT_STATE(ARMORY_AIRCRAFT_STATE_IDLE)
						g_bAddFadeDelay = FALSE
					ELSE
						EXIT
					ENDIF
				ENDIF	
			ENDIF
			#ENDIF
			
			// This is important, let internal script know that its child script is ready.
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				FORCE_ROOM_FOR_ENTITY(PLAYER_PED_ID(), thisArmoryAircraft.interiorIndex, ARMORY_AIRCRAFT_ROOM_KEY)
				PRINTLN("[SIMPLE_INTERIOR] AM_MP_ARMORY_AIRCRAFT - force player to room")
			ENDIF	
			SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
			SET_SIMPLE_INTERIOR_CHILD_SCRIPT_READY(TRUE) 
			SET_LOCAL_PLAYER_IN_SIMPLE_INTERIOR(TRUE, thisArmoryAircraft.eSimpleInteriorID)
			SET_CONCEAL_PLAYER_FOR_PILOT_SEAT_TRANS(FALSE)
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
				#IF IS_DEBUG_BUILD
					PRINTLN("[SIMPLE_INTERIOR][ARMORY_AIRCRAFT] AM_MP_ARMORY_AIRCRAFT - Cleared Wanted Level.")
				#ENDIF
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
			ENDIF
			SET_ARMORY_AIRCRAFT_STATE(ARMORY_AIRCRAFT_STATE_IDLE)
		ENDIF
	ELSE
		BOOL bResult = IS_BIT_SET(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_INTERIOR_REFRESHED_ON_INIT)
		PRINTLN("[SIMPLE_INTERIOR] AM_MP_ARMORY_AIRCRAFT - MAINTAIN_INTERIOR_LOAD - CAN_SIMPLE_INTERIOR_CHILD_SCRIPT_TAKE_CONTROL = FALSE & BS_ARMORY_AIRCRAFT_DATA_INTERIOR_REFRESHED_ON_INIT = ", bResult)
	ENDIF
ENDPROC

PROC RUN_CLIENT_ACTIVITIES()
	interiorStruct.pFactoryOwner 	= thisArmoryAircraft.pOwner
	interiorStruct.vInteriorPos 	= <<1103.562,-3014.000,-40.000>>
	interiorStruct.iInterior 		= GET_HASH_KEY("FACTORY_TRUCK")
//	IF interiorStruct.iInterior != 0
		INIT_ACTIVITY_LAUNCH_SCRIPT_CHECK_FOR_OSPREY(activityControllerStruct, activityCheck, serverBD.activityProps, interiorStruct)
		MAINTAIN_APARTMENT_ACTIVITIES_CREATOR(activityControllerStruct, activityCheck, serverBD.activityProps, activityPeds)
//	ENDIF
ENDPROC

PROC MAINTAIN_BASE_ENTRY_FROM_ARMORY_AIRCRAFT_ON_FOOT()
	IF (HAS_EXIT_TO_BASE_FROM_AIRCRAFT_TRIGGERED()
	OR IS_BIT_SET(g_SimpleInteriorData.iThirdBS, BS3_SIMPLE_INTERIOR_GLOBAL_DATA_ALL_EXIT_BASE_FROM_AIRCRAFT_TRIGGERED))
	AND NOT IS_PLAYER_IN_CORONA()
	AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND IS_SIMPLE_INTERIOR_EXT_SCRIPT_RUNNING(GET_SIMPLE_INTERIOR_ID_FROM_DEFUNCT_BASE_ID(GET_OWNED_DEFUNCT_BASE(thisArmoryAircraft.pOwner)))
		IF  IS_SCREEN_FADED_OUT()
//			IF NOT HAS_PLAYER_START_EXIT_TO_BUNKER_FROM_TRUCK()
//				DO_SCREEN_FADE_OUT(500)
//			ENDIF	
//		ELSE	
			NET_SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
			IF thisArmoryAircraft.pOwner != INVALID_PLAYER_INDEX()
			
				IF thisArmoryAircraft.pOwner != PLAYER_ID()
					IF NOT IS_TRANS_ON_FOOT_BETWEEN_AIRCRAFT_BASE_STARTED(thisArmoryAircraft.pOwner)
						SET_PLAYER_NEEDS_TO_BE_CONCAEL_FOR_BASE_AIRCRAFT_TRANS(TRUE)
						SET_START_EXIT_TO_BASE_FROM_AIRCRAFT(TRUE)
						RESET_AIRCRAFT_EXT_SCRIPT(TRUE)
						globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = thisArmoryAircraft.pOwner
						SET_SIMPLE_INTERIOR_AUTOWARP_OVERRIDE(thisArmoryAircraft.pOwner)
						PRINTLN("MAINTAIN_BASE_ENTRY_FROM_ARMORY_AIRCRAFT_ON_FOOT - g_SimpleInteriorData.iOverrideAutoWarpToPlayersProperty: ", g_SimpleInteriorData.iOverrideAutoWarpToPlayersProperty)
						IF g_SimpleInteriorData.iOverrideAutoWarpToPlayersProperty = -1 
							SET_SIMPLE_INTERIOR_AUTOWARP_OVERRIDE(thisArmoryAircraft.pOwner)
							PRINTLN("MAINTAIN_BASE_ENTRY_FROM_ARMORY_AIRCRAFT_ON_FOOT - g_SimpleInteriorData.iOverrideAutoWarpToPlayersProperty is -1 setting to : ", NATIVE_TO_INT(thisArmoryAircraft.pOwner))
						ENDIF	
						IF IS_OWNERS_ARMORY_AIRCRAFT_INSIDE_DEFUNCT_BASE(thisArmoryAircraft.pOwner)
							SET_EXIT_TO_BASE_FROM_AIRCRAFT_TRIGGERED(TRUE)
						ENDIF	
						g_SimpleInteriorData.bForceTerminateInteriorScript = TRUE
						PRINTLN("MAINTAIN_BASE_ENTRY_FROM_ARMORY_AIRCRAFT_ON_FOOT - remote player Triggered")
					ENDIF
				ELSE
					SET_PLAYER_NEEDS_TO_BE_CONCAEL_FOR_BASE_AIRCRAFT_TRANS(TRUE)	
					IF IS_OWNERS_ARMORY_AIRCRAFT_INSIDE_DEFUNCT_BASE(thisArmoryAircraft.pOwner)
						SET_EXIT_TO_BASE_FROM_AIRCRAFT_TRIGGERED(TRUE)
					ENDIF
					SET_START_EXIT_TO_BASE_FROM_AIRCRAFT(TRUE)
					RESET_AIRCRAFT_EXT_SCRIPT(TRUE)
					SET_TRANS_ON_FOOT_BETWEEN_AIRCRAFT_BASE_STARTED(TRUE)
					g_SimpleInteriorData.bForceTerminateInteriorScript = TRUE
					PRINTLN("MAINTAIN_BASE_ENTRY_FROM_ARMORY_AIRCRAFT_ON_FOOT - owner Triggered")
				ENDIF
			ENDIF	
		ENDIF	
	ELSE
		IF thisArmoryAircraft.pOwner != INVALID_PLAYER_INDEX()
			IF IS_SIMPLE_INTERIOR_EXT_SCRIPT_RUNNING(GET_SIMPLE_INTERIOR_ID_FROM_DEFUNCT_BASE_ID(GET_OWNED_DEFUNCT_BASE(thisArmoryAircraft.pOwner)))
				IF GET_FRAME_COUNT() % 60 = 0
					PRINTLN("MAINTAIN_BASE_ENTRY_FROM_ARMORY_AIRCRAFT_ON_FOOT exterior is not running ?? simple_interior_id: ", GET_SIMPLE_INTERIOR_ID_FROM_DEFUNCT_BASE_ID(GET_OWNED_DEFUNCT_BASE(thisArmoryAircraft.pOwner)))
				ENDIF	
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

PROC MAINTAIN_ENTER_BASE_FROM_ARMORY_AIRCRAFT_IN_VEHICLE()
	IF g_ownerOfArmoryAircraftPropertyIAmIn != INVALID_PLAYER_INDEX()
	AND IS_SIMPLE_INTERIOR_EXT_SCRIPT_RUNNING(GET_SIMPLE_INTERIOR_ID_FROM_DEFUNCT_BASE_ID(GET_OWNED_DEFUNCT_BASE(g_ownerOfArmoryAircraftPropertyIAmIn)))
		IF PLAYER_ID() = g_ownerOfArmoryAircraftPropertyIAmIn		
			IF IS_OWNERS_ARMORY_AIRCRAFT_INSIDE_DEFUNCT_BASE(PLAYER_ID())
				IF IS_PLAYER_TRIGGERED_ENTER_TO_BASE_FROM_AIRCRAFT_WITH_VEH(PLAYER_ID())
				//OR IS_TRUCK_PERSONAL_CAR_MOD_LEAVE_PROPERTY(PLAYER_ID())
					IF CAN_PLAYER_DRIVE_OUT_OF_ARMORY_AIRCRAFT()
						IF NOT IS_SCREEN_FADED_OUT()
							DO_SCREEN_FADE_OUT(500)
							NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
							SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW_IF_NEEDED()
						ELIF IS_SCREEN_FADED_OUT()
						AND !IS_PLAYER_CONTROL_ON(PLAYER_ID())
						AND NOT IS_ENTER_BASE_FROM_AIRCRAFT_WITH_VEH_TRIGGERED_IN_PROGRESS()
							VEHICLE_INDEX pedVeh
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								pedVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
								IF DOES_ENTITY_EXIST(pedVeh)
								AND NOT IS_ENTITY_DEAD(pedVeh)
									
									FREEZE_ENTITY_POSITION(pedVeh,TRUE)
									VECTOR vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
									vPlayerCoords = <<vPlayerCoords.x, vPlayerCoords.y, vPlayerCoords.z - 50>> + <<(NATIVE_TO_INT(PLAYER_ID()) * 32),0,0>>
									SET_ENTITY_COORDS(pedVeh , vPlayerCoords)
									SET_TRANS_DONT_CLEAR_ENTERING_FLAG(TRUE)		
									SET_ENTER_BASE_FROM_AIRCRAFT_WITH_VEH_TRIGGERED_IN_PROGRESS(TRUE)
									RESET_AIRCRAFT_EXT_SCRIPT(TRUE)
									PRINTLN("MAINTAIN_ENTER_BASE_FROM_ARMORY_AIRCRAFT_IN_VEHICLE 1 - Triggered")
		
									
								ENDIF	
							ENDIF
						ENDIF	
					ENDIF	
				ENDIF					
			ENDIF			
		ELSE
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND NOT IS_PED_TRYING_TO_ENTER_OR_EXIT_VEHICLE(PLAYER_PED_ID())
			AND IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF NOT IS_PLAYER_IN_CORONA()
					IF GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) = GET_PLAYER_PED(g_ownerOfArmoryAircraftPropertyIAmIn)
					AND IS_PLAYER_TRIGGERED_ENTER_TO_BASE_FROM_AIRCRAFT_WITH_VEH(g_ownerOfArmoryAircraftPropertyIAmIn)
						LAUNCH_CRIMINAL_STARTER_PACK_BROWSER(FALSE)
						IF NOT IS_SCREEN_FADED_OUT()
							DO_SCREEN_FADE_OUT(500)
						ELIF IS_SCREEN_FADED_OUT()
							SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PreventAutoShuffleToDriversSeat, TRUE)
							NET_SET_PLAYER_CONTROL(PLAYER_ID() , FALSE)
							SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW_IF_NEEDED()
							SET_PLAYER_TRIGGERED_ENTER_TO_BASE_FROM_AIRCRAFT_WITH_VEH(TRUE)
							SET_ENTER_BASE_FROM_AIRCRAFT_WITH_VEH_TRIGGERED_IN_PROGRESS(TRUE)
							RESET_AIRCRAFT_EXT_SCRIPT(TRUE)
						ENDIF	
					ENDIF
				ELSE
					TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
				ENDIF
			ENDIF
		ENDIF	
	ELSE
		IF g_ownerOfArmoryAircraftPropertyIAmIn != INVALID_PLAYER_INDEX()
			IF IS_SIMPLE_INTERIOR_EXT_SCRIPT_RUNNING(GET_SIMPLE_INTERIOR_ID_FROM_DEFUNCT_BASE_ID(GET_OWNED_DEFUNCT_BASE(g_ownerOfArmoryAircraftPropertyIAmIn)))
				IF GET_FRAME_COUNT() % 60 = 0
					PRINTLN("MAINTAIN_ENTER_BASE_FROM_ARMORY_AIRCRAFT_IN_VEHICLE exterior is not running ?? simple_interior_id: ", GET_SIMPLE_INTERIOR_ID_FROM_DEFUNCT_BASE_ID(GET_OWNED_DEFUNCT_BASE(g_ownerOfArmoryAircraftPropertyIAmIn)))
				ENDIF	
			ENDIF
		ENDIF	
	ENDIF							
ENDPROC


PROC BLOCK_VEHICLE_TURRET_SEAT()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
			IF IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_ENTERING_IN(PLAYER_PED_ID(), TRUE), HALFTRACK)
				IF IS_TURRET_SEAT(GET_VEHICLE_PED_IS_ENTERING_IN(PLAYER_PED_ID(), TRUE), GET_SEAT_PED_IS_IN(PLAYER_PED_ID()))
					IF NOT IS_PLAYER_TELEPORT_ACTIVE()
						IF NET_WARP_TO_COORD(<<527.0817, 4752.2305, -69.9960>>, 89.2540)
							PRINTLN("BLOCK_VEHICLE_TURRET_SEAT - warp player")
							EXIT
						ENDIF
					ENDIF	
				ENDIF	
			ENDIF	
		ENDIF
		IF GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID()) != NULL
			IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID()) ) = HALFTRACK
				IF !GET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableTurretOrRearSeatPreference)
					SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableTurretOrRearSeatPreference, TRUE)
				ENDIF	
			ENDIF	
		ENDIF
		VEHICLE_INDEX nearbyVehs[1]
		GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), nearbyVehs)
		IF DOES_ENTITY_EXIST(nearbyVehs[0])
		AND IS_ENTITY_A_VEHICLE(nearbyVehs[0])
			IF GET_ENTITY_MODEL(nearbyVehs[0]) = HALFTRACK
				IF !GET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableTurretOrRearSeatPreference)
					SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableTurretOrRearSeatPreference, TRUE)
				ENDIF
			ENDIF
		ENDIF
		
		IF PLAYER_ID() = thisArmoryAircraft.pOwner
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PlayersDontDragMeOutOfCar, TRUE)
		ENDIF	
	ENDIF	
ENDPROC 

PROC MAINTAIN_GET_OUT_OF_OWNERS_PERSONAL_VEHICLE()
	VEHICLE_INDEX vehIndex
	PED_INDEX DriverPedID
	
	IF thisArmoryAircraft.pOwner != PLAYER_ID()
		IF IS_NET_PLAYER_OK(PLAYER_ID())
		AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
				vehIndex = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)
				
				IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_GLOBAL_PLAYER_BD_DRIVER_ENTERING)
					IF IS_VEHICLE_A_PERSONAL_VEHICLE(vehIndex)
						IF thisArmoryAircraft.pOwner  != INVALID_PLAYER_INDEX()
							IF IS_NET_PLAYER_OK(thisArmoryAircraft.pOwner, FALSE, TRUE)
								IF NOT IS_VEHICLE_SEAT_FREE(vehIndex)
									DriverPedID = GET_PED_IN_VEHICLE_SEAT(vehIndex, VS_DRIVER)
									
									IF DOES_ENTITY_EXIST(DriverPedID)
									AND IS_PED_A_PLAYER(DriverPedID)
										IF NETWORK_GET_PLAYER_INDEX_FROM_PED(DriverPedID) != thisArmoryAircraft.pOwner
											IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehIndex, FALSE)
												IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
												AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
													TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
													
													PRINTLN("[SIMPLE_INTERIOR] AM_MP_ARMORY_AIRCRAFT - MAINTAIN_GET_OUT_OF_OWNERS_PERSONAL_VEHICLE: driver isn't owner, leave vehicle")
												ENDIF
											ELIF GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID()) = vehIndex
												CLEAR_PED_TASKS(PLAYER_PED_ID())
												
												PRINTLN("[SIMPLE_INTERIOR] AM_MP_ARMORY_AIRCRAFT - MAINTAIN_GET_OUT_OF_OWNERS_PERSONAL_VEHICLE: driver isn't owner, clear tasks")
											ENDIF
										ENDIF
									ENDIF
								ELSE
									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehIndex, FALSE)
										IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
										AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
											TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
											
											PRINTLN("[SIMPLE_INTERIOR] AM_MP_ARMORY_AIRCRAFT - MAINTAIN_GET_OUT_OF_OWNERS_PERSONAL_VEHICLE: no driver, leave vehicle")
											
											IF !CAN_SIMPLE_INTERIOR_CHILD_SCRIPT_TAKE_CONTROL()
												SET_SIMPLE_INTERIOR_CHILD_SCRIPT_CAN_TAKE_CONTROL(TRUE)
											ENDIF
										ENDIF
									ELIF GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID()) = vehIndex
										CLEAR_PED_TASKS(PLAYER_PED_ID())
										
										PRINTLN("[SIMPLE_INTERIOR] AM_MP_ARMORY_AIRCRAFT - MAINTAIN_GET_OUT_OF_OWNERS_PERSONAL_VEHICLE: no driver, clear tasks")
									ENDIF
								ENDIF
							ELSE
								IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehIndex, FALSE)
									IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
									AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
										TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
										
										PRINTLN("[SIMPLE_INTERIOR] AM_MP_ARMORY_AIRCRAFT - MAINTAIN_GET_OUT_OF_OWNERS_PERSONAL_VEHICLE: getting out owner is not ok, leave vehicle")
									ENDIF
								ELIF GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID()) = vehIndex
									CLEAR_PED_TASKS(PLAYER_PED_ID())
									
									PRINTLN("[SIMPLE_INTERIOR] AM_MP_ARMORY_AIRCRAFT - MAINTAIN_GET_OUT_OF_OWNERS_PERSONAL_VEHICLE: no driver, clear tasks")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF thisArmoryAircraft.pOwner != INVALID_PLAYER_INDEX()
			IF IS_NET_PLAYER_OK(thisArmoryAircraft.pOwner)
				IF NOT IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(thisArmoryAircraft.pOwner), FALSE)
				OR NOT IS_VEHICLE_SEAT_FREE(GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(thisArmoryAircraft.pOwner)), VS_ANY_PASSENGER)
				OR GET_SEAT_PED_IS_IN(GET_PLAYER_PED(thisArmoryAircraft.pOwner)) != VS_DRIVER
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡  AVENGER GUN LOCKER   ╞════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════════╛

PROC MANAGE_AVENGER_GUN_LOCKER_WEAPONS(VAULT_WEAPON_LOADOUT_CUSTOMIZATION& sWLoadoutCustomization)
	IF thisArmoryAircraft.pOwner != INVALID_PLAYER_INDEX()
		IF IS_BIT_SET(GlobalPlayerBD_FM[NATIVE_TO_INT(thisArmoryAircraft.pOwner)].propertyDetails.iBS, PROPERTY_BROADCAST_BS_OFFICE_VAULT_OWN_PISTOL)
		AND NOT IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS, VAULT_PISTOL_CREATED)
			INT pistolModel = HASH("W_PI_PISTOL")
			
			IF NOT DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons[0])
				REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, pistolModel))
				
				IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, pistolModel))
					sWLoadoutCustomization.weapons[0] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, pistolModel), <<420.762, 4808.864, -59.601>>, FALSE, FALSE)
					SET_ENTITY_COORDS_NO_OFFSET(sWLoadoutCustomization.weapons[0], <<420.762, 4808.864, -59.601>>)
					SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons[0], <<-90.0000, -165.0000, 0.0000>>)
					FREEZE_ENTITY_POSITION(sWLoadoutCustomization.weapons[0], TRUE)
					SET_ENTITY_COLLISION(sWLoadoutCustomization.weapons[0], TRUE)
					SET_ENTITY_INVINCIBLE(sWLoadoutCustomization.weapons[0], TRUE)
					SET_ENTITY_VISIBLE(sWLoadoutCustomization.weapons[0], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, pistolModel))
					SET_BIT(sWLoadoutCustomization.iWeaponModelBS, VAULT_PISTOL_CREATED)
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS: successfully created pistol.")
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - W_PI_PISTOL model exist")
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(GlobalPlayerBD_FM[NATIVE_TO_INT(thisArmoryAircraft.pOwner)].propertyDetails.iBS, PROPERTY_BROADCAST_BS_OFFICE_VAULT_OWN_PISTOL)
		AND NOT IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS2, VAULT_PISTOL_CREATED)
			INT pistolModel = HASH("W_PI_PISTOL")
			
			IF NOT DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons2[0])
				REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, pistolModel))
				
				IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, pistolModel))
					sWLoadoutCustomization.weapons2[0] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, pistolModel), <<417.086, 4808.654, -58.643>>, FALSE, FALSE)
					SET_ENTITY_COORDS_NO_OFFSET(sWLoadoutCustomization.weapons2[0], <<417.086, 4808.654, -58.643>>)
					SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons2[0], <<-90.0000, -160.0000, 0.0000>>)
					FREEZE_ENTITY_POSITION(sWLoadoutCustomization.weapons2[0], TRUE)
					SET_ENTITY_COLLISION(sWLoadoutCustomization.weapons2[0], TRUE)
					SET_ENTITY_INVINCIBLE(sWLoadoutCustomization.weapons2[0], TRUE)
					SET_ENTITY_VISIBLE(sWLoadoutCustomization.weapons2[0], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, pistolModel))
					SET_BIT(sWLoadoutCustomization.iWeaponModelBS2, VAULT_PISTOL_CREATED)
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS: successfully created pistol 2")
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - W_PI_PISTOL model exist")
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(GlobalPlayerBD_FM[NATIVE_TO_INT(thisArmoryAircraft.pOwner)].propertyDetails.iBS, PROPERTY_BROADCAST_BS_OFFICE_VAULT_OWN_RIFLE)
			IF NOT IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS, VAULT_RIFLE_CREATED)
				INT rifleModel = HASH("W_AR_SPECIALCARBINE")
				
				IF NOT DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons[1])
					REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, rifleModel))
					
					IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, rifleModel))
						sWLoadoutCustomization.weapons[1] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, rifleModel), <<420.8832, 4808.6084, -59.1203>>, FALSE, FALSE)
						SET_ENTITY_COORDS_NO_OFFSET(sWLoadoutCustomization.weapons[1], <<420.8832, 4808.6084, -59.1203>>)
						SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons[1], <<0.0000, -70.0000, -90.0000>>)
						FREEZE_ENTITY_POSITION(sWLoadoutCustomization.weapons[1], TRUE)
						SET_ENTITY_COLLISION(sWLoadoutCustomization.weapons[1], TRUE)
						SET_ENTITY_INVINCIBLE(sWLoadoutCustomization.weapons[1], TRUE)
						SET_ENTITY_VISIBLE(sWLoadoutCustomization.weapons[1], TRUE)
						SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, rifleModel))
						SET_BIT(sWLoadoutCustomization.iWeaponModelBS, VAULT_RIFLE_CREATED)
						
						CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS: successfully created rifle.")
					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - W_AR_SPECIALCARBINE model is not loaded")
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - W_AR_SPECIALCARBINE model exist")
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS2, VAULT_RIFLE_CREATED)
				INT rifleModel = HASH("W_AR_SPECIALCARBINE")
				
				IF NOT DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons2[1])
					REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, rifleModel))
					
					IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, rifleModel))
						sWLoadoutCustomization.weapons2[1] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, rifleModel), <<417.691, 4808.6084, -59.1203>>, FALSE, FALSE)
						SET_ENTITY_COORDS_NO_OFFSET(sWLoadoutCustomization.weapons2[1], <<417.691, 4808.6084, -59.1203>>)
						SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons2[1], <<0.0000, -70.0000, -90.0000>>)
						FREEZE_ENTITY_POSITION(sWLoadoutCustomization.weapons2[1], TRUE)
						SET_ENTITY_COLLISION(sWLoadoutCustomization.weapons2[1], TRUE)
						SET_ENTITY_INVINCIBLE(sWLoadoutCustomization.weapons2[1], TRUE)
						SET_ENTITY_VISIBLE(sWLoadoutCustomization.weapons2[1], TRUE)
						SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, rifleModel))
						SET_BIT(sWLoadoutCustomization.iWeaponModelBS2, VAULT_RIFLE_CREATED)
						
						CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS: successfully created rifle 2")
					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - W_AR_SPECIALCARBINE model is not loaded")
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - W_AR_SPECIALCARBINE model exist")
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS, VAULT_ASSRIFLE_CREATED)
				INT rifleModel = HASH("W_AR_ASSAULTRIFLE")
				
				IF NOT DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons[8])
					REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, rifleModel))
					
					IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, rifleModel))
						sWLoadoutCustomization.weapons[8] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, rifleModel), <<420.7702, 4808.557, -59.221>>, FALSE, FALSE)
						SET_ENTITY_COORDS_NO_OFFSET(sWLoadoutCustomization.weapons[8], <<420.7702, 4808.557, -59.221>>)
						SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons[8], <<0.0000, -70.0000, -90.0000>>)
						FREEZE_ENTITY_POSITION(sWLoadoutCustomization.weapons[8], TRUE)
						SET_ENTITY_COLLISION(sWLoadoutCustomization.weapons[8], TRUE)
						SET_ENTITY_INVINCIBLE(sWLoadoutCustomization.weapons[8], TRUE)
						SET_ENTITY_VISIBLE(sWLoadoutCustomization.weapons[8], TRUE)
						SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, rifleModel))
						SET_BIT(sWLoadoutCustomization.iWeaponModelBS, VAULT_ASSRIFLE_CREATED)
						
						CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS: successfully created assault rifle.")
					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - W_AR_ASSAULTRIFLE model is not loaded")
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - W_AR_ASSAULTRIFLE model exist")
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS2, VAULT_ASSRIFLE_CREATED)
				INT rifleModel = HASH("W_AR_ASSAULTRIFLE")
				
				IF NOT DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons2[8])
					REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, rifleModel))
					
					IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, rifleModel))
						sWLoadoutCustomization.weapons2[8] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, rifleModel), <<417.578, 4808.557, -59.221>>, FALSE, FALSE)
						SET_ENTITY_COORDS_NO_OFFSET(sWLoadoutCustomization.weapons2[8], <<417.578, 4808.557, -59.221>>)
						SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons2[8], <<0.0000, -70.0000, -90.0000>>)
						FREEZE_ENTITY_POSITION(sWLoadoutCustomization.weapons2[8], TRUE)
						SET_ENTITY_COLLISION(sWLoadoutCustomization.weapons2[8], TRUE)
						SET_ENTITY_INVINCIBLE(sWLoadoutCustomization.weapons2[8], TRUE)
						SET_ENTITY_VISIBLE(sWLoadoutCustomization.weapons2[8], TRUE)
						SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, rifleModel))
						SET_BIT(sWLoadoutCustomization.iWeaponModelBS2, VAULT_ASSRIFLE_CREATED)
						
						CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS: successfully created assault rifle 2")
					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - W_AR_ASSAULTRIFLE model is not loaded")
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - W_AR_ASSAULTRIFLE model exist")
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(GlobalPlayerBD_FM[NATIVE_TO_INT(thisArmoryAircraft.pOwner)].propertyDetails.iBSTwo, PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_SHOTGUN)
		AND NOT IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS, VAULT_SHOTGUN_CREATED)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - start creating shotgun")
			
			INT shotgunModel = HASH("w_sg_pumpshotgun")
			
			IF NOT DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons[2])
				REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, shotgunModel))
				
				IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, shotgunModel))
					sWLoadoutCustomization.weapons[2] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, shotgunModel), <<420.6552, 4808.602, -59.233>>, FALSE, FALSE)
					SET_ENTITY_COORDS_NO_OFFSET(sWLoadoutCustomization.weapons[2], <<420.6552, 4808.602, -59.233>>)
					SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons[2], <<0.0000, -70.0000, -90.0000>>)
					FREEZE_ENTITY_POSITION(sWLoadoutCustomization.weapons[2], TRUE)
					SET_ENTITY_COLLISION(sWLoadoutCustomization.weapons[2], TRUE)
					SET_ENTITY_INVINCIBLE(sWLoadoutCustomization.weapons[2], TRUE)
					SET_ENTITY_VISIBLE(sWLoadoutCustomization.weapons[2], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, shotgunModel))
					SET_BIT(sWLoadoutCustomization.iWeaponModelBS, VAULT_SHOTGUN_CREATED)
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS: successfully created shotgun.")
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - w_sg_pumpshotgun model is not loaded")
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - w_sg_pumpshotgun exist")
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(GlobalPlayerBD_FM[NATIVE_TO_INT(thisArmoryAircraft.pOwner)].propertyDetails.iBSTwo, PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_SHOTGUN)
		AND NOT IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS2, VAULT_SHOTGUN_CREATED)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - start creating shotgun 2")
			
			INT shotgunModel = HASH("w_sg_pumpshotgun")
			
			IF NOT DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons2[2])
				REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, shotgunModel))
				
				IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, shotgunModel))
					sWLoadoutCustomization.weapons2[2] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, shotgunModel), <<417.463, 4808.602, -59.233>>, FALSE, FALSE)
					SET_ENTITY_COORDS_NO_OFFSET(sWLoadoutCustomization.weapons2[2], <<417.463, 4808.602, -59.233>>)
					SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons2[2], <<0.0000, -70.0000, -90.0000>>)
					FREEZE_ENTITY_POSITION(sWLoadoutCustomization.weapons2[2], TRUE)
					SET_ENTITY_COLLISION(sWLoadoutCustomization.weapons2[2], TRUE)
					SET_ENTITY_INVINCIBLE(sWLoadoutCustomization.weapons2[2], TRUE)
					SET_ENTITY_VISIBLE(sWLoadoutCustomization.weapons2[2], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, shotgunModel))
					SET_BIT(sWLoadoutCustomization.iWeaponModelBS2, VAULT_SHOTGUN_CREATED)
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS: successfully created shotgun 2")
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - w_sg_pumpshotgun model is not loaded")
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - w_sg_pumpshotgun exist")
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(GlobalPlayerBD_FM[NATIVE_TO_INT(thisArmoryAircraft.pOwner)].propertyDetails.iBSTwo, PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_MICROSMG)
		AND NOT IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS, VAULT_SMG_CREATED)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - start creating smg")
			
			INT smgModel = HASH("w_sb_microsmg")
			
			IF NOT DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons[3])
				REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, smgModel))
				
				IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, smgModel))
					sWLoadoutCustomization.weapons[3] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, smgModel), <<420.198, 4808.438, -59.044>>, FALSE, FALSE)
					SET_ENTITY_COORDS_NO_OFFSET(sWLoadoutCustomization.weapons[3], <<420.198, 4808.438, -59.044>>)
					SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons[3], <<-160.0000, 90.0000, 0.0000>>)
					FREEZE_ENTITY_POSITION(sWLoadoutCustomization.weapons[3], TRUE)
					SET_ENTITY_COLLISION(sWLoadoutCustomization.weapons[3], TRUE)
					SET_ENTITY_INVINCIBLE(sWLoadoutCustomization.weapons[3], TRUE)
					SET_ENTITY_VISIBLE(sWLoadoutCustomization.weapons[3], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, smgModel))
					SET_BIT(sWLoadoutCustomization.iWeaponModelBS, VAULT_SMG_CREATED)
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS: successfully created smg.")
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - w_sb_microsmg model is not loaded")
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(GlobalPlayerBD_FM[NATIVE_TO_INT(thisArmoryAircraft.pOwner)].propertyDetails.iBSTwo, PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_MICROSMG)
		AND NOT IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS2, VAULT_SMG_CREATED)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - start creating smg 2")
			
			INT smgModel = HASH("w_sb_microsmg")
			
			IF NOT DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons2[3])
				REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, smgModel))
				
				IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, smgModel))
					sWLoadoutCustomization.weapons2[3] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, smgModel), <<417.463, 4808.613, -58.636>>, FALSE, FALSE)
					SET_ENTITY_COORDS_NO_OFFSET(sWLoadoutCustomization.weapons2[3], <<417.463, 4808.613, -58.636>>)
					SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons2[3], <<90.0000, 0.0000, 12.8000>>)
					FREEZE_ENTITY_POSITION(sWLoadoutCustomization.weapons2[3], TRUE)
					SET_ENTITY_COLLISION(sWLoadoutCustomization.weapons2[3], TRUE)
					SET_ENTITY_INVINCIBLE(sWLoadoutCustomization.weapons2[3], TRUE)
					SET_ENTITY_VISIBLE(sWLoadoutCustomization.weapons2[3], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, smgModel))
					SET_BIT(sWLoadoutCustomization.iWeaponModelBS2, VAULT_SMG_CREATED)
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS: successfully created smg 2")
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - w_sb_microsmg model is not loaded")
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(GlobalPlayerBD_FM[NATIVE_TO_INT(thisArmoryAircraft.pOwner)].propertyDetails.iBSTwo, PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_BOMB)
		AND NOT IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS, VAULT_BOMB_CREATED)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - start creating bomb")
			
			INT shotgunModel = HASH("w_ex_pe")
			
			IF NOT DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons[4])
				REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, shotgunModel))
				
				IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, shotgunModel))
					sWLoadoutCustomization.weapons[4] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, shotgunModel), <<420.4682, 4808.856, -59.584>>, FALSE, FALSE)
					SET_ENTITY_COORDS_NO_OFFSET(sWLoadoutCustomization.weapons[4], <<420.4682, 4808.856, -59.584>>)
					SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons[4], <<0.0000, -0.0000, -72.0000>>)
					FREEZE_ENTITY_POSITION(sWLoadoutCustomization.weapons[4], TRUE)
					SET_ENTITY_COLLISION(sWLoadoutCustomization.weapons[4], TRUE)
					SET_ENTITY_INVINCIBLE(sWLoadoutCustomization.weapons[4], TRUE)
					SET_ENTITY_VISIBLE(sWLoadoutCustomization.weapons[4], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, shotgunModel))
					SET_BIT(sWLoadoutCustomization.iWeaponModelBS, VAULT_BOMB_CREATED)
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS: successfully created smg.")
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - w_ex_pe model is not loaded")
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(GlobalPlayerBD_FM[NATIVE_TO_INT(thisArmoryAircraft.pOwner)].propertyDetails.iBSTwo, PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_BOMB)
		AND NOT IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS2, VAULT_BOMB_CREATED)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - start creating bomb 2")
			
			INT shotgunModel = HASH("w_ex_pe")
			
			IF NOT DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons2[4])
				REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, shotgunModel))
				
				IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, shotgunModel))
					sWLoadoutCustomization.weapons2[4] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, shotgunModel), <<417.455, 4808.860, -59.584>>, FALSE, FALSE)
					SET_ENTITY_COORDS_NO_OFFSET(sWLoadoutCustomization.weapons2[4], <<417.455, 4808.860, -59.584>>)
					SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons2[4], <<0.0000, -0.0000, -90.0000>>)
					FREEZE_ENTITY_POSITION(sWLoadoutCustomization.weapons2[4], TRUE)
					SET_ENTITY_COLLISION(sWLoadoutCustomization.weapons2[4], TRUE)
					SET_ENTITY_INVINCIBLE(sWLoadoutCustomization.weapons2[4], TRUE)
					SET_ENTITY_VISIBLE(sWLoadoutCustomization.weapons2[4], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, shotgunModel))
					SET_BIT(sWLoadoutCustomization.iWeaponModelBS2, VAULT_BOMB_CREATED)
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS: successfully created bomb 2")
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - w_ex_pe model is not loaded")
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(GlobalPlayerBD_FM[NATIVE_TO_INT(thisArmoryAircraft.pOwner)].propertyDetails.iBSTwo, PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_GRENADE)
		AND NOT IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS, VAULT_GRENADE_CREATED)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - start creating grenade")
			
			INT grenadeModel = HASH("w_ex_grenadefrag")
			
			IF NOT DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons[5])
				REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, grenadeModel))
				
				IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, grenadeModel))
					sWLoadoutCustomization.weapons[5] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, grenadeModel), <<420.561, 4808.679, -58.591>>, FALSE, FALSE)
					SET_ENTITY_COORDS_NO_OFFSET(sWLoadoutCustomization.weapons[5], <<420.561, 4808.679, -58.591>>)
					SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons[5], <<0.0000, 0.0000, 30.350>>)
					FREEZE_ENTITY_POSITION(sWLoadoutCustomization.weapons[5], TRUE)
					SET_ENTITY_COLLISION(sWLoadoutCustomization.weapons[5], TRUE)
					SET_ENTITY_INVINCIBLE(sWLoadoutCustomization.weapons[5], TRUE)
					SET_ENTITY_VISIBLE(sWLoadoutCustomization.weapons[5], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, grenadeModel))
					SET_BIT(sWLoadoutCustomization.iWeaponModelBS, VAULT_GRENADE_CREATED)
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS: successfully created grenade.")
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - w_ex_grenadefrag model is not loaded")
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(GlobalPlayerBD_FM[NATIVE_TO_INT(thisArmoryAircraft.pOwner)].propertyDetails.iBSTwo, PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_GRENADE)
		AND NOT IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS2, VAULT_GRENADE_CREATED)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - start creating grenade")
			
			INT grenadeModel = HASH("w_ex_grenadefrag")
			
			IF NOT DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons2[5])
				REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, grenadeModel))
				
				IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, grenadeModel))
					sWLoadoutCustomization.weapons2[5] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, grenadeModel), <<416.919, 4808.442, -58.591>>, FALSE, FALSE)
					SET_ENTITY_COORDS_NO_OFFSET(sWLoadoutCustomization.weapons2[5], <<416.919, 4808.442, -58.591>>)
					SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons2[5], <<0.0000, 0.0000, 0.0000>>)
					FREEZE_ENTITY_POSITION(sWLoadoutCustomization.weapons2[5], TRUE)
					SET_ENTITY_COLLISION(sWLoadoutCustomization.weapons2[5], TRUE)
					SET_ENTITY_INVINCIBLE(sWLoadoutCustomization.weapons2[5], TRUE)
					SET_ENTITY_VISIBLE(sWLoadoutCustomization.weapons2[5], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, grenadeModel))
					SET_BIT(sWLoadoutCustomization.iWeaponModelBS2, VAULT_GRENADE_CREATED)
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS: successfully created grenade 2")
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - w_ex_grenadefrag model is not loaded")
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(GlobalPlayerBD_FM[NATIVE_TO_INT(thisArmoryAircraft.pOwner)].propertyDetails.iBSTwo, PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_MG)
		AND NOT IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS, VAULT_MG_CREATED)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - start creating mg")
			
			INT gunModel = HASH("w_mg_combatmg")
			
			IF NOT DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons[6])
				REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, gunModel))
				
				IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, gunModel))
					sWLoadoutCustomization.weapons[6] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, gunModel), <<420.5412, 4808.592, -59.236>>, FALSE, FALSE)
					SET_ENTITY_COORDS_NO_OFFSET(sWLoadoutCustomization.weapons[6], <<420.5412, 4808.592, -59.236>>)
					SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons[6], <<0.0000, -70.0000, -90.0000>>)
					FREEZE_ENTITY_POSITION(sWLoadoutCustomization.weapons[6], TRUE)
					SET_ENTITY_COLLISION(sWLoadoutCustomization.weapons[6], TRUE)
					SET_ENTITY_INVINCIBLE(sWLoadoutCustomization.weapons[6], TRUE)
					SET_ENTITY_VISIBLE(sWLoadoutCustomization.weapons[6], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, gunModel))
					SET_BIT(sWLoadoutCustomization.iWeaponModelBS, VAULT_MG_CREATED)
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS: successfully created mg.")
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - w_mg_combatmg model is not loaded")
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - w_mg_combatmg exist")
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(GlobalPlayerBD_FM[NATIVE_TO_INT(thisArmoryAircraft.pOwner)].propertyDetails.iBSTwo, PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_MG)
		AND NOT IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS2, VAULT_MG_CREATED)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - start creating mg 2")
			
			INT gunModel = HASH("w_mg_combatmg")
			
			IF NOT DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons2[6])
				REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, gunModel))
				
				IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, gunModel))
					sWLoadoutCustomization.weapons2[6] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, gunModel), <<417.349, 4808.592, -59.236>>, FALSE, FALSE)
					SET_ENTITY_COORDS_NO_OFFSET(sWLoadoutCustomization.weapons2[6], <<417.349, 4808.592, -59.236>>)
					SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons2[6], <<0.0000, -70.0000, -90.0000>>)
					FREEZE_ENTITY_POSITION(sWLoadoutCustomization.weapons2[6], TRUE)
					SET_ENTITY_COLLISION(sWLoadoutCustomization.weapons2[6], TRUE)
					SET_ENTITY_INVINCIBLE(sWLoadoutCustomization.weapons2[6], TRUE)
					SET_ENTITY_VISIBLE(sWLoadoutCustomization.weapons2[6], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, gunModel))
					SET_BIT(sWLoadoutCustomization.iWeaponModelBS2, VAULT_MG_CREATED)
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS: successfully created mg 2")
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - w_mg_combatmg model is not loaded")
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - w_mg_combatmg exist")
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(GlobalPlayerBD_FM[NATIVE_TO_INT(thisArmoryAircraft.pOwner)].propertyDetails.iBSTwo, PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_SNIPER)
		AND NOT IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS, VAULT_SNIPER_CREATED)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - start creating sniper")
			
			INT gunModel = HASH("w_sr_marksmanrifle")
			
			IF NOT DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons[7])
				REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, gunModel))
				REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("w_at_scope_large")))
				REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("w_sr_marksmanrifle_mag1")))
				
				IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, gunModel))
				AND HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("w_at_scope_large")))
				AND HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("w_sr_marksmanrifle_mag1")))
					sWLoadoutCustomization.weapons[7] = CREATE_WEAPON_OBJECT(WEAPONTYPE_DLC_MARKSMANRIFLE, 0, <<420.4292, 4808.593, -59.211>>, TRUE)
					SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons[7], <<0.0000, -70.0000, -90.0000>>)
					SET_ENTITY_COORDS_NO_OFFSET(sWLoadoutCustomization.weapons[7], <<420.4292, 4808.593, -59.211>>)
					FREEZE_ENTITY_POSITION(sWLoadoutCustomization.weapons[7], TRUE)
					SET_ENTITY_COLLISION(sWLoadoutCustomization.weapons[7], TRUE)
					SET_ENTITY_INVINCIBLE(sWLoadoutCustomization.weapons[7], TRUE)
					SET_ENTITY_VISIBLE(sWLoadoutCustomization.weapons[7], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, gunModel))
					SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES ,HASH("w_at_scope_large")))
					SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("w_sr_marksmanrifle_mag1")))
					SET_BIT(sWLoadoutCustomization.iWeaponModelBS, VAULT_SNIPER_CREATED)
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS: successfully created sniper.")
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - w_sr_marksmanrifle model is not loaded")
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - w_sr_marksmanrifle exist")
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(GlobalPlayerBD_FM[NATIVE_TO_INT(thisArmoryAircraft.pOwner)].propertyDetails.iBSTwo, PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_SNIPER)
		AND NOT IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS2, VAULT_SNIPER_CREATED)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - start creating sniper 2")
			
			INT gunModel = HASH("w_sr_marksmanrifle")
			
			IF NOT DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons2[7])
				REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, gunModel))
				REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("w_at_scope_large")))
				REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("w_sr_marksmanrifle_mag1")))
				
				IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, gunModel))
				AND HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("w_at_scope_large")))
				AND HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("w_sr_marksmanrifle_mag1")))
					sWLoadoutCustomization.weapons2[7] = CREATE_WEAPON_OBJECT(WEAPONTYPE_DLC_MARKSMANRIFLE, 0, <<417.237, 4808.593, -59.211>>, TRUE)
					SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons2[7], <<0.0000, -70.0000, -90.0000>>)
					SET_ENTITY_COORDS_NO_OFFSET(sWLoadoutCustomization.weapons2[7], <<417.237, 4808.593, -59.211>>)
					FREEZE_ENTITY_POSITION(sWLoadoutCustomization.weapons2[7], TRUE)
					SET_ENTITY_COLLISION(sWLoadoutCustomization.weapons2[7], TRUE)
					SET_ENTITY_INVINCIBLE(sWLoadoutCustomization.weapons2[7], TRUE)
					SET_ENTITY_VISIBLE(sWLoadoutCustomization.weapons2[7], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, gunModel))
					SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES ,HASH("w_at_scope_large")))
					SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("w_sr_marksmanrifle_mag1")))
					SET_BIT(sWLoadoutCustomization.iWeaponModelBS2, VAULT_SNIPER_CREATED)
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS: successfully created sniper 2")
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - w_sr_marksmanrifle model is not loaded")
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_AVENGER_GUN_LOCKER_WEAPONS - w_sr_marksmanrifle exist")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL CAN_PLAYER_START_AVENGER_GUN_LOCKER()
	IF thisArmoryAircraft.pOwner != INVALID_PLAYER_INDEX()
		IF thisArmoryAircraft.pOwner = PLAYER_ID()
			IF IS_PLAYER_ARMORY_AIRCRAFT_WEAPON_WORKSHOP_PURCHASED(PLAYER_ID())
			AND NOT IS_PLAYER_USING_OFFICE_SEATID_VALID()
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		ELSE
			IF IS_PLAYER_ARMORY_AIRCRAFT_WEAPON_WORKSHOP_PURCHASED(thisArmoryAircraft.pOwner)
				IF (DOES_PLAYER_OWN_ANY_GUN_LOCKER())
				AND (NOT NETWORK_IS_IN_MP_CUTSCENE())
				AND NOT IS_PLAYER_USING_OFFICE_SEATID_VALID()
					RETURN TRUE
				ELSE
					RETURN FALSE
				ENDIF
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_SAFE_TO_START_AVENGER_GUN_LOCKER_MENUS(BOOL bAllowHeist = FALSE)
	IF NOT CAN_PLAYER_START_AVENGER_GUN_LOCKER()
		RETURN FALSE
	ENDIF
	
	IF NOT bAllowHeist
		IF Is_Player_Currently_On_MP_Heist(PLAYER_ID())
		OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_PHONE_ONSCREEN()
	OR IS_INTERACTION_MENU_OPEN()
	OR IS_BROWSER_OPEN()
	OR IS_SELECTOR_ONSCREEN()
	OR IS_CUSTOM_MENU_ON_SCREEN()
	OR IS_PAUSE_MENU_ACTIVE()
	OR MPGlobalsAmbience.bTriggerPropertyExitOnFoot
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_AVENGER_GUN_LOCKER(VAULT_WEAPON_LOADOUT_CUSTOMIZATION& sWLoadoutCustomization)
	IF thisArmoryAircraft.pOwner != INVALID_PLAYER_INDEX()
		IF IS_PLAYER_ARMORY_AIRCRAFT_WEAPON_WORKSHOP_PURCHASED(thisArmoryAircraft.pOwner)
			IF NOT IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
				SWITCH sWLoadoutCustomization.eCustomizationStage
					CASE VMC_STAGE_INIT
						CLEAN_UP_AVENGER_GUN_LOCKER(sWLoadoutCustomization)
						
						sWLoadoutCustomization.eCustomizationStage = VWC_STAGE_WAITING_TO_TRIGGER
					BREAK
					
					CASE VWC_STAGE_WAITING_TO_TRIGGER
						GET_NUM_AVAILABLE_WEAPON_GROUP(sWLoadoutCustomization)
						//MANAGE_AVENGER_GUN_LOCKER_WEAPONS(sWLoadoutCustomization)
						IF IS_SAFE_TO_START_AVENGER_GUN_LOCKER_MENUS(TRUE)
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<508.831665,4751.764648,-69.996010>>, <<507.676941,4751.750977,-67.996010>>, 1.00)
							AND NOT IS_PED_RUNNING(PLAYER_PED_ID())
							AND DOES_ENTITY_EXIST(PLAYER_PED_ID())
							AND IS_PED_STILL(PLAYER_PED_ID())
								
								IF sWLoadoutCustomization.iVaultWeaponContext = NEW_CONTEXT_INTENTION
									IF NOT IS_HELP_MESSAGE_ON_SCREEN()
										REGISTER_CONTEXT_INTENTION(sWLoadoutCustomization.iVaultWeaponContext, CP_HIGH_PRIORITY, "OF_VAULT_MENU")
									ENDIF
								ENDIF
								
								IF NOT IS_INTERACTION_MENU_OPEN()
									IF HAS_CONTEXT_BUTTON_TRIGGERED(sWLoadoutCustomization.iVaultWeaponContext)
										LOAD_MENU_ASSETS()
										BUILD_VAULT_WEAPON_MAIN_MENU(sWLoadoutCustomization)
										TASK_ACHIEVE_HEADING(PLAYER_PED_ID(), 84.8149, 0)
										
										NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
										SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_UseKinematicModeWhenStationary, TRUE)
										SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontActivateRagdollFromExplosions, TRUE)
										
										RELEASE_CONTEXT_INTENTION(sWLoadoutCustomization.iVaultWeaponContext)
										sWLoadoutCustomization.eCustomizationStage = VMC_STAGE_CUSTOMIZING
									ENDIF
								ENDIF
							ELSE
								RELEASE_CONTEXT_INTENTION(sWLoadoutCustomization.iVaultWeaponContext)
							ENDIF
						ELSE
							IF IS_CONTEXT_INTENTION_HELP_DISPLAYING(sWLoadoutCustomization.iVaultWeaponContext)
								RELEASE_CONTEXT_INTENTION(sWLoadoutCustomization.iVaultWeaponContext)
							ENDIF
						ENDIF
					BREAK
					
					CASE VMC_STAGE_CUSTOMIZING
						PROCESS_VAULT_WEAPON_LOADOUT_MENU(sWLoadoutCustomization)
					BREAK
					
					CASE VMC_STAGE_CLEANUP
						CLEAN_UP_AVENGER_GUN_LOCKER(sWLoadoutCustomization)
						
						START_NET_TIMER(sWLoadoutCustomization.sMenuTimer)
					BREAK
				ENDSWITCH
			ELSE
				IF sWLoadoutCustomization.eCustomizationStage > VMC_STAGE_INIT
					CLEAN_UP_AVENGER_GUN_LOCKER(sWLoadoutCustomization)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_PLAYER_MOVE_OUT_OF_WAY()
	IF g_ownerOfArmoryAircraftPropertyIAmIn = INVALID_PLAYER_INDEX()
		RETURN FALSE
	ENDIF

	IF IS_DRIVER_ENTERING_SIMPLE_INTERIOR(g_ownerOfArmoryAircraftPropertyIAmIn) 
	AND GET_SIMPLE_INTERIOR_DRIVER_IS_ENTERING(g_ownerOfArmoryAircraftPropertyIAmIn) = SIMPLE_INTERIOR_ARMORY_AIRCRAFT_1
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_IN_VEH_SEAT(g_ownerOfArmoryAircraftPropertyIAmIn, VS_DRIVER) 
	AND IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR(g_ownerOfArmoryAircraftPropertyIAmIn) 
	AND GET_SIMPLE_INTERIOR_PLAYER_IS_WALKING_IN_OR_OUT_FROM(g_ownerOfArmoryAircraftPropertyIAmIn) = SIMPLE_INTERIOR_ARMORY_AIRCRAFT_1
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC 

PROC MAINTAIN_MOVE_OUT_OF_WAY()
	BOOL bInBadArea
	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND g_ownerOfArmoryAircraftPropertyIAmIn != INVALID_PLAYER_INDEX()
	AND NOT IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
	AND PLAYER_ID() != g_ownerOfArmoryAircraftPropertyIAmIn
	AND NOT IS_SCREEN_FADED_OUT()
		IF SHOULD_PLAYER_MOVE_OUT_OF_WAY()
			VEHICLE_INDEX nearbyVehs[1]
			GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), nearbyVehs)
			IF DOES_ENTITY_EXIST(nearbyVehs[0])
			AND IS_ENTITY_A_VEHICLE(nearbyVehs[0])
				IF NOT IS_ENTITY_DEAD(nearbyVehs[0])
					IF IS_ENTITY_VISIBLE(nearbyVehs[0])
					AND NOT NETWORK_IS_ENTITY_CONCEALED(nearbyVehs[0])
						PRINTLN("MAINTAIN_MOVE_OUT_OF_WAY: there is a vehicle nearby... ")
						EXIT
					ENDIF
				ENDIF	
			ENDIF	
			
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<519.499695,4750.524414,-70.246033>>, <<532.032776,4750.608398,-66.620346>>, 4.750000)
				bInBadArea = TRUE
			ENDIF
			
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) != PERFORMING_TASK
			AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) != WAITING_TO_START_TASK
			AND bInBadArea
			AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
				VECTOR vTargetCoord
				IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<522.369934,4752.401367,-69.995972>>, <<529.122009,4752.437012,-67.789948>>, 1.750000)
					vTargetCoord.x = 518.7823
					vTargetCoord.y = 4751.9365
				ELIF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<522.256775,4748.777344,-69.995972>>, <<528.724060,4748.648438,-67.984009>>, 1.750000)
					vTargetCoord.x = 519.8637
					vTargetCoord.y = 4749.0088
				ELSE
					vTargetCoord.x = GET_RANDOM_FLOAT_IN_RANGE(518.6216, 518.6407)
					vTargetCoord.y = GET_RANDOM_FLOAT_IN_RANGE(4752.1323, 4748.9077)
				ENDIF
				
				vTargetCoord.z = -69.9960
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), vTargetCoord, PEDMOVEBLENDRATIO_WALK, DEFAULT, DEFAULT, 0.1)
				PRINTLN("MAINTAIN_MOVE_OUT_OF_WAY: giving ped TASK_GO_STRAIGHT_TO_COORD ")
			ENDIF
		ENDIF	
	ENDIF

ENDPROC

PROC UPDATE_AVENGER_EXIT_MENU_OPTION()
	IF g_OwnerOfArmoryAircraftPropertyIAmIn != PLAYER_ID()
	AND g_OwnerOfArmoryAircraftPropertyIAmIn != INVALID_PLAYER_INDEX()
		IF NOT IS_BIT_SET(thisArmoryAircraft.iBS ,BS_ARMORY_AIRCRAFT_COCKPIT_PASSENGER_ACCESS_NO_ONE)
			IF IS_AVNEGER_COCKPIT_ACCESS_PASSENGERS_NO_ONE(g_OwnerOfArmoryAircraftPropertyIAmIn)
				SET_BIT(thisArmoryAircraft.iBS ,BS_ARMORY_AIRCRAFT_COCKPIT_PASSENGER_ACCESS_NO_ONE)
				CLEAR_BIT(thisArmoryAircraft.iBS ,BS_ARMORY_AIRCRAFT_COCKPIT_PASSENGER_ACCESS_EVERYONE)
				g_SimpleInteriorData.bRefreshInteriorMenu = TRUE
			ENDIF
		ENDIF
		IF NOT IS_BIT_SET(thisArmoryAircraft.iBS ,BS_ARMORY_AIRCRAFT_COCKPIT_PASSENGER_ACCESS_EVERYONE)
			IF !IS_AVNEGER_COCKPIT_ACCESS_PASSENGERS_NO_ONE(g_OwnerOfArmoryAircraftPropertyIAmIn)
				SET_BIT(thisArmoryAircraft.iBS ,BS_ARMORY_AIRCRAFT_COCKPIT_PASSENGER_ACCESS_EVERYONE)
				CLEAR_BIT(thisArmoryAircraft.iBS ,BS_ARMORY_AIRCRAFT_COCKPIT_PASSENGER_ACCESS_NO_ONE)
				g_SimpleInteriorData.bRefreshInteriorMenu = TRUE
			ENDIF
		ENDIF	
	ENDIF	
	
	IF NOT IS_BIT_SET(thisArmoryAircraft.iBS ,BS_ARMORY_AIRCRAFT_CAN_LEAVE_TO_COCKPIT_PASSENGER)
		IF CAN_PLAYER_MOVE_TO_ARMORY_AIRCRAFT_PILOT_SEAT()
			CLEAR_BIT(thisArmoryAircraft.iBS ,BS_ARMORY_AIRCRAFT_CAN_NOT_LEAVE_TO_COCKPIT_PASSENGER)
			SET_BIT(thisArmoryAircraft.iBS ,BS_ARMORY_AIRCRAFT_CAN_LEAVE_TO_COCKPIT_PASSENGER)
			g_SimpleInteriorData.bRefreshInteriorMenu = TRUE
		ENDIF	
	ENDIF
	IF NOT IS_BIT_SET(thisArmoryAircraft.iBS ,BS_ARMORY_AIRCRAFT_CAN_NOT_LEAVE_TO_COCKPIT_PASSENGER)
		IF !CAN_PLAYER_MOVE_TO_ARMORY_AIRCRAFT_PILOT_SEAT()	
			SET_BIT(thisArmoryAircraft.iBS ,BS_ARMORY_AIRCRAFT_CAN_NOT_LEAVE_TO_COCKPIT_PASSENGER)
			CLEAR_BIT(thisArmoryAircraft.iBS ,BS_ARMORY_AIRCRAFT_CAN_LEAVE_TO_COCKPIT_PASSENGER)
			g_SimpleInteriorData.bRefreshInteriorMenu = TRUE
		ENDIF	
	ENDIF
ENDPROC

PROC UN_FREEZE_PLAYER_VEHICLE_INSIDE_ARMORY_AIRCRAFT()
	IF NOT IS_BIT_SET(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_UNFREEZE_PLAYER_VEHICLE)
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			IF g_OwnerOfArmoryAircraftPropertyIAmIn != INVALID_PLAYER_INDEX()
				IF g_OwnerOfArmoryAircraftPropertyIAmIn = PLAYER_ID()
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF NOT IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
							IF IS_PERSONAL_VEHICLE_IN_AVENGER(PLAYER_ID())
								IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehPersonalVehicle)
								AND NOT IS_ENTITY_DEAD(MPGlobalsAmbience.vehPersonalVehicle)
									IF NETWORK_HAS_CONTROL_OF_ENTITY(MPGlobalsAmbience.vehPersonalVehicle)
										FREEZE_ENTITY_POSITION(MPGlobalsAmbience.vehPersonalVehicle, FALSE)
										SET_BIT(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_UNFREEZE_PLAYER_VEHICLE)
									ELSE
										NETWORK_REQUEST_CONTROL_OF_ENTITY(MPGlobalsAmbience.vehPersonalVehicle)
									ENDIF
								ENDIF
							ELIF IS_PEGASUS_VEHICLE_IN_AVENGER(PLAYER_ID())
								IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehPegasusVehicle)
								AND NOT IS_ENTITY_DEAD(MPGlobalsAmbience.vehPegasusVehicle)
									IF NETWORK_HAS_CONTROL_OF_ENTITY(MPGlobalsAmbience.vehPegasusVehicle)
										FREEZE_ENTITY_POSITION(MPGlobalsAmbience.vehPegasusVehicle, FALSE)
										SET_BIT(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_UNFREEZE_PLAYER_VEHICLE)
									ELSE
										NETWORK_REQUEST_CONTROL_OF_ENTITY(MPGlobalsAmbience.vehPegasusVehicle)
									ENDIF
								ENDIF
							ENDIF
						ENDIF	
					ENDIF	
				ENDIF
			ENDIF
		ENDIF	
	ENDIF	
ENDPROC


PROC MAINTAIN_VEHICLE_ZOOM()
	IF NOT IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())
	AND (IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
	OR IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID()))
		SET_RADAR_ZOOM_PRECISE(m_vehicle_zoomed_in_value)
	ELSE
		SET_RADAR_ZOOM_PRECISE(m_vehicle_zoomed_out_value)	
	ENDIF
ENDPROC

PROC MAINTAIN_FIREPROOF_IN_AVENGER()
	IF GET_NUMBER_OF_FIRES_IN_RANGE(<<524.4728, 4750.6528, -69.9960>>, 15) > 0
		STOP_FIRE_IN_RANGE(<<524.4728, 4750.6528, -69.9960>>, 15)
	ENDIF
ENDPROC

PROC MAINTAIN_CHAIR_TINTS()
	IF NOT IS_OWNER_RENOVATED_AVENGER(thisArmoryAircraft.pOwner)
		LOAD_INTERIOR_TINTS(TRUE)	
	ENDIF	
ENDPROC

PROC MAINTAIN_AVENGER_WEAPON_WORK_BENCH_TIMECYCLE_MODS()
	
	IF IS_PLAYER_BROWSING_ITEMS_IN_SHOP(GUN_SHOP_ARMORY_AVENGER)
	AND g_bPlayerBrowsingWeaponWorkshop
		IF NOT IS_BIT_SET(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_SET_WEAPON_WORK_BENCH_TIMECYCLE_MOD)
			CLEAR_TIMECYCLE_MODIFIER()
			SET_TIMECYCLE_MODIFIER("mp_x17dlc_int_02_weapon_avenger_camera")
			SET_BIT(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_SET_WEAPON_WORK_BENCH_TIMECYCLE_MOD)
		ENDIF
	ELSE
		IF IS_BIT_SET(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_SET_WEAPON_WORK_BENCH_TIMECYCLE_MOD)
			CLEAR_TIMECYCLE_MODIFIER()
			LOAD_INTERIOR_TINTS()
			CLEAR_BIT(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_SET_WEAPON_WORK_BENCH_TIMECYCLE_MOD)
		ENDIF
	ENDIF
	
ENDPROC

PROC MANAGE_TURRET_AUDIO_SCENE()
	IF g_bPlayerViewingTurretHud
		IF IS_AUDIO_SCENE_ACTIVE("dlc_xm_avenger_interior_scene")
			STOP_AUDIO_SCENE("dlc_xm_avenger_interior_scene")
			PRINTLN("MANAGE_TURRET_AUDIO_SCENE stop dlc_xm_avenger_interior_scene")
		ENDIF
	ELSE
		IF NOT IS_AUDIO_SCENE_ACTIVE("dlc_xm_avenger_interior_scene")
			START_AUDIO_SCENE("dlc_xm_avenger_interior_scene")
			PRINTLN("MANAGE_TURRET_AUDIO_SCENE start dlc_xm_avenger_interior_scene")
		ENDIF
	ENDIF	
ENDPROC

PROC MAINTAIN_GAMEPLAY_CAM_WHEN_PLAYER_INSIDE_VEHICLE()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
			AND NOT IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
				SET_THIRD_PERSON_CAM_RELATIVE_PITCH_LIMITS_THIS_UPDATE(0,0)
			ENDIF
		ENDIF
	ENDIF	
ENDPROC

PROC BLOCK_AVENGER_EXIT()
	IF thisArmoryAircraft.pOwner != INVALID_PLAYER_INDEX()
		IF IS_NET_PLAYER_OK(thisArmoryAircraft.pOwner)
			IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(thisArmoryAircraft.pOwner))
				IF IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR(thisArmoryAircraft.pOwner)
					IF GET_SIMPLE_INTERIOR_TYPE(GET_SIMPLE_INTERIOR_PLAYER_IS_WALKING_IN_OR_OUT_FROM(thisArmoryAircraft.pOwner)) = SIMPLE_INTERIOR_TYPE_DEFUNCT_BASE
						IF IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(thisArmoryAircraft.pOwner)), AVENGER)
							IF GET_OWNER_OF_PERSONAL_ARMORY_AIRCRAFT(GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(thisArmoryAircraft.pOwner))) = thisArmoryAircraft.pOwner
								IF NOT IS_BIT_SET(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_BLOCK_EXIT)
									BLOCK_SIMPLE_INTERIOR_EXIT(TRUE)
									SET_BIT(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_BLOCK_EXIT)
								ENDIF	
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF IS_BIT_SET(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_BLOCK_EXIT)
						BLOCK_SIMPLE_INTERIOR_EXIT(FALSE)
						CLEAR_BIT(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_BLOCK_EXIT)
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_BLOCK_EXIT)
					BLOCK_SIMPLE_INTERIOR_EXIT(FALSE)
					CLEAR_BIT(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_BLOCK_EXIT)
				ENDIF	
			ENDIF
		ENDIF	
	ENDIF
ENDPROC 

PROC MAINTAIN_ARMORY_AVENGER_RADIO()
		// I own the avenger
	IF thisArmoryAircraft.pOwner = PLAYER_ID()
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_TRUCK_RADIO_OFF_INITIAL_SETTING) 
			playerBD[NATIVE_TO_INT(PLAYER_ID())].iRadioStation = -1 // OFF				
		ELSE				
			IF playerBD[NATIVE_TO_INT(PLAYER_ID())].iRadioStation != g_iAvengerRadioStation
				playerBD[NATIVE_TO_INT(PLAYER_ID())].iRadioStation = g_iAvengerRadioStation
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "[SIMPLE_INTERIOR] MAINTAIN_ARMORY_AVENGER_RADIO - iRadioStation = ",playerBD[NATIVE_TO_INT(PLAYER_ID())].iRadioStation)
			ENDIF
		ENDIF
	ENDIF


		
	IF thisArmoryAircraft.pOwner != INVALID_PLAYER_INDEX()
		IF thisArmoryAircraft.iLocalRadioStation != playerBD[NATIVE_TO_INT(thisArmoryAircraft.pOwner)].iRadioStation  
			thisArmoryAircraft.iLocalRadioStation = playerBD[NATIVE_TO_INT(thisArmoryAircraft.pOwner)].iRadioStation  
			CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "[SIMPLE_INTERIOR] MAINTAIN_ARMORY_AVENGER_RADIO - iLocalRadioStation = ",thisArmoryAircraft.iLocalRadioStation)
			
			IF thisArmoryAircraft.iLocalRadioStation != -1
				SET_STATIC_EMITTER_ENABLED("se_xm_int_01_avngr_radio", TRUE)
				SET_EMITTER_RADIO_STATION("se_xm_int_01_avngr_radio", GET_RADIO_STATION_NAME(thisArmoryAircraft.iLocalRadioStation))
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "[SIMPLE_INTERIOR] MAINTAIN_ARMORY_AVENGER_RADIO - SET_STATIC_EMITTER_ENABLED - TRUE")
			ELSE
				SET_STATIC_EMITTER_ENABLED("se_xm_int_01_avngr_radio", FALSE)
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "[SIMPLE_INTERIOR] MAINTAIN_ARMORY_AVENGER_RADIO - SET_STATIC_EMITTER_ENABLED - FALSE")
			ENDIF
		ENDIF
	ENDIF
		


ENDPROC

PROC MAINTAIN_ARMORY_AIRCRAFT_RELOAD_WEAPONS()
	IF g_ownerOfArmoryAircraftPropertyIAmIn = PLAYER_ID()
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND NOT IS_BIT_SET(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_RELOAD_VEHICLE_WEAPONS)
			VEHICLE_INDEX vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
			APPLY_WEAPONS_TO_VEHICLE(vehTemp, TRUE)
			
			SET_BIT(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_RELOAD_VEHICLE_WEAPONS)
		ENDIF
	ENDIF
ENDPROC

PROC BLOCK_PLAYER_VEHICLE_ENTRY()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		INT iReason
		IF SHOULD_LP_BE_KICKED_FROM_ARMOURY_AIRCRAFT_TO_BASE_EXT(iReason)
		OR SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_ARMORY_AIRCRAFT(thisArmoryAircraft.eSimpleInteriorID )
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
			PRINTLN("BLOCK_PLAYER_VEHICLE_ENTRY - DISABLE_CONTROL_ACTION INPUT_ENTER")
		ENDIF
	ENDIF
ENDPROC 

PROC MAINTAIN_COCKPIT_ENTRY_HELP()
	IF IS_SCREEN_FADED_IN()
		IF IS_VEHICLE_SEAT_UNAVAILABLE()
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				PRINT_HELP("AV_COCK_UNAV")	
				SET_VEHICLE_SEAT_UNAVAILABLE(FALSE)
			ENDIF
		ENDIF
	ENDIF	
	
	IF IS_VEHICLE_SEAT_UNAVAILABLE()
		INITIALISE_ENTITY_SETS()
		LOAD_INTERIOR_TINTS()
	ENDIF
	
ENDPROC

PROC MAINTAIN_AVENGER_VEHICLE_INDEX_EXISTENCE()
	IF IS_OWNERS_ARMORY_AIRCRAFT_INSIDE_DEFUNCT_BASE(thisArmoryAircraft.pOwner)
		IF NOT IS_BIT_SET(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_NEED_TO_SET_VEHICLE_INDEX_EXISTENCE)
			SET_BIT(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_NEED_TO_SET_VEHICLE_INDEX_EXISTENCE)
		ENDIF	
	ELSE
		IF IS_BIT_SET(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_NEED_TO_SET_VEHICLE_INDEX_EXISTENCE)
			BROADCAST_REQUEST_VEHICLE_EXISTENCE(thisArmoryAircraft.pOwner, TRUE, FALSE, TRUE, FALSE #IF FEATURE_HEIST_ISLAND , FALSE #ENDIF )
			CLEAR_BIT(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_NEED_TO_SET_VEHICLE_INDEX_EXISTENCE)
		ENDIF	
	ENDIF		
ENDPROC

PROC MAINTAIN_XMAS_TREE()
	IF g_sMPTunables.bEnable_XMAS2017_Vehicle_Tinsel
		
		IF REQUEST_LOAD_MODEL(eXmasTinselProp)
			IF NOT DOES_ENTITY_EXIST(thisArmoryAircraft.oXmasTinsel)
				IF IS_VALID_INTERIOR(thisArmoryAircraft.interiorIndex)
					IF IS_INTERIOR_READY(thisArmoryAircraft.interiorIndex)
						thisArmoryAircraft.oXmasTinsel = CREATE_OBJECT_NO_OFFSET(eXmasTinselProp, <<519.1948, 4751.1260, -69.9240>>, FALSE)
						FORCE_ROOM_FOR_ENTITY(thisArmoryAircraft.oXmasTinsel, thisArmoryAircraft.interiorIndex, ARMORY_AIRCRAFT_ROOM_KEY)
						CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_ARMORY_AIRCRAFT - INITIALISE_ENTITY_SETS - create xmas tinsel")
					ENDIF	
				ENDIF	
			ENDIF	
		ENDIF
	ENDIF
ENDPROC


PROC RUN_MAIN_CLIENT_LOGIC()
	
	MAINTAIN_XMAS_TREE()
	MAINTAIN_LAUNCHING_CARMOD_SCRIPT()
	KICK_PLAYERS_OUT_OF_VEHICLE_IN_AIRCRAFT_AFTER_AUTOWARP()
	MAINTAIN_PLAYER_VEHICLE_LOCKS()
	MAINTAIN_ARMORY_AIRCRAFT_RELOAD_WEAPONS()
	MAINTAIN_CAN_DRIVE_OUT_OF_ARMORY_AIRCRAFT()
	MAINTAIN_BASE_ENTRY_FROM_ARMORY_AIRCRAFT_ON_FOOT()
	MAINTAIN_ENTER_BASE_FROM_ARMORY_AIRCRAFT_IN_VEHICLE()
	
	IF IS_ARMORY_AIRCRAFT_STATE(ARMORY_AIRCRAFT_STATE_LOADING)
		
		IF IS_INTERACTION_MENU_DISABLED()
			ENABLE_INTERACTION_MENU()
		ENDIF
		
		MAINTAIN_INTERIOR_LOAD()
		
	ELIF IS_ARMORY_AIRCRAFT_STATE(ARMORY_AIRCRAFT_STATE_IDLE)		
		IF !NETWORK_IS_IN_MP_CUTSCENE() 
			SET_PLAYER_MOVING_FROM_BASE_TO_AIRCRAFT(FALSE)
		ENDIF
		BLOCK_AVENGER_EXIT()
		MAINTAIN_SEATS_ACTIVITY(thisArmoryAircraft.seatData)
		MAINTAIN_AVENGER_CCTV()
		MAINTAIN_ARMORY_AIRCRAFT_BLIPS()
		RUN_CLIENT_ACTIVITIES()
		MAINTAIN_AVENGER_GUN_LOCKER(sAvengerGunLocker)
		BLOCK_VEHICLE_TURRET_SEAT()
		MAINTAIN_GET_OUT_OF_OWNERS_PERSONAL_VEHICLE()
		MAINTAIN_MOVE_OUT_OF_WAY()
		UPDATE_AVENGER_EXIT_MENU_OPTION()
		UN_FREEZE_PLAYER_VEHICLE_INSIDE_ARMORY_AIRCRAFT()
		MAINTAIN_VEHICLE_ZOOM()
		MAINTAIN_FIREPROOF_IN_AVENGER()
		MANAGE_TURRET_AUDIO_SCENE()
		MAINTAIN_GAMEPLAY_CAM_WHEN_PLAYER_INSIDE_VEHICLE()
		MAINTAIN_ARMORY_AVENGER_RADIO()
		MAINTAIN_COCKPIT_ENTRY_HELP()
		IF NOT IS_BIT_SET(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_CALLED_CLEAR_HELP)
			IF IS_SCREEN_FADED_IN()
			AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
				CLEAR_HELP()
				SET_BIT(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_CALLED_CLEAR_HELP)
				PRINTLN("AM_MP_ARMORY_AIRCRAFT - Calling clear help")
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_CALLED_CLEAR_HELP)
			IF IS_SCREEN_FADED_IN()
			AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
				CLEAR_HELP()
				SET_BIT(thisArmoryAircraft.iBS, BS_ARMORY_AIRCRAFT_CALLED_CLEAR_HELP)
				PRINTLN("AM_MP_ARMORY_AIRCRAFT - Calling clear help")
			ENDIF
		ENDIF
		KICK_PLAYERS_OUT_OF_VEHICLE_IN_AIRCRAFT_AFTER_AUTOWARP_AS_PASSENGER()
		MAINTAIN_CHAIR_TINTS()
		MAINTAIN_AVENGER_WEAPON_WORK_BENCH_TIMECYCLE_MODS()
		BLOCK_PLAYER_VEHICLE_ENTRY()
		MAINTAIN_AVENGER_VEHICLE_INDEX_EXISTENCE()
	ENDIF
	
	IF NETWORK_GET_HOST_OF_THIS_SCRIPT() != INT_TO_NATIVE(PARTICIPANT_INDEX , -1)	
		g_iHostOfam_mp_armory_aircraft = NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_HOST_OF_THIS_SCRIPT()))
	ENDIF
	
ENDPROC



PROC RUN_MAIN_SERVER_LOGIC()
	
ENDPROC


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ MAIN LOOP ╞═════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

SCRIPT  (SIMPLE_INTERIOR_CHILD_SCRIPT_DATA scriptData) 
	
	IF NETWORK_IS_GAME_IN_PROGRESS() 
		SCRIPT_INITIALISE(scriptData)	
	ELSE
		SCRIPT_CLEANUP()
	ENDIF

	// Main loop
	WHILE TRUE

		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
				
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("AM_MP_ARMORY_AIRCRAFT - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE(scriptData.iScriptInstance, scriptData.eSimpleInteriorID)
			PRINTLN("AM_MP_ARMORY_AIRCRAFT - SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		RUN_MAIN_CLIENT_LOGIC()
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			RUN_MAIN_SERVER_LOGIC()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		UPDATE_DEBUG_WIDGETS()
		#ENDIF
		
	ENDWHILE
	

ENDSCRIPT
