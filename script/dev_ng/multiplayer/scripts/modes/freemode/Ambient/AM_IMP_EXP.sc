/// Freemode Import Export
///    Dave W



USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_camera.sch"
USING "commands_hud.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"

// 

USING "net_mission.sch"
USING "net_scoring_common.sch"

USING "commands_path.sch"
USING "net_blips.sch"
USING "commands_zone.sch"

USING "hud_drawing.sch"
USING "net_ambience.sch"
USING "net_importexport.sch"

USING "shop_public.sch"
USING "net_garages.sch"

USING "net_importexport.sch"
USING "net_car_value.sch"
USING "net_mission_details_overlay.sch"
USING "net_fps_cam.sch"
USING "net_hud_displays.sch"
USING "fm_post_mission_cleanup_public.sch"

USING "commands_money.sch"

USING "vehicle_public.sch"

USING "net_wait_zero.sch"

USING "net_cash_transactions.sch"

USING "carmod_shop_private.sch"

USING "net_gang_boss.sch"

#IF IS_DEBUG_BUILD
	USING "profiler.sch"
#ENDIF

#IF IS_DEBUG_BUILD
	//BOOL bIgnoreRenderTarget
	BOOL bActivateInFlow
#ENDIF

CONST_INT CLIENT_STATE_WAITING_TO_SWITCH_ON		0
CONST_INT CLIENT_STATE_WAITING_TO_SWITCH_OFF	1

CONST_INT PLAYER_DELIVERY_STATE_WAITING_FOR_CAR				0
CONST_INT PLAYER_DELIVERY_WAIT_TO_BE_WARPED_FROM_GARAGE		1
CONST_INT PLAYER_DELIVERY_STATE_TAKE_DELIVERY				2
CONST_INT PLAYER_DELIVERY_STATE_RESTORE						3

CONST_INT MECHANIC_STATE_IDLE 0
CONST_INT MECHANIC_STATE_PERFORMING_TASK 1
CONST_INT MECHANIC_STATE_FINISHED_TASK	2
CONST_INT MECHANIC_STATE_CANCELLED_TASK 3

CONST_INT BOARD_RENDER_STATE_NULL 			0
CONST_INT BOARD_RENDER_STATE_DRAWING 		1

CONST_INT MAX_HIGH_PRIORITY_VEHICLE_START_POSITIONS		10


CONST_INT LIST_STATE_INIT 				0
CONST_INT LIST_STATE_RUNNING 			1
CONST_INT LIST_STATE_WAITING_TO_CLEAR 	2
CONST_INT LIST_STATE_WAIT_FOR_HPV		3

CONST_INT MIN_NUM_OF_CARS_ON_LIST 3

//INT TEXT_MESSAGE_DELAY = 10000 //10000

CONST_INT BI_IMPEXP_FIRST_LAUNCH		0
CONST_INT BI_IMPEXP_FLASHED_BLIP_ONCE	1
CONST_INT BI_IMPEXP_FLASHED_BLIP_TWICE	2
CONST_INT BI_IMPEXP_OBJECTIVE_ONCE		3
CONST_INT BI_IMPEXP_OBJECTIVE_TWICE		4
CONST_INT BI_IMPEXP_OBJECTIVE_THREE		5

CONST_INT MAX_CAR_GENS		10



TWEAK_INT 	iMAX_REDELIVER_TIME		120000


SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerData


BOOL bNewCarList
BOOL bIAmDriver
//BOOL bTutorialInitialised
//BOOL bViewCamCreated
BOOL bRequestGarageOpen
BOOL bGarageStateOpen
BOOL bDisplayImpExpBlip
BOOL bDisplayCarmodBlip


BOOL bDisplayCarmodObjective
BOOL bDisplaySimeonObjective
BOOL bDisplayResprayObjective
BOOL bDisplayFixCarObjective
BOOL bDisplayLeaveCarmodObjective
BOOL bDisplayLoseCopsObjective
BOOL bPlayerInVehicleReadyForDelivery
BOOL bSimeonObjectiveTimerExpired
BOOL bHpvBlipSetShortRange
//BOOL bExplainWhyBlipRemoved_WantedLevel
BOOL bStreamingInCarModel
BOOL bWarpDisabled

BOOL bInWantedCarWhenListReceived = TRUE
BOOL bAllowUnwantedInGarage = FALSE

BOOL bDoStaggeredServerLoop = TRUE
INT iServerStaggeredCount

BOOL bSHowNewTextFeed = TRUE
BOOL bNewCarListThisSession
BOOL bSendThanksTextMessage
BOOL bSwitchedOffOtherCarList
BOOL bRemoveHpvBlipOnImpromptu
SHOP_NAME_ENUM shopClosestCarMod = EMPTY_SHOP

PED_INDEX pedMechanic
INT iMechanicProg
SCRIPT_TIMER timeMechanicCheck
SCRIPT_TIMER timeThankYouText
structPedsForConversation speechMechanic 
VEHICLE_INDEX vehMechanic

SCRIPT_TIMER timeWaitForCarListToArrive

VEHICLE_GENERATOR_INDEX vehGenImportExport[MAX_CAR_GENS]
BOOL bCarGensAdded
BOOL bCarWrecked
BOOL bDoCollectHighPriorityVehicle = TRUE
BOOL bUseLauncher = TRUE
INT iLocalReserveCount

#IF IS_DEBUG_BUILD

BOOL bWdUseDebug = FALSE
BOOL bWdDoStaggeredDebug = FALSE
BOOL bWdDoHpvDebug = FALSE
BOOL bWdSetAllCarsDelivered
BOOL bWdSetResprayed
BOOL bWdWarpToGarage
BOOL bWdLaunchHighPriority
BOOL bWdNotRecDel
BOOL bWdSHowVel
BOOL bWdServerBitSet
BOOL bWdPrintListModels
BOOL bWdClearObjectiveStat
BOOL bWdForceObjective
BOOL bWdSprayStolenCheck
BOOL bWdSprayed
BOOL bWdStolen
BOOL bWdSetAsStolen
BOOL bWdClearStolen
BOOL bWdDeliveredToday
BOOL bWdReceivedToday
BOOL bWdClearWanted
BOOL bWdDeleteTextMessages
BOOL bWdClearLastDeliveredTime
BOOL bWdCheckForCrewEmblem
BOOL bWdCrewEmblem
//VECTOR vWdVel
INT iWdHpvProg
INT iWdMyHpvProg
INT iWdServHpvProg

#ENDIF
INT iCandidate = 0
INT iTickerPartDrivingHpv = -1
INT iLocalPartDrivingHpv = -1
//BOOL bPlayerActivatedList


// help 

CONST_INT HELP_SWITCH_ON_UV					0
CONST_INT HELP_DELIVER_CARS_ON_BOARD		1
CONST_INT HELP_RESPRAY_MESSAGE				2
CONST_INT HELP_DELIVER_MESSAGE				3
CONST_INT HELP_NO_DELIVER_WANTED			4
CONST_INT HELP_LAUNCH_IMPEXP				5
CONST_INT HELP_HOOK_CAR						6
CONST_INT HELP_VEH_RIGGED					7
CONST_INT HELP_NO_CASH						8
CONST_INT HELP_NOT_NEEDED					9
CONST_INT HELP_PERSONAL_VEH					10
CONST_INT HELP_INITIAL_WANTED				11
CONST_INT HELP_CREW_EMBLEM					12

//BLIP_INDEX g_blipSimeonGarage
//BLIP_INDEX CarModBlip





//CAMERA_INDEX ViewCam

INT iTotalCashValue
INT iTotalDamage
FLOAT fCash
FLOAT fXp
INT iActualCash
INT iActualXp 

INT iInitialRepairCostforVehicle

SCRIPT_TIMER timeInitialHelp
SCRIPT_TIMER timeFinishHpv
SCRIPT_TIMER timeHpvTimeOfDayCheck
SCRIPT_TIMER timeHelpCheck
SCRIPT_TIMER timeGlobalOnListCheck
SCRIPT_TIMER timeGetRandomCarCHeck
SCRIPT_TIMER timeObjectiveHelp

INT iBS_Help


//INT iImportExportState
//INT iClientBoardState


//TIME_DATATYPE iViewingCamTimer
TIME_DATATYPE iListCompletedTimer



INT iContextID = NEW_CONTEXT_INTENTION

//timer for issue with syncing in garage checks
SCRIPT_TIMER SyncDelayTimerGarage

INT iLocalUpdateID = -1

INT iMainLoopCount = 0
 

//BOOL bSetUpCarGens


MODEL_NAMES StreamedInModel = DUMMY_MODEL_FOR_SCRIPT


PLAYER_INDEX PlayerInCar[4]
//PED_INDEX tempPed[4] 





VEHICLE_INDEX DeliveredCar
VEHICLE_INDEX vehLastDeliveredCar
VEHICLE_INDEX vehDeliverToSimeonObjective

MODEL_NAMES mLastDeliveredCar

POPSCHEDULE_ID RichPopSchedules[12]
	
IMPORT_EXPORT_LIST_OF_CARS ThisCarList

//BLIP_INDEX g_blipHighPriorityVeh

ENUM HIGH_PRIORITY_VEHICLE
	HPV_INIT,
	HPV_GET_CAR,
	HPV_CLEANUP_CAR,
	HPV_FINISHED
ENDENUM

CONST_INT biS_RunHighPriorityVehicle		0
CONST_INT biS_AllPlayersDone				1
CONST_INT biS_DoneVehWreckedTick			2
CONST_INT biS_SomeoneNearHpv				3
CONST_INT biS_DoneInitialCountdown			4
CONST_INT biS_InitRecentDel					5
CONST_INT biS_SomeoneFoundCarInGarageNoDri	6
CONST_INT biS_FOundOmeoneNotFinishedHpv		7
CONST_INT biS_SomeoneEnteredCarAtLeastOnce	8
CONST_INT biS_ResetNoCollision				9

CONST_INT MAX_RECENTLY_DELIVERED_CARS		10

// Everyone can read this data, only the server can update it. 
STRUCT ServerBroadcastData
	//INT CarList[MAX_NUM_OF_CARS_ON_IMPORT_EXPORT_LIST]
	//BOOL bDeliveredCar[MAX_NUM_OF_CARS_ON_IMPORT_EXPORT_LIST]
	IMPORT_EXPORT_LIST_OF_CARS CarList
	INT Notes[MAX_NUM_OF_NOTES]
	BOOL bBoardIsOn
	INT iLastDeliveredCar = -1
	PLAYER_INDEX LastPersonToDeliver
	INT iCarListState	
	TIME_DATATYPE iLightTimer
	NETWORK_INDEX niPriorityVeh
	INT iServerBitSet
	INT iHighPriorityCreateAreaId = -1
	INT iTelemetryId
	HIGH_PRIORITY_VEHICLE hpvServerProg
	TIMEOFDAY hpvTriggerTime = INVALID_TIMEOFDAY
	#IF IS_DEBUG_BUILD
		INT iHostPlayerInt
	#ENDIF
	INT iListIDToStreamIn = -1
	TIME_DATATYPE iStreamInCarTimer
	SCRIPT_TIMER timeHpvLaunched
	INT iUpdateID
	BOOL bNewList
	INT iNumReservedVeh = 0
	INT iRecentlyDelivered[MAX_RECENTLY_DELIVERED_CARS]
	SCRIPT_TIMER timeRecentlyDelivered[MAX_RECENTLY_DELIVERED_CARS]
	MODEL_NAMES mHighPriority = DUMMY_MODEL_FOR_SCRIPT
	
	INT iWarpWhenStuckInModshopProg
	INT iHpvNoCollisionProg
	VECTOR vStuckWarpTo
	FLOAT fStuckWarpHead
ENDSTRUCT
ServerBroadcastData serverBD

SCRIPT_TIMER timeMaintainRecentlydelivered

CONST_INT biP_RunningFps			0
CONST_INT biP_DeliveredSpecVehicle 	1
CONST_INT biP_NearHpv				2
CONST_INT biP_HpvInGarageNoDriver	3

STRUCT PlayerBroadcastData
	INT iVehicleListID = -1
	INT iDeliveryState
	INT iImportExportBitSet
	HIGH_PRIORITY_VEHICLE hpvProg
ENDSTRUCT
PlayerBroadcastData PlayerBD[NUM_NETWORK_PLAYERS]

INT iLocalImpExpBitSet
CONST_INT biL_NobodyOnFps					0
CONST_INT biL_SentInitialText				1
CONST_INT biL_OkToRun						2
CONST_INT biL_SomeoneDelSpecCar				3
CONST_INT biL_HelpDelSpecCar				4
CONST_INT BiL_LoopedThroughEveryone			5
CONST_INT biL_InVehicleOnList				6
CONST_INT biL_SetLocalPlayerOnAmb			7
CONST_INT biL_ClearedPreviousText			8
CONST_INT biL_FOundOmeoneNotFinishedHpv		9
CONST_INT BiL_ServerLoopedThroughEveryone	10
CONST_INT biL_FoundSomeoneNearHpv			11
CONST_INT biL_InRecentlyDeliveredCar		12
CONST_INT biL_AllCompleteXpBonus			13
CONST_INT biL_BonusXpFirstNewListCheck		14
CONST_INT biL_PlayerInPersonalVehicle		15
CONST_INT biL_ReservedForHpv				16
CONST_INT biL_InGarageInitialCheck			17	
CONST_INT biL_DoneAllObjectiveText			18
CONST_INT biL_DoneJackHelpTextThisInstance	19
CONST_INT biL_PlayerOwnedbackup				20
CONST_INT biL_SomeoneFoundHpvInGarageNoDri	21
CONST_INT biL_CreateMechanic				22
CONST_INT biL_HpvJustLaunched				23


INT iLocalStatBitset

CONST_INT biStat_DoneAllObjectiveText		0
CONST_INT biStat_DoneObjectiveOnce			1
CONST_INT biStat_DoneObjectiveTwice			2
CONST_INT biStat_DoneObjectiveThree			3



#IF IS_DEBUG_BUILD
	BOOL bTimedDebug
	SCRIPT_TIMER timeDebug
	BOOL bDisableAllDebug = FALSE
	PROC NET_DW_PRINT(STRING sText)
		IF NOT bDisableAllDebug
			NET_PRINT_TIME() NET_PRINT("[FM_ImportExport] [DSW] ") NET_PRINT_STRINGS(" ", sText) NET_NL()
		ENDIF
	ENDPROC 

	PROC NET_DW_PRINT_STRING_INT(STRING sText1, INT i)
		IF NOT bDisableAllDebug
			NET_PRINT_TIME() NET_PRINT("[FM_ImportExport] [DSW] ") NET_PRINT_STRING_INT(sText1, i) NET_NL()
		ENDIF
	ENDPROC

	PROC NET_DW_PRINT_STRING_FLOAT(STRING sText1, FLOAT f)
		IF NOT bDisableAllDebug
			NET_PRINT_TIME() NET_PRINT("[FM_ImportExport] [DSW] ") NET_PRINT_STRING_FLOAT(sText1, f) NET_NL()
		ENDIF
	ENDPROC

	PROC NET_DW_PRINT_STRING_VECTOR(STRING sText1, VECTOR v)
		IF NOT bDisableAllDebug
			NET_PRINT_TIME() NET_PRINT("[FM_ImportExport] [DSW] ") NET_PRINT_STRING_VECTOR(sText1, v) NET_NL()
		ENDIF
	ENDPROC
	
	PROC NET_DW_PRINT_STRINGS(STRING sText1, STRING sText2)
		IF NOT bDisableAllDebug
			NET_PRINT_TIME() NET_PRINT("[FM_ImportExport] [DSW] ") NET_PRINT_STRINGS(sText1, sText2) NET_NL()
		ENDIF
	ENDPROC
	
	PROC NET_DW_PRINT_STRING_WITH_PLAYER_NAME(STRING sText, PLAYER_INDEX player)
		IF NOT bDisableAllDebug
			NET_PRINT_TIME() NET_PRINT("[FM_ImportExport] [DSW] ") NET_PRINT_STRINGS(" ", sText) NET_PRINT_STRINGS(" ", GET_PLAYER_NAME(player)) NET_NL()
		ENDIF
	ENDPROC
	
	PROC NET_DW_PRINT_HOST_NAME()
		IF NOT bDisableAllDebug
			NET_PRINT_TIME() NET_PRINT("[FM_ImportExport] [DSW] Current host... ") NET_PRINT(GET_PLAYER_NAME(NETWORK_GET_HOST_OF_SCRIPT("am_imp_exp"))) NET_NL() 
		ENDIF
	ENDPROC
#ENDIF



//PURPOSE:checks to see if the vehicle is stuck or undrivable
FUNC BOOL MISSION_VEHICLE_STUCK_CHECKS()
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPriorityVeh)
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niPriorityVeh)
			IF IS_VEHICLE_STUCK_TIMER_UP(NET_TO_VEH(serverBD.niPriorityVeh), VEH_STUCK_ON_ROOF, ROOF_TIME)
			OR IS_VEHICLE_STUCK_TIMER_UP(NET_TO_VEH(serverBD.niPriorityVeh), VEH_STUCK_ON_SIDE, SIDE_TIME)
				IF IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.niPriorityVeh)
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niPriorityVeh)
						SET_VEHICLE_ENGINE_HEALTH(NET_TO_VEH(serverBD.niPriorityVeh),-2000)
						SET_VEHICLE_PETROL_TANK_HEALTH(NET_TO_VEH(serverBD.niPriorityVeh), 0.0)
						#IF IS_DEBUG_BUILD
							NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_CLIENT] MISSION_VEHICLE_STUCK_CHECKS Vehicle is stuck and I set engine health")
						#ENDIF
						
					ENDIF
					RETURN TRUE
				ELSE
					IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
						IF TAKE_CONTROL_OF_NET_ID(serverBD.niPriorityVeh)
							SET_VEHICLE_ENGINE_HEALTH(NET_TO_VEH(serverBD.niPriorityVeh),-2000)
							SET_VEHICLE_PETROL_TANK_HEALTH(NET_TO_VEH(serverBD.niPriorityVeh), 0.0)
							#IF IS_DEBUG_BUILD
								NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_CLIENT] MISSION_VEHICLE_STUCK_CHECKS Vehicle is stuck and server set engine health")
							#ENDIF
							RETURN TRUE
						ENDIF
					ELSE
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF 
		ELSE
			RETURN TRUE
		ENDIF 
	ENDIF 
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_HIDE_LOCAL_UI()
// Removed content
//	IF HANDLE_DEPRECATED_HIDE_BLIP(ciPI_HIDE_MENU_DEPRECATED_AMB_IMP_EXP)
//		RETURN TRUE
//	ENDIF
	
	IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID(), TRUE)
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_IN_CASINO(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC




FUNC BOOL IS_OK_TO_PRINT_HELP()
//	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
//	OR IS_PED_INJURED(PLAYER_PED_ID())
//	OR NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
//	OR  IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
//	OR IS_SCRIPT_HUD_DISPLAYING(HUDPART_MISSIONBRIEF)
//	OR IS_MP_MISSION_DETAILS_OVERLAY_ON_DISPLAY()
//	OR IS_SCRIPT_HUD_DISPLAYING(HUDPART_MISSIONBOX)
//	OR IS_HELP_MESSAGE_BEING_DISPLAYED()
//	OR IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
//	OR IS_FM_MISSION_LAUNCH_IN_PROGRESS()
//	OR IS_CUSTOM_MENU_ON_SCREEN() 
//	OR IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
//	OR IS_MISSION_SUMMARY_SCREEN_DISPLAYED()
//	OR GET_LOCAL_PLAYER_CORONA_POS_STATE() > 0
//	OR NOT IS_SKYSWOOP_AT_GROUND()
//	OR NOT IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
//		RETURN(FALSE)
//	ENDIF

	IF SHOULD_HIDE_LOCAL_UI()
		RETURN FALSE
	ENDIF
	
	IF NOT IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
	//	#IF IS_DEBUG_BUILD NET_DW_PRINT("Not ok to print help") #ENDIF
		RETURN FALSE
	ENDIF
	RETURN(TRUE)
ENDFUNC

FUNC BOOL OK_TO_PRINT_OBJECTIVE_TEXT()
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
	OR IS_PED_INJURED(PLAYER_PED_ID())
	OR NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
	OR  IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
	OR IS_SCRIPT_HUD_DISPLAYING(HUDPART_MISSIONBRIEF)
	OR IS_MP_MISSION_DETAILS_OVERLAY_ON_DISPLAY()
	OR IS_SCRIPT_HUD_DISPLAYING(HUDPART_MISSIONBOX)
	OR IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
	OR IS_FM_MISSION_LAUNCH_IN_PROGRESS()
	OR IS_CUSTOM_MENU_ON_SCREEN() 
	OR IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
	OR IS_MISSION_SUMMARY_SCREEN_DISPLAYED()
	OR Is_MP_Comms_Still_Playing()
	OR IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	OR IS_MESSAGE_BEING_DISPLAYED()
	OR GET_LOCAL_PLAYER_CORONA_POS_STATE() > 0
	OR NOT IS_SKYSWOOP_AT_GROUND()
	OR IS_LOCAL_PLAYER_DOING_ANY_AMBIENT_TUTORIAL()
	OR SHOULD_HIDE_LOCAL_UI()
		RETURN(FALSE)
	ENDIF
	
	
	RETURN(TRUE)
ENDFUNC



PROC SET_ALL_CARS_DELIVERED()
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		INT i
		REPEAT MAX_NUM_OF_CARS_ON_IMPORT_EXPORT_LIST i
			IF NOT (serverBD.CarList.bIsDelivered[i])	
				serverBD.CarList.bIsDelivered[i] = TRUE
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC	

PROC INIT_RECENTLY_DELIVERED()
	INT i
	IF NOT IS_BIT_SET(serverBD.iServerBitSet,biS_InitRecentDel )
		REPEAT MAX_RECENTLY_DELIVERED_CARS i
			serverBD.iRecentlyDelivered[i] = -1
		ENDREPEAT
		#IF IS_DEBUG_BUILD NET_DW_PRINT("Done INIT_RECENTLY_DELIVERED()") #ENDIF
		SET_BIT(serverBD.iServerBitSet,biS_InitRecentDel)
	ENDIF
ENDPROC


PROC SET_MODEL_AS_RECENTLY_DELIVERED_FROM_LIST_ID(INT iListId)
	INT iFirstExpired = -1
	INT iCounter
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		#IF IS_DEBUG_BUILD NET_DW_PRINT("SET_MODEL_AS_RECENTLY_DELIVERED_FROM_LIST_ID called... ") #ENDIF
		IF IS_BIT_SET(serverBD.iServerBitSet,biS_InitRecentDel )
			FOR iCounter = 0 TO MAX_RECENTLY_DELIVERED_CARS - 1
				IF serverBD.iRecentlyDelivered[iCounter] = -1
					//-- Entry in list of recently delievered car list is blank
					serverBD.iRecentlyDelivered[iCounter] = iListId
					RESET_NET_TIMER(serverBD.timeRecentlyDelivered[iCounter])
					START_NET_TIMER(serverBD.timeRecentlyDelivered[iCounter])
					#IF IS_DEBUG_BUILD 
						NET_DW_PRINT_STRINGS("Setting model as recently delivered... ", GET_MODEL_NAME_FOR_DEBUG(GET_MODEL_NAME_FROM_LIST_ID(iListId))) 
						NET_DW_PRINT_STRING_INT("Setting list ID as recently delivered... ", iListId)
					#ENDIF
					EXIT
				ELSE
					//-- See if any of the entries in the the recently delivered list are over 2 minutes old
					IF serverBD.iRecentlyDelivered[iCounter] <> iListId
						IF HAS_NET_TIMER_STARTED(serverBD.timeRecentlyDelivered[iCounter])
							IF HAS_NET_TIMER_EXPIRED(serverBD.timeRecentlyDelivered[iCounter], 120000)
								iFirstExpired = iCounter
							ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD 
							NET_DW_PRINT_STRINGS("Model was already recently delivered... ", GET_MODEL_NAME_FOR_DEBUG(GET_MODEL_NAME_FROM_LIST_ID(iListId))) 
							NET_DW_PRINT_STRING_INT("List ID was already recently delivered... ", iListId)
						#ENDIF
						EXIT
					ENDIF
				ENDIF
			ENDFOR
			
			IF iFirstExpired > -1
				serverBD.iRecentlyDelivered[iFirstExpired] = iListId
				RESET_NET_TIMER(serverBD.timeRecentlyDelivered[iFirstExpired])
				START_NET_TIMER(serverBD.timeRecentlyDelivered[iFirstExpired])
				#IF IS_DEBUG_BUILD 
					NET_DW_PRINT_STRINGS("Setting model as recently delivered... ", GET_MODEL_NAME_FOR_DEBUG(GET_MODEL_NAME_FROM_LIST_ID(iListId))) 
					NET_DW_PRINT_STRING_INT("Setting list ID as recently delivered... ", iListId)
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL WAS_MODEL_RECENTLY_DELIVERED(MODEL_NAMES mCar)
	INT iListId = GET_LIST_ID_FROM_MODEL(mCar)
	INT i
	IF iListId > -1
		REPEAT MAX_RECENTLY_DELIVERED_CARS i
			IF serverBD.iRecentlyDelivered[i] = iListId
				IF NOT HAS_NET_TIMER_EXPIRED(serverBD.timeRecentlyDelivered[i], iMAX_REDELIVER_TIME ) //120000
				#IF IS_DEBUG_BUILD AND NOT bWdNotRecDel #ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_RECENTLY_DELIVERED_CAR()
	VEHICLE_INDEX veh
	
	IF HAVE_I_DELIVERED_A_VEHICLE_TODAY()
		RETURN FALSE
	ENDIF
	
	IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
		veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF NOT IS_VEHICLE_A_PERSONAL_VEHICLE(veh)
		AND NOT IS_VEHICLE_A_TEST_DRIVE_VEHICLE(veh)
			
			IF WAS_MODEL_RECENTLY_DELIVERED(GET_ENTITY_MODEL(veh))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//-- From Conor, need to check for a vheilce about to be set as a personal vehicle
FUNC BOOL ABOUT_TO_SET_AS_PERSONAL_VEHICLE()
	IF MPGlobals.VehicleData.bAssignToMainScript
 	OR MPGlobals.VehicleData.bAwaitingAssignToMainScript
		
		RETURN TRUE
	ENDIF
	
	
	
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_RECENTLY_DELIVERED()
	INT i = 0
	
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF NOT HAS_NET_TIMER_STARTED(timeMaintainRecentlydelivered)
			START_NET_TIMER(timeMaintainRecentlydelivered)
			#IF IS_DEBUG_BUILD NET_DW_PRINT("Started timeMaintainRecentlydelivered") #ENDIF
		ELSE
			IF HAS_NET_TIMER_EXPIRED(timeMaintainRecentlydelivered, 5000)
				RESET_NET_TIMER(timeMaintainRecentlydelivered)
				
				
				REPEAT MAX_RECENTLY_DELIVERED_CARS i
					IF serverBD.iRecentlyDelivered[i] > -1
						IF HAS_NET_TIMER_EXPIRED(serverBD.timeRecentlyDelivered[i], 120000)
							serverBD.iRecentlyDelivered[i] = -1
							RESET_NET_TIMER(serverBD.timeRecentlyDelivered[i])
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
		ENDIF
	ENDIF
ENDPROC




FUNC BOOL ServerAreAllCarsDelivered()
	INT i
	REPEAT MAX_NUM_OF_CARS_ON_IMPORT_EXPORT_LIST i
		IF NOT (serverBD.CarList.bIsDelivered[i])	
			RETURN(FALSE)
		ENDIF
	ENDREPEAT
	RETURN(TRUE)
ENDFUNC

FUNC MODEL_NAMES GetModelOfVehiclePlayerIsIn()
	MODEL_NAMES ReturnModel
	VEHICLE_INDEX CarID
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) 
		CarID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())	
		IF NOT IS_ENTITY_A_MISSION_ENTITY (CarID)	
			IF IS_VEHICLE_DRIVEABLE(CarID)
				ReturnModel = GET_ENTITY_MODEL(CarID)
			ENDIF
		ENDIF
	ENDIF
	RETURN(ReturnModel)
ENDFUNC

FUNC BOOL DOES_PLAYER_HAVE_ENOUGH_CASH_FOR_RESPRAY(INT &iRepairCost)
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		//INT iCurrentCash = GET_LOCAL_PLAYER_VC_AMOUNT(TRUE)//GET_PLAYER_CASH(PLAYER_ID())
		
		IF DOES_ENTITY_EXIST(veh)
		AND NETWORK_HAS_CONTROL_OF_ENTITY(veh)
			iRepairCost = GET_CARMOD_REPAIR_COST(veh)
			
			#IF IS_DEBUG_BUILD
				IF bWdUseDebug
					NET_DW_PRINT_STRING_INT("[DOES_PLAYER_HAVE_ENOUGH_CASH_FOR_RESPRAY] Reapir cost... ", iRepairCost)
				ENDIF
			#ENDIF
			IF !NETWORK_CAN_SPEND_MONEY(400 + iRepairCost, FALSE, FALSE, TRUE)
				RETURN FALSE
			ENDIF
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Determine if the player is in the high priority (blipped) import/export vehicle)
/// RETURNS:
///    
FUNC BOOL IS_PLAYER_IN_SPECIFIC_IMPORT_EXPORT_VEHICLE()
	IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())		
		IF IS_BIT_SET(serverbd.iServerBitSet, biS_RunHighPriorityVehicle)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.niPriorityVeh)
				IF IS_NET_VEHICLE_DRIVEABLE(serverbd.niPriorityVeh)
					IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverbd.niPriorityVeh))
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC



FUNC BOOL IsPlayerInWantedImportExportCar()
	IF HAVE_I_DELIVERED_A_VEHICLE_TODAY()
		#IF IS_DEBUG_BUILD
			IF bTimedDebug
				NET_DW_PRINT("[bTimedDebug] [IsPlayerInWantedImportExportCar] Failed on HAVE_I_DELIVERED_A_VEHICLE_TODAY")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())		
		IF NOT g_bPlayerInProcessOfExitingInterior 
		//	#IF IS_DEBUG_BUILD NET_DW_PRINT("g_bPlayerInProcessOfExitingInterior is not set") #ENDIF
			VEHICLE_INDEX CarID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())			
			IF NOT IS_VEHICLE_A_PERSONAL_VEHICLE(CarID)
			AND NOT IS_VEHICLE_A_TEST_DRIVE_VEHICLE(CarID)
			//	IF (IS_PLAYER_IN_SPECIFIC_IMPORT_EXPORT_VEHICLE() OR NOT IS_ENTITY_A_MISSION_ENTITY(CarID)	)
					IF NOT DOES_VEHICLE_HAVE_CREW_EMBLEM(CarID)
						IF IS_VEHICLE_DRIVEABLE(CarID, TRUE)
						//	IF IS_VEHICLE_DRIVEABLE(CarID, TRUE)
						//	IF NOT IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), FALSE)
							IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty <= 0
								IF NOT DECOR_EXIST_ON(CarID, "Not_Allow_As_Saved_Veh")
								
									IF NOT g_bPropInteriorScriptRunning
										IF bCarWrecked
											bCarWrecked = FALSE
											#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing bCarWrecked") #ENDIF
										ENDIF
										IF IS_MODEL_ON_WANTED_LIST(ThisCarList, GET_ENTITY_MODEL(CarID) #IF IS_DEBUG_BUILD ,bTimedDebug #ENDIF)
											mpglobalsambience.bIsInImportExportCar = TRUE
											
											#IF IS_DEBUG_BUILD
												IF bTimedDebug
													NET_DW_PRINT("[bTimedDebug] [IsPlayerInWantedImportExportCar] TRUE!")
												ENDIF
											#ENDIF
											
											RETURN(TRUE)
										ELSE
											#IF IS_DEBUG_BUILD
												IF bTimedDebug
													NET_DW_PRINT("[bTimedDebug] [IsPlayerInWantedImportExportCar] Failed on IS_MODEL_ON_WANTED_LIST")
												ENDIF
											#ENDIF
										ENDIF
									ELSE
										#IF IS_DEBUG_BUILD
											IF bTimedDebug
												NET_DW_PRINT("[bTimedDebug] [IsPlayerInWantedImportExportCar] Failed on g_bPropInteriorScriptRunning")
											ENDIF
										#ENDIF
									ENDIF
								ELSE
									#IF IS_DEBUG_BUILD
										IF bTimedDebug
											NET_DW_PRINT("[bTimedDebug] [IsPlayerInWantedImportExportCar] Failed on DECOR_EXIST_ON Not_Allow_As_Saved_Veh")
										ENDIF
									#ENDIF
								ENDIF
								
							ELSE
								#IF IS_DEBUG_BUILD
									IF bTimedDebug
										NET_DW_PRINT("[bTimedDebug] [IsPlayerInWantedImportExportCar] Failed on iCurrentlyInsideProperty = 0")
									ENDIF
								#ENDIF
							ENDIF
					//		ELSE
								// On fire
					//		ENDIF
						ELSE
							IF NOT bCarWrecked
								#IF IS_DEBUG_BUILD NET_DW_PRINT("Setting bCarWrecked") #ENDIF
								bCarWrecked = TRUE
							ENDIF
							
							#IF IS_DEBUG_BUILD
								IF bTimedDebug
									NET_DW_PRINT("[bTimedDebug] [IsPlayerInWantedImportExportCar] Failed on IS_VEHICLE_DRIVEABLE")
								ENDIF
							#ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							IF bTimedDebug
								NET_DW_PRINT("[bTimedDebug] [IsPlayerInWantedImportExportCar] Failed on DOES_VEHICLE_HAVE_CREW_EMBLEM")
							ENDIF
						#ENDIF
					ENDIF

			
			ELSE
				#IF IS_DEBUG_BUILD
					IF bTimedDebug
						NET_DW_PRINT("[bTimedDebug] [IsPlayerInWantedImportExportCar] Failed on IS_VEHICLE_A_PERSONAL_VEHICLE")
					ENDIF
				#ENDIF
			ENDIF
		ELSE
		
			#IF IS_DEBUG_BUILD
				IF bTimedDebug
					NET_DW_PRINT("[bTimedDebug] [IsPlayerInWantedImportExportCar] Failed on g_bPlayerInProcessOfExitingInterior")
				ENDIF
			#ENDIF
		ENDIF
	ELSE
	//	#IF IS_DEBUG_BUILD NET_DW_PRINT("[IsPlayerInWantedImportExportCar] IS_PED_SITTING_IN_ANY_VEHICLE")  #ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IsPlayerDriver()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())		
			VEHICLE_INDEX CarID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())		
			IF IS_VEHICLE_DRIVEABLE(CarID)
				IF NOT IS_VEHICLE_SEAT_FREE(CarID, VS_DRIVER )
					IF (GET_PED_IN_VEHICLE_SEAT(CarID, VS_DRIVER) = PLAYER_PED_ID()	)
						RETURN(TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IsListIDOnList(INT iListID, IMPORT_EXPORT_LIST_OF_CARS inList, BOOL bIsStillWantedCheck=FALSE)
	INT i
	IF NOT (iListID = -1)
		REPEAT 	MAX_NUM_OF_CARS_ON_IMPORT_EXPORT_LIST i
			IF (inList.iCar[i] = iListID)					
				IF ((bIsStillWantedCheck=TRUE) AND inList.bIsDelivered[i] = FALSE)
				OR (bIsStillWantedCheck=FALSE)
					RETURN(TRUE)
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	RETURN(FALSE)
ENDFUNC


FUNC BOOL IsNoteIDOnList(INT iNoteID)
	INT i
	IF NOT (iNoteID = -1)
		REPEAT 	MAX_NUM_OF_NOTES i
			IF (serverBD.Notes[i] = iNoteID)					
				RETURN(TRUE)
			ENDIF
		ENDREPEAT
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC INT GetRandomCarOnWantedList(IMPORT_EXPORT_LIST_OF_CARS &inList)
	INT iOffset = GET_RANDOM_INT_IN_RANGE(0, MAX_NUM_OF_CARS_ON_IMPORT_EXPORT_LIST)	
	INT i		
	INT iPos
	REPEAT MAX_NUM_OF_CARS_ON_IMPORT_EXPORT_LIST i
		iPos = i + iOffset
		IF (iPos >= MAX_NUM_OF_CARS_ON_IMPORT_EXPORT_LIST)
			iPos -= MAX_NUM_OF_CARS_ON_IMPORT_EXPORT_LIST
		ENDIF
		IF NOT (inList.iCar[iPos] = -1)
			IF (inList.bIsDelivered[iPos] = FALSE)
				RETURN(inList.iCar[iPos])
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN(-1)
ENDFUNC

FUNC INT GetRandomCarNotAlreadyOnLists(IMPORT_EXPORT_LIST_OF_CARS &inList, IMPORT_EXPORT_LIST_OF_CARS &oldList)
	INT iRand = GET_RANDOM_INT_IN_RANGE(0, TOTAL_NUMBER_OF_CARS)		
	WHILE IsListIDOnList(iRand, inList)
	OR IsListIDOnList(iRand, oldList)
	OR iRand = 19 // Don't want daemon in Freemode
	OR iRand = 6 // Don't want z-type in FM - 980689 
		iRand = GET_RANDOM_INT_IN_RANGE(0, TOTAL_NUMBER_OF_CARS)
	ENDWHILE
	RETURN(iRand)
ENDFUNC

FUNC INT GetRandomNoteNotAlreadyOnList()
	INT iNote = GET_RANDOM_INT_IN_RANGE(0, TOTAL_NUM_OF_NOTES)
	WHILE IsNoteIDOnList(iNote)
		iNote = GET_RANDOM_INT_IN_RANGE(0, TOTAL_NUM_OF_NOTES)
	ENDWHILE
	RETURN(iNote)
ENDFUNC


FUNC MODEL_NAMES GET_HIGH_PRIORITY_VEHICLE_MODEL()
	#IF IS_DEBUG_BUILD
		NET_DW_PRINT_STRING_INT("[GET_HIGH_PRIORITY_VEHICLE_MODEL] Called. g_sMPTunables.ihighpriorityimpexpveh... ", g_sMPTunables.ihighpriorityimpexpveh)
	#ENDIF
	
	MODEL_NAMES mHigh = DUMMY_MODEL_FOR_SCRIPT
	MODEL_NAMES mTemp
	INT iRandomCar 
	
	//-- See if the tunable is set to something we can use
	IF g_sMPTunables.ihighpriorityimpexpveh <> 0
		mTemp = INT_TO_ENUM(MODEL_NAMES, g_sMPTunables.ihighpriorityimpexpveh)
		IF IS_MODEL_IN_CDIMAGE(mTemp)
			#IF IS_DEBUG_BUILD NET_DW_PRINT("[GET_HIGH_PRIORITY_VEHICLE_MODEL] Model is in CD image!") #ENDIF
			
			IF IS_MODEL_A_VEHICLE(mTemp)
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRINGS("GET_HIGH_PRIORITY_VEHICLE_MODEL]  Model is a vehicle. Model is... ",  GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mTemp)) #ENDIF
				mHigh = mTemp
			ELSE
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[GET_HIGH_PRIORITY_VEHICLE_MODEL] Model is in CD image, but not a vehicle!") #ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD NET_DW_PRINT("[GET_HIGH_PRIORITY_VEHICLE_MODEL] Model NOT in CD image!") #ENDIF
		ENDIF
	ENDIF
	 
	IF mHigh = DUMMY_MODEL_FOR_SCRIPT // Couldn't use tunable
		iRandomCar = GetRandomCarOnWantedList(serverBD.CarList)
		#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[GET_HIGH_PRIORITY_VEHICLE_MODEL] Couldn't use tunable. iRandomCar = ",iRandomCar) #ENDIF
		
		mHigh = GET_MODEL_NAME_FROM_LIST_ID(iRandomCar)
		#IF IS_DEBUG_BUILD NET_DW_PRINT_STRINGS("GET_HIGH_PRIORITY_VEHICLE_MODEL]  Model is... ",  GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mHigh)) #ENDIF
	ENDIF
	
	RETURN mHigh
ENDFUNC
PROC CreateList(IMPORT_EXPORT_LIST_OF_CARS &inList, INT iNumberOfCarsOnList, BOOL bIsTutorial=FALSE)
	
	INT i
	
//	IMPORT_EXPORT_LIST_OF_CARS oldList
	
//	oldList = inList
	
	// clear list
	REPEAT MAX_NUM_OF_CARS_ON_IMPORT_EXPORT_LIST i
		inList.iCar[i]	= -1
		inList.bIsDelivered[i] = FALSE	
	ENDREPEAT
	
	// set list
	REPEAT MAX_NUM_OF_CARS_ON_IMPORT_EXPORT_LIST i
		IF (i < iNumberOfCarsOnList)
			inList.iCar[i]	= i// GetRandomCarNotAlreadyOnLists(inList, oldList)
			inList.bIsDelivered[i] = FALSE
			#IF IS_DEBUG_BUILD
				IF bWdUseDebug
					NET_DW_PRINT_STRINGS("this car added to list... ",  GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_MODEL_NAME_FROM_LIST_ID(inList.iCar[i]))) 
				ENDIF
			#ENDIF
		ELSE
			inList.iCar[i] = -1
			inList.bIsDelivered[i] = TRUE
		ENDIF						
	ENDREPEAT
	
	IF bIsTutorial ENDIF
	
ENDPROC

FUNC INT GET_CAR_ID_FOR_SUNDAY(INT index)
	/*
	CASE 0 RETURN   "IMPEX_CAR_0B"
	CASE 1 RETURN	"IMPEX_CAR_2B"
	CASE 2 RETURN	"IMPEX_CAR_3B"
	CASE 3 RETURN	"IMPEX_CAR_4B"
	CASE 4 RETURN	"IMPEX_CAR_5B"
	CASE 5 RETURN	"IMPEX_CAR_6B"
	CASE 6 RETURN	"IMPEX_CAR_7B"
	CASE 7 RETURN	"IMPEX_CAR_8B"
	CASE 8 RETURN	"IMPEX_CAR_9B"
	CASE 9 RETURN	"IMPEX_CAR_10B"
	CASE 10 RETURN	"IMPEX_CAR_11B"
	CASE 11 RETURN	"IMPEX_CAR_12B"
	CASE 12 RETURN	"IMPEX_CAR_13B"
	CASE 13 RETURN	"IMPEX_CAR_14B"
	CASE 14 RETURN	"IMPEX_CAR_15B"
	CASE 15 RETURN	"IMPEX_CAR_16B"
	CASE 16 RETURN	"IMPEX_CAR_17B"
	CASE 17 RETURN	"IMPEX_CAR_22B"
	CASE 18 RETURN	"IMPEX_CAR_19B"
	CASE 19 RETURN	"IMPEX_CAR_20B"
	CASE 20 RETURN	"IMPEX_CAR_21B"
	
	[IMPEX_CAR_0B]
Ubermacht Sentinel XS

[IMPEX_CAR_3B]
Vapid Dominator

[IMPEX_CAR_4B]
Benefactor Schafter

[IMPEX_CAR_5B]
Cheval Surge

[IMPEX_CAR_6B]
Ocelot Jackal

[IMPEX_CAR_8B]
Obey Tailgater

[IMPEX_CAR_9B]
Dundreary Landstalker

[IMPEX_CAR_10B]
Inverto Coquette

[IMPEX_CAR_11B]
Ocelot F620

[IMPEX_CAR_12B]
Fathom FQ 2

[IMPEX_CAR_13B]
Mammoth Patriot

[IMPEX_CAR_14B]
Emperor Habanero

[IMPEX_CAR_16B]
Schyster Fusilade

[IMPEX_CAR_19B]
Albany Buccaneer

[IMPEX_CAR_20B]
Western Daemon

[IMPEX_CAR_21B]
Western Bagger

[IMPEX_CAR_22B]
Bravado Gresley
	*/
	#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[GET_CAR_ID_FOR_SUNDAY] called with index = ", index) #ENDIF
	INT iCarId = -1
	
	SWITCH index
		CASE 0
			iCarId = GET_LIST_ID_FROM_MODEL(ROCOTO)
		BREAK
		
		CASE 1
			iCarId = GET_LIST_ID_FROM_MODEL(FELON2)	
		BREAK
		
		CASE 2
			iCarId = GET_LIST_ID_FROM_MODEL(SCHAFTER2)
		BREAK
		
		CASE 3
			iCarId = GET_LIST_ID_FROM_MODEL(CARBONIZZARE)
		BREAK
		
		CASE 4
			iCarId = GET_LIST_ID_FROM_MODEL(EXEMPLAR)
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[GET_CAR_ID_FOR_SUNDAY] Returning = ", iCarId) #ENDIF
	RETURN iCarId
ENDFUNC

FUNC INT GET_CAR_ID_FOR_MONDAY(INT index)

	#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[GET_CAR_ID_FOR_MONDAY] called with index = ", index) #ENDIF
	INT iCarId = -1
	
	SWITCH index
		CASE 0
			iCarId = GET_LIST_ID_FROM_MODEL(BANSHEE)
		BREAK
		
		CASE 1
			iCarId = GET_LIST_ID_FROM_MODEL(COQUETTE)
		BREAK
		
		CASE 2
			iCarId = GET_LIST_ID_FROM_MODEL(SENTINEL2)
		BREAK
		
		CASE 3
			iCarId = GET_LIST_ID_FROM_MODEL(DUBSTA)
		BREAK
		
		CASE 4
			iCarId = GET_LIST_ID_FROM_MODEL(INFERNUS)
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[GET_CAR_ID_FOR_MONDAY] Returning = ", iCarId) #ENDIF
	RETURN iCarId
ENDFUNC

FUNC INT GET_CAR_ID_FOR_TUESDAY(INT index)

	#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[GET_CAR_ID_FOR_TUESDAY] called with index = ", index) #ENDIF
	INT iCarId = -1
	
	SWITCH index
		CASE 0
			iCarId = GET_LIST_ID_FROM_MODEL(FELTZER2)
		BREAK
		
		CASE 1
			iCarId = GET_LIST_ID_FROM_MODEL(JACKAL)
		BREAK
		
		CASE 2
			iCarId = GET_LIST_ID_FROM_MODEL(F620)
		BREAK
		
		CASE 3
			iCarId = GET_LIST_ID_FROM_MODEL(SUPERD)
		BREAK
		
		CASE 4
			iCarId = GET_LIST_ID_FROM_MODEL(ROCOTO)
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[GET_CAR_ID_FOR_TUESDAY] Returning = ", iCarId) #ENDIF
	RETURN iCarId
ENDFUNC

FUNC INT GET_CAR_ID_FOR_WEDNESDAY(INT index)

	#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[GET_CAR_ID_FOR_WEDNESDAY] called with index = ", index) #ENDIF
	INT iCarId = -1
	
	SWITCH index
		CASE 0
			iCarId = GET_LIST_ID_FROM_MODEL(SCHAFTER2)
		BREAK
		
		CASE 1
			iCarId = GET_LIST_ID_FROM_MODEL(BULLET)
		BREAK
		
		CASE 2
			iCarId = GET_LIST_ID_FROM_MODEL(F620)
		BREAK
		
		CASE 3
			iCarId = GET_LIST_ID_FROM_MODEL(CARBONIZZARE)
		BREAK
		
		CASE 4
			iCarId = GET_LIST_ID_FROM_MODEL(COMET2)
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[GET_CAR_ID_FOR_WEDNESDAY] Returning = ", iCarId) #ENDIF
	RETURN iCarId
ENDFUNC

FUNC INT GET_CAR_ID_FOR_THURSDAY(INT index)

	#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[GET_CAR_ID_FOR_THURSDAY] called with index = ", index) #ENDIF
	INT iCarId = -1
	
	SWITCH index
		CASE 0
			iCarId = GET_LIST_ID_FROM_MODEL(SENTINEL2)
		BREAK
		
		CASE 1
			iCarId = GET_LIST_ID_FROM_MODEL(SCHWARZER)
		BREAK
		
		CASE 2
			iCarId = GET_LIST_ID_FROM_MODEL(SUPERD)
		BREAK
		
		CASE 3
			iCarId = GET_LIST_ID_FROM_MODEL(JACKAL)
		BREAK
		
		CASE 4
			iCarId = GET_LIST_ID_FROM_MODEL(FELTZER2)
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[GET_CAR_ID_FOR_THURSDAY] Returning = ", iCarId) #ENDIF
	RETURN iCarId
ENDFUNC

FUNC INT GET_CAR_ID_FOR_FRIDAY(INT index)

	#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[GET_CAR_ID_FOR_FRIDAY] called with index = ", index) #ENDIF
	INT iCarId = -1
	
	SWITCH index
		CASE 0
			iCarId = GET_LIST_ID_FROM_MODEL(ZION)
		BREAK
		
		CASE 1
			iCarId = GET_LIST_ID_FROM_MODEL(BANSHEE)
		BREAK
		
		CASE 2
			iCarId = GET_LIST_ID_FROM_MODEL(COMET2)
		BREAK
		
		CASE 3
			iCarId = GET_LIST_ID_FROM_MODEL(SURANO)
		BREAK
		
		CASE 4
			iCarId = GET_LIST_ID_FROM_MODEL(EXEMPLAR)
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[GET_CAR_ID_FOR_FRIDAY] Returning = ", iCarId) #ENDIF
	
	RETURN iCarId
ENDFUNC

FUNC INT GET_CAR_ID_FOR_SATURDAY(INT index)

	#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[GET_CAR_ID_FOR_SATURDAY] called with index = ", index) #ENDIF
	INT iCarId = -1
	
	SWITCH index
		CASE 0
			iCarId = GET_LIST_ID_FROM_MODEL(FELON2)
		BREAK
		
		CASE 1
			iCarId = GET_LIST_ID_FROM_MODEL(SERRANO)
		BREAK
		
		CASE 2
			iCarId = GET_LIST_ID_FROM_MODEL(BULLET)
		BREAK
		
		CASE 3
			iCarId = GET_LIST_ID_FROM_MODEL(INFERNUS)
		BREAK
		
		CASE 4
			iCarId = GET_LIST_ID_FROM_MODEL(COQUETTE)
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[GET_CAR_ID_FOR_SATURDAY] Returning = ", iCarId) #ENDIF
	
	RETURN iCarId
ENDFUNC
FUNC INT GET_CAR_ID_FOR_DAY(DAY_OF_WEEK dayList, INT index)
	
	#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[GET_CAR_ID_FOR_TODAY] Called, Day of week (0 = Sunday, 6 = Saturday) =  ", ENUM_TO_INT(dayList))#ENDIF
	
	INT iId = -1
	SWITCH dayList
		CASE SUNDAY
			
			iId = GET_CAR_ID_FOR_SUNDAY(index)
			
		BREAK
		
		CASE MONDAY
			
			iId = GET_CAR_ID_FOR_MONDAY(index)
		BREAK
		
		CASE TUESDAY
			
			iId = GET_CAR_ID_FOR_TUESDAY(index)
		BREAK
		
		CASE WEDNESDAY
			
			iId = GET_CAR_ID_FOR_WEDNESDAY(index)
		BREAK
		
		CASE THURSDAY
			
			iId = GET_CAR_ID_FOR_THURSDAY(index)
		BREAK
		
		CASE FRIDAY
			
			iId = GET_CAR_ID_FOR_FRIDAY(index)
		BREAK
		
		CASE SATURDAY
			
			iId = GET_CAR_ID_FOR_SATURDAY(index)
		BREAK
	
	ENDSWITCH
	
	RETURN iId
ENDFUNC
PROC GET_TODAYS_LIST_OF_CARS(IMPORT_EXPORT_LIST_OF_CARS &inList, INT iNumberOfCarsOnList)
	INT i
	
	DAY_OF_WEEK Today = GET_CLOCK_DAY_OF_WEEK()
	
	// clear list
	REPEAT MAX_NUM_OF_CARS_ON_IMPORT_EXPORT_LIST i
		inList.iCar[i]	= -1
		inList.bIsDelivered[i] = FALSE	
	ENDREPEAT
	
	// set list
	REPEAT MAX_NUM_OF_CARS_ON_IMPORT_EXPORT_LIST i
		IF (i < iNumberOfCarsOnList)
			inList.iCar[i]	=GET_CAR_ID_FOR_DAY(Today, i) //GetRandomCarNotAlreadyOnLists(inList, oldList)
			inList.bIsDelivered[i] = FALSE
			#IF IS_DEBUG_BUILD
			//	IF bWdUseDebug
					NET_DW_PRINT_STRINGS("this car added to list... ",  GET_MODEL_NAME_FOR_DEBUG(GET_MODEL_NAME_FROM_LIST_ID(inList.iCar[i]))) 
			//	ENDIF
			#ENDIF
		ELSE
			inList.iCar[i] = -1
			inList.bIsDelivered[i] = TRUE
		ENDIF						
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		NET_DW_PRINT_STRING_INT("SettingsavedGeneral.iMyLastImportExportListDay = ", ENUM_TO_INT(Today))
	#ENDIF
	g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMyLastImportExportListDay = ENUM_TO_INT(Today)
ENDPROC

PROC RESEND_LIST_OF_CARS_FOR_DAY(IMPORT_EXPORT_LIST_OF_CARS &inList, INT iNumberOfCarsOnList, INT iDay)
	INT i
	
	DAY_OF_WEEK dayResend = INT_TO_ENUM(DAY_OF_WEEK, iDay) 
	
	// clear list
	REPEAT MAX_NUM_OF_CARS_ON_IMPORT_EXPORT_LIST i
		inList.iCar[i]	= -1
		inList.bIsDelivered[i] = FALSE	
	ENDREPEAT
	
	// set list
	REPEAT MAX_NUM_OF_CARS_ON_IMPORT_EXPORT_LIST i
		IF (i < iNumberOfCarsOnList)
			inList.iCar[i]	=GET_CAR_ID_FOR_DAY(dayResend, i) //GetRandomCarNotAlreadyOnLists(inList, oldList)
			inList.bIsDelivered[i] = FALSE
			#IF IS_DEBUG_BUILD
			//	IF bWdUseDebug
					NET_DW_PRINT_STRINGS("this car added to list... ",  GET_MODEL_NAME_FOR_DEBUG(GET_MODEL_NAME_FROM_LIST_ID(inList.iCar[i]))) 
			//	ENDIF
			#ENDIF
		ELSE
			inList.iCar[i] = -1
			inList.bIsDelivered[i] = TRUE
		ENDIF						
	ENDREPEAT

ENDPROC

PROC DELETE_IMPORT_EXPORT_TEXT_MESSAGES()
	
	#IF IS_DEBUG_BUILD
		NET_DW_PRINT("[DELETE_IMPORT_EXPORT_TEXT_MESSAGES] called")
	#ENDIF
	UNLOCK_TEXT_MESSAGE_BY_LABEL("CELL_CLTEST1")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("CELL_CLTEST1")
	
	UNLOCK_TEXT_MESSAGE_BY_LABEL("CELL_CLTEST6")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("CELL_CLTEST6")
ENDPROC


PROC RemoveCarGens()
	INT i
	
	IF bCarGensAdded ENDIF
	REPEAT MAX_CAR_GENS i
	//	IF vehGenImportExport[i] <> NULL
			IF DOES_SCRIPT_VEHICLE_GENERATOR_EXIST(vehGenImportExport[i])
				DELETE_SCRIPT_VEHICLE_GENERATOR(vehGenImportExport[i])
			//	#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT(" [RemoveCarGens] Deleting car gen ", i) #ENDIF
			ELSE
			//	#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT(" [RemoveCarGens] This vehicle generator doesn't exist ", i) #ENDIF
			ENDIF
			vehGenImportExport[i] = NULL
	//	ENDIF
	ENDREPEAT
	bCarGensAdded = FALSE
	//ENDIF
ENDPROC

PROC ADD_CAR_GENS_FOR_MODEL(MODEL_NAMES mCar)
	#IF IS_DEBUG_BUILD
	//	NET_DW_PRINT_STRINGS("Adding car gens for model...",  GET_MODEL_NAME_FOR_DEBUG(mCar))
	#ENDIF
	
	IF IS_BIT_SET(serverbd.iServerBitSet, biS_RunHighPriorityVehicle) 
		IF mCar = serverBD.mHighPriority
			#IF IS_DEBUG_BUILD NET_DW_PRINT_STRINGS("Not adding car gens as model is HPV model (suppressed). Model is ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mCar)) #ENDIF
			EXIT
		ENDIF
	ENDIF
	
	SWITCH mCar
		CASE SENTINEL 
		CASE SERRANO //Cogcabrio 
		CASE DOMINATOR //JB700
		CASE SCHAFTER2 //Monroe
		CASE SURGE
			// Showroom (Albany)
			vehGenImportExport[0] = CREATE_SCRIPT_VEHICLE_GENERATOR(<< -54.2664, -1679.5488, 28.4414 >>, 228.2736, 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)
			
			//-- Showroom (Original)
			vehGenImportExport[1] =	CREATE_SCRIPT_VEHICLE_GENERATOR(<< -47.0703, -1115.4104, 25.4327 >>, 204.5124, 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)
			
			// Showroom (Country)
			vehGenImportExport[2] = CREATE_SCRIPT_VEHICLE_GENERATOR(<< 1227.0597, 2718.6780, 37.0051 >>, 359.6756, 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)
			
			// Facing the golf club entrance.
			vehGenImportExport[3] = CREATE_SCRIPT_VEHICLE_GENERATOR(<< -1388.7040, 44.2178, 52.6041 >>, 313.0655, 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)
			
			// Showroom (Overpass)
			vehGenImportExport[4] =	CREATE_SCRIPT_VEHICLE_GENERATOR(<< 430.9073, -1154.7450, 28.2919 >>, 267.6113, 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)
			
			// Horse Racing track car park.
			vehGenImportExport[5] = CREATE_SCRIPT_VEHICLE_GENERATOR(<< 1123.5134, 243.2258, 79.8556 >>, 237.5495 , 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)
			
			// Around the high end hair salon.
			vehGenImportExport[6] =	CREATE_SCRIPT_VEHICLE_GENERATOR(<< -841.3950, -138.6138, 36.5745 >>, 65.0431, 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)
				
			// Under the Arcadius Business Center, car park.
			vehGenImportExport[7] =	CREATE_SCRIPT_VEHICLE_GENERATOR(<< -150.0053, -618.0887, 31.4271 >>, 249.9070, 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)
			
			// Michaels mansion 
			vehGenImportExport[8] =	CREATE_SCRIPT_VEHICLE_GENERATOR(<< -827.7974, 170.3065, 69.2223 >>, 158.4295, 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)	
			
			// Tennis Courts
			vehGenImportExport[9] =	CREATE_SCRIPT_VEHICLE_GENERATOR(<< -1215.9534, -1675.1797, 2.9847 >>, 305.2072, 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)
			
			bCarGensAdded = TRUE
		BREAK
		
		
		CASE JACKAL
		CASE Ztype
		CASE TAILGATER
		CASE LANDSTALKER //EntityXF
		CASE PENUMBRA
			// Showroom (Overpass)
			vehGenImportExport[0] = CREATE_SCRIPT_VEHICLE_GENERATOR(<< 443.7590, -1164.0020, 28.2918 >>, 87.9477, 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)
			
			// Facing the golf club entrance.
			vehGenImportExport[1] =	CREATE_SCRIPT_VEHICLE_GENERATOR(<< -1392.0012, 80.7104, 52.8682 >>, 309.0381, 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)
			
			// Horse Racing track car park.
			vehGenImportExport[2] =	CREATE_SCRIPT_VEHICLE_GENERATOR(<< 1075.8711, 248.5613, 79.8556 >>, 294.4635, 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)
			
			// Pedestiran harbor area in a water edge car park.
			vehGenImportExport[3] = CREATE_SCRIPT_VEHICLE_GENERATOR(<< -815.4279, -1318.0275, 4.0003 >>, 170.2029, 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)
				
			// Tennis Courts
			vehGenImportExport[4] =	CREATE_SCRIPT_VEHICLE_GENERATOR(<< -1215.9534, -1675.1797, 2.9847 >>, 305.2072, 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)
		
			// Arena car park.
			vehGenImportExport[5] = CREATE_SCRIPT_VEHICLE_GENERATOR(<< -195.9823, -1960.0959, 26.6205 >>, 284.8585, 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)		
		
			// Movie set car park.
			vehGenImportExport[6] = CREATE_SCRIPT_VEHICLE_GENERATOR(<< -1101.6074, -445.1016, 34.6997 >>, 297.4066, 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)
			
			// High class housing car spaces in terriory 15.
			vehGenImportExport[7] = CREATE_SCRIPT_VEHICLE_GENERATOR(<< -3018.7593, 739.4393, 26.5749 >>, 101.9264, 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)
		
			// Villa car park.
			vehGenImportExport[8] = CREATE_SCRIPT_VEHICLE_GENERATOR(<< -1550.7963, 881.1625, 177.8996 >>, 223.4805, 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)
			
			// Observatory car park.
			vehGenImportExport[9] =	CREATE_SCRIPT_VEHICLE_GENERATOR(<< -420.3277, 1202.5525, 324.6421 >>, 50.8570, 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)
			
			bCarGensAdded = TRUE
		BREAK
		
		
		CASE F620
		CASE FQ2
		CASE PATRIOT
		CASE HABANERO // Vacca
		CASE PRAIRIE // StingerGT
		CASE GRESLEY
			
			// Von Crastenburg's car park (around the corner for the star covered street).
			vehGenImportExport[0] =	CREATE_SCRIPT_VEHICLE_GENERATOR(<< 464.1028, 226.5939, 102.1875 >>, 247.3152, 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)
		
			// Army base (Secret Alien lab)
			vehGenImportExport[1] =	CREATE_SCRIPT_VEHICLE_GENERATOR(<< -2437.3716, 3377.2168, 31.9214 >>, 29.7691, 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)
			
			// Pier fairground car park.
			vehGenImportExport[2] =	CREATE_SCRIPT_VEHICLE_GENERATOR(<< -1639.8566, -906.3636, 7.7037 >>, 139.4463, 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)
			
			// High class housing car spaces in terriory 15.
			vehGenImportExport[3] =	CREATE_SCRIPT_VEHICLE_GENERATOR(<< -2981.2446, 612.5886, 19.1798 >>, 105.0653, 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)

			// Villa car park.
			vehGenImportExport[4] =	CREATE_SCRIPT_VEHICLE_GENERATOR(<< -1542.3317, 887.9052, 180.4910 >>, 19.9514 , 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)
		
			// Observatory car park.
			vehGenImportExport[5] =	CREATE_SCRIPT_VEHICLE_GENERATOR(<< -420.3277, 1202.5525, 324.6421 >>, 50.8570, 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)
			
			// City centre, Didersachs car park
			vehGenImportExport[6] =	CREATE_SCRIPT_VEHICLE_GENERATOR(<< -331.3999, -935.0096, 30.0798 >>, 249.9035, 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)
			
			// Top of Airport car park
			vehGenImportExport[7] =	CREATE_SCRIPT_VEHICLE_GENERATOR(<< -976.5854, -2573.4724, 35.6066 >>, 240.8510, 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)
			
			// Vinewood Eclipse driveways
			vehGenImportExport[8] =	CREATE_SCRIPT_VEHICLE_GENERATOR(<< -350.7184, 427.0929, 109.5371 >>, 17.3499, 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)
			
			// Arena car park.
			vehGenImportExport[9] =	CREATE_SCRIPT_VEHICLE_GENERATOR(<< -208.5685, -2077.8867, 26.6204 >>, 47.4426, 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)
			
			bCarGensAdded = TRUE
		BREAK
		
		CASE FUSILADE 
		CASE BJXL
		CASE BUCCANEER
		CASE DAEMON 
		CASE BAGGER
			// Pedestiran harbor area in a water edge car park.
			vehGenImportExport[0] = CREATE_SCRIPT_VEHICLE_GENERATOR(<< -815.4279, -1318.0275, 4.0003 >>, 170.2029, 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)
				
			// Tennis Courts
			vehGenImportExport[1] =	CREATE_SCRIPT_VEHICLE_GENERATOR(<< -1215.9534, -1675.1797, 2.9847 >>, 305.2072, 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)
			
			// Arena car park.
			vehGenImportExport[2] = CREATE_SCRIPT_VEHICLE_GENERATOR(<< -195.9823, -1960.0959, 26.6205 >>, 284.8585, 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)		
			
			// Around the high end hair salon.
			vehGenImportExport[3] =	CREATE_SCRIPT_VEHICLE_GENERATOR(<< -826.9526, -236.9994, 36.0555 >>, 209.0553, 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)
		
			// Michaels mansion 
			vehGenImportExport[4] =	CREATE_SCRIPT_VEHICLE_GENERATOR(<< -819.5961, 183.2204, 71.0876 >>, 115.4816, 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)
			
			// Under the Arcadius Business Center, car park.
			vehGenImportExport[5] =	CREATE_SCRIPT_VEHICLE_GENERATOR(<< -141.4775, -594.4868, 31.4271 >>, 69.7535, 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)
			
			// Showroom (Overpass)
			vehGenImportExport[6] = CREATE_SCRIPT_VEHICLE_GENERATOR(<< 443.7590, -1164.0020, 28.2918 >>, 87.9477, 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)
			
			// Facing the golf club entrance.
			vehGenImportExport[7] =	CREATE_SCRIPT_VEHICLE_GENERATOR(<< -1392.0012, 80.7104, 52.8682 >>, 309.0381, 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)
			
			// Horse Racing track car park.
			vehGenImportExport[8] =	CREATE_SCRIPT_VEHICLE_GENERATOR(<< 1075.8711, 248.5613, 79.8556 >>, 294.4635, 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)
			
			// Showroom (Country)
			vehGenImportExport[9] =	CREATE_SCRIPT_VEHICLE_GENERATOR(<< 1218.1803, 2708.2981, 37.0054 >>, 359.1518, 5.0,3, mCar, -1, -1, -1, -1, TRUE, 0, 0,FALSE)
			
			bCarGensAdded = TRUE
		BREAK
		
		
	ENDSWITCH
ENDPROC


PROC ServerCreateList()
	
	INT i
	//INT iNumberOfCarsOnList = GET_RANDOM_INT_IN_RANGE(MIN_NUM_OF_CARS_ON_LIST, MAX_NUM_OF_CARS_ON_IMPORT_EXPORT_LIST+1) 
	
	//CreateList(serverbd.CarList, iNumberOfCarsOnList)
	
	#IF IS_DEBUG_BUILD
		NET_DW_PRINT("[ServerCreateList] Called")
	#ENDIF
	GET_TODAYS_LIST_OF_CARS(serverbd.CarList, MAX_NUM_OF_CARS_ON_IMPORT_EXPORT_LIST )
	
	// set list of notes
	REPEAT MAX_NUM_OF_NOTES i
		serverBD.Notes[i] = GetRandomNoteNotAlreadyOnList()		
	ENDREPEAT
	
	// now score off a random one or two
/*	INT iNumberToScoreOff = GET_RANDOM_INT_IN_RANGE(1, 3)
	INT iRand
	REPEAT iNumberToScoreOff i
		iRand = GET_RANDOM_INT_IN_RANGE(0, iNumberOfCarsOnList)	
		serverbd.CarList.bIsDelivered[iRand] = TRUE
	ENDREPEAT
	*/
	serverBD.bNewList = TRUE
	
ENDPROC

PROC SET_MODEL_AS_DELIVERED(MODEL_NAMES ModelNames)
	IF IS_MODEL_ON_WANTED_LIST(ThisCarList, ModelNames)
		INT i
		INT iListID = GET_LIST_ID_FROM_MODEL(ModelNames)
		REPEAT MAX_NUM_OF_CARS_ON_IMPORT_EXPORT_LIST i
			IF NOT (ThisCarList.iCar[i] = -1)
				IF NOT (ThisCarList.bIsDelivered[i])
					IF (ThisCarList.iCar[i] = iListID)
						ThisCarList.bIsDelivered[i] = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT		
	ENDIF
ENDPROC	

PROC INIT_OBJECTIVE_STATS()
	#IF IS_DEBUG_BUILD
		NET_DW_PRINT("[INIT_OBJECTIVE_STATS] Called...")
	#ENDIF
	
	INT iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_IMP_EXP) 
	IF IS_BIT_SET(iStatInt, BI_IMPEXP_OBJECTIVE_ONCE)
		SET_BIT(iLocalStatBitset, biStat_DoneObjectiveOnce)

		#IF IS_DEBUG_BUILD NET_DW_PRINT("[INIT_OBJECTIVE_STATS] BI_IMPEXP_OBJECTIVE_ONCE is set") #ENDIF
	ENDIF
	
	IF IS_BIT_SET(iStatInt, BI_IMPEXP_OBJECTIVE_TWICE)
		SET_BIT(iLocalStatBitset, biStat_DoneObjectiveTwice)

		#IF IS_DEBUG_BUILD NET_DW_PRINT("[INIT_OBJECTIVE_STATS] BI_IMPEXP_OBJECTIVE_ONCE is set") #ENDIF
	ENDIF
	
	IF IS_BIT_SET(iStatInt, BI_IMPEXP_OBJECTIVE_THREE)
		SET_BIT(iLocalStatBitset, biStat_DoneObjectiveThree)
		SET_BIT(iLocalStatBitset, biStat_DoneAllObjectiveText)
		#IF IS_DEBUG_BUILD NET_DW_PRINT("[INIT_OBJECTIVE_STATS] BI_IMPEXP_OBJECTIVE_THREE is set") #ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_DW_PRINT("[INIT_OBJECTIVE_STATS] Done...")
	#ENDIF
ENDPROC

/// PURPOSE:
///    Do necessary pre game start ini.
/// RETURNS: FALSE if the script fails to receive its initial network broadcast.
FUNC BOOL PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission) , missionScriptArgs)
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	//RESERVE_NETWORK_MISSION_VEHICLES(1) //-- For high priority vehicles
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	// KGM: Wait for the first network broadcast before moving on - call this as the last instruction in pre-game
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		RETURN FALSE
	ENDIF
	
	MPGlobalsAmbience.bPlayerOnImportExportHpv = FALSE
	MPGlobalsAmbience.gCarModelOnList = DUMMY_MODEL_FOR_SCRIPT
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
	//	serverBD.iTelemetryId = PLAYSTATS_CREATE_MATCH_HISTORY_ID()
	ENDIF
	
	//-- Safety check for when re-launching. Don't want to be on HPV
	IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_IMPORTEXPORT)
		#IF IS_DEBUG_BUILD
			NET_DW_PRINT("Import export launching and player already set on HPV!")
		#ENDIF
		SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_IMPORTEXPORT, FALSE)
	ENDIF
	
	INIT_OBJECTIVE_STATS()
	
	#IF IS_DEBUG_BUILD
		IF HAVE_I_DELIVERED_A_VEHICLE_TODAY()
			NET_DW_PRINT("Launching import export and I have delivered a car todaye")
		ELSE
			NET_DW_PRINT("Launching import export and I have NOT delivered a car today")
		ENDIF
		
		IF HAVE_I_RECEIVED_A_CAR_LIST_TODAY()
			NET_DW_PRINT("Launching import export and I have received a list todaye")
		ELSE
			NET_DW_PRINT("Launching import export and I have NOT received a list today")
		ENDIF
		
	#ENDIF
	
	
	
	RETURN TRUE
ENDFUNC


#IF IS_DEBUG_BUILD
PROC CREATE_WIDGETS()

	INT i
	TEXT_LABEL_63 str

	START_WIDGET_GROUP("Import Export")
		//Profiling widgets
		#IF SCRIPT_PROFILER_ACTIVE
		CREATE_SCRIPT_PROFILER_WIDGET()
		#ENDIF		
		//ADD_WIDGET_BOOL("Allow Unwanted in Garage", bAllowUnwantedInGarage)
		ADD_WIDGET_BOOL("Respray car", bWdSetResprayed)
		ADD_WIDGET_BOOL("Warp to garage", bWdWarpToGarage)
		ADD_WIDGET_BOOL("Print wanted models", bWdPrintListModels)
		ADD_WIDGET_BOOL("Force objective", bWdForceObjective)
		ADD_WIDGET_BOOL("Clear objective stat", bWdClearObjectiveStat)
		ADD_WIDGET_BOOL("Clear wanted", bWdClearWanted)
		ADD_WIDGET_BOOL("Delete text msg", bWdDeleteTextMessages)
		//ADD_WIDGET_BOOL("bActivateInFlow", bActivateInFlow)
		ADD_WIDGET_BOOL("Launch high priority", bWdLaunchHighPriority)
		ADD_WIDGET_BOOL("Run stolen / sprayed checks", bWdSprayStolenCheck)
		ADD_WIDGET_BOOL("Stolen?", bWdStolen)
		ADD_WIDGET_BOOL("Sprayed?", bWdSprayed)
		ADD_WIDGET_BOOL("Set stolen", bWdSetAsStolen)
		ADD_WIDGET_BOOL("Clear stolen", bWdClearStolen)
		ADD_WIDGET_BOOL("Crew emblem?", bWdCrewEmblem)
		ADD_WIDGET_BOOL("Check for crew", bWdCheckForCrewEmblem)
		ADD_WIDGET_INT_SLIDER("Min del time", TIME_BETWEEN_DELIVERIES, 1, 100000, 1)
		ADD_WIDGET_BOOL("Delivered today?",bWdDeliveredToday) 
		ADD_WIDGET_BOOL("received List today?",bWdReceivedToday) 
		ADD_WIDGET_BOOL("CLear delivered today", bWdClearLastDeliveredTime)
		
		START_WIDGET_GROUP("Debug text")
			ADD_WIDGET_BOOL("Show debug text", bWdUseDebug)
			ADD_WIDGET_BOOL("Staggered debug (server)", bWdDoStaggeredDebug)
			ADD_WIDGET_BOOL("HPV Debug", bWdDoHpvDebug)
			ADD_WIDGET_BOOL("Timed debug", bTimedDebug)
			ADD_WIDGET_BOOL("Sec Van Launching Debug", g_SecVanLaunchingDebugPrints)
		STOP_WIDGET_GROUP()
		
		ADD_WIDGET_INT_SLIDER("Max Redeliver time", iMAX_REDELIVER_TIME,0, 200000,1)
		ADD_WIDGET_BOOL("Not recent Del", bWdNotRecDel)
		ADD_WIDGET_BOOL("Set all cars del'd", bWdSetAllCarsDelivered)
	//	ADD_WIDGET_INT_SLIDER("iImportExportState", iImportExportState, -1, 999, 1)
	//	ADD_WIDGET_INT_SLIDER("iClientBoardState", iClientBoardState, -1, 999, 1)
	//	ADD_WIDGET_BOOL("bIgnoreRenderTarget", bIgnoreRenderTarget)
	//	ADD_WIDGET_VECTOR_SLIDER("Velocity", vWdVel, -1000.0, 1000.0, 0.1)
	//	ADD_WIDGET_BOOL("Show Vel", bWdSHowVel)
	/*	
		ADD_WIDGET_VECTOR_SLIDER("vBoardPos", vBoardPos, -9999.9, 9999.9, 0.01)
		ADD_WIDGET_VECTOR_SLIDER("vBoardRot", vBoardRot, -360.0, 360.0, 0.01)
		ADD_WIDGET_VECTOR_SLIDER("vBoardSize", vBoardSize, -360.0, 360.0, 0.01)
		
		ADD_WIDGET_VECTOR_SLIDER("vNotePos[0]", vNotePos[0], -1.0, 1.0, 0.01)
		ADD_WIDGET_VECTOR_SLIDER("vNotePos[1]", vNotePos[1], -1.0, 1.0, 0.01)
		ADD_WIDGET_VECTOR_SLIDER("vNotePos[2]", vNotePos[2], -1.0, 1.0, 0.01)
		ADD_WIDGET_VECTOR_SLIDER("vNotePos[3]", vNotePos[3], -1.0, 1.0, 0.01)
		
		ADD_WIDGET_INT_SLIDER("iColourR", iColourR, 0, 255, 1)
		ADD_WIDGET_INT_SLIDER("iColourG", iColourG, 0, 255, 1)
		ADD_WIDGET_INT_SLIDER("iColourB", iColourB, 0, 255, 1)
		ADD_WIDGET_INT_SLIDER("iBrightness", iBrightness, 0, 255, 1)
		
		ADD_WIDGET_FLOAT_SLIDER("START_LIST_X", START_LIST_X, 0.0, 1.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("START_LIST_Y", START_LIST_Y, 0.0, 1.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("LIST_SPACER", LIST_SPACER, 0.0, 1.0, 0.001) */
		
		ADD_WIDGET_FLOAT_SLIDER("fCash", fCash, 0.0, 9999999.0, 1.0)
		ADD_WIDGET_BOOL("bIAmDriver", bIAmDriver)
		
		ADD_WIDGET_INT_SLIDER("iLocalUpdateID", iLocalUpdateID, -1, HIGHEST_INT, 1)
		
		ADD_WIDGET_BOOL("bStreamingInCarModel", bStreamingInCarModel)
		
		START_WIDGET_GROUP("HPV")
			ADD_WIDGET_BOOL("Do hpv debug", bWdDoHpvDebug)
			ADD_WIDGET_INT_SLIDER("hpvProg", iWdMyHpvProg, -1, 100, 1)
			ADD_WIDGET_BOOL("Server bit set?", bWdServerBitSet)
			ADD_WIDGET_INT_SLIDER("iHpvStuckBitset", mpglobalsambience.iHpvStuckBitset, 0, 1, 1)
			ADD_WIDGET_INT_READ_ONLY("iWarpWhenStuckInModshopProg", serverBD.iWarpWhenStuckInModshopProg)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("serverBD")
			ADD_WIDGET_INT_READ_ONLY("iHostPlayerInt", serverBD.iHostPlayerInt)
			ADD_WIDGET_INT_READ_ONLY("Server hpv prog", iWdServHpvProg)
		//	ADD_WIDGET_BOOL("bBoardIsOn", serverBD.bBoardIsOn)
			START_WIDGET_GROUP("Car List")
				REPEAT MAX_NUM_OF_CARS_ON_IMPORT_EXPORT_LIST i
					str = "CarList.iCar["
					str += i
					str += "]"
					ADD_WIDGET_INT_SLIDER(str, serverBD.CarList.iCar[i], -1, 30, 1)
					str = "CarList.bIsDelivered["
					str += i
					str += "]"
					ADD_WIDGET_BOOL(str, serverBD.CarList.bIsDelivered[i])
				ENDREPEAT
			STOP_WIDGET_GROUP()
			ADD_WIDGET_INT_SLIDER("iCarListState", serverBD.iCarListState, -1, 999, 1)
			ADD_WIDGET_INT_SLIDER("iListIDToStreamIn", serverBD.iListIDToStreamIn, -1, 99, 1)
			ADD_WIDGET_INT_SLIDER("iUpdateID", serverBD.iUpdateID, -1, HIGHEST_INT, 1)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("ThisCarList")
			REPEAT MAX_NUM_OF_CARS_ON_IMPORT_EXPORT_LIST i
				str = "ThisCarList.iCar["
				str += i
				str += "]"
				ADD_WIDGET_INT_SLIDER(str, ThisCarList.iCar[i], -1, 30, 1)
				str = "ThisCarList.bIsDelivered["
				str += i
				str += "]"
				ADD_WIDGET_BOOL(str, ThisCarList.bIsDelivered[i])
			ENDREPEAT
		STOP_WIDGET_GROUP()		
		
		START_WIDGET_GROUP("PlayerBD")
			REPEAT COUNT_OF(PlayerBD) i
				str = "PlayerBD["
				str += i
				str += "].iVehicleListID"
				ADD_WIDGET_INT_SLIDER(str, PlayerBD[i].iVehicleListID, -1, 30, 1)
				str = "PlayerBD["
				str += i
				str += "].iDeliveryState"
				ADD_WIDGET_INT_SLIDER(str, PlayerBD[i].iDeliveryState, -1, 30, 1)				
				str = "PlayerBD["
				str += i
				str += "].hpvProg"
				ADD_WIDGET_INT_SLIDER(str, iWdHpvProg, 0, 10, 1)	
			ENDREPEAT
		STOP_WIDGET_GROUP()
		/*
		START_WIDGET_GROUP("mechanic")
			ADD_WIDGET_INT_SLIDER("iMechanicState", iMechanicState, -1, 999, 1)	
			//ADD_WIDGET_INT_SLIDER("iMechanicTaskTime", iMechanicTaskTime, -1, HIGHEST_INT, 1)	
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Viewing Cam")
			//ADD_WIDGET_BOOL("bViewCamCreated", bViewCamCreated)
			//ADD_WIDGET_INT_SLIDER("iViewingCamTimer", iViewingCamTimer, 0, HIGHEST_INT, 1)
		STOP_WIDGET_GROUP()
		*/
		START_WIDGET_GROUP("Globals")
			ADD_WIDGET_INT_SLIDER("iNumberOfImportExportWantedCars", mpglobalsambience.iNumberOfImportExportWantedCars, -1, MAX_NUM_OF_CARS_ON_IMPORT_EXPORT_LIST, 1)
			REPEAT MAX_NUM_OF_CARS_ON_IMPORT_EXPORT_LIST i
				ADD_WIDGET_STRING(mpglobalsambience.ImportExportWantedCars[i])
			ENDREPEAT
		STOP_WIDGET_GROUP()
		
		
		
	STOP_WIDGET_GROUP()
	
ENDPROC

PROC UPDATE_WIDGETS()
	INT i
	IF bWdSetResprayed
		bWdSetResprayed = FALSE
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				SET_VEHICLE_IS_SPRAYED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			ENDIF
		ENDIF
	ENDIF
	
	IF bWdWarpToGarage
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF NET_WARP_TO_COORD( <<1205.2668, -3094.2190, 4.9018>>, 178.5499, TRUE)
				bWdWarpToGarage = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF bWdClearObjectiveStat
		bWdClearObjectiveStat = FALSE
		INT iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_IMP_EXP) 
		CLEAR_BIT(iStatInt, BI_IMPEXP_OBJECTIVE_ONCE)
		CLEAR_BIT(iStatInt, BI_IMPEXP_OBJECTIVE_TWICE)
		CLEAR_BIT(iStatInt, BI_IMPEXP_OBJECTIVE_THREE)
		CLEAR_BIT(iLocalStatBitset, biStat_DoneObjectiveOnce)
		CLEAR_BIT(iLocalStatBitset, biStat_DoneObjectiveTwice)
		CLEAR_BIT(iLocalStatBitset, biStat_DoneObjectiveThree)
		CLEAR_BIT(iLocalStatBitset, biStat_DoneAllObjectiveText)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_FM_IMP_EXP, iStatInt) 
	ENDIF
	IF bWdLaunchHighPriority
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			SET_BIT(serverbd.iServerBitSet, biS_RunHighPriorityVehicle)
			bWdLaunchHighPriority = FALSE
		ELSE
			NET_DW_PRINT("bWdLaunchHighPriority can only be run from host!")
		ENDIF
	ENDIF
	
	IF bWdSHowVel
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	//		vWdVel = GET_ENTITY_VELOCITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
		ENDIF
	ENDIF
	IF bWdSetAllCarsDelivered
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			SET_ALL_CARS_DELIVERED()
			bWdSetAllCarsDelivered = FALSE
		ELSE
			NET_DW_PRINT("bWdSetAllCarsDelivered can only be run from host!")
		ENDIF
	ENDIF
	iWdHpvProg = ENUM_TO_INT(playerbd[PARTICIPANT_ID_TO_INT()].hpvProg)
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		iWdServHpvProg = ENUM_TO_INT(serverbd.hpvServerProg)
	ENDIF
	
	IF bWdPrintListModels
		bWdPrintListModels = FALSE 
		REPEAT 	MAX_NUM_OF_CARS_ON_IMPORT_EXPORT_LIST i	
			IF NOT (ThisCarList.iCar[i] = -1)
				PRINTLN("Model on list...",  GET_MODEL_NAME_FOR_DEBUG(GET_MODEL_NAME_FROM_LIST_ID(ThisCarList.iCar[i])))
			ENDIF
		ENDREPEAT
	ENDIF
	
	VEHICLE_INDEX vehStolen
	IF bWdSprayStolenCheck
		vehStolen = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		bWdSprayed = GET_IS_VEHICLE_SPRAYED(vehStolen)
		IF bWdSprayed
			PRINTLN("[dsw] Sprayed....")
		ENDIF
		bWdStolen = IS_VEHICLE_STOLEN(vehStolen)
		IF bWdStolen
			PRINTLN("[dsw] Stolen....")
		ENDIF
	ENDIF
	
	IF bWdSetAsStolen
		bWdSetAsStolen = FALSE
		vehStolen = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		SET_VEHICLE_IS_STOLEN(vehStolen, TRUE)
	ENDIF
	
	IF bWdClearStolen
		bWdClearStolen = FALSE
		vehStolen = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		SET_VEHICLE_IS_STOLEN(vehStolen, FALSE)
	ENDIF
	
	IF bWdCheckForCrewEmblem
		vehStolen = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		bWdCrewEmblem = DOES_VEHICLE_HAVE_CREW_EMBLEM(vehStolen)
	ENDIF
	IF bWdClearLastDeliveredTime 
		g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iLastImportExportDelTime = 0
		g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iLastImportExportListTime = 0
		bWdClearLastDeliveredTime = FALSE
	ENDIF
	
	bWdDeliveredToday = HAVE_I_DELIVERED_A_VEHICLE_TODAY()
	bWdReceivedToday = HAVE_I_RECEIVED_A_CAR_LIST_TODAY()
	
	IF bWdClearWanted
		bWdClearWanted = FALSE
		CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
	ENDIF
	
	IF bWdDoHpvDebug
		iWdMyHpvProg = ENUM_TO_INT(playerbd[PARTICIPANT_ID_TO_INT()].hpvProg)
		bWdServerBitSet = IS_BIT_SET(serverbd.iServerBitSet, biS_RunHighPriorityVehicle)
	ENDIF
	
	IF bWdDeleteTextMessages
		bWdDeleteTextMessages = FALSE
		DELETE_IMPORT_EXPORT_TEXT_MESSAGES()	
	ENDIF
ENDPROC	
#ENDIF



PROC SET_CAR_MOD_BLIP_LONG_RANGE(SHOP_NAME_ENUM shopCarMod, BOOL bSetAsLongRange = TRUE)

//	BOOL bSetAsShortRange = NOT(bSetAsLongRange)
//	IF bSetAsShortRange ENDIF
	SWITCH shopCarMod
		CASE CARMOD_SHOP_01_AP
			SET_SHOP_BLIP_LONG_RANGE(CARMOD_SHOP_01_AP  ,bSetAsLongRange)
		/*	IF DOES_BLIP_EXIST(g_sShopSettings.blipID[CARMOD_SHOP_01_AP])
				
			ELSE
				#IF IS_DEBUG_BUILD NET_DW_PRINT("SET_CAR_MOD_BLIP_LONG_RANGE g_sShopSettings.blipID[CARMOD_SHOP_01_AP] doesn't exist!") #ENDIF
			ENDIF */
		BREAK

		CASE CARMOD_SHOP_05_ID2
			SET_SHOP_BLIP_LONG_RANGE(CARMOD_SHOP_05_ID2  ,bSetAsLongRange)
		BREAK
		
		CASE CARMOD_SHOP_06_BT1			// Car Mod - AMB3 (v_carmod)
			SET_SHOP_BLIP_LONG_RANGE(CARMOD_SHOP_06_BT1,bSetAsLongRange )
		BREAK
		
		CASE CARMOD_SHOP_07_CS1			// Car Mod - AMB4 (v_carmod3)
			SET_SHOP_BLIP_LONG_RANGE(CARMOD_SHOP_07_CS1,bSetAsLongRange)
		BREAK
		
		CASE CARMOD_SHOP_08_CS6			// Car Mod - AMB5 (v_carmod3)
			SET_SHOP_BLIP_LONG_RANGE(CARMOD_SHOP_08_CS6,bSetAsLongRange)
		BREAK
		
		CASE CARMOD_SHOP_SUPERMOD		// Car Mod - AMB6 (lr_supermod_int)
			SET_SHOP_BLIP_LONG_RANGE(CARMOD_SHOP_SUPERMOD,bSetAsLongRange)
		BREAK
		
		DEFAULT
			NET_SCRIPT_ASSERT("SET_CAR_MOD_BLIP_LONG_RANGE Didn't get carmod blip!")
		BREAK
	ENDSWITCH
ENDPROC

PROC CLEANUP_MECHANIC_PED()
	ENTITY_INDEX ent
	IF DOES_ENTITY_EXIST(pedMechanic)
		SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_XMECH_02)
		
		ent = pedMechanic
		DELETE_ENTITY(ent)
		#IF IS_DEBUG_BUILD
			NET_DW_PRINT("Cleaned up mechanic")
		#ENDIF

	ENDIF
ENDPROC


/// PURPOSE:
///    Handles the cleanup of the script
PROC CLEANUP_SCRIPT()
	
	
	#IF IS_DEBUG_BUILD
		NET_DW_PRINT("Running cleanup")
	#ENDIF
	// cleanup floating help
//	IF IS_THIS_FLOATING_HELP_BEING_DISPLAYED("IMPEX_HELP_1")
//		CLEAR_THIS_FLOATING_HELP("IMPEX_HELP_1")
//	ENDIF	
//	IF IS_THIS_FLOATING_HELP_BEING_DISPLAYED("IMPEX_HELP_2_FM")
//		CLEAR_THIS_FLOATING_HELP("IMPEX_HELP_2_FM")
//	ENDIF
//	IF IS_THIS_FLOATING_HELP_BEING_DISPLAYED("IMPEX_HELP_2_L")
//		CLEAR_THIS_FLOATING_HELP("IMPEX_HELP_2_L")
//	ENDIF
//	
//	#IF IS_DEBUG_BUILD
//		g_bLaunchImportExportHpv = FALSE
//	#ENDIF
	
	IF shopClosestCarMod <> EMPTY_SHOP
		SET_CAR_MOD_BLIP_LONG_RANGE(shopClosestCarMod, FALSE)
		#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("Cleanup Clearing SET_SHOP_BLIP_LONG_RANGE for shop ", ENUM_TO_INT(shopClosestCarMod)) #ENDIF
		
	ENDIF
	RELEASE_CONTEXT_INTENTION(iContextID)
	IF IS_BIT_SET(iLocalImpExpBitSet, biL_SentInitialText)
		REMOVE_ACTIVITY_FROM_DISPLAY_LIST(<<0,0,0>>,GET_ACTIVITY_NAME_FROM_FMMC_TYPE(FMMC_TYPE_IMPORT_EXPORT),TRUE)
	ENDIF
	IF NETWORK_IS_GAME_IN_PROGRESS()	
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			IF NETWORK_ENTITY_AREA_DOES_EXIST(serverBD.iHighPriorityCreateAreaId)
				NETWORK_REMOVE_ENTITY_AREA(serverBD.iHighPriorityCreateAreaId)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("Cleanup calling NETWORK_REMOVE_ENTITY_AREA") #ENDIF
			ENDIF
			serverBD.iHighPriorityCreateAreaId = -1
		ENDIF
	ENDIF
	 

	
	
	RemoveCarGens()
	
	MPGlobalsAmbience.bInitFmImportExportLaunch = FALSE
	MPGlobalsAmbience.time_LaunchImportExport = NULL
	MPGlobalsAmbience.bPlayerOnImportExportHpv = FALSE
	mpglobalsambience.bSendImportExportTextMessage = FALSE
	
	IF IS_BIT_SET(iLocalImpExpBitSet, biL_SetLocalPlayerOnAmb)
		#IF IS_DEBUG_BUILD
			NET_DW_PRINT("Cleanup... about to remove from ambient mission")
		#ENDIF
		SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_IMPORTEXPORT, FALSE)
	ENDIF
	
	CLEANUP_MECHANIC_PED()
	
	IF DOES_BLIP_EXIST(g_blipSimeonGarage)
		REMOVE_BLIP(g_blipSimeonGarage)
	ENDIF
	
	TURN_OFF_PASSIVE_MODE_OPTION(FALSE)
	
	IF DOES_BLIP_EXIST(g_blipHighPriorityVeh)
		REMOVE_BLIP(g_blipHighPriorityVeh)
	ENDIF
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF (bGarageStateOpen)
			REQUEST_GARAGE_OPEN(MP_GAR_SIMEON,FALSE)
			DISABLE_WARP_OUT_OF_LOCKED_GARAGE(FALSE)
		ENDIF
		IF (bWarpDisabled)
			DISABLE_WARP_OUT_OF_LOCKED_GARAGE(FALSE)
		ENDIF
	ENDIF
	TERMINATE_THIS_THREAD()
ENDPROC


FUNC BOOL IsCarInContactGarage(ENTITY_INDEX CarID, BOOL bIgnoreDoorBox = FALSE)
	IF SCRIPT_CHECK_IS_ENTITY_FULLY_IN_GARAGE(CarID, MP_GAR_SIMEON, SyncDelayTimerGarage, 0.0, 500, TRUE, bIgnoreDoorBox)
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC



FUNC BOOL IsPlayerGoodToDeliverCars(BOOL bIgnoreWantedCheck=FALSE, BOOL bIgnoreControlCheck=FALSE)
	/*
	#IF IS_DEBUG_BUILD
		BOOL bTempDoDebug
		IF bWdUseDebug
		OR PlayerBD[PARTICIPANT_ID_TO_INT()].iDeliveryState > PLAYER_DELIVERY_STATE_WAITING_FOR_CAR
			bTempDoDebug = TRUE
		ENDIF
	#ENDIF
	*/
	BOOL bGoodToDeliver = TRUE
	
	IF IS_PED_INJURED(PLAYER_PED_ID())
		bGoodToDeliver = FALSE
	ENDIF
	
	IF NOT bIgnoreControlCheck
		IF bGoodToDeliver
			IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
				bGoodToDeliver = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bIgnoreWantedCheck
		IF bGoodToDeliver
			IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				bGoodToDeliver = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF bGoodToDeliver
		IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
			bGoodToDeliver = FALSE
		ENDIF
	ENDIF
	
	IF bGoodToDeliver
		IF IS_FM_MISSION_LAUNCH_IN_PROGRESS()
			bGoodToDeliver = FALSE
		ENDIF
	ENDIF
	
	
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, FALSE)	
		bGoodToDeliver = FALSE
	ENDIF
	
	IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_GANGHIDEOUT)
		bGoodToDeliver = FALSE
	ENDIF
	
	IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID(), TRUE)
		bGoodToDeliver = FALSE
	ENDIF
	
	RETURN(bGoodToDeliver)
	
	/*
	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
	AND NOT IS_FM_MISSION_LAUNCH_IN_PROGRESS()
	AND ((NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)) OR (bIgnoreWantedCheck))
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND (  IS_PLAYER_CONTROL_ON(PLAYER_ID())  OR (bIgnoreControlCheck))
		#IF IS_DEBUG_BUILD
			IF bWdUseDebug
				NET_DW_PRINT("[IsPlayerGoodToDeliverCars] Returning True")
			ENDIF
		#ENDIF
		
		RETURN(TRUE)
	ELSE
		#IF IS_DEBUG_BUILD
			IF bTempDoDebug
				IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
					NET_DW_PRINT("[IsPlayerGoodToDeliverCars] Failing on IS_PLAYER_ON_ANY_FM_MISSION")
				ENDIF
				
				IF IS_FM_MISSION_LAUNCH_IN_PROGRESS()
					NET_DW_PRINT("[IsPlayerGoodToDeliverCars] Failing on IS_FM_MISSION_LAUNCH_IN_PROGRESS")
				ENDIF
				
				IF IS_PED_INJURED(PLAYER_PED_ID())
					NET_DW_PRINT("[IsPlayerGoodToDeliverCars] Failing on IS_PED_INJURED")
				ENDIF
				
				IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0) AND NOT  (bIgnoreWantedCheck)
					NET_DW_PRINT("[IsPlayerGoodToDeliverCars] Failing on IS_PLAYER_WANTED_LEVEL_GREATER")
				ENDIF
				
				IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID()) AND NOT (bIgnoreControlCheck)
					NET_DW_PRINT("[IsPlayerGoodToDeliverCars] Failing on IS_PLAYER_CONTROL_ON")
				ENDIF
				
				IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
					NET_DW_PRINT("[IsPlayerGoodToDeliverCars] Failing on IS_NET_PLAYER_OK")
				ENDIF
			ENDIF
		#ENDIF
	ENDIF
	RETURN(FALSE)
	*/
	
ENDFUNC



FUNC BOOL MakeDriverNetOwnerOfVehicle()
	IF IsPlayerDriver()
		NETWORK_REQUEST_CONTROL_OF_ENTITY(DeliveredCar)
		IF NETWORK_HAS_CONTROL_OF_ENTITY(DeliveredCar)
			RETURN(TRUE)
		ENDIF
	ELSE
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC









FUNC INT MAX_INT(INT iInt1, INT iInt2)
	IF (iInt1 > iInt2)
		RETURN(iInt1)
	ENDIF
	RETURN(iInt2)
ENDFUNC

FUNC INT MIN_INT(INT iInt1, INT iInt2)
	IF (iInt1 < iInt2)
		RETURN(iInt1)
	ENDIF
	RETURN(iInt2)
ENDFUNC

FUNC BOOL IS_ANY_NON_PLAYER_PED_IN_VEHICLE()
	PED_INDEX tempPed
	VEHICLE_SEAT seatPos
	INT i
	VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	REPEAT 4 i										
		seatPos = INT_TO_ENUM(VEHICLE_SEAT, i-1)
		
		IF NOT IS_VEHICLE_SEAT_FREE(veh, seatPos)

			IF DOES_ENTITY_EXIST( GET_PED_IN_VEHICLE_SEAT(veh, seatPos) )

				IF NOT IS_ENTITY_DEAD( GET_PED_IN_VEHICLE_SEAT(veh, seatPos) )

					tempPed = GET_PED_IN_VEHICLE_SEAT(veh, seatPos)
					IF NOT IS_PED_A_PLAYER(tempPed)
						#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[IS_ANY_NON_PLAYER_PED_IN_VEHICLE] Found a non player ped in seat ", ENUM_TO_INT(seatPos)) #ENDIF
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VEH_RIGGED(VEHICLE_INDEX veh)
	IF IS_VEHICLE_DRIVEABLE(veh)
		IF DECOR_IS_REGISTERED_AS_TYPE("bombdec1",DECOR_TYPE_INT)
			IF DECOR_EXIST_ON(veh,"bombdec1")
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF DECOR_IS_REGISTERED_AS_TYPE("bombdec",DECOR_TYPE_INT)
			IF DECOR_EXIST_ON(veh,"bombdec")
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF DECOR_IS_REGISTERED_AS_TYPE("bombowner",DECOR_TYPE_INT)
			IF DECOR_EXIST_ON(veh,"bombowner")
				RETURN TRUE
			ENDIF
		ENDIF
		
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_VEHICLE_BEEN_BEEN_MODDED_BY_ANOTHER_PLAYER(VEHICLE_INDEX veh)
	IF DECOR_EXIST_ON(veh, "Veh_Modded_By_Player") AND DECOR_GET_INT(veh, "Veh_Modded_By_Player") != NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


PROC UPDATE_HELP_IMPORT_EXPORT()
	INT iTempRepairCost
	IF NOT HAS_NET_TIMER_STARTED(timeHelpCheck)
		START_NET_TIMER(timeHelpCheck)
	ELSE
		IF HAS_NET_TIMER_EXPIRED(timeHelpCheck, 250)
			RESET_NET_TIMER(timeHelpCheck)
			
			IF IsPlayerGoodToDeliverCars(TRUE)
				IF NOT IS_BIT_SET(iBS_Help, HELP_LAUNCH_IMPEXP)
				AND NOT IS_PLAYER_IN_SPECIFIC_IMPORT_EXPORT_VEHICLE()
					//-- Gets set in MAINTAIN_TEXT_MESSAGE
//					#IF IS_DEBUG_BUILD
//						 NET_DW_PRINT("[UPDATE_HELP_IMPORT_EXPORT] else 1")
//					#ENDIF
				ELSE
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF IsPlayerInWantedImportExportCar()	
						OR IS_PLAYER_IN_SPECIFIC_IMPORT_EXPORT_VEHICLE()
						OR IS_PLAYER_IN_RECENTLY_DELIVERED_CAR()
							IF NOT HAS_VEHICLE_BEEN_BEEN_MODDED_BY_ANOTHER_PLAYER(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
								IF NOT GET_IS_VEHICLE_SPRAYED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))							
								AND NOT IS_VEH_RIGGED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
									IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
										IF DOES_PLAYER_HAVE_ENOUGH_CASH_FOR_RESPRAY(iTempRepairCost)
											// print help about getting this car resprayed
											IF NOT IS_BIT_SET(iBS_Help, HELP_RESPRAY_MESSAGE)
												IF IS_OK_TO_PRINT_HELP()		
												AND TIMERA() > 500
													//PRINT_HELP("IMPEX_REPSRAY_M")	
													PRINT_HELP("IMPEX_FSPRAY_FM")	
												
													SET_BIT(iBS_Help, HELP_RESPRAY_MESSAGE)
													#IF IS_DEBUG_BUILD
														NET_DW_PRINT_STRINGS("[UPDATE_HELP_IMPORT_EXPORT] Done get car resprayed help, I'm in this car model ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))))
													#ENDIF
												ELSE
													#IF IS_DEBUG_BUILD
														 NET_DW_PRINT("[UPDATE_HELP_IMPORT_EXPORT] else 3")
													#ENDIF
												ENDIF
											ELSE
												SETTIMERA(0)
											ENDIF
										ELSE
											// print help about not having enough cash
											IF NOT IS_BIT_SET(iBS_Help, HELP_NO_CASH)
												IF IS_OK_TO_PRINT_HELP()		
												AND TIMERA() > 500
													//PRINT_HELP("IMPEX_REPSRAY_M")	
													PRINT_HELP("IMPEX_CASH_FM")	
												
													SET_BIT(iBS_Help, HELP_NO_CASH)
													#IF IS_DEBUG_BUILD
														NET_DW_PRINT("[UPDATE_HELP_IMPORT_EXPORT] Done not enough cash help")
													#ENDIF
												ENDIF
											ELSE
												SETTIMERA(0)
											ENDIF
										ENDIF
									ELSE
										// if player has a wanted level then you can't deliver it
										IF NOT IS_BIT_SET(iBS_Help, HELP_INITIAL_WANTED)
											IF IS_OK_TO_PRINT_HELP()
											AND TIMERA() > 500
												PRINT_HELP("IMPEX_WANTED_FM")
										
												SET_BIT(iBS_Help, HELP_INITIAL_WANTED)
												#IF IS_DEBUG_BUILD NET_DW_PRINT(" [UPDATE_HELP_IMPORT_EXPORT] Doing initial wanted level help...") #ENDIF
											ENDIF
										ELSE
											SETTIMERA(0)
										ENDIF	
									ENDIF
								ELSE
									
									IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
										IF NOT IS_ANY_NON_PLAYER_PED_IN_VEHICLE()
											IF NOT IS_VEH_RIGGED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
												// print help about taking it to Madrazo	
												IF NOT IS_BIT_SET(iBS_Help, HELP_DELIVER_MESSAGE)
													CLEAR_BIT(iBS_Help, HELP_VEH_RIGGED)
													
													IF IS_OK_TO_PRINT_HELP()	
													AND TIMERA() > 2000 // 
														IF MPGlobalsAmbience.g_iGarageCamStage = 0
															
															IF NOT IsCarInContactGarage(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))	
																PRINT_HELP("IMPEX_DELIVER_FM")	
																SET_BIT(iBS_Help, HELP_DELIVER_MESSAGE)
																#IF IS_DEBUG_BUILD NET_DW_PRINT_STRINGS("[UPDATE_HELP_IMPORT_EXPORT] Done get car delivered help, I'm in this car model ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))) #ENDIF
															ENDIF
														ENDIF
													ENDIF	
												ELSE
													SETTIMERA(0)
												ENDIF	
											ELSE
												IF NOT IS_BIT_SET(iBS_Help, HELP_VEH_RIGGED)
													//-- Help text for explosives on the vehicle
													IF IS_OK_TO_PRINT_HELP()	
													AND TIMERA() > 2000
														PRINT_HELP("IMPEX_RIG_FM")	
														SET_BIT(iBS_Help, HELP_VEH_RIGGED)
														#IF IS_DEBUG_BUILD NET_DW_PRINT(" [UPDATE_HELP_IMPORT_EXPORT] Doing veh rigged help...") #ENDIF
													ENDIF	
												ELSE
													SETTIMERA(0)
												ENDIF	
											ENDIF
										ELSE
											IF NOT IS_BIT_SET(iBS_Help, HELP_HOOK_CAR)
												//-- Help text for non-player peds in the vehicle
												IF IS_OK_TO_PRINT_HELP()	
												AND TIMERA() > 2000
													PRINT_HELP("IMPEX_WANTED_P")	//Simeon wants this vehicle but he will not accept delivery while you are with a prostitute.
													SET_BIT(iBS_Help, HELP_HOOK_CAR)
												ENDIF	
											ELSE
												SETTIMERA(0)
											ENDIF	
										ENDIF
									ELSE
									
										// if player has a wanted level then you can't deliver it
										IF NOT IS_BIT_SET(iBS_Help, HELP_NO_DELIVER_WANTED)
											IF IS_OK_TO_PRINT_HELP()
											AND TIMERA() > 5000
												PRINT_HELP("IMPEX_WANTED_FM")
										
												SET_BIT(iBS_Help, HELP_NO_DELIVER_WANTED)
												#IF IS_DEBUG_BUILD NET_DW_PRINT(" [UPDATE_HELP_IMPORT_EXPORT] Doing got wanted level help...") #ENDIF
											ENDIF
										ELSE
											SETTIMERA(0)
										ENDIF						
									ENDIF				
								ENDIF
							ELSE
								//-- Vehicle has been modded by another player
								#IF IS_DEBUG_BUILD
									NET_DW_PRINT("NOT doing help...Vehicle modded by another player.")
								#ENDIF
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
							//	 NET_DW_PRINT("[UPDATE_HELP_IMPORT_EXPORT] else 2")
							#ENDIF
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("IMPEX_FSPRAY_FM")
								CLEAR_HELP()
							ENDIF
							
							CLEAR_BIT(iBS_Help, HELP_RESPRAY_MESSAGE)
							CLEAR_BIT(iBS_Help, HELP_DELIVER_MESSAGE)
							CLEAR_BIT(iBS_Help, HELP_NO_DELIVER_WANTED)
							CLEAR_BIT(iBS_Help, HELP_HOOK_CAR)
							CLEAR_BIT(iBS_Help, HELP_VEH_RIGGED)
							CLEAR_BIT(iBS_Help, HELP_NO_CASH)
							CLEAR_BIT(iBS_Help, HELP_INITIAL_WANTED)
							SETTIMERA(0)						
						ENDIF
					ELSE	
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("IMPEX_FSPRAY_FM")
							CLEAR_HELP()
						ENDIF
						
						CLEAR_BIT(iBS_Help, HELP_RESPRAY_MESSAGE)
						CLEAR_BIT(iBS_Help, HELP_DELIVER_MESSAGE)
						CLEAR_BIT(iBS_Help, HELP_NO_DELIVER_WANTED)
						CLEAR_BIT(iBS_Help, HELP_HOOK_CAR)
						CLEAR_BIT(iBS_Help, HELP_VEH_RIGGED)
						CLEAR_BIT(iBS_Help, HELP_NO_CASH)
						CLEAR_BIT(iBS_Help, HELP_INITIAL_WANTED)
						IF TIMERA() > 0
							SETTIMERA(0)	
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("IMPEX_FSPRAY_FM")
					CLEAR_HELP()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_MY_MOST_RECENT_CAR_IN_GARAGE()
	IF vehLastDeliveredCar = DeliveredCar
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(DeliveredCar)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_VEHICLE_DRIVEABLE(DeliveredCar)
		RETURN FALSE
	ENDIF
	
	IF NOT IsCarInContactGarage(DeliveredCar)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DISPLAY_OBJECTIVE_TEXT(#IF IS_DEBUG_BUILD BOOL bDoDebugPrint = FALSE #ENDIF)
	BOOL bSHouldDisplay
	
	#IF IS_DEBUG_BUILD
		IF bWdForceObjective
			RETURN TRUE
		ENDIF
		
		IF g_ShouldShiftingTutorialsBeSkipped
			RETURN FALSE
		ENDIF
	#ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPriorityVeh)
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niPriorityVeh)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niPriorityVeh))
			//		RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalStatBitset, biStat_DoneAllObjectiveText)
		IF NOT IS_BIT_SET(iLocalStatBitset, biStat_DoneObjectiveOnce)
			bSHouldDisplay = TRUE
			#IF IS_DEBUG_BUILD 
				IF bDoDebugPrint
					NET_DW_PRINT("[SHOULD_DISPLAY_OBJECTIVE_TEXT] True because biStat_DoneObjectiveOnce") 
				ENDIF
			#ENDIF
		ELIF NOT IS_BIT_SET(iLocalStatBitset, biStat_DoneObjectiveTwice)
			bSHouldDisplay = TRUE
			#IF IS_DEBUG_BUILD 
				IF bDoDebugPrint
					NET_DW_PRINT("[SHOULD_DISPLAY_OBJECTIVE_TEXT] True because biStat_DoneObjectiveTwice") 
				ENDIF
			#ENDIF
		ELIF NOT IS_BIT_SET(iLocalStatBitset, biStat_DoneObjectiveThree) 
			bSHouldDisplay = TRUE
			#IF IS_DEBUG_BUILD 
				IF bDoDebugPrint
					NET_DW_PRINT("[SHOULD_DISPLAY_OBJECTIVE_TEXT] True because biStat_DoneObjectiveThree") 
				ENDIF
			#ENDIF
		ENDIF
		
		IF NOT bSHouldDisplay
			SET_BIT(iLocalStatBitset, biStat_DoneAllObjectiveText)
			#IF IS_DEBUG_BUILD 
				IF bDoDebugPrint
					NET_DW_PRINT("[SHOULD_DISPLAY_OBJECTIVE_TEXT] Setting biL_DoneAllObjectiveText as done text 3 times ") 
				ENDIF
			#ENDIF
		ENDIF
	ENDIF
	
	
	RETURN bSHouldDisplay
ENDFUNC

PROC SET_DONE_IMPORT_EXPORT_OBJECTIVE_TEXT()
	INT iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_IMP_EXP) 
	BOOL bUpdated
	IF NOT IS_BIT_SET(iStatInt, BI_IMPEXP_OBJECTIVE_ONCE)
		SET_BIT(iStatInt, BI_IMPEXP_OBJECTIVE_ONCE)
		SET_BIT(iLocalStatBitset, biStat_DoneObjectiveOnce)
		bUpdated = TRUE
		#IF IS_DEBUG_BUILD NET_DW_PRINT("[SET_DONE_IMPORT_EXPORT_OBJECTIVE_TEXT] Set BI_IMPEXP_OBJECTIVE_ONCE") #ENDIF
	ELIF NOT IS_BIT_SET(iStatInt, BI_IMPEXP_OBJECTIVE_TWICE) 
		SET_BIT(iStatInt, BI_IMPEXP_OBJECTIVE_TWICE)
		SET_BIT(iLocalStatBitset, biStat_DoneObjectiveTwice)
		bUpdated = TRUE
		#IF IS_DEBUG_BUILD NET_DW_PRINT("[SET_DONE_IMPORT_EXPORT_OBJECTIVE_TEXT] Set BI_IMPEXP_OBJECTIVE_TWICE") #ENDIF
	ELIF NOT IS_BIT_SET(iStatInt, BI_IMPEXP_OBJECTIVE_THREE) 
		SET_BIT(iStatInt, BI_IMPEXP_OBJECTIVE_THREE)
		SET_BIT(iLocalStatBitset, biStat_DoneObjectiveThree)
		bUpdated = TRUE
		#IF IS_DEBUG_BUILD NET_DW_PRINT("[SET_DONE_IMPORT_EXPORT_OBJECTIVE_TEXT] Set BI_IMPEXP_OBJECTIVE_THREE") #ENDIF
	ENDIF
	
	IF bUpdated
		SET_MP_INT_CHARACTER_STAT(MP_STAT_FM_IMP_EXP, iStatInt) 
	ENDIF
ENDPROC



PROC CLEAR_IMPORT_EXPORT_OBJECTIVE_TEXT()

	Clear_Any_Objective_Text_From_This_Script()
ENDPROC

FUNC BOOL CREATE_IMP_MECHANIC_PED()
	MODEL_NAMES mMechanic = S_M_Y_XMECH_02	
	REQUEST_MODEL(mMechanic)
	IF HAS_MODEL_LOADED(mMechanic)
		pedMechanic = CREATE_PED(PEDTYPE_CIVMALE, mMechanic,<<1199.4033, -3113.5908, 4.5453>>, 261.7899, FALSE, FALSE)
	//	pedMechanic = CREATE_PED(PEDTYPE_CIVMALE, mMechanic,<<1202.0586, -3107.2048, 4.5535>>, 261.7899, FALSE, FALSE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedMechanic, TRUE)
		FREEZE_ENTITY_POSITION(pedMechanic, TRUE)
		REMOVE_WEAPON_FROM_PED(pedMechanic, GADGETTYPE_PARACHUTE)
		
		SET_PED_COMPONENT_VARIATION(pedMechanic, PED_COMP_HEAD, 1, 1)
		SET_PED_COMPONENT_VARIATION(pedMechanic, PED_COMP_BERD, 0, 0)
		SET_PED_COMPONENT_VARIATION(pedMechanic, PED_COMP_HAIR, 0, 2)
		SET_PED_COMPONENT_VARIATION(pedMechanic, PED_COMP_TORSO, 0, 2)
		SET_PED_COMPONENT_VARIATION(pedMechanic, PED_COMP_LEG, 1, 0)
		SET_PED_COMPONENT_VARIATION(pedMechanic, PED_COMP_HAND, 0, 0)
		SET_PED_COMPONENT_VARIATION(pedMechanic, PED_COMP_FEET, 0, 0)
		SET_PED_COMPONENT_VARIATION(pedMechanic, PED_COMP_TEETH, 0, 0)
		SET_PED_COMPONENT_VARIATION(pedMechanic, PED_COMP_SPECIAL, 0, 0)
		SET_PED_COMPONENT_VARIATION(pedMechanic, PED_COMP_SPECIAL2, 0, 0)
		SET_PED_COMPONENT_VARIATION(pedMechanic, PED_COMP_DECL, 0, 0)
		SET_PED_COMPONENT_VARIATION(pedMechanic, PED_COMP_JBIB, 0, 0)

		CLEAR_PED_PROP(pedMechanic, ANCHOR_HEAD)
		CLEAR_PED_PROP(pedMechanic, ANCHOR_EYES)
		CLEAR_PED_PROP(pedMechanic, ANCHOR_EARS)
		CLEAR_PED_PROP(pedMechanic, ANCHOR_MOUTH)
		CLEAR_PED_PROP(pedMechanic, ANCHOR_LEFT_HAND)
		CLEAR_PED_PROP(pedMechanic, ANCHOR_RIGHT_HAND)
		CLEAR_PED_PROP(pedMechanic, ANCHOR_LEFT_WRIST)
		CLEAR_PED_PROP(pedMechanic, ANCHOR_RIGHT_WRIST)
		CLEAR_PED_PROP(pedMechanic, ANCHOR_HIP)
		
		ADD_PED_FOR_DIALOGUE(speechMechanic, 4, pedMechanic, "Mechanic")
		#IF IS_DEBUG_BUILD
			NET_DW_PRINT("Created mechanic")
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

BOOL bGotStringForConv
STRING sConv
PROC MAINTAIN_MECHANIC_PED()
	
	VEHICLE_INDEX veh
	SWITCH iMechanicProg
		CASE 0
			IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
				IF NOT HAS_NET_TIMER_STARTED(timeMechanicCheck)
				OR HAS_NET_TIMER_EXPIRED(timeMechanicCheck, 2000)
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<1199.4033, -3113.5908, 4.5453>>) < 400.0
						//	IF CREATE_IMP_MECHANIC_PED()
								ADD_PED_FOR_DIALOGUE(speechMechanic, 4, pedMechanic, "MECHANIC_IMP")
								bGotStringForConv = FALSE
								iMechanicProg++
								#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("iMechanicProg = ", iMechanicProg) #ENDIF
						//	ENDIF
						ENDIF
					ENDIF
					RESET_NET_TIMER(timeMechanicCheck)
					START_NET_TIMER(timeMechanicCheck)
				ENDIF
			ENDIF
		BREAK
		
		CASE 1	
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
				
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<1199.4033, -3113.5908, 4.5453>>) < 600.0
						IF bPlayerInVehicleReadyForDelivery
							IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
								
								IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<1204.223755,-3121.533203,4.357823>>, <<1204.520874,-3107.603027,8.653814>>, 5.187500)
									IF NOT bGotStringForConv
										veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
										IF GET_CARMOD_REPAIR_COST(veh) > 500
											sConv = "FM_IEPOOR"
										ELSE
											sConv = "FM_IEGOOD"
										ENDIF
										#IF IS_DEBUG_BUILD NET_DW_PRINT_STRINGS("Doing mechanic speech with string = ", sConv) #ENDIF
										bGotStringForConv = TRUE
									ELSE
										IF CREATE_CONVERSATION_USING_V_CONTENT_IN_DLC(speechMechanic, "FM_1AU", sConv, CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT)
											vehMechanic = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
											iMechanicProg++
											bGotStringForConv = FALSE
											#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("Done mechanic speech iMechanicProg = ", iMechanicProg) #ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						CLEANUP_MECHANIC_PED()
						iMechanicProg = 0
						#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("Cleaned up mechanic as far from garage iMechanicProg = ", iMechanicProg) #ENDIF
					ENDIF
				ELSE
					CLEANUP_MECHANIC_PED()
					iMechanicProg = 0
					#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("Cleaned up mechanic as on mission iMechanicProg = ", iMechanicProg) #ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
				
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<1199.4033, -3113.5908, 4.5453>>) < 600.0
						IF bPlayerInVehicleReadyForDelivery
							IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
								IF GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) <> vehMechanic
									iMechanicProg = 1
									#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("Player in new vehicle, still close to mechanic iMechanicProg = ", iMechanicProg) #ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						CLEANUP_MECHANIC_PED()
						iMechanicProg = 0
						#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("Cleaned up mechanic as far from garage iMechanicProg = ", iMechanicProg) #ENDIF
					ENDIF
				ELSE
					CLEANUP_MECHANIC_PED()
					iMechanicProg = 0
					#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("Cleaned up mechanic as on mission iMechanicProg = ", iMechanicProg) #ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC BOOL IS_PLAYER_BROWSING_IN_ANY_CARMOD()
	RETURN (IS_PLAYER_BROWSING_ITEMS_IN_SHOP(CARMOD_SHOP_01_AP) 
	OR IS_PLAYER_BROWSING_ITEMS_IN_SHOP(CARMOD_SHOP_05_ID2 )
	OR IS_PLAYER_BROWSING_ITEMS_IN_SHOP(CARMOD_SHOP_06_BT1 )
	OR IS_PLAYER_BROWSING_ITEMS_IN_SHOP(CARMOD_SHOP_07_CS1)
	OR IS_PLAYER_BROWSING_ITEMS_IN_SHOP(CARMOD_SHOP_08_CS6 )
	OR IS_PLAYER_BROWSING_ITEMS_IN_SHOP(CARMOD_SHOP_SUPERMOD )
	)

ENDFUNC

PROC CLEAR_ALL_OBJECTIVE_TEXT_FLAGS()
	IF bDisplayLoseCopsObjective
		bDisplayLoseCopsObjective = FALSE
	ENDIF
	
	IF bDisplayCarmodObjective	
		bDisplayCarmodObjective = FALSE
	ENDIF
	
	IF bDisplaySimeonObjective
		bDisplaySimeonObjective = FALSE
	ENDIF
	
	IF bDisplayResprayObjective
		bDisplayResprayObjective = FALSE
	ENDIF
	
	IF bDisplayLeaveCarmodObjective
		bDisplayLeaveCarmodObjective = FALSE
	ENDIF
	
	IF bDisplayFixCarObjective
		bDisplayFixCarObjective = FALSE
	//	#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_IMPEX_OBJECTIVE] bDisplayFixCarObjective - False 5") #ENDIF
	ENDIF
ENDPROC

VEHICLE_INDEX vehLastWanted
VEHICLE_INDEX vehLastNotSprayed
FUNC BOOL SHOULD_GIVE_WANTED_LEVEL_FOR_VEHICLE(VEHICLE_INDEX veh)
	IF IS_VEHICLE_DRIVEABLE(veh)
		IF GET_PED_IN_VEHICLE_SEAT(veh) = PLAYER_PED_ID()	
			IF NOT HAS_VEHICLE_BEEN_BEEN_MODDED_BY_ANOTHER_PLAYER(veh)
				IF veh <> vehLastWanted
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 2
						#IF IS_DEBUG_BUILD NET_DW_PRINT("[SHOULD_GIVE_WANTED_LEVEL_FOR_VEHICLE] True") #ENDIF
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE

ENDFUNC



PROC UPDATE_VEHICLE_DELIVERY()

	PED_INDEX tempPed
	VEHICLE_SEAT seatPos
	VEHICLE_INDEX vehTemp
	VEHICLE_INDEX vehCurrent
	INT i
	INT iTotalPlayers
	INT iStatInt
//	INT iActualCash
//	INT iActualXp 
	BOOL bPlayerInRecentlyDel
	INT iTempRepairCost
	
	#IF IS_DEBUG_BUILD
		IF bTimedDebug
			NET_DW_PRINT("[bTimedDebug] [UPDATE_VEHICLE_DELIVERY] Called...")
		ENDIF
	#ENDIF
		
	
	IF IsPlayerGoodToDeliverCars(TRUE, TRUE)			
	
		SWITCH PlayerBD[PARTICIPANT_ID_TO_INT()].iDeliveryState
			CASE PLAYER_DELIVERY_STATE_WAITING_FOR_CAR 
			//	IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
			//	OR IS_BIT_SET(iLocalImpExpBitSet, biL_InGarageInitialCheck)
					IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
						bPlayerInRecentlyDel = IS_PLAYER_IN_RECENTLY_DELIVERED_CAR()
						IF (IsPlayerInWantedImportExportCar() OR IS_PLAYER_IN_SPECIFIC_IMPORT_EXPORT_VEHICLE() OR bPlayerInRecentlyDel)
							IF NOT IS_ANY_NON_PLAYER_PED_IN_VEHICLE()
								IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
									IF NOT ABOUT_TO_SET_AS_PERSONAL_VEHICLE()
										IF bDisplayLoseCopsObjective
											bDisplayLoseCopsObjective = FALSE
										ENDIF
										
										vehCurrent = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
										
										IF DECOR_IS_REGISTERED_AS_TYPE("AllowModSprayRepair", DECOR_TYPE_BOOL)
											IF NOT DECOR_EXIST_ON(vehCurrent, "AllowModSprayRepair")
											OR (DECOR_EXIST_ON(vehCurrent, "AllowModSprayRepair")
											AND DECOR_GET_BOOL(vehCurrent, "AllowModSprayRepair") = FALSE)
												DECOR_SET_BOOL(vehCurrent, "AllowModSprayRepair", TRUE)
												PRINTLN("[IMPEX] UPDATE_VEHICLE_DELIVERY - Setting bool decorator \"AllowModSprayRepair\" on current vehicle so it can be modded.")
											ENDIF
										ENDIF
										
										// Need to handle already being in wanted vehicle when script launches
										// or a new list is received
										IF NOT bInWantedCarWhenListReceived 
											IF SHOULD_GIVE_WANTED_LEVEL_FOR_VEHICLE(vehCurrent)
												SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
												SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
											ENDIF
										ENDIF
										
										vehLastWanted = vehCurrent
										IF IS_BIT_SET(iLocalImpExpBitSet, biL_PlayerInPersonalVehicle)
											CLEAR_BIT(iLocalImpExpBitSet, biL_PlayerInPersonalVehicle)
											#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing biL_PlayerInPersonalVehicle") #ENDIF
										ENDIF
										IF IS_BIT_SET(iBS_Help, HELP_NOT_NEEDED)
											CLEAR_BIT(iBS_Help, HELP_NOT_NEEDED)
										ENDIF
										
										IF IS_BIT_SET(iBS_Help, HELP_CREW_EMBLEM)
											CLEAR_BIT(iBS_Help, HELP_CREW_EMBLEM)
										ENDIF
										IF IS_BIT_SET(iBS_Help, HELP_PERSONAL_VEH)
											CLEAR_BIT(iBS_Help, HELP_PERSONAL_VEH)
										ENDIF
										
										
										IF NOT IS_BIT_SET(iLocalImpExpBitSet, biL_InRecentlyDeliveredCar)
											IF bPlayerInRecentlyDel
												SET_BIT(iLocalImpExpBitSet, biL_InRecentlyDeliveredCar)
												#IF IS_DEBUG_BUILD NET_DW_PRINT("Setting I'm in a recently delivered car") #ENDIF
											ENDIF
										ELSE	
											IF NOT bPlayerInRecentlyDel
												CLEAR_BIT(iLocalImpExpBitSet, biL_InRecentlyDeliveredCar)
												#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing I'm in a recently delivered car as I'm in a valid car, but not a rcently delivered one") #ENDIF
											ENDIF
										ENDIF
										
										// if vehicle needs to be resprayed first
										IF GET_IS_VEHICLE_SPRAYED(vehCurrent)
											IF NOT IS_VEH_RIGGED(vehCurrent)
											//	IF NOT IS_ANY_NON_PLAYER_PED_IN_VEHICLE()
													bDisplayImpExpBlip = TRUE
													IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
														IF bDisplayLeaveCarmodObjective
															bDisplayLeaveCarmodObjective = FALSE
														ENDIF
														IF GET_PED_IN_VEHICLE_SEAT(vehCurrent) = PLAYER_PED_ID()
															bDisplaySimeonObjective = TRUE
														ENDIF
													ELSE
														IF IS_PLAYER_BROWSING_IN_ANY_CARMOD()
															IF bDisplaySimeonObjective
																bDisplaySimeonObjective = FALSE
															ENDIF
															
															bDisplayLeaveCarmodObjective = TRUE
														ENDIF
													ENDIF
													
													#IF IS_DEBUG_BUILD 
														IF bTimedDebug
															NET_DW_PRINT("I'm good to deliver!") 
														ENDIF
													#ENDIF
													
													bDisplayCarmodBlip = FALSE
													bDisplayCarmodObjective = FALSE
													bDisplayResprayObjective = FALSE
													bDisplayFixCarObjective = FALSE
												//	#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_IMPEX_OBJECTIVE] bDisplayFixCarObjective - False 6") #ENDIF
													bDisplayLoseCopsObjective = FALSE
													
													bPlayerInVehicleReadyForDelivery = TRUE // for mechanic speech
													
													
													
														
													// open garage as we get near
													IF IS_PLAYER_IN_OPENING_RANGE_OF_GARAGE(MP_GAR_SIMEON)
														//NET_PRINT("Import export player is in vehicle and wants to drop it off.") NET_NL()
														#IF IS_DEBUG_BUILD 
															IF bTimedDebug
																NET_DW_PRINT("Set bRequestGarageOpen...") 
															ENDIF
														#ENDIF
														bRequestGarageOpen = TRUE
													ELSE
														#IF IS_DEBUG_BUILD 
															IF bTimedDebug
																NET_DW_PRINT("Not IS_PLAYER_IN_OPENING_RANGE_OF_GARAGE") 
															ENDIF
														#ENDIF
													ENDIF						
												
													// store car player is in
													DeliveredCar = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())	
													IF NOT IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(DeliveredCar)
														IF IsCarInContactGarage(DeliveredCar)		
															IF NOT IS_BIT_SET(iLocalImpExpBitSet, biL_InGarageInitialCheck)
																SET_BIT(iLocalImpExpBitSet, biL_InGarageInitialCheck)
																#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_VEHICLE_DELIVERY] - Set biL_InGarageInitialCheck") NET_NL() #ENDIF
															ELSE	
																IF IsCarInContactGarage(DeliveredCar, TRUE)
																	//bRequestGarageOpen = FALSE
																	VECTOR vVel 
																	vVel = GET_ENTITY_VELOCITY(DeliveredCar)
																	IF (GET_ENTITY_SPEED(DeliveredCar) < 0.5)
																	OR ABSF( vVel.y) < 1.3
																//	OR(IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE) AND NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID()))
																		
																		CLEAR_IMPORT_EXPORT_OBJECTIVE_TEXT()
																		
																		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_NO_COLLISION | NSPC_CLEAR_TASKS)
																	
																		#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_VEHICLE_DELIVERY] - IsCarInContactGarage = TRUE") NET_NL() #ENDIF
																		
																		IF IS_PLAYER_IN_SPECIFIC_IMPORT_EXPORT_VEHICLE()
																			#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_VEHICLE_DELIVERY] - I helped del spec car") NET_NL() #ENDIF
																			SET_BIT(iLocalImpExpBitSet, biL_HelpDelSpecCar)
																			SET_BIT(playerbd[PARTICIPANT_ID_TO_INT()].iImportExportBitSet, biP_DeliveredSpecVehicle)
																		ENDIF
																		
																		IF IsPlayerInWantedImportExportCar()
																		OR IS_PLAYER_IN_RECENTLY_DELIVERED_CAR()
																			SET_BIT(iLocalImpExpBitSet, biL_InVehicleOnList)
																		ENDIF
																		
																		IF IsPlayerDriver()	
																			#IF IS_DEBUG_BUILD 
																				NET_DW_PRINT("[UPDATE_VEHICLE_DELIVERY] - I am driver") 
																				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
																					NET_DW_PRINT_STRINGS("I just delivered this car model ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))))
																					
																				ENDIF
																			#ENDIF
																			
																			bIAmDriver = TRUE
																		ELSE
																			#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_VEHICLE_DELIVERY] - I'm not driving") NET_NL() #ENDIF
																			bIAmDriver = FALSE
																		ENDIF
																		SET_VEHICLE_DOORS_LOCKED(DeliveredCar, VEHICLELOCK_LOCKED)
																		IF NETWORK_HAS_CONTROL_OF_ENTITY(DeliveredCar)
																		//	FREEZE_ENTITY_POSITION(DeliveredCar, TRUE)
																			SET_ENTITY_PROOFS(DeliveredCar, TRUE, TRUE, TRUE, TRUE, TRUE)
																		ENDIF
																		
																		PlayerBD[PARTICIPANT_ID_TO_INT()].iVehicleListID = GET_LIST_ID_FROM_MODEL(GET_ENTITY_MODEL(DeliveredCar))	
																		PlayerBD[PARTICIPANT_ID_TO_INT()].iDeliveryState = PLAYER_DELIVERY_WAIT_TO_BE_WARPED_FROM_GARAGE	
																		#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_VEHICLE_DELIVERY] iDeliveryState = PLAYER_DELIVERY_WAIT_TO_BE_WARPED_FROM_GARAGE") #ENDIF
																		
																		// store all players in car
																		iTotalPlayers = 0
																		
																		REPEAT 4 i
																			
																			seatPos = INT_TO_ENUM(VEHICLE_SEAT, i-1)
																			
																			PlayerInCar[i] = INVALID_PLAYER_INDEX()
																			
																			IF NOT IS_VEHICLE_SEAT_FREE(DeliveredCar, seatPos)
																				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[UPDATE_VEHICLE_DELIVERY] seat not free ", ENUM_TO_INT(seatPos)) #ENDIF
																				IF DOES_ENTITY_EXIST( GET_PED_IN_VEHICLE_SEAT(DeliveredCar, seatPos) )
																					#IF IS_DEBUG_BUILD NET_DW_PRINT("Entity in seat exists") #ENDIF
																					IF NOT IS_ENTITY_DEAD( GET_PED_IN_VEHICLE_SEAT(DeliveredCar, seatPos) )
																						#IF IS_DEBUG_BUILD NET_DW_PRINT("Entity in seat alive") #ENDIF
																						tempPed = GET_PED_IN_VEHICLE_SEAT(DeliveredCar, seatPos)
																						IF IS_PED_A_PLAYER(tempPed)
																							#IF IS_DEBUG_BUILD NET_DW_PRINT("Ped in seat is a player") #ENDIF
																							IF IS_NET_PLAYER_OK(NETWORK_GET_PLAYER_INDEX_FROM_PED(tempPed), FALSE, FALSE)
																								#IF IS_DEBUG_BUILD NET_DW_PRINT("Player in seat ok") #ENDIF
																								IF NETWORK_IS_PLAYER_A_PARTICIPANT(NETWORK_GET_PLAYER_INDEX_FROM_PED(tempPed))
																									#IF IS_DEBUG_BUILD NET_DW_PRINT("Player in seat a participant") #ENDIF
																									PlayerInCar[i] = NETWORK_GET_PLAYER_INDEX_FROM_PED(tempPed)
																									iTotalPlayers += 1
																								ELSE
																									#IF IS_DEBUG_BUILD NET_DW_PRINT("Player in seat not a participant") #ENDIF
																								ENDIF
																							ENDIF
																						ENDIF
																					ENDIF
																				ENDIF
																			ENDIF
																			
																			
																		ENDREPEAT
																		
																		// store money for each player
																		iTotalCashValue = GET_CAR_MODEL_VALUE_FOR_IMPORT_EXPORT(GET_ENTITY_MODEL(DeliveredCar))
																		
																		#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("Cash value before damage applied... ", iTotalCashValue) #ENDIF
																		iTotalDamage = GET_CARMOD_REPAIR_COST(DeliveredCar) //GET_TOTAL_REPAIR_COSTS_FOR_CAR(DeliveredCar, iTotalCashValue)
																		
																		fCash = TO_FLOAT(iTotalCashValue - iTotalDamage)
																		
																		#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_FLOAT(" Total cash to be awarded (before split)... ", fCash) #ENDIF
																		
																		#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_FLOAT(" About to apply g_sMPTunables.fearnings_Import_Export_modifier modifier to fCash. Modifier = ", g_sMPTunables.fearnings_Import_Export_modifier ) #ENDIF
																		
																		fCash *= g_sMPTunables.fearnings_Import_Export_modifier 
																		
																		#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_FLOAT(" fCash after modifier = ", fCash) #ENDIF
																		
																		IF IS_BIT_SET(iLocalImpExpBitSet, biL_HelpDelSpecCar)
																			fCash *=1.3
																			#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_FLOAT("Vehicle was HPV, New cash awarded... ", fCash) #ENDIF
																		ENDIF
																		#IF IS_DEBUG_BUILD 
																			NET_DW_PRINT_STRING_INT("iTotalDamage... ", iTotalDamage)
																			NET_DW_PRINT_STRING_INT("iTotalPlayers... ", iTotalPlayers)
																			
																			NET_DW_PRINT_STRING_FLOAT("fCash before player count... ", fCash) 
																		#ENDIF
																		fCash /= TO_FLOAT(iTotalPlayers)
																		fXp = 500.0
																	//	fXp /= TO_FLOAT(iTotalPlayers)
																		#IF IS_DEBUG_BUILD 
																			NET_DW_PRINT_STRING_FLOAT("fCash after player count... ", fCash) 
																			NET_DW_PRINT_STRING_FLOAT("fXp after player count (not splitting)... ", fXp) 
																		#ENDIF	
																		ENABLE_LEAVE_VEHICLE_WHEN_IN_GARAGE()																											
																		DISABLE_WARP_OUT_OF_LOCKED_GARAGE(FALSE)
																		
																		INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_VEHEXPORTED, 1)
																		
																		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("IMPEX_DELIVER_FM")
																			CLEAR_HELP()
																		ENDIF
																		
																		CLEAR_BIT(iLocalImpExpBitSet, biL_InRecentlyDeliveredCar)
																		
																		//-- Update stat to track if we need to flash the import export blip
																		iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_IMP_EXP) 
																		IF NOT IS_BIT_SET(iStatInt, BI_IMPEXP_FLASHED_BLIP_ONCE)
																			SET_BIT(iStatInt, BI_IMPEXP_FLASHED_BLIP_ONCE)
																			SET_MP_INT_CHARACTER_STAT(MP_STAT_FM_IMP_EXP, iStatInt) 
																			#IF IS_DEBUG_BUILD NET_DW_PRINT("Setting stat bit BI_IMPEXP_FLASHED_BLIP_ONCE") #ENDIF
																		ELIF NOT IS_BIT_SET(iStatInt, BI_IMPEXP_FLASHED_BLIP_TWICE)
																			SET_BIT(iStatInt, BI_IMPEXP_FLASHED_BLIP_TWICE)
																			SET_MP_INT_CHARACTER_STAT(MP_STAT_FM_IMP_EXP, iStatInt) 
																			#IF IS_DEBUG_BUILD NET_DW_PRINT("Setting stat bit BI_IMPEXP_FLASHED_BLIP_TWICE") #ENDIF
																		ENDIF
																		
																		SET_DONE_IMPORT_EXPORT_OBJECTIVE_TEXT()
																		iActualCash = 0
																		
																	//	iActualCash = (100 * ROUND(fCash / 100.0))
																		
																		iActualCash = ROUND(fCash )
																		
																		#IF IS_DEBUG_BUILD
																			NET_DW_PRINT_STRING_INT("iActualCash before modulo ", iActualCash)
																		#ENDIF
																		
																		iActualCash -= (iActualCash % 25)
																		
																		#IF IS_DEBUG_BUILD
																			NET_DW_PRINT_STRING_INT("iActualCash after modulo ", iActualCash)
																		#ENDIF
																		//-- Add on an extra amount if I got the car fixed 
																		IF vehLastNotSprayed = vehCurrent
																			#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("Think I fixed the car, g_iLastResprayCost = ", g_iLastResprayCost) #ENDIF
																			
																			// - REMOVED 1728681 for business pack
	//																		IF g_iLastResprayCost > 0
	//																			iActualCash += g_iLastResprayCost
	//																			#IF IS_DEBUG_BUILD NET_DW_PRINT("Think I fixed the car, adding on respray cost (g_iLastResprayCost)") #ENDIF
	//																			
	//																		ELSE
	//																			#IF IS_DEBUG_BUILD NET_DW_PRINT("Think I fixed the car, g_iLastRepairCost = 0, adding on respray cost (400)") #ENDIF
	//																			iActualCash += 400
	//																			
	//																		ENDIF
																			
																			#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("Think I fixed the car, adding on repair cost = ", iInitialRepairCostforVehicle) #ENDIF
																			iActualCash += iInitialRepairCostforVehicle
																		ENDIF
																		
																		IF NOT IS_BIT_SET(iLocalImpExpBitSet, biL_HelpDelSpecCar)
																			IF iActualCash > g_sMPTunables.iImportExportEarnCap

																				#IF IS_DEBUG_BUILD
																					NET_DW_PRINT_STRING_INT("iActualCash exceeds Tunable, will clamp iImportExportEarnCap= ", g_sMPTunables.iImportExportEarnCap)
																					NET_DW_PRINT_STRING_INT("iActualCash exceeds Tunable, will clamp iActualCash= ", iActualCash)
																				#ENDIF
																				iActualCash = g_sMPTunables.iImportExportEarnCap

																			ENDIF
																		ELSE
																			IF iActualCash > g_sMPTunables.iImportExportEarnCap
																				#IF IS_DEBUG_BUILD
																					NET_DW_PRINT_STRING_INT("HPV iActualCash exceeds Tunable, will clamp iImportExportEarnCap= ", g_sMPTunables.iImportExportEarnCap)
																					NET_DW_PRINT_STRING_INT("HPV iActualCash exceeds Tunable, will clamp iActualCash= ", iActualCash)
																				#ENDIF
																				iActualCash = g_sMPTunables.iImportExportEarnCap
																			ENDIF
																		ENDIF
																		
																		#IF IS_DEBUG_BUILD
																			NET_DW_PRINT_STRING_FLOAT("RP Tunable... ", g_sMPTunables.fxp_tunable_ImportExport )
																		#ENDIF
																		iActualXp = 0
																		fXP *= g_sMPTunables.fxp_tunable_ImportExport
																		iActualXp = ROUND(fXP)
																		
																		CLEAR_BIT(iLocalImpExpBitSet, biL_InGarageInitialCheck)
																		#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_VEHICLE_DELIVERY] - Clear biL_InGarageInitialCheck - 1") NET_NL() #ENDIF
																		#IF IS_DEBUG_BUILD
																			INT iModel
																			iModel = ENUM_TO_INT(GET_ENTITY_MODEL(DeliveredCar))
																			NET_DW_PRINT("PLAYSTATS_IMPORT_EXPORT_MISSION_DONE Telemetry data...")
																			NET_DW_PRINT_STRING_INT("iActualXp = ", iActualXp)
																			NET_DW_PRINT_STRING_INT("iActualCash = ", iActualCash)
																			NET_DW_PRINT_STRING_INT("Model int = ", iModel)
																			NET_DW_PRINT_STRINGS("Model = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(DeliveredCar)))
																		#ENDIF
																		
																		vehLastDeliveredCar = DeliveredCar
																		mLastDeliveredCar = GET_ENTITY_MODEL(DeliveredCar)
																		IF NOT IS_BIT_SET(iLocalImpExpBitSet, biL_HelpDelSpecCar)
																			#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_VEHICLE_DELIVERY] PLAYSTATS_IMPORT_EXPORT_MISSION_DONE - not HPV") NET_NL() #ENDIF
																			PLAYSTATS_IMPORT_EXPORT_MISSION_DONE(FMMC_TYPE_IMPORT_EXPORT, iActualXp, iActualCash, DeliveredCar)
																		ELSE
																			#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_VEHICLE_DELIVERY] PLAYSTATS_IMPORT_EXPORT_MISSION_DONE - HPV") NET_NL() #ENDIF
																			PLAYSTATS_IMPORT_EXPORT_MISSION_DONE(FMMC_TYPE_IMPEXP_HPV, iActualXp, iActualCash, DeliveredCar)
																		ENDIF
																		

																		
																		RESET_REMINDER_TIMER_FOR_ACTIVITY(FMMC_TYPE_IMPORT_EXPORT) // For reminder phonecall
																		
																		SET_DELIVERED_CAR_TODAY()
																		
																		DELETE_IMPORT_EXPORT_TEXT_MESSAGES()
																	
																		#IF IS_DEBUG_BUILD
																			NET_DW_PRINT_STRING_INT("[UPDATE_VEHICLE_DELIVERY]  PLAYER_DELIVERY_STATE_WAITING_FOR_CAR Stack size...", GET_CURRENT_STACK_SIZE())
																		#ENDIF
																		#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing biL_InRecentlyDeliveredCar as car delivered") #ENDIF
																	ELSE
																		IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
																			NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_NO_COLLISION | NSPC_CLEAR_TASKS)
																		ENDIF
																	ENDIF
																ENDIF
															ENDIF	
																
														ELSE
															#IF IS_DEBUG_BUILD
																IF bWdUseDebug
																	NET_DW_PRINT("Car not in garage!")
																ENDIF
															#ENDIF
															//-- Not in garage anymore
															IF IS_BIT_SET(iLocalImpExpBitSet, biL_InGarageInitialCheck)
																CLEAR_BIT(iLocalImpExpBitSet, biL_InGarageInitialCheck)
																#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_VEHICLE_DELIVERY] - Clear biL_InGarageInitialCheck - 2") NET_NL() #ENDIF
															ENDIF
															
														ENDIF
													ENDIF
											//	ENDIF
											ELSE
												//-- Vehicle is rigged
												IF bDisplayImpExpBlip 
													#IF IS_DEBUG_BUILD NET_DW_PRINT("Removing impExp blip as veh is rigged") #ENDIF
													bDisplayImpExpBlip = FALSE
												ENDIF
												IF bPlayerInVehicleReadyForDelivery
													bPlayerInVehicleReadyForDelivery = FALSE
												ENDIF
												IF bDisplaySimeonObjective
													bDisplaySimeonObjective = FALSE
												ENDIF
												
												IF bDisplayLeaveCarmodObjective
													bDisplayLeaveCarmodObjective = FALSE
												ENDIF
												
												IF bDisplayLoseCopsObjective
													bDisplayLoseCopsObjective = FALSE
												ENDIF
											ENDIF
										ELSE
											//-- Not sprayed
											
											IF NOT IS_VEH_RIGGED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
												IF DOES_PLAYER_HAVE_ENOUGH_CASH_FOR_RESPRAY(iTempRepairCost)
													IF NOT HAS_VEHICLE_BEEN_BEEN_MODDED_BY_ANOTHER_PLAYER(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
														IF GET_PED_IN_VEHICLE_SEAT(vehCurrent) = PLAYER_PED_ID()
															IF vehLastNotSprayed <> vehCurrent	
																#IF IS_DEBUG_BUILD NET_DW_PRINT("Set vehLastNotSprayed = vehCurrent ") #ENDIF
																vehLastNotSprayed = vehCurrent
																iInitialRepairCostforVehicle = 0
															ELSE
																IF iInitialRepairCostforVehicle <> iTempRepairCost
																	IF iInitialRepairCostforVehicle < iTempRepairCost
																		iInitialRepairCostforVehicle = iTempRepairCost
																		#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT(" Setting iInitialRepairCostforVehicle = ", iInitialRepairCostforVehicle) #ENDIF
																	ENDIF
																ENDIF
															ENDIF
														ENDIF
														
														IF NOT bDisplayCarmodBlip
															bDisplayCarmodBlip = TRUE
														ENDIF
														
														IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
															IF IS_PLAYER_BROWSING_IN_ANY_CARMOD()
																IF bDisplayCarmodObjective
																	bDisplayCarmodObjective = FALSE
																ENDIF
																IF GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) = PLAYER_PED_ID()
																	IF GET_CARMOD_REPAIR_COST(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) > 0
																		IF NOT bDisplayFixCarObjective
																			bDisplayFixCarObjective = TRUE
																		ENDIF
																	ELSE
																		IF bDisplayFixCarObjective
																			bDisplayFixCarObjective = FALSE
																		//	#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_IMPEX_OBJECTIVE] bDisplayFixCarObjective - False 1") #ENDIF
																		ENDIF
																		IF NOT bDisplayResprayObjective													
																			bDisplayResprayObjective = TRUE
																		ENDIF
																	ENDIF
																ENDIF
															ENDIF
														ELSE	
															IF NOT IS_PLAYER_BROWSING_IN_ANY_CARMOD()
																IF bDisplayResprayObjective
																	bDisplayResprayObjective = FALSE
																ENDIF
																IF bDisplayFixCarObjective
																	bDisplayFixCarObjective = FALSE
																//	#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_IMPEX_OBJECTIVE] bDisplayFixCarObjective - False 2") #ENDIF
																ENDIF
																IF NOT bDisplayCarmodObjective
																	IF GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) = PLAYER_PED_ID()
																		bDisplayCarmodObjective = TRUE
																	ENDIF
																ENDIF
															ENDIF
														ENDIF
													ELSE
														#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_IMPEX_OBJECTIVE] CLEAR_ALL_OBJECTIVE_TEXT_FLAGS - Vehicle modded by another player") #ENDIF
														CLEAR_ALL_OBJECTIVE_TEXT_FLAGS()
													ENDIF
													
												ELSE
													//-- not enough cash
													CLEAR_ALL_OBJECTIVE_TEXT_FLAGS()
												//	#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_IMPEX_OBJECTIVE] CLEAR_ALL_OBJECTIVE_TEXT_FLAGS - False 1") #ENDIF
												ENDIF
											ELSE	
												//-- Rigged
												CLEAR_ALL_OBJECTIVE_TEXT_FLAGS()
											//	#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_IMPEX_OBJECTIVE] CLEAR_ALL_OBJECTIVE_TEXT_FLAGS - False 2") #ENDIF
												IF bPlayerInVehicleReadyForDelivery
													bPlayerInVehicleReadyForDelivery = FALSE
												ENDIF
											ENDIF
											
											
											#IF IS_DEBUG_BUILD
												IF bWdUseDebug
													NET_DW_PRINT("In Else 3")
												ENDIF
											#ENDIF
										ENDIF
									ELSE
										#IF IS_DEBUG_BUILD
											NET_DW_PRINT("Waiting for ABOUT_TO_SET_AS_PERSONAL_VEHICLE")
										#ENDIF	
									ENDIF
								ELSE
									//-- Got a wanted level
									
									//-- Need to deal with the player having a 1-star wanted rating prior to entering the wanted vehicle
									//-- Need to up to 2 stars
									vehCurrent = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
									IF vehLastWanted <> vehCurrent
										IF NOT bInWantedCarWhenListReceived 
											IF SHOULD_GIVE_WANTED_LEVEL_FOR_VEHICLE(vehCurrent)
												SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
												SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
												#IF IS_DEBUG_BUILD NET_DW_PRINT("Player had 1-star wanted rating prior to entering wanted vehicle. Upping to 2") #ENDIF
											ENDIF
										ENDIF
										vehLastWanted = vehCurrent
									ENDIF
									
									IF NOT bDisplayLoseCopsObjective
										
										CLEAR_ALL_OBJECTIVE_TEXT_FLAGS()
									//	#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_IMPEX_OBJECTIVE] CLEAR_ALL_OBJECTIVE_TEXT_FLAGS - False 3") #ENDIF
										#IF IS_DEBUG_BUILD NET_DW_PRINT("Set bDisplayLoseCopsObjective") #ENDIF
										bDisplayLoseCopsObjective = TRUE
									ENDIF
								ENDIF
							ELSE
								//-- Non player ped in vehicle
								#IF IS_DEBUG_BUILD
									IF bTimedDebug
										NET_DW_PRINT("[bTimedDebug] [UPDATE_VEHICLE_DELIVERY] Failed on non player ped in veh")
									ENDIF
								#ENDIF
							ENDIF

						ELSE
							#IF IS_DEBUG_BUILD
								IF bTimedDebug
									NET_DW_PRINT("[bTimedDebug] [UPDATE_VEHICLE_DELIVERY] Failed on IF (IsPlayerInWantedImportExportCar() OR IS_PLAYER_IN_SPECIFIC_IMPORT_EXPORT_VEHICLE() OR bPlayerInRecentlyDel)")
									IF NOT IsPlayerInWantedImportExportCar() 
										NET_DW_PRINT("[bTimedDebug] [UPDATE_VEHICLE_DELIVERY] Failed on IsPlayerInWantedImportExportCar(")
									ELIF NOT IS_PLAYER_IN_SPECIFIC_IMPORT_EXPORT_VEHICLE()
										NET_DW_PRINT("[bTimedDebug] [UPDATE_VEHICLE_DELIVERY] Failed on IS_PLAYER_IN_SPECIFIC_IMPORT_EXPORT_VEHICLE")
									ELIF NOT bPlayerInRecentlyDel
										NET_DW_PRINT("[bTimedDebug] [UPDATE_VEHICLE_DELIVERY] Failed on bPlayerInRecentlyDel")
									ENDIF
								ENDIF
							#ENDIF
							//-- Player not in a valid vehicle
							CLEAR_ALL_OBJECTIVE_TEXT_FLAGS()
						//	#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_IMPEX_OBJECTIVE] CLEAR_ALL_OBJECTIVE_TEXT_FLAGS - False 4") #ENDIF
							IF bPlayerInVehicleReadyForDelivery
								bPlayerInVehicleReadyForDelivery = FALSE
							ENDIF
							
							IF bInWantedCarWhenListReceived
								IF serverbd.bNewList
									IF bNewCarListThisSession
										#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing bInWantedCarWhenListReceived as not in a wanted ehicle - 1") #ENDIF
										bInWantedCarWhenListReceived = FALSE
									ENDIF
								ENDIF
							ENDIF
							#IF IS_DEBUG_BUILD
								IF bWdUseDebug
									NET_DW_PRINT("In Else 2")
								ENDIF
							#ENDIF
							
							IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("IMPEX_NOT_NEED")
								OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("IMPEX_NO_MORE")
								OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("IMPEX_NOT_PVEH")
									#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing help (IMPEX_NOT_NEED or IMPEX_NO_MORE or IMPEX_NOT_PVEH)") #ENDIF
									CLEAR_HELP()
								ENDIF
							ENDIF
							vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
							
							IF IS_VEHICLE_A_PERSONAL_VEHICLE(vehTemp)
								IF GET_PED_IN_VEHICLE_SEAT(vehTemp, VS_DRIVER) = PLAYER_PED_ID()
									IF DECOR_EXIST_ON(vehTemp, "Player_Vehicle")
										IF DECOR_GET_INT(vehTemp, "Player_Vehicle") <> NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID())
									//	OR SHOULD_DISPLAY_OBJECTIVE_TEXT() // Need to deal with objective text being cleared
										OR vehTemp = vehLastWanted	
											//-- Not my personal vehicle
											IF IS_VEHICLE_DRIVEABLE(vehTemp)
												IF IS_OK_TO_PRINT_HELP()
													IF NOT IS_BIT_SET(iLocalImpExpBitSet, biL_PlayerInPersonalVehicle)
														IF bPlayerInRecentlyDel
														OR IS_MODEL_ON_WANTED_LIST(ThisCarList, GET_ENTITY_MODEL(vehTemp))
															
															PRINT_HELP("IMPEX_NOT_PVEH") // Simeon doesn't want Personal Vehicles.
															SET_BIT(iLocalImpExpBitSet, biL_PlayerInPersonalVehicle)
															#IF IS_DEBUG_BUILD NET_DW_PRINT("Setting biL_PlayerInPersonalVehicle as in a personal vehicle") #ENDIF
															
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ELSE
											IF IS_BIT_SET(iLocalImpExpBitSet, biL_PlayerInPersonalVehicle)
												IF vehTemp <> vehLastWanted	
												
													#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing biL_PlayerInPersonalVehicle as vehTemp <> vehLastWanted - 1	") #ENDIF
													CLEAR_BIT(iLocalImpExpBitSet, biL_PlayerInPersonalVehicle)
												ENDIF
											ENDIF
											
											
										ENDIF
									ELSE
										#IF IS_DEBUG_BUILD NET_DW_PRINT("In personal vehicle, but decor doesn't exist!") #ENDIF
									ENDIF
								ENDIF
							ELSE	
								IF IS_BIT_SET(iLocalImpExpBitSet, biL_PlayerInPersonalVehicle)
									IF vehTemp <> vehLastWanted	
									
										#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing biL_PlayerInPersonalVehicle as vehTemp <> vehLastWanted - 2	") #ENDIF
										CLEAR_BIT(iLocalImpExpBitSet, biL_PlayerInPersonalVehicle)
									ENDIF
								ENDIF
											
								IF bAllowUnwantedInGarage
									IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									AND IS_PLAYER_IN_OPENING_RANGE_OF_GARAGE(MP_GAR_SIMEON)
										vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
										
										IF IsCarInContactGarage(vehTemp)
											IF NOT bWarpDisabled
												DISABLE_WARP_OUT_OF_LOCKED_GARAGE(TRUE)	
												bWarpDisabled = TRUE
												#IF IS_DEBUG_BUILD NET_DW_PRINT("Disabling warp out garage becuase I'm not in a wanted car, but I'm in the garage - Check 2") #ENDIF
											ENDIF
											bRequestGarageOpen = TRUE
										ENDIF	
											
										
									ELSE	
										IF (bWarpDisabled)
											DISABLE_WARP_OUT_OF_LOCKED_GARAGE(FALSE)	
											bWarpDisabled = FALSE	
											bRequestGarageOpen = FALSE
											#IF IS_DEBUG_BUILD NET_DW_PRINT("Enabling warp out garage becuase I'm not in a wanted car, and I'm not in the garage - Check 2 ") #ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							
							IF IS_PLAYER_IN_OPENING_RANGE_OF_GARAGE(MP_GAR_SIMEON)
								// HELP_NOT_NEEDED
								// HELP_PERSONAL_VEH
								IF IS_OK_TO_PRINT_HELP() 
									IF IS_VEHICLE_A_PERSONAL_VEHICLE(vehTemp)
										IF IS_VEHICLE_DRIVEABLE(vehTemp)
											IF NOT IS_BIT_SET(iBS_Help, HELP_PERSONAL_VEH)
												PRINT_HELP("IMPEX_NOT_PVEH") // Simeon doesn't want Personal Vehicles.
												SET_BIT(iBS_Help, HELP_PERSONAL_VEH)
											ENDIF
										ENDIF
									ELSE
										IF IS_MODEL_ON_WANTED_LIST(ThisCarList, GET_ENTITY_MODEL(vehTemp))
											IF DOES_VEHICLE_HAVE_CREW_EMBLEM(vehTemp)
												IF NOT IS_BIT_SET(iBS_Help, HELP_CREW_EMBLEM)
													PRINT_HELP("IMPEX_NOT_CREW") // Simeon doesn't want vehicles with Crew emblems applied
													SET_BIT(iBS_Help, HELP_CREW_EMBLEM)
													#IF IS_DEBUG_BUILD NET_DW_PRINT(" Doing SImeon doesn't want crew emblem help text") #ENDIF
												ENDIF
											ENDIF
										ELSE
											IF NOT IS_BIT_SET(iBS_Help, HELP_NOT_NEEDED)
												IF GET_PED_IN_VEHICLE_SEAT(vehTemp, VS_DRIVER) = PLAYER_PED_ID()
													PRINT_HELP("IMPEX_NOT_NEED") // Simeon doesn't need this vehicle.
													SET_BIT(iBS_Help, HELP_NOT_NEEDED)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									
									
								ENDIF
							ELSE 
								IF IS_BIT_SET(iBS_Help, HELP_NOT_NEEDED)
									CLEAR_BIT(iBS_Help, HELP_NOT_NEEDED)
								ENDIF
								
								IF IS_BIT_SET(iBS_Help, HELP_PERSONAL_VEH)
									CLEAR_BIT(iBS_Help, HELP_PERSONAL_VEH)
								ENDIF
								IF IS_BIT_SET(iBS_Help, HELP_CREW_EMBLEM)
									CLEAR_BIT(iBS_Help, HELP_CREW_EMBLEM)
								ENDIF
								
								IF IS_BIT_SET(iLocalImpExpBitSet, biL_InRecentlyDeliveredCar) //-- delivery window has expired
								//	#IF IS_DEBUG_BUILD BREAK_ON_NATIVE_COMMAND("Clear Help", FALSE) #ENDIF
									IF IS_OK_TO_PRINT_HELP()
										PRINT_HELP("IMPEX_NO_MORE") // Simeon doesn't need this vehicle anymore
										CLEAR_BIT(iLocalImpExpBitSet, biL_InRecentlyDeliveredCar)
										Clear_Any_Objective_Text_From_This_Script()
										#IF IS_DEBUG_BUILD 
											NET_DW_PRINT_STRINGS("Clearing biL_InRecentlyDeliveredCar as time expired. I'm in this vehicle ",
																GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))) 
											NET_DW_PRINT("Clearing objective text as recently delivbered time expired")
										#ENDIF
									ENDIF
									
								ENDIF
							ENDIF
						ENDIF
					ELSE	
						//-- pLayer not sat in a vehicle
						CLEAR_ALL_OBJECTIVE_TEXT_FLAGS()
					//	#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_IMPEX_OBJECTIVE] CLEAR_ALL_OBJECTIVE_TEXT_FLAGS - False 5") #ENDIF
						IF bPlayerInVehicleReadyForDelivery
							bPlayerInVehicleReadyForDelivery = FALSE
						ENDIF
						
						
						IF bInWantedCarWhenListReceived
							#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing bInWantedCarWhenListReceived as not in any vehicle ") #ENDIF
							bInWantedCarWhenListReceived = FALSE
						ENDIF
//						IF IS_BIT_SET(iLocalImpExpBitSet, biL_PlayerInPersonalVehicle)
//							#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing biL_PlayerInPersonalVehicle as not in a car") #ENDIF
//							CLEAR_BIT(iLocalImpExpBitSet, biL_PlayerInPersonalVehicle)
//						ENDIF
					/*
						Deal with player diving out car?
						Will get complicated if we have to track multiple people diving out car.
					
					*/
					//	IF IS_MY_MOST_RECENT_CAR_IN_GARAGE()
					//		vehLastDeliveredCar = DeliveredCar
					//		#IF IS_DEBUG_BUILD NET_DW_PRINT("I'm not in a car, but my most recent car is in the garage!") #ENDIF
					//	ENDIF
					ENDIF
			//	ENDIF
							
			BREAK
			
			CASE PLAYER_DELIVERY_WAIT_TO_BE_WARPED_FROM_GARAGE
				IF IS_NET_PLAYER_OK(PLAYER_ID())
					IF NOT SCRIPT_CHECK_IS_ENTITY_FULLY_IN_GARAGE(GET_PLAYER_PED(PLAYER_ID()),MP_GAR_SIMEON, SyncDelayTimerGarage)
						PlayerBD[PARTICIPANT_ID_TO_INT()].iDeliveryState = PLAYER_DELIVERY_STATE_TAKE_DELIVERY
						#IF IS_DEBUG_BUILD
							NET_DW_PRINT_STRING_INT("[UPDATE_VEHICLE_DELIVERY]  PLAYER_DELIVERY_WAIT_TO_BE_WARPED_FROM_GARAGE Stack size...", GET_CURRENT_STACK_SIZE())
						#ENDIF
						#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_VEHICLE_DELIVERY] iDeliveryState PLAYER_DELIVERY_WAIT_TO_BE_WARPED_FROM_GARAGE > PLAYER_DELIVERY_STATE_TAKE_DELIVERY") #ENDIF
					ENDIF
				ELSE
					CLEAR_BIT(iLocalImpExpBitSet, biL_HelpDelSpecCar)
					CLEAR_BIT(iLocalImpExpBitSet, biL_InVehicleOnList)
					PlayerBD[PARTICIPANT_ID_TO_INT()].iDeliveryState = PLAYER_DELIVERY_STATE_WAITING_FOR_CAR	
					#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_VEHICLE_DELIVERY] iDeliveryState PLAYER_DELIVERY_WAIT_TO_BE_WARPED_FROM_GARAGE > PLAYER_DELIVERY_STATE_WAITING_FOR_CAR") #ENDIF
				ENDIF
			BREAK
			
			CASE PLAYER_DELIVERY_STATE_TAKE_DELIVERY
			//	IF NOT IsListIDOnList(PlayerBD[PARTICIPANT_ID_TO_INT()].iVehicleListID , serverbd.CarList, TRUE)
					PlayerBD[PARTICIPANT_ID_TO_INT()].iDeliveryState = PLAYER_DELIVERY_STATE_RESTORE
					#IF IS_DEBUG_BUILD
						NET_DW_PRINT_STRING_INT("[UPDATE_VEHICLE_DELIVERY]  PLAYER_DELIVERY_STATE_RESTORE Stack size...", GET_CURRENT_STACK_SIZE())
					#ENDIF
					#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_VEHICLE_DELIVERY] iDeliveryState PLAYER_DELIVERY_STATE_TAKE_DELIVERY > PLAYER_DELIVERY_STATE_RESTORE") #ENDIF
			//	ENDIF
			BREAK

			
			CASE PLAYER_DELIVERY_STATE_RESTORE
				IF IS_SCREEN_FADED_IN()
					
					#IF IS_DEBUG_BUILD
						NET_DW_PRINT_STRING_INT("[UPDATE_VEHICLE_DELIVERY] Start PLAYER_DELIVERY_STATE_RESTORE Stack size...", GET_CURRENT_STACK_SIZE())
					#ENDIF
					// increase stat

					INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_CARS_EXPORTED, 1)	
					
					
					
					IF IS_BIT_SET(iLocalImpExpBitSet, biL_HelpDelSpecCar)
						SET_BIT(playerbd[PARTICIPANT_ID_TO_INT()].iImportExportBitSet, biP_DeliveredSpecVehicle)
						REQUEST_SYSTEM_ACTIVITY_TYPE_DONE_DELIVER()
					ELSE
						//-- Not an HPV
						REQUEST_SYSTEM_ACTIVITY_TYPE_DONE_IMPEXPORT()
					ENDIF
					
					// send a ticker to everyone else (only driver sends the ticker)
					IF (bIAmDriver)
						
						#IF IS_DEBUG_BUILD
							NET_DW_PRINT("[UPDATE_VEHICLE_DELIVERY] bIAmDriver is true!")
						#ENDIF
						IF IS_BIT_SET(iLocalImpExpBitSet, biL_InVehicleOnList)
							TickerData.TickerEvent = TICKER_EVENT_DELIVED_IMPORT_EXPORT_CAR
						ELSE
							TickerData.TickerEvent = TICKER_EVENT_HIGH_PRIORITY_IMPORT_EXPORT_CAR
							#IF IS_DEBUG_BUILD NET_DW_PRINT("Sending alt ticker as model not on list!") #ENDIF
						ENDIF
						TickerData.dataInt = PlayerBD[PARTICIPANT_ID_TO_INT()].iVehicleListID							
						TickerData.playerID = PlayerInCar[0]
						TickerData.playerID2 = PlayerInCar[1]
						TickerData.playerID3 = PlayerInCar[2]
						TickerData.playerID4 = PlayerInCar[3]
						
						IF NOT IS_BIT_SET(iLocalImpExpBitSet, biL_InVehicleOnList)
						//	BROADCAST_TICKER_EVENT(TickerData,ALL_PLAYERS_ON_SCRIPT(TRUE)) //ALL_PLAYERS_IN_TEAM
						ENDIF
						
					ELSE
						#IF IS_DEBUG_BUILD
							NET_DW_PRINT("[UPDATE_VEHICLE_DELIVERY] bIAmDriver is False!")
						#ENDIF
					ENDIF
					
					
					CLEAR_BIT(iLocalImpExpBitSet, biL_InVehicleOnList)
					
					// give cash to player
					//GIVE_LOCAL_PLAYER_CnC_CASH(FLOOR(fCash))
					
					//PRINT_WITH_NUMBER_NOW("IMPEX_PASS", FLOOR(fCash), 7000, 0)	
				//	FLOAT fActualCash 
					
					
				//	fActualCash = TO_FLOAT(iTotalCashValue - iTotalDamage)
				//	iActualCash = 100 * ROUND(fCash / 100.0)
					
					#IF IS_DEBUG_BUILD 
						NET_DW_PRINT_STRING_FLOAT("fCash ... ", fCash )
						NET_DW_PRINT_STRING_INT("iActualCash ... ", iActualCash ) 
					#ENDIF
					

					
					SET_LAST_JOB_DATA(LAST_JOB_IMPORT_EXPORT, iActualCash) // For share money in interaction menu
					
					PRINTLN(" IMP_EXP [UPDATE_VEHICLE_DELIVERY] - [MAGNATE_GANG_BOSS] - Cash before boss cut: $", iActualCash)
					GB_HANDLE_GANG_BOSS_CUT(iActualCash)
					PRINTLN(" IMP_EXP [UPDATE_VEHICLE_DELIVERY] - [MAGNATE_GANG_BOSS] - Cash after boss cut: $", iActualCash)
				//	NETWORK_EARN_FROM_IMPORT_EXPORT(iActualCash, GET_ENTITY_MODEL(vehLastDeliveredCar))
					INT iScriptTransactionIndex
					
					IF iActualCash > 0
						IF USE_SERVER_TRANSACTIONS()
							TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_IMPORT_EXPORT, iActualCash, iScriptTransactionIndex, DEFAULT, DEFAULT)
							g_cashTransactionData[iScriptTransactionIndex].cashInfo.mModelName = mLastDeliveredCar
						ELSE
							NETWORK_EARN_FROM_IMPORT_EXPORT(iActualCash, mLastDeliveredCar)
						ENDIF
					ENDIF
					
					#IF IS_DEBUG_BUILD 
						
					//	MODEL_NAMES mLastDelivered
					//	mLastDelivered =  GET_ENTITY_MODEL(vehLastDeliveredCar)
						
						NET_DW_PRINT_STRING_INT("NETWORK_EARN_FROM_IMPORT_EXPORT iActualCash ... ", iActualCash ) 
						IF mLastDeliveredCar <> DUMMY_MODEL_FOR_SCRIPT
							NET_DW_PRINT_STRINGS("NETWORK_EARN_FROM_IMPORT_EXPORT vehLastDeliveredCar Model = ",  GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mLastDeliveredCar))
						ENDIF
					#ENDIF

					IF (iTotalDamage > 0)
						PRINT_WITH_NUMBER_NOW("IMPEX_PASS", iActualCash, 7000, 0) // 852383
						//PRINT_WITH_2_NUMBERS_NOW("IMPEX_PASS_2", (iTotalCashValue  - iTotalDamage) , iTotalDamage, 7000, 0)
					ELSE
						PRINT_WITH_NUMBER_NOW("IMPEX_PASS", iActualCash, 7000, 0)
					ENDIF
					
					
				//	iActualXp = ROUND(fXP)
					
					// give xp to player
					//GIVE_LOCAL_PLAYER_XP("XPT_IMPEXP",MAX_INT(1000, MIN_INT(100, 50 *GET_MP_INT_CHARACTER_AWARD(MP_AWARD_CARS_EXPORTED))))
					
					#IF IS_DEBUG_BUILD 
						NET_DW_PRINT_STRING_FLOAT("fXP ... ", fXP )
						NET_DW_PRINT_STRING_INT("iActualXp ... ", iActualXp ) 
					#ENDIF
					
					GIVE_LOCAL_PLAYER_XP(eXPTYPE_CO_OP_AMBIENTFM, "XPT_IMPEXP",XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_IMPORT_EXPORT_DELIVERY, iActualXp)
					
					//-- For Martin's daily objective stuff.
					SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_DELIVER_VEHICLE)
					
					// give player control back 
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)					
										
					// reset
					PlayerBD[PARTICIPANT_ID_TO_INT()].iDeliveryState = PLAYER_DELIVERY_STATE_WAITING_FOR_CAR
					PlayerBD[PARTICIPANT_ID_TO_INT()].iVehicleListID = -1
					CLEAR_BIT(iLocalImpExpBitSet, biL_HelpDelSpecCar)
					
					bSendThanksTextMessage = TRUE
					
					REQUEST_SAVE(SSR_REASON_FM_IMPORT_EXPORT, STAT_SAVETYPE_END_MISSION)
					#IF IS_DEBUG_BUILD
						NET_DW_PRINT_STRING_INT("[UPDATE_VEHICLE_DELIVERY] End PLAYER_DELIVERY_STATE_RESTORE Stack size...", GET_CURRENT_STACK_SIZE())
					#ENDIF
					#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_VEHICLE_DELIVERY] iDeliveryState PLAYER_DELIVERY_STATE_RESTORE > PLAYER_DELIVERY_STATE_WAITING_FOR_CAR") #ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		
		
		
	ELSE
		
		#IF IS_DEBUG_BUILD
			IF bWdUseDebug
			OR bTimedDebug
				NET_DW_PRINT("In Else 1 / not ok to deliver cars")
			ENDIF
		#ENDIF
		// is player somehow in the garage? 
		
		
		IF NOT IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())
			IF NOT bAllowUnwantedInGarage
				IF (bWarpDisabled)
					DISABLE_WARP_OUT_OF_LOCKED_GARAGE(FALSE)	
					bWarpDisabled = FALSE	
				ENDIF 
			ELSE
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				AND IS_PLAYER_IN_OPENING_RANGE_OF_GARAGE(MP_GAR_SIMEON)
					vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					
					IF IsCarInContactGarage(vehTemp)
						IF NOT bWarpDisabled
							DISABLE_WARP_OUT_OF_LOCKED_GARAGE(TRUE)	
							bWarpDisabled = TRUE
							#IF IS_DEBUG_BUILD NET_DW_PRINT("Disabling warp out garage becuase I'm not in a wanted car, but I'm in the garage - Check 1") #ENDIF
						ENDIF
						bRequestGarageOpen = TRUE
						
					ENDIF
					
				ELSE	
					IF (bWarpDisabled)
						DISABLE_WARP_OUT_OF_LOCKED_GARAGE(FALSE)	
						bWarpDisabled = FALSE	
						bRequestGarageOpen = FALSE
						#IF IS_DEBUG_BUILD NET_DW_PRINT("Enabling warp out garage becuase I'm not in a wanted car, and I'm not in the garage - Check 1 ") #ENDIF
					ENDIF
				ENDIF
			ENDIF

		ENDIF
		
		IF bDisplayCarmodBlip
			bDisplayCarmodBlip = FALSE
		ENDIF
		IF bDisplayCarmodObjective
			bDisplayCarmodObjective = FALSE
		ENDIF
		IF bDisplayResprayObjective
			bDisplayResprayObjective = FALSE
		ENDIF
		IF bDisplayFixCarObjective
			bDisplayFixCarObjective = FALSE
		//	#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_IMPEX_OBJECTIVE] bDisplayFixCarObjective - False 3") #ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF bTimedDebug
			NET_DW_PRINT("[bTimedDebug] [UPDATE_VEHICLE_DELIVERY] ...Done")
		ENDIF
	#ENDIF
ENDPROC

PROC MAINTAIN_BONUS_XP()
	//--Bonus xp for all cars delievered
	IF NOT IS_BIT_SET(iLocalImpExpBitSet, biL_BonusXpFirstNewListCheck)
		//-- Don't want to count the first board we get sent. 
		IF IS_BIT_SET(iLocalImpExpBitSet, biL_SentInitialText)
			IF NOT serverBD.bNewList
				SET_BIT(iLocalImpExpBitSet, biL_BonusXpFirstNewListCheck)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[MAINATIN_BONUS_XP] Set") #ENDIF
			ENDIF
		ENDIF
	ELSE
		//-- Been sent at least one board
		IF NOT IS_BIT_SET(iLocalImpExpBitSet, biL_AllCompleteXpBonus)
			IF serverBD.bNewList
				IF NOT bInWantedCarWhenListReceived
					bInWantedCarWhenListReceived = TRUE
					#IF IS_DEBUG_BUILD NET_DW_PRINT("Set bInWantedCarWhenListReceived") #ENDIF
				ENDIF
				IF IS_NET_PLAYER_OK(PLAYER_ID())
					IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
						#IF IS_DEBUG_BUILD NET_DW_PRINT("[MAINATIN_BONUS_XP] Awarding bonus Xp as board complete") #ENDIF
				//		GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "XPT_IMPEXP",XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_IMPORT_EXPORT_BOARD ,250)
						SET_BIT(iLocalImpExpBitSet, biL_AllCompleteXpBonus)
					ELSE	
						#IF IS_DEBUG_BUILD NET_DW_PRINT("[MAINATIN_BONUS_XP] serverBD.bNewList was set, but I'm on a mission") #ENDIF
						SET_BIT(iLocalImpExpBitSet, biL_AllCompleteXpBonus)
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD NET_DW_PRINT("[MAINATIN_BONUS_XP] serverBD.bNewList was set, but I'm not ok") #ENDIF
					SET_BIT(iLocalImpExpBitSet, biL_AllCompleteXpBonus)
				ENDIF
			ENDIF
			
		ELSE
			IF NOT serverBD.bNewList
				CLEAR_BIT(iLocalImpExpBitSet, biL_AllCompleteXpBonus)
			ENDIF
		ENDIF
	ENDIF
ENDPROC
/*
PROC UPDATE_CAR_GENS()
	
	IF NOT bSetUpCarGens
		IF serverBD.iCarListState = LIST_STATE_RUNNING
			CreateCarGens()
			bSetUpCarGens = TRUE
			#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_CAR_GENS] Car gens updated...") #ENDIF
		ENDIF 
	ELSE
		IF serverBD.iCarListState = LIST_STATE_WAITING_TO_CLEAR
			bSetUpCarGens = FALSE
			#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_CAR_GENS] Reset...") #ENDIF
		ENDIF
	ENDIF 
ENDPROC
*/

PROC UPDATE_GARAGE_DOORS()
	// deal with opening and closing of the garage door
	IF (bRequestGarageOpen)
		IF NOT (bGarageStateOpen)
			REQUEST_GARAGE_OPEN(MP_GAR_SIMEON,TRUE)
			DISABLE_WARP_OUT_OF_LOCKED_GARAGE(TRUE)
			bGarageStateOpen = TRUE
			#IF IS_DEBUG_BUILD NET_DW_PRINT("Requested garage open") #ENDIF
		ENDIF
	ELSE
		IF (bGarageStateOpen)
			REQUEST_GARAGE_OPEN(MP_GAR_SIMEON,FALSE)	
			DISABLE_WARP_OUT_OF_LOCKED_GARAGE(FALSE) 
			bGarageStateOpen = FALSE
			#IF IS_DEBUG_BUILD NET_DW_PRINT("Cleared garage open request") #ENDIF
		ENDIF
	ENDIF	
ENDPROC

FUNC BLIP_INDEX GET_BLIP_FOR_CARMOD(SHOP_NAME_ENUM shopCarMod)
	BLIP_INDEX blipToUse
	SWITCH shopCarMod
		CASE CARMOD_SHOP_01_AP
			blipToUse =  g_sShopSettings.blipID[CARMOD_SHOP_01_AP]
		BREAK

		CASE CARMOD_SHOP_05_ID2
			blipToUse =  g_sShopSettings.blipID[CARMOD_SHOP_05_ID2]
		BREAK
		
		CASE CARMOD_SHOP_06_BT1			// Car Mod - AMB3 (v_carmod)
			blipToUse =  g_sShopSettings.blipID[CARMOD_SHOP_06_BT1]
		BREAK
		
		CASE CARMOD_SHOP_07_CS1			// Car Mod - AMB4 (v_carmod3)
			blipToUse =  g_sShopSettings.blipID[CARMOD_SHOP_07_CS1]
		BREAK
		
		CASE CARMOD_SHOP_08_CS6			// Car Mod - AMB5 (v_carmod3)
			blipToUse =  g_sShopSettings.blipID[CARMOD_SHOP_08_CS6]
		BREAK
		
		CASE CARMOD_SHOP_SUPERMOD			// Car Mod - AMB6 (lr_supermod_int)
			blipToUse =  g_sShopSettings.blipID[CARMOD_SHOP_SUPERMOD]
		BREAK
		
		DEFAULT
			NET_SCRIPT_ASSERT("GET_BLIP_FOR_CARMOD Didn't get carmod blip!")
		BREAK
	ENDSWITCH
	
	RETURN blipToUse
ENDFUNC


FUNC BOOL IS_CAR_MOD_SHOP_VALID(SHOP_NAME_ENUM shopCarMod)
	
	BOOL bValid
	SWITCH shopCarMod
		CASE CARMOD_SHOP_01_AP
			bValid =  DOES_BLIP_EXIST(g_sShopSettings.blipID[CARMOD_SHOP_01_AP])
		BREAK

		CASE CARMOD_SHOP_05_ID2
			bValid =  DOES_BLIP_EXIST(g_sShopSettings.blipID[CARMOD_SHOP_05_ID2])
		//	bValid =  DOES_BLIP_EXIST(g_blip_PayNSpray[1])
		BREAK
		
		CASE CARMOD_SHOP_06_BT1			// Car Mod - AMB3 (v_carmod)
			bValid =  DOES_BLIP_EXIST(g_sShopSettings.blipID[CARMOD_SHOP_06_BT1])
		BREAK
		
		CASE CARMOD_SHOP_07_CS1			// Car Mod - AMB4 (v_carmod3)
			bValid =  DOES_BLIP_EXIST(g_sShopSettings.blipID[CARMOD_SHOP_07_CS1])
		BREAK
		
		CASE CARMOD_SHOP_08_CS6			// Car Mod - AMB5 (v_carmod3)
			bValid =  DOES_BLIP_EXIST(g_sShopSettings.blipID[CARMOD_SHOP_08_CS6])
		BREAK
		
		CASE CARMOD_SHOP_SUPERMOD		// Car Mod - AMB6 (lr_supermod_int)
			bValid =  DOES_BLIP_EXIST(g_sShopSettings.blipID[CARMOD_SHOP_SUPERMOD])
		BREAK
			
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
//		NET_DW_PRINT_STRING_INT("IS_CAR_MOD_SHOP_VALID... Mod shop not valid! Shop int... ", ENUM_TO_INT(shopCarMod))
	#ENDIF
	
	RETURN bValid
ENDFUNC



FUNC SHOP_NAME_ENUM GET_IMP_CLOSEST_CARMOD(VECTOR vCoords, SHOP_TYPE_ENUM eType = SHOP_TYPE_CARMOD, INT iRange = -1)

	INT iCount
	FLOAT fCurrentDist
	FLOAT fShortDist = 999999.99
	SHOP_NAME_ENUM eShop = EMPTY_SHOP
	SHOP_NAME_ENUM carMod
	FOR iCount = 0 TO NUMBER_OF_SHOPS-1
		IF eType = SHOP_TYPE_EMPTY
		OR eType = GET_SHOP_TYPE_ENUM(INT_TO_ENUM(SHOP_NAME_ENUM, iCount))	
			carMod = INT_TO_ENUM(SHOP_NAME_ENUM, iCount)
			IF IS_CAR_MOD_SHOP_VALID(carMod)
		//	IF carMod = CARMOD_SHOP_01_AP
		//	OR carMod = CARMOD_SHOP_05_ID2
				fCurrentDist = GET_DISTANCE_BETWEEN_COORDS(vCoords, GET_SHOP_COORDS(INT_TO_ENUM(SHOP_NAME_ENUM, iCount)))
				IF fCurrentDist < fShortDist
				AND (fCurrentDist <= iRange OR iRange = -1)
				//	#IF IS_DEBUG_BUILD NET_DW_PRINT("Found A car mod") #ENDIF
				//	IF DOES_BLIP_EXIST(g_sShopSettings.blipID[INT_TO_ENUM(SHOP_NAME_ENUM, iCount)])
				//		#IF IS_DEBUG_BUILD NET_DW_PRINT("Found A car mod with a blip") #ENDIF
						fShortDist = fCurrentDist
						eShop = INT_TO_ENUM(SHOP_NAME_ENUM, iCount)
				//	ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN eShop
ENDFUNC

FUNC BOOL SHOULD_IMPEXP_BLIP_FLASH()
	BOOL bSHouldFlash
	INT iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_IMP_EXP) 
	IF NOT IS_BIT_SET(iStatInt, BI_IMPEXP_FLASHED_BLIP_ONCE)
		bSHouldFlash = TRUE
		#IF IS_DEBUG_BUILD NET_DW_PRINT("[SHOULD_IMPEXP_BLIP_FLASH] True because BI_IMPEXP_FLASHED_BLIP_ONCE") #ENDIF
	ELIF NOT IS_BIT_SET(iStatInt, BI_IMPEXP_FLASHED_BLIP_TWICE)
		bSHouldFlash = TRUE
		#IF IS_DEBUG_BUILD NET_DW_PRINT("[SHOULD_IMPEXP_BLIP_FLASH] True because BI_IMPEXP_FLASHED_BLIP_TWICE") #ENDIF
	ENDIF
	
	RETURN bSHouldFlash
ENDFUNC




PROC UPDATE_IMPEX_BLIP()
	

	// deal with the blip
	IF (bDisplayImpExpBlip)
		IF NOT DOES_BLIP_EXIST(g_blipSimeonGarage)			
			
			g_blipSimeonGarage = ADD_BLIP_FOR_COORD(g_vSimeonGarage) //vAvPos <<1204.2096, -3107.2524, 4.5502>>
			
			SET_BLIP_SPRITE(g_blipSimeonGarage, RADAR_TRACE_SIMEON_FAMILY) // 

			SET_BLIP_SCALE(g_blipSimeonGarage, BLIP_SIZE_NETWORK_COORD)	
			SET_BLIP_AS_SHORT_RANGE(g_blipSimeonGarage, FALSE)
			SET_BLIP_NAME_FROM_TEXT_FILE(g_blipSimeonGarage, "IMPEX_BLIP_FM")
			IF SHOULD_IMPEXP_BLIP_FLASH()
				SET_BLIP_FLASHES(g_blipSimeonGarage, TRUE)
			ENDIF
		ENDIF
		
	ELSE
		IF DOES_BLIP_EXIST(g_blipSimeonGarage)
			
			REMOVE_BLIP(g_blipSimeonGarage)
			IF bCarWrecked
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF NOT SCRIPT_CHECK_IS_ENTITY_FULLY_IN_GARAGE(GET_PLAYER_PED(PLAYER_ID()), MP_GAR_SIMEON , SyncDelayTimerGarage)
							#IF IS_DEBUG_BUILD NET_DW_PRINT("About to do car wrecked help") #ENDIF
							IF SHOULD_HIDE_LOCAL_UI()
								PRINT_HELP("IMPEX_HIPR_DAM") //This vehicle is no longer required  as it's too damaged.
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		

	ENDIF
	
	IF bDisplayCarmodBlip
		IF shopClosestCarMod = EMPTY_SHOP
			shopClosestCarMod = GET_IMP_CLOSEST_CARMOD(GET_ENTITY_COORDS(PLAYER_PED_ID()))
			IF shopClosestCarMod <> EMPTY_SHOP
			//	IF IS_CAR_MOD_SHOP_VALID(shopClosestCarMod)
					#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("Setting blip long range.. ", ENUM_TO_INT(shopClosestCarMod)) #ENDIF
					SET_CAR_MOD_BLIP_LONG_RANGE(shopClosestCarMod)
					
				//ENDIF
			ENDIF
			
			
		ENDIF
		
	ELSE
		IF shopClosestCarMod <> EMPTY_SHOP
			#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("UPDATE_IMPEX_BLIP Clearing SET_SHOP_BLIP_LONG_RANGE for shop ", ENUM_TO_INT(shopClosestCarMod)) #ENDIF
			SET_CAR_MOD_BLIP_LONG_RANGE(shopClosestCarMod, FALSE)
			
			shopClosestCarMod = EMPTY_SHOP
			
			
		ENDIF
	ENDIF
			
ENDPROC

FUNC BOOL SHOULD_HIDE_TEXT_BECAUSE_OF_FM_EVENT()
	IF NOT FM_EVENT_HAS_PLAYER_HAS_BLOCKED_CURRENT_FM_EVENT(PLAYER_ID())
		IF IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
		OR FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID()) != FMMC_TYPE_INVALID
			RETURN TRUE
		ENDIF
		IF FM_EVENT_IS_PLAYER_WAITING_FOR_FM_EVENT_TO_START(PLAYER_ID())
		AND g_FM_EVENT_VARS.iFmAmbientEventCountDownType = FMMC_TYPE_CHALLENGES
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_HIDE_TEXT_BECAUSE_OF_ANY_EVENT()
	IF SHOULD_HIDE_TEXT_BECAUSE_OF_FM_EVENT()
		RETURN TRUE
	ENDIF
	
	IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_CHALLENGE(PLAYER_ID(), TRUE)
		#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_IMPEX_OBJECTIVE] [SHOULD_HIDE_TEXT_BECAUSE_OF_ANY_EVENT] TRUE AS GB_IS_PLAYER_ON_ANY_GANG_BOSS_CHALLENGE") #ENDIF
		RETURN TRUE
	ENDIF
	
	IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID(), TRUE)
		#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_IMPEX_OBJECTIVE] [SHOULD_HIDE_TEXT_BECAUSE_OF_ANY_EVENT] TRUE AS GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION") #ENDIF
		RETURN TRUE
	ENDIF
	 //FEATURE_GANG_BOSS
	
	RETURN FALSE
ENDFUNC

PROC UPDATE_IMPEX_OBJECTIVE()
	IF bDisplayCarmodObjective
	OR bDisplaySimeonObjective
	OR bDisplayResprayObjective
	OR bDisplayLeaveCarmodObjective
	OR bDisplayFixCarObjective
	OR bDisplayLoseCopsObjective
		IF MPGlobalsAmbience.R2Pdata.bOnRaceToPoint
		OR IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
		OR IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
		OR NOT (IS_SKYSWOOP_AT_GROUND())
		OR SHOULD_HIDE_TEXT_BECAUSE_OF_ANY_EVENT()
		OR SHOULD_HIDE_LOCAL_UI()
			bDisplayCarmodObjective = FALSE
			bDisplayResprayObjective = FALSE
			bDisplaySimeonObjective = FALSE
			bDisplayLeaveCarmodObjective = FALSE
			bDisplayFixCarObjective = FALSE
		//	#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_IMPEX_OBJECTIVE] bDisplayFixCarObjective - False 4") #ENDIF
			Clear_Any_Objective_Text_From_This_Script()
		ENDIF
	ENDIF
	
	IF Is_This_The_Current_Objective_Text("FM_CTUT_RSP")
	OR Is_This_The_Current_Objective_Text("FM_CTUT_REP")
	OR Is_This_The_Current_Objective_Text("FM_CTUT_LLS")
		IF NOT IS_CUSTOM_MENU_ON_SCREEN()
		OR SHOULD_HIDE_LOCAL_UI()
			Clear_Any_Objective_Text_From_This_Script()
			#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_IMPEX_OBJECTIVE] Clearing objective text as custome menu not on screen") #ENDIF
		ENDIF
	ENDIF
	
	
	IF bDisplayLoseCopsObjective 
		IF NOT Is_This_The_Current_Objective_Text("FM_IHELP_LCP")
			
			IF NOT IS_MESSAGE_BEING_DISPLAYED()
				IF OK_TO_PRINT_OBJECTIVE_TEXT()
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
						IF SHOULD_DISPLAY_OBJECTIVE_TEXT(#IF IS_DEBUG_BUILD TRUE #ENDIF)
							#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_IMPEX_OBJECTIVE] Doing lose cops objective text") #ENDIF
							Print_Objective_Text("FM_IHELP_LCP") // lose the cops
						ELSE
						//	#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_IMPEX_OBJECTIVE] bDisplayFixCarObjective - False 7") #ENDIF
						ENDIF
					ELSE
					//	#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_IMPEX_OBJECTIVE] bDisplayFixCarObjective - False 6") #ENDIF
					ENDIF
				ELSE
				//	#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_IMPEX_OBJECTIVE] bDisplayFixCarObjective - False 5") #ENDIF
				ENDIF
			ELSE
				//#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_IMPEX_OBJECTIVE] bDisplayFixCarObjective - False 3") #ENDIF
			ENDIF
		ELSE
		//	#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_IMPEX_OBJECTIVE] bDisplayFixCarObjective - False 2") #ENDIF
		ENDIF
	ELSE
	//	#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_IMPEX_OBJECTIVE] bDisplayFixCarObjective - False 1") #ENDIF
		IF NOT IS_BIT_SET(iLocalImpExpBitSet, biL_DoneAllObjectiveText)
			IF Is_This_The_Current_Objective_Text("FM_IHELP_LCP")
				Clear_This_Objective_Text("FM_IHELP_LCP")
			ENDIF
		ENDIF
	ENDIF
	
	IF bDisplayCarmodObjective
		IF NOT Is_This_The_Current_Objective_Text("FM_CTUT_MOD")
			
			IF NOT IS_MESSAGE_BEING_DISPLAYED()
				IF OK_TO_PRINT_OBJECTIVE_TEXT()
					IF IS_BIT_SET(iBS_Help, HELP_RESPRAY_MESSAGE)
						IF SHOULD_DISPLAY_OBJECTIVE_TEXT(#IF IS_DEBUG_BUILD TRUE #ENDIF)
							#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_IMPEX_OBJECTIVE] Doing mod shop objective text") #ENDIF
							Print_Objective_Text("FM_CTUT_MOD") //Go to a Mod Shop ~BLIP_CAR_MOD_SHOP~.
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(iLocalImpExpBitSet, biL_DoneAllObjectiveText)
			IF Is_This_The_Current_Objective_Text("FM_CTUT_MOD")
				Clear_This_Objective_Text("FM_CTUT_MOD")
			ENDIF
		ENDIF
	ENDIF
	
	IF bDisplayResprayObjective
		IF NOT Is_This_The_Current_Objective_Text("FM_CTUT_RSP")

			IF NOT IS_MESSAGE_BEING_DISPLAYED()
				IF IS_SCREEN_FADED_IN()
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
							IF SHOULD_DISPLAY_OBJECTIVE_TEXT(#IF IS_DEBUG_BUILD TRUE #ENDIF)
								IF IS_CUSTOM_MENU_ON_SCREEN()
									#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_IMPEX_OBJECTIVE] Doing respray objective text") #ENDIF
									Print_Objective_Text("FM_CTUT_RSP") //Get your vehicle resprayed.
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(iLocalImpExpBitSet, biL_DoneAllObjectiveText)
			IF Is_This_The_Current_Objective_Text("FM_CTUT_RSP")
				Clear_This_Objective_Text("FM_CTUT_RSP")
			ENDIF
		ENDIF
	ENDIF
	
	 
	IF bDisplayFixCarObjective
		IF NOT Is_This_The_Current_Objective_Text("FM_CTUT_RSP")
			IF NOT IS_MESSAGE_BEING_DISPLAYED()
				IF IS_SCREEN_FADED_IN()
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
							IF SHOULD_DISPLAY_OBJECTIVE_TEXT(#IF IS_DEBUG_BUILD TRUE #ENDIF)
								IF IS_CUSTOM_MENU_ON_SCREEN()
									#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_IMPEX_OBJECTIVE] Doing repair objective text") #ENDIF
									Print_Objective_Text("FM_CTUT_REP") //Get your vehicle repaired.
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF

		ENDIF
	ELSE
		IF NOT IS_BIT_SET(iLocalImpExpBitSet, biL_DoneAllObjectiveText)
			IF Is_This_The_Current_Objective_Text("FM_CTUT_REP")
				Clear_This_Objective_Text("FM_CTUT_REP")
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_IMPEX_OBJECTIVE] Clearing repair vehicle objective") #ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bDisplayLeaveCarmodObjective
		IF NOT Is_This_The_Current_Objective_Text("FM_CTUT_LLS")
			
			IF NOT IS_MESSAGE_BEING_DISPLAYED()
				IF OK_TO_PRINT_OBJECTIVE_TEXT()
				OR IS_PLAYER_BROWSING_IN_ANY_CARMOD()
			//		IF IS_BIT_SET(iBS_Help, HELP_DELIVER_MESSAGE)
					IF SHOULD_DISPLAY_OBJECTIVE_TEXT(#IF IS_DEBUG_BUILD TRUE #ENDIF)
						IF IS_CUSTOM_MENU_ON_SCREEN()
					//		#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_IMPEX_OBJECTIVE] Doing leave car mod objective text") #ENDIF
					//		Print_Objective_Text("FM_CTUT_LLS") // Leave garage
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
			
	ELSE
		IF NOT IS_BIT_SET(iLocalImpExpBitSet, biL_DoneAllObjectiveText)
			IF Is_This_The_Current_Objective_Text("FM_CTUT_LLS")
				Clear_This_Objective_Text("FM_CTUT_LLS")
			ENDIF
		ENDIF
	ENDIF
	
	
	
	VEHICLE_INDEX vehTempObjective
	IF bDisplaySimeonObjective
		IF NOT Is_This_The_Current_Objective_Text("FM_IMP_SIM")
			
			IF NOT IS_MESSAGE_BEING_DISPLAYED()
				IF OK_TO_PRINT_OBJECTIVE_TEXT()
				OR IS_PLAYER_BROWSING_IN_ANY_CARMOD()
					IF IS_BIT_SET(iBS_Help, HELP_DELIVER_MESSAGE)
						
						IF NOT bSimeonObjectiveTimerExpired
							IF SHOULD_DISPLAY_OBJECTIVE_TEXT(#IF IS_DEBUG_BUILD TRUE #ENDIF)
								#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_IMPEX_OBJECTIVE] Doing simeon objective text") #ENDIF
								Print_Objective_Text("FM_IMP_SIM") // Go to Simeon's Garage ~BLIP_CRIM_CARSTEAL~.
								IF NOT IS_PED_INJURED(PLAYER_PED_ID())
									IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
										vehDeliverToSimeonObjective = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
										#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_IMPEX_OBJECTIVE] updated vehDeliverToSimeonObjective") #ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
									vehTempObjective = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
									IF vehTempObjective <> vehDeliverToSimeonObjective
										bSimeonObjectiveTimerExpired = FALSE
										RESET_NET_TIMER(timeObjectiveHelp)
										#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_IMPEX_OBJECTIVE] reset timeObjectiveHelp as player in a new car") #ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT HAS_NET_TIMER_STARTED(timeObjectiveHelp)
				START_NET_TIMER(timeObjectiveHelp)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_IMPEX_OBJECTIVE] Started timeObjectiveHelp") #ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(timeObjectiveHelp, 20000)
					Clear_Any_Objective_Text_From_This_Script()
					bSimeonObjectiveTimerExpired = TRUE
					#IF IS_DEBUG_BUILD NET_DW_PRINT("[UPDATE_IMPEX_OBJECTIVE] Clearing go to simeon objective as timeObjectiveHelp expired") #ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(iLocalImpExpBitSet, biL_DoneAllObjectiveText)
			IF Is_This_The_Current_Objective_Text("FM_IMP_SIM")
				Clear_This_Objective_Text("FM_IMP_SIM")
			ENDIF
		ENDIF
	ENDIF
ENDPROC



PROC UPDATE_STREAMED_IN_CAR_MODEL()
	
	INT i

	IF NOT (bStreamingInCarModel)

		IF NOT IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())		
			IF NOT GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID()) 
				IF NOT (serverBD.iListIDToStreamIn = -1)
					IF NOT (StreamedInModel = GET_MODEL_NAME_FROM_LIST_ID(serverBD.iListIDToStreamIn))	
					//	IF NOT IS_PLAYER_NEAR_ANY_GANG_HOUSES(PLAYER_ID(), 175)
						IF NOT IS_ENTITY_IN_STRIPCLUB(PLAYER_PED_ID())
							IF NOT IS_PLAYER_IN_NIGHTCLUB(PLAYER_ID())
								StreamedInModel = GET_MODEL_NAME_FROM_LIST_ID(serverBD.iListIDToStreamIn)										
								REPEAT COUNT_OF(RichPopSchedules) i
									OVERRIDE_POPSCHEDULE_VEHICLE_MODEL(RichPopSchedules[i], StreamedInModel)
								ENDREPEAT
								bStreamingInCarModel = TRUE	
								REQUEST_MODEL(StreamedInModel)
								RemoveCarGens()
								ADD_CAR_GENS_FOR_MODEL(StreamedInModel)
								#IF IS_DEBUG_BUILD
							//		NET_PRINT("[ImportExport] - UPDATE_STREAMED_IN_CAR_MODEL() - new model is ") NET_PRINT(GET_MODEL_NAME_FOR_DEBUG (StreamedInModel)) NET_NL()
									NET_DW_PRINT_STRINGS("UPDATE_STREAMED_IN_CAR_MODEL() - new model is ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL (StreamedInModel))
								#ENDIF		
							ENDIF
						ENDIF
					ENDIF
				ENDIF			
			ENDIF
		ENDIF
		
	ELSE
		
		// stop streaming in model if player is on mission or server changes the model to stream in. 
		IF IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())	
		OR (serverBD.iListIDToStreamIn > -1 AND NOT (StreamedInModel = GET_MODEL_NAME_FROM_LIST_ID(serverBD.iListIDToStreamIn)))
		OR IS_ENTITY_IN_STRIPCLUB(PLAYER_PED_ID())
		OR GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID()) 
	//	OR IS_PLAYER_NEAR_ANY_GANG_HOUSES(PLAYER_ID(), 150)
		OR IS_PLAYER_IN_NIGHTCLUB(PLAYER_ID())
			#IF IS_DEBUG_BUILD 
				IF IS_ENTITY_IN_STRIPCLUB(PLAYER_PED_ID())
					NET_DW_PRINT("[UPDATE_STREAMED_IN_CAR_MODEL] Clearing streamed model as in strip club") 
				ELIF IS_PLAYER_IN_NIGHTCLUB(PLAYER_ID())
					NET_DW_PRINT("[UPDATE_STREAMED_IN_CAR_MODEL] Clearing streamed model as in nightclub") 
				ELSE
					NET_DW_PRINT("[UPDATE_STREAMED_IN_CAR_MODEL] Clearing streamed model") 
				ENDIF
			#ENDIF
			SET_MODEL_AS_NO_LONGER_NEEDED(StreamedInModel)
			REPEAT COUNT_OF(RichPopSchedules) i
				CLEAR_POPSCHEDULE_OVERRIDE_VEHICLE_MODEL(RichPopSchedules[i])
			ENDREPEAT
			RemoveCarGens()
			StreamedInModel = DUMMY_MODEL_FOR_SCRIPT
			bStreamingInCarModel = FALSE
		ENDIF
	
	ENDIF

ENDPROC

FUNC BOOL IS_CAR_LIST_VALID()
	IF serverBD.iCarListState > LIST_STATE_INIT
		IF serverbd.CarList.iCar[0] <> serverbd.CarList.iCar[1]
			RETURN TRUE
		ELSE	
			#IF IS_DEBUG_BUILD 
				NET_DW_PRINT("[IS_CAR_LIST_VALID] serverBD.iCarListState > LIST_STATE_INIT but serverbd.CarList.iCar[0] = serverbd.CarList.iCar[1]") 
				NET_DW_PRINT_STRING_INT("[IS_CAR_LIST_VALID] serverbd.CarList.iCar[0] = ", serverbd.CarList.iCar[0])
				NET_DW_PRINT_STRING_INT("[IS_CAR_LIST_VALID] serverbd.CarList.iCar[1] = ", serverbd.CarList.iCar[1])
			#ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC UPDATE_GLOBAL_TEXT_LABELS_FOR_PHONE()
	INT iCount
	INT i
	REPEAT MAX_NUM_OF_CARS_ON_IMPORT_EXPORT_LIST i 			
		mpglobalsambience.ImportExportWantedCars[i] = ""	
		IF NOT (mpglobalsambience.impExpGLobalList.iCar[i] = -1)
		AND (mpglobalsambience.impExpGLobalList.bIsDelivered[i] = FALSE)						
			mpglobalsambience.ImportExportWantedCars[iCount] =  GET_CAR_STRING_FROM_LIST_ID(mpglobalsambience.impExpGLobalList.iCar[i])			
			iCount += 1
			#IF IS_DEBUG_BUILD
			//	IF bWdUseDebug
					NET_DW_PRINT_STRINGS("[UPDATE_GLOBAL_TEXT_LABELS_FOR_PHONE] Added this model ",  GET_CAR_STRING_FROM_LIST_ID(mpglobalsambience.impExpGLobalList.iCar[i]))
			//	ENDIF
			#ENDIF
		ENDIF		
	ENDREPEAT
	mpglobalsambience.iNumberOfImportExportWantedCars = iCount 
ENDPROC

PROC MAINTAIN_GLOBAL_CAR_LISTS()
//	IF PlayerBD[PARTICIPANT_ID_TO_INT()].iDeliveryState = PLAYER_DELIVERY_STATE_WAITING_FOR_CAR
//	AND NOT (mpglobalsambience.bSendImportExportTextMessage)
	
//	#IF IS_DEBUG_BUILD
//		NET_DW_PRINT("[MAINTAIN_GLOBAL_CAR_LISTS] Called. Callstack...")
//		DEBUG_PRINTCALLSTACK()
//	#ENDIF
	IF IS_CAR_LIST_VALID()
		IF NOT (iLocalUpdateID = serverBD.iUpdateID)	
			#IF IS_DEBUG_BUILD 
				PLAYER_INDEX playerHost = NETWORK_GET_HOST_OF_SCRIPT("AM_IMP_EXP")
				IF playerHost <> INVALID_PLAYER_INDEX()
					NET_DW_PRINT_STRING_WITH_PLAYER_NAME("[MAINTAIN_GLOBAL_CAR_LISTS] - updating. Current server is...", playerHost)
				ENDIF
			#ENDIF
			MPGlobalsAmbience.impExpGLobalList =  ThisCarList //serverBD.CarList	
			UPDATE_GLOBAL_TEXT_LABELS_FOR_PHONE()
			iLocalUpdateID = serverBD.iUpdateID
		ELSE
			#IF IS_DEBUG_BUILD
			//	NET_DW_PRINT_STRING_INT("[MAINTAIN_GLOBAL_CAR_LISTS] Called. But iLocalUpdateID = serverBD.iUpdateID. iLocalUpdateID = ", iLocalUpdateID)
			//	NET_DW_PRINT_STRING_INT("[MAINTAIN_GLOBAL_CAR_LISTS] Called. But iLocalUpdateID = serverBD.iUpdateID. serverBD.iUpdateID = ", serverBD.iUpdateID)
			#ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			NET_DW_PRINT("[MAINTAIN_GLOBAL_CAR_LISTS] Called but list not valid!")
		#ENDIF
	ENDIF
//	ELSE
	//	#IF IS_DEBUG_BUILD NET_DW_PRINT(" bSendImportExportTextMessage is set...") NET_NL()	#ENDIF
//	ENDIF
	
	
ENDPROC

PROC SERVER_GET_NEW_CAR_TO_STREAM_IN()
	INT iNewStream = GetRandomCarOnWantedList(serverBD.CarList)
	IF iNewStream <> serverBD.iListIDToStreamIn 
		serverBD.iListIDToStreamIn = iNewStream //GetRandomCarOnWantedList(serverBD.CarList)
		serverBD.iStreamInCarTimer = GET_NETWORK_TIME()	
		#IF IS_DEBUG_BUILD
			//NET_PRINT("[ImportExport] - SERVER_GET_NEW_CAR_TO_STREAM_IN() - new model is ") NET_PRINT(GET_MODEL_NAME_FOR_DEBUG (GET_MODEL_NAME_FROM_LIST_ID(serverBD.iListIDToStreamIn))) NET_NL()
			IF serverBD.iListIDToStreamIn > -1
				NET_DW_PRINT_STRINGS("SERVER_GET_NEW_CAR_TO_STREAM_IN() - new model is ", GET_MODEL_NAME_FOR_DEBUG (GET_MODEL_NAME_FROM_LIST_ID(serverBD.iListIDToStreamIn)))
			ENDIF
		#ENDIF
	ENDIF
ENDPROC

PROC RESET_GET_NEW_CAR_TO_STREAM_IN_TIMER()
	serverBD.iStreamInCarTimer = NULL
ENDPROC

FUNC BOOL GET_HIGH_PRIORITY_VEHICLE_START_POSITION(VECTOR &vPos, FLOAT &fHead)
	
	VECTOR vPos1, vPos2, vCreate
	FLOAT fCreateHead
	
	
	IF NOT ARE_VECTORS_EQUAL(vPos, <<0.0, 0.0, 0.0>>)
		RETURN TRUE
	ENDIF
	
	
	
	SWITCH iCandidate
		CASE 0
			vPos1 =<< -810.8889, -126.9419, 33.0 >>
			vPos2 =    << -807.2521, -116.0980, 38.0 >>
			vCreate =<< -809.0935, -121.9038, 36.5040 >>
			fCreateHead = 240.8011
		BREAK
		
		CASE 1
			vPos1 =<< -804.0040, -1317.2933, 0.0005 >>
			vPos2 =  << -797.8810, -1310.9979, 7.0005 >>
			vCreate =<< -801.6295, -1313.8650, 4.0005 >>
			fCreateHead = 350.3062
		BREAK
		
		CASE 2
			vPos1 =<< -194.8607, -1955.0051, 25.6205 >>
			vPos2 =  << -189.4078, -1945.2478, 28.6205 >>
			vCreate =<< -191.5592, -1950.2394, 26.6205 >> 
			fCreateHead = 288.8187
		BREAK
		
		CASE 3
			vPos1 =  << -2966.8748, 459.3981, 13.4644 >>
			vPos2 =   << -2961.4561, 466.0484, 16.1725 >>
			vCreate =	<< -2963.8867, 462.7986, 14.2156 >>
			fCreateHead = 30.4415 
		BREAK
		
		CASE 4
			vPos1 =  << -420.6335, 1217.7422, 322.7591 >>
			vPos2 =   << -411.9892, 1220.3464, 326.6421 >> 
			vCreate =	<< -416.0102, 1219.7706, 324.6421 >> 
			fCreateHead = 230.4509
		BREAK
		
		CASE 5
			vPos1 =  << -1634.7076, -890.8490, 6.9700 >>
			vPos2 =   << -1633.9034, -879.8690, 9.1264 >>  
			vCreate = << -1634.0059, -885.3531, 8.0518 >> 	 
			fCreateHead = 321.3100
		BREAK
		
		CASE 6
			vPos1 =  << -338.4312, -947.4233, 28.0788 >>
			vPos2 =  << -331.5926, -944.4080, 32.0788 >> 
			vCreate = << -334.8547, -945.2789, 30.0788 >>
			fCreateHead = 69.0442
		BREAK
		
		CASE 7
			vPos1 =  << 1093.9250, 245.6548, 77.9908 >>
			vPos2 =   << 1094.2432, 255.0715,82.8556 >> 
			vCreate =  << 1093.6864, 250.4772, 79.8556 >>
			fCreateHead = 328.5602 
		BREAK
		
		CASE 8
			vPos1 =  << -1407.8231, 58.1796, 50.8018 >>
			vPos2 =   << -1400.1899, 63.9074, 54.3222 >>
			vCreate = << -1404.4590, 62.1459, 52.0258 >>
			fCreateHead = 241.2814 
		BREAK
		
		CASE 9
			vPos1 =  << -1230.3242, -1656.8137, 2.0412 >>
			vPos2 =  << -1226.4288, -1648.1426, 4.1986 >>
			vCreate = << -1228.9010, -1652.3970, 3.1204 >>
			fCreateHead = 305.0972
		BREAK
	ENDSWITCH
			
	IF NOT ARE_VECTORS_EQUAL(vPos1, << 0.0, 0.0, 0.0>>)
		IF iCandidate < MAX_HIGH_PRIORITY_VEHICLE_START_POSITIONS
			IF serverBD.iHighPriorityCreateAreaId = -1
			//	#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("GET_HIGH_PRIORITY_VEHICLE_START_POSITION adding area to check, candidate...", iCandidate) #ENDIF
				serverBD.iHighPriorityCreateAreaId = NETWORK_ADD_ENTITY_AREA(vPos1, vPos2)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("GET_HIGH_PRIORITY_VEHICLE_START_POSITION... [NETWORK_ADD_ENTITY_AREA] Added an area ") #ENDIF
			ELSE
				IF NETWORK_ENTITY_AREA_DOES_EXIST(serverBD.iHighPriorityCreateAreaId)
					IF NETWORK_ENTITY_AREA_HAVE_ALL_REPLIED(serverBD.iHighPriorityCreateAreaId)
						IF NOT NETWORK_ENTITY_AREA_IS_OCCUPIED(serverBD.iHighPriorityCreateAreaId)
							IF NOT CAN_ANY_PLAYER_SEE_POINT(vCreate)
								#IF IS_DEBUG_BUILD NET_DW_PRINT("GET_HIGH_PRIORITY_VEHICLE_START_POSITION... got start pos, removed area [NETWORK_REMOVE_ENTITY_AREA] ") #ENDIF
								vPos = vCreate
								fHead = fCreateHead
								NETWORK_REMOVE_ENTITY_AREA(serverBD.iHighPriorityCreateAreaId)
								serverBD.iHighPriorityCreateAreaId = -1
								RETURN TRUE
							ELSE
								#IF IS_DEBUG_BUILD NET_DW_PRINT("GET_HIGH_PRIORITY_VEHICLE_START_POSITION... area is clear, but point on screen. Removed area [NETWORK_REMOVE_ENTITY_AREA] ") #ENDIF
								iCandidate++
								NETWORK_REMOVE_ENTITY_AREA(serverBD.iHighPriorityCreateAreaId)
								serverBD.iHighPriorityCreateAreaId = -1
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD NET_DW_PRINT("GET_HIGH_PRIORITY_VEHICLE_START_POSITION... area is occupied , removing area [NETWORK_REMOVE_ENTITY_AREA]") #ENDIF
							iCandidate++
							NETWORK_REMOVE_ENTITY_AREA(serverBD.iHighPriorityCreateAreaId)
							serverBD.iHighPriorityCreateAreaId = -1
						ENDIF			
					ELSE
						#IF IS_DEBUG_BUILD NET_DW_PRINT("GET_HIGH_PRIORITY_VEHICLE_START_POSITION... not everyone has replied ") #ENDIF
					ENDIF
				ELSE
				//	#IF IS_DEBUG_BUILD NET_DW_PRINT("GET_HIGH_PRIORITY_VEHICLE_START_POSITION... area doesn't exist ") #ENDIF
				ENDIF
			ENDIF
		ELSE	
		//	#IF IS_DEBUG_BUILD NET_DW_PRINT("GET_HIGH_PRIORITY_VEHICLE_START_POSITION... iCandidate >= MAX_HIGH_PRIORITY_VEHICLE_START_POSITIONS! ") #ENDIF
			iCandidate = 0
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL IS_HPV_TRAPPED_IN_MOD_SHOP()
	RETURN mpglobalsambience.iHpvStuckBitset <> 0 // Kenny will set
ENDFUNC

PROC CLEAR_HPV_TRAPPED_IN_MODSHOP()
	mpglobalsambience.iHpvStuckBitset = 0
ENDPROC

/// PURPOSE:
///		Need to handle the HPV being stuck in the mod shop due to the driver quiting while in the HPV in the modshop.
PROC MAINTAIN_HPV_TRAPPED_IN_MOD_SHOP()
	VECTOR vHpvPos
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		SWITCH serverbd.iWarpWhenStuckInModshopProg
			CASE 0
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.niPriorityVeh)
					IF IS_HPV_TRAPPED_IN_MOD_SHOP()
						serverbd.iWarpWhenStuckInModshopProg++
						#IF IS_DEBUG_BUILD
							NET_DW_PRINT_STRING_INT("[MAINTAIN_HPV_TRAPPED_IN_MOD_SHOP] Stuck in modshop! serverbd.iWarpWhenStuckInModshopProg = ", serverbd.iWarpWhenStuckInModshopProg)
						#ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 1
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.niPriorityVeh)
					IF IS_NET_VEHICLE_DRIVEABLE(serverbd.niPriorityVeh)
						vHpvPos = GET_ENTITY_COORDS(NET_TO_VEH(serverbd.niPriorityVeh))
						
						VEHICLE_SPAWN_LOCATION_PARAMS Params

						IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(vHpvPos, <<0.0, 0.0, 0.0>>, serverbd.mHighPriority, TRUE, serverbd.vStuckWarpTo, serverbd.fStuckWarpHead, Params)
							serverbd.iWarpWhenStuckInModshopProg++
							#IF IS_DEBUG_BUILD
								NET_DW_PRINT_STRING_VECTOR("[MAINTAIN_HPV_TRAPPED_IN_MOD_SHOP] Got coords, will warp to ",serverbd.vStuckWarpTo)
								NET_DW_PRINT_STRING_INT("[MAINTAIN_HPV_TRAPPED_IN_MOD_SHOP] Got coords, serverbd.iWarpWhenStuckInModshopProg = ", serverbd.iWarpWhenStuckInModshopProg)
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 2
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.niPriorityVeh)
					IF IS_NET_VEHICLE_DRIVEABLE(serverbd.niPriorityVeh)
						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverbd.niPriorityVeh)
							SET_ENTITY_COORDS(NET_TO_VEH(serverbd.niPriorityVeh), serverbd.vStuckWarpTo)
							SET_ENTITY_HEADING(NET_TO_VEH(serverbd.niPriorityVeh), serverbd.fStuckWarpHead)
							
							serverbd.iWarpWhenStuckInModshopProg++
							#IF IS_DEBUG_BUILD
							NET_DW_PRINT_STRING_INT("[MAINTAIN_HPV_TRAPPED_IN_MOD_SHOP] HPV moved, serverbd.iWarpWhenStuckInModshopProg = ", serverbd.iWarpWhenStuckInModshopProg)
							#ENDIF
						ELSE
							NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverbd.niPriorityVeh)
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 3
				CLEAR_HPV_TRAPPED_IN_MODSHOP()
				serverbd.iWarpWhenStuckInModshopProg = 0
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

FUNC BOOL DOES_HPV_HAVE_DRIVER()
	PLAYER_INDEX playerTemp
	PED_INDEX pedPlayer
	IF iLocalPartDrivingHpv = -1
	AND IS_BIT_SET(iLocalImpExpBitSet, BiL_LoopedThroughEveryone)
		RETURN FALSE
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPriorityVeh)
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niPriorityVeh)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niPriorityVeh))
					IF IS_PED_SITTING_IN_VEHICLE_SEAT(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niPriorityVeh), VS_DRIVER)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF iLocalPartDrivingHpv >= 0
				IF iLocalPartDrivingHpv <> PARTICIPANT_ID_TO_INT()
					IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iLocalPartDrivingHpv))
						playerTemp = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iLocalPartDrivingHpv))
						IF IS_NET_PLAYER_OK(playerTemp)
							pedPlayer = GET_PLAYER_PED(playerTemp)
							IF IS_PED_SITTING_IN_VEHICLE(pedPlayer, NET_TO_VEH(serverBD.niPriorityVeh))	
								IF IS_PED_SITTING_IN_VEHICLE_SEAT(pedPlayer, NET_TO_VEH(serverBD.niPriorityVeh), VS_DRIVER)
									RETURN TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_HPV_HAVE_DRIVER_NO_COLLISION_CHECK()
	PED_INDEX pedTemp
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPriorityVeh)
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niPriorityVeh)
			pedTemp = GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(serverBD.niPriorityVeh), VS_DRIVER)
			IF pedTemp <> NULL
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
		
	RETURN FALSE
ENDFUNC

PROC MAINTIAN_HPV_NO_COLLISION()
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		SWITCH serverbd.iHpvNoCollisionProg
			CASE 0
				IF IS_BIT_SET(serverbd.iServerBitSet, biS_SomeoneEnteredCarAtLeastOnce) 
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.niPriorityVeh)
						IF NOT DOES_HPV_HAVE_DRIVER_NO_COLLISION_CHECK()
							IF GET_ENTITY_COLLISION_DISABLED(NET_TO_VEH(serverbd.niPriorityVeh))
								//-- CHeck for near Simeon's
								IF NOT IS_ENTITY_IN_ANGLED_AREA( NET_TO_VEH(serverbd.niPriorityVeh), <<1204.052612,-3102.310547,1.770506>>, <<1204.265259,-3121.973633,17.920322>>, 31.625000)
									IF NOT IS_BIT_SET(serverbd.iServerBitSet, biS_ResetNoCollision)
										#IF IS_DEBUG_BUILD NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER] [MAINTIAN_HPV_NO_COLLISION] HPV has no colission, will turn back on...") #ENDIF
										SET_BIT(serverbd.iServerBitSet, biS_ResetNoCollision)
									ENDIF
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
									IF bWdDoHpvDebug
										NET_DW_PRINT("[MAINTIAN_HPV_NO_COLLISION] Else 4")
									ENDIF
								#ENDIF
							ENDIF
						ELSE
							IF IS_BIT_SET(serverbd.iServerBitSet, biS_ResetNoCollision)
								CLEAR_BIT(serverbd.iServerBitSet, biS_ResetNoCollision)
								#IF IS_DEBUG_BUILD NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER] [MAINTIAN_HPV_NO_COLLISION] biS_ResetNoCollision was set but HPV has driver, will clear") #ENDIF
							ENDIF
							
							#IF IS_DEBUG_BUILD
								IF bWdDoHpvDebug
									NET_DW_PRINT("[MAINTIAN_HPV_NO_COLLISION] Else 3")
								ENDIF
							#ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							IF bWdDoHpvDebug
								NET_DW_PRINT("[MAINTIAN_HPV_NO_COLLISION] Else 2")
							ENDIF
						#ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						IF bWdDoHpvDebug
							NET_DW_PRINT("[MAINTIAN_HPV_NO_COLLISION] Else 1")
						ENDIF
					#ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	IF IS_BIT_SET(serverbd.iServerBitSet, biS_ResetNoCollision)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.niPriorityVeh)
			IF IS_NET_VEHICLE_DRIVEABLE(serverbd.niPriorityVeh)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverbd.niPriorityVeh)
					SET_ENTITY_COLLISION(NET_TO_VEH(serverbd.niPriorityVeh), TRUE)	
					SET_VEHICLE_DOORS_LOCKED(NET_TO_VEH(serverbd.niPriorityVeh), VEHICLELOCK_UNLOCKED)
					FREEZE_ENTITY_POSITION(NET_TO_VEH(serverbd.niPriorityVeh), FALSE)
					NETWORK_FADE_IN_ENTITY(NET_TO_VEH(serverBD.niPriorityVeh), TRUE)
					CLEAR_BIT(serverbd.iServerBitSet, biS_ResetNoCollision)
					#IF IS_DEBUG_BUILD NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER] [MAINTIAN_HPV_NO_COLLISION] Turned HPV collision back on, unlocked doors") #ENDIF
				ELSE
					#IF IS_DEBUG_BUILD NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER] [MAINTIAN_HPV_NO_COLLISION] Requesting control of HPV") #ENDIF
					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverbd.niPriorityVeh)
				ENDIF
			ELSE
				CLEAR_BIT(serverbd.iServerBitSet, biS_ResetNoCollision)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER] [MAINTIAN_HPV_NO_COLLISION] biS_ResetNoCollision was set but HPV not drivable") #ENDIF
			ENDIF
		ELSE
			CLEAR_BIT(serverbd.iServerBitSet, biS_ResetNoCollision)
			#IF IS_DEBUG_BUILD NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER] [MAINTIAN_HPV_NO_COLLISION] biS_ResetNoCollision was set but NI doesn't exist") #ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER()
	VECTOR vCreate
	FLOAT fHead
	TIMEOFDAY CurrentTimeOfDay
	
	#IF IS_DEBUG_BUILD
		IF bWdUseDebug
			IF g_bLaunchImportExportHpv
				NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER] g_bLaunchImportExportHpv is set")
			ELSE	
				NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER] g_bLaunchImportExportHpv is NOT set")
			ENDIF
			IF NOT IS_BIT_SET(serverbd.iServerBitSet, biS_RunHighPriorityVehicle) 
				NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER] Not running HPV as biS_RunHighPriorityVehicle not set")
			ELSE	
				NET_DW_PRINT_STRING_INT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER] Running HPV, server prog... ", ENUM_TO_INT(serverbd.hpvServerProg))
			ENDIF
		ENDIF
	#ENDIF
	
	SWITCH serverbd.hpvServerProg
		CASE HPV_INIT
			IF NOT IS_BIT_SET(serverbd.iServerBitSet, biS_RunHighPriorityVehicle) 
				IF bDoCollectHighPriorityVehicle 
				#IF IS_DEBUG_BUILD OR g_bLaunchImportExportHpv #ENDIF
					IF bUseLauncher
						IF MPGlobalsAmbience.bLaunchImportExportHpv
							#IF IS_DEBUG_BUILD
								NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER]    ***********************  INIT  ***********************")
							#ENDIF
							CLEAR_BIT(serverbd.iServerBitSet, biS_DoneVehWreckedTick)
							CLEAR_BIT(serverbd.iServerBitSet, biS_SomeoneFoundCarInGarageNoDri)
							CLEAR_BIT(iLocalImpExpBitSet, biL_SomeoneFoundHpvInGarageNoDri)
							RESERVE_NETWORK_MISSION_VEHICLES(0)
							serverBD.iNumReservedVeh = 0
							SET_BIT(serverbd.iServerBitSet, biS_RunHighPriorityVehicle) 
							MPGlobalsAmbience.bLaunchImportExportHpv = FALSE
						ENDIF
					ELSE
					
						IF serverbd.hpvTriggerTime = INVALID_TIMEOFDAY
							CLEAR_BIT(serverbd.iServerBitSet, biS_DoneVehWreckedTick)
							CLEAR_BIT(serverbd.iServerBitSet, biS_SomeoneFoundCarInGarageNoDri)
							CLEAR_BIT(iLocalImpExpBitSet, biL_SomeoneFoundHpvInGarageNoDri)
							RESERVE_NETWORK_MISSION_VEHICLES(0)
							serverBD.iNumReservedVeh = 0
							CurrentTimeOfDay = GET_CURRENT_TIMEOFDAY()
							SET_TIMEOFDAY(serverbd.hpvTriggerTime, GET_TIMEOFDAY_SECOND(CurrentTimeOfDay), GET_TIMEOFDAY_MINUTE(CurrentTimeOfDay), GET_TIMEOFDAY_HOUR(CurrentTimeOfDay), GET_TIMEOFDAY_DAY(CurrentTimeOfDay), GET_TIMEOFDAY_MONTH(CurrentTimeOfDay), GET_TIMEOFDAY_YEAR(CurrentTimeOfDay))
							ADD_TIME_TO_TIMEOFDAY(serverbd.hpvTriggerTime,0, GET_RANDOM_INT_IN_RANGE(0, 60), GET_RANDOM_INT_IN_RANGE(0, 8), 1, 0, 0)
							#IF IS_DEBUG_BUILD
								NET_DW_PRINT("Server reserved 0 mission vehicle")
								NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER]    ***********************  INIT  ***********************")
								NET_NL() NET_PRINT_TIME() NET_PRINT("   >>> [DSW] [MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER]   HPV Import Export - CURRENT TIME = ") 		NET_PRINT("Day ") NET_PRINT_INT(GET_TIMEOFDAY_DAY(CurrentTimeOfDay)) NET_PRINT(" ") NET_PRINT_INT(GET_TIMEOFDAY_HOUR(serverbd.hpvTriggerTime)) NET_PRINT(":") NET_PRINT_INT(GET_TIMEOFDAY_MINUTE(CurrentTimeOfDay)) NET_NL()		//PRINT_TIMEOFDAY(CurrentTimeOfDay) NET_NL()
								NET_NL() NET_PRINT_TIME() NET_PRINT("   >>> [DSW] [MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER]  HPV Import Export  - NEXT TRIGGER TIME = ")	NET_PRINT("Day ") NET_PRINT_INT(GET_TIMEOFDAY_DAY(serverbd.hpvTriggerTime)) NET_PRINT(" ") NET_PRINT_INT(GET_TIMEOFDAY_HOUR(serverbd.hpvTriggerTime))NET_PRINT(":") NET_PRINT_INT(GET_TIMEOFDAY_MINUTE(serverbd.hpvTriggerTime)) NET_NL()
							#ENDIF
						ELSE
							IF NOT IS_MP_AMBIENT_SCRIPT_BLOCKED(MPAM_TYPE_IMPORTEXPORT)
								IF NOT HAS_NET_TIMER_STARTED(timeHpvTimeOfDayCheck)
									START_NET_TIMER(timeHpvTimeOfDayCheck)
								ELSE
									IF HAS_NET_TIMER_EXPIRED(timeHpvTimeOfDayCheck, 10000)
										RESET_NET_TIMER(timeHpvTimeOfDayCheck)
										IF IS_NOW_AFTER_TIMEOFDAY(serverbd.hpvTriggerTime)
										#IF IS_DEBUG_BUILD OR g_bLaunchImportExportHpv #ENDIF
											#IF IS_DEBUG_BUILD  
												IF g_bLaunchImportExportHpv 
													NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER] Launching HPV because g_bLaunchImportExportHpv")
												ELSE
													NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER] Launching HPV but NOT g_bLaunchImportExportHpv")
												ENDIF
											#ENDIF
									
											SET_BIT(serverbd.iServerBitSet, biS_RunHighPriorityVehicle)
											
											#IF IS_DEBUG_BUILD 
												g_bLaunchImportExportHpv = FALSE
												NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER] ***********************  Launching High Priority Vehicle grab  *********************** ")
												CurrentTimeOfDay = GET_CURRENT_TIMEOFDAY()
												NET_NL() NET_PRINT_TIME() NET_PRINT("   >>> [DSW] [MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER]  HPV Import Export - TIME AT LAUNCH = ") 	NET_PRINT("Day ")NET_PRINT_INT(GET_TIMEOFDAY_HOUR(CurrentTimeOfDay))NET_PRINT(":") NET_PRINT(" ") NET_PRINT_INT(GET_TIMEOFDAY_HOUR(CurrentTimeOfDay)) NET_PRINT(":")NET_PRINT_INT(GET_TIMEOFDAY_MINUTE(CurrentTimeOfDay)) NET_NL()	
											#ENDIF
										ELSE
											#IF IS_DEBUG_BUILD
												IF bTimedDebug
												OR bWdDoHpvDebug
													CurrentTimeOfDay = GET_CURRENT_TIMEOFDAY()
													NET_NL() NET_PRINT_TIME() NET_PRINT("   [dsw] [bTimedDebug] [MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER]    Not launching HPV as Sufficient time hasn't passed - CURRENT TIME = ") 			NET_PRINT("Day ") NET_PRINT_INT(GET_TIMEOFDAY_DAY(CurrentTimeOfDay)) NET_PRINT(" ") NET_PRINT_INT(GET_TIMEOFDAY_HOUR(CurrentTimeOfDay)) NET_PRINT(":") NET_PRINT_INT(GET_TIMEOFDAY_MINUTE(CurrentTimeOfDay)) NET_NL()
													NET_NL() NET_PRINT_TIME() NET_PRINT("   [dsw] [bTimedDebug] [MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER]    Not launching HPV as Sufficient time hasn't passed - NEXT TRIGGER TIME =  = ") 	NET_PRINT("Day ") NET_PRINT_INT(GET_TIMEOFDAY_DAY(serverbd.hpvTriggerTime)) NET_PRINT(" ") NET_PRINT_INT(GET_TIMEOFDAY_HOUR(serverbd.hpvTriggerTime)) NET_PRINT(":") NET_PRINT_INT(GET_TIMEOFDAY_MINUTE(serverbd.hpvTriggerTime)) NET_NL()
												ENDIF
											#ENDIF
										ENDIF
									ELSE
	//									#IF IS_DEBUG_BUILD
	//										IF bWdDoHpvDebug
	//											NET_DW_PRINT("[bTimedDebug] [MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER] Not launching HPV as timeHpvTimeOfDayCheck hasn't passed")
	//										ENDIF
	//									#ENDIF
									ENDIF
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
									IF bTimedDebug
									OR bWdDoHpvDebug
										NET_DW_PRINT("[bTimedDebug] [MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER] Not launching HPV as IS_MP_AMBIENT_SCRIPT_BLOCKED")
									ENDIF
								#ENDIF
							ENDIF
						ENDIF
						
					ENDIF
				ENDIF
				
			ELSE
				IF serverbd.mHighPriority = DUMMY_MODEL_FOR_SCRIPT
					IF CAN_RESERVE_NETWORK_VEHICLES_FOR_THIS_SCRIPT(1, TRUE, TRUE)
					//	INT iRandomCar 
					//	iRandomCar = GetRandomCarOnWantedList(serverBD.CarList) //GET_RANDOM_INT_IN_RANGE(0, TOTAL_NUMBER_OF_CARS)
						IF NETWORK_ENTITY_AREA_DOES_EXIST(serverBD.iHighPriorityCreateAreaId)
							NETWORK_REMOVE_ENTITY_AREA(serverBD.iHighPriorityCreateAreaId)
							#IF IS_DEBUG_BUILD NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER] Entity area existed! Calling NETWORK_REMOVE_ENTITY_AREA()") #ENDIF
						ELSE
							#IF IS_DEBUG_BUILD NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER] No entity area existed!") #ENDIF
						ENDIF
						serverBD.iHighPriorityCreateAreaId = -1
						iCandidate = GET_RANDOM_INT_IN_RANGE(0, MAX_HIGH_PRIORITY_VEHICLE_START_POSITIONS)
						MODEL_NAMES mTemp
						mTemp = GET_HIGH_PRIORITY_VEHICLE_MODEL()
						IF mTemp <> DUMMY_MODEL_FOR_SCRIPT
							
							
							serverbd.mHighPriority = mTemp  //GET_MODEL_NAME_FROM_LIST_ID(iRandomCar) //RUFFIAN
							RESERVE_NETWORK_MISSION_VEHICLES(1)
							serverBD.iNumReservedVeh = 1
							#IF IS_DEBUG_BUILD
								NET_DW_PRINT("Server reserved 1 mission vehicle")
								NET_DW_PRINT_STRINGS("[MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER] Using model ", GET_MODEL_NAME_FOR_DEBUG (serverbd.mHighPriority))
							#ENDIF
						ENDIF
					ENDIF
				ELSE
					IF CAN_REGISTER_MISSION_VEHICLES(1)
						IF REQUEST_LOAD_MODEL(serverbd.mHighPriority)
							IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niPriorityVeh)
								IF GET_HIGH_PRIORITY_VEHICLE_START_POSITION(vCreate, fHead)
									IF CREATE_NET_VEHICLE(serverBD.niPriorityVeh, serverbd.mHighPriority,  vCreate, fHead, DEFAULT, DEFAULT, DEFAULT, TRUE)
										
										IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
											INT iDecoratorValue
											IF DECOR_EXIST_ON(NET_TO_VEH(serverBD.niPriorityVeh), "MPBitset")
												iDecoratorValue = DECOR_GET_INT(NET_TO_VEH(serverBD.niPriorityVeh), "MPBitset")
											ENDIF
											SET_BIT(iDecoratorValue, MP_DECORATOR_BS_SIMEON_VEHICLE)
											DECOR_SET_INT(NET_TO_VEH(serverBD.niPriorityVeh), "MPBitset", iDecoratorValue)
										ENDIF
										
										//Prevents the vehicle from being used for activities that use passive mode - MJM
										IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
											INT iDecoratorValue
											IF DECOR_EXIST_ON(NET_TO_VEH(serverBD.niPriorityVeh), "MPBitset")
												iDecoratorValue = DECOR_GET_INT(NET_TO_VEH(serverBD.niPriorityVeh), "MPBitset")
											ENDIF
											SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_PASSIVE_MODE_VEHICLE)
											DECOR_SET_INT(NET_TO_VEH(serverBD.niPriorityVeh), "MPBitset", iDecoratorValue)
										ENDIF
										
										IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh",DECOR_TYPE_INT)
											DECOR_SET_INT(NET_TO_VEH(serverBD.niPriorityVeh),"Not_Allow_As_Saved_Veh",MP_NOT_ALLOWED_AS_SAVED_VEHICLE_DEC_SIMEON)
										ENDIF
										
										SET_BIT(serverbd.iServerBitSet, biS_RunHighPriorityVehicle)
										CLEAR_BIT(serverbd.iServerBitSet, biS_DoneVehWreckedTick)
										CLEAR_BIT(serverbd.iServerBitSet, biS_DoneInitialCountdown)
										CLEAR_BIT(serverbd.iServerBitSet, biS_SomeoneNearHpv)
										CLEAR_BIT(iLocalImpExpBitSet, biL_FoundSomeoneNearHpv)
										CLEAR_BIT(serverbd.iServerBitSet, biS_SomeoneFoundCarInGarageNoDri)
										
										SET_BIT(iLocalImpExpBitSet, biL_HpvJustLaunched)
										SET_MODEL_AS_NO_LONGER_NEEDED(serverbd.mHighPriority)
										serverbd.hpvServerProg = HPV_GET_CAR
										RESET_NET_TIMER(serverbd.timeHpvLaunched)
										START_NET_TIMER(serverbd.timeHpvLaunched)
										SET_VEHICLE_DOORS_LOCKED(NET_TO_VEH(serverBD.niPriorityVeh), VEHICLELOCK_LOCKED_BUT_CAN_BE_DAMAGED)
										SET_VEHICLE_NEEDS_TO_BE_HOTWIRED(NET_TO_VEH(serverBD.niPriorityVeh), TRUE)
										SET_VEHICLE_MODEL_IS_SUPPRESSED(serverbd.mHighPriority, TRUE)
										
										SET_NETWORK_ID_PASS_CONTROL_IN_TUTORIAL(serverBD.niPriorityVeh, TRUE) // 1802644
										#IF IS_DEBUG_BUILD
											NET_DW_PRINT_STRINGS("[MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER] HPV model is suppressed. Model is ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(serverbd.mHighPriority))
											NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER] HPV_INIT -> HPV_GET_CAR ")
										#ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER] Trying to launch but can't reserve mission veh") #ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE HPV_GET_CAR
			IF NOT IS_BIT_SET(serverbd.iServerBitSet, biS_DoneVehWreckedTick)
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.niPriorityVeh)
				//	IF NOT IS_NET_VEHICLE_DRIVEABLE(serverbd.niPriorityVeh)
					IF NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverbd.niPriorityVeh))
						#IF IS_DEBUG_BUILD
							NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER] Server thinks vehicle isn't driveable ")
							NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER] Sending vehicle wrecked ticker - 1 ")
						#ENDIF
						
						TickerData.TickerEvent = TICKER_EVENT_IMPORT_EXPORT_CAR_WRECKED
						BROADCAST_TICKER_EVENT(TickerData,ALL_PLAYERS_ON_SCRIPT(TRUE))
						SET_BIT(serverbd.iServerBitSet, biS_DoneVehWreckedTick)
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(iLocalImpExpBitSet, biL_SomeoneDelSpecCar)
			OR (IS_BIT_SET(iLocalImpExpBitSet, BiL_ServerLoopedThroughEveryone) AND IS_BIT_SET(serverbd.iServerBitSet, biS_AllPlayersDone))
				IF NOT IS_BIT_SET(iLocalImpExpBitSet, biL_SomeoneDelSpecCar)	
					IF NOT IS_BIT_SET(serverbd.iServerBitSet, biS_DoneVehWreckedTick)
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.niPriorityVeh)
							IF NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverbd.niPriorityVeh))
								#IF IS_DEBUG_BUILD
									NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER] Sending vehicle wrecked ticker - 2 ")
								#ENDIF
								TickerData.TickerEvent = TICKER_EVENT_IMPORT_EXPORT_CAR_WRECKED
								BROADCAST_TICKER_EVENT(TickerData,ALL_PLAYERS_ON_SCRIPT(TRUE))
								SET_BIT(serverbd.iServerBitSet, biS_DoneVehWreckedTick)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				serverbd.hpvServerProg = HPV_CLEANUP_CAR
				#IF IS_DEBUG_BUILD
					NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER] HPV_GET_CAR -> HPV_CLEANUP_CAR Reason...")
					IF IS_BIT_SET(iLocalImpExpBitSet, biL_SomeoneDelSpecCar)
						NET_DW_PRINT("biL_SomeoneDelSpecCar is set")
					ELIF (IS_BIT_SET(iLocalImpExpBitSet, BiL_ServerLoopedThroughEveryone) AND IS_BIT_SET(serverbd.iServerBitSet, biS_AllPlayersDone)) 
						NET_DW_PRINT("biS_AllPlayersDone is set")
						
					ENDIF
				#ENDIF
			ENDIF
			
			//-- Check for HPV rigged with explosives
			IF IS_VEH_RIGGED(NET_TO_VEH(serverBD.niPriorityVeh))
				serverbd.hpvServerProg = HPV_CLEANUP_CAR
				TickerData.TickerEvent = TICKER_EVENT_IMPORT_EXPORT_CAR_TIMEOUT
				BROADCAST_TICKER_EVENT(TickerData,ALL_PLAYERS_ON_SCRIPT(TRUE))
				#IF IS_DEBUG_BUILD
					NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER] HPV_GET_CAR -> HPV_CLEANUP_CAR because IS_VEH_RIGGED")
				#ENDIF
			ENDIF
			
			//--CHeck for time out
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_DoneInitialCountdown)
				//-- Initial 6 minute count down
				IF HAS_NET_TIMER_EXPIRED(serverbd.timeHpvLaunched, 360000) //
					SET_BIT(serverBD.iServerBitSet, biS_DoneInitialCountdown)
					RESET_NET_TIMER(serverbd.timeHpvLaunched)
					START_NET_TIMER(serverbd.timeHpvLaunched)
					#IF IS_DEBUG_BUILD NET_DW_PRINT("Initial HPV timer expired") #ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(iLocalImpExpBitSet, BiL_ServerLoopedThroughEveryone)
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneNearHpv)
				
						//-- 2 minutes for someone to get near
						IF HAS_NET_TIMER_STARTED(serverbd.timeHpvLaunched)
							IF HAS_NET_TIMER_EXPIRED(serverbd.timeHpvLaunched, 120000) //
								serverbd.hpvServerProg = HPV_CLEANUP_CAR
								TickerData.TickerEvent = TICKER_EVENT_IMPORT_EXPORT_CAR_TIMEOUT
								BROADCAST_TICKER_EVENT(TickerData,ALL_PLAYERS_ON_SCRIPT(TRUE))
								#IF IS_DEBUG_BUILD
									NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER] HPV_GET_CAR -> HPV_CLEANUP_CAR because no-one near car")
								#ENDIF
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
								NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER] Starting serverbd.timeHpvLaunched as no-one near hpv, but timer not started ")
							#ENDIF
							START_NET_TIMER(serverbd.timeHpvLaunched)
						ENDIF
					ELSE
						
						IF HAS_NET_TIMER_STARTED(serverbd.timeHpvLaunched)
							#IF IS_DEBUG_BUILD
								NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER] Resetting serverbd.timeHpvLaunched as someone near hpv")
							#ENDIF
							RESET_NET_TIMER(serverbd.timeHpvLaunched)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(serverbd.iServerBitSet, biS_SomeoneFoundCarInGarageNoDri)
				IF NOT IS_BIT_SET(iLocalImpExpBitSet, biL_SomeoneDelSpecCar)
					IF serverbd.hpvServerProg = HPV_GET_CAR
						serverbd.hpvServerProg = HPV_CLEANUP_CAR
					//	TickerData.TickerEvent = TICKER_EVENT_IMPORT_EXPORT_CAR_TIMEOUT
					//	BROADCAST_TICKER_EVENT(TickerData,ALL_PLAYERS_ON_SCRIPT(TRUE))
						#IF IS_DEBUG_BUILD
							NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER] HPV_GET_CAR -> HPV_CLEANUP_CAR because car in garage with no driver")
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//-- Dave G
			IF SHOULD_MP_AMBIENT_SCRIPT_TERMINATE(MPAM_TYPE_IMPORTEXPORT)
				IF serverbd.hpvServerProg = HPV_GET_CAR
					serverbd.hpvServerProg = HPV_CLEANUP_CAR
					#IF IS_DEBUG_BUILD
						NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER] HPV_GET_CAR -> HPV_CLEANUP_CAR because Dave G says so")
					#ENDIF
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
					IF MPGlobalsAmbience.bKillActiveEvent 
						IF serverbd.hpvServerProg = HPV_GET_CAR
							serverbd.hpvServerProg = HPV_CLEANUP_CAR
							#IF IS_DEBUG_BUILD
								NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER] HPV_GET_CAR -> HPV_CLEANUP_CAR because Martin says so")
							#ENDIF
						ENDIF
						
						MPGlobalsAmbience.bKillActiveEvent  = FALSE
					ENDIF
				 // FEATURE_NEW_AMBIENT_EVENTS
			#ENDIF
		BREAK
		
		CASE HPV_CLEANUP_CAR
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.niPriorityVeh)
				IF TAKE_CONTROL_OF_NET_ID(serverbd.niPriorityVeh)
					CLEANUP_NET_ID(serverbd.niPriorityVeh)
					serverbd.hpvServerProg = HPV_FINISHED
					#IF IS_DEBUG_BUILD
						NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER] HPV_CLEANUP_CAR -> HPV_FINISHED As cleaned up car")
					#ENDIF
				ENDIF
			ELSE
				serverbd.hpvServerProg = HPV_FINISHED
				#IF IS_DEBUG_BUILD
					NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER] HPV_CLEANUP_CAR -> HPV_FINISHED As network id doesn't exist")
				#ENDIF
			ENDIF
		BREAK
		
		CASE HPV_FINISHED
			
			IF IS_BIT_SET(iLocalImpExpBitSet, BiL_ServerLoopedThroughEveryone)
				IF IS_BIT_SET(serverbd.iServerBitSet, biS_AllPlayersDone)
					
					serverbd.hpvServerProg = HPV_INIT
					// = FALSE
					CLEAR_BIT(serverbd.iServerBitSet, biS_RunHighPriorityVehicle)
					serverbd.hpvTriggerTime = INVALID_TIMEOFDAY
					IF serverbd.mHighPriority <> DUMMY_MODEL_FOR_SCRIPT
						SET_VEHICLE_MODEL_IS_SUPPRESSED(serverbd.mHighPriority, FALSE)
						#IF IS_DEBUG_BUILD NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER] HPV model no longer suppressed ") #ENDIF
					ENDIF
					serverbd.mHighPriority = DUMMY_MODEL_FOR_SCRIPT
					#IF IS_DEBUG_BUILD
						g_bLaunchImportExportHpv = FALSE
						bWdDoStaggeredDebug = FALSE
						NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER] HPV_FINISHED -> HPV_INIT ")
					#ENDIF
					
					IF bUseLauncher
						BROADCAST_AML_SET_HPV_RUNNING(SPECIFIC_PLAYER(NETWORK_GET_HOST_OF_SCRIPT("AM_LAUNCHER")),FALSE)
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER] Case HPV_FINISHED but someone not finished, turning on debug. Please tell Dave W ")
						bWdDoStaggeredDebug = TRUE
					#ENDIF
				ENDIF
			ENDIF
			
		BREAK
	ENDSWITCH
	
	//MAINTAIN_HPV_TRAPPED_IN_MOD_SHOP()
	
	 MAINTIAN_HPV_NO_COLLISION()
ENDPROC



PROC UPDATE_SERVER_PROCESSING()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	OPEN_SCRIPT_PROFILE_MARKER_GROUP("UPDATE_SERVER_PROCESSING") // MUST BE AT START OF FUNCTION
	#ENDIF
	#ENDIF
	
	INT i, j
	
//	#IF IS_DEBUG_BUILD
//		IF bWdDoStaggeredDebug
//			NET_DW_PRINT("")
//			NET_DW_PRINT("START UPDATE_SERVER_PROCESSING")
//		ENDIF
//	#ENDIF

	// debug
	#IF IS_DEBUG_BUILD
		serverBD.iHostPlayerInt = NATIVE_TO_INT(PLAYER_ID())
	#ENDIF
	
//	SET_BIT(iLocalImpExpBitSet, biL_NobodyOnFps)
	
	
	IF NOT bDoStaggeredServerLoop
		
	ELSE
		IF iServerStaggeredCount >= COUNT_OF(PlayerBD) 
			iServerStaggeredCount = 0
			CLEAR_BIT(iLocalImpExpBitSet, BiL_ServerLoopedThroughEveryone)
	//		CLEAR_BIT(iLocalImpExpBitSet, biL_FOundOmeoneNotFinishedHpv)
			CLEAR_BIT(serverbd.iServerBitSet, biS_FOundOmeoneNotFinishedHpv)
			CLEAR_BIT(iLocalImpExpBitSet, biL_FoundSomeoneNearHpv) 
			CLEAR_BIT(iLocalImpExpBitSet, biL_HpvJustLaunched)
			IF IS_BIT_SET(serverbd.iServerBitSet, biS_RunHighPriorityVehicle)
				SET_BIT(serverbd.iServerBitSet, biS_AllPlayersDone)
				CLEAR_BIT(serverbd.iServerBitSet, biS_SomeoneNearHpv)
			ENDIF
		ENDIF
		
		i = iServerStaggeredCount
		
		// listen for cars being delivered	
		IF (PlayerBD[i].iDeliveryState = PLAYER_DELIVERY_STATE_TAKE_DELIVERY)
			REPEAT MAX_NUM_OF_CARS_ON_IMPORT_EXPORT_LIST j
				IF NOT (serverBD.CarList.iCar[j] = -1)
					IF NOT (serverBD.CarList.bIsDelivered[j])
						IF (serverBD.CarList.iCar[j] = PlayerBD[i].iVehicleListID)
						//	serverBD.CarList.bIsDelivered[j] = TRUE	
						//	SET_MODEL_AS_RECENTLY_DELIVERED_FROM_LIST_ID(PlayerBD[i].iVehicleListID)
							// update car model to be streamed in
							IF (serverBD.CarList.iCar[j] = serverBD.iListIDToStreamIn)
							//	SERVER_GET_NEW_CAR_TO_STREAM_IN()
								RESET_GET_NEW_CAR_TO_STREAM_IN_TIMER()
							ENDIF
							
							serverBD.bNewList = FALSE
							serverBD.iUpdateID += 1
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF bWdDoStaggeredDebug
				
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
					NET_DW_PRINT("")
					NET_DW_PRINT_STRING_INT("*************** Start Staggered server HPV check for part = ", i)
					IF IS_NET_PLAYER_OK((NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))), FALSE)
						NET_DW_PRINT_STRING_WITH_PLAYER_NAME("That part is player... ", NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i)))
					ENDIF
				ENDIF
			ENDIF
		#ENDIF
		
		IF IS_BIT_SET(serverbd.iServerBitSet, biS_RunHighPriorityVehicle)
//			#IF IS_DEBUG_BUILD
//				IF bWdDoStaggeredDebug
//					NET_DW_PRINT("HPV Staggered server... biS_RunHighPriorityVehicle is set")
//				ENDIF
//			#ENDIF
			
			IF NOT IS_BIT_SET(serverbd.iServerBitSet, biS_FOundOmeoneNotFinishedHpv)
//				#IF IS_DEBUG_BUILD
//					IF bWdDoStaggeredDebug
//						NET_DW_PRINT("HPV Staggered server... biL_FOundOmeoneNotFinishedHpv Not set")
//					ENDIF
//				#ENDIF
			
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
					#IF IS_DEBUG_BUILD
						IF bWdDoStaggeredDebug
							NET_DW_PRINT("HPV Staggered server... part is active")
						ENDIF
					#ENDIF
				
					IF playerbd[i].hpvProg > HPV_INIT
					OR IS_PLAYER_ON_MP_AMBIENT_SCRIPT(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i)), MPAM_TYPE_IMPORTEXPORT)
						IF playerbd[i].hpvProg < HPV_FINISHED
						//	CLEAR_BIT(serverbd.iServerBitSet, biS_AllPlayersDone)
							SET_BIT(serverbd.iServerBitSet, biS_FOundOmeoneNotFinishedHpv)
							#IF IS_DEBUG_BUILD
								IF bWdDoStaggeredDebug
								//	IF i >= 0
									//	NET_DW_PRINT_STRING_WITH_PLAYER_NAME("Server thinks this player not finished high priority vehicle grab ", INT_TO_PLAYERINDEX(i))
										NET_DW_PRINT_STRING_INT("Server thinks this part not finished high priority vehicle grab ", i)
										NET_DW_PRINT_STRING_WITH_PLAYER_NAME("That part is player ", NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i)))
										NET_DW_PRINT_STRING_INT("Their hpvProg = ", ENUM_TO_INT(playerbd[i].hpvProg))
									//ENDIF
								ENDIF
							#ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
								IF bWdDoStaggeredDebug
									NET_DW_PRINT("		HPV Staggered server... Failed on playerbd[i].hpvProg < HPV_FINISHED check")
								ENDIF
							#ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							IF bWdDoStaggeredDebug
								NET_DW_PRINT("		HPV Staggered server... Failed on hpv prog / on ambient check")
								NET_DW_PRINT_STRING_INT("			HPV Staggered server... HPV prog = ", ENUM_TO_INT(playerbd[i].hpvProg))
								IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i)), MPAM_TYPE_IMPORTEXPORT)
									NET_DW_PRINT("			HPV Staggered server... Player on ambient")
								ELSE
									NET_DW_PRINT("			HPV Staggered server... Player NOT on ambient")
								ENDIF
							ENDIF
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF bWdDoStaggeredDebug
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
					NET_DW_PRINT_STRING_INT("*************** DONE Staggered server HPV check for part = ", i)
					NET_DW_PRINT("")
				ENDIF
			ENDIF
		#ENDIF
		
		//-- Check for a player near hpv
		IF IS_BIT_SET(serverbd.iServerBitSet, biS_RunHighPriorityVehicle)
			IF NOT IS_BIT_SET(iLocalImpExpBitSet, biL_FoundSomeoneNearHpv)
				IF IS_BIT_SET(playerbd[i].iImportExportBitSet, biP_NearHpv)
					SET_BIT(iLocalImpExpBitSet, biL_FoundSomeoneNearHpv)
					
					#IF IS_DEBUG_BUILD
						IF bWdUseDebug
							NET_DW_PRINT_STRING_INT("Server thinks this part near high priority vehicle grab ", i)
						ENDIF
					#ENDIF
				
				ENDIF
			ENDIF 
			
			// Check for the HPV being in the garage but with no driver for some reason
			IF NOT IS_BIT_SET(iLocalImpExpBitSet, biL_SomeoneFoundHpvInGarageNoDri)
				IF NOT IS_BIT_SET(iLocalImpExpBitSet, biL_SomeoneDelSpecCar)
					IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
						IF IS_BIT_SET(playerbd[i].iImportExportBitSet, biP_HpvInGarageNoDriver)
							SET_BIT(iLocalImpExpBitSet, biL_SomeoneFoundHpvInGarageNoDri)
							#IF IS_DEBUG_BUILD
								NET_DW_PRINT_STRING_WITH_PLAYER_NAME("This player detected the HPV in the gargae without a driver ", NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i)))
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		iServerStaggeredCount++
		
		IF iServerStaggeredCount >= COUNT_OF(PlayerBD) 
			SET_BIT(iLocalImpExpBitSet, BiL_ServerLoopedThroughEveryone)
			IF IS_BIT_SET(serverbd.iServerBitSet, biS_FOundOmeoneNotFinishedHpv)
			OR IS_BIT_SET(iLocalImpExpBitSet, biL_HpvJustLaunched) 	// Need to handle HPV launching when staggered loop is not at beginning of loop
																	// Otherwise HPV will terminate due to not finding anyone running HPV due to
																	// staggered loop already having gone passed the active participant
				CLEAR_BIT(serverbd.iServerBitSet, biS_AllPlayersDone)	
			ENDIF
			IF IS_BIT_SET(iLocalImpExpBitSet, biL_FoundSomeoneNearHpv)
				SET_BIT(serverbd.iServerBitSet, biS_SomeoneNearHpv)
			ENDIF
			IF IS_BIT_SET(iLocalImpExpBitSet, biL_SomeoneFoundHpvInGarageNoDri)
				SET_BIT(serverbd.iServerBitSet, biS_SomeoneFoundCarInGarageNoDri)
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("Server loop")
	#ENDIF
	#ENDIF
		
	// listen for the board getting switched on and off
/*	IF (serverBD.bBoardIsOn)
		IF RECEIVED_GENERAL_EVENT(GENERAL_EVENT_TYPE_SWITCH_IMPORT_EXPORT_BOARD_OFF)
			serverBD.bBoardIsOn = FALSE
		ENDIF
		IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME() , serverBD.iLightTimer) > 30000)
			IF IS_BIT_SET(iLocalImpExpBitSet, biL_NobodyOnFps)
				serverBD.bBoardIsOn = FALSE				
			ELSE
				serverBD.iLightTimer = GET_NETWORK_TIME()
			ENDIF
		ENDIF
	ELSE
		IF RECEIVED_GENERAL_EVENT(GENERAL_EVENT_TYPE_SWITCH_IMPORT_EXPORT_BOARD_ON)										
			serverBD.bBoardIsOn = TRUE
			serverBD.iLightTimer = GET_NETWORK_TIME()
		ENDIF
	ENDIF
	*/
	
	
	// rotate car population
//	IF serverBD.iCarListState > LIST_STATE_INIT //--Wait until list is created before streaming a model in
	IF IS_CAR_LIST_VALID() //--Wait until list is created before streaming a model in
		IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), serverBD.iStreamInCarTimer) > 180000) //
			SERVER_GET_NEW_CAR_TO_STREAM_IN()					
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("SERVER_GET_NEW_CAR_TO_STREAM_IN")
	#ENDIF
	#ENDIF
	
	// update car list
	SWITCH serverBD.iCarListState
		CASE LIST_STATE_INIT
			ServerCreateList()	
			serverBD.iUpdateID += 1
			serverBD.iCarListState = LIST_STATE_RUNNING
			INIT_RECENTLY_DELIVERED()
			#IF IS_DEBUG_BUILD NET_DW_PRINT("serverBD.iCarListState LIST_STATE_INIT -> LIST_STATE_RUNNING") #ENDIF
		BREAK
		
		CASE LIST_STATE_RUNNING
			// wait for cars to be scored off list
			IF ServerAreAllCarsDelivered()
				iListCompletedTimer = GET_NETWORK_TIME()
				
				serverBD.iCarListState = LIST_STATE_WAITING_TO_CLEAR
				#IF IS_DEBUG_BUILD NET_DW_PRINT("serverBD.iCarListState LIST_STATE_RUNNING -> LIST_STATE_WAITING_TO_CLEAR") #ENDIF
			ELSE
				IF (serverBD.iListIDToStreamIn = -1)
				//	SERVER_GET_NEW_CAR_TO_STREAM_IN()
					RESET_GET_NEW_CAR_TO_STREAM_IN_TIMER()
				ENDIF
			ENDIF
		BREAK
		CASE LIST_STATE_WAITING_TO_CLEAR
			IF NOT (serverBD.bBoardIsOn)
			AND (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), iListCompletedTimer) > g_sMPTunables.itime_simeon_sends_text_ms) //g_sMPTunables.itime_simeon_sends_text_ms)
		
				serverBD.iCarListState = LIST_STATE_INIT
				#IF IS_DEBUG_BUILD NET_DW_PRINT("serverBD.iCarListState LIST_STATE_WAITING_TO_CLEAR -> LIST_STATE_INIT") #ENDIF
			ENDIF
		BREAK
		
		CASE LIST_STATE_WAIT_FOR_HPV
		//	IF NOT IS_BIT_SET(serverbd.iServerBitSet, biS_RunHighPriorityVehicle)
		//		serverBD.iCarListState = LIST_STATE_INIT
		//	ENDIF
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("LIST_STATE_RUNNING")
	#ENDIF
	#ENDIF
	
//	MAINTAIN_RECENTLY_DELIVERED()
	
	MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER()
	
//	#IF IS_DEBUG_BUILD
//		IF bWdDoStaggeredDebug
//			NET_DW_PRINT("DONE UPDATE_SERVER_PROCESSING")
//			NET_DW_PRINT("")
//		ENDIF
//	#ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_HIGH_PRIORITY_VEHICLE_SERVER")
	#ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	CLOSE_SCRIPT_PROFILE_MARKER_GROUP() 
	#ENDIF
	#ENDIF
ENDPROC


FUNC BOOL OK_TO_SEND_IMPORT_EXPORT_TEXT()
	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
		
		RETURN FALSE
	ENDIF
	
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		
		RETURN FALSE
	ENDIF
	
	IF NOT IS_SCREEN_FADED_IN()
		
		RETURN FALSE
	ENDIF
	
	IF IS_CUSTOM_MENU_ON_SCREEN()
		
		RETURN FALSE
	ENDIF
	
	IF IS_FM_MISSION_LAUNCH_IN_PROGRESS()
		
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
		
		RETURN FALSE
	ENDIF
	

	
	IF IS_MISSION_SUMMARY_SCREEN_DISPLAYED()
		
		RETURN FALSE
	ENDIF
	
	IF IS_PED_INJURED(PLAYER_PED_ID())
		
		RETURN FALSE
	ENDIF
	
	IF IS_PAUSE_MENU_ACTIVE()
		
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
		
		RETURN FALSE
	ENDIF
	
//	IF GET_IS_LEADERBOARD_CAM_ACTIVE_OR_INTERPING() 
//		PRINTLN("False 12")
//		RETURN FALSE
//	ENDIF
	
	IF g_Show_Lap_Dpad // Chris' dpad-down player list
		
		RETURN FALSE
	ENDIF
	
	IF SHOULD_TRANSITION_SESSIONS_HIDE_RADAR_UNTILL_LAUNCHED()
		
		RETURN FALSE
	ENDIF
	
	IF IS_TRANSITION_SESSION_LAUNCHING()
		
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_CORONA()

		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		RETURN FALSE
	ENDIF
	IF IS_BROWSER_OPEN()
		//-- Internet active
		RETURN FALSE
	ENDIF
	
	IF NOT IS_SKYSWOOP_AT_GROUND()
		RETURN FALSE
	ENDIF
	
	IF Is_MP_Comms_Still_Playing()
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_IN_TUTORIAL_SESSION()
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC


PROC MAINTAIN_TEXT_MESSAGE_V2()
//	#IF IS_DEBUG_BUILD
//		NET_DW_PRINT("MAINTAIN_TEXT_MESSAGE_V2 called")
//	#ENDIF
	INT iStatInt
	
	
	//IF NOT HAVE_I_RECEIVED_A_CAR_LIST_TODAY()
	IF bNewCarList
		MAINTAIN_GLOBAL_CAR_LISTS()
		IF NOT IS_BIT_SET(iLocalImpExpBitSet, biL_SentInitialText)
			
			IF mpglobalsambience.iNumberOfImportExportWantedCars > 0
				IF mpglobalsambience.time_ImportExportTextMessage = NULL	
					mpglobalsambience.time_ImportExportTextMessage = GET_NETWORK_TIME()
					#IF IS_DEBUG_BUILD
						NET_DW_PRINT("Initialised time_ImportExportTextMessage")
					#ENDIF
				ELSE
					IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), mpglobalsambience.time_ImportExportTextMessage) >g_sMPTunables.itime_simeon_sends_text_ms) // )
						#IF IS_DEBUG_BUILD
						//	NET_DW_PRINT("Time ok")
						#ENDIF
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF IS_CAR_LIST_VALID()
								IF IS_OK_TO_PRINT_HELP()
									IF NOT Is_Any_MP_Comms_Active_Or_Pending()
										IF NOT IS_PHONE_ONSCREEN()
										AND NOT SHOULD_HIDE_TEXT_BECAUSE_OF_ANY_EVENT()
										
											g_CarListMessageDecision = NEW_CAR_LIST
											iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_IMP_EXP) 
											IF NOT IS_BIT_SET(iStatInt, BI_IMPEXP_FIRST_LAUNCH)
												IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), mpglobalsambience.time_ImportExportTextMessage) > 20000) // 1545838
													//-- Hello soldier, here is shopping list. Keep them looking nice yes?
													IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS (CHAR_SIMEON, 
																													"CELL_CLTEST6", 
																													TXTMSG_LOCKED, 
																													"NULL", 
																													mpglobalsambience.iNumberOfImportExportWantedCars, 
																													"TestSender1", 
																													CAR_LIST_COMPONENT, 
																													DEFAULT, 
																													TXTMSG_DO_NOT_AUTO_UNLOCK, 
																													DEFAULT, 
																													DEFAULT, 
																													bSHowNewTextFeed, 
																													DEFAULT, 
																													CANNOT_CALL_SENDER) 
														
														SET_BIT(iStatInt, BI_IMPEXP_FIRST_LAUNCH)
														SET_MP_INT_CHARACTER_STAT(MP_STAT_FM_IMP_EXP, iStatInt)
														SET_BIT(iLocalImpExpBitSet, biL_SentInitialText)
														ADD_NAMED_ACTIVITY_TO_DISPLAY_LIST(<<0,0,0>>,GET_ACTIVITY_NAME_FROM_FMMC_TYPE(FMMC_TYPE_IMPORT_EXPORT),"")
														mpglobalsambience.bHasImportExportList = TRUE // Needed for reminder phonecall
														bNewCarList = FALSE
														#IF IS_DEBUG_BUILD NET_DW_PRINT("Sent text CELL_CLTEST6")#ENDIF
													ELSE
														#IF IS_DEBUG_BUILD
															NET_DW_PRINT("Unable to send text CELL_CLTEST6")
														#ENDIF
													ENDIF
												ELSE
												//	#IF IS_DEBUG_BUILD NET_DW_PRINT("Else 98") #ENDIF
												ENDIF
											ELSE
												//-- Check if we can print help so we can hopefully get the help message to display at the same time as the text arrives
												IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS (CHAR_SIMEON, 
																												"CELL_CLTEST1", 
																												TXTMSG_LOCKED, 
																												"NULL", 
																												mpglobalsambience.iNumberOfImportExportWantedCars, 
																												"TestSender1", 
																												CAR_LIST_COMPONENT,
																												DEFAULT, 
																												TXTMSG_DO_NOT_AUTO_UNLOCK, 
																												DEFAULT, 
																												DEFAULT, 
																												bSHowNewTextFeed, 
																												DEFAULT, 
																												CANNOT_CALL_SENDER)
																												
													ADD_NAMED_ACTIVITY_TO_DISPLAY_LIST(<<0,0,0>>,GET_ACTIVITY_NAME_FROM_FMMC_TYPE(FMMC_TYPE_IMPORT_EXPORT),"")
													SET_BIT(iLocalImpExpBitSet, biL_SentInitialText)
													mpglobalsambience.bHasImportExportList = TRUE // Needed for reminder phonecall
													bNewCarList = FALSE
													#IF IS_DEBUG_BUILD NET_DW_PRINT("Sent text CELL_CLTEST1")#ENDIF
												ELSE
													#IF IS_DEBUG_BUILD
														NET_DW_PRINT("Unable to send text CELL_CLTEST1")
													#ENDIF
												ENDIF
											ENDIF
											//
											
											//g_CarListMessageDecision = UPDATED_CAR_LIST
											#IF IS_DEBUG_BUILD 
											//	NET_DW_PRINT_STRING_INT("[MAINTAIN_TEXT_MESSAGE] Sending initial text, iNumberOfImportExportWantedCars = ", mpglobalsambience.iNumberOfImportExportWantedCars)
											#ENDIF
										ELSE
											#IF IS_DEBUG_BUILD NET_DW_PRINT("Else 7") #ENDIF
										ENDIF
									ELSE
										#IF IS_DEBUG_BUILD 
											NET_DW_PRINT("[MAINTAIN_TEXT_MESSAGE] Want to send initial text, but Is_Any_MP_Comms_Active_Or_Pending ")
										#ENDIF
										mpglobalsambience.time_ImportExportTextMessage = GET_NETWORK_TIME()
									ENDIF
								ELSE
									#IF IS_DEBUG_BUILD 
									//	NET_DW_PRINT("[MAINTAIN_TEXT_MESSAGE] Else 6")
									#ENDIF
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD NET_DW_PRINT("Else 5") #ENDIF
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD NET_DW_PRINT("Else 4") #ENDIF
						ENDIF
					ELSE
					//	#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("Else 3, time diff = ", GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), mpglobalsambience.time_ImportExportTextMessage)) #ENDIF
					ENDIF

				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD NET_DW_PRINT("Else 1")#ENDIF
			ENDIF
		ELSE
			
			
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalImpExpBitSet, biL_SentInitialText)
			IF NOT IS_BIT_SET(iBS_Help, HELP_LAUNCH_IMPEXP)

				IF IS_OK_TO_PRINT_HELP()
					
					#IF IS_DEBUG_BUILD NET_DW_PRINT("Set HELP_LAUNCH_IMPEXP") #ENDIF
					IF bSHowNewTextFeed
						PRINT_HELP("IMPEX_HELP_LNCH") // Check your phone for details of vehicles required by Jim Rivera.
						
					ENDIF
					SET_BIT(iBS_Help, HELP_LAUNCH_IMPEXP)
					START_NET_TIMER(timeInitialHelp)
				ELSE
					#IF IS_DEBUG_BUILD NET_DW_PRINT("ELSE HELP_LAUNCH_IMPEXP") #ENDIF
				ENDIF
					
			ELSE
			//	#IF IS_DEBUG_BUILD NET_DW_PRINT("Already set HELP_LAUNCH_IMPEXP")#ENDIF
			ENDIF 
		ENDIF
	ENDIF
	
	IF bSendThanksTextMessage
		IF OK_TO_SEND_IMPORT_EXPORT_TEXT()
			IF NOT HAS_NET_TIMER_STARTED(timeThankYouText)
				START_NET_TIMER(timeThankYouText)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("Started timeThankYouText")#ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(timeThankYouText, 10000)
					IF Request_MP_Comms_Txtmsg(CHAR_SIMEON, "CELL_IMPT")
						bSendThanksTextMessage = FALSE
						RESET_NET_TIMER(timeThankYouText)
						#IF IS_DEBUG_BUILD NET_DW_PRINT("Sent thank you up text")#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//-- Have a random model on the list stored in globals, which other scripts can access
PROC MAINTAIN_GLOBAL_CAR_MODEL()
	INT iNewCar
	//-- See also MAINTAIN_GLOBAL_CAR_LISTS()
	//IF serverBD.iCarListState > LIST_STATE_INIT
	IF IS_CAR_LIST_VALID()
		IF MPGlobalsAmbience.gCarModelOnList = DUMMY_MODEL_FOR_SCRIPT
		OR IS_BIT_SET(MPGlobalsAmbience.iGlobImpExpBitset, BI_IMPEXP_NEED_NEW_RANDOM_MODEL)
			IF NOT HAS_NET_TIMER_STARTED(timeGetRandomCarCHeck)
			OR HAS_NET_TIMER_EXPIRED(timeGetRandomCarCHeck, 2000)
			//	iNewCar = GetRandomCarOnWantedList(serverBD.CarList)
				iNewCar = GetRandomCarOnWantedList(ThisCarList)
				IF iNewCar > -1
					MPGlobalsAmbience.gCarModelOnList = GET_MODEL_NAME_FROM_LIST_ID(iNewCar)
					RESET_NET_TIMER(timeGlobalOnListCheck)
					START_NET_TIMER(timeGlobalOnListCheck)
					IF IS_BIT_SET(MPGlobalsAmbience.iGlobImpExpBitset, BI_IMPEXP_NEED_NEW_RANDOM_MODEL)
						CLEAR_BIT(MPGlobalsAmbience.iGlobImpExpBitset, BI_IMPEXP_NEED_NEW_RANDOM_MODEL)
						#IF IS_DEBUG_BUILD NET_DW_PRINT("Cleared BI_IMPEXP_NEED_NEW_RANDOM_MODEL" ) #ENDIF
					ENDIF
					#IF IS_DEBUG_BUILD
						NET_DW_PRINT_STRINGS("[MAINTAIN_GLOBAL_CAR_MODEL] Using model ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(MPGlobalsAmbience.gCarModelOnList)) 
					#ENDIF
				ELSE	
					#IF IS_DEBUG_BUILD NET_DW_PRINT("[MAINTAIN_GLOBAL_CAR_MODEL] GetRandomCarOnWantedList returning -1!") #ENDIF
					RESET_NET_TIMER(timeGetRandomCarCHeck)
					START_NET_TIMER(timeGetRandomCarCHeck)
				ENDIF
			ENDIF
		ELSE	
			IF MPGlobalsAmbience.gCarModelOnList <> DUMMY_MODEL_FOR_SCRIPT
				IF HAS_NET_TIMER_EXPIRED(timeGlobalOnListCheck, 20000)
					IF NOT IS_MODEL_ON_WANTED_LIST(ThisCarList, MPGlobalsAmbience.gCarModelOnList)
						MPGlobalsAmbience.gCarModelOnList = DUMMY_MODEL_FOR_SCRIPT
						
						#IF IS_DEBUG_BUILD NET_DW_PRINT("[MAINTAIN_GLOBAL_CAR_MODEL] Resetting MPGlobalsAmbience.gCarModelOnList as model not on list") #ENDIF
					ENDIF
					RESET_NET_TIMER(timeGlobalOnListCheck)
					START_NET_TIMER(timeGlobalOnListCheck)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_PLAYER_DRIVING_HPV_TICKER()

	IF IS_BIT_SET(iLocalImpExpBitSet, BiL_LoopedThroughEveryone)
		IF iTickerPartDrivingHpv <> iLocalPartDrivingHpv
			IF iLocalPartDrivingHpv > -1
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iLocalPartDrivingHpv))
					PLAYER_INDEX playerWithCar =  NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iLocalPartDrivingHpv))
					IF playerWithCar <> PLAYER_ID()
						IF NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE)
							// ~a~ ~s~has taken the high priority Import/Export vehicle.
							PRINT_TICKER_WITH_PLAYER_NAME( "IMPEX_TICK_DHPV",playerWithCar)
						
							#IF IS_DEBUG_BUILD
								NET_DW_PRINT_STRING_WITH_PLAYER_NAME("Doing HPV driver ticker with player... ", playerWithCar)
								NET_DW_PRINT_HOST_NAME()
							#ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
								NET_DW_PRINT("Not doing took-hpv ticker as local player in property")
							#ENDIF
						ENDIF
					ENDIF
					
					IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
						IF NOT IS_BIT_SET(serverbd.iServerBitSet, biS_SomeoneEnteredCarAtLeastOnce) 
							SET_BIT(serverbd.iServerBitSet,biS_SomeoneEnteredCarAtLeastOnce)
							#IF IS_DEBUG_BUILD
								NET_DW_PRINT("Set biS_SomeoneEnteredCarAtLeastOnce")
							#ENDIF
						ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						NET_DW_PRINT("Not doing took-hpv ticker as part not active")
					#ENDIF
				ENDIF
			ENDIF
			iTickerPartDrivingHpv = iLocalPartDrivingHpv
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_HPV_AUGMENTED_HUD()
	PLAYER_INDEX playerWithCar
	BOOL bSomeoneDrivingCar, bSameTutSession
	
	//-- Don't want to display overhead icon when car is driving into mod shop
	//-- Check for this by checking local player is in same tut session as player driving car.
	IF iLocalPartDrivingHpv > -1
		IF iLocalPartDrivingHpv <> PARTICIPANT_ID_TO_INT()
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iLocalPartDrivingHpv))
		 		playerWithCar = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iLocalPartDrivingHpv))
				bSomeoneDrivingCar = TRUE
				
				IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(playerWithCar, PLAYER_ID())
					bSameTutSession = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		 
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPriorityVeh)
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niPriorityVeh)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niPriorityVeh))
					IF PlayerBD[PARTICIPANT_ID_TO_INT()].iDeliveryState = PLAYER_DELIVERY_STATE_WAITING_FOR_CAR
						IF NOT bSomeoneDrivingCar
							OR (bSomeoneDrivingCar AND bSameTutSession)
							
							DRAW_SPRITE_ON_OBJECTIVE_VEHICLE_THIS_FRAME(NET_TO_VEH(serverBD.niPriorityVeh))
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
ENDPROC


PROC CHECK_LOCAL_PLAYER_NEAR_HPV()
	//-- CHeck for local player being near Hpv
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPriorityVeh)
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niPriorityVeh)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niPriorityVeh))
					IF NOT IS_BIT_SET(playerbd[PARTICIPANT_ID_TO_INT()].iImportExportBitSet, biP_NearHpv)
						#IF IS_DEBUG_BUILD NET_DW_PRINT("Setting biP_NearHpv because I'm in Hpv") #ENDIF
						SET_BIT(playerbd[PARTICIPANT_ID_TO_INT()].iImportExportBitSet, biP_NearHpv)
					ENDIF
				ELSE
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niPriorityVeh)) < 180.0
						IF NOT IS_BIT_SET(playerbd[PARTICIPANT_ID_TO_INT()].iImportExportBitSet, biP_NearHpv)
							#IF IS_DEBUG_BUILD NET_DW_PRINT("Setting biP_NearHpv because I'm near Hpv") #ENDIF
							SET_BIT(playerbd[PARTICIPANT_ID_TO_INT()].iImportExportBitSet, biP_NearHpv)
						ENDIF
					ELSE
						IF IS_BIT_SET(playerbd[PARTICIPANT_ID_TO_INT()].iImportExportBitSet, biP_NearHpv)
							#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing biP_NearHpv because I'm not near Hpv") #ENDIF
							CLEAR_BIT(playerbd[PARTICIPANT_ID_TO_INT()].iImportExportBitSet, biP_NearHpv)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


SCRIPT_TIMER timeInGarageNoDriver
FUNC BOOL CHECK_FOR_HPV_IN_GARAGE_WITHOUT_DRIVER()
	VECTOR vCoord
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPriorityVeh)
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niPriorityVeh)
			IF NOT DOES_HPV_HAVE_DRIVER()
				vCoord = GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niPriorityVeh))
				IF IS_COORD_NEAR_GARAGE(vCoord, MP_GAR_SIMEON)
					IF IsCarInContactGarage(NET_TO_VEH(serverBD.niPriorityVeh), TRUE)
						PRINTLN("[FM_ImportExport] [dsw] [CHECK_FOR_HPV_IN_GARAGE_WITHOUT_DRIVER] TRUE SIMEON ")
						RETURN TRUE
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF	
	
	RETURN FALSE
ENDFUNC

PROC CHECK_FOR_HPV_IN_MOD_SHOP_WITHOUT_DRIVER()
	VECTOR vWarp
	VECTOR vCoord
	FLOAT fHeading
	FLOAT fWeighting
	
	BOOL bDoWarp
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPriorityVeh)
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niPriorityVeh)
			IF NOT DOES_HPV_HAVE_DRIVER()
				IF NOT HAS_NET_TIMER_STARTED(timeInGarageNoDriver)
					START_NET_TIMER(timeInGarageNoDriver)
				ENDIF
				IF HAS_NET_TIMER_EXPIRED(timeInGarageNoDriver, 1000)
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niPriorityVeh)
						vCoord = GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niPriorityVeh))
						
						//-- Mod shops
						IF IS_POSITION_INSIDE_SPECIFC_INTERIOR(vCoord, GET_SHOP_INTERIOR_TYPE(CARMOD_SHOP_01_AP), GET_SHOP_COORDS(CARMOD_SHOP_01_AP))
							IF GET_CARMOD_WARP_POSITION(CARMOD_SHOP_01_AP, 0, vWarp, fHeading, fWeighting)
								PRINTLN("[FM_ImportExport] [dsw] [CHECK_FOR_HPV_IN_MOD_SHOP_WITHOUT_DRIVER] TRUE CARMOD_SHOP_01_AP ")
								bDoWarp = TRUE
							ENDIF
						ELIF IS_POSITION_INSIDE_SPECIFC_INTERIOR(vCoord, GET_SHOP_INTERIOR_TYPE(CARMOD_SHOP_05_ID2), GET_SHOP_COORDS(CARMOD_SHOP_05_ID2))
							IF GET_CARMOD_WARP_POSITION(CARMOD_SHOP_05_ID2, 0, vWarp, fHeading, fWeighting)
								PRINTLN("[FM_ImportExport] [dsw] [CHECK_FOR_HPV_IN_MOD_SHOP_WITHOUT_DRIVER] TRUE CARMOD_SHOP_05_ID2 ")
								bDoWarp = TRUE
							ENDIF
							
						ELIF IS_POSITION_INSIDE_SPECIFC_INTERIOR(vCoord, GET_SHOP_INTERIOR_TYPE(CARMOD_SHOP_06_BT1), GET_SHOP_COORDS(CARMOD_SHOP_06_BT1))
							IF GET_CARMOD_WARP_POSITION(CARMOD_SHOP_06_BT1, 0, vWarp, fHeading, fWeighting)
								PRINTLN("[FM_ImportExport] [dsw] [CHECK_FOR_HPV_IN_MOD_SHOP_WITHOUT_DRIVER] TRUE CARMOD_SHOP_06_BT1 ")
								bDoWarp = TRUE
							ENDIF
							
						ELIF IS_POSITION_INSIDE_SPECIFC_INTERIOR(vCoord, GET_SHOP_INTERIOR_TYPE(CARMOD_SHOP_07_CS1), GET_SHOP_COORDS(CARMOD_SHOP_07_CS1))
							IF GET_CARMOD_WARP_POSITION(CARMOD_SHOP_07_CS1, 0, vWarp, fHeading, fWeighting)
								PRINTLN("[FM_ImportExport] [dsw] [CHECK_FOR_HPV_IN_MOD_SHOP_WITHOUT_DRIVER] TRUE CACARMOD_SHOP_07_CS1RMOD_SHOP_01_AP ")
								bDoWarp = TRUE
							ENDIF
							
						ELIF IS_POSITION_INSIDE_SPECIFC_INTERIOR(vCoord, GET_SHOP_INTERIOR_TYPE(CARMOD_SHOP_08_CS6), GET_SHOP_COORDS(CARMOD_SHOP_08_CS6))
							IF GET_CARMOD_WARP_POSITION(CARMOD_SHOP_08_CS6, 0, vWarp, fHeading, fWeighting)
								PRINTLN("[FM_ImportExport] [dsw] [CHECK_FOR_HPV_IN_MOD_SHOP_WITHOUT_DRIVER] TRUE CARMOD_SHOP_08_CS6 ")
								bDoWarp = TRUE
							ENDIF
						ELIF IS_POSITION_INSIDE_SPECIFC_INTERIOR(vCoord, GET_SHOP_INTERIOR_TYPE(CARMOD_SHOP_SUPERMOD), GET_SHOP_COORDS(CARMOD_SHOP_SUPERMOD))
							IF GET_CARMOD_WARP_POSITION(CARMOD_SHOP_SUPERMOD, 0, vWarp, fHeading, fWeighting)
								PRINTLN("[FM_ImportExport] [dsw] [CHECK_FOR_HPV_IN_MOD_SHOP_WITHOUT_DRIVER] TRUE CARMOD_SHOP_SUPERMOD ")
								bDoWarp = TRUE
							ENDIF
						ENDIF
					ENDIF
					RESET_NET_TIMER(timeInGarageNoDriver)
				ENDIF
				
				IF bDoWarp
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niPriorityVeh)
						IF IS_VEHICLE_EMPTY(NET_TO_VEH(serverBD.niPriorityVeh), TRUE)
							PRINTLN("[FM_ImportExport] [dsw] [CHECK_FOR_HPV_IN_MOD_SHOP_WITHOUT_DRIVER] I'm warping HPV to coord ", vWarp) 
							SET_ENTITY_COORDS_NO_OFFSET(NET_TO_VEH(serverBD.niPriorityVeh), vWarp)
							SET_ENTITY_HEADING(NET_TO_VEH(serverBD.niPriorityVeh), fHeading)
							NETWORK_FADE_IN_ENTITY(NET_TO_VEH(serverBD.niPriorityVeh), TRUE)
							FREEZE_ENTITY_POSITION(NET_TO_VEH(serverBD.niPriorityVeh), FALSE)
							SET_VEHICLE_DOORS_LOCKED(NET_TO_VEH(serverBD.niPriorityVeh), VEHICLELOCK_UNLOCKED)

						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
					
ENDPROC

PROC ADD_BLIP_FOR_HPV()
	g_blipHighPriorityVeh = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(serverbd.niPriorityVeh))
	IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(NET_TO_VEH(serverbd.niPriorityVeh)))
		SET_BLIP_SPRITE(g_blipHighPriorityVeh,RADAR_TRACE_GANG_BIKE )
	ELSE
		SET_BLIP_SPRITE(g_blipHighPriorityVeh,RADAR_TRACE_GANG_VEHICLE )
	ENDIF
	SET_BLIP_COLOUR(g_blipHighPriorityVeh, BLIP_COLOUR_GREEN)
	SET_BLIP_NAME_FROM_TEXT_FILE(g_blipHighPriorityVeh, "IMPEX_HIPR_BLP")
	IF SHOULD_HIDE_LOCAL_UI()
		SET_BLIP_DISPLAY(g_blipHighPriorityVeh, DISPLAY_NOTHING)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_HPV_CLEANUP()
	IF (playerbd[PARTICIPANT_ID_TO_INT()].hpvProg > HPV_INIT
	AND playerbd[PARTICIPANT_ID_TO_INT()].hpvProg < HPV_FINISHED)
	OR (playerbd[PARTICIPANT_ID_TO_INT()].hpvProg = HPV_INIT
	AND IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_IMPORTEXPORT))
		
		IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
		OR IS_FM_MISSION_LAUNCH_IN_PROGRESS()
			
			IF NOT IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_IMPROMPTU_DM)
			AND NOT IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_RACE_TO_POINT)
			AND NOT IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_GB_BOSS_DEATHMATCH)
				#IF IS_DEBUG_BUILD NET_DW_PRINT(" [SHOULD_HPV_CLEANUP] TRUE because player on a mission") #ENDIF
				RETURN TRUE
			ELSE
				
				IF NOT bRemoveHpvBlipOnImpromptu
					IF DOES_BLIP_EXIST(g_blipHighPriorityVeh)
						REMOVE_BLIP(g_blipHighPriorityVeh)
						#IF IS_DEBUG_BUILD NET_DW_PRINT(" [SHOULD_HPV_CLEANUP] Removed HPV blip as on impromptu race / DM") #ENDIF
					ENDIF
					bRemoveHpvBlipOnImpromptu = TRUE
				ENDIF
			ENDIF
		ELSE
			IF bRemoveHpvBlipOnImpromptu
				#IF IS_DEBUG_BUILD NET_DW_PRINT(" [SHOULD_HPV_CLEANUP] Re-adding HPV blip as was on impromptu race / DM") #ENDIF
				ADD_BLIP_FOR_HPV()
				
				bRemoveHpvBlipOnImpromptu = FALSE
			ENDIF
		ENDIF
		
		IF serverbd.hpvServerProg = HPV_FINISHED
			IF (playerbd[PARTICIPANT_ID_TO_INT()].hpvProg = HPV_INIT
			AND IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_IMPORTEXPORT))
				#IF IS_DEBUG_BUILD NET_DW_PRINT(" [SHOULD_HPV_CLEANUP] TRUE because I'm set on ambient, but server has finished") #ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	
	ENDIF
	
	RETURN FALSE
ENDFUNC



///	PURPOSE:
///    Don't won't to display the HPV blip if the player is in a property
PROC MAINTAIN_HPV_BLIP_IN_PROPERTY()
	IF DOES_BLIP_EXIST(g_blipHighPriorityVeh)
		IF SHOULD_HIDE_LOCAL_UI()
		OR ARE_TEMP_HIDDEN_BLIPS_ACTIVE()
			SET_BLIP_DISPLAY(g_blipHighPriorityVeh, DISPLAY_NOTHING)
		ELSE
			SET_BLIP_DISPLAY(g_blipHighPriorityVeh, DISPLAY_BOTH)
		ENDIF
		IF NOT bHpvBlipSetShortRange
			IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE)
				SET_BLIP_AS_SHORT_RANGE(g_blipHighPriorityVeh, TRUE)
				bHpvBlipSetShortRange = TRUE
				#IF IS_DEBUG_BUILD
					NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_CLIENT] [MAINTAIN_HPV_BLIP_IN_PROPERTY] Set blip as short range as player in property")
				#ENDIF
			ENDIF
		ELSE
			IF NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE)
				SET_BLIP_AS_SHORT_RANGE(g_blipHighPriorityVeh, FALSE)
				bHpvBlipSetShortRange = FALSE
				
				#IF IS_DEBUG_BUILD
					NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_CLIENT] [MAINTAIN_HPV_BLIP_IN_PROPERTY] Blip no longer short range as no longer in property")
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC MAINTAIN_HIGH_PRIORITY_VEHICLE_CLIENT()
	
	IF IS_BIT_SET(serverbd.iServerBitSet, biS_RunHighPriorityVehicle)
		PLAYER_INDEX playerTemp
		PED_INDEX pedPlayer
		
		IF iMainLoopCount >= NUM_NETWORK_PLAYERS
			CLEAR_BIT(iLocalImpExpBitSet, BiL_LoopedThroughEveryone)
			iMainLoopCount = 0
			iLocalPartDrivingHpv = -1
		ENDIF
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE( INT_TO_PARTICIPANTINDEX(iMainLoopCount))
			playerTemp = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iMainLoopCount))
			pedPlayer = GET_PLAYER_PED(playerTemp)
			IF IS_NET_PLAYER_OK(playerTemp, FALSE)
				IF NOT IS_BIT_SET(iLocalImpExpBitSet, biL_SomeoneDelSpecCar)
					IF IS_BIT_SET(playerbd[iMainLoopCount].iImportExportBitSet, biP_DeliveredSpecVehicle)
						SET_BIT(iLocalImpExpBitSet, biL_SomeoneDelSpecCar)
						#IF IS_DEBUG_BUILD
							NET_DW_PRINT_STRING_WITH_PLAYER_NAME("I think this player was in car when it was dropped off ", playerTemp)
						#ENDIF
					ENDIF
				ENDIF
				IF iLocalPartDrivingHpv = -1
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.niPriorityVeh)
						IF IS_NET_VEHICLE_DRIVEABLE(serverbd.niPriorityVeh)
							IF NOT IS_PED_INJURED(pedPlayer)
								IF IS_PED_SITTING_IN_VEHICLE(pedPlayer, NET_TO_VEH(serverbd.niPriorityVeh))
									IF GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(serverbd.niPriorityVeh)) = pedPlayer
										iLocalPartDrivingHpv = iMainLoopCount
										#IF IS_DEBUG_BUILD
											IF bWdUseDebug
												NET_DW_PRINT_STRING_WITH_PLAYER_NAME("I think this player has taken the hpv ", playerTemp)
											ENDIF
										#ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		iMainLoopCount++
		
		IF iMainLoopCount >= NUM_NETWORK_PLAYERS
			SET_BIT(iLocalImpExpBitSet, BiL_LoopedThroughEveryone)
		ENDIF
		
		CHECK_LOCAL_PLAYER_NEAR_HPV()
		
		IF NOT IS_BIT_SET(iLocalImpExpBitSet, biL_SomeoneDelSpecCar)
			IF NOT IS_BIT_SET(playerbd[PARTICIPANT_ID_TO_INT()].iImportExportBitSet, biP_HpvInGarageNoDriver)
				IF NOT IS_BIT_SET(playerbd[PARTICIPANT_ID_TO_INT()].iImportExportBitSet, biP_DeliveredSpecVehicle)
					IF CHECK_FOR_HPV_IN_GARAGE_WITHOUT_DRIVER()	
						SET_BIT(playerbd[PARTICIPANT_ID_TO_INT()].iImportExportBitSet, biP_HpvInGarageNoDriver)
						#IF IS_DEBUG_BUILD NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_CLIENT] I detected HPV in garage without driver!") #ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE	
		#IF IS_DEBUG_BUILD
			IF bWdDoHpvDebug
			//	NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_CLIENT] biS_RunHighPriorityVehicle not set!")
			ENDIF
		#ENDIF
	ENDIF
	
	IF iLocalReserveCount <> serverBD.iNumReservedVeh
		
		IF serverBD.iNumReservedVeh = 0	
			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPriorityVeh)	
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("Num reserved vehicles don't match. Old value... ", iLocalReserveCount) #ENDIF
				iLocalReserveCount = serverBD.iNumReservedVeh
				RESERVE_NETWORK_MISSION_VEHICLES(iLocalReserveCount)
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("iLocalReserveCount updated, reserving... ", iLocalReserveCount) #ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("Num reserved vehicles don't match. Old value... ", iLocalReserveCount) #ENDIF
			iLocalReserveCount = serverBD.iNumReservedVeh
			RESERVE_NETWORK_MISSION_VEHICLES(iLocalReserveCount)
			#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("iLocalReserveCount updated, reserving... ", iLocalReserveCount) #ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(serverbd.iServerBitSet, biS_RunHighPriorityVehicle)
		SWITCH playerbd[PARTICIPANT_ID_TO_INT()].hpvProg
			CASE HPV_INIT
				IF NOT MPGlobalsAmbience.bPlayerFailedHPVChecks 
					#IF IS_DEBUG_BUILD 
						NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_CLIENT] Trying to launch....") 
					#ENDIF
				ENDIF
				IF IS_BIT_SET(playerbd[PARTICIPANT_ID_TO_INT()].iImportExportBitSet, biP_HpvInGarageNoDriver)
					CLEAR_BIT(playerbd[PARTICIPANT_ID_TO_INT()].iImportExportBitSet, biP_HpvInGarageNoDriver)
				ENDIF
				
				IF IS_BIT_SET(iLocalImpExpBitSet, biL_SomeoneFoundHpvInGarageNoDri)
					CLEAR_BIT(iLocalImpExpBitSet, biL_SomeoneFoundHpvInGarageNoDri)
				ENDIF
				
				IF IS_BIT_SET(playerbd[PARTICIPANT_ID_TO_INT()].iImportExportBitSet, biP_DeliveredSpecVehicle)
					CLEAR_BIT(playerbd[PARTICIPANT_ID_TO_INT()].iImportExportBitSet, biP_DeliveredSpecVehicle)
				ENDIF
				
				IF (NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
				AND NOT IS_FM_MISSION_LAUNCH_IN_PROGRESS()
				AND NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE))
				OR IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_IMPORTEXPORT)
					#IF IS_DEBUG_BUILD 
						NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_CLIENT] Passed initial launch checks....") 
					#ENDIF
					IF IS_BIT_SET(iLocalImpExpBitSet, biL_SomeoneDelSpecCar)
						CLEAR_BIT(iLocalImpExpBitSet, biL_SomeoneDelSpecCar)
					ENDIF
					
					IF IS_BIT_SET(iLocalImpExpBitSet, biL_DoneJackHelpTextThisInstance)
						CLEAR_BIT(iLocalImpExpBitSet, biL_DoneJackHelpTextThisInstance)
					ENDIF
					
					
					
					IF IS_BIT_SET(playerbd[PARTICIPANT_ID_TO_INT()].iImportExportBitSet, biP_HpvInGarageNoDriver)
						CLEAR_BIT(playerbd[PARTICIPANT_ID_TO_INT()].iImportExportBitSet, biP_HpvInGarageNoDriver)
					ENDIF
					IF iTickerPartDrivingHpv <> -1 
						iTickerPartDrivingHpv = -1
					ENDIF
					IF IS_BIT_SET(serverbd.iServerBitSet, biS_RunHighPriorityVehicle)
						IF NOT IS_BIT_SET(iLocalImpExpBitSet, biL_ReservedForHpv)
						
							SET_BIT(iLocalImpExpBitSet, biL_ReservedForHpv)
							SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_IMPORTEXPORT, TRUE)
							SET_BIT(iLocalImpExpBitSet, biL_SetLocalPlayerOnAmb)
							MPGlobalsAmbience.bPlayerOnImportExportHpv = TRUE
							MPGlobalsAmbience.bPlayerFailedHPVChecks = FALSE
							#IF IS_DEBUG_BUILD 
								NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_CLIENT] Set MPGlobalsAmbience.bPlayerOnImportExportHpv  - 1") 
							#ENDIF
						
						ENDIF
						IF CAN_REGISTER_MISSION_VEHICLES(1)
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.niPriorityVeh)
							//	IF IS_BIT_SET(iLocalImpExpBitSet, biL_SentInitialText)
							//	OR HAVE_I_DELIVERED_A_VEHICLE_TODAY()
									IF IS_OK_TO_PRINT_HELP()
										IF serverbd.mHighPriority <> DUMMY_MODEL_FOR_SCRIPT
											SET_VEHICLE_MODEL_IS_SUPPRESSED(serverbd.mHighPriority, TRUE)
											#IF IS_DEBUG_BUILD NET_DW_PRINT_STRINGS(" [MAINTAIN_HIGH_PRIORITY_VEHICLE_CLIENT] Suppressing HPV model. Model is ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(serverbd.mHighPriority)) #ENDIF
										ENDIF
										playerbd[PARTICIPANT_ID_TO_INT()].hpvProg = HPV_GET_CAR
										g_blipHighPriorityVeh = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(serverbd.niPriorityVeh))
									//	SET_BLIP_SPRITE(g_blipHighPriorityVeh,RADAR_TRACE_GANG_VEHICLE )
									//	PRINT_HELP("IMPEX_HIPR_INT") // Jim Rivera wants a specific ~b~vehicle~s~ delivered. Get it resprayed ~BLIP_CAR_MOD_SHOP~ and deliver it to him at the docks for cash.
										
										
										IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(NET_TO_VEH(serverbd.niPriorityVeh)))
											SET_BLIP_SPRITE(g_blipHighPriorityVeh,RADAR_TRACE_GANG_BIKE )
											PRINT_HELP("IMPEX_HIPB_INT") // Jim Rivera wants a specific ~b~vehicle~s~ delivered. Get it resprayed ~BLIP_CAR_MOD_SHOP~ and deliver it to him at the docks for cash.
										ELSE 
											SET_BLIP_SPRITE(g_blipHighPriorityVeh,RADAR_TRACE_GANG_VEHICLE )
											PRINT_HELP("IMPEX_HIPR_INT") // Jim Rivera wants a specific ~b~vehicle~s~ delivered. Get it resprayed ~BLIP_CAR_MOD_SHOP~ and deliver it to him at the docks for cash.
										ENDIF
										SET_BLIP_COLOUR(g_blipHighPriorityVeh, BLIP_COLOUR_GREEN)
										SET_BLIP_NAME_FROM_TEXT_FILE(g_blipHighPriorityVeh, "IMPEX_HIPR_BLP")
										SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_IMPORTEXPORT, TRUE)
										SET_BIT(iLocalImpExpBitSet, biL_SetLocalPlayerOnAmb)
										MPGlobalsAmbience.bPlayerOnImportExportHpv = TRUE
										IF SHOULD_HIDE_LOCAL_UI()
											SET_BLIP_DISPLAY(g_blipHighPriorityVeh, DISPLAY_NOTHING)
										ENDIF
										
										#IF IS_DEBUG_BUILD 
											NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_CLIENT] Set MPGlobalsAmbience.bPlayerOnImportExportHpv  - 2") 
										#ENDIF
						
										#IF IS_DEBUG_BUILD
											NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_CLIENT] HPV_INIT -> HPV_GET_CAR")
										#ENDIF
									ELSE
										#IF IS_DEBUG_BUILD
											NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_CLIENT] IS_OK_TO_PRINT_HELP")
										#ENDIF
									ENDIF

//								ELSE
//									#IF IS_DEBUG_BUILD
//									//	IF bWdDoHpvDebug
//											NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_CLIENT] in else 4")
//									//	ENDIF
//									#ENDIF
//								ENDIF
							ELSE	
								#IF IS_DEBUG_BUILD
									IF bWdDoHpvDebug
										NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_CLIENT] in else 3")
									ENDIF
								#ENDIF
							ENDIF
						ELSE	
							#IF IS_DEBUG_BUILD
							//	IF bWdDoHpvDebug
									NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_CLIENT] in else 2")
							//	ENDIF
							#ENDIF
						ENDIF
						
					ENDIF
				ELSE	
					#IF IS_DEBUG_BUILD
						IF bWdDoHpvDebug
							NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_CLIENT] in else 1")
						ENDIF
					#ENDIF
					
					IF NOT MPGlobalsAmbience.bPlayerFailedHPVChecks 
						// For Martin's launcher
						MPGlobalsAmbience.bPlayerFailedHPVChecks = TRUE
						#IF IS_DEBUG_BUILD
							NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_CLIENT] Failed to launch, set bPlayerFailedHPVChecks")
						#ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE HPV_GET_CAR
				
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.niPriorityVeh)
				//	IF IS_NET_VEHICLE_DRIVEABLE(serverbd.niPriorityVeh)
					IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverbd.niPriorityVeh))
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverbd.niPriorityVeh))
								TURN_OFF_PASSIVE_MODE_OPTION(TRUE)
								IF IS_MP_PASSIVE_MODE_ENABLED()
									DISABLE_MP_PASSIVE_MODE(PMER_START_OF_A_MATCH, FALSE)
									#IF IS_DEBUG_BUILD
										NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_CLIENT] Calling DISABLE_MP_PASSIVE_MODE(FALSE)")
									#ENDIF
								ENDIF
								IF DOES_BLIP_EXIST(g_blipHighPriorityVeh)
									REMOVE_BLIP(g_blipHighPriorityVeh)
								ENDIF
							ELSE
								TURN_OFF_PASSIVE_MODE_OPTION(FALSE)
								
								IF NOT IsCarInContactGarage(NET_TO_VEH(serverbd.niPriorityVeh))
									IF NOT DOES_BLIP_EXIST(g_blipHighPriorityVeh)
										IF NOT bRemoveHpvBlipOnImpromptu
											ADD_BLIP_FOR_HPV()
										ENDIF
										//SET_BLIP_COLOUR(g_blipHighPriorityVeh, BLIP_COLOUR_BLUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						//-- Help text about jacking player
						IF NOT IS_BIT_SET(iLocalImpExpBitSet, biL_DoneJackHelpTextThisInstance)
							IF IS_OK_TO_PRINT_HELP()
								IF DOES_HPV_HAVE_DRIVER()
									IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverbd.niPriorityVeh))
										IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niPriorityVeh))) < 2500.0
											IF SHOULD_HIDE_LOCAL_UI()
												PRINT_HELP("FM_IMP_JAC") //You can jack another Player from a vehicle by holding ~INPUT_ENTER~ while stood next to the vehicle.
											ENDIF
											SET_BIT(iLocalImpExpBitSet, biL_DoneJackHelpTextThisInstance)
											#IF IS_DEBUG_BUILD NET_DW_PRINT("Displaying jack-players help") #ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF MISSION_VEHICLE_STUCK_CHECKS()
						/*	#IF IS_DEBUG_BUILD
							NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_CLIENT] HPV_GET_CAR -> HPV_FINISHED as vehicle stuck")
							#ENDIF
							IF DOES_BLIP_EXIST(g_blipHighPriorityVeh)
								REMOVE_BLIP(g_blipHighPriorityVeh)
							ENDIF
							SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_IMPORTEXPORT, FALSE)
							CLEAR_BIT(iLocalImpExpBitSet, biL_SetLocalPlayerOnAmb)
							playerbd[PARTICIPANT_ID_TO_INT()].hpvProg = HPV_FINISHED */
						ENDIF
						
						MAINTAIN_HPV_BLIP_IN_PROPERTY()
					ELSE
						#IF IS_DEBUG_BUILD
							NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_CLIENT] HPV_GET_CAR -> HPV_FINISHED as vehicle not driveablet")
						#ENDIF
						IF DOES_BLIP_EXIST(g_blipHighPriorityVeh)
							REMOVE_BLIP(g_blipHighPriorityVeh)
						ENDIF
					//	PRINT_HELP("IMPEX_HIPR_WRK") // DOne as a ticker message
						SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_IMPORTEXPORT, FALSE)
						CLEAR_BIT(iLocalImpExpBitSet, biL_SetLocalPlayerOnAmb)
						playerbd[PARTICIPANT_ID_TO_INT()].hpvProg = HPV_FINISHED
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_CLIENT] HPV_GET_CAR -> HPV_FINISHED as network index doesn't exist")
					#ENDIF
					IF serverbd.mHighPriority <> DUMMY_MODEL_FOR_SCRIPT
						SET_VEHICLE_MODEL_IS_SUPPRESSED(serverbd.mHighPriority, FALSE)
						#IF IS_DEBUG_BUILD NET_DW_PRINT(" [MAINTAIN_HIGH_PRIORITY_VEHICLE_CLIENT] NOT suppressing HPV model - Check 5") #ENDIF
					ENDIF
					
					IF DOES_BLIP_EXIST(g_blipHighPriorityVeh)
						REMOVE_BLIP(g_blipHighPriorityVeh)
					ENDIF
					SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_IMPORTEXPORT, FALSE)
					CLEAR_BIT(iLocalImpExpBitSet, biL_SetLocalPlayerOnAmb)
					playerbd[PARTICIPANT_ID_TO_INT()].hpvProg = HPV_FINISHED
				ENDIF
				
				IF IS_BIT_SET(iLocalImpExpBitSet, biL_SomeoneDelSpecCar)
					#IF IS_DEBUG_BUILD
						NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_CLIENT] HPV_GET_CAR -> HPV_FINISHED as car delivered")
					#ENDIF
					SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_IMPORTEXPORT, FALSE)
					CLEAR_BIT(iLocalImpExpBitSet, biL_SetLocalPlayerOnAmb)
					
					IF DOES_BLIP_EXIST(g_blipHighPriorityVeh)
						REMOVE_BLIP(g_blipHighPriorityVeh)
					ENDIF
						
					playerbd[PARTICIPANT_ID_TO_INT()].hpvProg = HPV_FINISHED
				ENDIF
				
				IF serverbd.hpvServerProg = HPV_FINISHED
					IF playerbd[PARTICIPANT_ID_TO_INT()].hpvProg = HPV_GET_CAR
						#IF IS_DEBUG_BUILD
							NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_CLIENT] HPV_GET_CAR -> HPV_FINISHED as server says so.")
						#ENDIF
						IF serverbd.mHighPriority <> DUMMY_MODEL_FOR_SCRIPT
							SET_VEHICLE_MODEL_IS_SUPPRESSED(serverbd.mHighPriority, FALSE)
							#IF IS_DEBUG_BUILD NET_DW_PRINT(" [MAINTAIN_HIGH_PRIORITY_VEHICLE_CLIENT] NOT suppressing HPV model - Check 1") #ENDIF
						ENDIF
						IF DOES_BLIP_EXIST(g_blipHighPriorityVeh)
							REMOVE_BLIP(g_blipHighPriorityVeh)
						ENDIF
						
						IF IS_BIT_SET(iLocalImpExpBitSet, biL_ReservedForHpv)
						//	RESERVE_NETWORK_MISSION_VEHICLES(0)
							CLEAR_BIT(iLocalImpExpBitSet, biL_ReservedForHpv)
						//	#IF IS_DEBUG_BUILD NET_DW_PRINT("Client reserved 0 vehicle - check 4") #ENDIF
						ENDIF
				
						SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_IMPORTEXPORT, FALSE)
						CLEAR_BIT(iLocalImpExpBitSet, biL_SetLocalPlayerOnAmb)
						playerbd[PARTICIPANT_ID_TO_INT()].hpvProg = HPV_FINISHED
					ENDIF
				ENDIF
			BREAK
			
			
			CASE HPV_FINISHED
				TURN_OFF_PASSIVE_MODE_OPTION(FALSE)
			BREAK
		ENDSWITCH
		
//		IF (playerbd[PARTICIPANT_ID_TO_INT()].hpvProg > HPV_INIT
//		AND playerbd[PARTICIPANT_ID_TO_INT()].hpvProg < HPV_FINISHED)
//		OR (playerbd[PARTICIPANT_ID_TO_INT()].hpvProg = HPV_INIT
//		AND IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_IMPORTEXPORT))
		
//			IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
//			OR IS_FM_MISSION_LAUNCH_IN_PROGRESS()

		IF SHOULD_HPV_CLEANUP()
			#IF IS_DEBUG_BUILD NET_DW_PRINT(" [MAINTAIN_HIGH_PRIORITY_VEHICLE_CLIENT] Cleaning up hpv...") #ENDIF
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("IMPEX_HIPR_INT")
				CLEAR_HELP()
			ENDIF
			IF DOES_BLIP_EXIST(g_blipHighPriorityVeh)
				REMOVE_BLIP(g_blipHighPriorityVeh)
			ENDIF
			IF serverbd.mHighPriority <> DUMMY_MODEL_FOR_SCRIPT
				SET_VEHICLE_MODEL_IS_SUPPRESSED(serverbd.mHighPriority, FALSE)
				#IF IS_DEBUG_BUILD NET_DW_PRINT(" [MAINTAIN_HIGH_PRIORITY_VEHICLE_CLIENT] NOT suppressing HPV model - CHeck 2") #ENDIF
			ENDIF
			playerbd[PARTICIPANT_ID_TO_INT()].hpvProg = HPV_FINISHED
			SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_IMPORTEXPORT, FALSE)
			CLEAR_BIT(iLocalImpExpBitSet, biL_SetLocalPlayerOnAmb)
			
			IF IS_BIT_SET(iLocalImpExpBitSet, biL_ReservedForHpv)
			//	RESERVE_NETWORK_MISSION_VEHICLES(0)
				CLEAR_BIT(iLocalImpExpBitSet, biL_ReservedForHpv)
			//	#IF IS_DEBUG_BUILD NET_DW_PRINT("Client reserved 0 vehicle - check 3") #ENDIF
			ENDIF
			
			TURN_OFF_PASSIVE_MODE_OPTION(FALSE)
		//	ENDIF
		ENDIF
		
		MAINTAIN_PLAYER_DRIVING_HPV_TICKER()
		
		IF playerbd[PARTICIPANT_ID_TO_INT()].hpvProg > HPV_INIT
		AND playerbd[PARTICIPANT_ID_TO_INT()].hpvProg < HPV_FINISHED
			MAINTAIN_HPV_AUGMENTED_HUD()
		ENDIF
		
	ELSE
		IF playerbd[PARTICIPANT_ID_TO_INT()].hpvProg <> HPV_INIT
			IF serverbd.mHighPriority <> DUMMY_MODEL_FOR_SCRIPT
				SET_VEHICLE_MODEL_IS_SUPPRESSED(serverbd.mHighPriority, FALSE)
				#IF IS_DEBUG_BUILD NET_DW_PRINT(" [MAINTAIN_HIGH_PRIORITY_VEHICLE_CLIENT] NOT suppressing HPV model - CHeck 3") #ENDIF
			ENDIF
				
			#IF IS_DEBUG_BUILD
				NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_CLIENT] -> HPV_INIT as resetting")
			#ENDIF
						
			playerbd[PARTICIPANT_ID_TO_INT()].hpvProg = HPV_INIT
		ENDIF
		
		IF IS_BIT_SET(iLocalImpExpBitSet, biL_SomeoneFoundHpvInGarageNoDri)
			CLEAR_BIT(iLocalImpExpBitSet, biL_SomeoneFoundHpvInGarageNoDri)
			#IF IS_DEBUG_BUILD
				NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_CLIENT] Clear biL_SomeoneFoundHpvInGarageNoDri as resetting")
			#ENDIF
		ENDIF
		
		IF MPGlobalsAmbience.bPlayerOnImportExportHpv
			IF NOT HAS_NET_TIMER_STARTED(timeFinishHpv)
				START_NET_TIMER(timeFinishHpv)
				#IF IS_DEBUG_BUILD
					NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_CLIENT] Started timeFinishHpv ")
				#ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(timeFinishHpv, 5000)
					RESET_NET_TIMER(timeFinishHpv)
					MPGlobalsAmbience.bPlayerOnImportExportHpv = FALSE
					
					#IF IS_DEBUG_BUILD
						NET_DW_PRINT("[MAINTAIN_HIGH_PRIORITY_VEHICLE_CLIENT] MPGlobalsAmbience.bPlayerOnImportExportHpv = FALSE - 1")
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(iLocalImpExpBitSet, biL_ReservedForHpv)
			IF serverBD.iNumReservedVeh = 0
			//	RESERVE_NETWORK_MISSION_VEHICLES(0)
				CLEAR_BIT(iLocalImpExpBitSet, biL_ReservedForHpv)
			//	#IF IS_DEBUG_BUILD NET_DW_PRINT("Client reserved 0 vehicle - check 2") #ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	CHECK_FOR_HPV_IN_MOD_SHOP_WITHOUT_DRIVER()
ENDPROC



//TIME_DATATYPE time_LaunchImportExport
FUNC BOOL SHOULD_IMPORT_EXPORT_RUN()

	IF NOT IS_BIT_SET(iLocalImpExpBitSet, biL_OkToRun)
	/*	IF time_LaunchImportExport = NULL
			time_LaunchImportExport = GET_NETWORK_TIME()
			#IF IS_DEBUG_BUILD NET_DW_PRINT("time_LaunchImportExport initialised") #ENDIF
		ELSE 
			IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), time_LaunchImportExport) > 10000) */
		//	#IF IS_DEBUG_BUILD OR bActivateInFlow #ENDIF
			IF NOT IS_PLAYER_SCTV(PLAYER_ID()) 
				IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
				AND NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
				AND NOT IS_FM_MISSION_LAUNCH_IN_PROGRESS()
				AND ((NOT IS_PED_INJURED(PLAYER_PED_ID())) AND IS_PLAYER_CONTROL_ON(PLAYER_ID()))
				AND IS_SCREEN_FADED_IN()
				AND IS_PLAYER_RANK_OK_FOR_ACTIVITY_UNLOCK(FMMC_TYPE_IMPORT_EXPORT)
				AND NOT IS_PLAYER_TEST_DRIVING_A_VEHICLE(PLAYER_ID())
					SET_BIT(iLocalImpExpBitSet, biL_OkToRun)
				ENDIF
				
				#IF IS_DEBUG_BUILD
					IF bActivateInFlow
						SET_BIT(iLocalImpExpBitSet, biL_OkToRun)
					ENDIF
				#ENDIF
			ENDIF
	//	ENDIF
	ENDIF
	
	
	RETURN (IS_BIT_SET(iLocalImpExpBitSet, biL_OkToRun))
ENDFUNC


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                          ///////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

SCRIPT(MP_MISSION_DATA missionScriptArgs)
	
	
	IF NOT PROCESS_PRE_GAME(missionScriptArgs)
		#IF IS_DEBUG_BUILD
			NET_DW_PRINT("Failed to receive initial network broadcast. Cleaning up.")
		#ENDIF
		CLEANUP_SCRIPT()
	ENDIF

	
	#IF IS_DEBUG_BUILD
		CREATE_WIDGETS()	
	#ENDIF
	

		
	// initialise rich pop zones
	RichPopSchedules[0] = GET_ZONE_POPSCHEDULE(GET_ZONE_AT_COORDS(<<-891.3, 807.9, 188.1>>)) 	// vinewood hills 
	RichPopSchedules[1] = GET_ZONE_POPSCHEDULE(GET_ZONE_AT_COORDS(<<-276.5, -206.3, 38.4>>)) 	// burton	
	RichPopSchedules[2] = GET_ZONE_POPSCHEDULE(GET_ZONE_AT_COORDS(<<-1485.5, -644.5, 30.1>>))	// del perro	
	RichPopSchedules[3] = GET_ZONE_POPSCHEDULE(GET_ZONE_AT_COORDS(<<-1746.3, -939.5, 7.7>>)) 	// del perro beach
	RichPopSchedules[4] = GET_ZONE_POPSCHEDULE(GET_ZONE_AT_COORDS(<<-2106.4, -345.3, 13.0>>)) 	// pacific bluffs
	RichPopSchedules[5] = GET_ZONE_POPSCHEDULE(GET_ZONE_AT_COORDS(<<-1520.0, 74.4, 61.3>>)) 	// richman
	RichPopSchedules[6] = GET_ZONE_POPSCHEDULE(GET_ZONE_AT_COORDS(<<-1333.0, -286.0, 40.3>>)) 	// morningwood
	RichPopSchedules[7] = GET_ZONE_POPSCHEDULE(GET_ZONE_AT_COORDS(<<-1178.1, 54.8, 53.9>>)) 	// golf course
	RichPopSchedules[8] = GET_ZONE_POPSCHEDULE(GET_ZONE_AT_COORDS(<<-1298.7, -359.4, 36.7>>)) 	// rockford hills
	RichPopSchedules[9] = GET_ZONE_POPSCHEDULE(GET_ZONE_AT_COORDS(<<-355.2, 147.5, 75.8>>)) 	// west vinewood
	RichPopSchedules[10] = GET_ZONE_POPSCHEDULE(GET_ZONE_AT_COORDS(<<-982.1, -1064.7, 2.2>>)) 	// vespucci canals
	RichPopSchedules[11] = GET_ZONE_POPSCHEDULE(GET_ZONE_AT_COORDS(<<-1313.1, -1560.2, 4.3>>)) 	// vespucci beach	
	
	WHILE TRUE
	
		MP_LOOP_WAIT_ZERO()
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			SCRIPT_PROFILER_START_OF_FRAME()
		#ENDIF
		#ENDIF		
		
		bRequestGarageOpen = FALSE
		bDisplayImpExpBlip = FALSE
		bDisplaySimeonObjective = FALSE
		bDisplayLeaveCarmodObjective = FALSE
		bPlayerInVehicleReadyForDelivery = FALSE
		mpglobalsambience.bIsInImportExportCar = FALSE

		// If we have a match end event, bail.
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		OR IS_PLAYER_IN_CORONA()
	//	OR NOT IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
		//OR SHOULD_MP_AMBIENT_SCRIPT_TERMINATE(MPAM_TYPE_IMPORTEXPORT)
			#IF IS_DEBUG_BUILD
				IF IS_PLAYER_IN_CORONA()
					NET_DW_PRINT("Cleaning up because IS_PLAYER_IN_CORONA")
				ENDIF
			#ENDIF
			CLEANUP_SCRIPT()
		ENDIF	
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER( "Cleanup checks")
		#ENDIF
		
			IF NOT HAS_NET_TIMER_STARTED(timeDebug)
				START_NET_TIMER(timeDebug)
				
			ELSE
				IF HAS_NET_TIMER_EXPIRED(timeDebug, 300000)
					bTimedDebug = TRUE
					NET_DW_PRINT("Turned on timed debug output....")
					RESET_NET_TIMER(timeDebug)
				ENDIF 
			ENDIF
		#ENDIF 
		
		#IF IS_DEBUG_BUILD
			UPDATE_WIDGETS()
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER( "UPDATE_WIDGETS")
			#ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		IF g_bLaunchImportExportHpv
			IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
				NET_DW_PRINT("g_bLaunchImportExportHpv is set, but I'm not host, so clearing.")
				NET_DW_PRINT_HOST_NAME()
				g_bLaunchImportExportHpv = FALSE // Debug menu
			ENDIF
		ENDIF
		#ENDIF
// *****************************************************************************************************************************************************************	
// *****************************************************************************************************************************************************************	
//												LOCAL PROCESSING
// *****************************************************************************************************************************************************************	
// *****************************************************************************************************************************************************************

	//	IF CNC_FLOW_Get_flow_Flag_State(ciCNC_FLOW_CLIENT_CAR_EXPORT_ACTIVE)
		IF SHOULD_IMPORT_EXPORT_RUN()
				
			#IF IS_DEBUG_BUILD
				IF g_bFM_IgnoreRankOnNextLaunchedActivity
					g_bFM_IgnoreRankOnNextLaunchedActivity = FALSE
				ENDIF
			#ENDIF
			

			//	ThisCarList = serverBD.CarList
				IF NOT HAVE_I_RECEIVED_A_CAR_LIST_TODAY()
					IF NOT HAVE_I_DELIVERED_A_VEHICLE_TODAY()
						IF OK_TO_SEND_IMPORT_EXPORT_TEXT()
							IF NOT bNewCarList
								#IF IS_DEBUG_BUILD NET_DW_PRINT("******** Time for a new car list...") #ENDIF
								DELETE_IMPORT_EXPORT_TEXT_MESSAGES()
								GET_TODAYS_LIST_OF_CARS(ThisCarList, MAX_NUM_OF_CARS_ON_IMPORT_EXPORT_LIST )
								
//								#IF IS_DEBUG_BUILD
//									
//									REPEAT 	MAX_NUM_OF_CARS_ON_IMPORT_EXPORT_LIST i	
//										IF NOT (ThisCarList.iCar[i] = -1)
//											PRINTLN("Model on list...",  GET_MODEL_NAME_FOR_DEBUG(GET_MODEL_NAME_FROM_LIST_ID(ThisCarList.iCar[i])))
//										ENDIF
//									ENDREPEAT
//								#ENDIF
//								
								
								SET_RECEIVED_CAR_LIST_TODAY()
								//TEXT_MESSAGE_DELAY = 10000
								bSHowNewTextFeed = TRUE
								bNewCarListThisSession = TRUE
								IF IS_BIT_SET(iLocalImpExpBitSet, biL_SentInitialText)
									CLEAR_BIT(iLocalImpExpBitSet, biL_SentInitialText)
									#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing biL_SentInitialText") #ENDIF
								ENDIF
								
								bNewCarList = TRUE
								iLocalUpdateID = -999
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT HAVE_I_DELIVERED_A_VEHICLE_TODAY()
						//-- If I don't have a list, re-send my last list
						IF NOT bNewCarListThisSession
							IF NOT HAS_NET_TIMER_STARTED(timeWaitForCarListToArrive)
								START_NET_TIMER(timeWaitForCarListToArrive)
								#IF IS_DEBUG_BUILD
									NET_DW_PRINT("Started timeWaitForCarListToArrive")
								#ENDIF
							ELSE
								IF HAS_NET_TIMER_EXPIRED(timeWaitForCarListToArrive, 100)
									IF NOT IS_BIT_SET(iLocalImpExpBitSet, biL_SentInitialText)
										IF OK_TO_SEND_IMPORT_EXPORT_TEXT()
											#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("******** Re-sending list of cars for day = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMyLastImportExportListDay ) #ENDIF
											RESEND_LIST_OF_CARS_FOR_DAY(ThisCarList, MAX_NUM_OF_CARS_ON_IMPORT_EXPORT_LIST,g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMyLastImportExportListDay )
											//TEXT_MESSAGE_DELAY = 0
											bSHowNewTextFeed = FALSE
											bNewCarListThisSession = TRUE
											bNewCarList = TRUE
											iLocalUpdateID = -999
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			
		
			
			
			UPDATE_VEHICLE_DELIVERY()
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("UPDATE_VEHICLE_DELIVERY")
			#ENDIF
			#ENDIF
			
			MAINTAIN_MECHANIC_PED()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MECHANIC_PED")
			#ENDIF
			#ENDIF
			
			UPDATE_HELP_IMPORT_EXPORT()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("UPDATE_HELP_IMPORT_EXPORT")
			#ENDIF
			#ENDIF
			
			UPDATE_STREAMED_IN_CAR_MODEL()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("UPDATE_STREAMED_IN_CAR_MODEL")
			#ENDIF
			#ENDIF
			
			// sending of text messages
		//	MAINTAIN_TEXT_MESSAGE()
		
			MAINTAIN_TEXT_MESSAGE_V2()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_TEXT_MESSAGE")
			#ENDIF
			#ENDIF			
			
			
			MAINTAIN_HIGH_PRIORITY_VEHICLE_CLIENT()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_HIGH_PRIORITY_VEHICLE_CLIENT")
			#ENDIF
			#ENDIF	
			
//			MAINTAIN_BONUS_XP() 
//			#IF IS_DEBUG_BUILD
//			#IF SCRIPT_PROFILER_ACTIVE
//				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_BONUS_XP")
//			#ENDIF
//			#ENDIF	
		ELSE
			
		ENDIF
		
		UPDATE_GARAGE_DOORS()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("UPDATE_GARAGE_DOORS")
		#ENDIF
		#ENDIF		
		
		
		UPDATE_IMPEX_BLIP()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("UPDATE_IMPEX_BLIP")
		#ENDIF
		#ENDIF
		
		UPDATE_IMPEX_OBJECTIVE()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("UPDATE_IMPEX_OBJECTIVE")
		#ENDIF
		#ENDIF
		// release context 
	/*	IF NOT (bContextActive)
		RELEASE_CONTEXT_INTENTION(iContextID)
		ENDIF		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("RELEASE_CONTEXT_INTENTION")
		#ENDIF
		#ENDIF		
*/
		
		// update list of car
/*		MAINTAIN_GLOBAL_CAR_LISTS()		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_GLOBAL_CAR_LISTS")
		#ENDIF
		#ENDIF
		*/
		MAINTAIN_GLOBAL_CAR_MODEL()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_GLOBAL_CAR_MODEL")
		#ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("DisableModelHides")
			IF (GET_FRAME_COUNT() % 30) = 0	
				PRINTLN("AM_MP_IMP_EXP - Command line: 'DisableModelHides' is active, preventing model hides from being created.")
			ENDIF
		ELSE
		#ENDIF
		
			// switch off other car list (bug 545716)
			IF NOT (bSwitchedOffOtherCarList)
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),<< 471.9266, -1308.5956, 28.2359 >>) < 1000
				//	CREATE_MODEL_HIDE(<< 471.9266, -1308.5956, 28.2359 >>, 2.0, V_ILEV_UVSTINGER, TRUE)
				//	CREATE_MODEL_HIDE(<< 471.9266, -1308.5956, 28.2359 >>, 2.0, V_ILEV_UVZTYPE, TRUE)
					CREATE_MODEL_HIDE(<< 471.9266, -1308.5956, 28.2359 >>, 2.0, V_ILEV_UVCHEETAH, TRUE)
					CREATE_MODEL_HIDE(<< 471.9266, -1308.5956, 28.2359 >>, 2.0, V_ILEV_UVENTITY, TRUE)
					CREATE_MODEL_HIDE(<< 471.9266, -1308.5956, 28.2359 >>, 2.0, V_ILEV_UVJB700, TRUE)
					CREATE_MODEL_HIDE(<< 471.9266, -1308.5956, 28.2359 >>, 2.0, V_ILEV_UVENTITY, TRUE)
					CREATE_MODEL_HIDE(<< 471.9266, -1308.5956, 28.2359 >>, 2.0, V_ILEV_UVMONROE, TRUE)
					CREATE_MODEL_HIDE(<< 471.9266, -1308.5956, 28.2359 >>, 2.0, V_ILEV_UVTEXT, TRUE)
					bSwitchedOffOtherCarList = TRUE
				ENDIF		
			ENDIF
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF

		
// *****************************************************************************************************************************************************************	
// *****************************************************************************************************************************************************************	
//												SERVER PROCESSING
// *****************************************************************************************************************************************************************	
// *****************************************************************************************************************************************************************	
	
		// server processing
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			UPDATE_SERVER_PROCESSING()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("UPDATE_SERVER_PROCESSING")
			#ENDIF
			#ENDIF				
		ENDIF	
				
	
		
		#IF IS_DEBUG_BUILD 
			IF bTimedDebug
				bTimedDebug = FALSE
			ENDIF
			#IF SCRIPT_PROFILER_ACTIVE
				SCRIPT_PROFILER_END_OF_FRAME()
			#ENDIF
		#ENDIF			
	
	ENDWHILE

ENDSCRIPT
