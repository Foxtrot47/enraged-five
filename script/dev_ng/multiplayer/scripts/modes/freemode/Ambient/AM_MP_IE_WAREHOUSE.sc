//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_MP_IE_WAREHOUSE.sc																							//
// Description: Script for managing the interior of a AM_MP_IE_WAREHOUSE. AM_MP_IE_WAREHOUSE access, spawning etc. 				//
//				is managed by AM_MP_SMPL_INTERIOR_* script while this script is launched by simple interior						//
//				script to handle anything specific to AM_MP_IE_WAREHOUSE.														//
// Written by:  Online Technical team: Ata Tabrizi,																				//
// Date:  		29.06.2016																										//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"

USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_event.sch"

USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"

USING "net_wait_zero.sch"
USING "freemode_events_header.sch"

USING "net_MP_Radio.sch"

USING "safehouse_sitting_activities.sch"

USING "net_simple_interior.sch"
USING "net_ie_delivery_private.sch"
USING "net_realty_vehicle_garage.sch"

#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF



//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════════╡ VARIABLES  ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

//Walk out server bitset const
CONST_INT SV_WLK_OUT_SERVER_RUN_PLAYER_CLEAN_UP_CHECKS		0
//Walk out player bitset const
CONST_INT SV_WLK_OUT_PLAYER_IM_OUT_OF_THE_WAY				0
//Walk out local bitset const
CONST_INT SV_WLK_OUT_LOCAL_SET_UP_WALK_OUT					0

CONST_INT SV_WALK_OUT_SAFETY_TIME 							7000
CONST_FLOAT SV_SAFE_DISTANCE 								3.0

CONST_INT MP_PROP_CREATE_SVM_VEHICLES_BS_APPLIED_DETAILS	0
CONST_INT MP_PROP_CREATE_SVM_VEHICLES_BS_MODS_LOADED		1

// Hardcoded room key which represents the vehicle warehouse.
CONST_INT ciVEHICLE_WAREHOUSE_ROOM_KEY						-1392667554
CONST_INT ciVEHICLE_WAREHOUSE_OFFICE_ROOM_KEY				1488891926

CONST_INT ciMAX_LOAD_TIME									10000

ENUM UPDATE_HOST_VEH_DATA_STATE
	UPDATE_HOST_VEH_DATA_STATE_UPDATE_VEHS = 0,
	UPDATE_HOST_VEH_DATA_STATE_UPDATE_COMPLETE
ENDENUM

ENUM UPDATE_HOST_VEH_DATA_FLOW_STATE
	UPDATE_HOST_VEH_DATA_FLOW_STATE_UPDATE_ORDER = 0,
	UPDATE_HOST_VEH_DATA_FLOW_STATE_UPDATE_VISIBILITY,
	UPDATE_HOST_VEH_DATA_FLOW_STATE_UPDATE_REQUEST_VEH_MODELS,
	UPDATE_HOST_VEH_DATA_FLOW_STATE_UPDATE_COVER_VEHS,
	UPDATE_HOST_VEH_DATA_FLOW_STATE_UPDATE_VEH_DATA
ENDENUM

STRUCT GET_OUT_OF_THE_WAY_SV_SERVER_VARS
	INT iPlayerBitSet
	INT iCreationBitSet
	INT iClientRequestBS
ENDSTRUCT

ENUM PERSONAL_MOD_GARAGE_SERVER_STATE
	PERSONAL_MOD_GARAGE_SERVER_STATE_LOADING,
	PERSONAL_MOD_GARAGE_SERVER_STATE_IDLE
ENDENUM

ENUM WAREHOUSE_RENOVATION_STATE
	WAREHOUSE_RENOVATION_STATE_FADE_OUT,
	WAREHOUSE_RENOVATION_STATE_CLEANUP_SITTING_ACTIVITIES,
	WAREHOUSE_RENOVATION_STATE_MANAGE_ENTITY_SETS,
	WAREHOUSE_RENOVATION_STATE_REFRESH_INTERIOR,
	WAREHOUSE_RENOVATION_STATE_FADE_IN
ENDENUM

ACTIVITY_SEAT_STRUCT activitySeatStruct

ENUM WAREHOUSE_LAPTOP_SCREEN_STATE
	WH_LAPTOP_SCREEN_STATE_INIT,
	WH_LAPTOP_SCREEN_STATE_LINK_RT,
	WH_LAPTOP_SCREEN_STATE_UPDATE_SCALEFORM
ENDENUM

STRUCT WAREHOUSE_LAPTOP_SCREEN_STRUCT
	WAREHOUSE_LAPTOP_SCREEN_STATE eLaptopState
	INT iRenderTargetID = -1
ENDSTRUCT

STRUCT ServerBroadcastData
	PERSONAL_MOD_GARAGE_SERVER_STATE eState = PERSONAL_MOD_GARAGE_SERVER_STATE_LOADING
	
	INT iBS
	MP_RADIO_SERVER_DATA_STRUCT MPRadioServer
	MP_PROP_ACT_SERVER_CONTROL_STRUCT activityControl
	
	INT iReserved
	INT iCreatedBS
	INT iVehicleResBS
	INT iVehicleCreated
	INT iCarSpawnBS[MAX_SVM_VEHICLES_IN_GARAGE]
	INT ivehicleSaveSlot[MAX_SVM_VEHICLES_IN_GARAGE]
	
	INT iVehicles[ciMAX_IE_OWNED_VEHICLES]
	INT iVehicleSpawnOrder[ciMAX_IE_OWNED_VEHICLES]
	INT iVehicleTotal
	
	BOOL bUpdateVehData
	BOOL bVehFadeRequired
	
	NETWORK_INDEX customVehicleNetIDs[MAX_SVM_VEHICLES_IN_GARAGE]
	PLAYER_INDEX piOwner
	
	GET_OUT_OF_THE_WAY_SV_SERVER_VARS sGetOutOfTheWay
	
	SERVER_BROADCAST_DATA_FOR_SEATS serverSeatBD
	
		INT iStage
ENDSTRUCT
ServerBroadcastData serverBD

//ACTIVITY_SEAT_STRUCT officeSeatStruct

BOOL bPhantom2Created = FALSE
BOOL bDune5Created = FALSE
BOOL bGetOutOfWayFlagClearedThisFrame

INT iServerGetOutOfWayPartCheck

OBJECT_INDEX collisionGeomPhantom
OBJECT_INDEX collisionGeomDune

VEHICLE_INDEX currentVeh
VEHICLE_INDEX lastVeh

BLIP_INDEX bLaptopBlip
BLIP_INDEX bGarageBlip

FLOAT fPVHeadings[MAX_SVM_VEHICLES_IN_GARAGE]

SCRIPT_TIMER failSafeClearVehicleDelay[MAX_SVM_VEHICLES_IN_GARAGE]
SCRIPT_TIMER st_ServerWalkOutTimeout
SCRIPT_TIMER getOutOfWayTimer
SCRIPT_TIMER stTimeToLoad

VECTOR vPVPositions[MAX_SVM_VEHICLES_IN_GARAGE]
VECTOR vServerWalkOutLocation

MP_PROP_LOCAL_CONTROL serverLocalActControl
MP_RADIO_LOCAL_DATA_STRUCT MPRadioLocal
#IF FEATURE_DLC_1_2022
SMPL_INT_PROP_WITH_SCRIPT_DATA sVendingMachine
#ENDIF

STRUCT GET_OUT_OF_THE_WAY_SV_PLAYER_VARS
	INT iPlayerBitSet
	VECTOR vGetOutOfWayLoc
ENDSTRUCT

CONST_INT PERSONAL_MOD_GARAGE_BS_IS_CAR_MOD_SCRIPT_READY 	0
CONST_INT IE_WAREHOUSE_BITSET_HIDE_OWNERS_IE_MISSION_VEH	1

CONST_INT PLAYER_BD_BS_RUNNING_INTRO_CUTSCENE				0

STRUCT PlayerBroadcastData
	INT iBS
	MP_RADIO_CLIENT_DATA_STRUCT MPRadioClient
	INT iActivityRequested
	GET_OUT_OF_THE_WAY_SV_PLAYER_VARS sGetOutOfTheWay
	
		INT iOfficeSeatArmchairID = -1
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

STRUCT GET_OUT_OF_THE_WAY_SV_LOCAL_VARS
	INT iProgressBitSet
	VECTOR vSafePos
	SCRIPT_TIMER controlTimer
ENDSTRUCT
GET_OUT_OF_THE_WAY_SV_LOCAL_VARS sGetOutOfWayVars

STRUCT VEHICLE_DATA
	// Display Vehicles
	VEHICLE_INDEX oVehicleObjects[ciMAX_IE_OWNED_VEHICLES]
	INT iSetVehicleFadeTimer[ciMAX_IE_OWNED_VEHICLES]
	INT iVehicleFadeComplete[ciMAX_IE_OWNED_VEHICLES]
	TIME_DATATYPE tdVehicleFadeTimer[ciMAX_IE_OWNED_VEHICLES]
	// Cover Vehicles
	OBJECT_INDEX oCoverVehicles[ciMAX_IE_OWNED_VEHICLES]
	INT iSetCoverVehicleFadeTimer[ciMAX_IE_OWNED_VEHICLES]
	INT iCoverVehicleFadeComplete[ciMAX_IE_OWNED_VEHICLES]
	TIME_DATATYPE tdCoverVehicleFadeTimer[ciMAX_IE_OWNED_VEHICLES]
	INT iVehNeedToHideForMission
ENDSTRUCT

ENUM UPDATE_LOCAL_VEH_DATA_STATE
	UPDATE_LOCAL_VEH_DATA_STATE_UPDATE_DISPLAY_VEHS = 0,
	UPDATE_LOCAL_VEH_DATA_STATE_UPDATE_OWNED_VEHS,
	UPDATE_LOCAL_VEH_DATA_STATE_UPDATE_COMPLETE
ENDENUM
	
ENUM PERSONAL_MOD_GARAGE_SCRIPT_STATE
	PERSONAL_MOD_GARAGE_STATE_LOADING,
	PERSONAL_MOD_GARAGE_STATE_IDLE,
	PERSONAL_MOD_GARAGE_STATE_PLAYING_INTRO,
	PERSONAL_MOD_GARAGE_STATE_LOADING_GARAGE_EXIT,
	PERSONAL_MOD_GARAGE_STATE_PLAYING_GARAGE_EXIT,
	PERSONAL_MOD_GARAGE_STATE_FINISHING_GARAGE_EXIT,
	PERSONAL_MOD_GARAGE_STATE_FADE_OUT_FOR_INTO_CUT,
	PERSONAL_MOD_GARAGE_STATE_INTRO_CUT
ENDENUM

// Server BS
CONST_INT BS_IE_WAREHOUSE_SERVER_CREATE_COLLISION_GEOM_PHANTOM_FLAG		0
CONST_INT BS_IE_WAREHOUSE_SERVER_CREATE_COLLISION_GEOM_DUNE_FLAG		1

// Player BS
CONST_INT BS_IE_WAREHOUSE_DATA_DID_A_TEAM_VEH_WARP				0
CONST_INT BS_IE_WAREHOUSE_DATA_LAUNCH_CUT_IN_RUNNING_STATE		1
CONST_INT BS_IE_WAREHOUSE_CREATE_COLLISION_GEOM_PHANTOM			2
CONST_INT BS_IE_WAREHOUSE_GARAGE_ENTRY_HELP_TEXT				3
CONST_INT BS_IE_WAREHOUSE_CREATE_COLLISION_GEOM_DUNE			4
CONST_INT BS_IE_WAREHOUSE_VEH_POSITIONS_UPDATED					5
CONST_INT BS_IE_WAREHOUSE_READY_TO_WARP_OUT_W_OWNER				6
CONST_INT BS_IE_WAREHOUSE_RESTORE_VISIBILITY_AT_END_OF_CUTSCENE	7

STRUCT PERSONAL_MOD_GARAGE_DATA
	INT iBS
	PERSONAL_MOD_GARAGE_SCRIPT_STATE eState
	IMPORT_EXPORT_GARAGES eID // ID of this PERSONAL_MOD_GARAGE
	SIMPLE_INTERIORS eSimpleInteriorID
	INT iScriptInstance
	INT iInvitingPlayer
	INT iLocalBS
	INT iWHSaveSlot
	INT iCurrentVehicleSlot = 0
	INT iVehicles[ciMAX_IE_OWNED_VEHICLES]
	INT iVehicleSpawnFlow
	INT iEntranceVehicleIndex
	INT iVehicleTotal
	
	BOOL bVehFadeRequired
	BOOL bPopulateVehicleData
	
	PLAYER_INDEX pOwner
	SIMPLE_INTERIOR_DETAILS interiorDetails
	
	STRING sChildOfChildScript
	VECTOR vGarageExitMin
	VECTOR vGarageExitMax
	SIMPLE_INTERIOR_ENTRY_ANIM_DATA garageExitAnim
	THREADID CarModThread
	SCRIPT_TIMER sCarModScriptRunTimer
	VEHICLE_DATA vehData
	
	INTERIOR_INSTANCE_INDEX interiorUnderground
	
	BOOL bApplyRenovation = FALSE
	SCRIPT_TIMER renovationTimer
	WAREHOUSE_RENOVATION_STATE iRenovationState
	IE_GARAGE_INTERIORS eActiveInterior = IE_INTERIOR_INVALID
	WAREHOUSE_LAPTOP_SCREEN_STRUCT WHLaptopData
	
	BOOL bShouldPlayIntroCut
	INT iIntroCutsceneProg
	VEHICLE_INDEX vehForCut[2]
	PED_INDEX pedPAforCut
	
	INT iSpawnActivityStage 
	INT iSpawnActivitySceneID
	STRING sSpawnActivityDict
	SCRIPT_TIMER tSpawnActivity
	
	BOOL bDrivingOut = FALSE
ENDSTRUCT
PERSONAL_MOD_GARAGE_DATA thisPERSONAL_MOD_GARAGE

// Intro Cutscene
ENUM PERSONAL_MOD_GARAGE_INTRO_CUTSCENE_STAGE
	PERSONAL_MOD_GARAGE_CUTSCENE_LOADING,
	PERSONAL_MOD_GARAGE_CUTSCENE_SHELVES_SHOT,
	PERSONAL_MOD_GARAGE_CUTSCENE_LAPTOP_SHOT,
	PERSONAL_MOD_GARAGE_CUTSCENE_LAPTOP_CLOSEUP_SHOT,
	PERSONAL_MOD_GARAGE_CUTSCENE_WORKBENCH_SHOT
ENDENUM

#IF IS_DEBUG_BUILD
STRUCT PERSONAL_MOD_GARAGE_DEBUG_DATA
	BOOL bResetCutscene
	BOOL bPrintServerBDCrates
	BOOL bGiveSpecial
	BOOL bStatsTest
	BOOL bSpawnVehicle
	BOOL bDeleteVehicle
	BOOL bEnableWidgets
	INT iVehicleID = 1
	INT iVehicleSlot
	INT iVehicleFlow
	VECTOR vDisplayVehicleCoords[ciMAX_IE_DISPLAY_VEHICLES]
	VECTOR vDisplayVehicleRotation[ciMAX_IE_DISPLAY_VEHICLES]
	VECTOR vOwnedVehicleCoords[ciMAX_IE_OWNED_VEHICLES]
	VECTOR vOwnedVehicleRotation[ciMAX_IE_OWNED_VEHICLES]
	BOOL bStartSpawnActivity
ENDSTRUCT
PERSONAL_MOD_GARAGE_DEBUG_DATA debugData
#ENDIF


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ PROCEDURES  ╞═══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL IS_PERSONAL_MOD_GARAGE_SERVER_STATE(PERSONAL_MOD_GARAGE_SERVER_STATE eState)
	RETURN serverBD.eState = eState
ENDFUNC

FUNC BOOL IS_PERSONAL_MOD_GARAGE_STATE(PERSONAL_MOD_GARAGE_SCRIPT_STATE eState)
	RETURN thisPERSONAL_MOD_GARAGE.eState = eState
ENDFUNC

PROC SET_PERSONAL_MOD_GARAGE_SERVER_STATE(PERSONAL_MOD_GARAGE_SERVER_STATE eState)
	CDEBUG1LN(DEBUG_PROPERTY, "[IE_WAREHOUSE_SCRIPT] SET_PERSONAL_MOD_GARAGE_SERVER_STATE - New state: ", ENUM_TO_INT(eState))
	serverBD.eState = eState
ENDPROC

PROC SET_PERSONAL_MOD_GARAGE_STATE(PERSONAL_MOD_GARAGE_SCRIPT_STATE eState)
	CDEBUG1LN(DEBUG_PROPERTY, "[IE_WAREHOUSE_SCRIPT] SET_PERSONAL_MOD_GARAGE_STATE - New state: ", ENUM_TO_INT(eState))
	thisPERSONAL_MOD_GARAGE.eState = eState
ENDPROC

FUNC BOOL IS_CAR_MOD_SCRIPT_READY(PERSONAL_MOD_GARAGE_DATA& sPersonalModGarage)
	RETURN IS_BIT_SET(sPersonalModGarage.iLocalBS,PERSONAL_MOD_GARAGE_BS_IS_CAR_MOD_SCRIPT_READY)
ENDFUNC

FUNC BOOL IS_VEHICLE_PLACED_ON_CARRACK(INT iSlotID)
	SWITCH iSlotID
		CASE 14
		CASE 19	
		CASE 20	
		CASE 21	
		CASE 22	
		CASE 32	
		CASE 33	
		CASE 34	
		CASE 35	
		CASE 36	
		CASE 37	
		CASE 38	
		CASE 39 
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════╡ PERSONAL_MOD_GARAGE / GARAGE DOOR EXIT ANIM  ╞═════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════════════════╛

PROC INITIALISE_PERSONAL_MOD_GARAGE_GARAGE_EXIT()
	thisPERSONAL_MOD_GARAGE.garageExitAnim.dictionary = "anim@apt_trans@garage"
	thisPERSONAL_MOD_GARAGE.garageExitAnim.clip = "gar_open_1_left"
	thisPERSONAL_MOD_GARAGE.garageExitAnim.startingPhase = 0.0
	thisPERSONAL_MOD_GARAGE.garageExitAnim.endingPhase = 0.45
	
	thisPERSONAL_MOD_GARAGE.garageExitAnim.strOnEnterFadeOutFinishSoundSet = "GTAO_EXEC_WH_GARAGE_DOOR_SOUNDS"
	thisPERSONAL_MOD_GARAGE.garageExitAnim.strOnEnterFadeOutFinishSoundName = "Door_Open"
	

	thisPERSONAL_MOD_GARAGE.vGarageExitMin = <<1104.275, -3101.6143, -40.0000>>
	thisPERSONAL_MOD_GARAGE.vGarageExitMax = <<1105.4827, -3097.0706, -36.9999>>
	
	thisPERSONAL_MOD_GARAGE.garageExitAnim.syncScenePos = <<1105.095, -3099.446, -40.000>>
	thisPERSONAL_MOD_GARAGE.garageExitAnim.syncSceneRot = <<0.0, 0.0, 26.64>>

	
//	_SET_CAM_COORDS_SAME_AS_GARAGE_EXIT_PERSONAL_MOD_GARAGE(thisPERSONAL_MOD_GARAGE.garageExitAnim)
ENDPROC

FUNC BOOL IS_PLAYER_IN_PERSONAL_MOD_GARAGE_EXIT_LOCATE()
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND IS_ENTITY_IN_AREA(PLAYER_PED_ID(), thisPERSONAL_MOD_GARAGE.vGarageExitMin, thisPERSONAL_MOD_GARAGE.vGarageExitMax)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC START_EXIT_VIA_GARAGE_DOOR()
	g_SimpleInteriorData.bShouldExitMenuBeVisible = FALSE
	START_SIMPLE_INTERIOR_ENTRY_ANIM(thisPERSONAL_MOD_GARAGE.garageExitAnim)
	SET_PERSONAL_MOD_GARAGE_STATE(PERSONAL_MOD_GARAGE_STATE_LOADING_GARAGE_EXIT)
ENDPROC

PROC MAINTAIN_EXIT_VIA_GARAGE_DOOR()
	MAINTAIN_SIMPLE_INTERIOR_ENTRY_ANIM(thisPERSONAL_MOD_GARAGE.garageExitAnim, thisPERSONAL_MOD_GARAGE.eSimpleInteriorID)
ENDPROC

PROC CLEAN_UP_PERSONAL_CAR_MOD()
	PRINTLN("CLEAN_UP_PERSONAL_CAR_MOD - called")
	g_bCleanUpCarmodShop = TRUE
	g_sShopSettings.bShopScriptLaunched[ENUM_TO_INT(CARMOD_SHOP_PERSONALMOD)] = FALSE
	CLEAR_BIT(thisPERSONAL_MOD_GARAGE.iLocalBS,PERSONAL_MOD_GARAGE_BS_IS_CAR_MOD_SCRIPT_READY)
	RESET_NET_TIMER(thisPERSONAL_MOD_GARAGE.sCarModScriptRunTimer)
	IF NOT IS_STRING_NULL_OR_EMPTY(thisPERSONAL_MOD_GARAGE.sChildOfChildScript)
		SET_SCRIPT_AS_NO_LONGER_NEEDED(thisPERSONAL_MOD_GARAGE.sChildOfChildScript)
	ENDIF	
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════╡ SPAWN INTERIOR VEHICLES  ╞═════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL IS_IE_VEHICLE_SLOT_AVALIABLE(INT iVehicleSlot)
	RETURN (thisPERSONAL_MOD_GARAGE.vehData.oVehicleObjects[iVehicleSlot] = NULL)
ENDFUNC

FUNC BOOL IS_IE_COVER_VEHICLE_SLOT_AVALIABLE(INT iVehicleSlot)
	RETURN (thisPERSONAL_MOD_GARAGE.vehData.oCoverVehicles[iVehicleSlot] = NULL)
ENDFUNC

//FUNC MODEL_NAMES GET_IE_COVER_VEHICLE_MODEL(INT iCoverVehicleID)
//	INT iVehicleID = (iCoverVehicleID - ciIE_COVER_VEHICLE_INDEX)
//	MODEL_NAMES eCoverVehModel
//	IE_VEHICLE_ENUM eVehicle = INT_TO_ENUM(IE_VEHICLE_ENUM, iVehicleID)
//	MODEL_NAMES eVehicleModel = GET_MODEL_NAME_FROM_IE_VEHICLE(eVehicle)
//	
//	SWITCH eVehicleModel
//		CASE CHEETAH
//		CASE ENTITYXF
//		CASE FMJ
//		CASE JESTER
//		CASE MASSACRO
//		CASE OSIRIS
//		CASE PFISTER811
//		CASE PROTOTIPO
//		CASE REAPER
//		CASE SEVEN70
//		CASE SHEAVA
//		CASE T20
//		CASE TURISMOR
//		CASE TYRUS
//		CASE ZENTORNO
//			eCoverVehModel = IMP_PROP_COVERED_VEHICLE_01A
//		BREAK
//		
//		CASE BESTIAGTS
//		CASE FELTZER2
//		CASE OMNIS
//		CASE SULTANRS
//		CASE VERLIERER2
//			eCoverVehModel = IMP_PROP_COVERED_VEHICLE_02A
//		BREAK
//		
//		CASE NIGHTSHADE
//		CASE SABREGT2
//		CASE TAMPA
//			eCoverVehModel = IMP_PROP_COVERED_VEHICLE_03A
//		BREAK
//		
//		CASE BANSHEE2
//		CASE COQUETTE2
//		CASE COQUETTE3
//		CASE FELTZER3
//		CASE MAMBA
//		CASE TROPOS
//			eCoverVehModel = IMP_PROP_COVERED_VEHICLE_04A
//		BREAK
//		
//		CASE BTYPE3
//			eCoverVehModel = IMP_PROP_COVERED_VEHICLE_05A
//		BREAK
//		
//		CASE ALPHA
//		CASE ZTYPE
//			eCoverVehModel = IMP_PROP_COVERED_VEHICLE_06A
//		BREAK
//		
//		DEFAULT
//			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_IE_WAREHOUSE - GET_IE_COVER_VEHICLE_MODEL - Model not listed! Vehicle: ", eVehicle, ", Model: ", eVehicleModel)
//			
//			eCoverVehModel = IMP_PROP_COVERED_VEHICLE_01A
//		BREAK
//	ENDSWITCH
//	
//	RETURN eCoverVehModel
//ENDFUNC

FUNC BOOL CREATE_IE_VEHICLE(IE_VEHICLE_ENUM eVehicle, INT iVehicleSlot, BOOL bVehFadeIn = TRUE, BOOL bFreezeVehPos = TRUE)
	
	VECTOR vVehicleCoords 		= IE_GET_INTERIOR_GARAGE_VEHICLE_COORDS(serverBD.iVehicleSpawnOrder[iVehicleSlot])
	VECTOR vVehicleRotation 	= IE_GET_INTERIOR_GARAGE_VEHICLE_ROTATION(serverBD.iVehicleSpawnOrder[iVehicleSlot])
	
	IF IS_IE_VEHICLE_SLOT_AVALIABLE(iVehicleSlot)
		VEHICLE_INDEX viVehicleObj = IE_CREATE_VEHICLE(eVehicle, vVehicleCoords, DEFAULT, FALSE, FALSE)
		IF DOES_ENTITY_EXIST(viVehicleObj)
			SET_ENTITY_COORDS(viVehicleObj, IE_GET_INTERIOR_GARAGE_VEHICLE_COORDS(iVehicleSlot))
			SET_ENTITY_ROTATION(viVehicleObj, vVehicleRotation)		
			SET_VEHICLE_DOORS_LOCKED(viVehicleObj, VEHICLELOCK_LOCKED)
			FREEZE_ENTITY_POSITION(viVehicleObj, bFreezeVehPos)
			
			thisPERSONAL_MOD_GARAGE.vehData.oVehicleObjects[iVehicleSlot] = viVehicleObj
			
			IF bVehFadeIn
				SET_ENTITY_ALPHA(viVehicleObj, 0, FALSE)
			ELSE
				thisPERSONAL_MOD_GARAGE.vehData.iVehicleFadeComplete[iVehicleSlot] = 1
			ENDIF

			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_IE_WAREHOUSE] CREATE_IE_VEHICLE - Creating Vehicle: ", eVehicle, ", Slot: ", serverBD.iVehicleSpawnOrder[iVehicleSlot], " (index: ", iVehicleSlot,")")
			#ENDIF
			RETURN TRUE
		ENDIF
		
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_IE_COVER_VEHICLE(INT iCoverVehicleID, INT iVehicleSlot, BOOL bVehFadeIn = TRUE, BOOL bFreezeVehPos = TRUE)
	VECTOR vVehicleCoords 		= IE_GET_INTERIOR_GARAGE_VEHICLE_COORDS(serverBD.iVehicleSpawnOrder[iVehicleSlot])
	VECTOR vVehicleRotation 	= IE_GET_INTERIOR_GARAGE_VEHICLE_ROTATION(serverBD.iVehicleSpawnOrder[iVehicleSlot])
	MODEL_NAMES eCoverVehModel
	
	BOOL bUpdateVehPos = FALSE
	
	IF iCoverVehicleID <= (ENUM_TO_INT(IE_VEH_INVALID) + ciIE_COVER_VEHICLE_INDEX)
	OR iCoverVehicleID >= (ENUM_TO_INT(IE_VEHICLE_COUNT) + ciIE_COVER_VEHICLE_INDEX)
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_IE_WAREHOUSE - CREATE_IE_COVER_VEHICLE - Invalid iCoverVehicleID: ", iCoverVehicleID, " reverting to default covered vehicle model.")
		// Revert to a default covered vehicle model.
		eCoverVehModel = IMP_PROP_COVERED_VEHICLE_01A
	ELSE
		eCoverVehModel = GET_IE_COVER_VEHICLE_MODEL(iCoverVehicleID)
	ENDIF

	IF IS_IE_COVER_VEHICLE_SLOT_AVALIABLE(iVehicleSlot)
		IF IS_MODEL_VALID(eCoverVehModel)
			REQUEST_MODEL(eCoverVehModel)
			IF HAS_MODEL_LOADED(eCoverVehModel)
				OBJECT_INDEX oCoverVeh = CREATE_OBJECT(eCoverVehModel, vVehicleCoords, FALSE, FALSE)
				IF DOES_ENTITY_EXIST(oCoverVeh)
					SET_ENTITY_ROTATION(oCoverVeh, vVehicleRotation)
					
					IF IS_VEHICLE_PLACED_ON_CARRACK(iVehicleSlot)
						// Check if vehicle entity is touching car rack entity.
						IF IS_ENTITY_TOUCHING_MODEL(oCoverVeh, INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_impexp_carrack")))
							FREEZE_ENTITY_POSITION(oCoverVeh, bFreezeVehPos)
						ELSE
							bUpdateVehPos = TRUE
						ENDIF
					ELSE
						// Check if vehicle is touching the ground.
						IF IS_ENTITY_TOUCHING_MODEL(oCoverVeh, INT_TO_ENUM(MODEL_NAMES, HASH("imp_carewareware_floor_basic")))
							FREEZE_ENTITY_POSITION(oCoverVeh, bFreezeVehPos)
						ELSE
							bUpdateVehPos = TRUE
						ENDIF
					ENDIF
					
					IF bUpdateVehPos
						SET_ENTITY_COORDS(oCoverVeh, IE_GET_INTERIOR_GARAGE_VEHICLE_COORDS(iVehicleSlot))
						SET_ENTITY_ROTATION(oCoverVeh, IE_GET_INTERIOR_GARAGE_VEHICLE_ROTATION(iVehicleSlot))
						
						bUpdateVehPos = FALSE
					ENDIF
					
					thisPERSONAL_MOD_GARAGE.vehData.oCoverVehicles[iVehicleSlot] = oCoverVeh
					
					INT iRandTint = GET_RANDOM_INT_IN_RANGE(0,4)
					SET_OBJECT_TINT_INDEX(oCoverVeh, iRandTint)
					
					IF bVehFadeIn
						SET_ENTITY_ALPHA(oCoverVeh, 0, FALSE)
					ELSE
						SET_MODEL_AS_NO_LONGER_NEEDED(eCoverVehModel)
						thisPERSONAL_MOD_GARAGE.vehData.iCoverVehicleFadeComplete[iVehicleSlot] = 1
					ENDIF
					
					#IF IS_DEBUG_BUILD
						CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_IE_WAREHOUSE - CREATE_IE_COVER_VEHICLE - Creating Cover Vehicle in Slot: ", serverBD.iVehicleSpawnOrder[iVehicleSlot], " (index: ", iVehicleSlot,")")
					#ENDIF
					RETURN TRUE
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_IE_WAREHOUSE - CREATE_IE_COVER_VEHICLE - Cover Vehicle Model ID: ", iCoverVehicleID, " in Slot: ", serverBD.iVehicleSpawnOrder[iVehicleSlot], " (index: ", iVehicleSlot,") is loading model with hash ", eCoverVehModel)
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_IE_WAREHOUSE - CREATE_IE_COVER_VEHICLE - Cover Vehicle Model ID: ", iCoverVehicleID, " in Slot: ", serverBD.iVehicleSpawnOrder[iVehicleSlot], " (index: ", iVehicleSlot,") is invalid! eCoverVehModel = ",eCoverVehModel)
			#ENDIF	
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_IE_WAREHOUSE - CREATE_IE_COVER_VEHICLE - Cover Vehicle Model ID: ", iCoverVehicleID, " in Slot: ", serverBD.iVehicleSpawnOrder[iVehicleSlot], " (index: ", iVehicleSlot,") is not available!")
		#ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CLEANUP_IE_VEHICLE(INT iVehicleSlot)
	
	VEHICLE_INDEX viVehicleObj = thisPERSONAL_MOD_GARAGE.vehData.oVehicleObjects[iVehicleSlot]
	IF DOES_ENTITY_EXIST(viVehicleObj)
	AND NETWORK_HAS_CONTROL_OF_ENTITY(viVehicleObj)
		DELETE_VEHICLE(viVehicleObj)
		thisPERSONAL_MOD_GARAGE.vehData.oVehicleObjects[iVehicleSlot] = NULL
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CLEANUP_IE_COVER_VEHICLE(INT iVehicleSlot)
	
	OBJECT_INDEX oVehicleObj = thisPERSONAL_MOD_GARAGE.vehData.oCoverVehicles[iVehicleSlot]
	IF DOES_ENTITY_EXIST(oVehicleObj)
	AND NETWORK_HAS_CONTROL_OF_ENTITY(oVehicleObj)
		DELETE_OBJECT(oVehicleObj)
		thisPERSONAL_MOD_GARAGE.vehData.oCoverVehicles[iVehicleSlot] = NULL
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEANUP_ALL_IE_VEHICLES()
	
	INT iVehicleSlot
	REPEAT ciMAX_IE_OWNED_VEHICLES iVehicleSlot
		CLEANUP_IE_VEHICLE(iVehicleSlot)
	ENDREPEAT
	
	REPEAT ciMAX_IE_OWNED_VEHICLES iVehicleSlot
		CLEANUP_IE_COVER_VEHICLE(iVehicleSlot)
	ENDREPEAT
	
ENDPROC

PROC SET_IE_VEHICLE_FADE_TIMER(INT iVehicleSlot)
	IF thisPERSONAL_MOD_GARAGE.vehData.iSetVehicleFadeTimer[iVehicleSlot] 	= 0
		thisPERSONAL_MOD_GARAGE.vehData.tdVehicleFadeTimer[iVehicleSlot] 	= GET_TIME_OFFSET(GET_NETWORK_TIME(), ciIE_VEHICLE_FADE_TIME)
		thisPERSONAL_MOD_GARAGE.vehData.iSetVehicleFadeTimer[iVehicleSlot] 	= 1
	ENDIF
ENDPROC

PROC PERFORM_IE_VEHICLE_FADE(INT iVehicleSlot, BOOL bFadeIn, BOOL bDeleteAfterFade = TRUE)
	
	SET_IE_VEHICLE_FADE_TIMER(iVehicleSlot)
	
	IF IS_TIME_LESS_THAN(GET_NETWORK_TIME(), thisPERSONAL_MOD_GARAGE.vehData.tdVehicleFadeTimer[iVehicleSlot])
		VEHICLE_INDEX viVehicleObj = thisPERSONAL_MOD_GARAGE.vehData.oVehicleObjects[iVehicleSlot]
		IF DOES_ENTITY_EXIST(viVehicleObj)
			
			INT iTimeDifference = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), thisPERSONAL_MOD_GARAGE.vehData.tdVehicleFadeTimer[iVehicleSlot]))
			INT iAlpha = ROUND((TO_FLOAT(iTimeDifference) / ciIE_VEHICLE_FADE_TIME) * 255)
			
			IF bFadeIn
				iAlpha = (255 - (ROUND((TO_FLOAT(iTimeDifference) / ciIE_VEHICLE_FADE_TIME) * 255)))
			ENDIF
			
			SET_ENTITY_ALPHA(viVehicleObj, iAlpha, FALSE)
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_IE_WAREHOUSE - PERFORM_IE_VEHICLE_FADE - Vehicle: ", iVehicleSlot, " Alpha: ", iAlpha, " Time Difference: ", iTimeDifference)
			#ENDIF
		ENDIF
	ELSE
		VEHICLE_INDEX viVehicleObj = thisPERSONAL_MOD_GARAGE.vehData.oVehicleObjects[iVehicleSlot]
		IF DOES_ENTITY_EXIST(viVehicleObj)
			IF bFadeIn
				SET_ENTITY_ALPHA(viVehicleObj, 255, FALSE)
				IF IS_MODEL_VALID(GET_ENTITY_MODEL(viVehicleObj))
					SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(viVehicleObj))
				ENDIF
				
				thisPERSONAL_MOD_GARAGE.vehData.iVehicleFadeComplete[iVehicleSlot] = 1
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_IE_WAREHOUSE - PERFORM_IE_VEHICLE_FADE - Vehicle: ", iVehicleSlot, " Has Fully Faded In")
				#ENDIF
			ELSE
				SET_ENTITY_ALPHA(viVehicleObj, 0, FALSE)
				
				IF bDeleteAfterFade
					IF NETWORK_HAS_CONTROL_OF_ENTITY(viVehicleObj)
						CLEANUP_IE_VEHICLE(iVehicleSlot)
					ENDIF
				ENDIF
				
				thisPERSONAL_MOD_GARAGE.vehData.iVehicleFadeComplete[iVehicleSlot] = 0
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_IE_WAREHOUSE - PERFORM_IE_VEHICLE_FADE - Vehicle: ", iVehicleSlot, " Has Fully Faded Out & Been Deleted")
				#ENDIF
			ENDIF
		ELSE
			IF bFadeIn
				thisPERSONAL_MOD_GARAGE.vehData.iVehicleFadeComplete[iVehicleSlot] = 1
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_IE_WAREHOUSE - PERFORM_IE_VEHICLE_FADE - Vehicle: ", iVehicleSlot, " Does Not Exist For Fade In")
				#ENDIF
			ELSE
				thisPERSONAL_MOD_GARAGE.vehData.iVehicleFadeComplete[iVehicleSlot] = 0
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_IE_WAREHOUSE - PERFORM_IE_VEHICLE_FADE - Vehicle: ", iVehicleSlot, " Does Not Exist For Fade Out")
				#ENDIF
			ENDIF
		ENDIF
		thisPERSONAL_MOD_GARAGE.vehData.iSetVehicleFadeTimer[iVehicleSlot] = 0
	ENDIF
	
ENDPROC

PROC SET_IE_COVER_VEHICLE_FADE_TIMER(INT iVehicleSlot)
	IF thisPERSONAL_MOD_GARAGE.vehData.iSetCoverVehicleFadeTimer[iVehicleSlot] 	= 0
		thisPERSONAL_MOD_GARAGE.vehData.tdCoverVehicleFadeTimer[iVehicleSlot] 	= GET_TIME_OFFSET(GET_NETWORK_TIME(), ciIE_VEHICLE_FADE_TIME)
		thisPERSONAL_MOD_GARAGE.vehData.iSetCoverVehicleFadeTimer[iVehicleSlot] = 1
	ENDIF
ENDPROC

PROC PERFORM_IE_COVER_VEHICLE_FADE(INT iVehicleSlot, BOOL bFadeIn)
	
	SET_IE_COVER_VEHICLE_FADE_TIMER(iVehicleSlot)
	
	IF IS_TIME_LESS_THAN(GET_NETWORK_TIME(), thisPERSONAL_MOD_GARAGE.vehData.tdCoverVehicleFadeTimer[iVehicleSlot])
		OBJECT_INDEX oVehicleObj = thisPERSONAL_MOD_GARAGE.vehData.oCoverVehicles[iVehicleSlot]
		IF DOES_ENTITY_EXIST(oVehicleObj)
			
			INT iTimeDifference = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), thisPERSONAL_MOD_GARAGE.vehData.tdCoverVehicleFadeTimer[iVehicleSlot]))
			INT iAlpha = ROUND((TO_FLOAT(iTimeDifference) / ciIE_VEHICLE_FADE_TIME) * 255)
			
			IF bFadeIn
				iAlpha = (255 - (ROUND((TO_FLOAT(iTimeDifference) / ciIE_VEHICLE_FADE_TIME) * 255)))
			ENDIF
			
			SET_ENTITY_ALPHA(oVehicleObj, iAlpha, FALSE)
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_IE_WAREHOUSE - PERFORM_IE_COVER_VEHICLE_FADE - Vehicle: ", iVehicleSlot, " Alpha: ", iAlpha, " Time Difference: ", iTimeDifference)
			#ENDIF
		ENDIF
	ELSE
		OBJECT_INDEX oVehicleObj = thisPERSONAL_MOD_GARAGE.vehData.oCoverVehicles[iVehicleSlot]
		IF DOES_ENTITY_EXIST(oVehicleObj)
			IF bFadeIn
				SET_ENTITY_ALPHA(oVehicleObj, 255, FALSE)
				IF IS_MODEL_VALID(GET_ENTITY_MODEL(oVehicleObj))
					SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(oVehicleObj))
				ENDIF
				
				thisPERSONAL_MOD_GARAGE.vehData.iCoverVehicleFadeComplete[iVehicleSlot] = 1
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_IE_WAREHOUSE - PERFORM_IE_COVER_VEHICLE_FADE - Vehicle: ", iVehicleSlot, " Has Fully Faded In")
				#ENDIF
			ELSE
				SET_ENTITY_ALPHA(oVehicleObj, 0, FALSE)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(oVehicleObj)
					CLEANUP_IE_VEHICLE(iVehicleSlot)
				ENDIF
				
				thisPERSONAL_MOD_GARAGE.vehData.iCoverVehicleFadeComplete[iVehicleSlot] = 0
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_IE_WAREHOUSE - PERFORM_IE_COVER_VEHICLE_FADE - Vehicle: ", iVehicleSlot, " Has Fully Faded Out & Been Deleted")
				#ENDIF
			ENDIF
		ELSE
			IF bFadeIn
				thisPERSONAL_MOD_GARAGE.vehData.iCoverVehicleFadeComplete[iVehicleSlot] = 1
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_IE_WAREHOUSE - PERFORM_IE_COVER_VEHICLE_FADE - Vehicle: ", iVehicleSlot, " Does Not Exist For Fade In")
				#ENDIF
			ELSE
				thisPERSONAL_MOD_GARAGE.vehData.iCoverVehicleFadeComplete[iVehicleSlot] = 0
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_IE_WAREHOUSE - PERFORM_IE_COVER_VEHICLE_FADE - Vehicle: ", iVehicleSlot, " Does Not Exist For Fade Out")
				#ENDIF
			ENDIF
		ENDIF
		thisPERSONAL_MOD_GARAGE.vehData.iSetCoverVehicleFadeTimer[iVehicleSlot] = 0
	ENDIF
	
ENDPROC

FUNC BOOL IS_ANY_PLAYER_BLOCKING_IE_VEHICLE_SPAWN_COORDS(INT iVehicleSlot)

	VECTOR vVehicleCoords = IE_GET_INTERIOR_GARAGE_VEHICLE_COORDS(serverBD.iVehicleSpawnOrder[iVehicleSlot])
	
	INT iParticipant
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
		    PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
		    IF IS_NET_PLAYER_OK(playerID)
				IF ARE_VECTORS_ALMOST_EQUAL(GET_PLAYER_COORDS(playerID), vVehicleCoords, 2.5)
					RETURN TRUE
				ENDIF
			ENDIF
	    ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SPAWN_IE_VEHICLE(IE_VEHICLE_ENUM eVehicle, INT iVehicleSlot, BOOL bVehSlotEmpty, BOOL bCoverSlotEmpty)
	
	SWITCH thisPERSONAL_MOD_GARAGE.iVehicleSpawnFlow
		CASE 0
			IF bVehSlotEmpty
			AND bCoverSlotEmpty
				PRINTLN("SPAWN_IE_VEHICLE - No cover vehicles or display vehicles in Slot: ", iVehicleSlot, ", move to stage 1")
				thisPERSONAL_MOD_GARAGE.iVehicleSpawnFlow = 1
			ELSE
				IF NOT bVehSlotEmpty
					PERFORM_IE_VEHICLE_FADE(iVehicleSlot, FALSE)
					IF thisPERSONAL_MOD_GARAGE.vehData.iVehicleFadeComplete[iVehicleSlot] = 0
						PRINTLN("SPAWN_IE_VEHICLE - Slot: ", iVehicleSlot, " existing Display Vehicle has faded out, move to stage 1")
						thisPERSONAL_MOD_GARAGE.iVehicleSpawnFlow = 1
					ENDIF
				ELIF NOT bCoverSlotEmpty
					PERFORM_IE_COVER_VEHICLE_FADE(iVehicleSlot, FALSE)
					IF thisPERSONAL_MOD_GARAGE.vehData.iCoverVehicleFadeComplete[iVehicleSlot] = 0
						PRINTLN("SPAWN_IE_VEHICLE - Slot: ", iVehicleSlot, " existing Cover Vehicle has faded out, move to stage 1")
						thisPERSONAL_MOD_GARAGE.iVehicleSpawnFlow = 1
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 1
			IF NOT IS_ANY_PLAYER_BLOCKING_IE_VEHICLE_SPAWN_COORDS(iVehicleSlot)
				IF CREATE_IE_VEHICLE(eVehicle, iVehicleSlot, thisPERSONAL_MOD_GARAGE.bVehFadeRequired)
					thisPERSONAL_MOD_GARAGE.iVehicleSpawnFlow = 2
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF thisPERSONAL_MOD_GARAGE.bVehFadeRequired
				PERFORM_IE_VEHICLE_FADE(iVehicleSlot, TRUE)
				IF thisPERSONAL_MOD_GARAGE.vehData.iVehicleFadeComplete[iVehicleSlot] = 1
					thisPERSONAL_MOD_GARAGE.iVehicleSpawnFlow = 0
					RETURN TRUE
				ENDIF
			ELSE
				thisPERSONAL_MOD_GARAGE.iVehicleSpawnFlow = 0
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SPAWN_IE_COVER_VEHICLE(INT iCoverVehicleID, INT iVehicleSlot, BOOL bVehSlotEmpty, BOOL bCoverSlotEmpty)
	
	SWITCH thisPERSONAL_MOD_GARAGE.iVehicleSpawnFlow
		CASE 0
			IF bCoverSlotEmpty
			AND bVehSlotEmpty
				PRINTLN("SPAWN_IE_COVER_VEHICLE - No cover vehicles or display vehicles in Slot: ", iVehicleSlot, ", move to stage 1")
				thisPERSONAL_MOD_GARAGE.iVehicleSpawnFlow = 1
			ELSE
				IF NOT bCoverSlotEmpty
					PERFORM_IE_COVER_VEHICLE_FADE(iVehicleSlot, FALSE)
					IF thisPERSONAL_MOD_GARAGE.vehData.iCoverVehicleFadeComplete[iVehicleSlot] = 0
						PRINTLN("SPAWN_IE_COVER_VEHICLE - Slot: ", iVehicleSlot, " existing Cover Vehicle has faded out, move to stage 1")
						thisPERSONAL_MOD_GARAGE.iVehicleSpawnFlow = 1
					ENDIF
				ELIF NOT bVehSlotEmpty
					PERFORM_IE_VEHICLE_FADE(iVehicleSlot, FALSE)
					IF thisPERSONAL_MOD_GARAGE.vehData.iVehicleFadeComplete[iVehicleSlot] = 0
						PRINTLN("SPAWN_IE_COVER_VEHICLE - Slot: ", iVehicleSlot, " existing Display Vehicle has faded out, move to stage 1")
						thisPERSONAL_MOD_GARAGE.iVehicleSpawnFlow = 1
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 1
			IF NOT IS_ANY_PLAYER_BLOCKING_IE_VEHICLE_SPAWN_COORDS(iVehicleSlot)
				IF CREATE_IE_COVER_VEHICLE(iCoverVehicleID, iVehicleSlot, thisPERSONAL_MOD_GARAGE.bVehFadeRequired)
					thisPERSONAL_MOD_GARAGE.iVehicleSpawnFlow = 2
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF thisPERSONAL_MOD_GARAGE.bVehFadeRequired
				PERFORM_IE_COVER_VEHICLE_FADE(iVehicleSlot, TRUE)
				IF thisPERSONAL_MOD_GARAGE.vehData.iCoverVehicleFadeComplete[iVehicleSlot] = 1
					thisPERSONAL_MOD_GARAGE.iVehicleSpawnFlow = 0
					RETURN TRUE
				ENDIF
			ELSE
				thisPERSONAL_MOD_GARAGE.iVehicleSpawnFlow = 0
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
PROC DEBUG_PRINT_CURRENT_IE_WAREHOUSE_VEHICLES()
	
	PRINTLN("AM_MP_IE_WAREHOUSE - PRINT_CURRENT_IE_VEHICLES - Vehicle Total: ", serverBD.iVehicleTotal)
	
	INT iVehicleSlot
	REPEAT ciMAX_IE_OWNED_VEHICLES iVehicleSlot
		PRINTLN("AM_MP_IE_WAREHOUSE - PRINT_CURRENT_IE_VEHICLES - OWNED VEHICLES - Slot: ", iVehicleSlot, " Vehicle ID: ", serverBD.iVehicles[iVehicleSlot])
	ENDREPEAT
	
ENDPROC
#ENDIF

PROC HOST_INITIALISE_IE_WAREHOUSE_VEHICLES()
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
		INT iVehicleSlot
		REPEAT ciMAX_IE_OWNED_VEHICLES iVehicleSlot
			serverBD.iVehicles[iVehicleSlot] = GlobalplayerBD_FM[NATIVE_TO_INT(thisPERSONAL_MOD_GARAGE.pOwner)].propertyDetails.bdIEgarageData.iWarehouseVehicles[iVehicleSlot]
			serverBD.iVehicleSpawnOrder[iVehicleSlot] = iVehicleSlot  //Temp until events set up
		ENDREPEAT
		
		IF GlobalplayerBD_FM[NATIVE_TO_INT(thisPERSONAL_MOD_GARAGE.pOwner)].propertyDetails.bdIEgarageData.iVehicleTotal <= ciMAX_IE_OWNED_VEHICLES
			serverBD.iVehicleTotal = GlobalplayerBD_FM[NATIVE_TO_INT(thisPERSONAL_MOD_GARAGE.pOwner)].propertyDetails.bdIEgarageData.iVehicleTotal
		ELSE
			serverBD.iVehicleTotal = ciMAX_IE_OWNED_VEHICLES
		ENDIF
		
	ENDIF
	
	#IF IS_DEBUG_BUILD
	DEBUG_PRINT_CURRENT_IE_WAREHOUSE_VEHICLES()
	#ENDIF
	
ENDPROC

FUNC BOOL UPDATE_LOCAL_IE_WAREHOUSE_VEHICLES()
	
	// Tell remote players to populate local data
	IF thisPERSONAL_MOD_GARAGE.iVehicleTotal != serverBD.iVehicleTotal
		thisPERSONAL_MOD_GARAGE.bPopulateVehicleData 	= TRUE
		thisPERSONAL_MOD_GARAGE.iCurrentVehicleSlot 	= 0
		thisPERSONAL_MOD_GARAGE.bVehFadeRequired 		= serverBD.bVehFadeRequired
		thisPERSONAL_MOD_GARAGE.iVehicleTotal			= serverBD.iVehicleTotal
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			serverBD.bUpdateVehData = FALSE
		ENDIF
	ENDIF
	
	RETURN thisPERSONAL_MOD_GARAGE.bPopulateVehicleData
ENDFUNC

FUNC BOOL DOES_HOST_IE_WAREHOUSE_VEHICLE_DATA_NEED_UPDATING()
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF serverBD.iVehicleTotal != GlobalplayerBD_FM[NATIVE_TO_INT(thisPERSONAL_MOD_GARAGE.pOwner)].propertyDetails.bdIEgarageData.iVehicleTotal
			serverBD.bUpdateVehData = TRUE
		ENDIF
	ENDIF
	
	RETURN serverBD.bUpdateVehData
ENDFUNC

PROC ITERATE_IE_VEHICLE_SLOT()
	
	IF thisPERSONAL_MOD_GARAGE.iCurrentVehicleSlot < ciMAX_IE_OWNED_VEHICLES
		thisPERSONAL_MOD_GARAGE.iVehicles[thisPERSONAL_MOD_GARAGE.iCurrentVehicleSlot] = serverBD.iVehicles[thisPERSONAL_MOD_GARAGE.iCurrentVehicleSlot]
	ENDIF
	
	IF thisPERSONAL_MOD_GARAGE.iCurrentVehicleSlot >= (ciMAX_IE_OWNED_VEHICLES-1)
		PRINTLN("LOCAL_INITIALISE_IE_WAREHOUSE_VEHICLES - Local data updated")
				
		thisPERSONAL_MOD_GARAGE.bVehFadeRequired 		= FALSE
		thisPERSONAL_MOD_GARAGE.bPopulateVehicleData 	= FALSE
		thisPERSONAL_MOD_GARAGE.iCurrentVehicleSlot 	= 0
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			serverBD.bVehFadeRequired = FALSE
		ENDIF
	ELSE
		g_iTotalIEVehCreated++
		thisPERSONAL_MOD_GARAGE.iCurrentVehicleSlot++
	ENDIF
	
ENDPROC

PROC UPDATE_IE_MISSION_WAREHOUSE_MODDING_VEHICLE()
	IF thisPERSONAL_MOD_GARAGE.pOwner != INVALID_PLAYER_INDEX()
	AND PLAYER_ID() != thisPERSONAL_MOD_GARAGE.pOwner 
		IF IS_MISSION_PERSONAL_CAR_MOD_STARTED(thisPERSONAL_MOD_GARAGE.pOwner)
			IF NOT IS_BIT_SET(thisPERSONAL_MOD_GARAGE.iLocalBS , IE_WAREHOUSE_BITSET_HIDE_OWNERS_IE_MISSION_VEH)
				VEHICLE_SETUP_STRUCT_MP vehicleSetupStruc
				VEHICLE_SETUP_STRUCT_MP vehicleSetupStrucTwo
				
				IE_GET_VEHICLE_DATA(INT_TO_ENUM(IE_VEHICLE_ENUM ,GlobalplayerBD_FM[NATIVE_TO_INT(thisPERSONAL_MOD_GARAGE.pOwner)].propertyDetails.iIEMissionVehicleModding), vehicleSetupStruc)
				
				INT i 
				FOR i = 0 TO ciMAX_IE_OWNED_VEHICLES - 1
					
					IF DOES_ENTITY_EXIST(thisPERSONAL_MOD_GARAGE.vehData.oVehicleObjects[i])
						GET_VEHICLE_SETUP_MP(thisPERSONAL_MOD_GARAGE.vehData.oVehicleObjects[i], vehicleSetupStrucTwo)
						IF vehicleSetupStrucTwo.VehicleSetup.eModel = vehicleSetupStruc.VehicleSetup.eModel
						AND vehicleSetupStrucTwo.VehicleSetup.iPlateIndex = vehicleSetupStruc.VehicleSetup.iPlateIndex
						AND vehicleSetupStrucTwo.VehicleSetup.iColour1 = vehicleSetupStruc.VehicleSetup.iColour1
						AND vehicleSetupStrucTwo.VehicleSetup.iColour2 = vehicleSetupStruc.VehicleSetup.iColour2
						AND vehicleSetupStrucTwo.VehicleSetup.iColourExtra1 = vehicleSetupStruc.VehicleSetup.iColourExtra1
						AND vehicleSetupStrucTwo.VehicleSetup.iColourExtra2 = vehicleSetupStruc.VehicleSetup.iColourExtra2
							PERFORM_IE_VEHICLE_FADE(i,FALSE , FALSE)
							thisPERSONAL_MOD_GARAGE.vehData.iVehNeedToHideForMission = i
							IF thisPERSONAL_MOD_GARAGE.vehData.iVehicleFadeComplete[i] = 0
								SET_BIT(thisPERSONAL_MOD_GARAGE.iLocalBS , IE_WAREHOUSE_BITSET_HIDE_OWNERS_IE_MISSION_VEH)
								PRINTLN("UPDATE_IE_MISSION_WAREHOUSE_MODDING_VEHICLE - Faded out slot : ", thisPERSONAL_MOD_GARAGE.vehData.iVehNeedToHideForMission)
							ENDIF
						ENDIF
					ENDIF
				ENDFOR 
			ENDIF	
		ELSE
			IF IS_BIT_SET(thisPERSONAL_MOD_GARAGE.iLocalBS , IE_WAREHOUSE_BITSET_HIDE_OWNERS_IE_MISSION_VEH)
				IF DOES_ENTITY_EXIST(thisPERSONAL_MOD_GARAGE.vehData.oVehicleObjects[thisPERSONAL_MOD_GARAGE.vehData.iVehNeedToHideForMission])
					PERFORM_IE_VEHICLE_FADE(thisPERSONAL_MOD_GARAGE.vehData.iVehNeedToHideForMission,TRUE)
					IF thisPERSONAL_MOD_GARAGE.vehData.iVehicleFadeComplete[thisPERSONAL_MOD_GARAGE.vehData.iVehNeedToHideForMission] = 1
						CLEAR_BIT(thisPERSONAL_MOD_GARAGE.iLocalBS , IE_WAREHOUSE_BITSET_HIDE_OWNERS_IE_MISSION_VEH)
						PRINTLN("UPDATE_IE_MISSION_WAREHOUSE_MODDING_VEHICLE - Faded in slot : ", thisPERSONAL_MOD_GARAGE.vehData.iVehNeedToHideForMission)
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDIF	
ENDPROC

PROC UPDATE_IE_WAREHOUSE_VEHICLE_POSITIONS()

	IF thisPERSONAL_MOD_GARAGE.iCurrentVehicleSlot >= (ciMAX_IE_OWNED_VEHICLES - 1)
		thisPERSONAL_MOD_GARAGE.iCurrentVehicleSlot = 0
	ENDIF

	VEHICLE_INDEX vehIndex = thisPERSONAL_MOD_GARAGE.vehData.oVehicleObjects[thisPERSONAL_MOD_GARAGE.iCurrentVehicleSlot]

	IF DOES_ENTITY_EXIST(vehIndex)
	AND IS_ENTITY_ALIVE(vehIndex)
	
		VECTOR vDesiredCoords 	= IE_GET_INTERIOR_GARAGE_VEHICLE_COORDS(serverBD.iVehicleSpawnOrder[thisPERSONAL_MOD_GARAGE.iCurrentVehicleSlot])
		VECTOR vCurrentCoords 	= GET_ENTITY_COORDS(vehIndex)
	
		IF (vCurrentCoords.Z != vDesiredCoords.Z)
			SET_ENTITY_COORDS(vehIndex, vDesiredCoords)
			
			// Fix for B*3292541
			FREEZE_ENTITY_POSITION(vehIndex, TRUE)
			
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[AM_MP_IE_WAREHOUSE] UPDATE_IE_WAREHOUSE_VEHICLE_POSITIONS - Updating vehicle coords.")
			#ENDIF
		ENDIF
	ENDIF

	IF thisPERSONAL_MOD_GARAGE.iCurrentVehicleSlot < (ciMAX_IE_OWNED_VEHICLES - 1)
		thisPERSONAL_MOD_GARAGE.iCurrentVehicleSlot++
		
		// Fix for B*3292541
		IF thisPERSONAL_MOD_GARAGE.iCurrentVehicleSlot >= (ciMAX_IE_OWNED_VEHICLES - 1)
		AND NOT IS_BIT_SET(thisPERSONAL_MOD_GARAGE.iBS, BS_IE_WAREHOUSE_VEH_POSITIONS_UPDATED)
			SET_BIT(thisPERSONAL_MOD_GARAGE.iBS, BS_IE_WAREHOUSE_VEH_POSITIONS_UPDATED)
		ENDIF
	ENDIF

ENDPROC

PROC LOCAL_INITIALISE_IE_WAREHOUSE_VEHICLES()
	INT iVehicleTotal
	PLAYER_INDEX bossID
	
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
		bossID = GB_GET_LOCAL_PLAYER_GANG_BOSS()
	ELIF DOES_PLAYER_OWN_AN_IE_GARAGE(PLAYER_ID())
		bossID = GET_PLAYER_INDEX()
	ENDIF
	
	iVehicleTotal = GlobalplayerBD_FM[NATIVE_TO_INT(bossID)].propertyDetails.bdIEgarageData.iVehicleTotal
	
	IF thisPERSONAL_MOD_GARAGE.iVehicles[thisPERSONAL_MOD_GARAGE.iCurrentVehicleSlot] != serverBD.iVehicles[thisPERSONAL_MOD_GARAGE.iCurrentVehicleSlot]
		// Vehicles
		IF serverBD.iVehicles[thisPERSONAL_MOD_GARAGE.iCurrentVehicleSlot]  > ENUM_TO_INT(IE_VEH_INVALID)
		AND serverBD.iVehicles[thisPERSONAL_MOD_GARAGE.iCurrentVehicleSlot] < ENUM_TO_INT(IE_VEHICLE_COUNT)
			PRINTLN("LOCAL_INITIALISE_IE_WAREHOUSE_VEHICLES - Slot: ", thisPERSONAL_MOD_GARAGE.iCurrentVehicleSlot, " is a Vehicle with ID: ", serverBD.iVehicles[thisPERSONAL_MOD_GARAGE.iCurrentVehicleSlot])
			
			BOOL bVehSlotEmpty
			BOOL bCoverVehSlotEmpty
			IE_VEHICLE_ENUM eVehicle
			
			eVehicle 	  		= INT_TO_ENUM(IE_VEHICLE_ENUM, serverBD.iVehicles[thisPERSONAL_MOD_GARAGE.iCurrentVehicleSlot])
			bVehSlotEmpty 		= IS_IE_VEHICLE_SLOT_AVALIABLE(thisPERSONAL_MOD_GARAGE.iCurrentVehicleSlot)
			bCoverVehSlotEmpty 	= IS_IE_COVER_VEHICLE_SLOT_AVALIABLE(thisPERSONAL_MOD_GARAGE.iCurrentVehicleSlot)
			
			IF SPAWN_IE_VEHICLE(eVehicle, thisPERSONAL_MOD_GARAGE.iCurrentVehicleSlot, bVehSlotEmpty, bCoverVehSlotEmpty)
				ITERATE_IE_VEHICLE_SLOT()
			ENDIF
			
		// Cover Vehicles
		ELIF serverBD.iVehicles[thisPERSONAL_MOD_GARAGE.iCurrentVehicleSlot] >= (ENUM_TO_INT(IE_VEH_INVALID) + ciIE_COVER_VEHICLE_INDEX)
		AND serverBD.iVehicles[thisPERSONAL_MOD_GARAGE.iCurrentVehicleSlot]  <= (ENUM_TO_INT(IE_VEHICLE_COUNT) + ciIE_COVER_VEHICLE_INDEX)
			PRINTLN("LOCAL_INITIALISE_IE_WAREHOUSE_VEHICLES - Slot: ", thisPERSONAL_MOD_GARAGE.iCurrentVehicleSlot, " is a Cover Vehicle")
			
			BOOL bVehSlotEmpty
			BOOL bCoverVehSlotEmpty
			
			bVehSlotEmpty 		= IS_IE_VEHICLE_SLOT_AVALIABLE(thisPERSONAL_MOD_GARAGE.iCurrentVehicleSlot)
			bCoverVehSlotEmpty 	= IS_IE_COVER_VEHICLE_SLOT_AVALIABLE(thisPERSONAL_MOD_GARAGE.iCurrentVehicleSlot)
			
			IF SPAWN_IE_COVER_VEHICLE(serverBD.iVehicles[thisPERSONAL_MOD_GARAGE.iCurrentVehicleSlot], thisPERSONAL_MOD_GARAGE.iCurrentVehicleSlot, bVehSlotEmpty, bCoverVehSlotEmpty)
				ITERATE_IE_VEHICLE_SLOT()
			ENDIF
			
		ELSE
			IF serverBD.iVehicles[thisPERSONAL_MOD_GARAGE.iCurrentVehicleSlot]  = ENUM_TO_INT(IE_VEH_INVALID)
				CLEANUP_IE_VEHICLE(thisPERSONAL_MOD_GARAGE.iCurrentVehicleSlot)
				CLEANUP_IE_COVER_VEHICLE(thisPERSONAL_MOD_GARAGE.iCurrentVehicleSlot)
			ENDIF
			ITERATE_IE_VEHICLE_SLOT()
		ENDIF
	ELSE
		ITERATE_IE_VEHICLE_SLOT()
	ENDIF
	
	IF g_iTotalIEVehCreated = iVehicleTotal
	OR iVehicleTotal = 0
		g_bFinishedCreatingIEVehicles = TRUE
	ENDIF
	
ENDPROC

PROC UPDATE_HOST_IE_WAREHOUSE_VEHICLE_DATA()
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		serverBD.bVehFadeRequired = TRUE
		HOST_INITIALISE_IE_WAREHOUSE_VEHICLES()
	ENDIF
	
ENDPROC

FUNC BOOL CAN_IE_WAREHOUSE_VEHICLE_POSITIONS_BE_UPDATED()
	RETURN NOT IS_BIT_SET(thisPERSONAL_MOD_GARAGE.iBS, BS_IE_WAREHOUSE_VEH_POSITIONS_UPDATED)
ENDFUNC

PROC MAINTAIN_IE_WAREHOUSE_VEHICLES()
	
	IF DOES_HOST_IE_WAREHOUSE_VEHICLE_DATA_NEED_UPDATING()
		UPDATE_HOST_IE_WAREHOUSE_VEHICLE_DATA()
	ENDIF
	
	IF UPDATE_LOCAL_IE_WAREHOUSE_VEHICLES()
		LOCAL_INITIALISE_IE_WAREHOUSE_VEHICLES()
	ENDIF

	IF CAN_IE_WAREHOUSE_VEHICLE_POSITIONS_BE_UPDATED()
		UPDATE_IE_WAREHOUSE_VEHICLE_POSITIONS()
	ENDIF
	
	UPDATE_IE_MISSION_WAREHOUSE_MODDING_VEHICLE()
ENDPROC

//╒═══════════════════════════════════════════════════════════════════════╕
//╞════════════════════════╡  PERSONAL VEHICLES  ╞════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════╛

FUNC BOOL IS_IT_SAFE_TO_CREATE_AN_SVM_VEHICLE()
	RETURN NOT IS_BIT_SET(serverBD.sGetOutOfTheWay.iCreationBitSet, SV_WLK_OUT_SERVER_RUN_PLAYER_CLEAN_UP_CHECKS)
ENDFUNC

PROC SERVER_CHECK_ANY_PLAYERS_ARE_IN_THE_WAY_OF_SVM_VEHICLES(VECTOR vCreationPos)
	PLAYER_INDEX playerID
	INT iLoop
	VECTOR vPlayerPos
	FLOAT fdistance
	FLOAT fDistanceToCheck = SV_SAFE_DISTANCE
	
	IF NOT bGetOutOfWayFlagClearedThisFrame
		serverBD.sGetOutOfTheWay.iCreationBitSet = 0
		serverBD.sGetOutOfTheWay.iPlayerBitSet = 0
		bGetOutOfWayFlagClearedThisFrame = TRUE
	ENDIF
	
	IF ARE_VECTORS_ALMOST_EQUAL(vServerWalkOutLocation, vCreationPos, 0.1)
		IF NOT HAS_NET_TIMER_STARTED(st_ServerWalkOutTimeout)
			START_NET_TIMER(st_ServerWalkOutTimeout, TRUE)
		ELSE
			IF HAS_NET_TIMER_EXPIRED(st_ServerWalkOutTimeout, 15000, TRUE)
				EXIT
			ENDIF
		ENDIF
	ELSE
		RESET_NET_TIMER(st_ServerWalkOutTimeout)
	ENDIF
	
	vServerWalkOutLocation = vCreationPos
	
	REPEAT NUM_NETWORK_PLAYERS iLoop
		playerID = INT_TO_PLAYERINDEX(iLoop)
		
		IF IS_NET_PLAYER_OK(playerID, FALSE)
			IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerID)
				IF NOT IS_BIT_SET(serverBD.sGetOutOfTheWay.iPlayerBitSet, iLoop)
					IF IS_NET_PLAYER_OK(playerID, TRUE, TRUE)
						IF NOT IS_THIS_PLAYER_ACTIVE_IN_CORONA(playerID)
						AND NOT IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(playerID))
							vPlayerPos = GET_ENTITY_COORDS(GET_PLAYER_PED(playerID))
							fdistance = GET_DISTANCE_BETWEEN_COORDS(vPlayerPos, vCreationPos, TRUE)
							
							IF fDistance < fDistanceToCheck
								SET_BIT(serverBD.sGetOutOfTheWay.iPlayerBitSet, iLoop)
								SET_BIT(serverBD.sGetOutOfTheWay.iCreationBitSet, SV_WLK_OUT_SERVER_RUN_PLAYER_CLEAN_UP_CHECKS)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC SERVER_CLEANUP_PLAYERS_ARE_IN_THE_WAY_SV_FLAGS()
	IF IS_BIT_SET(serverBD.sGetOutOfTheWay.iCreationBitSet, SV_WLK_OUT_SERVER_RUN_PLAYER_CLEAN_UP_CHECKS)
		BOOL bActive
		PLAYER_INDEX playerID
		
		bActive = FALSE
		playerID = INT_TO_PLAYERINDEX(iServerGetOutOfWayPartCheck)
		
		IF IS_NET_PLAYER_OK(playerID, FALSE)
			IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerID)
				bActive = TRUE
				
				IF IS_BIT_SET(serverBD.sGetOutOfTheWay.iPlayerBitSet, iServerGetOutOfWayPartCheck)
					IF IS_BIT_SET(playerBD[NATIVE_TO_INT(playerID)].sGetOutOfTheWay.iPlayerBitSet, SV_WLK_OUT_PLAYER_IM_OUT_OF_THE_WAY)
						CLEAR_BIT(serverBD.sGetOutOfTheWay.iPlayerBitSet, iServerGetOutOfWayPartCheck)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF bActive = FALSE
			IF IS_BIT_SET(serverBD.sGetOutOfTheWay.iPlayerBitSet, iServerGetOutOfWayPartCheck)
				CLEAR_BIT(serverBD.sGetOutOfTheWay.iPlayerBitSet, iServerGetOutOfWayPartCheck)
			ENDIF
		ENDIF
		
		iServerGetOutOfWayPartCheck++
		
		IF iServerGetOutOfWayPartCheck >= NUM_NETWORK_PLAYERS
			iServerGetOutOfWayPartCheck = 0
			
			IF serverBD.sGetOutOfTheWay.iPlayerBitSet = 0
				CLEAR_BIT(serverBD.sGetOutOfTheWay.iCreationBitSet, SV_WLK_OUT_SERVER_RUN_PLAYER_CLEAN_UP_CHECKS)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC VECTOR SV_GET_SAFE_WALK_TO_COORD(VECTOR vReturn)
	IF vReturn.y < -2999.0
		IF vReturn.x < 962.6
			vReturn.x = 958.7
		ELIF vReturn.x < 970.3
			vReturn.x = 966.7
		ELSE
			vReturn.x = 974.3
		ENDIF
	ELSE
		vReturn.y = -2999.7
	ENDIF
	
	RETURN vReturn
ENDFUNC

FUNC VECTOR GET_CLIENT_GET_OUT_OF_WAY_POSTIION()
	PLAYER_INDEX playerID
	INT iLoop
	
	REPEAT NUM_NETWORK_PLAYERS iLoop
		CLEAR_BIT(serverBD.sGetOutOfTheWay.iClientRequestBS, iLoop)
		playerID = INT_TO_PLAYERINDEX(iLoop)
		
		IF IS_NET_PLAYER_OK(playerID, FALSE)
			IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerID)
				IF NOT ARE_VECTORS_EQUAL(playerBD[NATIVE_TO_INT(playerID)].sGetOutOfTheWay.vGetOutOfWayLoc, <<0.0, 0.0, 0.0>>)
					SET_BIT(serverBD.sGetOutOfTheWay.iClientRequestBS, NATIVE_TO_INT(playerID))
					RETURN playerBD[NATIVE_TO_INT(playerID)].sGetOutOfTheWay.vGetOutOfWayLoc
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC BOOL IS_PLAYER_CLEAR_OF_SV(VECTOR vPos)
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF GET_DISTANCE_BETWEEN_COORDS(vPos, GET_ENTITY_COORDS(PLAYER_PED_ID()), FALSE) < SV_SAFE_DISTANCE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL MAINTAIN_GET_OUT_OF_THE_WAY_SV()
	INT iMyPart = PARTICIPANT_ID_TO_INT()
	
	IF iMyPart = -1
		RETURN FALSE
	ENDIF
	
	INT iMyGBD = NATIVE_TO_INT(PLAYER_ID())
	
	IF (IS_BIT_SET(serverBD.sGetOutOfTheWay.iPlayerBitSet, iMyGBD)
	OR IS_BIT_SET(sGetOutOfWayVars.iProgressBitSet, SV_WLK_OUT_LOCAL_SET_UP_WALK_OUT))
		IF HAS_NET_TIMER_STARTED(getOutOfWayTimer)
		AND NOT HAS_NET_TIMER_EXPIRED(getOutOfWayTimer, 5000)
			SET_BIT(playerBD[iMyGBD].sGetOutOfTheWay.iPlayerBitSet, SV_WLK_OUT_PLAYER_IM_OUT_OF_THE_WAY)
			CLEAR_BIT(sGetOutOfWayVars.iProgressBitSet, SV_WLK_OUT_LOCAL_SET_UP_WALK_OUT)
			RESET_NET_TIMER(sGetOutOfWayVars.controlTimer)
		ELSE
			IF NOT IS_BIT_SET(playerBD[iMyGBD].sGetOutOfTheWay.iPlayerBitSet, SV_WLK_OUT_PLAYER_IM_OUT_OF_THE_WAY)
				IF NOT IS_BIT_SET(sGetOutOfWayVars.iProgressBitSet, SV_WLK_OUT_LOCAL_SET_UP_WALK_OUT)
					IF IS_NET_PLAYER_OK(PLAYER_ID())
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CLEAR_TASKS)			
						
						VECTOR vPlayer = GET_ENTITY_COORDS(PLAYER_PED_ID(), TRUE)
						
						sGetOutOfWayVars.vSafePos = SV_GET_SAFE_WALK_TO_COORD(vPlayer)
						TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), sGetOutOfWayVars.vSafePos, PEDMOVE_WALK, SV_WALK_OUT_SAFETY_TIME)
						REINIT_NET_TIMER(sGetOutOfWayVars.controlTimer, TRUE)
						SET_BIT(sGetOutOfWayVars.iProgressBitSet, SV_WLK_OUT_LOCAL_SET_UP_WALK_OUT)		
					ENDIF
				ELSE
					IF HAS_NET_TIMER_EXPIRED(sGetOutOfWayVars.controlTimer, SV_WALK_OUT_SAFETY_TIME, TRUE)
					OR IS_PLAYER_CLEAR_OF_SV(sGetOutOfWayVars.vSafePos)
						IF IS_NET_PLAYER_OK(PLAYER_ID())
							IF NOT IS_PLAYER_CLEAR_OF_SV(sGetOutOfWayVars.vSafePos)
								IF GET_DISTANCE_BETWEEN_COORDS(sGetOutOfWayVars.vSafePos, GET_ENTITY_COORDS(PLAYER_PED_ID())) > 0.5
									SET_ENTITY_COORDS(PLAYER_PED_ID(), sGetOutOfWayVars.vSafePos)
								ENDIF
							ENDIF
						ENDIF
						
						SET_BIT(playerBD[iMyGBD].sGetOutOfTheWay.iPlayerBitSet, SV_WLK_OUT_PLAYER_IM_OUT_OF_THE_WAY)
						CLEAR_BIT(sGetOutOfWayVars.iProgressBitSet, SV_WLK_OUT_LOCAL_SET_UP_WALK_OUT)
						RESET_NET_TIMER(sGetOutOfWayVars.controlTimer)
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					ELSE
						IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) != PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) != WAITING_TO_START_TASK
							TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), sGetOutOfWayVars.vSafePos, PEDMOVE_WALK, SV_WALK_OUT_SAFETY_TIME)
						ENDIF
					ENDIF
					
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		CLEAR_BIT(playerBD[iMyGBD].sGetOutOfTheWay.iPlayerBitSet, SV_WLK_OUT_PLAYER_IM_OUT_OF_THE_WAY)
		
		IF playerBD[iMyGBD].sGetOutOfTheWay.iPlayerBitSet != 0
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_MODDED_SVM_VEHICLE_FOR_GARAGE(VEHICLE_SETUP_STRUCT vehicleSetup, INT i, INT iSaveSlot, BOOL bHasEmblem)
	VEHICLE_INDEX tempVehicle
	
	IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.customVehicleNetIDs[i])
		IF IS_MODEL_IN_CDIMAGE(vehicleSetup.eModel)
			IF REQUEST_LOAD_MODEL(vehicleSetup.eModel)
				IF CAN_RESERVE_NETWORK_VEHICLES_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_VEHICLES() + 1, FALSE, TRUE)
					IF NOT IS_BIT_SET(serverBD.iVehicleResBS, i)
						RESERVE_NETWORK_MISSION_VEHICLES(GET_NUM_RESERVED_MISSION_VEHICLES() + 1)
						serverBD.iReserved++
						SET_BIT(serverBD.iVehicleResBS, i)
					ENDIF
					
					SERVER_CHECK_ANY_PLAYERS_ARE_IN_THE_WAY_OF_SVM_VEHICLES(vPVPositions[i])
					
					IF NOT IS_IT_SAFE_TO_CREATE_AN_SVM_VEHICLE()
						RETURN FALSE
					ENDIF
					
					IF IS_BIT_SET(serverBD.iVehicleResBS, i)
						IF CAN_REGISTER_MISSION_ENTITIES(0, 1, 0, 0)
							IF NETWORK_IS_IN_MP_CUTSCENE() 
								SET_NETWORK_CUTSCENE_ENTITIES(TRUE)	
							ENDIF
							
							tempVehicle = CREATE_VEHICLE(vehicleSetup.eModel, vPVPositions[i], fPVHeadings[i], TRUE, TRUE)	
							serverBD.customVehicleNetIDs[i] = VEH_TO_NET(tempVehicle)
							SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(tempVehicle, TRUE)	
							serverBD.iCarSpawnBS[i] = 0
							
							IF bHasEmblem
								MP_SAVE_VEHICLE_APPLY_SAVED_DETAILS_TO_VEHICLE_INC_EMBLEM(tempVehicle, iSaveSlot)
							ELSE
								MP_SAVE_VEHICLE_APPLY_SAVED_DETAILS_TO_VEHICLE(tempVehicle, iSaveSlot)
								SET_BIT(serverBD.iCarSpawnBS[i], MP_PROP_CREATE_SVM_VEHICLES_BS_APPLIED_DETAILS)
							ENDIF
							
							serverbD.ivehicleSaveSlot[i] = iSaveSlot
							NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(tempVehicle, TRUE)
							SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(tempVehicle, FALSE)
							SET_VEHICLE_FULLBEAM(tempVehicle, FALSE)
							SET_VEHICLE_LIGHTS(tempVehicle, FORCE_VEHICLE_LIGHTS_OFF)
							SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(tempVehicle, TRUE)
							
							IF thisPERSONAL_MOD_GARAGE.pOwner != INVALID_PLAYER_INDEX()
								IF IS_NET_PLAYER_OK(thisPERSONAL_MOD_GARAGE.pOwner, FALSE, FALSE)
									SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(tempVehicle, thisPERSONAL_MOD_GARAGE.pOwner, FALSE)
								ENDIF
							ENDIF
							
							SET_VEHICLE_FIXED(tempVehicle)
					        SET_ENTITY_HEALTH(tempVehicle, 1000)
					        SET_VEHICLE_ENGINE_HEALTH(tempVehicle, 1000)
					        SET_VEHICLE_PETROL_TANK_HEALTH(tempVehicle, 1000)
							SET_VEHICLE_DIRT_LEVEL(tempVehicle, 0.0)
			                SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(tempVehicle, TRUE)
							SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(tempVehicle, TRUE)
							CLEAR_BIT(serverBD.iCreatedBS, i)
							SET_ENTITY_CAN_BE_DAMAGED(tempVehicle, FALSE)
							SET_ENTITY_VISIBLE(tempVehicle, FALSE)
							
							IF (vehicleSetup.eModel = DUNE5)
							OR (vehicleSetup.eModel = BOXVILLE5)
							OR (vehicleSetup.eModel = WASTELANDER)
								SET_VEHICLE_ENVEFF_SCALE(tempVehicle, 1.0)
							ENDIF
							
							IF DECOR_IS_REGISTERED_AS_TYPE("Player_Vehicle", DECOR_TYPE_INT)
								IF NOT DECOR_EXIST_ON(tempVehicle, "Player_Vehicle")
									DECOR_SET_INT(tempVehicle, "Player_Vehicle", NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
								ENDIF
							ENDIF
							
							INT iMPBitset
							IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
								IF DECOR_EXIST_ON(tempVehicle, "MPBitset")	
									iMPBitset = DECOR_GET_INT(tempVehicle, "MPBitset")
								ENDIF
								SET_BIT(iMPBitset, MP_DECORATOR_BS_SVM_VEHICLE)
								DECOR_SET_INT(tempVehicle, "MPBitset", iMPBitset)
							ENDIF
							
							SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(serverbD.customVehicleNetIDs[i], PLAYER_ID(), FALSE)
							
							IF NETWORK_IS_IN_MP_CUTSCENE()
								SET_NETWORK_CUTSCENE_ENTITIES(FALSE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(serverBD.iCreatedBS, i)
			IF NOT IS_BIT_SET(serverBD.iCarSpawnBS[i], MP_PROP_CREATE_SVM_VEHICLES_BS_MODS_LOADED)
				tempVehicle = NET_TO_VEH(serverBD.customVehicleNetIDs[i])
				
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
					IF NOT IS_BIT_SET(serverBD.iCarSpawnBS[i], MP_PROP_CREATE_SVM_VEHICLES_BS_APPLIED_DETAILS)
						IF bHasEmblem
							IF MP_SAVE_VEHICLE_APPLY_SAVED_DETAILS_TO_VEHICLE_INC_EMBLEM(tempVehicle, iSaveSlot)
								SET_BIT(serverBD.iCarSpawnBS[i], MP_PROP_CREATE_SVM_VEHICLES_BS_APPLIED_DETAILS)
							ENDIF
						ELSE
							MP_SAVE_VEHICLE_APPLY_SAVED_DETAILS_TO_VEHICLE(tempVehicle, iSaveSlot)
							SET_BIT(serverBD.iCarSpawnBS[i], MP_PROP_CREATE_SVM_VEHICLES_BS_APPLIED_DETAILS)
						ENDIF
					ELSE
						IF HAVE_VEHICLE_MODS_STREAMED_IN(tempVehicle)
							SET_VEHICLE_ON_GROUND_PROPERLY(tempVehicle)
							SET_BIT(serverBD.iCarSpawnBS[i], MP_PROP_CREATE_SVM_VEHICLES_BS_MODS_LOADED)
							serverBD.iVehicleCreated++
							SET_BIT(serverBD.iCreatedBS, i)
							SET_ENTITY_VISIBLE(tempVehicle, TRUE)
							SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(tempVehicle, SC_WHEEL_CAR_FRONT_LEFT, 0.0)
							SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(tempVehicle, SC_WHEEL_CAR_FRONT_RIGHT, 0.0)
							SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(tempVehicle, SC_WHEEL_CAR_MID_LEFT, 0.0)
							SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(tempVehicle, SC_WHEEL_CAR_MID_RIGHT, 0.0)
							SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(tempVehicle, SC_WHEEL_CAR_REAR_LEFT, 0.0)
							SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(tempVehicle, SC_WHEEL_CAR_REAR_RIGHT, 0.0)
							SET_CAN_USE_HYDRAULICS(tempVehicle, FALSE)
							SET_MODEL_AS_NO_LONGER_NEEDED(vehicleSetup.eModel)
							SET_VEHICLE_DOORS_LOCKED(tempVehicle, VEHICLELOCK_UNLOCKED)
							SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(tempVehicle, TRUE)
							
							IF thisPERSONAL_MOD_GARAGE.pOwner != INVALID_PLAYER_INDEX()
								IF IS_NET_PLAYER_OK(thisPERSONAL_MOD_GARAGE.pOwner, FALSE, FALSE)
									SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(tempVehicle, thisPERSONAL_MOD_GARAGE.pOwner, FALSE)
								ENDIF
							ENDIF
							
			                SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(tempVehicle, TRUE)
							SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(tempVehicle, TRUE)
						ENDIF
					ENDIF
				ELSE
					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
				ENDIF
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_SVM_GARAGE(PLAYER_INDEX playerID)
	IF IS_ENTITY_IN_ANGLED_AREA(GET_PLAYER_PED(playerID), <<970.408752, -3013.567871, -48.542145>>, <<970.388916, -2985.817627, -43.785622>>, 68.4375)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC FORCE_ZERO_MASS_FOR_ALL_PLAYERS_IN_SVM_GARAGE()
	INT i
	PLAYER_INDEX playerID
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			
			IF IS_NET_PLAYER_OK(playerID)
				IF IS_PLAYER_IN_SVM_GARAGE(playerID)
					FORCE_ZERO_MASS_IN_COLLISIONS(GET_PLAYER_PED(playerID))
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC MAINTAIN_GET_OUT_OF_OWNERS_SVM_VEHICLE()
	VEHICLE_INDEX vehIndex
	PED_INDEX DriverPedID
	
	IF thisPERSONAL_MOD_GARAGE.pOwner != PLAYER_ID()
		IF IS_NET_PLAYER_OK(PLAYER_ID())
		AND thisPERSONAL_MOD_GARAGE.eState = PERSONAL_MOD_GARAGE_STATE_IDLE
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				vehIndex = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				
				IF IS_SVM_VEHICLE(vehIndex)
					IF thisPERSONAL_MOD_GARAGE.pOwner != INVALID_PLAYER_INDEX()
						IF IS_NET_PLAYER_OK(thisPERSONAL_MOD_GARAGE.pOwner, FALSE, TRUE)
							IF NOT IS_VEHICLE_SEAT_FREE(vehIndex)
								DriverPedID = GET_PED_IN_VEHICLE_SEAT(vehIndex, VS_DRIVER)	
								
								IF DOES_ENTITY_EXIST(DriverPedID)
								AND IS_PED_A_PLAYER(DriverPedID)
									IF NETWORK_GET_PLAYER_INDEX_FROM_PED(DriverPedID) != thisPERSONAL_MOD_GARAGE.pOwner
										IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
										AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
											TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
									TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
								ENDIF
							ENDIF
						ELSE
							IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
								TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF thisPERSONAL_MOD_GARAGE.pOwner != INVALID_PLAYER_INDEX()
			IF IS_NET_PLAYER_OK(thisPERSONAL_MOD_GARAGE.pOwner)
				IF NOT IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(thisPERSONAL_MOD_GARAGE.pOwner), FALSE)
				OR NOT IS_VEHICLE_SEAT_FREE(GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(thisPERSONAL_MOD_GARAGE.pOwner)), VS_ANY_PASSENGER)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL ARE_ALL_PEDS_IN_SV_READY(VEHICLE_INDEX theVeh #IF IS_DEBUG_BUILD , BOOL bOutputDebug = FALSE #ENDIF)
	INT i
	PLAYER_INDEX thePlayer
	REPEAT NUM_NETWORK_PLAYERS i
		thePlayer = INT_TO_PLAYERINDEX(i)
		
		IF thePlayer != PLAYER_ID()
		AND thePlayer != INVALID_PLAYER_INDEX()
		AND NETWORK_IS_PLAYER_A_PARTICIPANT(thePlayer)
			IF IS_NET_PLAYER_OK(thePlayer)
				IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(thePlayer), theVeh, FALSE)
					IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(thePlayer)].iBS, BS_IE_WAREHOUSE_READY_TO_WARP_OUT_W_OWNER)
						#IF IS_DEBUG_BUILD
						IF bOutputDebug
							PRINTLN("ARE_ALL_PEDS_IN_SV_READY: ", GET_PLAYER_NAME(thePlayer)," not ready not set flag")
						ENDIF
						#ENDIF
						
						RETURN FALSE
					ENDIF
				ELSE
					IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(thePlayer), theVeh, TRUE)
					OR IS_ENTITY_ATTACHED_TO_ENTITY(GET_PLAYER_PED(thePlayer), theVeh)
						#IF IS_DEBUG_BUILD
						IF bOutputDebug
							PRINTLN("ARE_ALL_PEDS_IN_SV_READY: ", GET_PLAYER_NAME(thePlayer)," entering or attached")
						ENDIF
						#ENDIF
						
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CAN_PLAYER_DRIVE_OUT_OF_IE_GARAGE()
	IF NOT ARE_ALL_PEDS_IN_SV_READY(currentVeh #IF IS_DEBUG_BUILD , TRUE #ENDIF)
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_IE_GARAGE - False NOT ARE_ALL_PEDS_IN_SV_READY")
		RETURN FALSE
	ENDIF
	
	IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) = PERFORMING_TASK
	OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) = WAITING_TO_START_TASK
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_IE_GARAGE - False leaving a vehicle")
		RETURN FALSE
	ENDIF
	
	IF g_bDisableVehicleWarehouseVehExit
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_IE_GARAGE - g_bDisableVehicleWarehouseVehExit is TRUE")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_CAN_DRIVE_OUT_OF_IE_GARAGE()
	IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND NOT g_MultiplayerSettings.g_bSuicide
	AND NOT IS_SELECTOR_ONSCREEN()
		DISABLE_SELECTOR_THIS_FRAME()
		SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_IE_WAREHOUSE_READY_TO_WARP_OUT_W_OWNER)
	ELSE
		CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_IE_WAREHOUSE_READY_TO_WARP_OUT_W_OWNER)
	ENDIF
ENDPROC

PROC MAINTAIN_SVM_VEHICLE_LOCKS()
	g_bPlayerLeavingCurrentInteriorInVeh = FALSE
	IF thisPERSONAL_MOD_GARAGE.pOwner = PLAYER_ID()
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), FALSE)
			currentVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)
			
			IF currentVeh != lastVeh
				IF NETWORK_HAS_CONTROL_OF_ENTITY(currentVeh)
					lastVeh = currentVeh
					SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(currentVeh, FALSE)
					SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(currentVeh, FALSE)
				ELSE
					NETWORK_REQUEST_CONTROL_OF_ENTITY(currentVeh)
				ENDIF
			ENDIF
			
			IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF GET_IS_VEHICLE_ENGINE_RUNNING(currentVeh)
				AND PLAYER_PED_ID() = GET_PED_IN_VEHICLE_SEAT(currentVeh)
					IF CAN_PLAYER_DRIVE_OUT_OF_IE_GARAGE()
						IF (GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_VEH_ACCELERATE) != 0
						OR GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_VEH_BRAKE) != 0
						OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
						OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
						OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
						OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
						OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
						OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
						OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE))
						AND NOT g_MultiplayerSettings.g_bSuicide	
						AND NOT IS_SELECTOR_ONSCREEN()
							g_bPlayerLeavingCurrentInteriorInVeh = TRUE
							
							thisPERSONAL_MOD_GARAGE.bDrivingOut = TRUE
							
							PRINTLN("g_bPlayerLeavingCurrentInteriorInVeh = TRUE IE_WAREHOUSE")
							IF NETWORK_HAS_CONTROL_OF_ENTITY(currentVeh)
								SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(currentVeh, TRUE) 
								SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(currentVeh, TRUE)
								
								DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
							ELSE
								NETWORK_REQUEST_CONTROL_OF_ENTITY(currentVeh)
							ENDIF
						ENDIF
					ELSE
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_VEHICLE_DRIVEABLE(lastVeh)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(lastVeh)
					SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(lastVeh, TRUE)
		        	SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(lastVeh, PLAYER_ID(), FALSE)
					lastVeh = NULL
				ELSE
					NETWORK_REQUEST_CONTROL_OF_ENTITY(lastVeh)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC RESET_SVM_EXITING_FLAG()
	INT i
	INT iSaveSlot
	INT iDisplaySlot
	
	REPEAT MAX_SVM_VEHICLES_IN_GARAGE i
		iDisplaySlot = i + GET_PROPERTY_SLOT_DISPLAY_START_INDEX(PROPERTY_OWNED_SLOT_IE_WAREHOUSE)
		MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(iDisplaySlot, iSaveSlot, FALSE)
		
		IF iSaveSlot >= 0
		AND IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_EXITING_SIMPLE_INTERIOR)
			CLEAR_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_EXITING_SIMPLE_INTERIOR)
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL MAINTAIN_SVM_GARAGE_VEHICLES()
	BOOL bHasEmblem
	BOOL bDeleteVeh = FALSE
	
	INT i = 0
	INT iSaveSlot
	INT iDisplaySlot
	
	REPEAT MAX_SVM_VEHICLES_IN_GARAGE i
		bDeleteVeh = FALSE
		iDisplaySlot = i + GET_PROPERTY_SLOT_DISPLAY_START_INDEX(PROPERTY_OWNED_SLOT_IE_WAREHOUSE)
		
		MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(iDisplaySlot, iSaveSlot, FALSE)
		
		IF thisPERSONAL_MOD_GARAGE.pOwner = PLAYER_ID()
			IF iSaveSlot >= 0
			AND g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup.eModel != DUMMY_MODEL_FOR_SCRIPT
			AND IS_VEHICLE_AVAILABLE_FOR_GAME(g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup.eModel, TRUE)
			AND IS_SVM_VEHICLE_MODEL(g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup.eModel)
				IF NOT IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_OUT_GARAGE)
				AND NOT IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_DESTROYED)
				AND NOT IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_IMPOUNDED)
				AND NOT IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_EXITING_SIMPLE_INTERIOR)
				AND MPGlobalsAmbience.iVDPersonalVehicleSlot != iSaveSlot
				AND NOT thisPERSONAL_MOD_GARAGE.bApplyRenovation
					IF IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_UPDATE_VEHICLE)
						IF NOT HAS_NET_TIMER_STARTED(failSafeClearVehicleDelay[i])
							START_NET_TIMER(failSafeClearVehicleDelay[i], TRUE)
						ENDIF
						
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.customVehicleNetIDs[i])
							IF IS_NET_VEHICLE_DRIVEABLE(serverBD.customVehicleNetIDs[i])
								IF GET_VEHICLE_DOOR_LOCK_STATUS(NET_TO_VEH(serverBD.customVehicleNetIDs[i])) != VEHICLELOCK_LOCKED
									IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
										SET_VEHICLE_DOORS_LOCKED(NET_TO_VEH(serverBD.customVehicleNetIDs[i]), VEHICLELOCK_LOCKED)
									ELSE
										NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
									ENDIF
								ENDIF
								
								IF IS_VEHICLE_EMPTY(NET_TO_VEH(serverBD.customVehicleNetIDs[i]), TRUE, TRUE)
								OR HAS_NET_TIMER_EXPIRED(failSafeClearVehicleDelay[i], 3000, TRUE)
									IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
										IF GET_ENTITY_MODEL(NET_TO_VEH(serverBD.customVehicleNetIDs[i])) = PHANTOM2
											bPhantom2Created = FALSE
										ELIF GET_ENTITY_MODEL(NET_TO_VEH(serverBD.customVehicleNetIDs[i])) = DUNE5
											bDune5Created = FALSE
										ENDIF
										
										DELETE_NET_ID(serverBD.customVehicleNetIDs[i])
										serverBD.ivehicleSaveSlot[i] = -1
										serverBD.iReserved--
										RESERVE_NETWORK_MISSION_VEHICLES(GET_NUM_RESERVED_MISSION_VEHICLES() - 1)
										CLEAR_BIT(serverBD.iVehicleResBS, i)
										CLEAR_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_UPDATE_VEHICLE)
									ELSE
										NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
									ENDIF
								ENDIF
							ELSE
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
									IF GET_ENTITY_MODEL(NET_TO_VEH(serverBD.customVehicleNetIDs[i])) = PHANTOM2
										bPhantom2Created = FALSE
									ELIF GET_ENTITY_MODEL(NET_TO_VEH(serverBD.customVehicleNetIDs[i])) = DUNE5
										bDune5Created = FALSE
									ENDIF
									
									DELETE_NET_ID(serverBD.customVehicleNetIDs[i])
									serverBD.ivehicleSaveSlot[i] = -1
									serverBD.iReserved--
									RESERVE_NETWORK_MISSION_VEHICLES(GET_NUM_RESERVED_MISSION_VEHICLES() - 1)
									CLEAR_BIT(serverBD.iVehicleResBS, i)
									CLEAR_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_UPDATE_VEHICLE)
								ELSE
									NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
								ENDIF
							ENDIF
						ELSE
							CLEAR_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_UPDATE_VEHICLE)
						ENDIF
					ELIF NOT IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_JUST_PURCHASED)
						IF IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_HAS_CREW_EMBLEM)
							bHasEmblem = TRUE
						ELSE
							bHasEmblem = FALSE
						ENDIF
						
						RUN_COMMERCIAL_VEHICLE_IN_SLOT_CHECK(iSaveSlot)
						
						IF NOT IS_MODEL_VALID_FOR_PERSONAL_VEHICLE(g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup.eModel)
							CLEAR_MP_SAVED_VEHICLE_SLOT(iSaveSlot)
						ELSE
							IF NOT CREATE_MODDED_SVM_VEHICLE_FOR_GARAGE(g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup, i, iSaveSlot, bHasEmblem)
								RETURN FALSE
							ELSE
								IF g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup.eModel = PHANTOM2
									bPhantom2Created = TRUE
								ELIF g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup.eModel = DUNE5
									bDune5Created = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						RESET_NET_TIMER(failSafeClearVehicleDelay[i])
					ELSE
						bDeleteVeh = TRUE
					ENDIF
				ELSE
					bDeleteVeh = TRUE
				ENDIF
			ELSE
				bDeleteVeh = TRUE
			ENDIF
			
			IF iSaveSlot >= 0
			AND IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_EXITING_SIMPLE_INTERIOR)
				PRINTLN("AM_MP_IE_WAREHOUSE - MAINTAIN_SVM_GARAGE_VEHICLES - MP_SAVED_VEHICLE_EXITING_SIMPLE_INTERIOR is set, not deleting")
				
				bDeleteVeh = FALSE
			ENDIF
			
			IF bDeleteVeh
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.customVehicleNetIDs[i])
					IF IS_NET_VEHICLE_DRIVEABLE(serverBD.customVehicleNetIDs[i])
						IF GET_VEHICLE_DOOR_LOCK_STATUS(NET_TO_VEH(serverBD.customVehicleNetIDs[i])) != VEHICLELOCK_LOCKED
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
								SET_VEHICLE_DOORS_LOCKED(NET_TO_VEH(serverBD.customVehicleNetIDs[i]), VEHICLELOCK_LOCKED)
							ELSE
								NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
							ENDIF
						ENDIF
						
						IF NOT HAS_NET_TIMER_STARTED(failSafeClearVehicleDelay[i])
							START_NET_TIMER(failSafeClearVehicleDelay[i], TRUE)
						ENDIF
						
						IF IS_VEHICLE_EMPTY(NET_TO_VEH(serverBD.customVehicleNetIDs[i]), TRUE, TRUE)
						OR HAS_NET_TIMER_EXPIRED(failSafeClearVehicleDelay[i], 3000, TRUE)
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
								IF GET_ENTITY_MODEL(NET_TO_VEH(serverBD.customVehicleNetIDs[i])) = PHANTOM2
									bPhantom2Created = FALSE
								ELIF GET_ENTITY_MODEL(NET_TO_VEH(serverBD.customVehicleNetIDs[i])) = DUNE5
									bDune5Created = FALSE
								ENDIF
								
								DELETE_NET_ID(serverBD.customVehicleNetIDs[i])
								RESERVE_NETWORK_MISSION_VEHICLES(GET_NUM_RESERVED_MISSION_VEHICLES() - 1)
								serverBD.iReserved--
								CLEAR_BIT(serverBD.iVehicleResBS, i)
								
								IF iSaveSlot >= 0
									CLEAR_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_UPDATE_VEHICLE)
								ENDIF
								
								serverBD.ivehicleSaveSlot[i] = -1
							ELSE
								IF iSaveSlot >= 0
									IF NOT IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_UPDATE_VEHICLE)
										SET_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_UPDATE_VEHICLE)
									ENDIF
								ENDIF
								
								NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
							ENDIF
						ELSE
							IF IS_NET_PLAYER_OK(PLAYER_ID())
								IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.customVehicleNetIDs[i]))
									TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), 0, ECF_WARP_IF_DOOR_IS_BLOCKED)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
							IF GET_ENTITY_MODEL(NET_TO_VEH(serverBD.customVehicleNetIDs[i])) = PHANTOM2
								bPhantom2Created = FALSE
							ELIF GET_ENTITY_MODEL(NET_TO_VEH(serverBD.customVehicleNetIDs[i])) = DUNE5
								bDune5Created = FALSE
							ENDIF
							
							DELETE_NET_ID(serverBD.customVehicleNetIDs[i])
							RESERVE_NETWORK_MISSION_VEHICLES(GET_NUM_RESERVED_MISSION_VEHICLES() - 1)
							serverBD.iReserved--
							CLEAR_BIT(serverBD.iVehicleResBS, i)
							serverBD.ivehicleSaveSlot[i] = -1
						ELSE
							NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
						ENDIF
					ENDIF
				ELSE
					RESET_NET_TIMER(failSafeClearVehicleDelay[i])
				ENDIF
			ENDIF
		ELSE
			IF thisPERSONAL_MOD_GARAGE.pOwner != INVALID_PLAYER_INDEX()
				IF IS_NET_PLAYER_OK(thisPERSONAL_MOD_GARAGE.pOwner, FALSE, TRUE)
					IF NOT NETWORK_IS_PLAYER_A_PARTICIPANT(thisPERSONAL_MOD_GARAGE.pOwner)
						IF GlobalplayerBD_FM[NATIVE_TO_INT(thisPERSONAL_MOD_GARAGE.pOwner)].iCurrentsActivePersonalVehicle = serverbD.ivehicleSaveSlot[i]
						AND serverBD.ivehicleSaveSlot[i] >= 0
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.customVehicleNetIDs[i])
								IF IS_NET_VEHICLE_DRIVEABLE(serverBD.customVehicleNetIDs[i])	
									IF NOT HAS_NET_TIMER_STARTED(failSafeClearVehicleDelay[i])
										START_NET_TIMER(failSafeClearVehicleDelay[i], TRUE)
									ENDIF
									
									IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
										IF GET_VEHICLE_DOOR_LOCK_STATUS(NET_TO_VEH(serverBD.customVehicleNetIDs[i])) != VEHICLELOCK_LOCKED
											SET_VEHICLE_DOORS_LOCKED(NET_TO_VEH(serverBD.customVehicleNetIDs[i]), VEHICLELOCK_LOCKED)
										ENDIF
									ENDIF
									
									IF IS_VEHICLE_EMPTY(NET_TO_VEH(serverBD.customVehicleNetIDs[i]), TRUE, TRUE)
									OR HAS_NET_TIMER_EXPIRED(failSafeClearVehicleDelay[i], 3000, TRUE)
										IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
											IF GET_ENTITY_MODEL(NET_TO_VEH(serverBD.customVehicleNetIDs[i])) = PHANTOM2
												bPhantom2Created = FALSE
											ELIF GET_ENTITY_MODEL(NET_TO_VEH(serverBD.customVehicleNetIDs[i])) = DUNE5
												bDune5Created = FALSE
											ENDIF
											
											DELETE_NET_ID(serverBD.customVehicleNetIDs[i])
											serverBD.ivehicleSaveSlot[i] = -1
											RESERVE_NETWORK_MISSION_VEHICLES(GET_NUM_RESERVED_MISSION_VEHICLES() - 1)
											serverBD.iReserved--
											CLEAR_BIT(serverBD.iVehicleResBS, i)
											
											IF iSaveSlot >= 0
												CLEAR_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_UPDATE_VEHICLE)
											ENDIF
										ELSE
											NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
										ENDIF
									ELSE
										IF IS_NET_PLAYER_OK(PLAYER_ID())
											IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.customVehicleNetIDs[i]))
												TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), 0, ECF_WARP_IF_DOOR_IS_BLOCKED)
											ENDIF
										ENDIF
									ENDIF
								ELSE
									IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
										IF GET_ENTITY_MODEL(NET_TO_VEH(serverBD.customVehicleNetIDs[i])) = PHANTOM2
											bPhantom2Created = FALSE
										ELIF GET_ENTITY_MODEL(NET_TO_VEH(serverBD.customVehicleNetIDs[i])) = DUNE5
											bDune5Created = FALSE
										ENDIF
										
										DELETE_NET_ID(serverBD.customVehicleNetIDs[i])
										serverBD.ivehicleSaveSlot[i] = -1
										RESERVE_NETWORK_MISSION_VEHICLES(GET_NUM_RESERVED_MISSION_VEHICLES() - 1)
										serverBD.iReserved--
										CLEAR_BIT(serverBD.iVehicleResBS, i)
										
										IF iSaveSlot >= 0
											CLEAR_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_UPDATE_VEHICLE)
										ENDIF
									ELSE
										NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
									ENDIF
								ENDIF
							ELSE
								IF i = GET_SVM_VEHICLE_INDEX(PHANTOM2)
									bPhantom2Created = FALSE
								ELIF i = GET_SVM_VEHICLE_INDEX(DUNE5)
									bDune5Created = FALSE
								ENDIF
								
								RESET_NET_TIMER(failSafeClearVehicleDelay[i])
							ENDIF
						ELSE
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.customVehicleNetIDs[i])
								IF i = GET_SVM_VEHICLE_INDEX(PHANTOM2)
									bPhantom2Created = TRUE
								ELIF i = GET_SVM_VEHICLE_INDEX(DUNE5)
									bDune5Created = TRUE
								ENDIF
							ELSE
								IF i = GET_SVM_VEHICLE_INDEX(PHANTOM2)
									bPhantom2Created = FALSE
								ELIF i = GET_SVM_VEHICLE_INDEX(DUNE5)
									bDune5Created = FALSE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.customVehicleNetIDs[i])
			IF IS_BIT_SET(serverBD.iCreatedBS, i)
				serverBD.iVehicleCreated--
				CLEAR_BIT(serverBD.iVehicleResBS, i)
				CLEAR_BIT(serverBD.iCreatedBS, i)
				serverBD.iCarSpawnBS[i] = 0	
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_INT: MAINTAIN_SVM_GARAGE_VEHICLES: a vehicle is now not driveable reducing serverBD.iVehicleCreated to :", serverBD.iVehicleCreated)
			ENDIF
			
			RESET_NET_TIMER(failSafeClearVehicleDelay[i])
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

PROC SET_PERSONAL_VEHICLE_COORDS()
	vPVPositions[0] = thisPERSONAL_MOD_GARAGE.interiorDetails.vInteriorPosition + <<3.2237, -7.5889, -12.8555>> // TECHNICAL2
	vPVPositions[1] = thisPERSONAL_MOD_GARAGE.interiorDetails.vInteriorPosition + <<-4.5872, -7.3855, -12.647>> // BOXVILLE5
	vPVPositions[2] = thisPERSONAL_MOD_GARAGE.interiorDetails.vInteriorPosition + <<-12.3512, -7.2808, -12.6738>> // WASTELANDER
	vPVPositions[3] = thisPERSONAL_MOD_GARAGE.interiorDetails.vInteriorPosition + <<-20.1239, -7.8441, -13.4795>> // PHANTOM2
	vPVPositions[4] = thisPERSONAL_MOD_GARAGE.interiorDetails.vInteriorPosition + <<-20.178, 6.7109, -12.8886>> // VOLTIC2
	vPVPositions[5] = thisPERSONAL_MOD_GARAGE.interiorDetails.vInteriorPosition + <<-12.3901, 7.4053, -12.7351>> // DUNE5
	vPVPositions[6] = thisPERSONAL_MOD_GARAGE.interiorDetails.vInteriorPosition + <<3.2148, 7.6892, -12.9813>> // RUINER2
	vPVPositions[7] = thisPERSONAL_MOD_GARAGE.interiorDetails.vInteriorPosition + <<11.0604, 6.6609, -13.1537>> // BLAZER5
	
	fPVHeadings[0] = thisPERSONAL_MOD_GARAGE.interiorDetails.fInteriorHeading + 0.0 // TECHNICAL2
	fPVHeadings[1] = thisPERSONAL_MOD_GARAGE.interiorDetails.fInteriorHeading + 0.0 // BOXVILLE5
	fPVHeadings[2] = thisPERSONAL_MOD_GARAGE.interiorDetails.fInteriorHeading + 0.0 // WASTELANDER
	fPVHeadings[3] = thisPERSONAL_MOD_GARAGE.interiorDetails.fInteriorHeading + 0.0 // PHANTOM2
	fPVHeadings[4] = thisPERSONAL_MOD_GARAGE.interiorDetails.fInteriorHeading + 180.0 // VOLTIC2
	fPVHeadings[5] = thisPERSONAL_MOD_GARAGE.interiorDetails.fInteriorHeading + 180.0 // DUNE5
	fPVHeadings[6] = thisPERSONAL_MOD_GARAGE.interiorDetails.fInteriorHeading + 180.0 // RUINER2
	fPVHeadings[7] = thisPERSONAL_MOD_GARAGE.interiorDetails.fInteriorHeading + 180.0 // BLAZER5
ENDPROC

FUNC BOOL ARE_PLAYERS_IN_SVM_VEHICLE()
	IF NOT CAN_SIMPLE_INTERIOR_CHILD_SCRIPT_TAKE_CONTROL()
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(thisPERSONAL_MOD_GARAGE.iBS, BS_IE_WAREHOUSE_DATA_DID_A_TEAM_VEH_WARP)
		//IF NET_WARP_TO_COORD(<<978.2659, -2999.1682, -48.6471>>, 0.0)
			IF thisPERSONAL_MOD_GARAGE.pOwner = PLAYER_ID()
				CLEANUP_MP_SAVED_VEHICLE(FALSE, DEFAULT, TRUE, DEFAULT, TRUE)
			ENDIF
			
			SET_BIT(thisPERSONAL_MOD_GARAGE.iBS, BS_IE_WAREHOUSE_DATA_DID_A_TEAM_VEH_WARP)
		//ENDIF
	ENDIF
	
	
	IF IS_BIT_SET(thisPERSONAL_MOD_GARAGE.iBS, BS_IE_WAREHOUSE_DATA_DID_A_TEAM_VEH_WARP)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.customVehicleNetIDs[thisPERSONAL_MOD_GARAGE.iEntranceVehicleIndex])
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.customVehicleNetIDs[thisPERSONAL_MOD_GARAGE.iEntranceVehicleIndex]))
				
				IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.customVehicleNetIDs[thisPERSONAL_MOD_GARAGE.iEntranceVehicleIndex]))
					NETWORK_FADE_IN_ENTITY(PLAYER_PED_ID(), TRUE, FALSE)
					
					RETURN TRUE
				ELSE
					IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE) != WAITING_TO_START_TASK
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.customVehicleNetIDs[thisPERSONAL_MOD_GARAGE.iEntranceVehicleIndex]), SIMPLE_INTERIOR_GET_ENTRY_VEH_SEAT())
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_SVM_VEHICLE_FLAGS()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX vTemp = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
		
		IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
		AND IS_SVM_VEHICLE(vTemp)
			SET_VEHICLE_FIXED(vTemp)
	        SET_ENTITY_HEALTH(vTemp, 1000)
	        SET_VEHICLE_ENGINE_HEALTH(vTemp, 1000)
	        SET_VEHICLE_PETROL_TANK_HEALTH(vTemp, 1000)
			SET_VEHICLE_DIRT_LEVEL(vTemp, 0.0)
			SET_ENTITY_COLLISION(vTemp, TRUE)
			SET_ENTITY_VISIBLE(vTemp, TRUE)
			FREEZE_ENTITY_POSITION(vTemp, FALSE)
			SET_VEHICLE_LIGHTS(vTemp, NO_VEHICLE_LIGHT_OVERRIDE)
			SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vTemp, FALSE)
			SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(vTemp, FALSE)
			SET_VEHICLE_IS_STOLEN(vTemp, FALSE)
			SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(vTemp, FALSE)
			SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(vTemp, FALSE)
			SET_ENTITY_CAN_BE_DAMAGED(vTemp, TRUE)
			SET_CAN_USE_HYDRAULICS(vTemp, TRUE)
		ENDIF
	ENDIF
ENDPROC

PROC SET_MAIN_ENTITY_SETS(BOOL bRefreshInterior = FALSE)

	#IF IS_DEBUG_BUILD
		PRINTLN("[IE_WAREHOUSE_SCRIPT] SET_MAIN_ENTITY_SETS - Doing this.")
	#ENDIF
	
	INTERIOR_INSTANCE_INDEX interiorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<974.9542, -3000.0908, -35.0000>>, "imp_impexp_intwaremed")
	
	IF IS_INTERIOR_ENTITY_SET_ACTIVE(interiorIndex, "Basic_style_set")
		DEACTIVATE_INTERIOR_ENTITY_SET(interiorIndex, "Basic_style_set")
		PRINTLN("[IE_WAREHOUSE_SCRIPT] SET_MAIN_ENTITY_SETS - Deactivating Basic_style_set")
	ENDIF
	IF IS_INTERIOR_ENTITY_SET_ACTIVE(interiorIndex, "Branded_style_set")
		DEACTIVATE_INTERIOR_ENTITY_SET(interiorIndex, "Branded_style_set")
		PRINTLN("[IE_WAREHOUSE_SCRIPT] SET_MAIN_ENTITY_SETS - Deactivating Branded_style_set")
	ENDIF
	IF IS_INTERIOR_ENTITY_SET_ACTIVE(interiorIndex, "Urban_style_set")
		DEACTIVATE_INTERIOR_ENTITY_SET(interiorIndex, "Urban_style_set")
		PRINTLN("[IE_WAREHOUSE_SCRIPT] SET_MAIN_ENTITY_SETS - Deactivating Urban_style_set")
	ENDIF
	
	SWITCH GlobalplayerBD_FM[NATIVE_TO_INT(thisPERSONAL_MOD_GARAGE.pOwner)].propertyDetails.bdIEgarageData.eWarehouseStyle
		CASE IE_INTERIOR_BASIC
			IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(interiorIndex, "Basic_style_set")
				ACTIVATE_INTERIOR_ENTITY_SET(interiorIndex, "Basic_style_set")
				SET_INTERIOR_ENTITY_SET_TINT_INDEX(interiorIndex, "Basic_style_set", 8)
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_WAREHOUSE_SCRIPT] SET_MAIN_ENTITY_SETS - Activating Basic_style_set")
				#ENDIF
			ENDIF
		BREAK
		CASE IE_INTERIOR_BRANDED
			IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(interiorIndex, "Branded_style_set")
				ACTIVATE_INTERIOR_ENTITY_SET(interiorIndex, "Branded_style_set")
				SET_INTERIOR_ENTITY_SET_TINT_INDEX(interiorIndex, "Branded_style_set", 1)
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_WAREHOUSE_SCRIPT] SET_MAIN_ENTITY_SETS - Activating Branded_style_set")
				#ENDIF
			ENDIF
		BREAK
		CASE IE_INTERIOR_URBAN
			IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(interiorIndex, "Urban_style_set")
				ACTIVATE_INTERIOR_ENTITY_SET(interiorIndex, "Urban_style_set")
				SET_INTERIOR_ENTITY_SET_TINT_INDEX(interiorIndex, "Urban_style_set", 0)
			ENDIF
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_WAREHOUSE_SCRIPT] SET_MAIN_ENTITY_SETS - Activating Urban_style_set")
			#ENDIF
		BREAK
		DEFAULT
			IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(interiorIndex, "Basic_style_set")
				ACTIVATE_INTERIOR_ENTITY_SET(interiorIndex, "Basic_style_set")
				SET_INTERIOR_ENTITY_SET_TINT_INDEX(interiorIndex, "Basic_style_set", 8)
			ENDIF
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_WAREHOUSE_SCRIPT] SET_MAIN_ENTITY_SETS - IE warehouse style ID invalid. Activating Basic_style_set (default)")
			#ENDIF
		BREAK
	ENDSWITCH
	
	IF bRefreshInterior
		REFRESH_INTERIOR(interiorIndex)
	ENDIF
	
	thisPERSONAL_MOD_GARAGE.eActiveInterior = GlobalplayerBD_FM[NATIVE_TO_INT(thisPERSONAL_MOD_GARAGE.pOwner)].propertyDetails.bdIEgarageData.eWarehouseStyle
ENDPROC

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════╡  UNDERGROUND GARAGE MANAGEMENT  ╞════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

PROC SET_PIPE_ENTITY_SETS()
	INTERIOR_INSTANCE_INDEX interiorIndex = thisPERSONAL_MOD_GARAGE.interiorUnderground //GET_INTERIOR_AT_COORDS_WITH_TYPE(<<969.5376, -3000.4111, -48.6469>>, "imp_impexp_int_02")
	
	IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(interiorIndex, "pump_01")
		ACTIVATE_INTERIOR_ENTITY_SET(interiorIndex, "pump_01")
	ENDIF
	IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(interiorIndex, "pump_06")
		ACTIVATE_INTERIOR_ENTITY_SET(interiorIndex, "pump_06")
	ENDIF
	IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(interiorIndex, "pump_07")
		ACTIVATE_INTERIOR_ENTITY_SET(interiorIndex, "pump_07")
	ENDIF
	IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(interiorIndex, "pump_08")
		ACTIVATE_INTERIOR_ENTITY_SET(interiorIndex, "pump_08")
	ENDIF
	
	IF IS_INTERIOR_ENTITY_SET_ACTIVE(interiorIndex, "pump_02")
		DEACTIVATE_INTERIOR_ENTITY_SET(interiorIndex, "pump_02")
	ENDIF
	IF IS_INTERIOR_ENTITY_SET_ACTIVE(interiorIndex, "pump_03")
		DEACTIVATE_INTERIOR_ENTITY_SET(interiorIndex, "pump_03")
	ENDIF
	IF IS_INTERIOR_ENTITY_SET_ACTIVE(interiorIndex, "pump_04")
		DEACTIVATE_INTERIOR_ENTITY_SET(interiorIndex, "pump_04")
	ENDIF
	IF IS_INTERIOR_ENTITY_SET_ACTIVE(interiorIndex, "pump_05")
		DEACTIVATE_INTERIOR_ENTITY_SET(interiorIndex, "pump_05")
	ENDIF
	
	REFRESH_INTERIOR(interiorIndex)
ENDPROC

#IF IS_DEBUG_BUILD
PROC DEBUG_MAINTAIN_IE_APP_LAUNCHING()
	IF g_bDebugLaunchappImportExportScript
	
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY("appImportExport")) >= 1
			g_bDebugLaunchappImportExportScript = FALSE
			EXIT
		ENDIF
	
		IF HAS_SCRIPT_LOADED("appImportExport")
			START_NEW_SCRIPT("appImportExport", DEFAULT_STACK_SIZE)
			g_bDebugLaunchappImportExportScript = FALSE
			SET_SCRIPT_AS_NO_LONGER_NEEDED("appImportExport")
		ELSE
			REQUEST_SCRIPT("appImportExport")
		ENDIF
	ENDIF
ENDPROC
#ENDIF

//╒═════════════════════════════════════════════════════════════╕
//╞════════════════════════╡  CLEANUP  ╞════════════════════════╡
//╘═════════════════════════════════════════════════════════════╛

INT iOfficeSeatContextIntention = NEW_CONTEXT_INTENTION

INT iJacuzziNetSceneid

INT iOfficeSeatiBitSet
SCRIPT_TIMER ntOfficeArmchairChangePosInputTimer

INT iPlayerChosenSeatSlot = -1
STRING sOfficeSeatAnimDict

PROC CLEAR_ALL_OFFICE_SEAT_PROMPTS_GARAGE()
	IF iOfficeSeatContextIntention != NEW_CONTEXT_INTENTION
		RELEASE_CONTEXT_INTENTION(iOfficeSeatContextIntention)
	ENDIF
		
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPOFSEAT_EXIT") 
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPOFSEAT_EXIT_L") 
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPOFSEAT_EXIT_C") 
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPOFSEAT_EXIT_R") 
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPOFSEAT_PCEXIT") 
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPOFSEAT_EXIT_PCL") 
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPOFSEAT_EX_PCC") 
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPOFSEAT_EXIT_PCR")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("WHOUSEINPUTTRIG")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("WHOUSEINPUTTRPC")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SECINPUTTREG")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SECINPUTTREGPC")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("OFF_COMP_MIS")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("OFF_COMP_MIS_PC")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("OFF_COMP_BDY")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("OFF_COMP_BDY_PC")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("OFFLAPTOPPC")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("OFFLAPTOP")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPOF_AC_EXIT")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPOF_AC_PC_EXIT")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CLBHINPUTTRIGPC") 
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ORINPUTTREGPC")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CLBHINPUTTRIG") 
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ORINPUTTREG")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("OFF_COMP_CH")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("OFF_COMP_CH_PC")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("OFF_COMP_CHV")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("OFF_COMP_CHV_PC")
		CLEAR_HELP()
	ENDIF
	
	CLEAR_BIT(iOfficeSeatiBitSet, OFFICE_SEAT_BS_EXIT_HELP_TEXT_DISPLAYED)
ENDPROC



/// PURPOSE:
///    Returns the vector the player should be placed at when cleaning up the seat activity in the IE warehouse
/// PARAMS:
///    iSeatSlot - Chosen seat
///    bGetRotation - IF true gets the heading. False returns the reset position
FUNC VECTOR GET_IE_WH_SEAT_ACT_CLEANUP_PLAYER_POS(INT iSeatSlot, BOOL bGetRotation)
	VECTOR vReturn
	IF bGetRotation
		SWITCH iSeatSlot
			CASE 0
				//Sofa seat 1
				vReturn = <<0.0, 0.0, 270.0>>
			BREAK
			CASE 1
				//Sofa seat 2
				vReturn = <<0.0, 0.0, 270.0>>
			BREAK
			CASE 2
				//Opposite desk
				vReturn = <<0.0, 0.0, 180.0>>
			BREAK
		ENDSWITCH
	ELSE
		SWITCH iSeatSlot
			CASE 0
				//Sofa seat 1
				vReturn = <<959.1672, -3001.8235, -40.6349>>
			BREAK
			CASE 1
				//Sofa seat 2
				vReturn = <<959.0532, -3000.6353, -40.6349>>
			BREAK
			CASE 2
				//Opposite desk
				vReturn = <<962.4810, -3003.0217, -40.6349>>
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN vReturn
ENDFUNC

PROC OFFICE_SEAT_ACTIVITY_CLEAN_UP_GARAGE(BOOL bInteriorSwitchCleanup = FALSE )
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_ACTIVITY_CLEAN_UP")
	g_bSecuroQuickExitOfficeChair = FALSE
	playerBD[NATIVE_TO_INT(PLAYER_ID())].iOfficeSeatArmchairID = -1
	
	IF IS_BROWSER_OPEN()
		CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_ACTIVITY_CLEAN_UP: Closing web broswer")
		CLOSE_WEB_BROWSER()
	ELSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_ACTIVITY_CLEAN_UP: web broswer not open")
	ENDIF
	
	IF IS_OFFICE_SEAT_EXIT_PROMPT_SHOWN()
	OR IS_OFFICE_SEAT_COMPUTER_EXIT_PROMPT_SHOWN()
	OR IS_OFFICE_ARMCHAIR_EXIT_PROMPT_SHOWN()
		CLEAR_HELP()
		
		IF iOfficeSeatContextIntention != NEW_CONTEXT_INTENTION
			RELEASE_CONTEXT_INTENTION(iOfficeSeatContextIntention)
		ENDIF
		
		CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_ACTIVITY_CLEAN_UP: Clearing help text")
	ENDIF
	
	IF bInteriorSwitchCleanup
		IF iPlayerChosenSeatSlot != -1
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				VECTOR vResetPos = GET_IE_WH_SEAT_ACT_CLEANUP_PLAYER_POS(iPlayerChosenSeatSlot, FALSE)
				VECTOR vResetRot = GET_IE_WH_SEAT_ACT_CLEANUP_PLAYER_POS(iPlayerChosenSeatSlot, TRUE)
				
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vResetPos)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), vResetRot.z)
			ENDIF
		ENDIF
		
		CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_ACTIVITY_CLEAN_UP: Clearing up for interior switch. Seat ID: ", iPlayerChosenSeatSlot)
	ENDIF
	
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA) 
	CLEAR_ALL_OFFICE_SEAT_PROMPTS_GARAGE()
	CLEAR_BIT(iOfficeSeatiBitSet, OFFICE_SEAT_BS_EXIT_HELP_TEXT_DISPLAYED)
	sOfficeSeatAnimDict = ""
	SET_PLAYER_HEALTH_RECHARGE_MAX_PERCENT(GET_PLAYER_INDEX(), 0.5)
	CLEAR_REQUEST_PROPERTY_ACTIVITY(playerBD[NATIVE_TO_INT(PLAYER_ID())].iActivityRequested)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "----------OFFICE_SEAT_ACTIVITY_CLEAN_UP----------")
	SET_OFFICE_SEAT_STAGE(g_OfficeArmChairState, OFFICE_SEAT_LOCATECHECK)
	iPlayerChosenSeatSlot = -1
	SET_PLAYER_USING_OFFICE_SEATID(-1)
	IF iOfficeSeatContextIntention != NEW_CONTEXT_INTENTION
		RELEASE_CONTEXT_INTENTION(iOfficeSeatContextIntention)
	ENDIF
	
	g_bSecuroQuickExitOfficeChair = FALSE

ENDPROC

PROC MAINTAIN_VEHICLE_WAREHOUSE_LAPTOP_SCREEN()
	
	SWITCH thisPERSONAL_MOD_GARAGE.WHLaptopData.eLaptopState
		CASE WH_LAPTOP_SCREEN_STATE_INIT
			
			REQUEST_STREAMED_TEXTURE_DICT("Prop_Screen_IE_Adhawk")
			IF HAS_STREAMED_TEXTURE_DICT_LOADED("Prop_Screen_IE_Adhawk")
				PRINTLN("MAINTAIN_VEHICLE_WAREHOUSE_LAPTOP_SCREEN - Changing to state: WH_LAPTOP_SCREEN_STATE_LINK_RT")
				thisPERSONAL_MOD_GARAGE.WHLaptopData.eLaptopState = WH_LAPTOP_SCREEN_STATE_LINK_RT
			ENDIF
			
		BREAK
		CASE WH_LAPTOP_SCREEN_STATE_LINK_RT
			IF NOT IS_NAMED_RENDERTARGET_REGISTERED("Prop_ImpExp_Lappy_01a")
				REGISTER_NAMED_RENDERTARGET("Prop_ImpExp_Lappy_01a")
				IF NOT IS_NAMED_RENDERTARGET_LINKED(INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_impexp_lappy_01a")))
					LINK_NAMED_RENDERTARGET(INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_impexp_lappy_01a")))
					IF thisPERSONAL_MOD_GARAGE.WHLaptopData.iRenderTargetID = -1
						thisPERSONAL_MOD_GARAGE.WHLaptopData.iRenderTargetID = GET_NAMED_RENDERTARGET_RENDER_ID("Prop_ImpExp_Lappy_01a")
					ENDIF
				ENDIF
			ENDIF
			
			PRINTLN("MAINTAIN_VEHICLE_WAREHOUSE_LAPTOP_SCREEN - Changing to state: WH_LAPTOP_SCREEN_STATE_UPDATE_SCALEFORM - iRenderTargetID: ", thisPERSONAL_MOD_GARAGE.WHLaptopData.iRenderTargetID)
			thisPERSONAL_MOD_GARAGE.WHLaptopData.eLaptopState = WH_LAPTOP_SCREEN_STATE_UPDATE_SCALEFORM
		BREAK
		CASE WH_LAPTOP_SCREEN_STATE_UPDATE_SCALEFORM
			SET_TEXT_RENDER_ID(thisPERSONAL_MOD_GARAGE.WHLaptopData.iRenderTargetID)
			SET_SCRIPT_GFX_ALIGN(UI_ALIGN_IGNORE, UI_ALIGN_IGNORE)
			SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
			SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
			
			DRAW_SPRITE_NAMED_RENDERTARGET("Prop_Screen_IE_Adhawk", "Prop_Screen_IE_Adhawk", 0.5, 0.5, 1.0, 1.0, 0.0, 255, 255, 255, 255)
			
			RESET_SCRIPT_GFX_ALIGN()
			SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
		BREAK
	ENDSWITCH
	
ENDPROC

PROC CLEANUP_WH_LAPTOP_SCREEN()
	IF IS_NAMED_RENDERTARGET_REGISTERED("Prop_ImpExp_Lappy_01a")   
		RELEASE_NAMED_RENDERTARGET("Prop_ImpExp_Lappy_01a")
	ENDIF
	thisPERSONAL_MOD_GARAGE.WHLaptopData.iRenderTargetID = -1
	thisPERSONAL_MOD_GARAGE.WHLaptopData.eLaptopState = WH_LAPTOP_SCREEN_STATE_INIT
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("Prop_Screen_IE_Adhawk")
ENDPROC

PROC SCRIPT_CLEANUP()
	
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_IE_WAREHOUSE - SCRIPT_CLEANUP")
	#ENDIF
	
	CLEAN_UP_PERSONAL_CAR_MOD()
	
	CLEANUP_MP_RADIO(playerBD[NATIVE_TO_INT(PLAYER_ID())].MPRadioClient, MPRadioLocal)
	
	IF NETWORK_GET_THIS_SCRIPT_IS_NETWORK_SCRIPT()
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			CLEANUP_SERVER_RADIO(serverBD.MPRadioServer, InteriorPropStruct)
		ENDIF
	ENDIF
	
	OFFICE_SEAT_ACTIVITY_CLEAN_UP_GARAGE()
	
	#IF FEATURE_DLC_1_2022
	CLEANUP_SIMPLE_INTERIOR_PROP_WITH_SCRIPT(sVendingMachine)
	#ENDIF
	
	CLEANUP_ALL_IE_VEHICLES()
	
	CLEANUP_SIMPLE_INTERIOR_ENTRY_ANIM(thisPERSONAL_MOD_GARAGE.garageExitAnim)
	
	CLEANUP_WH_LAPTOP_SCREEN()
	
	//SET_PLAYER_JUST_DELIVERED_IMPORT_VEHICLE_TO_THEIR_GARAGE(PLAYER_ID(), FALSE)
	
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_IN_A_GARAGE)
	
	IF DOES_BLIP_EXIST(bLaptopBlip)
		REMOVE_BLIP(bLaptopBlip)
	ENDIF
	
	SET_PLAYER_START_IE_SELL_MISSION_WITHOUT_VEHICLE(PLAYER_ID(),FALSE)
	
	SET_SVM_VEHICLE_FLAGS()
	
	SIMPLE_INTERIOR_CLEAR_ENTERING_WITH_VEHICLE()
	g_bFinishedCreatingIEVehicles = FALSE
	PRINTLN("AM_MP_IE_WAREHOUSE - SCRIPT_CLEANUP SIMPLE_INTERIOR_CLEAR_ENTERING_WITH_VEHICLE()")
	
	g_iTotalIEVehCreated = 0
	
	IF IS_INTERACTION_MENU_DISABLED()
		CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SCRIPT_CLEANUP: interaction menu was disabled so enabling")
		ENABLE_INTERACTION_MENU()
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_NotAllowedToJackAnyPlayers, FALSE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PreventAutoShuffleToDriversSeat, FALSE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PreventAutoShuffleToTurretSeat, FALSE)
	ENDIF
	
	IF thisPERSONAL_MOD_GARAGE.pOwner = PLAYER_ID()
		RESET_SVM_EXITING_FLAG()
		
		IF thisPERSONAL_MOD_GARAGE.bDrivingOut
			IF CURRENT_SAVED_VEHICLE_SLOT() >= 0
			AND CURRENT_SAVED_VEHICLE_SLOT() < MAX_MP_SAVED_VEHICLES
				PRINTLN("AM_MP_IE_WAREHOUSE - SCRIPT_CLEANUP - Updating iVehicleBS for save slot: ", CURRENT_SAVED_VEHICLE_SLOT())
				
				SET_MP_INT_CHARACTER_STAT(GET_INT_STAT_FOR_VEHICLE_SLOT(MPSV_STAT_ELEMENT_VEHICLE_BS, CURRENT_SAVED_VEHICLE_SLOT()), g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS)
			ENDIF
		ENDIF
	ENDIF
	
	IF activitySeatStruct.iOfficeSeatContextIntention != NEW_CONTEXT_INTENTION
		CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SCRIPT_CLEANUP: clearing context intention")
		RELEASE_CONTEXT_INTENTION(activitySeatStruct.iOfficeSeatContextIntention)
	ENDIF
	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡ DEBUG STUFF  ╞═════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD

PROC CREATE_DEBUG_WIDGETS()
	TEXT_LABEL_63 str = "AM_MP_IE_WAREHOUSE"
	
	START_WIDGET_GROUP(str)
		ADD_WIDGET_BOOL("Reset cutscene", debugData.bResetCutscene)
		ADD_WIDGET_BOOL("Start spawn activity", debugData.bStartSpawnActivity)
		
		ADD_WIDGET_BOOL("Stats test", debugData.bStatsTest)
		ADD_WIDGET_BOOL("Give Special Items", debugData.bGiveSpecial)
		ADD_WIDGET_BOOL("Print Server BD Crates", debugData.bPrintServerBDCrates)
		
		START_WIDGET_GROUP("Interior Vehicles")
			ADD_WIDGET_INT_SLIDER("Vehicle ID", debugData.iVehicleID, 1, 96, 1)
			ADD_WIDGET_INT_SLIDER("Vehicle Slot", debugData.iVehicleSlot, 0, 39, 1)
			ADD_WIDGET_INT_READ_ONLY("Vehicle Spawn Flow", debugData.iVehicleFlow)
			ADD_WIDGET_BOOL("Spawn Vehicle", debugData.bSpawnVehicle)
			ADD_WIDGET_BOOL("Delete Vehicle", debugData.bDeleteVehicle)
			
			START_WIDGET_GROUP("Display Vehicle Placement")
				ADD_WIDGET_BOOL("Enable Widget", debugData.bEnableWidgets)
				START_WIDGET_GROUP("Coords")
					
					INT iVehicleSlot
					REPEAT ciMAX_IE_DISPLAY_VEHICLES iVehicleSlot
						
						TEXT_LABEL_63 tlDisplayVehName
						
						tlDisplayVehName = "Vehicle Slot " 
						tlDisplayVehName += iVehicleSlot
						
						START_WIDGET_GROUP(tlDisplayVehName)
							ADD_WIDGET_VECTOR_SLIDER("Coords", debugData.vDisplayVehicleCoords[iVehicleSlot], -10000.0, 10000.0, 0.01)
						STOP_WIDGET_GROUP()
						
					ENDREPEAT
					
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("Rotation")
				
					REPEAT ciMAX_IE_DISPLAY_VEHICLES iVehicleSlot
						
						TEXT_LABEL_63 tlDisplayVehName
						
						tlDisplayVehName = "Vehicle Slot " 
						tlDisplayVehName += iVehicleSlot
						
						START_WIDGET_GROUP(tlDisplayVehName)
							ADD_WIDGET_VECTOR_SLIDER("Rot", debugData.vDisplayVehicleRotation[iVehicleSlot], -10000.0, 10000.0, 0.01)
						STOP_WIDGET_GROUP()
						
					ENDREPEAT
				
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Owned Vehicle Placement")
				ADD_WIDGET_BOOL("Enable Widget", debugData.bEnableWidgets)
				START_WIDGET_GROUP("Coords")
					
					INT iVehSlot
					REPEAT ciMAX_IE_OWNED_VEHICLES iVehSlot
						
						TEXT_LABEL_63 tlOwnedVehName
						
						tlOwnedVehName = "Vehicle Slot " 
						tlOwnedVehName += iVehSlot
						
						START_WIDGET_GROUP(tlOwnedVehName)
							ADD_WIDGET_VECTOR_SLIDER("Coords", debugData.vOwnedVehicleCoords[iVehSlot], -10000.0, 10000.0, 0.01)
						STOP_WIDGET_GROUP()
						
					ENDREPEAT
					
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("Rotation")
				
					REPEAT ciMAX_IE_OWNED_VEHICLES iVehSlot
						
						TEXT_LABEL_63 tlOwnedVehName
						
						tlOwnedVehName = "Vehicle Slot " 
						tlOwnedVehName += iVehSlot
						
						START_WIDGET_GROUP(tlOwnedVehName)
							ADD_WIDGET_VECTOR_SLIDER("Rot", debugData.vOwnedVehicleRotation[iVehSlot], -10000.0, 10000.0, 0.01)
						STOP_WIDGET_GROUP()
						
					ENDREPEAT
				
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
			
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC

PROC INITIALISE_DEBUG_WIDGETS()
	
	INT iVehicleSlot
	REPEAT ciMAX_IE_DISPLAY_VEHICLES iVehicleSlot
		debugData.vDisplayVehicleCoords[iVehicleSlot]   = IE_GET_INTERIOR_GARAGE_VEHICLE_COORDS(iVehicleSlot)
		debugData.vDisplayVehicleRotation[iVehicleSlot] = IE_GET_INTERIOR_GARAGE_VEHICLE_ROTATION(iVehicleSlot)
	ENDREPEAT
	
	REPEAT ciMAX_IE_OWNED_VEHICLES iVehicleSlot
		debugData.vOwnedVehicleCoords[iVehicleSlot]   = IE_GET_INTERIOR_GARAGE_VEHICLE_COORDS(iVehicleSlot)
		debugData.vOwnedVehicleRotation[iVehicleSlot] = IE_GET_INTERIOR_GARAGE_VEHICLE_ROTATION(iVehicleSlot)
	ENDREPEAT
	
ENDPROC

PROC UPDATE_DEBUG_WIDGETS()
	
	IF debugData.bResetCutscene
		thisPERSONAL_MOD_GARAGE.iIntroCutsceneProg = 0
		thisPERSONAL_MOD_GARAGE.bShouldPlayIntroCut = FALSE
		INT iStatInt = g_MP_STAT_FM_CUT_DONE[GET_SLOT_NUMBER(-1)]
		CLEAR_BIT(iStatInt, biFmCut_IE_IntroCut)
		SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_FM_CUT_DONE, iStatInt)
		debugData.bResetCutscene = FALSE
	ENDIF
	
	IF debugData.bSpawnVehicle
		SWITCH debugData.iVehicleFlow
			CASE 0
				IF NOT IS_ANY_PLAYER_BLOCKING_IE_VEHICLE_SPAWN_COORDS(debugData.iVehicleSlot)
					IF CREATE_IE_VEHICLE(INT_TO_ENUM(IE_VEHICLE_ENUM, debugData.iVehicleID), debugData.iVehicleSlot)
						debugData.iVehicleFlow = 1
					ENDIF
				ENDIF
			BREAK
			CASE 1
				PERFORM_IE_VEHICLE_FADE(debugData.iVehicleSlot, TRUE)
				IF thisPERSONAL_MOD_GARAGE.vehData.iVehicleFadeComplete[debugData.iVehicleSlot] = 1
					debugData.bSpawnVehicle = FALSE
					debugData.iVehicleFlow = 0
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	IF debugData.bDeleteVehicle
		PERFORM_IE_VEHICLE_FADE(debugData.iVehicleSlot, FALSE)
		IF thisPERSONAL_MOD_GARAGE.vehData.iVehicleFadeComplete[debugData.iVehicleSlot] = 0
			debugData.bDeleteVehicle = FALSE
		ENDIF
	ENDIF
	
	IF debugData.bEnableWidgets
		
		INT iVehicleSlot
		REPEAT ciMAX_IE_DISPLAY_VEHICLES iVehicleSlot
			SET_ENTITY_COORDS(thisPERSONAL_MOD_GARAGE.vehData.oVehicleObjects[iVehicleSlot], debugData.vDisplayVehicleCoords[iVehicleSlot], FALSE)
			SET_ENTITY_ROTATION(thisPERSONAL_MOD_GARAGE.vehData.oVehicleObjects[iVehicleSlot], debugData.vDisplayVehicleRotation[iVehicleSlot])
		ENDREPEAT
		
		REPEAT ciMAX_IE_OWNED_VEHICLES iVehicleSlot
			SET_ENTITY_COORDS(thisPERSONAL_MOD_GARAGE.vehData.oCoverVehicles[iVehicleSlot], debugData.vOwnedVehicleCoords[iVehicleSlot], FALSE)
			SET_ENTITY_ROTATION(thisPERSONAL_MOD_GARAGE.vehData.oCoverVehicles[iVehicleSlot], debugData.vOwnedVehicleRotation[iVehicleSlot])
		ENDREPEAT
		
	ENDIF
	
ENDPROC
#ENDIF

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ INITIALISATION  ╞════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC INITIALISE()
	g_bPlayerLeavingCurrentInteriorInVeh = FALSE
	
	// Get simple interior details
	GET_SIMPLE_INTERIOR_DETAILS(thisPERSONAL_MOD_GARAGE.eSimpleInteriorID, thisPERSONAL_MOD_GARAGE.interiorDetails)
	
	INITIALISE_PERSONAL_MOD_GARAGE_GARAGE_EXIT()
	
	INITIALISE_INTERIOR_BED_SPAWN_ACTIVITIES(GET_SIMPLE_INTERIOR_TYPE(thisPERSONAL_MOD_GARAGE.eSimpleInteriorID))
	
	HOST_INITIALISE_IE_WAREHOUSE_VEHICLES()
	
	RESERVE_NETWORK_MISSION_VEHICLES(ciMAX_IE_DISPLAY_VEHICLES)
	
	thisPERSONAL_MOD_GARAGE.interiorUnderground = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<969.5376, -3000.4111, -48.6469>>, "imp_impexp_int_02")
	
	IF IS_VALID_INTERIOR(thisPERSONAL_MOD_GARAGE.interiorUnderground)
		PIN_INTERIOR_IN_MEMORY_VALIDATE(thisPERSONAL_MOD_GARAGE.interiorUnderground)
	ELSE
		SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
		SCRIPT_CLEANUP()
	ENDIF
	
	SET_PERSONAL_VEHICLE_COORDS()
	
	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_IN_A_GARAGE)
	
	IF SIMPLE_INTERIOR_IS_LOCAL_PLAYER_ENTERING_WITH_VEHICLE()
		thisPERSONAL_MOD_GARAGE.iEntranceVehicleIndex = GET_SVM_VEHICLE_INDEX(SIMPLE_INTERIOR_GET_ENTRY_VEH_MODEL())
	ENDIF
	
	SET_MAIN_ENTITY_SETS() // Not actually needed for script init as we do this in the simple interior script
	SET_PIPE_ENTITY_SETS()
	SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_NotAllowedToJackAnyPlayers, TRUE)
	ENDIF
	
	IF thisPERSONAL_MOD_GARAGE.pOwner = PLAYER_ID()
		RESET_SVM_EXITING_FLAG()
	ENDIF
	
	#IF FEATURE_DLC_1_2022
	INITIALISE_SIMPLE_INTERIOR_PROP_WITH_SCRIPT_DATA(sVendingMachine, <<952.5567, -2999.308, -39.70513>>, 90.0, PROP_VEND_SODA_01)
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		CREATE_DEBUG_WIDGETS()
		INITIALISE_DEBUG_WIDGETS()
		CDEBUG1LN(DEBUG_PROPERTY, "[IE_WAREHOUSE_SCRIPT] INITIALISE - Done.")
	#ENDIF
	
	START_NET_TIMER(getOutOfWayTimer)
ENDPROC

PROC SCRIPT_INITIALISE(SIMPLE_INTERIOR_CHILD_SCRIPT_DATA& scriptData)
	
	thisPERSONAL_MOD_GARAGE.eSimpleInteriorID = scriptData.eSimpleInteriorID
	thisPERSONAL_MOD_GARAGE.iScriptInstance = scriptData.iScriptInstance
	thisPERSONAL_MOD_GARAGE.iInvitingPlayer = scriptData.iInvitingPlayer
	thisPERSONAL_MOD_GARAGE.eID = GET_IMPORT_EXPORT_GARAGE_ID_FROM_SIMPLE_INTERIOR_ID(scriptData.eSimpleInteriorID)
	thisPERSONAL_MOD_GARAGE.pOwner = GET_IE_GARAGE_OWNER(thisPERSONAL_MOD_GARAGE.eID)
	
	InteriorPropStruct.iIndex = -1 // reseting property struct so the radio doesn't think the player is still in the last apartment they were in.
	MPRadioLocal.piApartmentOwner = thisPERSONAL_MOD_GARAGE.pOwner
	
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_PROPERTY, "[IE_WAREHOUSE_SCRIPT] SCRIPT_INITIALISE - Launching instance ", thisPERSONAL_MOD_GARAGE.iScriptInstance)
	#ENDIF
	
	IF IS_BIT_SET(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_SPAWNING_IN_SIMPLE_INTERIOR)
		IF thisPERSONAL_MOD_GARAGE.pOwner = INVALID_PLAYER_INDEX()
			IMPORT_EXPORT_GARAGES eOwnedWarehouseFromSlot = INT_TO_ENUM(IMPORT_EXPORT_GARAGES, GET_MP_INT_CHARACTER_STAT(MP_STAT_OWNED_IE_WAREHOUSE))
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_WAREHOUSE_SCRIPT] SCRIPT_INITIALISE - Spawning inside IE warehouse but owner is invalid... Stat says we own warehouse ", eOwnedWarehouseFromSlot, ", the script is starting for ", GET_IMPORT_EXPORT_GARAGE_ID_FROM_SIMPLE_INTERIOR_ID(thisPERSONAL_MOD_GARAGE.eSimpleInteriorID))
			#ENDIF
			
			IF eOwnedWarehouseFromSlot = GET_IMPORT_EXPORT_GARAGE_ID_FROM_SIMPLE_INTERIOR_ID(thisPERSONAL_MOD_GARAGE.eSimpleInteriorID)
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_WAREHOUSE_SCRIPT] SCRIPT_INITIALISE - We are spawning in IE warehouse that we own so we are the owner.")
				#ENDIF
				thisPERSONAL_MOD_GARAGE.pOwner = PLAYER_ID()
			ENDIF
		ENDIF
		
		IF thisPERSONAL_MOD_GARAGE.pOwner = PLAYER_ID()
			IF NOT HAS_LOCAL_PLAYER_VIEWED_IE_WAREHOUSE_INTRO_CUTSCENE()
				SET_BIT(thisPERSONAL_MOD_GARAGE.iBS, BS_IE_WAREHOUSE_RESTORE_VISIBILITY_AT_END_OF_CUTSCENE)
			ENDIF
		ENDIF
	ENDIF
	
	IF thisPERSONAL_MOD_GARAGE.pOwner = INVALID_PLAYER_INDEX()
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "[IE_WAREHOUSE_SCRIPT] SCRIPT_INITIALISE - Owner of this PERSONAL_MOD_GARAGE is invalid, exiting...")
		#ENDIF
		
		SET_SIMPLE_INTERIOR_CHILD_SCRIPT_READY(TRUE)
		SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
		SCRIPT_CLEANUP()
	ENDIF
	
	IF IS_BIT_SET(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_SPAWNING_IN_SIMPLE_INTERIOR)
		IF thisPERSONAL_MOD_GARAGE.pOwner = INVALID_PLAYER_INDEX()
		
			IMPORT_EXPORT_GARAGES eWarehouseID = GET_PLAYERS_OWNED_IE_GARAGE(PLAYER_ID())
			
			PRINTLN("AM_MP_IE_WAREHOUSE - SCRIPT_INITIALISE - Spawning inside IE Warehouse but owner is invalid. Stat says we own IE Warehouse ", eWarehouseID, ", the script is starting for ", GET_IMPORT_EXPORT_GARAGE_ID_FROM_SIMPLE_INTERIOR_ID(thisPERSONAL_MOD_GARAGE.eSimpleInteriorID))
			
			IF eWarehouseID = GET_IMPORT_EXPORT_GARAGE_ID_FROM_SIMPLE_INTERIOR_ID(thisPERSONAL_MOD_GARAGE.eSimpleInteriorID)
				PRINTLN("AM_MP_IE_WAREHOUSE - SCRIPT_INITIALISE - We are spawning in the base that we own so we are the owner.")
				thisPERSONAL_MOD_GARAGE.pOwner = PLAYER_ID()
			ENDIF
		ENDIF
	ENDIF
	
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, thisPERSONAL_MOD_GARAGE.iScriptInstance)
	
	// This makes sure the net script is active, waits untill it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		PRINTLN("AM_MP_IE_WAREHOUSE - FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		// This script will not be paused if another script calls PAUSE_GAME
		PRINTLN("AM_MP_IE_WAREHOUSE - INITIALISED")
	ELSE
		PRINTLN("AM_MP_IE_WAREHOUSE - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	STORE_IE_GARAGE_WAREHOUSE_SEAT_POSITIONS(activitySeatStruct)
	INITIALISE()
	
ENDPROC

FUNC BOOL MAINTAIN_LAUNCHING_CARMOD_SCRIPT()
		//Make sure the car mod script is not started too early after being killed
	IF IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
		//Make sure the car mod script cleanup flag is false
		IF g_bCleanUpCarmodShop = TRUE
			IF NOT HAS_NET_TIMER_STARTED(thisPERSONAL_MOD_GARAGE.sCarModScriptRunTimer)
				START_NET_TIMER(thisPERSONAL_MOD_GARAGE.sCarModScriptRunTimer)
			ELSE
				IF HAS_NET_TIMER_EXPIRED(thisPERSONAL_MOD_GARAGE.sCarModScriptRunTimer, 8000)
					g_bCleanUpCarmodShop = FALSE
					RESET_NET_TIMER(thisPERSONAL_MOD_GARAGE.sCarModScriptRunTimer)
					#IF IS_DEBUG_BUILD
					PRINTLN("MAINTAIN_LAUNCHING_CARMOD_SCRIPT - Setting g_bCleanUpCarmodShop To FALSE")
					#ENDIF
				ENDIF	
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(thisPERSONAL_MOD_GARAGE.iLocalBS,PERSONAL_MOD_GARAGE_BS_IS_CAR_MOD_SCRIPT_READY)
			thisPERSONAL_MOD_GARAGE.sChildOfChildScript = "carmod_shop"
			REQUEST_SCRIPT(thisPERSONAL_MOD_GARAGE.sChildOfChildScript)
			IF HAS_SCRIPT_LOADED(thisPERSONAL_MOD_GARAGE.sChildOfChildScript)
			AND NOT IS_THREAD_ACTIVE(thisPERSONAL_MOD_GARAGE.CarModThread)
			AND !g_bCleanUpCarmodShop
				
				g_iCarModInstance = thisPERSONAL_MOD_GARAGE.iScriptInstance  + PERSONAL_CAR_MOD_IE_WAREHOUSE_SCRIPT_INSTANCE_OFFSET  
				
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(thisPERSONAL_MOD_GARAGE.sChildOfChildScript)) < 1
					IF NOT NETWORK_IS_SCRIPT_ACTIVE(thisPERSONAL_MOD_GARAGE.sChildOfChildScript, g_iCarModInstance, TRUE)
						SHOP_LAUNCHER_STRUCT sShopLauncherData
						sShopLauncherData.bLinkedShop = FALSE
						sShopLauncherData.iNetInstanceID = g_iCarModInstance
						sShopLauncherData.eShop = CARMOD_SHOP_PERSONALMOD
						
						sShopLauncherData.ePersonalCarModVariation = PERSONAL_CAR_MOD_VARIATION_IE_LARGE //large garage
						
						g_iPersonalCarModVariation = ENUM_TO_INT(sShopLauncherData.ePersonalCarModVariation)
						thisPERSONAL_MOD_GARAGE.CarModThread = START_NEW_SCRIPT_WITH_ARGS(thisPERSONAL_MOD_GARAGE.sChildOfChildScript, sShopLauncherData, SIZE_OF(sShopLauncherData), CAR_MOD_SHOP_STACK_SIZE)
						SET_SCRIPT_AS_NO_LONGER_NEEDED(thisPERSONAL_MOD_GARAGE.sChildOfChildScript)
						SET_BIT(thisPERSONAL_MOD_GARAGE.iLocalBS,PERSONAL_MOD_GARAGE_BS_IS_CAR_MOD_SCRIPT_READY)
						g_sShopSettings.bShopScriptLaunched[ENUM_TO_INT(CARMOD_SHOP_PERSONALMOD)] = TRUE
						g_sShopSettings.bShopScriptLaunchedInMP[ENUM_TO_INT(CARMOD_SHOP_PERSONALMOD)] = NETWORK_IS_GAME_IN_PROGRESS()
						PRINTLN("MAINTAIN_LAUNCHING_CARMOD_SCRIPT - TRUE sShopLauncherData.ePersonalCarModVariation: ", sShopLauncherData.ePersonalCarModVariation)
						RETURN TRUE
					ELSE
						#IF IS_DEBUG_BUILD
							PRINTLN("MAINTAIN_LAUNCHING_CARMOD_SCRIPT: Unable to start shop script for personal_car_mod_shop - last instance still active")
						#ENDIF					
					ENDIF
				ELSE
					PRINTLN("MAINTAIN_LAUNCHING_CARMOD_SCRIPT - carmod is already running")
					RETURN TRUE 
				ENDIF
			ENDIF
		ELSE
//			#IF IS_DEBUG_BUILD
//			PRINTLN("MAINTAIN_LAUNCHING_CARMOD_SCRIPT - PERSONAL_MOD_GARAGE_BS_IS_CAR_MOD_SCRIPT_READY is true - return true ")
//			#ENDIF
			RETURN TRUE
		ENDIF
	ENDIF	
	#IF IS_DEBUG_BUILD
		PRINTLN("MAINTAIN_LAUNCHING_CARMOD_SCRIPT - return false")
	#ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_VEHICLE_WAREHOUSE()
	IF GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) != ciVEHICLE_WAREHOUSE_ROOM_KEY
	AND GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) != ciVEHICLE_WAREHOUSE_OFFICE_ROOM_KEY
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_MP_RADIO_LOCAL()
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE)
		CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE)
	ENDIF
	IF NATIVE_TO_INT(PLAYER_ID()) != -1
		CLIENT_MAINTAIN_MP_RADIO(serverBD.MPRadioServer, playerBD[NATIVE_TO_INT(PLAYER_ID())].MPRadioClient, MPRadioLocal, InteriorPropStruct, playerBD[PARTICIPANT_ID_TO_INT()].iActivityRequested, serverBD.activityControl)	
	ENDIF
ENDPROC

PROC MAINTAIN_LAPTOP_BLIP()
	IF thisPERSONAL_MOD_GARAGE.pOwner = PLAYER_ID()
	AND IS_PLAYER_IN_VEHICLE_WAREHOUSE()
		IF NOT DOES_BLIP_EXIST(bLaptopBlip)
			bLaptopBlip = CREATE_BLIP_FOR_COORD(<<964.7871, -3004.5813, -40.6349>>)
			SET_BLIP_SPRITE(bLaptopBlip, RADAR_TRACE_LAPTOP)
			SET_BLIP_NAME_FROM_TEXT_FILE(bLaptopBlip, "OR_PC_BLIP")
			SET_BLIP_DISPLAY(bLaptopBlip, DISPLAY_RADAR_ONLY)
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(bLaptopBlip)
			REMOVE_BLIP(bLaptopBlip)
		ENDIF
	ENDIF
ENDPROC

PROC MANAGE_KICK_PLAYERS_OUT_FOR_SELL_MISSIONS()
	IF IS_PLAYER_START_IE_SELL_MISSION_WITHOUT_VEHICLE()
		IF GB_GET_LOCAL_PLAYER_GANG_BOSS() != INVALID_PLAYER_INDEX()
			IF IS_PERSONAL_CAR_MOD_IS_READY_TO_START_MISSION(GB_GET_LOCAL_PLAYER_GANG_BOSS())
				TRIGGER_EXIT_FROM_SIMPLE_INTERIOR_NOW()
				#IF IS_DEBUG_BUILD
				PRINTLN("MANAGE_KICK_PLAYERS_OUT_FOR_SELL_MISSIONS - TRIGGER_EXIT_FROM_SIMPLE_INTERIOR_NOW")
				#ENDIF
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

PROC MAINTAIN_UNDERGROUND_GARAGE_BLIP()	
	IF IS_PLAYER_IN_VEHICLE_WAREHOUSE()
		IF NOT DOES_BLIP_EXIST(bGarageBlip)
		AND thisPERSONAL_MOD_GARAGE.pOwner = PLAYER_ID()
			bGarageBlip = CREATE_BLIP_FOR_COORD(<<1006.8627, -2997.7349, -39.2055>>)
			SET_BLIP_SPRITE(bGarageBlip, RADAR_TRACE_GARAGE)
			SET_BLIP_NAME_FROM_TEXT_FILE(bGarageBlip, "IE_UNGRD_GARAGE")
			SET_BLIP_DISPLAY(bGarageBlip, DISPLAY_RADAR_ONLY)
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(bGarageBlip)
			REMOVE_BLIP(bGarageBlip)
		ENDIF
	ENDIF
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ MAIN LOGIC PROC ╞════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC PERFORM_GARAGE_RENOVATION()
	SWITCH thisPERSONAL_MOD_GARAGE.iRenovationState
		CASE WAREHOUSE_RENOVATION_STATE_FADE_OUT
			IF NOT IS_SCREEN_FADED_OUT()
			AND NOT IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_OUT(500)
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				PRINTLN("PERFORM_GARAGE_RENOVATION - Starting renovation")
			ENDIF
			
			IF IS_SCREEN_FADED_OUT()
				PRINTLN("PERFORM_GARAGE_RENOVATION - Moving to stage WAREHOUSE_RENOVATION_STATE_CLEANUP_SITTING_ACTIVITIES")
				thisPERSONAL_MOD_GARAGE.iRenovationState = WAREHOUSE_RENOVATION_STATE_CLEANUP_SITTING_ACTIVITIES
			ENDIF
		BREAK
		
		CASE WAREHOUSE_RENOVATION_STATE_CLEANUP_SITTING_ACTIVITIES
			IF g_OfficeArmChairState != OFFICE_SEAT_LOCATECHECK
				OFFICE_SEAT_ACTIVITY_CLEAN_UP_GARAGE(TRUE)
			ENDIF
			
			PRINTLN("PERFORM_GARAGE_RENOVATION - Moving to stage WAREHOUSE_RENOVATION_STATE_MANAGE_ENTITY_SETS")
			thisPERSONAL_MOD_GARAGE.iRenovationState = WAREHOUSE_RENOVATION_STATE_MANAGE_ENTITY_SETS
		BREAK
		
		CASE WAREHOUSE_RENOVATION_STATE_MANAGE_ENTITY_SETS
			IF IS_VALID_INTERIOR(thisPERSONAL_MOD_GARAGE.interiorUnderground)
				SET_MAIN_ENTITY_SETS(TRUE)				
				PRINTLN("PERFORM_GARAGE_RENOVATION - Interior updated - Moving to stage WAREHOUSE_RENOVATION_STATE_FADE_IN")
			ENDIF
			
			START_NET_TIMER(thisPERSONAL_MOD_GARAGE.renovationTimer)
			
			thisPERSONAL_MOD_GARAGE.iRenovationState = WAREHOUSE_RENOVATION_STATE_FADE_IN
		BREAK
		
		CASE WAREHOUSE_RENOVATION_STATE_FADE_IN
			IF IS_INTERIOR_READY(thisPERSONAL_MOD_GARAGE.interiorUnderground)
				IF IS_SCREEN_FADED_OUT()
					IF HAS_NET_TIMER_EXPIRED(thisPERSONAL_MOD_GARAGE.renovationTimer, 2000)
						DO_SCREEN_FADE_IN(500)
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						
						IF thisPERSONAL_MOD_GARAGE.pOwner = PLAYER_ID()
							GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdIEgarageData.bApplyRenovation = FALSE
						ENDIF
						
						thisPERSONAL_MOD_GARAGE.bApplyRenovation = FALSE
						thisPERSONAL_MOD_GARAGE.iRenovationState = WAREHOUSE_RENOVATION_STATE_FADE_OUT
						
						PRINTLN("PERFORM_GARAGE_RENOVATION - Renovation finished cleaning up")
						RESET_NET_TIMER(thisPERSONAL_MOD_GARAGE.renovationTimer)
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL SHOULD_UPDATE_IE_WH_INTERIOR()
	IF thisPERSONAL_MOD_GARAGE.eActiveInterior != IE_INTERIOR_INVALID
	AND GlobalplayerBD_FM[NATIVE_TO_INT(thisPERSONAL_MOD_GARAGE.pOwner)].propertyDetails.bdIEgarageData.eWarehouseStyle != IE_INTERIOR_INVALID
	AND NOT IS_PAUSE_MENU_ACTIVE_EX()
	AND NOT IS_WARNING_MESSAGE_ACTIVE()
	AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
	AND IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
	AND GET_PLAYERS_OWNED_IE_GARAGE(thisPERSONAL_MOD_GARAGE.pOwner) = thisPERSONAL_MOD_GARAGE.eID
		RETURN thisPERSONAL_MOD_GARAGE.eActiveInterior != GlobalplayerBD_FM[NATIVE_TO_INT(thisPERSONAL_MOD_GARAGE.pOwner)].propertyDetails.bdIEgarageData.eWarehouseStyle
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF thisPERSONAL_MOD_GARAGE.eActiveInterior = IE_INTERIOR_INVALID
			PRINTLN("AM_MP_IE_WAREHOUSE - MAINTAIN_SVM_GARAGE_RENOVATION: Not running because current interior ID is invalid")
		ENDIF
		
		IF GlobalplayerBD_FM[NATIVE_TO_INT(thisPERSONAL_MOD_GARAGE.pOwner)].propertyDetails.bdIEgarageData.eWarehouseStyle = IE_INTERIOR_INVALID
			PRINTLN("AM_MP_IE_WAREHOUSE - MAINTAIN_SVM_GARAGE_RENOVATION: Not running because new interior ID is invalid")
		ENDIF
		
		IF IS_PAUSE_MENU_ACTIVE_EX()
			PRINTLN("AM_MP_IE_WAREHOUSE - MAINTAIN_SVM_GARAGE_RENOVATION: Not running because pause menu is active")
		ENDIF
		
		IF IS_WARNING_MESSAGE_ACTIVE()
			PRINTLN("AM_MP_IE_WAREHOUSE - MAINTAIN_SVM_GARAGE_RENOVATION: Not running because a warning message is active")
		ENDIF
		
		IF IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
			PRINTLN("AM_MP_IE_WAREHOUSE - MAINTAIN_SVM_GARAGE_RENOVATION: Not running because player is transitioning in/out the factory")
		ENDIF
		
		IF NOT IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
			PRINTLN("AM_MP_IE_WAREHOUSE - MAINTAIN_SVM_GARAGE_RENOVATION: Not running because player is not in the factory")
		ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_SVM_GARAGE_RENOVATION()
	IF SHOULD_UPDATE_IE_WH_INTERIOR()
		IF thisPERSONAL_MOD_GARAGE.bApplyRenovation != TRUE
			thisPERSONAL_MOD_GARAGE.bApplyRenovation = TRUE
		ENDIF
	ENDIF
	
	IF thisPERSONAL_MOD_GARAGE.bApplyRenovation
		PERFORM_GARAGE_RENOVATION()
	ENDIF
ENDPROC

FUNC INT GET_MY_BOSS_PARTICIPANT_ID()
	IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
		RETURN PARTICIPANT_ID_TO_INT()
	ENDIF
	
	
	PLAYER_INDEX playerBoss
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
		playerBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
		IF NETWORK_IS_PLAYER_ACTIVE(playerBoss)
			IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerBoss)
				RETURN (NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(playerBoss)))
			ENDIF
		ENDIF
	ENDIF
	
	RETURN -1
ENDFUNC

FUNC BOOL IS_MY_BOSS_WATCHING_IE_INTRO_CUTSCENE()
	INT iPart = GET_MY_BOSS_PARTICIPANT_ID()
	IF iPart > -1
		IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
			RETURN IS_BIT_SET(playerBD[iPart].iBS, PLAYER_BD_BS_RUNNING_INTRO_CUTSCENE)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_BOSS_SITTING_AT_COMPUTER_DESK()
	IF NOT DOES_ENTITY_EXIST(PLAYER_PED_ID())
	OR IS_ENTITY_DEAD(PLAYER_PED_ID())
	OR NOT GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
		RETURN FALSE
	ENDIF
	
	RETURN IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<964.457336,-3003.585693,-39.634876>>,<<1.000000,1.000000,1.000000>>)
ENDFUNC

PROC MAINTAIN_SHOULD_START_INTRO_CUSTCENE()
	IF IS_PERSONAL_MOD_GARAGE_STATE(PERSONAL_MOD_GARAGE_STATE_INTRO_CUT)
		EXIT
	ENDIF
	
	IF g_SpawnData.bSpawningInSimpleInterior
	OR IS_BIT_SET(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_SPAWNING_IN_SIMPLE_INTERIOR)
		EXIT
	ENDIF

	BOOL bShouldCheck
	BOOL bDoneIntroCut
	IF IS_LOCAL_PLAYER_WALKING_INTO_SIMPLE_INTERIOR()
		IF NOT HAS_SIMPLE_INTERIOR_CHILD_SCRIPT_TIMED_OUT()
		AND IS_PERSONAL_MOD_GARAGE_STATE(PERSONAL_MOD_GARAGE_STATE_LOADING)
			bShouldCheck = TRUE
		ENDIF
	ELIF IS_PERSONAL_MOD_GARAGE_STATE(PERSONAL_MOD_GARAGE_STATE_IDLE)	
		IF NOT IS_LOCAL_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR()
		AND NOT IS_BROWSER_OPEN()
		AND NOT IS_SCREEN_FADED_OUT()
		AND NOT IS_MOBILE_PHONE_CALL_ONGOING()
		AND NOT IS_BOSS_SITTING_AT_COMPUTER_DESK()
			IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
				bShouldCheck = (GET_FRAME_COUNT() % 60 = 0)
			ELSE
				IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE)
					bShouldCheck = IS_MY_BOSS_WATCHING_IE_INTRO_CUTSCENE()
				ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			IF IS_LOCAL_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR()
				PRINTLN("[IE_WAREHOUSE_SCRIPT] [MAINTAIN_SHOULD_START_INTRO_CUSTCENE] [RUN_IE_INTRO_CUTSCENE] Not checking as IS_LOCAL_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR()")
			ENDIF
			
			IF IS_BROWSER_OPEN()
				PRINTLN("[IE_WAREHOUSE_SCRIPT] [MAINTAIN_SHOULD_START_INTRO_CUSTCENE] [RUN_IE_INTRO_CUTSCENE] Not checking as IS_BROWSER_OPEN()")
			ENDIF
			
			IF IS_SCREEN_FADED_OUT()
				PRINTLN("[IE_WAREHOUSE_SCRIPT] [MAINTAIN_SHOULD_START_INTRO_CUSTCENE] [RUN_IE_INTRO_CUTSCENE] Not checking as IS_SCREEN_FADED_OUT()")
			ENDIF
			
			IF IS_MOBILE_PHONE_CALL_ONGOING()
				PRINTLN("[IE_WAREHOUSE_SCRIPT] [MAINTAIN_SHOULD_START_INTRO_CUSTCENE] [RUN_IE_INTRO_CUTSCENE] Not checking as IS_MOBILE_PHONE_CALL_ONGOING()")
			ENDIF
			
			IF IS_BOSS_SITTING_AT_COMPUTER_DESK()
				PRINTLN("[IE_WAREHOUSE_SCRIPT] [MAINTAIN_SHOULD_START_INTRO_CUSTCENE] [RUN_IE_INTRO_CUTSCENE] Not checking as IS_BOSS_SITTING_AT_COMPUTER_DESK()")
			ENDIF
			#ENDIF
		ENDIF
		
		IF IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
			// If player starts walking out cleanup the radio
			IF MPRadioLocal.eStage != MP_RADIO_CLIENT_STAGE_INIT
				CLEANUP_MP_RADIO(playerBD[NATIVE_TO_INT(PLAYER_ID())].MPRadioClient, MPRadioLocal)
				IF IS_INTERACTION_MENU_DISABLED()
					ENABLE_INTERACTION_MENU()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//PRINTLN("[IE_WAREHOUSE_SCRIPT] [MAINTAIN_SHOULD_START_INTRO_CUSTCENE] bShouldCheck = ", bShouldCheck)
	IF bShouldCheck
		IF thisPERSONAL_MOD_GARAGE.iIntroCutsceneProg = 0
			IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
			OR thisPERSONAL_MOD_GARAGE.pOwner = PLAYER_ID()
				bDoneIntroCut = HAS_LOCAL_PLAYER_VIEWED_IE_WAREHOUSE_INTRO_CUTSCENE()
				IF NOT bDoneIntroCut
					thisPERSONAL_MOD_GARAGE.bShouldPlayIntroCut = TRUE
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBS, PLAYER_BD_BS_RUNNING_INTRO_CUTSCENE)
					PRINTLN("[IE_WAREHOUSE_SCRIPT] [MAINTAIN_SHOULD_START_INTRO_CUSTCENE] [RUN_IE_INTRO_CUTSCENE] thisPERSONAL_MOD_GARAGE.bShouldPlayIntroCut = TRUE - I'm a boss / owner")
					
					IF IS_PERSONAL_MOD_GARAGE_STATE(PERSONAL_MOD_GARAGE_STATE_IDLE)
						IF NOT IS_BIT_SET(thisPERSONAL_MOD_GARAGE.iBS, BS_IE_WAREHOUSE_DATA_LAUNCH_CUT_IN_RUNNING_STATE)
							SET_BIT(thisPERSONAL_MOD_GARAGE.iBS, BS_IE_WAREHOUSE_DATA_LAUNCH_CUT_IN_RUNNING_STATE)
							PRINTLN("[IE_WAREHOUSE_SCRIPT] [MAINTAIN_SHOULD_START_INTRO_CUSTCENE] [RUN_IE_INTRO_CUTSCENE] BS_IE_WAREHOUSE_DATA_LAUNCH_CUT_IN_RUNNING_STATE - I'm a boss")
						ENDIF
					ENDIF
				ELSE
					PRINTLN("[IE_WAREHOUSE_SCRIPT] [MAINTAIN_SHOULD_START_INTRO_CUSTCENE] [RUN_IE_INTRO_CUTSCENE] we're the boss/owner and the cutscene has already been done.")
				ENDIF
					
			ELIF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE)
				IF IS_MY_BOSS_WATCHING_IE_INTRO_CUTSCENE()
					thisPERSONAL_MOD_GARAGE.bShouldPlayIntroCut = TRUE
					IF IS_PERSONAL_MOD_GARAGE_STATE(PERSONAL_MOD_GARAGE_STATE_IDLE)
						IF NOT IS_BIT_SET(thisPERSONAL_MOD_GARAGE.iBS, BS_IE_WAREHOUSE_DATA_LAUNCH_CUT_IN_RUNNING_STATE)
							SET_BIT(thisPERSONAL_MOD_GARAGE.iBS, BS_IE_WAREHOUSE_DATA_LAUNCH_CUT_IN_RUNNING_STATE)
							PRINTLN("[IE_WAREHOUSE_SCRIPT] [MAINTAIN_SHOULD_START_INTRO_CUSTCENE] [RUN_IE_INTRO_CUTSCENE] BS_IE_WAREHOUSE_DATA_LAUNCH_CUT_IN_RUNNING_STATE")
						ENDIF
					ENDIF
					PRINTLN("[IE_WAREHOUSE_SCRIPT] [MAINTAIN_SHOULD_START_INTRO_CUSTCENE] [RUN_IE_INTRO_CUTSCENE] thisPERSONAL_MOD_GARAGE.bShouldPlayIntroCut = TRUE - IS_MY_BOSS_WATCHING_IE_INTRO_CUTSCENE")
				ENDIF
			ENDIF
		ELSE
			IF thisPERSONAL_MOD_GARAGE.bShouldPlayIntroCut
				thisPERSONAL_MOD_GARAGE.bShouldPlayIntroCut = FALSE
			ENDIF
			
			#IF IS_DEBUG_BUILD
			IF bDoneIntroCut
				IF GET_FRAME_COUNT() % 100 = 0
					PRINTLN("[IE_WAREHOUSE_SCRIPT] [MAINTAIN_SHOULD_START_INTRO_CUSTCENE] [RUN_IE_INTRO_CUTSCENE] Already done cutscene")
				ENDIF
			ENDIF
			#ENDIF
		ENDIF
	ENDIF
ENDPROC



FUNC MODEL_NAMES GET_PA_MODEL_FOR_INTRO_CUT()
//	PLAYER_INDEX playerBoss
//	playerBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS() 
	MODEL_NAMES model
	IF IS_PLAYERS_OFFICE_PA_MALE(thisPERSONAL_MOD_GARAGE.pOwner)
		model = mp_m_execpa_01
	ELSE
		model = INT_TO_ENUM(MODEL_NAMES, HASH("MP_F_ExecPA_02"))
	ENDIF
	
//	model = INT_TO_ENUM(MODEL_NAMES, HASH("MP_F_ExecPA_02"))
	
	RETURN model
ENDFUNC

PROC REQUEST_OFFICE_PA_MODEL_FOR_INTRO_CUT()
	REQUEST_MODEL(GET_PA_MODEL_FOR_INTRO_CUT())
ENDPROC

FUNC BOOL HAS_OFFICE_PA_MODEL_LOADED_FOR_INTRO_CUT()
	RETURN HAS_MODEL_LOADED(GET_PA_MODEL_FOR_INTRO_CUT())
ENDFUNC



PROC CREATE_OFFICE_PA_FOR_INTRO_CUT()
	MODEL_NAMES mPA = GET_PA_MODEL_FOR_INTRO_CUT()
	
	IF mPA = mp_m_execpa_01
		PRINTLN("[IE_WAREHOUSE_SCRIPT] [RUN_IE_INTRO_CUTSCENE] [CREATE_OFFICE_PA_FOR_INTRO_CUT] Creating male ped...")
		thisPERSONAL_MOD_GARAGE.pedPAforCut = CREATE_PED(PEDTYPE_CIVMALE, mPA, <<977.2020, -2997.6809, -40.6470>>, 0.0, FALSE, FALSE)
	ELSE
		PRINTLN("[IE_WAREHOUSE_SCRIPT] [RUN_IE_INTRO_CUTSCENE] [CREATE_OFFICE_PA_FOR_INTRO_CUT] Creating female ped...")
		thisPERSONAL_MOD_GARAGE.pedPAforCut = CREATE_PED(PEDTYPE_CIVFEMALE, mPA, <<977.2020, -2997.6809, -40.6470>>, 0.0, FALSE, FALSE)
	ENDIF
	
	PLAYER_INDEX playerBoss = thisPERSONAL_MOD_GARAGE.pOwner // GB_GET_LOCAL_PLAYER_GANG_BOSS() 
	SETUP_OFFICE_PA_CLOTHES_VARIATION(thisPERSONAL_MOD_GARAGE.pedPAforCut, playerBoss)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(thisPERSONAL_MOD_GARAGE.pedPAforCut, TRUE)
	SET_ENTITY_INVINCIBLE(thisPERSONAL_MOD_GARAGE.pedPAforCut, TRUE)
	
ENDPROC

PROC CLEANUP_OFFICE_PA_FOR_INTRO_CUT()
	PRINTLN("[IE_WAREHOUSE_SCRIPT] [RUN_IE_INTRO_CUTSCENE] [CLEANUP_OFFICE_PA_FOR_INTRO_CUT] Called ")
	ENTITY_INDEX ent

	ent = thisPERSONAL_MOD_GARAGE.pedPAforCut
	IF DOES_ENTITY_EXIST(ent)
		DELETE_ENTITY(ent)
	ENDIF
	
	MODEL_NAMES mPA = GET_PA_MODEL_FOR_INTRO_CUT()
	SET_MODEL_AS_NO_LONGER_NEEDED(mPA)
	
ENDPROC

PROC SET_CUTSCENE_STREAMING_FLAGS_FOR_OFFICE_PA()
	MODEL_NAMES mPA = GET_PA_MODEL_FOR_INTRO_CUT()
	
	IF mPA = mp_m_execpa_01
		SET_CUTSCENE_ENTITY_STREAMING_FLAGS("Male_PA", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
	ELSE
		SET_CUTSCENE_ENTITY_STREAMING_FLAGS("Female_PA_02", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
	ENDIF
ENDPROC

PROC REGISTER_OFFICE_PA_FOR_CUTSCENE()
	MODEL_NAMES mPA = GET_PA_MODEL_FOR_INTRO_CUT()
	IF NOT IS_PED_INJURED(thisPERSONAL_MOD_GARAGE.pedPAforCut)
		IF mPA = mp_m_execpa_01
			REGISTER_ENTITY_FOR_CUTSCENE(thisPERSONAL_MOD_GARAGE.pedPAforCut, "Male_PA", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
		ELSE
			REGISTER_ENTITY_FOR_CUTSCENE(thisPERSONAL_MOD_GARAGE.pedPAforCut, "Female_PA_02", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
		ENDIF
	ELSE
		PRINTLN("[IE_WAREHOUSE_SCRIPT] [RUN_IE_INTRO_CUTSCENE] Not calling REGISTER_ENTITY_FOR_CUTSCENE as ped injured!")
	ENDIF
	
ENDPROC

PROC REQUEST_INTRO_CUTSCENE()
	MODEL_NAMES mPA = GET_PA_MODEL_FOR_INTRO_CUT()
	IF mPA = mp_m_execpa_01
		PRINTLN("[IE_WAREHOUSE_SCRIPT] [RUN_IE_INTRO_CUTSCENE] requesting cutscene IMPEXP_INT_L1")
		REQUEST_CUTSCENE("IMPEXP_INT_L1") 
	ELSE
		PRINTLN("[IE_WAREHOUSE_SCRIPT] [RUN_IE_INTRO_CUTSCENE] requesting cutscene IMPEXP_INT")
		REQUEST_CUTSCENE("IMPEXP_INT") 
	ENDIF
ENDPROC

FUNC BOOL RUN_IE_INTRO_CUTSCENE()
//	VECTOR vSpawnPhantom 	= vPVPositions[3]
//	VECTOR vSpawnBlazer 	= vPVPositions[7]
//	FLOAT fSpawnPhantom		= fPVHeadings[3]
//	FLOAT fSpawnBlazer		= fPVHeadings[7]
	
//	PLAYER_INDEX playerBoss
	
//	ENTITY_INDEX ent
	
	SWITCH thisPERSONAL_MOD_GARAGE.iIntroCutsceneProg
		CASE 0
			
		//	REQUEST_MODEL(PHANTOM2)
		//	REQUEST_MODEL(BLAZER5)
			REQUEST_OFFICE_PA_MODEL_FOR_INTRO_CUT()
			thisPERSONAL_MOD_GARAGE.iIntroCutsceneProg++
			PRINTLN("[IE_WAREHOUSE_SCRIPT] [RUN_IE_INTRO_CUTSCENE] thisPERSONAL_MOD_GARAGE.iIntroCutsceneProg = ", thisPERSONAL_MOD_GARAGE.iIntroCutsceneProg)
		BREAK
		
		CASE 1	
			IF HAS_OFFICE_PA_MODEL_LOADED_FOR_INTRO_CUT()
		//	AND HAS_MODEL_LOADED(BLAZER5)
		//	AND HAS_MODEL_LOADED(PHANTOM2)
				
				//IF CAN_SIMPLE_INTERIOR_CHILD_SCRIPT_TAKE_CONTROL()
				//	SET_ENTITY_VISIBLE_IN_CUTSCENE(thisPERSONAL_MOD_GARAGE.vehForCut[0], TRUE)
				//	SET_ENTITY_VISIBLE_IN_CUTSCENE(thisPERSONAL_MOD_GARAGE.vehForCut[1], TRUE)
					
					REQUEST_INTRO_CUTSCENE()
					
					CREATE_OFFICE_PA_FOR_INTRO_CUT()
					CREATE_MODEL_HIDE(<<961.2841, -2999.7090, -40.6349>>, 5.0, V_ILEV_ROC_DOOR2, TRUE)
					CREATE_MODEL_HIDE(<<967.4402, -3007.4153, -40.6470>>, 5.0, V_ILEV_ROC_DOOR3, TRUE)
					
					//REQUEST_CUTSCENE("IMPEXP_INT")
					thisPERSONAL_MOD_GARAGE.iIntroCutsceneProg++
					PRINTLN("[IE_WAREHOUSE_SCRIPT] [RUN_IE_INTRO_CUTSCENE] thisPERSONAL_MOD_GARAGE.iIntroCutsceneProg = ", thisPERSONAL_MOD_GARAGE.iIntroCutsceneProg)
				//ELSE
				//	PRINTLN("[IE_WAREHOUSE_SCRIPT] [RUN_IE_INTRO_CUTSCENE] Waiting for CAN_SIMPLE_INTERIOR_CHILD_SCRIPT_TAKE_CONTROL()")
				//ENDIF
			ENDIF
		BREAK
		CASE 2
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			//	SET_CUTSCENE_ENTITY_STREAMING_FLAGS("Female_PA", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
				SET_CUTSCENE_STREAMING_FLAGS_FOR_OFFICE_PA()
				thisPERSONAL_MOD_GARAGE.iIntroCutsceneProg++
				PRINTLN("[IE_WAREHOUSE_SCRIPT] [RUN_IE_INTRO_CUTSCENE] thisPERSONAL_MOD_GARAGE.iIntroCutsceneProg = ", thisPERSONAL_MOD_GARAGE.iIntroCutsceneProg)
			ENDIF
		BREAK
		
		CASE 3
			IF HAS_CUTSCENE_LOADED() 
				REGISTER_OFFICE_PA_FOR_CUTSCENE()
				CLEAR_IMP_EXP_CALL_BITS()
				START_CUTSCENE()
				MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(FALSE, TRUE, TRUE, FALSE )
				START_MP_CUTSCENE()
				
//				PRINTLN("[IE_WAREHOUSE_SCRIPT] [RUN_IE_INTRO_CUTSCENE] Creating PHANTOM2 at ", vSpawnPhantom)
//				thisPERSONAL_MOD_GARAGE.vehForCut[0] =  CREATE_VEHICLE(PHANTOM2, 	vSpawnPhantom, 	fSpawnPhantom, 	FALSE, FALSE)
//				
//				PRINTLN("[IE_WAREHOUSE_SCRIPT] [RUN_IE_INTRO_CUTSCENE] Creating BLAZER5 at ", vSpawnPhantom)
//				thisPERSONAL_MOD_GARAGE.vehForCut[1] =  CREATE_VEHICLE(BLAZER5, 	vSpawnBlazer, 	fSpawnBlazer, 	FALSE, FALSE)
//				
			//	SET_ENTITY_VISIBLE_IN_CUTSCENE(thisPERSONAL_MOD_GARAGE.vehForCut[0], TRUE)
			//	SET_ENTITY_VISIBLE_IN_CUTSCENE(thisPERSONAL_MOD_GARAGE.vehForCut[1], TRUE)
				HANG_UP_AND_PUT_AWAY_PHONE()
				
				IF g_OfficeArmChairState != OFFICE_SEAT_LOCATECHECK
					OFFICE_SEAT_ACTIVITY_CLEAN_UP_GARAGE(TRUE)
				ENDIF
				
				thisPERSONAL_MOD_GARAGE.iIntroCutsceneProg++
				PRINTLN("[IE_WAREHOUSE_SCRIPT] [RUN_IE_INTRO_CUTSCENE] iIntroCutsceneProg = ", thisPERSONAL_MOD_GARAGE.iIntroCutsceneProg)
				
			ELSE
				PRINTLN("[IE_WAREHOUSE_SCRIPT] [RUN_IE_INTRO_CUTSCENE] Waiting for HAS_CUTSCENE_LOADED()")
			ENDIF
		BREAK
		
		CASE 4
			IF IS_CUTSCENE_PLAYING()
				thisPERSONAL_MOD_GARAGE.iIntroCutsceneProg++
				IF IS_SCREEN_FADED_OUT()					
					DO_SCREEN_FADE_IN(500)
				ENDIF
				PRINTLN("[IE_WAREHOUSE_SCRIPT] [RUN_IE_INTRO_CUTSCENE] iIntroCutsceneProg = ", thisPERSONAL_MOD_GARAGE.iIntroCutsceneProg)
			ELSE
				PRINTLN("[IE_WAREHOUSE_SCRIPT] [RUN_IE_INTRO_CUTSCENE] Waiting for IS_CUTSCENE_PLAYING()")
			ENDIF
		BREAK
		
		CASE 5
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				//	IF NOT IS_ENTITY_VISIBLE(PLAYER_PED_ID())
				//		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
				//		PRINTLN("[IE_WAREHOUSE_SCRIPT] [RUN_IE_INTRO_CUTSCENE] SET_ENTITY_VISIBLE ")
						IF IS_BIT_SET(thisPERSONAL_MOD_GARAGE.iBS, BS_IE_WAREHOUSE_DATA_LAUNCH_CUT_IN_RUNNING_STATE)
							NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							PRINTLN("[IE_WAREHOUSE_SCRIPT] [RUN_IE_INTRO_CUTSCENE] NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE) ")
						ENDIF
			//		ENDIF
				ENDIF
			ENDIF
			
			IF HAS_CUTSCENE_FINISHED() 
				REMOVE_MODEL_HIDE(<<961.2841, -2999.7090, -40.6349>>, 5.0, V_ILEV_ROC_DOOR2)
				REMOVE_MODEL_HIDE(<<967.4402, -3007.4153, -40.6470>>, 5.0, V_ILEV_ROC_DOOR3)
//				ent = thisPERSONAL_MOD_GARAGE.vehForCut[0]
//				IF DOES_ENTITY_EXIST(ent)
//					DELETE_ENTITY(ent)
//				ENDIF
//				
//				ent = thisPERSONAL_MOD_GARAGE.vehForCut[1]
//				IF DOES_ENTITY_EXIST(ent)
//					DELETE_ENTITY(ent)
//				ENDIF
				
				CLEANUP_OFFICE_PA_FOR_INTRO_CUT()
				
				SET_MODEL_AS_NO_LONGER_NEEDED(PHANTOM2)
				SET_MODEL_AS_NO_LONGER_NEEDED(BLAZER5)
				
				CLEANUP_MP_CUTSCENE(DEFAULT, FALSE)
//				IF NOT IS_SCREEN_FADED_OUT()
//				AND NOT IS_SCREEN_FADING_OUT()
//					DO_SCREEN_FADE_OUT(500)
//				ENDIF

				#IF IS_DEBUG_BUILD
				IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_NoIeCutSave")
				#ENDIF
				
				IF thisPERSONAL_MOD_GARAGE.pOwner = PLAYER_ID() // Only mark as seen for the boss
					INT iStatInt
					iStatInt = g_MP_STAT_FM_CUT_DONE[GET_SLOT_NUMBER(-1)]
					SET_BIT(iStatInt, biFmCut_IE_IntroCut)
					SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_FM_CUT_DONE, iStatInt)
					PRINTLN("[IE_WAREHOUSE_SCRIPT] [RUN_IE_INTRO_CUTSCENE] Setting MP_STAT_FM_CUT_DONE on Boss.")
				ENDIF
				
				#IF IS_DEBUG_BUILD
				ENDIF
				#ENDIF
				
				CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBS, PLAYER_BD_BS_RUNNING_INTRO_CUTSCENE)
				thisPERSONAL_MOD_GARAGE.bShouldPlayIntroCut = FALSE
				thisPERSONAL_MOD_GARAGE.iIntroCutsceneProg++
				PRINTLN("[IE_WAREHOUSE_SCRIPT] [RUN_IE_INTRO_CUTSCENE] iIntroCutsceneProg = ", thisPERSONAL_MOD_GARAGE.iIntroCutsceneProg)
				RETURN TRUE
			ENDIF
			
			
		BREAK
		
	ENDSWITCH
		
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_COLLISION_GEOMETRY_CHECK()
	IF bPhantom2Created
		IF NOT IS_BIT_SET(serverBD.iBS, BS_IE_WAREHOUSE_SERVER_CREATE_COLLISION_GEOM_PHANTOM_FLAG)
			SET_BIT(serverBD.iBS, BS_IE_WAREHOUSE_SERVER_CREATE_COLLISION_GEOM_PHANTOM_FLAG)
		ENDIF
	ELSE
		IF IS_BIT_SET(serverBD.iBS, BS_IE_WAREHOUSE_SERVER_CREATE_COLLISION_GEOM_PHANTOM_FLAG)
			CLEAR_BIT(serverBD.iBS, BS_IE_WAREHOUSE_SERVER_CREATE_COLLISION_GEOM_PHANTOM_FLAG)
		ENDIF
	ENDIF
	
	IF bDune5Created
		IF NOT IS_BIT_SET(serverBD.iBS, BS_IE_WAREHOUSE_SERVER_CREATE_COLLISION_GEOM_DUNE_FLAG)
			SET_BIT(serverBD.iBS, BS_IE_WAREHOUSE_SERVER_CREATE_COLLISION_GEOM_DUNE_FLAG)
		ENDIF
	ELSE
		IF IS_BIT_SET(serverBD.iBS, BS_IE_WAREHOUSE_SERVER_CREATE_COLLISION_GEOM_DUNE_FLAG)
			CLEAR_BIT(serverBD.iBS, BS_IE_WAREHOUSE_SERVER_CREATE_COLLISION_GEOM_DUNE_FLAG)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL CREATE_PHANTOM_COLLISION_GEOMETRY()
	MODEL_NAMES model = PROP_BOX_AMMO05B
	
	VECTOR vPosition = <<954.847, -2999.9116, -46.5>>
	vector vRotation = <<0.0, 0.0, 0.0>>
	
	IF REQUEST_LOAD_MODEL(model)
		IF CAN_REGISTER_MISSION_OBJECTS(1)
		AND NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_IE_WAREHOUSE_CREATE_COLLISION_GEOM_PHANTOM)
			collisionGeomPhantom = CREATE_OBJECT(model, vPosition, FALSE, FALSE, TRUE)
			SET_ENTITY_ROTATION(collisionGeomPhantom, vRotation)
			SET_ENTITY_VISIBLE(collisionGeomPhantom, FALSE)
			FREEZE_ENTITY_POSITION(collisionGeomPhantom, TRUE)
			
			SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_IE_WAREHOUSE_CREATE_COLLISION_GEOM_PHANTOM)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(model)
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_DUNE_COLLISION_GEOMETRY()
	MODEL_NAMES model = EX_PROP_CRATE_AMMO_BC
	
	VECTOR vPosition = <<962.630, -2994.804, -47.0>>
	vector vRotation = <<0.0, 0.0, 0.0>>
	
	IF REQUEST_LOAD_MODEL(model)
		IF CAN_REGISTER_MISSION_OBJECTS(1)
		AND NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_IE_WAREHOUSE_CREATE_COLLISION_GEOM_DUNE)
			collisionGeomDune = CREATE_OBJECT(model, vPosition, FALSE, FALSE, TRUE)
			SET_ENTITY_ROTATION(collisionGeomDune, vRotation)
			SET_ENTITY_VISIBLE(collisionGeomDune, FALSE)
			FREEZE_ENTITY_POSITION(collisionGeomDune, TRUE)
			
			SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_IE_WAREHOUSE_CREATE_COLLISION_GEOM_DUNE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(model)
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEANUP_PHANTOM_COLLISION_GEOMETRY()
	IF IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_IE_WAREHOUSE_CREATE_COLLISION_GEOM_PHANTOM)
		DELETE_OBJECT(collisionGeomPhantom)
		CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_IE_WAREHOUSE_CREATE_COLLISION_GEOM_PHANTOM)
	ENDIF
ENDPROC

PROC CLEANUP_DUNE_COLLISION_GEOMETRY()
	IF IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_IE_WAREHOUSE_CREATE_COLLISION_GEOM_DUNE)
		DELETE_OBJECT(collisionGeomDune)
		CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_IE_WAREHOUSE_CREATE_COLLISION_GEOM_DUNE)
	ENDIF
ENDPROC

PROC MAINTAIN_COLLISION_GEOMETRY()
	IF IS_BIT_SET(serverBD.iBS, BS_IE_WAREHOUSE_SERVER_CREATE_COLLISION_GEOM_PHANTOM_FLAG)
		IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_IE_WAREHOUSE_CREATE_COLLISION_GEOM_PHANTOM)
			CREATE_PHANTOM_COLLISION_GEOMETRY()
		ENDIF
	ELSE
		IF IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_IE_WAREHOUSE_CREATE_COLLISION_GEOM_PHANTOM)
			CLEANUP_PHANTOM_COLLISION_GEOMETRY()
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(serverBD.iBS, BS_IE_WAREHOUSE_SERVER_CREATE_COLLISION_GEOM_DUNE_FLAG)
		IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_IE_WAREHOUSE_CREATE_COLLISION_GEOM_DUNE)
			CREATE_DUNE_COLLISION_GEOMETRY()
		ENDIF
	ELSE
		IF IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_IE_WAREHOUSE_CREATE_COLLISION_GEOM_DUNE)
			CLEANUP_DUNE_COLLISION_GEOMETRY()
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_GARAGE_ENTRY_HELP_TEXT()
	IF IS_PLAYER_IN_SVM_GARAGE(PLAYER_ID())
	AND NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_IE_WAREHOUSE_GARAGE_ENTRY_HELP_TEXT)
	AND NOT IS_HELP_MESSAGE_ON_SCREEN()
	AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		IF PLAYER_ID() = thisPERSONAL_MOD_GARAGE.pOwner
			PRINT_HELP("CUST_GAR_LEAVEH")
		ELSE
			PRINT_HELP("CUST_GAR_LEAVEHb")
		ENDIF
		
		SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_IE_WAREHOUSE_GARAGE_ENTRY_HELP_TEXT)
	ENDIF
ENDPROC

PROC RUN_MAIN_CLIENT_LOGIC()
	MAINTAIN_EXIT_VIA_GARAGE_DOOR()
	
	MANAGE_KICK_PLAYERS_OUT_FOR_SELL_MISSIONS()
	
	MAINTAIN_LAUNCHING_CARMOD_SCRIPT()
	
	MAINTAIN_SHOULD_START_INTRO_CUSTCENE()
	
	IF IS_PERSONAL_MOD_GARAGE_STATE(PERSONAL_MOD_GARAGE_STATE_LOADING)
		BOOL bReady = FALSE
		
		IF !g_bFinishedCreatingIEVehicles
//			#IF IS_DEBUG_BUILD
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "[IE_WAREHOUSE_SCRIPT] [RUN_IE_INTRO_CUTSCENE] RUN_MAIN_CLIENT_LOGIC - Creating vehicles.")
//			#ENDIF
			LOCAL_INITIALISE_IE_WAREHOUSE_VEHICLES()
		ENDIF
	
		IF IS_CAR_MOD_SCRIPT_READY(thisPERSONAL_MOD_GARAGE)
			IF SIMPLE_INTERIOR_IS_LOCAL_PLAYER_ENTERING_WITH_VEHICLE()
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PreventAutoShuffleToDriversSeat, TRUE)
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PreventAutoShuffleToTurretSeat, TRUE)
				
				IF ARE_PLAYERS_IN_SVM_VEHICLE()
					bReady = TRUE
					PRINTLN("[IE_WAREHOUSE_SCRIPT] [RUN_IE_INTRO_CUTSCENE] RUN_MAIN_CLIENT_LOGIC - Everyone in SVM warped, bReady set to TRUE")
				ENDIF
			ELSE
				bReady = TRUE
				PRINTLN("[IE_WAREHOUSE_SCRIPT] [RUN_IE_INTRO_CUTSCENE] RUN_MAIN_CLIENT_LOGIC - Not in SVM, bReady set to TRUE")
			ENDIF
		ENDIF
		
		IF CAN_SIMPLE_INTERIOR_CHILD_SCRIPT_TAKE_CONTROL()
			PRINTLN("[IE_WAREHOUSE_SCRIPT] [RUN_IE_INTRO_CUTSCENE] RUN_MAIN_CLIENT_LOGIC - Child script can take control.")
			SET_SIMPLE_INTERIOR_CHILD_SCRIPT_IS_IN_CONTROL(TRUE)
			
			BOOL bHasLoadTimeExpired = FALSE
					
			IF NOT HAS_NET_TIMER_STARTED(stTimeToLoad)
				START_NET_TIMER(stTimeToLoad)
				
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[AM_MP_IE_WAREHOUSE] RUN_MAIN_CLIENT_LOGIC - Starting net timer.")
				#ENDIF
			ENDIF
			
			IF bReady
				IF thisPERSONAL_MOD_GARAGE.bShouldPlayIntroCut
					PRINTLN("[IE_WAREHOUSE_SCRIPT] [RUN_IE_INTRO_CUTSCENE] RUN_MAIN_CLIENT_LOGIC - Gonna play the cutscene.")
					SET_PERSONAL_MOD_GARAGE_STATE(PERSONAL_MOD_GARAGE_STATE_INTRO_CUT)
				ELSE
					PRINTLN("[IE_WAREHOUSE_SCRIPT] [RUN_IE_INTRO_CUTSCENE] RUN_MAIN_CLIENT_LOGIC - No cutscene, moving to idle.")
					
					IF HAS_NET_TIMER_EXPIRED(stTimeToLoad, ciMAX_LOAD_TIME)
						bHasLoadTimeExpired = TRUE
						
						#IF IS_DEBUG_BUILD
						CDEBUG1LN(DEBUG_SAFEHOUSE, "[AM_MP_IE_WAREHOUSE] RUN_MAIN_CLIENT_LOGIC - Vehicle warehouse load timer has expired, setting child script as ready.")
						#ENDIF
					ENDIF
					
					IF g_bFinishedCreatingIEVehicles
					OR bHasLoadTimeExpired
						// Fix to ensure vehicles are positioned correctly once created.
//						UPDATE_IE_WAREHOUSE_VEHICLE_POSITIONS()
					
						SET_PERSONAL_MOD_GARAGE_STATE(PERSONAL_MOD_GARAGE_STATE_IDLE)
						SET_SIMPLE_INTERIOR_CHILD_SCRIPT_IS_IN_CONTROL(FALSE)
						SET_SIMPLE_INTERIOR_CHILD_SCRIPT_READY(TRUE)
						
						RESET_NET_TIMER(stTimeToLoad)
						
						#IF IS_DEBUG_BUILD
						CDEBUG1LN(DEBUG_SAFEHOUSE, "[AM_MP_IE_WAREHOUSE] RUN_MAIN_CLIENT_LOGIC - Resetting net timer.")
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
	ELIF IS_PERSONAL_MOD_GARAGE_STATE(PERSONAL_MOD_GARAGE_STATE_IDLE)
		IF SIMPLE_INTERIOR_IS_LOCAL_PLAYER_ENTERING_WITH_VEHICLE()
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.customVehicleNetIDs[thisPERSONAL_MOD_GARAGE.iEntranceVehicleIndex]))
			AND IS_SCREEN_FADED_IN()
				TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.customVehicleNetIDs[thisPERSONAL_MOD_GARAGE.iEntranceVehicleIndex]))
			ENDIF
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PreventAutoShuffleToDriversSeat, FALSE)
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PreventAutoShuffleToTurretSeat, FALSE)
			ENDIF
			
			SIMPLE_INTERIOR_CLEAR_ENTERING_WITH_VEHICLE()
			PRINTLN("AM_MP_IE_WAREHOUSE - RUN_MAIN_CLIENT_LOGIC SIMPLE_INTERIOR_CLEAR_ENTERING_WITH_VEHICLE()")
		ENDIF
		
		#IF FEATURE_DLC_1_2022
		IF g_sMPTunables.bCREATE_VENDING_MACHINES_IN_OLD_PROPERTIES
			SIMPLE_INTERIOR_MANAGE_PROP_WITH_SCRIPT(sVendingMachine)
		ENDIF
		#ENDIF
		MAINTAIN_MP_RADIO_LOCAL()
		MAINTAIN_LAPTOP_BLIP()
		MAINTAIN_IE_WAREHOUSE_VEHICLES()
		MAINTAIN_UNDERGROUND_GARAGE_BLIP()
		MAINTAIN_SVM_VEHICLE_LOCKS()
		MAINTAIN_CAN_DRIVE_OUT_OF_IE_GARAGE()
		MAINTAIN_GET_OUT_OF_OWNERS_SVM_VEHICLE()
		MAINTAIN_SVM_GARAGE_RENOVATION()
		MAINTAIN_COLLISION_GEOMETRY()
		MAINTAIN_GARAGE_ENTRY_HELP_TEXT()
		
		#IF IS_DEBUG_BUILD
		DEBUG_MAINTAIN_IE_APP_LAUNCHING()
		#ENDIF
		
		IF thisPERSONAL_MOD_GARAGE.bShouldPlayIntroCut
			SET_PERSONAL_MOD_GARAGE_STATE(PERSONAL_MOD_GARAGE_STATE_FADE_OUT_FOR_INTO_CUT)
			DO_SCREEN_FADE_OUT(500)
		ENDIF
	
	ELIF IS_PERSONAL_MOD_GARAGE_STATE(PERSONAL_MOD_GARAGE_STATE_FADE_OUT_FOR_INTO_CUT)
		IF IS_SCREEN_FADED_OUT()
			SET_PERSONAL_MOD_GARAGE_STATE(PERSONAL_MOD_GARAGE_STATE_INTRO_CUT)
		ENDIF
		
	ELIF IS_PERSONAL_MOD_GARAGE_STATE(PERSONAL_MOD_GARAGE_STATE_PLAYING_GARAGE_EXIT)
		IF IS_SIMPLE_INTERIOR_ENTRY_ANIM_STAGE(SIMPLE_INTERIOR_ANIM_STAGE_FINISHED, thisPERSONAL_MOD_GARAGE.garageExitAnim)
			DO_SCREEN_FADE_OUT(SIMPLE_INTERIOR_SCREEN_FADE_TIME)
			SET_PERSONAL_MOD_GARAGE_STATE(PERSONAL_MOD_GARAGE_STATE_FINISHING_GARAGE_EXIT)
		ENDIF
		
	ELIF IS_PERSONAL_MOD_GARAGE_STATE(PERSONAL_MOD_GARAGE_STATE_FINISHING_GARAGE_EXIT)
		IF IS_SCREEN_FADED_OUT()
			IF NOT IS_BIT_SET(thisPERSONAL_MOD_GARAGE.garageExitAnim.iBS, BS_SIMPLE_INTERIOR_ANIM_ON_ENTER_FADE_OUT_FINISH_SOUND_PLAYED)
				IF NOT IS_STRING_NULL_OR_EMPTY(thisPERSONAL_MOD_GARAGE.garageExitAnim.strOnEnterFadeOutFinishSoundName)
				AND NOT IS_STRING_NULL_OR_EMPTY(thisPERSONAL_MOD_GARAGE.garageExitAnim.strOnEnterFadeOutFinishSoundSet)
					PLAY_SOUND_FRONTEND(-1, thisPERSONAL_MOD_GARAGE.garageExitAnim.strOnEnterFadeOutFinishSoundName, thisPERSONAL_MOD_GARAGE.garageExitAnim.strOnEnterFadeOutFinishSoundSet)
				ENDIF
				SET_BIT(thisPERSONAL_MOD_GARAGE.garageExitAnim.iBS, BS_SIMPLE_INTERIOR_ANIM_ON_ENTER_FADE_OUT_FINISH_SOUND_PLAYED)
			ENDIF
			STOP_SIMPLE_INTERIOR_ENTRY_ANIM(thisPERSONAL_MOD_GARAGE.garageExitAnim)
		ENDIF
		
		// Reset global value so we have to wait for all vehicles to load next time we enter a vehicle warehouse.
		g_bFinishedCreatingIEVehicles = FALSE
		g_iTotalIEVehCreated = 0
		
	ELIF IS_PERSONAL_MOD_GARAGE_STATE(PERSONAL_MOD_GARAGE_STATE_INTRO_CUT)
		IF RUN_IE_INTRO_CUTSCENE()
			IF NOT IS_SIMPLE_INTERIOR_CHILD_SCRIPT_IN_CONTROL() // There's nothing to handle the fade, we have to do it
				DO_SCREEN_FADE_IN(SIMPLE_INTERIOR_SCREEN_FADE_TIME)
				NETWORK_FADE_IN_ENTITY(PLAYER_PED_ID(), TRUE)
				FORCE_PLAYER_LOCALLY_VISIBLE_FOR_FADE()
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					SET_ENTITY_ALPHA(PLAYER_PED_ID(), 255, FALSE)
				ENDIF
			ENDIF
			SET_SIMPLE_INTERIOR_CHILD_SCRIPT_READY(TRUE)
			SET_SIMPLE_INTERIOR_CHILD_SCRIPT_IS_IN_CONTROL(FALSE)
			g_SimpleInteriorData.bDontSimulateInputGaitOnEntrance = TRUE
			SET_PERSONAL_MOD_GARAGE_STATE(PERSONAL_MOD_GARAGE_STATE_IDLE)
			
			IF IS_BIT_SET(thisPERSONAL_MOD_GARAGE.iBS, BS_IE_WAREHOUSE_RESTORE_VISIBILITY_AT_END_OF_CUTSCENE)
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE, NSPC_NONE)
				CLEAR_BIT(thisPERSONAL_MOD_GARAGE.iBS, BS_IE_WAREHOUSE_RESTORE_VISIBILITY_AT_END_OF_CUTSCENE)
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

PROC RUN_MAIN_SERVER_LOGIC()
	IF IS_PERSONAL_MOD_GARAGE_SERVER_STATE(PERSONAL_MOD_GARAGE_SERVER_STATE_LOADING)
	AND MAINTAIN_SVM_GARAGE_VEHICLES()
		SET_PERSONAL_MOD_GARAGE_SERVER_STATE(PERSONAL_MOD_GARAGE_SERVER_STATE_IDLE)
	ELIF IS_PERSONAL_MOD_GARAGE_SERVER_STATE(PERSONAL_MOD_GARAGE_SERVER_STATE_IDLE)
		SERVER_MAINTAIN_PROPERTY_ACTIVITIES(playerBD[serverLocalActControl.iSlowPlayerLoop].iActivityRequested, serverBD.activityControl, serverLocalActControl)
		SERVER_MAINTAIN_MP_RADIO(serverBD.MPRadioServer, playerBD[MPRadioLocal.iServerPlayerStagger].MPRadioClient, MPRadioLocal , InteriorPropStruct )
		FORCE_ZERO_MASS_FOR_ALL_PLAYERS_IN_SVM_GARAGE()
		MAINTAIN_SVM_GARAGE_VEHICLES()
		MAINTAIN_COLLISION_GEOMETRY_CHECK()
	ENDIF
	
	CREATE_BOARDROOM_CHAIRS(serverBD.serverSeatBD, activitySeatStruct)
ENDPROC


PED_INDEX piMechanic
PROC CREATE_MECHANIC_PED_FOR_FLOW_PLAY()
	IF NOT DOES_ENTITY_EXIST(piMechanic)
		CREATE_IE_WAREHOUSE_MECHANIC(piMechanic, <<963.2017, -2997.5359, -40.6471>>, 77.6136)
	ENDIF
ENDPROC



PROC GET_ARMCHAIR_POSITION_FOR_WAREHOUSE_GARAGE(INT iProperty, VECTOR &vPropertyCoords, VECTOR &vPropertyRot, VECTOR &vOffsetRot, VECTOR &vOffset, INT iScenarioNode, BOOL bIsFemale = FALSE)
	
	
	MP_PROP_OFFSET_STRUCT tempStruct
	tempStruct = GET_INTERIOR_LOCATION_FOR_GARAGE_MOD(iProperty)
	
	vPropertyCoords = tempStruct.vLoc
	vPropertyRot = tempStruct.vRot
//	CDEBUG1LN(DEBUG_SAFEHOUSE, "CALCULATE_ARMCHAIR_POSITION_FOR_OFFICE_GARAGE: vPropertyCoords = ", vPropertyCoords, ", vPropertyRot = ", vPropertyRot)
//			vPropertyCoords = <<-73.7992,-818.9580,242.3858>>
//			vPropertyRot = <<0,0,70>>
	
	SWITCH iScenarioNode
		CASE 0
			IF bIsFemale
				vOffset 	= << 958.336, -3001.676, -40.618 >>
				vOffsetRot	= << -0.000, -0.000, 101.806 >> 
			ELSE
				vOffset 	= << 958.336, -3001.676, -40.618 >>
				vOffsetRot	= << -0.000, -0.000, 101.806 >> 
			ENDIF
		BREAK
		CASE 1
			IF bIsFemale
				vOffset 	= <<-4.355, 9.025, 1.1>>
				vOffsetRot	= <<0.000, 0.000, -90.00>>
			ELSE
				vOffset 	= <<-4.355, 9.025, 1.1>>
				vOffsetRot	= <<0.000, 0.000, -90.00>>
			ENDIF
		BREAK
		CASE 2
			IF bIsFemale								
				vOffset 	= <<-2.913, 10.51, 1.1>> 
				vOffsetRot	= <<0.000, 0.000, 180.00>>
			ELSE
				vOffset 	= <<-2.913, 10.51, 1.1>> 
				vOffsetRot	= <<0.000, 0.000, 180.00>>
			ENDIF
		BREAK
		CASE 3
			IF bIsFemale
				vOffset 	= <<-2.078, 10.51, 1.1>>
				vOffsetRot	= <<0.000, 0.000, 180.00>>
			ELSE
				vOffset 	= <<-2.078, 10.51, 1.1>>
				vOffsetRot	= <<0.000, 0.000, 180.00>>
			ENDIF
		BREAK
	ENDSWITCH
//	vOffset += dbvPropertySeatArmchairOffsetLoc
	vOffset += <<1.152, 2.619, 0>>
ENDPROC

PED_INDEX clonePedsForArmchair[NUM_SEAT_IN_OFFICE]
INT iDBAnimVar
BOOL bDBAnimVarFemale

INT iSyncScneForArmchair[NUM_SEAT_IN_OFFICE]

//VECTOR dbvPropertySeatArmchairOffsetLoc		
//VECTOR dbvPropertySeatArmchairOffsetRot		
//VECTOR dbvPropertySeatArmchairOffsetInSeatLoc[NUM_SEAT_IN_OFFICE]
//VECTOR dbvPropertySeatArmchairFootCollisionOffsetInSeatRot//[NUM_SEAT_IN_OFFICE]
//VECTOR dbvPropertySeatArmchairFootCollisionOffsetInSeatLoc//[NUM_SEAT_IN_OFFICE]

INT iCurrentProperty

FUNC VECTOR APPLY_ARMCHAIR_IN_SEAT_OFFSETS(INT iScenarioNode)
	iScenarioNode = iScenarioNode
	VECTOR vReturn = <<0.0, 0.0, 0.0>>
	
	IF IS_PROPERTY_OFFICE(iCurrentProperty)
		SWITCH iScenarioNode
			CASE 0
				vReturn = <<0.0, -0.020, -0.028>>
			BREAK
			CASE 1
			CASE 2
				vReturn = <<0.0, 0.060, -0.028>> 
			BREAK
			CASE 3
				vReturn = <<0.0, 0.0, -0.028>>
			BREAK	
			CASE 4
			CASE 5
				vReturn = <<0.0, 0.080, 0.0>>
			BREAK
			CASE 6
				vReturn = <<0.0, 0.0, -0.030>>
			BREAK
			CASE 11
			CASE 12
				vReturn = <<0.0, 0.080, 0.0>>
			BREAK
		ENDSWITCH
	ENDIF
	
	PED_COMP_NAME_ENUM eCurrentJbib = GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(PLAYER_PED_ID(), COMP_TYPE_JBIB)
	INT iDLCJBIBHash = GET_HASH_NAME_FOR_COMPONENT(NATIVE_TO_INT(PLAYER_PED_ID()), ENUM_TO_INT(PED_COMP_JBIB), GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB), GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB))
	IF IS_JBIB_COMPONENT_A_JACKET(GET_ENTITY_MODEL(PLAYER_PED_ID()), eCurrentJBIB, iDLCJBIBHash)
//		CDEBUG1LN(DEBUG_SAFEHOUSE, "APPLY_ARMCHAIR_IN_SEAT_OFFSETS: applying offset because player is wearing jacket")
		vReturn += <<0, -0.030,0>>	
	ELSE
//		CDEBUG1LN(DEBUG_SAFEHOUSE, "APPLY_ARMCHAIR_IN_SEAT_OFFSETS: player is not wearing jacket")
	ENDIF
	RETURN vReturn
ENDFUNC


PROC GET_SEAT_SCENARIO_NODE_COORDS_FOR_INTERIOR(INT iScenarioNode, VECTOR &vWorldCoords, VECTOR &vWorldRotation, BOOL bIsFemale = FALSE)

	
	SWITCH iScenarioNode
		CASE 0
			IF bIsFemale
				vWorldCoords 	= << 958.374, -3001.805, -40.644 >>
				vWorldRotation	= << 0.000, -0.000, 90.000 >>
			ELSE
				vWorldCoords 	= << 958.374, -3001.805, -40.644 >>
				vWorldRotation	= << 0.000, -0.000, 90.000 >>
			ENDIF
		BREAK

		CASE 1
			IF bIsFemale
				vWorldCoords 	= << 958.374, -3000.684, -40.644 >>
				vWorldRotation	= << 0.000, -0.000, 90.000 >>
			ELSE
				vWorldCoords 	= << 958.374, -3000.684, -40.644 >>
				vWorldRotation	= << 0.000, -0.000, 90.000 >>
			ENDIF
		BREAK

		CASE 2
			IF bIsFemale								
				vWorldCoords 	=  << 962.486, -3002.038, -40.644 >>
				vWorldRotation	= << 0.000, 0.000, 15.840 >>
		
			ELSE
				vWorldCoords 	=  << 962.486, -3002.038, -40.644 >>
				vWorldRotation	= << 0.000, 0.000, 15.840 >>
			ENDIF
		BREAK
		CASE 3
			IF bIsFemale
				vWorldCoords 	= <<-2.078, 10.51, 1.1>>
				vWorldRotation	= <<0.000, 0.000, 180.00>>
			ELSE
				vWorldCoords 	= <<-2.078, 10.51, 1.1>>
				vWorldRotation	= <<0.000, 0.000, 180.00>>
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

FUNC INT GET_NUMBER_OF_ARMCHAIR_SEAT_NODES()
//	IF IS_PROPERTY_OFFICE(iCurrentProperty)
//		RETURN NUM_SEAT_IN_OFFICE
//	ENDIF
//	IF IS_PROPERTY_CLUBHOUSE(iCurrentProperty, PROPERTY_CLUBHOUSE_1_BASE_A)
//		RETURN NUM_SEAT_IN_CLUBHOUSE
//	ENDIF
//	IF IS_PROPERTY_CLUBHOUSE(iCurrentProperty, PROPERTY_CLUBHOUSE_7_BASE_B)
//		RETURN 7
//	ENDIF
	
//	#IF FEATURE_IMPORT_EXPORT
//	IF IS_PROPERTY_OFFICE_GARAGE(iCurrentProperty, TRUE)
//		RETURN 4
//	ENDIF
//	
//	IF IS_PROPERTY_OFFICE_GARAGE(iCurrentProperty, FALSE, TRUE)
//		RETURN 4
//	ENDIF
//	#ENDIF
//	
//	RETURN NUM_SEAT_IN_CLUBHOUSE
	RETURN 3
ENDFUNC
MP_PROP_OFFSET_STRUCT mpOffsetOfficeSeats[NUM_SEAT_IN_OFFICE]
MP_PROP_OFFSET_STRUCT mpOffsetOfficeSeatsFootCollisionOffsetInSeat//[NUM_SEAT_IN_OFFICE]
//MP_PROP_OFFSET_STRUCT mpOffsetOfficeSeatsOffsetInSeat[NUM_SEAT_IN_OFFICE]
BOOL dbCreateOfficeChairPeds

PROC MANAGE_SEAT_NODE_POSITIONS()
//	INT iTempPropertyVar = g_iCurrentPropertyVariation
//	IF iTempPropertyVar != dbPropertyVar
//		iTempPropertyVar = dbPropertyVar
//	ENDIF
	INT iSeat
	
	
	REPEAT GET_NUMBER_OF_ARMCHAIR_SEAT_NODES() iSeat
	
		GET_SEAT_SCENARIO_NODE_COORDS_FOR_INTERIOR(iSeat, mpOffsetOfficeSeats[iSeat].vLoc, mpOffsetOfficeSeats[iSeat].vRot)
		
		mpOffsetOfficeSeatsFootCollisionOffsetInSeat.vLoc = <<0,-0.720,0>>
		mpOffsetOfficeSeatsFootCollisionOffsetInSeat.vRot = <<0,0,177.840>>
		
		#IF IS_DEBUG_BUILD
//		mpOffsetOfficeSeats[iSeat].vLoc += dbvPropertySeatArmchairOffsetLoc
//		mpOffsetOfficeSeats[iSeat].vRot += dbvPropertySeatArmchairOffsetRot
//		mpOffsetOfficeSeatsOffsetInSeat[iSeat].vLoc  = dbvPropertySeatArmchairOffsetInSeatLoc[iSeat]
		#ENDIF
		
//		#IF FEATURE_BIKER
//		IF IS_PROPERTY_CLUBHOUSE(iCurrentProperty, PROPERTY_CLUBHOUSE_1_BASE_A)
//			mpOffsetOfficeSeats[iSeat].vLoc +=<<5.621, -8.162, -2.990>> + <<-1.813, -24.564, 0.0>> + <<0.105, 0, 0>> + <<-0.066, -0.094, 0>>
//		ENDIF
//		IF IS_PROPERTY_CLUBHOUSE(iCurrentProperty, PROPERTY_CLUBHOUSE_7_BASE_B)
//			mpOffsetOfficeSeats[iSeat].vLoc +=<<0, 1.493, -3.802>> 
//		ENDIF
//		#ENDIF
//		MP_PROP_OFFSET_STRUCT tempOffstruct
//		tempOffstruct = GET_INTERIOR_LOCATION_FOR_OFFICE_GARAGE(iCurrentProperty)
//		VECTOR vTempOffset = mpOffsetOfficeSeats[0].vLoc - 
//		CDEBUG1LN(DEBUG_SAFEHOUSE, "offset = ", vTempOffset )
//		#IF FEATURE_IMPORT_EXPORT
//		IF IS_PROPERTY_OFFICE_GARAGE(iCurrentProperty, TRUE)
//			mpOffsetOfficeSeats[iSeat].vLoc +=<<-4.530,-2.730, 0>>
//		ENDIF
//		
//		IF IS_PROPERTY_OFFICE_GARAGE(iCurrentProperty, FALSE, TRUE)
//			mpOffsetOfficeSeats[iSeat].vLoc += <<-0.699,-6.444, -5.127>>
//			
//		ENDIF
//		
//		#ENDIF
	ENDREPEAT

	
ENDPROC

FUNC STRING GET_OFFICE_SEAT_ARMCHAIR_ENTER_ANIM_DICT(eOfficeSeatArmChairAnimVar eAnimVar, BOOL bIsFemale = FALSE)
	STRING sAnimRequst
	
//	IF IS_PROPERTY_OFFICE(iCurrentProperty)
//	#IF FEATURE_IMPORT_EXPORT
//	OR IS_PROPERTY_OFFICE_GARAGE(iCurrentProperty)
//	#ENDIF
//		SWITCH eAnimVar
//			CASE VAR_A
//				IF bIsFemale
//					sAnimRequst = "ANIM@AMB@OFFICE@SEATING@FEMALE@VAR_A@BASE@"
//				ELSE
//					sAnimRequst = "ANIM@AMB@OFFICE@SEATING@MALE@VAR_A@BASE@"
//				ENDIF
//			BREAK
//			CASE VAR_B
//				IF bIsFemale
//					sAnimRequst = "ANIM@AMB@OFFICE@SEATING@FEMALE@VAR_B@BASE@"
//				ELSE
//					sAnimRequst = "ANIM@AMB@OFFICE@SEATING@MALE@VAR_B@BASE@"
//				ENDIF
//			BREAK
//			CASE VAR_C
//				IF bIsFemale
//					sAnimRequst = "ANIM@AMB@OFFICE@SEATING@FEMALE@VAR_C@BASE@"
//				ELSE
//					sAnimRequst = "ANIM@AMB@OFFICE@SEATING@MALE@VAR_C@BASE@"
//				ENDIF
//			BREAK
//			CASE VAR_D
//				IF bIsFemale
//					sAnimRequst = "ANIM@AMB@OFFICE@SEATING@FEMALE@VAR_D@BASE@"
//				ELSE
//					sAnimRequst = "ANIM@AMB@OFFICE@SEATING@MALE@VAR_D@BASE@"
//				ENDIF
//			BREAK
//			CASE VAR_E
//				IF bIsFemale
//					sAnimRequst = "ANIM@AMB@OFFICE@SEATING@FEMALE@VAR_E@BASE@"
//				ELSE
//					sAnimRequst = "ANIM@AMB@OFFICE@SEATING@MALE@VAR_E@BASE@"
//				ENDIF
//			BREAK
//		ENDSWITCH
//	ELIF IS_PROPERTY_CLUBHOUSE(iCurrentProperty)		
		SWITCH eAnimVar
			CASE VAR_A
				IF bIsFemale
					sAnimRequst = "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_A@BASE@"
				ELSE
					sAnimRequst = "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_A@BASE@"
				ENDIF
			BREAK
			CASE VAR_B
				IF bIsFemale
					sAnimRequst = "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_B@BASE@"
				ELSE
					sAnimRequst = "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_B@BASE@"
				ENDIF
			BREAK
			CASE VAR_C
				IF bIsFemale
					sAnimRequst = "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_C@BASE@"
				ELSE
					sAnimRequst = "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_C@BASE@"
				ENDIF
			BREAK
		ENDSWITCH
//	ENDIF
	
	RETURN sAnimRequst
ENDFUNC

PROC CREATE_PED_CLONES_FOR_ARMCHAIR_PLACEMENT()
	IF dbCreateOfficeChairPeds = TRUE
		STRING sTempAnimDict
		eOfficeSeatArmChairAnimVar eDBAnimVar
		eDBAnimVar = INT_TO_ENUM(eOfficeSeatArmChairAnimVar, iDBAnimVar)
		sTempAnimDict = GET_OFFICE_SEAT_ARMCHAIR_ENTER_ANIM_DICT(eDBAnimVar, bDBAnimVarFemale)
		REQUEST_ANIM_DICT(sTempAnimDict)
		REQUEST_MODEL(MP_F_FREEMODE_01)
		REQUEST_MODEL(MP_M_FREEMODE_01)
		REQUEST_MODEL(prop_phonebox_03)
		IF HAS_ANIM_DICT_LOADED(sTempAnimDict)
		AND HAS_MODEL_LOADED(MP_F_FREEMODE_01)
		AND HAS_MODEL_LOADED(MP_M_FREEMODE_01)
		AND HAS_MODEL_LOADED(prop_phonebox_03)
			INT iSeat
			
			REPEAT NUM_SEAT_IN_OFFICE iSeat
				IF bDBAnimVarFemale
					clonePedsForArmchair[iSeat] = CREATE_PED(PEDTYPE_CIVMALE, MP_F_FREEMODE_01, mpOffsetOfficeSeats[iSeat].vLoc, mpOffsetOfficeSeats[iSeat].vRot.Z, TRUE, FALSE)
				ELSE
					clonePedsForArmchair[iSeat] = CREATE_PED(PEDTYPE_CIVMALE, MP_M_FREEMODE_01, mpOffsetOfficeSeats[iSeat].vLoc, mpOffsetOfficeSeats[iSeat].vRot.Z, TRUE, FALSE)
				ENDIF
				
//				ioArmChairFootCollision[iSeat] = CREATE_OBJECT(prop_phonebox_03, mpOffsetOfficeSeats[iSeat].vLoc)
				
//				SET_ENTITY_ROTATION(ioArmChairFootCollision[iSeat], mpOffsetOfficeSeats[iSeat].vRot)
				
				iSyncScneForArmchair[iSeat] = NETWORK_CREATE_SYNCHRONISED_SCENE(mpOffsetOfficeSeats[iSeat].vLoc, mpOffsetOfficeSeats[iSeat].vRot, DEFAULT, FALSE, TRUE, DEFAULT)
					
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(clonePedsForArmchair[iSeat], iSyncScneForArmchair[iSeat], sTempAnimDict, "base", WALK_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE ,  RBF_PLAYER_IMPACT, WALK_BLEND_IN)					
				
				NETWORK_START_SYNCHRONISED_SCENE(iSyncScneForArmchair[iSeat])
			ENDREPEAT
			dbCreateOfficeChairPeds = FALSE
		ENDIF
	ENDIF
ENDPROC
BOOL dbDeleteOfficeChairPeds
BOOL dbShowArmchairLocates
BOOL dbUpdateArmchairDebugCoordsRealtime
PROC DELETE_PED_CLONES_FOR_ARMCHAIR_PLACEMENT()
	IF dbDeleteOfficeChairPeds = TRUE
		INT iSeat
		REPEAT NUM_SEAT_IN_OFFICE iSeat
			NETWORK_STOP_SYNCHRONISED_SCENE(iSyncScneForArmchair[iSeat])
			DELETE_PED(clonePedsForArmchair[iSeat])
			dbDeleteOfficeChairPeds = FALSE
		ENDREPEAT
	ENDIF
ENDPROC


VECTOR vOfficeSeatArmchairBoundingBox1
VECTOR vOfficeSeatArmchairBoundingBox2
FLOAT fOfficeSeatArmchairBoundingWidth = 0.928
VECTOR dbvOfficeSeatArmchairBoundingBox1 = <<0.015,-0.250,-0.222>>
VECTOR dbvOfficeSeatArmchairBoundingBox2 = <<0.015,-1.050,1.486>>

FUNC FLOAT GET_SEAT_LOCATE_WIDTH(INT iSeatIndex)
	
	SWITCH iSeatIndex
		CASE 6
			// Fix for B*3007589
			RETURN fOfficeSeatArmchairBoundingWidth * 1.368
		BREAK
		CASE 13
		CASE 14
			RETURN fOfficeSeatArmchairBoundingWidth * 2.0
		BREAK
		CASE 11
		CASE 12
			RETURN fOfficeSeatArmchairBoundingWidth * 0.8
		BREAK
	ENDSWITCH
	
	RETURN fOfficeSeatArmchairBoundingWidth
ENDFUNC

PROC DISPLAY_ARMCHAIR_LOCATES()
	IF dbShowArmchairLocates


		VECTOR CurrentSeatPos
		INT iSeat
		
		REPEAT GET_NUMBER_OF_ARMCHAIR_SEAT_NODES() iSeat	
//			VECTOR CurrentSeatPos
			VECTOR CurrentSeatRot
			CurrentSeatPos = mpOffsetOfficeSeats[iSeat].vLoc
			CurrentSeatRot = mpOffsetOfficeSeats[iSeat].vRot
//			CurrentSeatPos.z += 1

			vOfficeSeatArmchairBoundingBox1 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(CurrentSeatPos,  CurrentSeatRot.Z, dbvOfficeSeatArmchairBoundingBox1) //<<0.393104,0.603146,-0.627219>>)
//
			vOfficeSeatArmchairBoundingBox2 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(CurrentSeatPos,  CurrentSeatRot.Z, dbvOfficeSeatArmchairBoundingBox2) //<<-0.441553,0.563528,1.202967>>)
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vOfficeSeatArmchairBoundingBox1, vOfficeSeatArmchairBoundingBox2, GET_SEAT_LOCATE_WIDTH(iSeat))
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC


PROC ADJUST_ACTIVITY_SEAT_CLONE_SCENE()
	IF dbUpdateArmchairDebugCoordsRealtime
		INT i
		REPEAT GET_NUMBER_OF_ARMCHAIR_SEAT_NODES() i
			INT localScene  
			localScene  = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iSyncScneForArmchair[i])
			IF localScene != -1
				
				SET_SYNCHRONIZED_SCENE_ORIGIN(localScene, mpOffsetOfficeSeats[i].vLoc, mpOffsetOfficeSeats[i].vRot)
				
	//			animSequence.ScenePosition =  GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(animSequence.originalScenePosition, animSequence.originalSceneOrientation.Z, widgetAnimSeq.vOffsetPos)
	//			animSequence.sceneOrientation = animSequence.originalSceneOrientation - widgetAnimSeq.vOffsetRot
				
			ENDIF
			
		ENDREPEAT
	ENDIF
ENDPROC

PROC DISPLAY_SEAT_NODE_POSITIONS()
	CREATE_PED_CLONES_FOR_ARMCHAIR_PLACEMENT()
	DELETE_PED_CLONES_FOR_ARMCHAIR_PLACEMENT()
	DISPLAY_ARMCHAIR_LOCATES()
	ADJUST_ACTIVITY_SEAT_CLONE_SCENE()
ENDPROC
BOOL bFootCollisionCreated[NUM_NETWORK_PLAYERS]
OBJECT_INDEX ioArmChairFootCollision[NUM_NETWORK_PLAYERS]
PROC DESTROY_FOOT_COLLISION_PROP(INT iPlayer)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_FOOT_COLLISION_PROP: DELETE_OBJECT:  iPlayer= ", iPlayer)
	DELETE_OBJECT(ioArmChairFootCollision[iPlayer])
ENDPROC

FUNC BOOL CREATE_FOOT_COLLISION_PROP(INT iPlayer)
	INT iSeatID
	iSeatID = playerBD[iPlayer].iOfficeSeatArmchairID
	BOOL bAllowToCreate
	
	IF IS_PROPERTY_CLUBHOUSE(iCurrentProperty)
	AND GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_1_BASE_A
		IF iSeatID = 0 
		OR iSeatID = 2
			bAllowToCreate = FALSE
		ELSE
			bAllowToCreate = TRUE
		ENDIF
	ELSE
		bAllowToCreate = TRUE
	ENDIF	
		
	
	REQUEST_MODEL(prop_phonebox_03)
	
	IF HAS_MODEL_LOADED(prop_phonebox_03)
	AND bAllowToCreate
		IF NOT DOES_ENTITY_EXIST(ioArmChairFootCollision[iPlayer])			
		
			ioArmChairFootCollision[iPlayer] = CREATE_OBJECT_NO_OFFSET(prop_phonebox_03, mpOffsetOfficeSeats[iSeatID].vLoc, FALSE, FALSE)
						
			SET_ENTITY_ROTATION(ioArmChairFootCollision[iPlayer], mpOffsetOfficeSeats[iSeatID].vRot)
			
			FREEZE_ENTITY_POSITION(ioArmChairFootCollision[iPlayer], TRUE)
			SET_CAN_CLIMB_ON_ENTITY(ioArmChairFootCollision[iPlayer], FALSE)
			SET_ENTITY_NO_COLLISION_ENTITY(ioArmChairFootCollision[iPlayer], GET_PLAYER_PED(INT_TO_PLAYERINDEX(iPlayer)), FALSE)
			VECTOR temp = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(mpOffsetOfficeSeats[iSeatID].vLoc, mpOffsetOfficeSeats[iSeatID].vRot.Z, mpOffsetOfficeSeatsFootCollisionOffsetInSeat.vLoc)
			SET_ENTITY_VISIBLE(ioArmChairFootCollision[iPlayer], FALSE)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_FOOT_COLLISION_PROP: temp = ", temp )
			SET_ENTITY_COORDS(ioArmChairFootCollision[iPlayer], temp)
			VECTOR tempRotation = mpOffsetOfficeSeats[iSeatID].vRot - mpOffsetOfficeSeatsFootCollisionOffsetInSeat.vRot
			NORMALISE_ROTATION(tempRotation)
			SET_ENTITY_ROTATION(ioArmChairFootCollision[iPlayer], tempRotation)
		ENDIF	
		RETURN TRUE
		
	ENDIF
	RETURN FALSE
				
ENDFUNC
PROC RUN_FEET_COLLISION_LOGIC()
	INT iPlayer
	PLAYER_INDEX aPlayerID
	
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		aPlayerID = INT_TO_PLAYERINDEX(iPlayer) 
		IF aPlayerID != INVALID_PLAYER_INDEX()
		AND PLAYER_ID() != aPlayerID
		AND IS_NET_PLAYER_OK(aPlayerID)
		AND NETWORK_IS_PLAYER_A_PARTICIPANT(aPlayerID)
		AND playerBD[NATIVE_TO_INT(aPlayerID)].iOfficeSeatArmchairID != -1
			IF NOT bFootCollisionCreated[NATIVE_TO_INT(aPlayerID)]
				IF CREATE_FOOT_COLLISION_PROP(iPlayer)
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "RUN_FEET_COLLISION_LOGIC: Found player ", GET_PLAYER_NAME(aPlayerID), ", sitting in seat: ", playerBD[NATIVE_TO_INT(aPlayerID)].iOfficeSeatArmchairID, " creating foot collision")
					bFootCollisionCreated[NATIVE_TO_INT(aPlayerID)] = TRUE
				ENDIF
			ENDIF
		ELSE
//			IF aPlayerID != INVALID_PLAYER_INDEX()
			IF PLAYER_ID() != aPlayerID
				#IF IS_DEBUG_BUILD
				IF aPlayerID != INVALID_PLAYER_INDEX()
					IF bFootCollisionCreated[iPlayer]
						CDEBUG1LN(DEBUG_SAFEHOUSE, "RUN_FEET_COLLISION_LOGIC: iPlayer= ", iPlayer, ", bFootCollisionCreated = TRUE,  iOfficeSeatArmchairID = ", playerBD[iPlayer].iOfficeSeatArmchairID) 
					ENDIF
					IF playerBD[iPlayer].iOfficeSeatArmchairID != -1
						CDEBUG1LN(DEBUG_SAFEHOUSE, "RUN_FEET_COLLISION_LOGIC: iPlayer= ", iPlayer, ", iOfficeSeatArmchairID = ", playerBD[iPlayer].iOfficeSeatArmchairID) 
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "RUN_FEET_COLLISION_LOGIC: aPlayerID = INVALID_PLAYER_INDEX: iPlayer= ", iPlayer)
				ENDIF
				#ENDIF
				IF bFootCollisionCreated[iPlayer]
//				AND playerBD[NATIVE_TO_INT(aPlayerID)].iOfficeSeatArmchairID = -1
					DESTROY_FOOT_COLLISION_PROP(iPlayer)
					bFootCollisionCreated[iPlayer] = FALSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "RUN_FEET_COLLISION_LOGIC: Found an invalid player sitting in seat: ", playerBD[iPlayer].iOfficeSeatArmchairID, " destroying foot collision")
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC



PROC SUPPRESS_JAC_ARMCHAIR_PHONE_HUD_AND_ACTION_LOGIC()
	SWITCH g_OfficeArmChairState
		CASE OFFICE_SEAT_CHECK_ENTER_OR_IDLE_ANIM
		CASE OFFICE_SEAT_CHECK_BASE_ANIM
			INVALIDATE_IDLE_CAM()
			HUD_FORCE_WEAPON_WHEEL(FALSE)
			DISPLAY_AMMO_THIS_FRAME(FALSE)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
			SET_CLEAR_ON_CALL_HUD_THIS_FRAME()
			DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
			DISABLE_SELECTOR_THIS_FRAME()
		BREAK
		
		CASE OFFICE_SEAT_RUN_ENTER_ANIM
		CASE OFFICE_SEAT_APPROACH
		CASE OFFICE_SEAT_EXIT_TURN
		CASE OFFICE_SEAT_ENTER_TURN
		CASE OFFICE_SEAT_PLAY_COMPUTER_ENTER
		CASE OFFICE_SEAT_PLAY_COMPUTER_IDLE
		CASE OFFICE_SEAT_PLAY_COMPUTER_EXIT
		CASE OFFICE_SEAT_CLEANUP
			INVALIDATE_IDLE_CAM()
			HUD_FORCE_WEAPON_WHEEL(FALSE)
			DISPLAY_AMMO_THIS_FRAME(FALSE)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
			SET_CLEAR_ON_CALL_HUD_THIS_FRAME()
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			DISABLE_SELECTOR_THIS_FRAME()
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL IS_ARMCHAIR_TV_SEAT(INT iSeatID)
	IF IS_PROPERTY_OFFICE(iCurrentProperty)
		SWITCH iSeatID
			CASE 3
			CASE 4
			CASE 5
				RETURN TRUE
			BREAK
		ENDSWITCH
	ELIF IS_PROPERTY_CLUBHOUSE(iCurrentProperty)
		IF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_1_BASE_A
			SWITCH iSeatID
				CASE 0
				CASE 1
				CASE 2
					RETURN TRUE
				BREAK	
			ENDSWITCH
		ELSE
			SWITCH iSeatID
				CASE 0
				CASE 1
				CASE 2
				CASE 3
					RETURN TRUE
				BREAK	
			ENDSWITCH
		ENDIF
	ENDIF	
	
	RETURN FALSE
ENDFUNC


FUNC BOOL CAN_PED_CHANGE_POSE()
	IF IS_PLAYER_FEMALE()
		PED_COMP_NAME_ENUM eStoredDress
		eStoredDress = GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_JBIB) 
		PED_COMP_NAME_ENUM eStoredSkirt
		eStoredSkirt = GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_LEGS) 
		
		#IF IS_DEBUG_BUILD
		IF IS_ITEM_A_DRESS(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_JBIB, eStoredDress) 
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "CAN_PED_CHANGE_POSE: Ped is wearing a dress")
		ELSE
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "CAN_PED_CHANGE_POSE: Ped is not wearing a dress")
		ENDIF
		
		IF IS_ITEM_A_SKIRT(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_LEGS, eStoredSkirt)  
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "CAN_PED_CHANGE_POSE: Ped is wearing a skirt")
		ELSE
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "CAN_PED_CHANGE_POSE: Ped is not wearing a skirt")
		ENDIF
		#ENDIF
		
		IF IS_ITEM_A_DRESS(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_JBIB, eStoredDress) 
		OR IS_ITEM_A_SKIRT(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_LEGS, eStoredSkirt) 
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC


PROC SHOW_OFFICE_ARMCHAIR_EXIT_PROMPT()
	IF CAN_PED_CHANGE_POSE()
		IF !IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPOF_AC_PC_EXIT")
				CLEAR_HELP()
				RELEASE_CONTEXT_INTENTION(iOfficeSeatContextIntention)
			ENDIF
			IF iOfficeSeatContextIntention = NEW_CONTEXT_INTENTION
				REGISTER_CONTEXT_INTENTION(iOfficeSeatContextIntention, CP_HIGH_PRIORITY, "MPOF_AC_EXIT")
			ELSE
	//			CDEBUG1LN(DEBUG_SAFEHOUSE, "SHOW_OFFICE_ARMCHAIR_EXIT_PROMPT: iOfficeSeatContextIntention = ", iOfficeSeatContextIntention)
			ENDIF
		ELSE
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPOF_AC_EXIT")
				CLEAR_HELP()
				RELEASE_CONTEXT_INTENTION(iOfficeSeatContextIntention)
			ENDIF
			IF iOfficeSeatContextIntention = NEW_CONTEXT_INTENTION
				REGISTER_CONTEXT_INTENTION(iOfficeSeatContextIntention, CP_HIGH_PRIORITY, "MPOF_AC_PC_EXIT")
			ELSE
	//			CDEBUG1LN(DEBUG_SAFEHOUSE, "SHOW_OFFICE_ARMCHAIR_EXIT_PROMPT: iOfficeSeatContextIntention = ", iOfficeSeatContextIntention)
			ENDIF
		ENDIF
	ELSE
		IF !IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPOFSEAT_PCEXIT")
				CLEAR_HELP()
				RELEASE_CONTEXT_INTENTION(iOfficeSeatContextIntention)
			ENDIF
			
			IF iOfficeSeatContextIntention = NEW_CONTEXT_INTENTION
				REGISTER_CONTEXT_INTENTION(iOfficeSeatContextIntention, CP_HIGH_PRIORITY, "MPOFSEAT_EXIT")
			ENDIF
		ELSE
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPOFSEAT_EXIT")
				CLEAR_HELP()
				RELEASE_CONTEXT_INTENTION(iOfficeSeatContextIntention)
			ENDIF
			
			IF iOfficeSeatContextIntention = NEW_CONTEXT_INTENTION
				REGISTER_CONTEXT_INTENTION(iOfficeSeatContextIntention, CP_HIGH_PRIORITY, "MPOFSEAT_PCEXIT")
			ENDIF
		ENDIF
	ENDIF
ENDPROC


FUNC BOOL IS_PLAYER_RUNNING_TV_SEAT_ANIMS(PED_INDEX piPed)
	// Office Anim Dictionaries.
	IF IS_ENTITY_PLAYING_ANIM(piPed, "anim@AMB@OFFICE@SEATING@FEMALE@VAR_B@BASE@", "IDLE_A")
	OR IS_ENTITY_PLAYING_ANIM(piPed, "anim@AMB@OFFICE@SEATING@FEMALE@VAR_B@BASE@", "IDLE_B")
	OR IS_ENTITY_PLAYING_ANIM(piPed, "anim@AMB@OFFICE@SEATING@FEMALE@VAR_B@BASE@", "IDLE_C")
	OR IS_ENTITY_PLAYING_ANIM(piPed, "anim@AMB@OFFICE@SEATING@FEMALE@VAR_B@BASE@", "BASE")
	
	OR IS_ENTITY_PLAYING_ANIM(piPed, "anim@AMB@OFFICE@SEATING@FEMALE@VAR_C@BASE@", "IDLE_A")
	OR IS_ENTITY_PLAYING_ANIM(piPed, "anim@AMB@OFFICE@SEATING@FEMALE@VAR_C@BASE@", "IDLE_B")
	OR IS_ENTITY_PLAYING_ANIM(piPed, "anim@AMB@OFFICE@SEATING@FEMALE@VAR_C@BASE@", "IDLE_C")
	OR IS_ENTITY_PLAYING_ANIM(piPed, "anim@AMB@OFFICE@SEATING@FEMALE@VAR_C@BASE@", "BASE")
	
	OR IS_ENTITY_PLAYING_ANIM(piPed, "anim@AMB@OFFICE@SEATING@FEMALE@VAR_D@BASE@", "IDLE_A")
	OR IS_ENTITY_PLAYING_ANIM(piPed, "anim@AMB@OFFICE@SEATING@FEMALE@VAR_D@BASE@", "IDLE_B")
	OR IS_ENTITY_PLAYING_ANIM(piPed, "anim@AMB@OFFICE@SEATING@FEMALE@VAR_D@BASE@", "IDLE_C")
	OR IS_ENTITY_PLAYING_ANIM(piPed, "anim@AMB@OFFICE@SEATING@FEMALE@VAR_D@BASE@", "BASE")
	
	OR IS_ENTITY_PLAYING_ANIM(piPed, "anim@AMB@OFFICE@SEATING@MALE@VAR_A@BASE@", "IDLE_A")
	OR IS_ENTITY_PLAYING_ANIM(piPed, "anim@AMB@OFFICE@SEATING@MALE@VAR_A@BASE@", "IDLE_B")
	OR IS_ENTITY_PLAYING_ANIM(piPed, "anim@AMB@OFFICE@SEATING@MALE@VAR_A@BASE@", "IDLE_C")
	OR IS_ENTITY_PLAYING_ANIM(piPed, "anim@AMB@OFFICE@SEATING@MALE@VAR_A@BASE@", "BASE")
	
	OR IS_ENTITY_PLAYING_ANIM(piPed, "anim@AMB@OFFICE@SEATING@MALE@VAR_D@BASE@", "IDLE_A")
	OR IS_ENTITY_PLAYING_ANIM(piPed, "anim@AMB@OFFICE@SEATING@MALE@VAR_D@BASE@", "IDLE_B")
	OR IS_ENTITY_PLAYING_ANIM(piPed, "anim@AMB@OFFICE@SEATING@MALE@VAR_D@BASE@", "IDLE_C")
	OR IS_ENTITY_PLAYING_ANIM(piPed, "anim@AMB@OFFICE@SEATING@MALE@VAR_D@BASE@", "BASE")
	
	OR IS_ENTITY_PLAYING_ANIM(piPed, "anim@AMB@OFFICE@SEATING@MALE@VAR_E@BASE@", "IDLE_A")
	OR IS_ENTITY_PLAYING_ANIM(piPed, "anim@AMB@OFFICE@SEATING@MALE@VAR_E@BASE@", "IDLE_B")
	OR IS_ENTITY_PLAYING_ANIM(piPed, "anim@AMB@OFFICE@SEATING@MALE@VAR_E@BASE@", "IDLE_C")
	OR IS_ENTITY_PLAYING_ANIM(piPed, "anim@AMB@OFFICE@SEATING@MALE@VAR_E@BASE@", "BASE")
	
	// Clubhouse Anim Dictionaries.
	OR IS_ENTITY_PLAYING_ANIM(piPed, "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_A@BASE@", "IDLE_A")
	OR IS_ENTITY_PLAYING_ANIM(piPed, "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_A@BASE@", "IDLE_B")
	OR IS_ENTITY_PLAYING_ANIM(piPed, "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_A@BASE@", "IDLE_C")
	OR IS_ENTITY_PLAYING_ANIM(piPed, "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_A@BASE@", "BASE")
	
	OR IS_ENTITY_PLAYING_ANIM(piPed, "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_B@BASE@", "IDLE_A")
	OR IS_ENTITY_PLAYING_ANIM(piPed, "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_B@BASE@", "IDLE_B")
	OR IS_ENTITY_PLAYING_ANIM(piPed, "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_B@BASE@", "IDLE_C")
	OR IS_ENTITY_PLAYING_ANIM(piPed, "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_B@BASE@", "BASE")
	
	OR IS_ENTITY_PLAYING_ANIM(piPed, "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_C@BASE@", "IDLE_A")
	OR IS_ENTITY_PLAYING_ANIM(piPed, "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_C@BASE@", "IDLE_B")
	OR IS_ENTITY_PLAYING_ANIM(piPed, "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_C@BASE@", "IDLE_C")
	OR IS_ENTITY_PLAYING_ANIM(piPed, "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_C@BASE@", "BASE")
	
	OR IS_ENTITY_PLAYING_ANIM(piPed, "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_A@BASE@", "IDLE_A")
	OR IS_ENTITY_PLAYING_ANIM(piPed, "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_A@BASE@", "IDLE_B")
	OR IS_ENTITY_PLAYING_ANIM(piPed, "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_A@BASE@", "IDLE_C")
	OR IS_ENTITY_PLAYING_ANIM(piPed, "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_A@BASE@", "BASE")
	
	OR IS_ENTITY_PLAYING_ANIM(piPed, "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_B@BASE@", "IDLE_A")
	OR IS_ENTITY_PLAYING_ANIM(piPed, "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_B@BASE@", "IDLE_B")
	OR IS_ENTITY_PLAYING_ANIM(piPed, "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_B@BASE@", "IDLE_C")
	OR IS_ENTITY_PLAYING_ANIM(piPed, "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_B@BASE@", "BASE")
	
	OR IS_ENTITY_PLAYING_ANIM(piPed, "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_C@BASE@", "IDLE_A")
	OR IS_ENTITY_PLAYING_ANIM(piPed, "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_C@BASE@", "IDLE_B")
	OR IS_ENTITY_PLAYING_ANIM(piPed, "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_C@BASE@", "IDLE_C")
	OR IS_ENTITY_PLAYING_ANIM(piPed, "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_C@BASE@", "BASE")
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_PLAYER_CHANGE_POSITION(INT iSeatID)
	iSeatID  = iSeatID
//	IF NOT IS_ARMCHAIR_TV_SEAT(iSeatID)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "CAN_PLAYER_CHANGE_POSITION: Player is not in armchair, there for can change position")
		RETURN TRUE
//	ELSE
//		IF IS_ARMCHAIR_TV_SEAT(iSeatID)
//		AND IS_PLAYER_RUNNING_TV_SEAT_ANIMS(PLAYER_PED_ID())
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "CAN_PLAYER_CHANGE_POSITION: Player is in armchair and playing tv seat anim, there for can change position")
//			RETURN TRUE
//		ENDIF
//	ENDIF
//	RETURN FALSE
ENDFUNC

FUNC eOfficeSeatArmChairAnimVar GET_ANIM_VAR_TO_SWITCH_TO(eOfficeSeatArmChairAnimVar eAnimVar, BOOL bIsFemale = FALSE, BOOL bSofa = FALSE)
	eOfficeSeatArmChairAnimVar eReturnAnimVar
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_ANIM_VAR_TO_SWITCH_TO: eAnimVar:", eAnimVar, ", bIsFemale: ", bIsFemale, ", bSofa: ", bSofa)
	
//	IF IS_PROPERTY_OFFICE(iCurrentProperty)
//	#IF FEATURE_IMPORT_EXPORT
//	OR IS_PROPERTY_OFFICE_GARAGE(iCurrentProperty)
//	#ENDIF
//		IF bIsFemale
//			IF bSofa = FALSE
//				SWITCH eAnimVar
//					CASE VAR_A
//						eReturnAnimVar = VAR_B
//					BREAK
//					CASE VAR_B
//						eReturnAnimVar = VAR_C
//					BREAK
//					CASE VAR_C
//						eReturnAnimVar = VAR_D
//					BREAK
//					CASE VAR_D
//						eReturnAnimVar = VAR_A
//					BREAK
//				ENDSWITCH
//			ELSE
//				SWITCH eAnimVar
//					CASE VAR_B
//						eReturnAnimVar = VAR_C
//					BREAK
//					CASE VAR_C
//						eReturnAnimVar = VAR_D
//					BREAK
//					CASE VAR_D
//						eReturnAnimVar = VAR_B
//					BREAK
//				ENDSWITCH
//			ENDIF
//		ELSE
//			IF bSofa = FALSE
//				SWITCH eAnimVar
//					CASE VAR_A
//						eReturnAnimVar = VAR_B
//					BREAK
//					CASE VAR_B
//						eReturnAnimVar = VAR_C
//					BREAK
//					CASE VAR_C
//						eReturnAnimVar = VAR_D
//					BREAK
//					CASE VAR_D
//						eReturnAnimVar = VAR_E
//					BREAK
//					CASE VAR_E
//						eReturnAnimVar = VAR_A
//					BREAK
//				ENDSWITCH
//			ELSE
//				SWITCH eAnimVar
//					CASE VAR_A
//						eReturnAnimVar = VAR_D
//					BREAK
//					CASE VAR_D
//						eReturnAnimVar = VAR_E
//					BREAK
//					CASE VAR_E
//						eReturnAnimVar = VAR_A
//					BREAK
//				ENDSWITCH
//			ENDIF
//		ENDIF
//	ELIF IS_PROPERTY_CLUBHOUSE(iCurrentProperty)
		SWITCH eAnimvar
			CASE VAR_A
				eReturnAnimVar = VAR_B
			BREAK
			CASE VAR_B
				eReturnAnimVar = VAR_C
			BREAK
			CASE VAR_C
				eReturnAnimVar = VAR_A
			BREAK
		ENDSWITCH
//	ENDIF
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_ANIM_VAR_TO_SWITCH_TO: eAnimVar:", eAnimVar, ", bIsFemale: ", bIsFemale, ", bSofa: ", bSofa, ", eReturnAnimVar = ", ENUM_TO_INT(eReturnAnimVar))
		
	RETURN eReturnAnimVar
ENDFUNC

FUNC TEXT_LABEL_31 GET_TRANSITION_ANIM_CLIP(eOfficeSeatArmChairAnimVar eAnimVar, BOOL bIsFemale = FALSE, BOOL bSofa = FALSE)
	TEXT_LABEL_31 tlAnimClip
	SWITCH eAnimVar
		CASE VAR_A
			tlAnimClip = "A_TO_"
		BREAK
		CASE VAR_B
			tlAnimClip = "B_TO_"
		BREAK
		CASE VAR_C
			tlAnimClip = "C_TO_"
		BREAK
		CASE VAR_D
			tlAnimClip = "D_TO_"
		BREAK
		CASE VAR_E
			tlAnimClip = "E_TO_"
		BREAK
	ENDSWITCH
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_TRANSITION_ANIM_CLIP - ANIM VAR: ", eAnimVar, " ANIM CLIP: ", tlAnimClip)
	
	SWITCH GET_ANIM_VAR_TO_SWITCH_TO(eAnimVar, bIsFemale, bSofa)
		CASE VAR_A
			tlAnimClip += "A"
		BREAK
		CASE VAR_B
			tlAnimClip += "B"
		BREAK
		CASE VAR_C
			tlAnimClip += "C"
		BREAK
		CASE VAR_D
			tlAnimClip += "D"
		BREAK
		CASE VAR_E
			tlAnimClip += "E"
		BREAK
	ENDSWITCH
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_TRANSITION_ANIM_CLIP: ", tlAnimClip)
	
	RETURN tlAnimClip
ENDFUNC

eOfficeSeatArmChairAnimVar eAnimOfficeSeatVar


FUNC BOOL IS_SIMPLE_CHAIR_SOFA(INT iSeatID)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_SIMPLE_CHAIR_SOFA: iSeatID: ", iSeatID)
	
//	IF IS_PROPERTY_OFFICE(iCurrentProperty)
//		SWITCH	iSeatID
//			CASE 0
//			CASE 1
//			CASE 2
//			CASE 3
//			CASE 4
//			CASE 5
//			CASE 8
//			CASE 9
//			CASE 10
//			CASE 11
//			CASE 12
//				RETURN TRUE
//			BREAK
//		ENDSWITCH
//	#IF FEATURE_BIKER
//	ELIF IS_PROPERTY_CLUBHOUSE(iCurrentProperty)
//		IF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_1_BASE_A
////			SWITCH iSeatID
////				CASE 0
////				CASE 1
////				CASE 2
////				CASE 4
////				CASE 5
////				CASE 7
////				CASE 8
////				CASE 9
////				CASE 10
////				CASE 11
////				CASE 12
//					RETURN FALSE
////				BREAK
////			ENDSWITCH
//		ELSE
////			SWITCH iSeatID
////				CASE 0
////				CASE 1
////				CASE 2
////				CASE 3
////				CASE 4
////				CASE 5
//					RETURN FALSE
////				BREAK
////			ENDSWITCH
//		ENDIF
//	#ENDIF
//	ENDIF
//	
//	
//	#IF FEATURE_IMPORT_EXPORT
//	IF IS_PROPERTY_OFFICE_GARAGE(iCurrentProperty)

	SWITCH iSeatID
		CASE 0
		CASE 1
			RETURN TRUE
		BREAK
		CASE 2
		
			RETURN FALSE
		BREAK
	ENDSWITCH

	RETURN TRUE
//	ENDIF
//	#ENDIF
//	
//	RETURN FALSE
ENDFUNC

SCRIPT_TIMER stOfficeChairExitDelay

PROC OFFICE_SEAT_PLAYER_INTPUT_LOGIC_FOR_ARMCHAIR_INTERRUPT()
	
	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
		
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
	

	IF NOT IS_PAUSE_MENU_ACTIVE()
	AND NOT IS_BROWSER_OPEN() 
	AND NOT IS_PHONE_ONSCREEN(FALSE)
	AND NOT IS_INTERACTION_MENU_OPEN()
	AND NOT IS_ANY_INTERACTION_ANIM_PLAYING()
	AND NOT IS_SELECTOR_UI_BUTTON_PRESSED()
//	AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sOfficeSeatAnimDict, "enter")
		
		IF ENUM_TO_INT(g_OfficeArmChairState) > ENUM_TO_INT(OFFICE_SEAT_RUN_ENTER_ANIM)
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROP_OFF_M_6")
			AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROP_OFF_M_6F")
//			AND NOT IS_ARMCHAIR_TV_SEAT(iPlayerChosenSeatSlot)
				IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sOfficeSeatAnimDict, "exit")
				AND g_OfficeArmChairState != OFFICE_SEAT_PLAY_COMPUTER_IDLE
					SHOW_OFFICE_ARMCHAIR_EXIT_PROMPT()
				ENDIF
			ENDIF
		ENDIF
		
		CONTROL_ACTION playerControlAction 
		IF ENUM_TO_INT(g_OfficeArmChairState) > ENUM_TO_INT(OFFICE_SEAT_RUN_ENTER_ANIM)
		AND g_OfficeArmChairState != OFFICE_SEAT_CLEANUP
		
			
//			IF NOT IS_ARMCHAIR_TV_SEAT(iPlayerChosenSeatSlot)			
				IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
					playerControlAction = INPUT_SCRIPT_RRIGHT
				ELSE	
					playerControlAction = INPUT_FRONTEND_RIGHT
				ENDIF
			
			
			
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, playerControlAction)
				AND NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROP_OFF_M_6")
				AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROP_OFF_M_6F")
					
					iJacuzziNetSceneid = NETWORK_CREATE_SYNCHRONISED_SCENE(mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vLoc, mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vRot, DEFAULT, TRUE, FALSE)
					
				
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iJacuzziNetSceneid, sOfficeSeatAnimDict, "exit", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE ,  RBF_PLAYER_IMPACT,INSTANT_BLEND_IN)
							
					NETWORK_START_SYNCHRONISED_SCENE(iJacuzziNetSceneid)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "iJacuzziNetSceneid = ", iJacuzziNetSceneid)
					//IF IS_OFFICE_ARMCHAIR_EXIT_PROMPT_SHOWN()
		//				CLEAR_HELP()
						IF iOfficeSeatContextIntention != NEW_CONTEXT_INTENTION
							RELEASE_CONTEXT_INTENTION(iOfficeSeatContextIntention)
						ENDIF
					//ENDIF
					CLEAR_BIT(iOfficeSeatiBitSet, OFFICE_SEAT_BS_EXIT_HELP_TEXT_DISPLAYED)

					CDEBUG1LN(DEBUG_SAFEHOUSE, "exiting activity")
					SET_OFFICE_SEAT_STAGE(g_OfficeArmChairState, OFFICE_SEAT_CLEANUP_WAIT)

				ENDIF
//			ENDIF
//			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			// Grab the current analogue stick positions	
			INT iLeftX, iLeftY, iRightX, iRightY
			GET_CONTROL_VALUE_OF_ANALOGUE_STICKS( iLeftX, iLeftY, iRightX, iRightY)
			
			IF (iLeftX < -64
			OR iLeftX > 64)
			AND HAS_NET_TIMER_EXPIRED(ntOfficeArmchairChangePosInputTimer, 500, TRUE)
			AND NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, playerControlAction)
			AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROP_OFF_M_6")
			AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROP_OFF_M_6F")
//			AND NOT DONT89_IS_REMOTE_PLAYER_RUNNING_GAME(PLAYER_ID())
			AND CAN_PLAYER_CHANGE_POSITION(iPlayerChosenSeatSlot)
//			AND playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient.eStage != MP_TV_CLIENT_STAGE_ACTIVATED
			AND CAN_PED_CHANGE_POSE()
				REINIT_NET_TIMER(ntOfficeArmchairChangePosInputTimer, TRUE)
				
				iJacuzziNetSceneid = NETWORK_CREATE_SYNCHRONISED_SCENE(mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vLoc, mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vRot, DEFAULT, TRUE, FALSE)
				
				TEXT_LABEL_31 sAnimClip 
				sAnimClip = GET_TRANSITION_ANIM_CLIP(eAnimOfficeSeatVar, IS_PED_FEMALE(PLAYER_PED_ID()), IS_SIMPLE_CHAIR_SOFA(iPlayerChosenSeatSlot))
				CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_PLAYER_INTPUT_LOGIC_FOR_ARMCHAIR_INTERRUPT: sAnimClip : ", sAnimClip )
//				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iJacuzziNetSceneid, sOfficeSeatAnimDict, GET_TRANSITION_ANIM_CLIP(eAnimOfficeSeatVar, IS_PED_FEMALE(PLAYER_PED_ID())), SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE ,  RBF_PLAYER_IMPACT,INSTANT_BLEND_IN)
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iJacuzziNetSceneid, sOfficeSeatAnimDict, sAnimClip , SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE ,  RBF_PLAYER_IMPACT,INSTANT_BLEND_IN)
				
						
				NETWORK_START_SYNCHRONISED_SCENE(iJacuzziNetSceneid)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "iJacuzziNetSceneid = ", iJacuzziNetSceneid)
				//IF IS_OFFICE_ARMCHAIR_EXIT_PROMPT_SHOWN()
	//				CLEAR_HELP()
					//IF iOfficeSeatContextIntention != NEW_CONTEXT_INTENTION
						//RELEASE_CONTEXT_INTENTION(iOfficeSeatContextIntention)
					//ENDIF
				//ENDIF
				CLEAR_BIT(iOfficeSeatiBitSet, OFFICE_SEAT_BS_EXIT_HELP_TEXT_DISPLAYED)

				CDEBUG1LN(DEBUG_SAFEHOUSE, "transitioning activity")
				sOfficeSeatAnimDict = GET_OFFICE_SEAT_ARMCHAIR_ENTER_ANIM_DICT( GET_ANIM_VAR_TO_SWITCH_TO(eAnimOfficeSeatVar, IS_PED_FEMALE(PLAYER_PED_ID()), IS_SIMPLE_CHAIR_SOFA(iPlayerChosenSeatSlot)), IS_PLAYER_FEMALE())
				eAnimOfficeSeatVar = GET_ANIM_VAR_TO_SWITCH_TO(eAnimOfficeSeatVar, IS_PED_FEMALE(PLAYER_PED_ID()), IS_SIMPLE_CHAIR_SOFA(iPlayerChosenSeatSlot))
				SET_OFFICE_SEAT_STAGE(g_OfficeArmChairState, OFFICE_SEAT_CHECK_ENTER_OR_IDLE_ANIM)

			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			#ENDIF
		ENDIF
	ELSE
	
		IF IS_BROWSER_OPEN()
		OR IS_INTERACTION_MENU_OPEN()
			CDEBUG1LN(DEBUG_SAFEHOUSE, "g_bBrowserVisible = TRUE")	
			IF HAS_NET_TIMER_STARTED(stOfficeChairExitDelay)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "RESET_NET_TIMER(stOfficeChairExitDelay)	")	
				RESET_NET_TIMER(stOfficeChairExitDelay)	
			ENDIF
		ENDIF
		
		//IF IS_OFFICE_ARMCHAIR_EXIT_PROMPT_SHOWN()
//			CLEAR_HELP()
			IF iOfficeSeatContextIntention != NEW_CONTEXT_INTENTION
				RELEASE_CONTEXT_INTENTION(iOfficeSeatContextIntention)
			ENDIF
		//ENDIF
	ENDIF
		
ENDPROC

















//STRING sOfficeSeatAnimClip
//STRING sOutputOfficeSeatAnimDict
//BOOL bOfficeSeatBlockInterrupt
INT iIdleOfficeSeatClipVar 

PROC CHECK_FOR_OFFICE_SEAT_ARMCHAIR_ANIM()
	#IF IS_DEBUG_BUILD
	IF NOT IS_STRING_NULL_OR_EMPTY(sOfficeSeatAnimDict)
		IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sOfficeSeatAnimDict, "exit")

			#IF IS_DEBUG_BUILD
//			sOfficeSeatAnimClip = "exit"
//			sOutputOfficeSeatAnimDict = sOfficeSeatAnimDict
			#ENDIF
	//			CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_INTERRUPTABLE_INPUT: sOfficeSeatAnimDict: ", sOfficeSeatAnimDict, " Clip = exit")
		ENDIF
		IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sOfficeSeatAnimDict, "enter")
//			sOfficeSeatAnimClip = "enter"
			#IF IS_DEBUG_BUILD
//			sOutputOfficeSeatAnimDict = sOfficeSeatAnimDict
			#ENDIF
			
	//			CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_INTERRUPTABLE_INPUT: sOfficeSeatAnimDict: ", sOfficeSeatAnimDict, " Clip = enter")
		ENDIF
		IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sOfficeSeatAnimDict, "base")
//			sOfficeSeatAnimClip = "base"
			#IF IS_DEBUG_BUILD
//			sOutputOfficeSeatAnimDict = sOfficeSeatAnimDict
			#ENDIF
	//		CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_INTERRUPTABLE_INPUT: sOfficeSeatAnimDict: ", sOfficeSeatAnimDict, " Clip = base")
		ENDIF
		IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sOfficeSeatAnimDict, GET_OFFICE_SEAT_PED_IDLE_CLIP_STRING_FROM_INT(iIdleOfficeSeatClipVar))
//			sOfficeSeatAnimClip = GET_OFFICE_SEAT_PED_IDLE_CLIP_STRING_FROM_INT(iIdleOfficeSeatClipVar)
			#IF IS_DEBUG_BUILD
//			sOutputOfficeSeatAnimDict = sOfficeSeatAnimDict
			#ENDIF
	//		CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_INTERRUPTABLE_INPUT: sOfficeSeatAnimDict: ", sOfficeSeatAnimDict, " Clip = ", GET_OFFICE_SEAT_PED_IDLE_CLIP_STRING_FROM_INT(iIdleOfficeSeatClipVar))
		ENDIF	
	ENDIF
	#ENDIF
	
	IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("EXIT_INTERRUPT"))
		CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_INTERRUPTABLE_INPUT: EXIT_INTERRUPT")
		// Grab the current analogue stick positions	
		INT iLeftX, iLeftY, iRightX, iRightY
		GET_CONTROL_VALUE_OF_ANALOGUE_STICKS( iLeftX, iLeftY, iRightX, iRightY)
		IF iLeftX > 31
		OR iLeftY > 31
		OR iRightX > 31
		OR iRightY > 31
			CDEBUG1LN(DEBUG_SAFEHOUSE, "CHECK_FOR_OFFICE_SEAT_ANIM: EXIT_INTERRUPT tag true and input recieved")
			SET_OFFICE_SEAT_STAGE(g_OfficeArmChairState, OFFICE_SEAT_EXIT_INTERRUPT)
		ENDIF
	ENDIF
	IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("BLOCK_INTERRUPT"))

		CLEAR_ALL_OFFICE_SEAT_PROMPTS_GARAGE()
		
		#IF IS_DEBUG_BUILD
//		bOfficeSeatBlockInterrupt = TRUE
		#ENDIF
		
	ELSE
		
		OFFICE_SEAT_PLAYER_INTPUT_LOGIC_FOR_ARMCHAIR_INTERRUPT()
		#IF IS_DEBUG_BUILD
//		bOfficeSeatBlockInterrupt = FALSE
		#ENDIF
//		CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_INTERRUPTABLE_INPUT: Can take input, in case = ", officeSeatActStateString(g_OfficeArmChairState))
	ENDIF
	
ENDPROC


PROC MANAGE_INTERRUPTABLE_OFFICE_SEAT_ARMCHAIR_INPUT()
	INT localScene  
	localScene  = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iJacuzziNetSceneid)
	IF localScene != -1
	AND iPlayerChosenSeatSlot > -1
//		CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_INTERRUPTABLE_OFFICE_SEAT_ARMCHAIR_INPUT: iPlayerChosenSeatSlot: ", iPlayerChosenSeatSlot)
		CHECK_FOR_OFFICE_SEAT_ARMCHAIR_ANIM()
	

	ENDIF
ENDPROC

//INT iStaggerOfficeiSeatLoop = 0
INT iStaggerOfficeArmChairiSeatLoop = 0
CONST_INT SERVER_STAGE_INIT			0
CONST_INT SERVER_STAGE_RUNNING		1

ACTIVITY_ENUMS eChosenOfficeSeatSlotActivity

//ACTIVITY_ENUMS activeActivity = ACTIVITY_NONE


FUNC BOOL IS_ARMCHAIR_SOFA_SEAT(INT iSeatID)
	SWITCH iSeatID
		CASE 3
		CASE 4
		CASE 5
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC 

//OFFICE_CREW_SEAT_ORIENTATION ePlayerOfficeSeatOrientation = FACE_CENTER
//OFFICE_SEAT_ANIM_VARIATION eOfficePlayerSeatAnimVar = OFFICE_CREW_VAR_UNASSIGNED

PROC REMOVE_INCOMPATIBLE_CLOTHES()
	IF IS_PED_WEARING_PARACHUTE_MP(PLAYER_PED_ID())
		CDEBUG1LN(DEBUG_SAFEHOUSE, "REMOVE_INCOMPATIBLE_CLOTHES: Player is wearing a parachute, removing")
		SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 0, 0)
	ENDIF
ENDPROC
BOOL bOfficeSeatTurning
INT iSecondLastJacuzzibaseClipVar
INT iLastJacuzzibaseClipVar
FUNC INT GENERATE_RANDOM_OFFICE_SEAT_ARMCHAIR_IDLE_CLIP()
	INT iClipVar 
	INT iMaxValue
	
	iMaxValue = 3
	
	
	iClipVar = GET_RANDOM_INT_IN_RANGE(0, iMaxValue)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_RANDOM_OFFICE_SEAT_ARMCHAIR_IDLE_CLIP: iLastJacuzzibaseClipVar = ", iLastJacuzzibaseClipVar)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_RANDOM_OFFICE_SEAT_ARMCHAIR_IDLE_CLIP: iSecondLastJacuzzibaseClipVar = ", iSecondLastJacuzzibaseClipVar)
	WHILE iClipVar = iLastJacuzzibaseClipVar
	OR iClipVar = iSecondLastJacuzzibaseClipVar
//	OR (NOT SHOULD_PLAYER_PLAY_ANIM_CLIP(iPlayerChosenSeatSlot, iClipVar))
//	OR (IS_OFFICE_SEAT_BOARDROOM_BOSS(iPlayerChosenSeatSlot) 
//		AND NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_REVOLVER)
//		AND iClipVar = 1)
		
		CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_RANDOM_OFFICE_SEAT_ARMCHAIR_IDLE_CLIP: iClip was unsuitable: ", iClipVar, ", so regenerating")
		iClipVar = GET_RANDOM_INT_IN_RANGE(0, iMaxValue)
	ENDWHILE
		
		
	CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_RANDOM_OFFICE_SEAT_ARMCHAIR_IDLE_CLIP: using iClip : ", iClipVar)	
	iSecondLastJacuzzibaseClipVar = iLastJacuzzibaseClipVar
	iLastJacuzzibaseClipVar = iClipVar
	RETURN iClipVar 
ENDFUNC

OBJECT_INDEX objOfficeSeat
//OFFICE_SEAT_ACT_STATE officeSeatActState = OFFICE_SEAT_INIT

PROC SHOW_ARMCHAIR_SEAT_IDS_GARAGE()
	#IF IS_DEBUG_BUILD
	INT iRed, iGreen, iBlue, iAlpha
	TEXT_LABEL_63 str
	INT iSeat
	
	FLOAT fScreenX 		
	FLOAT fScreenY
	
	REPEAT NUM_SEAT_IN_OFFICE iSeat
		str += "SeatID: "
		str += GET_STRING_FROM_INT(iSeat)
		
//		CDEBUG1LN(DEBUG_SAFEHOUSE, "mpOffsetOfficeSeats[iSeat].vLoc = ", mpOffsetOfficeSeats[iSeat].vLoc, ", fScreenX: ", fScreenX, ", fScreenY: ", fScreenY)
		
		GET_SCREEN_COORD_FROM_WORLD_COORD(mpOffsetOfficeSeats[iSeat].vLoc+<<0,0, 1.5>>, fScreenX, fScreenY)
		
//		SET_TEXT_SCALE(0.0000, CUSTOM_MENU_TEXT_SCALE_Y)
//		SET_TEXT_COLOUR(255, 165, 0, 255)
//		SET_TEXT_CENTRE(TRUE)
//		BEGIN_TEXT_COMMAND_DISPLAY_TEXT("STRING") 
//		END_TEXT_COMMAND_DISPLAY_TEXT(fScreenX, fScreenY) 
		
		GET_HUD_COLOUR(INT_TO_ENUM(HUD_COLOURS, 0), iRed, iGreen, iBlue, iAlpha)
		DRAW_DEBUG_TEXT_2D(str, <<fScreenX, fScreenY, 0>>, iRed, iGreen, iBlue, iAlpha)
		iGreen = 0
		iRed = 0
		str = ""
	ENDREPEAT
	#ENDIF
ENDPROC

PROC DEBUG_OFFICE_CHAIR_DISPLAY_GARAGE()
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_DrawDebugOfficeSeatStuffAudio")
//		INT iStrOffset = 0, iColumn = 0, iRow = 0
//		INT iRed, iGreen, iBlue, iAlpha
//		TEXT_LABEL_63 str
		
		SHOW_ARMCHAIR_SEAT_IDS_GARAGE()
		
//		IF ENUM_TO_INT(officeSeatActState) > ENUM_TO_INT(OFFICE_SEAT_APPROACH)
//		OR ENUM_TO_INT(g_OfficeArmChairState) > ENUM_TO_INT(OFFICE_SEAT_APPROACH)
//			INT localScene  
//			localScene  = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iJacuzziNetSceneid)
//			IF localScene != -1
//				str += "OfficeSeatState: "
//				str += officeSeatActStateString(officeSeatActState)
//				
//				GET_HUD_COLOUR(INT_TO_ENUM(HUD_COLOURS, iStrOffset), iRed, iGreen, iBlue, iAlpha)
//				DRAW_DEBUG_TEXT_2D(str, <<0.3,0.2,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)+(<<0.0,fYvar,0>>*TO_FLOAT(iRow))), iRed, iGreen, iBlue, iAlpha)
//				iGreen = 0
//				iRed = 0
//				str = ""
//				iRow++
//				
//				str = "iPlayerChosenSeatSlot: "
//				str += iPlayerChosenSeatSlot
//				
//				GET_HUD_COLOUR(INT_TO_ENUM(HUD_COLOURS, iStrOffset), iRed, iGreen, iBlue, iAlpha)
//				DRAW_DEBUG_TEXT_2D(str, <<0.3,0.2,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)+(<<0.0,fYvar,0>>*TO_FLOAT(iRow))), iRed, iGreen, iBlue, iAlpha)
//				iGreen = 0
//				iRed = 0
//				str = ""
//				iRow++
//				
//				
//				IF bOfficeSeatBlockInterrupt
//					str += "BLOCKING_INTERRUPT"
//					GET_HUD_COLOUR(HUD_COLOUR_RED, iRed, iGreen, iBlue, iAlpha)
//				ELSE
//					str += "ACCEPTING_INTERRUPT"
//					GET_HUD_COLOUR(HUD_COLOUR_GREEN, iRed, iGreen, iBlue, iAlpha)
//				ENDIF
//				
//				DRAW_DEBUG_TEXT_2D(str, <<0.3,0.2,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)+(<<0.0,fYvar,0>>*TO_FLOAT(iRow))), iRed, iGreen, iBlue, iAlpha)
//				iGreen = 0
//				iRed = 0
//				str = ""
//				iRow++
//				
//				str += "Dict= "
//				str += sOutputOfficeSeatAnimDict
//				
//				GET_HUD_COLOUR(INT_TO_ENUM(HUD_COLOURS, iStrOffset), iRed, iGreen, iBlue, iAlpha)
//				DRAW_DEBUG_TEXT_2D(str, <<0.3,0.2,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)+(<<0.0,fYvar,0>>*TO_FLOAT(iRow))), iRed, iGreen, iBlue, iAlpha)
//				
//				str = ""
//				iRow++
//				
//				str += "Clip= "
//				str += sOfficeSeatAnimClip
//				GET_HUD_COLOUR(INT_TO_ENUM(HUD_COLOURS, iStrOffset), iRed, iGreen, iBlue, iAlpha)
//				DRAW_DEBUG_TEXT_2D(str, <<0.3,0.2,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)+(<<0.0,fYvar,0>>*TO_FLOAT(iRow))), iRed, iGreen, iBlue, iAlpha)
//				
//				str = ""
//				iRow++
//				
//				str += "Phase= "
//				str += GET_STRING_FROM_FLOAT(GET_SYNCHRONIZED_SCENE_PHASE(localScene))
//				GET_HUD_COLOUR(INT_TO_ENUM(HUD_COLOURS, iStrOffset), iRed, iGreen, iBlue, iAlpha)
//				DRAW_DEBUG_TEXT_2D(str, <<0.3,0.2,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)+(<<0.0,fYvar,0>>*TO_FLOAT(iRow))), iRed, iGreen, iBlue, iAlpha)
//				str = ""
//				iRow++
//				IF CAN_SEAT_SWIVEL(iPlayerChosenSeatSlot)
//				AND warpInControl.propertyOwner != PLAYER_ID()
//					str += "HeadTracking= "
//					GET_HUD_COLOUR(INT_TO_ENUM(HUD_COLOURS, iStrOffset), iRed, iGreen, iBlue, iAlpha)
//					DRAW_DEBUG_TEXT_2D(str, <<0.3,0.2,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)+(<<0.0,fYvar,0>>*TO_FLOAT(iRow))), iRed, iGreen, iBlue, iAlpha)
//					str = ""
//					iRow++
//				ENDIF
//				
//				IF playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient.eStage > MP_TV_CLIENT_STAGE_WALKING	
//				AND IS_ARMCHAIR_TV_SEAT(iPlayerChosenSeatSlot)
//					IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niOfficeSetTopRemote)
//						str += "REMOTE DOESN'T EXIST"
//						GET_HUD_COLOUR(HUD_COLOUR_RED, iRed, iGreen, iBlue, iAlpha)
//					ELSE
//						str += "REMOTE EXIST"
//						GET_HUD_COLOUR(HUD_COLOUR_GREEN, iRed, iGreen, iBlue, iAlpha)
//					ENDIF
//					
//					DRAW_DEBUG_TEXT_2D(str, <<0.3,0.2,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)+(<<0.0,fYvar,0>>*TO_FLOAT(iRow))), iRed, iGreen, iBlue, iAlpha)
//					iGreen = 0
//					iRed = 0
//					str = ""
//					iRow++
//					
//					
//					
//					
////					IF NOT HAS_ANIM_DICT_LOADED(GET_OFFICE_SEAT_GAME_ARMCHAIR_ENTER_ANIM_DICT(SWITCH_TO_GAME_ANIM_VAR(iPlayerChosenSeatSlot), IS_PLAYER_FEMALE()))
////						str += "GAME ANIM DICT NOT LOADED: "
////						str += GET_OFFICE_SEAT_GAME_ARMCHAIR_ENTER_ANIM_DICT(SWITCH_TO_GAME_ANIM_VAR(iPlayerChosenSeatSlot), IS_PLAYER_FEMALE())
////						GET_HUD_COLOUR(HUD_COLOUR_RED, iRed, iGreen, iBlue, iAlpha)
////					ELSE
////						str += "GAME ANIM DICT LOADED: "
////						str += GET_OFFICE_SEAT_GAME_ARMCHAIR_ENTER_ANIM_DICT(SWITCH_TO_GAME_ANIM_VAR(iPlayerChosenSeatSlot), IS_PLAYER_FEMALE())
////						GET_HUD_COLOUR(HUD_COLOUR_GREEN, iRed, iGreen, iBlue, iAlpha)
////					ENDIF
////					
////					DRAW_DEBUG_TEXT_2D(str, <<0.3,0.2,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)+(<<0.0,fYvar,0>>*TO_FLOAT(iRow))), iRed, iGreen, iBlue, iAlpha)
////					iGreen = 0
////					iRed = 0
////					str = ""
////					iRow++
//				ENDIF
//				
//				IF CAN_SEAT_SWIVEL(iPlayerChosenSeatSlot)
//				AND playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient.eStage <= MP_TV_CLIENT_STAGE_WALKING	
//					str += "Facing= "
//					str += GET_OFFICE_SEAT_CREW_ORIENTATION_STRING()
//					GET_HUD_COLOUR(INT_TO_ENUM(HUD_COLOURS, iStrOffset), iRed, iGreen, iBlue, iAlpha)
//					DRAW_DEBUG_TEXT_2D(str, <<0.3,0.2,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)+(<<0.0,fYvar,0>>*TO_FLOAT(iRow))), iRed, iGreen, iBlue, iAlpha)
//					str = ""
//					iRow++
//				ENDIF
//				IF IS_OFFICE_SEAT_BOARDROOM_CREW(iPlayerChosenSeatSlot)
//				AND playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient.eStage <= MP_TV_CLIENT_STAGE_WALKING	
//					
//					localScene  = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iJacuzziNetSceneid)
//					IF localScene != -1
//						str += "BEGIN_INHALE"
//						IF HAS_ANIM_EVENT_FIRED (PLAYER_PED_ID(), HASH("BEGIN_INHALE")) 
//							
//							GET_HUD_COLOUR(HUD_COLOUR_GREEN, iRed, iGreen, iBlue, iAlpha)
//							DRAW_DEBUG_TEXT_2D(str, <<0.3,0.2,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)+(<<0.0,fYvar,0>>*TO_FLOAT(iRow))), iRed, iGreen, iBlue, iAlpha)
//						ELSE
//							
//							GET_HUD_COLOUR( HUD_COLOUR_RED, iRed, iGreen, iBlue, iAlpha)
//							DRAW_DEBUG_TEXT_2D(str, <<0.3,0.2,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)+(<<0.0,fYvar,0>>*TO_FLOAT(iRow))), iRed, iGreen, iBlue, iAlpha)
//						ENDIF
//						
//						iGreen = 0
//						iRed = 0
//						str = ""
//						iRow++
//						
//						str += "BEGIN_EXHALE"
//						IF HAS_ANIM_EVENT_FIRED (PLAYER_PED_ID(), HASH("BEGIN_EXHALE")) 
//							
//							GET_HUD_COLOUR(HUD_COLOUR_GREEN, iRed, iGreen, iBlue, iAlpha)
//							DRAW_DEBUG_TEXT_2D(str, <<0.3,0.2,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)+(<<0.0,fYvar,0>>*TO_FLOAT(iRow))), iRed, iGreen, iBlue, iAlpha)
//						ELSE
//							
//							GET_HUD_COLOUR(HUD_COLOUR_RED, iRed, iGreen, iBlue, iAlpha)
//							DRAW_DEBUG_TEXT_2D(str, <<0.3,0.2,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)+(<<0.0,fYvar,0>>*TO_FLOAT(iRow))), iRed, iGreen, iBlue, iAlpha)
//						ENDIF
//						
//						iGreen = 0
//						iRed = 0
//						str = ""
//						iRow++
//					ENDIF
//				ENDIF
//				
//				
//				IF DOES_ENTITY_EXIST(objOfficeSeat)
//				AND (ENUM_TO_INT(officeSeatActState) > ENUM_TO_INT(OFFICE_SEAT_PROMPT)
//				OR ENUM_TO_INT(g_OfficeArmChairState) > ENUM_TO_INT(OFFICE_SEAT_PROMPT))
//					IF NOT IS_STRING_NULL(GET_CENTER_BASE_ANIM_GARAGE())
//					
//						REQUEST_ANIM_DICT(GET_CENTER_BASE_ANIM_GARAGE())
//					
//						IF HAS_ANIM_DICT_LOADED(GET_CENTER_BASE_ANIM_GARAGE())
//							VECTOR vEndChairPos 
//							vEndChairPos = GET_ANIM_INITIAL_OFFSET_POSITION(GET_CENTER_BASE_ANIM_GARAGE(), "exit", GET_ENTITY_COORDS(objOfficeSeat), GET_ENTITY_ROTATION(objOfficeSeat), 1)  
//		//					VECTOR vTempRotation 
//		//					vTempRotation = GET_ANIM_INITIAL_OFFSET_ROTATION(sOfficeSeatAnimDict, "exit", GET_MP_JACUZZI_SEAT_CHAIR_POSITION(iPlayerChosenSeatSlot, MPJacuzziLocal), GET_MP_JACUZZI_SEAT_CHAIR_ANIM_ROTATION(iPlayerChosenSeatSlot, MPJacuzziLocal), 1)  
//						
//						
//							str += "End Chair Pos: "
//							str += GET_STRING_FROM_VECTOR(vEndChairPos)
//							
//							IF ARE_VECTORS_EQUAL(vEndChairPos, MPJacuzziLocal.vInitalSeatPos )
//								
//								GET_HUD_COLOUR(HUD_COLOUR_GREEN, iRed, iGreen, iBlue, iAlpha)
//								DRAW_DEBUG_TEXT_2D(str, <<0.3,0.2,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)+(<<0.0,fYvar,0>>*TO_FLOAT(iRow))), iRed, iGreen, iBlue, iAlpha)
//							ELSE
//								
//								GET_HUD_COLOUR(HUD_COLOUR_RED, iRed, iGreen, iBlue, iAlpha)
//								DRAW_DEBUG_TEXT_2D(str, <<0.3,0.2,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)+(<<0.0,fYvar,0>>*TO_FLOAT(iRow))), iRed, iGreen, iBlue, iAlpha)
//							ENDIF
//							iGreen = 0
//							iRed = 0
//							str = ""
//							iRow++
//							
//							IF NOT ARE_VECTORS_EQUAL(vEndChairPos, MPJacuzziLocal.vInitalSeatPos )						
//								str  += "Offset: "
//								VECTOR vDiff 
//								vDiff = vEndChairPos - MPJacuzziLocal.vInitalSeatPos
//								str  += GET_STRING_FROM_VECTOR(vDiff )
//								GET_HUD_COLOUR(INT_TO_ENUM(HUD_COLOURS, iStrOffset), iRed, iGreen, iBlue, iAlpha)
//								DRAW_DEBUG_TEXT_2D(str, <<0.3,0.2,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)+(<<0.0,fYvar,0>>*TO_FLOAT(iRow))), iRed, iGreen, iBlue, iAlpha)
//								iGreen = 0
//								iRed = 0
//								str = ""
//								iRow++
//							ENDIF
//							
//							
//							str += "Original Chair Pos: "
//							str += GET_STRING_FROM_VECTOR(MPJacuzziLocal.vInitalSeatPos)
//								
//							GET_HUD_COLOUR(INT_TO_ENUM(HUD_COLOURS, iStrOffset), iRed, iGreen, iBlue, iAlpha)
//							DRAW_DEBUG_TEXT_2D(str, <<0.3,0.2,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)+(<<0.0,fYvar,0>>*TO_FLOAT(iRow))), iRed, iGreen, iBlue, iAlpha)
//							
//							iGreen = 0
//							iRed = 0
//							str = ""
//							iRow++
//						ENDIF
//					ENDIF
//					
//					
//					
//				ENDIF
//				
//				IF HAS_NET_TIMER_STARTED(stOfficeActivityCountDown)
//				
////					(ABSI(GET_TIME_DIFFERENCE(INT_TO_NATIVE(TIME_DATATYPE, GET_GAME_TIMER()), stActivityCountDown.Timer)) >= TimeToExpire)
//				
//					str += "iOfficeActivityCountDown= " 
//					
//					str += GET_STRING_FROM_INT(iOfficeActivityCountDown)
//					GET_HUD_COLOUR(INT_TO_ENUM(HUD_COLOURS, iStrOffset), iRed, iGreen, iBlue, iAlpha)
//					DRAW_DEBUG_TEXT_2D(str, <<0.3,0.2,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)+(<<0.0,fYvar,0>>*TO_FLOAT(iRow))), iRed, iGreen, iBlue, iAlpha)
//					str = ""
//					iRow++
//				
//					str += "ActivityCountDown= "
//					INT iCountDown 
//					IF iOfficeActivityCountDown - (ABSI(GET_TIME_DIFFERENCE(INT_TO_NATIVE(TIME_DATATYPE, GET_GAME_TIMER()), stOfficeActivityCountDown.Timer))) > 0
//						iCountDown = iOfficeActivityCountDown - (ABSI(GET_TIME_DIFFERENCE(INT_TO_NATIVE(TIME_DATATYPE, GET_GAME_TIMER()), stOfficeActivityCountDown.Timer)))
//					ENDIF
//					str += GET_STRING_FROM_INT(iCountDown)
//					GET_HUD_COLOUR(INT_TO_ENUM(HUD_COLOURS, iStrOffset), iRed, iGreen, iBlue, iAlpha)
//					DRAW_DEBUG_TEXT_2D(str, <<0.3,0.2,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)+(<<0.0,fYvar,0>>*TO_FLOAT(iRow))), iRed, iGreen, iBlue, iAlpha)
//					str = ""
//					iRow++
//					
//					str += "ActivityCountDownExpired= "
//					IF HAS_NET_TIMER_EXPIRED(stOfficeActivityCountDown, iOfficeActivityCountDown, TRUE)
//						str += "TRUE "
//					ELSE
//						str += "FALSE"
//					ENDIF
//					GET_HUD_COLOUR(INT_TO_ENUM(HUD_COLOURS, iStrOffset), iRed, iGreen, iBlue, iAlpha)
//					DRAW_DEBUG_TEXT_2D(str, <<0.3,0.2,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)+(<<0.0,fYvar,0>>*TO_FLOAT(iRow))), iRed, iGreen, iBlue, iAlpha)
//					str = ""
//					iRow++
//				ENDIF
//				
//				IF IS_OFFICE_SEAT_BOARDROOM_CREW(iPlayerChosenSeatSlot)
//				AND playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient.eStage <= MP_TV_CLIENT_STAGE_WALKING	
//					str += "bWhiskyUsed= "
//					IF serverBD.jacuzziServerSeatData[iPlayerChosenSeatSlot].bWhiskyUsed = TRUE
//						str += "TRUE "
//						
//						GET_HUD_COLOUR(HUD_COLOUR_GREEN, iRed, iGreen, iBlue, iAlpha)
//						
//					ELSE
//						str += "FALSE"
//						GET_HUD_COLOUR(HUD_COLOUR_RED, iRed, iGreen, iBlue, iAlpha)
//					ENDIF
//					
//					DRAW_DEBUG_TEXT_2D(str, <<0.3,0.2,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)+(<<0.0,fYvar,0>>*TO_FLOAT(iRow))), iRed, iGreen, iBlue, iAlpha)
//					str = ""
//					iRow++
//				ENDIF
//				
//			ENDIF
//		ENDIF
		
		
//		IF bOfficeAudioWidget = TRUE
//		OR GET_COMMANDLINE_PARAM_EXISTS("sc_DrawDebugOfficeSeatStuffAudio")
//			str += "Audio: "
//			GET_HUD_COLOUR(INT_TO_ENUM(HUD_COLOURS, iStrOffset), iRed, iGreen, iBlue, iAlpha)
//			DRAW_DEBUG_TEXT_2D(str, <<0.3,0.2,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)+(<<0.0,fYvar,0>>*TO_FLOAT(iRow))), iRed, iGreen, iBlue, iAlpha)
//			iGreen = 0
//			iRed = 0
//			str = ""
//			iRow++
//			
//			str += "bAudioBankHinted: "
//			IF bAudioBankHinted
//				str += "TRUE"
//			ELSE
//				str += "FALSE"
//			ENDIF
//			
//			GET_HUD_COLOUR(INT_TO_ENUM(HUD_COLOURS, iStrOffset), iRed, iGreen, iBlue, iAlpha)
//			DRAW_DEBUG_TEXT_2D(str, <<0.3,0.2,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)+(<<0.0,fYvar,0>>*TO_FLOAT(iRow))), iRed, iGreen, iBlue, iAlpha)
//			
//			iGreen = 0
//			iRed = 0
//			str = ""
//			iRow++
//			
//			str += "DistanceFromBoardroom: "
//			
//			MP_PROP_OFFSET_STRUCT boardRoomLocation
//			GET_POSITION_AS_OFFSET_FOR_PROPERTY(iCurrentProperty, MP_PROP_ELEMENT_OFFICE_BOARDROOM_LOCATION, boardRoomLocation, GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty))
//			
//			str += FLOAT_TO_STRING(GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), boardRoomLocation.vLoc) )
//			
//			
//			GET_HUD_COLOUR(INT_TO_ENUM(HUD_COLOURS, iStrOffset), iRed, iGreen, iBlue, iAlpha)
//			DRAW_DEBUG_TEXT_2D(str, <<0.3,0.2,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)+(<<0.0,fYvar,0>>*TO_FLOAT(iRow))), iRed, iGreen, iBlue, iAlpha)
//			
//			iGreen = 0
//			iRed = 0
//			str = ""
//			iRow++
//			
//			str += "bAudioBankRequested: "
//			IF bAudioBankRequested
//				str += "TRUE"
//			ELSE
//				IF bAudioBankLoading
//					str += "LOADING"
//				ELSE
//					str += "FALSE"
//				ENDIF
//				
//			ENDIF
//			
//			GET_HUD_COLOUR(INT_TO_ENUM(HUD_COLOURS, iStrOffset), iRed, iGreen, iBlue, iAlpha)
//			DRAW_DEBUG_TEXT_2D(str, <<0.3,0.2,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)+(<<0.0,fYvar,0>>*TO_FLOAT(iRow))), iRed, iGreen, iBlue, iAlpha)
//			
//			iGreen = 0
//			iRed = 0
//			str = ""
//			iRow++
//			
//			str += "iPlayerChosenSeatSlot: "
//			
//			str += iPlayerChosenSeatSlot
//			
//			
//			GET_HUD_COLOUR(INT_TO_ENUM(HUD_COLOURS, iStrOffset), iRed, iGreen, iBlue, iAlpha)
//			DRAW_DEBUG_TEXT_2D(str, <<0.3,0.2,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)+(<<0.0,fYvar,0>>*TO_FLOAT(iRow))), iRed, iGreen, iBlue, iAlpha)
//		ENDIF
		
//		DISPLAY_NETWORK_APT_PROP_INFO(iRow, iColumn)
		
	ENDIF
	#ENDIF
ENDPROC
PROC SHOW_OFFICE_SEAT_START_PROMPT_GARAGE(BOOL bTVSeat = FALSE)
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPJAC_NO_SIT")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPOFFCH_NO_SIT")
		RELEASE_CONTEXT_INTENTION(iOfficeSeatContextIntention)
		CLEAR_HELP()
	ENDIF
	
	IF iOfficeSeatContextIntention = NEW_CONTEXT_INTENTION
		IF bTVSeat
		AND IS_PROPERTY_OFFICE(iCurrentProperty)
			REGISTER_CONTEXT_INTENTION(iOfficeSeatContextIntention, CP_HIGH_PRIORITY, "MPTV_WALKOFF")
		ELSE
			REGISTER_CONTEXT_INTENTION(iOfficeSeatContextIntention, CP_HIGH_PRIORITY, "MPJAC_SIT")
		ENDIF
	ELSE
//		CDEBUG1LN(DEBUG_SAFEHOUSE, "SHOW_OFFICE_SEAT_START_PROMPT_GARAGE: iOfficeSeatContextIntention = ", iOfficeSeatContextIntention)
	ENDIF
//	PRINT_HELP_FOREVER("MPJAC_SIT")
ENDPROC
OFFICE_SEAT_ANIM_VARIATION eOfficePlayerSeatAnimVar = OFFICE_CREW_VAR_UNASSIGNED

FUNC STRING GET_CENTER_BASE_ANIM_GARAGE()
	STRING sReturnAnim
//	IF IS_OFFICE_SEAT_COMPUTER_BOSS(iPlayerChosenSeatSlot)	
//		sReturnAnim = GET_OFFICE_SEAT_ENTER_ANIM_DICT(iPlayerChosenSeatSlot, IS_PLAYER_FEMALE())
//	ELIF CAN_SEAT_SWIVEL(iPlayerChosenSeatSlot)
//		IF IS_PROPERTY_OFFICE(iCurrentProperty)
//			SWITCH eOfficePlayerSeatAnimVar
//				CASE OFFICE_CREW_VAR_UNASSIGNED
//					CWARNINGLN(DEBUG_SAFEHOUSE, "GET_CENTER_BASE_ANIM_GARAGE: eOfficePlayerSeatAnimVar = OFFICE_CREW_VAR_UNASSIGNED")
//				BREAK
//				CASE OFFICE_CREW_VAR_A
//					IF IS_PLAYER_FEMALE()
//						sReturnAnim =  "ANIM@AMB@OFFICE@BOARDROOM@CREW@FEMALE@VAR_A@BASE@"
//					ELSE
//						sReturnAnim =  "ANIM@AMB@OFFICE@BOARDROOM@CREW@MALE@VAR_A@BASE@"
//					ENDIF
//				BREAK
//				CASE OFFICE_CREW_VAR_B
//					IF IS_PLAYER_FEMALE()
//						sReturnAnim =  "ANIM@AMB@OFFICE@BOARDROOM@CREW@FEMALE@VAR_b@BASE@"
//					ELSE
//						sReturnAnim =  "ANIM@AMB@OFFICE@BOARDROOM@CREW@MALE@VAR_b@BASE@"
//					ENDIF
//				BREAK
//				CASE OFFICE_CREW_VAR_C
//					IF IS_PLAYER_FEMALE()
//						sReturnAnim =  "ANIM@AMB@OFFICE@BOARDROOM@CREW@FEMALE@VAR_C@BASE@"
//					ELSE
//						sReturnAnim =  "ANIM@AMB@OFFICE@BOARDROOM@CREW@MALE@VAR_C@BASE@"
//					ENDIF
//				BREAK
//			ENDSWITCH
//		#IF FEATURE_BIKER
//		ELIF IS_PROPERTY_CLUBHOUSE(iCurrentProperty)
			IF IS_OFFICE_SEAT_BOARDROOM_BOSS(iPlayerChosenSeatSlot)
				IF IS_PLAYER_FEMALE()
					sReturnAnim =  "ANIM@AMB@CLUBHOUSE@BOARDROOM@BOSS@FEMALE@BASE@"
				ELSE
					sReturnAnim =  "ANIM@AMB@CLUBHOUSE@BOARDROOM@BOSS@MALE@BASE@"
				ENDIF
			ELSE
				SWITCH eOfficePlayerSeatAnimVar
					CASE OFFICE_CREW_VAR_UNASSIGNED
						CWARNINGLN(DEBUG_SAFEHOUSE, "GET_CENTER_BASE_ANIM_GARAGE: eOfficePlayerSeatAnimVar = OFFICE_CREW_VAR_UNASSIGNED")
					BREAK
					CASE OFFICE_CREW_VAR_A
						IF IS_PLAYER_FEMALE()
							sReturnAnim =  "ANIM@AMB@CLUBHOUSE@BOARDROOM@CREW@FEMALE@VAR_A@BASE@"
						ELSE
							sReturnAnim =  "ANIM@AMB@CLUBHOUSE@BOARDROOM@CREW@MALE@VAR_A@BASE@"
						ENDIF
					BREAK
					CASE OFFICE_CREW_VAR_B
						IF IS_PLAYER_FEMALE()
							sReturnAnim =  "ANIM@AMB@CLUBHOUSE@BOARDROOM@CREW@FEMALE@VAR_B@BASE@"
						ELSE
							sReturnAnim =  "ANIM@AMB@CLUBHOUSE@BOARDROOM@CREW@MALE@VAR_B@BASE@"
						ENDIF
					BREAK
					CASE OFFICE_CREW_VAR_C
						IF IS_PLAYER_FEMALE()
							sReturnAnim =  "ANIM@AMB@CLUBHOUSE@BOARDROOM@CREW@FEMALE@VAR_C@BASE@"
						ELSE
							sReturnAnim =  "ANIM@AMB@CLUBHOUSE@BOARDROOM@CREW@MALE@VAR_C@BASE@"
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
//		#ENDIF
//		ENDIF
//	ENDIF
//	CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_CENTER_BASE_ANIM_GARAGE: ePlayerOfficeSeatOrientation = ", ENUM_TO_INT(ePlayerOfficeSeatOrientation) ,", sReturnAnim = ", sReturnAnim)
	RETURN sReturnAnim 
ENDFUNC




PROC RUN_IGNORE_COLLISION_FOR_POP_LOGIC()
	VECTOR CurrentSeatPos
	VECTOR CurrentSeatRot
	CurrentSeatPos = mpOffsetOfficeSeats[iStaggerOfficeArmChairiSeatLoop].vLoc
	CurrentSeatRot = mpOffsetOfficeSeats[iStaggerOfficeArmChairiSeatLoop].vRot
//			CurrentSeatPos.z += 1

	vOfficeSeatArmchairBoundingBox1 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(CurrentSeatPos,  CurrentSeatRot.Z, dbvOfficeSeatArmchairBoundingBox1) //<<0.393104,0.603146,-0.627219>>)
//
	vOfficeSeatArmchairBoundingBox2 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(CurrentSeatPos,  CurrentSeatRot.Z, dbvOfficeSeatArmchairBoundingBox2) //<<-0.441553,0.563528,1.202967>>)
	
	IF IS_PROPERTY_CLUBHOUSE(iCurrentProperty, PROPERTY_CLUBHOUSE_7_BASE_B)
		IF iStaggerOfficeArmChairiSeatLoop = 6
			vOfficeSeatArmchairBoundingBox1 = <<1002.847351,-3169.606445,-35.046421>>
			 
			vOfficeSeatArmchairBoundingBox2 = <<1001.693970,-3169.619141,-33.296421>> 
		ENDIF
	ENDIF
	
//			GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(mpOffsetOfficeSeats[iStaggerOfficeArmChairiSeatLoop].vLoc, mpOffsetOfficeSeats[iStaggerOfficeArmChairiSeatLoop].vRot, )
		
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vOfficeSeatArmchairBoundingBox1, vOfficeSeatArmchairBoundingBox2, GET_SEAT_LOCATE_WIDTH(iStaggerOfficeArmChairiSeatLoop))
		CDEBUG1LN(DEBUG_SAFEHOUSE, "RUN_IGNORE_COLLISION_FOR_POP_LOGIC: SET_PED_CAPSULE: 0.15")
		SET_PED_CAPSULE(PLAYER_PED_ID(), 0.15)
	ENDIF
	
ENDPROC

PROC MANAGE_CAMERA_COLLISION_ISSUE_FOR_SOFA()
	SWITCH g_OfficeArmChairState			
		CASE OFFICE_SEAT_APPROACH	
		CASE OFFICE_SEAT_RUN_ENTER_ANIM
			
			SET_PED_CAPSULE(PLAYER_PED_ID(), 0.15)
		BREAK
		CASE OFFICE_SEAT_CHECK_ENTER_OR_IDLE_ANIM           
		CASE OFFICE_SEAT_CHECK_BASE_ANIM 
		CASE OFFICE_SEAT_EXIT_TURN
		CASE OFFICE_SEAT_ENTER_TURN
		CASE OFFICE_SEAT_PLAY_COMPUTER_ENTER
		CASE OFFICE_SEAT_PLAY_COMPUTER_IDLE
		CASE OFFICE_SEAT_PLAY_COMPUTER_EXIT
		CASE OFFICE_SEAT_CLEANUP
//			SET_PED_CAPSULE(PLAYER_PED_ID(), 1)
			SET_GAMEPLAY_CAM_IGNORE_ENTITY_COLLISION_THIS_UPDATE(PLAYER_PED_ID())	
//			IF NOT IS_ENTITY_DEAD(ioCrateHeadCollision)
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_CAMERA_COLLISION_ISSUE_FOR_SOFA: SET_GAMEPLAY_CAM_IGNORE_ENTITY_COLLISION_THIS_UPDATE:ioCrateHeadCollision ")
//				SET_GAMEPLAY_CAM_IGNORE_ENTITY_COLLISION_THIS_UPDATE(ioCrateHeadCollision)
//			ENDIF
		BREAK                       	

	ENDSWITCH
ENDPROC 

PROC RUN_OFFICE_SEAT_ARMCHAIR_ACTIVITY()
	eChosenOfficeSeatSlotActivity = eChosenOfficeSeatSlotActivity
//	#IF IS_DEBUG_BUILD
	MANAGE_SEAT_NODE_POSITIONS()
	RUN_FEET_COLLISION_LOGIC()
	RUN_IGNORE_COLLISION_FOR_POP_LOGIC()
//	CREATE_HEAD_COLLISION_PROP()

	#IF IS_DEBUG_BUILD
	DISPLAY_SEAT_NODE_POSITIONS()
	DEBUG_OFFICE_CHAIR_DISPLAY_GARAGE()
//	ADJUST_DEBUG_OFFICE_SEAT_SYNC_SCENE()
	#ENDIF
	
//	MANAGE_GAME_ANIM_SWITCH()
	MANAGE_CAMERA_COLLISION_ISSUE_FOR_SOFA()
//	#ENDIF

	SUPPRESS_JAC_ARMCHAIR_PHONE_HUD_AND_ACTION_LOGIC()
	MANAGE_INTERRUPTABLE_OFFICE_SEAT_ARMCHAIR_INPUT()
	
//	MANAGE_COLLISION_GEOMETRY()

	SWITCH g_OfficeArmChairState
		CASE OFFICE_SEAT_INIT
//			IF serverBD.iStage > SERVER_STAGE_INIT
				SET_OFFICE_SEAT_STAGE(g_OfficeArmChairState, OFFICE_SEAT_LOCATECHECK)
//			ENDIF
		BREAK
	
		CASE OFFICE_SEAT_LOCATECHECK
//			FLOAT SeatWidth
//			SeatWidth = 0.45
			VECTOR CurrentSeatPos
			VECTOR CurrentSeatRot
			CurrentSeatPos = mpOffsetOfficeSeats[iStaggerOfficeArmChairiSeatLoop].vLoc
			CurrentSeatRot = mpOffsetOfficeSeats[iStaggerOfficeArmChairiSeatLoop].vRot
//			CurrentSeatPos.z += 1

			vOfficeSeatArmchairBoundingBox1 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(CurrentSeatPos,  CurrentSeatRot.Z, dbvOfficeSeatArmchairBoundingBox1) //<<0.393104,0.603146,-0.627219>>)
//
			vOfficeSeatArmchairBoundingBox2 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(CurrentSeatPos,  CurrentSeatRot.Z, dbvOfficeSeatArmchairBoundingBox2) //<<-0.441553,0.563528,1.202967>>)
			
//			GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(mpOffsetOfficeSeats[iStaggerOfficeArmChairiSeatLoop].vLoc, mpOffsetOfficeSeats[iStaggerOfficeArmChairiSeatLoop].vRot, )
				
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vOfficeSeatArmchairBoundingBox1, vOfficeSeatArmchairBoundingBox2, GET_SEAT_LOCATE_WIDTH(iStaggerOfficeArmChairiSeatLoop))
			AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			AND NOT IS_PED_RUNNING(PLAYER_PED_ID())
//			AND NOT ARE_THERE_TOO_MANY_PLAYERS_CLOSE_TO_ACTIVITY_ANGLED_AREA(vOfficeSeatArmchairBoundingBox1, vOfficeSeatArmchairBoundingBox2, GET_SEAT_LOCATE_WIDTH(iStaggerOfficeArmChairiSeatLoop))
							
				CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_LOCATECHECK: Player is in seat: ", iStaggerOfficeArmChairiSeatLoop," locate, CurrentSeatPos= ", CurrentSeatPos)
				SET_OFFICE_SEAT_STAGE(g_OfficeArmChairState, OFFICE_SEAT_PROMPT)	
			ELSE
				#IF IS_DEBUG_BUILD
//				IF IS_PED_RUNNING(PLAYER_PED_ID())
//						CDEBUG1LN(DEBUG_SAFEHOUSE, "Ped is running to not allowing to start activity")
//				ENDIF
				#ENDIF
				
				iStaggerOfficeArmChairiSeatLoop++
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_LOCATECHECK: iStaggerOfficeArmChairiSeatLoop = ", iStaggerOfficeArmChairiSeatLoop)
				IF (iStaggerOfficeArmChairiSeatLoop >= GET_NUMBER_OF_ARMCHAIR_SEAT_NODES())
					iStaggerOfficeArmChairiSeatLoop = 0
				ENDIF
			ENDIF
				
			
		BREAK
		
		
		CASE OFFICE_SEAT_PROMPT
			 
			eChosenOfficeSeatSlotActivity = INT_TO_ENUM(ACTIVITY_ENUMS, ENUM_TO_INT(OFFICE_SEAT_ARMCHAIR_SLOT_1_ENUM)+iStaggerOfficeArmChairiSeatLoop)
			#IF IS_DEBUG_BUILD
				//CDEBUG3LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_PROMPT: Checking against activity: ", DEBUG_GET_MP_PROP_ACT_NAME(eChosenOfficeSeatSlotActivity))
			#ENDIF
			
//			IF NOT ARE_THERE_TOO_MANY_PLAYERS_CLOSE_TO_ACTIVITY_ANGLED_AREA(vOfficeSeatArmchairBoundingBox1, vOfficeSeatArmchairBoundingBox2, GET_SEAT_LOCATE_WIDTH(iStaggerOfficeArmChairiSeatLoop))
//			IF serverBD.activityControl.iActivityUsedBS[ENUM_TO_INT(eChosenOfficeSeatSlotActivity)] = 0
			IF NOT IS_ANY_INTERACTION_ANIM_PLAYING()
			AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
			AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
			AND NOT IS_BROWSER_OPEN()
			AND NOT IS_PED_RUNNING(PLAYER_PED_ID())
//			AND activeActivity = ACTIVITY_NONE
				
				
				IF NOT IS_PHONE_ONSCREEN()
				AND NOT IS_HIGHER_PRIORITY_HELP_SHOWN()
					IF NOT IS_OFFICE_SEAT_START_PROMPT_SHOWN()
						IF IS_ARMCHAIR_SOFA_SEAT(iStaggerOfficeArmChairiSeatLoop)
							
							SHOW_OFFICE_SEAT_START_PROMPT_GARAGE(IS_ARMCHAIR_TV_SEAT(iStaggerOfficeArmChairiSeatLoop))
						ELSE
							SHOW_OFFICE_SEAT_START_PROMPT_GARAGE()
						ENDIF
					ENDIF
				ELSE
					//IF IS_OFFICE_SEAT_START_PROMPT_SHOWN()
						IF iOfficeSeatContextIntention != NEW_CONTEXT_INTENTION
							RELEASE_CONTEXT_INTENTION(iOfficeSeatContextIntention)
						ENDIF
					//ENDIF
				ENDIF
				
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
				AND NOT IS_HIGHER_PRIORITY_HELP_SHOWN()
				AND NOT IS_LOCAL_PLAYER_USING_JUKEBOX()
					iPlayerChosenSeatSlot = iStaggerOfficeArmChairiSeatLoop
					SET_PLAYER_USING_OFFICE_SEATID(iPlayerChosenSeatSlot)
//					activeActivity = eChosenOfficeSeatSlotActivity
					playerBD[NATIVE_TO_INT(PLAYER_ID())].iOfficeSeatArmchairID = iPlayerChosenSeatSlot
					
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_PROMPT: HAS_CONTEXT_BUTTON_TRIGGERED: TRUE, eChosenOfficeSeatSlotActivity = ", DEBUG_GET_MP_PROP_ACT_NAME(eChosenOfficeSeatSlotActivity))
//					IF IS_OFFICE_SEAT_START_PROMPT_SHOWN()
//						CLEAR_HELP()
						IF iOfficeSeatContextIntention != NEW_CONTEXT_INTENTION
							RELEASE_CONTEXT_INTENTION(iOfficeSeatContextIntention)
						ENDIF
//					ENDIF
										
//					ePlayerOfficeSeatOrientation = FACE_CENTER
//					eOfficePlayerSeatAnimVar = OFFICE_CREW_VAR_UNASSIGNED
					
					// Assign default seat anim variable.
					IF IS_PROPERTY_OFFICE(iCurrentProperty)
					OR IS_PROPERTY_OFFICE_GARAGE(iCurrentProperty)
						IF CAN_PED_CHANGE_POSE()
							eAnimOfficeSeatVar = VAR_D
						ELSE
							eAnimOfficeSeatVar = VAR_B
						ENDIF
					ELIF IS_PROPERTY_CLUBHOUSE(iCurrentProperty)
						eAnimOfficeSeatVar = VAR_A
					ENDIF
					
					CLEAR_BIT(iOfficeSeatiBitSet, OFFICE_SEAT_BS_EXIT_HELP_TEXT_DISPLAYED)
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_PROMPT: player doesn't require access for chair: ", iPlayerChosenSeatSlot)
					
//					IF IS_ARMCHAIR_TV_SEAT(iPlayerChosenSeatSlot)
//						ASSIGN_TV_SEAT_PLAYER_IS_SITTING_IN(playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient, GET_TV_ID_FROM_ARMCHAIR_ID(iPlayerChosenSeatSlot))
//						eAnimOfficeSeatVar = GET_ARMCHAIR_ANIM_VAR_FOR_COUCH(iPlayerChosenSeatSlot, IS_PLAYER_FEMALE())
//					ENDIF

					SET_OFFICE_SEAT_STAGE(g_OfficeArmChairState, OFFICE_SEAT_REQUEST)	
					
				ENDIF
				
			ELSE
				#IF IS_DEBUG_BUILD
				IF serverBD.activityControl.iActivityUsedBS[ENUM_TO_INT(eChosenOfficeSeatSlotActivity)] != 0
					CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_PROMPT: SEAT IN USE, activity = ", ENUM_TO_INT(eChosenOfficeSeatSlotActivity))
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_PROMPT: SEAT NOT IN USE")
				ENDIF
				
//				IF ARE_THERE_TOO_MANY_PLAYERS_CLOSE_TO_ACTIVITY_ANGLED_AREA(vOfficeSeatArmchairBoundingBox1, vOfficeSeatArmchairBoundingBox2, GET_SEAT_LOCATE_WIDTH(iStaggerOfficeArmChairiSeatLoop))
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_PROMPT: Someone else is in the same seat locate")
//				ENDIF
				#ENDIF
				IF NOT IS_ANY_INTERACTION_ANIM_PLAYING()
					CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_PROMPT: Either property activity isn't free and player can't use it or player is getting kicked from apartment")

//					IF IS_OFFICE_SEAT_START_PROMPT_SHOWN()
//					CLEAR_HELP()
						IF iOfficeSeatContextIntention != NEW_CONTEXT_INTENTION
							RELEASE_CONTEXT_INTENTION(iOfficeSeatContextIntention)
						ENDIF
//					ENDIF

//					IF IS_OFFICE_SEAT_BOARDROOM_BOSS(iStaggerOfficeiSeatLoop)
//						IF iOfficeSeatContextIntention = NEW_CONTEXT_INTENTION
//							REGISTER_CONTEXT_INTENTION(iOfficeSeatContextIntention, CP_HIGH_PRIORITY, "MPJAC_NO_SIT")
//						ENDIF
//					ENDIF

//					IF IS_OFFICE_SEAT_COMPUTER_BOSS(iStaggerOfficeiSeatLoop)
//						IF iOfficeSeatContextIntention = NEW_CONTEXT_INTENTION
//						
//							REGISTER_CONTEXT_INTENTION(iOfficeSeatContextIntention, CP_HIGH_PRIORITY, "MPOFFCH_NO_SIT")
//						ENDIF
//					ENDIF
					CLEAR_BIT(iOfficeSeatiBitSet, OFFICE_SEAT_BS_EXIT_HELP_TEXT_DISPLAYED)					
				ENDIF
			ENDIF
			
//			VECTOR CurrentSeatPos
//			VECTOR CurrentSeatRot
			CurrentSeatPos = mpOffsetOfficeSeats[iStaggerOfficeArmChairiSeatLoop].vLoc
			CurrentSeatRot = mpOffsetOfficeSeats[iStaggerOfficeArmChairiSeatLoop].vRot
			
			vOfficeSeatArmchairBoundingBox1 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(CurrentSeatPos,  CurrentSeatRot.Z, dbvOfficeSeatArmchairBoundingBox1) //<<0.393104,0.603146,-0.627219>>)
			vOfficeSeatArmchairBoundingBox2 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(CurrentSeatPos,  CurrentSeatRot.Z, dbvOfficeSeatArmchairBoundingBox2) //<<-0.441553,0.563528,1.202967>>)
			
			IF IS_PROPERTY_CLUBHOUSE(iCurrentProperty, PROPERTY_CLUBHOUSE_7_BASE_B)
				IF iStaggerOfficeArmChairiSeatLoop = 6
					vOfficeSeatArmchairBoundingBox1 = <<1002.847351,-3169.606445,-35.046421>>
 					vOfficeSeatArmchairBoundingBox2 = <<1001.693970,-3169.619141,-33.296421>> 
				ENDIF
			ENDIF
			
//			IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), CurrentSeatPos, <<SeatWidth, SeatWidth, SeatWidth>>)
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vOfficeSeatArmchairBoundingBox1, vOfficeSeatArmchairBoundingBox2, GET_SEAT_LOCATE_WIDTH(iStaggerOfficeArmChairiSeatLoop))
//			OR ARE_THERE_TOO_MANY_PLAYERS_CLOSE_TO_ACTIVITY_ANGLED_AREA(vOfficeSeatArmchairBoundingBox1, vOfficeSeatArmchairBoundingBox2, GET_SEAT_LOCATE_WIDTH(iStaggerOfficeArmChairiSeatLoop))
				CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_LOCATECHECK: Player is not in seat ", iStaggerOfficeArmChairiSeatLoop," locate anymore, CurrentSeatPos= ", CurrentSeatPos, ", iStaggerOfficeArmChairiSeatLoop = ", iStaggerOfficeArmChairiSeatLoop)

				IF iOfficeSeatContextIntention != NEW_CONTEXT_INTENTION
					RELEASE_CONTEXT_INTENTION(iOfficeSeatContextIntention)
				ENDIF

				
				CLEAR_BIT(iOfficeSeatiBitSet, OFFICE_SEAT_BS_EXIT_HELP_TEXT_DISPLAYED)
				SET_OFFICE_SEAT_STAGE(g_OfficeArmChairState, OFFICE_SEAT_LOCATECHECK)													
			ENDIF			
		BREAK
		
		CASE OFFICE_SEAT_REQUEST	
//			PROPERTY_ACTIVITY_REQUEST_RESULT requestResult
//			requestResult = MP_PROP_ACT_RESULT_IN_PROGRESS
			
//			IF REQUEST_PROPERTY_ACTIVITY(eChosenOfficeSeatSlotActivity, requestResult, playerBD[NATIVE_TO_INT(PLAYER_ID())].iActivityRequested, serverBD.activityControl)
			//AND (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vOfficeSeatArmchairBoundingBox1, vOfficeSeatArmchairBoundingBox2, GET_SEAT_LOCATE_WIDTH(iStaggerOfficeArmChairiSeatLoop))
			//OR (IS_PROPERTY_CLUBHOUSE(iCurrentProperty) AND bTVSpawnActivity))
//				
//				IF requestResult = MP_PROP_ACT_RESULT_IN_USE
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "RUN_OFFICE_SEAT_ACTIVITY: OFFICE_SEAT_REQUEST = TRUE: MP_PROP_ACT_RESULT_IN_USE ")
//					CLEAR_REQUEST_PROPERTY_ACTIVITY(playerBD[NATIVE_TO_INT(PLAYER_ID())].iActivityRequested)
//					SET_OFFICE_SEAT_STAGE(g_OfficeArmChairState, OFFICE_SEAT_LOCATECHECK)
//				ELIF requestResult = MP_PROP_ACT_RESULT_SUCCESS
					CDEBUG1LN(DEBUG_SAFEHOUSE, "RUN_OFFICE_SEAT_ACTIVITY: OFFICE_SEAT_REQUEST = TRUE: MP_PROP_ACT_RESULT_SUCCESS ")
//					IF IS_ARMCHAIR_TV_SEAT(iPlayerChosenSeatSlot)
//						IF OCCUPY_TV_SEAT(NATIVE_TO_INT(PLAYER_ID()), serverBD.MPTVServer, playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient, MPTVLocal)
//							SET_MP_TV_CLIENT_STAGE(playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient, MP_TV_CLIENT_STAGE_SITTING)
//							
//							sOfficeSeatAnimDict = GET_OFFICE_SEAT_ARMCHAIR_ENTER_ANIM_DICT(eAnimOfficeSeatVar, IS_PLAYER_FEMALE())
//							IF !bTVSpawnActivity 
//								PRINTLN("IS_ARMCHAIR_TV_SEAT - bTVSpawnActivity is falsesetting stage to OFFICE_SEAT_APPROACH")
//								SET_OFFICE_SEAT_STAGE(g_OfficeArmChairState, OFFICE_SEAT_APPROACH)
//							ELSE
//								g_bSpawnTVIsActive = FALSE
//								SET_OFFICE_SEAT_STAGE(g_OfficeArmChairState, OFFICE_SEAT_SPAWN_ACTIVITY)
//								PRINTLN("IS_ARMCHAIR_TV_SEAT - setting stage to OFFICE_SEAT_SPAWN_ACTIVITY")
//							ENDIF
//						ELSE
//							CDEBUG1LN(DEBUG_SAFEHOUSE, "OCCUPY_TV_SEAT FALSE")
//						ENDIF
//					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_ARMCHAIR_TV_SEAT FALSE")
						sOfficeSeatAnimDict = GET_OFFICE_SEAT_ARMCHAIR_ENTER_ANIM_DICT(eAnimOfficeSeatVar, IS_PLAYER_FEMALE())
						SET_OFFICE_SEAT_STAGE(g_OfficeArmChairState, OFFICE_SEAT_APPROACH)
//					ENDIF 
					
//					
//				ENDIF	
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "RUN_OFFICE_SEAT_ACTIVITY: REQUEST_PROPERTY_ACTIVITY = TRUE ")
//			ELSE
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "RUN_OFFICE_SEAT_ACTIVITY: REQUEST_PROPERTY_ACTIVITY = FALSE ")
//			ENDIF
		BREAK
		
		CASE OFFICE_SEAT_APPROACH

			REQUEST_ANIM_DICT(sOfficeSeatAnimDict)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_APPROACH: REQUEST_ANIM_DICT - Requesting: ", sOfficeSeatAnimDict, " anim dictionary.")
			
			IF NOT HAS_ANIM_DICT_LOADED(sOfficeSeatAnimDict)
				REQUEST_ANIM_DICT(sOfficeSeatAnimDict)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_APPROACH: HAS_ANIM_DICT_LOADED = FALSE, sOfficeSeatAnimDict = ", sOfficeSeatAnimDict)
				EXIT
			ENDIF
			
			VECTOR vTriggerPos 
			vTriggerPos = GET_ANIM_INITIAL_OFFSET_POSITION(sOfficeSeatAnimDict, "enter", mpOffsetOfficeSeats[iStaggerOfficeArmChairiSeatLoop].vLoc, mpOffsetOfficeSeats[iStaggerOfficeArmChairiSeatLoop].vRot)  
			VECTOR vTempRotation 
			vTempRotation = GET_ANIM_INITIAL_OFFSET_ROTATION(sOfficeSeatAnimDict, "enter", mpOffsetOfficeSeats[iStaggerOfficeArmChairiSeatLoop].vLoc, mpOffsetOfficeSeats[iStaggerOfficeArmChairiSeatLoop].vRot)  
			Float fTriggerHead 
			fTriggerHead = vTempRotation.Z
			#IF IS_DEBUG_BUILD
			VECTOR tempV 
			tempV = mpOffsetOfficeSeats[iStaggerOfficeArmChairiSeatLoop].vLoc
			CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_APPROACH: vTriggerPos = ", vTriggerPos, ", GET_MP_JACUZZI_SEAT_CHAIR_POSITION = ", tempV )
			#ENDIF
			
			CDEBUG1LN(DEBUG_SAFEHOUSE, "<OFFICE_SEAT_APPROACH> TASK_GO_STRAIGHT_TO_COORD - Tasking ped to go straight to coord.")
			
			// Fix for B*3078857, reduced target radius from 0.050 to 0.010
			TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), vTriggerPos, PEDMOVEBLENDRATIO_WALK, 100, fTriggerHead, 0.010)
		
			SET_OFFICE_SEAT_STAGE(g_OfficeArmChairState, OFFICE_SEAT_RUN_ENTER_ANIM)
		BREAK
		
		CASE OFFICE_SEAT_RUN_ENTER_ANIM
//			CDEBUG2LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_RUN_ENTER_ANIM: SET_PED_CAPSULE to 0.2")
//			SET_PED_CAPSULE(PLAYER_PED_ID(), 0.15)
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> PERFORMING_TASK
				REMOVE_INCOMPATIBLE_CLOTHES()
				iJacuzziNetSceneid = NETWORK_CREATE_SYNCHRONISED_SCENE(mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vLoc, mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vRot, DEFAULT, TRUE, FALSE)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_RUN_ENTER_ANIM: vLoc = ", mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vLoc,", rot = ", mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vRot,", sOfficeSeatAnimDict = ", sOfficeSeatAnimDict, ", clip = enter")
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iJacuzziNetSceneid, sOfficeSeatAnimDict, "enter", WALK_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_USE_PHYSICS |SYNCED_SCENE_ON_ABORT_STOP_SCENE ,  RBF_PLAYER_IMPACT, WALK_BLEND_IN)					
				
				NETWORK_START_SYNCHRONISED_SCENE(iJacuzziNetSceneid)

				SET_OFFICE_SEAT_STAGE(g_OfficeArmChairState, OFFICE_SEAT_CHECK_ENTER_OR_IDLE_ANIM)
				
			ELSE
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_RUN_ENTER_ANIM: waiting for ped to stop performing task")
			ENDIF
		BREAK
		
		CASE OFFICE_SEAT_SPAWN_ACTIVITY		// only used for TV spawn activity 
//			IF bTVSpawnActivity
//				iJacuzziNetSceneid = NETWORK_CREATE_SYNCHRONISED_SCENE(mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vLoc, mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vRot, DEFAULT, TRUE, FALSE)
//				IF bOfficeSeatTurning
//					bOfficeSeatTurning = FALSE
//				ENDIF
//				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iJacuzziNetSceneid, sOfficeSeatAnimDict, "BASE", INSTANT_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE,  RBF_PLAYER_IMPACT,DEFAULT)		
//				NETWORK_START_SYNCHRONISED_SCENE(iJacuzziNetSceneid)
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "RUN_OFFICE_SEAT_ARMCHAIR_ACTIVITY, ANIM_DICT: ", sOfficeSeatAnimDict,", ANIM_NAME: base - CASE OFFICE_SEAT_SPAWN_ACTIVITY - Start Base")
//				IF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) != PROPERTY_CLUBHOUSE_1_BASE_A
//					bTVSpawnActivity = FALSE
//				ENDIF	
//				bSpawningLaunchTVActivity = FALSE
//				SET_OFFICE_SEAT_STAGE(g_OfficeArmChairState, OFFICE_SEAT_CHECK_BASE_ANIM)
//			ENDIF	
		BREAK
		
		CASE OFFICE_SEAT_CHECK_ENTER_OR_IDLE_ANIM
			
			IF IS_PROPERTY_OFFICE(iCurrentProperty)
//				IF DONT89_IS_REMOTE_PLAYER_RUNNING_GAME(PLAYER_ID())
//				AND HAS_ANIM_EVENT_FIRED (PLAYER_PED_ID(), HASH("CREATE_REMOTE"))
//				AND IS_ANIM_DICT_SEAT_GAME()
//					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niOfficeSetTopRemote)
//						SET_ENTITY_VISIBLE(NET_TO_ENT(niOfficeSetTopRemote), TRUE)
//					ENDIF
//				ENDIF
				IF HAS_ANIM_EVENT_FIRED (PLAYER_PED_ID(), HASH("DESTROY_REMOTE"))
//					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niOfficeSetTopRemote)
//						CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_CHECK_ENTER_OR_IDLE_ANIM: DESTROY_REMOTE fired, making remote invisible")	
//						SET_ENTITY_VISIBLE(NET_TO_ENT(niOfficeSetTopRemote), FALSE)
//					ENDIF
				ENDIF
			ENDIF
			// the remote isn't create and destroyed for the clips, they are created while the player is sitting and destroyed when they leave
//			IF NOT DONT89_IS_REMOTE_PLAYER_RUNNING_GAME(PLAYER_ID())
//			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niOfficeSetTopRemote)
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_CHECK_ENTER_OR_IDLE_ANIM: DELETE_NET_ID(niOfficeSetTopRemote)")
//				DELETE_NET_ID(niOfficeSetTopRemote)
//			ENDIF
			
			INT localIdleScene
			localIdleScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iJacuzziNetSceneid)

			IF localIdleScene != -1
				
				IF GET_SYNCHRONIZED_SCENE_PHASE(localIdleScene) >= 1 // Has the idle or enter anim finished yet
					VECTOR vSceneLoc
					VECTOR vSceneRot
//					IF bOfficeStandingPlayingGame
//						vSceneLoc = vGameStandingPos
//						vSceneRot = vGameStandingRot
//						CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_CHECK_ENTER_OR_IDLE_ANIM: Player is standing so running scene from player pos, vSceneLoc: ", vSceneLoc)
//					ELSE
						vSceneLoc = mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vLoc
						vSceneRot = mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vRot
//					ENDIF
					iJacuzziNetSceneid = NETWORK_CREATE_SYNCHRONISED_SCENE(vSceneLoc, vSceneRot, DEFAULT, TRUE, FALSE)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_CHECK_ENTER_OR_IDLE_ANIM: CREATE net sync scene, vSceneLoc: " , vSceneLoc)
					IF bOfficeSeatTurning
						bOfficeSeatTurning = FALSE
					ENDIF
					#IF IS_DEBUG_BUILD
					IF GET_COMMANDLINE_PARAM_EXISTS("sc_ExecOfficeArmchairExpandPedCapsule")
						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iJacuzziNetSceneid, sOfficeSeatAnimDict, "base", SLOW_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON,  RBF_PLAYER_IMPACT,DEFAULT)
					ELSE
					#ENDIF
						CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_CHECK_ENTER_OR_IDLE_ANIM: sOfficeSeatAnimDict: ", sOfficeSeatAnimDict, ", clip: base")
						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iJacuzziNetSceneid, sOfficeSeatAnimDict, "base", SLOW_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE,  RBF_PLAYER_IMPACT,DEFAULT)
					#IF IS_DEBUG_BUILD
					ENDIF
					#ENDIF
					
//					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niOfficeSetTopRemote)
//					AND DONT89_IS_REMOTE_PLAYER_RUNNING_GAME(PLAYER_ID())
//					AND IS_ANIM_DICT_SEAT_GAME()
//						NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_ENT(niOfficeSetTopRemote), iJacuzziNetSceneid, sOfficeSeatAnimDict, "base_remote", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_NET_ON_EARLY_NON_PED_STOP_RETURN_TO_START)
//					ENDIF
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: PLAYER added to Net Sync Scene, ANIM_DICT: ", sOfficeSeatAnimDict,", ANIM_NAME: base")

					NETWORK_START_SYNCHRONISED_SCENE(iJacuzziNetSceneid)

					
					
					SET_OFFICE_SEAT_STAGE(g_OfficeArmChairState, OFFICE_SEAT_CHECK_BASE_ANIM)
					
				ENDIF
			ENDIF		
			
			
		BREAK
		
		CASE OFFICE_SEAT_CHECK_BASE_ANIM 
			INT localScene
			localScene  = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iJacuzziNetSceneid)
			IF localScene != -1
				
				
				IF GET_SYNCHRONIZED_SCENE_PHASE(localScene) >= 1
				
					VECTOR vSceneLoc
					VECTOR vSceneRot
//					IF bOfficeStandingPlayingGame
//						vSceneLoc = vGameStandingPos
//						vSceneRot = vGameStandingRot
//						CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_CHECK_ENTER_OR_IDLE_ANIM: Player is standing so running scene from player pos, vSceneLoc: ", vSceneLoc)
//					ELSE
						vSceneLoc = mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vLoc
						vSceneRot = mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vRot
//					ENDIF				
					iJacuzziNetSceneid = NETWORK_CREATE_SYNCHRONISED_SCENE(vSceneLoc, vSceneRot, DEFAULT, TRUE, FALSE)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_CHECK_BASE_ANIM: CREATE net sync scene, vSceneLoc: " , vSceneLoc)
					#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: CREATE net sync scene, SCENE_ID: " , iJacuzziNetSceneid)
					#ENDIF
					
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_CHECK_BASE_ANIM: Going to play idle anim")
										
					
					iIdleOfficeSeatClipVar = GENERATE_RANDOM_OFFICE_SEAT_ARMCHAIR_IDLE_CLIP()
					
					#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: PLAYER added to Net Sync Scene, ANIM_DICT: ", sOfficeSeatAnimDict,", ANIM_NAME: ", GET_OFFICE_SEAT_PED_IDLE_CLIP_STRING_FROM_INT(iIdleOfficeSeatClipVar))
					#ENDIF					
					#IF IS_DEBUG_BUILD
					IF GET_COMMANDLINE_PARAM_EXISTS("sc_ExecOfficeArmchairExpandPedCapsule")
						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iJacuzziNetSceneid, sOfficeSeatAnimDict, GET_OFFICE_SEAT_PED_IDLE_CLIP_STRING_FROM_INT(iIdleOfficeSeatClipVar), SLOW_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_USE_PHYSICS |SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON,  RBF_PLAYER_IMPACT,DEFAULT)
					ELSE
					#ENDIF
						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iJacuzziNetSceneid, sOfficeSeatAnimDict, GET_OFFICE_SEAT_PED_IDLE_CLIP_STRING_FROM_INT(iIdleOfficeSeatClipVar), SLOW_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_USE_PHYSICS |SYNCED_SCENE_ON_ABORT_STOP_SCENE,  RBF_PLAYER_IMPACT,DEFAULT)
					#IF IS_DEBUG_BUILD
					ENDIF
					#ENDIF
					
//					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niOfficeSetTopRemote)
//					AND DONT89_IS_REMOTE_PLAYER_RUNNING_GAME(PLAYER_ID())
//					AND IS_ANIM_DICT_SEAT_GAME()
//						CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_CHECK_BASE_ANIM: sOfficeSeatAnimDict = ", sOfficeSeatAnimDict, ", GET_OFFICE_REMOTE_SEAT_PED_IDLE_CLIP_STRING_FROM_INT = ", GET_OFFICE_REMOTE_SEAT_PED_IDLE_CLIP_STRING_FROM_INT(iIdleOfficeSeatClipVar), ", eAnimOfficeSeatVar = ", eAnimOfficeSeatVar )
//						NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_ENT(niOfficeSetTopRemote), iJacuzziNetSceneid, sOfficeSeatAnimDict, GET_OFFICE_REMOTE_SEAT_PED_IDLE_CLIP_STRING_FROM_INT(iIdleOfficeSeatClipVar), SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_NET_ON_EARLY_NON_PED_STOP_RETURN_TO_START)
//					ENDIF
					
					
//					IF IS_OFFICE_SEAT_BOARDROOM_BOSS(iPlayerChosenSeatSlot)
//					AND IS_PROPERTY_OFFICE(iCurrentProperty)
//					AND HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_REVOLVER)
//						NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NETWORK_GET_ENTITY_FROM_NETWORK_ID(niOfficeSeatRevolver), iJacuzziNetSceneid, sOfficeSeatAnimDict, GET_OFFICE_SEAT_GUN_IDLE_CLIP_STRING_FROM_INT(iIdleOfficeSeatClipVar), SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_NET_ON_EARLY_NON_PED_STOP_RETURN_TO_START| SYNCED_SCENE_USE_PHYSICS)
//					ENDIF
					
					NETWORK_START_SYNCHRONISED_SCENE(iJacuzziNetSceneid)
					
					g_bSpawnTVIsActive = FALSE
					SET_OFFICE_SEAT_STAGE(g_OfficeArmChairState, OFFICE_SEAT_CHECK_ENTER_OR_IDLE_ANIM)
					
				ENDIF
				
			ENDIF
			
		BREAK
		
		
		
		CASE OFFICE_SEAT_PLAY_COMPUTER_EXIT
			localScene  = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iJacuzziNetSceneid)
			IF localScene != -1
			

				
				#IF IS_DEBUG_BUILD
				VECTOR vRotation 
				vRotation = GET_ENTITY_ROTATION(objOfficeSeat)
				VECTOR vCoords 
				vCoords = GET_ENTITY_COORDS(objOfficeSeat)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_CHECK_BASE_ANIM: GET_ENTITY_COORDS(objOfficeSeat) = ", vCoords, ", GET_ENTITY_ROTATION(objOfficeSeat) = ", vRotation)
				#ENDIF
				
				iJacuzziNetSceneid = NETWORK_CREATE_SYNCHRONISED_SCENE(mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vLoc, mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vRot, DEFAULT, TRUE, FALSE)
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: CREATE net sync scene, SCENE_ID: " , iJacuzziNetSceneid)
				#ENDIF
				
				
				CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_CHECK_BASE_ANIM: Going to play computer idle anim")			
				
				
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iJacuzziNetSceneid, sOfficeSeatAnimDict, "COMPUTER_EXIT", SLOW_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE ,  DEFAULT,DEFAULT,AIK_DISABLE_HEAD_IK)
				NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(objOfficeSeat, iJacuzziNetSceneid, sOfficeSeatAnimDict, "COMPUTER_EXIT_CHAIR", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_NET_ON_EARLY_NON_PED_STOP_RETURN_TO_START| SYNCED_SCENE_USE_PHYSICS)
				
				
				NETWORK_START_SYNCHRONISED_SCENE(iJacuzziNetSceneid)
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: PLAYER added to Net Sync Scene, ANIM_DICT: ", sOfficeSeatAnimDict,", ANIM_NAME: COMPUTER_EXIT")
				#ENDIF
				
				
				IF g_bSecuroQuickExitOfficeChair = TRUE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_CHECK_ENTER_OR_IDLE_ANIM: exiting because g_bSecuroQuickExitOfficeChair = TRUE")
					SET_OFFICE_SEAT_STAGE(g_OfficeArmChairState, OFFICE_SEAT_EXIT)
				ELSE
					SET_OFFICE_SEAT_STAGE(g_OfficeArmChairState, OFFICE_SEAT_CHECK_ENTER_OR_IDLE_ANIM)
				ENDIF
			ENDIF
				
			
		BREAK
		
		CASE OFFICE_SEAT_EXIT_TURN

			iJacuzziNetSceneid = NETWORK_CREATE_SYNCHRONISED_SCENE(mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vLoc, mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vRot, DEFAULT, TRUE, FALSE)
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: CREATE net sync scene, SCENE_ID: " , iJacuzziNetSceneid)
			#ENDIF
																																															
			NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iJacuzziNetSceneid, sOfficeSeatAnimDict, "exit", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE , RBF_PLAYER_IMPACT,INSTANT_BLEND_IN)
			
			NETWORK_START_SYNCHRONISED_SCENE(iJacuzziNetSceneid)
//			IF IS_OFFICE_SEAT_EXIT_PROMPT_SHOWN()
//			OR IS_OFFICE_SEAT_COMPUTER_EXIT_PROMPT_SHOWN()
//				CLEAR_HELP()
//			ENDIF

			CLEAR_BIT(iOfficeSeatiBitSet, OFFICE_SEAT_BS_EXIT_HELP_TEXT_DISPLAYED)
			
				
			CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_PLAYER_INTPUT_LOGIC: Cancel was pressed by crew boardroom member, while turned")
			
			SET_OFFICE_SEAT_STAGE(g_OfficeArmChairState, OFFICE_SEAT_CHECK_ENTER_OR_IDLE_ANIM)
				
			
		BREAK
		
		CASE OFFICE_SEAT_SWITCH_TO_GAME_ANIM
//			REQUEST_ANIM_DICT(sOfficeSeatAnimDict)
//			
//			
//			
//			IF HAS_ANIM_DICT_LOADED(sOfficeSeatAnimDict)
//			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niOfficeSetTopRemote)
//				iJacuzziNetSceneid = NETWORK_CREATE_SYNCHRONISED_SCENE(mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vLoc, mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vRot, DEFAULT, TRUE, FALSE)
//				#IF IS_DEBUG_BUILD
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: CREATE net sync scene, SCENE_ID: " , iJacuzziNetSceneid)
//				#ENDIF
//																																																
//				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iJacuzziNetSceneid, sOfficeSeatAnimDict, GET_SEAT_ENTER_TO_GAME_ANIM_CLIP(eAnimOfficeSeatVar), SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_USE_PHYSICS |SYNCED_SCENE_ON_ABORT_STOP_SCENE , RBF_PLAYER_IMPACT,INSTANT_BLEND_IN)
//				#IF IS_DEBUG_BUILD
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_SWITCH_FROM_GAME_ANIM_TO_SEAT: PLAYER added to Net Sync Scene, ANIM_DICT: ", sOfficeSeatAnimDict,", GET_SEAT_ENTER_TO_GAME_ANIM_CLIP: ", GET_SEAT_ENTER_TO_GAME_ANIM_CLIP(eAnimOfficeSeatVar), ", eAnimOfficeSeatVar = ", eAnimOfficeSeatVar)
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_SWITCH_FROM_GAME_ANIM_TO_SEAT: PLAYER added to Net Sync Scene, ANIM_DICT: ", sOfficeSeatAnimDict,", GET_REMOTE_SEAT_ENTER_TO_GAME_ANIM_CLIP: ", GET_REMOTE_SEAT_ENTER_TO_GAME_ANIM_CLIP(eAnimOfficeSeatVar), ", eAnimOfficeSeatVar = ", eAnimOfficeSeatVar)
//				#ENDIF
//				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niOfficeSetTopRemote)
//				AND DONT89_IS_REMOTE_PLAYER_RUNNING_GAME(PLAYER_ID())
//				AND IS_ANIM_DICT_SEAT_GAME()
//					NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_ENT(niOfficeSetTopRemote), iJacuzziNetSceneid, sOfficeSeatAnimDict, GET_REMOTE_SEAT_ENTER_TO_GAME_ANIM_CLIP(eAnimOfficeSeatVar), SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_NET_ON_EARLY_NON_PED_STOP_RETURN_TO_START)
//				ENDIF
//				
//				NETWORK_START_SYNCHRONISED_SCENE(iJacuzziNetSceneid)
//
//
//				CLEAR_BIT(iOfficeSeatiBitSet, OFFICE_SEAT_BS_EXIT_HELP_TEXT_DISPLAYED)
//				
//				// We now switch anim var, from seat specific D/E/A to game specific A/B/C 
//				eAnimOfficeSeatVar = SWITCH_TO_GAME_ANIM_VAR(iPlayerChosenSeatSlot)
//				
//				
//				SET_OFFICE_SEAT_STAGE(g_OfficeArmChairState, OFFICE_SEAT_CHECK_ENTER_OR_IDLE_ANIM)
//			ELSE
//				IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niOfficeSetTopRemote)
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_SWITCH_TO_GAME_ANIM: niOfficeSetTopRemote not created")
//				ENDIF
//				IF HAS_ANIM_DICT_LOADED(sOfficeSeatAnimDict)
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_SWITCH_TO_GAME_ANIM: sOfficeSeatAnimDict: ", sOfficeSeatAnimDict, ", not loaded")
//				ENDIF
//			ENDIF
			
		BREAK
		
		CASE OFFICE_SEAT_PLAY_GAME_STANDING_ENTER
//			REQUEST_ANIM_DICT(sOfficeSeatAnimDict)
//
//			IF HAS_ANIM_DICT_LOADED(sOfficeSeatAnimDict)
//			AND CREATE_GAME_CONTROLLER()
//			AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_TURN_PED_TO_FACE_COORD) <> PERFORMING_TASK
//				vGameStandingPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
//				vGameStandingRot = GET_ENTITY_ROTATION(PLAYER_PED_ID())
//				iJacuzziNetSceneid = NETWORK_CREATE_SYNCHRONISED_SCENE(vGameStandingPos, vGameStandingRot, DEFAULT, TRUE, FALSE)
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_PLAY_GAME_STANDING_ENTER: CREATE net sync scene, vGameStandingPos: " , vGameStandingPos)
//				#IF IS_DEBUG_BUILD
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: CREATE net sync scene, SCENE_ID: " , iJacuzziNetSceneid)
//				#ENDIF
//																																																
//				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iJacuzziNetSceneid, sOfficeSeatAnimDict, "ENTER", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_USE_PHYSICS |SYNCED_SCENE_ON_ABORT_STOP_SCENE, RBF_PLAYER_IMPACT,INSTANT_BLEND_IN)
//				#IF IS_DEBUG_BUILD
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_SWITCH_FROM_GAME_ANIM_TO_SEAT: PLAYER added to Net Sync Scene, ANIM_DICT: ", sOfficeSeatAnimDict,", GET_SEAT_ENTER_TO_GAME_ANIM_CLIP: ENTER, eAnimOfficeSeatVar = ", eAnimOfficeSeatVar)
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_SWITCH_FROM_GAME_ANIM_TO_SEAT: PLAYER added to Net Sync Scene, ANIM_DICT: ", sOfficeSeatAnimDict,", GET_REMOTE_SEAT_ENTER_TO_GAME_ANIM_CLIP: ENTER_REMOTE eAnimOfficeSeatVar = ", eAnimOfficeSeatVar)
//				#ENDIF
//				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niOfficeSetTopRemote)
//					NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_ENT(niOfficeSetTopRemote), iJacuzziNetSceneid, sOfficeSeatAnimDict, "ENTER_REMOTE", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_NET_ON_EARLY_NON_PED_STOP_RETURN_TO_START | SYNCED_SCENE_BLOCK_MOVER_UPDATE)
//				ENDIF
//				
//				NETWORK_START_SYNCHRONISED_SCENE(iJacuzziNetSceneid)
//
//
//				CLEAR_BIT(iOfficeSeatiBitSet, OFFICE_SEAT_BS_EXIT_HELP_TEXT_DISPLAYED)
//				
//				eAnimOfficeSeatVar = VAR_STANDING
//				
//				
//				SET_OFFICE_SEAT_STAGE(g_OfficeArmChairState, OFFICE_SEAT_CHECK_ENTER_OR_IDLE_ANIM)
//			ELSE
//				IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niOfficeSetTopRemote)
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_PLAY_GAME_STANDING_ENTER: niOfficeSetTopRemote not created")
//				ENDIF
//				IF HAS_ANIM_DICT_LOADED(sOfficeSeatAnimDict)
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_PLAY_GAME_STANDING_ENTER: sOfficeSeatAnimDict: ", sOfficeSeatAnimDict, ", not loaded")
//				ENDIF
//			ENDIF
		BREAK
		
		CASE OFFICE_SEAT_SWITCH_FROM_GAME_ANIM_TO_STAND
//			REQUEST_ANIM_DICT(sOfficeSeatAnimDict)
//			
//			
//			
//			IF HAS_ANIM_DICT_LOADED(sOfficeSeatAnimDict)
//				iJacuzziNetSceneid = NETWORK_CREATE_SYNCHRONISED_SCENE(vGameStandingPos, vGameStandingRot, DEFAULT, TRUE, FALSE)
//				#IF IS_DEBUG_BUILD
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: CREATE net sync scene, SCENE_ID: " , iJacuzziNetSceneid)
//				#ENDIF
//				
//				#IF IS_DEBUG_BUILD
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_SWITCH_FROM_GAME_ANIM_TO_STAND: PLAYER added to Net Sync Scene, ANIM_DICT: ", sOfficeSeatAnimDict,", GET_SEAT_EXIT_TO_GAME_ANIM_CLIP: ", GET_SEAT_EXIT_TO_GAME_ANIM_CLIP(eAnimOfficeSeatVar), ", eAnimOfficeSeatVar = ", eAnimOfficeSeatVar)
//				#ENDIF
//				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iJacuzziNetSceneid, sOfficeSeatAnimDict, "EXIT", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_USE_PHYSICS |SYNCED_SCENE_ON_ABORT_STOP_SCENE , RBF_PLAYER_IMPACT,INSTANT_BLEND_IN)
//				
//				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niOfficeSetTopRemote)
//
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: CREATE net sync scene, SCENE_ID: " , iJacuzziNetSceneid, ", GET_REMOTE_SEAT_EXIT_TO_GAME_ANIM_CLIP() = EXIT")
//					
//					NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_ENT(niOfficeSetTopRemote), iJacuzziNetSceneid, sOfficeSeatAnimDict, "EXIT_REMOTE", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_NET_ON_EARLY_NON_PED_STOP_RETURN_TO_START)
//				ENDIF
//				
//				NETWORK_START_SYNCHRONISED_SCENE(iJacuzziNetSceneid)
//	//			IF IS_OFFICE_SEAT_EXIT_PROMPT_SHOWN()
//	//			OR IS_OFFICE_SEAT_COMPUTER_EXIT_PROMPT_SHOWN()
//	//				CLEAR_HELP()
//	//			ENDIF
//
//				CLEAR_BIT(iOfficeSeatiBitSet, OFFICE_SEAT_BS_EXIT_HELP_TEXT_DISPLAYED)
//				
//				SET_OFFICE_SEAT_STAGE(g_OfficeArmChairState, OFFICE_SEAT_CLEANUP)
//			ENDIF
		BREAK
		
		CASE OFFICE_SEAT_SWITCH_FROM_GAME_ANIM_TO_SEAT
//			REQUEST_ANIM_DICT(sOfficeSeatAnimDict)
//			
//			
//			
//			IF HAS_ANIM_DICT_LOADED(sOfficeSeatAnimDict)
//				iJacuzziNetSceneid = NETWORK_CREATE_SYNCHRONISED_SCENE(mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vLoc, mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vRot, DEFAULT, TRUE, FALSE)
//				#IF IS_DEBUG_BUILD
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: CREATE net sync scene, SCENE_ID: " , iJacuzziNetSceneid)
//				#ENDIF
//				
//				#IF IS_DEBUG_BUILD
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_SWITCH_FROM_GAME_ANIM_TO_SEAT: PLAYER added to Net Sync Scene, ANIM_DICT: ", sOfficeSeatAnimDict,", GET_SEAT_EXIT_TO_GAME_ANIM_CLIP: ", GET_SEAT_EXIT_TO_GAME_ANIM_CLIP(eAnimOfficeSeatVar), ", eAnimOfficeSeatVar = ", eAnimOfficeSeatVar)
//				#ENDIF
//				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iJacuzziNetSceneid, sOfficeSeatAnimDict, GET_SEAT_EXIT_TO_GAME_ANIM_CLIP(eAnimOfficeSeatVar), SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_USE_PHYSICS |SYNCED_SCENE_ON_ABORT_STOP_SCENE , RBF_PLAYER_IMPACT,INSTANT_BLEND_IN)
//				
//				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niOfficeSetTopRemote)
//
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: CREATE net sync scene, SCENE_ID: " , iJacuzziNetSceneid, ", GET_REMOTE_SEAT_EXIT_TO_GAME_ANIM_CLIP() = ", GET_REMOTE_SEAT_EXIT_TO_GAME_ANIM_CLIP(eAnimOfficeSeatVar))
//					
//					NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_ENT(niOfficeSetTopRemote), iJacuzziNetSceneid, sOfficeSeatAnimDict, GET_REMOTE_SEAT_EXIT_TO_GAME_ANIM_CLIP(eAnimOfficeSeatVar), SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_NET_ON_EARLY_NON_PED_STOP_RETURN_TO_START)
//				ENDIF
//				
//				NETWORK_START_SYNCHRONISED_SCENE(iJacuzziNetSceneid)
//	//			IF IS_OFFICE_SEAT_EXIT_PROMPT_SHOWN()
//	//			OR IS_OFFICE_SEAT_COMPUTER_EXIT_PROMPT_SHOWN()
//	//				CLEAR_HELP()
//	//			ENDIF
//
//				CLEAR_BIT(iOfficeSeatiBitSet, OFFICE_SEAT_BS_EXIT_HELP_TEXT_DISPLAYED)
//				
//					
//				
//				// now we update the anim dict, as the correct exit from the game anim has been performed and now we switch to the seated anim dict
//				sOfficeSeatAnimDict = GET_OFFICE_SEAT_ARMCHAIR_ENTER_ANIM_DICT(eAnimOfficeSeatVar, IS_PLAYER_FEMALE()) // Must change the anim dict to the seat one now.
//				SET_OFFICE_SEAT_STAGE(g_OfficeArmChairState, OFFICE_SEAT_CHECK_ENTER_OR_IDLE_ANIM)
//			ENDIF
		BREAK
		
		CASE OFFICE_SEAT_EXIT
//			INT localScene
			localScene  = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iJacuzziNetSceneid)
			IF localScene != -1

				IF GET_SYNCHRONIZED_SCENE_PHASE(localScene) >= 1
				OR NOT HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("BLOCK_INTERRUPT"))
					IF NOT HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("BLOCK_INTERRUPT"))
						CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_EXIT: BLOCK_INTERRUPT not found, exiting from anim early")
					ENDIF
					iJacuzziNetSceneid = NETWORK_CREATE_SYNCHRONISED_SCENE(mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vLoc, mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vRot, DEFAULT, TRUE, FALSE)
					#IF IS_DEBUG_BUILD
//					STRING 
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: CREATE net sync scene, SCENE_ID: " , iJacuzziNetSceneid, ", GET_CENTER_BASE_ANIM_GARAGE() = ", GET_CENTER_BASE_ANIM_GARAGE())
					#ENDIF
																																																	
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iJacuzziNetSceneid, GET_CENTER_BASE_ANIM_GARAGE(), "exit", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_USE_PHYSICS |SYNCED_SCENE_ON_ABORT_STOP_SCENE , DEFAULT,INSTANT_BLEND_IN)
					NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(objOfficeSeat, iJacuzziNetSceneid, sOfficeSeatAnimDict, "exit_CHAIR", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_NET_ON_EARLY_NON_PED_STOP_RETURN_TO_START| SYNCED_SCENE_USE_PHYSICS)
					
					NETWORK_START_SYNCHRONISED_SCENE(iJacuzziNetSceneid)
					
					//IF IS_OFFICE_ARMCHAIR_EXIT_PROMPT_SHOWN()
						//CLEAR_HELP()
						IF iOfficeSeatContextIntention != NEW_CONTEXT_INTENTION
							RELEASE_CONTEXT_INTENTION(iOfficeSeatContextIntention)
						ENDIF
					//ENDIF

					CLEAR_BIT(iOfficeSeatiBitSet, OFFICE_SEAT_BS_EXIT_HELP_TEXT_DISPLAYED)
					
					SET_OFFICE_SEAT_STAGE(g_OfficeArmChairState, OFFICE_SEAT_CLEANUP)	
				ENDIF
			ENDIF
		BREAK
		
		CASE OFFICE_SEAT_EXIT_INTERRUPT
			CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_CLEANUP: NETWORK_STOP_SYNCHRONISED_SCENE")
			NETWORK_STOP_SYNCHRONISED_SCENE(iJacuzziNetSceneid)
			SET_OFFICE_SEAT_STAGE(g_OfficeArmChairState, OFFICE_SEAT_PUT_ON_CLOTHES)	
			OFFICE_SEAT_ACTIVITY_CLEAN_UP_GARAGE()
		BREAK
		
		CASE OFFICE_SEAT_TV_EXIT_SEAT
			IF iPlayerChosenSeatSlot != -1 
				iJacuzziNetSceneid = NETWORK_CREATE_SYNCHRONISED_SCENE(mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vLoc, mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vRot, DEFAULT, TRUE, FALSE)

				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iJacuzziNetSceneid, sOfficeSeatAnimDict, "exit", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_USE_PHYSICS |SYNCED_SCENE_ON_ABORT_STOP_SCENE ,  RBF_PLAYER_IMPACT,INSTANT_BLEND_IN)
	
				NETWORK_START_SYNCHRONISED_SCENE(iJacuzziNetSceneid)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "iJacuzziNetSceneid = ", iJacuzziNetSceneid)
				
//				IF IS_OFFICE_ARMCHAIR_EXIT_PROMPT_SHOWN()
//					CLEAR_HELP()
					IF iOfficeSeatContextIntention != NEW_CONTEXT_INTENTION
						RELEASE_CONTEXT_INTENTION(iOfficeSeatContextIntention)
					ENDIF
//				ENDIF
				CLEAR_BIT(iOfficeSeatiBitSet, OFFICE_SEAT_BS_EXIT_HELP_TEXT_DISPLAYED)

				CDEBUG1LN(DEBUG_SAFEHOUSE, "exiting activity")
				SET_OFFICE_SEAT_STAGE(g_OfficeArmChairState, OFFICE_SEAT_CLEANUP_WAIT)
			ENDIF
		BREAK
		
		CASE OFFICE_SEAT_CLEANUP_WAIT
			localScene  = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iJacuzziNetSceneid)
			IF localScene != -1
				CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_CLEANUP_WAIT: scene has started so now going to cleanup stage")
				SET_OFFICE_SEAT_STAGE(g_OfficeArmChairState, OFFICE_SEAT_CLEANUP)
			ENDIF
		BREAK
		
		CASE OFFICE_SEAT_CLEANUP
			
			localScene  = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iJacuzziNetSceneid)
			IF localScene != -1
				
				IF IS_PROPERTY_OFFICE(iCurrentProperty)
					IF HAS_ANIM_EVENT_FIRED (PLAYER_PED_ID(), HASH("DESTROY_WEAPON"))
						IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_REVOLVER)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_CHECK_ENTER_OR_IDLE_ANIM: DESTROY_WEAPON")
//							SET_ENTITY_VISIBLE(NET_TO_ENT(niOfficeSeatRevolver), FALSE)
						ENDIF
					ENDIF
				ENDIF

				
				IF GET_SYNCHRONIZED_SCENE_PHASE(localScene) >= 1
				OR HAS_ANIM_EVENT_FIRED (PLAYER_PED_ID(), HASH("BREAKOUT_FINISH"))
				OR HAS_ANIM_EVENT_FIRED (PLAYER_PED_ID(), HASH("BREAK_OUT"))
					#IF IS_DEBUG_BUILD
					IF HAS_ANIM_EVENT_FIRED (PLAYER_PED_ID(), HASH("BREAKOUT_FINISH"))
						CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_CLEANUP: Finishing anim early as BREAKOUT_FINISH anim event fired")
					ENDIF
					IF HAS_ANIM_EVENT_FIRED (PLAYER_PED_ID(), HASH("BREAK_OUT"))
						CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_CLEANUP: Finishing anim early as BREAK_OUT anim event fired")
					ENDIF
					#ENDIF 
					CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_CLEANUP: NETWORK_STOP_SYNCHRONISED_SCENE")
					NETWORK_STOP_SYNCHRONISED_SCENE(iJacuzziNetSceneid)
					SET_OFFICE_SEAT_STAGE(g_OfficeArmChairState, OFFICE_SEAT_PUT_ON_CLOTHES)	
					OFFICE_SEAT_ACTIVITY_CLEAN_UP_GARAGE()
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_CLEANUP: Scene isn't running cleanup anyway")
//				SET_OFFICE_SEAT_STAGE(g_OfficeArmChairState, OFFICE_SEAT_LOCATECHECK)
//				OFFICE_SEAT_ACTIVITY_CLEAN_UP_GARAGE()
			ENDIF
			
//			playerBD[NATIVE_TO_INT(PLAYER_ID())].iJacuzziSeatUsed = -1
			
		BREAK
		
		CASE OFFICE_SEAT_PUT_ON_CLOTHES

			SET_OFFICE_SEAT_STAGE(g_OfficeArmChairState, OFFICE_SEAT_LOCATECHECK)

		BREAK
	ENDSWITCH
	

ENDPROC

FUNC BOOL CHECK_SV_EXIST()
	INT i
	INT iCount
	
	REPEAT MAX_SVM_VEHICLES_IN_GARAGE i
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.customVehicleNetIDs[i])
			PRINTLN("SV exists #", i)
			iCount++
		ELSE
			PRINTLN("SV doesn't exist #", i)
		ENDIF
	ENDREPEAT
	
	IF iCount = serverBD.iVehicleCreated
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ MAIN LOOP ╞══════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛
SCRIPT  (SIMPLE_INTERIOR_CHILD_SCRIPT_DATA scriptData) 
	
	
	IF NETWORK_IS_GAME_IN_PROGRESS() 
		SCRIPT_INITIALISE(scriptData)	
	ELSE
		SCRIPT_CLEANUP()
	ENDIF
	
	// Main loop
	WHILE TRUE

		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
		
		// Fix for B*3176414
		CAMERA_PREVENT_COLLISION_SETTINGS_FOR_TRIPLEHEAD_IN_INTERIORS_THIS_UPDATE()
		
		// Fix for B*3213226
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_SuppressNavmeshForEnterVehicleTask, TRUE)
		ENDIF
		
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ROCKET_BOOST)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CAR_JUMP)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PARACHUTE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ROOF)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_AIM)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_ATTACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BIKE_WINGS)
		
//		RUN_OFFICE_SEAT_ACTIVITY(officeSeatStruct)		
//		CREATE_MECHANIC_PED_FOR_FLOW_PLAY()		
		RUN_OFFICE_SEAT_ARMCHAIR_ACTIVITY()
		RUN_OFFICE_SEAT_ACTIVITY(activitySeatStruct, serverBD.serverSeatBD)
		MAINTAIN_VEHICLE_WAREHOUSE_LAPTOP_SCREEN()
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("AM_MP_IE_WAREHOUSE - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
			SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE(scriptData.iScriptInstance, thisPERSONAL_MOD_GARAGE.eSimpleInteriorID)
			PRINTLN("AM_MP_IE_WAREHOUSE - SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE = TRUE")
			SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
			SCRIPT_CLEANUP()
		ENDIF
		
		IF thisPERSONAL_MOD_GARAGE.pOwner = PLAYER_ID()
		AND NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		AND CHECK_SV_EXIST()
		AND NOT g_SpawnData.bSpawningInSImpleInterior
			NETWORK_REQUEST_TO_BE_HOST_OF_THIS_SCRIPT()
		ENDIF
		
		bGetOutOfWayFlagClearedThisFrame = FALSE
		
		IF MAINTAIN_GET_OUT_OF_THE_WAY_SV()
			playerBD[NATIVE_TO_INT(PLAYER_ID())].sGetOutOfTheWay.iPlayerBitSet = 0
			sGetOutOfWayVars.iProgressBitSet = 0
		ENDIF
		
		SERVER_CLEANUP_PLAYERS_ARE_IN_THE_WAY_SV_FLAGS()
		
		IF NOT IS_PERSONAL_MOD_GARAGE_STATE(PERSONAL_MOD_GARAGE_STATE_IDLE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
		ENDIF
		
		IF IS_PERSONAL_MOD_GARAGE_SERVER_STATE(PERSONAL_MOD_GARAGE_SERVER_STATE_IDLE)
			RUN_MAIN_CLIENT_LOGIC()
		ENDIF
		
		MAINTAIN_INTERIOR_BED_SPAWN_ACTIVITIES()
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			RUN_MAIN_SERVER_LOGIC()
		ENDIF
		
		#IF IS_DEBUG_BUILD
			UPDATE_DEBUG_WIDGETS()
		#ENDIF
		
	ENDWHILE
	

ENDSCRIPT
