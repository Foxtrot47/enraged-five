
USING "globals.sch"
USING "rage_builtins.sch"

CONST_INT ENABLE_CONTENT	FEATURE_SUMMER_2020

#IF ENABLE_CONTENT
USING "variations/fm_content_business_battles_aircraft_carrier.sch"
USING "variations/fm_content_business_battles_captured_ufo.sch"
USING "variations/fm_content_business_battles_factory_raid.sch"

/// PURPOSE: Use to set up the logic pointers for your mission
///    
PROC SETUP_MISSION_LOGIC()
	SWITCH GET_VARIATION()
		CASE BBCV_FACTORY_RAID
			FACTORY_RAID_SETUP_LOGIC()
		BREAK
		CASE BBCV_AIRCRAFT_CARRIER
			AIRCRAFT_CARRIER_SETUP_LOGIC()
		BREAK
		CASE BBCV_CAPTURED_UFO
			CAPTURED_UFO_SETUP_LOGIC()
		BREAK
	ENDSWITCH
ENDPROC
#ENDIF
