USING "globals.sch"
USING "rage_builtins.sch"

USING "fm_content_business_battles_structs.sch"
USING "fm_content_fm_event_structs.sch"
USING "fm_content_generic_structs.sch"

FUNC FMCBB_VARIATION GET_VARIATION()
	RETURN serverBD.sMissionData.eMissionVariation
ENDFUNC

FUNC FMCBB_SUBVARIATION GET_SUBVARIATION()
	RETURN serverBD.sMissionData.eMissionSubvariation
ENDFUNC

FUNC FMBB_TYPE GET_BUSINESS_TYPE()
	RETURN serverBD.sMissionData.eBusinessType
ENDFUNC

FUNC BLIP_SPRITE GET_MISSION_ENTITY_OVERRIDE_SPRITE()
	SWITCH GET_VARIATION()
		CASE BBCV_CAPTURED_UFO		RETURN RADAR_TRACE_UFO
	ENDSWITCH
	RETURN RADAR_TRACE_BAT_CARGO
ENDFUNC

FUNC STRING GET_VARIATION_NAME_FOR_DEBUG()
	RETURN BUSINESS_BATTLE_GET_VARIATION_NAME_FOR_DEBUG(GET_VARIATION())
ENDFUNC

FUNC STRING GET_SUBVARIATION_NAME_FOR_DEBUG()
	RETURN BUSINESS_BATTLE_GET_SUBVARIATION_NAME_FOR_DEBUG(GET_SUBVARIATION())
ENDFUNC

FUNC BOOL MISSION_SERVER_INIT()
	SWITCH GET_VARIATION()
		CASE BBCV_CAPTURED_UFO		RETURN TRUE
	ENDSWITCH
	
	serverBD.eSecondaryDropoff = FMBB_GET_RANDOM_SECONDARY_DROPOFF_FOR_BATTLE(CALL logic.Participation.Coords()) 
	
	RETURN TRUE
ENDFUNC

PROC SET_MODE_TIMER_PLACEMENT_DATA()
	INT iDuration
	SWITCH GET_VARIATION()
		CASE BBCV_CAPTURED_UFO			iDuration = g_sMPTunables.iSUM_BB_CAPTURE_UFO_DURATION			BREAK
		CASE BBCV_FACTORY_RAID			iDuration = g_sMPTunables.iSUM_BB_CHICKEN_FACTORY_DURATION		BREAK
		CASE BBCV_AIRCRAFT_CARRIER		iDuration = g_sMPTunables.iSUM_BB_AIRCRAFT_CARRIER_DURATION		BREAK
	ENDSWITCH
	
	data.ModeTimer.iTimeLimit = (iDuration / 60) / 1000
ENDPROC

FUNC XPCATEGORY GET_DEFAULT_RP_CATEGORY()
	RETURN XPCATEGORY_FM_CONTENT_SUM20_BUSINESS_BATTLE
ENDFUNC

PROC PROCESS_ON_DELIVERY_CASH_REWARD(INT iCashToGive)
	IF iCashToGive > 0
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] PROCESS_ON_DELIVERY_CASH_REWARD - iCashToGive = ", iCashToGive)
	
		IF USE_SERVER_TRANSACTIONS()
			INT iTransactionIndex
			TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_FROM_BUSINESS_BATTLE, iCashToGive, iTransactionIndex, FALSE, TRUE, FALSE)
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] PROCESS_ON_DELIVERY_CASH_REWARD - TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION, SERVICE_EARN_FROM_BUSINESS_BATTLE")
		ELSE
			NETWORK_EARN_FROM_BUSINESS_BATTLE(iCashToGive)
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] PROCESS_ON_DELIVERY_CASH_REWARD - NETWORK_EARN_FROM_BUSINESS_BATTLE")
		ENDIF
		
		sTelemetry.iCashEarned += iCashToGive
	ELSE
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] PROCESS_ON_DELIVERY_CASH_REWARD - iCashToGive <= 0")
	ENDIF
ENDPROC

PROC PROCESS_ON_DELIVERY_RP_REWARD(INT iRpToGive)
	IF iRpToGive > 0
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] PROCESS_ON_DELIVERY_RP_REWARD - iRpToGive = ", iRpToGive)
	
		GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "", XPTYPE_COMPLETE, CALL logic.Reward.RpCategory(), iRpToGive)
		
		sTelemetry.iRpEarned += iRpToGive
	ELSE
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] PROCESS_ON_DELIVERY_RP_REWARD - iRpToGive <= 0")
	ENDIF
ENDPROC

FUNC BOOL SAFE_FOR_VALUE_TICKER()
	IF NOT NETWORK_IS_IN_MP_CUTSCENE()
	AND NOT IS_HUD_HIDDEN()
	AND IS_PLAYER_CONTROL_ON(player_id())
	AND NOT IS_SCREEN_FADED_OUT()
	AND NOT IS_SCREEN_FADING_OUT()
		RETURN TRUE
	ENDIF	
	RETURN FALSE
ENDFUNC

PROC PROCESS_REWARD_TICKER()
	IF GET_BUSINESS_TYPE() != BBT_EVENT
		IF HAS_PLAYER_GANG_DELIVERED_ANY_MISSION_ENTITIES(LOCAL_PLAYER_INDEX)	
		AND NOT GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE)
		AND FMBB_DROPOFF_IS_PLAYER_PROPERTY(GET_DROPOFF())
			IF SAFE_FOR_VALUE_TICKER()
				TRIGGER_BUSINESS_HUB_STOCK_VALUE_TICKER()
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] GIVE_REWARD - SAFE_FOR_VALUE_TICKER = TRUE - Sending ticker for TRIGGER_BUSINESS_HUB_STOCK_VALUE_TICKER")
			ELSE
				MPGlobalsAmbience.bSendFinalBusinessEntityTicker = TRUE
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] GIVE_REWARD - SAFE_FOR_VALUE_TICKER = FALSE - MPGlobalsAmbience.bSendFinalBusinessEntityTicker = TRUE, dropoff = ",FREEMODE_DELIVERY_GET_DROPOFF_DEBUG_NAME(GET_DROPOFF()))
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CALCULATE_MINIMUM_PARTICIPATION(INT& iCashToGive, INT& iRpToGive)
	IF NOT FMBB_IS_MINIMUM_PARTICIPATION_INITIALISED()
		PRINTLN("[",scriptName,"][GIVE_REWARD] - Minimum participation has not been initialised, abandon calculation.")
		EXIT
	ENDIF

	PRINTLN("[",scriptName,"][GIVE_REWARD] - Minimum participation has been initialised, calculating...")
	
	PRINTLN("[",scriptName,"][GIVE_REWARD] - iCashToGive before  = $", iCashToGive)
	PRINTLN("[",scriptName,"][GIVE_REWARD] - iRpToGive before = ", iRpToGive, "RP")

	INT iMinPartCash, iMinPartRP

	IF logic.Reward.MinPartCash != NULL
		iMinPartCash = CALL logic.Reward.MinPartCash()
	ENDIF
	
	IF logic.Reward.MinPartRp != NULL
		iMinPartRP = CALL logic.Reward.MinPartRp()
	ENDIF
			
	INT iTimeDiffMs = ABSI(GET_TIME_DIFFERENCE(MPGlobalsAmbience.tdBusinessBattleParticipationTime, GET_NETWORK_TIME()))
	
	IF logic.Reward.MinPartTimeCap != NULL
		IF iTimeDiffMs > CALL logic.Reward.MinPartTimeCap()
			iTimeDiffMs = CALL logic.Reward.MinPartTimeCap()
			PRINTLN("[",scriptName,"][GIVE_REWARD] - iTimeDiffMs is greater than cap of ", iTimeDiffMs, "ms, capping.")
		ENDIF
	ENDIF
	
	INT iTimeDiffMins = FLOOR(TO_FLOAT(iTimeDiffMs) / (1000.0 * 60.0))
	IF iTimeDiffMins < 1
		iTimeDiffMins = 1
	ENDIF
	
	iMinPartCash = (iTimeDiffMins * iMinPartCash)
	iMinPartRP = (iTimeDiffMins * iMinPartRP)
		
	iCashToGive += iMinPartCash
	iRpToGive += iMinPartRP
	
	PRINTLN("[",scriptName,"][GIVE_REWARD] - iCashToGive after = $", iCashToGive)
	PRINTLN("[",scriptName,"][GIVE_REWARD] - iRpToGive after = ", iRpToGive)
	PRINTLN("[",scriptName,"][GIVE_REWARD] - 			INFO [iMinPartCash: 	$", iMinPartCash, ", iMinPartRP: ", iMinPartRP, "]")
	PRINTLN("[",scriptName,"][GIVE_REWARD] - 			INFO [iTimeDiffMs: 	", iTimeDiffMs, ", 	iTimeDiffMins: ", iTimeDiffMins, "]")
		
ENDPROC

PROC PROCESS_REWARD_EARN(INT iCashToGive)
	IF USE_SERVER_TRANSACTIONS()
		PRINTLN("[",scriptName,"][GIVE_REWARD] - TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_FROM_BUSINESS_BATTLE, ", iCashToGive, ", iScriptTransactionIndex, FALSE, TRUE)")
		INT iScriptTransactionIndex
		TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_FROM_BUSINESS_BATTLE, iCashToGive, iScriptTransactionIndex, FALSE, TRUE) //Add new service
	ELSE
		PRINTLN("[",scriptName,"][GIVE_REWARD] - NETWORK_EARN_FROM_BUSINESS_BATTLE = ", iCashToGive)
		NETWORK_EARN_FROM_BUSINESS_BATTLE(iCashToGive)  
	ENDIF
ENDPROC

PROC _DEPRECATED_GIVE_REWARD()
	IF playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iGoodsToTransfer > 0
		PRINTLN("[",scriptName,"][GIVE_REWARD] - Waiting on Goods to transfer before end. Goods remaining: ", playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iGoodsToTransfer)
		EXIT
	ENDIF
	
	IF IS_GENERIC_CLIENT_BIT_SET(LOCAL_PARTICIPANT_INDEX, eGENERICCLIENTBITSET_COMPLETED_REWARDS)
		EXIT
	ENDIF

	INT iCashToGive, iRpToGive
	
	IF IS_GENERIC_BIT_SET(eGENERICBITSET_I_WON)
		PRINTLN("[",scriptName,"][GIVE_REWARD] - eGENERICBITSET_I_WON is set, local player was a winner")
		IF logic.Reward.WinnerCash != NULL
			iCashToGive += CALL logic.Reward.WinnerCash()
		ENDIF
		IF logic.Reward.WinnerRp != NULL
			iRpToGive += CALL logic.Reward.WinnerRp()
		ENDIF
	ELSE
		PRINTLN("[",scriptName,"][GIVE_REWARD] - eGENERICBITSET_I_WON is not set, local player was not a winner")
	ENDIF
	
	CALCULATE_MINIMUM_PARTICIPATION(iCashToGive, iRpToGive)

	PRINTLN("[",scriptName,"][GIVE_REWARD] - iCashToGive before multipliers = $", iCashToGive)
	PRINTLN("[",scriptName,"][GIVE_REWARD] - iRpToGive before multipliers = ", iRpToGive, "RP")
	
	IF logic.Reward.Multiplier != NULL
		iCashToGive = ROUND(iCashToGive * CALL logic.Reward.Multiplier(eREWARDTYPE_CASH))
		iRpToGive = ROUND (iRpToGive * CALL logic.Reward.Multiplier(eREWARDTYPE_RP))
	ENDIF
	
	PRINTLN("[",scriptName,"][GIVE_REWARD] - iCashToGive after multipliers = $", iCashToGive)
	PRINTLN("[",scriptName,"][GIVE_REWARD] - iRpToGive after multipliers = ", iRpToGive, "RP")

	IF iCashToGive > 0
		PRINTLN("[",scriptName,"][GIVE_REWARD] - Giving player $", iCashToGive)
		PROCESS_REWARD_EARN(iCashToGive)
		
		IF data.Reward.bShowCashWithShard
			g_i_cashForEndEventShard = iCashToGive
			PRINTLN("[",scriptName,"][GIVE_REWARD] - g_i_cashForEndEventShard = iCashToGive = ", g_i_cashForEndEventShard)
			REINIT_NET_TIMER(st_ScriptTimers_BIG_MESSAGE_CashDisplay)
		ENDIF
	ENDIF
	IF iRpToGive > 0
		PRINTLN("[",scriptName,"][GIVE_REWARD] - Giving player ", iRpToGive, "RP")
		IF data.Reward.bShowRankbarOnRPEarn
			NEXT_RP_ADDITION_SHOW_RANKBAR()
		ENDIF
		GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, LOCAL_PED_INDEX, "", XPTYPE_COMPLETE, CALL logic.Reward.RpCategory(), iRpToGive)
	ENDIF
	
	sTelemetry.iCashEarned += iCashToGive
	sTelemetry.iRpEarned += iRpToGive
	
	SET_GENERIC_CLIENT_BIT(eGENERICCLIENTBITSET_COMPLETED_REWARDS)
ENDPROC

FUNC FREEMODE_DELIVERABLE_TYPE GET_MISSION_ENTITY_DELIVERABLE_TYPE()
	SWITCH GET_BUSINESS_TYPE()
		CASE BBT_WAREHOUSE		RETURN BBT_DELIVERABLE_CEO_PRODUCT
		CASE BBT_BIKER_FACTORY	RETURN BBT_DELIVERABLE_BIKER_PRODUCT
		CASE BBT_BUNKER			RETURN BBT_DELIVERABLE_BUNKER_PRODUCT
		CASE BBT_HANGAR			RETURN BBT_DELIVERABLE_HANGAR_PRODUCT
		CASE BBT_EVENT			RETURN AW_DELIVERABLE_EVENT_ITEM
		CASE BBT_UFO_PARTS		RETURN FMC_FFA_DELIVERABLE
	ENDSWITCH
	RETURN BBT_DELIVERABLE_CEO_PRODUCT
ENDFUNC

FUNC STRING GET_DROPOFF_LOCATION_LABEL()
	IF GET_BUSINESS_TYPE() = BBT_UFO_PARTS
		RETURN "FMC_LOC_OMEGA"
	ENDIF
	
	IF FMBB_DROPOFF_IS_BUS_BATTLE_SECONDARY(GET_DROPOFF())
		RETURN "BBT_DROPOFF"
	ENDIF
	RETURN "BBT_NIGHTCLUB"
ENDFUNC

FUNC STRING GET_PARTICIPATION_RANGE_PROMPT_STRING()
	RETURN "BHH_LEFTRANGE"
ENDFUNC

PROC OVERRIDE_MISSION_ENTITY_DATA_FOR_BUSINESS_TYPE_MODEL()
	
	SWITCH GET_VARIATION()
		CASE BBCV_AIRCRAFT_CARRIER
			SWITCH GET_BUSINESS_TYPE()
				CASE BBT_WAREHOUSE
				CASE BBT_HANGAR
					data.MissionEntity.MissionEntities[0].model     = ba_prop_battle_case_sm_03
					data.MissionEntity.MissionEntities[0].vCoords   = <<3062.2236, -4662.7813, 14.5228>>
					data.MissionEntity.MissionEntities[0].fHeading  = 285.3997
					data.MissionEntity.MissionEntities[0].vRotation = <<0.0000, 0.0000, -74.6003>>

					data.MissionEntity.MissionEntities[1].model     = ba_prop_battle_case_sm_03
					data.MissionEntity.MissionEntities[1].vCoords   = <<3095.3152, -4719.0786, 14.5240>>
					data.MissionEntity.MissionEntities[1].fHeading  = 0.0000
					data.MissionEntity.MissionEntities[1].vRotation = <<0.0000, 0.0000, 0.0000>>

					data.MissionEntity.MissionEntities[2].model     = ba_prop_battle_case_sm_03
					data.MissionEntity.MissionEntities[2].vCoords   = <<3075.7849, -4725.8022, 5.3387>>
					data.MissionEntity.MissionEntities[2].fHeading  = 248.1993
					data.MissionEntity.MissionEntities[2].vRotation = <<0.0000, -0.0000, -111.8007>>

					data.MissionEntity.MissionEntities[3].model     = ba_prop_battle_case_sm_03
					data.MissionEntity.MissionEntities[3].vCoords   = <<3053.5381, -4713.4683, 5.3387>>
					data.MissionEntity.MissionEntities[3].fHeading  = 244.5980
					data.MissionEntity.MissionEntities[3].vRotation = <<0.0000, -0.0000, -115.4020>>

					data.MissionEntity.MissionEntities[4].model     = ba_prop_battle_case_sm_03
					data.MissionEntity.MissionEntities[4].vCoords   = <<3035.0798, -4689.1748, 5.3387>>
					data.MissionEntity.MissionEntities[4].fHeading  = 105.9973
					data.MissionEntity.MissionEntities[4].vRotation = <<0.0000, -0.0000, 105.9973>>

					data.MissionEntity.MissionEntities[5].model     = ba_prop_battle_case_sm_03
					data.MissionEntity.MissionEntities[5].vCoords   = <<3047.5654, -4646.2876, 5.3387>>
					data.MissionEntity.MissionEntities[5].fHeading  = 331.3967
					data.MissionEntity.MissionEntities[5].vRotation = <<0.0000, 0.0000, -28.6033>>

					data.MissionEntity.MissionEntities[6].model     = ba_prop_battle_case_sm_03
					data.MissionEntity.MissionEntities[6].vCoords   = <<3069.0557, -4688.5132, 14.5237>>
					data.MissionEntity.MissionEntities[6].fHeading  = 358.8000
					data.MissionEntity.MissionEntities[6].vRotation = <<0.0000, 0.0000, -1.2000>>

					data.MissionEntity.MissionEntities[7].model     = ba_prop_battle_case_sm_03
					data.MissionEntity.MissionEntities[7].vCoords   = <<3088.5500, -4744.4297, 14.5237>>
					data.MissionEntity.MissionEntities[7].fHeading  = 21.2000
					data.MissionEntity.MissionEntities[7].vRotation = <<0.0000, 0.0000, 21.1999>>
				BREAK
				
				CASE BBT_BIKER_FACTORY
					data.MissionEntity.MissionEntities[0].model     = ba_Prop_Battle_drug_package_02
					data.MissionEntity.MissionEntities[0].vCoords   = <<3062.1426, -4661.9575, 14.3979>>
					data.MissionEntity.MissionEntities[0].fHeading  = 285.3997
					data.MissionEntity.MissionEntities[0].vRotation = <<0.0000, 0.0000, -74.6003>>

					data.MissionEntity.MissionEntities[1].model     = ba_Prop_Battle_drug_package_02
					data.MissionEntity.MissionEntities[1].vCoords   = <<3095.3677, -4718.2310, 14.3991>>
					data.MissionEntity.MissionEntities[1].fHeading  = 304.9998
					data.MissionEntity.MissionEntities[1].vRotation = <<0.0000, 0.0000, -55.0001>>

					data.MissionEntity.MissionEntities[2].model     = ba_Prop_Battle_drug_package_02
					data.MissionEntity.MissionEntities[2].vCoords   = <<3074.5149, -4725.3208, 5.7801>>
					data.MissionEntity.MissionEntities[2].fHeading  = 230.3991
					data.MissionEntity.MissionEntities[2].vRotation = <<0.0000, -0.0000, -129.6009>>

					data.MissionEntity.MissionEntities[3].model     = ba_Prop_Battle_drug_package_02
					data.MissionEntity.MissionEntities[3].vCoords   = <<3055.0862, -4715.6187, 5.2137>>
					data.MissionEntity.MissionEntities[3].fHeading  = 291.3976
					data.MissionEntity.MissionEntities[3].vRotation = <<0.0000, 0.0000, -68.6024>>

					data.MissionEntity.MissionEntities[4].model     = ba_Prop_Battle_drug_package_02
					data.MissionEntity.MissionEntities[4].vCoords   = <<3036.4663, -4687.5610, 5.2137>>
					data.MissionEntity.MissionEntities[4].fHeading  = 55.3971
					data.MissionEntity.MissionEntities[4].vRotation = <<0.0000, 0.0000, 55.3971>>

					data.MissionEntity.MissionEntities[5].model     = ba_Prop_Battle_drug_package_02
					data.MissionEntity.MissionEntities[5].vCoords   = <<3048.0352, -4645.3208, 6.1380>>
					data.MissionEntity.MissionEntities[5].fHeading  = 326.1966
					data.MissionEntity.MissionEntities[5].vRotation = <<0.0000, 0.0000, -33.8034>>

					data.MissionEntity.MissionEntities[6].model     = ba_Prop_Battle_drug_package_02
					data.MissionEntity.MissionEntities[6].vCoords   = <<3067.0728, -4689.0645, 14.3988>>
					data.MissionEntity.MissionEntities[6].fHeading  = 346.0000
					data.MissionEntity.MissionEntities[6].vRotation = <<0.0000, 0.0000, -14.0000>>

					data.MissionEntity.MissionEntities[7].model     = ba_Prop_Battle_drug_package_02
					data.MissionEntity.MissionEntities[7].vCoords   = <<3089.9221, -4743.8535, 14.3987>>
					data.MissionEntity.MissionEntities[7].fHeading  = 348.7999
					data.MissionEntity.MissionEntities[7].vRotation = <<0.0000, 0.0000, -11.2001>>
				BREAK
				
				CASE BBT_BUNKER
					data.MissionEntity.MissionEntities[0].model     = ba_Prop_Battle_rsply_crate_gr_02a
					data.MissionEntity.MissionEntities[0].vCoords   = <<3061.8120, -4661.0313, 14.4515>>
					data.MissionEntity.MissionEntities[0].fHeading  = -74.6003
					data.MissionEntity.MissionEntities[0].vRotation = <<0.0000, 0.0000, -74.6003>>

					data.MissionEntity.MissionEntities[1].model     = ba_Prop_Battle_rsply_crate_gr_02a
					data.MissionEntity.MissionEntities[1].vCoords   = <<3094.5181, -4720.2173, 14.4527>>
					data.MissionEntity.MissionEntities[1].fHeading  = 270.3995
					data.MissionEntity.MissionEntities[1].vRotation = <<0.0000, 0.0000, -89.6005>>

					data.MissionEntity.MissionEntities[2].model     = ba_Prop_Battle_rsply_crate_gr_02a
					data.MissionEntity.MissionEntities[2].vCoords   = <<3076.4590, -4726.7007, 5.2674>>
					data.MissionEntity.MissionEntities[2].fHeading  = 286.1986
					data.MissionEntity.MissionEntities[2].vRotation = <<0.0000, 0.0000, -73.8014>>

					data.MissionEntity.MissionEntities[3].model     = ba_Prop_Battle_rsply_crate_gr_02a
					data.MissionEntity.MissionEntities[3].vCoords   = <<3053.9885, -4714.1094, 5.2674>>
					data.MissionEntity.MissionEntities[3].fHeading  = 29.9975
					data.MissionEntity.MissionEntities[3].vRotation = <<0.0000, 0.0000, 29.9975>>

					data.MissionEntity.MissionEntities[4].model     = ba_Prop_Battle_rsply_crate_gr_02a
					data.MissionEntity.MissionEntities[4].vCoords   = <<3036.3074, -4689.5444, 5.2674>>
					data.MissionEntity.MissionEntities[4].fHeading  = 10.5970
					data.MissionEntity.MissionEntities[4].vRotation = <<0.0000, 0.0000, 10.5970>>

					data.MissionEntity.MissionEntities[5].model     = ba_Prop_Battle_rsply_crate_gr_02a
					data.MissionEntity.MissionEntities[5].vCoords   = <<3049.0752, -4646.4951, 5.2674>>
					data.MissionEntity.MissionEntities[5].fHeading  = 236.3963
					data.MissionEntity.MissionEntities[5].vRotation = <<0.0000, -0.0000, -123.6037>>

					data.MissionEntity.MissionEntities[6].model     = ba_Prop_Battle_rsply_crate_gr_02a
					data.MissionEntity.MissionEntities[6].vCoords   = <<3065.9890, -4688.8354, 14.4525>>
					data.MissionEntity.MissionEntities[6].fHeading  = -14.0000
					data.MissionEntity.MissionEntities[6].vRotation = <<0.0000, 0.0000, -14.0000>>

					data.MissionEntity.MissionEntities[7].model     = ba_Prop_Battle_rsply_crate_gr_02a
					data.MissionEntity.MissionEntities[7].vCoords   = <<3087.3479, -4744.9956, 14.4524>>
					data.MissionEntity.MissionEntities[7].fHeading  = 48.9999
					data.MissionEntity.MissionEntities[7].vRotation = <<0.0000, 0.0000, 48.9999>>
				BREAK
				
				CASE BBT_EVENT
					data.MissionEntity.MissionEntities[0].model     = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Crate_01a"))
					data.MissionEntity.MissionEntities[0].vCoords   = <<3061.5850, -4660.1470, 14.2615>>
					data.MissionEntity.MissionEntities[0].fHeading  = -74.6003
					data.MissionEntity.MissionEntities[0].vRotation = <<0.0000, 0.0000, -74.6003>>

					data.MissionEntity.MissionEntities[1].model     = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Crate_01a"))
					data.MissionEntity.MissionEntities[1].vCoords   = <<3095.3240, -4721.3999, 14.2627>>
					data.MissionEntity.MissionEntities[1].fHeading  = 358.9993
					data.MissionEntity.MissionEntities[1].vRotation = <<0.0000, 0.0000, -1.0007>>

					data.MissionEntity.MissionEntities[2].model     = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Crate_01a"))
					data.MissionEntity.MissionEntities[2].vCoords   = <<3073.8594, -4724.3276, 5.0774>>
					data.MissionEntity.MissionEntities[2].fHeading  = 49.1984
					data.MissionEntity.MissionEntities[2].vRotation = <<0.0000, 0.0000, 49.1984>>

					data.MissionEntity.MissionEntities[3].model     = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Crate_01a"))
					data.MissionEntity.MissionEntities[3].vCoords   = <<3052.6997, -4713.0664, 5.0774>>
					data.MissionEntity.MissionEntities[3].fHeading  = 65.9974
					data.MissionEntity.MissionEntities[3].vRotation = <<0.0000, 0.0000, 65.9974>>

					data.MissionEntity.MissionEntities[4].model     = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Crate_01a"))
					data.MissionEntity.MissionEntities[4].vCoords   = <<3035.9565, -4688.4985, 5.0774>>
					data.MissionEntity.MissionEntities[4].fHeading  = 196.3969
					data.MissionEntity.MissionEntities[4].vRotation = <<0.0000, -0.0000, -163.6030>>

					data.MissionEntity.MissionEntities[5].model     = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Crate_01a"))
					data.MissionEntity.MissionEntities[5].vCoords   = <<3046.9724, -4645.2085, 5.0774>>
					data.MissionEntity.MissionEntities[5].fHeading  = 243.5963
					data.MissionEntity.MissionEntities[5].vRotation = <<0.0000, -0.0000, -116.4038>>

					data.MissionEntity.MissionEntities[6].model     = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Crate_01a"))
					data.MissionEntity.MissionEntities[6].vCoords   = <<3069.3318, -4687.2383, 14.2625>>
					data.MissionEntity.MissionEntities[6].fHeading  = -14.0000
					data.MissionEntity.MissionEntities[6].vRotation = <<0.0000, 0.0000, -14.0000>>

					data.MissionEntity.MissionEntities[7].model     = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Crate_01a"))
					data.MissionEntity.MissionEntities[7].vCoords   = <<3087.2654, -4746.1206, 14.2624>>
					data.MissionEntity.MissionEntities[7].fHeading  = 105.5996
					data.MissionEntity.MissionEntities[7].vRotation = <<0.0000, -0.0000, 105.5996>>
				BREAK
				
				DEFAULT
					data.MissionEntity.MissionEntities[0].model     = ba_Prop_Battle_Bag_01a
					data.MissionEntity.MissionEntities[0].vCoords   = <<3063.8723, -4663.2891, 14.2680>>
					data.MissionEntity.MissionEntities[0].fHeading  = 195.3995
					data.MissionEntity.MissionEntities[0].vRotation = <<0.0000, -0.0000, -164.6005>>

					data.MissionEntity.MissionEntities[1].model     = ba_Prop_Battle_Bag_01a
					data.MissionEntity.MissionEntities[1].vCoords   = <<3096.4673, -4722.1855, 14.2692>>
					data.MissionEntity.MissionEntities[1].fHeading  = 232.5993
					data.MissionEntity.MissionEntities[1].vRotation = <<0.0000, -0.0000, -127.4007>>

					data.MissionEntity.MissionEntities[2].model     = ba_Prop_Battle_Bag_01a
					data.MissionEntity.MissionEntities[2].vCoords   = <<3074.5781, -4724.5986, 5.0839>>
					data.MissionEntity.MissionEntities[2].fHeading  = 319.3983
					data.MissionEntity.MissionEntities[2].vRotation = <<0.0000, 0.0000, -40.6017>>

					data.MissionEntity.MissionEntities[3].model     = ba_Prop_Battle_Bag_01a
					data.MissionEntity.MissionEntities[3].vCoords   = <<3054.7603, -4714.6899, 5.0839>>
					data.MissionEntity.MissionEntities[3].fHeading  = 105.9973
					data.MissionEntity.MissionEntities[3].vRotation = <<0.0000, -0.0000, 105.9973>>

					data.MissionEntity.MissionEntities[4].model     = ba_Prop_Battle_Bag_01a
					data.MissionEntity.MissionEntities[4].vCoords   = <<3037.7258, -4689.8115, 5.0839>>
					data.MissionEntity.MissionEntities[4].fHeading  = 277.5970
					data.MissionEntity.MissionEntities[4].vRotation = <<0.0000, 0.0000, -82.4030>>

					data.MissionEntity.MissionEntities[5].model     = ba_Prop_Battle_Bag_01a
					data.MissionEntity.MissionEntities[5].vCoords   = <<3048.6052, -4645.7456, 6.0244>>
					data.MissionEntity.MissionEntities[5].fHeading  = 328.5960
					data.MissionEntity.MissionEntities[5].vRotation = <<0.0000, 0.0000, -31.4040>>

					data.MissionEntity.MissionEntities[6].model     = ba_Prop_Battle_Bag_01a
					data.MissionEntity.MissionEntities[6].vCoords   = <<3070.7710, -4687.8604, 14.2690>>
					data.MissionEntity.MissionEntities[6].fHeading  = 334.6000
					data.MissionEntity.MissionEntities[6].vRotation = <<0.0000, 0.0000, -25.4000>>

					data.MissionEntity.MissionEntities[7].model     = ba_Prop_Battle_Bag_01a
					data.MissionEntity.MissionEntities[7].vCoords   = <<3090.9080, -4744.5654, 14.2689>>
					data.MissionEntity.MissionEntities[7].fHeading  = 145.3992
					data.MissionEntity.MissionEntities[7].vRotation = <<0.0000, -0.0000, 145.3992>>
				BREAK
			ENDSWITCH
		BREAK
		
		CASE BBCV_FACTORY_RAID
			SWITCH GET_SUBVARIATION()
				CASE BBCS_FR_CHICKEN_FACTORY
					SWITCH GET_BUSINESS_TYPE()
						CASE BBT_WAREHOUSE
						CASE BBT_HANGAR			// ba_prop_battle_case_sm_03
							data.MissionEntity.MissionEntities[0].model     = ba_prop_battle_case_sm_03
							data.MissionEntity.MissionEntities[0].vCoords   = <<-117.8916, 6180.4175, 30.2809>>
							data.MissionEntity.MissionEntities[0].fHeading  = 55.5999
							data.MissionEntity.MissionEntities[0].vRotation = <<0.0000, 0.0000, 55.5999>>

							data.MissionEntity.MissionEntities[1].model     = ba_prop_battle_case_sm_03
							data.MissionEntity.MissionEntities[1].vCoords   = <<-147.1199, 6152.9492, 31.7933>>
							data.MissionEntity.MissionEntities[1].fHeading  = 55.5999
							data.MissionEntity.MissionEntities[1].vRotation = <<0.0000, 0.0000, 55.5999>>

							data.MissionEntity.MissionEntities[2].model     = ba_prop_battle_case_sm_03
							data.MissionEntity.MissionEntities[2].vCoords   = <<-156.2971, 6142.6421, 31.5965>>
							data.MissionEntity.MissionEntities[2].fHeading  = 314.9998
							data.MissionEntity.MissionEntities[2].vRotation = <<0.0000, 0.0000, -45.0002>>

							data.MissionEntity.MissionEntities[3].model     = ba_prop_battle_case_sm_03
							data.MissionEntity.MissionEntities[3].vCoords   = <<-166.4463, 6142.1167, 30.8643>>
							data.MissionEntity.MissionEntities[3].fHeading  = -45.0002
							data.MissionEntity.MissionEntities[3].vRotation = <<0.0000, 0.0000, -45.0002>>

							data.MissionEntity.MissionEntities[4].model     = ba_prop_battle_case_sm_03
							data.MissionEntity.MissionEntities[4].vCoords   = <<-164.5949, 6171.5693, 31.4190>>
							data.MissionEntity.MissionEntities[4].fHeading  = 309.9996
							data.MissionEntity.MissionEntities[4].vRotation = <<0.0000, 0.0000, -50.0004>>

							data.MissionEntity.MissionEntities[5].model     = ba_prop_battle_case_sm_03
							data.MissionEntity.MissionEntities[5].vCoords   = <<-175.5306, 6171.1875, 30.4677>>
							data.MissionEntity.MissionEntities[5].fHeading  = 228.1991
							data.MissionEntity.MissionEntities[5].vRotation = <<0.0000, -0.0000, -131.8009>>

							data.MissionEntity.MissionEntities[6].model     = ba_prop_battle_case_sm_03
							data.MissionEntity.MissionEntities[6].vCoords   = <<-163.0321, 6156.0234, 30.4677>>
							data.MissionEntity.MissionEntities[6].fHeading  = 139.5987
							data.MissionEntity.MissionEntities[6].vRotation = <<0.0000, -0.0000, 139.5987>>

							data.MissionEntity.MissionEntities[7].model     = ba_prop_battle_case_sm_03
							data.MissionEntity.MissionEntities[7].vCoords   = <<-126.8480, 6164.6680, 31.2860>>
							data.MissionEntity.MissionEntities[7].fHeading  = 18.2000
							data.MissionEntity.MissionEntities[7].vRotation = <<0.0000, 0.0000, 18.2000>>
						BREAK
						
						CASE BBT_BIKER_FACTORY		// ba_Prop_Battle_drug_package_02
							data.MissionEntity.MissionEntities[0].model     = ba_Prop_Battle_drug_package_02
							data.MissionEntity.MissionEntities[0].vCoords   = <<-127.2331, 6164.5664, 31.1672>>
							data.MissionEntity.MissionEntities[0].fHeading  = 2.6000
							data.MissionEntity.MissionEntities[0].vRotation = <<0.0000, 0.0000, 2.6000>>

							data.MissionEntity.MissionEntities[1].model     = ba_Prop_Battle_drug_package_02
							data.MissionEntity.MissionEntities[1].vCoords   = <<-116.7577, 6181.2354, 30.1571>>
							data.MissionEntity.MissionEntities[1].fHeading  = 342.7994
							data.MissionEntity.MissionEntities[1].vRotation = <<0.0000, 0.0000, -17.2006>>

							data.MissionEntity.MissionEntities[2].model     = ba_Prop_Battle_drug_package_02
							data.MissionEntity.MissionEntities[2].vCoords   = <<-148.3074, 6151.7578, 31.6592>>
							data.MissionEntity.MissionEntities[2].fHeading  = 72.5992
							data.MissionEntity.MissionEntities[2].vRotation = <<0.0000, 0.0000, 72.5992>>

							data.MissionEntity.MissionEntities[3].model     = ba_Prop_Battle_drug_package_02
							data.MissionEntity.MissionEntities[3].vCoords   = <<-157.8316, 6140.5098, 32.7919>>
							data.MissionEntity.MissionEntities[3].fHeading  = 54.1991
							data.MissionEntity.MissionEntities[3].vRotation = <<0.0000, 0.0000, 54.1991>>

							data.MissionEntity.MissionEntities[4].model     = ba_Prop_Battle_drug_package_02
							data.MissionEntity.MissionEntities[4].vCoords   = <<-165.7211, 6141.7534, 30.7394>>
							data.MissionEntity.MissionEntities[4].fHeading  = 49.9989
							data.MissionEntity.MissionEntities[4].vRotation = <<0.0000, 0.0000, 49.9989>>

							data.MissionEntity.MissionEntities[5].model     = ba_Prop_Battle_drug_package_02
							data.MissionEntity.MissionEntities[5].vCoords   = <<-164.6559, 6173.2056, 31.2933>>
							data.MissionEntity.MissionEntities[5].fHeading  = 318.9988
							data.MissionEntity.MissionEntities[5].vRotation = <<0.0000, 0.0000, -41.0012>>

							data.MissionEntity.MissionEntities[6].model     = ba_Prop_Battle_drug_package_02
							data.MissionEntity.MissionEntities[6].vCoords   = <<-175.5318, 6168.9863, 31.3433>>
							data.MissionEntity.MissionEntities[6].fHeading  = 309.1986
							data.MissionEntity.MissionEntities[6].vRotation = <<0.0000, 0.0000, -50.8014>>

							data.MissionEntity.MissionEntities[7].model     = ba_Prop_Battle_drug_package_02
							data.MissionEntity.MissionEntities[7].vCoords   = <<-161.5983, 6154.4106, 30.3428>>
							data.MissionEntity.MissionEntities[7].fHeading  = 139.1982
							data.MissionEntity.MissionEntities[7].vRotation = <<0.0000, -0.0000, 139.1982>>
						BREAK
						
						CASE BBT_BUNKER		// ba_Prop_Battle_rsply_crate_gr_02a
							data.MissionEntity.MissionEntities[0].model     = ba_Prop_Battle_rsply_crate_gr_02a
							data.MissionEntity.MissionEntities[0].vCoords   = <<-128.0168, 6162.0405, 30.7882>>
							data.MissionEntity.MissionEntities[0].fHeading  = 61.9999
							data.MissionEntity.MissionEntities[0].vRotation = <<0.0000, 0.0000, 61.9999>>

							data.MissionEntity.MissionEntities[1].model     = ba_Prop_Battle_rsply_crate_gr_02a
							data.MissionEntity.MissionEntities[1].vCoords   = <<-118.2651, 6178.8965, 30.2087>>
							data.MissionEntity.MissionEntities[1].fHeading  = 45.3994
							data.MissionEntity.MissionEntities[1].vRotation = <<0.0000, 0.0000, 45.3994>>

							data.MissionEntity.MissionEntities[2].model     = ba_Prop_Battle_rsply_crate_gr_02a
							data.MissionEntity.MissionEntities[2].vCoords   = <<-146.4017, 6153.7988, 32.2676>>
							data.MissionEntity.MissionEntities[2].fHeading  = 72.5992
							data.MissionEntity.MissionEntities[2].vRotation = <<-0.0000, -0.0000, -32.4008>>

							data.MissionEntity.MissionEntities[3].model     = ba_Prop_Battle_rsply_crate_gr_02a
							data.MissionEntity.MissionEntities[3].vCoords   = <<-155.9928, 6139.6704, 31.5252>>
							data.MissionEntity.MissionEntities[3].fHeading  = 313.9990
							data.MissionEntity.MissionEntities[3].vRotation = <<0.0000, 0.0000, -46.0010>>

							data.MissionEntity.MissionEntities[4].model     = ba_Prop_Battle_rsply_crate_gr_02a
							data.MissionEntity.MissionEntities[4].vCoords   = <<-164.7737, 6141.0078, 30.3965>>
							data.MissionEntity.MissionEntities[4].fHeading  = 48.7989
							data.MissionEntity.MissionEntities[4].vRotation = <<0.0000, 0.0000, 48.7989>>

							data.MissionEntity.MissionEntities[5].model     = ba_Prop_Battle_rsply_crate_gr_02a
							data.MissionEntity.MissionEntities[5].vCoords   = <<-165.7179, 6172.8638, 30.3965>>
							data.MissionEntity.MissionEntities[5].fHeading  = -41.0012
							data.MissionEntity.MissionEntities[5].vRotation = <<0.0000, 0.0000, -41.0012>>

							data.MissionEntity.MissionEntities[6].model     = ba_Prop_Battle_rsply_crate_gr_02a
							data.MissionEntity.MissionEntities[6].vCoords   = <<-174.4594, 6171.8770, 30.3711>>
							data.MissionEntity.MissionEntities[6].fHeading  = 0.0000
							data.MissionEntity.MissionEntities[6].vRotation = <<0.0000, 0.0000, 18.9985>>

							data.MissionEntity.MissionEntities[7].model     = ba_Prop_Battle_rsply_crate_gr_02a
							data.MissionEntity.MissionEntities[7].vCoords   = <<-163.9509, 6156.8857, 30.3965>>
							data.MissionEntity.MissionEntities[7].fHeading  = 134.1982
							data.MissionEntity.MissionEntities[7].vRotation = <<0.0000, -0.0000, 134.1982>>
						BREAK
						
						CASE BBT_EVENT		// INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Crate_01a"))
							data.MissionEntity.MissionEntities[0].model     = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Crate_01a"))
							data.MissionEntity.MissionEntities[0].vCoords   = <<-126.0021, 6165.4883, 31.0008>>
							data.MissionEntity.MissionEntities[0].fHeading  = 43.9995
							data.MissionEntity.MissionEntities[0].vRotation = <<0.0000, 0.0000, 43.9995>>

							data.MissionEntity.MissionEntities[1].model     = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Crate_01a"))
							data.MissionEntity.MissionEntities[1].vCoords   = <<-115.5587, 6181.5107, 30.0217>>
							data.MissionEntity.MissionEntities[1].fHeading  = 38.1993
							data.MissionEntity.MissionEntities[1].vRotation = <<0.0000, 0.0000, 38.1993>>

							data.MissionEntity.MissionEntities[2].model     = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Crate_01a"))
							data.MissionEntity.MissionEntities[2].vCoords   = <<-148.7491, 6151.0981, 31.5215>>
							data.MissionEntity.MissionEntities[2].fHeading  = -42.0000
							data.MissionEntity.MissionEntities[2].vRotation = <<0.0000, 0.0000, -32.4008>>

							data.MissionEntity.MissionEntities[3].model     = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Crate_01a"))
							data.MissionEntity.MissionEntities[3].vCoords   = <<-157.5442, 6141.4766, 31.3252>>
							data.MissionEntity.MissionEntities[3].fHeading  = -46.0010
							data.MissionEntity.MissionEntities[3].vRotation = <<0.0000, 0.0000, -46.0010>>

							data.MissionEntity.MissionEntities[4].model     = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Crate_01a"))
							data.MissionEntity.MissionEntities[4].vCoords   = <<-164.3543, 6140.2285, 31.1703>>
							data.MissionEntity.MissionEntities[4].fHeading  = 340.1989
							data.MissionEntity.MissionEntities[4].vRotation = <<0.0000, 0.0000, -19.8011>>

							data.MissionEntity.MissionEntities[5].model     = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Crate_01a"))
							data.MissionEntity.MissionEntities[5].vCoords   = <<-165.3266, 6173.8442, 31.1869>>
							data.MissionEntity.MissionEntities[5].fHeading  = 317.5988
							data.MissionEntity.MissionEntities[5].vRotation = <<0.0000, 0.0000, -42.4012>>

							data.MissionEntity.MissionEntities[6].model     = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Crate_01a"))
							data.MissionEntity.MissionEntities[6].vCoords   = <<-175.5033, 6170.0171, 30.2065>>
							data.MissionEntity.MissionEntities[6].fHeading  = 128.1983
							data.MissionEntity.MissionEntities[6].vRotation = <<0.0000, -0.0000, 128.1983>>

							data.MissionEntity.MissionEntities[7].model     = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Crate_01a"))
							data.MissionEntity.MissionEntities[7].vCoords   = <<-161.0901, 6153.8691, 30.2065>>
							data.MissionEntity.MissionEntities[7].fHeading  = 134.1982
							data.MissionEntity.MissionEntities[7].vRotation = <<0.0000, -0.0000, 134.1981>>
						BREAK
						
						DEFAULT				// ba_Prop_Battle_Bag_01a
							data.MissionEntity.MissionEntities[0].model     = ba_Prop_Battle_Bag_01a
							data.MissionEntity.MissionEntities[0].vCoords   = <<-128.7507, 6162.7954, 30.0315>>
							data.MissionEntity.MissionEntities[0].fHeading  = 323.3994
							data.MissionEntity.MissionEntities[0].vRotation = <<0.0000, 0.0000, -36.6006>>

							data.MissionEntity.MissionEntities[1].model     = ba_Prop_Battle_Bag_01a
							data.MissionEntity.MissionEntities[1].vCoords   = <<-118.4701, 6179.5518, 30.9973>>
							data.MissionEntity.MissionEntities[1].fHeading  = 41.7993
							data.MissionEntity.MissionEntities[1].vRotation = <<0.0000, 0.0000, 41.7993>>

							data.MissionEntity.MissionEntities[2].model     = ba_Prop_Battle_Bag_01a
							data.MissionEntity.MissionEntities[2].vCoords   = <<-147.5876, 6152.3447, 31.5386>>
							data.MissionEntity.MissionEntities[2].fHeading  = -32.4008
							data.MissionEntity.MissionEntities[2].vRotation = <<0.0000, 0.0000, -32.4008>>

							data.MissionEntity.MissionEntities[3].model     = ba_Prop_Battle_Bag_01a
							data.MissionEntity.MissionEntities[3].vCoords   = <<-158.2183, 6140.8662, 32.1124>>
							data.MissionEntity.MissionEntities[3].fHeading  = 40.9990
							data.MissionEntity.MissionEntities[3].vRotation = <<0.0000, 0.0000, 40.9989>>

							data.MissionEntity.MissionEntities[4].model     = ba_Prop_Battle_Bag_01a
							data.MissionEntity.MissionEntities[4].vCoords   = <<-166.8948, 6142.9326, 30.4066>>
							data.MissionEntity.MissionEntities[4].fHeading  = 30.5988
							data.MissionEntity.MissionEntities[4].vRotation = <<0.0000, 0.0000, 30.5988>>

							data.MissionEntity.MissionEntities[5].model     = ba_Prop_Battle_Bag_01a
							data.MissionEntity.MissionEntities[5].vCoords   = <<-165.2589, 6170.9629, 30.2130>>
							data.MissionEntity.MissionEntities[5].fHeading  = 309.7986
							data.MissionEntity.MissionEntities[5].vRotation = <<0.0000, 0.0000, -50.2014>>

							data.MissionEntity.MissionEntities[6].model     = ba_Prop_Battle_Bag_01a
							data.MissionEntity.MissionEntities[6].vCoords   = <<-174.8595, 6169.1758, 30.2130>>
							data.MissionEntity.MissionEntities[6].fHeading  = 128.1983
							data.MissionEntity.MissionEntities[6].vRotation = <<0.0000, -0.0000, 128.1983>>

							data.MissionEntity.MissionEntities[7].model     = ba_Prop_Battle_Bag_01a
							data.MissionEntity.MissionEntities[7].vCoords   = <<-161.8117, 6155.3892, 30.2130>>
							data.MissionEntity.MissionEntities[7].fHeading  = 114.3980
							data.MissionEntity.MissionEntities[7].vRotation = <<0.0000, -0.0000, 114.3980>>
						BREAK
					ENDSWITCH
				BREAK
				CASE BBCS_FR_RECYCLE_PLANT
					SWITCH GET_BUSINESS_TYPE()
						CASE BBT_WAREHOUSE
						CASE BBT_HANGAR			// ba_prop_battle_case_sm_03
							EXIT
						BREAK
						
						CASE BBT_BIKER_FACTORY		// ba_Prop_Battle_drug_package_02
							data.MissionEntity.MissionEntities[0].model     = ba_Prop_Battle_drug_package_02
							data.MissionEntity.MissionEntities[0].vCoords   = <<-570.7850, -1626.2330, 33.0500>>
							data.MissionEntity.MissionEntities[0].fHeading  = 0.0000
							data.MissionEntity.MissionEntities[0].vRotation = <<0.0000, 0.0000, 0.0000>>

							data.MissionEntity.MissionEntities[1].model     = ba_Prop_Battle_drug_package_02
							data.MissionEntity.MissionEntities[1].vCoords   = <<-581.4700, -1625.9230, 33.0580>>
							data.MissionEntity.MissionEntities[1].fHeading  = 0.0000
							data.MissionEntity.MissionEntities[1].vRotation = <<0.0000, 0.0000, 0.0000>>

							data.MissionEntity.MissionEntities[2].model     = ba_Prop_Battle_drug_package_02
							data.MissionEntity.MissionEntities[2].vCoords   = <<-570.2510, -1606.9890, 26.9240>>
							data.MissionEntity.MissionEntities[2].fHeading  = 268.0000
							data.MissionEntity.MissionEntities[2].vRotation = <<0.0000, 0.0000, -92.0000>>

							data.MissionEntity.MissionEntities[3].model     = ba_Prop_Battle_drug_package_02
							data.MissionEntity.MissionEntities[3].vCoords   = <<-598.1550, -1610.9390, 27.2520>>
							data.MissionEntity.MissionEntities[3].fHeading  = 184.6000
							data.MissionEntity.MissionEntities[3].vRotation = <<0.0000, 0.0000, -175.4000>>

							data.MissionEntity.MissionEntities[4].model     = ba_Prop_Battle_drug_package_02
							data.MissionEntity.MissionEntities[4].vCoords   = <<-602.0170, -1603.6560, 29.5470>>
							data.MissionEntity.MissionEntities[4].fHeading  = 175.6000
							data.MissionEntity.MissionEntities[4].vRotation = <<0.0000, 0.0000, 175.6000>>

							data.MissionEntity.MissionEntities[5].model     = ba_Prop_Battle_drug_package_02
							data.MissionEntity.MissionEntities[5].vCoords   = <<-589.9830, -1601.7590, 27.2670>>
							data.MissionEntity.MissionEntities[5].fHeading  = 178.6000
							data.MissionEntity.MissionEntities[5].vRotation = <<0.0000, 0.0000, 178.6000>>

							data.MissionEntity.MissionEntities[6].model     = ba_Prop_Battle_drug_package_02
							data.MissionEntity.MissionEntities[6].vCoords   = <<-583.2290, -1608.5760, 27.2470>>
							data.MissionEntity.MissionEntities[6].fHeading  = 271.6000
							data.MissionEntity.MissionEntities[6].vRotation = <<0.0000, 0.0000, -88.4000>>

							data.MissionEntity.MissionEntities[7].model     = ba_Prop_Battle_drug_package_02
							data.MissionEntity.MissionEntities[7].vCoords   = <<-576.1840, -1601.6670, 27.2470>>
							data.MissionEntity.MissionEntities[7].fHeading  = -88.4000
							data.MissionEntity.MissionEntities[7].vRotation = <<0.0000, 0.0000, -88.4000>>
						BREAK
						
						CASE BBT_BUNKER		// ba_Prop_Battle_rsply_crate_gr_02a
							data.MissionEntity.MissionEntities[0].model     = ba_Prop_Battle_rsply_crate_gr_02a
							data.MissionEntity.MissionEntities[0].vCoords   = <<-568.9570, -1626.5580, 33.1060>>
							data.MissionEntity.MissionEntities[0].fHeading  = 354.7990
							data.MissionEntity.MissionEntities[0].vRotation = <<0.0000, 0.0000, -5.2010>>

							data.MissionEntity.MissionEntities[1].model     = ba_Prop_Battle_rsply_crate_gr_02a
							data.MissionEntity.MissionEntities[1].vCoords   = <<-581.4090, -1626.8160, 33.1050>>
							data.MissionEntity.MissionEntities[1].fHeading  = 6.7990
							data.MissionEntity.MissionEntities[1].vRotation = <<0.0000, 0.0000, 6.7990>>

							data.MissionEntity.MissionEntities[2].model     = ba_Prop_Battle_rsply_crate_gr_02a
							data.MissionEntity.MissionEntities[2].vCoords   = <<-570.5610, -1607.9380, 26.2110>>
							data.MissionEntity.MissionEntities[2].fHeading  = 6.7990
							data.MissionEntity.MissionEntities[2].vRotation = <<0.0000, 0.0000, 6.7990>>

							data.MissionEntity.MissionEntities[3].model     = ba_Prop_Battle_rsply_crate_gr_02a
							data.MissionEntity.MissionEntities[3].vCoords   = <<-597.0800, -1611.3831, 26.2190>>
							data.MissionEntity.MissionEntities[3].fHeading  = 277.9990
							data.MissionEntity.MissionEntities[3].vRotation = <<0.0000, 0.0000, -82.0010>>

							data.MissionEntity.MissionEntities[4].model     = ba_Prop_Battle_rsply_crate_gr_02a
							data.MissionEntity.MissionEntities[4].vCoords   = <<-600.9870, -1601.2460, 29.6000>>
							data.MissionEntity.MissionEntities[4].fHeading  = 264.1990
							data.MissionEntity.MissionEntities[4].vRotation = <<0.0000, 0.0000, -95.8010>>

							data.MissionEntity.MissionEntities[5].model     = ba_Prop_Battle_rsply_crate_gr_02a
							data.MissionEntity.MissionEntities[5].vCoords   = <<-591.3100, -1602.5050, 27.3010>>
							data.MissionEntity.MissionEntities[5].fHeading  = 171.1990
							data.MissionEntity.MissionEntities[5].vRotation = <<0.0000, 0.0000, 171.1990>>

							data.MissionEntity.MissionEntities[6].model     = ba_Prop_Battle_rsply_crate_gr_02a
							data.MissionEntity.MissionEntities[6].vCoords   = <<-584.0410, -1608.5820, 27.2810>>
							data.MissionEntity.MissionEntities[6].fHeading  = 124.9990
							data.MissionEntity.MissionEntities[6].vRotation = <<0.0000, 0.0000, 124.9990>>

							data.MissionEntity.MissionEntities[7].model     = ba_Prop_Battle_rsply_crate_gr_02a
							data.MissionEntity.MissionEntities[7].vCoords   = <<-577.1420, -1601.6620, 27.3010>>
							data.MissionEntity.MissionEntities[7].fHeading  = 96.5990
							data.MissionEntity.MissionEntities[7].vRotation = <<0.0000, 0.0000, 96.5990>>
						BREAK
						
						CASE BBT_EVENT		// INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Crate_01a"))
							data.MissionEntity.MissionEntities[0].model     = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Crate_01a"))
							data.MissionEntity.MissionEntities[0].vCoords   = <<-571.2781, -1625.1898, 32.9301>>
							data.MissionEntity.MissionEntities[0].fHeading  = 0.0000
							data.MissionEntity.MissionEntities[0].vRotation = <<0.0000, 0.0000, 0.0000>>

							data.MissionEntity.MissionEntities[1].model     = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Crate_01a"))
							data.MissionEntity.MissionEntities[1].vCoords   = <<-583.0562, -1627.2759, 32.0107>>
							data.MissionEntity.MissionEntities[1].fHeading  = 354.8000
							data.MissionEntity.MissionEntities[1].vRotation = <<0.0000, 0.0000, -5.2001>>

							data.MissionEntity.MissionEntities[2].model     = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Crate_01a"))
							data.MissionEntity.MissionEntities[2].vCoords   = <<-570.2272, -1605.6238, 27.1448>>
							data.MissionEntity.MissionEntities[2].fHeading  = -5.2001
							data.MissionEntity.MissionEntities[2].vRotation = <<0.0000, 0.0000, -5.2001>>

							data.MissionEntity.MissionEntities[3].model     = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Crate_01a"))
							data.MissionEntity.MissionEntities[3].vCoords   = <<-599.1612, -1611.5248, 26.0894>>
							data.MissionEntity.MissionEntities[3].fHeading  = 89.7997
							data.MissionEntity.MissionEntities[3].vRotation = <<0.0000, 0.0000, 89.7997>>

							data.MissionEntity.MissionEntities[4].model     = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Crate_01a"))
							data.MissionEntity.MissionEntities[4].vCoords   = <<-600.9266, -1600.2358, 29.4103>>
							data.MissionEntity.MissionEntities[4].fHeading  = 84.9996
							data.MissionEntity.MissionEntities[4].vRotation = <<0.0000, 0.0000, 84.9996>>

							data.MissionEntity.MissionEntities[5].model     = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Crate_01a"))
							data.MissionEntity.MissionEntities[5].vCoords   = <<-590.9517, -1603.7445, 27.1106>>
							data.MissionEntity.MissionEntities[5].fHeading  = 103.1995
							data.MissionEntity.MissionEntities[5].vRotation = <<0.0000, -0.0000, 103.1995>>

							data.MissionEntity.MissionEntities[6].model     = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Crate_01a"))
							data.MissionEntity.MissionEntities[6].vCoords   = <<-584.9916, -1609.7728, 26.0109>>
							data.MissionEntity.MissionEntities[6].fHeading  = 46.1993
							data.MissionEntity.MissionEntities[6].vRotation = <<0.0000, 0.0000, 46.1993>>

							data.MissionEntity.MissionEntities[7].model     = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Crate_01a"))
							data.MissionEntity.MissionEntities[7].vCoords   = <<-575.0418, -1600.4204, 27.1152>>
							data.MissionEntity.MissionEntities[7].fHeading  = 358.3993
							data.MissionEntity.MissionEntities[7].vRotation = <<0.0000, 0.0000, -1.6007>>
						BREAK
						
						DEFAULT				// ba_Prop_Battle_Bag_01a
							data.MissionEntity.MissionEntities[0].model     = ba_Prop_Battle_Bag_01a
							data.MissionEntity.MissionEntities[0].vCoords   = <<-570.0242, -1626.5637, 32.0171>>
							data.MissionEntity.MissionEntities[0].fHeading  = 281.9991
							data.MissionEntity.MissionEntities[0].vRotation = <<0.0000, 0.0000, -78.0009>>

							data.MissionEntity.MissionEntities[1].model     = ba_Prop_Battle_Bag_01a
							data.MissionEntity.MissionEntities[1].vCoords   = <<-580.4852, -1626.6725, 32.0821>>
							data.MissionEntity.MissionEntities[1].fHeading  = 262.1989
							data.MissionEntity.MissionEntities[1].vRotation = <<0.0000, -0.0000, -97.8011>>

							data.MissionEntity.MissionEntities[2].model     = ba_Prop_Battle_Bag_01a
							data.MissionEntity.MissionEntities[2].vCoords   = <<-570.3035, -1606.1379, 27.1535>>
							data.MissionEntity.MissionEntities[2].fHeading  = 178.9989
							data.MissionEntity.MissionEntities[2].vRotation = <<0.0000, -0.0000, 178.9989>>

							data.MissionEntity.MissionEntities[3].model     = ba_Prop_Battle_Bag_01a
							data.MissionEntity.MissionEntities[3].vCoords   = <<-596.1946, -1611.3446, 26.0350>>
							data.MissionEntity.MissionEntities[3].fHeading  = 88.5984
							data.MissionEntity.MissionEntities[3].vRotation = <<0.0000, 0.0000, 88.5984>>

							data.MissionEntity.MissionEntities[4].model     = ba_Prop_Battle_Bag_01a
							data.MissionEntity.MissionEntities[4].vCoords   = <<-601.4139, -1602.6523, 29.4168>>
							data.MissionEntity.MissionEntities[4].fHeading  = 82.5984
							data.MissionEntity.MissionEntities[4].vRotation = <<0.0000, 0.0000, 82.5984>>

							data.MissionEntity.MissionEntities[5].model     = ba_Prop_Battle_Bag_01a
							data.MissionEntity.MissionEntities[5].vCoords   = <<-589.2424, -1601.7434, 27.1171>>
							data.MissionEntity.MissionEntities[5].fHeading  = 88.1983
							data.MissionEntity.MissionEntities[5].vRotation = <<0.0000, 0.0000, 88.1983>>

							data.MissionEntity.MissionEntities[6].model     = ba_Prop_Battle_Bag_01a
							data.MissionEntity.MissionEntities[6].vCoords   = <<-584.8497, -1608.4520, 26.0174>>
							data.MissionEntity.MissionEntities[6].fHeading  = 80.9983
							data.MissionEntity.MissionEntities[6].vRotation = <<0.0000, 0.0000, 80.9982>>

							data.MissionEntity.MissionEntities[7].model     = ba_Prop_Battle_Bag_01a
							data.MissionEntity.MissionEntities[7].vCoords   = <<-577.1548, -1603.3020, 27.1171>>
							data.MissionEntity.MissionEntities[7].fHeading  = 71.9982
							data.MissionEntity.MissionEntities[7].vRotation = <<0.0000, 0.0000, 71.9982>>
						BREAK
					ENDSWITCH
				BREAK
				CASE BBCS_FR_FOUNDRY
					SWITCH GET_BUSINESS_TYPE()
						CASE BBT_WAREHOUSE
						CASE BBT_HANGAR		// ba_prop_battle_case_sm_03 
							data.MissionEntity.MissionEntities[0].model     = ba_prop_battle_case_sm_03
							data.MissionEntity.MissionEntities[0].vCoords   = <<1080.4531, -2001.4028, 36.4831>>
							data.MissionEntity.MissionEntities[0].fHeading  = 55.1999
							data.MissionEntity.MissionEntities[0].vRotation = <<0.0000, 0.0000, 55.1999>>

							data.MissionEntity.MissionEntities[1].model     = ba_prop_battle_case_sm_03
							data.MissionEntity.MissionEntities[1].vCoords   = <<1106.4540, -2015.2911, 34.7240>>
							data.MissionEntity.MissionEntities[1].fHeading  = 98.9996
							data.MissionEntity.MissionEntities[1].vRotation = <<0.0000, -0.0000, 98.9996>>

							data.MissionEntity.MissionEntities[2].model     = ba_prop_battle_case_sm_03
							data.MissionEntity.MissionEntities[2].vCoords   = <<1116.7869, -1998.9049, 35.7236>>
							data.MissionEntity.MissionEntities[2].fHeading  = 83.3991
							data.MissionEntity.MissionEntities[2].vRotation = <<0.0000, 0.0000, 83.3991>>

							data.MissionEntity.MissionEntities[3].model     = ba_prop_battle_case_sm_03
							data.MissionEntity.MissionEntities[3].vCoords   = <<1084.9048, -2016.2113, 41.1899>>
							data.MissionEntity.MissionEntities[3].fHeading  = 280.3985
							data.MissionEntity.MissionEntities[3].vRotation = <<0.0000, 0.0000, -79.6015>>

							data.MissionEntity.MissionEntities[4].model     = ba_prop_battle_case_sm_03
							data.MissionEntity.MissionEntities[4].vCoords   = <<1078.5925, -1981.2689, 33.9880>>
							data.MissionEntity.MissionEntities[4].fHeading  = 199.3986
							data.MissionEntity.MissionEntities[4].vRotation = <<0.0000, -0.0000, -160.6014>>

							data.MissionEntity.MissionEntities[5].model     = ba_prop_battle_case_sm_03
							data.MissionEntity.MissionEntities[5].vCoords   = <<1076.8087, -1976.9342, 30.7338>>
							data.MissionEntity.MissionEntities[5].fHeading  = 145.5985
							data.MissionEntity.MissionEntities[5].vRotation = <<0.0000, -0.0000, 145.5985>>

							data.MissionEntity.MissionEntities[6].model     = ba_prop_battle_case_sm_03
							data.MissionEntity.MissionEntities[6].vCoords   = <<1122.6431, -2015.9579, 34.7725>>
							data.MissionEntity.MissionEntities[6].fHeading  = 53.1981
							data.MissionEntity.MissionEntities[6].vRotation = <<0.0000, 0.0000, 53.1980>>

							data.MissionEntity.MissionEntities[7].model     = ba_prop_battle_case_sm_03
							data.MissionEntity.MissionEntities[7].vCoords   = <<1095.9850, -2020.3799, 43.1052>>
							data.MissionEntity.MissionEntities[7].fHeading  = 46.9980
							data.MissionEntity.MissionEntities[7].vRotation = <<0.0000, 0.0000, 46.9980>>
						BREAK
						
						CASE BBT_BIKER_FACTORY		// ba_Prop_Battle_drug_package_02
							data.MissionEntity.MissionEntities[0].model     = ba_Prop_Battle_drug_package_02
							data.MissionEntity.MissionEntities[0].vCoords   = <<1080.9672, -2001.7554, 36.3581>>
							data.MissionEntity.MissionEntities[0].fHeading  = 55.1999
							data.MissionEntity.MissionEntities[0].vRotation = <<0.0000, 0.0000, 55.1999>>

							data.MissionEntity.MissionEntities[1].model     = ba_Prop_Battle_drug_package_02
							data.MissionEntity.MissionEntities[1].vCoords   = <<1108.5048, -2013.8329, 34.6110>>
							data.MissionEntity.MissionEntities[1].fHeading  = 34.5994
							data.MissionEntity.MissionEntities[1].vRotation = <<0.0000, 0.0000, 34.5994>>

							data.MissionEntity.MissionEntities[2].model     = ba_Prop_Battle_drug_package_02
							data.MissionEntity.MissionEntities[2].vCoords   = <<1116.8651, -1998.1495, 35.5987>>
							data.MissionEntity.MissionEntities[2].fHeading  = 76.7990
							data.MissionEntity.MissionEntities[2].vRotation = <<0.0000, 0.0000, 76.7990>>

							data.MissionEntity.MissionEntities[3].model     = ba_Prop_Battle_drug_package_02
							data.MissionEntity.MissionEntities[3].vCoords   = <<1084.9971, -2017.0127, 41.0754>>
							data.MissionEntity.MissionEntities[3].fHeading  = -79.6015
							data.MissionEntity.MissionEntities[3].vRotation = <<0.0000, 0.0000, -79.6015>>

							data.MissionEntity.MissionEntities[4].model     = ba_Prop_Battle_drug_package_02
							data.MissionEntity.MissionEntities[4].vCoords   = <<1078.0946, -1980.5271, 33.7576>>
							data.MissionEntity.MissionEntities[4].fHeading  = 199.3990
							data.MissionEntity.MissionEntities[4].vRotation = <<0.0000, -0.0000, -160.6010>>

							data.MissionEntity.MissionEntities[5].model     = ba_Prop_Battle_drug_package_02
							data.MissionEntity.MissionEntities[5].vCoords   = <<1075.1115, -1976.4817, 30.6083>>
							data.MissionEntity.MissionEntities[5].fHeading  = 51.1983
							data.MissionEntity.MissionEntities[5].vRotation = <<0.0000, 0.0000, 51.1983>>

							data.MissionEntity.MissionEntities[6].model     = ba_Prop_Battle_drug_package_02
							data.MissionEntity.MissionEntities[6].vCoords   = <<1122.3707, -2016.4047, 34.7054>>
							data.MissionEntity.MissionEntities[6].fHeading  = 53.1980
							data.MissionEntity.MissionEntities[6].vRotation = <<0.0000, 0.0000, 53.1980>>

							data.MissionEntity.MissionEntities[7].model     = ba_Prop_Battle_drug_package_02
							data.MissionEntity.MissionEntities[7].vCoords   = <<1096.3207, -2019.8916, 42.9803>>
							data.MissionEntity.MissionEntities[7].fHeading  = 315.5979
							data.MissionEntity.MissionEntities[7].vRotation = <<0.0000, 0.0000, -44.4021>>
						BREAK 

						CASE BBT_BUNKER		// ba_Prop_Battle_rsply_crate_gr_02a
							data.MissionEntity.MissionEntities[0].model     = ba_Prop_Battle_rsply_crate_gr_02a
							data.MissionEntity.MissionEntities[0].vCoords   = <<1079.9398, -2001.9404, 36.4118>>
							data.MissionEntity.MissionEntities[0].fHeading  = 55.1999
							data.MissionEntity.MissionEntities[0].vRotation = <<0.0000, 0.0000, 55.1999>>

							data.MissionEntity.MissionEntities[1].model     = ba_Prop_Battle_rsply_crate_gr_02a
							data.MissionEntity.MissionEntities[1].vCoords   = <<1106.5165, -2015.7673, 34.6635>>
							data.MissionEntity.MissionEntities[1].fHeading  = 98.5992
							data.MissionEntity.MissionEntities[1].vRotation = <<0.0000, -0.0000, 98.5992>>

							data.MissionEntity.MissionEntities[2].model     = ba_Prop_Battle_rsply_crate_gr_02a
							data.MissionEntity.MissionEntities[2].vCoords   = <<1117.3533, -1997.5059, 35.6523>>
							data.MissionEntity.MissionEntities[2].fHeading  = 181.9986
							data.MissionEntity.MissionEntities[2].vRotation = <<0.0000, -0.0000, -178.0013>>

							data.MissionEntity.MissionEntities[3].model     = ba_Prop_Battle_rsply_crate_gr_02a
							data.MissionEntity.MissionEntities[3].vCoords   = <<1084.3356, -2016.7306, 40.6704>>
							data.MissionEntity.MissionEntities[3].fHeading  = -79.6015
							data.MissionEntity.MissionEntities[3].vRotation = <<0.0000, 0.0000, -79.6015>>

							data.MissionEntity.MissionEntities[4].model     = ba_Prop_Battle_rsply_crate_gr_02a
							data.MissionEntity.MissionEntities[4].vCoords   = <<1078.1664, -1980.5822, 33.8913>>
							data.MissionEntity.MissionEntities[4].fHeading  = -163.2014
							data.MissionEntity.MissionEntities[4].vRotation = <<0.0000, -0.0000, -161.8013>>

							data.MissionEntity.MissionEntities[5].model     = ba_Prop_Battle_rsply_crate_gr_02a
							data.MissionEntity.MissionEntities[5].vCoords   = <<1075.0798, -1977.2074, 30.6622>>
							data.MissionEntity.MissionEntities[5].fHeading  = 54.3983
							data.MissionEntity.MissionEntities[5].vRotation = <<0.0000, 0.0000, 54.3983>>

							data.MissionEntity.MissionEntities[6].model     = ba_Prop_Battle_rsply_crate_gr_02a
							data.MissionEntity.MissionEntities[6].vCoords   = <<1122.0410, -2016.8380, 34.6681>>
							data.MissionEntity.MissionEntities[6].fHeading  = 53.1980
							data.MissionEntity.MissionEntities[6].vRotation = <<0.0000, 0.0000, 53.1980>>

							data.MissionEntity.MissionEntities[7].model     = ba_Prop_Battle_rsply_crate_gr_02a
							data.MissionEntity.MissionEntities[7].vCoords   = <<1095.4561, -2019.8994, 43.0921>>
							data.MissionEntity.MissionEntities[7].fHeading  = 225.7977
							data.MissionEntity.MissionEntities[7].vRotation = <<0.0000, -0.0000, -134.2023>>
						BREAK
						
						CASE BBT_EVENT		// INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Crate_01a"))
							data.MissionEntity.MissionEntities[0].model     = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Crate_01a"))
							data.MissionEntity.MissionEntities[0].vCoords   = <<1080.6478, -2001.9099, 36.2218>>
							data.MissionEntity.MissionEntities[0].fHeading  = 55.1999
							data.MissionEntity.MissionEntities[0].vRotation = <<0.0000, 0.0000, 55.1999>>

							data.MissionEntity.MissionEntities[1].model     = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Crate_01a"))
							data.MissionEntity.MissionEntities[1].vCoords   = <<1106.7018, -2015.7269, 34.4685>>
							data.MissionEntity.MissionEntities[1].fHeading  = 98.5992
							data.MissionEntity.MissionEntities[1].vRotation = <<0.0000, -0.0000, 98.5992>>

							data.MissionEntity.MissionEntities[2].model     = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Crate_01a"))
							data.MissionEntity.MissionEntities[2].vCoords   = <<1117.6996, -1996.8038, 35.4623>>
							data.MissionEntity.MissionEntities[2].fHeading  = 228.1987
							data.MissionEntity.MissionEntities[2].vRotation = <<0.0000, -0.0000, -131.8013>>

							data.MissionEntity.MissionEntities[3].model     = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Crate_01a"))
							data.MissionEntity.MissionEntities[3].vCoords   = <<1085.1385, -2017.6294, 40.9192>>
							data.MissionEntity.MissionEntities[3].fHeading  = -79.6015
							data.MissionEntity.MissionEntities[3].vRotation = <<0.0000, 0.0000, -79.6015>>

							data.MissionEntity.MissionEntities[4].model     = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Crate_01a"))
							data.MissionEntity.MissionEntities[4].vCoords   = <<1078.6022, -1980.3478, 33.6213>>
							data.MissionEntity.MissionEntities[4].fHeading  = 1.2000
							data.MissionEntity.MissionEntities[4].vRotation = <<0.0000, -0.0000, -163.0013>>

							data.MissionEntity.MissionEntities[5].model     = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Crate_01a"))
							data.MissionEntity.MissionEntities[5].vCoords   = <<1075.5223, -1975.9081, 30.4718>>
							data.MissionEntity.MissionEntities[5].fHeading  = 40.7982
							data.MissionEntity.MissionEntities[5].vRotation = <<0.0000, 0.0000, 40.7982>>

							data.MissionEntity.MissionEntities[6].model     = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Crate_01a"))
							data.MissionEntity.MissionEntities[6].vCoords   = <<1121.5833, -2017.4188, 34.5413>>
							data.MissionEntity.MissionEntities[6].fHeading  = 53.1980
							data.MissionEntity.MissionEntities[6].vRotation = <<0.0000, 0.0000, 53.1980>>

							data.MissionEntity.MissionEntities[7].model     = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Crate_01a"))
							data.MissionEntity.MissionEntities[7].vCoords   = <<1094.8055, -2019.7653, 42.9024>>
							data.MissionEntity.MissionEntities[7].fHeading  = 316.3974
							data.MissionEntity.MissionEntities[7].vRotation = <<0.0000, 0.0000, -43.6026>>
						BREAK
						
						DEFAULT				// ba_Prop_Battle_Bag_01a
							data.MissionEntity.MissionEntities[0].model     = ba_Prop_Battle_Bag_01a
							data.MissionEntity.MissionEntities[0].vCoords   = <<1080.3816, -2002.7374, 36.2283>>
							data.MissionEntity.MissionEntities[0].fHeading  = 55.1999
							data.MissionEntity.MissionEntities[0].vRotation = <<0.0000, 0.0000, 55.1999>>

							data.MissionEntity.MissionEntities[1].model     = ba_Prop_Battle_Bag_01a
							data.MissionEntity.MissionEntities[1].vCoords   = <<1108.0754, -2013.4716, 34.4687>>
							data.MissionEntity.MissionEntities[1].fHeading  = 0.0000
							data.MissionEntity.MissionEntities[1].vRotation = <<0.0000, 0.0000, 37.1992>>

							data.MissionEntity.MissionEntities[2].model     = ba_Prop_Battle_Bag_01a
							data.MissionEntity.MissionEntities[2].vCoords   = <<1118.2908, -1995.8070, 35.3584>>
							data.MissionEntity.MissionEntities[2].fHeading  = 220.9987
							data.MissionEntity.MissionEntities[2].vRotation = <<0.0000, -0.0000, -139.0013>>

							data.MissionEntity.MissionEntities[3].model     = ba_Prop_Battle_Bag_01a
							data.MissionEntity.MissionEntities[3].vCoords   = <<1084.5409, -2017.4470, 40.4869>>
							data.MissionEntity.MissionEntities[3].fHeading  = -79.6015
							data.MissionEntity.MissionEntities[3].vRotation = <<0.0000, 0.0000, -79.6015>>

							data.MissionEntity.MissionEntities[4].model     = ba_Prop_Battle_Bag_01a
							data.MissionEntity.MissionEntities[4].vCoords   = <<1078.0165, -1979.8760, 33.6277>>
							data.MissionEntity.MissionEntities[4].fHeading  = 159.1989
							data.MissionEntity.MissionEntities[4].vRotation = <<0.0000, -0.0000, 159.1989>>

							data.MissionEntity.MissionEntities[5].model     = ba_Prop_Battle_Bag_01a
							data.MissionEntity.MissionEntities[5].vCoords   = <<1077.0797, -1977.8601, 30.4785>>
							data.MissionEntity.MissionEntities[5].fHeading  = 333.7981
							data.MissionEntity.MissionEntities[5].vRotation = <<0.0000, 0.0000, -26.2019>>

							data.MissionEntity.MissionEntities[6].model     = ba_Prop_Battle_Bag_01a
							data.MissionEntity.MissionEntities[6].vCoords   = <<1121.8185, -2016.6641, 34.5538>>
							data.MissionEntity.MissionEntities[6].fHeading  = 48.7980
							data.MissionEntity.MissionEntities[6].vRotation = <<0.0000, 0.0000, 48.7980>>

							data.MissionEntity.MissionEntities[7].model     = ba_Prop_Battle_Bag_01a
							data.MissionEntity.MissionEntities[7].vCoords   = <<1095.8021, -2019.3842, 42.9103>>
							data.MissionEntity.MissionEntities[7].fHeading  = -43.6026
							data.MissionEntity.MissionEntities[7].vRotation = <<0.0000, 0.0000, -43.6026>>
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

FUNC STRING GET_MISSION_ENTITY_NAME_FOR_BUSINESS_TYPE(BOOL bNeedsThe, BOOL bForBigMessage = FALSE, BOOL bPlural = FALSE)
	IF bNeedsThe
		SWITCH GET_BUSINESS_TYPE()
			CASE BBT_WAREHOUSE			RETURN "BBOTT_WCARGO"
			CASE BBT_HANGAR				RETURN "BBOTT_SPCCARGO"
			CASE BBT_BIKER_FACTORY		RETURN "BBOTT_BKRSPPLY"
			CASE BBT_BUNKER				RETURN "BBOTT_GRNSPPLY"	
			CASE BBT_EVENT				RETURN "BBOTT_EVNTCRGO"
			CASE BBT_UFO_PARTS			
				IF bPlural
					RETURN "CUFO_I_SPARTS_T"
				ELSE
					RETURN "CUFO_I_SPART_T"
				ENDIF
			BREAK
		ENDSWITCH
	
		RETURN "BBOTT_SPCCARGO"
	ENDIF
	
	IF bForBigMessage
		SWITCH GET_BUSINESS_TYPE()
			CASE BBT_WAREHOUSE				RETURN "BBOTS_WCARGO"
			CASE BBT_HANGAR					RETURN "BBOTS_SPCCARGO"
			CASE BBT_BIKER_FACTORY			RETURN "BBOTS_BKRSPPLY"
			CASE BBT_BUNKER					RETURN "BBOTS_GRNSPPLY"
			CASE BBT_EVENT					RETURN "BBOTS_EVNTCRGO"
			CASE BBT_UFO_PARTS				
				IF bPlural
					RETURN "CUFO_I_SPARTS"
				ELSE
					RETURN "CUFO_I_SPART"
				ENDIF
			BREAK
		ENDSWITCH
	ELSE
		SWITCH GET_BUSINESS_TYPE()
			CASE BBT_WAREHOUSE				RETURN "BBOT_WCARGO"
			CASE BBT_HANGAR					RETURN "BBOT_SPCCARGO"
			CASE BBT_BIKER_FACTORY			RETURN "BBOT_BKRSPPLY"
			CASE BBT_BUNKER					RETURN "BBOT_GRNSPPLY"
			CASE BBT_EVENT					RETURN "BBOT_EVNTCRGO"
			CASE BBT_UFO_PARTS				
				IF bPlural
					RETURN "CUFO_I_SPARTS"
				ELSE
					RETURN "CUFO_I_SPART"
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	RETURN "BBOT_SPCCARGO"
ENDFUNC

FUNC STRING GET_PACKAGE_HUD_TITLE()
	SWITCH GET_BUSINESS_TYPE()
		CASE BBT_WAREHOUSE				RETURN "FMC_HUD_CARGO"
		CASE BBT_HANGAR					RETURN "FMC_HUD_SCARGO"
		CASE BBT_BIKER_FACTORY			RETURN "FMC_HUD_BIKER"
		CASE BBT_BUNKER					RETURN "FMC_HUD_GUNRUN"
		CASE BBT_EVENT					RETURN "FMC_HUD_EVENT"
		CASE BBT_UFO_PARTS				RETURN "FMC_HUD_UFO"
	ENDSWITCH
	RETURN "FMC_HUD_CARGO"
ENDFUNC

PROC SET_PACKAGE_HUD_PLACEMENT_DATA()
	data.BottomRightHUD[0].eType = eBOTTOMRIGHTHUD_PACKAGE_DISPLAY
	data.BottomRightHUD[0].sTitle = GET_PACKAGE_HUD_TITLE()
ENDPROC

FUNC BOOL BB_SHOULD_RUN_PACKAGE_HUD(INT iHUD)
	IF iHUD = 0
		SWITCH GET_CLIENT_MODE_STATE()
			CASE eMODESTATE_COLLECT_MISSION_ENTITY
			CASE eMODESTATE_DELIVER_MISSION_ENTITY
			CASE eMODESTATE_HELP_DELIVER_MISSION_ENTITY
			CASE eMODESTATE_RECOVER_MISSION_ENTITY
				RETURN TRUE
			BREAK
		ENDSWITCH
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_EVENT_ITEM_TUNABLE_WEIGHTING()
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_ForceBBEventItems")
		RETURN 1.0
	ENDIF
	#ENDIF

	RETURN g_sMPTunables.fBB_BUSINESS_BATTLE_EVENT_CARGO_WEIGHTING
ENDFUNC

FUNC BOOL SHOULD_BUSINESS_TYPE_BE_EVENT_ITEM()
	// Check if we have an event item to give
	IF NOT IS_BBT_EVENT_ALLOWED()
		RETURN FALSE
	ENDIF
	IF GET_EVENT_ITEM_TUNABLE_WEIGHTING() = 0.0
		RETURN FALSE
	ENDIF
	IF GET_RANDOM_FLOAT_IN_RANGE() <= GET_EVENT_ITEM_TUNABLE_WEIGHTING()
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VARIATION_SUITABLE_FOR_BUSINESS_TYPE(FMCBB_VARIATION eVariation, FMBB_TYPE& eBusinessType)
	UNUSED_PARAMETER(eVariation)
	UNUSED_PARAMETER(eBusinessType)
	RETURN TRUE
ENDFUNC

PROC GET_BUSINESS_TYPE_FOR_VARIATION(FMCBB_VARIATION eVariation, FMBB_TYPE& eBusinessType)
	IF eBusinessType != BBT_INVALID
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] GET_BUSINESS_TYPE_FOR_VARIATION - Forced Business Type: ", GET_FMBB_TYPE_NAME_FOR_DEBUG(eBusinessType))
		EXIT
	ENDIF
	
	IF eVariation = BBCV_CAPTURED_UFO
		eBusinessType = BBT_UFO_PARTS
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] GET_BUSINESS_TYPE_FOR_VARIATION - Variation is BBCV_CAPTURED_UFO, only BBT_UFO_PARTS is valid.")
		EXIT
	ENDIF
	
	IF eVariation = BBCV_AIRCRAFT_CARRIER
		eBusinessType = BBT_BUNKER
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] GET_BUSINESS_TYPE_FOR_VARIATION - Variation is BBCV_AIRCRAFT_CARRIER, only BBT_BUNKER is valid.")
		EXIT
	ENDIF
	
	IF SHOULD_BUSINESS_TYPE_BE_EVENT_ITEM()
		eBusinessType = BBT_EVENT
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] GET_BUSINESS_TYPE_FOR_VARIATION - SHOULD_BUSINESS_TYPE_BE_EVENT_ITEM - ", GET_FMBB_TYPE_NAME_FOR_DEBUG(eBusinessType))
		EXIT
	ENDIF
	
	FMBB_TYPE eSuitableTypes[BBT_NORMAL_MAX]
	INT iType, iSuitableTypes
	REPEAT ENUM_TO_INT(BBT_NORMAL_MAX) iType
		FMBB_TYPE eType = INT_TO_ENUM(FMBB_TYPE, iType)
		IF IS_VARIATION_SUITABLE_FOR_BUSINESS_TYPE(eVariation, eType)
			eSuitableTypes[iSuitableTypes] = eType
			iSuitableTypes++
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] GET_BUSINESS_TYPE_FOR_VARIATION - ", GET_FMBB_TYPE_NAME_FOR_DEBUG(eType), " is suitable.")
		ENDIF
	ENDREPEAT
	
	IF iSuitableTypes > 0
		eBusinessType = eSuitableTypes[GET_RANDOM_INT_IN_RANGE(0, iSuitableTypes)]
	ELSE
		eBusinessType = BBT_INVALID
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] GET_BUSINESS_TYPE_FOR_VARIATION - Unable to find suitable Business Type.")
	ENDIF
ENDPROC

FUNC BOOL SETUP_VARIATION(INT iVariation, INT iSubvariation)
	
	// TODO: Variation selection
	UNUSED_PARAMETER(iSubvariation)
			
	serverBD.sMissionData.eMissionVariation = INT_TO_ENUM(FMCBB_VARIATION, iVariation)
	serverBD.sMissionData.eMissionSubvariation = INT_TO_ENUM(FMCBB_SUBVARIATION, iSubvariation)
	GET_BUSINESS_TYPE_FOR_VARIATION(GET_VARIATION(), serverBD.sMissionData.eBusinessType)
	serverBD.sMissionData.iChosenEventItem = SELECT_RANDOM_BBT_EVENT_ITEM()

	#IF IS_DEBUG_BUILD
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] SETUP_VARIATION - Variation: ", GET_VARIATION_NAME_FOR_DEBUG())
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] SETUP_VARIATION - Subvariation: ", GET_SUBVARIATION_NAME_FOR_DEBUG())	
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] SETUP_VARIATION - Business Type: ", GET_FMBB_TYPE_NAME_FOR_DEBUG(GET_BUSINESS_TYPE()))
	#ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL SHOULD_DRAW_MISSION_ENTITY_MARKER(INT iMissionEntity)
	UNUSED_PARAMETER(iMissionEntity)
	RETURN TRUE
ENDFUNC

FUNC BOOL DOES_PLAYER_HAVE_ALL_EVENT_CARGO()
	IF NOT DOES_LOCAL_PLAYER_OWN_BBT_EVENT_ITEM(serverBD.sMissionData.iChosenEventItem)
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_PLAYERS_BUSINESS_HUB_AT_CAPACITY_FOR_CURRENT_CARGO(PLAYER_INDEX player)
	SWITCH GET_BUSINESS_TYPE()
		CASE BBT_BIKER_FACTORY			RETURN IS_PLAYER_HUB_FULL_OF_ALL_BIKER_GOODS_TYPES(player)
		CASE BBT_EVENT					RETURN DOES_PLAYER_HAVE_ALL_EVENT_CARGO()
		CASE BBT_UFO_PARTS				RETURN FALSE
	ENDSWITCH
	
	IF eDeliveredType = BUSINESS_TYPE_INVALID
		RETURN FALSE
	ENDIF
	
	RETURN IS_PLAYER_HUB_FULL_OF_GOODS_TYPE(player, eDeliveredType) 
ENDFUNC

FUNC BUSINESS_TYPE GET_BUSINESS_TYPE_FOR_GOODS(INT iNumGoods)
	SWITCH GET_MISSION_ENTITY_DELIVERABLE_TYPE()
		CASE BBT_DELIVERABLE_CEO_PRODUCT		RETURN BUSINESS_TYPE_CARGO
		CASE BBT_DELIVERABLE_BIKER_PRODUCT		RETURN PICK_RANDOM_DRUG_TYPE_TO_AWARD_PLAYER(iNumGoods)
		CASE BBT_DELIVERABLE_BUNKER_PRODUCT		RETURN BUSINESS_TYPE_WEAPONS
		CASE BBT_DELIVERABLE_HANGAR_PRODUCT		RETURN BUSINESS_TYPE_CARGO
		CASE AW_DELIVERABLE_EVENT_ITEM			RETURN BUSINESS_TYPE_CARGO
	ENDSWITCH
	RETURN BUSINESS_TYPE_INVALID
ENDFUNC

FUNC BOOL SHOULD_SAY_WAS_RATHER_THAN_WERE()
	SWITCH GET_BUSINESS_TYPE()
		CASE BBT_WAREHOUSE				RETURN TRUE
		CASE BBT_HANGAR					RETURN TRUE
		CASE BBT_BIKER_FACTORY			RETURN TRUE
		CASE BBT_EXPORT_GARAGE			RETURN TRUE
		CASE BBT_BUNKER					RETURN FALSE
		CASE BBT_EVENT					RETURN TRUE
		CASE BBT_UFO_PARTS				RETURN FALSE
	ENDSWITCH
	RETURN TRUE
ENDFUNC

PROC CLEANUP_GOODS_TRANSFER_DATA()
	g_sFM_BUSINESS_BATTLE_Telemetry_data.m_entitiesstolentype = ENUM_TO_INT(eDeliveredType)
	
	playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iGoodsToTransfer = 0
	eDeliveredType = BUSINESS_TYPE_INVALID
ENDPROC

PROC PROCESS_EVENT_ITEM_TRANSFER()
	GIVE_LOCAL_PLAYER_BBT_EVENT_CASH_REWARD(playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iGoodsToTransfer)
	GIVE_LOCAL_PLAYER_BBT_EVENT_ITEM_REWARD(serverBD.sMissionData.iChosenEventItem)
	g_sFM_BUSINESS_BATTLE_Telemetry_data.m_eventitem = GET_HASH_FOR_BBT_EVENT_ITEM(serverBD.sMissionData.iChosenEventItem)
	
	CLEANUP_GOODS_TRANSFER_DATA()
ENDPROC

FUNC BOOL MAINTAIN_GOODS_TRANSFER()
	IF playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iGoodsToTransfer = 0
		RETURN TRUE
	ENDIF
	
	IF GET_BUSINESS_TYPE() = BBT_UFO_PARTS
		PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_GOODS_TRANSFER - Nothing to transfer for UFO parts.")
		CLEANUP_GOODS_TRANSFER_DATA()
		RETURN TRUE
	ENDIF
	
	IF GET_BUSINESS_TYPE() = BBT_EVENT
		PROCESS_EVENT_ITEM_TRANSFER()
		PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_GOODS_TRANSFER - Event item transferred.")
		RETURN TRUE
	ENDIF
	
	IF FMBB_DROPOFF_IS_BUS_BATTLE_SECONDARY(GET_DROPOFF())
	OR IS_PLAYERS_BUSINESS_HUB_AT_CAPACITY_FOR_CURRENT_CARGO(PLAYER_ID())
		CLEANUP_GOODS_TRANSFER_DATA()
		
		#IF IS_DEBUG_BUILD
		IF FMBB_DROPOFF_IS_BUS_BATTLE_SECONDARY(GET_DROPOFF())
			PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_GOODS_TRANSFER - Not transferring goods. Dropoff is secondary.")
		ELSE
			PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_GOODS_TRANSFER - Not transferring goods. Business hub at capacity.")
		ENDIF
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	INT iNumGoods = playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iGoodsToTransfer

	eDeliveredType = GET_BUSINESS_TYPE_FOR_GOODS(iNumGoods)

	iNumGoods = GET_BUSINESS_BATTLES_NUM_GOODS_FOR_TRANSFER(iNumGoods, eDeliveredType)
	
	IF ADD_PRODUCT_TO_BUSINESS_HUB(eDeliveredType, iNumGoods, sDeliveryLocal.eTransactionResult)
		IF sDeliveryLocal.eTransactionResult = CONTRABAND_TRANSACTION_STATE_SUCCESS
			PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_GOODS_TRANSFER - CONTRABAND_TRANSACTION_STATE_SUCCESS - ADD_PRODUCT_TO_BUSINESS_HUB - ", GET_BUSINESS_TYPE_STRING(eDeliveredType)," * ",iNumGoods)
		ELSE
			PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_GOODS_TRANSFER - ADD_PRODUCT_TO_BUSINESS_HUB - ", GET_BUSINESS_TYPE_STRING(eDeliveredType)," * ",iNumGoods)
		ENDIF
		
		PROCESS_REWARD_TICKER()
		CLEANUP_GOODS_TRANSFER_DATA()
	ENDIF

	RETURN FALSE
ENDFUNC

PROC MAINTAIN_ALLOW_QUICK_GPS()
	IF MPGlobalsAmbience.sMagnateGangBossData.bAllowFMBBMissionQuickGPS
	AND GET_CLIENT_MODE_STATE_ID() > 0
		MPGlobalsAmbience.sMagnateGangBossData.bAllowFMBBMissionQuickGPS = FALSE
	ENDIF
ENDPROC

PROC SET_MISSION_VEHICLE_DECORATOR(INT iVehicle, VEHICLE_INDEX vehId)

	IF DECOR_IS_REGISTERED_AS_TYPE("BBCarrier", DECOR_TYPE_BOOL)
		DECOR_SET_BOOL(vehID, "BBCarrier", TRUE)
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_VEHICLE_DECORATOR - BBCarrier")
	ENDIF
	
	IF DECOR_IS_REGISTERED_AS_TYPE("FMCVehicle", DECOR_TYPE_BOOL)
		DECOR_SET_BOOL(vehID, "FMCVehicle", TRUE)
		PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_VEHICLE_DECORATOR - FMCVehicle")
	ENDIF
	
	GB_SET_VEHICLE_AS_CONTRABAND(vehId, GET_MISSION_ENTITY_FROM_CARRIER(iVehicle, ET_VEHICLE), GBCT_FMBB)

ENDPROC

PROC DISPLAY_START_BIG_MESSAGE()
	IF GET_VARIATION() = BBCV_CAPTURED_UFO
		SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_GB_START_OF_JOB, "CUFO_I_UNKNOWN", "FMBB_STSTART_S", "FMBB_STSTART_T",DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,BIG_MESSAGE_BIT_DO_NOT_CLEANUP_GB_MESSAGES)
	ELSE
		SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_GB_START_OF_JOB, GET_MISSION_ENTITY_NAME_FOR_BUSINESS_TYPE(FALSE, TRUE), "FMBB_STSTART_S", "FMBB_STSTART_T",DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,BIG_MESSAGE_BIT_DO_NOT_CLEANUP_GB_MESSAGES)
	ENDIF
ENDPROC

PROC ACTIVATE_FREEMODE_DELIVERY_DROPOFF(BOOL bActive)
	IF bActive
		SWITCH GET_VARIATION()
			CASE BBCV_CAPTURED_UFO
				IF FREEMODE_DELIVERY_GET_ACTIVE_LOCAL_DROPOFF() = FREEMODE_DELIVERY_DROPOFF_INVALID
					FREEMODE_DELIVERY_ACTIVATE_DROPOFF_FOR_FM_EVENT(GET_DROPOFF(), serverBD.sMissionID)
				ENDIF
			BREAK
			DEFAULT 
				FREEMODE_DELIVERY_GET_UNRESERVED_DROPOFF_FOR_FMBB(serverBD.eSecondaryDropoff, GET_MISSION_ENTITY_DELIVERABLE_TYPE(), serverBD.sMissionID, TRUE)
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC UPDATE_ACTIVE_FREEMODE_DELIVERY_DROPOFF()
	SWITCH GET_VARIATION()
		CASE BBCV_CAPTURED_UFO
			EXIT
		BREAK
		DEFAULT 
			_FREEMODE_DELIVERY_UPDATE_ACTIVE_BUSINESS_BATTLES_DROPOFF(serverBD.eSecondaryDropoff, serverBD.sMissionID)
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL IS_MISSION_RESTRICTED_FOR_HIDE_OR_PASSIVE()
	IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(PLAYER_ID())
	OR FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID())
	OR SHOULD_HIDE_AMBIENT_EVENT(GET_iPI_TYPE_M3_CONST_FROM_FM_EVENT_TYPE(sMission.iType))
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_WARP_OUT_OF_MISSION_INTERIOR()
	RETURN IS_PED_IN_ANY_VEHICLE(LOCAL_PED_INDEX)
ENDFUNC

PROC DISPLAY_END_BIG_MESSAGE()
	IF NOT FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(PLAYER_ID())
	AND NOT FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID())
	AND NOT SHOULD_HIDE_AMBIENT_EVENT(GET_iPI_TYPE_M3_CONST_FROM_FM_EVENT_TYPE(sMission.iType))
		
		////// SPECIAL CONDITION SHARD FUNCTIONALITY ///////
		
		IF GET_END_REASON() = eENDREASON_NOT_ENOUGH_PLAYERS
			SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "FMBB_STOVER_T", "FMBB_FAIL_NOP")
			EXIT
		ENDIF
		
		////// NORMAL END SHARD FUNCTIONALITY ////////
		
		// Has local player delivered any mission entities
		IF HAS_PLAYER_GANG_DELIVERED_ANY_MISSION_ENTITIES(LOCAL_PLAYER_INDEX)		
			SETUP_NEW_BIG_MESSAGE_WITH_TWO_STRINGS_TWO_INTS(BIG_MESSAGE_GB_BUY_END_OF_JOB_SUCCESS, "FMBB_STPASS_GO", "FR_STPART1_S", GET_MISSION_ENTITY_NAME_FOR_BUSINESS_TYPE(FALSE, TRUE, TRUE), DEFAULT, DEFAULT, GET_NUMBER_OF_DELIVERED_MISSION_ENTITIES_BY_PLAYER_GANG(LOCAL_PLAYER_INDEX), data.MissionEntity.iCount)
		// No mission entities were delivered by any players
		ELIF GET_NUMBER_OF_DELIVERED_MISSION_ENTITIES() = 0
			IF SHOULD_SAY_WAS_RATHER_THAN_WERE()
				SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_GB_BUY_END_OF_JOB_SUCCESS, GET_MISSION_ENTITY_NAME_FOR_BUSINESS_TYPE(FALSE, TRUE, FALSE), "FMBB_FAIL_NDa", "FMBB_STOVER_T")
			ELSE
				SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_GB_BUY_END_OF_JOB_SUCCESS, GET_MISSION_ENTITY_NAME_FOR_BUSINESS_TYPE(FALSE, TRUE, TRUE), "FMBB_FAIL_NDb", "FMBB_STOVER_T") 
			ENDIF
		// Rival players delivered mission entities
		ELSE
			IF GET_NUMBER_OF_DELIVERED_MISSION_ENTITIES() = 1
				SETUP_NEW_BIG_MESSAGE_WITH_TWO_STRINGS_TWO_INTS(BIG_MESSAGE_GB_GENERIC, "FMBB_STOVER_T", "FMBB_STLOSE_Sa", GET_MISSION_ENTITY_NAME_FOR_BUSINESS_TYPE(TRUE, TRUE, FALSE), "FMBB_FAIL_RVa")
			ELSE
				SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_GB_GENERIC, GET_MISSION_ENTITY_NAME_FOR_BUSINESS_TYPE(TRUE, TRUE, TRUE), "FMBB_FAIL_RVS", "FMBB_STOVER_T")
			ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(PLAYER_ID())
			PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_END_BIG_MESSAGE - FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION = TRUE")
		ENDIF
		IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID())
			PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_END_BIG_MESSAGE - FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE = TRUE")
		ENDIF
		IF SHOULD_HIDE_AMBIENT_EVENT(GET_iPI_TYPE_M3_CONST_FROM_FM_EVENT_TYPE(sMission.iType))
			PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_END_BIG_MESSAGE - SHOULD_HIDE_AMBIENT_EVENT(GET_THIS_MISSION_FMMC_TYPE()) = TRUE")
		ENDIF
		#ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_ENABLE_GO_TO_POINT_BLIP_ROUTE(INT iGoto)
	UNUSED_PARAMETER(iGoto)
	RETURN FALSE
ENDFUNC

FUNC BLIP_SPRITE GET_GO_TO_POINT_BLIP_SPRITE(INT iGoto)
	UNUSED_PARAMETER(iGoto)
	RETURN RADAR_TRACE_BAT_CARGO
ENDFUNC

PROC BUSINESS_BATTLE_SET_WANTED_LEVEL_PLACEMENT_DATA()
	data.Wanted.iLevel = 2
ENDPROC

FUNC BOOL BUSINESS_BATTLE_PROGRESS_CLIENT_GO_TO_POINT()

	IF HAS_ANY_MISSION_ENTITY_BEEN_COLLECTED_FOR_THE_FIRST_TIME()
		RETURN TRUE
	ENDIF

	IF IS_GENERIC_CLIENT_BIT_SET(PARTICIPANT_ID(), eGENERICCLIENTBITSET_I_AM_AT_GOTO_LOCATION)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC BUSINESS_BATTLE_SERVER_PROCESSING()
	IF GET_END_REASON() = eENDREASON_NO_REASON_YET
	AND SHOULD_BAIL_FM_EVENT_DUE_TO_LOW_SESSION_NUMBERS(DEFAULT, DEFAULT, FALSE)
		SET_END_REASON(eENDREASON_NOT_ENOUGH_PLAYERS)
	ENDIF
ENDPROC

FUNC INT BUSINESS_BATTLE_GET_MINIMUM_PARTICIPATION_CASH()
	RETURN g_sMPTunables.iBB_BUSINESS_BATTLES_PARTICIPATION_CASH_PER_MIN
ENDFUNC

FUNC INT BUSINESS_BATTLE_GET_MINIMUM_PARTICIPATION_RP()
	RETURN g_sMPTunables.iBB_BUSINESS_BATTLES_PARTICIPATION_RP_PER_MIN
ENDFUNC

FUNC INT BUSINESS_BATTLE_GET_MINIMUM_PARTICIPATION_TIME_CAP()
	RETURN g_sMPTunables.iBB_BUSINESS_BATTLES_PARTICIPATION_TIME_CAP
ENDFUNC

FUNC BOOL SHOULD_GO_TO_POINT_PROGRESS_ON_COLLECTION()
	RETURN HAS_ANY_MISSION_ENTITY_BEEN_COLLECTED_FOR_THE_FIRST_TIME()
ENDFUNC

FUNC VECTOR BUSINESS_BATTLES_GET_PARTICIPATION_COORDS()

	IF HAS_ANY_MISSION_ENTITY_BEEN_COLLECTED_FOR_THE_FIRST_TIME()
		RETURN GET_LOCATION_OF_CLOSEST_MISSION_ENTITY()
	ENDIF

	RETURN data.GoToPoint.Locations[0].vCoords

ENDFUNC

FUNC BOOL SHOULD_QUIT_MISSION_SCRIPT()

	IF SHOULD_QUIT_MISSION_IN_LOCATION()
		PRINTLN("[",scriptName,"] - SHOULD_QUIT_MISSION_SCRIPT - SHOULD_QUIT_MISSION_IN_LOCATION")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC BUSINESS_BATTLE_CLEANUP()
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] BUSINESS_BATTLE_CLEANUP - Resetting MPGlobalsAmbience.sMagnateGangBossData.bAllowFMBBMissionQuickGPS to TRUE.")
	MPGlobalsAmbience.sMagnateGangBossData.bAllowFMBBMissionQuickGPS = TRUE
ENDPROC

FUNC STRING GET_MISSION_PLACEMENT_DATA_FILENAME()
	RETURN BUSINESS_BATTLE_GET_MISSION_PLACEMENT_DATA_FILENAME(GET_VARIATION(), GET_SUBVARIATION())
ENDFUNC
