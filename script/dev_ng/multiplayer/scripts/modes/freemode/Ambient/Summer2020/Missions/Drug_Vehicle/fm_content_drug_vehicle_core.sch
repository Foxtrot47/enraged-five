USING "globals.sch"
USING "rage_builtins.sch"

CONST_INT ENABLE_CONTENT	FEATURE_SUMMER_2020

#IF ENABLE_CONTENT
USING "fm_content_drug_vehicle_structs.sch"
USING "fm_content_random_events_structs.sch"
USING "fm_content_generic_structs.sch"

ENUM DRUG_VEHICLE_MODE_STATE
	eDV_MODESTATE_GO_TO_EVENT = 0,
	eDV_MODESTATE_COLLECT_VEHICLE,
	eDV_MODESTATE_DELIVER_VEHICLE,
	eDV_MODESTATE_END
ENDENUM

ENUM DRUG_VEHICLE_CLIENT_MODE_STATE
	eDV_CLIENTMODESTATE_GO_TO_EVENT = 0,
	eDV_CLIENTMODESTATE_COLLECT_VEHICLE,
	eDV_CLIENTMODESTATE_DELIVER_VEHICLE,
	eDV_CLIENTMODESTATE_HELP_DELIVER_VEHICLE,
	eDV_CLIENTMODESTATE_RECOVER_VEHICLE,
	eDV_CLIENTMODESTATE_END
ENDENUM

ENUM DRUG_VEHICLE_PED_TASKS
	eDV_PEDTASK_DO_SCENARIO = 0,
	eDV_PEDTASK_ATTACK
ENDENUM

ENUM DRUG_VEHICLE_DRUG_TYPE
	eDV_DRUGTYPE_METH = 0,
	eDV_DRUGTYPE_COKE = 1,
	eDV_DRUGTYPE_WEED = 2
ENDENUM

FUNC DRUG_VEHICLE_VARIATION GET_VARIATION()
	RETURN serverBD.sMissionData.eMissionVariation
ENDFUNC

FUNC DRUG_VEHICLE_SUBVARIATION GET_SUBVARIATION()
	RETURN serverBD.sMissionData.eMissionSubvariation
ENDFUNC

FUNC DRUG_VEHICLE_DRUG_TYPE GET_DRUG_VEHICLE_DRUG_TYPE()
	RETURN INT_TO_ENUM(DRUG_VEHICLE_DRUG_TYPE, serverBD.iRandomModeInt0)
ENDFUNC

FUNC STRING GET_VARIATION_NAME_FOR_DEBUG()
	RETURN DRUG_VEHICLE_GET_VARIATION_NAME_FOR_DEBUG(GET_VARIATION())
ENDFUNC

FUNC STRING GET_SUBVARIATION_NAME_FOR_DEBUG()
	RETURN DRUG_VEHICLE_GET_SUBVARIATION_NAME_FOR_DEBUG(GET_SUBVARIATION())
ENDFUNC

FUNC XPCATEGORY GET_DEFAULT_RP_CATEGORY()
	RETURN XPCATEGORY_FM_CONTENT_SUM20_DRUG_VEHICLE
ENDFUNC

PROC DISPLAY_END_BIG_MESSAGE()
	IF IS_GENERIC_BIT_SET(eGENERICBITSET_I_WON)
		SWITCH GET_LOCAL_PLAYER_MISSION_TEAM()
			CASE eMISSIONTEAMS_UNAFFILIATED
				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_WINNER, "RE_SHRD_TWIN", "RE_SHRD_SDVCRK", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME)
			BREAK
			
			#IF FEATURE_COPS_N_CROOKS
			CASE eMISSIONTEAMS_COP
				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_WINNER, "RE_SHRD_TWIN", "RE_SHRD_SDVCOP", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME)
			BREAK
			#ENDIF
			
		ENDSWITCH
	ENDIF
ENDPROC

FUNC eMISSION_TEAMS SETUP_PLAYER_MISSION_TEAM()
	
	#IF FEATURE_COPS_N_CROOKS
	IF IS_LOCAL_PLAYER_COP()
		RETURN eMISSIONTEAMS_COP
	ENDIF
	#ENDIF
	
	RETURN eMISSIONTEAMS_UNAFFILIATED
	
ENDFUNC

FUNC STRING GET_MISSION_TEAM_NAME(eMISSION_TEAMS eTeam)
	
	SWITCH eTeam
		CASE eMISSIONTEAMS_UNAFFILIATED	RETURN "eMISSIONTEAMS_UNAFFILIATED"
		
		#IF FEATURE_COPS_N_CROOKS
		CASE eMISSIONTEAMS_COP		RETURN "eMISSIONTEAMS_COP"
		#ENDIF
		
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC INT GET_DRUG_TYPE_FOR_LOCATION()
	SWITCH serverBD.sMissionData.eMissionSubvariation
		
		// Any
		CASE DVS_LOCATION_1
		CASE DVS_LOCATION_2
		CASE DVS_LOCATION_6
		CASE DVS_LOCATION_7
		CASE DVS_LOCATION_8
		CASE DVS_LOCATION_9
		CASE DVS_LOCATION_10
		CASE DVS_LOCATION_12
		CASE DVS_LOCATION_13
		CASE DVS_LOCATION_14
		CASE DVS_LOCATION_17
		CASE DVS_LOCATION_18
		CASE DVS_LOCATION_20
		CASE DVS_LOCATION_25
		CASE DVS_LOCATION_29
		CASE DVS_LOCATION_30
			RETURN GET_RANDOM_INT_IN_RANGE(0, 3)
		
		// Weed or coke
		CASE DVS_LOCATION_3
		CASE DVS_LOCATION_4
		CASE DVS_LOCATION_5
		CASE DVS_LOCATION_11
		CASE DVS_LOCATION_15
			RETURN PICK_INT(GET_RANDOM_BOOL(), ENUM_TO_INT(eDV_DRUGTYPE_WEED), ENUM_TO_INT(eDV_DRUGTYPE_COKE))
		
		// Meth
		CASE DVS_LOCATION_16
		CASE DVS_LOCATION_19
		CASE DVS_LOCATION_21
		CASE DVS_LOCATION_22
		CASE DVS_LOCATION_23
		CASE DVS_LOCATION_24
		CASE DVS_LOCATION_26
		CASE DVS_LOCATION_27
		CASE DVS_LOCATION_28
			RETURN ENUM_TO_INT(eDV_DRUGTYPE_METH)

	ENDSWITCH
	
	RETURN -1
ENDFUNC

FUNC BOOL SETUP_VARIATION(INT iVariation, INT iSubvariation)

	serverBD.sMissionData.eMissionVariation = INT_TO_ENUM(DRUG_VEHICLE_VARIATION, iVariation)
	serverBD.sMissionData.eMissionSubvariation = INT_TO_ENUM(DRUG_VEHICLE_SUBVARIATION, iSubvariation)

	// Drug type
	serverBD.iRandomModeInt0 = GET_DRUG_TYPE_FOR_LOCATION()

	// Model
	serverBD.iRandomModeInt1 = GET_RANDOM_INT_IN_RANGE(0, 5)

	PRINTLN("[",scriptName,"] - SETUP_VARIATION - Variation = ", serverBD.sMissionData.eMissionVariation)
	PRINTLN("[",scriptName,"] - SETUP_VARIATION - Subvariation = ", serverBD.sMissionData.eMissionSubvariation)	
	PRINTLN("[",scriptName,"] - SETUP_VARIATION - iRandomModeInt0 = ", serverBD.iRandomModeInt0)	
	PRINTLN("[",scriptName,"] - SETUP_VARIATION - iRandomModeInt1 = ", serverBD.iRandomModeInt1)	
	
	RETURN TRUE
	
ENDFUNC

FUNC STRING GET_MISSION_PLACEMENT_DATA_FILENAME()
	RETURN DRUG_VEHICLE_GET_MISSION_PLACEMENT_DATA_FILENAME(GET_VARIATION(), GET_SUBVARIATION())
ENDFUNC

FUNC MODEL_NAMES GET_DRUG_VEHICLE_MODEL()
	SWITCH serverBD.iRandomModeInt1
		CASE 0		RETURN FACTION
		CASE 1		RETURN PATRIOT
		CASE 2		RETURN SULTAN
		CASE 3		RETURN FUTO
		CASE 4		RETURN KANJO
	ENDSWITCH
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC MODEL_NAMES GET_DRUG_VEHICLE_CRATE_MODEL()
	SWITCH data.Vehicle.Vehicles[0].model
		CASE FACTION
			SWITCH GET_DRUG_VEHICLE_DRUG_TYPE()
				CASE eDV_DRUGTYPE_METH			RETURN bkr_prop_meth_pseudoephedrine
				CASE eDV_DRUGTYPE_COKE			RETURN ba_prop_battle_coke_doll_bigbox
				CASE eDV_DRUGTYPE_WEED			RETURN bkr_prop_weed_bigbag_03a
			ENDSWITCH
		BREAK
		CASE PATRIOT
			SWITCH GET_DRUG_VEHICLE_DRUG_TYPE()
				CASE eDV_DRUGTYPE_METH			RETURN bkr_prop_meth_pseudoephedrine
				CASE eDV_DRUGTYPE_COKE			RETURN ba_prop_battle_coke_doll_bigbox
				CASE eDV_DRUGTYPE_WEED			RETURN ba_prop_battle_weed_bigbag_01a
			ENDSWITCH
		BREAK
		CASE SULTAN
			SWITCH GET_DRUG_VEHICLE_DRUG_TYPE()
				CASE eDV_DRUGTYPE_METH			RETURN bkr_prop_meth_pseudoephedrine
				CASE eDV_DRUGTYPE_COKE			RETURN ba_prop_battle_coke_doll_bigbox
				CASE eDV_DRUGTYPE_WEED			RETURN bkr_prop_weed_bigbag_03a
			ENDSWITCH
		BREAK
		CASE FUTO
			SWITCH GET_DRUG_VEHICLE_DRUG_TYPE()
				CASE eDV_DRUGTYPE_METH			RETURN bkr_prop_meth_smallbag_01a
				CASE eDV_DRUGTYPE_COKE			RETURN prop_drug_package
				CASE eDV_DRUGTYPE_WEED			RETURN bkr_prop_weed_bigbag_03a
			ENDSWITCH
		BREAK
		CASE KANJO
			SWITCH GET_DRUG_VEHICLE_DRUG_TYPE()
				CASE eDV_DRUGTYPE_METH			RETURN bkr_prop_meth_smallbag_01a
				CASE eDV_DRUGTYPE_COKE			RETURN ba_prop_battle_coke_doll_bigbox
				CASE eDV_DRUGTYPE_WEED			RETURN ba_prop_battle_weed_bigbag_01a
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

PROC SET_MISSION_SCRIPT_PLACEMENT_DATA()

	data.GoToPoint.Locations[0].vCoords = data.Vehicle.Vehicles[0].vCoords
	data.GoToPoint.Locations[0].fRadius = 200.0
	data.GoToPoint.iCount = 1
	
	data.EventLocationBlips.iCount = 1
	data.EventLocationBlips.Blip[0].vCoord = data.GoToPoint.Locations[0].vCoords
	data.EventLocationBlips.Blip[0].fRadius = 50.0

	data.Ped.Group[0].iGoto = 0
	data.Ped.Group[0].Trigger[0].iArea = 0
	
	data.Vehicle.Vehicles[0].model = GET_DRUG_VEHICLE_MODEL()
	
	data.Prop.iCount = 1
	data.Prop.Props[0].model = GET_DRUG_VEHICLE_CRATE_MODEL()
	data.Prop.Props[0].vCoords = data.Vehicle.Vehicles[0].vCoords+<<0.0, 0.0, 2.0>>
	SET_PROP_DATA_BIT(0, ePROPDATABITSET_SPAWN_INVINCIBLE)
	
	data.ModeTimer.iTimeLimit = g_sMPTunables.iSUM_RANDOM_EVENT_DRUG_VEHICLE_DURATION
	data.ModeTimer.eDisplay = eMODETIMERDISPLAYTYPE_NEAR_END_IF_PARTICIPATING
		
	data.Reward.bShowCashWithShard = TRUE
	data.Reward.bShowRankbarOnRPEarn = TRUE
	
	data.TriggerArea.iCount = 2
	
	SWITCH GET_SUBVARIATION()
		CASE DVS_LOCATION_1
			data.TriggerArea.Areas[0].vMin = <<-89.947990,6390.054688,29.934315>>
			data.TriggerArea.Areas[0].vMax = <<-101.984184,6401.840820,35.454105>>
			data.TriggerArea.Areas[0].fWidth = 7.000000
			SET_TRIGGER_AREA_DATA_BIT(0, eTRIGGERAREADATABITSET_REACT_AREA)
			
			data.TriggerArea.Areas[1].vMin = <<-89.947990,6390.054688,29.934315>>
			data.TriggerArea.Areas[1].vMax = <<-101.984184,6401.840820,35.454105>>
			data.TriggerArea.Areas[1].fWidth = 7.000000
			SET_TRIGGER_AREA_DATA_BIT(1, eTRIGGERAREADATABITSET_DEFENSIVE_AREA)
		BREAK
		CASE DVS_LOCATION_2
			data.TriggerArea.Areas[0].vMin = <<1672.031616,4976.063965,40.313221>> 
			data.TriggerArea.Areas[0].vMax = <<1661.464844,4966.073242,46.482174>> 
			data.TriggerArea.Areas[0].fWidth = 7.000000
			SET_TRIGGER_AREA_DATA_BIT(0, eTRIGGERAREADATABITSET_REACT_AREA)
			
			data.TriggerArea.Areas[1].vMin = <<1672.031616,4976.063965,40.313221>> 
			data.TriggerArea.Areas[1].vMax = <<1661.464844,4966.073242,46.482174>> 
			data.TriggerArea.Areas[1].fWidth = 7.000000
			SET_TRIGGER_AREA_DATA_BIT(1, eTRIGGERAREADATABITSET_DEFENSIVE_AREA)
		BREAK
		CASE DVS_LOCATION_3
			data.TriggerArea.Areas[0].vMin = <<2642.567871,4250.915527,42.783058>> 
			data.TriggerArea.Areas[0].vMax = <<2654.980713,4258.502441,46.763134>> 
			data.TriggerArea.Areas[0].fWidth = 12.000000
			SET_TRIGGER_AREA_DATA_BIT(0, eTRIGGERAREADATABITSET_REACT_AREA)
			
			data.TriggerArea.Areas[1].vMin = <<2642.567871,4250.915527,42.783058>> 
			data.TriggerArea.Areas[1].vMax = <<2654.980713,4258.502441,46.763134>> 
			data.TriggerArea.Areas[1].fWidth = 12.000000
			SET_TRIGGER_AREA_DATA_BIT(1, eTRIGGERAREADATABITSET_DEFENSIVE_AREA)
		
			data.Population.Blockers[0].eType = ePOPULATIONBLOCKTYPE_SCENARIO
			data.Population.Blockers[0].vMin = GET_AREA_EXTENT(<<2635.7480, 4246.7178, 43.7970>>, 5.0)
			data.Population.Blockers[0].vMax = GET_AREA_EXTENT(<<2635.7480, 4246.7178, 43.7970>>, 5.0, TRUE)
			
			data.Population.iCount = 1
		BREAK
		CASE DVS_LOCATION_4
			data.TriggerArea.Areas[0].vMin = <<1442.678467,3664.649414,32.112328>> 
			data.TriggerArea.Areas[0].vMax = <<1426.805908,3658.941162,36.312809>> 
			data.TriggerArea.Areas[0].fWidth = 12.000000
			SET_TRIGGER_AREA_DATA_BIT(0, eTRIGGERAREADATABITSET_REACT_AREA)
			
			data.TriggerArea.Areas[1].vMin = <<1442.678467,3664.649414,32.112328>> 
			data.TriggerArea.Areas[1].vMax = <<1426.805908,3658.941162,36.312809>> 
			data.TriggerArea.Areas[1].fWidth = 12.000000
			SET_TRIGGER_AREA_DATA_BIT(1, eTRIGGERAREADATABITSET_DEFENSIVE_AREA)
		BREAK
		CASE DVS_LOCATION_5
			data.TriggerArea.Areas[0].vMin = <<854.921692,2854.431152,55.288994>> 
			data.TriggerArea.Areas[0].vMax = <<864.234436,2845.935791,59.534576>> 
			data.TriggerArea.Areas[0].fWidth = 15.000000
			SET_TRIGGER_AREA_DATA_BIT(0, eTRIGGERAREADATABITSET_REACT_AREA)
			
			data.TriggerArea.Areas[1].vMin = <<854.921692,2854.431152,55.288994>> 
			data.TriggerArea.Areas[1].vMax = <<864.234436,2845.935791,59.534576>> 
			data.TriggerArea.Areas[1].fWidth = 15.000000
			SET_TRIGGER_AREA_DATA_BIT(1, eTRIGGERAREADATABITSET_DEFENSIVE_AREA)
		
			data.Population.Blockers[0].eType = ePOPULATIONBLOCKTYPE_SCENARIO
			data.Population.Blockers[0].vMin = GET_AREA_EXTENT(<<853.6280, 2851.2390, 56.7360>>, 10.0)
			data.Population.Blockers[0].vMax = GET_AREA_EXTENT(<<853.6280, 2851.2390, 56.7360>>, 10.0, TRUE)
			
			data.Population.iCount = 1
		BREAK
		CASE DVS_LOCATION_6
			data.TriggerArea.Areas[0].vMin = <<-1895.456787,2053.533936,138.001419>> 
			data.TriggerArea.Areas[0].vMax = <<-1906.918091,2058.628662,142.738373>> 
			data.TriggerArea.Areas[0].fWidth = 16.000000
			SET_TRIGGER_AREA_DATA_BIT(0, eTRIGGERAREADATABITSET_REACT_AREA)
			
			data.TriggerArea.Areas[1].vMin = <<-1895.456787,2053.533936,138.001419>> 
			data.TriggerArea.Areas[1].vMax = <<-1906.918091,2058.628662,142.738373>> 
			data.TriggerArea.Areas[1].fWidth = 16.000000
			SET_TRIGGER_AREA_DATA_BIT(1, eTRIGGERAREADATABITSET_DEFENSIVE_AREA)
		
			data.Population.Blockers[0].eType = ePOPULATIONBLOCKTYPE_SCENARIO
			data.Population.Blockers[0].vMin = GET_AREA_EXTENT(<<-1902.0630, 2054.9500, 139.6950>>, 10.0)
			data.Population.Blockers[0].vMax = GET_AREA_EXTENT(<<-1902.0630, 2054.9500, 139.6950>>, 10.0, TRUE)
			
			data.Population.Blockers[1].eType = ePOPULATIONBLOCKTYPE_VEHICLE_GEN
			data.Population.Blockers[1].vMin = <<-1898.63733, 2057.72046, 138.88455>>
			data.Population.Blockers[1].vMax = <<-1906.70398, 2050.33765, 141.73845>>
			
			data.Population.iCount = 2
		BREAK
		CASE DVS_LOCATION_7
			data.TriggerArea.Areas[0].vMin = <<2316.227539,2539.870117,43.667675>> 
			data.TriggerArea.Areas[0].vMax = <<2300.958496,2543.842773,48.665813>> 
			data.TriggerArea.Areas[0].fWidth = 16.000000
			SET_TRIGGER_AREA_DATA_BIT(0, eTRIGGERAREADATABITSET_REACT_AREA)
			
			data.TriggerArea.Areas[1].vMin = <<2316.227539,2539.870117,43.667675>> 
			data.TriggerArea.Areas[1].vMax = <<2300.958496,2543.842773,48.665813>> 
			data.TriggerArea.Areas[1].fWidth = 16.000000
			SET_TRIGGER_AREA_DATA_BIT(1, eTRIGGERAREADATABITSET_DEFENSIVE_AREA)
			
			data.Population.Blockers[0].eType = ePOPULATIONBLOCKTYPE_VEHICLE_GEN
			data.Population.Blockers[0].vMin = <<2314.45654, 2543.48657, 44.66768>>
			data.Population.Blockers[0].vMax = <<2303.02319, 2534.61768, 48.70819>>
			
			data.Population.iCount = 1
		BREAK
		CASE DVS_LOCATION_8
			data.TriggerArea.Areas[0].vMin = <<-2229.237061,4223.523926,43.872520>> 
			data.TriggerArea.Areas[0].vMax = <<-2222.943604,4234.329102,48.988609>> 
			data.TriggerArea.Areas[0].fWidth = 16.000000
			SET_TRIGGER_AREA_DATA_BIT(0, eTRIGGERAREADATABITSET_REACT_AREA)
			
			data.TriggerArea.Areas[1].vMin = <<-2229.237061,4223.523926,43.872520>> 
			data.TriggerArea.Areas[1].vMax = <<-2222.943604,4234.329102,48.988609>> 
			data.TriggerArea.Areas[1].fWidth = 16.000000
			SET_TRIGGER_AREA_DATA_BIT(1, eTRIGGERAREADATABITSET_DEFENSIVE_AREA)
		BREAK
		CASE DVS_LOCATION_9
			data.TriggerArea.Areas[0].vMin = <<-2952.995605,413.334595,12.217690>> 
			data.TriggerArea.Areas[0].vMax = <<-2955.707031,396.099915,17.021677>> 
			data.TriggerArea.Areas[0].fWidth = 14.000000
			SET_TRIGGER_AREA_DATA_BIT(0, eTRIGGERAREADATABITSET_REACT_AREA)
			
			data.TriggerArea.Areas[1].vMin = <<-2952.995605,413.334595,12.217690>> 
			data.TriggerArea.Areas[1].vMax = <<-2955.707031,396.099915,17.021677>> 
			data.TriggerArea.Areas[1].fWidth = 14.000000
			SET_TRIGGER_AREA_DATA_BIT(1, eTRIGGERAREADATABITSET_DEFENSIVE_AREA)
		BREAK
		CASE DVS_LOCATION_10
			data.TriggerArea.Areas[0].vMin = <<1913.302612,575.914917,173.372711>> 
			data.TriggerArea.Areas[0].vMax = <<1908.430786,564.918762,177.768539>> 
			data.TriggerArea.Areas[0].fWidth = 14.000000
			SET_TRIGGER_AREA_DATA_BIT(0, eTRIGGERAREADATABITSET_REACT_AREA)
			
			data.TriggerArea.Areas[1].vMin = <<1913.302612,575.914917,173.372711>> 
			data.TriggerArea.Areas[1].vMax = <<1908.430786,564.918762,177.768539>> 
			data.TriggerArea.Areas[1].fWidth = 14.000000
			SET_TRIGGER_AREA_DATA_BIT(1, eTRIGGERAREADATABITSET_DEFENSIVE_AREA)
			
			data.Population.Blockers[0].eType = ePOPULATIONBLOCKTYPE_VEHICLE_GEN
			data.Population.Blockers[0].vMin = <<1903.08215, 579.72064, 173.84598>>
			data.Population.Blockers[0].vMax = <<1915.25330, 568.56482, 177.48785>>
			
			data.Population.iCount = 1
		BREAK
		CASE DVS_LOCATION_11
			data.TriggerArea.Areas[0].vMin = <<-286.342987,2532.834473,71.661880>> 
			data.TriggerArea.Areas[0].vMax = <<-286.296906,2518.544678,76.564919>> 
			data.TriggerArea.Areas[0].fWidth = 16.000000
			SET_TRIGGER_AREA_DATA_BIT(0, eTRIGGERAREADATABITSET_REACT_AREA)
			
			data.TriggerArea.Areas[1].vMin = <<-286.342987,2532.834473,71.661880>> 
			data.TriggerArea.Areas[1].vMax = <<-286.296906,2518.544678,76.564919>> 
			data.TriggerArea.Areas[1].fWidth = 16.000000
			SET_TRIGGER_AREA_DATA_BIT(1, eTRIGGERAREADATABITSET_DEFENSIVE_AREA)
			
			data.Population.Blockers[0].eType = ePOPULATIONBLOCKTYPE_SCENARIO
			data.Population.Blockers[0].vMin = GET_AREA_EXTENT(<<-289.5400, 2529.1121, 73.6400>>, 10.0)
			data.Population.Blockers[0].vMax = GET_AREA_EXTENT(<<-289.5400, 2529.1121, 73.6400>>, 10.0, TRUE)
			
			data.Population.iCount = 1
		BREAK
		CASE DVS_LOCATION_12
			data.TriggerArea.Areas[0].vMin = <<774.721130,4181.103027,38.679733>> 
			data.TriggerArea.Areas[0].vMax = <<761.700623,4181.493652,42.701111>> 
			data.TriggerArea.Areas[0].fWidth = 16.000000
			SET_TRIGGER_AREA_DATA_BIT(0, eTRIGGERAREADATABITSET_REACT_AREA)
			
			data.TriggerArea.Areas[1].vMin = <<774.721130,4181.103027,38.679733>> 
			data.TriggerArea.Areas[1].vMax = <<761.700623,4181.493652,42.701111>> 
			data.TriggerArea.Areas[1].fWidth = 16.000000
			SET_TRIGGER_AREA_DATA_BIT(1, eTRIGGERAREADATABITSET_DEFENSIVE_AREA)
			
			data.Population.Blockers[0].eType = ePOPULATIONBLOCKTYPE_SCENARIO
			data.Population.Blockers[0].vMin = GET_AREA_EXTENT(<<774.0580, 4180.6670, 39.7090>>, 10.0)
			data.Population.Blockers[0].vMax = GET_AREA_EXTENT(<<774.0580, 4180.6670, 39.7090>>, 10.0, TRUE)
			
			data.Population.iCount = 1
		BREAK
		CASE DVS_LOCATION_13
			data.TriggerArea.Areas[0].vMin = <<3334.185791,5164.316406,15.297468>> 
			data.TriggerArea.Areas[0].vMax = <<3327.577148,5151.516113,20.306721>> 
			data.TriggerArea.Areas[0].fWidth = 16.000000
			SET_TRIGGER_AREA_DATA_BIT(0, eTRIGGERAREADATABITSET_REACT_AREA)
			
			data.TriggerArea.Areas[1].vMin = <<3334.185791,5164.316406,15.297468>> 
			data.TriggerArea.Areas[1].vMax = <<3327.577148,5151.516113,20.306721>> 
			data.TriggerArea.Areas[1].fWidth = 16.000000
			SET_TRIGGER_AREA_DATA_BIT(1, eTRIGGERAREADATABITSET_DEFENSIVE_AREA)
		
			data.Population.Blockers[0].eType = ePOPULATIONBLOCKTYPE_SCENARIO
			data.Population.Blockers[0].vMin = GET_AREA_EXTENT(<<3332.7251, 5159.2368, 17.3100>>, 15.0)
			data.Population.Blockers[0].vMax = GET_AREA_EXTENT(<<3332.7251, 5159.2368, 17.3100>>, 15.0, TRUE)
			
			data.Population.Blockers[1].eType = ePOPULATIONBLOCKTYPE_VEHICLE_GEN
			data.Population.Blockers[1].vMin = <<3326.42871, 5169.90869, 16.36385>>
			data.Population.Blockers[1].vMax = <<3341.39233, 5150.87695, 20.59692>>
			
			data.Population.iCount = 2
		BREAK
		CASE DVS_LOCATION_14
			data.TriggerArea.Areas[0].vMin = <<-1509.940918,1490.981323,113.059570>> 
			data.TriggerArea.Areas[0].vMax = <<-1507.779663,1476.923828,119.441391>> 
			data.TriggerArea.Areas[0].fWidth = 16.000000
			SET_TRIGGER_AREA_DATA_BIT(0, eTRIGGERAREADATABITSET_REACT_AREA)
			
			data.TriggerArea.Areas[1].vMin = <<-1509.940918,1490.981323,113.059570>> 
			data.TriggerArea.Areas[1].vMax = <<-1507.779663,1476.923828,119.441391>> 
			data.TriggerArea.Areas[1].fWidth = 16.000000
			SET_TRIGGER_AREA_DATA_BIT(1, eTRIGGERAREADATABITSET_DEFENSIVE_AREA)
		
			data.Population.Blockers[0].eType = ePOPULATIONBLOCKTYPE_SCENARIO
			data.Population.Blockers[0].vMin = GET_AREA_EXTENT(<<-1513.4470, 1483.3260, 115.4030>>, 10.0)
			data.Population.Blockers[0].vMax = GET_AREA_EXTENT(<<-1513.4470, 1483.3260, 115.4030>>, 10.0, TRUE)
			
			data.Population.Blockers[1].eType = ePOPULATIONBLOCKTYPE_VEHICLE_GEN
			data.Population.Blockers[1].vMin = <<-1507.40088, 1477.77649, 114.38396>>
			data.Population.Blockers[1].vMax = <<-1517.64697, 1494.84155, 118.08833>>
			
			data.Population.iCount = 2
		BREAK
		CASE DVS_LOCATION_15
			data.TriggerArea.Areas[0].vMin = <<2480.107422,3430.862549,47.004730>> 
			data.TriggerArea.Areas[0].vMax = <<2470.806396,3419.803467,51.867737>> 
			data.TriggerArea.Areas[0].fWidth = 18.000000
			SET_TRIGGER_AREA_DATA_BIT(0, eTRIGGERAREADATABITSET_REACT_AREA)
			
			data.TriggerArea.Areas[1].vMin = <<2480.107422,3430.862549,47.004730>> 
			data.TriggerArea.Areas[1].vMax = <<2470.806396,3419.803467,51.867737>> 
			data.TriggerArea.Areas[1].fWidth = 18.000000
			SET_TRIGGER_AREA_DATA_BIT(1, eTRIGGERAREADATABITSET_DEFENSIVE_AREA)
		
			data.Population.Blockers[0].eType = ePOPULATIONBLOCKTYPE_SCENARIO
			data.Population.Blockers[0].vMin = GET_AREA_EXTENT(<<2478.1370, 3430.0271, 49.1990>>, 15.0)
			data.Population.Blockers[0].vMax = GET_AREA_EXTENT(<<2478.1370, 3430.0271, 49.1990>>, 15.0, TRUE)
			
			data.Population.iCount = 1
		BREAK
		CASE DVS_LOCATION_16
			data.TriggerArea.Areas[0].vMin = <<1448.436279,-2230.151367,58.119709>> 
			data.TriggerArea.Areas[0].vMax = <<1448.576904,-2243.749512,63.470673>> 
			data.TriggerArea.Areas[0].fWidth = 16.000000
			SET_TRIGGER_AREA_DATA_BIT(0, eTRIGGERAREADATABITSET_REACT_AREA)
			
			data.TriggerArea.Areas[1].vMin = <<1448.436279,-2230.151367,58.119709>> 
			data.TriggerArea.Areas[1].vMax = <<1448.576904,-2243.749512,63.470673>> 
			data.TriggerArea.Areas[1].fWidth = 16.000000
			SET_TRIGGER_AREA_DATA_BIT(1, eTRIGGERAREADATABITSET_DEFENSIVE_AREA)
		BREAK
		CASE DVS_LOCATION_17
			data.TriggerArea.Areas[0].vMin = <<161.984390,-2654.404785,2.995953>> 
			data.TriggerArea.Areas[0].vMax = <<162.009720,-2638.407959,7.995954>> 
			data.TriggerArea.Areas[0].fWidth = 16.000000
			SET_TRIGGER_AREA_DATA_BIT(0, eTRIGGERAREADATABITSET_REACT_AREA)
			
			data.TriggerArea.Areas[1].vMin = <<161.984390,-2654.404785,2.995953>> 
			data.TriggerArea.Areas[1].vMax = <<162.009720,-2638.407959,7.995954>> 
			data.TriggerArea.Areas[1].fWidth = 16.000000
			SET_TRIGGER_AREA_DATA_BIT(1, eTRIGGERAREADATABITSET_DEFENSIVE_AREA)
		BREAK
		CASE DVS_LOCATION_18
			data.TriggerArea.Areas[0].vMin = <<233.654419,21.627237,80.961105>> 
			data.TriggerArea.Areas[0].vMax = <<246.970734,16.968390,86.093353>> 
			data.TriggerArea.Areas[0].fWidth = 17.000000
			SET_TRIGGER_AREA_DATA_BIT(0, eTRIGGERAREADATABITSET_REACT_AREA)
			
			data.TriggerArea.Areas[1].vMin = <<233.654419,21.627237,80.961105>> 
			data.TriggerArea.Areas[1].vMax = <<246.970734,16.968390,86.093353>> 
			data.TriggerArea.Areas[1].fWidth = 17.000000
			SET_TRIGGER_AREA_DATA_BIT(1, eTRIGGERAREADATABITSET_DEFENSIVE_AREA)
		BREAK
		CASE DVS_LOCATION_19
			data.TriggerArea.Areas[0].vMin = <<-1369.613159,-324.752747,36.236115>> 
			data.TriggerArea.Areas[0].vMax = <<-1381.687866,-332.462341,41.524109>> 
			data.TriggerArea.Areas[0].fWidth = 17.000000
			SET_TRIGGER_AREA_DATA_BIT(0, eTRIGGERAREADATABITSET_REACT_AREA)
			
			data.TriggerArea.Areas[1].vMin = <<-1369.613159,-324.752747,36.236115>> 
			data.TriggerArea.Areas[1].vMax = <<-1381.687866,-332.462341,41.524109>> 
			data.TriggerArea.Areas[1].fWidth = 17.000000
			SET_TRIGGER_AREA_DATA_BIT(1, eTRIGGERAREADATABITSET_DEFENSIVE_AREA)
		
			data.Population.Blockers[0].eType = ePOPULATIONBLOCKTYPE_SCENARIO
			data.Population.Blockers[0].vMin = GET_AREA_EXTENT(<<-1371.8870, -330.3250, 38.1090>>, 15.0)
			data.Population.Blockers[0].vMax = GET_AREA_EXTENT(<<-1371.8870, -330.3250, 38.1090>>, 15.0, TRUE)
			
			data.Population.Blockers[1].eType = ePOPULATIONBLOCKTYPE_VEHICLE_GEN
			data.Population.Blockers[1].vMin = <<-1381.14307, -326.47748, 37.44429>>
			data.Population.Blockers[1].vMax = <<-1360.50598, -338.37012, 41.61538>>
			
			data.Population.iCount = 2
		BREAK
		CASE DVS_LOCATION_20
			data.TriggerArea.Areas[0].vMin = <<-936.980957,311.868744,68.245712>> 
			data.TriggerArea.Areas[0].vMax = <<-936.929810,301.754761,73.018921>> 
			data.TriggerArea.Areas[0].fWidth = 17.000000
			SET_TRIGGER_AREA_DATA_BIT(0, eTRIGGERAREADATABITSET_REACT_AREA)
			
			data.TriggerArea.Areas[1].vMin = <<-936.980957,311.868744,68.245712>> 
			data.TriggerArea.Areas[1].vMax = <<-936.929810,301.754761,73.018921>> 
			data.TriggerArea.Areas[1].fWidth = 17.000000
			SET_TRIGGER_AREA_DATA_BIT(1, eTRIGGERAREADATABITSET_DEFENSIVE_AREA)
		
			data.Population.Blockers[0].eType = ePOPULATIONBLOCKTYPE_SCENARIO
			data.Population.Blockers[0].vMin = GET_AREA_EXTENT(<<-935.6530, 307.5960, 70.2010>>, 15.0)
			data.Population.Blockers[0].vMax = GET_AREA_EXTENT(<<-935.6530, 307.5960, 70.2010>>, 15.0, TRUE)
			
			data.Population.iCount = 1
		BREAK
		CASE DVS_LOCATION_21
			data.TriggerArea.Areas[0].vMin = <<-966.767517,-1104.978638,-0.828153>> 
			data.TriggerArea.Areas[0].vMax = <<-955.943481,-1099.313110,4.150311>> 
			data.TriggerArea.Areas[0].fWidth = 17.000000
			SET_TRIGGER_AREA_DATA_BIT(0, eTRIGGERAREADATABITSET_REACT_AREA)
			
			data.TriggerArea.Areas[1].vMin = <<-966.767517,-1104.978638,-0.828153>> 
			data.TriggerArea.Areas[1].vMax = <<-955.943481,-1099.313110,4.150311>> 
			data.TriggerArea.Areas[1].fWidth = 17.000000
			SET_TRIGGER_AREA_DATA_BIT(1, eTRIGGERAREADATABITSET_DEFENSIVE_AREA)
		
			data.Population.Blockers[0].eType = ePOPULATIONBLOCKTYPE_SCENARIO
			data.Population.Blockers[0].vMin = GET_AREA_EXTENT(<<-935.6530, 307.5960, 70.2010>>, 15.0)
			data.Population.Blockers[0].vMax = GET_AREA_EXTENT(<<-935.6530, 307.5960, 70.2010>>, 15.0, TRUE)
			
			data.Population.iCount = 1
		BREAK
		CASE DVS_LOCATION_22
			data.TriggerArea.Areas[0].vMin = <<1035.473877,-775.108032,55.246300>> 
			data.TriggerArea.Areas[0].vMax = <<1046.137817,-771.841248,60.022827>> 
			data.TriggerArea.Areas[0].fWidth = 15.000000
			SET_TRIGGER_AREA_DATA_BIT(0, eTRIGGERAREADATABITSET_REACT_AREA)
			
			data.TriggerArea.Areas[1].vMin = <<1035.473877,-775.108032,55.246300>> 
			data.TriggerArea.Areas[1].vMax = <<1046.137817,-771.841248,60.022827>> 
			data.TriggerArea.Areas[1].fWidth = 15.000000
			SET_TRIGGER_AREA_DATA_BIT(1, eTRIGGERAREADATABITSET_DEFENSIVE_AREA)
		BREAK
		CASE DVS_LOCATION_23
			data.TriggerArea.Areas[0].vMin = <<332.719666,-2130.237793,11.722826>> 
			data.TriggerArea.Areas[0].vMax = <<343.360016,-2120.356689,17.732937>> 
			data.TriggerArea.Areas[0].fWidth = 10.000000
			SET_TRIGGER_AREA_DATA_BIT(0, eTRIGGERAREADATABITSET_REACT_AREA)
			
			data.TriggerArea.Areas[1].vMin = <<332.719666,-2130.237793,11.722826>> 
			data.TriggerArea.Areas[1].vMax = <<343.360016,-2120.356689,17.732937>> 
			data.TriggerArea.Areas[1].fWidth = 10.000000
			SET_TRIGGER_AREA_DATA_BIT(1, eTRIGGERAREADATABITSET_DEFENSIVE_AREA)
		
			data.Population.Blockers[0].eType = ePOPULATIONBLOCKTYPE_SCENARIO
			data.Population.Blockers[0].vMin = GET_AREA_EXTENT(<<337.4230, -2128.3081, 14.1630>>, 10.0)
			data.Population.Blockers[0].vMax = GET_AREA_EXTENT(<<337.4230, -2128.3081, 14.1630>>, 10.0, TRUE)
			
			data.Population.iCount = 1
		BREAK
		CASE DVS_LOCATION_24
			data.TriggerArea.Areas[0].vMin = <<-25.662518,-1489.863037,27.577801>> 
			data.TriggerArea.Areas[0].vMax = <<-36.810795,-1502.809204,38.664978>> 
			data.TriggerArea.Areas[0].fWidth = 14.000000
			SET_TRIGGER_AREA_DATA_BIT(0, eTRIGGERAREADATABITSET_REACT_AREA)
			
			data.TriggerArea.Areas[1].vMin = <<-25.662518,-1489.863037,27.577801>> 
			data.TriggerArea.Areas[1].vMax = <<-36.810795,-1502.809204,38.664978>> 
			data.TriggerArea.Areas[1].fWidth = 14.000000
			SET_TRIGGER_AREA_DATA_BIT(1, eTRIGGERAREADATABITSET_DEFENSIVE_AREA)
		BREAK
		CASE DVS_LOCATION_25
			data.TriggerArea.Areas[0].vMin = <<-361.860748,-58.325539,51.428146>> 
			data.TriggerArea.Areas[0].vMax = <<-354.593109,-68.446838,57.428146>> 
			data.TriggerArea.Areas[0].fWidth = 15.000000
			SET_TRIGGER_AREA_DATA_BIT(0, eTRIGGERAREADATABITSET_REACT_AREA)
			
			data.TriggerArea.Areas[1].vMin = <<-361.860748,-58.325539,51.428146>> 
			data.TriggerArea.Areas[1].vMax = <<-354.593109,-68.446838,57.428146>> 
			data.TriggerArea.Areas[1].fWidth = 15.000000
			SET_TRIGGER_AREA_DATA_BIT(1, eTRIGGERAREADATABITSET_DEFENSIVE_AREA)
		BREAK
		CASE DVS_LOCATION_26
			data.TriggerArea.Areas[0].vMin = <<469.987305,-1239.682007,26.731651>> 
			data.TriggerArea.Areas[0].vMax = <<472.342194,-1228.146729,32.873703>> 
			data.TriggerArea.Areas[0].fWidth = 15.000000
			SET_TRIGGER_AREA_DATA_BIT(0, eTRIGGERAREADATABITSET_REACT_AREA)
			
			data.TriggerArea.Areas[1].vMin = <<469.987305,-1239.682007,26.731651>> 
			data.TriggerArea.Areas[1].vMax = <<472.342194,-1228.146729,32.873703>> 
			data.TriggerArea.Areas[1].fWidth = 15.000000
			SET_TRIGGER_AREA_DATA_BIT(1, eTRIGGERAREADATABITSET_DEFENSIVE_AREA)
		BREAK
		CASE DVS_LOCATION_27
			data.TriggerArea.Areas[0].vMin = <<1391.072998,-1542.141235,53.709221>> 
			data.TriggerArea.Areas[0].vMax = <<1390.761963,-1524.761719,60.322670>> 
			data.TriggerArea.Areas[0].fWidth = 15.000000
			SET_TRIGGER_AREA_DATA_BIT(0, eTRIGGERAREADATABITSET_REACT_AREA)
			
			data.TriggerArea.Areas[1].vMin = <<1391.072998,-1542.141235,53.709221>> 
			data.TriggerArea.Areas[1].vMax = <<1390.761963,-1524.761719,60.322670>> 
			data.TriggerArea.Areas[1].fWidth = 15.000000
			SET_TRIGGER_AREA_DATA_BIT(1, eTRIGGERAREADATABITSET_DEFENSIVE_AREA)
		BREAK
		CASE DVS_LOCATION_28
			data.TriggerArea.Areas[0].vMin = <<38.658344,-884.371094,27.243511>> 
			data.TriggerArea.Areas[0].vMax = <<26.813839,-879.886292,33.267200>> 
			data.TriggerArea.Areas[0].fWidth = 16.000000
			SET_TRIGGER_AREA_DATA_BIT(0, eTRIGGERAREADATABITSET_REACT_AREA)
			
			data.TriggerArea.Areas[1].vMin = <<38.658344,-884.371094,27.243511>> 
			data.TriggerArea.Areas[1].vMax = <<26.813839,-879.886292,33.267200>> 
			data.TriggerArea.Areas[1].fWidth = 16.000000
			SET_TRIGGER_AREA_DATA_BIT(1, eTRIGGERAREADATABITSET_DEFENSIVE_AREA)
		BREAK
		CASE DVS_LOCATION_29
			data.TriggerArea.Areas[0].vMin = <<-1002.923645,-2526.458984,10.830778>> 
			data.TriggerArea.Areas[0].vMax = <<-997.778137,-2535.955322,16.830776>> 
			data.TriggerArea.Areas[0].fWidth = 14.000000
			SET_TRIGGER_AREA_DATA_BIT(0, eTRIGGERAREADATABITSET_REACT_AREA)
			
			data.TriggerArea.Areas[1].vMin = <<-1002.923645,-2526.458984,10.830778>> 
			data.TriggerArea.Areas[1].vMax = <<-997.778137,-2535.955322,16.830776>> 
			data.TriggerArea.Areas[1].fWidth = 14.000000
			SET_TRIGGER_AREA_DATA_BIT(1, eTRIGGERAREADATABITSET_DEFENSIVE_AREA)
			
			data.Population.Blockers[0].eType = ePOPULATIONBLOCKTYPE_VEHICLE_GEN
			data.Population.Blockers[0].vMin = <<-995.96570, -2539.31177, 11.97358>>
			data.Population.Blockers[0].vMax = <<-1004.58112, -2528.93652, 15.83078>>
			
			data.Population.iCount = 1
		BREAK
		CASE DVS_LOCATION_30
			data.TriggerArea.Areas[0].vMin = <<2683.179443,1562.670288,21.513088>> 
			data.TriggerArea.Areas[0].vMax = <<2670.715332,1562.556396,29.602880>> 
			data.TriggerArea.Areas[0].fWidth = 16.000000
			SET_TRIGGER_AREA_DATA_BIT(0, eTRIGGERAREADATABITSET_REACT_AREA)
			
			data.TriggerArea.Areas[1].vMin = <<2683.179443,1562.670288,21.513088>> 
			data.TriggerArea.Areas[1].vMax = <<2670.715332,1562.556396,29.602880>> 
			data.TriggerArea.Areas[1].fWidth = 16.000000
			SET_TRIGGER_AREA_DATA_BIT(1, eTRIGGERAREADATABITSET_DEFENSIVE_AREA)
		BREAK
	
	ENDSWITCH

ENDPROC

FUNC BOOL DRUG_VEHICLE_CLIENT_SHOULD_PROGRESS_TO_COLLECT()
	IF IS_GENERIC_CLIENT_BIT_SET(PARTICIPANT_ID(), eGENERICCLIENTBITSET_I_AM_AT_GOTO_LOCATION)
		RETURN TRUE
	ENDIF
	RETURN IS_MISSION_ENTITY_BIT_SET(0, eMISSIONENTITYBITSET_COLLECTED_FOR_FIRST_TIME)
ENDFUNC

PROC DRUG_VEHICLE_DELIVER_VEHICLE_OBJECTIVE_TEXT()

	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		Print_Objective_Text("BBOT_LOSECOPS")
		EXIT
	ENDIF

	TEXT_LABEL_15 tlLocation
	SWITCH INT_TO_ENUM(DRUG_VEHICLE_DRUG_TYPE, serverBD.iRandomModeInt0)
		CASE eDV_DRUGTYPE_METH
			SWITCH GET_LOCAL_PLAYER_MISSION_TEAM()
				CASE eMISSIONTEAMS_UNAFFILIATED
					tlLocation = "FMC_LOC_GRTCHP"
				BREAK
				
				#IF FEATURE_COPS_N_CROOKS
				CASE eMISSIONTEAMS_COP
					tlLocation = "FMC_LOC_SNSHPS"
				BREAK
				#ENDIF
				
			ENDSWITCH
		BREAK
		CASE eDV_DRUGTYPE_COKE
			SWITCH GET_LOCAL_PLAYER_MISSION_TEAM()
				CASE eMISSIONTEAMS_UNAFFILIATED
					tlLocation = "FMC_LOC_LAMESA"
				BREAK
				
				#IF FEATURE_COPS_N_CROOKS
				CASE eMISSIONTEAMS_COP
					tlLocation = "FMC_LOC_MIRWPS"
				BREAK
				#ENDIF
				
			ENDSWITCH
		BREAK
		CASE eDV_DRUGTYPE_WEED
			SWITCH GET_LOCAL_PLAYER_MISSION_TEAM()
				CASE eMISSIONTEAMS_UNAFFILIATED
					tlLocation = "FMC_LOC_STRAWB"
				BREAK
				
				#IF FEATURE_COPS_N_CROOKS
				CASE eMISSIONTEAMS_COP
					tlLocation = "FMC_LOC_VESPPS"
				BREAK
				#ENDIF
				
			ENDSWITCH
		BREAK
	ENDSWITCH

	Print_Objective_Text_With_Coloured_Text_Label("RE_DELIVER_VEH", tlLocation, HUD_COLOUR_YELLOW)
ENDPROC

PROC DRUG_VEHICLE_CLEAR_OBJECTIVE_TEXT()
	IF Is_There_Any_Current_Objective_Text_From_This_Script()
		Clear_Any_Objective_Text_From_This_Script()
	ENDIF
ENDPROC

PROC DRUG_VEHICLE_SETUP_MODE_STATES()

	ADD_MODE_STATE(eDV_MODESTATE_GO_TO_EVENT, eMODESTATE_GO_TO_POINT)
		ADD_MODE_STATE_TRANSITION_DEFAULT(eDV_MODESTATE_GO_TO_EVENT, eDV_MODESTATE_COLLECT_VEHICLE)
	
	ADD_MODE_STATE(eDV_MODESTATE_COLLECT_VEHICLE, eMODESTATE_COLLECT_MISSION_ENTITY)
		ADD_MODE_STATE_TRANSITION_DEFAULT(eDV_MODESTATE_COLLECT_VEHICLE, eDV_MODESTATE_DELIVER_VEHICLE)
		
	ADD_MODE_STATE(eDV_MODESTATE_DELIVER_VEHICLE, eMODESTATE_DELIVER_MISSION_ENTITY)
		ADD_MODE_STATE_TRANSITION_DEFAULT(eDV_MODESTATE_DELIVER_VEHICLE, eDV_MODESTATE_END)
	
	ADD_MODE_STATE(eDV_MODESTATE_END, eMODESTATE_END)

ENDPROC

PROC DRUG_VEHICLE_SETUP_CLIENT_MODE_STATES()

	ADD_CLIENT_MODE_STATE(eDV_CLIENTMODESTATE_GO_TO_EVENT, eMODESTATE_GO_TO_POINT, &EMPTY)
		ADD_CLIENT_MODE_STATE_TRANSITION_CUSTOM(eDV_CLIENTMODESTATE_GO_TO_EVENT, eDV_CLIENTMODESTATE_COLLECT_VEHICLE, &DRUG_VEHICLE_CLIENT_SHOULD_PROGRESS_TO_COLLECT)
		
	ADD_CLIENT_MODE_STATES_DELIVERY(eDV_CLIENTMODESTATE_COLLECT_VEHICLE, eDV_CLIENTMODESTATE_DELIVER_VEHICLE, eDV_CLIENTMODESTATE_HELP_DELIVER_VEHICLE, eDV_CLIENTMODESTATE_RECOVER_VEHICLE, eDV_CLIENTMODESTATE_END,
		&DRUG_VEHICLE_CLEAR_OBJECTIVE_TEXT, &DRUG_VEHICLE_DELIVER_VEHICLE_OBJECTIVE_TEXT, &DRUG_VEHICLE_CLEAR_OBJECTIVE_TEXT, &DRUG_VEHICLE_CLEAR_OBJECTIVE_TEXT)
	
	ADD_CLIENT_MODE_STATE(eDV_CLIENTMODESTATE_END, eMODESTATE_END, &EMPTY)

ENDPROC

FUNC BOOL DRUG_VEHICLE_SERVER_INIT()
	RETURN TRUE
ENDFUNC

FUNC BOOL DRUG_VEHICLE_CLIENT_INIT()

	#IF IS_DEBUG_BUILD
	g_sRandomEventDebug.bWarpToDrugVehicle = FALSE
	#ENDIF

	RETURN TRUE
ENDFUNC

PROC DRUG_VEHICLE_CLIENT_MAINTAIN()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[0].netId)
	AND IS_ENTITY_ALIVE(NET_TO_VEH(serverBD.sVehicle[0].netId))
		IF RANDOM_EVENT_IS_LOCAL_PLAYER_RESTRICTED_WITH_PASSIVE()
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.sVehicle[0].netId))
				IF NOT GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.sVehicle[0].netId), PLAYER_ID())
					SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.sVehicle[0].netId), PLAYER_ID(), TRUE)
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - DRUG_VEHICLE_CLIENT_MAINTAIN - Locked doors to local player as they are in passive mode")
				ENDIF
			ELSE
				IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(NET_TO_VEH(serverBD.sVehicle[0].netId))
					IF NOT IS_PED_PERFORMING_SCRIPT_TASK(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_VEHICLE)
						TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.sVehicle[0].netId))
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - DRUG_VEHICLE_CLIENT_MAINTAIN - Task local ped with leaving vehicle as they are in passive mode")
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.sVehicle[0].netId), PLAYER_ID())
				SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.sVehicle[0].netId), PLAYER_ID(), FALSE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC INT DRUG_VEHICLE_GET_WINNER_CASH_REWARD()
	RETURN g_sMPTunables.iSUM_RANDOM_EVENT_DRUG_VEHICLE_WINNER_CASH
ENDFUNC

FUNC INT DRUG_VEHICLE_GET_WINNER_RP_REWARD()
	RETURN g_sMPTunables.iSUM_RANDOM_EVENT_DRUG_VEHICLE_WINNER_RP
ENDFUNC

FUNC BOOL DRUG_VEHICLE_SHOULD_PED_ATTACK(INT iPed, PED_INDEX pedID)
	UNUSED_PARAMETER(pedID)
	RETURN HAS_PED_BEEN_TRIGGERED(iPed)
ENDFUNC

PROC DRUG_VEHICLE_ON_ATTACK_HATED_TARGETS(INT iPed, PED_INDEX pedID)
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - DRUG_VEHICLE_ON_ATTACK_HATED_TARGETS - Calling SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT for iPed #", iPed)
	SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(pedId)
		
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - DRUG_VEHICLE_ON_ATTACK_HATED_TARGETS - Calling SET_PED_DIES_WHEN_INJURED for iPed #", iPed)
	SET_PED_DIES_WHEN_INJURED(pedId, TRUE)
	
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedId, FALSE)
	SET_PED_COMBAT_ATTRIBUTES(pedId, CA_ALWAYS_FIGHT, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(pedId, CA_ALWAYS_FLEE, FALSE)
ENDPROC

PROC DRUG_VEHICLE_SETUP_PED_TRIGGERS(INT iPed)
	ADD_PED_TRIGGER(iPed, &PED_TRIGGER_HAS_PED_BEEN_TARGETTED)
	ADD_PED_TRIGGER(iPed, &PED_TRIGGER_HAS_PED_TRIGGER_AREA_BEEN_ACTIVATED)
	ADD_PED_TRIGGER(iPed, &PED_TRIGGER_HAS_PED_RECEIVED_PED_EVENT)
	ADD_PED_TRIGGER(iPed, &PED_TRIGGER_HAS_PED_BEEN_DAMAGED)
	ADD_PED_TRIGGER(iPed, &PED_TRIGGER_HAS_PED_BEEN_KNOCKED_INTO)
ENDPROC

PROC DRUG_VEHICLE_SETUP_PED_TASKS(INT iBehaviour)
	ADD_PED_TASK(iBehaviour, eDV_PEDTASK_DO_SCENARIO, ePEDTASK_DO_SCENARIOS)
		ADD_PED_TASK_TRANSITION(iBehaviour, eDV_PEDTASK_DO_SCENARIO, eDV_PEDTASK_ATTACK, &DRUG_VEHICLE_SHOULD_PED_ATTACK)
		
	ADD_PED_TASK(iBehaviour, eDV_PEDTASK_ATTACK, ePEDTASK_ATTACK_HATED_TARGETS)
ENDPROC

FUNC BOOL DRUG_VEHICLE_VEHICLE_MODS(INT iVehicle, VEHICLE_SETUP_STRUCT_MP& sData)
	SWITCH iVehicle
		CASE 0
			SWITCH data.Vehicle.Vehicles[iVehicle].model
				CASE FACTION
					SWITCH GET_DRUG_VEHICLE_DRUG_TYPE()
						CASE eDV_DRUGTYPE_METH
							sData.VehicleSetup.tlPlateText = "27IZA535"
							sData.VehicleSetup.iColour1 = 88
							sData.VehicleSetup.iColour2 = 0
							sData.VehicleSetup.iColourExtra1 = 88
							sData.VehicleSetup.iColourExtra2 = 0
							sData.iColour5 = 1
							sData.iColour6 = 132
							sData.iLivery2 = 0
							sData.VehicleSetup.iWheelType = 1
							sData.VehicleSetup.iTyreR = 255
							sData.VehicleSetup.iTyreG = 255
							sData.VehicleSetup.iTyreB = 255
							sData.VehicleSetup.iNeonR = 255
							sData.VehicleSetup.iNeonB = 255
							RETURN TRUE
						BREAK
						CASE eDV_DRUGTYPE_COKE
							sData.VehicleSetup.tlPlateText = "27IZA535"
							sData.VehicleSetup.iColour1 = 30
							sData.VehicleSetup.iColour2 = 112
							sData.VehicleSetup.iColourExtra1 = 36
							sData.VehicleSetup.iColourExtra2 = 0
							sData.iColour5 = 1
							sData.iColour6 = 132
							sData.iLivery2 = 0
							sData.VehicleSetup.iWheelType = 1
							sData.VehicleSetup.iTyreR = 255
							sData.VehicleSetup.iTyreG = 255
							sData.VehicleSetup.iTyreB = 255
							sData.VehicleSetup.iNeonR = 255
							sData.VehicleSetup.iNeonB = 255
							SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
							RETURN TRUE
						BREAK
						CASE eDV_DRUGTYPE_WEED
							sData.VehicleSetup.tlPlateText = "27IZA535"
							sData.VehicleSetup.iColour1 = 53
							sData.VehicleSetup.iColour2 = 5
							sData.VehicleSetup.iColourExtra1 = 59
							sData.VehicleSetup.iColourExtra2 = 0
							sData.iColour5 = 1
							sData.iColour6 = 132
							sData.iLivery2 = 0
							sData.VehicleSetup.iWheelType = 1
							sData.VehicleSetup.iTyreR = 255
							sData.VehicleSetup.iTyreG = 255
							sData.VehicleSetup.iTyreB = 255
							sData.VehicleSetup.iNeonR = 255
							sData.VehicleSetup.iNeonB = 255
							SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_2)
							RETURN TRUE
						BREAK
					ENDSWITCH
				BREAK
				CASE PATRIOT
					SWITCH GET_DRUG_VEHICLE_DRUG_TYPE()
						CASE eDV_DRUGTYPE_METH
							sData.VehicleSetup.tlPlateText = "87JFC263"
							sData.VehicleSetup.iPlateIndex = 3
							sData.VehicleSetup.iColour1 = 112
							sData.VehicleSetup.iColour2 = 112
							sData.VehicleSetup.iColourExtra1 = 0
							sData.VehicleSetup.iColourExtra2 = 156
							sData.iColour5 = 1
							sData.iColour6 = 132
							sData.iLivery2 = 0
							sData.VehicleSetup.iWheelType = 3
							sData.VehicleSetup.iTyreR = 255
							sData.VehicleSetup.iTyreG = 255
							sData.VehicleSetup.iTyreB = 255
							sData.VehicleSetup.iNeonR = 255
							sData.VehicleSetup.iNeonB = 255
							SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_11)
							sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
							sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
							sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
							sData.VehicleSetup.iModIndex[MOD_BONNET] = 2
							sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
							sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
							RETURN TRUE
						BREAK
						CASE eDV_DRUGTYPE_COKE
							sData.VehicleSetup.tlPlateText = "87JFC263"
							sData.VehicleSetup.iPlateIndex = 3
							sData.VehicleSetup.iColour1 = 104
							sData.VehicleSetup.iColour2 = 112
							sData.VehicleSetup.iColourExtra1 = 104
							sData.VehicleSetup.iColourExtra2 = 156
							sData.iColour5 = 1
							sData.iColour6 = 132
							sData.iLivery2 = 0
							sData.VehicleSetup.iWheelType = 3
							sData.VehicleSetup.iTyreR = 255
							sData.VehicleSetup.iTyreG = 255
							sData.VehicleSetup.iTyreB = 255
							sData.VehicleSetup.iNeonR = 255
							sData.VehicleSetup.iNeonB = 255
							SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_11)
							sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
							sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
							sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
							sData.VehicleSetup.iModIndex[MOD_BONNET] = 3
							sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
							RETURN TRUE
						BREAK
						CASE eDV_DRUGTYPE_WEED
							sData.VehicleSetup.tlPlateText = "87JFC263"
							sData.VehicleSetup.iPlateIndex = 3
							sData.VehicleSetup.iColour1 = 95
							sData.VehicleSetup.iColour2 = 112
							sData.VehicleSetup.iColourExtra1 = 97
							sData.VehicleSetup.iColourExtra2 = 156
							sData.iColour5 = 1
							sData.iColour6 = 132
							sData.iLivery2 = 0
							sData.VehicleSetup.iWheelType = 3
							sData.VehicleSetup.iTyreR = 255
							sData.VehicleSetup.iTyreG = 255
							sData.VehicleSetup.iTyreB = 255
							sData.VehicleSetup.iNeonR = 255
							sData.VehicleSetup.iNeonB = 255
							SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_11)
							sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
							sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
							sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
							sData.VehicleSetup.iModIndex[MOD_BONNET] = 2
							sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
							sData.VehicleSetup.iModIndex[MOD_LIVERY] = 3
							RETURN TRUE
						BREAK
					ENDSWITCH
				BREAK
				CASE SULTAN
					SWITCH GET_DRUG_VEHICLE_DRUG_TYPE()
						CASE eDV_DRUGTYPE_METH
							sData.VehicleSetup.tlPlateText = "47YNI062"
							sData.VehicleSetup.iColour1 = 65
							sData.VehicleSetup.iColour2 = 112
							sData.VehicleSetup.iColourExtra1 = 87
							sData.VehicleSetup.iColourExtra2 = 156
							sData.iColour5 = 1
							sData.iColour6 = 132
							sData.iLivery2 = 0
							sData.VehicleSetup.iTyreR = 255
							sData.VehicleSetup.iTyreG = 255
							sData.VehicleSetup.iTyreB = 255
							sData.VehicleSetup.iNeonR = 255
							sData.VehicleSetup.iNeonB = 255
							SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_11)
							sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
							sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
							sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
							sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
							sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
							sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
							sData.VehicleSetup.iModIndex[MOD_BONNET] = 4
							sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
							RETURN TRUE
						BREAK
						CASE eDV_DRUGTYPE_COKE
							sData.VehicleSetup.tlPlateText = "47YNI062"
							sData.VehicleSetup.iColour1 = 38
							sData.VehicleSetup.iColour2 = 0
							sData.VehicleSetup.iColourExtra1 = 37
							sData.VehicleSetup.iColourExtra2 = 156
							sData.iColour5 = 1
							sData.iColour6 = 132
							sData.iLivery2 = 0
							sData.VehicleSetup.iTyreR = 255
							sData.VehicleSetup.iTyreG = 255
							sData.VehicleSetup.iTyreB = 255
							sData.VehicleSetup.iNeonR = 255
							sData.VehicleSetup.iNeonB = 255
							SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_11)
							sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
							sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
							sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
							sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
							sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
							sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
							sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
							sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
							RETURN TRUE
						BREAK
						CASE eDV_DRUGTYPE_WEED
							sData.VehicleSetup.tlPlateText = "09KES296"
							sData.VehicleSetup.iColour1 = 137
							sData.VehicleSetup.iColour2 = 112
							sData.VehicleSetup.iColourExtra1 = 3
							sData.VehicleSetup.iColourExtra2 = 156
							sData.iColour5 = 1
							sData.iColour6 = 132
							sData.iLivery2 = 0
							sData.VehicleSetup.iTyreR = 255
							sData.VehicleSetup.iTyreG = 255
							sData.VehicleSetup.iTyreB = 255
							sData.VehicleSetup.iNeonR = 255
							sData.VehicleSetup.iNeonB = 255
							SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_11)
							sData.VehicleSetup.iModIndex[MOD_SPOILER] = 3
							sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
							sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
							sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
							sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
							sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
							sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
							sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
							RETURN TRUE
						BREAK
					ENDSWITCH
				BREAK
				CASE FUTO
					SWITCH GET_DRUG_VEHICLE_DRUG_TYPE()
						CASE eDV_DRUGTYPE_METH
							sData.VehicleSetup.tlPlateText = "60SKJ517"
							sData.VehicleSetup.iColour1 = 51
							sData.VehicleSetup.iColour2 = 4
							sData.VehicleSetup.iColourExtra1 = 66
							sData.VehicleSetup.iColourExtra2 = 156
							sData.iColour5 = 1
							sData.iColour6 = 132
							sData.iLivery2 = 0
							sData.VehicleSetup.iWheelType = 5
							sData.VehicleSetup.iTyreR = 255
							sData.VehicleSetup.iTyreG = 255
							sData.VehicleSetup.iTyreB = 255
							sData.VehicleSetup.iNeonR = 255
							sData.VehicleSetup.iNeonB = 255
							sData.VehicleSetup.iModIndex[MOD_SPOILER] = 3
							sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
							sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
							sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
							sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
							sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
							sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
							sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
							sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
							RETURN TRUE
						BREAK
						CASE eDV_DRUGTYPE_COKE
							sData.VehicleSetup.tlPlateText = "60SKJ517"
							sData.VehicleSetup.iColour1 = 111
							sData.VehicleSetup.iColour2 = 0
							sData.VehicleSetup.iColourExtra1 = 0
							sData.VehicleSetup.iColourExtra2 = 156
							sData.iColour5 = 1
							sData.iColour6 = 132
							sData.iLivery2 = 0
							sData.VehicleSetup.iWheelType = 5
							sData.VehicleSetup.iTyreR = 255
							sData.VehicleSetup.iTyreG = 255
							sData.VehicleSetup.iTyreB = 255
							sData.VehicleSetup.iNeonR = 255
							sData.VehicleSetup.iNeonB = 255
							sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
							sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
							sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
							sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
							sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
							sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
							sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
							sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
							sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
							RETURN TRUE
						BREAK
						CASE eDV_DRUGTYPE_WEED
							sData.VehicleSetup.tlPlateText = "60SKJ517"
							sData.VehicleSetup.iColour1 = 0
							sData.VehicleSetup.iColour2 = 28
							sData.VehicleSetup.iColourExtra1 = 10
							sData.VehicleSetup.iColourExtra2 = 156
							sData.iColour5 = 1
							sData.iColour6 = 132
							sData.iLivery2 = 0
							sData.VehicleSetup.iWheelType = 5
							sData.VehicleSetup.iTyreR = 255
							sData.VehicleSetup.iTyreG = 255
							sData.VehicleSetup.iTyreB = 255
							sData.VehicleSetup.iNeonR = 255
							sData.VehicleSetup.iNeonB = 255
							sData.VehicleSetup.iModIndex[MOD_SPOILER] = 4
							sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
							sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 3
							sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
							sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
							sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
							sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
							sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
							sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
							RETURN TRUE
						BREAK
					ENDSWITCH
				BREAK
				CASE KANJO
					SWITCH GET_DRUG_VEHICLE_DRUG_TYPE()
						CASE eDV_DRUGTYPE_METH
							sData.VehicleSetup.tlPlateText = "06UYZ390"
							sData.VehicleSetup.iColour1 = 145
							sData.VehicleSetup.iColour2 = 88
							sData.VehicleSetup.iColourExtra1 = 74
							sData.VehicleSetup.iColourExtra2 = 156
							sData.iColour5 = 1
							sData.iColour6 = 132
							sData.iLivery2 = 0
							sData.VehicleSetup.iWheelType = 5
							sData.VehicleSetup.iTyreR = 255
							sData.VehicleSetup.iTyreG = 255
							sData.VehicleSetup.iTyreB = 255
							sData.VehicleSetup.iNeonR = 255
							sData.VehicleSetup.iNeonB = 255
							SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
							sData.VehicleSetup.iModIndex[MOD_SPOILER] = 8
							sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 8
							sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 8
							sData.VehicleSetup.iModIndex[MOD_SKIRT] = 4
							sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
							sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 3
							sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
							sData.VehicleSetup.iModIndex[MOD_BONNET] = 9
							sData.VehicleSetup.iModIndex[MOD_WING_L] = 8
							sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 2
							sData.VehicleSetup.iModIndex[MOD_DOOR_L] = 1
							sData.VehicleSetup.iModIndex[MOD_LIVERY] = 3
							RETURN TRUE
						BREAK
						CASE eDV_DRUGTYPE_COKE
							sData.VehicleSetup.tlPlateText = "06UYZ390"
							sData.VehicleSetup.iColour1 = 61
							sData.VehicleSetup.iColour2 = 5
							sData.VehicleSetup.iColourExtra1 = 63
							sData.VehicleSetup.iColourExtra2 = 156
							sData.iColour5 = 1
							sData.iColour6 = 132
							sData.iLivery2 = 0
							sData.VehicleSetup.iWheelType = 5
							sData.VehicleSetup.iTyreR = 255
							sData.VehicleSetup.iTyreG = 255
							sData.VehicleSetup.iTyreB = 255
							sData.VehicleSetup.iNeonR = 255
							sData.VehicleSetup.iNeonB = 255
							SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
							sData.VehicleSetup.iModIndex[MOD_SPOILER] = 8
							sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 8
							sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 8
							sData.VehicleSetup.iModIndex[MOD_SKIRT] = 4
							sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
							sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 3
							sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
							sData.VehicleSetup.iModIndex[MOD_BONNET] = 9
							sData.VehicleSetup.iModIndex[MOD_WING_L] = 8
							sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 2
							sData.VehicleSetup.iModIndex[MOD_DOOR_L] = 1
							sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
							RETURN TRUE
						BREAK
						CASE eDV_DRUGTYPE_WEED
							sData.VehicleSetup.tlPlateText = "06UYZ390"
							sData.VehicleSetup.iColour1 = 27
							sData.VehicleSetup.iColour2 = 112
							sData.VehicleSetup.iColourExtra1 = 36
							sData.VehicleSetup.iColourExtra2 = 156
							sData.iColour5 = 1
							sData.iColour6 = 132
							sData.iLivery2 = 0
							sData.VehicleSetup.iWheelType = 5
							sData.VehicleSetup.iTyreR = 255
							sData.VehicleSetup.iTyreG = 255
							sData.VehicleSetup.iTyreB = 255
							sData.VehicleSetup.iNeonR = 255
							sData.VehicleSetup.iNeonB = 255
							SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
							sData.VehicleSetup.iModIndex[MOD_SPOILER] = 8
							sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 8
							sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 8
							sData.VehicleSetup.iModIndex[MOD_SKIRT] = 4
							sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
							sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 3
							sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
							sData.VehicleSetup.iModIndex[MOD_BONNET] = 9
							sData.VehicleSetup.iModIndex[MOD_WING_L] = 8
							sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 2
							sData.VehicleSetup.iModIndex[MOD_DOOR_L] = 1
							sData.VehicleSetup.iModIndex[MOD_LIVERY] = 5
							RETURN TRUE
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC DRUG_VEHICLE_SPAWN_ATTRIBUTES(INT iVehicle, VEHICLE_INDEX vehID)
	UNUSED_PARAMETER(iVehicle)
	SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(vehID, FALSE)
	HOLD_VEHICLE_DOOR_OPEN(vehID, SC_DOOR_BOOT, FALSE, TRUE, DT_DOOR_NO_RESET)
ENDPROC

PROC DRUG_VEHICLE_MAINTAIN_DOORS(INT iVehicle, VEHICLE_INDEX vehID)
	UNUSED_PARAMETER(iVehicle)
	IF NOT IS_MISSION_ENTITY_BIT_SET(0, eMISSIONENTITYBITSET_COLLECTED_FOR_FIRST_TIME)
		IF NOT IS_VEHICLE_DOOR_DAMAGED(vehID, SC_DOOR_BOOT)
			IF GET_VEHICLE_DOOR_ANGLE_RATIO(vehID, SC_DOOR_BOOT) < 0.8
			AND NETWORK_HAS_CONTROL_OF_ENTITY(vehID)
				HOLD_VEHICLE_DOOR_OPEN(vehID, SC_DOOR_BOOT, FALSE, TRUE, DT_DOOR_NO_RESET)
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_VEHICLE_DOOR_DAMAGED(vehID, SC_DOOR_BOOT)
			IF NOT HAS_NET_TIMER_STARTED(VehicleCloseDoorTimer)
			AND GET_VEHICLE_DOOR_ANGLE_RATIO(vehId, SC_DOOR_BOOT) > 0.1
				START_NET_TIMER(VehicleCloseDoorTimer)
			ELIF HAS_NET_TIMER_EXPIRED(VehicleCloseDoorTimer, 2000)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehID)
				AND NETWORK_HAS_CONTROL_OF_ENTITY(vehID)
					IF CLOSE_VEHICLE_DOOR_OVER_TIME(vehID, SC_DOOR_BOOT)
						RESET_NET_TIMER(VehicleCloseDoorTimer)
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - DRUG_VEHICLE_MAINTAIN_DOORS - Closed boot after collection")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC VECTOR DRUG_VEHICLE_GET_PARTICIPATION_COORD()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[0].netId)
		RETURN GET_ENTITY_COORDS(NET_TO_ENT(serverBD.sVehicle[0].netId), FALSE)
	ENDIF
	RETURN data.EventLocationBlips.Blip[0].vCoord
ENDFUNC

FUNC BOOL DRUG_VEHICLE_SHOULD_START_MODE_TIMER()
	RETURN TRUE
ENDFUNC

FUNC BOOL DRUG_VEHICLE_SHOULD_RESTART_MODE_TIMER()
	RETURN IS_MISSION_ENTITY_BIT_SET(0, eMISSIONENTITYBITSET_COLLECTED_FOR_FIRST_TIME)
ENDFUNC

PROC DRUG_VEHICLE_ON_EVENT_LOCATION_BLIP_CREATION(INT iLocation)
	UNUSED_PARAMETER(iLocation)
	
	#IF FEATURE_COPS_N_CROOKS
	IF GET_LOCAL_PLAYER_MISSION_TEAM() = eMISSIONTEAMS_COP
	AND NOT IS_GENERIC_BIT_SET(eGENERICBITSET_TRIGGERED_ON_CREATE_EVENT_LOCATION_BLIP)
		//DO_DISPATCH_CALL(eDISPATCH_CALL_DRUG_VEHICLE_NEARBY)
		SET_GENERIC_BIT(eGENERICBITSET_TRIGGERED_ON_CREATE_EVENT_LOCATION_BLIP)
	ENDIF
	#ENDIF
ENDPROC

FUNC STRING DRUG_VEHICLE_EVENT_LOCATION_BLIP_NAME(INT iLocation)
	UNUSED_PARAMETER(iLocation)
	RETURN "RE_BLIP_DRUGV"
ENDFUNC

FUNC BOOL DRUG_VEHICLE_SHOULD_SHOW_EVENT_LOCATION_AREA_BLIP(INT iLocation)
	UNUSED_PARAMETER(iLocation)
	RETURN FALSE
ENDFUNC

FUNC BOOL DRUG_VEHICLE_GET_MISSION_ENTITY_SHOULD_BE_BLIPPED(INT iMissionEntity)
	IF DOES_BLIP_EXIST(eventAreaCentreBlip[0])
	OR sMissionEntityLocal.sMissionEntity[iMissionEntity].holderPlayerId = LOCAL_PLAYER_INDEX
	OR IS_PLAYER_IN_MP_PROPERTY(LOCAL_PLAYER_INDEX, FALSE)
		RETURN FALSE
	ENDIF
	
	RETURN IS_GENERIC_CLIENT_BIT_SET(LOCAL_PARTICIPANT_INDEX, eGENERICCLIENTBITSET_I_AM_TEMPORARY_PARTICIPANT)
ENDFUNC

FUNC BLIP_SPRITE DRUG_VEHICLE_GET_MISSION_ENTITY_BLIP_SPRITE(INT iMissionEntity)
	UNUSED_PARAMETER(iMissionEntity)
	RETURN GET_RANDOM_EVENT_BLIP_SPRITE()
ENDFUNC

FUNC HUD_COLOURS DRUG_VEHICLE_GET_MISSION_ENTITY_BLIP_COLOUR(INT iMissionEntity)
	UNUSED_PARAMETER(iMissionEntity)
	RETURN GET_RANDOM_EVENT_BLIP_COLOUR()
ENDFUNC

FUNC STRING DRUG_VEHICLE_GET_MISSION_ENTITY_BLIP_NAME(INT iMissionEntity)
	UNUSED_PARAMETER(iMissionEntity)
	RETURN "RE_BLIP_DRUGV"
ENDFUNC

FUNC FLOAT DRUG_VEHICLE_GET_MISSION_ENTITY_BLIP_SCALE(INT iMissionEntity)
	UNUSED_PARAMETER(iMissionEntity)
	RETURN GET_RANDOM_EVENT_BLIP_SCALE()
ENDFUNC

PROC DRUG_VEHICLE_MISSION_ENTITY_ON_DELIVERY(INT iMissionEntity, BOOL bLocalDelivered)
	UNUSED_PARAMETER(iMissionEntity)
	IF bLocalDelivered
		SET_GENERIC_BIT(eGENERICBITSET_I_WON)
	ENDIF
ENDPROC

FUNC FREEMODE_DELIVERY_DROPOFFS DRUG_VEHICLE_SETUP_DROPOFF(INT iDropOff)
	
	SWITCH GET_DRUG_VEHICLE_DRUG_TYPE()	
		CASE eDV_DRUGTYPE_METH
			SWITCH GET_LOCAL_PLAYER_MISSION_TEAM()
				CASE eMISSIONTEAMS_UNAFFILIATED
					SWITCH iDropOff
						CASE 0		RETURN FMC_DROPOFF_GREAT_CHAPARRL
					ENDSWITCH
				BREAK
				
				#IF FEATURE_COPS_N_CROOKS
				CASE eMISSIONTEAMS_COP
					SWITCH iDropOff
						CASE 0		RETURN FMC_DROPOFF_SANDY_SHORES_SHERRIFF
					ENDSWITCH
				BREAK
				#ENDIF
				
			ENDSWITCH
		BREAK
		CASE eDV_DRUGTYPE_COKE
			SWITCH GET_LOCAL_PLAYER_MISSION_TEAM()
				CASE eMISSIONTEAMS_UNAFFILIATED
					SWITCH iDropOff
						CASE 0		RETURN FMC_DROPOFF_LA_MESA
					ENDSWITCH
				BREAK
				
				#IF FEATURE_COPS_N_CROOKS
				CASE eMISSIONTEAMS_COP
					SWITCH iDropOff
						CASE 0		RETURN FMC_DROPOFF_MISSION_ROW_POLICE
					ENDSWITCH
				BREAK
				#ENDIF
				
			ENDSWITCH
		BREAK
		CASE eDV_DRUGTYPE_WEED
			SWITCH GET_LOCAL_PLAYER_MISSION_TEAM()
				CASE eMISSIONTEAMS_UNAFFILIATED
					SWITCH iDropOff
						CASE 0		RETURN FMC_DROPOFF_STRAWBERRY
					ENDSWITCH
				BREAK
				
				#IF FEATURE_COPS_N_CROOKS
				CASE eMISSIONTEAMS_COP
					SWITCH iDropOff
						CASE 0		RETURN FMC_DROPOFF_VESPUCCI_POLICE
					ENDSWITCH
				BREAK
				#ENDIF
				
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FREEMODE_DELIVERY_DROPOFF_INVALID
ENDFUNC

FUNC VECTOR GET_DRUG_VEHICLE_PACKAGE_OFFSET()
	SWITCH data.Vehicle.Vehicles[0].model
		CASE FACTION		
			SWITCH GET_DRUG_VEHICLE_DRUG_TYPE()
				CASE eDV_DRUGTYPE_METH		RETURN <<0.0, -1.7, 0.07>>
				CASE eDV_DRUGTYPE_COKE		RETURN <<0.0, -2.0, 0.06>>
				CASE eDV_DRUGTYPE_WEED		RETURN <<0.0, -2.0, 0.07>>
			ENDSWITCH
		BREAK
		CASE PATRIOT
			SWITCH GET_DRUG_VEHICLE_DRUG_TYPE()
				CASE eDV_DRUGTYPE_METH		RETURN <<0.0, -2.0, 0.18>>
				CASE eDV_DRUGTYPE_COKE		RETURN <<0.0, -2.0, 0.2>>
				CASE eDV_DRUGTYPE_WEED		RETURN <<0.0, -2.0, 0.18>>
			ENDSWITCH
		BREAK
		CASE SULTAN
			SWITCH GET_DRUG_VEHICLE_DRUG_TYPE()
				CASE eDV_DRUGTYPE_METH		RETURN <<0.0, -1.9, 0.12>>
				CASE eDV_DRUGTYPE_COKE		RETURN <<0.0, -1.8, 0.12>>
				CASE eDV_DRUGTYPE_WEED		RETURN <<0.0, -1.9, 0.12>>
			ENDSWITCH
		BREAK
		CASE FUTO
			SWITCH GET_DRUG_VEHICLE_DRUG_TYPE()
				CASE eDV_DRUGTYPE_METH		RETURN <<0.0, -1.5, 0.02>>
				CASE eDV_DRUGTYPE_COKE		RETURN <<0.0, -1.625, 0.15>>
				CASE eDV_DRUGTYPE_WEED		RETURN <<0.0, -1.575, 0.05>>
			ENDSWITCH
		BREAK
		CASE KANJO
			SWITCH GET_DRUG_VEHICLE_DRUG_TYPE()
				CASE eDV_DRUGTYPE_METH		RETURN <<0.0, -1.5, 0.05>>
				CASE eDV_DRUGTYPE_COKE		RETURN <<0.0, -1.475, 0.02>>
				CASE eDV_DRUGTYPE_WEED		RETURN <<0.0, -1.5, 0.05>>
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC VECTOR GET_DRUG_VEHICLE_PACKAGE_ROTATION()
	SWITCH data.Vehicle.Vehicles[0].model
		CASE FACTION
		CASE PATRIOT
		CASE SULTAN
		CASE FUTO
		CASE KANJO
			SWITCH GET_DRUG_VEHICLE_DRUG_TYPE()
				CASE eDV_DRUGTYPE_WEED		RETURN <<0.0, 0.0, 90.0>>
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC BOOL DRUG_VEHICLE_SHOULD_PROP_CLEAN_UP(INT iProp)
	SWITCH iProp
		CASE 0
			IF IS_MISSION_ENTITY_BIT_SET(0, eMISSIONENTITYBITSET_DELIVERED)
			OR IS_MISSION_ENTITY_BIT_SET(0, eMISSIONENTITYBITSET_DESTROYED)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL DRUG_VEHICLE_GET_TEXT_MESSAGE_SHOULD_BE_SENT(INT iTextMessage)
	SWITCH iTextMessage
		CASE 0
			SWITCH GET_LOCAL_PLAYER_MISSION_TEAM()
				CASE eMISSIONTEAMS_UNAFFILIATED
					IF sMissionEntityLocal.sMissionEntity[0].holderPlayerId = PLAYER_ID()
						IF HAS_NET_TIMER_EXPIRED(textMessageDelayTimer, 5000)
							RETURN TRUE
						ENDIF
					ELSE
						IF HAS_NET_TIMER_STARTED(textMessageDelayTimer)
							RESET_NET_TIMER(textMessageDelayTimer)
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC enumCharacterList DRUG_VEHICLE_GET_TEXT_MESSAGE_CHARACTER(INT iTextMessage)
	enumCharacterList toReturn = CHAR_BLOCKED
	
	SWITCH iTextMessage
		CASE 0
			SWITCH GET_DRUG_VEHICLE_DRUG_TYPE()
				CASE eDV_DRUGTYPE_METH		toReturn = CHAR_MP_BIKER_BOSS	BREAK
				CASE eDV_DRUGTYPE_COKE		
					#IF FEATURE_DLC_2_2022
					toReturn = CHAR_VAGOS
					#ENDIF
					#IF NOT FEATURE_DLC_2_2022
					toReturn = CHAR_MP_MEX_BOSS
					#ENDIF
				BREAK
				CASE eDV_DRUGTYPE_WEED		toReturn = CHAR_MP_FAM_BOSS		BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN toReturn
ENDFUNC

FUNC STRING DRUG_VEHICLE_GET_TEXT_MESSAGE_LABEL(INT iTextMessage)

	SWITCH iTextMessage
		CASE 0
			SWITCH GET_DRUG_VEHICLE_DRUG_TYPE()
				CASE eDV_DRUGTYPE_METH		RETURN "RE_TXT_DV_0"		
				CASE eDV_DRUGTYPE_COKE		RETURN "RE_TXT_DV_1"		
				CASE eDV_DRUGTYPE_WEED		RETURN "RE_TXT_DV_2"		
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC BOOL DRUG_VEHICLE_SHOULD_HELP_TEXT_TRIGGER(INT iHelpText)
	SWITCH iHelpText
		CASE 0		RETURN sMissionEntityLocal.sMissionEntity[0].holderPlayerId = PLAYER_ID()
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC STRING DRUG_VEHICLE_GET_HELP_TEXT_STRING(INT iHelpText)
	SWITCH iHelpText
		CASE 0		
			SWITCH GET_LOCAL_PLAYER_MISSION_TEAM()
				CASE eMISSIONTEAMS_UNAFFILIATED
					RETURN "RE_HLP_DRUGV_1"
				BREAK
				
				#IF FEATURE_COPS_N_CROOKS
				CASE eMISSIONTEAMS_COP
					RETURN "RE_HLP_DRUGV_0"
				BREAK
				#ENDIF
				
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN ""
ENDFUNC

#IF IS_DEBUG_BUILD
PROC DRUG_VEHICLE_ADDITIONAL_DEBUG()
	IF g_sRandomEventDebug.bWarpToDrugVehicle
		sDebugVars.bSimulateJSkip = TRUE
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - DRUG_VEHICLE_ADDITIONAL_DEBUG - Warp to drug vehicle set, sDebugVars.bSimulateJSkip = TRUE")
		g_sRandomEventDebug.bWarpToDrugVehicle = FALSE
	ENDIF
ENDPROC
#ENDIF

PROC DRUG_VEHICLE_DATA_SETUP()
	SET_DATA_BIT(eDATABITSET_DISABLE_GANG_DELIVERY)
	
	// Attach Data
	data.Prop.AttachedProp[0].iPropIndex = 0
	data.Prop.AttachedProp[0].eParentType = ET_VEHICLE
	data.Prop.AttachedProp[0].iParentIndex = 0
	data.Prop.AttachedProp[0].vOffset = GET_DRUG_VEHICLE_PACKAGE_OFFSET()
	data.Prop.Props[0].vRotation = GET_DRUG_VEHICLE_PACKAGE_ROTATION()
ENDPROC

FUNC BOOL DRUG_VEHICLE_DETACH_PROP_ON_DEATH(INT iAttachIndex)
	RETURN iAttachIndex = 0
ENDFUNC

/// PURPOSE: Use to set up the logic pointers for your mission
///    
PROC SETUP_MISSION_LOGIC()
	
	// Server/Client States
	logic.StateMachine.SetupStates = &DRUG_VEHICLE_SETUP_MODE_STATES
	logic.StateMachine.SetupClientStates = &DRUG_VEHICLE_SETUP_CLIENT_MODE_STATES
	
	logic.Init.Server = &DRUG_VEHICLE_SERVER_INIT
	logic.Init.Client = &DRUG_VEHICLE_CLIENT_INIT
	
	logic.Data.Setup = &DRUG_VEHICLE_DATA_SETUP
	
	logic.Maintain.Client = &DRUG_VEHICLE_CLIENT_MAINTAIN
	
	logic.Reward.WinnerCash = &DRUG_VEHICLE_GET_WINNER_CASH_REWARD
	logic.Reward.WinnerRp = &DRUG_VEHICLE_GET_WINNER_RP_REWARD
	
	logic.Ped.Behaviour.Setup = &DRUG_VEHICLE_SETUP_PED_TASKS
	logic.Ped.Task.AttackHatedTargets.OnTask = &DRUG_VEHICLE_ON_ATTACK_HATED_TARGETS
	logic.Ped.Triggers.Setup = &DRUG_VEHICLE_SETUP_PED_TRIGGERS
	
	logic.Vehicle.Mods = &DRUG_VEHICLE_VEHICLE_MODS
	logic.Vehicle.Attributes = &DRUG_VEHICLE_SPAWN_ATTRIBUTES
	logic.Vehicle.Doors = &DRUG_VEHICLE_MAINTAIN_DOORS
	
	logic.Participation.Coords = &DRUG_VEHICLE_GET_PARTICIPATION_COORD
	
	logic.ModeTimer.Enable = &DRUG_VEHICLE_SHOULD_START_MODE_TIMER
	logic.ModeTimer.Restart = &DRUG_VEHICLE_SHOULD_RESTART_MODE_TIMER
	
	logic.EventLocationBlip.OnCreation = &DRUG_VEHICLE_ON_EVENT_LOCATION_BLIP_CREATION
	logic.EventLocationBlip.Name = &DRUG_VEHICLE_EVENT_LOCATION_BLIP_NAME
	logic.EventLocationBlip.ShowAreaBlip = &DRUG_VEHICLE_SHOULD_SHOW_EVENT_LOCATION_AREA_BLIP
	
	logic.MissionEntity.Blip.Enable = &DRUG_VEHICLE_GET_MISSION_ENTITY_SHOULD_BE_BLIPPED
	logic.MissionEntity.Blip.Sprite = &DRUG_VEHICLE_GET_MISSION_ENTITY_BLIP_SPRITE
	logic.MissionEntity.Blip.Colour = &DRUG_VEHICLE_GET_MISSION_ENTITY_BLIP_COLOUR
	logic.MissionEntity.Blip.Name = &DRUG_VEHICLE_GET_MISSION_ENTITY_BLIP_NAME
	logic.MissionEntity.Blip.Scale = &DRUG_VEHICLE_GET_MISSION_ENTITY_BLIP_SCALE
	logic.MissionEntity.Blip.PlayerScale = &DRUG_VEHICLE_GET_MISSION_ENTITY_BLIP_SCALE
	logic.MissionEntity.OnDelivery = &DRUG_VEHICLE_MISSION_ENTITY_ON_DELIVERY
	
	logic.Delivery.Dropoff.Setup = &DRUG_VEHICLE_SETUP_DROPOFF
	
	logic.Prop.DetachOnDeath = &DRUG_VEHICLE_DETACH_PROP_ON_DEATH
	logic.Prop.ShouldCleanup = &DRUG_VEHICLE_SHOULD_PROP_CLEAN_UP
	
	logic.TextMessages.ShouldSend = &DRUG_VEHICLE_GET_TEXT_MESSAGE_SHOULD_BE_SENT
	logic.TextMessages.Character = &DRUG_VEHICLE_GET_TEXT_MESSAGE_CHARACTER
	logic.TextMessages.Label = &DRUG_VEHICLE_GET_TEXT_MESSAGE_LABEL
	
	logic.HelpText.Trigger = &DRUG_VEHICLE_SHOULD_HELP_TEXT_TRIGGER
	logic.HelpText.Text = &DRUG_VEHICLE_GET_HELP_TEXT_STRING
	
	#IF IS_DEBUG_BUILD
	logic.Debug.AdditionalDebug = &DRUG_VEHICLE_ADDITIONAL_DEBUG
	#ENDIF
	
ENDPROC
#ENDIF












