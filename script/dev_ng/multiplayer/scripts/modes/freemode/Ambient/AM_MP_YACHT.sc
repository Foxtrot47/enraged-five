//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_MP_YACHT.sc																		//
// Description: Controls entrance to purchased properties in MP												//
// Written by:  Conor McGuire																				//
// Date: 2013-03-12 (ISO 8601) 																				//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
// Game Headers
USING "commands_network.sch"
USING "net_include.sch"
USING "context_control_public.sch"
USING "net_ambience.sch"
USING "net_spawn.sch"
USING "net_nodes.sch"
USING "fmmc_launcher_menu.sch"
USING "net_saved_vehicles.sch"
USING "net_realty_details.sch"
USING "net_MP_TV.sch"
USING "net_MP_Radio.sch"
USING "net_freemode_cut.sch"
USING "net_realty_new.sch"

USING "minigames_helpers.sch"
USING "net_doors.sch"

USING "net_realty_1_scene_public.sch"		//WALK_INTO_PROPERTY_CUTSCENE()
//USING "net_realty_2_scene_public.sch"		//DO_GARAGE_PED_ENTER_CUTSCENE(FALSE)
//USING "net_realty_3_scene_public.sch"		//DO_GARAGE_CAR_ENTER_CUTSCENE(FALSE)
//
//USING "net_realty_4_scene_public.sch"		//WALK_OUT_OF_PROPERTY()
//USING "net_realty_5_scene_public.sch"		//DO_GARAGE_PED_LEAVE_CUTSCENE()
//USING "net_realty_6_scene_public.sch"		//DO_GARAGE_CAR_LEAVE_CUTSCENE()

USING "net_wait_zero.sch"

USING "freemode_events_header.sch"
USING "net_swimwear.sch"

CONST_INT STAGE_INIT						0
CONST_INT STAGE_CHECK_FOR_IN_LOCATE			1 
CONST_INT STAGE_SETUP_FOR_MENU				2
CONST_INT STAGE_USING_MENU					3
CONST_INT STAGE_SETUP_FOR_USING_PROPERTY	4
CONST_INT STAGE_ENTER_PROPERTY				5
CONST_INT STAGE_LEAVE_ENTRANCE_AREA			6
CONST_INT STAGE_BUZZED_INTO_PROPERTY		7

CONST_INT MENU_TYPE_BUZZER					0
CONST_INT MENU_TYPE_AUTH_ENTRY				1
CONST_INT MENU_TYPE_PROPERTY_ENTRY_OPTIONS	2

CONST_INT LOCAL_BS_STARTED_FADE					0
CONST_INT LOCAL_BS_SKIP_ENTER_FADE				1
CONST_INT LOCAL_BS_CLEANUP_MP_PROPERTIES		2
CONST_INT LOCAL_BS_NO_FADE						3
CONST_INT LOCAL_BS_STARTED_CUTSCENE				4
CONST_INT LOCAL_BS_SETUP_MENU					5
CONST_INT LOCAL_BS_TAKEN_AWAY_CONTROL			6
CONST_INT LOCAL_BS_WANTED_HELP_TEXT				7
CONST_INT LOCAL_BS_JUST_BOUGHT_PROPERTY			8
CONST_INT LOCAL_BS_AWAITING_BUZZER_RESPONSE		9
CONST_INT LOCAL_BS_ENTER_NO_WARP				10
CONST_INT LOCAL_BS_ENTER_INGORE_PLAYER_OK		11
CONST_INT LOCAL_BS_USING_BUZZER_MENU			12
CONST_INT LOCAL_BS_BUZZER_REQUESTED_ANIM		13
CONST_INT LOCAL_BS_BUZZER_GIVE_GOTO_TASK		14
CONST_INT LOCAL_BS_BUZZER_GIVE_RING_TASK		15
CONST_INT LOCAL_BS_BUZZER_TRIGGERED_LOAD_SCENE	16
CONST_INT LOCAL_BS_PURCHASE_MENU_REQUEST_SF		17
CONST_INT LOCAL_BS_PURCHASE_MENU_SHOWING_WARN	18
CONST_INT LOCAL_BS_STOP_CUSTOM_MENU_CHECK		19
CONST_INT LOCAL_BS_FORCE_TERMINATED_INTERNET	20
CONST_INT LOCAL_BS_INVALID_VEHICLE_HELP			21
CONST_INT LOCAL_BS_WARPED_IN_PROPERTY			22
CONST_INT LOCAL_BS_SET_YACHT_DOOR_INVISIBLE		23

// Server bit for an active biker death match
CONST_INT LOCAL_BS_ON_IMPROMPTU_DEATHMATCH		24

CONST_INT ENTRY_CS_STAGE_INIT_DATA				0
CONST_INT ENTRY_CS_STAGE_WAIT_FOR_MODELS		1	
CONST_INT ENTRY_CS_STAGE_TRIGGER_CUT			2
CONST_INT ENTRY_CS_STAGE_RUN_CUT				3
CONST_INT ENTRY_CS_STAGE_RUN_SECOND_CUT			4
CONST_INT ENTRY_CS_STAGE_HANDLE_PLAYER_PED		5

CONST_INT ENTRY_CS_BS_GET_DATA					0
CONST_INT ENTRY_CS_BS_STARTED_MP_CUTSCENE		1
CONST_INT ENTRY_CS_BS_TOOK_AWAY_CONTROL			2
CONST_INT ENTRY_CS_BS_SECOND_CUT				3
CONST_INT ENTRY_CS_BS_APT_DOOR_MODEL_HIDE		4
CONST_INT ENTRY_CS_BS_STARTED_FAKE_CUT			5
CONST_INT ENTRY_CS_BS_RESET_DOOR				6

CONST_INT LOCAL_BS2_bCleanupOldPersonalVehicle		0
CONST_INT LOCAL_BS2_bClearedHeistPanCutscene		1
CONST_INT LOCAL_BS2_bKeepWantedFlag					2
CONST_INT LOCAL_BS2_bResetOnCallForTransition		3
CONST_INT LOCAL_BS2_bKeepScriptCameras				4
CONST_INT LOCAL_BS2_bEnteringAnotherPlayerProperty	5
CONST_INT LOCAL_BS2_bFadeOutWarpIntoGarage			6 //Bug 1615873 (this doesn't work so commenting out)
CONST_INT LOCAL_BS2_bCarEntryAborted				7
CONST_INT LOCAL_BS2_bKeepPurchaseContext			8
CONST_INT LOCAL_BS2_bWalkInTriggered				9
CONST_INT LOCAL_BS2_PlayerInVehicleDoingHeistDelivery	10

CONST_INT LOCAL_BS2_TriggeredAcceptWarning			12

CONST_INT LOCAL_BS2_bRunPropertySelectMenu			13
CONST_INT LOCAL_BS2_loadedGarageWalkInAnim			14
CONST_INT LOCAL_BS2_StoredYachtVariation			15
CONST_INT LOCAL_BS2_SetJacuzziEnterAnimCam			16 

// Server bit for an active sell mission
CONST_INT ciSERVER_BS_SELL_MISSION_ACTIVE 0

// Participant count for staggered check of sell mission
INT m_iParticipantSellCheck = 0

YACHT_APPEARANCE yachtAppearanceStored
INT iYachtTintStored = -1

INT iStoredPropertyIndex

DISPLAY_INFO_STRUCT yachtDisplayInfoStruct
BOOL bDoNotBuzzOthers	
BOOL bKeepInvalidText	
BOOL bPlayerWalkingOut
BOOL bSetEntryData	
BOOL bDisabledRadar		
//BOOL bLockCarOnMenu		
BOOL bDisabledStore		
BOOL bRemoveStickyBombs	
BOOL bShapeTestRunning	
BOOL bLeavingEntranceArea
BOOL bCaptainSaidBye
ENUM YACHT_LEAN_ACT_STATE
LEAN_INIT = 0,
LEAN_LOCATECHECK,
LEAN_PROMPT,
LEAN_REQUEST,
LEAN_RUN_ENTER_ANIM,
LEAN_CHECK_ENTER_OR_IDLE_ANIM,
LEAN_CHECK_BASE_ANIM,
LEAN_APPROACH,
LEAN_CLEANUP
ENDENUM

SWIMWEAR_DATA SwimwearData

ENUM JACUZZI_ACT_STATE
JAC_INIT = 0,
JAC_LOCATECHECK,
JAC_PROMPT,
JAC_REQUEST,
JAC_RUN_ENTER_ANIM,
JAC_CHECK_ENTER_OR_IDLE_ANIM,
JAC_CHECK_BASE_ANIM,
JAC_RUNANIM,
JAC_APPROACH,
JAC_CLEANUP,
JAC_PUT_ON_CLOTHES
ENDENUM

ENUM JACUZZI_ACT_SLOTS
JAC_SEAT_SLOT_1 = 0,
JAC_SEAT_SLOT_2,
JAC_SEAT_SLOT_3,
JAC_SEAT_SLOT_4,
JAC_SEAT_SLOT_5,
JAC_SEAT_SLOT_6,
JAC_LEAN_SLOT_7,
JAC_LEAN_SLOT_8
ENDENUM

ENUM YACHT_LEAN_ACT_SLOTS
YACHT_LEAN_SLOT_1 = 0
//YACHT_LEAN_SLOT_2,
//YACHT_LEAN_SLOT_3,
//YACHT_LEAN_SLOT_4,
//YACHT_LEAN_SLOT_5,
//YACHT_LEAN_SLOT_6,
//JAC_LEAN_SLOT_7,
//JAC_LEAN_SLOT_8
ENDENUM

MP_RADIO_LOCAL_DATA_STRUCT MPRadioLocal

CONST_INT YACHT_MAX_NUM_LEAN_SLOTS        30 // the number of the leaning slots for players 

CONST_INT JAC_MAX_NUM_SLOTS        8 // the number of slots the jacuzzi has for players 
CONST_INT JAC_MAX_NUM_SEAT_SLOTS   6 // the number of slots that are specifically for seated players, as opposed to standing players

CONST_FLOAT JAC_RADIUS 2.6

FLOAT fJacuzziBaseSeatRadius = 1.690
FLOAT fJacuzziBaseSeatHeight = -5.812
FLOAT fJacuzziRelativeWaterLevel = -4.840
FLOAT fJacuzziApproachOffset = 0.14
FLOAT fJacuzziActualWaterLevel // This one is calculated from the relative position
FLOAT fJacuzziCameraMinPitch_LOCATECHECK	=-70.0027
FLOAT fJacuzziCameraMaxPitch_LOCATECHECK	=10.7840
FLOAT fJacuzziCameraMinPitch_CLEANUP		=-70.0027
FLOAT fJacuzziCameraMaxPitch_CLEANUP		=2.1277
FLOAT fJacuzziCameraMinPitch_REQUEST		=-70.0027
FLOAT fJacuzziCameraMaxPitch_REQUEST		=7.9032
//VECTOR vJacuzziBound0
//VECTOR vJacuzziBound1

YACHT_LEAN_ACT_STATE yachtLeanActState = LEAN_LOCATECHECK

JACUZZI_ACT_STATE jacActState = JAC_INIT

INT iJacuzziNetSceneid
INT iYachtLeanNetSceneid
FLOAT fBaseAnimPhaseDuration
INT iLastJacuzziIdleClipVar = -1 
INT iSecondLastJacuzziIdleClipVar = -1
INT iLastJacuzziBaseClipVar = -1 
INT iSecondLastJacuzziBaseClipVar = -1

INT iLastYachtLeanIdleClipVar = -1 
INT iSecondLastYachtLeanIdleClipVar = -1
VECTOR vJacuzziCentre

BOOL bUserContentIsBlocked = FALSE

FUNC STRING jacuzziActStateString(JACUZZI_ACT_STATE eJacuzziActState)

	SWITCH eJacuzziActState
		CASE JAC_INIT								RETURN "JAC_INIT"
		CASE JAC_LOCATECHECK						RETURN "JAC_LOCATECHECK"
		CASE JAC_PROMPT								RETURN "JAC_PROMPT"
		CASE JAC_REQUEST							RETURN "JAC_REQUEST"
		CASE JAC_RUN_ENTER_ANIM                     RETURN "JAC_RUN_ENTER_ANIM"
		CASE JAC_CHECK_ENTER_OR_IDLE_ANIM           RETURN "JAC_CHECK_ENTER_OR_IDLE_ANIM"
		CASE JAC_CHECK_BASE_ANIM                    RETURN "JAC_CHECK_BASE_ANIM"
		CASE JAC_RUNANIM                         	RETURN "JAC_RUNANIM"
		CASE JAC_APPROACH                         	RETURN "JAC_APPROACH"
		CASE JAC_CLEANUP                         	RETURN "JAC_CLEANUP"
		CASE JAC_PUT_ON_CLOTHES						RETURN "JAC_PUT_ON_CLOTHES"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING leanActStateString(YACHT_LEAN_ACT_STATE eLeanActState)

	SWITCH eLeanActState
		CASE LEAN_INIT								RETURN "LEAN_INIT"
		CASE LEAN_LOCATECHECK						RETURN "LEAN_LOCATECHECK"
		CASE LEAN_PROMPT							RETURN "LEAN_PROMPT"
		CASE LEAN_REQUEST							RETURN "LEAN_REQUEST"
		CASE LEAN_RUN_ENTER_ANIM                    RETURN "LEAN_RUN_ENTER_ANIM"
		CASE LEAN_CHECK_ENTER_OR_IDLE_ANIM          RETURN "LEAN_CHECK_ENTER_OR_IDLE_ANIM"
		CASE LEAN_CHECK_BASE_ANIM                   RETURN "LEAN_CHECK_BASE_ANIM"
		CASE LEAN_APPROACH                         	RETURN "LEAN_APPROACH"
		CASE LEAN_CLEANUP                         	RETURN "LEAN_CLEANUP"
	ENDSWITCH

	
	RETURN ""
ENDFUNC


PROC SetJacuzziStage(JACUZZI_ACT_STATE &jacuzziStage, JACUZZI_ACT_STATE jacuzziStageNext)
	
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_SAFEHOUSE, "SetJacuzziStage  - from ", jacuzziActStateString(jacuzziStage), " to ", jacuzziActStateString(jacuzziStageNext))
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	jacuzziStage = jacuzziStageNext
ENDPROC

PROC SetLeanStage(YACHT_LEAN_ACT_STATE &leanStage, YACHT_LEAN_ACT_STATE leanStageNext)
	
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_SAFEHOUSE, "SetLeanStage  - from ", leanActStateString(leanStage), " to ", leanActStateString(leanStageNext))
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	leanStage = leanStageNext
ENDPROC

FLOAT fJacuzziInnerSphereSize = 3.5
FLOAT fJacuzziOutterSphereSize = 4.5

#IF IS_DEBUG_BUILD
INT iDebugJacuzziSittingAnimVariation = 0
INT iDebugJacuzziSittingAnimClip = 0
BOOL db_bDrawDebugJacuzziSphereOutter = FALSE
BOOL db_bDrawDebugJacuzziSphereInner = FALSE
BOOL db_bDrawDebugJacuzziSeatPositions = FALSE
BOOL db_bJacuzziForceCupSlots = FALSE
BOOL db_bJacuzziForceCigarSlots = FALSE
FLOAT fDebugJacuzziOutterSphereSize = fJacuzziOutterSphereSize
FLOAT fDebugJacuzziInnerSphereSize = fJacuzziInnerSphereSize 
#ENDIF

BOOL bForceDrinkSmokeActivity = TRUE // set this back to false after flow_play
INT iForceDrinkSmokeTimeout = 25000
SCRIPT_TIMER ntSmokeDrinkForceTimer
CONST_INT MAX_BASE_CLIP_SEQUENCE		3
INT iBaseClipSequence[MAX_BASE_CLIP_SEQUENCE]
INT iBaseClipSeqCount = 0
INT iBaseClipPlayedCount = 0
STRING sJacuzziAnimClip
INT iLeanBaseClipPlayedCount = 0 
INT iLeanNumBaseToPlay = 0
BOOL bJacuzziPropReserved = FALSE
INT iNumberOfJacuzziProps = 3
//INT iJacuzziPromptContextIntention = 0
INT iStaggerJacuzziSeatLoop = 0
INT iPlayerChosenSeatSlot
INT iStaggerLeanActLoop = 0

/// Stuff relating to yacht effects
CONST_INT YACHT_EFFECTS_BS_EXTERIOR_EFFECTS_STARTED		0
CONST_INT YACHT_EFFECTS_BS_WADE_STARTED					1
CONST_INT YACHT_EFFECTS_BS_DRIPS_STARTED				2
CONST_INT YACHT_EFFECTS_BS_DRIPS_NEEDED					3
CONST_INT YACHT_EFFECTS_BS_PLAYER_SUBMERGED				4
CONST_INT YACHT_EFFECTS_BS_SPLASH_STARTED				5
CONST_INT YACHT_EFFECTS_BS_SPLASH_NEEDED				6
CONST_INT YACHT_EFFECTS_BS_SOUND_SPLASH_NEEDED			7
CONST_INT YACHT_EFFECTS_BS_WATER_EXIT_SOUND_STARTED		8
CONST_INT YACHT_EFFECTS_BS_GOT_SOUND_IDS				9

STRUCT MP_YACHT_EFFECTS
	INT iBS = 0
	INT iJacuzziSoundID = -1
	INT iEngineSoundID = -1
	INT iLowerDeckSoundID = -1
	INT iHigherDeckSoundID = -1
	PTFX_ID ptfxJacuzziSteam
	PTFX_ID ptfxJacuzziWade
	PTFX_ID ptfxJacuzziSplash
	PTFX_ID ptfxJacuzziDripsLHand
	PTFX_ID ptfxJacuzziDripsRHand
	#IF IS_DEBUG_BUILD
		VECTOR vJacuzziSteamOffset
	#ENDIF
	TIME_DATATYPE tdJacuzziDripsTimer
	INT iJacuzziSplashType = -1
	INT iSplashSoundID = -1
	TIME_DATATYPE tdSplashTimer
	INT iWaterExitSoundID = -1
	VECTOR vPlayerPositionLastFrame
ENDSTRUCT
MP_YACHT_EFFECTS mpYachtEffects
// end of stuff relating to yacht effects

STRING sJaccuzziAnimDict
STRING sYachtLeanAnimDict
ACTIVITY_ENUMS eChosenJacuzziSlotActivity
ACTIVITY_ENUMS eChosenYachtLeanSlotActivity
INT iJacuzziBitSet
CONST_INT JAC_BS_EXIT_HELP_TEXT_DISPLAYED   					1
CONST_INT JAC_BS_SERVER_DATA_GENERATED		   					2
#IF IS_DEBUG_BUILD
CONST_INT JAC_BS_SERVER_DATA_REINIT								4
CONST_INT JAC_BS_SERVER_DATA_REINITED							5
#ENDIF
CONST_INT JAC_BS_PLAYER_INSIDE_OUTTER_AND_OUTSIDE_INNER_SPHERE	6
CONST_INT JAC_BS_PLAYER_INSIDE_INNER_JAC_SPHERE					7
CONST_INT JAC_BS_PLAYER_OUTSIDE_OUTTER_JAC_SPHERE				8
CONST_INT JAC_BS_SWIMWEAR_REQUIRED								9
CONST_INT JAC_BS_PREVENT_FIRST_PERSON_MODE						10
CONST_INT JAC_BS_POSITIONS_CALCULATED							11

INT iLeanBitSet
CONST_INT LEAN_BS_EXIT_HELP_TEXT_DISPLAYED   					1

STRUCT MP_JACUZZI_LOCAL_DATA_STRUCT
	MP_PROP_OFFSET_STRUCT seatAnimPositions[JAC_MAX_NUM_SLOTS]
	MP_PROP_OFFSET_STRUCT seatLocates0[JAC_MAX_NUM_SLOTS]
	MP_PROP_OFFSET_STRUCT seatLocates1[JAC_MAX_NUM_SLOTS]
	VECTOR seatApproachPositions[JAC_MAX_NUM_SLOTS]
	FLOAT fSeatHeadingOffsets[JAC_MAX_NUM_SEAT_SLOTS] // By how much to offset a heading on the seat from calculated one
	FLOAT fSeatAngleOffsets[JAC_MAX_NUM_SEAT_SLOTS] // How much to offset angle that particular seat position is calculated from
	FLOAT fSeatRadiusOffsets[JAC_MAX_NUM_SEAT_SLOTS] // How much to offset radius that particular seat position is calculated from
ENDSTRUCT

STRUCT MP_YACHT_LEAN_LOCAL_DATA_STRUCT
	VECTOR leanAnimPositions[YACHT_MAX_NUM_LEAN_SLOTS]
	FLOAT leanAnimHeadings[YACHT_MAX_NUM_LEAN_SLOTS]
	VECTOR leanLocates0[YACHT_MAX_NUM_LEAN_SLOTS]
	VECTOR leanLocates1[YACHT_MAX_NUM_LEAN_SLOTS]
ENDSTRUCT

MP_JACUZZI_LOCAL_DATA_STRUCT mpJacuzziLocal
MP_YACHT_LEAN_LOCAL_DATA_STRUCT mpYachtLeanLocal
INT iBowIndex
MP_PROP_LOCAL_CONTROL serverLocalActControl

ENUM ENTRY_CUTSCENE_TYPE
	ENTRY_CUTSCENE_TYPE_BUZZER,
	ENTRY_CUTSCENE_TYPE_FRONT_DOOR
ENDENUM

ENUM SYNC_SCENE_ELEMENTS
	SYNC_SCENE_PED_A,
	SYNC_SCENE_DOOR,
	SYNC_SCENE_CAM,
	SYNC_SCENE_MAX_ELEMENTS
ENDENUM

STRUCT ENTRY_CUTSCENE_DATA
	PED_INDEX playerClone
	
	//OBJECT_INDEX objIDs[2]
	CAMERA_INDEX cameras[2]
	SCRIPT_TIMER timer
	INT iStage
	INT iSyncedSceneID = -1
	INT iBS
	
	STRING animDictionary
	STRING animName[SYNC_SCENE_MAX_ELEMENTS]
	
	VECTOR vCamLoc[2]
	VECTOR vCamRot[2]
	FLOAT fCamFOV[2]
	
	VECTOR vSyncSceneLoc
	VECTOR vSyncSceneRot
	
	FLOAT fSyncedSceneCompleteStage
	
	INT iSoundID = -1
ENDSTRUCT
ENTRY_CUTSCENE_DATA entryCutData

BOOL bPlayerDoorShutSound
OBJECT_INDEX yachtDoor

GAMER_HANDLE heistLeader

MP_PROP_OFFSET_STRUCT tempPropOffset
MP_CAM_OFFSET_STRUCT tempCamOffset

//FLOAT fGroundHeightWalk[MAX_ENTRANCES]

CONST_INT WIS_INIT						0
CONST_INT WIS_WAIT_FOR_DOOR_CONTROL		1
CONST_INT WIS_GIVE_SEQUENCE				2
CONST_INT WIS_WAIT_FOR_SEQUENCE_END		3
CONST_INT WIS_WAIT_FOR_LOAD				4
CONST_INT WIS_CLEANUP					5
INT iWalkInStage = WIS_INIT

CONST_INT WALK_IN_TIMEOUT	10000
SCRIPT_TIMER iWalkInTimer

SCRIPT_TIMER iCameraTimer

SCRIPT_TIMER pauseMenuDelay

SCRIPT_TIMER menuInputDelay
INT iMenuInputDelay

INT iCurrentMenuType
INT iLocalStage
INT iLocalBS	

INT iLocalBS2

VECTOR vYachtLoc

INT iCurrentYacht
INT iPackageDropPropID = -1

INT iEntrancePlayerIsUsing

INT iKnowOwnerState

INT iRejectedEntryBS
SCRIPT_TIMER buzzerTimer

SCRIPT_TIMER cinematicDelay

CAMERA_INDEX propertyCam0, propertyCam1

INT iWarpingOutOfPropertyID


SCRIPT_TIMER failSafeEntryTimer
//SCRIPT_TIMER walkToBuzzerTimer

INT iBuzzerContextIntention = NEW_CONTEXT_INTENTION
INT iPurchaseContextIntention = NEW_CONTEXT_INTENTION

//OWNED_PROPERTIES_IN_BUILDING ownedDetails

INT iPlayersInYacht
INT iPlayersPropertyNum[NUM_NETWORK_PLAYERS]
INT iSlowLoopCounter

MP_REALITY_WARP_IN_STRUCT warpInControl


CONST_INT MAX_EXTERIOR_DOORS	3

//CONST_INT MAX_MAP_HOLE_PROTECTORS	3
//OBJECT_INDEX mapHoleProtectors[MAX_MAP_HOLE_PROTECTORS]

OBJECT_INDEX createdBuzzers[MAX_ENTRANCES]

//MP_DOOR_DETAILS propertyDoors[MAX_EXTERIOR_DOORS]

MP_PROP_BLOCKING_OBJECTS_STRUCT blockObjectDetails

TIME_DATATYPE timeScrollDelay

#IF IS_DEBUG_BUILD
INT iCantEnterFramePrintDelay
BOOL db_bFullDebug
BOOL bDebugTestBuzzer
BOOL bDebugSetSwimwear
BOOL bDebugSetStoredClothing

BOOL db_bBlockPlayerBeast
BOOL db_bBlockPlayerGen
BOOL db_bClearBlockPlayer
FLOAT fDebugTestShapeTestHeightOffset = -0.25
FLOAT fDebugTestShapeTestRadius = 0.5
BOOL bDebugTestShapeTestDraw
BOOL db_bClearFlames
#ENDIF

CONST_FLOAT fBuzzerPressAnimLength 2.433333	//GET_ENTITY_ANIM_TOTAL_TIME(PLAYER_PED_ID(), "mp_doorbell", "ring_bell_a")
INT iBuzzerPressSyncedSceneTimer

//INT iBuzzerPressSyncedScene

INT iBuzzerWalkAwayShapeTest	//0 = 180, 1 = 135, 2 = 225, 3 = 90, 4 = 270

BLIP_INDEX forSaleBlip

INT iBuzzerID = -1

CONST_INT SERVER_STAGE_INIT			0
CONST_INT SERVER_STAGE_RUNNING		1

OBJECT_INDEX garageBlockingObj
OBJECT_INDEX entryBlockingObj

CONST_INT PLAYER_BD_BS_ABLE_TO_ENTER_PROPERTY	0

//SCRIPT_TIMER failSafeWarp

INT iBoolsBitSet
CONST_INT iBS_LAUNCH_STORE				0
CONST_INT iBS_STORE_ACTIVE				1

INT iCalmID =-1



CONST_INT YACHT_MOVE_TRANSACTION_INVALID 	0
CONST_INT YACHT_MOVE_TRANSACTION_PENDING 	1
CONST_INT YACHT_MOVE_TRANSACTION_SUCCESS 	2
CONST_INT YACHT_MOVE_TRANSACTION_FAILED 	3

CONST_FLOAT YACHT_MAP_ZOOM_SPEED 50.0
CONST_FLOAT YACHT_MAP_ZOOM_DEFAULT 2400.0
CONST_FLOAT YACHT_MAP_ZOOM_MAX 0.0
CONST_FLOAT YACHT_MAP_ZOOM_MIN -2000.0

CONST_INT YACHT_MOVE_BITSET_FLAG_MENU_BUILT 	0
CONST_INT YACHT_MOVE_BITSET_FLAG_START_WARP		1
CONST_INT YACHT_MOVE_BITSET_FLAG_BLOCK_MISSIONS	2
CONST_INT YACHT_MOVE_BITSET_FLAG_HIDE_BLIPS		3
CONST_INT YACHT_MOVE_BITSET_FLAG_CONF_MOVE 		4


INT iBlipHideStatValue
INT iYachtMoveControl = 0
INT iYachtMoveContext = NEW_CONTEXT_INTENTION
INT iYachtMoveBitset
INT iYachtMoveCurrentItem
BLIP_INDEX blipYachtMenu
FLOAT fYachtMapZoom
TIME_DATATYPE tdYachtMoveTimer
SCRIPT_TIMER stYachtCaptainIdleSpeech
SCRIPT_TIMER stYachtCaptainFirghtenedSpeech
SCRIPT_TIMER stYachtCaptainFirghtenedPreventSpeech
BOOL bHasBumpedCaptainSpeech
BOOL bHasFirghtenedCaptainSpeech
BOOL bDelaySpeechAfterCaptainShocked
BOOL bCaptainYachtMissionSpeechTriggered
BOOL bCaptainYachtMissionMapSpeechTriggered

PED_INDEX pedID_Captain
INT iYachtCaptainControl = 0
TIME_DATATYPE tdCaptainTimer

BOOL bFreezeCapt

INT iYachtMoveTransactionState = YACHT_MOVE_TRANSACTION_INVALID
INT iYachtMoveTransactionID


CONST_INT YACHT_BRIDGE_FLAG_TRIGGER_LOCATE	0
CONST_INT YACHT_BRIDGE_FLAG_ENTRY_1			1
CONST_INT YACHT_BRIDGE_FLAG_ENTRY_2			2
CONST_INT YACHT_BRIDGE_FLAG_EXIT_1			3
CONST_INT YACHT_BRIDGE_FLAG_EXIT_2			4
CONST_INT YACHT_BRIDGE_FLAG_BAIL			5

CAMERA_INDEX camID_BridgeEntry
TIME_DATATYPE td_BridgeEntry
PED_INDEX pedID_BridgeClone
INT iYachtBridgeEntrySceneID
INTERIOR_INSTANCE_INDEX interiorID_BridgeRoom
OBJECT_INDEX objID_BridgeDoor[2]
INT iYachtBridgeRoomEntry
INT iYachtBridgeRoomFlags

BOOL bYachtWhiteText
BOOL bYachtOwnerNameRequested
SCALEFORM_INDEX sfID_YachtOwnerName
INT iPortRenderID = -1
INT iStarbRenderID = -1
INT iSternRenderID = -1
INT iYachtNameControl = 0
OBJECT_INDEX oYachtNamePort
OBJECT_INDEX oYachtNameStarb
OBJECT_INDEX oYachtNameStern
SCALEFORM_INDEX sYachtNameOverlay
SCALEFORM_INDEX sYachtNameSternOverlay
#IF IS_DEBUG_BUILD
STRING strYachtName
#ENDIF
TEXT_LABEL_15 tlFlagCountry
TEXT_LABEL_31 sTempName
TEXT_LABEL_31 sTempNameHash
INT iTintID
INT iTintIDHash
INT iFlagID
INT iFlagIDHash
INT iHashYachtName

#IF IS_DEBUG_BUILD
TEXT_WIDGET_ID wYachtName
BOOL bCustomYachtName
#ENDIF

FLOAT fPortCentreX = 0.395
FLOAT fPortCentreY = 0.350
FLOAT fPortWidth = 0.500
FLOAT fPortHeight = 1.000

FLOAT fStarbCentreX = 0.315
FLOAT fStarbCentreY = 0.350
FLOAT fStarbWidth = 0.500
FLOAT fStarbHeight = 1.000

FLOAT fSternCentreX = 0.400
FLOAT fSternCentreY = 0.350
FLOAT fSternWidth = 0.700
FLOAT fSternHeight = 0.750

STRUCT JACUZZI_SEAT_DATA
	INT iJacuzziAnimVariation
	BOOL bCigar
	BOOL bCup
	NETWORK_INDEX niCigar
	NETWORK_INDEX niAshtray
	NETWORK_INDEX niCup
	VECTOR vCigarPosition
	VECTOR vCigarRotation
	
	VECTOR vCupPosition
	VECTOR vCupRotation
ENDSTRUCT

BOOL bDelayWarpAfterConfirm = FALSE
SCRIPT_TIMER stDelayedWarp

//Fire checks
INT iFireSlowLoop
SCRIPT_TIMER fireCheckTimer
SCRIPT_TIMER serverFireKillTime
BOOL bClientFire

INT iNavMeshBlocks[NUMBER_OF_PRIVATE_YACHTS]
BOOL bNavBlockingForMission

//BOOL bProcessingBasket = FALSE
//CONST_INT SHOP_BASKET_STAGE_ADD     0
//CONST_INT SHOP_BASKET_STAGE_PENDING 1
//CONST_INT SHOP_BASKET_STAGE_SUCCESS 2
//CONST_INT SHOP_BASKET_STAGE_FAILED  3
//INT iProcessingBasketStage = SHOP_BASKET_STAGE_ADD
//
//STRUCT PROPERTY_SHOPPING_BASKET_STRUCT
//	INT m_iProcessSuccess = -1
//
//	INT m_iItemId = -1
//	INT m_iInventoryKey = -1
//
//	INT m_iSellingPrice = 0
//	INT m_iSellingItemId = 0
//ENDSTRUCT
//PROPERTY_SHOPPING_BASKET_STRUCT sPropertyShoppingBasket

/// BROADCAST DATA
STRUCT ServerBroadcastData
//	NETWORK_INDEX	normalDoorIDs[MAX_EXTERIOR_DOORS]
	INT iStage
//	INT iRequestDoorUsageBS[MAX_EXTERIOR_DOORS]
//	INT iPlayersInsideEntranceBS[MAX_EXTERIOR_DOORS]
	MP_PROP_ACT_SERVER_CONTROL_STRUCT activityControl
	JACUZZI_SEAT_DATA jacuzziServerSeatData[JAC_MAX_NUM_SLOTS]
	MP_RADIO_SERVER_DATA_STRUCT MPRadioServer
	BOOL bServerStopFire
	INT iSwimwearOptions[NUM_NETWORK_PLAYERS] // Preselected default swimwear options
	
	// Server bit set, added for sell mission check
	INT iServerBitSet
ENDSTRUCT
ServerBroadcastData serverBD

CONST_INT iBS_PLAYER_IN_JACUZZI_PLAYING_IDLE 0 // If the player is playing idle animation sitting in the jacuzzi
CONST_INT iBS_PLAYER_BD_FIRE_IN_RANGE			1 // player thinks there is fire in range
CONST_INT iBS_PLAYER_BD_EXIT_GROUP_READY	2

STRUCT PlayerBroadcastData
	// iBS has been used in some commented out code with CONSTs that no longer exist.
	// In case that code ever gets uncommented and CONSTs restored I've changed iBS to iBitSet
	// so anyone who intends on using this bitset has to update their CONSTs so they don't clash
	// with the new ones.
	INT iBitSet 
	INT iActivityRequested
	INT iBridgeEntryStage
	INT iJacuzziSeatUsed = -1
	MP_RADIO_CLIENT_DATA_STRUCT MPRadioClient
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]
JACUZZI_SEAT_DATA jacuzziClientSeatData[JAC_MAX_NUM_SLOTS]

PROC START_MENU_INPUT_DELAY(INT iDelayTime)
	REINIT_NET_TIMER(menuInputDelay,TRUE)
	iMenuInputDelay = iDelayTime
ENDPROC

FUNC BOOL CAN_PLAYER_USE_MENU_INPUT()
	IF HAS_NET_TIMER_STARTED(menuInputDelay)
		IF HAS_NET_TIMER_EXPIRED(menuInputDelay,iMenuInputDelay,TRUE)
			RETURN TRUE
		ENDIF
	ENDIF		
	RETURN FALSE
ENDFUNC

PROC SET_PROPERTY_ENTER_STAGE(INT iStage)
	#IF IS_DEBUG_BUILD
	SWITCH iStage
		CASE PROP_ENTER_STAGE_INIT
			CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_PROPERTY_ENTER_STAGE: = PROP_ENTER_STAGE_INIT")
		BREAK
		CASE PROP_ENTER_STAGE_FULL
			CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_PROPERTY_ENTER_STAGE: = PROP_ENTER_STAGE_FULL")
		BREAK
		CASE PROP_ENTER_STAGE_NO_VEHICLE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_PROPERTY_ENTER_STAGE: = PROP_ENTER_STAGE_NO_VEHICLE")
		BREAK
		CASE PROP_ENTER_STAGE_FAKE_CUTSCENE_HEIST
			CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_PROPERTY_ENTER_STAGE: = PROP_ENTER_STAGE_FAKE_CUTSCENE_HEIST")
		BREAK
	ENDSWITCH
	#ENDIF
	IF iStage = PROP_ENTER_STAGE_FULL
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CAN_BE_TARGETTED | NSPC_ALLOW_PLAYER_DAMAGE)
		START_MENU_INPUT_DELAY(500)
	ENDIF
	warpInControl.iStage = iStage
ENDPROC

PROC INIT_CURRENT_YACHT_DETAILS()
	//INT i
	iCurrentYacht = mpPropMaintain.iMostRecentYachtID
	vYachtLoc = mpYachts[iCurrentYacht].vBaseLocation
	CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: iCurrentYacht = ", iCurrentYacht," vLoc = ",vYachtLoc)
	IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()),mpYachts[iCurrentYacht].vBaseLocation,TRUE) > 400
	AND (NOT IS_PLAYER_SPECTATING(PLAYER_ID()) 
	OR (IS_PLAYER_SPECTATING(PLAYER_ID()) AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(GET_SPECTATOR_SELECTED_PED(),FALSE),vYachtLoc,FALSE) > 400))
		#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: Launched but not at yachts's locations???? See Conor")
		SCRIPT_ASSERT("AM_MP_YACHT: Launched but not at yachts's locations???? SSee Conor")
		#ENDIF
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: Killing script")
		CLEAR_BIT(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_LAUNCHED_YACHT_SCRIPT)
		TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
	ENDIF
	//VECTOR vTemp = mpYachts[iCurrentYacht].yachtPropertyDetails.house.vMidPoint
	//INT i
	//REPEAT MAX_CALMING_QUADS_PER_YACHT i
	//ENDREPEAT
//	iCalmID = ADD_EXTRA_CALMING_QUAD(vTemp.x-200,vTemp.y-200,vTemp.x+200,vTemp.y+200,0.02)
//	PRINTLN("AM_MP_YACHT: ADD_EXTRA_CALMING_QUAD at xMin:",vTemp.x-200," xMax:",vTemp.x+200," yMin:",vTemp.y-200," yMax:",vTemp.y+200, "ID #",iCalmID)
//	iCalmID[1] = ADD_EXTRA_CALMING_QUAD(vTemp.x-150,vTemp.y-150,vTemp.x+150,vTemp.y+150,0.02)
//	PRINTLN("AM_MP_YACHT: ADD_EXTRA_CALMING_QUAD at xMin:",vTemp.x-150," xMax:",vTemp.x+150," yMin:",vTemp.y-150," yMax:",vTemp.y+150, "ID #",iCalmID[1])
	
ENDPROC

//PROC REGISTER_PROPERTY_DOORS()
//	INT i
//	MP_PROP_OFFSET_STRUCT doorLocationDetails
//	BOOL bDoorGarage
//	REPEAT MAX_EXTERIOR_DOORS i
//		bDoorGarage = FALSE
//		propertyDoors[i].vCoords = <<0,0,0>>
//		propertyDoors[i].iDoorHash = 0
//		IF GET_BUILDING_DOOR_DETAILS(doorLocationDetails,propertyDoors[i],bDoorGarage,mpYachts[iCurrentYacht].iBuildingID,i)
//			SETUP_MP_DOOR(propertyDoors[i])
//		ENDIF
//	ENDREPEAT
//	
//	IF mpYachts[iCurrentYacht].iBuildingID = MP_PROPERTY_BUILDING_38
//		CREATE_MODEL_HIDE(<<2494.2651,1587.5674,31.7398>> , 0.02, PROP_FNCLINK_02GATE3, TRUE)	//gates
//		CREATE_MODEL_HIDE(<<2484.0889,1566.9988,31.7453>> , 0.02, PROP_FNCLINK_02GATE3, TRUE)	//gates
//	ENDIF
//ENDPROC

// Do necessary pre game start ini.
// Returns FALSE if the script fails to receive an initial network broadcast.
FUNC BOOL PROCESS_PRE_GAME()
	
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
		CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_SCRIPT: mark player to leave ped behind: SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), TRUE) (+ network)")
		SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), FALSE)
		NETWORK_DISABLE_LEAVE_REMOTE_PED_BEHIND(TRUE)
	ENDIF


	INT iInstance	

	iInstance = iCurrentYacht
	
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE,iInstance)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: set network instance/current yacht to be = ",iInstance )
	
	
//	REGISTER_PROPERTY_DOORS()
	// This makes sure the net script is active, waits untull it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
		
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// KGM: Wait for the first network broadcast before moving on - call this as the last instruction in pre-game
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		RETURN FALSE
	ENDIF
	
	// This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	RETURN TRUE
ENDFUNC

PROC SET_LOCAL_STAGE(INT iStage)
	#IF IS_DEBUG_BUILD
	SWITCH istage
		CASE STAGE_CHECK_FOR_IN_LOCATE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: yacht #",iCurrentYacht," setting stage to be STAGE_CHECK_FOR_IN_LOCATE")
		BREAK
		CASE STAGE_SETUP_FOR_MENU
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: yacht #",iCurrentYacht ," setting stage to be STAGE_SETUP_FOR_MENU")
		BREAK
		CASE STAGE_USING_MENU
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: yacht #",iCurrentYacht," setting stage to be STAGE_USING_MENU")
		BREAK
		CASE STAGE_SETUP_FOR_USING_PROPERTY
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: yacht #",iCurrentYacht," setting stage to be STAGE_SETUP_FOR_USING_PROPERTY")
		BREAK
		CASE STAGE_ENTER_PROPERTY
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: yacht #",iCurrentYacht," setting stage to be STAGE_ENTER_PROPERTY")
		BREAK
		CASE STAGE_LEAVE_ENTRANCE_AREA
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: yacht #",iCurrentYacht," setting stage to be STAGE_LEAVE_ENTRANCE_AREA")
		BREAK
		CASE STAGE_BUZZED_INTO_PROPERTY
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: yacht #",iCurrentYacht," setting stage to be STAGE_BUZZED_INTO_PROPERTY")
		BREAK
	ENDSWITCH
	#ENDIF
	IF iStage = STAGE_SETUP_FOR_USING_PROPERTY
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)//,NSPC_PREVENT_VISIBILITY_CHANGES)
		IF NOT g_bPropInteriorScriptRunning
			globalPropertyEntryData.bLeftOrRejectedBuzzer = FALSE
		ENDIF
		bRemoveStickyBombs = FALSE
	ENDIF
	IF iStage = STAGE_LEAVE_ENTRANCE_AREA
		bLeavingEntranceArea = TRUE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_LOCAL_STAGE: bLeavingEntranceArea = TRUE")
	ENDIF
	IF iStage = STAGE_USING_MENU
		START_MENU_INPUT_DELAY(500)
	ENDIF
	iLocalStage = iStage
ENDPROC

PROC CLEAR_INVALID_VEHICLE_HELP()
	IF IS_BIT_SET(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MP_PROP_IVD_VEH")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MP_PROP_IVD_VEH1")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MP_PROP_IVD_VEH2")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MP_PROP_IVD_3a")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MP_PROP_IVD_3b")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MP_PROP_IVD_3d")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MP_PROP_IVD_3e")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MP_PROP_IVD_3f")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MP_PROP_IVD_3g")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PPA_GENYACHT0")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PPA_GEN1")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PPA_BEAST0")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PPA_BEAST1")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PPA_CONTRA3")
			CLEAR_HELP()
		ENDIF
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: Clearing invalid vehicle help")
	ENDIF
	CLEAR_BIT(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
ENDPROC

//FUNC BOOL DOES_DECORATOR_PREVENT_ALLOWANCE_IN_GARAGE(VEHICLE_INDEX theVeh)
//	IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh",DECOR_TYPE_INT)
//		IF DECOR_EXIST_ON(theVeh,"Not_Allow_As_Saved_Veh")
//			IF DECOR_GET_INT(theVeh,"Not_Allow_As_Saved_Veh") != 0
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "DOES_DECORATOR_PREVENT_ALLOWANCE_IN_GARAGE = TRUE")
//				RETURN TRUE
//			ENDIF
//		ENDIF
//	ENDIF
//	RETURN FALSE
//ENDFUNC

FUNC BOOL IS_ENTERING_DISABLED_DUE_TO_EXIT_WARP()
	IF iWarpingOutOfPropertyID != 0
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC CLEAR_ENTERING_DISABLED_DUE_TO_EXIT_WARP()
	IF NOT HAS_NET_TIMER_STARTED(tempPropertyExtMenu.exitDoorDelay)
	OR (HAS_NET_TIMER_STARTED(tempPropertyExtMenu.exitDoorDelay) AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tempPropertyExtMenu.exitDoorDelay,2000,TRUE))
		IF iWarpingOutOfPropertyID != 0
			iWarpingOutOfPropertyID = 0
			bPlayerWalkingOut = FALSE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEAR_ENTERING_DISABLED_DUE_TO_EXIT_WARP: Setting iWarpingOutOfPropertyID = 0")
			#IF IS_DEBUG_BUILD
			IF NOT HAS_NET_TIMER_STARTED(tempPropertyExtMenu.exitDoorDelay)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEAR_ENTERING_DISABLED_DUE_TO_EXIT_WARP: timer hasn't started")
			ELSE	
				CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEAR_ENTERING_DISABLED_DUE_TO_EXIT_WARP: timer expired")
			ENDIF
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

//FUNC BOOL IS_NPC_IN_VEHICLE()
//	INT iSeat
//	VEHICLE_INDEX theVeh
//	PED_INDEX pedIndex
//	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//		theVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
//		IF IS_VEHICLE_DRIVEABLE(theVeh)
//			FOR iSeat = ENUM_TO_INT(VS_DRIVER) TO ENUM_TO_INT(VS_EXTRA_RIGHT_3)
//				IF NOT IS_VEHICLE_SEAT_FREE(theVeh, INT_TO_ENUM(VEHICLE_SEAT, iSeat))
//					pedIndex = GET_PED_IN_VEHICLE_SEAT(theVeh,INT_TO_ENUM(VEHICLE_SEAT, iSeat))
//					IF DOES_ENTITY_EXIST(pedIndex)
//						IF NOT IS_PED_A_PLAYER(pedIndex)
//							//IF NOT IS_PED_INJURED(pedIndex)
//							
//								RETURN TRUE
//							//ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDFOR
//		ENDIF
//	ENDIF
//	RETURN FALSE
//ENDFUNC

PROC PRINT_REASON_PLAYER_IS_BLOCKED_FROM_YACHT(BOOL bGarage)
	IF IS_BIT_SET(g_iPreventPropertyAccessBS,ENUM_TO_INT(PPAF_FREEMODE_EVENT_BEAST))
		IF bGarage
			PRINT_HELP_NO_SOUND("PPA_BEAST1")
			SET_FREEMODE_EVENT_HELP_BACKGROUND()
			EXIT
		ENDIF
		PRINT_HELP_NO_SOUND("PPA_BEAST0")
		SET_FREEMODE_EVENT_HELP_BACKGROUND()
		EXIT
	ELIF IS_BIT_SET(g_iPreventPropertyAccessBS,ENUM_TO_INT(PPAF_CONTRABAND))
		PRINT_HELP_NO_SOUND("PPA_CONTRA3")
		EXIT
	ENDIF
	IF bGarage
		PRINT_HELP("PPA_GEN1")
		EXIT
	ENDIF
	PRINT_HELP("PPA_GENYACHT0")
ENDPROC

FUNC BOOL IS_LOCAL_PLAYER_EXITING_A_VEHICLE()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF GET_IS_TASK_ACTIVE(PLAYER_PED_ID(),CODE_TASK_EXIT_VEHICLE)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_ENTERING_OR_EXITING_A_VEHICLE()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF GET_IS_TASK_ACTIVE(PLAYER_PED_ID(),CODE_TASK_EXIT_VEHICLE)
			RETURN TRUE
		ENDIF
		IF IS_PED_IN_ANY_VEHICLE_OR_WAITING_TO_START_TASK_ENTER_VEHICLE(PLAYER_PED_ID())
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC
//
//FUNC BOOL IS_PLAYER_GETTING_IN_OR_OUT_OF_VEHICLE(PLAYER_INDEX thePlayer) //does not include in vehicle
//	VEHICLE_INDEX theVeh
//	PED_INDEX playerPed
//	IF IS_NET_PLAYER_OK(thePlayer)
//		playerPed = GET_PLAYER_PED(thePlayer)
//		IF GET_IS_TASK_ACTIVE(playerPed,CODE_TASK_EXIT_VEHICLE)
//			RETURN TRUE
//		ENDIF
//		IF IS_PED_IN_ANY_VEHICLE(playerPed,TRUE)
//			IF NOT IS_PED_IN_ANY_VEHICLE(playerPed,FALSE)
//				RETURN TRUE
//			ENDIF
//		ELSE
//			theVeh = GET_VEHICLE_PED_IS_ENTERING(playerPed)
//			IF DOES_ENTITY_EXIST(theVeh)
//				RETURN TRUE
//			ENDIF
//		ENDIF
//		IF IS_PED_GETTING_INTO_A_VEHICLE(playerPed)
//			RETURN TRUE
//		ENDIF
//		
//		IF GET_SCRIPT_TASK_STATUS(playerPed,SCRIPT_TASK_ENTER_VEHICLE) = PERFORMING_TASK
//		OR GET_SCRIPT_TASK_STATUS(playerPed,SCRIPT_TASK_ENTER_VEHICLE) = WAITING_TO_START_TASK
//			RETURN TRUE
//		ENDIF
//	ENDIF
//	RETURN FALSE
//ENDFUNC


///// PURPOSE:
/////    To prevent access into garage in special cases where garage locate completely covers buzzer.  
//FUNC BOOL DISABLE_WALK_IN_FOR_BUZZER_LOCATE()
//	SWITCH mpYachts[iCurrentYacht].iBuildingID
//		CASE MP_PROPERTY_BUILDING_7
//			SWITCH iEntrancePlayerIsUsing
//				CASE 1
//					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),<<-634.766479,52.733532,42.303310>>,
//													<<-630.410400,52.705669,48.118065>>,
//													3.500000,
//													FALSE,TRUE,TM_ON_FOOT)
//						RETURN TRUE
//					ENDIF
//				BREAK
//			ENDSWITCH
//		BREAK
//		CASE MP_PROPERTY_BUILDING_2
//			SWITCH iEntrancePlayerIsUsing
//				CASE 1
//					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),<<-281.222504,-991.745361,22.908405>>,
//													<<-276.880646,-993.169128,27.328550>>,
//													3.500000,
//													FALSE,TRUE,TM_ON_FOOT)
//						RETURN TRUE
//					ENDIF
//				BREAK
//			ENDSWITCH
//		BREAK
//		CASE MP_PROPERTY_BUILDING_3
//			SWITCH iEntrancePlayerIsUsing
//				CASE 1
//					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),<<-1454.700928,-498.933228,31.232906>>,
//													<<-1451.583740,-503.054321,34.242401>>,
//													2.250000,
//													FALSE,TRUE,TM_ON_FOOT)
//						RETURN TRUE
//					ENDIF
//				BREAK
//			ENDSWITCH
//		BREAK
//		CASE MP_PROPERTY_BUILDING_4
//			SWITCH iEntrancePlayerIsUsing
//				CASE 1
//					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),<<-819.030945,-439.883911,35.194271>>,
//													<<-823.445496,-442.112579,39.639885>> ,
//													3.000000,
//													FALSE,TRUE,TM_ON_FOOT)
//						RETURN TRUE
//					ENDIF
//				BREAK
//			ENDSWITCH
//		BREAK
//		
//		CASE MP_PROPERTY_BUILDING_6
//			SWITCH iEntrancePlayerIsUsing
//				CASE 1
//					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),<<-876.873169,-358.780823,34.578461>>,
//													<<-874.390808,-363.407959,38.623848>> ,
//													2.750000,
//													FALSE,TRUE,TM_ON_FOOT)
//						RETURN TRUE
//					ENDIF
//				BREAK
//			ENDSWITCH
//		BREAK
//	ENDSWITCH
//	RETURN FALSE
//ENDFUNC

FUNC BOOL IS_PLAYER_ENTERING_THE_PROPERTY()
	IF NOT IS_PLAYER_TELEPORT_ACTIVE()
	AND NOT IS_PLAYER_GETTING_IN_OR_OUT_OF_VEHICLE(PLAYER_ID())
	AND NOT IS_PLAYER_IN_CORONA()
	AND NOT IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
	AND NOT IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_AcceptedInvite)
		
		//CDEBUG1LN(DEBUG_SAFEHOUSE, "Teleport not active not checking vehile entrance")
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[iEntrancePlayerIsUsing].locateDetails.vPos1,
												mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[iEntrancePlayerIsUsing].locateDetails.vPos2,
												mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[iEntrancePlayerIsUsing].locateDetails.fWidth,
												FALSE,TRUE,TM_ON_FOOT)
			
			bKeepInvalidText = TRUE
			SET_BIT(iLocalBS2,LOCAL_BS2_bKeepWantedFlag)
			IF IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
			AND mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[iEntrancePlayerIsUsing].iType != ENTRANCE_TYPE_HOUSE
			OR IS_PLAYER_BLOCKED_FROM_PROPERTY()
				IF NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
							IF IS_PLAYER_BLOCKED_FROM_PROPERTY()
								PRINT_REASON_PLAYER_IS_BLOCKED_FROM_YACHT(FALSE)
							ELSE
								PRINT_HELP("CUST_GAR_MISO")
							ENDIF
							//CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: Printing help MP_PROP_IVD_VEH: player owns DLC content but it is excluded from garage")
							SET_BIT(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				//CDEBUG1LN(DEBUG_SAFEHOUSE, "Player in angled area")
				IF IS_ENTERING_DISABLED_DUE_TO_EXIT_WARP()
					RETURN FALSE
				ENDIF
				//CDEBUG1LN(DEBUG_SAFEHOUSE, "Checking heading")
				IF IS_HEADING_ACCEPTABLE(GET_ENTITY_HEADING(PLAYER_PED_ID()),mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[iEntrancePlayerIsUsing].locateDetails.fEnterHeading,30 #IF IS_DEBUG_BUILD,FALSE #ENDIF)
					IF NOT IS_PLAYER_GETTING_IN_OR_OUT_OF_VEHICLE(PLAYER_ID())
					AND NOT IS_LOCAL_PLAYER_ENTERING_OR_EXITING_A_VEHICLE()
						//CDEBUG1LN(DEBUG_SAFEHOUSE, "Heading OK checking time")
						IF NOT HAS_NET_TIMER_STARTED(tempPropertyExtMenu.exitDoorDelay)
						OR HAS_NET_TIMER_EXPIRED_READ_ONLY(tempPropertyExtMenu.exitDoorDelay,1000,TRUE)
							IF NOT IS_LOCAL_PLAYER_ENTERING_OR_EXITING_A_VEHICLE()
								CLEAR_INVALID_VEHICLE_HELP()
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//FUNC BOOL IS_PLAYER_IN_PURCHASE_LOCATE()
//	IF GET_SKYBLUR_STAGE() = SKYBLUR_NONE
//		DRAW_MARKER(MARKER_CYLINDER, mpYachts[iCurrentYacht].vPurchaseLocation + <<0,0,-0.25>>, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<0.75, 0.75, 0.75>>, 255, 0, 128, 178)
//	ENDIF
//	IF mpYachts[iCurrentYacht].iType = PROPERTY_TYPE_GARAGE
//	AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
//		//DRAW_MARKER(MARKER_CYLINDER, mpYachts[iCurrentYacht].vPurchaseLocation, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<2, 2, 0.75>>, 255, 0, 128, 178)
//		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),mpYachts[iCurrentYacht].vPurchaseLocation,<<3, 3, 2>>,FALSE,TRUE,TM_ANY)
//			RETURN TRUE								
//		ENDIF
//	ELSE
//		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),mpYachts[iCurrentYacht].vPurchaseLocation,<<1, 1, 2>>,FALSE,TRUE,TM_ON_FOOT)
//			RETURN TRUE
//		ENDIF
//	ENDIF
//	RETURN FALSE
//ENDFUNC

FLOAT fGroundHeight[MAX_ENTRANCES]

PROC DRAW_BUZZER_LOCATE(INT iEntrance)
//	#IF FEATURE_HEIST_PLANNING
//	IF (SHOULD_BYPASS_CHECKS_FOR_ENTRY(FALSE)
//	AND mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[iEntrance].iType = ENTRANCE_TYPE_HOUSE)
//		CDEBUG1LN(DEBUG_SAFEHOUSE, "DRAW_BUZZER_LOCATE: bypassing drawn for heist drop off")
//		EXIT
//	ENDIF
//	IF Is_Player_Currently_On_MP_Heist(PLAYER_ID())
//	OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
//		IF SHOULD_BYPASS_CHECKS_FOR_ENTRY()
//		AND mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[iEntrance].iType = ENTRANCE_TYPE_GARAGE
//		AND g_FMMC_STRUCT.iRootContentIDHash = g_sMPTunables.iroot_id_HASH_Tutorial_Scope_Out    
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "DRAW_BUZZER_LOCATE: bypassing drawn for heist garage drop off - fleeca")
//			EXIT
////		ELSE
////			CDEBUG1LN(DEBUG_SAFEHOUSE, "SHOULD_BYPASS_CHECKS_FOR_ENTRY() = ",SHOULD_BYPASS_CHECKS_FOR_ENTRY())
////			CDEBUG1LN(DEBUG_SAFEHOUSE, "mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[",iEntrance,"].iType = ",mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[iEntrance].iType)
////			CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_STRAND_ROOT_CONTENT_ID_HASH() = ",g_FMMC_STRUCT.iRootContentIDHash)
////			CDEBUG1LN(DEBUG_SAFEHOUSE, "g_sMPTunables.iroot_id_HASH_Tutorial_Scope_Out = ",g_sMPTunables.iroot_id_HASH_Tutorial_Scope_Out)
//		ENDIF
//	ENDIF
//	#ENDIF
	IF GET_SKYBLUR_STAGE() = SKYBLUR_NONE
		IF fGroundHeight[iEntrance] = 0.0
			GET_GROUND_Z_FOR_3D_COORD(mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[iEntrance].vBuzzerLoc + <<0.0, 0.0, 0.5>>, fGroundHeight[iEntrance])
		ENDIF
		
		INT iR, iG, iB, iA
		GET_HUD_COLOUR(HUD_COLOUR_BLUE,iR, iG, iB, iA)
		DRAW_MARKER_EX(MARKER_CYLINDER, <<mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[iEntrance].vBuzzerLoc.X, mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[iEntrance].vBuzzerLoc.Y, fGroundHeight[iEntrance]>> + <<0,0,-0.25>>, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<0.75, 0.75, 0.75>>, iR, iG, iB, 255,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,FALSE)
		//CDEBUG1LN(DEBUG_SAFEHOUSE, "Drawing marker at: ", mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[iEntrance].vBuzzerLoc)
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_IN_BUZZER_LOCATION(INT iEntrance)//, INT iKnowState)
	//BOOl bDisplayBuzzerCorona
	//CDEBUG1LN(DEBUG_SAFEHOUSE, "iKnowState = ", iKnowState)
	//CDEBUG1LN(DEBUG_SAFEHOUSE, "mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[iEntrance].iType = ", mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[iEntrance].iType)
//	IF IS_BIT_SET(iKnowState,1)
//	AND mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[iEntrance].iType = ENTRANCE_TYPE_HOUSE
//		bDisplayBuzzerCorona = TRUE
//	ENDIF
//	IF IS_BIT_SET(iKnowState,2)
//	AND mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[iEntrance].iType = ENTRANCE_TYPE_GARAGE
//		bDisplayBuzzerCorona = TRUE
//	ENDIF
	//CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_PLAYER_IN_BUZZER_LOCATION: Checking player is in buzzer for entrance #", iEntrance, " (FC= ",GET_FRAME_COUNT())
	//IF bDisplayBuzzerCorona
	VECTOR vLocateSize = <<1, 1, 2>>
	
	IF NOT IS_PLAYER_TELEPORT_ACTIVE()
	AND NOT IS_PLAYER_IN_CORONA()
	AND NOT IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
	AND NOT IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_AcceptedInvite)
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[iEntrance].vBuzzerLoc,vLocateSize,FALSE,TRUE,TM_ON_FOOT)
			SET_BIT(iLocalBS2,LOCAL_BS2_bKeepWantedFlag)
			bKeepInvalidText = TRUE
			//CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_BIT(iLocalBS2,LOCAL_BS2_bKeepWantedFlag) buzzer locate(FC= ",GET_FRAME_COUNT())
			IF IS_LOCAL_PLAYER_ENTERING_OR_EXITING_A_VEHICLE()
				#IF IS_DEBUG_BUILD
				IF db_bFullDebug
					CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_PLAYER_IN_BUZZER_LOCATION- IS_LOCAL_PLAYER_ENTERING_OR_EXITING_A_VEHICLE")
				ENDIF
				#ENDIF
				RETURN FALSE
			ENDIF
			IF IS_PLAYER_GETTING_IN_OR_OUT_OF_VEHICLE(PLAYER_ID())
				#IF IS_DEBUG_BUILD
				IF db_bFullDebug
					CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_PLAYER_IN_BUZZER_LOCATION- IS_LOCAL_PLAYER_ENTERING_OR_EXITING_A_VEHICLE")
				ENDIF
				#ENDIF
				RETURN FALSE
			ENDIF
			IF IS_PED_RAGDOLL(PLAYER_PED_ID())
				#IF IS_DEBUG_BUILD
				IF db_bFullDebug
					CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_PLAYER_IN_BUZZER_LOCATION- IS_PED_RAGDOLL")
				ENDIF
				#ENDIF
				RETURN FALSE
			ENDIF
			//IF IS_ENTERING_DISABLED_DUE_TO_EXIT_WARP()
			//	RETURN FALSE
			//ENDIF
			IF NOT HAS_NET_TIMER_STARTED(tempPropertyExtMenu.exitDoorDelay)
			OR HAS_NET_TIMER_EXPIRED_READ_ONLY(tempPropertyExtMenu.exitDoorDelay,2000,TRUE)
				#IF IS_DEBUG_BUILD
				IF db_bFullDebug
					CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_PLAYER_IN_BUZZER_LOCATION- TRUE")
				ENDIF
				#ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		IF db_bFullDebug
			IF IS_PLAYER_TELEPORT_ACTIVE()
				CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_PLAYER_IN_BUZZER_LOCATION- IS_PLAYER_TELEPORT_ACTIVE")
			ENDIF
			IF IS_PLAYER_IN_CORONA()
				CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_PLAYER_IN_BUZZER_LOCATION- IS_PLAYER_IN_CORONA")
			ENDIF
			IF IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
				CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_PLAYER_IN_BUZZER_LOCATION- IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE")	
			ENDIF
			IF IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_AcceptedInvite)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_PLAYER_IN_BUZZER_LOCATION- biITA_AcceptedInvite")
			ENDIF
		ENDIF
		#ENDIF
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[iEntrance].vBuzzerLoc,vLocateSize,FALSE,TRUE,TM_ON_FOOT)
			SET_BIT(iLocalBS2,LOCAL_BS2_bKeepWantedFlag)
			bKeepInvalidText = TRUE
			//CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_BIT(iLocalBS2,LOCAL_BS2_bKeepWantedFlag) buzzer locate (TELEPORT ACTIVE)(FC= ",GET_FRAME_COUNT())
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC ADD_MENU_OPTION_FOR_PLAYER(INT& iMenuCounter, PLAYER_INDEX playerID, BOOL bRangBuzzer)
//	IF NOT bAuthMenu
		//IF mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
			ADD_MENU_ITEM_TEXT(iMenuCounter, "MP_PROP_MENU1", 1,TRUE)
		//ELSE
		//	ADD_MENU_ITEM_TEXT(iMenuCounter, "MP_PROP_MENU1b", 1,TRUE)
		//ENDIF
		//ADD_MENU_ITEM_TEXT_COMPONENT_PLAYER_NAME(GET_PLAYER_NAME(playerID))
		ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL(GET_PLAYER_NAME(playerID))
		IF bRangBuzzer
			ADD_MENU_ITEM_TEXT(iMenuCounter, "", 1,TRUE)
			ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_TICK)
		ENDIF
//	ELSE
//		ADD_MENU_ITEM_TEXT(iMenuCounter, "STRING", 1,TRUE)
//		ADD_MENU_ITEM_TEXT_COMPONENT_PLAYER_NAME(GET_PLAYER_NAME(playerID))
//		IF bRangBuzzer
//			ADD_MENU_ITEM_TEXT(iMenuCounter, "", 1,TRUE)
//			ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_TICK)
//		ENDIF
//	ENDIF
	iMenuCounter++
ENDPROC

PROC SETUP_PROPERTY_OPTIONS_MENU(INT iMenuType)
	INT iMenuCounter = 0
	
	INT i
	PLAYER_INDEX playerID
	
	
	BOOL bHasThisYacht
	tempPropertyExtMenu.iAllNearby = -1
	tempPropertyExtMenu.iAllFriendsCrew = -1
	tempPropertyExtMenu.iOrganisationNearby = -1
	tempPropertyExtMenu.iMaxVertSel = 0
	
	CLEAR_MENU_DATA()
	REPEAT NUM_NETWORK_PLAYERS+4 i
		tempPropertyExtMenu.menuPlayerArray[i]= INVALID_PLAYER_INDEX()
		tempPropertyExtMenu.iPropertyID[i] = 0
	ENDREPEAT
	
	i = 0

	SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT,MENU_ITEM_TEXT)
	SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
	SET_MENU_ITEM_TOGGLEABLE(FALSE, FALSE)	
	
	SWITCH iMenuType
		CASE MENU_TYPE_BUZZER
			SET_MENU_TITLE("MP_PROP_YACHT")
			IF DOES_LOCAL_PLAYER_OWN_YACHT(iCurrentYacht)
				//IF ownedDetails.iNumProperties = 1
					IF PROPERTY_NEAR_PLYS_OPT_AVAILABLE(mpYachts[iCurrentYacht].yachtPropertyDetails.vBlipLocation[iEntrancePlayerIsUsing],tempPropertyExtMenu.bFriendsNearby, tempPropertyExtMenu.bOrganisationNearby,  PROPERTY_YACHT_APT_1_BASE,iEntrancePlayerIsUsing,DEFAULT,iCurrentYacht)
						IF tempPropertyExtMenu.bOrganisationNearby
							ADD_MENU_ITEM_TEXT(iMenuCounter,"PROP_HEI_E_3",0,TRUE)
							tempPropertyExtMenu.menuPlayerArray[iMenuCounter]= PLAYER_ID()
							tempPropertyExtMenu.iPropertyID[iMenuCounter] = PROPERTY_YACHT_APT_1_BASE
							tempPropertyExtMenu.iOrganisationNearby = iMenuCounter	
							iMenuCounter++
						ENDIF	
						IF Is_Player_Currently_On_MP_Heist(PLAYER_ID())
					    OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
					    OR Is_Player_Currently_On_MP_Contact_Mission(PLAYER_ID())
							ADD_MENU_ITEM_TEXT(iMenuCounter,"PROP_HEI_E_0",0,TRUE)
							tempPropertyExtMenu.menuPlayerArray[iMenuCounter]= PLAYER_ID()
							tempPropertyExtMenu.iPropertyID[iMenuCounter] = PROPERTY_YACHT_APT_1_BASE
							tempPropertyExtMenu.iAllNearby = iMenuCounter
							iMenuCounter++
						ENDIF
						IF tempPropertyExtMenu.bFriendsNearby
							ADD_MENU_ITEM_TEXT(iMenuCounter,"PROP_HEI_E_2",0,TRUE)
							tempPropertyExtMenu.menuPlayerArray[iMenuCounter]= PLAYER_ID()
							tempPropertyExtMenu.iPropertyID[iMenuCounter] = PROPERTY_YACHT_APT_1_BASE
							tempPropertyExtMenu.iAllFriendsCrew = iMenuCounter	
							iMenuCounter++
						ENDIF
						
					ENDIF
				//ENDIF


				//ADD_MENU_ITEM_TEXT(iMenuCounter,"MP_PROP_MENU0",0,TRUE)
				ADD_MENU_ITEM_TEXT(iMenuCounter,"PROP_YACHT",0,TRUE)
				tempPropertyExtMenu.menuPlayerArray[iMenuCounter]= PLAYER_ID()
				tempPropertyExtMenu.iPropertyID[iMenuCounter] = PROPERTY_YACHT_APT_1_BASE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "tempPropertyExtMenu.iPropertyID[",iMenuCounter,"] = ", tempPropertyExtMenu.iPropertyID[iMenuCounter])
				iMenuCounter++
	
			ENDIF
			i = 0
			IF NOT bDoNotBuzzOthers
				REPEAT NUM_NETWORK_PLAYERS i
					//addOptionForPlayer = FALSE
					playerID = INT_TO_PLAYERINDEX(i)
					//iBuddyLoop = 0
					IF playerID != PLAYER_ID()
						IF IS_NET_PLAYER_OK(playerID,FALSE)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "SETUP_PROPERTY_OPTIONS_MENU: checking player: ", GET_PLAYER_NAME(playerID))
							CDEBUG1LN(DEBUG_SAFEHOUSE, "in property = ",GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(playerID) )
							CDEBUG1LN(DEBUG_SAFEHOUSE, "owned flag = ",IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_OWNED_PROPERTY))
							bHasThisYacht = FALSE
	
							IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iCurrentlyInsideProperty = PROPERTY_YACHT_APT_1_BASE
							AND GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(playerID) = iCurrentYacht
							AND IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_OWNED_PROPERTY)
							AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_EXITING_PROPERTY)
								bHasThisYacht = TRUE
								SET_BIT(iPlayersInYacht, NATIVE_TO_INT(playerID))
								iPlayersPropertyNum[i] = GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(playerID)

								tempPropertyExtMenu.menuPlayerArray[iMenuCounter] = playerID
								ADD_MENU_OPTION_FOR_PLAYER(iMenuCounter,playerID,FALSE)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "SETUP_PROPERTY_OPTIONS_MENU: add option for ", GET_PLAYER_NAME(playerID))
							ENDIF
							IF NOT bHasThisYacht
								CLEAR_BIT(iPlayersInYacht, NATIVE_TO_INT(playerID))
							ENDIF
						ELSE
							CDEBUG1LN(DEBUG_SAFEHOUSE, "SETUP_PROPERTY_OPTIONS_MENU: playerID: not ok ",i)
							CLEAR_BIT(iPlayersInYacht, NATIVE_TO_INT(playerID))
						ENDIF
					ENDIF
				ENDREPEAT
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "SETUP_PROPERTY_OPTIONS_MENU: bDoNotBuzzOthers is true skipping player list")
			ENDIF
			tempPropertyExtMenu.iMaxVertSel = iMenuCounter
			SET_CURRENT_MENU_ITEM(tempPropertyExtMenu.iCurVertSel)
					
			REMOVE_MENU_HELP_KEYS()
			IF iMenuCounter > 0
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "BB_SELECT")
			ENDIF
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "BB_BACK")
			
		BREAK
		CASE MENU_TYPE_PROPERTY_ENTRY_OPTIONS
			SET_MENU_TITLE(mpYachts[tempPropertyExtMenu.iExitSelection].yachtPropertyDetails.tl_PropertyName)
			
						
			//IF PROPERTY_NEAR_FRIENDS_OPT_AVAILABLE
			IF PROPERTY_NEAR_PLYS_OPT_AVAILABLE(mpYachts[iCurrentYacht].yachtPropertyDetails.vBlipLocation[iEntrancePlayerIsUsing],tempPropertyExtMenu.bFriendsNearby, tempPropertyExtMenu.bOrganisationNearby,  PROPERTY_YACHT_APT_1_BASE,iEntrancePlayerIsUsing,DEFAULT,iCurrentYacht)
				IF tempPropertyExtMenu.bOrganisationNearby
					ADD_MENU_ITEM_TEXT(iMenuCounter,"PROP_HEI_E_3",0,TRUE)
					tempPropertyExtMenu.menuPlayerArray[iMenuCounter]= PLAYER_ID()
					tempPropertyExtMenu.iPropertyID[iMenuCounter] = tempPropertyExtMenu.iExitSelection
					tempPropertyExtMenu.iOrganisationNearby = iMenuCounter	
					iMenuCounter++
				ENDIF	
				IF Is_Player_Currently_On_MP_Heist(PLAYER_ID())
			    OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
			    OR Is_Player_Currently_On_MP_Contact_Mission(PLAYER_ID())
					ADD_MENU_ITEM_TEXT(iMenuCounter,"PROP_HEI_E_0",0,TRUE)
					tempPropertyExtMenu.menuPlayerArray[iMenuCounter]= PLAYER_ID()
					tempPropertyExtMenu.iPropertyID[iMenuCounter] = tempPropertyExtMenu.iExitSelection
					tempPropertyExtMenu.iAllNearby = iMenuCounter
					iMenuCounter++
				ENDIF
				IF tempPropertyExtMenu.bFriendsNearby
					ADD_MENU_ITEM_TEXT(iMenuCounter,"PROP_HEI_E_2",0,TRUE)
					tempPropertyExtMenu.menuPlayerArray[iMenuCounter]= PLAYER_ID()
					tempPropertyExtMenu.iPropertyID[iMenuCounter] = tempPropertyExtMenu.iExitSelection
					tempPropertyExtMenu.iAllFriendsCrew = iMenuCounter	
					iMenuCounter++
				ENDIF
				
			ENDIF
			
			ADD_MENU_ITEM_TEXT(iMenuCounter,"PROP_HEI_E_1",0,TRUE)
			tempPropertyExtMenu.menuPlayerArray[iMenuCounter]= PLAYER_ID()
			tempPropertyExtMenu.iPropertyID[iMenuCounter] = tempPropertyExtMenu.iExitSelection
			iMenuCounter++
			
			tempPropertyExtMenu.iMaxVertSel = iMenuCounter
			SET_CURRENT_MENU_ITEM(tempPropertyExtMenu.iCurVertSel)
			
			REMOVE_MENU_HELP_KEYS()
			ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_ACCEPT , "BB_SELECT")
			ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_CANCEL, "BB_BACK")
			ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_DPAD_UD,"HUD_INPUT1C")
		BREAK
	ENDSWITCH
	NET_PRINT("SETUP_PROPERTY_OPTIONS_MENU: setup menu") NET_NL()
ENDPROC

PROC SETUP_PROPERTY_MENU_DESCRIPTION_BIT(INT iBit)
	REINIT_NET_TIMER(tempPropertyExtMenu.menuDescriptionTimer)
	SET_BIT(tempPropertyExtMenu.iMenuDescriptionBS,iBit)
ENDPROC

PROC DO_WARNING_MESSAGE(INT iValue, BOOL bSmallerGarage)
	STRING pHeaderTextLabel
	STRING pBodyTextLabel
	STRING pBodySubTextLabel = NULL
	INT NumberToInsert = -1
	STRING pFirstSubStringTextLabel = NULL
	
	pHeaderTextLabel = ("BRSCRWTEX")				//"Alert"
	IF bSmallerGarage
		pBodyTextLabel = ("BRDISPROP2B1")			//"You already own a property, purchasing this one will trade in your old one.~n~
													// You will also lose access to some of your stored vehicles as the Garage is smaller.  The trade in will ~a~ you $~1~.""
		pBodySubTextLabel = ("BRSHETPROSUB1")		//"Do you wish to continue with this purchase?
	ELSE
		pBodyTextLabel = ("BRDISPROPB1")			//"You already own a property, purchasing this one will trade in your old one.  The trade in will ~a~ you $~1~.""
		pBodySubTextLabel = ("BRSHETPROSUB1")		//"Do you wish to continue with this purchase?
	ENDIF
	NumberToInsert = iValue
	IF NumberToInsert < 0
		NumberToInsert *= -1			
	ENDIF
	IF iValue < 0  //3rd
		pFirstSubStringTextLabel = ("BRSHETMAK")	//make
	ELSE
		pFirstSubStringTextLabel = ("BRSHETPRSA")	//save
	ENDIF
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "DO_WARNING_MESSAGE(pHeaderTextLabel:\"",
			pHeaderTextLabel, "\", pBodyTextLabel:\"",
			pBodyTextLabel, "\", pBodySubTextLabel:\"",
			pBodySubTextLabel, "\", NumberToInsert:\"",
			NumberToInsert, "\", pFirstSubStringTextLabel:\"",
			pFirstSubStringTextLabel, "\")")

	warpInControl.iWarningScreenButtonBS = FE_WARNING_OKCANCEL
	SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS(pHeaderTextLabel,
			pBodyTextLabel,
			warpInControl.iWarningScreenButtonBS,
			pBodySubTextLabel,
			TRUE,
			NumberToInsert,
			WARNING_MESSAGE_DEFAULT,
			pFirstSubStringTextLabel)
ENDPROC

PROC HANDLE_MENU_DESCRIPTION(INT iMenuType)
	//item description
	SWITCH iMenuType
		CASE MENU_TYPE_BUZZER
			IF tempPropertyExtMenu.iMenuDescriptionBS = 0 
				IF NOT IS_BIT_SET(iLocalBS,LOCAL_BS_AWAITING_BUZZER_RESPONSE)
					IF tempPropertyExtMenu.iCurVertSel = tempPropertyExtMenu.iAllNearby
						SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_HEI_Y_0b",100)
					ELIF tempPropertyExtMenu.iCurVertSel = tempPropertyExtMenu.iAllFriendsCrew
						SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_HEI_Y_2b",100)
					ELIF tempPropertyExtMenu.iCurVertSel = tempPropertyExtMenu.iOrganisationNearby
						SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_HEI_I_3b2",100)
					ELSE
						IF tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel]= PLAYER_ID()
							SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_PROP_MENU0c",100)
						ELSE
							IF tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel] != INVALID_PLAYER_INDEX()
								SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_PROP_MENUyb",100)
								ADD_CURRENT_MENU_ITEM_DESCRIPTION_PLAYER_NAME(GET_PLAYER_NAME(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel]))
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel] != INVALID_PLAYER_INDEX()
						SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_PROP_MENU1c",100)
						ADD_CURRENT_MENU_ITEM_DESCRIPTION_PLAYER_NAME(GET_PLAYER_NAME(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel]))
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(tempPropertyExtMenu.iMenuDescriptionBS,MP_PROP_MENU_DES_REJECTED_ENTRY)
					SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_PROP_DES0",100)
				ENDIF
				IF HAS_NET_TIMER_EXPIRED(tempPropertyExtMenu.menuDescriptionTimer,MP_PROP_MENU_DES_TIME)
					RESET_NET_TIMER(tempPropertyExtMenu.menuDescriptionTimer)
					tempPropertyExtMenu.iMenuDescriptionBS = 0
				ENDIF
			ENDIF
		BREAK
		CASE MENU_TYPE_PROPERTY_ENTRY_OPTIONS
			IF tempPropertyExtMenu.iMenuDescriptionBS = 0 
				IF tempPropertyExtMenu.iCurVertSel = tempPropertyExtMenu.iAllNearby
					SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_HEI_Y_0b",100)
				ELIF tempPropertyExtMenu.iCurVertSel = tempPropertyExtMenu.iAllFriendsCrew
					SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_HEI_Y_2b",100)
				ELIF tempPropertyExtMenu.iCurVertSel = tempPropertyExtMenu.iOrganisationNearby
					SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_HEI_I_3b2",100)
				ELSE
					SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_PROP_MENU0c",100)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC HANDLE_RESPONSE_TO_ENTRY_REQUEST()
	IF IS_BIT_SET(iLocalBS,LOCAL_BS_AWAITING_BUZZER_RESPONSE)
		IF IS_NET_PLAYER_OK(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel],FALSE)
			IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS,NATIVE_TO_INT(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel]))
				IF NOT IS_PLAYER_IN_PROPERTY((tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel]),FALSE)
				OR (GlobalplayerBD_FM[NATIVE_TO_INT(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel])].propertyDetails.iCurrentlyInsideProperty <= 0) // added by Neil F. for 1822930
				OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel])].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_EXITING_PROPERTY)

					CLEAR_BIT(iLocalBS,LOCAL_BS_AWAITING_BUZZER_RESPONSE)
					CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_MENU)
					//NETWORK_SET_TALKER_PROXIMITY(CHAT_PROXIMITY_FM)
					NETWORK_CLEAR_VOICE_CHANNEL()
					RESET_NET_TIMER(buzzerTimer)
					//SETUP_SPECIFIC_SPAWN_LOCATION(mpYachts[iCurrentYacht].yachtPropertyDetails.house.exits[0].vOutPlayerLoc,mpYachts[iCurrentYacht].yachtPropertyDetails.house.exits[0].fOutPlayerHeading, 10, FALSE)
					//SET_LOCAL_STAGE(PROP_STAGE_LEAVE_ENTRANCE_AREA)
					GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0
					globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE")
					CDEBUG1LN(DEBUG_SAFEHOUSE, "GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0")
					CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_RESPONSE_TO_ENTRY_REQUEST: property owner is no longer in propety cleaning up")
				ELSE
					IF GlobalplayerBD_FM[NATIVE_TO_INT(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel])].propertyDetails.entryResponse[NATIVE_TO_INT(PLAYER_ID())].iResponse = PROPERTY_BROADCAST_RESPONSE_YES
						globalPropertyEntryData.ownerID = tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel]
						globalPropertyEntryData.ownerHandle = GET_GAMER_HANDLE_PLAYER(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel])
						CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_RESPONSE_TO_ENTRY_REQUEST:setting owner as ",NATIVE_TO_INT(globalPropertyEntryData.ownerID))
						SET_BIT(iLocalBS2,LOCAL_BS2_bEnteringAnotherPlayerProperty)
						//GET_MP_PROPERTY_DETAILS(mpYachts[iCurrentYacht],iCurrentYacht)
						globalPropertyEntryData.iEntrance = 0
						
						CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_RESPONSE_TO_ENTRY_REQUEST:player recieved permission to enter property #",iCurrentYacht)
						globalPropertyEntryData.iPropertyEntered =  GlobalplayerBD_FM[NATIVE_TO_INT(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel])].propertyDetails.iCurrentlyInsideProperty
						PRINTLN("5 globalPropertyEntryData.iPropertyEntered = ",globalPropertyEntryData.iPropertyEntered)
						//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0
						globalPropertyEntryData.iYachtEntered = iCurrentYacht
						globalPropertyEntryData.bBuzzAccepted = TRUE
						DISPLAY_RADAR(FALSE)
						bDisabledRadar = TRUE
						SET_LOCAL_STAGE(STAGE_BUZZED_INTO_PROPERTY)
						CLEAR_BIT(iLocalBS,LOCAL_BS_AWAITING_BUZZER_RESPONSE)
						
//						
						EXIT
					ELIF GlobalplayerBD_FM[NATIVE_TO_INT(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel])].propertyDetails.entryResponse[NATIVE_TO_INT(PLAYER_ID())].iResponse = PROPERTY_BROADCAST_RESPONSE_NO
					//OR HAS_NET_TIMER_EXPIRED(buzzerTimer,60000) 
						SET_BIT(iRejectedEntryBS,NATIVE_TO_INT(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel]) )
						CLEAR_BIT(iLocalBS,LOCAL_BS_AWAITING_BUZZER_RESPONSE)
						CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_MENU)
						//NETWORK_SET_TALKER_PROXIMITY(CHAT_PROXIMITY_FM)
						NETWORK_CLEAR_VOICE_CHANNEL()
						RESET_NET_TIMER(buzzerTimer)
						//SETUP_SPECIFIC_SPAWN_LOCATION(mpYachts[iCurrentYacht].yachtPropertyDetails.house.exits[0].vOutPlayerLoc,mpYachts[iCurrentYacht].yachtPropertyDetails.house.exits[0].fOutPlayerHeading, 10, FALSE)
						//SET_LOCAL_STAGE(PROP_STAGE_LEAVE_ENTRANCE_AREA)
						globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "globalPropertyEntryData.bLeftOrRejectedBuzzer 2 = TRUE")
						GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0
						CDEBUG1LN(DEBUG_SAFEHOUSE, "No Response: GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0")
						EXIT
					ENDIF
				ENDIF
				//CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: Waiting for response from property owner")
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: Awaitng response but bit is not set to current selection??")
			ENDIF
		ELSE
			CLEAR_BIT(iLocalBS,LOCAL_BS_AWAITING_BUZZER_RESPONSE)
			CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_MENU)
			globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "globalPropertyEntryData.bLeftOrRejectedBuzzer 3 = TRUE")
			NETWORK_CLEAR_VOICE_CHANNEL()
			CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_RESPONSE_TO_ENTRY_REQUEST: removing player from voice channel property owner is no longer OK")
			//NETWORK_SET_TALKER_PROXIMITY(CHAT_PROXIMITY_FM)
			RESET_NET_TIMER(buzzerTimer)
			//SETUP_SPECIFIC_SPAWN_LOCATION(mpYachts[iCurrentYacht].yachtPropertyDetails.house.exits[0].vOutPlayerLoc,mpYachts[iCurrentYacht].yachtPropertyDetails.house.exits[0].fOutPlayerHeading, 10, FALSE)
			//SET_LOCAL_STAGE(PROP_STAGE_LEAVE_ENTRANCE_AREA)
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0
			CDEBUG1LN(DEBUG_SAFEHOUSE, "Property owner no longer ok: GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0")
			CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_RESPONSE_TO_ENTRY_REQUEST: property owner is no longer OK cleaning up")
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_ENTER_STRUCT()
	warpInControl.iStage = 0
	warpInControl.iWarningScreenButtonBS = FE_WARNING_NONE
	warpInControl.ibuttonBS = 0
	warpInControl.iVehicleSlot = -1
	warpInControl.iBS = 0
ENDPROC

PROC CLEANUP_THIS_PROPERTY_CAM(CAMERA_INDEX &propertyCam)
	IF DOES_CAM_EXIST(propertyCam)
		IF NOT IS_BIT_SET(iLocalBS2,LOCAL_BS2_bKeepScriptCameras)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
		ENDIF
		
		IF IS_CAM_ACTIVE(propertyCam)
			SET_CAM_ACTIVE(propertyCam,FALSE)
		ENDIF
		DESTROY_CAM(propertyCam)
	ENDIF	
ENDPROC
PROC CLEANUP_PROPERTY_CAMERAS()
	CLEANUP_THIS_PROPERTY_CAM(propertyCam0)
	CLEANUP_THIS_PROPERTY_CAM(propertyCam1)
ENDPROC

PROC RESET_PROPERTY_MENU()
	CLEANUP_MENU_ASSETS()
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		tempPropertyExtMenu.menuPlayerArray[i] = INVALID_PLAYER_INDEX()
	ENDREPEAT
	RESET_NET_TIMER(tempPropertyExtMenu.menuDescriptionTimer)
	RESET_NET_TIMER(tempPropertyExtMenu.menuNavigationDelay)
	tempPropertyExtMenu.iMenuDescriptionBS = 0
	tempPropertyExtMenu.iMaxVertSel = 0
	tempPropertyExtMenu.iCurVertSel = 0
	tempPropertyExtMenu.iButtonBS = 0
	
	tempPropertyExtMenu.iExitSelection = -1
	tempPropertyExtMenu.iMoveBetweenSelection = -1
	tempPropertyExtMenu.iAllExitOption = -1
	tempPropertyExtMenu.iAllGarageOption = -1
	
	IF IS_BIT_SET(iLocalBS,LOCAL_BS_AWAITING_BUZZER_RESPONSE)
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0
		CDEBUG1LN(DEBUG_SAFEHOUSE, "RESET_PROPERTY_MENU: GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0")
	ENDIF
	CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_MENU)
	CLEAR_BIT(iLocalBS,LOCAL_BS_AWAITING_BUZZER_RESPONSE)
	CLEAR_BIT(iLocalBS,LOCAL_BS_PURCHASE_MENU_REQUEST_SF)
	CLEAR_BIT(iLocalBS,LOCAL_BS_PURCHASE_MENU_SHOWING_WARN)
	CLEAR_BIT(iLocalBS,LOCAL_BS_STOP_CUSTOM_MENU_CHECK)
	//IF warningscreen != NULL
	//	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(warningscreen)
	//ENDIF
	CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: RESET_PROPERTY_MENU called")
ENDPROC

PROC CLEANUP_ENTRY_CS()
	IF entryCutData.iSyncedSceneID >= 0
		IF NOT IS_PED_INJURED(entryCutData.playerClone)
			CLEAR_PED_TASKS(entryCutData.playerClone)
		ENDIF
		//IF IS_SYNCHRONIZED_SCENE_RUNNING(entryCutData.iSyncedSceneID)
			
		entryCutData.iSyncedSceneID = -1 
		CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_ENTRY_CS- killing sync scene")
		//ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(entryCutData.playerClone)
		DELETE_PED(entryCutData.playerClone)
	ENDIF
	RESET_NET_TIMER(entryCutData.timer)
	IF DOES_CAM_EXIST(entryCutData.cameras[0])
		DESTROY_CAM(entryCutData.cameras[0])
	ENDIF
	
	entryCutData.iStage = ENTRY_CS_STAGE_INIT_DATA
	IF IS_BIT_SET(entryCutData.iBS,ENTRY_CS_BS_STARTED_MP_CUTSCENE)
		IF NOT g_sTransitionSessionData.bMissionCutsceneInProgress 
			CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_ENTRY_CS- bMissionCutsceneInProgress = FALSE, do CLEANUP_MP_CUTSCENE.")
			CLEANUP_MP_CUTSCENE(FALSE,FALSE)
		ELSE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_ENTRY_CS- bMissionCutsceneInProgress = TRUE, SKIP CLEANUP_MP_CUTSCENE.")
		ENDIF
	ENDIF
	
//	IF IS_BIT_SET(entryCutData.iBS,ENTRY_CS_BS_TOOK_AWAY_CONTROL)
//		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//		CLEAR_BIT(entryCutData.iBS,ENTRY_CS_BS_TOOK_AWAY_CONTROL)	
//		CLEAR_BIT(iLocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL)
//	ENDIF
	
	INT i
	REPEAT SYNC_SCENE_MAX_ELEMENTS i
		entryCutData.animName[i] = ""
	ENDREPEAT
	IF IS_BIT_SET(iLocalBS, LOCAL_BS_BUZZER_REQUESTED_ANIM)
			REMOVE_ANIM_DICT(entryCutData.animDictionary)
		CLEAR_BIT(iLocalBS, LOCAL_BS_BUZZER_REQUESTED_ANIM)
	ENDIF
	entryCutData.animDictionary = ""
	i = 0
	REPEAT 2 i
		entryCutData.vCamLoc[i] = <<0,0,0>>
		entryCutData.vCamRot[i] = <<0,0,0>>
		entryCutData.fCamFOV[i] = 0
	ENDREPEAT

	entryCutData.vSyncSceneLoc = <<0,0,0>>
	entryCutData.vSyncSceneRot = <<0,0,0>>
	
//	IF IS_BIT_SET(entryCutData.iBS,ENTRY_CS_BS_APT_DOOR_MODEL_HIDE)
//		REMOVE_MODEL_HIDE(propertyDoors[1].vCoords,1,propertyDoors[1].doorModel)
//	ENDIF
	
//	IF IS_BIT_SET(entryCutData.iBS,ENTRY_CS_BS_APT_DOOR_MODEL_HIDE)
//		IF GET_PROPERTY_BUILDING(iCurrentYacht) <> MP_PROPERTY_BUILDING_5
//			IF GET_PROPERTY_SIZE_TYPE(iCurrentYacht) = PROP_SIZE_TYPE_LARGE_APT
//			OR NOT ARE_VECTORS_EQUAL(propertyDoors[1].vCoords,<<0,0,0>>)
//				REMOVE_MODEL_HIDE(propertyDoors[1].vCoords,1,propertyDoors[1].doorModel)
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "REMOVING model hide 6  at ",propertyDoors[1].vCoords, " model: ",propertyDoors[1].doorModel)
//			ELSE
//				REMOVE_MODEL_HIDE(propertyDoors[0].vCoords,1,propertyDoors[0].doorModel)	
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "REMOVING  model hide 7  at ",propertyDoors[0].vCoords, " model: ",propertyDoors[0].doorModel)
//			ENDIF
//		ELSE
//			REMOVE_MODEL_HIDE(propertyDoors[0].vCoords,1,propertyDoors[0].doorModel)	
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "REMOVING  model hide 8  at ",propertyDoors[0].vCoords, " model: ",propertyDoors[0].doorModel)
//		ENDIF
//		CLEAR_BIT(entryCutData.iBS,ENTRY_CS_BS_APT_DOOR_MODEL_HIDE)
//	ENDIF
	
//	IF DOES_ENTITY_EXIST(entryCutData.objIDs[0])
//		DELETE_OBJECT(entryCutData.objIDs[0])
//	ENDIF
	IF DOES_ENTITY_EXIST(entryCutData.playerClone)
		DELETE_PED(entryCutData.playerClone)
	ENDIF
	entryCutData.iBS = 0
	
	IF entryCutData.iSoundID >= 0
		STOP_SOUND(entryCutData.iSoundID)
	ENDIF
	
	//DELETE_CLONED_ENTITIES_AND_CLEAR_REQUESTS(4)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_ENTRY_CS- called")
ENDPROC

PROC RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
	IF IS_BIT_SET(iLocalBS, LOCAL_BS_FORCE_TERMINATED_INTERNET)
		MP_FORCE_TERMINATE_INTERNET_CLEAR()
		CLEAR_BIT(iLocalBS, LOCAL_BS_FORCE_TERMINATED_INTERNET)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: Force terminate internet cleared - 1")
	ENDIF
	CLEAR_BIT(ilocalBS,LOCAL_BS2_TriggeredAcceptWarning)
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_USING_EXT)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "PROPERTY_BROADCAST_BS_PLAYER_USING_EXT- CLEARED")
	CLEAR_BIT(iLocalBS2,LOCAL_BS2_bWalkInTriggered)
	CLEAR_BIT(iLocalBS2,LOCAL_BS2_bClearedHeistPanCutscene)
	
		CLEANUP_ENTRY_CS()
	
	IF IS_BIT_SET(iLocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL)
	OR IS_BIT_SET(entryCutData.iBS,ENTRY_CS_BS_TOOK_AWAY_CONTROL)
		IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_MP_PROPERTY_INT")) <= 0
		AND NOT g_bPropInteriorScriptRunning)
		OR globalPropertyEntryData.bLeftOrRejectedBuzzer
		OR bLeavingEntranceArea
			IF NOT IS_PLAYER_TELEPORT_ACTIVE()
			AND NOT IS_PLAYER_IN_CORONA()
			AND NOT IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				CLEAR_BIT(iLocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL)
				CLEAR_BIT(entryCutData.iBS,ENTRY_CS_BS_TOOK_AWAY_CONTROL)
				PRINTLN("AM_MP_YACHT: RESET_TO_ENTRANCE_TO_PROPERTY_STAGE turning on player control")
			ELSE
				PRINTLN("AM_MP_YACHT: RESET_TO_ENTRANCE_TO_PROPERTY_STAGE not turning on player control as IS_PLAYER_TELEPORT_ACTIVE")
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: RESET_TO_ENTRANCE_TO_PROPERTY_STAGE not turning on player control as interior is running")
			CLEAR_BIT(iLocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL)
			CLEAR_BIT(entryCutData.iBS,ENTRY_CS_BS_TOOK_AWAY_CONTROL)
		ENDIF
	ENDIF
	
	
	
	bLeavingEntranceArea = FALSE
	IF IS_BIT_SET(ilocalBS,LOCAL_BS_STARTED_FADE)
		IF IS_SCREEN_FADING_OUT()
		OR IS_SCREEN_FADED_OUT()
			DO_SCREEN_FADE_IN(500)		
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: RESET_TO_ENTRANCE_TO_PROPERTY_STAGE force fading screen back in")
		ENDIF
		CLEAR_BIT(ilocalBS,LOCAL_BS_STARTED_FADE)
	ENDIF
	CLEAR_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ENTER_GARAGE)
	IF globalPropertyEntryData.iGarageIntroProperty = iCurrentYacht
		globalPropertyEntryData.iGarageIntroProperty = -1
	ENDIF
	IF IS_BIT_SET(iLocalBS, LOCAL_BS_BUZZER_REQUESTED_ANIM)
			REMOVE_ANIM_DICT(entryCutData.animDictionary)
		CLEAR_BIT(iLocalBS, LOCAL_BS_BUZZER_REQUESTED_ANIM)
	ENDIF

	SET_LOCAL_STAGE(STAGE_CHECK_FOR_IN_LOCATE)
	
	CLEAR_INVALID_VEHICLE_HELP()
	CLEANUP_ENTER_STRUCT()
	CLEANUP_PROPERTY_CAMERAS()
	RESET_PROPERTY_MENU()
	RESET_NET_TIMER(failSafeEntryTimer)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "bVehBelongsToOtherGarage = FALSE - 2")
	IF iPurchaseContextIntention != NEW_CONTEXT_INTENTION
		RELEASE_CONTEXT_INTENTION(iPurchaseContextIntention)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: Releasing the context intention for the purchase menu. (RESET)")
	ENDIF
	IF bDisabledRadar
		DISPLAY_RADAR(TRUE)
	ENDIF
	
	IF bSetEntryData = TRUE
		//CLEAR_GLOBAL_ENTRY_DATA()	//Some of this stuff might need to be cleared, but some might still be needed. SO don#t clear just now.
		CLEAR_FOCUS()
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: RESET_TO_ENTRANCE_TO_PROPERTY_STAGE - CLEAR FOCUS")
	ENDIF
	bDisabledRadar = FALSE
	
	CLEAR_BIT(iLocalBS, LOCAL_BS_BUZZER_GIVE_GOTO_TASK)	
	CLEAR_BIT(iLocalBS, LOCAL_BS_BUZZER_GIVE_RING_TASK)	
	IF globalPropertyEntryData.bLeftOrRejectedBuzzer
	OR bSetEntryData
		IF NOT g_bPropInteriorScriptRunning
		AND NOT IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_AcceptedInvite)
		AND NOT IS_BIT_SET(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_LAUNCHED_INT_SCRIPT)
			IF globalPropertyEntryData.bLeftOrRejectedBuzzer
				CDEBUG1LN(DEBUG_SAFEHOUSE, "globalPropertyEntryData.bLeftOrRejectedBuzzer clearing global entry data")
			ELIF bSetEntryData
				CDEBUG1LN(DEBUG_SAFEHOUSE, "bSetEntryData clearing global entry data")
			ENDIF
			CLEAR_GLOBAL_ENTRY_DATA()
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_SPAWNING_IN_A_PROPERTY)
		
		ENDIF
	ENDIF
	bSetEntryData = FALSE
	
	IF bDisabledStore
		SET_STORE_ENABLED(TRUE)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "enabling store reset to entrance stage- EXT")
		bDisabledStore = FALSE
	ENDIF
	
	bDoNotBuzzOthers = FALSE

	IF DOES_CAM_EXIST(propertyCam0)
		DESTROY_CAM(propertyCam0)
	ENDIF
	IF DOES_CAM_EXIST(propertyCam1)
		DESTROY_CAM(propertyCam1)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
	ENDIF
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_ENTERING_A_PROPERTY)
	
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_MP_PROPERTY_INT")) <= 0
	AND NOT g_bPropInteriorScriptRunning
		CLEAR_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ENTER_GARAGE)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: Clearing GLOBAL_PROPERTY_ENTRY_BS_ENTER_GARAGE")
	ENDIF
	VEHICLE_INDEX vehFadeOut
	IF IS_BIT_SET(iLocalBS2,LOCAL_BS2_bFadeOutWarpIntoGarage)
		IF IS_BIT_SET(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_MADE_INVISIBLE_ON_EXTERIOR)
			//IF NOT IS_PLAYER_VISIBLE_TO_SCRIPT(PLAYER_ID())	
			//ENDIF
			//NETWORK_SET_LOCAL_PLAYER_INVINCIBLE_TIME(0)
			IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
				
				vehFadeOut = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF GET_PED_IN_VEHICLE_SEAT(vehFadeOut) = PLAYER_PED_ID()
					
					//NETWORK_FADE_IN_ENTITY(vehFadeOut, TRUE)
					SET_ENTITY_VISIBLE(vehFadeOut,TRUE)
					SET_ENTITY_VISIBLE(PLAYER_PED_ID(),TRUE)
					//SET_BIT(iLocalBS2,LOCAL_BS2_bFadeOutWarpIntoGarage)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "NETWORK_FADE_IN_ENTITY called from exterior script- VEH")
					CLEAR_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_MADE_INVISIBLE_ON_EXTERIOR)
				ENDIF
			ELSE
				//FADE_OUT_LOCAL_PLAYER(FALSE)
				//NETWORK_FADE_IN_ENTITY(PLAYER_PED_ID(), TRUE)
				SET_ENTITY_VISIBLE(PLAYER_PED_ID(),TRUE)
				NETWORK_SET_LOCAL_PLAYER_INVINCIBLE_TIME(0)
				CLEAR_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_MADE_INVISIBLE_ON_EXTERIOR)
				//SET_BIT(iLocalBS2,LOCAL_BS2_bFadeOutWarpIntoGarage)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "NETWORK_FADE_IN_ENTITY called from exterior script - PED")
			ENDIF
			CLEAR_BIT(iLocalBS2,LOCAL_BS2_bFadeOutWarpIntoGarage)
		ENDIF
	ENDIF
	
	IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_MP_PROPERTY_INT")) <= 0
	AND NOT g_bPropInteriorScriptRunning)
	OR IS_PLAYER_SWITCH_IN_PROGRESS()
		IF g_bMadeCarInvunerableOnEntry
			IF IS_NET_PLAYER_OK(PLAYER_ID(),TRUE,FALSE)
				IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
					vehFadeOut = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF GET_PED_IN_VEHICLE_SEAT(vehFadeOut) = PLAYER_PED_ID()
						SET_ENTITY_CAN_BE_DAMAGED(vehFadeOut,TRUE)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_ENTITY_CAN_BE_DAMAGED TRUE called from exterior script- VEH")	
					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_ENTITY_CAN_BE_DAMAGED not driver")	
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_ENTITY_CAN_BE_DAMAGED not sitting in vehicle")	
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_ENTITY_CAN_BE_DAMAGED not ok player")	
			ENDIF
			CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_ENTITY_CAN_BE_DAMAGED cleared flag to check")	
			g_bMadeCarInvunerableOnEntry = FALSE
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_ENTITY_CAN_BE_DAMAGED bypassed interior script is running no switch")	
	ENDIF
	CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: Clearing PROPERTY_BROADCAST_BS_PLAYER_ENTERING_A_PROPERTY")
	CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT:RESET_TO_ENTRANCE_TO_PROPERTY_STAGE called") NET_NL()
ENDPROC

FUNC BOOL SHOULD_MENU_UPDATE()
	PLAYER_INDEX playerID
	//addOptionForPlayer = FALSE
	
	SWITCH iCurrentMenuType 
		CASE MENU_TYPE_BUZZER
			playerID = INT_TO_PLAYERINDEX(iSlowLoopCounter)
			IF playerID != PLAYER_ID()
				IF NOT IS_BIT_SET(iPlayersInYacht, NATIVE_TO_INT(playerID))
					IF IS_NET_PLAYER_OK(playerID,FALSE)
						IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iCurrentlyInsideProperty = PROPERTY_YACHT_APT_1_BASE
						AND GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(playerID) = iCurrentYacht
						AND IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_OWNED_PROPERTY)	
						AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_EXITING_PROPERTY)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: Menu should update as players in building bit is not set for player in property")
							RETURN TRUE
						ENDIF
					ENDIF
				ELSE
					IF IS_NET_PLAYER_OK(playerID)
						IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_OWNED_PROPERTY)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: Menu should update as player no longer in property")
							RETURN TRUE
						ELSE
							IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iCurrentlyInsideProperty != iPlayersPropertyNum[iSlowLoopCounter]
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: Menu should update as player in property now in a new property")
								RETURN TRUE
							ENDIF
							IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_EXITING_PROPERTY)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: Menu should update as player in property now exiting")
								RETURN TRUE
							ENDIF
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: Menu should update as player in property not ok")
						RETURN TRUE
					ENDIF
					
				ENDIF
			ENDIF
			IF HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: HAVE_CONTROLS_CHANGED returned true")
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	iSlowLoopCounter++
	IF iSlowLoopCounter >= NUM_NETWORK_PLAYERS
		iSlowLoopCounter = 0
	ENDIF
	IF tempPropertyExtMenu.iAllNearby != -1
	OR tempPropertyExtMenu.iAllFriendsCrew != -1
	OR tempPropertyExtMenu.iOrganisationNearby != -1
		IF PROPERTY_NEAR_PLYS_OPT_AVAILABLE(mpYachts[iCurrentYacht].yachtPropertyDetails.vBlipLocation[iEntrancePlayerIsUsing],tempPropertyExtMenu.bFriendsNearby, tempPropertyExtMenu.bOrganisationNearby,  PROPERTY_YACHT_APT_1_BASE,iEntrancePlayerIsUsing,DEFAULT,iCurrentYacht)
			IF Is_Player_Currently_On_MP_Heist(PLAYER_ID())
			OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
			OR Is_Player_Currently_On_MP_Contact_Mission(PLAYER_ID())
				IF tempPropertyExtMenu.iAllNearby = -1
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: tempPropertyExtMenu.iAllNearby = -1")
					RETURN TRUE
				ENDIF
			ENDIF
			
			IF tempPropertyExtMenu.bFriendsNearby
				IF tempPropertyExtMenu.iAllFriendsCrew = -1
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: tempPropertyExtMenu.iAllFriendsCrew = -1")
					RETURN TRUE
				ENDIF
				
			ENDIF
			IF tempPropertyExtMenu.bOrganisationNearby
				IF tempPropertyExtMenu.iOrganisationNearby = -1
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: tempPropertyExtMenu.iOrganisationNearby  = -1")
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: PROPERTY_NEAR_PLYS_OPT_AVAILABLE returned false")
			RETURN TRUE
		ENDIF
	ENDIF
			
	RETURN FALSE
ENDFUNC

FUNC BOOL HANDLE_PROPERTY_OPTIONS_MENU_NAVIGATION_DELAY(CONTROL_ACTION theInput)
	IF (NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,theInput)
	AND NOT IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,theInput))
	OR HAS_NET_TIMER_EXPIRED(tempPropertyExtMenu.menuNavigationDelay,250)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC HANDLE_PROPERTY_OPTIONS_MENU()
	HIDE_HELP_TEXT_THIS_FRAME()
	DISABLE_DPADDOWN_THIS_FRAME()
	DISABLE_SELECTOR_THIS_FRAME()
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
	REINIT_NET_TIMER(cinematicDelay,TRUE)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_LEFT)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_RIGHT)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_UP)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_DOWN)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_CURSOR_SCROLL_UP)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_CURSOR_SCROLL_DOWN)
	
	BOOL bCursorAccept = FALSE
	BOOL bUsingCursor = FALSE
	// Handle mouse clicks on the buzzer menu
	IF iCurrentMenuType = MENU_TYPE_BUZZER
	AND mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[iEntrancePlayerIsUsing].iType != ENTRANCE_TYPE_GARAGE
		HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
		IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
			SET_MOUSE_CURSOR_THIS_FRAME()
			IF IS_MENU_CURSOR_ACCEPT_PRESSED()
				IF g_iMenuCursorItem != tempPropertyExtMenu.iCurVertSel
					tempPropertyExtMenu.iCurVertSel = g_iMenuCursorItem
				ELSE
					bCursorAccept = TRUE
				ENDIF
			ENDIF
			bUsingCursor = TRUE
		ENDIF
	ENDIF
	
//	IF IS_NET_PLAYER_OK(PLAYER_ID())
//		SET_PED_RESET_FLAG(PLAYER_PED_ID(),PED_RESET_FLAGS)
//	ENDIF

	INT iTempSelectionVert
	
	BOOL bInputUsedThisFrame = FALSE
	
	IF IS_COMMERCE_STORE_OPEN()
	OR IS_PAUSE_MENU_ACTIVE()
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iBoolsBitSet, iBS_STORE_ACTIVE)
		IF NOT g_sShopSettings.bProcessStoreAlert
			CLEAR_BIT(iBoolsBitSet, iBS_LAUNCH_STORE)
			CLEAR_BIT(iBoolsBitSet, iBS_STORE_ACTIVE)
			//NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			CLEAR_BIT(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "  -->  CONTACT REQUESTS - LAUNCH_STORE_CASH_ALERT - CLEARED")
		ENDIF
		EXIT
	ENDIF
	
	//Cleanup if another Custom Menu is displaying before ours
	IF NOT IS_BIT_SET(iLocalBS,LOCAL_BS_STOP_CUSTOM_MENU_CHECK)
		IF IS_CUSTOM_MENU_ON_SCREEN()
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: CUSTOM MENU ALREADY ON SCREEN EXIT HANDLE_PROPERTY_OPTIONS_MENU")
			RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
			EXIT
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_AcceptedInvite)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT:  biITA_AcceptedInvite aborting menu")
		RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
		EXIT
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_USING_PROPERTY)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: PROPERTY_BROADCAST_BS_PLAYER_USING_PROPERTY true aborting")
		RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
		EXIT
	ENDIF

	IF LOAD_MENU_ASSETS()
		
		//Stop Doing Cutsom Menu Check
		IF NOT IS_BIT_SET(iLocalBS,LOCAL_BS_STOP_CUSTOM_MENU_CHECK)
			SET_BIT(iLocalBS,LOCAL_BS_STOP_CUSTOM_MENU_CHECK)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: LOCAL_BS_STOP_CUSTOM_MENU_CHECK set")
		ENDIF
		IF NOT IS_BIT_SET(iLocalBS,LOCAL_BS_SETUP_MENU)
			CLEAR_HELP()
			SETUP_PROPERTY_OPTIONS_MENU(iCurrentMenuType)
			SET_BIT(iLocalBS,LOCAL_BS_SETUP_MENU)
			HANDLE_MENU_DESCRIPTION(iCurrentMenuType)
			DRAW_MENU()
		ELSE
			IF NOT IS_PAUSE_MENU_ACTIVE()
			AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
				IF NOT IS_BIT_SET(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_PRESSED_ACCEPT)
					IF (IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT) OR bCursorAccept = TRUE)
					AND CAN_PLAYER_USE_MENU_INPUT()
					AND NOT bInputUsedThisFrame
						bInputUsedThisFrame = TRUE
						IF globalPropertyEntryData.bLeftOrRejectedBuzzer
						AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_MP_PROPERTY_INT")) > 0
							CDEBUG1LN(DEBUG_SAFEHOUSE, "Not allowing accept while previous script is terminating")
							HANDLE_MENU_DESCRIPTION(iCurrentMenuType)
							DRAW_MENU()
							EXIT 
						ENDIF
						SWITCH iCurrentMenuType 
							CASE MENU_TYPE_BUZZER
								IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_MP_PROPERTY_INT")) > 0
									CDEBUG1LN(DEBUG_SAFEHOUSE, "Not allowing accept while previous script is terminating-b")
									HANDLE_MENU_DESCRIPTION(iCurrentMenuType)
									DRAW_MENU()
									EXIT 
								ENDIF
								SET_BIT(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_PRESSED_ACCEPT)
								IF tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel] = PLAYER_ID()
								AND tempPropertyExtMenu.iPropertyID[tempPropertyExtMenu.iCurVertSel] > 0
									PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FREEMODE_SOUNDSET")
//									#IF FEATURE_HEIST_PLANNING
//									IF ownedDetails.iNumProperties >= 2
//									AND PROPERTY_NEAR_PLYS_OPT_AVAILABLE(mpYachts[iCurrentYacht].yachtPropertyDetails.vBlipLocation[iEntrancePlayerIsUsing],tempPropertyExtMenu.bFriendsNearby,PROPERTY_YACHT_APT_1_BASE,iEntrancePlayerIsUsing,DEFAULT,iCurrentYacht)
//										tempPropertyExtMenu.iExitSelection = tempPropertyExtMenu.iPropertyID[tempPropertyExtMenu.iCurVertSel]
//										HANDLE_MENU_DESCRIPTION(iCurrentMenuType)
//										DRAW_MENU()
//										iCurrentMenuType = MENU_TYPE_PROPERTY_ENTRY_OPTIONS
//										CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_MENU)
//										tempPropertyExtMenu.iCurVertSel = 0
//										EXIT 
//									ELSE
//									#ENDIF
										NET_PRINT("HANDLE_PROPERTY_OPTIONS_MENU player used buzzer menu and choose to enter their own house") NET_NL()
										CDEBUG1LN(DEBUG_SAFEHOUSE, "Player entered yacht # ", iCurrentYacht)
										GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0
										CDEBUG1LN(DEBUG_SAFEHOUSE, "STAGE_SETUP_FOR_USING_PROPERTY: GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0")
										DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
										IF tempPropertyExtMenu.iCurVertSel = tempPropertyExtMenu.iAllNearby
											SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_INVITE_NEARBY_PLAYERS_INSIDE)
											BROADCAST_PROPERTY_NEARBY_PLAYERS_INVITE_EVENT(PLAYERS_IN_RANGE_OF_POINT(GET_PLAYER_COORDS(PLAYER_ID()),NORMAL_APARTMENT_ENTRY_DISTANCE,TRUE),PROPERTY_YACHT_APT_1_BASE,-1,iCurrentYacht)
										ELIF tempPropertyExtMenu.iCurVertSel = tempPropertyExtMenu.iAllFriendsCrew
											BROADCAST_PROPERTY_NEARBY_PLAYERS_INVITE_EVENT(PLAYERS_IN_RANGE_OF_POINT(GET_PLAYER_COORDS(PLAYER_ID()),NORMAL_APARTMENT_ENTRY_DISTANCE,FALSE,TRUE,TRUE),PROPERTY_YACHT_APT_1_BASE,-1,iCurrentYacht)
											SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_INVITE_CREW_FRIEND_INSIDE)
										ELIF tempPropertyExtMenu.iCurVertSel = tempPropertyExtMenu.iOrganisationNearby
											BROADCAST_PROPERTY_NEARBY_PLAYERS_INVITE_EVENT(PLAYERS_IN_RANGE_OF_POINT(GET_PLAYER_COORDS(PLAYER_ID()),NORMAL_APARTMENT_ENTRY_DISTANCE,FALSE,FALSE,FALSE,DEFAULT,DEFAULT,TRUE),PROPERTY_YACHT_APT_1_BASE,-1,iCurrentYacht)
											SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_INVITE_ORGANISATION_INSIDE)
										ENDIF
										SET_LOCAL_STAGE(STAGE_SETUP_FOR_USING_PROPERTY)
//									#IF FEATURE_HEIST_PLANNING
//									ENDIF
//									#ENDIF
								ELSE
									IF iBuzzerID = -1
										iBuzzerID = GET_SOUND_ID()
										PLAY_SOUND_FRONTEND(iBuzzerID , "DOOR_BUZZ","MP_PLAYER_APARTMENT")
									ELSE
										IF HAS_SOUND_FINISHED(iBuzzerID)
											PLAY_SOUND_FRONTEND(iBuzzerID , "DOOR_BUZZ","MP_PLAYER_APARTMENT")
										ENDIF
									ENDIF
								
									CDEBUG1LN(DEBUG_SAFEHOUSE, "PLAY_SOUND_FRONTEND  called")
									IF NOT IS_BIT_SET(iLocalBS,LOCAL_BS_AWAITING_BUZZER_RESPONSE)
										IF tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel] != INVALID_PLAYER_INDEX()
										AND IS_NET_PLAYER_OK(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel])
											IF NOT IS_BIT_SET(iRejectedEntryBS,NATIVE_TO_INT(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel]) )
											AND (GlobalplayerBD_FM[NATIVE_TO_INT(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel])].propertyDetails.iCurrentlyInsideProperty > 0) // added by Neil F. for 1822930

												IF iCurrentYacht > 0
													SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS,NATIVE_TO_INT(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel]))
													SET_BIT(iLocalBS,LOCAL_BS_AWAITING_BUZZER_RESPONSE)
													CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_PROPERTY_OPTIONS_MENU: GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = ",GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS)
													INT instance  
											        

													instance = iCurrentYacht

													IF instance > 0
													
													ENDIF
													CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_PROPERTY_OPTIONS_MENU: set in voice session #",FM_VOICE_CHANNEL_PROPERTY0+instance) 
			                						globalPropertyEntryData.ownerID = tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel]
													globalPropertyEntryData.ownerHandle = GET_GAMER_HANDLE_PLAYER(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel])
													globalPropertyEntryData.iPropertyEntered =  GlobalplayerBD_FM[NATIVE_TO_INT(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel])].propertyDetails.iCurrentlyInsideProperty
													globalPropertyEntryData.iYachtEntered = iCurrentYacht
													globalPropertyEntryData.iEntrance = 0
													PRINTLN("4 globalPropertyEntryData.iPropertyEntered = ",globalPropertyEntryData.iPropertyEntered)
													CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_PROPERTY_OPTIONS_MENU: setting owner as ",NATIVE_TO_INT(globalPropertyEntryData.ownerID))
													SET_BIT(iLocalBS2,LOCAL_BS2_bEnteringAnotherPlayerProperty)
													CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_PROPERTY_OPTIONS_MENU: player asking permission to enter yacht #",iCurrentYacht)
													SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_BUZZER_ENTER_HOUSE)
													globalPropertyEntryData.iPropertyEntered = PROPERTY_YACHT_APT_1_BASE
													PRINTLN("3 globalPropertyEntryData.iPropertyEntered = ",globalPropertyEntryData.iPropertyEntered)
													//SET_BIT(ilocalBS,LOCAL_BS_BUZZER_TRIGGERED_LOAD_SCENE)
													NETWORK_SET_TALKER_PROXIMITY(0.0)
													CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_PROPERTY_OPTIONS_MENU: TURNING OFF PROXIMITY CHAT FOR BUZZER MENU!!")
													CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_PROPERTY_OPTIONS_MENU: set in tutorial session #",instance, " to talk on buzzer") 

													REINIT_NET_TIMER(buzzerTimer)
													CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_PROPERTY_OPTIONS_MENU player has requested entry into property owned by ", GET_PLAYER_NAME(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel]))
												ELSE
													SET_LOCAL_STAGE(STAGE_LEAVE_ENTRANCE_AREA)
													IF IS_BIT_SET(iLocalBS,LOCAL_BS_AWAITING_BUZZER_RESPONSE)
														//NETWORK_SET_TALKER_PROXIMITY(CHAT_PROXIMITY_FM)
														globalPropertyEntryData.ownerID = INVALID_PLAYER_INDEX()
														GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0
														CDEBUG1LN(DEBUG_SAFEHOUSE, "GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0")
														CDEBUG1LN(DEBUG_SAFEHOUSE, "globalPropertyEntryData.bLeftOrRejectedBuzzer 4 = TRUE")
														NETWORK_CLEAR_VOICE_CHANNEL()
														RESET_NET_TIMER(buzzerTimer)
														globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE
													ENDIF
													CLEAR_BIT(iLocalBS,LOCAL_BS_AWAITING_BUZZER_RESPONSE)
													CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_PROPERTY_OPTIONS_MENU: Aborting iCurrentYacht = ",iCurrentYacht)
													EXIT
												ENDIF
											ELSE
												SETUP_PROPERTY_MENU_DESCRIPTION_BIT(MP_PROP_MENU_DES_REJECTED_ENTRY)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							BREAK
							CASE MENU_TYPE_PROPERTY_ENTRY_OPTIONS
								IF tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel] = PLAYER_ID()
									PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FREEMODE_SOUNDSET")
									NET_PRINT("HANDLE_PROPERTY_OPTIONS_MENU player used MENU_TYPE_PROPERTY_ENTRY_OPTIONS menu and choose to enter their own house") NET_NL()
									CDEBUG1LN(DEBUG_SAFEHOUSE, "Player entered property # ", iCurrentYacht)
									GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0
									CDEBUG1LN(DEBUG_SAFEHOUSE, "STAGE_SETUP_FOR_USING_PROPERTY: GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0")
									DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
									IF tempPropertyExtMenu.iCurVertSel = tempPropertyExtMenu.iAllNearby
										SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_INVITE_NEARBY_PLAYERS_INSIDE)
										BROADCAST_PROPERTY_NEARBY_PLAYERS_INVITE_EVENT(PLAYERS_IN_RANGE_OF_POINT(GET_PLAYER_COORDS(PLAYER_ID()),NORMAL_APARTMENT_ENTRY_DISTANCE,TRUE),PROPERTY_YACHT_APT_1_BASE,-1,iCurrentYacht)
									ELIF tempPropertyExtMenu.iCurVertSel = tempPropertyExtMenu.iAllFriendsCrew
										BROADCAST_PROPERTY_NEARBY_PLAYERS_INVITE_EVENT(PLAYERS_IN_RANGE_OF_POINT(GET_PLAYER_COORDS(PLAYER_ID()),NORMAL_APARTMENT_ENTRY_DISTANCE,FALSE,TRUE,TRUE),PROPERTY_YACHT_APT_1_BASE,-1,iCurrentYacht)
										SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_INVITE_CREW_FRIEND_INSIDE)
									ELIF tempPropertyExtMenu.iCurVertSel = tempPropertyExtMenu.iOrganisationNearby
										BROADCAST_PROPERTY_NEARBY_PLAYERS_INVITE_EVENT(PLAYERS_IN_RANGE_OF_POINT(GET_PLAYER_COORDS(PLAYER_ID()),NORMAL_APARTMENT_ENTRY_DISTANCE,FALSE,FALSE,FALSE,DEFAULT,DEFAULT,TRUE),PROPERTY_YACHT_APT_1_BASE,-1,iCurrentYacht)
										SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_INVITE_ORGANISATION_INSIDE)
									ENDIF
									
									SET_LOCAL_STAGE(STAGE_SETUP_FOR_USING_PROPERTY)
								ENDIF
							BREAK
							
						ENDSWITCH
						SET_BIT(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_PRESSED_ACCEPT)
					ENDIF
				ELSE
					IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
					OR (bUsingCursor AND bCursorAccept = FALSE)
						CLEAR_BIT(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_PRESSED_ACCEPT)
					ENDIF
				ENDIF
				IF NOT IS_BIT_SET(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_PRESSED_CANCEL)
					IF (IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL))
					AND CAN_PLAYER_USE_MENU_INPUT()
					AND NOT bInputUsedThisFrame
						bInputUsedThisFrame = TRUE
						PLAY_SOUND_FRONTEND(-1, "CANCEL","HUD_FREEMODE_SOUNDSET")
						IF NOT IS_BIT_SET(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
							NET_PRINT("HANDLE_PROPERTY_OPTIONS_MENU player exited menu") NET_NL()
							globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE. Exited menu")
							SWITCH iCurrentMenuType 
								CASE MENU_TYPE_BUZZER
									//SETUP_SPECIFIC_SPAWN_LOCATION(mpYachts[iCurrentYacht].yachtPropertyDetails.house.exits[0].vOutPlayerLoc,mpYachts[iCurrentYacht].yachtPropertyDetails.house.exits[0].fOutPlayerHeading, 10, FALSE)
									SET_LOCAL_STAGE(STAGE_LEAVE_ENTRANCE_AREA)
									IF IS_BIT_SET(iLocalBS,LOCAL_BS_AWAITING_BUZZER_RESPONSE)
										//NETWORK_SET_TALKER_PROXIMITY(CHAT_PROXIMITY_FM)
										globalPropertyEntryData.ownerID = INVALID_PLAYER_INDEX()
										CDEBUG1LN(DEBUG_SAFEHOUSE, "globalPropertyEntryData.bLeftOrRejectedBuzzer 5 = TRUE")
										GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0
										CDEBUG1LN(DEBUG_SAFEHOUSE, "GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0")
										NETWORK_CLEAR_VOICE_CHANNEL()
										RESET_NET_TIMER(buzzerTimer)
										globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE
									ENDIF
									CLEAR_BIT(iLocalBS,LOCAL_BS_AWAITING_BUZZER_RESPONSE)
									EXIT
								BREAK
								CASE MENU_TYPE_PROPERTY_ENTRY_OPTIONS
									HANDLE_MENU_DESCRIPTION(iCurrentMenuType)
									DRAW_MENU()
									iCurrentMenuType = MENU_TYPE_BUZZER
									CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_MENU)
									EXIT
								BREAK
							ENDSWITCH
							EXIT
						ELSE
							NET_PRINT("HANDLE_PROPERTY_OPTIONS_MENU pressed cancel on menu") NET_NL()
							CLEAR_BIT(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
						ENDIF
						SET_BIT(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_PRESSED_CANCEL)
					ENDIF
				ELSE
					IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
						CLEAR_BIT(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_PRESSED_CANCEL)
					ENDIF
				ENDIF
				
				iTempSelectionVert = tempPropertyExtMenu.iCurVertSel
				IF tempPropertyExtMenu.iMaxVertSel > 0
				AND NOT IS_BIT_SET(iLocalBS,LOCAL_BS_AWAITING_BUZZER_RESPONSE)
				AND NOT IS_BIT_SET(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
					IF NOT IS_BIT_SET(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_PRESSED_UP)
						IF (IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_UP)
						OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_UP)
						OR (IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND iCurrentMenuType = MENU_TYPE_BUZZER AND mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[iEntrancePlayerIsUsing].iType != ENTRANCE_TYPE_GARAGE AND IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)))
						AND NOT bInputUsedThisFrame
							bInputUsedThisFrame = TRUE
							tempPropertyExtMenu.iCurVertSel--
							PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN","HUD_FREEMODE_SOUNDSET")
							SET_BIT(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_PRESSED_UP)
							RESET_NET_TIMER(tempPropertyExtMenu.menuNavigationDelay)
						ENDIF
					ELSE
						IF HANDLE_PROPERTY_OPTIONS_MENU_NAVIGATION_DELAY(INPUT_FRONTEND_UP)
							CLEAR_BIT(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_PRESSED_UP)	
						ENDIF
					ENDIF
					IF NOT IS_BIT_SET(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_PRESSED_DOWN)
						IF (IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_DOWN)
						OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_DOWN)
						OR (IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND iCurrentMenuType = MENU_TYPE_BUZZER AND mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[iEntrancePlayerIsUsing].iType != ENTRANCE_TYPE_GARAGE AND IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)))
						AND NOT bInputUsedThisFrame
							bInputUsedThisFrame = TRUE
							tempPropertyExtMenu.iCurVertSel++
							PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN","HUD_FREEMODE_SOUNDSET")
							SET_BIT(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_PRESSED_DOWN)
							RESET_NET_TIMER(tempPropertyExtMenu.menuNavigationDelay)
						ENDIF
					ELSE
						IF HANDLE_PROPERTY_OPTIONS_MENU_NAVIGATION_DELAY(INPUT_FRONTEND_DOWN)
							CLEAR_BIT(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_PRESSED_DOWN)	
						ENDIF
					ENDIF
					
					IF tempPropertyExtMenu.iCurVertSel > tempPropertyExtMenu.iMaxVertSel-1
						tempPropertyExtMenu.iCurVertSel = 0
					ENDIF
					IF tempPropertyExtMenu.iCurVertSel < 0
						tempPropertyExtMenu.iCurVertSel = tempPropertyExtMenu.iMaxVertSel -1
					ENDIF
				
					IF tempPropertyExtMenu.iCurVertSel != iTempSelectionVert
						tempPropertyExtMenu.iMenuDescriptionBS = 0
						CDEBUG1LN(DEBUG_SAFEHOUSE, "Updating menu vertical selection changed")
						CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_MENU)
					ENDIF
				ENDIF
				
				HANDLE_MENU_DESCRIPTION(iCurrentMenuType)
				DRAW_MENU()
			ENDIF
			IF SHOULD_MENU_UPDATE()
				CDEBUG1LN(DEBUG_SAFEHOUSE, "Updating menu SHOULD_MENU_UPDATE() = true")
				CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_MENU)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_TASK_ONGOING(PED_INDEX thisPed, SCRIPT_TASK_NAME thisTaskName)
	IF GET_SCRIPT_TASK_STATUS(thisPed, thisTaskName) = PERFORMING_TASK
	OR GET_SCRIPT_TASK_STATUS(thisPed, thisTaskName) = WAITING_TO_START_TASK
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VEHICLE_ROUGHLY_FACING_THIS_DIRECTION(VEHICLE_INDEX veh, FLOAT idealHeading, FLOAT acceptableRange = 30.0)
	FLOAT upperLimit, lowerLimit
	upperLimit = idealHeading + (acceptableRange/2)
	IF upperLimit > 360
		upperLimit -= 360.0
	ENDIF
	
	lowerLimit = idealHeading - (acceptableRange/2)
	IF lowerLimit < 0
		lowerLimit += 360.0
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(veh)
		IF upperLimit > lowerLimit
			IF GET_ENTITY_HEADING(veh) < upperLimit
			AND GET_ENTITY_HEADING(veh) > lowerLimit
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		ELSE
			IF GET_ENTITY_HEADING(veh) < upperLimit
			OR GET_ENTITY_HEADING(veh) > lowerLimit
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANYONE_TRYING_TO_ENTER_CURRENT_VEHICLE()
	VEHICLE_INDEX theVeh
	PLAYER_INDEX vehiclePlayer
	INT i
	VECTOR vCoords
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			theVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF IS_VEHICLE_DRIVEABLE(theVeh)
				vCoords = GET_PLAYER_COORDS(PLAYER_ID())
				REPEAT NUM_NETWORK_PLAYERS i
					vehiclePlayer = INT_TO_PLAYERINDEX(i)
					IF vehiclePlayer != PLAYER_ID()
						IF IS_NET_PLAYER_OK(vehiclePlayer,TRUE,FALSE)
							IF GET_DISTANCE_BETWEEN_COORDS(vCoords,GET_PLAYER_COORDS(vehiclePlayer)) <= 125
								IF GET_VEHICLE_PED_IS_ENTERING(GET_PLAYER_PED(vehiclePlayer)) = theVeh
									RETURN TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC CLEAR_VEHICLE_OF_PROJECTILES()
	VEHICLE_INDEX vehClear
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			vehClear = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF IS_VEHICLE_DRIVEABLE(vehClear)
				IF HAS_VEHICLE_GOT_PROJECTILE_ATTACHED(PLAYER_PED_ID(), vehClear)
					CLEAR_AREA_OF_PROJECTILES(GET_ENTITY_COORDS(vehClear), 5.0)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HANDLE_WARP_INTO_HOUSE()
	SWITCH warpInControl.iStage
		CASE PROP_ENTER_STAGE_INIT
			IF IS_BIT_SET(iLocalBS,LOCAL_BS_JUST_BOUGHT_PROPERTY)
				IF IS_NET_PLAYER_OK(PLAYER_ID())
					warpInControl.vehID = GET_PLAYERS_LAST_VEHICLE()
					IF NOT DOES_ENTITY_EXIST(warpInControl.vehID)
						SET_PROPERTY_ENTER_STAGE(PROP_ENTER_STAGE_NO_VEHICLE)
						RETURN FALSE
					ENDIF
					IF NOT IS_VEHICLE_DRIVEABLE(warpInControl.vehID)
						SET_PROPERTY_ENTER_STAGE(PROP_ENTER_STAGE_NO_VEHICLE)
						RETURN FALSE
					ENDIF

					SET_PROPERTY_ENTER_STAGE(PROP_ENTER_STAGE_NO_VEHICLE)
					RETURN FALSE
				ELSE
					SET_BIT(ilocalBS,LOCAL_BS_CLEANUP_MP_PROPERTIES)
				ENDIF
			ELSE
				SET_PROPERTY_ENTER_STAGE(PROP_ENTER_STAGE_NO_VEHICLE)
			ENDIF
		BREAK
		CASE PROP_ENTER_STAGE_NO_VEHICLE
			IF NOT IS_BIT_SET(ilocalBS,LOCAL_BS_STARTED_FADE)
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				OR IS_BIT_SET(ilocalBS,LOCAL_BS_ENTER_INGORE_PLAYER_OK)
					IF NOT IS_BIT_SET(ilocalBS,LOCAL_BS_SKIP_ENTER_FADE)
						SET_BIT(ilocalBS,LOCAL_BS_NO_FADE)
					ENDIF
					IF NOT IS_BIT_SET(ilocalBS,LOCAL_BS_NO_FADE)
						IF IS_BIT_SET(ilocalBS,LOCAL_BS_SKIP_ENTER_FADE)
							DO_SCREEN_FADE_OUT(0)
						ELSE
							DO_SCREEN_FADE_OUT(500)
						ENDIF
					ENDIF
					CDEBUG1LN(DEBUG_SAFEHOUSE, "Entering house property ID is # ", iCurrentYacht)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_HOUSE: setting fade started") 
					SET_BIT(ilocalBS,LOCAL_BS_STARTED_FADE)
					//IF GET_PROPERTY_SIZE_TYPE(iCurrentYacht) = PROP_SIZE_TYPE_LARGE_APT
					//	SET_BIT(ilocalBS,LOCAL_BS_BUZZER_TRIGGERED_LOAD_SCENE)
					//ENDIF
				ELSE
					SET_BIT(ilocalBS,LOCAL_BS_CLEANUP_MP_PROPERTIES)
				ENDIF
			ELSE
				//Rowan c, fix for PT 1449237
				IF IS_BIT_SET(ilocalBS,LOCAL_BS_NO_FADE)
				OR IS_SCREEN_FADED_OUT()
				OR IS_BIT_SET(ilocalBS,LOCAL_BS_STARTED_CUTSCENE)
					//SET_BIT(g_iExteriorScriptProcessingWarpBS,0)
					IF NOT IS_BIT_SET(ilocalBS,LOCAL_BS_STARTED_CUTSCENE)
						IF NOT IS_BIT_SET(iMP_Prop_RetainBS,MP_PROP_RETAIN_VEH_TRUE)
							SET_BIT(iMP_Prop_RetainBS,MP_PROP_RETAIN_VEH_TRUE)
						ELSE
							IF IS_BIT_SET( iMP_Prop_RetainBS,MP_PROP_RETAIN_VEH_RETAINED)
								SET_BIT(ilocalBS,LOCAL_BS_STARTED_CUTSCENE)
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_BIT_SET(ilocalBS,LOCAL_BS_WARPED_IN_PROPERTY)
							IF NOT IS_BIT_SET(ilocalBS,LOCAL_BS_ENTER_NO_WARP)
								IF IS_BIT_SET(ilocalBS,LOCAL_BS_BUZZER_TRIGGERED_LOAD_SCENE)
									IF (NOT IS_NEW_LOAD_SCENE_ACTIVE() OR IS_NEW_LOAD_SCENE_LOADED())
									//AND IS_INTERIOR_READY(theInterior)
										IF NOT IS_BIT_SET(iLocalBS2,LOCAL_BS2_bEnteringAnotherPlayerProperty)
											SET_ENTITY_COORDS(PLAYER_PED_ID(),mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[0].vInPlayerLoc)
											SET_ENTITY_HEADING(PLAYER_PED_ID(),mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[0].fInPlayerHeading)
										ELSE
											SET_ENTITY_COORDS(PLAYER_PED_ID(),mpYachts[iCurrentYacht].yachtPropertyDetails.house.buzzerEnter.vOutsidePlayerPos)
											SET_ENTITY_HEADING(PLAYER_PED_ID(),mpYachts[iCurrentYacht].yachtPropertyDetails.house.buzzerEnter.fOutsidePlayerHeading)
											ENDIF
										SET_BIT(ilocalBS,LOCAL_BS_WARPED_IN_PROPERTY)
										CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: warp into house interior has completed (loaded scene outside)")
									#IF IS_DEBUG_BUILD
									ELSE
										IF IS_NEW_LOAD_SCENE_LOADED()
											CDEBUG1LN(DEBUG_SAFEHOUSE, "waiting for new load scene- 125")
										ENDIF
									#ENDIF
									ENDIF
								ELSE
									IF globalPropertyEntryData.bInteriorWarpDone
										SET_BIT(ilocalBS,LOCAL_BS_WARPED_IN_PROPERTY)
										CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: warp into house interior has completed at outside of interior")
									ENDIF
								ENDIF
							ELSE
								SET_BIT(ilocalBS,LOCAL_BS_WARPED_IN_PROPERTY)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: house warp no warp flag set finishing")
							ENDIF
						ELSE
							IF IS_BIT_SET(ilocalBS,LOCAL_BS_BUZZER_TRIGGERED_LOAD_SCENE)
								NEW_LOAD_SCENE_STOP()
							ENDIF
							RETURN TRUE
						ENDIF
					ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_HOUSE: stuck waiting for fade") 
				#ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
			
	RETURN FALSE
ENDFUNC

PROC LOOP_ENTRANCES()
	INT j
	FLOAT fDistance
	BOOL bLocalPlayerOwnsThisYacht
	IF NOT IS_PLAYER_TELEPORT_ACTIVE()
	AND NOT IS_PLAYER_IN_CORONA()
	AND NOT IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
		IF GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID()) = iCurrentYacht
			bLocalPlayerOwnsThisYacht = TRUE
		ENDIF
		REPEAT mpYachts[iCurrentYacht].yachtPropertyDetails.iNumEntrances j
			iEntrancePlayerIsUsing = j
			fDistance = GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()),mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[j].vEntranceMarkerLoc)
			IF fDistance <= 30
				
				IF DOES_ANYONE_IN_GAME_OWN_YACHT(iKnowOwnerState,iCurrentYacht)
				OR bLocalPlayerOwnsThisYacht
				#IF IS_DEBUG_BUILD
				OR bDebugTestBuzzer
				#ENDIF
					DRAW_BUZZER_LOCATE(j)
					IF IS_PLAYER_IN_BUZZER_LOCATION(j)//,iKnowOwnerState)
						IF (bLocalPlayerOwnsThisYacht
						AND iKnowOwnerState= 0)
							#IF IS_DEBUG_BUILD
							IF db_bFullDebug
								CDEBUG1LN(DEBUG_SAFEHOUSE, "LOOP_ENTRANCES - ABC: iKnowOwnerState = ",iKnowOwnerState)
							ENDIF
							#ENDIF
							IF NOT IS_ENTERING_DISABLED_DUE_TO_EXIT_WARP()
								IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) <= 0
								OR ARE_PLAYER_STARS_GREYED_OUT(PLAYER_ID()))
								AND NOT g_bDisablePropertyAccessForLoseCopMissionObj
									IF IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
									OR IS_PLAYER_BLOCKED_FROM_PROPERTY()
										IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
											IF NOT IS_BIT_SET(ilocalBS, LOCAL_BS_WANTED_HELP_TEXT)
												IF IS_PLAYER_BLOCKED_FROM_PROPERTY()
													PRINT_REASON_PLAYER_IS_BLOCKED_FROM_YACHT(FALSE)
												ELSE
													PRINT_HELP("CUST_YAC_MISO")
												ENDIF	
												
												SET_BIT(iLocalBS, LOCAL_BS_WANTED_HELP_TEXT)
											ENDIF
										ENDIF
									ELSE
										CLEAR_BIT(ilocalBS, LOCAL_BS_WANTED_HELP_TEXT)
										PRINTLN("MAINTAIN_MP_PROPERTIES: Player is entering their property entrance #",iEntrancePlayerIsUsing," no one known owns it as well or garage (BUZZER)")
										PRINTLN("AM_MP_YACHT: Nearby location: ",mpYachts[iCurrentYacht].yachtPropertyDetails.vBlipLocation[iEntrancePlayerIsUsing])
										IF (mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_HOUSE
										AND PROPERTY_NEAR_PLYS_OPT_AVAILABLE(mpYachts[iCurrentYacht].yachtPropertyDetails.vBlipLocation[iEntrancePlayerIsUsing],tempPropertyExtMenu.bFriendsNearby, tempPropertyExtMenu.bOrganisationNearby,  PROPERTY_YACHT_APT_1_BASE,iEntrancePlayerIsUsing,DEFAULT,iCurrentYacht))
										#IF IS_DEBUG_BUILD
										OR bDebugTestBuzzer 
										#ENDIF
											SET_LOCAL_STAGE(STAGE_SETUP_FOR_MENU)
											iCurrentMenuType = MENU_TYPE_BUZZER
											CDEBUG1LN(DEBUG_SAFEHOUSE, "Player entered yacht # ", iCurrentYacht)
										ELSE
											SET_LOCAL_STAGE(STAGE_SETUP_FOR_USING_PROPERTY)
											CDEBUG1LN(DEBUG_SAFEHOUSE, "Player entered yacht # ", iCurrentYacht)
										ENDIF
										DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
										
										globalPropertyEntryData.ownerID = INVALID_PLAYER_INDEX()
										CLEANUP_MENU_ASSETS()
										EXIT
									ENDIF
								ELSE
									IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
										IF NOT IS_BIT_SET(ilocalBS, LOCAL_BS_WANTED_HELP_TEXT)
											IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
												PRINT_HELP("CUST_YACHT_WH")
											ELIF g_bDisablePropertyAccessForLoseCopMissionObj
												PRINT_HELP("PROP_YACHT_HEI_WOH")
											ENDIF
											SET_BIT(iLocalBS, LOCAL_BS_WANTED_HELP_TEXT)
										ENDIF
									ENDIF
								ENDIF
							ELSE
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: can't enter buzzer as ")
							ENDIF
						ELSE
							iEntrancePlayerIsUsing = j
							#IF IS_DEBUG_BUILD
							IF db_bFullDebug
								CDEBUG1LN(DEBUG_SAFEHOUSE, "LOOP_ENTRANCES - DEF")
							ENDIF
							#ENDIF
							IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) <= 0
							OR ARE_PLAYER_STARS_GREYED_OUT(PLAYER_ID()))
							AND NOT g_bDisablePropertyAccessForLoseCopMissionObj
								IF IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
								OR IS_PLAYER_BLOCKED_FROM_PROPERTY()
									//CDEBUG1LN(DEBUG_SAFEHOUSE, "CDM: mission critical!")
									IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
										IF NOT IS_BIT_SET(ilocalBS, LOCAL_BS_WANTED_HELP_TEXT)
											IF IS_PLAYER_BLOCKED_FROM_PROPERTY()
												PRINT_REASON_PLAYER_IS_BLOCKED_FROM_YACHT(FALSE)
											ELSE
												PRINT_HELP("CUST_YAC_MISO")
											ENDIF
											SET_BIT(iLocalBS, LOCAL_BS_WANTED_HELP_TEXT)
										ENDIF
									ENDIF
								ELSE
									//CDEBUG1LN(DEBUG_SAFEHOUSE, "CDM: ok for context")
									IF iBuzzerContextIntention = NEW_CONTEXT_INTENTION
										IF mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[j].iType = ENTRANCE_TYPE_HOUSE
											REGISTER_CONTEXT_INTENTION(iBuzzerContextIntention,CP_MEDIUM_PRIORITY,"MP_BUZZ2_Y")
										ELSE
											REGISTER_CONTEXT_INTENTION(iBuzzerContextIntention,CP_MEDIUM_PRIORITY,"MP_BUZZ2_Y")
										ENDIF
										CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: registered context intention for buzzer")
									ENDIF
									IF HAS_CONTEXT_BUTTON_TRIGGERED(iBuzzerContextIntention)
										//CDEBUG1LN(DEBUG_SAFEHOUSE, "someone in game owns property")
										CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: Player is starting enter menu as someone known owns it as well") NET_NL()
										CDEBUG1LN(DEBUG_SAFEHOUSE, "Player starting enter menu yacht # ", iCurrentYacht)
										j = mpYachts[iCurrentYacht].yachtPropertyDetails.iNumEntrances
										RELEASE_CONTEXT_INTENTION(iBuzzerContextIntention)
										SET_LOCAL_STAGE(STAGE_SETUP_FOR_MENU)
										iCurrentMenuType = MENU_TYPE_BUZZER
										CLEANUP_MENU_ASSETS()
										EXIT
									ENDIF
								ENDIF
							ELSE
								//CDEBUG1LN(DEBUG_SAFEHOUSE, "CDM: wanted! = ")
								IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
									IF NOT IS_BIT_SET(ilocalBS, LOCAL_BS_WANTED_HELP_TEXT)
										IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
											PRINT_HELP("CUST_YACHT_WH")
										ELIF g_bDisablePropertyAccessForLoseCopMissionObj
											PRINT_HELP("PROP_YACHT_HEI_WOH")
										ENDIF
										SET_BIT(ilocalBS, LOCAL_BS_WANTED_HELP_TEXT)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF fDistance <= 10
				//IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(),mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[j].vBuzzerLoc,<<1, 1, 2>>,FALSE,TRUE,TM_ON_FOOT)
					iEntrancePlayerIsUsing = j
					IF bLocalPlayerOwnsThisYacht //DOES_LOCAL_PLAYER_OWN_PROPERTY_IN_BUILDING(mpYachts[iCurrentYacht].iBuildingID)
						IF IS_PLAYER_ENTERING_THE_PROPERTY()
						AND NOT IS_PLAYER_IN_BUZZER_LOCATION(iEntrancePlayerIsUsing)

							IF bLocalPlayerOwnsThisYacht
								IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) <= 0
								OR ARE_PLAYER_STARS_GREYED_OUT(PLAYER_ID()))
								AND NOT g_bDisablePropertyAccessForLoseCopMissionObj

									IF IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
									OR IS_PLAYER_BLOCKED_FROM_PROPERTY()
										IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
											IF NOT IS_BIT_SET(ilocalBS, LOCAL_BS_WANTED_HELP_TEXT)
												IF IS_PLAYER_BLOCKED_FROM_PROPERTY()
													PRINT_REASON_PLAYER_IS_BLOCKED_FROM_YACHT(FALSE)
												ELSE
													PRINT_HELP("CUST_YAC_MISO")
												ENDIF
												SET_BIT(iLocalBS, LOCAL_BS_WANTED_HELP_TEXT)
											ENDIF
										ENDIF
									ELSE
										CLEAR_BIT(ilocalBS, LOCAL_BS_WANTED_HELP_TEXT)
										PRINTLN("MAINTAIN_MP_PROPERTIES: Player is entering their property entrance #",iEntrancePlayerIsUsing," no one known owns it as well or garage")
										PRINTLN("AM_MP_YACHT: Nearby location: ",mpYachts[iCurrentYacht].yachtPropertyDetails.vBlipLocation[iEntrancePlayerIsUsing])
										IF (mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_HOUSE
										AND PROPERTY_NEAR_PLYS_OPT_AVAILABLE(mpYachts[iCurrentYacht].yachtPropertyDetails.vBlipLocation[iEntrancePlayerIsUsing],tempPropertyExtMenu.bFriendsNearby, tempPropertyExtMenu.bOrganisationNearby,  PROPERTY_YACHT_APT_1_BASE,iEntrancePlayerIsUsing,DEFAULT,iCurrentYacht))
											SET_BIT(iLocalBS2, LOCAL_BS2_bRunPropertySelectMenu)
											bDoNotBuzzOthers = TRUE
											SET_LOCAL_STAGE(STAGE_SETUP_FOR_MENU)
											iCurrentMenuType = MENU_TYPE_BUZZER
											CDEBUG1LN(DEBUG_SAFEHOUSE, "(nearby players)Player entered yacht # ", iCurrentYacht)
										ELSE
											SET_LOCAL_STAGE(STAGE_SETUP_FOR_USING_PROPERTY)
											CDEBUG1LN(DEBUG_SAFEHOUSE, "Player entered yacht # ", iCurrentYacht )
										ENDIF
										DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)

										NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)

										SET_BIT(iLocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL)
										
										globalPropertyEntryData.ownerID = INVALID_PLAYER_INDEX()
										CLEANUP_MENU_ASSETS()
										EXIT
									ENDIF
								ELSE
									IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
										IF NOT IS_BIT_SET(ilocalBS, LOCAL_BS_WANTED_HELP_TEXT)
											IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
												PRINT_HELP("CUST_YACHT_WH")
											ELIF g_bDisablePropertyAccessForLoseCopMissionObj

												PRINT_HELP("PROP_YACHT_HEI_WOH")
											ENDIF
											SET_BIT(iLocalBS, LOCAL_BS_WANTED_HELP_TEXT)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
			ENDIF
		ENDREPEAT
	#IF IS_DEBUG_BUILD
	ELSE
		IF db_bFullDebug
			IF IS_PLAYER_TELEPORT_ACTIVE()
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: Loop entrances IS_PLAYER_TELEPORT_ACTIVE TRUE ")
			ELIF IS_PLAYER_IN_CORONA()
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: Loop entrances IS_PLAYER_IN_CORONA TRUE ")
			ELIF IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: Loop entrances IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE	TRUE ")
			ENDIF
		ENDIF
	#ENDIF
	ENDIF
ENDPROC

PROC RETAIN_BUZZER_MARKER()
	FLOAT fDistance
	BOOL bLocalPlayerOwnsThisYacht
	INT j
	BOOl bDrawnBuzzer
	IF NOT IS_PLAYER_TELEPORT_ACTIVE()
	
		IF GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID()) = iCurrentYacht
			bLocalPlayerOwnsThisYacht = TRUE
		ENDIF
		REPEAT mpYachts[iCurrentYacht].yachtPropertyDetails.iNumEntrances j
			bDrawnBuzzer = FALSE
			fDistance = GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()),mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[j].vEntranceMarkerLoc)
			IF fDistance <= 30
				IF NOT bDrawnBuzzer
					IF DOES_ANYONE_IN_GAME_OWN_YACHT(iKnowOwnerState,iCurrentYacht)
					OR bLocalPlayerOwnsThisYacht
						DRAW_BUZZER_LOCATE(j)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

PROC SET_ENTRY_CS_STAGE(INT iStage)
	#IF IS_DEBUG_BUILD
	SWITCH iStage
		CASE ENTRY_CS_STAGE_INIT_DATA
			CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_ENTRY_CS_STAGE - ENTRY_CS_STAGE_INIT_DATA")
		BREAK
		CASE ENTRY_CS_STAGE_WAIT_FOR_MODELS
			CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_ENTRY_CS_STAGE - ENTRY_CS_STAGE_WAIT_FOR_MODELS")
		BREAK
		CASE ENTRY_CS_STAGE_TRIGGER_CUT
			CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_ENTRY_CS_STAGE - ENTRY_CS_STAGE_TRIGGER_CUT")
		BREAK
		CASE ENTRY_CS_STAGE_RUN_CUT
			CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_ENTRY_CS_STAGE - ENTRY_CS_STAGE_RUN_CUT")
		BREAK
		CASE ENTRY_CS_STAGE_RUN_SECOND_CUT
			CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_ENTRY_CS_STAGE - ENTRY_CS_STAGE_RUN_SECOND_CUT")
		BREAK
		CASE ENTRY_CS_STAGE_HANDLE_PLAYER_PED
			CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_ENTRY_CS_STAGE - ENTRY_CS_STAGE_HANDLE_PLAYER_PED")
		BREAK
	ENDSWITCH
	#ENDIF
	entryCutData.iStage = iStage
ENDPROC

INT iApartmentDoorSoundStage
INT iApartmentBuzzerSoundStage

PROC DO_APARTMENT_BUZZER_PRESS_SOUNDS()
	FLOAT fSoundTime 
	IF GET_PROPERTY_SIZE_TYPE(iCurrentYacht) = PROP_SIZE_TYPE_LARGE_APT

		IF NOT IS_STRING_NULL_OR_EMPTY(entryCutData.animName[SYNC_SCENE_PED_A] )
			IF ARE_STRINGS_EQUAL(entryCutData.animName[SYNC_SCENE_PED_A] ,"Buzz_Short")
				fSoundTime = 0.353
			ELIF ARE_STRINGS_EQUAL(entryCutData.animName[SYNC_SCENE_PED_A] ,"Buzz_Reg")
				fSoundTime = 0.320
			ELSE
				fSoundTime = 0.373
			ENDIF
		ENDIF
		IF entryCutData.iSyncedSceneID >= 0
			IF IS_SYNCHRONIZED_SCENE_RUNNING(entryCutData.iSyncedSceneID)
				SWITCH iApartmentBuzzerSoundStage
					CASE 0	//Door starts opening
						IF GET_SYNCHRONIZED_SCENE_PHASE(entryCutData.iSyncedSceneID) >= fSoundTime
							IF NOT IS_PED_INJURED(entryCutData.playerClone)
								IF entryCutData.iSoundID < 0
									entryCutData.iSoundID = GET_SOUND_ID()
								ENDIF
								PLAY_SOUND_FROM_ENTITY(entryCutData.iSoundID, "DOOR_BUZZ_ONESHOT_MASTER",entryCutData.playerClone, "GTAO_APT_DOOR_DOWNSTAIRS_GLASS_SOUNDS")
								CDEBUG1LN(DEBUG_SAFEHOUSE, "DO_APARTMENT_BUZZER_PRESS_SOUNDS- Doing buzzer one shot")
							ENDIF
							
							iApartmentBuzzerSoundStage++
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DO_APARTMENT_DOOR_SOUNDS()
	
	FLOAT fDoorOpen, fDoorShutting//, fDoorClosed
	
	IF NOT IS_STRING_NULL_OR_EMPTY(entryCutData.animDictionary)
		IF ARE_STRINGS_EQUAL(entryCutData.animDictionary,"anim@apt_trans@hinge_l")
			fDoorOpen = 0.271
			fDoorShutting = 0.411
			//fDoorClosed = 0.542
		ELSE
			fDoorOpen = 0.242
			fDoorShutting = 0.418
			//fDoorClosed = 0.598
		ENDIF
	ENDIF
	IF entryCutData.iSyncedSceneID >= 0
		IF IS_SYNCHRONIZED_SCENE_RUNNING(entryCutData.iSyncedSceneID)
			SWITCH iApartmentDoorSoundStage
				CASE 0	//Door starts opening
				
					IF GET_SYNCHRONIZED_SCENE_PHASE(entryCutData.iSyncedSceneID) >= fDoorOpen
						IF entryCutData.iSoundID < 0
							entryCutData.iSoundID = GET_SOUND_ID()
						ENDIF
						
						IF GET_PROPERTY_BUILDING(iCurrentYacht) = MP_PROPERTY_BUILDING_57	
						OR GET_PROPERTY_BUILDING(iCurrentYacht) = MP_PROPERTY_BUILDING_58	
						OR GET_PROPERTY_BUILDING(iCurrentYacht) = MP_PROPERTY_BUILDING_59	
						OR GET_PROPERTY_BUILDING(iCurrentYacht) = MP_PROPERTY_BUILDING_60	
						OR GET_PROPERTY_BUILDING(iCurrentYacht) = MP_PROPERTY_BUILDING_61	
						OR GET_PROPERTY_BUILDING(iCurrentYacht) = MP_PROPERTY_BUILDING_62	
						OR GET_PROPERTY_BUILDING(iCurrentYacht) = MP_PROPERTY_BUILDING_63
							PLAY_SOUND_FRONTEND(entryCutData.iSoundID, "WOODEN_DOOR_OPEN_NO_HANDLE_AT")
						ELIF IS_PLAYER_ON_ANY_YACHT(PLAYER_ID())
							PLAY_SOUND_FRONTEND(entryCutData.iSoundID, "PUSH", "DLC_APT_YACHT_DOOR_SOUNDS")
						ELSE
							PLAY_SOUND_FRONTEND(entryCutData.iSoundID, "PUSH", "GTAO_APT_DOOR_DOWNSTAIRS_GLASS_SOUNDS")
						ENDIF
						iApartmentDoorSoundStage++
					ENDIF
				BREAK
				CASE 1	//Door begins to swing shut
					IF GET_SYNCHRONIZED_SCENE_PHASE(entryCutData.iSyncedSceneID) >= fDoorShutting

						IF entryCutData.iSoundID < 0
							entryCutData.iSoundID = GET_SOUND_ID()
						ENDIF
						IF GET_PROPERTY_BUILDING(iCurrentYacht) = MP_PROPERTY_BUILDING_57	
						OR GET_PROPERTY_BUILDING(iCurrentYacht) = MP_PROPERTY_BUILDING_58	
						OR GET_PROPERTY_BUILDING(iCurrentYacht) = MP_PROPERTY_BUILDING_59	
						OR GET_PROPERTY_BUILDING(iCurrentYacht) = MP_PROPERTY_BUILDING_60	
						OR GET_PROPERTY_BUILDING(iCurrentYacht) = MP_PROPERTY_BUILDING_61	
						OR GET_PROPERTY_BUILDING(iCurrentYacht) = MP_PROPERTY_BUILDING_62	
						OR GET_PROPERTY_BUILDING(iCurrentYacht) = MP_PROPERTY_BUILDING_63
						
						ELIF IS_PLAYER_ON_ANY_YACHT(PLAYER_ID())
							PLAY_SOUND_FRONTEND(entryCutData.iSoundID, "LIMIT", "DLC_APT_YACHT_DOOR_SOUNDS")						
						ELSE
							PLAY_SOUND_FRONTEND(entryCutData.iSoundID, "LIMIT", "GTAO_APT_DOOR_DOWNSTAIRS_GLASS_SOUNDS")
						ENDIF

						iApartmentDoorSoundStage++
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL GET_ENTRY_CUTSCENE_DATA(ENTRY_CUTSCENE_TYPE type, BOOL bSkipSecondCut = FALSE)
	
	MODEL_NAMES modelToLoad
	IF NOT IS_BIT_SET(entryCutData.iBS,ENTRY_CS_BS_GET_DATA)
		iApartmentDoorSoundStage = 0
		SWITCH type
			CASE ENTRY_CUTSCENE_TYPE_BUZZER
				
				entryCutData.animDictionary = "ANIM@APT_TRANS@BUZZER"
				//entryCutData.animName[SYNC_SCENE_PED_A] = "buzz_short"
				entryCutData.animName[SYNC_SCENE_PED_A] = "buzz_reg"

				tempCamOffset = mpYachts[iCurrentYacht].buzzCamDetails

				entryCutData.vCamLoc[0] = tempCamOffset.vLoc
				entryCutData.vCamRot[0] = tempCamOffset.vRot
				entryCutData.fCamFOV[0] = TO_FLOAT(tempCamOffset.iFov)
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: EXT: ENTRY ANIM: CAMERA tempPropOffset: ",  tempPropOffset.vLoc, ", ROT: ", tempPropOffset.vRot)
				#ENDIF
				tempPropOffset = mpYachts[iCurrentYacht].buzzSceneDetails
				//GET_TRANSITION_BUZZ_SCENE_DETAILS(mpYachts[iCurrentYacht].iBuildingID, tempPropOffset,iEntrancePlayerIsUsing) 
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: EXT: ENTRY ANIM: Sync scene tempPropOffset: ",  tempPropOffset.vLoc, ", ROT: ", tempPropOffset.vRot)
				#ENDIF
				entryCutData.vSyncSceneLoc = tempPropOffset.vLoc
				entryCutData.vSyncSceneRot = tempPropOffset.vRot
				IF NOT DOES_ENTITY_EXIST(entryCutData.playerClone)
					//IF IS_PED_MALE(PLAYER_PED_ID()) 
					IF NOT IS_PLAYER_FEMALE()
						entryCutData.playerClone = CREATE_PED(PEDTYPE_CIVMALE, GET_ENTITY_MODEL(PLAYER_PED_ID()), GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<0.0, 0.0, -10.0>>, GET_ENTITY_HEADING(PLAYER_PED_ID()), FALSE, FALSE)
					ELSE
						entryCutData.playerClone = CREATE_PED(PEDTYPE_CIVFEMALE, GET_ENTITY_MODEL(PLAYER_PED_ID()), GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<0.0, 0.0, -10.0>>, GET_ENTITY_HEADING(PLAYER_PED_ID()), FALSE, FALSE)
					ENDIF
					CLONE_PED_TO_TARGET(PLAYER_PED_ID(), entryCutData.playerClone)
				ENDIF
				FREEZE_ENTITY_POSITION(entryCutData.playerClone,TRUE)
				SET_ENTITY_PROOFS(entryCutData.playerClone,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
				entryCutData.fSyncedSceneCompleteStage = 0.6
				CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_ENTRY_CUTSCENE_DATA- for buzzer")
			BREAK
			CASE ENTRY_CUTSCENE_TYPE_FRONT_DOOR
				//entryCutData.animDictionary = "anim@apt_trans@hinge_r"
				entryCutData.animDictionary = "anim@apt_trans@hinge_l"

				entryCutData.animName[SYNC_SCENE_PED_A] = "ext_player"
				entryCutData.animName[SYNC_SCENE_DOOR] = "ext_door"
				IF GET_RAILING_OF_PRIVATE_YACHT(iCurrentYacht) = 0
					modelToLoad = INT_TO_ENUM(MODEL_NAMES,HASH("apa_mp_apa_yacht_door"))
				ELSE
					modelToLoad = INT_TO_ENUM(MODEL_NAMES,HASH("apa_mp_apa_yacht_door2"))
				ENDIF
				 //Prop_bh1_44_door_01l//INT_TO_ENUM(MODEL_NAMES,HASH("MP_Apa_Yacht_door")) ///temp CDM
				IF modelToLoad != DUMMY_MODEL_FOR_SCRIPT
					CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_ENTRY_CUTSCENE_DATA- Creating a special door object")
//				ELSE
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_ENTRY_CUTSCENE_DATA- Creating a non special door object")
//					modelToLoad = propertyDoors[0].doorModel
				ENDIF
				
				IF NOT REQUEST_LOAD_MODEL(modelToLoad)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "Waiting for model to load 121331232")
					RETURN FALSE
				ENDIF
				

				tempCamOffset.vLoc = <<0,0,0>>
				tempCamOffset.vRot = <<0,0,0>>
				tempCamOffset.iFov = 0

				//GET_TRANSITION_EXIT_CAM_DETAILS(mpYachts[iCurrentYacht].iBuildingID, 0, tempCamOffset) //WARPING WORK CDM 176870
				tempCamOffset = mpYachts[iCurrentYacht].exitCamDetails
				entryCutData.vCamLoc[0] = tempCamOffset.vLoc
				entryCutData.vCamRot[0] = tempCamOffset.vRot
				entryCutData.fCamFOV[0] = TO_FLOAT(tempCamOffset.iFov)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: TRANS: vCameraPos", tempCamOffset.vLoc)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: TRANS: vCameraRot", tempCamOffset.vRot)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: TRANS: fCameraFov", tempCamOffset.iFov)

				//tempPropOffset = GET_TRANSITION_ENTER_SCENE_DETAILS(mpYachts[iCurrentYacht].iBuildingID, tempPropOffset )
				tempPropOffset = mpYachts[iCurrentYacht].enterSceneDetails
				entryCutData.vSyncSceneLoc = tempPropOffset.vLoc
				entryCutData.vSyncSceneRot = tempPropOffset.vRot
				CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: TRANS: entryCutData.vSyncSceneLoc", tempCamOffset.vLoc)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: TRANS: entryCutData.vSyncSceneRot", tempCamOffset.vRot)
				
				SET_BIT(entryCutData.iBS,ENTRY_CS_BS_SECOND_CUT)
				IF bSkipSecondCut
					CLEAR_BIT(entryCutData.iBS,ENTRY_CS_BS_SECOND_CUT)
				ENDIF
								
//				IF NOT DOES_ENTITY_EXIST(entryCutData.objIDs[0])
//					entryCutData.objIDs[0] = CREATE_OBJECT_NO_OFFSET(modelToLoad, GET_PLAYER_COORDS(PLAYER_ID()) + <<0.0, 0.0, -20.0>>,FALSE,FALSE,TRUE)
//				ENDIF
				CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_ENTRY_CUTSCENE_DATA- door exists")
				IF NOT IS_PLAYER_FEMALE()
					entryCutData.playerClone = CREATE_PED(PEDTYPE_CIVMALE, GET_ENTITY_MODEL(PLAYER_PED_ID()), GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<0.0, 0.0, -10.0>>, GET_ENTITY_HEADING(PLAYER_PED_ID()), FALSE, FALSE)
				ELSE
					entryCutData.playerClone = CREATE_PED(PEDTYPE_CIVFEMALE, GET_ENTITY_MODEL(PLAYER_PED_ID()), GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<0.0, 0.0, -10.0>>, GET_ENTITY_HEADING(PLAYER_PED_ID()), FALSE, FALSE)
				ENDIF
				CLONE_PED_TO_TARGET(PLAYER_PED_ID(), entryCutData.playerClone)
				FREEZE_ENTITY_POSITION(entryCutData.playerClone,TRUE)
				SET_ENTITY_PROOFS(entryCutData.playerClone,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_ENTRY_CUTSCENE_DATA- for front door")
				entryCutData.fSyncedSceneCompleteStage = 0.6
			BREAK
		ENDSWITCH
		
		SET_BIT(entryCutData.iBS,ENTRY_CS_BS_GET_DATA)
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL DO_YACHT_PAN(CAMERA_INDEX& hCam0, CAMERA_INDEX& hCam1)

	// Create new cams
	IF DOES_CAM_EXIST(hCam0)
		DESTROY_CAM(hCam0)
	ENDIF
	IF DOES_CAM_EXIST(hCam1)
		DESTROY_CAM(hCam1)
	ENDIF
	hCam0 = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
	hCam1 = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
	IF DOES_CAM_EXIST(hCam0) AND DOES_CAM_EXIST(hCam1)

		SET_CAM_COORD(hCam0,	mpYachts[iCurrentYacht].panStart.vLoc)
		SET_CAM_ROT(hCam0,		mpYachts[iCurrentYacht].panStart.vRot)
		SET_CAM_FOV(hCam0,		50)
		
		SET_CAM_COORD(hCam1,	mpYachts[iCurrentYacht].panEnd.vLoc)
		SET_CAM_ROT(hCam1,		mpYachts[iCurrentYacht].panEnd.vRot)
		SET_CAM_FOV(hCam1,		50)

		SHAKE_CAM(hCam0,		"HAND_SHAKE", 0.2500)
		SHAKE_CAM(hCam1,		"HAND_SHAKE", 0.2500)
			
		//IF pan.fDuration > 0.1
			SET_CAM_ACTIVE_WITH_INTERP(hCam1, hCam0, ROUND(5.0000 * 1000.0))
		//ELSE
		//	SET_CAM_ACTIVE(hCam0, TRUE)
		//ENDIF
		
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
		RETURN TRUE
		
	ENDIF

	RETURN FALSE
ENDFUNC

PROC RESET_YACHT_DOOR_POSITION()
	IF DOES_ENTITY_EXIST(yachtDoor)
		SET_ENTITY_COORDS_NO_OFFSET(yachtDoor,mpYachts[iCurrentYacht].doorLocDetails.vLoc)
		SET_ENTITY_ROTATION(yachtDoor,mpYachts[iCurrentYacht].doorLocDetails.vRot)
	ENDIF
	PRINTLN("RESET_YACHT_DOOR_POSITION: called this frame")
ENDPROC

FUNC BOOL RUN_ENTRY_CUTSCENE(ENTRY_CUTSCENE_TYPE type, BOOL bSkipSecondCut = FALSE)
	
	IF IS_BIT_SET(iLocalBS2, LOCAL_BS2_bRunPropertySelectMenu)
	ENDIF
	
	IF NOT IS_GAMEPLAY_CAM_RENDERING()
		HIDE_HUD_AND_RADAR_THIS_FRAME()
	ENDIF
//	STRUCT_net_realty_1_scene scene
//	IF Private_Get_net_realty_1_scene(GET_PROPERTY_BUILDING(iCurrentYacht), scene)
//	ENDIF
	FLOAT fStartPhase = 0.0
	IF type = ENTRY_CUTSCENE_TYPE_FRONT_DOOR
	AND entryCutData.iStage < ENTRY_CS_STAGE_RUN_SECOND_CUT
		DO_APARTMENT_DOOR_SOUNDS()
		fStartPhase = 0.15
	ENDIF
	
	//VECTOR vOffset
	IF type = ENTRY_CUTSCENE_TYPE_BUZZER
		DO_APARTMENT_BUZZER_PRESS_SOUNDS()
		IF NOT IS_PED_INJURED(entryCutData.playerClone)
			SET_PED_RESET_FLAG(entryCutData.playerClone,PRF_DisableHighHeels,TRUE)
		ENDIF
	ENDIF
	//MP_PROP_OFFSET_STRUCT playerOffset
	CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: TRANS: PROPERTY_EXT RUN_ENTRY_CUTSCENE type ",ENUM_TO_INT(type),": iStage: ", entryCutData.iStage)
	SWITCH entryCutData.iStage
		
		CASE ENTRY_CS_STAGE_INIT_DATA
			iApartmentBuzzerSoundStage = 0
			SET_BIT(iLocalBS, LOCAL_BS_BUZZER_REQUESTED_ANIM)
			IF GET_ENTRY_CUTSCENE_DATA(type,bSkipSecondCut)
				REQUEST_ANIM_DICT(entryCutData.animDictionary)
				IF HAS_ANIM_DICT_LOADED(entryCutData.animDictionary)
					REINIT_NET_TIMER(entryCutData.timer,TRUE)
					SET_ENTRY_CS_STAGE(ENTRY_CS_STAGE_WAIT_FOR_MODELS)
				ENDIF
			ENDIF
		BREAK
		
		CASE ENTRY_CS_STAGE_WAIT_FOR_MODELS
			IF NOT HAS_NET_TIMER_EXPIRED(entryCutData.timer,5000,TRUE)
				IF DOES_ENTITY_EXIST(entryCutData.playerClone)
					IF NOT IS_PED_INJURED(entryCutData.playerClone)
						IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(entryCutData.playerClone)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "Waiting player clone to stream" )
							RETURN FALSE
						ENDIF
						IF NOT HAS_PED_HEAD_BLEND_FINISHED(entryCutData.playerClone)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "Waiting player clone to finish head blend" )
							RETURN FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			SET_ENTRY_CS_STAGE(ENTRY_CS_STAGE_HANDLE_PLAYER_PED)
		BREAK
		
		CASE ENTRY_CS_STAGE_HANDLE_PLAYER_PED
			
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_NO_COLLISION | NSPC_FREEZE_POSITION | NSPC_PREVENT_VISIBILITY_CHANGES)
			
			//SET_LOCAL_PLAYER_INVISIBLE_LOCALLY()
			NETWORK_FADE_OUT_ENTITY(PLAYER_PED_ID(), TRUE, TRUE)
			
			CLEAR_AREA_OF_PROJECTILES(entryCutData.vSyncSceneLoc, 2.0)
			
			CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: EXT ENTRY ANIM: - fading player and mp_cutscene ")
			
			SET_BIT(entryCutData.iBS,ENTRY_CS_BS_TOOK_AWAY_CONTROL)
			SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(FALSE, TRUE)
			START_MP_CUTSCENE(TRUE)
			SET_BIT(entryCutData.iBS,ENTRY_CS_BS_STARTED_MP_CUTSCENE)
			SET_ENTRY_CS_STAGE(ENTRY_CS_STAGE_TRIGGER_CUT)
		FALLTHRU // url:bugstar:2167563 - Character pops / disappears right before camera cut of player entering apartment building
		
		CASE ENTRY_CS_STAGE_TRIGGER_CUT
			
			//Create Camera
			IF DOES_CAM_EXIST(entryCutData.cameras[0])
				DESTROY_CAM(entryCutData.cameras[0])
			ENDIF
			
			IF type = ENTRY_CUTSCENE_TYPE_FRONT_DOOR
//				CREATE_MODEL_HIDE(propertyDoors[0].vCoords,1,propertyDoors[0].doorModel,TRUE)	
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "Creating model hide 2  at ",propertyDoors[0].vCoords, " model: ",propertyDoors[0].doorModel)
				SET_BIT(entryCutData.iBS,ENTRY_CS_BS_APT_DOOR_MODEL_HIDE)

//				IF DOES_ENTITY_EXIST(yachtDoor)
//					vOffset = GET_ANIM_INITIAL_OFFSET_POSITION(entryCutData.animDictionary,entryCutData.animName[SYNC_SCENE_DOOR],entryCutData.vSyncSceneLoc,entryCutData.vSyncSceneRot,fStartPhase) + <<0,0,-0.5>>
//					SET_ENTITY_COORDS_NO_OFFSET(yachtDoor,
//												vOffset)
//					SET_ENTITY_ROTATION(yachtDoor,
//												GET_ANIM_INITIAL_OFFSET_ROTATION(entryCutData.animDictionary,entryCutData.animName[SYNC_SCENE_DOOR],entryCutData.vSyncSceneLoc,entryCutData.vSyncSceneRot,fStartPhase))
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: setting entity coords: here")
//				ELSE
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: entity does not exist??")
//				ENDIF
				
				
			ENDIF	
			entryCutData.cameras[0] = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
				
			SET_CAM_PARAMS(entryCutData.cameras[0],
							entryCutData.vCamLoc[0],
							entryCutData.vCamRot[0],
							entryCutData.fCamFOV[0])
			
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: TRANS: CAMERA LOC: ", entryCutData.vCamLoc[0], ", ROT: ", entryCutData.vCamRot[0])
			#ENDIF
				
			SET_CAM_FAR_CLIP(entryCutData.cameras[0],1000)
			SHAKE_CAM(entryCutData.cameras[0], "HAND_SHAKE", 0.25)

			SET_BIT(entryCutData.iBS,ENTRY_CS_BS_STARTED_MP_CUTSCENE)

			REINIT_NET_TIMER(entryCutData.timer,TRUE)	
			
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			FREEZE_ENTITY_POSITION(entryCutData.playerClone,FALSE)
			entryCutData.iSyncedSceneID = CREATE_SYNCHRONIZED_SCENE(entryCutData.vSyncSceneLoc,entryCutData.vSyncSceneRot)
			IF NOT IS_PED_INJURED(entryCutData.playerClone)

				TASK_SYNCHRONIZED_SCENE(entryCutData.playerClone, entryCutData.iSyncedSceneID, 
													entryCutData.animDictionary, entryCutData.animName[SYNC_SCENE_PED_A], 
													INSTANT_BLEND_IN,INSTANT_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT,RBF_NONE)
			ENDIF
			
			IF DOES_ENTITY_EXIST(yachtDoor)
			AND type = ENTRY_CUTSCENE_TYPE_FRONT_DOOR
				//VECTOR sceneRotation = << -1.725, -0.000, 2.215 >>

				//BOOL bDoorGarage 
//				SET_ENTITY_COORDS_NO_OFFSET(entryCutData.objIDs[0], mpYachts[iCurrentYacht].doorLocDetails.vLoc)
//				SET_ENTITY_ROTATION(entryCutData.objIDs[0], mpYachts[iCurrentYacht].doorLocDetails.vRot)
//				PLAY_ENTITY_ANIM(entryCutData.objIDs[0],
//											entryCutData.animName[ENUM_TO_INT(SYNC_SCENE_DOOR)], entryCutData.animDictionary,
//											INSTANT_BLEND_IN,FALSE, FALSE, DEFAULT, 0.153,ENUM_TO_INT(AF_USE_MOVER_EXTRACTION))
											
											
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: RUN_ENTRY_CUTSCENE: ENTRY_CS_STAGE_TRIGGER_CUT")		
//				
//				PLAY_SYNCHRONIZED_ENTITY_ANIM(yachtDoor,entryCutData.iSyncedSceneID,
//																	entryCutData.animName[SYNC_SCENE_DOOR],entryCutData.animDictionary,
//																	INSTANT_BLEND_IN,INSTANT_BLEND_OUT)
//				BOOL bDoorGarage 
//				MP_PROP_OFFSET_STRUCT tempOffset
//				GET_BUILDING_DOOR_DETAILS(tempOffset,propertyDoors[0],bDoorGarage,mpProperties[iCurrentPropertyID].iBuildingID,1)
//				SET_ENTITY_COORDS_NO_OFFSET(entryCutData.objIDs[0], tempOffset.vLoc)
//				SET_ENTITY_ROTATION(entryCutData.objIDs[0], <<0,0,-90>>)
				PLAY_ENTITY_ANIM(yachtDoor,
											entryCutData.animName[ENUM_TO_INT(SYNC_SCENE_DOOR)], entryCutData.animDictionary,
											INSTANT_BLEND_IN,FALSE,FALSE, DEFAULT, 0,ENUM_TO_INT(AF_USE_MOVER_EXTRACTION))
													
													
				CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: RUN_ENTRY_CUTSCENE: ENTRY_CS_STAGE_TRIGGER_CUT: with offset for yach")
			ENDIF
			SET_SYNCHRONIZED_SCENE_PHASE(entryCutData.iSyncedSceneID,fStartPhase)
			
			SET_ENTRY_CS_STAGE(ENTRY_CS_STAGE_RUN_CUT)
		BREAK
		CASE ENTRY_CS_STAGE_RUN_CUT
			IF HAS_NET_TIMER_EXPIRED(entryCutData.timer,5000,TRUE)
				IF IS_BIT_SET(entryCutData.iBS,ENTRY_CS_BS_SECOND_CUT)
					REINIT_NET_TIMER(iCameraTimer,TRUE)
					REINIT_NET_TIMER(iWalkInTimer,TRUE)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "ENTRY_CS_STAGE_RUN_CUT timer expired")
					DO_YACHT_PAN(propertyCam0, propertyCam1)
					IF NOT IS_PED_INJURED(entryCutData.playerClone)
					AND type = ENTRY_CUTSCENE_TYPE_FRONT_DOOR
						SET_ENTITY_VISIBLE(entryCutData.playerClone,FALSE)
					ENDIF
					SET_ENTRY_CS_STAGE(ENTRY_CS_STAGE_RUN_SECOND_CUT)
					bPlayerDoorShutSound = FALSE
				ELSE
					RETURN TRUE
				ENDIF
			ENDIF
			IF entryCutData.iSyncedSceneID >= 0
				IF IS_SYNCHRONIZED_SCENE_RUNNING(entryCutData.iSyncedSceneID)
					IF GET_SYNCHRONIZED_SCENE_PHASE(entryCutData.iSyncedSceneID) > entryCutData.fSyncedSceneCompleteStage
						IF NOT IS_BIT_SET(entryCutData.iBS,ENTRY_CS_BS_RESET_DOOR)
							IF type = ENTRY_CUTSCENE_TYPE_FRONT_DOOR
							AND DOES_ENTITY_EXIST(yachtDoor)
								STOP_ENTITY_ANIM(yachtDoor,entryCutData.animName[ENUM_TO_INT(SYNC_SCENE_DOOR)], entryCutData.animDictionary,
											INSTANT_BLEND_OUT)
								//RESET_YACHT_DOOR_POSITION()
//								SET_ENTITY_VISIBLE(yachtDoor, FALSE)
//								SET_BIT(iLocalBs,LOCAL_BS_SET_YACHT_DOOR_INVISIBLE)
//								PRINTLN("ENTRY_CUTSCENE_TYPE_FRONT_DOOR: door set not visible -1")
//								//REMOVE_MODEL_HIDE(propertyDoors[0].vCoords,1,propertyDoors[0].doorModel)	
//								//CDEBUG1LN(DEBUG_SAFEHOUSE, "REMOVING  model hide 2  at ",propertyDoors[0].vCoords, " model: ",propertyDoors[0].doorModel)
//								//CLEAR_BIT(entryCutData.iBS,ENTRY_CS_BS_APT_DOOR_MODEL_HIDE)
							ENDIF
							SET_BIT(entryCutData.iBS,ENTRY_CS_BS_RESET_DOOR)
						ELSE
							IF IS_BIT_SET(entryCutData.iBS,ENTRY_CS_BS_SECOND_CUT)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "ENTRY_CS_STAGE_RUN_CUT scene is complete")
								REINIT_NET_TIMER(iCameraTimer,TRUE)
								REINIT_NET_TIMER(iWalkInTimer,TRUE)
								DO_YACHT_PAN(propertyCam0, propertyCam1)
								IF NOT IS_PED_INJURED(entryCutData.playerClone)
								AND type = ENTRY_CUTSCENE_TYPE_FRONT_DOOR
									SET_ENTITY_VISIBLE(entryCutData.playerClone,FALSE)
								ENDIF
								bPlayerDoorShutSound = FALSE
								SET_ENTRY_CS_STAGE(ENTRY_CS_STAGE_RUN_SECOND_CUT)
							ELSE
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(entryCutData.iBS,ENTRY_CS_BS_SECOND_CUT)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "ENTRY_CS_STAGE_RUN_CUT scene is <= 0")
					REINIT_NET_TIMER(iCameraTimer,TRUE)
					REINIT_NET_TIMER(iWalkInTimer,TRUE)
					DO_YACHT_PAN(propertyCam0, propertyCam1)
					IF NOT IS_PED_INJURED(entryCutData.playerClone)
					AND type = ENTRY_CUTSCENE_TYPE_FRONT_DOOR
						SET_ENTITY_VISIBLE(entryCutData.playerClone,FALSE)
					ENDIF
					bPlayerDoorShutSound = FALSE
					SET_ENTRY_CS_STAGE(ENTRY_CS_STAGE_RUN_SECOND_CUT)
				ELSE
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		CASE ENTRY_CS_STAGE_RUN_SECOND_CUT
			IF HAS_NET_TIMER_EXPIRED(iCameraTimer, ROUND(( 5 + 0.5) * 1000.0),TRUE)
				IF entryCutData.iSoundID >= 0
					STOP_SOUND(entryCutData.iSoundID)
				ENDIF
				RETURN TRUE
			ELSE
				IF type = ENTRY_CUTSCENE_TYPE_FRONT_DOOR
					IF NOT bPlayerDoorShutSound
						CDEBUG1LN(DEBUG_SAFEHOUSE, "ENTRY_CS_STAGE_RUN_CUT: playing sound for door closed -2")
						PLAY_SOUND_FRONTEND(-1, "Closed", "DLC_APT_YACHT_DOOR_SOUNDS")
						bPlayerDoorShutSound = TRUE
					ENDIF
					DO_APARTMENT_DOOR_SOUNDS()
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC


ENUM BRIDGE_GROUP_EXIT_SYNC_SCENE_ELEMENTS
	BGE_SYNC_SCENE_PED_A,
	BGE_SYNC_SCENE_PED_B,
	BGE_SYNC_SCENE_PED_C,
	BGE_SYNC_SCENE_PED_D,
	BGE_SYNC_SCENE_DOOR,
	BGE_SYNC_SCENE_CAM,
	BGE_SYNC_SCENE_MAX_ELEMENTS
ENDENUM

CONST_INT YACHT_BRIDGE_GROUP_BS_PUSH_SOUND		0
CONST_INT YACHT_BRIDGE_GROUP_BS_LIMIT_SOUND		1

STRUCT YACHT_BRIDGE_GROUP_WALKOUT
	INT iStage
	INT iSyncedSceneID = -1
	INT iSoundID = -1
	
	PED_INDEX playerPeds[4]
	PLAYER_INDEX playerForScene[4]
	CAMERA_INDEX cam
	SCRIPT_TIMER timer
	SCRIPT_TIMER playerReadyTimer
	
	INT iBS
	
	STRING animDictionary
	STRING animName[BGE_SYNC_SCENE_MAX_ELEMENTS]
	
	VECTOR vCamLoc[1]
	VECTOR vCamRot[1]
	FLOAT fCamFOV[1]
	
	VECTOR vSyncSceneLoc
	VECTOR vSyncSceneRot
	FLOAT fSyncedSceneCompleteStage
ENDSTRUCT
YACHT_BRIDGE_GROUP_WALKOUT yachtBridgeGroupWalkout

CONST_INT YACHT_BRIDGE_GROUP_WALKOUT_INIT			0
CONST_INT YACHT_BRIDGE_GROUP_WALKOUT_LOAD			1	
CONST_INT YACHT_BRIDGE_GROUP_WALKOUT_TRIGGER_SCENE	2
CONST_INT YACHT_BRIDGE_GROUP_WALKOUT_RUN_SCENE		3

//transAnimData.animDictionary = "anim@apt_trans@hinge_r"
//transAnimData.animName[ENUM_TO_INT(SYNC_SCENE_PED_A)] = "ext_player"
//transAnimData.animName[ENUM_TO_INT(SYNC_SCENE_PED_B)] = "ext_a"
//transAnimData.animName[ENUM_TO_INT(SYNC_SCENE_PED_C)] = "ext_b"
//transAnimData.animName[ENUM_TO_INT(SYNC_SCENE_PED_D)] = "ext_c"
//transAnimData.animName[ENUM_TO_INT(SYNC_SCENE_DOOR)] = "ext_door"

PROC GET_GROUP_BRIDGE_EXIT_DETAILS(YACHT_BRIDGE_GROUP_WALKOUT &walkout)

	MP_PROP_OFFSET_STRUCT sPropOffsetA
	
	
	walkout.animDictionary = "anim@apt_trans@hinge_r"
	walkout.animName[BGE_SYNC_SCENE_PED_A] = "ext_player"
	walkout.animName[BGE_SYNC_SCENE_PED_B] = "ext_a"
	walkout.animName[BGE_SYNC_SCENE_PED_C] = "ext_b"
	walkout.animName[BGE_SYNC_SCENE_PED_D] = "ext_c"
	walkout.animName[BGE_SYNC_SCENE_DOOR] = "ext_door"
	
	//camera details
	GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_BRIDGE_EXIT_CAM, sPropOffsetA)
	
	walkout.vCamLoc[0] = sPropOffsetA.vLoc
	walkout.vCamRot[0] = sPropOffsetA.vRot
	walkout.fCamFOV[0] = 24.915972
	
	//scene details
	GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_BRIDGE_EXIT_ANIM_POS, sPropOffsetA)
	
	walkout.vSyncSceneLoc = sPropOffsetA.vLoc
	walkout.vSyncSceneRot = sPropOffsetA.vRot
	walkout.fSyncedSceneCompleteStage = 0.6
ENDPROC

PROC START_GROUP_BRIDGE_SCENE(YACHT_BRIDGE_GROUP_WALKOUT &walkout)
	INT i
	IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
	AND IS_SKYSWOOP_AT_GROUND()
	AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	ENDIF
	SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(FALSE, FALSE)

//	BGE_SYNC_SCENE_PED_A,
//	BGE_SYNC_SCENE_PED_B,
//	BGE_SYNC_SCENE_PED_C,
//	BGE_SYNC_SCENE_PED_D,
	
	walkout.iSyncedSceneID = CREATE_SYNCHRONIZED_SCENE(walkout.vSyncSceneLoc, walkout.vSyncSceneRot, EULER_YXZ)
	
	REPEAT 4 i
		IF DOES_ENTITY_EXIST(walkout.playerPeds[i])
		AND NOT IS_PED_INJURED(walkout.playerPeds[i])
			CLEAR_ROOM_FOR_ENTITY(walkout.playerPeds[i])
			FREEZE_ENTITY_POSITION(walkout.playerPeds[i],FALSE)
			SET_ENTITY_ROTATION(walkout.playerPeds[i],
										GET_ANIM_INITIAL_OFFSET_ROTATION(walkout.animDictionary, walkout.animName[i], walkout.vSyncSceneLoc, walkout.vSyncSceneRot, 0.0))						
			SET_ENTITY_COORDS_NO_OFFSET(walkout.playerPeds[i],
										GET_ANIM_INITIAL_OFFSET_POSITION(walkout.animDictionary, walkout.animName[i], walkout.vSyncSceneLoc, walkout.vSyncSceneRot, 0.0))
			PRINTLN("START_GROUP_BRIDGE_SCENE: positioned clone #", i)//, " at: ",GET_ANIM_INITIAL_OFFSET_POSITION(walkout.animDictionary, walkout.animName[i], walkout.vSyncSceneLoc, walkout.vSyncSceneRot, 0.0))
			TASK_SYNCHRONIZED_SCENE(walkout.playerPeds[i], walkout.iSyncedSceneID, 
														walkout.animDictionary,walkout.animName[i], 
														INSTANT_BLEND_IN,INSTANT_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT,RBF_NONE)
			SET_ENTITY_VISIBLE(walkout.playerPeds[i],TRUE)
		ENDIF
	ENDREPEAT
		
	// start the door task later
	
	// create cam, activate
	walkout.cam = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", walkout.vCamLoc[0], walkout.vCamRot[0], walkout.fCamFOV[0])//33.7)
	SET_CAM_FAR_CLIP(walkout.cam,1000)
	SHAKE_CAM(walkout.cam, "HAND_SHAKE", 0.25)
	SET_CAM_ACTIVE(walkout.cam, TRUE)
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
	CLEAR_ROOM_FOR_GAME_VIEWPORT()
	

	PLAY_ENTITY_ANIM(objID_BridgeDoor[0],walkout.animName[BGE_SYNC_SCENE_DOOR], walkout.animDictionary,
									INSTANT_BLEND_IN,FALSE,FALSE, DEFAULT, 0,ENUM_TO_INT(AF_USE_MOVER_EXTRACTION))
											
	REINIT_NET_TIMER(walkout.timer,TRUE)	
ENDPROC

FUNC BOOL CHECK_ALL_PLAYERS_READY_FOR_YACHT_EXIT(YACHT_BRIDGE_GROUP_WALKOUT &walkout)
	INT iParticipants
	INT iPlayers
	INT iPlayersReady
	PLAYER_INDEX tempPlayer
	SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBitSet, iBS_PLAYER_BD_EXIT_GROUP_READY)
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipants
		IF iPlayers < 4
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipants))
				tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipants))
				IF NOT IS_PLAYER_SCTV(tempPlayer)
					IF IS_NET_PLAYER_OK(tempPlayer)
						//IF IS_BIT_SET(GlobalplayerBD_FM_HeistPlanning[NATIVE_TO_INT(tempPlayer)].iBSPlayerHeistSettings, ciHEIST_PLAYER_OUTFIT_SET)
						IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(GET_PLAYER_PED(tempPlayer))
							// url:bugstar:2214268
							IF tempPlayer = PLAYER_ID()
								FINALIZE_HEAD_BLEND(GET_PLAYER_PED(tempPlayer))
							ENDIF
							CDEBUG1LN(DEBUG_HEIST_PREP_EXIT,"Do_Transition_Anim: INIT: CHECK_HEIST_EXIT_OUTFIT_READY: Player: ",   GET_PLAYER_NAME(tempPlayer), "'s outfit is set, ciHEIST_PLAYER_OUTFIT_SET = TRUE and Streaming requests true")
							iPlayersReady++
						ELSE
//							IF NOT IS_BIT_SET(GlobalplayerBD_FM_HeistPlanning[NATIVE_TO_INT(tempPlayer)].iBSPlayerHeistSettings, ciHEIST_PLAYER_OUTFIT_SET)
//								CWARNINGLN(DEBUG_HEIST_PREP_EXIT,"Do_Transition_Anim: INIT: CHECK_HEIST_EXIT_OUTFIT_READY: Player: ",   GET_PLAYER_NAME(tempPlayer), "'s outfit is NOT set")
//							ENDIF
//							IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(GET_PLAYER_PED(tempPlayer))
								PRINTLN("CHECK_ALL_PLAYERS_READY_FOR_YACHT_EXIT: Player: ",   GET_PLAYER_NAME(tempPlayer), "'s streaming request not completed")
//							ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						IF IS_NET_PLAYER_OK(tempPlayer,FALSE,FALSE)
							PRINTLN("CHECK_ALL_PLAYERS_READY_FOR_YACHT_EXIT: Player: ",   GET_PLAYER_NAME(tempPlayer), " not OK")
						ELSE
							PRINTLN("CHECK_ALL_PLAYERS_READY_FOR_YACHT_EXIT: Player: ",   NATIVE_TO_INT(tempPlayer), " not OK")
						ENDIF
						#ENDIF
					ENDIF
					iPlayers++
				ELSE
					PRINTLN("CHECK_ALL_PLAYERS_READY_FOR_YACHT_EXIT: player is SCTV")
				ENDIF
			ELSE
				PRINTLN("CHECK_ALL_PLAYERS_READY_FOR_YACHT_EXIT:participant NOT active i: ",iParticipants, " participant: " ,NATIVE_TO_INT(INT_TO_PARTICIPANTINDEX(iParticipants)))
			ENDIF
		ENDIF
	ENDREPEAT
	
	PRINTLN("CHECK_ALL_PLAYERS_READY_FOR_YACHT_EXIT: ", iPlayersReady, " player's ready, out of ", iPlayers)
	
	IF iPlayersReady != iPlayers
	AND NOT HAS_NET_TIMER_EXPIRED(walkout.playerReadyTimer, 5000, TRUE)
	
		IF NOT HAS_NET_TIMER_STARTED(walkout.playerReadyTimer)
			START_NET_TIMER(walkout.playerReadyTimer,TRUE)
			PRINTLN("CHECK_ALL_PLAYERS_READY_FOR_YACHT_EXIT: Timer started")
		ENDIF
		
		PRINTLN("CHECK_ALL_PLAYERS_READY_FOR_YACHT_EXIT: Not enough players ready")
		
		RETURN FALSE 
	ELSE	
		IF HAS_NET_TIMER_EXPIRED(walkout.playerReadyTimer, 5000, TRUE)
			PRINTLN("CHECK_ALL_PLAYERS_READY_FOR_YACHT_EXIT: Timer expired")
		ELSE
			PRINTLN("CHECK_ALL_PLAYERS_READY_FOR_YACHT_EXIT: Enough players ready")
		ENDIF
		RESET_NET_TIMER(walkout.playerReadyTimer)
	ENDIF			

	RETURN TRUE
ENDFUNC

PROC SET_YACHT_BRIDGE_GROUP_WALKOUT_STAGE(YACHT_BRIDGE_GROUP_WALKOUT &walkout, INT iStage)
	PRINTLN("SET_YACHT_BRIDGE_GROUP_WALKOUT_STAGE: moving from stage: ",walkout.iStage," to stage: ",iStage)
	walkout.iStage = iStage
ENDPROC

FUNC BOOL CREATE_CLONE_OF_PLAYER(YACHT_BRIDGE_GROUP_WALKOUT &walkout,INT iCloneSlot, PLAYER_INDEX PlayerID, VECTOR vOffset)
	IF NOT DOES_ENTITY_EXIST(walkout.playerPeds[iCloneSlot])
		IF IS_NET_PLAYER_OK(PlayerID)
			IF NOT IS_PLAYER_PED_FEMALE(PlayerID)
				walkout.playerPeds[iCloneSlot] = CREATE_PED(PEDTYPE_CIVMALE, GET_ENTITY_MODEL(GET_PLAYER_PED(PlayerID)), GET_ENTITY_COORDS(GET_PLAYER_PED(PlayerID)) + vOffset, GET_ENTITY_HEADING(GET_PLAYER_PED(PlayerID)), FALSE, FALSE)
			ELSE
				walkout.playerPeds[iCloneSlot] = CREATE_PED(PEDTYPE_CIVFEMALE, GET_ENTITY_MODEL(GET_PLAYER_PED(PlayerID)), GET_ENTITY_COORDS(GET_PLAYER_PED(PlayerID)) + vOffset, GET_ENTITY_HEADING(GET_PLAYER_PED(PlayerID)), FALSE, FALSE)
			ENDIF
			CLONE_PED_TO_TARGET(GET_PLAYER_PED(PlayerID), walkout.playerPeds[iCloneSlot])
			UPDATE_MC_EMBLEM(PlayerID, walkout.playerPeds[iCloneSlot])
			
			FREEZE_ENTITY_POSITION(walkout.playerPeds[iCloneSlot],TRUE)
			PRINTLN("CREATE_CLONE_OF_PLAYER : transAnimData.bCloneCreated[",iCloneSlot,"] = TRUE")
			SET_CURRENT_PED_WEAPON(walkout.playerPeds[iCloneSlot], WEAPONTYPE_UNARMED, TRUE)
			// url:bugstar:2170681 - No footsteps playing when the players exit the apartment to start the heists
			SET_FORCE_FOOTSTEP_UPDATE(walkout.playerPeds[iCloneSlot], TRUE)
			//SET_ENTITY_VISIBLE_IN_CUTSCENE(walkout.playerPeds[iCloneSlot],TRUE)
			walkout.playerForScene[iCloneSlot] = PlayerID
			#IF IS_DEBUG_BUILD
				PRINTLN("CREATE_CLONE_OF_PLAYER - creating clone of player ",GET_PLAYER_NAME(PlayerID)," iCloneSlot = ",iCloneSlot)
			#ENDIF
		ELSE
			PRINTLN("CREATE_CLONE_OF_PLAYER - player NOT ok #",NATIVE_TO_INT(PlayerID))
		ENDIF
		RETURN FALSE
	ELSE
		IF NOT IS_PED_INJURED(walkout.playerPeds[iCloneSlot])
			IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(walkout.playerPeds[iCloneSlot])
				PRINTLN("CREATE_CLONE_OF_PLAYER: waiting for streaming requests to be complete for clone# ",iCloneSlot)
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF NOT HAS_PED_HEAD_BLEND_FINISHED(walkout.playerPeds[iCloneSlot])
			PRINTLN("CREATE_CLONE_OF_PLAYER: waiting for head blend to be complete for clone# ",iCloneSlot)
			RETURN FALSE
		ENDIF
	ENDIF	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_PLAYER_ALREADY_A_CLONE(YACHT_BRIDGE_GROUP_WALKOUT &walkout,PLAYER_INDEX &tempPlayer)
	INT i
	REPEAT 4 i
		IF tempPlayer = walkout.playerForScene[i]
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_AND_LOAD_FOR_GROUP_BRIDGE_WALKOUT(YACHT_BRIDGE_GROUP_WALKOUT &walkout)
	INT iPlayers
	PLAYER_INDEX tempPlayer
	INT iClones, iNumPlayers, iPlayersReady
	
	IF NOT DOES_ENTITY_EXIST(objID_BridgeDoor[0])
		PRINTLN("CREATE_AND_LOAD_FOR_GROUP_BRIDGE_WALKOUT: waiting for door to be created")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
		IF CREATE_CLONE_OF_PLAYER(walkout,iClones, PLAYER_ID(), <<0.0, 0.0, -10.0>>)
			iClones++
		ELSE
			PRINTLN("CREATE_AND_LOAD_FOR_GROUP_BRIDGE_WALKOUT: waiting for local clone # ",iClones)
		ENDIF
	ENDIF
	
	REPEAT NUM_NETWORK_PLAYERS iPlayers
		PRINTLN("CREATE_AND_LOAD_FOR_GROUP_BRIDGE_WALKOUT: iClones = ",iClones)
		IF iClones < 4
			tempPlayer = INT_TO_PLAYERINDEX(iPlayers)
			IF IS_NET_PLAYER_OK(tempPlayer,FALSE)
				IF tempPlayer != PLAYER_ID()
					IF NOT IS_PLAYER_ALREADY_A_CLONE(walkout,tempPlayer)
						IF NOT IS_PLAYER_SCTV(tempPlayer)
							IF CREATE_CLONE_OF_PLAYER(walkout,iClones, tempPlayer, <<0.0, 0.0, -10.0>>)
								//RETURN FALSE
								iClones++
							ELSE
								PRINTLN("CREATE_AND_LOAD_FOR_GROUP_BRIDGE_WALKOUT: waiting for clone# ",iClones)
							ENDIF
						ELSE
							PRINTLN("CREATE_AND_LOAD_FOR_GROUP_BRIDGE_WALKOUT: player is SCTV")
						ENDIF
					ELSE
						iClones++
					ENDIF
				ENDIF
			ELSE
				PRINTLN("CREATE_AND_LOAD_FOR_GROUP_BRIDGE_WALKOUT: participant NOT active iPlayers: ",iPlayers, " participant: " ,NATIVE_TO_INT(INT_TO_PARTICIPANTINDEX(iPlayers)))
			ENDIF
		ENDIF
	ENDREPEAT
	SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBitSet, iBS_PLAYER_BD_EXIT_GROUP_READY)
	REPEAT NUM_NETWORK_PLAYERS iPlayers
		IF iPlayers < 4
			tempPlayer = INT_TO_PLAYERINDEX(iPlayers)
			IF NOT IS_PLAYER_SCTV(tempPlayer)
				IF IS_NET_PLAYER_OK(tempPlayer)
					IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(tempPlayer)].iBitSet, iBS_PLAYER_BD_EXIT_GROUP_READY)
						PRINTLN("CREATE_AND_LOAD_FOR_GROUP_BRIDGE_WALKOUT:  ",   GET_PLAYER_NAME(tempPlayer), " not set flag iBS_PLAYER_BD_EXIT_GROUP_READY" )
					ELSE
						PRINTLN("CREATE_AND_LOAD_FOR_GROUP_BRIDGE_WALKOUT:  ",   GET_PLAYER_NAME(tempPlayer), " has set flag iBS_PLAYER_BD_EXIT_GROUP_READY" )
						iPlayersReady++
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					IF IS_NET_PLAYER_OK(tempPlayer,FALSE,FALSE)
						PRINTLN("CREATE_AND_LOAD_FOR_GROUP_BRIDGE_WALKOUT:  ",   GET_PLAYER_NAME(tempPlayer), " not OK")
					ELSE
						PRINTLN("CREATE_AND_LOAD_FOR_GROUP_BRIDGE_WALKOUT: : ",   NATIVE_TO_INT(tempPlayer), " not OK")
					ENDIF
					#ENDIF
				ENDIF
				iNumPlayers++
			ELSE
				PRINTLN("CREATE_AND_LOAD_FOR_GROUP_BRIDGE_WALKOUT: player is SCTV")
			ENDIF
		ENDIF
	ENDREPEAT
	
	PRINTLN("CREATE_AND_LOAD_FOR_GROUP_BRIDGE_WALKOUT: player's ready: ", iPlayersReady, " iClones: ",iClones," out of ", iNumPlayers)
	
	IF (iPlayersReady != iNumPlayers OR iClones != iNumPlayers)
	AND NOT HAS_NET_TIMER_EXPIRED(walkout.playerReadyTimer, 10000, TRUE)
	
		IF NOT HAS_NET_TIMER_STARTED(walkout.playerReadyTimer)
			START_NET_TIMER(walkout.playerReadyTimer,TRUE)
			PRINTLN("CREATE_AND_LOAD_FOR_GROUP_BRIDGE_WALKOUT: Timer started")
		ENDIF
		
		PRINTLN("CREATE_AND_LOAD_FOR_GROUP_BRIDGE_WALKOUT: Not enough players ready")
		
		RETURN FALSE 
	ELSE	
		IF HAS_NET_TIMER_EXPIRED(walkout.playerReadyTimer, 10000, TRUE)
			PRINTLN("CREATE_AND_LOAD_FOR_GROUP_BRIDGE_WALKOUT: Timer expired")
		ELSE
			PRINTLN("CREATE_AND_LOAD_FOR_GROUP_BRIDGE_WALKOUT: Enough players ready")
		ENDIF
		RESET_NET_TIMER(walkout.playerReadyTimer)
	ENDIF			

	RETURN TRUE
ENDFUNC

PROC CLEANUP_GROUP_BRIDGE_WALKOUT(YACHT_BRIDGE_GROUP_WALKOUT &walkout)
	MP_PROP_OFFSET_STRUCT sPropOffsetA
	INT i 
	
	walkout.iStage = 0
	walkout.iSyncedSceneID = -1
	walkout.iSoundID = -1
	CLEANUP_MP_CUTSCENE(TRUE,FALSE)
	
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
	SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
	IF DOES_CAM_EXIST(walkout.cam)
		DESTROY_CAM(walkout.cam)
	ENDIF
	
	REMOVE_ANIM_DICT(walkout.animDictionary)
	
	PLAY_SOUND_FRONTEND(-1, "CLOSED", "DLC_APT_YACHT_DOOR_SOUNDS")
	
	REPEAT 4 i 
		IF DOES_ENTITY_EXIST(walkout.playerPeds[i])
			DELETE_PED(walkout.playerPeds[i])
		ENDIF

		walkout.playerForScene[i] = INVALID_PLAYER_INDEX()
	ENDREPEAT
	
	// reset door
	IF DOES_ENTITY_EXIST(objID_BridgeDoor[0])
		STOP_ENTITY_ANIM(objID_BridgeDoor[0], walkout.animName[BGE_SYNC_SCENE_DOOR], walkout.animDictionary, INSTANT_BLEND_OUT)
		GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_BRIDGE_DOOR, sPropOffsetA)
		SET_ENTITY_COORDS_NO_OFFSET(objID_BridgeDoor[0], sPropOffsetA.vLoc)
		SET_ENTITY_ROTATION(objID_BridgeDoor[0], sPropOffsetA.vRot)
	ENDIF

	RESET_NET_TIMER(walkout.timer)
	RESET_NET_TIMER(walkout.playerReadyTimer)
	
	walkout.iBS = 0
	
	//NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE) //url:bugstar:6501828 - bon voyage - could we find out a way to prevent players from dying at the start as we're walking towards out chopper
	PRINTLN("CLEANUP_GROUP_BRIDGE_WALKOUT: not changing player control, mission will do this.")
	g_bRunGroupYachtExit = FALSE
	g_bRunningGroupYachtExit = FALSE
	
	CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBitSet, iBS_PLAYER_BD_EXIT_GROUP_READY)
	PRINTLN("CLEANUP_GROUP_BRIDGE_WALKOUT: run this frame")
ENDPROC

PROC RUN_GROUP_BRIDGE_WALKOUT(YACHT_BRIDGE_GROUP_WALKOUT &walkout)
	INT i
	IF NOT IS_GAMEPLAY_CAM_RENDERING()
		HIDE_HUD_AND_RADAR_THIS_FRAME()
	ENDIF
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "RUN_GROUP_BRIDGE_WALKOUT: iStage: ", walkout.iStage)
	SWITCH walkout.iStage
		CASE YACHT_BRIDGE_GROUP_WALKOUT_INIT
			GET_GROUP_BRIDGE_EXIT_DETAILS(walkout)
			REQUEST_ANIM_DICT(walkout.animDictionary)
			REPEAT 4 i 
				walkout.playerForScene[i] = INVALID_PLAYER_INDEX()
			ENDREPEAT
			IF HAS_ANIM_DICT_LOADED(walkout.animDictionary)
				REINIT_NET_TIMER(walkout.timer,TRUE)
				SET_YACHT_BRIDGE_GROUP_WALKOUT_STAGE(walkout,YACHT_BRIDGE_GROUP_WALKOUT_LOAD)
			ENDIF
		BREAK
		
		CASE YACHT_BRIDGE_GROUP_WALKOUT_LOAD
			IF CREATE_AND_LOAD_FOR_GROUP_BRIDGE_WALKOUT(walkout)
				START_MP_CUTSCENE(TRUE)
				
				g_bRunningGroupYachtExit = TRUE
				SET_YACHT_BRIDGE_GROUP_WALKOUT_STAGE(walkout,YACHT_BRIDGE_GROUP_WALKOUT_TRIGGER_SCENE)
			ENDIF
		BREAK
		
		CASE YACHT_BRIDGE_GROUP_WALKOUT_TRIGGER_SCENE
			CLEANUP_ALL_CORONA_FX(TRUE) 
			SET_SKYFREEZE_CLEAR(TRUE)
			SET_SKYBLUR_CLEAR()
			ANIMPOSTFX_STOP("MP_job_load")
			START_GROUP_BRIDGE_SCENE(walkout)
			REINIT_NET_TIMER(walkout.timer,TRUE)
			SET_YACHT_BRIDGE_GROUP_WALKOUT_STAGE(walkout,YACHT_BRIDGE_GROUP_WALKOUT_RUN_SCENE)
		BREAK
		
		CASE YACHT_BRIDGE_GROUP_WALKOUT_RUN_SCENE
			REPEAT 4 i
				IF DOES_ENTITY_EXIST(walkout.playerPeds[i])
				AND NOT IS_PED_INJURED(walkout.playerPeds[i])
					CLEAR_ROOM_FOR_ENTITY(walkout.playerPeds[i])
					//PRINTLN("RUN_GROUP_BRIDGE_WALKOUT: forcing clone #",i," visible for walkout")
				ENDIF
			ENDREPEAT
			// start the door task
			IF DOES_ENTITY_EXIST(objID_BridgeDoor[0])
				IF NOT IS_BIT_SET(walkout.iBS,YACHT_BRIDGE_GROUP_BS_PUSH_SOUND)
					IF (GET_SYNCHRONIZED_SCENE_PHASE(walkout.iSyncedSceneID) >= 0.26)
						PLAY_SOUND_FRONTEND(-1, "PUSH", "DLC_APT_YACHT_DOOR_SOUNDS")
						SET_BIT(walkout.iBS,YACHT_BRIDGE_GROUP_BS_PUSH_SOUND)
						PRINTLN("RUN_GROUP_BRIDGE_WALKOUT: triggering door sound PUSH")
					ENDIF
				ENDIF
				IF NOT IS_BIT_SET(walkout.iBS,YACHT_BRIDGE_GROUP_BS_LIMIT_SOUND)
					IF (GET_SYNCHRONIZED_SCENE_PHASE(walkout.iSyncedSceneID) >= 0.369)
						PLAY_SOUND_FRONTEND(-1, "LIMIT", "DLC_APT_YACHT_DOOR_SOUNDS")
						SET_BIT(walkout.iBS,YACHT_BRIDGE_GROUP_BS_LIMIT_SOUND)
						PRINTLN("RUN_GROUP_BRIDGE_WALKOUT: triggering door sound LIMIT")
					ENDIF
				ENDIF
			ENDIF
			
			IF (GET_SYNCHRONIZED_SCENE_PHASE(walkout.iSyncedSceneID) >= 0.8 OR HAS_NET_TIMER_EXPIRED(walkout.timer,10000,TRUE))
				CLEANUP_GROUP_BRIDGE_WALKOUT(walkout)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC MAINTAIN_GROUP_EXIT_BRIDGE()
	IF g_bRunGroupYachtExit
		IF NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
			RUN_GROUP_BRIDGE_WALKOUT(yachtBridgeGroupWalkout)
		ELSE
			g_bRunGroupYachtExit = FALSE
			PRINTLN("MAINTAIN_GROUP_EXIT_BRIDGEg_bRunGroupYachtExit = FALSE as player joined as spectator")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_PLAYER_FINISHED_BUZZ_ANIM()
	
	IF IS_BIT_SET(iLocalBS2, LOCAL_BS2_bRunPropertySelectMenu)
	ENDIF
	
	
	IF DOES_ANYONE_IN_GAME_OWN_YACHT(iKnowOwnerState,iCurrentYacht)
	AND NOT IS_BIT_SET(iLocalBS2, LOCAL_BS2_bRunPropertySelectMenu)
	#IF IS_DEBUG_BUILD
	OR bDebugTestBuzzer 
	#ENDIF
		GET_ENTRY_CUTSCENE_DATA(ENTRY_CUTSCENE_TYPE_BUZZER)
		IF NOT RUN_ENTRY_CUTSCENE(ENTRY_CUTSCENE_TYPE_BUZZER)
			SET_BIT(iLocalBS2,LOCAL_BS2_bWalkInTriggered)
			RETURN FALSE
		ELSE
			RETURN TRUE
		ENDIF
	ELSE
		GET_ENTRY_CUTSCENE_DATA(ENTRY_CUTSCENE_TYPE_FRONT_DOOR,TRUE)
		IF NOT RUN_ENTRY_CUTSCENE(ENTRY_CUTSCENE_TYPE_FRONT_DOOR,TRUE)
			SET_BIT(iLocalBS2,LOCAL_BS2_bWalkInTriggered)
			RETURN FALSE
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ YACHT DOCKING ╞═════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛
#IF IS_DEBUG_BUILD
BOOL db_bDrawDockingDebug = FALSE
BOOL db_bRecomputeDockingPoints
#ENDIF

VECTOR vDockingZoneCentre = <<-0.0677, -53.2476, -11.0>> // this is offset from yacht
VECTOR vDockingZoneCentreInWorldCoords

TWEAK_FLOAT PRIVATE_YACHT_DOCKING_RADIUS 						9.8
TWEAK_FLOAT PRIVATE_YACHT_DOCKING_MAX_Y 					   -7.8
TWEAK_FLOAT PRIVATE_YACHT_DOCKING_MIN_DISTANCE_TO_EDGE			3.5 // minimum required distance to the edge of the yacht to trigger docking
TWEAK_FLOAT PRIVATE_YACHT_DOCKING_ZONE_WIDTH 					6.0
TWEAK_FLOAT PRIVATE_YACHT_DOCKING_ZONE_LENGTH 					12.0
TWEAK_FLOAT PRIVATE_YACHT_DOCKING_ZONE_HEADING 					210.0
TWEAK_FLOAT PRIVATE_YACHT_DOCKING_ZONE_X 					   -8.0
TWEAK_FLOAT PRIVATE_YACHT_DOCKING_ZONE_Y 					   -60.0
TWEAK_FLOAT PRIVATE_YACHT_DOCKING_MAX_ANCHORING_DISTANCE		80.0 // max distance from the docking centre in which the vehicles will automatically anchor if left alone
TWEAK_FLOAT PRIVATE_YACHT_DOCKING_MAX_DISTANCE_FOR_AUTO_DOCKING 18.0 // max distance from the docking centre in which automatic docking is available

CONST_INT PRIVATE_YACHT_DOCKING_POINTS_COUNT 20

CONST_INT YACHT_DOCKING_BS_HELP_PROMPT_SHOWN 			0
CONST_INT YACHT_DOCKING_BS_ENGINE_ANIM_REQUESTED		1
CONST_INT YACHT_DOCKING_BS_ENGINE_ANIM_STARTED			2

CONST_INT YACHT_DOCKING_STAGE_CANT_DOCK		0
CONST_INT YACHT_DOCKING_STAGE_WAITING		1
CONST_INT YACHT_DOCKING_STAGE_DOCKING		2
CONST_INT YACHT_DOCKING_STAGE_FINISHING		3
CONST_INT YACHT_DOCKING_STAGE_DONE			4
CONST_INT YACHT_DOCKING_STAGE_INTERRUPTED	5

STRUCT YACHT_DOCKING_DATA
	INT iDockingBS
	INT iDockingStage = -1
	VEHICLE_INDEX vehDockedVehicle = NULL// This is a vehicle that the player just docked, it should always be docked if it's not NULL
	
	VECTOR vPlayerCoords, vNormalisedPlayerForward
	FLOAT fPlayerHeading
	
	VECTOR vFrontOfBoat, vBackOfBoat // front and back points of currently docked vehicle
	FLOAT fBoatLength // length of curently docked vehicle
	
	VECTOR vSourceDockPosition
	FLOAT fSourceDockHeading
	VECTOR vTargetDockPosition
	FLOAT fTargetDockHeading
	
	VECTOR vDockingPoints[PRIVATE_YACHT_DOCKING_POINTS_COUNT]
	VECTOR vDockingNormals[PRIVATE_YACHT_DOCKING_POINTS_COUNT]
	FLOAT fDockingTangents[PRIVATE_YACHT_DOCKING_POINTS_COUNT]
	FLOAT fDockingHeadings[PRIVATE_YACHT_DOCKING_POINTS_COUNT]

	SCRIPT_TIMER st_VehArrayTimer
	VEHICLE_INDEX nearbyVehs[20]
	INT iNearbyVehsCount
	INT iNearbyVehsStagger
	
	STRING strEngineAnimDict
	STRING strEngineAnimClip
	TIME_DATATYPE tdEngineAnimTimer
	
	SCRIPT_TIMER st_AnchorAllBoatsTimer
	
	TIME_DATATYPE tdDockingTimer
	TIME_DATATYPE tdDockingTryAgainTimer
	FLOAT fDockingDuration
ENDSTRUCT
YACHT_DOCKING_DATA LocalDockingData

/// PURPOSE:
///    Periodically anchor all nearby free floating boats without anyone on them
/// PARAMS:
///    DockingData - 
PROC MAINTAIN_NEARBY_VEHICLES_DOCKED(YACHT_DOCKING_DATA &DockingData)

	#IF IS_DEBUG_BUILD
		// Draw the zone where boats are automatically anchored
		IF db_bDrawDockingDebug
			DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iCurrentYacht, vDockingZoneCentre), PRIVATE_YACHT_DOCKING_MAX_ANCHORING_DISTANCE, 0, 64, 128, 128)
		ENDIF
	#ENDIF

	IF NOT HAS_NET_TIMER_STARTED(DockingData.st_AnchorAllBoatsTimer)
		START_NET_TIMER(DockingData.st_AnchorAllBoatsTimer)
		EXIT
	ENDIF
	
	IF NOT HAS_NET_TIMER_EXPIRED(DockingData.st_AnchorAllBoatsTimer, 250)
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		//CDEBUG2LN(DEBUG_SAFEHOUSE, "ANCHOR_ALL_BOATS_NEARBY_WITH_NOONE_ON_THEM - start")
	#ENDIF
	
	INT i
	REPEAT IMIN(20, DockingData.iNearbyVehsCount) i
	    IF NOT DOES_ENTITY_EXIST(DockingData.nearbyVehs[i])
			#IF IS_DEBUG_BUILD
				CDEBUG2LN(DEBUG_SAFEHOUSE, "ANCHOR_ALL_BOATS_NEARBY_WITH_NOONE_ON_THEM - entity doesnt exist")
			#ENDIF
			RELOOP
		ENDIF
		
		MODEL_NAMES aModel = GET_ENTITY_MODEL(DockingData.nearbyVehs[i])
		
		#IF IS_DEBUG_BUILD
			//CDEBUG2LN(DEBUG_SAFEHOUSE, "ANCHOR_ALL_BOATS_NEARBY_WITH_NOONE_ON_THEM - checking ", GET_MODEL_NAME_FOR_DEBUG(aModel))
		#ENDIF
		
		IF NOT IS_VEHICLE_A_YACHT_DOCKABLE_VEHICLE(aModel)
			#IF IS_DEBUG_BUILD
				CDEBUG2LN(DEBUG_SAFEHOUSE, "ANCHOR_ALL_BOATS_NEARBY_WITH_NOONE_ON_THEM - not a yacht dockable vehicle")
			#ENDIF
			RELOOP
		ENDIF
		
		IF IS_VEHICLE_FUCKED_MP(DockingData.nearbyVehs[i])
			IF NETWORK_HAS_CONTROL_OF_ENTITY((DockingData.nearbyVehs[i]))
				IF IS_BOAT_ANCHORED(DockingData.nearbyVehs[i])
					SET_BOAT_ANCHOR(DockingData.nearbyVehs[i], FALSE)
					#IF IS_DEBUG_BUILD
						CDEBUG2LN(DEBUG_SAFEHOUSE, "ANCHOR_ALL_BOATS_NEARBY_WITH_NOONE_ON_THEM - removing anchor on destroyed boat")
					#ENDIF
				ENDIF
			ENDIF
			#IF IS_DEBUG_BUILD
				CDEBUG2LN(DEBUG_SAFEHOUSE, "ANCHOR_ALL_BOATS_NEARBY_WITH_NOONE_ON_THEM - can't anchor as vehicle is dead ", GET_MODEL_NAME_FOR_DEBUG(aModel))
			#ENDIF
			RELOOP
		ENDIF
		
		// Unanchor boats that go under water too much
		IF NOT IS_ENTITY_DEAD(DockingData.nearbyVehs[i])
			IF GET_ENTITY_SUBMERGED_LEVEL(DockingData.nearbyVehs[i]) > 0.99
				IF NETWORK_HAS_CONTROL_OF_ENTITY((DockingData.nearbyVehs[i]))
					IF IS_BOAT_ANCHORED(DockingData.nearbyVehs[i])
						SET_BOAT_ANCHOR(DockingData.nearbyVehs[i], FALSE)
						#IF IS_DEBUG_BUILD
							CDEBUG2LN(DEBUG_SAFEHOUSE, "ANCHOR_ALL_BOATS_NEARBY_WITH_NOONE_ON_THEM - removing anchor as vehicle is under water")
						#ENDIF
					ENDIF
				ENDIF
				RELOOP
			ENDIF
		ENDIF
		
		IF IS_ENTITY_IN_WATER(DockingData.nearbyVehs[i])
			IF GET_PED_IN_VEHICLE_SEAT(DockingData.nearbyVehs[i], VS_DRIVER) = NULL
				IF NETWORK_HAS_CONTROL_OF_ENTITY((DockingData.nearbyVehs[i]))
					IF NOT IS_BOAT_ANCHORED(DockingData.nearbyVehs[i])
						IF CAN_ANCHOR_BOAT_HERE_IGNORE_PLAYERS(DockingData.nearbyVehs[i])
							IF VDIST(vDockingZoneCentreInWorldCoords, GET_ENTITY_COORDS(DockingData.nearbyVehs[i])) < PRIVATE_YACHT_DOCKING_MAX_ANCHORING_DISTANCE
								IF (IS_PRIVATE_YACHT_ID_VALID(iCurrentYacht) AND NOT IS_POINT_IN_YACHT_BOUNDING_BOX(GET_ENTITY_COORDS(DockingData.nearbyVehs[i], FALSE), iCurrentYacht))
								OR NOT IS_PRIVATE_YACHT_ID_VALID(iCurrentYacht)
									SET_BOAT_ANCHOR(DockingData.nearbyVehs[i], TRUE)
									CDEBUG2LN(DEBUG_SAFEHOUSE, "ANCHOR_ALL_BOATS_NEARBY_WITH_NOONE_ON_THEM - Anchoring boat ", GET_MODEL_NAME_FOR_DEBUG(aModel))
								ELSE
									CDEBUG2LN(DEBUG_SAFEHOUSE, "ANCHOR_ALL_BOATS_NEARBY_WITH_NOONE_ON_THEM - can't anchor as it's in the yachts bounding box ", GET_MODEL_NAME_FOR_DEBUG(aModel))
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
									//CDEBUG2LN(DEBUG_SAFEHOUSE, "ANCHOR_ALL_BOATS_NEARBY_WITH_NOONE_ON_THEM - not docking because too far")
								#ENDIF
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
								//CDEBUG2LN(DEBUG_SAFEHOUSE, "ANCHOR_ALL_BOATS_NEARBY_WITH_NOONE_ON_THEM - cant anchor it here")
							#ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							//CDEBUG2LN(DEBUG_SAFEHOUSE, "ANCHOR_ALL_BOATS_NEARBY_WITH_NOONE_ON_THEM - already anchored")
						#ENDIF
					ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_SAFEHOUSE, "ANCHOR_ALL_BOATS_NEARBY_WITH_NOONE_ON_THEM - someone is in drivers seat")
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				//CDEBUG2LN(DEBUG_SAFEHOUSE, "ANCHOR_ALL_BOATS_NEARBY_WITH_NOONE_ON_THEM - can't anchor as vehicle is not in water ", GET_MODEL_NAME_FOR_DEBUG(aModel))
			#ENDIF
		ENDIF
	ENDREPEAT
	
	RESET_NET_TIMER(DockingData.st_AnchorAllBoatsTimer)
ENDPROC

FUNC VECTOR GET_YACHT_DOCKING_SINGLE_POINT(INT iYachtID, FLOAT fAngle)
	FLOAT fPointXCoord = vDockingZoneCentre.X + PRIVATE_YACHT_DOCKING_RADIUS * COS(fAngle)
	FLOAT fPointYCoord = vDockingZoneCentre.Y + FMAX(PRIVATE_YACHT_DOCKING_MAX_Y, PRIVATE_YACHT_DOCKING_RADIUS * SIN(fAngle))
		
	VECTOR vPoint = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iYachtID, <<fPointXCoord, fPointYCoord, 0.0>>)
	vPoint.Z = 1.0
	
	RETURN vPoint
ENDFUNC

PROC GET_YACHT_DOCKING_POINTS(INT iYachtID, VECTOR &vPoints[], VECTOR &vNormals[], FLOAT &fTangents[], FLOAT &fHeadings[], INT iCount)
	
	INT i
	FLOAT fAllowedSpace = 90.0
	FLOAT fAngleStep = fAllowedSpace / TO_FLOAT(iCount)
	FLOAT fCenteringOffset = (180.0 - fAllowedSpace) * 0.5 // Center the docking points at the back of the yacht
	
	fAngleStep = fAngleStep + fAngleStep / TO_FLOAT(iCount)
	
	FLOAT fCurrentAngle = fCenteringOffset
	
	VECTOR vPoint1
	VECTOR vPoint2
	
	REPEAT iCount i
		
		vPoints[i] = GET_YACHT_DOCKING_SINGLE_POINT(iYachtID, fCurrentAngle + 180.0)
		
		IF fHeadings[i] < 0.0
			fHeadings[i] = fHeadings[i] + 360.0
		ENDIF
		
		IF fHeadings[i] > 360.0
			fHeadings[i] = fHeadings[i] - 360.0
		ENDIF
		
		// Calculate real normal and tangent on that point
		vPoint1 = GET_YACHT_DOCKING_SINGLE_POINT(iYachtID, fCurrentAngle + 180.0 - 1.0)
		vPoint2 = GET_YACHT_DOCKING_SINGLE_POINT(iYachtID, fCurrentAngle + 180.0 + 1.0)
		
		vNormals[i] = <<vPoint1.Y - vPoint2.Y, (vPoint1.X - vPoint2.X) * (-1.0), 0.0>>
		vNormals[i] = NORMALISE_VECTOR(vNormals[i] * (-1.0)) // Flip them, for some reason they go in wrong direction
		
		fHeadings[i] = ATAN2(vPoint1.Y - vPoint2.Y, vPoint1.X - vPoint2.X)
		IF fHeadings[i] < 0.0
			fHeadings[i] = fHeadings[i] + 360.0
		ENDIF
		IF fHeadings[i] > 360.0
			fHeadings[i] = fHeadings[i] - 360.0
		ENDIF
		fTangents[i] = ATAN2(vPoint1.Y - vPoint2.Y, vPoint1.X - vPoint2.X) + 90.0
		
		fCurrentAngle = fCurrentAngle + fAngleStep
	
	ENDREPEAT
	
ENDPROC

FUNC BOOL IS_SPACE_OCCUPIED_BY_ANY_OF_BOATS_EXCEPT_PLAYERS(VECTOR vPoint, FLOAT fRadius)
	VEHICLE_INDEX playersVeh = NULL
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		playersVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	ENDIF
	
	BOOL bResult = IS_AREA_OCCUPIED(vPoint - <<fRadius, fRadius, fRadius>>, vPoint + <<fRadius, fRadius, fRadius>>, FALSE, TRUE, FALSE, FALSE, FALSE, playersVeh)// OR IS_AREA_OCCUPIED(vPoint - <<fRadius, fRadius, fRadius>>, vPoint + <<fRadius, fRadius, fRadius>>, FALSE, FALSE, TRUE, FALSE, FALSE, PLAYER_PED_ID())
	
	RETURN bResult
ENDFUNC

/// PURPOSE:
///    Get vehicle area to use with is_point_in_angled_area
/// PARAMS:
///    vVehPos - 
///    fVehicleHeading - 
///    VehicleModel - 
///    vCoords1 - 
///    vCoords2 - 
///    fWidth - 
PROC GET_VEHICLE_AREA(VECTOR vVehPos, FLOAT fVehicleHeading, MODEL_NAMES VehicleModel, VECTOR &vCoords1, VECTOR &vCoords2, FLOAT &fWidth, FLOAT fMarginOfError = 0.25, BOOL bSuperHeight = FALSE)
	// next define 3d area of vehicle
	VECTOR vForward = <<0.0, 1.0, 0.0>>
	RotateVec(vForward, <<0.0, 0.0, fVehicleHeading>> )
	vForward /= VMAG(vForward)

	VECTOR vMin, vMax
	SAFE_GET_MODEL_DIMENSIONS(VehicleModel, vMin, vMax)
	FLOAT fLength, fHeight
	
	fLength = ABSF(vMax.y - vMin.y)
	fWidth = ABSF(vMax.x - vMin.x) + fMarginOfError * 2.0
	fHeight = ABSF(vMax.z - vMin.z)
	
	IF bSuperHeight
		fHeight = fHeight + 10.0
	ENDIF
	
	//PRINTLN("GET_VEHICLE_AREA - fLength = ", fLength)
	//PRINTLN("GET_VEHICLE_AREA - fWidth = ", fWidth)
	//PRINTLN("GET_VEHICLE_AREA - fHeight = ", fHeight)
	
	vCoords1 = vVehPos + (vForward * ((0.5 * fLength) + fMarginOfError))
	vCoords1.z = (vCoords1.z - ((0.5 * fHeight) + fMarginOfError)) 
	
	vCoords2 = vVehPos - (vForward * ((0.5 * fLength) + fMarginOfError))
	vCoords2.z = (vCoords2.z + ((0.5 * fHeight) + fMarginOfError))  
ENDPROC

/// PURPOSE:
///    This is a corrected version of function from net_include.sch that accounts for margin of error on the width
/// PARAMS:
///    vPos - 
///    vVehPos - 
///    fVehicleHeading - 
///    VehicleModel - 
///    fMarginOfError - 
/// RETURNS:
///    
FUNC BOOL IS_POINT_WITHIN_VEHICLE_BOUNDS(VECTOR vPos, VECTOR vVehPos, FLOAT fVehicleHeading, MODEL_NAMES VehicleModel, FLOAT fMarginOfError = 0.1)
	// first do a basic radius check
	IF VDIST(vPos, vVehPos) < GET_MIN_RADIUS_FOR_MODEL(VehicleModel)
		VECTOR vCoords1, vCoords2
		FLOAT fWidth 
		
		GET_VEHICLE_AREA(vVehPos, fVehicleHeading, VehicleModel, vCoords1, vCoords2, fWidth, fMarginOfError)
		
		IF IS_POINT_IN_ANGLED_AREA(vPos, vCoords1, vCoords2, fWidth)			
			RETURN(TRUE)	
		ENDIF
	ENDIF

	RETURN(FALSE)
ENDFUNC

/// PURPOSE:
///    Computes point at the very front and back of the vehicle and its length
///    
/// PARAMS:
///    mnEntityModel - 
///    vCoords - 
///    fHeading - 
///    vFront - 
///    vBack - 
///    fLength - 
PROC GET_FRONT_BACK_AND_LENGTH_OF_VEHICLE(MODEL_NAMES mnEntityModel, VECTOR vCoords, FLOAT fHeading, VECTOR &vFront, VECTOR &vBack, FLOAT &fLength)
	VECTOR vMin, vMax
	SAFE_GET_MODEL_DIMENSIONS(mnEntitymodel, vMin, vMax)
	fLength = ABSF(vMax.y - vMin.y)
	//FLOAT fWidth = ABSF(vMax.x - vMin.x)
	
	vFront = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords, fHeading, <<0.0,(vMax.y),1.0>> )
	vBack = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords, fHeading, <<0.0,(vMin.y),1.00>> )
	//VECTOR vLeftOfBoat = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPlayerCoords, fPlayerHeading, <<(vMin.x),0.0,1.00>> )
	//VECTOR vRightOfBoat = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPlayerCoords, fPlayerHeading, <<(vMax.x),0.0,1.00>> )
ENDPROC

PROC GET_VEHICLE_CORNERS(MODEL_NAMES mnEntityModel, VECTOR vCoords, FLOAT fHeading, VECTOR &vCorners[])
	VECTOR vMin, vMax
	SAFE_GET_MODEL_DIMENSIONS(mnEntityModel, vMin, vMax)
	
	FLOAT fLength, fWidth, fHeight
	fLength = ABSF(vMax.y - vMin.y)
	fWidth = ABSF(vMax.x - vMin.x)
	fHeight = ABSF(vMax.z - vMin.z)
	
	VECTOR vForward = <<0.0, fLength*0.5, 0.0>>
	RotateVec(vForward, <<0.0, 0.0, fHeading>> )
	
	VECTOR vRight = <<fWidth*0.5, 0.0, 0.0>>
	RotateVec(vRight, <<0.0, 0.0, fHeading>> )
	
	vCorners[0] = vCoords + vForward + vRight 
	vCorners[0].z += 0.5 * fHeight
	
	vCorners[1] = vCoords + vForward - vRight
	vCorners[1].z += 0.5 * fHeight
	
	vCorners[2] = vCoords - vForward + vRight
	vCorners[2].z += 0.5 * fHeight
	
	vCorners[3] = vCoords - vForward - vRight
	vCorners[3].z += 0.5 * fHeight
ENDPROC

CONST_INT DOCKING_PATH_CHECK_MAX_STEPS_COUNT 18
/// PURPOSE:
///    Test if the straight path to the docking position will result in collision with anything
///    This is done by checking if any of the future positions of the vehicle (in step increments)
///    will possibly overlap with any of the vehicles already around the yacht
/// PARAMS:
///    vDestination - 
///    fDestinationHeading - 
/// RETURNS:
///
FUNC BOOL IS_DOCKING_PATH_CLEAR(VEHICLE_INDEX aPlayerVeh, VECTOR vDestination, FLOAT fHeadingChange, YACHT_DOCKING_DATA &DockingData, FLOAT fStepLength = 0.6)
	
	IF IS_ENTITY_DEAD(aPlayerVeh)
		RETURN FALSE
	ENDIF
	
	UNUSED_PARAMETER(fHeadingChange)
	
	VECTOR vPlayerCoords = GET_ENTITY_COORDS(aPlayerVeh)
	FLOAT fPlayerHeading = GET_ENTITY_HEADING(aPlayerVeh)
	
	FLOAT fTotalDistance = VMAG(vPlayerCoords - vDestination)
	VECTOR vStep = NORMALISE_VECTOR(vDestination - vPlayerCoords) * fStepLength
	INT iStepsCount = IMIN(FLOOR(fTotalDistance / fStepLength), DOCKING_PATH_CHECK_MAX_STEPS_COUNT)
	
	IF iStepsCount = 0
		RETURN TRUE
	ENDIF
	
	INT i, j, k

	// Precompute back and front points to test for each step
	//VECTOR vFronts[DOCKING_PATH_CHECK_MAX_STEPS_COUNT]
	//VECTOR vBacks[DOCKING_PATH_CHECK_MAX_STEPS_COUNT]
	
	VECTOR vTestCoords // Test point we will be generating along the route
	FLOAT fTestHeading, fPhase//, fTemp
	MODEL_NAMES mnPlayer = GET_ENTITY_MODEL(aPlayerVeh)
	VECTOR vMin, vMax
	SAFE_GET_MODEL_DIMENSIONS(mnPlayer, vMin, vMax)
	FLOAT fPlayerWidth = ABSF(vMax.x - vMin.x)
	
	VECTOR vCorners[DOCKING_PATH_CHECK_MAX_STEPS_COUNT][4]
	
	REPEAT iStepsCount i
		vTestCoords = vPlayerCoords + vStep * TO_FLOAT(i)
		fPhase = 1.0 - (VDIST(vTestCoords, vDestination) / fTotalDistance)
		fTestHeading = fPlayerHeading + fHeadingChange * fPhase
		
		//GET_FRONT_BACK_AND_LENGTH_OF_VEHICLE(mnPlayer, vTestCoords, fTestHeading, vFronts[i], vBacks[i], fTemp)
		GET_VEHICLE_CORNERS(mnPlayer, vTestCoords, fTestHeading, vCorners[i])
		
		#IF IS_DEBUG_BUILD
			IF db_bDrawDockingDebug
				DRAW_DEBUG_SPHERE(vCorners[i][0], 0.1, 255, 100, 100)
				DRAW_DEBUG_SPHERE(vCorners[i][1], 0.1, 100, 255, 100)
				DRAW_DEBUG_SPHERE(vCorners[i][2], 0.1, 100, 100, 255)
				DRAW_DEBUG_SPHERE(vCorners[i][3], 0.1, 255, 255, 100)
			ENDIF
		#ENDIF
	ENDREPEAT
	
	BOOL bPathClear = TRUE
	
	VECTOR vCoords1, vCoords2
	FLOAT fWidth
	
	// For each position and each vehicle test to see if they will overlap
	REPEAT IMIN(DockingData.iNearbyVehsCount, 20) i
		IF DOES_ENTITY_EXIST(DockingData.nearbyVehs[i])
			IF NOT IS_ENTITY_DEAD(DockingData.nearbyVehs[i])
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), DockingData.nearbyVehs[i])
					IF NOT (GET_ENTITY_POPULATION_TYPE(DockingData.nearbyVehs[i]) = PT_RANDOM_PERMANENT) // so we don't include trains etc. see 1984133
						
						GET_VEHICLE_AREA(
							GET_ENTITY_COORDS(DockingData.nearbyVehs[i]), 
							GET_ENTITY_HEADING(DockingData.nearbyVehs[i]), 
							GET_ENTITY_MODEL(DockingData.nearbyVehs[i]),
							vCoords1,
							vCoords2,
							fWidth,
							fPlayerWidth * 0.25,
							TRUE
						)
						
						REPEAT iStepsCount j
							REPEAT 4 k
								IF IS_POINT_IN_ANGLED_AREA(vCorners[j][k], vCoords1, vCoords2, fWidth)
									bPathClear = FALSE
									#IF IS_DEBUG_BUILD
										CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_DOCKING_PATH_CLEAR - potential collision with at corner: ", k, " with: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(DockingData.nearbyVehs[i])))
									#ENDIF
									BREAKLOOP
								ENDIF
							ENDREPEAT
							
							IF NOT bPathClear
								BREAKLOOP
							ENDIF
						ENDREPEAT
						
						IF NOT bPathClear
							BREAKLOOP
						ENDIF
						/*
						REPEAT iStepsCount j
							IF IS_POINT_IN_ANGLED_AREA(vBacks[j], vCoords1, vCoords2, fWidth)
								bPathClear = FALSE
								#IF IS_DEBUG_BUILD
									CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_DOCKING_PATH_CLEAR - potential back collision with: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(DockingData.nearbyVehs[i])))
								#ENDIF
								BREAKLOOP
							ENDIF
						ENDREPEAT
						*/
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT bPathClear
			BREAKLOOP
		ENDIF 
	ENDREPEAT
	
	RETURN bPathClear

ENDFUNC

FUNC BOOL IS_VEHICLE_JETSKI(ENTITY_INDEX veh)
	RETURN GET_ENTITY_MODEL(veh) = SEASHARK OR GET_ENTITY_MODEL(veh) = SEASHARK2 OR GET_ENTITY_MODEL(veh) = SEASHARK3
ENDFUNC

PROC ANCHOR_YACHT_DOCKING_VEHICLE(VEHICLE_INDEX veh)
	IF CAN_ANCHOR_BOAT_HERE_IGNORE_PLAYERS(veh)
		IF NOT IS_POINT_IN_YACHT_BOUNDING_BOX(GET_ENTITY_COORDS(veh), iCurrentYacht)
			SET_BOAT_REMAINS_ANCHORED_WHILE_PLAYER_IS_DRIVER(veh, TRUE)
			SET_BOAT_ANCHOR(veh, TRUE)	
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[YACHTDOCK] ANCHOR_YACHT_DOCKING_VEHICLE - setting anchor on vehicle.")
		ELSE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[YACHTDOCK] ANCHOR_YACHT_DOCKING_VEHICLE - can't anchor as vehicle is in bounding box.")
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[YACHTDOCK] ANCHOR_YACHT_DOCKING_VEHICLE - can't anchor on vehicle.")
	ENDIF
ENDPROC

FUNC BOOL DID_ENGINE_TURN_OFF(VEHICLE_INDEX veh)
	
	IF NOT IS_BIT_SET(LocalDockingData.iDockingBS, YACHT_DOCKING_BS_ENGINE_ANIM_REQUESTED)
	
		IF GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON
			LocalDockingData.strEngineAnimClip = "pov_start_engine"
		ELSE
			LocalDockingData.strEngineAnimClip = "start_engine"
		ENDIF
	
		LocalDockingData.strEngineAnimDict = ""
	
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), veh)
			SWITCH GET_ENTITY_MODEL(veh)
				CASE SEASHARK
				CASE SEASHARK2
				CASE SEASHARK3
					LocalDockingData.strEngineAnimDict = "veh@boat@jetski@front@base"
				BREAK
				
				CASE DINGHY
				CASE DINGHY2
				CASE DINGHY3
				CASE DINGHY4
				CASE SQUALO
				CASE TORO
				CASE TORO2
				CASE TROPIC
				CASE TROPIC2
				CASE SPEEDER
				CASE SPEEDER2
					LocalDockingData.strEngineAnimDict = "veh@boat@speed@fds@base"
				BREAK
				
				CASE PREDATOR
					LocalDockingData.strEngineAnimDict = "veh@boat@predator@ds@base"
				BREAK
				
			ENDSWITCH
		ENDIF
	
		IF NOT IS_STRING_EMPTY(LocalDockingData.strEngineAnimDict)
			REQUEST_ANIM_DICT(LocalDockingData.strEngineAnimDict)
			SET_BIT(LocalDockingData.iDockingBS, YACHT_DOCKING_BS_ENGINE_ANIM_REQUESTED)
			
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SAFEHOUSE, "DID_ENGINE_TURN_OFF - requested animation dict", LocalDockingData.strEngineAnimDict)
			#ENDIF
		ELSE
			// Turn off the engine without animation
			SET_VEHICLE_ENGINE_ON(veh, FALSE, FALSE, TRUE)
			
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SAFEHOUSE, "DID_ENGINE_TURN_OFF - dont have animation for this one, just turn off the engine")
			#ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(LocalDockingData.iDockingBS, YACHT_DOCKING_BS_ENGINE_ANIM_REQUESTED)
		IF NOT IS_BIT_SET(LocalDockingData.iDockingBS, YACHT_DOCKING_BS_ENGINE_ANIM_STARTED)
			IF HAS_ANIM_DICT_LOADED(LocalDockingData.strEngineAnimDict)
			AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), veh)
				TASK_PLAY_ANIM(PLAYER_PED_ID(), LocalDockingData.strEngineAnimDict, LocalDockingData.strEngineAnimClip, NORMAL_BLEND_IN, SLOW_BLEND_OUT)
				LocalDockingData.tdEngineAnimTimer = GET_NETWORK_TIME()
				SET_BIT(LocalDockingData.iDockingBS, YACHT_DOCKING_BS_ENGINE_ANIM_STARTED)
			ENDIF
		ELSE
			//IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PLAY_ANIM) = FINISHED_TASK
			IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), LocalDockingData.tdEngineAnimTimer)) > 1750
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				// Finished animation, we can turn the engine off
				SET_VEHICLE_ENGINE_ON(veh, FALSE, FALSE, TRUE)
				
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_SAFEHOUSE, "DID_ENGINE_TURN_OFF - animation finished, turning off the engine")
				#ENDIF
				
				LocalDockingData.strEngineAnimDict = ""
				CLEAR_BIT(LocalDockingData.iDockingBS, YACHT_DOCKING_BS_ENGINE_ANIM_REQUESTED)
				CLEAR_BIT(LocalDockingData.iDockingBS, YACHT_DOCKING_BS_ENGINE_ANIM_STARTED)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
		
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    When player applies acceleration/breaks it stops anchoring process
/// PARAMS:
///    bLeavingIsBreaking - should trying to leave the vehicle stop anchoring
/// RETURNS:
///    
FUNC BOOL HAS_PLAYER_BROKE_ANCHOR(BOOL bLeavingIsBreaking = FALSE)
	//Handle player breaking out
							
	INT  ileft_x, ileft_y

	ileft_x = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_X) - 128
	ileft_y = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y) - 128

	IF ((ileft_x >= -128) AND (ileft_x < -50))
	OR ((ileft_x <= 128) AND (ileft_x > 50))
	OR ((ileft_y >= -128) AND (ileft_y < -50))
	OR ((ileft_y <= 128) AND (ileft_y > 50))
		RETURN TRUE
	ENDIF
	
	IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
	OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
	OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
	OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
		RETURN TRUE
	ENDIF
	
	IF bLeavingIsBreaking
		IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
		OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    
///    Given two angles return the minimum angle change to go from a to b
/// PARAMS:
///    a - 
///    b - 
/// RETURNS:
///    
FUNC FLOAT GET_MINIMUM_ANGLE_ROTATION(FLOAT a, FLOAT b)
	 RETURN ATAN2(SIN(a - b), COS(a - b))
ENDFUNC

FUNC VECTOR GET_2D_LINES_INTERSECTION(FLOAT x1, FLOAT y1, FLOAT x2, FLOAT y2, FLOAT x3, FLOAT y3, FLOAT x4, FLOAT y4, BOOL &bDoIntersect)
    FLOAT d = (x1-x2)*(y3-y4) - (y1-y2)*(x3-x4)
    IF d = 0
		bDoIntersect = FALSE
		RETURN <<0.0, 0.0, 0.0>>
	ENDIF
    
	bDoIntersect = TRUE
	
    FLOAT xi = ((x3-x4)*(x1*y2-y1*x2)-(x1-x2)*(x3*y4-y3*x4))/d
    FLOAT yi = ((y3-y4)*(x1*y2-y1*x2)-(y1-y2)*(x3*y4-y3*x4))/d
    
    RETURN <<xi,yi,0.0>>
ENDFUNC

/*
FUNC VECTOR GET_SMOOTH_BOAT_TRAVEL_POINT(VECTOR vStartPosition, VECTOR vFinishPosition, FLOAT fStartAngle, FLOAT fFinishAngle, FLOAT fPhase, VECTOR &vNormal)
	
	VECTOR vA1 = vStartPosition
	VECTOR vB1 = vFinishPosition

	// Get two other points on these lines
	VECTOR vA2 = <<vStartPosition.X + COS(fStartAngle), vStartPosition.Y + SIN(fStartAngle), 0.0>>
	VECTOR vB2 = <<vFinishPosition.X + COS(fFinishAngle), vFinishPosition.Y + SIN(fFinishAngle), 0.0>>
	
	BOOL bDoIntersect
	VECTOR vControl = GET_2D_LINES_INTERSECTION(vA1.x, vA1.y, vA2.x, vA2.y, vB1.x, vB1.y, vB2.x, vB2.y, bDoIntersect)
	
	#IF IS_DEBUG_BUILD
		IF db_bDrawDockingDebug
			DRAW_DEBUG_SPHERE(vControl, 0.2, 0, 0, 255)
		ENDIF
	#ENDIF
	
	VECTOR vResult 
	
	IF bDoIntersect
		vResult = QUADRATIC_BEZIER_PLOT(vStartPosition, vControl, vFinishPosition, fPhase)
	ELSE
		vResult = LERP_VECTOR(vStartPosition, vFinishPosition, fPhase)
	ENDIF
	
	RETURN vResult 
	
ENDFUNC
*/

/// PURPOSE:
///    Given an anchor point and a position of point that we want to align with the anchor point returns the actual docking position for player's vehicle
/// PARAMS:
///    vAnchorPoint - 
///    vPlayerAnchor - absolute position of a point that we want to dock (this point will be aligned with anchor point in the end)
///    fHeadingChange - how much will the heading change in the anchored position
/// RETURNS:
///    
FUNC VECTOR GET_FINAL_ANCHOR_DESTINATION(VECTOR &vAnchorPoint, VECTOR &vPlayerAnchor, FLOAT &fHeadingChange)
	VECTOR vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
	VECTOR vParkForward = NORMALISE_VECTOR(<<vPlayerAnchor.X, vPlayerAnchor.Y, 0.0>> - <<vPlayerCoords.X, vPlayerCoords.Y, 0.0>>)
	VECTOR vDir = ROTATE_VECTOR_ABOUT_Z(vParkForward, fHeadingChange) // Final direction player will be facing after docking
			
	RETURN vAnchorPoint + VMAG(vPlayerAnchor - <<vPlayerCoords.X, vPlayerCoords.Y, 0.0>>) * (vDir * (-1.0))
ENDFUNC

FUNC BOOL CAN_VEHICLE_USE_DOCKING_MECHANISM(VEHICLE_INDEX aPlayerVeh)
	MODEL_NAMES mnEntityModel = GET_ENTITY_MODEL(aPlayerVeh)
	
	IF NOT IS_THIS_MODEL_A_BOAT(mnEntityModel)
		RETURN FALSE
	ENDIF
	
	IF mnEntityModel = MARQUIS
	OR mnEntityModel = TUG
		RETURN FALSE
	ENDIF
	
	IF DOES_THIS_VEHICLE_BELONG_TO_GANG_BOSS_MISSION(aPlayerVeh)
		#IF IS_DEBUG_BUILD
			PRINTLN("CAN_VEHICLE_USE_DOCKING_MECHANISM - No, this vehicle belongs to GB mission script.")
		#ENDIF
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

CONST_INT DOCKING_EDGE_CONTROL_POINTS_COUNT 3
FUNC BOOL IS_DOCKING_EDGE_POINT_OCCUPIED(INT iPointID, YACHT_DOCKING_DATA &DockingData)

	IF iPointID < 0 OR iPointID > PRIVATE_YACHT_DOCKING_POINTS_COUNT - 1
		RETURN TRUE
	ENDIF
	
	INT i, j
	
	// Control points run along the normal from the docking point, 1.0m apart
	VECTOR vControlPoints[DOCKING_EDGE_CONTROL_POINTS_COUNT]
	REPEAT DOCKING_EDGE_CONTROL_POINTS_COUNT i
		vControlPoints[i] = DockingData.vDockingPoints[iPointID] + (DockingData.vDockingNormals[iPointID] * TO_FLOAT(i) * 1.0)
		GET_GROUND_Z_FOR_3D_COORD(vControlPoints[i], vControlPoints[i].Z, TRUE)
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		IF db_bDrawDockingDebug
			REPEAT DOCKING_EDGE_CONTROL_POINTS_COUNT i
				DRAW_DEBUG_SPHERE(vControlPoints[i], 0.12, 255, 255, 255, 128)
			ENDREPEAT
		ENDIF
	#ENDIF

	BOOL IsOccupied = FALSE
	
	REPEAT IMIN(DockingData.iNearbyVehsCount, 20) i
	    IF DOES_ENTITY_EXIST(DockingData.nearbyVehs[i])
			IF NOT IS_ENTITY_DEAD(DockingData.nearbyVehs[i])
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), DockingData.nearbyVehs[i])
					IF NOT (GET_ENTITY_POPULATION_TYPE(DockingData.nearbyVehs[i]) = PT_RANDOM_PERMANENT) // so we don't include trains etc. see 1984133
						
						REPEAT DOCKING_EDGE_CONTROL_POINTS_COUNT j
							IF IS_POINT_WITHIN_VEHICLE_BOUNDS(vControlPoints[j], GET_ENTITY_COORDS(DockingData.nearbyVehs[i], FALSE), GET_ENTITY_HEADING(DockingData.nearbyVehs[i]), GET_ENTITY_MODEL(DockingData.nearbyVehs[i]), 0.5)
								IsOccupied = TRUE
								BREAKLOOP
							ENDIF
						ENDREPEAT
						
						IF IsOccupied
							BREAKLOOP
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF
	    ENDIF
	ENDREPEAT
	
	RETURN IsOccupied
ENDFUNC

FUNC BOOL CAN_ANCHOR_THE_BOAT_AT_EDGE(ENTITY_INDEX aPlayerVeh, VECTOR &destPos, FLOAT &destHeading, YACHT_DOCKING_DATA &DockingData)

	IF IS_ENTITY_DEAD(aPlayerVeh)
		RETURN FALSE
	ENDIF
	
	INT i
	
	#IF IS_DEBUG_BUILD
		IF db_bDrawDockingDebug
			REPEAT PRIVATE_YACHT_DOCKING_POINTS_COUNT i
				DRAW_DEBUG_SPHERE(DockingData.vDockingPoints[i], 0.15, 255, PICK_INT(i = 0, 0, 255), 255)
				DRAW_DEBUG_LINE(DockingData.vDockingPoints[i], DockingData.vDockingPoints[i] + DockingData.vDockingNormals[i], 128, 0, 128)
				DRAW_DEBUG_ARROW(DockingData.vDockingPoints[i], DockingData.fDockingHeadings[i], 0.0)
			ENDREPEAT
			
			DRAW_DEBUG_SPHERE(DockingData.vFrontOfBoat, 0.2, 255, 0, 0)
			DRAW_DEBUG_SPHERE(DockingData.vBackOfBoat, 0.2, 0, 255, 0)
			DRAW_DEBUG_LINE(DockingData.vPlayerCoords, (DockingData.vPlayerCoords + DockingData.vNormalisedPlayerForward * 3.0), 255, 255, 255)
		ENDIF
	#ENDIF
	
	// Find the point closest to the boat we want to dock
	INT iClosestPointIndex = -1
	FLOAT fDistance
	FLOAT fClosestDistance = 9999.9
	
	VECTOR vFlatDockingPoint
	VECTOR vFlatFront = <<DockingData.vFrontOfBoat.X, DockingData.vFrontOfBoat.Y, 0.0>>
	VECTOR vFlatBack = <<DockingData.vBackOfBoat.X, DockingData.vBackOfBoat.Y, 0.0>>
	
	VECTOR vFlatDockingCentre = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iCurrentYacht, vDockingZoneCentre)
	vFlatDockingCentre.Z = 0.0
	
	BOOL bFrontApproach = VMAG(vFlatDockingCentre - vFlatFront) < VMAG(vFlatDockingCentre - vFlatBack)
	VECTOR vTestAgainst
	
	BOOL bFrontParking // If the actual parking happens with front or back of the boat
	
	REPEAT PRIVATE_YACHT_DOCKING_POINTS_COUNT i
		vFlatDockingPoint = <<DockingData.vDockingPoints[i].X, DockingData.vDockingPoints[i].Y, 0.0>>
		
		IF bFrontApproach // Only allow docking front first if we are approaching from the front
			fDistance = VMAG(vFlatFront - vFlatDockingPoint)
			IF fDistance < fClosestDistance
				fClosestDistance = fDistance
				iClosestPointIndex = i
				vTestAgainst = vFlatFront
				bFrontParking = TRUE
			ENDIF
		ENDIF
		
		fDistance = VMAG(vFlatBack - vFlatDockingPoint)
		IF fDistance < fClosestDistance
			fClosestDistance = fDistance
			iClosestPointIndex = i
			vTestAgainst = vFlatBack
			bFrontParking = FALSE
		ENDIF
	ENDREPEAT
	
	IF iClosestPointIndex > -1
	
		IF IS_DOCKING_EDGE_POINT_OCCUPIED(iClosestPointIndex, DockingData)
			RETURN FALSE
		ENDIF
		
		FLOAT head1
		//FLOAT head2
	
		#IF IS_DEBUG_BUILD
			IF db_bDrawDockingDebug
				DRAW_DEBUG_SPHERE(DockingData.vDockingPoints[iClosestPointIndex], 0.3, 255, 255, 0)
			ENDIF
		#ENDIF
			
		VECTOR vRealDockPoint = DockingData.vDockingPoints[iClosestPointIndex]
		GET_GROUND_Z_FOR_3D_COORD(vRealDockPoint, vRealDockPoint.Z, TRUE)
		
		FLOAT vDistToAnchor = VDIST(<<vRealDockPoint.X, vRealDockPoint.Y, 0.0>>,  vTestAgainst)
		
		IF GET_FRAME_COUNT() % 60 = 0
			CDEBUG1LN(DEBUG_SAFEHOUSE, "vmag to closest point", vDistToAnchor)
		ENDIF
		
		IF vDistToAnchor < PRIVATE_YACHT_DOCKING_MIN_DISTANCE_TO_EDGE
			// Pick the best rotation
			
			// Rotate using heading
			head1 = DockingData.fDockingHeadings[iClosestPointIndex]
			
			IF ABSF(GET_MINIMUM_ANGLE_ROTATION(head1, DockingData.fPlayerHeading)) > ABSF(GET_MINIMUM_ANGLE_ROTATION(head1 + 180.0, DockingData.fPlayerHeading))
				head1 = head1 + 180.0
			ENDIF
				
			head1 = GET_MINIMUM_ANGLE_ROTATION(head1, DockingData.fPlayerHeading)
			
			// TODO rotate using tangent
			
			/*FLOAT maxChange = RAD_TO_DEG(VMAG(vTestAgainst - vPlayerCoords) / fLength)
			
			IF GET_FRAME_COUNT() % 60 = 0
				CDEBUG1LN(DEBUG_SAFEHOUSE, "max angle change: ", maxChange)
			ENDIF*/
			
			IF ABSF(head1) > 45.0
				RETURN FALSE // too much fiddling, skip
			/*ELSE
				IF head1 > maxChange
					head1 = maxChange
				ENDIF
				
				IF head1 < -maxChange
					head1 = -maxChange
				ENDIF*/
			ENDIF
			
			// Offset the docking point enough so the docking boat wont drive up the yacht
			//VECTOR vDir = NORMALISE_VECTOR(<<COS(fPlayerHeading + head1), SIN(fPlayerHeading + head1), 0.0>>)
			//VECTOR vT1 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPlayerCoords, fPlayerHeading + head1, <<0.0,(vMax.y),1.0>> )
			
			destHeading = head1
			VECTOR vPlayerAnchor = PICK_VECTOR(bFrontParking, DockingData.vFrontOfBoat, DockingData.vBackOfBoat)
			destPos = GET_FINAL_ANCHOR_DESTINATION(vRealDockPoint, vPlayerAnchor, destHeading)
			
			//VECTOR vParkForward = NORMALISE_VECTOR(vTestAgainst - <<vPlayerCoords.X, vPlayerCoords.Y, 0.0>>)
			//VECTOR vDir = ROTATE_VECTOR_ABOUT_Z(vParkForward, head1)
			
			//destPos = vRealDockPoint + VMAG(vTestAgainst - <<vPlayerCoords.X, vPlayerCoords.Y, 0.0>>) * (vDir * (-1.0))
			
			/*
			// If the rotation is too extreme then dont rotate
			IF ABSF(head1) > 45.0
				destHeading = 0.0
				destPos = vRealDockPoint + (vPlayerCoords - vTestAgainst)
			ELSE
				destHeading = head1
				destPos = vRealDockPoint + VMAG(vTestAgainst - vPlayerCoords) * vDockingNormals[iClosestPointIndex]
			ENDIF
			*/
			
			RETURN TRUE
		ENDIF
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC SET_YACHT_DOCKING_STAGE(INT iStage)
	LocalDockingData.iDockingStage = iStage
	/*
	SWITCH iStage
		CASE YACHT_DOCKING_STAGE_WAITING
			iDockingStage = YACHT_DOCKING_STAGE_WAITING
		BREAK
		CASE YACHT_DOCKING_STAGE_CANT_DOCK
			iDockingStage = YACHT_DOCKING_STAGE_CANT_DOCK
		BREAK
	ENDSWITCH
	*/
ENDPROC

// DOCKING ZONES STUFF

PROC GET_DOCKING_ZONE_ANCHOR_POINT(INT iZoneNumber, VECTOR &vAnchor, FLOAT &fHeading, BOOL bInWorldCoords = TRUE)
	SWITCH iZoneNumber
		CASE 0
			vAnchor = <<PRIVATE_YACHT_DOCKING_ZONE_X, PRIVATE_YACHT_DOCKING_ZONE_Y, 0.0>>
			fHeading = PRIVATE_YACHT_DOCKING_ZONE_HEADING
		BREAK
		CASE 1
			vAnchor = <<PRIVATE_YACHT_DOCKING_ZONE_X * (-1.0), PRIVATE_YACHT_DOCKING_ZONE_Y, 0.0>>
			fHeading = 180.0 - (PRIVATE_YACHT_DOCKING_ZONE_HEADING - 180.0)
		BREAK
	ENDSWITCH
	
	IF bInWorldCoords
		vAnchor = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iCurrentYacht, vAnchor)
		fHeading = GET_OFFSET_HEADING_FROM_YACHT_AS_WORLD_HEADING(iCurrentYacht, fHeading)
		GET_GROUND_Z_FOR_3D_COORD(vAnchor, vAnchor.Z, TRUE)
	ENDIF
ENDPROC

FUNC FLOAT GET_DOCKING_ZONE_LENGTH(INT iZoneNumber)
	UNUSED_PARAMETER(iZoneNumber)
	RETURN PRIVATE_YACHT_DOCKING_ZONE_LENGTH
ENDFUNC

FUNC FLOAT GET_DOCKING_ZONE_WIDTH(INT iZoneNumber)
	UNUSED_PARAMETER(iZoneNumber)
	RETURN PRIVATE_YACHT_DOCKING_ZONE_WIDTH
ENDFUNC

FUNC VECTOR GET_DOCKING_ZONE_DIRECTION(INT iZoneNumber)
	VECTOR vP1, vP2
	SWITCH iZoneNumber
		CASE 0
			vP1 = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iCurrentYacht, <<0.0, 0.0, 0.0>>)
			vP2 = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iCurrentYacht, <<-1.0, 0.0, 0.0>>)
		BREAK
		CASE 1
			vP1 = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iCurrentYacht, <<0.0, 0.0, 0.0>>)
			vP2 = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iCurrentYacht, <<1.0, 0.0, 0.0>>)
		BREAK
	ENDSWITCH
	
	RETURN NORMALISE_VECTOR(vP2 - vP1)
ENDFUNC

PROC GET_DOCKING_ZONE_AREA(INT iZoneNumber, VECTOR &v1, VECTOR &v2, FLOAT &fLength, FLOAT fAdditional = 0.0)
	VECTOR vAnchor, vDir
	FLOAT fHeading, fWidth
	
	GET_DOCKING_ZONE_ANCHOR_POINT(iZoneNumber, vAnchor, fHeading, TRUE)
	
	#IF IS_DEBUG_BUILD
		IF db_bDrawDockingDebug
			DRAW_DEBUG_SPHERE(vAnchor, 0.2, 255, 0, 0)
			DRAW_DEBUG_ARROW(vAnchor, fHeading, 0.0)
		ENDIF
	#ENDIF
	
	fWidth = GET_DOCKING_ZONE_WIDTH(iZoneNumber)
	vDir = GET_DOCKING_ZONE_DIRECTION(iZoneNumber)
	
	v1 = vAnchor
	v2 = vAnchor + vDir * (fWidth + fAdditional)
	
	v1.Z = -1.0
	v2.Z = 4.0
	
	fLength = GET_DOCKING_ZONE_LENGTH(iZoneNumber) + 2.0 * fAdditional
ENDPROC

PROC GET_DOCKING_ZONE_BOUNDING(INT iZoneNumber, VECTOR &min, VECTOR &max)
	VECTOR vDir = GET_DOCKING_ZONE_DIRECTION(iZoneNumber)
	VECTOR vTangent = ROTATE_VECTOR_ABOUT_Z(vDir, 90.0)
	VECTOR vAnchor
	FLOAT fHeading
	FLOAT fWidth = GET_DOCKING_ZONE_WIDTH(iZoneNumber)
	FLOAT fLength = GET_DOCKING_ZONE_LENGTH(iZoneNumber)
	
	GET_DOCKING_ZONE_ANCHOR_POINT(iZoneNumber, vAnchor, fHeading)
	
	FLOAT fSizeModifier = 0.75
	
	VECTOR vP1 = vAnchor + vDir * fWidth * fSizeModifier
	VECTOR vP2 = vAnchor + vTangent * (fLength * 0.5 * fSizeModifier)
	VECTOR vP3 = vAnchor - vTangent * (fLength * 0.5 * fSizeModifier)
	
	min = <<FMIN(FMIN(vP1.X, vP2.X), vP3.X), FMIN(FMIN(vP1.Y, vP2.Y), vP3.Y), -5.0>>
	max = <<FMAX(FMAX(vP1.X, vP2.X), vP3.X), FMAX(FMAX(vP1.Y, vP2.Y), vP3.Y), 5.0>>
ENDPROC

PROC GET_DOCKING_ZONE_CONTROL_POINTS(INT iZoneNumber, VECTOR &vControlPoints[], FLOAT fPointsDistance = 2.0)
	VECTOR vAnchor
	FLOAT fHeading
	GET_DOCKING_ZONE_ANCHOR_POINT(iZoneNumber, vAnchor, fHeading)
	
	VECTOR vDir = GET_DOCKING_ZONE_DIRECTION(iZoneNumber)
	VECTOR vTangent = ROTATE_VECTOR_ABOUT_Z_ORTHO(vDir, ROTSTEP_90)
	
	vControlPoints[0] = vAnchor
	vControlPoints[1] = vAnchor + vDir * fPointsDistance
	vControlPoints[2] = vAnchor + vTangent * fPointsDistance
	vControlPoints[3] = vAnchor + vTangent * (fPointsDistance * (-1.0))
	vControlPoints[4] = vControlPoints[1] + vTangent * fPointsDistance
	vControlPoints[5] = vControlPoints[1] + vTangent * (fPointsDistance * (-1.0))
	
	INT i
	REPEAT 6 i
		GET_GROUND_Z_FOR_3D_COORD(vControlPoints[i], vControlPoints[i].Z, TRUE)
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		IF db_bDrawDockingDebug
			REPEAT 6 i
				DRAW_DEBUG_SPHERE(vControlPoints[i], 0.12, 255, 255, 255, 128)
			ENDREPEAT
		ENDIF
	#ENDIF
ENDPROC

/// PURPOSE:
///    Uses control points to check if there's anything in the docking zone.
///    Excludes players own vehicle
/// PARAMS:
///    iZoneNumber - 
///    DockingData - 
/// RETURNS:
///    
FUNC BOOL IS_DOCKING_ZONE_OCCUPIED(INT iZoneNumber, YACHT_DOCKING_DATA &DockingData)
	VECTOR vControlPoints[6]
	GET_DOCKING_ZONE_CONTROL_POINTS(iZoneNumber, vControlPoints)
	
	BOOL IsOccupied = FALSE
	
	INT i, j
	REPEAT IMIN(DockingData.iNearbyVehsCount, 20) i
	    IF DOES_ENTITY_EXIST(DockingData.nearbyVehs[i])
			IF NOT IS_ENTITY_DEAD(DockingData.nearbyVehs[i])
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), DockingData.nearbyVehs[i])
					IF NOT (GET_ENTITY_POPULATION_TYPE(DockingData.nearbyVehs[i]) = PT_RANDOM_PERMANENT) // so we don't include trains etc. see 1984133
						
						REPEAT 6 j
							IF IS_POINT_WITHIN_VEHICLE_BOUNDS(vControlPoints[j], GET_ENTITY_COORDS(DockingData.nearbyVehs[i], FALSE), GET_ENTITY_HEADING(DockingData.nearbyVehs[i]), GET_ENTITY_MODEL(DockingData.nearbyVehs[i]), 0.25)
								IsOccupied = TRUE
								BREAKLOOP
							ENDIF
						ENDREPEAT
						
						IF IsOccupied
							BREAKLOOP
						ENDIF
					ENDIF
				ENDIF
			ENDIF
	    ENDIF
	ENDREPEAT
	
    RETURN IsOccupied
ENDFUNC

FUNC BOOL CAN_ANCHOR_THE_BOAT_IN_DOCKING_ZONE(ENTITY_INDEX aPlayerVeh, VECTOR &destPos, FLOAT &destHeading)

	IF IS_ENTITY_DEAD(aPlayerVeh)
		RETURN FALSE
	ENDIF
	
	VECTOR vZoneAreaPoint1, vZoneAreaPoint2, vAnchorPosition, vPlayerAnchorPoint
	FLOAT fZoneLength, fAnchorHeading
	FLOAT fRequiredRotation
	
	INT i
	
	REPEAT 2 i
		IF NOT IS_DOCKING_ZONE_OCCUPIED(i, LocalDockingData)
			GET_DOCKING_ZONE_AREA(i, vZoneAreaPoint1, vZoneAreaPoint2, fZoneLength, 1.0)
			
			IF IS_POINT_IN_ANGLED_AREA(LocalDockingData.vFrontOfBoat, vZoneAreaPoint1, vZoneAreaPoint2, fZoneLength * (-1.0))
			OR IS_POINT_IN_ANGLED_AREA(LocalDockingData.vBackOfBoat, vZoneAreaPoint1, vZoneAreaPoint2, fZoneLength * (-1.0))
			
				//GET_DOCKING_ZONE_BOUNDING(i, vBoundMin, vBoundMax)
			
				//IS_ENTITY_IN_AREA(aPlayerVeh, vBoundMin, vBoundMax)
				
				//IF NOT IS_AREA_OCCUPIED(vBoundMin, vBoundMax, FALSE, TRUE, FALSE, FALSE, FALSE, aPlayerVeh)
				//IF NOT IS_POSITION_OCCUPIED(vAnchorPosition, YACHT_DOCKING_ANCHOR_CLEAR_RADIUS, FALSE, TRUE, FALSE, FALSE, FALSE, aPlayerVeh)
				
				GET_DOCKING_ZONE_ANCHOR_POINT(i, vAnchorPosition, fAnchorHeading)
				
				IF ABSF(GET_MINIMUM_ANGLE_ROTATION(fAnchorHeading, LocalDockingData.fPlayerHeading)) > ABSF(GET_MINIMUM_ANGLE_ROTATION(fAnchorHeading + 180.0, LocalDockingData.fPlayerHeading))
					// We are parking backwards
					fAnchorHeading = fAnchorHeading + 180.0
					vPlayerAnchorPoint = LocalDockingData.vBackOfBoat
				ELSE
					// We are parking frontally
					vPlayerAnchorPoint = LocalDockingData.vFrontOfBoat
				ENDIF
				
				fRequiredRotation = GET_MINIMUM_ANGLE_ROTATION(fAnchorHeading, LocalDockingData.fPlayerHeading)
				
				IF ABSF(fRequiredRotation) < 60.0
					destPos = GET_FINAL_ANCHOR_DESTINATION(vAnchorPosition, vPlayerAnchorPoint, fRequiredRotation)
					destHeading = fRequiredRotation
					RETURN TRUE
				ENDIF
				
			ENDIF
		ELSE	
			#IF IS_DEBUG_BUILD
				CDEBUG2LN(DEBUG_SAFEHOUSE, "CAN_ANCHOR_THE_BOAT_IN_DOCKING_ZONE - No, something is taking the space, Zone ID ", i)
			#ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
	
ENDFUNC

PROC MAINTAIN_ARRAY_OF_NEARBY_VEHICLES(YACHT_DOCKING_DATA &DockingData)
	IF NOT HAS_NET_TIMER_STARTED(DockingData.st_VehArrayTimer)
		START_NET_TIMER(DockingData.st_VehArrayTimer)
		EXIT
	ENDIF
	
	IF NOT HAS_NET_TIMER_EXPIRED(DockingData.st_VehArrayTimer, 250)
		EXIT
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
	    DockingData.iNearbyVehsCount = GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), DockingData.nearbyVehs)
		#IF IS_DEBUG_BUILD
			CDEBUG2LN(DEBUG_SAFEHOUSE, "MAINTAIN_ARRAY_OF_NEARBY_VEHICLES - nearby vehs count: ", DockingData.iNearbyVehsCount)
		#ENDIF
	ENDIF
	
	RESET_NET_TIMER(DockingData.st_VehArrayTimer)
ENDPROC

PROC COMPUTE_AND_STORE_PLAYERS_FRONT_AND_BACK_AND_NORMAL(VEHICLE_INDEX &aPlayerVeh)
	MODEL_NAMES mnEntityModel = GET_ENTITY_MODEL(aPlayerVeh)
		
	LocalDockingData.vPlayerCoords = GET_ENTITY_COORDS(aPlayerVeh)
	LocalDockingData.fPlayerHeading = GET_ENTITY_HEADING(aPlayerVeh)

	GET_FRONT_BACK_AND_LENGTH_OF_VEHICLE(
		mnEntityModel, 
		LocalDockingData.vPlayerCoords, 
		LocalDockingData.fPlayerHeading, 
		LocalDockingData.vFrontOfBoat, 
		LocalDockingData.vBackOfBoat, 
		LocalDockingData.fBoatLength
	)
	
	LocalDockingData.vNormalisedPlayerForward = NORMALISE_VECTOR(LocalDockingData.vFrontOfBoat - LocalDockingData.vBackOfBoat)
ENDPROC

PROC MAINTAIN_YACHT_DOCKING()

	IF IS_YACHT_REAR_DOCKING_DISABLED()
		EXIT
	ENDIF
	
	IF LocalDockingData.iDockingStage = -1
		vDockingZoneCentreInWorldCoords = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iCurrentYacht, vDockingZoneCentre)
		GET_YACHT_DOCKING_POINTS(
			iCurrentYacht, 
			LocalDockingData.vDockingPoints,
			LocalDockingData.vDockingNormals,
			LocalDockingData.fDockingTangents, 
			LocalDockingData.fDockingHeadings, 
			PRIVATE_YACHT_DOCKING_POINTS_COUNT
		)
		SET_YACHT_DOCKING_STAGE(YACHT_DOCKING_STAGE_CANT_DOCK)
		EXIT
	ENDIF
	
	MAINTAIN_ARRAY_OF_NEARBY_VEHICLES(LocalDockingData)
	MAINTAIN_NEARBY_VEHICLES_DOCKED(LocalDockingData)
	
	VEHICLE_INDEX aPlayerVeh
	BOOL bCanAnchor = FALSE
	BOOL bPlayerDrivingDockableBoat = FALSE
	VECTOR vTargetPosition
	FLOAT fTargetHeading
	PED_INDEX playerPed = PLAYER_PED_ID()

	IF IS_PED_IN_ANY_VEHICLE(playerPed)
		IF IS_PED_IN_ANY_BOAT(playerPed)
			IF NOT IS_LOCAL_PLAYER_EXITING_A_VEHICLE()
				aPlayerVeh = GET_VEHICLE_PED_IS_IN(playerPed)
				IF CAN_VEHICLE_USE_DOCKING_MECHANISM(aPlayerVeh)
				AND NETWORK_HAS_CONTROL_OF_ENTITY(aPlayerVeh)
				AND IS_PED_DRIVING_ANY_VEHICLE(playerPed)
					bPlayerDrivingDockableBoat = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bPlayerDrivingDockableBoat
		LocalDockingData.vehDockedVehicle = NULL
		CLEAR_BIT(LocalDockingData.iDockingBS, YACHT_DOCKING_BS_ENGINE_ANIM_REQUESTED)
		CLEAR_BIT(LocalDockingData.iDockingBS, YACHT_DOCKING_BS_ENGINE_ANIM_STARTED)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF GET_FRAME_COUNT() % 60 = 0
			CDEBUG2LN(DEBUG_SAFEHOUSE, "Current docking stage: ", LocalDockingData.iDockingStage)
		ENDIF
	#ENDIF
	
	IF bPlayerDrivingDockableBoat
	AND IS_PLAYER_IN_VEHICLE_WITH_YACHT_OWNER(iCurrentYacht)
		
		COMPUTE_AND_STORE_PLAYERS_FRONT_AND_BACK_AND_NORMAL(aPlayerVeh)
		
		// Check if boat that the player is trying to dock is outside the yacht bounds...
		// (url:bugstar:2622745 - Player is able to dock the Lampadati Speeder inside the yacht.)
		IF NOT IS_POINT_IN_YACHT_BOUNDING_BOX(LocalDockingData.vFrontOfBoat, iCurrentYacht) AND NOT IS_POINT_IN_YACHT_BOUNDING_BOX(LocalDockingData.vBackOfBoat, iCurrentYacht)
			
			// There are two types of anchoring:
			// Zone anchoring - there are two zones at the sides of the yacht and boats are docked there in such a manner to be
			// as close as possible to the yacht
			bCanAnchor = CAN_ANCHOR_THE_BOAT_IN_DOCKING_ZONE(aPlayerVeh, vTargetPosition, fTargetHeading)
			
			IF NOT bCanAnchor
				// Edge docking docks boats perpendicular to the yacht around the edge
				bCanAnchor = CAN_ANCHOR_THE_BOAT_AT_EDGE(aPlayerVeh, vTargetPosition, fTargetHeading, LocalDockingData)
			ENDIF
			
			IF LocalDockingData.iDockingStage = YACHT_DOCKING_STAGE_CANT_DOCK
			OR LocalDockingData.iDockingStage = YACHT_DOCKING_STAGE_WAITING
			
				IF bCanAnchor
					IF NOT IS_DOCKING_PATH_CLEAR(aPlayerVeh, vTargetPosition, fTargetHeading, LocalDockingData)
						bCanAnchor = FALSE
						#IF IS_DEBUG_BUILD
							CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_YACHT_DOCKING: Chosen path will result in collision")
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ELSE
			//IS_DOCKING_PATH_CLEAR(aPlayerVeh, vTargetDockPosition, fTargetDockHeading, LocalDockingData)
			bCanAnchor = FALSE
		ENDIF
	ELSE
		IF LocalDockingData.iDockingStage != YACHT_DOCKING_STAGE_CANT_DOCK
			IF LocalDockingData.iDockingStage = YACHT_DOCKING_STAGE_DOCKING
			OR LocalDockingData.iDockingStage = YACHT_DOCKING_STAGE_FINISHING
				// If player breaks out of docking procedure then remember to clean it up which restores control and other things
				LocalDockingData.iDockingStage = YACHT_DOCKING_STAGE_INTERRUPTED
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_YACHT_DOCKING: Docking interrupted!")
				#ENDIF
			ELSE
				LocalDockingData.iDockingStage = YACHT_DOCKING_STAGE_CANT_DOCK
			ENDIF
		ENDIF
	ENDIF
	
	IF LocalDockingData.iDockingStage = YACHT_DOCKING_STAGE_CANT_DOCK
	OR LocalDockingData.iDockingStage = YACHT_DOCKING_STAGE_WAITING
		IF bPlayerDrivingDockableBoat
			// All boats that are anchored near yacht are permanently anchored so detect using input to break out of the anchor
			IF IS_BOAT_ANCHORED(aPlayerVeh) OR LocalDockingData.vehDockedVehicle = aPlayerVeh
				IF HAS_PLAYER_BROKE_ANCHOR()
					SET_BOAT_ANCHOR(aPlayerVeh, FALSE)
					SET_BOAT_REMAINS_ANCHORED_WHILE_PLAYER_IS_DRIVER(aPlayerVeh, FALSE)
					LocalDockingData.vehDockedVehicle = NULL
				ENDIF
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
			// Draw the zone where boats can use docking mechanism
			IF db_bDrawDockingDebug
				DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iCurrentYacht, vDockingZoneCentre), PRIVATE_YACHT_DOCKING_MAX_DISTANCE_FOR_AUTO_DOCKING, 0, 64, 256, 128)
			ENDIF
		#ENDIF
		
		IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iCurrentYacht, vDockingZoneCentre)) <= PRIVATE_YACHT_DOCKING_MAX_DISTANCE_FOR_AUTO_DOCKING
		
			#IF IS_DEBUG_BUILD
				IF GET_FRAME_COUNT() % 60 = 0
					CDEBUG1LN(DEBUG_SAFEHOUSE, "We are in the docking zone!")
					CDEBUG1LN(DEBUG_SAFEHOUSE, "Can anchor ", bCanAnchor)
				ENDIF
			#ENDIF
		
			IF bCanAnchor
				SET_YACHT_DOCKING_STAGE(YACHT_DOCKING_STAGE_WAITING)
			ELSE
				SET_YACHT_DOCKING_STAGE(YACHT_DOCKING_STAGE_CANT_DOCK)
			ENDIF
		ELSE
			SET_YACHT_DOCKING_STAGE(YACHT_DOCKING_STAGE_CANT_DOCK)
		ENDIF
	ENDIF
	
	// Maintain the docking prompt
	IF LocalDockingData.iDockingStage = YACHT_DOCKING_STAGE_WAITING
	AND NOT IS_BOAT_ANCHORED(aPlayerVeh)
	AND LocalDockingData.vehDockedVehicle != aPlayerVeh
		//IF NOT IS_BIT_SET(LocalDockingData.iDockingBS, YACHT_DOCKING_BS_HELP_PROMPT_SHOWN)
			PRINT_HELP("YACHT_HELP_01")
			SET_BIT(LocalDockingData.iDockingBS, YACHT_DOCKING_BS_HELP_PROMPT_SHOWN)
		//ENDIF
	ELSE
		IF IS_BIT_SET(LocalDockingData.iDockingBS, YACHT_DOCKING_BS_HELP_PROMPT_SHOWN)
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("YACHT_HELP_01")
				CLEAR_HELP()
			ENDIF
			CLEAR_BIT(LocalDockingData.iDockingBS, YACHT_DOCKING_BS_HELP_PROMPT_SHOWN)
		ENDIF
	ENDIF
	
	FLOAT fPhase
	VECTOR vTemp
	
	// Docking stage handling
	SWITCH LocalDockingData.iDockingStage
	
		// Awaiting player input
		CASE YACHT_DOCKING_STAGE_WAITING
		
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			
			IF bPlayerDrivingDockableBoat
				IF NOT IS_BOAT_ANCHORED(aPlayerVeh) AND NOT IS_ENTITY_DEAD(aPlayerVeh)
					IF IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_CONTEXT)
						
						// START DOCKING PROCEDURE
						
						LocalDockingData.vTargetDockPosition = vTargetPosition
						LocalDockingData.fTargetDockHeading = fTargetHeading
						LocalDockingData.vSourceDockPosition = GET_ENTITY_COORDS(aPlayerVeh)
						LocalDockingData.fSourceDockHeading = GET_ENTITY_HEADING(aPlayerVeh)
						
						LocalDockingData.tdDockingTimer = GET_NETWORK_TIME()
						LocalDockingData.fDockingDuration = VDIST(LocalDockingData.vSourceDockPosition, LocalDockingData.vTargetDockPosition) * 500 + 300
						
						LocalDockingData.strEngineAnimDict = ""
						CLEAR_BIT(LocalDockingData.iDockingBS, YACHT_DOCKING_BS_ENGINE_ANIM_REQUESTED)
						CLEAR_BIT(LocalDockingData.iDockingBS, YACHT_DOCKING_BS_ENGINE_ANIM_STARTED)
						
						#IF IS_DEBUG_BUILD
							CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_YACHT_DOCKING - STARTING THE DOCKING PROCEDURE! Duration: ", LocalDockingData.fDockingDuration)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "heading change: ", LocalDockingData.fTargetDockHeading)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "final heading: ", LocalDockingData.fSourceDockHeading + LocalDockingData.fTargetDockHeading)
						#ENDIF
						
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
						
						IF IS_VEHICLE_JETSKI(aPlayerVeh)
							PLAY_SOUND_FROM_ENTITY(-1, "Moor_SEASHARK_Engine", aPlayerVeh, "DLC_Apt_Yacht_Ambient_Soundset", TRUE, 30)
						ELSE
							PLAY_SOUND_FROM_ENTITY(-1, "Moor_Boat_Engine", aPlayerVeh, "DLC_Apt_Yacht_Ambient_Soundset", TRUE, 30)
						ENDIF
						
						SET_YACHT_DOCKING_STAGE(YACHT_DOCKING_STAGE_DOCKING)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		// Docking in progress
		CASE YACHT_DOCKING_STAGE_DOCKING
			
			IF HAS_PLAYER_BROKE_ANCHOR(FALSE) OR NOT bPlayerDrivingDockableBoat
				SET_YACHT_DOCKING_STAGE(YACHT_DOCKING_STAGE_INTERRUPTED)
				EXIT
			ENDIF
			
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_YACHT_DOCKING: Docking stage: YACHT_DOCKING_STAGE_DOCKING")
			#ENDIF
		
			fPhase = COSINE_INTERP_FLOAT(0.0, 1.0, TO_FLOAT(ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), LocalDockingData.tdDockingTimer))) / LocalDockingData.fDockingDuration)
			
			vTemp = LERP_VECTOR(LocalDockingData.vSourceDockPosition, LocalDockingData.vTargetDockPosition, fPhase)
			
			GET_GROUND_Z_FOR_3D_COORD(vTemp, vTemp.Z, TRUE)
			
			SET_ENTITY_COORDS_NO_OFFSET(aPlayerVeh, vTemp, FALSE, FALSE, FALSE)
			SET_ENTITY_ROTATION(aPlayerVeh , <<0.0, 0.0, LocalDockingData.fSourceDockHeading + LocalDockingData.fTargetDockHeading * fPhase>>, DEFAULT, FALSE)
			
			//inc = (vTargetDockPosition - vSourceDockPosition) * 0.24
			//SLIDE_OBJECT(GET_OBJECT_INDEX_FROM_ENTITY_INDEX(aPlayerVeh), vTargetDockPosition, inc, TRUE)
			
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) != CAM_VIEW_MODE_FIRST_PERSON
			AND IS_VEHICLE_JETSKI(aPlayerVeh)
				SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_ForceIntoStandPoseOnJetski , TRUE)
			ENDIF
			
			IF fPhase > 0.99
				SET_YACHT_DOCKING_STAGE(YACHT_DOCKING_STAGE_FINISHING)
				LocalDockingData.tdDockingTimer = GET_NETWORK_TIME()
			ENDIF
		BREAK
		
		// Docked vehicle is in correct position, now attempt to dock it
		CASE YACHT_DOCKING_STAGE_FINISHING
		
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_YACHT_DOCKING: Docking stage: YACHT_DOCKING_STAGE_FINISHING")
			#ENDIF
			
			IF HAS_PLAYER_BROKE_ANCHOR(TRUE) OR NOT bPlayerDrivingDockableBoat
				SET_YACHT_DOCKING_STAGE(YACHT_DOCKING_STAGE_DONE)
				EXIT
			ENDIF
			
			ANCHOR_YACHT_DOCKING_VEHICLE(aPlayerVeh)
			
			IF IS_BOAT_ANCHORED(aPlayerVeh)
			OR ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), LocalDockingData.tdDockingTimer)) > 2400 // Give it 2 secs to sort itself out, if it doesnt then screw it, ambient script will sort out the docking if the boat drifts too far
				IF DID_ENGINE_TURN_OFF(aPlayerVeh)
					SET_YACHT_DOCKING_STAGE(YACHT_DOCKING_STAGE_DONE)
				ENDIF
			ENDIF
		BREAK
		
		// Docking done, successful, do a cleanup, restore control
		CASE YACHT_DOCKING_STAGE_DONE
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_YACHT_DOCKING: Docking stage: YACHT_DOCKING_STAGE_DONE")
			#ENDIF
			IF bPlayerDrivingDockableBoat
				LocalDockingData.vehDockedVehicle = aPlayerVeh
			ENDIF
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_ForceIntoStandPoseOnJetski , FALSE)
			LocalDockingData.tdDockingTryAgainTimer = GET_NETWORK_TIME()
			SET_YACHT_DOCKING_STAGE(YACHT_DOCKING_STAGE_WAITING)
		BREAK
		
		// Player interuppted the docking stage, do the usual cleanup but don't try to anchor the vehicle
		CASE YACHT_DOCKING_STAGE_INTERRUPTED
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_YACHT_DOCKING: Docking stage: YACHT_DOCKING_STAGE_INTERRUPTED")
			#ENDIF
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_ForceIntoStandPoseOnJetski , FALSE)
			SET_YACHT_DOCKING_STAGE(YACHT_DOCKING_STAGE_WAITING)
		BREAK
	ENDSWITCH
	
	// Try to anchor the boat the player just tried to anchor if it's not yet anchored,
	// which sometimes happens when the boat collides with something
	// This mitigates the issue when player just used the docking mechanism and
	// then the boat starts drifting away and is not docked at all
	IF bPlayerDrivingDockableBoat
		IF LocalDockingData.vehDockedVehicle = aPlayerVeh
		AND NOT IS_BOAT_ANCHORED(aPlayerVeh)
			IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), LocalDockingData.tdDockingTryAgainTimer)) > 250
				ANCHOR_YACHT_DOCKING_VEHICLE(aPlayerVeh)
				LocalDockingData.tdDockingTryAgainTimer = GET_NETWORK_TIME()
				CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_YACHT_DOCKING - Tried to dock again")
			ENDIF
		ENDIF
	ENDIF

ENDPROC

/////////////////////////////////////////
// END OF DOCKING STUFF /////////////////
/////////////////////////////////////////

BOOL bRobeChecksComplete = FALSE
PROC MAINTAIN_YACHT_CLOTHING_GIFT()
	IF iCurrentYacht >= 0
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND IS_PROPERTY_YACHT_APARTMENT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
	AND NOT bRobeChecksComplete
		SWITCH GET_ENTITY_MODEL(PLAYER_PED_ID())
			CASE MP_M_FREEMODE_01
				IF NOT IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_APA_M_JBIB_23_0"), PED_COMPONENT_ACQUIRED_SLOT)
					IF USE_SERVER_TRANSACTIONS()
						IF GET_BASKET_TRANSACTION_SCRIPT_INDEX() = -1
							IF NOT NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_CLOTH, HASH("M_CL_CLO_APM_U_23_0_t11_v2"), NET_SHOP_ACTION_SPEND, 1, 0, 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, 0, CTPF_AUTO_PROCESS_REPLY)
							OR NOT NETWORK_START_BASKET_TRANSACTION_CHECKOUT()
								DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
								bRobeChecksComplete = TRUE
							ENDIF
						ENDIF
					ELSE
						SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_APA_M_JBIB_23_0"), PED_COMPONENT_ACQUIRED_SLOT)
					ENDIF
				ELSE
					bRobeChecksComplete = TRUE
				ENDIF
			BREAK
			CASE MP_F_FREEMODE_01
				IF NOT IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_APA_F_JBIB_23_0"), PED_COMPONENT_ACQUIRED_SLOT)
					IF USE_SERVER_TRANSACTIONS()
						IF GET_BASKET_TRANSACTION_SCRIPT_INDEX() = -1
							IF NOT NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_CLOTH, HASH("F_CL_CLO_APF_U_23_0_t11_v2"), NET_SHOP_ACTION_SPEND, 1, 0, 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, 0, CTPF_AUTO_PROCESS_REPLY)
							OR NOT NETWORK_START_BASKET_TRANSACTION_CHECKOUT()
								DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
								bRobeChecksComplete = TRUE
							ENDIF
						ENDIF
					ELSE
						SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_APA_F_JBIB_23_0"), PED_COMPONENT_ACQUIRED_SLOT)
					ENDIF
				ELSE
					bRobeChecksComplete = TRUE
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC


FUNC BOOL IS_JACUZZI_ACTIVITY_DISABLED()
	#IF IS_DEBUG_BUILD
	IF g_sMPTunables.byacht_disable_hottub
		CDEBUG2LN(DEBUG_SAFEHOUSE, "IS_JACUZZI_ACTIVITY_DISABLED: byacht_disable_hottub = TRUE")
	ENDIF
	#ENDIF
	
	IF FMMC_CURRENTLY_USING_PLAYER_OWNED_YACHT()
	
		IF GET_YACHT_LOCAL_PLAYER_IS_ON(TRUE) > -1
			IF IS_BIT_SET(g_FMMC_STRUCT.sMissionYachtInfo[ciYACHT_LOBBY_HOST_YACHT_INDEX].iMYacht_Bitset, ciYACHT_BITSET__BLOCK_JACUZZI_ORION)
			AND GET_OPTION_OF_PRIVATE_YACHT(GET_YACHT_LOCAL_PLAYER_IS_ON(TRUE)) = ciYACHT_MODEL__ORION
				RETURN TRUE
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sMissionYachtInfo[ciYACHT_LOBBY_HOST_YACHT_INDEX].iMYacht_Bitset, ciYACHT_BITSET__BLOCK_JACUZZI_PISCES)
			AND GET_OPTION_OF_PRIVATE_YACHT(GET_YACHT_LOCAL_PLAYER_IS_ON(TRUE)) = ciYACHT_MODEL__PISCES
				RETURN TRUE
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sMissionYachtInfo[ciYACHT_LOBBY_HOST_YACHT_INDEX].iMYacht_Bitset, ciYACHT_BITSET__BLOCK_JACUZZI_AQUARIUS)
			AND GET_OPTION_OF_PRIVATE_YACHT(GET_YACHT_LOCAL_PLAYER_IS_ON(TRUE)) = ciYACHT_MODEL__AQUARIUS
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF g_bMissionEnding
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN g_sMPTunables.byacht_disable_hottub    
ENDFUNC

FUNC BOOL IS_JACUZZI_JACUZZI_CLOTHING_FUNC_DISABLED()
	
	//dont update the clothes for hte jacuzzi if the player is the beast
	IF IS_PLAYER_BEAST(PLAYER_ID())
	OR IS_PED_WEARING_JUGGERNAUT_SUIT(PLAYER_PED_ID())
		RETURN TRUE
	ENDIF
	
	IF FMMC_CURRENTLY_USING_PLAYER_OWNED_YACHT()
	
		IF g_bMissionEnding
			RETURN TRUE
		ENDIF
		
		IF GET_YACHT_LOCAL_PLAYER_IS_ON(TRUE) > -1
			IF IS_BIT_SET(g_FMMC_STRUCT.sMissionYachtInfo[ciYACHT_LOBBY_HOST_YACHT_INDEX].iMYacht_Bitset, ciYACHT_BITSET__BLOCK_JACUZZI_ORION)
			AND GET_OPTION_OF_PRIVATE_YACHT(GET_YACHT_LOCAL_PLAYER_IS_ON(TRUE)) = ciYACHT_MODEL__ORION
				RETURN TRUE
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sMissionYachtInfo[ciYACHT_LOBBY_HOST_YACHT_INDEX].iMYacht_Bitset, ciYACHT_BITSET__BLOCK_JACUZZI_PISCES)
			AND GET_OPTION_OF_PRIVATE_YACHT(GET_YACHT_LOCAL_PLAYER_IS_ON(TRUE)) = ciYACHT_MODEL__PISCES
				RETURN TRUE
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sMissionYachtInfo[ciYACHT_LOBBY_HOST_YACHT_INDEX].iMYacht_Bitset, ciYACHT_BITSET__BLOCK_JACUZZI_AQUARIUS)
			AND GET_OPTION_OF_PRIVATE_YACHT(GET_YACHT_LOCAL_PLAYER_IS_ON(TRUE)) = ciYACHT_MODEL__AQUARIUS
				RETURN TRUE
			ENDIF
		ELSE
			// If we're not sure then block it during instanced missions just in case
			RETURN TRUE
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF g_sMPTunables.byacht_disable_hottub_swimwear
		CDEBUG2LN(DEBUG_SAFEHOUSE, "IS_JACUZZI_ACTIVITY_DISABLED: byacht_disable_hottub_swimwear = TRUE")
	ENDIF
	#ENDIF
	
	RETURN g_sMPTunables.byacht_disable_hottub_swimwear
ENDFUNC

PROC MAINTAIN_JACUZZI_CLOTHING()
	IF NOT IS_JACUZZI_JACUZZI_CLOTHING_FUNC_DISABLED()
		IF IS_BIT_SET(iJacuzziBitSet, JAC_BS_PLAYER_INSIDE_INNER_JAC_SPHERE)
			IF GET_PRIVATE_YACHT_USE_SWIMWEAR(iCurrentYacht)
				// This yacht requires swimwear but player ain't sporting it, change him
				PLAYER_WEAR_SWIMWEAR(SwimwearData, serverBD.iSwimwearOptions[NATIVE_TO_INT(PLAYER_ID())])
			ELSE
				PLAYER_RESTORE_CLOTHES(SwimwearData)
			ENDIF
		ELSE
			IF IS_BIT_SET(iJacuzziBitSet, JAC_BS_PLAYER_OUTSIDE_OUTTER_JAC_SPHERE)
				PLAYER_RESTORE_CLOTHES(SwimwearData)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_LEAN_ACTIVITY_DISABLED()

	IF FMMC_CURRENTLY_USING_PLAYER_OWNED_YACHT()
		IF IS_BIT_SET(g_FMMC_STRUCT.sMissionYachtInfo[ciYACHT_LOBBY_HOST_YACHT_INDEX].iMYacht_Bitset, ciYACHT_BITSET__BLOCK_RAIL_LEANING)
			#IF IS_DEBUG_BUILD
			IF GET_FRAME_COUNT() % 60 = 0
				PRINTLN("IS_LEAN_ACTIVITY_DISABLED | Disabled due to FMMC_CURRENTLY_USING_PLAYER_OWNED_YACHT")
			ENDIF
			#ENDIF
			
			RETURN TRUE
		ENDIF
		
		IF g_bMissionEnding
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

PROC REQUEST_YACHT_EFFECTS()
	REQUEST_NAMED_PTFX_ASSET("scr_apartment_mp")
ENDPROC

PROC RELEASE_YACHT_EFFECTS()
	REMOVE_NAMED_PTFX_ASSET("scr_apartment_mp")
ENDPROC

/// PURPOSE:
///    Controls local exterior yacht sounds and vfx
PROC MAINTAIN_YACHT_EXTERIOR_EFFECTS()

	IF IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_EXTERIOR_EFFECTS_STARTED)
		EXIT
	ENDIF
	
	// We need jacuzzi positions calculated to start the sounds at correct positions, wait for them to be calculated
	IF NOT IS_BIT_SET(iJacuzziBitSet, JAC_BS_POSITIONS_CALCULATED)
		EXIT
	ENDIF

	#IF IS_DEBUG_BUILD
		IF mpYachtEffects.iEngineSoundID = -1
			mpYachtEffects.vJacuzziSteamOffset = <<-0.150, -52.470, -5.820>>
		ENDIF
	#ENDIF

	IF NOT IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_GOT_SOUND_IDS)
		mpYachtEffects.iEngineSoundID = GET_SOUND_ID()
		mpYachtEffects.iLowerDeckSoundID = GET_SOUND_ID()
		mpYachtEffects.iHigherDeckSoundID = GET_SOUND_ID()
		
		IF DOES_YACHT_HAVE_JACUZZI(iCurrentYacht)
			mpYachtEffects.iJacuzziSoundID = GET_SOUND_ID()
		ENDIF
		
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_YACHT_EXTERIOR_EFFECTS - Got sound IDs")
		#ENDIF
		
		SET_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_GOT_SOUND_IDS)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_YACHT_EXTERIOR_EFFECTS - Effects are not started on this yacht, requesting them")
	#ENDIF

	IF REQUEST_SCRIPT_AUDIO_BANK("DLC_APARTMENT/APT_Yacht_01")
	AND HAS_NAMED_PTFX_ASSET_LOADED("scr_apartment_mp")
		VECTOR vEnginePosition = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iCurrentYacht, <<-0.3605, -58.7587, -8.9570>>)
		VECTOR vLowerDeckPosition = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iCurrentYacht, <<-0.2356, -24.0488, 0.5037>>)
		VECTOR vHigherDeckPosition = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iCurrentYacht, <<-0.0944, 28.8718, -2.3929>>)
		VECTOR vJacuzziPosition = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iCurrentYacht, <<-0.150, -52.470, -5.820>>)
		VECTOR vJacuzziWaterPosition = <<vJacuzziPosition.X, vJacuzziPosition.Y, fJacuzziActualWaterLevel>>
		
		#IF IS_DEBUG_BUILD
		vJacuzziPosition = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iCurrentYacht, mpYachtEffects.vJacuzziSteamOffset) // Jacuzzi sink
		#ENDIF
		
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_YACHT_EXTERIOR_EFFECTS - Effects request successful")
		#ENDIF
		
		// Engine sound
		PLAY_SOUND_FROM_COORD(mpYachtEffects.iEngineSoundID, "Engine", vEnginePosition, "DLC_Apt_Yacht_Ambient_Soundset", FALSE, 0, TRUE)
		
		// Lower deck sound
		PLAY_SOUND_FROM_COORD(mpYachtEffects.iLowerDeckSoundID, "Deck", vLowerDeckPosition, "DLC_Apt_Yacht_Ambient_Soundset", FALSE, 0, TRUE)
		
		// Higher deck sound
		PLAY_SOUND_FROM_COORD(mpYachtEffects.iHigherDeckSoundID, "Deck", vHigherDeckPosition, "DLC_Apt_Yacht_Ambient_Soundset", FALSE, 0, TRUE)
		
		IF DOES_YACHT_HAVE_JACUZZI(iCurrentYacht)
			// Jacuzzi sound
			PLAY_SOUND_FROM_COORD(mpYachtEffects.iJacuzziSoundID, "Hot_Tub_Loop", vJacuzziWaterPosition, "GTAO_Yacht_SoundSet", FALSE, 0, TRUE)
			
			// Jacuzzi steam effect
			USE_PARTICLE_FX_ASSET("scr_apartment_mp")
			mpYachtEffects.ptfxJacuzziSteam = START_PARTICLE_FX_LOOPED_AT_COORD("scr_apa_jacuzzi_steam", vJacuzziPosition, <<0.0, 0.0, 0.0>>, 1.0, FALSE, FALSE, FALSE, TRUE)
		ENDIF
		
		SET_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_EXTERIOR_EFFECTS_STARTED)
	ENDIF
ENDPROC

PROC CLEANUP_YACHT_EXTERIOR_EFFECTS()
	IF IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_EXTERIOR_EFFECTS_STARTED)
		STOP_SOUND(mpYachtEffects.iEngineSoundID)
		STOP_SOUND(mpYachtEffects.iLowerDeckSoundID)
		STOP_SOUND(mpYachtEffects.iHigherDeckSoundID)
		IF DOES_YACHT_HAVE_JACUZZI(iCurrentYacht)
			STOP_SOUND(mpYachtEffects.iJacuzziSoundID)
		ENDIF
			
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_APARTMENT/APT_Yacht_01")
		
		IF DOES_YACHT_HAVE_JACUZZI(iCurrentYacht)
			IF DOES_PARTICLE_FX_LOOPED_EXIST(mpYachtEffects.ptfxJacuzziSteam)
				REMOVE_PARTICLE_FX(mpYachtEffects.ptfxJacuzziSteam, TRUE)
			ENDIF
		ENDIF
		
		CLEAR_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_EXTERIOR_EFFECTS_STARTED)
	ENDIF
	
	IF IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_GOT_SOUND_IDS)
		RELEASE_SOUND_ID(mpYachtEffects.iEngineSoundID)
		RELEASE_SOUND_ID(mpYachtEffects.iLowerDeckSoundID)
		RELEASE_SOUND_ID(mpYachtEffects.iHigherDeckSoundID)
	
		mpYachtEffects.iEngineSoundID = -1
		mpYachtEffects.iLowerDeckSoundID = -1
		mpYachtEffects.iHigherDeckSoundID = -1
		
		IF DOES_YACHT_HAVE_JACUZZI(iCurrentYacht)
			RELEASE_SOUND_ID(mpYachtEffects.iJacuzziSoundID)
			mpYachtEffects.iJacuzziSoundID = -1
		ENDIF
		
		CLEAR_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_GOT_SOUND_IDS)
		
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_YACHT_EXTERIOR_EFFECTS - releasing sound ids")
		#ENDIF
	ENDIF
		
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_YACHT_EXTERIOR_EFFECTS - cleaned up")
	#ENDIF
		
ENDPROC

PROC RUN_JACUZZI_EFFECT_WADE(VECTOR &vPos)
	IF NOT IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_WADE_STARTED)
		IF HAS_NAMED_PTFX_ASSET_LOADED("scr_apartment_mp")
			USE_PARTICLE_FX_ASSET("scr_apartment_mp")
			mpYachtEffects.ptfxJacuzziWade = START_PARTICLE_FX_LOOPED_AT_COORD(
				"scr_apa_jacuzzi_wade", 
				vPos,
				<<0.0, 0.0, 0.0>>, 
				1.0, 
				FALSE, 
				FALSE, 
				FALSE,
				TRUE
			)
			SET_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_WADE_STARTED)
			
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SAFEHOUSE, "START_JACUZZI_EFFECT_WADE - started")
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC STOP_JACUZZI_EFFECT_WADE()
	IF IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_WADE_STARTED)
		STOP_PARTICLE_FX_LOOPED(mpYachtEffects.ptfxJacuzziWade, TRUE)
		CLEAR_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_WADE_STARTED)
		
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_SAFEHOUSE, "STOP_JACUZZI_EFFECT_WADE - stopped")
		#ENDIF
	ENDIF
ENDPROC

PROC RUN_JACUZZI_EFFECT_DRIPS()
	IF NOT IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_DRIPS_STARTED)
		IF HAS_NAMED_PTFX_ASSET_LOADED("scr_apartment_mp")
		
			USE_PARTICLE_FX_ASSET("scr_apartment_mp")
			mpYachtEffects.ptfxJacuzziDripsLHand = START_PARTICLE_FX_LOOPED_ON_ENTITY_BONE(
				"scr_apa_jacuzzi_drips", 
				PLAYER_PED_ID(), 
				<<0.0, 0.0, 0.0>>, 
				<<0.0, 0.0, 0.0>>, 
				GET_PED_BONE_INDEX(PLAYER_PED_ID(),BONETAG_L_HAND)
			)
			
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SAFEHOUSE, "START_JACUZZI_EFFECT_DRIPS - started left hand drips at bone ", GET_PED_BONE_INDEX(PLAYER_PED_ID(),BONETAG_L_HAND))
			#ENDIF
			
			USE_PARTICLE_FX_ASSET("scr_apartment_mp")
			mpYachtEffects.ptfxJacuzziDripsRHand = START_PARTICLE_FX_LOOPED_ON_ENTITY_BONE(
				"scr_apa_jacuzzi_drips", 
				PLAYER_PED_ID(), 
				<<0.0, 0.0, 0.0>>, 
				<<0.0, 0.0, 0.0>>, 
				GET_PED_BONE_INDEX(PLAYER_PED_ID(),BONETAG_R_HAND)
			)
			
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SAFEHOUSE, "START_JACUZZI_EFFECT_DRIPS - started right hand drips at bone ", GET_PED_BONE_INDEX(PLAYER_PED_ID(),BONETAG_R_HAND))
			#ENDIF
			
			SET_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_DRIPS_STARTED)
			mpYachtEffects.tdJacuzziDripsTimer = GET_NETWORK_TIME()
			
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SAFEHOUSE, "START_JACUZZI_EFFECT_DRIPS - started drips")
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC STOP_JACUZZI_EFFECT_DRIPS()
	IF IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_DRIPS_STARTED)
		STOP_PARTICLE_FX_LOOPED(mpYachtEffects.ptfxJacuzziDripsLHand, TRUE)
		STOP_PARTICLE_FX_LOOPED(mpYachtEffects.ptfxJacuzziDripsRHand, TRUE)
		CLEAR_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_DRIPS_STARTED)
	ENDIF
ENDPROC

PROC MAINTAIN_JACUZZI_EFFECTS()

	// Request sound IDs as soon as player is near the jacuzzi
	IF NOT IS_BIT_SET(iJacuzziBitSet, JAC_BS_PLAYER_OUTSIDE_OUTTER_JAC_SPHERE)
		IF mpYachtEffects.iSplashSoundID = -1
			mpYachtEffects.iSplashSoundID = GET_SOUND_ID()
			#IF IS_DEBUG_BUILD
				CDEBUG2LN(DEBUG_SAFEHOUSE, "MAINTAIN_JACUZZI_EFFECTS - got sound ID iSplashSoundID")
			#ENDIF
		ENDIF
		
		IF mpYachtEffects.iWaterExitSoundID = -1
			mpYachtEffects.iWaterExitSoundID = GET_SOUND_ID()
			#IF IS_DEBUG_BUILD
				CDEBUG2LN(DEBUG_SAFEHOUSE, "MAINTAIN_JACUZZI_EFFECTS - got sound ID iWaterExitSoundID")
			#ENDIF
		ENDIF
	ELSE
		IF mpYachtEffects.iSplashSoundID != -1
			RELEASE_SOUND_ID(mpYachtEffects.iSplashSoundID)
			mpYachtEffects.iSplashSoundID = -1
			#IF IS_DEBUG_BUILD
				CDEBUG2LN(DEBUG_SAFEHOUSE, "MAINTAIN_JACUZZI_EFFECTS - released iSplashSoundID")
			#ENDIF
		ENDIF
		
		IF mpYachtEffects.iWaterExitSoundID != -1
			RELEASE_SOUND_ID(mpYachtEffects.iWaterExitSoundID)
			mpYachtEffects.iWaterExitSoundID = -1
			#IF IS_DEBUG_BUILD
				CDEBUG2LN(DEBUG_SAFEHOUSE, "MAINTAIN_JACUZZI_EFFECTS - released iWaterExitSoundID")
			#ENDIF
		ENDIF
	ENDIF

	IF IS_BIT_SET(iJacuzziBitSet, JAC_BS_PLAYER_INSIDE_INNER_JAC_SPHERE)
		
		FLOAT fWadeScale = 0.0
		FLOAT fPlayerSubmersion
		FLOAT fPlayerSpeed = GET_ENTITY_SPEED(PLAYER_PED_ID())
		FLOAT fSubmersionFactor // 0.0 - not submerged, 1.0 - fully submerged
		VECTOR vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
		
		IF fPlayerSpeed > 0.0
			VECTOR vPlayerFootCoords = GET_WORLD_POSITION_OF_ENTITY_BONE(PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_L_FOOT))
			
			FLOAT fMaxSubmersion = fJacuzziActualWaterLevel - vJacuzziCentre.Z
			fPlayerSubmersion = FMAX(0.0, ABSF(fJacuzziActualWaterLevel - (vPlayerFootCoords.Z))) // by how much the player is submerged in jacuzzi water
			
			fSubmersionFactor = fPlayerSubmersion / fMaxSubmersion
			
			fWadeScale = FMIN(1.0, (fSubmersionFactor + (fPlayerSpeed * 0.5)) * 0.5)
		ENDIF
		
		/*
		#IF IS_DEBUG_BUILD
			IF fWadeScale > 0.0
				CDEBUG2LN(DEBUG_SAFEHOUSE, "MAINTAIN_JACUZZI_EFFECTS - fPlayerSubmersion: ", fPlayerSubmersion)
				CDEBUG2LN(DEBUG_SAFEHOUSE, "MAINTAIN_JACUZZI_EFFECTS - fWadeScale: ", fWadeScale)
				CDEBUG2LN(DEBUG_SAFEHOUSE, "MAINTAIN_JACUZZI_EFFECTS - fPlayerSpeed: ", fPlayerSpeed)
				CDEBUG2LN(DEBUG_SAFEHOUSE, "MAINTAIN_JACUZZI_EFFECTS - fSubmersionFactor: ", fSubmersionFactor)
			ENDIF
		#ENDIF
		*/
		
		IF fPlayerSpeed > 0.1 AND fWadeScale > 0.0
			VECTOR vWadePosition = <<vPlayerCoords.X, vPlayerCoords.Y, fJacuzziActualWaterLevel>>
			
			RUN_JACUZZI_EFFECT_WADE(vWadePosition)
			
			SET_PARTICLE_FX_LOOPED_OFFSETS(mpYachtEffects.ptfxJacuzziWade, vWadePosition, <<0.0, 0.0, 0.0>>)
			SET_PARTICLE_FX_LOOPED_EVOLUTION(mpYachtEffects.ptfxJacuzziWade, "size", fWadeScale, TRUE)
		ELSE
			STOP_JACUZZI_EFFECT_WADE()
		ENDIF
		
		IF NOT IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_DRIPS_NEEDED)
			SET_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_DRIPS_NEEDED)
		ENDIF
		
		// Detect when to play sounds for splashes of water
		// First check if player is submerged
		INT i
		VECTOR vBonePosition
		BOOL bSubmerged = FALSE
		PED_BONETAG iBoneTags[6]
		iBoneTags[0] = BONETAG_L_FOOT
		iBoneTags[1] = BONETAG_R_FOOT
		iBoneTags[2] = BONETAG_L_FOREARM
		iBoneTags[3] = BONETAG_R_FOREARM
		iBoneTags[4] = BONETAG_ROOT
		iBoneTags[5] = BONETAG_HEAD
		
		// Check some critical bones, if any of them is in the water then at leasst some part of the player is under water for sure
		REPEAT 6 i
			vBonePosition = GET_WORLD_POSITION_OF_ENTITY_BONE(PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), iBoneTags[i]))
			IF vBonePosition.Z <= fJacuzziActualWaterLevel
				bSubmerged = TRUE
				BREAKLOOP
			ENDIF
		ENDREPEAT
		
		IF bSubmerged
			IF NOT IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_PLAYER_SUBMERGED)
				// Player just touched the water surface
				// See if their speed is large enough to trigger splashes
				VECTOR vPlayerVelocity = GET_ENTITY_VELOCITY(PLAYER_PED_ID())
				FLOAT fDownwardSpeed = ABSF(vPlayerVelocity.Z)
				
				#IF IS_DEBUG_BUILD
					CDEBUG2LN(DEBUG_SAFEHOUSE, "MAINTAIN_JACUZZI_EFFECTS - WATER CONTACT! fDownwardSpeed: ", fDownwardSpeed)
				#ENDIF
				
				IF fDownwardSpeed > 5.5
					mpYachtEffects.iJacuzziSplashType = 3
				ELIF fDownwardSpeed > 4.0
					mpYachtEffects.iJacuzziSplashType = 2
				ELIF fDownwardSpeed > 2.0
					mpYachtEffects.iJacuzziSplashType = 1
				ELIF fDownwardSpeed > 0.5
					mpYachtEffects.iJacuzziSplashType = 0
				ELSE
					mpYachtEffects.iJacuzziSplashType = -1
				ENDIF
				
				SET_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_PLAYER_SUBMERGED)
				SET_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_SOUND_SPLASH_NEEDED)
			ELSE
				// Only allow other splash to happen if the previous one already stopped (both particles and sound)
				IF NOT IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_SPLASH_STARTED)
				AND HAS_SOUND_FINISHED(mpYachtEffects.iSplashSoundID)
					
					// Detect any sudden changes in position which mean that we might need to do another splash (player jumping or being pushed etc.)
					FLOAT fZChange = vPlayerCoords.Z - mpYachtEffects.vPlayerPositionLastFrame.Z
					
					IF fZChange < -0.04
						mpYachtEffects.iJacuzziSplashType = 0
						IF fZChange < -0.06
							mpYachtEffects.iJacuzziSplashType = 1
						ENDIF
						
						#IF IS_DEBUG_BUILD
							CDEBUG2LN(DEBUG_SAFEHOUSE, "MAINTAIN_JACUZZI_EFFECTS - Player jump around, need splash")
						#ENDIF
						
						SET_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_SOUND_SPLASH_NEEDED)
					ENDIF
				ENDIF
				
				// Monitor player leaving the water through climbing out so we can play water exit sound
				IF IS_PED_CLIMBING(PLAYER_PED_ID())
					IF NOT IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_WATER_EXIT_SOUND_STARTED)

						STOP_SOUND(mpYachtEffects.iWaterExitSoundID)

						PLAY_SOUND_FROM_ENTITY(mpYachtEffects.iWaterExitSoundID, "ExitWater", PLAYER_PED_ID(), "GTAO_Hot_Tub_PED_INSIDE_WATER", TRUE)
						SET_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_WATER_EXIT_SOUND_STARTED)
						
						#IF IS_DEBUG_BUILD
							CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_JACUZZI_EFFECTS - Playing water exit sound")
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
		
		mpYachtEffects.vPlayerPositionLastFrame = vPlayerCoords
		
		// Do we need to play a splash sound?
		IF IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_SOUND_SPLASH_NEEDED)
			SET_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_SPLASH_NEEDED) // along with the sound go particles
			
			STOP_SOUND(mpYachtEffects.iSplashSoundID)
			
			STRING sndName = ""
			SWITCH mpYachtEffects.iJacuzziSplashType
				CASE 0 sndName = "FallingInWaterSmall" BREAK
				CASE 1 sndName = "FallingInWaterMedium" BREAK
				CASE 2 sndName = "FallingInWaterHeavy" BREAK
				CASE 3 sndName = "DiveInWater" BREAK
			ENDSWITCH
		
			PLAY_SOUND_FROM_ENTITY(mpYachtEffects.iSplashSoundID, sndName, PLAYER_PED_ID(), "PED_INSIDE_WATER", TRUE)
			
			#IF IS_DEBUG_BUILD
				CDEBUG2LN(DEBUG_SAFEHOUSE, "MAINTAIN_JACUZZI_EFFECTS - Playing splash sound: ", sndName)
			#ENDIF
			
			CLEAR_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_SOUND_SPLASH_NEEDED)
		ENDIF
		
		// Do we need to show splash effect?
		IF IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_SPLASH_NEEDED)
		AND NOT IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_SPLASH_STARTED)
		AND mpYachtEffects.iJacuzziSplashType > 0
			IF HAS_NAMED_PTFX_ASSET_LOADED("scr_apartment_mp")
			
				VECTOR vSplashPosition = vPlayerCoords
				vSplashPosition.Z = fJacuzziActualWaterLevel + 0.1
				
				FLOAT fScale = 1.0
				SWITCH mpYachtEffects.iJacuzziSplashType
					CASE 1 fScale = 1.25 BREAK
					CASE 2 fScale = 1.66 BREAK
					CASE 3 fScale = 2.0 BREAK
				ENDSWITCH
			
				USE_PARTICLE_FX_ASSET("scr_apartment_mp")
				mpYachtEffects.ptfxJacuzziSplash = START_PARTICLE_FX_LOOPED_AT_COORD("scr_apa_jacuzzi_wade", vSplashPosition, <<0.0, 0.0, 0.0>>, fScale, FALSE, FALSE, FALSE, TRUE)
				SET_PARTICLE_FX_LOOPED_EVOLUTION(mpYachtEffects.ptfxJacuzziSplash, "size", 1.0, TRUE)
				
				#IF IS_DEBUG_BUILD
					CDEBUG2LN(DEBUG_SAFEHOUSE, "MAINTAIN_JACUZZI_EFFECTS - splash particles started")
				#ENDIF
				
				mpYachtEffects.tdSplashTimer = GET_NETWORK_TIME()
				SET_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_SPLASH_STARTED)
				CLEAR_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_SPLASH_NEEDED)
			ENDIF
		ENDIF
	ELSE
		STOP_JACUZZI_EFFECT_WADE()
		
		IF IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_DRIPS_NEEDED)
		
			IF IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_DRIPS_STARTED)
				mpYachtEffects.tdJacuzziDripsTimer = GET_NETWORK_TIME()
			ENDIF
		
			RUN_JACUZZI_EFFECT_DRIPS()
			
			IF IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_DRIPS_STARTED)
				CLEAR_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_DRIPS_NEEDED)
			ENDIF
			
			CLEAR_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_PLAYER_SUBMERGED)
		ENDIF
		
		CLEAR_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_PLAYER_SUBMERGED)
	ENDIF
	
	// Water drips timer
	IF IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_DRIPS_STARTED)
		IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), mpYachtEffects.tdJacuzziDripsTimer)) > 7000
			STOP_JACUZZI_EFFECT_DRIPS()
			
			#IF IS_DEBUG_BUILD
				CDEBUG2LN(DEBUG_SAFEHOUSE, "MAINTAIN_JACUZZI_EFFECTS - stopped drips")
			#ENDIF
		ENDIF
	ENDIF
	
	// Big jump splash timer
	IF IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_SPLASH_STARTED)
		IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), mpYachtEffects.tdSplashTimer)) > 920
			STOP_PARTICLE_FX_LOOPED(mpYachtEffects.ptfxJacuzziSplash)
			CLEAR_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_SPLASH_STARTED)
			
			#IF IS_DEBUG_BUILD
				CDEBUG2LN(DEBUG_SAFEHOUSE, "MAINTAIN_JACUZZI_EFFECTS - stopped splash particles")
			#ENDIF
		ENDIF
	ENDIF
	
	// Water exiting sound
	IF IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_WATER_EXIT_SOUND_STARTED)
		IF HAS_SOUND_FINISHED(mpYachtEffects.iWaterExitSoundID)
			CLEAR_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_WATER_EXIT_SOUND_STARTED)
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_JACUZZI_EFFECTS()
	IF IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_WADE_STARTED)
		REMOVE_PARTICLE_FX(mpYachtEffects.ptfxJacuzziWade)
		CLEAR_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_WADE_STARTED)
		#IF IS_DEBUG_BUILD
			CDEBUG2LN(DEBUG_SAFEHOUSE, "CLEANUP_JACUZZI_EFFECTS - released PTFX ptfxJacuzziWade")
		#ENDIF
	ENDIF
	IF IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_DRIPS_STARTED)
		REMOVE_PARTICLE_FX(mpYachtEffects.ptfxJacuzziDripsLHand, TRUE)
		REMOVE_PARTICLE_FX(mpYachtEffects.ptfxJacuzziDripsRHand, TRUE)
		CLEAR_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_DRIPS_STARTED)
		#IF IS_DEBUG_BUILD
			CDEBUG2LN(DEBUG_SAFEHOUSE, "CLEANUP_JACUZZI_EFFECTS - released PTFX ptfxJacuzziDripsLHand, ptfxJacuzziDripsRHand")
		#ENDIF
	ENDIF
	IF IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_SPLASH_STARTED)
		REMOVE_PARTICLE_FX(mpYachtEffects.ptfxJacuzziSplash)
		CLEAR_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_SPLASH_STARTED)
		#IF IS_DEBUG_BUILD
			CDEBUG2LN(DEBUG_SAFEHOUSE, "CLEANUP_JACUZZI_EFFECTS - released PTFX ptfxJacuzziSplash")
		#ENDIF
	ENDIF
	
	// Release sound IDs if they haven't been released when player left jacuzzi sphere
	// (this happens when script terminates when player is in jacuzzi)
	IF mpYachtEffects.iSplashSoundID != -1
		RELEASE_SOUND_ID(mpYachtEffects.iSplashSoundID)
		mpYachtEffects.iSplashSoundID = -1
		#IF IS_DEBUG_BUILD
			CDEBUG2LN(DEBUG_SAFEHOUSE, "CLEANUP_JACUZZI_EFFECTS - released iSplashSoundID")
		#ENDIF
	ENDIF
	
	IF mpYachtEffects.iWaterExitSoundID != -1
		RELEASE_SOUND_ID(mpYachtEffects.iWaterExitSoundID)
		mpYachtEffects.iWaterExitSoundID = -1
		#IF IS_DEBUG_BUILD
			CDEBUG2LN(DEBUG_SAFEHOUSE, "CLEANUP_JACUZZI_EFFECTS - released iWaterExitSoundID")
		#ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_YACHT_EFFECTS()
	MAINTAIN_YACHT_EXTERIOR_EFFECTS()
	
	IF DOES_YACHT_HAVE_JACUZZI(iCurrentYacht)
		MAINTAIN_JACUZZI_EFFECTS()
	ENDIF
ENDPROC

PROC CLEANUP_YACHT_EFFECTS()
	CLEANUP_YACHT_EXTERIOR_EFFECTS()
	
	IF DOES_YACHT_HAVE_JACUZZI(iCurrentYacht)
		CLEANUP_JACUZZI_EFFECTS()
	ENDIF
ENDPROC

PROC JACUZZI_SERVER_CLEANUP
	bJacuzziPropReserved = FALSE
	CDEBUG1LN(DEBUG_SAFEHOUSE, "JACUZZI_SERVER_CLEANUP")
ENDPROC

PROC YACHT_LEAN_CLEAN_UP()
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPYACHT_UNLEAN")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPYACHT_LEAN")
		CLEAR_HELP()
	ENDIF
	CLEAR_BIT(iLeanBitSet, LEAN_BS_EXIT_HELP_TEXT_DISPLAYED)
	sYachtLeanAnimDict = ""
	CLEAR_REQUEST_PROPERTY_ACTIVITY(playerBD[NATIVE_TO_INT(PLAYER_ID())].iActivityRequested)
	ENABLE_INTERACTION_MENU()
	CDEBUG1LN(DEBUG_SAFEHOUSE, "----------YACHT_LEAN_CLEAN_UP----------")
	SetLeanStage(yachtLeanActState, LEAN_LOCATECHECK)
ENDPROC

PROC RELEASE_JACUZZI_PROPS(INT iSlot)
	IF iSlot > -1
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.jacuzziServerSeatData[iSlot].niCup)
		
			IF NETWORK_HAS_CONTROL_OF_ENTITY(NET_TO_ENT(serverBD.jacuzziServerSeatData[iSlot].niCup))
				SET_ENTITY_COLLISION(NET_TO_ENT(serverBD.jacuzziServerSeatData[iSlot].niCup), TRUE)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "RELEASE_JACUZZI_PROPS: Cup props set to migrate")
			ELSE
				CWARNINGLN(DEBUG_SAFEHOUSE, "RELEASE_JACUZZI_PROPS: Player doesn't have control of Cup props ")
			ENDIF
		ELSE
			CWARNINGLN(DEBUG_SAFEHOUSE, "RELEASE_CIGAR_PROPS: Cup props don't exist anymore ")
		ENDIF
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.jacuzziServerSeatData[iSlot].niCigar)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(NET_TO_ENT(serverBD.jacuzziServerSeatData[iSlot].niCigar))
				SET_ENTITY_COLLISION(NET_TO_ENT(serverBD.jacuzziServerSeatData[iSlot].niCigar), TRUE)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "RELEASE_JACUZZI_PROPS: Cigar props set to migrate")
			ELSE
				CWARNINGLN(DEBUG_SAFEHOUSE, "RELEASE_JACUZZI_PROPS: Player doesn't have control of Cigar props ")
			ENDIF
		ELSE
			CWARNINGLN(DEBUG_SAFEHOUSE, "RELEASE_CIGAR_PROPS: Cigar props don't exist anymore ")
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "RELEASE_CIGAR_PROPS: Invalid seatID")
	ENDIF

ENDPROC

FUNC BOOL IS_JACUZZI_START_PROMPT_SHOWN()
	RETURN IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPJAC_SIT") OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPJAC_LEAN")
ENDFUNC

FUNC BOOL IS_JACUZZI_EXIT_PROMPT_SHOWN()
	RETURN IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPJAC_EXIT") OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPJAC_UNLEAN")
ENDFUNC

PROC SHOW_JACUZZI_START_PROMPT(BOOL bLean = FALSE)
	IF bLean
		PRINT_HELP_FOREVER("MPJAC_LEAN")
	ELSE
		PRINT_HELP_FOREVER("MPJAC_SIT")
	ENDIF
ENDPROC

PROC SHOW_JACUZZI_EXIT_PROMPT(BOOL bLean = FALSE)
	IF bLean
		PRINT_HELP_FOREVER("MPJAC_UNLEAN")
	ELSE
		PRINT_HELP_FOREVER("MPJAC_EXIT")
	ENDIF
ENDPROC

PROC CLEAR_ALL_JACUZZI_PROMPTS()
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPJAC_SIT")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPJAC_EXIT")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPJAC_LEAN")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPJAC_UNLEAN")
		CLEAR_HELP()
	ENDIF
	CLEAR_BIT(iJacuzziBitSet, JAC_BS_EXIT_HELP_TEXT_DISPLAYED)
ENDPROC

PROC JACUZZI_CLEAN_UP()
	RELEASE_JACUZZI_PROPS(iPlayerChosenSeatSlot)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA) 
	CLEAR_ALL_JACUZZI_PROMPTS()
	CLEAR_BIT(iJacuzziBitSet, JAC_BS_EXIT_HELP_TEXT_DISPLAYED)
	sJaccuzziAnimDict = ""
	SET_PLAYER_HEALTH_RECHARGE_MAX_PERCENT(GET_PLAYER_INDEX(), 0.5)
	CLEAR_BIT(iJacuzziBitSet, JAC_BS_EXIT_HELP_TEXT_DISPLAYED)
	CLEAR_REQUEST_PROPERTY_ACTIVITY(playerBD[NATIVE_TO_INT(PLAYER_ID())].iActivityRequested)
	iBaseClipPlayedCount = 0
	iBaseClipSeqCount = 0
	CDEBUG1LN(DEBUG_SAFEHOUSE, "----------JACUZZI_CLEAN_UP----------")
	SetJacuzziStage(jacActState, JAC_LOCATECHECK)
	iStaggerJacuzziSeatLoop = 0
	iPlayerChosenSeatSlot = 0
	g_BInYachtJacuzzi = FALSE
	IF IS_BIT_SET(iLocalBS2,LOCAL_BS2_SetJacuzziEnterAnimCam)
		CLEAR_BIT(iLocalBS2,LOCAL_BS2_SetJacuzziEnterAnimCam)
	ENDIF	
ENDPROC

PROC BLOCK_ACTIVE_YACHTS()
	INT i
	REPEAT NUMBER_OF_PRIVATE_YACHTS i
		IF IS_PRIVATE_YACHT_ACTIVE(i)
		AND IS_PLAYER_NEAR_YACHT(PLAYER_ID(), i, 1000)
			IF NOT DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iNavMeshBlocks[i])
				VECTOR vCoords = GET_COORDS_OF_PRIVATE_YACHT(i)
				vCoords.z = -100.0
				iNavMeshBlocks[i] = ADD_NAVMESH_BLOCKING_OBJECT(vCoords, <<500.0, 500.0, 200.0>>, DEG_TO_RAD(GET_HEADING_OF_PRIVATE_YACHT(i)))
			ENDIF
		ELSE
			IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iNavMeshBlocks[i])
				REMOVE_NAVMESH_BLOCKING_OBJECT(iNavMeshBlocks[i])
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC UNBLOCK_ACTIVE_YACHTS()
	INT i
	REPEAT NUMBER_OF_PRIVATE_YACHTS i
		IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iNavMeshBlocks[i])
			REMOVE_NAVMESH_BLOCKING_OBJECT(iNavMeshBlocks[i])
		ENDIF
	ENDREPEAT
	
	IF bNavBlockingForMission
		bNavBlockingForMission = FALSE
	ENDIF
ENDPROC

PROC MAINTAIN_NAV_BLOCKING_FOR_MISSION()
	IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_BIKER_DEAL_GONE_WRONG
		IF NOT bNavBlockingForMission
			BLOCK_ACTIVE_YACHTS()
			
			bNavBlockingForMission = TRUE
		ENDIF
	ELSE
		IF bNavBlockingForMission
			UNBLOCK_ACTIVE_YACHTS()
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_SCRIPT()
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_SCRIPT")
	JACUZZI_CLEAN_UP()
	JACUZZI_SERVER_CLEANUP()
	CLEANUP_YACHT_EFFECTS()
	RELEASE_YACHT_EFFECTS()
	
	// Release sound ids that the script could have requested
	IF iBuzzerID != -1
		RELEASE_SOUND_ID(iBuzzerID)
	ENDIF
	
	IF entryCutData.iSoundID != -1
		RELEASE_SOUND_ID(entryCutData.iSoundID)
	ENDIF
	
	UNBLOCK_ACTIVE_YACHTS()
	
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
	AND NOT g_bPropInteriorScriptRunning
		CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_SCRIPT: mark player to leave ped behind: SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), TRUE) (+ network)")
		SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), TRUE)
		NETWORK_DISABLE_LEAVE_REMOTE_PED_BEHIND(FALSE)
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_YACHT_INFO_DISPLAY)
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_YACHT_INFO_DISPLAY)
	ENDIF
	
	IF IS_BIT_SET(iLocalBS2,LOCAL_BS2_SetJacuzziEnterAnimCam)
		CLEAR_BIT(iLocalBS2,LOCAL_BS2_SetJacuzziEnterAnimCam)
	ENDIF
	
	IF DOES_ENTITY_EXIST(yachtDoor)
		DELETE_OBJECT(yachtDoor)
	ENDIF
	CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT cleanup script called for building #", iCurrentYacht)
	IF IS_BIT_SET(iLocalBS, LOCAL_BS_FORCE_TERMINATED_INTERNET)
		MP_FORCE_TERMINATE_INTERNET_CLEAR()
		CLEAR_BIT(iLocalBS, LOCAL_BS_FORCE_TERMINATED_INTERNET)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: Force terminate internet cleared - 2")
	ENDIF
	//INT i
	//REPEAT MAX_CALMING_QUADS_PER_YACHT i
	IF iCalmID != -1
		REMOVE_EXTRA_CALMING_QUAD(iCalmID)
		SET_CALMED_WAVE_HEIGHT_SCALER(1)
		PRINTLN("AM_MP_YACHT: REMOVE_EXTRA_CALMING_QUAD cleanup ID #",iCalmID)
	ENDIF
	//ENDREPEAT
	IF iPackageDropPropID = iCurrentYacht
		//SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(TRUE)
		SET_MAX_NUM_PORTABLE_PICKUPS_CARRIED_BY_PLAYER(DUMMY_MODEL_FOR_SCRIPT,-1)
		MPGlobalsAmbience.g_bPropertyReset = TRUE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[RCC MISSION] - EXT CLEANUP PROPERTY SO ALLOWING PICKUP: ",iCurrentYacht)
	ENDIF
	
	IF DOES_BLIP_EXIST(forSaleBlip)
		REMOVE_BLIP(forSaleBlip)
	ENDIF
	IF bSetEntryData
		CLEAR_FOCUS()
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: CLEAR_FOCUS() - 1")
	ENDIF
	RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
	CLEAR_PLAYER_BROADCAST_PLAYING_PROPERTY_TRANSITION_CUTSCENE()
	IF IS_NET_PLAYER_OK(PLAYER_ID(),FALSE,FALSE)
		
		playerBD[NATIVE_TO_INT(PLAYER_ID())].iBitSet = 0
		//IF iCurrentYacht = mpYachts[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iEnteringPropertyID].iBuildingID
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_WARPING_INTO_GARAGE_CS)
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_WARPING_WITH_OWNER_IN_CAR)
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_WARPING_INTO_GARAGE_FADE)
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_REPLACING_CAR)
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_USING_EXT)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "PROPERTY_BROADCAST_BS_PLAYER_USING_EXT- CLEARED-2")
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iEnteringPropertyID = 0
		//ENDIF
	ENDIF
	
	IF iBuzzerContextIntention != NEW_CONTEXT_INTENTION
		RELEASE_CONTEXT_INTENTION(iBuzzerContextIntention)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: Releasing the context intention on script cleanup")
	ENDIF
	

//	IF mpYachts[iCurrentYacht].iBuildingID = MP_PROPERTY_BUILDING_38
//		REMOVE_MODEL_HIDE(<<2494.2651,1587.5674,31.7398>> , 0.02, PROP_FNCLINK_02GATE3)	//gates
//		REMOVE_MODEL_HIDE(<<2484.0889,1566.9988,31.7453>> , 0.02, PROP_FNCLINK_02GATE3)	//gates
//	ENDIF
	
	IF IS_BIT_SET(iLocalBS2,LOCAL_BS2_bFadeOutWarpIntoGarage)
		IF IS_BIT_SET(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_MADE_INVISIBLE_ON_EXTERIOR)

			IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
				VEHICLE_INDEX vehFadeOut
				vehFadeOut = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF GET_PED_IN_VEHICLE_SEAT(vehFadeOut) = PLAYER_PED_ID()
					
					SET_ENTITY_VISIBLE(vehFadeOut,TRUE)
					SET_ENTITY_VISIBLE(PLAYER_PED_ID(),TRUE)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "NETWORK_FADE_IN_ENTITY called from exterior script- VEH")
					CLEAR_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_MADE_INVISIBLE_ON_EXTERIOR)
				ENDIF
			ELSE
			
				SET_ENTITY_VISIBLE(PLAYER_PED_ID(),TRUE)
				NETWORK_SET_LOCAL_PLAYER_INVINCIBLE_TIME(0)
				CLEAR_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_MADE_INVISIBLE_ON_EXTERIOR)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "NETWORK_FADE_IN_ENTITY called from exterior script - PED")
			ENDIF
			CLEAR_BIT(iLocalBS2,LOCAL_BS2_bFadeOutWarpIntoGarage)
		ENDIF
	ENDIF
	bPersonalVehicleCleanupDontLeaveVehicle = FALSE
	CDEBUG1LN(DEBUG_SAFEHOUSE, "bPersonalVehicleCleanupDontLeaveVehicle = FALSE - B")
	
	IF iYachtMoveControl != 0
		iYachtMoveControl = 0
		DISABLE_RADAR_MAP(FALSE)
		g_PlayerBlipsData.bBigMapIsActive = FALSE
		CLEANUP_MENU_ASSETS()
		SET_RADAR_ZOOM(0)
		UNLOCK_MINIMAP_ANGLE()
		UNLOCK_MINIMAP_POSITION()
		SET_BIGMAP_ACTIVE(FALSE)
		THEFEED_SHOW()
		REMOVE_MULTIPLAYER_BANK_CASH()
		REMOVE_MULTIPLAYER_WALLET_CASH()
		
		// Cleanup transaction
		IF iYachtMoveTransactionState != YACHT_MOVE_TRANSACTION_INVALID
			IF IS_CASH_TRANSACTION_COMPLETE(iYachtMoveTransactionID)
				DELETE_CASH_TRANSACTION(iYachtMoveTransactionID)
			ELSE
				UPDATE_CASH_TRANSACTION_FLAGS(iYachtMoveTransactionID, CTPF_AUTO_PROCESS_REPLY)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(iYachtMoveBitset, YACHT_MOVE_BITSET_FLAG_BLOCK_MISSIONS)
			Block_All_MissionsAtCoords_Missions(FALSE)
			CLEAR_BIT(iYachtMoveBitset, YACHT_MOVE_BITSET_FLAG_BLOCK_MISSIONS)
		ENDIF
		IF IS_BIT_SET(iYachtMoveBitset, YACHT_MOVE_BITSET_FLAG_HIDE_BLIPS)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_PIM_HIDE_MISC, iBlipHideStatValue)
			SET_HIDE_ALL_JOB_BLIPS_FOR_YACHT_STATUS(FALSE)
			CLEAR_BIT(iYachtMoveBitset, YACHT_MOVE_BITSET_FLAG_HIDE_BLIPS)
		ENDIF
		
		UPDATE_ALL_SHOP_BLIPS()
		
		g_bForceUpdateActivityBlips = TRUE
	ENDIF
	IF DOES_BLIP_EXIST(blipYachtMenu)
		REMOVE_BLIP(blipYachtMenu)
	ENDIF
	IF iYachtMoveContext != NEW_CONTEXT_INTENTION
		RELEASE_CONTEXT_INTENTION(iYachtMoveContext)
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedID_BridgeClone)
		DELETE_PED(pedID_BridgeClone)
	ENDIF
	
	IF DOES_CAM_EXIST(camID_BridgeEntry)
		DESTROY_CAM(camID_BridgeEntry)
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedID_Captain) AND NETWORK_HAS_CONTROL_OF_ENTITY(pedID_Captain)
		DELETE_PED(pedID_Captain)
	ENDIF
	
	IF DOES_ENTITY_EXIST(objID_BridgeDoor[0])
		DELETE_OBJECT(objID_BridgeDoor[0])
	ENDIF
	IF DOES_ENTITY_EXIST(objID_BridgeDoor[1])
		DELETE_OBJECT(objID_BridgeDoor[1])
	ENDIF
	
	IF IS_NAMED_RENDERTARGET_REGISTERED("port_text")   RELEASE_NAMED_RENDERTARGET("port_text")   ENDIF
	IF IS_NAMED_RENDERTARGET_REGISTERED("starb_text")  RELEASE_NAMED_RENDERTARGET("starb_text")  ENDIF
	IF IS_NAMED_RENDERTARGET_REGISTERED("stern_text")  RELEASE_NAMED_RENDERTARGET("stern_text")  ENDIF
	IF DOES_ENTITY_EXIST(oYachtNamePort)   SAFE_DELETE_OBJECT(oYachtNamePort)   ENDIF
	IF DOES_ENTITY_EXIST(oYachtNameStarb)  SAFE_DELETE_OBJECT(oYachtNameStarb)  ENDIF
	IF DOES_ENTITY_EXIST(oYachtNameStern)  SAFE_DELETE_OBJECT(oYachtNameStern)  ENDIF
	
	PLAYER_RESTORE_CLOTHES(SwimwearData, FALSE)
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("YACHT_MOVE_BLK")
		CLEAR_HELP(TRUE)
	ENDIF
	
	IF bYachtOwnerNameRequested
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfID_YachtOwnerName)
	ENDIF
	
	IF g_iYachtBridgeEntryStage != YACHT_BRIDGE_FLAG_TRIGGER_LOCATE
		SET_PED_CAN_RAGDOLL(PLAYER_PED_ID(), TRUE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableExplosionReactions,FALSE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontActivateRagdollFromExplosions,FALSE)
		g_iYachtBridgeEntryStage = YACHT_BRIDGE_FLAG_TRIGGER_LOCATE
	ENDIF
	
	IF HAS_ANIM_DICT_LOADED("ANIM@AMB@YACHT@CAPTAIN@")
		REMOVE_ANIM_DICT("ANIM@AMB@YACHT@CAPTAIN@")
	ENDIF
	
	CLEAR_BIT(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_LAUNCHED_YACHT_SCRIPT)
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
	
ENDPROC

//FUNC BOOL DOES_SERVER_DOOR_EXIST(INT iDoor)
//	IF iDoor = 0
//		RETURN TRUE
//	ENDIF
//	IF ARE_VECTORS_EQUAL(propertyDoors[iDoor].vCoords, <<0,0,0>>)
//		RETURN TRUE
//	ENDIF
//	//IF bVisible
//		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.normalDoorIDs[iDoor])
//			RETURN TRUE
//		ENDIF
//	//ELSE
////		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.invisibleDoorIDs[iDoor])
////			RETURN TRUE
////		ENDIF
//	//ENDIF
//	RETURN FALSE
//ENDFUNC

FUNC BOOL DOES_LOCAL_GARAGE_BLOCKING_OBJECT_EXIST()
	IF ARE_VECTORS_EQUAL(blockObjectDetails.vGarageLoc, <<0,0,0>>)
		RETURN TRUE
	ENDIF

	IF DOES_ENTITY_EXIST(garageBlockingObj)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_LOCAL_ENTRY_BLOCKING_OBJECT_EXIST()
	IF ARE_VECTORS_EQUAL(blockObjectDetails.vEntryLoc, <<0,0,0>>)
		RETURN TRUE
	ENDIF

	IF DOES_ENTITY_EXIST(entryBlockingObj)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

//PROC GET_MAP_HOLE_PROTECTOR_DATA(MP_PROP_OFFSET_STRUCT &positionData,INT iIndex)
//	IF mpYachts[iCurrentYacht].iBuildingID = MP_PROPERTY_BUILDING_9	
//		SWITCH iIndex
//			CASE 0 //interior left wall
//				positionData.vLoc = <<7.941,40.869,70.586>>	
//				positionData.vRot = <<0,0,71.30>>
//			BREAK
//			
//			CASE 1 //interior right wall
//				positionData.vLoc = <<2.611,42.849,70.586>>	
//				positionData.vRot = <<0,0,71.30>>
//			BREAK
//			
//			CASE 2 //interior back wall
//				positionData.vLoc = <<4.881,42.099,70.586>>	
//				positionData.vRot = <<0,0,160>>
//			BREAK
//		ENDSWITCH
//	ENDIF
//ENDPROC
//
//FUNC BOOL CREATE_MAP_HOLE_PROTECTOR(INT iIndex)
//	IF mpYachts[iCurrentYacht].iBuildingID = MP_PROPERTY_BUILDING_9	
//		IF NOT DOES_ENTITY_EXIST(mapHoleProtectors[iIndex])
//			IF REQUEST_LOAD_MODEL(prop_ss1_mpint_garage)
//				tempPropOffset.vLoc = <<0,0,0>>
//				tempPropOffset.vRot = <<0,0,0>>
//				GET_MAP_HOLE_PROTECTOR_DATA(tempPropOffset,iIndex)
//				mapHoleProtectors[iIndex] = CREATE_OBJECT(prop_ss1_mpint_garage,tempPropOffset.vLoc,FALSE,FALSE,TRUE)
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_MAP_HOLE_PROTECTOR: created at ",tempPropOffset.vLoc)
//				SET_ENTITY_ROTATION(mapHoleProtectors[iIndex],tempPropOffset.vRot)
//				FREEZE_ENTITY_POSITION(mapHoleProtectors[iIndex],TRUE)
//				SET_ENTITY_PROOFS(mapHoleProtectors[iIndex],TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
//				SET_ENTITY_VISIBLE(mapHoleProtectors[iIndex],FALSE)
//				SET_ENTITY_INVINCIBLE(mapHoleProtectors[iIndex],TRUE)
//				SET_ENTITY_DYNAMIC(mapHoleProtectors[iIndex],FALSE)
//			ENDIF
//			RETURN FALSE
//		ENDIF
//	ENDIF
//	
//	RETURN TRUE
//ENDFUNC

FUNC BOOL CREATE_BUZZER_PROPS_IF_NEEDED()
	INT i

	IF NOT ARE_VECTORS_EQUAL(mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[0].vBuzzerProp,<<0,0,0>>)
		IF NOT DOES_ENTITY_EXIST(createdBuzzers[0])
			IF REQUEST_LOAD_MODEL(PROP_LD_KEYPAD_01B)
				createdBuzzers[i] = CREATE_OBJECT_NO_OFFSET(PROP_LD_KEYPAD_01B,mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[0].vBuzzerProp,FALSE,FALSE,TRUE)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_BUZZER_PROPS_IF_NEEDED: created #",i," at ",mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[0].vBuzzerProp)
				SET_ENTITY_ROTATION(createdBuzzers[0],mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[0].vBuzzerPropRot)
				FREEZE_ENTITY_POSITION(createdBuzzers[0],TRUE)
				SET_ENTITY_PROOFS(createdBuzzers[0],TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
				SET_ENTITY_INVINCIBLE(createdBuzzers[0],TRUE)
				SET_ENTITY_DYNAMIC(createdBuzzers[0],FALSE)
			ENDIF
			RETURN FALSE
		ENDIF
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC BOOL CREATE_YACHT_DOOR()
	MODEL_NAMES doorModel
	IF NOT ARE_VECTORS_EQUAL(mpYachts[iCurrentYacht].doorLocDetails.vLoc,<<0,0,0>>)
		IF NOT DOES_ENTITY_EXIST(yachtDoor)
			IF GET_RAILING_OF_PRIVATE_YACHT(iCurrentYacht) = 0
				doorModel = INT_TO_ENUM(MODEL_NAMES,HASH("apa_mp_apa_yacht_door"))
			ELSE
				doorModel = INT_TO_ENUM(MODEL_NAMES,HASH("apa_mp_apa_yacht_door2"))
			ENDIF
			IF REQUEST_LOAD_MODEL(doorModel)
				yachtDoor = CREATE_OBJECT_NO_OFFSET(doorModel,mpYachts[iCurrentYacht].doorLocDetails.vLoc,FALSE,FALSE,TRUE)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_YACHT_DOOR: created at ",mpYachts[iCurrentYacht].doorLocDetails.vRot)
				SET_ENTITY_ROTATION(yachtDoor,mpYachts[iCurrentYacht].doorLocDetails.vRot)
				FREEZE_ENTITY_POSITION(yachtDoor,TRUE)
				SET_ENTITY_PROOFS(yachtDoor,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
				SET_ENTITY_INVINCIBLE(yachtDoor,TRUE)
				SET_ENTITY_DYNAMIC(yachtDoor,FALSE)
				IF NOT IS_BIT_SET(iLocalBs,LOCAL_BS_SET_YACHT_DOOR_INVISIBLE)
					IF IS_PLAYER_EXITING_PROPERTY(PLAYER_ID())
						SET_ENTITY_VISIBLE(yachtDoor,FALSE)
						SET_BIT(iLocalBs,LOCAL_BS_SET_YACHT_DOOR_INVISIBLE)
						PRINTLN("CREATE_YACHT_DOOR: set door invisible exiting property")
					ENDIF
				ENDIF
				SET_OBJECT_TINT_INDEX(yachtDoor,GET_TINT_OF_PRIVATE_YACHT(iCurrentYacht))
				PRINTLN("CREATE_YACHT_DOOR: door created")
			ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_YACHT_DOOR_VISIBILITY()
	IF DOES_ENTITY_EXIST(yachtDoor)
		IF IS_PLAYER_EXITING_PROPERTY(PLAYER_ID())
			IF NOT IS_BIT_SET(iLocalBs,LOCAL_BS_SET_YACHT_DOOR_INVISIBLE)
				SET_ENTITY_VISIBLE(yachtDoor,FALSE)
				SET_BIT(iLocalBs,LOCAL_BS_SET_YACHT_DOOR_INVISIBLE)
				PRINTLN("MAINTAIN_YACHT_DOOR_VISIBILITY: door set not visible")
			ENDIF
		ELSE
			IF IS_BIT_SET(iLocalBs,LOCAL_BS_SET_YACHT_DOOR_INVISIBLE)
			AND entryCutData.iStage <= 0
				SET_ENTITY_COORDS_NO_OFFSET(yachtDoor,mpYachts[iCurrentYacht].doorLocDetails.vLoc)
				SET_ENTITY_ROTATION(yachtDoor,mpYachts[iCurrentYacht].doorLocDetails.vRot)
				FREEZE_ENTITY_POSITION(yachtDoor,TRUE)
				SET_ENTITY_VISIBLE(yachtDoor,TRUE)
				CLEAR_BIT(iLocalBs,LOCAL_BS_SET_YACHT_DOOR_INVISIBLE)
				PRINTLN("MAINTAIN_YACHT_DOOR_VISIBILITY: door set visible")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//FUNC BOOL CREATE_ENTRY_AREAS_MAP_HOLE_PROTECTORS()
//	IF mpYachts[iCurrentYacht].iBuildingID = MP_PROPERTY_BUILDING_9	
//		INT i
//		REPEAT MAX_MAP_HOLE_PROTECTORS i
//			IF NOT CREATE_MAP_HOLE_PROTECTOR(i)
//				RETURN FALSE
//			ENDIF
//		ENDREPEAT
//	ENDIF
//	CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_ENTRY_AREAS_MAP_HOLE_PROTECTORS() - finished")
//	RETURN TRUE
//ENDFUNC


FUNC BOOL CREATE_LOCAL_BLOCKING_OBJECTS()
	IF GET_PROPERTY_BUILDING(iCurrentYacht) = MP_PROPERTY_BUILDING_6
		IF NOT DOES_ENTITY_EXIST(entryBlockingObj)
			IF REQUEST_LOAD_MODEL(prop_dummy_car)
				IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
					CLEAR_AREA_OF_OBJECTS(<<-935.1886,-378.4521,38.8713>>,3)
					entryBlockingObj = CREATE_OBJECT_NO_OFFSET(prop_dummy_car, <<-935.1886,-378.4521,38.8713>>,FALSE,FALSE,TRUE)
					SET_ENTITY_ROTATION(entryBlockingObj, <<0,0,26>>)
					//NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(garageBlockingObj,TRUE)
					FREEZE_ENTITY_POSITION(entryBlockingObj,TRUE)
					SET_ENTITY_PROOFS(entryBlockingObj,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
					SET_ENTITY_VISIBLE(entryBlockingObj,FALSE)
					SET_ENTITY_INVINCIBLE(entryBlockingObj,TRUE)
					SET_ENTITY_DYNAMIC(entryBlockingObj,FALSE)
					SET_CAN_CLIMB_ON_ENTITY(entryBlockingObj,FALSE)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: CREATE_LOCAL_BLOCKING_OBJECTS: property #",iCurrentYacht,"created Revolving door block 1 ")
				ENDIF
			ENDIF
			RETURN FALSE
		ENDIF
		IF NOT DOES_ENTITY_EXIST(garageBlockingObj)
			IF REQUEST_LOAD_MODEL(prop_dummy_car)
				IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
					CLEAR_AREA_OF_OBJECTS(<<-935.1886,-378.4521,39.8713>>,3)
					garageBlockingObj = CREATE_OBJECT_NO_OFFSET(prop_dummy_car, <<-935.1886,-378.4521,39.8713>>,FALSE,FALSE,TRUE)
					SET_ENTITY_ROTATION(garageBlockingObj, <<0,0,26>>)
					//NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(garageBlockingObj,TRUE)
					FREEZE_ENTITY_POSITION(garageBlockingObj,TRUE)
					SET_ENTITY_PROOFS(garageBlockingObj,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
					SET_ENTITY_VISIBLE(garageBlockingObj,FALSE)
					SET_ENTITY_INVINCIBLE(garageBlockingObj,TRUE)
					SET_ENTITY_DYNAMIC(garageBlockingObj,FALSE)
					SET_CAN_CLIMB_ON_ENTITY(garageBlockingObj,FALSE)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: CREATE_LOCAL_BLOCKING_OBJECTS: property #",iCurrentYacht,"created Revolving door block 2 ")
					SET_MODEL_AS_NO_LONGER_NEEDED(prop_dummy_car)
				ENDIF
			ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

//╔═════════════════════════════════════════════════════════════════════════════╗
//║								WALK INTO PROPERTY																		
//╚═════════════════════════════════════════════════════════════════════════════╝


PROC WALK_IN_CLEANUP(BOOL bSetPlayerControlOn)
	IF bSetPlayerControlOn
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		CLEAR_BIT(iLocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL)
		CLEAR_BIT(entryCutData.iBS,ENTRY_CS_BS_TOOK_AWAY_CONTROL)
	ENDIF
	
	globalPropertyEntryData.bWalkInSeqDone = TRUE
	iWalkInStage = WIS_INIT
	CLEANUP_PROPERTY_CAMERAS()
	//SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_OpenDoorArmIK, FALSE) 
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     WALK_IN_CLEANUP - DONE")
ENDPROC

//PURPOSE: Controls the player walking into a property through a door
FUNC BOOL WALK_INTO_THIS_PROPERTY()//BOOL bResetCoords, STRUCT_net_realty_1_scene &scene)
	
	//Check to see if we should cleanup
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
	OR IS_PLAYER_SWITCH_IN_PROGRESS()
		iWalkInStage = WIS_CLEANUP
		CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     WALK_INTO_THIS_PROPERTY - iWalkInStage = WIS_CLEANUP - A")
	ENDIF
	
	IF iWalkInStage <=WIS_WAIT_FOR_DOOR_CONTROL
		RETAIN_BUZZER_MARKER()
	ENDIF
		
	//Process Stages
	SWITCH iWalkInStage
		CASE WIS_INIT
			globalPropertyEntryData.bWalkInSeqDone = FALSE
			IF NOT IS_BIT_SET(iLocalBS2,LOCAL_BS2_bWalkInTriggered)
				IF RUN_ENTRY_CUTSCENE(ENTRY_CUTSCENE_TYPE_FRONT_DOOR) //moved to pre menu
					iWalkInStage = WIS_WAIT_FOR_LOAD
					SET_BIT(iLocalBS2,LOCAL_BS2_bWalkInTriggered)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     WALK_INTO_THIS_PROPERTY - iWalkInStage = WIS_WAIT_FOR_LOAD - YTV")
				ENDIF
			ELSE
				iWalkInStage = WIS_WAIT_FOR_LOAD
				CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     WALK_INTO_THIS_PROPERTY - iWalkInStage = WIS_WAIT_FOR_LOAD - bypassing as already run walk in sequence.")
			ENDIF
		BREAK
		
//		CASE WIS_WAIT_FOR_DOOR_CONTROL
//			iWalkInStage = WIS_GIVE_SEQUENCE
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     WALK_INTO_THIS_PROPERTY - iWalkInStage = WIS_GIVE_SEQUENCE - A")
//			
//			HIDE_HUD_AND_RADAR_THIS_FRAME()
//		BREAK
//		
//		CASE WIS_GIVE_SEQUENCE
//			
//			IF bResetCoords
//				SET_ENTITY_COORDS(PLAYER_PED_ID(), scene.mPlacers[net_realty_1_scene_PLACER_playerPos].vPos)
//				SET_ENTITY_HEADING(PLAYER_PED_ID(), scene.mPlacers[net_realty_1_scene_PLACER_playerPos].fRot)
//			ENDIF
//			
//
//			TASK_FLUSH_ROUTE()
//			IF iCurrentYacht = PROPERTY_IND_DAY_MEDIUM_3
//			AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),<<-105.851982,6528.291992,28.911039>>,<<-103.993401,6530.095703,31.722012>>,2.250000)
//				TASK_EXTEND_ROUTE( <<-106.0008, 6530.2056, 28.8582>>)
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "CDM: walking to  <<-106.0008, 6530.2056, 28.8582>>")
//			ELSE
//				TASK_EXTEND_ROUTE(scene.mMarkers[net_realty_1_scene_MARKER_playerWalk0].vPos)
//			ENDIF
//			
//			TASK_EXTEND_ROUTE(scene.mMarkers[net_realty_1_scene_MARKER_playerWalk1].vPos)
//			TASK_EXTEND_ROUTE(scene.mMarkers[net_realty_1_scene_MARKER_playerWalk2].vPos)
//			TASK_FOLLOW_POINT_ROUTE(PLAYER_PED_ID(), PEDMOVE_WALK, TICKET_SINGLE )
//
//
//			SceneTool_ExecutePan(scene.mPans[net_realty_1_scene_PAN_establishing], propertyCam0, propertyCam1)
//			
//			REINIT_NET_TIMER(iCameraTimer,TRUE)
//			
//			//SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_OpenDoorArmIK, TRUE)
//			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_SearchForClosestDoor, TRUE)
//			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_OpenDoorArmIK, TRUE) //CODE CHANGES REQUIRED RANDOM NUMBER #1123124893
//			
//			REINIT_NET_TIMER(iWalkInTimer,TRUE)
//			HIDE_HUD_AND_RADAR_THIS_FRAME()
//			
//			iWalkInStage = WIS_WAIT_FOR_SEQUENCE_END
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     WALK_INTO_THIS_PROPERTY - iWalkInStage = WIS_WAIT_FOR_SEQUENCE_END - A")
//		BREAK
//		
//		CASE WIS_WAIT_FOR_SEQUENCE_END
//			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_SearchForClosestDoor, TRUE)
//			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_OpenDoorArmIK, TRUE) //CODE CHANGES REQUIRED RANDOM NUMBER #1123124893
//			HIDE_HUD_AND_RADAR_THIS_FRAME()
//			
//			IF HAS_NET_TIMER_EXPIRED(iCameraTimer, ROUND((scene.mPans[net_realty_1_scene_PAN_establishing].fDuration + scene.fExitDelay) * 1000.0),TRUE)
//				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE) != WAITING_TO_START_TASK
//				AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
//					//SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_OpenDoorArmIK, FALSE) 
//					iWalkInStage = WIS_WAIT_FOR_LOAD
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     WALK_INTO_THIS_PROPERTY - iWalkInStage = WIS_WAIT_FOR_LOAD - A")
//				ELIF HAS_NET_TIMER_EXPIRED(iWalkInTimer, WALK_IN_TIMEOUT,TRUE)
//					//SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_OpenDoorArmIK, FALSE) 
//					iWalkInStage = WIS_WAIT_FOR_LOAD
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     WALK_INTO_THIS_PROPERTY - iWalkInStage = WIS_WAIT_FOR_LOAD - B")
//				ELSE
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     WALK_INTO_THIS_PROPERTY - PROCESSING...")
//				ENDIF
//			ENDIF
//		BREAK
		
		CASE WIS_WAIT_FOR_LOAD
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			IF globalPropertyEntryData.bWalkInSeqDone = FALSE	// STAY ON HERE FOR NOW - CONOR WILL DO CLEANUP ONCE HE IS READY
				globalPropertyEntryData.bWalkInSeqDone = TRUE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     WALK_INTO_THIS_PROPERTY - globalPropertyEntryData.bWalkInSeqDone = TRUE")
			ENDIF
			
		BREAK
		
		CASE WIS_CLEANUP
			WALK_IN_CLEANUP(TRUE)
			RETURN TRUE
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Controls the player walking into a property through a door
FUNC BOOL WALK_INTO_PROPERTY_CUTSCENE()//BOOL bResetCoords)

//		STRUCT_net_realty_1_scene scene
//		IF NOT Private_Get_net_realty_1_scene(GET_PROPERTY_BUILDING(iCurrentYacht), scene)
//			iWalkInStage = WIS_CLEANUP
//			globalPropertyEntryData.bWalkInSeqDone = TRUE
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     WALK_INTO_PROPERTY - iWalkInStage = WIS_CLEANUP - C")
//			RETURN TRUE
//		ENDIF
		

	
		IF WALK_INTO_THIS_PROPERTY()//bResetCoords), scene)
			RETURN TRUE
		ENDIF

	RETURN FALSE
ENDFUNC

//
//PROC MAINTAIN_WARP_WITH_OWNER()
//	VEHICLE_INDEX theVeh
//	PED_INDEX vehiclePed
//	PLAYER_INDEX vehiclePlayer
//	INT i, iSeat
//	BOOL bPlayerInVehicleDoingDropOff
//	CLEAR_BIT(iLocalBS2,LOCAL_BS2_PlayerInVehicleDoingHeistDelivery)
//	IF IS_NET_PLAYER_OK(PLAYER_ID())
//		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//		AND NOT IS_PLAYER_GETTING_IN_OR_OUT_OF_VEHICLE(PLAYER_ID())
//			theVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
//			IF IS_VEHICLE_DRIVEABLE(theVeh)
//				#IF FEATURE_HEIST_PLANNING
//				IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_DOING_HEIST_DROPOFF)
//				AND (Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
//				OR Is_Player_Currently_On_MP_Heist(PLAYER_ID()))
//					FOR iSeat = ENUM_TO_INT(VS_DRIVER) TO ENUM_TO_INT(VS_EXTRA_RIGHT_3)
//						IF NOT IS_VEHICLE_SEAT_FREE(theVeh, INT_TO_ENUM(VEHICLE_SEAT, iSeat))
//							vehiclePed = GET_PED_IN_VEHICLE_SEAT(theVeh,INT_TO_ENUM(VEHICLE_SEAT, iSeat))
//							IF NOT IS_PED_INJURED(vehiclePed)
//								IF IS_PED_A_PLAYER(vehiclePed)
//									vehiclePlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(vehiclePed)
//									IF IS_NET_PLAYER_OK(vehiclePlayer)
//										IF IS_BIT_SET(playerBD[NATIVE_TO_INT(vehiclePlayer)].iBS,PLAYER_BD_BS_DOING_HEIST_DROPOFF)
//											SET_BIT(iLocalBS2,LOCAL_BS2_PlayerInVehicleDoingHeistDelivery)
//											IF IS_BIT_SET(playerBD[NATIVE_TO_INT(vehiclePlayer)].iBS,PLAYER_BD_BS_CAR_IN_GARAGE_LOCATE)
//												bPlayerInVehicleDoingDropOff = TRUE
//											ENDIF
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDFOR
//					IF bPlayerInVehicleDoingDropOff = TRUE
//						IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
//						AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
//							TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
//							CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_WARP_WITH_OWNER- Getting player to leave vehicle due to someone in vehicle doing a heist drop off but not local player")
//							EXIT
//						ENDIF
//					ENDIF
//				ENDIF
//				#ENDIF
//				vehiclePed = GET_PED_IN_VEHICLE_SEAT(theVeh)
//				IF NOT IS_PED_INJURED(vehiclePed)
//					IF IS_PED_A_PLAYER(vehiclePed)
//						vehiclePlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(vehiclePed)
//						IF vehiclePlayer != PLAYER_ID()
//						AND IS_NET_PLAYER_OK(vehiclePlayer)
//							#IF FEATURE_HEIST_PLANNING
//							IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(vehiclePlayer)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_WARPING_INTO_GARAGE_CS)
//							AND SHOULD_BYPASS_CHECKS_FOR_ENTRY()
//								warpInWithOwnerID = vehiclePlayer
//								SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_WARPING_WITH_OWNER_IN_CAR)
//								REPEAT mpYachts[iCurrentYacht].yachtPropertyDetails.iNumEntrances i
//									IF mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[i].iType = ENTRANCE_TYPE_GARAGE
//										globalPropertyEntryData.iEntrance = i
//										i = mpYachts[iCurrentYacht].yachtPropertyDetails.iNumEntrances
//										iEntrancePlayerIsUsing = globalPropertyEntryData.iEntrance
//									ENDIF
//								ENDREPEAT
//								CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_WARP_WITH_OWNER: entering heist property with driver")
//								NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//								SET_BIT(iLocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL)
//								SET_LOCAL_STAGE(STAGE_SETUP_FOR_USING_PROPERTY)
//							ELSE
//							#ENDIF
//								IF (IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(vehiclePlayer)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_WARPING_INTO_GARAGE_CS)
//								OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(vehiclePlayer)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_WARPING_INTO_GARAGE_FADE))
//								AND GlobalplayerBD_FM[NATIVE_TO_INT(vehiclePlayer)].propertyDetails.iEnteringPropertyID > 0
//								AND GET_PROPERTY_BUILDING(GlobalplayerBD_FM[NATIVE_TO_INT(vehiclePlayer)].propertyDetails.iEnteringPropertyID) = GET_PROPERTY_BUILDING(iCurrentYacht)
//									IF IS_PLAYER_BLOCKED_FROM_PROPERTY()
//										IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
//										AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
//											TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
//											CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_WARP_WITH_OWNER- Getting player to leave vehicle as they are blocked while driver wants to enter")
//											EXIT
//										ENDIF
//									ELSE
//										globalPropertyEntryData.ownerID = vehiclePlayer
//										globalPropertyEntryData.ownerHandle = GET_GAMER_HANDLE_PLAYER(vehiclePlayer)
//										iCurrentYacht = GlobalplayerBD_FM[NATIVE_TO_INT(vehiclePlayer)].propertyDetails.iEnteringPropertyID
//										GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iEnteringPropertyID = iCurrentYacht
//										CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_WARP_WITH_OWNER:setting owner as ",NATIVE_TO_INT(vehiclePlayer))
//										SET_BIT(iLocalBS2,LOCAL_BS2_bEnteringAnotherPlayerProperty)
//										REPEAT mpYachts[iCurrentYacht].yachtPropertyDetails.iNumEntrances i
//											IF mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[i].iType = ENTRANCE_TYPE_GARAGE
//												globalPropertyEntryData.iEntrance = i
//												i = mpYachts[iCurrentYacht].yachtPropertyDetails.iNumEntrances
//												iEntrancePlayerIsUsing = globalPropertyEntryData.iEntrance
//											ENDIF
//										ENDREPEAT
//										CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_WARP_WITH_OWNER:setting entrance = ",iEntrancePlayerIsUsing )
//										globalPropertyEntryData.iPropertyEntered = GlobalplayerBD_FM[NATIVE_TO_INT(vehiclePlayer)].propertyDetails.iEnteringPropertyID
//										DISPLAY_RADAR(FALSE)
//										bDisabledRadar = TRUE
//										bSetEntryData = TRUE
//										warpInWithOwnerID = vehiclePlayer
//										SET_FOCUS_POS_AND_VEL(GET_PLAYER_COORDS(PLAYER_ID()),<<0,0,0>>)
//										SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_ENTERING_A_PROPERTY)
//										CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: setting PROPERTY_BROADCAST_BS_PLAYER_ENTERING_A_PROPERTY- A")
//										SET_PLAYER_BROADCAST_PLAYING_PROPERTY_TRANSITION_CUTSCENE(globalPropertyEntryData.iPropertyEntered)
//										SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_WARPING_WITH_OWNER_IN_CAR)
//										NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//										SET_BIT(iLocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL)
//										DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
//										SET_LOCAL_STAGE(STAGE_SETUP_FOR_USING_PROPERTY)
//									ENDIF
//								ENDIF
//							#IF FEATURE_HEIST_PLANNING
//							ENDIF
//							#ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC

FUNC PRIVATE_YACHT_SPAWN_ZONE_ENUM GET_YACHT_LOCATION_FOR_MENU_POSITION(INT iMenuPosition)
	SWITCH iMenuPosition
		CASE 0	RETURN PYSZ_1 		BREAK
		CASE 1	RETURN PYSZ_2		BREAK
		CASE 2	RETURN PYSZ_3		BREAK
		CASE 3	RETURN PYSZ_4		BREAK
		CASE 4	RETURN PYSZ_5 		BREAK
		CASE 5	RETURN PYSZ_6 		BREAK
		CASE 6	RETURN PYSZ_7 		BREAK
		CASE 7	RETURN PYSZ_8 		BREAK
		CASE 8	RETURN PYSZ_9 		BREAK
		CASE 9	RETURN PYSZ_10 		BREAK
		CASE 10	RETURN PYSZ_11 		BREAK
		CASE 11	RETURN PYSZ_12 		BREAK
	ENDSWITCH
	RETURN PYSZ_NULL
ENDFUNC

FUNC STRING GET_YACHT_LOCATION_NAME_FOR_MENU(PRIVATE_YACHT_SPAWN_ZONE_ENUM eZone)
	SWITCH eZone		
		CASE PYSZ_1 	RETURN "YACHT_LOC_0"  		BREAK //[YACHT_LOC_0]North Chumash
		CASE PYSZ_2		RETURN "YACHT_LOC_1" 		BREAK //[YACHT_LOC_1]Lago Zancudo
		CASE PYSZ_3		RETURN "YACHT_LOC_2" 		BREAK //[YACHT_LOC_2]Pacific Bluffs
		CASE PYSZ_4		RETURN "YACHT_LOC_3" 		BREAK //[YACHT_LOC_3]Vespucci Beach
		CASE PYSZ_5 	RETURN "YACHT_LOC_4"  		BREAK //[YACHT_LOC_4]Los Santos International Airport
		CASE PYSZ_6 	RETURN "YACHT_LOC_5"  		BREAK //[YACHT_LOC_5]Terminal 
		CASE PYSZ_7 	RETURN "YACHT_LOC_6"  		BREAK //[YACHT_LOC_6]Palomino Highlands
		CASE PYSZ_8 	RETURN "YACHT_LOC_7"  		BREAK //[YACHT_LOC_7]Tataviam Mountains
		CASE PYSZ_9 	RETURN "YACHT_LOC_8"  		BREAK //[YACHT_LOC_8]San Chianski Mountain Range
		CASE PYSZ_10	RETURN "YACHT_LOC_9"  		BREAK //[YACHT_LOC_9]Mount Gordo
		CASE PYSZ_11	RETURN "YACHT_LOC_10"  		BREAK //[YACHT_LOC_10]Procopio Beach
		CASE PYSZ_12	RETURN "YACHT_LOC_11"  		BREAK //[YACHT_LOC_11]Paleto Bay				
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC INT GET_YACHT_MOVE_COST(PRIVATE_YACHT_SPAWN_ZONE_ENUM eCurrentZone, PRIVATE_YACHT_SPAWN_ZONE_ENUM eZone)

	// Use this if we need to vary cost on distance.
	UNUSED_PARAMETER(eCurrentZone)
	
	// if the desired zone is same as bought one, no charge.
	INT iDesiredYachtID = GET_MP_INT_CHARACTER_STAT(MP_STAT_YACHTPREFERREDAREA)
	iDesiredYachtID += -1 // the stat default is zero, so we use this as NULL and add 1 when storing
	IF (iDesiredYachtID > -1)
		IF (eZone = GET_ZONE_YACHT_IS_IN(iDesiredYachtID))
			RETURN 0
		ENDIF
	ENDIF
	
	IF HAS_LOCAL_PLAYER_COMPLETED_ALL_YACHT_MISSIONS_AS_LEADER()
		SWITCH eZone
			CASE PYSZ_1 	RETURN g_sMPTunables.iYACHT_MOVE_COST_NORTHCHUMASH_MIS_COM	BREAK
			CASE PYSZ_2		RETURN g_sMPTunables.iYACHT_MOVE_COST_LAGOZANCUDO_MIS_COM	BREAK
			CASE PYSZ_3		RETURN g_sMPTunables.iYACHT_MOVE_COST_PACIFICBLUFFS_MIS_COM	BREAK
			CASE PYSZ_4		RETURN g_sMPTunables.iYACHT_MOVE_COST_VESPUCCIBEACH_MIS_COM	BREAK
			CASE PYSZ_5 	RETURN g_sMPTunables.iYACHT_MOVE_COST_LSIA_MIS_COM 			BREAK
			CASE PYSZ_6 	RETURN g_sMPTunables.iYACHT_MOVE_COST_TERMINAL_MIS_COM 		BREAK
			CASE PYSZ_7 	RETURN g_sMPTunables.iYACHT_MOVE_COST_PALOMINO_MIS_COM 		BREAK
			CASE PYSZ_8 	RETURN g_sMPTunables.iYACHT_MOVE_COST_TATAVIAM_MIS_COM 		BREAK
			CASE PYSZ_9 	RETURN g_sMPTunables.iYACHT_MOVE_COST_SANCHIANSKI_MIS_COM 	BREAK
			CASE PYSZ_10	RETURN g_sMPTunables.iYACHT_MOVE_COST_MOUNTGORDO_MIS_COM 	BREAK
			CASE PYSZ_11	RETURN g_sMPTunables.iYACHT_MOVE_COST_PROCOPIO_MIS_COM 		BREAK
			CASE PYSZ_12	RETURN g_sMPTunables.iYACHT_MOVE_COST_PALETO_MIS_COM 		BREAK		
		ENDSWITCH
	ELSE
		SWITCH eZone
			CASE PYSZ_1 	RETURN g_sMPTunables.iYACHT_MOVE_COST_NORTHCHUMASH	BREAK
			CASE PYSZ_2		RETURN g_sMPTunables.iYACHT_MOVE_COST_LAGOZANCUDO	BREAK
			CASE PYSZ_3		RETURN g_sMPTunables.iYACHT_MOVE_COST_PACIFICBLUFFS	BREAK
			CASE PYSZ_4		RETURN g_sMPTunables.iYACHT_MOVE_COST_VESPUCCIBEACH	BREAK
			CASE PYSZ_5 	RETURN g_sMPTunables.iYACHT_MOVE_COST_LSIA BREAK
			CASE PYSZ_6 	RETURN g_sMPTunables.iYACHT_MOVE_COST_TERMINAL BREAK
			CASE PYSZ_7 	RETURN g_sMPTunables.iYACHT_MOVE_COST_PALOMINO BREAK
			CASE PYSZ_8 	RETURN g_sMPTunables.iYACHT_MOVE_COST_TATAVIAM BREAK
			CASE PYSZ_9 	RETURN g_sMPTunables.iYACHT_MOVE_COST_SANCHIANSKI BREAK
			CASE PYSZ_10	RETURN g_sMPTunables.iYACHT_MOVE_COST_MOUNTGORDO BREAK
			CASE PYSZ_11	RETURN g_sMPTunables.iYACHT_MOVE_COST_PROCOPIO BREAK
			CASE PYSZ_12	RETURN g_sMPTunables.iYACHT_MOVE_COST_PALETO BREAK		
		ENDSWITCH
	ENDIF
	RETURN 5000
ENDFUNC

FUNC BOOL IS_YACHT_LOCATION_MOVE_BLOCKED_BY_TUNABLE(PRIVATE_YACHT_SPAWN_ZONE_ENUM eZone)

	IF g_sMPTunables.byacht_disable_move
		RETURN TRUE
	ENDIF

	SWITCH eZone		
		CASE PYSZ_1 	RETURN g_sMPTunables.byacht_disable_move_northchumash        BREAK
		CASE PYSZ_2		RETURN g_sMPTunables.byacht_disable_move_lagozancudo         BREAK
		CASE PYSZ_3		RETURN g_sMPTunables.byacht_disable_move_pacificbluffs       BREAK
		CASE PYSZ_4		RETURN g_sMPTunables.byacht_disable_move_vespuccibeach       BREAK
		CASE PYSZ_5 	RETURN g_sMPTunables.byacht_disable_move_lsia                BREAK
		CASE PYSZ_6 	RETURN g_sMPTunables.byacht_disable_move_terminal            BREAK
		CASE PYSZ_7 	RETURN g_sMPTunables.byacht_disable_move_palomino            BREAK
		CASE PYSZ_8 	RETURN g_sMPTunables.byacht_disable_move_tataviam            BREAK
		CASE PYSZ_9 	RETURN g_sMPTunables.byacht_disable_move_sanchianski         BREAK
		CASE PYSZ_10	RETURN g_sMPTunables.byacht_disable_move_mountgordo          BREAK
		CASE PYSZ_11	RETURN g_sMPTunables.byacht_disable_move_procopio            BREAK
		CASE PYSZ_12	RETURN g_sMPTunables.byacht_disable_move_paleto              BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC RUN_DELAYED_WARP()
	IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(stDelayedWarp, 1600)
		INT iStartYacht = LOCAL_PLAYER_PRIVATE_YACHT_ID()
		SET_BIT(iYachtMoveBitset, YACHT_MOVE_BITSET_FLAG_START_WARP)
		PRIVATE_YACHT_SPAWN_ZONE_ENUM eZone = GET_YACHT_LOCATION_FOR_MENU_POSITION(iYachtMoveCurrentItem)
		START_WARP_PRIVATE_YACHT_TO_ZONE(eZone)
		tdYachtMoveTimer = GET_NETWORK_TIME()
		CLEAR_BIT(iYachtMoveBitset, YACHT_MOVE_BITSET_FLAG_CONF_MOVE)
		bDelayWarpAfterConfirm = FALSE
		
		///TELEMETRY
		VECTOR vStart = GET_ENTITY_COORDS(PLAYER_PED_ID() , FALSE)	
		VECTOR vLocation = GET_COORDS_FOR_YACHT_SPAWN_ZONE(GET_YACHT_LOCATION_FOR_MENU_POSITION(iYachtMoveCurrentItem))		
		INT iStartHash, iEndHash
		TEXT_LABEL_23 tempName = GET_YACHT_LOCATION_NAME_FOR_MENU(eZone)
		iEndHash = GET_HASH_KEY(tempName)
		
		eZone = GET_ZONE_YACHT_IS_IN (iStartYacht)
		tempName = GET_YACHT_LOCATION_NAME_FOR_MENU(eZone)
		iStartHash = GET_HASH_KEY(tempName)
		PLAYSTATS_FAST_TRVL(iStartHash, vStart.x, vStart.y, vStart.z, iEndHash, vLocation.x, vLocation.y, vLocation.z, ENUM_TO_INT(FT_TYPE_YACHT))
		PRINTLN("[AM_MP_YACHT][PLAYSTATS_FAST_TRVL] START hash = ", iStartHash," END hash = ",iEndHash)
		PRINTLN("[AM_MP_YACHT][PLAYSTATS_FAST_TRVL] START: ",vStart.x, ", ", vStart.y,", ", vStart.z)
		PRINTLN("[AM_MP_YACHT][PLAYSTATS_FAST_TRVL] DESTINATION: ",vLocation.x, ", ", vLocation.y,", ", vLocation.z)
		PRINTLN("[AM_MP_YACHT][PLAYSTATS_FAST_TRVL] TYPE: ", ENUM_TO_INT(FT_TYPE_YACHT))
		PRINTLN("[AM_MP_YACHT][PLAYSTATS_FAST_TRVL] endLoc: ", iYachtMoveCurrentItem)
	ENDIF
ENDPROC

//SPEECH
PROC TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_ENTRY(BOOL bForceStopSpeech = FALSE)


	BOOL IsPlayerFemale = IS_PLAYER_FEMALE()

	STRING Context, Params

	IF GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID()) = GET_YACHT_PLAYER_IS_ON(PLAYER_ID())
		PRINTLN("[YACHTCONT] TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_ENTRY - my captain played ")

		//captain, my captain.
		Context = "CAPTAIN_HI"
		Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)

	ELSE
		PRINTLN("[YACHTCONT] TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_ENTRY - not my captain played ")

		//Not my captain
		IF IsPlayerFemale
			Context = "GENERIC_HI_FEMALE"
			Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)
		ELSE
			Context = "GENERIC_HI_MALE"
			Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)
		ENDIF

	ENDIF
	IF bForceStopSpeech
		STOP_CURRENT_PLAYING_SPEECH(pedID_Captain)
	ENDIF
	PLAY_PED_AMBIENT_SPEECH_NATIVE(pedID_Captain, Context,Params, FALSE)

ENDPROC
PROC TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_WHERETO()
	PRINTLN("[YACHTCONT] TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_WHERETO - played ")
	STRING Context, Params
	Context = "CAPTAIN_WHERE_TO"
	Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)
	PLAY_PED_AMBIENT_SPEECH_NATIVE(pedID_Captain, Context,Params, FALSE)
ENDPROC
PROC TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_IDLE()
	PRINTLN("[YACHTCONT] TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_IDLE - played ")
	STRING Context, Params
	Context = "CAPTAIN_IDLE"
	Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)
	PLAY_PED_AMBIENT_SPEECH_NATIVE(pedID_Captain, Context,Params, FALSE)
ENDPROC
PROC TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_BYE()

	FLOAT fDist 	= GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedID_Captain)
	FLOAT fHeading 	= (GET_HEADING_OF_PRIVATE_YACHT(iCurrentYacht) - 180)
	
	IF fHeading < 0.0
		fHeading += 360
	ELSE
		WHILE fHeading > 180.0
			fHeading -= 360.0
		ENDWHILE
		WHILE fHeading < -180.0
			fHeading += 360.0
		ENDWHILE
	ENDIF
	
	IF bCaptainSaidBye
	AND fDist < 3.65
		bCaptainSaidBye = FALSE
		EXIT
	ENDIF
		
	IF NOT bCaptainSaidBye
	AND  fDist > 6.4
	AND GET_ENTITY_HEADING(PLAYER_PED_ID()) >= (fHeading- 30)
	AND GET_ENTITY_HEADING(PLAYER_PED_ID()) <= (fHeading + 30)
		STRING Context, Params
		IF GET_RANDOM_INT_IN_RANGE(0, 10) > 4
			PRINTLN("[YACHTCONT] TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_BYE - my captain played ")
			Context = "CAPTAIN_BYE"
			Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)
		ELSE
			PRINTLN("[YACHTCONT] TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_BYE - not my captain played ")
			Context = "GENERIC_BYE"
			Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)
		ENDIF
		PLAY_PED_AMBIENT_SPEECH_NATIVE(pedID_Captain, Context,Params, FALSE)
		bCaptainSaidBye = TRUE
	ENDIF
ENDPROC

PROC TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_CHAT()
	STRING Context, Params
	IF GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID()) = GET_YACHT_PLAYER_IS_ON(PLAYER_ID())
		PRINTLN("[YACHTCONT] TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_CHAT - my captain played ")
		Context = "CAPTAIN_CHAT"
		Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)	
		PLAY_PED_AMBIENT_SPEECH_NATIVE(pedID_Captain, Context,Params, FALSE)
	ENDIF
ENDPROC

PROC TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_AFFIRM()
	STRING Context, Params
	IF GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID()) = GET_YACHT_PLAYER_IS_ON(PLAYER_ID())
		STOP_CURRENT_PLAYING_SPEECH(pedID_Captain)
		PRINTLN("[YACHTCONT] TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_AFFIRM - my captain played ")
		Context = "CAPTAIN_AFFIRM"
		Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)	
		PLAY_PED_AMBIENT_SPEECH_NATIVE(pedID_Captain, Context,Params, FALSE)
	ENDIF
ENDPROC

PROC TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_NEGATIVE()
	STRING Context, Params
	IF GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID()) = GET_YACHT_PLAYER_IS_ON(PLAYER_ID())
		PRINTLN("[YACHTCONT] TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_NEGATIVE - my captain played ")
		Context = "CAPTAIN_NEGATIVE"
		Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)	
		PLAY_PED_AMBIENT_SPEECH_NATIVE(pedID_Captain, Context,Params, FALSE)
	ENDIF
ENDPROC
PROC TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_STORMY()
	STRING Context, Params
	IF GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID()) = GET_YACHT_PLAYER_IS_ON(PLAYER_ID())
		PRINTLN("[YACHTCONT] TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_STORMY - my captain played ")
		Context = "CAPTAIN_STORMY"
		Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)	
		PLAY_PED_AMBIENT_SPEECH_NATIVE(pedID_Captain, Context,Params, FALSE)
	ENDIF
ENDPROC
PROC TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_RAINY()
	STRING Context, Params
	IF GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID()) = GET_YACHT_PLAYER_IS_ON(PLAYER_ID())
		PRINTLN("[YACHTCONT] TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_RAINY - my captain played ")
		Context = "CAPTAIN_RAINY"
		Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)	
		PLAY_PED_AMBIENT_SPEECH_NATIVE(pedID_Captain, Context,Params, FALSE)
	ENDIF
ENDPROC
PROC TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_SUNNY()
	STRING Context, Params
	IF GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID()) = GET_YACHT_PLAYER_IS_ON(PLAYER_ID())
		PRINTLN("[YACHTCONT] TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_SUNNY - my captain played ")
		Context = "CAPTAIN_SUNNY"
		Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)	
		PLAY_PED_AMBIENT_SPEECH_NATIVE(pedID_Captain, Context,Params, FALSE)
	ENDIF
ENDPROC
PROC TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_FOG()
	STRING Context, Params
	IF GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID()) = GET_YACHT_PLAYER_IS_ON(PLAYER_ID())
		PRINTLN("[YACHTCONT] TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_FOG - my captain played ")
		Context = "CAPTAIN_FOG"
		Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)	
		PLAY_PED_AMBIENT_SPEECH_NATIVE(pedID_Captain, Context,Params, FALSE)
	ENDIF
ENDPROC



PROC TRIGGER_YACHT_CAPTAIN_SPEED_CURRENT_WEATHER()
	IF IS_PREV_WEATHER_TYPE("RAIN")
	OR IS_NEXT_WEATHER_TYPE("RAIN")
		TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_RAINY()
	ELIF IS_NEXT_WEATHER_TYPE("THUNDER")
	OR IS_PREV_WEATHER_TYPE("THUNDER")
		TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_STORMY()
	ELIF IS_NEXT_WEATHER_TYPE("EXTRASUNNY")
	OR IS_PREV_WEATHER_TYPE("EXTRASUNNY") 
		TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_SUNNY()
	ELIF IS_NEXT_WEATHER_TYPE("FOGGY")
	OR IS_PREV_WEATHER_TYPE("FOGGY") 
		TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_FOG()
	ENDIF
ENDPROC

PROC TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_LAGO()
	STRING Context, Params
	IF GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID()) = GET_YACHT_PLAYER_IS_ON(PLAYER_ID())
		PRINTLN("[YACHTCONT] TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_LAGO - my captain played ")
		Context = "CAPTAIN_LAGO"
		Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)	
		PLAY_PED_AMBIENT_SPEECH_NATIVE(pedID_Captain, Context,Params, FALSE)
	ENDIF
ENDPROC
PROC TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_CHUMASH()
	STRING Context, Params
	IF GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID()) = GET_YACHT_PLAYER_IS_ON(PLAYER_ID())
		PRINTLN("[YACHTCONT] TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_CHUMASH - my captain played ")
		Context = "CAPTAIN_CHUMASH"
		Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)	
		PLAY_PED_AMBIENT_SPEECH_NATIVE(pedID_Captain, Context,Params, FALSE)
	ENDIF
ENDPROC
PROC TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_PACIFIC()
	STRING Context, Params
	IF GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID()) = GET_YACHT_PLAYER_IS_ON(PLAYER_ID())
		PRINTLN("[YACHTCONT] TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_PACIFIC - my captain played ")
		Context = "CAPTAIN_PACIFIC"
		Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)	
		PLAY_PED_AMBIENT_SPEECH_NATIVE(pedID_Captain, Context,Params, FALSE)
	ENDIF
ENDPROC
PROC TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_VESPUCCI()
	STRING Context, Params
	IF GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID()) = GET_YACHT_PLAYER_IS_ON(PLAYER_ID())
		PRINTLN("[YACHTCONT] TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_VESPUCCI - my captain played ")
		Context = "CAPTAIN_VESPUCCI"
		Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)	
		PLAY_PED_AMBIENT_SPEECH_NATIVE(pedID_Captain, Context,Params, FALSE)
	ENDIF
ENDPROC
PROC TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_LSIA()
	STRING Context, Params
	IF GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID()) = GET_YACHT_PLAYER_IS_ON(PLAYER_ID())
		PRINTLN("[YACHTCONT] TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_LSIA - my captain played ")
		Context = "CAPTAIN_LSIA"
		Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)	
		PLAY_PED_AMBIENT_SPEECH_NATIVE(pedID_Captain, Context,Params, FALSE)
	ENDIF
ENDPROC
PROC TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_TERMINAL()
	STRING Context, Params
	IF GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID()) = GET_YACHT_PLAYER_IS_ON(PLAYER_ID())
		PRINTLN("[YACHTCONT] TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_TERMINAL - my captain played ")
		Context = "CAPTAIN_TERMINAL"
		Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)	
		PLAY_PED_AMBIENT_SPEECH_NATIVE(pedID_Captain, Context,Params, FALSE)
	ENDIF
ENDPROC
PROC TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_PALOMINO()
	STRING Context, Params
	IF GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID()) = GET_YACHT_PLAYER_IS_ON(PLAYER_ID())
		PRINTLN("[YACHTCONT] TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_PALOMINO - my captain played ")
		Context = "CAPTAIN_PALOMINO"
		Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)	
		PLAY_PED_AMBIENT_SPEECH_NATIVE(pedID_Captain, Context,Params, FALSE)
	ENDIF
ENDPROC
PROC TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_TATAVIAM()
	STRING Context, Params
	IF GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID()) = GET_YACHT_PLAYER_IS_ON(PLAYER_ID())
		PRINTLN("[YACHTCONT] TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_TATAVIAM - my captain played ")
		Context = "CAPTAIN_TATAVIAM"
		Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)	
		PLAY_PED_AMBIENT_SPEECH_NATIVE(pedID_Captain, Context,Params, FALSE)
	ENDIF
ENDPROC
PROC TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_CHIANSKI()
	STRING Context, Params
	IF GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID()) = GET_YACHT_PLAYER_IS_ON(PLAYER_ID())
		PRINTLN("[YACHTCONT] TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_CHIANSKI - my captain played ")
		Context = "CAPTAIN_CHIANSKI"
		Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)	
		PLAY_PED_AMBIENT_SPEECH_NATIVE(pedID_Captain, Context,Params, FALSE)
	ENDIF
ENDPROC
PROC TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_GORDO()
	STRING Context, Params
	IF GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID()) = GET_YACHT_PLAYER_IS_ON(PLAYER_ID())
		PRINTLN("[YACHTCONT] TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_GORDO - my captain played ")
		Context = "CAPTAIN_GORDO"
		Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)	
		PLAY_PED_AMBIENT_SPEECH_NATIVE(pedID_Captain, Context,Params, FALSE)
	ENDIF
ENDPROC
PROC TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_PROCOPIO()
	STRING Context, Params
	IF GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID()) = GET_YACHT_PLAYER_IS_ON(PLAYER_ID())
		PRINTLN("[YACHTCONT] TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_PROCOPIO - my captain played ")
		Context = "CAPTAIN_PROCOPIO"
		Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)	
		PLAY_PED_AMBIENT_SPEECH_NATIVE(pedID_Captain, Context,Params, FALSE)
	ENDIF
ENDPROC
PROC TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_PALETO()
	STRING Context, Params
	IF GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID()) = GET_YACHT_PLAYER_IS_ON(PLAYER_ID())
		PRINTLN("[YACHTCONT] TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_PALETO - my captain played ")
		Context = "CAPTAIN_PALETO"
		Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)	
		PLAY_PED_AMBIENT_SPEECH_NATIVE(pedID_Captain, Context,Params, FALSE)
	ENDIF
ENDPROC
PROC TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_GENERIC_LOCATION()
	STRING Context, Params
	IF GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID()) = GET_YACHT_PLAYER_IS_ON(PLAYER_ID())
		PRINTLN("[YACHTCONT] TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_GENERIC_LOCATION - my captain played ")
		Context = "CAPTAIN_GENERIC_LOCATION"
		Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)	
		PLAY_PED_AMBIENT_SPEECH_NATIVE(pedID_Captain, Context,Params, FALSE)
	ENDIF
ENDPROC

PROC TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_LOCATION_MOVE(PRIVATE_YACHT_SPAWN_ZONE_ENUM eZone)
	IF GET_RANDOM_INT_IN_RANGE(0, 10) >= 8
		TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_GENERIC_LOCATION()
	ELSE	
		SWITCH eZone	
			CASE PYSZ_1 	TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_CHUMASH()			BREAK
			CASE PYSZ_2		TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_LAGO()				BREAK
			CASE PYSZ_3		TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_PACIFIC()			BREAK
			CASE PYSZ_4		TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_VESPUCCI()			BREAK
			CASE PYSZ_5 	TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_LSIA()				BREAK
			CASE PYSZ_6 	TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_TERMINAL()			BREAK
			CASE PYSZ_7 	TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_PALOMINO()			BREAK
			CASE PYSZ_8 	TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_TATAVIAM()			BREAK
			CASE PYSZ_9 	TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_CHIANSKI()			BREAK
			CASE PYSZ_10	TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_GORDO()				BREAK
			CASE PYSZ_11	TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_PROCOPIO()			BREAK
			CASE PYSZ_12	TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_PALETO()			BREAK
			DEFAULT 		TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_GENERIC_LOCATION()	BREAK
		ENDSWITCH
	ENDIF	
ENDPROC

PROC TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_AFFORD()
	STRING Context, Params
	IF GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID()) = GET_YACHT_PLAYER_IS_ON(PLAYER_ID())
		PRINTLN("[YACHTCONT] TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_AFFORD - my captain played ")
		Context = "CAPTAIN_AFFORD"
		Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)	
		PLAY_PED_AMBIENT_SPEECH_NATIVE(pedID_Captain, Context,Params, FALSE)
	ENDIF
ENDPROC
PROC TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_BUMP()
	STRING Context, Params
	IF GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID()) = GET_YACHT_PLAYER_IS_ON(PLAYER_ID())
	AND NOT bDelaySpeechAfterCaptainShocked
		IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), pedID_Captain)
		AND bHasBumpedCaptainSpeech = FALSE
			STOP_CURRENT_PLAYING_SPEECH(pedID_Captain)
			PRINTLN("[YACHTCONT] TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_BUMP - my captain played ")
			Context = "BUMP"
			Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)	
			PLAY_PED_AMBIENT_SPEECH_NATIVE(pedID_Captain, Context,Params, FALSE)
			bHasBumpedCaptainSpeech = TRUE
		ELIF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedID_Captain) >= 2.0
			 bHasBumpedCaptainSpeech = FALSE
		ENDIF
	ENDIF
ENDPROC
PROC TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_GENERIC_FRIGHTENED_HIGH()
	STRING Context, Params
	PRINTLN("[YACHTCONT] TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_GENERIC_FRIGHTENED_HIGH - my captain played ")
	Context = "GENERIC_FRIGHTENED_HIGH"
	Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)	
	PLAY_PED_AMBIENT_SPEECH_NATIVE(pedID_Captain, Context,Params, FALSE)
ENDPROC
PROC TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_GENERIC_FRIGHTENED_MED()
	STRING Context, Params
	PRINTLN("[YACHTCONT] TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_GENERIC_FRIGHTENED_MED - my captain played ")
	Context = "GENERIC_FRIGHTENED_MED"
	Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)	
	PLAY_PED_AMBIENT_SPEECH_NATIVE(pedID_Captain, Context,Params, FALSE)
ENDPROC
PROC TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_GENERIC_SHOCKED_HIGH()
	STRING Context, Params
	PRINTLN("[YACHTCONT] TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_GENERIC_SHOCKED_HIGH - my captain played ")
	Context = "GENERIC_SHOCKED_HIGH"
	Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)	
	PLAY_PED_AMBIENT_SPEECH_NATIVE(pedID_Captain, Context,Params, FALSE)
ENDPROC
PROC TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_GENERIC_SHOCKED_MED()
	STRING Context, Params
	PRINTLN("[YACHTCONT] TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_GENERIC_SHOCKED_MED - my captain played ")
	Context = "GENERIC_SHOCKED_MED"
	Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)	
	PLAY_PED_AMBIENT_SPEECH_NATIVE(pedID_Captain, Context,Params, FALSE)
ENDPROC

FUNC STRING GET_CAPTAIN_YACHT_MISSION_SPEECH(INT iCurrentMission)
	STRING sSpeech = ""
	SWITCH iCurrentMission
		CASE 0	sSpeech = "CAPTAIN_MISSION_START"	BREAK
		CASE 1	sSpeech = "CAPTAIN_MISSION_ONE"		BREAK
		CASE 2	sSpeech = "CAPTAIN_MISSION_TWO"		BREAK
		CASE 3	sSpeech = "CAPTAIN_MISSION_THREE"	BREAK
		CASE 4	sSpeech = "CAPTAIN_MISSION_FOUR"	BREAK
		CASE 5	sSpeech = "CAPTAIN_MISSION_FIVE"	BREAK
	ENDSWITCH
	// Flow resets to zero after completion so using this check for last mission
	IF HAS_LOCAL_PLAYER_COMPLETED_THIS_YACHT_MISSION_AS_LEADER(eYACHT_MISSION_SIX__DDAY)
		sSpeech = "CAPTAIN_MISSION_SIX"
	ENDIF
	RETURN sSpeech
ENDFUNC

PROC TRIGGER_YACHT_CAPTAIN_CURRENT_MISSION_SPEECH()
	STRING Context = GET_CAPTAIN_YACHT_MISSION_SPEECH(ENUM_TO_INT(GET_LOCAL_PLAYER_CURRENT_YACHT_MISSION()))
	PRINTLN("[YACHTCONT] TRIGGER_YACHT_CAPTAIN_CURRENT_MISSION_SPEECH - my captain played: ", Context)
	PLAY_PED_AMBIENT_SPEECH_NATIVE(pedID_Captain, Context, AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE), FALSE)
ENDPROC

PROC RUN_CAPTAIN_CURRENT_MISSION_SPEECH()
	IF NOT bCaptainYachtMissionSpeechTriggered
	AND GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iCurrentYacht) = PLAYER_ID()
	AND GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) = HASH("YachtRm_Bridge")
	AND NOT IS_ANY_SPEECH_PLAYING(pedID_Captain)
	AND NOT bDelaySpeechAfterCaptainShocked
	AND HAS_NET_TIMER_EXPIRED_ONE_FRAME(stYachtCaptainIdleSpeech, 15000)
	AND iYachtMoveControl = 0	// Yacht location menu isn't on screen
		bCaptainYachtMissionSpeechTriggered = TRUE
		TRIGGER_YACHT_CAPTAIN_CURRENT_MISSION_SPEECH()
	ENDIF
ENDPROC

FUNC STRING GET_YACHT_MISSION_LOCATE_MESSAGE(YACHT_MISSION_DATA &sYachtMissions)
	SWITCH sYachtMissions.eCurrentMission
		CASE eYACHT_MISSION_ONE__OVERBOARD			RETURN "YACHT_LOCATE1"
		CASE eYACHT_MISSION_TWO__SALVAGE			RETURN "YACHT_LOCATE2"
		CASE eYACHT_MISSION_THREE__ALLHANDS		RETURN "YACHT_LOCATE3"
		CASE eYACHT_MISSION_FOUR__ICEBREAKER		RETURN "YACHT_LOCATE4"
		CASE eYACHT_MISSION_FIVE__PLAINSAILING		RETURN "YACHT_LOCATE5"
		CASE eYACHT_MISSION_SIX__DDAY			RETURN "YACHT_LOCATE6"
	ENDSWITCH
	RETURN ""
ENDFUNC

PROC TRIGGER_YACHT_CAPTAIN_MISSION_MAP_SPEECH()
	STRING Context, Params
	IF GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID()) = GET_YACHT_PLAYER_IS_ON(PLAYER_ID())
		PRINTLN("[YACHTCONT] TRIGGER_YACHT_CAPTAIN_MISSION_MAP_SPEECH - my captain played ")
		Context = "CAPTAIN_INTERACT_MAP"
		Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)	
		PLAY_PED_AMBIENT_SPEECH_NATIVE(pedID_Captain, Context,Params, FALSE)
	ENDIF
ENDPROC

PROC RUN_CAPTAIN_MISSION_MAP_SPEECH()
	IF NOT bCaptainYachtMissionMapSpeechTriggered
	AND GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iCurrentYacht) = PLAYER_ID()
	AND GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) = HASH("YachtRm_Bridge")
	AND NOT IS_ANY_SPEECH_PLAYING(pedID_Captain)
	AND NOT bDelaySpeechAfterCaptainShocked
	AND iYachtMoveControl = 0	// Yacht location menu isn't on screen
	AND IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_YACHT_MISSION_LOCATE_MESSAGE(g_sYachtMissions))
		bCaptainYachtMissionMapSpeechTriggered = TRUE
		TRIGGER_YACHT_CAPTAIN_MISSION_MAP_SPEECH()
	ENDIF
ENDPROC

PROC TRIGGER_YACHT_CAPTAIN_SPEECH_GET_OUT_OF_HERE()
	STRING Context, Params
	IF GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID()) = GET_YACHT_PLAYER_IS_ON(PLAYER_ID())
		PRINTLN("[YACHTCONT] TRIGGER_YACHT_CAPTAIN_SPEECH_GET_OUT_OF_HERE - my captain played ")
		Context = "CAPTAIN_GET_OUT_OF_HERE"
		Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)	
		PLAY_PED_AMBIENT_SPEECH_NATIVE(pedID_Captain, Context,Params, FALSE)
	ENDIF
ENDPROC

PROC RUN_CAPTAIN_FRIGHTENED_DIALOGUE()	
	
	IF bDelaySpeechAfterCaptainShocked
	AND HAS_NET_TIMER_EXPIRED_ONE_FRAME(stYachtCaptainFirghtenedPreventSpeech, 10000)
		CDEBUG1LN(DEBUG_AMBIENT, "RUN_CAPTAIN_FRIGHTENED_DIALOGUE: Delay Speech Timer expired")
		bDelaySpeechAfterCaptainShocked = FALSE
	ENDIF
	
	IF bHasFirghtenedCaptainSpeech
	AND NOT HAS_NET_TIMER_EXPIRED_ONE_FRAME(stYachtCaptainFirghtenedSpeech, 15000)
		EXIT
	ENDIF
	
	bHasFirghtenedCaptainSpeech = FALSE
	
	IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_ENTITY_COORDS(pedID_Captain), 8.0)
		STOP_CURRENT_PLAYING_SPEECH(pedID_Captain)
		INT iRandomNo = GET_RANDOM_INT_IN_RANGE(1, 5)
		SWITCH iRandomNo
			CASE 1	TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_GENERIC_FRIGHTENED_HIGH() 	BREAK
			CASE 2	TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_GENERIC_FRIGHTENED_MED() 	BREAK
			CASE 3	TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_GENERIC_SHOCKED_HIGH()		BREAK
			CASE 4	TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_GENERIC_SHOCKED_MED()		BREAK
		ENDSWITCH
		bHasFirghtenedCaptainSpeech 	= TRUE
		bDelaySpeechAfterCaptainShocked = TRUE
		REINIT_NET_TIMER(stYachtCaptainFirghtenedPreventSpeech)
	ENDIF
ENDPROC

PROC MAINTAIN_YACHT_CAPTAIN()

	MP_PROP_OFFSET_STRUCT sPropOffset
	GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_CAPTAIN, sPropOffset)				
	IF NOT DOES_ENTITY_EXIST(pedID_Captain)
	OR IS_PED_INJURED(pedID_Captain)
		IF iCurrentYacht >= 0
			REQUEST_MODEL(MP_M_BOATSTAFF_01)
			IF HAS_MODEL_LOADED(MP_M_BOATSTAFF_01)
			
				// Captain
				
				pedID_Captain = CREATE_PED(PEDTYPE_CIVMALE, MP_M_BOATSTAFF_01, sPropOffset.vLoc, sPropOffset.vRot.z, FALSE)
				
				SET_ENTITY_CAN_BE_DAMAGED(pedID_Captain, FALSE)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(pedID_Captain, TRUE)
				FREEZE_ENTITY_POSITION(pedID_Captain,TRUE)
				bFreezeCapt = TRUE
				SET_PED_DEFAULT_COMPONENT_VARIATION(pedID_Captain)
				SET_PED_AS_ENEMY(pedID_Captain, FALSE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedID_Captain, TRUE)
				SET_PED_CONFIG_FLAG(pedID_Captain, PCF_UseKinematicModeWhenStationary,TRUE)
				SET_PED_CONFIG_FLAG(pedID_Captain, PCF_DisableExplosionReactions,TRUE)
				SET_PED_CONFIG_FLAG(pedID_Captain, PCF_DontActivateRagdollFromExplosions,TRUE)
				SET_PED_CAN_EVASIVE_DIVE(pedID_Captain, FALSE)
				SET_TREAT_AS_AMBIENT_PED_FOR_DRIVER_LOCKON(pedID_Captain, TRUE)
				SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedID_Captain, FALSE)
				SET_TREAT_AS_AMBIENT_PED_FOR_DRIVER_LOCKON(pedID_Captain, TRUE)
				SET_PED_CAN_BE_TARGETTED(pedID_Captain, FALSE)
				SET_PED_COMPONENT_VARIATION(pedID_Captain, PED_COMP_LEG, 0, 1)
				SET_PED_COMPONENT_VARIATION(pedID_Captain, PED_COMP_TORSO, 0, 0)
				SET_PED_PROP_INDEX(pedID_Captain, ANCHOR_HEAD, 0, 0)
				SET_MODEL_AS_NO_LONGER_NEEDED(MP_M_BOATSTAFF_01)
				PRINTLN("MAINTAIN_YACHT_CAPTAIN- Creating captain at ",sPropOffset.vLoc," yacht #",iCurrentYacht)
				
				REQUEST_ANIM_DICT("ANIM@AMB@YACHT@CAPTAIN@")
			ENDIF
		ENDIF
	ELSE
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedID_Captain),sPropOffset.vLoc) >= 5
			PRINTLN("MAINTAIN_YACHT_CAPTAIN- captain out of position resetting to ",sPropOffset.vLoc," yacht #",iCurrentYacht)
			SET_ENTITY_COORDS(pedID_Captain,sPropOffset.vLoc)
			SET_ENTITY_HEADING(pedID_Captain,sPropOffset.vRot.z)
		ENDIF
		
		IF GET_DISTANCE_BETWEEN_ENTITIES(pedID_Captain, PLAYER_PED_ID()) < 10
		AND GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) = HASH("YachtRm_Bridge")
			IF bFreezeCapt
				FREEZE_ENTITY_POSITION(pedID_Captain,FALSE)
				bFreezeCapt = FALSE
				PRINTLN("MAINTAIN_YACHT_CAPTAIN: captain UN-frozen")
			ENDIF
		ELSE
			IF NOT bFreezeCapt
				FREEZE_ENTITY_POSITION(pedID_Captain,TRUE)
				bFreezeCapt = TRUE
				PRINTLN("MAINTAIN_YACHT_CAPTAIN: captain frozen")
			ENDIF
		ENDIF
			
		SET_PED_RESET_FLAG(pedID_Captain, PRF_DisablePotentialBlastReactions, TRUE)
		
		IF NOT bDelaySpeechAfterCaptainShocked
		AND HAS_NET_TIMER_EXPIRED_ONE_FRAME(stYachtCaptainIdleSpeech, 20000)
			IF GET_RANDOM_INT_IN_RANGE(0, 10) > 5
				TRIGGER_YACHT_CAPTAIN_SPEED_CURRENT_WEATHER()
			ELSE
				TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_CHAT()
			ENDIF
		ENDIF
		
		TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_BUMP()
		
		RUN_CAPTAIN_CURRENT_MISSION_SPEECH()
		RUN_CAPTAIN_MISSION_MAP_SPEECH()
		
		RUN_CAPTAIN_FRIGHTENED_DIALOGUE()
		
		IF GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) = GET_HASH_KEY("YachtRm_Bridge")
			TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_BYE()
		ENDIF
	
		SWITCH iYachtCaptainControl
			CASE 0
				// Turn to face player
				IF iYachtMoveControl != 0
					CLEAR_PED_TASKS(pedID_Captain)
					
					SEQUENCE_INDEX siTemp							
					OPEN_SEQUENCE_TASK(siTemp)
						TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), -1)
					CLOSE_SEQUENCE_TASK(siTemp)
					TASK_PERFORM_SEQUENCE(pedID_Captain, siTemp)
					CLEAR_SEQUENCE_TASK(siTemp)
					
					TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_ENTRY()
					
					iYachtCaptainControl = 1
				ENDIF
			BREAK
			CASE 1
				// Turn back to desk
				IF iYachtMoveControl != 0
					tdCaptainTimer = GET_NETWORK_TIME()
				ENDIF
				
				IF iYachtMoveControl = 0
				AND ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), tdCaptainTimer)) > 4000
				
					CLEAR_PED_TASKS(pedID_Captain)
					TASK_CLEAR_LOOK_AT(pedID_Captain)
					
					GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_CAPTAIN, sPropOffset)
					
					SEQUENCE_INDEX siTemp							
					OPEN_SEQUENCE_TASK(siTemp)
						TASK_ACHIEVE_HEADING(NULL, sPropOffset.vRot.z)
						IF HAS_ANIM_DICT_LOADED("ANIM@AMB@YACHT@CAPTAIN@")
							TASK_PED_SLIDE_TO_COORD(NULL, sPropOffset.vLoc, sPropOffset.vRot.z)
							TASK_PLAY_ANIM(NULL, "ANIM@AMB@YACHT@CAPTAIN@", "idle", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING)
						ENDIF
					CLOSE_SEQUENCE_TASK(siTemp)
					TASK_PERFORM_SEQUENCE(pedID_Captain, siTemp)
					CLEAR_SEQUENCE_TASK(siTemp)
					
					iYachtCaptainControl = 0
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC MAINTAIN_YACHT_MOVE()
	
	IF IS_BROWSER_OPEN()
	OR SHOULD_LAUNCH_CRIMINAL_STARTER_PACK_BROWSER()
		EXIT
	ENDIF
		
	// Update transaction state
	IF USE_SERVER_TRANSACTIONS()
		IF iYachtMoveTransactionState = YACHT_MOVE_TRANSACTION_FAILED
			DELETE_CASH_TRANSACTION(iYachtMoveTransactionID)
			iYachtMoveTransactionState = YACHT_MOVE_TRANSACTION_INVALID
		ENDIF
		
		IF iYachtMoveTransactionState != YACHT_MOVE_TRANSACTION_INVALID
			IF IS_CASH_TRANSACTION_COMPLETE(iYachtMoveTransactionID)
				IF GET_CASH_TRANSACTION_STATUS(iYachtMoveTransactionID) = CASH_TRANSACTION_STATUS_SUCCESS
					iYachtMoveTransactionState = YACHT_MOVE_TRANSACTION_SUCCESS
				ELSE
					iYachtMoveTransactionState = YACHT_MOVE_TRANSACTION_FAILED
					DELETE_CASH_TRANSACTION(iYachtMoveTransactionID)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// Grab the current analogue stick positions
	INT iLeftX, iLeftY, iRightX, iRightY, iMoveUp
	GET_CONTROL_VALUE_OF_ANALOGUE_STICKS( iLeftX, iLeftY, iRightX, iRightY)
		
	// Set the input flag states
	BOOL bAccept		 = (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
	BOOL bBack			 = (IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) OR IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL))
	BOOL bPrevious	     = ((iLeftY < -64) OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)) 
	BOOL bNext		     = ((iLeftY > 64) OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN))			
	
	SWITCH iYachtMoveControl
		CASE 0 // Wait for trigger
			IF NOT IS_PED_INJURED(pedID_Captain)
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND NOT IS_PLAYER_DOING_A_YACHT_WARP(PLAYER_ID())
			AND GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID()) = iCurrentYacht
			AND NOT (GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID()) AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GB_YACHT_ROBBERY)
			AND GET_DISTANCE_BETWEEN_ENTITIES(pedID_Captain, PLAYER_PED_ID()) < 1.7
			AND IS_PED_FACING_PED(PLAYER_PED_ID(), pedID_Captain, 75.0)
			AND GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) = HASH("YachtRm_Bridge")
			AND NOT g_sMPTunables.byacht_disable_move
			AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_YACHT_MISSION_LOCATE_MESSAGE(g_sYachtMissions))
			AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
				IF iYachtMoveContext = NEW_CONTEXT_INTENTION
					REGISTER_CONTEXT_INTENTION(iYachtMoveContext, CP_HIGH_PRIORITY, "YACHT_MOVE")
				ENDIF
				IF HAS_CONTEXT_BUTTON_TRIGGERED(iYachtMoveContext)
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
					RELEASE_CONTEXT_INTENTION(iYachtMoveContext)
					LOAD_MENU_ASSETS()
					iYachtMoveBitset = 0
					fYachtMapZoom = 0
					iYachtMoveTransactionState = YACHT_MOVE_TRANSACTION_INVALID
					TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_WHERETO()
					iYachtMoveControl = 1 // Menu process
				ENDIF
			ELIF iYachtMoveContext != NEW_CONTEXT_INTENTION
				RELEASE_CONTEXT_INTENTION(iYachtMoveContext)
			ENDIF
		BREAK
		CASE 1 // Menu process
		
			IF LOAD_MENU_ASSETS()
			AND NOT IS_PAUSE_MENU_ACTIVE_EX()
			AND NOT IS_SYSTEM_UI_BEING_DISPLAYED()
			AND NOT IS_WARNING_MESSAGE_ACTIVE()
			AND NOT g_sShopSettings.bProcessStoreAlert
				
				
				IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
				ENDIF
				
				BOOL bMenuBuiltThisFrame
				bMenuBuiltThisFrame = FALSE
				
				IF NOT bDelaySpeechAfterCaptainShocked
				AND HAS_NET_TIMER_EXPIRED_ONE_FRAME(stYachtCaptainIdleSpeech, 18000)	
					TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_IDLE()
				ENDIF
								
				//--------------------------------------------------------------------------------
				// Mouse control support  
				IF IS_PC_VERSION()	
				AND NOT NETWORK_TEXT_CHAT_IS_TYPING()
					IF IS_USING_CURSOR(FRONTEND_CONTROL)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
						DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)	
						ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_ACCEPT)
						ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_CANCEL)
						ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_SCROLL_UP)
						ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_SCROLL_DOWN)					
						HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
						HANDLE_MENU_CURSOR(FALSE)
					ENDIF
					
					// Mouse select
					IF IS_MENU_CURSOR_ACCEPT_PRESSED()
					AND NOT bDelayWarpAfterConfirm
						IF g_iMenuCursorItem = iYachtMoveCurrentItem
							bAccept = TRUE
						ELSE
							IF IS_BIT_SET(iYachtMoveBitset, YACHT_MOVE_BITSET_FLAG_CONF_MOVE)
								bBack = TRUE
							ENDIF
							iYachtMoveCurrentItem = g_iMenuCursorItem
							SET_CURRENT_MENU_ITEM(iYachtMoveCurrentItem)		
						ENDIF		
					ENDIF
				
					// Menu cursor back
					IF IS_MENU_CURSOR_CANCEL_PRESSED()
						bBack = TRUE
					ENDIF
					
					// Mouse scroll up
					IF IS_MENU_CURSOR_SCROLL_UP_PRESSED()
						bPrevious = TRUE
					ENDIF
					
					// Mouse scroll down
					IF IS_MENU_CURSOR_SCROLL_DOWN_PRESSED()
						bNext = TRUE
					ENDIF
				ENDIF				
				//--------------------------------------------------------------------------------
				
				// Prepare for warp
				IF IS_BIT_SET(iYachtMoveBitset, YACHT_MOVE_BITSET_FLAG_START_WARP)
					IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), tdYachtMoveTimer)) > 3000
					OR NOT IS_GAMEPLAY_CAM_RENDERING()
						CLEAR_BIT(iYachtMoveBitset, YACHT_MOVE_BITSET_FLAG_START_WARP)
						iYachtMoveControl = 0 // Wait for trigger
					ENDIF
			
				// Build menu
				ELIF NOT IS_BIT_SET(iYachtMoveBitset, YACHT_MOVE_BITSET_FLAG_MENU_BUILT)
					CLEAR_MENU_DATA()
					SET_MENU_TITLE("YACHT_MOVE_T")
					SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
					SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)		

					INT iLocation
					PRIVATE_YACHT_SPAWN_ZONE_ENUM eZone
					PRIVATE_YACHT_SPAWN_ZONE_ENUM eCurrentZone
					eCurrentZone = GET_ZONE_OF_LOCAL_PLAYER_YACHT()
					
					iYachtMoveCurrentItem = 0
					
					INT iMoveCost 					
					
					REPEAT (iMAX_PRIVATE_YACHT_SPAWN_ZONES - 1) iLocation
						eZone = GET_YACHT_LOCATION_FOR_MENU_POSITION(iLocation)
						ADD_MENU_ITEM_TEXT(iLocation, GET_YACHT_LOCATION_NAME_FOR_MENU(eZone))
						
						IF eCurrentZone = eZone
							ADD_MENU_ITEM_TEXT(iLocation, "", 1)
							ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_YACHT)
							iYachtMoveCurrentItem = iLocation
						ELSE
						
							iMoveCost = GET_YACHT_MOVE_COST(eCurrentZone, eZone)
						
							IF (iMoveCost = 0)
								ADD_MENU_ITEM_TEXT(iLocation, "PIM_DFREE", 0)
							ELSE
								ADD_MENU_ITEM_TEXT(iLocation, "ITEM_COST", 1)
								ADD_MENU_ITEM_TEXT_COMPONENT_INT(iMoveCost)
							ENDIF
						ENDIF
					ENDREPEAT
					
					SET_CURRENT_MENU_ITEM(iYachtMoveCurrentItem)
					
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
					ADD_MENU_HELP_KEY_GROUP(INPUTGROUP_FRONTEND_TRIGGERS, "IB_ZOOM")
					
					IF NOT g_matcBlockAndHideAllMissions
						Block_All_MissionsAtCoords_Missions(TRUE)
						SET_BIT(iYachtMoveBitset, YACHT_MOVE_BITSET_FLAG_BLOCK_MISSIONS)
					ENDIF
					
					iBlipHideStatValue = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_PIM_HIDE_MISC)
					SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_PIM_HIDE_MISC, -1)
					g_bForceUpdateActivityBlips = TRUE
					SET_HIDE_ALL_JOB_BLIPS_FOR_YACHT_STATUS(TRUE)
					SET_BIT(iYachtMoveBitset, YACHT_MOVE_BITSET_FLAG_HIDE_BLIPS)
					UPDATE_ALL_SHOP_BLIPS()
					
					CLEAR_BIT(iYachtMoveBitset, YACHT_MOVE_BITSET_FLAG_CONF_MOVE)
					
					SET_BIT(iYachtMoveBitset, YACHT_MOVE_BITSET_FLAG_MENU_BUILT)
					
					bMenuBuiltThisFrame = TRUE
				
				
				// Purchase
				ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT) OR (iYachtMoveTransactionState != YACHT_MOVE_TRANSACTION_INVALID) 
				OR bAccept = TRUE
				
					// Transaction checks
					IF iYachtMoveTransactionState = YACHT_MOVE_TRANSACTION_PENDING
						// wait
					ELIF iYachtMoveTransactionState = YACHT_MOVE_TRANSACTION_FAILED
						SET_CURRENT_MENU_ITEM_DESCRIPTION("YACHT_FAIL_2", 4000) // There was an error processing this transaction.
						PLAY_SOUND_FRONTEND(-1, "ERROR", "HUD_FRONTEND_DEFAULT_SOUNDSET")
						CLEAR_BIT(iYachtMoveBitset, YACHT_MOVE_BITSET_FLAG_CONF_MOVE)
						
					// Current
					ELIF GET_ZONE_OF_LOCAL_PLAYER_YACHT() = GET_YACHT_LOCATION_FOR_MENU_POSITION(iYachtMoveCurrentItem)
						SET_CURRENT_MENU_ITEM_DESCRIPTION("YACHT_FAIL_0", 4000) // Your Yacht is already located here.
						PLAY_SOUND_FRONTEND(-1, "ERROR", "HUD_FRONTEND_DEFAULT_SOUNDSET")
						CLEAR_BIT(iYachtMoveBitset, YACHT_MOVE_BITSET_FLAG_CONF_MOVE)
						
					// Unavailable
					ELIF NOT IS_FREE_YACHT_SLOT_NEAR_YACHT_SPAWN_ZONE(GET_YACHT_LOCATION_FOR_MENU_POSITION(iYachtMoveCurrentItem))
						SET_CURRENT_MENU_ITEM_DESCRIPTION("YACHT_FAIL_1", 4000) // There are no free spaces here.
						PLAY_SOUND_FRONTEND(-1, "ERROR", "HUD_FRONTEND_DEFAULT_SOUNDSET")
						CLEAR_BIT(iYachtMoveBitset, YACHT_MOVE_BITSET_FLAG_CONF_MOVE)
						TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_NEGATIVE()
						
					// A nearby player has a sell mission active
					// or impromptu deathmatch
					ELIF IS_BIT_SET(serverBD.iServerBitSet, ciSERVER_BS_SELL_MISSION_ACTIVE)
					OR IS_BIT_SET(iLocalBS, LOCAL_BS_ON_IMPROMPTU_DEATHMATCH)
					OR (IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID()) AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_BIKER_CONTRABAND_SELL)
					OR (IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID()) 
						AND (GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GUNRUNNING_BUY
						OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GUNRUNNING_DEFEND
						OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GUNRUNNING_SELL
						OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GUNRUNNING_WEAPONIZED_VEHICLE))
						SET_CURRENT_MENU_ITEM_DESCRIPTION("YACHT_FAIL_4", 4000) // There are no free spaces here.
						PLAY_SOUND_FRONTEND(-1, "ERROR", "HUD_FRONTEND_DEFAULT_SOUNDSET")
						CLEAR_BIT(iYachtMoveBitset, YACHT_MOVE_BITSET_FLAG_CONF_MOVE)
						TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_NEGATIVE()
					
					// Blocked by tunable
					ELIF IS_YACHT_LOCATION_MOVE_BLOCKED_BY_TUNABLE(GET_YACHT_LOCATION_FOR_MENU_POSITION(iYachtMoveCurrentItem))
						SET_CURRENT_MENU_ITEM_DESCRIPTION("YACHT_FAIL_1", 4000) // There are no free spaces here.
						PLAY_SOUND_FRONTEND(-1, "ERROR", "HUD_FRONTEND_DEFAULT_SOUNDSET")
						CLEAR_BIT(iYachtMoveBitset, YACHT_MOVE_BITSET_FLAG_CONF_MOVE)
						TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_NEGATIVE()
						
					// Can't afford
					ELIF (iYachtMoveTransactionState = YACHT_MOVE_TRANSACTION_INVALID AND NOT CAN_PLAYER_AFFORD_ITEM_COST(GET_YACHT_MOVE_COST(GET_ZONE_OF_LOCAL_PLAYER_YACHT(), GET_YACHT_LOCATION_FOR_MENU_POSITION(iYachtMoveCurrentItem))))
						SET_CURRENT_MENU_ITEM_DESCRIPTION("YACHT_FAIL_3", 4000) // You can't afford to move your Yacht here.
						STORE_LAST_VIEWED_SHOP_ITEM_FOR_COMMERCE_STORE(GET_HASH_KEY(GET_YACHT_LOCATION_NAME_FOR_MENU(GET_YACHT_LOCATION_FOR_MENU_POSITION(iYachtMoveCurrentItem))), GET_YACHT_MOVE_COST(GET_ZONE_OF_LOCAL_PLAYER_YACHT(), GET_YACHT_LOCATION_FOR_MENU_POSITION(iYachtMoveCurrentItem)))
						LAUNCH_STORE_CASH_ALERT(FALSE, DEFAULT, SPL_AMBIENT)
						PLAY_SOUND_FRONTEND(-1, "ERROR", "HUD_FRONTEND_DEFAULT_SOUNDSET")
						CLEAR_BIT(iYachtMoveBitset, YACHT_MOVE_BITSET_FLAG_CONF_MOVE)
						
						TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_AFFORD()

						
					// Confirm
					ELIF NOT IS_BIT_SET(iYachtMoveBitset, YACHT_MOVE_BITSET_FLAG_CONF_MOVE)
						SET_CURRENT_MENU_ITEM_DESCRIPTION("YACHT_MOVE_CONF")
						ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_YACHT_LOCATION_NAME_FOR_MENU(GET_YACHT_LOCATION_FOR_MENU_POSITION(iYachtMoveCurrentItem)))
						PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
						SET_BIT(iYachtMoveBitset, YACHT_MOVE_BITSET_FLAG_CONF_MOVE)
						TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_LOCATION_MOVE(GET_YACHT_LOCATION_FOR_MENU_POSITION(iYachtMoveCurrentItem))
						
						
						
					// Start transaction and warp
					ELIF NOT bDelayWarpAfterConfirm
						BOOL bStartWarp
						bStartWarp = FALSE
						
						INT iMoveCost 
						iMoveCost = GET_YACHT_MOVE_COST(GET_ZONE_OF_LOCAL_PLAYER_YACHT(), GET_YACHT_LOCATION_FOR_MENU_POSITION(iYachtMoveCurrentItem))
						
						PRINTLN("MAINTAIN_YACHT_MOVE - iMoveCost = ", iMoveCost)
						
						IF (iMoveCost = 0)
							PRINTLN("MAINTAIN_YACHT_MOVE - free move")
							bStartWarp = TRUE
							PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
							SET_CURRENT_MENU_ITEM_DESCRIPTION("YACHT_PASS_0", 4000) // Destination confirmed.
						ELSE
							IF USE_SERVER_TRANSACTIONS()
								IF iYachtMoveTransactionState = YACHT_MOVE_TRANSACTION_INVALID
									PRINTLN("MAINTAIN_YACHT_MOVE - Requesting cash transaction")
									IF NETWORK_REQUEST_CASH_TRANSACTION(iYachtMoveTransactionID, NET_SHOP_TTYPE_SERVICE, NET_SHOP_ACTION_SPEND, CATEGORY_SERVICE_WITH_THRESHOLD, SERVICE_SPEND_MOVE_YACHT, iMoveCost, CATALOG_ITEM_FLAG_BANK_THEN_WALLET)
										PRINTLN("MAINTAIN_YACHT_MOVE - Cash transaction started")
										iYachtMoveTransactionState = YACHT_MOVE_TRANSACTION_PENDING
										PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
									ELSE
										PRINTLN("MAINTAIN_YACHT_MOVE - Cash transaction failed")
										SET_CURRENT_MENU_ITEM_DESCRIPTION("YACHT_FAIL_2", 4000) // There was an error processing this transaction.
										PLAY_SOUND_FRONTEND(-1, "ERROR", "HUD_FRONTEND_DEFAULT_SOUNDSET")
										CLEAR_BIT(iYachtMoveBitset, YACHT_MOVE_BITSET_FLAG_CONF_MOVE)
									ENDIF
								ELSE
									PRINTLN("MAINTAIN_YACHT_MOVE - Cash transaction complete")
									NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED(GET_CASH_TRANSACTION_ID_FROM_INDEX(iYachtMoveTransactionID))
									NETWORK_SPENT_MOVE_YACHT(iMoveCost, FALSE, TRUE)
									DELETE_CASH_TRANSACTION(iYachtMoveTransactionID)
									iYachtMoveTransactionState = YACHT_MOVE_TRANSACTION_INVALID
									SET_CURRENT_MENU_ITEM_DESCRIPTION("YACHT_PASS_0", 4000) // Destination confirmed.
									bStartWarp = TRUE
								ENDIF
							ELSE
								NETWORK_SPENT_MOVE_YACHT(iMoveCost, FALSE, TRUE)
								bStartWarp = TRUE
								PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
								SET_CURRENT_MENU_ITEM_DESCRIPTION("YACHT_PASS_0", 4000) // Destination confirmed.
							ENDIF
						ENDIF
					
						IF bStartWarp
							IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
								TRIGGER_YACHT_CAPTAIN_SPEECH_GET_OUT_OF_HERE()
							ELSE
								TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_CAPTAIN_AFFIRM()
							ENDIF
							REINIT_NET_TIMER(stDelayedWarp)
							bDelayWarpAfterConfirm = TRUE
						ENDIF
					ENDIF
				
				// Exit menu
				ELIF (IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL))
				OR (bBack = TRUE AND bDelayWarpAfterConfirm = FALSE)
					IF IS_BIT_SET(iYachtMoveBitset, YACHT_MOVE_BITSET_FLAG_CONF_MOVE)
						CLEAR_BIT(iYachtMoveBitset, YACHT_MOVE_BITSET_FLAG_CONF_MOVE)
						SET_CURRENT_MENU_ITEM_DESCRIPTION("")
					ELSE
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						iYachtMoveControl = 0 // Wait for trigger
					ENDIF
					PLAY_SOUND_FRONTEND(-1, "BACK","HUD_FREEMODE_SOUNDSET")
					//TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_BYE()
					REINIT_NET_TIMER(cinematicDelay,TRUE)
				
				// Navigate menu
				ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_UP) AND NOT IS_BIT_SET(iYachtMoveBitset, YACHT_MOVE_BITSET_FLAG_CONF_MOVE) AND ALLOW_ANALOGUE_MOVEMENT(timeScrollDelay,iMoveUp)
				OR bPrevious AND NOT IS_BIT_SET(iYachtMoveBitset, YACHT_MOVE_BITSET_FLAG_CONF_MOVE) AND ALLOW_ANALOGUE_MOVEMENT(timeScrollDelay,iMoveUp)
					
					IF iMoveUp > 0
						iYachtMoveCurrentItem--
						IF iYachtMoveCurrentItem < 0
							iYachtMoveCurrentItem = (iMAX_PRIVATE_YACHT_SPAWN_ZONES - 2)
						ENDIF
						SET_CURRENT_MENU_ITEM(iYachtMoveCurrentItem)	
						PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					ENDIF	
					
				ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_DOWN) AND NOT IS_BIT_SET(iYachtMoveBitset, YACHT_MOVE_BITSET_FLAG_CONF_MOVE) AND ALLOW_ANALOGUE_MOVEMENT(timeScrollDelay,iMoveUp)
				OR bNext AND NOT IS_BIT_SET(iYachtMoveBitset, YACHT_MOVE_BITSET_FLAG_CONF_MOVE) AND ALLOW_ANALOGUE_MOVEMENT(timeScrollDelay,iMoveUp)
					
					IF iMoveUp < 0
						iYachtMoveCurrentItem++
						IF iYachtMoveCurrentItem > (iMAX_PRIVATE_YACHT_SPAWN_ZONES - 2)
							iYachtMoveCurrentItem = 0
						ENDIF
						SET_CURRENT_MENU_ITEM(iYachtMoveCurrentItem)
						PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					ENDIF
				ENDIF
				
				// Ensure blips are off.
				IF GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_PIM_HIDE_MISC) != -1
					SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_PIM_HIDE_MISC, -1)
				ENDIF
				g_bForceUpdateActivityBlips = TRUE
				SET_HIDE_ALL_JOB_BLIPS_FOR_YACHT_STATUS(TRUE)
				
				// Map zoom
				IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_LT)
					fYachtMapZoom += YACHT_MAP_ZOOM_SPEED
				ELIF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_RT)
					fYachtMapZoom -= YACHT_MAP_ZOOM_SPEED
				ENDIF
				IF fYachtMapZoom > YACHT_MAP_ZOOM_MAX
					fYachtMapZoom = YACHT_MAP_ZOOM_MAX
				ENDIF
				IF fYachtMapZoom < YACHT_MAP_ZOOM_MIN
					fYachtMapZoom = YACHT_MAP_ZOOM_MIN
				ENDIF
				
				IF NOT bMenuBuiltThisFrame
					DRAW_MENU()
					SET_MULTIPLAYER_BANK_CASH()
					SET_MULTIPLAYER_WALLET_CASH()
					SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
					THEFEED_HIDE()
					DISPLAY_RADAR(TRUE)
					SET_BIGMAP_ACTIVE(TRUE, FALSE)
					SET_RADAR_ZOOM_TO_DISTANCE(YACHT_MAP_ZOOM_DEFAULT+fYachtMapZoom)
					SET_RADAR_AS_EXTERIOR_THIS_FRAME()
					LOCK_MINIMAP_ANGLE(0)
					
					IF DOES_BLIP_EXIST(blipYachtMenu)
						REMOVE_BLIP(blipYachtMenu)
					ENDIF
					
					PRIVATE_YACHT_SPAWN_ZONE_ENUM eZone
					VECTOR vBlipCoords
					
					eZone = GET_YACHT_LOCATION_FOR_MENU_POSITION(iYachtMoveCurrentItem)
					IF eZone != PYSZ_NULL
					AND GET_ZONE_OF_LOCAL_PLAYER_YACHT() != GET_YACHT_LOCATION_FOR_MENU_POSITION(iYachtMoveCurrentItem)
						vBlipCoords = g_PrivateYachtZoneCoords[eZone]
					ELSE
						vBlipCoords = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iCurrentYacht, <<-0.1665, -38.7851, -5.5231>>)
					ENDIF
					
					LOCK_MINIMAP_POSITION(vBlipCoords.x, vBlipCoords.y)
					blipYachtMenu = ADD_BLIP_FOR_COORD(vBlipCoords)
					SET_BLIP_SPRITE(blipYachtMenu, RADAR_TRACE_YACHT)
					SET_BLIP_DISPLAY(blipYachtMenu, DISPLAY_MINIMAP_OR_BIGMAP)
					SET_BLIP_SCALE(blipYachtMenu, PRIVATE_YACHT_BLIP_SCALE*1.5)
					SET_BLIP_ALPHA(blipYachtMenu, PRIVATE_YACHT_BLIP_ALPHA)
					
					DISABLE_RADAR_MAP(TRUE)
					g_PlayerBlipsData.bBigMapIsActive = TRUE
				ENDIF
			ENDIF
			
			IF iYachtMoveControl != 1
				DISABLE_RADAR_MAP(FALSE)
				g_PlayerBlipsData.bBigMapIsActive = FALSE
				CLEANUP_MENU_ASSETS()
				SET_RADAR_ZOOM(0)
				UNLOCK_MINIMAP_ANGLE()
				UNLOCK_MINIMAP_POSITION()
				SET_BIGMAP_ACTIVE(FALSE)
				THEFEED_SHOW()
				REMOVE_MULTIPLAYER_BANK_CASH()
				REMOVE_MULTIPLAYER_WALLET_CASH()
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
				IF DOES_BLIP_EXIST(blipYachtMenu)
					REMOVE_BLIP(blipYachtMenu)
				ENDIF
				IF IS_BIT_SET(iYachtMoveBitset, YACHT_MOVE_BITSET_FLAG_BLOCK_MISSIONS)
					Block_All_MissionsAtCoords_Missions(FALSE)
					CLEAR_BIT(iYachtMoveBitset, YACHT_MOVE_BITSET_FLAG_BLOCK_MISSIONS)
				ENDIF
				SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_PIM_HIDE_MISC, iBlipHideStatValue)
				g_bForceUpdateActivityBlips = TRUE
				SET_HIDE_ALL_JOB_BLIPS_FOR_YACHT_STATUS(FALSE)
				CLEAR_BIT(iYachtMoveBitset, YACHT_MOVE_BITSET_FLAG_HIDE_BLIPS)
				UPDATE_ALL_SHOP_BLIPS()
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL PHONE_MISSION_LAUNCHING_APPS_ACTIVE()
	IF DOES_SCRIPT_EXIST("appMPJobListNEW")
	AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appMPJobListNEW")) > 0
		CDEBUG1LN(DEBUG_SAFEHOUSE, "CAN_PLAYER_DRIVE_OUT_OF_GARAGE- False appMPJobListNew")
		REINIT_NET_TIMER(pauseMenuDelay,TRUE)
		RETURN TRUE
	ENDIF
	IF DOES_SCRIPT_EXIST("appJIPMP")
	AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appJIPMP")) > 0
		CDEBUG1LN(DEBUG_SAFEHOUSE, "CAN_PLAYER_DRIVE_OUT_OF_GARAGE- False appJIPMP")
		REINIT_NET_TIMER(pauseMenuDelay,TRUE)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_YACHT_BRIDGE_ENTRY()

	IF iCurrentYacht = -1
		EXIT
	ENDIF
	
	IF g_iYachtBridgeEntryStage = YACHT_BRIDGE_FLAG_ENTRY_1			
	OR g_iYachtBridgeEntryStage = YACHT_BRIDGE_FLAG_ENTRY_2
	OR g_iYachtBridgeEntryStage = YACHT_BRIDGE_FLAG_EXIT_1
	OR g_iYachtBridgeEntryStage = YACHT_BRIDGE_FLAG_EXIT_2
		PRINTLN("bPlayingPropertyTransitionCutscene set TRUE by MAINTAIN_YACHT_BRIDGE_ENTRY")
		//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bPlayerEnteringYachtBridge = TRUE
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bPlayerEnteringYachtBridge)
	ENDIF
	
	MP_PROP_OFFSET_STRUCT sPropOffsetA, sPropOffsetB
	MODEL_NAMES eBridgeDoor
	IF GET_RAILING_OF_PRIVATE_YACHT(iCurrentYacht) = 0
		eBridgeDoor = INT_TO_ENUM(MODEL_NAMES,HASH("apa_mp_apa_yacht_door"))
	ELSE
		eBridgeDoor = INT_TO_ENUM(MODEL_NAMES,HASH("apa_mp_apa_yacht_door2"))
	ENDIF
	// Need to have some doors.
	INT iBridgeDoor
	VECTOR returnMin
	VECTOR returnMax
	REPEAT 2 iBridgeDoor
	
		IF NOT DOES_ENTITY_EXIST(objID_BridgeDoor[iBridgeDoor])
			REQUEST_MODEL(eBridgeDoor)
			IF HAS_MODEL_LOADED(eBridgeDoor)
				
				GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_BRIDGE_DOOR, sPropOffsetA)
				GET_MODEL_DIMENSIONS(eBridgeDoor, returnMin, returnMax)
				
				IF iBridgeDoor = 0
					objID_BridgeDoor[iBridgeDoor] = CREATE_OBJECT_NO_OFFSET(eBridgeDoor, sPropOffsetA.vLoc, FALSE, FALSE, TRUE)
					SET_ENTITY_ROTATION(objID_BridgeDoor[iBridgeDoor], sPropOffsetA.vRot)
				ELSE
					objID_BridgeDoor[iBridgeDoor] = CREATE_OBJECT_NO_OFFSET(eBridgeDoor, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objID_BridgeDoor[0], << (returnMax.x-returnMin.x)*2.0, 0.0, 0.0 >>), FALSE, FALSE, TRUE)
					SET_ENTITY_ROTATION(objID_BridgeDoor[iBridgeDoor], sPropOffsetA.vRot-<< 0.0, 0.0, 180.0 >>)
				ENDIF
				
				SET_IS_EXTERIOR_ONLY(objID_BridgeDoor[iBridgeDoor], TRUE)
				FREEZE_ENTITY_POSITION(objID_BridgeDoor[iBridgeDoor], TRUE)
				SET_ENTITY_PROOFS(objID_BridgeDoor[iBridgeDoor], TRUE, TRUE, TRUE, TRUE, TRUE, TRUE)
				SET_ENTITY_INVINCIBLE(objID_BridgeDoor[iBridgeDoor], TRUE)
				SET_ENTITY_DYNAMIC(objID_BridgeDoor[iBridgeDoor], FALSE)
				SET_OBJECT_TINT_INDEX(objID_BridgeDoor[iBridgeDoor],GET_TINT_OF_PRIVATE_YACHT(iCurrentYacht))
			ENDIF
		/*
		// Debug set position
		ELSE
			
			GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_BRIDGE_DOOR, sPropOffsetA)
			GET_MODEL_DIMENSIONS(eBridgeDoor, returnMin, returnMax)
			
			IF iBridgeDoor = 0
				SET_ENTITY_COORDS_NO_OFFSET(objID_BridgeDoor[iBridgeDoor], sPropOffsetA.vLoc)
				SET_ENTITY_ROTATION(objID_BridgeDoor[iBridgeDoor], sPropOffsetA.vRot)
			ELSE
				SET_ENTITY_COORDS_NO_OFFSET(objID_BridgeDoor[iBridgeDoor], GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objID_BridgeDoor[0], << (returnMax.x-returnMin.x)*2.0, 0.0, 0.0 >>))
				SET_ENTITY_ROTATION(objID_BridgeDoor[iBridgeDoor], sPropOffsetA.vRot-<<0,0,180.0>>)
			ENDIF
			*/
		ENDIF
	ENDREPEAT
	
	SWITCH g_iYachtBridgeEntryStage
		CASE YACHT_BRIDGE_FLAG_TRIGGER_LOCATE
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND NOT IS_PLAYER_DOING_A_YACHT_WARP(PLAYER_ID())
			AND GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID()) = iCurrentYacht
			AND NOT g_sMPTunables.byacht_disable_move
				// Entry
				IF GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) != HASH("YachtRm_Bridge")
					IF NOT (GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID()) AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GB_YACHT_ROBBERY)
					AND CAN_PLAYER_USE_PROPERTY(FALSE)
					AND NOT g_bPlayerInProcessOfExitingInterior
					AND NOT IS_PAUSE_MENU_ACTIVE_EX()
					AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
					AND NOT IS_CUSTOM_MENU_ON_SCREEN()
					AND NOT IS_BROWSER_OPEN()
					AND NOT IS_COMMERCE_STORE_OPEN()
					AND NOT IS_STORE_PENDING_NETWORK_SHUTDOWN_TO_OPEN()
					AND NOT HAS_NET_TIMER_STARTED(pauseMenuDelay)
					AND NOT IS_INTERACTION_MENU_OPEN()
					AND NOT PROPERTY_HAS_JUST_ACCEPTED_A_MISSION(#IF IS_DEBUG_BUILD db_bFullDebug #ENDIF)
					AND NOT PHONE_MISSION_LAUNCHING_APPS_ACTIVE()
					AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
					AND NOT IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_AcceptedInvite)
					AND NOT IS_TRIP_SKIP_IN_PROGRESS_GLOBAL()
					AND NOT MPGlobalsAmbience.R2Pdata.bLaunchRaceToPoint 
					AND NOT IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
					AND NOT IS_PLAYER_BLOCKED_FROM_PROPERTY()
					AND NOT SHOULD_FMMC_YACHT_BRIDGE_ENTRANCE_BE_BLOCKED()
					
						GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_BRIDGE_DOOR_ENTRY_2_A, sPropOffsetA)
						GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_BRIDGE_DOOR_ENTRY_2_B, sPropOffsetB)
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), sPropOffsetA.vLoc, sPropOffsetB.vLoc, 0.937500)
							g_iYachtBridgeEntryStage = YACHT_BRIDGE_FLAG_ENTRY_1
						ENDIF
						
						GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_BRIDGE_DOOR_ENTRY_1_A, sPropOffsetA)
						GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_BRIDGE_DOOR_ENTRY_1_B, sPropOffsetB)
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), sPropOffsetA.vLoc, sPropOffsetB.vLoc, 1.5)
							g_iYachtBridgeEntryStage = YACHT_BRIDGE_FLAG_ENTRY_1
						ENDIF
						
						INT iR, iG, iB, iA
						GET_HUD_COLOUR(HUD_COLOUR_BLUE,iR, iG, iB, iA)
						DRAW_MARKER_EX(MARKER_CYLINDER, sPropOffsetA.vLoc+((sPropOffsetB.vLoc-sPropOffsetA.vLoc)*0.5)+(<< 0.0, 0.0, -1.125>>), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<0.75, 0.75, 0.75>>, iR, iG, iB, 255,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,FALSE)
						
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("YACHT_MOVE_BLK")
							CLEAR_HELP(TRUE)
						ENDIF
						
						IF DOES_ENTITY_EXIST(pedID_Captain)
						AND NOT IS_ENTITY_DEAD(pedID_Captain)
							PRINTLN("YACHT BRIDGE ENTRY: STOP_CURRENT_PLAYING_SPEECH")
							STOP_CURRENT_PLAYING_SPEECH(pedID_Captain)
						ENDIF
					ELSE
					
						BOOL bTryingToEnter
						bTryingToEnter = FALSE
						
						GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_BRIDGE_DOOR_ENTRY_2_A, sPropOffsetA)
						GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_BRIDGE_DOOR_ENTRY_2_B, sPropOffsetB)
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), sPropOffsetA.vLoc, sPropOffsetB.vLoc, 0.937500)
							bTryingToEnter = TRUE
						ENDIF
						
						GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_BRIDGE_DOOR_ENTRY_1_A, sPropOffsetA)
						GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_BRIDGE_DOOR_ENTRY_1_B, sPropOffsetB)
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), sPropOffsetA.vLoc, sPropOffsetB.vLoc, 1.5)
							bTryingToEnter = TRUE
						ENDIF
						
						IF bTryingToEnter
						AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
						AND IS_GAMEPLAY_CAM_RENDERING()
							IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
							OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("YACHT_MOVE_BLK")
								PRINT_HELP("YACHT_MOVE_BLK")
							ENDIF
						ELSE
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("YACHT_MOVE_BLK")
								CLEAR_HELP(TRUE)
							ENDIF
						ENDIF
					ENDIF
				
				// Exit
				ELSE
					GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_BRIDGE_DOOR_EXIT_A, sPropOffsetA)
					GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_BRIDGE_DOOR_EXIT_B, sPropOffsetB)
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), sPropOffsetA.vLoc, sPropOffsetB.vLoc, 1.062500)
						g_iYachtBridgeEntryStage = YACHT_BRIDGE_FLAG_EXIT_1
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE YACHT_BRIDGE_FLAG_ENTRY_1
			REQUEST_ANIM_DICT("anim@apt_trans@hinge_l")
			IF HAS_ANIM_DICT_LOADED("anim@apt_trans@hinge_l")
			
				// start mp cutscene
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				NETWORK_FADE_OUT_ENTITY(PLAYER_PED_ID(), TRUE, TRUE)
				SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(FALSE, TRUE)
				START_MP_CUTSCENE(TRUE)
				
				// clone player, task
				IF NOT IS_PLAYER_FEMALE()
					pedID_BridgeClone = CREATE_PED(PEDTYPE_CIVMALE, GET_ENTITY_MODEL(PLAYER_PED_ID()), GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_HEADING(PLAYER_PED_ID()), FALSE, FALSE)
				ELSE
					pedID_BridgeClone = CREATE_PED(PEDTYPE_CIVFEMALE, GET_ENTITY_MODEL(PLAYER_PED_ID()), GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_HEADING(PLAYER_PED_ID()), FALSE, FALSE)
				ENDIF
				CLONE_PED_TO_TARGET(PLAYER_PED_ID(), pedID_BridgeClone)
				FREEZE_ENTITY_POSITION(pedID_BridgeClone, TRUE)
				SET_ENTITY_PROOFS(pedID_BridgeClone, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE)
				SET_ENTITY_VISIBLE(pedID_BridgeClone, TRUE)
				SET_FORCE_FOOTSTEP_UPDATE(pedID_BridgeClone, TRUE)
				
				SET_ENTITY_CAN_BE_DAMAGED(pedID_BridgeClone, FALSE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedID_BridgeClone, TRUE)
				SET_PED_CONFIG_FLAG(pedID_BridgeClone, PCF_UseKinematicModeWhenStationary,TRUE)
				SET_PED_CONFIG_FLAG(pedID_BridgeClone, PCF_DisableExplosionReactions,TRUE)
				SET_PED_CONFIG_FLAG(pedID_BridgeClone, PCF_DontActivateRagdollFromExplosions,TRUE)
				SET_PED_CAN_EVASIVE_DIVE(pedID_BridgeClone, FALSE)
				SET_TREAT_AS_AMBIENT_PED_FOR_DRIVER_LOCKON(pedID_BridgeClone, TRUE)
				SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedID_BridgeClone, FALSE)
				SET_PED_CAN_BE_TARGETTED(pedID_BridgeClone, FALSE)
				SET_PED_RESET_FLAG(pedID_BridgeClone, PRF_DisablePotentialBlastReactions, TRUE)
				
				GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_BRIDGE_ENTRY_ANIM_POS, sPropOffsetA)
				
				iYachtBridgeEntrySceneID = CREATE_SYNCHRONIZED_SCENE(sPropOffsetA.vLoc, sPropOffsetA.vRot)
				TASK_SYNCHRONIZED_SCENE(pedID_BridgeClone, iYachtBridgeEntrySceneID, "anim@apt_trans@hinge_l", "ext_player", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
				// start the door task later
				
				// create cam, activate
				GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_BRIDGE_ENTRY_CAM, sPropOffsetA)
				camID_BridgeEntry = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", sPropOffsetA.vLoc, sPropOffsetA.vRot, 24.915972)//33.7)
				SET_CAM_ACTIVE(camID_BridgeEntry, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				// set up interior
				GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_CAPTAIN, sPropOffsetA)
				
				IF iCurrentYacht = 36
					interiorID_BridgeRoom = GET_INTERIOR_AT_COORDS_WITH_TYPE(sPropOffsetA.vLoc, "ac_mpapa_yacht")
				ELIF iCurrentYacht = 37
					interiorID_BridgeRoom = GET_INTERIOR_AT_COORDS_WITH_TYPE(sPropOffsetA.vLoc, "h4_islandx_yacht_01_int")
				ELIF iCurrentYacht = 38
					interiorID_BridgeRoom = GET_INTERIOR_AT_COORDS_WITH_TYPE(sPropOffsetA.vLoc, "h4_islandx_yacht_02_int")
				ELIF iCurrentYacht = 39
					interiorID_BridgeRoom = GET_INTERIOR_AT_COORDS_WITH_TYPE(sPropOffsetA.vLoc, "h4_islandx_yacht_03_int")
				#IF FEATURE_FIXER
				ELIF iCurrentYacht = 40
					interiorID_BridgeRoom = GET_INTERIOR_AT_COORDS_WITH_TYPE(sPropOffsetA.vLoc, "sf_yacht_01_int")
				ELIF iCurrentYacht = 41
					interiorID_BridgeRoom = GET_INTERIOR_AT_COORDS_WITH_TYPE(sPropOffsetA.vLoc, "sf_yacht_02_int")
				#ENDIF
				ELSE
					interiorID_BridgeRoom = GET_INTERIOR_AT_COORDS_WITH_TYPE(sPropOffsetA.vLoc, "apa_mpapa_yacht")
				ENDIF
				
				IF interiorID_BridgeRoom != NULL
					PIN_INTERIOR_IN_MEMORY(interiorID_BridgeRoom)
				ENDIF
				GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_BRIDGE_ENTRY_POS, sPropOffsetA)
				IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
					NEW_LOAD_SCENE_START_SPHERE(sPropOffsetA.vLoc, 20.0, NEWLOADSCENE_FLAG_INTERIOR_AND_EXTERIOR)
				ENDIF
				
				td_BridgeEntry = GET_NETWORK_TIME()
				g_iYachtBridgeEntryStage = YACHT_BRIDGE_FLAG_ENTRY_2
			ENDIF
		BREAK
		CASE YACHT_BRIDGE_FLAG_ENTRY_2
		
			IF DOES_ENTITY_EXIST(pedID_BridgeClone)
			AND NOT IS_PED_INJURED(pedID_BridgeClone)
				SET_PED_RESET_FLAG(pedID_BridgeClone, PRF_DisablePotentialBlastReactions, TRUE)
			ENDIF
		
			// start the door task
			IF DOES_ENTITY_EXIST(objID_BridgeDoor[0])
				IF NOT IS_ENTITY_PLAYING_ANIM(objID_BridgeDoor[0], "anim@apt_trans@hinge_l", "ext_door")
				AND (GET_SYNCHRONIZED_SCENE_PHASE(iYachtBridgeEntrySceneID) >= 0.271)
					PLAY_ENTITY_ANIM(objID_BridgeDoor[0], "ext_door", "anim@apt_trans@hinge_l", INSTANT_BLEND_IN, FALSE, FALSE, DEFAULT, 0.26, ENUM_TO_INT(AF_USE_MOVER_EXTRACTION))
					PLAY_SOUND_FRONTEND(-1, "PUSH", "DLC_APT_YACHT_DOOR_SOUNDS")
				ENDIF
			ENDIF
			
			IF (GET_SYNCHRONIZED_SCENE_PHASE(iYachtBridgeEntrySceneID) >= 0.8 OR ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), td_BridgeEntry)) > 8000)
			AND IS_INTERIOR_READY(interiorID_BridgeRoom)
			AND (NOT IS_NEW_LOAD_SCENE_ACTIVE() OR IS_NEW_LOAD_SCENE_LOADED())
				// set player pos
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
				GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_BRIDGE_ENTRY_POS, sPropOffsetA)
				SET_ENTITY_COORDS(PLAYER_PED_ID(), sPropOffsetA.vLoc)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), sPropOffsetA.vRot.z)
				SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
				SET_PED_CAN_RAGDOLL(PLAYER_PED_ID(), FALSE)
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableExplosionReactions,TRUE)
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontActivateRagdollFromExplosions,TRUE)
				RETAIN_ENTITY_IN_INTERIOR(PLAYER_PED_ID(), interiorID_BridgeRoom)
				
				// cleanup clone
				IF DOES_ENTITY_EXIST(pedID_BridgeClone)
					DELETE_PED(pedID_BridgeClone)
				ENDIF
				REMOVE_ANIM_DICT("anim@apt_trans@hinge_l")
				
				// cleanup cameras
				IF DOES_CAM_EXIST(camID_BridgeEntry)
					DESTROY_CAM(camID_BridgeEntry)
				ENDIF
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
				
				// set up interior
				SET_ROOM_FOR_GAME_VIEWPORT_BY_KEY(HASH("YachtRm_Bridge"))
				IF interiorID_BridgeRoom != NULL
					UNPIN_INTERIOR(interiorID_BridgeRoom)
					interiorID_BridgeRoom = NULL
				ENDIF
				IF IS_NEW_LOAD_SCENE_ACTIVE()
					NEW_LOAD_SCENE_STOP()
				ENDIF
				
				// reset door
				IF DOES_ENTITY_EXIST(objID_BridgeDoor[0])
					STOP_ENTITY_ANIM(objID_BridgeDoor[0], "ext_door", "anim@apt_trans@hinge_l", INSTANT_BLEND_OUT)
					GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_BRIDGE_DOOR, sPropOffsetA)
					SET_ENTITY_COORDS_NO_OFFSET(objID_BridgeDoor[0], sPropOffsetA.vLoc)
					SET_ENTITY_ROTATION(objID_BridgeDoor[0], sPropOffsetA.vRot)
				ENDIF
				
				// end network cutscene
				CLEANUP_MP_CUTSCENE(TRUE, TRUE)
				
				IF HAS_ANIM_DICT_LOADED("ANIM@AMB@YACHT@CAPTAIN@")
					TASK_PLAY_ANIM(pedID_Captain, "ANIM@AMB@YACHT@CAPTAIN@", "idle", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING)
				ENDIF
				
				TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_ENTRY(TRUE)
				bCaptainYachtMissionMapSpeechTriggered = FALSE
				bCaptainYachtMissionSpeechTriggered = FALSE
				RESET_NET_TIMER(stYachtCaptainIdleSpeech)
				
				//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bPlayerEnteringYachtBridge = FALSE
				CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bPlayerEnteringYachtBridge)
				g_iYachtBridgeEntryStage = YACHT_BRIDGE_FLAG_TRIGGER_LOCATE
			ENDIF
		BREAK
		
		CASE YACHT_BRIDGE_FLAG_EXIT_1
			REQUEST_ANIM_DICT("anim@apt_trans@hinge_r")
			IF HAS_ANIM_DICT_LOADED("anim@apt_trans@hinge_r")
			
				// start mp cutscene
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(FALSE, TRUE)
				START_MP_CUTSCENE(TRUE)
				
				// clone player, task
				IF NOT IS_PLAYER_FEMALE()
					pedID_BridgeClone = CREATE_PED(PEDTYPE_CIVMALE, GET_ENTITY_MODEL(PLAYER_PED_ID()), GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_HEADING(PLAYER_PED_ID()), FALSE, FALSE)
				ELSE
					pedID_BridgeClone = CREATE_PED(PEDTYPE_CIVFEMALE, GET_ENTITY_MODEL(PLAYER_PED_ID()), GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_HEADING(PLAYER_PED_ID()), FALSE, FALSE)
				ENDIF
				CLONE_PED_TO_TARGET(PLAYER_PED_ID(), pedID_BridgeClone)
				FREEZE_ENTITY_POSITION(pedID_BridgeClone, TRUE)
				SET_ENTITY_PROOFS(pedID_BridgeClone, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE)
				SET_ENTITY_VISIBLE(pedID_BridgeClone, TRUE)
				SET_FORCE_FOOTSTEP_UPDATE(pedID_BridgeClone, TRUE)
				
				SET_ENTITY_CAN_BE_DAMAGED(pedID_BridgeClone, FALSE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedID_BridgeClone, TRUE)
				SET_PED_CONFIG_FLAG(pedID_BridgeClone, PCF_UseKinematicModeWhenStationary,TRUE)
				SET_PED_CONFIG_FLAG(pedID_BridgeClone, PCF_DisableExplosionReactions,TRUE)
				SET_PED_CONFIG_FLAG(pedID_BridgeClone, PCF_DontActivateRagdollFromExplosions,TRUE)
				SET_PED_CAN_EVASIVE_DIVE(pedID_BridgeClone, FALSE)
				SET_TREAT_AS_AMBIENT_PED_FOR_DRIVER_LOCKON(pedID_BridgeClone, TRUE)
				SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedID_BridgeClone, FALSE)
				SET_PED_CAN_BE_TARGETTED(pedID_BridgeClone, FALSE)
				SET_PED_RESET_FLAG(pedID_BridgeClone, PRF_DisablePotentialBlastReactions, TRUE)
				
				CLEAR_ROOM_FOR_ENTITY(pedID_BridgeClone)
				
				GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_BRIDGE_EXIT_ANIM_POS, sPropOffsetA)
				
				iYachtBridgeEntrySceneID = CREATE_SYNCHRONIZED_SCENE(sPropOffsetA.vLoc, sPropOffsetA.vRot)
				TASK_SYNCHRONIZED_SCENE(pedID_BridgeClone, iYachtBridgeEntrySceneID, "anim@apt_trans@hinge_r", "ext_player", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
				// start the door task later
				
				// create cam, activate
				GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_BRIDGE_EXIT_CAM, sPropOffsetA)
				camID_BridgeEntry = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", sPropOffsetA.vLoc, sPropOffsetA.vRot, 24.915972)//33.7)
				SET_CAM_ACTIVE(camID_BridgeEntry, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				CLEAR_ROOM_FOR_GAME_VIEWPORT()
				
				td_BridgeEntry = GET_NETWORK_TIME()
				g_iYachtBridgeEntryStage = YACHT_BRIDGE_FLAG_EXIT_2
			ENDIF
		BREAK
		CASE YACHT_BRIDGE_FLAG_EXIT_2
		
			IF DOES_ENTITY_EXIST(pedID_BridgeClone)
			AND NOT IS_PED_INJURED(pedID_BridgeClone)
				CLEAR_ROOM_FOR_ENTITY(pedID_BridgeClone)
				SET_PED_RESET_FLAG(pedID_BridgeClone, PRF_DisablePotentialBlastReactions, TRUE)
			ENDIF
			
			// start the door task
			IF DOES_ENTITY_EXIST(objID_BridgeDoor[0])
				IF NOT IS_ENTITY_PLAYING_ANIM(objID_BridgeDoor[0], "anim@apt_trans@hinge_r", "ext_door")
				AND (GET_SYNCHRONIZED_SCENE_PHASE(iYachtBridgeEntrySceneID) >= 0.26)
					PLAY_ENTITY_ANIM(objID_BridgeDoor[0], "ext_door", "anim@apt_trans@hinge_r", INSTANT_BLEND_IN, FALSE, FALSE, DEFAULT, 0.26, ENUM_TO_INT(AF_USE_MOVER_EXTRACTION))
					PLAY_SOUND_FRONTEND(-1, "PUSH", "DLC_APT_YACHT_DOOR_SOUNDS")
					CLEAR_BIT(iYachtBridgeRoomFlags, 1)
				ENDIF
				
				IF NOT IS_BIT_SET(iYachtBridgeRoomFlags, 1)
					IF (GET_SYNCHRONIZED_SCENE_PHASE(iYachtBridgeEntrySceneID) >= 0.369)
						PLAY_SOUND_FRONTEND(-1, "LIMIT", "DLC_APT_YACHT_DOOR_SOUNDS")
						SET_BIT(iYachtBridgeRoomFlags, 1)
					ENDIF
				ENDIF
			ENDIF
			
			IF (GET_SYNCHRONIZED_SCENE_PHASE(iYachtBridgeEntrySceneID) >= 0.6 OR ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), td_BridgeEntry)) > 15000)
				
				// end network cutscene
				CLEANUP_MP_CUTSCENE(TRUE, TRUE)
				
				// set player pos
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
				GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_BRIDGE_EXIT_POS, sPropOffsetA)
				SET_ENTITY_COORDS(PLAYER_PED_ID(), sPropOffsetA.vLoc)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), sPropOffsetA.vRot.z)
				FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
				SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 1000)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
				SET_PED_CAN_RAGDOLL(PLAYER_PED_ID(), TRUE)
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableExplosionReactions,FALSE)
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontActivateRagdollFromExplosions,FALSE)
				
				// cleanup clone
				IF DOES_ENTITY_EXIST(pedID_BridgeClone)
					DELETE_PED(pedID_BridgeClone)
				ENDIF
				REMOVE_ANIM_DICT("anim@apt_trans@hinge_r")
				
				// cleanup cameras
				IF DOES_CAM_EXIST(camID_BridgeEntry)
					DESTROY_CAM(camID_BridgeEntry)
				ENDIF
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
				
				IF entryCutData.iSoundID < 0
					entryCutData.iSoundID = GET_SOUND_ID()
				ENDIF
				PLAY_SOUND_FRONTEND(-1, "CLOSED", "DLC_APT_YACHT_DOOR_SOUNDS")
				
				// set up interior
				IF interiorID_BridgeRoom != NULL
					UNPIN_INTERIOR(interiorID_BridgeRoom)
					interiorID_BridgeRoom = NULL
				ENDIF
				
				// reset door
				IF DOES_ENTITY_EXIST(objID_BridgeDoor[0])
					STOP_ENTITY_ANIM(objID_BridgeDoor[0], "ext_door", "anim@apt_trans@hinge_r", INSTANT_BLEND_OUT)
					GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_BRIDGE_DOOR, sPropOffsetA)
					SET_ENTITY_COORDS_NO_OFFSET(objID_BridgeDoor[0], sPropOffsetA.vLoc)
					SET_ENTITY_ROTATION(objID_BridgeDoor[0], sPropOffsetA.vRot)
				ENDIF
				
				TRIGGER_YACHT_CAPTAIN_SPEECH_PLAYER_ENTRY(TRUE)
				bCaptainYachtMissionMapSpeechTriggered = FALSE
				bCaptainYachtMissionSpeechTriggered = FALSE
				RESET_NET_TIMER(stYachtCaptainIdleSpeech)
				
				td_BridgeEntry = GET_NETWORK_TIME()
				//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bPlayerEnteringYachtBridge = FALSE
				CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bPlayerEnteringYachtBridge)
				g_iYachtBridgeEntryStage = YACHT_BRIDGE_FLAG_TRIGGER_LOCATE
			ENDIF
		BREAK
		CASE YACHT_BRIDGE_FLAG_BAIL
		
			PRINTLN("MAINTAIN_YACHT_BRIDGE_ENTRY - Bail!")
			
			IF DOES_ENTITY_EXIST(pedID_BridgeClone)
				DELETE_PED(pedID_BridgeClone)
			ENDIF
			
			IF DOES_CAM_EXIST(camID_BridgeEntry)
				DESTROY_CAM(camID_BridgeEntry)
			ENDIF
			
			IF DOES_ENTITY_EXIST(objID_BridgeDoor[0])
				DELETE_OBJECT(objID_BridgeDoor[0])
			ENDIF
			IF DOES_ENTITY_EXIST(objID_BridgeDoor[1])
				DELETE_OBJECT(objID_BridgeDoor[1])
			ENDIF
			
			IF IS_NEW_LOAD_SCENE_ACTIVE()
				NEW_LOAD_SCENE_STOP()
			ENDIF
			
			REMOVE_ANIM_DICT("anim@apt_trans@hinge_l")
			REMOVE_ANIM_DICT("anim@apt_trans@hinge_r")
			
			//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bPlayerEnteringYachtBridge = FALSE
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bPlayerEnteringYachtBridge)
		BREAK
		
		g_iYachtBridgeEntryStage = YACHT_BRIDGE_FLAG_TRIGGER_LOCATE
	ENDSWITCH
	
	// Update broadcast data for player
	playerBD[NATIVE_TO_INT(PLAYER_ID())].iBridgeEntryStage = g_iYachtBridgeEntryStage
	
	// Manage network fade for other platyers
	INT iPlayer
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(iPlayer))
		AND INT_TO_PLAYERINDEX(iPlayer) != PLAYER_ID()
			IF IS_BIT_SET(iYachtBridgeRoomEntry, iPlayer)
				IF playerBD[iPlayer].iBridgeEntryStage = YACHT_BRIDGE_FLAG_TRIGGER_LOCATE
					PRINTLN("MAINTAIN_YACHT_BRIDGE_ENTRY() - Fading in player ", iPlayer)
					NETWORK_FADE_IN_ENTITY(GET_PLAYER_PED(INT_TO_PLAYERINDEX(iPlayer)), FALSE)
					CLEAR_BIT(iYachtBridgeRoomEntry, iPlayer)
				ENDIF
			ELSE
				IF playerBD[iPlayer].iBridgeEntryStage != YACHT_BRIDGE_FLAG_TRIGGER_LOCATE
					SET_BIT(iYachtBridgeRoomEntry, iPlayer)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Manage inside yacht stat
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) = HASH("YachtRm_Bridge")
			SET_BIT(iYachtBridgeRoomFlags, 0)
			NETWORK_SET_LOCAL_PLAYER_INVINCIBLE_TIME(1000)
			
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_UNARMED, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_MELEE, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_HANDGUN, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SHOTGUN, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SMG, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_AUTO_RIFLE, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SNIPER, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_HEAVY, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SPECIAL, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_NEXT_WEAPON, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_PREV_WEAPON, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PICKUP, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_ATTACK, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_NEXT_WEAPON, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, TRUE)
			
			WEAPON_TYPE eCurrentWeapon
			IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), eCurrentWeapon)
				IF eCurrentWeapon != WEAPONTYPE_OBJECT
					PRINTLN("Holstering weapon ", eCurrentWeapon)
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED,TRUE)
					
					IF (GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), FALSE, FALSE)
					ENDIF
				ENDIF
			ENDIF
			
		ELIF IS_BIT_SET(iYachtBridgeRoomFlags, 0)
			CLEAR_BIT(iYachtBridgeRoomFlags, 0)
			NETWORK_SET_LOCAL_PLAYER_INVINCIBLE_TIME(0)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Returns the render target for yacht models 
FUNC STRING GET_YACHT_RENDER_TARGET(MODEL_NAMES mModel)
	SWITCH mModel
		CASE APA_PROP_AP_PORT_TEXT   RETURN "port_text"   BREAK
		CASE APA_PROP_AP_STARB_TEXT  RETURN "starb_text"  BREAK
		CASE APA_PROP_AP_STERN_TEXT  RETURN "stern_text"  BREAK
//		APA_PROP_AP_NAME_TEXT
	ENDSWITCH
	RETURN ""
ENDFUNC

/// PURPOSE: Registers and links a render target to a specified model
PROC REGISTER_AND_LINK_RENDER_TARGET(INT &iRenderTargetID, STRING sRenderTarget, MODEL_NAMES mModel)
	IF NOT IS_NAMED_RENDERTARGET_REGISTERED(sRenderTarget)
		REGISTER_NAMED_RENDERTARGET(sRenderTarget)
		IF NOT IS_NAMED_RENDERTARGET_LINKED(mModel)
			LINK_NAMED_RENDERTARGET(mModel)
			IF iRenderTargetID = -1
				iRenderTargetID = GET_NAMED_RENDERTARGET_RENDER_ID(sRenderTarget)
			ELSE
				// .. Already have render target ID
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Display chosen name on yacht
PROC MAINTAIN_YACHT_NAME()
	
	MP_PROP_OFFSET_STRUCT sPropOffset
	IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), mpYachts[iCurrentYacht].vBaseLocation) <= 150
		//IF GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID()) = iCurrentYacht
			SWITCH iYachtNameControl
				CASE 0
					// Prop creation & render targets
					IF CAN_REGISTER_MISSION_OBJECTS(3)
						IF NOT DOES_ENTITY_EXIST(oYachtNamePort)
							REQUEST_MODEL(APA_PROP_AP_PORT_TEXT)
							IF HAS_MODEL_LOADED(APA_PROP_AP_PORT_TEXT)
								GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_NAME_PORT, sPropOffset)
								oYachtNamePort = CREATE_OBJECT_NO_OFFSET(APA_PROP_AP_PORT_TEXT, sPropOffset.vLoc, FALSE, FALSE, TRUE)
								SET_ENTITY_ROTATION(oYachtNamePort, sPropOffset.vRot)
								REGISTER_AND_LINK_RENDER_TARGET(iPortRenderID, GET_YACHT_RENDER_TARGET(APA_PROP_AP_PORT_TEXT), APA_PROP_AP_PORT_TEXT)
								SET_MODEL_AS_NO_LONGER_NEEDED(APA_PROP_AP_PORT_TEXT)
							ENDIF
						ENDIF
						IF NOT DOES_ENTITY_EXIST(oYachtNameStarb)
							REQUEST_MODEL(APA_PROP_AP_STARB_TEXT)
							IF HAS_MODEL_LOADED(APA_PROP_AP_STARB_TEXT)
								GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_NAME_STARB, sPropOffset)
								oYachtNameStarb = CREATE_OBJECT_NO_OFFSET(APA_PROP_AP_STARB_TEXT, sPropOffset.vLoc, FALSE, FALSE, TRUE)
								SET_ENTITY_ROTATION(oYachtNameStarb, sPropOffset.vRot)
								REGISTER_AND_LINK_RENDER_TARGET(iStarbRenderID, GET_YACHT_RENDER_TARGET(APA_PROP_AP_STARB_TEXT), APA_PROP_AP_STARB_TEXT)
								SET_MODEL_AS_NO_LONGER_NEEDED(APA_PROP_AP_STARB_TEXT)
							ENDIF
						ENDIF
						IF NOT DOES_ENTITY_EXIST(oYachtNameStern)
							REQUEST_MODEL(APA_PROP_AP_STERN_TEXT)
							IF HAS_MODEL_LOADED(APA_PROP_AP_STERN_TEXT)
								GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_NAME_STERN, sPropOffset)
								oYachtNameStern = CREATE_OBJECT_NO_OFFSET(APA_PROP_AP_STERN_TEXT, sPropOffset.vLoc, FALSE, FALSE, TRUE)
								SET_ENTITY_ROTATION(oYachtNameStern, sPropOffset.vRot)
								REGISTER_AND_LINK_RENDER_TARGET(iSternRenderID, GET_YACHT_RENDER_TARGET(APA_PROP_AP_STERN_TEXT), APA_PROP_AP_STERN_TEXT)
								SET_MODEL_AS_NO_LONGER_NEEDED(APA_PROP_AP_STERN_TEXT)
							ENDIF
						ENDIF
						IF DOES_ENTITY_EXIST(oYachtNamePort)
						AND DOES_ENTITY_EXIST(oYachtNameStarb)
						AND DOES_ENTITY_EXIST(oYachtNameStern)
							iYachtNameControl = 1
						ENDIF
					ENDIF
				BREAK
				CASE 1
					// White Text?
					iTintID = GET_TINT_OF_PRIVATE_YACHT(iCurrentYacht)
					bYachtWhiteText = FALSE
					
					IF GET_TINT_OF_PRIVATE_YACHT(iCurrentYacht) = 1
					OR GET_TINT_OF_PRIVATE_YACHT(iCurrentYacht) = 4
					OR GET_TINT_OF_PRIVATE_YACHT(iCurrentYacht) = 5
					OR GET_TINT_OF_PRIVATE_YACHT(iCurrentYacht) = 6
					OR GET_TINT_OF_PRIVATE_YACHT(iCurrentYacht) = 9
					OR GET_TINT_OF_PRIVATE_YACHT(iCurrentYacht) = 11
					OR GET_TINT_OF_PRIVATE_YACHT(iCurrentYacht) = 15
						bYachtWhiteText = TRUE
					ENDIF
					
					// Port & Starb Scaleform
					IF HAS_SCALEFORM_MOVIE_LOADED(sYachtNameOverlay)
						BEGIN_SCALEFORM_MOVIE_METHOD(sYachtNameOverlay, "SET_YACHT_NAME")
							#IF IS_DEBUG_BUILD
							IF bCustomYachtName
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(GET_CONTENTS_OF_TEXT_WIDGET(wYachtName))
								strYachtName = GET_CONTENTS_OF_TEXT_WIDGET(wYachtName)
							ELSE
							#ENDIF
								// Dont show yacht name for restricted accounts
								IF GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iCurrentYacht) != INVALID_PLAYER_INDEX()
									
									PRINTLN("[AM_MP_YACHT] MAINTAIN_YACHT_NAME is yacht owner restricted: "
										, GET_STRING_FROM_BOOL(GB_IS_GLOBAL_CLIENT_BIT0_SET(GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iCurrentYacht), eGB_GLOBAL_CLIENT_BITSET_0_RESTRICTED_ACCOUNT)))
									
									PRINTLN("[AM_MP_YACHT] MAINTAIN_YACHT_NAME is local player restricted: "
										, GET_STRING_FROM_BOOL(GB_IS_GLOBAL_CLIENT_BIT0_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_0_RESTRICTED_ACCOUNT)))
									
									IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iCurrentYacht))) AND GB_IS_THIS_PLAYERS_GANG_NAME_BLOCKED(GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iCurrentYacht))
										PRINTLN("[AM_MP_YACHT] MAINTAIN_YACHT_NAME yacht owner gang name is blocked")
									ELSE
										PRINTLN("[AM_MP_YACHT] MAINTAIN_YACHT_NAME yacht owner gang name is NOT blocked")
									ENDIF
									
									PRINTLN("[AM_MP_YACHT] MAINTAIN_YACHT_NAME bUserContentIsBlocked: ", GET_STRING_FROM_BOOL(bUserContentIsBlocked))
									
									IF IS_DURANGO_AND_CAN_VIEW_GAMER_USER_CONTENT(GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iCurrentYacht))
										PRINTLN("[AM_MP_YACHT] MAINTAIN_YACHT_NAME IS_DURANGO_AND_CAN_VIEW_GAMER_USER_CONTENT TRUE")
									ELSE
										PRINTLN("[AM_MP_YACHT] MAINTAIN_YACHT_NAME IS_DURANGO_AND_CAN_VIEW_GAMER_USER_CONTENT FALSE")
									ENDIF
									
									IF GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iCurrentYacht) = PLAYER_ID()
										sTempName = GET_NAME_OF_PRIVATE_YACHT(iCurrentYacht)
										SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(sTempName)
										iHashYachtName = GET_HASH_KEY(sTempName)
										
									ELIF (GB_IS_GLOBAL_CLIENT_BIT0_SET(GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iCurrentYacht), eGB_GLOBAL_CLIENT_BITSET_0_RESTRICTED_ACCOUNT)
									OR GB_IS_GLOBAL_CLIENT_BIT0_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_0_RESTRICTED_ACCOUNT)
									OR (DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iCurrentYacht))) AND GB_IS_THIS_PLAYERS_GANG_NAME_BLOCKED(GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iCurrentYacht)))
									OR bUserContentIsBlocked = TRUE)
									AND NOT IS_DURANGO_AND_CAN_VIEW_GAMER_USER_CONTENT(GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iCurrentYacht))
										IF NOT (bUserContentIsBlocked)
											bUserContentIsBlocked = TRUE
											CPRINTLN(DEBUG_YACHT, "setting bUserContentIsBlocked = TRUE (1) ")
										ENDIF
										SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(GET_FILENAME_FOR_AUDIO_CONVERSATION("YACHT_GSY"))
										iHashYachtName = GET_HASH_KEY(GET_FILENAME_FOR_AUDIO_CONVERSATION("YACHT_GSY"))
									ELSE
										sTempName = GET_NAME_OF_PRIVATE_YACHT(iCurrentYacht)
										SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(sTempName)
										iHashYachtName = GET_HASH_KEY(sTempName)
									ENDIF
								ENDIF
							#IF IS_DEBUG_BUILD
							ENDIF
							#ENDIF
							// isWhiteText param
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bYachtWhiteText)
							// Country
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
						END_SCALEFORM_MOVIE_METHOD()
						SET_SCALEFORM_MOVIE_TO_USE_LARGE_RT(sYachtNameOverlay, TRUE)
						iYachtNameControl = 2
					ELSE
						sYachtNameOverlay = REQUEST_SCALEFORM_MOVIE("YACHT_NAME")
					ENDIF
				BREAK
				CASE 2
					// Stern Scaleform
					IF HAS_SCALEFORM_MOVIE_LOADED(sYachtNameSternOverlay)
						BEGIN_SCALEFORM_MOVIE_METHOD(sYachtNameSternOverlay, "SET_YACHT_NAME")
							#IF IS_DEBUG_BUILD
							IF bCustomYachtName
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(GET_CONTENTS_OF_TEXT_WIDGET(wYachtName))
								strYachtName = GET_CONTENTS_OF_TEXT_WIDGET(wYachtName)
							ELSE
							#ENDIF
								// Dont show yacht name for restricted accounts
								IF GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iCurrentYacht) != INVALID_PLAYER_INDEX()
									IF GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iCurrentYacht) = PLAYER_ID()
										sTempName = GET_NAME_OF_PRIVATE_YACHT(iCurrentYacht)
										SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(sTempName)
										iHashYachtName = GET_HASH_KEY(sTempName)
										
									ELIF (GB_IS_GLOBAL_CLIENT_BIT0_SET(GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iCurrentYacht), eGB_GLOBAL_CLIENT_BITSET_0_RESTRICTED_ACCOUNT)
									OR GB_IS_GLOBAL_CLIENT_BIT0_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_0_RESTRICTED_ACCOUNT)
									OR (DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iCurrentYacht))) AND GB_IS_THIS_PLAYERS_GANG_NAME_BLOCKED(GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iCurrentYacht)))
									OR bUserContentIsBlocked = TRUE)									
									AND NOT IS_DURANGO_AND_CAN_VIEW_GAMER_USER_CONTENT(GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iCurrentYacht))
										IF NOT (bUserContentIsBlocked)
											bUserContentIsBlocked = TRUE
											CPRINTLN(DEBUG_YACHT, "setting bUserContentIsBlocked = TRUE (2) ")
										ENDIF
										SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(GET_FILENAME_FOR_AUDIO_CONVERSATION("YACHT_GSY"))
										iHashYachtName = GET_HASH_KEY(GET_FILENAME_FOR_AUDIO_CONVERSATION("YACHT_GSY"))
									ELSE
										sTempName = GET_NAME_OF_PRIVATE_YACHT(iCurrentYacht)
										SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(sTempName)
										iHashYachtName = GET_HASH_KEY(sTempName)
									ENDIF
								ENDIF
							#IF IS_DEBUG_BUILD
							ENDIF
							#ENDIF
							// isWhiteText param
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bYachtWhiteText)
							// Country
							iFlagID = GET_FLAG_OF_PRIVATE_YACHT(iCurrentYacht)
							tlFlagCountry = "FLAG_CNTRY_"
							tlFlagCountry += iFlagID
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tlFlagCountry)
						END_SCALEFORM_MOVIE_METHOD()
						SET_SCALEFORM_MOVIE_TO_USE_LARGE_RT(sYachtNameSternOverlay, TRUE)
						iYachtNameControl = 3
					ELSE
						sYachtNameSternOverlay = REQUEST_SCALEFORM_MOVIE("YACHT_NAME_STERN")
					ENDIF
				BREAK
				CASE 3
					// Display name
					SET_TEXT_RENDER_ID(iPortRenderID)
					SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
					SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
					DRAW_SCALEFORM_MOVIE(sYachtNameOverlay, fPortCentreX, fPortCentreY, fPortWidth, fPortHeight, 255, 255, 255, 255)
					
					SET_TEXT_RENDER_ID(iStarbRenderID)
					SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
					SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
					DRAW_SCALEFORM_MOVIE(sYachtNameOverlay, fStarbCentreX, fStarbCentreY, fStarbWidth, fStarbHeight, 255, 255, 255, 255)
					
					SET_TEXT_RENDER_ID(iSternRenderID)
					SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
					SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
					DRAW_SCALEFORM_MOVIE(sYachtNameSternOverlay, fSternCentreX, fSternCentreY, fSternWidth, fSternHeight, 255, 255, 255, 255)
					
					SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
					#IF IS_DEBUG_BUILD
					IF bCustomYachtName
						IF GET_HASH_KEY(strYachtName) != GET_HASH_KEY(GET_CONTENTS_OF_TEXT_WIDGET(wYachtName))
							iYachtNameControl = 1
						ENDIF
					ELSE
					#ENDIF
						IF GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iCurrentYacht) != INVALID_PLAYER_INDEX()
							IF GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iCurrentYacht) = PLAYER_ID()
								sTempNameHash = GET_NAME_OF_PRIVATE_YACHT(iCurrentYacht)
								IF iHashYachtName != GET_HASH_KEY(sTempNameHash)
									iYachtNameControl = 1
								ENDIF
								
							ELIF (GB_IS_GLOBAL_CLIENT_BIT0_SET(GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iCurrentYacht), eGB_GLOBAL_CLIENT_BITSET_0_RESTRICTED_ACCOUNT)
							OR GB_IS_GLOBAL_CLIENT_BIT0_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_0_RESTRICTED_ACCOUNT)
							OR (DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iCurrentYacht))) AND GB_IS_THIS_PLAYERS_GANG_NAME_BLOCKED(GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iCurrentYacht)))
							OR bUserContentIsBlocked = TRUE)							
							AND NOT IS_DURANGO_AND_CAN_VIEW_GAMER_USER_CONTENT(GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iCurrentYacht))
								IF NOT (bUserContentIsBlocked)
									bUserContentIsBlocked = TRUE
									CPRINTLN(DEBUG_YACHT, "setting bUserContentIsBlocked = TRUE (3) ")
								ENDIF
								sTempNameHash = GET_FILENAME_FOR_AUDIO_CONVERSATION("YACHT_GSY")
								IF iHashYachtName != GET_HASH_KEY(sTempNameHash)
									iYachtNameControl = 1
								ENDIF
							ELSE
								sTempNameHash = GET_NAME_OF_PRIVATE_YACHT(iCurrentYacht)
								IF iHashYachtName != GET_HASH_KEY(sTempNameHash)
									iYachtNameControl = 1
								ENDIF
							ENDIF
						ENDIF
					#IF IS_DEBUG_BUILD
					ENDIF
					#ENDIF
					iFlagIDHash = GET_FLAG_OF_PRIVATE_YACHT(iCurrentYacht)
					IF iFlagID != iFlagIDHash
						iYachtNameControl = 1
					ENDIF
					iTintIDHash = GET_TINT_OF_PRIVATE_YACHT(iCurrentYacht)
					IF iTintID != iTintIDHash
						iYachtNameControl = 1
					ENDIF
				BREAK
			ENDSWITCH
		//ENDIF
	ELSE
		// Clean up
		IF HAS_SCALEFORM_MOVIE_LOADED(sYachtNameOverlay)
			SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sYachtNameOverlay)
		ENDIF
		IF HAS_SCALEFORM_MOVIE_LOADED(sYachtNameSternOverlay)
			SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sYachtNameSternOverlay)
		ENDIF
		IF IS_NAMED_RENDERTARGET_REGISTERED("port_text")   RELEASE_NAMED_RENDERTARGET("port_text")   ENDIF
		IF IS_NAMED_RENDERTARGET_REGISTERED("starb_text")  RELEASE_NAMED_RENDERTARGET("starb_text")  ENDIF
		IF IS_NAMED_RENDERTARGET_REGISTERED("stern_text")  RELEASE_NAMED_RENDERTARGET("stern_text")  ENDIF
		IF DOES_ENTITY_EXIST(oYachtNamePort)   SAFE_DELETE_OBJECT(oYachtNamePort)   ENDIF
		IF DOES_ENTITY_EXIST(oYachtNameStarb)  SAFE_DELETE_OBJECT(oYachtNameStarb)  ENDIF
		IF DOES_ENTITY_EXIST(oYachtNameStern)  SAFE_DELETE_OBJECT(oYachtNameStern)  ENDIF
		iPortRenderID = -1
		iStarbRenderID = -1
		iSternRenderID = -1
		iYachtNameControl = 0
	ENDIF
	
	
	// Overhead name
	IF NOT bYachtOwnerNameRequested
		IF iCurrentYacht != -1
		AND IS_GAMEPLAY_CAM_RENDERING()
			sfID_YachtOwnerName = REQUEST_SCALEFORM_MOVIE("YACHT_GAMERNAME")
			IF HAS_SCALEFORM_MOVIE_LOADED(sfID_YachtOwnerName)
				bYachtOwnerNameRequested = TRUE
			ENDIF
		ENDIF
	ELSE
	
		GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_NAME_OVERHEAD, sPropOffset)
		
		IF iCurrentYacht != -1
		AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
		AND MPGlobalsHud_TitleUpdate.bGamertagsDpadDownState != DPADDOWN_NONE
			PLAYER_INDEX playerID_YachtOwner = GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iCurrentYacht)
			IF playerID_YachtOwner != INVALID_PLAYER_INDEX()
			AND IS_NET_PLAYER_OK(playerID_YachtOwner, FALSE, TRUE)
			
				//SET_MISSION_INFO("missionName", "missionType", "playerInfo", "percentage", "debugValue", true, "playersRequired", 2, 2); 
				BEGIN_SCALEFORM_MOVIE_METHOD(sfID_YachtOwnerName, "SET_MISSION_INFO")
					// Mission Name - this may be from the mission creator
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
						ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY("")
					END_TEXT_COMMAND_SCALEFORM_STRING()
					
					// Mission Type - this will use the big display text if there is no Mission Name
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
						SET_COLOUR_OF_NEXT_TEXT_COMPONENT(HUD_COLOUR_WHITE)
						ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(GET_PLAYER_NAME(playerID_YachtOwner))
					END_TEXT_COMMAND_SCALEFORM_STRING()
					
					// Need to pad out any unused strings so that the percentage appears in the correct place
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
						ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY("")
					END_TEXT_COMMAND_SCALEFORM_STRING()
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
						ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY("")
					END_TEXT_COMMAND_SCALEFORM_STRING()
					// Percentage as a string so we append the % symbol - only display it if it is positive, otherwise pad out the parameter
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
						ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY("")
					END_TEXT_COMMAND_SCALEFORM_STRING()
				END_SCALEFORM_MOVIE_METHOD()
				
				VECTOR vCoords = sPropOffset.vLoc + <<0.0, 0.0, -2.5 >>
				VECTOR vRotation = GET_MISSION_LOCATE_ROTATION_VECTOR(vCoords, GET_FINAL_RENDERED_CAM_COORD())
				VECTOR vScale = << 1.0, 1.0, 1.0>>
				VECTOR vWorldScale = << 21.0, 21.0, 7.0 >>
				
				DRAW_SCALEFORM_MOVIE_3D_SOLID(sfID_YachtOwnerName, vCoords, vRotation, vScale, vWorldScale)
			ELSE
				PRINTLN("AM_MP_YACHT: playerID_YachtOwner = INVALID_PLAYER_INDEX() - Do Not Display Players Name Above Yacht.")
			ENDIF
		ELSE
			SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfID_YachtOwnerName)
			bYachtOwnerNameRequested = FALSE
		ENDIF
	ENDIF
ENDPROC

PROC KILL_ON_CALL_FOR_TRANSITION_STAGES()
	SWITCH iLocalStage
		CASE STAGE_USING_MENU
		CASE STAGE_SETUP_FOR_USING_PROPERTY	
		CASE STAGE_ENTER_PROPERTY	
		CASE STAGE_BUZZED_INTO_PROPERTY		
			IF NOT IS_BIT_SET(iLocalBS2,LOCAL_BS2_bResetOnCallForTransition)
				SET_TRANSITION_SESSIONS_RESTART_ON_CALL()
				SET_BIT(iLocalBS2,LOCAL_BS2_bResetOnCallForTransition)
			ELSE
				SET_TRANSITION_SESSIONS_ON_CALL_DISABLED_THIS_FRAME_AFTER_RESTART()
			ENDIF
		BREAK
		DEFAULT
			CLEAR_BIT(iLocalBS2,LOCAL_BS2_bResetOnCallForTransition)
		BREAK
	ENDSWITCH
ENDPROC

//#IF FEATURE_HEIST_PLANNING
//FUNC BOOL SHOULD_PLAYER_BYPASS_GENERAL_ACCESS_CHECKS_FOR_HEIST()
//	IF SHOULD_BYPASS_CHECKS_FOR_ENTRY(FALSE)
//	OR SHOULD_BYPASS_CHECKS_FOR_ENTRY(TRUE)
//		RETURN TRUE
//	ENDIF
//	RETURN FALSE
//ENDFUNC
//#ENDIF

FUNC BOOL IS_ANY_CONTEXT_ACTIVE()
	INT i
	REPEAT MAX_CONTEXT_INTENTION i
		IF g_IntentionList[i].bUsed
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_THERE_TOO_MANY_PLAYERS_CLOSE_TO_ACTIVITY(VECTOR vAngledAreaA, VECTOR vAngledAreaB)
	INT iParticipant
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))

			PLAYER_INDEX PlayerId 
			PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
			IF PlayerID <> PLAYER_ID()
				IF IS_NET_PLAYER_OK(PlayerId)

					IF IS_ENTITY_IN_ANGLED_AREA(GET_PLAYER_PED(PlayerID), vAngledAreaA, vAngledAreaB, 1)

//						PRINT_HELP_NO_SOUND("POD_TOO_MANY")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF

		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

PROC GENERATE_JACUZZI_DATA()
	INT iJacuzziSeatSlots[JAC_MAX_NUM_SLOTS] 
	iJacuzziSeatSlots[0] = 0
	iJacuzziSeatSlots[1] = 1
	iJacuzziSeatSlots[2] = 2
	iJacuzziSeatSlots[3] = 0
	iJacuzziSeatSlots[4] = 1
	iJacuzziSeatSlots[5] = 2
//	iJacuzziSeatSlots[0] = 2
//	iJacuzziSeatSlots[1] = 2
//	iJacuzziSeatSlots[2] = 2
//	iJacuzziSeatSlots[3] = 1
//	iJacuzziSeatSlots[4] = 1
//	iJacuzziSeatSlots[5] = 1
	INT iSeatSlot = 6-1
	WHILE iSeatSlot != 0 

		IF iSeatSlot != 0
			INT iRand = GET_RANDOM_INT_IN_RANGE(0,iSeatSlot+1)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "iSeatSlot = ", iSeatSlot, ", iRand = ", iRand)
			
			INT a = iJacuzziSeatSlots[iRand]
			iJacuzziSeatSlots[iRand] = iJacuzziSeatSlots[iSeatSlot]
			iJacuzziSeatSlots[iSeatSlot] = a
			CDEBUG1LN(DEBUG_SAFEHOUSE, "swapping ", iJacuzziSeatSlots[iSeatSlot]," with ", iJacuzziSeatSlots[iRand])
		ELSE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "iSeatSlot = 0")
		ENDIF
		
		iSeatSlot--
	ENDWHILE

	iJacuzziSeatSlots[6] = 0
	iJacuzziSeatSlots[7] = 1
	
	FOR iSeatSlot = 0 TO JAC_MAX_NUM_SLOTS-1	
		CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_JACUZZI_DATA: iJacuzziSeatSlots[", iSeatSlot,"] = ", iJacuzziSeatSlots[iSeatSlot])
		serverBD.jacuzziServerSeatData[iSeatSlot].iJacuzziAnimVariation = iJacuzziSeatSlots[iSeatSlot]
//		serverBD.jacuzziServerSeatData[iSeatSlot].bCigar = TRUE
//		serverBD.jacuzziServerSeatData[iSeatSlot].bCUP = TRUE
	ENDFOR 
	
	INT iNumberOfCigarActivities
	iNumberOfCigarActivities = GET_RANDOM_INT_IN_RANGE(2, 3)
	
	iNumberOfJacuzziProps = (iNumberOfCigarActivities*2) // 2 props per cigar activity
	CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_JACUZZI_DATA: number of cigar activities = ", iNumberOfCigarActivities)
	INT iNumCigarActAssigned
	iNumCigarActAssigned = 0
	REPEAT iNumberOfCigarActivities iNumCigarActAssigned
		INT iCigarSlot = GET_RANDOM_INT_IN_RANGE(0, JAC_MAX_NUM_SLOTS)
		WHILE serverBD.jacuzziServerSeatData[iCigarSlot].bCigar = TRUE
			iCigarSlot = GET_RANDOM_INT_IN_RANGE(0, JAC_MAX_NUM_SLOTS)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_JACUZZI_DATA: regenerating iCigarSlot becuase current cigar is already assigned as a cigar slot: ", iCigarSlot)
		ENDWHILE
		IF iCigarSlot < JAC_MAX_NUM_SEAT_SLOTS
			WHILE iJacuzziSeatSlots[iCigarSlot] = 0
				CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_JACUZZI_DATA: regenerating iCigarSlot becuase current cigar slot uses 0 variation: ", iCigarSlot)
				iCigarSlot = GET_RANDOM_INT_IN_RANGE(0, JAC_MAX_NUM_SEAT_SLOTS )
			ENDWHILE	
		ENDIF
		CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_JACUZZI_DATA: Cigar slot chosen: ", iCigarSlot)
		serverBD.jacuzziServerSeatData[iCigarSlot].bCigar = TRUE
	ENDREPEAT
	
	
	
	INT iNumberOfCupActivities
	iNumberOfCupActivities = GET_RANDOM_INT_IN_RANGE(1, 3)
	iNumberOfJacuzziProps += iNumberOfCupActivities
	CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_JACUZZI_DATA: number of cup activities = ", iNumberOfCupActivities)
	INT i
	i = 0
	REPEAT iNumberOfCupActivities i
		INT iCupSlot
		iCupSlot = GET_RANDOM_INT_IN_RANGE(0, JAC_MAX_NUM_SEAT_SLOTS)
		WHILE serverBD.jacuzziServerSeatData[iCupSlot].bCigar = TRUE
		OR iJacuzziSeatSlots[iCupSlot] = 0
			iCupSlot = GET_RANDOM_INT_IN_RANGE(0, JAC_MAX_NUM_SEAT_SLOTS)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_JACUZZI_DATA: regenerating iCupSlot becuase current cigar is already assigned as a cigar slot: ", iCupSlot)
		ENDWHILE
		serverBD.jacuzziServerSeatData[iCupSlot].bCup = TRUE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_JACUZZI_DATA: Cup slot chosen:", iCupSlot)
	ENDREPEAT
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_JACUZZI_DATA: iNumberOfJacuzziProps = ", iNumberOfJacuzziProps)
	
	// Make a selection from random swimwear
	REPEAT NUM_NETWORK_PLAYERS i
		serverBD.iSwimwearOptions[i] = GET_RANDOM_INT_IN_RANGE(0,6)
	ENDREPEAT
	
ENDPROC

//PROC GENERATE_JACUZZI_DATA()
//	INT iJacuzziSeatSlots[JAC_MAX_NUM_SLOTS] 
//	iJacuzziSeatSlots[0] = 0
//	iJacuzziSeatSlots[1] = 1
//	iJacuzziSeatSlots[2] = 0
//	iJacuzziSeatSlots[3] = 1
//	iJacuzziSeatSlots[4] = 0
//	iJacuzziSeatSlots[5] = 1
//	INT iSeatSlot = 6-1
//	WHILE iSeatSlot != 0 
//
//		IF iSeatSlot != 0
//			INT iRand = GET_RANDOM_INT_IN_RANGE(0,iSeatSlot+1)
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "iSeatSlot = ", iSeatSlot, ", iRand = ", iRand)
//			
//			INT a = iJacuzziSeatSlots[iRand]
//			iJacuzziSeatSlots[iRand] = iJacuzziSeatSlots[iSeatSlot]
//			iJacuzziSeatSlots[iSeatSlot] = a
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "swapping ", iJacuzziSeatSlots[iSeatSlot]," with ", iJacuzziSeatSlots[iRand])
//		ELSE
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "iSeatSlot = 0")
//		ENDIF
//		
//		iSeatSlot--
//	ENDWHILE
//
//	iJacuzziSeatSlots[6] = 0
//	iJacuzziSeatSlots[7] = 1
//	
//	FOR iSeatSlot = 0 TO JAC_MAX_NUM_SLOTS-1	
//		IF iJacuzziSeatSlots[iSeatSlot] != 0 
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_JACUZZI_DATA: iJacuzziSeatSlots[", iSeatSlot,"] = ", iJacuzziSeatSlots[iSeatSlot])
//			serverBD.jacuzziServerSeatData[iSeatSlot].iJacuzziAnimVariation = iJacuzziSeatSlots[iSeatSlot]
//		ENDIF
//	ENDFOR 
//	
//	INT iCigarSlot = GET_RANDOM_INT_IN_RANGE(0, JAC_MAX_NUM_SLOTS)
//	IF iCigarSlot < JAC_MAX_NUM_SEAT_SLOTS
//		WHILE iJacuzziSeatSlots[iCigarSlot] = 0
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_JACUZZI_DATA: regenerating iCigarSlot becuase current cigar slot uses 0 variation: ", iCigarSlot)
//			iCigarSlot = GET_RANDOM_INT_IN_RANGE(0, JAC_MAX_NUM_SEAT_SLOTS )
//		ENDWHILE	
//	ENDIF
//	CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_JACUZZI_DATA: Cigar slot chosen: ", iCigarSlot)
//	
//	#IF IS_DEBUG_BUILD
//	IF NOT db_bJacuzziForceCigarSlots
//	#ENDIF
//		serverBD.jacuzziServerSeatData[iCigarSlot].bCigar = TRUE
//		CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_JACUZZI_DATA: cigar slot = ", iCigarSlot)
//	#IF IS_DEBUG_BUILD
//	ELSE
//		iSeatSlot = 0
//		REPEAT JAC_MAX_NUM_SLOTS iSeatSlot	
//			serverBD.jacuzziServerSeatData[iSeatSlot].bCigar = TRUE
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_JACUZZI_DATA: cigar slot = ", iSeatSlot)
//		ENDREPEAT	
//	ENDIF
//	#ENDIF
//	
//	
//	#IF IS_DEBUG_BUILD
//	IF NOT db_bJacuzziForceCupSlots
//	#ENDIF
//		INT iCupSlot
//		iCupSlot = GET_RANDOM_INT_IN_RANGE(0, JAC_MAX_NUM_SLOTS)
//		WHILE iCupSlot = iCigarSlot
//			iCupSlot = GET_RANDOM_INT_IN_RANGE(0, JAC_MAX_NUM_SLOTS)
//		ENDWHILE
//		
//		serverBD.jacuzziServerSeatData[iCupSlot].bCup = TRUE
//		CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_JACUZZI_DATA: cup slot = ", iCupSlot)
//	
//	#IF IS_DEBUG_BUILD
//	ELSE
//		iSeatSlot = 0
//		REPEAT JAC_MAX_NUM_SLOTS iSeatSlot	
//			serverBD.jacuzziServerSeatData[iSeatSlot].bCup = TRUE
//		ENDREPEAT
//	ENDIF
//	#ENDIF
//ENDPROC
// Gets the variations of animations that should be played for each seat from the server
PROC GET_SERVER_JACUZZI_SEAT_ANIM_VARIATION_DATA() 
	INT iSeat = 0
	REPEAT JAC_MAX_NUM_SLOTS iSeat
//		ServerBroadcastData.jacuzziServerSeatData[iSeat].iJacuzziAnimVariation
		jacuzziClientSeatData[iSeat].iJacuzziAnimVariation = serverBD.jacuzziServerSeatData[iSeat].iJacuzziAnimVariation
	ENDREPEAT
	
ENDPROC

PROC INIT_CLIENT_JACUZZI_ACITVITY()
//	iJacuzziPromptContextIntention = NEW_CONTEXT_INTENTION
	GET_SERVER_JACUZZI_SEAT_ANIM_VARIATION_DATA()
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    iSeatID - 
///    MPJacuzziLocalTemp - 
///    fAngleOffset - for calculating locates we want to offset the angle used by a couple of degrees on both sides (for two different locates)
/// RETURNS:
///    
FUNC FLOAT GET_MP_JACUZZI_SEAT_ANGLE(INT iSeatID, MP_JACUZZI_LOCAL_DATA_STRUCT &MPJacuzziLocalTemp, FLOAT fAngleOffset = 0.0)
	IF iSeatID > -1 AND iSeatID < JAC_MAX_NUM_SEAT_SLOTS
		FLOAT fTotalAngleSpace = 214.0 // We need to fit 6 seats in the specified by the angle section of the circle
		
		FLOAT fSeatAngleSpace = fTotalAngleSpace / TO_FLOAT(JAC_MAX_NUM_SEAT_SLOTS)
		FLOAT fCenteringOffset = (fTotalAngleSpace - 180.0) * 0.5 // Make sure the seats are equally aligned on both sides of the jacuzzi
		
		FLOAT fTotalAngleOffset = MPJacuzziLocalTemp.fSeatAngleOffsets[iSeatID] + fAngleOffset // fSeatAngleOffsets are used to move each seat a bit in order to avoid perfectly uniform seat distibution, which might look weird
		
		RETURN GET_OFFSET_HEADING_FROM_YACHT_AS_WORLD_HEADING(iCurrentYacht, 180.0 + fSeatAngleSpace * 0.5 - fCenteringOffset + iSeatID * fSeatAngleSpace + fTotalAngleOffset)
	ENDIF
	
	RETURN 0.0
ENDFUNC

FUNC VECTOR GET_MP_JACUZZI_SEAT_ROTATION(INT iSeatID, MP_JACUZZI_LOCAL_DATA_STRUCT &MPJacuzziLocalTemp)
	IF iSeatID > -1 AND iSeatID < JAC_MAX_NUM_SEAT_SLOTS
		RETURN <<0.0, 0.0, GET_MP_JACUZZI_SEAT_ANGLE(iSeatID, MPJacuzziLocalTemp) + MPJacuzziLocalTemp.fSeatHeadingOffsets[iSeatID] - 270.0>>
	ENDIF
	
	RETURN <<0,0,0>>
ENDFUNC

FUNC VECTOR GET_JACUZZI_POSITION_ON_CIRCLE(FLOAT fRadius, FLOAT fAngle, FLOAT fTop)
	FLOAT fXCoord = vJacuzziCentre.X + fRadius * COS(fAngle)
	FLOAT fYCoord = vJacuzziCentre.Y + fRadius * SIN(fAngle)
	
	VECTOR vPosition = <<fXCoord, fYCoord, fTop>>
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_JACUZZI_POSITION_ON_CIRCLE - the calculated position is ", VECTOR_TO_STRING(vPosition), " with radius ", fRadius)
	
	RETURN vPosition
ENDFUNC

/// PURPOSE:
///    Calculates position of jacuzzi seat (SEATS ONLY, standing positions are hardcoded) using some circle math
///    There's a whole set of offset vars used to finetune each seat position (fSeatRadiusOffsets, fSeatHeadingOffsets, fSeatAngleOffsets)
/// PARAMS:
///    iSeatID - 
///    MPJacuzziLocalTemp - 
/// RETURNS:
///    
FUNC VECTOR GET_MP_JACUZZI_SEAT_POSITION(INT iSeatID, MP_JACUZZI_LOCAL_DATA_STRUCT &MPJacuzziLocalTemp)
	IF iSeatID > -1 AND iSeatID < JAC_MAX_NUM_SEAT_SLOTS
		// I dunno what it is, either jacuzzi is not round enough or jacuzzi centre is slightly offset but we need additional tweaks on radius of each seat
		FLOAT fJacuzziRadius = fJacuzziBaseSeatRadius + MPJacuzziLocalTemp.fSeatRadiusOffsets[iSeatID]
		FLOAT fSeatAngle = GET_MP_JACUZZI_SEAT_ANGLE(iSeatID, MPJacuzziLocalTemp)
		VECTOR vSeatTop = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iCurrentYacht, <<0.0, 0.0, fJacuzziBaseSeatHeight>>)
		
		#IF IS_DEBUG_BUILD
			//vSeatTop.Z = vSeatTop.Z + fDebugSeatBaseTopOffset
		#ENDIF
		
		RETURN GET_JACUZZI_POSITION_ON_CIRCLE(fJacuzziRadius, fSeatAngle, vSeatTop.Z)
	ENDIF
	
	RETURN <<0,0,0>>
ENDFUNC

FUNC VECTOR GET_MP_JACUZZI_SEAT_APPROACH(INT iSeatID, MP_JACUZZI_LOCAL_DATA_STRUCT &MPJacuzziLocalTemp)
	IF iSeatID > -1 AND iSeatID < JAC_MAX_NUM_SEAT_SLOTS
		FLOAT fJacuzziRadius = fJacuzziBaseSeatRadius + MPJacuzziLocalTemp.fSeatRadiusOffsets[iSeatID] - fJacuzziApproachOffset
		FLOAT fSeatAngle = GET_MP_JACUZZI_SEAT_ANGLE(iSeatID, MPJacuzziLocalTemp)
		VECTOR vSeatTop = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iCurrentYacht, <<0.0, 0.0, fJacuzziBaseSeatHeight>>)
		
		#IF IS_DEBUG_BUILD
			//vSeatTop.Z = vSeatTop.Z + fDebugSeatBaseTopOffset + fDebugSeatApproachTopOffset
		#ENDIF
		
		RETURN GET_JACUZZI_POSITION_ON_CIRCLE(fJacuzziRadius, fSeatAngle, vSeatTop.Z)
	ENDIF
	
	RETURN <<0,0,0>>
ENDFUNC

FUNC VECTOR GET_MP_JACUZZI_LOCATE0_POSITION(INT iSeatID, MP_JACUZZI_LOCAL_DATA_STRUCT &MPJacuzziLocalTemp)
	IF iSeatID > -1 AND iSeatID < JAC_MAX_NUM_SEAT_SLOTS
		FLOAT fJacuzziRadius = (fJacuzziBaseSeatRadius - 0.36) + MPJacuzziLocalTemp.fSeatRadiusOffsets[iSeatID]
		FLOAT fSeatAngle = GET_MP_JACUZZI_SEAT_ANGLE(iSeatID, MPJacuzziLocalTemp, -17.0)
		VECTOR vSeatTop = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iCurrentYacht, <<0.0, 0.0, fJacuzziBaseSeatHeight - 0.1>>)
		
		#IF IS_DEBUG_BUILD
			//vSeatTop.Z = vSeatTop.Z + fDebugSeatBaseTopOffset
		#ENDIF
		
		RETURN GET_JACUZZI_POSITION_ON_CIRCLE(fJacuzziRadius, fSeatAngle, vSeatTop.Z)
	ENDIF
	
	RETURN <<0,0,0>>
ENDFUNC

FUNC VECTOR GET_MP_JACUZZI_LOCATE1_POSITION(INT iSeatID, MP_JACUZZI_LOCAL_DATA_STRUCT &MPJacuzziLocalTemp)
	IF iSeatID > -1 AND iSeatID < JAC_MAX_NUM_SEAT_SLOTS
		FLOAT fJacuzziRadius = (fJacuzziBaseSeatRadius - 0.36) + MPJacuzziLocalTemp.fSeatRadiusOffsets[iSeatID]
		FLOAT fSeatAngle = GET_MP_JACUZZI_SEAT_ANGLE(iSeatID, MPJacuzziLocalTemp, 17.0)
		VECTOR vSeatTop = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iCurrentYacht, <<0.0, 0.0, fJacuzziBaseSeatHeight + 2.0>>)
		
		#IF IS_DEBUG_BUILD
			//vSeatTop.Z = vSeatTop.Z + fDebugSeatBaseTopOffset
		#ENDIF
		
		RETURN GET_JACUZZI_POSITION_ON_CIRCLE(fJacuzziRadius, fSeatAngle, vSeatTop.Z)
	ENDIF
	
	RETURN <<0,0,0>>
ENDFUNC

PROC COMPUTE_AND_STORE_JACUZZI_SEATS_POSITIONS(MP_JACUZZI_LOCAL_DATA_STRUCT &MPJacuzziLocalTemp)
	INT iSeatID
	REPEAT JAC_MAX_NUM_SEAT_SLOTS iSeatID
		MPJacuzziLocalTemp.seatAnimPositions[iSeatID].vLoc = GET_MP_JACUZZI_SEAT_POSITION(iSeatID, MPJacuzziLocalTemp)
		MPJacuzziLocalTemp.seatAnimPositions[iSeatID].vRot = GET_MP_JACUZZI_SEAT_ROTATION(iSeatID, MPJacuzziLocalTemp)
		MPJacuzziLocalTemp.seatLocates0[iSeatID].vLoc = GET_MP_JACUZZI_LOCATE0_POSITION(iSeatID, MPJacuzziLocalTemp)
		MPJacuzziLocalTemp.seatLocates1[iSeatID].vLoc = GET_MP_JACUZZI_LOCATE1_POSITION(iSeatID, MPJacuzziLocalTemp)
		MPJacuzziLocalTemp.seatApproachPositions[iSeatID] = GET_MP_JACUZZI_SEAT_APPROACH(iSeatID, MPJacuzziLocalTemp)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "=== MP_Jacuzzi LOCAL === COMPUTE_AND_STORE_JACUZZI_SEATS_POSITIONS Seat ",iSeatID," at ",MPJacuzziLocalTemp.seatAnimPositions[iSeatID].vLoc)
	ENDREPEAT
ENDPROC

PROC INITIALISE_LEAN_STRUCT_DATA()

	// Mid deck aft Left
	mpYachtLeanLocal.leanAnimPositions[0] = <<-1422.334, -3770.827, 7.99>>
	mpYachtLeanLocal.leanAnimHeadings[0] = 92.800
	
	// Mid deck aft Middle
	mpYachtLeanLocal.leanAnimPositions[1] = <<-1422.630, -3771.717, 7.99>>
	mpYachtLeanLocal.leanAnimHeadings[1] = 92.800

	//Mid deck aft Right
	mpYachtLeanLocal.leanAnimPositions[2] = <<-1422.928, -3772.645, 7.99>>
	mpYachtLeanLocal.leanAnimHeadings[2] = 92.800

	//Starboard(Right
	//Back
	//Upper deck right
	mpYachtLeanLocal.leanAnimPositions[3] = << -1433.080, -3762.269, 10.927 >>
	mpYachtLeanLocal.leanAnimHeadings[3] = 161.300
	mpYachtLeanLocal.leanLocates0[3] = <<-1432.213989,-3762.189209,10.907578>>
	mpYachtLeanLocal.leanLocates1[3] = <<-1433.215820,-3761.533691,12.657583>> 
	//mpYachtLeanLocal.leanHeadingTrigger[3].vLoc = <<0,0,-35.59>>

	//Middle right up deck
	mpYachtLeanLocal.leanAnimPositions[4] = << -1433.944, -3761.670, 10.920 >>
	mpYachtLeanLocal.leanAnimHeadings[4] = 173.000
	mpYachtLeanLocal.leanLocates0[4] = <<-1434.303589,-3760.929443,10.907553>>
	mpYachtLeanLocal.leanLocates1[4] = <<-1433.215820,-3761.533691,12.657583>> 
	//mpYachtLeanLocal.leanHeadingTrigger[4].vLoc = <<0,0,-35.59>>

	//Front right up deck
	mpYachtLeanLocal.leanAnimPositions[5] = << -1435.261, -3761.163, 10.921 >>
	mpYachtLeanLocal.leanAnimHeadings[5] = -177.000
	mpYachtLeanLocal.leanLocates0[5] = <<-1434.303589,-3760.929443,10.907553>> 
	mpYachtLeanLocal.leanLocates1[5] = <<-1435.515625,-3760.405273,12.657524>> 
	//mpYachtLeanLocal.leanHeadingTrigger[5].vLoc = <<0,0,-35.59>>

	//Port(left)
	//front
	//Upper deck left
	mpYachtLeanLocal.leanAnimPositions[6] = << -1439.200, -3772.767, 10.923 >>
	mpYachtLeanLocal.leanAnimHeadings[6] = 2.700
	mpYachtLeanLocal.leanLocates0[6] = <<-1438.466309,-3773.501221,10.907542>>
	mpYachtLeanLocal.leanLocates1[6] = <<-1439.696533,-3773.107666,12.657513>>  
	//mpYachtLeanLocal.leanHeadingTrigger[6].vLoc = <<0,0,-173.11>>

	//Middle Left up deck
	mpYachtLeanLocal.leanAnimPositions[7] = << -1437.980, -3773.140, 10.923 >>
	mpYachtLeanLocal.leanAnimHeadings[7] = 9.500
	mpYachtLeanLocal.leanLocates0[7] = <<-1438.466309,-3773.501221,10.907542>>
	mpYachtLeanLocal.leanLocates1[7] = <<-1437.260376,-3773.740234,12.657553>>  
	//mpYachtLeanLocal.leanHeadingTrigger[7].vLoc = <<0,0,-173.11>>

	//Stirn
	mpYachtLeanLocal.leanAnimPositions[8] = << -1436.703, -3773.217, 10.920 >>
	mpYachtLeanLocal.leanAnimHeadings[8] = 23.520
	mpYachtLeanLocal.leanLocates0[8] = <<-1435.980591,-3773.656006,10.907570>> 
	mpYachtLeanLocal.leanLocates1[8] = <<-1437.260376,-3773.740234,12.657553>> 
	//mpYachtLeanLocal.leanHeadingTrigger[8].vLoc = <<0,0,-173.11>>

	// Bow
	iBowIndex = 9
	mpYachtLeanLocal.leanAnimPositions[9] = << -1509.926, -3741.487, 8.415 >>
	mpYachtLeanLocal.leanAnimHeadings[9] = 0.050
	mpYachtLeanLocal.leanLocates0[9] = <<-1515.272461,-3742.173584,8.379925>>
	mpYachtLeanLocal.leanLocates1[9] = <<-1514.966431,-3741.023926,10.058345>> 
	//mpYachtLeanLocal.leanHeadingTrigger[9].vLoc = <<0,0,-173.11>>
	
	// Front starboard
	mpYachtLeanLocal.leanAnimPositions[10] = <<-1495.222, -3742.470, 7.991>>
	mpYachtLeanLocal.leanAnimHeadings[10] = -169.000
	
	mpYachtLeanLocal.leanAnimPositions[11] = <<-1494.012, -3742.702, 7.991>>
	mpYachtLeanLocal.leanAnimHeadings[11] = -169.000
	
	mpYachtLeanLocal.leanAnimPositions[12] = <<-1492.652, -3743.002, 7.991>>
	mpYachtLeanLocal.leanAnimHeadings[12] = -171.500
	
	mpYachtLeanLocal.leanAnimPositions[13] = <<-1490.859, -3743.429, 7.991>>
	mpYachtLeanLocal.leanAnimHeadings[13] = -171.500
	
	mpYachtLeanLocal.leanAnimPositions[14] = <<-1489.410, -3743.811, 7.991>>
	mpYachtLeanLocal.leanAnimHeadings[14] = -173.000
	
	mpYachtLeanLocal.leanAnimPositions[15] = <<-1487.932, -3744.229, 7.991>>
	mpYachtLeanLocal.leanAnimHeadings[15] = -173.500
	
	mpYachtLeanLocal.leanAnimPositions[16] = <<-1486.594, -3744.627, 7.991>>
	mpYachtLeanLocal.leanAnimHeadings[16] = -175.000
	
	mpYachtLeanLocal.leanAnimPositions[17] = <<-1485.050, -3745.095, 7.991>>
	mpYachtLeanLocal.leanAnimHeadings[17] = -175.000
	
	mpYachtLeanLocal.leanAnimPositions[18] = <<-1483.342, -3745.626, 7.991>>
	mpYachtLeanLocal.leanAnimHeadings[18] = -175.000
	
	mpYachtLeanLocal.leanAnimPositions[19] = <<-1481.785, -3746.129, 7.991>>
	mpYachtLeanLocal.leanAnimHeadings[19] = -176.000
	
	// Front port
	mpYachtLeanLocal.leanAnimPositions[20] = <<-1498.730, -3752.512, 7.991>>
	mpYachtLeanLocal.leanAnimHeadings[20] = -5.500
	
	mpYachtLeanLocal.leanAnimPositions[21] = <<-1497.558, -3753.062, 7.991>>
	mpYachtLeanLocal.leanAnimHeadings[21] = -4.000
	
	mpYachtLeanLocal.leanAnimPositions[22] = <<-1496.202, -3753.665, 7.991>>
	mpYachtLeanLocal.leanAnimHeadings[22] = -2.900
	
	mpYachtLeanLocal.leanAnimPositions[23] = <<-1494.724, -3754.285, 7.991>>
	mpYachtLeanLocal.leanAnimHeadings[23] = -1.500
	
	mpYachtLeanLocal.leanAnimPositions[24] = <<-1493.310, -3754.848, 7.991>>
	mpYachtLeanLocal.leanAnimHeadings[24] = -0.200
	
	mpYachtLeanLocal.leanAnimPositions[25] = <<-1491.810, -3755.402, 7.991>>
	mpYachtLeanLocal.leanAnimHeadings[25] = 0.500
	
	mpYachtLeanLocal.leanAnimPositions[26] = <<-1490.376, -3755.913, 7.991>>
	mpYachtLeanLocal.leanAnimHeadings[26] = 1.000
	
	mpYachtLeanLocal.leanAnimPositions[27] = <<-1488.787, -3756.469, 7.991>>
	mpYachtLeanLocal.leanAnimHeadings[27] = 1.900
	
	mpYachtLeanLocal.leanAnimPositions[28] = <<-1487.175, -3757.013, 7.991>>
	mpYachtLeanLocal.leanAnimHeadings[28] = 1.700
	
	mpYachtLeanLocal.leanAnimPositions[29] = <<-1485.658, -3757.519, 7.991>>
	mpYachtLeanLocal.leanAnimHeadings[29] = 3.000
	
	// Calculate locates for front of the ship beased on the heading, also first three (middle of the back deck)
	INT i
	VECTOR locateTangent, locateNormal
	FLOAT fAngle, fLocateLength, fLocateOffset
	REPEAT YACHT_MAX_NUM_LEAN_SLOTS i
		IF i < 3 OR i > 9
			fAngle = mpYachtLeanLocal.leanAnimHeadings[i] - 21.0
			locateTangent = NORMALISE_VECTOR(<<COS(fAngle), SIN(fAngle), 0.0>>)
			locateNormal = ROTATE_VECTOR_ABOUT_Z_ORTHO(locateTangent, ROTSTEP_NEG_90)
			
			IF i > 9
				fLocateLength = 0.75
				fLocateOffset = 0.2
			ELSE
				fLocateLength = 0.52
				fLocateOffset = 0.45
			ENDIF
		
			mpYachtLeanLocal.leanLocates0[i] = mpYachtLeanLocal.leanAnimPositions[i] + (locateTangent * fLocateLength) + (locateNormal * fLocateOffset)
			mpYachtLeanLocal.leanLocates1[i] = mpYachtLeanLocal.leanAnimPositions[i] - (locateTangent * fLocateLength) + (locateNormal * fLocateOffset) + <<0.0, 0.0, 1.75>>
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC STORE_YACHT_LEAN_SEAT_POSITIONS(MP_YACHT_LEAN_LOCAL_DATA_STRUCT &MPYachtLeanLocalTemp)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "=== MP_YACHT_LEAN_LOCAL_DATA_STRUCT LOCAL === iCurrentYacht ", iCurrentYacht)

	MP_PROP_OFFSET_STRUCT tempOffset

	INT iLeanID
	REPEAT YACHT_MAX_NUM_LEAN_SLOTS iLeanID
		
		tempOffset.vLoc = MPYachtLeanLocalTemp.leanAnimPositions[iLeanID]
		GET_OFFSET_LOCATION_FOR_YACHT_EXTERIOR_VECTOR(iCurrentYacht, tempOffset)
		MPYachtLeanLocalTemp.leanAnimPositions[iLeanID] = tempOffset.vLoc
		
		tempOffset.vRot = <<0.0, 0.0, MPYachtLeanLocalTemp.leanAnimHeadings[iLeanID]>>
		GET_OFFSET_LOCATION_FOR_YACHT_EXTERIOR_VECTOR(iCurrentYacht, tempOffset)
		MPYachtLeanLocalTemp.leanAnimHeadings[iLeanID] = tempOffset.vRot.Z
		
		tempOffset.vLoc = MPYachtLeanLocalTemp.leanLocates0[iLeanID]
		GET_OFFSET_LOCATION_FOR_YACHT_EXTERIOR_VECTOR(iCurrentYacht, tempOffset)
		MPYachtLeanLocalTemp.leanLocates0[iLeanID] = tempOffset.vLoc
		
		tempOffset.vLoc = MPYachtLeanLocalTemp.leanLocates1[iLeanID]
		GET_OFFSET_LOCATION_FOR_YACHT_EXTERIOR_VECTOR(iCurrentYacht, tempOffset)
		MPYachtLeanLocalTemp.leanLocates1[iLeanID] = tempOffset.vLoc
		
		CDEBUG1LN(DEBUG_SAFEHOUSE, "=== MP_YACHT_LEAN_LOCAL_DATA_STRUCT LOCAL === STORE_SEAT_LOCATE0 Seat ",iLeanID," Locate0 at ",MPYachtLeanLocalTemp.leanLocates0[iLeanID])
		CDEBUG1LN(DEBUG_SAFEHOUSE, "=== MP_YACHT_LEAN_LOCAL_DATA_STRUCT LOCAL === STORE_SEAT_LOCATE1 Seat ",iLeanID," Locate1 at ",MPYachtLeanLocalTemp.leanLocates1[iLeanID])
		CDEBUG1LN(DEBUG_SAFEHOUSE, "=== MP_YACHT_LEAN_LOCAL_DATA_STRUCT LOCAL === STORE_SEAT_ANIM_POSITIONS Seat ",iLeanID," at ",MPYachtLeanLocalTemp.leanAnimPositions[iLeanID])
		CDEBUG1LN(DEBUG_SAFEHOUSE, "=== MP_YACHT_LEAN_LOCAL_DATA_STRUCT LOCAL === STORE_SEAT_ANIM_POSITIONS Seat ",iLeanID," at ",MPYachtLeanLocalTemp.leanAnimHeadings[iLeanID])
	ENDREPEAT
	
ENDPROC
/// PURPOSE:
///    Store seats positions for MP Jacuzzi in high end apartments
/// PARAMS:
///    MPJacuzziLocal -
///    property - current property ID

PROC STORE_JACUZZI_SEAT_POSITIONS(MP_JACUZZI_LOCAL_DATA_STRUCT &MPJacuzziLocalTemp, BOOL bGenerateSeatPos = FALSE)
	IF bGenerateSeatPos
		COMPUTE_AND_STORE_JACUZZI_SEATS_POSITIONS(MPJacuzziLocalTemp)
		
		INT iSeatID
		FOR iSeatID = JAC_MAX_NUM_SEAT_SLOTS TO (JAC_MAX_NUM_SLOTS - 1)
			GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_MPJACUZZI_SEAT_LOCATE0_0 + iSeatID, MPJacuzziLocalTemp.seatLocates0[iSeatID])
			GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_MPJACUZZI_SEAT_LOCATE1_0 + iSeatID, MPJacuzziLocalTemp.seatLocates1[iSeatID])
			GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_MPJACUZZI_SEAT_ANIM_ROOT_0 + iSeatID, MPJacuzziLocalTemp.seatAnimPositions[iSeatID])
			MPJacuzziLocalTemp.seatApproachPositions[iSeatID] = MPJacuzziLocalTemp.seatAnimPositions[iSeatID].vLoc
			CDEBUG1LN(DEBUG_SAFEHOUSE, "=== MP_Jacuzzi LOCAL === STORE_SEAT_LOCATE0 Seat ",iSeatID," Locate0 at ",MPJacuzziLocalTemp.seatLocates0[iSeatID].vLoc)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "=== MP_Jacuzzi LOCAL === STORE_SEAT_LOCATE1 Seat ",iSeatID," Locate1 at ",MPJacuzziLocalTemp.seatLocates1[iSeatID].vLoc)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "=== MP_Jacuzzi LOCAL === STORE_SEAT_ANIM_POSITIONS Seat ",iSeatID," at ",MPJacuzziLocalTemp.seatAnimPositions[iSeatID].vLoc)
		ENDFOR
		
		// Offsets used to finetune seats positions
		MPJacuzziLocalTemp.fSeatRadiusOffsets[0] = 0.208
		MPJacuzziLocalTemp.fSeatRadiusOffsets[1] = 0.070
		MPJacuzziLocalTemp.fSeatRadiusOffsets[2] = -0.032
		MPJacuzziLocalTemp.fSeatRadiusOffsets[3] = -0.048
		MPJacuzziLocalTemp.fSeatRadiusOffsets[4] = 0.060
		MPJacuzziLocalTemp.fSeatRadiusOffsets[5] = 0.208
		
		MPJacuzziLocalTemp.fSeatAngleOffsets[0] = 0.0
		MPJacuzziLocalTemp.fSeatAngleOffsets[1] = 0.0
		MPJacuzziLocalTemp.fSeatAngleOffsets[2] = 0.0
		MPJacuzziLocalTemp.fSeatAngleOffsets[3] = 0.0
		MPJacuzziLocalTemp.fSeatAngleOffsets[4] = 0.0
		MPJacuzziLocalTemp.fSeatAngleOffsets[5] = 0.0
		
		MPJacuzziLocalTemp.fSeatHeadingOffsets[0] = -3.960
		MPJacuzziLocalTemp.fSeatHeadingOffsets[1] = -1.080
		MPJacuzziLocalTemp.fSeatHeadingOffsets[2] = -3.960
		MPJacuzziLocalTemp.fSeatHeadingOffsets[3] = -13.320
		MPJacuzziLocalTemp.fSeatHeadingOffsets[4] = -14.400
		MPJacuzziLocalTemp.fSeatHeadingOffsets[5] = -12.600
	ELSE
		INT iSeatID
		REPEAT JAC_MAX_NUM_SEAT_SLOTS iSeatID
			
			CDEBUG1LN(DEBUG_SAFEHOUSE, "STORE_JACUZZI_SEAT_POSITIONS: iSeatID: ", iSeatID, " var = ", serverBD.jacuzziServerSeatData[iSeatID].iJacuzziAnimVariation)
			
			IF serverBD.jacuzziServerSeatData[iSeatID].iJacuzziAnimVariation = 0
				GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_MPJACUZZI_SEAT_LOCATE0_0 + iSeatID, MPJacuzziLocalTemp.seatLocates0[iSeatID])
				GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_MPJACUZZI_SEAT_LOCATE1_0 + iSeatID, MPJacuzziLocalTemp.seatLocates1[iSeatID])
				GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_MPJACUZZI_SEAT_ANIM_ROOT_0 + iSeatID, MPJacuzziLocalTemp.seatAnimPositions[iSeatID])
				MPJacuzziLocalTemp.seatApproachPositions[iSeatID] = MPJacuzziLocalTemp.seatAnimPositions[iSeatID].vLoc
			ELIF serverBD.jacuzziServerSeatData[iSeatID].iJacuzziAnimVariation = 1
				GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_MPJACUZZI_SEAT_LOCATE0_0 + iSeatID, MPJacuzziLocalTemp.seatLocates0[iSeatID])
				GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_MPJACUZZI_SEAT_LOCATE1_0 + iSeatID, MPJacuzziLocalTemp.seatLocates1[iSeatID])
				GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_MPJACUZZI_SEAT_ANIM_ROOT_0_V1 + iSeatID, MPJacuzziLocalTemp.seatAnimPositions[iSeatID])
				MPJacuzziLocalTemp.seatApproachPositions[iSeatID] = MPJacuzziLocalTemp.seatAnimPositions[iSeatID].vLoc
			ELIF serverBD.jacuzziServerSeatData[iSeatID].iJacuzziAnimVariation = 2
				GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_MPJACUZZI_SEAT_LOCATE0_0 + iSeatID, MPJacuzziLocalTemp.seatLocates0[iSeatID])
				GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_MPJACUZZI_SEAT_LOCATE1_0 + iSeatID, MPJacuzziLocalTemp.seatLocates1[iSeatID])
				GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_MPJACUZZI_SEAT_ANIM_ROOT_0_V2 + iSeatID, MPJacuzziLocalTemp.seatAnimPositions[iSeatID])
				MPJacuzziLocalTemp.seatApproachPositions[iSeatID] = MPJacuzziLocalTemp.seatAnimPositions[iSeatID].vLoc
			ENDIF
			CDEBUG1LN(DEBUG_SAFEHOUSE, "STORE_JACUZZI_SEAT_POSITIONS: === MP_Jacuzzi LOCAL === STORE_SEAT_LOCATE0 Seat ",iSeatID," Locate0 at ",MPJacuzziLocalTemp.seatLocates0[iSeatID].vLoc)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "STORE_JACUZZI_SEAT_POSITIONS: === MP_Jacuzzi LOCAL === STORE_SEAT_LOCATE1 Seat ",iSeatID," Locate1 at ",MPJacuzziLocalTemp.seatLocates1[iSeatID].vLoc)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "STORE_JACUZZI_SEAT_POSITIONS: === MP_Jacuzzi LOCAL === STORE_SEAT_ANIM_POSITIONS Seat ",iSeatID," at ",MPJacuzziLocalTemp.seatAnimPositions[iSeatID].vLoc)
		ENDREPEAT
		
		GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_MPJACUZZI_LEAN_LOCATE0_6, MPJacuzziLocalTemp.seatLocates0[6])
		GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_MPJACUZZI_LEAN_LOCATE1_6, MPJacuzziLocalTemp.seatLocates1[6])
		GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_MPJACUZZI_LEAN_ANIM_ROOT_6, MPJacuzziLocalTemp.seatAnimPositions[6])
		MPJacuzziLocalTemp.seatApproachPositions[6] = MPJacuzziLocalTemp.seatAnimPositions[6].vLoc
		
		GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_MPJACUZZI_LEAN_LOCATE0_7, MPJacuzziLocalTemp.seatLocates0[7])
		GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_MPJACUZZI_LEAN_LOCATE1_7, MPJacuzziLocalTemp.seatLocates1[7])
		GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_MPJACUZZI_LEAN_ANIM_ROOT_7, MPJacuzziLocalTemp.seatAnimPositions[7])
		MPJacuzziLocalTemp.seatApproachPositions[7] = MPJacuzziLocalTemp.seatAnimPositions[7].vLoc
	ENDIF
ENDPROC

/// PURPOSE:
///    Runs all routines which calculate different positions (seats, lean, center, water) used by jacuzzi logic
PROC STORE_JACUZZI_POSITIONS()

	MP_PROP_OFFSET_STRUCT tempOffset
	
	// Get jacuzzi center
	GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_JACUZZI_CENTRE, tempOffset)
	vJacuzziCentre = tempOffset.vLoc
	
	// Get jacuzzi absolute water level
	VECTOR vWaterLevel = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iCurrentYacht, <<0.0, 0.0, fJacuzziRelativeWaterLevel>>)
	fJacuzziActualWaterLevel = vWaterLevel.Z
	
	/*
	// Calculate rough jacuzzi bounds
	vJacuzziBound0 = <<vJacuzziCentre.X - JAC_RADIUS, vJacuzziCentre.Y - JAC_RADIUS, vJacuzziCentre.Z>>
	vJacuzziBound1 = <<vJacuzziCentre.X + JAC_RADIUS, vJacuzziCentre.Y + JAC_RADIUS, fJacuzziActualWaterLevel>> 
	
	vJacuzziBound0 = vJacuzziBound0
	vJacuzziBound1 = vJacuzziBound1
	*/
	
	STORE_JACUZZI_SEAT_POSITIONS(mpJacuzziLocal)
	
	SET_BIT(iJacuzziBitSet, JAC_BS_POSITIONS_CALCULATED)
ENDPROC

PROC STORE_JACUZZI_LOCAL_POSITIONS()

ENDPROC

FUNC VECTOR GET_MP_JACUZZI_SEAT_ANIM_POSITION(INT iSeatID, MP_JACUZZI_LOCAL_DATA_STRUCT &MPJacuzziLocalTemp)
	IF iSeatID > -1 AND iSeatID < JAC_MAX_NUM_SLOTS
		RETURN MPJacuzziLocalTemp.seatAnimPositions[iSeatID].vLoc
	ENDIF
	
	RETURN <<0,0,0>>
ENDFUNC

FUNC VECTOR GET_MP_JACUZZI_SEAT_ANIM_ROTATION(INT iSeatID, MP_JACUZZI_LOCAL_DATA_STRUCT &MPJacuzziLocalTemp)
	IF iSeatID > -1 AND iSeatID < JAC_MAX_NUM_SLOTS
		RETURN MPJacuzziLocalTemp.seatAnimPositions[iSeatID].vRot
	ENDIF
	
	RETURN <<0,0,0>>
ENDFUNC

FUNC VECTOR GET_MP_JACUZZI_SEAT_LOCATE0(INT iSeatID, MP_JACUZZI_LOCAL_DATA_STRUCT &MPJacuzziLocalTemp)
	IF iSeatID > -1 AND iSeatID < JAC_MAX_NUM_SLOTS
		RETURN MPJacuzziLocalTemp.seatLocates0[iSeatID].vLoc
	ENDIF
	
	RETURN <<0,0,0>>
ENDFUNC

FUNC VECTOR GET_MP_JACUZZI_SEAT_LOCATE1(INT iSeatID, MP_JACUZZI_LOCAL_DATA_STRUCT &MPJacuzziLocalTemp)
	IF iSeatID > -1 AND iSeatID < JAC_MAX_NUM_SLOTS
		RETURN MPJacuzziLocalTemp.seatLocates1[iSeatID].vLoc
	ENDIF
	
	RETURN <<0,0,0>>
ENDFUNC

FUNC VECTOR GET_MP_JACUZZI_SEAT_ANIM_APPROACH(INT iSeatID, MP_JACUZZI_LOCAL_DATA_STRUCT &MPJacuzziLocalTemp)
	IF iSeatID > -1 AND iSeatID < JAC_MAX_NUM_SLOTS
		RETURN MPJacuzziLocalTemp.seatApproachPositions[iSeatID]
	ENDIF
	
	RETURN <<0,0,0>>
ENDFUNC



PROC RESERVE_JACUZZI_PROPS_FOR_CREATION()
	IF bJacuzziPropReserved = FALSE
		IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_OBJECTS()+ iNumberOfJacuzziProps,false,true)
			RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()+ iNumberOfJacuzziProps)
		
			CDEBUG1LN(DEBUG_SAFEHOUSE, "RESERVE_JACUZZI_PROPS_FOR_CREATION:", iNumberOfJacuzziProps," Props registered, GET_NUM_RESERVED_MISSION_OBJECTS: ", GET_NUM_RESERVED_MISSION_OBJECTS())	
			bJacuzziPropReserved = TRUE
		ENDIF
	ENDIF

ENDPROC

FUNC VECTOR GET_MP_YACHT_LEAN_ANIM_POSITION(INT iLeanID, MP_YACHT_LEAN_LOCAL_DATA_STRUCT &MPLeanLocalTemp)
	IF iLeanID > -1 AND iLeanID < YACHT_MAX_NUM_LEAN_SLOTS
		RETURN MPLeanLocalTemp.leanAnimPositions[iLeanID]
	ENDIF
	
	RETURN <<0,0,0>>
ENDFUNC

FUNC VECTOR GET_MP_YACHT_LEAN_ANIM_ROTATION(INT iLeanID, MP_YACHT_LEAN_LOCAL_DATA_STRUCT &MPLeanLocalTemp)
	IF iLeanID > -1 AND iLeanID < YACHT_MAX_NUM_LEAN_SLOTS
		RETURN <<0.0, 0.0, MPLeanLocalTemp.leanAnimHeadings[iLeanID]>>
	ENDIF
	
	RETURN <<0,0,0>>
ENDFUNC

FUNC VECTOR GET_MP_YACHT_LEAN_LOCATE0(INT iLeanID, MP_YACHT_LEAN_LOCAL_DATA_STRUCT &MPLeanLocalTemp)
	IF iLeanID > -1 AND iLeanID < YACHT_MAX_NUM_LEAN_SLOTS
		RETURN MPLeanLocalTemp.leanLocates0[iLeanID]
	ENDIF
	
	RETURN <<0,0,0>>
ENDFUNC

FUNC VECTOR GET_MP_YACHT_LEAN_LOCATE1(INT iLeanID, MP_YACHT_LEAN_LOCAL_DATA_STRUCT &MPLeanLocalTemp)
	IF iLeanID > -1 AND iLeanID < YACHT_MAX_NUM_LEAN_SLOTS
		RETURN MPLeanLocalTemp.leanLocates1[iLeanID]
	ENDIF
	
	RETURN <<0,0,0>>
ENDFUNC

FUNC STRING GET_JAC_SEAT_ANIM_DICT_VAR_STRING(INT iVar, BOOL bIsFemale = FALSE)
	STRING sReturnString = ""
	IF bIsFemale = FALSE
		SWITCH iVar
			CASE 0
				sReturnString = "anim@amb@yacht@jacuzzi@seated@male@variation_01@"
			BREAK
			CASE 1
				sReturnString = "anim@amb@yacht@jacuzzi@seated@male@variation_02@"
			BREAK
			CASE 2
				sReturnString = "anim@amb@yacht@jacuzzi@seated@male@variation_03@"
			BREAK
			CASE 3
				sReturnString = "anim@amb@yacht@jacuzzi@seated@male@variation_04@"
			BREAK
			CASE 4
				sReturnString = "anim@amb@yacht@jacuzzi@seated@male@variation_05@"
			BREAK
		ENDSWITCH
	ELSE
		SWITCH iVar
			CASE 0
				sReturnString = "anim@amb@yacht@jacuzzi@seated@female@variation_01@"
			BREAK
			CASE 1
				sReturnString = "anim@amb@yacht@jacuzzi@seated@female@variation_02@"
			BREAK
			CASE 2
				sReturnString = "anim@amb@yacht@jacuzzi@seated@female@variation_03@"
			BREAK
			CASE 3
				sReturnString = "anim@amb@yacht@jacuzzi@seated@female@variation_04@"
			BREAK
			CASE 4
				sReturnString = "anim@amb@yacht@jacuzzi@seated@female@variation_05@"
			BREAK
		ENDSWITCH
	ENDIF
	RETURN sReturnString
ENDFUNC

FUNC STRING GET_JAC_LEAN_ANIM_DICT_VAR_STRING(INT iVar, BOOL bIsFemale = FALSE)
	STRING sReturnString = ""
	IF bIsFemale = FALSE
		SWITCH iVar
			CASE 0
				sReturnString = "anim@amb@yacht@jacuzzi@standing@male@variation_01@"
			BREAK
			CASE 1
				sReturnString = "anim@amb@yacht@jacuzzi@standing@male@variation_02@"
			BREAK
		ENDSWITCH
	ELSE
		SWITCH iVar
			CASE 0
				sReturnString = "anim@amb@yacht@jacuzzi@standing@female@variation_01@"
			BREAK
			CASE 1
				sReturnString = "anim@amb@yacht@jacuzzi@standing@female@variation_02@"
			BREAK
		ENDSWITCH
	ENDIF
	RETURN sReturnString
ENDFUNC

FUNC STRING GET_JACUZZI_ANIM_DICT_VAR(INT iJacuzziSlot, BOOL bIsFemale = FALSE)
	STRING sReturnString
	
	IF iJacuzziSlot <= JAC_MAX_NUM_SEAT_SLOTS-1
		#IF IS_DEBUG_BUILD
			CDEBUG3LN(DEBUG_SAFEHOUSE, "GET_JACUZZI_ANIM_DICT_VAR: iJacuzziSlot = ", iJacuzziSlot, " jacuzziClientSeatData[", iJacuzziSlot, "] = ", jacuzziClientSeatData[iJacuzziSlot].iJacuzziAnimVariation, " = ", GET_JAC_SEAT_ANIM_DICT_VAR_STRING(jacuzziClientSeatData[iJacuzziSlot].iJacuzziAnimVariation))
		#ENDIF
		
		sReturnString = GET_JAC_SEAT_ANIM_DICT_VAR_STRING(jacuzziClientSeatData[iJacuzziSlot].iJacuzziAnimVariation, bIsFemale)
//		#IF IS_DEBUG_BUILD
//		IF iDebugJacuzziSittingAnimVariation != 0
//			sReturnString = GET_JAC_SEAT_ANIM_DICT_VAR_STRING(iDebugJacuzziSittingAnimVariation, bIsFemale)
//		ENDIF
//		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			CDEBUG3LN(DEBUG_SAFEHOUSE, "GET_JAC_LEAN_ANIM_DICT_VAR_STRING: iJacuzziSlot = ", iJacuzziSlot, " jacuzziClientSeatData[", iJacuzziSlot, "] = ", jacuzziClientSeatData[iJacuzziSlot].iJacuzziAnimVariation, " = ", GET_JAC_LEAN_ANIM_DICT_VAR_STRING(jacuzziClientSeatData[iJacuzziSlot].iJacuzziAnimVariation))
		#ENDIF
		
		sReturnString = GET_JAC_LEAN_ANIM_DICT_VAR_STRING(jacuzziClientSeatData[iJacuzziSlot].iJacuzziAnimVariation, bIsFemale)
//		#IF IS_DEBUG_BUILD
//		IF iDebugJacuzziSittingAnimVariation != 0
//			sReturnString = GET_JAC_LEAN_ANIM_DICT_VAR_STRING(iDebugJacuzziSittingAnimVariation, bIsFemale)
//		ENDIF
//		#ENDIF
	ENDIF
	
	
	RETURN sReturnString
ENDFUNC

FUNC BOOL DESTROY_JACUZZI_PROPS()
	INT iSeatSlot
	FOR iSeatSlot = 0 TO JAC_MAX_NUM_SLOTS-1
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.jacuzziServerSeatData[iSeatSlot].niCigar)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.jacuzziServerSeatData[iSeatSlot].niCigar)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "DESTROY_JACUZZI_PROPS: niYachtBarBB")
				
				DELETE_NET_ID(serverBD.jacuzziServerSeatData[iSeatSlot].niCigar)
				RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()- 1)
			ELSE
				TAKE_CONTROL_OF_NET_ID(serverBD.jacuzziServerSeatData[iSeatSlot].niCigar)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "DESTROY_JACUZZI_PROPS: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " doesn't have control of niYachtBarBB")
			ENDIF
		ENDIF
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.jacuzziServerSeatData[iSeatSlot].niAshtray)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.jacuzziServerSeatData[iSeatSlot].niAshtray)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.jacuzziServerSeatData[iSeatSlot].niAshtray)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "DESTROY_JACUZZI_PROPS: niYachtBedRoomBB")
					
					DELETE_NET_ID(serverBD.jacuzziServerSeatData[iSeatSlot].niAshtray)
					RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()- 1)
				ELSE
					TAKE_CONTROL_OF_NET_ID(serverBD.jacuzziServerSeatData[iSeatSlot].niAshtray)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "DESTROY_JACUZZI_PROPS: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " doesn't have control of niYachtBedRoomBB")
				ENDIF
			ENDIF
		ENDIF
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.jacuzziServerSeatData[iSeatSlot].niCup)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.jacuzziServerSeatData[iSeatSlot].niCup)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.jacuzziServerSeatData[iSeatSlot].niCup)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "DESTROY_JACUZZI_PROPS: niYachtBedRoomBB")
					
					DELETE_NET_ID(serverBD.jacuzziServerSeatData[iSeatSlot].niCup)
					RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()- 1)
				ELSE
					TAKE_CONTROL_OF_NET_ID(serverBD.jacuzziServerSeatData[iSeatSlot].niCup)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "DESTROY_JACUZZI_PROPS: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " doesn't have control of niYachtBedRoomBB")
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	FOR iSeatSlot = 0 TO JAC_MAX_NUM_SEAT_SLOTS-1
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.jacuzziServerSeatData[iSeatSlot].niCup)
		OR NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.jacuzziServerSeatData[iSeatSlot].niAshtray)
		OR NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.jacuzziServerSeatData[iSeatSlot].niCigar)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "DESTROY_JACUZZI_PROPS: a prop exists in seat: ", iSeatSlot)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "DESTROY_JACUZZI_PROPS: Not all props destroyed")
			RETURN FALSE
		ENDIF
	ENDFOR
	CDEBUG1LN(DEBUG_SAFEHOUSE, "DESTROY_JACUZZI_PROPS: All props destroyed")
	RETURN TRUE
ENDFUNC

FUNC BOOL CREATE_JACUZZI_OBJECTS()

	INT iSeatSlot
	FOR iSeatSlot = 0 TO JAC_MAX_NUM_SLOTS-1	
		IF serverBD.jacuzziServerSeatData[iSeatSlot].bCigar = TRUE
			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.jacuzziServerSeatData[iSeatSlot].niCigar)
				REQUEST_ANIM_DICT(GET_JACUZZI_ANIM_DICT_VAR(iSeatSlot))
				IF HAS_ANIM_DICT_LOADED(GET_JACUZZI_ANIM_DICT_VAR(iSeatSlot))
					CDEBUG2LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_OBJECTS: REQUESTING niCigar")
					IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("lux_Prop_Cigar_01_LUXE")))					
						IF CAN_REGISTER_MISSION_OBJECTS(1)
							IF NETWORK_IS_IN_MP_CUTSCENE()
								SET_NETWORK_CUTSCENE_ENTITIES(TRUE)
							ENDIF
							
							serverBD.jacuzziServerSeatData[iSeatSlot].vCigarPosition = GET_ANIM_INITIAL_OFFSET_POSITION( GET_JACUZZI_ANIM_DICT_VAR(iSeatSlot), "idle_d_prop_cigar", GET_MP_JACUZZI_SEAT_ANIM_POSITION(iSeatSlot, MPJacuzziLocal), GET_MP_JACUZZI_SEAT_ANIM_ROTATION(iSeatSlot, MPJacuzziLocal))  
							serverBD.jacuzziServerSeatData[iSeatSlot].vCigarRotation = GET_ANIM_INITIAL_OFFSET_ROTATION(GET_JACUZZI_ANIM_DICT_VAR(iSeatSlot), "idle_d_prop_cigar", GET_MP_JACUZZI_SEAT_ANIM_POSITION(iSeatSlot, MPJacuzziLocal), GET_MP_JACUZZI_SEAT_ANIM_ROTATION(iSeatSlot, MPJacuzziLocal))  
							
							CREATE_NET_OBJ(serverBD.jacuzziServerSeatData[iSeatSlot].niCigar, LUX_PROP_CIGAR_01_LUXE, serverBD.jacuzziServerSeatData[iSeatSlot].vCigarPosition, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE, TRUE)
							SET_ENTITY_ROTATION(NET_TO_ENT(serverBD.jacuzziServerSeatData[iSeatSlot].niCigar), serverBD.jacuzziServerSeatData[iSeatSlot].vCigarRotation)
							SET_ENTITY_INVINCIBLE(NET_TO_ENT(serverBD.jacuzziServerSeatData[iSeatSlot].niCigar), TRUE)
							FREEZE_ENTITY_POSITION(NET_TO_ENT(serverBD.jacuzziServerSeatData[iSeatSlot].niCigar), TRUE)
							SET_ENTITY_COLLISION(NET_TO_ENT(serverBD.jacuzziServerSeatData[iSeatSlot].niCigar), FALSE)
							
							IF NETWORK_IS_IN_MP_CUTSCENE()
								SET_NETWORK_CUTSCENE_ENTITIES(FALSE)
							ENDIF
							
							CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_OBJECTS: CREATING niCigar, position: ", serverBD.jacuzziServerSeatData[iSeatSlot].vCigarPosition, ", for iSeatSlot: ", iSeatSlot, " animDict Var: ", jacuzziClientSeatData[iSeatSlot].iJacuzziAnimVariation, " anim = ", GET_JACUZZI_ANIM_DICT_VAR(iSeatSlot))
						ELSE
							CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_OBJECTS: niCigar - CAN_REGISTER_MISSION_ENTITIES = FALSE")
			    		ENDIF

					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_OBJECTS: lux_Prop_Cigar_01_LUXE not Loaded yet")
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_OBJECTS: anim dict not loaded yet: ", GET_JACUZZI_ANIM_DICT_VAR(iSeatSlot, IS_PLAYER_FEMALE()))
					RETURN FALSE
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_OBJECTS: Cigar already created for seat slot : ", iSeatSlot)
			ENDIF
			
			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.jacuzziServerSeatData[iSeatSlot].niAshtray)
				REQUEST_ANIM_DICT(GET_JACUZZI_ANIM_DICT_VAR(iSeatSlot))
				IF HAS_ANIM_DICT_LOADED(GET_JACUZZI_ANIM_DICT_VAR(iSeatSlot))
					CDEBUG2LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_OBJECTS: REQUESTING niAshtray")
					IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("lux_prop_ashtray_LUXE_01")))					
						IF CAN_REGISTER_MISSION_OBJECTS(1)
							IF NETWORK_IS_IN_MP_CUTSCENE()
								SET_NETWORK_CUTSCENE_ENTITIES(TRUE)
							ENDIF
							
							VECTOR vPropPos = GET_ANIM_INITIAL_OFFSET_POSITION( GET_JACUZZI_ANIM_DICT_VAR(iSeatSlot), "idle_d_prop_ashtray", GET_MP_JACUZZI_SEAT_ANIM_POSITION(iSeatSlot, MPJacuzziLocal), GET_MP_JACUZZI_SEAT_ANIM_ROTATION(iSeatSlot, MPJacuzziLocal))  
							VECTOR vTempRotation = GET_ANIM_INITIAL_OFFSET_ROTATION(GET_JACUZZI_ANIM_DICT_VAR(iSeatSlot), "idle_d_prop_ashtray", GET_MP_JACUZZI_SEAT_ANIM_POSITION(iSeatSlot, MPJacuzziLocal), GET_MP_JACUZZI_SEAT_ANIM_ROTATION(iSeatSlot, MPJacuzziLocal))  
							CREATE_NET_OBJ(serverBD.jacuzziServerSeatData[iSeatSlot].niAshtray, LUX_PROP_ASHTRAY_LUXE_01, vPropPos, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE, TRUE)
							SET_ENTITY_ROTATION(NET_TO_ENT(serverBD.jacuzziServerSeatData[iSeatSlot].niAshtray), vTempRotation )
							SET_ENTITY_INVINCIBLE(NET_TO_ENT(serverBD.jacuzziServerSeatData[iSeatSlot].niAshtray), TRUE)
							FREEZE_ENTITY_POSITION(NET_TO_ENT(serverBD.jacuzziServerSeatData[iSeatSlot].niAshtray), TRUE)
							SET_ENTITY_COLLISION(NET_TO_ENT(serverBD.jacuzziServerSeatData[iSeatSlot].niAshtray), FALSE)
							
							IF NETWORK_IS_IN_MP_CUTSCENE()
								SET_NETWORK_CUTSCENE_ENTITIES(FALSE)
							ENDIF
							
							CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_OBJECTS: CREATING niAshtray, position: ", vPropPos, " animDict Var: ", jacuzziClientSeatData[iSeatSlot].iJacuzziAnimVariation, " anim = ", GET_JACUZZI_ANIM_DICT_VAR(iSeatSlot))
						ELSE
							CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_OBJECTS: niAshtray - CAN_REGISTER_MISSION_ENTITIES = FALSE")
			    		ENDIF

					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_OBJECTS: lux_prop_ashtray_LUXE_01 not Loaded yet")
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_OBJECTS: anim dict not loaded yet: ", GET_JACUZZI_ANIM_DICT_VAR(iSeatSlot, IS_PLAYER_FEMALE()))
					RETURN FALSE
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_OBJECTS: Ashtray already created for seat slot : ", iSeatSlot)
			ENDIF
		ELSE
			CDEBUG3LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_OBJECTS: no cigar or ashtray needed for iSlot: ", iSeatSlot)
		ENDIF
			
		IF serverBD.jacuzziServerSeatData[iSeatSlot].bCup = TRUE
			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.jacuzziServerSeatData[iSeatSlot].niCup)
				REQUEST_ANIM_DICT(GET_JACUZZI_ANIM_DICT_VAR(iSeatSlot))
				IF HAS_ANIM_DICT_LOADED(GET_JACUZZI_ANIM_DICT_VAR(iSeatSlot))
					CDEBUG2LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_OBJECTS: REQUESTING niCup")
					IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("apa_Prop_cs_Plastic_cup_01")))					
						IF CAN_REGISTER_MISSION_OBJECTS(1)
							IF NETWORK_IS_IN_MP_CUTSCENE()
								SET_NETWORK_CUTSCENE_ENTITIES(TRUE)
							ENDIF
							
							serverBD.jacuzziServerSeatData[iSeatSlot].vCupPosition = GET_ANIM_INITIAL_OFFSET_POSITION( GET_JACUZZI_ANIM_DICT_VAR(iSeatSlot), "idle_e_prop_cup", GET_MP_JACUZZI_SEAT_ANIM_POSITION(iSeatSlot, MPJacuzziLocal), GET_MP_JACUZZI_SEAT_ANIM_ROTATION(iSeatSlot, MPJacuzziLocal))  
							serverBD.jacuzziServerSeatData[iSeatSlot].vCupRotation = GET_ANIM_INITIAL_OFFSET_ROTATION(GET_JACUZZI_ANIM_DICT_VAR(iSeatSlot), "idle_e_prop_cup", GET_MP_JACUZZI_SEAT_ANIM_POSITION(iSeatSlot, MPJacuzziLocal), GET_MP_JACUZZI_SEAT_ANIM_ROTATION(iSeatSlot, MPJacuzziLocal))  
							
							CREATE_NET_OBJ(serverBD.jacuzziServerSeatData[iSeatSlot].niCup, INT_TO_ENUM(MODEL_NAMES, HASH("apa_Prop_cs_Plastic_cup_01")), serverBD.jacuzziServerSeatData[iSeatSlot].vCupPosition, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE, TRUE)
							SET_ENTITY_ROTATION(NET_TO_ENT(serverBD.jacuzziServerSeatData[iSeatSlot].niCup), serverBD.jacuzziServerSeatData[iSeatSlot].vCupRotation)
							SET_ENTITY_INVINCIBLE(NET_TO_ENT(serverBD.jacuzziServerSeatData[iSeatSlot].niCup), TRUE)
							FREEZE_ENTITY_POSITION(NET_TO_ENT(serverBD.jacuzziServerSeatData[iSeatSlot].niCup), TRUE)
							SET_ENTITY_COLLISION(NET_TO_ENT(serverBD.jacuzziServerSeatData[iSeatSlot].niCup), FALSE)
							
							IF NETWORK_IS_IN_MP_CUTSCENE()
								SET_NETWORK_CUTSCENE_ENTITIES(FALSE)
							ENDIF
							
							CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_OBJECTS: CREATING niCup, position: ", serverBD.jacuzziServerSeatData[iSeatSlot].vCupPosition, ", for iSeatSlot:  ", iSeatSlot , " animDict Var: ", jacuzziClientSeatData[iSeatSlot].iJacuzziAnimVariation, " anim = ", GET_JACUZZI_ANIM_DICT_VAR(iSeatSlot))
						ELSE
							CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_OBJECTS: cup - CAN_REGISTER_MISSION_ENTITIES = FALSE")
			    		ENDIF

					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_OBJECTS: apa_Prop_cs_Plastic_cup_01 not Loaded yet")
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_OBJECTS: anim dict not loaded yet: ", GET_JACUZZI_ANIM_DICT_VAR(iSeatSlot, IS_PLAYER_FEMALE()))
					RETURN FALSE
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_OBJECTS: Cup already created for seat slot : ", iSeatSlot)
			ENDIF
		ELSE
			CDEBUG3LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_OBJECTS: no cup needed for iSlot: ", iSeatSlot)
		ENDIF
	ENDFOR
	iSeatSlot = 0
	FOR iSeatSlot = 0 TO JAC_MAX_NUM_SLOTS-1
		IF serverBD.jacuzziServerSeatData[iSeatSlot].bCigar = TRUE
			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.jacuzziServerSeatData[iSeatSlot].niCigar)
			OR NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.jacuzziServerSeatData[iSeatSlot].niAshtray)
				#IF IS_DEBUG_BUILD
					IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.jacuzziServerSeatData[iSeatSlot].niCigar)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_OBJECTS: Cigar prop missing for seatSlot: ", iSeatSlot)
					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_OBJECTS: Cigar prop created for seatSlot: ", iSeatSlot)
					ENDIF
					IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.jacuzziServerSeatData[iSeatSlot].niAshtray)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_OBJECTS: Ashtray prop missing for seatSlot: ", iSeatSlot)
					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_OBJECTS: Ashtray prop created for seatSlot: ", iSeatSlot)
					ENDIF
				#ENDIF
				CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_OBJECTS: NOT ALL PROPS CREATED")
				RETURN FALSE
			ENDIF
		ENDIF
		IF serverBD.jacuzziServerSeatData[iSeatSlot].bCup = TRUE
			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.jacuzziServerSeatData[iSeatSlot].niCup)
				#IF IS_DEBUG_BUILD
					IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.jacuzziServerSeatData[iSeatSlot].niCup)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_OBJECTS: niCup prop missing for seatSlot: ", iSeatSlot)
					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_OBJECTS: niCup prop created for seatSlot: ", iSeatSlot)
					ENDIF
				#ENDIF
				CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_OBJECTS: NOT ALL PROPS CREATED")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDFOR
	CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_OBJECTS: ALL PROPS CREATED")
	RETURN TRUE
ENDFUNC


FUNC BOOL CREATE_JACUZZI_PROPS()
	IF bJacuzziPropReserved = TRUE
		IF CREATE_JACUZZI_OBJECTS()
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ELSE
		RESERVE_JACUZZI_PROPS_FOR_CREATION()
	ENDIF
	RETURN FALSE
ENDFUNC



	
FUNC BOOL INIT_SERVER_JACUZZI_ACTIVITY()
	IF NOT IS_JACUZZI_ACTIVITY_DISABLED()
		IF NOT IS_BIT_SET(iJacuzziBitSet, JAC_BS_SERVER_DATA_GENERATED)
			GENERATE_JACUZZI_DATA()
			STORE_JACUZZI_POSITIONS()
			GET_SERVER_JACUZZI_SEAT_ANIM_VARIATION_DATA() 
			SET_BIT(iJacuzziBitSet, JAC_BS_SERVER_DATA_GENERATED)
		ENDIF
		IF IS_BIT_SET(iJacuzziBitSet, JAC_BS_SERVER_DATA_GENERATED)
			IF CREATE_JACUZZI_PROPS()	
				RETURN TRUE
			ENDIF
		ENDIF
		RETURN FALSE
	ELSE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC
FUNC STRING GET_IDLE_SEAT_CLIP(INT iVar)
	STRING sReturnString = ""

	SWITCH iVar
		CASE 0
			sReturnString = "idle_a"
		BREAK
		CASE 1
			sReturnString = "idle_b"
		BREAK
		CASE 2
			sReturnString = "idle_c"
		BREAK
		CASE 3
			sReturnString = "idle_d"
		BREAK
		CASE 4
			sReturnString = "idle_e"
		BREAK
	ENDSWITCH

	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_IDLE_SEAT_CLIP: returning: ", sReturnString)
	RETURN sReturnString
ENDFUNC


PROC REQUEST_JAC_ANIM_DICT(INT iJacuzziSlot, BOOL bIsFemale = FALSE)
	STRING sAnimRequst
	IF iJacuzziSlot <= JAC_MAX_NUM_SEAT_SLOTS-1
		#IF IS_DEBUG_BUILD
			CDEBUG2LN(DEBUG_SAFEHOUSE, "GET_JACUZZI_ANIM_DICT_VAR: iJacuzziSlot = ", iJacuzziSlot, " jacuzziClientSeatData[", iJacuzziSlot, "] = ", jacuzziClientSeatData[iJacuzziSlot].iJacuzziAnimVariation, " = ", GET_JAC_SEAT_ANIM_DICT_VAR_STRING(jacuzziClientSeatData[iJacuzziSlot].iJacuzziAnimVariation))
		#ENDIF
		
		sAnimRequst = GET_JAC_SEAT_ANIM_DICT_VAR_STRING(jacuzziClientSeatData[iJacuzziSlot].iJacuzziAnimVariation, bIsFemale)
		#IF IS_DEBUG_BUILD
		IF iDebugJacuzziSittingAnimVariation != 0
			sAnimRequst = GET_JAC_SEAT_ANIM_DICT_VAR_STRING(iDebugJacuzziSittingAnimVariation, bIsFemale)
		ENDIF
		#ENDIF
		REQUEST_ANIM_DICT(sAnimRequst)
	ELSE
		#IF IS_DEBUG_BUILD
			CDEBUG2LN(DEBUG_SAFEHOUSE, "GET_JAC_LEAN_ANIM_DICT_VAR_STRING: iJacuzziSlot = ", iJacuzziSlot, " jacuzziClientSeatData[", iJacuzziSlot, "] = ", jacuzziClientSeatData[iJacuzziSlot].iJacuzziAnimVariation, " = ", GET_JAC_LEAN_ANIM_DICT_VAR_STRING(jacuzziClientSeatData[iJacuzziSlot].iJacuzziAnimVariation))
		#ENDIF
		
		sAnimRequst = GET_JAC_LEAN_ANIM_DICT_VAR_STRING(jacuzziClientSeatData[iJacuzziSlot].iJacuzziAnimVariation, bIsFemale)
		#IF IS_DEBUG_BUILD
		IF iDebugJacuzziSittingAnimVariation != 0
			sAnimRequst = GET_JAC_LEAN_ANIM_DICT_VAR_STRING(iDebugJacuzziSittingAnimVariation, bIsFemale)
		ENDIF
		#ENDIF
		REQUEST_ANIM_DICT(sAnimRequst)
	ENDIF
ENDPROC


FUNC STRING GET_LEAN_ANIM_DICT_VAR(BOOL bIsFemale)
	STRING sReturnString
	IF iStaggerLeanActLoop != iBowIndex
		IF bIsFemale	
			sReturnString = "anim@amb@yacht@rail@standing@female@variant_01@"
		ELSE
			sReturnString = "anim@amb@yacht@rail@standing@male@variant_01@"
		ENDIF	
	ELSE
		IF bIsFemale	
			sReturnString = "anim@amb@yacht@bow@female@variation_01@"
		ELSE
			sReturnString = "anim@amb@yacht@bow@male@variation_01@"
		ENDIF
	ENDIF
	
	RETURN sReturnString
ENDFUNC

PROC REQUEST_LEAN_ANIM_DICT(BOOL bIsFemale = FALSE)


	REQUEST_ANIM_DICT(GET_LEAN_ANIM_DICT_VAR(bIsFemale))
	
ENDPROC




FUNC STRING GET_BASE_CLIP_STRING_FROM_INT(INT iVar)
	STRING sReturnString
	SWITCH iVar
		CASE 0
			sReturnString = "base_a"
		BREAK
		CASE 1
			sReturnString = "base_b"
		BREAK
		CASE 2
			sReturnString = "base_c"
		BREAK
	ENDSWITCH
	RETURN sReturnString
ENDFUNC


FUNC STRING GENERATE_RANDOM_YACHT_LEAN_IDLE_CLIP()
	INT iClipVar
	INT iMaxClip = 3
	
	IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
		IF eChosenYachtLeanSlotActivity = YACHT_LEAN_SLOT_1_ENUM
		OR eChosenYachtLeanSlotActivity = YACHT_LEAN_SLOT_2_ENUM
		OR eChosenYachtLeanSlotActivity = YACHT_LEAN_SLOT_3_ENUM
			// The rail on these 3 locations is thicker so idle_c for females cannot be played without bad clipping
			iMaxClip = 2
			CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_RANDOM_YACHT_LEAN_IDLE_CLIP: being female on rails 1-3 - no idle_c")
		ENDIF
	ENDIF
	
	iClipVar = GET_RANDOM_INT_IN_RANGE(0, iMaxClip)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_RANDOM_YACHT_LEAN_IDLE_CLIP: iLastYachtLeanIdleClipVar = ", iLastYachtLeanIdleClipVar)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_RANDOM_YACHT_LEAN_IDLE_CLIP: iSecondLastYachtLeanIdleClipVar = ", iSecondLastYachtLeanIdleClipVar)
	WHILE 
		(iClipVar < 0 OR iClipVar > (iMaxClip - 1))
		OR (iClipVar = iLastYachtLeanIdleClipVar OR (iMaxClip > 2 AND iClipVar = iSecondLastYachtLeanIdleClipVar))
		CDEBUG1LN(DEBUG_SAFEHOUSE, "iClip was unsuitable: ", iClipVar, ", so regenerating")
		iClipVar = GET_RANDOM_INT_IN_RANGE(0, iMaxClip)
	ENDWHILE
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_RANDOM_YACHT_LEAN_IDLE_CLIP: iClipVar = ", iClipVar, ", = ", GET_IDLE_SEAT_CLIP(iClipVar))
		
	iSecondLastYachtLeanIdleClipVar = iLastYachtLeanIdleClipVar
	iLastYachtLeanIdleClipVar = iClipVar
	RETURN GET_IDLE_SEAT_CLIP(iClipVar)
ENDFUNC

FUNC INT GENERATE_RANDOM_JACUZZI_BASE_CLIP()
	INT iClipVar 
	iClipVar = GET_RANDOM_INT_IN_RANGE(0, 3)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_RANDOM_JACUZZI_BASE_CLIP: iLastJacuzzibaseClipVar = ", iLastJacuzzibaseClipVar)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_RANDOM_JACUZZI_BASE_CLIP: iSecondLastJacuzzibaseClipVar = ", iSecondLastJacuzzibaseClipVar)
	WHILE (iClipVar < 0
		OR iClipVar > 2)
	OR(iClipVar = iLastJacuzzibaseClipVar
		OR iClipVar = iSecondLastJacuzzibaseClipVar)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "iClip was unsuitable: ", iClipVar, ", so regenerating")
		iClipVar = GET_RANDOM_INT_IN_RANGE(0, 3)
	ENDWHILE
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_RANDOM_JACUZZI_base_CLIP: iClipVar = ", iClipVar, ", = ", GET_BASE_CLIP_STRING_FROM_INT(iClipVar))
		
	iSecondLastJacuzzibaseClipVar = iLastJacuzzibaseClipVar
	iLastJacuzzibaseClipVar = iClipVar
	RETURN iClipVar 
ENDFUNC

PROC CREATE_JACUZZI_BASE_ANIM_SEQUENCE()
	FLOAT fAnimClipTotalTime 
	INT iRandomBase 
	iRandomBase = GENERATE_RANDOM_JACUZZI_BASE_CLIP()
	fAnimClipTotalTime = GET_ANIM_DURATION(sJaccuzziAnimDict, GET_BASE_CLIP_STRING_FROM_INT(iRandomBase))
	CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_BASE_ANIM_SEQUENCE: fAnimClipTotalTime = ", fAnimClipTotalTime)
	FLOAT fAnimRandomTime 
	fAnimRandomTime = GET_RANDOM_FLOAT_IN_RANGE(8, 10) // we want to play the base anim between 8 and 12 seconds
	CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_BASE_ANIM_SEQUENCE: fAnimRandomTime = ", fAnimRandomTime)
	
	fBaseAnimPhaseDuration = fAnimRandomTime/fAnimClipTotalTime
	CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_BASE_ANIM_SEQUENCE: fBaseAnimPhaseDuration = ", fBaseAnimPhaseDuration)
	IF fBaseAnimPhaseDuration > 1.0
		WHILE fBaseAnimPhaseDuration > 1.0
		AND iBaseClipSeqCount < MAX_BASE_CLIP_SEQUENCE
			iBaseClipSequence[iBaseClipSeqCount] = iRandomBase
			CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_BASE_ANIM_SEQUENCE: WHILE: iBaseClipSequence[", iBaseClipSeqCount, "] = ", iRandomBase)
			iBaseClipSeqCount++
			fAnimRandomTime = fAnimRandomTime - fAnimClipTotalTime // how much time is left to play
			CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_BASE_ANIM_SEQUENCE: WHILE: fAnimRandomTime = ", fAnimRandomTime)
			
			iRandomBase = GENERATE_RANDOM_JACUZZI_BASE_CLIP()
			fAnimClipTotalTime = GET_ANIM_DURATION(sJaccuzziAnimDict, GET_BASE_CLIP_STRING_FROM_INT(iRandomBase))
			CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_BASE_ANIM_SEQUENCE: WHILE: fAnimClipTotalTime = ", fAnimClipTotalTime)
			
			fBaseAnimPhaseDuration = fAnimRandomTime/fAnimClipTotalTime
			CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_BASE_ANIM_SEQUENCE: WHILE: fBaseAnimPhaseDuration  = ", fBaseAnimPhaseDuration)
			IF fBaseAnimPhaseDuration < 1.0
				IF iBaseClipSeqCount < MAX_BASE_CLIP_SEQUENCE
					iBaseClipSequence[iBaseClipSeqCount] = iRandomBase
					CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_BASE_ANIM_SEQUENCE: WHILE last entry: iBaseClipSequence[", iBaseClipSeqCount, "] = ", iRandomBase)
					iBaseClipSeqCount++
				ELSE
					CWARNINGLN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_BASE_ANIM_SEQUENCE: WHILE last entry is too much aborting")
				ENDIF
			ENDIF
		ENDWHILE
	ELSE
		iBaseClipSequence[iBaseClipSeqCount] = iRandomBase
		CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_BASE_ANIM_SEQUENCE: WHILE: iBaseClipSequence[", iBaseClipSeqCount, "] = ", iRandomBase)
		iBaseClipSeqCount++
	ENDIF
	
	#IF IS_DEBUG_BUILD
	INT iSequenceCount
	REPEAT iBaseClipSeqCount iSequenceCount
		CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_JACUZZI_BASE_ANIM_SEQUENCE: --: iBaseClipSequence[", iSequenceCount, "] = ", iBaseClipSequence[iSequenceCount], " = ", GET_BASE_CLIP_STRING_FROM_INT(iBaseClipSequence[iSequenceCount]))
	ENDREPEAT
	#ENDIF
ENDPROC

// Checks to see if the clip to be played is a cup or cigar anim and weather it is appropriate for that player to play it
FUNC BOOL SHOULD_PLAYER_PLAY_ANIM_CLIP(INT iJacuzziSlot, INT iClipVar) 
	SWITCH iClipVar
		CASE 0 // a
		CASE 1 // b
		CASE 2 // c
			CDEBUG1LN(DEBUG_SAFEHOUSE, "SHOULD_PLAYER_PLAY_ANIM_CLIP: ", iClipVar, " return true")
			RETURN TRUE // anyone can play these
		BREAK
		
		CASE 3 // d
			IF serverBD.jacuzziServerSeatData[iJacuzziSlot].bCigar
				IF ARE_VECTORS_ALMOST_EQUAL( GET_ENTITY_COORDS(NET_TO_ENT(serverBD.jacuzziServerSeatData[iJacuzziSlot].niCigar)), serverBD.jacuzziServerSeatData[iJacuzziSlot].vCigarPosition, 0.1)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "SHOULD_PLAYER_PLAY_ANIM_CLIP: ", iClipVar, " is d Clip and slot is registered to play cigar, returning true")
					RETURN TRUE
				ELSE
					CWARNINGLN(DEBUG_SAFEHOUSE, "SHOULD_PLAYER_PLAY_ANIM_CLIP: ", iClipVar, " is d Clip and slot is registered to play cigar, but cigar is not in the right position returning false")
					#IF IS_DEBUG_BUILD
						VECTOR vTempCupShouldbe 
						vTempCupShouldbe = GET_ENTITY_COORDS(NET_TO_ENT(serverBD.jacuzziServerSeatData[iJacuzziSlot].niCigar))

						CWARNINGLN(DEBUG_SAFEHOUSE, "SHOULD_PLAYER_PLAY_ANIM_CLIP: Cup is at ", vTempCupShouldbe, " cup was created at ", serverBD.jacuzziServerSeatData[iJacuzziSlot].vCigarPosition)
					#ENDIF
					
					RETURN FALSE
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "SHOULD_PLAYER_PLAY_ANIM_CLIP: ", iClipVar, " is d Clip and slot is NOT registered to play cigar, returning FALSE")
				RETURN FALSE
			ENDIF
		BREAK
		CASE 4 // e
			IF serverBD.jacuzziServerSeatData[iJacuzziSlot].bCup
				IF ARE_VECTORS_ALMOST_EQUAL( GET_ENTITY_COORDS(NET_TO_ENT(serverBD.jacuzziServerSeatData[iJacuzziSlot].niCup)), serverBD.jacuzziServerSeatData[iJacuzziSlot].vCupPosition, 0.05)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "SHOULD_PLAYER_PLAY_ANIM_CLIP: ", iClipVar, " is e bCup and slot is registered to play cup, returning true")
					RETURN TRUE
				ELSE
					CWARNINGLN(DEBUG_SAFEHOUSE, "SHOULD_PLAYER_PLAY_ANIM_CLIP: ", iClipVar, " is e Clip and slot is registered to play cup, but cup is not in the right position returning false")
					#IF IS_DEBUG_BUILD
						VECTOR vTempCupShouldbe 
						vTempCupShouldbe = GET_ENTITY_COORDS(NET_TO_ENT(serverBD.jacuzziServerSeatData[iJacuzziSlot].niCup))

						CWARNINGLN(DEBUG_SAFEHOUSE, "SHOULD_PLAYER_PLAY_ANIM_CLIP: Cup is at ", vTempCupShouldbe, " cup was created at ", serverBD.jacuzziServerSeatData[iJacuzziSlot].vCupPosition)
					#ENDIF
					
					RETURN FALSE
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "SHOULD_PLAYER_PLAY_ANIM_CLIP: ", iClipVar, " is e bCup and slot is NOT registered to play cup, returning FALSE")
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN TRUE
ENDFUNC

FUNC INT GENERATE_RANDOM_JACUZZI_IDLE_CLIP()
	
	INT iClipVar 
	iClipVar = GET_RANDOM_INT_IN_RANGE(0, 5)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_RANDOM_JACUZZI_IDLE_CLIP: iLastJacuzziIdleClipVar = ", iLastJacuzziIdleClipVar)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_RANDOM_JACUZZI_IDLE_CLIP: iSecondLastJacuzziIdleClipVar = ", iSecondLastJacuzziIdleClipVar)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_RANDOM_JACUZZI_IDLE_CLIP: iPlayerChosenSeatSlot = ", iPlayerChosenSeatSlot)
	IF NOT bForceDrinkSmokeActivity
		WHILE (iClipVar < 0
			OR iClipVar > 4)
		OR(iClipVar = iLastJacuzziIdleClipVar
			OR iClipVar = iSecondLastJacuzziIdleClipVar
			OR SHOULD_PLAYER_PLAY_ANIM_CLIP(iPlayerChosenSeatSlot, iClipVar) = FALSE)
			
			CDEBUG1LN(DEBUG_SAFEHOUSE, "iClip was unsuitable: ", iClipVar, ", so regenerating")
			iClipVar = GET_RANDOM_INT_IN_RANGE(0, 5)
		ENDWHILE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_RANDOM_JACUZZI_IDLE_CLIP: iClipVar = ", iClipVar)
		iSecondLastJacuzziIdleClipVar = iLastJacuzziIdleClipVar
		iLastJacuzziIdleClipVar = iClipVar
	ELSE
		IF NOT HAS_NET_TIMER_EXPIRED(ntSmokeDrinkForceTimer, iForceDrinkSmokeTimeout, TRUE)
		OR (NOT SHOULD_PLAYER_PLAY_ANIM_CLIP(iPlayerChosenSeatSlot, 3) 
		AND NOT SHOULD_PLAYER_PLAY_ANIM_CLIP(iPlayerChosenSeatSlot, 4))
		
			IF NOT SHOULD_PLAYER_PLAY_ANIM_CLIP(iPlayerChosenSeatSlot, 3) 
			AND NOT SHOULD_PLAYER_PLAY_ANIM_CLIP(iPlayerChosenSeatSlot, 4)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_RANDOM_JACUZZI_IDLE_CLIP: slot isn't suitable for cigar or cup anim clip")
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_RANDOM_JACUZZI_IDLE_CLIP: iForceDrinkSmokeTimeout = True, timer hasn't expired though")	
			ENDIF
			
			WHILE (iClipVar < 0
				OR iClipVar > 4)
			OR(iClipVar = iLastJacuzziIdleClipVar
				OR iClipVar = iSecondLastJacuzziIdleClipVar
				OR iClipVar = 3
				OR iClipVar = 4) // if we're going to force the clip to play a drink/smoke when the timer expires, lets force them to play something else if the timer hasn't expired
				
				CDEBUG1LN(DEBUG_SAFEHOUSE, "iClip was unsuitable: ", iClipVar, ", so regenerating")
				iClipVar = GET_RANDOM_INT_IN_RANGE(0, 5)
			ENDWHILE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_RANDOM_JACUZZI_IDLE_CLIP: iClipVar = ", iClipVar)
			iSecondLastJacuzziIdleClipVar = iLastJacuzziIdleClipVar
			iLastJacuzziIdleClipVar = iClipVar
		ELSE

			REINIT_NET_TIMER(ntSmokeDrinkForceTimer, TRUE)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_RANDOM_JACUZZI_IDLE_CLIP: iForceDrinkSmokeTimeout = True, timer has expired")
			CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_RANDOM_JACUZZI_IDLE_CLIP: iForceDrinkSmokeTimeout reset")
			IF SHOULD_PLAYER_PLAY_ANIM_CLIP(iPlayerChosenSeatSlot, 3)
				iClipVar = 3
			ELIF SHOULD_PLAYER_PLAY_ANIM_CLIP(iPlayerChosenSeatSlot, 4)
				iClipVar = 4
			ENDIF
			CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_RANDOM_JACUZZI_IDLE_CLIP: iForceDrinkSmokeTimeout = True, iClipVar = ", iClipVar)
		ENDIF
	ENDIF
	RETURN iClipVar 
ENDFUNC

FUNC STRING GET_JACUZZI_IDLE_ANIM_CLIP()
//	IF iStaggerJacuzziSeatLoop <= JAC_MAX_NUM_SEAT_SLOTS-1
//		RETURN GET_IDLE_SEAT_CLIP(GENERATE_RANDOM_JACUZZI_IDLE_CLIP())
//	ELSE
		RETURN GET_IDLE_SEAT_CLIP(GENERATE_RANDOM_JACUZZI_IDLE_CLIP())
//	ENDIF
ENDFUNC 

PROC CREATE_RANDOM_BASE_PHASE_DURATION()
	FLOAT fAnimTotalTime 
	fAnimTotalTime = GET_ANIM_DURATION(sJaccuzziAnimDict, "base")																																										
	CDEBUG1LN(DEBUG_SAFEHOUSE, "JAC_CHECK_ENTER_OR_IDLE_ANIM: fAnimTotalTime = ", fAnimTotalTime)
	FLOAT fAnimRandomTime 
	fAnimRandomTime = GET_RANDOM_FLOAT_IN_RANGE(8, 12) // we want to play the base anim between 8 and 12 seconds
	CDEBUG1LN(DEBUG_SAFEHOUSE, "JAC_CHECK_ENTER_OR_IDLE_ANIM: fAnimRandomTime = ", fAnimRandomTime)
	fBaseAnimPhaseDuration = fAnimRandomTime/fAnimTotalTime
	CDEBUG1LN(DEBUG_SAFEHOUSE, "JAC_CHECK_ENTER_OR_IDLE_ANIM: fBaseAnimPhaseDuration = ", fBaseAnimPhaseDuration)
ENDPROC

PROC SUPPRESS_JAC_PHONE_HUD_AND_ACTION_LOGIC()
	SWITCH jacActState			
		CASE JAC_PROMPT								
		CASE JAC_REQUEST
		CASE JAC_LOCATECHECK
			IF IS_BIT_SET(iJacuzziBitSet, JAC_BS_PLAYER_INSIDE_INNER_JAC_SPHERE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
				CDEBUG2LN(DEBUG_SAFEHOUSE, "JAC_BS_PLAYER_INSIDE_INNER_JAC_SPHERE: SET_THIRD_PERSON_CAM_RELATIVE_PITCH_LIMITS_THIS_UPDATE")
			ENDIF
		BREAK
		CASE JAC_RUN_ENTER_ANIM  
		CASE JAC_CHECK_ENTER_OR_IDLE_ANIM           
		CASE JAC_CHECK_BASE_ANIM 
		CASE JAC_APPROACH                         	 
			HUD_FORCE_WEAPON_WHEEL(FALSE)
			DISPLAY_AMMO_THIS_FRAME(FALSE)			
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
			SET_CLEAR_ON_CALL_HUD_THIS_FRAME()
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			DISABLE_SELECTOR_THIS_FRAME()
		//	HIDE_HUD_AND_RADAR_THIS_FRAME() // want to see that we are regenerating health?
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_UNARMED, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_MELEE, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_HANDGUN, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SHOTGUN, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SMG, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_AUTO_RIFLE, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SNIPER, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_HEAVY, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SPECIAL, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_NEXT_WEAPON, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_PREV_WEAPON, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PICKUP, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_ATTACK, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_NEXT_WEAPON, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, TRUE)

			WEAPON_TYPE eCurrentWeapon
			IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), eCurrentWeapon)
			    IF eCurrentWeapon != WEAPONTYPE_OBJECT
			          PRINTLN("Holstering weapon ", eCurrentWeapon)
			          SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED,TRUE)
			          
			          IF (GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON)
			                FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), FALSE, FALSE)
			          ENDIF
			    ENDIF
			ENDIF
		
		BREAK                       	

	ENDSWITCH
	
	

	
ENDPROC

PROC SUPPRESS_CAMERA_SWITCH()
	IF IS_BIT_SET(iJacuzziBitSet, JAC_BS_PREVENT_FIRST_PERSON_MODE)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
		CLEAR_BIT(iJacuzziBitSet, JAC_BS_PREVENT_FIRST_PERSON_MODE)
	ENDIF
	SWITCH jacActState			
										
		CASE JAC_REQUEST
		CASE JAC_RUN_ENTER_ANIM  
		CASE JAC_CHECK_ENTER_OR_IDLE_ANIM           
		CASE JAC_CHECK_BASE_ANIM 
		CASE JAC_APPROACH  		
			IF NOT IS_BIT_SET(iJacuzziBitSet, JAC_BS_PREVENT_FIRST_PERSON_MODE)
			
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
				SET_BIT(iJacuzziBitSet, JAC_BS_PREVENT_FIRST_PERSON_MODE)
			ENDIF
		BREAK                       	

	ENDSWITCH	
ENDPROC
PROC SUPPRESS_LEAN_PHONE_HUD_AND_ACTION_LOGIC()
	SWITCH yachtLeanActState			
		CASE LEAN_PROMPT								
		CASE LEAN_REQUEST
		CASE LEAN_LOCATECHECK
			IF IS_BIT_SET(iJacuzziBitSet, JAC_BS_PLAYER_INSIDE_INNER_JAC_SPHERE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
				CDEBUG2LN(DEBUG_SAFEHOUSE, "JAC_BS_PLAYER_INSIDE_INNER_JAC_SPHERE: SET_THIRD_PERSON_CAM_RELATIVE_PITCH_LIMITS_THIS_UPDATE")
			ENDIF
		BREAK
		CASE LEAN_RUN_ENTER_ANIM  
		CASE LEAN_CHECK_ENTER_OR_IDLE_ANIM           
		CASE LEAN_CHECK_BASE_ANIM 
		CASE LEAN_APPROACH                         	 
			HUD_FORCE_WEAPON_WHEEL(FALSE)
			DISPLAY_AMMO_THIS_FRAME(FALSE)			
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
			SET_CLEAR_ON_CALL_HUD_THIS_FRAME()
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			DISABLE_SELECTOR_THIS_FRAME()
			DISABLE_INTERACTION_MENU()
		//	HIDE_HUD_AND_RADAR_THIS_FRAME() // want to see that we are regenerating health?
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_UNARMED, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_MELEE, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_HANDGUN, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SHOTGUN, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SMG, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_AUTO_RIFLE, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SNIPER, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_HEAVY, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SPECIAL, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_NEXT_WEAPON, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_PREV_WEAPON, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PICKUP, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_ATTACK, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_NEXT_WEAPON, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK, FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, TRUE)

			WEAPON_TYPE eCurrentWeapon
			IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), eCurrentWeapon)
			    IF eCurrentWeapon != WEAPONTYPE_OBJECT
			          PRINTLN("Holstering weapon ", eCurrentWeapon)
			          SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED,TRUE)
			          
			          IF (GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON)
			                FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), FALSE, FALSE)
			          ENDIF
			    ENDIF
			ENDIF
		
		BREAK                       	

	ENDSWITCH
	
	

	
ENDPROC


/*
FUNC ACTIVITY_ENUMS GET_ACTIVITY_SLOT_FOR_JACUZZI_SEAT(INT seatID)
	SWITCH (seatID)
		CASE 0
			RETURN JAC_SEAT_SLOT_1_ENUM
		BREAK
		CASE 1
			RETURN JAC_SEAT_SLOT_2_ENUM
		BREAK
		CASE 2
			RETURN JAC_SEAT_SLOT_3_ENUM
		BREAK
		CASE 3
			RETURN JAC_SEAT_SLOT_4_ENUM
		BREAK
		CASE 4
			RETURN JAC_SEAT_SLOT_5_ENUM
		BREAK
		CASE 5
			RETURN JAC_SEAT_SLOT_6_ENUM
		BREAK
		CASE 6
			RETURN JAC_SEAT_SLOT_7_ENUM
		BREAK
		CASE 7
			RETURN JAC_SEAT_SLOT_8_ENUM
		BREAK
	ENDSWITCH
	
	RETURN NULL
ENDFUNC
*/

FUNC BOOL IS_SOMEONE_IN_JACUZZI_SLOT_PLAYING_IDLE_ANIM(INT seatID)
	IF seatID < 0 OR seatID >= JAC_MAX_NUM_SLOTS
		RETURN FALSE
	ENDIF
	
	INT iParticipant
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			IF playerBD[iParticipant].iJacuzziSeatUsed = seatID
				RETURN IS_BIT_SET(playerBD[iParticipant].iBitSet, iBS_PLAYER_IN_JACUZZI_PLAYING_IDLE)
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_ALLOWED_TO_PLAY_IDLE_JACUZZI_CLIP()
	CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_LOCAL_PLAYER_ALLOWED_TO_PLAY_IDLE_JACUZZI_CLIP")
	
	// The only problematic seats are 4, 3 and 2
	IF eChosenJacuzziSlotActivity != JAC_SEAT_SLOT_4_ENUM
	AND eChosenJacuzziSlotActivity != JAC_SEAT_SLOT_3_ENUM
	AND eChosenJacuzziSlotActivity != JAC_SEAT_SLOT_2_ENUM
		CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_LOCAL_PLAYER_ALLOWED_TO_PLAY_IDLE_JACUZZI_CLIP - yes they are, not sitting in affected slots")
		RETURN TRUE
	ENDIF
	
	BOOL bAllowed = TRUE
	
	// Check if anyone is sitting beside the player
	SWITCH eChosenJacuzziSlotActivity
		CASE JAC_SEAT_SLOT_4_ENUM
			bAllowed = NOT IS_SOMEONE_IN_JACUZZI_SLOT_PLAYING_IDLE_ANIM(2)
		BREAK
		CASE JAC_SEAT_SLOT_3_ENUM
			bAllowed = NOT IS_SOMEONE_IN_JACUZZI_SLOT_PLAYING_IDLE_ANIM(3) AND NOT IS_SOMEONE_IN_JACUZZI_SLOT_PLAYING_IDLE_ANIM(1)
		BREAK
		CASE JAC_SEAT_SLOT_2_ENUM
			bAllowed = NOT IS_SOMEONE_IN_JACUZZI_SLOT_PLAYING_IDLE_ANIM(2)
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		IF bAllowed
			CDEBUG2LN(DEBUG_SAFEHOUSE, "IS_LOCAL_PLAYER_ALLOWED_TO_PLAY_IDLE_JACUZZI_CLIP - yes, they are in problematic slots but nobody is using IDLE anim next to them")
		ELSE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_LOCAL_PLAYER_ALLOWED_TO_PLAY_IDLE_JACUZZI_CLIP - no! someone sitting next to them is using IDLE anim")
		ENDIF
	#ENDIF
	
	RETURN bAllowed
ENDFUNC

#IF IS_DEBUG_BUILD
PROC DEBUG_JACUZZI_LOGIC()
	IF db_bDrawDebugJacuzziSphereOutter 
		DRAW_DEBUG_SPHERE(vJacuzziCentre, fDebugJacuzziOutterSphereSize, 0, 178, 255, 100)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "RUN_JACUZZI_ACTIVITY: drawing debug sphere, from vJacuzziCentre: ", vJacuzziCentre, ", fDebugJacuzziOutterSphereSize = ", fDebugJacuzziOutterSphereSize)
		fJacuzziOutterSphereSize = fDebugJacuzziOutterSphereSize
	ENDIF
	IF db_bDrawDebugJacuzziSphereInner
		DRAW_DEBUG_SPHERE(vJacuzziCentre, fDebugJacuzziInnerSphereSize, 0, 0, 255, 100)
		fJacuzziInnerSphereSize = fDebugJacuzziInnerSphereSize
	ENDIF
	IF db_bDrawDebugJacuzziSeatPositions
		INT i
		VECTOR vSeatPosition
		VECTOR vHeading
	
		STORE_JACUZZI_POSITIONS()
		
		REPEAT JAC_MAX_NUM_SEAT_SLOTS i
			vSeatPosition = GET_MP_JACUZZI_SEAT_ANIM_POSITION(i, mpJacuzziLocal)
			vHeading = GET_MP_JACUZZI_SEAT_ANIM_ROTATION(i, mpJacuzziLocal)
			DRAW_DEBUG_SPHERE(vSeatPosition, 0.15, 255, 0, 0)
			DRAW_DEBUG_ARROW(vSeatPosition, vHeading.Z, 0.0)
		ENDREPEAT
	ENDIF
	
	IF db_bJacuzziForceCigarSlots
	OR db_bJacuzziForceCupSlots
		IF NOT IS_BIT_SET(iJacuzziBitSet, JAC_BS_SERVER_DATA_REINITED)
			IF NOT IS_BIT_SET(iJacuzziBitSet, JAC_BS_SERVER_DATA_REINIT)
				bJacuzziPropReserved = FALSE
				CLEAR_BIT(iJacuzziBitSet, JAC_BS_SERVER_DATA_GENERATED)
				SET_BIT(iJacuzziBitSet, JAC_BS_SERVER_DATA_REINIT)
			ENDIF
			IF IS_BIT_SET(iJacuzziBitSet, JAC_BS_SERVER_DATA_REINIT)
				IF DESTROY_JACUZZI_PROPS()
					IF INIT_SERVER_JACUZZI_ACTIVITY()
						SET_BIT(iJacuzziBitSet, JAC_BS_SERVER_DATA_REINITED)
						CLEAR_BIT(iJacuzziBitSet, JAC_BS_SERVER_DATA_REINIT)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(iJacuzziBitSet, JAC_BS_SERVER_DATA_REINITED)
			IF NOT IS_BIT_SET(iJacuzziBitSet, JAC_BS_SERVER_DATA_REINIT)
				bJacuzziPropReserved = FALSE
				CLEAR_BIT(iJacuzziBitSet, JAC_BS_SERVER_DATA_GENERATED)
				SET_BIT(iJacuzziBitSet, JAC_BS_SERVER_DATA_REINIT)
			ENDIF
			IF IS_BIT_SET(iJacuzziBitSet, JAC_BS_SERVER_DATA_REINIT)
				IF DESTROY_JACUZZI_PROPS()
					IF INIT_SERVER_JACUZZI_ACTIVITY()
						CLEAR_BIT(iJacuzziBitSet, JAC_BS_SERVER_DATA_REINITED)
						CLEAR_BIT(iJacuzziBitSet, JAC_BS_SERVER_DATA_REINIT)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC
#ENDIF

PROC LEAN_CLEANUP_CHECKS()
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
	AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
	OR IS_SCREEN_FADED_OUT()
		IF yachtLeanActState = LEAN_CHECK_BASE_ANIM
		OR yachtLeanActState = LEAN_CHECK_ENTER_OR_IDLE_ANIM
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_SYNCHRONIZED_SCENE) != PERFORMING_TASK
				CDEBUG1LN(DEBUG_SAFEHOUSE, "LEAN_CLEANUP_CHECKS: is not performing sync when they should be ")	
				YACHT_LEAN_CLEAN_UP()
			ENDIF
		ENDIF
		
		IF IS_PLAYER_DEAD(PLAYER_ID())
			CDEBUG1LN(DEBUG_SAFEHOUSE, "LEAN_CLEANUP_CHECKS: Player is dead")
			YACHT_LEAN_CLEAN_UP()
		ENDIF
		
		IF IS_SCREEN_FADED_OUT()
			CDEBUG1LN(DEBUG_SAFEHOUSE, "LEAN_CLEANUP_CHECKS: Player is faded out at stage: ", leanActStateString(yachtLeanActState))
			YACHT_LEAN_CLEAN_UP()
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "LEAN_CLEANUP_CHECKS: entity is dead")
		YACHT_LEAN_CLEAN_UP()
	ENDIF
ENDPROC

PROC JACUZZI_CLEANUP_CHECKS()
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
	AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF jacActState = JAC_CHECK_BASE_ANIM
		OR jacActState = JAC_CHECK_ENTER_OR_IDLE_ANIM
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_SYNCHRONIZED_SCENE) != PERFORMING_TASK
				
				CDEBUG1LN(DEBUG_SAFEHOUSE, "JACUZZI_CLEANUP_CHECKS: is not performing sync when they should be ")	
				JACUZZI_CLEAN_UP()
			ENDIF
		ENDIF
		
		IF IS_PLAYER_DEAD(PLAYER_ID())
			CDEBUG1LN(DEBUG_SAFEHOUSE, "JACUZZI_CLEANUP_CHECKS: Player is dead")
			JACUZZI_CLEAN_UP()
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "JACUZZI_CLEANUP_CHECKS: entity is dead")
		JACUZZI_CLEAN_UP()
	ENDIF
ENDPROC
PROC RUN_CAM_PITCH_LOGIC()
	IF NOT IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM) 
		SWITCH jacActState				
			CASE JAC_LOCATECHECK
				IF IS_BIT_SET(iJacuzziBitSet, JAC_BS_PLAYER_INSIDE_INNER_JAC_SPHERE)
					SET_THIRD_PERSON_CAM_RELATIVE_PITCH_LIMITS_THIS_UPDATE(-70.0027, 7.9032)
					CDEBUG2LN(DEBUG_SAFEHOUSE, "JAC_BS_PLAYER_INSIDE_INNER_JAC_SPHERE: SET_THIRD_PERSON_CAM_RELATIVE_PITCH_LIMITS_THIS_UPDATE")
				ENDIF
				IF IS_BIT_SET(iJacuzziBitSet, JAC_BS_PLAYER_INSIDE_OUTTER_AND_OUTSIDE_INNER_SPHERE)
				OR (NOT IS_BIT_SET(iJacuzziBitSet, JAC_BS_PLAYER_INSIDE_INNER_JAC_SPHERE)
				AND GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), vJacuzziCentre) < 7.362)
					SET_THIRD_PERSON_CAM_RELATIVE_PITCH_LIMITS_THIS_UPDATE(fJacuzziCameraMinPitch_LOCATECHECK, fJacuzziCameraMaxPitch_LOCATECHECK)					
					CDEBUG2LN(DEBUG_SAFEHOUSE, "JAC_BS_PLAYER_INSIDE_OUTTER_AND_OUTSIDE_INNER_SPHERE: SET_THIRD_PERSON_CAM_RELATIVE_PITCH_LIMITS_THIS_UPDATE")
				ENDIF
			BREAK
			CASE JAC_RUN_ENTER_ANIM  
			CASE JAC_CHECK_ENTER_OR_IDLE_ANIM           
			CASE JAC_CHECK_BASE_ANIM 
			CASE JAC_APPROACH                         	
			CASE JAC_CLEANUP   
				SET_THIRD_PERSON_CAM_RELATIVE_PITCH_LIMITS_THIS_UPDATE(fJacuzziCameraMinPitch_CLEANUP,fJacuzziCameraMaxPitch_CLEANUP)			
			BREAK                       	
			CASE JAC_PROMPT								
			CASE JAC_REQUEST	                      						
				SET_THIRD_PERSON_CAM_RELATIVE_PITCH_LIMITS_THIS_UPDATE(fJacuzziCameraMinPitch_REQUEST, fJacuzziCameraMaxPitch_REQUEST)
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC RUN_INNER_OUTTER_SPHERE_CHECKS()
	IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), vJacuzziCentre) <= fJacuzziInnerSphereSize
		CDEBUG3LN(DEBUG_SAFEHOUSE, "Inner Check: Player is ", GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), vJacuzziCentre), ", of vJacuzziCentre: ", vJacuzziCentre," fJacuzziInnerSphereSize = ", fJacuzziInnerSphereSize)
		
		// Just entered the jacuzzi
		IF NOT g_BInYachtJacuzzi
			g_BInYachtJacuzzi = TRUE
			
			// url:bugstar:2628378
			IF NOT IS_PLAYER_SCTV(PLAYER_ID())
				SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), FALSE)
				NETWORK_DISABLE_LEAVE_REMOTE_PED_BEHIND(TRUE)
			ENDIF
		ENDIF
		
		SET_BIT(iJacuzziBitSet, JAC_BS_PLAYER_INSIDE_INNER_JAC_SPHERE)
	ELSE
		CLEAR_BIT(iJacuzziBitSet, JAC_BS_PLAYER_INSIDE_OUTTER_AND_OUTSIDE_INNER_SPHERE)
		CLEAR_BIT(iJacuzziBitSet, JAC_BS_PLAYER_INSIDE_INNER_JAC_SPHERE)
	ENDIF

	IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), vJacuzziCentre) > fJacuzziOutterSphereSize
		IF g_BInYachtJacuzzi
			g_BInYachtJacuzzi = FALSE
			
			
			// url:bugstar:2628378
			// 21/03/2016: commenting this out after speaking to Conor. This script disables leaveing ped behind in pregame and enables it in cleanup so no point disableg it after leaving jacuzzi (which interferes with what script is doing)
			//IF NOT IS_PLAYER_SCTV(PLAYER_ID())
			//  SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), TRUE)
			//  NETWORK_DISABLE_LEAVE_REMOTE_PED_BEHIND(FALSE)
			//ENDIF
		ENDIF
		
		CLEAR_BIT(iJacuzziBitSet, JAC_BS_PLAYER_INSIDE_OUTTER_AND_OUTSIDE_INNER_SPHERE)
		//CDEBUG3LN(DEBUG_SAFEHOUSE, "JAC_LOCATECHECK: CLearing bit JAC_BS_PLAYER_INSIDE_OUTTER_AND_OUTSIDE_INNER_SPHERE")
	
		SET_BIT(iJacuzziBitSet, JAC_BS_PLAYER_OUTSIDE_OUTTER_JAC_SPHERE)
		//CDEBUG3LN(DEBUG_SAFEHOUSE, "Outter Check: Player is ", GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), vJacuzziCentre), ", of vJacuzziCentre: ", vJacuzziCentre, ", fJacuzziOutterSphereSize = ", fJacuzziOutterSphereSize)
	ELSE
		CLEAR_BIT(iJacuzziBitSet, JAC_BS_PLAYER_OUTSIDE_OUTTER_JAC_SPHERE)
		IF NOT IS_BIT_SET(iJacuzziBitSet, JAC_BS_PLAYER_INSIDE_INNER_JAC_SPHERE)
		AND NOT IS_BIT_SET(iJacuzziBitSet, JAC_BS_PLAYER_OUTSIDE_OUTTER_JAC_SPHERE)
			SET_BIT(iJacuzziBitSet, JAC_BS_PLAYER_INSIDE_OUTTER_AND_OUTSIDE_INNER_SPHERE)
			CDEBUG3LN(DEBUG_SAFEHOUSE, "JAC_LOCATECHECK: Setting bit JAC_BS_PLAYER_INSIDE_OUTTER_AND_OUTSIDE_INNER_SPHERE")
		ENDIF
	ENDIF
ENDPROC

PROC RUN_YACHT_LEAN_ACTIVITIES()
	LEAN_CLEANUP_CHECKS()
	SUPPRESS_LEAN_PHONE_HUD_AND_ACTION_LOGIC()
	SWITCH yachtLeanActState	
		CASE LEAN_LOCATECHECK
								
			//CDEBUG2LN(DEBUG_SAFEHOUSE, "RUN_YACHT_LEAN_ACTIVITIES: iStaggerLeanActLoop++, iStaggerLeanActLoop = ", iStaggerLeanActLoop )
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), GET_MP_YACHT_LEAN_LOCATE0(iStaggerLeanActLoop,mpYachtLeanLocal), 
															GET_MP_YACHT_LEAN_LOCATE1(iStaggerLeanActLoop,mpYachtLeanLocal), 
															1.0)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "JAC_LOCATECHECK: Player is in seat: ", iStaggerLeanActLoop)
				SetLeanStage(yachtLeanActState, LEAN_PROMPT)													
			ELSE
				IF iStaggerLeanActLoop >= YACHT_MAX_NUM_LEAN_SLOTS-1
					iStaggerLeanActLoop = 0
					CDEBUG2LN(DEBUG_SAFEHOUSE, "RUN_YACHT_LEAN_ACTIVITIES: iStaggerLeanActLoop = 0")
				ELSE
					iStaggerLeanActLoop++
				ENDIF
			ENDIF
			
			IF g_YachtLeanInProgress
				g_YachtLeanInProgress = FALSE
			ENDIF
		BREAK
		
		CASE LEAN_PROMPT
			 
			eChosenYachtLeanSlotActivity = INT_TO_ENUM(ACTIVITY_ENUMS, ENUM_TO_INT(YACHT_LEAN_SLOT_1_ENUM)+iStaggerLeanActLoop)
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SAFEHOUSE, "LEAN_PROMPT: Checking against activity: ", DEBUG_GET_MP_PROP_ACT_NAME(eChosenYachtLeanSlotActivity))
			#ENDIF
			
			IF NOT ARE_THERE_TOO_MANY_PLAYERS_CLOSE_TO_ACTIVITY(GET_MP_YACHT_LEAN_LOCATE0(iStaggerLeanActLoop,mpYachtLeanLocal), GET_MP_YACHT_LEAN_LOCATE1(iStaggerLeanActLoop,mpYachtLeanLocal))
			AND NOT IS_ANY_INTERACTION_ANIM_PLAYING()
			AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
			AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
			AND NOT IS_BROWSER_OPEN()
				IF IS_PROPERTY_ACTIVITY_FREE_AND_CAN_PLAYER_USE_IT(eChosenYachtLeanSlotActivity, GET_ENTITY_COORDS(PLAYER_PED_ID()), serverBD.activityControl)	
					
					REQUEST_LEAN_ANIM_DICT(IS_PLAYER_FEMALE())
					
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					AND NOT IS_PHONE_ONSCREEN()
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPYACHT_LEAN")
							CLEAR_BIT(iLeanBitSet, LEAN_BS_EXIT_HELP_TEXT_DISPLAYED)
						ENDIF
						IF NOT IS_BIT_SET(iLeanBitSet, LEAN_BS_EXIT_HELP_TEXT_DISPLAYED)
							SET_BIT(iLeanBitSet, LEAN_BS_EXIT_HELP_TEXT_DISPLAYED)
							PRINT_HELP_FOREVER("MPYACHT_LEAN")
						ENDIF
					ELSE
						IF IS_PHONE_ONSCREEN()
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPYACHT_LEAN")
								CLEAR_HELP()
							ENDIF
						ENDIF
						CLEAR_BIT(iLeanBitSet, LEAN_BS_EXIT_HELP_TEXT_DISPLAYED)
					ENDIF
					
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)

						CDEBUG1LN(DEBUG_SAFEHOUSE, "LEAN_PROMPT: HAS_CONTEXT_BUTTON_TRIGGERED: TRUE")

						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPYACHT_LEAN")
							CLEAR_HELP()
						ENDIF
						CLEAR_BIT(iLeanBitSet, LEAN_BS_EXIT_HELP_TEXT_DISPLAYED)
						SetLeanStage(yachtLeanActState, LEAN_REQUEST)																									
					ENDIF
					
					
				ENDIF
				
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "LEAN_PROMPT: Either property activity isn't free and player can't use it or player is getting kicked from apartment")
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPYACHT_LEAN")
					CLEAR_HELP()
				ENDIF
				CLEAR_BIT(iLeanBitSet, LEAN_BS_EXIT_HELP_TEXT_DISPLAYED)
				
			ENDIF
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), GET_MP_YACHT_LEAN_LOCATE0(iStaggerLeanActLoop,mpYachtLeanLocal), 
															GET_MP_YACHT_LEAN_LOCATE1(iStaggerLeanActLoop,MPYachtLeanLocal), 
															1.0)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "LEAN_PROMPT: Player is not in seat ", iStaggerLeanActLoop," locate anymore ")
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPYACHT_LEAN")
					CLEAR_HELP()
				ENDIF
				CLEAR_BIT(iLeanBitSet, LEAN_BS_EXIT_HELP_TEXT_DISPLAYED)
				SetLeanStage(yachtLeanActState, LEAN_LOCATECHECK)													
			ENDIF
		BREAK
		
		CASE LEAN_REQUEST	
			PROPERTY_ACTIVITY_REQUEST_RESULT requestResult
			requestResult = MP_PROP_ACT_RESULT_IN_PROGRESS
			IF REQUEST_PROPERTY_ACTIVITY(eChosenYachtLeanSlotActivity, requestResult, playerBD[NATIVE_TO_INT(PLAYER_ID())].iActivityRequested, serverBD.activityControl)
				IF requestResult = MP_PROP_ACT_RESULT_IN_USE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "RUN_YACHT_LEAN_ACTIVITIES: LEAN_REQUEST = TRUE: MP_PROP_ACT_RESULT_IN_USE ")
					CLEAR_REQUEST_PROPERTY_ACTIVITY(playerBD[NATIVE_TO_INT(PLAYER_ID())].iActivityRequested)
					SetLeanStage(yachtLeanActState, LEAN_LOCATECHECK)
				ELIF requestResult = MP_PROP_ACT_RESULT_SUCCESS
					CDEBUG1LN(DEBUG_SAFEHOUSE, "RUN_YACHT_LEAN_ACTIVITIES: LEAN_REQUEST = TRUE: MP_PROP_ACT_RESULT_SUCCESS ")
					sYachtLeanAnimDict = GET_LEAN_ANIM_DICT_VAR(IS_PLAYER_FEMALE())
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_LEAN_ANIM_DICT_VAR: iStaggerLeanActLoop = ", iStaggerLeanActLoop, " jacuzziClientSeatData[", iStaggerLeanActLoop, "] = ", jacuzziClientSeatData[iStaggerLeanActLoop].iJacuzziAnimVariation, " = ", GET_JAC_SEAT_ANIM_DICT_VAR_STRING(jacuzziClientSeatData[iStaggerLeanActLoop].iJacuzziAnimVariation))					
					SetLeanStage(yachtLeanActState, LEAN_APPROACH)
					
					IF NOT g_YachtLeanInProgress 
						CDEBUG1LN(DEBUG_SAFEHOUSE, "RUN_YACHT_LEAN_ACTIVITIES: Setting global g_YachtLeanInProgress to TRUE.")
						g_YachtLeanInProgress = TRUE
					ENDIF
				ENDIF	
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "RUN_YACHT_LEAN_ACTIVITIES: REQUEST_PROPERTY_ACTIVITY = FALSE ")
			ENDIF
		BREAK
		
		CASE LEAN_APPROACH
			
			
			IF NOT HAS_ANIM_DICT_LOADED(sYachtLeanAnimDict)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "LEAN_APPROACH: HAS_ANIM_DICT_LOADED = FALSE")
				EXIT
			ENDIF
			
			
			VECTOR vTriggerPos 
			vTriggerPos = GET_ANIM_INITIAL_OFFSET_POSITION(sYachtLeanAnimDict, "enter", GET_MP_YACHT_LEAN_ANIM_POSITION(iStaggerLeanActLoop, MPYachtLeanLocal), GET_MP_YACHT_LEAN_ANIM_ROTATION(iStaggerLeanActLoop, MPYachtLeanLocal))  
			VECTOR vTempRotation 
			vTempRotation = GET_ANIM_INITIAL_OFFSET_ROTATION(sYachtLeanAnimDict, "enter", GET_MP_YACHT_LEAN_ANIM_POSITION(iStaggerLeanActLoop, MPYachtLeanLocal), GET_MP_YACHT_LEAN_ANIM_ROTATION(iStaggerLeanActLoop, MPYachtLeanLocal))  
			Float fTriggerHead 
			fTriggerHead = vTempRotation.Z
			
			TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), vTriggerPos, PEDMOVEBLENDRATIO_WALK, 3000, fTriggerHead, 0.050)
			
			SetLeanStage(yachtLeanActState, LEAN_RUN_ENTER_ANIM)
		BREAK
		
		CASE LEAN_RUN_ENTER_ANIM
			
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> PERFORMING_TASK
				iYachtLeanNetSceneid = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_MP_YACHT_LEAN_ANIM_POSITION(iStaggerLeanActLoop, MPYachtLeanLocal), GET_MP_YACHT_LEAN_ANIM_ROTATION(iStaggerLeanActLoop, MPYachtLeanLocal), DEFAULT, TRUE, FALSE)
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iYachtLeanNetSceneid, sYachtLeanAnimDict, "enter", WALK_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT, WALK_BLEND_IN)
				NETWORK_START_SYNCHRONISED_SCENE(iYachtLeanNetSceneid)
				SetLeanStage(yachtLeanActState, LEAN_CHECK_ENTER_OR_IDLE_ANIM)
				playerBD[NATIVE_TO_INT(PLAYER_ID())].iJacuzziSeatUsed = iStaggerLeanActLoop
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "LEAN_RUN_ENTER_ANIM: waiting for ped to stop performing task")
			ENDIF
			
		BREAK
		
		CASE LEAN_CHECK_ENTER_OR_IDLE_ANIM
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPYACHT_UNLEAN")
				CLEAR_HELP()
			ENDIF
			CLEAR_BIT(iLeanBitSet, LEAN_BS_EXIT_HELP_TEXT_DISPLAYED)
			
			INT localIdleScene
			localIdleScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iYachtLeanNetSceneid)
			IF localIdleScene != -1
				IF GET_SYNCHRONIZED_SCENE_PHASE(localIdleScene) >= 0.990 // Has the idle or enter anim finished yet
					iYachtLeanNetSceneid = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_MP_YACHT_LEAN_ANIM_POSITION(iStaggerLeanActLoop, MPYachtLeanLocal), GET_MP_YACHT_LEAN_ANIM_ROTATION(iStaggerLeanActLoop, MPYachtLeanLocal), DEFAULT, TRUE, FALSE)
					#IF IS_DEBUG_BUILD
					#ENDIF
					
					STRING sLeanClip
					iLeanNumBaseToPlay = GET_RANDOM_INT_IN_RANGE(1, 3)
					iLeanBaseClipPlayedCount++
					sLeanClip = "base"
					CDEBUG1LN(DEBUG_SAFEHOUSE, "LEAN_CHECK_ENTER_OR_IDLE_ANIM: iLeanNumBaseToPlay = ", iLeanNumBaseToPlay)
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iYachtLeanNetSceneid, sYachtLeanAnimDict, sLeanClip, SLOW_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT,DEFAULT,AIK_DISABLE_HEAD_IK)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: PLAYER added to Net Sync Scene, ANIM_DICT: ", sYachtLeanAnimDict,", ANIM_NAME: ", sLeanClip)
					
					NETWORK_START_SYNCHRONISED_SCENE(iYachtLeanNetSceneid)
		
					
					SetLeanStage(yachtLeanActState, LEAN_CHECK_BASE_ANIM)
				ENDIF
			ENDIF
		BREAK
		
		CASE LEAN_CHECK_BASE_ANIM
			INT localScene
			localScene  = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iYachtLeanNetSceneid)
			IF localScene != -1
				
				IF GET_SYNCHRONIZED_SCENE_PHASE(localScene) >= 0.990
					iYachtLeanNetSceneid = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_MP_YACHT_LEAN_ANIM_POSITION(iStaggerLeanActLoop, MPYachtLeanLocal), GET_MP_YACHT_LEAN_ANIM_ROTATION(iStaggerLeanActLoop, MPYachtLeanLocal), DEFAULT, TRUE, FALSE)
					#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: CREATE net sync scene, SCENE_ID: " , iYachtLeanNetSceneid)
					#ENDIF
					
					STRING sClipVar
					
					IF iLeanBaseClipPlayedCount < iLeanNumBaseToPlay
						CDEBUG1LN(DEBUG_SAFEHOUSE, "LEAN_CHECK_BASE_ANIM: More base sequence clips to be played")
						iLeanBaseClipPlayedCount++
						sClipVar = "base"
					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "LEAN_CHECK_BASE_ANIM: Going to play idle anim")
						iLeanBaseClipPlayedCount = 0
						iLeanNumBaseToPlay = 0						
						CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: iLeanNumBaseToPlay: ", iLeanNumBaseToPlay)
						
						sClipVar = GENERATE_RANDOM_YACHT_LEAN_IDLE_CLIP()
						
						SetLeanStage(yachtLeanActState, LEAN_CHECK_ENTER_OR_IDLE_ANIM)
					ENDIF
						
					
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iYachtLeanNetSceneid, sYachtLeanAnimDict, sClipVar, SLOW_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT,DEFAULT,AIK_DISABLE_HEAD_IK)
					NETWORK_START_SYNCHRONISED_SCENE(iYachtLeanNetSceneid)
					#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: PLAYER added to Net Sync Scene, ANIM_DICT: ", sYachtLeanAnimDict,", ANIM_NAME: ", sClipVar)
					#ENDIF
					
				ENDIF
				
			ENDIF
			
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPYACHT_UNLEAN")
					CLEAR_BIT(iLeanBitSet, LEAN_BS_EXIT_HELP_TEXT_DISPLAYED)
				ENDIF
				IF NOT IS_BIT_SET(iLeanBitSet, LEAN_BS_EXIT_HELP_TEXT_DISPLAYED)
					SET_BIT(iLeanBitSet, LEAN_BS_EXIT_HELP_TEXT_DISPLAYED)
					PRINT_HELP_FOREVER("MPYACHT_UNLEAN")
				ENDIF
			ELSE
				CLEAR_BIT(iLeanBitSet, LEAN_BS_EXIT_HELP_TEXT_DISPLAYED)
			ENDIF
			
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "exiting activity")
				iYachtLeanNetSceneid = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_MP_YACHT_LEAN_ANIM_POSITION(iStaggerLeanActLoop, MPYachtLeanLocal), GET_MP_YACHT_LEAN_ANIM_ROTATION(iStaggerLeanActLoop, MPYachtLeanLocal), DEFAULT, FALSE, FALSE)
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: CREATE net sync scene, SCENE_ID: " , iYachtLeanNetSceneid)
				#ENDIF
																																																
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iYachtLeanNetSceneid, sYachtLeanAnimDict, "exit", SLOW_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT,INSTANT_BLEND_IN,AIK_DISABLE_HEAD_IK)
				NETWORK_START_SYNCHRONISED_SCENE(iYachtLeanNetSceneid)
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPYACHT_UNLEAN")
					CLEAR_HELP()
				ENDIF
				CLEAR_BIT(iLeanBitSet, LEAN_BS_EXIT_HELP_TEXT_DISPLAYED)
				SetLeanStage(yachtLeanActState, LEAN_CLEANUP)
			ENDIF			
		BREAK
		
		
		CASE LEAN_CLEANUP
			
			localScene  = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iYachtLeanNetSceneid)
			IF localScene != -1
			
				IF GET_SYNCHRONIZED_SCENE_PHASE(localScene) >= 0.990
				OR HAS_ANIM_EVENT_FIRED (PLAYER_PED_ID(), HASH("BREAKOUT"))
					#IF IS_DEBUG_BUILD
					IF HAS_ANIM_EVENT_FIRED (PLAYER_PED_ID(), HASH("BREAKOUT"))
						CDEBUG1LN(DEBUG_SAFEHOUSE, "LEAN_CLEANUP: Finishing anim early as BREAKOUT anim event fired, at frame: ", GET_SYNCHRONIZED_SCENE_PHASE(localScene))
					ENDIF
					#ENDIF 
					NETWORK_STOP_SYNCHRONISED_SCENE(iYachtLeanNetSceneid)
					SetLeanStage(yachtLeanActState, LEAN_LOCATECHECK)	
					YACHT_LEAN_CLEAN_UP()
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "JAC_CLEANUP: Scene isn't running cleanup anyway")
				SetLeanStage(yachtLeanActState, LEAN_LOCATECHECK)
				YACHT_LEAN_CLEAN_UP()
			ENDIF
			
			IF g_YachtLeanInProgress
				g_YachtLeanInProgress = FALSE
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL GRAB_JACUZZI_PROPS(INT iSlot)
	IF iSlot > -1
		IF serverBD.jacuzziServerSeatData[iSlot].bCigar = TRUE
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.jacuzziServerSeatData[iSlot].niCigar)
				IF TAKE_CONTROL_OF_NET_ID(serverBD.jacuzziServerSeatData[iSlot].niCigar)
					SET_ENTITY_COLLISION(NET_TO_ENT(serverBD.jacuzziServerSeatData[iSlot].niCigar), FALSE)
					RETURN TRUE
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "GRAB_JACUZZI_PROPS: niCigar TAKE_CONTROL_OF_NET_ID = FALSE ")
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "GRAB_JACUZZI_PROPS: niCigar NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID = FALSE ")
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF serverBD.jacuzziServerSeatData[iSlot].bCup = TRUE
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.jacuzziServerSeatData[iSlot].niCup)
				IF TAKE_CONTROL_OF_NET_ID(serverBD.jacuzziServerSeatData[iSlot].niCup)
					SET_ENTITY_COLLISION(NET_TO_ENT(serverBD.jacuzziServerSeatData[iSlot].niCup), FALSE)
					RETURN TRUE
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "GRAB_JACUZZI_PROPS: propLighter TAKE_CONTROL_OF_NET_ID = FALSE ")
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "GRAB_JACUZZI_PROPS: niCup NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID = FALSE ")
				RETURN FALSE
			ENDIF
		ENDIF
	ELSE
		CWARNINGLN(DEBUG_SAFEHOUSE, "GRAB_JACUZZI_PROPS: Invalid seatID")
		RETURN TRUE
	ENDIF
	
	IF serverBD.jacuzziServerSeatData[iSlot].bCigar = FALSE
	AND serverBD.jacuzziServerSeatData[iSlot].bCup = FALSE
		RETURN TRUE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "GRAB_JACUZZI_PROPS: slot not registered for any activity, not grabbing props ")
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MANAGE_FORCE_DRINK_SMOKE_ANIM()
	SWITCH jacActState
		
		CASE JAC_RUN_ENTER_ANIM 
			IF bForceDrinkSmokeActivity = TRUE
				
					START_NET_TIMER(ntSmokeDrinkForceTimer, TRUE)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_FORCE_DRINK_SMOKE_ANIM: ntSmokeDrinkForceTimer has started")
					
			ENDIF
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_FORCE_DRINK_SMOKE_ANIM: ntSmokeDrinkForceTimer: ", ABSI(GET_TIME_DIFFERENCE(INT_TO_NATIVE(TIME_DATATYPE, GET_GAME_TIMER()), ntSmokeDrinkForceTimer.Timer)))
		BREAK
  
		CASE JAC_CHECK_ENTER_OR_IDLE_ANIM           
		CASE JAC_CHECK_BASE_ANIM 
		CASE JAC_APPROACH                  
			
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_FORCE_DRINK_SMOKE_ANIM: ntSmokeDrinkForceTimer: ", ABSI(GET_TIME_DIFFERENCE(INT_TO_NATIVE(TIME_DATATYPE, GET_GAME_TIMER()), ntSmokeDrinkForceTimer.Timer)))
		BREAK
		CASE JAC_CLEANUP
			REINIT_NET_TIMER(ntSmokeDrinkForceTimer, TRUE)
		BREAK
	ENDSWITCH
		
ENDPROC

PROC RUN_JACUZZI_ACTIVITY()

	// Sitting in jacuzzi and being involved in yacht warp causes an array overflow so dont run jacuzzi logic if warp is active
	// Another solution would be to find that overflow and fix it but let's try this for now...
	IF IS_PLAYER_DOING_A_YACHT_WARP(PLAYER_ID())
		CLEAR_ALL_JACUZZI_PROMPTS()
		EXIT
	ENDIF
	MANAGE_FORCE_DRINK_SMOKE_ANIM()
	SUPPRESS_JAC_PHONE_HUD_AND_ACTION_LOGIC()
	JACUZZI_CLEANUP_CHECKS()
	RUN_CAM_PITCH_LOGIC()
	MAINTAIN_JACUZZI_CLOTHING()
	#IF IS_DEBUG_BUILD
	DEBUG_JACUZZI_LOGIC()
	#ENDIF
	
	SWITCH jacActState
		CASE JAC_INIT
			IF serverBD.iStage > SERVER_STAGE_INIT
				INIT_CLIENT_JACUZZI_ACITVITY()
				SetJacuzziStage(jacActState, JAC_LOCATECHECK)
			ENDIF
		BREAK
	
		CASE JAC_LOCATECHECK
			
			RUN_INNER_OUTTER_SPHERE_CHECKS()
					
			//CDEBUG3LN(DEBUG_SAFEHOUSE, "RUN_JACUZZI_ACTIVITY: iStaggerJacuzziSeatLoop++, iStaggerJacuzziSeatLoop = ", iStaggerJacuzziSeatLoop )

			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), GET_MP_JACUZZI_SEAT_LOCATE0(iStaggerJacuzziSeatLoop,MPJacuzziLocal), 
															GET_MP_JACUZZI_SEAT_LOCATE1(iStaggerJacuzziSeatLoop,MPJacuzziLocal), 
															1.0)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "JAC_LOCATECHECK: Player is in seat: ", iStaggerJacuzziSeatLoop)
				SetJacuzziStage(jacActState, JAC_PROMPT)													
			ELSE
				IF iStaggerJacuzziSeatLoop >= JAC_MAX_NUM_SLOTS
					iStaggerJacuzziSeatLoop = 0
					CDEBUG3LN(DEBUG_SAFEHOUSE, "RUN_JACUZZI_ACTIVITY: iStaggerJacuzziSeatLoop = 0")
				ELSE
					iStaggerJacuzziSeatLoop++
				ENDIF
			ENDIF
		BREAK
		
		CASE JAC_PROMPT
			 
			eChosenJacuzziSlotActivity = INT_TO_ENUM(ACTIVITY_ENUMS, ENUM_TO_INT(JAC_SEAT_SLOT_1_ENUM)+iStaggerJacuzziSeatLoop)
			#IF IS_DEBUG_BUILD
				CDEBUG3LN(DEBUG_SAFEHOUSE, "JAC_PROMPT: Checking against activity: ", DEBUG_GET_MP_PROP_ACT_NAME(eChosenJacuzziSlotActivity))
			#ENDIF
			
			IF NOT ARE_THERE_TOO_MANY_PLAYERS_CLOSE_TO_ACTIVITY(GET_MP_JACUZZI_SEAT_LOCATE0(iStaggerJacuzziSeatLoop,MPJacuzziLocal), GET_MP_JACUZZI_SEAT_LOCATE1(iStaggerJacuzziSeatLoop,MPJacuzziLocal))
			AND NOT IS_ANY_INTERACTION_ANIM_PLAYING()
			AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
			AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
			AND NOT IS_BROWSER_OPEN()
				IF IS_PROPERTY_ACTIVITY_FREE_AND_CAN_PLAYER_USE_IT(eChosenJacuzziSlotActivity, GET_ENTITY_COORDS(PLAYER_PED_ID()), serverBD.activityControl)	
					
					REQUEST_JAC_ANIM_DICT(iStaggerJacuzziSeatLoop, IS_PLAYER_FEMALE())
					
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					AND NOT IS_PHONE_ONSCREEN()
						IF NOT IS_JACUZZI_START_PROMPT_SHOWN()
							CLEAR_BIT(iJacuzziBitSet, JAC_BS_EXIT_HELP_TEXT_DISPLAYED)
						ENDIF
						IF NOT IS_BIT_SET(iJacuzziBitSet, JAC_BS_EXIT_HELP_TEXT_DISPLAYED)
							SET_BIT(iJacuzziBitSet, JAC_BS_EXIT_HELP_TEXT_DISPLAYED)
							SHOW_JACUZZI_START_PROMPT(iStaggerJacuzziSeatLoop >= JAC_MAX_NUM_SEAT_SLOTS)
						ENDIF
					ELSE
						IF IS_PHONE_ONSCREEN()
							IF IS_JACUZZI_START_PROMPT_SHOWN()
								CLEAR_HELP()
							ENDIF
						ENDIF
						CLEAR_BIT(iJacuzziBitSet, JAC_BS_EXIT_HELP_TEXT_DISPLAYED)
					ENDIF
					
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
						iPlayerChosenSeatSlot = iStaggerJacuzziSeatLoop
						CDEBUG1LN(DEBUG_SAFEHOUSE, "JAC_PROMPT: HAS_CONTEXT_BUTTON_TRIGGERED: TRUE")

						IF IS_JACUZZI_START_PROMPT_SHOWN()
							CLEAR_HELP()
						ENDIF
						CLEAR_BIT(iJacuzziBitSet, JAC_BS_EXIT_HELP_TEXT_DISPLAYED)
						
						SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_RELAX_IN_HOT_TUB)
						
						SetJacuzziStage(jacActState, JAC_REQUEST)																									
					ENDIF
					
					
				ENDIF
				
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "JAC_PROMPT: Either property activity isn't free and player can't use it or player is getting kicked from apartment")
				IF IS_JACUZZI_START_PROMPT_SHOWN()
					CLEAR_HELP()
				ENDIF
				CLEAR_BIT(iJacuzziBitSet, JAC_BS_EXIT_HELP_TEXT_DISPLAYED)
				
			ENDIF
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), GET_MP_JACUZZI_SEAT_LOCATE0(iStaggerJacuzziSeatLoop,MPJacuzziLocal), 
															GET_MP_JACUZZI_SEAT_LOCATE1(iStaggerJacuzziSeatLoop,MPJacuzziLocal), 
															1.0)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "JAC_LOCATECHECK: Player is not in seat ", iStaggerJacuzziSeatLoop," locate anymore ")
				IF IS_JACUZZI_START_PROMPT_SHOWN()
					CLEAR_HELP()
				ENDIF
				CLEAR_BIT(iJacuzziBitSet, JAC_BS_EXIT_HELP_TEXT_DISPLAYED)
				SetJacuzziStage(jacActState, JAC_LOCATECHECK)													
			ENDIF
		BREAK
		
		CASE JAC_REQUEST	
			PROPERTY_ACTIVITY_REQUEST_RESULT requestResult
			requestResult = MP_PROP_ACT_RESULT_IN_PROGRESS
			IF REQUEST_PROPERTY_ACTIVITY(eChosenJacuzziSlotActivity, requestResult, playerBD[NATIVE_TO_INT(PLAYER_ID())].iActivityRequested, serverBD.activityControl)
				IF requestResult = MP_PROP_ACT_RESULT_IN_USE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "RUN_JACUZZI_ACTIVITY: JAC_REQUEST = TRUE: MP_PROP_ACT_RESULT_IN_USE ")
					CLEAR_REQUEST_PROPERTY_ACTIVITY(playerBD[NATIVE_TO_INT(PLAYER_ID())].iActivityRequested)
					SetJacuzziStage(jacActState, JAC_LOCATECHECK)
				ELIF requestResult = MP_PROP_ACT_RESULT_SUCCESS
					CDEBUG1LN(DEBUG_SAFEHOUSE, "RUN_JACUZZI_ACTIVITY: JAC_REQUEST = TRUE: MP_PROP_ACT_RESULT_SUCCESS ")
					sJaccuzziAnimDict = GET_JACUZZI_ANIM_DICT_VAR(iPlayerChosenSeatSlot, IS_PLAYER_FEMALE())
					CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_JACUZZI_ANIM_DICT_VAR: iPlayerChosenSeatSlot = ", iPlayerChosenSeatSlot, " jacuzziClientSeatData[", iPlayerChosenSeatSlot, "] = ", jacuzziClientSeatData[iPlayerChosenSeatSlot].iJacuzziAnimVariation, " = ", GET_JAC_SEAT_ANIM_DICT_VAR_STRING(jacuzziClientSeatData[iPlayerChosenSeatSlot].iJacuzziAnimVariation))					
					SetJacuzziStage(jacActState, JAC_APPROACH)
				ENDIF	
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "RUN_JACUZZI_ACTIVITY: REQUEST_PROPERTY_ACTIVITY = FALSE ")
			ENDIF
		BREAK
		
		CASE JAC_APPROACH
			
			
			IF NOT HAS_ANIM_DICT_LOADED(sJaccuzziAnimDict)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "JAC_APPROACH: HAS_ANIM_DICT_LOADED = FALSE")
				EXIT
			ENDIF
			
			
			VECTOR vTriggerPos 
			vTriggerPos = GET_ANIM_INITIAL_OFFSET_POSITION(sJaccuzziAnimDict, "enter", GET_MP_JACUZZI_SEAT_ANIM_APPROACH(iPlayerChosenSeatSlot, MPJacuzziLocal), GET_MP_JACUZZI_SEAT_ANIM_ROTATION(iPlayerChosenSeatSlot, MPJacuzziLocal))  
			VECTOR vTempRotation 
			vTempRotation = GET_ANIM_INITIAL_OFFSET_ROTATION(sJaccuzziAnimDict, "enter", GET_MP_JACUZZI_SEAT_ANIM_POSITION(iPlayerChosenSeatSlot, MPJacuzziLocal), GET_MP_JACUZZI_SEAT_ANIM_ROTATION(iPlayerChosenSeatSlot, MPJacuzziLocal))  
			Float fTriggerHead 
			fTriggerHead = vTempRotation.Z
			
			TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), vTriggerPos, PEDMOVEBLENDRATIO_WALK, 100, fTriggerHead, 0.050)
			
			SetJacuzziStage(jacActState, JAC_RUN_ENTER_ANIM)
		BREAK
		
		CASE JAC_RUN_ENTER_ANIM
			SET_PED_CAPSULE(PLAYER_PED_ID(), 0.1)
			CDEBUG2LN(DEBUG_SAFEHOUSE, "JAC_RUN_ENTER_ANIM: SET_PED_CAPSULE to 0.2")
			IF GRAB_JACUZZI_PROPS(iPlayerChosenSeatSlot)
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> PERFORMING_TASK
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
						IF NOT IS_BIT_SET(iLocalBS2,LOCAL_BS2_SetJacuzziEnterAnimCam)
							SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT, CAM_VIEW_MODE_THIRD_PERSON_MEDIUM)
							SET_BIT(iLocalBS2,LOCAL_BS2_SetJacuzziEnterAnimCam)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "JAC_RUN_ENTER_ANIM: setting camera view mode to third person")
						ENDIF	
					ENDIF
					iJacuzziNetSceneid = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_MP_JACUZZI_SEAT_ANIM_POSITION(iPlayerChosenSeatSlot, MPJacuzziLocal), GET_MP_JACUZZI_SEAT_ANIM_ROTATION(iPlayerChosenSeatSlot, MPJacuzziLocal), DEFAULT, TRUE, FALSE)
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iJacuzziNetSceneid, sJaccuzziAnimDict, "enter", WALK_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT, WALK_BLEND_IN, AIK_DISABLE_HEAD_IK)
					NETWORK_START_SYNCHRONISED_SCENE(iJacuzziNetSceneid)
					SET_PLAYER_HEALTH_RECHARGE_MAX_PERCENT(GET_PLAYER_INDEX(), 1.0)
					SetJacuzziStage(jacActState, JAC_CHECK_ENTER_OR_IDLE_ANIM)
					playerBD[NATIVE_TO_INT(PLAYER_ID())].iJacuzziSeatUsed = iPlayerChosenSeatSlot
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "JAC_RUN_ENTER_ANIM: waiting for ped to stop performing task")
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "JAC_RUN_ENTER_ANIM: Haven't grabbed props successfully yet")
			ENDIF
			
		BREAK
		
		CASE JAC_CHECK_ENTER_OR_IDLE_ANIM
			IF IS_JACUZZI_EXIT_PROMPT_SHOWN()
				CLEAR_HELP()
			ENDIF
			CLEAR_BIT(iJacuzziBitSet, JAC_BS_EXIT_HELP_TEXT_DISPLAYED)
			
			INT localIdleScene
			localIdleScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iJacuzziNetSceneid)
			IF localIdleScene != -1
				IF GET_SYNCHRONIZED_SCENE_PHASE(localIdleScene) >= 0.990 // Has the idle or enter anim finished yet
					IF IS_BIT_SET(iLocalBS2,LOCAL_BS2_SetJacuzziEnterAnimCam)
					AND GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_THIRD_PERSON_MEDIUM
						SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT, CAM_VIEW_MODE_FIRST_PERSON)
						CLEAR_BIT(iLocalBS2,LOCAL_BS2_SetJacuzziEnterAnimCam)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "JAC_CHECK_ENTER_OR_IDLE_ANIM: setting camera view mode back to first person")
					ENDIF
					iJacuzziNetSceneid = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_MP_JACUZZI_SEAT_ANIM_POSITION(iPlayerChosenSeatSlot, MPJacuzziLocal), GET_MP_JACUZZI_SEAT_ANIM_ROTATION(iPlayerChosenSeatSlot, MPJacuzziLocal), DEFAULT, TRUE, FALSE)
					#IF IS_DEBUG_BUILD
					#ENDIF
					
					CREATE_JACUZZI_BASE_ANIM_SEQUENCE()
					
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iJacuzziNetSceneid, sJaccuzziAnimDict, GET_BASE_CLIP_STRING_FROM_INT(iBaseClipSequence[iBaseClipPlayedCount]), SLOW_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT,DEFAULT,AIK_DISABLE_HEAD_IK)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: PLAYER added to Net Sync Scene, ANIM_DICT: ", sJaccuzziAnimDict,", ANIM_NAME: ", GET_BASE_CLIP_STRING_FROM_INT(iBaseClipSequence[iBaseClipPlayedCount]))
					iBaseClipPlayedCount++
					
					NETWORK_START_SYNCHRONISED_SCENE(iJacuzziNetSceneid)
					CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBitSet, iBS_PLAYER_IN_JACUZZI_PLAYING_IDLE)
					
					SetJacuzziStage(jacActState, JAC_CHECK_BASE_ANIM)
				ENDIF
			ENDIF
		BREAK
		
		CASE JAC_CHECK_BASE_ANIM 
			INT localScene
			localScene  = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iJacuzziNetSceneid)
			IF localScene != -1

				IF GET_SYNCHRONIZED_SCENE_PHASE(localScene) >= 0.990
					iJacuzziNetSceneid = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_MP_JACUZZI_SEAT_ANIM_POSITION(iPlayerChosenSeatSlot, MPJacuzziLocal), GET_MP_JACUZZI_SEAT_ANIM_ROTATION(iPlayerChosenSeatSlot, MPJacuzziLocal), DEFAULT, TRUE, FALSE)
					#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: CREATE net sync scene, SCENE_ID: " , iJacuzziNetSceneid)
					#ENDIF
					
					
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "JAC_CHECK_BASE_ANIM: iBaseClipPlayedCount = ", iBaseClipPlayedCount, ", iBaseClipSeqCount = ", iBaseClipSeqCount)
					IF iBaseClipPlayedCount < iBaseClipSeqCount
						CDEBUG1LN(DEBUG_SAFEHOUSE, "JAC_CHECK_BASE_ANIM: More base sequence clips to be played")
						sJacuzziAnimClip = GET_BASE_CLIP_STRING_FROM_INT(iBaseClipSequence[iBaseClipPlayedCount])
						iBaseClipPlayedCount++
					ELIF IS_LOCAL_PLAYER_ALLOWED_TO_PLAY_IDLE_JACUZZI_CLIP() = FALSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "JAC_CHECK_BASE_ANIM: Idle meant to play but IS_LOCAL_PLAYER_ALLOWED_TO_PLAY_IDLE_JACUZZI_CLIP = FALSE")
						sJacuzziAnimClip = GET_BASE_CLIP_STRING_FROM_INT(GENERATE_RANDOM_JACUZZI_BASE_CLIP())
					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "JAC_CHECK_BASE_ANIM: Going to play idle anim")
						iBaseClipPlayedCount = 0
						iBaseClipSeqCount = 0
						
						sJacuzziAnimClip = GET_JACUZZI_IDLE_ANIM_CLIP()
						
						SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBitSet, iBS_PLAYER_IN_JACUZZI_PLAYING_IDLE)
						SetJacuzziStage(jacActState, JAC_CHECK_ENTER_OR_IDLE_ANIM)
					ENDIF
					
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iJacuzziNetSceneid, sJaccuzziAnimDict, sJacuzziAnimClip, SLOW_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT,DEFAULT,AIK_DISABLE_HEAD_IK)
					IF COMPARE_STRINGS(sJacuzziAnimClip, "idle_e") = 0 // cup anim
						FREEZE_ENTITY_POSITION(NET_TO_ENT(serverBD.jacuzziServerSeatData[iPlayerChosenSeatSlot].niCup), FALSE)
						NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_ENT(serverBD.jacuzziServerSeatData[iPlayerChosenSeatSlot].niCup), iJacuzziNetSceneid, sJaccuzziAnimDict, "idle_e_prop_cup", 6.0, SLOW_BLEND_OUT, SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_PRESERVE_VELOCITY)
					ELIF COMPARE_STRINGS(sJacuzziAnimClip, "idle_d") = 0 // cigar anim
						FREEZE_ENTITY_POSITION(NET_TO_ENT(serverBD.jacuzziServerSeatData[iPlayerChosenSeatSlot].niCigar), FALSE)
						NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_ENT(serverBD.jacuzziServerSeatData[iPlayerChosenSeatSlot].niCigar), iJacuzziNetSceneid, sJaccuzziAnimDict, "idle_d_prop_cigar", 6.0, SLOW_BLEND_OUT, SYNCED_SCENE_ON_ABORT_STOP_SCENE| SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_PRESERVE_VELOCITY)
					ENDIF
					NETWORK_START_SYNCHRONISED_SCENE(iJacuzziNetSceneid)
					#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: PLAYER added to Net Sync Scene, ANIM_DICT: ", sJaccuzziAnimDict,", ANIM_NAME: ", sJacuzziAnimClip)
					#ENDIF
					
				ENDIF
				
			ENDIF
			
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				IF NOT IS_JACUZZI_EXIT_PROMPT_SHOWN()
					CLEAR_BIT(iJacuzziBitSet, JAC_BS_EXIT_HELP_TEXT_DISPLAYED)
				ENDIF
				IF NOT IS_BIT_SET(iJacuzziBitSet, JAC_BS_EXIT_HELP_TEXT_DISPLAYED)
					SET_BIT(iJacuzziBitSet, JAC_BS_EXIT_HELP_TEXT_DISPLAYED)
					SHOW_JACUZZI_EXIT_PROMPT(iPlayerChosenSeatSlot >= JAC_MAX_NUM_SEAT_SLOTS)
				ENDIF
			ELSE
				CLEAR_BIT(iJacuzziBitSet, JAC_BS_EXIT_HELP_TEXT_DISPLAYED)
			ENDIF
			
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "exiting activity")
				iJacuzziNetSceneid = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_MP_JACUZZI_SEAT_ANIM_POSITION(iPlayerChosenSeatSlot, MPJacuzziLocal), GET_MP_JACUZZI_SEAT_ANIM_ROTATION(iPlayerChosenSeatSlot, MPJacuzziLocal), DEFAULT, FALSE, FALSE)
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: CREATE net sync scene, SCENE_ID: " , iJacuzziNetSceneid)
				#ENDIF
																																																
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iJacuzziNetSceneid, sJaccuzziAnimDict, "exit", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT,INSTANT_BLEND_IN,AIK_DISABLE_HEAD_IK)
				NETWORK_START_SYNCHRONISED_SCENE(iJacuzziNetSceneid)
				IF IS_JACUZZI_EXIT_PROMPT_SHOWN()
					CLEAR_HELP()
				ENDIF
				CLEAR_BIT(iJacuzziBitSet, JAC_BS_EXIT_HELP_TEXT_DISPLAYED)
				SetJacuzziStage(jacActState, JAC_CLEANUP)
			ENDIF		
		BREAK
		
		
		CASE JAC_CLEANUP
			
			localScene  = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iJacuzziNetSceneid)
			IF localScene != -1
				IF GET_SYNCHRONIZED_SCENE_PHASE(localScene) >= 0.990
				OR HAS_ANIM_EVENT_FIRED (PLAYER_PED_ID(), HASH("BREAKOUT_FINISH"))
					#IF IS_DEBUG_BUILD
					IF HAS_ANIM_EVENT_FIRED (PLAYER_PED_ID(), HASH("BREAKOUT_FINISH"))
						CDEBUG1LN(DEBUG_SAFEHOUSE, "JAC_CLEANUP: Finishing anim early as BREAKOUT_FINISH anim event fired")
					ENDIF
					#ENDIF 
					NETWORK_STOP_SYNCHRONISED_SCENE(iJacuzziNetSceneid)
					SetJacuzziStage(jacActState, JAC_PUT_ON_CLOTHES)	
					JACUZZI_CLEAN_UP()
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "JAC_CLEANUP: Scene isn't running cleanup anyway")
				SetJacuzziStage(jacActState, JAC_LOCATECHECK)
				JACUZZI_CLEAN_UP()
			ENDIF
			
			playerBD[NATIVE_TO_INT(PLAYER_ID())].iJacuzziSeatUsed = -1
			
		BREAK
		
		CASE JAC_PUT_ON_CLOTHES
//			IF SET_CLOTHING_FOR_JACUZZI_EXIT()
				SetJacuzziStage(jacActState, JAC_LOCATECHECK)
//			ELSE
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "JAC_PUT_ON_CLOTHES: SET_CLOTHING_FOR_JACUZZI_EXIT = FALSE")
//			ENDIF
		BREAK
	ENDSWITCH
	

ENDPROC

FUNC BOOL SHOULD_MEDIA_PLAY()
	RETURN TRUE
ENDFUNC

PROC OVERRIDE_PROPERTY_STRUCT_INDEX_FOR_RADIO(BOOL bOverride)
	IF bOverride
		iStoredPropertyIndex = 0
		IF InteriorPropStruct.iIndex != PROPERTY_YACHT_APT_1_BASE
			iStoredPropertyIndex = InteriorPropStruct.iIndex
			PRINTLN("OVERRIDE_PROPERTY_STRUCT_INDEX_FOR_RADIO: Overriding InteriorPropStruct.iIndex to PROPERTY_YACHT_APT_1_BASE")
			InteriorPropStruct.iIndex = PROPERTY_YACHT_APT_1_BASE
		ENDIF
	ELSE
		IF iStoredPropertyIndex != PROPERTY_YACHT_APT_1_BASE
		AND iStoredPropertyIndex > 0
			PRINTLN("OVERRIDE_PROPERTY_STRUCT_INDEX_FOR_RADIO: resetting InteriorPropStruct.iIndex to iStoredPropertyIndex: ",iStoredPropertyIndex)
			InteriorPropStruct.iIndex = iStoredPropertyIndex
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_CLIENT_PROPERTY_MEDIA()
	IF iLocalStage > STAGE_INIT
	AND iLocalStage < STAGE_BUZZED_INTO_PROPERTY
		IF SHOULD_MEDIA_PLAY()
			INT iPlayer = NATIVE_TO_INT(PLAYER_ID())
			IF iPlayer <> -1
				OVERRIDE_PROPERTY_STRUCT_INDEX_FOR_RADIO(TRUE)
				CLIENT_MAINTAIN_MP_RADIO(serverBD.MPRadioServer, playerBD[iPlayer].MPRadioClient, MPRadioLocal, InteriorPropStruct, playerBD[NATIVE_TO_INT(PLAYER_ID())].iActivityRequested, serverBD.activityControl)
				OVERRIDE_PROPERTY_STRUCT_INDEX_FOR_RADIO(FALSE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC 



PROC MAINTAIN_CALMING_QUADS()
	
	VECTOR vTemp = mpYachts[iCurrentYacht].yachtPropertyDetails.house.vMidPoint
	FLOAT fCalmingQuadSize = 200.0
	
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_MP_PROPERTY_INT")) <= 0
	AND NOT FMMC_CURRENTLY_USING_PLAYER_OWNED_YACHT()
		IF iCalmID = -1
			iCalmID = ADD_EXTRA_CALMING_QUAD(vTemp.x - fCalmingQuadSize, vTemp.y - fCalmingQuadSize, vTemp.x + fCalmingQuadSize, vTemp.y + fCalmingQuadSize, 0.02)
			SET_CALMED_WAVE_HEIGHT_SCALER(0.02)
			PRINTLN("AM_MP_YACHT: ADD_EXTRA_CALMING_QUAD at xMin:", vTemp.x - fCalmingQuadSize," xMax:", vTemp.x + fCalmingQuadSize," yMin:", vTemp.y - fCalmingQuadSize, " yMax:", vTemp.y + fCalmingQuadSize, "ID #", iCalmID)
		ENDIF
	ELSE
		IF NOT IS_PLAYER_SPECTATING(PLAYER_ID())
			IF iCalmID != -1
				REMOVE_EXTRA_CALMING_QUAD(iCalmID)
				SET_CALMED_WAVE_HEIGHT_SCALER(1)
				PRINTLN("AM_MP_YACHT: REMOVE_EXTRA_CALMING_QUAD maintain ID #",iCalmID)
				iCalmID = -1
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Kills any fire that might happen in the jacuzzi
PROC MAINTAIN_FIREPROOF_JACUZZI()
	IF GET_NUMBER_OF_FIRES_IN_RANGE(vJacuzziCentre, JAC_RADIUS) > 0
		STOP_FIRE_IN_RANGE(vJacuzziCentre, JAC_RADIUS)
		
		CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_FIREPROOF_JACUZZI - Fire has been killed by player ", NATIVE_TO_INT(PLAYER_ID()))
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_BLOCKED_FROM_YACHT_ENTRY()
	IF GB_IS_GLOBAL_CLIENT_BIT0_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_0_TEMPORARY_PARTICIPANT) 
	AND MPGlobalsAmbience.iYachtAttackYacht = iCurrentYacht
		RETURN TRUE
	ENDIF
	IF g_sMPTunables.byacht_disable_access
		PRINTLN("IS_PLAYER_BLOCKED_FROM_YACHT_ENTRY()- true g_sMPTunables.byacht_disable_access")
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC CLIENT_MAINTAIN_FIRE_CHECK()
	IF serverBD.bServerStopFire
		STOP_FIRE_IN_RANGE(mpYachts[iCurrentYacht].yachtPropertyDetails.house.vMidPoint,100)
		CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBitSet, iBS_PLAYER_BD_FIRE_IN_RANGE)
		//REINIT_NET_TIMER(fireCheckTimer,TRUE)	
		PRINTLN("CLIENT_MAINTAIN_FIRE_CHECK - player killing nearby fire")
	ENDIF
	
	IF HAS_NET_TIMER_EXPIRED(fireCheckTimer,500,TRUE)
		IF GET_NUMBER_OF_FIRES_IN_RANGE(mpYachts[iCurrentYacht].yachtPropertyDetails.house.vMidPoint,100) >= 1
			IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBitSet, iBS_PLAYER_BD_FIRE_IN_RANGE)
				SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBitSet, iBS_PLAYER_BD_FIRE_IN_RANGE)
				PRINTLN("CLIENT_MAINTAIN_FIRE_CHECK - player thinks there is fire nearby")
			ENDIF
		ELSE
			IF IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBitSet, iBS_PLAYER_BD_FIRE_IN_RANGE)
				CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBitSet, iBS_PLAYER_BD_FIRE_IN_RANGE)
				PRINTLN("CLIENT_MAINTAIN_FIRE_CHECK - player no longer thinks there is fire nearby")
			ENDIF
		ENDIF
		REINIT_NET_TIMER(fireCheckTimer,TRUE)	
	ENDIF
ENDPROC

PROC SERVER_MAINTAIN_FIRE_CHECK()
	PLAYER_INDEX thePlayer
	
	thePlayer = INT_TO_PLAYERINDEX(iFireSlowLoop)	
	IF IS_NET_PLAYER_OK(thePlayer,FALSE,TRUE)
		IF IS_BIT_SET(playerBD[iFireSlowLoop].iBitSet, iBS_PLAYER_BD_FIRE_IN_RANGE)
			bClientFire = TRUE
		ENDIF
	ENDIF
	iFireSlowLoop++
	IF iFireSlowLoop >= NUM_NETWORK_PLAYERS
		iFireSlowLoop = 0
		IF bClientFire 
			START_NET_TIMER(serverFireKillTime,TRUE)
			bClientFire = FALSE
		ELSE
			RESET_NET_TIMER(serverFireKillTime)
			IF serverBD.bServerStopFire
				serverBD.bServerStopFire = FALSE
				PRINTLN("SERVER_MAINTAIN_FIRE_CHECK- bServerStopFire = FALSE")
			ENDIF
		ENDIF
	ENDIF
	IF HAS_NET_TIMER_STARTED(serverFireKillTime)
		IF HAS_NET_TIMER_EXPIRED(serverFireKillTime,5000,TRUE)
			IF NOT serverBD.bServerStopFire
				serverBD.bServerStopFire = TRUE
				PRINTLN("SERVER_MAINTAIN_FIRE_CHECK- bServerStopFire = TRUE")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SHOULD_YACHT_SCRIPT_RESTART()
	YACHT_APPEARANCE Appearance
	BOOL bTryToCleanup
	INT iBridgeDoor
	IF NOT IS_BIT_SET(iLocalBS2,LOCAL_BS2_StoredYachtVariation)
		GET_APPEARANCE_OF_YACHT(iCurrentYacht, yachtAppearanceStored)
		SET_BIT(iLocalBS2,LOCAL_BS2_StoredYachtVariation)
		PRINTLN("SHOULD_YACHT_SCRIPT_RESTART: stored yacht variation")
	ELSE
		GET_APPEARANCE_OF_YACHT(iCurrentYacht, Appearance)
		IF Appearance.iOption != yachtAppearanceStored.iOption
			PRINTLN("SHOULD_YACHT_SCRIPT_RESTART: option has changed restarting")
			bTryToCleanup = TRUE
		ELIF Appearance.iRailing != yachtAppearanceStored.iRailing
			PRINTLN("SHOULD_YACHT_SCRIPT_RESTART: railing has changed restarting")
			bTryToCleanup = TRUE
		ENDIF
		IF bTryToCleanup = TRUE
			IF IS_LOCAL_PLAYER_DOING_FADE_SCENE_FOR_YACHT() 
				IF IS_SCREEN_FADED_OUT()
				OR IS_BROWSER_OPEN()
					PRINTLN("SHOULD_YACHT_SCRIPT_RESTART: browser or faded killing")
					CLEANUP_SCRIPT()
				ENDIF
			ELSE
				PRINTLN("SHOULD_YACHT_SCRIPT_RESTART: killing not doing fade")
				CLEANUP_SCRIPT()
			ENDIF
		ENDIF
	ENDIF
	IF iYachtTintStored = -1
		iYachtTintStored = GET_TINT_OF_PRIVATE_YACHT(iCurrentYacht)
	ELIF iYachtTintStored != GET_TINT_OF_PRIVATE_YACHT(iCurrentYacht)
		REPEAT 2 iBridgeDoor
			IF DOES_ENTITY_EXIST(objID_BridgeDoor[iBridgeDoor])
				SET_OBJECT_TINT_INDEX(objID_BridgeDoor[iBridgeDoor],GET_TINT_OF_PRIVATE_YACHT(iCurrentYacht))
			ENDIF
		ENDREPEAT
		IF DOES_ENTITY_EXIST(yachtDoor)
			SET_OBJECT_TINT_INDEX(yachtDoor,GET_TINT_OF_PRIVATE_YACHT(iCurrentYacht))
		ENDIF
		iYachtTintStored = GET_TINT_OF_PRIVATE_YACHT(iCurrentYacht)
		PRINTLN("AM_MP_YACHT: Updated tint of exterior doors.")
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC CREATE_WIDGETS()
	#IF ACTIVATE_PROPERTY_CS_DEBUG
	INT iBuilding
	#ENDIF
	TEXT_LABEL_63 str = "AM_MP_YACHT - "
	str += iCurrentYacht
	
	START_WIDGET_GROUP(str)
		ADD_WIDGET_BOOL("Print full debug",db_bFullDebug)
		ADD_WIDGET_BOOL("g_bLauncherWillControlMiniMap",g_bLauncherWillControlMiniMap)
		ADD_WIDGET_BOOL("Block player BEAST",db_bBlockPlayerBeast)
		ADD_WIDGET_BOOL("Block player GEN",db_bBlockPlayerGen)
		ADD_WIDGET_BOOL("Clear player block",db_bClearBlockPlayer)
		ADD_WIDGET_BOOL("Clear flames",db_bClearFlames)
		ADD_WIDGET_BOOL("g_bRunGroupYachtExit",g_bRunGroupYachtExit)
		
		
		START_WIDGET_GROUP("Docking debug")
			ADD_WIDGET_BOOL("db_bDrawDockingDebug", db_bDrawDockingDebug)
			ADD_WIDGET_FLOAT_SLIDER("PRIVATE_YACHT_DOCKING_RADIUS", PRIVATE_YACHT_DOCKING_RADIUS, 0.0, 100.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("PRIVATE_YACHT_DOCKING_MAX_Y", PRIVATE_YACHT_DOCKING_MAX_Y, -100.0, 100.0, 0.01)
			ADD_WIDGET_BOOL("db_bRecomputeDockingPoints", db_bRecomputeDockingPoints)
			ADD_WIDGET_FLOAT_SLIDER("PRIVATE_YACHT_DOCKING_MIN_DISTANCE_TO_EDGE", PRIVATE_YACHT_DOCKING_MIN_DISTANCE_TO_EDGE, 0.0, 100.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("PRIVATE_YACHT_DOCKING_ZONE_WIDTH", PRIVATE_YACHT_DOCKING_ZONE_WIDTH, 0.0, 100.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("PRIVATE_YACHT_DOCKING_ZONE_LENGTH", PRIVATE_YACHT_DOCKING_ZONE_LENGTH, 0.0, 100.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("PRIVATE_YACHT_DOCKING_ZONE_HEADING", PRIVATE_YACHT_DOCKING_ZONE_HEADING, -360.0, 360.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("PRIVATE_YACHT_DOCKING_ZONE_X", PRIVATE_YACHT_DOCKING_ZONE_X, -100.0, 100.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("PRIVATE_YACHT_DOCKING_ZONE_Y", PRIVATE_YACHT_DOCKING_ZONE_Y, -100.0, 100.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("PRIVATE_YACHT_DOCKING_MAX_ANCHORING_DISTANCE", PRIVATE_YACHT_DOCKING_MAX_ANCHORING_DISTANCE, 0.0, 100.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("PRIVATE_YACHT_DOCKING_MAX_DISTANCE_FOR_AUTO_DOCKING", PRIVATE_YACHT_DOCKING_MAX_DISTANCE_FOR_AUTO_DOCKING, 0.0, 100.0, 0.01)
		STOP_WIDGET_GROUP()
		
		
		START_WIDGET_GROUP("Jacuzzi Debug")
			ADD_WIDGET_INT_SLIDER("Force Jacuzzi sitting Anim variation ",iDebugJacuzziSittingAnimVariation,0,5,1)
			ADD_WIDGET_INT_SLIDER("Force Jacuzzi sitting Anim clip ",iDebugJacuzziSittingAnimClip,0,4,1)
			ADD_WIDGET_BOOL("Draw Jacuzzi Inner sphere ",db_bDrawDebugJacuzziSphereInner)
			ADD_WIDGET_FLOAT_SLIDER("Set size of inner sphere ",fDebugJacuzziInnerSphereSize,0,8,0.5)
			ADD_WIDGET_BOOL("Draw Jacuzzi Outter sphere ",db_bDrawDebugJacuzziSphereOutter)
			ADD_WIDGET_FLOAT_SLIDER("Set size of outter sphere ",fDebugJacuzziOutterSphereSize,0,8,0.5)
			ADD_WIDGET_BOOL("Draw Jacuzzi seats positions ",db_bDrawDebugJacuzziSeatPositions)
			ADD_WIDGET_BOOL("Force all slots to be cigar slots ",db_bJacuzziForceCigarSlots)
			ADD_WIDGET_BOOL("Force all slots to be cup slots ",db_bJacuzziForceCupSlots)
			ADD_WIDGET_FLOAT_SLIDER("fJacuzziBaseSeatRadius ",fJacuzziBaseSeatRadius,0,3,0.01)
			ADD_WIDGET_FLOAT_SLIDER("fJacuzziBaseSeatHeight ",fJacuzziBaseSeatHeight,-10,10,0.01)
			ADD_WIDGET_FLOAT_SLIDER("fJacuzziRelativeWaterLevel ",fJacuzziRelativeWaterLevel,-10,10,0.01)
			ADD_WIDGET_FLOAT_READ_ONLY("fJacuzziActualWaterLevel", fJacuzziActualWaterLevel)
			ADD_WIDGET_FLOAT_SLIDER("fJacuzziApproachOffset ",fJacuzziApproachOffset,-1,1,0.01)
			ADD_WIDGET_FLOAT_SLIDER("fJacuzziCameraMinPitch_LOCATECHECK ",fJacuzziCameraMinPitch_LOCATECHECK,-360,360,0.01)
			ADD_WIDGET_FLOAT_SLIDER("fJacuzziCameraMaxPitch_LOCATECHECK ",fJacuzziCameraMaxPitch_LOCATECHECK,-360,360,0.01)
			ADD_WIDGET_FLOAT_SLIDER("fJacuzziCameraMinPitch_CLEANUP ",fJacuzziCameraMinPitch_CLEANUP,-360,360,0.01)
			ADD_WIDGET_FLOAT_SLIDER("fJacuzziCameraMaxPitch_CLEANUP ",fJacuzziCameraMaxPitch_CLEANUP,-360,360,0.01)
			ADD_WIDGET_FLOAT_SLIDER("fJacuzziCameraMinPitch_REQUEST ",fJacuzziCameraMinPitch_REQUEST,-360,360,0.01)
			ADD_WIDGET_FLOAT_SLIDER("fJacuzziCameraMaxPitch_REQUEST ",fJacuzziCameraMaxPitch_REQUEST,-360,360,0.01)
			
			
			INT i
			TEXT_LABEL_63 str2
			REPEAT JAC_MAX_NUM_SEAT_SLOTS i
				str2 = "fSeatRadiusOffsets["
				str2 += i
				str2 += "] "
				ADD_WIDGET_FLOAT_SLIDER(str2,mpJacuzziLocal.fSeatRadiusOffsets[i],-1,1,0.01)
			ENDREPEAT
			REPEAT JAC_MAX_NUM_SEAT_SLOTS i
				str2 = "fSeatAngleOffsets["
				str2 += i
				str2 += "] "
				ADD_WIDGET_FLOAT_SLIDER(str2,mpJacuzziLocal.fSeatAngleOffsets[i],-180,180,0.05)
			ENDREPEAT
			REPEAT JAC_MAX_NUM_SEAT_SLOTS i
				str2 = "fSeatHeadingOffsets["
				str2 += i
				str2 += "] "
				ADD_WIDGET_FLOAT_SLIDER(str2,mpJacuzziLocal.fSeatHeadingOffsets[i],-180,180,0.05)
			ENDREPEAT
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Heist Entry Debug")
			ADD_WIDGET_BOOL("force walk in complete",globalPropertyEntryData.bInteriorWarpDone)
			ADD_WIDGET_BOOL("g_bPropertyDropOff",g_bPropertyDropOff)
			ADD_WIDGET_BOOL("g_bGarageDropOff",g_bGarageDropOff)
			ADD_WIDGET_BOOL("g_bGotMissionCriticalEntity",g_bGotMissionCriticalEntity)
			ADD_WIDGET_INT_SLIDER("g_iHeistPropertyID",g_iHeistPropertyID,-1,MAX_MP_PROPERTIES,1)
		STOP_WIDGET_GROUP()

		START_WIDGET_GROUP("Buzzers")
			ADD_WIDGET_BOOL("Test Buzzer", bDebugTestBuzzer)
			ADD_WIDGET_FLOAT_SLIDER("Shapetest Height Offset", fDebugTestShapeTestHeightOffset, -2.0, 2.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Shapetest Radius", fDebugTestShapeTestRadius, 0.0, 2.0, 0.01)
			ADD_WIDGET_BOOL("Shapetest Debug Draw", bDebugTestShapeTestDraw)
		STOP_WIDGET_GROUP()

		START_WIDGET_GROUP("Jacuzzi")
			ADD_WIDGET_BOOL("Set swimwear", bDebugSetSwimwear)
			ADD_WIDGET_BOOL("Set stored clothing", bDebugSetStoredClothing)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("blockObjectDetails")
			
			str = "mn_GarageModel: \""
			IF blockObjectDetails.mn_GarageModel = DUMMY_MODEL_FOR_SCRIPT
				str += "dummy"
			ELSE
				str += GET_MODEL_NAME_FOR_DEBUG(blockObjectDetails.mn_GarageModel)
			ENDIF
			str += "\""
			ADD_WIDGET_STRING(str)
			ADD_WIDGET_VECTOR_SLIDER("vGarageLoc", blockObjectDetails.vGarageLoc, -5000, 5000, 0.01)
			ADD_WIDGET_VECTOR_SLIDER("vGarageRot", blockObjectDetails.vGarageRot, -180, 180, 1.0)
			
			str = "mn_EntryModel: \""
			IF blockObjectDetails.mn_EntryModel = DUMMY_MODEL_FOR_SCRIPT
				str += "dummy"
			ELSE
				str += GET_MODEL_NAME_FOR_DEBUG(blockObjectDetails.mn_EntryModel)
			ENDIF
			str += "\""
			ADD_WIDGET_STRING(str)
			ADD_WIDGET_VECTOR_SLIDER("vEntryLoc", blockObjectDetails.vEntryLoc, -5000, 5000, 0.01)
			ADD_WIDGET_VECTOR_SLIDER("vEntryRot", blockObjectDetails.vEntryRot, -180, 180, 1.0)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Yacht Name")
			ADD_WIDGET_STRING("PORT")
			ADD_WIDGET_FLOAT_SLIDER("Text Centre X", fPortCentreX, -1000, 1000, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Text Centre Y", fPortCentreY, -1000, 1000, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Text Width", fPortWidth, -1000, 1000, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Text Height", fPortHeight, -1000, 1000, 0.1)
			ADD_WIDGET_STRING("STARB")
			ADD_WIDGET_FLOAT_SLIDER("Text Centre X", fStarbCentreX, -1000, 1000, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Text Centre Y", fStarbCentreY, -1000, 1000, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Text Width", fStarbWidth, -1000, 1000, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Text Height", fStarbHeight, -1000, 1000, 0.1)
			ADD_WIDGET_STRING("STERN")
			ADD_WIDGET_FLOAT_SLIDER("Text Centre X", fSternCentreX, -1000, 1000, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Text Centre Y", fSternCentreY, -1000, 1000, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Text Width", fSternWidth, -1000, 1000, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Text Height", fSternHeight, -1000, 1000, 0.1)
			ADD_WIDGET_STRING("CUSTOM NAME")
			wYachtName = ADD_TEXT_WIDGET("Yacht Name")
			ADD_WIDGET_BOOL("Use Custom Yacht Name", bCustomYachtName)
		STOP_WIDGET_GROUP()
		
	STOP_WIDGET_GROUP()
ENDPROC

PROC MAINTAIN_DEBUG()
	
	IF db_bBlockPlayerBeast
		SET_PLAYER_BLOCK_FROM_PROPERTY_FLAG(PPAF_FREEMODE_EVENT_BEAST)
		db_bBlockPlayerBeast = FALSE
	ENDIF
	
	IF db_bBlockPlayerGen
		SET_PLAYER_BLOCK_FROM_PROPERTY_FLAG(PPAF_GEN)
		db_bBlockPlayerGen = FALSE
	ENDIF
	
	IF db_bClearBlockPlayer
		CLEAR_PLAYER_BLOCK_FROM_PROPERTY_FLAG(PPAF_FREEMODE_EVENT_BEAST)
		CLEAR_PLAYER_BLOCK_FROM_PROPERTY_FLAG(PPAF_GEN)
		db_bClearBlockPlayer = FALSE
	ENDIF
	
	IF db_bClearFlames
		STOP_FIRE_IN_RANGE(mpYachts[iCurrentYacht].yachtPropertyDetails.house.vMidPoint,100)
		db_bClearFlames = FALSE
	ENDIF
	
	IF bDebugSetSwimwear
		PLAYER_WEAR_SWIMWEAR(SwimwearData, serverBD.iSwimwearOptions[NATIVE_TO_INT(PLAYER_ID())])
		IF IS_BIT_SET(SwimwearData.iJacuzziClothingFlags, JAACUZZI_PLAYER_WEARING_SWIMSUIT)
			bDebugSetSwimwear = FALSE
		ENDIF
	ENDIF
	IF bDebugSetStoredClothing
		PLAYER_RESTORE_CLOTHES(SwimwearData)
		IF NOT IS_BIT_SET(SwimwearData.iJacuzziClothingFlags, JAACUZZI_PLAYER_WEARING_SWIMSUIT)
			bDebugSetStoredClothing = FALSE
		ENDIF
	ENDIF

	IF db_bRecomputeDockingPoints
		LocalDockingData.iDockingStage = -1
		db_bRecomputeDockingPoints = FALSE
	ENDIF
	
ENDPROC
#ENDIF

// Staggered loop to check for an active sell mission
PROC CHECK_FOR_SELL_MISSION()
	IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(m_iParticipantSellCheck))
		PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(m_iParticipantSellCheck))
		
		// If the current active participant is in a sell mission, set the server bit
		IF GlobalplayerBD_FM_3[NATIVE_TO_INT(PlayerId)].sMagnateGangBossData.iActiveMission = FMMC_TYPE_GB_CONTRABAND_SELL
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, ciSERVER_BS_SELL_MISSION_ACTIVE)
				SET_BIT(serverBD.iServerBitSet, ciSERVER_BS_SELL_MISSION_ACTIVE)
			ENDIF
		ENDIF
	ENDIF
	
	m_iParticipantSellCheck++
	
	IF m_iParticipantSellCheck >= NETWORK_GET_MAX_NUM_PARTICIPANTS()
		m_iParticipantSellCheck = 0
	ENDIF
ENDPROC

// Check each active participant for active sell mission
FUNC BOOL CHECK_FOR_NO_SELL_MISSION()
	INT iParticipant
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
			
			// Leave the function if active sell mission is found
			IF GlobalplayerBD_FM_3[NATIVE_TO_INT(PlayerId)].sMagnateGangBossData.iActiveMission = FMMC_TYPE_GB_CONTRABAND_SELL
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_IMPROMPTU_DEATHMATCH_CHECK()
	IF IS_PLAYER_ON_BIKER_DM()
	OR IS_PLAYER_ON_IMPROMPTU_DM()
		IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_ON_IMPROMPTU_DEATHMATCH)
			SET_BIT(iLocalBS, LOCAL_BS_ON_IMPROMPTU_DEATHMATCH)
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBS, LOCAL_BS_ON_IMPROMPTU_DEATHMATCH)
			CLEAR_BIT(iLocalBS, LOCAL_BS_ON_IMPROMPTU_DEATHMATCH)
		ENDIF
	ENDIF
ENDPROC

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                             ////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT
	IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: cleaning up as SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE() BEFORE PROCESS PREGAME COMPLETE!")
		CLEANUP_SCRIPT()
	ENDIF
	INIT_CURRENT_YACHT_DETAILS()
	IF NOT PROCESS_PRE_GAME()
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: cleaning up as script failed to receive an initial network broadcast.")
		CLEANUP_SCRIPT()
	ENDIF
	
	// Reserve for yacht name props
	RESERVE_NETWORK_MISSION_OBJECTS(3)
	
	//GET_MP_PROP_BLOCKING_OBJECT_DETAILS(blockObjectDetails,GET_PROPERTY_BUILDING(iCurrentYacht))
	START_NET_TIMER(tempPropertyExtMenu.exitDoorDelay,TRUE)
	
	iWarpingOutOfPropertyID = g_iWarpingOutOfPropertyWithID 
	CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: iWarpingOutOfPropertyID = ",iWarpingOutOfPropertyID)
	
	#IF IS_DEBUG_BUILD
	CREATE_WIDGETS()
	#ENDIF
	
	INT i
	PLAYER_INDEX playerID
//	VECTOR vTemp = mpYachts[iCurrentYacht].yachtPropertyDetails.house.vMidPoint	
//	VECTOR min = <<vTemp.x-100,vTemp.y-100,vTemp.z-1>>
//	VECTOR max = <<vTemp.x+100,vTemp.y+100,vTemp.z+1>>
	
	MPRadioLocal.bOnYachtDeck = TRUE
	MPRadioLocal.iYachtID = iCurrentYacht
	MPRadioLocal.piApartmentOwner = GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iCurrentYacht)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "MPRadioLocal.iYachtID = ", MPRadioLocal.iYachtID)
	IF NATIVE_TO_INT(MPRadioLocal.piApartmentOwner) > -1
		CDEBUG1LN(DEBUG_SAFEHOUSE, "MPRadioLocal.piApartmentOwner = ", GET_PLAYER_NAME(MPRadioLocal.piApartmentOwner))
	ELSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "MPRadioLocal.piApartmentOwner = UNASSIGNED, no owner of Yacht ")
	ENDIF
	REQUEST_YACHT_EFFECTS()
	
	// Reset bridge entry.
	g_iYachtBridgeEntryStage = YACHT_BRIDGE_FLAG_TRIGGER_LOCATE
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT - Resetting vehicle access for mission")
		
		SET_LOCAL_PLAYER_BD_YACHT_ACCESS(0)
	ENDIF
	
	WHILE TRUE
//		DRAW_DEBUG_BOX(min,max)
		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
		
		MAINTAIN_CLIENT_PROPERTY_MEDIA()
		
		IF HAS_NET_TIMER_STARTED(cinematicDelay)
			IF NOT HAS_NET_TIMER_EXPIRED(cinematicDelay,500,TRUE)
				SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
				DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
			ELSE
				RESET_NET_TIMER(cinematicDelay)
			ENDIF
		ENDIF
		//reset at the start of each frame
		iKnowOwnerState = 0
		// If we have a match end event, bail.
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: cleaning up as SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()")
			IF iLocalStage >= STAGE_SETUP_FOR_USING_PROPERTY
				CLEAR_GLOBAL_ENTRY_DATA()
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: clearing global data for multiplayer thread terminate")
			ENDIF
			CLEANUP_SCRIPT()
		ENDIF
		
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			IF iLocalStage <= STAGE_USING_MENU
			AND GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()),vYachtLoc,FALSE) > 325 
			AND NOT IS_SPECTATED_PLAYER_NEAR_YACHT(iCurrentYacht,325)
				IF NOT IS_PLAYER_TELEPORT_ACTIVE()
				AND NOT IS_PLAYER_IN_CORONA()
				AND NOT IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: cleaning up as outside of distance")
					CLEANUP_SCRIPT()
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: want to clean up as outside distance, but player teleport active!")
				ENDIF
			ENDIF
		ENDIF
		
		
		
		INT iNearestYachtID = GET_NEAREST_ACTIVE_PRIVATE_YACHT_TO_PLAYER(PLAYER_ID())
		IF IS_PRIVATE_YACHT_ID_VALID(iNearestYachtID)
			IF (IS_PLAYER_NEAR_YACHT(PLAYER_ID(),iNearestYachtID)
			OR IS_PLAYER_ON_YACHT(PLAYER_ID(),iNearestYachtID))
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
				IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_DOWN) 
				AND IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_YACHT_INFO_DISPLAY)
					CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_YACHT_INFO_DISPLAY)
				ELIF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_YACHT_INFO_DISPLAY)
				AND NOT IS_PHONE_ONSCREEN()
				AND NOT Is_Player_Currently_On_MP_Heist(PLAYER_ID())
				AND NOT Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
				AND NOT IS_INTERACTION_MENU_OPEN()
				AND NOT IS_BROWSER_OPEN()
				AND NOT IS_SELECTOR_ONSCREEN()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
					DISPLAY_YACHT_INFO(iNearestYachtID,yachtDisplayInfoStruct)
				ENDIF	
			ENDIF
		ENDIF
		
		IF NOT IS_PRIVATE_YACHT_ACTIVE(iCurrentYacht)
			IF NOT IS_PLAYER_TELEPORT_ACTIVE()
			AND NOT IS_PLAYER_IN_CORONA()
			AND NOT IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: cleaning up as yacht not active: ",iCurrentYacht)
				CLEANUP_SCRIPT()
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: want to clean up as yacht not active, but player teleport active!: ",iCurrentYacht)
			ENDIF
		ENDIF
		
		SHOULD_YACHT_SCRIPT_RESTART()
		
		//IF GET_FRAME_COUNT() % 60 = 0
		//	CDEBUG1LN(DEBUG_SAFEHOUSE, "we are on yacht option: ", GET_OPTION_OF_PRIVATE_YACHT(iCurrentYacht))
		//ENDIF
		
		IF g_iHeistLeaderPlayerID >= 0
			IF NOT IS_GAMER_HANDLE_VALID(heistLeader)
				IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(g_iHeistLeaderPlayerID))
					heistLeader = GET_GAMER_HANDLE_PLAYER(INT_TO_PLAYERINDEX(g_iHeistLeaderPlayerID))
					#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: Assigning heistLeader from player: ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(g_iHeistLeaderPlayerID)), " Handle to be:")
					DEBUG_PRINT_GAMER_HANDLE(heistLeader)
					#ENDIF
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: NOT Assigning heistLeader player NOT ok!")
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(entryCutData.iBS,ENTRY_CS_BS_STARTED_MP_CUTSCENE)
			REPEAT NUM_NETWORK_PLAYERS i
				playerID = INT_TO_PLAYERINDEX(i)
				IF playerID != PLAYER_ID()
					IF IS_NET_PLAYER_OK(playerID,FALSE,FALSE)
						IF IS_PLAYER_IN_CUTSCENE(playerID)
							SET_PLAYER_INVISIBLE_LOCALLY(playerID,TRUE)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT:hiding player in cutscene: ", GET_PLAYER_NAME(playerID))
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF

		#IF IS_DEBUG_BUILD
		MAINTAIN_DEBUG()
		#ENDIF
		
		KILL_ON_CALL_FOR_TRANSITION_STAGES()
		IF IS_PAUSE_MENU_ACTIVE_EX()
			REINIT_NET_TIMER(pauseMenuDelay,TRUE)
		ELSE
			IF HAS_NET_TIMER_EXPIRED_READ_ONLY(pauseMenuDelay,1500,TRUE)
				RESET_NET_TIMER(pauseMenuDelay)
			ENDIF
		ENDIF
		
		//CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: iLocalStage: ", iLocalStage)
		
		// If there is an active sell mission, keep checking until it is gone
		IF IS_BIT_SET(serverBD.iServerBitSet, ciSERVER_BS_SELL_MISSION_ACTIVE)
			IF CHECK_FOR_NO_SELL_MISSION()
				CLEAR_BIT(serverBD.iServerBitSet, ciSERVER_BS_SELL_MISSION_ACTIVE)
			ENDIF
		ELSE
			// Check if any player has a sell mission active in a staggered loop
			CHECK_FOR_SELL_MISSION()
		ENDIF
		
		MAINTAIN_YACHT_DOOR_VISIBILITY()
		MAINTAIN_YACHT_CAPTAIN()
		MAINTAIN_YACHT_MOVE()
		MAINTAIN_YACHT_NAME()
		MAINTAIN_YACHT_BRIDGE_ENTRY()
		CLIENT_MAINTAIN_FIRE_CHECK()
		MAINTAIN_YACHT_DOCKING()
		MAINTAIN_YACHT_CLOTHING_GIFT()
		MAINTAIN_IMPROMPTU_DEATHMATCH_CHECK()
		MAINTAIN_NAV_BLOCKING_FOR_MISSION() // url:bugstar:2966159
		MAINTAIN_EXTERIOR_MAP()
		
		SWITCH iLocalStage
			CASE STAGE_INIT
				
				//IF CREATE_ENTRY_AREAS_MAP_HOLE_PROTECTORS()
				
				IF CREATE_LOCAL_BLOCKING_OBJECTS()
				AND CREATE_BUZZER_PROPS_IF_NEEDED()
				AND CREATE_YACHT_DOOR()
					STORE_JACUZZI_POSITIONS()
					INITIALISE_LEAN_STRUCT_DATA()
					STORE_YACHT_LEAN_SEAT_POSITIONS(mpYachtLeanLocal)
					SET_LOCAL_STAGE(STAGE_CHECK_FOR_IN_LOCATE)
				#IF IS_DEBUG_BUILD
				ELSE
					
//					IF NOT CREATE_ENTRY_AREAS_MAP_HOLE_PROTECTORS()
//						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: NOT CREATE_ENTRY_AREAS_MAP_HOLE_PROTECTORS")
					IF NOT CREATE_LOCAL_BLOCKING_OBJECTS()
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: NOT CREATE_LOCAL_BLOCKING_OBJECTS")
					ELIF NOT CREATE_BUZZER_PROPS_IF_NEEDED()
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: NOT CREATE_BUZZER_PROPS_IF_NEEDED")
					ELIF NOT CREATE_YACHT_DOOR()
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: NOT CREATE_YACHT_DOOR")
					ENDIF
					IF NATIVE_TO_INT(NETWORK_GET_HOST_OF_THIS_SCRIPT()) >= 0
						IF NETWORK_IS_PARTICIPANT_ACTIVE(NETWORK_GET_HOST_OF_THIS_SCRIPT())
							playerID = NETWORK_GET_PLAYER_INDEX( NETWORK_GET_HOST_OF_THIS_SCRIPT())
							IF IS_NET_PLAYER_OK(playerID)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: waiting for host: ",GET_PLAYER_NAME(playerID), " to create object")
							ENDIF
						ENDIF
					ENDIF
				#ENDIF
				ENDIF
			BREAK
		
			CASE STAGE_CHECK_FOR_IN_LOCATE
			
				MAINTAIN_GROUP_EXIT_BRIDGE()
				
				IF NOT IS_LEAN_ACTIVITY_DISABLED()
					RUN_YACHT_LEAN_ACTIVITIES()
				ENDIF
				
				IF NOT IS_JACUZZI_ACTIVITY_DISABLED()
					RUN_JACUZZI_ACTIVITY()
				ENDIF
				MAINTAIN_FIREPROOF_JACUZZI()
			
				IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(),TRUE)
				OR g_bPropInteriorScriptRunning
					IF NOT GlobalPropertyEntryData.bLeftOrRejectedBuzzer
					AND NOT g_bPlayerInProcessOfExitingInterior
					AND globalPropertyEntryData.iPropertyEntered <= 0
					AND NOT IS_SPECTATED_PLAYER_NEAR_YACHT(iCurrentYacht,325)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: Cleaning up script as player is inside property.bLeftOrRejectedBuzzer = ",GlobalPropertyEntryData.bLeftOrRejectedBuzzer,
													" g_bPropInteriorScriptRunning = ",g_bPropInteriorScriptRunning,
													" IS_PLAYER_IN_PROPERTY = ", IS_PLAYER_IN_PROPERTY(PLAYER_ID(),TRUE))
						CLEANUP_SCRIPT()
						BREAK
					ENDIF
				ENDIF
				MAINTAIN_CALMING_QUADS()
				IF g_bPlayerInProcessOfExitingInterior
					bPlayerWalkingOut = TRUE
					#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: g_bPlayerInProcessOfExitingInterior = TRUE this frame")
					#ENDIF
				
				ENDIF

				CLEAR_BIT(iLocalBS2,LOCAL_BS2_bKeepWantedFlag)
				bKeepInvalidText = FALSE
				CLEAR_BIT(iLocalBS2,LOCAL_BS2_bKeepPurchaseContext)
				IF IS_PLAYER_TELEPORT_ACTIVE()
				OR IS_PLAYER_IN_CORONA()
				OR IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: Reseting timer player is warping or in corona")
					REINIT_NET_TIMER(tempPropertyExtMenu.exitDoorDelay,TRUE)
				ENDIF
				
//				IF IS_NET_PLAYER_OK(PLAYER_ID())
//					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),mpYachts[iCurrentYacht].ext_EntanceArea.vPos1,mpYachts[iCurrentYacht].ext_EntanceArea.vPos2,mpYachts[iCurrentYacht].ext_EntanceArea.fWidth)
//						bKeepInvalidText = TRUE
//						IF NOT HAS_NET_TIMER_STARTED(failSafeWarp)
//							START_NET_TIMER(failSafeWarp,TRUE)
//						ELSE
//							IF HAS_NET_TIMER_EXPIRED_READ_ONLY(failSafeWarp,45000,TRUE)
//								SET_ENTITY_COORDS(PLAYER_PED_ID(),mpYachts[iCurrentYacht].yachtPropertyDetails.house.exits[0].vOutPlayerLoc)
//							ENDIF
//						ENDIF
//					ELSE
//						RESET_NET_TIMER(failSafeWarp)
//					ENDIF
//				ENDIF
				#IF IS_DEBUG_BUILD
					iCantEnterFramePrintDelay++
					IF iCantEnterFramePrintDelay >= 30
//						db_bFullDebug = TRUE
						IF iCantEnterFramePrintDelay > 32
							iCantEnterFramePrintDelay = 0
							db_bFullDebug = FALSE
						ENDIF
//						CDEBUG1LN(DEBUG_SAFEHOUSE, "db_bFullDebug = ", db_bFullDebug)
					ENDIF
				#ENDIF

				IF IS_FM_TYPE_UNLOCKED(FMMC_TYPE_CUSTOM_CAR_GARAGE)
					iRejectedEntryBS = 0
					IF IS_NET_PLAYER_OK(PLAYER_ID())	
					
						IF CAN_PLAYER_USE_PROPERTY(FALSE #IF IS_DEBUG_BUILD, db_bFullDebug #ENDIF)
							IF NOT g_bPlayerInProcessOfExitingInterior
								IF NOT IS_PAUSE_MENU_ACTIVE_EX()
								AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
								AND NOT IS_CUSTOM_MENU_ON_SCREEN()
								AND NOT IS_BROWSER_OPEN()
								AND NOT IS_COMMERCE_STORE_OPEN()
								AND NOT IS_STORE_PENDING_NETWORK_SHUTDOWN_TO_OPEN()
								AND NOT HAS_NET_TIMER_STARTED(pauseMenuDelay)
								AND NOT IS_INTERACTION_MENU_OPEN()
								AND NOT PROPERTY_HAS_JUST_ACCEPTED_A_MISSION(#IF IS_DEBUG_BUILD db_bFullDebug #ENDIF)
								AND NOT PHONE_MISSION_LAUNCHING_APPS_ACTIVE()
								AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
								AND NOT IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_AcceptedInvite)
								AND NOT IS_TRIP_SKIP_IN_PROGRESS_GLOBAL()
								AND NOT MPGlobalsAmbience.R2Pdata.bLaunchRaceToPoint
								AND NOT IS_PLAYER_BLOCKED_FROM_YACHT_ENTRY()
								
									LOOP_ENTRANCES()
								ELSE
									SET_BIT(iLocalBS2,LOCAL_BS2_bKeepWantedFlag)
									bKeepInvalidText = TRUE
									#IF IS_DEBUG_BUILD
										IF db_bFullDebug
											IF IS_PAUSE_MENU_ACTIVE_EX()
												CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: IS_PAUSE_MENU_ACTIVE()	TRUE ")
											ELIF IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
												CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)	TRUE ")
											ELIF IS_CUSTOM_MENU_ON_SCREEN()
												CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: IS_CUSTOM_MENU_ON_SCREEN()	TRUE ")
											ELIF IS_BROWSER_OPEN()
												CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: IS_BROWSER_OPEN()	TRUE ")
											ELIF IS_INTERACTION_MENU_OPEN()
												CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: IS_INTERACTION_MENU_OPEN()	TRUE ")
											ELIF PROPERTY_HAS_JUST_ACCEPTED_A_MISSION(FALSE)
												CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: PROPERTY_HAS_JUST_ACCEPTED_A_MISSION()	TRUE ")
											ELIF PHONE_MISSION_LAUNCHING_APPS_ACTIVE()
												CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: PHONE_MISSION_LAUNCHING_APPS_ACTIVE()	TRUE ")
											ELIF IS_COMMERCE_STORE_OPEN()
												CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: IS_COMMERCE_STORE_OPEN())	TRUE ")	
											ELIF IS_STORE_PENDING_NETWORK_SHUTDOWN_TO_OPEN()
												CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: IS_STORE_PENDING_NETWORK_SHUTDOWN_TO_OPEN()	TRUE ")
											ELIF HAS_NET_TIMER_STARTED(pauseMenuDelay)
												CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: HAS_NET_TIMER_STARTED(pauseMenuDelay)	TRUE ")
											ELIF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
												CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: IS_PLAYER_CONTROL_ON(PLAYER_ID())	FALSE ")
											ELIF IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_AcceptedInvite)
												CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: biITA_AcceptedInvite set not allowing entry ")
											ELIF IS_TRIP_SKIP_IN_PROGRESS_GLOBAL()
												CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: IS_TRIP_SKIP_IN_PROGRESS_GLOBAL set not allowing entry ")	
											ELIF MPGlobalsAmbience.R2Pdata.bLaunchRaceToPoint
												CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: MPGlobalsAmbience.R2Pdata.bLaunchRaceToPoint set not allowing entry ")	
											ELIF IS_PLAYER_BLOCKED_FROM_YACHT_ENTRY()
												CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: IS_PLAYER_BLOCKED_FROM_YACHT_ENTRY set not allowing entry ")	
											ENDIF
										ENDIF
									#ENDIF
								ENDIF
							#IF IS_DEBUG_BUILD
							ELSE
								IF db_bFullDebug
									CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: NOT - g_bPlayerInProcessOfExitingInterior	")
								ENDIF
							#ENDIF
							ENDIF
						#IF IS_DEBUG_BUILD
						ELSE
							IF db_bFullDebug
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: NOT - CAN_PLAYER_USE_PROPERTY()	")
							ENDIF
						#ENDIF
						ENDIF
					#IF IS_DEBUG_BUILD
					ELSE
						IF db_bFullDebug
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: NOT - IS_NET_PLAYER_OK(PLAYER_ID())	")
						ENDIF
					#ENDIF
					ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					IF db_bFullDebug
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: NOT - IS_FM_TYPE_UNLOCKED(FMMC_TYPE_CUSTOM_CAR_GARAGE)")
					ENDIF
				#ENDIF
				ENDIF
				IF NOT IS_BIT_SET(iLocalBS2,LOCAL_BS2_bKeepWantedFlag)
					IF IS_BIT_SET(ilocalBS, LOCAL_BS_WANTED_HELP_TEXT)
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CUST_GAR_WH")
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CUST_YACHT_WH")
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CUST_GAR_MISO")
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CUST_YAC_MISO")
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CUST_PROP_W")
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROP_GAR_HEI_WOH")
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROP_YACHT_HEI_WOH")
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PPA_GENYACHT0")
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PPA_GEN1")
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PPA_BEAST0")
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PPA_BEAST1")
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PPA_CONTRA3")
							CLEAR_HELP()
						ENDIF
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: Clearing wanted help message")
						CLEAR_BIT(ilocalBS,LOCAL_BS_WANTED_HELP_TEXT)
					ENDIF
				ENDIF
				IF NOT bKeepInvalidText
					IF iWarpingOutOfPropertyID != 0
						IF NOT IS_PLAYER_TELEPORT_ACTIVE()
						AND NOT IS_PLAYER_IN_CORONA()
						AND NOT g_bPlayerInProcessOfExitingInterior
						AND NOT IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
							IF bPlayerWalkingOut
								REINIT_NET_TIMER(tempPropertyExtMenu.exitDoorDelay,TRUE)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: tempPropertyExtMenu.exitDoorDelay started as player is walking out of apartment")
								bPlayerWalkingOut = FALSE
							ENDIF
							CLEAR_ENTERING_DISABLED_DUE_TO_EXIT_WARP()
						ENDIF
					ENDIF
					IF iBuzzerContextIntention != NEW_CONTEXT_INTENTION
						RELEASE_CONTEXT_INTENTION(iBuzzerContextIntention)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: Releasing the context intention for the buzzer.")
					ENDIF
					CLEAR_INVALID_VEHICLE_HELP()
				ENDIF
				IF IS_BIT_SET(iLocalBS2,LOCAL_BS2_bKeepWantedFlag)
				OR bKeepInvalidText
					IF IS_BIT_SET(ilocalBS, LOCAL_BS_WANTED_HELP_TEXT)
					OR IS_BIT_SET(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
					OR IS_ANY_CONTEXT_ACTIVE()
						IF iPurchaseContextIntention = NEW_CONTEXT_INTENTION 
						AND iBuzzerContextIntention = NEW_CONTEXT_INTENTION
							SUPPRESS_CONTEXT_BUTTON_ACTIONS_THIS_FRAME()
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: suppressing context for help text")
						ENDIF
						//IS_CONTEXT_INTENTION_HELP_DISPLAYING(iPurchaseContextIntention))
					ENDIF
				ENDIF
				IF NOT IS_BIT_SET(iLocalBS2,LOCAL_BS2_bKeepPurchaseContext)
					IF iPurchaseContextIntention != NEW_CONTEXT_INTENTION
						RELEASE_CONTEXT_INTENTION(iPurchaseContextIntention)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: Releasing the context intention for the purchase menu.")
					ENDIF
				ENDIF
				
			BREAK
			CASE STAGE_SETUP_FOR_MENU
				IF iBuzzerContextIntention != NEW_CONTEXT_INTENTION
					RELEASE_CONTEXT_INTENTION(iBuzzerContextIntention)
				ENDIF
				IF iPurchaseContextIntention != NEW_CONTEXT_INTENTION
					RELEASE_CONTEXT_INTENTION(iPurchaseContextIntention)
				ENDIF
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
				AND NOT g_bMissionEnding
				AND NOT g_bAbortPropertyMenus
				AND NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
				AND CAN_PLAYER_USE_PROPERTY(FALSE)//SHOULD_PLAYER_BYPASS_GENERAL_ACCESS_CHECKS_FOR_HEIST())
				AND NOT PROPERTY_HAS_JUST_ACCEPTED_A_MISSION(#IF IS_DEBUG_BUILD db_bFullDebug #ENDIF)
					IF NOT MP_FORCE_TERMINATE_INTERNET_ACTIVE()
						MP_FORCE_TERMINATE_INTERNET()
						SET_BIT(iLocalBS, LOCAL_BS_FORCE_TERMINATED_INTERNET)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: Force terminating internet for entry into property.-1")
					ENDIF
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
					
//					IF NOT bLockCarOnMenu
//						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//							IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
//								carDoorLocks = GET_VEHICLE_DOOR_LOCK_STATUS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
//								IF NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
//									SET_VEHICLE_DOORS_LOCKED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),VEHICLELOCK_LOCKED)
//									CDEBUG1LN(DEBUG_SAFEHOUSE, "Player locking car doors for menu")
//								ENDIF
//							ENDIF	
//						ENDIF
//						bLockCarOnMenu = TRUE
//					ENDIF
					IF iCurrentMenuType = MENU_TYPE_BUZZER
						
						IF HAS_PLAYER_FINISHED_BUZZ_ANIM()

							IF DOES_CAM_EXIST(entryCutData.cameras[0])
								DESTROY_CAM(entryCutData.cameras[0])
							ENDIF

							DISPLAY_RADAR(FALSE)
							bDisabledRadar = TRUE
							NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CAN_BE_TARGETTED | NSPC_ALLOW_PLAYER_DAMAGE | NSPC_PREVENT_EVERYBODY_BACKOFF)// | NSPC_PREVENT_VISIBILITY_CHANGES)
							SET_BIT(iLocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL)
							globalPropertyEntryData.ownerID = INVALID_PLAYER_INDEX()
							globalPropertyEntryData.bLeftOrRejectedBuzzer = FALSE
							GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0
							CDEBUG1LN(DEBUG_SAFEHOUSE, "STAGE_SETUP_FOR_MENU: GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0")
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: resetting globalPropertyEntryData.ownerID= INVALID_PLAYER_INDEX()") 
							IF NOT DOES_CAM_EXIST(propertyCam0)
								propertyCam0 = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", mpYachts[iCurrentYacht].yachtPropertyDetails.camData.vPos,mpYachts[iCurrentYacht].yachtPropertyDetails.camData.vRot,mpYachts[iCurrentYacht].yachtPropertyDetails.camData.fFOV)
								SET_CAM_ACTIVE(propertyCam0,TRUE)
								RENDER_SCRIPT_CAMS(TRUE, FALSE)
							ENDIF
							globalPropertyEntryData.iCostOfPropertyJustBought = 0
							CLEAR_BIT(iLocalBS2, LOCAL_BS2_bRunPropertySelectMenu)
							
							SET_LOCAL_STAGE(STAGE_USING_MENU)
						ENDIF
					ELSE
						IF mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[iEntrancePlayerIsUsing].iType != ENTRANCE_TYPE_GARAGE
							GET_ENTRY_CUTSCENE_DATA(ENTRY_CUTSCENE_TYPE_FRONT_DOOR,TRUE)
							IF NOT RUN_ENTRY_CUTSCENE(ENTRY_CUTSCENE_TYPE_FRONT_DOOR,TRUE)
								BREAK
							ENDIF
						ELSE

							IF mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[iEntrancePlayerIsUsing].iType != ENTRANCE_TYPE_GARAGE
								SET_BIT(iLocalBS2,LOCAL_BS2_bWalkInTriggered)
							ENDIF
							DISPLAY_RADAR(FALSE)
							bDisabledRadar = TRUE
							NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CAN_BE_TARGETTED | NSPC_ALLOW_PLAYER_DAMAGE | NSPC_PREVENT_EVERYBODY_BACKOFF)
							SET_BIT(ilocalBS,LOCAL_BS_TAKEN_AWAY_CONTROL)
							
							IF NOT DOES_CAM_EXIST(propertyCam0)
								propertyCam0 = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", mpYachts[iCurrentYacht].yachtPropertyDetails.camData.vPos,mpYachts[iCurrentYacht].yachtPropertyDetails.camData.vRot,mpYachts[iCurrentYacht].yachtPropertyDetails.camData.fFOV)
								SET_CAM_ACTIVE(propertyCam0,TRUE)
								RENDER_SCRIPT_CAMS(TRUE, FALSE)
							ENDIF
							globalPropertyEntryData.iCostOfPropertyJustBought = 0
							SET_LOCAL_STAGE(STAGE_USING_MENU)
						ENDIF
					ENDIF
				ELSE
					IF g_bAbortPropertyMenus
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: aborting menu. 1 g_bAbortPropertyMenus = TRUE")
					ENDIF
					RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
				ENDIF
			BREAK
			
			CASE STAGE_USING_MENU
				CLEAR_BIT(iLocalBS, LOCAL_BS_BUZZER_GIVE_GOTO_TASK)	
				CLEAR_BIT(iLocalBS, LOCAL_BS_BUZZER_GIVE_RING_TASK)	
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_USING_EXT)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "PROPERTY_BROADCAST_BS_PLAYER_USING_EXT- SET- 1")
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
				AND NOT IS_STORE_PENDING_NETWORK_SHUTDOWN_TO_OPEN()
				AND NOT g_bMissionEnding
				AND NOT g_bAbortPropertyMenus
				AND NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
				AND CAN_PLAYER_USE_PROPERTY(FALSE)//SHOULD_PLAYER_BYPASS_GENERAL_ACCESS_CHECKS_FOR_HEIST())	
				AND NOT IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_AcceptedInvite)
				AND NOT PROPERTY_HAS_JUST_ACCEPTED_A_MISSION(#IF IS_DEBUG_BUILD db_bFullDebug #ENDIF)
				AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_USING_PROPERTY)
					IF NOT MP_FORCE_TERMINATE_INTERNET_ACTIVE()
						MP_FORCE_TERMINATE_INTERNET()
						SET_BIT(iLocalBS, LOCAL_BS_FORCE_TERMINATED_INTERNET)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: Force terminating internet for entry into property.-2")
					ENDIF
//					IF NOT bLockCarOnMenu
//						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//							IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
//								carDoorLocks = GET_VEHICLE_DOOR_LOCK_STATUS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
//								IF NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
//									SET_VEHICLE_DOORS_LOCKED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),VEHICLELOCK_LOCKED)
//									CDEBUG1LN(DEBUG_SAFEHOUSE, "Player locking car doors for menu")
//								ENDIF
//							ENDIF	
//						ENDIF
//						bLockCarOnMenu = TRUE
//					ENDIF
					HANDLE_PROPERTY_OPTIONS_MENU()
					HANDLE_RESPONSE_TO_ENTRY_REQUEST()
				ELSE
					IF g_bAbortPropertyMenus
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: aborting menu. 2 g_bAbortPropertyMenus = TRUE")
					ENDIF
					IF IS_SCREEN_FADING_OUT()
					AND IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_AcceptedInvite)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: Not aborting menu screen is fading.")
					ELSE
						IF iCurrentMenuType = MENU_TYPE_BUZZER
					
							IF NOT globalPropertyEntryData.bLeftOrRejectedBuzzer
							AND NOT IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_AcceptedInvite)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE. STAGE_USING_MENU")
								globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE
							ELSE
								RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
							ENDIF
						ELSE
							RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE STAGE_SETUP_FOR_USING_PROPERTY
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_USING_EXT)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "PROPERTY_BROADCAST_BS_PLAYER_USING_EXT- SET- 2")
				IF NOT MP_FORCE_TERMINATE_INTERNET_ACTIVE()
					MP_FORCE_TERMINATE_INTERNET()
					SET_BIT(iLocalBS, LOCAL_BS_FORCE_TERMINATED_INTERNET)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: Force terminating internet for entry into property.-3")
				ENDIF
//				IF NOT bLockCarOnMenu
//					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//						IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
//							carDoorLocks = GET_VEHICLE_DOOR_LOCK_STATUS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
//							IF NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
//								SET_VEHICLE_DOORS_LOCKED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),VEHICLELOCK_LOCKED)
//								CDEBUG1LN(DEBUG_SAFEHOUSE, "Player locking car doors for menu")
//							ENDIF
//						ENDIF	
//					ENDIF
//					bLockCarOnMenu = TRUE
//				ENDIF
				bDoNotBuzzOthers = FALSE
				IF iBuzzerContextIntention != NEW_CONTEXT_INTENTION
					RELEASE_CONTEXT_INTENTION(iBuzzerContextIntention)
				ENDIF
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
				IF NOT bDisabledStore
					SET_STORE_ENABLED(FALSE)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "disabling store player has entered- EXT")
					bDisabledStore = TRUE
				ENDIF
				
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
				AND (CAN_PLAYER_USE_PROPERTY(FALSE) OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_WARPING_WITH_OWNER_IN_CAR))
				AND NOT PROPERTY_HAS_JUST_ACCEPTED_A_MISSION(#IF IS_DEBUG_BUILD db_bFullDebug #ENDIF)
				
					IF IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
						iPackageDropPropID = iCurrentYacht
						//SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(FALSE)
						SET_MAX_NUM_PORTABLE_PICKUPS_CARRIED_BY_PLAYER(DUMMY_MODEL_FOR_SCRIPT,0) 
						CDEBUG1LN(DEBUG_SAFEHOUSE, "[RCC MISSION] - ENTERING PROPERTY SO DROPPING PICKUP: ",iCurrentYacht)
					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "[RCC MISSION] - NOT DROP PICKUP! no mission critical ")
					ENDIF
					IF NOT bRemoveStickyBombs
						REMOVE_ALL_STICKY_BOMBS_FROM_ENTITY(PLAYER_PED_ID())
						CDEBUG1LN(DEBUG_SAFEHOUSE, "CDM: removed sticky bombs from ped")
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
								IF NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
									REMOVE_ALL_STICKY_BOMBS_FROM_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
									CDEBUG1LN(DEBUG_SAFEHOUSE, "CDM: removed sticky bombs from vehicle ped is in")
								ENDIF
							ENDIF
						ENDIF
						bRemoveStickyBombs = TRUE
					ENDIF
					
					RETAIN_BUZZER_MARKER()
					SET_BIT(ilocalBS,LOCAL_BS_NO_FADE)
					IF NOT bSetEntryData
						globalPropertyEntryData.iPropertyEntered = PROPERTY_YACHT_APT_1_BASE
						PRINTLN("2 globalPropertyEntryData.iPropertyEntered = ",globalPropertyEntryData.iPropertyEntered)
						globalPropertyEntryData.iYachtEntered = iCurrentYacht
//						#IF FEATURE_HEIST_PLANNING
//						IF (SHOULD_BYPASS_CHECKS_FOR_ENTRY(FALSE)
//						AND mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_HOUSE)
//							IF NETWORK_IS_GAMER_IN_MY_SESSION(heistLeader)
//								globalPropertyEntryData.ownerID = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(heistLeader)
//								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: heist data owner in game")
//							ELSE
//								globalPropertyEntryData.ownerID = player_id()
//								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: heist data owner NOT in game")
//							ENDIF
//							globalPropertyEntryData.ownerHandle = heistLeader
//							IF IS_NET_PLAYER_OK(globalPropertyEntryData.ownerID ,FALSE,FALSE)
//								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: setting entry data based on heist data: iProperty " ,g_iHeistPropertyID, " owned by: ", GET_PLAYER_NAME(globalPropertyEntryData.ownerID ))
//								#IF IS_DEBUG_BUILD
//								DEBUG_PRINT_GAMER_HANDLE(globalPropertyEntryData.ownerHandle)
//								#ENDIF
//							ENDIF
//							globalPropertyEntryData.iPropertyEntered = mpYachts[g_iHeistPropertyID].iIndex
//							SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_HEIST_DROPOFF)
//							g_HeistApartmentDropoffPanStarted = TRUE
//							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: g_iApartmentDropoffWarpRadius = ",g_iApartmentDropoffWarpRadius)
//							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: Skipping anims inside as doing heist drop off")
//							IF g_iApartmentDropoffWarpRadius > 0
//								IF g_iApartmentDropoffWarpRadius = iALWAYS_WARP_TO_APARTMENT_DISTANCE 
//									BROADCAST_PROPERTY_NEARBY_PLAYERS_INVITE_EVENT(ALL_PLAYERS(FALSE,FALSE),globalPropertyEntryData.iPropertyEntered,TRUE,FALSE,NATIVE_TO_INT(globalPropertyEntryData.ownerID))
//								ELSE
//									BROADCAST_PROPERTY_NEARBY_PLAYERS_INVITE_EVENT(PLAYERS_IN_RANGE_OF_POINT(GET_PLAYER_COORDS(PLAYER_ID()),g_iApartmentDropoffWarpRadius,TRUE),globalPropertyEntryData.iPropertyEntered,TRUE,FALSE,NATIVE_TO_INT(globalPropertyEntryData.ownerID))
//								ENDIF
//							ENDIF
//						ELSE
//						#ENDIF
						globalPropertyEntryData.ownerID = player_id()
						globalPropertyEntryData.ownerHandle = GET_GAMER_HANDLE_PLAYER(player_id())
//						#IF FEATURE_HEIST_PLANNING
//						ENDIF
//						#ENDIF
						SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ENTER_HOUSE)
						
						globalPropertyEntryData.iEntrance = iEntrancePlayerIsUsing
						
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: ENTERED HOUSE Setting globalPropertyEntryData.iPropertyEntered to be: ", globalPropertyEntryData.iPropertyEntered )
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: ENTERED HOUSE Setting iEntrancePlayerIsUsing to be: ", iEntrancePlayerIsUsing )
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: local player owns this property setting flags accordingly")
						SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_ENTERING_A_PROPERTY)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: setting PROPERTY_BROADCAST_BS_PLAYER_ENTERING_A_PROPERTY- D")
						SET_PLAYER_BROADCAST_PLAYING_PROPERTY_TRANSITION_CUTSCENE(PROPERTY_YACHT_APT_1_BASE)
						//SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ENTER_VIA_EXTERIOR)
						IF iEntrancePlayerIsUsing > mpYachts[iCurrentYacht].yachtPropertyDetails.iNumEntrances
							globalPropertyEntryData.iEntrance = 0
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: iEntrancePlayerIsUsing > mpYachts[iCurrentYacht].yachtPropertyDetails.iNumEntrances forcing entrance to be: ", globalPropertyEntryData.iEntrance)
						ENDIF
						SET_FOCUS_POS_AND_VEL(GET_PLAYER_COORDS(PLAYER_ID()),<<0,0,0>>)
						bSetEntryData = TRUE

						
						
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						SET_BIT(iLocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL)
						SET_LOCAL_STAGE(STAGE_ENTER_PROPERTY)
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: cleaning up cutscenes for invalid player")
					IF g_bPropInteriorScriptRunning
					OR IS_BIT_SET(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_LAUNCHED_INT_SCRIPT)
						globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE. STAGE_SETUP_FOR_USING_PROPERTY")
					ENDIF
					RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
				ENDIF
			BREAK
			
			
			CASE STAGE_LEAVE_ENTRANCE_AREA
				BOOL bShapeTestComplete
				
				bShapeTestComplete = FALSE
				
				IF GET_GAME_TIMER() > iBuzzerPressSyncedSceneTimer
					SHAPETEST_INDEX shapeTest
					SHAPETEST_STATUS shapeTestStatus
					INT bShapeTestHitSomething
					VECTOR vShapeTestPos
					VECTOR vShapeTestNormal
					ENTITY_INDEX shapeTestEntityIndex
					
					bShapeTestHitSomething = 0
					vShapeTestPos = <<0.0, 0.0, 0.0>>
					vShapeTestNormal = <<0.0, 0.0, 0.0>>
					shapeTestEntityIndex = NULL
					
					VECTOR vOffset
					
					//0 = 180, 1 = 135, 2 = 225, 3 = 90, 4 = 270
					
					// 	   \|/
					//  3 - O - 4
					//	   /|\
					//    1 0 2
					
					IF iBuzzerWalkAwayShapeTest = 0
						vOffset = <<0.0, -4.0, 0.0>>
					ELIF iBuzzerWalkAwayShapeTest = 1
						vOffset = <<-2.83, -2.83, 0.0>>
					ELIF iBuzzerWalkAwayShapeTest = 2
						vOffset = <<2.83, -2.83, 0.0>>
					ELIF iBuzzerWalkAwayShapeTest = 3
						vOffset = <<-4.0, 0.0, 0.0>>
					ELIF iBuzzerWalkAwayShapeTest = 4
						vOffset = <<4.0, 0.0, 0.0>>
					ELSE
						vOffset = <<0.0, 0.0, 0.0>>
					ENDIF
					
					FLOAT fShapeTestHeightOffset
					FLOAT fShapeTestRadius
					VECTOR vNearOffset
					VECTOR vFarOffset
					FLOAT fNearVerticalOffset
					FLOAT fFarVerticalOffset
					
					fShapeTestHeightOffset = -0.25
					fShapeTestRadius = 0.5
					
					#IF IS_DEBUG_BUILD
					IF fShapeTestHeightOffset <> fDebugTestShapeTestHeightOffset
						fShapeTestHeightOffset = fDebugTestShapeTestHeightOffset
					ENDIF
					IF fShapeTestRadius <> fDebugTestShapeTestRadius
						fShapeTestRadius = fDebugTestShapeTestRadius
					ENDIF
					#ENDIF
					
					vNearOffset = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), vOffset / 4.0) + <<0.0, 0.0, fShapeTestHeightOffset>>
					vFarOffset = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), vOffset) + <<0.0, 0.0, fShapeTestHeightOffset>>
					
					fNearVerticalOffset = 0.0
					fFarVerticalOffset = 0.0
					
					GET_GROUND_Z_FOR_3D_COORD(vNearOffset + <<0.0, 0.0, 1.0>>, fNearVerticalOffset)
					IF vNearOffset.Z < fNearVerticalOffset
						fNearVerticalOffset = 0.0
					ENDIF
					GET_GROUND_Z_FOR_3D_COORD(vFarOffset + <<0.0, 0.0, 1.0>>, fFarVerticalOffset)
					IF vFarOffset.Z < fFarVerticalOffset
						fFarVerticalOffset = 0.0
					ENDIF
					
					IF NOT bShapeTestRunning
						IF fNearVerticalOffset <> 0.0 AND fFarVerticalOffset <> 0.0 AND ABSF(fFarVerticalOffset - fNearVerticalOffset) < 2.0
							shapeTest = START_SHAPE_TEST_CAPSULE(<<vNearOffset.X, vNearOffset.Y, fNearVerticalOffset + 1.0>>, <<vFarOffset.X, vFarOffset.Y, fFarVerticalOffset + 1.0>>, fShapeTestRadius, SCRIPT_INCLUDE_ALL, PLAYER_PED_ID())
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: SHAPETEST[STARTING]")
							bShapeTestRunning = TRUE
						ELSE
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: SHAPETEST[GroundHeightFail]")
							iBuzzerWalkAwayShapeTest++
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: SHAPETEST[iBuzzerWalkAwayShapeTest=", iBuzzerWalkAwayShapeTest, "]")
						ENDIF
					ENDIF
					
					IF bShapeTestRunning
						shapeTestStatus = GET_SHAPE_TEST_RESULT(shapeTest, bShapeTestHitSomething, vShapeTestPos, vShapeTestNormal, shapeTestEntityIndex)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: SHAPETEST[RUNNING]")
						IF shapeTestStatus != SHAPETEST_STATUS_RESULTS_NOTREADY
							IF shapeTestStatus = SHAPETEST_STATUS_RESULTS_READY
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: SHAPETEST[SHAPETEST_STATUS_RESULTS_READY]")
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: SHAPETEST[HitSomething=", bShapeTestHitSomething, "]")
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: SHAPETEST[shapeTestEntityIndex=", DOES_ENTITY_EXIST(shapeTestEntityIndex), "]")
								IF bShapeTestHitSomething = 1
								OR DOES_ENTITY_EXIST(shapeTestEntityIndex)
									CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: SHAPETEST[TRUE]")
									iBuzzerWalkAwayShapeTest++
									CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: SHAPETEST[iBuzzerWalkAwayShapeTest=", iBuzzerWalkAwayShapeTest, "]")
									bShapeTestRunning = FALSE
									CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: SHAPETEST[bShapeTestRunning=FALSE]")
								ELSE
									bShapeTestRunning = FALSE
									CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: SHAPETEST[bShapeTestRunning=FALSE]")
									bShapeTestComplete = TRUE
									CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: SHAPETEST[bShapeTestComplete=TRUE]")
								ENDIF
							#IF IS_DEBUG_BUILD
							ELSE
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: SHAPETEST[SHAPETEST_STATUS_NONEXISTENT]")
							#ENDIF
							ENDIF
						#IF IS_DEBUG_BUILD
						ELSE
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: SHAPETEST[SHAPETEST_STATUS_RESULTS_NOTREADY]")
						#ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: SHAPETEST[LAST CHECK iBuzzerWalkAwayShapeTest=", iBuzzerWalkAwayShapeTest, "]")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: SHAPETEST[LAST CHECK bShapeTestComplete=", bShapeTestComplete, "]")
				
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
				AND GET_GAME_TIMER() > iBuzzerPressSyncedSceneTimer
				AND (bShapeTestComplete OR iBuzzerWalkAwayShapeTest >= 5)	//0 = 180, 1 = 135, 2 = 225, 3 = 90, 4 = 270

					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: iEntrance = ", iEntrancePlayerIsUsing)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: Leave entrance area property in heading = ", mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[iEntrancePlayerIsUsing].fInPlayerHeading)
					RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
					IF iBuzzerWalkAwayShapeTest = 0
						SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2000, 180, TRUE)
					ELIF iBuzzerWalkAwayShapeTest = 1
						SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2000, 135, TRUE)
					ELIF iBuzzerWalkAwayShapeTest = 2
						SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2000, 225, TRUE)
					ELIF iBuzzerWalkAwayShapeTest = 3
						SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2000, 90, TRUE)
					ELIF iBuzzerWalkAwayShapeTest = 4
						SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2000, 270, TRUE)
					ENDIF

					
					REINIT_NET_TIMER(tempPropertyExtMenu.exitDoorDelay,TRUE)

				ENDIF
			BREAK
			
			CASE STAGE_ENTER_PROPERTY
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_USING_EXT)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "PROPERTY_BROADCAST_BS_PLAYER_USING_EXT- SET- 3")
				IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
				OR IS_PLAYER_SWITCH_IN_PROGRESS()
					RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
				ENDIF
				
				WALK_INTO_PROPERTY_CUTSCENE()//FALSE)
				IF globalPropertyEntryData.bForceCleanupExterior

					IF globalPropertyEntryData.bWalkInSeqDone = TRUE	//iWalkInStage >= WIS_WAIT_FOR_LOAD
						globalPropertyEntryData.bForceCleanupExterior = FALSE
						CLEAR_FOCUS()
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: CLEAR_FOCUS() - 3")
						bSetEntryData = FALSE
						CLEANUP_PROPERTY_CAMERAS()
					ENDIF
				ELSE
					IF globalPropertyEntryData.bInteriorWarpDone
						
						CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     WALK_INTO_PROPERTY - Call WALK_IN_CLEANUP - C")
						WALK_IN_CLEANUP(FALSE)
						IF NOT IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_InitDone)
							globalPropertyEntryData.bInteriorWarpDone = FALSE
							CDEBUG1LN(DEBUG_SAFEHOUSE, "1 globalPropertyEntryData.bInteriorWarpDone = FALSE") 
						ENDIF
						globalPropertyEntryData.bBuzzAccepted = FALSE
						globalPropertyEntryData.bForceCleanupExterior = FALSE
						DISPLAY_RADAR(TRUE)
						bDisabledRadar = FALSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "Player has starting internal synced scene killing exterior script")
						CLEANUP_SCRIPT()
					ENDIF
				ENDIF
			BREAK
			
			CASE STAGE_BUZZED_INTO_PROPERTY
				IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
				OR IS_PLAYER_SWITCH_IN_PROGRESS()
					RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
				ENDIF
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				globalPropertyEntryData.bWalkInSeqDone = TRUE
				
				IF globalPropertyEntryData.bInteriorWarpDone
					IF NOT IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_InitDone)
						globalPropertyEntryData.bInteriorWarpDone = FALSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "2 globalPropertyEntryData.bInteriorWarpDone = FALSE") 
					ENDIF
					globalPropertyEntryData.bBuzzAccepted = FALSE
					DISPLAY_RADAR(TRUE)
					bDisabledRadar = FALSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "Player has starting internal synced scene killing exterior script")
					CLEANUP_SCRIPT()
				ENDIF
			BREAK
		ENDSWITCH

		MAINTAIN_YACHT_EFFECTS() // we use various conditions that get calculated in the switch above so this had to be moved beneath the switch
		
		IF bDelayWarpAfterConfirm
			RUN_DELAYED_WARP()
		ENDIF
				
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			
			SERVER_MAINTAIN_PROPERTY_ACTIVITIES(playerBD[serverLocalActControl.iSlowPlayerLoop].iActivityRequested,serverBD.activityControl,serverLocalActControl)
			
			SWITCH serverBD.iStage
				CASE SERVER_STAGE_INIT
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: Server created exterior objects")
					
					
					
					IF INIT_SERVER_JACUZZI_ACTIVITY()
						serverBD.iStage = SERVER_STAGE_RUNNING
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_YACHT: serverBD.iStage going to SERVER_STAGE_RUNNING")
					ENDIF
				BREAK
				
				CASE SERVER_STAGE_RUNNING
					SERVER_MAINTAIN_FIRE_CHECK()
					IF iCurrentYacht > -1
						IF GET_FRAME_COUNT() % 60 = 0
							CDEBUG1LN(DEBUG_SAFEHOUSE, "SERVER_STAGE_RUNNING: iCurrentYachtID: ", iCurrentYacht)
						ENDIF
						serverBD.MPRadioServer.bOnYachtDeck = TRUE
						serverBD.MPRadioServer.iYachtID = iCurrentYacht
						OVERRIDE_PROPERTY_STRUCT_INDEX_FOR_RADIO(TRUE)
						SERVER_MAINTAIN_MP_RADIO(serverBD.MPRadioServer, playerBD[MPRadioLocal.iServerPlayerStagger].MPRadioClient, MPRadioLocal , InteriorPropStruct )
						OVERRIDE_PROPERTY_STRUCT_INDEX_FOR_RADIO(FALSE)
					ELSE
						IF GET_FRAME_COUNT() % 60 = 0
							CDEBUG1LN(DEBUG_SAFEHOUSE, "SERVER_STAGE_RUNNING: iCurrentYachtID = -1")
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDWHILE
ENDSCRIPT

//EOF
