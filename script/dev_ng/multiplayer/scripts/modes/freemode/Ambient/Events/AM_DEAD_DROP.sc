//////////////////////////////////////////////////////////////////////////////////////////
// Name:        	AM_DEAD_DROP.sc														//
// Description: 	Controls a package that needs to be dropped off by a player.		//
// Written by:  	Steve Tiley															//
// Date: 			18/05/2015															//
//////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "net_mission.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_event.sch"

USING "DM_Leaderboard.sch"
USING "net_challenges.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_doors.sch"

//
USING "net_scoring_common.sch"
USING "net_ambience.sch"

USING "net_missions_at_coords_public.sch"

//USING "help_at_location.sch"
USING "commands_object.sch"
USING "net_xp_animation.sch"

USING "net_wait_zero.sch"

USING "am_common_ui.sch"

USING "freemode_events_header.sch"

USING "net_gang_boss.sch"

#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF

// Game States. 
CONST_INT GAME_STATE_INI 				0
CONST_INT GAME_STATE_RUNNING			1
CONST_INT GAME_STATE_LEAVE				2
CONST_INT GAME_STATE_TERMINATE_DELAY	3
CONST_INT GAME_STATE_END				4

ENUM DEAD_DROP_STAGE_ENUM
	eAD_IDLE,
	eAD_CONTEND,
	eAD_DELIVER,
	eAD_KILL,
	eAD_OFF_MISSION,
	eAD_END,
	eAD_CLEANUP
ENDENUM

ENUM DEAD_DROP_ROUTE_NAME_ENUM
	eDDR_HANGAR,
	eDDR_CARGO_SHIP,
	eDDR_AIRFIELD,
	eDDR_WESTONS_HOUSE,
	eDDR_RAIL_YARD
ENDENUM

//Server Bitset Data
CONST_INT biS_PackageCollected 			0
CONST_INT biS_AwaitingFirstCollection	1
CONST_INT biS_MissionEndTimerHit 		2
CONST_INT biS_PackageDropped			3
CONST_INT biS_StartTimer				4
CONST_INT biS_PackageDelivered			5
CONST_INT biS_MissionTimedOut			6
CONST_INT biS_MissionShouldCleanup		7
CONST_INT biS_EventEnded				8
//CONST_INT biS_PlayerDiedInShop			7

// The server broadcast data.
// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData

	// Game State
	INT iServerGameState
		
	// Server Bitset
	INT iServerBitSet
	
	// Indices
	NETWORK_INDEX niPackage
	PLAYER_INDEX piCarrier
	PLAYER_INDEX piDeliverer
	PLAYER_INDEX piCarrierKiller
	
	// Models
	MODEL_NAMES PackageModel = prop_ld_case_01
		
	// Variables
	VECTOR vStartLocation
	VECTOR vDroppedPackageLocation
	VECTOR vDeliveryLocation
	VECTOR vDeliveryDimensions
	FLOAT fStartHeading
	FLOAT fDeliveryHeading
	
	INT iRoute
	INT iVariation
	
	// Enums
	DEAD_DROP_STAGE_ENUM eStage = eAD_IDLE
	DEAD_DROP_ROUTE_NAME_ENUM eRouteName
	
	// Timers
	SCRIPT_TIMER DeliverPackageTimer
	SCRIPT_TIMER MissionTerminateDelayTimer
	SCRIPT_TIMER MissionStartTimeout
	SCRIPT_TIMER MissionEndStageTimer
	
	INT iHashedMac
	INT iMatchHistoryId
	
	INT iPlayersLeftInProgress 
	INT iPeakParticipants
	INT iPeakQualifiers
//	SHOP_NAME_ENUM sne_ShopPackageIsIn

	BOOL bOpenAirportGates
	
ENDSTRUCT

ServerBroadcastData serverBD
CHALLENGE_DPAD_STRUCT chDPStruct
scrFmEventAmbientMission telemetryStruct
STRUCT_FM_EVENTS_END_UI sEndUiVars

CONST_INT biP_DeliveredPackage			0
CONST_INT biP_CollectedPackage  		1
CONST_INT biP_ShowingContendSticky		2
CONST_INT biP_ShowingDeliverSticky 		3
CONST_INT biP_ShowingKillSticky 		4
CONST_INT biP_ParticipantViaInteraction 5
CONST_INT biP_ParticipantViaDistance 	6
CONST_INT biP_HaveHadPackage			7
CONST_INT biP_IHelpedDeliver			8
CONST_INT biP_IKilledCarrier			9
CONST_INT biP_WithinQualifyDistance		10
//CONST_INT biP_PlayerDiedInShop			8

CONST_FLOAT DEAD_DROP_FULLY_ACTIVE_PARTICIPANT_DISTANCE 500.0
CONST_FLOAT DEAD_DROP_REMOVE_PARTICIPANT_DISTANCE 700.0

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	// Player Gamestate
	INT iGameState
	
	// Player bitset
	INT iPlayerBitSet
	
	INT iParticipatedBitSet
	
	INT iTotalCash = -1
	
	// Enums
	DEAD_DROP_STAGE_ENUM eStage = eAD_IDLE
	
//	SHOP_NAME_ENUM sne_Shop
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

///// MISSION VARIABLES /////
INT iBoolsBitSet
INT iBoolsBitSet2
CONST_INT biHelp0Done			0
CONST_INT biHelp1Done			1
CONST_INT biHelp2Done			2
CONST_INT biHelp3Done			3
CONST_INT biHelp4Done			4
CONST_INT biHelp5Done			5
CONST_INT biHelp6Done			6
CONST_INT biHelp7Done			7
CONST_INT biHelp8Done			8
CONST_INT biHelp9Done			9
CONST_INT biHelp10Done			10
CONST_INT iLOCALBITSET0_MEMBER_OF_A_GANG 11
CONST_INT biBigM0Done			0
CONST_INT biBigM1Done			1
CONST_INT biBigM2Done			2
CONST_INT biBigM3Done			3
CONST_INT biBigM4Done			4
CONST_INT biBigM5Done			5
CONST_INT biBigM0Tracker		10

CONST_INT TIME_DELIVERY_TIMEOUT 600000
CONST_INT TIME_START_TIMEOUT	600000
CONST_INT TIME_DEBUG			30000

INT iStaggeredParticipant
INT iNoOfCarrierKills = 0
INT iTimeHeld = 0
INT iLocalTimeHolding = 0
INT iStaggeredCheck
INT iParticipationTime = 0
INT iTimeRemaining
INT iBagChangedHands

INT iPackageOnYachtStep
INT iYachtStagger
VECTOR vPackageAvoidCoord
VECTOR vWarpPackageCoord
FLOAT fWarpPackageHeading

BOOL bOnOff
BOOL bSetSpawnLocations
BOOL bDisplayedEndMessage = FALSE
BOOL bProcessedReward = FALSE
BOOL bBlipsBlocked = FALSE
BOOL bUpdateObjectiveGangMembershipChange = FALSE

CONST_INT PACKAGE_CLOSE_RANGE 	 20

VECTOR vStripClubCoords = <<104.44887, -1320.72302, 28.26478>>

BLIP_INDEX PlayerBlip
BLIP_INDEX PackageBlip
BLIP_INDEX DeliveryBlip
//CHECKPOINT_INDEX DeliveryCP

SCRIPT_TIMER TimeHeldTimer
SCRIPT_TIMER ParticipationTimer

SCRIPT_TIMER iGarageCheckTimer
SCRIPT_TIMER iGarageSyncDelayTimer

PLAYER_INDEX carrier = INVALID_PLAYER_INDEX()

// Widgets & Debug.
#IF IS_DEBUG_BUILD
	BOOL bHostEndMissionNow
	BOOL bWarpToVeh
	BOOL bDebugTimeChange = FALSE
#ENDIF

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MAIN PROCS                            ////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE:
///    Block or Unblock the blips for missions and jobs.
/// PARAMS:
///    block - if true, block, if false, unblock
PROC BLOCK_BLIPS(BOOL block)
	IF block
		PRINTLN("[AM DEAD DROP] -  BLOCK_BLIPS - TRUE - Block_All_MissionsAtCoords_Missions(true)")
		Block_All_MissionsAtCoords_Missions(TRUE)
		bBlipsBlocked = TRUE
	ELSE
		PRINTLN("[AM DEAD DROP] -  BLOCK_BLIPS - FALSE - Block_All_MissionsAtCoords_Missions(false)")
		Block_All_MissionsAtCoords_Missions(FALSE)
		bBlipsBlocked = FALSE
	ENDIF
ENDPROC

/// PURPOSE:
///    Remove the package, delivery and player blips if they exist
PROC REMOVE_BLIPS()

	IF DOES_BLIP_EXIST(PackageBlip)
		REMOVE_BLIP(PackageBlip)
	ENDIF
	
	IF DOES_BLIP_EXIST(DeliveryBlip)
		REMOVE_BLIP(DeliveryBlip)
	ENDIF
	
	IF DOES_BLIP_EXIST(PlayerBlip)
		REMOVE_BLIP(PlayerBlip)
	ENDIF

ENDPROC

//Helper function to get the servers game/mission state
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC

//Cleanup Mission Data
PROC SCRIPT_CLEANUP()
	
	BLOCK_BLIPS(FALSE)
	
	REMOVE_BLIPS()
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND PARTICIPANT_ID_TO_INT() != -1
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_CollectedPackage)
			CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_CollectedPackage)
		ENDIF
	ENDIF
	
	// Remove THEFEED message
//	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ShowingContendSticky)
//	OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ShowingDeliverSticky)
//	OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ShowingKillSticky)
//		THEFEED_CLEAR_FROZEN_POST()
//	ENDIF
	
//	IF PLAYER_ID() = serverBD.piDriver
//		IF serverBD.piGunner != INVALID_PLAYER_INDEX()
//			BROADCAST_LEAVE_VEHICLE(SPECIFIC_PLAYER(serverBD.piGunner), FALSE, 0, 0, FALSE, FALSE)
//		ENDIF
//	ENDIF
		
//	IF GET_SERVER_MISSION_STATE() = GAME_STATE_END
//	AND serverBD.PackageModel != DUMMY_MODEL_FOR_SCRIPT
//		SET_VEHICLE_MODEL_IS_SUPPRESSED(serverBD.PackageModel, FALSE)
//		PRINTLN("[AM DEAD DROP] -  SET_VEHICLE_MODEL_IS_SUPPRESSED - FALSE      <----------     ")
//	ENDIF
	
	SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY(FALSE)
	PRINTLN("[AM DEAD DROP] - CLEANUP - SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY = FALSE")
	
//	FM_EVENT_SET_PLAYER_IS_CRITICAL_TO_FM_EVENT(FALSE)
//	PRINTLN("[AM DEAD DROP] - CLEANUP - FM_EVENT_SET_PLAYER_IS_CRITICAL_TO_FM_EVENT(FALSE)")
	
	IF FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_FM_EVENT(PLAYER_ID())
		FM_EVENT_SET_PLAYER_IS_PERMANENT_PARTICIPANT_IN_FM_EVENT(FALSE)
		PRINTLN("[AM DEAD DROP] - CLEANUP - FM_EVENT_SET_PLAYER_IS_PERMANENT_PARTICIPANT_IN_FM_EVENT(FALSE)")
	ENDIF
	
	CLEAR_BIT(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_PLAYER_USING_DESTROY_VEH)
	IF bSetSpawnLocations
		//Clear the spawn locations
		PRINTLN("[AM DEAD DROP] - CLEANUP - CLEAR_PLAYER_NEXT_RESPAWN_LOCATION   <----------     ")
		CLEAR_PLAYER_NEXT_RESPAWN_LOCATION()
		CLEAR_SPECIFIC_SPAWN_LOCATION_FROM_THIS_SCRIPT()
	ENDIF
	SET_FREEMODE_FLOW_COMMS_DISABLED(FALSE)
	
	STOP_AUDIO_SCENE("MP_Deathmatch_Type_Challenge_Scene")
	
	IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		telemetryStruct.m_endReason             =AE_END_FORCED
	ELIF SHOULD_BAIL_FM_EVENT_DUE_TO_LOW_SESSION_NUMBERS()
		telemetryStruct.m_endReason             =AE_END_LOW_NUMBERS
	ELSE
		IF NETWORK_IS_GAME_IN_PROGRESS()
		AND PARTICIPANT_ID_TO_INT() != -1
		AND IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DeliveredPackage)
			telemetryStruct.m_endReason             =AE_END_WON
		ELSE
			telemetryStruct.m_endReason             =AE_END_LOST
		ENDIF
	ENDIF
	
	telemetryStruct.m_uid0 = serverBD.iHashedMac
	telemetryStruct.m_uid1 = serverBD.iMatchHistoryId
	telemetryStruct.m_playersLeftInProgress = serverBD.iPlayersLeftInProgress
	telemetryStruct.m_playersParticipating = serverBD.iPeakQualifiers
	telemetryStruct.m_timeTakenToComplete = GET_CLOUD_TIME_AS_INT() - telemetryStruct.m_startTime
	
	IF NOT g_sMPTunables.bDead_Drop_Disable_Share_Cash
		IF telemetryStruct.m_cashEarned > 0
//			SET_LAST_JOB_DATA(LAST_JOB_DEAD_DROP, telemetryStruct.m_cashEarned)
			IF telemetryStruct.m_endReason             =AE_END_WON
				SET_DO_FM_EVENT_SHARE_CASH_HELP_IF_NEEDED()
			ENDIF
		ENDIF
	ENDIF
	iLocalTimeHolding += iTimeHeld
	
	PROCESS_CURRENT_AMBIENT_EVENT_PLAYSTATS(telemetryStruct, iBagChangedHands, iLocalTimeHolding)
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_EventEnded)
		PRINTLN("[AM DEAD DROP] - SCRIPT_CLEANUP - biS_EventEnded SET, players is cleaning with:  COMMON_FREEMODE_AMBIENT_EVENTS_CLEAN_UP(FMMC_TYPE_DEAD_DROP, FALSE, TRUE)")
		COMMON_FREEMODE_AMBIENT_EVENTS_CLEAN_UP(FMMC_TYPE_DEAD_DROP, FALSE, TRUE)
	ELSE
		PRINTLN("[AM DEAD DROP] - SCRIPT_CLEANUP - biS_EventEnded ** NOT ** SET, players is cleaning with:  COMMON_FREEMODE_AMBIENT_EVENTS_CLEAN_UP(FMMC_TYPE_DEAD_DROP, FALSE, FALSE)")
		COMMON_FREEMODE_AMBIENT_EVENTS_CLEAN_UP(FMMC_TYPE_DEAD_DROP, FALSE, FALSE)
	ENDIF
	
	SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(TRUE)
	
	PRINTLN("[AM DEAD DROP] -  CLEANUP MISSION      <----------     ")
	#IF IS_DEBUG_BUILD NET_LOG("SCRIPT_CLEANUP")	#ENDIF
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

FUNC BOOL IS_SPECTATOR_FOCUS_PED_DEAD()
	IF IS_PLAYER_SCTV(PLAYER_ID())
		IF IS_ENTITY_DEAD(GET_SPECTATOR_CURRENT_FOCUS_PED())
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL AM_I_SPECTATING_THE_CARRIER()
	PED_INDEX carrierPed

	IF IS_PLAYER_SCTV(PLAYER_ID())
		IF serverBD.piCarrier != INVALID_PLAYER_INDEX()
			IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piCarrier)
				carrierPed = GET_PLAYER_PED(serverBD.piCarrier)
			ENDIF
		ENDIF
		IF GET_SPECTATOR_CURRENT_FOCUS_PED() = carrierPed
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL AM_I_SPECTATING_A_NON_CARRIER_PARTICIPANT()
	PED_INDEX carrierPed

	IF IS_PLAYER_SCTV(PLAYER_ID())
		IF serverBD.piCarrier != INVALID_PLAYER_INDEX()
			IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piCarrier)
				carrierPed = GET_PLAYER_PED(serverBD.piCarrier)
			ENDIF
		ENDIF
		IF GET_SPECTATOR_CURRENT_FOCUS_PED() != carrierPed
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PARTICIPANT_FULLY_TAKING_PART(INT iParticipant)	
	
	IF FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_FM_EVENT(PLAYER_ID())
		RETURN TRUE
	ENDIF
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HaveHadPackage)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(playerBD[iParticipant].iPlayerBitSet, biP_ParticipantViaInteraction)
		RETURN TRUE
	ENDIF
		
	IF IS_BIT_SET(playerBD[iParticipant].iPlayerBitSet, biP_ParticipantViaDistance)
		RETURN TRUE
	ENDIF	
	
	IF IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iFmAmbientEventBitSet, ciSET_PLAYER_OPTED_IN)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE	
ENDFUNC

FUNC BOOL HAVE_MET_REWARD_CONDITIONS()
	
	IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_FM_EVENT(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	IF FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT(PLAYER_ID())
		RETURN TRUE
	ENDIF	
	
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HaveHadPackage)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WithinQualifyDistance)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
		
ENDFUNC

PROC MAINTAIN_SPAWN_LOCATIONS()
	IF NOT GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
		IF IS_PARTICIPANT_FULLY_TAKING_PART(PARTICIPANT_ID_TO_INT())
			IF NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(),MPAM_TYPE_TIME_TRIAL)
			AND NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_RC_TIME_TRIAL)
				IF NOT IS_PLAYER_IN_CORONA()
				AND NOT IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), FALSE)
				AND NOT IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
					VECTOR vTargetCoords
					IF serverBD.piCarrier = PLAYER_ID()
					AND NOT IS_PED_INJURED(PLAYER_PED_ID())
						vTargetCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
						//Set up the span locations
						bSetSpawnLocations = TRUE
						SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_NEAR_SPECIFIC_COORDS)
						SETUP_SPECIFIC_SPAWN_LOCATION(vTargetCoords, 0.0, 250.0, TRUE, FMEVENT_MINIMUM_SPAWN_DISTANCE_FROM_ACTION, FALSE, TRUE, 65, TRUE, TRUE)
					ELIF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
						vTargetCoords = GET_ENTITY_COORDS(NET_TO_ENT(serverBD.niPackage))	
						//Set up the span locations
						bSetSpawnLocations = TRUE
						SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_NEAR_SPECIFIC_COORDS)
						SETUP_SPECIFIC_SPAWN_LOCATION(vTargetCoords, 0.0, 250.0, TRUE, FMEVENT_MINIMUM_SPAWN_DISTANCE_FROM_ACTION, FALSE, TRUE, 65, TRUE, TRUE)
					ENDIF
				ELSE
					IF bSetSpawnLocations = TRUE
						PRINTLN("[AM DEAD DROP] - MAINTAIN_SPAWN_LOCATIONS - Player in corona - CLEAR_PLAYER_NEXT_RESPAWN_LOCATION")
						CLEAR_PLAYER_NEXT_RESPAWN_LOCATION()
						CLEAR_SPECIFIC_SPAWN_LOCATION_FROM_THIS_SCRIPT()
						bSetSpawnLocations = FALSE
					ENDIF
				ENDIF
			ELSE
				IF bSetSpawnLocations = TRUE
					PRINTLN("[AM DEAD DROP] - MAINTAIN_SPAWN_LOCATIONS - Player is on time trial - CLEAR_PLAYER_NEXT_RESPAWN_LOCATION")
					CLEAR_PLAYER_NEXT_RESPAWN_LOCATION()
					CLEAR_SPECIFIC_SPAWN_LOCATION_FROM_THIS_SCRIPT()
					bSetSpawnLocations = FALSE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF bSetSpawnLocations = TRUE
			PRINTLN("[AM DEAD DROP] - MAINTAIN_SPAWN_LOCATIONS - Critical to boss work")
			CLEAR_PLAYER_NEXT_RESPAWN_LOCATION()
			CLEAR_SPECIFIC_SPAWN_LOCATION_FROM_THIS_SCRIPT()
			bSetSpawnLocations = FALSE
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_MISSION_TIME()

	#IF IS_DEBUG_BUILD
		IF MPGlobalsAmbience.bEndCurrentFreemodeEvent
			RETURN TIME_DEBUG
		ENDIF	
	#ENDIF
	
	RETURN g_sMPTunables.iDead_Drop_Event_Time_Limit

ENDFUNC

FUNC BOOL SHOULD_UI_BE_HIDDEN()
	
	IF NOT IS_IT_SAFE_TO_DISPLAY_FM_EVENT_UI()
		RETURN TRUE
	ENDIF
	
	IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	IF g_bCelebrationScreenIsActive = TRUE
		RETURN TRUE
	ENDIF
	
	IF IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID()) = TRUE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gets a suitable route to use for DEAD DROP - returns false for the randomly selected route if blocked by tunable
/// PARAMS:
///    iRoute - reference to the variable used to select the route
/// RETURNS:
///    TRUE if the randomly selected route is not blocked by tunable - debug selections will always return true to enable testing.
FUNC INT GET_SUITABLE_DEAD_DROP_ROUTE()

	#IF IS_DEBUG_BUILD
		IF MPGlobalsAmbience.iDeadDropLocation > -1
		AND MPGlobalsAmbience.iDeadDropLocation < 5
			PRINTLN("[AM DEAD DROP] - DEBUG - GET_SUITABLE_DEAD_DROP_ROUTE - MPGlobalsAmbience.iDeadDropLocation = ", MPGlobalsAmbience.iDeadDropLocation)
			RETURN MPGlobalsAmbience.iDeadDropLocation
		ENDIF
	#ENDIF	

	INT i, iValidRoutes[5], iRouteDisabled[5]
	INT iValidCount = 0
	
	iRouteDisabled[0] = BOOL_TO_INT(g_sMPTunables.bDead_Drop_Disable_variant_1)
	PRINTLN("[AM DEAD DROP] - GET_SUITABLE_DEAD_DROP_ROUTE - iRouteDisabled[0] = ", g_sMPTunables.bDead_Drop_Disable_variant_1)
	iRouteDisabled[1] = BOOL_TO_INT(g_sMPTunables.bDead_Drop_Disable_variant_2)
	PRINTLN("[AM DEAD DROP] - GET_SUITABLE_DEAD_DROP_ROUTE - iRouteDisabled[1] = ", g_sMPTunables.bDead_Drop_Disable_variant_2)
	iRouteDisabled[2] = BOOL_TO_INT(g_sMPTunables.bDead_Drop_Disable_variant_3)
	PRINTLN("[AM DEAD DROP] - GET_SUITABLE_DEAD_DROP_ROUTE - iRouteDisabled[2] = ", g_sMPTunables.bDead_Drop_Disable_variant_3)
	iRouteDisabled[3] = BOOL_TO_INT(g_sMPTunables.bDead_Drop_Disable_variant_4)
	PRINTLN("[AM DEAD DROP] - GET_SUITABLE_DEAD_DROP_ROUTE - iRouteDisabled[3] = ", g_sMPTunables.bDead_Drop_Disable_variant_4)
	iRouteDisabled[4] = BOOL_TO_INT(g_sMPTunables.bDead_Drop_Disable_variant_5)
	PRINTLN("[AM DEAD DROP] - GET_SUITABLE_DEAD_DROP_ROUTE - iRouteDisabled[4] = ", g_sMPTunables.bDead_Drop_Disable_variant_5)
		
	REPEAT 5 i	
	
		IF iRouteDisabled[i] = 0
			iValidRoutes[iValidCount] = i
			iValidCount++
			PRINTLN("[AM DEAD DROP] - GET_SUITABLE_DEAD_DROP_ROUTE - Adding iRouteDisabled[", i,"] to valid route array element [", iValidCount,"] - total valid routes: ", iValidCount)
		ENDIF
	
	ENDREPEAT
	
	IF iValidCount > 0	
		i = iValidRoutes[GET_RANDOM_INT_IN_RANGE(0, iValidCount)]
		PRINTLN("[AM DEAD DROP] - GET_SUITABLE_DEAD_DROP_ROUTE - Found suitable route - route selected randomly for valid list is ", i)
	ELSE
		i = 0
		PRINTLN("[AM DEAD DROP] - GET_SUITABLE_DEAD_DROP_ROUTE - Failed to find a suitable route - all routes blocked by debug - returning 0 to launch hangar by default")
	ENDIF
	
	RETURN i

ENDFUNC


//PURPOSE: Get Random Location for Veh to be Parked at
PROC GET_RANDOM_LOCATIONS(VECTOR &vStartLocation, FLOAT &fStartHeading, VECTOR &vDelLocation, FLOAT &fDelHeading, VECTOR &vDelDimensions, DEAD_DROP_ROUTE_NAME_ENUM &eRoute, INT &iRoute, INT &iVariation)

	iRoute = GET_SUITABLE_DEAD_DROP_ROUTE()		
	iVariation = GET_RANDOM_INT_IN_RANGE(0,3)
			
	SWITCH iRoute
		//Hanger
		CASE 0
			eRoute 			= 	eDDR_HANGAR
			vStartLocation 	=	<<-924.6494, -2988.9478, 18.8454>>		
			serverBD.bOpenAirportGates = TRUE
			SWITCH iVariation
				CASE 0
					vDelLocation 	=	<<-1791, 459, 127.3>>
				BREAK
				CASE 1
					vDelLocation 	=	<<-995.4894, 830.6302, 171.4505>>
				BREAK
				CASE 2
					vDelLocation 	=	<<-1863.1039, -352.8481, 48.2404>>
				BREAK			
			ENDSWITCH
			vDelDimensions 	=	<<LOCATE_SIZE_ON_FOOT_ONLY,LOCATE_SIZE_ON_FOOT_ONLY,LOCATE_SIZE_HEIGHT>>	
			fStartHeading 	=	52.5901	
			fDelHeading 	= 	4.1685
		BREAK
		//CArgo Ship
		CASE 1
			eRoute 			= 	eDDR_CARGO_SHIP
			vStartLocation 	=	<<-333.2185, -2634.6274, 5.0003>>			
			SWITCH iVariation
				CASE 0
					vDelLocation 	=	<<1914.4386, 3901.6917, 31.6063>>
				BREAK
				CASE 1
					vDelLocation 	=	<<-466.5143, 1128.7052, 324.8556>>
				BREAK
				CASE 2
					vDelLocation 	=	<<3717.5400, 4535.0923, 20.5715>>
				BREAK			
			ENDSWITCH
			vDelDimensions 	=	<<LOCATE_SIZE_ON_FOOT_ONLY,LOCATE_SIZE_ON_FOOT_ONLY,LOCATE_SIZE_HEIGHT>>			
			fStartHeading 	=	150.3481
			fDelHeading 	= 	350.7538			
		BREAK
		//Airfield
		CASE 2
			eRoute 			= 	eDDR_AIRFIELD
			vStartLocation 	=	<<1967.3677, 4636.5894, 39.7677>>			
			SWITCH iVariation
				CASE 0
					vDelLocation 	=	<<-1911.7172, 2071.6768, 139.3890>>
				BREAK
				CASE 1
					vDelLocation 	=	<<-2168, 4282, 48>>
				BREAK
				CASE 2
					vDelLocation 	=	<<-253.9300, 2189.7214, 129.1242>>
				BREAK			
			ENDSWITCH
			vDelDimensions 	=	<<LOCATE_SIZE_ON_FOOT_ONLY,LOCATE_SIZE_ON_FOOT_ONLY,LOCATE_SIZE_HEIGHT>>	
			fStartHeading	=	81.7556
			fDelHeading 	= 	315.9328			
		BREAK
		//Westons House
		CASE 3
			eRoute 			= 	eDDR_WESTONS_HOUSE
			vStartLocation 	=	<<-2586.1382, 1911.6526, 166.4988>>			
			SWITCH iVariation
				CASE 0
					vDelLocation 	=	<<1371.4475, 1147.4385, 112.7590>>
				BREAK
				CASE 1
					vDelLocation 	=	<<1312.4015, -590.4759, 71.9316>>
				BREAK
				CASE 2
					vDelLocation 	=	<<-1171.7576, -1390.9805, 3.8677>>
				BREAK			
			ENDSWITCH
			vDelDimensions	=	<<LOCATE_SIZE_ON_FOOT_ONLY,LOCATE_SIZE_ON_FOOT_ONLY,LOCATE_SIZE_HEIGHT>>			
			fStartHeading 	=	336.9964
			fDelHeading 	= 	1.0650			
		BREAK
		//Railyard
		CASE 4
			eRoute 			= 	eDDR_RAIL_YARD
			vStartLocation 	=	<<2632.8911, 2933.2898, 43.7440>>			
			SWITCH iVariation
				CASE 0
					vDelLocation 	=	<<1735.9739, -1669.6835, 111.5818>>
				BREAK
				CASE 1
					vDelLocation 	=	<<-1527.9689, 857.5285, 180.6074>>
				BREAK
				CASE 2
					vDelLocation 	=	<<-2010, 3383, 30.7>>
				BREAK			
			ENDSWITCH			
			vDelDimensions 	=	<<LOCATE_SIZE_ON_FOOT_ONLY,LOCATE_SIZE_ON_FOOT_ONLY,LOCATE_SIZE_HEIGHT>>			
			fStartHeading 	=	71.7770
			fDelHeading 	= 	109.3098			
		BREAK
			
	ENDSWITCH
		
	PRINTLN("[AM DEAD DROP] - GET_RANDOM_LOCATIONS - vStartLocation = ", vStartLocation, " fStartHeading = ", fStartHeading)
	PRINTLN("[AM DEAD DROP] - GET_RANDOM_LOCATIONS - vDelLocation = ", vDelLocation, " fDelHeading = ", fDelHeading, " vDelDimensions = ", vDelDimensions)
	
ENDPROC

//PURPOSE: Sets the data for this instance of Destroy Veh
PROC GET_RANDOM_SETUP_DETAILS()
	
	GET_RANDOM_LOCATIONS(serverBD.vStartLocation, serverBD.fStartHeading, serverBD.vDeliveryLocation, serverBD.fDeliveryHeading, serverBD.vDeliveryDimensions, serverBD.eRouteName, serverBD.iRoute, serverBD.iVariation)
	
	//Double check we aren't doing the same route as last time
	INT i
	REPEAT 10 i
		IF NOT FM_EVENT_IS_VARIATION_IN_HISTORY_LIST(FMMC_TYPE_DEAD_DROP, serverBD.iRoute, serverBD.iVariation)
			BROADCAST_SET_LAST_FM_EVENT_VARIATION(FM_EVENT_GET_VARIATION_FROM_EVENT_TYPE_AND_SUB_TYPE(FMMC_TYPE_DEAD_DROP, serverBD.iRoute, serverBD.iVariation))
			PRINTLN("[AM DEAD DROP] - ROUTE IS DIFFERENT TO THE LAST ONE")
			EXIT
		ELSE
			PRINTLN("[AM DEAD DROP] - REGRAB ROUTE - CURRENT IS THE SAME AS THE LAST ONE")
			GET_RANDOM_LOCATIONS(serverBD.vStartLocation, serverBD.fStartHeading, serverBD.vDeliveryLocation, serverBD.fDeliveryHeading, serverBD.vDeliveryDimensions, serverBD.eRouteName, serverBD.iRoute, serverBD.iVariation)
		ENDIF
	ENDREPEAT	
		
ENDPROC

FUNC INT GET_MINIMUM_PARTICIPATION_MULTIPLIER()
	FLOAT fAmount = TO_FLOAT((iParticipationTime / 60000))
	
	IF fAmount > TO_FLOAT(g_sMPTunables.iParticipation_T_Cap)
		fAmount = TO_FLOAT(g_sMPTunables.iParticipation_T_Cap)
	ENDIF
	
	IF fAmount < 2
		PRINTLN("[AM DEAD DROP] - GET_MINIMUM_PARTICIPATION_MULTIPLIER - Participant has been in event less than 2 minutes, returning 1")
		RETURN 1
	ENDIF
	
	PRINTLN("[AM DEAD DROP] - GET_MINIMUM_PARTICIPATION_MULTIPLIER - Participant has been in session for ", FLOOR(fAmount)," minutes. Returning this as multiplier")
	RETURN FLOOR(fAmount)
ENDFUNC

FUNC INT WINNER_CASH()
	RETURN ROUND((g_sMPTunables.iDEAD_DROP_DEFAULT_CASH * g_sMPTunables.fDead_Drop_Event_Multiplier_Cash))
ENDFUNC

FUNC INT GET_WINNER_REWARD()
	RETURN WINNER_CASH()
ENDFUNC

//PURPOSE: Returns the Cash reward for completing the objective
FUNC INT GET_CASH_REWARD()
	INT iAmount = (g_sMPTunables.iDEAD_DROP_MINIMUM_PARTICIPATION_CASH * GET_MINIMUM_PARTICIPATION_MULTIPLIER())
		
	PRINTLN("[AM DEAD DROP] - GET_CASH_REWARD - Participant to receive $", iAmount)
	RETURN iAmount
ENDFUNC

FUNC INT WINNER_RP()
	RETURN ROUND((g_sMPTunables.iDEAD_DROP_DEFAULT_RP * g_sMPTunables.fDead_Drop_Event_Multiplier_RP))
ENDFUNC

//PURPOSE: Returns the RP reward for completing the objective
FUNC INT GET_RP_REWARD(BOOL bDeliverer, BOOL bHelpAndShardInfo = FALSE)	
	INT iAmount = (g_sMPTunables.iDEAD_DROP_MINIMUM_PARTICIPATION_RP * GET_MINIMUM_PARTICIPATION_MULTIPLIER())
	
	IF bHelpAndShardInfo
		WINNER_RP()
	ENDIF
	
	IF bDeliverer
		PRINTLN("[AM DEAD DROP] - GET_RP_REWARD - Deliverer to receive ", (WINNER_RP() + iAmount), "RP")
		RETURN (WINNER_RP() + iAmount)
	ENDIF
	
	PRINTLN("[AM DEAD DROP] - GET_RP_REWARD - Participant to receive ", iAmount, "RP")
	RETURN iAmount
ENDFUNC

//Do necessary pre game start ini.
PROC PROCESS_PRE_GAME( MP_MISSION_DATA missionScriptArgs )
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission) ,  missionScriptArgs)	//NUM_NETWORK_PLAYERS, missionScriptArgs)	//
	//NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, PARTICIPANT_ID_TO_INT())  
	
	//Reserve Entities
	RESERVE_NETWORK_MISSION_VEHICLES(1)

	// This makes sure the net script is active, waits untill it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
		
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		#IF IS_DEBUG_BUILD NET_LOG("DEAD DROP -  FAILED TO RECEIVE INITIAL NETWORK BROADCAST")	#ENDIF
		PRINTLN("[AM DEAD DROP] -  MISSION END - FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP D     <----------     ")
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		// This script will not be paused if another script calls PAUSE_GAME
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			GET_RANDOM_SETUP_DETAILS()
			serverBD.piCarrier = INVALID_PLAYER_INDEX()
			serverBD.piDeliverer = INVALID_PLAYER_INDEX()
			serverBD.piCarrierKiller = INVALID_PLAYER_INDEX()
			PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBD.iHashedMac, serverBD.iMatchHistoryId)	
		ENDIF
		
		IF serverBD.eRouteName = eDDR_HANGAR
			UNLOCK_AIRPORT_GATES()
			PRINTLN("[AM DEAD DROP] -  Hangar Route Selected, calling  UNLOCK_AIRPORT_GATES()    <----------     ")
		ENDIF
		
		COMMON_FREEMODE_AMBIENT_EVENTS_PROCESS_PRE_GAME(FMMC_TYPE_DEAD_DROP)
		IF serverBD.bOpenAirportGates
			UNLOCK_AIRPORT_GATES()
		ENDIF
		
		telemetryStruct.m_playersNotified = NETWORK_GET_TOTAL_NUM_PLAYERS()
		telemetryStruct.m_notifiedTime = GET_CLOUD_TIME_AS_INT()
		telemetryStruct.m_startTime	= GET_CLOUD_TIME_AS_INT()
		
		IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
			PRINTLN("[AM DEAD DROP] -  PROCESS_PRE_GAME -  GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING - I am joining in progress.   <----------     ")
		ENDIF
		
		playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_INI
		PRINTLN("[AM DEAD DROP] -  PROCESS_PRE_GAME - PRE_GAME DONE     <----------     ")
	ELSE
		#IF IS_DEBUG_BUILD NET_LOG("DEAD DROP -  NETWORK_IS_GAME_IN_PROGRESS = FALSE")	#ENDIF
		PRINTLN("[AM DEAD DROP] -  MISSION END - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP Q     <----------     ")
		SCRIPT_CLEANUP()
	ENDIF
ENDPROC

/// PURPOSE:
///    Draws a marker above the carrier and its not you
PROC MAINTAIN_TARGET_MARKER()
	IF SHOULD_UI_BE_HIDDEN()
		EXIT
	ENDIF
	
	INT r,g,b,a
	r = 255
	g = 0
	b = 0
	a = 0
	
	//Not invalid
	IF serverBD.piCarrier != INVALID_PLAYER_INDEX()
		//I'm ok
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			//it's not me
			IF serverBD.piCarrier != PLAYER_ID()
				//the carrier is ok
				IF IS_NET_PLAYER_OK(serverBD.piCarrier)
					VECTOR vTargetCoords = GET_ENTITY_COORDS(GET_PLAYER_PED(serverBD.piCarrier))
					IF IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerBitSet, biP_ParticipantViaDistance)
					AND NOT (GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
					OR GB_IS_PLAYER_PERMANENT_PARTICIPANT(PLAYER_ID()))
						IF NOT AM_I_SPECTATING_THE_CARRIER()
							IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
							AND GB_IS_PLAYER_MEMBER_OF_THIS_GANG(serverBD.piCarrier, GB_GET_LOCAL_PLAYER_GANG_BOSS())
								GET_HUD_COLOUR(GET_PLAYER_HUD_COLOUR(serverBD.piCarrier), r, g, b, a)
								IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(serverBD.piCarrier))
								AND IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(serverBD.piCarrier), TRUE))
									FM_EVENTDRAW_MARKER_ABOVE_VEHICLE(GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(serverBD.piCarrier), TRUE), r, g, b)
								ELSE
									//draw that marker. 
									DRAW_MARKER(MARKER_ARROW, GET_ENTITY_COORDS(GET_PLAYER_PED(serverBD.piCarrier))+<<0,0,2.0>>, <<0,0,0>>, <<180,0,0>>, <<0.5,0.5,0.5>>, r, g, b, 100, TRUE, TRUE)
								ENDIF
							ELSE
								IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(serverBD.piCarrier))
								AND IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(serverBD.piCarrier), TRUE))
									FM_EVENTDRAW_MARKER_ABOVE_VEHICLE(GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(serverBD.piCarrier), TRUE), r, g, b)
								ELSE
									//draw that marker. 
									DRAW_MARKER(MARKER_ARROW, GET_ENTITY_COORDS(GET_PLAYER_PED(serverBD.piCarrier))+<<0,0,2.0>>, <<0,0,0>>, <<180,0,0>>, <<0.5,0.5,0.5>>, r, g, b, 100, TRUE, TRUE)
								ENDIF
							ENDIF
						ENDIF
						IF NOT IS_PLAYER_IN_CORONA()
						AND NOT IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), FALSE)
						AND NOT IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
							//Set up the span locations
							bSetSpawnLocations = TRUE
							SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_NEAR_SPECIFIC_COORDS)
							SETUP_SPECIFIC_SPAWN_LOCATION(vTargetCoords, 0.0, 250.0, TRUE, FMEVENT_MINIMUM_SPAWN_DISTANCE_FROM_ACTION, FALSE, TRUE, 65, TRUE, TRUE)
						ELSE
							IF bSetSpawnLocations
								PRINTLN("[AM DEAD DROP] - CLEAR_PLAYER_NEXT_RESPAWN_LOCATION   <----------     ")
								CLEAR_PLAYER_NEXT_RESPAWN_LOCATION()
								CLEAR_SPECIFIC_SPAWN_LOCATION_FROM_THIS_SCRIPT()
								bSetSpawnLocations = FALSE
							ENDIF
						ENDIF
					ELSE
						IF bSetSpawnLocations
							//Clear the spawn locations
							PRINTLN("[AM DEAD DROP] - CLEAR_PLAYER_NEXT_RESPAWN_LOCATION   <----------     ")
							CLEAR_PLAYER_NEXT_RESPAWN_LOCATION()
							CLEAR_SPECIFIC_SPAWN_LOCATION_FROM_THIS_SCRIPT()
							bSetSpawnLocations = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//Server only function that checks to see if the game mode should now end. For this test script, it if time has passed.
FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET()
	
	#IF IS_DEBUG_BUILD
		IF bHostEndMissionNow
			PRINTLN("[AM DEAD DROP] -  HAVE_MISSION_END_CONDITIONS_BEEN_MET - bHostEndMissionNow = TRUE    <----------     ")
			RETURN TRUE
		ENDIF
	#ENDIF	
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_MissionShouldCleanup)
		PRINTLN("[AM DEAD DROP] -  HAVE_MISSION_END_CONDITIONS_BEEN_MET - biS_MissionShouldCleanup SET    <----------     ")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


//Helper function to get a clients game/mission state
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iGameState
ENDFUNC

#IF IS_DEBUG_BUILD
PROC CREATE_WIDGETS()
	INT iPlayer
	TEXT_LABEL_15 tl63

	START_WIDGET_GROUP("DEAD DROP")  
		ADD_WIDGET_BOOL("Warp to Package", bWarpToVeh)
				
		// Gameplay debug, sever only.
		START_WIDGET_GROUP("Server Only Gameplay") 
			ADD_WIDGET_BOOL("End mission", bHostEndMissionNow)			
		STOP_WIDGET_GROUP()		
		
		// Data about the server
		START_WIDGET_GROUP("Server BD") 
			ADD_WIDGET_INT_SLIDER("S. Game state", serverBD.iServerGameState,-1, HIGHEST_INT,1)
		STOP_WIDGET_GROUP()	

		// Data about the clients. * = You.
		START_WIDGET_GROUP("Client BD")  				
			REPEAT NUM_NETWORK_PLAYERS iPlayer
				tl63 = "Player "
				tl63 += iPlayer
				IF iPlayer = PARTICIPANT_ID_TO_INT()
					tl63 += "*"
				ENDIF
				START_WIDGET_GROUP(tl63)
					ADD_WIDGET_INT_SLIDER("Game state", playerBD[iPlayer].iGameState,-1, HIGHEST_INT,1)
				STOP_WIDGET_GROUP()
			ENDREPEAT				
		STOP_WIDGET_GROUP()	
		
	STOP_WIDGET_GROUP()
ENDPROC		

PROC UPDATE_WIDGETS()
	//Warp player to Veh
	IF bWarpToVeh = TRUE
		IF DOES_ENTITY_EXIST(NET_TO_ENT(serverBD.niPackage))
			IF NET_WARP_TO_COORD(GET_ENTITY_COORDS(NET_TO_ENT(serverBD.niPackage))+<<0, 0, 3>>, 0, TRUE, FALSE)
				PRINTLN("[AM DEAD DROP] -  UPDATE_WIDGETS - bWarpToPackage DONE    <----------     ")
				bWarpToVeh = FALSE
			ELSE
				PRINTLN("[AM DEAD DROP] -  UPDATE_WIDGETS - bWarpToPackage IN PROGRESS    <----------     ")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

#ENDIF

FUNC STRING GET_PACKAGE_BLIP_STRING()
	RETURN "DD_BLIP"			//"~BLIP_TEMP_4~"
ENDFUNC

/// PURPOSE:
///    Checks if the passed player is within participation range of the package
/// PARAMS:
///    PlayerID - the index of the player to check
/// RETURNS:
///    TRUE:	if the player is in range
///    FALSE:	if not
//FUNC BOOL IS_PLAYER_WITHIN_PARTICIPATION_RANGE(PLAYER_INDEX PlayerID)
//	IF GET_DISTANCE_BETWEEN_ENTITIES(GET_PLAYER_PED(PlayerID), NET_TO_ENT(serverBD.niPackage), FALSE) <= 100
//		RETURN TRUE
//	ENDIF
//	
//	RETURN FALSE
//ENDFUNC

//PURPOSE: Adds a blip to the Veh
PROC ADD_PACKAGE_BLIP()
	IF SHOULD_UI_BE_HIDDEN()
		EXIT
	ENDIF
	IF NOT DOES_BLIP_EXIST(PackageBlip)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)		
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_AwaitingFirstCollection)
				PackageBlip = ADD_BLIP_FOR_ENTITY(NET_TO_ENT(serverBD.niPackage))
				SET_BLIP_SPRITE(PackageBlip, RADAR_TRACE_DEAD_DROP)
				SET_BLIP_COLOUR_FROM_HUD_COLOUR(PackageBlip, HUD_COLOUR_NET_PLAYER2)
			ELSE
				PackageBlip = ADD_BLIP_FOR_ENTITY(NET_TO_ENT(serverBD.niPackage))
				SET_BLIP_SPRITE(PackageBlip, RADAR_TRACE_DEAD_DROP)
				SET_BLIP_COLOUR_FROM_HUD_COLOUR(PackageBlip, HUD_COLOUR_GREEN)
			ENDIF
			
			IF DOES_BLIP_EXIST(PackageBlip)
				SHOW_HEIGHT_ON_BLIP(PackageBlip, TRUE)
				SET_BLIP_EXTENDED_HEIGHT_THRESHOLD(PackageBlip, TRUE)
				SET_BLIP_FLASH_TIMER(PackageBlip, 7000)
				SHOW_HEIGHT_ON_BLIP(PackageBlip, TRUE)
				SET_BLIP_PRIORITY(PackageBlip, BLIP_PRIORITY_HIGHEST_SPECIAL_LOW)
				SET_BLIP_SCALE(PackageBlip, 1.25)
				SET_BLIP_NAME_FROM_TEXT_FILE(PackageBlip, "DD_BLIP1")	//DEAD DROP
				PRINTLN("[AM DEAD DROP] -  PACKAGE BLIP ADDED ")
			ENDIF
		ELSE
			PRINTLN("[AM DEAD DROP] -  ADD_PACKAGE_BLIP - Not adding package blip as NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID is returning false")
		ENDIF
	ELSE
		// Maintain the correct blip
		IF IS_BIT_SET(serverBD.iServerBitSet, biS_AwaitingFirstCollection)
			IF GET_BLIP_SPRITE(PackageBlip) != RADAR_TRACE_DEAD_DROP
				SET_BLIP_SPRITE(PackageBlip, RADAR_TRACE_DEAD_DROP)
			ENDIF
			IF GET_BLIP_COLOUR(PackageBlip) != GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_NET_PLAYER2)
				SET_BLIP_COLOUR_FROM_HUD_COLOUR(PackageBlip, HUD_COLOUR_NET_PLAYER2)
			ENDIF
		ELSE
			IF GET_BLIP_SPRITE(PackageBlip) != RADAR_TRACE_DEAD_DROP
				SET_BLIP_SPRITE(PackageBlip, RADAR_TRACE_DEAD_DROP)
			ENDIF
			IF GET_BLIP_COLOUR(PackageBlip) != GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_NET_PLAYER2)
				SET_BLIP_COLOUR_FROM_HUD_COLOUR(PackageBlip, HUD_COLOUR_GREEN)
			ENDIF
			SET_BLIP_SCALE(PackageBlip, 1.25)
			SHOW_HEIGHT_ON_BLIP(PackageBlip, TRUE)
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_PACKAGE_BLIP()
	IF DOES_BLIP_EXIST(PackageBlip)
		REMOVE_BLIP(PackageBlip)
	ENDIF
ENDPROC

/// PURPOSE:
///    Adds a blip to the player
PROC ADD_PLAYER_BLIP()
	IF SHOULD_UI_BE_HIDDEN()
		EXIT
	ENDIF
	IF NOT DOES_BLIP_EXIST(PlayerBlip)
		IF serverBD.piCarrier != INVALID_PLAYER_INDEX()
			IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piCarrier)
				IF IS_NET_PLAYER_OK(serverBD.piCarrier)
					IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niPackage)
						PlayerBlip = ADD_BLIP_FOR_ENTITY(NET_TO_ENT(serverBD.niPackage))
						SET_BLIP_SPRITE(PlayerBlip, RADAR_TRACE_DEAD_DROP)			
						SHOW_HEIGHT_ON_BLIP(PlayerBlip, TRUE)
						SET_BLIP_EXTENDED_HEIGHT_THRESHOLD(PlayerBlip, TRUE)
						//SET_BLIP_FLASH_TIMER(PlayerBlip, 7000)
						SET_BLIP_PRIORITY(PlayerBlip, BLIP_PRIORITY_HIGHEST_SPECIAL_LOW)
						SET_BLIP_NAME_FROM_TEXT_FILE(PlayerBlip, "DD_BLIP2")	//DEAD DROP
						SET_BLIP_AS_SHORT_RANGE(PlayerBlip, FALSE)
						SET_BLIP_SCALE(PlayerBlip, 1.25)
						PRINTLN("[AM DEAD DROP] -  PLAYER BLIP ADDED ")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_PLAYER_BLIP()
	IF DOES_BLIP_EXIST(PlayerBlip)
		REMOVE_BLIP(PlayerBlip)
	ENDIF
ENDPROC

//PURPOSE: Adds a blip for the delivery location
PROC ADD_DELIVERY_BLIP()
	IF SHOULD_UI_BE_HIDDEN()
		EXIT
	ENDIF
	IF NOT DOES_BLIP_EXIST(DeliveryBlip)
		DeliveryBlip = CREATE_BLIP_FOR_COORD(serverBD.vDeliveryLocation, TRUE)
		SET_BLIP_COLOUR(DeliveryBlip, BLIP_COLOUR_YELLOW)
		SET_BLIP_COLOUR_FROM_HUD_COLOUR(DeliveryBlip, HUD_COLOUR_YELLOW)
		SHOW_HEIGHT_ON_BLIP(DeliveryBlip, TRUE)
		SET_BLIP_FLASH_TIMER(DeliveryBlip, 7000)
		SET_BLIP_PRIORITY(DeliveryBlip, BLIPPRIORITY_MED_HIGH)
		SET_BLIP_NAME_FROM_TEXT_FILE(DeliveryBlip, "DD_BLIP3")	//DEAD DROP Delivery
		SET_BLIP_ROUTE(DeliveryBlip, TRUE)
		PRINTLN("[AM DEAD DROP] -  DELIVERY BLIP ADDED ")
	ENDIF
ENDPROC

PROC REMOVE_DELIVERY_BLIP()
	IF DOES_BLIP_EXIST(DeliveryBlip)
		REMOVE_BLIP(DeliveryBlip)
	ENDIF
ENDPROC

FUNC BOOL AM_I_RESTRICTED(BOOL bIgnoreSCTV = FALSE)
	IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	IF bIgnoreSCTV = FALSE
		IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_SCTV(PLAYER_ID())
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEAR_CONTEND_OBJECTIVE_TEXT()
	IF Has_This_MP_Objective_Text_Been_Received("DD_GT0")
		Clear_This_Objective_Text("DD_GT0")
	ENDIF
ENDPROC

PROC CLEAR_DELIVER_OBJECTIVE_TEXT()
	IF Has_This_MP_Objective_Text_Been_Received("DD_GT1")
		Clear_This_Objective_Text("DD_GT1")
	ENDIF
ENDPROC

PROC CLEAR_KILL_OBJECTIVE_TEXT()
	IF carrier != INVALID_PLAYER_INDEX()
		IF Has_This_MP_Objective_Text_With_Player_Name_Been_Received("DD_GT2", carrier)
			Clear_This_Objective_Text("DD_GT2")
			carrier = INVALID_PLAYER_INDEX()
		ENDIF
	ENDIF
ENDPROC

PROC CLEAR_ALL_OBJECTIVE_TEXT()
	CLEAR_CONTEND_OBJECTIVE_TEXT()
	CLEAR_DELIVER_OBJECTIVE_TEXT()
	CLEAR_KILL_OBJECTIVE_TEXT()
ENDPROC

PROC MAINTAIN_CONTEND_OBJECTIVE_TEXT()
	CLEAR_DELIVER_OBJECTIVE_TEXT()
	CLEAR_KILL_OBJECTIVE_TEXT()
	IF IS_PARTICIPANT_FULLY_TAKING_PART(PARTICIPANT_ID_TO_INT())
		IF NOT AM_I_RESTRICTED(TRUE)
			IF NOT Is_There_Any_Current_Objective_Text_From_This_Script()
				Print_Objective_Text("DD_GT0")
			ENDIF
		ELSE
			CLEAR_CONTEND_OBJECTIVE_TEXT()
		ENDIF
	ELSE
		CLEAR_CONTEND_OBJECTIVE_TEXT()
	ENDIF
ENDPROC

PROC MAINTAIN_DELIVER_OBJECTIVE_TEXT()
	CLEAR_CONTEND_OBJECTIVE_TEXT()
	CLEAR_KILL_OBJECTIVE_TEXT()
	IF IS_PARTICIPANT_FULLY_TAKING_PART(PARTICIPANT_ID_TO_INT())
		IF NOT AM_I_RESTRICTED(TRUE)
			IF NOT Is_There_Any_Current_Objective_Text_From_This_Script()
				Print_Objective_Text("DD_GT1")
			ENDIF
		ELSE
			CLEAR_DELIVER_OBJECTIVE_TEXT()
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_KILL_OBJECTIVE_TEXT()
	CLEAR_CONTEND_OBJECTIVE_TEXT()
	CLEAR_DELIVER_OBJECTIVE_TEXT()
	IF IS_PARTICIPANT_FULLY_TAKING_PART(PARTICIPANT_ID_TO_INT())
		IF NOT AM_I_RESTRICTED(TRUE)
			IF serverBD.piCarrier != INVALID_PLAYER_INDEX()
				IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piCarrier)
				AND NETWORK_IS_PLAYER_A_PARTICIPANT(serverBD.piCarrier)
					IF carrier != serverBD.piCarrier
					OR bUpdateObjectiveGangMembershipChange
						carrier = serverBD.piCarrier
						bUpdateObjectiveGangMembershipChange = FALSE
					
						IF NETWORK_IS_PLAYER_ACTIVE(carrier)
						AND NETWORK_IS_PLAYER_A_PARTICIPANT(carrier)
							IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
							AND GB_IS_PLAYER_MEMBER_OF_THIS_GANG(carrier, GB_GET_LOCAL_PLAYER_GANG_BOSS())
								Print_Objective_Text_With_Player_Name_And_String_Coloured("DD_GT2b", carrier, "DD_PACKAGE", DEFAULT, GET_PLAYER_HUD_COLOUR(carrier))
							ELSE
								Print_Objective_Text_With_Player_Name("DD_GT2", carrier)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			CLEAR_KILL_OBJECTIVE_TEXT()
		ENDIF
	ELSE
		CLEAR_KILL_OBJECTIVE_TEXT()
	ENDIF
ENDPROC

PROC MAINTAIN_PLAYER_BLIP_COLOUR()
	IF serverBD.piCarrier != INVALID_PLAYER_INDEX()
	AND DOES_BLIP_EXIST(PlayerBlip)
		IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
		AND GB_IS_PLAYER_MEMBER_OF_A_GANG(serverBD.piCarrier)
		AND GB_IS_PLAYER_MEMBER_OF_THIS_GANG(serverBD.piCarrier, GB_GET_LOCAL_PLAYER_GANG_BOSS())
			IF GET_BLIP_COLOUR(PlayerBlip) != GET_BLIP_COLOUR_FROM_HUD_COLOUR(GET_PLAYER_HUD_COLOUR(serverBD.piCarrier))
				SET_BLIP_COLOUR_FROM_HUD_COLOUR(PlayerBlip, GET_PLAYER_HUD_COLOUR(serverBD.piCarrier))
			ENDIF
		ELSE
			IF GET_BLIP_COLOUR(PlayerBlip) != GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_RED)
				SET_BLIP_COLOUR_FROM_HUD_COLOUR(PlayerBlip, HUD_COLOUR_RED)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_BLIPS_AND_OBJECTIVE_TEXT()
	IF SHOULD_UI_BE_HIDDEN()
		REMOVE_BLIPS()
		CLEAR_ALL_OBJECTIVE_TEXT()
		EXIT
	ENDIF

	SWITCH playerBD[PARTICIPANT_ID_TO_INT()].eStage
	
		CASE eAD_IDLE
			REMOVE_PLAYER_BLIP()
			REMOVE_DELIVERY_BLIP()
			ADD_PACKAGE_BLIP()
		BREAK
		
		CASE eAD_CONTEND
			REMOVE_PLAYER_BLIP()
			REMOVE_DELIVERY_BLIP()
			ADD_PACKAGE_BLIP()
			IF NOT IS_PLAYER_SCTV(PLAYER_ID())
			OR (IS_PLAYER_SCTV(PLAYER_ID()) AND NOT IS_SPECTATOR_FOCUS_PED_DEAD())
				MAINTAIN_CONTEND_OBJECTIVE_TEXT()
			ENDIF
		BREAK
		
		CASE eAD_DELIVER
			IF AM_I_SPECTATING_A_NON_CARRIER_PARTICIPANT()
				IF IS_A_SPECTATOR_CAM_RUNNING()
				AND NOT IS_SPECTATOR_HUD_HIDDEN()
					REMOVE_PACKAGE_BLIP()
					REMOVE_DELIVERY_BLIP()
					ADD_PLAYER_BLIP()
					MAINTAIN_PLAYER_BLIP_COLOUR()
					MAINTAIN_KILL_OBJECTIVE_TEXT()
				ENDIF
			ELSE
				REMOVE_PLAYER_BLIP()
				REMOVE_PACKAGE_BLIP()
				ADD_DELIVERY_BLIP()
				MAINTAIN_DELIVER_OBJECTIVE_TEXT()
			ENDIF
		BREAK
		
		CASE eAD_KILL
			IF AM_I_SPECTATING_THE_CARRIER()
				IF IS_A_SPECTATOR_CAM_RUNNING()
				AND NOT IS_SPECTATOR_HUD_HIDDEN()
					REMOVE_PLAYER_BLIP()
					REMOVE_PACKAGE_BLIP()
					ADD_DELIVERY_BLIP()
					MAINTAIN_DELIVER_OBJECTIVE_TEXT()
				ENDIF
			ELSE
				REMOVE_PACKAGE_BLIP()
				REMOVE_DELIVERY_BLIP()
				ADD_PLAYER_BLIP()
				MAINTAIN_PLAYER_BLIP_COLOUR()
				MAINTAIN_KILL_OBJECTIVE_TEXT()
			ENDIF
		BREAK
		
		CASE eAD_OFF_MISSION
			IF NOT AM_I_SPECTATING_A_NON_CARRIER_PARTICIPANT()
			AND NOT AM_I_SPECTATING_THE_CARRIER()
				CLEAR_ALL_OBJECTIVE_TEXT()
			ENDIF
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_AwaitingFirstCollection)
				REMOVE_PLAYER_BLIP()
				REMOVE_DELIVERY_BLIP()
				ADD_PACKAGE_BLIP()
			ELSE
				IF IS_BIT_SET(serverBD.iServerBitSet, biS_PackageCollected)
					IF AM_I_SPECTATING_THE_CARRIER()
						IF IS_A_SPECTATOR_CAM_RUNNING()
						AND NOT IS_SPECTATOR_HUD_HIDDEN()
							REMOVE_PLAYER_BLIP()
							REMOVE_PACKAGE_BLIP()
							ADD_DELIVERY_BLIP()
							IF NOT IS_SPECTATOR_FOCUS_PED_DEAD()
								MAINTAIN_DELIVER_OBJECTIVE_TEXT()
							ENDIF
						ENDIF
					ELSE
						REMOVE_PACKAGE_BLIP()
						REMOVE_DELIVERY_BLIP()
						ADD_PLAYER_BLIP()
						MAINTAIN_PLAYER_BLIP_COLOUR()
						IF AM_I_SPECTATING_A_NON_CARRIER_PARTICIPANT()
							IF IS_A_SPECTATOR_CAM_RUNNING()
							AND NOT IS_SPECTATOR_HUD_HIDDEN()
							AND NOT IS_SPECTATOR_FOCUS_PED_DEAD()
								MAINTAIN_KILL_OBJECTIVE_TEXT()
							ENDIF
						ENDIF
					ENDIF
				ELSE
					REMOVE_PLAYER_BLIP()
					REMOVE_DELIVERY_BLIP()
					ADD_PACKAGE_BLIP()
					IF AM_I_SPECTATING_THE_CARRIER()
					OR AM_I_SPECTATING_A_NON_CARRIER_PARTICIPANT()
						IF NOT IS_SPECTATOR_FOCUS_PED_DEAD()
							MAINTAIN_CONTEND_OBJECTIVE_TEXT()
						ENDIF
					ENDIF
				ENDIF	
			ENDIF
		BREAK
		
		CASE eAD_END
			CLEAR_ALL_OBJECTIVE_TEXT()
			REMOVE_BLIPS()
		BREAK
	
	ENDSWITCH

ENDPROC


/// PURPOSE:
///    Handle the help text
/// PARAMS:
///    iHelp - The index of the help text to draw
PROC DO_HELP_TEXT(INT iHelp)
	IF SHOULD_UI_BE_HIDDEN()
		EXIT
	ENDIF
	IF serverBD.eStage = eAD_END
	OR serverBD.eStage = eAD_CLEANUP
		EXIT
	ENDIF
	SWITCH iHelp
		CASE 0
			IF NOT IS_BIT_SET(iBoolsBitSet, biHelp0Done)
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_PAUSE_MENU_ACTIVE()
					PRINT_HELP_NO_SOUND("DD_HELP0", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)	// A DEAD DROP event is available. Collect the package and deliver it for $~1~.
					SET_FREEMODE_EVENT_HELP_BACKGROUND()
					SET_BIT(iBoolsBitSet, biHelp0Done)
					PRINTLN("[AM DEAD DROP] -  HELP TEXT - biHelp0Done SET    <----------     ")
				ENDIF
			ENDIF
		BREAK
		CASE 1
			IF NOT IS_BIT_SET(iBoolsBitSet, biHelp1Done)
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_PAUSE_MENU_ACTIVE()
					PRINT_HELP_WITH_NUMBER_NO_SOUND("DD_HELP1",  GET_WINNER_REWARD(), DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)	// A DEAD DROP event is available. Collect the package and deliver it for $~1~.
					SET_FREEMODE_EVENT_HELP_BACKGROUND(FALSE)
					PLAY_SOUND_FRONTEND(-1, "Event_Start_Text", "GTAO_FM_Events_Soundset", FALSE)
					SET_BIT(iBoolsBitSet, biHelp1Done)
					PRINTLN("[AM DEAD DROP] -  HELP TEXT - biHelp1Done SET    <----------     ")
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF NOT IS_BIT_SET(iBoolsBitSet, biHelp2Done)
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_FM_EVENT_START)
					PRINT_HELP_WITH_NUMBER_NO_SOUND("DD_HELP2", GET_WINNER_REWARD(), DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)	// You have collected the ~g~Package. ~s~Deliver it before the time expires to receive $~1~.
					SET_FREEMODE_EVENT_HELP_BACKGROUND()
					SET_BIT(iBoolsBitSet, biHelp2Done)
					PRINTLN("[AM DEAD DROP] - HELP TEXT - biHelp2Done SET    <----------     ")
				ENDIF
			ENDIF
		BREAK
		CASE 3
			IF NOT IS_BIT_SET(iBoolsBitSet, biHelp3Done)
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_PAUSE_MENU_ACTIVE()
					PRINT_HELP_NO_SOUND("DD_HELP3", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)									// The delivery time has expired. The DEAD DROP event is no longer available.
					SET_FREEMODE_EVENT_HELP_BACKGROUND()
					SET_BIT(iBoolsBitSet, biHelp3Done)
					PRINTLN("[AM DEAD DROP] - HELP TEXT - biHelp3Done SET    <----------     ")
				ENDIF
			ENDIF
		BREAK		
		CASE 4
			IF NOT IS_BIT_SET(iBoolsBitSet, biHelp4Done)
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_FM_EVENT_START)
					IF serverBD.piCarrier != INVALID_PLAYER_INDEX()
					
						IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
						AND GB_IS_PLAYER_MEMBER_OF_THIS_GANG(serverBD.piCarrier, GB_GET_LOCAL_PLAYER_GANG_BOSS())
							PRINT_HELP_WITH_LITERAL_STRING_AND_COLOURED_STRING_NO_SOUND("DD_HELP4b", GET_PLAYER_NAME(serverBD.piCarrier), HUD_COLOUR_WHITE, "DD_BLIPN", GET_PLAYER_HUD_COLOUR(serverBD.piCarrier), DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
						ELSE
							PRINT_HELP_WITH_LITERAL_STRING_AND_COLOURED_STRING_NO_SOUND("DD_HELP4", GET_PLAYER_NAME(serverBD.piCarrier), HUD_COLOUR_WHITE, "DD_BLIPN", HUD_COLOUR_RED, DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
						ENDIF
						
						SET_FREEMODE_EVENT_HELP_BACKGROUND()
						SET_BIT(iBoolsBitSet, biHelp4Done)
						PRINTLN("[AM DEAD DROP] - HELP TEXT - biHelp4Done SET    <----------     ")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 5
			IF NOT IS_BIT_SET(iBoolsBitSet, biHelp5Done)
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_PAUSE_MENU_ACTIVE()
					PRINT_HELP_WITH_PLAYER_NAME_NO_SOUND("DD_HELP5", GET_PLAYER_NAME(serverBD.piCarrier), HUD_COLOUR_WHITE, DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)	// The DEAD DROP event is no longer available.
					SET_FREEMODE_EVENT_HELP_BACKGROUND()
					SET_BIT(iBoolsBitSet, biHelp5Done)
					PRINTLN("[AM DEAD DROP] - HELP TEXT - biHelp5Done SET    <----------     ")
				ENDIF
			ENDIF
		BREAK
		
		CASE 6
			IF NOT IS_BIT_SET(iBoolsBitSet, biHelp6Done)
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_PAUSE_MENU_ACTIVE()
					PRINT_HELP_NO_SOUND("DD_HELP6", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
					SET_FREEMODE_EVENT_HELP_BACKGROUND()
					SET_BIT(iBoolsBitSet, biHelp6Done)
					PRINTLN("[AM DEAD DROP] - HELP TEXT - biHelp6Done SET    <----------     ")
				ENDIF
			ENDIF
		BREAK
		
		CASE 7
			IF NOT IS_BIT_SET(iBoolsBitSet, biHelp7Done)
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_FM_EVENT_START)
					PRINT_HELP_NO_SOUND("DD_HELP7", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
					SET_FREEMODE_EVENT_HELP_BACKGROUND()
					SET_BIT(iBoolsBitSet, biHelp7Done)
					PRINTLN("[AM DEAD DROP] - HELP TEXT - biHelp7Done SET    <----------     ")
				ENDIF
			ENDIF
		BREAK
		CASE 8
			IF NOT IS_BIT_SET(iBoolsBitSet, biHelp8Done)
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_FM_EVENT_START)
					PRINT_HELP_NO_SOUND("DD_HELP8", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
					SET_FREEMODE_EVENT_HELP_BACKGROUND()
					SET_BIT(iBoolsBitSet, biHelp8Done)
					PRINTLN("[AM DEAD DROP] - HELP TEXT - biHelp8Done SET    <----------     ")
				ENDIF
			ENDIF
		BREAK
		CASE 9
			IF NOT IS_BIT_SET(iBoolsBitSet, biHelp9Done)
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_FM_EVENT_START)
					PRINT_HELP_NO_SOUND("DD_HELP9")
					SET_FREEMODE_EVENT_HELP_BACKGROUND()
					SET_BIT(iBoolsBitSet, biHelp9Done)
					PRINTLN("[AM DEAD DROP] - HELP TEXT - biHelp9Done    <----------     ")
				ENDIF
			ENDIF
		BREAK
		CASE 10
			IF NOT IS_BIT_SET(iBoolsBitSet, biHelp10Done)
				CLEAR_HELP()
				IF NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_FM_EVENT_START)
					PRINT_HELP_NO_SOUND("DD_HELP10", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
					SET_FREEMODE_EVENT_HELP_BACKGROUND()
					SET_BIT(iBoolsBitSet, biHelp10Done)
					PRINTLN("[AM DEAD DROP] - HELP TEXT - biHelp10Done    <----------     ")
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Draws big messages at the start and end of the event
/// PARAMS:
///    iBigMessage - the index of the big message to draw
PROC DO_BIG_MESSAGE(INT iBigMessage)
	IF SHOULD_UI_BE_HIDDEN()
		EXIT
	ENDIF
	SWITCH iBigMessage
		CASE 0
			// PACKAGE COLLECTED. Deliver the package to the destination
			IF NOT IS_BIT_SET(iBoolsBitSet2, biBigM0Done)
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_RADAR_HIDDEN()
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP(TRUE)
					ENDIF					
					IF NOT IS_BIT_SET(iBoolsBitSet2, biBigM0Tracker)
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_START, "DD_BMT0", "DD_BMS0", GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE(), DEFAULT, GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE())
					ELSE
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_START, "DD_BMT0", "DD_BMS0", DEFAULT, DEFAULT)
					ENDIF				
					SET_BIT(iBoolsBitSet2, biBigM0Done)
					SET_BIT(iBoolsBitSet2, biBigM0Tracker)
					PRINTLN("[AM DEAD DROP] - BIG MESSAGE - biBigM0Done SET    <----------     ")
				ENDIF
			ENDIF
		BREAK		
		CASE 1
			// DEAD DROP. ~a~ has the package. Take it from them
			IF NOT IS_BIT_SET(iBoolsBitSet2, biBigM1Done)
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_RADAR_HIDDEN()
					IF serverBD.piCarrier != INVALID_PLAYER_INDEX()
						IF IS_HELP_MESSAGE_BEING_DISPLAYED()
							CLEAR_HELP(TRUE)
						ENDIF
						
						IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
						AND GB_IS_PLAYER_MEMBER_OF_THIS_GANG(serverBD.piCarrier, GB_GET_LOCAL_PLAYER_GANG_BOSS())
							SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_FM_EVENT_START, "DD_BMS1b", GET_PLAYER_NAME(serverBD.piCarrier), GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE(), DEFAULT, "DD_BMT1", GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE())
						ELSE
							SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_FM_EVENT_START, "DD_BMS1", GET_PLAYER_NAME(serverBD.piCarrier), GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE(), DEFAULT, "DD_BMT1", GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE())
						ENDIF
						
						SET_BIT(iBoolsBitSet2, biBigM1Done)
						SET_BIT(iBoolsBitSet2, biBigM0Tracker)
						PRINTLN("[AM DEAD DROP] - BIG MESSAGE - biBigM1Done SET    <----------     ")
					ENDIF
				ENDIF
			ENDIF
		BREAK		
		CASE 2
			// WINNER. You delivered the package
			IF NOT IS_BIT_SET(iBoolsBitSet2, biBigM2Done)
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_RADAR_HIDDEN()
					IF FM_EVENT_END_SHARD_SAFE_TO_DISPLAY()
						IF IS_HELP_MESSAGE_BEING_DISPLAYED()
							CLEAR_HELP(TRUE)
						ENDIF
						CLEAR_ALL_BIG_MESSAGES()
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_WIN, "DD_BMT2", "DD_BMS2", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME)
						SET_BIT(iBoolsBitSet2, biBigM2Done)
						PRINTLN("[AM DEAD DROP] - BIG MESSAGE - biBigM2Done SET    <----------     ")
					ENDIF
				ENDIF
			ENDIF
		BREAK		
		CASE 3
			// EVENT OVER. ~a~ delivered the DEAD DROP package
			IF NOT IS_BIT_SET(iBoolsBitSet2, biBigM3Done)
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_RADAR_HIDDEN()
					IF FM_EVENT_END_SHARD_SAFE_TO_DISPLAY()
						IF serverBD.piDeliverer != INVALID_PLAYER_INDEX()
							IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piDeliverer)
								IF IS_HELP_MESSAGE_BEING_DISPLAYED()
									CLEAR_HELP(TRUE)
								ENDIF
								CLEAR_ALL_BIG_MESSAGES()
								
								IF GB_IS_PLAYER_MEMBER_OF_A_GANG(serverBD.piDeliverer)
									SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_FM_EVENT_FINISHED, "DD_BMS3", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(serverBD.piDeliverer), DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME, "DD_BMT3", DEFAULT)
								ELSE
									SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_FM_EVENT_FINISHED, "DD_BMS3", GET_PLAYER_NAME(serverBD.piDeliverer), DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME, "DD_BMT3", DEFAULT)
								ENDIF
								
								SET_BIT(iBoolsBitSet2, biBigM3Done)
								PRINTLN("[AM DEAD DROP] - BIG MESSAGE - biBigM3Done SET    <----------     ")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK		
		CASE 4
			// EVENT OVER. The delivery time has expired
			IF NOT IS_BIT_SET(iBoolsBitSet2, biBigM4Done)
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_RADAR_HIDDEN()
					IF FM_EVENT_END_SHARD_SAFE_TO_DISPLAY()
						IF IS_HELP_MESSAGE_BEING_DISPLAYED()
							CLEAR_HELP(TRUE)
						ENDIF
						CLEAR_ALL_BIG_MESSAGES()
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_FINISHED, "DD_BMT3", "DD_BMS4", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME)
						SET_BIT(iBoolsBitSet2, biBigM4Done)
						PRINTLN("[AM DEAD DROP] - BIG MESSAGE - biBigM4Done SET    <----------     ")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 5
			// WINNER. You helped deliver the package
			IF NOT IS_BIT_SET(iBoolsBitSet2, biBigM5Done)
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_RADAR_HIDDEN()
					IF FM_EVENT_END_SHARD_SAFE_TO_DISPLAY()
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_WIN, "DD_BMT2", "DD_BMS5", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME)
						SET_BIT(iBoolsBitSet2, biBigM5Done)
						PRINTLN("[AM DEAD DROP] - BIG MESSAGE - biBigM5Done SET    <----------     ")
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  ENTITY CREATION                  /////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////
///    			 PACKAGE		   		///
///////////////////////////////////////////
//PURPOSE: Create the Package
FUNC BOOL CREATE_PACKAGE()
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niPackage)
		IF REQUEST_LOAD_MODEL(serverBD.PackageModel)
			IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
				serverBD.niPackage = OBJ_TO_NET(CREATE_PORTABLE_PICKUP(PICKUP_PORTABLE_CRATE_FIXED_INCAR, serverBD.vStartLocation, TRUE, serverBD.PackageModel))
				PRINTLN("[AM DEAD DROP] - CREATE_PACKAGE - Package has been created! Coords: ", serverBD.vStartLocation)
				
				// Set the package coords again without the offset
				SET_ENTITY_COORDS_NO_OFFSET(NET_TO_OBJ(serverBD.niPackage), serverBD.vStartLocation)
				
				SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(serverBD.niPackage, TRUE)
				
				SET_ENTITY_INVINCIBLE(NET_TO_OBJ(serverBD.niPackage), TRUE)
				SET_OBJECT_FORCE_VEHICLES_TO_AVOID(NET_TO_OBJ(serverBD.niPackage), TRUE)
				
				SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(NET_TO_OBJ(serverBD.niPackage), TRUE)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_OBJ(serverBD.niPackage), FALSE)
				ACTIVATE_PHYSICS(NET_TO_OBJ(serverBD.niPackage))
				SET_ENTITY_LOD_DIST(NET_TO_OBJ(serverBD.niPackage), 1200)				
			ENDIF
		ENDIF
	ENDIF
	
	//Check we have created the vehicle
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niPackage)
		PRINTLN("[AM DEAD DROP] -  FAILING ON PACKAGE CREATION")
		RETURN FALSE
	ENDIF
	
	SET_BIT(serverBD.iServerBitSet, biS_AwaitingFirstCollection)
	
	RETURN TRUE
ENDFUNC

//PURPOSE: Creates the Veh and Driver that will deliver the Vehicle
FUNC BOOL CREATE_DEAD_DROP_PACKAGE()
	IF REQUEST_LOAD_MODEL(serverBD.PackageModel)
		IF CREATE_PACKAGE()
			SET_MODEL_AS_NO_LONGER_NEEDED(serverBD.PackageModel)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Runs through the max num participents and does relevant checks
PROC MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()	
	INT iTempPeakParts, iTempPeakQualifiers
	IF serverBD.iServerGameState != GAME_STATE_END
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iStaggeredParticipant
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iStaggeredParticipant))
				PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iStaggeredParticipant))
				
				IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_ParticipantViaDistance)
				OR FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_FM_EVENT(PlayerId)
					iTempPeakQualifiers++	
				ENDIF
				iTempPeakParts++
				//** CHECKS THAT DON'T NEED THE PLAYER TO BE ALIVE **//
				//DELIVERY CHECK
				IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_DeliveredPackage)
					IF serverBD.piDeliverer = INVALID_PLAYER_INDEX()
						serverBD.piDeliverer = PlayerId
						PRINTLN("[AM DEAD DROP] - ", GET_PLAYER_NAME(PlayerId), " DELIVERED THE PACKAGE")
					ENDIF
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_PackageDelivered)
						SET_BIT(serverBD.iServerBitSet, biS_PackageDelivered)
					ENDIF
					EXIT
				//KILL CHECKS
				ELSE
					IF serverBD.piCarrier = PlayerId
					AND IS_BIT_SET(serverBD.iServerBitSet, biS_PackageCollected)
						IF NOT IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_CollectedPackage)							
							serverBD.piCarrier = INVALID_PLAYER_INDEX()
							CLEAR_BIT(serverBD.iServerBitSet, biS_PackageCollected)
							SET_BIT(serverBD.iServerBitSet, biS_PackageDropped)
							PRINTLN("[AM DEAD DROP] - CARRIER HAS BEEN KILLED - serverBD.piCarrier = INVALID_PLAYER_INDEX. biS_PackageCollected CLEARED - biS_PackageDropped - SET")
						ENDIF
					ENDIF
					
//					// Clear the killer when the killer has awarded themselves RP.
//					IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_IKilledCarrier)
//						IF serverBD.piCarrierKiller != INVALID_PLAYER_INDEX()
//							serverBD.piCarrierKiller = INVALID_PLAYER_INDEX()
//							PRINTLN("[AM DEAD DROP] - ", GET_PLAYER_NAME(PlayerId), " was the carrier killer, they have been rewarded RP, resetting carrier killer.")
//						ENDIF
//					ENDIF
					
					// Check if the carrier becomes inactive
					IF IS_BIT_SET(serverBD.iServerBitSet, biS_PackageCollected)
					AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_PackageDropped)
						IF serverBD.piCarrier != INVALID_PLAYER_INDEX()
							IF NOT NETWORK_IS_PLAYER_ACTIVE(serverBD.piCarrier)
								CLEAR_BIT(serverBD.iServerBitSet, biS_PackageCollected)
								SET_BIT(serverBD.iServerBitSet, biS_PackageDropped)
								serverBD.piCarrier = INVALID_PLAYER_INDEX()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//** CHECKS THAT REQUIRE THE PLAYER TO BE ALIVE **//
				IF IS_NET_PLAYER_OK(PlayerId)
					IF serverBD.piCarrier = INVALID_PLAYER_INDEX()
						IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_CollectedPackage)
							serverBD.piCarrier = PlayerId
							// If the package was previously dropped, it has now be re-collected.
							IF IS_BIT_SET(serverBD.iServerBitSet, biS_PackageDropped)
								CLEAR_BIT(serverBD.iServerBitSet, biS_PackageDropped)
							ENDIF
							SET_BIT(serverBD.iServerBitSet, biS_PackageCollected)
							PRINTLN("[AM DEAD DROP] - serverBD.piCarrier = ", GET_PLAYER_NAME(PlayerId), ". biS_PackageCollected SET")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF iTempPeakParts > serverBD.iPeakParticipants
			serverBD.iPeakParticipants = iTempPeakParts
			PRINTLN(DEBUG_NET_AMBIENT, "[AM DEAD DROP] MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - iTempPeakParts (",iTempPeakParts,") is GREATER than player's participating count so far (",serverBD.iPeakParticipants,"), updating value.")
		ELSE
			INT iPlayers = (serverBD.iPeakParticipants  - iTempPeakParts)		
			IF serverBD.iPlayersLeftInProgress > iPlayers OR serverBD.iPlayersLeftInProgress < iPlayers
				serverBD.iPlayersLeftInProgress = iPlayers
			ENDIF
		ENDIF
		
		IF iTempPeakQualifiers > serverBD.iPeakQualifiers
			serverBD.iPeakQualifiers = iTempPeakQualifiers
			PRINTLN(DEBUG_NET_AMBIENT, "[AM DEAD DROP] MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - iTempPeakQualifiers (",iTempPeakQualifiers,") is GREATER than player's participating count so far (",serverBD.iPeakQualifiers,"), updating value.")
		ENDIF
		
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns the tunable value for the carrier kill cap
FUNC INT GET_CARRIER_KILL_CAP()
	RETURN g_sMPTunables.iDEAD_DROP_CARRIER_KILL_CAP
ENDFUNC

/// PURPOSE:
///    Returns the tunable value for the rp awarded per carrier kill
FUNC INT GET_CARRIER_KILL_RP()
	RETURN g_sMPTunables.iDEAD_DROP_CARRIER_KILLS
ENDFUNC

//PROC MAINTAIN_GIVE_CARRIER_KILLER_CASH()	
//	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_IKilledCarrier)
//		IF serverBD.piCarrierKiller = INVALID_PLAYER_INDEX()
//			CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_IKilledCarrier)
//			PRINTLN("[AM DEAD DROP] - MAINTAIN_GIVE_CARRIER_KILLER_CASH - piCarrier Killer now -1, I am no longer the latest killer.")
//		ENDIF
//	ELSE
//		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AwaitingFirstCollection)
//			IF IS_BIT_SET(serverBD.iServerBitSet, biS_PackageDropped)
//			AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_PackageDelivered)
//				IF serverBD.piCarrierKiller != INVALID_PLAYER_INDEX()
//					IF PLAYER_ID() = serverBD.piCarrierKiller						
////						iNoOfCarrierKills++
////						IF iNoOfCarrierKills <= GET_CARRIER_KILL_CAP()
////							PRINTLN("[AM DEAD DROP] - MAINTAIN_GIVE_CARRIER_KILLER_CASH - GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), \"XPT_KAIE\", XPTYPE_SKILL, XPCATEGORY_COMPLETE_HOLD_UP, 100, 1)")
////							GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "XPT_KAIE", XPTYPE_SKILL, XPCATEGORY_AMB_KILL_DEAD_DROP_PACKAGE_CARRIER, GET_CARRIER_KILL_RP(), 1)
////							telemetryStruct.m_rpEarned += 100
////						ELSE
////							PRINTLN("[AM DEAD DROP] - MAINTAIN_GIVE_CARRIER_KILLER_CASH - Player has killed carrier 20 times, no longer awarding RP")
////						ENDIF
//						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_IKilledCarrier)						
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC

PROC PROCESS_CARRIER_DAMAGE(INT iCount)
	
	PED_INDEX VictimPedID	
	PED_INDEX DamagerPedID	
	PLAYER_INDEX VictimPlayerID	
	PLAYER_INDEX DamagerPlayerID	

	STRUCT_ENTITY_DAMAGE_EVENT sEntityID
	
	GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sEntityID, SIZE_OF(sEntityID))
	
	
	// IF the victim exists
	IF DOES_ENTITY_EXIST(sEntityID.VictimIndex)
		// If the victom is a ped
		IF IS_ENTITY_A_PED(sEntityID.VictimIndex)
			// If the Damager exists
			IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
				// If the Damager is a ped
				IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
					//Get the two peds
					VictimPedID = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)
					DamagerPedID = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
					//If they are both players
					IF IS_PED_A_PLAYER(VictimPedID)
					AND IS_PED_A_PLAYER(DamagerPedID)
						//Get the two players
						VictimPlayerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(VictimPedID)
						DamagerPlayerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(DamagerPedID)
						//If they are both active
						IF NETWORK_IS_PLAYER_ACTIVE(VictimPlayerID)
						AND NETWORK_IS_PLAYER_ACTIVE(DamagerPlayerID)
							IF DamagerPlayerID = PLAYER_ID()
								IF VictimPlayerID = serverBD.piCarrier
									IF sEntityID.VictimDestroyed
										iNoOfCarrierKills++
										IF iNoOfCarrierKills <= GET_CARRIER_KILL_CAP()
											PRINTLN("[AM DEAD DROP] - PROCESS_CARRIER_DAMAGE - GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), \"XPT_KAIE\", XPTYPE_SKILL, XPCATEGORY_COMPLETE_HOLD_UP, 100, 1)")
											GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "XPT_KAIE", XPTYPE_SKILL, XPCATEGORY_AMB_KILL_DEAD_DROP_PACKAGE_CARRIER, GET_CARRIER_KILL_RP(), 1)
											telemetryStruct.m_rpEarned += 100
										ELSE
											PRINTLN("[AM DEAD DROP] - PROCESS_CARRIER_DAMAGE - Player has killed carrier 20 times, no longer awarding RP")
										ENDIF
									ENDIF
									IF NOT HAS_NET_TIMER_STARTED(ParticipationTimer)
										START_NET_TIMER(ParticipationTimer)
									ENDIF
									IF NOT FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_FM_EVENT(PLAYER_ID())
										IF NOT SHOULD_UI_BE_HIDDEN()
										AND NOT FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID())
											SET_PLAYERS_CURRENT_FREEMODE_AMBIENT_EVENT_TYPE(FMMC_TYPE_DEAD_DROP)
											FM_EVENT_SET_PLAYER_IS_PERMANENT_PARTICIPANT_IN_FM_EVENT(TRUE)
											PRINTLN("[AM DEAD DROP] - PROCESS_CARRIER_DAMAGE - FM_EVENT_SET_PLAYER_IS_PERMANENT_PARTICIPANT_IN_FM_EVENT(TRUE)")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC PROCESS_DAMAGE_EVENTS_AM_DEAD_DROP()
	INT iCount
	EVENT_NAMES ThisScriptEvent
	
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount
	
		ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)
		
		IF ThisScriptEvent = EVENT_NETWORK_DAMAGE_ENTITY
			PROCESS_CARRIER_DAMAGE(iCount)
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Maintains the timer that displays informing the players 
///    of the available time remaining to deliver the package
PROC MAINTAIN_DEAD_DROP_EVENT_TIMER()
	HUD_COLOURS timeColour
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_StartTimer)
	AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_PackageDelivered)
		IF HAS_NET_TIMER_STARTED(serverBD.DeliverPackageTimer)
			iTimeRemaining = (GET_MISSION_TIME() - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.DeliverPackageTimer))
			IF iTimeRemaining > 30000
				timeColour = HUD_COLOUR_WHITE
			ELSE
				timeColour = HUD_COLOUR_RED
			ENDIF
					
			IF iTimeRemaining > 0
				IF NOT SHOULD_UI_BE_HIDDEN()
					HANDLE_AMBIENT_EVENT_COUNTDOWN_AUDIO(iTimeRemaining)
					BOTTOM_RIGHT_UI_EVENT_TIMER(iTimeRemaining, timeColour, GET_COUNTDOWN_TIMER_NAME_FOR_EVENT())
				ENDIF
			ELSE
				IF NOT SHOULD_UI_BE_HIDDEN()
					DRAW_GENERIC_TIMER(0, "DD_EVENTEND_R", 0, TIMER_STYLE_DONTUSEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_DONTCARE, DEFAULT, HUD_COLOUR_RED)		
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if the requirements have been met to run the event
/// RETURNS:
///    
FUNC BOOL HAS_PACKAGE_BEEN_COLLECTED_TO_START_EVENT()
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_PackageCollected)
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_StartTimer)
			SET_BIT(serverBD.iServerBitSet, biS_StartTimer)
		ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE	
ENDFUNC

FUNC BOOL HAS_PACKAGE_BEEN_COLLECTED_FOLLOWING_DROP()
	IF serverBD.piCarrier != INVALID_PLAYER_INDEX()
		IF IS_BIT_SET(serverBD.iServerBitSet, biS_PackageCollected)
		AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_PackageDropped)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Checks for the Veh (destroyed, stuck, etc)
PROC CONTROL_CLIENT_PACKAGE_COLLECTION()
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niPackage)
	AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_PackageCollected)
		
		// Print help text when getting close to package
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niPackage)
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_AwaitingFirstCollection)
				IF IS_NET_PLAYER_OK(PLAYER_ID())
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
					AND DOES_ENTITY_EXIST(PLAYER_PED_ID())
						IF IS_ENTITY_AT_ENTITY(NET_TO_OBJ(serverBD.niPackage),PLAYER_PED_ID(),<<PACKAGE_CLOSE_RANGE,PACKAGE_CLOSE_RANGE,PACKAGE_CLOSE_RANGE>>, FALSE )
							DO_HELP_TEXT(6)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		// Wait for package collection
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niPackage)
			IF IS_ENTITY_ATTACHED_TO_ENTITY(NET_TO_OBJ(serverBD.niPackage), PLAYER_PED_ID())
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_CollectedPackage)
					
					IF NOT HAS_NET_TIMER_STARTED(TimeHeldTimer)
						START_NET_TIMER(TimeHeldTimer)
					ENDIF
					IF NOT HAS_NET_TIMER_STARTED(ParticipationTimer)
						START_NET_TIMER(ParticipationTimer)
					ENDIF
					
					PLAY_SOUND_FRONTEND(-1,"Object_Collect_Player", "GTAO_FM_Events_Soundset", FALSE)
					
					// Local player has had package at least once
					IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HaveHadPackage)
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HaveHadPackage)
					ENDIF
					
					// Let others know of collection
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_CollectedPackage)
					
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AwaitingFirstCollection)
						//Send Ticker to Everyone
						SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
						TickerEventData.TickerEvent = TICKER_EVENT_DD_PACKAGE_COLLECTED
						TickerEventData.playerID = PLAYER_ID()
						BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT(FALSE))
					ENDIF
					
					IF NOT IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
						SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY(TRUE)
						PRINTLN("[AM DEAD DROP] - SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY = TRUE, I have a mission critical entity")
					ENDIF
					
					IF NOT FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT(PLAYER_ID())
						COMMON_FREEMODE_AMBIENT_EVENTS_SET_UP(FMMC_TYPE_DEAD_DROP, 0, 0.25)
					ENDIF
					FM_EVENT_SET_PLAYER_IS_CRITICAL_TO_FM_EVENT(TRUE)
					PRINTLN("[AM DEAD DROP] - FM_EVENT_SET_PLAYER_IS_CRITICAL_TO_FM_EVENT(TRUE), I am now mission critical")
					
					IF NOT FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_FM_EVENT(PLAYER_ID())
						FM_EVENT_SET_PLAYER_IS_PERMANENT_PARTICIPANT_IN_FM_EVENT(TRUE)
					ENDIF
					
					// Block blips for carrier
					IF NOT bBlipsBlocked
						BLOCK_BLIPS(TRUE)
					ENDIF
					
					PRINTLN("[AM DEAD DROP] - biS_PackageCollected SET")
				ENDIF
			ENDIF
		ENDIF
	ELSE
		// Check for carrier being killed and create a new object at death coords
		IF serverBD.piCarrier = PLAYER_ID()
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_CollectedPackage)
				IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
					PRINTLN("[AM DEAD DROP] - I was the carrier and I have been killed! biP_CollectedPackage CLEARED, text and blip cleared")
				
					// Player carrying the package has been killed
					CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_CollectedPackage)
																		
					// Clear Objective text
					Clear_Any_Objective_Text_From_This_Script()
					
					IF bBlipsBlocked
						BLOCK_BLIPS(FALSE)
					ENDIF
					
					IF IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
						SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY(FALSE)
						PRINTLN("[AM DEAD DROP] - SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY = FALSE, I no longer have a mission critical entity")
					ENDIF
					
					FM_EVENT_SET_PLAYER_IS_CRITICAL_TO_FM_EVENT(FALSE)
					PRINTLN("[AM DEAD DROP] - FM_EVENT_SET_PLAYER_IS_CRITICAL_TO_FM_EVENT(FALSE), I am no longer mission critical")
					
					IF DOES_BLIP_EXIST(DeliveryBlip)
						REMOVE_BLIP(DeliveryBlip)
						//DELETE_CHECKPOINT(DeliveryCP)
					ENDIF
				ENDIF					
			ENDIF
		ENDIF
		
		// Check for when the carrier is killed
		IF serverBD.piCarrier != PLAYER_ID()
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_PackageDropped)								
				IF DOES_BLIP_EXIST(PlayerBlip)
					REMOVE_BLIP(PlayerBlip)
				ENDIF
			ENDIF			
		ENDIF
		
		//Delivered Package
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DeliveredPackage)
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_CollectedPackage)
				IF NOT IS_PLAYER_SCTV(PLAYER_ID())	// Cant deliver while spectating
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), serverBD.vDeliveryLocation, serverBD.vDeliveryDimensions, TRUE)
						
						IF bBlipsBlocked
							BLOCK_BLIPS(FALSE)
						ENDIF
						
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DeliveredPackage)				
						
						IF TAKE_CONTROL_OF_NET_ID(serverBD.niPackage)
							DELETE_NET_ID(serverBD.niPackage)
						ENDIF
						
						PRINTLN("[AM DEAD DROP] - biP_DeliveredPackage SET")
					ENDIF
				ENDIF
			ENDIF
		ENDIF				
	ENDIF	
ENDPROC

PROC HANDLE_EVENT_END_PACKAGE_LOGIC()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
		IF IS_BIT_SET(serverBD.iServerBitSet, biS_PackageDelivered)
		OR IS_BIT_SET(serverBD.iServerBitSet, biS_MissionEndTimerHit)
		OR IS_BIT_SET(serverBD.iServerBitSet, biS_MissionTimedOut)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niPackage)
				IF IS_ENTITY_ATTACHED_TO_ANY_PED(NET_TO_OBJ(serverBD.niPackage))
					DETACH_ENTITY(NET_TO_OBJ(serverBD.niPackage))
				ENDIF
				SET_ENTITY_VISIBLE(NET_TO_OBJ(serverBD.niPackage), FALSE)
				PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.niPackage), TRUE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if the player should return to idle stage
/// RETURNS:
///    TRUE:  if the package has not been collected at all.
FUNC BOOL SHOULD_PLAYER_MOVE_TO_IDLE_STAGE()
	// If the player is delivering or killing
	IF playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_DELIVER
	OR playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_KILL
	OR playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_OFF_MISSION
	OR playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CONTEND
		// If the package has been dropped and the server knows it has not been collected
		IF IS_BIT_SET(serverBD.iServerBitSet, biS_AwaitingFirstCollection)
			PRINTLN("[AM DEAD DROP] - SHOULD_PLAYER_MOVE_TO_IDLE_STAGE - TRUE, player should move to idle stage")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the player should return to contend stage
/// RETURNS:
///    TRUE:  if the package has been dropped and it has not been collected or delivered
FUNC BOOL SHOULD_PLAYER_MOVE_TO_CONTEND_STAGE()
	// If the player is delivering or killing
	IF playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_DELIVER
	OR playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_KILL
	OR playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_OFF_MISSION
		// If the package has been dropped and the server knows it has not been collected
		IF IS_BIT_SET(serverBD.iServerBitSet, biS_PackageDropped)
		AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_PackageCollected)
			// If the player does not think they have the package and they have no delivered it
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_CollectedPackage)
			AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DeliveredPackage)
				PRINTLN("[AM DEAD DROP] - SHOULD_PLAYER_MOVE_TO_CONTEND_STAGE - TRUE, player should move to contend stage")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the player should return to kill stage
/// RETURNS:
///    TRUE:  if the package has been collected by another player
FUNC BOOL SHOULD_PLAYER_MOVE_TO_KILL_STAGE()
	// If the player is delivering or killing
	IF playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_DELIVER
	OR playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CONTEND
	OR playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_OFF_MISSION
		// If the package has been dropped and the server knows it has not been collected
		IF IS_BIT_SET(serverBD.iServerBitSet, biS_PackageCollected)
		AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_PackageDropped)
			// If the player does not think they have the package and they have no delivered it
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_CollectedPackage)
			AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DeliveredPackage)
				PRINTLN("[AM DEAD DROP] - SHOULD_PLAYER_MOVE_TO_KILL_STAGE - TRUE, player should move to kill stage")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the player should return to deliver stage
/// RETURNS:
///    TRUE:  if the pplayer has collected the package
FUNC BOOL SHOULD_PLAYER_MOVE_TO_DELIVER_STAGE()
	// If the player is delivering or killing
	IF playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CONTEND
	OR playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_KILL
	OR playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_OFF_MISSION
		// If the package has been dropped and the server knows it has not been collected
		IF IS_BIT_SET(serverBD.iServerBitSet, biS_PackageCollected)
		AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_PackageDelivered)
			// If the player does not think they have the package and they have no delivered it
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_CollectedPackage)
			AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DeliveredPackage)
				PRINTLN("[AM DEAD DROP] - SHOULD_PLAYER_MOVE_TO_DELIVER_STAGE - TRUE, player should move to deliver stage")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_DPAD_DOWN_LEADERBOARD()
	DRAW_CHALLENGE_DPAD_LBD(chDPStruct.challengeLbdStruct, chDPStruct.siDpadMovie, SUB_CHALLENGE, chDPStruct.dpadVariables, chDPStruct.fmDpadStruct)
ENDPROC


/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MISSION PROCS                  //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

PROC MAINTAIN_LOCAL_PLAYER_PARTICIPATION()
	FLOAT fDistance
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_CollectedPackage)	
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ParticipantViaInteraction)
		CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - MAINTAIN_LOCAL_PLAYER_PARTICIPATION biP_ParticipantViaInteraction has been set.")
	ENDIF
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niPackage)
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ParticipantViaDistance)
			fDistance = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), NET_TO_OBJ(serverBD.niPackage))
			IF fDistance > g_sMPTunables.iFREEMODE_EVENT_UI_EXIT_DISTANCE
				CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ParticipantViaDistance)
				CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - MAINTAIN_LOCAL_PLAYER_PARTICIPATION biP_ParticipantViaDistance has been cleared.")
			ENDIF
		ELSE
			fDistance = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), NET_TO_OBJ(serverBD.niPackage))
			IF fDistance <= g_sMPTunables.iFREEMODE_EVENT_UI_ENTRY_DISTANCE
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ParticipantViaDistance)
				CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - MAINTAIN_LOCAL_PLAYER_PARTICIPATION biP_ParticipantViaDistance has been set.")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Draws a marker above the package when it has been dropped
PROC MAINTAIN_PACKAGE_MARKER()
	IF SHOULD_UI_BE_HIDDEN()
		EXIT
	ENDIF
	INT r, g, b, a
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niPackage)
	AND NOT IS_ENTITY_DEAD(NET_TO_OBJ(serverBD.niPackage))
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_PackageDelivered)
		AND NOT IS_ENTITY_ATTACHED_TO_ANY_PED(NET_TO_OBJ(serverBD.niPackage))
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AwaitingFirstCollection)
			AND IS_BIT_SET(serverBD.iServerBitSet, biS_PackageDropped)
				GET_HUD_COLOUR(HUD_COLOUR_GREEN, r, g, b, a)
				DRAW_MARKER(MARKER_ARROW, GET_ENTITY_COORDS(NET_TO_ENT(serverBD.niPackage))+<<0,0,1>>, <<0,0,0>>, <<180,0,0>>, <<0.5,0.5,0.5>>, r, g, b, 100, TRUE, TRUE)
			ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_AwaitingFirstCollection)
				GET_HUD_COLOUR(HUD_COLOUR_GREEN, r, g, b, a)
				DRAW_MARKER(MARKER_ARROW, GET_ENTITY_COORDS(NET_TO_ENT(serverBD.niPackage))+<<0,0,1>>, <<0,0,0>>, <<180,0,0>>, <<0.5,0.5,0.5>>, r, g, b, 100, TRUE, TRUE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

INT iRoomKeyCache
PROC HANDLE_PACKAGE_IN_SHOP()
		
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
		INT iRoomKey = GET_ROOM_KEY_FROM_ENTITY(NET_TO_OBJ(serverBD.niPackage))
		IF iRoomKey != iRoomKeyCache
			iRoomKeyCache = iRoomKey
			CPRINTLN(DEBUG_NET_AMBIENT, "[AM DEAD DROP] - iRoomKeyCache = iRoomKey = ", iRoomKey)
			IF iRoomKey != 0
				VECTOR vPackagePos = GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.niPackage),FALSE)
				SHOP_NAME_ENUM closestShop = GET_CLOSEST_SHOP_OF_TYPE(vPackagePos)

				VECTOR vShopCoords = GET_SHOP_COORDS(closestShop)

				INTERIOR_INSTANCE_INDEX interiorShop = GET_INTERIOR_AT_COORDS_WITH_TYPE(vShopCoords, GET_SHOP_INTERIOR_TYPE(closestShop))
				INTERIOR_INSTANCE_INDEX interiorPackage = GET_INTERIOR_FROM_ENTITY(NET_TO_OBJ(serverBD.niPackage))
				IF interiorShop != NULL
				AND interiorShop = interiorPackage
					IF NOT MPGlobalsAmbience.bKeepShopDoorsOpen
						MPGlobalsAmbience.bKeepShopDoorsOpen = TRUE
						CPRINTLN(DEBUG_NET_AMBIENT, "[AM DEAD DROP] - MPGlobalsAmbience.bKeepShopDoorsOpen = TRUE")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF iRoomKeyCache = 0
	AND MPGlobalsAmbience.bKeepShopDoorsOpen
		MPGlobalsAmbience.bKeepShopDoorsOpen = FALSE
		CPRINTLN(DEBUG_NET_AMBIENT, "[AM DEAD DROP] - MPGlobalsAmbience.bKeepShopDoorsOpen = FALSE")
	ENDIF

ENDPROC

PROC HANDLE_PACKAGE_ON_YACHT()

	VECTOR vPackageCoords
	BOOL bFindCoord
	
	SWITCH iPackageOnYachtStep
	
		CASE 0
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
				vPackageCoords = GET_ENTITY_COORDS(NET_TO_ENT(serverBD.niPackage))
				IF NOT IS_VECTOR_ZERO(vPackageAvoidCoord)
					vPackageAvoidCoord = <<0.0, 0.0, 0.0>>
				ENDIF
			
				IF IS_POINT_IN_YACHT_BOUNDING_BOX(vPackageCoords, iYachtStagger)
					iPackageOnYachtStep++
					PRINTLN("[AM DEAD DROP] - HANDLE_PACKAGE_ON_YACHT - Package is on yacht ", iYachtStagger)
				ENDIF
				
				IF iPackageOnYachtStep = 0
					iYachtStagger++
				ENDIF
				
				IF iYachtStagger >= NUMBER_OF_PRIVATE_YACHTS
					iYachtStagger = 0
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF serverBD.piCarrier = INVALID_PLAYER_INDEX()
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niPackage)
					bFindCoord = TRUE
					IF IS_VECTOR_ZERO(vPackageAvoidCoord)
						vPackageAvoidCoord = GET_ENTITY_COORDS(NET_TO_ENT(serverBD.niPackage))
						PRINTLN("[AM DEAD DROP] - HANDLE_PACKAGE_ON_YACHT - No one is carrying package, I am in control, vPackageAvoidCoord = ", vPackageAvoidCoord)
					ENDIF
				ENDIF
			ELSE
				IF serverBD.piCarrier = PLAYER_ID()
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niPackage)
						IF IS_VECTOR_ZERO(vPackageAvoidCoord)
							vPackageAvoidCoord = GET_ENTITY_COORDS(NET_TO_ENT(serverBD.niPackage))
							PRINTLN("[AM DEAD DROP] - HANDLE_PACKAGE_ON_YACHT - I am carrying the package, I am in control, vPackageAvoidCoord = ", vPackageAvoidCoord)
						ENDIF
						
						bFindCoord = TRUE
					ELSE
						NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.niPackage)
					ENDIF
				ENDIF
			ENDIF
			
			IF bFindCoord
				SPAWN_SEARCH_PARAMS SpawnSearchParams
				SpawnSearchParams.bConsiderInteriors = FALSE
				SpawnSearchParams.fMinDistFromPlayer = 1.0
				SpawnSearchParams.vAvoidCoords[0] = vPackageAvoidCoord
				SpawnSearchParams.fAvoidRadius[0] = 8.0
				SpawnSearchParams.fAvoidRadius[0] = 100.0
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niPackage)
					IF GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY(vPackageAvoidCoord, 20.0, vWarpPackageCoord, fWarpPackageHeading, SpawnSearchParams)
						vWarpPackageCoord += << 0.0, 0.0, 0.5 >>
						IF IS_ENTITY_ATTACHED_TO_ENTITY(NET_TO_OBJ(serverBD.niPackage), PLAYER_PED_ID())
							DETACH_PORTABLE_PICKUP_FROM_PED(NET_TO_OBJ(serverBD.niPackage))
							PRINTLN("[AM DEAD DROP] - HANDLE_PACKAGE_ON_YACHT - Package has been detached from local player")
						ENDIF
						SET_ENTITY_COORDS_NO_OFFSET(NET_TO_OBJ(serverBD.niPackage), vWarpPackageCoord)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_OBJ(serverBD.niPackage), TRUE)
						
						PRINTLN("[AM DEAD DROP] - HANDLE_PACKAGE_ON_YACHT - Package has been moved! vWarpPackageCoord = ", vWarpPackageCoord)
						
						DO_HELP_TEXT(10)
						iPackageOnYachtStep = 0
					ENDIF
				ENDIF
			ELSE
				IF serverBD.piCarrier != PLAYER_ID()
					vPackageCoords = GET_ENTITY_COORDS(NET_TO_ENT(serverBD.niPackage))
					IF NOT IS_POINT_IN_YACHT_BOUNDING_BOX(vPackageCoords, iYachtStagger)
						PRINTLN("[AM DEAD DROP] - HANDLE_PACKAGE_ON_YACHT - I'm not looking for new coords, just acknowledging its been moved")
						iPackageOnYachtStep = 0
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
		BREAK
	ENDSWITCH
ENDPROC


FUNC INT GET_PARTICIPATION_TIME()
	RETURN GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(ParticipationTimer)
ENDFUNC

/// PURPOSE:
///    Grabs the cash reward for DEAD DROP and gives the cash to players and updates UI.
PROC PROCESS_CLIENT_END_CASH_RP_REWARD(BOOL bDeliverer = FALSE)
	// Only process this once for this event
	IF NOT bProcessedReward
		IF HAVE_MET_REWARD_CONDITIONS()
			INT iCashReward, iRPReward
			
			IF HAS_NET_TIMER_STARTED(ParticipationTimer)
				iParticipationTime = GET_PARTICIPATION_TIME()
			ENDIF

			iRPReward = GET_RP_REWARD(bDeliverer)		
			IF bDeliverer
				iCashReward = GET_WINNER_REWARD()
			ELSE
				FM_EVENT_SETUP_DO_MINIMUM_PARTICIPATION_HELP(TRUE)
			ENDIF
			
			playerBD[PARTICIPANT_ID_TO_INT()].iTotalCash = iCashReward
			
				PRINTLN("[AM DEAD DROP][MAGNATE_GANG_BOSS] - Cash before boss cut: $", iCashReward)
				GB_HANDLE_GANG_BOSS_CUT(iCashReward)
				PRINTLN("[AM DEAD DROP][MAGNATE_GANG_BOSS] - Cash after boss cut: $", iCashReward)
			
			iCashReward += GET_CASH_REWARD()
			PRINTLN("[AM DEAD DROP] - PROCESS_CLIENT_END_CASH_RP_REWARD - Participation cash: $", GET_CASH_REWARD())
			PRINTLN("[AM DEAD DROP] - PROCESS_CLIENT_END_CASH_RP_REWARD - Total cash to give player: $", iCashReward)
			
			IF iCashReward > 0
				IF USE_SERVER_TRANSACTIONS()
					INT iScriptTransactionIndex
					TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_AMBIENT_JOB_DEAD_DROP, iCashReward, iScriptTransactionIndex, FALSE, TRUE) //Add new service
				ELSE
					AMBIENT_JOB_DATA amData
					NETWORK_EARN_FROM_AMBIENT_JOB(iCashReward,"AM_DEAD_DROP",amData)
				ENDIF
				g_i_cashForEndEventShard = iCashReward
				
				NEXT_RP_ADDITION_SHOW_RANKBAR()
				GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "XPT_KAIE", XPTYPE_SKILL, XPCATEGORY_AMB_DELIVER_DEAD_DROP_PACKAGE, iRPReward, 1)
				
				telemetryStruct.m_rpEarned += iRPReward
				telemetryStruct.m_cashEarned += iCashReward	
				IF NOT g_sMPTunables.bDead_Drop_Disable_Share_Cash
					IF telemetryStruct.m_cashEarned > 0
						SET_LAST_JOB_DATA(LAST_JOB_DEAD_DROP, telemetryStruct.m_cashEarned)
					ENDIF
				ENDIF
				PRINTLN("[AM DEAD DROP] - Process cash reward: PROCESS_CLIENT_END_CASH_REWARD $", iCashReward)
			ENDIF
			
			// Locally flag that we have processed the reward
			bProcessedReward = TRUE
		ELSE
			FM_EVENT_SETUP_DO_MINIMUM_PARTICIPATION_HELP(FALSE)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_DELIVERER_MY_GOON()
	IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
		IF serverBD.piDeliverer != INVALID_PLAYER_INDEX()
			IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(serverBD.piDeliverer, PLAYER_ID())
				PRINTLN("[AM DEAD DROP] [ST_GANGBOSS] - IS_DELIVERER_MY_GOON - true, I am a boss and the deliverer is in my gang")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_EVENT_END_REWARDS_AND_MESSAGES()

	// MESSAGES	
	IF Is_There_Any_Current_Objective_Text_From_This_Script()
		Clear_Any_Objective_Text_From_This_Script()
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_MissionTimedOut)
		IF NOT SHOULD_UI_BE_HIDDEN()
		AND bDisplayedEndMessage = FALSE
			PRINT_TICKER("DD_TCNLA")
			bDisplayedEndMessage = TRUE
		ENDIF
		EXIT
	ENDIF
	
	// Player delivered the package
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_PackageDelivered)
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DeliveredPackage)
		OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_CollectedPackage)
			PROCESS_CLIENT_END_CASH_RP_REWARD(TRUE)
			
			DO_BIG_MESSAGE(2)
		ELSE	
			IF IS_PARTICIPANT_FULLY_TAKING_PART(PARTICIPANT_ID_TO_INT())
			OR IS_DELIVERER_MY_GOON()
				DO_BIG_MESSAGE(3)
			ELSE
				IF bDisplayedEndMessage = FALSE
					// do ticker
					SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
					TickerEventData.TickerEvent = TICKER_EVENT_DD_PACKAGE_DELIVERED
					TickerEventData.playerID = serverBD.piCarrier
					BROADCAST_TICKER_EVENT(TickerEventData, SPECIFIC_PLAYER(PLAYER_ID()))
					bDisplayedEndMessage = TRUE
				ENDIF
			ENDIF
			
			IF HAVE_MET_REWARD_CONDITIONS()
				PROCESS_CLIENT_END_CASH_RP_REWARD()
				PRINTLN("[AM DEAD DROP] - Remote player delivered package")
			ENDIF
		ENDIF
	ENDIF
	
	// Time expired
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_MissionEndTimerHit)
	AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_PackageDelivered)
		// If participating, do big message
		IF IS_PARTICIPANT_FULLY_TAKING_PART(PARTICIPANT_ID_TO_INT())
			DO_BIG_MESSAGE(4)
		ELSE
			IF bDisplayedEndMessage = FALSE
			AND IS_IT_SAFE_TO_DISPLAY_FM_EVENT_UI()
				// do ticker
				SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
				TickerEventData.TickerEvent = TICKER_EVENT_DD_TIME_HAS_EXPIRED
				TickerEventData.playerID = PLAYER_ID()
				BROADCAST_TICKER_EVENT(TickerEventData, SPECIFIC_PLAYER(PLAYER_ID()))
				bDisplayedEndMessage = TRUE
			ENDIF
		ENDIF
		
		IF HAVE_MET_REWARD_CONDITIONS()
			PROCESS_CLIENT_END_CASH_RP_REWARD()
			PRINTLN("[AM DEAD DROP] - Delivery time has run out")
		ENDIF
	ENDIF

ENDPROC

PROC TRACK_TIME_PACKAGE_HELD()
	IF HAS_NET_TIMER_STARTED(TimeHeldTimer)
		IF IS_BIT_SET(playerbd[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_CollectedPackage)
			iTimeHeld = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(TimeHeldTimer)
			CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - TRACK_TIME_PACKAGE_HELD, biP_CollectedPackage, iTimeHeld = ", iTimeHeld)
		ELSE
			CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - TRACK_TIME_PACKAGE_HELD, iTimeHeld = ", iTimeHeld)
			iLocalTimeHolding += iTimeHeld
			iTimeHeld = 0
			RESET_NET_TIMER(TimeHeldTimer)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_PASSIVE_MODE_AND_UI_CHECKS()

	IF SHOULD_UI_BE_HIDDEN()
		IF Is_There_Any_Current_Objective_Text_From_This_Script()
			Clear_Any_Objective_Text_From_This_Script()
		ENDIF
		// Player cannot collect portable pickups in passive
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
			PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.niPackage), TRUE, TRUE)
		ENDIF
		EXIT
	ENDIF

	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AwaitingFirstCollection)
			MAINTAIN_FREEMODE_AMBIENT_EVENT_PASSIVE_MODE_WARNING_FLAG(GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.niPackage)), TO_FLOAT(g_sMPTunables.iFREEMODE_EVENT_UI_ENTRY_DISTANCE))
		ENDIF
		
		IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID())
			IF Is_There_Any_Current_Objective_Text_From_This_Script()
				Clear_Any_Objective_Text_From_This_Script()
			ENDIF
			
			IF Is_There_Any_Current_Objective_Text_From_This_Script()
				Clear_Any_Objective_Text_From_This_Script()
			ENDIF
		
			// Player cannot collect portable pickups in passive
			PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.niPackage), TRUE, TRUE)
			
			// If the package is collected but I dont have it
			IF IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iFmAmbientEventBitSet, ciSET_PLAYER_IS_RESTRICTED_WITH_PASSIVE)
				DO_HELP_TEXT(7)
			ENDIF
			
			// DO help text saying they cant collect it
			IF IS_MP_PASSIVE_MODE_ENABLED()
				IF IS_BIT_SET(serverBD.iServerBitSet, biS_PackageDropped)
				OR IS_BIT_SET(serverBD.iServerBitSet, biS_AwaitingFirstCollection)
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), NET_TO_OBJ(serverBD.niPackage)) < 10
						DO_HELP_TEXT(0)
					ENDIF
				ENDIF
			ENDIF
			
			// Move them off mission
			IF playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_KILL
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_OFF_MISSION
				PRINTLN("[AM DEAD DROP] - MAINTAIN_PASSIVE_MODE_CHECKS - setting player stage to OFF_MISSION as they are in passive")
			ENDIF
		ELSE
			IF IS_PLAYER_SCTV(PLAYER_ID())
			OR IS_PED_IN_ANY_SUB(PLAYER_PED_ID())
				PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.niPackage), TRUE, TRUE)
			ELSE
				// Player canot collect portable pickups while spectating
				PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.niPackage), FALSE, TRUE)
			ENDIF
			
			// Disable passive mode option when you enter the vehicle
			IF !FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_FM_EVENT(PLAYER_ID())
				IF playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_OFF_MISSION
					IF NOT SHOULD_FM_EVENT_DROP_TO_IDLE_STATE()
						IF SHOULD_PLAYER_MOVE_TO_CONTEND_STAGE()
							playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CONTEND
							PRINTLN("[AM DEAD DROP] - MAINTAIN_PASSIVE_MODE_CHECKS - Setting player to CONTEND stage as passive mode is no longer enabled")
						ELIF SHOULD_PLAYER_MOVE_TO_IDLE_STAGE()
							playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_IDLE
							PRINTLN("[AM DEAD DROP] - MAINTAIN_PASSIVE_MODE_CHECKS - Setting player to IDLE stage as passive mode is not enabled")
						ELIF SHOULD_PLAYER_MOVE_TO_DELIVER_STAGE()
							playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_DELIVER
							PRINTLN("[AM DEAD DROP] - MAINTAIN_PASSIVE_MODE_CHECKS - Setting player to DELIVER stage as passive mode is not enabled")
						ELSE
							playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_KILL
							PRINTLN("[AM DEAD DROP] - MAINTAIN_PASSIVE_MODE_CHECKS - Setting player to KILL as passive mode is no longer enabled")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_MOVE_OFF_MISSION_CHECKS()
	IF SHOULD_FM_EVENT_DROP_TO_IDLE_STATE()
		IF playerBD[PARTICIPANT_ID_TO_INT()].eStage != eAD_OFF_MISSION
			
			// Remove the package from the player before moving them off mission.
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
				IF IS_ENTITY_ATTACHED_TO_ENTITY(NET_TO_OBJ(serverBD.niPackage), PLAYER_PED_ID())
					IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_CollectedPackage)
						CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_CollectedPackage)
					ENDIF
					PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.niPackage), TRUE, TRUE)
					DETACH_PORTABLE_PICKUP_FROM_PED(NET_TO_OBJ(serverBD.niPackage))
				ENDIF
			ENDIF
		
			// Remove blips here as we want these to be refreshed in the off mission state
			REMOVE_BLIPS()
		
			playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_OFF_MISSION
			PRINTLN("[AM DEAD DROP] -  SHOULD_FM_EVENT_DROP_TO_IDLE_STATE - PLAYER STAGE = eAD_OFF_MISSION    <----------     ")
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_PACKAGE_CARRIER_CHECKS()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)	
		IF IS_ENTITY_ATTACHED_TO_ANY_PED(NET_TO_OBJ(serverBD.niPackage))			
			IF IS_ENTITY_ATTACHED_TO_ENTITY(NET_TO_OBJ(serverBD.niPackage), PLAYER_PED_ID())
				IF NOT HAS_NET_TIMER_STARTED(ParticipationTimer)
					START_NET_TIMER(ParticipationTimer)
				ENDIF
				IF NOT FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT(PLAYER_ID())
					IF NOT FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT(PLAYER_ID())
						COMMON_FREEMODE_AMBIENT_EVENTS_SET_UP(FMMC_TYPE_DEAD_DROP, 0, 0.25)
					ENDIF
					FM_EVENT_SET_PLAYER_IS_CRITICAL_TO_FM_EVENT(TRUE)
				ENDIF
				IF NOT IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
					SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY(TRUE)
					PRINTLN("[AM DEAD DROP] - SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY = TRUE, I have a mission critical entity")
				ENDIF
				IF NOT bBlipsBlocked
					BLOCK_BLIPS(TRUE)
				ENDIF
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_CollectedPackage)
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_CollectedPackage)
					PRINTLN("[AM DEAD DROP] - MAINTAIN_PACKAGE_CARRIER_CHECKS - Failsafe check, i have collected the package!")
				ENDIF
				
				IF IS_PED_IN_ANY_SUB(PLAYER_PED_ID())
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niPackage)
						PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.niPackage), TRUE, TRUE)
						DETACH_ENTITY(NET_TO_OBJ(serverBD.niPackage))
						DO_HELP_TEXT(9)
						PRINTLN("[AM DEAD DROP] - Player is in a sub with the package, detaching package")
					ENDIF
				ENDIF
				
				// Move package outside if you enter the strip club
				IF IS_ENTITY_IN_STRIPCLUB(PLAYER_PED_ID())
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niPackage)
						DETACH_ENTITY(NET_TO_OBJ(serverBD.niPackage))
						SET_ENTITY_COORDS_NO_OFFSET(NET_TO_OBJ(serverBD.niPackage), vStripClubCoords)
						PRINTLN("[AM DEAD DROP] -  MAINTAIN_PACKAGE_CARRIER_CHECKS - IS_ENTITY_IN_STRIPCLUB - TRUE - detaching package and moving to ", vStripClubCoords)
					ENDIF
				ENDIF
			ELSE
				// Move package outside if you enter the strip club
				IF IS_ENTITY_IN_STRIPCLUB(NET_TO_OBJ(serverBD.niPackage))
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niPackage)
						SET_ENTITY_COORDS(NET_TO_OBJ(serverBD.niPackage), vStripClubCoords)
						PRINTLN("[AM DEAD DROP] -  MAINTAIN_PACKAGE_CARRIER_CHECKS - IS_PLAYER_IN_STRIP_CLUB - TRUE - detaching package and moving to ", vStripClubCoords)
					ENDIF
				ENDIF
				IF FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT(PLAYER_ID())
					FM_EVENT_SET_PLAYER_IS_CRITICAL_TO_FM_EVENT(FALSE)
				ENDIF
				IF IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
					SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY(FALSE)
					PRINTLN("[AM DEAD DROP] - SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY = FALSE, I no longer have a mission critical entity")
				ENDIF
				IF bBlipsBlocked
					BLOCK_BLIPS(FALSE)
				ENDIF
				IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_CollectedPackage)
					CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_CollectedPackage)
					PRINTLN("[AM DEAD DROP] - MAINTAIN_PACKAGE_CARRIER_CHECKS - Failsafe check, someone has the package, but not me!")
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_CollectedPackage)
				CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_CollectedPackage)
				PRINTLN("[AM DEAD DROP] - MAINTAIN_PACKAGE_CARRIER_CHECKS - Failsafe check, no one has the package!")
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

PROC MAINTAIN_QUALIFICATION_CHECKS()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WithinQualifyDistance)
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AwaitingFirstCollection)
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.niPackage)), FALSE) < 100
					IF NOT HAS_NET_TIMER_STARTED(ParticipationTimer)
						START_NET_TIMER(ParticipationTimer)
					ENDIF
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WithinQualifyDistance)
					PRINTLN("[AM DEAD DROP] - MAINTAIN_QUALIFICATION_CHECKS - Player has gotten within 100m of the package, player now qualifies for min participation reward.")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_DISPLAY_DD_LBD()

	IF playerBD[PARTICIPANT_ID_TO_INT()].eStage != eAD_END
	AND playerBD[PARTICIPANT_ID_TO_INT()].eStage != eAD_CLEANUP
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_SAFE_LOCATION_CHECKS()
	IF SHOULD_FM_EVENT_NET_ENTITY_WARP_TO_SAFE_LOCATION()
		HANDLE_WARP_FM_EVENT_NET_ENTITY_TO_SAFE_LOCATION(serverBD.niPackage)
	ENDIF
ENDPROC

PROC MAINTAIN_GANG_MEMBERSHIP()
	IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(TRUE)
		IF NOT IS_BIT_SET(iBoolsBitSet, iLOCALBITSET0_MEMBER_OF_A_GANG)		
			bUpdateObjectiveGangMembershipChange = TRUE
			SET_BIT(iBoolsBitSet, iLOCALBITSET0_MEMBER_OF_A_GANG)
			PRINTLN("[AM DEAD DROP] - MAINTAIN_GANG_MEMBERSHIP - I have joined a gang, setting iLOCALBITSET0_MEMBER_OF_A_GANG")
		ENDIF
	ELSE
		IF IS_BIT_SET(iBoolsBitSet, iLOCALBITSET0_MEMBER_OF_A_GANG)
			bUpdateObjectiveGangMembershipChange = TRUE
			CLEAR_BIT(iBoolsBitSet, iLOCALBITSET0_MEMBER_OF_A_GANG)
			PRINTLN("[AM DEAD DROP] - MAINTAIN_GANG_MEMBERSHIP - I have left a gang, clearing iLOCALBITSET0_MEMBER_OF_A_GANG")
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Process the JOYRIDER stages for the Client
PROC PROCESS_DEAD_DROP_CLIENT()
	
	PROCESS_DAMAGE_EVENTS_AM_DEAD_DROP()
	MAINTAIN_LOCAL_PLAYER_PARTICIPATION()
	MAINTAIN_MOVE_OFF_MISSION_CHECKS()
	MAINTAIN_DEAD_DROP_EVENT_TIMER()
	MAINTAIN_DPAD_DOWN_LEADERBOARD()
	MAINTAIN_PASSIVE_MODE_AND_UI_CHECKS()
	MAINTAIN_BLIPS_AND_OBJECTIVE_TEXT()
	MAINTAIN_SAFE_LOCATION_CHECKS()
	MAINTAIN_QUALIFICATION_CHECKS()
	TRACK_TIME_PACKAGE_HELD()
	HANDLE_PACKAGE_IN_SHOP()
	HANDLE_PACKAGE_ON_YACHT()
	MAINTAIN_GANG_MEMBERSHIP()
	
	IF DRAW_CHALLENGE_DPAD_FOR_DEV_SPECTATOR()
		MAINTAIN_DPAD_DOWN_LEADERBOARD()
	ENDIF
		
	IF FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT(PLAYER_ID())
		//HANDLE_MAP_EXITING_PLAYERS()
		IF IS_PED_NEAR_STRIPCLUB_ENTRYWAY(PLAYER_PED_ID())
			DO_HELP_TEXT(8)
		ENDIF
	ENDIF
	
	SWITCH playerBD[PARTICIPANT_ID_TO_INT()].eStage		
		CASE eAD_IDLE			
			CONTROL_CLIENT_PACKAGE_COLLECTION()
			FM_EVENT_SET_PLAYER_WAITING_FOR_FM_EVENT_TO_START(TRUE)
			
			//Check if we should become Deliverer or Destroyer
			IF HAS_PACKAGE_BEEN_COLLECTED_TO_START_EVENT()
				//common event proccess pre game
				IF NOT FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT(PLAYER_ID())
					COMMON_FREEMODE_AMBIENT_EVENTS_SET_UP(FMMC_TYPE_DEAD_DROP, 0, 0.25)
				ENDIF
				FM_EVENT_SET_PLAYER_WAITING_FOR_FM_EVENT_TO_START(FALSE)
				IF telemetryStruct.m_timeTakenForObjective = 0
					telemetryStruct.m_timeTakenForObjective = GET_CLOUD_TIME_AS_INT() 
				ENDIF
				iBagChangedHands++
				START_AUDIO_SCENE("MP_Deathmatch_Type_Challenge_Scene")
				IF serverBD.piCarrier = PLAYER_ID()
					SET_FREEMODE_FLOW_COMMS_DISABLED(TRUE)
					playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_DELIVER
					PRINTLN("[AM DEAD DROP] -  PLAYER STAGE = eAD_DELIVER    <----------     ")
				ELSE
					PLAY_SOUND_FRONTEND(-1, "Object_Collect_Remote", "GTAO_FM_Events_Soundset", FALSE)
					IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID())
						playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_OFF_MISSION
						PRINTLN("[AM DEAD DROP] -  PLAYER STAGE = eAD_OFF_MISSION (Passive enabled)    <----------     ")
					ELSE
						playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_KILL
						PRINTLN("[AM DEAD DROP] -  PLAYER STAGE = eAD_KILL    <----------     ")
					ENDIF
				ENDIF
			ELSE
				DO_HELP_TEXT(1)
				MAINTAIN_PACKAGE_MARKER()
			ENDIF
			IF serverBD.eStage = eAD_END
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_END
				PRINTLN("[AM DEAD DROP] -  PLAYER STAGE = eAD_END    <----------     ")
			ENDIF
			IF serverBD.eStage = eAD_CLEANUP
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CLEANUP
				PRINTLN("[AM DEAD DROP] -  PLAYER STAGE = eAD_CLEANUP    <----------     ")
			ENDIF
		BREAK
		
		CASE eAD_CONTEND
			MAINTAIN_PACKAGE_MARKER()
			CONTROL_CLIENT_PACKAGE_COLLECTION()
			MAINTAIN_SPAWN_LOCATIONS()
			HANDLE_OBJECTIVE_IN_GARAGE(serverBD.niPackage, iGarageCheckTimer, iGarageSyncDelayTimer, FALSE)
			IF IS_BIT_SET(iBoolsBitSet2, biBigM0Done)
				// We potentially will collect the package again so should be able to trigger shard 0. (2375266)
				CLEAR_BIT(iBoolsBitSet2, biBigM0Done)
			ENDIF
			//Check if we should become Deliverer or Destroyer
			IF HAS_PACKAGE_BEEN_COLLECTED_FOLLOWING_DROP()
				iBagChangedHands++
				IF serverBD.piCarrier = PLAYER_ID()
					DO_BIG_MESSAGE(0)
					playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_DELIVER
					PRINTLN("[AM DEAD DROP] -  PLAYER STAGE = eAD_DELIVER    <----------     ")
				ELSE
					IF IS_PARTICIPANT_FULLY_TAKING_PART(PARTICIPANT_ID_TO_INT())
						PLAY_SOUND_FRONTEND(-1, "Object_Collect_Remote", "GTAO_FM_Events_Soundset", FALSE)
					ENDIF
					playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_KILL
					PRINTLN("[AM DEAD DROP] -  PLAYER STAGE = eAD_KILL    <----------     ")
				ENDIF
			ENDIF
			IF serverBD.eStage = eAD_END
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_END
				PRINTLN("[AM DEAD DROP] -  PLAYER STAGE = eAD_END    <----------     ")
			ENDIF
			IF serverBD.eStage = eAD_CLEANUP
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CLEANUP
				PRINTLN("[AM DEAD DROP] -  PLAYER STAGE = eAD_CLEANUP    <----------     ")
			ENDIF
		BREAK
		
		CASE eAD_DELIVER
			CONTROL_CLIENT_PACKAGE_COLLECTION()
			MAINTAIN_SPAWN_LOCATIONS()
			HANDLE_OBJECTIVE_IN_GARAGE(serverBD.niPackage, iGarageCheckTimer, iGarageSyncDelayTimer, FALSE)
			DO_BIG_MESSAGE(0)			
			DO_HELP_TEXT(2)
			
			IF SHOULD_PLAYER_MOVE_TO_KILL_STAGE()
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_KILL
				PRINTLN("[AM DEAD DROP] -  PLAYER STAGE = eAD_KILL (from Deliver)    <----------     ")
			ENDIF
			IF SHOULD_PLAYER_MOVE_TO_CONTEND_STAGE()
			
				// We potentially will collect the package again so should be able to trigger shard 0. (2375266)
				CLEAR_BIT(iBoolsBitSet2, biBigM0Done)
			
				// Package has been dropped, clear the objective text
				IF Is_There_Any_Current_Objective_Text_From_This_Script()
					Clear_Any_Objective_Text_From_This_Script()
				ENDIF
			
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CONTEND
				PRINTLN("[AM DEAD DROP] -  PLAYER STAGE = eAD_CONTEND (from Deliver)    <----------     ")
			ENDIF			
			IF serverBD.eStage = eAD_END
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_END
				PRINTLN("[AM DEAD DROP] -  PLAYER STAGE = eAD_END    <----------     ")
			ENDIF
			IF serverBD.eStage = eAD_CLEANUP
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CLEANUP
				PRINTLN("[AM DEAD DROP] -  PLAYER STAGE = eAD_CLEANUP    <----------     ")
			ENDIF
		BREAK
		
		CASE eAD_KILL
			IF AM_I_SPECTATING_THE_CARRIER()
				IF IS_A_SPECTATOR_CAM_RUNNING()
				AND NOT IS_SPECTATOR_HUD_HIDDEN()
					DO_BIG_MESSAGE(0)
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HaveHadPackage)
					DO_BIG_MESSAGE(1)
					DO_HELP_TEXT(4)
				ENDIF
			ENDIF
			CONTROL_CLIENT_PACKAGE_COLLECTION()
			MAINTAIN_TARGET_MARKER()
			IF SHOULD_PLAYER_MOVE_TO_DELIVER_STAGE()
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_DELIVER
				PRINTLN("[AM DEAD DROP] -  PLAYER STAGE = eAD_DELIVER (from Kill)    <----------     ")
			ENDIF
			IF SHOULD_PLAYER_MOVE_TO_CONTEND_STAGE()
			
				// We potentially will collect the package again so should be able to trigger shard 0. (2375266)
				CLEAR_BIT(iBoolsBitSet2, biBigM0Done)
				
				// Package has been dropped, clear the objective text
				IF Is_There_Any_Current_Objective_Text_From_This_Script()
					Clear_Any_Objective_Text_From_This_Script()
				ENDIF
				
				IF IS_PARTICIPANT_FULLY_TAKING_PART(PARTICIPANT_ID_TO_INT())
					PLAY_SOUND_FRONTEND(-1, "Object_Dropped_Remote", "GTAO_FM_Events_Soundset", FALSE)
				ENDIF
			
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CONTEND
				PRINTLN("[AM DEAD DROP] -  PLAYER STAGE = eAD_CONTEND (from Kill)    <----------     ")
			ENDIF
			IF serverBD.eStage = eAD_END
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_END
				PRINTLN("[AM DEAD DROP] -  PLAYER STAGE = eAD_END    <----------     ")
			ENDIF
			IF serverBD.eStage = eAD_CLEANUP
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CLEANUP
				PRINTLN("[AM DEAD DROP] -  PLAYER STAGE = eAD_CLEANUP")
			ENDIF
		BREAK
		
		CASE eAD_OFF_MISSION
			IF Is_There_Any_Current_Objective_Text_From_This_Script()
				Clear_Any_Objective_Text_From_This_Script()
			ENDIF
			IF NOT FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID())
				IF NOT SHOULD_FM_EVENT_DROP_TO_IDLE_STATE()
					IF SHOULD_PLAYER_MOVE_TO_IDLE_STAGE()
						IF NOT IS_PLAYER_SCTV(PLAYER_ID())
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
								PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.niPackage), FALSE, TRUE)
							ENDIF
						ENDIF
						playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_IDLE
						PRINTLN("[AM DEAD DROP] -  SHOULD_FM_EVENT_DROP_TO_IDLE_STATE(FALSE) - PLAYER STAGE = eAD_CONTEND    <----------     ")
					ENDIF
					IF SHOULD_PLAYER_MOVE_TO_CONTEND_STAGE()
						IF NOT IS_PLAYER_SCTV(PLAYER_ID())
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
								PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.niPackage), FALSE, TRUE)
							ENDIF
						ENDIF
						playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CONTEND
						PRINTLN("[AM DEAD DROP] -  SHOULD_FM_EVENT_DROP_TO_IDLE_STATE(FALSE) - PLAYER STAGE = eAD_CONTEND    <----------     ")
					ENDIF
					IF SHOULD_PLAYER_MOVE_TO_DELIVER_STAGE()
						IF NOT IS_PLAYER_SCTV(PLAYER_ID())
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
								PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.niPackage), FALSE, TRUE)
							ENDIF
						ENDIF
						playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_DELIVER
						PRINTLN("[AM DEAD DROP] -  SHOULD_FM_EVENT_DROP_TO_IDLE_STATE(FALSE) - PLAYER STAGE = eAD_DELIVER    <----------     ")
					ENDIF
					IF SHOULD_PLAYER_MOVE_TO_KILL_STAGE()
						IF NOT IS_PLAYER_SCTV(PLAYER_ID())
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
								PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.niPackage), FALSE, TRUE)
							ENDIF
						ENDIF
						playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_KILL
						PRINTLN("[AM DEAD DROP] -  SHOULD_FM_EVENT_DROP_TO_IDLE_STATE(FALSE) - PLAYER STAGE = eAD_KILL    <----------     ")
					ENDIF
				ENDIF
			ENDIF
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_PackageCollected)
			AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_PackageDropped)
				MAINTAIN_TARGET_MARKER()
			ELSE
				MAINTAIN_PACKAGE_MARKER()
			ENDIF
			IF serverBD.eStage = eAD_END
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_END
				PRINTLN("[AM DEAD DROP] -  PLAYER STAGE = eAD_END    <----------     ")
			ENDIF
			IF serverBD.eStage = eAD_CLEANUP
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CLEANUP
				PRINTLN("[AM DEAD DROP] -  PLAYER STAGE = eAD_CLEANUP")
			ENDIF
		BREAK
		
		CASE eAD_END
			// Only do the UI if the mode timed out with no one taking part
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_MissionTimedOut)
				IF MAINTAIN_FM_EVENTS_END_UI(sEndUiVars, FALSE)
					playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CLEANUP
					PRINTLN("[AM DEAD DROP] -  PLAYER STAGE = eAD_CLEANUP")
				ENDIF
			ENDIF
			HANDLE_EVENT_END_PACKAGE_LOGIC()
			KILL_AMBIENT_EVENT_COUNTDOWN_AUDIO()
			IF FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT(PLAYER_ID())
				FM_EVENT_SET_PLAYER_IS_CRITICAL_TO_FM_EVENT(FALSE)
			ENDIF
			PROCESS_EVENT_END_REWARDS_AND_MESSAGES()			
		BREAK
				
		CASE eAD_CLEANUP
			PRINTLN("[AM DEAD DROP] -  SCRIPT CLEANUP B")
			SCRIPT_CLEANUP()
		BREAK
	ENDSWITCH
	
	// FAILSAFE CHECKS
	MAINTAIN_PACKAGE_CARRIER_CHECKS()
ENDPROC

PROC MAINTAIN_EVENT_TIMER()
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_StartTimer)
		IF NOT HAS_NET_TIMER_STARTED(serverBD.DeliverPackageTimer)
			START_NET_TIMER(serverBD.DeliverPackageTimer)
		ELSE
			IF HAS_NET_TIMER_EXPIRED(serverBD.DeliverPackageTimer, GET_MISSION_TIME())
				PRINTLN("[AM DEAD DROP] - iTimeRemaining <= 0, biS_MissionEndTimerHit SET")
				SET_BIT(serverBD.iServerBitSet, biS_MissionEndTimerHit)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_MOVE_TO_EVENT_END()

	IF IS_BIT_SET(serverBD.iServerBitSet, biS_PackageDelivered)
		PRINTLN("[AM DEAD DROP] -  SHOULD_MOVE_TO_EVENT_END - biS_PackageDelivered")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_MissionEndTimerHit)
		PRINTLN("[AM DEAD DROP] -  SHOULD_MOVE_TO_EVENT_END - biS_MissionEndTimerHit")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_MissionTimedOut)
		PRINTLN("[AM DEAD DROP] -  SHOULD_MOVE_TO_EVENT_END - biS_MissionTimedOut")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

//PURPOSE: Process the JOYRIDER stages for the Server
PROC PROCESS_DEAD_DROP_SERVER()
	SWITCH serverBD.eStage		
		CASE eAD_IDLE
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_AwaitingFirstCollection)
				
				IF NOT HAS_NET_TIMER_STARTED(serverBD.MissionStartTimeout)
					START_NET_TIMER(serverBD.MissionStartTimeout)
					PRINTLN("[AM DEAD DROP] MissionTimeoutTimer has started ")
				ELSE
					IF HAS_PACKAGE_BEEN_COLLECTED_TO_START_EVENT()
						CLEAR_BIT(serverBD.iServerBitSet, biS_AwaitingFirstCollection)
						RESET_NET_TIMER(serverBD.MissionStartTimeout)
						PRINTLN("[AM DEAD DROP] MissionTimeoutTimer has been reset as HAVE_PLAYERS_ENTERED_TO_START_EVENT is TRUE")
					ELSE
						IF HAS_NET_TIMER_EXPIRED(serverbd.MissionStartTimeout, g_sMPTunables.iDead_Drop_Event_Expiry_Time)
							SET_BIT(serverBD.iServerBitSet, biS_MissionTimedOut)
							SET_BIT(serverBD.iServerBitSet, biS_EventEnded)
							PRINTLN("[AM DEAD DROP] - MissionTimeoutTimer has expired - biS_MissionTimedOut SET")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			MAINTAIN_EVENT_TIMER()

			IF SHOULD_MOVE_TO_EVENT_END()
				PRINTLN("[AM DEAD DROP] -  PROCESS_DEAD_DROP_SERVER - SHOULD_MOVE_TO_EVENT_END(TRUE) - serverBD.eStage = eAD_END")
				SET_BIT(serverBD.iServerBitSet, biS_EventEnded)
				serverBD.eStage = eAD_END
			ENDIF
		BREAK
		
		CASE eAD_END
			IF NOT HAS_NET_TIMER_STARTED(serverBD.MissionEndStageTimer)
				START_NET_TIMER(serverBD.MissionEndStageTimer)
			ELSE
				IF HAS_NET_TIMER_EXPIRED(serverBD.MissionEndStageTimer, ciFM_EVENTS_END_UI_SHARD_TIME)
					PRINTLN("[AM DEAD DROP] -  PROCESS_DEAD_DROP_SERVER - SHOULD_MOVE_TO_EVENT_END(TRUE) - serverBD.eStage = eAD_END")
					SET_BIT(serverBD.iServerBitSet, biS_MissionShouldCleanup)
				ENDIF
			ENDIF
		BREAK
				
		CASE eAD_CLEANUP			
		BREAK
	ENDSWITCH
ENDPROC

PROC MAINTAIN_ACTIVE_PLAYER_CHECK()

	IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iStaggeredCheck))
		PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iStaggeredCheck))
			
		IF NETWORK_IS_PLAYER_ACTIVE(PlayerId)
			FM_EVENT_MAINTAIN_ACTIVE_PLAYER_CHECK(PlayerId)
		ENDIF
	ENDIF
	
	iStaggeredCheck++
	
	IF iStaggeredCheck >= NUM_NETWORK_PLAYERS
		iStaggeredCheck = 0
	ENDIF

ENDPROC


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                      //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT(MP_MISSION_DATA missionScriptArgs)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
		#IF IS_DEBUG_BUILD	
			PRINTLN("MP: Starting mission ")
		#ENDIF
		
		// Vehry out all the initial game starting duties. 
		PROCESS_PRE_GAME(missionScriptArgs)	
		
		// Setup widgets. Make sure this is done after PROCESS_PRE_GAME()
		#IF IS_DEBUG_BUILD
			CREATE_WIDGETS()
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD NET_LOG("DEAD DROP -  NETWORK_IS_GAME_IN_PROGRESS = FALSE")	#ENDIF
		PRINTLN("[AM DEAD DROP] -  MISSION END - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP P     <----------     ")
		SCRIPT_CLEANUP()
	ENDIF
	
	// Main loop
	WHILE TRUE

		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
		
				
		// If we have a match end event, bail
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			//PROCESS_CLIENT_END_CASH_RP_REWARD()
			#IF IS_DEBUG_BUILD NET_LOG("SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")	#ENDIF
			PRINTLN("[AM DEAD DROP] -  MISSION END - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE - SCRIPT CLEANUP B     <----------     ")
			SCRIPT_CLEANUP()
		ENDIF
				
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_PackageDelivered)
			IF SHOULD_BAIL_FM_EVENT_DUE_TO_LOW_SESSION_NUMBERS()
				PROCESS_CLIENT_END_CASH_RP_REWARD()
				#IF IS_DEBUG_BUILD NET_LOG("SHOULD_BAIL_FM_EVENT_DUE_TO_LOW_SESSION_NUMBERS")	#ENDIF
				PRINTLN("[AM DEAD DROP] -  MISSION END - SHOULD_BAIL_FM_EVENT_DUE_TO_LOW_SESSION_NUMBERS - SCRIPT CLEANUP B     <----------     ")
				SCRIPT_CLEANUP()
			ENDIF
		ENDIF
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niPackage)
			MAINTAIN_FREEMODE_AMBIENT_EVENT_DISTANT_CHECKS(FMMC_TYPE_DEAD_DROP, GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.niPackage)), bOnOff, TO_FLOAT(g_sMPTunables.iFREEMODE_EVENT_UI_ENTRY_DISTANCE), DEFAULT, bSetSpawnLocations)
		ENDIF
		
		IF IS_KILL_CURRENT_FREEMODE_EVENT_GLOBAL_SET()
			PRINTLN("[AM DEAD DROP] - IS_KILL_CURRENT_FREEMODE_EVENT_GLOBAL_SET - TRUE - calling script cleanup to kill Dead Drop.")
			SCRIPT_CLEANUP()
		ENDIF
		
		MAINTAIN_ACTIVE_PLAYER_CHECK()
		
		//Call the common FM Events every frame callse
		FM_EVENT_COMMON_EVERY_FRAME_CALLS()
		
//		//Check if player is in a tutorial session and end
//		IF NETWORK_IS_IN_TUTORIAL_SESSION()
//		AND NOT IS_PLAYER_IN_CORONA()
//			#IF IS_DEBUG_BUILD NET_LOG("PLAYER IN TUT SESSION")	#ENDIF
//			PRINTLN("[AM DEAD DROP] -  MISSION END - PLAYER IN TUTORIAL SESSION - SCRIPT CLEANUP C     <----------     ")
//			SCRIPT_CLEANUP()
//		ENDIF
		
		// Deal with the debug
		#IF IS_DEBUG_BUILD		
			UPDATE_WIDGETS()
			
			IF MPGlobalsAmbience.bEndCurrentFreemodeEvent
				IF bDebugTimeChange = FALSE
					IF HAS_NET_TIMER_STARTED(serverBD.DeliverPackageTimer)
						REINIT_NET_TIMER(serverBD.DeliverPackageTimer)
					ENDIF
					PRINTLN("[AM DEAD DROP] -  MPGlobalsAmbience.bEndCurrentFreemodeEvent TRUE, serverBD.DeliverPackageTimer reinitialised.")
					bDebugTimeChange = TRUE
				ENDIF
			ENDIF
		#ENDIF		
	
		// If we need to leave the mission, make sure we just straight to the end stage and terminate the script - DON'T NEED FOR AMBIENT MISSION
		/*IF SHOULD_PLAYER_LEAVE_MP_MISSION() 
			#IF IS_DEBUG_BUILD
			PRINTLN( GET_MP_MISSION_NAME(thisMission), " Leave mission = TRUE \n")
			#ENDIF
			playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_LEAVE
			#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_LEAVE")	#ENDIF
		ENDIF*/
		
		// -----------------------------------
		// Process your game logic.....
		SWITCH GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT())
			
			// Wait untill the server gives the all go before moving on
			CASE GAME_STATE_INI
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					IF IS_BIT_SET(serverBD.iServerBitSet, biS_AwaitingFirstCollection)
						IF NOT SHOULD_UI_BE_HIDDEN()
							ADD_PACKAGE_BLIP()
							FLASH_MINIMAP_DISPLAY() // 2369127
							IF DOES_BLIP_EXIST(PackageBlip)
								SET_BLIP_FLASHES(PackageBlip, TRUE)
	                    		SET_BLIP_FLASH_TIMER(PackageBlip, 7000)
							ENDIF
						ENDIF
					ENDIF
					
					FM_EVENT_BLOCK_SPECIFIC_GANG_ATTACKS(FMMC_TYPE_DEAD_DROP, serverBD.iRoute, serverBD.iVariation)
					
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING
					PRINTLN("[AM DEAD DROP] -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING    <----------     ")
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_RUNNING")	#ENDIF
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					PRINTLN("[AM DEAD DROP] -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 1    <----------     ")
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY")	#ENDIF
				ENDIF
			BREAK
			
			// Main gameplay state.
			CASE GAME_STATE_RUNNING
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					PROCESS_DEAD_DROP_CLIENT()
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					PRINTLN("[AM DEAD DROP] -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 2   <----------     ")
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY")	#ENDIF
				ENDIF
			BREAK
			
			CASE GAME_STATE_TERMINATE_DELAY 
				SET_MISSION_FINISHED(serverBD.MissionTerminateDelayTimer)
		      	IF IS_MISSION_READY_TO_CLEANUP(serverBD.MissionTerminateDelayTimer)
		            playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					PRINTLN("[AM DEAD DROP] -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 3    <----------     ")
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 3")	#ENDIF
		      	ENDIF
			BREAK
			
			// The game stage the local player is placed when we want him to leave a mission
			CASE GAME_STATE_LEAVE
				//LEAVE_MY_MP_MISSION(thisMission)
				playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
				PRINTLN("[AM DEAD DROP] -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 2    <----------     ")
				#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 2")	#ENDIF
			FALLTHRU
			
			//Cleans up then terminates the mission
			CASE GAME_STATE_END
				PRINTLN("[AM DEAD DROP] -  SCRIPT CLEANUP A     <----------     ")
				SCRIPT_CLEANUP()
			BREAK

		ENDSWITCH
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			
			SWITCH GET_SERVER_MISSION_STATE()
				
				// For CnC group missions, we may want to wait for all players to have launched the script
				CASE GAME_STATE_INI
					IF CREATE_DEAD_DROP_PACKAGE()
						serverBD.iServerGameState = GAME_STATE_RUNNING
						PRINTLN("[AM DEAD DROP] -  serverBD.iServerGameState = GAME_STATE_RUNNING    <----------     ")
						#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_RUNNING")	#ENDIF
					ENDIF
				BREAK
				
				// Look for game end conditions
				CASE GAME_STATE_RUNNING
					
					MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
					PROCESS_DEAD_DROP_SERVER()
					
					#IF IS_DEBUG_BUILD
						IF bHostEndMissionNow
						OR MPGlobalsAmbience.bKillActiveEvent
							MPGlobalsAmbience.bKillActiveEvent = FALSE
							serverBD.iServerGameState = GAME_STATE_END
							PRINTLN("[AM DEAD DROP] -  serverBD.iServerGameState = GAME_STATE_END 1    <----------     ")
							#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 1")	#ENDIF
						ENDIF
					#ENDIF	
					
					IF HAVE_MISSION_END_CONDITIONS_BEEN_MET()
						serverBD.iServerGameState = GAME_STATE_END
						PRINTLN("[AM DEAD DROP] -  serverBD.iServerGameState = GAME_STATE_END 4    <----------     ")
						#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 4")	#ENDIF
					ENDIF
					
				BREAK
				
				CASE GAME_STATE_END
				BREAK
				
			ENDSWITCH
			
		ENDIF
		
	ENDWHILE
	
	
ENDSCRIPT
