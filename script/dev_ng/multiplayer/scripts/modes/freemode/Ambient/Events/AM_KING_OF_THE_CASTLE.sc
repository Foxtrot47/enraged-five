


//////////////////////////////////////////////////////////////////////////////////////////////////
//    																							//
//	//////////////////////////////////////////////////////////////////////////////////////////	//
//	//																						//	//
//	// 		Name:      		AM_KING_OF_THE_CASTLE.sc										//	//
//	// 		Description: 	King of the Castle ambient activity controller					//	//
//	// 		Written by: 	William.Kennedy@RockstarNorth.com								//	//
//	// 		Date: 			26/05/2015														//	//
//	//																						//	//
//	//////////////////////////////////////////////////////////////////////////////////////////	//
//																								//
//////////////////////////////////////////////////////////////////////////////////////////////////



USING "globals.sch"
USING "net_mission.sch"
USING "commands_network.sch"
USING "script_player.sch" 
USING "commands_path.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_scoring_common.sch"
USING "net_ambience.sch"
USING "commands_object.sch"
USING "net_xp_animation.sch"
USING "net_wait_zero.sch"
USING "DM_Leaderboard.sch"
USING"net_challenges.sch"

USING "AM_Common_UI.sch"
USING "freemode_events_header.sch"

USING "net_rank_ui.sch"

#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF

USING "net_gang_boss.sch"

CONST_INT NUM_CASTLE_AREAS 											1

CONST_INT MAX_NUM_LOCATES											3

CONST_INT NUM_CASTLE_AREA_VARIATIONS								8

CONST_INT KING_OF_THE_CASTLE_NEARLY_FINISHED_TIME					1000*30

CONST_FLOAT KING_OF_THE_CASTLE_TRIGGER_RADIUS						25.0

CONST_FLOAT KING_OF_THE_CASTLE_HEIGHT								10.0

CONST_FLOAT KING_OF_THE_CASTLE_WANTED_LEVEL_MULTIPLIER_0_DISTANCE	500.0
CONST_FLOAT KING_OF_THE_CASTLE_FULLY_ACTIVE_PARTICIPANT_DISTANCE 	500.0

CONST_INT NUM_LEADERBOARD_PLAYERS									3

CONST_FLOAT FLASH_HEIGHT_ON_BLIP_2D_DISTANCE						50.0
CONST_FLOAT FLASH_HEIGHT_ON_BLIP_Z_DISTANCE							2.5

CONST_INT NUM_CASTLE_AREA_SPAWN_POINTS	60

ENUM eSPAWN_POINT_WEIGHTING
	ESPAWNPOINTWEIGHTING_A = 0,
	ESPAWNPOINTWEIGHTING_B,
	ESPAWNPOINTWEIGHTING_C
ENDENUM

ENUM eGAME_STATE
	eGAMESTATE_INI = 0,
	eGAMESTATE_RUNNING,
	eGAMESTATE_LEAVE,
	eGAMESTATE_DELAY_TERMINATION,
	eGAMESTATE_END
ENDENUM

ENUM eKING_OF_THE_CASTLE_STATE
	eKINGOFTHECASTLESTATE_INITIALISE = 0,
	eKINGOFTHECASTLESTATE_FIGHT_OVER_CASTLE,
	eKINGOFTHECASTLESTATE_TIME_UP,
	eKINGOFTHECASTLESTATE_CLEANUP,
	eKINGOFTHECASTLESTATE_END
ENDENUM

ENUM eSERVER_BITSET_0
	eSERVERBITSET0_ALL_PARTICIPANTS_INITIALISED_LOCATE_DATA = 0,
	eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS,
	eSERVERBITSET0_CASTLE_AREA_HAS_BEEN_POPULATED
ENDENUM

ENUM eCLIENT_BITSET_0
	eCLIENTBITSET0_INITIALISED_LOCATE_DATA = 0,
	eCLIENTBITSET0_COMPLETED_REWARDS,
	eCLIENTBITSET0_I_AM_FULLY_ACTIVE_PARTICIPANT_TRIGGERED_BY_INTERACTION,
	eCLIENTBITSET0_I_AM_FULLY_ACTIVE_PARTICIPANT_TRIGGERED_BY_DISTANCE,
	eCLIENTBITSET0_I_AM_QUALIFYING_PARTICIPANT_FOR_BEING_KING,
	eCLIENTBITSET0_I_AM_QUALIFYING_PARTICIPANT_FOR_ENTERING_CASTLE_AREA,
	eCLIENTBITSET0_I_AM_QUALIFYING_PARTICIPANT_FOR_DAMAGING_KING,
	eCLIENTBITSET0_I_AM_QUALIFYING_PARTICIPANT_FOR_BEING_DAMAGED_BY_KING,
	eCLIENTBITSET0_AMBIENT_EVENT_SHOULD_BE_HIDDEN,
	eCLIENTBITSET0_AMBIENT_EVENT_SHOULD_BE_HIDDEN_NO_PASSIVE_CHECK,
	eCLIENTBITSET0_AMBIENT_EVENT_SHOULD_BE_HIDDEN_NO_SPECTATOR_CHECK,
	eCLIENTBITSET0_AMBIENT_EVENT_SHOULD_BE_HIDDEN_NO_SPECTATOR_NO_PASSIVE_CHECK,
	eCLIENTBITSET0_I_AM_WITHIN_RANGE_FOR_UI
ENDENUM

ENUM eLOCAL_BITSET_0
	eLOCALBITSET0_SET_HEIR_TO_THRONE_AS_KING = 0,
	eLOCALBITSET0_SETUP_INTRO_BIG_MESSAGE,
	eLOCALBITSET0_DISPLAYED_INTRO_BIG_MESSAGE,
	eLOCALBITSET0_SETUP_EOM_BIG_MESSAGE,
	eLOCALBITSET0_DISPLAYED_EOM_BIG_MESSAGE,
	eLOCALBITSET0_GIVEN_EOM_REWARDS,
	eLOCALBITSET0_SHOWN_I_AM_KING_HELP,
	eLOCALBITSET0_SHOWN_OTHER_IS_KING_HELP,
	eLOCALBITSET0_SHOWN_INIT_HELP,
	eLOCALBITSET0_ZEROED_WANTED_MULTIPLIER,
	eLOCALBITSET0_DISPLAYED_CONTESTED_CASTLE_HELP,
	eLOCALBITSET0_DISPLAYED_CONTESTED_CASTLE_HELP_GOON,
	eLOCALBITSET0_FLASHED_MINIMAP_DISPLAY,
	eLOCALBITSET0_FLASHED_KING_OF_THE_CASTLE_BLIP,
	eLOCALBITSET0_SETUP_YOU_ARE_KING_BIG_MESSAGE,
	eLOCALBITSET0_DISPLAYED_YOU_ARE_KING_BIG_MESSAGE,
	eLOCALBITSET0_AUDIO_SCENE_ACTIVE,
	eLOCALBITSET0_UI_END_TIME_ELAPSED,
	eLOCALBITSET0_SETUP_SPAWN_AREAS,
	eLOCALBITSET_LEAVING_FOR_NOT_ENOUGH_PARTICIPANTS,
	eLOCALBITSET_BEEN_KING_ONCE,
	eLOCALBITSET_CLEANED_UP_BIG_MESSAGE_FOR_NOT_KING,
	eLOCALBITSET0_SET_DO_FIRST_PLACE_HELP,
	eLOCALBITSET0_PLAYED_TOP_LEADERBOARD_AUDIO,
	eLOCALBITSET0_LEAVING_FOR_LOW_SESSION_NUMBERS,
	eLOCALBITSET0_UPDATE_TIME_TO_OBJ_TELEMETRY,
	eLOCALBITSET0_CALLED_COMMON_FREEMODE_AMBIENT_EVENTS_SET_UP
ENDENUM

ENUM eCASTLE_AREA_STATE
	eCASTLEAREASTATE_UNENTERED = 0,
	eCASTLEAREASTATE_POPULATED,
	eCASTLEAREASTATE_EMPTY
ENDENUM

STRUCT STRUCT_LEADERBOARD_DATA
	INT iParticipant
	INT iPlayer
	PLAYER_INDEX playerId
	FLOAT fTotalKingPoints
ENDSTRUCT

STRUCT STRUCT_KING_OF_THE_CASTLE_SERVER
	eKING_OF_THE_CASTLE_STATE eState
	SCRIPT_TIMER ModeTimer
	SCRIPT_TIMER AvailableTimer
ENDSTRUCT

STRUCT STRUCT_CASTLE_AREA_LOCAL
	VECTOR vLocateMin[MAX_NUM_LOCATES]
	VECTOR vLocateMax[MAX_NUM_LOCATES]
	FLOAT fLocateWidth[MAX_NUM_LOCATES]
	VECTOR vBlipCoords
	BLIP_INDEX blipCenterPoint
	BLIP_INDEX blipRadiusBlip
	BLIP_INDEX blipKing
	PED_INDEX kingPedId
	PED_INDEX heirPedId
	MARKER_TYPE eMarkerType = MARKER_SPHERE
	HUD_COLOURS eMarkerColour = HUD_COLOUR_YELLOW
	INT iHeirToThroneParticipant = -1
	INT iHeirToThronePlayer = -1
	BOOL bCastleAreaPopulated
	INT iNumParticipantsInCastleArea
	FLOAT fDistanceFromCastleAreaCenterPoint2d
	FLOAT fDistanceFromCastleAreaCenterPointZ
	FLOAT fDistanceFromCastleAreaCenterPoint3d
	FLOAT fDistanceFromCastleAreaCircularLocate2d[MAX_NUM_LOCATES]
	FLOAT fDistanceFromCastleCircularLocateZ[MAX_NUM_LOCATES]
	FLOAT fDistanceFromCastleCircularLocate3d[MAX_NUM_LOCATES]
	STRUCT_LEADERBOARD_DATA sBottomRightData[NUM_LEADERBOARD_PLAYERS]
	SCALEFORM_INDEX scDpadDownLeaderBoardMovie
	INT iCastleAreaKingParticipantFixedTickerCopy = -1
ENDSTRUCT

STRUCT STRUCT_CASTLE_AREA_CLIENT
	BOOL bIAmInCastleArea
ENDSTRUCT

STRUCT STRUCT_CASTLE_AREA_SERVER
	eCASTLE_AREA_STATE eState
	INT iKingPart = -1
	INT iKingPlayer = -1
	FLOAT fKingAreaPoints[NUM_NETWORK_PLAYERS]
	FLOAT fKingKillPoints[NUM_NETWORK_PLAYERS]
	INT iCastleAreaSeed = -1
	SCRIPT_TIMER KingInUncontestedAreaTimer
	BOOL bAreaContested
	INT iParticipantsInAreaBitset
	STRUCT_LEADERBOARD_DATA sLeaderboardData[NUM_NETWORK_PLAYERS]
	INT iPartFirstTimeAsKingBitset
	#IF IS_DEBUG_BUILD
	BOOL bForceEndForNotEnoughParticipants
	#ENDIF
ENDSTRUCT

STRUCT STRUCT_CASTLE_AREAS_COMMON
	INT iCastleAreasIAmInBitset
ENDSTRUCT

// The server broadcast data.
// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData
	eGAME_STATE eGameState
	STRUCT_KING_OF_THE_CASTLE_SERVER sKingOfTheCastle
	STRUCT_CASTLE_AREA_SERVER sCastleArea[NUM_CASTLE_AREAS]
	SCRIPT_TIMER MissionTerminateDelayTimer
	INT iBitset0
	
	INT iHashedMac
	INT iMatchHistoryId
	INT iPlayersLeftInProgress 
	
	FLOAT fHighestScore[NUM_NETWORK_PLAYERS]
	INT iHighestParticipant[NUM_NETWORK_PLAYERS]
	
	INT iPrevLeader = -1
	
	#IF IS_DEBUG_BUILD
	INT iSPassParticipant = -1
	INT iFFailParticipant = -1
	#ENDIF
ENDSTRUCT
ServerBroadcastData serverBD

scrFmEventAmbientMission telemetryStruct
INT iKingPartTelemetryCopy = -1
INT iNumOfKings

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	eGAME_STATE eGameState
	STRUCT_CASTLE_AREA_CLIENT sCastleArea[NUM_CASTLE_AREAS]
	STRUCT_CASTLE_AREAS_COMMON sCastleAreasCommonData
	
	INT iBitset0
	#IF IS_DEBUG_BUILD
	BOOL bSPass
	BOOL bFFail
	#ENDIF
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

STRUCT STRUCT_PARTICIPANT_ID_DATA
	PARTICIPANT_INDEX participantId
	INT iPlayerId
	TEXT_LABEL_63 tl63Name
ENDSTRUCT

STRUCT STRUCT_PLAYER_ID_DATA
	PLAYER_INDEX playerId
	INT iParticipantId
	PED_INDEX playerPedId
ENDSTRUCT

INT iLocalBitset0

INT iMinimapComponentBitset

INT iPeakQualifiedParticipants

SCRIPT_TIMER localParticipationTimer

//INT iFinalScores[4]

STRUCT_PARTICIPANT_ID_DATA sParticipant[NUM_NETWORK_PLAYERS]
STRUCT_PLAYER_ID_DATA sPlayer[NUM_NETWORK_PLAYERS]

// End 10s UI struct
STRUCT_FM_EVENTS_END_UI sEndUiVars

STRUCT_CASTLE_AREA_LOCAL sCastleAreaLocal[NUM_CASTLE_AREAS]

THE_LEADERBOARD_STRUCT sDpadDownLeaderBoardData[NUM_NETWORK_PLAYERS]
DPAD_VARS sDPadVars
FM_DPAD_LBD_STRUCT sFmDPadLbData

INT iStaggeredPartLoopCount

INT iPlayerOkBitset
INT iParticipantActiveBitset
INT iPlayerDeadBitset
INT iPeakParticipants
INT iTimesKilledAKing
INT iDeathsAsKing
INT iKillsAsKing

INT iAddedCustomBlipForPlayerBitset

INT iFocusParticipant = -1

HUD_COLOURS eCastleAreaColourCopy
INT iUpdateMinimapComponentColourStage

//CONST_INT CENTRE_POINT_BLIP_SWITCH_TIME	1000
//INT iSwitchCentrePointBlipStage
//SCRIPT_TIMER timerSwitchCentrePointBlip

//INT iSortingOuterCount

BOOL bForceRebuildNames // Ref var required by Brenda's bottom right HUD command. 

BOOL bCustomSpawn

BOOL bSet

// Widgets & Debug.
#IF IS_DEBUG_BUILD
	BOOL bHostEndMissionNow
	INT iStaggeredPartLoopCountWidgets
	TEXT_WIDGET_ID twIdCastleAreaState[NUM_CASTLE_AREAS]
	BOOL bDoJSkip
	VECTOR vJSkipCoords
	FLOAT fJSkipHeading
	BOOL bPrintLeaderboardSortingDebug
	BOOL bDoBottomRightPrints
	BOOL bSkipToTimeEnd
	BOOL bFakeHideEventNotPassiveCheck
	BOOL bFakeHideEventWithPassiveCheck
	BOOL bForceCastleAreaContested
	BOOL bForceNotPlacedWinner
	BOOL bCreateWidgets
	INT iWidgetsStage
	WIDGET_GROUP_ID kingOfTheCastleWidgetsId
	SCRIPT_TIMER timerSortingDebugDump
	CONST_INT SORTING_DEBUG_DUMP_TIME	60000
#ENDIF

#IF IS_DEBUG_BUILD 
FUNC STRING GET_GAME_STATE_NAME(eGAME_STATE eState)
	
	SWITCH eState
		CASE eGAMESTATE_INI					RETURN "eGAMESTATE_INI"
		CASE eGAMESTATE_RUNNING				RETURN "eGAMESTATE_RUNNING"
		CASE eGAMESTATE_LEAVE				RETURN "eGAMESTATE_LEAVE"
		CASE eGAMESTATE_DELAY_TERMINATION	RETURN "eGAMESTATE_DELAY_TERMINATION"
		CASE eGAMESTATE_END					RETURN "eGAMESTATE_END"
	ENDSWITCH
	
	RETURN "INVALID"
	
ENDFUNC
#ENDIF


/// PURPOSE:
///		1000 - Cash reward default.
FUNC INT GET_EOM_DEFAULT_CASH_REWARD()
	RETURN g_sMPTunables.iKING_OF_THE_CASTLE_EOM_DEFAULT_CASH_REWARD // TODO: Replace with tunable.
ENDFUNC

/// PURPOSE:
///		100 - RP reward default.
FUNC INT GET_EOM_DEFAULT_RP_REWARD()
	RETURN g_sMPTunables.iKING_OF_THE_CASTLE_EOM_DEFAULT_RP_REWARD  // TODO: Replace with tunable.
ENDFUNC

/// PURPOSE:
/// 	400 - Cash reward scale modifier.
FUNC INT GET_CASH_REWARD_SCALE()
	RETURN g_sMPTunables.iKING_OF_THE_CASTLE_CASH_REWARD_SCALE // TODO: Replace with tunable.
ENDFUNC

/// PURPOSE:
/// 	50 - RP reward scale modifier.
FUNC INT GET_RP_REWARD_SCALE()
	RETURN g_sMPTunables.iKING_OF_THE_CASTLE_RP_REWARD_SCALE // TODO: Replace with tunable.
ENDFUNC

/// PURPOSE:
/// 	3000 - Cash reward base.
FUNC INT GET_CASH_REWARD_BASE()
	RETURN g_sMPTunables.iKING_OF_THE_CASTLE_CASH_REWARD_BASE  // TODO: Replace with tunable.
ENDFUNC

/// PURPOSE:
/// 	400 - RP reward base.
FUNC INT GET_RP_REWARD_BASE()
	RETURN g_sMPTunables.iKING_OF_THE_CASTLE_RP_REWARD_BASE  // TODO: Replace with tunable.
ENDFUNC

/// PURPOSE:
/// 	50 - RP reward for killing the king.
FUNC INT GET_RP_REWARD_KILLED_KING()
	RETURN g_sMPTunables.iKING_OF_THE_CASTLE_RP_REWARD_KILLED_KING  // TODO: Replace with tunable.
ENDFUNC

/// PURPOSE:
/// 	20 - RP reward for killing participants as the king.
FUNC INT GET_RP_REWARD_KILL_AS_KING()
	RETURN g_sMPTunables.iKING_OF_THE_CASTLE_RP_REWARD_KILL_AS_KING // TODO: Replace with tunable.
ENDFUNC

/// PURPOSE:
/// 	20 - Cap for number of rewards for killing the king.
FUNC INT GET_RP_REWARD_KILLED_KING_CAP()
	RETURN g_sMPTunables.iKING_OF_THE_CASTLE_RP_REWARD_KILLED_KING_CAP // TODO: Replace with tunable.
ENDFUNC

/// PURPOSE:
/// 	40 - Cap for number of rewards for killing participants as the king.
FUNC INT GET_RP_REWARD_KILL_AS_KING_CAP()
	RETURN g_sMPTunables.iKING_OF_THE_CASTLE_RP_REWARD_KILL_AS_KING_CAP	 // TODO: Replace with tunable.
ENDFUNC

/// PURPOSE:
/// 	3.0 - Time weight modifier for all reward calculations.
FUNC FLOAT GET_REWARD_TIME_WEIGHTING()
	RETURN g_sMPTunables.fKING_OF_THE_CASTLE_REWARD_TIME_WEIGHTING // TODO: Replace with tunable.
ENDFUNC

/// PURPOSE:
/// 	0.5 - Player weight modifier for all reward calculations.
FUNC FLOAT GET_REWARD_PLAYER_WEIGHTING()
	RETURN g_sMPTunables.fKING_OF_THE_CASTLE_REWARD_PLAYER_WEIGHTING // TODO: Replace with tunable.
ENDFUNC

/// PURPOSE:
/// 	15.0 - Player divider for all reward calculations.
FUNC FLOAT GET_REWARD_PLAYER_DIVIDER()
	RETURN g_sMPTunables.fKING_OF_THE_CASTLE_REWARD_PLAYER_DIVIDER // TODO: Replace with tunable.
ENDFUNC

/// PURPOSE:
/// 	10 - Time cap for reward calculation.
FUNC INT GET_REWARD_TIME_CAP()
	RETURN g_sMPTunables.iKing_of_the_Castle_T_Cap                       
ENDFUNC

/// PURPOSE:
/// 	30 - Player cap for reward calculation.
FUNC INT GET_REWARD_PLAYER_CAP()
	RETURN g_sMPTunables.iKing_of_the_Castle_P_Cap
ENDFUNC


FUNC VECTOR GET_CASTLE_AREA_LOCATE_MIN(INT iCastleArea, INT iLocate)
	RETURN sCastleAreaLocal[iCastleArea].vLocateMin[iLocate]
ENDFUNC

FUNC INT ACCURATLEY_ROUND_SCORE(FLOAT fScore)
	
	INT iScore = FLOOR(fScore)
	iScore /= g_sMPTunables.iKing_of_the_Castle_Points_per_X_seconds_as_King
	iScore *= g_sMPTunables.iKing_of_the_Castle_Points_per_X_seconds_as_King
	
	RETURN  iScore
	
ENDFUNC

PROC SET_CASTLE_AREA_LOCATE_MIN(INT iCastleArea, INT iLocate, VECTOR vLocateMin)
	
	#IF IS_DEBUG_BUILD
	IF NOT ARE_VECTORS_EQUAL(vLocateMin, GET_CASTLE_AREA_LOCATE_MIN(iCastleArea, iLocate))
		PRINTLN("[KINGCASTLE] - [CASTLE AREA] - set castle area ", iCastleArea, " locate [", iLocate, "] min to ", vLocateMin)
	ENDIF
	#ENDIF
	
	sCastleAreaLocal[iCastleArea].vLocateMin[iLocate] = vLocateMin
	
ENDPROC

FUNC VECTOR GET_CASTLE_AREA_LOCATE_MAX(INT iCastleArea, INT iLocate)
	RETURN sCastleAreaLocal[iCastleArea].vLocateMax[iLocate]
ENDFUNC

PROC SET_CASTLE_AREA_LOCATE_MAX(INT iCastleArea, INT iLocate, VECTOR vLocateMax)
	
	#IF IS_DEBUG_BUILD
	IF NOT ARE_VECTORS_EQUAL(vLocateMax, GET_CASTLE_AREA_LOCATE_MAX(iCastleArea, iLocate))
		PRINTLN("[KINGCASTLE] - [CASTLE AREA] - set castle area ", iCastleArea, " locate [", iLocate, "] max to ", vLocateMax)
	ENDIF
	#ENDIF
	
	sCastleAreaLocal[iCastleArea].vLocateMax[iLocate] = vLocateMax
	
ENDPROC

FUNC FLOAT GET_CASTLE_AREA_LOCATE_WIDTH(INT iCastleArea, INT iLocate)
	RETURN sCastleAreaLocal[iCastleArea].fLocateWidth[iLocate]
ENDFUNC

PROC SET_CASTLE_AREA_LOCATE_WIDTH(INT iCastleArea, INT iLocate, FLOAT fLocateWidth)

	#IF IS_DEBUG_BUILD
	IF fLocateWidth != GET_CASTLE_AREA_LOCATE_WIDTH(iCastleArea, iLocate)
		PRINTLN("[KINGCASTLE] - [CASTLE AREA] - set castle area ", iCastleArea, " locate [", iLocate, "] width to ", fLocateWidth)
	ENDIF
	#ENDIF
	
	sCastleAreaLocal[iCastleArea].fLocateWidth[iLocate] = fLocateWidth
	
ENDPROC

FUNC BLIP_INDEX GET_CASTLE_AREA_CENTER_POINT_BLIP_INDEX(INT iCastleArea)
	RETURN sCastleAreaLocal[iCastleArea].blipCenterPoint
ENDFUNC

PROC SET_CASTLE_AREA_CENTER_POINT_BLIP_INDEX(INT iCastleArea, BLIP_INDEX blipId)

	#IF IS_DEBUG_BUILD
	IF blipId != GET_CASTLE_AREA_CENTER_POINT_BLIP_INDEX(iCastleArea)
		INT iBlipId = NATIVE_TO_INT(blipId)
		PRINTLN("[KINGCASTLE] - [CASTLE BLIP] - set castle area ", iCastleArea, " NATIVE_TO_INT(blipId) to ", iBlipId)
	ENDIF
	#ENDIF
	
	sCastleAreaLocal[iCastleArea].blipCenterPoint = blipId
	
ENDPROC

FUNC BLIP_INDEX GET_CASTLE_AREA_RADIUS_BLIP_INDEX(INT iCastleArea)
	RETURN sCastleAreaLocal[iCastleArea].blipRadiusBlip
ENDFUNC

PROC SET_CASTLE_AREA_RADIUS_BLIP_INDEX(INT iCastleArea, BLIP_INDEX blipId)

	#IF IS_DEBUG_BUILD
	IF blipId != GET_CASTLE_AREA_RADIUS_BLIP_INDEX(iCastleArea)
		INT iBlipId = NATIVE_TO_INT(blipId)
		PRINTLN("[KINGCASTLE] - [CASTLE RADIUS BLIP] - set castle area radius ", iCastleArea, " NATIVE_TO_INT(blipId) to ", iBlipId)
	ENDIF
	#ENDIF
	
	sCastleAreaLocal[iCastleArea].blipRadiusBlip = blipId
	
ENDPROC

FUNC BLIP_INDEX GET_CASTLE_AREA_KING_BLIP_INDEX(INT iCastleArea)
	RETURN sCastleAreaLocal[iCastleArea].blipKing
ENDFUNC

PROC SET_CASTLE_AREA_KING_BLIP_INDEX(INT iCastleArea, BLIP_INDEX blipId)
	
	#IF IS_DEBUG_BUILD
	IF blipId != GET_CASTLE_AREA_KING_BLIP_INDEX(iCastleArea)
		INT iBlipId = NATIVE_TO_INT(blipId)
		PRINTLN("[KINGCASTLE] - [CASTLE KING BLIP] - set castle area king ", iCastleArea, " NATIVE_TO_INT(blipId) to ", iBlipId)
	ENDIF
	#ENDIF
	
	sCastleAreaLocal[iCastleArea].blipKing = blipId
	
ENDPROC

FUNC PED_INDEX GET_CASTLE_AREA_KING_PED_ID(INT iCastleArea)
	RETURN sCastleAreaLocal[iCastleArea].kingPedId
ENDFUNC

PROC SET_CASTLE_AREA_KING_PED_ID(INT iCastleArea, PED_INDEX kingPedId)
	
	#IF IS_DEBUG_BUILD
	IF kingPedId != GET_CASTLE_AREA_KING_PED_ID(iCastleArea)
		INT iKingPedId = NATIVE_TO_INT(kingPedId)
		PRINTLN("[KINGCASTLE] - [CASTLE KING PED ID] - set castle area king ped ", iCastleArea, " NATIVE_TO_INT(kingPedId) to ", iKingPedId)
	ENDIF
	#ENDIF
	
	sCastleAreaLocal[iCastleArea].kingPedId = kingPedId
	
ENDPROC

FUNC PED_INDEX GET_CASTLE_AREA_HEIR_PED_ID(INT iCastleArea)
	RETURN sCastleAreaLocal[iCastleArea].heirPedId
ENDFUNC

PROC SET_CASTLE_AREA_HEIR_PED_ID(INT iCastleArea, PED_INDEX heirPedId)
//	
//	#IF IS_DEBUG_BUILD
//	IF heirPedId != GET_CASTLE_AREA_HEIR_PED_ID(iCastleArea)
//		INT iHeirPedId = NATIVE_TO_INT(heirPedId)
//		PRINTLN("[KINGCASTLE] - [CASTLE KING PED ID] - set castle area heir ped ", iCastleArea, " NATIVE_TO_INT(heirPedId) to ", iHeirPedId)
//	ENDIF
//	#ENDIF
//	
	sCastleAreaLocal[iCastleArea].heirPedId = heirPedId
	
ENDPROC

FUNC MARKER_TYPE GET_CASTLE_AREA_MARKER_TYPE(INT iCastleArea)
	RETURN sCastleAreaLocal[iCastleArea].eMarkerType
ENDFUNC

PROC SET_CASTLE_AREA_MARKER_TYPE(INT iCastleArea, MARKER_TYPE eMarkerType)

	#IF IS_DEBUG_BUILD
	IF eMarkerType != GET_CASTLE_AREA_MARKER_TYPE(iCastleArea)
		INT iMarker = ENUM_TO_INT(eMarkerType)
		PRINTLN("[KINGCASTLE] - [CASTLE MARKER] - set castle area ", iCastleArea, " ENUM_TO_INT(eMarkerType) to ", iMarker)
	ENDIF
	#ENDIF
	
	sCastleAreaLocal[iCastleArea].eMarkerType = eMarkerType
	
ENDPROC

FUNC HUD_COLOURS GET_CASTLE_AREA_MARKER_COLOUR(INT iCastleArea)
	RETURN sCastleAreaLocal[iCastleArea].eMarkerColour
ENDFUNC

PROC SET_CASTLE_AREA_MARKER_COLOUR(INT iCastleArea, HUD_COLOURS eMarkerColour)

	#IF IS_DEBUG_BUILD
	IF eMarkerColour != GET_CASTLE_AREA_MARKER_COLOUR(iCastleArea)
		INT iMarker = ENUM_TO_INT(eMarkerColour)
		PRINTLN("[KINGCASTLE] - [CASTLE MARKER] - set castle area ", iCastleArea, " ENUM_TO_INT(eMarkerColour) to ", iMarker)
	ENDIF
	#ENDIF
	
	sCastleAreaLocal[iCastleArea].eMarkerColour = eMarkerColour
	
ENDPROC

FUNC INT GET_CASTLE_AREA_HEIR_PARTICIPANT_ID(INT iCastleArea)
	RETURN sCastleAreaLocal[iCastleArea].iHeirToThroneParticipant
ENDFUNC

PROC SET_CASTLE_AREA_HEIR_PARTICIPANT_ID(INT iCastleArea, INT iParticipant)
	
//	#IF IS_DEBUG_BUILD
//	IF iParticipant != GET_CASTLE_AREA_HEIR_PARTICIPANT_ID(iCastleArea)
//		PRINTLN("[KINGCASTLE] - [CASTLE AREA HEIR] - set castle area ", iCastleArea, " heir iParticipant to ", iParticipant)
//	ENDIF
//	#ENDIF
	
	sCastleAreaLocal[iCastleArea].iHeirToThroneParticipant = iParticipant
	
ENDPROC

FUNC INT GET_CASTLE_AREA_HEIR_PLAYER_ID(INT iCastleArea)
	RETURN sCastleAreaLocal[iCastleArea].iHeirToThronePlayer
ENDFUNC

PROC SET_CASTLE_AREA_HEIR_PLAYER_ID(INT iCastleArea, INT iPlayer)
	
//	#IF IS_DEBUG_BUILD
//	IF iPlayer != GET_CASTLE_AREA_HEIR_PLAYER_ID(iCastleArea)
//		PRINTLN("[KINGCASTLE] - [CASTLE AREA HEIR] - set castle area ", iCastleArea, " heir iPlayer to ", iPlayer)
//	ENDIF
//	#ENDIF
	
	sCastleAreaLocal[iCastleArea].iHeirToThronePlayer = iPlayer
	
ENDPROC

FUNC BOOL GET_IS_CASTLE_AREA_POPULATED(INT iCastleArea)
	RETURN sCastleAreaLocal[iCastleArea].bCastleAreaPopulated
ENDFUNC

PROC SET_CASTLE_AREA_POPULATED(INT iCastleArea, BOOL bPopulated)
//	
//	#IF IS_DEBUG_BUILD
//	IF bPopulated != GET_IS_CASTLE_AREA_POPULATED(iCastleArea)
//		PRINTLN("[KINGCASTLE] - [CASTLE AREA] - set castle area ", iCastleArea, " populated: ", bPopulated)
//	ENDIF
//	#ENDIF
//	
	sCastleAreaLocal[iCastleArea].bCastleAreaPopulated = bPopulated
	
ENDPROC

FUNC INT GET_NUMBER_OF_PARTICIPANTS_IN_CASTLE_AREA(INT iCastleArea)
	RETURN sCastleAreaLocal[iCastleArea].iNumParticipantsInCastleArea
ENDFUNC

PROC SET_NUMBER_OF_PARTICIPANTS_IN_CASTLE_AREA(INT iCastleArea, INT iNumParticipantsInCastleArea)
//	
//	#IF IS_DEBUG_BUILD
//	IF iNumParticipantsInCastleArea != GET_NUMBER_OF_PARTICIPANTS_IN_CASTLE_AREA(iCastleArea)
//		PRINTLN("[KINGCASTLE] - [CASTLE AREA] - set num participants in castle area ", iCastleArea, " to ", iNumParticipantsInCastleArea)
//	ENDIF
//	#ENDIF
//	
	sCastleAreaLocal[iCastleArea].iNumParticipantsInCastleArea = iNumParticipantsInCastleArea
	
ENDPROC

FUNC VECTOR GET_CASTLE_AREA_BLIP_COORDS(INT iCastleArea)
	RETURN sCastleAreaLocal[iCastleArea].vBlipCoords
ENDFUNC

PROC SET_CASTLE_AREA_BLIP_COORDS(INT iCastleArea, VECTOR vBlipCoords)

	#IF IS_DEBUG_BUILD
	IF NOT ARE_VECTORS_EQUAL(vBlipCoords, GET_CASTLE_AREA_BLIP_COORDS(iCastleArea))
		PRINTLN("[KINGCASTLE] - [CASTLE MARKER] - set castle area ", iCastleArea, " blip coords to ", vBlipCoords)
	ENDIF
	#ENDIF
	
	sCastleAreaLocal[iCastleArea].vBlipCoords = vBlipCoords
	
ENDPROC

FUNC INT GET_CASTLE_AREA_PARTICIPANT_IS_IN(INT iParticipant)
	
	INT iArea
	
	REPEAT NUM_CASTLE_AREAS iArea
		IF IS_BIT_SET(playerBD[iParticipant].sCastleAreasCommonData.iCastleAreasIAmInBitset, iArea)
			RETURN iArea
		ENDIF
	ENDREPEAT
	
	RETURN (-1)
	
ENDFUNC

FUNC BOOL GET_DOES_PARTICIPANT_SAY_INSIDE_CASTLE_AREA(INT iParticipant, INT iCastleArea)
	RETURN playerBD[iParticipant].sCastleArea[iCastleArea].bIAmInCastleArea
ENDFUNC

/// PURPOSE:
///    Query if a participant is considered a critical player due to them holding a castle area.
FUNC BOOL IS_PARTICIPANT_IN_ANY_CASTLE_AREA(INT iParticipant)
	
	// Check the participant is valid
	IF iParticipant = -1
		SCRIPT_ASSERT("[KINGCASTLE] IS_PARTICIPANT_IN_ANY_CASTLE_AREA - iParticipant = -1. Invalid participant passed.")
		RETURN FALSE
	ENDIF
	
	// If the participant bitset is non-zero, they are in one of the castle areas
	IF playerBD[iParticipant].sCastleAreasCommonData.iCastleAreasIAmInBitset != 0
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC SET_I_AM_INSIDE_CASTLE_AREA(INT iCastleArea, BOOL bInArea)
	
	#IF IS_DEBUG_BUILD
	IF bInArea != GET_DOES_PARTICIPANT_SAY_INSIDE_CASTLE_AREA(PARTICIPANT_ID_TO_INT(), iCastleArea)
		IF bInArea
			PRINTLN("[KINGCASTLE] - [CASTLE AREA] - setting I am in castle area ", iCastleArea, ". Participant Id = ", PARTICIPANT_ID_TO_INT())
			PRINTLN("[KINGCASTLE] - [CASTLE AREA] - setting iCastleAreasIAmInBitset bit ", iCastleArea, ". Participant Id = ", PARTICIPANT_ID_TO_INT())
		ELSE
			PRINTLN("[KINGCASTLE] - [CASTLE AREA] - setting I am not in castle area ", iCastleArea, ". Participant Id = ", PARTICIPANT_ID_TO_INT())
			PRINTLN("[KINGCASTLE] - [CASTLE AREA] - clearing iCastleAreasIAmInBitset bit ", iCastleArea, ". Participant Id = ", PARTICIPANT_ID_TO_INT())
		ENDIF
	ENDIF
	#ENDIF
	
	playerBD[PARTICIPANT_ID_TO_INT()].sCastleArea[iCastleArea].bIAmInCastleArea = bInArea
	
	IF bInArea
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sCastleAreasCommonData.iCastleAreasIAmInBitset, iCastleArea)
	ELSE
		CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sCastleAreasCommonData.iCastleAreasIAmInBitset, iCastleArea)
	ENDIF
	
ENDPROC

FUNC FLOAT GET_PARTICIPANT_2D_DISTANCE_FROM_CASTLE_AREA_CENTER_POINT(INT iCastleArea)
	RETURN sCastleAreaLocal[iCastleArea].fDistanceFromCastleAreaCenterPoint2d
ENDFUNC

PROC SET_LOCAL_PARTICIPANT_2D_DISTANCE_FROM_CASTLE_AREA_CENTER_POINT(INT iCastleArea, FLOAT fDistanceFromCastleAreaCenterPoint2d)
	sCastleAreaLocal[iCastleArea].fDistanceFromCastleAreaCenterPoint2d = fDistanceFromCastleAreaCenterPoint2d
ENDPROC

FUNC FLOAT GET_PARTICIPANT_Z_DISTANCE_FROM_CASTLE_AREA_CENTER_POINT(INT iCastleArea)
	RETURN sCastleAreaLocal[iCastleArea].fDistanceFromCastleAreaCenterPointZ
ENDFUNC

PROC SET_LOCAL_PARTICIPANT_Z_DISTANCE_FROM_CASTLE_AREA_CENTER_POINT(INT iCastleArea, FLOAT fDistanceFromCastleAreaCenterPointZ)
	sCastleAreaLocal[iCastleArea].fDistanceFromCastleAreaCenterPointZ = fDistanceFromCastleAreaCenterPointZ
ENDPROC

FUNC FLOAT GET_PARTICIPANT_3D_DISTANCE_FROM_CASTLE_AREA_CENTER_POINT(INT iCastleArea)
	RETURN sCastleAreaLocal[iCastleArea].fDistanceFromCastleAreaCenterPoint3d
ENDFUNC

PROC SET_LOCAL_PARTICIPANT_3D_DISTANCE_FROM_CASTLE_AREA_CENTER_POINT(INT iCastleArea, FLOAT fDistanceFromCastleAreaCenterPoint3d)
	sCastleAreaLocal[iCastleArea].fDistanceFromCastleAreaCenterPoint3d = fDistanceFromCastleAreaCenterPoint3d
ENDPROC

// ***

FUNC FLOAT GET_PARTICIPANT_2D_DISTANCE_FROM_CASTLE_CIRCULAR_LOCATE_POINT(INT iCastleArea, INT iLocate)
	RETURN sCastleAreaLocal[iCastleArea].fDistanceFromCastleAreaCircularLocate2d[iLocate]
ENDFUNC

PROC SET_LOCAL_PARTICIPANT_2D_DISTANCE_FROM_CASTLE_CIRCULAR_LOCATE_POINT(INT iCastleArea, INT iLocate, FLOAT fDistanceFromCastleAreaCircularLocatePoint2d)
	sCastleAreaLocal[iCastleArea].fDistanceFromCastleAreaCircularLocate2d[iLocate] = fDistanceFromCastleAreaCircularLocatePoint2d
ENDPROC

FUNC FLOAT GET_PARTICIPANT_Z_DISTANCE_FROM_CASTLE_CIRCULAR_LOCATE_POINT(INT iCastleArea, INT iLocate)
	RETURN sCastleAreaLocal[iCastleArea].fDistanceFromCastleCircularLocateZ[iLocate]
ENDFUNC

PROC SET_LOCAL_PARTICIPANT_Z_DISTANCE_FROM_CASTLE_CIRCULAR_LOCATE_POINT(INT iCastleArea, INT iLocate, FLOAT fDistanceFromCastleAreaCircularLocatePointZ)
	sCastleAreaLocal[iCastleArea].fDistanceFromCastleCircularLocateZ[iLocate] = fDistanceFromCastleAreaCircularLocatePointZ
ENDPROC

FUNC FLOAT GET_PARTICIPANT_3D_DISTANCE_FROM_CASTLE_CIRCULAR_LOCATE_POINT(INT iCastleArea, INT iLocate)
	RETURN sCastleAreaLocal[iCastleArea].fDistanceFromCastleCircularLocate3d[iLocate]
ENDFUNC

PROC SET_LOCAL_PARTICIPANT_3D_DISTANCE_FROM_CASTLE_CIRCULAR_LOCATE_POINT(INT iCastleArea, INT iLocate, FLOAT fDistanceFromCastleAreaCircularLocatePoint3d)
	sCastleAreaLocal[iCastleArea].fDistanceFromCastleCircularLocate3d[iLocate] = fDistanceFromCastleAreaCircularLocatePoint3d
ENDPROC

FUNC BOOL GET_IS_PARTICIPANT_FULLY_TAKING_PART(INT iParticipant)
	
	IF IS_BIT_SET(playerBD[iParticipant].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_FULLY_ACTIVE_PARTICIPANT_TRIGGERED_BY_INTERACTION))
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(playerBD[iParticipant].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_FULLY_ACTIVE_PARTICIPANT_TRIGGERED_BY_DISTANCE))
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iFmAmbientEventBitSet, ciSET_PLAYER_OPTED_IN)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

#IF IS_DEBUG_BUILD 
FUNC STRING GET_CASTLE_AREA_STATE_NAME(eCASTLE_AREA_STATE eState)
	
	SWITCH eState
		CASE eCASTLEAREASTATE_UNENTERED	RETURN "eCASTLEAREASTATE_UNENTERED"
		CASE eCASTLEAREASTATE_POPULATED	RETURN "eCASTLEAREASTATE_POPULATED"
		CASE eCASTLEAREASTATE_EMPTY		RETURN "eCASTLEAREASTATE_EMPTY"
	ENDSWITCH
	
	RETURN "INVALID"
	
ENDFUNC
#ENDIF

FUNC eCASTLE_AREA_STATE GET_CASTLE_AREA_STATE(INT iCastleArea)
	RETURN serverBD.sCastleArea[iCastleArea].eState
ENDFUNC

PROC SET_CASTLE_AREA_STATE(INT iCastleArea, eCASTLE_AREA_STATE eState)
	
	#IF IS_DEBUG_BUILD
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		TEXT_LABEL_63 errorMsg = "King Castle - Client set area state. Client,Area = "
		errorMsg += PARTICIPANT_ID_TO_INT()
		errorMsg += iCastleArea
		SCRIPT_ASSERT(errorMsg)
	ENDIF
	IF eState != GET_CASTLE_AREA_STATE(iCastleArea)
		STRING strStateName = GET_CASTLE_AREA_STATE_NAME(eState)
		PRINTLN("[KINGCASTLE] - [CASTLE AREA STATE] - castle area ", iCastleArea, " going to state ", strStateName)
	ENDIF
	#ENDIF
	
	serverBD.sCastleArea[iCastleArea].eState = eState	
	
ENDPROC

FUNC INT GET_CASTLE_AREA_KING_PARTICIPANT_ID(INT iCastleArea)
	RETURN serverBD.sCastleArea[iCastleArea].iKingPart
ENDFUNC

PROC SET_CASTLE_AREA_KING_PARTICIPANT_ID(INT iCastleArea, INT iParticipant)
	
	#IF IS_DEBUG_BUILD
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		TEXT_LABEL_63 errorMsg = "King Castle - Client set king part. Client,Area = "
		errorMsg += PARTICIPANT_ID_TO_INT()
		errorMsg += iCastleArea
		SCRIPT_ASSERT(errorMsg)
	ENDIF
	#ENDIF
	
	IF iParticipant != GET_CASTLE_AREA_KING_PARTICIPANT_ID(iCastleArea)
		PRINTLN("[KINGCASTLE] - [CASTLE AREA KING] - castle area ", iCastleArea, " iKingPart to ", iParticipant)
	ENDIF
	
	serverBD.sCastleArea[iCastleArea].iKingPart = iParticipant
	
ENDPROC

FUNC INT GET_CASTLE_AREA_KING_PLAYER_ID(INT iCastleArea)
	RETURN serverBD.sCastleArea[iCastleArea].iKingPlayer
ENDFUNC

PROC SET_CASTLE_AREA_KING_PLAYER_ID(INT iCastleArea, INT iPlayer)
	
	#IF IS_DEBUG_BUILD
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		TEXT_LABEL_63 errorMsg = "King Castle - Client set king plyr. Client,Area = "
		errorMsg += PARTICIPANT_ID_TO_INT()
		errorMsg += iCastleArea
		SCRIPT_ASSERT(errorMsg)
	ENDIF
	IF iPlayer != GET_CASTLE_AREA_KING_PLAYER_ID(iCastleArea)
		PRINTLN("[KINGCASTLE] - [CASTLE AREA KING] - castle area ", iCastleArea, " iKingPlayer to ", iPlayer)
	ENDIF
	#ENDIF
	
	serverBD.sCastleArea[iCastleArea].iKingPlayer = iPlayer
	
ENDPROC

FUNC FLOAT GET_CASTLE_AREA_KING_AREA_POINTS(INT iCastleArea, INT iParticipant)
	RETURN serverBD.sCastleArea[iCastleArea].fKingAreaPoints[iParticipant]
ENDFUNC

PROC SET_CASTLE_AREA_KING_AREA_POINTS(INT iCastleArea, INT iParticipant, FLOAT fPoints)
	
//	#IF IS_DEBUG_BUILD
//	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
//		TEXT_LABEL_63 errorMsg = "King Castle - Client set king aPts. Client,Area = "
//		errorMsg += PARTICIPANT_ID_TO_INT()
//		errorMsg += iCastleArea
//		SCRIPT_ASSERT(errorMsg)
//	ENDIF
//	IF iPoints != GET_CASTLE_AREA_KING_AREA_POINTS(iCastleArea, iParticipant)
//		PRINTLN("[KINGCASTLE] - [CASTLE AREA KING] - castle area ", iCastleArea, " fKingAreaPoints[", iParticipant, "] to ", iPoints)
//	ENDIF
//	#ENDIF
	
	serverBD.sCastleArea[iCastleArea].fKingAreaPoints[iParticipant] = fPoints
	
ENDPROC

FUNC FLOAT GET_CASTLE_AREA_KING_KILL_POINTS(INT iCastleArea, INT iParticipant)
	RETURN serverBD.sCastleArea[iCastleArea].fKingKillPoints[iParticipant]
ENDFUNC

PROC SET_CASTLE_AREA_KING_KILL_POINTS(INT iCastleArea, INT iParticipant, FLOAT fPoints)
	
	#IF IS_DEBUG_BUILD
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		TEXT_LABEL_63 errorMsg = "King Castle - Client set king kPts. Client,Area = "
		errorMsg += PARTICIPANT_ID_TO_INT()
		errorMsg += iCastleArea
		SCRIPT_ASSERT(errorMsg)
	ENDIF
	IF fPoints != GET_CASTLE_AREA_KING_KILL_POINTS(iCastleArea, iParticipant)
		PRINTLN("[KINGCASTLE] - [CASTLE AREA KING] - castle area ", iCastleArea, " fKingKillPoints[", iParticipant, "] to ", fPoints)
	ENDIF
	#ENDIF
	
	serverBD.sCastleArea[iCastleArea].fKingKillPoints[iParticipant] = fPoints
	
ENDPROC

FUNC FLOAT GET_CASTLE_AREA_KING_TOTAL_POINTS(INT iCastleArea, INT iParticipant)
	FLOAT fKillPoints = GET_CASTLE_AREA_KING_KILL_POINTS(iCastleArea, iParticipant)
	FLOAT fAreaPoints = GET_CASTLE_AREA_KING_AREA_POINTS(iCastleArea, iParticipant)
	RETURN (fKillPoints+fAreaPoints)
ENDFUNC

FUNC INT GET_CASTLE_AREA_SEED(INT iCastleArea)
	RETURN serverBD.sCastleArea[iCastleArea].iCastleAreaSeed
ENDFUNC

PROC SET_CASTLE_AREA_SEED(INT iCastleArea, INT iCastleAreaSeed)
	
	#IF IS_DEBUG_BUILD
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		TEXT_LABEL_63 errorMsg = "King Castle - Client set area seed. Client,Area = "
		errorMsg += PARTICIPANT_ID_TO_INT()
		errorMsg += iCastleArea
		SCRIPT_ASSERT(errorMsg)
	ENDIF
	IF iCastleAreaSeed != GET_CASTLE_AREA_SEED(iCastleArea)
		PRINTLN("[KINGCASTLE] - [CASTLE AREA SEED] - castle area ", iCastleArea, " iCastleAreaSeed to ", iCastleAreaSeed)
	ENDIF
	#ENDIF
	
	serverBD.sCastleArea[iCastleArea].iCastleAreaSeed = iCastleAreaSeed
	
ENDPROC

FUNC BOOL GET_IS_CASTLE_AREA_CONTESTED(INT iCastleArea)
	#IF IS_DEBUG_BUILD
	IF bForceCastleAreaContested
		RETURN TRUE
	ENDIF
	#ENDIF
	RETURN serverBD.sCastleArea[iCastleArea].bAreaContested
ENDFUNC

PROC SET_CASTLE_AREA_CONTESTED(INT iCastleArea, BOOL bContested)
	
	#IF IS_DEBUG_BUILD
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		TEXT_LABEL_63 errorMsg = "King Castle - Client set area contested. Client,Area = "
		errorMsg += PARTICIPANT_ID_TO_INT()
		errorMsg += iCastleArea
		SCRIPT_ASSERT(errorMsg)
	ENDIF
	IF bContested != GET_IS_CASTLE_AREA_CONTESTED(iCastleArea)
		PRINTLN("[KINGCASTLE] - [CASTLE AREA STATE] - setting castle area ", iCastleArea, " contested ", bContested)
	ENDIF
	#ENDIF
	
	serverBD.sCastleArea[iCastleArea].bAreaContested = bContested	
	
ENDPROC

FUNC BOOL IS_PARTICIPANT_QUALIFYING(INT iParticipant)
	
	IF IS_BIT_SET(playerBD[iParticipant].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_QUALIFYING_PARTICIPANT_FOR_ENTERING_CASTLE_AREA))
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(playerBD[iParticipant].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_QUALIFYING_PARTICIPANT_FOR_BEING_KING))
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(playerBD[iParticipant].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_QUALIFYING_PARTICIPANT_FOR_DAMAGING_KING))
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(playerBD[iParticipant].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_QUALIFYING_PARTICIPANT_FOR_BEING_DAMAGED_BY_KING))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC INT GET_CASTLE_AREA_NUM_SPAWN_AREAS(INT iCastleArea)
	
	SWITCH iCastleArea
		CASE CASTLE_AREA_BEACH_TOWER 		RETURN 1
		CASE CASTLE_AREA_ALIEN_HIPPY_HILL 	RETURN 1
		CASE CASTLE_AREA_FIRE_STATION 		RETURN 4
		CASE CASTLE_AREA_LAND_ACT_DAM		RETURN 3
		CASE CASTLE_AREA_CABLE_CAR_STATION	RETURN 4
		CASE CASTLE_AREA_OBSERVATORY		RETURN 4
		CASE CASTLE_AREA_SCRAP_YARD			RETURN 4
		CASE CASTLE_AREA_PLAYBOY_GROTTO		RETURN 4
	ENDSWITCH
	
	RETURN 0
	
ENDFUNC

FUNC INT GET_CASTLE_AREA_NUM_SPAWN_OCCLUSION_AREAS(INT iCastleArea)
	
	SWITCH iCastleArea
		CASE CASTLE_AREA_BEACH_TOWER 		RETURN 2
		CASE CASTLE_AREA_ALIEN_HIPPY_HILL 	RETURN 1
		CASE CASTLE_AREA_FIRE_STATION 		RETURN 2
		CASE CASTLE_AREA_LAND_ACT_DAM		RETURN 3
		CASE CASTLE_AREA_CABLE_CAR_STATION	RETURN 3
		CASE CASTLE_AREA_OBSERVATORY		RETURN 4
		CASE CASTLE_AREA_SCRAP_YARD			RETURN 4
		CASE CASTLE_AREA_PLAYBOY_GROTTO		RETURN 4
	ENDSWITCH
	
	RETURN 0
	
ENDFUNC

PROC GET_CASTLE_AREA_SPAWN_ANGLED_AREA_DATA(INT iSeed, INT iSpawnArea, VECTOR &vMin, VECTOR &vMax, FLOAT &fWidth)
	
	SWITCH iSeed
		CASE CASTLE_AREA_BEACH_TOWER
			vMin = << -1289.200, -1785.100, 0.000 >>
			vMax = << -1134.600, -1676.800, 18.000 >>
			fWidth = 200.0
		BREAK
		CASE CASTLE_AREA_FIRE_STATION
			SWITCH iSpawnArea
				CASE 0
					vMin = <<-366.314209,6064.000488,30.250107>>
					vMax = <<-354.428284,6076.069336,35.496456>>
					fWidth = 14.000000
				BREAK
				CASE 1
					vMin = <<-396.411957,6063.504395,30.250103>>
					vMax = <<-386.700287,6054.695801,35.500107>>
					fWidth = 14.000000
				BREAK
				CASE 2
					vMin = <<-397.520691,6110.494141,30.078651>>
					vMax = <<-388.736542,6119.384277,35.398296>>
					fWidth = 14.000000
				BREAK
				CASE 3
					vMin = <<-355.892090,6122.812012,31.044201>>
					vMax = <<-349.429871,6129.835938,35.464859>>
					fWidth = 16.000000
				BREAK
			ENDSWITCH
		BREAK
		CASE CASTLE_AREA_ALIEN_HIPPY_HILL
			vMin = << 2374.600, 3727.500, 25.0 >>
			vMax = << 2366.700, 3872.900, 60.0 >>
			fWidth = 80.0
		BREAK
		CASE CASTLE_AREA_LAND_ACT_DAM
			SWITCH iSpawnArea
				CASE 0
					vMin = <<1684.910034,74.408989,169.963150>>
					vMax = <<1644.802856,24.899330,177.575394>>
					fWidth = 20.000000
				BREAK
				CASE 1
					vMin = <<1677.546021,-71.202583,172.652496>>
					vMax = <<1742.510010,-109.694901,184.986771>>
					fWidth = 20.000000
				BREAK
				CASE 2
					vMin = <<1587.505981,-75.729324,160.302856>>
					vMax = <<1657.814819,-88.202606,175.601624>>
					fWidth = 16.000000
				BREAK
			ENDSWITCH
		BREAK
		CASE CASTLE_AREA_CABLE_CAR_STATION
			SWITCH iSpawnArea
				CASE 0
					vMin = <<-714.969482,5560.145020,26.115997>>
					vMax = <<-710.665955,5652.130859,49.028946>>
					fWidth = 40.000000
				BREAK
				CASE 1
					vMin = <<-700.229797,5623.050293,20.074814>>
					vMax = <<-767.116333,5628.692383,45.705383>>
					fWidth = 40.000000
				BREAK
				CASE 2
					vMin = <<-742.955811,5546.903809,22.604477>>
					vMax = <<-774.377563,5529.258301,52.476028>>
					fWidth = 25.000000
				BREAK
				CASE 3
					vMin = <<-777.961548,5535.595215,22.604042>>
					vMax = <<-776.951416,5590.397461,52.485695>>
					fWidth = 25.000000
				BREAK
			ENDSWITCH
		BREAK
		CASE CASTLE_AREA_OBSERVATORY
			SWITCH iSpawnArea
				CASE 0
					vMin = <<-435.651001,1116.253906,315.768036>>
					vMax = <<-424.683472,1160.606079,344.903900>>
					fWidth = 50.000000
				BREAK
				CASE 1
					vMin = <<-419.847809,1158.815552,314.860596>>
					vMax = <<-394.071411,1235.371704,344.641571>>
					fWidth = 45.000000
				BREAK
				CASE 2
					vMin = <<-465.618958,1191.162720,314.473145>>
					vMax = <<-427.457916,1187.662964,344.641571>>
					fWidth = 25.000000
				BREAK
				CASE 3
					vMin = <<-372.856873,1211.112183,316.644043>>
					vMax = <<-327.537842,1195.219604,341.390137>>
					fWidth = 35.000000
				BREAK
			ENDSWITCH
		BREAK
		CASE CASTLE_AREA_SCRAP_YARD		
			SWITCH iSpawnArea
				CASE 0
					vMin = <<458.320831,-1310.049805,-11.717857>>
					vMax = <<486.683380,-1335.440063,35.789238>>
					fWidth = 10.000000
				BREAK
				CASE 1
					vMin = <<488.302765,-1336.539063,-11.701889>>
					vMax = <<507.593933,-1315.068115,35.781502>>
					fWidth = 20.000000
				BREAK
				CASE 2
					vMin = <<443.446899,-1304.968628,-10.676559>>
					vMax = <<409.397583,-1279.366333,36.769272>>
					fWidth = 15.000000
				BREAK
				CASE 3
					vMin = <<422.306732,-1279.385498,-10.735863>>
					vMax = <<447.110901,-1287.214722,36.871071>>
					fWidth = 15.000000
				BREAK
			ENDSWITCH
		BREAK
		CASE CASTLE_AREA_PLAYBOY_GROTTO	
			SWITCH iSpawnArea
				CASE 0
					vMin = <<-1413.713013,230.975174,18.571609>>
					vMax = <<-1433.787231,258.248474,69.722748>>
					fWidth = 25.000000
				BREAK
				CASE 1
					vMin = <<-1431.786987,144.023926,13.127708>>
					vMax = <<-1404.448486,187.682892,67.423134>>
					fWidth = 37.500000
				BREAK
				CASE 2
					vMin = <<-1488.216431,54.262108,13.038620>>
					vMax = <<-1450.508545,57.427700,61.394867>>
					fWidth = 15.000000
				BREAK
				CASE 3
					vMin = <<-1638.114990,77.175896,21.614952>>
					vMax = <<-1600.567383,55.059853,69.537941>>
					fWidth = 20.000000
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
ENDPROC

PROC GET_CASTLE_AREA_SPAWN_OCCLUSION_ANGLED_AREA_DATA(INT iSeed, INT iLocate, VECTOR &vMin, VECTOR &vMax, FLOAT &fWidth)
	
	SWITCH iSeed
	
		CASE CASTLE_AREA_BEACH_TOWER
		
			SWITCH iLocate
				CASE 0
					vMin = << -1234.600, -1815.300, 0.000 >>
					vMax = << -1153.700, -1758.700, 20.000 >>
					fWidth = 120.000
				BREAK
				CASE 1
					vMin = << -1167.800, -1808.400, 0.000 >>
					vMax = << -1047.700, -1920.500, 20.000 >>
					fWidth = 500.0
				BREAK
			ENDSWITCH
			
		BREAK
		
		CASE CASTLE_AREA_FIRE_STATION
			
			SWITCH iLocate
				CASE 0
					vMin = <<-419.056183,6116.040039,30.542007>>
					vMax = <<-367.991943,6167.362793,35.379303>>
					fWidth = 18.000000
				BREAK
				CASE 1
					vMin = <<-433.421600,6083.794922,30.213593>>
					vMax = <<-418.049866,6033.327637,35.327545>>
					fWidth = 30.000000
				BREAK
			ENDSWITCH
			
		BREAK
		
		CASE CASTLE_AREA_ALIEN_HIPPY_HILL
			
			SWITCH iLocate
				CASE 0
					vMin = << 2478.000, 3710.100, 25.0 >>
					vMax = << 2510.200, 3842.900, 60.000 >>
					fWidth = 60.0
				BREAK
			ENDSWITCH
			
		BREAK
			
		CASE CASTLE_AREA_LAND_ACT_DAM	
			
			SWITCH iLocate
				CASE 0
					vMin = <<1464.566895,12.293417,133.240219>>
					vMax = <<1705.170654,158.197998,246.618256>>
					fWidth = 100.000000
				BREAK
				CASE 1
					vMin = <<1649.848389,-23.973900,102.215378>>
					vMax = <<1509.272705,-48.354912,181.470963>>
					fWidth = 60.000000
				BREAK
				CASE 2
					vMin = <<1502.811768,-168.371094,146.060410>>
					vMax = <<1731.251099,-174.828888,309.768524>>
					fWidth = 120.000000
				BREAK
			ENDSWITCH
			
		BREAK
		
		CASE CASTLE_AREA_CABLE_CAR_STATION	
			
			SWITCH iLocate
				CASE 0
					vMin = <<-667.110474,5549.823242,27.484978>>
					vMax = <<-802.937012,5466.949219,42.920204>>
					fWidth = 50.000000
				BREAK
				CASE 1
					vMin = <<-645.469788,5541.823242,27.518860>>
					vMax = <<-695.415222,5619.520508,40.602722>>
					fWidth = 50.000000
				BREAK
				CASE 2
					vMin = <<-871.736511,5603.768555,-5.027634>>
					vMax = <<-803.515381,5472.693359,52.936203>>
					fWidth = 60.000000
				BREAK
			ENDSWITCH
			
		BREAK
		
		CASE CASTLE_AREA_OBSERVATORY
			
			SWITCH iLocate
				CASE 0
					vMin = <<-320.335663,944.179504,187.655792>>
					vMax = <<-641.592712,1035.758911,347.567230>>
					fWidth = 160.000000
				BREAK
				CASE 1
					vMin = <<-523.904907,1142.648315,279.788361>>
					vMax = <<-481.443176,1130.882568,339.096313>>
					fWidth = 80.000000
				BREAK
				CASE 2
					vMin = <<-599.515137,1331.258301,257.327545>>
					vMax = <<-370.480255,1279.798218,357.227844>>
					fWidth = 80.000000
				BREAK
				CASE 3
					vMin = <<-379.555237,997.802124,234.014496>>
					vMax = <<-285.671112,1192.552490,336.891113>>
					fWidth = 80.000000
				BREAK
			ENDSWITCH
			
		BREAK
				
		CASE CASTLE_AREA_SCRAP_YARD		
			
			SWITCH iLocate
				CASE 0
					vMin = <<559.680420,-1324.259888,-26.731071>>
					vMax = <<458.829712,-1417.004150,43.341690>>
					fWidth = 60.000000
				BREAK
				CASE 1
					vMin = <<446.758179,-1278.799438,-10.652725>>
					vMax = <<490.253510,-1279.105957,45.703926>>
					fWidth = 40.000000
				BREAK
				CASE 2
					vMin = <<576.278137,-1048.417358,8.357483>>
					vMax = <<647.928833,-1520.915527,48.708973>>
					fWidth = 170.000000
				BREAK
				CASE 3
					vMin = <<185.398468,-1203.645752,36.046143>>
					vMax = <<814.618286,-1195.951904,84.484985>>
					fWidth = 90.000000
				BREAK
			ENDSWITCH
			
		BREAK
			
		CASE CASTLE_AREA_PLAYBOY_GROTTO	
			
			SWITCH iLocate
				CASE 0
					vMin = <<-1472.275635,253.080109,20.625763>>
					vMax = <<-1544.572998,176.344025,71.477783>>
					fWidth = 40.000000
				BREAK
				CASE 1
					vMin = <<-1435.753174,144.426605,12.880310>>
					vMax = <<-1431.234863,46.565567,66.455811>>
					fWidth = 50.000000
				BREAK
				CASE 2
					vMin = <<-1413.014282,59.438416,12.324799>>
					vMax = <<-1513.969116,57.445824,69.808594>>
					fWidth = 50.000000
				BREAK
				CASE 3
					vMin = <<-1497.228638,135.685822,14.653244>>
					vMax = <<-1610.698486,42.770973,100.756699>>
					fWidth = 100.000000
				BREAK
			ENDSWITCH
			
		BREAK
		
	ENDSWITCH
	
ENDPROC

FUNC VECTOR GET_CASTLE_AREA_MIN_COORDS_FROM_SEED(INT iCastleArea, INT iLocate)
	
	SWITCH iLocate
		CASE 0
			SWITCH GET_CASTLE_AREA_SEED(iCastleArea)
				CASE CASTLE_AREA_BEACH_TOWER 		RETURN <<-1212.380615,-1780.108276,7.481341>>
				CASE CASTLE_AREA_FIRE_STATION 		RETURN <<-381.232758,6089.680664,34.377781>>
				CASE CASTLE_AREA_ALIEN_HIPPY_HILL 	RETURN <<2516.819824,3795.645508,49.829475>>
				CASE CASTLE_AREA_LAND_ACT_DAM		RETURN <<1660.531738,-27.948818,181.587021>>
				CASE CASTLE_AREA_CABLE_CAR_STATION	RETURN <<-736.018311,5595.020020,40.531216>>
				CASE CASTLE_AREA_OBSERVATORY		RETURN <<-386.508026,1144.122314,321.579193>>
				CASE CASTLE_AREA_SCRAP_YARD			RETURN <<479.062714,-1343.185669,34.141537>>
				CASE CASTLE_AREA_PLAYBOY_GROTTO		RETURN << 0.0, 0.0, 0.0 >>
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH GET_CASTLE_AREA_SEED(iCastleArea)
				CASE CASTLE_AREA_BEACH_TOWER 		RETURN <<-1209.829712,-1798.181152,7.481341>>
				CASE CASTLE_AREA_FIRE_STATION 		RETURN << 0.0, 0.0, 0.0 >>
				CASE CASTLE_AREA_ALIEN_HIPPY_HILL 	RETURN << 0.0, 0.0, 0.0 >>
				CASE CASTLE_AREA_LAND_ACT_DAM		RETURN <<1669.613403,-27.267546,182.287430>>
				CASE CASTLE_AREA_CABLE_CAR_STATION	RETURN << 0.0, 0.0, 0.0 >>
				CASE CASTLE_AREA_OBSERVATORY		RETURN << 0.0, 0.0, 0.0 >>
				CASE CASTLE_AREA_SCRAP_YARD			RETURN << 0.0, 0.0, 0.0 >>
				CASE CASTLE_AREA_PLAYBOY_GROTTO		RETURN << 0.0, 0.0, 0.0 >>
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH GET_CASTLE_AREA_SEED(iCastleArea)
				CASE CASTLE_AREA_BEACH_TOWER 		RETURN <<-1197.332764,-1769.584351,7.474364>>
				CASE CASTLE_AREA_FIRE_STATION 		RETURN << 0.0, 0.0, 0.0 >>
				CASE CASTLE_AREA_ALIEN_HIPPY_HILL 	RETURN << 0.0, 0.0, 0.0 >>
				CASE CASTLE_AREA_LAND_ACT_DAM		RETURN << 0.0, 0.0, 0.0 >>
				CASE CASTLE_AREA_CABLE_CAR_STATION	RETURN << 0.0, 0.0, 0.0 >>
				CASE CASTLE_AREA_OBSERVATORY		RETURN << 0.0, 0.0, 0.0 >>
				CASE CASTLE_AREA_SCRAP_YARD			RETURN << 0.0, 0.0, 0.0 >>
				CASE CASTLE_AREA_PLAYBOY_GROTTO		RETURN << 0.0, 0.0, 0.0 >>
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD 
	SCRIPT_ASSERT("King Castle - GET_CASTLE_AREA_MIN_COORDS_FROM_SEED invalid seed.")
	#ENDIF
	RETURN << 0.0, 0.0, 0.0 >>
	
ENDFUNC

FUNC VECTOR GET_CASTLE_AREA_MAX_COORDS_FROM_SEED(INT iCastleArea, INT iLocate)
	
	SWITCH iLocate
		CASE 0
			SWITCH GET_CASTLE_AREA_SEED(iCastleArea)
				CASE CASTLE_AREA_BEACH_TOWER 		RETURN <<-1178.575073,-1795.218872,22.428284>>
				CASE CASTLE_AREA_FIRE_STATION 		RETURN <<-377.419281,6083.058105,46.635361>>
				CASE CASTLE_AREA_ALIEN_HIPPY_HILL 	RETURN <<2518.118408,3772.497803,58.135464>>
				CASE CASTLE_AREA_LAND_ACT_DAM		RETURN <<1673.974731,-24.626637,199.256317>>
				CASE CASTLE_AREA_CABLE_CAR_STATION	RETURN <<-756.487671,5594.868164,44.170082>>
				CASE CASTLE_AREA_OBSERVATORY		RETURN <<-391.538116,1125.141479,325.028839>>
				CASE CASTLE_AREA_SCRAP_YARD			RETURN <<470.238831,-1335.767822,38.441536>>
				CASE CASTLE_AREA_PLAYBOY_GROTTO		RETURN << 0.0, 0.0, 0.0 >>
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH GET_CASTLE_AREA_SEED(iCastleArea)
				CASE CASTLE_AREA_BEACH_TOWER 		RETURN <<-1180.375244,-1777.347778,22.481255>>
				CASE CASTLE_AREA_FIRE_STATION 		RETURN << 0.0, 0.0, 0.0 >>
				CASE CASTLE_AREA_ALIEN_HIPPY_HILL 	RETURN << 0.0, 0.0, 0.0 >>
				CASE CASTLE_AREA_LAND_ACT_DAM		RETURN <<1676.202515,-25.814840,200.180191>>
				CASE CASTLE_AREA_CABLE_CAR_STATION	RETURN << 0.0, 0.0, 0.0 >>
				CASE CASTLE_AREA_OBSERVATORY		RETURN << 0.0, 0.0, 0.0 >>
				CASE CASTLE_AREA_SCRAP_YARD			RETURN << 0.0, 0.0, 0.0 >>
				CASE CASTLE_AREA_PLAYBOY_GROTTO		RETURN << 0.0, 0.0, 0.0 >>
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH GET_CASTLE_AREA_SEED(iCastleArea)
				CASE CASTLE_AREA_BEACH_TOWER 		RETURN <<-1194.205078,-1806.819824,22.480000>>
				CASE CASTLE_AREA_FIRE_STATION 		RETURN << 0.0, 0.0, 0.0 >>
				CASE CASTLE_AREA_ALIEN_HIPPY_HILL 	RETURN << 0.0, 0.0, 0.0 >>
				CASE CASTLE_AREA_LAND_ACT_DAM		RETURN << 0.0, 0.0, 0.0 >>
				CASE CASTLE_AREA_CABLE_CAR_STATION	RETURN << 0.0, 0.0, 0.0 >>
				CASE CASTLE_AREA_OBSERVATORY		RETURN << 0.0, 0.0, 0.0 >>
				CASE CASTLE_AREA_SCRAP_YARD			RETURN << 0.0, 0.0, 0.0 >>
				CASE CASTLE_AREA_PLAYBOY_GROTTO		RETURN << 0.0, 0.0, 0.0 >>
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD 
	SCRIPT_ASSERT("King Castle - GET_CASTLE_AREA_MAX_COORDS_FROM_SEED invalid seed.")
	#ENDIF
	RETURN << 0.0, 0.0, 0.0 >>
	
ENDFUNC

FUNC FLOAT GET_CASTLE_AREA_WIDTH_FROM_SEED(INT iCastleArea, INT iLocate)
	
	SWITCH iLocate
		CASE 0
			SWITCH GET_CASTLE_AREA_SEED(iCastleArea)
				CASE CASTLE_AREA_BEACH_TOWER 		RETURN 23.000000
				CASE CASTLE_AREA_FIRE_STATION 		RETURN 6.000000
				CASE CASTLE_AREA_ALIEN_HIPPY_HILL 	RETURN 7.000000
				CASE CASTLE_AREA_LAND_ACT_DAM		RETURN 8.250000
				CASE CASTLE_AREA_CABLE_CAR_STATION	RETURN 16.500000
				CASE CASTLE_AREA_OBSERVATORY		RETURN 9.650000
				CASE CASTLE_AREA_SCRAP_YARD			RETURN 9.150000
				CASE CASTLE_AREA_PLAYBOY_GROTTO		RETURN 0.0
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH GET_CASTLE_AREA_SEED(iCastleArea)
				CASE CASTLE_AREA_BEACH_TOWER 		RETURN 23.000000
				CASE CASTLE_AREA_FIRE_STATION 		RETURN 0.0
				CASE CASTLE_AREA_ALIEN_HIPPY_HILL 	RETURN 0.0
				CASE CASTLE_AREA_LAND_ACT_DAM		RETURN 3.500000
				CASE CASTLE_AREA_CABLE_CAR_STATION	RETURN 0.0
				CASE CASTLE_AREA_OBSERVATORY		RETURN 0.0
				CASE CASTLE_AREA_SCRAP_YARD			RETURN 0.0
				CASE CASTLE_AREA_PLAYBOY_GROTTO		RETURN 0.0
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH GET_CASTLE_AREA_SEED(iCastleArea)
				CASE CASTLE_AREA_BEACH_TOWER 		RETURN 23.000000
				CASE CASTLE_AREA_FIRE_STATION 		RETURN 0.0
				CASE CASTLE_AREA_ALIEN_HIPPY_HILL 	RETURN 0.0
				CASE CASTLE_AREA_LAND_ACT_DAM		RETURN 0.0
				CASE CASTLE_AREA_CABLE_CAR_STATION	RETURN 0.0
				CASE CASTLE_AREA_OBSERVATORY		RETURN 0.0
				CASE CASTLE_AREA_SCRAP_YARD			RETURN 0.0
				CASE CASTLE_AREA_PLAYBOY_GROTTO		RETURN 0.0
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD 
	SCRIPT_ASSERT("King Castle - GET_CASTLE_AREA_WIDTH_FROM_SEED invalid seed.")
	#ENDIF
	RETURN 0.0
	
ENDFUNC

PROC GET_CIRCULAR_LOCATE_DATA_FROM_SEED(INT iCastleArea, INT iLocate, VECTOR &vCenterPoint, FLOAT &fRadius)
	
	SWITCH iLocate
		CASE 0
			SWITCH GET_CASTLE_AREA_SEED(iCastleArea)
				CASE CASTLE_AREA_BEACH_TOWER
					vCenterPoint = << 0.0, 0.0, 0.0 >>
					fRadius = 0.0
				BREAK
				CASE CASTLE_AREA_FIRE_STATION 		
					vCenterPoint = << 0.0, 0.0, 0.0 >>
					fRadius = 0.0
				BREAK
				CASE CASTLE_AREA_ALIEN_HIPPY_HILL 	
					vCenterPoint = << 2515.966, 3777.477, 51.767 >>
					fRadius = 5.3
				BREAK
				CASE CASTLE_AREA_LAND_ACT_DAM		
					vCenterPoint = << 0.0, 0.0, 0.0 >>
					fRadius = 0.0
				BREAK
				CASE CASTLE_AREA_CABLE_CAR_STATION	
					vCenterPoint = << 0.0, 0.0, 0.0 >>
					fRadius = 0.0
				BREAK
				CASE CASTLE_AREA_OBSERVATORY	
					vCenterPoint = << 0.0, 0.0, 0.0 >>
					fRadius = 0.0
				BREAK	
				CASE CASTLE_AREA_SCRAP_YARD			
					vCenterPoint = << 0.0, 0.0, 0.0 >>
					fRadius = 0.0
				BREAK
				CASE CASTLE_AREA_PLAYBOY_GROTTO	
					vCenterPoint = << 0.0, 0.0, 0.0 >>
					fRadius = 0.0
				BREAK	
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH GET_CASTLE_AREA_SEED(iCastleArea)
				CASE CASTLE_AREA_BEACH_TOWER
					vCenterPoint = << 0.0, 0.0, 0.0 >>
					fRadius = 0.0
				BREAK
				CASE CASTLE_AREA_FIRE_STATION 		
					vCenterPoint = << 0.0, 0.0, 0.0 >>
					fRadius = 0.0
				BREAK
				CASE CASTLE_AREA_ALIEN_HIPPY_HILL 	
					vCenterPoint = << 2515.315, 3789.364, 52.088 >>
					fRadius = 3.35
				BREAK
				CASE CASTLE_AREA_LAND_ACT_DAM		
					vCenterPoint = << 0.0, 0.0, 0.0 >>
					fRadius = 0.0
				BREAK
				CASE CASTLE_AREA_CABLE_CAR_STATION	
					vCenterPoint = << 0.0, 0.0, 0.0 >>
					fRadius = 0.0
				BREAK
				CASE CASTLE_AREA_OBSERVATORY	
					vCenterPoint = << 0.0, 0.0, 0.0 >>
					fRadius = 0.0
				BREAK	
				CASE CASTLE_AREA_SCRAP_YARD			
					vCenterPoint = << 0.0, 0.0, 0.0 >>
					fRadius = 0.0
				BREAK
				CASE CASTLE_AREA_PLAYBOY_GROTTO	
					vCenterPoint = << 0.0, 0.0, 0.0 >>
					fRadius = 0.0
				BREAK	
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH GET_CASTLE_AREA_SEED(iCastleArea)
				CASE CASTLE_AREA_BEACH_TOWER
					vCenterPoint = << 0.0, 0.0, 0.0 >>
					fRadius = 0.0
				BREAK
				CASE CASTLE_AREA_FIRE_STATION 		
					vCenterPoint = << 0.0, 0.0, 0.0 >>
					fRadius = 0.0
				BREAK
				CASE CASTLE_AREA_ALIEN_HIPPY_HILL 	
					vCenterPoint = << 0.0, 0.0, 0.0 >>
					fRadius = 0.0
				BREAK
				CASE CASTLE_AREA_LAND_ACT_DAM		
					vCenterPoint = << 0.0, 0.0, 0.0 >>
					fRadius = 0.0
				BREAK
				CASE CASTLE_AREA_CABLE_CAR_STATION	
					vCenterPoint = << 0.0, 0.0, 0.0 >>
					fRadius = 0.0
				BREAK
				CASE CASTLE_AREA_OBSERVATORY	
					vCenterPoint = << 0.0, 0.0, 0.0 >>
					fRadius = 0.0
				BREAK	
				CASE CASTLE_AREA_SCRAP_YARD			
					vCenterPoint = << 0.0, 0.0, 0.0 >>
					fRadius = 0.0
				BREAK
				CASE CASTLE_AREA_PLAYBOY_GROTTO	
					vCenterPoint = << 0.0, 0.0, 0.0 >>
					fRadius = 0.0
				BREAK	
			ENDSWITCH
		BREAK
	ENDSWITCH
					
ENDPROC

FUNC VECTOR GET_CASTLE_AREA_BLIP_COORDS_FROM_SEED(INT iCastleArea)

	SWITCH GET_CASTLE_AREA_SEED(iCastleArea)
		CASE CASTLE_AREA_BEACH_TOWER 		RETURN <<-1195.4352, -1788.2170, 14.6269>>
		CASE CASTLE_AREA_FIRE_STATION 		RETURN <<-379.8141, 6087.2153, 43.2552>>
		CASE CASTLE_AREA_ALIEN_HIPPY_HILL 	RETURN <<2516.5901, 3784.6768, 52.0053>>
		CASE CASTLE_AREA_LAND_ACT_DAM		RETURN <<1667.1927, -27.4697, 183.7740>>
		CASE CASTLE_AREA_CABLE_CAR_STATION	RETURN << -746.2, 5594.6, 41.7 >>
		CASE CASTLE_AREA_OBSERVATORY		RETURN << -389.3, 1135.7, 322.6 >>
		CASE CASTLE_AREA_SCRAP_YARD		 	RETURN << 473.6, -1339.1, 35.2 >>
		CASE CASTLE_AREA_PLAYBOY_GROTTO		RETURN << -1459.651, 179.275, 53.000 >> 
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD 
	SCRIPT_ASSERT("King Castle - GET_CASTLE_AREA_BLIP_COORDS_FROM_SEED invalid seed.")
	#ENDIF
	
	RETURN << 0.0, 0.0, 0.0 >>
	
ENDFUNC

FUNC eGAME_STATE GET_SERVER_GAME_STATE()
	RETURN serverBD.eGameState
ENDFUNC

PROC SET_SERVER_GAME_STATE(eGAME_STATE eState)

	#IF IS_DEBUG_BUILD
	IF eState != GET_SERVER_GAME_STATE()
		STRING strStateName = GET_GAME_STATE_NAME(eState)
		PRINTLN("[KINGCASTLE] - [GAME STATE] - server going to game state ", strStateName)
	ENDIF
	#ENDIF
	
	serverBD.eGameState = eState
	
ENDPROC

FUNC eGAME_STATE GET_CLIENT_GAME_STATE(INT iParticipant)
	RETURN playerBD[iParticipant].eGameState
ENDFUNC

PROC SET_CLIENT_GAME_STATE(eGAME_STATE eState)

	#IF IS_DEBUG_BUILD
	IF eState != GET_CLIENT_GAME_STATE(PARTICIPANT_ID_TO_INT())
		STRING strStateName = GET_GAME_STATE_NAME(eState)
		PRINTLN("[KINGCASTLE] - [GAME STATE] - local client going to game state ", strStateName)
	ENDIF
	#ENDIF
	
	playerBD[PARTICIPANT_ID_TO_INT()].eGameState = eState
	
ENDPROC

#IF IS_DEBUG_BUILD 
FUNC STRING GET_KING_OF_THE_CASTLE_STATE_NAME(eKING_OF_THE_CASTLE_STATE eState)
	
	SWITCH eState
		CASE eKINGOFTHECASTLESTATE_INITIALISE			RETURN "eKINGOFTHECASTLESTATE_INITIALISE"
		CASE eKINGOFTHECASTLESTATE_FIGHT_OVER_CASTLE	RETURN "eKINGOFTHECASTLESTATE_FIGHT_OVER_CASTLE"
		CASE eKINGOFTHECASTLESTATE_TIME_UP				RETURN "eKINGOFTHECASTLESTATE_TIME_UP"
		CASE eKINGOFTHECASTLESTATE_CLEANUP				RETURN "eKINGOFTHECASTLESTATE_CLEANUP"
		CASE eKINGOFTHECASTLESTATE_END					RETURN "eKINGOFTHECASTLESTATE_END"
	ENDSWITCH
	
	RETURN "INVALID"
	
ENDFUNC
#ENDIF

FUNC eKING_OF_THE_CASTLE_STATE GET_KING_OF_THE_CASTLE_STATE()
	RETURN serverBD.sKingOfTheCastle.eState
ENDFUNC

PROC SET_KING_OF_THE_CASTLE_STATE(eKING_OF_THE_CASTLE_STATE eState)
	
	#IF IS_DEBUG_BUILD
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		TEXT_LABEL_63 errorMsg = "King Castle - Client set mode state. Client,Area = "
		errorMsg += PARTICIPANT_ID_TO_INT()
		SCRIPT_ASSERT(errorMsg)
	ENDIF
	IF eState != GET_KING_OF_THE_CASTLE_STATE()
		STRING strStateName = GET_KING_OF_THE_CASTLE_STATE_NAME(eState)
		PRINTLN("[KINGCASTLE] - [MODE STATE] - mode state going to ", strStateName)
	ENDIF
	#ENDIF
	
	serverBD.sKingOfTheCastle.eState = eState	
	
ENDPROC

PROC CLEANUP_KING_OF_THE_CASTLE_HELP()
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("KOTC_1STHELP")
		CLEAR_HELP()
		PRINTLN("[KINGCASTLE] - [HELP TEXT] - CLEANUP_KING_OF_THE_CASTLE_HELP has cleared help msg KOTC_1STHELP")
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("KOTC_OTHRKG")
		CLEAR_HELP()
		PRINTLN("[KINGCASTLE] - [HELP TEXT] - CLEANUP_KING_OF_THE_CASTLE_HELP has cleared help msg KOTC_OTHRKG")
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("KOTC_YOUKNG")
		CLEAR_HELP()
		PRINTLN("[KINGCASTLE] - [HELP TEXT] - CLEANUP_KING_OF_THE_CASTLE_HELP has cleared help msg KOTC_YOUKNG")
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("KOTC_CONTS")
		CLEAR_HELP()
		PRINTLN("[KINGCASTLE] - [HELP TEXT] - CLEANUP_KING_OF_THE_CASTLE_HELP has cleared help msg KOTC_CONTS")
	ENDIF
	
ENDPROC

FUNC eMINIMAP_COMPONENT GET_CASTLE_AREA_RADAR_COMPONENT(INT iCastleArea, BOOL bHippyCirclesOverride = FALSE)
	
	SWITCH GET_CASTLE_AREA_SEED(iCastleArea)
	
		CASE CASTLE_AREA_BEACH_TOWER 		RETURN MINIMAP_COMPONENT_KING_HILL_1
		CASE CASTLE_AREA_LAND_ACT_DAM		RETURN MINIMAP_COMPONENT_KING_HILL_4			
		CASE CASTLE_AREA_FIRE_STATION		RETURN MINIMAP_COMPONENT_KING_HILL_3			
		CASE CASTLE_AREA_CABLE_CAR_STATION	RETURN MINIMAP_COMPONENT_KING_HILL_5
		CASE CASTLE_AREA_OBSERVATORY 		RETURN MINIMAP_COMPONENT_KING_HILL_6			
		CASE CASTLE_AREA_SCRAP_YARD			RETURN MINIMAP_COMPONENT_KING_HILL_7	
		CASE CASTLE_AREA_PLAYBOY_GROTTO		RETURN MINIMAP_COMPONENT_KING_HILL_8
		
		CASE CASTLE_AREA_ALIEN_HIPPY_HILL		
			IF NOT bHippyCirclesOverride
				RETURN MINIMAP_COMPONENT_KING_HILL_2
			ELSE
				RETURN MINIMAP_COMPONENT_KING_HILL_9
			ENDIF
		BREAK
		
	ENDSWITCH
	
	SCRIPT_ASSERT("[HUNTBEAST] - no valid entry found in minimap component lookup.")
	RETURN MINIMAP_COMPONENT_KING_HILL_1
	
ENDFUNC

FUNC HUD_COLOURS GET_CASTLE_AREA_BLIP_COLOUR(INT iCastleArea, BOOL bForRadius = TRUE)
	
	IF GET_CASTLE_AREA_KING_PARTICIPANT_ID(iCastleArea) <= (-1)
		IF bForRadius
			RETURN HUD_COLOUR_PURPLE
		ENDIF
		RETURN HUD_COLOUR_PURPLE
	ELSE
		IF GET_CASTLE_AREA_KING_PARTICIPANT_ID(iCastleArea) = PARTICIPANT_ID_TO_INT()
			RETURN HUD_COLOUR_BLUE
		ELSE
			IF GB_ARE_PLAYERS_IN_SAME_GANG(PLAYER_ID(), INT_TO_NATIVE(PLAYER_INDEX, GET_CASTLE_AREA_KING_PLAYER_ID(iCastleArea)))
				RETURN GET_PLAYER_HUD_COLOUR(PLAYER_ID())
			ENDIF
		ENDIF
	ENDIF
	
	RETURN HUD_COLOUR_RED
	
ENDFUNC

PROC ACTIVATE_CASTLE_AREA_MINIMAP_COMPONENT(INT iCastleArea)
	
	IF NOT IS_BIT_SET(iMinimapComponentBitset, GET_CASTLE_AREA_SEED(iCastleArea))
		SET_MINIMAP_COMPONENT(GET_CASTLE_AREA_RADAR_COMPONENT(iCastleArea), TRUE, GET_CASTLE_AREA_BLIP_COLOUR(iCastleArea))
		IF GET_CASTLE_AREA_SEED(iCastleArea) = CASTLE_AREA_ALIEN_HIPPY_HILL
			SET_MINIMAP_COMPONENT(GET_CASTLE_AREA_RADAR_COMPONENT(iCastleArea, TRUE), TRUE, GET_CASTLE_AREA_BLIP_COLOUR(iCastleArea))
		ENDIF
		SET_BIT(iMinimapComponentBitset, GET_CASTLE_AREA_SEED(iCastleArea))
		#IF IS_DEBUG_BUILD
		eMINIMAP_COMPONENT eComp = GET_CASTLE_AREA_RADAR_COMPONENT(iCastleArea)
		INT iComp = ENUM_TO_INT(eComp)
		PRINTLN("[HUNTBEAST] - activated castle area minimap component #", iComp)
		#ENDIF
	ENDIF
	
ENDPROC

PROC DEACTIVATE_CASTLE_AREA_MINIMAP_COMPONENT(INT iCastleArea, INT iSeedOverride = -1)
	
	INT i = GET_CASTLE_AREA_SEED(iCastleArea)
	
	IF iSeedOverride != (-1)
		i = iSeedOverride
	ENDIF
	
	IF IS_BIT_SET(iMinimapComponentBitset, i)
		SET_MINIMAP_COMPONENT(GET_CASTLE_AREA_RADAR_COMPONENT(iCastleArea), FALSE, GET_CASTLE_AREA_BLIP_COLOUR(iCastleArea))
		IF i = CASTLE_AREA_ALIEN_HIPPY_HILL
			SET_MINIMAP_COMPONENT(GET_CASTLE_AREA_RADAR_COMPONENT(iCastleArea, TRUE), FALSE, GET_CASTLE_AREA_BLIP_COLOUR(iCastleArea))
		ENDIF
		CLEAR_BIT(iMinimapComponentBitset, i)
		#IF IS_DEBUG_BUILD
		eMINIMAP_COMPONENT eComp = GET_CASTLE_AREA_RADAR_COMPONENT(iCastleArea)
		INT iComp = ENUM_TO_INT(eComp)
		PRINTLN("[HUNTBEAST] - deactivated castle area minimap component #", iComp)
		#ENDIF
	ENDIF
	
ENDPROC

PROC DEACTIVATE_ALL_CASTLE_AREAS_MINIMAP_COMPONENTS()
	
	INT iCastleArea, i
	
	REPEAT NUM_CASTLE_AREAS iCastleArea
		REPEAT NUM_CASTLE_AREA_VARIATIONS i
			DEACTIVATE_CASTLE_AREA_MINIMAP_COMPONENT(iCastleArea, i)
		ENDREPEAT
	ENDREPEAT
	
ENDPROC

FUNC BOOL IS_LOCAL_PARTICIPANT_RESTRICTED(BOOL bPassiveCheck = FALSE, BOOL bSpectatorCheck = FALSE)
	
	IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(PLAYER_ID()) 
		RETURN TRUE
	ENDIF
	
	IF bPassiveCheck
		IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID())
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF bSpectatorCheck
		IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_SCTV(PLAYER_ID())
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_HIDE_THIS_AMBIENT_EVENT(BOOL bPassiveCheck = FALSE, BOOL bSpectatorCheck = FALSE)
	
	#IF IS_DEBUG_BUILD
	IF bFakeHideEventNotPassiveCheck
		RETURN TRUE
	ENDIF
	#ENDIF
	 
	IF bPassiveCheck
		#IF IS_DEBUG_BUILD
		IF bFakeHideEventWithPassiveCheck
			RETURN TRUE
		ENDIF
		#ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_UI_END_TIME_ELAPSED))
		RETURN TRUE
	ENDIF
	
	//Fix for bug url:bugstar:2410997
	IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE)
		RETURN TRUE
	ENDIF
//
//	IF IS_RADAR_HIDDEN()
//		RETURN TRUE
//	ENDIF
	
	IF IS_PLAYER_IN_CORONA()
		RETURN TRUE
	ENDIF
	
	IF SHOULD_HIDE_AMBIENT_EVENT(ciPI_HIDE_MENU_ITEM_AMBIENT_EVENTS_KING_OF_THE_CASTLE)
		RETURN TRUE
	ENDIF
	
	IF NOT IS_IT_SAFE_TO_DISPLAY_FM_EVENT_UI()
		RETURN TRUE
	ENDIF
	
	IF SHOULD_FM_EVENT_DROP_TO_IDLE_STATE()
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_BROWSING_ITEMS_IN_SHOP_OF_TYPE(SHOP_TYPE_CARMOD)
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
		RETURN TRUE
	ENDIF
	
	IF IS_LOCAL_PARTICIPANT_RESTRICTED(bPassiveCheck, bSpectatorCheck)
		RETURN TRUE
	ENDIF
	
	IF g_bCelebrationScreenIsActive
		RETURN TRUE
	ENDIF
	
	IF IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC MAINTAIN_BROADCAST_AMBIENT_EVENT_UI_SHOULD_BE_HIDDEN()
	
	IF SHOULD_HIDE_THIS_AMBIENT_EVENT(TRUE, TRUE)
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_AMBIENT_EVENT_SHOULD_BE_HIDDEN))
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_AMBIENT_EVENT_SHOULD_BE_HIDDEN))
			PRINTLN("[KINGCASTLE] - Set bit: eCLIENTBITSET0_AMBIENT_EVENT_SHOULD_BE_HIDDEN.")
		ENDIF
	ELSE
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_AMBIENT_EVENT_SHOULD_BE_HIDDEN))
			CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_AMBIENT_EVENT_SHOULD_BE_HIDDEN))
			PRINTLN("[KINGCASTLE] - Clear bit: eCLIENTBITSET0_AMBIENT_EVENT_SHOULD_BE_HIDDEN.")
		ENDIF
	ENDIF
	
	IF SHOULD_HIDE_THIS_AMBIENT_EVENT(FALSE, TRUE)
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_AMBIENT_EVENT_SHOULD_BE_HIDDEN_NO_PASSIVE_CHECK))
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_AMBIENT_EVENT_SHOULD_BE_HIDDEN_NO_PASSIVE_CHECK))
			PRINTLN("[KINGCASTLE] - Set bit: eCLIENTBITSET0_AMBIENT_EVENT_SHOULD_BE_HIDDEN_NO_PASSIVE_CHECK.")
		ENDIF
	ELSE
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_AMBIENT_EVENT_SHOULD_BE_HIDDEN_NO_PASSIVE_CHECK))
			CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_AMBIENT_EVENT_SHOULD_BE_HIDDEN_NO_PASSIVE_CHECK))
			PRINTLN("[KINGCASTLE] - Clear bit: eCLIENTBITSET0_AMBIENT_EVENT_SHOULD_BE_HIDDEN_NO_PASSIVE_CHECK.")
		ENDIF
	ENDIF
	
	IF SHOULD_HIDE_THIS_AMBIENT_EVENT(TRUE, FALSE)
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_AMBIENT_EVENT_SHOULD_BE_HIDDEN_NO_SPECTATOR_CHECK))
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_AMBIENT_EVENT_SHOULD_BE_HIDDEN_NO_SPECTATOR_CHECK))
			PRINTLN("[KINGCASTLE] - Set bit: eCLIENTBITSET0_AMBIENT_EVENT_SHOULD_BE_HIDDEN_NO_SPECTATOR_CHECK.")
		ENDIF
	ELSE
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_AMBIENT_EVENT_SHOULD_BE_HIDDEN_NO_SPECTATOR_CHECK))
			CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_AMBIENT_EVENT_SHOULD_BE_HIDDEN_NO_SPECTATOR_CHECK))
			PRINTLN("[KINGCASTLE] - Clear bit: eCLIENTBITSET0_AMBIENT_EVENT_SHOULD_BE_HIDDEN_NO_SPECTATOR_CHECK.")
		ENDIF
	ENDIF
	
	IF SHOULD_HIDE_THIS_AMBIENT_EVENT()
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_AMBIENT_EVENT_SHOULD_BE_HIDDEN_NO_SPECTATOR_NO_PASSIVE_CHECK))
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_AMBIENT_EVENT_SHOULD_BE_HIDDEN_NO_SPECTATOR_NO_PASSIVE_CHECK))
			PRINTLN("[KINGCASTLE] - Set bit: eCLIENTBITSET0_AMBIENT_EVENT_SHOULD_BE_HIDDEN_NO_SPECTATOR_NO_PASSIVE_CHECK.")
		ENDIF
	ELSE
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_AMBIENT_EVENT_SHOULD_BE_HIDDEN_NO_SPECTATOR_NO_PASSIVE_CHECK))
			CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_AMBIENT_EVENT_SHOULD_BE_HIDDEN_NO_SPECTATOR_NO_PASSIVE_CHECK))
			PRINTLN("[KINGCASTLE] - Clear bit: eCLIENTBITSET0_AMBIENT_EVENT_SHOULD_BE_HIDDEN_NO_SPECTATOR_NO_PASSIVE_CHECK.")
		ENDIF
	ENDIF
	
ENDPROC

PROC CLEANUP_SPAWN_AREAS(INT iCastleArea)
	
	INT iLocateCount
	
	REPEAT GET_CASTLE_AREA_NUM_SPAWN_OCCLUSION_AREAS(iCastleArea) iLocateCount
		CLEAR_MISSION_SPAWN_OCCLUSION_AREA(iLocateCount)
	ENDREPEAT
	CLEAR_PLAYER_NEXT_RESPAWN_LOCATION()
	CLEAR_SPECIFIC_SPAWN_LOCATION_FROM_THIS_SCRIPT()
	SET_PLAYER_WILL_SPAWN_FACING_COORDS(GET_CASTLE_AREA_BLIP_COORDS(iCastleArea), FALSE, FALSE)
		
ENDPROC

//Cleanup Mission Data
PROC SCRIPT_CLEANUP()
	
	INT iCastleArea
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[KINGCASTLE] - [SCRIPT CLEANUP] - been called. Callstack:")
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	Clear_Any_Objective_Text_From_This_Script()
	CALL_FREEMODE_AMBIENT_EVENTS_TELEMETRY(FMMC_TYPE_KING_CASTLE)
	COMMON_FREEMODE_AMBIENT_EVENTS_CLEAN_UP(FMMC_TYPE_KING_CASTLE, FALSE, IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_GIVEN_EOM_REWARDS)))
	CLEANUP_KING_OF_THE_CASTLE_HELP()
	
	FM_EVENT_SET_PLAYER_IS_PERMANENT_PARTICIPANT_IN_FM_EVENT(FALSE)
	PRINTLN("[KINGCASTLE] - [SCRIPT CLEANUP] - FM_EVENT_SET_PLAYER_IS_PERMANENT_PARTICIPANT_IN_FM_EVENT(FALSE).")
	
	// End of mode, no longer critical. 
	//FM_EVENT_SET_PLAYER_IS_CRITICAL_TO_FM_EVENT(FALSE)
	
	IF IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_AUDIO_SCENE_ACTIVE))
		STOP_AUDIO_SCENE("MP_Deathmatch_Type_Challenge_Scene")
		CLEAR_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_AUDIO_SCENE_ACTIVE))
		PRINTLN("[KINGCASTLE] - [AUDIO] - [SCRIPT CLEANUP] - called STOP_AUDIO_SCENE(MP_Deathmatch_Type_Challenge_Scene)")
		PRINTLN("[KINGCASTLE] - [AUDIO] - [SCRIPT CLEANUP] - cleared bit: iLocalBitset0, eLOCALBITSET0_AUDIO_SCENE_ACTIVE")
	ENDIF
		
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	PRINTLN("[KINGCASTLE] - [SCRIPT CLEANUP] - [WANTED MULTIPLIER] - [SCRIPT CLEANUP] - called SET_WANTED_LEVEL_MULTIPLIER(1.0).")
					
	
	DEACTIVATE_ALL_CASTLE_AREAS_MINIMAP_COMPONENTS()
	
	
	REPEAT NUM_CASTLE_AREAS iCastleArea
		CLEANUP_SPAWN_AREAS(iCastleArea)
	ENDREPEAT
	
	CLEAR_PLAYER_NEXT_RESPAWN_LOCATION()
	CLEAR_SPECIFIC_SPAWN_LOCATION_FROM_THIS_SCRIPT()
	
	SET_FREEMODE_FLOW_COMMS_DISABLED(FALSE)
	PRINTLN("[KINGCASTLE] - [SCRIPT CLEANUP] - called SET_FREEMODE_FLOW_COMMS_DISABLED(FALSE).")
	
	SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_KING_OF_THE_CASTLE, FALSE)
	PRINTLN("[KINGCASTLE] - [SCRIPT CLEANUP] - called SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_KING_OF_THE_CASTLE, FALSE).")
	
	
	IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		telemetryStruct.m_endReason             =AE_END_FORCED
	ELIF IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_LEAVING_FOR_LOW_SESSION_NUMBERS))// SHOULD_BAIL_FM_EVENT_DUE_TO_LOW_SESSION_NUMBERS(DEFAULT, DEFAULT, FALSE)
		telemetryStruct.m_endReason             =AE_END_LOW_NUMBERS
	ELSE
		IF telemetryStruct.m_endReason       	!=AE_END_WON
			telemetryStruct.m_endReason         =AE_END_LOST
		ENDIF
	ENDIF
	
	telemetryStruct.m_uid0 = serverBD.iHashedMac
	telemetryStruct.m_uid1 = serverBD.iMatchHistoryId
	telemetryStruct.m_playersLeftInProgress = serverBD.iPlayersLeftInProgress
	telemetryStruct.m_playersParticipating = iPeakQualifiedParticipants
	telemetryStruct.m_timeTakenToComplete	= GET_CLOUD_TIME_AS_INT() - telemetryStruct.m_startTime
	
	IF NOT g_sMPTunables.bKing_of_the_Castle_Disable_Share_Cash
		IF telemetryStruct.m_cashEarned > 0							
//			SET_LAST_JOB_DATA(LAST_JOB_KING_CASTLE, telemetryStruct.m_cashEarned)
			IF  telemetryStruct.m_endReason       	=AE_END_WON
				SET_DO_FM_EVENT_SHARE_CASH_HELP_IF_NEEDED()
			ENDIF
		ENDIF
	ENDIF
	
	IF PARTICIPANT_ID_TO_INT() != (-1)
		PRINTLN("[KINGCASTLE] - calling PROCESS_CURRENT_AMBIENT_EVENT_PLAYSTATS - telemetryStruct.m_timeTakenForObjective = ", telemetryStruct.m_timeTakenForObjective)
		PROCESS_CURRENT_AMBIENT_EVENT_PLAYSTATS(telemetryStruct, iNumOfKings, iKillsAsKing, iDeathsAsKing, ACCURATLEY_ROUND_SCORE(GET_CASTLE_AREA_KING_TOTAL_POINTS(0, PARTICIPANT_ID_TO_INT())))
	ENDIF
	
	PRINTLN("[KINGCASTLE] - [SCRIPT CLEANUP] - calling TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS() to terminate thread...")
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
	
ENDPROC

/// PURPOSE:
///    Return FALSE if the variation is disabled via a tunable
FUNC BOOL IS_KING_OF_THE_CASTLE_VARIATION_VALID(INT iVar)

	SWITCH iVar
		CASE CASTLE_AREA_BEACH_TOWER 
			IF g_sMPTunables.bKing_of_the_Castle_Disable_variant_1 
				RETURN FALSE
			ENDIF
		BREAK
		CASE CASTLE_AREA_ALIEN_HIPPY_HILL 
			IF g_sMPTunables.bKing_of_the_Castle_Disable_variant_2 
				RETURN FALSE
			ENDIF
		BREAK
		CASE CASTLE_AREA_FIRE_STATION
			IF g_sMPTunables.bKing_of_the_Castle_Disable_variant_3 
				RETURN FALSE
			ENDIF
		BREAK
		CASE CASTLE_AREA_LAND_ACT_DAM
			IF g_sMPTunables.bKing_of_the_Castle_Disable_variant_4 
				RETURN FALSE
			ENDIF
		BREAK
		CASE CASTLE_AREA_CABLE_CAR_STATION
			IF g_sMPTunables.bKing_of_the_Castle_Disable_variant_5 
				RETURN FALSE
			ENDIF
		BREAK
		CASE CASTLE_AREA_OBSERVATORY
			IF g_sMPTunables.bKing_of_the_Castle_Disable_variant_6 
				RETURN FALSE
			ENDIF
		BREAK
		CASE CASTLE_AREA_SCRAP_YARD
			IF g_sMPTunables.bKing_of_the_Castle_Disable_variant_7 
				RETURN FALSE
			ENDIF
		BREAK
		CASE CASTLE_AREA_PLAYBOY_GROTTO
			IF g_sMPTunables.bKing_of_the_Castle_Disable_variant_8 
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Return a valid variation for the hot property event. Debug option can override this.
FUNC INT GET_VALID_KING_OF_THE_CASTLE_VARIATION()

	INT iValidVariations
	INT iVariationsAvailable[NUM_CASTLE_AREA_VARIATIONS]
	
	INT i
	// Collate the valid hot property variants that we can pick from
	FOR i = 0 TO (NUM_CASTLE_AREA_VARIATIONS-1) STEP 1
		
		IF IS_KING_OF_THE_CASTLE_VARIATION_VALID(i)
		AND NOT FM_EVENT_IS_VARIATION_IN_HISTORY_LIST(FMMC_TYPE_KING_CASTLE, i)
			iVariationsAvailable[iValidVariations] = i
			iValidVariations++
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[KINGCASTLE] - GET_VALID_KING_OF_THE_CASTLE_VARIATION - Variation: ", i, " is not valid.")
		#ENDIF
		ENDIF
	ENDFOR

	// Randomise to pick one of these variants
	INT iSeed = iVariationsAvailable[GET_RANDOM_INT_IN_RANGE(0, iValidVariations)]
	PRINTLN("[KINGCASTLE] - [CASTLE AREA COORDS] - seed = ", iSeed, ", iValidVariations = ", iValidVariations)
		
	#IF IS_DEBUG_BUILD
		IF MPGlobalsAmbience.iKotCLocation > (-1)
		AND MPGlobalsAmbience.iKotCLocation < NUM_CASTLE_AREA_VARIATIONS
			iSeed = MPGlobalsAmbience.iKotCLocation
			PRINTLN("[KINGCASTLE] - [CASTLE AREA COORDS] - iKotCLocation > (-1), iSeed = ", iSeed)
		ENDIF
	#ENDIF

	// Return the hot property variation
	RETURN iSeed
ENDFUNC

FUNC FLOAT GET_SPAWN_POINT_WEIGHTING(INT iKingCastleVariation, eSPAWN_POINT_WEIGHTING eWeighting)
	
	SWITCH iKingCastleVariation
		CASE CASTLE_AREA_BEACH_TOWER	
			SWITCH eWeighting
				CASE ESPAWNPOINTWEIGHTING_A		RETURN g_sMPTunables.fKingOfCastle_BeachTowerAClassSpawnPointWeighting
				CASE ESPAWNPOINTWEIGHTING_B		RETURN g_sMPTunables.fKingOfCastle_BeachTowerBClassSpawnPointWeighting
				CASE ESPAWNPOINTWEIGHTING_C		RETURN g_sMPTunables.fKingOfCastle_BeachTowerCClassSpawnPointWeighting
			ENDSWITCH
		BREAK
		CASE CASTLE_AREA_LAND_ACT_DAM	
			SWITCH eWeighting
				CASE ESPAWNPOINTWEIGHTING_A		RETURN g_sMPTunables.fKingOfCastle_LandActDamAClassSpawnPointWeighting
				CASE ESPAWNPOINTWEIGHTING_B		RETURN g_sMPTunables.fKingOfCastle_LandActDamBClassSpawnPointWeighting
				CASE ESPAWNPOINTWEIGHTING_C		RETURN g_sMPTunables.fKingOfCastle_LandActDamCClassSpawnPointWeighting
			ENDSWITCH
		BREAK							
		CASE CASTLE_AREA_FIRE_STATION		
			SWITCH eWeighting
				CASE ESPAWNPOINTWEIGHTING_A		RETURN g_sMPTunables.fKingOfCastle_FireStationAClassSpawnPointWeighting
				CASE ESPAWNPOINTWEIGHTING_B		RETURN g_sMPTunables.fKingOfCastle_FireStationBClassSpawnPointWeighting
				CASE ESPAWNPOINTWEIGHTING_C		RETURN g_sMPTunables.fKingOfCastle_FireStationCClassSpawnPointWeighting
			ENDSWITCH
		BREAK						
		CASE CASTLE_AREA_CABLE_CAR_STATION			
			SWITCH eWeighting
				CASE ESPAWNPOINTWEIGHTING_A		RETURN g_sMPTunables.fKingOfCastle_CableCarAClassSpawnPointWeighting
				CASE ESPAWNPOINTWEIGHTING_B		RETURN g_sMPTunables.fKingOfCastle_CableCarBClassSpawnPointWeighting
				CASE ESPAWNPOINTWEIGHTING_C		RETURN g_sMPTunables.fKingOfCastle_CableCarCClassSpawnPointWeighting
			ENDSWITCH
		BREAK				
		CASE CASTLE_AREA_ALIEN_HIPPY_HILL		
			SWITCH eWeighting
				CASE ESPAWNPOINTWEIGHTING_A		RETURN g_sMPTunables.fKingOfCastle_HippyHillAClassSpawnPointWeighting
				CASE ESPAWNPOINTWEIGHTING_B		RETURN g_sMPTunables.fKingOfCastle_HippyHillBClassSpawnPointWeighting
				CASE ESPAWNPOINTWEIGHTING_C		RETURN g_sMPTunables.fKingOfCastle_HippyHillCClassSpawnPointWeighting
			ENDSWITCH
		BREAK					
		CASE CASTLE_AREA_OBSERVATORY		
			SWITCH eWeighting
				CASE ESPAWNPOINTWEIGHTING_A		RETURN g_sMPTunables.fKingOfCastle_ObservatoryAClassSpawnPointWeighting
				CASE ESPAWNPOINTWEIGHTING_B		RETURN g_sMPTunables.fKingOfCastle_ObservatoryBClassSpawnPointWeighting
				CASE ESPAWNPOINTWEIGHTING_C		RETURN g_sMPTunables.fKingOfCastle_ObservatoryCClassSpawnPointWeighting
			ENDSWITCH
		BREAK						
		CASE CASTLE_AREA_SCRAP_YARD		
			SWITCH eWeighting
				CASE ESPAWNPOINTWEIGHTING_A		RETURN g_sMPTunables.fKingOfCastle_ScrapYardAClassSpawnPointWeighting
				CASE ESPAWNPOINTWEIGHTING_B		RETURN g_sMPTunables.fKingOfCastle_ScrapYardBClassSpawnPointWeighting
				CASE ESPAWNPOINTWEIGHTING_C		RETURN g_sMPTunables.fKingOfCastle_ScrapYardCClassSpawnPointWeighting
			ENDSWITCH
		BREAK							
		CASE CASTLE_AREA_PLAYBOY_GROTTO			
			SWITCH eWeighting
				CASE ESPAWNPOINTWEIGHTING_A		RETURN g_sMPTunables.fKingOfCastle_GrottoAClassSpawnPointWeighting
				CASE ESPAWNPOINTWEIGHTING_B		RETURN g_sMPTunables.fKingOfCastle_GrottoBClassSpawnPointWeighting
				CASE ESPAWNPOINTWEIGHTING_C		RETURN g_sMPTunables.fKingOfCastle_GrottoCClassSpawnPointWeighting
			ENDSWITCH
		BREAK					
	ENDSWITCH
	
	RETURN 1.0
	
ENDFUNC

PROC GET_SPAWN_POINT_DATA(INT iKingCastleVariation, INT iSpawnPoint, VECTOR &vCoords, FLOAT &fHeading, FLOAT &fWeighting)
	
	SWITCH iKingCastleVariation
		CASE CASTLE_AREA_BEACH_TOWER	
			SWITCH iSpawnPoint
				CASE 0	
					vCoords 	=	<<-1221.7085	,	-1849.3352	,	1.251	>>
					fHeading 	=	337.5771						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK
				CASE 1			
					vCoords 	=	<<-1237.6702	,	-1835.705	,	1.2517	>>
					fHeading 	=	323.4358						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)		
				BREAK
				CASE 2			
					vCoords 	=	<<-1249.548	,	-1817.3547	,	1.2506	>>
					fHeading 	=	297.8504						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)		
				BREAK
				CASE 3			
					vCoords 	=	<<-1248.6849	,	-1803.2574	,	1.3833	>>
					fHeading 	=	286.9426						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)				
				BREAK
				CASE 4							
					vCoords 	=	<<-1248.9908	,	-1791.1484	,	1.6577	>>
					fHeading 	=	279.8152						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)				
				BREAK
				CASE 5							
					vCoords 	=	<<-1255.6776	,	-1781.7412	,	1.5292	>>
					fHeading 	=	271.3008						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)				
				BREAK
				CASE 6							
					vCoords 	=	<<-1245.8599	,	-1762.1647	,	1.7061	>>
					fHeading 	=	232.2266						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)				
				BREAK
				CASE 7							
					vCoords 	=	<<-1245.3335	,	-1747.1135	,	2.0783	>>
					fHeading 	=	246.9348						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)				
				BREAK
				CASE 8							
					vCoords		=	<<-1231.5773	,	-1747.2778	,	2.2692	>>
					fHeading 	=	208.6838						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)				
				BREAK
				CASE 9							
					vCoords 	=	<<-1220.9473	,	-1735.3784	,	3.3682	>>
					fHeading 	=	201.3756						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)				
				BREAK	
				CASE 10							
					vCoords 	=	<<-1210.0747	,	-1736.3596	,	3.2672	>>
					fHeading 	=	188.154						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)				
				BREAK	
				CASE 11					
					vCoords 	=	<<-1198.2474	,	-1731.74	,	3.3204	>>
					fHeading 	=	194.4361						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)				
				BREAK
				CASE 12							
					vCoords 	=	<<-1181.9507	,	-1730.0043	,	3.3951	>>
					fHeading 	=	177.6483						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)				
				BREAK
				CASE 13							
					vCoords 	=	<<-1170.5897	,	-1719.6531	,	3.3414	>>
					fHeading 	=	159.2419						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)				
				BREAK
				CASE 14							
					vCoords 	=	<<-1159.5476	,	-1712.5056	,	3.327	>>
					fHeading 	=	173.1471						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)				
				BREAK
				CASE 15							
					vCoords 	=	<<-1164.5105	,	-1729.8396	,	3.2972	>>
					fHeading 	=	164.5387						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)				
				BREAK
				CASE 16							
					vCoords 	=	<<-1153.6821	,	-1723.6842	,	3.2673	>>
					fHeading 	=	159.7204						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)				
				BREAK
				CASE 17							
					vCoords 	=	<<-1144.6997	,	-1734.5724	,	3.2823	>>
					fHeading 	=	132.856						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)				
				BREAK
				CASE 18							
					vCoords 	=	<<-1128.2719	,	-1755.8715	,	3.0265	>>
					fHeading 	=	111.5717						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)				
				BREAK
				CASE 19						
					vCoords		=	<<-1140.4221	,	-1768.4541	,	3.1609	>>
					fHeading 	=	108.3239						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)				
				BREAK
				CASE 20							
					vCoords 	=	<<-1236.1691	,	-1851.2838	,	1.2074	>>
					fHeading 	=	330.7894						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)				
				BREAK	
				CASE 21						
					vCoords 	=	<<-1250.0258	,	-1849.9877	,	0.8113	>>
					fHeading 	=	315.2169						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)				
				BREAK	
				CASE 22					
					vCoords 	=	<<-1249.307	,	-1832.0099	,	1.2365	>>
					fHeading 	=	308.724						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)				
				BREAK
				CASE 23							
					vCoords 	=	<<-1262.0798	,	-1826.3419	,	0.88	>>
					fHeading 	=	291.1317						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)				
				BREAK
				CASE 24							
					vCoords 	=	<<-1262.7282	,	-1810.0209	,	1.2095	>>
					fHeading 	=	279.2619						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)				
				BREAK
				CASE 25							
					vCoords 	=	<<-1260.259	,	-1797.3158	,	1.24	>>
					fHeading 	=	266.2758						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)				
				BREAK
				CASE 26							
					vCoords 	=	<<-1274.1533	,	-1773.825	,	1.229	>>
					fHeading 	=	253.8689						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)				
				BREAK
				CASE 27							
					vCoords 	=	<<-1260.6898	,	-1766.2112	,	1.6755	>>
					fHeading 	=	244.6821						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)				
				BREAK
				CASE 28							
					vCoords 	=	<<-1266.0349	,	-1751.4143	,	1.7303	>>
					fHeading 	=	225.9904						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)				
				BREAK
				CASE 29							
					vCoords 	=	<<-1208.9519	,	-1720.7599	,	3.4325	>>
					fHeading 	=	190.2951						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)				
				BREAK
				CASE 30							
					vCoords		=	<<-1195.1249	,	-1711.3389	,	3.4745	>>
					fHeading 	=	177.7994						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)				
				BREAK
				CASE 31							
					vCoords 	=	<<-1186.9827	,	-1699.1566	,	3.4862	>>
					fHeading 	=	185.7234						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)				
				BREAK	
				CASE 32						
					vCoords 	=	<<-1174.9994	,	-1703.7756	,	3.4646	>>
					fHeading 	=	172.7709						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)			
				BREAK	
				CASE 33							
					vCoords 	=	<<-1163.1329	,	-1699.9053	,	3.4278	>>
					fHeading 	=	169.2118						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)			
				BREAK
				CASE 34								
					vCoords 	=	<<-1150.3359	,	-1701.652	,	3.3947	>>
					fHeading 	=	148.2826						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)				
				BREAK
				CASE 35						
					vCoords 	=	<<-1144.0776	,	-1687.1299	,	3.4853	>>
					fHeading 	=	149.4927						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)				
				BREAK
				CASE 36						
					vCoords 	=	<<-1138.1356	,	-1707.8545	,	3.387	>>
					fHeading 	=	133.8241						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)				
				BREAK
				CASE 37						
					vCoords 	=	<<-1140.0554	,	-1722.3411	,	3.3376	>>
					fHeading 	=	127.5326						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)				
				BREAK
				CASE 38						
					vCoords 	=	<<-1116.7631	,	-1746.2201	,	3.1299	>>
					fHeading 	=	107.5649						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)				
				BREAK
				CASE 39						
					vCoords 	=	<<-1108.2073	,	-1735.9645	,	3.2643	>>
					fHeading 	=	123.7412						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)				
				BREAK
				CASE 40						
					vCoords 	=	<<-1227.0859	,	-1873.856	,	4.935	>>
					fHeading 	=	337.0957						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)				
				BREAK
				CASE 41						
					vCoords		=	<<-1215.9049	,	-1861.8759	,	4.9443	>>
					fHeading 	=	346.5332						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)				
				BREAK	
				CASE 42							
					vCoords 	=	<<-1206.479	,	-1854.0223	,	5.0039	>>
					fHeading 	=	346.756						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)				
				BREAK		
				CASE 43								
					vCoords 	=	<<-1234.5697	,	-1863.3016	,	0.4019	>>
					fHeading 	=	334.4932						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)				
				BREAK		
				CASE 44					
					vCoords 	=	<<-1261.5416	,	-1840.8429	,	0.429	>>
					fHeading 	=	308.3732						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)				
				BREAK
				CASE 45							
					vCoords 	=	<<-1271.8208	,	-1799.4036	,	0.9933	>>
					fHeading 	=	276.274						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)				
				BREAK
				CASE 46							
					vCoords 	=	<<-1284.1946	,	-1789.1056	,	0.6971	>>
					fHeading 	=	266.1992						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)				
				BREAK
				CASE 47							
					vCoords 	=	<<-1257.9714	,	-1736.8583	,	2.0784	>>
					fHeading 	=	227.487						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)				
				BREAK
				CASE 48							
					vCoords 	=	<<-1247.2671	,	-1725.901	,	3.2245	>>
					fHeading 	=	214.7514						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)				
				BREAK
				CASE 49							
					vCoords 	=	<<-1234.9883	,	-1722.8982	,	3.5186	>>
					fHeading 	=	220.7502						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)			
				BREAK
				CASE 50								
					vCoords 	=	<<-1235.8738	,	-1703.2379	,	3.0997	>>
					fHeading 	=	214.0405						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)				
				BREAK
				CASE 51							
					vCoords 	=	<<-1231.5876	,	-1682.8486	,	2.571	>>
					fHeading 	=	203.4435						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)				
				BREAK
				CASE 52							
					vCoords		=	<<-1215.568	,	-1695.7775	,	3.1209	>>
					fHeading 	=	196.5891						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)				
				BREAK
				CASE 53							
					vCoords 	=	<<-1132.4147, -1669.4354, 3.5755>>
					fHeading 	=	148.2112					
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)				
				BREAK	
				CASE 54							
					vCoords 	=	<<-1159.3087	,	-1677.7773	,	3.4694	>>
					fHeading 	=	175.6314						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)				
				BREAK	
				CASE 55							
					vCoords 	=	<<-1129.1797	,	-1690.5024	,	3.4678	>>
					fHeading 	=	143.3497						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)				
				BREAK
				CASE 56							
					vCoords 	=	<<-1119.6694	,	-1702.9875	,	3.4241	>>
					fHeading 	=	129.3894						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)				
				BREAK
				CASE 57						
					vCoords 	=	<<-1120.0632	,	-1717.3258	,	3.3807	>>
					fHeading 	=	123.5869						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)				
				BREAK
				CASE 58						
					vCoords 	=	<<-1111.9006	,	-1758.6924	,	4.7663	>>
					fHeading 	=	93.7592						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)				
				BREAK
				CASE 59							
					vCoords 	=	<<-1122.0592	,	-1769.5427	,	4.9921	>>
					fHeading 	=	93.2178						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)						
				BREAK
			ENDSWITCH
		BREAK
		CASE CASTLE_AREA_LAND_ACT_DAM		
			SWITCH iSpawnPoint
				CASE 0	
					vCoords 	= <<1692.8611	,	-80.4741	,	174.6247	>>
					fHeading 	=22.7116						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)		
				BREAK
				CASE 1			
					vCoords 	= <<1699.0492	,	-73.278	,	175.3116	>>
					fHeading 	=36.6067						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)		
				BREAK
				CASE 2						
					vCoords 	= <<1686.1504	,	-72.261	,	173.9488	>>
					fHeading 	=33.5969						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK
				CASE 3							
					vCoords 	= <<1673.5732	,	-76.8662	,	172.6441	>>
					fHeading 	=358.3647						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK
				CASE 4							
					vCoords 	= <<1668.3027	,	-68.8711	,	172.6008	>>
					fHeading 	=344.7672						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK
				CASE 5							
					vCoords 	= <<1664.004	,	-81.4696	,	171.3247	>>
					fHeading 	=335.9399						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK
				CASE 6							
					vCoords 	= <<1653.9427	,	-82.7018	,	170.2733	>>
					fHeading 	=316.5097						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)		
				BREAK
				CASE 7						
					vCoords 	= <<1642.1841	,	16.9022	,	172.8744	>>
					fHeading 	=213.6659						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)		
				BREAK
				CASE 8						
					vCoords		= <<1647.1166	,	26.0881	,	172.469	>>
					fHeading 	=200.4823						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)		
				BREAK
				CASE 9						
					vCoords 	= <<1643.2511	,	34.2645	,	171.8502	>>
					fHeading 	=198.3856						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK	
				CASE 10							
					vCoords 	= <<1652.1549	,	38.4683	,	171.5731	>>
					fHeading 	=167.6548						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK	
				CASE 11							
					vCoords 	= <<1647.5205	,	46.6361	,	171.515	>>
					fHeading 	=173.6852						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK
				CASE 12							
					vCoords 	= <<1658.3408	,	46.5354	,	171.4155	>>
					fHeading 	=142.775						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)		
				BREAK
				CASE 13						
					vCoords 	= <<1654.5568	,	54.9274	,	171.5186	>>
					fHeading 	=158.0565						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK
				CASE 14							
					vCoords 	= <<1679.6868	,	-62.2895	,	172.8752	>>
					fHeading 	=19.1425						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK
				CASE 15							
					vCoords 	= <<1648.2603	,	-92.0332	,	169.0467	>>
					fHeading 	=320.4252						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK
				CASE 16							
					vCoords 	= << 1641.1202, -90.8545, 167.3707 >>
					fHeading 	= 340.133						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)		
				BREAK
				CASE 17						
					vCoords 	= <<1632.8395	,	-88.4347	,	166.232	>>
					fHeading 	=275.8517						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)		
				BREAK
				CASE 18						
					vCoords 	= <<1624.5255	,	-82.0814	,	165.1201	>>
					fHeading 	=268.6114						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK
				CASE 19							
					vCoords		= <<1709.025	,	-79.3046	,	176.215	>>
					fHeading 	=47.9711						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)		
				BREAK
				CASE 20						
					vCoords 	= <<1709.4705	,	-89.1149	,	176.8704	>>
					fHeading 	=45.0986						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)		
				BREAK	
				CASE 21						
					vCoords 	= <<1719.7617	,	-89.4824	,	177.2371	>>
					fHeading 	=40.521						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK	
				CASE 22							
					vCoords 	= <<1715.4106	,	-97.7661	,	177.0435	>>
					fHeading 	=39.9521						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK
				CASE 23							
					vCoords 	= <<1725.2697	,	-101.2808	,	177.3414	>>
					fHeading 	=28.2699						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK
				CASE 24							
					vCoords 	= <<1664.7621	,	51.6959	,	171.2758	>>
					fHeading 	=125.1599						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)		
				BREAK
				CASE 25						
					vCoords 	= <<1659.656	,	60.4243	,	171.7457	>>
					fHeading 	=155.8516						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK
				CASE 26							
					vCoords 	= <<1671.6151	,	58.2652	,	171.0533	>>
					fHeading 	=130.5652						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK
				CASE 27							
					vCoords 	= <<1670.401	,	68.8127	,	170.8255	>>
					fHeading 	=142.8485						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK
				CASE 28							
					vCoords 	= <<1680.8239	,	64.6627	,	170.8684	>>
					fHeading 	=171.7255						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK
				CASE 29							
					vCoords 	= <<1686.4758	,	72.1055	,	170.6706	>>
					fHeading 	=118.2492						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK
				CASE 30							
					vCoords		= <<1724.4978	,	78.3834	,	169.7617	>>
					fHeading 	=122.1907						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK
				CASE 31							
					vCoords 	= <<1716.5553	,	77.9	,	170.125	>>
					fHeading 	=133.2787						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK	
				CASE 32						
					vCoords 	= <<1718.6584	,	69.3794	,	169.7158	>>
					fHeading 	=123.6501						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK	
				CASE 33							
					vCoords 	= <<1708.2229	,	67.7488	,	170.122	>>
					fHeading 	=125.7215						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK
				CASE 34							
					vCoords 	= <<1696.6658	,	67.5787	,	170.4112	>>
					fHeading 	=130.5078						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK
				CASE 35						
					vCoords 	= <<1688.9244	,	64.4787	,	170.5247	>>
					fHeading 	=116.7526						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK
				CASE 36							
					vCoords 	= <<1654.4088	,	85.3155	,	178.6964	>>
					fHeading 	=185.1295						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK
				CASE 37						
					vCoords 	= <<1642.5211	,	61.9855	,	183.7891	>>
					fHeading 	=187.6025						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK
				CASE 38						
					vCoords 	= <<1663.5216	,	37.8715	,	170.7838	>>
					fHeading 	=181.9699						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK
				CASE 39							
					vCoords 	= <<1663.4307	,	25.7403	,	167.747	>>
					fHeading 	=165.7094						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK
				CASE 40						
					vCoords 	= <<1660.1564	,	12.794	,	165.1707	>>
					fHeading 	=184.4829						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK
				CASE 41						
					vCoords		= <<1658.881	,	-53.3581	,	167.4092	>>
					fHeading 	=340.2356						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK	
				CASE 42						
					vCoords 	= <<1650.8048	,	-63.341	,	163.7801	>>
					fHeading 	=325.3763						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK		
				CASE 43							
					vCoords 	= <<1619.0186	,	-87.1216	,	164.7846	>>
					fHeading 	=285.7851						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK		
				CASE 44							
					vCoords 	= <<1616.2164	,	-76.6892	,	164.2788	>>
					fHeading 	=277.6688						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK
				CASE 45						
					vCoords 	= <<1611.5674	,	-84.1719	,	164.6911	>>
					fHeading 	=279.2012						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK
				CASE 46						
					vCoords 	= <<1606.5641	,	-73.1815	,	163.3484	>>
					fHeading 	=256.1666						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK
				CASE 47						
					vCoords 	= <<1595.9971	,	-78.4415	,	162.1879	>>
					fHeading 	=273.5168						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK
				CASE 48							
					vCoords 	= <<1596.4407	,	-69.8807	,	162.1016	>>
					fHeading 	=260.1767						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK
				CASE 49							
					vCoords 	= <<1587.1611	,	-67.6343	,	161.2196	>>
					fHeading 	=253.1009						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK
				CASE 50							
					vCoords 	= <<1582.3531	,	-76.1702	,	160.4171	>>
					fHeading 	=265.3223						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK
				CASE 51							
					vCoords 	= <<1725.6759	,	-110.6558	,	177.8831	>>
					fHeading 	=34.6784						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK
				CASE 52							
					vCoords		= <<1736.2031	,	-108.6194	,	179.2085	>>
					fHeading 	=50.2519						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK
				CASE 53						
					vCoords 	= <<1746.1355	,	-106.5357	,	180.7151	>>
					fHeading 	=65.9975						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK	
				CASE 54							
					vCoords 	= <<1749.5942	,	-96.7965	,	182.065	>>
					fHeading 	=79.3328						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK	
				CASE 55						
					vCoords 	= <<1758.5133	,	-98.7928	,	182.982	>>
					fHeading 	=82.5892						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK
				CASE 56							
					vCoords 	= <<1760.4727	,	-88.7761	,	184.3481	>>
					fHeading 	=83.4158						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK
				CASE 57							
					vCoords 	= <<1770.1532	,	-89.9473	,	186.0895	>>
					fHeading 	=69.9826						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK
				CASE 58						
					vCoords 	= <<1772.7311	,	-80.3556	,	187.6041	>>
					fHeading 	=87.3442						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK
				CASE 59						
					vCoords 	= <<1782.0383	,	-81.4923	,	188.8666	>>
					fHeading 	=92.9392						
					fWeighting 	= GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK
			ENDSWITCH
		BREAK
		CASE CASTLE_AREA_FIRE_STATION	
			SWITCH iSpawnPoint
				CASE 0								
					vCoords 	=	<<-358.9362	,	6101.9922	,	30.5445	>>
					fHeading 	=	124.1256						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)						
				BREAK							
				CASE 1										
					vCoords 	=	<<-348.5437	,	6102.8457	,	30.5442	>>
					fHeading 	=	115.8979						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)						
				BREAK							
				CASE 2										
					vCoords 	=	<<-354.9039	,	6112.2227	,	30.5402	>>
					fHeading 	=	155.6832						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)						
				BREAK							
				CASE 3										
					vCoords 	=	<<-341.2615	,	6111.4473	,	30.542	>>
					fHeading 	=	114.7182						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)						
				BREAK							
				CASE 4										
					vCoords 	=	<<-347.4731	,	6120.3389	,	30.5401	>>
					fHeading 	=	161.4131						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)						
				BREAK							
				CASE 5										
					vCoords 	=	<<-376.9227	,	6129.001	,	30.5322	>>
					fHeading 	=	153.5222						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)						
				BREAK							
				CASE 6										
					vCoords 	=	<<-384.3775	,	6136.9468	,	30.4047	>>
					fHeading 	=	187.2874						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)							
				BREAK							
				CASE 7									
					vCoords 	=	<<-389.1951	,	6129.6172	,	30.5795	>>
					fHeading 	=	181.9189						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)							
				BREAK							
				CASE 8									
					vCoords		=	<<-399.0005	,	6127.2749	,	30.549	>>
					fHeading 	=	204.8398						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)							
				BREAK							
				CASE 9									
					vCoords 	=	<<-383.5045	,	6120.4644	,	30.5795	>>
					fHeading 	=	140.9251						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)							
				BREAK								
				CASE 10									
					vCoords 	=	<<-391.13	,	6120.8037	,	30.4824	>>
					fHeading 	=	201.1372						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)							
				BREAK								
				CASE 11									
					vCoords 	=	<<-402.7893	,	6117.3657	,	30.403	>>
					fHeading 	=	226.6899						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)							
				BREAK							
				CASE 12									
					vCoords 	=	<<-412.6236	,	6114.75	,	30.403	>>
					fHeading 	=	226.6899						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)							
				BREAK							
				CASE 13									
					vCoords 	=	<<-394.3771	,	6111.2544	,	30.4438	>>
					fHeading 	=	239.3517						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)							
				BREAK							
				CASE 14									
					vCoords 	=	<<-389.6141	,	6105.1421	,	30.5445	>>
					fHeading 	=	223.9884						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)							
				BREAK							
				CASE 15									
					vCoords 	=	<<-393.7008	,	6063.0386	,	30.6001	>>
					fHeading 	=	307.3006						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)								
				BREAK							
				CASE 16								
					vCoords 	=	<<-390.5935	,	6052.4204	,	30.6001	>>
					fHeading 	=	318.0318						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)								
				BREAK							
				CASE 17								
					vCoords 	=	<<-370.7218	,	6062.1602	,	30.6001	>>
					fHeading 	=	22.496						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)								
				BREAK							
				CASE 18								
					vCoords 	=	<<-360.3093	,	6063.8345	,	30.6001	>>
					fHeading 	=	37.8362						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)							
				BREAK							
				CASE 19									
					vCoords		=	<<-342.4209	,	6079.6538	,	30.3972	>>
					fHeading 	=	68.9811						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)								
				BREAK							
				CASE 20								
					vCoords 	=	<<-341.2047	,	6070.1709	,	30.4855	>>
					fHeading 	=	63.9301						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)							
				BREAK								
				CASE 21									
					vCoords 	=	<<-329.9387	,	6066.4023	,	30.3113	>>
					fHeading 	=	44.5343						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)							
				BREAK								
				CASE 22									
					vCoords 	=	<<-335.7858	,	6122.2773	,	31.2548	>>
					fHeading 	=	129.6423						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)							
				BREAK							
				CASE 23									
					vCoords 	=	<<-342.0471	,	6128.1714	,	31.2548	>>
					fHeading 	=	142.1917						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)								
				BREAK							
				CASE 24								
					vCoords 	=	<<-349.7313	,	6135.2832	,	31.2548	>>
					fHeading 	=	158.8037						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)								
				BREAK							
				CASE 25								
					vCoords 	=	<<-356.2828	,	6125.2026	,	30.5402	>>
					fHeading 	=	199.7626						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)								
				BREAK							
				CASE 26								
					vCoords 	=	<<-359.0895	,	6139.2144	,	30.5635	>>
					fHeading 	=	124.4663						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)								
				BREAK							
				CASE 27								
					vCoords 	=	<<-369.4224	,	6152.7251	,	30.4406	>>
					fHeading 	=	161.3973						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)								
				BREAK							
				CASE 28								
					vCoords 	=	<<-375.9839	,	6141.0225	,	30.4406	>>
					fHeading 	=	161.3973						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)								
				BREAK							
				CASE 29								
					vCoords 	=	<<-369.7337	,	6134.1655	,	30.5525	>>
					fHeading 	=	136.8736						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)								
				BREAK							
				CASE 30								
					vCoords		=	<<-359.228	,	6073.2998	,	30.5975	>>
					fHeading 	=	40.8648						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)								
				BREAK							
				CASE 31								
					vCoords 	=	<<-350.0499, 6083.4185, 30.3773>>
					fHeading 	=	81.9925					
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)								
				BREAK								
				CASE 32								
					vCoords 	=	<<-332.9335	,	6056.562	,	30.218	>>
					fHeading 	=	34.6386						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)								
				BREAK								
				CASE 33								
					vCoords 	=	<<-319.7262	,	6071.6548	,	30.4404	>>
					fHeading 	=	98.0741						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)							
				BREAK							
				CASE 34									
					vCoords 	=	<<-305.5597	,	6086.6714	,	30.3145	>>
					fHeading 	=	105.1401						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)							
				BREAK							
				CASE 35									
					vCoords 	=	<<-315.9	,	6098.8	,	30.5622	>>
					fHeading 	=	68.0701						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)							
				BREAK							
				CASE 36									
					vCoords 	=	<<-321.8067	,	6105.2114	,	30.5847	>>
					fHeading 	=	77.1148						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)								
				BREAK							
				CASE 37								
					vCoords 	=	<<-333.3944	,	6102.0605	,	30.5452	>>
					fHeading 	=	94.9811						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)								
				BREAK							
				CASE 38								
					vCoords 	=	<<-328.1533	,	6111.5444	,	30.5992	>>
					fHeading 	=	112.3565						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)								
				BREAK							
				CASE 39								
					vCoords 	=	<<-346.6402	,	6170.4795	,	30.5104	>>
					fHeading 	=	153.9383						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)							
				BREAK							
				CASE 40									
					vCoords 	=	<<-348.4091	,	6161.7412	,	30.5903	>>
					fHeading 	=	127.3381						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)							
				BREAK							
				CASE 41									
					vCoords		=	<<-357.501	,	6164.666	,	30.3218	>>
					fHeading 	=	165.9396						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)								
				BREAK								
				CASE 42								
					vCoords 	=	<<-362.3921	,	6158.9976	,	30.4267	>>
					fHeading 	=	164.6814						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)								
				BREAK									
				CASE 43								
					vCoords 	=	<<-299.2778, 6094.6816, 30.3967>>
					fHeading 	=	97.7633					
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)								
				BREAK									
				CASE 44								
					vCoords 	=	<<-403.1499	,	6144.1792	,	30.5344	>>
					fHeading 	=	192.2655						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)							
				BREAK							
				CASE 45									
					vCoords 	=	<<-419.8507	,	6128.8936	,	30.3768	>>
					fHeading 	=	226.1505						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)							
				BREAK							
				CASE 46								
					vCoords 	=	<<-403.4776	,	6109.1255	,	30.432	>>
					fHeading 	=	231.4631						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)							
				BREAK							
				CASE 47									
					vCoords 	=	<<-418.8898	,	6102.5942	,	30.5063	>>
					fHeading 	=	250.378						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)							
				BREAK							
				CASE 48									
					vCoords 	=	<<-423.8823	,	6087.7207	,	30.3067	>>
					fHeading 	=	249.7169						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)								
				BREAK							
				CASE 49								
					vCoords 	=	<<-409.3334	,	6087.3013	,	30.6001	>>
					fHeading 	=	255.2337						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)								
				BREAK							
				CASE 50								
					vCoords 	=	<<-415.4413	,	6074.5645	,	30.6001	>>
					fHeading 	=	295.8184						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)							
				BREAK							
				CASE 51									
					vCoords 	=	<<-424.9092	,	6072.749	,	30.4263	>>
					fHeading 	=	283.0375						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)							
				BREAK							
				CASE 52									
					vCoords		=	<<-420.9166	,	6060.0981	,	30.5557	>>
					fHeading 	=	253.9669						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)								
				BREAK							
				CASE 53								
					vCoords 	=	<<-410.8875	,	6060.5493	,	30.6001	>>
					fHeading 	=	233.6391						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)								
				BREAK								
				CASE 54								
					vCoords 	=	<<-401.7228	,	6048.293	,	30.6001	>>
					fHeading 	=	316.9792						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)							
				BREAK								
				CASE 55									
					vCoords 	=	<<-410.8336	,	6049.9565	,	30.7181	>>
					fHeading 	=	282.7038						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)							
				BREAK							
				CASE 56									
					vCoords 	=	<<-388.0199	,	6039.7676	,	30.5993	>>
					fHeading 	=	17.0043						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)							
				BREAK							
				CASE 57									
					vCoords 	=	<<-395.2367	,	6033.0864	,	30.6906	>>
					fHeading 	=	344.4481						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)							
				BREAK							
				CASE 58									
					vCoords 	=	<<-385.8562	,	6026.0259	,	30.7199	>>
					fHeading 	=	19.855						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)							
				BREAK							
				CASE 59									
					vCoords 	=	<<-380.7711	,	6033.2837	,	30.5989	>>
					fHeading 	=	40.8827						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)						
				BREAK
			ENDSWITCH
		BREAK
		CASE CASTLE_AREA_CABLE_CAR_STATION	
			SWITCH iSpawnPoint
				CASE 0			
					vCoords 	=	<<-692.3021	,	5591.0327	,	31.803	>>
					fHeading 	=	74.8653						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)		
				BREAK			
				CASE 1			
					vCoords 	=	<<-688.3215	,	5575.978	,	38.7867	>>
					fHeading 	=	59.0735						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)		
				BREAK			
				CASE 2							
					vCoords 	=	<<-697.7473	,	5566.1304	,	37.8698	>>
					fHeading 	=	50.5049						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)		
				BREAK			
				CASE 3							
					vCoords 	=	<<-707.0853	,	5561.9028	,	37.7135	>>
					fHeading 	=	46.5953						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 4								
					vCoords 	=	<<-718.0601	,	5562.4038	,	35.1231	>>
					fHeading 	=	35.9231						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)		
				BREAK			
				CASE 5							
					vCoords 	=	<<-729.6589	,	5559.356	,	33.8316	>>
					fHeading 	=	2.3291						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)		
				BREAK			
				CASE 6							
					vCoords 	=	<<-723.8281	,	5544.7368	,	34.8933	>>
					fHeading 	=	18.3278						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)		
				BREAK			
				CASE 7							
					vCoords 	=	<<-780.6183	,	5563.9707	,	32.6778	>>
					fHeading 	=	329.6704						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)		
				BREAK			
				CASE 8							
					vCoords		=	<<-767.3	,	5569.3032	,	35.3	>>
					fHeading 	=	331.3245						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)		
				BREAK			
				CASE 9							
					vCoords 	=	<<-772.2	,	5581.6	,	32.6824	>>
					fHeading 	=	301.3555						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)		
				BREAK			
				CASE 10							
					vCoords 	=	<<-778.3644	,	5588.7148	,	32.6826	>>
					fHeading 	=	270.8067						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)		
				BREAK			
				CASE 11							
					vCoords 	=	<<-768.7381	,	5595.1025	,	32.6827	>>
					fHeading 	=	230.1011						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)		
				BREAK			
				CASE 12							
					vCoords 	=	<<-782.3171	,	5573.2622	,	32.6847	>>
					fHeading 	=	294.6598						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)		
				BREAK			
				CASE 13							
					vCoords 	=	<<-781.341	,	5582.1	,	32.6771	>>
					fHeading 	=	336.2302						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)		
				BREAK			
				CASE 14							
					vCoords 	=	<<-753.9799	,	5627.1069	,	27.1403	>>
					fHeading 	=	171.4856						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)		
				BREAK			
				CASE 15							
					vCoords 	=	<<-730.2549	,	5635.9937	,	27.8518	>>
					fHeading 	=	162.9716						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)		
				BREAK			
				CASE 16							
					vCoords 	=	<<-723.6004	,	5622.3833	,	28.385	>>
					fHeading 	=	172.2795						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)		
				BREAK			
				CASE 17							
					vCoords 	=	<<-718.595	,	5631.0156	,	27.7666	>>
					fHeading 	=	172.2795						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)		
				BREAK			
				CASE 18							
					vCoords 	=	<<-708.1818	,	5621.5117	,	29.6452	>>
					fHeading 	=	126.8752						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 19								
					vCoords		=	<<-696.5601	,	5615.6738	,	29.9981	>>
					fHeading 	=	125.8635						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 20								
					vCoords 	=	<<-708.3126	,	5550.2056	,	36.7429	>>
					fHeading 	=	29.6831						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)		
				BREAK			
				CASE 21							
					vCoords 	=	<<-766.3	,	5544.3	,	32.6826	>>
					fHeading 	=	30.2344						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)		
				BREAK			
				CASE 22							
					vCoords 	=	<<-749.5	,	5527.9	,	33.1	>>
					fHeading 	=	346.3935						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)		
				BREAK			
				CASE 23							
					vCoords 	=	<<-768.97	,	5520.835	,	32.6799	>>
					fHeading 	=	340.5899						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)		
				BREAK			
				CASE 24							
					vCoords 	=	<<-763.8516	,	5533.3115	,	32.6785	>>
					fHeading 	=	1.7896						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)		
				BREAK			
				CASE 25							
					vCoords 	=	<<-775.123	,	5542.0396	,	32.6934	>>
					fHeading 	=	349.4598						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)		
				BREAK			
				CASE 26							
					vCoords 	=	<<-766.6	,	5553.1	,	34.4	>>
					fHeading 	=	1.046						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)		
				BREAK			
				CASE 27							
					vCoords 	=	<<-771.2969	,	5556.7451	,	32.6834	>>
					fHeading 	=	349.9529						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)		
				BREAK			
				CASE 28							
					vCoords 	=	<<-781.6904	,	5555.6045	,	32.6802	>>
					fHeading 	=	351.5664						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 29								
					vCoords 	=	<<-769.5516	,	5621.6582	,	26.7823	>>
					fHeading 	=	235.7381						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)		
				BREAK			
				CASE 30							
					vCoords		=	<<-761.2958	,	5636.4438	,	25.4519	>>
					fHeading 	=	168.4409						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)		
				BREAK			
				CASE 31							
					vCoords 	=	<<-760.2766	,	5661.2959	,	22.954	>>
					fHeading 	=	164.5044						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 32								
					vCoords 	=	<<-741.7708	,	5648.583	,	25.8433	>>
					fHeading 	=	171.5803						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 33								
					vCoords 	=	<<-731.1522	,	5650.5605	,	27.2619	>>
					fHeading 	=	172.6602						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)		
				BREAK			
				CASE 34							
					vCoords 	=	<<-721.4506	,	5664.8423	,	26.6086	>>
					fHeading 	=	175.8839						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)		
				BREAK			
				CASE 35							
					vCoords 	=	<<-719.6653	,	5647.0815	,	28.1182	>>
					fHeading 	=	166.6596						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)		
				BREAK			
				CASE 36							
					vCoords 	=	<<-708.6494	,	5641.4883	,	29.7045	>>
					fHeading 	=	139.2498						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)		
				BREAK			
				CASE 37							
					vCoords 	=	<<-698.6087	,	5635.2778	,	31.1732	>>
					fHeading 	=	130.991						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)		
				BREAK			
				CASE 38							
					vCoords 	=	<<-764.6	,	5648.1	,	23.8802	>>
					fHeading 	=	183.034						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)		
				BREAK			
				CASE 39							
					vCoords 	=	<<-750.9	,	5666.5	,	23.3295	>>
					fHeading 	=	231.3595						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)		
				BREAK			
				CASE 40							
					vCoords 	=	<<-719.377	,	5680.3633	,	25.3398	>>
					fHeading 	=	160.6696						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 41							
					vCoords		=	<<-698.3569	,	5694.5371	,	26.6459	>>
					fHeading 	=	147.8193						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 42							
					vCoords 	=	<<-680.2346	,	5676.2715	,	30.6741	>>
					fHeading 	=	86.9398						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 43							
					vCoords 	=	<<-676.3746	,	5655.0313	,	32.0498	>>
					fHeading 	=	139.4649						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 44							
					vCoords 	=	<<-680.2036	,	5638.7993	,	32.6927	>>
					fHeading 	=	134.8275						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 45							
					vCoords 	=	<<-650.3818	,	5653.8398	,	33.0871	>>
					fHeading 	=	123.0167						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 46							
					vCoords 	=	<<-670.8715	,	5583.5093	,	38.7761	>>
					fHeading 	=	85.3828						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 47							
					vCoords 	=	<<-743.9585	,	5544.0444	,	32.6886	>>
					fHeading 	=	10.6683						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 48							
					vCoords 	=	<<-786.1422	,	5543.0693	,	32.7461	>>
					fHeading 	=	320.9388						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 49							
					vCoords 	=	<<-800.3854	,	5544.438	,	32.1822	>>
					fHeading 	=	318.5256						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 50							
					vCoords 	=	<<-808.8489	,	5557.1782	,	31.0331	>>
					fHeading 	=	284.0945						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 51								
					vCoords 	=	<<-804.6185	,	5577.9814	,	30.1522	>>
					fHeading 	=	312.7694						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 52								
					vCoords		=	<<-804.7294	,	5593.23	,	28.3762	>>
					fHeading 	=	257.0917						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 53								
					vCoords 	=	<<-796.4553	,	5614.6685	,	26.6406	>>
					fHeading 	=	245.4986						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 54								
					vCoords 	=	<<-790.2338	,	5632.4834	,	25.0732	>>
					fHeading 	=	224.0737						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 55								
					vCoords 	=	<<-807.922	,	5624.8472	,	24.5138	>>
					fHeading 	=	240.116						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 56								
					vCoords 	=	<<-778.2	,	5620.3	,	27	>>
					fHeading 	=	221.8036						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 57								
					vCoords 	=	<<-820.9523	,	5595.459	,	24.6204	>>
					fHeading 	=	248.8524						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 58								
					vCoords 	=	<<-712.2	,	5689.9	,	26	>>
					fHeading 	=	156.9858						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 59								
					vCoords 	=	<<-699.6256	,	5678.481	,	29.242	>>
					fHeading 	=	137.245						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)						
				BREAK
			ENDSWITCH
		BREAK
		CASE CASTLE_AREA_ALIEN_HIPPY_HILL	
			SWITCH iSpawnPoint
				CASE 0			
					vCoords 	=	<<2504.7078	,	3832.864	,	44.2233	>>
					fHeading 	=	183.6139						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)		
				BREAK			
				CASE 1			
					vCoords 	=	<<2470.2336	,	3832.2144	,	39.2978	>>
					fHeading 	=	221.0469						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)				
				BREAK			
				CASE 2							
					vCoords 	=	<<2469.9836	,	3815.5874	,	39.1223	>>
					fHeading 	=	236.1043						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)			
				BREAK			
				CASE 3								
					vCoords 	=	<<2472.6819	,	3802.5955	,	39.5029	>>
					fHeading 	=	252.6169						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)			
				BREAK			
				CASE 4								
					vCoords 	=	<<2448.3369	,	3799.731	,	39.2155	>>
					fHeading 	=	245.4931						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)			
				BREAK			
				CASE 5								
					vCoords 	=	<<2429.2783	,	3803.2915	,	38.8134	>>
					fHeading 	=	251.2017						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)			
				BREAK			
				CASE 6								
					vCoords 	=	<<2461.7683	,	3786.9314	,	40.1218	>>
					fHeading 	=	273.0054						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)			
				BREAK			
				CASE 7								
					vCoords 	=	<<2446.4331	,	3784.8638	,	39.6849	>>
					fHeading 	=	253.3649						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)			
				BREAK			
				CASE 8								
					vCoords		=	<<2431.8403	,	3775.9771	,	39.5771	>>
					fHeading 	=	262.8813						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)			
				BREAK			
				CASE 9								
					vCoords 	=	<<2426.948	,	3759.1062	,	40.5694	>>
					fHeading 	=	286.9683						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)			
				BREAK			
				CASE 10								
					vCoords 	=	<<2413.0908	,	3747.1243	,	40.6396	>>
					fHeading 	=	291.6124						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)			
				BREAK			
				CASE 11								
					vCoords 	=	<<2435.1926	,	3749.9844	,	41.1655	>>
					fHeading 	=	292.603						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)				
				BREAK			
				CASE 12							
					vCoords 	=	<<2447.6604	,	3752.5833	,	41.4906	>>
					fHeading 	=	297.6301						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)			
				BREAK			
				CASE 13								
					vCoords 	=	<<2458.2578	,	3758.1418	,	41.1153	>>
					fHeading 	=	303.2986						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)			
				BREAK			
				CASE 14								
					vCoords 	=	<<2467.8015	,	3750.6018	,	41.4918	>>
					fHeading 	=	303.2986						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)			
				BREAK			
				CASE 15								
					vCoords 	=	<<2473.2534	,	3734.2852	,	41.4643	>>
					fHeading 	=	319.8912						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)				
				BREAK			
				CASE 16							
					vCoords 	=	<<2474.7927	,	3718.1887	,	43.5229	>>
					fHeading 	=	353.5786						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)			
				BREAK			
				CASE 17								
					vCoords 	=	<<2494.5686	,	3724.2971	,	42.2386	>>
					fHeading 	=	354.6885						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)			
				BREAK			
				CASE 18								
					vCoords 	=	<<2336.6045	,	3786.8047	,	35.4554	>>
					fHeading 	=	271.7747						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)			
				BREAK			
				CASE 19								
					vCoords		=	<<2449.8853	,	3769.5168	,	40.3259	>>
					fHeading 	=	284.606						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)				
				BREAK			
				CASE 20							
					vCoords 	=	<<2486.1914	,	3708.9319	,	42.8584	>>
					fHeading 	=	338.9825						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)			
				BREAK			
				CASE 21								
					vCoords 	=	<<2441.5249	,	3726.4927	,	49.833	>>
					fHeading 	=	305.777						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)				
				BREAK			
				CASE 22							
					vCoords 	=	<<2435.7161	,	3718.4797	,	50.9846	>>
					fHeading 	=	316.2304						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)			
				BREAK			
				CASE 23								
					vCoords 	=	<<2414.7297	,	3734.1436	,	41.656	>>
					fHeading 	=	304.645						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)			
				BREAK			
				CASE 24								
					vCoords 	=	<<2397.8293	,	3753.812	,	38.2667	>>
					fHeading 	=	277.5322						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)				
				BREAK			
				CASE 25							
					vCoords 	=	<<2413.9607	,	3768.8892	,	38.9386	>>
					fHeading 	=	262.8003						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)			
				BREAK			
				CASE 26								
					vCoords 	=	<<2418.1165	,	3784.6604	,	38.7937	>>
					fHeading 	=	271.0046						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)			
				BREAK			
				CASE 27								
					vCoords 	=	<<2402.4158	,	3792.6714	,	38.1692	>>
					fHeading 	=	252.6272						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)			
				BREAK			
				CASE 28								
					vCoords 	=	<<2392.1743	,	3800.2141	,	37.3174	>>
					fHeading 	=	233.5119						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)			
				BREAK			
				CASE 29								
					vCoords 	=	<<2379.0337	,	3800.1042	,	36.6627	>>
					fHeading 	=	253.7779						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)			
				BREAK			
				CASE 30								
					vCoords		=	<<2421.4197	,	3819.2175	,	36.6498	>>
					fHeading 	=	240.598						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)			
				BREAK			
				CASE 31								
					vCoords 	=	<<2433.6646	,	3833.3997	,	36.8711	>>
					fHeading 	=	235.1875						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)			
				BREAK			
				CASE 32								
					vCoords 	=	<<2446.7837	,	3840.4302	,	36.9341	>>
					fHeading 	=	232.4636						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)			
				BREAK			
				CASE 33								
					vCoords 	=	<<2462.1951	,	3846.708	,	37.3552	>>
					fHeading 	=	199.2328						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)			
				BREAK			
				CASE 34								
					vCoords 	=	<<2477.1333	,	3855.7542	,	37.2602	>>
					fHeading 	=	202.825						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)			
				BREAK			
				CASE 35								
					vCoords 	=	<<2450.0374	,	3811.5098	,	39.475	>>
					fHeading 	=	250.1392						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)			
				BREAK			
				CASE 36								
					vCoords 	=	<<2485.0647	,	3829.4417	,	39.1108	>>
					fHeading 	=	211.7988						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)			
				BREAK			
				CASE 37								
					vCoords 	=	<<2484.8298	,	3814.1406	,	39.3918	>>
					fHeading 	=	229.5693						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)			
				BREAK			
				CASE 38								
					vCoords 	=	<<2566.0852	,	3806.8542	,	64.6304	>>
					fHeading 	=	105.4003						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)			
				BREAK			
				CASE 39								
					vCoords 	=	<<2585.6636	,	3806.8542	,	71.7304	>>
					fHeading 	=	110.9439						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)				
				BREAK			
				CASE 40							
					vCoords 	=	<<2620.4988	,	3798.988	,	78.4414	>>
					fHeading 	=	80.1436						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)				
				BREAK			
				CASE 41							
					vCoords		=	<<2612.8306	,	3816.7251	,	77.148	>>
					fHeading 	=	105.885						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)			
				BREAK			
				CASE 42								
					vCoords 	=	<<2481.0029	,	3874.7219	,	38.2954	>>
					fHeading 	=	168.6165						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)			
				BREAK			
				CASE 43								
					vCoords 	=	<<2439.9307	,	3869.7268	,	36.8359	>>
					fHeading 	=	194.6768						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)			
				BREAK			
				CASE 44								
					vCoords 	=	<<2415.0559	,	3867.7405	,	36.4367	>>
					fHeading 	=	227.8888						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)			
				BREAK			
				CASE 45								
					vCoords 	=	<<2405.0571	,	3840.1289	,	37.6172	>>
					fHeading 	=	237.4134						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)			
				BREAK			
				CASE 46								
					vCoords 	=	<<2402.6426	,	3831.9866	,	37.3666	>>
					fHeading 	=	243.847						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)				
				BREAK			
				CASE 47							
					vCoords 	=	<<2393.4341	,	3820.8044	,	37.399	>>
					fHeading 	=	274.6625						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)			
				BREAK			
				CASE 48								
					vCoords 	=	<<2379.6448	,	3814.4709	,	37.141	>>
					fHeading 	=	261.4475						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)			
				BREAK			
				CASE 49								
					vCoords 	=	<<2362.3362	,	3795.1851	,	36.6862	>>
					fHeading 	=	270.3616						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)			
				BREAK			
				CASE 50								
					vCoords 	=	<<2347.0305	,	3771.605	,	36.6862	>>
					fHeading 	=	270.3616						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)			
				BREAK			
				CASE 51								
					vCoords 	=	<<2390.3516	,	3771.605	,	37.0186	>>
					fHeading 	=	277.3893						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)			
				BREAK			
				CASE 52								
					vCoords		=	<<2376.8694	,	3747.8899	,	43.6471	>>
					fHeading 	=	283.6571						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)			
				BREAK			
				CASE 53								
					vCoords 	=	<<2397.2896	,	3720.2244	,	45.2658	>>
					fHeading 	=	309.3541						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)			
				BREAK			
				CASE 54								
					vCoords 	=	<<2389.842	,	3708.4375	,	48.8025	>>
					fHeading 	=	328.7773						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)			
				BREAK			
				CASE 55								
					vCoords 	=	<<2414.2627	,	3688.0715	,	55.1156	>>
					fHeading 	=	318.0387						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)				
				BREAK			
				CASE 56							
					vCoords 	=	<<2425.9771	,	3703.0522	,	52.2051	>>
					fHeading 	=	321.8817						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)			
				BREAK			
				CASE 57								
					vCoords 	=	<<2457.9834	,	3723.8035	,	47.774	>>
					fHeading 	=	320.001						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)			
				BREAK			
				CASE 58								
					vCoords 	=	<<2467.429	,	3686.1089	,	46.7001	>>
					fHeading 	=	326.4034						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)			
				BREAK			
				CASE 59								
					vCoords 	=	<<2500.6707	,	3701.1533	,	40.9656	>>
					fHeading 	=	343.9347						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)						
				BREAK				
			ENDSWITCH
		BREAK
		CASE CASTLE_AREA_OBSERVATORY			
			SWITCH iSpawnPoint
				CASE 0			
					vCoords 	=	<<-393.9336	,	1106.4625	,	324.963	>>
					fHeading 	=	3.7402						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 1			
					vCoords 	=	<<-410.0362	,	1109.4043	,	324.9667	>>
					fHeading 	=	333.049						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)			
				BREAK			
				CASE 2						
					vCoords 	=	<<-428.784	,	1115.0994	,	325.868	>>
					fHeading 	=	300.9811						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)		
				BREAK			
				CASE 3							
					vCoords 	=	<<-447.0386	,	1119.3855	,	324.9694	>>
					fHeading 	=	291.3791						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)		
				BREAK			
				CASE 4							
					vCoords 	=	<<-442.3369	,	1130.7311	,	324.9604	>>
					fHeading 	=	277.1605						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)		
				BREAK			
				CASE 5							
					vCoords 	=	<<-440.9719	,	1142.6072	,	324.9557	>>
					fHeading 	=	250.3044						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 6								
					vCoords 	=	<<-446.4467	,	1154.6282	,	325.0045	>>
					fHeading 	=	248.4445						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 7								
					vCoords 	=	<<-434.5363	,	1160.3206	,	324.9632	>>
					fHeading 	=	230.0039						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)		
				BREAK			
				CASE 8							
					vCoords		=	<<-424.4414	,	1172.2935	,	325.0043	>>
					fHeading 	=	219.6946						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 9								
					vCoords 	=	<<-421.9871	,	1161.7736	,	325.0063	>>
					fHeading 	=	219.721						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 10								
					vCoords 	=	<<-427.4456	,	1148.0509	,	325.0044	>>
					fHeading 	=	230.6394						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 11								
					vCoords 	=	<<-428.7997	,	1136.8693	,	325.0044	>>
					fHeading 	=	260.5906						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 12								
					vCoords 	=	<<-431.5406	,	1126.3973	,	325.0403	>>
					fHeading 	=	280.8301						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 13								
					vCoords 	=	<<-416.0247	,	1120.7711	,	324.9593	>>
					fHeading 	=	308.4634						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 14								
					vCoords 	=	<<-419.4272	,	1131.455	,	325.0045	>>
					fHeading 	=	267.8754						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 15								
					vCoords 	=	<<-418.7003	,	1142.9387	,	324.962	>>
					fHeading 	=	234.7959						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 16								
					vCoords 	=	<<-408.7494	,	1158.5554	,	325.0084	>>
					fHeading 	=	202.298						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 17								
					vCoords 	=	<<-411.2803	,	1169.9507	,	324.9431	>>
					fHeading 	=	205.6539						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 18								
					vCoords 	=	<<-398.8847	,	1164.1007	,	325.0133	>>
					fHeading 	=	184.3107						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 19								
					vCoords		=	<<-388.1964	,	1173.0496	,	324.8147	>>
					fHeading 	=	166.4872						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 20								
					vCoords 	=	<<-368.8524	,	1165.4137	,	324.8589	>>
					fHeading 	=	143.9187						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 21								
					vCoords 	=	<<-378.9884	,	1168.1243	,	324.9442	>>
					fHeading 	=	138.8804						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 22								
					vCoords 	=	<<-386.2831	,	1186.9691	,	324.8583	>>
					fHeading 	=	168.679						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 23								
					vCoords 	=	<<-424.0227	,	1207.0771	,	324.8583	>>
					fHeading 	=	192.8885						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 24								
					vCoords 	=	<<-425.9954	,	1196.5197	,	324.8583	>>
					fHeading 	=	182.6931						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 25								
					vCoords 	=	<<-400.9391	,	1177.7406	,	324.7394	>>
					fHeading 	=	183.8522						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 26								
					vCoords 	=	<<-414.3408	,	1178.5253	,	324.7417	>>
					fHeading 	=	190.7249						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 27								
					vCoords 	=	<<-424.777	,	1183.848	,	324.7417	>>
					fHeading 	=	204.1173						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 28								
					vCoords 	=	<<-438.3099	,	1184.9691	,	324.9972	>>
					fHeading 	=	216.3494						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 29								
					vCoords 	=	<<-440.8298	,	1174.3301	,	324.9674	>>
					fHeading 	=	236.8431						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 30								
					vCoords		=	<<-450.9589	,	1165.9045	,	324.9547	>>
					fHeading 	=	238.6017						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 31								
					vCoords 	=	<<-459.008	,	1156.9045	,	324.9622	>>
					fHeading 	=	235.1455						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 32								
					vCoords 	=	<<-466.6086	,	1151.5485	,	324.9625	>>
					fHeading 	=	262.671						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 33								
					vCoords 	=	<<-463.0775	,	1140.1715	,	325.0044	>>
					fHeading 	=	255.8193						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 34								
					vCoords 	=	<<-453.6404	,	1141.6218	,	325.0044	>>
					fHeading 	=	270.7759						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 35								
					vCoords 	=	<<-468.3044	,	1126.4921	,	324.9643	>>
					fHeading 	=	263.469						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 36								
					vCoords 	=	<<-457.6102	,	1126.9038	,	324.9546	>>
					fHeading 	=	262.384						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 37								
					vCoords 	=	<<-385.6385	,	1102.2517	,	324.7293	>>
					fHeading 	=	25.2881						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 38								
					vCoords 	=	<<-383.8219	,	1093.5663	,	324.3149	>>
					fHeading 	=	2.7963						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 39								
					vCoords 	=	<<-390.1671	,	1092.1189	,	325.2136	>>
					fHeading 	=	20.6423						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 40								
					vCoords 	=	<<-399.5618	,	1081.5601	,	326.7102	>>
					fHeading 	=	304.4997						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 41								
					vCoords		=	<<-390.9579	,	1075.8285	,	323.3234	>>
					fHeading 	=	339.4828						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 42								
					vCoords 	=	<<-386.3601	,	1084.1661	,	323.7919	>>
					fHeading 	=	354.7784						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 43								
					vCoords 	=	<<-343.6368	,	1155.1866	,	324.7273	>>
					fHeading 	=	115.9307						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 44								
					vCoords 	=	<<-350.9581	,	1163.9796	,	324.5156	>>
					fHeading 	=	116.0754						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 45								
					vCoords 	=	<<-359.8979	,	1158.5658	,	324.6362	>>
					fHeading 	=	120.9973						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 46								
					vCoords 	=	<<-399.4072	,	1196.4825	,	324.7383	>>
					fHeading 	=	182.8968						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 47								
					vCoords 	=	<<-400.8836	,	1210.9829	,	325.0406	>>
					fHeading 	=	195.0742						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 48								
					vCoords 	=	<<-381.3676	,	1203.0039	,	324.8555	>>
					fHeading 	=	162.6808						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 49							
					vCoords 	=	<<-377.298	,	1218.6995	,	324.8556	>>
					fHeading 	=	161.6516						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 50								
					vCoords 	=	<<-416.3955	,	1238.6814	,	324.8614	>>
					fHeading 	=	196.7896						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 51								
					vCoords 	=	<<-420.2233	,	1223.6271	,	324.8551	>>
					fHeading 	=	195.917						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 52								
					vCoords		=	<<-449.2621	,	1187.4248	,	324.988	>>
					fHeading 	=	245.0986						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 53								
					vCoords 	=	<<-462.3227	,	1187.6145	,	324.7824	>>
					fHeading 	=	250.3676						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 54								
					vCoords 	=	<<-472.915	,	1142.9796	,	324.9524	>>
					fHeading 	=	260.1427						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 55								
					vCoords 	=	<<-476.2405	,	1120.7596	,	324.9513	>>
					fHeading 	=	281.8387						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 56								
					vCoords 	=	<<-480.6375	,	1107.1132	,	324.9517	>>
					fHeading 	=	330.5185						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 57								
					vCoords 	=	<<-467.9135	,	1119.1298	,	324.975	>>
					fHeading 	=	291.4161						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 58								
					vCoords 	=	<<-472.083	,	1186.2556	,	324.4922	>>
					fHeading 	=	248.7844						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 59								
					vCoords 	=	<<-336.9653	,	1155.261	,	324.8132	>>
					fHeading 	=	116.8097						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)						
				BREAK										
			ENDSWITCH
		BREAK
		CASE CASTLE_AREA_SCRAP_YARD					
			SWITCH iSpawnPoint
				CASE 0			
					vCoords 	=	<<457.789	,	-1306.329	,	28.3817	>>
					fHeading 	=	213.4484						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)		
				BREAK			
				CASE 1			
					vCoords 	=	<<462.9742	,	-1315.8162	,	28.3365	>>
					fHeading 	=	211.6687						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 2								
					vCoords 	=	<<491.3282	,	-1343.6619	,	28.3462	>>
					fHeading 	=	29.1021						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 3								
					vCoords 	=	<<488.5634	,	-1353.8071	,	28.365	>>
					fHeading 	=	350.4963						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 4								
					vCoords 	=	<<489.778	,	-1362.1779	,	28.3936	>>
					fHeading 	=	333.8013						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 5								
					vCoords 	=	<<487.9373	,	-1371.324	,	28.3937	>>
					fHeading 	=	350.2063						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 6								
					vCoords 	=	<<491.692	,	-1377.6813	,	28.4808	>>
					fHeading 	=	6.2341						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 7								
					vCoords 	=	<<508.4521	,	-1332.7253	,	28.8217	>>
					fHeading 	=	81.6066						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 8								
					vCoords		=	<<482.3437	,	-1302.9136	,	28.3646	>>
					fHeading 	=	185.9192						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 9								
					vCoords 	=	<<489.6881	,	-1306.1202	,	28.375	>>
					fHeading 	=	181.4375						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 10								
					vCoords 	=	<<486.8906	,	-1314.5516	,	28.3349	>>
					fHeading 	=	201.3654						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)		
				BREAK			
				CASE 11							
					vCoords 	=	<<510.3459	,	-1328.1589	,	28.4041	>>
					fHeading 	=	38.9104						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 12								
					vCoords 	=	<<503.6635	,	-1316.6436	,	28.254	>>
					fHeading 	=	78.5885						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 13								
					vCoords 	=	<<498.5652	,	-1306.8005	,	28.375	>>
					fHeading 	=	181.4375						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 14								
					vCoords 	=	<<494.3388	,	-1296.7885	,	27.8975	>>
					fHeading 	=	181.588						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)		
				BREAK			
				CASE 15							
					vCoords 	=	<<445.3724	,	-1312.7767	,	33.034	>>
					fHeading 	=	231.2194						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 16								
					vCoords 	=	<<494.6642	,	-1289.0895	,	28.3994	>>
					fHeading 	=	190.9093						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 17								
					vCoords 	=	<<512.1788	,	-1296.7981	,	29.0244	>>
					fHeading 	=	127.6323						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)		
				BREAK			
				CASE 18							
					vCoords 	=	<<516.3178	,	-1306.5907	,	29.0325	>>
					fHeading 	=	105.4519						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)		
				BREAK			
				CASE 19							
					vCoords		=	<<522.4741	,	-1315.7163	,	28.6153	>>
					fHeading 	=	86.1907						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)		
				BREAK			
				CASE 20							
					vCoords 	=	<<530.4	,	-1324.764	,	28.611	>>
					fHeading 	=	86.0417						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)		
				BREAK			
				CASE 21							
					vCoords 	=	<< 512.5676, -1266.1318, 29.2571>>
					fHeading 	=	138.7522						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 22								
					vCoords 	=	<<503.5058	,	-1408.5267	,	28.4094	>>
					fHeading 	=	38.8401						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 23								
					vCoords 	=	<<494.4435	,	-1405.5176	,	28.4094	>>
					fHeading 	=	10.3526						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 24								
					vCoords 	=	<<480.3399	,	-1404.5524	,	28.3783	>>
					fHeading 	=	327.4608						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 25								
					vCoords 	=	<<486.9532	,	-1397.7285	,	28.4044	>>
					fHeading 	=	9.2252						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 26								
					vCoords 	=	<<478.9936	,	-1390.9114	,	28.4649	>>
					fHeading 	=	301.6067						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 27								
					vCoords 	=	<<491.3508	,	-1387.0237	,	28.4781	>>
					fHeading 	=	349.894						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 28								
					vCoords 	=	<<423.8583	,	-1342.4025	,	34.9472	>>
					fHeading 	=	261.565						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)		
				BREAK			
				CASE 29							
					vCoords 	=	<<436.9487	,	-1354.9086	,	32.8767	>>
					fHeading 	=	281.1848						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)		
				BREAK			
				CASE 30							
					vCoords		=	<<418.0431	,	-1351.6908	,	30.4924	>>
					fHeading 	=	242.0479						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 31							
					vCoords 	=	<<420.5738	,	-1360.1549	,	31.3616	>>
					fHeading 	=	258.2115						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 32							
					vCoords 	=	<<427.6197	,	-1367.6482	,	32.4927	>>
					fHeading 	=	295.2934						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 33								
					vCoords 	=	<<464.2118	,	-1407.3252	,	28.4406	>>
					fHeading 	=	264.6266						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 34								
					vCoords 	=	<<473.348	,	-1411.3252	,	28.4462	>>
					fHeading 	=	282.62						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 35								
					vCoords 	=	<<482.7524	,	-1415.4581	,	28.2625	>>
					fHeading 	=	321.8356						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 36							
					vCoords 	=	<<495.2037	,	-1418.0582	,	28.2691	>>
					fHeading 	=	7.1158						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 37								
					vCoords 	=	<<503.2256	,	-1399.4984	,	29.6255	>>
					fHeading 	=	163.0629						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 38							
					vCoords 	=	<<504.0861	,	-1418.8737	,	28.399	>>
					fHeading 	=	19.1681						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 39								
					vCoords 	=	<<515.1004	,	-1418.2662	,	28.4334	>>
					fHeading 	=	70.436						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 40							
					vCoords 	=	<<526.7444	,	-1417.3265	,	28.3217	>>
					fHeading 	=	77.2632						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 41							
					vCoords		=	<<537.8759	,	-1339.4774	,	28.7478	>>
					fHeading 	=	32.234						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 42							
					vCoords 	=	<<523.1588	,	-1342.8318	,	28.402	>>
					fHeading 	=	44.3382						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 43							
					vCoords 	=	<<516.2181	,	-1333.8085	,	28.4019	>>
					fHeading 	=	42.2223						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 44							
					vCoords 	=	<<545.4319	,	-1321.2938	,	28.8519	>>
					fHeading 	=	82.4327						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 45							
					vCoords 	=	<<543.2325	,	-1310.2601	,	29.2949	>>
					fHeading 	=	90.2737						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 46							
					vCoords 	=	<<519.066	,	-1293.9352	,	27.1144	>>
					fHeading 	=	165.5317						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 47							
					vCoords 	=	<<526.7755	,	-1289.459	,	29.7686	>>
					fHeading 	=	143.1124						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 48							
					vCoords 	=	<<523.58	,	-1272.3276	,	30.0824	>>
					fHeading 	=	138.49						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 49							
					vCoords 	=	<<524.081	,	-1260.6821	,	29.5842	>>
					fHeading 	=	132.1786						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 50								
					vCoords 	=	<<517.0681	,	-1239.373	,	29.9303	>>
					fHeading 	=	154.6133						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 51							
					vCoords 	=	<<516.2507	,	-1253.0201	,	29.6427	>>
					fHeading 	=	162.9373						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 52							
					vCoords		=	<<513.244	,	-1278.3508	,	29.7743	>>
					fHeading 	=	150.8734						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 53							
					vCoords 	=	<<493.7339	,	-1268.2126	,	28.392	>>
					fHeading 	=	179.546						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 54							
					vCoords 	=	<<494.1401	,	-1281.4906	,	28.394	>>
					fHeading 	=	175.0507						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 55							
					vCoords 	=	<<555.4178	,	-1356.7433	,	28.7709	>>
					fHeading 	=	50.8085						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 56							
					vCoords 	=	<<475.9386	,	-1304.7938	,	32.0473	>>
					fHeading 	=	112.0399						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 57							
					vCoords 	=	<<465.516	,	-1301.6041	,	32.0473	>>
					fHeading 	=	189.1235						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 58							
					vCoords 	=	<<440.8456	,	-1321.1478	,	30.3086	>>
					fHeading 	=	235.1875						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 59								
					vCoords 	=	<<421.228	,	-1316.1882	,	30.3078	>>
					fHeading 	=	236.0292						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)						
				BREAK				
			ENDSWITCH
		BREAK
		CASE CASTLE_AREA_PLAYBOY_GROTTO					
			SWITCH iSpawnPoint
				CASE 0			
					vCoords 	=	<<-1432.4506	,	236.9887	,	59.1671	>>
					fHeading 	=	127.6804						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)		
				BREAK			
				CASE 1			
					vCoords 	=	<<-1434.5403	,	243.4075	,	59.3382	>>
					fHeading 	=	176.5221						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)		
				BREAK			
				CASE 2							
					vCoords 	=	<<-1425.9647	,	233.6748	,	58.9327	>>
					fHeading 	=	67.1914						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 3								
					vCoords 	=	<<-1431.8331	,	176.7196	,	55.6359	>>
					fHeading 	=	115.4533						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 4								
					vCoords 	=	<<-1436.5707	,	168.3388	,	54.8029	>>
					fHeading 	=	42.4646						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 5								
					vCoords 	=	<<-1441.1395	,	162.3594	,	54.1846	>>
					fHeading 	=	5.2323						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 6								
					vCoords 	=	<<-1526.2965	,	160.2353	,	53.2037	>>
					fHeading 	=	277.6807						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 7								
					vCoords 	=	<<-1518.4971	,	145.7663	,	54.7527	>>
					fHeading 	=	310.257						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 8								
					vCoords		=	<<-1506.654	,	132.8194	,	54.7529	>>
					fHeading 	=	324.8665						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 9								
					vCoords 	=	<<-1495.2866	,	118.9496	,	54.7569	>>
					fHeading 	=	328.5237						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 10								
					vCoords 	=	<<-1482.5004	,	114.8686	,	54.589	>>
					fHeading 	=	341.3174						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 11								
					vCoords 	=	<<-1465.1595, 114.8327, 52.2589>>
					fHeading 	=	357.6763						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 12								
					vCoords 	=	<<-1451.5073, 64.7578, 51.4041>>
					fHeading 	=	91.7588						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 13								
					vCoords 	=	<<-1461.1804	,	63.8165	,	51.8802	>>
					fHeading 	=	26.7651						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 14								
					vCoords 	=	<< -1502.689, 44.4251, 53.8926>>
					fHeading 	=	308.9227					
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 15								
					vCoords 	=	<<-1470.5115	,	65.7823	,	52.315	>>
					fHeading 	=	20.0152						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 16								
					vCoords 	=	<< -1485.597, 73.3039, 53.7206>>
					fHeading 	=	353.3321						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 17								
					vCoords 	=	<< -1598.5277, 149.7495, 58.6949>>
					fHeading 	=	277.3246							
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 18								
					vCoords 	=	<<-1583.4247	,	155.9397	,	57.9714	>>
					fHeading 	=	236.3493						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 19								
					vCoords		=	<<-1561.7214	,	153.229	,	56.9434	>>
					fHeading 	=	282.9456						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_A)	
				BREAK			
				CASE 20								
					vCoords 	=	<<-1597.874	,	138.8909	,	58.9224	>>
					fHeading 	=	291.1977						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)		
				BREAK			
				CASE 21							
					vCoords 	=	<<-1624.6096	,	118.2495	,	60.9528	>>
					fHeading 	=	286.5444						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 22								
					vCoords 	=	<<-1635.5459	,	98.7833	,	61.5816	>>
					fHeading 	=	307.8016						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 23								
					vCoords 	=	<<-1615.5929	,	74.5104	,	60.5005	>>
					fHeading 	=	358.8846						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 24								
					vCoords 	=	<<-1623.2659	,	72.784	,	60.7418	>>
					fHeading 	=	303.1707						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 25								
					vCoords 	=	<<-1611.0909	,	66.0932	,	60.1192	>>
					fHeading 	=	8.1702						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 26								
					vCoords 	=	<<-1485.7559	,	49.183	,	53.0408	>>
					fHeading 	=	329.1895						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 27								
					vCoords 	=	<<-1464.8406	,	52.066	,	52.0575	>>
					fHeading 	=	26.7727						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)		
				BREAK			
				CASE 28							
					vCoords 	=	<<-1450.8086	,	49.7653	,	51.6797	>>
					fHeading 	=	42.6481						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 29								
					vCoords 	=	<<-1462.3258	,	88.4265	,	53.8826	>>
					fHeading 	=	353.3917						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 30								
					vCoords		=	<<-1491.8698	,	95.0945	,	54.1122	>>
					fHeading 	=	314.473						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 31								
					vCoords 	=	<<-1476.8082	,	108.3742	,	53.8967	>>
					fHeading 	=	348.5514						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 32								
					vCoords 	=	<<-1443.9645	,	153.6641	,	53.4228	>>
					fHeading 	=	5.4015						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 33								
					vCoords 	=	<<-1422.5583	,	161.544	,	55.0401	>>
					fHeading 	=	41.9873						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 34								
					vCoords 	=	<<-1425.5946	,	187.9517	,	56.689	>>
					fHeading 	=	129.5252						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 35								
					vCoords 	=	<<-1411.3945	,	239.4108	,	58.8958	>>
					fHeading 	=	94.2972						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 36								
					vCoords 	=	<<-1415.5724	,	249.2832	,	59.1409	>>
					fHeading 	=	122.1957						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 37								
					vCoords 	=	<<-1459.7379	,	238.6189	,	58.7949	>>
					fHeading 	=	180.7222						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 38								
					vCoords 	=	<<-1573.8116	,	161.2271	,	57.4846	>>
					fHeading 	=	134.8161						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)	
				BREAK			
				CASE 39								
					vCoords 	=	<<-1589.4623	,	172.0972	,	57.9565	>>
					fHeading 	=	211.4448						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_B)		
				BREAK			
				CASE 40							
					vCoords 	=	<<-1599.6311	,	177.347	,	58.4655	>>
					fHeading 	=	218.6622						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 41							
					vCoords		=	<<-1605.2471	,	166.7319	,	58.6104	>>
					fHeading 	=	240.273						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 42								
					vCoords 	=	<<-1610.4893	,	133.2583	,	59.7152	>>
					fHeading 	=	286.0001						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 43								
					vCoords 	=	<<-1609.1329	,	118.5496	,	59.985	>>
					fHeading 	=	309.8568						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 44								
					vCoords 	=	<<-1600.1472	,	104.7807	,	60.079	>>
					fHeading 	=	345.0445						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 45								
					vCoords 	=	<<-1595.3364	,	88.6422	,	59.9101	>>
					fHeading 	=	325.4662						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 46								
					vCoords 	=	<<-1571.0393	,	95.7035	,	57.4104	>>
					fHeading 	=	320.9921						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 47								
					vCoords 	=	<<-1528.822	,	60.1519	,	56.3681	>>
					fHeading 	=	299.9443						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 48								
					vCoords 	=	<<-1581.0634	,	69.4907	,	59.9072	>>
					fHeading 	=	302.81						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 49								
					vCoords 	=	<<-1502.4277	,	75.9977	,	54.5965	>>
					fHeading 	=	333.9787						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 50							
					vCoords 	=	<<-1533.2805	,	81.1755	,	55.8724	>>
					fHeading 	=	288.1328						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 51								
					vCoords 	=	<<-1533.9839	,	102.1409	,	55.8727	>>
					fHeading 	=	232.9465						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 52							
					vCoords		=	<<-1598.3889	,	49.5038	,	59.7089	>>
					fHeading 	=	24.7707						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 53							
					vCoords 	=	<<-1613.3031	,	45.0376	,	60.9909	>>
					fHeading 	=	352.9884						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 54							
					vCoords 	=	<<-1632.7849	,	50.5315	,	61.543	>>
					fHeading 	=	325.0906						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 55								
					vCoords 	=	<<-1636.2584	,	69.9035	,	61.7993	>>
					fHeading 	=	291.5305						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 56								
					vCoords 	=	<<-1429.6908	,	143.2913	,	53.2468	>>
					fHeading 	=	17.521						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 57								
					vCoords 	=	<<-1408.5233	,	152.6476	,	54.6591	>>
					fHeading 	=	54.2465						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)	
				BREAK			
				CASE 58								
					vCoords 	=	<<-1403.5007	,	245.4762	,	59.2192	>>
					fHeading 	=	112.9726						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)		
				BREAK			
				CASE 59							
					vCoords 	=	<<-1417.5635	,	260.8751	,	59.5187	>>
					fHeading 	=	146.682						
					fWeighting 	=	GET_SPAWN_POINT_WEIGHTING(iKingCastleVariation, ESPAWNPOINTWEIGHTING_C)						
				BREAK				
			ENDSWITCH
		BREAK
	ENDSWITCH
	
ENDPROC

PROC SETUP_CUSTOM_SPAWN_POINTS()
	
	INT iCastleArea
	INT iSpawnPoint
	VECTOR vCoords
	FLOAT fHeading, fWeighting
	
	REPEAT NUM_CASTLE_AREAS iCastleArea
		REPEAT NUM_CASTLE_AREA_SPAWN_POINTS iSpawnPoint
			GET_SPAWN_POINT_DATA(GET_CASTLE_AREA_SEED(iCastleArea), iSpawnPoint, vCoords, fHeading, fWeighting)
			ADD_CUSTOM_SPAWN_POINT(vCoords, fHeading, fWeighting)
		ENDREPEAT
	ENDREPEAT
	
ENDPROC

FUNC BOOL HAS_MODE_TIMER_DIFF_WITH_CURRENT_TIME_GONE_WRONG()
	
	IF MPGlobalsAmbience.bTurnOffKingOftheCastleTimerFailsafeFunction 
		RETURN FALSE
	ENDIF
	
	INT iDiff, iLimit
	SCRIPT_TIMER timerTemp
	
	timerTemp.Timer = GET_NETWORK_TIME_ACCURATE()
	
	IF HAS_NET_TIMER_STARTED(serverBd.sKingOfTheCastle.ModeTimer)
		
		iDiff = GET_ABS_NET_TIMER_DIFFERENCE(timerTemp, serverBd.sKingOfTheCastle.ModeTimer)
		iLimit = g_sMPTunables.iKing_of_the_Castle_Event_Time_Limit
		
	ELIF HAS_NET_TIMER_STARTED(serverBd.sKingOfTheCastle.AvailableTimer)
		
		iDiff = GET_ABS_NET_TIMER_DIFFERENCE(timerTemp, serverBd.sKingOfTheCastle.AvailableTimer)
		iLimit = g_sMPTunables.iKing_of_the_Castle_Event_Expiry_Time
		
	ELSE
		
		RETURN FALSE
		
	ENDIF
	
	iLimit += (1000*60*5) // 5 minute leeway.
	
	IF iDiff > iLimit
		PRINTLN("[KINGCASTLE] - HAS_MODE_TIMER_DIFF_WITH_CURRENT_TIME_GONE_WRONG = TRUE - iDiff = ", iDiff)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

//Do necessary pre game start ini.
PROC PROCESS_PRE_GAME( MP_MISSION_DATA missionScriptArgs )
	
	INT iSeed
	INT iCastleArea
	INT iCount
	
	// This marks the script as a net script, and handles any instancing setup. 
	INT iMaxNumPartsForMpMission = GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission)
	SETUP_MP_MISSION_FOR_NETWORK(iMaxNumPartsForMpMission,  missionScriptArgs)
	PRINTLN("[KINGCASTLE] - [PROCESS_PRE_GAME] - called SETUP_MP_MISSION_FOR_NETWORK. iMaxNumPartsForMpMission = ", iMaxNumPartsForMpMission, ", idMission = ", missionScriptArgs.mdID.idMission)
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	//common event proccess pre game
	COMMON_FREEMODE_AMBIENT_EVENTS_PROCESS_PRE_GAME(FMMC_TYPE_KING_CASTLE)
		
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	PRINTLN("[KINGCASTLE] - [PROCESS_PRE_GAME] - registered broadcast data.")
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		PRINTLN("[KINGCASTLE] - [PROCESS_PRE_GAME] - Wait_For_First_Network_Broadcast() = FALSE.")
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	
		// This script will not be paused if another script calls PAUSE_GAME
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
		PRINTLN("[KINGCASTLE] - [PROCESS_PRE_GAME] - Wait_For_First_Network_Broadcast() = FALSE.")
		
		
		SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_KING_OF_THE_CASTLE, TRUE)
		PRINTLN("[KINGCASTLE] - [PROCESS_PRE_GAME] - set player on ambient script.")
		
		PRINTLN("[KINGCASTLE] - [PROCESS_PRE_GAME] - My Participant ID is NETWORK_GET_PARTICIPANT_INDEX = ", NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(PLAYER_ID())), " Name = ", GET_PLAYER_NAME(PLAYER_ID()))
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
						
			iSeed = GET_VALID_KING_OF_THE_CASTLE_VARIATION()
			BROADCAST_SET_LAST_FM_EVENT_VARIATION(FM_EVENT_GET_VARIATION_FROM_EVENT_TYPE_AND_SUB_TYPE(FMMC_TYPE_KING_CASTLE, iSeed))
			
			REPEAT NUM_CASTLE_AREAS iCastleArea
				SET_CASTLE_AREA_SEED(iCastleArea, iSeed)
			ENDREPEAT
			
			PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBD.iHashedMac, serverBD.iMatchHistoryId)
			
		ENDIF
		
		telemetryStruct.m_playersNotified = NETWORK_GET_TOTAL_NUM_PLAYERS()
		telemetryStruct.m_startTime	= GET_CLOUD_TIME_AS_INT()
		telemetryStruct.m_notifiedTime = GET_CLOUD_TIME_AS_INT()
		
		REPEAT NUM_NETWORK_PLAYERS iCount
			serverBD.iHighestParticipant[iCount] = (-1)
		ENDREPEAT
		
		SET_CLIENT_GAME_STATE(eGAMESTATE_INI)
		
	ELSE
		
		PRINTLN("[KINGCASTLE] - [PROCESS_PRE_GAME] - NETWORK_IS_GAME_IN_PROGRESS = FALSE.")
		SCRIPT_CLEANUP()
		
	ENDIF
	
ENDPROC

PROC PROCESS_PARTICIPANT_INSIDE_CASTLE_AREA(INT iCastleArea)
	
	INT iLocate
	VECTOR vMinimum[MAX_NUM_LOCATES]
	VECTOR vMaximum[MAX_NUM_LOCATES]
	FLOAT fWidth[MAX_NUM_LOCATES]
	VECTOR vCircularAreaCoords[MAX_NUM_LOCATES]
	FLOAT fCircularAreaRadius[MAX_NUM_LOCATES]
	
	BOOL bIAmInsideCastleArea
	
	FLOAT fHeight = KING_OF_THE_CASTLE_HEIGHT
	FLOAT fTriggerRadius = KING_OF_THE_CASTLE_TRIGGER_RADIUS
	
	REPEAT MAX_NUM_LOCATES iLocate
		
		IF GET_CASTLE_AREA_SEED(iCastleArea) = CASTLE_AREA_PLAYBOY_GROTTO
			fHeight = 5
			fTriggerRadius = 9.000
		ENDIF
		
		vMinimum[iLocate] =  GET_CASTLE_AREA_LOCATE_MIN(iCastleArea, iLocate)
		vMaximum[iLocate] = GET_CASTLE_AREA_LOCATE_MAX(iCastleArea, iLocate)
		fWidth[iLocate] = GET_CASTLE_AREA_LOCATE_WIDTH(iCastleArea, iLocate)
		
		GET_CIRCULAR_LOCATE_DATA_FROM_SEED(iCastleArea, iLocate, vCircularAreaCoords[iLocate], fCircularAreaRadius[iLocate])
		
		IF IS_BIT_SET(iParticipantActiveBitset, PARTICIPANT_ID_TO_INT())
			IF IS_BIT_SET(iPlayerOkBitset, sParticipant[PARTICIPANT_ID_TO_INT()].iPlayerId)
				IF NOT IS_BIT_SET(iPlayerDeadBitset, sParticipant[PARTICIPANT_ID_TO_INT()].iPlayerId)	
					IF NOT SHOULD_HIDE_THIS_AMBIENT_EVENT(TRUE, TRUE)
						IF iFocusParticipant = PARTICIPANT_ID_TO_INT()
							IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
								IF NOT IS_VECTOR_ZERO(vMinimum[iLocate])
								AND NOT IS_VECTOR_ZERO(vMaximum[iLocate])
								AND fWidth[iLocate] > 0.0
									IF IS_ENTITY_IN_ANGLED_AREA(sPlayer[NATIVE_TO_INT(PLAYER_ID())].playerPedId, vMinimum[iLocate], vMaximum[iLocate], fWidth[iLocate], FALSE, TRUE, TM_ANY)
										bIAmInsideCastleArea = TRUE
									ENDIF
								ENDIF
								IF GET_CASTLE_AREA_SEED(iCastleArea) = CASTLE_AREA_PLAYBOY_GROTTO
									IF GET_PARTICIPANT_Z_DISTANCE_FROM_CASTLE_AREA_CENTER_POINT(iCastleArea) < 0.0
										IF GET_PARTICIPANT_Z_DISTANCE_FROM_CASTLE_AREA_CENTER_POINT(iCastleArea) <= fHeight
										AND GET_PARTICIPANT_Z_DISTANCE_FROM_CASTLE_AREA_CENTER_POINT(iCastleArea) >= fHeight*(-1)
											IF GET_PARTICIPANT_2D_DISTANCE_FROM_CASTLE_AREA_CENTER_POINT(iCastleArea) <= fTriggerRadius
												bIAmInsideCastleArea = TRUE
											ENDIF
										ENDIF
									ENDIF
								ELSE
									IF NOT IS_VECTOR_ZERO(vCircularAreaCoords[iLocate])
										IF GET_PARTICIPANT_Z_DISTANCE_FROM_CASTLE_CIRCULAR_LOCATE_POINT(iCastleArea, iLocate) < 0.0
											IF GET_PARTICIPANT_Z_DISTANCE_FROM_CASTLE_CIRCULAR_LOCATE_POINT(iCastleArea, iLocate) >= fCircularAreaRadius[iLocate]*(-1)
												IF GET_PARTICIPANT_2D_DISTANCE_FROM_CASTLE_CIRCULAR_LOCATE_POINT(iCastleArea, iLocate) <= fCircularAreaRadius[iLocate]
													bIAmInsideCastleArea = TRUE
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDREPEAT
	
	SET_I_AM_INSIDE_CASTLE_AREA(iCastleArea, bIAmInsideCastleArea)
	
ENDPROC

PROC PROCESS_CASTLE_AREA_KING_POINTS(INT iCastleArea)
	
	INT iKingPart = GET_CASTLE_AREA_KING_PARTICIPANT_ID(iCastleArea)
	INT iKingPlayer = GET_CASTLE_AREA_KING_PLAYER_ID(iCastleArea)
	FLOAT fKingAreaPoints
	FLOAT fTime, fScore
	
	BOOL bResetTimer
	BOOL bHasTimerStarted = HAS_NET_TIMER_STARTED(serverBD.sCastleArea[iCastleArea].KingInUncontestedAreaTimer)
	
	IF iKingPart > (-1)
		
		IF IS_BIT_SET(iParticipantActiveBitset, iKingPart)
			IF IS_BIT_SET(iPlayerOkBitset, iKingPlayer)
				IF NOT IS_BIT_SET(iPlayerDeadBitset, iKingPlayer)
					
					fKingAreaPoints = GET_CASTLE_AREA_KING_AREA_POINTS(iCastleArea, iKingPart)
					
//					IF NOT GET_IS_CASTLE_AREA_CONTESTED(iCastleArea)
						
						IF HAS_NET_TIMER_STARTED(serverBD.sKingOfTheCastle.ModeTimer)
							IF NOT HAS_NET_TIMER_STARTED(serverBD.sCastleArea[iCastleArea].KingInUncontestedAreaTimer)
								START_NET_TIMER(serverBD.sCastleArea[iCastleArea].KingInUncontestedAreaTimer, FALSE, TRUE)
							ELSE
								IF HAS_NET_TIMER_EXPIRED(serverBD.sCastleArea[iCastleArea].KingInUncontestedAreaTimer, /*(g_sMPTunables.iKing_of_the_Castle_Seconds_as_King_to_earn_Points * */ 1000)
								AND GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.sKingOfTheCastle.ModeTimer) <= g_sMPTunables.iKing_of_the_Castle_Event_Time_Limit					// We shouldn't give scores after the time is up
								AND (GET_KING_OF_THE_CASTLE_STATE() <> eKINGOFTHECASTLESTATE_TIME_UP)
								
									PRINTLN("[KINGCASTLE] - [CASTLE AREA KING] ")
									PRINTLN("[KINGCASTLE] - [CASTLE AREA KING] ")
									INT iTime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.sCastleArea[iCastleArea].KingInUncontestedAreaTimer, FALSE, TRUE)
									PRINTLN("[KINGCASTLE] - [CASTLE AREA KING] - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME, iTime = ", iTime)
									fTime = TO_FLOAT(iTime)
									PRINTLN("[KINGCASTLE] - [CASTLE AREA KING] - TO_FLOAT(iTime) = ", fTime)
									fTime = (fTime/(g_sMPTunables.iKing_of_the_Castle_Seconds_as_King_to_earn_Points * 1000))
									PRINTLN("[KINGCASTLE] - [CASTLE AREA KING] - (fTime/(g_sMPTunables.iKing_of_the_Castle_Seconds_as_King_to_earn_Points * 1000)) = ", fTime)
									fScore = TO_FLOAT(g_sMPTunables.iKing_of_the_Castle_Points_per_X_seconds_as_King)*fTime
									PRINTLN("[KINGCASTLE] - [CASTLE AREA KING] - TO_FLOAT(g_sMPTunables.iKing_of_the_Castle_Points_per_X_seconds_as_King)*fTime = ", fTime)
									fKingAreaPoints += fScore
									PRINTLN("[KINGCASTLE] - [CASTLE AREA KING] - fKingAreaPoints += fScore ", fKingAreaPoints)
									SET_CASTLE_AREA_KING_AREA_POINTS(iCastleArea, iKingPart, fKingAreaPoints)
									bResetTimer = TRUE
									
									#IF IS_DEBUG_BUILD
									INT iTemp = (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.sKingOfTheCastle.ModeTimer, FALSE, TRUE)/1000)
									PRINTLN("[KINGCASTLE] - [CASTLE AREA KING] - (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.sKingOfTheCastle.ModeTimer, FALSE, TRUE)/1000) = ", iTemp)
									#ENDIF
									
								ENDIF
							ENDIF
						ENDIF
						
//					ELSE
//						IF bHasTimerStarted
//							bResetTimer = TRUE
//							PRINTLN("[KINGCASTLE] - [KING AREA POINTS] - bResetTimer = TRUE, GET_IS_CASTLE_AREA_CONTESTED = TRUE.")
//						ENDIF
//					ENDIF
					
				ELSE
					IF bHasTimerStarted
						bResetTimer = TRUE
						PRINTLN("[KINGCASTLE] - [KING AREA POINTS] - bResetTimer = TRUE, king is dead.")
					ENDIF
				ENDIF
			ELSE
				IF bHasTimerStarted
					bResetTimer = TRUE
					PRINTLN("[KINGCASTLE] - [KING AREA POINTS] - bResetTimer = TRUE, king is not net ok.")
				ENDIF
			ENDIF
		ELSE
			IF bHasTimerStarted
				bResetTimer = TRUE
				PRINTLN("[KINGCASTLE] - [KING AREA POINTS] - bResetTimer = TRUE, king participant is not active.")
			ENDIF
		ENDIF
	ELSE
		IF bHasTimerStarted
			bResetTimer = TRUE
//			PRINTLN("[KINGCASTLE] - [KING AREA POINTS] - bResetTimer = TRUE, iKingPart <= (-1).")
		ENDIF
	ENDIF
		
	IF bResetTimer
		IF bHasTimerStarted
//			PRINTLN("[KINGCASTLE] - [KING AREA POINTS] - bResetTimer = TRUE, resetting KingInUncontestedAreaTimer.")
			RESET_NET_TIMER(serverBD.sCastleArea[iCastleArea].KingInUncontestedAreaTimer)
			IF iKingPart > (-1)
				START_NET_TIMER(serverBD.sCastleArea[iCastleArea].KingInUncontestedAreaTimer, FALSE, TRUE)
	//			INT iTemp
	//			INT iTemp2
	//			iTemp = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.sKingOfTheCastle.ModeTimer, FALSE, TRUE)
	//			iTemp2 = (iTemp - 1000)
	//			SET_NET_TIMER_TO_SAME_VALUE_AS_TIMER(serverBD.sCastleArea[iCastleArea].KingInUncontestedAreaTimer, GET_TIME_OFFSET(serverBD.sKingOfTheCastle.ModeTimer.Timer, -iTemp2), FALSE, TRUE)
//				INT int1 = NATIVE_TO_INT(serverBD.sCastleArea[iCastleArea].KingInUncontestedAreaTimer.Timer)
//				INT int2 = NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE())
//				PRINTLN("[KINGCASTLE] - [CASTLE AREA KING] - (after) - KingInUncontestedAreaTimer = ", int1, ", GET_NETWORK_TIME_ACCURATE = ", int2)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC FILL_LEADERBOARD_DATA(INT iParticipant, INT iPlayer, INT iCastleArea, FLOAT fTotalKingPoints)
	
	serverBD.sCastleArea[iCastleArea].sLeaderboardData[iParticipant].iParticipant = iParticipant
	serverBD.sCastleArea[iCastleArea].sLeaderboardData[iParticipant].iPlayer = iPlayer
	serverBD.sCastleArea[iCastleArea].sLeaderboardData[iParticipant].playerId = INT_TO_NATIVE(PLAYER_INDEX, iPlayer)
	serverBD.sCastleArea[iCastleArea].sLeaderboardData[iParticipant].fTotalKingPoints = fTotalKingPoints
	
ENDPROC

PROC CLEAR_LEADERBOARD_DATA(INT iParticipant, INT iCastleArea)
	
	serverBD.sCastleArea[iCastleArea].sLeaderboardData[iParticipant].iParticipant = (-1)
	serverBD.sCastleArea[iCastleArea].sLeaderboardData[iParticipant].iPlayer = (-1)
	serverBD.sCastleArea[iCastleArea].sLeaderboardData[iParticipant].playerId = INVALID_PLAYER_INDEX()
	serverBD.sCastleArea[iCastleArea].sLeaderboardData[iParticipant].fTotalKingPoints = (-1)
	
ENDPROC

PROC SWAP_LEADERBOARD_PARTICIPANTS(INT iCastleArea, INT iPartBeingMovedUp, INT iPartBeingMovedDown, INT iPlacingToUpdate)
	
	// Print lb sorting data every 10 seconds.
	#IF IS_DEBUG_BUILD
	BOOL bTimerSaysDebugDump
	
	IF NOT HAS_NET_TIMER_STARTED(timerSortingDebugDump)
		START_NET_TIMER(timerSortingDebugDump)
	ELSE
		IF HAS_NET_TIMER_EXPIRED(timerSortingDebugDump, SORTING_DEBUG_DUMP_TIME)
			bTimerSaysDebugDump = TRUE
			RESET_NET_TIMER(timerSortingDebugDump)
		ENDIF	
	ENDIF
	#ENDIF
	
	FLOAT fTempPoints = serverBD.sCastleArea[iCastleArea].sLeaderboardData[iPartBeingMovedUp].fTotalKingPoints
	INT iTempPartId = serverBD.sCastleArea[iCastleArea].sLeaderboardData[iPartBeingMovedUp].iParticipant
	INT iTempPlayerId = serverBD.sCastleArea[iCastleArea].sLeaderboardData[iPartBeingMovedUp].iPlayer
	
	IF iPlacingToUpdate <= (NUM_NETWORK_PLAYERS-1)
		serverBD.fHighestScore[iPlacingToUpdate] = serverBD.sCastleArea[iCastleArea].sLeaderboardData[iPartBeingMovedUp].fTotalKingPoints
		serverBD.iHighestParticipant[iPlacingToUpdate] = serverBD.sCastleArea[iCastleArea].sLeaderboardData[iPartBeingMovedUp].iParticipant
		#IF IS_DEBUG_BUILD
		IF bPrintLeaderboardSortingDebug
		OR bTimerSaysDebugDump
			PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA")
			PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA - SWAP_LEADERBOARD_PARTICIPANTS - serverBD.fHighestScore[", iPlacingToUpdate, "] = ", serverBD.fHighestScore[iPlacingToUpdate])
			PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA - SWAP_LEADERBOARD_PARTICIPANTS - serverBD.iHighestParticipant[", iPlacingToUpdate, "] = ", serverBD.iHighestParticipant[iPlacingToUpdate])
		ENDIF
		#ENDIF
	ENDIF
	
	serverBD.sCastleArea[iCastleArea].sLeaderboardData[iPartBeingMovedUp].fTotalKingPoints = serverBD.sCastleArea[iCastleArea].sLeaderboardData[iPartBeingMovedDown].fTotalKingPoints
	serverBD.sCastleArea[iCastleArea].sLeaderboardData[iPartBeingMovedUp].iParticipant = serverBD.sCastleArea[iCastleArea].sLeaderboardData[iPartBeingMovedDown].iParticipant
	serverBD.sCastleArea[iCastleArea].sLeaderboardData[iPartBeingMovedUp].iPlayer = serverBD.sCastleArea[iCastleArea].sLeaderboardData[iPartBeingMovedDown].iPlayer
	
	serverBD.sCastleArea[iCastleArea].sLeaderboardData[iPartBeingMovedDown].fTotalKingPoints = fTempPoints
	serverBD.sCastleArea[iCastleArea].sLeaderboardData[iPartBeingMovedDown].iParticipant = iTempPartId
	serverBD.sCastleArea[iCastleArea].sLeaderboardData[iPartBeingMovedDown].iPlayer = iTempPlayerId
	
ENDPROC
/*
PROC SORT_LEADERBOARD_DATA(INT iCastleArea)
	
	// Print lb sorting data every 10 seconds.
	#IF IS_DEBUG_BUILD
	BOOL bTimerSaysDebugDump
	
	IF NOT HAS_NET_TIMER_STARTED(timerSortingDebugDump)
		START_NET_TIMER(timerSortingDebugDump)
	ELSE
		IF HAS_NET_TIMER_EXPIRED(timerSortingDebugDump, SORTING_DEBUG_DUMP_TIME)
			bTimerSaysDebugDump = TRUE
			RESET_NET_TIMER(timerSortingDebugDump)
		ENDIF	
	ENDIF
	#ENDIF
	
	INT iCount, iCount2
	
	FOR iCount = (NUM_NETWORK_PLAYERS - 1) TO 0 STEP -1
		
		#IF IS_DEBUG_BUILD
		IF bPrintLeaderboardSortingDebug
		OR bTimerSaysDebugDump
			PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA")
			PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA")
			PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA *********** PARTICIPANT ", iCount, " ***********")
		ENDIF
		#ENDIF
		
		FOR iCount2 = (NUM_NETWORK_PLAYERS - 1) TO 0 STEP -1
		
			IF ( (iCount2 - 1) >= 0 )
				
				#IF IS_DEBUG_BUILD
				INT iCount3 = iCount2 - 1
				IF bPrintLeaderboardSortingDebug
				OR bTimerSaysDebugDump
					PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA")
					PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA ---------- slot being checked = ", iCount2, ", possible slot to swap with = ", iCount3, " ----------")
					PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA slot ", iCount2, " points = ", serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2].fTotalKingPoints, ", slot ", iCount2-1, " points = ", serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2-1].fTotalKingPoints)
					PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA slot ", iCount2, " part = ", serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2].iParticipant, ", slot ", iCount2-1, " part = ", serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2-1].iParticipant)
					PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA slot ", iCount2, " player = ", serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2].iPlayer, ", slot ", iCount2-1, " player = ", serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2-1].iPlayer)
					PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA iCount2-1 highestpoints = ", serverBD.fHighestScore[(iCount2-1)], ", iCount2-1 highest points participant = ", serverBD.iHighestParticipant[(iCount2-1)])
				ENDIF
				#ENDIF
				
				IF ACCURATLEY_ROUND_SCORE(serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2].fTotalKingPoints) > ACCURATLEY_ROUND_SCORE(serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2-1].fTotalKingPoints)
					
					#IF IS_DEBUG_BUILD
					IF bPrintLeaderboardSortingDebug
					OR bTimerSaysDebugDump
						PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA")
						PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA iCount2 time > than iCount2-1 points.")
					ENDIF
					#ENDIF
					
					SWAP_LEADERBOARD_PARTICIPANTS(iCastleArea, iCount2, iCount2-1, iCount2-1)
					
					#IF IS_DEBUG_BUILD
					IF bPrintLeaderboardSortingDebug
					OR bTimerSaysDebugDump
						PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA")
						PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA ---------- swapped slot ", iCount2, ", with slot ", iCount3, " ----------")
						PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA slot ", iCount2, " points = ", serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2].fTotalKingPoints, ", slot ", iCount2-1, " points = ", serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2-1].fTotalKingPoints)
						PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA slot ", iCount2, " part = ", serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2].iParticipant, ", slot ", iCount2-1, " part = ", serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2-1].iParticipant)
						PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA slot ", iCount2, " player = ", serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2].iPlayer, ", slot ", iCount2-1, " player = ", serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2-1].iPlayer)
						PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA iCount2-1 highestpoints = ", serverBD.fHighestScore[(iCount2-1)], ", iCount2-1 highest points participant = ", serverBD.iHighestParticipant[(iCount2-1)])
					ENDIF
					#ENDIF
						
				ELIF ACCURATLEY_ROUND_SCORE(serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2].fTotalKingPoints) = ACCURATLEY_ROUND_SCORE(serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2-1].fTotalKingPoints)
					
					#IF IS_DEBUG_BUILD
					IF bPrintLeaderboardSortingDebug
					OR bTimerSaysDebugDump
						PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA")
						PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA iCount2 time = than iCount2-1 points.")
					ENDIF
					#ENDIF
					
					IF ACCURATLEY_ROUND_SCORE(serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2].fTotalKingPoints) > ACCURATLEY_ROUND_SCORE(serverBD.fHighestScore[(iCount2-1)])
					OR serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2].iParticipant = serverBD.iHighestParticipant[(iCount2-1)]
						
						#IF IS_DEBUG_BUILD
						IF bPrintLeaderboardSortingDebug
						OR bTimerSaysDebugDump
							IF ACCURATLEY_ROUND_SCORE(serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2].fTotalKingPoints) > ACCURATLEY_ROUND_SCORE(serverBD.fHighestScore[(iCount2-1)])
								PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA")
								PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA icount2 score > serverBD.fHighestScore[", iCount3, "]")
							ENDIF
							IF serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2].iParticipant = serverBD.iHighestParticipant[(iCount2-1)]
								PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA")
								PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA icount2 score = serverBD.iHighestParticipant[", iCount3, "]")
							ENDIF
						ENDIF
						#ENDIF
						
						SWAP_LEADERBOARD_PARTICIPANTS(iCastleArea, iCount2, iCount2-1, iCount2-1)
						
						#IF IS_DEBUG_BUILD
						IF bPrintLeaderboardSortingDebug
						OR bTimerSaysDebugDump
							PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA")
							PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA swapped slot ", iCount2, ", with slot ", iCount3, "")
							PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA slot ", iCount2, " points = ", serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2].fTotalKingPoints, ", slot ", iCount2-1, " points = ", serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2-1].fTotalKingPoints)
							PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA slot ", iCount2, " part = ", serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2].iParticipant, ", slot ", iCount2-1, " part = ", serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2-1].iParticipant)
							PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA slot ", iCount2, " player = ", serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2].iPlayer, ", slot ", iCount2-1, " player = ", serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2-1].iPlayer)
							PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA iCount2-1 highestpoints = ", serverBD.fHighestScore[(iCount2-1)], ", iCount2-1 highest points participant = ", serverBD.iHighestParticipant[(iCount2-1)])
						ENDIF
						#ENDIF
				
					ENDIF
					
				ENDIF
			
			ELIF  ( (iCount2 - 1) = (-1) )
				
				IF ACCURATLEY_ROUND_SCORE(serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2].fTotalKingPoints) > ACCURATLEY_ROUND_SCORE(serverBD.fHighestScore[0])
					serverBD.fHighestScore[0] = serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2].fTotalKingPoints
					serverBD.iHighestParticipant[0] = serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2].iParticipant
					#IF IS_DEBUG_BUILD
					IF bPrintLeaderboardSortingDebug
					OR bTimerSaysDebugDump
						PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA")
						PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA - slot 0 - serverBD.fHighestScore[0] = ", serverBD.fHighestScore[0])
						PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA - slot 0 - serverBD.iHighestParticipant[0] = ", serverBD.iHighestParticipant[0])
					ENDIF
					#ENDIF
				ENDIF
				
			ENDIF
			
		ENDFOR
		
	ENDFOR
	
//	iSortingOuterCount--
//	
//	IF iSortingOuterCount = (NUM_NETWORK_PLAYERS - 1)
//		iSortingOuterCount = 0
//	ENDIF
	
ENDPROC
*/


PROC SORT_LEADERBOARD_DATA(INT iCastleArea)
	
	INT iCount, iCount2
	SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
	
	FOR iCount = (NUM_NETWORK_PLAYERS - 1) TO 0 STEP -1
		
		#IF IS_DEBUG_BUILD
		IF bPrintLeaderboardSortingDebug
			PRINTLN("")
			PRINTLN("")
			PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA *********** PARTICIPANT ", iCount, " ***********")
		ENDIF
		#ENDIF
		
		FOR iCount2 = (NUM_NETWORK_PLAYERS - 1) TO 0 STEP -1
			
			IF ( (iCount2 - 1) >= 0 )
			
				#IF IS_DEBUG_BUILD
				
				INT iCount3 = iCount2 - 1
				
				IF bPrintLeaderboardSortingDebug
					PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA iCount2 = ", iCount2, ", iCount2-1 = ", iCount3)
					PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA iCount2 points = ", serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2].fTotalKingPoints, ", iCount2-1 points = ", serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2-1].fTotalKingPoints)
				ENDIF
				
				#ENDIF
				
				IF serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2].fTotalKingPoints > serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2-1].fTotalKingPoints
					
					#IF IS_DEBUG_BUILD
					IF bPrintLeaderboardSortingDebug
						PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA iCount2 time > than iCount2-1 points - doing swap!")
					ENDIF
					#ENDIF
					
					FLOAT fTempPoints = serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2].fTotalKingPoints
					INT iTempPartId = serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2].iParticipant
					INT iTempPlayerId = serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2].iPlayer
					
					serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2].fTotalKingPoints = serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2-1].fTotalKingPoints
					serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2].iParticipant = serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2-1].iParticipant
					serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2].iPlayer = serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2-1].iPlayer
					
					serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2-1].fTotalKingPoints = fTempPoints
					serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2-1].iParticipant = iTempPartId
					serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2-1].iPlayer = iTempPlayerId
					
					#IF IS_DEBUG_BUILD
					IF bPrintLeaderboardSortingDebug
					PRINTLN("[KINGCASTLE] - SORT_LEADERBOARD_DATA swapped. iCount2 points = ", serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2].fTotalKingPoints, ", iCount2-1 points = ", serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2-1].fTotalKingPoints)
					ENDIF
					#ENDIF
					
				ENDIF
				
				IF (iCount2 - 1 = 0) AND serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2-1].fTotalKingPoints > 0
					IF NOT (serverBD.iPrevLeader = serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2-1].iParticipant)
						serverBD.iPrevLeader = serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2-1].iParticipant
						TickerEventData.TickerEvent = TICKER_EVENT_SCORE_IN_BLAST
						TickerEventData.playerID = INT_TO_PLAYERINDEX(serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount2-1].iPlayer)
						BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT())
					ENDIF
				ENDIF
				
			ENDIF
			
		ENDFOR
		
	ENDFOR
	
//	iSortingOuterCount--
//	
//	IF iSortingOuterCount = (NUM_NETWORK_PLAYERS - 1)
//		iSortingOuterCount = 0
//	ENDIF
	
ENDPROC

PROC MAINTAIN_DPAD_DOWN_LEADERBOARD(INT iCastleArea)
	
	IF SHOULD_HIDE_THIS_AMBIENT_EVENT()
		EXIT
	ENDIF
	
	INT iCount
	
	REPEAT NUM_NETWORK_PLAYERS iCount
		
		sDpadDownLeaderBoardData[iCount].iParticipant = -1
		sDpadDownLeaderBoardData[iCount].playerID = INVALID_PLAYER_INDEX()
		sDpadDownLeaderBoardData[iCount].iScore = 0
		sDpadDownLeaderBoardData[iCount].iFinishTime = (-1)
		
		IF serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount].iParticipant > (-1)
			sDpadDownLeaderBoardData[iCount].iParticipant = serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount].iParticipant
			sDpadDownLeaderBoardData[iCount].playerID = INT_TO_NATIVE(PLAYER_INDEX, serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount].iPlayer)
			sDpadDownLeaderBoardData[iCount].iScore = ACCURATLEY_ROUND_SCORE(serverBD.sCastleArea[iCastleArea].sLeaderboardData[iCount].fTotalKingPoints)
		ENDIF
		
	ENDREPEAT
	
	DRAW_CHALLENGE_DPAD_LBD(sDpadDownLeaderBoardData, sCastleAreaLocal[iCastleArea].scDpadDownLeaderBoardMovie, SUB_CHALLENGE, sDPadVars, sFmDPadLbData)
	
ENDPROC

PROC DRAW_BOTTOM_RIGHT_HUD(INT iCastleArea)
	
	INT i
//	INT iPart
	INT iScore[NUM_LEADERBOARD_PLAYERS]
	INT iMyScore
	INT iEventTime
//	TEXT_LABEL_63 tl63Name
	HUD_COLOURS eColour
	HUD_COLOURS eFleckColour[NUM_LEADERBOARD_PLAYERS]
//	HUDORDER ePosition
//	STRING strEmpty
//	BOOL bDrawLbScore
//	BOOL bIAmOnLeaderboard
	
	IF iFocusParticipant = (-1)
		EXIT
	ENDIF
	
	IF SHOULD_HIDE_THIS_AMBIENT_EVENT()
		EXIT
	ENDIF
	
	iEventTime = ((g_sMPTunables.iKing_of_the_Castle_Event_Time_Limit+1000) - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.sKingOfTheCastle.ModeTimer))
	
	IF iEventTime < 0
		iEventTime = 0
	ENDIF
	
	// Don't do this if we haven't collected the package yet.
	IF GET_CASTLE_AREA_STATE(iCastleArea) = eCASTLEAREASTATE_UNENTERED
		EXIT
	ENDIF
	
	IF iEventTime > KING_OF_THE_CASTLE_NEARLY_FINISHED_TIME
		eColour = HUD_COLOUR_WHITE
	ELSE
		eColour = HUD_COLOUR_RED
	ENDIF
	
	IF NOT IS_BIT_SET(playerBD[iFocusParticipant].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_WITHIN_RANGE_FOR_UI))
		IF NOT GET_IS_PARTICIPANT_FULLY_TAKING_PART(iFocusParticipant)
			BOTTOM_RIGHT_UI_EVENT_TIMER(iEventTime, eColour, GET_COUNTDOWN_TIMER_NAME_FOR_EVENT()) 
			EXIT
		ENDIF
	ENDIF
	
	HANDLE_AMBIENT_EVENT_COUNTDOWN_AUDIO(iEventTime)
	
	#IF IS_DEBUG_BUILD
	IF bDoBottomRightPrints
		PRINTLN(" ")
		PRINTLN("[KINGCASTLE] - [***SORTING***] - START *******************")
		PRINTLN(" ")
	ENDIF
	#ENDIF
	
	REPEAT NUM_LEADERBOARD_PLAYERS i
		sCastleAreaLocal[iCastleArea].sBottomRightData[i].iParticipant = -1
		sCastleAreaLocal[iCastleArea].sBottomRightData[i].iPlayer = -1
		sCastleAreaLocal[iCastleArea].sBottomRightData[i].playerId = INVALID_PLAYER_INDEX()
		sCastleAreaLocal[iCastleArea].sBottomRightData[i].fTotalKingPoints = 0
		#IF IS_DEBUG_BUILD
		IF bDoBottomRightPrints
			PRINTLN("[KINGCASTLE] - [***SORTING***] - clear - set sBottomRightData[", i, "].iParticipant = ", sCastleAreaLocal[iCastleArea].sBottomRightData[i].iParticipant)
			PRINTLN("[KINGCASTLE] - [***SORTING***] - clear - set sBottomRightData[", i, "].iPlayer = ", sCastleAreaLocal[iCastleArea].sBottomRightData[i].iPlayer)
			PRINTLN("[KINGCASTLE] - [***SORTING***] - clear - set sBottomRightData[", i, "].fTotalKingPoints = ", sCastleAreaLocal[iCastleArea].sBottomRightData[i].fTotalKingPoints)
		ENDIF
		#ENDIF
	ENDREPEAT
	
	REPEAT NUM_LEADERBOARD_PLAYERS i
		
		sCastleAreaLocal[iCastleArea].sBottomRightData[i].iParticipant = serverBD.sCastleArea[iCastleArea].sLeaderboardData[i].iParticipant
		sCastleAreaLocal[iCastleArea].sBottomRightData[i].iPlayer = serverBD.sCastleArea[iCastleArea].sLeaderboardData[i].iPlayer
		sCastleAreaLocal[iCastleArea].sBottomRightData[i].playerId = serverBD.sCastleArea[iCastleArea].sLeaderboardData[i].playerId
		sCastleAreaLocal[iCastleArea].sBottomRightData[i].fTotalKingPoints = TO_FLOAT(ACCURATLEY_ROUND_SCORE(serverBD.sCastleArea[iCastleArea].sLeaderboardData[i].fTotalKingPoints))
		
		#IF IS_DEBUG_BUILD
		IF bDoBottomRightPrints
			PRINTLN("[KINGCASTLE] - [***SORTING***] - set - set sBottomRightData[", i, "].iParticipant = ", sCastleAreaLocal[iCastleArea].sBottomRightData[i].iParticipant)
			PRINTLN("[KINGCASTLE] - [***SORTING***] - set - set sBottomRightData[", i, "].iPlayer = ", sCastleAreaLocal[iCastleArea].sBottomRightData[i].iPlayer)
			PRINTLN("[KINGCASTLE] - [***SORTING***] - set - set sBottomRightData[", i, "].fTotalKingPoints = ", sCastleAreaLocal[iCastleArea].sBottomRightData[i].fTotalKingPoints)
		ENDIF
		#ENDIF
		
//		IF sCastleAreaLocal[iCastleArea].sBottomRightData[i].iParticipant = PARTICIPANT_ID_TO_INT()
//			bIAmOnLeaderboard = TRUE
//			#IF IS_DEBUG_BUILD
//			IF bDoBottomRightPrints
//				INT iPartTemp = PARTICIPANT_ID_TO_INT()
//				PRINTLN("[KINGCASTLE] - [***SORTING***] - set - set sBottomRightData[", i, "].iParticipant = ", sCastleAreaLocal[iCastleArea].sBottomRightData[i].iParticipant, ". My part ID = ", iPartTemp, " setting bIAmOnLeaderboard = TRUE.")
//			ENDIF
//			#ENDIF
//		ENDIF
		
	ENDREPEAT
	
//	IF NOT bIAmOnLeaderboard
//		sCastleAreaLocal[iCastleArea].sBottomRightData[(NUM_LEADERBOARD_PLAYERS-1)].iParticipant = PARTICIPANT_ID_TO_INT()
//		sCastleAreaLocal[iCastleArea].sBottomRightData[(NUM_LEADERBOARD_PLAYERS-1)].iPlayer = sParticipant[PARTICIPANT_ID_TO_INT()].iPlayerId
//		sCastleAreaLocal[iCastleArea].sBottomRightData[(NUM_LEADERBOARD_PLAYERS-1)].fTotalKingPoints = GET_CASTLE_AREA_KING_TOTAL_POINTS(iCastleArea, PARTICIPANT_ID_TO_INT())
//		#IF IS_DEBUG_BUILD
//		IF bDoBottomRightPrints
//			PRINTLN("[KINGCASTLE] - [***SORTING***] - set - bIAmOnLeaderboard = TRUE.")
//			PRINTLN("[KINGCASTLE] - [***SORTING***] - set - set sBottomRightData[", i, "].iParticipant = ", sCastleAreaLocal[iCastleArea].sBottomRightData[(NUM_LEADERBOARD_PLAYERS-1)].iParticipant)
//			PRINTLN("[KINGCASTLE] - [***SORTING***] - set - set sBottomRightData[", i, "].iPlayer = ", sCastleAreaLocal[iCastleArea].sBottomRightData[(NUM_LEADERBOARD_PLAYERS-1)].iPlayer)
//			PRINTLN("[KINGCASTLE] - [***SORTING***] - set - set sBottomRightData[", i, "].fTotalKingPoints = ", sCastleAreaLocal[iCastleArea].sBottomRightData[(NUM_LEADERBOARD_PLAYERS-1)].fTotalKingPoints)
//		ENDIF
//		#ENDIF
//	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bDoBottomRightPrints
		PRINTLN(" ")
		PRINTLN("[KINGCASTLE] - [***SORTING***] - END *******************")
		PRINTLN(" ")
	ENDIF
	#ENDIF
	
	// Draw leaderboard.
//	FOR i = (NUM_LEADERBOARD_PLAYERS - 1) TO 0 STEP -1
	REPEAT NUM_LEADERBOARD_PLAYERS i
		
////		iPart = sCastleAreaLocal[iCastleArea].sBottomRightData[i].iParticipant
//		IF GET_KING_OF_THE_CASTLE_STATE() = eKINGOFTHECASTLESTATE_TIME_UP
//			iScore[i] = iFinalScores[i]
//		ELSE
			iScore[i] = ACCURATLEY_ROUND_SCORE(sCastleAreaLocal[iCastleArea].sBottomRightData[i].fTotalKingPoints)
//		ENDIF

			eFleckColour[i] = HUD_COLOUR_PURE_WHITE
			
			IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
				IF sCastleAreaLocal[iCastleArea].sBottomRightData[i].iPlayer != (-1)
					IF NETWORK_IS_PLAYER_ACTIVE(INT_TO_NATIVE(PLAYER_INDEX, sCastleAreaLocal[iCastleArea].sBottomRightData[i].iPlayer))
	//					IF GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID()) = GB_GET_THIS_PLAYER_GANG_BOSS(INT_TO_NATIVE(PLAYER_INDEX, sCastleAreaLocal[iCastleArea].sBottomRightData[i].iPlayer))
						IF GB_IS_PLAYER_MEMBER_OF_A_GANG(INT_TO_NATIVE(PLAYER_INDEX, sCastleAreaLocal[iCastleArea].sBottomRightData[i].iPlayer))
							eFleckColour[i] = GET_PLAYER_HUD_COLOUR(INT_TO_NATIVE(PLAYER_INDEX, sCastleAreaLocal[iCastleArea].sBottomRightData[i].iPlayer))
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
	ENDREPEAT
	
//	IF GET_KING_OF_THE_CASTLE_STATE() = eKINGOFTHECASTLESTATE_TIME_UP
//		iMyScore = iFinalScores[3]
//	ELSE
		iMyScore = ACCURATLEY_ROUND_SCORE(GET_CASTLE_AREA_KING_TOTAL_POINTS(iCastleArea, iFocusParticipant))
//	ENDIF
	
	BOTTOM_RIGHT_UI_1STINT_2NDINT_3RDINT_LOCALINT_TIMER(	INT_TO_NATIVE(PLAYER_INDEX, sCastleAreaLocal[iCastleArea].sBottomRightData[0].iPlayer),
															INT_TO_NATIVE(PLAYER_INDEX, sCastleAreaLocal[iCastleArea].sBottomRightData[1].iPlayer),
															INT_TO_NATIVE(PLAYER_INDEX, sCastleAreaLocal[iCastleArea].sBottomRightData[2].iPlayer),
															iScore[0],
															iScore[1],
															iScore[2],
															iMyScore,
															iEventTime,
															bForceRebuildNames, eColour, GET_COUNTDOWN_TIMER_NAME_FOR_EVENT(), 
															FMEVENT_SCORETITLE_YOUR_SCORE, 
															eFleckColour[0], eFleckColour[1], eFleckColour[2])
	
	IF sCastleAreaLocal[iCastleArea].sBottomRightData[0].iParticipant = PARTICIPANT_ID_TO_INT()
		IF NOT IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_PLAYED_TOP_LEADERBOARD_AUDIO))
			IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
				PLAY_SOUND_FRONTEND(-1,"Enter_1st","GTAO_Biker_Modes_Soundset", FALSE)
			ELSE
				PLAY_SOUND_FRONTEND(-1,"Enter_1st","GTAO_FM_Events_Soundset", FALSE)
			ENDIF
			SET_BIT(iLocalBitset0,ENUM_TO_INT(eLOCALBITSET0_PLAYED_TOP_LEADERBOARD_AUDIO))
			CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_KING_OF_THE_CASTLE === eLOCALBITSET0_PLAYED_TOP_LEADERBOARD_AUDIO - Enter_1st")
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBitset0,ENUM_TO_INT(eLOCALBITSET0_PLAYED_TOP_LEADERBOARD_AUDIO))
			IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
				PLAY_SOUND_FRONTEND(-1,"Lose_1st","GTAO_Biker_Modes_Soundset", FALSE)
			ELSE
				PLAY_SOUND_FRONTEND(-1,"Lose_1st","GTAO_FM_Events_Soundset", FALSE)
			ENDIF
			CLEAR_BIT(iLocalBitset0,ENUM_TO_INT(eLOCALBITSET0_PLAYED_TOP_LEADERBOARD_AUDIO))
			CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_KING_OF_THE_CASTLE === eLOCALBITSET0_PLAYED_TOP_LEADERBOARD_AUDIO - Lose_1st")
		ENDIF
	ENDIF
ENDPROC

PROC ADD_CASTLE_AREA_RADIUS_BLIP(INT iCastleArea)
	
	FLOAT fTriggerRadius = KING_OF_THE_CASTLE_TRIGGER_RADIUS
	
	SWITCH GET_CASTLE_AREA_SEED(iCastleArea)
		CASE CASTLE_AREA_BEACH_TOWER					
		CASE CASTLE_AREA_LAND_ACT_DAM					
		CASE CASTLE_AREA_FIRE_STATION					
		CASE CASTLE_AREA_CABLE_CAR_STATION				
		CASE CASTLE_AREA_ALIEN_HIPPY_HILL				
		CASE CASTLE_AREA_OBSERVATORY					
		CASE CASTLE_AREA_SCRAP_YARD	
			fTriggerRadius = GET_CASTLE_AREA_WIDTH_FROM_SEED(iCastleArea, 0)
		BREAK
		CASE CASTLE_AREA_PLAYBOY_GROTTO	
			fTriggerRadius = 9.000 
		BREAK
	ENDSWITCH
	
	IF GET_CASTLE_AREA_SEED(iCastleArea) = CASTLE_AREA_PLAYBOY_GROTTO
		fTriggerRadius = 9.000
	ENDIF
	
	IF NOT DOES_BLIP_EXIST(GET_CASTLE_AREA_RADIUS_BLIP_INDEX(iCastleArea))
		SET_CASTLE_AREA_RADIUS_BLIP_INDEX(iCastleArea, ADD_BLIP_FOR_RADIUS(GET_CASTLE_AREA_BLIP_COORDS(iCastleArea), fTriggerRadius))
		SET_BLIP_COLOUR_FROM_HUD_COLOUR(sCastleAreaLocal[iCastleArea].blipRadiusBlip, GET_CASTLE_AREA_BLIP_COLOUR(iCastleArea))
		PRINTLN("[KINGCASTLE] - [CASTLE AREA BLIP] - Added radius blip for castle area ", iCastleArea)
	ENDIF
	IF DOES_BLIP_EXIST(GET_CASTLE_AREA_RADIUS_BLIP_INDEX(iCastleArea))
		SET_BLIP_ALPHA(GET_CASTLE_AREA_RADIUS_BLIP_INDEX(iCastleArea), 100)
		SET_BLIP_COLOUR_FROM_HUD_COLOUR(sCastleAreaLocal[iCastleArea].blipRadiusBlip, GET_CASTLE_AREA_BLIP_COLOUR(iCastleArea))
		SHOW_HEIGHT_ON_BLIP(GET_CASTLE_AREA_RADIUS_BLIP_INDEX(iCastleArea), FALSE)
	ENDIF

ENDPROC

PROC REMOVE_CASTLE_AREA_RADIUS_BLIP(INT iCastleArea)

	IF DOES_BLIP_EXIST(GET_CASTLE_AREA_RADIUS_BLIP_INDEX(iCastleArea))
		REMOVE_BLIP(sCastleAreaLocal[iCastleArea].blipRadiusBlip)
		PRINTLN("[KINGCASTLE] - [CASTLE AREA BLIP] - Removed radius blip for castle area ", iCastleArea)
	ENDIF
	
ENDPROC

//PROC SET_CASTLE_CENTRE_POINT_BLIP_PROPERTIES(INT iCastleArea, BOOL bShowHeight)
//	
//	IF bShowHeight
//		SET_BLIP_SPRITE(GET_CASTLE_AREA_CENTER_POINT_BLIP_INDEX(iCastleArea), RADAR_TRACE_OBJECTIVE)
//		SET_BLIP_COLOUR_FROM_HUD_COLOUR(sCastleAreaLocal[iCastleArea].blipCenterPoint, GET_CASTLE_AREA_BLIP_COLOUR(iCastleArea))
//		SHOW_HEIGHT_ON_BLIP(GET_CASTLE_AREA_CENTER_POINT_BLIP_INDEX(iCastleArea), TRUE)
//	ELSE
//		SET_BLIP_SPRITE(GET_CASTLE_AREA_CENTER_POINT_BLIP_INDEX(iCastleArea), RADAR_TRACE_KING_OF_THE_CASTLE)
//		SET_BLIP_COLOUR_FROM_HUD_COLOUR(sCastleAreaLocal[iCastleArea].blipCenterPoint, GET_CASTLE_AREA_BLIP_COLOUR(iCastleArea))
//		SHOW_HEIGHT_ON_BLIP(GET_CASTLE_AREA_CENTER_POINT_BLIP_INDEX(iCastleArea), FALSE)
//	ENDIF
//	
//ENDPROC
//
//PROC MAINTAIN_SWITCHING_BETWEEN_CENTRE_POINT_BLIP_AND_NORMAL_BLIP(INT iCastleArea)
//	
//	IF IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_FLASHED_KING_OF_THE_CASTLE_BLIP))
//		IF NOT IS_BLIP_FLASHING(GET_CASTLE_AREA_CENTER_POINT_BLIP_INDEX(iCastleArea))
//			IF GET_PARTICIPANT_2D_DISTANCE_FROM_CASTLE_AREA_CENTER_POINT(iCastleArea) <= FLASH_HEIGHT_ON_BLIP_2D_DISTANCE
//				IF GET_PARTICIPANT_Z_DISTANCE_FROM_CASTLE_AREA_CENTER_POINT(iCastleArea) > 0.0
//				OR GET_PARTICIPANT_Z_DISTANCE_FROM_CASTLE_AREA_CENTER_POINT(iCastleArea) < (FLASH_HEIGHT_ON_BLIP_Z_DISTANCE+(-1))
//				
//					SWITCH iSwitchCentrePointBlipStage
//						
//						CASE 0 
//							SET_CASTLE_CENTRE_POINT_BLIP_PROPERTIES(iCastleArea, FALSE)
//							RESET_NET_TIMER(timerSwitchCentrePointBlip)
//							iSwitchCentrePointBlipStage++
//						BREAK
//						
//						CASE 1
//							IF NOT HAS_NET_TIMER_STARTED(timerSwitchCentrePointBlip)
//								START_NET_TIMER(timerSwitchCentrePointBlip)
//							ELSE
//								iSwitchCentrePointBlipStage++
//							ENDIF
//						BREAK
//						
//						CASE 2
//							IF HAS_NET_TIMER_EXPIRED(timerSwitchCentrePointBlip, CENTRE_POINT_BLIP_SWITCH_TIME)
//								SET_CASTLE_CENTRE_POINT_BLIP_PROPERTIES(iCastleArea, TRUE)
//								RESET_NET_TIMER(timerSwitchCentrePointBlip)
//								iSwitchCentrePointBlipStage++
//							ENDIF
//						BREAK
//						
//						CASE 3
//							IF NOT HAS_NET_TIMER_STARTED(timerSwitchCentrePointBlip)
//								START_NET_TIMER(timerSwitchCentrePointBlip)
//							ELSE
//								iSwitchCentrePointBlipStage++
//							ENDIF
//						BREAK
//						
//						CASE 4
//							IF HAS_NET_TIMER_EXPIRED(timerSwitchCentrePointBlip, CENTRE_POINT_BLIP_SWITCH_TIME)
//								iSwitchCentrePointBlipStage = 0
//							ENDIF
//						BREAK
//						
//					ENDSWITCH
//					
//				ELSE
//					
//					SET_CASTLE_CENTRE_POINT_BLIP_PROPERTIES(iCastleArea, FALSE)
//					RESET_NET_TIMER(timerSwitchCentrePointBlip)
//					iSwitchCentrePointBlipStage = 0
//					
//				ENDIF
//				
//			ELSE
//			
//				SET_CASTLE_CENTRE_POINT_BLIP_PROPERTIES(iCastleArea, FALSE)
//				RESET_NET_TIMER(timerSwitchCentrePointBlip)
//				iSwitchCentrePointBlipStage = 0
//						
//			ENDIF
//		ENDIF
//	ENDIF
//	
//ENDPROC

PROC ADD_CASTLE_AREA_CENTER_POINT_BLIP(INT iCastleArea)
	
	IF NOT DOES_BLIP_EXIST(GET_CASTLE_AREA_CENTER_POINT_BLIP_INDEX(iCastleArea))
	
//		iSwitchCentrePointBlipStage = 0
//		RESET_NET_TIMER(timerSwitchCentrePointBlip)
		
		SET_CASTLE_AREA_CENTER_POINT_BLIP_INDEX(iCastleArea, ADD_BLIP_FOR_COORD(GET_CASTLE_AREA_BLIP_COORDS(iCastleArea)))
		PRINTLN("[KINGCASTLE] - [CASTLE AREA BLIP] - Added center point blip for castle area ", iCastleArea)
		
		SET_BLIP_PRIORITY(GET_CASTLE_AREA_CENTER_POINT_BLIP_INDEX(iCastleArea), BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
		IF DOES_TEXT_LABEL_EXIST("KOTC_AREANAME")
			SET_BLIP_NAME_FROM_TEXT_FILE(GET_CASTLE_AREA_CENTER_POINT_BLIP_INDEX(iCastleArea), "KOTC_AREANAME")
		ELSE
			PRINTLN("[KINGCASTLE] - [CASTLE AREA BLIP] - DOES_TEXT_LABEL_EXIST(KOTC_AREANAME) = FALSE ")
		ENDIF		
		
		SET_BLIP_SPRITE(GET_CASTLE_AREA_CENTER_POINT_BLIP_INDEX(iCastleArea), RADAR_TRACE_KING_OF_THE_CASTLE)
		SET_BLIP_COLOUR_FROM_HUD_COLOUR(sCastleAreaLocal[iCastleArea].blipCenterPoint, GET_CASTLE_AREA_BLIP_COLOUR(iCastleArea))
		
		IF NOT IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_FLASHED_KING_OF_THE_CASTLE_BLIP))
			SET_BLIP_FLASHES(sCastleAreaLocal[iCastleArea].blipCenterPoint, TRUE)
			SET_BLIP_FLASH_TIMER(sCastleAreaLocal[iCastleArea].blipCenterPoint, 7000)
			SET_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_FLASHED_KING_OF_THE_CASTLE_BLIP))
			PRINTLN("[KINGCASTLE] - [CASTLE AREA BLIP] - called SET_BLIP_FLASHES() and SET_BLIP_FLASH_TIMER with 7000ms.", iCastleArea)
			PRINTLN("[KINGCASTLE] - [CASTLE AREA BLIP] - set bit: iLocalBitset0, eLOCALBITSET0_FLASHED_KING_OF_THE_CASTLE_BLIP")
		ENDIF
		
		IF NOT IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_FLASHED_MINIMAP_DISPLAY))
			FLASH_MINIMAP_DISPLAY()
			SET_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_FLASHED_MINIMAP_DISPLAY))
			PRINTLN("[KINGCASTLE] - [CASTLE AREA BLIP] - called FLASH_MINIMAP_DISPLAY().", iCastleArea)
			PRINTLN("[KINGCASTLE] - [CASTLE AREA BLIP] - set bit: iLocalBitset0, eLOCALBITSET0_FLASHED_MINIMAP_DISPLAY")
		ENDIF
		
		SHOW_HEIGHT_ON_BLIP(GET_CASTLE_AREA_CENTER_POINT_BLIP_INDEX(iCastleArea), TRUE)
		SET_BLIP_EXTENDED_HEIGHT_THRESHOLD(GET_CASTLE_AREA_CENTER_POINT_BLIP_INDEX(iCastleArea), TRUE)
		
	ELSE
		
		SET_BLIP_COLOUR_FROM_HUD_COLOUR(sCastleAreaLocal[iCastleArea].blipCenterPoint, GET_CASTLE_AREA_BLIP_COLOUR(iCastleArea, FALSE))
		
//		MAINTAIN_SWITCHING_BETWEEN_CENTRE_POINT_BLIP_AND_NORMAL_BLIP(iCastleArea)
		
		IF DOES_BLIP_EXIST(GET_CASTLE_AREA_CENTER_POINT_BLIP_INDEX(iCastleArea))
			IF DOES_TEXT_LABEL_EXIST("KOTC_AREANAME")
				SET_BLIP_NAME_FROM_TEXT_FILE(GET_CASTLE_AREA_CENTER_POINT_BLIP_INDEX(iCastleArea), "KOTC_AREANAME")
			ELSE
				PRINTLN("[KINGCASTLE] - [CASTLE AREA BLIP] - DOES_TEXT_LABEL_EXIST(KOTC_AREANAME) = FALSE 2 ")
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC REMOVE_CASTLE_AREA_CENTER_POINT_BLIP(INT iCastleArea)
	
	IF DOES_BLIP_EXIST(GET_CASTLE_AREA_CENTER_POINT_BLIP_INDEX(iCastleArea))
		REMOVE_BLIP(sCastleAreaLocal[iCastleArea].blipCenterPoint)
		PRINTLN("[KINGCASTLE] - [CASTLE AREA BLIP] - Removed center point blip for castle area ", iCastleArea)
	ENDIF
	
ENDPROC

PROC ADD_CASTLE_AREA_KING_CUSTOM_BLIP(INT iPlayer)
	
	HUD_COLOURS eHudColour = HUD_COLOUR_RED
	
	IF iPlayer > (-1)
		IF IS_BIT_SET(iPlayerOkBitset, iPlayer)
			IF NOT IS_BIT_SET(iPlayerDeadBitset, iPlayer) 
				IF GB_ARE_PLAYERS_IN_SAME_GANG(PLAYER_ID(), sPlayer[iPlayer].playerId)
					eHudColour = GET_PLAYER_HUD_COLOUR(PLAYER_ID())
				ENDIF
				SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(sPlayer[iPlayer].playerId, RADAR_TRACE_PLAYER_KING, TRUE)
				IF DOES_THIS_THREAD_HAVE_CONTROL_OF_CUSTOM_BLIP_COLOUR_FOR_PLAYER(sPlayer[iPlayer].playerId)
					SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(sPlayer[iPlayer].playerId, GET_BLIP_COLOUR_FROM_HUD_COLOUR(eHudColour), TRUE)
				ENDIF
				FORCE_BLIP_PLAYER(sPlayer[iPlayer].playerId, TRUE, TRUE)
				SET_BIT(iAddedCustomBlipForPlayerBitset, iPlayer)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC REMOVE_CASTLE_AREA_KING_CUSTOM_BLIP(INT iPlayer)
	
	HUD_COLOURS eHudColour = HUD_COLOUR_RED
	
	IF IS_BIT_SET(iAddedCustomBlipForPlayerBitset, iPlayer)
		IF iPlayer > (-1)
			IF IS_BIT_SET(iPlayerOkBitset, iPlayer)
				IF NOT IS_BIT_SET(iPlayerDeadBitset, iPlayer)
					IF (PLAYER_ID() != sPlayer[iPlayer].playerId)
						IF GB_ARE_PLAYERS_IN_SAME_GANG(PLAYER_ID(), sPlayer[iPlayer].playerId)
							eHudColour = GET_PLAYER_HUD_COLOUR(PLAYER_ID())
						ENDIF
					ENDIF
					SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(sPlayer[iPlayer].playerId, RADAR_TRACE_PLAYER_KING, FALSE)
					IF DOES_THIS_THREAD_HAVE_CONTROL_OF_CUSTOM_BLIP_COLOUR_FOR_PLAYER(sPlayer[iPlayer].playerId)
						SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(sPlayer[iPlayer].playerId, GET_BLIP_COLOUR_FROM_HUD_COLOUR(eHudColour), FALSE)
					ENDIF
					FORCE_BLIP_PLAYER(sPlayer[iPlayer].playerId, FALSE, FALSE)
					CLEAR_BIT(iAddedCustomBlipForPlayerBitset, iPlayer)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_KING_OF_THE_CASTLE_SPAWNING(INT iCastleArea)
	
//	VECTOR vMin, vMax
//	FLOAT fWidth
//	INT iLocateCount
	
	IF iCastleArea = iCastleArea
		iCastleArea = iCastleArea
	ENDIF
	
	IF GET_IS_PARTICIPANT_FULLY_TAKING_PART(PARTICIPANT_ID_TO_INT())
	AND NOT SHOULD_HIDE_THIS_AMBIENT_EVENT(TRUE, TRUE)
	AND iFocusParticipant = PARTICIPANT_ID_TO_INT()
	AND NOT IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), FALSE)
	AND NOT IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
	AND NOT IS_ON_IMPROMPTU_DEATHMATCH_GLOBAL_SET()
		IF NOT IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_SETUP_SPAWN_AREAS))
			
//			// Direction to face.
//			SET_PLAYER_WILL_SPAWN_FACING_COORDS(GET_CASTLE_AREA_BLIP_COORDS(iCastleArea), TRUE, FALSE)
//			
//			// Spawn area.
//			REPEAT 2 iLocateCount
//				vMin = << 0.0, 0.0, 0.0 >>
//				vMax = << 0.0, 0.0, 0.0 >>
//				fWidth = 0.0
//				GET_CASTLE_AREA_SPAWN_ANGLED_AREA_DATA(GET_CASTLE_AREA_SEED(iCastleArea), iLocateCount, vMin, vMax, fWidth)
//				IF NOT IS_VECTOR_ZERO(vMin)
//					SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_MISSION_AREA)
//	//				SETUP_SPECIFIC_SPAWN_LOCATION_ANGLED_AREA(vMin, vMax, fWidth, GET_CASTLE_AREA_BLIP_COORDS(iCastleArea), TRUE, TRUE, FALSE, 65.0, FALSE)
//					SET_MISSION_SPAWN_ANGLED_AREA(vMin, vMax, fWidth, iLocateCount, TRUE)
//				ENDIF
//			ENDREPEAT
//			
//			// Occlusion areas.
//			REPEAT GET_CASTLE_AREA_NUM_SPAWN_OCCLUSION_AREAS(GET_CASTLE_AREA_SEED(iCastleArea)) iLocateCount
//				vMin = << 0.0, 0.0, 0.0 >>
//				vMax = << 0.0, 0.0, 0.0 >>
//				fWidth = 0.0
//				GET_CASTLE_AREA_SPAWN_OCCLUSION_ANGLED_AREA_DATA(GET_CASTLE_AREA_SEED(iCastleArea), iLocateCount, vMin, vMax, fWidth)
//				IF NOT IS_VECTOR_ZERO(vMin)
//					SET_MISSION_SPAWN_OCCLUSION_ANGLED_AREA(vMin, vMax, fWidth, iLocateCount, TRUE)
//				ENDIF
//			ENDREPEAT
			
			IF bCustomSpawn = FALSE
				bCustomSpawn = TRUE
			ENDIF
			
			USE_CUSTOM_SPAWN_POINTS(TRUE)
			PRINTLN("[KINGCASTLE] - [SPAWNING] - USE_CUSTOM_SPAWN_POINTS(TRUE)")
			SETUP_CUSTOM_SPAWN_POINTS()
			
	//		SETUP_SPECIFIC_SPAWN_LOCATION(GET_CASTLE_AREA_BLIP_COORDS(iCastleArea), 0.0, 250.0, TRUE, 15.0)
			
			SET_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_SETUP_SPAWN_AREAS))
			PRINTLN("[KINGCASTLE] - Set bit: iLocalBitset0, eLOCALBITSET0_SETUP_SPAWN_AREAS")
			
		ENDIF
		
	ELSE
		
		IF IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_SETUP_SPAWN_AREAS))
//			CLEANUP_SPAWN_AREAS(iCastleArea)
			IF bCustomSpawn
				bCustomSpawn = FALSE
			ENDIF
			CLEAR_CUSTOM_SPAWN_POINTS()
			USE_CUSTOM_SPAWN_POINTS(FALSE)
			CLEAR_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_SETUP_SPAWN_AREAS))
			PRINTLN("[KINGCASTLE] - Cleared bit: iLocalBitset0, eLOCALBITSET0_SETUP_SPAWN_AREAS")
		ENDIF
		
	ENDIF
	
ENDPROC

PROC PROCESS_DRAW_CASTLE_AREA_BLIPS(INT iCastleArea)
	
	IF SHOULD_HIDE_THIS_AMBIENT_EVENT()
	OR iFocusParticipant = (-1)
		REMOVE_CASTLE_AREA_RADIUS_BLIP(iCastleArea)
		REMOVE_CASTLE_AREA_CENTER_POINT_BLIP(iCastleArea)
		DEACTIVATE_CASTLE_AREA_MINIMAP_COMPONENT(iCastleArea)
		EXIT
	ENDIF
		
	IF GET_CASTLE_AREA_KING_PLAYER_ID(iCastleArea) = (-1)
		
		IF GET_DOES_PARTICIPANT_SAY_INSIDE_CASTLE_AREA(PARTICIPANT_ID_TO_INT(), iCastleArea)
			REMOVE_CASTLE_AREA_CENTER_POINT_BLIP(iCastleArea)
		ELSE
			ADD_CASTLE_AREA_CENTER_POINT_BLIP(iCastleArea)
		ENDIF
		
	ELSE
		
		REMOVE_CASTLE_AREA_CENTER_POINT_BLIP(iCastleArea)
		
		// Note adding and removing of the king blip is done in the participant loop. 
		
	ENDIF
	
	IF eCastleAreaColourCopy != GET_CASTLE_AREA_BLIP_COLOUR(iCastleArea)
		SWITCH iUpdateMinimapComponentColourStage
			CASE 0
				DEACTIVATE_CASTLE_AREA_MINIMAP_COMPONENT(iCastleArea)
				iUpdateMinimapComponentColourStage++
				PRINTLN("[HUNTBEAST] - removed minimap component.")
			BREAK
			CASE 1
				ACTIVATE_CASTLE_AREA_MINIMAP_COMPONENT(iCastleArea)
				eCastleAreaColourCopy = GET_CASTLE_AREA_BLIP_COLOUR(iCastleArea)
				#IF IS_DEBUG_BUILD
				STRING strColour
				SWITCH GET_CASTLE_AREA_BLIP_COLOUR(iCastleArea)
					CASE HUD_COLOUR_BLUE 	strColour = "HUD_COLOUR_BLUE" 		BREAK
					CASE HUD_COLOUR_PURPLE 	strColour = "HUD_COLOUR_PURPLE" 	BREAK
					CASE HUD_COLOUR_RED		strColour = "HUD_COLOUR_RED" 		BREAK
					DEFAULT 				strColour = "OTHER"					BREAK
				ENDSWITCH	
				PRINTLN("[HUNTBEAST] - added minimap component. Colour = ", strColour)
				#ENDIF
				iUpdateMinimapComponentColourStage = 0
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

PROC PROCESS_PLAYER_LEFT_SERVER(INT iParticipant)

	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
//		INT iParticipant
//		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
			PRINTLN("[KINGCASTLE] - [PLAYER LEFT] - PROCESS_PLAYER_LEFT_SERVER - iParticipant = ",iParticipant, " ")

//			IF GET_IS_PARTICIPANT_FULLY_TAKING_PART(iParticipant) = FALSE
				INT iCastleArea
				REPEAT NUM_CASTLE_AREAS iCastleArea
					IF GET_CASTLE_AREA_KING_AREA_POINTS(iCastleArea, iParticipant) > 0
					OR GET_CASTLE_AREA_KING_KILL_POINTS(iCastleArea, iParticipant) > 0
						SET_CASTLE_AREA_KING_AREA_POINTS(iCastleArea, iParticipant, 0)
						SET_CASTLE_AREA_KING_KILL_POINTS(iCastleArea, iParticipant, 0)
						PRINTLN("[KINGCASTLE] - [PLAYER LEFT] - PROCESS_PLAYER_LEFT_SERVER - resetting players scores. ")

					ELSE
						PRINTLN("[KINGCASTLE] - [PLAYER LEFT] - PROCESS_PLAYER_LEFT_SERVER - Scores already at 0 ")

					ENDIF
				ENDREPEAT
//			ELSE
//				PRINTLN("[KINGCASTLE] - [PLAYER LEFT] - PROCESS_PLAYER_LEFT_SERVER - GET_IS_PARTICIPANT_FULLY_TAKING_PART(iParticipant) = TRUE ")
//
//			ENDIF
//		ENDREPEAT
	ELSE
		PRINTLN("[KINGCASTLE] - [PLAYER LEFT] - PROCESS_PLAYER_LEFT_SERVER - NETWORK_IS_HOST_OF_THIS_SCRIPT = FALSE ")

	ENDIF
	
ENDPROC

PROC PROCESS_CASTLE_AREA_SERVER()
	
	INT iCastleArea
	SCRIPT_TIMER timerTemp
	
	REPEAT NUM_CASTLE_AREAS iCastleArea
		
		SWITCH GET_CASTLE_AREA_STATE(iCastleArea)
			
			CASE eCASTLEAREASTATE_UNENTERED
				
				IF GET_IS_CASTLE_AREA_POPULATED(iCastleArea)
					IF NOT HAS_NET_TIMER_STARTED(serverBd.sKingOfTheCastle.ModeTimer)
						serverBd.sKingOfTheCastle.AvailableTimer = timerTemp
						START_NET_TIMER(serverBd.sKingOfTheCastle.ModeTimer, FALSE, TRUE)
						PRINTLN("[KINGCASTLE] - set serverBd.sKingOfTheCastle.AvailableTimer = timerTemp (cleared the timer).")
						PRINTLN("[KINGCASTLE] - started ModeTimer.")
					ENDIF
					IF NOT HAS_NET_TIMER_STARTED(serverBD.sCastleArea[iCastleArea].KingInUncontestedAreaTimer)
						START_NET_TIMER(serverBD.sCastleArea[iCastleArea].KingInUncontestedAreaTimer, FALSE, TRUE)
//						PRINTLN("[KINGCASTLE] - [MODE TIMER] - resetting mode timer, someone has entered the castle.")
////						IF GET_CASTLE_AREA_KING_PARTICIPANT_ID(iCastleArea) > (-1)
////							FLOAT fKingAreaPoints
////							fKingAreaPoints = GET_CASTLE_AREA_KING_AREA_POINTS(iCastleArea, GET_CASTLE_AREA_KING_PARTICIPANT_ID(iCastleArea))
////							fKingAreaPoints += g_sMPTunables.iKing_of_the_Castle_Points_per_X_seconds_as_King
////							SET_CASTLE_AREA_KING_AREA_POINTS(iCastleArea, GET_CASTLE_AREA_KING_PARTICIPANT_ID(iCastleArea), fKingAreaPoints)
////						ENDIF
					ENDIF
					SET_CASTLE_AREA_STATE(iCastleArea, eCASTLEAREASTATE_POPULATED)
					
					// IF we have not yet had any castle area populated update the server knowledge (used for expiry time)
					IF NOT IS_BIT_SET(serverBD.iBitset0, ENUM_TO_INT(eSERVERBITSET0_CASTLE_AREA_HAS_BEEN_POPULATED))
						PRINTLN("[KINGCASTLE] - [MODE TIMER] - This is the first castle area to have been populated. Set server bit: eSERVERBITSET0_CASTLE_AREA_HAS_BEEN_POPULATED")
						SET_BIT(serverBD.iBitset0, ENUM_TO_INT(eSERVERBITSET0_CASTLE_AREA_HAS_BEEN_POPULATED))
					ENDIF
				ENDIF
					
			BREAK
			
			CASE eCASTLEAREASTATE_POPULATED
				
				IF NOT GET_IS_CASTLE_AREA_POPULATED(iCastleArea)
					SET_CASTLE_AREA_STATE(iCastleArea, eCASTLEAREASTATE_EMPTY)
				ENDIF
				
			BREAK
			
			CASE eCASTLEAREASTATE_EMPTY
				
				IF GET_IS_CASTLE_AREA_POPULATED(iCastleArea)
					SET_CASTLE_AREA_STATE(iCastleArea, eCASTLEAREASTATE_POPULATED)
				ENDIF
				
			BREAK
			
		ENDSWITCH
		
	ENDREPEAT
	
ENDPROC

PROC PROCESS_CASTLE_AREA_CLIENT()
	
	INT iCastleArea
	
	REPEAT NUM_CASTLE_AREAS iCastleArea	
		
		// Broadcast if the local player is inside the castle area or not.
		PROCESS_PARTICIPANT_INSIDE_CASTLE_AREA(iCastleArea)
		
		// Draw sphere on radar if more than 25m away.
		PROCESS_DRAW_CASTLE_AREA_BLIPS(iCastleArea)
		
		// Main states.
		SWITCH GET_CASTLE_AREA_STATE(iCastleArea)
			
			CASE eCASTLEAREASTATE_UNENTERED
				
				//BC: removed as this is done in PROCESS_DRAW_CASTLE_AREA_BLIPS
//				IF NOT SHOULD_HIDE_THIS_AMBIENT_EVENT()
//					ADD_CASTLE_AREA_CENTER_POINT_BLIP(iCastleArea)
//				ELSE
//					REMOVE_CASTLE_AREA_CENTER_POINT_BLIP(iCastleArea)
//				ENDIF
				
			BREAK
			
			CASE eCASTLEAREASTATE_POPULATED
				
				
			BREAK
			
			CASE eCASTLEAREASTATE_EMPTY
				//BC: removed as this is done in PROCESS_DRAW_CASTLE_AREA_BLIPS
//				ADD_CASTLE_AREA_CENTER_POINT_BLIP(iCastleArea)
			BREAK
			
		ENDSWITCH
		
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Maintains participants on the script if they become the critical player (ie. the one in a castle area)
PROC PROCESS_CRITICAL_PLAYER_CHECKS(INT iCastleArea)
	
	IF GET_KING_OF_THE_CASTLE_STATE() = eKINGOFTHECASTLESTATE_FIGHT_OVER_CASTLE
		
		// If we are holding a castle we are considered critical.
		IF GET_CASTLE_AREA_KING_PARTICIPANT_ID(iCastleArea) = PARTICIPANT_ID_TO_INT()
		AND NOT IS_LOCAL_PARTICIPANT_RESTRICTED(TRUE)
		
			// Mark ourselves as critical if we are not already.
			IF NOT FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT(PLAYER_ID())
				FM_EVENT_SET_PLAYER_IS_CRITICAL_TO_FM_EVENT(TRUE)
			ENDIF
				
		ELSE
		
			// We are not holding a castle area. If we are considered critical, clear this flag
			IF FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT(PLAYER_ID())
				FM_EVENT_SET_PLAYER_IS_CRITICAL_TO_FM_EVENT(FALSE)
			ENDIF
			
		ENDIF
		
	ENDIF
	
ENDPROC


PROC PROCESS_KING_OF_THE_CASTLE_SERVER()

	INT iCastleArea
	
	PROCESS_CASTLE_AREA_SERVER()
	
	SWITCH GET_KING_OF_THE_CASTLE_STATE()
	
		CASE eKINGOFTHECASTLESTATE_INITIALISE
			
			IF IS_BIT_SET(serverBD.iBitset0, ENUM_TO_INT(eSERVERBITSET0_ALL_PARTICIPANTS_INITIALISED_LOCATE_DATA))
				PRINTLN("[KINGCASTLE] - [STATE] - eSERVERBITSET0_ALL_PARTICIPANTS_INITIALISED_LOCATE_DATA bit is set.")
				IF NOT HAS_NET_TIMER_STARTED(serverBd.sKingOfTheCastle.AvailableTimer)
					START_NET_TIMER(serverBd.sKingOfTheCastle.AvailableTimer, FALSE, TRUE)
					PRINTLN("[KINGCASTLE] - started AvailableTimer.")
				ENDIF
//				IF NOT HAS_NET_TIMER_STARTED(serverBD.sCastleArea[iCastleArea].KingInUncontestedAreaTimer)
//					START_NET_TIMER(serverBD.sCastleArea[iCastleArea].KingInUncontestedAreaTimer, FALSE, TRUE)
////					IF GET_CASTLE_AREA_KING_PARTICIPANT_ID(iCastleArea) > (-1)
////						FLOAT fKingAreaPoints
////						fKingAreaPoints = GET_CASTLE_AREA_KING_AREA_POINTS(iCastleArea, GET_CASTLE_AREA_KING_PARTICIPANT_ID(iCastleArea))
////						fKingAreaPoints += g_sMPTunables.iKing_of_the_Castle_Points_per_X_seconds_as_King
////						SET_CASTLE_AREA_KING_AREA_POINTS(iCastleArea, GET_CASTLE_AREA_KING_PARTICIPANT_ID(iCastleArea), fKingAreaPoints)
////					ENDIF
//				ENDIF
				SET_KING_OF_THE_CASTLE_STATE(eKINGOFTHECASTLESTATE_FIGHT_OVER_CASTLE)
			ENDIF
			
		BREAK
		
		CASE eKINGOFTHECASTLESTATE_FIGHT_OVER_CASTLE
			
			BOOL bGoToTimeUp
			
			#IF IS_DEBUG_BUILD
			IF serverBD.iSPassParticipant > (-1)
			OR serverBD.iFFailParticipant > (-1)
				IF serverBD.iSPassParticipant > (-1)
					IF IS_BIT_SET(iParticipantActiveBitset, serverBD.iSPassParticipant)
						REPEAT NUM_CASTLE_AREAS iCastleArea
							PRINTLN("[KINGCASTLE] - [DEBUG SKIPPING] - participant ", serverBD.iSPassParticipant, " pressed s pass. Setting points to 200000 so they win.")
							SET_CASTLE_AREA_KING_AREA_POINTS(iCastleArea, serverBD.iSPassParticipant, 200000)
						ENDREPEAT
						SET_KING_OF_THE_CASTLE_STATE(eKINGOFTHECASTLESTATE_TIME_UP)
					ENDIF
				ELIF serverBD.iFFailParticipant > (-1)
					IF IS_BIT_SET(iParticipantActiveBitset, serverBD.iFFailParticipant)
						REPEAT NUM_CASTLE_AREAS iCastleArea
							PRINTLN("[KINGCASTLE] - [DEBUG SKIPPING] - participant ", serverBD.iSPassParticipant, " pressed f fail. Setting points to -200000 so they lose.")
							SET_CASTLE_AREA_KING_AREA_POINTS(iCastleArea, serverBD.iFFailParticipant, -200000)
						ENDREPEAT
						SET_KING_OF_THE_CASTLE_STATE(eKINGOFTHECASTLESTATE_TIME_UP)
					ENDIF
				ENDIF
			ENDIF
			#ENDIF	
			
			INT iTime
			
			// If somebody has entered the castle, use the main mode timer. 
			IF IS_BIT_SET(serverBD.iBitset0, ENUM_TO_INT(eSERVERBITSET0_CASTLE_AREA_HAS_BEEN_POPULATED))
				
				iTime = g_sMPTunables.iKing_of_the_Castle_Event_Time_Limit
				
				IF HAS_NET_TIMER_STARTED(serverBD.sKingOfTheCastle.ModeTimer)
					IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.sKingOfTheCastle.ModeTimer) >= iTime
						PRINTLN("[KINGCASTLE] - ModeTimer > g_sMPTunables.iKing_of_the_Castle_Event_Time_Limit - bGoToTimeUp = TRUE")
						bGoToTimeUp = TRUE
					ENDIF
				ENDIF
			
			// If nobody has entered the castle, use the available timer.
			ELSE
			
				iTime = g_sMPTunables.iKing_of_the_Castle_Event_Expiry_Time
				
				IF HAS_NET_TIMER_STARTED(serverBD.sKingOfTheCastle.AvailableTimer)
					IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.sKingOfTheCastle.AvailableTimer) >= iTime
						PRINTLN("[KINGCASTLE] - AvailableTimer > g_sMPTunables.iKing_of_the_Castle_Event_Expiry_Time - bGoToTimeUp = TRUE")
						bGoToTimeUp = TRUE
					ENDIF
				ENDIF
				
			ENDIF
				
			IF bGoToTimeUp
			
			#IF IS_DEBUG_BUILD
			OR bSkipToTimeEnd
			OR MPGlobalsAmbience.bEndCurrentFreemodeEvent
				
				// DEBUG ONLY!
				IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				AND (bSkipToTimeEnd OR MPGlobalsAmbience.bEndCurrentFreemodeEvent)
					PRINTLN("[KINGCASTLE] - [MODE TIME] - DEBUG: Offset our event timer to be expired")
					SET_NET_TIMER_TO_SAME_VALUE_AS_TIMER(serverBD.sKingOfTheCastle.ModeTimer, GET_TIME_OFFSET(GET_NETWORK_TIME(), (-1 * iTime)))
				ENDIF
				
				IF NOT IS_BIT_SET(serverBD.iBitset0, ENUM_TO_INT(eSERVERBITSET0_CASTLE_AREA_HAS_BEEN_POPULATED))
					PRINTLN("[KINGCASTLE] - [MODE TIME] - Our mode has timed out without a castle area being populated")
				ENDIF
				
			#ENDIF
			
				PRINTLN("[KINGCASTLE] - [MODE TIME] - time >= ", iTime)
				SET_KING_OF_THE_CASTLE_STATE(eKINGOFTHECASTLESTATE_TIME_UP)
				
			ENDIF
			
		BREAK
		
		CASE eKINGOFTHECASTLESTATE_TIME_UP
			
			IF IS_BIT_SET(serverBD.iBitset0, ENUM_TO_INT(eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS))
				PRINTLN("[KINGCASTLE] - [REWARDS] - all participants completed rewards.")
				SET_KING_OF_THE_CASTLE_STATE(eKINGOFTHECASTLESTATE_CLEANUP)
			ENDIF
			
		BREAK
		
		CASE eKINGOFTHECASTLESTATE_CLEANUP
			SET_KING_OF_THE_CASTLE_STATE(eKINGOFTHECASTLESTATE_END)
		BREAK
		
		CASE eKINGOFTHECASTLESTATE_END
			SET_SERVER_GAME_STATE(eGAMESTATE_END)
		BREAK
		
	ENDSWITCH
	
	REPEAT NUM_CASTLE_AREAS iCastleArea
		PROCESS_CASTLE_AREA_KING_POINTS(iCastleArea)
	ENDREPEAT
	
ENDPROC


/// PURPOSE:
///    Take the actual position of the player from the leaderboard and convert to
///    a position modifier for use in the reward formula.
/// PARAMS:
///    iPosition - INT Leaderboard position.
/// RETURNS:
///    FLOAT position modifier.
FUNC FLOAT GET_POSITION_MODIFIER(INT iPosition)
	SWITCH iPosition
		CASE 0 	RETURN 	g_sMPTunables.fKING_OF_THE_CASTLE_MODIFIER_IN_PLACE_1
		CASE 1 	RETURN  g_sMPTunables.fKING_OF_THE_CASTLE_MODIFIER_IN_PLACE_2
		CASE 2 	RETURN 	g_sMPTunables.fKING_OF_THE_CASTLE_MODIFIER_IN_PLACE_3
		DEFAULT RETURN  0.0
	ENDSWITCH
ENDFUNC


/// PURPOSE:
///    Using the predefined forumla, calculate the reward (RP or GTA$).
FUNC INT CALCULATE_REWARD_USING_FORUMLA(INT iBase, 				INT iCashScale, 
										INT iTimeMins, 			FLOAT fPositionMod, 	
										FLOAT fTimeWeight, 		FLOAT fPlayerWeight, 	
										FLOAT fPlayerDivider,	INT iParticipantCount)
	
	// Too many args for PRINTLN.
	PRINTSTRING("[KINGCASTLE] - [CALCULATE_EOM_REWARDS] - (iBase)")
	PRINTINT(iBase)
	PRINTSTRING(" * (fPositionMod)")
	PRINTFLOAT(fPositionMod)
	PRINTSTRING(" + ((iCashScale)")
	PRINTINT(iCashScale)
	PRINTSTRING(" * (fPositionMod)")
	PRINTFLOAT(fPositionMod)
	PRINTSTRING(" * ((iTimeMins)")
	PRINTINT(iTimeMins)
	PRINTSTRING(" * (fTimeWeight)")
	PRINTFLOAT(fTimeWeight)
	PRINTSTRING(") * (((iParticipantCount)")
	PRINTINT(iParticipantCount)
	PRINTSTRING(" * (fPlayerWeight)")
	PRINTFLOAT(fPlayerWeight)
	PRINTSTRING(") / (fPlayerDivider)")
	PRINTFLOAT(fPlayerDivider)
	PRINTSTRING(")))")
	PRINTNL()

	// 	(Base x Position + (Scale x Position x (Time Limit(max T_Cap) x T_Weight) x ((Participants(max P_Cap) x P_Weight) / P_Divider))
	RETURN ROUND_TO_NEAREST(ROUND(iBase * fPositionMod + (iCashScale * fPositionMod * (iTimeMins * fTimeWeight) * ((iParticipantCount * fPlayerWeight) / fPlayerDivider))), 50)
	
ENDFUNC


/// PURPOSE:
///    Calculate RP and GTA$ rewards for completing this activity.
/// PARAMS:
///    fPositionMod - Position modifier.
///    iCash - ByREF GTA$ reward.
///    iRP - ByREF RP reward.
PROC CALCULATE_EOM_REWARDS(FLOAT fPositionMod, INT& iCash, INT& iRP, INT &iParticipationCash)
	
	INT iParticipationTime = (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(localParticipationTimer) / 60) / 1000
	IF iParticipationTime > g_sMPTunables.iParticipation_T_Cap
		iParticipationTime = g_sMPTunables.iParticipation_T_Cap
	ELIF iParticipationTime < 1
		iParticipationTime = 1
	ENDIF
	
	PRINTLN("[KINGCASTLE] - [CALCULATE_EOM_REWARDS] - iParticipationTime (mins) = ", iParticipationTime)
	
	iParticipationCash = GET_EOM_DEFAULT_CASH_REWARD() * iParticipationTime
	iRP = GET_EOM_DEFAULT_RP_REWARD()	  * iParticipationTime
	PRINTLN("[KINGCASTLE] - [CALCULATE_EOM_REWARDS] - iCash = GET_EOM_DEFAULT_CASH_REWARD * iParticipationTime = ", iCash)
	PRINTLN("[KINGCASTLE] - [CALCULATE_EOM_REWARDS] - iRP = GET_EOM_DEFAULT_RP_REWARD * iParticipationTime = ", iRP)
	
	IF fPositionMod > 0.0
	
		INT iTimeMins = ((g_sMPTunables.iKing_of_the_Castle_Event_Time_Limit / 60) / 1000)
		PRINTLN("[KINGCASTLE] - [CALCULATE_EOM_REWARDS] - iTimeMins = (g_sMPTunables.iKing_of_the_Castle_Event_Time_Limit / 60) / 1000) = (", g_sMPTunables.iKing_of_the_Castle_Event_Time_Limit, " / 60) / 1000) = ", iTimeMins)
		
		IF iTimeMins > GET_REWARD_TIME_CAP()
			PRINTLN("[KINGCASTLE] - [CALCULATE_EOM_REWARDS] - iTimeMins > cap, reducing. Actual: ", iTimeMins, ", Target: ", GET_REWARD_TIME_CAP())
			iTimeMins = GET_REWARD_TIME_CAP()
			PRINTLN("[KINGCASTLE] - [CALCULATE_EOM_REWARDS] - iTimeMins now = ", iTimeMins)
		ENDIF
		
		INT iParticipantCount = iPeakQualifiedParticipants
		PRINTLN("[KINGCASTLE] - [CALCULATE_EOM_REWARDS] - iParticipantCount = iPeakQualifiedParticipants = ", iParticipantCount)
		
		IF iParticipantCount > GET_REWARD_PLAYER_CAP()
			PRINTLN("[KINGCASTLE] - [CALCULATE_EOM_REWARDS] - iParticipantCount > cap, reducing. Actual: ", iParticipantCount, ", Target: ", GET_REWARD_PLAYER_CAP())
			iParticipantCount = GET_REWARD_PLAYER_CAP()
			PRINTLN("[KINGCASTLE] - [CALCULATE_EOM_REWARDS] - iParticipantCount now = ", iParticipantCount)
		ENDIF

		iCash = ROUND(CALCULATE_REWARD_USING_FORUMLA(	GET_CASH_REWARD_BASE(), 
												GET_CASH_REWARD_SCALE(), 
												iTimeMins, fPositionMod, 
												GET_REWARD_TIME_WEIGHTING(), 
												GET_REWARD_PLAYER_WEIGHTING(), 
												GET_REWARD_PLAYER_DIVIDER(),
												iParticipantCount) * g_sMPTunables.fKing_of_the_Castle_Event_Multiplier_Cash)
		PRINTLN("[KINGCASTLE] - [CALCULATE_EOM_REWARDS] - iCash = CALCULATE_REWARD_USING_FORUMLA = ", iCash)
		
		iRP += ROUND(CALCULATE_REWARD_USING_FORUMLA(	GET_RP_REWARD_BASE(), 
												GET_RP_REWARD_SCALE(), 
												iTimeMins, fPositionMod, 
												GET_REWARD_TIME_WEIGHTING(), 
												GET_REWARD_PLAYER_WEIGHTING(), 
												GET_REWARD_PLAYER_DIVIDER(),
												iParticipantCount)* g_sMPTunables.fKing_of_the_Castle_Event_Multiplier_RP)
		PRINTLN("[KINGCASTLE] - [CALCULATE_EOM_REWARDS] - iRP = CALCULATE_REWARD_USING_FORUMLA = ", iRP)
		
	ENDIF

	PRINTLN("[KINGCASTLE] - [CALCULATE_EOM_REWARDS] - Pos: ",fPositionMod,", results in cash: ",iCash,", and RP: ",iRP, ", iParticipationCash: ", iParticipationCash)

ENDPROC

PROC MAINTAIN_AUDIO()
	
	BOOL bStartScene
	
	IF iFocusParticipant != (-1)
		IF GET_IS_PARTICIPANT_FULLY_TAKING_PART(iFocusParticipant)
			bStartScene = TRUE
		ENDIF
	ENDIF
		
	IF bStartScene
		IF NOT IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_AUDIO_SCENE_ACTIVE))
			START_AUDIO_SCENE("MP_Deathmatch_Type_Challenge_Scene")
			SET_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_AUDIO_SCENE_ACTIVE))
			PRINTLN("[KINGCASTLE] - [AUDIO] - called START_AUDIO_SCENE(MP_Deathmatch_Type_Challenge_Scene)")
			PRINTLN("[KINGCASTLE] - [AUDIO] - set bit: iLocalBitset0, eLOCALBITSET0_AUDIO_SCENE_ACTIVE")
		ENDIF
		
	ELSE
		
		IF IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_AUDIO_SCENE_ACTIVE))
			STOP_AUDIO_SCENE("MP_Deathmatch_Type_Challenge_Scene")
			CLEAR_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_AUDIO_SCENE_ACTIVE))
			PRINTLN("[KINGCASTLE] - [AUDIO] - called STOP_AUDIO_SCENE(MP_Deathmatch_Type_Challenge_Scene)")
			PRINTLN("[KINGCASTLE] - [AUDIO] - cleared bit: iLocalBitset0, eLOCALBITSET0_AUDIO_SCENE_ACTIVE")
		ENDIF
		
	ENDIF
	
ENDPROC

#IF IS_DEBUG_BUILD
PROC MAINTAIN_DEBUG_SKIPS()
	
	INT iCastleArea
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
		playerBD[PARTICIPANT_ID_TO_INT()].bSPass = TRUE
		PRINTLN("[KINGCASTLE] - [DEBUG SKIPPING] - I have pressed S pass.")
	ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
		playerBD[PARTICIPANT_ID_TO_INT()].bFFail = TRUE
		PRINTLN("[KINGCASTLE] - [DEBUG SKIPPING] - I have pressed F fail.")
	ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
		bDoJSkip = TRUE
		PRINTLN("[KINGCASTLE] - [DEBUG SKIPPING] - I have pressed J skip.")
	ELIF IS_DEBUG_KEY_JUST_PRESSED(KEY_S, KEYBOARD_MODIFIER_ALT, "Alternative S pass")
		bSkipToTimeEnd = TRUE
		PRINTLN("[KINGCASTLE] - [DEBUG SKIPPING] - I have pressed alternative S pass.")
	ENDIF
	
	IF bDoJSkip
		SPAWN_SEARCH_PARAMS SpawnSearchParams
		SpawnSearchParams.fMinDistFromPlayer = 1.0
		SpawnSearchParams.fAvoidRadius[0] = 1.5
		REPEAT NUM_CASTLE_AREAS iCastleArea
			IF NOT IS_VECTOR_ZERO(vJSkipCoords)
				IF NET_WARP_TO_COORD(vJSkipCoords, 0.0)
					bDoJSkip = FALSE
					vJSkipCoords = << 0.0, 0.0, 0.0 >>
					fJSkipHeading = 0.0
					PRINTLN("[KINGCASTLE] - [DEBUG SKIPPING] - completed J skip to near coords ", vJSkipCoords)
				ENDIF
			ELSE
				GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY(GET_CASTLE_AREA_BLIP_COORDS_FROM_SEED(iCastleArea), 5.0, vJSkipCoords, fJSkipHeading, SpawnSearchParams)
			ENDIF
		ENDREPEAT
	ENDIF
	
ENDPROC
#ENDIF

/// PURPOSE:
///    Render bottom right HUD that displays top players and their scores
/// PARAMS:
///    iCastleArea - 
PROC RENDER_KING_OF_THE_CASTLE_BOTTOM_RIGHT(INT iCastleArea)
	
	IF GET_CASTLE_AREA_STATE(iCastleArea) != eCASTLEAREASTATE_UNENTERED
		MAINTAIN_DPAD_DOWN_LEADERBOARD(iCastleArea)
		DRAW_BOTTOM_RIGHT_HUD(iCastleArea)
	ENDIF
	
ENDPROC

PROC UPDATE_KING_OF_THE_CASTLE_TIME_TO_OBJECTIVE_TELEMETRY()
	
	IF SHOULD_HIDE_THIS_AMBIENT_EVENT()
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_UPDATE_TIME_TO_OBJ_TELEMETRY))
		PRINTLN("[KINGCASTLE] - bit eLOCALBITSET0_UPDATE_TIME_TO_OBJ_TELEMETRY is set")
		IF iKingPartTelemetryCopy = PARTICIPANT_ID_TO_INT()
			PRINTLN("[KINGCASTLE] - iKingPartTelemetryCopy = ", iKingPartTelemetryCopy, " = me")
			// Commenting out, this is handled in James's shard. 
			// PLAY_SOUND_FRONTEND(-1, "Castle_Capture_Player", "GTAO_FM_Events_Soundset", FALSE) 
			IF telemetryStruct.m_timeTakenForObjective = 0
				telemetryStruct.m_timeTakenForObjective	= GET_CLOUD_TIME_AS_INT()
				PRINTLN("[KINGCASTLE] - set telemetryStruct.m_timeTakenForObjective = ", telemetryStruct.m_timeTakenForObjective)
			ENDIF
		ELSE
//			IF NOT SHOULD_HIDE_THIS_AMBIENT_EVENT()
//				PLAY_SOUND_FRONTEND(-1, "Castle_Capture_Remote", "GTAO_FM_Events_Soundset", FALSE)
//				PRINTLN("[KINGCASTLE] - UPDATE_KING_OF_THE_CASTLE_TIME_TO_OBJECTIVE_TELEMETRY Castle_Capture_Remote")
//			ENDIF
		ENDIF
		CLEAR_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_UPDATE_TIME_TO_OBJ_TELEMETRY))
	ENDIF
ENDPROC

PROC MAINTAIN_TRACKING_NUMBER_OF_KINGS(INT iCastleArea)
	
	IF iKingPartTelemetryCopy != GET_CASTLE_AREA_KING_PARTICIPANT_ID(iCastleArea)
		iKingPartTelemetryCopy = GET_CASTLE_AREA_KING_PARTICIPANT_ID(iCastleArea)
		IF iKingPartTelemetryCopy > -1 
			iNumOfKings++
			PRINTLN("[KINGCASTLE] - we have a new king - king part now equals ", iKingPartTelemetryCopy, ", total num kings thise event equals ", iNumOfKings)
			SET_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_UPDATE_TIME_TO_OBJ_TELEMETRY))
			PRINTLN("[KINGCASTLE] - set bit eLOCALBITSET0_UPDATE_TIME_TO_OBJ_TELEMETRY")
		ELSE
			PRINTLN("[KINGCASTLE] - we have NO king - king part now equals ", iKingPartTelemetryCopy, ", total num kings thise event equals ", iNumOfKings)
		ENDIF
	ENDIF
	
ENDPROC

PROC GIVE_REWARDS(INT iCastleArea, INT iPosition)
					
	IF NOT IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_GIVEN_EOM_REWARDS))
		
		IF GET_CASTLE_AREA_STATE(iCastleArea) != eCASTLEAREASTATE_UNENTERED
			
			IF NOT FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(PLAYER_ID())
				IF NOT FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID())
				
					IF IS_PARTICIPANT_QUALIFYING(PARTICIPANT_ID_TO_INT())
						
			//			REPEAT NUM_CASTLE_AREAS iCastleArea

								INT iCash, iRP, iParticipationCash
								
								IF iPosition <= 2
									IF GET_IS_PARTICIPANT_FULLY_TAKING_PART(PARTICIPANT_ID_TO_INT())
									OR IS_PARTICIPANT_QUALIFYING(PARTICIPANT_ID_TO_INT())
										CALCULATE_EOM_REWARDS(GET_POSITION_MODIFIER(iPosition), iCash, iRP, iParticipationCash)
									ENDIF
								ELSE
									FM_EVENT_SETUP_DO_MINIMUM_PARTICIPATION_HELP(TRUE)
								ENDIF
								IF iRP > 0
									NEXT_RP_ADDITION_SHOW_RANKBAR()
									GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD,PLAYER_PED_ID(),"",XPTYPE_COMPLETE,XPCATEGORY_COMPLETE_CHALLENGES,iRP) 
									telemetryStruct.m_rpEarned += iRP
								ELSE
									PRINTLN("[KINGCASTLE] - [REWARDS] - Calculated RP reward is below threshold, not awarding. Value: ", iRP)
								ENDIF
								
								IF iCash > 0
									PRINTLN("[KINGCASTLE] - [REWARDS] - Cash before boss cut: $", iCash)
									GB_HANDLE_GANG_BOSS_CUT(iCash)
									PRINTLN("[KINGCASTLE] - [REWARDS] - Cash after boss cut: $", iCash)
								ENDIF
								
								iCash += iParticipationCash
								PRINTLN("[KINGCASTLE] - [REWARDS] - Cash after adding particpation cash (", iParticipationCash, ") = $", iCash)
								
								IF iCash > 0
									IF USE_SERVER_TRANSACTIONS()
										INT iScriptTransactionIndex
										TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_AMBIENT_JOB_KING, iCash, iScriptTransactionIndex, FALSE, TRUE) //Add new service
									ELSE
										AMBIENT_JOB_DATA amData
										NETWORK_EARN_FROM_AMBIENT_JOB(iCash, "AM_KING_OF_THE_HILL", amData)
									ENDIF
									telemetryStruct.m_cashEarned += iCash
									
									IF NOT g_sMPTunables.bKing_of_the_Castle_Disable_Share_Cash
										IF telemetryStruct.m_cashEarned > 0							
											SET_LAST_JOB_DATA(LAST_JOB_KING_CASTLE, telemetryStruct.m_cashEarned)
										ENDIF
									ENDIF
									g_i_cashForEndEventShard = iCash
								ELSE
									PRINTLN("[KINGCASTLE] - [REWARDS] - Calculated cash reward is below threshold, not awarding. Value: ", iCash)
								ENDIF

								
								PRINTLN("[KINGCASTLE] - [REWARDS] - awarded player GTA$ ", iCash, ", RP: ",iRP," for finishing in position ", iPosition)
							
			//			ENDREPEAT
					
					ELSE
						FM_EVENT_SETUP_DO_MINIMUM_PARTICIPATION_HELP(FALSE)
						PRINTLN("[KINGCASTLE] - [REWARDS] - local player is not a qualifying participant, no rewards.")
					
					ENDIF
				
				ELSE
					FM_EVENT_SETUP_DO_MINIMUM_PARTICIPATION_HELP(FALSE)
					PRINTLN("[KINGCASTLE] - [REWARDS] - FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE, no rewards.")
					
				ENDIF
				
			ELSE
				FM_EVENT_SETUP_DO_MINIMUM_PARTICIPATION_HELP(FALSE)
				PRINTLN("[KINGCASTLE] - [REWARDS] - FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION, no rewards.")
				
			ENDIF
			
		ELSE
			FM_EVENT_SETUP_DO_MINIMUM_PARTICIPATION_HELP(FALSE)
			PRINTLN("[KINGCASTLE] - [REWARDS] - GET_CASTLE_AREA_STATE(iCastleArea) != eCASTLEAREASTATE_UNENTERED, no rewards.")
			
		ENDIF
		
		SET_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_GIVEN_EOM_REWARDS))
		PRINTLN("[KINGCASTLE] - [SHARDS] - set bit: iLocalBitset0, eLOCALBITSET0_DISPLAYED_INTRO_BIG_MESSAGE")
	ENDIF
	
ENDPROC
		
PROC MAINTAIN_SET_DO_FM_EVENT_FIRST_PLACE_HELP_IF_NEEDED(INT iCastleArea)
	
	IF NOT IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_SET_DO_FIRST_PLACE_HELP))
		IF iFocusParticipant = PARTICIPANT_ID_TO_INT()
			IF GET_IS_PARTICIPANT_FULLY_TAKING_PART(PARTICIPANT_ID_TO_INT())
				IF NOT SHOULD_HIDE_THIS_AMBIENT_EVENT(TRUE, TRUE)
					IF serverBD.sCastleArea[iCastleArea].sLeaderboardData[0].iParticipant != (-1)
						IF serverBD.sCastleArea[iCastleArea].sLeaderboardData[0].fTotalKingPoints > 0
							IF serverBD.sCastleArea[iCastleArea].sLeaderboardData[0].iParticipant != PARTICIPANT_ID_TO_INT()
								IF NOT GB_ARE_PLAYERS_IN_SAME_GANG(PLAYER_ID(), INT_TO_NATIVE(PLAYER_INDEX, serverBD.sCastleArea[iCastleArea].sLeaderboardData[0].iPlayer))
									SET_DO_FM_EVENT_FIRST_PLACE_HELP_IF_NEEDED()
									SET_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_SET_DO_FIRST_PLACE_HELP))
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_START_PLAYER_PARTICIPATION_TIMER()
	IF NOT HAS_NET_TIMER_STARTED(localParticipationTimer)
		IF IS_PARTICIPANT_QUALIFYING(PARTICIPANT_ID_TO_INT())
			START_NET_TIMER(localParticipationTimer, FALSE, TRUE)
			PRINTLN("[KINGCASTLE] - MAINTAIN_PLAYER_PARTICIPATION_TIMER - Player is now participating, localParticipationTimer has been started.")
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_KING_OF_THE_CASTLE_CLIENT()
	
	INT iCastleArea
	
	PROCESS_CASTLE_AREA_CLIENT()
	
	MAINTAIN_BROADCAST_AMBIENT_EVENT_UI_SHOULD_BE_HIDDEN()
	
	MAINTAIN_START_PLAYER_PARTICIPATION_TIMER()
	
	IF DRAW_CHALLENGE_DPAD_FOR_DEV_SPECTATOR()
		MAINTAIN_DPAD_DOWN_LEADERBOARD(iCastleArea)
	ENDIF
	 
	MAINTAIN_FREEMODE_AMBIENT_EVENT_DISTANT_CHECKS(FMMC_TYPE_KING_CASTLE, GET_CASTLE_AREA_BLIP_COORDS(0), bSet, TO_FLOAT(g_sMPTunables.iFREEMODE_EVENT_UI_ENTRY_DISTANCE), DEFAULT, bCustomSpawn)
	
	// If the event is hidden for spectator or hide option, hide the help. Passive players still get the help (Katie and Rob say so).
	IF SHOULD_HIDE_THIS_AMBIENT_EVENT(FALSE, TRUE)
		CLEANUP_KING_OF_THE_CASTLE_HELP()
	ENDIF
	
	SWITCH GET_KING_OF_THE_CASTLE_STATE()
	
		CASE eKINGOFTHECASTLESTATE_INITIALISE
			
			
			
		BREAK
		
		CASE eKINGOFTHECASTLESTATE_FIGHT_OVER_CASTLE
			
			#IF IS_DEBUG_BUILD
			MAINTAIN_DEBUG_SKIPS()
			#ENDIF
			
			UPDATE_KING_OF_THE_CASTLE_TIME_TO_OBJECTIVE_TELEMETRY()
			
			REPEAT NUM_CASTLE_AREAS iCastleArea
				
//				PROCESS_CASTLE_OWNER_KING_FIXED_TICKER(iCastleArea)

				// Keep updating if a player is critical or not
				PROCESS_CRITICAL_PLAYER_CHECKS(iCastleArea)
				
				MAINTAIN_TRACKING_NUMBER_OF_KINGS(iCastleArea)
				MAINTAIN_SET_DO_FM_EVENT_FIRST_PLACE_HELP_IF_NEEDED(iCastleArea)
				
				IF iFocusParticipant = PARTICIPANT_ID_TO_INT()
				
					IF NOT IS_LOCAL_PARTICIPANT_RESTRICTED(TRUE)
						
						IF GET_DOES_PARTICIPANT_SAY_INSIDE_CASTLE_AREA(PARTICIPANT_ID_TO_INT(), iCastleArea)
							IF FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID()) = FMMC_TYPE_KING_CASTLE
								IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_FULLY_ACTIVE_PARTICIPANT_TRIGGERED_BY_INTERACTION))
									SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_FULLY_ACTIVE_PARTICIPANT_TRIGGERED_BY_INTERACTION))
									FM_EVENT_SET_PLAYER_IS_PERMANENT_PARTICIPANT_IN_FM_EVENT(TRUE)
									PRINTLN("[KINGCASTLE] - [PARTICIPATION] - I have been inside the castle area. Setting bit eCLIENTBITSET0_I_AM_FULLY_ACTIVE_PARTICIPANT_TRIGGERED_BY_INTERACTION.")
									PRINTLN("[KINGCASTLE] - [PARTICIPATION] - I have been inside the castle area. Called FM_EVENT_SET_PLAYER_IS_PERMANENT_PARTICIPANT_IN_FM_EVENT(TRUE).")
								ENDIF
							ENDIF
							IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_QUALIFYING_PARTICIPANT_FOR_ENTERING_CASTLE_AREA))
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_QUALIFYING_PARTICIPANT_FOR_ENTERING_CASTLE_AREA))
								PRINTLN("[KINGCASTLE] - set bit: eCLIENTBITSET0_I_AM_QUALIFYING_PARTICIPANT_FOR_ENTERING_CASTLE_AREA")
							ENDIF
							IF GET_CASTLE_AREA_KING_PARTICIPANT_ID(iCastleArea) = PARTICIPANT_ID_TO_INT()
								IF NOT IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET_BEEN_KING_ONCE))
									SET_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET_BEEN_KING_ONCE))
									PRINTLN("[KINGCASTLE] - set bit: eLOCALBITSET_BEEN_KING_ONCE")
								ENDIF
							ENDIF
						ENDIF
					
						IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_QUALIFYING_PARTICIPANT_FOR_BEING_KING))
							IF GET_CASTLE_AREA_KING_PARTICIPANT_ID(iCastleArea) = PARTICIPANT_ID_TO_INT()
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_QUALIFYING_PARTICIPANT_FOR_BEING_KING))
								PRINTLN("[KINGCASTLE] - set bit: iLocalBitset0, eCLIENTBITSET0_I_AM_QUALIFYING_PARTICIPANT_FOR_BEING_KING")
							ENDIF
						ENDIF
					
					ELSE
						
						IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_FULLY_ACTIVE_PARTICIPANT_TRIGGERED_BY_INTERACTION))
							CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_FULLY_ACTIVE_PARTICIPANT_TRIGGERED_BY_INTERACTION))
							PRINTLN("[KINGCASTLE] - [PARTICIPATION] - event hidden. Clearing bit eCLIENTBITSET0_I_AM_FULLY_ACTIVE_PARTICIPANT_TRIGGERED_BY_INTERACTION.")
						ENDIF
						IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_QUALIFYING_PARTICIPANT_FOR_ENTERING_CASTLE_AREA))
							CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_QUALIFYING_PARTICIPANT_FOR_ENTERING_CASTLE_AREA))
							PRINTLN("[KINGCASTLE] - [PARTICIPATION] - event hidden. Clearing bit eCLIENTBITSET0_I_AM_QUALIFYING_PARTICIPANT_FOR_ENTERING_CASTLE_AREA.")
						ENDIF
						IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_QUALIFYING_PARTICIPANT_FOR_BEING_KING))
							CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_QUALIFYING_PARTICIPANT_FOR_BEING_KING))
							PRINTLN("[KINGCASTLE] - [PARTICIPATION] - event hidden. Clearing bit eCLIENTBITSET0_I_AM_QUALIFYING_PARTICIPANT_FOR_BEING_KING.")
						ENDIF
						IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_QUALIFYING_PARTICIPANT_FOR_DAMAGING_KING))
							CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_QUALIFYING_PARTICIPANT_FOR_DAMAGING_KING))
							PRINTLN("[KINGCASTLE] - [PARTICIPATION] - event hidden. Clearing bit eCLIENTBITSET0_I_AM_QUALIFYING_PARTICIPANT_FOR_DAMAGING_KING.")
						ENDIF
						IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_QUALIFYING_PARTICIPANT_FOR_BEING_DAMAGED_BY_KING))
							CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_QUALIFYING_PARTICIPANT_FOR_BEING_DAMAGED_BY_KING))
							PRINTLN("[KINGCASTLE] - [PARTICIPATION] - event hidden. Clearing bit eCLIENTBITSET0_I_AM_QUALIFYING_PARTICIPANT_FOR_BEING_DAMAGED_BY_KING.")
						ENDIF
						
					ENDIF
					
				ENDIF
				
				IF GET_CASTLE_AREA_STATE(iCastleArea) = eCASTLEAREASTATE_UNENTERED
					IF NOT IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_SHOWN_INIT_HELP))
						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
							IF NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
								IF iFocusParticipant = PARTICIPANT_ID_TO_INT()
									IF NOT SHOULD_HIDE_THIS_AMBIENT_EVENT()
										IF IS_BIT_SET(iPlayerOkBitset, sParticipant[PARTICIPANT_ID_TO_INT()].iPlayerId)
											IF NOT IS_BIT_SET(iPlayerDeadBitset, sParticipant[PARTICIPANT_ID_TO_INT()].iPlayerId)
												PRINT_HELP_NO_SOUND("KOTC_1STHELP", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
												SET_FREEMODE_EVENT_HELP_BACKGROUND(FALSE)
												PLAY_SOUND_FRONTEND(-1, "Event_Start_Text", "GTAO_FM_Events_Soundset", FALSE)
												SET_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_SHOWN_INIT_HELP))
												PRINTLN("[KINGCASTLE] - [HELP TEXT] - printed help KOTC_1STHELP.")
												PRINTLN("[KINGCASTLE] - set bit: iLocalBitset0, eLOCALBITSET0_SHOWN_INIT_HELP")
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					// Don't want mode intro help if we're not in the intro stage any more. 
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("KOTC_1STHELP")
						CLEAR_HELP()
						PRINTLN("[KINGCASTLE] - [HELP TEXT] - GET_CASTLE_AREA_STATE(iCastleArea) != eCASTLEAREASTATE_UNENTERED")
					ENDIF
					IF NOT IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_SHOWN_INIT_HELP))
						SET_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_SHOWN_INIT_HELP))
						PRINTLN("[KINGCASTLE] - set bit: iLocalBitset0, eLOCALBITSET0_SHOWN_INIT_HELP")
					ENDIF
				ENDIF
				
//				IF GET_PARTICIPANT_Z_DISTANCE_FROM_CASTLE_AREA_CENTER_POINT(iCastleArea) <= KING_OF_THE_CASTLE_WANTED_LEVEL_MULTIPLIER_0_DISTANCE
//				OR GET_PARTICIPANT_2D_DISTANCE_FROM_CASTLE_AREA_CENTER_POINT(iCastleArea) <= KING_OF_THE_CASTLE_WANTED_LEVEL_MULTIPLIER_0_DISTANCE
				IF iFocusParticipant = PARTICIPANT_ID_TO_INT()
					IF GET_PARTICIPANT_3D_DISTANCE_FROM_CASTLE_AREA_CENTER_POINT(iCastleArea) <= KING_OF_THE_CASTLE_WANTED_LEVEL_MULTIPLIER_0_DISTANCE
						IF NOT IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_ZEROED_WANTED_MULTIPLIER))
							SET_WANTED_LEVEL_MULTIPLIER(0.0)
							SET_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_ZEROED_WANTED_MULTIPLIER))
							PRINTLN("[KINGCASTLE] - [WANTED MULTIPLIER] - called SET_WANTED_LEVEL_MULTIPLIER(0.0)")
							PRINTLN("[KINGCASTLE] - [CASTLE AREA] - set bit: iLocalBitset0, eLOCALBITSET0_ZEROED_WANTED_MULTIPLIER")
						ENDIF
					ELSE
						IF IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_ZEROED_WANTED_MULTIPLIER))
							SET_WANTED_LEVEL_MULTIPLIER(1.0)
							CLEAR_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_ZEROED_WANTED_MULTIPLIER))
							PRINTLN("[KINGCASTLE] - [WANTED MULTIPLIER] - called SET_WANTED_LEVEL_MULTIPLIER(1.0)")
							PRINTLN("[KINGCASTLE] - [CASTLE AREA] - cleared bit: iLocalBitset0, eLOCALBITSET0_ZEROED_WANTED_MULTIPLIER")
						ENDIF
					ENDIF
				ENDIF
				
				// Get rid of messages saying you are the king, defend the area, if not the king anymore.
				IF iFocusParticipant != (-1)
					IF IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_SETUP_YOU_ARE_KING_BIG_MESSAGE))
					AND GET_CASTLE_AREA_KING_PARTICIPANT_ID(iCastleArea) != iFocusParticipant
						IF NOT IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET_CLEANED_UP_BIG_MESSAGE_FOR_NOT_KING))
							CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_FM_EVENT_KING_OF_THE_CASTLE)
							SET_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET_CLEANED_UP_BIG_MESSAGE_FOR_NOT_KING))
							PRINTLN("[KINGCASTLE] - cleaned up big message BIG_MESSAGE_FM_EVENT_START")
							PRINTLN("[KINGCASTLE] - Set bit: eLOCALBITSET_CLEANED_UP_BIG_MESSAGE_FOR_NOT_KING")
						ENDIF
					ELSE
						IF IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET_CLEANED_UP_BIG_MESSAGE_FOR_NOT_KING))
							CLEAR_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET_CLEANED_UP_BIG_MESSAGE_FOR_NOT_KING))
							PRINTLN("[KINGCASTLE] - Clear bit: eLOCALBITSET_CLEANED_UP_BIG_MESSAGE_FOR_NOT_KING")
						ENDIF
					ENDIF
				ENDIF
				
				IF GET_CASTLE_AREA_STATE(iCastleArea) != eCASTLEAREASTATE_UNENTERED
					
					IF NOT IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_CALLED_COMMON_FREEMODE_AMBIENT_EVENTS_SET_UP))
						COMMON_FREEMODE_AMBIENT_EVENTS_SET_UP(FMMC_TYPE_KING_CASTLE, 0.0, 0.0)
						SET_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_CALLED_COMMON_FREEMODE_AMBIENT_EVENTS_SET_UP))
						PRINTLN("[KINGCASTLE] - set bit: iLocalBitset0, eLOCALBITSET0_CALLED_COMMON_FREEMODE_AMBIENT_EVENTS_SET_UP.")
					ENDIF
					
					IF NOT IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_SETUP_INTRO_BIG_MESSAGE))
							
						IF iFocusParticipant != (-1)
							IF NOT SHOULD_HIDE_THIS_AMBIENT_EVENT()
								IF IS_BIT_SET(iPlayerOkBitset, sParticipant[iFocusParticipant].iPlayerId)
									IF NOT IS_BIT_SET(iPlayerDeadBitset, sParticipant[iFocusParticipant].iPlayerId)
										IF GET_CASTLE_AREA_KING_PARTICIPANT_ID(iCastleArea) != (-1)
										
											IF GET_CASTLE_AREA_KING_PARTICIPANT_ID(iCastleArea) != iFocusParticipant
												//SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_FM_EVENT_KING_OF_THE_CASTLE, INT_TO_NATIVE(PLAYER_INDEX, GET_CASTLE_AREA_KING_PLAYER_ID(iCastleArea)), -1, "KOTC_STRAP2", "KOTC_START", GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE(), DEFAULT, GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE())
												STRING strStrapLine
												strStrapLine = "KOTC_STRAP2"
												IF GB_ARE_PLAYERS_IN_SAME_GANG(PLAYER_ID(), INT_TO_NATIVE(PLAYER_INDEX, GET_CASTLE_AREA_KING_PLAYER_ID(iCastleArea)))
													strStrapLine = "KOTC_STRAP2GN"
												ENDIF
												SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_FM_EVENT_KING_OF_THE_CASTLE, strStrapLine, GET_PLAYER_NAME(INT_TO_NATIVE(PLAYER_INDEX, GET_CASTLE_AREA_KING_PLAYER_ID(iCastleArea))), GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE(), DEFAULT, "KOTC_START", GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE())
												PRINTLN("[KINGCASTLE] - [SHARDS] - setup intro bug message - BIG_MESSAGE_FM_EVENT_KING_OF_THE_CASTLE. Strapline = ", strStrapLine)
											ELSE
												SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_KING_OF_THE_CASTLE, "KOTC_START2", "KOTC_STRAP1", GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE(), DEFAULT, GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE())
												PRINTLN("[KINGCASTLE] - [SHARDS] - setup intro bug message - BIG_MESSAGE_FM_EVENT_KING_OF_THE_CASTLE. Strapline = ", "KOTC_STRAP1")
												SET_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_SETUP_YOU_ARE_KING_BIG_MESSAGE))
												SET_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_DISPLAYED_YOU_ARE_KING_BIG_MESSAGE))
												PRINTLN("[KINGCASTLE] - [SHARDS] - set bit: iLocalBitset0, eLOCALBITSET0_SETUP_YOU_ARE_KING_BIG_MESSAGE so we don't show it twice during intro.")
												PRINTLN("[KINGCASTLE] - [SHARDS] - set bit: iLocalBitset0, eLOCALBITSET0_DISPLAYED_YOU_ARE_KING_BIG_MESSAGE so we don't show it twice during intro.")
											ENDIF
										ELSE
											SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_KING_OF_THE_CASTLE, "KOTC_START", "KOTC_STRAP3", GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE(), DEFAULT, GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE())
											PRINTLN("[KINGCASTLE] - [SHARDS] - setup intro bug message - BIG_MESSAGE_FM_EVENT_KING_OF_THE_CASTLE. Strapline = ", "KOTC_STRAP3")
										ENDIF
										SET_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_SETUP_INTRO_BIG_MESSAGE))
										PRINTLN("[KINGCASTLE] - [SHARDS] - set bit: iLocalBitset0, eLOCALBITSET0_SETUP_INTRO_BIG_MESSAGE")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
					ELSE
						
						IF NOT IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_DISPLAYED_INTRO_BIG_MESSAGE))
							
							IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_FM_EVENT_KING_OF_THE_CASTLE)
								PRINTLN("[KINGCASTLE] - [SHARDS] - intro big message has started displaying - BIG_MESSAGE_FM_EVENT_START.")
								SET_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_DISPLAYED_INTRO_BIG_MESSAGE))
								PRINTLN("[KINGCASTLE] - [SHARDS] - set bit: iLocalBitset0, eLOCALBITSET0_DISPLAYED_INTRO_BIG_MESSAGE")
							ENDIF
						
						ELSE
							
							IF iFocusParticipant != (-1)
								
								IF GET_CASTLE_AREA_KING_PARTICIPANT_ID(iCastleArea) = iFocusParticipant
								
									IF NOT IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_SETUP_YOU_ARE_KING_BIG_MESSAGE))
										
										IF NOT SHOULD_HIDE_THIS_AMBIENT_EVENT()
											IF IS_BIT_SET(iPlayerOkBitset, sParticipant[iFocusParticipant].iPlayerId)
												IF NOT IS_BIT_SET(iPlayerDeadBitset, sParticipant[iFocusParticipant].iPlayerId)
													SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_KING_OF_THE_CASTLE, "KOTC_START2", "KOTC_STRAP1")
													SET_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_SETUP_YOU_ARE_KING_BIG_MESSAGE))
													PRINTLN("[KINGCASTLE] - [SHARDS] - setup now the king big message - BIG_MESSAGE_FM_EVENT_START. Strapline = ", "KOTC_STRAP1")
													PRINTLN("[KINGCASTLE] - [SHARDS] - set bit: iLocalBitset0, eLOCALBITSET0_SETUP_YOU_ARE_KING_BIG_MESSAGE.")
												ENDIF
											ENDIF
										ENDIF
										
									ELSE
										
										IF NOT IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_DISPLAYED_YOU_ARE_KING_BIG_MESSAGE))
											IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_FM_EVENT_KING_OF_THE_CASTLE)
												PRINTLN("[KINGCASTLE] - [SHARDS] - now the king big message has started displaying - BIG_MESSAGE_FM_EVENT_START.")
												SET_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_DISPLAYED_YOU_ARE_KING_BIG_MESSAGE))
												PRINTLN("[KINGCASTLE] - [SHARDS] - set bit: iLocalBitset0, eLOCALBITSET0_DISPLAYED_INTRO_BIG_MESSAGE")
											ENDIF
										ENDIF
										
									ENDIF
									
								ELSE
									
									IF IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_SETUP_YOU_ARE_KING_BIG_MESSAGE))
										CLEAR_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_SETUP_YOU_ARE_KING_BIG_MESSAGE))
										PRINTLN("[KINGCASTLE] - [SHARDS] - cleared bit: iLocalBitset0, eLOCALBITSET0_SETUP_YOU_ARE_KING_BIG_MESSAGE since I am not the king.")
									ENDIF
									
									IF IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_DISPLAYED_YOU_ARE_KING_BIG_MESSAGE))
										CLEAR_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_DISPLAYED_YOU_ARE_KING_BIG_MESSAGE))
										PRINTLN("[KINGCASTLE] - [SHARDS] - cleared bit: iLocalBitset0, eLOCALBITSET0_DISPLAYED_YOU_ARE_KING_BIG_MESSAGE since I am not the king.")
									ENDIF
									
								ENDIF
								
							ELSE
							
								IF IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_SETUP_YOU_ARE_KING_BIG_MESSAGE))
									CLEAR_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_SETUP_YOU_ARE_KING_BIG_MESSAGE))
									PRINTLN("[KINGCASTLE] - [SHARDS] - cleared bit: iLocalBitset0, eLOCALBITSET0_SETUP_YOU_ARE_KING_BIG_MESSAGE since iFocusParticipant = (-1).")
								ENDIF
								
								IF IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_DISPLAYED_YOU_ARE_KING_BIG_MESSAGE))
									CLEAR_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_DISPLAYED_YOU_ARE_KING_BIG_MESSAGE))
									PRINTLN("[KINGCASTLE] - [SHARDS] - cleared bit: iLocalBitset0, eLOCALBITSET0_DISPLAYED_YOU_ARE_KING_BIG_MESSAGE since iFocusParticipant = (-1).")
								ENDIF
							
							ENDIF
								
						ENDIF
						
					ENDIF
					
				ELSE
				
					Clear_Any_Objective_Text_From_This_Script()
				
				ENDIF
				
				IF GET_CASTLE_AREA_STATE(iCastleArea) = eCASTLEAREASTATE_POPULATED
				
					IF GET_CASTLE_AREA_KING_PARTICIPANT_ID(iCastleArea) != (-1)
						IF iFocusParticipant != (-1)
							IF GET_CASTLE_AREA_KING_PARTICIPANT_ID(iCastleArea) = iFocusParticipant
	//							IF GET_IS_CASTLE_AREA_CONTESTED(iCastleArea)
								IF NOT SHOULD_HIDE_THIS_AMBIENT_EVENT(TRUE)
									Print_Objective_Text("KOTC_KINGOB1")
								ELSE
									Clear_Any_Objective_Text_From_This_Script()
								ENDIF
	//							ENDIF
								IF IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_DISPLAYED_CONTESTED_CASTLE_HELP))
									IF iFocusParticipant = PARTICIPANT_ID_TO_INT()
										IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
											IF NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
												IF NOT SHOULD_HIDE_THIS_AMBIENT_EVENT()
													IF IS_BIT_SET(iPlayerOkBitset, sParticipant[PARTICIPANT_ID_TO_INT()].iPlayerId)
														IF NOT IS_BIT_SET(iPlayerDeadBitset, sParticipant[PARTICIPANT_ID_TO_INT()].iPlayerId)
															IF IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_DISPLAYED_INTRO_BIG_MESSAGE))
																IF NOT GB_ARE_PLAYERS_IN_SAME_GANG(PLAYER_ID(), INT_TO_NATIVE(PLAYER_INDEX, GET_CASTLE_AREA_KING_PLAYER_ID(iCastleArea)))
																	IF NOT IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_DISPLAYED_CONTESTED_CASTLE_HELP))
																		PRINT_HELP_NO_SOUND("KOTC_YOUKNG", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
																		SET_FREEMODE_EVENT_HELP_BACKGROUND()
																		SET_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_SHOWN_I_AM_KING_HELP))
																		PRINTLN("[KINGCASTLE] - [HELP TEXT] - printed help KOTC_YOUKNG.")
																		PRINTLN("[KINGCASTLE] - [CASTLE AREA] - set bit: iLocalBitset0, eLOCALBITSET0_SHOWN_I_AM_KING_HELP")
																	ENDIF
																ENDIF
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
							ELSE
	//							IF GET_DOES_PARTICIPANT_SAY_INSIDE_CASTLE_AREA(PARTICIPANT_ID_TO_INT(), iCastleArea)
	//								IF NOT SHOULD_HIDE_THIS_AMBIENT_EVENT(TRUE)
	//									Print_Objective_Text("KOTC_CONOBJ1")
	//								ELSE
	//									Clear_Any_Objective_Text_From_This_Script()
	//								ENDIF
	//							ELSE
								IF iFocusParticipant = PARTICIPANT_ID_TO_INT()
									IF NOT IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_SHOWN_OTHER_IS_KING_HELP))
										IF GET_IS_PARTICIPANT_FULLY_TAKING_PART(PARTICIPANT_ID_TO_INT())
											IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
												IF NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
													IF NOT SHOULD_HIDE_THIS_AMBIENT_EVENT()
														IF IS_BIT_SET(iPlayerOkBitset, sParticipant[PARTICIPANT_ID_TO_INT()].iPlayerId)
															IF NOT IS_BIT_SET(iPlayerDeadBitset, sParticipant[PARTICIPANT_ID_TO_INT()].iPlayerId)
																IF IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_DISPLAYED_INTRO_BIG_MESSAGE))
																	IF NOT GB_ARE_PLAYERS_IN_SAME_GANG(PLAYER_ID(), INT_TO_NATIVE(PLAYER_INDEX, GET_CASTLE_AREA_KING_PLAYER_ID(iCastleArea)))
																		IF NOT IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_SHOWN_OTHER_IS_KING_HELP))
																			PRINT_HELP_NO_SOUND("KOTC_OTHRKG", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
																			SET_FREEMODE_EVENT_HELP_BACKGROUND()
																			SET_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_SHOWN_OTHER_IS_KING_HELP))
																			PRINTLN("[KINGCASTLE] - [HELP TEXT] - printed help KOTC_OTHRKG.")
																			PRINTLN("[KINGCASTLE] - [CASTLE AREA] - set bit: iLocalBitset0, eLOCALBITSET0_SHOWN_OTHER_IS_KING_HELP")
																		ENDIF
																	ELSE
																		IF NOT IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_DISPLAYED_CONTESTED_CASTLE_HELP_GOON))
//																			PRINT_HELP_NO_SOUND("KOTC_OTHRKGGN", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
																			PRINT_HELP_WITH_LITERAL_STRING_AND_COLOURED_STRING_NO_SOUND("KOTC_OTHRKGGN", GB_GET_ORGANIZATION_NAME_AS_A_STRING(), HUD_COLOUR_WHITE, 
																																		"KOTC_KINGBLIP", GET_PLAYER_HUD_COLOUR(PLAYER_ID()))
																			SET_FREEMODE_EVENT_HELP_BACKGROUND()
																			SET_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_DISPLAYED_CONTESTED_CASTLE_HELP_GOON))
																			PRINTLN("[KINGCASTLE] - [HELP TEXT] - printed help KOTC_OTHRKGGN.")
																			PRINTLN("[KINGCASTLE] - [CASTLE AREA] - set bit: iLocalBitset0, eLOCALBITSET0_DISPLAYED_CONTESTED_CASTLE_HELP_GOON")
																		ENDIF
																	ENDIF
																ENDIF
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								IF ( GET_IS_PARTICIPANT_FULLY_TAKING_PART(iFocusParticipant) OR IS_BIT_SET(playerBD[iFocusParticipant].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_WITHIN_RANGE_FOR_UI)) )
								AND IS_BIT_SET(iParticipantActiveBitset, GET_CASTLE_AREA_KING_PARTICIPANT_ID(iCastleArea))
								AND NOT SHOULD_HIDE_THIS_AMBIENT_EVENT(TRUE)
									IF GB_ARE_PLAYERS_IN_SAME_GANG(PLAYER_ID(), INT_TO_NATIVE(PLAYER_INDEX, GET_CASTLE_AREA_KING_PLAYER_ID(iCastleArea)))
										Print_Objective_Text_With_Player_Name_And_String_Coloured("KOTC_PERMKNGb", INT_TO_NATIVE(PLAYER_INDEX, GET_CASTLE_AREA_KING_PLAYER_ID(iCastleArea)), "KOTC_KINGNAME", HUD_COLOUR_WHITE, GET_PLAYER_HUD_COLOUR(PLAYER_ID()))
									ELSE
										Print_Objective_Text_With_User_Created_String("KOTC_PERMKNG", sParticipant[GET_CASTLE_AREA_KING_PARTICIPANT_ID(iCastleArea)].tl63Name)
									ENDIF
								ELSE
									Clear_Any_Objective_Text_From_This_Script()
								ENDIF
	//							ENDIF
							ENDIF
						ELSE
							Clear_Any_Objective_Text_From_This_Script()
						ENDIF
					ELSE
						
						IF iFocusParticipant != (-1)
						
							IF NOT SHOULD_HIDE_THIS_AMBIENT_EVENT(TRUE)
								IF GET_DOES_PARTICIPANT_SAY_INSIDE_CASTLE_AREA(PARTICIPANT_ID_TO_INT(), iCastleArea)
									Print_Objective_Text("KOTC_KLLALL")
								ELSE
									Print_Objective_Text("KOTC_ENTER")
								ENDIF
							ELSE
								Clear_Any_Objective_Text_From_This_Script()
							ENDIF
							
							IF NOT IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_DISPLAYED_CONTESTED_CASTLE_HELP))
								IF iFocusParticipant = PARTICIPANT_ID_TO_INT()
									IF GET_IS_PARTICIPANT_FULLY_TAKING_PART(PARTICIPANT_ID_TO_INT())
										IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
											IF NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
												IF NOT SHOULD_HIDE_THIS_AMBIENT_EVENT()
													IF IS_BIT_SET(iPlayerOkBitset, sParticipant[PARTICIPANT_ID_TO_INT()].iPlayerId)
														IF NOT IS_BIT_SET(iPlayerDeadBitset, sParticipant[PARTICIPANT_ID_TO_INT()].iPlayerId)
															IF GET_IS_CASTLE_AREA_CONTESTED(iCastleArea)
																PRINT_HELP_NO_SOUND("KOTC_CONTS", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
																SET_FREEMODE_EVENT_HELP_BACKGROUND()
																SET_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_DISPLAYED_CONTESTED_CASTLE_HELP))
																PRINTLN("[KINGCASTLE] - [HELP TEXT] - printed help KOTC_CONTS.")
																PRINTLN("[KINGCASTLE] - [CASTLE AREA] - set bit: iLocalBitset0, eLOCALBITSET0_DISPLAYED_CONTESTED_CASTLE_HELP")
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
						ELSE
							
							Clear_Any_Objective_Text_From_This_Script()
							
						ENDIF
						
					ENDIF
					
				ENDIF
				
				IF iFocusParticipant != (-1)
					IF GET_CASTLE_AREA_STATE(iCastleArea) = eCASTLEAREASTATE_EMPTY
						IF GET_IS_PARTICIPANT_FULLY_TAKING_PART(PARTICIPANT_ID_TO_INT())
						AND NOT SHOULD_HIDE_THIS_AMBIENT_EVENT(TRUE)
							Print_Objective_Text("KOTC_ENTER")
						ELSE
							Clear_Any_Objective_Text_From_This_Script()
						ENDIF
					ENDIF
				ELSE
					Clear_Any_Objective_Text_From_This_Script()
				ENDIF
				
				RENDER_KING_OF_THE_CASTLE_BOTTOM_RIGHT(iCastleArea)
				
				IF FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID()) = FMMC_TYPE_KING_CASTLE
					MAINTAIN_KING_OF_THE_CASTLE_SPAWNING(iCastleArea)
				ENDIF
				
				MAINTAIN_AUDIO()
				
			ENDREPEAT
			
		BREAK
		
		CASE eKINGOFTHECASTLESTATE_TIME_UP
			
			INT i
			INT iPosition
			STRING strTitle, strStrapLine
			BOOL bIWasAWinner
			INT iMyLbSlot
			iPosition = -1
			BOOL bNobodyWon
			
			REPEAT NUM_CASTLE_AREAS iCastleArea
				REMOVE_CASTLE_AREA_CENTER_POINT_BLIP(iCastleArea)
				REMOVE_CASTLE_AREA_RADIUS_BLIP(iCastleArea)
			ENDREPEAT
			Clear_Any_Objective_Text_From_This_Script()
			CLEANUP_KING_OF_THE_CASTLE_HELP()
			DEACTIVATE_ALL_CASTLE_AREAS_MINIMAP_COMPONENTS()
			
//			REPEAT NUM_LEADERBOARD_PLAYERS i
//				IF iFinalScores[i] = 0
//					iFinalScores[i] = ACCURATLEY_ROUND_SCORE(sCastleAreaLocal[iCastleArea].sBottomRightData[i].fTotalKingPoints)
//				ENDIF
//			ENDREPEAT
//	
//			IF iFinalScores[3] = 0
//				iFinalScores[3] = ACCURATLEY_ROUND_SCORE(GET_CASTLE_AREA_KING_TOTAL_POINTS(iCastleArea, iFocusParticipant))
//			ENDIF			
			
			#IF IS_DEBUG_BUILD
			PRINTLN("")
			PRINTLN("[KINGCASTLE] - [SHARDS] - ********** Printing Final Leaderboard info **********")
			PRINTLN("")
			REPEAT NUM_CASTLE_AREAS iCastleArea
				REPEAT NUM_NETWORK_PLAYERS i
					PRINTLN("[KINGCASTLE] - [SHARDS] - iCastleArea = ", iCastleArea, ", position = ", i, ", participantId = ", serverBD.sCastleArea[iCastleArea].sLeaderboardData[i].iParticipant, " score = ", serverBD.sCastleArea[iCastleArea].sLeaderboardData[i].fTotalKingPoints)			
				ENDREPEAT
			ENDREPEAT
			PRINTLN("")
			PRINTLN("[KINGCASTLE] - [SHARDS] - ********** Printed Final Leaderboard info **********")
			PRINTLN("")
			#ENDIF
			
			strTitle = "KOTC_OVER1"
			strStrapLine = "KOTC_OVER2"
			iMyLbSlot = -1
			
			#IF IS_DEBUG_BUILD
			IF NOT bForceNotPlacedWinner
			#ENDIF
			
			REPEAT NUM_CASTLE_AREAS iCastleArea
				IF iFocusParticipant != (-1)
					IF IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET_BEEN_KING_ONCE))
						REPEAT NUM_LEADERBOARD_PLAYERS i
							IF serverBD.sCastleArea[iCastleArea].sLeaderboardData[i].iParticipant = iFocusParticipant
								IF serverBD.sCastleArea[iCastleArea].sLeaderboardData[i].fTotalKingPoints > 0
									PRINTLN("[KINGCASTLE] - [SHARDS] - iFocusParticipant in final leaderboard = ", iFocusParticipant, ". iCastleArea = ", iCastleArea, ", position = ", i, ", participantId = ", serverBD.sCastleArea[iCastleArea].sLeaderboardData[i].iParticipant)
									iPosition = i
									iMyLbSlot = i
									SWITCH i
										CASE 0
											strStrapLine = "KOTC1ST_STR"
											strTitle = "KOTC_WIN1"
											bIWasAWinner = TRUE
											PRINTLN("[KINGCASTLE] - [SHARDS] - I was in 1st place, setup 1st place big message.")
										BREAK
										CASE 1
											strStrapLine = "KOTC2ND_STR"
											bIWasAWinner = TRUE
											PRINTLN("[KINGCASTLE] - [SHARDS] - I was in 2nd place, setup 2nd place big message.")
										BREAK
										CASE 2
											strStrapLine = "KOTC3RD_STR"
											bIWasAWinner = TRUE
											PRINTLN("[KINGCASTLE] - [SHARDS] - I was in 3rd place, setup 3rd place big message.")
										BREAK
									ENDSWITCH
								ENDIF
							ENDIF
						ENDREPEAT
					ELSE
						PRINTLN("[HOTPROPERTY] - [SHARDS] - no placement, player never entered the castle area.")
					ENDIF
				ENDIF
				
				// Keep rendering the scoreboard throughout this stage
				RENDER_KING_OF_THE_CASTLE_BOTTOM_RIGHT(iCastleArea)
				
				IF serverBD.sCastleArea[iCastleArea].sLeaderboardData[0].iParticipant = (-1)
					bNobodyWon = TRUE
					PRINTLN("[KINGCASTLE] - [SHARDS] - top of leaderboard does not have a valid participant value, setting bNobodyWon = TRUE.")
				ELSE
					IF NOT IS_BIT_SET(iParticipantActiveBitset, serverBD.sCastleArea[iCastleArea].sLeaderboardData[0].iParticipant)
						bNobodyWon = TRUE
						PRINTLN("[KINGCASTLE] - [SHARDS] - top of leaderboard participant is not active, setting bNobodyWon = TRUE.")
					ELSE
						IF serverBD.sCastleArea[iCastleArea].sLeaderboardData[0].fTotalKingPoints <= 0
							bNobodyWon = TRUE
							PRINTLN("[KINGCASTLE] - [SHARDS] - top of leaderboard participant score = ", serverBD.sCastleArea[iCastleArea].sLeaderboardData[0].fTotalKingPoints, ", setting bNobodyWon = TRUE.")
						ENDIF
					ENDIF
				ENDIF
				
			ENDREPEAT
			
			#IF IS_DEBUG_BUILD
			ENDIF
			#ENDIF
			
			IF bIWasAWinner
				telemetryStruct.m_endReason       	=AE_END_WON
			ENDIF
				
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_COMPLETED_REWARDS))
			AND FM_EVENT_END_SHARD_SAFE_TO_DISPLAY()				
				
				IF NOT IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_SETUP_EOM_BIG_MESSAGE))
					REPEAT NUM_CASTLE_AREAS iCastleArea
						IF GET_CASTLE_AREA_STATE(iCastleArea) = eCASTLEAREASTATE_UNENTERED
							IF NOT SHOULD_HIDE_THIS_AMBIENT_EVENT()
								PRINT_TICKER("KOTC_NOENTRY")
							ENDIF
						ELSE
							IF bNobodyWon
								IF iFocusParticipant != (-1)
									IF NOT SHOULD_HIDE_THIS_AMBIENT_EVENT()
										IF NOT SHOULD_HIDE_THIS_AMBIENT_EVENT()
											IF IS_BIT_SET(iPlayerOkBitset, sParticipant[iFocusParticipant].iPlayerId)
												IF NOT IS_BIT_SET(iPlayerDeadBitset, sParticipant[iFocusParticipant].iPlayerId)
													IF GET_IS_PARTICIPANT_FULLY_TAKING_PART(iFocusParticipant)
														SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_DRAW, "KOTC_OVER1", "KOTC_NOWIN", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME)
													ELSE
														PRINT_TICKER("KOTC_NOWIN")
													endif
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF bIWasAWinner
									IF iFocusParticipant != (-1)
										IF NOT SHOULD_HIDE_THIS_AMBIENT_EVENT()
											IF IS_BIT_SET(iPlayerOkBitset, sParticipant[iFocusParticipant].iPlayerId)
												IF NOT IS_BIT_SET(iPlayerDeadBitset, sParticipant[iFocusParticipant].iPlayerId)
													IF GET_IS_PARTICIPANT_FULLY_TAKING_PART(iFocusParticipant)
														SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_FM_EVENT_WIN, ACCURATLEY_ROUND_SCORE(serverBD.sCastleArea[iCastleArea].sLeaderboardData[iMyLbSlot].fTotalKingPoints), strStrapLine, strTitle, DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME)
														PRINTLN("[KINGCASTLE] - [SHARDS] - score passed into shard - ", serverBD.sCastleArea[iCastleArea].sLeaderboardData[iMyLbSlot].fTotalKingPoints)
													ELSE
														PRINT_TICKER_WITH_INT(strStrapLine, ACCURATLEY_ROUND_SCORE(serverBD.sCastleArea[iCastleArea].sLeaderboardData[iMyLbSlot].fTotalKingPoints))
														PRINTLN("[KINGCASTLE] - [SHARDS] - score passed into ticker - ", serverBD.sCastleArea[iCastleArea].sLeaderboardData[iMyLbSlot].fTotalKingPoints)
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ELSE
									IF iFocusParticipant != (-1)
										IF NOT SHOULD_HIDE_THIS_AMBIENT_EVENT()
											IF IS_BIT_SET(iPlayerOkBitset, sParticipant[iFocusParticipant].iPlayerId)
												IF NOT IS_BIT_SET(iPlayerDeadBitset, sParticipant[iFocusParticipant].iPlayerId)
													IF GET_IS_PARTICIPANT_FULLY_TAKING_PART(iFocusParticipant)
														STRING strWinnerName
														strStrapLine = "KOTC_OVER3"
														IF GB_IS_PLAYER_MEMBER_OF_A_GANG(INT_TO_NATIVE(PLAYER_INDEX, serverBD.sCastleArea[iCastleArea].sLeaderboardData[0].iPlayer))
															strWinnerName = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(INT_TO_NATIVE(PLAYER_INDEX, serverBD.sCastleArea[iCastleArea].sLeaderboardData[0].iPlayer))
														ELSE
															strWinnerName = GET_PLAYER_NAME(INT_TO_NATIVE(PLAYER_INDEX, serverBD.sCastleArea[iCastleArea].sLeaderboardData[0].iPlayer))
														ENDIF
														SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_AND_INT(BIG_MESSAGE_FM_EVENT_FAIL_PLAYER_STRING_NUM_STRING, ACCURATLEY_ROUND_SCORE(serverBD.sCastleArea[iCastleArea].sLeaderboardData[0].fTotalKingPoints), strStrapLine, strWinnerName, strTitle, HUD_COLOUR_WHITE, ciFM_EVENTS_END_UI_SHARD_TIME)
													ELSE
//														IF GB_IS_PLAYER_MEMBER_OF_A_GANG(INT_TO_NATIVE(PLAYER_INDEX, serverBD.sCastleArea[iCastleArea].sLeaderboardData[0].iPlayer))
//															strStrapLine = "KOTC_OVER2"
//														ELSE
															strStrapLine = "KOTC_OVER3"
//														ENDIF
														PRINT_TICKER_WITH_PLAYER_NAME_WITH_HUD_COLOUR_AND_INT(strStrapLine, INT_TO_NATIVE(PLAYER_INDEX, serverBD.sCastleArea[iCastleArea].sLeaderboardData[0].iPlayer), HUD_COLOUR_WHITE, ACCURATLEY_ROUND_SCORE(serverBD.sCastleArea[iCastleArea].sLeaderboardData[0].fTotalKingPoints))
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							endif
						ENDIF
					ENDREPEAT
					IF IS_BIT_SET(iPlayerOkBitset, sParticipant[PARTICIPANT_ID_TO_INT()].iPlayerId)
						IF NOT IS_BIT_SET(iPlayerDeadBitset, sParticipant[PARTICIPANT_ID_TO_INT()].iPlayerId)
							SET_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_SETUP_EOM_BIG_MESSAGE))
							PRINTLN("[KINGCASTLE] - [SHARDS] - set bit: iLocalBitset0, eLOCALBITSET0_SETUP_EOM_BIG_MESSAGE")
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_SETUP_EOM_BIG_MESSAGE))
					IF NOT IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_DISPLAYED_EOM_BIG_MESSAGE))
						IF iFocusParticipant != (-1)
							IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_FM_EVENT_WIN)
							OR IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_FM_EVENT_FAIL_PLAYER_STRING_NUM_STRING)
							OR IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_FM_EVENT_DRAW)
							OR NOT GET_IS_PARTICIPANT_FULLY_TAKING_PART(iFocusParticipant)
							OR SHOULD_HIDE_THIS_AMBIENT_EVENT(TRUE, TRUE)
							OR IS_PAUSE_MENU_ACTIVE()
							OR GET_CASTLE_AREA_STATE(0) = eCASTLEAREASTATE_UNENTERED
							OR iFocusParticipant = (-1)
								SET_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_DISPLAYED_EOM_BIG_MESSAGE))
								PRINTLN("[KINGCASTLE] - [SHARDS] - set bit: iLocalBitset0, eLOCALBITSET0_DISPLAYED_EOM_BIG_MESSAGE")
							ENDIF
						ELSE
							SET_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_DISPLAYED_EOM_BIG_MESSAGE))
							PRINTLN("[KINGCASTLE] - [SHARDS] - set bit: iLocalBitset0, eLOCALBITSET0_DISPLAYED_EOM_BIG_MESSAGE - iFocusParticipant = (-1).")
						ENDIf
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_DISPLAYED_EOM_BIG_MESSAGE))
					REPEAT NUM_CASTLE_AREAS iCastleArea
						GIVE_REWARDS(iCastleArea, iPosition)
					ENDREPEAT
				ENDIF
				
			ENDIF
			
			BOOL bShowLbWithEndShard 
			bShowLbWithEndShard = TRUE
			IF SHOULD_HIDE_THIS_AMBIENT_EVENT()
				bShowLbWithEndShard = FALSE
			ENDIF
			IF (GET_CASTLE_AREA_STATE(0) = eCASTLEAREASTATE_UNENTERED)
				bShowLbWithEndShard = FALSE
			ENDIF
			IF iFocusParticipant = (-1)
				bShowLbWithEndShard = FALSE
			ENDIF
				
			// Run the freemode event end ui flow (~10s)
			IF MAINTAIN_FM_EVENTS_END_UI(sEndUiVars, bShowLbWithEndShard) // Only show help if someone actually played the mode. Don't do it if we're ending the mode because nobody took part.	
			AND IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_DISPLAYED_EOM_BIG_MESSAGE))
				
				IF NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
				OR IS_PAUSE_MENU_ACTIVE()
					SET_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_UI_END_TIME_ELAPSED))
					PRINTLN("[KINGCASTLE] - [SHARDS] - set bit: iLocalBitset0, eLOCALBITSET0_UI_END_TIME_ELAPSED")
					IF IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_GIVEN_EOM_REWARDS))
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_COMPLETED_REWARDS))
						PRINTLN("[KINGCASTLE] - [REWARDS] - set bit: playerBD.iBitset0, eCLIENTBITSET0_COMPLETED_REWARDS")
						SET_CLIENT_GAME_STATE(eGAMESTATE_DELAY_TERMINATION)
					ENDIF
				ENDIF
				
			#IF IS_DEBUG_BUILD
			ELSE
				IF GET_GAME_TIMER() % 1000 < 50
					PRINTLN("[KINGCASTLE] - [END UI] - MAINTAIN_FM_EVENTS_END_UI(sEndUiVars) = FALSE")
				ENDIF
			#ENDIF
				
			ENDIF
				
		BREAK
		
		CASE eKINGOFTHECASTLESTATE_CLEANUP
		
		BREAK
		
		CASE eKINGOFTHECASTLESTATE_END
		
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC PROCESS_KING_OF_THE_CASTLE_DAMAGE_EVENT(INT iCount)
    
    PED_INDEX VictimPedID
    PED_INDEX KillerPedID
    PLAYER_INDEX VictimPlayerID
    PLAYER_INDEX KillerPlayerID
    PARTICIPANT_INDEX VictimParticipantID
    PARTICIPANT_INDEX KillerParticipantID
    INT iKillerParticipant = -1
    INT iVictimParticipant = -1
    INT iKillerPlayer = -1
    INT iCastleArea
    INT iTickerParticipantCount
    STRUCT_ENTITY_DAMAGE_EVENT sEntityID
    SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
  
    GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sEntityID, SIZE_OF(sEntityID))
  
    IF DOES_ENTITY_EXIST(sEntityID.VictimIndex)
            
        IF IS_ENTITY_A_PED(sEntityID.VictimIndex)

            VictimPedID = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)

            IF IS_PED_A_PLAYER(VictimPedID)

                VictimPlayerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(VictimPedID)

                IF NETWORK_IS_PLAYER_A_PARTICIPANT(VictimPlayerID)

                    VictimParticipantID = NETWORK_GET_PARTICIPANT_INDEX(VictimPlayerID)

                	IF NETWORK_IS_PARTICIPANT_ACTIVE(VictimParticipantID)
                    	iVictimParticipant = NATIVE_TO_INT(VictimParticipantID)
                   	ENDIF
					
                ENDIF

                IF iVictimParticipant != -1
                    IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
                        IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)

                            KillerPedID = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)

                            IF IS_PED_A_PLAYER(KillerPedID)

                                KillerPlayerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(KillerPedID)
                                iKillerPlayer = NATIVE_TO_INT(KillerPlayerID)
                                                                    
                                IF NETWORK_IS_PLAYER_A_PARTICIPANT(KillerPlayerID)

                                    KillerParticipantID = NETWORK_GET_PARTICIPANT_INDEX(KillerPlayerID)

                                    IF NETWORK_IS_PARTICIPANT_ACTIVE(KillerParticipantID)
                                    	iKillerParticipant = NATIVE_TO_INT(KillerParticipantID)
                                    ENDIF

                                    IF iKillerParticipant != -1
                                                                                    
                                        IF iKillerParticipant != iVictimParticipant
                                            
											PRINTLN("[KINGCASTLE] - [DAMAGE EVENT] - iKillerParticipant != iVictimParticipant")
											
                                            REPEAT NUM_CASTLE_AREAS iCastleArea
                                                                                                
                                                IF iKillerParticipant = GET_CASTLE_AREA_KING_PARTICIPANT_ID(iCastleArea)
                                                    
													PRINTLN("[KINGCASTLE] - [DAMAGE EVENT] - iKillerParticipant = GET_CASTLE_AREA_KING_PARTICIPANT_ID")
													
													IF NOT IS_LOCAL_PARTICIPANT_RESTRICTED(TRUE)
														IF iVictimParticipant = PARTICIPANT_ID_TO_INT()
															PRINTLN("[KINGCASTLE] - [DAMAGE EVENT] - iVictimParticipant = PARTICIPANT_ID_TO_INT")
															IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_QUALIFYING_PARTICIPANT_FOR_BEING_DAMAGED_BY_KING))
																SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_QUALIFYING_PARTICIPANT_FOR_BEING_DAMAGED_BY_KING))
																PRINTLN("[KINGCASTLE] - [DAMAGE EVENT] - Set bit: eCLIENTBITSET0_I_AM_QUALIFYING_PARTICIPANT_FOR_BEING_DAMAGED_BY_KING.")
															ENDIF
														ENDIF
													ENDIF
													
                                                    IF sEntityID.VictimDestroyed
														
														PRINTLN("[KINGCASTLE] - [DAMAGE EVENT] - VictimDestroyed")
														
														// If I am king and I have killed someone.
														IF iKillerParticipant = PARTICIPANT_ID_TO_INT()
															PRINTLN("[KINGCASTLE] - [DAMAGE EVENT] - iKillerParticipant = PARTICIPANT_ID_TO_INT")
	                                                        IF iKillsAsKing < GET_RP_REWARD_KILL_AS_KING_CAP()
	                                                            iKillsAsKing++
	                                                            GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD,KillerPedID,"",XPTYPE_ACTION,XPCATEGORY_ACTION_KILLS, GET_RP_REWARD_KILL_AS_KING()) 
	                                                            telemetryStruct.m_rpEarned += GET_RP_REWARD_KILL_AS_KING()
	                                                            PRINTLN("[KINGCASTLE] - [REWARDS] - [DAMAGE EVENT] - part ", iKillerParticipant, " is me, I am the King and have killed ", iVictimParticipant,", iKillsAsKing: ",iKillsAsKing,
	                                                                            ". Setting bit eCLIENTBITSET0_I_AM_FULLY_ACTIVE_PARTICIPANT_TRIGGERED_BY_INTERACTION.")
	                                                        ELSE
	                                                            iKillsAsKing++
	                                                            PRINTLN("[KINGCASTLE] - [REWARDS] - [DAMAGE EVENT] - part ", iKillerParticipant, " is me, I am the King and have killed ", iVictimParticipant, " with no reward, iKillsAsKing: ",iKillsAsKing, ", call 1.")
	                                                        ENDIF
                                                        ENDIF
														   
                                                        IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
                                                                                                                            
                                                            FLOAT fKingKillPoints = GET_CASTLE_AREA_KING_KILL_POINTS(iCastleArea, iKillerParticipant)
                                                            fKingKillPoints+= g_sMPTunables.iKing_of_the_Castle_Points_per_player_kill_as_King
                                                            SET_CASTLE_AREA_KING_KILL_POINTS(iCastleArea, iKillerParticipant, fKingKillPoints)
                                                            
                                                            #IF IS_DEBUG_BUILD
                                                            FLOAT fAreaPoints = GET_CASTLE_AREA_KING_AREA_POINTS(iCastleArea, iKillerParticipant)
                                                            FLOAT fTotalPoints = GET_CASTLE_AREA_KING_TOTAL_POINTS(iCastleArea, iKillerParticipant)
                                                            PRINTLN("[KINGCASTLE] - [CASTLE AREA KING] - [DAMAGE EVENT] - giving local player (part = ", iKillerParticipant, " ", g_sMPTunables.iKing_of_the_Castle_Points_per_player_kill_as_King, " king kill points for killing part ", iVictimParticipant)
                                                            PRINTLN("[KINGCASTLE] - [CASTLE AREA KING] - [DAMAGE EVENT] - local player (part = ", iKillerParticipant, " king kill points now equals ", fKingKillPoints)
                                                            PRINTLN("[KINGCASTLE] - [CASTLE AREA KING] - [DAMAGE EVENT] - local player (part = ", iKillerParticipant, " king area points now equals ", fAreaPoints)
                                                            PRINTLN("[KINGCASTLE] - [CASTLE AREA KING] - [DAMAGE EVENT] - local player (part = ", iKillerParticipant, " king total points now equals ", fTotalPoints)
                                                            #ENDIF
                                                                                                                            
                                                        ENDIF
                                                                                                                    
                                                    ENDIF
                                                
												// If the King has been damaged.
                                                ELIF iVictimParticipant = GET_CASTLE_AREA_KING_PARTICIPANT_ID(iCastleArea)
                                                    
													PRINTLN("[KINGCASTLE] - [DAMAGE EVENT] - iVictimParticipant = GET_CASTLE_AREA_KING_PARTICIPANT_ID")
													
													// If I have damaged the King.
	                                                IF iKillerParticipant = PARTICIPANT_ID_TO_INT()
	                                                    
														PRINTLN("[KINGCASTLE] - [DAMAGE EVENT] - iKillerParticipant = PARTICIPANT_ID_TO_INT")
														
														IF NOT IS_LOCAL_PARTICIPANT_RESTRICTED(TRUE)
															
															IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_QUALIFYING_PARTICIPANT_FOR_DAMAGING_KING))
																SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_QUALIFYING_PARTICIPANT_FOR_DAMAGING_KING))
																PRINTLN("[KINGCASTLE] - [DAMAGE EVENT] - Set bit: eCLIENTBITSET0_I_AM_QUALIFYING_PARTICIPANT_FOR_DAMAGING_KING.")
															ENDIF
															
															IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_FULLY_ACTIVE_PARTICIPANT_TRIGGERED_BY_INTERACTION))
																SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_FULLY_ACTIVE_PARTICIPANT_TRIGGERED_BY_INTERACTION))
																FM_EVENT_SET_PLAYER_IS_PERMANENT_PARTICIPANT_IN_FM_EVENT(TRUE)
																PRINTLN("[KINGCASTLE] - [PARTICIPATION] - [DAMAGE EVENT] - part ", iKillerParticipant, " is me and I have damaged the King. Called FM_EVENT_SET_PLAYER_IS_PERMANENT_PARTICIPANT_IN_FM_EVENT(TRUE).")
			                                                    PRINTLN("[KINGCASTLE] - [PARTICIPATION] -[REWARDS] - [DAMAGE EVENT] - part ", iKillerParticipant, " is me and I have damaged the King. Setting bit eCLIENTBITSET0_I_AM_FULLY_ACTIVE_PARTICIPANT_TRIGGERED_BY_INTERACTION.")
		                                                    ENDIF
														
														ENDIF
														
														// If I have killed the King.
	                                                    IF sEntityID.VictimDestroyed
															PRINTLN("[KINGCASTLE] - [DAMAGE EVENT] - VictimDestroyed")
	                                                        IF iTimesKilledAKing < GET_RP_REWARD_KILLED_KING_CAP()
	                                                            iTimesKilledAKing++
	                                                            PRINTLN("[KINGCASTLE] - [REWARDS] - [DAMAGE EVENT] - part ", iKillerParticipant, " is me and I have killed the King. iTimesKilledAKing: ",iTimesKilledAKing,
	                                                                            ". Setting bit eCLIENTBITSET0_I_AM_FULLY_ACTIVE_PARTICIPANT_TRIGGERED_BY_INTERACTION.")
	                                                            GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD,KillerPedID,"",XPTYPE_ACTION,XPCATEGORY_ACTION_KILLS, GET_RP_REWARD_KILLED_KING()) 
	                                                            telemetryStruct.m_rpEarned += GET_RP_REWARD_KILLED_KING()
	                                                    	ELSE
	                                                            iTimesKilledAKing++
	                                                            PRINTLN("[KINGCASTLE] - [REWARDS] - [DAMAGE EVENT] - part ", iKillerParticipant, " is me and I have killed the King with no reward, iTimesKilledAKing: ", iTimesKilledAKing, ", call 0.")
	                                                    	ENDIF
	                                                                                                                    
	                                                	ENDIF
	                                                        
													ENDIF
													
													IF iVictimParticipant = PARTICIPANT_ID_TO_INT()
														PRINTLN("[KINGCASTLE] - [DAMAGE EVENT] - iVictimParticipant = PARTICIPANT_ID_TO_INT")
														iDeathsAsKing++
														PRINTLN("[KINGCASTLE] - [REWARDS] - [DAMAGE EVENT] - ", iKillerParticipant, " is me and I have been killed as the King, iDeathsAsKing: ", iDeathsAsKing, ", call 0.")
	                                            	ENDIF
													
													// If the damager was inside the castle area.
                                                    IF GET_DOES_PARTICIPANT_SAY_INSIDE_CASTLE_AREA(iKillerParticipant, iCastleArea)
                                                        
														PRINTLN("[KINGCASTLE] - [DAMAGE EVENT] - iKillerParticipant says GET_DOES_PARTICIPANT_SAY_INSIDE_CASTLE_AREA")
																							  
                                                        IF sEntityID.VictimDestroyed
                                                            
															PRINTLN("[KINGCASTLE] - [DAMAGE EVENT] - VictimDestroyed")
																															
                                                            IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
                                                                                                                                    
                                                                IF GET_CASTLE_AREA_HEIR_PARTICIPANT_ID(iCastleArea) = (-1)
                                                                    SET_CASTLE_AREA_HEIR_PARTICIPANT_ID(iCastleArea, iKillerParticipant)
                                                                ENDIF
                                                                IF GET_CASTLE_AREA_HEIR_PLAYER_ID(iCastleArea) = (-1)
                                                                    SET_CASTLE_AREA_HEIR_PLAYER_ID(iCastleArea, iKillerPlayer)
                                                                ENDIF
                                                                                                                                    
	                                                            SET_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_SET_HEIR_TO_THRONE_AS_KING))
	                                                            PRINTLN("[KINGCASTLE] - [CASTLE AREA KING] - [DAMAGE EVENT] - part ", iKillerParticipant, " has killed the king from inside the castle area. Setting bit eLOCALBITSET0_SET_HEIR_TO_THRONE_AS_KING.")
                                                                                                                                    
                                                            ENDIF
                                                                                                                            
                                                        ENDIF
                                                    
													// If the damager was outside the castle area.
                                                    ELSE
                                                        
														PRINTLN("[KINGCASTLE] - [DAMAGE EVENT] - iKillerParticipant does not say GET_DOES_PARTICIPANT_SAY_INSIDE_CASTLE_AREA")
																												  
                                                        IF sEntityID.VictimDestroyed
                                                            
															PRINTLN("[KINGCASTLE] - [DAMAGE EVENT] - VictimDestroyed")
																															
                                                            IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
                                                                                                                                    
                                                                TickerEventData.TickerEvent = TICKER_EVENT_KILLED_KING
                                                                TickerEventData.playerID = KillerPlayerID
                                                                                                                                    
                                                				REPEAT NUM_NETWORK_PLAYERS iTickerParticipantCount
                                                        			IF IS_BIT_SET(iParticipantActiveBitset, iTickerParticipantCount)
                                                                		IF IS_BIT_SET(iPlayerOkBitset, sParticipant[iTickerParticipantCount].iPlayerId)
																			IF NOT IS_BIT_SET(playerBD[iTickerParticipantCount].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_AMBIENT_EVENT_SHOULD_BE_HIDDEN_NO_PASSIVE_CHECK))
																			AND NOT IS_BIT_SET(playerBD[iTickerParticipantCount].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_AMBIENT_EVENT_SHOULD_BE_HIDDEN_NO_SPECTATOR_CHECK))
	                                                                        	IF GET_IS_PARTICIPANT_FULLY_TAKING_PART(iTickerParticipantCount)
	                                                                                BROADCAST_TICKER_EVENT(TickerEventData, SPECIFIC_PLAYER(INT_TO_NATIVE(PLAYER_INDEX, sParticipant[iTickerParticipantCount].iPlayerId)))
	                                                                                PRINTLN("[KINGCASTLE] - [TICKERS] - [DAMAGE EVENT] - broadcast ticker event TICKER_EVENT_KILLED_KING to fully active participant ", iTickerParticipantCount)
	                                                                            ENDIF
																			ENDIF
                                                                        ENDIF
                                                                    ENDIF
                                                                ENDREPEAT
                                                                                                                                    
                                                                PRINTLN("[KINGCASTLE] - [CASTLE AREA KING] - [DAMAGE EVENT] - part ", iKillerParticipant, " has killed the king from outside the castle area. NOt setting as heir to the throne.")
                                                                                                                                    
                                                            ENDIF
                                                                                                                            
                                                        ENDIF
                                                                                                                    
                                                    ENDIF
                                                                                                            
                                                ENDIF
                                                                                                    
                                            ENDREPEAT
                                                                                            
                                        ELSE
                                                                                            
                                            PRINTLN("[KINGCASTLE] - [CASTLE AREA KING] - [DAMAGE EVENT] - killer part ", iKillerParticipant, " and victim part ", iVictimParticipant, " are the same, not doing anything.")
                                                                                                    
                                        ENDIF
                                                                                    
                                    ENDIF                                           
                                ENDIF
                            ENDIF
                        ENDIF
                    ENDIF                               
                ENDIF 
            ENDIF
        ENDIF
    ENDIF
    
ENDPROC


PROC PROCESS_KING_OF_THE_CASTLE_PLAYER_LEFT_EVENT(INT iCount)

	STRUCT_PLAYER_SCRIPT_EVENTS data
	
	#IF IS_DEBUG_BUILD
	PARTICIPANT_INDEX LeftPlayerParticipantID
	INT iLeftPlayerParticipant
	#ENDIF
	
	INT iLeftPlayerIndex

	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, data, SIZE_OF(data))
	
//		IF NETWORK_IS_PLAYER_A_PARTICIPANT(data.PlayerIndex)
		
//			LeftPlayerParticipantID = NETWORK_GET_PARTICIPANT_INDEX(data.PlayerIndex)
			iLeftPlayerIndex = NATIVE_TO_INT(data.PlayerIndex)
			
			#IF IS_DEBUG_BUILD
			iLeftPlayerParticipant = NATIVE_TO_INT(LeftPlayerParticipantID)
			PRINTLN("[KINGCASTLE] - [PLAYER LEFT] - PROCESS_KING_OF_THE_CASTLE_PLAYER_LEFT_EVENT iLeftPlayerParticipant = ", iLeftPlayerParticipant, "  ")
	
			PRINTLN("[KINGCASTLE] - [PLAYER LEFT] - PROCESS_KING_OF_THE_CASTLE_PLAYER_LEFT_EVENT iLeftPlayerIndex = ", iLeftPlayerIndex, "  ")
			PRINTLN("[KINGCASTLE] - [PLAYER LEFT] - PROCESS_KING_OF_THE_CASTLE_PLAYER_LEFT_EVENT Player Name = ", data.PlayerName, "  ")
			
			PRINTLN("[KINGCASTLE] - [PLAYER LEFT] - PROCESS_KING_OF_THE_CASTLE_PLAYER_LEFT_EVENT My Name = ", GET_PLAYER_NAME(PLAYER_ID()), "  ")
		 
			PRINTLN("[KINGCASTLE] - [PLAYER LEFT] - PROCESS_KING_OF_THE_CASTLE_PLAYER_LEFT_EVENT sPlayer[iLeftPlayerIndex].iParticipantId = ", sPlayer[iLeftPlayerIndex].iParticipantId, "  ")
			#ENDIF
			
			IF sPlayer[iLeftPlayerIndex].iParticipantId != -1
				PROCESS_PLAYER_LEFT_SERVER(sPlayer[iLeftPlayerIndex].iParticipantId)
			ENDIF
			
//		ELSE
//			PRINTLN("[KINGCASTLE] - [CASTLE AREA KING] - NETWORK_IS_PLAYER_A_PARTICIPANT = FALSE data.PlayerIndex = ", NATIVE_TO_INT(data.PlayerIndex), " ")
//	    ENDIF  	

	
	ENDIF

ENDPROC

PROC PROCESS_EVENTS()
	
	INT iCount
	EVENT_NAMES ThisScriptEvent
      
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount
      
		ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)
            
		IF ThisScriptEvent = EVENT_NETWORK_DAMAGE_ENTITY
			PRINTLN("[KINGCASTLE] - EVENT_NETWORK_DAMAGE_ENTITY recieved ")
			PROCESS_KING_OF_THE_CASTLE_DAMAGE_EVENT(iCount)
		ENDIF
		
		IF ThisScriptEvent = EVENT_NETWORK_PLAYER_LEFT_SCRIPT
			PRINTLN("[KINGCASTLE] - [PLAYER LEFT] - EVENT_NETWORK_PLAYER_LEFT_SCRIPT recieved ")
			
//			PROCESS_KING_OF_THE_CASTLE_PLAYER_LEFT_EVENT(iCount)
		ENDIF
      
	ENDREPEAT
	
ENDPROC

FUNC BOOL SHOULD_PARTICIPANT_BLIP_BE_RED(INT iParticipant, INT iPlayer, INT iCastleArea)
	
	IF SHOULD_HIDE_THIS_AMBIENT_EVENT()
		RETURN FALSE
	ENDIF
	
	IF iParticipant = PARTICIPANT_ID_TO_INT()
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_PASSIVE(INT_TO_NATIVE(PLAYER_INDEX, iPlayer))
		RETURN FALSE
	ENDIF
	
	IF iParticipant = GET_CASTLE_AREA_KING_PARTICIPANT_ID(iCastleArea)
		RETURN TRUE
	ENDIF
	
	IF PARTICIPANT_ID_TO_INT() != GET_CASTLE_AREA_KING_PARTICIPANT_ID(iCastleArea)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
					
ENDFUNC

PROC CLEAR_PARTICIPANT_SCORE(INT iParticipant)
	
	INT iCastleArea, iCount
	
	REPEAT NUM_CASTLE_AREAS iCastleArea
		IF GET_CASTLE_AREA_KING_AREA_POINTS(iCastleArea, iParticipant) > 0
		OR GET_CASTLE_AREA_KING_KILL_POINTS(iCastleArea, iParticipant) > 0
			SET_CASTLE_AREA_KING_AREA_POINTS(iCastleArea, iParticipant, 0)
			SET_CASTLE_AREA_KING_KILL_POINTS(iCastleArea, iParticipant, 0)
			PRINTLN("[KINGCASTLE] - resetting particiapnt ", iParticipant, " scores for castle area ", iCastleArea)
		ENDIF
		REPEAT NUM_NETWORK_PLAYERS iCount
			IF serverBD.fHighestScore[iCount] > 0
				IF serverBD.iHighestParticipant[iCount] = iParticipant
					serverBD.fHighestScore[iCount] = 0
					serverBD.iHighestParticipant[iCount] = (-1)
					PRINTLN("[KINGCASTLE] - resetting highest score ", serverBD.fHighestScore[iCount], " for participant ", serverBD.iHighestParticipant[iCount])
				ENDIF
			ENDIF
		ENDREPEAT
	ENDREPEAT
	
ENDPROC

PROC PROCESS_PARTICIPANT_LOOP()
	
	INT iParticipant = -1
	INT iPlayer = -1
	PLAYER_INDEX playerTemp
	PED_INDEX pedTemp
	TEXT_LABEL_63 tl63Temp
	VECTOR vCircularLocateCentrePoint
	FLOAT fCircularLocateRadius
	
	HUD_COLOURS eHudColour
	
	INT iCastleArea, iNumParticipantsInCastleArea, iTempPeakParts, iLocate, iTempPeakQualParts
	
	// Reset data.
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		sPlayer[iPlayer].playerId = INVALID_PLAYER_INDEX()
		sPlayer[iPlayer].iParticipantId = (-1)
		sPlayer[iPlayer].playerPedId = pedTemp
		CLEAR_BIT(iPlayerOkBitset, iPlayer)
		CLEAR_BIT(iPlayerDeadBitset, iPlayer)
	ENDREPEAT
	
	REPEAT NUM_NETWORK_PLAYERS iParticipant
		sParticipant[iParticipant].participantId = INVALID_PARTICIPANT_INDEX()
		sParticipant[iParticipant].iPlayerId = (-1)
		CLEAR_BIT(iParticipantActiveBitset, iParticipant)
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			REPEAT NUM_CASTLE_AREAS iCastleArea
				CLEAR_BIT(serverBD.sCastleArea[iCastleArea].iParticipantsInAreaBitset, iParticipant)
			ENDREPEAT
		ENDIF
		sParticipant[iParticipant].tl63Name = tl63Temp
	ENDREPEAT
	
	REPEAT NUM_CASTLE_AREAS iCastleArea
		SET_CASTLE_AREA_POPULATED(iCastleArea, FALSE)
		SET_NUMBER_OF_PARTICIPANTS_IN_CASTLE_AREA(iCastleArea, 0)
		SET_LOCAL_PARTICIPANT_2D_DISTANCE_FROM_CASTLE_AREA_CENTER_POINT(iCastleArea, 0.0)
		SET_LOCAL_PARTICIPANT_Z_DISTANCE_FROM_CASTLE_AREA_CENTER_POINT(iCastleArea, 0.0)
		SET_LOCAL_PARTICIPANT_3D_DISTANCE_FROM_CASTLE_AREA_CENTER_POINT(iCastleArea, 0.0)
		REPEAT MAX_NUM_LOCATES iLocate
			SET_LOCAL_PARTICIPANT_2D_DISTANCE_FROM_CASTLE_CIRCULAR_LOCATE_POINT(iCastleArea, iLocate, 0.0)
			SET_LOCAL_PARTICIPANT_Z_DISTANCE_FROM_CASTLE_CIRCULAR_LOCATE_POINT(iCastleArea, iLocate, 0.0)
			SET_LOCAL_PARTICIPANT_3D_DISTANCE_FROM_CASTLE_CIRCULAR_LOCATE_POINT(iCastleArea, iLocate, 0.0)
		ENDREPEAT
	ENDREPEAT
	
	BOOL bINITIALISED_LOCATE_DATA = TRUE
	BOOL bCOMPLETED_REWARDS = TRUE
	INT iR, iG, iB, iA
	
	// Refill data.
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant))
			
			sParticipant[iParticipant].participantId = INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant)
			SET_BIT(iParticipantActiveBitset, iParticipant)
			
			playerTemp = NETWORK_GET_PLAYER_INDEX(sParticipant[iParticipant].participantId)
			iPlayer = NATIVE_TO_INT(playerTemp)
			
			IF IS_NET_PLAYER_OK(playerTemp, FALSE)
				
				sPlayer[iPlayer].playerId = playerTemp
				sParticipant[iParticipant].iPlayerId = iPlayer
				sPlayer[iPlayer].iParticipantId = iParticipant
				sPlayer[iPlayer].playerPedId = GET_PLAYER_PED(playerTemp)
				sParticipant[iParticipant].tl63Name = GET_PLAYER_NAME(playerTemp)
				iTempPeakParts++
				
				IF IS_PARTICIPANT_QUALIFYING(iParticipant)
					iTempPeakQualParts++
				ENDIF
				
				SET_BIT(iPlayerOkBitset, iPlayer)
				
				IF IS_ENTITY_DEAD(sPlayer[iPlayer].playerPedId)
				OR IS_PED_INJURED(sPlayer[iPlayer].playerPedId)
				OR NOT IS_PLAYER_PLAYING(sPlayer[iPlayer].playerId)
					SET_BIT(iPlayerDeadBitset, iPlayer)
				ENDIF
				
				IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
					IF NOT IS_BIT_SET(playerBD[iParticipant].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_INITIALISED_LOCATE_DATA))
						bINITIALISED_LOCATE_DATA = FALSE
					ENDIF
					IF NOT IS_BIT_SET(playerBD[iParticipant].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_COMPLETED_REWARDS))
						bCOMPLETED_REWARDS = FALSE
					ENDIF
				ENDIF
				
				FM_EVENT_MAINTAIN_ACTIVE_PLAYER_CHECK(INT_TO_NATIVE(PLAYER_INDEX, iPlayer))
				
				IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(INT_TO_NATIVE(PLAYER_INDEX, iPlayer))
					IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
						PRINTLN("[KINGCASTLE] - calling CLEAR_PARTICIPANT_SCORE for player ", iPlayer, " because of FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION")
						CLEAR_PARTICIPANT_SCORE(iParticipant)
					ENDIF
				ENDIF
				
				IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(INT_TO_NATIVE(PLAYER_INDEX, iPlayer))
					IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
						PRINTLN("[KINGCASTLE] - calling CLEAR_PARTICIPANT_SCORE for player ", iPlayer, " because of FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE")
						CLEAR_PARTICIPANT_SCORE(iParticipant)
					ENDIF
				ENDIF
				
				IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_SCTV(INT_TO_NATIVE(PLAYER_INDEX, iPlayer))
					IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
						PRINTLN("[KINGCASTLE] - calling CLEAR_PARTICIPANT_SCORE for player ", iPlayer, " because of FM_EVENT_IS_PLAYER_RESTRICTED_WITH_SCTV")
						CLEAR_PARTICIPANT_SCORE(iParticipant)
					ENDIF
				ENDIF
				
				#IF IS_DEBUG_BUILD
				IF playerBD[iParticipant].bSPass
					IF serverBD.iSPassParticipant = -1
						serverBD.iSPassParticipant = iParticipant
						PRINTLN("[KINGCASTLE] - [DEBUG SKIPPING] - participant ", iParticipant, " has pressed S pass.")
					ENDIF
				ELIF playerBD[iParticipant].bFFail
					IF serverBD.iFFailParticipant = -1
						serverBD.iFFailParticipant = iParticipant
						PRINTLN("[KINGCASTLE] - [DEBUG SKIPPING] - participant ", iParticipant, " has pressed F fail.")
					ENDIF
				ENDIF
				#ENDIF
				
			ENDIF
			
		ELSE
			CLEAR_BIT(iParticipantActiveBitset, iParticipant)
			IF GET_KING_OF_THE_CASTLE_STATE() <= eKINGOFTHECASTLESTATE_FIGHT_OVER_CASTLE
				IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
					CLEAR_PARTICIPANT_SCORE(iParticipant)
				ENDIF
			ENDIF
			
		ENDIF
		
		REPEAT NUM_CASTLE_AREAS iCastleArea
			
			IF NOT IS_BIT_SET(serverBD.iBitset0, ENUM_TO_INT(eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS))
				IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
					CLEAR_LEADERBOARD_DATA(iParticipant, iCastleArea)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(iParticipantActiveBitset, iParticipant)
				IF IS_BIT_SET(iPlayerOkBitset, iPlayer)
					
					IF NOT IS_BIT_SET(serverBD.iBitset0, ENUM_TO_INT(eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS))
						IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
							FILL_LEADERBOARD_DATA(iParticipant, iPlayer, iCastleArea, GET_CASTLE_AREA_KING_TOTAL_POINTS(iCastleArea, iParticipant))
						ENDIF
					ENDIF
					
					IF NOT IS_BIT_SET(iPlayerDeadBitset, iPlayer)
						
						IF iParticipant = PARTICIPANT_ID_TO_INT()
							FLOAT fDis2d = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(sPlayer[iPlayer].playerPedId, GET_CASTLE_AREA_BLIP_COORDS(iCastleArea), FALSE)
							FLOAT fDis3d = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(sPlayer[iPlayer].playerPedId, GET_CASTLE_AREA_BLIP_COORDS(iCastleArea), TRUE)
							VECTOR vPartCoords = GET_ENTITY_COORDS(sPlayer[iPlayer].playerPedId)
							FLOAT fDisZ = (sCastleAreaLocal[iCastleArea].vBlipCoords.z - vPartCoords.z)
							SET_LOCAL_PARTICIPANT_2D_DISTANCE_FROM_CASTLE_AREA_CENTER_POINT(iCastleArea, fDis2d)
							SET_LOCAL_PARTICIPANT_Z_DISTANCE_FROM_CASTLE_AREA_CENTER_POINT(iCastleArea, fDisZ)
							SET_LOCAL_PARTICIPANT_3D_DISTANCE_FROM_CASTLE_AREA_CENTER_POINT(iCastleArea, fDis3d)
							REPEAT MAX_NUM_LOCATES iLocate
								vCircularLocateCentrePoint = << 0.0, 0.0, 0.0 >>
								fCircularLocateRadius = 0.0
								GET_CIRCULAR_LOCATE_DATA_FROM_SEED(iCastleArea, iLocate, vCircularLocateCentrePoint, fCircularLocateRadius)
//								PRINTLN("[KINGCASTLE] - [CIRC LOCATES] - Data - vCircularLocateCentrePoint ", iLocate, " = ", vCircularLocateCentrePoint)
//								PRINTLN("[KINGCASTLE] - [CIRC LOCATES] - Data - fCircularLocateRadius ", iLocate, " = ", fCircularLocateRadius)
								IF NOT IS_VECTOR_ZERO(vCircularLocateCentrePoint)
									FLOAT fCircDis2d = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(sPlayer[iPlayer].playerPedId, vCircularLocateCentrePoint, FALSE)
									FLOAT fCircDis3d = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(sPlayer[iPlayer].playerPedId, vCircularLocateCentrePoint, TRUE)
									FLOAT fCircDisZ = (vCircularLocateCentrePoint.z - vPartCoords.z)
									SET_LOCAL_PARTICIPANT_2D_DISTANCE_FROM_CASTLE_CIRCULAR_LOCATE_POINT(iCastleArea, iLocate, fCircDis2d)
									SET_LOCAL_PARTICIPANT_Z_DISTANCE_FROM_CASTLE_CIRCULAR_LOCATE_POINT(iCastleArea, iLocate, fCircDisZ)
									SET_LOCAL_PARTICIPANT_3D_DISTANCE_FROM_CASTLE_CIRCULAR_LOCATE_POINT(iCastleArea, iLocate, fCircDis3d)
//									PRINTLN("[KINGCASTLE] - [CIRC LOCATES] - Data - fCircDis2d ", iLocate, " = ", fCircDis2d)
//									PRINTLN("[KINGCASTLE] - [CIRC LOCATES] - Data - fCircDisZ ", iLocate, " = ", fCircDisZ)
//									PRINTLN("[KINGCASTLE] - [CIRC LOCATES] - Data - fCircDis3d ", iLocate, " = ", fCircDis3d)
								ENDIF
							ENDREPEAT
						ENDIF
						
						IF GET_DOES_PARTICIPANT_SAY_INSIDE_CASTLE_AREA(iParticipant, iCastleArea)
							
							SET_CASTLE_AREA_POPULATED(iCastleArea, TRUE)
							
							iNumParticipantsInCastleArea = GET_NUMBER_OF_PARTICIPANTS_IN_CASTLE_AREA(iCastleArea)
							iNumParticipantsInCastleArea++
							SET_NUMBER_OF_PARTICIPANTS_IN_CASTLE_AREA(iCastleArea, iNumParticipantsInCastleArea)
							
							IF GET_CASTLE_AREA_KING_PARTICIPANT_ID(iCastleArea) != iParticipant
							AND GET_CASTLE_AREA_KING_PLAYER_ID(iCastleArea) != iPlayer
								IF GET_CASTLE_AREA_HEIR_PARTICIPANT_ID(iCastleArea) = (-1)
									SET_CASTLE_AREA_HEIR_PARTICIPANT_ID(iCastleArea, iParticipant)
								ENDIF
								IF GET_CASTLE_AREA_HEIR_PLAYER_ID(iCastleArea) = (-1)
									SET_CASTLE_AREA_HEIR_PLAYER_ID(iCastleArea, iPlayer)
								ENDIF
								SET_CASTLE_AREA_HEIR_PED_ID(iCastleArea, sPlayer[iPlayer].playerPedId)
							ENDIF
							
							IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
								SET_BIT(serverBD.sCastleArea[iCastleArea].iParticipantsInAreaBitset, iParticipant)
							ENDIF
						ENDIF
						
					ENDIF
					
					// These are seperate but currently have the same conditions, in case we ever need to split them.
					IF iParticipant = PARTICIPANT_ID_TO_INT()
//						IF GET_PARTICIPANT_2D_DISTANCE_FROM_CASTLE_AREA_CENTER_POINT(iCastleArea) <= KING_OF_THE_CASTLE_FULLY_ACTIVE_PARTICIPANT_DISTANCE
//						OR GET_PARTICIPANT_Z_DISTANCE_FROM_CASTLE_AREA_CENTER_POINT(iCastleArea) <= KING_OF_THE_CASTLE_FULLY_ACTIVE_PARTICIPANT_DISTANCE
						IF NOT IS_BIT_SET(iPlayerDeadBitset, iPlayer)
							IF GET_PARTICIPANT_3D_DISTANCE_FROM_CASTLE_AREA_CENTER_POINT(iCastleArea) <= g_sMPTunables.iFREEMODE_EVENT_UI_ENTRY_DISTANCE
								IF NOT IS_BIT_SET(playerBD[iParticipant].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_WITHIN_RANGE_FOR_UI))
									SET_BIT(playerBD[iParticipant].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_WITHIN_RANGE_FOR_UI))
									PRINTLN("[KINGCASTLE] - [PARTICIPATION] - I am within ", g_sMPTunables.iFREEMODE_EVENT_UI_ENTRY_DISTANCE, "m of the castle. Setting bit eCLIENTBITSET0_I_AM_WITHIN_RANGE_FOR_UI.")
								ENDIF
							ELIF GET_PARTICIPANT_3D_DISTANCE_FROM_CASTLE_AREA_CENTER_POINT(iCastleArea) >= g_sMPTunables.iFREEMODE_EVENT_UI_EXIT_DISTANCE
								IF IS_BIT_SET(playerBD[iParticipant].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_WITHIN_RANGE_FOR_UI))
									CLEAR_BIT(playerBD[iParticipant].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_WITHIN_RANGE_FOR_UI))
									PRINTLN("[KINGCASTLE] - [PARTICIPATION] - I am within ", g_sMPTunables.iFREEMODE_EVENT_UI_EXIT_DISTANCE, "m of the castle. Clearing bit eCLIENTBITSET0_I_AM_WITHIN_RANGE_FOR_UI.")
								ENDIF
							ENDIF
							IF NOT IS_LOCAL_PARTICIPANT_RESTRICTED(TRUE)
								IF GET_PARTICIPANT_3D_DISTANCE_FROM_CASTLE_AREA_CENTER_POINT(iCastleArea) <= g_sMPTunables.iFREEMODE_EVENT_UI_ENTRY_DISTANCE
									IF NOT IS_BIT_SET(playerBD[iParticipant].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_FULLY_ACTIVE_PARTICIPANT_TRIGGERED_BY_DISTANCE))
										SET_BIT(playerBD[iParticipant].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_FULLY_ACTIVE_PARTICIPANT_TRIGGERED_BY_DISTANCE))
										PRINTLN("[KINGCASTLE] - [PARTICIPATION] - I am within ", g_sMPTunables.iFREEMODE_EVENT_UI_ENTRY_DISTANCE, "m of the castle. Setting bit eCLIENTBITSET0_I_AM_FULLY_ACTIVE_PARTICIPANT_TRIGGERED_BY_DISTANCE.")
									ENDIF
								ELIF GET_PARTICIPANT_3D_DISTANCE_FROM_CASTLE_AREA_CENTER_POINT(iCastleArea) >= g_sMPTunables.iFREEMODE_EVENT_UI_EXIT_DISTANCE
									IF IS_BIT_SET(playerBD[iParticipant].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_FULLY_ACTIVE_PARTICIPANT_TRIGGERED_BY_DISTANCE))
										CLEAR_BIT(playerBD[iParticipant].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_FULLY_ACTIVE_PARTICIPANT_TRIGGERED_BY_DISTANCE))
										PRINTLN("[KINGCASTLE] - [PARTICIPATION] - I am outwith ", g_sMPTunables.iFREEMODE_EVENT_UI_EXIT_DISTANCE, "m of the castle. Clearing bit eCLIENTBITSET0_I_AM_FULLY_ACTIVE_PARTICIPANT_TRIGGERED_BY_DISTANCE.")
									ENDIF
								ENDIF
							ELSE
								IF IS_BIT_SET(playerBD[iParticipant].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_FULLY_ACTIVE_PARTICIPANT_TRIGGERED_BY_DISTANCE))
									CLEAR_BIT(playerBD[iParticipant].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_I_AM_FULLY_ACTIVE_PARTICIPANT_TRIGGERED_BY_DISTANCE))
									PRINTLN("[KINGCASTLE] - [PARTICIPATION] - event hidden. Clearing bit eCLIENTBITSET0_I_AM_FULLY_ACTIVE_PARTICIPANT_TRIGGERED_BY_DISTANCE.")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
				
				eHudColour = HUD_COLOUR_RED
				
				IF (PLAYER_ID() != sPlayer[iPlayer].playerId)
					IF GB_ARE_PLAYERS_IN_SAME_GANG(PLAYER_ID(), sPlayer[iPlayer].playerId)
						eHudColour = GET_PLAYER_HUD_COLOUR(PLAYER_ID())
					ENDIF
				ENDIF
				
				// If this player isn't the king, make sure their blip is normal again.
				IF NOT IS_BIT_SET(iPlayerDeadBitset, iPlayer)
					IF SHOULD_HIDE_THIS_AMBIENT_EVENT()
						REMOVE_CASTLE_AREA_KING_CUSTOM_BLIP(iPlayer)
					ELIF GET_CASTLE_AREA_KING_PLAYER_ID(iCastleArea) != iPlayer
						REMOVE_CASTLE_AREA_KING_CUSTOM_BLIP(iPlayer)		
					ELIF GET_KING_OF_THE_CASTLE_STATE() != eKINGOFTHECASTLESTATE_FIGHT_OVER_CASTLE
						REMOVE_CASTLE_AREA_KING_CUSTOM_BLIP(iPlayer)
					// If they are the king add the king blip and draw marker above their head.
					ELSE
						ADD_CASTLE_AREA_KING_CUSTOM_BLIP(iPlayer)
						IF iParticipant != PARTICIPANT_ID_TO_INT()
							IF GET_IS_PARTICIPANT_FULLY_TAKING_PART(PARTICIPANT_ID_TO_INT())
								GET_HUD_COLOUR(eHudColour, iR, iG, iB, iA)
								iA = 100
								DRAW_MARKER(MARKER_ARROW, GET_ENTITY_COORDS(sPlayer[iPlayer].playerPedId)+<<0,0,1.5>>, <<0,0,0>>, <<180,0,0>>, <<0.5,0.5,0.5>>, iR, iG, iB, iA, TRUE, TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_NET_PLAYER_OK(sPlayer[iPlayer].playerId)
					IF DOES_THIS_THREAD_HAVE_CONTROL_OF_CUSTOM_BLIP_COLOUR_FOR_PLAYER(sPlayer[iPlayer].playerId)
						SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(sPlayer[iPlayer].playerId, GET_BLIP_COLOUR_FROM_HUD_COLOUR(eHudColour), SHOULD_PARTICIPANT_BLIP_BE_RED(iParticipant, iPlayer, iCastleArea))
					ENDIF
				ENDIF
				
			ENDIF
			
		ENDREPEAT
				
	ENDREPEAT
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF bINITIALISED_LOCATE_DATA
			#IF IS_DEBUG_BUILD
			IF NOT IS_BIT_SET(serverBD.iBitset0, ENUM_TO_INT(eSERVERBITSET0_ALL_PARTICIPANTS_INITIALISED_LOCATE_DATA))
				PRINTLN("[KINGCASTLE] - SET_BIT(serverBD.iBitset0, ENUM_TO_INT(eSERVERBITSET0_ALL_PARTICIPANTS_INITIALISED_LOCATE_DATA))")
			ENDIF
			#ENDIF
			SET_BIT(serverBD.iBitset0, ENUM_TO_INT(eSERVERBITSET0_ALL_PARTICIPANTS_INITIALISED_LOCATE_DATA))
		ELSE
			#IF IS_DEBUG_BUILD
			IF IS_BIT_SET(serverBD.iBitset0, ENUM_TO_INT(eSERVERBITSET0_ALL_PARTICIPANTS_INITIALISED_LOCATE_DATA))
				PRINTLN("[KINGCASTLE] - CLEAR_BIT(serverBD.iBitset0, ENUM_TO_INT(eSERVERBITSET0_ALL_PARTICIPANTS_INITIALISED_LOCATE_DATA))")
			ENDIF
			#ENDIF
			CLEAR_BIT(serverBD.iBitset0, ENUM_TO_INT(eSERVERBITSET0_ALL_PARTICIPANTS_INITIALISED_LOCATE_DATA))
		ENDIF
		IF bCOMPLETED_REWARDS
			#IF IS_DEBUG_BUILD
			IF NOT IS_BIT_SET(serverBD.iBitset0, ENUM_TO_INT(eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS))
				PRINTLN("[KINGCASTLE] - SET_BIT(serverBD.iBitset0, ENUM_TO_INT(eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS))")
			ENDIF
			#ENDIF
			SET_BIT(serverBD.iBitset0, ENUM_TO_INT(eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS))
		ELSE
			#IF IS_DEBUG_BUILD
			IF IS_BIT_SET(serverBD.iBitset0, ENUM_TO_INT(eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS))
				PRINTLN("[KINGCASTLE] - CLEAR_BIT(serverBD.iBitset0, ENUM_TO_INT(eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS))")
			ENDIF
			#ENDIF
			CLEAR_BIT(serverBD.iBitset0, ENUM_TO_INT(eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS))
		ENDIF
	ENDIF
	
	IF iTempPeakQualParts > iPeakQualifiedParticipants
		iPeakQualifiedParticipants = iTempPeakQualParts
		PRINTLN("[KINGCASTLE] - [PARTICIPATION] - iTempPeakQualParts (",iTempPeakQualParts,") is GREATER than peak qualifying participant count so far (",iPeakQualifiedParticipants,"), updating value.")
	ENDIF
	
	IF iTempPeakParts > iPeakParticipants
		PRINTLN("[KINGCASTLE] - [PARTICIPATION] - iTempPeakParts (",iTempPeakParts,") is GREATER than peak participant count so far (",iPeakParticipants,"), updating value.")
		iPeakParticipants = iTempPeakParts
	ELSE
		serverBD.iPlayersLeftInProgress = iPeakParticipants  - iTempPeakParts
	ENDIF
	
ENDPROC

PROC PROCESS_CASTLE_AREAS_LOOP()
	
	INT iKingPart = -1
	INT iKingPlayer = -1
	INT iCastleArea
	INT iLocate
	PED_INDEX pedTemp
	BOOL bResetKingData
	
	// Grab castle area data.
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_INITIALISED_LOCATE_DATA))
		REPEAT NUM_CASTLE_AREAS iCastleArea	
			IF GET_CASTLE_AREA_SEED(iCastleArea) > (-1)
				REPEAT MAX_NUM_LOCATES iLocate
					SET_CASTLE_AREA_LOCATE_MIN(iCastleArea, iLocate, GET_CASTLE_AREA_MIN_COORDS_FROM_SEED(iCastleArea, iLocate))
					SET_CASTLE_AREA_LOCATE_MAX(iCastleArea, iLocate, GET_CASTLE_AREA_MAX_COORDS_FROM_SEED(iCastleArea, iLocate))
					SET_CASTLE_AREA_BLIP_COORDS(iCastleArea, GET_CASTLE_AREA_BLIP_COORDS_FROM_SEED(iCastleArea))
					SET_CASTLE_AREA_LOCATE_WIDTH(iCastleArea, iLocate, GET_CASTLE_AREA_WIDTH_FROM_SEED(iCastleArea, iLocate))	
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_INITIALISED_LOCATE_DATA))
				ENDREPEAT
			ENDIF
		ENDREPEAT
	ENDIF
	
	REPEAT NUM_CASTLE_AREAS iCastleArea
		
		// Decide if the castle is contested.
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			IF GET_NUMBER_OF_PARTICIPANTS_IN_CASTLE_AREA(iCastleArea) <= 1
				SET_CASTLE_AREA_CONTESTED(iCastleArea, FALSE)
			ELSE
				SET_CASTLE_AREA_CONTESTED(iCastleArea, TRUE) // Set this to TRUE to turn on contested logic again.
			ENDIF
		ENDIF
		
		// If we have no king.
		IF GET_CASTLE_AREA_KING_PARTICIPANT_ID(iCastleArea) = (-1)
			
			// If the castle is not contested then set the heir as king.
			IF NOT GET_IS_CASTLE_AREA_CONTESTED(iCastleArea)
				SET_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_SET_HEIR_TO_THRONE_AS_KING))
			ENDIF				
		
		// If we do have a king.
		ELSE
			
			IF NOT GET_IS_CASTLE_AREA_POPULATED(iCastleArea)
				
				// If there is nobody in the castle, it has no king.
				bResetKingData = TRUE
				
				IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
					SET_CASTLE_AREA_CONTESTED(iCastleArea, FALSE)
				ENDIF
				
			ELSE
				
				iKingPart = GET_CASTLE_AREA_KING_PARTICIPANT_ID(iCastleArea)	
				iKingPlayer = GET_CASTLE_AREA_KING_PLAYER_ID(iCastleArea)
				
				// If the area is not contested and someone goes inside it, they become king.
				IF NOT GET_IS_CASTLE_AREA_CONTESTED(iCastleArea)
				
					IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
						IF iKingPart > (-1)
							IF NOT IS_BIT_SET(iParticipantActiveBitset, iKingPart)
								SET_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_SET_HEIR_TO_THRONE_AS_KING))
								PRINTLN("[KINGCASTLE] - [CASTLE AREA KING] - king part ", iKingPart, " iParticipantActiveBitset bit not set. Setting bit eLOCALBITSET0_SET_HEIR_TO_THRONE_AS_KING.")
							ENDIF
							IF NOT IS_BIT_SET(iPlayerOkBitset, iKingPart)
								SET_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_SET_HEIR_TO_THRONE_AS_KING))
								PRINTLN("[KINGCASTLE] - [CASTLE AREA KING] - king part ", iKingPart, " iPlayerOkBitset bit not set. Setting bit eLOCALBITSET0_SET_HEIR_TO_THRONE_AS_KING.")
							ENDIF
							IF IS_BIT_SET(iPlayerDeadBitset, iKingPart)
								SET_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_SET_HEIR_TO_THRONE_AS_KING))
								PRINTLN("[KINGCASTLE] - [CASTLE AREA KING] - king part ", iKingPart, " iPlayerDeadBitset bit is set. Setting bit eLOCALBITSET0_SET_HEIR_TO_THRONE_AS_KING.")
							ENDIF
							IF NOT GET_DOES_PARTICIPANT_SAY_INSIDE_CASTLE_AREA(iKingPart, iCastleArea)
								SET_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_SET_HEIR_TO_THRONE_AS_KING))
								PRINTLN("[KINGCASTLE] - [CASTLE AREA KING] - king part ", iKingPart, " GET_DOES_PARTICIPANT_SAY_INSIDE_CASTLE_AREA = FALSE. Setting bit eLOCALBITSET0_SET_HEIR_TO_THRONE_AS_KING.")
							ENDIF
						ENDIF
					ENDIF
					
				ELSE
					
					// If the king is dead or not active and the area is contested, reset the king data.
					IF iKingPart > (-1)
						
						IF NOT IS_BIT_SET(iParticipantActiveBitset, iKingPart)
							bResetKingData = TRUE
							PRINTLN("[KINGCASTLE] - [CASTLE AREA KING] - king part ", iKingPart, " iParticipantActiveBitset bit not set. Setting bResetKingData = TRUE.")
						ENDIF
						
						IF NOT IS_BIT_SET(iPlayerOkBitset, iKingPlayer)
							bResetKingData = TRUE
							PRINTLN("[KINGCASTLE] - [CASTLE AREA KING] - king player ", iKingPlayer, " iPlayerOkBitset bit not set. Setting bResetKingData = TRUE.")
						ENDIF
						
						IF IS_BIT_SET(iPlayerDeadBitset, iKingPlayer)
							bResetKingData = TRUE
							PRINTLN("[KINGCASTLE] - [CASTLE AREA KING] - king player ", iKingPlayer, " iPlayerDeadBitset bit is set. Setting bResetKingData = TRUE.")
						ENDIF
						
						IF NOT GET_DOES_PARTICIPANT_SAY_INSIDE_CASTLE_AREA(iKingPart, iCastleArea)
							bResetKingData = TRUE
							PRINTLN("[KINGCASTLE] - [CASTLE AREA KING] - king player ", iKingPlayer, " GET_DOES_PARTICIPANT_SAY_INSIDE_CASTLE_AREA = FALSE. Setting bResetKingData = TRUE.")
						ENDIF
						
					ENDIF
					
				ENDIF
				
			ENDIF
			
		ENDIF
		
		IF bResetKingData
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				SET_CASTLE_AREA_KING_PARTICIPANT_ID(iCastleArea, -1)
				SET_CASTLE_AREA_KING_PLAYER_ID(iCastleArea, -1)
			ENDIF
			SET_CASTLE_AREA_KING_PED_ID(iCastleArea, pedTemp)
		ENDIF
		
		IF IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_SET_HEIR_TO_THRONE_AS_KING))
			IF GET_CASTLE_AREA_HEIR_PARTICIPANT_ID(iCastleArea) > (-1)
			AND GET_CASTLE_AREA_HEIR_PLAYER_ID(iCastleArea) > (-1)
				IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
					SET_CASTLE_AREA_KING_PARTICIPANT_ID(iCastleArea, GET_CASTLE_AREA_HEIR_PARTICIPANT_ID(iCastleArea))
					SET_CASTLE_AREA_KING_PLAYER_ID(iCastleArea, GET_CASTLE_AREA_HEIR_PLAYER_ID(iCastleArea))
					SET_CASTLE_AREA_KING_PED_ID(iCastleArea, GET_CASTLE_AREA_HEIR_PED_ID(iCastleArea))
//					IF serverBD.sCastleArea[iCastleArea].iPartFirstTimeAsKingBitset != 0
//						SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
//						TickerEventData.TickerEvent = TICKER_EVENT_BECAME_KING
//						INT playerIdTicker = GET_CASTLE_AREA_HEIR_PLAYER_ID(iCastleArea)
//						TickerEventData.playerID = INT_TO_NATIVE(PLAYER_INDEX, playerIdTicker)
//						REPEAT NUM_NETWORK_PLAYERS iTickerParticipantCount
//							IF IS_BIT_SET(iParticipantActiveBitset, iTickerParticipantCount)
//								IF IS_BIT_SET(iPlayerOkBitset, sParticipant[iTickerParticipantCount].iPlayerId)
//									IF GET_IS_PARTICIPANT_FULLY_TAKING_PART(iTickerParticipantCount)
//										IF NOT IS_BIT_SET(playerBD[iTickerParticipantCount].iBitset0, ENUM_TO_INT(eCLIENTBITSET0_AMBIENT_EVENT_SHOULD_BE_HIDDEN_NO_PASSIVE_CHECK))
	//										BROADCAST_TICKER_EVENT(TickerEventData, SPECIFIC_PLAYER(INT_TO_NATIVE(PLAYER_INDEX, sParticipant[iTickerParticipantCount].iPlayerId)))
	//										PRINTLN("[KINGCASTLE] - [TICKERS] - broadcast ticker event TICKER_EVENT_BECAME_KING to fully active participant ", iTickerParticipantCount)
	//								
	//									ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDREPEAT
//					ENDIF
					IF NOT IS_BIT_SET(serverBD.sCastleArea[iCastleArea].iPartFirstTimeAsKingBitset, GET_CASTLE_AREA_HEIR_PARTICIPANT_ID(iCastleArea))
						SET_BIT(serverBD.sCastleArea[iCastleArea].iPartFirstTimeAsKingBitset, GET_CASTLE_AREA_HEIR_PARTICIPANT_ID(iCastleArea))
						#IF IS_DEBUG_BUILD
						INT iPartTemp
						iPartTemp = GET_CASTLE_AREA_HEIR_PARTICIPANT_ID(iCastleArea)
						PRINTLN("[KINGCASTLE] - [CASTLE AREA KING] - set bit iPartFirstTimeAsKingBitset, ", iPartTemp)
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			SORT_LEADERBOARD_DATA(iCastleArea)
		ENDIF
		
	ENDREPEAT
	
ENDPROC

PROC PRE_PROCESS_STAGGERED_PARTICIPANT_LOOP()
	
	IF (iStaggeredPartLoopCount = 0)
		
	ENDIF
	
ENDPROC

PROC POST_PROCESS_STAGGERED_PARTICIPANT_LOOP()
	
	IF iStaggeredPartLoopCount >= (NETWORK_GET_MAX_NUM_PARTICIPANTS() - 1)
		
		iStaggeredPartLoopCount = 0
		
	ENDIF
	
ENDPROC

PROC PROCESS_STAGGERED_PARTICIPANT_LOOP()
	
	PRE_PROCESS_STAGGERED_PARTICIPANT_LOOP()
	
	iStaggeredPartLoopCount++
	
	POST_PROCESS_STAGGERED_PARTICIPANT_LOOP()
	
ENDPROC

#IF IS_DEBUG_BUILD

TEXT_WIDGET_ID twIdServerGameState
TEXT_WIDGET_ID twIdClientGameState[NUM_NETWORK_PLAYERS]
TEXT_WIDGET_ID twIdClientName[NUM_NETWORK_PLAYERS]

PROC UPDATE_SERVER_GAME_STATE_WIDGET()
	SET_CONTENTS_OF_TEXT_WIDGET(twIdServerGameState, GET_GAME_STATE_NAME(GET_SERVER_GAME_STATE()))
ENDPROC

PROC UPDATE_CLIENT_GAME_STATE_WIDGET(INT iParticipant)
	
	STRING strName
	
	SET_CONTENTS_OF_TEXT_WIDGET(twIdClientGameState[iParticipant], "NOT ACTIVE")
	SET_CONTENTS_OF_TEXT_WIDGET(twIdClientName[iParticipant], "NOT ACTIVE")
	
	IF IS_BIT_SET(iParticipantActiveBitset, iParticipant)
		IF IS_BIT_SET(iPlayerOkBitset, sParticipant[iParticipant].iPlayerId)
			SET_CONTENTS_OF_TEXT_WIDGET(twIdClientGameState[iParticipant], GET_GAME_STATE_NAME(GET_CLIENT_GAME_STATE(iParticipant)))
			strName = GET_PLAYER_NAME(INT_TO_NATIVE(PLAYER_INDEX, sParticipant[iParticipant].iPlayerId))
			SET_CONTENTS_OF_TEXT_WIDGET(twIdClientName[iParticipant], strName)
		ENDIF
	ENDIF
	
ENDPROC

PROC UPDATE_WIDGETS()
	
	INT iArea
	
	UPDATE_SERVER_GAME_STATE_WIDGET()
	UPDATE_CLIENT_GAME_STATE_WIDGET(iStaggeredPartLoopCount)
	
	REPEAT NUM_CASTLE_AREAS iArea
		SET_CONTENTS_OF_TEXT_WIDGET(twIdCastleAreaState[iArea], GET_CASTLE_AREA_STATE_NAME(GET_CASTLE_AREA_STATE(iArea)))
	ENDREPEAT
	
	iStaggeredPartLoopCountWidgets++
	
	IF iStaggeredPartLoopCountWidgets >= (NETWORK_GET_MAX_NUM_PARTICIPANTS() - 1)
		iStaggeredPartLoopCountWidgets = 0
	ENDIF
	
ENDPROC

/// PURPOSE:
/// Create mode widgets
PROC MAINTAIN_WIDGETS()

	INT iArea
	INT iLocate
	INT iParticipant
	TEXT_LABEL_63 tl63Temp
	
	SWITCH iWidgetsStage
		CASE 0
			kingOfTheCastleWidgetsId = START_WIDGET_GROUP("King of the Castle")
			ADD_WIDGET_BOOL("Create King of the Castle Widgets", bCreateWidgets)
			STOP_WIDGET_GROUP()
			
			iWidgetsStage++
		BREAK
		
		CASE 1
			IF bCreateWidgets
				DELETE_WIDGET_GROUP(kingOfTheCastleWidgetsId)
				
				kingOfTheCastleWidgetsId = START_WIDGET_GROUP("King of the Castle")
					
					ADD_WIDGET_INT_READ_ONLY("Peak Qual Parts", iPeakQualifiedParticipants)
					ADD_WIDGET_BOOL("Fake Hide Event No Passive Check", bFakeHideEventNotPassiveCheck)
					ADD_WIDGET_BOOL("Fake Hide Event with Passive Check", bFakeHideEventWithPassiveCheck)
					ADD_WIDGET_BOOL("Force Castle Contested", bForceCastleAreaContested)
					ADD_WIDGET_BOOL("Force Not Placed Winner", bForceNotPlacedWinner)
					
					START_WIDGET_GROUP("Server Only Gameplay") 
						ADD_WIDGET_BOOL("End mission", bHostEndMissionNow)	
						ADD_WIDGET_BOOL("Skip Time To End", bSkipToTimeEnd)
						ADD_WIDGET_INT_READ_ONLY("S Pass Part", serverBD.iSPassParticipant)
						ADD_WIDGET_INT_READ_ONLY("F Fail Part", serverBD.iFFailParticipant)
					STOP_WIDGET_GROUP()	
					
					START_WIDGET_GROUP("Server Bitsets")
						ADD_BIT_FIELD_WIDGET("Bitset 0", serverBD.iBitset0)
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("Participants/Players")
						ADD_BIT_FIELD_WIDGET("Players Ok", iPlayerOkBitset)
						ADD_BIT_FIELD_WIDGET("Players Dead", iPlayerDeadBitset)
						ADD_BIT_FIELD_WIDGET("Participants Active", iParticipantActiveBitset)
					STOP_WIDGET_GROUP()
					
					REPEAT NUM_CASTLE_AREAS iArea
						tl63Temp = ""
						tl63Temp += "Castle Area "
						tl63Temp += iArea
						START_WIDGET_GROUP(tl63Temp)
							tl63Temp = ""
							tl63Temp += "Local Data"
							START_WIDGET_GROUP(tl63Temp)
								REPEAT MAX_NUM_LOCATES iLocate
									tl63Temp = ""
									tl63Temp += "Locate "
									tl63Temp += iLocate
									tl63Temp += " Min"
									ADD_WIDGET_VECTOR_SLIDER(tl63Temp, sCastleAreaLocal[iArea].vLocateMin[iLocate], -100000.0, 100000.0, 0.1)
									tl63Temp = ""
									tl63Temp += "Locate "
									tl63Temp += iLocate
									tl63Temp += " Max"
									ADD_WIDGET_VECTOR_SLIDER(tl63Temp, sCastleAreaLocal[iArea].vLocateMax[iLocate], -100000.0, 100000.0, 0.1)
									tl63Temp = ""
									tl63Temp += "Locate "
									tl63Temp += iLocate
									tl63Temp += " Width"
									ADD_WIDGET_FLOAT_READ_ONLY(tl63Temp, sCastleAreaLocal[iArea].fLocateWidth[iLocate])
								ENDREPEAT
								tl63Temp = ""
								tl63Temp += "Blip Coords"
								ADD_WIDGET_VECTOR_SLIDER(tl63Temp, sCastleAreaLocal[iArea].vBlipCoords, -100000.0, 100000.0, 0.1)
								ADD_WIDGET_INT_READ_ONLY("Throne Heir Part", sCastleAreaLocal[iArea].iHeirToThroneParticipant)
								ADD_WIDGET_INT_READ_ONLY("Throne Heir Player", sCastleAreaLocal[iArea].iHeirToThronePlayer)
								ADD_WIDGET_FLOAT_READ_ONLY("Local part 2D dis from area", sCastleAreaLocal[iArea].fDistanceFromCastleAreaCenterPoint2d)
								ADD_WIDGET_FLOAT_READ_ONLY("Local part Z dis from area", sCastleAreaLocal[iArea].fDistanceFromCastleAreaCenterPointZ)
								ADD_WIDGET_FLOAT_READ_ONLY("Local part 3D dis from area", sCastleAreaLocal[iArea].fDistanceFromCastleAreaCenterPoint3d)
								REPEAT MAX_NUM_LOCATES iLocate
									tl63Temp = ""
									tl63Temp += "Circ Area "
									tl63Temp += iLocate
									tl63Temp += "Dis 2D"
									ADD_WIDGET_FLOAT_READ_ONLY(tl63Temp, sCastleAreaLocal[iArea].fDistanceFromCastleAreaCircularLocate2d[iLocate])
									tl63Temp = ""
									tl63Temp += "Circ Area "
									tl63Temp += iLocate
									tl63Temp += "Dis Z"
									ADD_WIDGET_FLOAT_READ_ONLY(tl63Temp, sCastleAreaLocal[iArea].fDistanceFromCastleCircularLocateZ[iLocate])
									tl63Temp = ""
									tl63Temp += "Circ Area "
									tl63Temp += iLocate
									tl63Temp += "Dis 3D"
									ADD_WIDGET_FLOAT_READ_ONLY(tl63Temp, sCastleAreaLocal[iArea].fDistanceFromCastleCircularLocate3d[iLocate])
								ENDREPEAT
							STOP_WIDGET_GROUP()
							tl63Temp = ""
							tl63Temp += "Client Data"
							START_WIDGET_GROUP(tl63Temp)
								tl63Temp = ""
								tl63Temp += "Local Player Says In Area"
								ADD_WIDGET_BOOL(tl63Temp, playerBD[PARTICIPANT_ID_TO_INT()].sCastleArea[iArea].bIAmInCastleArea)
							STOP_WIDGET_GROUP()
							tl63Temp = ""
							tl63Temp += "Server Data"
							START_WIDGET_GROUP(tl63Temp)
								tl63Temp = ""
								tl63Temp += "Area State"
								twIdCastleAreaState[iArea] = ADD_TEXT_WIDGET(tl63Temp)
								tl63Temp = ""
								tl63Temp += "Area Contested"
								ADD_WIDGET_BOOL(tl63Temp, serverBD.sCastleArea[iArea].bAreaContested)
								tl63Temp = ""
								tl63Temp += "Participants in Area"
								ADD_BIT_FIELD_WIDGET(tl63Temp, serverBD.sCastleArea[iArea].iParticipantsInAreaBitset)
								tl63Temp = ""
								tl63Temp += "Area King Part"
								ADD_WIDGET_INT_READ_ONLY(tl63Temp, serverBD.sCastleArea[iArea].iKingPart)
								tl63Temp = ""
								tl63Temp += "Area King Player"
								ADD_WIDGET_INT_READ_ONLY(tl63Temp, serverBD.sCastleArea[iArea].iKingPlayer)
								tl63Temp = ""
								tl63Temp += "Area Seed"
								ADD_WIDGET_INT_READ_ONLY(tl63Temp, serverBD.sCastleArea[iArea].iCastleAreaSeed)
								ADD_WIDGET_BOOL("End For Lack of Participants", serverBD.sCastleArea[iArea].bForceEndForNotEnoughParticipants)
								REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
									tl63Temp = ""
									tl63Temp += "Participant "
									tl63Temp += iParticipant
									tl63Temp += " King Points"
									START_WIDGET_GROUP(tl63Temp)
										tl63Temp = ""
										tl63Temp += "King Area Points"
										ADD_WIDGET_FLOAT_READ_ONLY(tl63Temp, serverBD.sCastleArea[iArea].fKingAreaPoints[iParticipant])
										tl63Temp = ""
										tl63Temp += "King Kill Points"
										ADD_WIDGET_FLOAT_READ_ONLY(tl63Temp, serverBD.sCastleArea[iArea].fKingKillPoints[iParticipant])
									STOP_WIDGET_GROUP()
								ENDREPEAT
							STOP_WIDGET_GROUP()
						STOP_WIDGET_GROUP()
					ENDREPEAT
					
					tl63Temp = ""
					tl63Temp += "Castle Areas Common Client Data"
					START_WIDGET_GROUP(tl63Temp)
						tl63Temp = ""
						tl63Temp += "Castle Areas Local Part Is In"
						ADD_BIT_FIELD_WIDGET(tl63Temp, playerBD[PARTICIPANT_ID_TO_INT()].sCastleAreasCommonData.iCastleAreasIAmInBitset)
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("Game State")
						START_WIDGET_GROUP("Server")
							twIdServerGameState = ADD_TEXT_WIDGET("Game State")
							UPDATE_SERVER_GAME_STATE_WIDGET()
						STOP_WIDGET_GROUP()
						START_WIDGET_GROUP("Client")
							REPEAT NUM_NETWORK_PLAYERS iParticipant
								tl63Temp = ""
								tl63Temp +="Part "
								tl63Temp += iParticipant
								tl63Temp += " Name"
								twIdClientName[iParticipant] = ADD_TEXT_WIDGET(tl63Temp)
								tl63Temp = ""
								tl63Temp += "Part "
								tl63Temp += iParticipant
								tl63Temp += " Game State"
								twIdClientGameState[iParticipant] = ADD_TEXT_WIDGET(tl63Temp)
								tl63Temp = ""
								tl63Temp +="Part "
								tl63Temp += iParticipant
								tl63Temp += " Player ID"
								ADD_WIDGET_INT_READ_ONLY(tl63Temp, sParticipant[iParticipant].iPlayerId)
								UPDATE_CLIENT_GAME_STATE_WIDGET(iParticipant)
								tl63Temp = ""
								tl63Temp +="Part "
								tl63Temp += iParticipant
								tl63Temp += " S Pass"
								ADD_WIDGET_BOOL(tl63Temp, playerBD[iParticipant].bSPass)
								tl63Temp = ""
								tl63Temp +="Part "
								tl63Temp += iParticipant
								tl63Temp += " F Fail"
								ADD_WIDGET_BOOL(tl63Temp, playerBD[iParticipant].bFFail)
								ADD_BIT_FIELD_WIDGET("Bitset 0", playerBD[iParticipant].iBitset0)
							ENDREPEAT
						STOP_WIDGET_GROUP()
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("Scoring")
						ADD_WIDGET_BOOL("Print sorting debug", bPrintLeaderboardSortingDebug)
						ADD_WIDGET_BOOL("Print bottom right debug", bDoBottomRightPrints)
						REPEAT NUM_CASTLE_AREAS iArea
							START_WIDGET_GROUP("Leaderboard")
								REPEAT NUM_NETWORK_PLAYERS iParticipant
									tl63Temp = "Position "
									tl63Temp += iParticipant
									START_WIDGET_GROUP(tl63Temp)
										tl63Temp = ""
										tl63Temp +="Participant"
										ADD_WIDGET_INT_READ_ONLY(tl63Temp, serverBD.sCastleArea[iArea].sLeaderboardData[iParticipant].iParticipant)
										tl63Temp = ""
										tl63Temp +="Player"
										ADD_WIDGET_INT_READ_ONLY(tl63Temp, serverBD.sCastleArea[iArea].sLeaderboardData[iParticipant].iPlayer)
										tl63Temp = ""
										tl63Temp +="Score"
										ADD_WIDGET_FLOAT_READ_ONLY(tl63Temp, serverBD.sCastleArea[iArea].sLeaderboardData[iParticipant].fTotalKingPoints)
									STOP_WIDGET_GROUP()
								ENDREPEAT
							STOP_WIDGET_GROUP()
							START_WIDGET_GROUP("Bottom Right HUD")
								REPEAT NUM_LEADERBOARD_PLAYERS iParticipant
									tl63Temp = ""
									tl63Temp +="BR Participant"
									ADD_WIDGET_INT_READ_ONLY(tl63Temp, sCastleAreaLocal[iArea].sBottomRightData[iParticipant].iParticipant)
									tl63Temp = ""
									tl63Temp +="BR Participant"
									ADD_WIDGET_INT_READ_ONLY(tl63Temp, sCastleAreaLocal[iArea].sBottomRightData[iParticipant].iPlayer)
									tl63Temp = ""
									tl63Temp +="BR Participant"
									ADD_WIDGET_FLOAT_READ_ONLY(tl63Temp, sCastleAreaLocal[iArea].sBottomRightData[iParticipant].fTotalKingPoints)
								ENDREPEAT
							STOP_WIDGET_GROUP()
						ENDREPEAT
					STOP_WIDGET_GROUP()
					
				STOP_WIDGET_GROUP()
				
				PRINTLN("[KINGCASTLE] - created widgets.")
				
				iWidgetsStage++
				
			ENDIF
		BREAK
		
		CASE 2
			UPDATE_WIDGETS()
		BREAK
			
	ENDSWITCH
	
ENDPROC

#ENDIF

SCRIPT(MP_MISSION_DATA missionScriptArgs)
	
	
	INT iCastleArea
	PED_INDEX pedTemp
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
		
		#IF IS_DEBUG_BUILD	
			PRINTLN("MP: Starting mission.")
			PRINTLN("[KINGCASTLE] - SCRIPT LAUNCHED.")
		#ENDIF
		
		// Vehry out all the initial game starting duties. 
		PROCESS_PRE_GAME(missionScriptArgs)	
	ELSE
		
		SCRIPT_CLEANUP()
		
	ENDIF
	
	// Main loop
	WHILE TRUE

		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE() // If we have a match end event, bail
			PRINTLN("[KINGCASTLE] - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE.")
			SCRIPT_CLEANUP()
		ENDIF
		IF SHOULD_MP_AMBIENT_SCRIPT_TERMINATE(MPAM_TYPE_KING_OF_THE_CASTLE)
			PRINTLN("[KINGCASTLE] - SHOULD_MP_AMBIENT_SCRIPT_TERMINATE = TRUE.")
			SCRIPT_CLEANUP()
		ENDIF
		IF HAS_MODE_TIMER_DIFF_WITH_CURRENT_TIME_GONE_WRONG()
			PRINTLN("[KINGCASTLE] - HAS_MODE_TIMER_DIFF_WITH_CURRENT_TIME_GONE_WRONG = TRUE.")
			SCRIPT_ASSERT("[KINGCASTLE] - HAS_MODE_TIMER_DIFF_WITH_CURRENT_TIME_GONE_WRONG = TRUE.")
			SCRIPT_CLEANUP()
		ENDIF
		IF IS_KILL_CURRENT_FREEMODE_EVENT_GLOBAL_SET()
			PRINTLN("[KINGCASTLE] - IS_KILL_CURRENT_FREEMODE_EVENT_GLOBAL_SET = TRUE.")
			SCRIPT_CLEANUP()
		ENDIF
		
		// Save out the current focus participant. Stays at -1 if not the local player or not spectating a script participant.
		iFocusParticipant = (-1)
		
		IF IS_BIT_SET(iPlayerOkBitset, NATIVE_TO_INT(PLAYER_ID()))
			IF IS_BIT_SET(iParticipantActiveBitset, PARTICIPANT_ID_TO_INT())
				IF NOT IS_PLAYER_SPECTATING(PLAYER_ID())
					iFocusParticipant = PARTICIPANT_ID_TO_INT()
				ELSE
					IF IS_A_SPECTATOR_CAM_RUNNING()
					AND NOT IS_SPECTATOR_HUD_HIDDEN()
						PED_INDEX specTargetPed = GET_SPECTATOR_SELECTED_PED()
						IF IS_PED_A_PLAYER(specTargetPed)
							PLAYER_INDEX specPlayerTemp = NETWORK_GET_PLAYER_INDEX_FROM_PED(specTargetPed)
							IF NETWORK_IS_PLAYER_A_PARTICIPANT(specPlayerTemp)
								PARTICIPANT_INDEX specTargetParticipant = NETWORK_GET_PARTICIPANT_INDEX(specPlayerTemp)
								iFocusParticipant = NATIVE_TO_INT(specTargetParticipant)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Call the common FM Events every frame callse
		FM_EVENT_COMMON_EVERY_FRAME_CALLS()
		
//		IF NOT IS_PLAYER_IN_CORONA() //Check if player is in a tutorial session and end
//			IF NETWORK_IS_IN_TUTORIAL_SESSION()
//				PRINTLN("[KINGCASTLE] - NETWORK_IS_IN_TUTORIAL_SESSION = TRUE.")
//				SCRIPT_CLEANUP()
//			ENDIF
//		ENDIF
//		IF IS_ANY_TRANSITION_SESSION_INVITE_BEING_ACTIONED()
//			PRINTLN("[KINGCASTLE] - IS_ANY_TRANSITION_SESSION_INVITE_BEING_ACTIONED = TRUE.")
//			SCRIPT_CLEANUP()
//		ENDIF
//		IF AM_I_STARTING_TRANSITION_SESSIONS_QUICK_MATCH()
//			PRINTLN("[KINGCASTLE] - AM_I_STARTING_TRANSITION_SESSIONS_QUICK_MATCH = TRUE.")
//			SCRIPT_CLEANUP()
//		ENDIF
//		IF g_sJoblistWarpMP.jlwIsActive
//			PRINTLN("[KINGCASTLE] - g_sJoblistWarpMP.jlwIsActive = TRUE.")
//			SCRIPT_CLEANUP()
//		ENDIF
		IF GET_KING_OF_THE_CASTLE_STATE() < eKINGOFTHECASTLESTATE_TIME_UP // Only do this if we have not completed the mode. Once we have an end reason players will start leaving the script, which could wrongly activate this. 
			IF SHOULD_BAIL_FM_EVENT_DUE_TO_LOW_SESSION_NUMBERS() // Clean up this script if we go below minimum players in current session. Default to 2 for now.
			#IF IS_DEBUG_BUILD
			OR serverBD.sCastleArea[0].bForceEndForNotEnoughParticipants
			#ENDIF
				SET_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_LEAVING_FOR_LOW_SESSION_NUMBERS))
				PRINTLN("[KINGCASTLE] - Set bit: eLOCALBITSET0_LEAVING_FOR_LOW_SESSION_NUMBERS")
				GIVE_REWARDS(0, -1) // Give participation rewards for those that should get them.
				PRINTLN("[KINGCASTLE] - SHOULD_BAIL_FM_EVENT_DUE_TO_LOW_SESSION_NUMBERS = TRUE")
				SCRIPT_CLEANUP()
			ENDIF
		ENDIF
		
		REPEAT NUM_CASTLE_AREAS iCastleArea
			SET_CASTLE_AREA_HEIR_PARTICIPANT_ID(iCastleArea, -1)
			SET_CASTLE_AREA_HEIR_PLAYER_ID(iCastleArea, -1)
			SET_CASTLE_AREA_HEIR_PED_ID(iCastleArea, pedTemp)
			CLEAR_BIT(iLocalBitset0, ENUM_TO_INT(eLOCALBITSET0_SET_HEIR_TO_THRONE_AS_KING))
		ENDREPEAT
		
		// Process events.
		PROCESS_EVENTS()
		
		// Loops.
		PROCESS_PARTICIPANT_LOOP()
		PROCESS_STAGGERED_PARTICIPANT_LOOP()
		PROCESS_CASTLE_AREAS_LOOP()
		
		// Deal with the debug
		#IF IS_DEBUG_BUILD		
			MAINTAIN_WIDGETS()
		#ENDIF		
		
		// -----------------------------------
		// Process your game logic.....
		SWITCH GET_CLIENT_GAME_STATE(PARTICIPANT_ID_TO_INT())
			
			// Wait untill the server gives the all go before moving on
			CASE eGAMESTATE_INI
				IF GET_SERVER_GAME_STATE() = eGAMESTATE_RUNNING
					FM_EVENT_BLOCK_SPECIFIC_GANG_ATTACKS(FMMC_TYPE_KING_CASTLE, GET_CASTLE_AREA_SEED(0))
					SET_CLIENT_GAME_STATE(eGAMESTATE_RUNNING)
				ELIF GET_SERVER_GAME_STATE() = eGAMESTATE_END
					SET_CLIENT_GAME_STATE(eGAMESTATE_END)
				ENDIF
			BREAK
			
			// Main gameplay state.
			CASE eGAMESTATE_RUNNING
				IF GET_SERVER_GAME_STATE() = eGAMESTATE_RUNNING
					
					PROCESS_KING_OF_THE_CASTLE_CLIENT()
					
				ELIF GET_SERVER_GAME_STATE() = eGAMESTATE_END
					SET_MISSION_FINISHED(serverBD.MissionTerminateDelayTimer)
					SET_CLIENT_GAME_STATE(eGAMESTATE_DELAY_TERMINATION)
				ENDIF
			BREAK
			
			CASE eGAMESTATE_DELAY_TERMINATION
				BOOL bClean
				IF HAS_NET_TIMER_STARTED(serverBD.MissionTerminateDelayTimer)
					IF IS_MISSION_READY_TO_CLEANUP(serverBD.MissionTerminateDelayTimer)
						bClean = TRUE
					ENDIF
				ELSE
					bClean = TRUE
				ENDIF
		      	IF bClean
		            SET_CLIENT_GAME_STATE(eGAMESTATE_END)
		      	ENDIF
			BREAK
			
			// The game stage the local player is placed when we want him to leave a mission
			CASE eGAMESTATE_LEAVE
				SET_CLIENT_GAME_STATE(eGAMESTATE_END)
			FALLTHRU
			
			//Cleans up then terminates the mission
			CASE eGAMESTATE_END
				SCRIPT_CLEANUP()
			BREAK

		ENDSWITCH
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			
			SWITCH GET_SERVER_GAME_STATE()
				
				// For CnC group missions, we may want to wait for all players to have launched the script
				CASE eGAMESTATE_INI
					SET_SERVER_GAME_STATE(eGAMESTATE_RUNNING)
				BREAK
				
				// Look for game end conditions
				CASE eGAMESTATE_RUNNING
					
					PROCESS_KING_OF_THE_CASTLE_SERVER()
					
					#IF IS_DEBUG_BUILD
						IF bHostEndMissionNow
							PRINTLN("[KINGCASTLE] - bHostEndMissionNow = TRUE.")
							SET_SERVER_GAME_STATE(eGAMESTATE_END)
						ENDIF
						IF MPGlobalsAmbience.bKillActiveEvent
							PRINTLN("[KINGCASTLE] - MPGlobalsAmbience.bKillActiveEvent = TRUE.")
							MPGlobalsAmbience.bKillActiveEvent = FALSE
							SET_SERVER_GAME_STATE(eGAMESTATE_END)
						ENDIF
					#ENDIF	
					
				BREAK
				
				CASE eGAMESTATE_END
				BREAK
				
			ENDSWITCH
			
		ENDIF
		
	ENDWHILE
	
	
ENDSCRIPT
