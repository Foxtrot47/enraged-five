//////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_CRIMINAL_DAMAGE.sc														//
// Description: Players try to cause the most destruction in freemode					//
// Written by:  Martin McMillan															//
// Date: 21/05/2015																		//
//////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "net_mission.sch"


//----------------------
//	INCLUDES
//----------------------
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "flow_public_core_override.sch"
USING "net_wait_zero.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"
USING "net_ambience_launcher.sch"
USING "script_conversion.sch"
USING "DM_Leaderboard.sch"
USING "net_challenges.sch"
USING "am_common_ui.sch"
USING "freemode_events_header.sch"
USING "net_rank_ui.sch"
USING "clothes_shop_private.sch"
//----------------------
//	ENUM
//----------------------

//----------------------
//	CONSTANTS
//----------------------

CONST_INT TOTAL_CHALLENGES				12

CONST_INT RESEND_EVENT_DELAY			250
CONST_INT SERVER_PROCESS_WINNER_DELAY	750
CONST_INT SERVER_TERMINATE_DELAY		1000
CONST_INT CH_REWARD_TIME				20000

CONST_INT NUM_LEADERBOARD_PLAYERS		NUM_NETWORK_PLAYERS

CONST_INT BROADCAST_DELAY_TIME			1000 

CONST_INT NUM_MINI_LEADERBOARD_PLAYERS	3

//----------------------
//	Local Bitset
//----------------------

CONST_INT AMBC_BS_RESET_TRACKING			0
CONST_INT AMBC_BS_STARTED_PERFORMING		1
CONST_INT AMBC_BS_SHOWN_START_HELP			2
CONST_INT AMBC_BS_DONE_TIDYUP				3
CONST_INT AMBC_BS_CALCULATED_PLAYER_COST	4
CONST_INT AMBC_BS_CHANGING_CLOTHES			5
CONST_INT AMBC_BS_DONE_PV_HELP				6
CONST_INT AMBC_BS_LOCAL_PLAYER_FIRST		7

//----------------------
//	STRUCT
//----------------------

STRUCT CLIENT_CHALLENGE_DATA
	
	INT iChallengeInt
	INT iChallengeInitialInt
	INT iPerformingTime
	
ENDSTRUCT




//----------------------
//	VARIABLES
//----------------------

SCRIPT_TIMER iServerTerminateTimer

INT iLocalBestAttempt

INT iLocalBitset
INT iClientPart

STRUCT_FM_EVENTS_END_UI sEndUiVars

INT iTotalCashDamage

INT iStartTime 

INT iHelpBS

INT iTickerWinner = -1
INT iTickerParticipant = -1

//----------------------
//	GAME STATE
//----------------------

CONST_INT GAME_STATE_INIT 									0
CONST_INT GAME_STATE_RUNNING								1
CONST_INT GAME_STATE_TERMINATE_DELAY						2
CONST_INT GAME_STATE_END									3

ENUM AMBC_RUN_STAGE
	AMBC_WARN = 0,
	AMBC_WAIT,
	AMBC_START,
	AMBC_CHALLENGE,
	AMBC_REWARDS,
	AMBC_END
ENDENUM


//----------------------
//	BROADCAST VARIABLES
//----------------------

// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData

	INT iServerGameState = GAME_STATE_INIT
	
	INT iServerBitSet
	
	INT iEndTime
	
	AM_FREEMODE_CHALLENGES challenge
	
	INT iWinnerPart[NUM_LEADERBOARD_PLAYERS] //Store the top 3
		
	AMBC_RUN_STAGE eAMBC_Stage = AMBC_WARN
	
	TIME_DATATYPE StartTime
	
	SCRIPT_TIMER eventTimer
	SCRIPT_TIMER rewardTimer
	SCRIPT_TIMER warningTimer
	
	INT iActivePartBS
	
	INT iParticipatedBS
	INT iTotalParticipants
	INT iPeakParticipants
	
	INT iHashedMac
	INT iMatchHistoryId
	INT iPlayersLeftInProgress 
	INT iPeakQualifiers
	
ENDSTRUCT

CONST_INT SERVER_BS_LOW_PARTICIPANTS	0

ServerBroadcastData serverBD
CHALLENGE_DPAD_STRUCT chDPStruct

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	
	INT iClientGameState = GAME_STATE_INIT
	
	INT iClientBitSet
	
	AMBC_RUN_STAGE eAMBC_Stage = AMBC_WARN
	
	CLIENT_CHALLENGE_DATA clientChallengeData
	
	INT iPlayerClothingCost
	
ENDSTRUCT

CONST_INT CLIENT_BS_REWARD_GIVEN			0
CONST_INT CLIENT_BS_ENDING_SHOWN			1
CONST_INT CLIENT_BS_PARTICIPATED_PERMANENT	2

PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

scrFmEventAmbientMission telemetryStruct

//----------------------
//	DBG FUNCTIONS
//----------------------

#IF IS_DEBUG_BUILD

BOOL bEndScript
BOOL bEndScriptLocal
BOOL bExtraPrints
BOOL bSkipEventTime

PROC CREATE_WIDGETS()

	START_WIDGET_GROUP("Criminal Damage")
		START_WIDGET_GROUP("Server Only")
			ADD_WIDGET_BOOL("bSkipEventTime",bSkipEventTime)
			ADD_WIDGET_BOOL("End Script",bEndScript)
		STOP_WIDGET_GROUP()
		ADD_WIDGET_BOOL("End Script Locally",bEndScriptLocal)
		ADD_WIDGET_BOOL("Extra prints",bExtraPrints)
	STOP_WIDGET_GROUP()
	
	CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE DEBUG ===  CREATED WIDGETS ")
	
ENDPROC

#ENDIF

//----------------------
//	FUNCTIONS
//----------------------

/// PURPOSE:
///		1000 - Cash reward default.
FUNC INT GET_EOM_DEFAULT_CASH_REWARD()
	RETURN g_sMPTunables.iCRIMINAL_DAMAGE_GET_EOM_DEFAULT_CASH_REWARD
ENDFUNC

/// PURPOSE:
///		100 - RP reward default.
FUNC INT GET_EOM_DEFAULT_RP_REWARD()
	RETURN g_sMPTunables.iCRIMINAL_DAMAGE_GET_EOM_DEFAULT_RP_REWARD
ENDFUNC

/// PURPOSE:
/// 	3000 - Cash reward base.
FUNC INT GET_CASH_REWARD_BASE()
	RETURN g_sMPTunables.iCRIMINAL_DAMAGE_GET_CASH_REWARD_BASE
ENDFUNC

/// PURPOSE:
/// 	200 - RP reward base.
FUNC INT GET_RP_REWARD_BASE()
	RETURN g_sMPTunables.iCRIMINAL_DAMAGE_GET_RP_REWARD_BASE
ENDFUNC

/// PURPOSE:
/// 	0.01 - Cash reward percentage.
FUNC FLOAT GET_CASH_REWARD_PERCENT()
	RETURN g_sMPTunables.fCriminal_Damage_Percent_Cash
ENDFUNC

/// PURPOSE:
/// 	0.001 - RP reward percentage.
FUNC FLOAT GET_RP_REWARD_PERCENT()
	RETURN g_sMPTunables.fCriminal_Damage_Percent_RP
ENDFUNC

/// PURPOSE:
/// 	1000 - Cash reward bonus (winner).
FUNC INT GET_CASH_REWARD_BONUS()
	RETURN g_sMPTunables.iCriminal_Damage_Bonus_Cash
ENDFUNC

/// PURPOSE:
/// 	5000 - RP reward bonus (winner).
FUNC INT GET_RP_REWARD_BONUS()
	RETURN g_sMPTunables.iCriminal_Damage_Bonus_RP
ENDFUNC

/// PURPOSE:
///    2,000,000 - Cap for score.
FUNC INT GET_REWARD_SCORE_CAP()
	RETURN g_sMPTunables.iCriminal_Damage_S_Cap
ENDFUNC

/// PURPOSE:
/// 	10,000 - Player score threshold before qualifying for reward.
FUNC INT GET_REWARD_PLAYER_THRESHOLD()
	RETURN g_sMPTunables.iCRIMINAL_DAMAGE_GET_REWARD_PLAYER_THRESHOLD // TODO: Replace with tunable.
ENDFUNC

/// PURPOSE:
///    1.0 - Modifier for PROP value.
FUNC FLOAT GET_VALUE_MODIFIER_FOR_PROPS()
	RETURN g_sMPTunables.fCriminal_damage_Base_value_modifier_applied_to_props
ENDFUNC

/// PURPOSE:
///    1.0 - Modifier for VEHICLE value.
FUNC FLOAT GET_VALUE_MODIFIER_FOR_VEHICLES()
	RETURN g_sMPTunables.fCriminal_damage_Base_value_modifier_applied_to_vehicles
ENDFUNC

/// PURPOSE:
///    1.0 - Modifier for CLOTHING value.
FUNC FLOAT GET_VALUE_MODIFIER_FOR_CLOTHING()
	RETURN g_sMPTunables.fCriminal_damage_Base_value_modifier_applied_to_clothing
ENDFUNC

/// PURPOSE:
///    10 - Cap for total time.
FUNC INT GET_REWARD_TIME_CAP()
	RETURN g_sMPTunables.iCriminal_damage_T_Cap
ENDFUNC

/// PURPOSE:
///    30 - Cap for total participants.
FUNC INT GET_REWARD_PLAYER_CAP()
	RETURN g_sMPTunables.iCriminal_damage_P_Cap
ENDFUNC



/// PURPOSE: Helper function to get debug string for stage
///    
/// PARAMS: 
///    stage - The stage 
/// RETURNS: The name of the stage
///    
FUNC STRING GET_AMBC_STAGE_STRING(AMBC_RUN_STAGE stage)

	SWITCH(stage)
		CASE AMBC_WARN				RETURN "AMBC_WARN"
		CASE AMBC_WAIT				RETURN "AMBC_WAIT"
		CASE AMBC_START				RETURN "AMBC_START"
		CASE AMBC_CHALLENGE			RETURN "AMBC_CHALLENGE"
		CASE AMBC_REWARDS			RETURN "AMBC_REWARDS"
		CASE AMBC_END 				RETURN "AMBC_END"
	ENDSWITCH

	RETURN "UNKNOWN STAGE!"
	
ENDFUNC



/// PURPOSE:  Get the time limit for the current challenge
///    
/// RETURNS: The time limit
///    
FUNC INT GET_CHALLENGE_TIME_LIMIT()

	RETURN g_sMPTunables.iCriminal_damage_Event_Time_Limit

ENDFUNC

PROC SET_AS_PERMANENT_PARTICIPANT()

	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_PARTICIPATED_PERMANENT)
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_PARTICIPATED_PERMANENT)
		FM_EVENT_SET_PLAYER_IS_PERMANENT_PARTICIPANT_IN_FM_EVENT(TRUE)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === SET_AS_PERMANENT_PARTICIPANT")
	ENDIF
	
ENDPROC

FUNC BOOL IS_SCORE_BETTER_THAN_SCORE(INT iScore1, INT iScore2)

	RETURN iScore1 > iScore2

ENDFUNC

//----------------------
//	Client Functions
//----------------------

PROC DO_START_HELP()

	IF NOT IS_RADAR_HIDDEN()
		IF NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_FM_EVENT_START)
		AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			IF NOT IS_BIT_SET(iHelpBS,0)
				PRINT_HELP_NO_SOUND("AMBC_HELP",DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
				SET_FREEMODE_EVENT_HELP_BACKGROUND()
				SET_BIT(iHelpBS,0)	
			ELIF NOT IS_BIT_SET(iHelpBS,1)	
				PRINT_HELP_NO_SOUND("AMBC_HELP1",DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
				SET_FREEMODE_EVENT_HELP_BACKGROUND()
				SET_BIT(iHelpBS,1)	
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC STORE_INITIAL_VALUES()

ENDPROC

/// PURPOSE: Get the local player's current score
///    
/// RETURNS: The score
///    
FUNC INT GET_CURRENT_CHALLENGE_VALUE()
	
	RETURN iTotalCashDamage

ENDFUNC

PROC CLEAR_PLAYER_SCORE()

	IF playerBD[PARTICIPANT_ID_TO_INT()].clientChallengeData.iChallengeInt != 0
		playerBD[PARTICIPANT_ID_TO_INT()].clientChallengeData.iChallengeInt = 0
	ENDIF
	
ENDPROC

/// PURPOSE: Tracks the local player's best score and notifies the server when it increases
///    
PROC TRACK_CHALLENGE()

	IF NOT FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID())
	AND NOT IS_PLAYER_SCTV(PLAYER_ID())

		INT iCurrentValue
		iCurrentValue = GET_CURRENT_CHALLENGE_VALUE()
		
		IF iCurrentValue != playerBD[PARTICIPANT_ID_TO_INT()].clientChallengeData.iChallengeInt
			IF iStartTime = 0
				iStartTime = NATIVE_TO_INT(GET_NETWORK_TIME())
				IF telemetryStruct.m_timeTakenForObjective = 0
					telemetryStruct.m_timeTakenForObjective = GET_CLOUD_TIME_AS_INT()  
				ENDIF
			ENDIF
			IF iCurrentValue > iLocalBestAttempt	
				iLocalBestAttempt = iCurrentValue
				playerBD[PARTICIPANT_ID_TO_INT()].clientChallengeData.iChallengeInt = iCurrentValue
				SET_AS_PERMANENT_PARTICIPANT()
				CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === TRACK_CHALLENGE - Updating challenge value to ",iCurrentValue)
			ENDIF
		ENDIF
	ENDIF

ENDPROC



/// PURPOSE: Formats the player's score into the correct data type
///    
/// PARAMS:
///    iReturnScore - The value is returned here if the score is converted to an int
///    fReturnScore - The value is returned here if the score is converted to an float
///    iPassed - The original value that is passed in
PROC FORMAT_SCORE_FOR_DISPLAY(INT &iReturnScore, INT iPassed)

	iReturnScore = iPassed

ENDPROC

PROC DRAW_LOCAL_PLAYER_SCORE_AT_POSITION(HUDORDER position)

	INT iScore = playerBD[PARTICIPANT_ID_TO_INT()].clientChallengeData.iChallengeInt
		
	IF iScore > 0

		PLAYER_INDEX localPlayerID = NETWORK_GET_PLAYER_INDEX(PARTICIPANT_ID())
		INT iDisplayScore = 0
		
		FORMAT_SCORE_FOR_DISPLAY(iDisplayScore,iScore)
		DRAW_GENERIC_SCORE(iDisplayScore,GET_PLAYER_NAME(localPlayerID), 0, HUD_COLOUR_BLUE, position, TRUE, "HUD_CASH", DEFAULT,DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_BLUE)

	ENDIF

ENDPROC

PROC DRAW_PARTICIPANT_SCORE_AT_POSITION(INT iPart, HUDORDER position)

	IF iPart > -1

		PARTICIPANT_INDEX part = INT_TO_PARTICIPANTINDEX(iPart)	
		IF NETWORK_IS_PARTICIPANT_ACTIVE(part)
			
			INT iScore = playerBD[iPart].clientChallengeData.iChallengeInt
						
			IF iScore > 0
								
				PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart))
				
				INT iDisplayScore = 0
				FORMAT_SCORE_FOR_DISPLAY(iDisplayScore,iScore)
				
				DRAW_GENERIC_SCORE(iDisplayScore,GET_PLAYER_NAME(playerID), 0, HUD_COLOUR_RED, position, TRUE, "HUD_CASH",DEFAULT,DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_RED)
				
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE: Draws the bottom right UI for the challenge including scores and timer
///    
PROC DRAW_CHALLENGE_LEADER_BOARD()
	
	IF serverBD.iWinnerPart[0] > -1
	
		PLAYER_INDEX playerList[NUM_MINI_LEADERBOARD_PLAYERS]
		PLAYER_INDEX playerTemp
		INT iScore[NUM_MINI_LEADERBOARD_PLAYERS]
		INT iLocal, iTime, i, iPartToUse
		BOOL bForceRebuild = FALSE
		HUD_COLOURS timeColour
		
		HUD_COLOURS iFleckColour[NUM_MINI_LEADERBOARD_PLAYERS]
		
		REPEAT NUM_MINI_LEADERBOARD_PLAYERS i
			playerList[i] = INVALID_PLAYER_INDEX()
		ENDREPEAT
		REPEAT NUM_MINI_LEADERBOARD_PLAYERS i
			IF serverBD.iWinnerPart[i] > -1
				PARTICIPANT_INDEX partID = INT_TO_PARTICIPANTINDEX(serverBD.iWinnerPart[i])
				IF NETWORK_IS_PARTICIPANT_ACTIVE(partID)
					playerTemp = NETWORK_GET_PLAYER_INDEX(partID)
					IF IS_NET_PLAYER_OK(playerTemp,FALSE)
						IF NOT IS_PLAYER_SCTV(playerTemp)
							playerList[i] = playerTemp
							iScore[i] = playerBD[serverBD.iWinnerPart[i]].clientChallengeData.iChallengeInt	
							IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
//							AND GB_GET_THIS_PLAYER_GANG_BOSS(playerTemp) = GB_GET_LOCAL_PLAYER_GANG_BOSS()
								IF GB_IS_PLAYER_MEMBER_OF_A_GANG(playerTemp)
									iFleckColour[i] = GET_PLAYER_HUD_COLOUR(playerTemp)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		//Deal with Spectators
		IF IS_PLAYER_SPECTATING(PLAYER_ID())
			PED_INDEX specPed = GET_SPECTATOR_SELECTED_PED()
			PLAYER_INDEX specPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(specPed)
			IF NETWORK_IS_PLAYER_ACTIVE(specPlayer)
				PARTICIPANT_INDEX specPart = NETWORK_GET_PARTICIPANT_INDEX(specPlayer)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(specPart)	
					iPartToUse = NATIVE_TO_INT(specPart)
				ENDIF
			ENDIF
		ELSE
			iPartToUse = PARTICIPANT_ID_TO_INT()
		ENDIF
		
		iLocal = IMAX(0,playerBD[iPartToUse].clientChallengeData.iChallengeInt)
		iTime = IMAX(0,GET_CHALLENGE_TIME_LIMIT()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBd.eventTimer))
		
		HANDLE_AMBIENT_EVENT_COUNTDOWN_AUDIO(iTime)
	
		IF iTime > 30000
			timeColour = HUD_COLOUR_WHITE
		ELSE
			timeColour = HUD_COLOUR_RED
		ENDIF
	
		BOTTOM_RIGHT_UI_1STTIME_2NDTIME_3RDTIME_DAMAGE_TIMER(playerList[0],playerList[1],playerList[2],iScore[0],iScore[1],iScore[2],iLocal,iTime,bForceRebuild,timeColour, GET_COUNTDOWN_TIMER_NAME_FOR_EVENT(),DEFAULT,DEFAULT,DEFAULT,iFleckColour[0],iFleckColour[1],iFleckColour[2])

	ENDIF

ENDPROC

PROC DRAW_CHALLENGES_DPAD_LEADERBOARD()
	DRAW_CHALLENGE_DPAD_LBD(chDPStruct.challengeLbdStruct, chDPStruct.siDpadMovie,SUB_CHALLENGE, chDPStruct.dpadVariables, chDPStruct.fmDpadStruct)
ENDPROC

FUNC BOOL IS_LOCAL_PLAYER_IN_PLACE(INT iPlace)

	IF iPlace >= 0
		IF serverBD.iWinnerPart[iPlace] = PARTICIPANT_ID_TO_INT()
		AND playerBD[PARTICIPANT_ID_TO_INT()].clientChallengeData.iChallengeInt > 0
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_PARTICIPANT_IN_PLACE(INT iPart, INT iPlace)

	IF iPart >= 0
		IF iPlace >= 0
			IF serverBD.iWinnerPart[iPart] = iPart
			AND IS_SCORE_BETTER_THAN_SCORE(playerBD[iPart].clientChallengeData.iChallengeInt, 0)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_ANYONE_IN_PLACE(INT iPlace)

	IF iPlace >= 0
		INT iPart = serverBD.iWinnerPart[iPlace]
		IF iPart >= 0
			IF IS_SCORE_BETTER_THAN_SCORE(playerBD[iPart].clientChallengeData.iChallengeInt, 0)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL HAS_PARTICIPANT_PARTICIPATED_IN_EVENT(INT iPart)

	IF iPart >= 0 AND iPart < 32
		RETURN (playerBD[iPart].clientChallengeData.iChallengeInt > 0)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DID_I_ACHIEVE_PODIUM_FINISH()

	IF IS_LOCAL_PLAYER_IN_PLACE(0)
	OR IS_LOCAL_PLAYER_IN_PLACE(1)
	OR IS_LOCAL_PLAYER_IN_PLACE(2)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE	
ENDFUNC

/// PURPOSE:
///    Calculate RP and GTA$ rewards for completing this activity.
/// PARAMS:
///    fPositionMod - Position modifier.
///    iCash - ByREF GTA$ reward.
///    iRP - ByREF RP reward.
PROC CALCULATE_EOM_REWARDS(INT& iCash, INT& iRP)
	INT iPartCash = 0
	
	iCash = 0
	iRP = 0

	//
	// Latest calculations are based on url:bugstar:2435074 - Update the reward calculation for Criminal Damage.
	//
	IF playerBD[PARTICIPANT_ID_TO_INT()].clientChallengeData.iChallengeInt >= GET_REWARD_PLAYER_THRESHOLD()
	OR DID_I_ACHIEVE_PODIUM_FINISH()
	
		// New formula = (Base + (Score(max S_Cap) x Percent))) + Bonus(winner only)
		FLOAT fCashPercent = GET_CASH_REWARD_PERCENT()
		FLOAT fRPPercent = GET_RP_REWARD_PERCENT()
		
		INT iCashBase = GET_CASH_REWARD_BASE()
		INT iRPBase = GET_RP_REWARD_BASE()
		INT iScore = playerBD[PARTICIPANT_ID_TO_INT()].clientChallengeData.iChallengeInt
		INT iCashBonus = 0
		INT iRPBonus = 0
		
		INT iPartRP = 0
		
		// Winner bonus
		IF IS_LOCAL_PLAYER_IN_PLACE(0)
			iCashBonus = GET_CASH_REWARD_BONUS()
			iRPBonus = GET_RP_REWARD_BONUS()
		ELSE
			FM_EVENT_SETUP_DO_MINIMUM_PARTICIPATION_HELP(TRUE)
		ENDIF
		
		// Score cap
		IF iScore > GET_REWARD_SCORE_CAP()
			iScore = GET_REWARD_SCORE_CAP()
		ENDIF
		INT iParticipationTime = FLOOR((TO_FLOAT(serverBD.iEndTime - iStartTime) / 60) / 1000)
		IF iParticipationTime > g_sMPTunables.iParticipation_T_Cap
			iParticipationTime = g_sMPTunables.iParticipation_T_Cap
		ELIF iParticipationTime < 1
			iParticipationTime = 1
		ENDIF

		iPartCash = GET_EOM_DEFAULT_CASH_REWARD() * iParticipationTime
		iPartRP = GET_EOM_DEFAULT_RP_REWARD() * iParticipationTime
		
		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_EOM_REWARDS - Participation Cash: ", iPartCash)
		
		iCash = ROUND_TO_NEAREST(ROUND((((iCashBase + FLOOR(iScore * fCashPercent)) + iCashBonus) * g_sMPTunables.fCriminal_Damage_Event_Multiplier_Cash)), 50)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_EOM_REWARDS - iCash: ((", iCashBase, " + (", iScore, " * ", fCashPercent, ")) + ", iCashBonus, ") = ", iCash, " - rounded to nearest 50")
		
		iRP = ROUND_TO_NEAREST(ROUND((((iRPBase + FLOOR(iScore * fRPPercent)) + iRPBonus) * g_sMPTunables.fCriminal_Damage_Event_Multiplier_RP) + iPartRP), 50)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_EOM_REWARDS - iRP: ((", iRPBase, " + (", iScore, " * ", fRPPercent, ")) + ", iRPBonus, " + ", iPartRP, ") = ", iRP, " - rounded to nearest 50")
		
	ELSE
		FM_EVENT_SETUP_DO_MINIMUM_PARTICIPATION_HELP(FALSE)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_EOM_REWARDS - Player score not high enough for participant reward! Value: ", playerBD[PARTICIPANT_ID_TO_INT()].clientChallengeData.iChallengeInt)
	ENDIF
	
	GB_HANDLE_GANG_BOSS_CUT(iCash)
	
	iCash += iPartCash
	CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_EOM_REWARDS - Cash after Boss Cut with Participation Added: ", iCash)
ENDPROC

PROC GIVE_REWARDS()

	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_REWARD_GIVEN)
	
		//Do not reward player if passive or hiding the event
		IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(PLAYER_ID())
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_REWARD_GIVEN)
			EXIT
		ENDIF
		IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID())
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_REWARD_GIVEN)
			EXIT
		ENDIF	
		
		INT iCash, iRP
		CALCULATE_EOM_REWARDS(iCash, iRP)
		
		telemetryStruct.m_cashEarned = iCash
		IF !g_sMPTunables.bcriminal_damage_disable_share_cash      
			IF telemetryStruct.m_cashEarned > 0
				SET_LAST_JOB_DATA(LAST_JOB_BLAST, telemetryStruct.m_cashEarned)
			ENDIF
		ENDIF
		telemetryStruct.m_rpEarned = iRP
	
		IF iCash > 0
			IF USE_SERVER_TRANSACTIONS()
				INT iScriptTransactionIndex
				TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_AMBIENT_JOB_BLAST, iCash, iScriptTransactionIndex, FALSE, TRUE) //Add new service
			ELSE
				AMBIENT_JOB_DATA amData
				NETWORK_EARN_FROM_AMBIENT_JOB(iCash,"AM_CRIMINAL_DAMAGE",amData)
			ENDIF
			g_i_cashForEndEventShard = iCash
		ENDIF
		
		IF iRP > 0
			NEXT_RP_ADDITION_SHOW_RANKBAR()
			GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD,PLAYER_PED_ID(),"",XPTYPE_COMPLETE,XPCATEGORY_COMPLETE_CRIMINAL_DAMAGE,iRP)	
		ENDIF
		
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_REWARD_GIVEN)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === GIVE_REWARDS - Awarded player $", iCash, "& RP: ",iRP)
		
	ENDIF
	
ENDPROC


/// PURPOSE: Displays a big message if local player is the winner, otherwise displays a ticker
///    
PROC SHOW_END_MESSAGES()

	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_ENDING_SHOWN)
	AND (FM_EVENT_END_SHARD_SAFE_TO_DISPLAY()
		OR IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_LOW_PARTICIPANTS))
		
		IF IS_BIG_MESSAGE_TYPE_FOR_FM_EVENTS(GET_TYPE_OF_CURRENT_BIG_MESSAGE())
			CLEAR_ALL_BIG_MESSAGES()
		ENDIF
	
		IF NOT IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_LOW_PARTICIPANTS)
	
			IF IS_ANYONE_IN_PLACE(0)
		
				INT iDisplayScore
				INT iScore = playerBD[PARTICIPANT_ID_TO_INT()].clientChallengeData.iChallengeInt
				FORMAT_SCORE_FOR_DISPLAY(iDisplayScore,iScore)
				
				IF IS_LOCAL_PLAYER_IN_PLACE(0)

					SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_FM_EVENT_WIN_GENERIC_CASH,iDisplayScore,"AMBC_WIN","AMCH_WIN", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME)
					CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === You Win!")
				ELSE
					PARTICIPANT_INDEX winnerPart = INT_TO_PARTICIPANTINDEX(serverBD.iWinnerPart[0])
					IF NETWORK_IS_PARTICIPANT_ACTIVE(winnerPart)
					
						IF IS_LOCAL_PLAYER_IN_PLACE(1)
							SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_FM_EVENT_WIN_GENERIC_CASH,iDisplayScore,"AMBC_2ND","AMCH_OVER", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME)
						ELIF IS_LOCAL_PLAYER_IN_PLACE(2)
							SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_FM_EVENT_WIN_GENERIC_CASH,iDisplayScore,"AMBC_3RD","AMCH_OVER", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME)
						ELSE
						
							PLAYER_INDEX winningPlayer = NETWORK_GET_PLAYER_INDEX(winnerPart)
							iScore = playerBD[serverBD.iWinnerPart[0]].clientChallengeData.iChallengeInt
							FORMAT_SCORE_FOR_DISPLAY(iDisplayScore,iScore)
							
							STRING sMessage
							sMessage = "AMBC_LOSE"
							STRING sPlayer = GET_PLAYER_NAME(winningPlayer)
							IF GB_IS_PLAYER_MEMBER_OF_A_GANG(winningPlayer)
								sPlayer = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(winningPlayer)
							ENDIF
							
							SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_AND_INT(BIG_MESSAGE_FM_EVENT_FINISHED_GENERIC_CASH, iDisplayScore, sMessage, sPlayer, "AMPP_LOSER", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME)
						ENDIF
					ENDIF
					CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === You Lose!")
				ENDIF
			ELSE
				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_FINISHED,"CPC_OVER","AMCH_NOWIN",DEFAULT,ciFM_EVENTS_END_UI_SHARD_TIME)
				CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === No winner!")
			ENDIF
		ELSE
			SET_UP_BAIL_FM_EVENT_DUE_TO_LOW_SESSION_NUMBERS_BIG_MESSAGE("","")
		ENDIF
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_ENDING_SHOWN)
		CALL_FREEMODE_AMBIENT_EVENTS_TELEMETRY(FMMC_TYPE_CRIMINAL_DAMAGE)
	ENDIF

ENDPROC

PROC CLIENT_PROCESS_SORT_FOR_LBD()

	INT iParticipant
	THE_LEADERBOARD_STRUCT blankStruct
	PLAYER_INDEX playerTemp
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
	
		chDPStruct.challengeLbdStruct[iParticipant] = blankStruct
		chDPStruct.challengeLbdStruct[iParticipant].playerID = INVALID_PLAYER_INDEX()
	
		IF serverBD.iWinnerPart[iParticipant] > -1
			PARTICIPANT_INDEX partID = INT_TO_PARTICIPANTINDEX(serverBD.iWinnerPart[iParticipant])
			IF NETWORK_IS_PARTICIPANT_ACTIVE(partID)
				playerTemp = NETWORK_GET_PLAYER_INDEX(partID)
				IF NOT IS_PLAYER_SCTV(playerTemp)
					chDPStruct.challengeLbdStruct[iParticipant].iParticipant = serverBD.iWinnerPart[iParticipant]
					chDPStruct.challengeLbdStruct[iParticipant].playerID = playerTemp
					
					INT iCurrentScore = playerBD[serverBD.iWinnerPart[iParticipant]].clientChallengeData.iChallengeInt
					
					chDPStruct.challengeLbdStruct[iParticipant].iScore = iCurrentScore
					chDPStruct.challengeLbdStruct[iParticipant].iFinishTime = -1
					chDPStruct.dpadVariables.eDpadVarType = DPAD_VAR_CASH
						
					CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CHALLENGES === CLIENT_PROCESS_SORT_FOR_LBD - iParticipant = ", chDPStruct.challengeLbdStruct[iParticipant].iParticipant)
					CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CHALLENGES === CLIENT_PROCESS_SORT_FOR_LBD - Player = ",GET_PLAYER_NAME( chDPStruct.challengeLbdStruct[iParticipant].playerID))
					CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CHALLENGES === CLIENT_PROCESS_SORT_FOR_LBD - iScore = ", chDPStruct.challengeLbdStruct[iParticipant].iScore)
				ENDIF
				ENDIF
		ENDIF
	ENDREPEAT

ENDPROC

PROC CRIMINAL_DAMAGE_CLIENT_CALCULATE_CLOTHING_COST()

	//Do we need to recalculate
	IF IS_BIT_SET(iLocalBitset,AMBC_BS_CALCULATED_PLAYER_COST)
		IF IS_PLAYER_CHANGING_CLOTHES_DURING_CHALLENGE()
			IF NOT IS_BIT_SET(iLocalBitset,AMBC_BS_CHANGING_CLOTHES)
				SET_BIT(iLocalBitset,AMBC_BS_CHANGING_CLOTHES)
				CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CLIENT_CALCULATE_CLOTHING_COST - SET_BIT(iLocalBitset,AMBC_BS_CHANGING_CLOTHES)")
			ENDIF
		ELSE
			IF IS_BIT_SET(iLocalBitset,AMBC_BS_CHANGING_CLOTHES)
				CLEAR_BIT(iLocalBitset,AMBC_BS_CHANGING_CLOTHES)
				CLEAR_BIT(iLocalBitset,AMBC_BS_CALCULATED_PLAYER_COST)
				CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CLIENT_CALCULATE_CLOTHING_COST - Need to recalculate clothing cost.")
			ENDIF
		ENDIF
	ENDIF
	
	//Calculate local player's clothing cost
	IF NOT IS_BIT_SET(iLocalBitset,AMBC_BS_CALCULATED_PLAYER_COST)
		IF IS_NET_PLAYER_OK(PLAYER_ID())
		AND NOT IS_PLAYER_SCTV(PLAYER_ID())
		
			PED_INDEX pedID = PLAYER_PED_ID()
			MODEL_NAMES playerModel = GET_ENTITY_MODEL(pedID)
			INT iClothingCost
											
			iClothingCost += CALCULATE_PLAYER_CLOTHING_ITEM_COST(playerModel,pedID,COMP_TYPE_JBIB)
			iClothingCost += CALCULATE_PLAYER_CLOTHING_ITEM_COST(playerModel,pedID,COMP_TYPE_LEGS)
			iClothingCost += CALCULATE_PLAYER_CLOTHING_ITEM_COST(playerModel,pedID,COMP_TYPE_FEET)
			iClothingCost += CALCULATE_PLAYER_CLOTHING_ITEM_COST(playerModel,pedID,COMP_TYPE_BERD)
			iClothingCost += CALCULATE_PLAYER_CLOTHING_ITEM_COST(playerModel,pedID,COMP_TYPE_TEETH)
			iClothingCost += CALCULATE_PLAYER_CLOTHING_ITEM_COST(playerModel,pedID,COMP_TYPE_SPECIAL)
			iClothingCost += CALCULATE_PLAYER_CLOTHING_ITEM_COST(playerModel,pedID,COMP_TYPE_PROPS,ENUM_TO_INT(ANCHOR_HEAD))
			iClothingCost += CALCULATE_PLAYER_CLOTHING_ITEM_COST(playerModel,pedID,COMP_TYPE_PROPS,ENUM_TO_INT(ANCHOR_LEFT_WRIST))
			iClothingCost += CALCULATE_PLAYER_CLOTHING_ITEM_COST(playerModel,pedID,COMP_TYPE_PROPS,ENUM_TO_INT(ANCHOR_RIGHT_WRIST))
			iClothingCost += CALCULATE_PLAYER_CLOTHING_ITEM_COST(playerModel,pedID,COMP_TYPE_PROPS,ENUM_TO_INT(ANCHOR_EYES))
			iClothingCost += CALCULATE_PLAYER_CLOTHING_ITEM_COST(playerModel,pedID,COMP_TYPE_PROPS,ENUM_TO_INT(ANCHOR_EARS))
			
			playerBD[PARTICIPANT_ID_TO_INT()].iPlayerClothingCost = iClothingCost
			CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CLIENT_CALCULATE_CLOTHING_COST - Updated player clothing cost to ",iClothingCost)
			
			SET_BIT(iLocalBitset,AMBC_BS_CALCULATED_PLAYER_COST)
		ENDIF
	ENDIF
	
ENDPROC

PROC HANDLE_OBJECTIVE_TEXT()

	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND NOT FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(PLAYER_ID())
	AND NOT FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID())
	AND IS_IT_SAFE_TO_DISPLAY_FM_EVENT_UI() 
		IF playerBD[PARTICIPANT_ID_TO_INT()].eAMBC_Stage = AMBC_WAIT
			Print_Objective_Text("AMBC_PREP")
		ELIF playerBD[PARTICIPANT_ID_TO_INT()].eAMBC_Stage = AMBC_CHALLENGE
			Print_Objective_Text("AMBC_START")
		ELSE
			Clear_Any_Objective_Text_From_This_Script()
		ENDIF
	ELSE
		Clear_Any_Objective_Text_From_This_Script()
	ENDIF


ENDPROC

PROC HANDLE_CD_AUDIO()

	//Do 1st place sounds
	IF IS_LOCAL_PLAYER_IN_PLACE(0)	
		IF NOT IS_BIT_SET(iLocalBitset,AMBC_BS_LOCAL_PLAYER_FIRST)
			IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
				PLAY_SOUND_FRONTEND(-1,"Enter_1st","GTAO_Biker_Modes_Soundset", FALSE)
			ELSE
				PLAY_SOUND_FRONTEND(-1,"Enter_1st","GTAO_FM_Events_Soundset", FALSE)
			ENDIF
			SET_BIT(iLocalBitset,AMBC_BS_LOCAL_PLAYER_FIRST) 
			CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === HANDLE_CD_AUDIO - Enter_1st")
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBitset,AMBC_BS_LOCAL_PLAYER_FIRST)
			IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
				PLAY_SOUND_FRONTEND(-1,"Lose_1st","GTAO_Biker_Modes_Soundset", FALSE)
			ELSE
				PLAY_SOUND_FRONTEND(-1,"Lose_1st","GTAO_FM_Events_Soundset", FALSE)
			ENDIF
			CLEAR_BIT(iLocalBitset,AMBC_BS_LOCAL_PLAYER_FIRST) 
			CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === HANDLE_CD_AUDIO - Lose_1st")
		ENDIF
	ENDIF

ENDPROC

//----------------------
//	Server Functions
//----------------------

/// PURPOSE: Helper function to swap the values of two variables
///    
/// PARAMS:
///    iNum1
///    iNum2
PROC SWAP(INT &iNum1, INT &iNum2)
	
	CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === SWAP ",iNum1," AND ",iNum2)

	iNum1 = iNum1 + iNum2
	iNum2 = iNum1 - iNum2
	iNum1 = iNum1 - iNum2

ENDPROC

/// PURPOSE: Sends tickers to players to inform them of any relevant leaderboard position changes
///    
/// PARAMS:
///    iOldPart - The participant who previously occupied this position on the leaderboard
///    iNewPart - The particpant who is replacing them
///    iPosition - The position on the leaderboard
PROC SEND_POSITION_UPDATE_TICKERS(INT &iNewPart,INT &iPosition)

	IF iNewPart >= 0
	AND iPosition >= 0

		SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData

		//Send ticker to participants
				
		PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iNewPart))	
		
		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === New player in place ",iPosition," = ",GET_PLAYER_NAME(PlayerId))
		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === Attempt = ",playerBD[iNewPart].clientChallengeData.iChallengeInt)
		
		IF iPosition = 0 //In first place

			TickerEventData.TickerEvent = TICKER_EVENT_SCORE_IN_BLAST
			TickerEventData.playerID = PlayerId

			CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === TickerEventData.playerID = ",GET_PLAYER_NAME(TickerEventData.playerID ))
			BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT())
		
		ENDIF
	ENDIF
	
	iNewPart = -1
	iPosition = -1
	
ENDPROC

/// PURPOSE: Sorts the players in order of score and updates the server data
///    
PROC SERVER_PROCESS_PLAYERS_ATTEMPTING_CHALLENGE(INT iParticipant)
		
	//Find the top three players
	INT iChallengeAttempt, iChallengeTime
	
	iChallengeAttempt = playerBD[iParticipant].clientChallengeData.iChallengeInt
				
	iChallengeTime = playerBD[iParticipant].clientChallengeData.iPerformingTime
	CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CHALLENGES === Participant ",iParticipant," iChallengeTime = ",iChallengeTime)

	//IF iChallengeAttempt <> 0
	
		IF NOT IS_BIT_SET(serverBD.iParticipatedBS,iParticipant)
			INT iThreshold = serverBD.iWinnerPart[0] / 2 //Threshold for participation
			IF iChallengeAttempt >= iThreshold
				serverBD.iTotalParticipants++
				SET_BIT(serverBD.iParticipatedBS,iParticipant)
			ENDIF
		ENDIF
	
		INT iWinner
		INT iTemp = -2
		REPEAT NUM_LEADERBOARD_PLAYERS iWinner
			IF serverBD.iWinnerPart[iWinner] = iParticipant
				iWinner = NUM_LEADERBOARD_PLAYERS //Player is already on leaderboard at this position, don't bother checking lower positions
				//CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CHALLENGES === serverBD.iWinnerPart[iWinner] = iParticipant = ",iParticipant)
			ELSE
				IF iTemp >= -1
					CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CHALLENGES === iTemp >= -1 ",iTemp)
					IF iTemp >= 0
						//New entry, rearrange the list
						SWAP(serverBD.iWinnerPart[iWinner],iTemp)
					ENDIF
				ELIF (serverBD.iWinnerPart[iWinner] < 0 //Slot empty
				OR (iChallengeAttempt > playerBD[serverBD.iWinnerPart[iWinner]].clientChallengeData.iChallengeInt) //We have higher score
				OR (iChallengeAttempt = playerBD[serverBD.iWinnerPart[iWinner]].clientChallengeData.iChallengeInt
				AND iChallengeTime > playerBD[serverBD.iWinnerPart[iWinner]].clientChallengeData.iPerformingTime))
					iTemp = serverBD.iWinnerPart[iWinner]
					CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CHALLENGES === iTemp = ",iTemp)
					IF iChallengeAttempt <> 0
						iTickerWinner = iWinner
						iTickerParticipant = iParticipant
						//SEND_POSITION_UPDATE_TICKERS(serverBD.iWinnerPart[iWinner],iParticipant,iWinner)
					ENDIF
					serverBD.iWinnerPart[iWinner] = iParticipant
					
					//Check for any other places the player appears and remove them
					INT i
					FOR i = iWinner+1 TO NUM_LEADERBOARD_PLAYERS-1
						IF serverBD.iWinnerPart[i] = iParticipant
							serverBD.iWinnerPart[i] = -1
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
		ENDREPEAT
	//ENDIF
	
ENDPROC

PROC SERVER_PROCESS_INACTIVE_PARTICPANT(INT iParticipant)

	IF IS_BIT_SET(serverBD.iActivePartBS,iParticipant) //Have been active but are no longer
		INT iPart
		REPEAT NUM_LEADERBOARD_PLAYERS iPart
			IF serverBD.iWinnerPart[iPart] = iParticipant
				serverBD.iWinnerPart[iPart] = -1
				iPart = NUM_LEADERBOARD_PLAYERS
				CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CHALLENGES === Clearing leaderboard data for participant ",iParticipant)
			ENDIF
		ENDREPEAT
		CLEAR_BIT(serverBD.iParticipatedBS,iParticipant)
		CLEAR_BIT(serverBD.iActivePartBS,iParticipant)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CHALLENGES === Participant ",iParticipant," has left the script.")
	ENDIF


ENDPROC

//PROC SERVER_PROCESS_ACTIVE_PLAYER_CHECK(PARTICIPANT_INDEX partID)
//
//	PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX(partID)
//	IF NETWORK_IS_PLAYER_ACTIVE(playerID)
//		FM_EVENT_MAINTAIN_ACTIVE_PLAYER_CHECK(playerID)
//	ENDIF
//
//ENDPROC

/// PURPOSE: Main server participant loop
///    
PROC SERVER_PROCESS_PARTICIPANT_CHECKS()

	INT iParticipant, iTempPeakParts, iTempPeakQualifiers,iPlayerBitSet
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			SET_BIT(iPlayerBitSet, iParticipant)
		ELSE
			SERVER_PROCESS_INACTIVE_PARTICPANT(iParticipant)
		ENDIF
	ENDREPEAT
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF IS_BIT_SET(iPlayerBitSet, iParticipant)
			//SERVER_PROCESS_ACTIVE_PLAYER_CHECK(INT_TO_PARTICIPANTINDEX(iParticipant))
			SERVER_PROCESS_PLAYERS_ATTEMPTING_CHALLENGE(iParticipant)
			SET_BIT(serverBD.iActivePartBS,iParticipant)
			iTempPeakParts++
			IF IS_BIT_SET(playerBD[iParticipant].iClientBitSet,CLIENT_BS_PARTICIPATED_PERMANENT)
				iTempPeakQualifiers++
			ENDIF
		ENDIF
		
	ENDREPEAT
	
	IF iTempPeakParts > ServerBD.iPeakParticipants
		ServerBD.iPeakParticipants = iTempPeakParts
		PRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === SERVER_PROCESS_PARTICIPANT_CHECKS - iTempPeakParts (",iTempPeakParts,") is GREATER than peak participant count so far (",ServerBD.iPeakParticipants,"), updating value.")
	ELSE
		serverBD.iPlayersLeftInProgress = serverBD.iPeakParticipants  - iTempPeakParts
	ENDIF
	
	IF iTempPeakQualifiers > ServerBD.iPeakQualifiers
		ServerBD.iPeakQualifiers = iTempPeakQualifiers
		PRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === SERVER_PROCESS_PARTICIPANT_CHECKS - iTempPeakQualifiers (",iTempPeakQualifiers,") is GREATER than peak participant count so far (",ServerBD.iPeakQualifiers,"), updating value.")
	ENDIF
	
	

ENDPROC

/// PURPOSE: Check if a reward has been given to any player
///    
/// RETURNS: TRUE if any player has received the reward, FALSE otherwise
///    
FUNC BOOL SERVER_HAS_GIVEN_REWARD()

	BOOL bRewardGiven = FALSE
	INT iParticipant
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX( iParticipant))	
			PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX( iParticipant))
			IF NOT FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(playerID)
				IF NOT IS_BIT_SET(playerBD[iParticipant].iClientBitSet,CLIENT_BS_ENDING_SHOWN)
				OR playerBD[iParticipant].eAMBC_Stage != AMBC_END
					RETURN FALSE
				ELSE		
					IF IS_BIT_SET(playerBD[iParticipant].iClientBitSet,CLIENT_BS_REWARD_GIVEN)
						bRewardGiven = TRUE
						CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === Server given reward to participant ",iParticipant)
					ENDIF
				ENDIF
			ELSE
				bRewardGiven = TRUE
				CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === Participant has hidden event, not giving reward ",iParticipant)
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN bRewardGiven

ENDFUNC

FUNC BOOL IS_VEHICLE_YACHT_VEHICLE(VEHICLE_INDEX victimVeh)

	IF DECOR_IS_REGISTERED_AS_TYPE("PYV_Owner",DECOR_TYPE_INT)
		IF DECOR_EXIST_ON(victimVeh,"PYV_Owner")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

//FUNC FLOAT GET_CD_ENTITY_HEALTH(ENTITY_INDEX entID)
//
//	IF IS_ENTITY_A_PED(entID)
//		RETURN TO_FLOAT(GET_ENTITY_HEALTH(entID))
//	ELSE
//		VEHICLE_INDEX vehID = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entID)
//	
//		FLOAT fVehHealthPercentage = GET_VEHICLE_HEALTH_PERCENTAGE(vehID)
//		FLOAT fVehHealthEngine = GET_VEHICLE_ENGINE_HEALTH(vehID)
//		FLOAT fVehHealthBody = GET_VEHICLE_BODY_HEALTH(vehID)
//		FLOAT fVehHealthPetrol = GET_VEHICLE_PETROL_TANK_HEALTH(vehID)
//		
//		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_PRICE_OF_DAMAGE - fVehHealthEngine = ",fVehHealthEngine)
//		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_PRICE_OF_DAMAGE - fVehHealthBody = ",fVehHealthBody)
//		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_PRICE_OF_DAMAGE - fVehHealthPetrol = ",fVehHealthPetrol)
//		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_PRICE_OF_DAMAGE - fVehHealthPercentage = ",fVehHealthPercentage)
//		
//		RETURN fVehHealthEngine + fVehHealthBody + fVehHealthPetrol
//	ENDIF
//
//ENDFUNC
//
//FUNC FLOAT GET_CD_ENTITY_MAX_HEALTH(ENTITY_INDEX entID)
//
//	IF IS_ENTITY_A_PED(entID)
//		RETURN TO_FLOAT(GET_ENTITY_MAX_HEALTH(entID))
//	ELSE
////		FLOAT fVehHealthEngine = GET_VEHICLE_ENGINE_MAX_HEALTH(entID)
////		FLOAT fVehHealthBody = GET_VEHICLE_BODY_HEALTH(entID)
////		FLOAT fVehHealthPetrol = GET_VEHICLE_PETROL_TANK_HEALTH(entID)
////		
////		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_PRICE_OF_DAMAGE - fVehHealthEngine = ",fVehHealthEngine)
////		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_PRICE_OF_DAMAGE - fVehHealthBody = ",fVehHealthBody)
////		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_PRICE_OF_DAMAGE - fVehHealthPetrol = ",fVehHealthPetrol)
////		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_PRICE_OF_DAMAGE - fVehHealthPercentage = ",fVehHealthPercentage)
////		
////		RETURN fVehHealthEngine + fVehHealthBody + fVehHealthPetrol
//		RETURN 11000.0
//	ENDIF
//
//ENDFUNC

//----------------------
//	Event Processing
//----------------------

PROC CALCULATE_PRICE_OF_DAMAGE(INT iCount)

	STRUCT_ENTITY_DAMAGE_EVENT sei 
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sei, SIZE_OF(sei))

		IF DOES_ENTITY_EXIST(sei.DamagerIndex)
			IF IS_ENTITY_A_PED(sei.DamagerIndex) 
				IF GET_PED_INDEX_FROM_ENTITY_INDEX(sei.DamagerIndex) =  PLAYER_PED_ID()
					
					IF DOES_ENTITY_EXIST(sei.VictimIndex)
						IF sei.VictimIndex != sei.DamagerIndex
							FLOAT fModelValue
						
							//Damaged a vehicle
							IF IS_ENTITY_A_VEHICLE(sei.VictimIndex)
								
								VEHICLE_INDEX victimVeh =  GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sei.VictimIndex)
								IF NOT IS_VEHICLE_A_PERSONAL_VEHICLE(victimVeh, FALSE)
								AND NOT IS_VEHICLE_YACHT_VEHICLE(victimVeh)
								AND NOT IS_VEHICLE_A_PERSONAL_TRUCK(victimVeh, FALSE)
								AND NOT IS_VEHICLE_A_PERSONAL_ARMORY_AIRCRAFT(victimVeh, FALSE)
								AND NOT IS_VEHICLE_A_PERSONAL_HACKER_TRUCK(victimVeh, FALSE)
								AND NOT IS_VEHICLE_A_TEST_DRIVE_VEHICLE(victimVeh, FALSE)
									
									CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_PRICE_OF_DAMAGE - Entity is vehicle.")
									
									MODEL_NAMES victimModel = GET_ENTITY_MODEL(victimVeh)
									
									VEHICLE_VALUE_DATA sValueData
									IF GET_VEHICLE_VALUE(sValueData, victimModel, IS_VEHICLE_ALT_VERSION(victimVeh))
										fModelValue = TO_FLOAT(sValueData.iStandardPrice)
										CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_PRICE_OF_DAMAGE - GET_VEHICLE_VALUE = ",fModelValue)
									ENDIF
									
									IF fModelValue <= 0
										fModelValue = TO_FLOAT(GET_VEHICLE_MODEL_VALUE(victimModel))
										CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_PRICE_OF_DAMAGE - No vehicle value, using = GET_VEHICLE_MODEL_VALUE = ",fModelValue)
									ENDIF
									
									fModelValue *= GET_VALUE_MODIFIER_FOR_VEHICLES()
									CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_PRICE_OF_DAMAGE - GET_VALUE_MODIFIER_FOR_VEHICLES = ",GET_VALUE_MODIFIER_FOR_VEHICLES()," fModelValue is now ",fModelValue)
													
									FLOAT fMaxDamageCap = TO_FLOAT(g_sMPTunables.icriminal_damage_vehicle_value_cap)
									IF fModelValue > fMaxDamageCap
										fModelValue = fMaxDamageCap
										CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_PRICE_OF_DAMAGE - Applying cap of ",fMaxDamageCap," to vehicle model price.")
									ENDIF
																		
								ELSE
									IF NOT IS_BIT_SET(iLocalBitset,AMBC_BS_DONE_PV_HELP)
										PRINT_HELP_NO_SOUND("AMBC_PERS",DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
										SET_FREEMODE_EVENT_HELP_BACKGROUND(FALSE)
										SET_BIT(iLocalBitset,AMBC_BS_DONE_PV_HELP)
									ENDIF
									CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_PRICE_OF_DAMAGE - IS_VEHICLE_A_PERSONAL_VEHICLE - not counting.")
								ENDIF
								
							//Damaged a prop		
							ELIF IS_ENTITY_AN_OBJECT(sei.VictimIndex)
//								fModelValue = (GET_VALUE_MODIFIER_FOR_PROPS() * g_sMPTunables.fCriminal_damage_Base_value_applied_to_props)
//								CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_PRICE_OF_DAMAGE - Entity is prop.")
								
							ELIF IS_ENTITY_A_PED(sei.VictimIndex)
								PED_INDEX pedID = GET_PED_INDEX_FROM_ENTITY_INDEX(sei.VictimIndex)
								IF IS_PED_A_PLAYER(pedID)
									CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_PRICE_OF_DAMAGE - Entity is player.")
	
									PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedID)
									IF NETWORK_IS_PLAYER_ACTIVE(playerID)
										PARTICIPANT_INDEX partID = NETWORK_GET_PARTICIPANT_INDEX(playerID)
										IF NETWORK_IS_PARTICIPANT_ACTIVE(partID)
											INT iPart = NATIVE_TO_INT(partID)
											IF iPart > -1 AND iPart < NUM_NETWORK_PLAYERS
											AND iPart != PARTICIPANT_ID_TO_INT() //No score for suicide
												fModelValue = TO_FLOAT(playerBD[iPart].iPlayerClothingCost)
												fModelValue *= GET_VALUE_MODIFIER_FOR_CLOTHING()
												CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_PRICE_OF_DAMAGE - CLOTHING - Total fModelValue: ", fModelValue)
											ENDIF
										ENDIF
									ENDIF							

								ENDIF
	
							//Dont care
							ELSE
								CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_PRICE_OF_DAMAGE - Entity not prop or veh or ped, don't care.")
								EXIT
							ENDIF
							
							IF fModelValue > 0
							
								INT iTotalCash
							
								IF IS_ENTITY_A_VEHICLE(sei.VictimIndex)
									iTotalCash = ROUND(fModelValue * g_sMPTunables.fCriminal_Damage_Damage_value_modifier * sei.Damage)
									
									IF iTotalCash < g_sMPTunables.iCriminal_damage_bullet_min_cash
										iTotalCash = g_sMPTunables.iCriminal_damage_bullet_min_cash
										CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_PRICE_OF_DAMAGE - Applying min cap of ",g_sMPTunables.iCriminal_damage_bullet_min_cash)
									ENDIF
									
									IF iTotalCash > g_sMPTunables.iCriminal_damage_bullet_max_cash
										iTotalCash = g_sMPTunables.iCriminal_damage_bullet_max_cash
										CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_PRICE_OF_DAMAGE - Applying max cap of ",g_sMPTunables.iCriminal_damage_bullet_max_cash)
									ENDIF
									
									CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_PRICE_OF_DAMAGE - VEHICLE - iTotalCash = ",iTotalCash," = ",fModelValue," * ",g_sMPTunables.fCriminal_Damage_Damage_value_modifier," * ",sei.Damage)
							
								ELSE //Players
									IF GET_ENTITY_HEALTH(sei.VictimIndex) > 0 
								
										FLOAT fMaxHealth = TO_FLOAT(GET_ENTITY_MAX_HEALTH(sei.VictimIndex))
										FLOAT fDamage = FMIN(sei.Damage, fMaxHealth)
										FLOAT fMultiplier = (fDamage / fMaxHealth)
										FLOAT fTotalCash = fMultiplier * fModelValue
										iTotalCash = ROUND(g_sMPTunables.fCriminal_Damage_Player_Damage_value_modifier * fTotalCash)
										
										CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_PRICE_OF_DAMAGE - Model Value = ",fModelValue)
										CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_PRICE_OF_DAMAGE - Max Health = ",fMaxHealth)
										CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_PRICE_OF_DAMAGE - Damage = ",fDamage)
										CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_PRICE_OF_DAMAGE - Health Damage % = ",fMultiplier)
										CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_PRICE_OF_DAMAGE - Damage Cash = ",fTotalCash," = ",iTotalCash)
										
										IF iTotalCash < g_sMPTunables.icriminal_damage_player_bullet_min_cash
											iTotalCash = g_sMPTunables.icriminal_damage_player_bullet_min_cash
											CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_PRICE_OF_DAMAGE - Applying min cap of ",g_sMPTunables.icriminal_damage_player_bullet_min_cash)
										ENDIF
										
										IF iTotalCash > g_sMPTunables.icriminal_damage_player_bullet_max_cash
											iTotalCash = g_sMPTunables.icriminal_damage_player_bullet_max_cash
											CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_PRICE_OF_DAMAGE - Applying max cap of ",g_sMPTunables.icriminal_damage_player_bullet_max_cash)
										ENDIF
										
										IF fDamage = fMaxHealth
											iTotalCash = 0
											CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_PRICE_OF_DAMAGE - Not counting, damage is >= total health, should be counted in a destroyed event.",iTotalCash)
										ENDIF	
									ELSE
										iTotalCash = 0
										CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_PRICE_OF_DAMAGE - Not counting since entity <Dead> = ",iTotalCash)
									ENDIF
								ENDIF
								
								PLAY_SOUND_FRONTEND(-1,"Criminal_Damage_Low_Value","GTAO_FM_Events_Soundset", FALSE)
								CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_PRICE_OF_DAMAGE - Criminal_Damage_Low_Value")
								
								IF sei.VictimDestroyed
									FLOAT fDeadModifier
									IF IS_ENTITY_A_VEHICLE(sei.VictimIndex)
										fDeadModifier = g_sMPTunables.fCriminal_Damage_Destruction_value_modifier
									ELSE
										fDeadModifier = g_sMPTunables.fCriminal_Damage_Player_Death_value_modifier
									ENDIF
								
									iTotalCash = ROUND(fDeadModifier * fModelValue)
									
									//Play sound
									IF IS_ENTITY_A_PED(sei.VictimIndex)
										PLAY_SOUND_FRONTEND(-1,"Criminal_Damage_Kill_Player","GTAO_FM_Events_Soundset", FALSE)
										CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_PRICE_OF_DAMAGE - Criminal_Damage_Kill_Player")
									ELSE
										START_AUDIO_SCENE("GTAO_FM_Events_Blade_Scene" )
										PLAY_SOUND_FRONTEND(-1,"Criminal_Damage_High_Value","GTAO_FM_Events_Soundset", FALSE)
										CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_PRICE_OF_DAMAGE - Criminal_Damage_High_Value")
									ENDIF
									
									CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_PRICE_OF_DAMAGE - Entity Destroyed = ",iTotalCash)
								ENDIF
								
								IF iTotalCash > 0
									CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_PRICE_OF_DAMAGE - Total Cash = ",iTotalCashDamage," + ",iTotalCash)
									iTotalCashDamage += iTotalCash
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

//PROC CALCULATE_PRICE_OF_ENVIRONMENT_DAMAGE(INT iCount)
//
//	STRUCT_ENTITY_ID sei 
//	
//	IF GET_EVENT_DATA( SCRIPT_EVENT_QUEUE_AI, iCount, sei, SIZE_OF(STRUCT_ENTITY_ID) )
//		
//		iTotalCashDamage += 1234
//		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CALCULATE_PRICE_OF_ENVIRONMENT_DAMAGE - iTotalCashDamage += 1234")
//	ENDIF
//
//ENDPROC

PROC PROCESS_CRIMINAL_DAMAGE_EVENTS()

	IF NOT FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID())

		INT iEventID
		EVENT_NAMES ThisScriptEvent

		REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iEventID
		
			ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iEventID)
						
			SWITCH ThisScriptEvent			
					
				CASE EVENT_NETWORK_DAMAGE_ENTITY
					CALCULATE_PRICE_OF_DAMAGE(iEventID)
				BREAK
									
			ENDSWITCH
			
		ENDREPEAT
		
//		REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_AI) iEventID
//		
//			ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_AI, iEventID)
//						
//			SWITCH ThisScriptEvent			
//									
//				CASE EVENT_ENTITY_DESTROYED
//					CALCULATE_PRICE_OF_ENVIRONMENT_DAMAGE(iEventID)
//				BREAK
//					
//			ENDSWITCH
//		
//		ENDREPEAT
		
	ENDIF
ENDPROC

//----------------------
//	Main Functions
//----------------------

//Helper proc for setting client stage
PROC SET_LOCAL_AMBC_STAGE(AMBC_RUN_STAGE stage)
	playerBD[PARTICIPANT_ID_TO_INT()].eAMBC_Stage = stage
	CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === playerBD.eAMBC_Stage = ",GET_AMBC_STAGE_STRING(stage))
ENDPROC

//Helper proc for setting server stage
PROC SET_SERVER_AMBC_STAGE(AMBC_RUN_STAGE stage)
	serverBD.eAMBC_Stage = stage
	CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === serverBD.eAMBC_Stage = ",GET_AMBC_STAGE_STRING(stage))
ENDPROC

PROC CLIENT_MAINTAIN_PARTICIPANT_LOOP()

	IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iClientPart))
		PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iClientPart))
		IF NETWORK_IS_PLAYER_ACTIVE(playerID)
			FM_EVENT_MAINTAIN_ACTIVE_PLAYER_CHECK(playerID)
		ENDIF
	ENDIF
	
	iClientPart++
	IF iClientPart >= NETWORK_GET_MAX_NUM_PARTICIPANTS()
		iClientPart = 0
	ENDIF

ENDPROC

//Used when event is hidden
PROC CLIENT_TIDYUP()

	IF NOT IS_BIT_SET(iLocalBitset,AMBC_BS_DONE_TIDYUP)	
		CLEAR_ALL_HELP_MESSAGES()
		SET_BIT(iLocalBitset,AMBC_BS_DONE_TIDYUP)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CLIENT_TIDYUP")
	ENDIF
	
ENDPROC

/// PURPOSE: Main client processing loop
///    
PROC CLIENT_PROCESSING()

	//maintain client staggered check
	CLIENT_MAINTAIN_PARTICIPANT_LOOP()
	
	IF DRAW_CHALLENGE_DPAD_FOR_DEV_SPECTATOR()
		DRAW_CHALLENGES_DPAD_LEADERBOARD()
	ENDIF
	
	IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID())
	OR FM_EVENT_IS_PLAYER_RESTRICTED_WITH_SCTV(PLAYER_ID())
		CLEAR_PLAYER_SCORE()
	ENDIF

	IF NOT FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(PLAYER_ID())
		IF IS_IT_SAFE_TO_DISPLAY_FM_EVENT_UI() 
		
			IF IS_BIT_SET(iLocalBitset,AMBC_BS_DONE_TIDYUP)
				CLEAR_BIT(iLocalBitset,AMBC_BS_DONE_TIDYUP)
				CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === CLIENT_TIDYUP CLEAR")
			ENDIF

			SWITCH playerBD[PARTICIPANT_ID_TO_INT()].eAMBC_Stage
			
				CASE AMBC_WARN
					IF serverBD.eAMBC_Stage > AMBC_WARN
						
						FM_EVENT_SET_PLAYER_WAITING_FOR_FM_EVENT_TO_START(TRUE)
						SET_LOCAL_AMBC_STAGE(AMBC_WAIT)
					ENDIF
				BREAK
				
				CASE AMBC_WAIT
					IF NOT IS_BIT_SET(iHelpBS,2)
						IF NOT IS_RADAR_HIDDEN()
							PRINT_HELP_NO_SOUND("AMBC_WARN",DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
							SET_FREEMODE_EVENT_HELP_BACKGROUND(FALSE)
							PLAY_SOUND_FRONTEND(-1, "Event_Start_Text", "GTAO_FM_Events_Soundset", FALSE)
							SET_BIT(iHelpBS, 2)
						ENDIF
					ENDIF
					
					IF serverBD.eAMBC_Stage >= AMBC_START //Event started, go!
						SET_LOCAL_AMBC_STAGE(AMBC_START) 
					ENDIF
					HANDLE_AMBIENT_EVENT_START_COUNTDOWN_AUDIO(g_sMPTunables.iCriminal_Damage_Event_Start_Countdown_Time-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.warningTimer))
					BOTTOM_RIGHT_UI_STARTING_EVENT_TIMER(IMAX(g_sMPTunables.iCriminal_Damage_Event_Start_Countdown_Time-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.warningTimer),0), "HUD_STARTING")	
				BREAK
					
				CASE AMBC_START
					IF serverBD.eAMBC_Stage > AMBC_START
						//Init any values
						STORE_INITIAL_VALUES()
						FM_EVENT_SET_PLAYER_WAITING_FOR_FM_EVENT_TO_START(FALSE)
						COMMON_FREEMODE_AMBIENT_EVENTS_SET_UP(FMMC_TYPE_CRIMINAL_DAMAGE,0.0,0.0)
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_START,"AMBC_TITLE","AMBC_BIG_START", GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE(), DEFAULT, GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE())
						SET_LOCAL_AMBC_STAGE(AMBC_CHALLENGE)
						telemetryStruct.m_startTime = GET_CLOUD_TIME_AS_INT()
						telemetryStruct.m_notifiedTime = GET_CLOUD_TIME_AS_INT()
					ENDIF	
				BREAK 
			
				CASE AMBC_CHALLENGE
					IF serverBD.eAMBC_Stage > AMBC_CHALLENGE
						SET_LOCAL_AMBC_STAGE(AMBC_REWARDS)
					ELSE
						IF IS_BIT_SET(iLocalBitset,AMBC_BS_RESET_TRACKING)
							CLEAR_BIT(iLocalBitset,AMBC_BS_RESET_TRACKING)
						ENDIF
					
						DO_START_HELP()
						PROCESS_CRIMINAL_DAMAGE_EVENTS()
						TRACK_CHALLENGE()	
						//If someone in first that's not me, do help
						IF IS_ANYONE_IN_PLACE(0)
						AND NOT IS_LOCAL_PLAYER_IN_PLACE(0)
							SET_DO_FM_EVENT_FIRST_PLACE_HELP_IF_NEEDED()
						ENDIF
						HANDLE_CD_AUDIO()
					ENDIF
					
					CLIENT_PROCESS_SORT_FOR_LBD()
					DRAW_CHALLENGE_LEADER_BOARD()
					DRAW_CHALLENGES_DPAD_LEADERBOARD()
				BREAK 
				
				CASE AMBC_REWARDS
					IF MAINTAIN_FM_EVENTS_END_UI(sEndUiVars, NOT IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_LOW_PARTICIPANTS))
					OR IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_LOW_PARTICIPANTS)
						SET_LOCAL_AMBC_STAGE(AMBC_END)
					ENDIF
					SHOW_END_MESSAGES()
					GIVE_REWARDS()
					DRAW_CHALLENGE_LEADER_BOARD()
					DRAW_CHALLENGES_DPAD_LEADERBOARD()
					Clear_Any_Objective_Text_From_This_Script()
				BREAK
				
				CASE AMBC_END
					
				BREAK
				

			ENDSWITCH
		
		ELSE
			CLIENT_TIDYUP()
		ENDIF
	ELSE
		CLEAR_PLAYER_SCORE()
		CLIENT_TIDYUP()
	ENDIF

	HANDLE_OBJECTIVE_TEXT()
	CRIMINAL_DAMAGE_CLIENT_CALCULATE_CLOTHING_COST()

ENDPROC

/// PURPOSE: Main server processing loop
///    
PROC SERVER_PROCESSING()

	IF SHOULD_BAIL_FM_EVENT_DUE_TO_LOW_SESSION_NUMBERS(DEFAULT,DEFAULT,FALSE)
		IF NOT IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_LOW_PARTICIPANTS)
			SET_BIT(serverBD.iServerBitSet,SERVER_BS_LOW_PARTICIPANTS)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === SET_BIT(serverBD.iServerBitSet,SERVER_BS_LOW_PARTICIPANTS)")
			serverBD.iEndTime = NATIVE_TO_INT(GET_NETWORK_TIME())

		ENDIF
	ENDIF
	
	SWITCH serverBD.eAMBC_Stage
		
		CASE AMBC_WARN
			SET_SERVER_AMBC_STAGE(AMBC_WAIT)
		BREAK
		
		CASE AMBC_WAIT
			IF HAS_NET_TIMER_EXPIRED(serverBD.warningTimer, g_sMPTunables.iCriminal_Damage_Event_Start_Countdown_Time)
			OR IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_LOW_PARTICIPANTS)
			#IF IS_DEBUG_BUILD
			OR MPGlobalsAmbience.bSkipWarningDelay
				MPGlobalsAmbience.bSkipWarningDelay = FALSE
			#ENDIF
				SET_SERVER_AMBC_STAGE(AMBC_START)
			ENDIF
		BREAK
		
		CASE AMBC_START
			
			serverBD.StartTime = GET_NETWORK_TIME()
			SET_SERVER_AMBC_STAGE(AMBC_CHALLENGE)
		BREAK 
	
		CASE AMBC_CHALLENGE
			IF HAS_NET_TIMER_EXPIRED(serverBD.eventTimer,GET_CHALLENGE_TIME_LIMIT())
			OR IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_LOW_PARTICIPANTS)
			#IF IS_DEBUG_BUILD
			OR bSkipEventTime
			OR MPGlobalsAmbience.bEndCurrentFreemodeEvent
			#ENDIF
				serverBD.iEndTime = NATIVE_TO_INT(GET_NETWORK_TIME())
				SET_SERVER_AMBC_STAGE(AMBC_REWARDS)
			ENDIF
			SERVER_PROCESS_PARTICIPANT_CHECKS()
			SEND_POSITION_UPDATE_TICKERS(iTickerParticipant, iTickerWinner)
		BREAK 
		
		CASE AMBC_REWARDS
			IF HAS_NET_TIMER_EXPIRED(serverBD.rewardTimer,CH_REWARD_TIME) //Timeout
			OR serverBD.iWinnerPart[0] < 0 //No winner
			OR SERVER_HAS_GIVEN_REWARD() //Given rewards and shown end messages
				SET_SERVER_AMBC_STAGE(AMBC_END)		
			ENDIF
		BREAK
		
		CASE AMBC_END
		
		BREAK

	ENDSWITCH

ENDPROC

/// PURPOSE: Initialises the client data
///    
/// RETURNS: TRUE if initialised, FALSE otherwise
///    
FUNC BOOL INIT_CLIENT()

	//IF REQUEST_SCRIPT_AUDIO_BANK("DLC_SFX1/FRONTEND_2") 

		#IF IS_DEBUG_BUILD
		CREATE_WIDGETS()
		#ENDIF
		
		INT i
		REPEAT NUM_LEADERBOARD_PLAYERS i
			chDPStruct.challengeLbdStruct[i].playerID = INVALID_PLAYER_INDEX()
		ENDREPEAT

		RETURN TRUE
	
	//ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE: Initialises the server data
///    
/// RETURNS: TRUE if initialised, FALSE otherwise
///   
FUNC BOOL INIT_SERVER()
	
	INT i
	REPEAT NUM_LEADERBOARD_PLAYERS i
		serverBD.iWinnerPart[i] = -1
	ENDREPEAT

	RESET_NET_TIMER(serverBD.eventTimer)

	RETURN TRUE
ENDFUNC

/// PURPOSE: Help fucntion to get a debug string for the mission state
///    
/// PARAMS:
///    iState - The mission state
/// RETURNS: The name of the state
///    
FUNC STRING GET_MISSION_STATE_STRING(INT iState)

	SWITCH(iState)
		CASE GAME_STATE_INIT 				RETURN "GAME_STATE_INIT"
		CASE GAME_STATE_RUNNING 			RETURN "GAME_STATE_RUNNING"
		CASE GAME_STATE_TERMINATE_DELAY 	RETURN "GAME_STATE_TERMINATE_DELAY"
		CASE GAME_STATE_END 				RETURN "GAME_STATE_END"
	ENDSWITCH

	RETURN "UNKNOWN STATE!"

ENDFUNC

//Helper function to get a clients game/mission state
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iClientGameState
ENDFUNC

//Helper function to get the servers game/mission state
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC

//Helper function to set a clients game/mission state
PROC SET_CLIENT_MISSION_STATE(INT iPlayer,INT iState)
	playerBD[iPlayer].iClientGameState = iState
	CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === SET_CLIENT_MISSION_STATE = ",GET_MISSION_STATE_STRING(iState))
ENDPROC

//Helper function to set the servers game/mission state
PROC SET_SERVER_MISSION_STATE(INT iState)
	serverBD.iServerGameState = iState
	CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === SET_SERVER_MISSION_STATE = ",GET_MISSION_STATE_STRING(iState))
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------

PROC SCRIPT_CLEANUP()

	CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === SCRIPT_CLEANUP")
	
	
	IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		telemetryStruct.m_endReason             =AE_END_FORCED
	ELIF IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_LOW_PARTICIPANTS)
		telemetryStruct.m_endReason             =AE_END_LOW_NUMBERS
	ELSE
		IF IS_LOCAL_PLAYER_IN_PLACE(0)
			telemetryStruct.m_endReason             =AE_END_WON
		ELSE
			telemetryStruct.m_endReason             =AE_END_LOST
		ENDIF
	ENDIF
	
	FM_EVENT_SET_PLAYER_WAITING_FOR_FM_EVENT_TO_START(FALSE)
	COMMON_FREEMODE_AMBIENT_EVENTS_CLEAN_UP(FMMC_TYPE_CRIMINAL_DAMAGE,DEFAULT,(serverBD.eAMBC_Stage = AMBC_END))
	SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_CRIMINAL_DAMAGE,FALSE)
	
	telemetryStruct.m_uid0 = serverBD.iHashedMac
	telemetryStruct.m_uid1 = serverBD.iMatchHistoryId
	telemetryStruct.m_playersLeftInProgress = serverBD.iPlayersLeftInProgress
	telemetryStruct.m_playersParticipating = serverBD.iPeakQualifiers
	telemetryStruct.m_timeTakenToComplete = GET_CLOUD_TIME_AS_INT() - telemetryStruct.m_startTime
	PROCESS_CURRENT_AMBIENT_EVENT_PLAYSTATS(telemetryStruct, iLocalBestAttempt)
	
	IF !g_sMPTunables.bcriminal_damage_disable_share_cash      
		IF telemetryStruct.m_cashEarned > 0
	//		SET_LAST_JOB_DATA(LAST_JOB_BLAST, telemetryStruct.m_cashEarned)
	//		IF telemetryStruct.m_endReason             =AE_END_WON
	//			SET_DO_FM_EVENT_SHARE_CASH_HELP_IF_NEEDED()
	//		ENDIF
		ENDIF
	ENDIF
	TERMINATE_THIS_THREAD()
	
ENDPROC

FUNC BOOL SHOULD_CRIMINAL_DAMAGE_END()

	IF IS_KILL_CURRENT_FREEMODE_EVENT_GLOBAL_SET()
		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === SHOULD_CRIMINAL_DAMAGE_END - IS_KILL_CURRENT_FREEMODE_EVENT_GLOBAL_SET")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_AN_ANIMAL(PLAYER_ID())
		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === SHOULD_CRIMINAL_DAMAGE_END - IS_PLAYER_AN_ANIMAL")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)

	CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === PROCESS_PRE_GAME")
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission), missionScriptArgs)
	
	//Reserve Entities
	RESERVE_NETWORK_MISSION_OBJECTS(0)
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	COMMON_FREEMODE_AMBIENT_EVENTS_PROCESS_PRE_GAME(FMMC_TYPE_CRIMINAL_DAMAGE)
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD,SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD,SIZE_OF(playerBD))
	
	IF !Wait_For_First_Network_Broadcast()
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBD.iHashedMac, serverBD.iMatchHistoryId)
	ENDIF
	
	telemetryStruct.m_playersNotified = NETWORK_GET_TOTAL_NUM_PLAYERS()
	
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_CRIMINAL_DAMAGE,TRUE)
	
	RETURN TRUE
	
ENDFUNC

//----------------------
//	MAIN SCRIPT
//----------------------

SCRIPT(MP_MISSION_DATA missionScriptArgs)
	

	CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === START")
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NOT PROCESS_PRE_GAME(missionScriptArgs)
			SCRIPT_CLEANUP()
		ENDIF	
	ENDIF
	
	WHILE (TRUE)
	
		MP_LOOP_WAIT_ZERO()
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		OR SHOULD_CRIMINAL_DAMAGE_END()
			CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CRIMINAL_DAMAGE === SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")
			SCRIPT_CLEANUP()
		ENDIF
		
		//Call the common FM Events every frame callse
		FM_EVENT_COMMON_EVERY_FRAME_CALLS()
		
		SWITCH(GET_CLIENT_MISSION_STATE(NETWORK_PLAYER_ID_TO_INT()))
		
			CASE GAME_STATE_INIT
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					IF INIT_CLIENT()
						SET_CLIENT_MISSION_STATE(NETWORK_PLAYER_ID_TO_INT(),GAME_STATE_RUNNING)
					ENDIF
				ENDIF
			BREAK
			
			CASE GAME_STATE_RUNNING
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					CLIENT_PROCESSING()
					
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					#IF IS_DEBUG_BUILD OR bEndScriptLocal #ENDIF
					
					SET_CLIENT_MISSION_STATE(NETWORK_PLAYER_ID_TO_INT(),GAME_STATE_END)
				ENDIF
			BREAK
						
			CASE GAME_STATE_END
				SCRIPT_CLEANUP()
			BREAK
			
		ENDSWITCH
	
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
			SWITCH(GET_SERVER_MISSION_STATE())
			
				CASE GAME_STATE_INIT
					IF INIT_SERVER()
						SET_SERVER_MISSION_STATE(GAME_STATE_RUNNING)
					ENDIF
				BREAK
				
				CASE GAME_STATE_RUNNING
					IF serverBD.eAMBC_Stage = AMBC_END
					#IF IS_DEBUG_BUILD 
					OR bEndScript
					OR MPGlobalsAmbience.bKillActiveEvent
						MPGlobalsAmbience.bKillActiveEvent = FALSE
						#ENDIF
						SET_SERVER_MISSION_STATE(GAME_STATE_TERMINATE_DELAY)
					ELSE
						SERVER_PROCESSING()
					ENDIF
				BREAK
				
				CASE GAME_STATE_TERMINATE_DELAY
					IF HAS_NET_TIMER_EXPIRED(iServerTerminateTimer,SERVER_TERMINATE_DELAY)					
						SET_SERVER_MISSION_STATE(GAME_STATE_END)
					ENDIF
				BREAK
				
				CASE GAME_STATE_END
					SCRIPT_CLEANUP()
				BREAK
				
			ENDSWITCH
		
		ENDIF
		
	ENDWHILE
	

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT

