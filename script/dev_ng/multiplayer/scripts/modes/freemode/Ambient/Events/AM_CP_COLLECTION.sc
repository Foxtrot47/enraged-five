//////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_CP_COLLECTION.sc														//
// Description: Checkpoints located across the map. Players collect for a cash reward.	//
// Written by:  Martin McMillan															//
// Date: 09/03/2015																		//
//////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "net_mission.sch"



//----------------------
//	INCLUDES
//----------------------
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "flow_public_core_override.sch"
USING "net_wait_zero.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"
USING "net_ambience_launcher.sch"
USING "net_checkpoints.sch"
USING "DM_Leaderboard.sch"
USING "net_challenges.sch"
USING "am_common_ui.sch"
USING "freemode_events_header.sch"
USING "chase_hint_cam.sch"
USING "net_rank_ui.sch"
//----------------------
//	ENUM
//----------------------

//----------------------
//	CONSTANTS
//----------------------

CONST_FLOAT CHECKPOINT_RADIUS			2.5
CONST_FLOAT CHECKPOINT_RADIUS_HEIGHT	3.0
CONST_FLOAT CHECKPOINT_Z_POS			2.8
CONST_FLOAT CHECKPOINT_SIZE				5.0
CONST_FLOAT CHECKPOINT_CYLINDER			5.0

CONST_FLOAT CHECKPOINT_RADIUS_AIR			6.0
CONST_FLOAT CHECKPOINT_RADIUS_HEIGHT_AIR	6.0
CONST_FLOAT CHECKPOINT_Z_POS_AIR			4.0
CONST_FLOAT CHECKPOINT_SIZE_AIR				10.0
CONST_FLOAT CHECKPOINT_CYLINDER_AIR			7.5

CONST_INT RESEND_EVENT_DELAY			250
CONST_INT SERVER_PROCESS_WINNER_DELAY	750
CONST_INT SERVER_TERMINATE_DELAY		1000
//CONST_INT CPC_TIME_LIMIT			600000 //10 mins g_sMPTunables.iCheckpoint_Event_Time_Limit
CONST_INT CPC_START_EVENT_DELAY			750
//CONST_INT CPC_COUNTDOWN_TIME				180000 g_sMPTunables.iCheckpoint_Event_Start_Countdown_Time
CONST_INT VEHICLE_EXPLOSION_TIME		30000

CONST_INT MAX_CHECKPOINT_ARRAY_SIZE		130

CONST_INT NUM_CHECKPOINT_PER_GROUP		20
CONST_INT ACTIVE_CHECKPOINT_GROUPS		3

CONST_INT TOTAL_CHECKPOINT_GROUPS		23
CONST_INT LAND_CHECKPOINT_GROUPS		15
CONST_INT AIR_CHECKPOINT_GROUPS			4
CONST_INT SEA_CHECKPOINT_GROUPS			4

CONST_INT CP_MESSAGE_TIME				20000

CONST_INT CHECKPOINT_PROCESSING_RANGE	500
CONST_INT CHECKPOINT_DRAW_RANGE			750

CONST_INT NUM_LEADERBOARD_PLAYERS		NUM_NETWORK_PLAYERS

CONST_INT NUM_CHECKPOINT_AREAS				4
CONST_INT NUM_CHECKPOINT_GROUPS_PER_AREA	1

CONST_INT NUM_WATER_CHECKPOINTS			8

CONST_INT NUM_MINI_LEADERBOARD_PLAYERS	3

CONST_INT NUM_VEHICLES_TO_SPAWN			10
CONST_INT VEH_EXP_RANGE					50

//----------------------
//	Local Bitset
//----------------------

CONST_INT AMCP_BS_STARTED_COUNTDOWN		0
CONST_INT AMCP_BS_SHOWN_START_HELP 		1
CONST_INT AMCP_BS_SWAPPED_MAP			2
CONST_INT AMCP_BS_DONE_TIDYUP			3
CONST_INT AMCP_BS_LOCAL_PLAYER_FIRST	4
CONST_INT AMCP_BS_INTRO_SHARD_DISPLAYED	5

//----------------------
//	STRUCT
//----------------------

//Focus cam struct
//STRUCT LOOK_AT_CHECKPOINT_STRUCT
//	CHASE_HINT_CAM_STRUCT 		s_CheckPointChaseCam
//	INT							iCheckpointClosest
//	INT							tempClosest
//	INT							iLoopCount
//	FLOAT						fCurrentClosestDist
//ENDSTRUCT
//LOOK_AT_CHECKPOINT_STRUCT sLookCheckpoint

STRUCT LOCAL_CHECKPOINT_DATA

	BLIP_INDEX cpBlip
	CHECKPOINT_INDEX checkpointObj
	INT iAlpha = 0 //url:bugstar:2414057
	
ENDSTRUCT

STRUCT SERVER_CHECKPOINT_DATA

	VECTOR vLocation
	INT iCollectedBy = -1
	INT iCheckpointIndex //Unique identifier
	
ENDSTRUCT

STRUCT CHALLENGE_VEHICLE

	MODEL_NAMES vehModel
	NETWORK_INDEX niVeh
	
ENDSTRUCT

//----------------------
//	VARIABLES
//----------------------

INT iCheckpointStagger = 0
INT iServerCheckpointStagger = 0

INT iStartTime 
INT iSpawningVehicle = 0
INT iLocalBitset

BOOL bCheckpointInRange[MAX_CHECKPOINT_ARRAY_SIZE]
BOOL bCheckpointInDrawingRange[MAX_CHECKPOINT_ARRAY_SIZE]

INT iCheckpointUsedBS
INT iClientPart

INT iCheckpointReachedTime

INT iLocalCheckpointCollectedBS[CHECKPOINT_BS_SIZE]
INT iLocalRewardGivenBS[CHECKPOINT_BS_SIZE]

INT iLocalCheckpointCreated[CHECKPOINT_BS_SIZE]

INT iTotalCashEarned

//INT iCountdownSound
INT iAreaSelection = 0

INT iLocalVehicle = 0

SCRIPT_TIMER iResendEventTimer

SCRIPT_TIMER iServerProcessWinnerTimer

SCRIPT_TIMER iServerTerminateTimer

LOCAL_CHECKPOINT_DATA localCheckpointData[MAX_CHECKPOINT_ARRAY_SIZE]

INT iHelpBS

BOOL bRenderAll

BOOL bSetSpawnLocations = FALSE

STRUCT_FM_EVENTS_END_UI sEndUiVars

BLIP_INDEX vehBlip[NUM_VEHICLES_TO_SPAWN]

//SERVER ONLY

INT iExplodeBS
INT iExplodeSOund[NUM_VEHICLES_TO_SPAWN]

BOOL bDrawTimer = FALSE
BOOL bShouldDrawTimer

STRUCT CHECK_POINT_CACHE_EVENT
	INT iCheckpoint	 = -1
	INT iCheckpointTime = -1
ENDSTRUCT
CHECK_POINT_CACHE_EVENT sCaheEvent
//----------------------
//	GAME STATE
//----------------------

CONST_INT GAME_STATE_INIT 									0
CONST_INT GAME_STATE_RUNNING								1
CONST_INT GAME_STATE_TERMINATE_DELAY						2
CONST_INT GAME_STATE_END									3

ENUM AMCP_RUN_STAGE
	AMCP_WARN = 0,
	AMCP_WAIT,
	AMCP_START,
	AMCP_COLLECT,
	AMCP_MESSAGE,
	AMCP_EXPLODE,
	AMCP_END
ENDENUM


//----------------------
//	BROADCAST VARIABLES
//----------------------

CONST_INT SERVER_BS_ALL_CP_COLLECTED		0
CONST_INT SERVER_BS_SUDDEN_DEATH			1
CONST_INT SERVER_BS_LOW_PARTICIPANTS		2

// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData

	INT iServerGameState = GAME_STATE_INIT
	
	INT iEndTime
	
	INT iServerBitSet
	
	INT iCheckpointCollectedBS[CHECKPOINT_BS_SIZE]
	
	AMCP_RUN_STAGE eAMCP_Stage = AMCP_WARN
	
	TIME_DATATYPE StartTime

	SERVER_CHECKPOINT_DATA serverCheckpointData[MAX_CHECKPOINT_ARRAY_SIZE]
		
	SCRIPT_TIMER eventTimer
	SCRIPT_TIMER messageTimer
	SCRIPT_TIMER startTimer
	SCRIPT_TIMER warningTimer
	SCRIPT_TIMER ExplodeVehTimer
	SCRIPT_TIMER allCollectedTimer
	
	INT iActivePartBS
	
	INT iWinnerPart[NUM_LEADERBOARD_PLAYERS]
	
	BOOL bAir
	
	INT iRemaining
	INT iNumCheckpoints
	
	BOOL bOpenAirportGates
	
	INT iNumMatchingPlayers
	
	INT iCheckpointArea
	INT iClosestPlayer[NUM_VEHICLES_TO_SPAWN]
	
	INT iHashedMac 
	INT iMatchHistoryId
	INT iPlayersLeftInProgress 
	INT iPeakParticipants
	INT iPeakQualifiers
	
	CHALLENGE_VEHICLE vehiclesToSpawn[NUM_VEHICLES_TO_SPAWN]
	
ENDSTRUCT

ServerBroadcastData serverBD
CHALLENGE_DPAD_STRUCT chDPStruct
scrFmEventAmbientMission telemetryStruct

CONST_INT CLIENT_BS_SHOWN_MESSAGE			0
CONST_INT CLIENT_BS_GIVEN_END_REWARD		1
CONST_INT CLIENT_BS_PARTICIPATED_PERMANENT	2

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	
	INT iClientGameState = GAME_STATE_INIT
	
	INT iVehSpawnBitSet
	
	int iClientBitSet
	
	AMCP_RUN_STAGE eAMCP_Stage = AMCP_WARN
	
	INT iRewardCounter
	
ENDSTRUCT

PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

//----------------------
//	DBG FUNCTIONS
//----------------------

#IF IS_DEBUG_BUILD

BOOL bEndScript
BOOL bEndScriptLocal
BOOL bDebugDrawMapSpawnPoints
BOOL bSkipEventTime
INT iCurrentJCheckpoint = 0

BOOL bDebugBail

BOOL bShowCentre

STRUCT DEBUG_MAP_SPAWN
	BLIP_INDEX left
	BLIP_INDEX right

ENDSTRUCT

DEBUG_MAP_SPAWN bDebugMapSpawnPoints[NUM_CHECKPOINT_AREAS]

FLOAT fAreaSize = 3000

/// PURPOSE: Creates the debug widgets
///    
PROC CREATE_WIDGETS()

	START_WIDGET_GROUP("Checkpoint Collection")
		START_WIDGET_GROUP("Server Only")
			ADD_WIDGET_BOOL("bSkipEventTime",bSkipEventTime)
			ADD_WIDGET_BOOL("bDebugBail",bDebugBail)
			ADD_WIDGET_BOOL("End Script",bEndScript)
		STOP_WIDGET_GROUP()
		ADD_WIDGET_BOOL("Display Spawn Areas",bDebugDrawMapSpawnPoints)
		ADD_WIDGET_BOOL("bShowCentre",bShowCentre)
		ADD_WIDGET_BOOL("End Script Locally",bEndScriptLocal)
	STOP_WIDGET_GROUP()
	
	CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION DEBUG ===  CREATED WIDGETS ")
	
ENDPROC

/// PURPOSE: Processes any debug that needs to be maintained
///    
PROC PROCESS_DEBUG()

	IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(PLAYER_ID())
		EXIT
	ENDIF
	IF GB_IS_PLAYER_ON_ISLAND_HEIST_PREP_MISSION(PLAYER_ID())
		EXIT
	ENDIF

	INT iArea

	IF bDebugDrawMapSpawnPoints
		VECTOR vBottomLeft,vTopRight
		
		REPEAT NUM_CHECKPOINT_AREAS iArea
			IF NOT DOES_BLIP_EXIST(bDebugMapSpawnPoints[iArea].left)
				IF GET_RECTANGLE_VECTORS_FOR_CHECKPOINT_AREA(iArea,vBottomLeft,vTopRight)
					VECTOR vCentre = <<((vBottomLeft.x + vTopRight.x) / 2),((vBottomLeft.y + vTopRight.y) / 2),0.0>>
					//fAreaSize = ABSF(vBottomLeft.x - vTopRight.x) * ABSF(vBottomLeft.y - vTopRight.y)
					bDebugMapSpawnPoints[iArea].left = ADD_BLIP_FOR_COORD(vBottomLeft)
					bDebugMapSpawnPoints[iArea].right = ADD_BLIP_FOR_COORD(vTopRight)
					SWITCH(iArea)
						CASE 0 SET_BLIP_COLOUR_FROM_HUD_COLOUR(bDebugMapSpawnPoints[iArea].left,HUD_COLOUR_BLUE)
								SET_BLIP_COLOUR_FROM_HUD_COLOUR(bDebugMapSpawnPoints[iArea].right,HUD_COLOUR_BLUE)
						BREAK
						CASE 1 SET_BLIP_COLOUR_FROM_HUD_COLOUR(bDebugMapSpawnPoints[iArea].left,HUD_COLOUR_YELLOW)
								SET_BLIP_COLOUR_FROM_HUD_COLOUR(bDebugMapSpawnPoints[iArea].right,HUD_COLOUR_YELLOW)
						BREAK
						CASE 2 SET_BLIP_COLOUR_FROM_HUD_COLOUR(bDebugMapSpawnPoints[iArea].left,HUD_COLOUR_RED)
								SET_BLIP_COLOUR_FROM_HUD_COLOUR(bDebugMapSpawnPoints[iArea].right,HUD_COLOUR_RED)
						BREAK
						CASE 3 SET_BLIP_COLOUR_FROM_HUD_COLOUR(bDebugMapSpawnPoints[iArea].left,HUD_COLOUR_GREEN)
								SET_BLIP_COLOUR_FROM_HUD_COLOUR(bDebugMapSpawnPoints[iArea].right,HUD_COLOUR_GREEN)
						BREAK
					ENDSWITCH
					SET_BLIP_ALPHA(bDebugMapSpawnPoints[iArea].left,80)
					SET_BLIP_ALPHA(bDebugMapSpawnPoints[iArea].right,80)
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION DEBUG ===  Adding spawn point area blip ",iArea," of size ",fAreaSize," at ",vCentre)
				ENDIF
			ENDIF
		ENDREPEAT
	ELSE
		IF DOES_BLIP_EXIST(bDebugMapSpawnPoints[0].left)
			REPEAT NUM_CHECKPOINT_AREAS iArea
				IF DOES_BLIP_EXIST(bDebugMapSpawnPoints[iArea].left)
					REMOVE_BLIP(bDebugMapSpawnPoints[iArea].left)
					REMOVE_BLIP(bDebugMapSpawnPoints[iArea].right)
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION DEBUG ===  Removed spawn point area blip ",iArea)
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
	//J Skip
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
		IF iCurrentJCheckpoint < serverBD.iNumCheckpoints
			VECTOR vJ = GET_VECTOR_FROM_CHECKPOINT_INDEX(serverBD.iCheckpointArea,serverBD.bAir,0,iCurrentJCheckpoint)
			NET_WARP_TO_COORD(vJ,0.0,TRUE)
			iCurrentJCheckpoint++
		ENDIF
	ENDIF
	
	IF bShowCentre
		IF NOT DOES_BLIP_EXIST(bDebugMapSpawnPoints[0].left)
			bDebugMapSpawnPoints[0].left = ADD_BLIP_FOR_COORD(GET_MIDPOINT_VECTOR_FOR_CHECKPOINT_AREA(serverBD.iCheckpointArea))
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(bDebugMapSpawnPoints[0].left)
			REMOVE_BLIP(bDebugMapSpawnPoints[0].left)
		ENDIF
	ENDIF

ENDPROC

#ENDIF

//----------------------
//	FUNCTIONS
//----------------------

/// PURPOSE: Helper function to get debug string for stage
///    
/// PARAMS: 
///    stage - The stage 
/// RETURNS: The name of the stage
///    
FUNC STRING GET_AMCP_STAGE_STRING(AMCP_RUN_STAGE stage)

	SWITCH(stage)
		CASE AMCP_WARN				RETURN "AMCP_WARN"
		CASE AMCP_WAIT				RETURN "AMCP_WAIT"
		CASE AMCP_START				RETURN "AMCP_START"
		CASE AMCP_COLLECT			RETURN "AMCP_COLLECT"
		CASE AMCP_MESSAGE			RETURN "AMCP_MESSAGE"
		CASE AMCP_EXPLODE			RETURN "AMCP_EXPLODE"
		CASE AMCP_END 				RETURN "AMCP_END"
	ENDSWITCH

	RETURN "UNKNOWN STAGE!"
	
ENDFUNC

///// PURPOSE: Check if a checkpoint is in the air
/////    
///// PARAMS:
/////    iCheckpointIndex - The unique checkpoint index
///// RETURNS: TRUE if an air checkpoint, FALSE otherwise
/////    
//FUNC BOOL IS_WATER_CHECKPOINT(INT iCheckpointIndex)
//
//	RETURN iCheckpointIndex >= LAND_CHECKPOINT_GROUPS+AIR_CHECKPOINT_GROUPS AND iCheckpointIndex < LAND_CHECKPOINT_GROUPS+AIR_CHECKPOINT_GROUPS+SEA_CHECKPOINT_GROUPS
//
//ENDFUNC
//
///// PURPOSE: Check if a checkpoint is in the water
/////    
///// PARAMS:
/////    iCheckpointIndex - The unique checkpoint index
///// RETURNS: TRUE if an water checkpoint, FALSE otherwise
/////    
//FUNC BOOL IS_AIR_CHECKPOINT(INT iCheckpointIndex)
//
//	RETURN iCheckpointIndex >= LAND_CHECKPOINT_GROUPS AND iCheckpointIndex < LAND_CHECKPOINT_GROUPS+AIR_CHECKPOINT_GROUPS
//
//ENDFUNC

/// PURPOSE: Check if a checkpoint is in the air
///    
/// PARAMS:
///    iCheckpointIndex - The unique checkpoint index
/// RETURNS: TRUE if an air checkpoint, FALSE otherwise
///    
FUNC BOOL IS_AIR_CHECKPOINT()

	RETURN serverBD.bAir

ENDFUNC

/// PURPOSE: Check if a checkpoint is in the water
///    
/// PARAMS:
///    iCheckpointIndex - The unique checkpoint index
/// RETURNS: TRUE if an water checkpoint, FALSE otherwise
///    
FUNC BOOL IS_WATER_CHECKPOINT(INT iCheckpointIndex)

	IF NOT IS_AIR_CHECKPOINT()

		RETURN iCheckpointIndex >= (serverBD.iNumCheckpoints - NUM_WATER_CHECKPOINTS) 
		
	ELSE
	
		RETURN FALSE
	ENDIF

ENDFUNC


/// PURPOSE: Helper function to swap the values of two variables
///    
/// PARAMS:
///    iNum1
///    iNum2
PROC SWAP(INT &iNum1, INT &iNum2)
	
	CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === SWAP ",iNum1," AND ",iNum2)

	iNum1 = iNum1 + iNum2
	iNum2 = iNum1 - iNum2
	iNum1 = iNum1 - iNum2

ENDPROC

FUNC BOOL IS_SCORE_BETTER_THAN_SCORE(INT iScore1, INT iScore2)

	RETURN iScore1 > iScore2

ENDFUNC

PROC SET_AS_PERMANENT_PARTICIPANT()

	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_PARTICIPATED_PERMANENT)
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_PARTICIPATED_PERMANENT)
		FM_EVENT_SET_PLAYER_IS_PERMANENT_PARTICIPANT_IN_FM_EVENT(TRUE)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === SET_AS_PERMANENT_PARTICIPANT")
	ENDIF
	
ENDPROC

//----------------------
//	Client Functions
//----------------------

FUNC BOOL SHOULD_RENDER_ALL_CHECKPOINT_BLIPS_THIS_FRAME()

	IF IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())
		IF NOT IS_BIT_SET(iLocalBitset,AMCP_BS_SWAPPED_MAP)
			SET_BIT(iLocalBitset,AMCP_BS_SWAPPED_MAP)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === SWAPPED map to big")
			RETURN TRUE
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBitset,AMCP_BS_SWAPPED_MAP)
			CLEAR_BIT(iLocalBitset,AMCP_BS_SWAPPED_MAP)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === SWAPPED map to small")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC CHECKPOINT_FLASH_FADE(CHECKPOINT_INDEX &checkpoint,INT &iPrevAlpha, BLIP_INDEX &blip,INT iCheckpoint)
	
	INT iR, iG, iB, iA
	IF checkpoint != NULL
		iPrevAlpha -= 25
	    IF iPrevAlpha > 0
	        GET_HUD_COLOUR(HUD_COLOUR_WHITE, iR, iG, iB, iA)
	        SET_CHECKPOINT_RGBA(checkpoint,iR, iG, iB, iPrevAlpha)
			SET_CHECKPOINT_RGBA2(checkpoint,iR, iG, iB, iPrevAlpha)
	    ELSE
			IF DOES_BLIP_EXIST(blip)
				REMOVE_BLIP(blip)
			ENDIF
	       	DELETE_CHECKPOINT(checkpoint)
			checkpoint = NULL
			CLEAR_BIT(iLocalCheckpointCreated[GET_LONG_BITSET_INDEX(iCheckpoint)],GET_LONG_BITSET_BIT(iCheckpoint))
	    ENDIF
	ELSE
		PRINTLN("[JS] - FLASH FADE - CHECKPOINT NULL")
	ENDIF
ENDPROC

FUNC BOOL CHECKPOINT_FLASH_FADE_IN(CHECKPOINT_INDEX &checkpoint, INT &iNextAlpha)
	INT iR, iG, iB, iA
    IF iNextAlpha < 175
		iNextAlpha += 10
        GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
        SET_CHECKPOINT_RGBA(checkpoint,iR, iG, iB, iNextAlpha)
		SET_CHECKPOINT_RGBA2(checkpoint,iR, iG, iB, iNextAlpha)
		RETURN FALSE
	ELSE
		RETURN TRUE
    ENDIF
		PRINTLN("[JS] FADE IN - something broke")
	RETURN TRUE
ENDFUNC

FUNC BOOL CHECKPOINT_FLASH_FADE_OUT(CHECKPOINT_INDEX &checkpoint, INT &iNextAlpha)
	INT iR, iG, iB, iA
	iNextAlpha -= 10
    IF iNextAlpha > 0
        GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
        SET_CHECKPOINT_RGBA(checkpoint,iR, iG, iB, iNextAlpha)
		SET_CHECKPOINT_RGBA2(checkpoint,iR, iG, iB, iNextAlpha)
		RETURN FALSE
	ELSE
		RETURN TRUE
    ENDIF
		PRINTLN("[JS] FADE OUT - something broke")
	RETURN TRUE
ENDFUNC

PROC DRAW_CHECKPOINT_LIGHTS(VECTOR &vLocation)

	INT iR, iG, iB
	iR = 239
	iG = 250
	iB = 187
	
	FLOAT fIntensity = 5.0
	
	FLOAT fFallOffDist	
	FLOAT fFallOffExp
	
	fFallOffExp = 64.00
	
	IF IS_AIR_CHECKPOINT()
		fFallOffDist = 40.00	
	ELSE
		fFallOffDist = 15.00	
	ENDIF

	IF NOT IS_VECTOR_ZERO(vLocation)
		vLocation += MATC_CORONA_LIGHTS_ABOVE_GROUND_m
		DRAW_LIGHT_WITH_RANGEEX(vLocation, iR, iG, iB, fFallOffDist, fIntensity, fFallOffExp)
	ENDIF
ENDPROC

/// PURPOSE: Maintain the drawing of blips and checkpoints
///    
/// PARAMS:
///    currentCheckpoint - Contains the local checkpoint objects 
///    serverCheckpoint - Contains the server checkpoint data
///    iCheckpoint - The current checkpoint index
PROC DRAW_CHECKPOINTS(LOCAL_CHECKPOINT_DATA &currentCheckpoint,SERVER_CHECKPOINT_DATA serverCheckpoint,INT iCheckpoint)
	
	VECTOR vDrawPosition = serverCheckpoint.vLocation

	INT iR, iG, iB, iA
	GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
	
	//If checkpoint has not been collected by local player and has not been collected by anyone else
	IF NOT IS_BIT_SET(iLocalCheckpointCollectedBS[GET_LONG_BITSET_INDEX(iCheckpoint)],GET_LONG_BITSET_BIT(iCheckpoint))
	AND NOT IS_BIT_SET(serverBD.iCheckpointCollectedBS[GET_LONG_BITSET_INDEX(iCheckpoint)],GET_LONG_BITSET_BIT(iCheckpoint))
		
		//Manage Blips
		IF NOT DOES_BLIP_EXIST(currentCheckpoint.cpBlip)
			IF NOT ARE_VECTORS_EQUAL(vDrawPosition, <<0, 0, 0>>)
			AND NOT ARE_VECTORS_EQUAL(vDrawPosition, <<0, 0, -2000>>)
				
				currentCheckpoint.cpBlip = ADD_BLIP_FOR_COORD(vDrawPosition)
				SET_BLIP_SPRITE(currentCheckpoint.cpBlip, RADAR_TRACE_TEMP_3)
				SET_BLIP_PRIORITY(currentCheckpoint.cpBlip, BLIPPRIORITY_HIGHEST)
				SET_BLIP_NAME_FROM_TEXT_FILE(currentCheckpoint.cpBlip, "CPC_BLIP")
				SET_BLIP_ALPHA_BASED_ON_DIST_FROM_PLAYER(currentCheckpoint.cpBlip,25)
				SET_BLIP_COLOUR_FROM_HUD_COLOUR(currentCheckpoint.cpBlip,HUD_COLOUR_YELLOW)
				SHOW_HEIGHT_ON_BLIP(currentCheckpoint.cpBlip,TRUE)
				CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION ===  BLIP ADDED AT ",vDrawPosition)
			ENDIF
		ELSE
			//Fade blips based on range
			IF IS_PAUSE_MENU_ACTIVE()
				SET_BLIP_ALPHA(currentCheckpoint.cpBlip,255)
			ELSE
				SET_BLIP_ALPHA_BASED_ON_DIST_FROM_PLAYER(currentCheckpoint.cpBlip,25)
			ENDIF
		ENDIF
		
		//Manage Checkpoints
		IF bCheckpointInDrawingRange[iCheckpoint]	
			IF NOT IS_BIT_SET(iLocalCheckpointCreated[GET_LONG_BITSET_INDEX(iCheckpoint)],GET_LONG_BITSET_BIT(iCheckpoint))
				CHECKPOINT_TYPE checkpointType
				checkpointType = CHECKPOINT_MONEY

				IF IS_AIR_CHECKPOINT()
					//Create air checkpoint
					currentCheckpoint.checkpointObj = CREATE_CHECKPOINT(checkpointType, vDrawPosition+<<0.0, 0.0, CHECKPOINT_Z_POS_AIR>>, vDrawPosition, CHECKPOINT_SIZE_AIR ,iR, iG, iB, currentCheckpoint.iAlpha)
					SET_CHECKPOINT_CYLINDER_HEIGHT(currentCheckpoint.checkpointObj, CHECKPOINT_CYLINDER_AIR, CHECKPOINT_CYLINDER_AIR, 100)
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION ===  IS_AIR_CHECKPOINT ",vDrawPosition)
				ELSE
					//Create ground checkpoint
					currentCheckpoint.checkpointObj = CREATE_CHECKPOINT(checkpointType, vDrawPosition+<<0.0, 0.0, CHECKPOINT_Z_POS>>, vDrawPosition, CHECKPOINT_SIZE ,iR, iG, iB, currentCheckpoint.iAlpha)
					SET_CHECKPOINT_CYLINDER_HEIGHT(currentCheckpoint.checkpointObj, CHECKPOINT_CYLINDER, CHECKPOINT_CYLINDER, 100)
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION ===  NOT IS_AIR_CHECKPOINT ",vDrawPosition)
				ENDIF
				SET_BIT(iLocalCheckpointCreated[GET_LONG_BITSET_INDEX(iCheckpoint)],GET_LONG_BITSET_BIT(iCheckpoint))
				CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION ===  CHECKPOINT ADDED AT ",vDrawPosition)
			ELSE
				//Fade in checkpoint
				CHECKPOINT_FLASH_FADE_IN(currentCheckpoint.checkpointObj,currentCheckpoint.iAlpha)
			ENDIF
			
			DRAW_CHECKPOINT_LIGHTS(serverCheckpoint.vLocation)
		ELSE
			//Checkpoint not in drawing range, remove it
			IF IS_BIT_SET(iLocalCheckpointCreated[GET_LONG_BITSET_INDEX(iCheckpoint)],GET_LONG_BITSET_BIT(iCheckpoint))
				IF CHECKPOINT_FLASH_FADE_OUT(currentCheckpoint.checkpointObj,currentCheckpoint.iAlpha)
					DELETE_CHECKPOINT(currentCheckpoint.checkpointObj)
					CLEAR_BIT(iLocalCheckpointCreated[GET_LONG_BITSET_INDEX(iCheckpoint)],GET_LONG_BITSET_BIT(iCheckpoint))
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION ===  CHECKPOINT REMOVED AT ",vDrawPosition)
				ENDIF
			ENDIF
		ENDIF
		
	//Checkpoint has been collected
	ELSE
		IF IS_BIT_SET(iLocalCheckpointCreated[GET_LONG_BITSET_INDEX(iCheckpoint)],GET_LONG_BITSET_BIT(iCheckpoint))
			//Fade out checkpoint and delete when faded
			CHECKPOINT_FLASH_FADE(currentCheckpoint.checkpointObj,currentCheckpoint.iAlpha,currentCheckpoint.cpBlip,iCheckpoint)
		ELSE
			//Remove blip
			IF DOES_BLIP_EXIST(currentCheckpoint.cpBlip)
				REMOVE_BLIP(currentCheckpoint.cpBlip)
			ENDIF
		ENDIF
		CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === BLIP AND CHECKPOINT REMOVED - CHECKPOINT COLLECTED at ",vDrawPosition)
	ENDIF
		
ENDPROC

FUNC STRING GET_LABEL_FOR_QUADRANT()
	SWITCH serverBD.iCheckpointArea
		CASE CHECK_QUADRANT_SOUTHEAST
			RETURN "CPC_WARN0"
		BREAK
		CASE CHECK_QUADRANT_SOUTHWEST
			RETURN "CPC_WARN1"
		BREAK
		CASE CHECK_QUADRANT_NORTHWEST
			RETURN "CPC_WARN2"
		BREAK
		CASE CHECK_QUADRANT_NORTHEAST
			RETURN "CPC_WARN3"
		BREAK
	ENDSWITCH
	RETURN ""
ENDFUNC

PROC DO_HELP_TEXT_AND_MESSAGES(INT iHelp)
	
	IF NOT IS_RADAR_HIDDEN()
		SWITCH iHelp
			CASE 0 
				IF NOT IS_BIT_SET(iHelpBS, iHelp)
					IF NOT IS_PAUSE_MENU_ACTIVE()
						PRINT_HELP("CPC_PASSIVE",DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) //Unavailable in passive mode
						SET_FREEMODE_EVENT_HELP_BACKGROUND()
						SET_BIT(iHelpBS, iHelp)
						CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === DO_HELP_TEXT_AND_MESSAGES - 0")
					ENDIF
				ENDIF
			BREAK
			CASE 1
				IF NOT IS_BIT_SET(iHelpBS, iHelp)
					IF NOT IS_PAUSE_MENU_ACTIVE()
						PRINT_HELP("CPC_NOAIR",DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) //Unavailable in aircraft
						SET_FREEMODE_EVENT_HELP_BACKGROUND()
						SET_BIT(iHelpBS, iHelp)
						CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === DO_HELP_TEXT_AND_MESSAGES - 1")
					ENDIF
				ENDIF
			BREAK

			CASE 2
				IF NOT IS_BIT_SET(iHelpBS, iHelp)
					IF NOT IS_PAUSE_MENU_ACTIVE()
					AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					AND IS_AIR_CHECKPOINT()
						PRINT_HELP("AMCH_AIRAV",DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) //Aircraft are available
						SET_FREEMODE_EVENT_HELP_BACKGROUND()
						SET_BIT(iHelpBS, iHelp)
						CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === DO_HELP_TEXT_AND_MESSAGES - 2")
					ENDIF
				ENDIF
			BREAK

			CASE 3
				IF NOT IS_BIT_SET(iHelpBS, iHelp)
					IF NOT IS_PAUSE_MENU_ACTIVE()
						PRINT_HELP("AMCH_BLOW",DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) //Aircraft will self-destruct
						SET_FREEMODE_EVENT_HELP_BACKGROUND()
						SET_BIT(iHelpBS, iHelp)
						CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === DO_HELP_TEXT_AND_MESSAGES - 3")
					ENDIF
				ENDIF
			BREAK

			CASE 4
				IF NOT IS_BIT_SET(iHelpBS, iHelp)
					IF NOT IS_PAUSE_MENU_ACTIVE()
					AND NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_FM_EVENT_START)
						STRING sHelp
						IF serverBD.bAir
							sHelp = "CPC_HELPA"
						ELSE
							sHelp = "CPC_HELP"
						ENDIF		
						PRINT_HELP(sHelp,DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
						SET_FREEMODE_EVENT_HELP_BACKGROUND()
						SET_BIT(iHelpBS, iHelp)
						CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === DO_HELP_TEXT_AND_MESSAGES - 4")
					ENDIF
				ENDIF
			BREAK
			CASE 5
				IF NOT IS_BIT_SET(iHelpBS, iHelp)
					IF NOT IS_PAUSE_MENU_ACTIVE()
					AND NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_FM_EVENT_START)
					AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					AND IS_BIT_SET(iHelpBS, 4)
						STRING sHelp
						IF serverBD.bAir
							sHelp = "CPC_HELPA2"
						ELSE
							sHelp = "CPC_HELP2"
						ENDIF		
						PRINT_HELP(sHelp,DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
						SET_FREEMODE_EVENT_HELP_BACKGROUND()
						SET_BIT(iHelpBS, iHelp)
						CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === DO_HELP_TEXT_AND_MESSAGES - 5")
					ENDIF
				ENDIF
			BREAK
			CASE 6
				IF NOT IS_BIT_SET(iHelpBS, iHelp)
					IF NOT IS_PAUSE_MENU_ACTIVE()
					AND NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_FM_EVENT_START)
					AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					AND IS_BIT_SET(iHelpBS, 5)
						STRING sWarn
						IF serverBD.bAir
							sWarn = "CPC_WARNA"
						ELSE
							sWarn = "CPC_WARN"
						ENDIF
						
						PRINT_HELP_WITH_STRING_NO_SOUND(sWarn, GET_LABEL_FOR_QUADRANT(), DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
						SET_FREEMODE_EVENT_HELP_BACKGROUND()
						PLAY_SOUND_FRONTEND(-1, "Event_Start_Text", "GTAO_FM_Events_Soundset", FALSE)
						SET_BIT(iHelpBS, iHelp)
						CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === DO_HELP_TEXT_AND_MESSAGES - 6")
					ENDIF
				ENDIF
			BREAK
			CASE 7
				IF NOT IS_BIT_SET(iHelpBS, iHelp)
					IF NOT IS_PAUSE_MENU_ACTIVE()
						PRINT_HELP("CPC_PASSIV1",DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) //Unavailable in passive mode
						SET_FREEMODE_EVENT_HELP_BACKGROUND()
						SET_BIT(iHelpBS, iHelp)
						CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === DO_HELP_TEXT_AND_MESSAGES - 7")
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

FUNC BOOL CAN_PLAYER_COLLECT_CHECKPOINT()

	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
		RETURN FALSE
	ENDIF

	IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID())
		IF (IS_MP_PASSIVE_MODE_ENABLED()
		AND NOT IS_TEMP_PASSIVE_MODE_ENABLED())
		OR IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_TIME_TRIAL)
		OR IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_RC_TIME_TRIAL)
			DO_HELP_TEXT_AND_MESSAGES(0)
		ELSE
			DO_HELP_TEXT_AND_MESSAGES(7)
		ENDIF
		RETURN FALSE
	ENDIF
	
	//Don't allow passengers to collect
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) != VS_DRIVER
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF !IS_AIR_CHECKPOINT()
		IF IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
			DO_HELP_TEXT_AND_MESSAGES(1)
			RETURN FALSE
		ENDIF	
	ENDIF
	
	RETURN TRUE

ENDFUNC

/// PURPOSE: Check if you have reached a checkpoint and notify the server
///    
/// PARAMS: 
///    serverCheckpoint - Contains the server checkpoint data
///    iCheckpoint - The current checkpoint index
PROC CLIENT_PROCESS_REACHING_CHECKPOINTS(SERVER_CHECKPOINT_DATA serverCheckpoint,INT iCheckpoint)

	IF NOT IS_BIT_SET(serverBD.iCheckpointCollectedBS[GET_LONG_BITSET_INDEX(iCheckpoint)],GET_LONG_BITSET_BIT(iCheckpoint))

		IF NOT IS_BIT_SET(iLocalCheckpointCollectedBS[GET_LONG_BITSET_INDEX(iCheckpoint)],GET_LONG_BITSET_BIT(iCheckpoint))
			FLOAT checkpointRadiusHeight,checkpointRadius,checkpointZPos
			IF IS_AIR_CHECKPOINT()
				checkpointRadiusHeight = CHECKPOINT_RADIUS_HEIGHT_AIR
				checkpointRadius = CHECKPOINT_RADIUS_AIR
				checkpointZPos = CHECKPOINT_Z_POS_AIR
			ELSE
				checkpointRadiusHeight = CHECKPOINT_RADIUS_HEIGHT
				checkpointRadius = CHECKPOINT_RADIUS
				checkpointZPos = CHECKPOINT_Z_POS
			ENDIF
		
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), serverCheckpoint.vLocation +<<0, 0, checkpointZPos>>, <<checkpointRadius, checkpointRadius, checkpointRadiusHeight>>)
				IF CAN_PLAYER_COLLECT_CHECKPOINT()

					iCheckpointReachedTime = GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), serverBD.StartTime)
					IF NETWORK_GET_HOST_OF_THIS_SCRIPT() != INVALID_PARTICIPANT_INDEX()
					AND NETWORK_IS_PARTICIPANT_ACTIVE(NETWORK_GET_HOST_OF_THIS_SCRIPT())
						BROADCAST_CHECKPOINT_COLLECTION_REACHED_CHECKPOINT(iCheckpointReachedTime,iCheckpoint, SPECIFIC_PLAYER(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_HOST_OF_THIS_SCRIPT())))
						sCaheEvent.iCheckpoint = -1
						sCaheEvent.iCheckpointTime = -1
					ELSE
						sCaheEvent.iCheckpoint = iCheckpoint
						sCaheEvent.iCheckpointTime = iCheckpointReachedTime
						CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === CLIENT_PROCESS_REACHING_CHECKPOINTS - sCaheEvent.iCheckpoint =  ",sCaheEvent.iCheckpoint," - sCaheEvent.iCheckpointTime = ",sCaheEvent.iCheckpointTime)
					ENDIF
					SET_BIT(iLocalCheckpointCollectedBS[GET_LONG_BITSET_INDEX(iCheckpoint)],GET_LONG_BITSET_BIT(iCheckpoint))
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === CLIENT_PROCESS_REACHING_CHECKPOINTS - REACHED CHECKPOINT ",iCheckpoint," - SENT EVENT - TIME = ",iCheckpointReachedTime)
					
					
					
				ENDIF
			ENDIF
		ELSE
			//Resend event if no response
			IF HAS_NET_TIMER_EXPIRED(iResendEventTimer, RESEND_EVENT_DELAY)
				IF NETWORK_GET_HOST_OF_THIS_SCRIPT() != INVALID_PARTICIPANT_INDEX()
				AND NETWORK_IS_PARTICIPANT_ACTIVE(NETWORK_GET_HOST_OF_THIS_SCRIPT())
					BROADCAST_CHECKPOINT_COLLECTION_REACHED_CHECKPOINT(iCheckpointReachedTime,iCheckpoint, SPECIFIC_PLAYER(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_HOST_OF_THIS_SCRIPT())))
					sCaheEvent.iCheckpointTime = -1
					sCaheEvent.iCheckpoint = -1
				ELSE
					sCaheEvent.iCheckpoint = iCheckpoint
					sCaheEvent.iCheckpointTime = iCheckpointReachedTime
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === CLIENT_PROCESS_REACHING_CHECKPOINTS B - sCaheEvent.iCheckpoint =  ",sCaheEvent.iCheckpoint," - sCaheEvent.iCheckpointTime = ",sCaheEvent.iCheckpointTime)
				ENDIF
				RESET_NET_TIMER(iResendEventTimer)
				CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === CLIENT_PROCESS_REACHING_CHECKPOINTS - REACHED CHECKPOINT ",iCheckpoint," - RESENT EVENT - TIME = ",iCheckpointReachedTime)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL HAS_PARTICIPANT_PARTICIPATED_IN_EVENT(INT iPart)

	IF iPart >= 0 AND iPart < 32
		RETURN (playerBD[iPart].iRewardCounter > 0)
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///		1000 - Cash reward default.
FUNC INT GET_EOM_DEFAULT_CASH_REWARD()
	RETURN g_sMPTunables.iCheckpoint_Minimum_Participation_Cash
ENDFUNC

/// PURPOSE:
///		100 - RP reward default.
FUNC INT GET_EOM_DEFAULT_RP_REWARD()
	RETURN g_sMPTunables.iCheckpoint_Minimum_Participation_RP
ENDFUNC

/// PURPOSE:
/// 	5,000 - Cash reward bonus.
FUNC INT GET_CASH_REWARD_BONUS()
	RETURN g_sMPTunables.iCheckpoint_Bonus_Cash
ENDFUNC

/// PURPOSE:
/// 	1,000 - RP reward bonus.
FUNC INT GET_RP_REWARD_BONUS()
	RETURN g_sMPTunables.iCheckpoint_Bonus_RP
ENDFUNC

/// PURPOSE:
/// 	5.0 - Space between cash reward categories.
FUNC FLOAT GET_CASH_REWARD_CHECKPOINT_SPACER()
	RETURN g_sMPTunables.fCP_COLLECTION_CASH_REWARD_CHECKPOINT_SPACER
ENDFUNC

/// PURPOSE:
/// 	20.0 - Space between RP reward categories.
FUNC FLOAT GET_RP_REWARD_CHECKPOINT_SPACER()
	RETURN g_sMPTunables.fCP_COLLECTION_RP_REWARD_CHECKPOINT_SPACER 
ENDFUNC

/// PURPOSE:
/// 	0.5 - Cash reward air checkpoint multiplier.
FUNC FLOAT GET_CASH_REWARD_AIR_CHECKPOINT_MULTIPLIER()
	RETURN g_sMPTunables.fCheckpoint_Air_Checkpoint_Multiplier_Cash
ENDFUNC

/// PURPOSE:
/// 	0.5 - RP reward air checkpoint multiplier.
FUNC FLOAT GET_RP_REWARD_AIR_CHECKPOINT_MULTIPLIER()
	RETURN g_sMPTunables.fCheckpoint_Air_Checkpoint_Multiplier_RP
ENDFUNC



/// PURPOSE:
///    Take the group ID for a checkpoint and return the cash reward. Supports 12 unique reward values.
/// PARAMS:
///    iCheckPointGroup - CEIL(Total checkpoints / Spacer)
/// RETURNS:
///    INT Cash reward value.
FUNC INT GET_CASH_REWARD_FOR_CHECKPOINT(INT iCheckPointGroup)
	SWITCH iCheckPointGroup
		
		CASE 1	RETURN g_sMPTunables.iCheckpoint_Set_1Cash_Value
		CASE 2 	RETURN g_sMPTunables.iCheckpoint_Set_2_Cash_Value
		CASE 3 	RETURN g_sMPTunables.iCheckpoint_Set_3_Cash_Value
		CASE 4 	RETURN g_sMPTunables.iCheckpoint_Set_4_Cash_Value
		CASE 5 	RETURN g_sMPTunables.iCheckpoint_Set_5_Cash_Value
		CASE 6 	RETURN g_sMPTunables.iCheckpoint_Set_6_Cash_Value
		CASE 7	RETURN g_sMPTunables.iCheckpoint_Set_7_Cash_Value
		CASE 8	RETURN g_sMPTunables.iCheckpoint_Set_8_Cash_Value
		CASE 9	
		CASE 10	
		CASE 11
		CASE 12 RETURN g_sMPTunables.iCheckpoint_Set_9_Cash_Value
		DEFAULT RETURN g_sMPTunables.iCheckpoint_Set_10_Cash_Value
	ENDSWITCH
ENDFUNC


/// PURPOSE:
///    Take the group ID for a checkpoint and return the RP reward. Supports 12 unique reward values.
/// PARAMS:
///    iCheckPointGroup - CEIL(Total checkpoints / Spacer)
/// RETURNS:
///    INT RP reward value.
FUNC INT GET_RP_REWARD_FOR_CHECKPOINT(INT iCheckPointGroup)
	SWITCH iCheckPointGroup
		
		CASE 1	RETURN g_sMPTunables.iCheckpoint_Set_1_RP_Value 
		CASE 2 	RETURN g_sMPTunables.iCheckpoint_Set_2_RP_Value
		CASE 3 	RETURN g_sMPTunables.iCheckpoint_Set_3_RP_Value
		CASE 4 	RETURN g_sMPTunables.iCheckpoint_Set_4_RP_Value
		DEFAULT RETURN g_sMPTunables.iCheckpoint_Set_4_RP_Value
	ENDSWITCH
ENDFUNC


/// PURPOSE: To give rewards to the local player for reaching checkpoints
///    
/// PARAMS:
///    serverCheckpoint - Contains the server checkpoint data
///    iCheckpoint - The current checkpoint index
PROC GIVE_REWARDS(SERVER_CHECKPOINT_DATA serverCheckpoint, INT iCheckpoint)
	
	
	
	IF serverCheckpoint.iCollectedBy = PARTICIPANT_ID_TO_INT()
	AND NOT IS_BIT_SET(iLocalRewardGivenBS[GET_LONG_BITSET_INDEX(iCheckpoint)],GET_LONG_BITSET_BIT(iCheckpoint))
		playerBD[PARTICIPANT_ID_TO_INT()].iRewardCounter++
		
		IF iStartTime = 0
			iStartTime = NATIVE_TO_INT(GET_NETWORK_TIME())
		ENDIF
		
		INT iCashGiven = GET_CASH_REWARD_FOR_CHECKPOINT(CEIL(playerBD[PARTICIPANT_ID_TO_INT()].iRewardCounter / GET_CASH_REWARD_CHECKPOINT_SPACER()))
		INT iRPGiven = GET_RP_REWARD_FOR_CHECKPOINT(CEIL(playerBD[PARTICIPANT_ID_TO_INT()].iRewardCounter / GET_RP_REWARD_CHECKPOINT_SPACER()))
		
		iCashGiven = ROUND(iCashGiven * g_sMPTunables.fCheckpoints_Event_Multiplier_Cash)
		iRPGiven = ROUND(iRPGiven * g_sMPTunables.fCheckpoints_Event_Multiplier_RP)
		
		IF IS_AIR_CHECKPOINT()
			iCashGiven = ROUND(iCashGiven * GET_CASH_REWARD_AIR_CHECKPOINT_MULTIPLIER())
			iRPGiven = ROUND(iRPGiven * GET_RP_REWARD_AIR_CHECKPOINT_MULTIPLIER())
			CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === GIVE_REWARDS - Applying air checkpoint multiplier ($: ",GET_CASH_REWARD_AIR_CHECKPOINT_MULTIPLIER(),", RP: ",GET_RP_REWARD_AIR_CHECKPOINT_MULTIPLIER(),") to reward values.")
		ENDIF
		
		
		IF USE_SERVER_TRANSACTIONS()
			INT iScriptTransactionIndex
			TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_AMBIENT_JOB_CHECKPOINT_COLLECTION, iCashGiven, iScriptTransactionIndex, FALSE, TRUE) //Add new service
		ELSE
			AMBIENT_JOB_DATA amData
			NETWORK_EARN_FROM_AMBIENT_JOB(iCashGiven,"AM_CP_COLLECTION",amData)
		ENDIF
		NEXT_RP_ADDITION_SHOW_RANKBAR()
		GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD,PLAYER_PED_ID(),"",XPTYPE_SKILL,XPCATEGORY_COLLECT_CHECKPOINT,iRPGiven)
		iTotalCashEarned += iCashGiven
		g_i_cashForEndEventShard = iCashGiven
		telemetryStruct.m_rpEarned		+= iRPGiven
		
		SET_BIT(iLocalRewardGivenBS[GET_LONG_BITSET_INDEX(iCheckpoint)],GET_LONG_BITSET_BIT(iCheckpoint))
		IF telemetryStruct.m_timeTakenForObjective = 0
			telemetryStruct.m_timeTakenForObjective = GET_CLOUD_TIME_AS_INT()  
		ENDIF
		SET_AS_PERMANENT_PARTICIPANT()
		
		//Play sound
		PLAY_SOUND_FRONTEND(-1,"Checkpoint_Cash_Hit","GTAO_FM_Events_Soundset", FALSE)

		CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === GIVE_REWARDS - Awarding $",iCashGiven," & RP: ",iRPGiven," for checkpoint ",iCheckpoint)
	ENDIF
	
ENDPROC

FUNC BOOL IS_LOCAL_PLAYER_IN_PLACE(INT iPlace)
	IF iPlace >= 0
		IF serverBD.iWinnerPart[iPlace] = PARTICIPANT_ID_TO_INT()
		AND playerBD[PARTICIPANT_ID_TO_INT()].iRewardCounter > 0
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC GIVE_END_REWARDS()
	//If all checkpoints collected
		
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_GIVEN_END_REWARD)
		//Do not reward player if passive or hiding the event
		IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(PLAYER_ID())
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_GIVEN_END_REWARD)
			EXIT
		ENDIF
		IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID())
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_GIVEN_END_REWARD)
			EXIT
		ENDIF
		
		INT iCashGiven
		INT iRPGiven
		
		INT iParticipationCash
		
		//	2423973: Allow second/third place to still receive cash bonus, even if they didn't hit the participant threshold
		IF playerBD[PARTICIPANT_ID_TO_INT()].iRewardCounter >= g_sMPTunables.iCheckpoint_Participation_Threshold_Min_Checkpoints_Collected 
			//Participant cash and RP
			INT iParticipationTime = FLOOR((TO_FLOAT(serverBD.iEndTime - iStartTime) / 60) / 1000)
			IF iParticipationTime > g_sMPTunables.iParticipation_T_Cap
				iParticipationTime = g_sMPTunables.iParticipation_T_Cap
			ELIF iParticipationTime < 1
				iParticipationTime = 1
			ENDIF
			CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === GIVE_END_REWARDS - Participation Time (minutes): ", iParticipationTime)
			
			iParticipationCash = GET_EOM_DEFAULT_CASH_REWARD() * iParticipationTime
			iRPGiven = GET_EOM_DEFAULT_RP_REWARD() * iParticipationTime
			
			CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === GIVE_END_REWARDS - Participation Cash: ", iParticipationCash)
		ELSE
			FM_EVENT_SETUP_DO_MINIMUM_PARTICIPATION_HELP(FALSE)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === GIVE_END_REWARDS - Player score not high enough for participant reward! Value: ", playerBD[PARTICIPANT_ID_TO_INT()].iRewardCounter)
		ENDIF
		
		IF NOT IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_LOW_PARTICIPANTS)
				
			// 2432086: Player received all checkpoints collected bonus without participating
			IF playerBD[PARTICIPANT_ID_TO_INT()].iRewardCounter >= g_sMPTunables.iCheckpoint_Participation_Threshold_Min_Checkpoints_Collected
				
				//Bonus cash for all collected
				IF IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_ALL_CP_COLLECTED)
					iCashGiven += ROUND(GET_CASH_REWARD_BONUS() * g_sMPTunables.fCheckpoints_Event_Multiplier_Cash)
					
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === GIVE_END_REWARDS - Awarding $",iCashGiven," as bonus for collecting all checkpoints.")
				ENDIF
			ENDIF
		
			//Bonus RP for winning
			IF IS_LOCAL_PLAYER_IN_PLACE(0)
				telemetryStruct.m_endReason       	=AE_END_WON
				iRPGiven += ROUND(GET_RP_REWARD_BONUS() * g_sMPTunables.fCheckpoints_Event_Multiplier_RP)
			ENDIF
			IF serverBD.iWinnerPart[0] != PARTICIPANT_ID_TO_INT()
			AND serverBD.iWinnerPart[1] != PARTICIPANT_ID_TO_INT()
			AND serverBD.iWinnerPart[2] != PARTICIPANT_ID_TO_INT()
				IF playerBD[PARTICIPANT_ID_TO_INT()].iRewardCounter >= g_sMPTunables.iCheckpoint_Participation_Threshold_Min_Checkpoints_Collected 
					FM_EVENT_SETUP_DO_MINIMUM_PARTICIPATION_HELP(TRUE)
				ENDIF
			ENDIF
		ENDIF
		
		GB_HANDLE_GANG_BOSS_CUT(iCashGiven)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === GIVE_END_REWARDS - iCashGiven after boss cut = ",iCashGiven)
		
		iCashGiven += iParticipationCash
		CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === GIVE_END_REWARDS - Cash after Boss Cut with Participation Added: ", iCashGiven)
		
		// Give cash to local player
		IF iCashGiven > 0
			IF USE_SERVER_TRANSACTIONS()
				INT iScriptTransactionIndex
				TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_AMBIENT_JOB_CHECKPOINT_COLLECTION, iCashGiven, iScriptTransactionIndex, FALSE, TRUE) //Add new service
			ELSE
				AMBIENT_JOB_DATA amData
				NETWORK_EARN_FROM_AMBIENT_JOB(iCashGiven,"AM_CP_COLLECTION",amData)
			ENDIF
		ENDIF
		telemetryStruct.m_cashEarned 	= iCashGiven
		IF !g_sMPTunables.bcheckpoints_disable_share_cash               
			IF telemetryStruct.m_cashEarned > 0
				SET_LAST_JOB_DATA(LAST_JOB_CHECKPOINT_COLLECTION, telemetryStruct.m_cashEarned)
			ENDIF
		ENDIF

		g_i_cashForEndEventShard = iCashGiven
		NEXT_RP_ADDITION_SHOW_RANKBAR()
		// Give RP to local player
		GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD,PLAYER_PED_ID(),"",XPTYPE_COMPLETE,XPCATEGORY_COMPLETE_CHALLENGES,iRPGiven)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === GIVE_END_REWARDS - Awarding ",iRPGiven," RP.")
		telemetryStruct.m_rpEarned		+= iRPGiven
		
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_GIVEN_END_REWARD)
	
	ENDIF
ENDPROC


FUNC BOOL IS_PARTICIPANT_IN_PLACE(INT iPart, INT iPlace)

	IF iPart >= 0
		IF iPlace >= 0
			IF serverBD.iWinnerPart[iPart] = iPart
			AND IS_SCORE_BETTER_THAN_SCORE(playerBD[iPart].iRewardCounter, 0)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_ANYONE_IN_PLACE(INT iPlace)

	IF iPlace >= 0
		INT iPart = serverBD.iWinnerPart[iPlace]
		IF iPart >= 0
			IF IS_SCORE_BETTER_THAN_SCORE(playerBD[iPart].iRewardCounter, 0)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE: Displays a big message to inform the player how they have done
///    
PROC SHOW_END_MESSAGES()

	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_SHOWN_MESSAGE)
	AND (FM_EVENT_END_SHARD_SAFE_TO_DISPLAY()
		OR IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_LOW_PARTICIPANTS))
		
		IF IS_BIG_MESSAGE_TYPE_FOR_FM_EVENTS(GET_TYPE_OF_CURRENT_BIG_MESSAGE())
			CLEAR_ALL_BIG_MESSAGES()
		ENDIF

		IF NOT IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_LOW_PARTICIPANTS)
			IF IS_ANYONE_IN_PLACE(0)
			
				STRING sEndMessage
			
				IF IS_LOCAL_PLAYER_IN_PLACE(0)
					IF NOT IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_ALL_CP_COLLECTED)
						sEndMessage = "CPC_END"
					ELSE
						sEndMessage = "CPC_ENDBONUS"
					ENDIF
					SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_FM_EVENT_WIN, playerBD[PARTICIPANT_ID_TO_INT()].iRewardCounter,sEndMessage,"AMCH_WIN", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME)
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === You win!!")		
				ELSE
					PARTICIPANT_INDEX winnerPart = INT_TO_PARTICIPANTINDEX(serverBD.iWinnerPart[0])
					IF NETWORK_IS_PARTICIPANT_ACTIVE(winnerPart)
						PLAYER_INDEX winningPlayer = NETWORK_GET_PLAYER_INDEX(winnerPart)
			
						IF IS_LOCAL_PLAYER_IN_PLACE(1)
							IF NOT IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_ALL_CP_COLLECTED)
								sEndMessage = "CPC_2ND"
							ELSE
								sEndMessage = "CPC_2NDBONUS"
							ENDIF
							SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_FM_EVENT_PASS,playerBD[PARTICIPANT_ID_TO_INT()].iRewardCounter,sEndMessage,"CPC_OVER", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME)
						ELIF IS_LOCAL_PLAYER_IN_PLACE(2)
							IF NOT IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_ALL_CP_COLLECTED)
								sEndMessage = "CPC_3RD"
							ELSE
								sEndMessage = "CPC_3RDBONUS"
							ENDIF
							SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_FM_EVENT_PASS,playerBD[PARTICIPANT_ID_TO_INT()].iRewardCounter,sEndMessage,"CPC_OVER", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME)
						ELSE
							IF IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_ALL_CP_COLLECTED)
							AND playerBD[PARTICIPANT_ID_TO_INT()].iRewardCounter >= g_sMPTunables.iCheckpoint_Participation_Threshold_Min_Checkpoints_Collected
								sEndMessage = "CPC_LOSEBONUS"
							ELSE
								sEndMessage = "CPC_LOSE"
							ENDIF
							
							STRING sPlayer = GET_PLAYER_NAME(winningPlayer)
							IF GB_IS_PLAYER_MEMBER_OF_A_GANG(winningPlayer)
								sPlayer = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(winningPlayer)
							ENDIF
							
							SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_AND_INT(BIG_MESSAGE_FM_EVENT_FINISHED, playerBD[serverBD.iWinnerPart[0]].iRewardCounter, sEndMessage, sPlayer, "CPC_OVER", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME)
							
							CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_AND_INT - ",GET_PLAYER_NAME(winningPlayer))
						ENDIF
						CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === You lose!!")
					ENDIF
				ENDIF
			ELSE
				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_FINISHED,"CPC_OVER","CPC_NOWIN",DEFAULT,ciFM_EVENTS_END_UI_SHARD_TIME)
				CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === No winner!")
			ENDIF
		ELSE
			SET_UP_BAIL_FM_EVENT_DUE_TO_LOW_SESSION_NUMBERS_BIG_MESSAGE("","")
		ENDIF
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_SHOWN_MESSAGE)
		CALL_FREEMODE_AMBIENT_EVENTS_TELEMETRY(FMMC_TYPE_CHECKPOINTS)
	ENDIF
	
	UNUSED_PARAMETER(iTotalCashEarned)

ENDPROC

PROC HANDLE_CHALLENGE_VEHICLE_CLEANUP()

	IF IS_AIR_CHECKPOINT()
		CHALLENGE_VEHICLE currentVeh = serverBD.vehiclesToSpawn[iLocalVehicle]
		IF NETWORK_DOES_NETWORK_ID_EXIST(currentVeh.niVeh)
			VEHICLE_INDEX vehID = NET_TO_VEH(currentVeh.niVeh)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(vehID)
				IF NOT IS_VEHICLE_DRIVEABLE(vehID)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(vehID)
				ENDIF
			ENDIF
		ENDIF
		
		iLocalVehicle++
			
		IF iLocalVehicle >= NUM_VEHICLES_TO_SPAWN
			iLocalVehicle = 0
			bShouldDrawTimer = bDrawTimer
			bDrawTimer = FALSE
		ENDIF
	ENDIF
ENDPROC

PROC DRAW_LOCAL_PLAYER_SCORE_AT_POSITION(HUDORDER position)

	INT iScore = playerBD[PARTICIPANT_ID_TO_INT()].iRewardCounter
		
	IF iScore > 0

		PLAYER_INDEX localPlayerID = NETWORK_GET_PLAYER_INDEX(PARTICIPANT_ID())
		
		DRAW_GENERIC_SCORE(iScore,GET_PLAYER_NAME(localPlayerID), 0, HUD_COLOUR_BLUE, position, TRUE, DEFAULT, DEFAULT,DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_BLUE)

	ENDIF

ENDPROC

PROC DRAW_PARTICIPANT_SCORE_AT_POSITION(INT iPart, HUDORDER position)

	IF iPart > -1

		PARTICIPANT_INDEX part = INT_TO_PARTICIPANTINDEX(iPart)	
		IF NETWORK_IS_PARTICIPANT_ACTIVE(part)
			
			INT iScore = playerBD[iPart].iRewardCounter
			
			IF iScore > 0
								
				PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart))
								
				DRAW_GENERIC_SCORE(iScore,GET_PLAYER_NAME(playerID), 0, HUD_COLOUR_RED, position, TRUE, DEFAULT,DEFAULT,DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_RED)

			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE: Draws the bottom right UI for the challenge including scores and timer
///    
PROC DRAW_CHALLENGE_LEADER_BOARD()
	
	IF serverBD.iWinnerPart[0] > -1
	
		PLAYER_INDEX playerList[NUM_MINI_LEADERBOARD_PLAYERS]
		PLAYER_INDEX playerTemp
		INT iScore[NUM_MINI_LEADERBOARD_PLAYERS]
		INT iRemaining, iTotal, iLocal, iTime, i, iPartToUse
		BOOL bForceRebuild = FALSE
		HUD_COLOURS timeColour
		
		HUD_COLOURS iFleckColour[NUM_MINI_LEADERBOARD_PLAYERS]
		
		REPEAT NUM_MINI_LEADERBOARD_PLAYERS i
			playerList[i] = INVALID_PLAYER_INDEX()
		ENDREPEAT
		REPEAT NUM_MINI_LEADERBOARD_PLAYERS i
			IF serverBD.iWinnerPart[i] > -1
				PARTICIPANT_INDEX partID = INT_TO_PARTICIPANTINDEX(serverBD.iWinnerPart[i])
				IF NETWORK_IS_PARTICIPANT_ACTIVE(partID)
					playerTemp = NETWORK_GET_PLAYER_INDEX(partID)
					IF IS_NET_PLAYER_OK(playerTemp,FALSE)
						IF NOT IS_PLAYER_SCTV(playerTemp)
							playerList[i] = playerTemp
							iScore[i] = playerBD[serverBD.iWinnerPart[i]].iRewardCounter	
							IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
//							AND GB_GET_THIS_PLAYER_GANG_BOSS(playerTemp) = GB_GET_LOCAL_PLAYER_GANG_BOSS()
								IF GB_IS_PLAYER_MEMBER_OF_A_GANG(playerTemp)
									iFleckColour[i] = GET_PLAYER_HUD_COLOUR(playerTemp)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		iPartToUse = PARTICIPANT_ID_TO_INT()
		
		//Deal with Spectators
		IF IS_PLAYER_SPECTATING(PLAYER_ID())
			PED_INDEX specPed = GET_SPECTATOR_SELECTED_PED()
			PLAYER_INDEX specPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(specPed)
			IF NETWORK_IS_PLAYER_ACTIVE(specPlayer)
				PARTICIPANT_INDEX specPart = NETWORK_GET_PARTICIPANT_INDEX(specPlayer)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(specPart)	
					iPartToUse = NATIVE_TO_INT(specPart)
				ENDIF
			ENDIF
		ENDIF
		iRemaining = IMAX(0,serverBD.iRemaining)
		iTotal = serverBD.iNumCheckpoints
		iLocal = IMAX(0,playerBD[iPartToUse].iRewardCounter)
		iTime = IMAX(0,g_sMPTunables.iCheckpoint_Event_Time_Limit-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBd.eventTimer))
	
		HANDLE_AMBIENT_EVENT_COUNTDOWN_AUDIO(iTime)
		
		IF iTime > 30000
			timeColour = HUD_COLOUR_WHITE
		ELSE
			timeColour = HUD_COLOUR_RED
		ENDIF
	
		BOTTOM_RIGHT_UI_1STINT_2NDINT_3RDINT_CHECKPT_LOCALINT_TIMER(playerList[0],playerList[1],playerList[2],iScore[0],iScore[1],iScore[2],iRemaining,iTotal,iLocal,iTime,bForceRebuild,timeColour, GET_COUNTDOWN_TIMER_NAME_FOR_EVENT(),iFleckColour[0],iFleckColour[1],iFleckColour[2])
	ENDIF

ENDPROC

PROC DRAW_CHALLENGES_DPAD_LEADERBOARD()
	DRAW_CHALLENGE_DPAD_LBD(chDPStruct.challengeLbdStruct, chDPStruct.siDpadMovie, SUB_CHALLENGE, chDPStruct.dpadVariables, chDPStruct.fmDpadStruct)
ENDPROC

/// PURPOSE: Draws the UI for the event
///    
PROC DRAW_EVENT_UI()
	
	//If we've done the intro shard then starrt drawing the UI
	IF IS_BIT_SET(iLocalBitset,AMCP_BS_INTRO_SHARD_DISPLAYED)
		DRAW_CHALLENGE_LEADER_BOARD()
	ENDIF
	DRAW_CHALLENGES_DPAD_LEADERBOARD()

// 2459037 - Commented the below out as the same functionality has been added to HANDLE_AMBIENT_EVENT_COUNTDOWN_AUDIO

//	INT iCurrentTime = (g_sMPTunables.iCheckpoint_Event_Time_Limit-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBd.eventTimer))
//
//	IF iCurrentTime <= 10000
//		IF NOT IS_BIT_SET(iLocalBitset,AMCP_BS_STARTED_COUNTDOWN)
//			PLAY_SOUND_FRONTEND(iCountdownSound,"10S","MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
//			SET_BIT(iLocalBitset,AMCP_BS_STARTED_COUNTDOWN)
//			CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === Play 10 second timer")
//		ENDIF
//	ENDIF

ENDPROC
//Hint cam: find closest checkpoint
//PROC MAINTAIN_CLOSEST_CHECKPOINT(LOOK_AT_CHECKPOINT_STRUCT &LookCheckpoint)
//	FLOAT fMaxDist2 = 150000.0
//	FLOAT fDist2
//	VECTOR vMyLoc = GET_ENTITY_COORDS(PLAYER_PED_ID())
//	VECTOR vTemp
//	
//	IF LookCheckpoint.iLoopCount = 0
//		LookCheckpoint.tempClosest = -1
//		LookCheckpoint.fCurrentClosestDist = 999999.0
//	ENDIF
//	
//	IF NOT IS_BIT_SET(serverBD.iCheckpointCollectedBS[GET_LONG_BITSET_INDEX(LookCheckpoint.iLoopCount)], GET_LONG_BITSET_BIT(LookCheckpoint.iLoopCount))
//		vTemp = serverBD.serverCheckpointData[LookCheckpoint.iLoopCount].vLocation
//		fDist2 = VDIST2(vMyLoc, vTemp)
//		IF fDist2 <= fMaxDist2
//			IF fDist2 <= LookCheckpoint.fCurrentClosestDist
//				LookCheckpoint.fCurrentClosestDist = fDist2
//				LookCheckpoint.tempClosest = LookCheckpoint.iLoopCount
//			ENDIF
//		ENDIF
//	ENDIF
//	LookCheckpoint.iLoopCount ++
//	IF LookCheckpoint.iLoopCount = serverBD.iNumCheckpoints
//		LookCheckpoint.iLoopCount = 0
//		IF LookCheckpoint.tempClosest <> -1
//			IF LookCheckpoint.tempClosest <> LookCheckpoint.iCheckpointClosest
//				KILL_CHASE_HINT_CAM(sLookCheckpoint.s_CheckPointChaseCam)
//			ENDIF
//			LookCheckpoint.iCheckpointClosest = LookCheckpoint.tempClosest
//		ELSE
//			LookCheckpoint.iCheckpointClosest = -1
//		ENDIF
//	ENDIF
//ENDPROC
//Hint cam: call the hint cam
//PROC CONTROL_HINT_CAM()
//	IF sLookCheckpoint.iCheckpointClosest <> -1
//		CONTROL_COORD_CHASE_HINT_CAM_IN_VEHICLE(sLookCheckpoint.s_CheckPointChaseCam, serverBD.serverCheckpointData[sLookCheckpoint.iCheckpointClosest].vLocation)
//	ELSE
//		KILL_CHASE_HINT_CAM(sLookCheckpoint.s_CheckPointChaseCam)
//	ENDIF
//ENDPROC

/// PURPOSE: Checks if a checkpoint is close enough to be processed
///    
/// PARAMS:
///    iCheckpoint - The checkpoint to check
/// RETURNS: TRUE if in range, FALSE otherwise
///    
FUNC BOOL IS_CHECKPOINT_IN_PROCESSING_RANGE(INT iCheckpoint)
	RETURN GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(),serverBD.serverCheckpointData[iCheckpoint].vLocation) < CHECKPOINT_PROCESSING_RANGE
ENDFUNC

FUNC BOOL IS_CHECKPOINT_IN_DRAWING_RANGE(INT iCheckpoint)
	RETURN GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(),serverBD.serverCheckpointData[iCheckpoint].vLocation) < CHECKPOINT_DRAW_RANGE
ENDFUNC

PROC CLEANUP_CHECKPOINTS()

	INT iCheckpoint
	REPEAT serverBD.iNumCheckpoints iCheckpoint 
		IF DOES_BLIP_EXIST(localCheckpointData[iCheckpoint].cpBlip)
			REMOVE_BLIP(localCheckpointData[iCheckpoint].cpBlip)
			DELETE_CHECKPOINT(localCheckpointData[iCheckpoint].checkpointObj)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === BLIP AND CHECKPOINT ",iCheckpoint,"REMOVED - CLEANUP")
		ENDIF
	ENDREPEAT	
	
	REPEAT CHECKPOINT_BS_SIZE iCheckpoint
		iLocalCheckpointCreated[iCheckpoint] = 0
	ENDREPEAT

ENDPROC

FUNC BLIP_SPRITE GET_BLIP_SPRITE_TYPE(INT iVeh)
	SWITCH serverBD.vehiclesToSpawn[iVeh].vehModel
		CASE MAVERICK RETURN RADAR_TRACE_HELICOPTER
		CASE MAMMATUS RETURN RADAR_TRACE_PLAYER_PLANE
	ENDSWITCH
	RETURN RADAR_TRACE_INVALID
ENDFUNC

PROC HANDLE_CHALLENGE_BLIPS(BOOL bRemoveAll = FALSE)

	IF IS_AIR_CHECKPOINT()
		IF IS_NET_PLAYER_OK(PLAYER_ID())

			INT iVeh
			REPEAT NUM_VEHICLES_TO_SPAWN iVeh
			
				IF NOT bRemoveAll 
				AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.vehiclesToSpawn[iVeh].niVeh)
				AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.vehiclesToSpawn[iVeh].niVeh))
				AND IS_VEHICLE_SEAT_FREE(NET_TO_VEH(serverBD.vehiclesToSpawn[iVeh].niVeh),VS_DRIVER)
				
					//Add Blip
					IF NOT DOES_BLIP_EXIST(vehBlip[iVeh])
							
						vehBlip[iVeh] = ADD_BLIP_FOR_ENTITY(NET_TO_ENT(serverBD.vehiclesToSpawn[iVeh].niVeh))
						SET_BLIP_PRIORITY(vehBlip[iVeh], BLIPPRIORITY_HIGHEST)
						SET_BLIP_NAME_FROM_TEXT_FILE(vehBlip[iVeh], "AMCH_AC")
						SET_BLIP_SPRITE(vehBlip[iVeh],GET_BLIP_SPRITE_TYPE(iVeh))
						SET_BLIP_COLOUR_FROM_HUD_COLOUR(vehBlip[iVeh],HUD_COLOUR_BLUE)
						
						CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION ===  BLIP ADDED FOR VEH ",iVeh)
					ELSE
						VECTOR vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
						IF VDIST(vPlayerCoords,GET_ENTITY_COORDS(NET_TO_VEH(serverBD.vehiclesToSpawn[iVeh].niVeh))) <= 150
							SHOW_HEIGHT_ON_BLIP(vehBlip[iVeh],TRUE)
						ELSE	
							SHOW_HEIGHT_ON_BLIP(vehBlip[iVeh],FALSE)
						ENDIF
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(vehBlip[iVeh])
						REMOVE_BLIP(vehBlip[iVeh])
						CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION ===  Removing blip for veh ",iVeh)
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF

ENDPROC

PROC HANDLE_CHECKPOINT_AUDIO()

	//Do 1st place sounds
	IF IS_LOCAL_PLAYER_IN_PLACE(0)	
		IF NOT IS_BIT_SET(iLocalBitset,AMCP_BS_LOCAL_PLAYER_FIRST)
			IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
				PLAY_SOUND_FRONTEND(-1,"Enter_1st","GTAO_Biker_Modes_Soundset", FALSE)
			ELSE
				PLAY_SOUND_FRONTEND(-1,"Enter_1st","GTAO_FM_Events_Soundset", FALSE)
			ENDIF
			SET_BIT(iLocalBitset,AMCP_BS_LOCAL_PLAYER_FIRST) 
			CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === HANDLE_CHECKPOINT_AUDIO - Enter_1st")
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBitset,AMCP_BS_LOCAL_PLAYER_FIRST)
			IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
				PLAY_SOUND_FRONTEND(-1,"Lose_1st","GTAO_Biker_Modes_Soundset", FALSE)
			ELSE
				PLAY_SOUND_FRONTEND(-1,"Lose_1st","GTAO_FM_Events_Soundset", FALSE)
			ENDIF
			CLEAR_BIT(iLocalBitset,AMCP_BS_LOCAL_PLAYER_FIRST) 
			CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === HANDLE_CHECKPOINT_AUDIO - Lose_1st")
		ENDIF
	ENDIF

ENDPROC

//PURPOSE: Controls the explode vehicle stage
PROC MAINTAIN_EXPLODE_VEH()

	IF serverBD.eAMCP_Stage = AMCP_EXPLODE

		IF HAS_NET_TIMER_STARTED(serverBD.ExplodeVehTimer)

			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.vehiclesToSpawn[iLocalVehicle].niVeh)
				IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.vehiclesToSpawn[iLocalVehicle].niVeh))
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === Explosion Time = ",VEHICLE_EXPLOSION_TIME-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.ExplodeVehTimer))
					
					//Do sound
					IF NOT IS_BIT_SET(iExplodeBS, iLocalVehicle)
						SET_BIT(iExplodeBS, iLocalVehicle)
						iExplodeSOund[iLocalVehicle] = GET_SOUND_ID()
						PLAY_SOUND_FROM_ENTITY(iExplodeSOund[iLocalVehicle], "Explosion_Countdown", NET_TO_VEH(serverBD.vehiclesToSpawn[iLocalVehicle].niVeh), "GTAO_FM_Events_Soundset") 
						SET_VARIABLE_ON_SOUND(iExplodeSOund[iLocalVehicle],  "Time", 30)
						CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CHALLENGES === MAINTAIN_EXPLODE_VEH - STARTED SOUND ON VEH ", iLocalVehicle)
					ENDIF
					
					IF (VEHICLE_EXPLOSION_TIME-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.ExplodeVehTimer)) >= 0
						CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === Doing explosion countdown.")
						//IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), NET_TO_VEH(serverBD.vehiclesToSpawn[iLocalVehicle].niVeh), <<VEH_EXP_RANGE, VEH_EXP_RANGE, VEH_EXP_RANGE>>) // Check whether the player is in range
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.vehiclesToSpawn[iLocalVehicle].niVeh)) // Check whether the player is in the vehicle
							DO_HELP_TEXT_AND_MESSAGES(3)
							bDrawTimer = TRUE
						ENDIF
						//ENDIF
					ELSE
						CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === Explosion countdown finished.")
//						IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), NET_TO_VEH(serverBD.vehiclesToSpawn[iLocalVehicle].niVeh), <<VEH_EXP_RANGE, VEH_EXP_RANGE, VEH_EXP_RANGE>>)
//							bDrawTimer = TRUE
//						ENDIF
												
						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.vehiclesToSpawn[iLocalVehicle].niVeh)
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(NET_TO_VEH(serverBD.vehiclesToSpawn[iLocalVehicle].niVeh))
							NETWORK_EXPLODE_VEHICLE(NET_TO_VEH(serverBD.vehiclesToSpawn[iLocalVehicle].niVeh))
							CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === MAINTAIN_EXPLODE_VEH - VEHICLE EXPLODED: ",iLocalVehicle)
						ENDIF
					ENDIF
				ELSE
					IF IS_BIT_SET(iExplodeBS, iLocalVehicle)
					AND NOT HAS_SOUND_FINISHED(iExplodeSOund[iLocalVehicle])
						STOP_SOUND(iExplodeSOund[iLocalVehicle])
					ENDIF
				ENDIF
			ENDIF

			
			IF bShouldDrawTimer
				IF NOT IS_PLAYER_SPECTATING(PLAYER_ID())
					DRAW_GENERIC_TIMER(IMAX((VEHICLE_EXPLOSION_TIME-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.ExplodeVehTimer)),0), "HTV_DESTR", 0, TIMER_STYLE_USEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_DONTCARE, DEFAULT, HUD_COLOUR_RED,DEFAULT,DEFAULT,DEFAULT,HUD_COLOUR_RED)
					SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
				ENDIF
			ENDIF
		
		ENDIF
	ENDIF
	
ENDPROC

PROC CLIENT_PROCESS_SORT_FOR_LBD()

	INT iParticipant
	THE_LEADERBOARD_STRUCT blankStruct
	PLAYER_INDEX playerTemp
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
	
		chDPStruct.challengeLbdStruct[iParticipant] = blankStruct
		chDPStruct.challengeLbdStruct[iParticipant].playerID = INVALID_PLAYER_INDEX()
		
		IF serverBD.iWinnerPart[iParticipant] > -1
			PARTICIPANT_INDEX partID = INT_TO_PARTICIPANTINDEX(serverBD.iWinnerPart[iParticipant])
			IF NETWORK_IS_PARTICIPANT_ACTIVE(partID)
				playerTemp = NETWORK_GET_PLAYER_INDEX(partID)
				IF NOT IS_PLAYER_SCTV(playerTemp)
					chDPStruct.challengeLbdStruct[iParticipant].iParticipant = serverBD.iWinnerPart[iParticipant]
					chDPStruct.challengeLbdStruct[iParticipant].playerID = playerTemp
					
					INT iCurrentScore = playerBD[serverBD.iWinnerPart[iParticipant]].iRewardCounter

					chDPStruct.challengeLbdStruct[iParticipant].iScore = iCurrentScore
					chDPStruct.challengeLbdStruct[iParticipant].iFinishTime = -1
				
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === CLIENT_PROCESS_SORT_FOR_LBD - iParticipant = ", chDPStruct.challengeLbdStruct[iParticipant].iParticipant)
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === CLIENT_PROCESS_SORT_FOR_LBD - Player = ",GET_PLAYER_NAME(chDPStruct.challengeLbdStruct[iParticipant].playerID))
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === CLIENT_PROCESS_SORT_FOR_LBD - iScore = ", chDPStruct.challengeLbdStruct[iParticipant].iScore)
				ENDIF
			ENDIF
		ENDIF
		
	ENDREPEAT

ENDPROC



PROC HANDLE_SENDING_CACHED_HOST_CHECKPOINT_ENVET()
	IF sCaheEvent.iCheckpoint != -1
	AND sCaheEvent.iCheckpointTime != -1
		IF NETWORK_GET_HOST_OF_THIS_SCRIPT() != INVALID_PARTICIPANT_INDEX()
		AND NETWORK_IS_PARTICIPANT_ACTIVE(NETWORK_GET_HOST_OF_THIS_SCRIPT())
			BROADCAST_CHECKPOINT_COLLECTION_REACHED_CHECKPOINT(sCaheEvent.iCheckpointTime,sCaheEvent.iCheckpoint, SPECIFIC_PLAYER(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_HOST_OF_THIS_SCRIPT())))
			sCaheEvent.iCheckpoint = -1
			sCaheEvent.iCheckpointTime = -1
			CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === HANDLE_SENDING_CACHED_HOST_CHECKPOINT_ENVET - sCaheEvent.iCheckpoint =  ",sCaheEvent.iCheckpoint," - sCaheEvent.iCheckpointTime = ",sCaheEvent.iCheckpointTime)
		ENDIF
	ENDIF
ENDPROC



PROC HANDLE_OBJECTIVE_TEXT()

	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND NOT FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(PLAYER_ID())
	AND NOT FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID())
	AND IS_IT_SAFE_TO_DISPLAY_FM_EVENT_UI() 
		IF playerBD[PARTICIPANT_ID_TO_INT()].eAMCP_Stage = AMCP_WAIT
			STRING sMode
			IF serverBD.bAir
				sMode = "CPC_TILELA"
			ELSE
				sMode = "CPC_TILEL"
			ENDIF
			IF IS_AIR_CHECKPOINT()
			AND NOT IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
				Print_Objective_Text_With_Two_String("CPC_PREPAIR",sMode,GET_LABEL_FOR_QUADRANT())
			ELSE
				Print_Objective_Text_With_Two_String("CPC_PREP",sMode,GET_LABEL_FOR_QUADRANT())
			ENDIF
		ELSE
			Clear_Any_Objective_Text_From_This_Script()
		ENDIF
	ELSE
		Clear_Any_Objective_Text_From_This_Script()
	ENDIF

ENDPROC

//----------------------
//	Server Functions
//----------------------

/// PURPOSE: Determines whether a checkpoint group is unsuitable due to some distance/line checks
///    
/// PARAMS:
///    iCheckpointGroup - The current checkpoint group
///    iIteration - The current iteration
/// RETURNS: TRUE if unsuitable, FALSE otherwise
///    
FUNC BOOL IS_CHECKPOINT_GROUP_UNSUITABLE(INT iCheckpointGroup,INT iIteration)

	INT i
	REPEAT iIteration i
		VECTOR vPrevious = GET_MIDPOINT_VECTOR_FOR_CHECKPOINT_GROUP(serverBD.serverCheckpointData[(i * NUM_CHECKPOINT_PER_GROUP)].iCheckpointIndex) 
		VECTOR vCurrent = GET_MIDPOINT_VECTOR_FOR_CHECKPOINT_GROUP(iCheckpointGroup)

		//Check if groups are in line on axes
		IF (vCurrent.x > (vPrevious.x - 1000) AND vCurrent.x < (vPrevious.x + 1000) AND vCurrent.y > (vPrevious.y - 3000) AND vCurrent.y < (vPrevious.y + 3000) )
		OR (vCurrent.y > (vPrevious.y - 1000) AND vCurrent.y < (vPrevious.y + 1000))
			CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === IS_CHECKPOINT_GROUP_UNSUITABLE - Not using group ",iCheckpointGroup,", in line with ",serverBD.serverCheckpointData[(i * NUM_CHECKPOINT_PER_GROUP)].iCheckpointIndex)
			RETURN TRUE
		ENDIF
		
		//Check if groups' radii intersect
		IF GET_DISTANCE_BETWEEN_COORDS(vPrevious,vCurrent) < 3000
			CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === IS_CHECKPOINT_GROUP_UNSUITABLE - Not using group ",iCheckpointGroup,", in radius of ",serverBD.serverCheckpointData[(i * NUM_CHECKPOINT_PER_GROUP)].iCheckpointIndex)
			RETURN TRUE
		ENDIF
		
	ENDREPEAT

	RETURN FALSE

ENDFUNC

/// PURPOSE: Grabs a random checkpoint group that has not already been used.
///    
/// RETURNS: The checkpoint group index
///    
FUNC INT GET_RANDOM_CHECKPOINT_GROUP(INT iIteration)
	
	INT iCheckpointGroup
	
	IF iIteration = 0
		BOOL bAir,bSea
	
		//Decide first checkpoint group type: land (50%), sea(25%) or air(25%)
		INT iType = GET_RANDOM_INT_IN_RANGE(0,4)
		IF iType = 0
			bAir = TRUE
		ELIF iType = 1
			bSea = TRUE
		ENDIF
		
		IF bAir
			iCheckpointGroup = GET_RANDOM_INT_IN_RANGE(LAND_CHECKPOINT_GROUPS,LAND_CHECKPOINT_GROUPS+AIR_CHECKPOINT_GROUPS)
		ELIF bSea
			iCheckpointGroup = GET_RANDOM_INT_IN_RANGE(LAND_CHECKPOINT_GROUPS+AIR_CHECKPOINT_GROUPS,LAND_CHECKPOINT_GROUPS+AIR_CHECKPOINT_GROUPS+SEA_CHECKPOINT_GROUPS)
		ELSE
			iCheckpointGroup = GET_RANDOM_INT_IN_RANGE(0,LAND_CHECKPOINT_GROUPS)
		ENDIF
		
		SET_BIT(iCheckpointUsedBS,iCheckpointGroup)
		
		RETURN iCheckpointGroup
	ELSE
		iCheckpointGroup = GET_RANDOM_INT_IN_RANGE(0,LAND_CHECKPOINT_GROUPS)

		INT iCounter = 0		
		WHILE iCounter < LAND_CHECKPOINT_GROUPS
			IF NOT IS_BIT_SET(iCheckpointUsedBS,iCheckpointGroup)
				IF NOT IS_CHECKPOINT_GROUP_UNSUITABLE(iCheckpointGroup,iIteration) //Line check

					SET_BIT(iCheckpointUsedBS,iCheckpointGroup)
					RETURN iCheckpointGroup
				ENDIF
			ENDIF
			iCheckpointGroup++
			iCounter++
			IF iCheckpointGroup >= LAND_CHECKPOINT_GROUPS
				iCheckpointGroup = 0
			ENDIF
		ENDWHILE
		
		CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === NO SUITABLE CHECKPOINT FOUND - ABORT")
		RETURN -1
	ENDIF
	
ENDFUNC

FUNC BOOL IS_CHECKPOINT_AIR_AREA_DISABLED(INT iArea)
	
	SWITCH(iArea)
	
		CASE 0		RETURN (g_sMPTunables.bCheckpoint_Disable_SE_Quarter_Air_Variation OR g_sMPTunables.bCheckpoint_Disable_All_Air_Variations)

		CASE 1		RETURN (g_sMPTunables.bCheckpoint_Disable_SW_Quarter_Air_Variation OR g_sMPTunables.bCheckpoint_Disable_All_Air_Variations)
							
		CASE 2		RETURN (g_sMPTunables.bCheckpoint_Disable_NW_Quarter_Air_Variation OR g_sMPTunables.bCheckpoint_Disable_All_Air_Variations)
							
		CASE 3		RETURN (g_sMPTunables.bCheckpoint_Disable_NE_Quarter_Air_Variation OR g_sMPTunables.bCheckpoint_Disable_All_Air_Variations)
	
	ENDSWITCH

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_CHECKPOINT_LAND_AREA_DISABLED(INT iArea)

	SWITCH(iArea)
	
		CASE 0		RETURN (g_sMPTunables.bCheckpoint_Disable_SE_Quarter_Land_Variation OR g_sMPTunables.bCheckpoint_Disable_All_Land_Variations )
							
		CASE 1		RETURN (g_sMPTunables.bCheckpoint_Disable_SW_Quarter_Land_Variation OR g_sMPTunables.bCheckpoint_Disable_All_Land_Variations )
							
		CASE 2		RETURN (g_sMPTunables.bCheckpoint_Disable_NW_Quarter_Land_Variation OR g_sMPTunables.bCheckpoint_Disable_All_Land_Variations )
							
		CASE 3		RETURN (g_sMPTunables.bCheckpoint_Disable_NE_Quarter_Land_Variation OR g_sMPTunables.bCheckpoint_Disable_All_Land_Variations )
	
	ENDSWITCH

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_CHECKPOINT_AREA_DISABLED(INT iArea)

	RETURN (IS_CHECKPOINT_AIR_AREA_DISABLED(iArea) AND IS_CHECKPOINT_LAND_AREA_DISABLED(iArea))

ENDFUNC

/// PURPOSE: Calculates and stores the number of participants in each area
///    
/// PARAMS:
///    iParticipants - An array to store the participant/area scores
/// RETURNS: The total number of participants across all areas  - N.B. Each player may be counted more than once
///    
FUNC INT CALCULATE_PARTICPANTS_IN_AREA(INT &iParticipants[])

	INT iParticipant, iTotalPart
	VECTOR vBottomLeft,vTopRight
	BOOL bPickRandom = TRUE
	INT iArea
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
			IF IS_NET_PLAYER_OK(playerID)
				PED_INDEX pedID = GET_PLAYER_PED(playerID)
				IF NOT IS_PED_INJURED(pedID)
					REPEAT NUM_CHECKPOINT_AREAS iArea
						IF NOT IS_CHECKPOINT_AREA_DISABLED(iArea)
							IF GET_RECTANGLE_VECTORS_FOR_CHECKPOINT_AREA(iArea,vBottomLeft,vTopRight)
								IF IS_ENTITY_IN_AREA(pedID,vBottomLeft,vTopRight,FALSE,FALSE)
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === CALCULATE_PARTICPANTS_IN_AREA - Participant ",iParticipant," in area ",iArea)
									bPickRandom = FALSE
									iParticipants[iArea]++
									iTotalPart++
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

	//If no one is in any quadrent that's not disabled then set areas to have one part to give equal waiting to the random pick
	IF bPickRandom
		REPEAT NUM_CHECKPOINT_AREAS iArea
			IF NOT IS_CHECKPOINT_AREA_DISABLED(iArea)
				iParticipants[iArea] = 1
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN iTotalPart
		
ENDFUNC

/// PURPOSE: Grabs a checkpoint area from one of the 4 corners of the map
///    
/// RETURNS: The checkpoint area index
///    
FUNC INT PICK_CHECKPOINT_AREA()

	#IF IS_DEBUG_BUILD
	IF MPGlobalsAmbience.iCPCollectionArea >= 0
		RETURN MPGlobalsAmbience.iCPCollectionArea
	ENDIF
	#ENDIF

	iAreaSelection = 1

	INT iParticipants[NUM_CHECKPOINT_AREAS]

	SWITCH(iAreaSelection)
		
		//Random
		CASE 0	
			RETURN GET_RANDOM_INT_IN_RANGE(0,NUM_CHECKPOINT_AREAS)
		BREAK
		
		//Weighted by participants
		CASE 1
			INT iTotal, iChosen, iArea, iPreviousCount
			iTotal = CALCULATE_PARTICPANTS_IN_AREA(iParticipants)
			iChosen = GET_RANDOM_INT_IN_RANGE(0,iTotal)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === PICK_CHECKPOINT_AREA - iChosen = ",iChosen)
			
			REPEAT NUM_CHECKPOINT_AREAS iArea
				IF iChosen >= iPreviousCount AND iChosen < (iParticipants[iArea] + iPreviousCount)
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === PICK_CHECKPOINT_AREA - iChosen => ",iPreviousCount," AND < ",(iParticipants[iArea] + iPreviousCount) )
					RETURN iArea
				ENDIF
				CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === PICK_CHECKPOINT_AREA - iPreviousCount = ",iPreviousCount," + ",iParticipants[iArea])
				iPreviousCount += iParticipants[iArea]
			ENDREPEAT
		BREAK
		
		//Weighted by order
		CASE 2
		
		BREAK
		
		//Top 1
		CASE 3
		
		BREAK

	ENDSWITCH
	
	CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === PICK_CHECKPOINT_AREA - Picking random area!")
	RETURN GET_RANDOM_INT_IN_RANGE(0,NUM_CHECKPOINT_AREAS)
	
ENDFUNC

/// PURPOSE: Picks whether to spawn air or land checkpoints
///    
/// RETURNS: TRUE if air is randomly selected, FALSE otherwise
///    
FUNC BOOL SHOULD_CHECKPOINT_AREA_BE_IN_AIR()
	
	#IF IS_DEBUG_BUILD
	IF MPGlobalsAmbience.iCPCollectionType >= 0
		RETURN (MPGlobalsAmbience.iCPCollectionType = 1)
	ENDIF
	#ENDIF
		
	IF IS_CHECKPOINT_AIR_AREA_DISABLED(serverBD.iCheckpointArea)
		RETURN FALSE
	ELIF IS_CHECKPOINT_LAND_AREA_DISABLED(serverBD.iCheckpointArea)
		RETURN TRUE
	ENDIF
	
	RETURN (GET_RANDOM_INT_IN_RANGE(0,2) = 1)

ENDFUNC

/// PURPOSE: Picks the variation to use within the area
///    
/// RETURNS: The variation index
///    
FUNC INT PICK_CHECKPOINT_VARIATION()
	
	#IF IS_DEBUG_BUILD
	IF MPGlobalsAmbience.iCPCollectionVariation >= 0
		RETURN MPGlobalsAmbience.iCPCollectionVariation
	ENDIF
	#ENDIF
	
	RETURN GET_RANDOM_INT_IN_RANGE(0,NUM_CHECKPOINT_GROUPS_PER_AREA)

ENDFUNC

FUNC INT GET_NUM_CHECKPOINTS_BASED_ON_PARTICIPANTS()

	INT iPart,iCount
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iPart
		PARTICIPANT_INDEX partID = INT_TO_PARTICIPANTINDEX(iPart)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(partID)
			iCount++
		ENDIF
	ENDREPEAT
	
	IF iCount >= g_sMPTunables.iCheckpoint_Checkpoint_Player_Threshold_3 
		RETURN g_sMPTunables.iCP_COLLECTION_MAX_ACTIVE_CHECKPOINTS_3
	ELIF iCount >= g_sMPTunables.iCheckpoint_Checkpoint_Player_Threshold_2
		RETURN g_sMPTunables.iCP_COLLECTION_MAX_ACTIVE_CHECKPOINTS_2
	ELIF iCount >= g_sMPTunables.iCheckpoint_Checkpoint_Player_Threshold_1
		RETURN g_sMPTunables.iCP_COLLECTION_MAX_ACTIVE_CHECKPOINTS
	ELSE
		RETURN g_sMPTunables.iCP_COLLECTION_MAX_ACTIVE_CHECKPOINTS
	ENDIF

ENDFUNC

/// PURPOSE: Picks which checkpoints to use and their positions
///    
PROC PICK_CHECKPOINTS()

	serverBD.iNumCheckpoints = GET_NUM_CHECKPOINTS_BASED_ON_PARTICIPANTS()

	serverBD.iRemaining = serverBD.iNumCheckpoints

	//Pick which quarter of the map to use
	serverBD.iCheckpointArea = PICK_CHECKPOINT_AREA()
			
	//Pick whether the checkpoints should be air or land/sea
	serverBD.bAir = SHOULD_CHECKPOINT_AREA_BE_IN_AIR()

	//Pick a variation
	INT iCheckpointVariation = PICK_CHECKPOINT_VARIATION()

	CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === PICK_CHECKPOINTS - Area = ",serverBD.iCheckpointArea," Air = ",serverBD.bAir," Variation = ",iCheckpointVariation)
	INT iSubType
	IF serverBD.bAir
		iSubType = 1
	ENDIF
	BROADCAST_SET_LAST_FM_EVENT_VARIATION(FM_EVENT_GET_VARIATION_FROM_EVENT_TYPE_AND_SUB_TYPE(FMMC_TYPE_CHECKPOINTS, serverBD.iCheckpointArea, iSubType))
	
	IF serverBD.iCheckpointArea = 1
	AND NOT serverBD.bAir
		serverBD.bOpenAirportGates = TRUE
	ENDIF
	
	INT j
	REPEAT serverBD.iNumCheckpoints j
		serverBD.serverCheckpointData[j].iCheckpointIndex = j
		serverBD.serverCheckpointData[j].vLocation = GET_VECTOR_FROM_CHECKPOINT_INDEX(serverBD.iCheckpointArea,serverBD.bAir,iCheckpointVariation,j)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === PICK_CHECKPOINTS ",j," = ",serverBD.serverCheckpointData[j].vLocation )
	ENDREPEAT

ENDPROC

FUNC MODEL_NAMES GET_RANDOM_CHALLENGE_VEHICLE()

	INT iRandom = GET_RANDOM_INT_IN_RANGE(0,2)
	
	SWITCH iRandom
		CASE 0	RETURN MAVERICK
		CASE 1	RETURN MAMMATUS
	ENDSWITCH

	RETURN DUMMY_MODEL_FOR_SCRIPT

ENDFUNC

PROC PICK_CHALLENGE_VEHICLES()

	INT i
	REPEAT NUM_VEHICLES_TO_SPAWN i
		serverBD.vehiclesToSpawn[i].vehModel = GET_RANDOM_CHALLENGE_VEHICLE()
	ENDREPEAT
	
ENDPROC

FUNC BOOL IS_SPAWN_POINT_SUITABLE_FOR_EVENT_PARTICIPATION(VECTOR vPlaneLoc, VECTOR vPlayerCoords)

	IF serverBD.eAMCP_Stage = AMCP_COLLECT
		
		VECTOR vAreaMidpoint = GET_MIDPOINT_VECTOR_FOR_CHECKPOINT_AREA(serverBD.iCheckpointArea)
		
		//Player is in checkpoint area
		IF VDIST(vPlayerCoords,vAreaMidpoint ) < 3500
			
			//Spawn point is in checkpoint area
			IF VDIST(vPlaneLoc,vAreaMidpoint) > 3500
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
	
ENDFUNC
FUNC BOOL IS_PLANE_SUITABLE_FOR_EVENT_PARTICIPATION(VECTOR vPlaneLoc)
	IF serverBD.eAMCP_Stage = AMCP_COLLECT
		VECTOR vAreaMidpoint = GET_MIDPOINT_VECTOR_FOR_CHECKPOINT_AREA(serverBD.iCheckpointArea)
		//Spawn point is in checkpoint area
		IF VDIST(vPlaneLoc,vAreaMidpoint) > 2500
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

PROC CLEAR_CHALLENGE_PLAYER_SPAWN_POINTS()

	IF bSetSpawnLocations
		CLEAR_PLAYER_NEXT_RESPAWN_LOCATION()
		CLEAR_SPECIFIC_SPAWN_LOCATION_FROM_THIS_SCRIPT()
		bSetSpawnLocations = FALSE
		CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === CLEAR_CHALLENGE_PLAYER_SPAWN_POINTS - CLEAR_SPECIFIC_SPAWN_LOCATION_FROM_THIS_SCRIPT")
	ENDIF

ENDPROC

PROC HANDLE_PLAYER_RESPAWNING()

	VECTOR vSpawnPoint
	FLOAT fSpawnHeading

	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF NOT IS_PLAYER_IN_CORONA()
		AND NOT IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), FALSE)
		AND NOT IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
			INT iVeh
			FLOAT fClosest = HIGHEST_INT
			INT iClosestVeh = -1
			INT iCheckPointBitSet
			VECTOR vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
			BOOL bInVehicle = IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			BOOL bPlayerPermanent = FM_EVENT_SKIP_SET_AS_PERNAMENT_CHECK()
							
			REPEAT NUM_VEHICLES_TO_SPAWN iVeh
				NETWORK_INDEX netID = serverBD.vehiclesToSpawn[iVeh].niVeh
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(netID)
				AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(netID))
					IF NOT bPlayerPermanent
						IF bInVehicle
						AND GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) = NET_TO_VEH(netID)
							 FM_EVENT_SET_PLAYER_IS_PERMANENT_PARTICIPANT_IN_FM_EVENT(TRUE)
						ENDIF
					ENDIF
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === Checking vehicle ",iVeh)
					IF GET_VEHICLE_SPAWNING_COORDS_CP_COLLECTION(iVeh,vSpawnPoint,fSpawnHeading)
						VECTOR vPlaneLoc = GET_ENTITY_COORDS(NET_TO_VEH(netID))
						IF VDIST(vPlaneLoc,vSpawnPoint) <= 10.0 //Vehicle near spawn location
						
							IF IS_PLANE_SUITABLE_FOR_EVENT_PARTICIPATION(vPlaneLoc)
								SET_BIT(iCheckPointBitSet, iVeh)
							ENDIF
							
							CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === near spawn location ",iVeh)
							IF IS_SPAWN_POINT_SUITABLE_FOR_EVENT_PARTICIPATION(vPlaneLoc,vPlayerCoords)
								IF NOT IS_ANY_OTHER_PLAYER_SPAWNING_OR_WARPING_NEAR_COORDS(vSpawnPoint, 100.0)
									FLOAT fDist = VDIST(vPlaneLoc,vPlayerCoords)
									IF fDist < fClosest
										fClosest = fDist
										iClosestVeh = iVeh
										CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === New closest vehicle ",iVeh)
									ENDIF
								ELSE
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === Another player is spawning near vehicle ",iVeh,", looking for another safe place...")
								ENDIF
							ELSE
								CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === vehicle not in checkpoint area ",iVeh)
							ENDIF	
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			#IF IS_DEBUG_BUILD
			IF GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iCheckPointBitSet != iCheckPointBitSet
				CPRINTLN(DEBUG_NET_AMBIENT, "[FMEG] === CHECKPOINT_COLLECTION === GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iCheckPointBitSet =  ",iCheckPointBitSet)
			ENDIF
			#ENDIF
			GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iCheckPointBitSet = iCheckPointBitSet
			
			IF IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
			OR (IS_ENTITY_IN_AIR(PLAYER_PED_ID()) AND NOT IS_PED_RAGDOLL(PLAYER_PED_ID()))
			OR playerBD[PARTICIPANT_ID_TO_INT()].iRewardCounter > 0	
				IF fClosest < HIGHEST_INT
				AND iClosestVeh >= 0 AND iClosestVeh < NUM_VEHICLES_TO_SPAWN
					IF GET_VEHICLE_SPAWNING_COORDS_CP_COLLECTION(iClosestVeh,vSpawnPoint,fSpawnHeading)
						bSetSpawnLocations = TRUE
						SETUP_SPECIFIC_SPAWN_LOCATION(vSpawnPoint, 0.0, 60.0, DEFAULT, 20.0, DEFAULT, FALSE, 10.0, DEFAULT, TRUE)
						SET_PLAYER_WILL_SPAWN_FACING_COORDS(vSpawnPoint, TRUE, FALSE)
						SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_NEAR_SPECIFIC_COORDS)
						CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === Setting to spawn near vehicle ",iClosestVeh," that is ",fClosest," metres away at ",vSpawnPoint," this frame.")
					ENDIF
				ENDIF
			ELSE
				CLEAR_CHALLENGE_PLAYER_SPAWN_POINTS()
			ENDIF
		ELSE
			CLEAR_CHALLENGE_PLAYER_SPAWN_POINTS()
		ENDIF
	ENDIF

ENDPROC

FUNC INT GET_CLOSEST_PLAYER_TO_VEH_SPAWN(INT iSpawnVeh)
	INT i
	INT closestPlayer
	FLOAT currentClosestDistance = 999999.99
	FLOAT tempDist
	VECTOR vSpawnPoint
	FLOAT fSpawnHeading
	IF GET_VEHICLE_SPAWNING_COORDS_CP_COLLECTION(iSpawnVeh,vSpawnPoint,fSpawnHeading)
		PRINTLN("[JS] === CHECKPOINT_COLLECTION === GET_CLOSEST_PLAYER_TO_VEH_SPAWN - ", vSpawnPoint, " and ", iSpawnVeh)
		FOR i = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() - 1
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
				PLAYER_INDEX vehCheckPID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
				IF IS_NET_PLAYER_OK(vehCheckPID)
					tempDist = VDIST_2D(GET_PLAYER_COORDS(vehCheckPID), vSpawnPoint)
					IF tempDist < currentClosestDistance
						currentClosestDistance = tempDist
						closestPlayer = i
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	PRINTLN("[JS] === CHECKPOINT_COLLECTION === GET_CLOSEST_PLAYER_TO_VEH_SPAWN - Participant ", closestPlayer, " is closest")
	RETURN closestPlayer
ENDFUNC

FUNC BOOL IS_OK_TO_SPAWN_VEHICLE(INT iSpawnVeh)
	VECTOR vSpawnPoint
	FLOAT fSpawnHeading
	IF GET_VEHICLE_SPAWNING_COORDS_CP_COLLECTION(iSpawnVeh,vSpawnPoint,fSpawnHeading)
		PRINTLN("[JS] === CHECKPOINT_COLLECTION ===  - IS_OK_TO_SPAWN_VEHICLE ", vSpawnPoint, " and ", iSpawnVeh)
		IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(vSpawnPoint, 10,DEFAULT,5)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SPAWN_CHALLENGE_VEHICLES()

	IF IS_AIR_CHECKPOINT()

		CHALLENGE_VEHICLE currentVeh = serverBD.vehiclesToSpawn[iSpawningVehicle]
		
		IF REQUEST_LOAD_MODEL(currentVeh.vehModel)
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(currentVeh.niVeh)
			
				VECTOR vSpawnPoint
				FLOAT fSpawnHeading
				IF GET_VEHICLE_SPAWNING_COORDS_CP_COLLECTION(iSpawningVehicle,vSpawnPoint,fSpawnHeading)
					serverBD.iClosestPlayer[iSpawningVehicle] = GET_CLOSEST_PLAYER_TO_VEH_SPAWN(iSpawningVehicle)
					PRINTLN("[JS] === CHECKPOINT_COLLECTION ===  - SPAWN_CHALLENGE_VEHICLES ", vSpawnPoint, " and ", iSpawningVehicle)
					//IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(vSpawnPoint, 15.0,DEFAULT,15.0)
					IF IS_BIT_SET(playerBD[serverBD.iClosestPlayer[iSpawningVehicle]].iVehSpawnBitSet, iSpawningVehicle)
						PRINTLN("[JS] === CHECKPOINT_COLLECTION ===  - SPAWN_CHALLENGE_VEHICLES - Bit Set for veh ", iSpawningVehicle, " closest player: ", serverBD.iClosestPlayer[iSpawningVehicle])
						IF CREATE_NET_VEHICLE(serverBD.vehiclesToSpawn[iSpawningVehicle].niVeh, currentVeh.vehModel, vSpawnPoint, fSpawnHeading, DEFAULT, DEFAULT, DEFAULT,DEFAULT,TRUE,FALSE)	
							PRINTLN("[JS] === CHECKPOINT_COLLECTION === SPAWN_CHALLENGE_VEHICLES - IS_POINT_OK_FOR_NET_ENTITY_CREATION returned TRUE at ", vSpawnPoint,	"for vehicle ", iSpawningVehicle)	
							SET_VEHICLE_DOORS_LOCKED(NET_TO_VEH(serverBD.vehiclesToSpawn[iSpawningVehicle].niVeh), VEHICLELOCK_UNLOCKED)
							SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_VEH(serverBD.vehiclesToSpawn[iSpawningVehicle].niVeh), TRUE)
							SET_VEHICLE_INFLUENCES_WANTED_LEVEL(NET_TO_VEH(serverBD.vehiclesToSpawn[iSpawningVehicle].niVeh), FALSE)
							SET_MENTAL_STATE_WILL_IGNORE_THIS_VEHICLE(NET_TO_VEH(serverBD.vehiclesToSpawn[iSpawningVehicle].niVeh),TRUE)
							
							IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh", DECOR_TYPE_INT)
			                    DECOR_SET_INT(NET_TO_VEH(serverBD.vehiclesToSpawn[iSpawningVehicle].niVeh), "Not_Allow_As_Saved_Veh", MP_NOT_ALLOWED_AS_SAVED_VEHICLE_DEC_SIMEON)
								PRINTLN("     ---------->     PASS THE PARCEL - DECORATOR 'Not_Allow_As_Saved_Veh' SET")
			  				ENDIF
							
							//Prevents the vehicle from being used for activities that use passive mode - MJM 
							IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
								INT iDecoratorValue
								IF DECOR_EXIST_ON(NET_TO_VEH(serverBD.vehiclesToSpawn[iSpawningVehicle].niVeh), "MPBitset")
									iDecoratorValue = DECOR_GET_INT(NET_TO_VEH(serverBD.vehiclesToSpawn[iSpawningVehicle].niVeh), "MPBitset")
								ENDIF
								SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_PASSIVE_MODE_VEHICLE)
								SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_MODDABLE_VEHICLE)
								DECOR_SET_INT(NET_TO_VEH(serverBD.vehiclesToSpawn[iSpawningVehicle].niVeh), "MPBitset", iDecoratorValue)
							ENDIF
							
							//SET_MODEL_AS_NO_LONGER_NEEDED(serverBD.vehiclesToSpawn[iSpawningVehicle].vehModel)
							
							serverBD.iClosestPlayer[iSpawningVehicle] = -1
							CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === SPAWN_CHALLENGE_VEHICLES - Created vehicle ",iSpawningVehicle ," at  ",vSpawnPoint)
						ENDIF
					ELSE
						CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === SPAWN_CHALLENGE_VEHICLES - Point not ok, not spawning vehicle ",iSpawningVehicle)
						
					ENDIF
				ENDIF
			ENDIF
			iSpawningVehicle++
			CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === SPAWN_CHALLENGE_VEHICLES - iSpawningVehicle = ",iSpawningVehicle)
			IF iSpawningVehicle >= NUM_VEHICLES_TO_SPAWN
				iSpawningVehicle = 0
				RETURN TRUE
			ENDIF
		ENDIF
		
		RETURN FALSE

	ENDIF
	
	RETURN TRUE

ENDFUNC

FUNC BOOL ARE_ALL_VEHICLES_DEAD()

	INT iVeh
	REPEAT NUM_VEHICLES_TO_SPAWN iVeh
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.vehiclesToSpawn[iVeh].niVeh)
		AND NOT IS_ENTITY_DEAD(NET_TO_ENT(serverBD.vehiclesToSpawn[iVeh].niVeh))
			RETURN FALSE	
		ENDIF
	ENDREPEAT
	
	RETURN TRUE

ENDFUNC

//PROC PICK_CHECKPOINTS()
//
//	INT i
//	REPEAT ACTIVE_CHECKPOINT_GROUPS i
//		INT iCheckpointGroup = GET_RANDOM_CHECKPOINT_GROUP(i)
//
//		CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === PICK_CHECKPOINTS - Group ",i," = ",iCheckpointGroup)
//		INT j
//		REPEAT NUM_CHECKPOINT_PER_GROUP j
//			serverBD.serverCheckpointData[j+(i*NUM_CHECKPOINT_PER_GROUP)].iCheckpointIndex = iCheckpointGroup
//			serverBD.serverCheckpointData[j+(i*NUM_CHECKPOINT_PER_GROUP)].vLocation = GET_VECTOR_FROM_CHECKPOINT_INDEX(iCheckpointGroup,j)
//			CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === PICK_CHECKPOINTS ",i," ",j," = ",serverBD.serverCheckpointData[j+(i*NUM_CHECKPOINT_PER_GROUP)].vLocation )
//		ENDREPEAT
//	ENDREPEAT
//	
//
//ENDPROC


/// PURPOSE: Process players that have reached checkpoints, determine order
///    
/// PARAMS:
///    iParticipant - The current participant
PROC SERVER_PROCESS_PLAYERS_IN_CHECKPOINTS(INT iParticipant)
	
	INT iPlayer, iFinishTime, iShortestTime

	PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
	
	iPlayer = NATIVE_TO_INT(PlayerId)
	IF MPGlobalsAmbience.CPCData.iCheckpointReached[iPlayer] >= 0
		INT iCheckpoint = MPGlobalsAmbience.CPCData.iCheckpointReached[iPlayer]
	
		iFinishTime = MPGlobalsAmbience.CPCData.iReachedCheckpointTime[iPlayer]
		
		IF NOT IS_BIT_SET(serverBD.iCheckpointCollectedBS[GET_LONG_BITSET_INDEX(iCheckpoint)],GET_LONG_BITSET_BIT(iCheckpoint))
		
			IF IS_BIT_SET(MPGlobalsAmbience.CPCData.iReachedCheckpointEventReceivedBS[GET_LONG_BITSET_INDEX(iCheckpoint)],GET_LONG_BITSET_BIT(iCheckpoint))
				//Hint cam: kill the hint cam after a checkpoint has been collected
				//KILL_CHASE_HINT_CAM(sLookCheckpoint.s_CheckPointChaseCam)
				IF HAS_NET_TIMER_EXPIRED(iServerProcessWinnerTimer, SERVER_PROCESS_WINNER_DELAY)
					IF iFinishTime <> 0
						IF (iFinishTime < iShortestTime)
						OR iShortestTime = 0
							iShortestTime = iFinishTime
							serverBD.serverCheckpointData[iCheckpoint].iCollectedBy = iParticipant
							MPGlobalsAmbience.CPCData.iCheckpointReached[iPlayer] = -1
							SET_BIT(serverBD.iCheckpointCollectedBS[GET_LONG_BITSET_INDEX(iCheckpoint)],GET_LONG_BITSET_BIT(iCheckpoint))
							serverBD.iRemaining--
							CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === Checkpoint Collected = ",iCheckpoint)
							CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === iShortestTime = ",iShortestTime)
							CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === iCollectedBy = ",iParticipant)
							CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === COLLECTED BY PLAYER = ",GET_PLAYER_NAME(PlayerId))
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE: Sends tickers to players to inform them of any relevant leaderboard position changes
///    
/// PARAMS:
///    iOldPart - The participant who previously occupied this position on the leaderboard
///    iNewPart - The particpant who is replacing them
///    iPosition - The position on the leaderboard
PROC SEND_POSITION_UPDATE_TICKERS(INT iOldPart, INT iNewPart,INT iPosition)
	SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
			
	PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iNewPart))	
	
	CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === New player in place ",iPosition," = ",GET_PLAYER_NAME(PlayerId))
	CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === Attempt = ",playerBD[iNewPart].iRewardCounter)
	
	UNUSED_PARAMETER(iOldPart)
	
	IF iPosition = 0 //In first place
	
		TickerEventData.TickerEvent = TICKER_EVENT_SCORE_IN_CHALLENGE
		TickerEventData.playerID = PlayerId

		CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === TickerEventData.playerID = ",GET_PLAYER_NAME(TickerEventData.playerID ))
		BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT())
	
	ENDIF
	
ENDPROC

/// PURPOSE: Sorts the players in order of score and updates the server data for the leaderboard
///    
/// PARAMS:
///    iParticipant - The current participant
PROC SERVER_PROCESS_PLAYER_SCORE(INT iParticipant)
		
	//Find the top three players
	INT iChallengeAttempt
	
	iChallengeAttempt = playerBD[iParticipant].iRewardCounter
	
	CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === SERVER_PROCESS_PLAYER_SCORE - iParticipant",iParticipant)

	//IF iChallengeAttempt <> 0
	
		INT iWinner
		INT iTemp = -2
		REPEAT NUM_LEADERBOARD_PLAYERS iWinner
			
			IF serverBD.iWinnerPart[iWinner] = iParticipant
				iWinner = NUM_LEADERBOARD_PLAYERS //Player is already on leaderboard at this position, don't bother checking lower positions
				CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === SERVER_PROCESS_PLAYER_SCORE - Player is already on leaderboard at this position")
			ELSE
				IF iTemp >= -1
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === iTemp >= -1 ",iTemp)
					IF iTemp >= 0
						//New entry, rearrange the list
						SWAP(serverBD.iWinnerPart[iWinner],iTemp)
					ENDIF
				ELIF (serverBD.iWinnerPart[iWinner] < 0 //Slot empty
				OR (iChallengeAttempt > playerBD[serverBD.iWinnerPart[iWinner]].iRewardCounter)) //We have higher score
					iTemp = serverBD.iWinnerPart[iWinner]
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === iTemp = ",iTemp)
					IF iChallengeAttempt <> 0
						SEND_POSITION_UPDATE_TICKERS(serverBD.iWinnerPart[iWinner],iParticipant,iWinner)
					ENDIF
					serverBD.iWinnerPart[iWinner] = iParticipant
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === SERVER_PROCESS_PLAYER_SCORE 1- serverBD.iWinnerPart[",iWinner,"] = ",iParticipant)
					
					//Check for any other places the player appears and remove them
					INT i
					FOR i = iWinner+1 TO NUM_LEADERBOARD_PLAYERS-1
						IF serverBD.iWinnerPart[i] = iParticipant
							serverBD.iWinnerPart[i] = -1
							CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === SERVER_PROCESS_PLAYER_SCORE 2- serverBD.iWinnerPart[",i,"] = -1")
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
		ENDREPEAT
	//ENDIF
	
ENDPROC

/// PURPOSE: Clears the server data for an inactive participant
///    
/// PARAMS:
///    iParticipant - The participant to check
PROC SERVER_PROCESS_INACTIVE_PARTICPANT(INT iParticipant)

	IF IS_BIT_SET(serverBD.iActivePartBS,iParticipant) //Have been active but are no longer
		INT iPart
		REPEAT NUM_LEADERBOARD_PLAYERS iPart
			IF serverBD.iWinnerPart[iPart] = iParticipant
				serverBD.iWinnerPart[iPart] = -1
				iPart = NUM_LEADERBOARD_PLAYERS
				CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === Clearing leaderboard data for participant ",iParticipant)
			ENDIF
		ENDREPEAT
		
		INT iCheckpoint
		REPEAT serverBD.iNumCheckpoints iCheckpoint
			IF serverBD.serverCheckpointData[iCheckpoint].iCollectedBy = iParticipant
				serverBD.serverCheckpointData[iCheckpoint].iCollectedBy = -1
			ENDIF
		ENDREPEAT
		CLEAR_BIT(serverBD.iActivePartBS,iParticipant)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === Participant ",iParticipant," has left the script.")
	ENDIF
	
ENDPROC

//PROC SERVER_PROCESS_ACTIVE_PLAYER_CHECK(PARTICIPANT_INDEX partID)
//
//	PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX(partID)
//	IF NETWORK_IS_PLAYER_ACTIVE(playerID)
//		FM_EVENT_MAINTAIN_ACTIVE_PLAYER_CHECK(playerID)
//	ENDIF
//
//ENDPROC

/// PURPOSE: Main server participant loop
///    
PROC SERVER_PROCESS_PARTICIPANT_CHECKS()

	INT iTempPeakParts, iTempPeakQualifiers
	INT iParticipant, iPlayerBitset
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			SET_BIT(iPlayerBitSet, iParticipant)
		ELSE
			SERVER_PROCESS_INACTIVE_PARTICPANT(iParticipant)
		ENDIF
	ENDREPEAT
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF IS_BIT_SET(iPlayerBitSet, iParticipant)
			IF HAS_PARTICIPANT_PARTICIPATED_IN_EVENT(iParticipant)
				iTempPeakQualifiers++
			ENDIF
			iTempPeakParts++
			//SERVER_PROCESS_ACTIVE_PLAYER_CHECK(INT_TO_PARTICIPANTINDEX(iParticipant))
			SERVER_PROCESS_PLAYERS_IN_CHECKPOINTS(iParticipant)
			SERVER_PROCESS_PLAYER_SCORE(iParticipant)
			SET_BIT(serverBD.iActivePartBS,iParticipant)
		ENDIF
	ENDREPEAT
	
	
	IF iTempPeakParts > serverBD.iPeakParticipants
		serverBD.iPeakParticipants = iTempPeakParts
		PRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === SERVER_PROCESS_PARTICIPANT_CHECKS - iTempPeak (",iTempPeakParts,") is GREATER than player's participating count so far (",serverBD.iPeakParticipants,"), updating value.")
	ELSE
		serverBD.iPlayersLeftInProgress = serverBD.iPeakParticipants  - iTempPeakParts
	ENDIF
	
	IF iTempPeakQualifiers > serverBD.iPeakQualifiers
		serverBD.iPeakQualifiers = iTempPeakQualifiers
		PRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === SERVER_PROCESS_PARTICIPANT_CHECKS - iTempPeakQualifiers (",iTempPeakQualifiers,") is GREATER than player's participating count so far (",serverBD.iPeakQualifiers,"), updating value.")
	ENDIF
ENDPROC

/// PURPOSE: To check if the event should end because checkpoints have all been collected
///    
/// RETURNS: TRUE if all checkpoints collected, FALSE otherwise
///    
FUNC BOOL HAVE_ALL_CHECKPOINTS_BEEN_COLLECTED()
	
//	INT i
//	REPEAT serverBD.iNumCheckpoints i
//		IF NOT IS_BIT_SET(serverBD.iCheckpointCollectedBS[GET_LONG_BITSET_INDEX(i)],GET_LONG_BITSET_BIT(i))
//			RETURN FALSE
//		ENDIF
//	ENDREPEAT
	
	IF serverBD.iRemaining > 0
		RETURN FALSE
	ENDIF
	
	IF HAS_NET_TIMER_EXPIRED(serverBD.allCollectedTimer,SERVER_PROCESS_WINNER_DELAY)
	
		SET_BIT(serverBD.iServerBitSet,SERVER_BS_ALL_CP_COLLECTED)	
		
		CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === HAVE_ALL_CHECKPOINTS_BEEN_COLLECTED = TRUE")
		RETURN TRUE
	
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_TIE_BREAK_REQUIRED()
	
	RETURN FALSE

//	IF serverBD.iWinnerPart[0] >= 0
//		INT iScore = playerBD[serverBD.iWinnerPart[0]].iRewardCounter
//
//		INT iCounter
//		REPEAT NUM_LEADERBOARD_PLAYERS iCounter
//			IF serverBD.iWinnerPart[iCounter] >= 0
//				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(serverBD.iWinnerPart[iCounter]))	
//					IF iScore = playerBD[serverBD.iWinnerPart[iCounter]].iRewardCounter
//						serverBD.iNumMatchingPlayers++
//					ELSE
//						CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === IS_TIE_BREAK_REQUIRED 1 - serverBD.iNumMatchingPlayers = ",serverBD.iNumMatchingPlayers)
//						RETURN serverBD.iNumMatchingPlayers > 1
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDREPEAT
//	ENDIF
//
//	CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === IS_TIE_BREAK_REQUIRED 2 - serverBD.iNumMatchingPlayers = ",serverBD.iNumMatchingPlayers)
//	RETURN serverBD.iNumMatchingPlayers > 1
	
ENDFUNC

/// PURPOSE: Check if a reward has been given to any player
///    
/// RETURNS: TRUE if any player has received the reward, FALSE otherwise
///    
FUNC BOOL HAS_SHOWN_END_MESSAGES()

	INT iParticipant
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX( iParticipant))	
			PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX( iParticipant))
			IF NOT FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(playerID)
				IF NOT IS_BIT_SET(playerBD[iParticipant].iClientBitSet,CLIENT_BS_SHOWN_MESSAGE)
				OR playerBD[iParticipant].eAMCP_Stage != AMCP_END
				OR (IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_ALL_CP_COLLECTED) AND NOT IS_BIT_SET(playerBD[iParticipant].iClientBitSet,CLIENT_BS_GIVEN_END_REWARD))
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === Server not shown message to participant ",iParticipant)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE

ENDFUNC

//----------------------
//	Main Functions
//----------------------

//Helper proc for setting client stage
PROC SET_LOCAL_AMCP_STAGE(AMCP_RUN_STAGE stage)
	playerBD[PARTICIPANT_ID_TO_INT()].eAMCP_Stage = stage
	CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === playerBD.eAMCP_Stage = ",GET_AMCP_STAGE_STRING(stage))
ENDPROC

//Helper proc for setting server stage
PROC SET_SERVER_AMCP_STAGE(AMCP_RUN_STAGE stage)
	serverBD.eAMCP_Stage = stage
	CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === serverBD.eAMCP_Stage = ",GET_AMCP_STAGE_STRING(stage))
ENDPROC

/// PURPOSE: Main function for processing the current checkpoint in the loop
///    
/// PARAMS:
///    iCheckpoint - The current checkpoint to be processed
PROC PROCESS_CHECKPOINT(INT iCheckpoint)
	DRAW_CHECKPOINTS(localCheckpointData[iCheckpoint],serverBD.serverCheckpointData[iCheckpoint],iCheckpoint)
	IF bCheckpointInRange[iCheckPoint]
		CLIENT_PROCESS_REACHING_CHECKPOINTS(serverBD.serverCheckpointData[iCheckpoint],iCheckpoint)
		GIVE_REWARDS(serverBD.serverCheckpointData[iCheckpoint],iCheckpoint)
	ENDIF
ENDPROC

PROC CLIENT_MAINTAIN_PARTICIPANT_LOOP()

	IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iClientPart))
		PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iClientPart))
		IF NETWORK_IS_PLAYER_ACTIVE(playerID)
			FM_EVENT_MAINTAIN_ACTIVE_PLAYER_CHECK(playerID)
		ENDIF
	ENDIF
	
	iClientPart++
	IF iClientPart >= NETWORK_GET_MAX_NUM_PARTICIPANTS()
		iClientPart = 0
	ENDIF

ENDPROC

//Used when event is hidden
PROC CLIENT_TIDYUP()

	IF NOT IS_BIT_SET(iLocalBitset,AMCP_BS_DONE_TIDYUP)
		bRenderAll = TRUE
		CLEANUP_CHECKPOINTS()
		CLEAR_ALL_HELP_MESSAGES()
		HANDLE_CHALLENGE_BLIPS(TRUE)
		CLEAR_PLAYER_NEXT_RESPAWN_LOCATION()
		CLEAR_SPECIFIC_SPAWN_LOCATION_FROM_THIS_SCRIPT()
		SET_BIT(iLocalBitset,AMCP_BS_DONE_TIDYUP)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === CLIENT_TIDYUP")
		CLEAN_UP_LOCAL_PLAYERS_CHECKPOINT_BITSET()
	ENDIF
	
ENDPROC

PROC CLEAR_PLAYER_SCORE()

	IF playerBD[PARTICIPANT_ID_TO_INT()].iRewardCounter != 0
		playerBD[PARTICIPANT_ID_TO_INT()].iRewardCounter = 0
		CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === CLEAR_PLAYER_SCORE - playerBD[PARTICIPANT_ID_TO_INT()].iRewardCounter = 0")
	ENDIF
	
ENDPROC

PROC MAINTAIN_INTRO_SHARD()
	IF NOT IS_BIT_SET(iLocalBitset,AMCP_BS_INTRO_SHARD_DISPLAYED)
	AND NOT IS_RADAR_HIDDEN()
		STRING sShard
		IF serverBD.bAir
			sShard = "CPC_TITLEA"
		ELSE
			sShard = "CPC_TITLE"
		ENDIF								
		SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_START,sShard,"CPC_START", GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE(), DEFAULT, GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE())
		FLASH_MINIMAP_DISPLAY()
		SET_BIT(iLocalBitset,AMCP_BS_INTRO_SHARD_DISPLAYED)
	ENDIF
ENDPROC
PROC CLIENT_CHECK_SPAWN_LOCATIONS()
	IF IS_AIR_CHECKPOINT()
		INT iLoop
		REPEAT NUM_VEHICLES_TO_SPAWN iLoop
			IF serverBD.iClosestPlayer[iLoop] = PARTICIPANT_ID_TO_INT()
				//PRINTLN("[JS] I am the closest player to vehicle: ", iLoop)
				IF IS_OK_TO_SPAWN_VEHICLE(iLoop)
					IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iVehSpawnBitSet, iLoop)
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iVehSpawnBitSet, iLoop)
						PRINTLN("[JS] === AM_CHALLENGES === CLIENT_PROCESSING - Setting bit ", iLoop)
					ENDIF
				ELSE
					IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iVehSpawnBitSet, iLoop)
						CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iVehSpawnBitSet, iLoop)
						PRINTLN("[JS] === AM_CHALLENGES === CLIENT_PROCESSING - Clearing bit ", iLoop)
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iVehSpawnBitSet, iLoop)
					CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iVehSpawnBitSet, iLoop)
					PRINTLN("[JS] === AM_CHALLENGES === CLIENT_PROCESSING - Clearing bit ", iLoop)
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC
/// PURPOSE: Main client processing loop
///    
///    
PROC CLIENT_PROCESSING()

	INT iRepeat
	CONST_INT ciMAX_STAGGERED_CHECK_POINTS 3
	
	//maintain client staggered check
	CLIENT_MAINTAIN_PARTICIPANT_LOOP()
	
	IF DRAW_CHALLENGE_DPAD_FOR_DEV_SPECTATOR()
		DRAW_CHALLENGES_DPAD_LEADERBOARD()
	ENDIF
	
	IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_SCTV(PLAYER_ID())
	OR FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID())
		CLEAR_PLAYER_SCORE()
	ENDIF

	IF NOT FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(PLAYER_ID())
	AND NOT SHOULD_HIDE_AMBIENT_EVENT(GET_iPI_TYPE_M3_CONST_FROM_FM_EVENT_TYPE(FMMC_TYPE_CHECKPOINTS))
		IF IS_IT_SAFE_TO_DISPLAY_FM_EVENT_UI()
		
			IF IS_BIT_SET(iLocalBitset,AMCP_BS_DONE_TIDYUP)
				CLEAR_BIT(iLocalBitset,AMCP_BS_DONE_TIDYUP)
				CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === CLIENT_TIDYUP CLEAR")
			ENDIF

			SWITCH playerBD[PARTICIPANT_ID_TO_INT()].eAMCP_Stage
			
				CASE AMCP_WARN
					IF serverBD.eAMCP_Stage = AMCP_WAIT
						FM_EVENT_SET_PLAYER_WAITING_FOR_FM_EVENT_TO_START(TRUE)
						SET_LOCAL_AMCP_STAGE(AMCP_WAIT)
					ELIF serverBD.eAMCP_Stage >= AMCP_START	
						SET_LOCAL_AMCP_STAGE(AMCP_WAIT) //Event already started, don't warn
					ENDIF
					
				BREAK
				
				CASE AMCP_WAIT
					DO_HELP_TEXT_AND_MESSAGES(6)
					
					IF serverBD.eAMCP_Stage >= AMCP_START //Event started, go!
						SET_LOCAL_AMCP_STAGE(AMCP_START) 
					ENDIF
					//DRAW_GENERIC_TIMER(IMAX(g_sMPTunables.iCheckpoint_Event_Start_Countdown_Time-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.warningTimer),0),"HUD_STARTING")
					DO_HELP_TEXT_AND_MESSAGES(2)
					HANDLE_CHALLENGE_BLIPS()
					HANDLE_AMBIENT_EVENT_START_COUNTDOWN_AUDIO(g_sMPTunables.iCheckpoint_Event_Start_Countdown_Time-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.warningTimer))
					BOTTOM_RIGHT_UI_STARTING_EVENT_TIMER(IMAX(g_sMPTunables.iCheckpoint_Event_Start_Countdown_Time-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.warningTimer),0), GET_START_TIMER_NAME_FOR_EVENT())
					HANDLE_PLAYER_RESPAWNING()
				BREAK
			
				CASE AMCP_START
					IF serverBD.eAMCP_Stage > AMCP_START
							
						FM_EVENT_SET_PLAYER_WAITING_FOR_FM_EVENT_TO_START(FALSE)
						COMMON_FREEMODE_AMBIENT_EVENTS_SET_UP(FMMC_TYPE_CHECKPOINTS)
						
						IF NOT FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(PLAYER_ID())
							MAINTAIN_INTRO_SHARD()
						ELSE
							SET_BIT(iLocalBitset,AMCP_BS_INTRO_SHARD_DISPLAYED)
						ENDIF
						
						IF serverBD.bOpenAirportGates
							UNLOCK_AIRPORT_GATES()
						ENDIF
						IF serverBD.bAir
							MPGlobalsAmbience.iCurrentCPCType = 1
						ELSE
							MPGlobalsAmbience.iCurrentCPCType = 0
						ENDIF
						Clear_Any_Objective_Text_From_This_Script()
									
						IF NOT FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(PLAYER_ID())
							IF IS_AIR_CHECKPOINT()
								ADD_MISSION_SPAWN_OCCLUSION_SPHERE(<<678, 794, 206>>, 8) // 2432756 - Player spawned in large bush.
								ADD_MISSION_SPAWN_OCCLUSION_SPHERE(<<1911.3,4714.6,41.1>>,8)  // 2446515 - Player spawned in another large bush.
								ADD_MISSION_SPAWN_OCCLUSION_SPHERE(<<688.5021, 735.4581, 181.2960>>,8) // 2457124 - Player spawned on a cliff.
							ENDIF
						ENDIF
									
						SET_LOCAL_AMCP_STAGE(AMCP_COLLECT)
					ENDIF	
				BREAK 
			 
				CASE AMCP_COLLECT

					IF serverBD.eAMCP_Stage > AMCP_COLLECT
						SET_LOCAL_AMCP_STAGE(AMCP_MESSAGE)
					ELSE
						
						IF NOT bRenderAll
							IF SHOULD_RENDER_ALL_CHECKPOINT_BLIPS_THIS_FRAME()
								bRenderAll = TRUE
							ENDIF
						ENDIF
					
						INT iCheckpoint
						REPEAT serverBD.iNumCheckpoints iCheckpoint
							IF bCheckpointInDrawingRange[iCheckpoint]
							OR bRenderAll
								PROCESS_CHECKPOINT(iCheckpoint)
							ENDIF
						ENDREPEAT
						
						REPEAT ciMAX_STAGGERED_CHECK_POINTS iRepeat
							bCheckpointInDrawingRange[iCheckpointStagger] = IS_CHECKPOINT_IN_DRAWING_RANGE(iCheckpointStagger)
							bCheckpointInRange[iCheckpointStagger] = IS_CHECKPOINT_IN_PROCESSING_RANGE(iCheckpointStagger)	
						
							IF NOT bCheckpointInRange[iCheckpointStagger]
								PROCESS_CHECKPOINT(iCheckpointStagger)
							ENDIF
							
							iCheckpointStagger++
							IF iCheckpointStagger >= serverBD.iNumCheckpoints
								iCheckpointStagger = 0
							ENDIF
						ENDREPEAT
						HANDLE_CHALLENGE_BLIPS(FALSE)
						DO_HELP_TEXT_AND_MESSAGES(4)
						DO_HELP_TEXT_AND_MESSAGES(5)
						
						//If someone in first that's not me, do help
						IF IS_ANYONE_IN_PLACE(0)
						AND NOT IS_LOCAL_PLAYER_IN_PLACE(0)
							SET_DO_FM_EVENT_FIRST_PLACE_HELP_IF_NEEDED()
						ENDIF
						
						// EXPANDED RADAR
						IF NOT IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())
						AND NOT IS_PAUSE_MENU_ACTIVE()
						AND NOT IS_HELP_MESSAGE_ON_SCREEN()
							SET_DO_FM_EVENT_EXPANDED_RADAR_HELP_IF_NEEDED()
						ENDIF
						
						HANDLE_CHECKPOINT_AUDIO()
					ENDIF
					HANDLE_SENDING_CACHED_HOST_CHECKPOINT_ENVET()
					CLIENT_PROCESS_SORT_FOR_LBD()
					MAINTAIN_INTRO_SHARD()
					DRAW_EVENT_UI()	
					HANDLE_PLAYER_RESPAWNING()
					//Call hint cam procs
	//				MAINTAIN_CLOSEST_CHECKPOINT(sLookCheckpoint)
	//				CONTROL_HINT_CAM()
				BREAK 
				
				CASE AMCP_MESSAGE
					GIVE_END_REWARDS()
					SHOW_END_MESSAGES()
					CLEANUP_CHECKPOINTS()
					HANDLE_CHALLENGE_BLIPS(TRUE)
					KILL_AMBIENT_EVENT_COUNTDOWN_AUDIO()
					DRAW_EVENT_UI()	

					IF MAINTAIN_FM_EVENTS_END_UI(sEndUiVars,NOT IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_LOW_PARTICIPANTS))
					OR IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_LOW_PARTICIPANTS)
						IF IS_AIR_CHECKPOINT()
							SET_LOCAL_AMCP_STAGE(AMCP_EXPLODE)
						ELSE
							SET_LOCAL_AMCP_STAGE(AMCP_END)
						ENDIF
					ENDIF
				BREAK
				
				CASE AMCP_EXPLODE
					IF serverBD.eAMCP_Stage > AMCP_EXPLODE
						SET_LOCAL_AMCP_STAGE(AMCP_END)
					ENDIF
				BREAK
				
				CASE AMCP_END
					
				BREAK
				
			ENDSWITCH
			
		ELSE
			CLIENT_TIDYUP()
		ENDIF
	ELSE
		CLEAR_PLAYER_SCORE()
		CLIENT_TIDYUP()
	ENDIF	

	HANDLE_OBJECTIVE_TEXT()
	MAINTAIN_EXPLODE_VEH()
	HANDLE_CHALLENGE_VEHICLE_CLEANUP()

ENDPROC

/// PURPOSE: Main server processing loop
///    
PROC SERVER_PROCESSING()
	
	IF SHOULD_BAIL_FM_EVENT_DUE_TO_LOW_SESSION_NUMBERS(DEFAULT,DEFAULT,FALSE)
	#IF IS_DEBUG_BUILD
	OR bDebugBail
	#ENDIF
		IF NOT IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_LOW_PARTICIPANTS)
			serverBD.iEndTime = NATIVE_TO_INT(GET_NETWORK_TIME())
			SET_BIT(serverBD.iServerBitSet,SERVER_BS_LOW_PARTICIPANTS)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === SET_BIT(serverBD.iServerBitSet,SERVER_BS_LOW_PARTICIPANTS)")
		ENDIF
	ENDIF
	
	SWITCH serverBD.eAMCP_Stage
	
		CASE AMCP_WARN
			//Delay start to allow participants time to join
			IF IS_AIR_CHECKPOINT()
				IF SPAWN_CHALLENGE_VEHICLES()
					SET_SERVER_AMCP_STAGE(AMCP_WAIT)
				ENDIF
			ELSE
				SET_SERVER_AMCP_STAGE(AMCP_WAIT)
			ENDIF
		BREAK
		
		CASE AMCP_WAIT
			IF HAS_NET_TIMER_EXPIRED(serverBD.warningTimer,g_sMPTunables.iCheckpoint_Event_Start_Countdown_Time)
			OR IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_LOW_PARTICIPANTS)
			#IF IS_DEBUG_BUILD
			OR MPGlobalsAmbience.bSkipWarningDelay
				MPGlobalsAmbience.bSkipWarningDelay = FALSE
			#ENDIF
				SET_SERVER_AMCP_STAGE(AMCP_START)
			ENDIF
			SPAWN_CHALLENGE_VEHICLES()
		BREAK
	
		CASE AMCP_START
			serverBD.StartTime = GET_NETWORK_TIME()
			SET_SERVER_AMCP_STAGE(AMCP_COLLECT)
		BREAK 
	
		CASE AMCP_COLLECT
			IF HAVE_ALL_CHECKPOINTS_BEEN_COLLECTED()
			OR HAS_NET_TIMER_EXPIRED(serverBD.eventTimer,g_sMPTunables.iCheckpoint_Event_Time_Limit)
			OR IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_LOW_PARTICIPANTS)
			#IF IS_DEBUG_BUILD
			OR bSkipEventTime
			OR MPGlobalsAmbience.bEndCurrentFreemodeEvent
			#ENDIF
				IF NOT IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_SUDDEN_DEATH)
				AND IS_TIE_BREAK_REQUIRED()
					
					REINIT_NET_TIMER(serverBD.eventTimer)
					SET_BIT(serverBD.iServerBitSet,SERVER_BS_SUDDEN_DEATH)
				ELSE
					SET_SERVER_AMCP_STAGE(AMCP_MESSAGE)
					serverBD.iEndTime = NATIVE_TO_INT(GET_NETWORK_TIME())
				ENDIF
			ENDIF
			SERVER_PROCESS_PARTICIPANT_CHECKS()
						
			iServerCheckpointStagger++

			IF iServerCheckpointStagger >= serverBD.iNumCheckpoints
				iServerCheckpointStagger = 0
			ENDIF
			
			SPAWN_CHALLENGE_VEHICLES()
		BREAK 
		
		CASE AMCP_MESSAGE
			IF HAS_NET_TIMER_EXPIRED(serverBD.messageTimer,CP_MESSAGE_TIME)
			OR HAS_SHOWN_END_MESSAGES()
				IF IS_AIR_CHECKPOINT()
					REINIT_NET_TIMER(serverBD.ExplodeVehTimer)
					SET_SERVER_AMCP_STAGE(AMCP_EXPLODE)
				ELSE
					SET_SERVER_AMCP_STAGE(AMCP_END)
				ENDIF
			ENDIF
			SPAWN_CHALLENGE_VEHICLES()
		BREAK
		
		CASE AMCP_EXPLODE
			IF ARE_ALL_VEHICLES_DEAD()
				SET_SERVER_AMCP_STAGE(AMCP_END)	
			ENDIF
		BREAK
		
		CASE AMCP_END
		
		BREAK

	ENDSWITCH

ENDPROC

/// PURPOSE: Initialises the client data
///    
/// RETURNS: TRUE if initialised, FALSE otherwise
///    
FUNC BOOL INIT_CLIENT()

	CHECKPOINT_COLLECTION_STRUCT emptyStruct
	MPGlobalsAmbience.CPCData = emptyStruct

	INT iPlayer
	REPEAT NUM_NETWORK_REAL_PLAYERS() iPlayer

		MPGlobalsAmbience.CPCData.iCheckpointReached[iPlayer] = -1

	ENDREPEAT
	
	INT i
	REPEAT NUM_LEADERBOARD_PLAYERS i
		chDPStruct.challengeLbdStruct[i].playerID = INVALID_PLAYER_INDEX()
	ENDREPEAT

	#IF IS_DEBUG_BUILD
	CREATE_WIDGETS()
	#ENDIF

	RETURN TRUE
ENDFUNC

/// PURPOSE: Initialises the server data
///    
/// RETURNS: TRUE if initialised, FALSE otherwise
///    
FUNC BOOL INIT_SERVER()
		
	IF HAS_NET_TIMER_EXPIRED(serverBD.StartTimer,CPC_START_EVENT_DELAY)
			
		INT i
		REPEAT NUM_LEADERBOARD_PLAYERS i
			serverBD.iWinnerPart[i] = -1
		ENDREPEAT
		
		RESET_NET_TIMER(serverBD.eventTimer)
	
		PICK_CHECKPOINTS()
		
		IF IS_AIR_CHECKPOINT()
			REPEAT NUM_VEHICLES_TO_SPAWN i
				serverbd.iClosestPlayer[i] = GET_CLOSEST_PLAYER_TO_VEH_SPAWN(i)
			ENDREPEAT
			PICK_CHALLENGE_VEHICLES()
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE: Help function to get a debug string for the mission state
///    
/// PARAMS:
///    iState - The mission state
/// RETURNS: The name of the state
///    
FUNC STRING GET_MISSION_STATE_STRING(INT iState)

	SWITCH(iState)
		CASE GAME_STATE_INIT 				RETURN "GAME_STATE_INIT"
		CASE GAME_STATE_RUNNING 			RETURN "GAME_STATE_RUNNING"
		CASE GAME_STATE_TERMINATE_DELAY 	RETURN "GAME_STATE_TERMINATE_DELAY"
		CASE GAME_STATE_END 				RETURN "GAME_STATE_END"
	ENDSWITCH

	RETURN "UNKNOWN STATE!"

ENDFUNC

//Helper function to get a clients game/mission state
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iClientGameState
ENDFUNC

//Helper function to get the servers game/mission state
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC

//Helper function to set a clients game/mission state
PROC SET_CLIENT_MISSION_STATE(INT iPlayer,INT iState)
	playerBD[iPlayer].iClientGameState = iState
	CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === SET_CLIENT_MISSION_STATE = ",GET_MISSION_STATE_STRING(iState))
ENDPROC

//Helper function to set the servers game/mission state
PROC SET_SERVER_MISSION_STATE(INT iState)
	serverBD.iServerGameState = iState
	CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === SET_SERVER_MISSION_STATE = ",GET_MISSION_STATE_STRING(iState))
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------

PROC INVERT_BITSET(INT &iInput)
	INT i
	REPEAT 32 i
		IF IS_BIT_SET(iInput, i)
			CLEAR_BIT(iInput, i)
		ELSE
			SET_BIT(iInput, i)
		ENDIF
	ENDREPEAT
ENDPROC

PROC SCRIPT_CLEANUP()
	INT iTempBS[4]
	CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === SCRIPT_CLEANUP")
	
	CLEANUP_CHECKPOINTS()
	
//	IF IS_BIT_SET(iLocalBitset,AMCP_BS_STARTED_COUNTDOWN)
//		STOP_SOUND(iCountdownSound)
//	ENDIF
//	RELEASE_SOUND_ID(iCountdownSound)
	
	INT iVeh
	REPEAT NUM_VEHICLES_TO_SPAWN iVeh
		IF IS_BIT_SET(iExplodeBS, iVeh)
		AND NOT HAS_SOUND_FINISHED(iExplodeSOund[iVeh])
			STOP_SOUND(iExplodeSOund[iVeh])
			RELEASE_SOUND_ID(iExplodeSOund[iVeh])
		ENDIF
	ENDREPEAT
	
	Clear_Any_Objective_Text_From_This_Script()
	
	FM_EVENT_SET_PLAYER_WAITING_FOR_FM_EVENT_TO_START(FALSE)
	COMMON_FREEMODE_AMBIENT_EVENTS_CLEAN_UP(FMMC_TYPE_CHECKPOINTS,DEFAULT,(serverBD.eAMCP_Stage = AMCP_END))
	SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_CP_COLLECTION,FALSE)
	
	MPGlobalsAmbience.iCurrentCPCType = -1
	
	iTempBS[0] = serverBD.iCheckpointCollectedBS[0]
	iTempBS[1] = serverBD.iCheckpointCollectedBS[1]
	iTempBS[2] = serverBD.iCheckpointCollectedBS[2]
	iTempBS[3] = serverBD.iCheckpointCollectedBS[3]
	
	INVERT_BITSET(iTempBS[0])
	INVERT_BITSET(iTempBS[1])
	INVERT_BITSET(iTempBS[2])
	INVERT_BITSET(iTempBS[3])
	
	IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		telemetryStruct.m_endReason             =AE_END_FORCED
	ELIF IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_LOW_PARTICIPANTS)
		telemetryStruct.m_endReason             =AE_END_LOW_NUMBERS
	ELSE
		IF telemetryStruct.m_endReason       	!=AE_END_WON
			telemetryStruct.m_endReason         =AE_END_LOST
		ENDIF
	ENDIF
	
	telemetryStruct.m_uid0 = serverBD.iHashedMac
	telemetryStruct.m_uid1 = serverBD.iMatchHistoryId
	telemetryStruct.m_playersLeftInProgress = serverBD.iPlayersLeftInProgress
	telemetryStruct.m_playersParticipating = serverBD.iPeakQualifiers
	
	telemetryStruct.m_timeTakenToComplete = GET_CLOUD_TIME_AS_INT() - telemetryStruct.m_startTime
	
	
	INT iCheckQuadrant = serverBD.iCheckpointArea
	IF serverBD.bAir
		iCheckQuadrant = serverBD.iCheckpointArea+4
	ENDIF
	
	IF !g_sMPTunables.bcheckpoints_disable_share_cash               
		IF telemetryStruct.m_cashEarned > 0
	//		SET_LAST_JOB_DATA(LAST_JOB_CHECKPOINT_COLLECTION, telemetryStruct.m_cashEarned)
	//		IF telemetryStruct.m_endReason             =AE_END_WON
	//			SET_DO_FM_EVENT_SHARE_CASH_HELP_IF_NEEDED()
	//		ENDIF
		ENDIF
	ENDIF
	telemetryStruct.m_cashEarned += iTotalCashEarned
	
	IF PARTICIPANT_ID_TO_INT() != -1
		PROCESS_CURRENT_AMBIENT_EVENT_PLAYSTATS(telemetryStruct, iCheckQuadrant, playerBD[PARTICIPANT_ID_TO_INT()].iRewardCounter, iTempBS[0], iTempBS[1], iTempBS[2], iTempBS[3])
	ENDIF

	
	HANDLE_CHALLENGE_BLIPS(TRUE)
	
	TERMINATE_THIS_THREAD()
	
ENDPROC

FUNC BOOL PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)

	CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === PROCESS_PRE_GAME")
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission), missionScriptArgs)
	
	//Reserve Entities
	RESERVE_NETWORK_MISSION_OBJECTS(0)
	RESERVE_NETWORK_MISSION_VEHICLES(GET_AML_NUM_VEHICLES_REQUIRED(AML_SCRIPT_CPCOLLECTION))
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	//common event proccess pre game
	COMMON_FREEMODE_AMBIENT_EVENTS_PROCESS_PRE_GAME(FMMC_TYPE_CHECKPOINTS)
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD,SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD,SIZE_OF(playerBD))
	
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	IF !Wait_For_First_Network_Broadcast()
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBD.iHashedMac, serverBD.iMatchHistoryId)
	ENDIF
	
	telemetryStruct.m_playersNotified = NETWORK_GET_TOTAL_NUM_PLAYERS()
	telemetryStruct.m_startTime	= GET_CLOUD_TIME_AS_INT()
	telemetryStruct.m_notifiedTime	= GET_CLOUD_TIME_AS_INT()
	
	SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_CP_COLLECTION,TRUE)
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE: Checks to see whether the event should end
///    
/// RETURNS: TRUE if it should end, FALSE otherwise
///    
FUNC BOOL SHOULD_CHECKPOINT_COLLECTION_END()

	IF IS_KILL_CURRENT_FREEMODE_EVENT_GLOBAL_SET()
		CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === SHOULD_CHECKPOINT_COLLECTION_END - IS_KILL_CURRENT_FREEMODE_EVENT_GLOBAL_SET")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


//----------------------
//	MAIN SCRIPT
//----------------------

SCRIPT(MP_MISSION_DATA missionScriptArgs)


	CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === START")
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NOT PROCESS_PRE_GAME(missionScriptArgs)
			SCRIPT_CLEANUP()
		ENDIF	
	ENDIF
	
	WHILE (TRUE)
	
		MP_LOOP_WAIT_ZERO()
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		OR SHOULD_CHECKPOINT_COLLECTION_END()
			CPRINTLN(DEBUG_NET_AMBIENT, "=== CHECKPOINT_COLLECTION === SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")
			SCRIPT_CLEANUP()
		ENDIF
		
		//Call the common FM Events every frame callse
		FM_EVENT_COMMON_EVERY_FRAME_CALLS()
		
		SWITCH(GET_CLIENT_MISSION_STATE(NETWORK_PLAYER_ID_TO_INT()))
		
			CASE GAME_STATE_INIT
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					IF INIT_CLIENT()
						SET_CLIENT_MISSION_STATE(NETWORK_PLAYER_ID_TO_INT(),GAME_STATE_RUNNING)
					ENDIF
				ENDIF
			BREAK
			
			CASE GAME_STATE_RUNNING
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					CLIENT_PROCESSING()
					CLIENT_CHECK_SPAWN_LOCATIONS()
					#IF IS_DEBUG_BUILD
					PROCESS_DEBUG()
					#ENDIF
					
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					#IF IS_DEBUG_BUILD OR bEndScriptLocal #ENDIF
					
					SET_CLIENT_MISSION_STATE(NETWORK_PLAYER_ID_TO_INT(),GAME_STATE_END)
				ENDIF
			BREAK
						
			CASE GAME_STATE_END
				SCRIPT_CLEANUP()
			BREAK
			
		ENDSWITCH
	
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
			SWITCH(GET_SERVER_MISSION_STATE())
			
				CASE GAME_STATE_INIT
					IF INIT_SERVER()
						SET_SERVER_MISSION_STATE(GAME_STATE_RUNNING)
					ENDIF
				BREAK
				
				CASE GAME_STATE_RUNNING
					IF serverBD.eAMCP_Stage = AMCP_END
					#IF IS_DEBUG_BUILD 
					OR bEndScript 
					OR MPGlobalsAmbience.bKillActiveEvent
						MPGlobalsAmbience.bKillActiveEvent = FALSE
						#ENDIF
						SET_SERVER_MISSION_STATE(GAME_STATE_TERMINATE_DELAY)
					ELSE
						SERVER_PROCESSING()
					ENDIF
				BREAK
				
				CASE GAME_STATE_TERMINATE_DELAY
					IF HAS_NET_TIMER_EXPIRED(iServerTerminateTimer,SERVER_TERMINATE_DELAY)					
						SET_SERVER_MISSION_STATE(GAME_STATE_END)
					ENDIF
				BREAK
				
				CASE GAME_STATE_END
					SCRIPT_CLEANUP()
				BREAK
				
			ENDSWITCH
		
		ELSE
			#IF IS_DEBUG_BUILD
				MPGlobalsAmbience.bSkipWarningDelay = FALSE
			#ENDIF
		ENDIF
		
	ENDWHILE
	

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT

