//////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_PENNED_IN.sc															//
// Description: Players fight to stay in a shrinking area								//
// Written by:  David Watson															//
// Date: 18/05/2015																		//
//////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "net_mission.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_event.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"

//
USING "net_scoring_common.sch"
USING "net_ambience.sch"

//USING "help_at_location.sch"
USING "commands_object.sch"

USING "net_xp_animation.sch"

USING "net_wait_zero.sch"
//
USING "DM_Leaderboard.sch"
USING "net_challenges.sch"
USING "am_common_ui.sch"
USING "freemode_events_header.sch"
USING "net_rank_ui.sch"

USING "net_heli_SCTV.sch"

#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF

#IF IS_DEBUG_BUILD
	USING "profiler.sch"
#ENDIF

// Game States. 
CONST_INT GAME_STATE_INI 				0
CONST_INT GAME_STATE_RUNNING			1
CONST_INT GAME_STATE_LEAVE				2
CONST_INT GAME_STATE_TERMINATE_DELAY	3
CONST_INT GAME_STATE_END				4

ENUM PENNED_IN_STAGE_ENUM
	eAD_IDLE,
	eAD_COUNTDOWN,
	eAD_IN_AREA,
	eAD_OFF_MISSION,
	eAD_EXPLODE_VEH,
	eAD_CLEANUP
ENDENUM




CONST_INT VEH_EXP_RANGE	50
CONST_INT MAX_PLAYERS	4

CONST_INT MAX_NODES		217

 //FEATURE_BIKER

	#IF FEATURE_STUNT_FM_EVENTS
		CONST_INT MAX_ROUTES	11
	#ENDIF
	
	#IF NOT FEATURE_STUNT_FM_EVENTS
		CONST_INT MAX_ROUTES	10
	#ENDIF

CONST_INT MAX_MW_VEHS	5
CONST_INT MAX_MW_PEDS	1
CONST_INT MAX_MW_ON_FOOT_PEDS	4

CONST_INT MAX_MW_PED_SPAWN_LOC	5
CONST_INT MAX_MW_VEH_SPAWN_LOC	5
CONST_INT MAX_MW_HEL_SPAWN_LOC	5

CONST_INT MIN_ACTIVE_PLAYERS_FOR_HELI_SPECTATE 2


CONST_INT MAX_MW_PEDS_PER_VEH	4

CONST_INT MAX_VEH_AT_ONCE		3

CONST_INT TIME_MISSION_START		180000 //180000 //180000 // //60000
CONST_INT TIME_MISSION_START_SHORT	30000
CONST_INT TIME_OUT_OF_AREA			10000 //10000 //5000

CONST_INT TIME_EXPOLDE_VEH		30000

CONST_INT TIME_MISSION_TIMEOUT	600000

//TWEAK_FLOAT BLIP_MOVE_MULTIPLIER_MAX  	8.0 //50.0
//TWEAK_FLOAT BLIP_MOVE_MULTIPLIER_START	5.0
//TWEAK_FLOAT BLIP_MOVE_MULTIPLIER_END	5.6 // 7.0
//TWEAK_FLOAT AREA_START_RADIUS			187.5 //150.0
//TWEAK_FLOAT AREA_FIN_RADIUS				1.0 //10.0

//Server Bitset Data
CONST_INT biS_AtLeastOnePlayerOnMission		0
CONST_INT biS_AtLeastTwoPlayersOnMission	1
CONST_INT biS_MissionStartExpired			2
CONST_INT biS_SomeoneWon					3
CONST_INT biS_ReachedEndOfRoute				4
CONST_INT biS_EveryoneFinished				5
CONST_INT biS_UseShortCountDown				6
CONST_INT biS_ResetForShortCOuntdown		7
CONST_INT biS_Start321Countdown				8
CONST_INT biS_CountdownExpired				9
CONST_INT biS_WaitForWinnerExpired			10
CONST_INT biS_FinalLbUpdate					11
CONST_INT biS_EventExpired					12
CONST_INT biS_PlayerLimitReached			13
CONST_INT biS_WonButOnlyPlayerLeft			14
CONST_INT biS_AllAircraftDestroyed			15
CONST_INT biS_CagedInExpired				16
CONST_INT biS_CagedInBossQuit				17
CONST_INT biS_CagedInNotEnoughPlayers		18


CONST_INT PED_AI_STATE_CREATED			0
CONST_INT PED_AI_STATE_GOTO_PLAYERS		1
CONST_INT PED_AI_STATE_ATTACK_PLAYERS	2
CONST_INT PED_AI_STATE_CLEAN_UP			3
CONST_INT PED_AI_STATE_FINISHED			4

STRUCT STRUCT_LEADERBOARD_DATA
	INT iParticipant
	INT iPlayer
	INT iStillInvolved
	INT iWasInvolved
ENDSTRUCT



#IF FEATURE_STUNT_FM_EVENTS
CONST_INT MAX_AIRCRAFT_TO_SPAWN		1
#ENDIF
// The server broadcast data.
// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData
	INT iServerGameState
		
	INT iServerBitSet
	
	NETWORK_INDEX niVeh
	
	PLAYER_INDEX piPlayers[MAX_PLAYERS]
	
//	VECTOR vNodes[MAX_NODES]
	FLOAT fRadius 
	
	INT iPartStillPlayingBitset
	INT iPlayerSTillPlayingBitSet
	
	
	NETWORK_INDEX niMwPed[MAX_MW_PEDS]
	NETWORK_INDEX niMwVeh[MAX_MW_VEHS]
	
	INT iPedState[MAX_MW_PEDS]
	
	PENNED_IN_STAGE_ENUM eStage = eAD_IDLE
	
	SCRIPT_TIMER MissionTerminateDelayTimer
	SCRIPT_TIMER timeMissionStart
	SCRIPT_TIMER timeSinceEventBegin
	SCRIPT_TIMER timeWaitForWinner
	SCRIPT_TIMER timeEventExpired
	SCRIPT_TIMER timeSinceLaunched
	INT iMissionStartOffset
	INT iStartingPartBitSet
	STRUCT_LEADERBOARD_DATA sLeaderboardData[NUM_NETWORK_PLAYERS]
	
	INT iPlayersRemaining
	FLOAT fMoveMultiplier
	INT iLoc
	
	INT iAmountToAward
	INT iRpToAward
	INT iMinPlayers = -1
	
	
	INT iNode
	SCRIPT_TIMER timePosUpdate
	SCRIPT_TIMER timeFinalUpdate
	VECTOR vSpherePos
	
	INT iHashedMac 
	INT iMatchHistoryId
	INT iPlayersLeftInProgress 
	INT iPeakParticipants
	INT iPeakQualifiers
	
	HELI_SCTV_STRUCT heliStruct
	VECTOR vHeliStartPos
	
	INT iPennedInType = -1
	
	#IF FEATURE_STUNT_FM_EVENTS
	NETWORK_INDEX niAircraft[MAX_AIRCRAFT_TO_SPAWN]
	SCRIPT_TIMER timeExplodeAircraft
	INT iLockedAircraftDoorsBitset
	#ENDIF
	
	INT iLaunchBossPart = -1
	
	#IF IS_DEBUG_BUILD
	BOOL bAllowWith2Players = FALSE
	INT iServerDebugBitSet
	#ENDIF
ENDSTRUCT
ServerBroadcastData serverBD


CONST_INT biP_InArea				0
CONST_INT biP_FailedToStayInArea	1
CONST_INT biP_DoneReward			2
CONST_INT biP_ReachedEnd			3
CONST_INT biP_DoneCOuntdown			4
CONST_INT biP_OutCoronaTimeStarted	5
CONST_INT biP_InAreaAsPassenger		6
CONST_INT biP_SHouldHideEvent		7
CONST_INT biP_SHouldNotBeDamageable	8
CONST_INT biP_InAreaForAudioScene	9
CONST_INT biP_InAreaAtEndOfROute	10
CONST_INT biP_IsSpectating			11
CONST_INT biP_ControlOff			12
CONST_INT biP_WantToStart			13


// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	INT iGameState
	
	INT iPlayerBitSet
	INT iOtherPlayerDamageableBitset
	
	INT iHelpTextBitSet
	INT iObjTextBitset
	INT iAmountWonForGang = -1
	FLOAT fMyPenSize
	
	PENNED_IN_STAGE_ENUM eStage = eAD_IDLE
ENDSTRUCT

PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

///// MISSION VARIABLES /////
INT iBoolsBitSet
CONST_INT biHelp1Done			0
CONST_INT biHelp2Done			1
CONST_INT biHelp3Done			2
CONST_INT biHelp2PlayerDone		3
CONST_INT biHelpOffMission		4
CONST_INT biOtherPlayerHelp		5
CONST_INT biDoneTimeCycle		6
CONST_INT biReqCountdown		7
CONST_INT biDoneCOuntdown		8
CONST_INT biChangeColour		9
CONST_INT biEventStartTick		10
CONST_INT biDoneNonDriverHelp	11
CONST_INT biDoneExplodeVehicle	12
CONST_INT biPassHelp			13
CONST_INT biBlockCargobob		14
CONST_INT biHeartBeat			15
CONST_INT biSetAsMissionVeh		16
CONST_INT biWaitForShard		17
CONST_INT biSetPlayerInvincible	18
CONST_INT biClearVehInvincible	19
CONST_INT biDestWhenKnockedOut	20
CONST_INT biDoExitVehHelp		21
CONST_INT biDOneExitVehHelp		22
CONST_INT biPlayerPassive		23
CONST_INT biValidVehHelp		24
CONST_INT biQuckStartHElp		25
CONST_INT biNonPartPassive		26
CONST_INT biPassiveAlready		27
CONST_INT biNonPartDoHelp		28
CONST_INT biNonPartDoneHelp		29
CONST_INT biFlashedRadar		30
CONST_INT biEventUiHidden		31

INT iBoolsBitSet2
CONST_INT bi2_StartCountdownAudio	0
CONST_INT bi2_CheckForInvalidVeh	1
CONST_INT bi2_VehInvalid			2
CONST_INT bi2_NotRegHelp			3
CONST_INT bi2_SetDoNotRegHelp		4
CONST_INT bi2_RemovedFromEventAsPass		5
CONST_INT bi2_PassRemovedHelpText	6
CONST_INT bi2_OutOfAreaCountdown	7
CONST_INT bi2_RemovedFromEventAsDri 8
CONST_INT bi2_localReachedEndOfRoute	9
CONST_INT bi2_LOcalThinksSOmeoneWon	10
CONST_INT bi2_DoneMusic				11
CONST_INT bi2_ResetMusic			12
CONST_INT bi2_SCTV_Clear_God_Text	13
CONST_INT bi2_KnockedOutAsDead		14
CONST_INT bi2_Travelled70perc		15
CONST_INT bi2_Started70PerMusic		16
CONST_INT bi2_SetBlipForNonParts	17
CONST_INT bi2_Spectating_Colour		18
CONST_INT bi2_CleanedUpSpectate		19
CONST_INT bi2_EnteredCorona			20
CONST_INT bi2_DoneKnockedOut		21
CONST_INT bi2_resetForSctv			22
CONST_INT bi2_ExitSCTVFaded			23
CONST_INT bi2_CommonCleanup			24
CONST_INT bi2_DoneExpHeli			25
CONST_INT bi2_DoneAmbHeliHelp		26
#IF FEATURE_STUNT_FM_EVENTS
CONST_INT bi2_DoLockedVehHelp		27
CONST_INT bi2_InitLocalMulti		28
#ENDIF
CONST_INT bi2_DoneCommonSetup		29
CONST_INT bi2_DoneMeleeHelp			30
CONST_INT bi2_DoneCommonTelem		31

INT iBoolsBitSet3
CONST_INT bi3_SetMaxWanted			0
CONST_INT bi3_OnLaunchGang			1
CONST_INT bi3_ExpProof				2
CONST_INT bi3_VehExpProof			3

///// VEHICLE DIMENSION VARIABLES /////
//INT iVehicleBitset
CONST_INT biDimensionsStored	0
CONST_INT biPlayerLeftVehicle	1


//FLOAT fVehicleDimensions[NUM_NETWORK_PLAYERS]

INT iStaggeredParticipant

INT iOtherPlayerBlipsBitset
INT iOtherPlayerAudioBitset

INT iOtherPlayerGhostedPartBitset
INT iOtherPlayerGhostedPlayerBitset
//BLIP_INDEX MwPedBlip[MAX_MW_PEDS]
//BLIP_INDEX MwVehBlip[MAX_MW_VEHS]

AI_BLIP_STRUCT MwPedBlipData[MAX_MW_PEDS]

//--Relationship groups

REL_GROUP_HASH relPennedInPlayer
REL_GROUP_HASH relUWAi 
REL_GROUP_HASH relMyFmGroup

BLIP_INDEX blipArea
BLIP_INDEX blipStart

INT iMyCurrentNode
VECTOR vMyBlipPos

VECTOR vLocalBlipPos
INT iLocalCurrentNode

FLOAT fMyBlipSize
FLOAT fMyOldBlipSize

FLOAT fMyBlipMoveMultiplier

SCRIPT_TIMER OutOfAreaTimer
SCRIPT_TIMER timeUpdateServerMoveRate
SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerData

SCRIPT_TIMER timeMeleeHelp

SCRIPT_TIMER timeMyParticpation
VECTOR vNodes[MAX_NODES]
FLOAT fDistToNextNode[MAX_NODES]
INT iNumNodes
FLOAT fRouteDist
FLOAT fDistTravelled
FLOAT fDist70Percent

SCALEFORM_INDEX scDpadDownLeaderBoardMovie
DPAD_VARS sDPadVars
FM_DPAD_LBD_STRUCT sFmDPadLbData

INT iLastHeartBeatTime


INT iClientPLayerCOunt
VEHICLE_INDEX vehMyPennedInVeh
VEHICLE_INDEX vehMyLastVeh
VEHICLE_INDEX vehProofs
BLIP_INDEX blipVeh

//STRUCT_FM_EVENTS_END_UI sEndUiVars

INT iOutAreaSOund
//INT iMissionStartCountdownSound

//INT iMyTimeCycleModifier
ENUM COUNTDOWN_UI_FLAGS_RACE
	CNTDWN_UI_SoundGo  	= BIT4,
	CNTDWN_UI2_Played_3 	= BIT0,
	CNTDWN_UI2_Played_2 	= BIT1,
	CNTDWN_UI2_Played_1 	= BIT2,
	CNTDWN_UI2_Played_Go = BIT3
ENDENUM

STRUCT RACE_COUNTDOWN_UI
	SCALEFORM_INDEX uiCountdown
	INT iBitFlags
	INT iFrameCount
	structTimer CountdownTimer
ENDSTRUCT

HELI_SCTV_STRUCT_CLIENT heliStructClient

RACE_COUNTDOWN_UI countdown

scrFmEventAmbientMission telemetryStruct

SCRIPT_TIMER timeDisableAirstrike
SCRIPT_TIMER timeNonPartPassiveUpdate
SCRIPT_TIMER timeMyPenSizeUpdate

SCRIPT_TIMER timeNotRegHelp

SCRIPT_TIMER timeMyParticipation

INT iMyPartiticpationTime = 0
INT iLocalPotentialPlayerCount

INT iInitResetRemotePlayerGhosted

#IF FEATURE_STUNT_FM_EVENTS
BLIP_INDEX blipAircraft[MAX_AIRCRAFT_TO_SPAWN]
#ENDIF

HUD_COLOURS hclLaunchGang  = HUD_COLOUR_PURE_WHITE

// Widgets & Debug.
#IF IS_DEBUG_BUILD
	CONST_INT ROUTE_MAX	100
	BOOL bWdStartWith1P = FALSE
	BOOL bWdDoLeaderboardDebug
	BOOL bHostEndMissionNow
	BOOL bWdDoBlipDebug
	BOOl bWdDrawNodePos
	BOOL bWdDoMarkerDebug
	BOOL bWdPlotRoute
	BOOL bWdTestModifier
	BOOL bWdTestPostFx
	BOOL bSTopPostFx
	BOOL bWdClearTimecycle
	TEXT_WIDGET_ID widgetText
	BLIP_INDEX blipWDRoute[ROUTE_MAX]
	VECTOR vWdRoute[ROUTE_MAX]
	FLOAT fWdDistTravelled
	FLOAT fWdSTillToGo
	BOOL bWdSHowDistances
	BOOL bWdTestCountdown
	FLOAT fWdCoronaHeight = 0.0
	FLOAT fWdCoronaGroundZ = -999.0
	FLOAT fWdCoronaSize = 0.0
	FLOAT fWdCoronaX	= 0.0
	FLOAT fWdCoronaY	= 0.0
	BOOL bWdDisableWIn = FALSE
	BOOL bPauseSphere
	BOOL bWdDrawStartSPhere
	BOOL bWdMoveToExpVeh
	SCRIPT_TIMER timeSCTVDebug
#ENDIF

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MAIN PROCS                            ////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Helper function to get the servers game/mission state
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC


///	PURPOSE: Need specific relationship groups for players taking part. Don't want AI to attack non-mission players
PROC SETUP_UW_REL_GROUPS()
	PRINTLN("     ---------->     PENNED IN - SETUP_UW_REL_GROUPS")
	
	ADD_RELATIONSHIP_GROUP("relPennedInPlayer", relPennedInPlayer)
	ADD_RELATIONSHIP_GROUP("relUWAi", relUWAi)
	
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		
		//Like Player
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rgFM_Team[i], relPennedInPlayer)	
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relPennedInPlayer, rgFM_Team[i])
		
	//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rgFM_Team[i], relUWAi)	
	//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relUWAi, rgFM_Team[i])
		
	ENDREPEAT
	
	//Hate UW players
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relPennedInPlayer, relUWAi)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relUWAi, relPennedInPlayer)
	
	// Like Cops
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rgFM_Team[i], rgFM_AiLikePlyrLikeCops)
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rgFM_AiLikePlyrLikeCops, rgFM_Team[i])
//	//Dislike Player Like Cops
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE, rgFM_Team[i], rgFM_AiDislikePlyrLikeCops)
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE, rgFM_AiDislikePlyrLikeCops, rgFM_Team[i])
//	//Hate Player Like Cops
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rgFM_Team[i], rgFM_AiHatePlyrLikeCops)
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rgFM_AiHatePlyrLikeCops, rgFM_Team[i])
	
	//SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rgFM_Team[i], rgFM_AiHatedByCopsAndMercs)
	

	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_AMBIENT_GANG_CULT, relUWAi)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE , RELGROUPHASH_AMBIENT_GANG_CULT, relPennedInPlayer)

	
	
	//-- Cops
	SET_AMBIENT_COP_REL_TO_THIS_RELGROUP(ACQUAINTANCE_TYPE_PED_LIKE, relUWAi)
	
	//**AMBIENT GANGS
	SET_AMBIENT_GANG_LIKES_THIS_RELGROUP(relUWAi)
	SET_AMBIENT_GANG_LIKES_THIS_RELGROUP(relPennedInPlayer)
ENDPROC

PROC REMOVE_UW_REL_GROUPS()
	REMOVE_RELATIONSHIP_GROUP(relUWAi)
	REMOVE_RELATIONSHIP_GROUP(relPennedInPlayer)
ENDPROC

/// PURPOSE:
///    Clears the scritpers handle to the countdown UI.
PROC RELEASE_RACE_COUNTDOWN_UI(RACE_COUNTDOWN_UI & uiToRelease)
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(uiToRelease.uiCountdown)
	//RELEASE_SCRIPT_AUDIO_BANK()
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("HUD_MINI_GAME_SOUNDSET")
ENDPROC

FUNC BOOL IS_CAGED_IN_LAUNCHING()
	//-- Only true until GB_COMMON_BOSS_MISSION_SETUP is called!
	RETURN (GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION_TO_LAUNCH(PLAYER_ID()) = FMMC_TYPE_BIKER_CAGED_IN)
ENDFUNC

PROC INIT_LAUNCH_BOSS_SERVER_PART()
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF serverBD.iLaunchBossPart = -1
			serverBD.iLaunchBossPart = PARTICIPANT_ID_TO_INT()
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [INIT_LAUNCH_BOSS_SERVER_PART] serverBD.iLaunchBossPart = ", serverBD.iLaunchBossPart)
		ENDIF
	ENDIF
ENDPROC

FUNC PLAYER_INDEX GET_LAUNCH_BOSS_AS_PLAYER()
	IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(serverBD.iLaunchBossPart))
		RETURN NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.iLaunchBossPart))
	ENDIF
	
	RETURN INVALID_PLAYER_INDEX()
ENDFUNC

FUNC BOOL AM_I_IN_LAUNCH_GANG()
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
		IF PLAYER_ID() = GET_LAUNCH_BOSS_AS_PLAYER()
			RETURN TRUE
		ELSE
			IF GB_IS_PLAYER_MEMBER_OF_THIS_BIKER_GANG(PLAYER_ID(), GET_LAUNCH_BOSS_AS_PLAYER(), FALSE)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC



PROC MAINATAIN_LAUNCH_GANG_HUD_COLOUR()
	PLAYER_INDEX playerLaunch
	IF hclLaunchGang = HUD_COLOUR_PURE_WHITE
		playerLaunch = GET_LAUNCH_BOSS_AS_PLAYER()
		IF playerLaunch != INVALID_PLAYER_INDEX()
			hclLaunchGang = GB_GET_PLAYER_GANG_HUD_COLOUR(playerLaunch)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL OK_TO_DISPLAY_OBJECTIVE_TEXT_FOR_MODE()
	
	IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
		RETURN TRUE
	ELSE
		IF AM_I_IN_LAUNCH_GANG()
			RETURN TRUE
		ELSE
			IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) = GB_UI_LEVEL_FULL
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	
	#IF IS_DEBUG_BUILD
	GB_UI_LEVEL uiLevel = GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())
	IF GET_FRAME_COUNT() % 500 = 0
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN OK_TO_DISPLAY_OBJECTIVE_TEXT_FOR_MODE FALSE! uiLevel = ", ENUM_TO_INT(uiLevel))
	ENDIF
	#ENDIF
	
	
	
	RETURN FALSE
ENDFUNC


FUNC BOOL OK_TO_DISPLAY_HUD_FOR_MODE()
	
	IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
		RETURN TRUE
	ELSE
		IF AM_I_IN_LAUNCH_GANG()
			RETURN TRUE
		ELSE
			IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) = GB_UI_LEVEL_FULL
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF Is_This_The_Current_Objective_Text("CAG_STARTO")
		Clear_Any_Objective_Text_From_This_Script()
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN OK_TO_DISPLAY_OBJECTIVE_TEXT_FOR_MODE False - clearing CAG_STARTO")
	ENDIF
	
	
	
	
	#IF IS_DEBUG_BUILD
	GB_UI_LEVEL uiLevel = GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())
	IF GET_FRAME_COUNT() % 500 = 0
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN OK_TO_DISPLAY_HUD_FOR_MODE FALSE! uiLevel = ", ENUM_TO_INT(uiLevel))
	ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL OK_TO_DISPLAY_HELP_FOR_MODE()
	
	IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
		RETURN TRUE
	ELSE
		IF AM_I_IN_LAUNCH_GANG()
			RETURN TRUE
		ELSE
			IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_MINIMAL
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	
	#IF IS_DEBUG_BUILD
	GB_UI_LEVEL uiLevel = GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())
	IF GET_FRAME_COUNT() % 500 = 0
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN OK_TO_DISPLAY_HELP_FOR_MODE FALSE! uiLevel = ", ENUM_TO_INT(uiLevel))
	ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL OK_TO_DISPLAY_BLIPS_FOR_MODE()
	
	IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
		RETURN TRUE
	ELSE
		IF AM_I_IN_LAUNCH_GANG()
			RETURN TRUE
		ELSE
			IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_MINIMAL
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	
	#IF IS_DEBUG_BUILD
	GB_UI_LEVEL uiLevel = GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())
	IF GET_FRAME_COUNT() % 500 = 0
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN OK_TO_DISPLAY_BLIPS_FOR_MODE FALSE! uiLevel = ", ENUM_TO_INT(uiLevel))
	ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

// TUNABLES

FUNC INT GET_EVENT_START_TIME()
//	RETURN 75000
	
	#IF IS_DEBUG_BUILD
		IF bWdStartWith1P
		OR bWdDisableWIn
			RETURN 5000
		ENDIF
	#ENDIF
	
	IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
		RETURN(g_sMPTunables.iPenned_In_Event_Start_Countdown_Time)
	ELSE
		RETURN 60000 * 5
	ENDIF
ENDFUNC

FUNC INT GET_QUICK_LAUNCH_TIME()
//	RETURN 5000
	RETURN(g_sMPTunables.iPenned_In_Quick_Launch_Timer) // 30000
ENDFUNC

FUNC INT GET_MIN_NUMBER_OF_PLAYERS_FOR_QUICK_LAUNCH()
	IF g_sMPTunables.iPenned_In_Max_Players < g_sMPTunables.imum_number_of_players_session_before_launch
		RETURN g_sMPTunables.iPenned_In_Max_Players
	ENDIF
	RETURN g_sMPTunables.imum_number_of_players_session_before_launch
ENDFUNC

FUNC INT GET_MAX_NUMBER_OF_PLAYERS_FOR_LAUNCH()
	IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
		RETURN g_sMPTunables.iPenned_In_Max_Players
	ELSE
		RETURN g_sMPTunables.iBIKER_CAGED_IN_MAX_PLAYERS
	ENDIF
ENDFUNC
FUNC INT GET_EVENT_TIME_OUT_TIME()
//	RETURN 999999999
	IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
		RETURN(g_sMPTunables.iPenned_In_Event_Expiry_Time) // 600000
	ELSE
		RETURN g_sMPTunables.iBIKER_CAGED_IN_TIME_LIMIT   * 1000          
	ENDIF
ENDFUNC
FUNC FLOAT GET_PEN_START_SIZE()
//	RETURN 10000.0
	
	IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
		RETURN(g_sMPTunables.fPenned_In_Pen_Starting_Size) 
	ELSE
		RETURN g_sMPTunables.fBIKER_CAGED_IN_AREA_START_SIZE
	ENDIF
ENDFUNC

FUNC FLOAT GET_PEN_END_SIZE()
//	RETURN 10000.0
//	RETURN 20.0
	
	IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
		RETURN(g_sMPTunables.fPenned_In_Pen_Finishing_Size) 
	ELSE
		RETURN g_sMPTunables.fBIKER_CAGED_IN_AREA_END_SIZE              
	ENDIF
ENDFUNC

FUNC FLOAT GET_PEN_MAX_SPEED_MODIFIER()
	#IF FEATURE_STUNT_FM_EVENTS
	IF serverBD.iPennedInType = PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY
		RETURN 10.0
	ENDIF
	#ENDIF
	RETURN(g_sMPTunables.fPenned_In_Pen_Movement_Max_Speed_Modifier) // 8.0 BLIP_MOVE_MULTIPLIER_MAX
ENDFUNC

FUNC FLOAT GET_PEN_START_SPEED_MODIFIER()
	RETURN(g_sMPTunables.fPenned_In_Pen_Movement_Start_Speed_Modifier) // 5.0 BLIP_MOVE_MULTIPLIER_START
ENDFUNC

FUNC FLOAT GET_PEN_END_SPEED_MODIFIER()
	#IF FEATURE_STUNT_FM_EVENTS
	IF serverBD.iPennedInType = PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY
		RETURN 6.5
	ENDIF
	#ENDIF
	
	RETURN(g_sMPTunables.fPenned_In_Pen_Movement_End_Speed_Modifier) // 5.6 BLIP_MOVE_MULTIPLIER_END
ENDFUNC

FUNC FLOAT GET_PEN_SPEED_MULTIPLIER()
//	RETURN 0.0
	RETURN(g_sMPTunables.fPenned_In_Pen_movement_Speed_Modifier) // 1.0
ENDFUNC

FUNC FLOAT GET_PEN_SHRINK_MULTIPLIER()	
	RETURN(g_sMPTunables.fPENNED_IN_SIZE_MODIFIER) // 1.0
ENDFUNC

FUNC INT GET_QUICK_LAUNCH_PERCENTAGE()
	RETURN(g_sMPTunables.iPenned_In_Percentage_of_session_required_for_quick_launch) // 50
ENDFUNC


FUNC INT GET_KNOCKED_OUT_TIME()

	
	IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
		RETURN(g_sMPTunables.iPenned_In_Knockout_timer) 
	ELSE
		RETURN 15000
	ENDIF
ENDFUNC

FUNC BOOL SHOULD_DISABLE_SHARE_CASH()
	RETURN g_sMPTunables.bPenned_In_Disable_Share_Cash
ENDFUNC

FUNC BOOL IS_ROUTE_DISABLED(INT iRoute)
	SWITCH iRoute
		CASE 0 RETURN(g_sMPTunables.bPenned_In_Disable_variant_1)		BREAK
		CASE 1 RETURN(g_sMPTunables.bPenned_In_Disable_variant_2)		BREAK
		CASE 2 RETURN(g_sMPTunables.bPenned_In_Disable_variant_3)		BREAK
		CASE 3 RETURN(g_sMPTunables.bPenned_In_Disable_variant_4)		BREAK
		CASE 4 RETURN(g_sMPTunables.bPenned_In_Disable_variant_5)		BREAK
		
		CASE 6 RETURN(	g_sMPTunables.bBIKER_DISABLE_CAGED_IN_ROUTE_0)		BREAK
		CASE 7 RETURN(	g_sMPTunables.bBIKER_DISABLE_CAGED_IN_ROUTE_1)		BREAK
		CASE 8 RETURN(	g_sMPTunables.bBIKER_DISABLE_CAGED_IN_ROUTE_2)		BREAK
		CASE 9 RETURN(	g_sMPTunables.bBIKER_DISABLE_CAGED_IN_ROUTE_3)		BREAK
		CASE 10 RETURN(	g_sMPTunables.bBIKER_DISABLE_CAGED_IN_ROUTE_4)		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_VEHICLE_DETONATION_TIME()
	RETURN 30000
ENDFUNC


INT iMyTimeInEvent = -1
INT iLocalPartCountAtStart = -1


FUNC INT GET_CASH_BASE()
	IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
		RETURN g_sMPTunables.iBIKER_CAGED_IN_BASE_CASH     
	ELSE
		RETURN g_sMPTunables.iPENNED_IN_BASE_CASH          //3000
	ENDIF
ENDFUNC

FUNC INT GET_CASH_SCALE()
	IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
		RETURN g_sMPTunables.iBIKER_CAGED_IN_SCALE_CASH        
	ELSE
		RETURN g_sMPTunables.iPENNED_IN_SCALE_CASH          //600
	ENDIF
ENDFUNC


FUNC INT GET_RP_BASE()
	IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
		RETURN g_sMPTunables.iBIKER_CAGED_IN_BASE_RP    
	ELSE
		RETURN g_sMPTunables.iPENNED_IN_BASE_RP //500
	ENDIF
ENDFUNC

FUNC INT GET_RP_SCALE()
	IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
		RETURN g_sMPTunables.iBIKER_CAGED_IN_SCALE_RP    
	ELSE
		RETURN g_sMPTunables.iPENNED_IN_SCALE_RP       //50
	ENDIF
ENDFUNC

FUNC FLOAT GET_PARTICIPANT_WEIGHT()
	IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
		RETURN g_sMPTunables.fBIKER_CAGED_IN_P_WEIGHT  
	ELSE
		RETURN g_sMPTunables.fPENNED_IN_P_WEIGHT            //0.5
	ENDIF
ENDFUNC

FUNC INT GET_PARTICIPANT_CAP()
	IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
		RETURN g_sMPTunables.iBIKER_CAGED_IN_P_CAP    
	ELSE
		RETURN g_sMPTunables.iPENNED_IN_P_CAP       //30
	ENDIF
ENDFUNC


FUNC STRING GET_PENNED_IN_START_HELP()
	STRING sHelp
	SWITCH serverBD.iPennedInType
		CASE PENNED_IN_TYPE_ROAD_VEHICLE
			sHelp =  "PEN_START" // ~s~Penned In ~HUD_COLOUR_NET_PLAYER2~~BLIP_PENNED_IN~ ~s~~HUD_COLOUR_WHITE~is available. Compete with other players to stay within the moving zone. You will need a vehicle to participate.
		BREAK
		
		#IF FEATURE_STUNT_FM_EVENTS
		CASE PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY
			sHelp = "PEN_STARTH" // ~s~Air Penned In ~HUD_COLOUR_NET_PLAYER2~~BLIP_PENNED_IN~ ~s~~HUD_COLOUR_WHITE~is available. Compete with other players to stay within the moving zone. You will need a helicopter to participate.
		BREAK
		#ENDIF
		
		CASE PENNED_IN_TYPE_CAGED_IN
			sHelp = "CAG_STARC"
		BREAK
	ENDSWITCH
	
	RETURN sHelp
ENDFUNC

FUNC TEXT_LABEL_15 GET_PENNED_IN_TIMER_HELP()
	TEXT_LABEL_15 tl15Label  
	
	
	SWITCH serverBD.iPennedInType
		CASE PENNED_IN_TYPE_ROAD_VEHICLE
			tl15Label =  "PEN_START2" 
		BREAK
		
		#IF FEATURE_STUNT_FM_EVENTS
		CASE PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY
			tl15Label = "PEN_START2" 
		BREAK
		#ENDIF
		
		CASE PENNED_IN_TYPE_CAGED_IN
			tl15Label = "CAG_START2"
		BREAK
	ENDSWITCH
	
	RETURN tl15Label
ENDFUNC




FUNC TEXT_LABEL_15 GET_PENNED_IN_MIN_PLAYERS_HELP()
	TEXT_LABEL_15 tl15Label  
	
	
	SWITCH serverBD.iPennedInType
		CASE PENNED_IN_TYPE_ROAD_VEHICLE
			tl15Label =  "PEN_MPLAY" 
		BREAK
		
		#IF FEATURE_STUNT_FM_EVENTS
		CASE PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY
			tl15Label = "PEN_MPLAY" 
		BREAK
		#ENDIF
		
		CASE PENNED_IN_TYPE_CAGED_IN
			tl15Label = "CAG_MPLAY"
		BREAK
	ENDSWITCH
	
	RETURN tl15Label
ENDFUNC


FUNC STRING GET_PENNED_IN_INVALID_VEHICLE_HELP_TEXT()
	STRING sHelp
	SWITCH serverBD.iPennedInType
		CASE PENNED_IN_TYPE_ROAD_VEHICLE
			sHelp =  "PEN_VALVEH" // You can't use this vehicle during the Penned In event
		BREAK
		
		#IF FEATURE_STUNT_FM_EVENTS
		CASE PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY
			sHelp = "PEN_NEEDH" // You need a helicopter to participate in this event.
		BREAK
		#ENDIF
		
		CASE PENNED_IN_TYPE_CAGED_IN
			sHelp = "CAG_VEH"
		BREAK
	ENDSWITCH
	
	RETURN sHelp
ENDFUNC

FUNC STRING GET_PENNED_IN_NEED_VEHICLE_HELP_TEXT()
	STRING sHelp
	SWITCH serverBD.iPennedInType
		CASE PENNED_IN_TYPE_ROAD_VEHICLE
			sHelp =  "PEN_VEH" // You need a vehicle to take part in Penned In
		BREAK
		
		#IF FEATURE_STUNT_FM_EVENTS
		CASE PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY
			sHelp = "PEN_NEEDH" // You need a helicopter to participate in this event.
		BREAK
		#ENDIF
		
		CASE PENNED_IN_TYPE_CAGED_IN
			sHelp = "CAG_VEH"
		BREAK
	ENDSWITCH
	
	RETURN sHelp
ENDFUNC

FUNC TEXT_LABEL_15 GET_PENNED_IN_BLIP_NAME()
	TEXT_LABEL_15 tl15Label  
	
	
	SWITCH serverBD.iPennedInType
		CASE PENNED_IN_TYPE_ROAD_VEHICLE
			tl15Label =  "PEN_BLIP" 
		BREAK
		
		#IF FEATURE_STUNT_FM_EVENTS
		CASE PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY
			tl15Label = "PEN_BLIP" 
		BREAK
		#ENDIF
		
		CASE PENNED_IN_TYPE_CAGED_IN
			tl15Label = "CAG_BLIP"
		BREAK
	ENDSWITCH
	
	RETURN tl15Label
ENDFUNC

FUNC TEXT_LABEL_15 GET_PENNED_IN_DRAWN_TEXT()
	TEXT_LABEL_15 tl15Label  
	
	
	SWITCH serverBD.iPennedInType
		CASE PENNED_IN_TYPE_ROAD_VEHICLE
			tl15Label =  "PEN_DRAWD" 
		BREAK
		
		#IF FEATURE_STUNT_FM_EVENTS
		CASE PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY
			tl15Label = "PEN_DRAWD" 
		BREAK
		#ENDIF
		
		CASE PENNED_IN_TYPE_CAGED_IN
			tl15Label = "CAG_DRAWD"
		BREAK
	ENDSWITCH
	
	RETURN tl15Label
ENDFUNC

FUNC BIG_MESSAGE_TYPE GET_BIG_MESSAGE_TYPE_FOR_EVENT_DRAWN()
	
	IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
		RETURN BIG_MESSAGE_FM_EVENT_DRAW
	ELSE
		RETURN BIG_MESSAGE_GB_END_OF_JOB_OVER
	ENDIF
ENDFUNC


FUNC BIG_MESSAGE_TYPE GET_BIG_MESSAGE_TYPE_FOR_EVENT_OVER()
	
	IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
		RETURN BIG_MESSAGE_FM_EVENT_FINISHED
	ELSE
		RETURN BIG_MESSAGE_GB_END_OF_JOB_OVER
	ENDIF
ENDFUNC

FUNC BIG_MESSAGE_TYPE GET_BIG_MESSAGE_TYPE_FOR_EVENT_WON()
	
	IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
		RETURN BIG_MESSAGE_FM_EVENT_WIN
	ELSE
		RETURN BIG_MESSAGE_GB_END_OF_JOB_SUCCESS
	ENDIF
ENDFUNC

FUNC TEXT_LABEL_15 GET_PENNED_IN_TIED_WITH_PLAYER_TEXT()
	TEXT_LABEL_15 tl15Label  
	
	
	SWITCH serverBD.iPennedInType
		CASE PENNED_IN_TYPE_ROAD_VEHICLE
			tl15Label =  "PEN_DRAWP" 
		BREAK
		
		#IF FEATURE_STUNT_FM_EVENTS
		CASE PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY
			tl15Label = "PEN_DRAWP" 
		BREAK
		#ENDIF
		
		CASE PENNED_IN_TYPE_CAGED_IN
			tl15Label = "CAG_DRAWP"
		BREAK
	ENDSWITCH
	
	RETURN tl15Label
ENDFUNC

FUNC TEXT_LABEL_15 GET_PENNED_IN_TWO_PLAYERS_TIED_TEXT()
	TEXT_LABEL_15 tl15Label  
	
	
	SWITCH serverBD.iPennedInType
		CASE PENNED_IN_TYPE_ROAD_VEHICLE		tl15Label =  "PEN_DRAW2P" 	BREAK
		
		#IF FEATURE_STUNT_FM_EVENTS
		CASE PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY	tl15Label = "PEN_DRAW2P" 	BREAK
		#ENDIF
		
		CASE PENNED_IN_TYPE_CAGED_IN			tl15Label = "CAG_DRAW2P"	BREAK
	ENDSWITCH
	
	RETURN tl15Label
ENDFUNC

FUNC TEXT_LABEL_15 GET_PENNED_IN_EVENT_OVER_TEXT()
	TEXT_LABEL_15 tl15Label  
	
	
	SWITCH serverBD.iPennedInType
		CASE PENNED_IN_TYPE_ROAD_VEHICLE		tl15Label =  "PEN_OVR" 	BREAK
		
		#IF FEATURE_STUNT_FM_EVENTS
		CASE PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY	tl15Label = "PEN_OVR" 	BREAK
		#ENDIF
		
		CASE PENNED_IN_TYPE_CAGED_IN			tl15Label = "BK_RUN_OVER"	BREAK
	ENDSWITCH
	
	RETURN tl15Label
ENDFUNC

FUNC TEXT_LABEL_15 GET_PENNED_IN_NOBODY_WON_TEXT()
	TEXT_LABEL_15 tl15Label  
	
	
	SWITCH serverBD.iPennedInType
		CASE PENNED_IN_TYPE_ROAD_VEHICLE		
		#IF FEATURE_STUNT_FM_EVENTS
		CASE PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY	 	
		#ENDIF
			tl15Label = "PEN_NWN"
		BREAK

		CASE PENNED_IN_TYPE_CAGED_IN			tl15Label = "CAG_NWN"	BREAK
	ENDSWITCH
	
	RETURN tl15Label
ENDFUNC

FUNC TEXT_LABEL_15 GET_PENNED_IN_I_WON_TEXT()
	TEXT_LABEL_15 tl15Label  
	
	
	SWITCH serverBD.iPennedInType
		CASE PENNED_IN_TYPE_ROAD_VEHICLE		
		#IF FEATURE_STUNT_FM_EVENTS
		CASE PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY	 	
		#ENDIF
			tl15Label = "PEN_WON"
		BREAK

		CASE PENNED_IN_TYPE_CAGED_IN			tl15Label = "CAG_WON"	BREAK
	ENDSWITCH
	
	RETURN tl15Label
ENDFUNC

FUNC TEXT_LABEL_15 GET_PENNED_IN_PLAYER_WON_TEXT()
	TEXT_LABEL_15 tl15Label  
	
	
	SWITCH serverBD.iPennedInType
		CASE PENNED_IN_TYPE_ROAD_VEHICLE		
		#IF FEATURE_STUNT_FM_EVENTS
		CASE PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY	 	
		#ENDIF
			tl15Label = "PEN_WONP"
		BREAK

		CASE PENNED_IN_TYPE_CAGED_IN			tl15Label = "CAG_WONP"	BREAK
	ENDSWITCH
	
	RETURN tl15Label
ENDFUNC

FUNC TEXT_LABEL_15 GET_PENNED_IN_GANG_WON_TEXT()
	TEXT_LABEL_15 tl15Label  
	
	
	SWITCH serverBD.iPennedInType
		CASE PENNED_IN_TYPE_ROAD_VEHICLE		
		#IF FEATURE_STUNT_FM_EVENTS
		CASE PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY	 	
		#ENDIF
			tl15Label = "PEN_WONPG2"
		BREAK

		CASE PENNED_IN_TYPE_CAGED_IN			tl15Label = "CAG_WONPG2"	BREAK
	ENDSWITCH
	
	RETURN tl15Label
ENDFUNC

FUNC TEXT_LABEL_15 GET_PENNED_IN_GANGMEMBER_WON_TEXT()
	TEXT_LABEL_15 tl15Label  
	
	
	SWITCH serverBD.iPennedInType
		CASE PENNED_IN_TYPE_ROAD_VEHICLE		
		#IF FEATURE_STUNT_FM_EVENTS
		CASE PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY	 	
		#ENDIF
			tl15Label = "PEN_WONPG"
		BREAK

		CASE PENNED_IN_TYPE_CAGED_IN			tl15Label = "CAG_WONPG"	BREAK
	ENDSWITCH
	
	RETURN tl15Label
ENDFUNC

FUNC TEXT_LABEL_15 GET_PENNED_IN_TWO_PLAYERS_WON_TEXT()
	TEXT_LABEL_15 tl15Label  
	
	
	SWITCH serverBD.iPennedInType
		CASE PENNED_IN_TYPE_ROAD_VEHICLE		
		#IF FEATURE_STUNT_FM_EVENTS
		CASE PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY	 	
		#ENDIF
			tl15Label = "PEN_WON2P"
		BREAK

		CASE PENNED_IN_TYPE_CAGED_IN			tl15Label = "CAG_WON2P"	BREAK
	ENDSWITCH
	
	RETURN tl15Label
ENDFUNC
FUNC TEXT_LABEL_15 GET_PENNED_IN_STARTED_TICKER_TEXT()
	TEXT_LABEL_15 tl15Label  
	
	
	SWITCH serverBD.iPennedInType
		CASE PENNED_IN_TYPE_ROAD_VEHICLE		
		#IF FEATURE_STUNT_FM_EVENTS
		CASE PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY	 	
		#ENDIF
			tl15Label = "PEN_NAVAIL"
		BREAK

		CASE PENNED_IN_TYPE_CAGED_IN			tl15Label = "CAG_NAVAIL"	BREAK
	ENDSWITCH
	
	RETURN tl15Label
ENDFUNC

FUNC TEXT_LABEL_15 GET_PENNED_IN_NOT_AVAILABLE_TEXT()
	TEXT_LABEL_15 tl15Label  
	
	
	SWITCH serverBD.iPennedInType
		CASE PENNED_IN_TYPE_ROAD_VEHICLE		
		#IF FEATURE_STUNT_FM_EVENTS
		CASE PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY	 	
		#ENDIF
			tl15Label = "PEN_NAVAIL2"
		BREAK

		CASE PENNED_IN_TYPE_CAGED_IN			tl15Label = "CAG_NAVAIL2"	BREAK
	ENDSWITCH
	
	RETURN tl15Label
ENDFUNC

FUNC TEXT_LABEL_15 GET_PENNED_IN_NEED_TO_BE_DRIVER_TEXT()
	TEXT_LABEL_15 tl15Label  
	
	
	SWITCH serverBD.iPennedInType
		CASE PENNED_IN_TYPE_ROAD_VEHICLE		
		#IF FEATURE_STUNT_FM_EVENTS
		CASE PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY	 	
		#ENDIF
			tl15Label = "PEN_NDRV"
		BREAK

		CASE PENNED_IN_TYPE_CAGED_IN			tl15Label = "CAG_NDRV"	BREAK
	ENDSWITCH
	
	RETURN tl15Label
ENDFUNC

FUNC TEXT_LABEL_15 GET_PENNED_IN_PASSENGER_TEXT()
	TEXT_LABEL_15 tl15Label  
	
	
	SWITCH serverBD.iPennedInType
		CASE PENNED_IN_TYPE_ROAD_VEHICLE		
		#IF FEATURE_STUNT_FM_EVENTS
		CASE PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY	 	
		#ENDIF
			tl15Label = "PEN_NDRV2"
		BREAK

		CASE PENNED_IN_TYPE_CAGED_IN			tl15Label = "CAG_NDRV2"	BREAK
	ENDSWITCH
	
	RETURN tl15Label
ENDFUNC

FUNC TEXT_LABEL_15 GET_PENNED_IN_NO_EXIT_TEXT()
	TEXT_LABEL_15 tl15Label  
	
	
	SWITCH serverBD.iPennedInType
		CASE PENNED_IN_TYPE_ROAD_VEHICLE		
		#IF FEATURE_STUNT_FM_EVENTS
		CASE PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY	 	
		#ENDIF
			tl15Label = "PEN_EXV"
		BREAK

		CASE PENNED_IN_TYPE_CAGED_IN			tl15Label = "CAG_EXV"	BREAK
	ENDSWITCH
	
	RETURN tl15Label
ENDFUNC

FUNC TEXT_LABEL_15 GET_PENNED_IN_EXPIRED_TICKER_TEXT()
	TEXT_LABEL_15 tl15Label  
	
	
	SWITCH serverBD.iPennedInType
		CASE PENNED_IN_TYPE_ROAD_VEHICLE		
		#IF FEATURE_STUNT_FM_EVENTS
		CASE PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY	 	
		#ENDIF
			tl15Label = "PEN_EXPD"
		BREAK

		CASE PENNED_IN_TYPE_CAGED_IN			tl15Label = "CAG_EXPD"	BREAK
	ENDSWITCH
	
	RETURN tl15Label
ENDFUNC


FUNC TEXT_LABEL_15 GET_PENNED_IN_QUICK_START_TEXT()
	TEXT_LABEL_15 tl15Label  
	
	
	SWITCH serverBD.iPennedInType
		CASE PENNED_IN_TYPE_ROAD_VEHICLE		
		#IF FEATURE_STUNT_FM_EVENTS
		CASE PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY	 	
		#ENDIF
			tl15Label = "PEN_QSTART"
		BREAK

		CASE PENNED_IN_TYPE_CAGED_IN			tl15Label = "CAG_QSTART"	BREAK
	ENDSWITCH
	
	RETURN tl15Label
ENDFUNC

FUNC TEXT_LABEL_15 GET_PENNED_IN_PASSIVE_TEXT()
	TEXT_LABEL_15 tl15Label  
	
	
	SWITCH serverBD.iPennedInType
		CASE PENNED_IN_TYPE_ROAD_VEHICLE		
		#IF FEATURE_STUNT_FM_EVENTS
		CASE PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY	 	
		#ENDIF
			tl15Label = "PEN_PASSV"
		BREAK

		CASE PENNED_IN_TYPE_CAGED_IN			tl15Label = "CAG_PASSV"	BREAK
	ENDSWITCH
	
	RETURN tl15Label
ENDFUNC

FUNC TEXT_LABEL_15 GET_PENNED_IN_OUTSIDE_AREA_TEXT()
	TEXT_LABEL_15 tl15Label  
	
	
	SWITCH serverBD.iPennedInType
		CASE PENNED_IN_TYPE_ROAD_VEHICLE		
		#IF FEATURE_STUNT_FM_EVENTS
		CASE PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY	 	
		#ENDIF
			tl15Label = "PEN_REGS"
		BREAK

		CASE PENNED_IN_TYPE_CAGED_IN			tl15Label = "CAG_REGS"	BREAK
	ENDSWITCH
	
	RETURN tl15Label
ENDFUNC

FUNC TEXT_LABEL_15 GET_PENNED_IN_DRIVER_EXITED_VEH_TEXT()
	TEXT_LABEL_15 tl15Label  
	
	
	SWITCH serverBD.iPennedInType
		CASE PENNED_IN_TYPE_ROAD_VEHICLE		
		#IF FEATURE_STUNT_FM_EVENTS
		CASE PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY	 	
		#ENDIF
			tl15Label = "PEN_EXITP"
		BREAK

		CASE PENNED_IN_TYPE_CAGED_IN			tl15Label = "CAG_EXITP"	BREAK
	ENDSWITCH
	
	RETURN tl15Label
ENDFUNC

FUNC TEXT_LABEL_15 GET_PENNED_IN_CANT_TAKE_PART_TEXT()
	TEXT_LABEL_15 tl15Label  
	
	
	SWITCH serverBD.iPennedInType
		CASE PENNED_IN_TYPE_ROAD_VEHICLE		
		#IF FEATURE_STUNT_FM_EVENTS
		CASE PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY	 	
		#ENDIF
			tl15Label = "PEN_EXITD"
		BREAK

		CASE PENNED_IN_TYPE_CAGED_IN			tl15Label = "CAG_EXITD"	BREAK
	ENDSWITCH
	
	RETURN tl15Label
ENDFUNC

FUNC TEXT_LABEL_15 GET_PENNED_IN_ABOUT_TO_START_OBJ_TEXT()
	TEXT_LABEL_15 tl15Label  
	
	
	SWITCH serverBD.iPennedInType
		CASE PENNED_IN_TYPE_ROAD_VEHICLE		
		#IF FEATURE_STUNT_FM_EVENTS
		CASE PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY	 	
		#ENDIF
			tl15Label = "PEN_STARTO"
		BREAK

		CASE PENNED_IN_TYPE_CAGED_IN			tl15Label = "CAG_STARTO"	BREAK
	ENDSWITCH
	
	RETURN tl15Label
ENDFUNC


PROC INIT_MY_PARTICIPATION_TIME_MULTIPLIER()
	FLOAT fPartTime
	
	IF iMyPartiticpationTime <> 0
		EXIT
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(timeMyParticipation)
		iMyPartiticpationTime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(timeMyParticipation)
	ENDIF

	
	PRINTLN("     ---------->     PENNED IN - [GET_PARTICIPATION_CASH] [INIT_MY_PARTICIPATION_TIME_MULTIPLIER] iMyPartiticpationTime (ms) = ", iMyPartiticpationTime)
	
	fPartTime = TO_FLOAT(iMyPartiticpationTime)
	fPartTime /= 60000 
	
	iMyPartiticpationTime = FLOOR(fPartTime) 
	
	
	IF iMyPartiticpationTime >= 1
		PRINTLN("     ---------->     PENNED IN - [GET_PARTICIPATION_CASH] [INIT_MY_PARTICIPATION_TIME_MULTIPLIER] iMyPartiticpationTime (min) = ", iMyPartiticpationTime)
	ELSE
		iMyPartiticpationTime = 1
		PRINTLN("     ---------->     PENNED IN - [GET_PARTICIPATION_CASH] [INIT_MY_PARTICIPATION_TIME_MULTIPLIER] iMyPartiticpationTime Was < 1 (min) = ", iMyPartiticpationTime)
	ENDIF
	
	IF iMyPartiticpationTime > g_sMPTunables.iParticipation_T_Cap                         
		iMyPartiticpationTime = g_sMPTunables.iParticipation_T_Cap
		PRINTLN("     ---------->     KILL LIST - [GET_PARTICIPATION_CASH] [INIT_MY_PARTICIPATION_TIME_MULTIPLIER] SET iMyPartiticpationTime TO TUNABLE CAP AS iParticipation_T_Cap = ", g_sMPTunables.iParticipation_T_Cap)
	ENDIF
	
ENDPROC


FUNC INT GET_PARTICIPATION_CASH(BOOL bScaleByTime = TRUE)
	PRINTLN("     ---------->     PENNED IN - [GET_PARTICIPATION_CASH] CALLED bScaleByTime = ", bScaleByTime)
	
	INT iMyPartCash
	
	IF NOT bScaleByTime
		RETURN g_sMPTunables.iPENNED_IN_MINIMUM_PARTICIPATION_CASH  //1000
	ENDIF
	
	INIT_MY_PARTICIPATION_TIME_MULTIPLIER()
	
	iMyPartCash = g_sMPTunables.iPENNED_IN_MINIMUM_PARTICIPATION_CASH * iMyPartiticpationTime
	
	PRINTLN("     ---------->     PENNED IN - [GET_PARTICIPATION_CASH] iMyPartCash = ", iMyPartCash, " iMyPartiticpationTime = ", iMyPartiticpationTime)
	
	IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
		RETURN 0 //-- Steve will handle this
	ENDIF
	RETURN iMyPartCash
ENDFUNC

FUNC INT GET_PARTICIPATION_RP(BOOL bScaleByTime = TRUE)
	PRINTLN("     ---------->     PENNED IN - [GET_PARTICIPATION_RP] CALLED bScaleByTime = ", bScaleByTime)
	
	INT iMyPartRP
	
	IF NOT bScaleByTime
		RETURN g_sMPTunables.iPENNED_IN_MINIMUM_PARTICIPATION_RP      //100
	ENDIF
	
	INIT_MY_PARTICIPATION_TIME_MULTIPLIER()
	
	iMyPartRP = g_sMPTunables.iPENNED_IN_MINIMUM_PARTICIPATION_RP * iMyPartiticpationTime
	
	PRINTLN("     ---------->     PENNED IN - [GET_PARTICIPATION_RP] iMyPartRP = ", iMyPartRP, " iMyPartiticpationTime = ", iMyPartiticpationTime)
	
	IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
		RETURN 0 //-- Steve will handle this
	ENDIF
	
	RETURN iMyPartRP
ENDFUNC

PROC GET_MY_CAGED_IN_CASH_AND_RP_REWARD(BOOL bWon, INT &iCash, INT &iRp, INT iNumWinners = 1)
	/*
	FROM BUG 2371436
	
	
	FORMULA: 
	Cash:
	- (((Base x T_Weight) + (Scale x T_Weight x (Participants(max P_Cap) x P_Weight))) / Players In Area at End)
	RP:
	- ((Base x T_Weight) + (Scale x T_Weight x (Participants(max P_Cap) x P_Weight)))

	DEFAULTS:
	- Base:
	  o Cash = 3000
	  o RP = 500
	- Scale: 
	  o Cash = 600
	  o RP = 50
	- T_Weight:
	  o Under 1 mins = 0.5
	  o Under 2 mins = 1.0
	  o Under 4 mins = 1.5
	  o Under 8 mins = 2.0
	  o Over 8 mins = 2.5
	- P_Weight = 0.5
	- P_Cap = 30
	*/
	FLOAT fTimeWeight
	IF iMyTimeInEvent = -1
		iMyTimeInEvent = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(timeMyParticpation)
	ENDIF
	

	
	
	IF iMyTimeInEvent < (g_sMPTunables.iPENNED_IN_T_WEIGHT_TIME_THRESHOLD_1 * 60000)  //60000
		fTimeWeight = g_sMPTunables.fBIKER_CAGED_IN_T_WEIGHT_UNDER_1          // 0.5
	ELIF iMyTimeInEvent < (g_sMPTunables.iPENNED_IN_T_WEIGHT_TIME_THRESHOLD_2 * 60000) //120000 
		fTimeWeight = g_sMPTunables.fBIKER_CAGED_IN_T_WEGHT_UNDER_2 //1.0
	ELIF iMyTimeInEvent < (g_sMPTunables.iPENNED_IN_T_WEIGHT_TIME_THRESHOLD_3 * 60000) //240000 
		fTimeWeight = g_sMPTunables.fBIKER_CAGED_IN_T_WEGHT_UNDER_4 //1.5
	ELIF iMyTimeInEvent < (g_sMPTunables.iPENNED_IN_T_WEIGHT_TIME_THRESHOLD_4 * 60000) //480000
		fTimeWeight = g_sMPTunables.fBIKER_CAGED_IN_T_WEGHT_UNDER_8 //2.0
	ELSE
		fTimeWeight = g_sMPTunables.fBIKER_CAGED_IN_T_WEGHT_OVER_8           //2.5
	ENDIF
	
	FLOAT fCashBase 	= TO_FLOAT(GET_CASH_BASE())
	FLOAT fRpBase 		= TO_FLOAT(GET_RP_BASE())
	FLOAT fCashScale	= TO_FLOAT(GET_CASH_SCALE())
	FLOAT fRpScale		= TO_FLOAT(GET_RP_SCALE())
	
	FLOAT fPartWeight 	= GET_PARTICIPANT_WEIGHT()
	INT iPartCap		= GET_PARTICIPANT_CAP()
	
	IF iLocalPartCountAtStart > iPartCap
		iLocalPartCountAtStart = iPartCap
	ENDIF
	
	INT iParticipationCash = GET_PARTICIPATION_CASH()
	INT iParticipationRP = GET_PARTICIPATION_RP()
	
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [CHECK_FOR_REWARD] - [GET_MY_CAGED_IN_CASH_AND_RP_REWARD] iParticipationCash = ", iParticipationCash, " iParticipationRP = ", iParticipationRP)
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [CHECK_FOR_REWARD] - [GET_MY_CAGED_IN_CASH_AND_RP_REWARD] iMyTimeInEvent = ", iMyTimeInEvent, " fTimeWeight = ", fTimeWeight)
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [CHECK_FOR_REWARD] - [GET_MY_CAGED_IN_CASH_AND_RP_REWARD] fCashBase = ", fCashBase, " fCashScale = ", fCashScale)
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [CHECK_FOR_REWARD] - [GET_MY_CAGED_IN_CASH_AND_RP_REWARD] fRpBase = ", fRpBase, " fRpScale = ", fRpScale)
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [CHECK_FOR_REWARD] - [GET_MY_CAGED_IN_CASH_AND_RP_REWARD] iLocalPartCountAtStart = ", iLocalPartCountAtStart, " fPartWeight = ", fPartWeight)
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [CHECK_FOR_REWARD] - [GET_MY_CAGED_IN_CASH_AND_RP_REWARD] iNumWinners = ", iNumWinners)
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [CHECK_FOR_REWARD] - [GET_MY_CAGED_IN_CASH_AND_RP_REWARD] g_sMPTunables.fPenned_In_Event_Multiplier_Cash  = ", g_sMPTunables.fPenned_In_Event_Multiplier_Cash)
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [CHECK_FOR_REWARD] - [GET_MY_CAGED_IN_CASH_AND_RP_REWARD] g_sMPTunables.fPenned_In_Event_Multiplier_RP  = ", g_sMPTunables.fPenned_In_Event_Multiplier_RP)
	
	iCash = iParticipationCash
	iRp = iParticipationRP
		
	IF bWon
		IF NOT IS_BIT_SET(serverBD.iServerBitSet ,biS_WonButOnlyPlayerLeft)
			INT iCashForBossCut = ROUND(((fCashBase * fTimeWeight) + (fCashScale * fTimeWeight * (TO_FLOAT(iLocalPartCountAtStart) * fPartWeight)) / iNumWinners) * g_sMPTunables.fPenned_In_Event_Multiplier_Cash)
			
		//	iCash += ROUND(((fCashBase * fTimeWeight) + (fCashScale * fTimeWeight * (TO_FLOAT(iLocalPartCountAtStart) * fPartWeight)) / iNumWinners) * g_sMPTunables.fPenned_In_Event_Multiplier_Cash)
	
			iRp += ROUND(((fRpBase * fTimeWeight) + (fRpScale * fTimeWeight * (TO_FLOAT(iLocalPartCountAtStart) * fPartWeight))) * g_sMPTunables.fPenned_In_Event_Multiplier_RP)
			IF playerBD[PARTICIPANT_ID_TO_INT()].iAmountWonForGang = -1
				playerBD[PARTICIPANT_ID_TO_INT()].iAmountWonForGang = iCash
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [CHECK_FOR_REWARD] - [GET_MY_CASH_AND_RP_REWARD] SET iAmountWonForGang = ", iCash)
			ENDIF
			
			
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [CHECK_FOR_REWARD] - [GET_MY_CASH_AND_RP_REWARD] iCashForBossCut = ", iCashForBossCut)
			GB_HANDLE_GANG_BOSS_CUT(iCashForBossCut)
			
			
			iCash += iCashForBossCut
			
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [CHECK_FOR_REWARD] - [GET_MY_CASH_AND_RP_REWARD] iCash AFTER BOSS CUT = ", iCash)
		ELSE
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [CHECK_FOR_REWARD] - [GET_MY_CASH_AND_RP_REWARD] I WON BUT ONLY GETTING PART CASH/RP AS biS_WonButOnlyPlayerLeft")
		ENDIF
	ENDIF
	
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [CHECK_FOR_REWARD] - [GET_MY_CASH_AND_RP_REWARD] bWon = ", bWon," iCash = ", iCash, " iRp = ", iRp) 
ENDPROC

PROC GET_MY_CASH_AND_RP_REWARD(BOOL bWon, INT &iCash, INT &iRp, INT iNumWinners = 1)
	/*
	FROM BUG 2371436
	
	
	FORMULA: 
	Cash:
	- (((Base x T_Weight) + (Scale x T_Weight x (Participants(max P_Cap) x P_Weight))) / Players In Area at End)
	RP:
	- ((Base x T_Weight) + (Scale x T_Weight x (Participants(max P_Cap) x P_Weight)))

	DEFAULTS:
	- Base:
	  o Cash = 3000
	  o RP = 500
	- Scale: 
	  o Cash = 600
	  o RP = 50
	- T_Weight:
	  o Under 1 mins = 0.5
	  o Under 2 mins = 1.0
	  o Under 4 mins = 1.5
	  o Under 8 mins = 2.0
	  o Over 8 mins = 2.5
	- P_Weight = 0.5
	- P_Cap = 30
	*/
	
	IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
		GET_MY_CAGED_IN_CASH_AND_RP_REWARD(bWon, iCash, iRp, iNumWinners)
		EXIT
	ENDIF
	
	
	FLOAT fTimeWeight
	IF iMyTimeInEvent = -1
		iMyTimeInEvent = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(timeMyParticpation)
	ENDIF
	
	IF NOT bWon
		IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
			FM_EVENT_SETUP_DO_MINIMUM_PARTICIPATION_HELP(TRUE)
		ENDIF
	ENDIF
	
	
	IF iMyTimeInEvent < (g_sMPTunables.iPENNED_IN_T_WEIGHT_TIME_THRESHOLD_1 * 60000)  //60000
		fTimeWeight = g_sMPTunables.fPENNED_IN_T_WEIGHT_THRESHOLD_VALUE_1 // 0.5
	ELIF iMyTimeInEvent < (g_sMPTunables.iPENNED_IN_T_WEIGHT_TIME_THRESHOLD_2 * 60000) //120000 
		fTimeWeight = g_sMPTunables.fPENNED_IN_T_WEIGHT_THRESHOLD_VALUE_2 //1.0
	ELIF iMyTimeInEvent < (g_sMPTunables.iPENNED_IN_T_WEIGHT_TIME_THRESHOLD_3 * 60000) //240000 
		fTimeWeight = g_sMPTunables.fPENNED_IN_T_WEIGHT_THRESHOLD_VALUE_3 //1.5
	ELIF iMyTimeInEvent < (g_sMPTunables.iPENNED_IN_T_WEIGHT_TIME_THRESHOLD_4 * 60000) //480000
		fTimeWeight = g_sMPTunables.fPENNED_IN_T_WEIGHT_THRESHOLD_VALUE_4 //2.0
	ELSE
		fTimeWeight = g_sMPTunables.fPENNED_IN_T_WEIGHT_THRESHOLD_VALUE_5 //2.5
	ENDIF
	
	FLOAT fCashBase 	= TO_FLOAT(GET_CASH_BASE())
	FLOAT fRpBase 		= TO_FLOAT(GET_RP_BASE())
	FLOAT fCashScale	= TO_FLOAT(GET_CASH_SCALE())
	FLOAT fRpScale		= TO_FLOAT(GET_RP_SCALE())
	
	FLOAT fPartWeight 	= GET_PARTICIPANT_WEIGHT()
	INT iPartCap		= GET_PARTICIPANT_CAP()
	
	IF iLocalPartCountAtStart > iPartCap
		iLocalPartCountAtStart = iPartCap
	ENDIF
	
	INT iParticipationCash = GET_PARTICIPATION_CASH()
	INT iParticipationRP = GET_PARTICIPATION_RP()
	
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [CHECK_FOR_REWARD] - [GET_MY_CASH_AND_RP_REWARD] iParticipationCash = ", iParticipationCash, " iParticipationRP = ", iParticipationRP)
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [CHECK_FOR_REWARD] - [GET_MY_CASH_AND_RP_REWARD] iMyTimeInEvent = ", iMyTimeInEvent, " fTimeWeight = ", fTimeWeight)
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [CHECK_FOR_REWARD] - [GET_MY_CASH_AND_RP_REWARD] fCashBase = ", fCashBase, " fCashScale = ", fCashScale)
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [CHECK_FOR_REWARD] - [GET_MY_CASH_AND_RP_REWARD] fRpBase = ", fRpBase, " fRpScale = ", fRpScale)
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [CHECK_FOR_REWARD] - [GET_MY_CASH_AND_RP_REWARD] iLocalPartCountAtStart = ", iLocalPartCountAtStart, " fPartWeight = ", fPartWeight)
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [CHECK_FOR_REWARD] - [GET_MY_CASH_AND_RP_REWARD] iNumWinners = ", iNumWinners)
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [CHECK_FOR_REWARD] - [GET_MY_CASH_AND_RP_REWARD] g_sMPTunables.fPenned_In_Event_Multiplier_Cash  = ", g_sMPTunables.fPenned_In_Event_Multiplier_Cash)
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [CHECK_FOR_REWARD] - [GET_MY_CASH_AND_RP_REWARD] g_sMPTunables.fPenned_In_Event_Multiplier_RP  = ", g_sMPTunables.fPenned_In_Event_Multiplier_RP)
	
	iCash = iParticipationCash
	iRp = iParticipationRP
		
	IF bWon
		IF NOT IS_BIT_SET(serverBD.iServerBitSet ,biS_WonButOnlyPlayerLeft)
			INT iCashForBossCut = ROUND(((fCashBase * fTimeWeight) + (fCashScale * fTimeWeight * (TO_FLOAT(iLocalPartCountAtStart) * fPartWeight)) / iNumWinners) * g_sMPTunables.fPenned_In_Event_Multiplier_Cash)
			
		//	iCash += ROUND(((fCashBase * fTimeWeight) + (fCashScale * fTimeWeight * (TO_FLOAT(iLocalPartCountAtStart) * fPartWeight)) / iNumWinners) * g_sMPTunables.fPenned_In_Event_Multiplier_Cash)
	
			iRp += ROUND(((fRpBase * fTimeWeight) + (fRpScale * fTimeWeight * (TO_FLOAT(iLocalPartCountAtStart) * fPartWeight))) * g_sMPTunables.fPenned_In_Event_Multiplier_RP)
			IF playerBD[PARTICIPANT_ID_TO_INT()].iAmountWonForGang = -1
				playerBD[PARTICIPANT_ID_TO_INT()].iAmountWonForGang = iCash
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [CHECK_FOR_REWARD] - [GET_MY_CASH_AND_RP_REWARD] SET iAmountWonForGang = ", iCash)
			ENDIF
			
			
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [CHECK_FOR_REWARD] - [GET_MY_CASH_AND_RP_REWARD] iCashForBossCut = ", iCashForBossCut)
			GB_HANDLE_GANG_BOSS_CUT(iCashForBossCut)
			
			
			iCash += iCashForBossCut
			
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [CHECK_FOR_REWARD] - [GET_MY_CASH_AND_RP_REWARD] iCash AFTER BOSS CUT = ", iCash)
		ELSE
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [CHECK_FOR_REWARD] - [GET_MY_CASH_AND_RP_REWARD] I WON BUT ONLY GETTING PART CASH/RP AS biS_WonButOnlyPlayerLeft")
		ENDIF
	ENDIF
	
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [CHECK_FOR_REWARD] - [GET_MY_CASH_AND_RP_REWARD] bWon = ", bWon," iCash = ", iCash, " iRp = ", iRp) 
ENDPROC

//PURPOSE: Returns the Cash reward for completing the objective
FUNC INT GET_CASH_REWARD()
	RETURN g_sMPTunables.iUrbanWarfareCashReward
ENDFUNC
				

FUNC BOOL ARE_ALL_PEDS_CLEANED_UP()
	
	
	RETURN TRUE
ENDFUNC

#IF FEATURE_STUNT_FM_EVENTS
FUNC BOOL HAVE_ALL_AIRCRAFT_BEEN_DESTROYED()
	INT i
	REPEAT MAX_AIRCRAFT_TO_SPAWN i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niAircraft[i])
			IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niAircraft[i])
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_VEHICLE_A_SPAWNED_PENNED_IN_AIRCRAFT(VEHICLE_INDEX veh, INT &iSpawnedVeh)
	INT i
	REPEAT MAX_AIRCRAFT_TO_SPAWN i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niAircraft[i])
			IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niAircraft[i])
				IF veh = NET_TO_VEH(serverBD.niAircraft[i])
					iSpawnedVeh = i
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC



PROC MAINTAIN_TRYING_TO_ENTER_LOCKED_PENNED_IN_VEHICLE_HELP()
	IF serverBD.iPennedInType != PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iBoolsBitSet2, bi2_DoLockedVehHelp)
		IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PEN_LOCKV")
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				PRINT_HELP_NO_SOUND("PEN_LOCKV", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)	
				IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
					SET_FREEMODE_EVENT_HELP_BACKGROUND()
				ENDIF
				CLEAR_BIT(iBoolsBitSet2, bi2_DoLockedVehHelp)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [MAINTAIN_TRYING_TO_ENTER_LOCKED_PENNED_IN_VEHICLE_HELP] Displaying help")
			ENDIF
		ELSE
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [MAINTAIN_TRYING_TO_ENTER_LOCKED_PENNED_IN_VEHICLE_HELP] Clear bi2_DoLockedVehHelp as help being displayed")
			CLEAR_BIT(iBoolsBitSet2, bi2_DoLockedVehHelp)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_TRYING_TO_ENTER_LOCKED_PENNED_IN_VEHICLE(INT iEventID)
	STRUCT_VEHICLE_ID Event
	INT iSpawnedVeh
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_AI, iEventID, Event, SIZE_OF(Event))
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [PROCESS_TRYING_TO_ENTER_LOCKED_PENNED_IN_VEHICLE] EVENT RECEIVED")
		IF IS_VEHICLE_DRIVEABLE(Event.VehicleId)
			IF IS_VEHICLE_A_SPAWNED_PENNED_IN_AIRCRAFT(Event.VehicleId, iSpawnedVeh)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [PROCESS_TRYING_TO_ENTER_LOCKED_PENNED_IN_VEHICLE] TRYING TO ENTER PENNED IN VEH ", iSpawnedVeh, " Will try to do help")
				SET_BIT(iBoolsBitSet2, bi2_DoLockedVehHelp)
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC PROCESS_PENNED_IN_AI_EVENTS()
	IF serverBD.iPennedInType != PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY
		EXIT
	ENDIF
	
	INT iEventID
	EVENT_NAMES ThisScriptEvent
	
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_AI) iEventID
		
		ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_AI, iEventID)
		
		SWITCH (ThisScriptEvent)

			CASE EVENT_PLAYER_UNABLE_TO_ENTER_VEHICLE
				PROCESS_TRYING_TO_ENTER_LOCKED_PENNED_IN_VEHICLE(iEventID)
			BREAK
		ENDSWITCH
	ENDREPEAT
ENDPROC
#ENDIF

FUNC BOOL MISSION_END_CHECK()
	BOOL bOffMission
	INT i
	IF serverBD.eStage >= eAD_IN_AREA
		bOffMission = TRUE
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
				IF playerBD[i].eStage < eAD_CLEANUP
					bOffMission = FALSE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	IF bOffMission
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Server only function that checks to see if the game mode should now end. For this test script, it if time has passed.
FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET()
	
	#IF IS_DEBUG_BUILD
		IF bHostEndMissionNow
			RETURN TRUE
		ENDIF
	#ENDIF	
	
	IF MISSION_END_CHECK()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC




//Helper function to get a clients game/mission state
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iGameState
ENDFUNC

#IF IS_DEBUG_BUILD

// iServerDebugBitSet
CONST_INT ciSERVER_DEBUG_WARP 0

STRUCT sDEBUG_VARS
	NETWORK_INDEX niVeh
	INT iDebugProgress
	BOOL bDoneWarp
ENDSTRUCT

sDEBUG_VARS sDebugVars

PROC CLIENT_DO_DEBUG_WARP()

	VECTOR vPos
	FLOAT fHead
	
	IF IS_BIT_SET(serverBD.iServerDebugBitSet, ciSERVER_DEBUG_WARP)
	
		IF NOT sDebugVars.bDoneWarp
		
			IF NETWORK_PLAYER_ID_TO_INT() = 0 
				vPos = <<1446.9082, -2347.1489, 65.7580>>
				fHead = 7.2919
			ELSE
				vPos = <<1445.0231, -2310.7654, 66.1009>>
				fHead = 348.6076
			ENDIF
		
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				
					CLEAR_AREA_OF_VEHICLES(vPos, 20.00)
					
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						
						IF CREATE_NET_VEHICLE(sDebugVars.niVeh, BLISTA, vPos, fHead, FALSE)
							TASK_ENTER_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(sDebugVars.niVeh), -1, VS_DRIVER, PEDMOVEBLENDRATIO_RUN, ECF_WARP_PED)
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(fHead)
							sDebugVars.bDoneWarp = TRUE
						ENDIF
					ELSE
						IF NET_WARP_TO_COORD(vPos, fHead, TRUE)	
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(fHead)
							sDebugVars.bDoneWarp = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF sDebugVars.bDoneWarp
			sDebugVars.bDoneWarp = FALSE
		ENDIF
	ENDIF
ENDPROC

PROC SERVER_MANAGE__DEBUG_WARP()

	INT i
	PLAYER_INDEX tempPlayerID
	
	SWITCH sDebugVars.iDebugProgress
	
		CASE 0
		
			IF IS_DEBUG_KEY_PRESSED(KEY_NUMPAD1, KEYBOARD_MODIFIER_SHIFT, "")
			
				REPEAT NUM_NETWORK_PLAYERS i
				
					tempPlayerID = INT_TO_PLAYERINDEX(i)
					
					IF IS_NET_PLAYER_OK(tempPlayerID, FALSE)
					
						CLEAR_BIT(serverBD.iServerDebugBitSet, ciSERVER_DEBUG_WARP)
					ENDIF
				ENDREPEAT
			
				sDebugVars.iDebugProgress ++
			ENDIF
		BREAK
		
		CASE 1
			
			REPEAT NUM_NETWORK_PLAYERS i
			
				tempPlayerID = INT_TO_PLAYERINDEX(i)
				
				IF IS_NET_PLAYER_OK(tempPlayerID, FALSE)
				
					SET_BIT(serverBD.iServerDebugBitSet, ciSERVER_DEBUG_WARP)
				ENDIF
			ENDREPEAT
			
			sDebugVars.iDebugProgress = 0
		BREAK
	ENDSWITCH
ENDPROC

PROC SETUP_ROUTE()
	// Penned-In, Steve
	vWdRoute[0] =	<<-1900.1852, 2050.7751, 139.7211>>
	vWdRoute[1] =	<<-1495.9578, 2410.7122, 4.8343>>
	vWdRoute[2] =   <<-1130.1449, 2664.2593, 16.5918>>
	vWdRoute[3] =   <<-654.5994, 2853.8596, 30.3318>>
	vWdRoute[4] =   <<-297.8209, 2825.7312, 58.2026>>
	vWdRoute[5] =   <<6.1263, 2811.8782, 56.2030>>
	vWdRoute[6] =   <<290.2636, 2861.3303, 42.6424>>
	vWdRoute[7] =   <<567.5082, 2665.8188, 41.0551>>
	vWdRoute[8] =   <<744.1198, 2577.0474, 74.3547>>
	vWdRoute[9] =   <<1041.2521, 2669.2830, 38.5412>>
	//-- Wind Farm to lighthouse
	
	
ENDPROC
PROC CREATE_WIDGETS()
	INT iPlayer
	TEXT_LABEL_15 tl63

	START_WIDGET_GROUP("AM_PENNED_IN")  
		#IF SCRIPT_PROFILER_ACTIVE
		CREATE_SCRIPT_PROFILER_WIDGET()
		#ENDIF	
		
		ADD_WIDGET_BOOL("Start with 1P", bWdStartWith1P) 
		ADD_WIDGET_BOOL("Disable Win", bWdDisableWIn)
		ADD_WIDGET_BOOL("bPauseSphere", bPauseSphere)
		ADD_WIDGET_FLOAT_SLIDER("Heli Speed", g_sMPTunables.fPenned_In_Heli_Speed, 0.0, 1000.0, 0.01) 
		ADD_WIDGET_FLOAT_SLIDER("Heli Offset", g_sMPTunables.fPenned_In_Heli_Offset, 0.0, 1000.0, 0.01) 
		ADD_WIDGET_INT_SLIDER("Heli Minhight", g_sMPTunables.iPenned_In_Heli_Minhight,0, HIGHEST_INT,1)
		
		START_WIDGET_GROUP("Start") 
			ADD_WIDGET_FLOAT_SLIDER("Corona Height", fWdCoronaHeight, 0.0, 1000.0, 0.5) 
			ADD_WIDGET_FLOAT_SLIDER("Corona Height Diff", fWdCoronaGroundZ, -1000.0, 1000.0, 0.5)
			ADD_WIDGET_FLOAT_SLIDER("Corona Size", fWdCoronaSize, 0.0, 1000.0, 0.5)
			ADD_WIDGET_FLOAT_SLIDER("Corona X", fWdCoronaX, -10000.0, 10000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Corona Y", fWdCoronaY, -10000.0, 10000.0, 0.1)
			ADD_WIDGET_BOOL("Show Shere", bWdDrawStartSPhere)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Area") 
			
			ADD_WIDGET_FLOAT_READ_ONLY("Size", fMyBlipSize)
			ADD_WIDGET_FLOAT_READ_ONLY("Move Mult", serverBD.fMoveMultiplier)
			ADD_WIDGET_BOOL("Area blip Debug", bWdDoBlipDebug) 
			ADD_WIDGET_BOOL("Area marker Debug", bWdDoMarkerDebug)
			ADD_WIDGET_BOOL("Draw Nodes", bWdDrawNodePos)
			ADD_WIDGET_BOOL("Plot Route", bWdPlotRoute)
			ADD_WIDGET_FLOAT_READ_ONLY("Pos x", vMyBlipPos.x)
			ADD_WIDGET_FLOAT_READ_ONLY("Pos y", vMyBlipPos.y)
			ADD_WIDGET_FLOAT_READ_ONLY("Pos z", vMyBlipPos.z)
			ADD_WIDGET_FLOAT_READ_ONLY("d Travelled", fWdDistTravelled)
			ADD_WIDGET_FLOAT_READ_ONLY("d Still To Go", fWdSTillToGo)
			ADD_WIDGET_BOOL("Show Distances", bWdSHowDistances)
			ADD_WIDGET_BOOL("Test Modifier", bWdTestModifier)
			ADD_WIDGET_BOOL("Test PostFx",bWdTestPostFx)
			ADD_WIDGET_BOOL("Stop PostFx",bSTopPostFx)
			ADD_WIDGET_BOOL("Clear Modifier", bWdClearTimecycle)
			widgetText = ADD_TEXT_WIDGET("Timecycle")
			ADD_WIDGET_BOOL("Test Countdown", bWdTestCountdown)
		//	ADD_WIDGET_FLOAT_SLIDER("Area Move Mult", BLIP_MOVE_MULTIPLIER_MAX, 0.1, 100.0, 0.1)
		STOP_WIDGET_GROUP()
		
		// Gameplay debug, sever only.
		START_WIDGET_GROUP("Server Only Gameplay") 
			ADD_WIDGET_BOOL("Move to Exp Veh stage", bWdMoveToExpVeh)
			ADD_WIDGET_BOOL("End mission", bHostEndMissionNow)			
		STOP_WIDGET_GROUP()		
		
		// Data about the server
		START_WIDGET_GROUP("Server BD") 
			ADD_WIDGET_INT_SLIDER("S. Game state", serverBD.iServerGameState,-1, HIGHEST_INT,1)

		STOP_WIDGET_GROUP()	

		// Data about the clients. * = You.
		START_WIDGET_GROUP("Client BD")  				
			REPEAT NUM_NETWORK_PLAYERS iPlayer
				tl63 = "Player "
				tl63 += iPlayer
				IF iPlayer = PARTICIPANT_ID_TO_INT()
					tl63 += "*"
				ENDIF
				START_WIDGET_GROUP(tl63)
					ADD_WIDGET_INT_SLIDER("Game state", playerBD[iPlayer].iGameState,-1, HIGHEST_INT,1)
				STOP_WIDGET_GROUP()
			ENDREPEAT				
		STOP_WIDGET_GROUP()	
		
	STOP_WIDGET_GROUP()
	
	SET_CONTENTS_OF_TEXT_WIDGET(widgetText, "Trevor")
	SETUP_ROUTE()
ENDPROC		

PROC DISPLAY_PENNED_IN_STRING_AND_NUMBER(FLOAT fX, FLOAT fY, STRING sDisplay, INT iDisplay)
	INT iR, iG, iB, iA
	GET_HUD_COLOUR(HUD_COLOUR_RED, iR, iG, iB, iA)
	
	SET_TEXT_COLOUR(iR, iG, iB, iA)
	SET_TEXT_SCALE(0.0000, 0.5)
	DISPLAY_TEXT_WITH_LITERAL_STRING(fX, fY, "STRING", sDisplay)
	
	SET_TEXT_COLOUR(iR, iG, iB, iA)
	SET_TEXT_SCALE(0.0000, 0.5)
	DISPLAY_TEXT_WITH_NUMBER((fX + 0.2), fY, "NUMBER", iDisplay)	
ENDPROC


#ENDIF

FUNC STRING GET_VEHICLE_TYPE_STRING()
	RETURN "" //GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(serverBD.VehModel)
ENDFUNC

FUNC STRING GET_VEH_BLIP_STRING()
//	IF serverBD.VehModel = RHINO
//		RETURN "ABLIP_TANK"			//"~BLIP_TEMP_6~"
//	ELIF IS_THIS_MODEL_A_PLANE(serverBD.VehModel)
//		RETURN "ABLIP_PLANE"		//"~BLIP_TEMP_6~"
//	ENDIF
//	
//	RETURN "ABLIP_HELI"				//"~BLIP_TEMP_6~"*/
	RETURN ""
ENDFUNC


PROC DISABLE_PLAYER_MISSION_VEHICLE_CONTROLS_THIS_FRAME(BOOL bDisableExit = TRUE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HANDBRAKE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_UD)
	IF bDisableExit
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
	ENDIF
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ROLL_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_PITCH_UD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_YAW_LEFT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_YAW_LEFT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_UP)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_DOWN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_UNDERCARRIAGE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_NEXT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_PREV_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_TARGET_LEFT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_TARGET_RIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_VERTICAL_FLIGHT_MODE)
ENDPROC

PROC STOP_PLAYER_LEAVING_VEHICLE_THIS_FRAME()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
	ENDIF
ENDPROC

PROC MAINTAIN_LEAVE_VEHICLE_HELP_TEXT()
	IF NOT IS_BIT_SET(iBoolsBitSet, biDoExitVehHelp)

		IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
		OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
		OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
			SET_BIT(iBoolsBitSet, biDoExitVehHelp)
			PRINTLN("     ---------->     PENNED IN - SET biDoExitVehHelp")
		ENDIF
	
	ELSE
		IF NOT IS_BIT_SET(iBoolsBitSet, biDOneExitVehHelp)
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				TEXT_LABEL_15 tl15Help = GET_PENNED_IN_NO_EXIT_TEXT()
				PRINT_HELP_NO_SOUND(tl15Help, DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) //You cannot exit your vehicle while in the area.
				IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
					SET_FREEMODE_EVENT_HELP_BACKGROUND()
				ENDIF
				SET_BIT(iBoolsBitSet, biDOneExitVehHelp)
				PRINTLN("     ---------->     PENNED IN - SET biDOneExitVehHelp")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_STOP_PLAYER_LEAVING_VEHICLE()

	IF IS_PED_INJURED(PLAYER_PED_ID())
		EXIT
	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		EXIT
	ENDIF
	
	VEHICLE_INDEX vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	
	//-- If I started as a participant, and I'm now somehow in a passenger seat, then I can still get out.
	IF GET_PED_IN_VEHICLE_SEAT(vehTemp) <> PLAYER_PED_ID()
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InArea)
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_FailedToStayInArea)
				EXIT
			ENDIF
		ENDIF
	ENDIF
	
	//-- Passengers can exit
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaAsPassenger)
		EXIT
	ENDIF
	
	//-- In an invalid vehicle - told to get out
	IF IS_BIT_SET(iBoolsBitSet2, bi2_VehInvalid)
		EXIT
	ENDIF
	
	MAINTAIN_LEAVE_VEHICLE_HELP_TEXT()
	
	STOP_PLAYER_LEAVING_VEHICLE_THIS_FRAME()
ENDPROC

PROC STOP_PLAYER_SHOOTING_THIS_FRAME()

	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_NEXT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_PREV_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_ATTACK)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2) 
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
	
	
	IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
	ENDIF

ENDPROC

PROC MAINTAIN_HIDE_BOUNTY()
	VEHICLE_INDEX vehTemp
	PED_INDEX pedTempDriver
	PLAYER_INDEX playerTemp
	PARTICIPANT_INDEX partTemp
	INT iPart

	BOOL bPauseBounty
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InArea)
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_FailedToStayInArea)
			bPauseBounty = TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaAsPassenger)
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_FailedToStayInArea)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
					vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					pedTempDriver = GET_PED_IN_VEHICLE_SEAT(vehTemp, VS_DRIVER)
					
					IF DOES_ENTITY_EXIST(pedTempDriver)
						IF pedTempDriver <> PLAYER_PED_ID()
							IF IS_PED_A_PLAYER(pedTempDriver)
								playerTemp = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedTempDriver)
								IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerTemp)
									partTemp = NETWORK_GET_PARTICIPANT_INDEX(playerTemp)
									iPart = NATIVE_TO_INT(partTemp)
									IF IS_BIT_SET(playerBD[iPart].iPlayerBitSet, biP_InArea)
										bPauseBounty = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF FM_EVENT_IS_PLAYER_WAITING_FOR_FM_EVENT_TO_START(PLAYER_ID())
		bPauseBounty = TRUE
	ENDIF
	
	IF bPauseBounty
		PAUSE_BOUNTY_TIMER_THIS_FRAME()
		HIDE_BOUNTY_BLIP_THIS_FRAME()
	ENDIF
ENDPROC

#IF FEATURE_STUNT_FM_EVENTS
FUNC MODEL_NAMES GET_AIRCRAFT_MODEL()
	RETURN MAVERICK
ENDFUNC

PROC GET_LOCATION_AND_HEADING_FOR_AIRCRAFT(INT iAircraft, VECTOR &vSpawn, FLOAT &fHead)
	SWITCH iAircraft
		CASE 0	vSpawn = <<-1729.5818, -2943.7695, 12.9443>> fHead = 300.0780	BREAK
		CASE 1	vSpawn = <<677.2984, 769.7758, 204.6846>> 	 fHead = 82.8903 	BREAK
		CASE 2	vSpawn = <<1073.9720, 3003.8894, 40.5508>> 	 fHead = 333.2717	BREAK
		CASE 3	vSpawn = <<1928.6350, 4702.3267, 40.1958>> 	 fHead = 327.9112	BREAK
		CASE 4	vSpawn = <<1278.6503, 6579.3662, 1.5050>> 	 fHead = 84.2600 	BREAK
		CASE 5	vSpawn = <<-1700.4066, -829.8932, 8.2542>> 	 fHead = 70.1811 	BREAK
		CASE 6	vSpawn = <<-2733.5889, 2925.5627, 1.2152>> 	 fHead = 173.6421	BREAK
		CASE 7	vSpawn = <<1493.4181, -2442.9897, 64.9693>>  fHead = 52.9918 	BREAK
		CASE 8	vSpawn = <<569.0449, -772.5692, 10.4088>> 	 fHead = 179.3501	BREAK
		CASE 9	vSpawn = <<-905.1526, 5548.1719, 5.5251>> 	 fHead = 95.8361 	BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL SPAWN_CHALLENGE_VEHICLES()

	MODEL_NAMES mVeh
	INT iDecoratorValue
	INT i
	
	VECTOR vSpawnPoint
	FLOAT fSpawnHeading
				
	REPEAT MAX_AIRCRAFT_TO_SPAWN i
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niAircraft[i])
			mVeh = GET_AIRCRAFT_MODEL()
			IF REQUEST_LOAD_MODEL(mVeh)
			
			
				
				GET_LOCATION_AND_HEADING_FOR_AIRCRAFT(i, vSpawnPoint, fSpawnHeading)
					
				IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(vSpawnPoint)
				

					IF CREATE_NET_VEHICLE(serverBD.niAircraft[i], mVeh, vSpawnPoint, fSpawnHeading, DEFAULT, DEFAULT, DEFAULT,DEFAULT,TRUE,FALSE)	
						NETWORK_FADE_IN_ENTITY(NET_TO_ENT(serverBD.niAircraft[i]),TRUE)					
						SET_VEHICLE_DOORS_LOCKED(NET_TO_VEH(serverBD.niAircraft[i]), VEHICLELOCK_UNLOCKED)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_VEH(serverBD.niAircraft[i]), TRUE)
						SET_VEHICLE_INFLUENCES_WANTED_LEVEL(NET_TO_VEH(serverBD.niAircraft[i]), FALSE)
						SET_MENTAL_STATE_WILL_IGNORE_THIS_VEHICLE(NET_TO_VEH(serverBD.niAircraft[i]),TRUE)
						
						IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh", DECOR_TYPE_INT)
		                    DECOR_SET_INT(NET_TO_VEH(serverBD.niAircraft[i]), "Not_Allow_As_Saved_Veh", MP_NOT_ALLOWED_AS_SAVED_VEHICLE_DEC_SIMEON)
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  - DECORATOR 'Not_Allow_As_Saved_Veh' SET")
		  				ENDIF
						
						//Prevents the vehicle from being used for activities that use passive mode - MJM  
						IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
							
							IF DECOR_EXIST_ON(NET_TO_VEH(serverBD.niAircraft[i]), "MPBitset")
								iDecoratorValue = DECOR_GET_INT(NET_TO_VEH(serverBD.niAircraft[i]), "MPBitset")
							ENDIF
							SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_PASSIVE_MODE_VEHICLE)
							SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_MODDABLE_VEHICLE)
							DECOR_SET_INT(NET_TO_VEH(serverBD.niAircraft[i]), "MPBitset", iDecoratorValue)
						ENDIF
							
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [SPAWN_CHALLENGE_VEHICLES] - Created vehicle ",i ," at  ",vSpawnPoint)
					ELSE
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [SPAWN_CHALLENGE_VEHICLES] Failed to create vehicle ",i, " vSpawnPoint = ", vSpawnPoint)
					ENDIF
				ELSE
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [SPAWN_CHALLENGE_VEHICLES] Point not ok, not spawning vehicle ",i, " vSpawnPoint = ", vSpawnPoint)
				ENDIF
				
			ENDIF
			
			RETURN FALSE
		ENDIF
	ENDREPEAT
		
	RETURN TRUE

ENDFUNC

FUNC BOOL DO_ALL_AIRCRAFT_EXIST()
	INT i
	REPEAT MAX_AIRCRAFT_TO_SPAWN i
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niAircraft[i])
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_PLAYER_SITTING_IN_ANY_PENNED_IN_AIRCRAFT()
	IF IS_PED_INJURED(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	INT i
	
	REPEAT MAX_AIRCRAFT_TO_SPAWN i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niAircraft[i])
			IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niAircraft[i])		
				IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niAircraft[i]))
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

PROC REMOVE_ALL_PENNED_IN_AIRCRAFT_BLIPS()
	INT i
	REPEAT MAX_AIRCRAFT_TO_SPAWN i
		IF DOES_BLIP_EXIST(blipAircraft[i])
			REMOVE_BLIP(blipAircraft[i])
		ENDIF
	ENDREPEAT
ENDPROC

PROC MAINTAIN_PENNED_IN_AIRCRAFT_BLIPS()
	INT i
	
	IF IS_PLAYER_SITTING_IN_ANY_PENNED_IN_AIRCRAFT()
		REMOVE_ALL_PENNED_IN_AIRCRAFT_BLIPS()
		EXIT
	ENDIF
	
	REPEAT MAX_AIRCRAFT_TO_SPAWN i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niAircraft[i])
			IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niAircraft[i])		
				IF NOT DOES_BLIP_EXIST(blipAircraft[i])
					blipAircraft[i] = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(serverBD.niAircraft[i]))
					SET_BLIP_SPRITE(blipAircraft[i],RADAR_TRACE_HELICOPTER)
				//	SET_BLIP_PRIORITY(blipAircraft[i], BLIPPRIORITY_HIGHEST)
				//	SET_BLIP_NAME_FROM_TEXT_FILE(blipAircraft[i], GET_CHALLENGE_BLIP_NAME(iVeh))
					SET_BLIP_COLOUR_FROM_HUD_COLOUR(blipAircraft[i],HUD_COLOUR_BLUE)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [MAINTAIN_PENNED_IN_AIRCRAFT_BLIPS] ADDED BLIP FOR AIRCRAFT ", i)
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(blipAircraft[i])
					REMOVE_BLIP(blipAircraft[i])
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [MAINTAIN_PENNED_IN_AIRCRAFT_BLIPS] VEH NOT DRIVEABLE REMOVE BLIP FOR AIRCRAFT ", i)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC
#ENDIF

FUNC BOOL DO_ALL_PENNED_IN_ENTITIES_EXIST()
	#IF FEATURE_STUNT_FM_EVENTS
		IF serverBD.iPennedInType = PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				SPAWN_CHALLENGE_VEHICLES()
			ENDIF
			
			IF NOT DO_ALL_AIRCRAFT_EXIST()
				RETURN FALSE
			ENDIF
		ENDIF
	#ENDIF
	
	RETURN TRUE
ENDFUNC
FUNC INT GET_RANDOM_START_LOCATION()
	INT iLoc
	
	
	#IF IS_DEBUG_BUILD
	IF MPGlobalsAmbience.iPennedInLoc = -1
	#ENDIF
		iLoc = GET_RANDOM_INT_IN_RANGE(0, MAX_ROUTES)
	#IF IS_DEBUG_BUILD
	ELSE
		iLoc = MPGlobalsAmbience.iPennedInLoc
	ENDIF
	#ENDIF
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - GET_RANDOM_START_LOCATION iLoc  = ", iLoc, " MPGlobalsAmbience.iPennedInLoc = ", MPGlobalsAmbience.iPennedInLoc)
	RETURN iLoc
ENDFUNC

PROC SET_PENNED_IN_TYPE_FROM_ROUTE()
	IF serverBD.iPennedInType != -1
		EXIT
	ENDIF
	
	SWITCH serverBD.iLoc
		CASE 0
		CASE 1
		CASE 2
		CASE 3
		CASE 4
			serverBD.iPennedInType = PENNED_IN_TYPE_ROAD_VEHICLE
		BREAK
		
		#IF FEATURE_STUNT_FM_EVENTS
		CASE 5
			serverBD.iPennedInType = PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY
		BREAK
		#ENDIF
		
		CASE 6
		CASE 7
		CASE 8
		CASE 9
		CASE 10
			serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
			INIT_LAUNCH_BOSS_SERVER_PART()
		BREAK
	ENDSWITCH
	
	//serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [SET_PENNED_IN_TYPE_FROM_ROUTE] serverBD.iPennedInType = ", serverBD.iPennedInType, " serverBD.iLoc = ", serverBD.iLoc)
ENDPROC

FUNC VECTOR GET_START_LOCATION_FOR_CAGED_IN_ROUTE(INT iRoute)
	VECTOR vLoc
	
	SWITCH iRoute
		CASE 6	vLoc = <<770.8729, -232.9857, 65.1145>>	 	BREAK
		CASE 7	vLoc = <<157.237, -1519.715, 28.1416>>	 	BREAK
		CASE 8	vLoc = <<-899.8332, 5567.7617, 2.7747>>	 	BREAK
		CASE 9	vLoc = <<-1894.427, 2010.2701, 140.5029>>	BREAK
		CASE 10	vLoc = <<1036.6892, -619.3324, 57.1851>> 	BREAK
	ENDSWITCH
	
	RETURN vLoc
ENDFUNC


FUNC BOOL IS_ANY_PLAYER_NEAR_START_LOCATION_FOR_ROUTE(INT iRoute, FLOAT fDist = 200.0)
	RETURN IS_ANY_PLAYER_NEAR_POINT(GET_START_LOCATION_FOR_CAGED_IN_ROUTE(iRoute), fDist)
ENDFUNC

FUNC BOOL GET_ROUTE_TO_USE(INT &iRoute)
//	iRoute = 6
//	serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
//	RETURN TRUE
	
	#IF IS_DEBUG_BUILD
		IF NOT IS_CAGED_IN_LAUNCHING()
			IF MPGlobalsAmbience.iPennedInLoc <> -1
				iRoute = MPGlobalsAmbience.iPennedInLoc
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - GET_ROUTE_TO_USE TRUE FROM DEBUG MENU iLoc  = ", iRoute, " MPGlobalsAmbience.iPennedInLoc = ", MPGlobalsAmbience.iPennedInLoc)
				
				
				RETURN TRUE
			ENDIF
		ELSE
			iRoute =  GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION() 
			IF iRoute >= 0
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - GET_ROUTE_TO_USE TRUE FROM GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION iLoc  = ", iRoute, " MPGlobalsAmbience.iPennedInLoc = ", MPGlobalsAmbience.iPennedInLoc)
				RETURN TRUE
			ENDIF
		ENDIF
	#ENDIF
	
	
	
	IF IS_CAGED_IN_LAUNCHING()
		iRoute =  GET_RANDOM_INT_IN_RANGE(6, 11)
		IF NOT IS_ANY_PLAYER_NEAR_START_LOCATION_FOR_ROUTE(iRoute)
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - GET_ROUTE_TO_USE FMMC_TYPE_BIKER_CAGED_IN - ", iRoute)
		ELSE
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - GET_ROUTE_TO_USE REJECTING ROUTE AS THERE'S A NEARBY PLAYER ", iRoute)
			//iRoute = -1
			RETURN FALSE
		ENDIF
	ELSE
		iRoute = GET_RANDOM_INT_IN_RANGE(0, 5)
	ENDIF
	
	IF NOT IS_ROUTE_DISABLED(iRoute)
		IF NOT FM_EVENT_IS_VARIATION_IN_HISTORY_LIST(FMMC_TYPE_PENNED_IN, iRoute)
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - GET_ROUTE_TO_USE TRUE FROM NORMAL LAUNCH iLoc  = ", iRoute, " MPGlobalsAmbience.iPennedInLoc = ", MPGlobalsAmbience.iPennedInLoc)
			RETURN TRUE
		ELSE
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - GET_ROUTE_TO_USE TRUE FROM NORMAL LAUNCH iLoc  = ", iRoute, " IGNORING AS FM_EVENT_IS_VARIATION_IN_HISTORY_LIST" )
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC



PROC FILL_NODES()
//	vNodes[0] = <<-2305.6235, -320.6781, 12.7252>>
//	vNodes[1] = <<-2333.4534, -301.4988, 12.9115>>
//	vNodes[2] = <<-2358.7078, -281.5601, 13.2102>>
//	vNodes[3] = <<-2395.0425, -258.7455, 14.0933>>
//	vNodes[4] = <<-2440.8423, -233.8241, 15.4869>>
	
	IF NOT ARE_VECTORS_EQUAL(vNodes[0], << 0.0, 0.0, 0.0 >>)
		EXIT
	ENDIF


	

	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - FILL_NODES serverBD.iLoc  = ", serverBD.iLoc)
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	SWITCH serverBD.iLoc
		CASE 0
			//-- El Burro
			vNodes[0]	 = <<1476.6367, -2436.0750, 65.2401>>
			vNodes[1]	 = <<1591.1584, -1980.8351, 93.9884>>
			vNodes[2]	 = <<1105.5802, -1734.0144, 34.7122>>
			vNodes[3]	 = <<674.1869, -1733.1830, 28.3527>>
			vNodes[4]	 = <<276.4475, -1857.6871, 25.8607>>
			vNodes[5]	 = <<241.6554, -1713.7201, 28.1339>>
			vNodes[6]	 = <<276.8612, -1670.4513, 28.3137>>
			vNodes[7]	 = <<158.5084, -1512.4120, 28.1416>>
			vNodes[8]	 = <<154.0520, -1394.6040, 28.3049>>
			vNodes[9]	 = <<297.3363, -1202.9008, 28.1848>>
			vNodes[10]	 = <<216.0453, -1130.8625, 28.3051>>
			vNodes[11]	 = <<303.6096, -1124.2592, 28.4586>>
			vNodes[12]	 = <<349.6563, -953.8064, 28.4776>>
			vNodes[13]	 = <<228.3761, -955.8815, 28.3232>>
			iNumNodes = 14
		BREAK
		CASE 1

			// Vineyard (Route 1)
			vNodes[0]	 = <<-1900.1852, 2050.7751, 139.7211>>
			vNodes[1]	 = <<-1797.9186, 1886.7753, 147.7400>>
			vNodes[2]	 = <<-1710.1752, 1885.3156, 160.2170>>
			vNodes[3]	 = <<-1610.5203, 2033.9445, 101.5237>>
			vNodes[4]	 = <<-1517.0043, 2141.7026, 55.0511>>
			vNodes[5]	 = <<-1337.5049, 2178.0967, 51.2807>>
			vNodes[6]	 = <<-1147.6368, 2341.3315, 98.8094>>
			vNodes[7]	 = <<-934.5955, 2262.8425, 140.7183>>
			vNodes[8]	 = <<-730.4700, 2344.8604, 68.4050>>
			vNodes[9]	 = <<-708.7010, 2426.9895, 61.1469>>
			vNodes[10]	 = <<-573.3401, 2500.4941, 50.3152>>
			vNodes[11]	 = <<-545.0461, 2672.9302, 44.4979>>
			vNodes[12]	 = <<-436.4757, 2756.3894, 44.6457>>
			vNodes[13]	 = <<-322.5151, 2775.0254, 61.0514>>
			vNodes[14]	 = <<-149.1279, 2737.5154, 54.9277>>
			vNodes[15]	 = <<222.3020, 2489.4434, 53.8394>>
			vNodes[16]	 = <<569.7415, 2454.8682, 58.4488>>
			vNodes[17]	 = <<753.5460, 2518.7876, 72.1194>>
			vNodes[18]	 = <<924.3738, 2593.3752, 60.2089>>
			vNodes[19]	 = <<965.1749, 2621.3711, 45.0674>>
			vNodes[20]	 = <<1039.3630, 2671.2891, 38.5509>>
			 
			iNumNodes = 21
		BREAK
		
		CASE 2
			//-- Wind Farm
			vNodes[0] = <<2304, 1469, 66>>
			vNodes[1] = <<2131, 2031, 96>>
			vNodes[2] = <<2059, 2868, 52>>
			vNodes[3] = <<2707, 3485, 64>>
			vNodes[4] = <<2855, 3729, 47>>
			vNodes[5] = <<2506, 4153, 41>>
			vNodes[6] = <<2885, 4472, 51>>
			vNodes[7] = <<2786.2361, 4941.3779, 32.6857>>
			vNodes[8] = <<2890.6079, 5034.0259, 30.7725>>
			vNodes[9] = <<3058.7649, 5052.0908, 24.4410>>
			vNodes[10] = <<3063.0515, 5094.1157, 23.0524>>
			vNodes[11] = <<3131.7178, 5101.3706, 20.6628>>
			vNodes[12] = <<3175.0176, 5118.7710, 16.6100>>
			vNodes[13] = <<3228.4673, 5133.3999, 18.3210>>
			vNodes[14] = <<3283.4993, 5150.8643, 17.6323>>
			vNodes[15] = <<3297.6697, 5150.7012, 17.3023>>
			vNodes[16] = <<3320.5652, 5151.2954, 17.3289>> 
			vNodes[17] = <<3337.0173, 5151.8267, 17.4344>>
			vNodes[18] = <<3348.8008, 5152.0161, 18.4475>>

			iNumNodes = 19
		BREAK
		
		CASE 3			
			//LSIA to Maze Bank Arena.
			UNLOCK_AIRPORT_GATES() // This route requires map state changes.

			vNodes[0] 	= <<-1210.1974, -2437.7385, 12.9452>>
			vNodes[1] 	= <<-1342.5873, -2667.4619, 12.9449>>
			vNodes[2] 	= <<-1220.7985, -2861.6501, 12.9455>>
			vNodes[3] 	= <<-1126.435059,-2699.412842,22.002935>>
			vNodes[4]	= <<-1017.039185,-2713.214844,34.565468>>
			vNodes[5] 	= <<-936.59552,-2585.454102,21.386209>>
			vNodes[6] 	= <<-841.792175,-2569.529053,16.836029>>
			vNodes[7] 	= <<-723.109497,-2396.833008,17.031235>>
			vNodes[8] 	= <<-616.298523,-2279.408203,13.040651>>
			vNodes[9] 	= <<-677.783447,-2181.811035,14.214618>>
			vNodes[10] 	= <<-613.153931,-2109.984619,21.81665>>
			vNodes[11] 	= <<-475.037994,-2148.000488,17.486799>>
			vNodes[12] 	= <<-336.459717,-2143.65332,13.957203>>
			vNodes[13] 	= <<-256.331207,-2131.89624,22.140804>>
			vNodes[14] 	= <<-162.249008,-2084.908203,26.850729>>
			vNodes[15] 	= <<-161.8921, -2043.3702, 21.8606>>
			vNodes[16] 	= <<-154.6475, -2009.5287, 21.8819>>
			vNodes[17] 	= <<-186.8038, -2005.3405, 26.6204>>
			vNodes[18]	= <<-216.7214, -1999.2321, 26.7554>>

			iNumNodes = 19
			
			

		//	 Test
//			vNodes[0] 	= <<-1210.1974, -2437.7385, 12.9452>>
//			vNodes[1] =  <<-1198.6550, -2383.3979, 12.9452>>
//			vNodes[2] =  <<-1198.7269, -2358.1582, 12.9452>>
//			vNodes[3] =  <<-1204.1433, -2322.8992, 12.9446>>
//			vNodes[4] =  <<-1209.3929, -2289.0879, 12.9446>>
//			vNodes[5] =	 <<-1213.4330, -2263.0688, 12.9446>>
//			iNumNodes = 6
		BREAK

		CASE 4
			//-- Paleto Bay to Paleto Forest
			vNodes[0] = <<105.4430, 7028.3799, 10.2844>>
			vNodes[1] = <<157.7284, 6525.1431, 30.6533>>
			vNodes[2] = <<-59.2422, 6653.8037, 28.7210>>
			vNodes[3] = <<-168.4514, 6476.5537, 29.0678>>
			vNodes[4] = <<-111.8802, 6419.2456, 30.4455>>
			vNodes[5] = <<-154.2424, 6339.7319, 30.6176>>
			vNodes[6] = <<-93.9484, 6279.4985, 30.3124>>
			vNodes[7] = <<-182.7545, 6195.6855, 30.1968>>
			vNodes[8] = <<-260.1141, 6185.8115, 30.4429>>
			vNodes[9] = <<-304.2122, 6227.9678, 30.4542>>
			vNodes[10] =<<-473.7666, 6066.5293, 29.3400>>
			vNodes[11] =<<-715.7206, 6049.0933, -0.0726>>
			vNodes[12] =<<-811.3691, 5957.2822, 19.4103>>
			vNodes[13] =<<-897.6384, 6050.5581, 43.1834>>

			iNumNodes = 14
		BREAK

		
		#IF FEATURE_STUNT_FM_EVENTS
		//-- Air routes
		CASE 5
			//LSIA to Maze Bank Arena.
		//	UNLOCK_AIRPORT_GATES() // This route requires map state changes.

			vNodes[0] 	= <<-100.0952, -426.9939, 35.2710>>
			vNodes[1] 	= <<-98.9446, -423.3243, 57.5361>> 
			vNodes[2] 	= <<-150.0045, -553.3818, 59.6046>>
			vNodes[3] 	= <<-147.5415, -554.0623, 195.7528>>
			vNodes[4]	= <<-153.4184, -577.8098, 220.6278>>
			vNodes[5] 	= <<-177.7460, -645.8831, 196.4430>>
			vNodes[6] 	= <<-200.7587, -672.9637, 111.8984>>
			vNodes[7] 	= <<-249.6124, -751.9779, 68.3624>>
			vNodes[8] 	= <<-307.9167, -777.4626, 68.2468>>
			vNodes[9] 	= <<-301.9985, -903.9934, 64.2572>>
			vNodes[10] 	= <<-232.9033, -946.9965, 58.6472>>
			vNodes[11] 	= <<-205.6438, -1007.2483, 50.3966>>
			vNodes[12] 	= <<-229.8583, -1100.0001, 53.7555>>
			vNodes[13] 	= <<-207.0158, -1132.3162, 50.6243>>
			vNodes[14] 	= <<-185.5479, -1122.2982, 49.1481>>
			vNodes[15] 	= <<-144.8336, -1039.8969, 64.7437>>
			vNodes[16] 	= <<-130.8733, -1013.4182, 63.2637>>
			vNodes[17] 	= <<-94.6737, -1023.9681, 62.2392>>
			vNodes[18]	= <<-27.0800, -953.7226, 51.1911>> 
			vNodes[19] 	= <<146.7325, -1001.5510, 37.5444>>
			vNodes[20] 	= <<220.6031, -981.7498, 43.7509>> 
			vNodes[21] 	= <<263.6793, -950.0878, 46.8017>> 
			vNodes[22] 	= <<358.3800, -953.9030, 42.4390>> 
			vNodes[23] 	= <<406.8450, -1005.8547, 47.6112>>
			vNodes[24] 	= <<574.0334, -1033.8680, 47.5560>>
			vNodes[25] 	= <<622.2460, -891.4860, 30.2415>> 
			vNodes[26] 	= <<611.6356, -803.4489, 27.2838>> 
			vNodes[27]	= <<514.6652, -767.3392, 45.2011>> 
			vNodes[28] 	= <<426.8458, -761.4703, 58.3940>> 
			vNodes[29] 	= <<357.2304, -734.1724, 64.0690>> 
			vNodes[30] 	= <<313.3172, -627.9628, 67.2870>> 
			vNodes[31] 	= <<286.5880, -583.9189, 67.7187>> 
			vNodes[32] 	= <<290.1108, -492.7945, 52.2439>> 
			vNodes[33] 	= <<290.9938, -436.5169, 57.5871>> 
			vNodes[34] 	= <<248.6064, -407.1052, 62.3840>> 
			vNodes[35] 	= <<184.0140, -392.7382, 64.4339>> 
			vNodes[36]	= <<65.8017, -423.2232, 55.0079>>
			vNodes[37] 	= <<17.1449, -401.8788, 67.0204>>
			vNodes[38] 	= <<4.7689, -399.7884, 63.5673>>
			vNodes[39] 	= <<-76.2476, -344.2556, 63.5545>>
			vNodes[40] 	= <<-82.3624, -341.3049, 45.0090>> 


			iNumNodes = 41
		BREAK
		#ENDIF


		CASE 6 // Caged in route 0 //  SEE ALSO GET_START_LOCATION_FOR_CAGED_IN_ROUTE
			vNodes[0] 	= <<770.8729, -232.9857, 65.1145>>
			vNodes[1] 	= <<838.7502, -256.9497, 64.6297>>
			vNodes[2] 	= <<903.3063, -221.7793, 68.666>>
			vNodes[3] 	= <<935.5661, -166.9423, 73.4974>>
			vNodes[4]	= <<935.502, -150.7537, 73.7236>>
			vNodes[5] 	= <<903.4067, -123.2328, 76.2099>>
			vNodes[6] 	= <<881.9219, -103.4221, 78.4213>>
			vNodes[7] 	= <<880.8146, -80.6718, 77.7643>>
			vNodes[8] 	= <<924.9553, -20.0444, 77.7643>>
			vNodes[9] 	= <<953.8312, -19.9404, 77.764>>
			vNodes[10] 	= <<968.3574, -3.2091, 79.9909>>
			vNodes[11] 	= <<1009.3735, 64.5172, 79.9909>>
			vNodes[12] 	= <<1004.3337, 70.7857, 79.9909>>
			vNodes[13] 	= <<961.9669, 97.0632, 79.9909>>
			vNodes[14] 	= <<974.4698, 116.5434, 79.9909>>
			vNodes[15] 	= <<1023.8721, 169.1899, 79.9905>>
			vNodes[16] 	= <<1023.7948, 181.0932, 79.8559>>
			vNodes[17] 	= <<1003.2242, 191.8311, 79.9087>>
			vNodes[18]	= <<945.6973, 122.5348, 79.7235>>
			vNodes[19] 	= <<931.2999, 104.402, 78.3884>>
			vNodes[20] 	= <<884.1791, 48.7134, 77.631>>
			vNodes[21] 	= <<820.7046, -20.0586, 79.7007>>
			vNodes[22] 	= <<824.5161, -65.76, 79.6442>>
			vNodes[23] 	= <<854.3058, -113.1723, 78.3323>>
			vNodes[24] 	= <<847.928, -140.3758, 77.3008>>
			vNodes[25] 	= <<825.0543, -176.4968, 71.886>>
			vNodes[26] 	= <<803.1815, -203.4303, 71.9304>>
			vNodes[27]	= <<772.341, -248.5203, 65.1143>>
			vNodes[28] 	= <<766.5759, -268.8026, 65.4267>>
			vNodes[29] 	= <<751.6126, -300.7643, 58.892>>
			vNodes[30] 	= <<723.4518, -321.2040, 51.9976>>
			vNodes[31] 	= <<691.1819, -341.8955, 39.0741>>
			vNodes[32] 	= <<644.3198, -353.9926, 29.0699>>
			vNodes[33] 	= <<628.4379, -365.0165, 23.5751>>
			vNodes[34] 	= <<607.2872, -399.7075, 23.7997>>
			vNodes[35] 	= <<599.2954, -478.0787, 23.7562>>
			vNodes[36]	= <<566.7018, -553.8478, 23.7368>>
			vNodes[37] 	= <<538.7103, -643.3187, 23.7397>>
			vNodes[38] 	= <<508.0503, -682.8132, 24.2827>>
			vNodes[39] 	= <<456.5249, -685.2498, 26.8735>>
			vNodes[40] 	= <<456.4284, -821.7599, 26.5584>>
			vNodes[41] 	= <<406.9605, -830.3608, 28.3306>>
			vNodes[42] 	= <<406.1039, -876.7354, 28.3905>>
			vNodes[43] 	= <<402.9865, -938.0498, 28.4551>>
			vNodes[44] 	= <<404.8474, -950.4066, 28.4433>>
			vNodes[45] 	= <<416.964, -954.1458, 28.4324>>
			vNodes[46]	= <<485.1107, -954.6018, 26.3922>>
			vNodes[47] 	= <<496.7718, -957.6362, 26.1527>>
			vNodes[48] 	= <<500.8447, -966.6793, 26.4009>>
			vNodes[49] 	= <<499.6066, -1038.969, 27.357>>
			vNodes[50] 	= <<482.2613, -1070.1821, 28.2113>>
			vNodes[51] 	= <<462.5394, -1125.0022, 28.3189>>
			vNodes[52] 	= <<452.1105, -1131.4785, 28.4588>>
			vNodes[53] 	= <<411.7074, -1132.2982, 28.4582>>
			vNodes[54] 	= <<401.2792, -1126.8384, 28.4871>>
			vNodes[55] 	= <<398.4309, -1108.8948, 28.4618>>
			vNodes[56] 	= <<398.5521, -1011.6366, 28.4602>>
			vNodes[57] 	= <<391.9118, -1000.9362, 28.4172>>
			vNodes[58] 	= <<384.0373, -999.108, 28.4212>>
			vNodes[59]	= <<280.6655, -998.2255, 28.2999>>
			vNodes[60] 	= <<274.747, -997.3961, 28.3239>>
			vNodes[61] 	= <<254.8134, -990.8824, 28.2714>>
			vNodes[62] 	= <<244.1949, -980.8467, 28.3573>>
			vNodes[63] 	= <<244.9183, -958.1761, 28.3041>>
			vNodes[64] 	= <<265.0497, -896.29, 27.9801>>
			vNodes[65] 	= <<259.8165, -869.8301, 28.273>>
			vNodes[66] 	= <<226.9681, -839.9088, 29.2632>>
			vNodes[67] 	= <<224.8172, -815.2034, 29.5278>>
			vNodes[68]	= <<223.5029, -758.9518, 29.8235>>
			vNodes[69] 	= <<234.023, -729.6653, 29.8196>>
			vNodes[70] 	= <<268.7956, -743.4052, 33.64>>
			vNodes[71] 	= <<224.9066, -736.6704, 33.2013>>
			vNodes[72] 	= <<172.0603, -721.1422, 32.1324>>
			vNodes[73] 	= <<75.1364, -688.8935, 30.6218>>
			vNodes[74] 	= <<112.1867, -577.6705, 30.47>>
			vNodes[75] 	= <<58.5058, -554.6415, 32.7843>>
			vNodes[76] 	= <<-35.6377, -545.9135, 38.9156>>
			vNodes[77]	= <<-77.4362, -537.5404, 39.1736>>
			


			iNumNodes = 78
		BREAK





		
		CASE 7 // Caged In Route 1
			vNodes[0] 	= <<174.5999, -2079.6912, 16.7487>>
			vNodes[1] 	= <<152.0048, -2053.0337, 17.3217>>
			vNodes[2] 	= <<150.1924, -2031.7955, 17.3217>>
			vNodes[3] 	= <<152.2747, -2013.484, 17.1417>>
			vNodes[4]	= <<156.9549, -2002.1061, 17.2518>>
			vNodes[5] 	= <<164.4116, -1991.5688, 17.2641>>
			vNodes[6] 	= <<205.3133, -1942.9099, 20.535>>
			vNodes[7] 	= <<211.3832, -1932.9548, 21.5803>>
			vNodes[8] 	= <<208.533, -1920.3668, 22.2848>>
			vNodes[9] 	= <<196.7893, -1907.7605, 22.6872>>
			vNodes[10] 	= <<182.3722, -1898.4238, 22.5606>>
			vNodes[11] 	= <<108.5148, -1864.7413, 23.4672>>
			vNodes[12] 	= <<86.2217, -1841.1938, 24.2874>>
			vNodes[13] 	= <<43.7092, -1805.9956, 24.4549>>
			vNodes[14] 	= <<17.8062, -1777.3511, 28.2925>>
			vNodes[15] 	= <<21.8941, -1750.0643, 28.3031>>
			vNodes[16] 	= <<44.7469, -1721.5055, 28.3031>>
			vNodes[17] 	= <<63.9396, -1707.9142, 28.2608>>
			vNodes[18]	= <<76.1903, -1681.8864, 28.3242>>
			vNodes[19] 	= <<86.0975, -1653.2185, 28.3469>>
			vNodes[20] 	= <<116.5378, -1632.7563, 28.3471>>
			vNodes[21] 	= <<152.7263, -1602.194, 28.3415>>
			vNodes[22] 	= <<143.9074, -1564.7122, 28.2093>>
			vNodes[23] 	= <<145.1265, -1550.9044, 28.1529>>
			vNodes[24] 	= <<153.1621, -1536.4783, 28.3099>>
			vNodes[25] 	= <<156.3295, -1515.1399, 28.1416>>
			vNodes[26] 	= <<148.7433, -1498.5558, 28.1416>>
			vNodes[27]	= <<128.6626, -1476.8137, 28.1416>>
			vNodes[28] 	= <<104.1582, -1414.0946, 28.1921>>
			vNodes[29] 	= <<83.6966, -1380.6022, 28.2915>>
			vNodes[30] 	= <<50.458, -1383.5314, 28.2943>>
			vNodes[31] 	= <<-1.9688, -1383.9425, 28.3059>>
			vNodes[32] 	= <<-24.7047, -1377.8663, 28.1996>>
			vNodes[33] 	= <<-92.3499, -1377.0834, 28.3276>>
			vNodes[34] 	= <<-130.9038, -1387.6121, 28.4695>>
			vNodes[35] 	= <<-148.5784, -1398.7073, 29.0652>>
			vNodes[36]	= <<-148.0146, -1415.7241, 29.7007>>
			vNodes[37] 	= <<-140.9057, -1431.5103, 29.8724>>
			vNodes[38] 	= <<-172.1216, -1469.8943, 31.0887>>
			vNodes[39] 	= <<-200.9877, -1510.3435, 30.632>>
			vNodes[40] 	= <<-228.8574, -1535.4877, 30.6189>>
			vNodes[41] 	= <<-242.2845, -1539.5383, 30.5592>>
			vNodes[42] 	= <<-256.5187, -1570.9397, 30.9427>>
			vNodes[43] 	= <<-268.7609, -1629.4132, 30.8488>>
			vNodes[44] 	= <<-255.8852, -1689.9532, 30.8488>>
			vNodes[45] 	= <<-224.6088, -1727.5677, 31.5576>>
			vNodes[46] 	= <<-182.6528, -1758.6221, 28.8952>>
			vNodes[47]	= <<-148.3479, -1787.9008, 28.7988>>
			vNodes[48] 	= <<-122.57, -1807.9994, 27.3089>>
			vNodes[49] 	= <<-73.0749, -1876.6112, 7.596>>
			vNodes[50] 	= <<-39.416, -1901.59, 9.9099>>
			vNodes[51] 	= <<7.5512, -1955.0984, 4.4117>>
			vNodes[52] 	= <<55.3094, -2021.3112, 0.4589>>
			vNodes[53] 	= <<69.4583, -2095.6624, 0.2051>>
			vNodes[54] 	= <<49.8936, -2146.7078, 1.5683>>
			vNodes[55] 	= <<41.0185, -2157.9426, 4.4323>>
			vNodes[56]	= <<24.7718, -2164.3352, 8.1637>>
			vNodes[57] 	= <<-2.2288, -2158.396, 9.2835>>
			vNodes[58] 	= <<-22.7019, -2158.6682, 9.3111>>
			vNodes[59] 	= <<-38.4647, -2199.5984, 6.8117>>
			vNodes[60] 	= <<-45.3264, -2212.2307, 6.8117>>
			vNodes[61] 	= <<-55.3598, -2222.49, 6.8117>>
			vNodes[62] 	= <<-82.3358, -2223.9185, 6.8117>>
			vNodes[63] 	= <<-131.381, -2223.0808, 6.8117>>
			vNodes[64] 	= <<-150.9629, -2233.8962, 6.8117>>
			vNodes[65] 	= <<-158.0284, -2244.3459, 6.8117>>
			vNodes[66] 	= <<-181.3241, -2250.3481, 6.8117>>
			vNodes[67] 	= <<-262.8565, -2253.1746, 6.9858>>
			vNodes[68]	= <<-294.0698, -2233.2671, 7.2625>>
			vNodes[69] 	= <<-297.9966, -2218.0999, 8.5029>>
			vNodes[70] 	= <<-313.9771, -2206.9487, 8.4925>>
			vNodes[71] 	= <<-327.7408, -2209.1553, 7.2109>>
			vNodes[72] 	= <<-364.1004, -2262.1985, 6.6082>>
			vNodes[73] 	= <<-374.4983, -2272.4255, 6.6082>>
			vNodes[74] 	= <<-375.8295, -2282.5625, 6.6082>>


			iNumNodes = 75
		BREAK


		CASE 8 // Caged in route 2
			vNodes[0] 	=  <<-2653.0486, 1505.6366, 116.9031>>
			vNodes[1] 	=  <<-2663.5945, 1470.7433, 122.7177>>
			vNodes[2] 	=  <<-2681.7805, 1433.7673, 120.1581>>
			vNodes[3] 	=  <<-2701.2942, 1403.5181, 112.9536>>
			vNodes[4]	=  <<-2717.1575, 1376.8065, 105.1303>>
			vNodes[5] 	=  <<-2748.0938, 1336.4413, 89.7583>>
			vNodes[6] 	=  <<-2772.6619, 1306.3376, 82.0139>>
			vNodes[7] 	=  <<-2807.1157, 1282.4966, 71.1255>>
			vNodes[8] 	=  <<-2860.7271, 1279.0245, 58.2654>>
			vNodes[9] 	=  <<-2904.5193, 1259.0967, 40.7003>>
			vNodes[10] 	=  <<-2921.8503, 1257.9926, 37.2144>>
			vNodes[11] 	=  <<-2945.2415, 1243.1675, 31.8372>>
			vNodes[12] 	=  <<-2971.9104, 1212.2054, 23.1993>>
			vNodes[13] 	=  <<-2993.6616, 1208.5237, 18.0748>>
			vNodes[14] 	=  <<-3038.5508, 1209.651, 14.0096>>
			vNodes[15] 	=  <<-3107.9707, 1226.0641, 9.658>>
			vNodes[16] 	=  <<-3199.3032, 1252.4889, 4.3497>>
			vNodes[17] 	=  <<-3218.386, 1252.1344, 2.8398>>
			vNodes[18]	=  <<-3236.5867, 1234.2126, 2.2581>>
			vNodes[19] 	=  <<-3257.2288, 1188.7001, 1.6827>>
			vNodes[20] 	=  <<-3268.8472, 1110.9958, 1.1708>>
			vNodes[21] 	=  <<-3286.1995, 1046.9299, 2.2738>>
			vNodes[22] 	=  <<-3291.4673, 984.0225, 2.3848>>
			vNodes[23] 	=  <<-3291.9409, 951.162, 1.1346>>
			vNodes[24] 	=  <<-3284.4282, 937.3236, 1.0801>>
			vNodes[25] 	=  <<-3266.2422, 925.3865, 1.6379>>
			vNodes[26] 	=  <<-3234.6465, 874.3011, 1.695>>
			vNodes[27]	=  <<-3219.0833, 829.6504, 1.2816>>
			vNodes[28] 	=  <<-3198.2463, 813.9434, 2.7979>>
			vNodes[29] 	=  <<-3165.0793, 798.1581, 6.5002>>
			vNodes[30] 	=  <<-3143.2959, 790.5193, 7.0137>>
			vNodes[31] 	=  <<-3124.5156, 789.1387, 16.9934>>
			vNodes[32] 	=  <<-3094.2124, 777.5895, 18.5135>>
			vNodes[33] 	=  <<-3086.8984, 756.6077, 19.5227>>
			vNodes[34] 	=  <<-3093.9788, 726.5751, 20.3224>>
			vNodes[35] 	=  <<-3082.2632, 693.1727, 15.8985>>
			vNodes[36]	=  <<-3056.8462, 661.2524, 9.0803>>
			vNodes[37] 	=  <<-3043.5581, 636.358, 6.5869>>
			vNodes[38] 	=  <<-3024.625, 583.9476, 6.8377>>
			vNodes[39] 	=  <<-3019.4368, 554.1682, 6.6729>>
			vNodes[40] 	=  <<-3021.3811, 512.9918, 6.234>>
			vNodes[41] 	=  <<-3031.1975, 466.6764, 5.5596>>
			vNodes[42] 	=  <<-3049.9348, 424.7576, 5.5665>>
			vNodes[43] 	=  <<-3066.9609, 377.4491, 6.0668>>
			vNodes[44] 	=  <<-3057.4312, 345.7499, 8.5701>>
			vNodes[45] 	=  <<-3046.6309, 292.6304, 18.9137>>
			vNodes[46]	=  <<-3041.8259, 279.6342, 16.9957>>
			vNodes[47] 	=  <<-3035.3059, 269.0156, 14.7234>>
			vNodes[48] 	=  <<-3012.3623, 233.9675, 15.2325>>
			vNodes[49] 	=  <<-3013.1875, 213.9026, 15.268>>
			vNodes[50] 	=  <<-3011.6711, 196.2197, 15.146>>
			vNodes[51] 	=  <<-3006.4539, 177.695, 14.8855>>
			vNodes[52] 	=  <<-2994.6628, 154.9807, 14.3746>>
			vNodes[53] 	=  <<-2979.6724, 136.2853, 13.7377>>
			vNodes[54] 	=  <<-2950.8318, 112.3324, 12.904>>
			vNodes[55] 	=  <<-2921.3289, 97.7186, 12.7178>>
			vNodes[56] 	=  <<-2851.4888, 75.2672, 13.5427>>
			vNodes[57] 	=  <<-2836.6099, 32.9254, 12.8794>>
			vNodes[58]	=  <<-2831.9875, 18.7339, 10.4085>>
			vNodes[59] 	=  <<-2824.5603, -8.0105, 6.1181>>
			vNodes[60] 	=  <<-2803.6489, -81.9439, -0.221>>
			vNodes[61] 	=  <<-2780.2065, -96.5313, 0.3584>>
			vNodes[62] 	=  <<-2736.6733, -109.0135, 0.5296>>
			vNodes[63] 	=  <<-2700.4514, -141.5591, 0.9655>>
			vNodes[64] 	=  <<-2671.8303, -175.5449, 1.0133>>
			vNodes[65] 	=  <<-2630.613, -198.3817, 2.1479>>
			vNodes[66] 	=  <<-2547.9849, -249.0516, 2.2801>>
			vNodes[67]	=  <<-2473.8474, -295.0556, 2.7219>>
			vNodes[68] 	=  <<-2434.0935, -326.3175, 3.0194>>
			vNodes[69] 	=  <<-2410.1133, -331.2293, 2.6013>>
			vNodes[70] 	=  <<-2376.031, -335.9692, 2.5363>>
			vNodes[71] 	=  <<-2356.186, -344.7604, 2.6766>>
			vNodes[72] 	=  <<-2327.2456, -365.7754, 2.3142>>
			vNodes[73] 	=  <<-2276.5415, -398.5323, 1.8233>>
			vNodes[74] 	=  <<-2230.8225, -415.3987, 3.087>>
			vNodes[75] 	=  <<-2213.564, -418.7813, 5.5765>>
			vNodes[76]	=  <<-2199.3962, -420.4765, 11.8061>>
			vNodes[77] 	=  <<-2176.1367, -416.0254, 12.2345>>
			vNodes[78] 	=  <<-2126.0286, -402.4269, 11.7143>>
			vNodes[79] 	=  <<-2102.2246, -366.2369, 11.8076>>
			vNodes[80] 	=  <<-2095.2676, -318.8827, 12.0248>>
			vNodes[81] 	=  <<-2093.9993, -264.2969, 18.7933>>
			vNodes[82] 	=  <<-2089.104, -214.1321, 19.5783>>
			vNodes[83] 	=  <<-2071.1531, -210.7178, 21.0689>>
			vNodes[84] 	=  <<-2050.0754, -223.6322, 25.6497>>
			vNodes[85] 	=  <<-2030.9808, -224.0641, 27.9967>>
			vNodes[86]	=  <<-1998.8669, -224.0556, 31.1763>>
			vNodes[87] 	=  <<-1987.9215, -232.1616, 32.9357>>
			vNodes[88] 	=  <<-1986.7235, -241.0943, 33.9168>>
			vNodes[89] 	=  <<-1980.2367, -257.3627, 33.6684>>
			vNodes[90] 	=  <<-1966.5735, -261.1625, 34.9521>>
			vNodes[91] 	=  <<-1948.8265, -242.7568, 34.8759>>
			vNodes[92] 	=  <<-1938.9633, -233.8106, 35.8376>>
			vNodes[93] 	=  <<-1911.051, -214.8513, 35.2168>>
			vNodes[94] 	=  <<-1874.8605, -185.5185, 37.115>>
			vNodes[95] 	=  <<-1864.5593, -180.1412, 43.2547>>
			vNodes[96] 	=  <<-1850.8513, -185.8726, 45.6611>>
			vNodes[97] 	=  <<-1834.4126, -189.1538, 48.8693>>
			vNodes[98]	=  <<-1815.9832, -200.1424, 50.7411>>
			vNodes[99] 	=  <<-1805.7876, -203.853, 51.7412>>
			vNodes[100] =  <<-1792.7168, -217.502, 50.9357>>
			vNodes[101] =  <<-1774.6949, -229.8553, 51.7138>>
			vNodes[102] =  <<-1756.9034, -238.1252, 52.0989>>
			vNodes[103] =  <<-1736.2507, -241.4447, 52.672>>
			vNodes[104] =  <<-1712.5735, -240.3414, 53.0528>>
			vNodes[105] =  <<-1699.116, -234.5364, 54.4754>>
			vNodes[106] =  <<-1689.3018, -217.4817, 56.5364>>
			vNodes[107]	=  <<-1682.4846, -202.5944, 56.595>>
			vNodes[108] =  <<-1680.8939, -188.3022, 56.5344>>
			vNodes[109] =  <<-1682.9626, -173.7911, 56.5401>>
			vNodes[110] =  <<-1689.3328, -162.1221, 56.5802>>
			vNodes[111] =  <<-1702.2227, -155.7313, 56.3958>>
			vNodes[112] =  <<-1709.647, -161.5643, 56.4539>>
			vNodes[113] =  <<-1715.4739, -168.1092, 56.503>>
			vNodes[114] =  <<-1725.5365, -168.0596, 56.7325>>
			vNodes[115] =  <<-1733.6442, -169.4988, 57.5363>>
			vNodes[116] =  <<-1733.4342, -177.1803, 58.3052>>
			vNodes[117] =  <<-1731.4307, -182.4709, 57.4901>>
			vNodes[118] =  <<-1730.0248, -191.5667, 57.3605>>

			iNumNodes = 119
		BREAK



		CASE 9 // Caged in route 3
			vNodes[0] 	= <<-1463.9116, 2789.8257, 20.0877>>
			vNodes[1] 	= <<-1477.1481, 2845.4761, 25.45>>
			vNodes[2] 	= <<-1506.9653, 2872.3396, 30.2234>>
			vNodes[3] 	= <<-1577.2604, 2932.7048, 31.917>>
			vNodes[4]	= <<-1591.626, 2997.5771, 32.261>>
			vNodes[5] 	= <<-1639.9797, 3059.1699, 29.948>>
			vNodes[6] 	= <<-1645.0012, 3178.4958, 29.4591>>
			vNodes[7] 	= <<-1655.6416, 3226.4429, 33.7777>>
			vNodes[8] 	= <<-1694.7043, 3249.4023, 32.0673>>
			vNodes[9] 	= <<-1725.4783, 3262.4731, 31.5433>>
			vNodes[10] 	= <<-1951.8118, 3391.708, 31.1172>>
			vNodes[11] 	= <<-1984.6615, 3398.1184, 31.1172>>
			vNodes[12] 	= <<-2011.3973, 3398.7388, 30.4796>>
			vNodes[13] 	= <<-2034.7754, 3383.3872, 30.2602>>
			vNodes[14] 	= <<-2065.7727, 3384.6152, 30.3096>>
			vNodes[15] 	= <<-2109.0266, 3382.4377, 30.9568>>
			vNodes[16] 	= <<-2137.1829, 3393.0408, 31.1325>>
			vNodes[17] 	= <<-2143.365, 3397.5947, 32.7541>>
			vNodes[18]	= <<-2161.1116, 3410.7014, 31.6593>>
			vNodes[19] 	= <<-2187.906, 3415.6211, 31.7261>>
			vNodes[20] 	= <<-2199.6711, 3426.6108, 31.6199>>
			vNodes[21] 	= <<-2217.0139, 3434.519, 31.6187>>
			vNodes[22] 	= <<-2237.5474, 3433.5896, 31.6194>>
			vNodes[23] 	= <<-2251.7886, 3432.9031, 32.4218>>
			vNodes[24] 	= <<-2256.2153, 3432.5642, 31.059>>
			vNodes[25] 	= <<-2276.6184, 3439.0962, 31.0393>>
			vNodes[26] 	= <<-2292.811, 3446.875, 30.8394>>
			vNodes[27]	= <<-2315.6826, 3443.7644, 30.3699>>
			vNodes[28] 	= <<-2341.1333, 3432.4043, 28.5928>>
			vNodes[29] 	= <<-2365.814, 3428.073, 26.8742>>
			vNodes[30] 	= <<-2381.9834, 3423.8833, 30.3016>>
			vNodes[31] 	= <<-2394.0405, 3416.595, 30.5506>>
			vNodes[32] 	= <<-2530.5889, 3337.5151, 30.4319>>
			vNodes[33] 	= <<-2548.137, 3339.1338, 28.6846>>
			vNodes[34] 	= <<-2562.0081, 3341.5801, 28.0224>>
			vNodes[35] 	= <<-2588.2229, 3351.021, 24.6836>>
			vNodes[36]	= <<-2594.0706, 3356.0793, 22.1251>>
			vNodes[37] 	= <<-2595.7449, 3365.9146, 17.9894>>
			vNodes[38] 	= <<-2595.793, 3377.5737, 12.8038>>
			vNodes[39] 	= <<-2608.6619, 3392.6123, 13.1174>>
			vNodes[40] 	= <<-2623.4954, 3407.1426, 13.6065>>
			vNodes[41] 	= <<-2653.2124, 3424.437, 13.7192>>
			vNodes[42] 	= <<-2699.8762, 3437.2478, 12.9223>>
			vNodes[43] 	= <<-2721.9482, 3443.9595, 11.9598>>
			vNodes[44] 	= <<-2779.478, 3456.6555, 10.6559>>
			vNodes[45] 	= <<-2804.6438, 3470.1799, 9.7878>>
			vNodes[46]	= <<-2825.7708, 3501.1694, 7.9953>>
			vNodes[47] 	= <<-2837.0437, 3518.584, 7.4085>>
			vNodes[48] 	= <<-2864.5059, 3523.5164, 7.1469>>
			vNodes[49] 	= <<-2913.5396, 3523.989, 7.2342>>
			vNodes[50] 	= <<-2944.4956, 3519.3491, 7.215>>
			vNodes[51] 	= <<-2957.936, 3509.1182, 7.5977>>
			vNodes[52] 	= <<-2968.7124, 3494.8137, 8.0967>>
			vNodes[53] 	= <<-2982.3132, 3472.1548, 8.5156>>
			vNodes[54] 	= <<-2997.3589, 3440.916, 8.5745>>
			vNodes[55] 	= <<-3006.3542, 3441.8679, 8.3125>>
			vNodes[56] 	= <<-3010.1709, 3442.1609, 7.8361>>
			vNodes[57] 	= <<-3016.1267, 3442.6323, 6.1649>>
			vNodes[58] 	= <<-3044.0186, 3439.9016, 2.6837>>
			vNodes[59]	= <<-3083.78, 3434.4299, 0.6736>>
			vNodes[60] 	= <<-3108.8635, 3426.9143, 0.5969>>
			vNodes[61] 	= <<-3121.0525, 3417.5256, 0.8856>>
			vNodes[62] 	= <<-3128.9331, 3401.3784, 0.723>>
			vNodes[63] 	= <<-3127.1169, 3385.6082, -0.0289>>
			vNodes[64] 	= <<-3120.6143, 3370.2451, -0.3007>>
			vNodes[65] 	= <<-3120.8433, 3355.3367, -0.3184>>
			vNodes[66] 	= <<-3131.457, 3337.6663, -0.4124>>
			vNodes[67] 	= <<-3130.0549, 3331.1333, 0.453>>
			vNodes[68]	= <<-3126.0444, 3311.187, 1.9264>>
			vNodes[69] 	= <<-3121.6252, 3297.5825, 4.969>>
			vNodes[70] 	= <<-3114.6248, 3288.4163, 5.7302>>
			vNodes[71] 	= <<-3109.7534, 3286.7271, 5.3114>>
			vNodes[72] 	= <<-3094.2087, 3285.1382, 3.4169>>
			vNodes[73] 	= <<-3078.4607, 3294.3872, 3.5013>>
			vNodes[74] 	= <<-3064.6958, 3299.3279, 4.393>>
			vNodes[75] 	= <<-3042.4851, 3298.6611, 4.2975>>
			vNodes[76] 	= <<-3012.8215, 3298.938, 8.3148>>
			vNodes[77]	= <<-3005.9194, 3298.7898, 8.8372>>
			vNodes[78]	= <<-2979.5244, 3296.4094, 9.4458>>
			vNodes[79] 	= <<-2965.9092, 3290.2358, 10.2551>>
			vNodes[80] 	= <<-2956.04, 3279.582, 10.3873>>
			vNodes[81] 	= <<-2941.2598, 3267.0317, 10.8597>>
			vNodes[82] 	= <<-2928.6233, 3259.0635, 10.4515>>
			vNodes[83] 	= <<-2922.0327, 3248.5322, 9.6349>>
			vNodes[84] 	= <<-2917.5198, 3233.9338, 9.5718>>
			vNodes[85] 	= <<-2907.6809, 3215.4233, 9.8787>>
			vNodes[86] 	= <<-2879.2188, 3175.5818, 9.9335>>
			vNodes[87]	= <<-2837.9138, 3137.4614, 9.104>>
			vNodes[88] 	= <<-2778.5952, 3095.4136, 7.9147>>
			vNodes[89] 	= <<-2755.3391, 3093.4353, 7.9238>>
			vNodes[90] 	= <<-2732.7539, 3091.2634, 11.8101>>
			vNodes[91] 	= <<-2715.5098, 3088.6289, 18.9392>>
			vNodes[92] 	= <<-2704.9119, 3088.0649, 22.5439>>
			vNodes[93] 	= <<-2698.5708, 3088.8293, 24.5249>>
			vNodes[94] 	= <<-2688.0886, 3088.7551, 26.9138>>
			vNodes[95] 	= <<-2668.2019, 3087.6262, 31.0204>>
			vNodes[96]	= <<-2663.9529, 3087.5232, 31.2972>>
			vNodes[97] 	= <<-2649.5559, 3076.8613, 31.3129>>
			vNodes[98] 	= <<-2625.8721, 3048.0569, 31.3104>>
			vNodes[99] 	= <<-2598.4082, 3019.6755, 31.4648>>
			vNodes[100] = <<-2586.2258, 3015.6416, 31.7198>>
			vNodes[101] = <<-2560.5659, 2996.8481, 37.5706>>
			vNodes[102] = <<-2552.7366, 2986.7651, 36.5308>>
			vNodes[103] = <<-2545.4165, 2969.7095, 35.8487>>
			vNodes[104] = <<-2540.3657, 2955.3162, 35.0142>>
			vNodes[105] = <<-2532.9888, 2947.1414, 34.4408>>
			vNodes[106]	= <<-2527.3262, 2945.0369, 33.7632>>
			vNodes[107] = <<-2510.0122, 2935.0139, 31.8324>>
			vNodes[108] = <<-2506.855, 2927.783, 31.8324>>
			vNodes[109] = <<-2502, 2923.3804, 31.8656>>
			vNodes[110] = <<-2453.2827, 2895.4429, 31.827>>
			vNodes[111] = <<-2448.6328, 2893.2339, 31.8105>>
			vNodes[112] = <<-2441.9705, 2894.0757, 31.8161>>
			vNodes[113] = <<-2435.053, 2899.2109, 31.8101>>
			vNodes[114] = <<-2419.4944, 2912.7275, 31.8101>>
			vNodes[115] = <<-2410.9985, 2920.6646, 31.8101>>
			vNodes[116] = <<-2402.9812, 2926.1086, 31.8101>>
			vNodes[117] = <<-2388.7112, 2931.5481, 31.8101>>
			vNodes[118] = <<-2376.1946, 2935.0681, 31.8101>>
			vNodes[119]	= <<-2362.1738, 2937.1716, 31.8101>>
			vNodes[120] = <<-2349.6929, 2937.4854, 31.8097>>
			vNodes[121] = <<-2335.7468, 2935.8542, 31.8095>>
			vNodes[122] = <<-2318.9424, 2929.6599, 31.7445>>
			vNodes[123] = <<-2297.4214, 2923.0554, 31.402>>
			vNodes[124] = <<-2273.0647, 2913.4954, 31.1485>>
			vNodes[125] = <<-2255.3586, 2902.8337, 31.3591>>
			vNodes[126] = <<-2229.6899, 2883.8074, 31.236>>
			vNodes[127] = <<-2202.9248, 2869.3142, 31.1578>>
			vNodes[128] = <<-2183.3064, 2857.042, 31.1936>>
			vNodes[129] = <<-2163.49, 2841.4644, 31.3816>>
			vNodes[130] = <<-2149.8618, 2823.8911, 31.4544>>
			vNodes[131] = <<-2144.5872, 2808.7349, 31.4497>>
			vNodes[132] = <<-2132.1765, 2795.0198, 31.7241>>
			vNodes[133] = <<-2108.1406, 2785.4629, 31.7808>>
			vNodes[134]	= <<-2068.5793, 2784.0205, 31.4615>>
			vNodes[135] = <<-2051.136, 2791.3096, 31.825>>
			vNodes[136] = <<-2042.0018, 2795.7283, 31.2093>>
			vNodes[137] = <<-2024.1208, 2799.8665, 31.5836>>
			vNodes[138] = <<-2004.3717, 2797.4036, 31.7003>>
			vNodes[139] = <<-1976.0474, 2791.3828, 31.5104>>
			vNodes[140] = <<-1947.483, 2778.5649, 31.1414>>
			vNodes[141] = <<-1936.3785, 2768.8975, 31.1447>>
			vNodes[142] = <<-1929.7583, 2758.106, 30.6489>>
			vNodes[143] = <<-1925.9906, 2751.949, 25.9925>>
			vNodes[144] = <<-1921.4097, 2746.2681, 24.8392>>
			vNodes[145] = <<-1915.1368, 2739.6587, 23.8019>>
			vNodes[146] = <<-1900.3069, 2727.2009, 21.1878>>
			vNodes[147]	= <<-1883.7455, 2715.2625, 19.6892>>
			vNodes[148] = <<-1875.7476, 2711.0803, 12.552>>
			vNodes[149] = <<-1867.8955, 2703.1389, 3.0778>>
			vNodes[150] = <<-1861.9625, 2692.4229, 3.1018>>
			vNodes[151] = <<-1857.4758, 2683.575, 3.2905>>
			vNodes[152] = <<-1848.774, 2668.9883, 2.0379>>
			vNodes[153] = <<-1842.3397, 2663.9846, 1.5316>>
			vNodes[154] = <<-1838.2953, 2661.605, 0.5979>>
			vNodes[155] = <<-1815.5259, 2665.4827, 0.7053>>
			vNodes[156] = <<-1805.6318, 2669.1565, 2.2314>>
			vNodes[157] = <<-1786.1377, 2670.6023, 2.2725>>
			vNodes[158] = <<-1771.0234, 2668.4907, 2.1361>>
			vNodes[159] = <<-1760.8687, 2665.9751, 1.8861>>
			vNodes[160] = <<-1755.2838, 2664.3669, 1.7509>>
			vNodes[161] = <<-1748.7069, 2663.0032, 1.9587>>
			vNodes[162]	= <<-1736.1887, 2659.8857, 1.9917>>
			vNodes[163] = <<-1728.4756, 2658.1829, 1.9464>>
			vNodes[164] = <<-1722.7252, 2657.9961, 2.0025>>
			vNodes[165] = <<-1719.5546, 2657.8838, 1.4411>>
			vNodes[166] = <<-1715.6056, 2657.7336, 0.7234>>
			vNodes[167] = <<-1710.3817, 2658.301, 0.4668>>
			vNodes[168] = <<-1707.3594, 2659.4832, 0.282>>
			vNodes[169] = <<-1703.7361, 2660.5779, 0.5126>>
			vNodes[170] = <<-1697.2767, 2662.1221, 0.7382>>
			vNodes[171] = <<-1694.4106, 2662.7053, 1.5014>>
			vNodes[172] = <<-1690.5967, 2663.5403, 1.8258>>
			vNodes[173] = <<-1683.0225, 2665.116, 1.8626>>
			vNodes[174] = <<-1677.6127, 2664.2864, 1.8642>>
			vNodes[175]	= <<-1675.0409, 2662.9934, 1.525>>
			vNodes[176] = <<-1669.6145, 2661.5696, 1.3865>>
			vNodes[177] = <<-1660.9961, 2660.5144, 1.5346>>
			vNodes[178] = <<-1654.2736, 2660.6213, 1.3163>>
			vNodes[179] = <<-1652.6024, 2661.7214, 0.846>>
			vNodes[180] = <<-1650.7717, 2663.6042, 0.3205>>
			vNodes[181] = <<-1648.7227, 2666.7673, -0.3498>>
			vNodes[182] = <<-1646.6138, 2670.3889, -0.2246>>
			vNodes[183] = <<-1645.2985, 2672.6589, 0.4074>>
			vNodes[184] = <<-1643.8861, 2675.4792, 1.3415>>
			vNodes[185] = <<-1643.0524, 2678.9778, 1.7816>>
			vNodes[186] = <<-1643.8099, 2684.7126, 1.9387>>
			vNodes[187] = <<-1644.4364, 2692.437, 2.6905>>
			vNodes[188] = <<-1643.396, 2700.8857, 4.7337>>
			vNodes[189] = <<-1642.3713, 2702.6987, 4.6934>>
			vNodes[190]	= <<-1634.9198, 2708.2915, 4.7769>>
			vNodes[191] = <<-1622.8417, 2712.6648, 4.7364>>
			vNodes[192] = <<-1616.7406, 2718.5923, 4.7278>>
			vNodes[193] = <<-1615.26, 2726.2915, 4.782>>
			vNodes[194] = <<-1615.3722, 2740.1968, 5.415>>
			vNodes[195] = <<-1615.5627, 2747.6731, 7.4045>>
			vNodes[196] = <<-1615.0973, 2751.5652, 8.8844>>
			vNodes[197] = <<-1611.7622, 2755.532, 10.7072>>
			vNodes[198] = <<-1608.429, 2757.8992, 11.9354>>
			vNodes[199] = <<-1603.6677, 2759.2871, 13.1858>>
			vNodes[200] = <<-1595.8516, 2761.2637, 15.6171>>
			vNodes[201] = <<-1591.1067, 2762.9976, 17.0786>>
			vNodes[202] = <<-1588.6313, 2765.0896, 17.4527>>
			vNodes[203]	= <<-1583.6526, 2766.2439, 17.432>>
			vNodes[204] = <<-1575.535, 2763.5183, 17.2576>>
			vNodes[205] = <<-1568.7128, 2759.8894, 17.0294>>
			vNodes[206] = <<-1560.9313, 2756.5371, 16.5327>>
			vNodes[207] = <<-1550.3893, 2752.6716, 16.8426>>
			vNodes[208] = <<-1539.9153, 2745.9878, 16.6513>>
			vNodes[209] = <<-1329.1594, 2560.4021, 16.6908>>
			vNodes[210] = <<-1312.1677, 2545.4243, 17.302>>
			vNodes[211] = <<-1299.6853, 2536.0061, 17.8598>>
			vNodes[212] = <<-1294.1882, 2532.7388, 18.3548>>
			vNodes[213] = <<-1288.1488, 2528.1138, 18.6202>>
						  
						  
			iNumNodes = 214
		BREAK

		CASE 10 // Caged in route 4
			vNodes[0] 	= <<1007.3921, -677.4196, 55.6986>>
			vNodes[1] 	= <<1015.8800, -640.8702, 57.6189>>
			vNodes[2] 	= <<1028.693, -521.0793, 60.0037>>
			vNodes[3] 	= <<1001.0128, -469.0658, 62.3685>>
			vNodes[4]	= <<1024.2726, -438.0688, 64.0854>>
			vNodes[5] 	= <<1099.3308, -371.6267, 66.0863>>
			vNodes[6] 	= <<1154.2778, -350.7854, 66.0453>>
			vNodes[7] 	= <<1196.4534, -317.2407, 68.006>>
			vNodes[8] 	= <<1228.6578, -285.5526, 69.5564>>
			vNodes[9] 	= <<1269.1396, -280.8539, 78.9299>>
			vNodes[10] 	= <<1303.4813, -279.4128, 89.277>>
			vNodes[11] 	= <<1307.2644, -261.738, 93.4534>>
			vNodes[12] 	= <<1275.5297, -228.7392, 97.2897>>
			vNodes[13] 	= <<1280.2871, -201.123, 100.2999>>
			vNodes[14] 	= <<1316.5217, -182.0543, 106.8589>>
			vNodes[15] 	= <<1328.3549, -161.1792, 109.7738>>
			vNodes[16] 	= <<1330.9841, -124.4583, 116.3178>>
			vNodes[17] 	= <<1349.7177, -112.3109, 120.3794>>
			vNodes[18]	= <<1440.8389, -126.4933, 136.0253>>
			vNodes[19] 	= <<1547.4641, -88.492, 155.8418>>
			vNodes[20] 	= <<1572.3961, -73.2005, 158.7627>>
			vNodes[21] 	= <<1617.5365, -76.5607, 164.2157>>
			vNodes[22] 	= <<1634.6884, -86.0398, 166.32>>
			vNodes[23] 	= <<1652.0536, -86.2994, 169.5492>>
			vNodes[24] 	= <<1677.1063, -69.6387, 172.8701>>
			vNodes[25] 	= <<1708.0125, -81.5238, 176.1925>>
			vNodes[26] 	= <<1727.5781, -106.2123, 177.8113>>
			vNodes[27]	= <<1743.8796, -104.5237, 180.5338>>
			vNodes[28] 	= <<1787.022, -75.6841, 189.1488>>
			vNodes[29] 	= <<1830.5481, -88.8782, 185.5151>>
			vNodes[30] 	= <<1869.1479, -84.2897, 188.3576>>
			vNodes[31] 	= <<1908.1248, -94.0947, 189.7598>>
			vNodes[32] 	= <<1935.8561, -70.0841, 194.3318>>
			vNodes[33] 	= <<1965.0117, -81.9638, 206.0977>>
			vNodes[34] 	= <<1994.3817, -81.4886, 210.8208>>
			vNodes[35] 	= <<2015.3173, -15.1936, 200.4373>>
			vNodes[36]	= <<2045.5973, -7.7503, 209.3502>>
			vNodes[37] 	= <<2097.7356, 3.6562, 214.3764>>
			vNodes[38] 	= <<2170.9116, -23.0015, 227.7495>>
			vNodes[39] 	= <<2216.6003, -39.4283, 202.1804>>
			vNodes[40] 	= <<2265.2534, -42.4669, 176.595>>
			vNodes[41] 	= <<2317.6343, -62.7793, 143.5891>>
			vNodes[42] 	= <<2361.6211, -88.1377, 114.5316>>
			vNodes[43] 	= <<2415.5408, -127.203, 88.2837>>
			vNodes[44] 	= <<2442.6055, -132.4298, 88.5436>>
			vNodes[45] 	= <<2467.7297, -87.5151, 90.4025>>
			vNodes[46]	= <<2478.9272, -49.3336, 91.7194>>
			vNodes[47] 	= <<2514.8621, -54.7729, 90.5497>>
			vNodes[48] 	= <<2535.4668, -44.4498, 95.4292>>
			vNodes[49] 	= <<2551.9873, -14.4109, 96.8787>>
			vNodes[50] 	= <<2564.8479, 7.4979, 95.322>>
			vNodes[51] 	= <<2574.2439, 28.346, 94.1519>>
			vNodes[52] 	= <<2583.1035, 64.6646, 97.2344>>
			vNodes[53] 	= <<2590.3132, 90.8608, 96.7709>>
			vNodes[54] 	= <<2605.6155, 146.4206, 97.0373>>
			vNodes[55] 	= <<2608.4385, 159.9902, 97.7789>>
			vNodes[56] 	= <<2616.8008, 193.4394, 97.8768>>
			vNodes[57] 	= <<2620.917, 208.1355, 99.8613>>
			vNodes[58] 	= <<2622.7163, 214.0569, 100.3944>>
			vNodes[59]	= <<2625.1553, 223.4091, 100.4454>>
			vNodes[60] 	= <<2628.5095, 238.1807, 98.0427>>
			vNodes[61] 	= <<2631.146, 251.6104, 99.134>>
			vNodes[62] 	= <<2644.0332, 286.1281, 96.6342>>
			vNodes[63] 	= <<2668.0095, 344.2533, 93.6781>>
			vNodes[64] 	= <<2674.79, 475.0252, 93.3655>>
			vNodes[65] 	= <<2677.3213, 563.7356, 92.1778>>
			vNodes[66] 	= <<2676.7358, 657.8879, 88.5073>>
			vNodes[67] 	= <<2689.552, 707.0382, 83.2467>>
			vNodes[68] 	= <<2694.6851, 719.9797, 79.1180>>
			vNodes[69]	= <<2705.1357, 742.6617, 63.8294>>
			vNodes[70] 	= <<2732.7515, 770.7942, 48.8289>>
			vNodes[71] 	= <<2754.7522, 779.1329, 37.7362>>
			vNodes[72] 	= <<2795.073, 775.9935, 19.6025>>
			vNodes[73] 	= <<2815.3784, 772.0298, 18.5162>>
			vNodes[74] 	= <<2823.1011, 760.8618, 18.5399>>
			vNodes[75] 	= <<2829.4165, 747.6486, 18.4988>>
			vNodes[76] 	= <<2838.5505, 742.6433, 18.5283>>
			vNodes[77] 	= <<2852.9902, 747.0413, 18.3301>>
			vNodes[78] 	= <<2866.3821, 755.1819, 18.1503>>
			vNodes[79]	= <<2876.9031, 759.4967, 16.8825>>
			vNodes[80] 	= <<2883.9407, 767.9843, 17.9021>>
			vNodes[81] 	= <<2890.1692, 774.4565, 18.7225>>
			vNodes[82] 	= <<2898.9746, 775.8782, 19.8196>>
			vNodes[83] 	= <<2908.9463, 777.8163, 22.677>>
			vNodes[84] 	= <<2911.9561, 780.126, 23.7715>>
			vNodes[85] 	= <<2917.168, 783.8945, 24.2316>>
			vNodes[86] 	= <<2924.8569, 790.0166, 24.7335>>
			vNodes[87] 	= <<2934.356, 798.6711, 23.9277>>
			vNodes[88] 	= <<2940.8499, 803.635, 23.7534>>
			vNodes[89]	= <<2944.6406, 806.6476, 23.5292>>
			vNodes[90] 	= <<2951.7722, 813.8921, 22.0205>>

			
			iNumNodes = 91
		BREAK
	ENDSWITCH
	
	
	INT i
	VECTOR vStart, vEnd
	REPEAT iNumNodes i
		vStart = vNodes[i]
		vEnd = vNodes[i+1]
		IF NOT ARE_VECTORS_EQUAL(vEnd, << 0.0, 0.0, 0.0>>)
			fDistToNextNode[i] = GET_DISTANCE_BETWEEN_COORDS(vStart, vEnd)
			PRINTLN("     ---------->     PENNED IN - FILL_NODES fDistToNextNode[", i, "] = ", fDistToNextNode[i])
			fRouteDist += fDistToNextNode[i]
		ENDIF
	ENDREPEAT
	
	fDist70Percent = (fRouteDist * 70) / 100 
	
	FM_EVENT_SET_ACTIVE_FM_EVENT_LOCAL_ACTION_VECTOR(vNodes[0])
//	PRINTLN("     ---------->     PENNED IN - FILL_NODES fRouteDist = ", fRouteDist)
	
//	fRouteDist = GET_DISTANCE_BETWEEN_COORDS(vNodes[0], vNodes[iNumNodes - 1])
ENDPROC

FUNC FLOAT GET_DISTANCE_MARKER_HAS_TRAVELLED()
	FLOAT fDist = GET_DISTANCE_BETWEEN_COORDS(vMyBlipPos, vNodes[iMyCurrentNode])
	
	INT i
	
	IF iMyCurrentNode > 0
		FOR i = 0 TO iMyCurrentNode - 1
			fDist += fDistToNextNode[i]
		ENDFOR
	ENDIF
	//PRINTLN("     ---------->     PENNED IN - GET_DISTANCE_MARKER_HAS_TRAVELLED = ", fDist)
	RETURN fDist
ENDFUNC

FUNC FLOAT GET_DISTANCE_MARKER_STILL_HAS_TO_TRAVEL()
	FLOAT fDist = GET_DISTANCE_BETWEEN_COORDS(vMyBlipPos, vNodes[iMyCurrentNode+1])
	INT iNextNode = iMyCurrentNode + 1
	INT i
//	PRINTLN("     ---------->     PENNED IN - GET_DISTANCE_MARKER_STILL_HAS_TO_TRAVEL iNumNodes = ", iNumNodes, " iMyCurrentNode = ",iMyCurrentNode, " Dist to next node = ", fDist)
	IF iNextNode < (iNumNodes - 1)
		FOR i = iNextNode TO iNumNodes - 1
			fDist += fDistToNextNode[i]
	//		PRINTLN("     ---------->     PENNED IN - GET_DISTANCE_MARKER_STILL_HAS_TO_TRAVEL fDistToNextNode[", i , "] = ", fDistToNextNode[i], " Distance now ", fDist)
		ENDFOR
	ENDIF
//	PRINTLN("     ---------->     PENNED IN - GET_DISTANCE_MARKER_STILL_HAS_TO_TRAVEL = ", fDist)
	RETURN fDist
ENDFUNC

FUNC VECTOR GET_START_COORDS_FOR_AREA()
	RETURN vNodes[0]
ENDFUNC

FUNC FLOAT GET_START_CORONA_SIZE_FOR_ROUTE()
	FLOAT fSize
	SWITCH serverBD.iLoc
		CASE 0
			fSize = 155.375
		BREAK
		
		CASE 1
			fSize = 94.750 // 101.625
		BREAK
		
		CASE 2
			fSize = 198.750
		BREAK
		
		CASE 3
			fSize = 250.0
		BREAK
		
		CASE 4
			fSize = 196.375
		BREAK
		
		#IF FEATURE_STUNT_FM_EVENTS
		CASE 5
			fSize =  93.125
		BREAK
		#ENDIF
		
		
		
		CASE 7
			fSize = 36.0
		BREAK
		
		CASE 6
		CASE 8
		CASE 9
		CASE 10
			fSize =  42.5
		BREAK
	ENDSWITCH
	
	RETURN fSize
ENDFUNC
FUNC VECTOR GET_START_CORONA_LOCATION_FOR_ROUTE()
	VECTOR vStart
	SWITCH serverBD.iLoc
		CASE 0
			vStart = vMyBlipPos
			vStart.x = 1457.639 //1472.439
			vStart.y = -2371.45
		BREAK
		
		CASE 1
			vStart = vMyBlipPos
			vStart.x = -1903.733 //-1903.3
			vStart.y = 2042.75 //2038.025
		BREAK
		
		CASE 2
			vStart = vMyBlipPos
			vStart.x = 2237.850
			vStart.y = 1469.0
		BREAK
		
		CASE 3
			vStart = vMyBlipPos
			vStart.x = -1210.197
			vStart.y = -2437.739
		BREAK
		
		CASE 4
			vStart = vMyBlipPos
			vStart.x = 106.968
			vStart.y = 7003.605
		BREAK
		
		#IF FEATURE_STUNT_FM_EVENTS
		CASE 5
			vStart = vMyBlipPos
			vStart.x = -101.545
			vStart.y = -434.894
		BREAK
		#ENDIF
		
		CASE 6
			vStart = vMyBlipPos
		BREAK
		
		CASE 7
			vStart = vMyBlipPos
		//	vStart.x = 188.850
		//	vStart.y = -2087.891
		BREAK
		
		CASE 8
			vStart = vMyBlipPos
			vStart.x = -2653.049
			vStart.y = 1505.637
		BREAK
		
		CASE 9
			vStart = vMyBlipPos
			vStart.x = -1464.237
			vStart.y = 2789.826
		BREAK
		
		CASE 10
			vStart = vMyBlipPos
			vStart.x = 999.892
			vStart.y = -677.420
		BREAK
		
	ENDSWITCH
	
	RETURN vStart
ENDFUNC



PROC ADD_START_BLIP()
	TEXT_LABEL_15 tl15Label  
	IF NOT DOES_BLIP_EXIST(blipStart)
		IF OK_TO_DISPLAY_BLIPS_FOR_MODE()
		//	blipStart = ADD_BLIP_FOR_COORD(GET_START_COORDS_FOR_AREA())
			blipStart = ADD_BLIP_FOR_COORD(GET_START_CORONA_LOCATION_FOR_ROUTE())
			
			
			IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
				SET_BLIP_SPRITE(blipStart, RADAR_TRACE_BIKER_CAGED_IN)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [ADD_START_BLIP] Added Caged in blip")
			ELSE
				SET_BLIP_SPRITE(blipStart, RADAR_TRACE_PENNED_IN)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [ADD_START_BLIP] Added Penned in blip")
			ENDIF
			
			SET_BLIP_FLASHES(blipStart, TRUE)
			SET_BLIP_FLASH_TIMER(blipStart, 7000)
			SET_BLIP_PRIORITY(blipStart, BLIPPRIORITY_HIGHEST)
			tl15Label = GET_PENNED_IN_BLIP_NAME()
			SET_BLIP_NAME_FROM_TEXT_FILE(blipStart, tl15Label)
			//SET_BLIP_COLOUR(blipStart, BLIP_COLOUR_PURPLE)
			
			
			IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
				SET_BLIP_COLOUR_FROM_HUD_COLOUR(blipStart, hclLaunchGang)
			ELSE
				SET_BLIP_COLOUR_FROM_HUD_COLOUR(blipStart, HUD_COLOUR_NET_PLAYER2)
			ENDIF
			
			IF NOT IS_BIT_SET(iBoolsBitSet, biFlashedRadar)
				FLASH_MINIMAP_DISPLAY()
				SET_BIT(iBoolsBitSet, biFlashedRadar)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Adds a blip to the Veh
PROC ADD_AREA_BLIP(VECTOR vCoords,BOOL bIsStart = FALSE)
//	UNUSED_PARAMETER(vCoords)
	IF NOT DOES_BLIP_EXIST(blipArea)
		IF OK_TO_DISPLAY_BLIPS_FOR_MODE()
			IF NOT bIsStart
				blipArea = ADD_BLIP_FOR_RADIUS(vCoords, fMyBlipSize)
				
				
				IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
					SET_BLIP_COLOUR(blipArea, BLIP_COLOUR_YELLOW)
				ELSE
					SET_BLIP_COLOUR(blipArea, BLIP_COLOUR_YELLOW)
				ENDIF
			ELSE
				blipArea = ADD_BLIP_FOR_RADIUS(GET_START_CORONA_LOCATION_FOR_ROUTE(), (GET_START_CORONA_SIZE_FOR_ROUTE()/2.0))
				
				
				IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
					SET_BLIP_COLOUR_FROM_HUD_COLOUR(blipArea, HUD_COLOUR_NET_PLAYER2)
				ELSE
					SET_BLIP_COLOUR_FROM_HUD_COLOUR(blipArea, hclLaunchGang)
				ENDIF
			ENDIF
			
			SET_BLIP_ALPHA(blipArea, 100)
			SHOW_HEIGHT_ON_BLIP(blipArea, FALSE)
			//NET_NL() CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  AREA BLIP ADDED ") NET_NL()
		ENDIF
	ENDIF

ENDPROC

PROC REMOVE_AREA_BLIP()
	IF DOES_BLIP_EXIST(blipArea)
		REMOVE_BLIP(blipArea)
	//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  AREA BLIP REMOVED    <----------     ") NET_NL()
	ENDIF
	
ENDPROC

FUNC BOOL ADJUST_BLIP_SPEED_OVER_TIME(FLOAT fStartSpeed, FLOAT fEndSpeed, FLOAT fTime)
	FLOAT fDiff = ABSF(fStartSpeed - fEndSpeed)
	
	IF fMyBlipMoveMultiplier < fEndSpeed
		fMyBlipMoveMultiplier +=  FLOOR(((fDiff/ fTime) * GET_FRAME_TIME()))
		IF fMyBlipMoveMultiplier > fEndSpeed
			fMyBlipMoveMultiplier = fEndSpeed
		ENDIF
	ELSE
		fMyBlipMoveMultiplier = fEndSpeed
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
/// PURPOSE:
///    Adjust the size of the area from fStartSize to fEndSize over fTime ms
FUNC BOOL ADJUST_BLIP_RADIUS_OVER_TIME(FLOAT fStartSize, FLOAT fEndSize, FLOAT fTime)
	FLOAT fDiff = ABSF(fStartSize - fEndSize)
	IF fMyBlipSize > fEndSize
		fMyBlipSize -=  FLOOR(((fDiff/ fTime) * GET_FRAME_TIME()))
		IF fMyBlipSize < fEndSize
			fMyBlipSize = fEndSize
		ENDIF
	ELSE
		fMyBlipSize = fEndSize
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Linearly adjust the size of the area from AREA_START_RADIUS m to AREA_FIN_RADIUS m over the straight-line distance from the start
///    position to the final node.
PROC ADJUST_BLIP_RADIUS_OVER_DISTANCE()
	
	
	FLOAT fDistStillToGo = GET_DISTANCE_MARKER_STILL_HAS_TO_TRAVEL() //GET_DISTANCE_BETWEEN_COORDS(vNodes[iNumNodes-1], vMyBlipPos)
	FLOAT fDistPer = fDistStillToGo/fRouteDist
	
	FLOAT fNewSize =  (((GET_PEN_START_SIZE() - GET_PEN_END_SIZE()) * fDistPer) + GET_PEN_END_SIZE()) * GET_PEN_SHRINK_MULTIPLIER()
	
	IF fNewSize < GET_PEN_END_SIZE()
		fNewSize = GET_PEN_END_SIZE()
	ENDIF
	
	IF fNewSize > GET_PEN_START_SIZE()
		fNewSize = GET_PEN_START_SIZE()
	ENDIF
	
	
	fMyOldBlipSize = fMyBlipSize
	
	IF fNewSize > fMyOldBlipSize
		fNewSize = fMyOldBlipSize
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_PennedInSizeCheck")
		
	ENDIF
	#ENDIF
	
	
	fMyBlipSize = fNewSize
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_PennedInSizeCheck")
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [ADJUST_BLIP_RADIUS_OVER_DISTANCE]               ")
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [ADJUST_BLIP_RADIUS_OVER_DISTANCE] fMyOldBlipSize = ", fMyOldBlipSize)
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [ADJUST_BLIP_RADIUS_OVER_DISTANCE] fMyBlipSize = ", fMyBlipSize)
	ENDIF
	#ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_ReachedEndOfRoute)
		 IF fMyBlipSize <> GET_PEN_END_SIZE()
		 	fMyBlipSize = GET_PEN_END_SIZE()
			#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_PennedInSizeCheck")
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [ADJUST_BLIP_RADIUS_OVER_DISTANCE] FORCING TO PEN END SIZE fMyBlipSize = ", fMyBlipSize)
				ENDIF
			#ENDIF
		ENDIF
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(timeMyPenSizeUpdate)
	OR (HAS_NET_TIMER_STARTED(timeMyPenSizeUpdate) AND HAS_NET_TIMER_EXPIRED(timeMyPenSizeUpdate, 5000))
		RESET_NET_TIMER(timeMyPenSizeUpdate)
		START_NET_TIMER(timeMyPenSizeUpdate)
		playerBD[PARTICIPANT_ID_TO_INT()].fMyPenSize = fMyBlipSize
	ENDIF
ENDPROC

BOOL bAdjust
PROC UPDATE_BLIP_RADIUS()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	OPEN_SCRIPT_PROFILE_MARKER_GROUP("UPDATE_BLIP_RADIUS") // MUST BE AT START OF FUNCTION
	#ENDIF
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_LEFT)
		bAdjust = TRUE
	ENDIF
	#ENDIF
	
	IF bAdjust
		REMOVE_AREA_BLIP()
		IF ADJUST_BLIP_RADIUS_OVER_TIME(100.0, 10.0, 5.0)
			bAdjust = TRUE
		ENDIF
	ENDIF
	
//	REMOVE_AREA_BLIP()
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("REMOVE_AREA_BLIP")
	#ENDIF
	#ENDIF
	
	ADJUST_BLIP_RADIUS_OVER_DISTANCE()
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("ADJUST_BLIP_RADIUS_OVER_DISTANCE")
	#ENDIF
	#ENDIF
	
	//ADD_AREA_BLIP(vMyBlipPos)
	IF DOES_BLIP_EXIST(blipArea)
		SET_BLIP_SCALE(blipArea, fMyBlipSize)
	ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("ADD_AREA_BLIP")
	#ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	CLOSE_SCRIPT_PROFILE_MARKER_GROUP() // MUST BE AT START OF FUNCTION
	#ENDIF
	#ENDIF
ENDPROC

PROC MAINTAIN_MOVE_MULTIPLIER_HOST()
	FLOAT fTempSpeed
	FLOAT fSpeedDiff
	FLOAT fDistTravPer
	FLOAT fHalfDist
	FLOAT fNewSpeed
	IF NOT HAS_NET_TIMER_STARTED(serverBD.timeSinceEventBegin)
		START_NET_TIMER(serverBD.timeSinceEventBegin)
	ENDIF
	
	IF NOT HAS_NET_TIMER_EXPIRED(serverBD.timeSinceEventBegin, 10000)
		// BLIP_MOVE_MULTIPLIER_START
		EXIT 
	ELSE
		IF NOT HAS_NET_TIMER_EXPIRED(serverBD.timeSinceEventBegin, 20000)
			IF NOT HAS_NET_TIMER_STARTED(timeUpdateServerMoveRate)
				START_NET_TIMER(timeUpdateServerMoveRate)
			ENDIF
			
			IF HAS_NET_TIMER_EXPIRED(timeUpdateServerMoveRate, 1000)
				fTempSpeed = serverBD.fMoveMultiplier
				IF fTempSpeed < GET_PEN_MAX_SPEED_MODIFIER() //BLIP_MOVE_MULTIPLIER_MAX
					fTempSpeed += 0.5
					
					fTempSpeed *= GET_PEN_SPEED_MULTIPLIER()
					
					IF fTempSpeed > GET_PEN_MAX_SPEED_MODIFIER()	
						fTempSpeed = GET_PEN_MAX_SPEED_MODIFIER()	
					ENDIF
					serverBD.fMoveMultiplier = fTempSpeed
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [MAINTAIN_MOVE_MULTIPLIER_HOST] fMoveMultiplier now ", serverBD.fMoveMultiplier)
				ENDIF
				
				RESET_NET_TIMER(timeUpdateServerMoveRate)
			ENDIF
		ELSE
			IF fDistTravelled <= (fRouteDist / 2.0)
				IF serverBD.fMoveMultiplier <> GET_PEN_MAX_SPEED_MODIFIER()
					serverBD.fMoveMultiplier = GET_PEN_MAX_SPEED_MODIFIER()
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [MAINTAIN_MOVE_MULTIPLIER_HOST] fMoveMultiplier now BLIP_MOVE_MULTIPLIER_MAX as 20 seconds expired ")
				ENDIF
			ELSE	
				IF NOT HAS_NET_TIMER_STARTED(timeUpdateServerMoveRate)
					START_NET_TIMER(timeUpdateServerMoveRate)
				ELSE
					IF HAS_NET_TIMER_EXPIRED(timeUpdateServerMoveRate, 1000)
						IF serverBD.fMoveMultiplier > GET_PEN_END_SPEED_MODIFIER()
							fSpeedDiff = GET_PEN_MAX_SPEED_MODIFIER() - GET_PEN_END_SPEED_MODIFIER()
							
							fHalfDist = (fRouteDist / 2.0)
							fDistTravPer = (fDistTravelled - fHalfDist) / fHalfDist
							fNewSpeed = GET_PEN_MAX_SPEED_MODIFIER() - (fSpeedDiff * fDistTravPer)
							
							fNewSpeed *= 0.8 // 2383510
							
							fNewSpeed *= GET_PEN_SPEED_MULTIPLIER()
							
							IF fNewSpeed < GET_PEN_END_SPEED_MODIFIER()
								fNewSpeed = GET_PEN_END_SPEED_MODIFIER()
							ENDIF
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [MAINTAIN_MOVE_MULTIPLIER_HOST] fMoveMultiplier now  ", fNewSpeed, " fDistTravelled = ", fDistTravelled, " fDistTravPer = ", fDistTravPer)
							serverBD.fMoveMultiplier = fNewSpeed
							RESET_NET_TIMER(timeUpdateServerMoveRate)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/*
PROC UPDATE_BLIP_POS(BOOL bSKipIfHost = TRUE)
	IF bSKipIfHost
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			EXIT
		ENDIF
	ENDIF
	
//	IF iMyCurrentNode < 9
//		iMyCurrentNode = 9
//		vMyBlipPos = vNodes[9]
//	ENDIF
	
	INT iNextNode = iMyCurrentNode + 1
	VECTOR vNextNodePos 
	VECTOR vDirNextNode
	VECTOR vNomDir
	VECTOR vTempNewPos
	FLOAT fDistNextNode2 
	FLOAT fDistNewPos2
	IF iNextNode < MAX_NODES
		vNextNodePos = vNodes[iNextNode]
		fDistNextNode2 = VDIST2(vMyBlipPos, vNextNodePos)
		
		IF NOT ARE_VECTORS_EQUAL(vNodes[iNextNode], << 0.0, 0.0, 0.0 >>)
			IF NOT ARE_VECTORS_EQUAL(vNodes[iMyCurrentNode], << 0.0, 0.0, 0.0 >>)
				IF fDistNextNode2 < (0.1 * 0.1)
					vMyBlipPos = vNodes[iNextNode]
					iMyCurrentNode++
				ELSE
					vDirNextNode = vNextNodePos - vMyBlipPos
					vNomDir = NORMALISE_VECTOR(vDirNextNode)
					vTempNewPos = vMyBlipPos + ((vNomDir * GET_FRAME_TIME() * serverBD.fMoveMultiplier))
			//		vMyBlipPos += (vNomDir * GET_FRAME_TIME() * BLIP_MOVE_MULTIPLIER_MAX)
					
					fDistNewPos2 = VDIST2(vMyBlipPos, vTempNewPos) 
					IF fDistNewPos2 > fDistNextNode2
						vMyBlipPos = vNextNodePos
						#IF IS_DEBUG_BUILD
							IF bWdDoBlipDebug
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN Setting vMyBlipPos = vNextNodePos as fDistNewPos2 = ", fDistNewPos2, " > fDistNextNode2 = ", fDistNextNode2, " , vNextNodePos = ", vNextNodePos )
							ENDIF
						#ENDIF
					ELSE
						vMyBlipPos = vTempNewPos
						
						#IF IS_DEBUG_BUILD
							IF bWdDoBlipDebug
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN Setting vMyBlipPos = vTempNewPos as fDistNewPos2 = ", fDistNewPos2, " <= fDistNextNode2 = ", fDistNextNode2, " , vTempNewPos = ", vTempNewPos )
							ENDIF
						#ENDIF
						
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ReachedEnd)
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ReachedEnd)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - UPDATE_BLIP_POS - SET biP_ReachedEnd")
			ENDIF
//			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
//				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ReachedEndOfRoute)
//					SET_BIT(serverBD.iServerBitSet, biS_ReachedEndOfRoute)
//					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - UPDATE_BLIP_POS - SET biS_ReachedEndOfRoute")
//				ENDIF
//			ENDIF
		ENDIF
		
	ENDIF
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF NOT HAS_NET_TIMER_STARTED(serverBD.timePosUpdate)
			START_NET_TIMER(serverBD.timePosUpdate)
		ENDIF
		
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ReachedEndOfRoute)
			IF NOT HAS_NET_TIMER_EXPIRED(serverBD.timePosUpdate, 20000)
				serverBD.vSpherePos = vMyBlipPos
				serverBD.iNode = iMyCurrentNode
			ELSE
				IF HAS_NET_TIMER_EXPIRED(serverBD.timePosUpdate, 21000)
					RESET_NET_TIMER(serverBD.timePosUpdate)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT ARE_VECTORS_EQUAL(serverBD.vSpherePos, << 0.0, 0.0, 0.0 >>)
		IF iLocalCurrentNode <> serverBD.iNode
		OR NOT ARE_VECTORS_EQUAL(vLocalBlipPos, serverBD.vSpherePos)
			iLocalCurrentNode = serverBD.iNode
			
			vLocalBlipPos = serverBD.vSpherePos
			
			vMyBlipPos = vLocalBlipPos
			iMyCurrentNode = iLocalCurrentNode
		ENDIF
	ENDIF
	
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_ReachedEndOfRoute)
		IF NOT ARE_VECTORS_EQUAL(vMyBlipPos, vNodes[iNumNodes-1])
			vMyBlipPos = vNodes[iNumNodes-1]
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - UPDATE_BLIP_POS - Setting to final node pos as server says we're at the end ")
		ENDIF
	ENDIF
		
	
	#IF IS_DEBUG_BUILD
		IF bWdDoBlipDebug
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - UPDATE_BLIP_POS - vMyBlipPos = ", vMyBlipPos, " iMyCurrentNode = ", iMyCurrentNode, " Move multiplier = ",serverBD.fMoveMultiplier)
		ENDIF
	#ENDIF
	
	
	IF DOES_BLIP_EXIST(blipArea)
		SET_BLIP_COORDS(blipArea, vMyBlipPos)
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bPauseSphere
		vMyBlipPos = vNodes[0]
	ENDIF
	#ENDIF
ENDPROC
*/

FUNC FLOAT GET_LOCAL_MOVE_MULTIPLIER()
	#IF FEATURE_STUNT_FM_EVENTS
	IF serverBD.iPennedInType = PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY
		RETURN 1.0
	ENDIF
	#ENDIF
	
	RETURN 0.9
ENDFUNC

VECTOR vOldBlipPos
//BOOL bMoveBackwards
FLOAT fTempMoveMulti
PROC UPDATE_BLIP_POS_V2(BOOL bSKipIfHost = TRUE)
	
	IF bSKipIfHost
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			EXIT
		ENDIF
	ENDIF

	IF serverBD.iPennedInType = PENNED_IN_TYPE_ROAD_VEHICLE
	OR serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
		fTempMoveMulti = GET_LOCAL_MOVE_MULTIPLIER()
	
	#IF FEATURE_STUNT_FM_EVENTS
	ELSE
		IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_InitLocalMulti)
			fTempMoveMulti = GET_LOCAL_MOVE_MULTIPLIER()
			SET_BIT(iBoolsBitSet2, bi2_InitLocalMulti)
		ENDIF
	#ENDIF	
	ENDIF
	
	INT iNextNode = iMyCurrentNode + 1
	VECTOR vNextNodePos 
	VECTOR vDirNextNode
	VECTOR vNomDir
	VECTOR vTempNewPos
	FLOAT fDistNextNode2 
	FLOAT fDistNewPos2
	
	BOOL bNodeUpdate
	BOOL bServerUpdate
	IF NOT ARE_VECTORS_EQUAL(vMyBlipPos, << 0.0, 0.0, 0.0>>)
		IF NOT ARE_VECTORS_EQUAL(vOldBlipPos, vMyBlipPos)
			vOldBlipPos = vMyBlipPos
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	FLOAT fServerBlipCHoiceDist
	FLOAT fOldBlipPosDist

	#ENDIF
	
	VECTOR vLocalBlipPosChoice
	VECTOR vServerBlipPosChoice
	
	
	
	
	#IF IS_DEBUG_BUILD
		
		FLOAT fLocalBlipPosCHoiceDist	
		VECTOR vCurrentNodePosFOrChoice = vNodes[iMyCurrentNode]


		IF bWdDoBlipDebug
		//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [UPDATE_BLIP_POS_V2] " )
			//CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [UPDATE_BLIP_POS] start updated vOldBlipPos = ", vOldBlipPos )
		ENDIF

		
	#ENDIF
	
	IF iNextNode < MAX_NODES
		vNextNodePos = vNodes[iNextNode]
		fDistNextNode2 = VDIST2(vMyBlipPos, vNextNodePos)
		
		IF NOT ARE_VECTORS_EQUAL(vNodes[iNextNode], << 0.0, 0.0, 0.0 >>)
			IF NOT ARE_VECTORS_EQUAL(vNodes[iMyCurrentNode], << 0.0, 0.0, 0.0 >>)
			//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - UPDATE_BLIP_POS_V2.....")
			//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - UPDATE_BLIP_POS_V2			vMyBlipPos 				= ", vMyBlipPos)
			//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - UPDATE_BLIP_POS_V2			iMyCurrentNode 			= ", iMyCurrentNode)
			//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - UPDATE_BLIP_POS_V2			vNodes[iMyCurrentNode] 	= ", vNodes[iMyCurrentNode])
			//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - UPDATE_BLIP_POS_V2			iNextNode 				= ", iNextNode)
			//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - UPDATE_BLIP_POS_V2			vNodes[iNextNode] 		= ", vNodes[iNextNode])
			//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - UPDATE_BLIP_POS_V2			vNextNodePos	 		= ", vNextNodePos)
				IF fDistNextNode2 < (0.1 * 0.1)
					vMyBlipPos = vNodes[iNextNode]
					iMyCurrentNode++
					
					bNodeUpdate = TRUE
				ELSE
					vDirNextNode = vNextNodePos - vMyBlipPos
					vNomDir = NORMALISE_VECTOR(vDirNextNode)
					
					IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
						vTempNewPos = vMyBlipPos + ((vNomDir * GET_FRAME_TIME() * serverBD.fMoveMultiplier * fTempMoveMulti))
					ELSE
						vTempNewPos = vMyBlipPos + ((vNomDir * GET_FRAME_TIME() * serverBD.fMoveMultiplier))
					ENDIF
					
					fDistNewPos2 = VDIST2(vMyBlipPos, vTempNewPos) 
			//		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - UPDATE_BLIP_POS_V2			fDistNewPos2	 		= ", fDistNewPos2)
			//		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - UPDATE_BLIP_POS_V2			fDistNextNode2	 		= ", fDistNextNode2)
					IF fDistNewPos2 > fDistNextNode2 
						//-- The next potential sphere position is further away from the next node than the current position. Just use the node position
						vLocalBlipPosChoice = vNextNodePos
			//			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - UPDATE_BLIP_POS_V2 vLocalBlipPosChoice = vNextNodePos = ", vNextNodePos)
					ELSE
						//-- Next position is closer to the next node than the current position
						vLocalBlipPosChoice = vTempNewPos

			//			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - UPDATE_BLIP_POS_V2 vLocalBlipPosChoice = vTempNewPos = ", vTempNewPos)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - UPDATE_BLIP_POS_V2  ARE_VECTORS_EQUAL(vNodes[iNextNode], << 0.0, 0.0, 0.0 >>)")
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ReachedEnd)
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ReachedEnd)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - UPDATE_BLIP_POS_V2 - SET biP_ReachedEnd")
			ENDIF
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ReachedEndOfRoute)
				IF NOT ARE_VECTORS_EQUAL(vNodes[iNumNodes-1], << 0.0, 0.0, 0.0 >>)
					vLocalBlipPosChoice = vNodes[iNumNodes-1]
					
					#IF IS_DEBUG_BUILD
						IF bWdDoBlipDebug
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [UPDATE_BLIP_POS_V2] SETTING vLocalBlipPosChoice = vNodes[iNumNodes-1] AS NEXT NODE IS ZERO VECTOR")
						ENDIF
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	
	//-- Update server
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF NOT HAS_NET_TIMER_STARTED(serverBD.timePosUpdate)
			START_NET_TIMER(serverBD.timePosUpdate)
		ENDIF
		
		//CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN -  Tim - ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timePosUpdate))
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ReachedEndOfRoute)
			IF NOT HAS_NET_TIMER_EXPIRED(serverBD.timePosUpdate, 16000)
				IF NOT bNodeUpdate
					serverBD.vSpherePos = vLocalBlipPosChoice
					serverBD.iNode = iMyCurrentNode
				ELSE
					serverBD.vSpherePos = vMyBlipPos
					serverBD.iNode = iMyCurrentNode
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(serverBD.timePosUpdate, 16500)
					RESET_NET_TIMER(serverBD.timePosUpdate)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	//-- Check if there was a server update
	IF NOT ARE_VECTORS_EQUAL(serverBD.vSpherePos, << 0.0, 0.0, 0.0 >>)
		IF iLocalCurrentNode <> serverBD.iNode
		OR NOT ARE_VECTORS_EQUAL(vLocalBlipPos, serverBD.vSpherePos)
		
			IF iLocalCurrentNode <> serverBD.iNode
				iLocalCurrentNode = serverBD.iNode
			
				vLocalBlipPos = serverBD.vSpherePos
			
				vMyBlipPos = vLocalBlipPos
				iMyCurrentNode = iLocalCurrentNode
				bNodeUpdate = TRUE
			ELSE
				vLocalBlipPos = serverBD.vSpherePos
				vServerBlipPosChoice = serverBD.vSpherePos
			ENDIF
			
			bServerUpdate = TRUE

		ELSE

		ENDIF
	ENDIF
	

	
	IF NOT bNodeUpdate
		
		IF bServerUpdate
			// Sync with server
			
			#IF IS_DEBUG_BUILD
				IF bWdDoBlipDebug
					//-- Figure out if the server wants to move the blip backwards
					fServerBlipCHoiceDist = GET_DISTANCE_BETWEEN_COORDS(vCurrentNodePosFOrChoice, vServerBlipPosChoice, FALSE) //-- Distance from where server wants to put the sphere from the last node
					fOldBlipPosDist = GET_DISTANCE_BETWEEN_COORDS(vCurrentNodePosFOrChoice, vOldBlipPos, FALSE) //-- Distance from where the blip was after last updated to the last node
					fLocalBlipPosCHoiceDist = GET_DISTANCE_BETWEEN_COORDS(vCurrentNodePosFOrChoice, vLocalBlipPosChoice, FALSE) //-- Distance from where local player wants to put the sphere from the last node

					
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [UPDATE_BLIP_POS_V2] ")
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [UPDATE_BLIP_POS_V2] SERVER UPDATE")
					IF fServerBlipCHoiceDist < fOldBlipPosDist
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [UPDATE_BLIP_POS_V2] SERVER UPDATE WANT TO MOVE BACKWARDS Server Dist = ", fServerBlipCHoiceDist, " Old Dist = ", fOldBlipPosDist)
					ENDIF
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [UPDATE_BLIP_POS_V2] ******************** SERVER UPDATE vOldBlipPos = ", vOldBlipPos, " fOldBlipPosDist = ", fOldBlipPosDist)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [UPDATE_BLIP_POS_V2] ******************** SERVER UPDATE vServerBlipPosChoice = ", vServerBlipPosChoice, " fServerBlipCHoiceDist = ", fServerBlipCHoiceDist)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [UPDATE_BLIP_POS_V2] ******************** SERVER UPDATE vCurrentNodePosFOrChoice = ", vCurrentNodePosFOrChoice, " fLocalBlipPosCHoiceDist = ", fLocalBlipPosCHoiceDist)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [UPDATE_BLIP_POS_V2] ")

				ENDIF
			#ENDIF
			
			BOOL bUseServerPos = TRUE
			
			#IF FEATURE_STUNT_FM_EVENTS
			IF serverBD.iPennedInType = PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY
				VECTOR vLastNodeHit = vNodes[iMyCurrentNode]
				FLOAT fServerDistFromLastNode =  GET_DISTANCE_BETWEEN_COORDS(vLastNodeHit, vServerBlipPosChoice, FALSE)
				FLOAT fLastUpdateDistanceFromLastNode = GET_DISTANCE_BETWEEN_COORDS(vLastNodeHit, vOldBlipPos, FALSE)
				IF fServerDistFromLastNode < fLastUpdateDistanceFromLastNode
					fTempMoveMulti -= 0.1
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [UPDATE_BLIP_POS_V2] SERVER UPDATE WANT TO MOVE BACKWARDS Server Dist = ", fServerDistFromLastNode, " Old Dist = ", fLastUpdateDistanceFromLastNode, " local multiplier now ", fTempMoveMulti)
					bUseServerPos = FALSE
				ELSE
					IF fTempMoveMulti != GET_LOCAL_MOVE_MULTIPLIER()
						fTempMoveMulti = GET_LOCAL_MOVE_MULTIPLIER()
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [UPDATE_BLIP_POS_V2] RESET fTempMoveMulti")
					ENDIF
				ENDIF
			ENDIF
			#ENDIF
			////
			IF bUseServerPos 
				vMyBlipPos = vServerBlipPosChoice 
			ENDIF
				

		ELSE
			//-- No server update
			vMyBlipPos = vLocalBlipPosChoice
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			IF bWdDoBlipDebug
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [UPDATE_BLIP_POS_V2] NODE UPDATE ")
			ENDIF
		#ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF bWdDoBlipDebug
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - UPDATE_BLIP_POS_V2 - vMyBlipPos = ", vMyBlipPos, " iMyCurrentNode = ", iMyCurrentNode, " Move multiplier = ",serverBD.fMoveMultiplier)
		ENDIF
	#ENDIF
	
	//-- If we're at the end of the route, use the end-of-route coord
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_ReachedEndOfRoute)
		IF NOT ARE_VECTORS_EQUAL(vMyBlipPos, vNodes[iNumNodes-1])
			vMyBlipPos = vNodes[iNumNodes-1]
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - UPDATE_BLIP_POS_V2 - Setting to final node pos as server says we're at the end ")
		ENDIF
	ENDIF
		
	
	IF DOES_BLIP_EXIST(blipArea)
		SET_BLIP_COORDS(blipArea, vMyBlipPos)
	ENDIF
	
	#IF IS_DEBUG_BUILD
//	DISPLAY_PENNED_IN_STRING_AND_NUMBER(0.1, 0.2, "fTempMoveMulti", ROUND(10.0 * fTempMoveMulti))
	IF bPauseSphere
		vMyBlipPos = vNodes[0]
	ENDIF
	#ENDIF
ENDPROC


FUNC BOOL WAS_LOCAL_PLAYER_PLAYER_REMOVED_FROM_EVENT()
	RETURN(IS_BIT_SET(iBoolsBitSet2, bi2_RemovedFromEventAsPass)
	OR IS_BIT_SET(iBoolsBitSet2, bi2_RemovedFromEventAsDri))
ENDFUNC

FUNC BOOL IS_SPECTATING_WITH_HELI()
	IF PARTICIPANT_ID_TO_INT() <> -1
		RETURN IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_IsSpectating) 
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_UI_BE_HIDDEN(BOOL bCheckSAfeToDisplayEventUi = TRUE)
	BOOL bSHouldHide
	
	
	
	IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
		IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(PLAYER_ID())
			bSHouldHide = TRUE
		ENDIF
		
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaAsPassenger)
			IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(PLAYER_ID())
				bSHouldHide = TRUE
			ENDIF
		ENDIF
	ELSE
		IF GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
			PRINTLN("     ---------->     PENNED IN [SHOULD_UI_BE_HIDDEN] TRUE GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE  <---------- ")
			bSHouldHide = TRUE
		ENDIF
		
		IF GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE()
			PRINTLN("     ---------->     PENNED IN [SHOULD_UI_BE_HIDDEN] TRUE GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE  <---------- ")
			bSHouldHide = TRUE
		ENDIF
		
		IF NOT MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
			PRINTLN("     ---------->     PENNED IN [SHOULD_UI_BE_HIDDEN] TRUE GB_IS_IT_SAFE_TO_DISPLAY_BOSS_MISSION_UI  <---------- ")
			bSHouldHide = TRUE
		ENDIF
		
		IF GB_SHOULD_HIDE_GANG_BOSS_EVENT(FMMC_TYPE_BIKER_CAGED_IN)
			PRINTLN("     ---------->     PENNED IN [SHOULD_UI_BE_HIDDEN] TRUE GB_SHOULD_HIDE_GANG_BOSS_EVENT  <---------- ")
			bSHouldHide = TRUE
		ENDIF
		
	ENDIF
	
	
	
	
	
	IF bCheckSAfeToDisplayEventUi
		IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
			IF NOT MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
				PRINTLN("     ---------->     PENNED IN [SHOULD_UI_BE_HIDDEN] TRUE GB_IS_IT_SAFE_TO_DISPLAY_BOSS_MISSION_UI  <---------- ")
				bSHouldHide = TRUE
			ENDIF
		ELSE
			IF NOT IS_IT_SAFE_TO_DISPLAY_FM_EVENT_UI()
				PRINTLN("     ---------->     PENNED IN [SHOULD_UI_BE_HIDDEN] TRUE IS_IT_SAFE_TO_DISPLAY_FM_EVENT_UI  <---------- ")
				bSHouldHide = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_FM_TYPE_UNLOCKED(FMMC_TYPE_PENNED_IN)
		bSHouldHide = TRUE
	ENDIF
	
	
	
	IF g_bCelebrationScreenIsActive
		PRINTLN("     ---------->     PENNED IN [SHOULD_UI_BE_HIDDEN] TRUE g_bCelebrationScreenIsActive")
		bSHouldHide = TRUE
	ENDIF
	
	IF IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
		PRINTLN("     ---------->     PENNED IN [SHOULD_UI_BE_HIDDEN] TRUE IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE")
		bSHouldHide = TRUE
	ENDIF
		
	RETURN bSHouldHide
ENDFUNC


PROC UPDATE_SPHERE(BOOL bIsStart = FALSE, BOOL bNonPart = FALSE)
	INT iR, iG, iB,  iA
	
	IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
		IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) < GB_UI_LEVEL_MINIMAL
		
			#IF IS_DEBUG_BUILD
			IF GET_FRAME_COUNT() % 500 = 0
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN UPDATE_SPHERE EXIT as uiLevel < GB_UI_LEVEL_MINIMAL ")
			ENDIF
			#ENDIF
			EXIT
		ENDIF
	ENDIF
	
	
	IF NOT bIsStart
		IF NOT bNonPart
		OR IS_SPECTATING_WITH_HELI()
			GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
		ELSE
			GET_HUD_COLOUR(HUD_COLOUR_GREY, iR, iG, iB, iA)
		ENDIF
	ELSE
		
		IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
			GET_HUD_COLOUR(hclLaunchGang, iR, iG, iB, iA)
		ELSE
			GET_HUD_COLOUR(HUD_COLOUR_NET_PLAYER2, iR, iG, iB, iA)
		ENDIF
	ENDIF
	
//	iA = 100
//	
//	IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_RemovedFromEventAsPass)
//		DRAW_MARKER( MARKER_SPHERE,
//					vMyBlipPos,
//					<<0,0,0>>,
//					<<0,0,0>>,
//					<<fMyBlipSize, fMyBlipSize, fMyBlipSize>>,
//					iR,
//					iG,
//					iB,
//					iA )
//	ENDIF
	
	FLOAT fA
	fA = 0.50
	VECTOR vCorornaPos = vMyBlipPos
	
	
	
	FLOAT fCoronaSize = fMyBlipSize * 2.0
	
	
	
	FLOAT fCoronaHeight //fMyBlipSize  
	FLOAT fHeightDiff 
	
	SWITCH serverBD.iLoc
		CASE 0
			fCoronaHeight = 36.9 //21.65 //fMyBlipSize  
			fHeightDiff = 7.875
			fCoronaSize = GET_START_CORONA_SIZE_FOR_ROUTE()
			vCorornaPos = GET_START_CORONA_LOCATION_FOR_ROUTE()
		BREAK
		
		CASE 1
			fCoronaHeight =26.25 //9.625 //fMyBlipSize  
			fHeightDiff = 4.375 //4.625
			fCoronaSize = GET_START_CORONA_SIZE_FOR_ROUTE()
			vCorornaPos = GET_START_CORONA_LOCATION_FOR_ROUTE()
		BREAK
		
		CASE 2
		
			fCoronaHeight =49.875 //38.375 //fMyBlipSize  
			fHeightDiff = 12.0
			fCoronaSize = GET_START_CORONA_SIZE_FOR_ROUTE()
			vCorornaPos = GET_START_CORONA_LOCATION_FOR_ROUTE()
		BREAK
		
		CASE 3
			fCoronaHeight =9.5// 4.375 //fMyBlipSize  
			fHeightDiff = 0.0
			fCoronaSize = GET_START_CORONA_SIZE_FOR_ROUTE()
			vCorornaPos = GET_START_CORONA_LOCATION_FOR_ROUTE()
		BREAK
		
		CASE 4
			fCoronaHeight = 38.75 //27.25//fMyBlipSize  
			fHeightDiff = 10.25
			fCoronaSize = GET_START_CORONA_SIZE_FOR_ROUTE()
			vCorornaPos = GET_START_CORONA_LOCATION_FOR_ROUTE()
		BREAK
		
		#IF FEATURE_STUNT_FM_EVENTS
		CASE 5
			fCoronaHeight = 9.5
			fHeightDiff = 3.875
			fCoronaSize = GET_START_CORONA_SIZE_FOR_ROUTE()
			vCorornaPos = GET_START_CORONA_LOCATION_FOR_ROUTE()
		BREAK
		#ENDIF
		
		CASE 6
			fCoronaHeight = 5.25
			fHeightDiff = 0.0
			fCoronaSize = GET_START_CORONA_SIZE_FOR_ROUTE()
			vCorornaPos = GET_START_CORONA_LOCATION_FOR_ROUTE()
		BREAK
		
		CASE 7
			fCoronaHeight = 5.25
			fHeightDiff = 0.875
			fCoronaSize = GET_START_CORONA_SIZE_FOR_ROUTE()
			vCorornaPos = GET_START_CORONA_LOCATION_FOR_ROUTE()
		BREAK
		
		CASE 8
			fCoronaHeight = 15.375
			fHeightDiff = 4.750
			fCoronaSize = GET_START_CORONA_SIZE_FOR_ROUTE()
			vCorornaPos = GET_START_CORONA_LOCATION_FOR_ROUTE()
		BREAK
		
		CASE 9
			fCoronaHeight = 13.625
			fHeightDiff = 4.500
			fCoronaSize = 38.875 //GET_START_CORONA_SIZE_FOR_ROUTE()
			vCorornaPos = GET_START_CORONA_LOCATION_FOR_ROUTE()
		BREAK
		
		CASE 10
			fCoronaHeight = 7.75
			fHeightDiff = 2.0
			fCoronaSize = GET_START_CORONA_SIZE_FOR_ROUTE()
			vCorornaPos = GET_START_CORONA_LOCATION_FOR_ROUTE()
		BREAK
	ENDSWITCH
	
	
	#IF IS_DEBUG_BUILD
	IF bWdDrawStartSPhere
		DRAW_MARKER_SPHERE(vMyBlipPos, 
					fMyBlipSize, iR, iG, iB, fA) 
	ENDIF
	
	IF fWdCoronaHeight > 0.0
		IF fCoronaHeight <> fWdCoronaHeight
			fCoronaHeight = fWdCoronaHeight
		ENDIF
		
		IF fWdCoronaGroundZ > -999.0
			IF fHeightDiff <> fWdCoronaGroundZ
				fHeightDiff = fWdCoronaGroundZ
			ENDIF
		ENDIF
	ENDIF
	
	IF fWdCoronaSize > 0.0
		IF fCoronaSize <> fWdCoronaSize
			fCoronaSize = fWdCoronaSize
		ENDIF
	ENDIF
	
	
	IF fWdCoronaX <> 0.0
		IF vCorornaPos.x <> fWdCoronaX
			vCorornaPos.x = fWdCoronaX
		ENDIF
	ENDIF
	
	IF fWdCoronaY <> 0.0
		IF vCorornaPos.y <> fWdCoronaY
			vCorornaPos.y = fWdCoronaY
		ENDIF
	ENDIF
	
	#ENDIF
	
	vCorornaPos.z -=  fHeightDiff //fWdCoronaGroundZ
	
	IF IS_SPECTATING_WITH_HELI()
		
		fA = (fA/2)
	ENDIF
	
	BOOL bHideForPassenger
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaAsPassenger)
		IF SHOULD_UI_BE_HIDDEN()
			bHideForPassenger = TRUE
		ENDIF
	ENDIF
	IF NOT WAS_LOCAL_PLAYER_PLAYER_REMOVED_FROM_EVENT()
		IF GET_PLAYER_CORONA_STATUS(PLAYER_ID()) = CORONA_STATUS_IDLE
			IF NOT AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE() 
				IF NOT bIsStart
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneWon)
						IF NOT bHideForPassenger
							DRAW_MARKER_SPHERE(vMyBlipPos, 
								fMyBlipSize, iR, iG, iB, fA) 
						ENDIF

					ELSE
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN NOT CALLING DRAW_MARKER_SPHERE as biS_SomeoneWon time ", GET_CLOUD_TIME_AS_INT())
					ENDIF
				ELSE
				
					DRAW_MARKER( MARKER_CYLINDER,
						vCorornaPos,
						<<0,0,0>>,
						<<0,0,0>>,
						<<fCoronaSize, fCoronaSize, fCoronaHeight>>,
						iR,
						iG,
						iB,
						iA )
					
					#IF IS_DEBUG_BUILD
						IF fWdCoronaHeight = 0.0
							fWdCoronaHeight = fCoronaHeight
						ENDIF
						
						IF fWdCoronaGroundZ = -999.0
							IF fWdCoronaGroundZ <> fHeightDiff	
								fWdCoronaGroundZ = fHeightDiff
							ENDIF
						ENDIF
											
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	#IF IS_DEBUG_BUILD
		IF bWdDoMarkerDebug
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN MARKER DEBUG - Pos = ", vMyBlipPos, " Radius = ", fMyBlipSize)
		ENDIF
		
		IF fWdCoronaSize = 0.0
			fWdCoronaSize = fCoronaSize
		ENDIF
		
		IF fWdCoronaX = 0.0
			fWdCoronaX = vCorornaPos.x
		ENDIF
		
		IF fWdCoronaY = 0.0
			fWdCoronaY = vCorornaPos.y
		ENDIF
	#ENDIF
	
ENDPROC

FUNC BOOL IS_LOCAL_PLAYER_IN_AREA(BOOL bIsStart = FALSE)
	FLOAT fMyDistFromCentre
	VECTOR vPlayerPos
	VECTOR vCoronaStart
	FLOAT fWithinPenDist
	
	IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
		IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) < GB_UI_LEVEL_MINIMAL
			#IF IS_DEBUG_BUILD
			IF GET_FRAME_COUNT() % 500 = 0
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN IS_LOCAL_PLAYER_IN_AREA FALSE! uiLevel < GB_UI_LEVEL_MINIMAL")
			ENDIF
			#ENDIF
	
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF bIsStart
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
			vCoronaStart = GET_START_CORONA_LOCATION_FOR_ROUTE()
			fMyDistFromCentre = GET_DISTANCE_BETWEEN_COORDS(vPlayerPos, vCoronaStart, FALSE)
			fWithinPenDist = GET_START_CORONA_SIZE_FOR_ROUTE() / 2.0 
			
			#IF IS_DEBUG_BUILD				
				IF GET_COMMANDLINE_PARAM_EXISTS("SC_PennedInArea")
					PRINTLN("     ----------> [SC_PennedInArea] Checking start. Checking fMyDistFromCentre = ", fMyDistFromCentre, " <= fWithinPenDist = ", fWithinPenDist, " height diff = ", ABSF(vCoronaStart.z - vPlayerPos.z) )
				ENDIF
			#ENDIF
			
			IF fMyDistFromCentre <= fWithinPenDist
				IF ABSF(vCoronaStart.z - vPlayerPos.z) <= fWithinPenDist 
					
					
					#IF IS_DEBUG_BUILD				
						IF GET_COMMANDLINE_PARAM_EXISTS("SC_PennedInArea")
							DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.24, "STRING", "In Area - Start")
						ENDIF
					#ENDIF
					
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF fMyBlipSize < 10.0
				vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
				fMyDistFromCentre = GET_DISTANCE_BETWEEN_COORDS(vPlayerPos, vMyBlipPos, FALSE)
				#IF IS_DEBUG_BUILD
					IF GET_COMMANDLINE_PARAM_EXISTS("SC_PennedInArea")
						PRINTLN("     ----------> [SC_PennedInArea] fMyDistFromCentre = ", fMyDistFromCentre, " fMyBlipSize = ", fMyBlipSize , " vMyBlipPos = ", vMyBlipPos)
					ENDIF
				#ENDIF
				
				IF fMyDistFromCentre <= fMyBlipSize
					IF ABSF(vMyBlipPos.z - vPlayerPos.z) <= 10.0 
						#IF IS_DEBUG_BUILD
							IF GET_COMMANDLINE_PARAM_EXISTS("SC_PennedInArea")
								DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.24, "STRING", "In Area - 2d")
							ENDIF
						#ENDIF
						
						
						RETURN TRUE
					ENDIF
				ENDIF
			ELSE
				fMyDistFromCentre = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vMyBlipPos)
				
				#IF IS_DEBUG_BUILD
					IF GET_COMMANDLINE_PARAM_EXISTS("SC_PennedInArea")
						PRINTLN("     ----------> [SC_PennedInArea] fMyDistFromCentre = ", fMyDistFromCentre, " fMyBlipSize = ", fMyBlipSize , " vMyBlipPos = ", vMyBlipPos)
					ENDIF
				#ENDIF
				
				IF fMyDistFromCentre <= fMyBlipSize
					#IF IS_DEBUG_BUILD
						IF GET_COMMANDLINE_PARAM_EXISTS("SC_PennedInArea")
							DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.24, "STRING", "In Area - 3d")
						ENDIF
					#ENDIF
					
					RETURN TRUE
				ENDIF
				
			ENDIF	
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("SC_PennedInArea")
			DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.24, "STRING", "Out Area")
		ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC



FUNC BOOL VEHICLE_HAS_BOMB(VEHICLE_INDEX& bombVehicle)

	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [REMOVE_BOMBS_FROM_MY_VEH] - Called ")
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [REMOVE_BOMBS_FROM_MY_VEH] - I'm in a veh ")
		bombVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF DECOR_IS_REGISTERED_AS_TYPE("bombdec1",DECOR_TYPE_INT)
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [REMOVE_BOMBS_FROM_MY_VEH] - bombdec1 registered ")
			IF DECOR_EXIST_ON(bombVehicle,"bombdec1")
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [REMOVE_BOMBS_FROM_MY_VEH] - bombdec1 Exists on ")
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF DECOR_IS_REGISTERED_AS_TYPE("bombdec",DECOR_TYPE_INT)
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [REMOVE_BOMBS_FROM_MY_VEH] - bombdec registered")
			IF DECOR_EXIST_ON(bombVehicle,"bombdec")
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [REMOVE_BOMBS_FROM_MY_VEH] - bombdec Exists on ")
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF DECOR_IS_REGISTERED_AS_TYPE("bombowner",DECOR_TYPE_INT)
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [REMOVE_BOMBS_FROM_MY_VEH] -bombowner registered")
			IF DECOR_EXIST_ON(bombVehicle,"bombowner")
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [REMOVE_BOMBS_FROM_MY_VEH] - bombowner Exists on ")
				RETURN TRUE
			ENDIF
		ENDIF
		
//		IF HAS_VEHICLE_PHONE_EXPLOSIVE_DEVICE(vehTemp)
//			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [REMOVE_BOMBS_FROM_MY_VEH] - HAS_VEHICLE_PHONE_EXPLOSIVE_DEVICE")
//			CLEAR_VEHICLE_PHONE_EXPLOSIVE_DEVICE()
//		ENDIF
	ENDIF
	
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [REMOVE_BOMBS_FROM_MY_VEH] - DONE ")
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_VEHICLE_BOMB_REMOVAL()
	VEHICLE_INDEX vehTemp 
	IF VEHICLE_HAS_BOMB(vehTemp)
	AND DECOR_EXIST_ON(vehTemp, "Veh_Modded_By_Player")
		PRINTLN("   ---------> [PENNED IN] - [REMOVEBOMB] - Vehicle modded by decor exists.")
		INT iPlayer
		PLAYER_INDEX tempPlayerID
		REPEAT NUM_NETWORK_PLAYERS iPlayer
			tempPlayerID = INT_TO_PLAYERINDEX(iPlayer)
			IF IS_NET_PLAYER_OK(tempPlayerID, FALSE)
				IF DECOR_GET_INT(vehTemp, "Veh_Modded_By_Player") = NETWORK_HASH_FROM_PLAYER_HANDLE(tempPlayerID)
					PRINTLN("   ---------> [PENNED IN] - [REMOVEBOMB] - Vehicle modded by: ", GET_PLAYER_NAME(tempPlayerID))
					BROADCAST_PENNED_IN_REMOVE_BOMB(SPECIFIC_PLAYER(tempPlayerID))
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

FUNC BOOL IS_VEHICLE_MODEL_VALID_FOR_PENNED_IN(MODEL_NAMES mVeh)
	BOOL bValid = TRUE
	
	IF mVeh = RHINO
	OR mVeh = DUMP
	OR mVeh = LIMO2
	#IF FEATURE_CASINO
	OR mVeh = RCBANDITO
	#ENDIF
	#IF FEATURE_CASINO_HEIST
	OR mVeh = MINITANK
	#ENDIF
		bValid = FALSE
	ENDIF
	
	SWITCH serverBD.iPennedInType
		CASE PENNED_IN_TYPE_ROAD_VEHICLE
		BREAK
		
		#IF FEATURE_STUNT_FM_EVENTS
		CASE PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY
			IF NOT IS_THIS_MODEL_A_HELI(mVeh)
				bValid = FALSE
			ENDIF
		BREAK
		#ENDIF
		
		CASE PENNED_IN_TYPE_CAGED_IN
			IF (NOT IS_THIS_MODEL_A_BIKE(mVeh)
			AND NOT IS_THIS_MODEL_A_QUADBIKE(mVeh)
			AND NOT IS_THIS_MODEL_A_TRIKE(mVeh))
			OR IS_THIS_MODEL_A_BICYCLE(mVeh) 

				bValid = FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	//If the model name is valid
	IF g_FM_EVENT_VARS.mnPennedInBlocked != DUMMY_MODEL_FOR_SCRIPT
	//And it's what we're in then block it
	AND mVeh = g_FM_EVENT_VARS.mnPennedInBlocked
		bValid = FALSE
	ENDIF
	
	RETURN bValid
		
ENDFUNC

FUNC BOOL IS_HEARTBEAT_FRAME()
	RETURN IS_BIT_SET(iBoolsBitSet, biHeartBeat)
ENDFUNC

PROC MANAGE_HEARTBEAT()

	FLOAT fHeartRate
	
	FLOAT fBPM_Base
	fBPM_Base = 80
	FLOAT fBPM_Spike 
	fBPM_Spike = 0
	
	INT iShakeDuration
	iShakeDuration  = 130
	INT iShakeFrequency
	iShakeFrequency = 130
	
	
//	#IF IS_DEBUG_BUILD
//	fBPM_Base 	= tfHeartBase
//	fBPM_Spike 	= tfHeartSpike
//	#ENDIF

	// Overall rate
	fHeartRate = fBPM_Base + fBPM_Spike + GET_RANDOM_INT_IN_RANGE(0, 3)
	
	IF NATIVE_TO_INT(GET_NETWORK_TIME()) - iLastHeartBeatTime >= ROUND(60000/fHeartRate)
		IF NOT IS_BIT_SET(iBoolsBitSet, biHeartBeat)
			SET_BIT(iBoolsBitSet, biHeartBeat)
		ENDIF
		iLastHeartBeatTime = NATIVE_TO_INT(GET_NETWORK_TIME())
	ELSE
		IF IS_BIT_SET(iBoolsBitSet, biHeartBeat)
			CLEAR_BIT(iBoolsBitSet, biHeartBeat)
		ENDIF
	ENDIF
	
	IF IS_HEARTBEAT_FRAME()
		SET_CONTROL_SHAKE(PLAYER_CONTROL, 0, 0)
		SET_CONTROL_SHAKE(PLAYER_CONTROL, iShakeDuration, iShakeFrequency)
	//	PLAY_SOUND_FRONTEND(-1, "FBI_03_TORTURE_Heart_Monitor_Single")
		SET_CONTROL_LIGHT_EFFECT_COLOR(PLAYER_CONTROL, 255, 0, 0)
	ELSE
		SET_CONTROL_LIGHT_EFFECT_COLOR(PLAYER_CONTROL, 255, 255, 255)
	ENDIF
ENDPROC	


PROC MAINTAIN_MY_PENNED_IN_VEHICLE()
	VEHICLE_INDEX vehTemp
	STRING strVehType
	
	
	
	IF NOT IS_BIT_SET(iBoolsBitSet, biSetAsMissionVeh)
		IF serverBD.eStage > eAD_COUNTDOWN
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InArea)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
						vehMyPennedInVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						IF GET_PED_IN_VEHICLE_SEAT(vehMyPennedInVeh) = PLAYER_PED_ID()
							IF NETWORK_HAS_CONTROL_OF_ENTITY(vehMyPennedInVeh)
								IF NOT IS_VEHICLE_A_PERSONAL_VEHICLE(vehMyPennedInVeh)
								//And it's not a Pegasus
								AND NOT IS_VEHICLE_MY_PEGASUS(vehMyPennedInVeh)
								AND NOT IS_VEHICLE_MY_TRUCK(vehMyPennedInVeh)
								AND NOT GB_IS_THIS_VEHICLE_A_GANG_BOSS_LIMO(vehMyPennedInVeh)
								AND NOT IS_VEHICLE_MY_AVENGER(vehMyPennedInVeh)
								AND NOT IS_VEHICLE_MY_HACKERTRUCK(vehMyPennedInVeh)
								#IF FEATURE_DLC_2_2022
								AND NOT IS_VEHICLE_MY_ACIDLAB(vehMyPennedInVeh)
								#ENDIF
									IF IS_VEHICLE_MODEL_VALID_FOR_PENNED_IN(GET_ENTITY_MODEL(vehMyPennedInVeh))
										
										SET_ENTITY_AS_MISSION_ENTITY(vehMyPennedInVeh, FALSE, TRUE)
										SET_BIT(iBoolsBitSet, biSetAsMissionVeh)
									//	SET_ENTITY_INVINCIBLE(vehMyPennedInVeh, TRUE)
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Set veh invincible - 2")
										INT j
										REPEAT NUM_NETWORK_PLAYERS j
											SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(vehMyPennedInVeh, FALSE, rgFM_Team[j])
										ENDREPEAT
								
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [MAINTAIN_MY_PENNED_IN_VEHICLE] SET biSetAsMissionVeh")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	BOOL bGotVehObjText
	IF DOES_ENTITY_EXIST(vehMyPennedInVeh)
		IF NOT IS_VEHICLE_A_PERSONAL_VEHICLE(vehMyPennedInVeh)
		AND NOT GB_IS_THIS_VEHICLE_A_GANG_BOSS_LIMO(vehMyPennedInVeh)
			IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(vehMyPennedInVeh))
				strVehType = "PEN_BIKE"
			ELSE
				strVehType = "PEN_VEHG"
			ENDIF
			bGotVehObjText = TRUE
		ELSE	
			
			IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(vehMyPennedInVeh))
				strVehType = "PEN_PBIKE"
			ELSE
				strVehType = "PEN_PVEHG"
			ENDIF
			
			bGotVehObjText = TRUE
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehMyPennedInVeh)
		IF IS_VEHICLE_A_PERSONAL_VEHICLE(vehMyPennedInVeh)
		OR GB_IS_THIS_VEHICLE_A_GANG_BOSS_LIMO(vehMyPennedInVeh)
			IF serverBD.eStage = eAD_IN_AREA
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_FailedToStayInArea)
					IF IS_VEHICLE_DRIVEABLE(vehMyPennedInVeh)
						IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_OutCoronaTimeStarted)
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehMyPennedInVeh)
									IF NOT Is_This_The_Current_Objective_Text(strVehType)	
										Print_Objective_Text(strVehType)
									ENDIF
								ELSE
									IF Is_This_The_Current_Objective_Text(strVehType)
										Clear_Any_Objective_Text_From_This_Script()
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF Is_This_The_Current_Objective_Text(strVehType)
								Clear_Any_Objective_Text_From_This_Script()
							ENDIF
						ENDIF	
					ELSE
						IF bGotVehObjText
							IF Is_This_The_Current_Objective_Text(strVehType)
								Clear_Any_Objective_Text_From_This_Script()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iBoolsBitSet, biSetAsMissionVeh)
		IF serverBD.eStage = eAD_IN_AREA
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_FailedToStayInArea)
				IF IS_VEHICLE_DRIVEABLE(vehMyPennedInVeh)
					IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_OutCoronaTimeStarted)
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehMyPennedInVeh)
								IF NOT DOES_BLIP_EXIST(blipVeh)
								
									blipVeh = ADD_BLIP_FOR_ENTITY(vehMyPennedInVeh)
									SET_BLIP_AS_FRIENDLY(blipVeh, TRUE)
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [MAINTAIN_MY_PENNED_IN_VEHICLE] ADDED blipVeh")
								ENDIF
								
								
								IF NOT Is_This_The_Current_Objective_Text(strVehType)
									Print_Objective_Text(strVehType)
								ENDIF
							ELSE
								IF IS_VEHICLE_MODEL_VALID_FOR_PENNED_IN(GET_ENTITY_MODEL(vehMyPennedInVeh))
									IF DOES_BLIP_EXIST(blipVeh)
										REMOVE_BLIP(blipVeh)
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [MAINTAIN_MY_PENNED_IN_VEHICLE] REMOVED blipVeh")
									ENDIF
									
									IF bGotVehObjText
										IF Is_This_The_Current_Objective_Text(strVehType)
											Clear_Any_Objective_Text_From_This_Script()
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF bGotVehObjText
							IF Is_This_The_Current_Objective_Text(strVehType)
								Clear_Any_Objective_Text_From_This_Script()
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(blipVeh)
						REMOVE_BLIP(blipVeh)
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [MAINTAIN_MY_PENNED_IN_VEHICLE] REMOVED blipVeh")
					ENDIF
					
					IF bGotVehObjText
						IF Is_This_The_Current_Objective_Text(strVehType)
							Clear_Any_Objective_Text_From_This_Script()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_BLIP_EXIST(blipVeh)
	OR NOT IS_VEHICLE_DRIVEABLE(vehMyPennedInVeh)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
				vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF IS_VEHICLE_MODEL_VALID_FOR_PENNED_IN(GET_ENTITY_MODEL(vehTemp))
					IF vehTemp <> vehMyPennedInVeh
						IF GET_PED_IN_VEHICLE_SEAT(vehTemp) = PLAYER_PED_ID()
							IF NETWORK_HAS_CONTROL_OF_ENTITY(vehTemp)
								vehMyPennedInVeh = vehTemp
								IF bGotVehObjText
									IF Is_This_The_Current_Objective_Text(strVehType)
										Clear_Any_Objective_Text_From_This_Script()
									ENDIF
								ENDIF
								REMOVE_BLIP(blipVeh)
								IF NOT IS_VEHICLE_A_PERSONAL_VEHICLE(vehMyPennedInVeh)
									SET_ENTITY_AS_MISSION_ENTITY(vehMyPennedInVeh, FALSE, TRUE)
								ENDIF
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [MAINTAIN_MY_PENNED_IN_VEHICLE] UPDATED vehMyPennedInVeh AS IN A NEW VEHICLE")
							ENDIF
						ELSE
							IF bGotVehObjText
								IF Is_This_The_Current_Objective_Text(strVehType)
									Clear_Any_Objective_Text_From_This_Script()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_VEHICLE_DRIVEABLE(vehMyPennedInVeh)
				//	SET_ENTITY_INVINCIBLE(vehMyPennedInVeh, FALSE)
			//		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Cleared veh invincible - 6")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	//-- Invalid vehicle check, after event has started
	IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_CheckForInvalidVeh)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				SET_BIT(iBoolsBitSet2, bi2_CheckForInvalidVeh)
				CLEAR_BIT(iBoolsBitSet2, bi2_VehInvalid)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [MAINTAIN_MY_PENNED_IN_VEHICLE] SET bi2_CheckForInvalidVeh")
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) 
				IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_VehInvalid)
					vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF IS_VEHICLE_MODEL_VALID_FOR_PENNED_IN(GET_ENTITY_MODEL(vehTemp))
						CLEAR_BIT(iBoolsBitSet2, bi2_CheckForInvalidVeh)
						CLEAR_BIT(iBoolsBitSet2, bi2_VehInvalid)
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [MAINTAIN_MY_PENNED_IN_VEHICLE] CLEAR bi2_CheckForInvalidVeh")
					ELSE 
						SET_BIT(iBoolsBitSet2, bi2_VehInvalid)
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [MAINTAIN_MY_PENNED_IN_VEHICLE] SET bi2_VehInvalid")
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(iBoolsBitSet2, bi2_VehInvalid)
					CLEAR_BIT(iBoolsBitSet2, bi2_VehInvalid)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [MAINTAIN_MY_PENNED_IN_VEHICLE] CLEAR bi2_VehInvalid")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	VEHICLE_INDEX vehPass
	
	//-- Deal with passengers ending up in driver seat during event
	IF playerBD[PARTICIPANT_ID_TO_INT()].eStage	= eAD_IN_AREA
		IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_RemovedFromEventAsPass)
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaAsPassenger)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						vehPass = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						IF GET_PED_IN_VEHICLE_SEAT(vehPass) = PLAYER_PED_ID()
							SET_BIT(iBoolsBitSet2, bi2_RemovedFromEventAsPass)
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [MAINTAIN_MY_PENNED_IN_VEHICLE] set bi2_RemovedFromEventAsPass")
							Clear_Any_Objective_Text_From_This_Script()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//-- ...and with drivers who end up as passengers
	IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_RemovedFromEventAsDri)
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_inArea)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					vehPass = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF GET_PED_IN_VEHICLE_SEAT(vehPass) <> PLAYER_PED_ID()
					AND GET_PED_IN_VEHICLE_SEAT(vehPass) <> NULL
						CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_inArea)
						SET_BIT(iBoolsBitSet2, bi2_RemovedFromEventAsDri)
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [MAINTAIN_MY_PENNED_IN_VEHICLE] I WAS A DRIVER, NOW A PASSENGER SET bi2_RemovedFromEventAsDri")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC



PROC KNOCK_PLAYER_OUT_PENNED_IN()
	PRINTLN("     ---------->     PENNED IN - [KNOCK_PLAYER_OUT_PENNED_IN] CALLED")

	VEHICLE_INDEX vehTemp
	
	
	// EXIT
	
	
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_FailedToStayInArea)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iBoolsBitSet, biSetPlayerInvincible)
		CLEAR_BIT(iBoolsBitSet, biSetPlayerInvincible)
	//	SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - CLEAR biSetPlayerInvincible - 4")
	ENDIF
	
	
	
											
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableVehicleCombat, FALSE)
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableVehicleCombat, FALSE) 8")
	
	#IF FEATURE_STUNT_FM_EVENTS
	IF serverBD.iPennedInType = PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY
		SET_LOCAL_PLAYER_AS_GHOST(FALSE)
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN SET_LOCAL_PLAYER_AS_GHOST(FALSE) 1")
	ENDIF
	#ENDIF
											
	CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_SHouldNotBeDamageable)
	
//	SET_CONTROL_LIGHT_EFFECT_COLOR(PLAYER_CONTROL, 255, 255, 255)
	CLEAR_CONTROL_LIGHT_EFFECT(PLAYER_CONTROL)
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaAsPassenger)
		
		INIT_MY_PARTICIPATION_TIME_MULTIPLIER()
		
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_FailedToStayInArea)
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - SET biP_FailedToStayInArea TIME ", GET_CLOUD_TIME_AS_INT())
		
		TickerData.TickerEvent = TICKER_EVENT_KNOCKED_OUT_PENNED_IN
		TickerData.playerID = PLAYER_ID()
		BROADCAST_TICKER_EVENT(TickerData,ALL_PLAYERS_ON_SCRIPT(TRUE))
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PlayersDontDragMeOutOfCar, FALSE)
		ENDIF
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF NETWORK_HAS_CONTROL_OF_ENTITY(vehTemp)
				//	SET_ENTITY_INVINCIBLE(vehMyLastVeh, FALSE)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Cleared veh invincible - 3")
				ENDIF

				IF NOT IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(vehTemp))
					vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(PLAYER_PED_ID())
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(vehTemp)
					
					IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
						//-- 3059272 - Want to disable friendly fire in caged in just before the player explodes, so that they
						//-- don't take out players who are still in.
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->   NETWORK_SET_FRIENDLY_FIRE_OPTION(FALSE)")
						SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
						SET_ENTITY_PROOFS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), FALSE, FALSE, FALSE, FALSE, FALSE)
						CLEAR_BIT(iBoolsBitSet3, bi3_ExpProof)
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->   SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE) clear bi3_ExpProof")
						NETWORK_SET_FRIENDLY_FIRE_OPTION(FALSE)
						
					ELSE
						SET_PLAYER_DEFAULT_FRIENDLY_FIRE_OPTION()
					ENDIF
					
					IF NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehTemp))
						NETWORK_EXPLODE_VEHICLE(vehTemp)
					ELSE
						NETWORK_EXPLODE_HELI(vehTemp)
					ENDIF
					
					// Mark player as knocked out of penned in area
					SET_PLAYER_HAS_BEEN_KNOCKED_OUT_OF_MODE(TRUE)
					SET_BIT(iBoolsBitSet, biDestWhenKnockedOut)
				ENDIF
				
			ELSE
				IF IS_BIT_SET(iBoolsBitSet3, bi3_ExpProof)
					SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
					CLEAR_BIT(iBoolsBitSet3, bi3_ExpProof)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->   SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE) clear bi3_ExpProof 4")
				ENDIF
						
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(PLAYER_PED_ID())
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - CLEAR_ENTITY_LAST_DAMAGE_ENTITY(PLAYER_PED_ID())")
				SET_ENTITY_HEALTH(PLAYER_PED_ID(), 0)
				SET_PLAYER_HAS_BEEN_KNOCKED_OUT_OF_MODE(TRUE)
				SET_BIT(iBoolsBitSet, biDestWhenKnockedOut)
				
				
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Killed local player")
			ENDIF
		ELSE

			SET_PLAYER_HAS_BEEN_KNOCKED_OUT_OF_MODE(TRUE)
			SET_BIT(iBoolsBitSet2, bi2_KnockedOutAsDead)
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Knocked out as died")
		ENDIF
		
		iMyTimeInEvent = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(timeMyParticpation)
	ENDIF
	
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_OutCoronaTimeStarted)
		CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_OutCoronaTimeStarted)
		STOP_SOUND(iOutAreaSOund)
		RELEASE_SOUND_ID(iOutAreaSOund)
		CLEAR_BIT(iBoolsBitSet2, bi2_OutOfAreaCountdown)
	ENDIF
	
	SET_BIT(iBoolsBitSet, biDoneExplodeVehicle)
	
	
	UNUSED_PARAMETER(TickerData)
	UNUSED_PARAMETER(vehTemp)
ENDPROC

PROC MAINTAIN_LOCAL_PLAYER_IN_AREA()
	//VEHICLE_INDEX vehTemp
	
//	DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.14, "STRING", "Not Rejected")
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneWon)
	OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaAtEndOfROute)
		IF IS_BIT_SET(iBoolsBitSet, biDoneTimeCycle)
			IF IS_AUDIO_SCENE_ACTIVE("MP_Player_Inside_Bubble_Scene")
				STOP_AUDIO_SCENE("MP_Player_Inside_Bubble_Scene")
			ENDIF
			
			IF IS_AUDIO_SCENE_ACTIVE("MP_Player_Outside_Bubble_Scene")
				STOP_AUDIO_SCENE("MP_Player_Outside_Bubble_Scene")
			ENDIF
			
			
			
			ANIMPOSTFX_STOP("pennedIn")
			ANIMPOSTFX_PLAY("PennedInOut", 0, FALSE)
			CLEAR_BIT(iBoolsBitSet, biDoneTimeCycle)
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - CLEAR_TIMECYCLE_MODIFIER() - someone won")
		ENDIF
		
		IF Is_This_The_Current_Objective_Text("PEN_BAREA")
			Clear_Any_Objective_Text_From_This_Script()
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - clear out-area text - someone won")
		ENDIF
		
		IF Is_This_The_Current_Objective_Text("PEN_BAREA")
			Clear_Any_Objective_Text_From_This_Script()
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - clear out-area text - someone won")
		ENDIF
		
		IF Is_This_The_Current_Objective_Text("PEN_AREA")
			Clear_Any_Objective_Text_From_This_Script()
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - clear stay in area text - someone won")
		ENDIF
		

		EXIT
	ENDIF
	
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaAsPassenger)
		IF SHOULD_UI_BE_HIDDEN()
			IF IS_BIT_SET(iBoolsBitSet, biDoneTimeCycle)
				IF IS_AUDIO_SCENE_ACTIVE("MP_Player_Inside_Bubble_Scene")
					STOP_AUDIO_SCENE("MP_Player_Inside_Bubble_Scene")
				ENDIF
				
				IF IS_AUDIO_SCENE_ACTIVE("MP_Player_Outside_Bubble_Scene")
					STOP_AUDIO_SCENE("MP_Player_Outside_Bubble_Scene")
				ENDIF
				
				
				
				ANIMPOSTFX_STOP("pennedIn")
				ANIMPOSTFX_PLAY("PennedInOut", 0, FALSE)
				CLEAR_BIT(iBoolsBitSet, biDoneTimeCycle)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - CLEAR_TIMECYCLE_MODIFIER() - Passenger & Should be hidden")
			ENDIF
			
			IF Is_This_The_Current_Objective_Text("PEN_BAREA")
				Clear_Any_Objective_Text_From_This_Script()
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - clear out-area text - Passenger & Should be hidden")
			ENDIF
			
			IF Is_This_The_Current_Objective_Text("PEN_BAREA")
				Clear_Any_Objective_Text_From_This_Script()
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - clear out-area text - Passenger & Should be hidden")
			ENDIF
			
			IF Is_This_The_Current_Objective_Text("PEN_AREA")
				Clear_Any_Objective_Text_From_This_Script()
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - clear stay in area text - Passenger & Should be hidden")
			ENDIF
			
		//	CLEAR_HELP()
			
			REMOVE_AREA_BLIP()
			
			EXIT
		ENDIF
	ENDIF
	
	STRING sHelp
	IF IS_LOCAL_PLAYER_IN_AREA()
	AND NOT IS_BIT_SET(iBoolsBitSet2, bi2_VehInvalid)
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaForAudioScene)
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaForAudioScene)
		ENDIF
		
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaAtEndOfROute)
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_ReachedEndOfRoute)
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaAtEndOfROute)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [MAINTAIN_LOCAL_PLAYER_IN_AREA] SET biP_InAreaAtEndOfROute AS biS_ReachedEndOfRoute TIME ", GET_CLOUD_TIME_AS_INT())
			ELSE
				IF IS_BIT_SET(iBoolsBitSet2, bi2_localReachedEndOfRoute)
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaAtEndOfROute)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [MAINTAIN_LOCAL_PLAYER_IN_AREA] SET biP_InAreaAtEndOfROute AS biP_InAreaAtEndOfROute TIME ", GET_CLOUD_TIME_AS_INT())
				ENDIF
			ENDIF
		ENDIF
		
		
		IF HAS_NET_TIMER_STARTED(OutOfAreaTimer)
			RESET_NET_TIMER(OutOfAreaTimer)
			SET_CONTROL_LIGHT_EFFECT_COLOR(PLAYER_CONTROL, 255, 255, 255)
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - RESET OUT OF AREA TIMER")
		ENDIF
		
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_OutCoronaTimeStarted)
			CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_OutCoronaTimeStarted)
			PLAY_SOUND_FRONTEND(-1, "OOB_Cancel", "GTAO_FM_Events_Soundset", FALSE)
			
			STOP_SOUND(iOutAreaSOund)
			RELEASE_SOUND_ID(iOutAreaSOund)
			CLEAR_BIT(iBoolsBitSet2, bi2_OutOfAreaCountdown)
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Cleared biP_OutCOronaTimeStarte time ", GET_CLOUD_TIME_AS_INT())
		ENDIF
		
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaAsPassenger)
			IF NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_START_OF_JOB)
				IF DOES_ENTITY_EXIST(vehMyPennedInVeh)
				AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				AND IS_VEHICLE_DRIVEABLE(vehMyPennedInVeh)
					IF Is_This_The_Current_Objective_Text("PEN_AREA") // Stay in the marked ~y~area.
						Clear_Any_Objective_Text_From_This_Script()
					ENDIF
				ELSE
					IF NOT DOES_BLIP_EXIST(blipVeh)
						IF NOT Is_This_The_Current_Objective_Text("PEN_AREA")
							Print_Objective_Text("PEN_AREA") // Stay in the marked ~y~area.
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - TOLD TO STAY IN AREA")
						ENDIF
					ELSE
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - NOT BEING TOLD TO STAY IN AREA AS blipVeh exists")
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT Is_This_The_Current_Objective_Text("PEN_AREA")
				Print_Objective_Text("PEN_AREA") // Stay in the marked ~y~area.
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - TOLD TO STAY IN AREA as I'm a passenger in the area")
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(iBoolsBitSet, biDoneTimeCycle)
			IF IS_AUDIO_SCENE_ACTIVE("MP_Player_Outside_Bubble_Scene")
				STOP_AUDIO_SCENE("MP_Player_Outside_Bubble_Scene")
			ENDIF
			
			START_AUDIO_SCENE("MP_Player_Inside_Bubble_Scene")
			
			ANIMPOSTFX_PLAY("pennedIn", 0, TRUE)
			SET_BIT(iBoolsBitSet, biDoneTimeCycle)
		ENDIF
		
//		IF NOT IS_PLAYER_IN_PENNED_IN_AREA()
//			SET_PLAYER_IN_PENNED_IN_AREA(TRUE)
//		ENDIF

	ELSE
		
		#IF IS_DEBUG_BUILD
			IF IS_LOCAL_PLAYER_IN_AREA()
				IF IS_BIT_SET(iBoolsBitSet2, bi2_VehInvalid)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [MAINTAIN_LOCAL_PLAYER_IN_AREA] PLAYER IN AREA BUT STILL KNOCKING OUT! bi2_VehInvalid IS SET")
				ENDIF
				
				IF IS_PED_INJURED(PLAYER_PED_ID())
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [MAINTAIN_LOCAL_PLAYER_IN_AREA] PLAYER IN AREA BUT STILL KNOCKING OUT! PLAYER PED IS INJURED")
				ENDIF
			ENDIF
		#ENDIF

			
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaForAudioScene)
			CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaForAudioScene)
		ENDIF
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			MANAGE_HEARTBEAT()
			
			IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_VehInvalid)
				IF NOT Is_This_The_Current_Objective_Text("PEN_BAREA") //Get back in the ~y~area.
					Print_Objective_Text("PEN_BAREA")
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - TOLD TO GET_BACK IN AREA")
				ENDIF
			ELSE
				IF NOT Is_This_The_Current_Objective_Text("PEN_EXITV") //Exit the vehicle.
					Print_Objective_Text("PEN_EXITV")
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - TOLD TO EXIT THE VEHICLE")
				ENDIF
				
				IF NOT IS_BIT_SET(iBoolsBitSet, biValidVehHelp)
					IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
						sHelp = GET_PENNED_IN_INVALID_VEHICLE_HELP_TEXT()
						PRINT_HELP_NO_SOUND(sHelp, DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
						IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
							SET_FREEMODE_EVENT_HELP_BACKGROUND()
						ENDIF
							
						SET_BIT(iBoolsBitSet, biValidVehHelp)
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Set biValidVehHelp")
					ELSE
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - IS_OK_TO_PRINT_FREEMODE_HELP fail VALID VEH 2")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(iBoolsBitSet, biDoneTimeCycle)
			IF IS_AUDIO_SCENE_ACTIVE("MP_Player_Inside_Bubble_Scene")
				STOP_AUDIO_SCENE("MP_Player_Inside_Bubble_Scene")
			ENDIF
			START_AUDIO_SCENE("MP_Player_Outside_Bubble_Scene")
			
			ANIMPOSTFX_STOP("pennedIn")
			ANIMPOSTFX_PLAY("PennedInOut", 0, FALSE)
			CLEAR_BIT(iBoolsBitSet, biDoneTimeCycle)
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - CLEAR_TIMECYCLE_MODIFIER() - out area")
		ENDIF
		
		IF NOT HAS_NET_TIMER_STARTED(OutOfAreaTimer)
			START_NET_TIMER(OutOfAreaTimer)

			
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - START OUT OF AREA TIMER")
		ENDIF
		
		IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_OutOfAreaCountdown)
			SET_BIT(iBoolsBitSet2, bi2_OutOfAreaCountdown)
			PLAY_SOUND_FRONTEND(-1, "OOB_Start", "GTAO_FM_Events_Soundset", FALSE)
			
			iOutAreaSOund = GET_SOUND_ID()
			PLAY_SOUND_FRONTEND(iOutAreaSOund, "OOB_Timer_Dynamic", "GTAO_FM_Events_Soundset", FALSE)
			SET_VARIABLE_ON_SOUND(iOutAreaSOund, "Time", (TO_FLOAT(GET_KNOCKED_OUT_TIME()))/1000.0)
			
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - START OUT OF AREA SOUND")
		ENDIF
		
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_OutCoronaTimeStarted)
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_OutCoronaTimeStarted)
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - set biP_OutCoronaTimeStarted time ", GET_CLOUD_TIME_AS_INT())
		ENDIF
			
		//IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_FailedToStayInArea)
		IF NOT IS_BIT_SET(iBoolsBitSet, biDoneExplodeVehicle)
			IF HAS_NET_TIMER_STARTED(OutOfAreaTimer)
			//	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ReachedEndOfRoute)
					IF (GET_KNOCKED_OUT_TIME()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(OutOfAreaTimer)) >= 0
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							DRAW_GENERIC_TIMER((GET_KNOCKED_OUT_TIME()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(OutOfAreaTimer)), "PEN_ELM", 0, TIMER_STYLE_USEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_DONTCARE, DEFAULT, HUD_COLOUR_RED, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_RED)			
						ELSE
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - KNOCKING OUT AS PLAYER_INJURED")
							KNOCK_PLAYER_OUT_PENNED_IN()
						ENDIF
					ELSE
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - KNOCKING OUT AS TIMER EXPIRED")
						KNOCK_PLAYER_OUT_PENNED_IN()
					ENDIF
			//	ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	MAINTAIN_STOP_PLAYER_LEAVING_VEHICLE()
	
	#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_weaponWheel")
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ----------> [sc_weaponWheel] calling STOP_PLAYER_SHOOTING_THIS_FRAME - MAINTAIN_LOCAL_PLAYER_IN_AREA")
		ENDIF
	#ENDIF
	
	STOP_PLAYER_SHOOTING_THIS_FRAME()
ENDPROC



PROC FILL_LEADERBOARD_DATA(INT iParticipant, INT iPlayer, BOOL bStillIn, BOOL bTookPart)
	
	INT iLocalStillIn, iLocalTookPart
	
	IF bStillIn 	iLocalStillIn = 1 	ENDIF
	IF bTookPart	iLocalTookPart = 1	ENDIF
	
	
	serverBD.sLeaderboardData[iParticipant].iParticipant = iParticipant
	serverBD.sLeaderboardData[iParticipant].iPlayer = iPlayer
	serverBD.sLeaderboardData[iParticipant].iStillInvolved = iLocalStillIn
	serverBD.sLeaderboardData[iParticipant].iWasInvolved = iLocalTookPart
	
ENDPROC

PROC CLEAR_LEADERBOARD_DATA(INT iParticipant)
	
	serverBD.sLeaderboardData[iParticipant].iParticipant = (-1)
	serverBD.sLeaderboardData[iParticipant].iPlayer = (-1)
	serverBD.sLeaderboardData[iParticipant].iStillInvolved = -1
	serverBD.sLeaderboardData[iParticipant].iWasInvolved = -1
	
ENDPROC

PROC OUTPUT_PENNED_IN_LB_DATA()
	INT i
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     MTA - OUTPUT_PENNED_IN_LB_DATA ...    ")
	REPEAT NUM_NETWORK_PLAYERS i
		PLAYER_INDEX player 
		IF serverBD.sLeaderboardData[i].iPlayer <> -1
			player = INT_TO_PLAYERINDEX(serverBD.sLeaderboardData[i].iPlayer)
			IF player <> INVALID_PLAYER_INDEX()
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN -     serverBD.sLeaderboardData[", i, "].iParticipant = ", serverBD.sLeaderboardData[i].iParticipant, " player = ", GET_PLAYER_NAME(player), " .iStillInvolved = ", serverBD.sLeaderboardData[i].iStillInvolved, " .iWasInvolved = ", serverBD.sLeaderboardData[i].iWasInvolved)
			ELSE
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN -     serverBD.sLeaderboardData[", i, "].iParticipant = ", serverBD.sLeaderboardData[i].iParticipant, " PLAYER IS INVALID iPlayer = ",serverBD.sLeaderboardData[i].iPlayer, " .iStillInvolved = ", serverBD.sLeaderboardData[i].iStillInvolved, " .iWasInvolved = ", serverBD.sLeaderboardData[i].iWasInvolved)
			ENDIF
		ENDIF
		
	ENDREPEAT
ENDPROC

PROC SORT_LEADERBOARD_DATA()
	
	INT iCount, iCount2
	
	FOR iCount = (NUM_NETWORK_PLAYERS - 1) TO 0 STEP -1
		
		#IF IS_DEBUG_BUILD
		IF bWdDoLeaderboardDebug
			PRINTLN("")
			PRINTLN("")
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN -  - SORT_LEADERBOARD_DATA *********** PARTICIPANT ", iCount, " ***********")
		ENDIF
		#ENDIF
		
		FOR iCount2 = (NUM_NETWORK_PLAYERS - 1) TO 0 STEP -1
			
			IF ( (iCount2 - 1) >= 0 )
			
				#IF IS_DEBUG_BUILD
				
				INT iCount3 = iCount2 - 1
				
				IF bWdDoLeaderboardDebug
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN -  - SORT_LEADERBOARD_DATA iCount2 = ", iCount2, ", iCount2-1 = ", iCount3)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN -  - SORT_LEADERBOARD_DATA iCount2 iStillInvolved = ", serverBD.sLeaderboardData[iCount2].iStillInvolved, ", iCount2-1 iStillInvolved = ", serverBD.sLeaderboardData[iCount2-1].iStillInvolved)
				ENDIF
				
				#ENDIF
				
				IF serverBD.sLeaderboardData[iCount2].iStillInvolved > serverBD.sLeaderboardData[iCount2-1].iStillInvolved
					
					#IF IS_DEBUG_BUILD
					IF bWdDoLeaderboardDebug
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN -  - SORT_LEADERBOARD_DATA iCount2 time > than iCount2-1 time - doing swap!")
					ENDIF
					#ENDIF
					
					INT iTempTime = serverBD.sLeaderboardData[iCount2].iStillInvolved
					INT iTempPartId = serverBD.sLeaderboardData[iCount2].iParticipant
					INT iTempPlayerId = serverBD.sLeaderboardData[iCount2].iPlayer
					INT iTempTookPart = serverBD.sLeaderboardData[iCount2].iWasInvolved
					
					serverBD.sLeaderboardData[iCount2].iStillInvolved = serverBD.sLeaderboardData[iCount2-1].iStillInvolved
					serverBD.sLeaderboardData[iCount2].iParticipant = serverBD.sLeaderboardData[iCount2-1].iParticipant
					serverBD.sLeaderboardData[iCount2].iPlayer = serverBD.sLeaderboardData[iCount2-1].iPlayer
					serverBD.sLeaderboardData[iCount2].iWasInvolved = serverBD.sLeaderboardData[iCount2-1].iWasInvolved
					
					serverBD.sLeaderboardData[iCount2-1].iStillInvolved = iTempTime
					serverBD.sLeaderboardData[iCount2-1].iWasInvolved = iTempTookPart
					serverBD.sLeaderboardData[iCount2-1].iParticipant = iTempPartId
					serverBD.sLeaderboardData[iCount2-1].iPlayer = iTempPlayerId
					
					#IF IS_DEBUG_BUILD
					IF bWdDoLeaderboardDebug
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN -  - SORT_LEADERBOARD_DATA swapped. iCount2 iStillInvolved = ", serverBD.sLeaderboardData[iCount2].iStillInvolved, ", iCount2-1 iStillInvolved = ", serverBD.sLeaderboardData[iCount2-1].iStillInvolved)
					ENDIF
					#ENDIF
					
				ENDIF
				
			ENDIF
			
		ENDFOR
		
	ENDFOR
	
ENDPROC

PROC UPDATE_PENNED_IN_LEADERBOARD()
	INT i
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			
			CLEAR_LEADERBOARD_DATA(i)
			
			
			BOOL bTookPart = IS_BIT_SET(playerBD[i].iPlayerBitSet, biP_InArea)
			
			BOOL bStillInvolved = TRUE 
			
			IF NOT bTookPart
				bStillInvolved = FALSE
			ENDIF
			
			IF IS_BIT_SET(playerBD[i].iPlayerBitSet, biP_FailedToStayInArea)
				bStillInvolved = FALSE
			ENDIF
			
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_ReachedEndOfRoute)
				IF IS_BIT_SET(playerBD[i].iPlayerBitSet, biP_OutCoronaTimeStarted)
					bStillInvolved = FALSE
				ENDIF
			ENDIF
			
			
			
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ------m---->     PENNED IN - [UPDATE_PENNED_IN_LEADERBOARD] Call FILL_LEADERBOARD_DATA with part = ", i," player = ", GET_PLAYER_NAME(PlayerId), " bStillInvolved = ", bStillInvolved, " bTookPart = ", bTookPart )
			
			FILL_LEADERBOARD_DATA(i, NATIVE_TO_INT(PlayerId), bStillInvolved, bTookPart)
		ELSE
			IF serverBD.sLeaderboardData[i].iStillInvolved <> -1
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [UPDATE_PENNED_IN_LEADERBOARD] Call CLEAR_LEADERBOARD_DATA with part = ", i, " as no longer active")
				CLEAR_LEADERBOARD_DATA(i)
			ENDIF
		ENDIF
	ENDREPEAT
	
	SORT_LEADERBOARD_DATA()
ENDPROC

PROC MAINTAIN_DPAD_DOWN_LEADERBOARD(BOOL bForce = FALSE)
	#IF IS_DEBUG_BUILD
		PLAYER_INDEX playerTemp
		
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_PennedInLB")
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [sc_PennedInLB] ")
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [sc_PennedInLB] Checking LB...")
		ENDIF
	#ENDIF
	
	
	
	INT iCount
	
	//CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN MAINTAIN_DPAD_DOWN_LEADERBOARD") 
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iCount
		
		g_DpadDownLeaderBoardData[iCount].iParticipant = -1
		g_DpadDownLeaderBoardData[iCount].playerID = INVALID_PLAYER_INDEX()
		g_DpadDownLeaderBoardData[iCount].iScore = -1
		g_DpadDownLeaderBoardData[iCount].iFinishTime = -1
		
		sDPadVars.sDpadRowString = "FM_AE_SORT_12"
		   
		#IF IS_DEBUG_BUILD
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_PennedInLB")
				IF serverBD.sLeaderboardData[iCount].iPlayer > -1
					playerTemp = INT_TO_PLAYERINDEX(serverBD.sLeaderboardData[iCount].iPlayer)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [sc_PennedInLB] Checking sLeaderboardData[",iCount, "] who is player ", GET_PLAYER_NAME(playerTemp), " iParticipant = ", serverBD.sLeaderboardData[iCount].iParticipant, " iWasInvolved = ", serverBD.sLeaderboardData[iCount].iWasInvolved, " iStillInvolved = ", serverBD.sLeaderboardData[iCount].iStillInvolved ) 
				ENDIF
			ENDIF
		#ENDIF
		
		IF serverBD.sLeaderboardData[iCount].iParticipant > (-1)
			IF serverBD.sLeaderboardData[iCount].iWasInvolved > 0
				#IF IS_DEBUG_BUILD
					IF GET_COMMANDLINE_PARAM_EXISTS("sc_PennedInLB")
						playerTemp = INT_TO_PLAYERINDEX(serverBD.sLeaderboardData[iCount].iPlayer)
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [sc_PennedInLB] Adding player ", GET_PLAYER_NAME(playerTemp))
					ENDIF
				#ENDIF
				
				g_DpadDownLeaderBoardData[iCount].iParticipant = serverBD.sLeaderboardData[iCount].iParticipant
				g_DpadDownLeaderBoardData[iCount].playerID = INT_TO_NATIVE(PLAYER_INDEX, serverBD.sLeaderboardData[iCount].iPlayer)
				g_DpadDownLeaderBoardData[iCount].iScore = -1
				
				g_DpadDownLeaderBoardData[iCount].iRowStatus = serverBD.sLeaderboardData[iCount].iStillInvolved
				 
			ENDIF
		ENDIF
		
	ENDREPEAT
	
	IF bForce
		IF NOT SHOULD_FORCE_DRAW_CHALLENGE_DPAD_LBD()
			IF sDPadVars.iCachedEventType != FMMC_TYPE_PENNED_IN
				sDPadVars.iCachedEventType = FMMC_TYPE_PENNED_IN
			ELSE
				FORCE_DRAW_CHALLENGE_DPAD_LBD(TRUE, FMMC_TYPE_PENNED_IN)
			ENDIF
		ENDIF
	ENDIF
	
	LBD_SUB_MODE subMode = SUB_CHALLENGE
	IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN 
		subMode = SUB_BOSS
	ENDIF
		
	DRAW_CHALLENGE_DPAD_LBD(g_DpadDownLeaderBoardData, scDpadDownLeaderBoardMovie, subMode, sDPadVars, sFmDPadLbData)
ENDPROC

PROC MAINTAIN_NON_PART_IN_PART_VEHICLE()
	VEHICLE_INDEX vehTemp
	PED_INDEX pedTemp
	PLAYER_INDEX playerTemp
	PARTICIPANT_INDEX partTemp
	INT iPart
	BOOL bDriverISPart
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN MAINTAIN_NON_PART_IN_PART_VEHICLE NOT INJURED")
		IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
		//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN MAINTAIN_NON_PART_IN_PART_VEHICLE SITTING")
			vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			pedTemp = GET_PED_IN_VEHICLE_SEAT(vehTemp)
			IF pedTemp <> NULL
			//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN MAINTAIN_NON_PART_IN_PART_VEHICLE pedTemp <> NULL")
				IF pedTemp <> PLAYER_PED_ID()
				//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN MAINTAIN_NON_PART_IN_PART_VEHICLE pedTemp <> PLAYER_PED_ID()")
					IF IS_PED_A_PLAYER(pedTemp)
					//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN MAINTAIN_NON_PART_IN_PART_VEHICLE IS_PED_A_PLAYER")
						playerTemp = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedTemp)
						IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerTemp)
						//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN MAINTAIN_NON_PART_IN_PART_VEHICLE NETWORK_IS_PLAYER_A_PARTICIPANT")
							partTemp = NETWORK_GET_PARTICIPANT_INDEX(playerTemp)
							iPart = NATIVE_TO_INT(partTemp)
							IF IS_BIT_SET(playerBD[iPart].iPlayerBitSet, biP_InArea)
								IF NOT IS_BIT_SET(playerBD[iPart].iPlayerBitSet, biP_FailedToStayInArea)
								//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN MAINTAIN_NON_PART_IN_PART_VEHICLE biP_InArea")
									bDriverISPart = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bDriverISPart
		#IF IS_DEBUG_BUILD
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_weaponWheel")
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ----------> [sc_weaponWheel] calling STOP_PLAYER_SHOOTING_THIS_FRAME - MAINTAIN_NON_PART_IN_PART_VEHICLE")
			ENDIF
		#ENDIF
	
		STOP_PLAYER_SHOOTING_THIS_FRAME()
	ENDIF
							
						
ENDPROC

FLOAT fNonPartDistToPen = -1
FUNC FLOAT GET_DISTANCE_TO_PEN_FOR_NON_PARTS()
	IF fNonPartDistToPen = -1
		fNonPartDistToPen = GET_PEN_START_SIZE()
	ENDIF
	
	FLOAT fTemp = GET_PEN_START_SIZE()
	BOOL bGotNewSize
	INT i
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF NOT bGotNewSize
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
				IF IS_BIT_SET(playerBD[i].iPlayerBitSet, biP_InArea)
					IF NOT IS_BIT_SET(playerBD[i].iPlayerBitSet, biP_FailedToStayInArea)
						IF playerBD[i].fMyPenSize > 0.0
							IF playerBD[i].fMyPenSize < fTemp
								IF playerBD[i].fMyPenSize < fNonPartDistToPen
									fTemp = playerBD[i].fMyPenSize
									bGotNewSize = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN fTemp
ENDFUNC

/// PURPOSE:
///    	Want to stop players not participating players from disruptuing thos who are. For non-parts into passive mode when nearby
PROC MAINTAIN_NON_PART_PASSIVE_MODE()
	TEXT_LABEL_15 tl15Help = GET_PENNED_IN_PASSIVE_TEXT()
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneWon)
		IF IS_PASSIVE_MODE_DISABLED_FOR_PENNED_IN(PLAYER_ID())
			SET_PLAYER_DISABLED_PASSIVE_MODE_FOR_PENNED_IN(FALSE)
			
			IF NOT IS_BIT_SET(iBoolsBitSet, biPassiveAlready)
				DISABLE_MP_PASSIVE_MODE(PMER_NON_PARTICIPANT_OF_MATCH, FALSE)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [MAINTAIN_NON_PART_PASSIVE_MODE] Disabling passive mode as biS_SomeoneWon")
			ENDIF
		ENDIF
		
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(tl15Help)
			CLEAR_HELP()
		ENDIF
		
		EXIT
	ENDIF
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InArea)
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_FailedToStayInArea)
			EXIT
		ENDIF
	ENDIF
	
	
	VECTOR vMyCoords
	FLOAT fPassiveOnDist 
	FLOAT fPassiveOffDist 
	FLOAT fMyDist2
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF NOT HAS_NET_TIMER_STARTED(timeNonPartPassiveUpdate)
		OR(HAS_NET_TIMER_STARTED(timeNonPartPassiveUpdate) AND HAS_NET_TIMER_EXPIRED(timeNonPartPassiveUpdate, 2500))
		OR (IS_PASSIVE_MODE_DISABLED_FOR_PENNED_IN(PLAYER_ID()) != IS_MP_PASSIVE_MODE_ENABLED())

			IF NOT WAS_LOCAL_PLAYER_PLAYER_REMOVED_FROM_EVENT()
			OR (WAS_LOCAL_PLAYER_PLAYER_REMOVED_FROM_EVENT() AND IS_BIT_SET(iBoolsBitSet2, bi2_PassRemovedHelpText))
				IF NOT ARE_VECTORS_EQUAL(serverBD.vSpherePos, <<0.0, 0.0, 0.0>>)
					vMyCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
					fMyDist2 = VDIST2(vMyCoords, serverBD.vSpherePos)
					
					fNonPartDistToPen = GET_DISTANCE_TO_PEN_FOR_NON_PARTS()
					fPassiveOnDist = fNonPartDistToPen + 25.0
					fPassiveOffDist = fNonPartDistToPen + 50.0
					
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [MAINTAIN_NON_PART_PASSIVE_MODE] UPDATE vMyCoords = ", vMyCoords, " fMyDist2 = ", fMyDist2)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [MAINTAIN_NON_PART_PASSIVE_MODE] UPDATE fNonPartDistToPen = ", fNonPartDistToPen, " fPassiveOnDist2 = ", (fPassiveOnDist * fPassiveOnDist), " fPassiveOffDist2 = ", (fPassiveOffDist * fPassiveOffDist))
					
					IF NOT IS_PASSIVE_MODE_DISABLED_FOR_PENNED_IN(PLAYER_ID())
					//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [MAINTAIN_NON_PART_PASSIVE_MODE] Hit 1")
						IF fMyDist2  <= (fPassiveOnDist * fPassiveOnDist)
					//		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [MAINTAIN_NON_PART_PASSIVE_MODE] Hit 3")
							IF IS_MP_PASSIVE_MODE_ENABLED()
								SET_BIT(iBoolsBitSet, biPassiveAlready)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [MAINTAIN_NON_PART_PASSIVE_MODE] Player was already passive, set biPassiveAlready")
							ELSE
								CLEAR_BIT(iBoolsBitSet, biPassiveAlready)
								ENABLE_MP_PASSIVE_MODE(PMER_NON_PARTICIPANT_OF_MATCH)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [MAINTAIN_NON_PART_PASSIVE_MODE] Enabling passive mode")
							ENDIF
							
							SET_PLAYER_DISABLED_PASSIVE_MODE_FOR_PENNED_IN(TRUE)
							SET_BIT(iBoolsBitSet, biNonPartDoHelp)
							CLEAR_BIT(iBoolsBitSet, biNonPartDoneHelp)
						ENDIF
					ELSE
					//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [MAINTAIN_NON_PART_PASSIVE_MODE] Hit 2")
						IF fMyDist2  >= (fPassiveOffDist * fPassiveOffDist)
					//		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [MAINTAIN_NON_PART_PASSIVE_MODE] Hit 4")
							IF NOT IS_BIT_SET(iBoolsBitSet, biPassiveAlready)
								DISABLE_MP_PASSIVE_MODE(PMER_NON_PARTICIPANT_OF_MATCH, FALSE)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [MAINTAIN_NON_PART_PASSIVE_MODE] Disabling passive mode")
							ENDIF
							
							SET_PLAYER_DISABLED_PASSIVE_MODE_FOR_PENNED_IN(FALSE)  
						ELSE
							// The players passive mode state has been disabled by an external script.
							// Lets force it back on.
							IF NOT IS_MP_PASSIVE_MODE_ENABLED()
								ENABLE_MP_PASSIVE_MODE(PMER_NON_PARTICIPANT_OF_MATCH)
								CLEAR_BIT(iBoolsBitSet, biPassiveAlready)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [MAINTAIN_NON_PART_PASSIVE_MODE] Enabling passive mode (forced)")
								SET_PLAYER_DISABLED_PASSIVE_MODE_FOR_PENNED_IN(FALSE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				RESET_NET_TIMER(timeNonPartPassiveUpdate)
				START_NET_TIMER(timeNonPartPassiveUpdate)
			ELSE
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [MAINTAIN_NON_PART_PASSIVE_MODE] WAITING FOR bi2_PassRemovedHelpText")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iBoolsBitSet, biNonPartDoHelp)
		IF NOT IS_BIT_SET(iBoolsBitSet, biNonPartDoneHelp)
			IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
				PRINT_HELP_NO_SOUND(tl15Help, DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) // Passive Mode will be turned on if you approach the Penned In area.
				IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
					SET_FREEMODE_EVENT_HELP_BACKGROUND()
				ENDIF
				SET_BIT(iBoolsBitSet, biNonPartDoneHelp)
			ELSE
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [MAINTAIN_NON_PART_PASSIVE_MODE] IS_OK_TO_PRINT_FREEMODE_HELP fail non-part passive help")
			ENDIF
		ENDIF
	ENDIF
	
	
ENDPROC

PROC DO_PLAYERS_STILL_IN_COUNTER()
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneWon)
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN DO_PLAYERS_STILL_IN_COUNTER EXIT as biS_SomeoneWon")
		EXIT
	ENDIF
	
	
	
	IF iLocalPartCountAtStart = -1
		IF serverBD.iPlayersRemaining > 0
			iLocalPartCountAtStart = serverBD.iPlayersRemaining
		ENDIF
	ELSE
		IF serverBD.iPlayersRemaining > iLocalPartCountAtStart
			iLocalPartCountAtStart = serverBD.iPlayersRemaining
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - iLocalPartCountAtStart UPDATED TO ", iLocalPartCountAtStart)
		ENDIF
	ENDIF
	IF SHOULD_DISPLAY_BOTTOM_RIGHT_UI_EVENTS()
		IF NOT SHOULD_UI_BE_HIDDEN()
			DRAW_GENERIC_SCORE(serverBD.iPlayersRemaining, "PEN_REMP")
		ENDIF
	ENDIF
	FLOAT fTempDistTravelled = GET_DISTANCE_MARKER_HAS_TRAVELLED() // GET_DISTANCE_BETWEEN_COORDS(vMyBlipPos, vNodes[0])
//	GET_DISTANCE_MARKER_STILL_HAS_TO_TRAVEL()
	IF fTempDistTravelled >= fDistTravelled 	
		fDistTravelled = fTempDistTravelled
	ENDIF
	
	IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_Travelled70perc)
		IF fDistTravelled >= fDist70Percent
			SET_BIT(iBoolsBitSet2, bi2_Travelled70perc)
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - SET bi2_Travelled70perc fDist70Percent = ", fDist70Percent, " fDistTravelled = ", fDistTravelled)
		ENDIF
	ENDIF
	
	FLOAT fMeterMin = (fRouteDist - fDistTravelled)
	IF SHOULD_DISPLAY_BOTTOM_RIGHT_UI_EVENTS()
		IF NOT SHOULD_UI_BE_HIDDEN()
			DRAW_GENERIC_METER(ROUND(fMeterMin), ROUND(fRouteDist), "PEN_DST")
		ENDIF
	ENDIF
ENDPROC


PROC MAINTAIN_OFF_MISSION_HELP()
	TEXT_LABEL_15 tl15Help
	IF NOT IS_BIT_SET(iBoolsBitSet, biHelpOffMission)
		IF NOT IS_BIT_SET(serverbd.iServerBitSet, biS_EventExpired)
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InArea)
				IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
					IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_FailedToStayInArea)
						IF IS_BIT_SET(iBoolsBitSet2, bi2_RemovedFromEventAsPass)
							IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_PassRemovedHelpText)
								tl15Help = GET_PENNED_IN_DRIVER_EXITED_VEH_TEXT()
								PRINT_HELP_NO_SOUND(tl15Help) // The driver of your vehicle has exited the vehicle. You are no longer able to take part in the Penned In event.
								IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
									SET_FREEMODE_EVENT_HELP_BACKGROUND()
								ENDIF
								SET_BIT(iBoolsBitSet2, bi2_PassRemovedHelpText)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - eAD_OFF_MISSION SET bi2_PassRemovedHelpText - FORMER PASSENGER")
							ENDIF
						ELIF IS_BIT_SET(iBoolsBitSet2, bi2_RemovedFromEventAsDri)
							IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_PassRemovedHelpText)
								tl15Help = GET_PENNED_IN_CANT_TAKE_PART_TEXT()
								PRINT_HELP_NO_SOUND(tl15Help) // You are no longer able to take part in Penned In.
								IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
									SET_FREEMODE_EVENT_HELP_BACKGROUND()
								ENDIF
								SET_BIT(iBoolsBitSet2, bi2_PassRemovedHelpText)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - eAD_OFF_MISSION SET bi2_PassRemovedHelpText - FORMER DRIVER")
							ENDIF
						ELIF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaAsPassenger) // Started as passenger, knocked out.
							IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_PassRemovedHelpText)
								tl15Help = GET_PENNED_IN_CANT_TAKE_PART_TEXT()
								PRINT_HELP_NO_SOUND(tl15Help) // You are no longer able to take part in Penned In.
								IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
									SET_FREEMODE_EVENT_HELP_BACKGROUND()
								ENDIF
								SET_BIT(iBoolsBitSet2, bi2_PassRemovedHelpText)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - eAD_OFF_MISSION SET bi2_PassRemovedHelpText - PASSENGER KNOCKED OUT")
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_EveryoneFinished)
							tl15Help = GET_PENNED_IN_NOT_AVAILABLE_TEXT()
							PRINT_HELP_NO_SOUND(tl15Help, DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) //You were knocked out the Penned In event. The Penned In event is no longer available.
							IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
								SET_FREEMODE_EVENT_HELP_BACKGROUND()
							ENDIF

						ENDIF
					ENDIF
					SET_BIT(iBoolsBitSet, biHelpOffMission)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - SET biHelpOffMission - 1")
				ELSE
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [MAINTAIN_OFF_MISSION_HELP] IS_OK_TO_PRINT_FREEMODE_HELP - FAIL 1")
				ENDIF
			ELSE
				IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_FailedToStayInArea)
					IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
						IF NOT IS_BIT_SET(iBoolsBitSet, biDestWhenKnockedOut)
							tl15Help = GET_PENNED_IN_NOT_AVAILABLE_TEXT()
							PRINT_HELP_NO_SOUND(tl15Help, DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) //You were knocked out the Penned In event. The Penned In event is no longer available.
							IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
								SET_FREEMODE_EVENT_HELP_BACKGROUND()
							ENDIF
							SET_BIT(iBoolsBitSet, biHelpOffMission)
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - SET biHelpOffMission - 2")
						ENDIF
					ELSE
						
					ENDIF
//				ELIF IS_BIT_SET(iBoolsBitSet2, bi2_RemovedFromEventAsPass)
//					IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_PassRemovedHelpText)
//						IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
//							PRINT_HELP_NO_SOUND("PEN_EXITP") // The driver of your vehicle has exited the vehicle. You are no longer able to take part in the Penned In event.
//							SET_BIT(iBoolsBitSet2, bi2_PassRemovedHelpText)
//							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - eAD_OFF_MISSION SET bi2_PassRemovedHelpText")
//						ELSE
//							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - eAD_OFF_MISSION - OK_TO_PRINT_FREEMODE_HELP fail bi2_PassRemovedHelpText")
//						ENDIF
//					ENDIF 
				ENDIF
			ENDIF
		ELSE
			tl15Help = GET_PENNED_IN_EXPIRED_TICKER_TEXT()
			PRINT_TICKER(tl15Help) // The Penned In event is no longer available.
			SET_BIT(iBoolsBitSet, biHelpOffMission)
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - SET biHelpOffMission - 3")
		ENDIF
		
	ENDIF
		
ENDPROC

PROC MAINTAIN_TICKERS()
	IF NOT IS_BIT_SET(serverbd.iServerBitSet, biS_EventExpired)
		IF NOT IS_BIT_SET(iBoolsBitSet, biEventStartTick)
			IF playerBD[PARTICIPANT_ID_TO_INT()].eStage = 	eAD_OFF_MISSION	
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_FailedToStayInArea)
					IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InArea)
						IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaAsPassenger)
							IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_EnteredCorona)
								//PRINT_TICKER("PEN_NAVAIL")
								IF NOT WAS_LOCAL_PLAYER_PLAYER_REMOVED_FROM_EVENT()
									IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
										TEXT_LABEL_15 tl15Started = GET_PENNED_IN_STARTED_TICKER_TEXT()
										PRINT_HELP_NO_SOUND(tl15Started, DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
										IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
											SET_FREEMODE_EVENT_HELP_BACKGROUND()
										ENDIF
										SET_BIT(iBoolsBitSet, biEventStartTick)
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - SET biEventStartTick")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_EVENT_HIDDEN()
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_SHouldHideEvent)
		IF SHOULD_HIDE_AMBIENT_EVENT(ciPI_HIDE_MENU_ITEM_AMBIENT_EVENTS_PENNED_IN)
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_SHouldHideEvent)
			PRINTLN("     ---------->     PENNED IN -  SET biP_SHouldHideEvent 2  <---------- ")
		ENDIF
	ELSE
		IF NOT SHOULD_HIDE_AMBIENT_EVENT(ciPI_HIDE_MENU_ITEM_AMBIENT_EVENTS_PENNED_IN)
			CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_SHouldHideEvent)
			PRINTLN("     ---------->     PENNED IN -  CLEAR biP_SHouldHideEvent 2  <---------- ")
		ENDIF
	ENDIF
ENDPROC


PROC DO_HELP_TEXT(INT iHelp)
	
	SWITCH iHelp
		CASE 1
			IF NOT IS_BIT_SET(iBoolsBitSet, biHelp1Done)
				IF HAS_PLAYER_BEEN_IN_SESSION_THIS_LONG(CRATE_DROP_DISPLAY_DELAY)
				AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_PAUSE_MENU_ACTIVE()
					PRINT_HELP_WITH_STRING_AND_NUMBER_NO_SOUND("UW_HELP1", GET_VEH_BLIP_STRING(), GET_CASH_REWARD())	//A ~p~Hot Target~s~ ~p~~a~ ~s~is available. Enter it and destroy Merryweather patrols for $~1~.
					IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
						SET_FREEMODE_EVENT_HELP_BACKGROUND()
					ENDIF
					SET_BIT(iBoolsBitSet, biHelp1Done)
					PRINTLN("     ---------->     PENNED IN -  HELP TEXT - biHelp1Done SET    <----------     ")
				ENDIF
			ENDIF
		BREAK
		/*CASE 2
			IF NOT IS_BIT_SET(iBoolsBitSet, biHelp2Done)
				IF HAS_PLAYER_BEEN_IN_SESSION_THIS_LONG(CRATE_DROP_DISPLAY_DELAY)
				AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_PAUSE_MENU_ACTIVE()
					PRINT_HELP_WITH_STRING_NO_SOUND("UW_HELP2", GET_VEHICLE_TYPE_STRING())	//You are a ~r~Hot Target~s~. Other players will be rewarded for killing you while you are delivering the ~a~.
					SET_BIT(iBoolsBitSet, biHelp2Done)
					PRINTLN("     ---------->     PENNED IN - HELP TEXT - biHelp2Done SET    <----------     ")
				ENDIF
			ENDIF
		BREAK*/
	ENDSWITCH
ENDPROC

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  ENTITY CREATION                  /////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////
///    			VEHICLE   				///
///////////////////////////////////////////
//PURPOSE: Create the Veh
FUNC BOOL CREATE_VEH()

	
	RETURN TRUE
ENDFUNC

//PURPOSE: Creates the Veh and Driver that will deliver the Vehicle
FUNC BOOL CREATE_MAIN_VEHICLE()
	
	
	RETURN FALSE
ENDFUNC




PROC MAINTAIN_MAX_PARTICIPANT_CHECKS_CLIENT()
	INT i
	INT iTempValidPlayerCount
	INT iTempPotentialPlayer
	VEHICLE_INDEX vehTemp
	
	IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_localReachedEndOfRoute)
		IF IS_BIT_SET(serverBD.iServerBitSet, biS_ReachedEndOfRoute)
			SET_BIT(iBoolsBitSet2, bi2_localReachedEndOfRoute)
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [MAINTAIN_MAX_PARTICIPANT_CHECKS_CLIENT] SET bi2_localReachedEndOfRoute AS SERVER SAYS SO")
		ENDIF
	ENDIF
	
	
	IF serverBD.iServerGameState != GAME_STATE_END
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
				PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
				PED_INDEX pedPlayer = GET_PLAYER_PED(PlayerId)
				
				IF NOT IS_PLAYER_SCTV(PlayerId)
					
					IF playerBD[i].eStage = eAD_IDLE
						IF i <> PARTICIPANT_ID_TO_INT()
							IF IS_BIT_SET(playerBD[i].iPlayerBitSet, biP_InArea)
								iTempValidPlayerCount++
							ENDIF
						ENDIF
						
						IF NOT FM_EVENT_HAS_PLAYER_HAS_BLOCKED_CURRENT_FM_EVENT(PlayerId) 
							iTempPotentialPlayer++
						ENDIF
					ENDIF
					
					

					IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_LOcalThinksSOmeoneWon)
						IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_localReachedEndOfRoute)
							IF IS_BIT_SET(playerBD[i].iPlayerBitSet, biP_ReachedEnd)
								SET_BIT(iBoolsBitSet2, bi2_localReachedEndOfRoute)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [MAINTAIN_MAX_PARTICIPANT_CHECKS_CLIENT] SET bi2_localReachedEndOfRoute THIS PLAYER SAYS SO ", GET_PLAYER_NAME(PlayerId))
							ENDIF
						ENDIF
					ENDIF
					
					IF i <> PARTICIPANT_ID_TO_INT()	
						IF serverBD.iPennedInType = PENNED_IN_TYPE_ROAD_VEHICLE
						OR serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
							IF NOT IS_BIT_SET(iInitResetRemotePlayerGhosted, i)
								SET_REMOTE_PLAYER_AS_GHOST(PlayerId, FALSE)
								SET_BIT(iInitResetRemotePlayerGhosted, i)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [MAINTAIN_MAX_PARTICIPANT_CHECKS_CLIENT]  I've set player ", GET_PLAYER_NAME(PlayerId), " as NOT ghosted as the script has just launched")
							ENDIF
						ENDIF
						
						IF playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_IN_AREA
						
							IF playerBD[i].eStage = eAD_IN_AREA
								IF NOT IS_PED_INJURED(PLAYER_PED_ID())
									IF NOT IS_BIT_SET(playerBD[i].iPlayerBitSet, biP_InAreaAsPassenger)
									AND NOT ARE_PEDS_IN_SAME_VEHICLE(PLAYER_PED_ID(), pedPlayer)
										IF NOT IS_BIT_SET(iOtherPlayerBlipsBitset, i)
											IF NOT IS_BIT_SET(playerBD[i].iPlayerBitSet, biP_FailedToStayInArea)
												SET_BIT(iOtherPlayerBlipsBitset, i)
												SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(PlayerId, RADAR_TRACE_PENNED_IN, TRUE)
												IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
													SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(PlayerId, BLIP_COLOUR_RED, TRUE)
												ENDIF
												CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [MAINTAIN_MAX_PARTICIPANT_CHECKS_CLIENT] Added custom blip for player ", GET_PLAYER_NAME(PlayerId))
											ENDIF
										ELSE
											IF IS_BIT_SET(playerBD[i].iPlayerBitSet, biP_FailedToStayInArea)
												CLEAR_BIT(iOtherPlayerBlipsBitset, i)
												SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(PlayerId, RADAR_TRACE_PENNED_IN, FALSE)
												IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
													SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(PlayerId, BLIP_COLOUR_RED, FALSE)
												ENDIF
												CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [MAINTAIN_MAX_PARTICIPANT_CHECKS_CLIENT] Removed custom blip as knocked out for player ", GET_PLAYER_NAME(PlayerId))
											ENDIF
										ENDIF
										
//										IF IS_PED_IN_ANY_VEHICLE(pedPlayer)
//											
//											IF NOT IS_BIT_SET(iVehicleBitset, i)
//												VECTOR returnMin, returnMax
//												VEHICLE_INDEX viTemp
//												
//												IF NOT IS_PED_INJURED(pedPlayer)
//													viTemp = GET_VEHICLE_PED_IS_USING(pedPlayer)
//													GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(viTemp), returnMin, returnMax)
//													fVehicleDimensions[i] = ((returnMax.z - returnMin.z)/2) + 0.6
//													
//													SET_BIT(iVehicleBitset, i)
//												ENDIF
//											ELSE
//												IF NOT IS_PED_INJURED(pedPlayer)
//													DRAW_MARKER(MARKER_ARROW, GET_ENTITY_COORDS(pedPlayer)+<<0,0,(fVehicleDimensions[i])>>, <<0,0,0>>, <<180,0,0>>, <<0.5,0.5,0.5>>, 255, 0, 0, 100, TRUE, TRUE)
//												ENDIF
//											ENDIF
//										ELSE
//											CLEAR_BIT(iVehicleBitset, i)
//										ENDIF
									ENDIF
								ENDIF
								
								
							ELSE
								IF IS_BIT_SET(iOtherPlayerBlipsBitset, i)
									CLEAR_BIT(iOtherPlayerBlipsBitset, i)
									SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(PlayerId, RADAR_TRACE_PENNED_IN, FALSE)
									
									IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
										SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(PlayerId, BLIP_COLOUR_RED, FALSE)
									ENDIF
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [MAINTAIN_MAX_PARTICIPANT_CHECKS_CLIENT] Removed custom blip for player ", GET_PLAYER_NAME(PlayerId))
								ENDIF
							ENDIF
							
							
						
						ELSE
							IF IS_BIT_SET(iOtherPlayerBlipsBitset, i)
								CLEAR_BIT(iOtherPlayerBlipsBitset, i)
								SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(PlayerId, RADAR_TRACE_PENNED_IN, FALSE)
								
								IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
									SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(PlayerId, BLIP_COLOUR_RED, FALSE)
								ENDIF
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [MAINTAIN_MAX_PARTICIPANT_CHECKS_CLIENT] Removed custom blip for player ", GET_PLAYER_NAME(PlayerId), " as my stage no longer eAD_IN_AREA")
							ENDIF
						ENDIF
					
						IF playerBD[PARTICIPANT_ID_TO_INT()].eStage > eAD_IDLE
						
							IF NOT IS_BIT_SET(iOtherPlayerAudioBitset, i)
							AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneWon)
								IF IS_BIT_SET(playerBD[i].iPlayerBitSet, biP_InAreaForAudioScene)
									ADD_ENTITY_TO_AUDIO_MIX_GROUP(pedPlayer, "MP_Penned_In_Vehicles_In_Bubble_Group", 0.0)
									SET_BIT(iOtherPlayerAudioBitset, i)
								//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [MAINTAIN_MAX_PARTICIPANT_CHECKS_CLIENT] Set audio scene for player ", GET_PLAYER_NAME(PlayerId))
									IF IS_PED_IN_ANY_VEHICLE(pedPlayer)
										vehTemp = GET_VEHICLE_PED_IS_IN(pedPlayer)
										ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehTemp, "MP_Penned_In_Vehicles_In_Bubble_Group", 0.0)
									//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [MAINTAIN_MAX_PARTICIPANT_CHECKS_CLIENT] Set audio scene for player's veh ", GET_PLAYER_NAME(PlayerId))
									ENDIF
								ENDIF
							ELSE 
								IF NOT IS_BIT_SET(playerBD[i].iPlayerBitSet, biP_InAreaForAudioScene)
								OR IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneWon)
									REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(pedPlayer, 0.0)
									CLEAR_BIT(iOtherPlayerAudioBitset, i)
								//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [MAINTAIN_MAX_PARTICIPANT_CHECKS_CLIENT] Removed audio scene for player ", GET_PLAYER_NAME(PlayerId))
									IF IS_PED_IN_ANY_VEHICLE(pedPlayer)
										vehTemp = GET_VEHICLE_PED_IS_IN(pedPlayer)
										REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehTemp, 0.0)
									//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [MAINTAIN_MAX_PARTICIPANT_CHECKS_CLIENT] Removed audio scene for player's veh ", GET_PLAYER_NAME(PlayerId))
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						
						//-- Set other plays as ghosted / not ghosted based on whether or not they're in the pen
						IF serverBD.iPennedInType = PENNED_IN_TYPE_ROAD_VEHICLE
						OR serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								IF NOT IS_PED_INJURED(pedPlayer)
									IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_SHouldNotBeDamageable)
										IF NOT IS_BIT_SET(iOtherPlayerGhostedPartBitset, i)
											IF NOT IS_BIT_SET(playerBD[i].iPlayerBitSet, biP_SHouldNotBeDamageable)
												IF NOT ARE_PEDS_IN_SAME_VEHICLE(PLAYER_PED_ID(), pedPlayer)
													IF NOT IS_BIT_SET(playerBD[i].iPlayerBitSet, biP_IsSpectating) 
													//	IF NOT IS_ENTITY_A_GHOST(pedPlayer)
															#IF IS_DEBUG_BUILD
															IF IS_ENTITY_A_GHOST(pedPlayer)
																CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [MAINTAIN_MAX_PARTICIPANT_CHECKS_CLIENT] biP_SHouldNotBeDamageable set for me and I'm going to set player ", GET_PLAYER_NAME(PlayerId), " as ghosted as but they are already a ghost")
															ENDIF
															#ENDIF
															
															SET_BIT(iOtherPlayerGhostedPartBitset, i)
															SET_BIT(iOtherPlayerGhostedPlayerBitset, NATIVE_TO_INT(PlayerId))
															SET_REMOTE_PLAYER_AS_GHOST(PlayerId, TRUE)
															CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [MAINTAIN_MAX_PARTICIPANT_CHECKS_CLIENT] biP_SHouldNotBeDamageable set for me and I've set player ", GET_PLAYER_NAME(PlayerId), " as ghosted as they're not in the area")
															
															
	//													ELSE
	//														CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [MAINTAIN_MAX_PARTICIPANT_CHECKS_CLIENT] biP_SHouldNotBeDamageable set for me and want to set player ", GET_PLAYER_NAME(PlayerId), " as ghosted as they're not in the area but they're already a ghost!")
	//													ENDIF
													ENDIF
												ENDIF
											ENDIF
										ELSE
											IF IS_BIT_SET(playerBD[i].iPlayerBitSet, biP_SHouldNotBeDamageable)
												CLEAR_BIT(iOtherPlayerGhostedPartBitset, i)
												CLEAR_BIT(iOtherPlayerGhostedPlayerBitset, NATIVE_TO_INT(PlayerId))
												SET_REMOTE_PLAYER_AS_GHOST(PlayerId, FALSE)
												CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [MAINTAIN_MAX_PARTICIPANT_CHECKS_CLIENT] biP_SHouldNotBeDamageable set for me and I've set player ", GET_PLAYER_NAME(PlayerId), " as NOT ghosted as they're in the area")
											ELIF IS_BIT_SET(playerBD[i].iPlayerBitSet, biP_IsSpectating)
											
												CLEAR_BIT(iOtherPlayerGhostedPartBitset, i)
												CLEAR_BIT(iOtherPlayerGhostedPlayerBitset, NATIVE_TO_INT(PlayerId))
												SET_REMOTE_PLAYER_AS_GHOST(PlayerId, FALSE)
												CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [MAINTAIN_MAX_PARTICIPANT_CHECKS_CLIENT] biP_SHouldNotBeDamageable set for me and I've set player ", GET_PLAYER_NAME(PlayerId), " as NOT ghosted as they're in the spectator helicopter")
											ELSE		
												IF ARE_PEDS_IN_SAME_VEHICLE(PLAYER_PED_ID(), pedPlayer)
												
													CLEAR_BIT(iOtherPlayerGhostedPartBitset, i)
													CLEAR_BIT(iOtherPlayerGhostedPlayerBitset, NATIVE_TO_INT(PlayerId))
													SET_REMOTE_PLAYER_AS_GHOST(PlayerId, FALSE)
													CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [MAINTAIN_MAX_PARTICIPANT_CHECKS_CLIENT] biP_SHouldNotBeDamageable set for me and I've set player ", GET_PLAYER_NAME(PlayerId), " as NOT ghosted as they're in the same vehicle as me")
																
												ENDIF
											ENDIF
										ENDIF
									ELSE 
										IF IS_BIT_SET(iOtherPlayerGhostedPartBitset, i)
											SET_REMOTE_PLAYER_AS_GHOST(PlayerId, FALSE)
											CLEAR_BIT(iOtherPlayerGhostedPartBitset, i)
											CLEAR_BIT(iOtherPlayerGhostedPlayerBitset, NATIVE_TO_INT(PlayerId))
											CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [MAINTAIN_MAX_PARTICIPANT_CHECKS_CLIENT] biP_SHouldNotBeDamageable NOT set for me and I've set player ", GET_PLAYER_NAME(PlayerId), " as NOT ghosted as I'm not in the area - they'll ghost me")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		
		IF iClientPLayerCOunt <> iTempValidPlayerCount
			iClientPLayerCOunt = iTempValidPlayerCount
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [MAINTAIN_MAX_PARTICIPANT_CHECKS_CLIENT] iClientPLayerCOunt UPDATED TO ", iClientPLayerCOunt)
		ENDIF
		
		IF iLocalPotentialPlayerCount <> iTempPotentialPlayer
			iLocalPotentialPlayerCount = iTempPotentialPlayer
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [MAINTAIN_MAX_PARTICIPANT_CHECKS_CLIENT] iLocalPotentialPlayerCount UPDATED TO ", iLocalPotentialPlayerCount)
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_NUMBER_OF_PLAYERS_ALREADY_STARTING()
	INT iCOunt
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		IF IS_BIT_SET(serverBD.iStartingPartBitSet, i)
			iCOunt++
		ENDIF
	ENDREPEAT
	
	RETURN iCount
ENDFUNC

FUNC BOOL OK_TO_COUNT_PART_AS_STILL_PLAYING(BOOL bLocalReachedEnd #IF IS_DEBUG_BUILD ,PLAYER_INDEX playerCurrent #ENDIF)
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneWon)	
		//-- Nobody has won yet, can count the part
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneWon)	
		//-- Someone has won...
		
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_FinalLbUpdate)
			//-- Haven't done final update yet
			
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER biS_SomeoneWon set but not done final update. Will check for end of route ", GET_PLAYER_NAME(playerCurrent))
			#ENDIF
			
			
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_ReachedEndOfRoute)
				//-- Server bit saying the ped is at the end of its route is set
				
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER biS_SomeoneWon set but not done final update. At end of route - biS_ReachedEndOfRoute ", GET_PLAYER_NAME(playerCurrent))
				#ENDIF
			
				RETURN TRUE
			ENDIF
			
			IF bLocalReachedEnd
				//-- Have just detected that the pen is at the end of its route, prior to setting the server bit
				
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER biS_SomeoneWon set but not done final update. At end of route - bLocalReachedEnd ", GET_PLAYER_NAME(playerCurrent))
				#ENDIF
			
				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

#IF FEATURE_STUNT_FM_EVENTS
PROC MAINTAIN_LOCK_AIRCRAFT_DOORS_FOR_PART(INT iPart)
	IF serverBD.iPennedInType != PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY
		EXIT
	ENDIF
	
	IF serverBD.eStage = eAD_IDLE
		EXIT
	ENDIF
	
	INT i
	PLAYER_INDEX playerLock =  NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart))
	
	IF NOT IS_BIT_SET(serverBD.iLockedAircraftDoorsBitset, iPart)
		REPEAT MAX_AIRCRAFT_TO_SPAWN i
		
			IF serverBD.eStage = eAD_COUNTDOWN
			OR serverBD.eStage = eAD_IN_AREA
				IF NOT IS_BIT_SET(playerBD[iPart].iPlayerBitSet, biP_InArea)
				AND NOT IS_BIT_SET(playerBD[iPart].iPlayerBitSet, biP_InAreaAsPassenger)
					SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.niAircraft[i]), playerLock, TRUE)
					SET_BIT(serverBD.iLockedAircraftDoorsBitset, iPart)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER LOCKING DOORS ON AIRCRAFT ",i, " FOR PLAYER ", GET_PLAYER_NAME(playerLock))
				ENDIF
			ELIF serverBD.eStage > eAD_IN_AREA
				SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.niAircraft[i]), playerLock, TRUE)
				SET_BIT(serverBD.iLockedAircraftDoorsBitset, iPart)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER LOCKING DOORS ON AIRCRAFT ",i, " FOR PLAYER ", GET_PLAYER_NAME(playerLock))
				
			ENDIF
		
		ENDREPEAT
	ENDIF
ENDPROC
#ENDIF

//PURPOSE: Runs through the max num participents and does relevant checks
PROC MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
	INT iNumInSession	
	INT iNumPlayers
	INT iNumPlayersTakingPart
	INT iNumPlayersTakingPartAtRouteEnd
	INT iNumPotentialPlayers
	INT iNumPlayersAlreadyStarting
//	INT iNumPlayersIgnoringEvent
//	INT iNumPlayersRunningScript 
	
	BOOL bSomeoneNotFinished = FALSE
	BOOL bSomeonesCountdownNotFinished = FALSE
	BOOL bSomeoneReachedEnd
	BOOL bSomeoneWon
	
	BOOL bFoundLaunchBoss
	
	
	
	IF serverBD.iServerGameState != GAME_STATE_END
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iStaggeredParticipant
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iStaggeredParticipant))
				iNumInSession++
				PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iStaggeredParticipant))
				IF NOT IS_PLAYER_SCTV(PlayerId)

					//PED_INDEX PlayerPedId = GET_PLAYER_PED(PlayerId)
					
					//** CHECKS THAT DON'T NEED THE PLAYER TO BE ALIVE **//
					
					//-- Launch boss quit?
					IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
						IF NOT bFoundLaunchBoss
							
							IF GET_LAUNCH_BOSS_AS_PLAYER() != INVALID_PLAYER_INDEX()
								IF GET_LAUNCH_BOSS_AS_PLAYER() = PlayerId
									bFoundLaunchBoss = TRUE
								ENDIF
							ELSE
								bFoundLaunchBoss = TRUE
							ENDIF
						ENDIF
					ENDIF
					 // FEATURE_BIKER
					
					IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_InArea)
						iNumPlayers++
					ELSE
					ENDIF
					

					IF NOT FM_EVENT_HAS_PLAYER_HAS_BLOCKED_CURRENT_FM_EVENT(PlayerId)
					OR (serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN AND GB_GET_PLAYER_UI_LEVEL(PlayerId) > GB_UI_LEVEL_NONE)
						iNumPotentialPlayers++
					ELSE
						IF serverBD.eStage = eAD_IDLE
							IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
								IF GB_GET_PLAYER_UI_LEVEL(PlayerId) = GB_UI_LEVEL_NONE
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER iNumPotentialPlayers - REJECTING PLAYER BECAUSE THEIR UI LEVEL IS GB_UI_LEVEL_NONE ", GET_PLAYER_NAME(PlayerId))
								ENDIF
							ENDIF
						ENDIF
						
					ENDIF
					
					//-- Set who can start
					IF serverBD.eStage = eAD_IDLE
						IF NOT IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_InArea)
							IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_WantToStart)
								iNumPlayersAlreadyStarting = GET_NUMBER_OF_PLAYERS_ALREADY_STARTING()
								IF iNumPlayersAlreadyStarting < GET_MAX_NUMBER_OF_PLAYERS_FOR_LAUNCH()
									SET_BIT(serverBD.iStartingPartBitSet, iStaggeredParticipant)
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER SET iStartingPartBitSet BIT ", iStaggeredParticipant, " AS PLAYER ", GET_PLAYER_NAME(PlayerId), " CAN START iNumPlayersAlreadyStarting = ", iNumPlayersAlreadyStarting)
								ELSE
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER NOT SETTING  iStartingPartBitSet BIT ", iStaggeredParticipant, " AS PLAYER ", GET_PLAYER_NAME(PlayerId), " CANNOT START iNumPlayersAlreadyStarting = ", iNumPlayersAlreadyStarting)
								ENDIF
							ELSE
								IF IS_BIT_SET(serverBD.iStartingPartBitSet, iStaggeredParticipant)
									CLEAR_BIT(serverBD.iStartingPartBitSet, iStaggeredParticipant)
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER CLEARING iStartingPartBitSet BIT ", iStaggeredParticipant, " AS PLAYER ", GET_PLAYER_NAME(PlayerId), " NO LONGER WANTS TO START")
								ENDIF
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
								IF NOT IS_BIT_SET(serverBD.iStartingPartBitSet, iStaggeredParticipant)
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER biP_InArea SET FOR PLAYER ", GET_PLAYER_NAME(PlayerId), " BUT STARTING BIT NOT SET!")
								ENDIF
							#ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_CountdownExpired)
						IF serverBD.eStage = eAD_COUNTDOWN
							IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_InArea)
								IF NOT IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_DoneCOuntdown)
									
									bSomeonesCountdownNotFinished = TRUE
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - Server Thinks this player not finished their 321 countdown - ", GET_PLAYER_NAME(PlayerId))
								ENDIF
							ENDIF
						ELSE
							bSomeonesCountdownNotFinished = TRUE
						ENDIF
					ENDIF
				//	IF serverBD.eStage = eAD_IN_AREA
					
				//	ENDIF
					
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ReachedEndOfRoute)
						IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_ReachedEnd)
							bSomeoneReachedEnd = TRUE
							
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - Server Thinks this player's area reached end of route- ", GET_PLAYER_NAME(PlayerId), " Part Bit = ", iStaggeredParticipant, " time ", GET_CLOUD_TIME_AS_INT())
						ENDIF
					ENDIF
					
					IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_InArea)
						IF NOT IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_FailedToStayInArea)
							IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ReachedEndOfRoute)
							OR (IS_BIT_SET(serverBD.iServerBitSet, biS_ReachedEndOfRoute) AND NOT IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_OutCoronaTimeStarted))
							//	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneWon)
							//	OR (IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneWon) 
							//		AND (IS_BIT_SET(serverBD.iServerBitSet, biS_ReachedEndOfRoute) 
							//			OR bSomeoneReachedEnd)
							//		AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_FinalLbUpdate))
									
									
								IF OK_TO_COUNT_PART_AS_STILL_PLAYING(bSomeoneReachedEnd #IF IS_DEBUG_BUILD , PlayerId #ENDIF)	
									iNumPlayersTakingPart++
									IF NOT IS_BIT_SET(serverBD.iPartStillPlayingBitset, iStaggeredParticipant)
										SET_BIT(serverBD.iPartStillPlayingBitset, iStaggeredParticipant)
										SET_BIT(serverBD.iPlayerSTillPlayingBitSet, NATIVE_TO_INT(PlayerId))
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - Server Thinks this player is still on mission - ", GET_PLAYER_NAME(PlayerId), " Part Bit = ", iStaggeredParticipant, " time ", GET_CLOUD_TIME_AS_INT())
										
										IF (IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneWon)  AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_FinalLbUpdate))
											CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - (biS_SomeoneWon set / biS_FinalLbUpdate not set) Server Thinks this player is still on mission - ", GET_PLAYER_NAME(PlayerId), " Part Bit = ", iStaggeredParticipant, " time ", GET_CLOUD_TIME_AS_INT())
										ENDIF
									ENDIF
									
									IF IS_BIT_SET(serverBD.iServerBitSet, biS_ReachedEndOfRoute)
										bSomeoneWon = TRUE
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - Server Thinks this player won - ", GET_PLAYER_NAME(PlayerId), " because the end of the route has been reached time ", GET_CLOUD_TIME_AS_INT())
									ENDIF
								ENDIF
							ELSE
								IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneWon)
									IF IS_BIT_SET(serverBD.iPartStillPlayingBitset, iStaggeredParticipant)
										CLEAR_BIT(serverBD.iPartStillPlayingBitset, iStaggeredParticipant)
										CLEAR_BIT(serverBD.iPlayerSTillPlayingBitSet, NATIVE_TO_INT(PlayerId))
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - Server Thinks this player failed to stay in area- ", GET_PLAYER_NAME(PlayerId), " Part Bit = ", iStaggeredParticipant, " (biS_ReachedEndOfRoute is set) time ", GET_CLOUD_TIME_AS_INT())
									ENDIF
								
									IF IS_BIT_SET(serverBD.iServerBitSet, biS_ReachedEndOfRoute)
										IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_OutCoronaTimeStarted)
											iNumPlayersTakingPartAtRouteEnd++
										ENDIF
									ENDIF
								ELSE
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER NOT UPDATING iPartStillPlayingBitset / iNumPlayersTakingPartAtRouteEnd as biS_SomeoneWon. biS_ReachedEndOfRoute is set")
								ENDIF
							ENDIF
						ELSE
							IF IS_BIT_SET(serverBD.iPartStillPlayingBitset, iStaggeredParticipant)
								CLEAR_BIT(serverBD.iPartStillPlayingBitset, iStaggeredParticipant)
								CLEAR_BIT(serverBD.iPlayerSTillPlayingBitSet, NATIVE_TO_INT(PlayerId))
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - Server Thinks this player failed to stay in area- ", GET_PLAYER_NAME(PlayerId), " Part Bit = ", iStaggeredParticipant)
							ENDIF
						ENDIF
					ELSE
						IF IS_BIT_SET(serverBD.iPartStillPlayingBitset, iStaggeredParticipant)
							CLEAR_BIT(serverBD.iPartStillPlayingBitset, iStaggeredParticipant)
						ENDIF
						
						IF IS_BIT_SET(serverBD.iPlayerSTillPlayingBitSet, NATIVE_TO_INT(PlayerId))
							CLEAR_BIT(serverBD.iPlayerSTillPlayingBitSet, NATIVE_TO_INT(PlayerId))
						ENDIF
						
					ENDIF
					
					#IF FEATURE_STUNT_FM_EVENTS
					//-- Lock the doors on all the ambient helicopters once the event starts
					MAINTAIN_LOCK_AIRCRAFT_DOORS_FOR_PART(iStaggeredParticipant)
					#ENDIF
					
					//** CHECKS THAT REQUIRE THE PLAYER TO BE ALIVE **//
					IF IS_NET_PLAYER_OK(PlayerId)
						
					ENDIF
				ELSE
					// SCTV
					IF IS_BIT_SET(serverBD.iPartStillPlayingBitset, iStaggeredParticipant)
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - Clear iPartStillPlayingBitset bit for Part ", iStaggeredParticipant, " as SCTV")
						CLEAR_BIT(serverBD.iPartStillPlayingBitset, iStaggeredParticipant)
					ENDIF
					
					IF IS_BIT_SET(serverBD.iStartingPartBitSet, iStaggeredParticipant)
						CLEAR_BIT(serverBD.iStartingPartBitSet, iStaggeredParticipant)
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER CLEARING iStartingPartBitSet BIT ", iStaggeredParticipant, " AS PLAYER ", GET_PLAYER_NAME(PlayerId), " IS SCTV")
					ENDIF
				ENDIF
				
				
				
				IF serverBD.eStage <= eAD_IN_AREA
					bSomeoneNotFinished = TRUE
				ELSE
					IF NOT IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_DoneReward)
						IF NOT IS_PLAYER_SCTV(PlayerId)
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - Server Thinks this player not finished ", GET_PLAYER_NAME(PlayerId))
						ELSE
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - Server Thinks this player not finished ", GET_PLAYER_NAME(PlayerId), " PLAYER IS SCTV")
						ENDIF
						bSomeoneNotFinished = TRUE
					ENDIF
				ENDIF
					
			ELSE
				IF IS_BIT_SET(serverBD.iPartStillPlayingBitset, iStaggeredParticipant)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - Clear iPartStillPlayingBitset bit for Part ", iStaggeredParticipant, " as no longer active")
					CLEAR_BIT(serverBD.iPartStillPlayingBitset, iStaggeredParticipant)
				ENDIF
				
				IF IS_BIT_SET(serverBD.iStartingPartBitSet, iStaggeredParticipant)
					CLEAR_BIT(serverBD.iStartingPartBitSet, iStaggeredParticipant)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER CLEARING iStartingPartBitSet BIT ", iStaggeredParticipant, " AS PART NO LONGER ACTIVE")
				ENDIF
			ENDIF
			
		ENDREPEAT
		
		//-- Launch boss quit?
		IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_CagedInBossQuit)
				IF NOT bFoundLaunchBoss
					SET_BIT(serverBD.iServerBitSet, biS_CagedInBossQuit)
				ENDIF
			ENDIF
		ENDIF
	
		
		IF iNumPlayers > serverBD.iPeakQualifiers 
			serverBD.iPeakQualifiers = iNumPlayers
		ENDIF
		
		IF iNumInSession > serverBD.iPeakParticipants
			serverBD.iPeakParticipants = iNumInSession
		ELSE
			serverBD.iPlayersLeftInProgress = serverBD.iPeakParticipants - iNumInSession
		ENDIF
		
		IF serverBD.iPlayersRemaining <> (iNumPlayersTakingPart + iNumPlayersTakingPartAtRouteEnd)
			serverBD.iPlayersRemaining = (iNumPlayersTakingPart + iNumPlayersTakingPartAtRouteEnd)
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - serverBD.iPlayersRemaining updated to ", serverBD.iPlayersRemaining)
			
			UPDATE_PENNED_IN_LEADERBOARD()
		ENDIF
		
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_PlayerLimitReached)
			IF serverBD.iPlayersRemaining >= GET_MAX_NUMBER_OF_PLAYERS_FOR_LAUNCH()
			OR (iNumPotentialPlayers >= 2 AND serverBD.iPlayersRemaining >= iNumPotentialPlayers)
				SET_BIT(serverBD.iServerBitSet, biS_PlayerLimitReached)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - SET biS_PlayerLimitReached ")
			ENDIF
		ENDIF
				
		IF serverBD.eStage = eAD_IDLE
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AtLeastOnePlayerOnMission)
				IF iNumPlayers >=1
					SET_BIT(serverBD.iServerBitSet, biS_AtLeastOnePlayerOnMission)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - Set biS_AtLeastOnePlayerOnMission iNumPlayers = ", iNumPlayers)
				ENDIF
			ELSE
				IF iNumPlayers = 0
					CLEAR_BIT(serverBD.iServerBitSet, biS_AtLeastOnePlayerOnMission)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - Clear biS_AtLeastOnePlayerOnMission iNumPlayers = ", iNumPlayers)
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AtLeastTwoPlayersOnMission)
				IF iNumPlayers >= 2
					SET_BIT(serverBD.iServerBitSet, biS_AtLeastTwoPlayersOnMission)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - Set biS_AtLeastTwoPlayersOnMission iNumPlayers = ", iNumPlayers)
				ENDIF
			ELSE
				IF iNumPlayers < 2
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - Clear biS_AtLeastTwoPlayersOnMission iNumPlayers = ", iNumPlayers)
					CLEAR_BIT(serverBD.iServerBitSet, biS_AtLeastTwoPlayersOnMission)
				ENDIF
			ENDIF
			
			//-- Reduce start timer to 30 secs if there's >= half the potential players in the area
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_UseShortCountDown)
			//	INT iNumPotentialPlayers = iNumPlayersRunningScript - iNumPlayersIgnoringEvent
			
				IF iNumPlayers >= GET_MIN_NUMBER_OF_PLAYERS_FOR_QUICK_LAUNCH() //CEIL(TO_FLOAT(iNumPotentialPlayers) * (TO_FLOAT(GET_QUICK_LAUNCH_PERCENTAGE())/ 100.0))
					SET_BIT(serverBD.iServerBitSet, biS_UseShortCountDown)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - Set biS_UseShortCountDown as  iNumPlayers = ", iNumPlayers)
				ENDIF
			ENDIF
			
			
			//-- Enough valid players?
			IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_CagedInNotEnoughPlayers)
					IF HAS_NET_TIMER_STARTED(serverBD.timeSinceLaunched)
						IF HAS_NET_TIMER_EXPIRED(serverBD.timeSinceLaunched, 10000)
							IF iNumPotentialPlayers < 2
								#IF IS_DEBUG_BUILD
								IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_NoGoonsRequiredForBossWork")
								#ENDIF
									SET_BIT(serverBD.iServerBitSet, biS_CagedInNotEnoughPlayers)
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER Set biS_CagedInNotEnoughPlayers")
								#IF IS_DEBUG_BUILD
								ENDIF
								#ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			 // FEATURE_BIKER
			
		ENDIF
		
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_CountdownExpired)
			IF serverBD.eStage = eAD_COUNTDOWN
				IF NOT bSomeonesCountdownNotFinished
					SET_BIT(serverBD.iServerBitSet, biS_CountdownExpired)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER SET biS_CountdownExpired")
				ENDIF
			ENDIF
		ENDIF
		
		//-- Corona reached end?
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ReachedEndOfRoute)	
			IF bSomeoneReachedEnd
				SET_BIT(serverBD.iServerBitSet, biS_ReachedEndOfRoute)
			ENDIF
		ENDIF
		
			
		//-- Timeout at end of route
		IF IS_BIT_SET(serverBD.iServerBitSet, biS_ReachedEndOfRoute)	
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneWon)
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_WaitForWinnerExpired)	
					IF NOT HAS_NET_TIMER_STARTED(serverBD.timeWaitForWinner)
						START_NET_TIMER(serverBD.timeWaitForWinner)
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER started timeWaitForWinner")
					ELSE
						IF HAS_NET_TIMER_EXPIRED(serverBD.timeWaitForWinner, 60000)
							SET_BIT(serverBD.iServerBitSet, biS_WaitForWinnerExpired)	
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER SET biS_WaitForWinnerExpired")
						ENDIF
					ENDIF
				ENDIF
				
				IF iNumPlayersTakingPart = 0 AND iNumPlayersTakingPartAtRouteEnd = 0
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_WaitForWinnerExpired)
						SET_BIT(serverBD.iServerBitSet, biS_WaitForWinnerExpired)	
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER SET biS_WaitForWinnerExpired as nobody left")
					ENDIF
				ENDIF
			ELSE
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER NOT UPDATING biS_WaitForWinnerExpired as biS_SomeoneWon")
			ENDIF
		ELSE
			
		ENDIF
		
		
		IF serverBD.eStage = eAD_IN_AREA
		#IF IS_DEBUG_BUILD
		AND NOT bWdStartWith1P
		#ENDIF
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneWon)
				IF iNumPlayersTakingPart = 1
				OR  bSomeoneWon
					#IF IS_DEBUG_BUILD
					IF NOT bWdDisableWIn
					#ENDIF
						serverBD.iRpToAward = 1900
						serverBD.iAmountToAward = 18000
						SET_BIT(serverBD.iServerBitSet, biS_SomeoneWon)
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - Set biS_SomeoneWon as iNumPlayersTakingPart = ", iNumPlayersTakingPart, "  iAmountToAward = ", serverBD.iAmountToAward, " time ", GET_CLOUD_TIME_AS_INT())
						
						IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ReachedEndOfRoute)
							SET_BIT(serverBD.iServerBitSet, biS_FinalLbUpdate)
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - Set biS_FinalLbUpdate early as biS_ReachedEndOfRoute not set time ", GET_CLOUD_TIME_AS_INT())
						ENDIF
						IF iNumPlayers = 1
							//-- iNumPlayers is the number of players remaining who at least started the event
							IF NOT IS_BIT_SET(serverBD.iServerBitSet ,biS_WonButOnlyPlayerLeft)
								SET_BIT(serverBD.iServerBitSet ,biS_WonButOnlyPlayerLeft)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - Set biS_WonButOnlyPlayerLeft as iNumPlayers = ", iNumPlayers)
							ENDIF
						ENDIF //
					#IF IS_DEBUG_BUILD
					ENDIF
					#ENDIF
				ELIF iNumPlayersTakingPart = 0
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneWon)
						IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ReachedEndOfRoute)
						OR (IS_BIT_SET(serverBD.iServerBitSet, biS_ReachedEndOfRoute) AND iNumPlayersTakingPartAtRouteEnd = 0)
							SET_BIT(serverBD.iServerBitSet, biS_SomeoneWon)
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - Set biS_SomeoneWon as iNumPlayersTakingPart = ", iNumPlayersTakingPart, " !!!")
							
							IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ReachedEndOfRoute)
								SET_BIT(serverBD.iServerBitSet, biS_FinalLbUpdate)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - Set biS_FinalLbUpdate early as biS_ReachedEndOfRoute not set time ", GET_CLOUD_TIME_AS_INT())
							ENDIF
						ENDIF
					ENDIF
				ELIF iNumPotentialPlayers = 0
					serverBD.eStage = eAD_CLEANUP
					PRINTLN("     ---------->     PENNED IN - Server stage eAD_CLEANUP - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER")
				ELSE
					IF IS_BIT_SET(serverBD.iServerBitSet, biS_ReachedEndOfRoute)
//						INT iNumWinners = SUM_BITS(serverBD.iPartStillPlayingBitset)
//						INT iTotalCash = 18000
//						INT iTotalRP = 1900
//						
//						serverBD.iAmountToAward = ROUND(TO_FLOAT(iTotalCash) / TO_FLOAT(iNumWinners))
//						
//						serverBD.iRpToAward = ROUND(TO_FLOAT(iTotalRP) / TO_FLOAT(iNumWinners))
//						SET_BIT(serverBD.iServerBitSet, biS_SomeoneWon)
//						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - Set biS_SomeoneWon as biS_ReachedEndOfRoute iNumPlayersTakingPart = ", iNumPlayersTakingPart, " iNumWinners = ",iNumWinners, " iAmountToAward = ", serverBD.iAmountToAward, " iTotalRP = ", iTotalRP)
				
					ENDIF
				ENDIF
			ENDIF
		
		#IF IS_DEBUG_BUILD
		ELSE
			IF bWdStartWith1P
				IF bSomeoneWon
					serverBD.iRpToAward = 1900
					serverBD.iAmountToAward = 18000
					SET_BIT(serverBD.iServerBitSet, biS_SomeoneWon)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - Set biS_SomeoneWon as bSomeoneWon / bWdStartWith1P , iAmountToAward = ", serverBD.iAmountToAward)
				ENDIF
			ENDIF
		#ENDIF
		ENDIF
		
		
		//--Final LB update
		IF IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneWon)
			IF NOT HAS_NET_TIMER_STARTED(serverBD.timeFinalUpdate)
				START_NET_TIMER(serverBD.timeFinalUpdate)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - START serverBD.timeFinalUpdate")
			ENDIF
			IF HAS_NET_TIMER_EXPIRED(serverBD.timeFinalUpdate, 100)
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_FinalLbUpdate)
					UPDATE_PENNED_IN_LEADERBOARD()
					SET_BIT(serverBD.iServerBitSet, biS_FinalLbUpdate)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - Set biS_FinalLbUpdate")
				ENDIF
			ENDIF
		ENDIF
				
				
		IF serverBD.eStage > eAD_IN_AREA
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_EveryoneFinished)
				IF NOT bSomeoneNotFinished
					SET_BIT(serverBD.iServerBitSet, biS_EveryoneFinished)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - Set biS_EveryoneFinished")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC






FUNC PLAYER_INDEX GET_PLAYER_WHO_WON()
	PLAYER_INDEX playerWon = INVALID_PLAYER_INDEX()
	PLAYER_INDEX playerTEmp
	INT i
	//INT iPart
	//PARTICIPANT_INDEX part
	REPEAT NUM_NETWORK_PLAYERS i
		IF playerWon = INVALID_PLAYER_INDEX()
			IF IS_BIT_SET(serverBD.iPlayerSTillPlayingBitSet, i)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] iPlayerSTillPlayingBitSet set for player i = ", i, " who is player ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(i)))
					playerTEmp = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
					IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerTEmp)
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] iPlayerSTillPlayingBitSet player is a part")
			//			part = NETWORK_GET_PARTICIPANT_INDEX(playerTEmp)
			//			iPart = NATIVE_TO_INT( part)
			//			IF NOT IS_BIT_SET(playerBD[iPart].iPlayerBitSet, biP_FailedToStayInArea) // removed for 2470599 as timing issue meant player wouldn't recognise self as winner if knocked out after server had done final update
						
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] I think this player won ", GET_PLAYER_NAME(playerTEmp))
							playerWon = playerTEmp
			//			ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT 
	
	#IF IS_DEBUG_BUILD
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InArea)
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_FailedToStayInArea)
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_OutCoronaTimeStarted)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] ********************   I SHOULD BE AMONGST THE WINNERS ***********************	")
				ENDIF
			ENDIF
		ENDIF
	#ENDIF
	
	
	RETURN playerWon
ENDFUNC

FUNC BOOL WAS_THERE_MORE_THAN_ONE_WINNER(PLAYER_INDEX &APlayerWhoWon, PLAYER_INDEX &AnotherPlayerWhoWon, INT &iNumberOfWinners)
	PLAYER_INDEX playerWon = GET_PLAYER_WHO_WON()
	PLAYER_INDEX playerTEmp
	INT i
	INT iPart
	PARTICIPANT_INDEX part
	AnotherPlayerWhoWon = INVALID_PLAYER_INDEX()
	
	IF playerWon <> INVALID_PLAYER_INDEX()
		iNumberOfWinners = 1
		REPEAT NUM_NETWORK_PLAYERS i
			IF IS_BIT_SET(serverBD.iPlayerSTillPlayingBitSet, i)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
					playerTEmp = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
					IF NOT IS_PLAYER_SCTV(playerTEmp)
						IF playerTEmp <> playerWon
							IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerTEmp)
								part = NETWORK_GET_PARTICIPANT_INDEX(playerTEmp)
								iPart = NATIVE_TO_INT( part)
								IF NOT IS_BIT_SET(playerBD[iPart].iPlayerBitSet, biP_FailedToStayInArea)
									iNumberOfWinners++
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] [WAS_THERE_MORE_THAN_ONE_WINNER] TRUE because this player ", GET_PLAYER_NAME(playerWon), " and this player ", GET_PLAYER_NAME(playerTEmp), " both won")
									IF AnotherPlayerWhoWon = INVALID_PLAYER_INDEX()
										APlayerWhoWon = playerWon
										AnotherPlayerWhoWon = playerTEmp
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] [WAS_THERE_MORE_THAN_ONE_WINNER] setting APlayerWhoWon = ", GET_PLAYER_NAME(playerWon), " setting AnotherPlayerWhoWon =  ", GET_PLAYER_NAME(playerTEmp))
									ENDIF
								//	RETURN TRUE 
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	IF iNumberOfWinners > 1
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] [WAS_THERE_MORE_THAN_ONE_WINNER] TRUE THERE WAS THIS MANY WINNERS ", iNumberOfWinners)
		RETURN TRUE
	ENDIF
	
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] [WAS_THERE_MORE_THAN_ONE_WINNER] FALSE")
	RETURN FALSE
ENDFUNC
 
FUNC BOOL NEED_TO_DO_BOSS_SHARD_FOR_MULTIPLE_PENNED_IN_WINNERS(PLAYER_INDEX playerLocal, PLAYER_INDEX playerWinner, PLAYER_INDEX playerWinner2)
	RETURN FALSE
	
	
	IF NOT GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
		RETURN FALSE
	ENDIF
	
	IF playerWinner != INVALID_PLAYER_INDEX()
		IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerWinner, playerLocal)
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] [NEED_TO_DO_BOSS_SHARD_FOR_MULTIPLE_PENNED_IN_WINNERS] TRUE BECAUSE PLAYER ", GET_PLAYER_NAME(playerWinner), " IS IN MY GANG")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF playerWinner2 != INVALID_PLAYER_INDEX()
		IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerWinner2, playerLocal)
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] [NEED_TO_DO_BOSS_SHARD_FOR_MULTIPLE_PENNED_IN_WINNERS] TRUE BECAUSE PLAYER ", GET_PLAYER_NAME(playerWinner2), " IS IN MY GANG")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DO_BOSS_SHARD_FOR_MULTIPLE_PENNED_IN_WINNERS(PLAYER_INDEX playerLocal, PLAYER_INDEX playerWinner, PLAYER_INDEX playerWinner2)
	INT iPartW1
	INT iPartW2
	
	INT iNumInMyGang
	BOOL bGotW1
	BOOL bGotW2
	INT iCashEarned

	IF playerWinner != INVALID_PLAYER_INDEX()
		IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerWinner, playerLocal)
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] [DO_BOSS_SHARD_FOR_MULTIPLE_PENNED_IN_WINNERS] THINK THIS WINNING PLAYER WAS IN MY GANG ", GET_PLAYER_NAME(playerWinner))
			IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerWinner)
				iNumInMyGang++
				iPartW1 = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(playerWinner))
				IF playerBD[iPartW1].iAmountWonForGang >= 0
					iCashEarned += playerBD[iPartW1].iAmountWonForGang
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] [DO_BOSS_SHARD_FOR_MULTIPLE_PENNED_IN_WINNERS] I EARNED THIS AMOUNT ", iCashEarned, " FROM PLAYER ", GET_PLAYER_NAME(playerWinner))
					bGotW1 = TRUE
				ELSE
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] [DO_BOSS_SHARD_FOR_MULTIPLE_PENNED_IN_WINNERS] WAITING FOR THIS PLAYER TO TELL ME HOW MUCH THEY WON ",GET_PLAYER_NAME(playerWinner))
				ENDIF
			ENDIF
		ELSE
			bGotW1 = TRUE
		ENDIF
	ELSE
		bGotW1 = TRUE
	ENDIF
	
	IF playerWinner2 != INVALID_PLAYER_INDEX()
		IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerWinner2, playerLocal)
			IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerWinner2)
				iNumInMyGang++
				iPartW2 = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(playerWinner2))
				IF playerBD[iPartW2].iAmountWonForGang >= 0
					iCashEarned += playerBD[iPartW2].iAmountWonForGang
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] [DO_BOSS_SHARD_FOR_MULTIPLE_PENNED_IN_WINNERS] I EARNED THIS AMOUNT ", playerBD[iPartW2].iAmountWonForGang, " FROM PLAYER ", GET_PLAYER_NAME(playerWinner))
					bGotW2 = TRUE
				ELSE
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] [DO_BOSS_SHARD_FOR_MULTIPLE_PENNED_IN_WINNERS] WAITING FOR THIS PLAYER TO TELL ME HOW MUCH THEY WON ",GET_PLAYER_NAME(playerWinner2))
				ENDIF
			ENDIF
		ELSE
			bGotW2 = TRUE
		ENDIF
	ELSE
		bGotW2 = TRUE
	ENDIF
	
	IF bGotW1 AND bGotW2
		IF iNumInMyGang = 1
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] [DO_BOSS_SHARD_FOR_MULTIPLE_PENNED_IN_WINNERS] DOING SHARD AS 1 of the winners was in my gang")
	//		SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_GENERIC ,"PEN_OVR","GB_FME_G1P", DEFAULT)
		ELSE
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] [DO_BOSS_SHARD_FOR_MULTIPLE_PENNED_IN_WINNERS] DOING SHARD AS 2 of the winners were in my gang")
		//	SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_GENERIC ,"PEN_OVR","GB_FME_G2P", DEFAULT)
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	IF iCashEarned =0 ENDIF
	RETURN FALSE
ENDFUNC



PROC DO_REWARD_TELEMETRY_FOR_CAGED_IN(BOOL bWon)
	IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
		IF NOT IS_BIT_SET(iBoolsBitSet2,bi2_DoneCommonTelem)
			g_sGb_Telemetry_data.sdata.m_match1 = serverBD.iHashedMac
			g_sGb_Telemetry_data.sdata.m_match2 = serverBD.iMatchHistoryId
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  [CHECK_FOR_REWARD] DO_REWARD_TELEMETRY_FOR_CAGED_IN called bWon = ", bWon, " g_sGb_Telemetry_data.sdata.m_match1 = ", g_sGb_Telemetry_data.sdata.m_match1, " g_sGb_Telemetry_data.sdata.m_match2 = ", g_sGb_Telemetry_data.sdata.m_match2)
			IF bWon
				GB_SET_COMMON_TELEMETRY_DATA_ON_END(TRUE, GB_TELEMETRY_END_WON, DEFAULT, !IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InArea))
			ELSE
				GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST, DEFAULT, !IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InArea))
			ENDIF
		ENDIF
	ENDIF
ENDPROC



//PURPOSE: Checks to see if this is the end and the player should be rewarded.
PROC CHECK_FOR_REWARD()
	INT iCash
	INT iRp
//	INT iWinningPart
//	INT iEarnedFromGoons
//	PARTICIPANT_INDEX partWon
//	INT i
//	INT iPart
//	PARTICIPANT_INDEX part
//	PLAYER_INDEX playerTEmp
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_PennedInDebug")
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD_EXTRA_DEBUG] Hit 1")
	ENDIF
	#ENDIF
	
	
	BOOL bDoBossShard
	BOOL bDoGangShard
	BOOL bDoneBossShard
	INT iLocalPartToCheck = -1
	
	TEXT_LABEL_15 tl15NoWin
	TEXT_LABEL_15 tl15IWon
	TEXT_LABEL_15 tl15PlayerWon
	TEXT_LABEL_15 tl15Over = GET_PENNED_IN_EVENT_OVER_TEXT()
	PLAYER_INDEX localPlayer = INVALID_PLAYER_INDEX()
	
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
		iLocalPartToCheck = PARTICIPANT_ID_TO_INT()
		localPlayer = PLAYER_ID()
	ELSE
		PLAYER_INDEX playerSPec = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget
		IF NETWORK_IS_PLAYER_ACTIVE(playerSPec)
			IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerSPec)
				localPlayer = playerSPec
				iLocalPartToCheck = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(playerSPec))
			ENDIF
		ENDIF
	ENDIF
	
	GANG_BOSS_MANAGE_REWARDS_DATA gbRewards
	
	
	PLAYER_INDEX playerWon, playerWon2
	IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
		IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(PLAYER_ID())
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet,biP_DoneReward)
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] - biP_DoneReward - FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION")
			
			#IF IS_DEBUG_BUILD
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_PennedInDebug")
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD_EXTRA_DEBUG] Hit 2")
			ENDIF
			#ENDIF
			EXIT
		ENDIF	
	ELSE
		IF GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet,biP_DoneReward)
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] - biP_DoneReward - GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE")
			
			#IF IS_DEBUG_BUILD
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_PennedInDebug")
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD_EXTRA_DEBUG] Hit 6")
			ENDIF
			#ENDIF
			EXIT
		ENDIF	
	ENDIF
	
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneReward)
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_PennedInDebug")
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD_EXTRA_DEBUG] Hit 3")
		ENDIF
		#ENDIF
		EXIT
	ENDIF
	IF IS_BIT_SET(serverbd.iServerBitSet, biS_EventExpired)
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneReward)
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] - biP_DoneReward - EVENT EXPIRED")
		
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_PennedInDebug")
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD_EXTRA_DEBUG] Hit 4")
		ENDIF
		#ENDIF
		
		EXIT
	ENDIF
	INT iNumWInners
	BOOL bDoDefaultDrawn
	BOOL bIwasAWinner 
	IF NOT SHOULD_UI_BE_HIDDEN()	
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_PennedInDebug")
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD_EXTRA_DEBUG] Hit 5")
		ENDIF
		#ENDIF
		
		IF iLocalPartToCheck > -1
			#IF IS_DEBUG_BUILD
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_PennedInDebug")
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD_EXTRA_DEBUG] Hit 6")
			ENDIF
			#ENDIF
		
			IF localPlayer <> INVALID_PLAYER_INDEX()
				#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_PennedInDebug")
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD_EXTRA_DEBUG] Hit 7")
				ENDIF
				#ENDIF
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_CagedInBossQuit)
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_CagedInExpired)
						IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_CagedInNotEnoughPlayers)
							IF IS_BIT_SET(playerBD[iLocalPartToCheck].iPlayerBitSet, biP_InArea) // Took part at some staget
								IF NOT IS_BIT_SET(iBoolsBitSet, biWaitForShard)
									IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_WaitForWinnerExpired)
										IF IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneWon)
										AND IS_BIT_SET(serverBD.iServerBitSet, biS_FinalLbUpdate)
											IF NOT IS_BIT_SET(iBoolsBitSet, biClearVehInvincible)
												IF NOT IS_PED_INJURED(PLAYER_PED_ID())
													IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
														IF NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
														//	SET_ENTITY_INVINCIBLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), FALSE)
															SET_BIT(iBoolsBitSet, biClearVehInvincible)
															CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Cleared veh invincible - 32")
														ENDIF
													ENDIF
												ENDIF
											ENDIF
											

											IF IS_BIT_SET(serverBD.iPlayerSTillPlayingBitSet, iLocalPartToCheck)
												IF NOT IS_BIT_SET(playerBD[iLocalPartToCheck].iPlayerBitSet, biP_FailedToStayInArea)
													bIwasAWinner = TRUE
													CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] - THINK I WAS A WINNER")
												ENDIF
											ENDIF
											
											
											IF WAS_THERE_MORE_THAN_ONE_WINNER(playerWon, playerWon2, iNumWinners)
												
												IF iNumWInners > 2
													//-- Draw with more than 2 winners
													
													IF NEED_TO_DO_BOSS_SHARD_FOR_MULTIPLE_PENNED_IN_WINNERS(localPlayer, playerWon, playerWon2)
														bDoBossShard = TRUE
														CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] -  THERE WAS A DRAW, bDoBossShard = TRUE iNumWinners = ",iNumWinners) 
													ENDIF
													
													
													IF NOT bDoBossShard
														CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] -  THERE WAS A DRAW, WILL DO DEFAULT DRAW SHARD AS THIS MANY WINNERS, ", iNumWinners)
														bDoDefaultDrawn = TRUE
													ENDIF
												ELSE
													IF iNumWinners = 2
														IF localPlayer = playerWon
														OR localPlayer = playerWon2
															//-- There was a draw, I was one of the winners

															CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] -  THERE WAS A DRAW, 2 PLAYERS, I WAS ONE OF THE WINNERS, WINNERS ARE ", GET_PLAYER_NAME(playerWon), " AND ", GET_PLAYER_NAME(playerWon2))
															TEXT_LABEL_15 tl15Draw2p = GET_PENNED_IN_TIED_WITH_PLAYER_TEXT()
															IF localPlayer = playerWon	
																// You tied with ~a~ to win the Penned In event
																//SETUP_NEW_BIG_MESSAGE_WITH_PLAYER_AND_STRING(BIG_MESSAGE_FM_EVENT_DRAW, playerWon2, "", "PEN_DRAWP", "PEN_OVR" )
																SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(GET_BIG_MESSAGE_TYPE_FOR_EVENT_DRAWN(), tl15Draw2p, GET_PLAYER_NAME(playerWon2), DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME, tl15Over)
															ELSE
																// You tied with ~a~ to win the Penned In event
																SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(GET_BIG_MESSAGE_TYPE_FOR_EVENT_DRAWN(), tl15Draw2p, GET_PLAYER_NAME(playerWon), DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME, tl15Over)
															ENDIF
															
															DO_REWARD_TELEMETRY_FOR_CAGED_IN(TRUE)
														ELSE
															//-- There was a draw, I wasn't one of the winners
															
															IF NEED_TO_DO_BOSS_SHARD_FOR_MULTIPLE_PENNED_IN_WINNERS(localPlayer, playerWon, playerWon2)
																bDoBossShard = TRUE
																CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] -  THERE WAS A DRAW WITH 2 PLAYERS, bDoBossShard = TRUE ") 
															ENDIF
															
															IF NOT bDoBossShard
																TEXT_LABEL_15 tl152pDraw = GET_PENNED_IN_TWO_PLAYERS_TIED_TEXT()
																SETUP_NEW_BIG_MESSAGE_WITH_2_PLAYERS(GET_BIG_MESSAGE_TYPE_FOR_EVENT_OVER(), INVALID_PLAYER_INDEX(), INVALID_PLAYER_INDEX(), DEFAULT, tl152pDraw, tl15Over, DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME, DEFAULT, GET_PLAYER_NAME(playerWon), GET_PLAYER_NAME(playerWon2))
																CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] -  THERE WAS A DRAW, 2 PLAYERS, I WASN'T ONE OF THE WINNERS, WINNERS ARE ", GET_PLAYER_NAME(playerWon), " AND ", GET_PLAYER_NAME(playerWon2))
															ENDIF
															
															DO_REWARD_TELEMETRY_FOR_CAGED_IN(FALSE)
														ENDIF
													ELSE
														//--SHouldn't get here!
														CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] -  THERE WAS A DRAW BUT iNumWinners = ", iNumWinners) 
													ENDIF
												ENDIF
												
			//									#IF FEATURE_GANG_BOSS
			//									IF bDoBossShard
			//										IF DO_BOSS_SHARD_FOR_MULTIPLE_PENNED_IN_WINNERS(localPlayer, playerWon, playerWon2)
			//											bDoneBossShard = TRUE
			//										ENDIF
			//									ENDIF
			//									#ENDIF
												
												IF NOT bDoBossShard
												OR (bDoBossShard AND bDoneBossShard)
													IF iLocalPartToCheck = PARTICIPANT_ID_TO_INT()
														GET_MY_CASH_AND_RP_REWARD(bIwasAWinner, iCash, iRp, iNumWinners)
														IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
															telemetryStruct.m_cashEarned = iCash 
															telemetryStruct.m_rpEarned = iRp
															g_i_cashForEndEventShard = iCash		
															IF iCash > 0
																IF USE_SERVER_TRANSACTIONS()
																	INT iScriptTransactionIndex
																	TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_AMBIENT_JOB_PENNED_IN, iCash, iScriptTransactionIndex, FALSE, TRUE) //Add new service
																ELSE
																	AMBIENT_JOB_DATA amData
																	NETWORK_EARN_FROM_AMBIENT_JOB(iCash, "AM_PENNED_IN", amData)
																ENDIF	
															ENDIF
															
															IF NOT SHOULD_DISABLE_SHARE_CASH()
																IF telemetryStruct.m_cashEarned > 0
																	SET_LAST_JOB_DATA(LAST_JOB_PENNED_IN, telemetryStruct.m_cashEarned)
																ENDIF
															ENDIF
															NEXT_RP_ADDITION_SHOW_RANKBAR()
															GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "",XPTYPE_COMPLETE, XPCATEGORY_PENNED_IN, iRp)
														ElSE
															gbRewards.iExtraCash = iCash
															gbRewards.iExtraRp = iRp
															GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_BIKER_CAGED_IN, bIwasAWinner, gbRewards)
														ENDIF
													ENDIF
													SET_BIT(iBoolsBitSet, biWaitForShard)
												ELSE
													CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [CHECK_FOR_REWARD] (2) WAITING FOR bDoBossShard = ", bDoBossShard, " bDoneBossShard = ", bDoneBossShard)
												ENDIF
											ELSE
												//-- There wasn't a draw
												IF bIwasAWinner
													//-- I won!
													
													IF iLocalPartToCheck = PARTICIPANT_ID_TO_INT()
														GET_MY_CASH_AND_RP_REWARD(TRUE,iCash,iRp)
														IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
															telemetryStruct.m_cashEarned = iCash //serverBD.iAmountToAward
															telemetryStruct.m_rpEarned = iRp //serverBD.iRpToAward
															g_i_cashForEndEventShard = iCash
															IF iCash > 0
																IF USE_SERVER_TRANSACTIONS()
																	INT iScriptTransactionIndex
																	TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_AMBIENT_JOB_PENNED_IN, iCash, iScriptTransactionIndex, FALSE, TRUE) //Add new service
																ELSE
																	AMBIENT_JOB_DATA amData
																	NETWORK_EARN_FROM_AMBIENT_JOB(iCash, "AM_PENNED_IN", amData)
																ENDIF	
															ENDIF
															IF NOT SHOULD_DISABLE_SHARE_CASH()
																IF telemetryStruct.m_cashEarned > 0
																	SET_LAST_JOB_DATA(LAST_JOB_PENNED_IN, telemetryStruct.m_cashEarned)
																ENDIF
															ENDIF
															NEXT_RP_ADDITION_SHOW_RANKBAR()
															GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "",XPTYPE_COMPLETE, XPCATEGORY_PENNED_IN, iRp)
														ELSE
															gbRewards.iExtraCash = iCash
															gbRewards.iExtraRp = iRp
															GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_BIKER_CAGED_IN, TRUE, gbRewards)
														ENDIF
													ENDIF
													tl15IWon = GET_PENNED_IN_I_WON_TEXT()
													SETUP_NEW_BIG_MESSAGE(GET_BIG_MESSAGE_TYPE_FOR_EVENT_WON() ,"PEN_WIN",tl15IWon, DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME)
												//	SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneReward)
													SET_BIT(iBoolsBitSet, biWaitForShard)
													
													DO_REWARD_TELEMETRY_FOR_CAGED_IN(TRUE)
															
													CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] -  I WON! iCash = ", iCash, " iRp = ", iRp, " <----------     ")
												ELSE
													//-- I didn't win
													IF IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneWon)
														CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN -  [CHECK_FOR_REWARD] - I DIDN'T WIN  <----------     ")
														IF IS_NET_PLAYER_OK(localPlayer)
															
															playerWon = GET_PLAYER_WHO_WON()
															
															
															IF playerWon <> INVALID_PLAYER_INDEX()
																IF NETWORK_IS_PLAYER_ACTIVE(playerWon)
			//														IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
			//															
			//															IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerWon)
			//																IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerWon, localPlayer)
			//																	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] SET bDoBossShard AS ", GET_PLAYER_NAME(playerWon), " is in my gang")
			//																	bDoBossShard = TRUE
			//																ENDIF
			//															ENDIF
			//														ENDIF
																	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN -  [CHECK_FOR_REWARD] playerWon IS VALID ", GET_PLAYER_NAME(playerWon))
																	
																	INT iTempGangID
																	STRING sOrganization
																	HUD_COLOURS hcTempGang
																	
																	IF NOT bDoBossShard
																		IF GB_IS_PLAYER_MEMBER_OF_A_GANG(playerWon)
																			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN -  [CHECK_FOR_REWARD] playerWon IS A GANG MEMBER ", GET_PLAYER_NAME(playerWon))
																			iTempGangID = GET_GANG_ID_FOR_PLAYER(playerWon)
																			IF iTempGangID > -1
																				hcTempGang = GET_HUD_COLOUR_FOR_GANG_ID(iTempGangID)
																				sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(playerWon)
																				bDoGangShard = TRUE
																				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [CHECK_FOR_REWARD] GOING TO DO GANG SHARD iTempGangID = ", iTempGangID, " sOrganization = ", sOrganization, " hcTempGang = ", ENUM_TO_INT(hcTempGang))
																			ENDIF
																		ELSE
																			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN -  [CHECK_FOR_REWARD] playerWon IS NOT A GANG MEMBER ", GET_PLAYER_NAME(playerWon))
																		ENDIF
																	ENDIF
																
																	IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
																		bDoBossShard = FALSE
																		bDoGangShard = FALSE
																	ENDIF
																	
																	IF NOT bDoBossShard	
																	AND NOT bDoGangShard
																		tl15PlayerWon = GET_PENNED_IN_PLAYER_WON_TEXT()
																		SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(GET_BIG_MESSAGE_TYPE_FOR_EVENT_OVER(), tl15PlayerWon, GET_PLAYER_NAME(playerWon), DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME, tl15Over)
																	ELIF bDoGangShard
																	//	SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_GB_GENERIC, "PEN_WONPG2", sOrganization, hcTempGang, ciFM_EVENTS_END_UI_SHARD_TIME, "")
																		tl15PlayerWon = GET_PENNED_IN_GANG_WON_TEXT()
																		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [CHECK_FOR_REWARD] GANG SHARD iTempGangID = ", iTempGangID, " sOrganization = ", sOrganization, " hcTempGang = ", ENUM_TO_INT(hcTempGang))
																		SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(GET_BIG_MESSAGE_TYPE_FOR_EVENT_OVER(), tl15Over, tl15PlayerWon, sOrganization, hcTempGang)
																	ELSE
																		IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerWon)
																			tl15PlayerWon = GET_PENNED_IN_GANGMEMBER_WON_TEXT()
																			SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_GB_GENERIC, tl15PlayerWon, GET_PLAYER_NAME(playerWon), DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME, tl15Over)
																			bDoneBossShard = TRUE
																			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] MY GOON ", GET_PLAYER_NAME(playerWon), " WON") 

																		ELSE
																			tl15PlayerWon = GET_PENNED_IN_PLAYER_WON_TEXT()
																			SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(GET_BIG_MESSAGE_TYPE_FOR_EVENT_OVER(), tl15PlayerWon, GET_PLAYER_NAME(playerWon), DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME, tl15Over)
																			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] WANT TO GIVE BOSS WINNINGS BUT playerWon ", GET_PLAYER_NAME(playerWon), " IS NOT A PART!")
																			bDoneBossShard = TRUE
																		ENDIF
																	ENDIF
																ENDIF
															ELSE
																CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] Failed to find winning player! Doing Event Over")
																tl15NoWin = GET_PENNED_IN_NOBODY_WON_TEXT()
																SETUP_NEW_BIG_MESSAGE(GET_BIG_MESSAGE_TYPE_FOR_EVENT_OVER(),tl15Over,tl15NoWin, DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME) //Nobody won the Penned In event.
															ENDIF
															
															IF NOT bDoBossShard
															OR (bDoBossShard AND bDoneBossShard)
																IF iLocalPartToCheck = PARTICIPANT_ID_TO_INT()
																	GET_MY_CASH_AND_RP_REWARD(FALSE, iCash,iRp)
																//	iCash = GET_PARTICIPATION_CASH()
																//	iRp = GET_PARTICIPATION_RP()
																	IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
																		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [CHECK_FOR_REWARD] - iCash = ", iCash, " iRp = ", iRp) 
																		
																		telemetryStruct.m_cashEarned = iCash //serverBD.iAmountToAward
																		telemetryStruct.m_rpEarned = iRp //serverBD.iRpToAward
																		g_i_cashForEndEventShard = iCash
																		
																		IF iCash > 0
																			IF USE_SERVER_TRANSACTIONS()
																				INT iScriptTransactionIndex
																				TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_AMBIENT_JOB_PENNED_IN, iCash, iScriptTransactionIndex, FALSE, TRUE) //Add new service
																			ELSE
																				AMBIENT_JOB_DATA amData
																				NETWORK_EARN_FROM_AMBIENT_JOB(iCash, "AM_PENNED_IN", amData)
																			ENDIF	
																		ENDIF
																		
																		IF NOT SHOULD_DISABLE_SHARE_CASH()
																			IF telemetryStruct.m_cashEarned > 0
																				SET_LAST_JOB_DATA(LAST_JOB_PENNED_IN, telemetryStruct.m_cashEarned)
																			ENDIF
																		ENDIF
																		
																		NEXT_RP_ADDITION_SHOW_RANKBAR()
																		GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "",XPTYPE_COMPLETE, XPCATEGORY_PENNED_IN, iRp)
																	ELSE
																		gbRewards.iExtraCash = iCash
																		gbRewards.iExtraRp = iRp
																		GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_BIKER_CAGED_IN, FALSE, gbRewards)
													
																	ENDIF
																ENDIF
														
															//	SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneReward)
																SET_BIT(iBoolsBitSet, biWaitForShard)
															ELSE
																CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [CHECK_FOR_REWARD] (1) WAITING FOR bDoBossShard = ", bDoBossShard, " bDoneBossShard = ", bDoneBossShard)
															ENDIF
															
															DO_REWARD_TELEMETRY_FOR_CAGED_IN(FALSE)
														ENDIF
													ENDIF
												ENDIF
											ENDIF
											
											//-- Draw with > 2 players
											IF bDoDefaultDrawn
												TEXT_LABEL_15 tl15Draw
												tl15Draw = GET_PENNED_IN_DRAWN_TEXT()
												IF bIwasAWinner
													SETUP_NEW_BIG_MESSAGE(GET_BIG_MESSAGE_TYPE_FOR_EVENT_DRAWN() ,tl15Over,tl15Draw) // The Penned In event was drawn.
												ELSE
													SETUP_NEW_BIG_MESSAGE(GET_BIG_MESSAGE_TYPE_FOR_EVENT_DRAWN() ,tl15Over,tl15Draw, DEFAULT)
												ENDIF
												
												DO_REWARD_TELEMETRY_FOR_CAGED_IN(FALSE)
											ENDIF
										ENDIF
									ELSE
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] NOBODY WON - biS_WaitForWinnerExpired IS SET!")
										tl15NoWin = GET_PENNED_IN_NOBODY_WON_TEXT()
										SETUP_NEW_BIG_MESSAGE(GET_BIG_MESSAGE_TYPE_FOR_EVENT_OVER(),tl15Over,tl15NoWin, DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME) //Nobody won the Penned In event.
										
										IF iLocalPartToCheck = PARTICIPANT_ID_TO_INT()
											GET_MY_CASH_AND_RP_REWARD(FALSE, iCash,iRp)
											IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
												CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [CHECK_FOR_REWARD] - iCash = ", iCash, " iRp = ", iRp) 
												
												telemetryStruct.m_cashEarned = iCash //serverBD.iAmountToAward
												telemetryStruct.m_rpEarned = iRp //serverBD.iRpToAward
												g_i_cashForEndEventShard = iCash
												
												IF iCash > 0
													IF USE_SERVER_TRANSACTIONS()
														INT iScriptTransactionIndex
														TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_AMBIENT_JOB_PENNED_IN, iCash, iScriptTransactionIndex, FALSE, TRUE) //Add new service
													ELSE
														AMBIENT_JOB_DATA amData
														NETWORK_EARN_FROM_AMBIENT_JOB(iCash, "AM_PENNED_IN", amData)
													ENDIF	
												ENDIF

												NEXT_RP_ADDITION_SHOW_RANKBAR()
												GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "",XPTYPE_COMPLETE, XPCATEGORY_PENNED_IN, iRp)
											ELSE
												gbRewards.iExtraCash = iCash
												gbRewards.iExtraRp = iRp
												GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_BIKER_CAGED_IN, FALSE, gbRewards)
											ENDIF
										ENDIF
										
										SET_BIT(iBoolsBitSet, biWaitForShard) 
									ENDIF
								ENDIF
								
								IF IS_BIT_SET(iBoolsBitSet, biWaitForShard)
									IF IS_BIT_SET(playerBD[iLocalPartToCheck].iPlayerBitSet, biP_OutCoronaTimeStarted)
										IF NOT IS_BIT_SET(playerBD[iLocalPartToCheck].iPlayerBitSet, biP_FailedToStayInArea)
											STOP_SOUND(iOutAreaSOund)
											RELEASE_SOUND_ID(iOutAreaSOund)
											CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] Stopping countdown timer")
										ENDIF
									ENDIF
									IF IS_SCREEN_FADED_IN()
									//	IF MAINTAIN_FM_EVENTS_END_UI(sEndUiVars)
											SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneReward)
											CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] Set biP_DoneReward - 27")
									//	ELSE
									//		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD]  WAITING FOR MAINTAIN_FM_EVENTS_END_UI")
									//	ENDIF
									ENDIF
								ENDIF
							ELSE
								//-- I didn't take part, just do a ticker
								IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_WaitForWinnerExpired)
									IF IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneWon)
									
										playerWon = GET_PLAYER_WHO_WON()
										IF playerWon <> INVALID_PLAYER_INDEX()
											IF NOT WAS_THERE_MORE_THAN_ONE_WINNER(playerWon, playerWon2, iNumWInners)
												TEXT_LABEL_15 tl15Won = GET_PENNED_IN_PLAYER_WON_TEXT()
												PRINT_TICKER_WITH_PLAYER_NAME_WITH_HUD_COLOUR(tl15Won, playerWon, HUD_COLOUR_WHITE)
												CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] SET biP_DoneReward - I DIDN'T TAKE PART - THIS PLAYER WON ",GET_PLAYER_NAME(playerWon))
											ELSE
												IF iNumWInners = 2
													TEXT_LABEL_15 tl152PWon = GET_PENNED_IN_TWO_PLAYERS_WON_TEXT()
													PRINT_TICKER_WITH_TWO_PLAYER_NAMES_WITH_HUD_COLOUR(tl152PWon, playerWon, playerWon2, HUD_COLOUR_WHITE, HUD_COLOUR_WHITE)
													CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] SET biP_DoneReward - I DIDN'T TAKE PART - TWO WINNERS - THIS PLAYER WON ",GET_PLAYER_NAME(playerWon), " AND THIS PLAYER WON ", GET_PLAYER_NAME(playerWon2))
												ELSE
													TEXT_LABEL_15 tl15Draw
													tl15Draw = GET_PENNED_IN_DRAWN_TEXT()
													PRINT_TICKER(tl15Draw)
													CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] SET biP_DoneReward - I DIDN'T TAKE PART - MULTIPLE WINNERS iNumWInners = ", iNumWInners)
												ENDIF
											ENDIF
										ELSE
											CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] SET biP_DoneReward - I DIDN'T TAKE PART - DON'T KNOW WHO WON!")
										ENDIF
										SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneReward)
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] SET biP_DoneReward - I DIDN'T TAKE PART")
										IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
											FM_EVENT_SETUP_DO_MINIMUM_PARTICIPATION_HELP(FALSE)
										ELSE
											DO_REWARD_TELEMETRY_FOR_CAGED_IN(FALSE)
										ENDIF
									ENDIF
								ELSE
									SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneReward)
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] SET biP_DoneReward - I DIDN'T TAKE PART - NOBODY WON")
									IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
										FM_EVENT_SETUP_DO_MINIMUM_PARTICIPATION_HELP(FALSE)
									ELSE
										DO_REWARD_TELEMETRY_FOR_CAGED_IN(FALSE)
									ENDIF
								ENDIF
							ENDIF
						ELSE
							//-- Not enough players for caged in
							IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneReward)
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneReward)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] SET biP_DoneReward - biS_CagedInNotEnoughPlayers ")
								IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_MINIMAL
									SETUP_NEW_BIG_MESSAGE(GET_BIG_MESSAGE_TYPE_FOR_EVENT_OVER(),tl15Over,"CAG_NOP", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME) //Nobody won the Penned In event.
								ENDIF
								DO_REWARD_TELEMETRY_FOR_CAGED_IN(FALSE)
								
							ENDIF
						ENDIF
					ELSE
						//-- Caged in expired
						IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneReward)
							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneReward)
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] SET biP_DoneReward - Caged In start time expired ")
							IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_MINIMAL
								SETUP_NEW_BIG_MESSAGE(GET_BIG_MESSAGE_TYPE_FOR_EVENT_OVER(),tl15Over,"CAG_NOP", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME) //Nobody won the Penned In event.
							ENDIF
							DO_REWARD_TELEMETRY_FOR_CAGED_IN(FALSE)
						ENDIF
					ENDIF
				ELSE
					//-- Caged in boss quit
					IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneReward)
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneReward)
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] SET biP_DoneReward - Caged In boss quit ")
						IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_MINIMAL
							IF NOT IS_BIT_SET(iBoolsBitSet3, bi3_OnLaunchGang)
								SETUP_NEW_BIG_MESSAGE(GET_BIG_MESSAGE_TYPE_FOR_EVENT_OVER(),tl15Over,"CAG_PRES", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME) //Nobody won the Penned In event.
							ENDIF
						ENDIF 
						DO_REWARD_TELEMETRY_FOR_CAGED_IN(FALSE)
					ENDIF
				ENDIF
			ELSE
				IF IS_PLAYER_SCTV(PLAYER_ID())
					IF IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneWon)
					AND IS_BIT_SET(serverBD.iServerBitSet, biS_FinalLbUpdate)
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneReward)
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] SET biP_DoneReward - I'm SCTV  - 1")
					ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_WaitForWinnerExpired)
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneReward)
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] SET biP_DoneReward - I'm SCTV  - 2")
					ENDIF
							
				ENDIF
			ENDIF
		ENDIF
	ELSE
		//-- UI should be hidden
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneReward)
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneWon)
			OR IS_BIT_SET(serverBD.iServerBitSet, biS_WaitForWinnerExpired)
			OR IS_BIT_SET(serverBD.iServerBitSet, biS_CagedInExpired)
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneReward)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [CHECK_FOR_REWARD] SET biP_DoneReward - I 'VE HIDDEN THE REWARDS")
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

//PURPOSE: Controls the ped blips displaying for when they shoot
PROC CONTROL_PED_LOOP()
	INT i
	REPEAT MAX_MW_PEDS i
		UPDATE_ENEMY_NET_PED_BLIP(serverBD.niMwPed[i], MwPedBlipData[i], DEFAULT, DEFAULT, TRUE, FALSE)
		IF DOES_BLIP_EXIST(MwPedBlipData[i].blipId)
			SET_BLIP_NAME_FROM_TEXT_FILE(MwPedBlipData[i].blipId, "UW_BLIP2")
		ENDIF
		
		IF DOES_BLIP_EXIST(MwPedBlipData[i].VehicleBlipID)
			SET_BLIP_COLOUR(MwPedBlipData[i].VehicleBlipID, BLIP_COLOUR_RED)
		ENDIF
	ENDREPEAT
ENDPROC





/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MISSION PROCS                  //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE:
///    Start a countdown when at least one player is in the vehicle. Mission will automatically start when timer ends
PROC MAINTAIN_START_MISSION_TIMER_SERVER()
	BOOL bTimerShouldStart
	
//	bTimerShouldStart = IS_BIT_SET(serverBD.iServerBitSet, biS_AtLeastOnePlayerOnMission)
	bTimerShouldStart = IS_BIT_SET(serverBD.iServerBitSet, biS_AtLeastTwoPlayersOnMission)
	
	#IF IS_DEBUG_BUILD 
	IF bWdStartWith1P 
		bTimerShouldStart = IS_BIT_SET(serverBD.iServerBitSet, biS_AtLeastOnePlayerOnMission)
	ENDIF
	#ENDIF
	
	
	IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
		bTimerShouldStart = TRUE
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_UseShortCountDown)
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ResetForShortCOuntdown)
			IF (GET_EVENT_START_TIME()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeMissionStart)) >= GET_QUICK_LAUNCH_TIME()
				RESET_NET_TIMER(serverbd.timeMissionStart)
				SET_BIT(serverBD.iServerBitSet, biS_ResetForShortCOuntdown)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  SET biS_ResetForShortCOuntdown <----------     ")
			ENDIF
		ENDIF
	ENDIF
	
	INT iTimeToUse = GET_EVENT_START_TIME()
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_ResetForShortCOuntdown)
		iTimeToUse = GET_QUICK_LAUNCH_TIME()
	ENDIF
	
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_EventExpired)
		IF bTimerShouldStart 
			IF HAS_NET_TIMER_STARTED(serverBD.timeEventExpired)
				RESET_NET_TIMER(serverBD.timeEventExpired)
			ENDIF
			
			IF NOT HAS_NET_TIMER_STARTED(serverbd.timeMissionStart)
				START_NET_TIMER(serverbd.timeMissionStart)
				
				// If we have already processed some start time, grab a timer from the past so we continue where we left off (paused the timer)
				IF serverBD.iMissionStartOffset > 0
					TIME_DATATYPE newTimer = GET_TIME_OFFSET(GET_NETWORK_TIME(), -1*serverBD.iMissionStartOffset)
					SET_NET_TIMER_TO_SAME_VALUE_AS_TIMER(serverbd.timeMissionStart, newTimer)
					
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  START - Initialise to previous time offset  <----------     serverBD.iMissionStartOffset = ", serverBD.iMissionStartOffset) NET_NL()
					
					serverBD.iMissionStartOffset = 0
				ENDIF				
				
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  START timeMissionStart    <----------     ") NET_NL()
			ELSE
				IF NOT IS_BIT_SET(serverbd.iServerBitSet, biS_MissionStartExpired)
					IF (iTimeToUse-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeMissionStart)) <= 0
						IF IS_BIT_SET(serverBD.iServerBitSet, biS_AtLeastTwoPlayersOnMission)
						#IF IS_DEBUG_BUILD 
						OR bWdStartWith1P
						#ENDIF
							SET_BIT(serverbd.iServerBitSet, biS_MissionStartExpired)
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  MISSION START TIME EXPIRED (2)   <----------     ") NET_NL()
						ELSE
							SET_BIT(serverBD.iServerBitSet, biS_CagedInExpired)
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  set biS_CagedInExpired asstart timer expired but not enpugh players   <----------     ") NET_NL()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			// If we have a player remaining in the area, save out the time we have processed.
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_AtLeastOnePlayerOnMission)
				IF HAS_NET_TIMER_STARTED(serverbd.timeMissionStart)
					serverBD.iMissionStartOffset = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeMissionStart)
				ENDIF
			ELSE
				serverBD.iMissionStartOffset = 0
			ENDIF
		
			IF HAS_NET_TIMER_STARTED(serverbd.timeMissionStart)
				RESET_NET_TIMER(serverbd.timeMissionStart)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  RESET timeMissionStart    <----------     ") NET_NL()
			ENDIF
			
			
			IF NOT HAS_NET_TIMER_STARTED(serverBD.timeEventExpired)
				START_NET_TIMER(serverBD.timeEventExpired)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  START timeEventExpired    <----------     ") NET_NL()
			ELSE
				IF HAS_NET_TIMER_EXPIRED(serverBD.timeEventExpired, GET_EVENT_TIME_OUT_TIME())
					SET_BIT(serverBD.iServerBitSet, biS_EventExpired)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  SET biS_EventExpired!    <----------     ") NET_NL()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC
PROC MAINTAIN_START_MISSION_TIMER_CLIENT()
	
	BOOL bTimerShouldStart
	
	bTimerShouldStart = IS_BIT_SET(serverBD.iServerBitSet, biS_AtLeastTwoPlayersOnMission)
//	bTimerShouldStart = IS_BIT_SET(serverBD.iServerBitSet, biS_AtLeastOnePlayerOnMission)
	
	#IF IS_DEBUG_BUILD 
	IF bWdStartWith1P 
		bTimerShouldStart = IS_BIT_SET(serverBD.iServerBitSet, biS_AtLeastOnePlayerOnMission)
	ENDIF
	#ENDIF
	
	IF NOT OK_TO_DISPLAY_HUD_FOR_MODE()
		EXIT
	ENDIF //
	
	INT iTimeToUse = GET_EVENT_START_TIME()
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_ResetForShortCOuntdown)
		iTimeToUse = GET_QUICK_LAUNCH_TIME() // TIME_MISSION_START_SHORT
	ENDIF
	
	IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
		bTimerShouldStart = TRUE
	ENDIF
	
	
	TEXT_LABEL_15 tl15Obj
	IF bTimerShouldStart //IS_BIT_SET(serverbd.iServerBitSet, biS_AtLeastOnePlayerInVeh)
		INT iMaxPossiblePlayers = GET_MAX_NUMBER_OF_PLAYERS_FOR_LAUNCH()
		IF iMaxPossiblePlayers > iLocalPotentialPlayerCount
			iMaxPossiblePlayers = iLocalPotentialPlayerCount
		ENDIF 
		
		INT iCurrentPlayers
		
		IF iCurrentPlayers <> serverBD.iPlayersRemaining		
			iCurrentPlayers = serverBD.iPlayersRemaining
		//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [MAINTAIN_START_MISSION_TIMER_CLIENT] iCurrentPlayers updated to ",iCurrentPlayers) 
		ENDIF
		

		
		IF NOT IS_PLAYER_SCTV(PLAYER_ID())
			tl15Obj = GET_PENNED_IN_ABOUT_TO_START_OBJ_TEXT()
			
			IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
				IF NOT Is_This_The_Current_Objective_Text(tl15Obj)
					Print_Objective_Text(tl15Obj) //~HUD_COLOUR_NET_PLAYER2~Penned In ~s~is about to start.
				ENDIF
			ELSE
				IF NOT Is_This_The_Current_Objective_Text(tl15Obj) 
					Print_Objective_Text_With_Coloured_Text_Label(tl15Obj, "CAG_BLIP", hclLaunchGang)
				ENDIF
			ENDIF
		ENDIF
		
		
		
		IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
			IF (iTimeToUse-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeMissionStart)) >= 0
			//	BOTTOM_RIGHT_UI_STARTING_EVENT_TIMER((iTimeToUse-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeMissionStart)), GET_START_TIMER_NAME_FOR_EVENT())
				HANDLE_AMBIENT_EVENT_START_COUNTDOWN_AUDIO((iTimeToUse-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeMissionStart)))
				BOTTOM_RIGHT_UI_DOUBLE_NUM_TIMER(iCurrentPlayers, iMaxPossiblePlayers, (iTimeToUse-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeMissionStart)), DEFAULT, GET_START_TIMER_NAME_FOR_EVENT())
			ELSE
				BOTTOM_RIGHT_UI_DOUBLE_NUM_TIMER(iCurrentPlayers, iMaxPossiblePlayers, 0, DEFAULT, GET_START_TIMER_NAME_FOR_EVENT())
				//BOTTOM_RIGHT_UI_STARTING_EVENT_TIMER(0, GET_START_TIMER_NAME_FOR_EVENT())

			ENDIF
		ELSE
			IF (iTimeToUse-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeMissionStart)) >= 0
		//	BOTTOM_RIGHT_UI_STARTING_EVENT_TIMER((iTimeToUse-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeMissionStart)), GET_START_TIMER_NAME_FOR_EVENT())
				HANDLE_AMBIENT_EVENT_START_COUNTDOWN_AUDIO((iTimeToUse-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeMissionStart)))
				BOTTOM_RIGHT_UI_DOUBLE_NUM_TIMER(iCurrentPlayers, iMaxPossiblePlayers, (iTimeToUse-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeMissionStart)), DEFAULT, "SYG_GOTOTIMER")
			ELSE
				BOTTOM_RIGHT_UI_DOUBLE_NUM_TIMER(iCurrentPlayers, iMaxPossiblePlayers, 0, DEFAULT, "SYG_GOTOTIMER")
				//BOTTOM_RIGHT_UI_STARTING_EVENT_TIMER(0, GET_START_TIMER_NAME_FOR_EVENT())

			ENDIF
		ENDIF
		
	ELSE
		IF NOT IS_PLAYER_SCTV(PLAYER_ID())
			tl15Obj = GET_PENNED_IN_ABOUT_TO_START_OBJ_TEXT()
			IF Is_This_The_Current_Objective_Text(tl15Obj)
				Clear_Any_Objective_Text_From_This_Script()
			ENDIF
		ENDIF
	ENDIF		
ENDPROC

PROC MAINTAIN_PLAYER_DISABLE_AIRSTRIKES()
	FLOAT fMyDistFromPen
	IF NOT HAS_NET_TIMER_STARTED(timeDisableAirstrike)
	OR (HAS_NET_TIMER_STARTED(timeDisableAirstrike) AND HAS_NET_TIMER_EXPIRED(timeDisableAirstrike, 5000))
		
		fMyDistFromPen = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vMyBlipPos)
	//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN MAINTAIN_PLAYER_DISABLE_AIRSTRIKES MY DIST FROM PEN ", fMyDistFromPen)
		
		IF NOT IS_AIRSTRIKE_DISABLED_FOR_PENNED_IN(PLAYER_ID())
			IF fMyDistFromPen <= 700.0
				SET_PLAYER_DISABLED_AIRSTRIKE_FOR_PENNED_IN(TRUE)
			ENDIF
		ELSE
			IF fMyDistFromPen > 700.0
				SET_PLAYER_DISABLED_AIRSTRIKE_FOR_PENNED_IN(FALSE)
			ENDIF
		ENDIF
		
		RESET_NET_TIMER(timeDisableAirstrike)
		START_NET_TIMER(timeDisableAirstrike)
	ENDIF
	
ENDPROC

PROC MAINTAIN_PENNED_IN_MUSIC()
	
	IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InArea)
		OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaAsPassenger)
			IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_DoneMusic)
				IF serverBD.eStage = eAD_IN_AREA
					IF TRIGGER_MUSIC_EVENT("PENNED_IN_START_MUSIC")
						SET_BIT(iBoolsBitSet2, bi2_DoneMusic)
						NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  MAINTAIN_PENNED_IN_MUSIC - PENNED_IN_START_MUSIC    <----------     ") NET_NL()
					ENDIF
					
				ENDIF
			ELSE
				IF IS_BIT_SET(iBoolsBitSet2, bi2_Travelled70perc)
					IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_ResetMusic)
						IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_Started70PerMusic)
							IF TRIGGER_MUSIC_EVENT("PENNED_IN_70_PERCENT")
								SET_BIT(iBoolsBitSet2, bi2_Started70PerMusic)
								NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  MAINTAIN_PENNED_IN_MUSIC - PENNED_IN_START_MUSIC STARTED 70 PERCENT MUSIC")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_ResetMusic)
				IF IS_BIT_SET(iBoolsBitSet2, bi2_DoneMusic)
					IF playerBD[PARTICIPANT_ID_TO_INT()].eStage	> eAD_IN_AREA
						IF TRIGGER_MUSIC_EVENT("PENNED_IN_STOP_MUSIC")
							SET_BIT(iBoolsBitSet2, bi2_ResetMusic)						
							NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  MAINTAIN_PENNED_IN_MUSIC - PENNED_IN_STOP_MUSIC    <----------     ") NET_NL()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InArea)
		OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaAsPassenger)
			IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_DoneMusic)
				IF serverBD.eStage = eAD_IN_AREA
					IF TRIGGER_MUSIC_EVENT("CAGED_IN_START_MUSIC")
						SET_BIT(iBoolsBitSet2, bi2_DoneMusic)
						SET_USER_RADIO_CONTROL_ENABLED(FALSE)
						NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  MAINTAIN_PENNED_IN_MUSIC - PENNED_IN_START_MUSIC    SET_USER_RADIO_CONTROL_ENABLED(FALSE) <----------     ") NET_NL()
					ENDIF
					
				ENDIF
			ELSE
				IF IS_BIT_SET(iBoolsBitSet2, bi2_Travelled70perc)
					IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_ResetMusic)
						IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_Started70PerMusic)
							IF TRIGGER_MUSIC_EVENT("CAGED_IN_70_PERCENT")
								SET_BIT(iBoolsBitSet2, bi2_Started70PerMusic)
								NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  MAINTAIN_PENNED_IN_MUSIC - PENNED_IN_START_MUSIC STARTED 70 PERCENT MUSIC")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_ResetMusic)
				IF IS_BIT_SET(iBoolsBitSet2, bi2_DoneMusic)
					IF playerBD[PARTICIPANT_ID_TO_INT()].eStage	> eAD_IN_AREA
						IF TRIGGER_MUSIC_EVENT("CAGED_IN_STOP_MUSIC")
							SET_BIT(iBoolsBitSet2, bi2_ResetMusic)		
							SET_USER_RADIO_CONTROL_ENABLED(TRUE)
							NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  MAINTAIN_PENNED_IN_MUSIC - PENNED_IN_STOP_MUSIC   SET_USER_RADIO_CONTROL_ENABLED(TRUE) <----------     ") NET_NL()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC



/// PURPOSE:
///    Provides the scripter with a handle to the countdown UI.
PROC REQUEST_RACE_COUNTDOWN_UI(RACE_COUNTDOWN_UI & uiToRequest)
	uiToRequest.uiCountdown = REQUEST_SCALEFORM_MOVIE("COUNTDOWN")
ENDPROC

/// PURPOSE:
///    Lets the scripter know if the countdown UI is ready for use. Should be done in streaming checks.
FUNC BOOL HAS_RACE_COUNTDOWN_UI_LOADED(RACE_COUNTDOWN_UI & uiToCheck)
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(uiToCheck.uiCountdown)
	
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Private minigame countdown UI func.
PROC SET_RACE_COUNTDOWN_UI_NUMBER(RACE_COUNTDOWN_UI & uiToUpdate, INT iNum)
	INT iR, iG, iB, iA
	GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(uiToUpdate.uiCountdown, "SET_MESSAGE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("NUMBER")
			ADD_TEXT_COMPONENT_INTEGER(ABSI(iNum))
		END_TEXT_COMMAND_SCALEFORM_STRING()
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iR)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iG)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iB)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

FUNC BOOL UPDATE_RACE_COUNTDOWN_UI(RACE_COUNTDOWN_UI & uiToUpdate, BOOL bWaitExtraSec = TRUE, BOOL bSkipOnButtonPress = FALSE, BOOL bCancelTimerAtEnd = TRUE)
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaAsPassenger)
		IF SHOULD_UI_BE_HIDDEN()
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF HAS_RACE_COUNTDOWN_UI_LOADED(uiToUpdate)

		// Make sure we have a timer.
		IF NOT IS_TIMER_STARTED(uiToUpdate.CountdownTimer)
			RESTART_TIMER_NOW(uiToUpdate.CountdownTimer)
		ENDIF
		
		// Always draw the movie.
		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
		IF HAS_SCALEFORM_MOVIE_LOADED(uiToUpdate.uiCountdown)
			DRAW_SCALEFORM_MOVIE_FULLSCREEN(uiToUpdate.uiCountdown, 255, 255, 255, 100)
		ENDIF
		
		// Get the timer values.
		INT iTimerVal = FLOOR(GET_TIMER_IN_SECONDS(uiToUpdate.CountdownTimer))
		INT iSeconds = ABSI(iTimerVal - 3)
		BOOL bForceEnd = FALSE
		
		IF IS_BITMASK_AS_ENUM_SET(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_1)
			IF uiToUpdate.iFrameCount >= 5
				IF NOT IS_BITMASK_AS_ENUM_SET(uiToUpdate.iBitFlags, CNTDWN_UI_SoundGo)
					PLAY_SOUND_FRONTEND(-1, "GO", "HUD_MINI_GAME_SOUNDSET", FALSE) //PLAY_SOUND_FRONTEND(-1, "GO", "HUD_MINI_GAME_SOUNDSET")
					SET_BITMASK_AS_ENUM(uiToUpdate.iBitFlags, CNTDWN_UI_SoundGo)
					STOP_STREAM()
				ENDIF
			ELSE
				uiToUpdate.iFrameCount++
			ENDIF
		ENDIF
		
		IF NOT IS_BITMASK_AS_ENUM_SET(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_Go)
			// See if we're due to update.
			IF (iSeconds = 3) AND NOT IS_BITMASK_AS_ENUM_SET(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_3)
				SET_BITMASK_AS_ENUM(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_3)
				
				PLAY_SOUND_FRONTEND(-1, "3_2_1", "HUD_MINI_GAME_SOUNDSET", FALSE)
				SET_RACE_COUNTDOWN_UI_NUMBER(uiToUpdate, iSeconds)
				
			ELIF (iSeconds = 2) AND NOT IS_BITMASK_AS_ENUM_SET(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_2)
				SET_BITMASK_AS_ENUM(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_2)
				PLAY_SOUND_FRONTEND(-1, "3_2_1", "HUD_MINI_GAME_SOUNDSET", FALSE)
				SET_RACE_COUNTDOWN_UI_NUMBER(uiToUpdate, iSeconds)
				
			ELIF (iSeconds = 1) AND NOT IS_BITMASK_AS_ENUM_SET(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_1)
				SET_BITMASK_AS_ENUM(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_1)
				PLAY_SOUND_FRONTEND(-1, "3_2_1", "HUD_MINI_GAME_SOUNDSET", FALSE)
				SET_RACE_COUNTDOWN_UI_NUMBER(uiToUpdate, iSeconds)
				IF NOT IS_PLAYER_SCTV(PLAYER_ID())
					Print_Objective_Text("PEN_AREA") // Stay in the marked ~y~area.
				ENDIF
			ELIF (iSeconds = 0) AND NOT IS_BITMASK_AS_ENUM_SET(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_Go)
			

		
				//STOP_AUDIO_SCENE("RACES_RADIO_MUTE_scene") //Fade radio back up *1238581
				SET_BITMASK_AS_ENUM(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_Go)
				// Play GO noise
				/*
				IF NOT IS_BITMASK_AS_ENUM_SET(uiToUpdate.iBitFlags, CNTDWN_UI_SoundGo)
					//PLAY_SOUND_FRONTEND(-1, "GO", "HUD_MINI_GAME_SOUNDSET") //PLAY_SOUND_FRONTEND(-1, "GO", "HUD_MINI_GAME_SOUNDSET")
					SET_BITMASK_AS_ENUM(uiToUpdate.iBitFlags, CNTDWN_UI_SoundGo)
				ENDIF
				*/
				
				INT iR, iG, iB, iA
				GET_HUD_COLOUR(HUD_COLOUR_GREEN, iR, iG, iB, iA)
				
				BEGIN_SCALEFORM_MOVIE_METHOD(uiToUpdate.uiCountdown, "SET_MESSAGE")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CNTDWN_GO")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iR)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iG)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iB)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
				END_SCALEFORM_MOVIE_METHOD()
				
				REMOVE_AREA_BLIP()
				ADD_AREA_BLIP(GET_START_COORDS_FOR_AREA())
				
				SET_BIT(iBoolsBitSet, biChangeColour)
				
				IF NOT IS_BIT_SET(iBoolsBitSet, biDoneTimeCycle)
				//	SET_TRANSITION_TIMECYCLE_MODIFIER("TrevorColorCodeBasic", 0.1)
					ANIMPOSTFX_PLAY("pennedIn", 0, TRUE)
					SET_BIT(iBoolsBitSet, biDoneTimeCycle)
					
					IF IS_AUDIO_SCENE_ACTIVE("MP_Player_Outside_Bubble_Scene")
						STOP_AUDIO_SCENE("MP_Player_Outside_Bubble_Scene")
					ENDIF
					
					START_AUDIO_SCENE("MP_Player_Inside_Bubble_Scene")
					//iMyTimeCycleModifier = GET_TIMECYCLE_TRANSITION_MODIFIER_INDEX()
					//CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - SET TIMECYCLE (1) iMyTimeCycleModifier = ", iMyTimeCycleModifier)
				ENDIF
				IF NOT bWaitExtraSec
					bForceEnd = TRUE
				ENDIF
			ENDIF
		ELSE
			IF (iSeconds = 1)
				bForceEnd = TRUE
			ENDIF
		ENDIF
		
		//check for a button press to skip the countdown
		IF (bSkipOnButtonPress AND IS_CUTSCENE_SKIP_BUTTON_PRESSED()) OR iTimerVal > 5
			bForceEnd = TRUE
		ENDIF
		
		// If we've been ordered to leave, do so.
		IF bForceEnd
			IF bCancelTimerAtEnd
				// Reset our vars.
				uiToUpdate.iBitFlags = 0
				CANCEL_TIMER(uiToUpdate.CountdownTimer)
			ENDIF
			STOP_AUDIO_SCENE("RACES_RADIO_MUTE_scene") //Fade radio back up *1238581
			PRINTLN(" RACES - STOP_AUDIO_SCENE 'RACES_RADIO_MUTE_scene'")
			START_AUDIO_SCENE("MP_RACE_GENERAL_SCENE") // B*1625318
			
			// also B*1625318
			INT iParticipant
		    REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
					PLAYER_INDEX playerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
					IF playerId <> PLAYER_ID()
						PED_INDEX newPedIdx = GET_PLAYER_PED(playerId)
						IF IS_PED_IN_ANY_VEHICLE(newPedIdx)
							VEHICLE_INDEX newVehIdx = GET_VEHICLE_PED_IS_IN(newPedIdx)
							ADD_ENTITY_TO_AUDIO_MIX_GROUP(newVehIdx, "MP_RACE_NPC_CAR_Group", 0.0)
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT

			
			// We're done.
			RETURN TRUE
		ENDIF
	ELSE
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN [MAINTAIN_START_321_COUNTDOWN] WAITING FOR UPDATE_RACE_COUNTDOWN_UI")
	ENDIF

	RETURN FALSE
ENDFUNC

PROC MAINTAIN_START_321_COUNTDOWN()
//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN [MAINTAIN_START_321_COUNTDOWN] Hit 1")
	IF NOT IS_BIT_SET(iBoolsBitSet, biReqCountdown)
		REQUEST_RACE_COUNTDOWN_UI(countdown)
		SET_BIT(iBoolsBitSet, biReqCountdown)
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN [MAINTAIN_START_321_COUNTDOWN] SET biReqCountdown")
	ENDIF
	
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneCOuntdown)
	//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN [MAINTAIN_START_321_COUNTDOWN] Hit 2")
		IF HAS_RACE_COUNTDOWN_UI_LOADED(countdown)
		//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN [MAINTAIN_START_321_COUNTDOWN] Hit 3")
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_Start321Countdown)
			//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN [MAINTAIN_START_321_COUNTDOWN] Hit 4")
				IF UPDATE_RACE_COUNTDOWN_UI(countdown)
				//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN [MAINTAIN_START_321_COUNTDOWN] Hit 5")
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneCOuntdown)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN [MAINTAIN_START_321_COUNTDOWN] SET biP_DoneCOuntdown")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_DISABLE_CARGOBOB_ATTACH()
	VEHICLE_INDEX vehTemp
	IF NOT IS_BIT_SET(iBoolsBitSet, biBlockCargobob)
		IF playerBD[PARTICIPANT_ID_TO_INT()].eStage	= eAD_COUNTDOWN
		OR playerBD[PARTICIPANT_ID_TO_INT()].eStage	= eAD_IN_AREA
		OR (FM_EVENT_IS_PLAYER_WAITING_FOR_FM_EVENT_TO_START(PLAYER_ID()) AND IS_LOCAL_PLAYER_IN_AREA() AND IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InArea) AND NOT SHOULD_UI_BE_HIDDEN())
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
					vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF NETWORK_HAS_CONTROL_OF_ENTITY(vehTemp)
						SET_VEHICLE_AUTOMATICALLY_ATTACHES(vehTemp, FALSE)
						SET_BIT(iBoolsBitSet, biBlockCargobob)
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Set biBlockCargobob")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF playerBD[PARTICIPANT_ID_TO_INT()].eStage	>= eAD_OFF_MISSION
				OR IS_BIT_SET(iBoolsBitSet, biEventUiHidden)
					vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF IS_VEHICLE_DRIVEABLE(vehTemp)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(vehTemp)
							SET_VEHICLE_AUTOMATICALLY_ATTACHES(vehTemp, TRUE)
							CLEAR_BIT(iBoolsBitSet, biBlockCargobob)
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - CLEAR biBlockCargobob")
						ENDIF
					ENDIF
				ENDIF
			ELSE
				CLEAR_BIT(iBoolsBitSet, biBlockCargobob)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - CLEAR biBlockCargobob as not in a vehicle")
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

VECTOR vPosStored
FLOAT fHeadingStored

PROC DETACH_PLAYER_FROM_HELI()
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF IS_ENTITY_ATTACHED(PLAYER_PED_ID())
				PRINTLN("[CS_HELI_SPEC] DEATCH_PLAYER_FROM_HELI  ")
				DETACH_ENTITY(PLAYER_PED_ID())
			ENDIF
		ENDIF
	ENDIF
ENDPROC

INT iFadeProgress
CONST_INT ciHELI_FADE_TIME 500

PROC CLEANUP_HELI_SPECTATE_MODE()
	IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_CleanedUpSpectate)
	
		ENABLE_INTERACTION_MENU()
		SET_FAKE_SPECTATOR_MODE(FALSE)
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCTV_HELI_HLP")
			CLEAR_HELP()
		ENDIF
		
		DO_SCTV_HELI_CLEANUP(serverBD.heliStruct, TRUE, IS_BIT_SET(serverBD.iServerBitSet, biS_EveryoneFinished), FALSE)
		IF NOT IS_PLAYER_SCTV(PLAYER_ID())
			CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_IsSpectating)
			CLEAR_BIT(GlobalplayerBD[NETWORK_PLAYER_ID_TO_INT()].iSpecInfoBitset, SPEC_INFO_BS_SPECTATOR_CAM_IS_RUNNING)
			CLEAR_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iFmAmbientEventBitSet, ciSET_PLAYER_IS_PENNED_IN_SPECTATOR)
			CLEAR_MP_DECORATOR_BIT(PLAYER_ID(), MP_DECORATOR_BS_INVALID_TO_SPECTATE)
		ENDIF
		
		PRINTLN("[CS_HELI_SPEC], CLEANUP_HELI_SPECTATE_MODE ")
	
		SET_BIT(iBoolsBitSet2, bi2_CleanedUpSpectate)
	ENDIF
ENDPROC

FUNC BOOL MOVE_PLAYER_BACK_AFTER_HELI_SPECTATE()

	IF (IS_SPECTATING_WITH_HELI()
	AND NOT IS_VECTOR_ZERO(vPosStored))
	OR IS_BIT_SET(iBoolsBitSet2, bi2_ExitSCTVFaded)
	
		PRINTLN("[CS_HELI_SPEC] MOVE_PLAYER_BACK_AFTER_HELI_SPECTATE, iFadeProgress = ", iFadeProgress)
	
		SWITCH iFadeProgress
		
			CASE 0
			
				IF NOT IS_SCREEN_FADED_OUT()
				AND NOT IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_OUT(ciHELI_FADE_TIME)
				ELSE
					
					iFadeProgress++
				ENDIF
			BREAK
			
			CASE 1
				IF IS_SCREEN_FADED_OUT()
					SET_BIT(iBoolsBitSet2, bi2_ExitSCTVFaded)
					DETACH_PLAYER_FROM_HELI()
					CLEANUP_HELI_SPECTATE_MODE()
					
					iFadeProgress++
				ENDIF
			BREAK
			
			CASE 2
				IF NET_WARP_TO_COORD(vPosStored, fHeadingStored)
			
					iFadeProgress++
				ENDIF
			BREAK
			
			CASE 3
				IF NOT IS_SCREEN_FADING_IN()
				AND NOT IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_IN(ciHELI_FADE_TIME)
				
					iFadeProgress++
				ENDIF
			BREAK
			
			CASE 4
				IF IS_SCREEN_FADED_IN()
				
					iFadeProgress++
				ENDIF
			BREAK
			
			CASE 5
			
				RETURN TRUE
			BREAK			
		ENDSWITCH
	ELSE
		PRINTLN("[CS_HELI_SPEC] MOVE_PLAYER_BACK_AFTER_HELI_SPECTATE, BAIL " )
		
		RETURN TRUE
	ENDIF
	
	PRINTLN("[CS_HELI_SPEC] MOVE_PLAYER_BACK_AFTER_HELI_SPECTATE, RETURN FALSE " )
	
	RETURN FALSE
ENDFUNC

//Cleanup Mission Data
PROC SCRIPT_CLEANUP()
	
	IF IS_BIT_SET(iBoolsBitSet3, bi3_SetMaxWanted)
		SET_MAX_WANTED_LEVEL(5)
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  CLEANUP MISSION SET_MAX_WANTED_LEVEL(5)")
		CLEAR_BIT(iBoolsBitSet3, bi3_SetMaxWanted)
	ENDIF
	
//	IF serverBD.eStage = eAD_CLEANUP
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_EveryoneFinished)
	OR IS_BIT_SET(serverbd.iServerBitSet, biS_EventExpired)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.heliStruct.niPilot)
			IF NOT IS_PED_INJURED(NET_TO_PED(serverBD.heliStruct.niPilot))
				IF NETWORK_HAS_CONTROL_OF_ENTITY(NET_TO_PED(serverBD.heliStruct.niPilot))
					IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.heliStruct.niHeli))
						CLEAR_PED_TASKS(NET_TO_PED(serverBD.heliStruct.niPilot))
						IF DOES_ENTITY_EXIST(GET_PLAYER_PED(serverBD.heliStruct.playerToSpectate))
						AND NOT IS_ENTITY_DEAD(GET_PLAYER_PED(serverBD.heliStruct.playerToSpectate))
							TASK_HELI_MISSION(NET_TO_PED(serverBD.heliStruct.niPilot), NET_TO_VEH(serverBD.heliStruct.niHeli), NULL, GET_PLAYER_PED(serverBD.heliStruct.playerToSpectate), <<0.0,0.0,0.0>>, MISSION_FLEE, 20.0, 1000.0, -1.0, 60, 50)
						ELSE
							SET_PED_FLEE_ATTRIBUTES(NET_TO_PED(serverBD.heliStruct.niPilot), FA_DISABLE_EXIT_VEHICLE, TRUE) 
							TASK_SMART_FLEE_COORD(NET_TO_PED(serverBD.heliStruct.niPilot), GET_ENTITY_COORDS(NET_TO_PED(serverBD.heliStruct.niPilot)), 10000.0, 999999)
						ENDIF
						SET_PED_KEEP_TASK(NET_TO_PED(serverBD.heliStruct.niPilot), TRUE)
						PRINTLN("[CS_HELI_SPEC] TASK_HELI_MISSION = MISSION_FLEE")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InArea)
		OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaAsPassenger)
			IF BIK_IS_FORMATION_RESTRICTED()
				BIK_CLEAR_FORMATION_RESTRICTED()
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [MAINTAIN_CLEAR_FORMATION] CLEANUP MISSION BIK_CLEAR_FORMATION_RESTRICTED()")
			ENDIF
		ENDIF

	ENDIF
	
	//-- Reset vehicle proofs
	IF IS_BIT_SET(iBoolsBitSet3, bi3_VehExpProof)
		IF DOES_ENTITY_EXIST(vehProofs)
			IF IS_VEHICLE_DRIVEABLE(vehProofs)
				SET_ENTITY_PROOFS(vehProofs, FALSE, FALSE, FALSE, FALSE, FALSE)
				CLEAR_BIT(iBoolsBitSet3, bi3_VehExpProof)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [MAINTAIN_SET_MY_VEHICLE_PROOFS] CLEANUP MISSION Clear veh proofs")
			ENDIF
		ENDIF
	ENDIF
	
	// Cleanup spectating heli logic
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_SPECTATING_WITH_HELI()
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			CLEANUP_HELI_SPECTATE_MODE() // if not done previously
			PRINTLN("[CS_HELI_SPEC], IS_SPECTATING_WITH_HELI, END ")
		ENDIF
	ENDIF
	
	SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_PENNED_IN, FALSE)
	SET_FREEMODE_FLOW_COMMS_DISABLED(FALSE)
	
	IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_CommonCleanup)
		
		IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
			GB_COMMON_BOSS_MISSION_CLEANUP()
		ELSE
			IF NETWORK_IS_GAME_IN_PROGRESS() 
				COMMON_FREEMODE_AMBIENT_EVENTS_CLEAN_UP(FMMC_TYPE_PENNED_IN, DEFAULT, (IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneReward)))
			ELSE
				COMMON_FREEMODE_AMBIENT_EVENTS_CLEAN_UP(FMMC_TYPE_PENNED_IN, DEFAULT, FALSE)
			ENDIF
		ENDIF
		
	ELSE
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  CLEANUP MISSION - NOT CALLING COMMON_FREEMODE_AMBIENT_EVENTS_CLEAN_UP AS bi2_CommonCleanup SET")
		
		IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
			GB_COMMON_BOSS_MISSION_CLEANUP()
		ENDIF
	ENDIF

	IF GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()) = relPennedInPlayer
		SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), relMyFmGroup)
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  CLEANUP MISSION - RESTORED MY FM REL GROUP     <----------     ") NET_NL()
	ENDIF
	
	IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
		IF IS_BIT_SET(g_FM_EVENT_VARS.iEventBitSet, ciFM_EVENT_EVENT_WANTED_SUPRESSED_BY_EVENT)
			FM_EVENTS_CLEANUP_WANTED_OVERRIDES()
		ENDIF
	ENDIF
	
	IF NATIVE_TO_INT(PLAYER_ID()) != -1
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_PENNED_IN
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = -1
		ENDIF
	ENDIF

	IF IS_BIT_SET(iBoolsBitSet, biDoneTimeCycle)
	//	CLEAR_TIMECYCLE_MODIFIER()
		ANIMPOSTFX_STOP("pennedIn")
		ANIMPOSTFX_PLAY("PennedInOut", 0, FALSE)
		CLEAR_BIT(iBoolsBitSet, biDoneTimeCycle)
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - SCRIPT_CLEANUP - CLEAR_TIMECYCLE_MODIFIER()")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("MP_Player_Inside_Bubble_Scene")
		STOP_AUDIO_SCENE("MP_Player_Inside_Bubble_Scene")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("MP_Player_Outside_Bubble_Scene")
		STOP_AUDIO_SCENE("MP_Player_Outside_Bubble_Scene")
	ENDIF
	
	RELEASE_RACE_COUNTDOWN_UI(countdown)		
			
	REMOVE_UW_REL_GROUPS()
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		MAINTAIN_DISABLE_CARGOBOB_ATTACH()
	ENDIF
	
	ACTIVATE_KILL_TICKERS(FALSE)
	
	FORCE_DRAW_CHALLENGE_DPAD_LBD(FALSE, FMMC_TYPE_PENNED_IN)
	
	CLEAR_CONTROL_LIGHT_EFFECT(PLAYER_CONTROL)
	IF IS_BIT_SET(iBoolsBitSet2, bi2_DoneMusic)
		IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_ResetMusic)
			SET_USER_RADIO_CONTROL_ENABLED(TRUE)
			TRIGGER_MUSIC_EVENT("MP_MC_STOP")
		//	TRIGGER_MUSIC_EVENT("PENNED_IN_FIRA")
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  CLEANUP MISSION - MP_MC_STOP SET_USER_RADIO_CONTROL_ENABLED(TRUE)    <----------     ") NET_NL()	
		ENDIF
	ENDIF
	
	
		IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
			IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
				telemetryStruct.m_endReason             =AE_END_FORCED
			ELIF SHOULD_BAIL_FM_EVENT_DUE_TO_LOW_SESSION_NUMBERS()
				telemetryStruct.m_endReason             =AE_END_LOW_NUMBERS
			ELSE
				IF GET_PLAYER_WHO_WON() = PLAYER_ID()
					telemetryStruct.m_endReason             =AE_END_WON
				ELSE
					telemetryStruct.m_endReason             =AE_END_LOST
				ENDIF
			ENDIF
				
			telemetryStruct.m_uid0 = serverBD.iHashedMac
			telemetryStruct.m_uid1 = serverBD.iMatchHistoryId
			telemetryStruct.m_playersLeftInProgress = serverBD.iPlayersLeftInProgress
			telemetryStruct.m_playersParticipating = serverBD.iPeakQualifiers
			telemetryStruct.m_timeTakenToComplete	= GET_CLOUD_TIME_AS_INT() - telemetryStruct.m_startTime
			
			PROCESS_CURRENT_AMBIENT_EVENT_PLAYSTATS(telemetryStruct)
		ELSE
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  CLEANUP MISSION - calling PROCESS_CURRENT_BOSS_CHALLENGE_PLAYSTATS")
			 g_sBIK_Telemetry_data.m_PlayersLeftInProgress = serverBD.iPlayersLeftInProgress
			g_sGb_Telemetry_data.sdata.m_match1 = serverBD.iHashedMac
			g_sGb_Telemetry_data.sdata.m_match2 = serverBD.iMatchHistoryId
			PROCESS_CURRENT_BOSS_CHALLENGE_PLAYSTATS(g_sGb_Telemetry_data.sdata)
			
			
			IF NOT IS_BIT_SET(iBoolsBitSet2,bi2_DoneCommonTelem)
				IF PARTICIPANT_ID_TO_INT() > -1
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  CLEANUP MISSION GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LEFT)")
					GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LEFT, DEFAULT, !IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InArea))
				ENDIF
			ENDIF
		ENDIF
	
	
	IF NOT SHOULD_DISABLE_SHARE_CASH()
		IF telemetryStruct.m_cashEarned > 0
//			SET_LAST_JOB_DATA(LAST_JOB_PENNED_IN, telemetryStruct.m_cashEarned)
			IF telemetryStruct.m_endReason             =AE_END_WON
				
				IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
					SET_DO_FM_EVENT_SHARE_CASH_HELP_IF_NEEDED()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	
	RESET_UNLOCK_AIRPORT_GATES()
	
	//FM_EVENT_SET_PLAYER_IS_CRITICAL_TO_FM_EVENT(FALSE)
	
	// Clean up our flag for the killstrip
	SET_PLAYER_HAS_BEEN_KNOCKED_OUT_OF_MODE(FALSE)
	
	SET_PLAYER_DISABLED_AIRSTRIKE_FOR_PENNED_IN(FALSE)
	
	IF FM_EVENT_IS_PLAYER_WAITING_FOR_FM_EVENT_TO_START(PLAYER_ID())
		FM_EVENT_SET_PLAYER_WAITING_FOR_FM_EVENT_TO_START(FALSE)
	ENDIF
	
	// 2416571: Prevent ped being knocked off bikes while waiting for event to start
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND NOT CAN_KNOCK_PED_OFF_VEHICLE(PLAYER_PED_ID())
		SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_DEFAULT)
	ENDIF
		
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
		//	SET_ENTITY_INVINCIBLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), FALSE)
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Cleared veh invincible - 10")
		ENDIF
		
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PlayersDontDragMeOutOfCar, FALSE)
	ENDIF
	
	SET_PLAYER_DISABLED_PASSIVE_MODE_FOR_PENNED_IN(FALSE)
	
	//IF IS_BIT_SET(iBoolsBitSet, biSetPlayerInvincible)
		CLEAR_BIT(iBoolsBitSet, biSetPlayerInvincible)
	//	SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - CLEAR biSetPlayerInvincible - 3")
	//ENDIF
	
	
	INT iCLeanupPlayer
	REPEAT NUM_NETWORK_PLAYERS iCLeanupPlayer
		IF IS_BIT_SET(iOtherPlayerGhostedPlayerBitset, iCLeanupPlayer)
			IF NETWORK_IS_PLAYER_ACTIVE(INT_TO_PLAYERINDEX(iCLeanupPlayer))		
				SET_REMOTE_PLAYER_AS_GHOST(INT_TO_PLAYERINDEX(iCLeanupPlayer), FALSE)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN CLEANUP SET_REMOTE_PLAYER_AS_GHOST FALSE PLAYER ID ", iCLeanupPlayer) 
			ELSE
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN CLEANUP WANT TO CALL SET_REMOTE_PLAYER_AS_GHOST FALSE BUT PLAYER NOT ACTIVE! PLAYER ID ", iCLeanupPlayer) 
			ENDIF
		ENDIF
	ENDREPEAT
	
	SET_PLAYER_DEFAULT_FRIENDLY_FIRE_OPTION()
	
	
											
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableVehicleCombat, FALSE)
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableVehicleCombat, FALSE) 9")
	
	IF IS_BIT_SET(iBoolsBitSet3, bi3_ExpProof)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
			CLEAR_BIT(iBoolsBitSet3, bi3_ExpProof)
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE) 2")
		ENDIF
	ENDIF
											
	#IF IS_DEBUG_BUILD
	IF MPGlobalsAmbience.iPennedInLoc != -1
		MPGlobalsAmbience.iPennedInLoc = -1
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  CLEANUP MPGlobalsAmbience.iPennedInLoc = -1")
	ENDIF
	#ENDIF
	
	
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  CLEANUP MISSION      <----------     ") NET_NL()
	#IF IS_DEBUG_BUILD NET_LOG("SCRIPT_CLEANUP")	#ENDIF
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

PROC RESET_THE_LEADERBOARD_STRUCT()
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  RESET_THE_LEADERBOARD_STRUCT      <----------     ") NET_NL()
	THE_LEADERBOARD_STRUCT sDpadDownLeaderBoardData
	INT iLoop
	REPEAT NUM_NETWORK_PLAYERS iLoop
		g_DpadDownLeaderBoardData[iLoop] = sDpadDownLeaderBoardData
	ENDREPEAT
ENDPROC
//Do necessary pre game start ini.
PROC PROCESS_PRE_GAME( MP_MISSION_DATA missionScriptArgs )
	RESET_THE_LEADERBOARD_STRUCT()
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission) ,  missionScriptArgs)	//NUM_NETWORK_PLAYERS, missionScriptArgs)	//
	//NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, PARTICIPANT_ID_TO_INT())  
	
	//Reserve Entities
//	RESERVE_NETWORK_MISSION_PEDS(MAX_MW_PEDS)
	
	#IF FEATURE_STUNT_FM_EVENTS
		RESERVE_NETWORK_MISSION_VEHICLES(MAX_AIRCRAFT_TO_SPAWN)
	#ENDIF
	
	#IF NOT FEATURE_STUNT_FM_EVENTS
		RESERVE_NETWORK_MISSION_VEHICLES(1)
	#ENDIF

	// This makes sure the net script is active, waits untill it is.
	HANDLE_NET_SCRIPT_INITIALISATION()

	//common event proccess pre game
	
	IF IS_CAGED_IN_LAUNCHING()
		GB_COMMON_BOSS_MISSION_PREGAME()
	ELSE
		COMMON_FREEMODE_AMBIENT_EVENTS_PROCESS_PRE_GAME(FMMC_TYPE_PENNED_IN)
	ENDIF
		
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		#IF IS_DEBUG_BUILD NET_LOG("PENNED IN -  FAILED TO RECEIVE INITIAL NETWORK BROADCAST")	#ENDIF
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  MISSION END - FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP D     <----------     ") NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		// This script will not be paused if another script calls PAUSE_GAME
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()

			INT i
			REPEAT MAX_PLAYERS i
				serverBD.piPlayers[i] = INVALID_PLAYER_INDEX()
			ENDREPEAT
			
			PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBD.iHashedMac, serverBD.iMatchHistoryId)
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [PROCESS_PRE_GAME] serverBD.iHashedMac = ", serverBD.iHashedMac, " serverBD.iMatchHistoryId = ", serverBD.iMatchHistoryId)
		ENDIF
		
		telemetryStruct.m_playersNotified = NETWORK_GET_TOTAL_NUM_PLAYERS()
		telemetryStruct.m_notifiedTime = GET_CLOUD_TIME_AS_INT()
		
		
		SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_PENNED_IN, TRUE)
		
		
		SETUP_UW_REL_GROUPS()
		
		IF SHOULD_UI_BE_HIDDEN()
			NET_PRINT_TIME() NET_PRINT("     ---------->     PENNED IN -  EVENT DISABLED VIA PI MENU     <----------     ") NET_NL()
		ENDIF
		
		
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_PennedInDebug")
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - Running with sc_PennedInDebug")
		ENDIF
		#ENDIF
		playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_INI
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  PRE_GAME DONE     <----------     ") NET_NL()
	ELSE
		#IF IS_DEBUG_BUILD NET_LOG("PENNED IN -  NETWORK_IS_GAME_IN_PROGRESS = FALSE")	#ENDIF
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  MISSION END - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP Q     <----------     ") NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
	OR IS_CAGED_IN_LAUNCHING()
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - STARTING	(CAGED IN)	*******************************************************")
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN")
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - TUNABLES:	")
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - g_sMPTunables.BIKER_DISABLE_CAGED_IN_ROUTE_0 = ", IS_ROUTE_DISABLED(6))
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - g_sMPTunables.BIKER_DISABLE_CAGED_IN_ROUTE_1 = ", IS_ROUTE_DISABLED(7))
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - g_sMPTunables.BIKER_DISABLE_CAGED_IN_ROUTE_2 = ", IS_ROUTE_DISABLED(8))
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - g_sMPTunables.BIKER_DISABLE_CAGED_IN_ROUTE_3 = ", IS_ROUTE_DISABLED(9))
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - g_sMPTunables.BIKER_DISABLE_CAGED_IN_ROUTE_4 = ", IS_ROUTE_DISABLED(10))
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - GET_EVENT_START_TIME() 		- g_sMPTunables.iPenned_In_Event_Start_Countdown_Time = ", GET_EVENT_START_TIME())
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - GET_EVENT_TIME_OUT_TIME() 		- g_sMPTunables.iBIKER_CAGED_IN_TIME_LIMIT = ", GET_EVENT_TIME_OUT_TIME())
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - GET_PEN_START_SIZE() 			- g_sMPTunables.fBIKER_CAGED_IN_AREA_START_SIZE = ", GET_PEN_START_SIZE())
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - GET_PEN_END_SIZE() 			- g_sMPTunables.fBIKER_CAGED_IN_AREA_END_SIZE = ", GET_PEN_END_SIZE())
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - GET_MAX_NUMBER_OF_PLAYERS_FOR_LAUNCH() 			- g_sMPTunables.BIKER_CAGED_IN_MAX_PLAYERS = ",  g_sMPTunables.iBIKER_CAGED_IN_MAX_PLAYERS)
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN")
	ELSE
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - STARTING		*******************************************************")
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN")
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - TUNABLES:	")
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - GET_EVENT_START_TIME() 		- g_sMPTunables.iPenned_In_Event_Start_Countdown_Time = ", GET_EVENT_START_TIME())
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - GET_EVENT_TIME_OUT_TIME() 		- g_sMPTunables.iPenned_In_Event_Expiry_Time = ", GET_EVENT_TIME_OUT_TIME())
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - GET_PEN_START_SIZE() 			- g_sMPTunables.fPenned_In_Pen_Starting_Size = ", GET_PEN_START_SIZE())
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - GET_PEN_END_SIZE() 			- g_sMPTunables.fPenned_In_Pen_Finishing_Size = ", GET_PEN_END_SIZE())
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - GET_PEN_MAX_SPEED_MODIFIER() 	- g_sMPTunables.fPenned_In_Pen_Movement_Max_Speed_Modifier = ", GET_PEN_MAX_SPEED_MODIFIER())
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - GET_PEN_START_SPEED_MODIFIER() - g_sMPTunables.fPenned_In_Pen_Movement_Start_Speed_Modifier = ", GET_PEN_START_SPEED_MODIFIER())
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - GET_PEN_END_SPEED_MODIFIER() 	- g_sMPTunables.fPenned_In_Pen_Movement_End_Speed_Modifier = ", GET_PEN_END_SPEED_MODIFIER())
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - GET_PEN_SPEED_MULTIPLIER() 	- g_sMPTunables.fPenned_In_Pen_movement_Speed_Modifier = ", GET_PEN_SPEED_MULTIPLIER())
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - GET_PEN_SHRINK_MULTIPLIER() 	- g_sMPTunables.fPENNED_IN_SIZE_MODIFIER = ", GET_PEN_SHRINK_MULTIPLIER())
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - GET_QUICK_LAUNCH_PERCENTAGE() 	- g_sMPTunables.iPenned_In_Percentage_of_session_required_for_quick_launch = ", GET_QUICK_LAUNCH_PERCENTAGE())
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - GET_QUICK_LAUNCH_TIME() 		- g_sMPTunables.iPenned_In_Quick_Launch_Timer = ", GET_QUICK_LAUNCH_TIME())
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - GET_KNOCKED_OUT_TIME() 		- g_sMPTunables.iPenned_In_Knockout_timer = ", GET_KNOCKED_OUT_TIME())
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - GET_MAX_NUMBER_OF_PLAYERS_FOR_LAUNCH() - g_sMPTunables.iPenned_In_Max_Players = ", GET_MAX_NUMBER_OF_PLAYERS_FOR_LAUNCH())
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - SHOULD_DISABLE_SHARE_CASH() 	- g_sMPTunables.bPenned_In_Disable_Share_Cash = ", SHOULD_DISABLE_SHARE_CASH())
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - IS_ROUTE_DISABLED(0) 			- g_sMPTunables.bPenned_In_Disable_variant_1 = ", IS_ROUTE_DISABLED(0))
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - IS_ROUTE_DISABLED(1) 			- g_sMPTunables.bPenned_In_Disable_variant_2 = ", IS_ROUTE_DISABLED(1))
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - IS_ROUTE_DISABLED(2) 			- g_sMPTunables.bPenned_In_Disable_variant_3 = ", IS_ROUTE_DISABLED(2))
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - IS_ROUTE_DISABLED(3) 			- g_sMPTunables.bPenned_In_Disable_variant_4 = ", IS_ROUTE_DISABLED(3))
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - IS_ROUTE_DISABLED(4) 			- g_sMPTunables.bPenned_In_Disable_variant_5 = ", IS_ROUTE_DISABLED(4))
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN")
	ENDIF
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_PennedInSizeCheck")
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - SC_PENNEDINSIZECHECK RUNNING")
	ENDIF

	IF GET_COMMANDLINE_PARAM_EXISTS("sc_weaponWheel")
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ----------> [sc_weaponWheel] -sc_weaponWheel running")
	ENDIF

	#ENDIF
ENDPROC

FUNC BOOL OK_TO_START_HELI_SPECTATE()
	
	IF NOT DO_HELI_AND_PILOT_NET_IDS_EXIST(serverBD.heliStruct)
	
		PRINTLN("[CS_HELI_SPEC] OK_TO_START_HELI_SPECTATE, DO_HELI_AND_PILOT_NET_IDS_EXIST ")
	
		RETURN FALSE
	ENDIF
	
	// Check the local player actually started Penned In
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InArea)
	
		PRINTLN("[CS_HELI_SPEC] OK_TO_START_HELI_SPECTATE, eAD_OFF_MISSION ")
	
		RETURN FALSE
	ENDIF
	
	IF playerBD[PARTICIPANT_ID_TO_INT()].eStage < eAD_OFF_MISSION
	
		PRINTLN("[CS_HELI_SPEC] OK_TO_START_HELI_SPECTATE, eAD_OFF_MISSION ")
		PRINTLN("[CS_HELI_SPEC] OK_TO_START_HELI_SPECTATE, eStage = ", playerBD[PARTICIPANT_ID_TO_INT()].eStage)
	
		RETURN FALSE
	ENDIF
	
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
	
		PRINTLN("[CS_HELI_SPEC] OK_TO_START_HELI_SPECTATE, IS_NET_PLAYER_OK ")
	
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneWon)
	
		PRINTLN("[CS_HELI_SPEC] OK_TO_START_HELI_SPECTATE, biS_SomeoneWon ")
	
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_EveryoneFinished)
	
		PRINTLN("[CS_HELI_SPEC] OK_TO_START_HELI_SPECTATE, biS_EveryoneFinished ")
	
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_EventExpired)
	
		PRINTLN("[CS_HELI_SPEC] OK_TO_START_HELI_SPECTATE, biS_EventExpired ")
	
		RETURN FALSE
	ENDIF
	
	IF serverBD.heliStruct.playerToSpectate = INVALID_PLAYER_INDEX()
	
		PRINTLN("[CS_HELI_SPEC] OK_TO_START_HELI_SPECTATE, INVALID_PLAYER_INDEX ")
	
		RETURN FALSE
	ENDIF
	
	IF NOT IS_NET_PLAYER_OK(serverBD.heliStruct.playerToSpectate)
	
		PRINTLN("[CS_HELI_SPEC] OK_TO_START_HELI_SPECTATE, playerToSpectate ")
	
		RETURN FALSE
	ENDIF
	
	IF serverBD.heliStruct.playerToSpectate = PLAYER_ID()
	
		PRINTLN("[CS_HELI_SPEC] OK_TO_START_HELI_SPECTATE, serverBD.heliStruct.playerToSpectate = PLAYER_ID() ")
	
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_DoneKnockedOut)
		
		PRINTLN("[CS_HELI_SPEC] OK_TO_START_HELI_SPECTATE, BIT NOT SET = (iBoolsBitSet2, bi2_DoneKnockedOut)")
		
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_CORONA()
	
		PRINTLN("[CS_HELI_SPEC] OK_TO_START_HELI_SPECTATE, IS_PLAYER_IN_CORONA")
		
		RETURN FALSE
	ENDIF
	
	IF IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(PLAYER_PED_ID())
	
		PRINTLN("[CS_HELI_SPEC] OK_TO_START_HELI_SPECTATE, IS_ENTITY_ATTACHED_TO_ANY_VEHICLE")
	
		RETURN FALSE
	ENDIF
	
	IF serverBD.iPlayersRemaining < MIN_ACTIVE_PLAYERS_FOR_HELI_SPECTATE
	
	#IF IS_DEBUG_BUILD
	AND NOT bWdDisableWIn
	#ENDIF
	
		PRINTLN("[CS_HELI_SPEC] OK_TO_START_HELI_SPECTATE, MIN_ACTIVE_PLAYERS_FOR_HELI_SPECTATE ", serverBD.iPlayersRemaining)
	
		RETURN FALSE
	ENDIF

	PRINTLN("[CS_HELI_SPEC] OK_TO_START_HELI_SPECTATE, TRUE ")
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_SPECTATING_END()
	
	IF HAS_PLAYER_QUIT_HELI_SPECTATE(heliStructClient)
	
		PRINTLN("[CS_HELI_SPEC] SHOULD_SPECTATING_END, HAS_PLAYER_QUIT_HELI_SPECTATE ")
	
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneWon)
	
		PRINTLN("[CS_HELI_SPEC] SHOULD_SPECTATING_END, biS_SomeoneWon ")
	
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_EveryoneFinished)
	
		PRINTLN("[CS_HELI_SPEC] SHOULD_SPECTATING_END, biS_EveryoneFinished ")
	
		RETURN TRUE
	ENDIF
	
	IF SHOULD_FM_EVENT_DROP_TO_IDLE_STATE()
	
		PRINTLN("[CS_HELI_SPEC] SHOULD_SPECTATING_END, SHOULD_FM_EVENT_DROP_TO_IDLE_STATE ")
	
		RETURN TRUE
	ENDIF
	
	IF SHOULD_UI_BE_HIDDEN(FALSE)
	
		PRINTLN("[CS_HELI_SPEC] SHOULD_SPECTATING_END, SHOULD_UI_BE_HIDDEN ")
		
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
	
		PRINTLN("[CS_HELI_SPEC] SHOULD_SPECTATING_END, IS_PLAYER_SCTV ")
	
		RETURN TRUE
	ENDIF
	
	IF serverBD.heliStruct.playerToSpectate = INVALID_PLAYER_INDEX()
	
		PRINTLN("[CS_HELI_SPEC] SHOULD_SPECTATING_END, playerToSpectate, INVALID_PLAYER_INDEX ")
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

INT iHeliClientProgress
CONST_INT ciCLIENT_HELI_WAIT 	0
CONST_INT ciCLIENT_HELI_RUN 	1
CONST_INT ciCLIENT_HELI_END		2

// Client logic for the spectating heli
PROC MAINTAIN_SPECTATOR_CHOPPER_CLIENT()

	PED_INDEX pedFollow
	VECTOR vPointCamsPos
	BOOL bShowButtons 
	bShowButtons = TRUE
	BOOL bNoButtonToPress
	bNoButtonToPress = TRUE
	
	IF iHeliClientProgress = ciCLIENT_HELI_RUN
	AND NOT IS_PLAYER_IN_CORONA()
		HANDLE_QUIT_HELI_SPECTATE(heliStructClient)
	ENDIF
	
	SWITCH iHeliClientProgress
	
		CASE ciCLIENT_HELI_WAIT
		
			IF OK_TO_START_HELI_SPECTATE()
			
				PRINTLN("[CS_HELI_SPEC] MAINTAIN_SPECTATOR_CHOPPER_CLIENT, iHeliClientProgress = ciCLIENT_HELI_RUN ")
			
				iHeliClientProgress++
			ENDIF
		BREAK
		
		CASE ciCLIENT_HELI_RUN
		
			// Grab coords to follow
			pedFollow = GET_PLAYER_PED(serverBD.heliStruct.playerToSpectate)
				
			// Grab coords for the camera to point at	
			IF DOES_ENTITY_EXIST(pedFollow)
			AND NOT IS_ENTITY_DEAD(pedFollow)
				vPointCamsPos = GET_ENTITY_COORDS(pedFollow)
				heliStructClient.vPosFollow = vPointCamsPos
			ELSE
				IF NOT IS_VECTOR_ZERO(heliStructClient.vPosFollow)
					vPointCamsPos = heliStructClient.vPosFollow
				ENDIF
			ENDIF
			
			heliStructClient.eHeli = NET_TO_ENT(serverBD.heliStruct.niHeli)

			IF DOES_ENTITY_EXIST(heliStructClient.eHeli)
				IF NOT IS_ENTITY_DEAD(heliStructClient.eHeli)
					
					// Store player pos before we warp
					IF NOT SHOULD_SPECTATING_END()
						IF IS_VECTOR_ZERO(vPosStored)
							IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
								IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
									vPosStored = GET_ENTITY_COORDS(PLAYER_PED_ID())
									fHeadingStored = GET_ENTITY_HEADING(PLAYER_PED_ID())
									PRINTLN("[CS_HELI_SPEC] MAINTAIN_SPECTATOR_CHOPPER_CLIENT, vPosStored = ", vPosStored, " fHeadingStored = ", fHeadingStored)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					SET_ENTITY_LOCALLY_INVISIBLE(heliStructClient.eHeli)
					
					IF (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState = RESPAWN_STATE_PLAYING) // Because net_spawn cleans up NET_SET_PLAYER_CONTROL
					
						// Player/chopper housekeeping
						IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_IsSpectating) 
							SET_NIGHTVISION(FALSE)
							IF NOT GET_FAKE_SPECTATOR_MODE()
								SET_FAKE_SPECTATOR_MODE(TRUE)
							ENDIF
							NETWORK_ALLOW_REMOTE_ATTACHMENT_MODIFICATION(heliStructClient.eHeli, TRUE)
							ATTACH_ENTITY_TO_ENTITY(PLAYER_PED_ID(), heliStructClient.eHeli, GET_ENTITY_BONE_INDEX_BY_NAME(heliStructClient.eHeli, "chassis"), <<0,0,0>>, <<0,0,0>>)
							PRINTLN("[CS_HELI_SPEC] MAINTAIN_SPECTATOR_CHOPPER_CLIENT, bi2_Spectating_Heli_On, vPointCamsPos = ", vPointCamsPos)
							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_IsSpectating)	
							SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iSpecInfoBitset, SPEC_INFO_BS_SPECTATOR_CAM_IS_RUNNING)
							SET_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iFmAmbientEventBitSet, ciSET_PLAYER_IS_PENNED_IN_SPECTATOR)
							SET_MP_DECORATOR_BIT(PLAYER_ID(), MP_DECORATOR_BS_INVALID_TO_SPECTATE)
							SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW_IF_NEEDED()
						ENDIF
						
						// Control off
						IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ControlOff) 
							NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE)
							SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
							PRINTLN("[CS_HELI_SPEC] MAINTAIN_SPECTATOR_CHOPPER_CLIENT, biP_ControlOff ")
							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ControlOff)
						ENDIF
					
					
						// Hide hud elements during spectate				
						HIDE_HUD_DURING_HELI_SPECTATE()
						REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
						// Handle quitting spectate
						IF SHOULD_SPECTATING_END()
						
							NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)

							iHeliClientProgress++
							PRINTLN("[CS_HELI_SPEC] MAINTAIN_SPECTATOR_CHOPPER_CLIENT, = ciCLIENT_HELI_END ")
						ELSE
								
							// Run the spectating heli logic
							RUN_HELI_CAM_LOGIC_CLIENT(serverBD.heliStruct, heliStructClient, heliStructClient.eHeli, vPointCamsPos, bNoButtonToPress, bShowButtons)
							
							// Quit Help text
							IF bShowButtons
								IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_Spectating_Colour)
								AND IS_OK_TO_PRINT_FREEMODE_HELP(FALSE, DEFAULT, DEFAULT, FALSE)
									// "You are now spectating. Press ~INPUT_FRONTEND_CANCEL~ to quit."
									PRINT_HELP_NO_SOUND("SCTV_HELI_HLP", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
									IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
										SET_FREEMODE_EVENT_HELP_BACKGROUND()
									ENDIF
									PRINTLN("[CS_HELI_SPEC] PRINT_HELP_NO_SOUND(SCTV_HELI_HLP, DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)")
									SET_BIT(iBoolsBitSet2, bi2_Spectating_Colour)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						PRINTLN("[CS_HELI_SPEC] MAINTAIN_SPECTATOR_CHOPPER_CLIENT - (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState = ", GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE ciCLIENT_HELI_END
		
			PRINTLN("[CS_HELI_SPEC] MAINTAIN_SPECTATOR_CHOPPER_CLIENT, ciCLIENT_HELI_END")
		BREAK
	ENDSWITCH
ENDPROC

PROC SET_PLAYER_TO_SPECTATE()

	PLAYER_INDEX PlayerId
	PLAYER_INDEX playerToSpectate
	playerToSpectate = INVALID_PLAYER_INDEX()
	
	INT i
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
//			IF PlayerId <> PLAYER_ID()
				IF IS_NET_PLAYER_OK(PlayerId)
					IF IS_BIT_SET(playerBD[i].iPlayerBitSet, biP_InArea)
						IF NOT IS_BIT_SET(playerBD[i].iPlayerBitSet, biP_FailedToStayInArea)
							IF NOT IS_BIT_SET(playerBD[i].iPlayerBitSet, biP_IsSpectating) 
						
								playerToSpectate = PlayerId
							ENDIF
						ENDIF
					ENDIF
				ENDIF
//			ENDIF
		ENDIF
	ENDREPEAT
	
	
	IF serverBD.heliStruct.playerToSpectate <> playerToSpectate
		serverBD.heliStruct.playerToSpectate = playerToSpectate
		
		#IF IS_DEBUG_BUILD
		IF serverBD.heliStruct.playerToSpectate <> INVALID_PLAYER_INDEX()
		AND IS_NET_PLAYER_OK(serverBD.heliStruct.playerToSpectate)
			PRINTLN("[CS_HELI_SPEC] SET_PLAYER_TO_SPECTATE, playerToSpectate = ", GET_PLAYER_NAME(playerToSpectate))
		ELSE
			PRINTLN("[CS_HELI_SPEC] SET_PLAYER_TO_SPECTATE, playerToSpectate = INVALID_PLAYER_INDEX() ")
		ENDIF
		#ENDIF
	ENDIF
ENDPROC

FUNC BOOL FOUND_VALID_PLAYER_TO_SPECTATE()
	
	PARTICIPANT_INDEX piPlayer
	INT iPart

	IF serverBD.heliStruct.playerToSpectate <> INVALID_PLAYER_INDEX()
		IF IS_NET_PLAYER_OK(serverBD.heliStruct.playerToSpectate)
		
			IF NETWORK_IS_PLAYER_A_PARTICIPANT(serverBD.heliStruct.playerToSpectate)
		
				piPlayer = NETWORK_GET_PARTICIPANT_INDEX(serverBD.heliStruct.playerToSpectate)
				
				IF NETWORK_IS_PARTICIPANT_ACTIVE(piPlayer)
				
					iPart = NATIVE_TO_INT(piPlayer)
					
					IF NOT IS_BIT_SET(playerBD[iPart].iPlayerBitSet, biP_FailedToStayInArea)
						
						IF NOT IS_BIT_SET(playerBD[iPart].iPlayerBitSet, biP_IsSpectating)
						
							IF IS_BIT_SET(playerBD[iPart].iPlayerBitSet, biP_InArea)
							
								RETURN TRUE
								
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

SCRIPT_TIMER timeSpecUpdate
// Server logic for the spectating heli
PROC MAINTAIN_SPECTATOR_CHOPPER_SERVER()
	IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
		EXIT
	ENDIF
	PED_INDEX pedFollow
	
	BOOL bFly 
	bFly = TRUE 
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_NoHeliSpectate")
	
		EXIT
	ENDIF
	#ENDIF

	IF serverBD.eStage > eAD_IDLE
	
		IF FOUND_VALID_PLAYER_TO_SPECTATE()

			pedFollow = GET_PLAYER_PED(serverBD.heliStruct.playerToSpectate)
		ELSE	
			SET_PLAYER_TO_SPECTATE()
		ENDIF
		
		// Grab heli start coords
		IF NOT ARE_VECTORS_ALMOST_EQUAL(vMyBlipPos, serverBD.vHeliStartPos)
			IF NOT HAS_NET_TIMER_STARTED(timeSpecUpdate)
				START_NET_TIMER(timeSpecUpdate)
			ELSE
				IF HAS_NET_TIMER_EXPIRED(timeSpecUpdate, 500)
					serverBD.vHeliStartPos = vMyBlipPos
					
					#IF FEATURE_STUNT_FM_EVENTS
					//-- Spectator heli needs to be a bit higher if the players are in helicopters
					IF serverBD.iPennedInType = PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY
						serverBD.vHeliStartPos.z += 50.0
					ENDIF
					#ENDIF
					
					RESET_NET_TIMER(timeSpecUpdate)
					PRINTLN("[CS_HELI_SPEC] MAINTAIN_SPECTATOR_CHOPPER_SERVER (Timer Check), serverBD.vHeliStartPos = ", serverBD.vHeliStartPos, " vMyBlipPos = ", vMyBlipPos )
				ENDIF
			ENDIF
		ENDIF
		
		INT iMinHeight = g_sMPTunables.iPenned_In_Heli_Minhight
		
		#IF FEATURE_STUNT_FM_EVENTS
		//-- Spectator heli needs to be a bit higher if the players are in helicopters
		IF serverBD.iPennedInType = PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY
			iMinHeight = g_sMPTunables.iPenned_In_Heli_Minhight * 3
		ENDIF
		#ENDIF
		
		RUN_SCTV_HELI_LOGIC_SERVER(serverBD.heliStruct, pedFollow, serverBD.vHeliStartPos, bFly, g_sMPTunables.fPenned_In_Heli_Speed, g_sMPTunables.fPenned_In_Heli_Offset, iMinHeight, HF_ForceHeightMapAvoidance | HF_MaintainHeightAboveTerrain)
	ENDIF
ENDPROC


INT iStaggeredObjCheck

CONST_INT biObj_BIKE	0
CONST_INT biObj_VEHG	1
CONST_INT biObj_PBIKE	2
CONST_INT biObj_PVEHG	3
CONST_INT biObj_AREA	4
CONST_INT biObj_BAREA	5
CONST_INT biObj_EXITV	6
CONST_INT biObj_STARTO	7

PROC MAINTAIN_OBJECTIVE_TEXT_FOR_SCTV()
	//-- PEN_BIKE
	//-- PEN_VEHG
	//-- PEN_PBIKE
	//-- PEN_PVEHG
	//-- PEN_AREA
	//-- PEN_BAREA
	//-- PEN_EXITV
	//-- PEN_STARTO
	
	TEXT_LABEL_15 tl15Obj
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
		SWITCH iStaggeredObjCheck
			CASE 0
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iObjTextBitset, biObj_BIKE)
					IF Is_This_The_Current_Objective_Text("PEN_BIKE")
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iObjTextBitset, biObj_BIKE)
					ENDIF
				ELSE
					IF NOT Is_This_The_Current_Objective_Text("PEN_BIKE")
						CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iObjTextBitset, biObj_BIKE)
					ENDIF
				ENDIF
				
				iStaggeredObjCheck++
			BREAK
			
			CASE 1
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iObjTextBitset, biObj_VEHG)
					IF Is_This_The_Current_Objective_Text("PEN_VEHG")
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iObjTextBitset, biObj_VEHG)
					ENDIF
				ELSE
					IF NOT Is_This_The_Current_Objective_Text("PEN_VEHG")
						CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iObjTextBitset, biObj_VEHG)
					ENDIF
				ENDIF
				
				iStaggeredObjCheck++
			BREAK
			
			CASE 2
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iObjTextBitset, biObj_PBIKE)
					IF Is_This_The_Current_Objective_Text("PEN_PBIKE")
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iObjTextBitset, biObj_PBIKE)
					ENDIF
				ELSE
					IF NOT Is_This_The_Current_Objective_Text("PEN_PBIKE")
						CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iObjTextBitset, biObj_PBIKE)
					ENDIF
				ENDIF
				
				iStaggeredObjCheck++
			BREAK
			
			CASE 3
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iObjTextBitset, biObj_PVEHG)
					IF Is_This_The_Current_Objective_Text("PEN_PVEHG")
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iObjTextBitset, biObj_PVEHG)
					ENDIF
				ELSE
					IF NOT Is_This_The_Current_Objective_Text("PEN_PVEHG")
						CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iObjTextBitset, biObj_PVEHG)
					ENDIF
				ENDIF
				
				iStaggeredObjCheck++
			BREAK
			
			CASE 4
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iObjTextBitset, biObj_AREA)
					IF Is_This_The_Current_Objective_Text("PEN_AREA")
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iObjTextBitset, biObj_AREA)
					ENDIF
				ELSE
					IF NOT Is_This_The_Current_Objective_Text("PEN_AREA")
						CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iObjTextBitset, biObj_AREA)
					ENDIF
				ENDIF
				
				iStaggeredObjCheck++
			BREAK
			
			CASE 5
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iObjTextBitset, biObj_BAREA)
					IF Is_This_The_Current_Objective_Text("PEN_BAREA")
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iObjTextBitset, biObj_BAREA)
					ENDIF
				ELSE
					IF NOT Is_This_The_Current_Objective_Text("PEN_BAREA")
						CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iObjTextBitset, biObj_BAREA)
					ENDIF
				ENDIF
				
				iStaggeredObjCheck++
			BREAK
			
			CASE 6
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iObjTextBitset, biObj_EXITV)
					IF Is_This_The_Current_Objective_Text("PEN_EXITV")
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iObjTextBitset, biObj_EXITV)
					ENDIF
				ELSE
					IF NOT Is_This_The_Current_Objective_Text("PEN_EXITV")
						CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iObjTextBitset, biObj_EXITV)
					ENDIF
				ENDIF
				
				iStaggeredObjCheck++
			BREAK
			
			CASE 7
			//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [MAINTAIN_HELP_TEXT_FOR_SCTV] CHECK  biObj_STARTO")
				tl15Obj = GET_PENNED_IN_ABOUT_TO_START_OBJ_TEXT()
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iObjTextBitset, biObj_STARTO)
					IF Is_This_The_Current_Objective_Text(tl15Obj)
					//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [MAINTAIN_HELP_TEXT_FOR_SCTV] SET biObj_STARTO")
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iObjTextBitset, biObj_STARTO)
					ENDIF
				ELSE
					IF NOT Is_This_The_Current_Objective_Text(tl15Obj)
						CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iObjTextBitset, biObj_STARTO)
					//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [MAINTAIN_HELP_TEXT_FOR_SCTV] CLEAR biObj_STARTO")
					ENDIF
				ENDIF
				
				iStaggeredObjCheck++
			BREAK
		ENDSWITCH
		
		IF iStaggeredObjCheck > 7
			iStaggeredObjCheck = 0
		ENDIF
	ELSE
		//-- I'm SCTV
		IF playerBD[PARTICIPANT_ID_TO_INT()].iObjTextBitset <> 0
			playerBD[PARTICIPANT_ID_TO_INT()].iObjTextBitset = 0
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [MAINTAIN_HELP_TEXT_FOR_SCTV] CLEARED iObjTextBitset AS I'M NOW SCTV")
		ENDIF
		
		PLAYER_INDEX playerSPec = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget
		PARTICIPANT_INDEX partSpec
		INT iPartSpec
		
		IF NETWORK_IS_PLAYER_ACTIVE(playerSPec)
			IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerSPec)
				partSpec = NETWORK_GET_PARTICIPANT_INDEX(playerSPec)
				iPartSpec = NATIVE_TO_INT(partSpec)
				
				SWITCH iStaggeredObjCheck
					CASE 0
						IF NOT IS_BIT_SET(playerBD[iPartSpec].iObjTextBitset, biObj_BIKE)
							IF Is_This_The_Current_Objective_Text("PEN_BIKE")
								Clear_Any_Objective_Text_From_This_Script()
							ENDIF
						ELSE
							IF NOT Is_This_The_Current_Objective_Text("PEN_BIKE")
								Print_Objective_Text("PEN_BIKE")
							ENDIF
						ENDIF
						iStaggeredObjCheck++
					BREAK
					
					CASE 1
						IF NOT IS_BIT_SET(playerBD[iPartSpec].iObjTextBitset, biObj_VEHG)
							IF Is_This_The_Current_Objective_Text("PEN_VEHG")
								Clear_Any_Objective_Text_From_This_Script()
							ENDIF
						ELSE
							IF NOT Is_This_The_Current_Objective_Text("PEN_VEHG")
								Print_Objective_Text("PEN_VEHG")
							ENDIF
						ENDIF
						iStaggeredObjCheck++
					BREAK
					
					CASE 2
						IF NOT IS_BIT_SET(playerBD[iPartSpec].iObjTextBitset, biObj_PBIKE)
							IF Is_This_The_Current_Objective_Text("PEN_PBIKE")
								Clear_Any_Objective_Text_From_This_Script()
							ENDIF
						ELSE
							IF NOT Is_This_The_Current_Objective_Text("PEN_PBIKE")
								Print_Objective_Text("PEN_PBIKE")
							ENDIF
						ENDIF
						iStaggeredObjCheck++
					BREAK
					
					CASE 3
						IF NOT IS_BIT_SET(playerBD[iPartSpec].iObjTextBitset, biObj_PVEHG)
							IF Is_This_The_Current_Objective_Text("PEN_PVEHG")
								Clear_Any_Objective_Text_From_This_Script()
							ENDIF
						ELSE
							IF NOT Is_This_The_Current_Objective_Text("PEN_PVEHG")
								Print_Objective_Text("PEN_PVEHG")
							ENDIF
						ENDIF
						iStaggeredObjCheck++
					BREAK
					
					CASE 4
						IF NOT IS_BIT_SET(playerBD[iPartSpec].iObjTextBitset, biObj_AREA)
							IF Is_This_The_Current_Objective_Text("PEN_AREA")
								Clear_Any_Objective_Text_From_This_Script()
							ENDIF
						ELSE
							IF NOT Is_This_The_Current_Objective_Text("PEN_AREA")
								Print_Objective_Text("PEN_AREA")
							ENDIF
						ENDIF
						iStaggeredObjCheck++
					BREAK
					
					CASE 5
						IF NOT IS_BIT_SET(playerBD[iPartSpec].iObjTextBitset, biObj_BAREA)
							IF Is_This_The_Current_Objective_Text("PEN_BAREA")
								Clear_Any_Objective_Text_From_This_Script()
							ENDIF
						ELSE
							IF NOT Is_This_The_Current_Objective_Text("PEN_BAREA")
								Print_Objective_Text("PEN_BAREA")
							ENDIF
						ENDIF
						iStaggeredObjCheck++
					BREAK
					
					CASE 6
						IF NOT IS_BIT_SET(playerBD[iPartSpec].iObjTextBitset, biObj_EXITV)
							IF Is_This_The_Current_Objective_Text("PEN_EXITV")
								Clear_Any_Objective_Text_From_This_Script()
							ENDIF
						ELSE
							IF NOT Is_This_The_Current_Objective_Text("PEN_EXITV")
								Print_Objective_Text("PEN_EXITV")
							ENDIF
						ENDIF
						iStaggeredObjCheck++
					BREAK
					
					CASE 7
						tl15Obj = GET_PENNED_IN_ABOUT_TO_START_OBJ_TEXT()
						IF NOT IS_BIT_SET(playerBD[iPartSpec].iObjTextBitset, biObj_STARTO)
							IF Is_This_The_Current_Objective_Text(tl15Obj)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [MAINTAIN_HELP_TEXT_FOR_SCTV] SCTV CHECK  CLEARING PEN_STARTO2")
								Clear_Any_Objective_Text_From_This_Script()
							ENDIF
						ELSE
							IF NOT Is_This_The_Current_Objective_Text(tl15Obj)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - [MAINTAIN_HELP_TEXT_FOR_SCTV] SCTV CHECK  DISPLAYING PEN_STARTO2")
								Print_Objective_Text(tl15Obj)
							ENDIF
						ENDIF
						iStaggeredObjCheck++
					BREAK
				ENDSWITCH
				
				IF iStaggeredObjCheck > 7
					iStaggeredObjCheck = 0
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

#IF FEATURE_STUNT_FM_EVENTS

FUNC BOOL HOST_TAKE_CONTROL_OF_NETWORK_ID(NETWORK_INDEX niEntity)
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		RETURN FALSE
	ENDIF
	
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niEntity)
		RETURN FALSE
	ENDIF
	
	IF TAKE_CONTROL_OF_NET_ID(niEntity)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL MAINTAIN_OWNERSHIP_OF_NETWORK_ID(NETWORK_INDEX niEntity)

	IF IS_NETWORK_ID_OWNED_BY_PARTICIPANT(niEntity)
		RETURN TRUE
	ENDIF
	
	IF HOST_TAKE_CONTROL_OF_NETWORK_ID(niEntity)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_EXPLODE_AIRCRAFT_SERVER()
	IF NOT HAS_NET_TIMER_STARTED(serverBD.timeExplodeAircraft)
		START_NET_TIMER(serverBD.timeExplodeAircraft)
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [MAINTAIN_EXPLODE_AIRCRAFT_SERVER] STARTED timeExplodeAircraft")
	ELSE
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AllAircraftDestroyed)
			IF HAVE_ALL_AIRCRAFT_BEEN_DESTROYED()
				SET_BIT(serverBD.iServerBitSet, biS_AllAircraftDestroyed)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [MAINTAIN_EXPLODE_AIRCRAFT_SERVER] Set biS_AllAircraftDestroyed")
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC MAINTAIN_EXPLODE_AIRCRAFT_CLIENT()
	INT i
	BOOL bDrawingTimer
	
	IF serverBD.iPennedInType != PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY
		EXIT
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(serverBD.timeExplodeAircraft)
		REPEAT MAX_AIRCRAFT_TO_SPAWN i

			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niAircraft[i])
				IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niAircraft[i]))
					IF (GET_VEHICLE_DETONATION_TIME()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeExplodeAircraft)) >= 0
						IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niAircraft[i]), <<VEH_EXP_RANGE, VEH_EXP_RANGE, VEH_EXP_RANGE>>) 
							IF NOT SHOULD_UI_BE_HIDDEN()
								IF NOT bDrawingTimer
									SET_MIDDLE_TITLE_POSITION_HUD_THIS_FRAME()
									DRAW_GENERIC_TIMER((GET_VEHICLE_DETONATION_TIME()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeExplodeAircraft)), "UW_DEST", 0, TIMER_STYLE_USEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_DONTCARE, DEFAULT, HUD_COLOUR_RED, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_RED)
									bDrawingTimer = TRUE
								ENDIF
							ENDIF
						ENDIF
					ELSE
						//-- time expired
						IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niAircraft[i]), <<VEH_EXP_RANGE, VEH_EXP_RANGE, VEH_EXP_RANGE>>)
							IF NOT SHOULD_UI_BE_HIDDEN()
								IF NOT bDrawingTimer
									SET_MIDDLE_TITLE_POSITION_HUD_THIS_FRAME()
									DRAW_GENERIC_TIMER(0, "UW_DEST", 0, TIMER_STYLE_USEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_DONTCARE, DEFAULT, HUD_COLOUR_RED, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_RED)
									bDrawingTimer = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						IF MAINTAIN_OWNERSHIP_OF_NETWORK_ID(serverBD.niAircraft[i])
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niAircraft[i])
								SET_ENTITY_INVINCIBLE(NET_TO_VEH(serverBD.niAircraft[i]), FALSE)
								SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(NET_TO_VEH(serverBD.niAircraft[i]), TRUE)
								IF NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(NET_TO_VEH(serverBD.niAircraft[i])))
									NETWORK_EXPLODE_VEHICLE(NET_TO_VEH(serverBD.niAircraft[i]))
									CLEANUP_NET_ID(serverBD.niAircraft[i])
									PRINTLN("     ---------->     PENNED IN - [MAINTAIN_EXPLODE_AIRCRAFT_CLIENT] - NETWORK_EXPLODE_VEHICLE DONE VEH = ", i)
								ELSE
									NETWORK_EXPLODE_HELI(NET_TO_VEH(serverBD.niAircraft[i]))
									CLEANUP_NET_ID(serverBD.niAircraft[i])
									PRINTLN("     ---------->     PENNED IN - [MAINTAIN_EXPLODE_AIRCRAFT_CLIENT] - NETWORK_EXPLODE_HELI DONE VEH = ", i)
								ENDIF
							ELSE
							//	PRINTLN("     ---------->     KILL LIST - EXPLODE THE VEHICLE WANT TO EXPLODE VEH BUT I DON'T HAVE CONTROL!")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
		ENDREPEAT
		
		IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_DoneExpHeli)
			IF IS_PLAYER_SITTING_IN_ANY_PENNED_IN_AIRCRAFT()
				IF NOT Is_This_The_Current_Objective_Text("PEN_EXPLH")
					Print_Objective_Text("PEN_EXPLH")
					SET_BIT(iBoolsBitSet2, bi2_DoneExpHeli)
					PRINTLN("     ---------->     PENNED IN - [MAINTAIN_EXPLODE_AIRCRAFT_CLIENT] SET bi2_DoneExpHeli")
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_PLAYER_SITTING_IN_ANY_PENNED_IN_AIRCRAFT()
				IF Is_This_The_Current_Objective_Text("PEN_EXPLH")
					Clear_Any_Objective_Text_From_This_Script()
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
ENDPROC
#ENDIF // FEATURE_STUNT_FM_EVENTS


PROC MAINTAIN_PLAYER_MELEE_HELP()
	IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
		IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_DoneMeleeHelp)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_VehInvalid)
						IF NOT HAS_NET_TIMER_STARTED(timeMeleeHelp)
							START_NET_TIMER(timeMeleeHelp)
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [MAINTAIN_PLAYER_MELEE_HELP] Started timeMeleeHelp")
						ELSE
							IF HAS_NET_TIMER_EXPIRED(timeMeleeHelp, 10000)
								IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
									PRINT_HELP("BRS_BM_EXP1")
									SET_BIT(iBoolsBitSet2, bi2_DoneMeleeHelp)
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - [MAINTAIN_PLAYER_MELEE_HELP] Set bi2_DoneMeleeHelp")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
		ENDIF
	ENDIF
ENDPROC
 // FEATURE_BIKER

BOOL bDoneDistanceCheck
PROC MAINTAIN_DISTANCE_CHECKS_FOR_VARIATION()
	
	IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
		EXIT
	ENDIF
	
	IF ServerBD.eStage > eAD_IDLE
		EXIT
	ENDIF
	
	VECTOR vAction = vNodes[0]
	
	IF NOT ARE_VECTORS_EQUAL(vAction, <<0.0, 0.0, 0.0>>)
		GB_MAINTAIN_BOSS_WORK_DISTANT_CHECKS(FMMC_TYPE_BIKER_CAGED_IN, vAction, bDoneDistanceCheck)
	ENDIF
ENDPROC

PROC MAINTAIN_CLEAR_FORMATION()
	IF playerBD[PARTICIPANT_ID_TO_INT()].eStage	 = eAD_IDLE
	OR playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_IN_AREA
		//-- If we haven't started, and we're in the area, restrict formation
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InArea)
		OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaAsPassenger)
			IF NOT BIK_IS_FORMATION_RESTRICTED()
				BIK_SET_FORMATION_RESTRICTED()
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [MAINTAIN_CLEAR_FORMATION] BIK_SET_FORMATION_RESTRICTED")
			ENDIF
		ENDIF
	ELSE
		//-- If we've been knocked out, and we took part, clear the restriction
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InArea)
		OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaAsPassenger)
			IF BIK_IS_FORMATION_RESTRICTED()
				BIK_CLEAR_FORMATION_RESTRICTED()
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [MAINTAIN_CLEAR_FORMATION] BIK_CLEAR_FORMATION_RESTRICTED playerBD[PARTICIPANT_ID_TO_INT()].eStage = ", ENUM_TO_INT(playerBD[PARTICIPANT_ID_TO_INT()].eStage))
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC MAINTAIN_SET_MY_VEHICLE_PROOFS()
	VEHICLE_INDEX veh
	
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneWon)
		IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InArea)
				IF serverBD.eStage >= eAD_IN_AREA
					
					IF NOT IS_BIT_SET(iBoolsBitSet3, bi3_VehExpProof)
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
								veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
								vehProofs = veh
								SET_ENTITY_PROOFS(veh, FALSE, TRUE, TRUE, FALSE, FALSE)
								SET_BIT(iBoolsBitSet3, bi3_VehExpProof)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [MAINTAIN_SET_MY_VEHICLE_PROOFS] Set veh proofs ")
							ENDIF
						ENDIF
					ELSE
						IF IS_PED_INJURED(PLAYER_PED_ID())
							IF DOES_ENTITY_EXIST(vehProofs)
								IF IS_VEHICLE_DRIVEABLE(vehProofs)
									SET_ENTITY_PROOFS(vehProofs, FALSE, FALSE, FALSE, FALSE, FALSE)
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [MAINTAIN_SET_MY_VEHICLE_PROOFS] Clear veh proofs - about to explode")
									CLEAR_BIT(iBoolsBitSet3, bi3_VehExpProof)
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [MAINTAIN_SET_MY_VEHICLE_PROOFS] Clear veh proofs  - I'm injured")
								ENDIF
							ENDIF
						ELSE
							IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
								veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
								IF veh != vehProofs
									IF DOES_ENTITY_EXIST(vehProofs)
										IF IS_VEHICLE_DRIVEABLE(vehProofs)
											SET_ENTITY_PROOFS(vehProofs, FALSE, FALSE, FALSE, FALSE, FALSE)
											CLEAR_BIT(iBoolsBitSet3, bi3_VehExpProof)
											CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [MAINTAIN_SET_MY_VEHICLE_PROOFS] Clear veh proofs  - veh != vehProofs")
										ENDIF
									ENDIF
								ENDIF
								
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF		
					
		ENDIF
	ELSE
		IF IS_BIT_SET(iBoolsBitSet3, bi3_VehExpProof)
			IF DOES_ENTITY_EXIST(vehProofs)
				IF IS_VEHICLE_DRIVEABLE(vehProofs)
					SET_ENTITY_PROOFS(vehProofs, FALSE, FALSE, FALSE, FALSE, FALSE)
					CLEAR_BIT(iBoolsBitSet3, bi3_VehExpProof)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN [MAINTAIN_SET_MY_VEHICLE_PROOFS] Clear veh proofs  - biS_SomeoneWon")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

//PURPOSE: Process the PENNED IN stages for the Client
PROC PROCESS_PENNED_IN_CLIENT()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	OPEN_SCRIPT_PROFILE_MARKER_GROUP("PROCESS_PENNED_IN_CLIENT") // MUST BE AT START OF FUNCTION
	#ENDIF
	#ENDIF
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
		EXIT
	ENDIF
	
	TEXT_LABEL_15 tl15Label
	
	STRING sHelp
	VEHICLE_INDEX vehTemp
	SWITCH playerBD[PARTICIPANT_ID_TO_INT()].eStage		
		CASE eAD_IDLE

			IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
				IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_DoneCommonSetup)
					IF AM_I_IN_LAUNCH_GANG()
						GB_COMMON_BOSS_MISSION_SETUP(FMMC_TYPE_BIKER_CAGED_IN, TRUE,serverBD.iPennedInType)
					ELSE
						//-- Don't block jobs for non-launch gang players
						GB_COMMON_BOSS_MISSION_SETUP(FMMC_TYPE_BIKER_CAGED_IN, FALSE,serverBD.iPennedInType)
					ENDIF
					SET_BIT(iBoolsBitSet2, bi2_DoneCommonSetup)
				ENDIF
			ENDIF
			IF NOT SHOULD_UI_BE_HIDDEN()
				IF IS_BIT_SET(iBoolsBitSet, biEventUiHidden)
					ADD_AREA_BLIP(GET_START_COORDS_FOR_AREA(), TRUE)
					ADD_START_BLIP()
					CLEAR_BIT(iBoolsBitSet, biEventUiHidden)
				ENDIF
				
				//--Help text when Event is available
				IF serverBD.eStage = eAD_IDLE
					IF NOT IS_BIT_SET(iBoolsBitSet, biHelp1Done)
						IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
							IF OK_TO_DISPLAY_HELP_FOR_MODE()
								sHelp = GET_PENNED_IN_START_HELP()
								
								IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
									IF OK_TO_DISPLAY_HELP_FOR_MODE()
										PRINT_HELP_WITH_COLOURED_STRING_NO_SOUND(sHelp, "CAG_BLIPS", hclLaunchGang, DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
									ENDIF
								ELSE
									PRINT_HELP_NO_SOUND(sHelp, DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
									SET_FREEMODE_EVENT_HELP_BACKGROUND(FALSE)
								ENDIF
								
								SET_BIT(iBoolsBitSet, biHelp1Done)
								PLAY_SOUND_FRONTEND(-1, "Event_Start_Text", "GTAO_FM_Events_Soundset", FALSE)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Set biHelp1Done")
							ENDIF
						ENDIF
					
					#IF FEATURE_STUNT_FM_EVENTS
					ELSE
						IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_DoneAmbHeliHelp)
							IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
								IF DOES_BLIP_EXIST(blipAircraft[0])
									PRINT_HELP_NO_SOUND("PEN_HELPH", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) //Helicopters ~HUD_COLOUR_BLUE~~BLIP_HELICOPTER~~s~ are available at several locations. These will self-destruct once the event is over.
									IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
										SET_FREEMODE_EVENT_HELP_BACKGROUND()
									ENDIF
									SET_BIT(iBoolsBitSet2, bi2_DoneAmbHeliHelp)
									PLAY_SOUND_FRONTEND(-1, "Event_Start_Text", "GTAO_FM_Events_Soundset", FALSE)
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Set bi2_DoneAmbHeliHelp")
								ENDIF
							ENDIF
						ENDIF
					#ENDIF
					ENDIF
				ENDIF
				
				
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InArea)
					
					IF IS_LOCAL_PLAYER_IN_AREA(TRUE)
						
						IF NOT IS_BIT_SET(g_FM_EVENT_VARS.iEventBitSet, ciFM_EVENT_EVENT_WANTED_SUPRESSED_BY_EVENT)
							FM_EVENTS_SET_UP_WANTED_OVERRIDES()
						ENDIF
						IF IS_BIT_SET(iBoolsBitSet2, bi2_NotRegHelp)
							tl15Label = GET_PENNED_IN_OUTSIDE_AREA_TEXT()
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(tl15Label)
								IF HAS_NET_TIMER_EXPIRED(timeNotRegHelp, 3000)
									CLEAR_BIT(iBoolsBitSet2, bi2_NotRegHelp)
									RESET_NET_TIMER(timeNotRegHelp)
									CLEAR_HELP()
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Clear bi2_NotRegHelp (1) ")
								ENDIF
							ENDIF
						ENDIF
						IF iClientPLayerCOunt < GET_MAX_NUMBER_OF_PLAYERS_FOR_LAUNCH()
							IF NOT FM_EVENT_IS_PLAYER_WAITING_FOR_FM_EVENT_TO_START(PLAYER_ID())
								FM_EVENT_SET_PLAYER_WAITING_FOR_FM_EVENT_TO_START(TRUE)
							ENDIF
							
							IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_SHouldNotBeDamageable)
								SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
								IF NOT IS_MP_PASSIVE_MODE_ENABLED()
									IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
										NETWORK_SET_FRIENDLY_FIRE_OPTION(FALSE)
										SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_SHouldNotBeDamageable)
										SET_PED_CAN_BE_DRAGGED_OUT(PLAYER_PED_ID(), FALSE)
										#IF FEATURE_STUNT_FM_EVENTS
										IF serverBD.iPennedInType = PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY
											SET_LOCAL_PLAYER_AS_GHOST(TRUE)
											CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN SET_LOCAL_PLAYER_AS_GHOST(TRUE) 1")
										ENDIF
										#ENDIF
										
											
										SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableVehicleCombat, TRUE)
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableVehicleCombat, TRUE) 1")
											
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Set biP_SHouldNotBeDamageable - 1")
									ELSE
										IF IS_VEHICLE_MODEL_VALID_FOR_PENNED_IN(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))
											IF IS_VEHICLE_SUITABLE_FOR_PASSIVE_MODE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
												NETWORK_SET_FRIENDLY_FIRE_OPTION(FALSE)
												SET_PED_CAN_BE_DRAGGED_OUT(PLAYER_PED_ID(), FALSE)
												SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_SHouldNotBeDamageable)
												
												
												SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableVehicleCombat, TRUE)
												CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableVehicleCombat, TRUE) 35")
												
												#IF FEATURE_STUNT_FM_EVENTS
												IF serverBD.iPennedInType = PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY
													SET_LOCAL_PLAYER_AS_GHOST(TRUE)
													CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN SET_LOCAL_PLAYER_AS_GHOST(TRUE) 2")
												ENDIF
												#ENDIF
											
												CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Set biP_SHouldNotBeDamageable - 42")
											ENDIF
										ENDIF
									ENDIF
								ELSE
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - NOT SETTING biP_SHouldNotBeDamageable - AS LOCAL PLAYER IS GHOSTED")
								ENDIF
							ELSE
								IF IS_MP_PASSIVE_MODE_ENABLED()
									CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_SHouldNotBeDamageable)
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN -  CELAR  biP_SHouldNotBeDamageable - IS_MP_PASSIVE_MODE_ENABLED")
								ENDIF
							ENDIF 
							IF NOT IS_BIT_SET(iBoolsBitSet, biSetPlayerInvincible)
								SET_BIT(iBoolsBitSet, biSetPlayerInvincible)
						//		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
						//		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Set SET_ENTITY_ONLY_DAMAGED_BY_RELATIONSHIP_GROUP - SET biSetPlayerInvincible")
							//	SET_ENTITY_ONLY_DAMAGED_BY_RELATIONSHIP_GROUP(PLAYER_PED_ID(), TRUE, relPennedInPlayer)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Set SET_ENTITY_ONLY_DAMAGED_BY_RELATIONSHIP_GROUP - 1")
							ENDIF
							
							

							IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
								IF NOT IS_MP_PASSIVE_MODE_ENABLED()
									IF IS_VEHICLE_MODEL_VALID_FOR_PENNED_IN(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))
										IF IS_BIT_SET(iBoolsBitSet, biValidVehHelp)
											CLEAR_BIT(iBoolsBitSet, biValidVehHelp)
										ENDIF
										IF IS_BIT_SET(iBoolsBitSet, biPlayerPassive)
											CLEAR_BIT(iBoolsBitSet, biPlayerPassive)
										ENDIF
										vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
										
										IF IS_VEHICLE_DRIVEABLE(vehTemp)
											SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PlayersDontDragMeOutOfCar, TRUE)
											IF GET_PED_IN_VEHICLE_SEAT(vehTemp) = PLAYER_PED_ID()
												// 2416571: Prevent ped being knocked off bikes while waiting for event to start
												IF NOT IS_PED_INJURED(PLAYER_PED_ID())
													IF IS_PED_ON_ANY_BIKE(PLAYER_PED_ID())
														IF CAN_KNOCK_PED_OFF_VEHICLE(PLAYER_PED_ID())
															SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_NEVER)
														ENDIF
													ELSE
														IF NOT CAN_KNOCK_PED_OFF_VEHICLE(PLAYER_PED_ID())
															SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_DEFAULT)
														ENDIF
													ENDIF
												ENDIF
												
												IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WantToStart)
													SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WantToStart)
													CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - biP_WantToStart")
												ENDIF
												
												IF IS_BIT_SET(serverBD.iStartingPartBitSet, PARTICIPANT_ID_TO_INT())
													IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaAsPassenger)
														CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaAsPassenger)
														CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Clear biP_InAreaAsPassenger 1")
													ENDIF
													
													SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InArea)
													
													vehMyLastVeh = vehTemp
													
													IF telemetryStruct.m_timeTakenForObjective = 0
														telemetryStruct.m_timeTakenForObjective = GET_CLOUD_TIME_AS_INT()  
													ENDIF													
													
													CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - biP_InArea")
													sHelp = GET_PENNED_IN_NEED_VEHICLE_HELP_TEXT()
													IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sHelp)
														CLEAR_HELP()
													ENDIF
													
													
												ELSE
													CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - WAITING FOR SERVER TO SAY I CAN JOIN")
												ENDIF
											ELSE
												
												//-- In a car but not a driver
												IF NOT IS_BIT_SET(iBoolsBitSet, biDoneNonDriverHelp)
													IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
														IF OK_TO_DISPLAY_HELP_FOR_MODE()
															TEXT_LABEL_15 tl15Driver
															tl15Driver = GET_PENNED_IN_NEED_TO_BE_DRIVER_TEXT()
															IF OK_TO_DISPLAY_HELP_FOR_MODE()
																PRINT_HELP_NO_SOUND(tl15Driver, DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) // You need to be the driver of a vehicle to take part in the Penned In event.
															ENDIF
															IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
																SET_FREEMODE_EVENT_HELP_BACKGROUND()
															ENDIF
															SET_BIT(iBoolsBitSet, biDoneNonDriverHelp)
															CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Set biDoneNonDriverHelp")
														ENDIF
													ENDIF
												ENDIF
												
												IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaAsPassenger)
													SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaAsPassenger)
													CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Set biP_InAreaAsPassenger")
												ENDIF
											ENDIF
										ENDIF
									ELSE
										 //-- Invalid vehicle
										IF NOT IS_BIT_SET(iBoolsBitSet, biValidVehHelp)
											IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
												IF OK_TO_DISPLAY_HELP_FOR_MODE()
													sHelp = GET_PENNED_IN_INVALID_VEHICLE_HELP_TEXT()
													IF OK_TO_DISPLAY_HELP_FOR_MODE()
														PRINT_HELP_NO_SOUND(sHelp,DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) 
													ENDIF
													
													
													IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
														SET_FREEMODE_EVENT_HELP_BACKGROUND()
													ENDIF
													SET_BIT(iBoolsBitSet, biValidVehHelp)
													CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Set biValidVehHelp")
												ENDIF
											ELSE
												CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - IS_OK_TO_PRINT_FREEMODE_HELP fail VALID VEH")
											ENDIF
										ENDIF
										
										IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_SHouldNotBeDamageable)
											
											SET_PLAYER_DEFAULT_FRIENDLY_FIRE_OPTION()
											CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_SHouldNotBeDamageable)
											SET_PED_CAN_BE_DRAGGED_OUT(PLAYER_PED_ID(), TRUE)
											CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - CLEAR biP_SHouldNotBeDamageable - not valid veh")
											
											#IF FEATURE_STUNT_FM_EVENTS
											IF serverBD.iPennedInType = PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY
												SET_LOCAL_PLAYER_AS_GHOST(FALSE)
												CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN SET_LOCAL_PLAYER_AS_GHOST(FALSE) 2")
											ENDIF
											#ENDIF
										ENDIF
									ENDIF
								ELSE
									IF NOT IS_BIT_SET(iBoolsBitSet, biPlayerPassive)
										IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
											SET_BIT(iBoolsBitSet, biPlayerPassive)
											IF OK_TO_DISPLAY_HELP_FOR_MODE()
												PRINT_HELP_NO_SOUND("PEN_PASSMD", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) // You can't participate in this event while Passive Mode is enabled.
											ENDIF
											IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
												SET_FREEMODE_EVENT_HELP_BACKGROUND()
											ENDIF
											CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - SET biPlayerPassiveP")
										ELSE
											CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - IS_OK_TO_PRINT_FREEMODE_HELP FALSE PASSIVE HELP")
										ENDIF
									ENDIF
									
									
								ENDIF
								
							ELSE
								//-- Help text for needing to be in a vehicle
								IF NOT IS_BIT_SET(iBoolsBitSet, biHelp2Done)
									IF IS_BIT_SET(iBoolsBitSet, biHelp1Done)
										IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
											IF OK_TO_DISPLAY_HELP_FOR_MODE()
												sHelp = GET_PENNED_IN_NEED_VEHICLE_HELP_TEXT()
												IF OK_TO_DISPLAY_HELP_FOR_MODE()
													PRINT_HELP_NO_SOUND(sHelp, DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
												ENDIF
												IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
													SET_FREEMODE_EVENT_HELP_BACKGROUND()
												ENDIF
												SET_BIT(iBoolsBitSet, biHelp2Done)
												CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Set biHelp2Done")
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								IF IS_VEHICLE_DRIVEABLE(vehMyLastVeh)
								//	SET_ENTITY_INVINCIBLE(vehMyLastVeh, FALSE)
								//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Cleared veh invincible - 1")
								ENDIF
								IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaAsPassenger)
									CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaAsPassenger)
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - clear biP_InAreaAsPassenger 2")
								ENDIF
							ENDIF
							
							#IF IS_DEBUG_BUILD
								IF GET_COMMANDLINE_PARAM_EXISTS("sc_weaponWheel")
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ----------> [sc_weaponWheel] calling STOP_PLAYER_SHOOTING_THIS_FRAME - PROCESS_PENNED_IN_CLIENT - 1")
								ENDIF
							#ENDIF
		
							STOP_PLAYER_SHOOTING_THIS_FRAME()
						ENDIF
					ELSE
						//-- Not in area
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF IS_BIT_SET(g_FM_EVENT_VARS.iEventBitSet, ciFM_EVENT_EVENT_WANTED_SUPRESSED_BY_EVENT)
								FM_EVENTS_CLEANUP_WANTED_OVERRIDES()
							ENDIF
							IF IS_BIT_SET(iBoolsBitSet, biValidVehHelp)
								CLEAR_BIT(iBoolsBitSet, biValidVehHelp)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - CLEAR biValidVehHelp - not in area")
							ENDIF
							
							IF IS_BIT_SET(iBoolsBitSet, biSetPlayerInvincible)
								CLEAR_BIT(iBoolsBitSet, biSetPlayerInvincible)
						//		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - CLEAR biSetPlayerInvincible - 1")
							ENDIF
							
							IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_SHouldNotBeDamageable)
								SET_PLAYER_DEFAULT_FRIENDLY_FIRE_OPTION()
								SET_PED_CAN_BE_DRAGGED_OUT(PLAYER_PED_ID(), TRUE)
								CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_SHouldNotBeDamageable)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - CLEAR biP_SHouldNotBeDamageable - 1")
								
								#IF FEATURE_STUNT_FM_EVENTS
								IF serverBD.iPennedInType = PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY
									SET_LOCAL_PLAYER_AS_GHOST(FALSE)
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN SET_LOCAL_PLAYER_AS_GHOST(FALSE) 3")
								ENDIF
								#ENDIF
								
											
								SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableVehicleCombat, FALSE)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableVehicleCombat, FALSE) 1")
							ENDIF
							
							IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaAsPassenger)
								CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaAsPassenger)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - clear biP_InAreaAsPassenger 65")
							ENDIF
							
							// 2416571: Prevent ped being knocked off bikes while waiting for event to start
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							AND NOT CAN_KNOCK_PED_OFF_VEHICLE(PLAYER_PED_ID())
								SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_DEFAULT)
							ENDIF

							IF IS_BIT_SET(iBoolsBitSet2, bi2_SetDoNotRegHelp)
								IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_NotRegHelp)
									IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
										IF OK_TO_DISPLAY_HELP_FOR_MODE()
											CLEAR_BIT(iBoolsBitSet2, bi2_SetDoNotRegHelp)
											SET_BIT(iBoolsBitSet2, bi2_NotRegHelp)
											CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - SET bi2_NotRegHelp")
											RESET_NET_TIMER(timeNotRegHelp)
											START_NET_TIMER(timeNotRegHelp)
											tl15Label = GET_PENNED_IN_OUTSIDE_AREA_TEXT()
											IF OK_TO_DISPLAY_HELP_FOR_MODE()
												PRINT_HELP_NO_SOUND(tl15Label, DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) // You are outside the Penned In area and are not registered to take part.
											ENDIF
											IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
												SET_FREEMODE_EVENT_HELP_BACKGROUND()
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					//-- biP_InArea set
					IF NOT IS_LOCAL_PLAYER_IN_AREA(TRUE)
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_SHouldNotBeDamageable)
								SET_PLAYER_DEFAULT_FRIENDLY_FIRE_OPTION()
								CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_SHouldNotBeDamageable)
								SET_PED_CAN_BE_DRAGGED_OUT(PLAYER_PED_ID(), TRUE)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - CLEAR biP_SHouldNotBeDamageable - 2")
								
											
								SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableVehicleCombat, FALSE)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableVehicleCombat, FALSE) 2")
								#IF FEATURE_STUNT_FM_EVENTS
								IF serverBD.iPennedInType = PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY
									SET_LOCAL_PLAYER_AS_GHOST(FALSE)
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN SET_LOCAL_PLAYER_AS_GHOST(FALSE) 4")
								ENDIF
								#ENDIF
							ENDIF
							
							CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InArea)
							CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WantToStart)
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - eAD_IDLE CLEARED biP_InArea / biP_WantToStart as not in area")
							
							ADD_START_BLIP()
							
							// 2416571: Prevent ped being knocked off bikes while waiting for event to start
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							AND NOT CAN_KNOCK_PED_OFF_VEHICLE(PLAYER_PED_ID())
								SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_DEFAULT)
							ENDIF
							
							IF IS_BIT_SET(iBoolsBitSet, biSetPlayerInvincible)
								CLEAR_BIT(iBoolsBitSet, biSetPlayerInvincible)
							//	SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
							//	SET_ENTITY_ONLY_DAMAGED_BY_RELATIONSHIP_GROUP(PLAYER_PED_ID(), FALSE, relPennedInPlayer)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - CLEAR biSetPlayerInvincible - 2")
							ENDIF
							
							IF IS_VEHICLE_DRIVEABLE(vehMyLastVeh)
							//	SET_ENTITY_INVINCIBLE(vehMyLastVeh, FALSE)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Cleared veh invincible - 2")
							ENDIF
							
							SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PlayersDontDragMeOutOfCar, FALSE)
							
							IF FM_EVENT_IS_PLAYER_WAITING_FOR_FM_EVENT_TO_START(PLAYER_ID())
								FM_EVENT_SET_PLAYER_WAITING_FOR_FM_EVENT_TO_START(FALSE)
							ENDIF
							
							SET_BIT(iBoolsBitSet2, bi2_SetDoNotRegHelp)
						ENDIF
					ELSE
						IF IS_BIT_SET(iBoolsBitSet2, bi2_NotRegHelp)
							tl15Label = GET_PENNED_IN_OUTSIDE_AREA_TEXT()
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(tl15Label)
								IF HAS_NET_TIMER_EXPIRED(timeNotRegHelp, 3000)
									CLEAR_BIT(iBoolsBitSet2, bi2_NotRegHelp)
									RESET_NET_TIMER(timeNotRegHelp)
									CLEAR_HELP()
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Clear bi2_NotRegHelp (2) ")
								ENDIF
							ENDIF
						ENDIF
			
						IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
					//		STOP_PLAYER_LEAVING_VEHICLE_THIS_FRAME()
				
					//		STOP_PLAYER_SHOOTING_THIS_FRAME()
						ENDIF
						
						IF IS_MP_PASSIVE_MODE_ENABLED()
							// 2416571: Prevent ped being knocked off bikes while waiting for event to start
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							AND NOT CAN_KNOCK_PED_OFF_VEHICLE(PLAYER_PED_ID())
								SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_DEFAULT)
							ENDIF
							
							CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InArea)
							CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WantToStart)
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - eAD_IDLE CLEARED biP_InArea / biP_WantToStartas player passive")
						
						ELSE
							// Quick start help
							IF NOT IS_BIT_SET(iboolsBitSet, biQuckStartHElp)
								IF IS_BIT_SET(serverBD.iServerBitSet, biS_ResetForShortCOuntdown)
									
									IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
										IF OK_TO_DISPLAY_HELP_FOR_MODE()
											TEXT_LABEL_15 tl15Qstart
											tl15Qstart = GET_PENNED_IN_QUICK_START_TEXT()
											IF OK_TO_DISPLAY_HELP_FOR_MODE()
												PRINT_HELP_NO_SOUND(tl15Qstart, DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) // The event start time will be reduced when most of the available players are waiting within the area. 
											ENDIF
											IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
												SET_FREEMODE_EVENT_HELP_BACKGROUND()
											ENDIF
											SET_BIT(iBoolsBitSet, biQuckStartHElp)
											CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Set biValidVehHelp")
										ENDIF
									ELSE
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - IS_OK_TO_PRINT_FREEMODE_HELP fail quick start help")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						#IF IS_DEBUG_BUILD
							IF GET_COMMANDLINE_PARAM_EXISTS("sc_weaponWheel")
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ----------> [sc_weaponWheel] calling STOP_PLAYER_SHOOTING_THIS_FRAME - MAINTAIN_LOCAL_PLAYER_IN_AREA PROCESS_PENNED_IN_CLIENT - 2")
							ENDIF
						#ENDIF
						
						STOP_PLAYER_SHOOTING_THIS_FRAME()
					ENDIF
					
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
						OR (IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())AND NOT IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))
						OR NOT (IS_VEHICLE_MODEL_VALID_FOR_PENNED_IN(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))))
							CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InArea)
							CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WantToStart)
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - eAD_IDLE CLEARED biP_InArea / biP_WantToStart as not in a vehicle")
							
							IF IS_VEHICLE_DRIVEABLE(vehMyLastVeh)
							//	SET_ENTITY_INVINCIBLE(vehMyLastVeh, FALSE)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Cleared veh invincible - 4")
							ENDIF
						ELSE
							IF IS_BIT_SET(serverBD.iServerBitSet, biS_AtLeastTwoPlayersOnMission)
									
							//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - eAD_IDLE SET biP_InArea")
								tl15Label = GET_PENNED_IN_MIN_PLAYERS_HELP()
								
								IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(tl15Label)
									CLEAR_HELP()
								ENDIF
								
							ELSE
								//-- Need at least 2 players
								IF NOT IS_BIT_SET(iBoolsBitSet, biHelp2PlayerDone)
									IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
										IF OK_TO_DISPLAY_HELP_FOR_MODE()
											tl15Label = GET_PENNED_IN_MIN_PLAYERS_HELP()
											IF OK_TO_DISPLAY_HELP_FOR_MODE()
												PRINT_HELP_NO_SOUND(tl15Label, DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) //--At least two players in separate vehicles are required to start Penned In.
											ENDIF
											IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
												SET_FREEMODE_EVENT_HELP_BACKGROUND()
											ENDIF
											SET_BIT(iBoolsBitSet, biHelp2PlayerDone)
											CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - SET biHelp2PlayerDone")
										ENDIF
									ELSE
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - IS_OK_TO_PRINT_FREEMODE_HELP FAILING biHelp2PlayerDone")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					//-- Help for event start timer
					IF IS_BIT_SET(iBoolsBitSet, biHelp1Done)
						IF NOT IS_BIT_SET(iBoolsBitSet, biHelp3Done)
							IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InArea)
								IF IS_BIT_SET(serverBD.iServerBitSet, biS_AtLeastTwoPlayersOnMission)
									IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
										IF OK_TO_DISPLAY_HELP_FOR_MODE()
											tl15Label = GET_PENNED_IN_TIMER_HELP()
											IF OK_TO_DISPLAY_HELP_FOR_MODE()
												PRINT_HELP_NO_SOUND(tl15Label, DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) // Penned In will start when the time expires.
											ENDIF
											IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
												SET_FREEMODE_EVENT_HELP_BACKGROUND()
											ENDIF
											SET_BIT(iBoolsBitSet, biHelp3Done)
										
											CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Set biHelp3Done")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
				
				
				UPDATE_SPHERE(TRUE)
				
				MAINTAIN_START_MISSION_TIMER_CLIENT()
				
				MAINTAIN_START_321_COUNTDOWN()
				
				#IF FEATURE_STUNT_FM_EVENTS
				MAINTAIN_PENNED_IN_AIRCRAFT_BLIPS()	
				#ENDIF
			ELSE
				// Event should be hidden
				IF NOT IS_BIT_SET(iBoolsBitSet, biEventUiHidden)
					IF DOES_BLIP_EXIST(blipStart)
						REMOVE_BLIP(blipStart)
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Removing radar blips as should be hidden...")
					ENDIF
					
					REMOVE_AREA_BLIP()
					
					Clear_Any_Objective_Text_From_This_Script()
					
					SET_BIT(iBoolsBitSet, biEventUiHidden)
					
					SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableVehicleCombat, FALSE)
					
					IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InArea)
						CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InArea)
					ENDIF
					
					#IF FEATURE_STUNT_FM_EVENTS
					REMOVE_ALL_PENNED_IN_AIRCRAFT_BLIPS()	
					#ENDIF
				
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Set biEventUiHidden")
				ENDIF
			ENDIF
			
			IF serverBD.eStage >= eAD_COUNTDOWN
				IF NOT IS_BIT_SET(serverbd.iServerBitSet, biS_EventExpired)
					#IF FEATURE_STUNT_FM_EVENTS
					REMOVE_ALL_PENNED_IN_AIRCRAFT_BLIPS()	
					#ENDIF
					
					IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InArea)
						//-- Taking part...
						
						IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
						//	GB_COMMON_BOSS_MISSION_SETUP(FMMC_TYPE_BIKER_CAGED_IN, TRUE)
							IF NOT AM_I_IN_LAUNCH_GANG()
								//-- Jobs not initially blocked for non-launch gang players. Block them now.
								Block_All_MissionsAtCoords_Missions(TRUE)
							ENDIF
							GB_SET_PLAYER_AS_PERMANENT_ON_BOSS_MISSION()
							GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
							GB_SET_LOCAL_PLAYER_CRITICAL_TO_JOB(TRUE)
						ELSE
							COMMON_FREEMODE_AMBIENT_EVENTS_SET_UP(FMMC_TYPE_PENNED_IN, 0.0, 0, TRUE, TRUE, TRUE)
							FM_EVENT_SET_PLAYER_IS_CRITICAL_TO_FM_EVENT(TRUE)
							FM_EVENT_SET_PLAYER_WAITING_FOR_FM_EVENT_TO_START(FALSE)
						ENDIF
						
						
						FM_EVENT_SET_PLAYER_WAITING_FOR_FM_EVENT_TO_START(FALSE)
						
						IF DOES_BLIP_EXIST(blipStart)
							REMOVE_BLIP(blipStart)
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN REMoved blipStart")
						ENDIF
						Clear_Any_Objective_Text_From_This_Script()
						ACTIVATE_KILL_TICKERS(TRUE)
						
						MAINTAIN_VEHICLE_BOMB_REMOVAL()
						
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						AND NOT CAN_KNOCK_PED_OFF_VEHICLE(PLAYER_PED_ID())
							SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_DEFAULT)
						ENDIF
						
						relMyFmGroup = GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID())
						
						
						IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
							SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(),relPennedInPlayer )
								SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableVehicleCombat, TRUE)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableVehicleCombat, TRUE) 2")
						
						ELSE
							SET_PLAYER_DEFAULT_FRIENDLY_FIRE_OPTION(FALSE)
							SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, TRUE, TRUE, FALSE, FALSE)
							SET_BIT(iBoolsBitSet3, bi3_ExpProof)
							
							SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableVehicleCombat, FALSE)
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableVehicleCombat, FALSE) 3")
						ENDIF
						
						
						DISABLE_MP_PASSIVE_MODE(PMER_NON_PARTICIPANT_OF_MATCH, FALSE)
						
						CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
						
						
						
						
						
						START_NET_TIMER(timeMyParticipation)
						
						playerBD[PARTICIPANT_ID_TO_INT()].eStage = 	eAD_COUNTDOWN	
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  PLAYER STAGE eAD_IDLE -> eAD_COUNTDOWN AS biP_InArea    <----------     ") NET_NL()
					ELIF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaAsPassenger)
						//--Taking part as passenger...
						
						IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
						//	GB_COMMON_BOSS_MISSION_SETUP(FMMC_TYPE_BIKER_CAGED_IN, TRUE)
						ELSE
							COMMON_FREEMODE_AMBIENT_EVENTS_SET_UP(FMMC_TYPE_PENNED_IN, 0.0, 0, TRUE, TRUE)
						ENDIF
						
						
						Clear_Any_Objective_Text_From_This_Script()
						IF DOES_BLIP_EXIST(blipStart)
							REMOVE_BLIP(blipStart)
						ENDIF
						ACTIVATE_KILL_TICKERS(TRUE)
						CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
						
						FM_EVENT_SET_PLAYER_WAITING_FOR_FM_EVENT_TO_START(FALSE)
						
						playerBD[PARTICIPANT_ID_TO_INT()].eStage = 	eAD_COUNTDOWN	
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  PLAYER STAGE eAD_IDLE -> eAD_COUNTDOWN AS biP_InAreaAsPassenger    <----------     ") NET_NL()
					ELSE
						//-- Not taking part
						
					//	SET_PLAYER_IN_PENNED_IN_AREA(FALSE)
						
						IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_SHouldNotBeDamageable)
							SET_PLAYER_DEFAULT_FRIENDLY_FIRE_OPTION()
							SET_PED_CAN_BE_DRAGGED_OUT(PLAYER_PED_ID(), TRUE)
							CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_SHouldNotBeDamageable)
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - CLEAR biP_SHouldNotBeDamageable - Not taking part")
							
							#IF FEATURE_STUNT_FM_EVENTS
							IF serverBD.iPennedInType = PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY
								SET_LOCAL_PLAYER_AS_GHOST(FALSE)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN SET_LOCAL_PLAYER_AS_GHOST(FALSE) 5")
							ENDIF
							#ENDIF
							
											
							SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableVehicleCombat, FALSE)
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableVehicleCombat, FALSE) 4")
						ENDIF
						
					//	REMOVE_AREA_BLIP()
						
						IF DOES_BLIP_EXIST(blipStart)
							REMOVE_BLIP(blipStart)
						ENDIF
						
						FM_EVENT_SET_PLAYER_WAITING_FOR_FM_EVENT_TO_START(FALSE)
						
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PlayersDontDragMeOutOfCar, FALSE)
						
						Clear_Any_Objective_Text_From_This_Script()
						RELEASE_RACE_COUNTDOWN_UI(countdown)
						playerBD[PARTICIPANT_ID_TO_INT()].eStage = 	eAD_OFF_MISSION	
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  PLAYER STAGE eAD_IDLE -> eAD_OFF_MISSION     <----------     ") NET_NL()
					ENDIF
				ELSE
					//-- Event expired
					IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_SHouldNotBeDamageable)
						SET_PLAYER_DEFAULT_FRIENDLY_FIRE_OPTION()
						SET_PED_CAN_BE_DRAGGED_OUT(PLAYER_PED_ID(), TRUE)
						CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_SHouldNotBeDamageable)
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - CLEAR biP_SHouldNotBeDamageable - Not taking part")
						
						#IF FEATURE_STUNT_FM_EVENTS
						IF serverBD.iPennedInType = PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY
							SET_LOCAL_PLAYER_AS_GHOST(FALSE)
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN SET_LOCAL_PLAYER_AS_GHOST(FALSE) 6")
						ENDIF
						#ENDIF
						
											
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableVehicleCombat, FALSE)
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableVehicleCombat, FALSE) 5")
					ENDIF
					
				//	SET_PLAYER_IN_PENNED_IN_AREA(FALSE)
				
					#IF FEATURE_STUNT_FM_EVENTS
					REMOVE_ALL_PENNED_IN_AIRCRAFT_BLIPS()	
					#ENDIF
					
					REMOVE_AREA_BLIP()
					
					IF DOES_BLIP_EXIST(blipStart)
						REMOVE_BLIP(blipStart)
					ENDIF
					Clear_Any_Objective_Text_From_This_Script()
					RELEASE_RACE_COUNTDOWN_UI(countdown)
					
					FM_EVENT_SET_PLAYER_WAITING_FOR_FM_EVENT_TO_START(FALSE)
					
					SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PlayersDontDragMeOutOfCar, FALSE)
					
					playerBD[PARTICIPANT_ID_TO_INT()].eStage = 	eAD_OFF_MISSION	
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  PLAYER STAGE eAD_IDLE -> eAD_OFF_MISSION as biS_EventExpired   <----------     ") NET_NL()
					
				ENDIF
			ELIF (serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
				AND SHOULD_BAIL_FM_EVENT_DUE_TO_LOW_SESSION_NUMBERS())
			
				FM_EVENT_SET_PLAYER_WAITING_FOR_FM_EVENT_TO_START(FALSE)
				
			//	SET_PLAYER_IN_PENNED_IN_AREA(FALSE)
				
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CLEANUP
			
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN -  PLAYER STAGE = eAD_CLEANUP AS SHOULD_BAIL_FM_EVENT_DUE_TO_LOW_SESSION_NUMBERS    <----------      ")
			
			ENDIF
			
			MAINTAIN_DISABLE_CARGOBOB_ATTACH()
		BREAK
		
		CASE eAD_COUNTDOWN
			
	
			MAINTAIN_START_321_COUNTDOWN()
			
			MAINTAIN_DISABLE_CARGOBOB_ATTACH()
			
			UPDATE_SPHERE(NOT IS_BIT_SET(iBoolsBitSet, biChangeColour))
			
			#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_weaponWheel")
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ----------> [sc_weaponWheel] calling STOP_PLAYER_SHOOTING_THIS_FRAME - eAD_COUNTDOWN")
				ENDIF
			#ENDIF
	
			STOP_PLAYER_SHOOTING_THIS_FRAME()
			
			STOP_PLAYER_LEAVING_VEHICLE_THIS_FRAME()
			
			#IF FEATURE_STUNT_FM_EVENTS
			PROCESS_PENNED_IN_AI_EVENTS()
			
			MAINTAIN_TRYING_TO_ENTER_LOCKED_PENNED_IN_VEHICLE_HELP()
			#ENDIF
			
			IF serverBD.eStage >= eAD_IN_AREA
				START_NET_TIMER(timeMyParticpation)
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = 	eAD_IN_AREA	
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  PLAYER STAGE eAD_IDLE -> eAD_COUNTDOWN     <----------     ") NET_NL()
			ENDIF
		BREAK
		
		CASE eAD_IN_AREA
			IF telemetryStruct.m_startTime = 0
				telemetryStruct.m_startTime	= GET_CLOUD_TIME_AS_INT()
			ENDIF
			
			IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
				IF NOT IS_BIT_SET(iBoolsBitSet3, bi3_SetMaxWanted)
					SET_MAX_WANTED_LEVEL(0)
					SET_BIT(iBoolsBitSet3, bi3_SetMaxWanted)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  SET_BIT(iBoolsBitSet3, bi3_SetMaxWanted)")
				ELSE
					IF GET_MAX_WANTED_LEVEL() > 0
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  SET_MAX_WANTED_LEVEL(0) as it has increased somehow")
						SET_MAX_WANTED_LEVEL(0)
					ENDIF
				ENDIF
			ENDIF
			
			UPDATE_BLIP_POS_V2()
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("UPDATE_BLIP_POS")
			#ENDIF
			#ENDIF
			
			UPDATE_BLIP_RADIUS()
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("UPDATE_BLIP_RADIUS")
			#ENDIF
			#ENDIF
			
			UPDATE_SPHERE()	
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("UPDATE_SPHERE")
			#ENDIF
			#ENDIF
				
			MAINTAIN_LOCAL_PLAYER_IN_AREA()
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_LOCAL_PLAYER_IN_AREA")
			#ENDIF
			#ENDIF
			
			DO_PLAYERS_STILL_IN_COUNTER()
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("DO_PLAYERS_STILL_IN_COUNTER")
			#ENDIF
			#ENDIF
			
			MAINTAIN_DPAD_DOWN_LEADERBOARD()
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_DPAD_DOWN_LEADERBOARD")
			#ENDIF
			#ENDIF
			
			MAINTAIN_MY_PENNED_IN_VEHICLE()
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_DPAD_DOWN_LEADERBOARD")
			#ENDIF
			#ENDIF
			
			MAINTAIN_DISABLE_CARGOBOB_ATTACH()
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_DISABLE_CARGOBOB_ATTACH")
			#ENDIF
			#ENDIF
			
			MAINTAIN_PLAYER_MELEE_HELP()

			sHelp = GET_PENNED_IN_NEED_VEHICLE_HELP_TEXT()
			TEXT_LABEL_15 tl15NeedDriver 
			TEXT_LABEL_15 tl15Qstart 	
			TEXT_LABEL_15 tl15OutsideArea
			
			//-- Passenger help
			IF NOT IS_BIT_SET(iBoolsBitSet, biPassHelp)
				TEXT_LABEL_15 tl15Pass
				tl15Pass = GET_PENNED_IN_PASSENGER_TEXT()
				IF NOT SHOULD_UI_BE_HIDDEN()
					 IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaAsPassenger)
				 	
						IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE, FALSE)
							IF NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_START_OF_JOB)
								PRINT_HELP_NO_SOUND(tl15Pass, DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) // As a passenger, you are not actively taking part in the Penned In event. 
								IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
									SET_FREEMODE_EVENT_HELP_BACKGROUND()
								ENDIF
								SET_BIT(iBoolsBitSet, biPassHelp)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - set biPassHelp - 1")
							ENDIF
						ENDIF
					ELSE
					//	SET_BIT(iBoolsBitSet, biPassHelp)
					//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - set biPassHelp - 2")
					ENDIF
				ELSE
					tl15NeedDriver 	= GET_PENNED_IN_NEED_TO_BE_DRIVER_TEXT()
					tl15Qstart 		= GET_PENNED_IN_QUICK_START_TEXT()
					tl15OutsideArea	= GET_PENNED_IN_OUTSIDE_AREA_TEXT()
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(tl15Pass)
					OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(tl15OutsideArea)
					OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(tl15NeedDriver)
					OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sHelp)
					OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(tl15Qstart)
					OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PEN_MPLAY")
					OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PEN_START2")
						CLEAR_HELP()
					ENDIF
				ENDIF
			ELSE
				//-- Other player blip help
				IF iOtherPlayerBlipsBitset <> 0
					IF NOT IS_BIT_SET(iBoolsBitSet, biOtherPlayerHelp)
						IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE, FALSE)
							IF NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_START_OF_JOB)
								PRINT_HELP_NO_SOUND("PEN_STILLP", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) // Other players still in the event are shown by ~BLIP_TEMP_6~
								IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
									SET_FREEMODE_EVENT_HELP_BACKGROUND()
								ENDIF
								SET_BIT(iBoolsBitSet, biOtherPlayerHelp)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - set biOtherPlayerHelp ")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			
			
			//-- Falied to stay in area?
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_FailedToStayInArea)
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_OFF_MISSION
				Clear_Any_Objective_Text_From_This_Script()
			//	REMOVE_AREA_BLIP()
				
				IF DOES_BLIP_EXIST(blipVeh)
					REMOVE_BLIP(blipVeh)
				ENDIF
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  PLAYER STAGE = eAD_OFF_MISSION - biP_FailedToStayInArea    <----------     ") NET_NL()
			ELIF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaAsPassenger)
				IF IS_BIT_SET(iBoolsBitSet, biDoneExplodeVehicle)
					playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_OFF_MISSION
					Clear_Any_Objective_Text_From_This_Script()
				//	REMOVE_AREA_BLIP()
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  PLAYER STAGE = eAD_OFF_MISSION - biP_InAreaAsPassenger and  biDoneExplodeVehicle   <----------     ") NET_NL()
				ENDIF
			ENDIF
			
			IF serverBD.eStage = eAD_OFF_MISSION
			//	CHECK_FOR_REWARD()
				REMOVE_AREA_BLIP()
				Clear_Any_Objective_Text_From_This_Script()
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_OFF_MISSION
			ENDIF
			
//			IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
//				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_OFF_MISSION
//				Clear_Any_Objective_Text_From_This_Script()
//				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  PLAYER STAGE = eAD_OFF_MISSION - DEAD    <----------     ") NET_NL()
//			ENDIF
			
			IF IS_PLAYER_IN_CORONA()
				IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InArea)
					CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InArea)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - biP_InArea SET BUT CLEARING AS  IS_PLAYER_IN_CORONA")
				ENDIF
				
				IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaAsPassenger)
					CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaAsPassenger)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - biP_InAreaAsPassenger SET BUT CLEARING AS IS_PLAYER_IN_CORONA")
				ENDIF
				
				
				//--2483199
				IF HAS_PLAYER_BEEN_KNOCKED_OUT_OF_MODE()
					SET_PLAYER_HAS_BEEN_KNOCKED_OUT_OF_MODE(FALSE)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - HAS_PLAYER_BEEN_KNOCKED_OUT_OF_MODE SET BUT CLEARING AS IS_PLAYER_IN_CORONA")
				ENDIF
				
				
				
				SET_BIT(iBoolsBitSet2, bi2_EnteredCorona)
				
				REMOVE_AREA_BLIP()
				Clear_Any_Objective_Text_From_This_Script()
				
				
				SET_PLAYER_DEFAULT_FRIENDLY_FIRE_OPTION()
				CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_SHouldNotBeDamageable)
				
				#IF FEATURE_STUNT_FM_EVENTS
				IF serverBD.iPennedInType = PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY
					SET_LOCAL_PLAYER_AS_GHOST(FALSE)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN SET_LOCAL_PLAYER_AS_GHOST(FALSE) 6")
				ENDIF
				#ENDIF
				
											
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableVehicleCombat, FALSE)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableVehicleCombat, FALSE) 6")
											
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - CLEAR biSetPlayerInvincible - 24")
				
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_OFF_MISSION
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  PLAYER STAGE = eAD_OFF_MISSION - CORONA <----------     ") NET_NL()
			ELIF serverBD.eStage = eAD_EXPLODE_VEH
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_EXPLODE_VEH
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  PLAYER STAGE = eAD_EXPLODE_VEH    <----------     ") NET_NL()
			ELIF serverBD.eStage > eAD_EXPLODE_VEH
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CLEANUP
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  PLAYER STAGE = eAD_CLEANUP    <----------     ") NET_NL()
			ELIF (serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
				AND SHOULD_BAIL_FM_EVENT_DUE_TO_LOW_SESSION_NUMBERS())
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CLEANUP
			
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN -  PLAYER STAGE = eAD_CLEANUP AS SHOULD_BAIL_FM_EVENT_DUE_TO_LOW_SESSION_NUMBERS    <----------      ")
			ELIF WAS_LOCAL_PLAYER_PLAYER_REMOVED_FROM_EVENT()
				REMOVE_AREA_BLIP()
				Clear_Any_Objective_Text_From_This_Script()
				
				SET_PLAYER_DEFAULT_FRIENDLY_FIRE_OPTION()
				CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_SHouldNotBeDamageable)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - CLEAR biSetPlayerInvincible - 25")
				
				#IF FEATURE_STUNT_FM_EVENTS
				IF serverBD.iPennedInType = PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY
					SET_LOCAL_PLAYER_AS_GHOST(FALSE)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN SET_LOCAL_PLAYER_AS_GHOST(FALSE) 7")
				ENDIF
				#ENDIF
				
				
											
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableVehicleCombat, FALSE)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableVehicleCombat, FALSE) 7")
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_OFF_MISSION
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  PLAYER STAGE = eAD_OFF_MISSION as WAS_LOCAL_PLAYER_PLAYER_REMOVED_FROM_EVENT")
			ENDIF
		BREAK
		
		CASE eAD_OFF_MISSION	
			IF NOT SHOULD_UI_BE_HIDDEN()
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_CagedInBossQuit)
				AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_CagedInExpired)
				AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_CagedInNotEnoughPlayers)
					MAINTAIN_OFF_MISSION_HELP()
				
					MAINTAIN_TICKERS()
				ENDIF
				
				MAINTAIN_DISABLE_CARGOBOB_ATTACH()
				
				#IF FEATURE_STUNT_FM_EVENTS
				PROCESS_PENNED_IN_AI_EVENTS()
				
				MAINTAIN_TRYING_TO_ENTER_LOCKED_PENNED_IN_VEHICLE_HELP()
				#ENDIF
			
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InArea)
				OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_FailedToStayInArea)
				OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaAsPassenger)
					IF serverBD.eStage > eAD_COUNTDOWN
					AND serverBD.estage < eAD_OFF_MISSION
						//IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InArea)
							IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_SetBlipForNonParts)
								IF DOES_BLIP_EXIST(blipArea)
									SET_BLIP_COLOUR(blipArea, BLIP_COLOUR_GREY)
									SET_BIT(iBoolsBitSet2, bi2_SetBlipForNonParts)
								ENDIF
							ENDIF
						//ENDIF
						UPDATE_BLIP_POS_V2()
						UPDATE_BLIP_RADIUS()
					ENDIF
					IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InArea)
					OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InAreaAsPassenger) 
						UPDATE_SPHERE(FALSE, TRUE)
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(iBoolsBitSet2, bi2_SetBlipForNonParts)
					IF DOES_BLIP_EXIST(blipArea)
						REMOVE_BLIP(blipArea)
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - REMOVED blipArea AS HIDDEN")
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(iBoolsBitSet3, bi3_SetMaxWanted)
				SET_MAX_WANTED_LEVEL(5)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  eAD_OFF_MISSION SET_MAX_WANTED_LEVEL(5)")
				CLEAR_BIT(iBoolsBitSet3, bi3_SetMaxWanted)
			ENDIF
	
			//--2483199
			IF HAS_PLAYER_BEEN_KNOCKED_OUT_OF_MODE()
				IF IS_PLAYER_IN_CORONA()
					SET_PLAYER_HAS_BEEN_KNOCKED_OUT_OF_MODE(FALSE)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - HAS_PLAYER_BEEN_KNOCKED_OUT_OF_MODE SET BUT CLEARING AS IS_PLAYER_IN_CORONA (2)")
				ENDIF
			ENDIF
				
			CHECK_FOR_REWARD()
			
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InArea)
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneReward)
					MAINTAIN_DPAD_DOWN_LEADERBOARD(TRUE)
				ELSE
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - NOT DOING DPAD_DOWN LB AS biP_DoneReward ")
				ENDIF
			ELSE
				MAINTAIN_NON_PART_IN_PART_VEHICLE()
				
				
			ENDIF
			
		//	MAINTAIN_NON_PART_PASSIVE_MODE()
			
			IF IS_BIT_SET(iBoolsBitSet, biDoneTimeCycle)
				IF IS_AUDIO_SCENE_ACTIVE("MP_Player_Inside_Bubble_Scene")
					STOP_AUDIO_SCENE("MP_Player_Inside_Bubble_Scene")
				ENDIF
				IF IS_AUDIO_SCENE_ACTIVE("MP_Player_Outside_Bubble_Scene")
					STOP_AUDIO_SCENE("MP_Player_Outside_Bubble_Scene")
				ENDIF
				ANIMPOSTFX_STOP("pennedIn")
				ANIMPOSTFX_PLAY("PennedInOut", 0, FALSE)
				CLEAR_BIT(iBoolsBitSet, biDoneTimeCycle)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - eAD_OFF_MISSION - CLEAR_TIMECYCLE_MODIFIER()")
			ENDIF
			
//			IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_PENNED_IN
//				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = -1
//			ENDIF
			
			IF serverBD.iPennedInType != PENNED_IN_TYPE_CAGED_IN
				IF FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT(PLAYER_ID())
					FM_EVENT_SET_PLAYER_IS_CRITICAL_TO_FM_EVENT(FALSE)
					ACTIVATE_KILL_TICKERS(FALSE)
					SET_BIT(iBoolsBitSet2, bi2_CommonCleanup)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - eAD_OFF_MISSION - SET bi2_CommonCleanup ACTIVATE_KILL_TICKERS(FALSE)" )
					COMMON_FREEMODE_AMBIENT_EVENTS_CLEAN_UP(FMMC_TYPE_PENNED_IN, DEFAULT, TRUE)
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_CommonCleanup)
					ACTIVATE_KILL_TICKERS(FALSE)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - eAD_OFF_MISSION - SET bi2_CommonCleanup ACTIVATE_KILL_TICKERS(FALSE) - caged in" )
					SET_BIT(iBoolsBitSet2, bi2_CommonCleanup)
				ENDIF
			ENDIF
			
			//-- For Chris' spec cam
			IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_DoneKnockedOut)
				IF IS_BIT_SET(iBoolsBitSet2, bi2_KnockedOutAsDead)
				OR IS_BIT_SET(iBoolsBitSet, biDestWhenKnockedOut)
					SET_BIT(iBoolsBitSet2, bi2_DoneKnockedOut)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - eAD_OFF_MISSION - SET bi2_DoneKnockedOut")
				ENDIF
			ENDIF
			
			IF SHOULD_SPECTATING_END()
				IF MOVE_PLAYER_BACK_AFTER_HELI_SPECTATE()
					IF serverBD.eStage = eAD_EXPLODE_VEH
						playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_EXPLODE_VEH
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  PLAYER STAGE = eAD_EXPLODE_VEH    <----------     ") NET_NL()
					ELIF serverBD.eStage > eAD_EXPLODE_VEH
						playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CLEANUP
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  PLAYER STAGE = eAD_CLEANUP    <----------     ") NET_NL()		
					ELSE
						CLEANUP_HELI_SPECTATE_MODE()
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE eAD_EXPLODE_VEH
			#IF FEATURE_STUNT_FM_EVENTS
			MAINTAIN_EXPLODE_AIRCRAFT_CLIENT()
			#ENDIF
			IF serverBD.eStage > eAD_EXPLODE_VEH
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CLEANUP
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  PLAYER STAGE = eAD_CLEANUP    <----------     ") NET_NL()
			ENDIF
		BREAK
		
		CASE eAD_CLEANUP
		
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  SCRIPT CLEANUP B     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
			
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	CLOSE_SCRIPT_PROFILE_MARKER_GROUP() // 
	#ENDIF
	#ENDIF
ENDPROC


PROC PROCESS_SCTV_PLAYER_PENNED_IN_CLIENT()
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
		IF IS_BIT_SET(iBoolsBitSet2, bi2_resetForSctv)
			CLEAR_BIT(iBoolsBitSet2, bi2_resetForSctv)
		ENDIF
		
		EXIT
	ENDIF
	
	IF DRAW_CHALLENGE_DPAD_FOR_DEV_SPECTATOR()
		MAINTAIN_DPAD_DOWN_LEADERBOARD()
	ENDIF
	
	PLAYER_INDEX playerSPec = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget
	PARTICIPANT_INDEX partSpec
	INT iPartSpec
	PENNED_IN_STAGE_ENUM partSpecStage
	#IF IS_DEBUG_BUILD
		BOOL bPlayerValid
	#ENDIF
	
	//Kill the pit set if they were in the script then became SCTV
	IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_resetForSctv)
		IF playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet != 0
			playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet = 0
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  [PROCESS_SCTV_PLAYER_PENNED_IN_CLIENT]  playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet != 0 - Clearing")
		ENDIF
		SET_BIT(iBoolsBitSet2, bi2_resetForSctv)
	ENDIF
	
	//Clear the objective text
	IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_SCTV_Clear_God_Text)
		SET_BIT(iBoolsBitSet2, bi2_SCTV_Clear_God_Text)	
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  [PROCESS_SCTV_PLAYER_PENNED_IN_CLIENT]  Clear_Any_Objective_Text_From_This_Script")
		Clear_Any_Objective_Text_From_This_Script()
	ENDIF
	
	// Cleanup heli spectate cam
	IF ( iHeliClientProgress = ciCLIENT_HELI_END )
		CLEANUP_HELI_SPECTATE_MODE()
	ENDIF
	
	IF NETWORK_IS_PLAYER_ACTIVE(playerSPec)
		IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerSPec)
			partSpec = NETWORK_GET_PARTICIPANT_INDEX(playerSPec)
			iPartSpec = NATIVE_TO_INT(partSpec)
			partSpecStage = playerBD[iPartSpec].eStage
			
			#IF IS_DEBUG_BUILD
				bPlayerValid = TRUE
			#ENDIF
			
			IF partSpecStage > eAD_IDLE
				IF DOES_BLIP_EXIST(blipStart)
					REMOVE_BLIP(blipStart)
				ENDIF
			ENDIF
			
			SWITCH partSpecStage
				CASE eAD_IDLE
					// DRAW SPHERE
					
					UPDATE_SPHERE(TRUE)
				
					MAINTAIN_START_MISSION_TIMER_CLIENT()
				
					MAINTAIN_START_321_COUNTDOWN()
				BREAK
				
				CASE eAD_COUNTDOWN
					MAINTAIN_START_321_COUNTDOWN()
					
					UPDATE_SPHERE(NOT IS_BIT_SET(iBoolsBitSet, biChangeColour))
				BREAK
				
				CASE eAD_IN_AREA
					UPDATE_BLIP_POS_V2()
					
					UPDATE_BLIP_RADIUS()
					
					UPDATE_SPHERE()	
					
					DO_PLAYERS_STILL_IN_COUNTER()
					
					IF IS_BIT_SET(playerBD[iPartSpec].iPlayerBitSet, biP_OutCoronaTimeStarted)
						IF NOT IS_BIT_SET(playerBD[iPartSpec].iPlayerBitSet, biP_FailedToStayInArea)
							IF NOT HAS_NET_TIMER_STARTED(OutOfAreaTimer)
								START_NET_TIMER(OutOfAreaTimer)
							ENDIF
							
							IF HAS_NET_TIMER_STARTED(OutOfAreaTimer)
								IF (GET_KNOCKED_OUT_TIME()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(OutOfAreaTimer)) >= 0
									DRAW_GENERIC_TIMER((GET_KNOCKED_OUT_TIME()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(OutOfAreaTimer)), "PEN_ELM", 0, TIMER_STYLE_USEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_DONTCARE, DEFAULT, HUD_COLOUR_RED, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_RED)			
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF HAS_NET_TIMER_STARTED(OutOfAreaTimer)
							RESET_NET_TIMER(OutOfAreaTimer)
						ENDIF
					ENDIF
					
					CHECK_FOR_REWARD()
				BREAK
				
				CASE eAD_OFF_MISSION
					IF IS_BIT_SET(playerBD[iPartSpec].iPlayerBitSet, biP_InArea)
					OR IS_BIT_SET(playerBD[iPartSpec].iPlayerBitSet, biP_InAreaAsPassenger) 
						UPDATE_SPHERE(FALSE, TRUE)
					ENDIF
					
					IF DOES_BLIP_EXIST(blipArea)
						SET_BLIP_COLOUR(blipArea, BLIP_COLOUR_GREY)
					ENDIF
					
					CHECK_FOR_REWARD()
				BREAK
				DEFAULT
					IF IS_BIT_SET(playerBD[iPartSpec].iPlayerBitSet, biP_DoneReward)
						REMOVE_AREA_BLIP()
					ENDIF
				BREAK
			ENDSWITCH

		ENDIF
	ENDIF
	
	IF serverBD.eStage > eAD_EXPLODE_VEH
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  [PROCESS_SCTV_PLAYER_PENNED_IN_CLIENT] SCRIPT_CLEANUPP    <----------     ") NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
		
	#IF IS_DEBUG_BUILD
		IF NOT HAS_NET_TIMER_STARTED(timeSCTVDebug)
		OR (HAS_NET_TIMER_STARTED(timeSCTVDebug) AND HAS_NET_TIMER_EXPIRED(timeSCTVDebug, 5000))
			IF bPlayerValid
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  [PROCESS_SCTV_PLAYER_PENNED_IN_CLIENT]   I'm spectating player ", GET_PLAYER_NAME(playerSPec), " participant index = ", iPartSpec, " Their stage = ", ENUM_TO_INT(partSpecStage))
			ENDIF
			RESET_NET_TIMER(timeSCTVDebug)
			START_NET_TIMER(timeSCTVDebug)
		ENDIF
	#ENDIF
ENDPROC



//PURPOSE: Process the stages for the Server
PROC PROCESS_PENNED_IN_SERVER()

	
	SWITCH serverBD.eStage
		CASE eAD_IDLE
			
			
			MAINTAIN_START_MISSION_TIMER_SERVER()
			
			IF IS_BIT_SET(serverbd.iServerBitSet, biS_MissionStartExpired)
				IF IS_BIT_SET(serverbd.iServerBitSet, biS_AtLeastOnePlayerOnMission)	
					SET_BIT(serverbd.iServerBitSet, biS_Start321Countdown)
					serverBD.eStage = eAD_COUNTDOWN //eAD_IN_AREA
					CLEAR_AREA_OF_PROJECTILES(vNodes[0], (GET_PEN_START_SIZE() + 50), TRUE)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Server stage eAD_IDLE -> eAD_COUNTDOWN AS COUNTDOWN EXPIRED")
				ENDIF
			ELIF IS_BIT_SET(serverbd.iServerBitSet, biS_PlayerLimitReached)
				SET_BIT(serverbd.iServerBitSet, biS_Start321Countdown)
				serverBD.eStage = eAD_COUNTDOWN //eAD_IN_AREA
				CLEAR_AREA_OF_PROJECTILES(vNodes[0], (GET_PEN_START_SIZE() + 50), TRUE)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Server stage eAD_IDLE -> eAD_COUNTDOWN as MAX PLAYERS REACHED")
			ENDIF
			
			IF IS_BIT_SET(serverbd.iServerBitSet, biS_EventExpired)
				serverBD.eStage = eAD_OFF_MISSION
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Server stage eAD_IDLE -> eAD_OFF_MISSION as biS_EventExpired")
			ENDIF
			
			IF IS_BIT_SET(serverbd.iServerBitSet, biS_CagedInExpired)
				CHECK_FOR_REWARD()
				serverBD.eStage = eAD_OFF_MISSION
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Server stage eAD_IDLE -> eAD_OFF_MISSION as biS_CagedInExpired")
			ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_CagedInNotEnoughPlayers)
				serverBD.eStage = eAD_OFF_MISSION
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Server stage eAD_IDLE -> eAD_OFF_MISSION as biS_CagedInNotEnoughPlayers")
			ENDIF
		BREAK
		
		CASE eAD_COUNTDOWN
			IF IS_BIT_SET(serverbd.iServerBitSet, biS_CountdownExpired)
				serverBD.eStage = eAD_IN_AREA //
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Server stage eAD_COUNTDOWN -> eAD_IN_AREA")
			ENDIF
		BREAK
		
		CASE eAD_IN_AREA
			MAINTAIN_MOVE_MULTIPLIER_HOST()
			
			UPDATE_BLIP_POS_V2(FALSE)
			
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneWon)
				serverBD.eStage = eAD_OFF_MISSION
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Server stage eAD_IN_AREA -> eAD_OFF_MISSION as biS_SomeoneWon")
			ENDIF
			
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_WaitForWinnerExpired)
				serverBD.eStage = eAD_OFF_MISSION
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Server stage eAD_IN_AREA -> eAD_OFF_MISSION as biS_WaitForWinnerExpired")
			ENDIF
		BREAK
		
		CASE eAD_OFF_MISSION
			IF IS_BIT_SET(serverbd.iServerBitSet, biS_CagedInExpired)
				CHECK_FOR_REWARD()
			ENDIF
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_EveryoneFinished)
				#IF FEATURE_STUNT_FM_EVENTS
				IF serverBD.iPennedInType = PENNED_IN_TYPE_AIRCRAFT_HELI_ONLY
					serverBD.eStage = eAD_EXPLODE_VEH
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Server stage eAD_OFF_MISSION -> eAD_EXPLODE_VEH as biS_EveryoneFinished")	
				ENDIF
				#ENDIF
				
				IF serverBD.eStage = eAD_OFF_MISSION
					serverBD.eStage = eAD_CLEANUP
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Server stage eAD_OFF_MISSION -> eAD_CLEANUP as biS_EveryoneFinished")
				ENDIF
			ENDIF
		BREAK
		
		CASE eAD_EXPLODE_VEH
			#IF FEATURE_STUNT_FM_EVENTS
			MAINTAIN_EXPLODE_AIRCRAFT_SERVER()
			
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllAircraftDestroyed)
				serverBD.eStage = eAD_CLEANUP
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     PENNED IN - Server stage eAD_EXPLODE_VEH -> eAD_CLEANUP as biS_AllAircraftDestroyed")
			ENDIF
			#ENDIF
		BREAK
		
		CASE eAD_CLEANUP
		BREAK
	ENDSWITCH
ENDPROC



#IF IS_DEBUG_BUILD
	
PROC UPDATE_WIDGETS()
	INT i
	INT iR, iG, iB, iA
	
	IF bWdDrawNodePos
		GET_HUD_COLOUR(HUD_COLOUR_BLUE, iR, iG, iB, iA)
		
		REPEAT MAX_NODES i
			IF NOT ARE_VECTORS_EQUAL(vNodes[i], << 0.0, 0.0, 0.0 >>)
				DRAW_MARKER( MARKER_CYLINDER,
				vNodes[i],
				<<0,0,0>>,
				<<0,0,0>>,
				<<1.0, 1.0,100.0>>,
				iR,
				iG,
				iB,
				iA )
			ENDIF
			
		ENDREPEAT
		
		IF DOES_BLIP_EXIST(blipWDRoute[0]) ENDIF
	ENDIF
	
	IF bWdPlotRoute
		i = 0
		REPEAT ROUTE_MAX i
			IF NOT ARE_VECTORS_EQUAL(vWdRoute[i], << 0.0, 0.0, 0.0>>)
				blipWDRoute[i] = ADD_BLIP_FOR_COORD(vWdRoute[i])
			ENDIF
		ENDREPEAT
	ENDIF
	
	IF bWdTestModifier
		SET_TRANSITION_TIMECYCLE_MODIFIER(GET_CONTENTS_OF_TEXT_WIDGET(widgetText), 1.0)
	//	SET_NEXT_PLAYER_TCMODIFIER(GET_PLAYER_PED_TIMECYCLE_MODIFIER(CHAR_TREVOR))
	//	SET_PLAYER_TCMODIFIER_TRANSITION(0.0)
	//	SET_CURRENT_PLAYER_TCMODIFIER(GET_PLAYER_PED_TIMECYCLE_MODIFIER(CHAR_TREVOR))
		bWdTestModifier = FALSE
	ENDIF
	
	IF bWdTestPostFx
		bWdTestPostFx = FALSE 
	//	ANIMPOSTFX_PLAY("pennedIn", 0, TRUE)
		ANIMPOSTFX_PLAY("PennedInOut", 0, FALSE)
	ENDIF
	
	IF bSTopPostFx
		ANIMPOSTFX_STOP("pennedIn")
		bSTopPostFx = FALSE
	ENDIF
	IF bWdClearTimecycle
		CLEAR_TIMECYCLE_MODIFIER()
		bWdClearTimecycle = FALSE
	ENDIF
	
	IF bWdSHowDistances
		fWdDistTravelled = GET_DISTANCE_MARKER_HAS_TRAVELLED()
		fWdSTillToGo = GET_DISTANCE_MARKER_STILL_HAS_TO_TRAVEL()
	ENDIF
	
	IF bWdTestCountdown
		SET_BIT(serverBD.iServerBitSet, biS_Start321Countdown)
		MAINTAIN_START_321_COUNTDOWN()
	ENDIF
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF bWdMoveToExpVeh
			bWdMoveToExpVeh =  FALSE
			IF serverBD.eStage < eAD_EXPLODE_VEH
				serverBD.eStage = eAD_EXPLODE_VEH
			ENDIF
		ENDIF
	ENDIF
	// MARKER_CYLINDER
ENDPROC
#ENDIF

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                      //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT(MP_MISSION_DATA missionScriptArgs)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
		#IF IS_DEBUG_BUILD	
			NET_PRINT_TIME() NET_PRINT("MP: Starting mission ") NET_NL()
		#ENDIF
		
		// Vehry out all the initial game starting duties. 
		PROCESS_PRE_GAME(missionScriptArgs)	
		
		// Setup widgets. Make sure this is done after PROCESS_PRE_GAME()
		#IF IS_DEBUG_BUILD
			CREATE_WIDGETS()
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD NET_LOG("PENNED IN -  NETWORK_IS_GAME_IN_PROGRESS = FALSE")	#ENDIF
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  MISSION END - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP P     <----------     ") NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
	
	// Main loop
	WHILE TRUE

		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			SCRIPT_PROFILER_START_OF_FRAME()
		#ENDIF
		#ENDIF		
		
		// If we have a match end event, bail
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		OR SHOULD_MP_AMBIENT_SCRIPT_TERMINATE(MPAM_TYPE_PENNED_IN)
			#IF IS_DEBUG_BUILD NET_LOG("SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")	#ENDIF
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  MISSION END - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE - SCRIPT CLEANUP B     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		
		IF IS_KILL_CURRENT_FREEMODE_EVENT_GLOBAL_SET() 
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  MISSION END - IS_KILL_CURRENT_FREEMODE_EVENT_GLOBAL_SET - SCRIPT CLEANUP F     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		
		//Call the common FM Events every frame callse
		FM_EVENT_COMMON_EVERY_FRAME_CALLS()
		
		IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
			IF GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION()
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  MISSION END  - GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION = TRUE")
				GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_FORCED, DEFAULT, !IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InArea))
				SCRIPT_CLEANUP()
			ENDIF
		ENDIF
		
		// Deal with the debug
		#IF IS_DEBUG_BUILD		
			UPDATE_WIDGETS()
		#ENDIF		
		 
		// -----------------------------------
		// Process your game logic.....
		SWITCH GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT())
			
			// Wait untill the server gives the all go before moving on
			CASE GAME_STATE_INI
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					IF DO_ALL_PENNED_IN_ENTITIES_EXIST()
						IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_SHouldHideEvent)
							IF SHOULD_HIDE_AMBIENT_EVENT(ciPI_HIDE_MENU_ITEM_AMBIENT_EVENTS_PENNED_IN)
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_SHouldHideEvent)
								PRINTLN("     ---------->     PENNED IN -  SET biP_SHouldHideEvent   <---------- ")
							ENDIF
						ENDIF
						PRINTLN("     ---------->     PENNED IN -  Client about to fill nodes...   <---------- ")
						FILL_NODES()
						vMyBlipPos = vNodes[0]
						fMyBlipSize = GET_PEN_START_SIZE()
						fMyOldBlipSize = fMyBlipSize
						iMyCurrentNode = 0
						fMyBlipMoveMultiplier = GET_PEN_START_SPEED_MODIFIER()
						
						IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
							MAINATAIN_LAUNCH_GANG_HUD_COLOUR()
						ENDIF
						
						IF NOT SHOULD_UI_BE_HIDDEN()
							ADD_AREA_BLIP(GET_START_COORDS_FOR_AREA(), TRUE)
							ADD_START_BLIP()
						ENDIF
						
						FM_EVENT_BLOCK_SPECIFIC_GANG_ATTACKS(FMMC_TYPE_PENNED_IN, serverBD.iLoc)
						
						IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN	
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - GB_SET_COMMON_TELEMETRY_DATA_ON_START")
							GB_SET_COMMON_TELEMETRY_DATA_ON_START(DEFAULT, DEFAULT, (serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN), FMMC_TYPE_BIKER_CAGED_IN)
							
							IF AM_I_IN_LAUNCH_GANG()
								SET_BIT(iBoolsBitSet3, bi3_OnLaunchGang)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN - bi3_OnLaunchGang")
							ENDIF
						ENDIF
						
						
						
						
						playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING    <----------     ") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_RUNNING")	#ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN CLIENT WAITING FOR DO_ALL_PENNED_IN_ENTITIES_EXIST")
						ENDIF
						#ENDIF
					ENDIF
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 1    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY")	#ENDIF
				ENDIF
			BREAK
			
			// Main gameplay state.
			CASE GAME_STATE_RUNNING
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					MAINTAIN_MAX_PARTICIPANT_CHECKS_CLIENT()
					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
						ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MAX_PARTICIPANT_CHECKS_CLIENT")
					#ENDIF
					#ENDIF
					
					PROCESS_PENNED_IN_CLIENT()
					
					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
						ADD_SCRIPT_PROFILE_MARKER("PROCESS_PENNED_IN_CLIENT")
					#ENDIF
					#ENDIF
					
					MAINTAIN_CLEAR_FORMATION()
					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
						ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CLEAR_FORMATION")
					#ENDIF
					#ENDIF
					
					MAINTAIN_SET_MY_VEHICLE_PROOFS()
					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
						ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CLEAR_FORMATION")
					#ENDIF
					#ENDIF
					
					MAINTAIN_DISTANCE_CHECKS_FOR_VARIATION()
					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
						ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_DISTANCE_CHECKS_FOR_VARIATION")
					#ENDIF
					#ENDIF
					
					MAINTAIN_SPECTATOR_CHOPPER_CLIENT()
						
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
						ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SPECTATOR_CHOPPER_CLIENT")
					#ENDIF
					#ENDIF
					
					PROCESS_SCTV_PLAYER_PENNED_IN_CLIENT()
					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
						ADD_SCRIPT_PROFILE_MARKER("PROCESS_SCTV_PLAYER_PENNED_IN_CLIENT")
					#ENDIF
					#ENDIF
					
					MAINTAIN_PENNED_IN_MUSIC()
			
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
						ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PENNED_IN_MUSIC")
					#ENDIF
					#ENDIF
					
					MAINTAIN_PLAYER_DISABLE_AIRSTRIKES()
					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
						ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PLAYER_DISABLE_AIRSTRIKES")
					#ENDIF
					#ENDIF
					
					MAINTAIN_HIDE_BOUNTY()
					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
						ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_HIDE_BOUNTY")
					#ENDIF
					#ENDIF
					
					#IF IS_DEBUG_BUILD
					CLIENT_DO_DEBUG_WARP()
					#ENDIF
					
					MAINTAIN_OBJECTIVE_TEXT_FOR_SCTV()
					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
						ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_OBJECTIVE_TEXT_FOR_SCTV")
					#ENDIF
					#ENDIF

					
					
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 2   <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY")	#ENDIF
				
				ENDIF
			BREAK
			
			CASE GAME_STATE_TERMINATE_DELAY 
				SET_MISSION_FINISHED(serverBD.MissionTerminateDelayTimer)
		      	IF IS_MISSION_READY_TO_CLEANUP(serverBD.MissionTerminateDelayTimer)
		            playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 3    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 3")	#ENDIF
		      	ENDIF
			BREAK
			
			// The game stage the local player is placed when we want him to leave a mission
			CASE GAME_STATE_LEAVE
				//LEAVE_MY_MP_MISSION(thisMission)
				playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 2    <----------     ") NET_NL()
				#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 2")	#ENDIF
			FALLTHRU
			
			//Cleans up then terminates the mission
			CASE GAME_STATE_END
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  SCRIPT CLEANUP A     <----------     ") NET_NL()
				SCRIPT_CLEANUP()
			BREAK

		ENDSWITCH
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			
			IF serverBD.iPennedInType = PENNED_IN_TYPE_CAGED_IN
				IF GB_SHOULD_KILL_ACTIVE_BOSS_MISSION()
					IF GET_SERVER_MISSION_STATE() < GAME_STATE_END
						serverBD.iServerGameState = GAME_STATE_END
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  serverBD.iServerGameState = GAME_STATE_END AS GB_SHOULD_KILL_ACTIVE_BOSS_MISSION    <----------     ") NET_NL()
					
					ENDIF
				ENDIF
			ENDIF
			
			SWITCH GET_SERVER_MISSION_STATE()
				
				
				CASE GAME_STATE_INI
					//serverBD.iLoc = GET_RANDOM_START_LOCATION()
					IF GET_ROUTE_TO_USE(serverBD.iLoc)
					
						SET_PENNED_IN_TYPE_FROM_ROUTE()
						
						IF DO_ALL_PENNED_IN_ENTITIES_EXIST()
							
							
							PRINTLN("     ---------->     PENNED IN -  Server about to fill nodes...   <---------- ")
							FILL_NODES()
							
							serverBD.heliStruct.playerToSpectate = INVALID_PLAYER_INDEX()
							serverBD.fRadius = GET_PEN_START_SIZE()
							serverBD.fMoveMultiplier = GET_PEN_START_SPEED_MODIFIER()
							serverBD.iServerGameState = GAME_STATE_RUNNING
							
							START_NET_TIMER(serverBD.timeSinceLaunched)
							
							BROADCAST_SET_LAST_FM_EVENT_VARIATION(FM_EVENT_GET_VARIATION_FROM_EVENT_TYPE_AND_SUB_TYPE(FMMC_TYPE_PENNED_IN, serverBD.iLoc))
							
							PRINTLN("     ---------->     PENNED IN -  serverBD.iServerGameState = GAME_STATE_RUNNING    <---------- ")
							#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_RUNNING")	#ENDIF
						ELSE
							PRINTLN("     ---------->     PENNED IN - HOST WAITING FOR DO_ALL_PENNED_IN_ENTITIES_EXIST")
						ENDIF
					ENDIF
				BREAK
				
				// Look for game end conditions
				CASE GAME_STATE_RUNNING
					
					MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
						ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER")
					#ENDIF
					#ENDIF
					
					PROCESS_PENNED_IN_SERVER()
					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
						ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER")
					#ENDIF
					#ENDIF
					
					MAINTAIN_SPECTATOR_CHOPPER_SERVER()
					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
						ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SPECTATOR_CHOPPER_SERVER")
					#ENDIF
					#ENDIF

					#IF IS_DEBUG_BUILD
						IF bHostEndMissionNow
						OR MPGlobalsAmbience.bKillActiveEvent
						OR IS_DEBUG_KEY_JUST_PRESSED(KEY_X, KEYBOARD_MODIFIER_SHIFT, "SKIP")
							MPGlobalsAmbience.bKillActiveEvent = FALSE
							serverBD.iServerGameState = GAME_STATE_END
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  serverBD.iServerGameState = GAME_STATE_END 1    <----------     ") NET_NL()
							#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 1")	#ENDIF
						ENDIF
						
						// Debug warp players
						SERVER_MANAGE__DEBUG_WARP()
					#ENDIF	
					
					IF HAVE_MISSION_END_CONDITIONS_BEEN_MET()
						serverBD.iServerGameState = GAME_STATE_END
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     PENNED IN -  serverBD.iServerGameState = GAME_STATE_END 4    <----------     ") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 4")	#ENDIF
					ENDIF
					
				BREAK
				
				CASE GAME_STATE_END
				BREAK
				
			ENDSWITCH
			
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			SCRIPT_PROFILER_END_OF_FRAME()
		#ENDIF
		#ENDIF		
		
	ENDWHILE
	
	
ENDSCRIPT
