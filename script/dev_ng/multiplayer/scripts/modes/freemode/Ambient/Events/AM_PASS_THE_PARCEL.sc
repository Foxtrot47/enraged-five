//////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_PASS_THE_PARCEL.sc													//
// Description: Players compete to be the last one in a vehicle at the end of the time.	//
// Written by:  Martin McMillan															//
// Date: 18/05/2015																		//
//////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "net_mission.sch"


//----------------------
//	INCLUDES 
//----------------------
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "flow_public_core_override.sch"
USING "net_wait_zero.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"
USING "net_ambience_launcher.sch"
USING "script_conversion.sch"

USING "DM_Leaderboard.sch"
USING "net_challenges.sch"

USING "am_common_ui.sch"
USING "freemode_events_header.sch"
USING "net_rank_ui.sch"
//----------------------
//	ENUM
//----------------------

//----------------------
//	CONSTANTS
//----------------------

CONST_INT PP_REWARD_TIME				20000
CONST_INT SERVER_TERMINATE_DELAY		1000
CONST_INT VEHICLE_EXPLOSION_TIME		10000
CONST_INT INVINCIBLE_TIME				20000

CONST_INT TOTAL_PTP_VEHICLES	7
CONST_INT TOTAL_ROUTES_SANCHEZ	2
CONST_INT TOTAL_START_COORDS    7

CONST_INT VEH_EXP_RANGE	50

//----------------------
//	Local Bitset
//----------------------

INT iLocalBitset

CONST_INT LOCAL_BS_MUSIC_TRIGGERED		0
CONST_INT LOCAL_BS_INITIAL_SHARD_SHOWN	1
CONST_INT LOCAL_BS_DONE_SETUP			2
CONST_INT LOCAL_BS_SD_MUSIC_TRIGGERED	3


//----------------------
//	STRUCT
//----------------------




//----------------------
//	VARIABLES
//----------------------

SCRIPT_TIMER iServerTerminateTimer

INT iServerPart
INT iClientPart

INT iHelpBS

BLIP_INDEX VehBlip

BOOL bMultipliersActive
BOOL bCustomSpawn

//SCRIPT_TIMER blipFlashTimer

STRUCT_FM_EVENTS_END_UI sEndUiVars

SCRIPT_TIMER iGarageCheckTimer
SCRIPT_TIMER iGarageSyncDelayTimer

//----------------------
//	GAME STATE
//----------------------

CONST_INT GAME_STATE_INIT 									0
CONST_INT GAME_STATE_RUNNING								1
CONST_INT GAME_STATE_TERMINATE_DELAY						2
CONST_INT GAME_STATE_END									3

ENUM AMPP_RUN_STAGE
	AMPP_START = 0,
	AMPP_GET,
	AMPP_HOLD,
	AMPP_REWARDS,
	AMPP_EXPLODE,
	AMPP_END
ENDENUM

//----------------------
//	BROADCAST VARIABLES
//----------------------

// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData

	INT iServerGameState = GAME_STATE_INIT
	
	INT iServerBitSet	
	
	INT iEndTime	
	
	AMPP_RUN_STAGE eAMPP_Stage = AMPP_START
	
	TIME_DATATYPE StartTime
	
	SCRIPT_TIMER eventTimer
	SCRIPT_TIMER rewardTimer
	SCRIPT_TIMER ExplodeVehTimer
	SCRIPT_TIMER timeoutTimer
	SCRIPT_TIMER invincibilityTimer
	
	//Vehicle
	MODEL_NAMES VehModel
	NETWORK_INDEX niVeh
	
	//Location
	VECTOR vStartLocation
	FLOAT fStartHeading
	
	PLAYER_INDEX piDriver
	
	BOOL bOpenAirportGates
	
	INT iHashedMac 
	INT iMatchHistoryId
	
	INT iNumTimesVehicleExchangeHands
ENDSTRUCT

//Server bitset
CONST_INT SERVER_BS_VEH_DESTROYED		0
CONST_INT SERVER_BS_SUDDEN_DEATH		1
CONST_INT SERVER_BS_VEHICLE_COLLECTED	2
CONST_INT SERVER_BS_LOW_PARTICIPANTS	3
CONST_INT SERVER_BS_VEHICLE_STUCK		4

ServerBroadcastData serverBD

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	
	INT iClientGameState = GAME_STATE_INIT
	
	INT iClientBitSet
	INT iClientKillCounter = 0		//Fix for url:bugstar:2414367 - Keeps count of each players kills
	
	AMPP_RUN_STAGE eAMPP_Stage = AMPP_START
	
ENDSTRUCT

//Client bitset
CONST_INT CLIENT_BS_REWARD_GIVEN			0
CONST_INT CLIENT_BS_ENDING_SHOWN			1
CONST_INT CLIENT_BS_PARTICIPATED_PERMANENT	2
CONST_INT CLIENT_BS_PARTICIPATED_TEMPORARY	3
CONST_INT CLIENT_BS_PARTICIPATED_EVER		4
CONST_INT CLIENT_BS_VEHICLE_STUCK			5

PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

CHALLENGE_DPAD_STRUCT chDPStruct

scrFmEventAmbientMission telemetryStruct
INT iPeakParticipants, iLeftInProgress, iPlayerCount, iQualifiedCount, iPeakQualified

INT iStartTime

//----------------------
//	DBG FUNCTIONS
//----------------------

#IF IS_DEBUG_BUILD

BOOL bEndScript
BOOL bEndScriptLocal
BOOL bExtraPrints
BOOL bSkipTime

PROC CREATE_WIDGETS()

	START_WIDGET_GROUP("Pass the Parcel")
		START_WIDGET_GROUP("Server Only")
			ADD_WIDGET_BOOL("Skip Time",bSkipTime)
			ADD_WIDGET_BOOL("End Script",bEndScript)
		STOP_WIDGET_GROUP()
		ADD_WIDGET_BOOL("End Script Locally",bEndScriptLocal)
		ADD_WIDGET_BOOL("Extra prints",bExtraPrints)
	STOP_WIDGET_GROUP()
	
	CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL DEBUG ===  CREATED WIDGETS ")
	
ENDPROC

#ENDIF

//----------------------
//	FUNCTIONS
//----------------------

/// PURPOSE:
///		1000 - Cash reward default.
FUNC INT GET_EOM_DEFAULT_CASH_REWARD()
	RETURN g_sMPTunables.iPASS_THE_PARCEL_EOM_DEFAULT_CASH_REWARD
ENDFUNC

/// PURPOSE:
///		100 - RP reward default.
FUNC INT GET_EOM_DEFAULT_RP_REWARD()
	RETURN g_sMPTunables.iPASS_THE_PARCEL_EOM_DEFAULT_RP_REWARD
ENDFUNC

/// PURPOSE:
/// 	10,000 - Cash reward base.
FUNC INT GET_CASH_REWARD_BASE()
	RETURN g_sMPTunables.iPASS_THE_PARCEL_CASH_REWARD_BASE
ENDFUNC

/// PURPOSE:
/// 	1,000 - RP reward base.
FUNC INT GET_RP_REWARD_BASE()
	RETURN g_sMPTunables.iPASS_THE_PARCEL_RP_REWARD_BASE
ENDFUNC

/// PURPOSE:
/// 	50 - RP reward for killing a package/vehicle carrier.
FUNC INT GET_RP_REWARD_KILL_CARRIER()
	RETURN g_sMPTunables.iPASS_THE_PARCEL_RP_REWARD_KILL_CARRIER // TODO: Replace with tunable.
ENDFUNC

/// PURPOSE:
/// 	20 - Cap for number of rewards for killing the package/vehicle carrier.
FUNC INT GET_RP_REWARD_KILL_CARRIER_CAP()
	RETURN g_sMPTunables.iPASS_THE_PARCEL_RP_REWARD_KILL_CARRIER_CAP // TODO: Replace with tunable.
ENDFUNC

/// PURPOSE:
/// 	100 - Distance from the vehicle that the player has to have been less than at least once.
FUNC INT GET_REWARD_PARTICIPATION_RANGE()
	RETURN g_sMPTunables.iPASS_THE_PARCEL_REWARD_PARTICIPATION_RANGE // TODO: Replace with tunable.
ENDFUNC

/// PURPOSE:
///    1.0 - Modifier for vehicle health. 
FUNC FLOAT GET_VEHICLE_HEALTH_MODIFIER()
	RETURN g_sMPTunables.fPass_the_Parcel_Vehicle_Health_Modifier
ENDFUNC


/// PURPOSE: Helper function to get debug string for stage
///    
/// PARAMS: 
///    stage - The stage 
/// RETURNS: The name of the stage
///    
FUNC STRING GET_AMPP_STAGE_STRING(AMPP_RUN_STAGE stage)

	SWITCH(stage)
		CASE AMPP_START				RETURN "AMPP_START"
		CASE AMPP_GET				RETURN "AMPP_GET"
		CASE AMPP_HOLD				RETURN "AMPP_HOLD"
		CASE AMPP_REWARDS			RETURN "AMPP_REWARDS"
		CASE AMPP_EXPLODE			RETURN "AMPP_EXPLODE"
		CASE AMPP_END 				RETURN "AMPP_END"
	ENDSWITCH

	RETURN "UNKNOWN STAGE!"
	
ENDFUNC

/// PURPOSE:  Get the time limit for the current challenge
///    
/// RETURNS: The time limit
///    
FUNC INT GET_CHALLENGE_TIME_LIMIT()

	IF IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_SUDDEN_DEATH)
		RETURN g_sMPTunables.iPass_the_Parcel_Sudden_Death_Time_Limit
	ELSE
		RETURN g_sMPTunables.iPass_the_Parcel_Event_Time_Limit
	ENDIF

ENDFUNC

FUNC BOOL IS_PARTICIPANT_ACTIVE_IN_EVENT(INT iPart)
	RETURN IS_BIT_SET(playerBD[iPart].iClientBitSet,CLIENT_BS_PARTICIPATED_PERMANENT)
		OR IS_BIT_SET(playerBD[iPart].iClientBitSet,CLIENT_BS_PARTICIPATED_TEMPORARY)
ENDFUNC


//----------------------
//	Client Functions
//----------------------

/// PURPOSE:
///    Returns TRUE if we are considered in the parcel vehicle
FUNC BOOL AM_I_IN_THE_PARCEL_VEHICLE(BOOL bDoVehicleChecks = FALSE, BOOL bCheckDriverSeat = FALSE)
	
	IF serverBD.piDriver = PLAYER_ID()
		RETURN TRUE
	ENDIF	
	
	IF bDoVehicleChecks
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
			IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh), TRUE)
				IF bCheckDriverSeat
					IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID(), TRUE) = VS_DRIVER
						RETURN TRUE
					ENDIF
				ELSE
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE: Gives the local player the reward if they are the winner
///    
PROC GIVE_REWARDS()
	
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_REWARD_GIVEN)
	
		//Do not reward player if passive or hiding the event
		IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(PLAYER_ID())
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_REWARD_GIVEN)
			EXIT
		ENDIF
		IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID())
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_REWARD_GIVEN)
			EXIT
		ENDIF
		
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_PARTICIPATED_PERMANENT) 
		OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_PARTICIPATED_EVER)
		
			IF NOT IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_VEH_DESTROYED)
						
				INT iCashGiven, iRPGiven, iParticipationCash
				INT iParticipationTime = serverBD.iEndTime - iStartTime
				PRINTLN("=== AM_PASS_THE_PARCEL === iParticipationTime = ", iParticipationTime)
				
				
				// Factoring in teh tunables. Get teh smaller value
				If IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_SUDDEN_DEATH)
					iParticipationTime = IMIN (	iParticipationTime,(g_sMPTunables.iPass_the_Parcel_Sudden_Death_Time_Limit+ g_sMPTunables.iPass_the_Parcel_Event_Time_Limit)) //adding both tunables as thats the max time it can be for Sudden death
					PRINTLN("=== AM_PASS_THE_PARCEL === iParticipationTime is sudden death",iParticipationTime )
			
				ELSE
					iParticipationTime = IMIN (	iParticipationTime, g_sMPTunables.iPass_the_Parcel_Event_Time_Limit)
					PRINTLN("=== AM_PASS_THE_PARCEL === iParticipationTime pass parcel regular",iParticipationTime)
				ENDIF
				// flooring the ipaticipation time 
				iParticipationTime = FLOOR((TO_FLOAT(iParticipationTime) / 60) / 1000)
				PRINTLN("=== AM_PASS_THE_PARCEL === Floored iParticipationTime = ", iParticipationTime)
				
				IF iParticipationTime > g_sMPTunables.iParticipation_T_Cap
					iParticipationTime = g_sMPTunables.iParticipation_T_Cap
				ELIF iParticipationTime < 1
					iParticipationTime = 1
				ENDIF
				
				iParticipationCash = GET_EOM_DEFAULT_CASH_REWARD() * iParticipationTime
				CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === GIVE_REWARDS - Participation Cash: ", iParticipationCash)
				
				// Check we are in the vehicle
				IF AM_I_IN_THE_PARCEL_VEHICLE()
				AND NOT IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_LOW_PARTICIPANTS)
						
					// Default to the $10000 (will be a tunable)
					iCashGiven = ROUND(GET_CASH_REWARD_BASE() * g_sMPTunables.fHold_the_Wheel_Event_Multiplier_Cash)
					// Default to 1000 (will be a tunable)
					iRPGiven = ROUND(GET_RP_REWARD_BASE() * g_sMPTunables.fHold_the_Wheel_Event_Multiplier_RP) + (GET_EOM_DEFAULT_RP_REWARD() * iParticipationTime)
					
										
					PRINTLN("=== AM_PASS_THE_PARCEL === GIVE_REWARDS: $", iCashGiven)
					PRINTLN("=== AM_PASS_THE_PARCEL === GIVE_REWARDS: ", iRPGiven, " RP")
				ELSE
					FM_EVENT_SETUP_DO_MINIMUM_PARTICIPATION_HELP(TRUE)
					iRPGiven = GET_EOM_DEFAULT_RP_REWARD() * iParticipationTime
					PRINTLN("=== AM_PASS_THE_PARCEL === GIVE_REWARDS: $",iParticipationCash,", RP: ",iRPGiven," as DEFAULT reward for all participants NOT in the vehicle.")
				ENDIF
					
				GB_HANDLE_GANG_BOSS_CUT(iCashGiven)
				
				iCashGiven += iParticipationCash
				CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === GIVE_REWARDS - Cash after Boss Cut with Participation Added: ", iCashGiven)
								
				IF iCashGiven > 0
					
					IF USE_SERVER_TRANSACTIONS()
						INT iScriptTransactionIndex
						TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_AMBIENT_JOB_PASS_PARCEL, iCashGiven, iScriptTransactionIndex, FALSE, TRUE) //Add new service
					ELSE
						AMBIENT_JOB_DATA amData
						NETWORK_EARN_FROM_AMBIENT_JOB(iCashGiven,"AM_PASS_THE_PARCEL",amData)
					ENDIF

					CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === GIVE_REWARDS - Awarding $",iCashGiven)
				ENDIF
				
				IF iRPGiven > 0
					NEXT_RP_ADDITION_SHOW_RANKBAR()
					GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD,PLAYER_PED_ID(),"",XPTYPE_COMPLETE,XPCATEGORY_COMPLETE_CHALLENGES,iRPGiven)
					CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === GIVE_REWARDS - Awarding ",iRPGiven," RP.")
				ENDIF
			
				g_i_cashForEndEventShard = iCashGiven		
				telemetryStruct.m_cashEarned 	+= iCashGiven
				IF NOT g_sMPTunables.bPass_the_Parcel_Disable_Share_Cash
					IF telemetryStruct.m_cashEarned > 0							
						SET_LAST_JOB_DATA(LAST_JOB_PASS_PARCEL, telemetryStruct.m_cashEarned)
					ENDIF
				ENDIF
				telemetryStruct.m_rpEarned		+= iRPGiven
			ENDIF
			
		ELSE
			FM_EVENT_SETUP_DO_MINIMUM_PARTICIPATION_HELP(FALSE)
			PRINTLN("=== AM_PASS_THE_PARCEL === GIVE_REWARDS: Bypassing reward payments. Bitset: ", playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet)
		ENDIF
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_REWARD_GIVEN)
	ENDIF

ENDPROC

PROC RESET_HELP_OR_MESSAGE(INT iHelp)

	IF IS_BIT_SET(iHelpBS, iHelp)
		CLEAR_BIT(iHelpBS, iHelp)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === RESET_HELP_OR_MESSAGE - 0")
	ENDIF

ENDPROC

FUNC STRING GET_STRING_FOR_PASS_THE_PARCEL_VEHICLE()

	SWITCH(serverBD.VehModel)
	
		CASE CADDY 
		CASE TRACTOR2 
			RETURN "ABLIP_CAR"
			
		CASE BMX 
		CASE TRIBIKE 
		CASE SANCHEZ 
		CASE FAGGIO2 
		CASE LECTRO 
			RETURN "ABLIP_BIKE"
	
	ENDSWITCH

	RETURN "ABLIP_BIKE"

ENDFUNC

FUNC BOOL IS_TALL_VEHICLE()

	SWITCH(serverBD.VehModel)
	
		CASE CADDY	RETURN TRUE

	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

PROC DO_HELP_TEXT_AND_MESSAGES(INT iHelp)
	
	IF NOT IS_RADAR_HIDDEN()
		SWITCH iHelp
			CASE 0 
				IF NOT IS_BIT_SET(iHelpBS, iHelp)
					IF NOT IS_PAUSE_MENU_ACTIVE()
					AND IS_NET_PLAYER_OK(PLAYER_ID())
						STRING sHold
						IF NOT IS_THIS_MODEL_A_BIKE(serverBD.VehModel)
							sHold = "AMPP_BIG_HOLD"
						ELSE
							sHold = "AMPP_BIG_HOLD2"
						ENDIF
						
						HUD_COLOURS hcShardAnimate, hcShardBG
						
						
						// Ensure we only show the purple shard the first time this is triggered
						IF NOT IS_BIT_SET(iLocalBitSet, LOCAL_BS_INITIAL_SHARD_SHOWN)
							hcShardAnimate = GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE()
							hcShardBG = GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE()
						ELSE
							hcShardAnimate = HUD_COLOUR_WHITE
							hcShardBG = HUD_COLOUR_BLACK
						ENDIF
						
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_VEHICLE_GAINED,"AMPP_COLLECT",sHold, hcShardAnimate, DEFAULT, hcShardBG)	//You have the Parcel Vehicle. Hold on to it until the time runs out.
						
						SET_BIT(iLocalBitSet, LOCAL_BS_INITIAL_SHARD_SHOWN)
						
						SET_BIT(iHelpBS, iHelp)
						CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === DO_HELP_TEXT_AND_MESSAGES - 0")
					ENDIF
				ENDIF
			BREAK
			CASE 1
				IF NOT IS_BIT_SET(iHelpBS, iHelp)
					IF NOT IS_PAUSE_MENU_ACTIVE()
						IF IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_SUDDEN_DEATH)
							SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_WIN,"AMCH_WIN","AMPP_WIN_SD", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME) //Winner Sudden Death
						ELSE
							SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_WIN,"AMCH_WIN","AMPP_WIN", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME) //Winner
						ENDIF
						SET_BIT(iHelpBS, iHelp)
						CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === DO_HELP_TEXT_AND_MESSAGES - 1")
					ENDIF
				ENDIF
			BREAK
			CASE 2
				IF NOT IS_BIT_SET(iHelpBS, iHelp)
					IF NOT IS_PAUSE_MENU_ACTIVE()
					AND serverBD.piDriver != INVALID_PLAYER_INDEX()
						IF IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_SUDDEN_DEATH)
							//SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_FM_EVENT_FINISHED,serverBD.piDriver,DEFAULT,"AMPP_LOSE_SD","AMPP_LOSER", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME) //Loser Sudden Death
							SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_FM_EVENT_FINISHED, "AMPP_LOSE_SD", GET_PLAYER_NAME(serverBD.piDriver), DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME, "AMPP_LOSER")
						ELSE
							STRING sMessage
							sMessage = "AMPP_LOSE"
							STRING sPlayer
							sPlayer = GET_PLAYER_NAME(serverBD.piDriver)
							IF GB_IS_PLAYER_MEMBER_OF_A_GANG(serverBD.piDriver)
								sPlayer = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(serverBD.piDriver)
							ENDIF
							SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_FM_EVENT_FINISHED, sMessage, sPlayer, DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME, "AMPP_LOSER")
						ENDIF
						SET_BIT(iHelpBS, iHelp)
						CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === DO_HELP_TEXT_AND_MESSAGES - 2")
					ENDIF
				ENDIF
			BREAK
			CASE 3
				IF NOT IS_BIT_SET(iHelpBS, iHelp)
					IF NOT IS_PAUSE_MENU_ACTIVE()
					AND NOT IS_CUSTOM_MENU_ON_SCREEN()
						PRINT_HELP_NO_SOUND("AMPP_UNVLB",DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) //Unavailable
						SET_FREEMODE_EVENT_HELP_BACKGROUND()
						SET_BIT(iHelpBS, iHelp)
						CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === DO_HELP_TEXT_AND_MESSAGES - 3")
					ENDIF
				ENDIF
			BREAK
			CASE 4
				IF NOT IS_BIT_SET(iHelpBS, iHelp)
					IF NOT IS_PAUSE_MENU_ACTIVE()
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_FINISHED,"AMPP_LOSER","AMPP_DESTR", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME) //Loser
						SET_BIT(iHelpBS, iHelp)
						CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === DO_HELP_TEXT_AND_MESSAGES - 4")
					ENDIF
				ENDIF
			BREAK
			CASE 5
				IF NOT IS_BIT_SET(iHelpBS, iHelp)
					IF NOT IS_PAUSE_MENU_ACTIVE()
						STRING sDeath
						IF NOT IS_THIS_MODEL_A_BIKE(serverBD.VehModel)
							sDeath = "AMPP_SDGET"
						ELSE
							sDeath = "AMPP_SDGET2"
						ENDIF
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_HOLD_THE_WHEEL_SD,"AMPP_SDEATH",sDeath)	//The first person to get in the Parcel vehicle wins.
						SET_BIT(iHelpBS, iHelp)
						CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === DO_HELP_TEXT_AND_MESSAGES - 5")
					ENDIF
				ENDIF
			BREAK
			CASE 6
				IF NOT IS_BIT_SET(iHelpBS, iHelp)
					IF NOT IS_PAUSE_MENU_ACTIVE()
					AND NOT IS_CUSTOM_MENU_ON_SCREEN()
						IF NOT IS_THIS_MODEL_A_BIKE(serverBD.VehModel)
							PRINT_HELP_NO_SOUND("AMPP_NEAR",DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) //Enter the vehicle to start Pass the Parcel.
						ELSE
							PRINT_HELP_NO_SOUND("AMPP_NEAR2",DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) //Get on the vehicle to start Pass the Parcel.
						ENDIF
						SET_FREEMODE_EVENT_HELP_BACKGROUND()
						SET_BIT(iHelpBS, iHelp)
						CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === DO_HELP_TEXT_AND_MESSAGES - 6")
					ENDIF
				ENDIF
			BREAK
			CASE 7
				IF NOT IS_BIT_SET(iHelpBS, iHelp)
					IF NOT IS_PAUSE_MENU_ACTIVE()
					AND NOT IS_CUSTOM_MENU_ON_SCREEN()
					AND serverBD.piDriver != INVALID_PLAYER_INDEX()
						PRINT_TICKER_WITH_PLAYER_NAME_WITH_HUD_COLOUR("AMPP_OWON",serverBD.piDriver,HUD_COLOUR_WHITE)
						SET_BIT(iHelpBS, iHelp)
						CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === DO_HELP_TEXT_AND_MESSAGES - 7")
					ENDIF
				ENDIF
			BREAK
			CASE 8
				IF NOT IS_BIT_SET(iHelpBS, iHelp)
					IF NOT IS_PAUSE_MENU_ACTIVE()
						IF serverBD.piDriver != PLAYER_ID()
							STRING sMessage
							sMessage = "AMPP_GET"
							IF GB_ARE_PLAYERS_IN_SAME_GANG(PLAYER_ID(),serverBD.piDriver)
								sMessage = "AMPP_GBHELP"
							ENDIF
							SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_FM_EVENT_START, sMessage, GET_PLAYER_NAME(serverBD.piDriver), GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE(), DEFAULT, "AMPP_TITLE", GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE()) //~a~ has the Parcel Vehicle. Get it and hold on to it until the time runs out.
						ENDIF
						SET_BIT(iHelpBS, iHelp)
						CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === DO_HELP_TEXT_AND_MESSAGES - 8")
					ENDIF
				ENDIF
			BREAK
			CASE 9
				IF NOT IS_BIT_SET(iHelpBS, iHelp)
					IF NOT IS_PAUSE_MENU_ACTIVE()
					AND NOT IS_CUSTOM_MENU_ON_SCREEN()
						PRINT_TICKER("AMPP_DESTP")
						SET_BIT(iHelpBS, iHelp)
						CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === DO_HELP_TEXT_AND_MESSAGES - 9")
					ENDIF
				ENDIF
			BREAK
			CASE 10
				IF NOT IS_BIT_SET(iHelpBS, iHelp)
					IF NOT IS_PAUSE_MENU_ACTIVE()
					AND NOT IS_CUSTOM_MENU_ON_SCREEN()
					AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
					AND GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(NET_TO_VEH(serverBD.niVeh)) > 0
						PRINT_HELP_NO_SOUND("AMPP_SHARE",DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
						SET_FREEMODE_EVENT_HELP_BACKGROUND()
						SET_BIT(iHelpBS, iHelp)
						CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === DO_HELP_TEXT_AND_MESSAGES - 10")
					ENDIF
				ENDIF
			BREAK
			CASE 11
				IF NOT IS_BIT_SET(iHelpBS, iHelp)
					IF NOT IS_PAUSE_MENU_ACTIVE()
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_FINISHED,"CPC_OVER","AMPP_NOWIN", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME) //No Winner
						SET_BIT(iHelpBS, iHelp)
						CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === DO_HELP_TEXT_AND_MESSAGES - 11")
					ENDIF
				ENDIF
			BREAK
			CASE 12
				IF NOT IS_BIT_SET(iHelpBS, iHelp)
					IF NOT IS_PAUSE_MENU_ACTIVE()
					AND NOT IS_CUSTOM_MENU_ON_SCREEN()
						PRINT_TICKER("AMPP_NOWIN2")
						SET_BIT(iHelpBS, iHelp)
						CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === DO_HELP_TEXT_AND_MESSAGES - 12")
					ENDIF
				ENDIF
			BREAK
			CASE 13
				IF NOT IS_BIT_SET(iHelpBS, iHelp)
					IF NOT IS_PAUSE_MENU_ACTIVE()
					AND NOT IS_CUSTOM_MENU_ON_SCREEN()
						PRINT_HELP_WITH_STRING_NO_SOUND("AMPP_START",GET_STRING_FOR_PASS_THE_PARCEL_VEHICLE(), DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
						SET_FREEMODE_EVENT_HELP_BACKGROUND(FALSE)
						telemetryStruct.m_notifiedTime = GET_CLOUD_TIME_AS_INT()
						PLAY_SOUND_FRONTEND(-1, "Event_Start_Text", "GTAO_FM_Events_Soundset", FALSE)
						SET_BIT(iHelpBS, iHelp)
						CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === DO_HELP_TEXT_AND_MESSAGES - 13")
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC



/// PURPOSE: Displays a big message if local player is the winner, otherwise displays a ticker
///    
PROC SHOW_END_MESSAGES()

	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_ENDING_SHOWN)
	AND (FM_EVENT_END_SHARD_SAFE_TO_DISPLAY()
		OR IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_LOW_PARTICIPANTS))

		IF IS_PARTICIPANT_ACTIVE_IN_EVENT(PARTICIPANT_ID_TO_INT())
			IF IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_LOW_PARTICIPANTS)
				telemetryStruct.m_endReason       	=AE_END_LOW_NUMBERS
				SET_UP_BAIL_FM_EVENT_DUE_TO_LOW_SESSION_NUMBERS_BIG_MESSAGE("","")
			ELIF IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_VEH_DESTROYED)
				telemetryStruct.m_endReason       	=AE_END_LOST
				DO_HELP_TEXT_AND_MESSAGES(4) //Loser, dead
			ELIF serverBD.piDriver = INVALID_PLAYER_INDEX()	//No winner
				telemetryStruct.m_endReason       	=AE_END_LOST
				DO_HELP_TEXT_AND_MESSAGES(11)
			ELSE
				IF AM_I_IN_THE_PARCEL_VEHICLE()
					telemetryStruct.m_endReason       	=AE_END_WON
					DO_HELP_TEXT_AND_MESSAGES(1) //Winner
				ELSE
					telemetryStruct.m_endReason       	=AE_END_LOST
					DO_HELP_TEXT_AND_MESSAGES(2) //Loser
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_LOW_PARTICIPANTS)
				SET_UP_BAIL_FM_EVENT_DUE_TO_LOW_SESSION_NUMBERS_BIG_MESSAGE("","")
			ELIF IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_VEH_DESTROYED)
				DO_HELP_TEXT_AND_MESSAGES(9)
			ELIF serverBD.piDriver = INVALID_PLAYER_INDEX()	//No winner
				DO_HELP_TEXT_AND_MESSAGES(12)
			ELSE
				DO_HELP_TEXT_AND_MESSAGES(7)
			ENDIF
		ENDIF
		
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_ENDING_SHOWN)
	ENDIF

ENDPROC

PROC TRIGGER_PTP_AUDIO()
	INT iCurrentTime = (GET_CHALLENGE_TIME_LIMIT()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBd.eventTimer))
	IF iCurrentTime > 30000
		IF NOT IS_BIT_SET(iLocalBitset,LOCAL_BS_MUSIC_TRIGGERED)
		
			//Music
			TRIGGER_MUSIC_EVENT("PTP_START")
			SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
			
			//Gained vehicle sound (now handled in shard see 2447989)
			//PLAY_SOUND_FRONTEND(-1,"Parcel_Vehicle_Gained","GTAO_FM_Events_Soundset", FALSE)
			
			SET_BIT(iLocalBitset,LOCAL_BS_MUSIC_TRIGGERED)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === TRIGGER_PTP_AUDIO")
		ENDIF
	ENDIF
ENDPROC

PROC KILL_PTP_AUDIO(BOOL bKillOnly=FALSE)
	INT iCurrentTime = (GET_CHALLENGE_TIME_LIMIT()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBd.eventTimer))
	IF iCurrentTime > 30000
		IF IS_BIT_SET(iLocalBitset,LOCAL_BS_MUSIC_TRIGGERED)
			TRIGGER_MUSIC_EVENT("PTP_STOP")
			SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE)
			
			IF NOT bKillOnly
				//Gained vehicle sound
				PLAY_SOUND_FRONTEND(-1,"Parcel_Vehicle_Lost","GTAO_FM_Events_Soundset", FALSE)
				CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === KILL_PTP_AUDIO - Parcel_Vehicle_Lost")
			ENDIF
			
			CLEAR_BIT(iLocalBitset,LOCAL_BS_MUSIC_TRIGGERED)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === KILL_PTP_AUDIO")
		ENDIF
	ENDIF
ENDPROC

PROC TRIGGER_PTP_SUDDEN_DEATH_AUDIO(INT iCurrentTime)
	
	IF NOT IS_BIT_SET(iLocalBitset,LOCAL_BS_SD_MUSIC_TRIGGERED)
		IF iCurrentTime <= GET_CHALLENGE_TIME_LIMIT() - 3000	//Delay so no conflicts with end of countdown music
			TRIGGER_MUSIC_EVENT("FM_SUDDEN_DEATH_START_MUSIC")
			SET_AUDIO_FLAG("DisableFlightMusic", TRUE)
			SET_AUDIO_FLAG("WantedMusicDisabled", TRUE)
			SET_BIT(iLocalBitset,LOCAL_BS_SD_MUSIC_TRIGGERED)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === TRIGGER_PTP_SUDDEN_DEATH_AUDIO")
		ENDIF
	ENDIF
	
ENDPROC

PROC KILL_PTP_SUDDEN_DEATH_AUDIO()

	IF IS_BIT_SET(iLocalBitset,LOCAL_BS_SD_MUSIC_TRIGGERED)
		TRIGGER_MUSIC_EVENT("FM_SUDDEN_DEATH_STOP_MUSIC")
		SET_AUDIO_FLAG("DisableFlightMusic", FALSE)
		SET_AUDIO_FLAG("WantedMusicDisabled", FALSE)
		CLEAR_BIT(iLocalBitset,LOCAL_BS_SD_MUSIC_TRIGGERED)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === KILL_PTP_SUDDEN_DEATH_AUDIO")
	ENDIF
	
ENDPROC

PROC DRAW_EVENT_TIMER()

	IF HAS_NET_TIMER_STARTED(serverBD.eventTimer)
		HUD_COLOURS timeColour
		INT iCurrentTime = (GET_CHALLENGE_TIME_LIMIT()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBd.eventTimer))
		
		IF NOT IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_SUDDEN_DEATH)
			HANDLE_AMBIENT_EVENT_COUNTDOWN_AUDIO(iCurrentTime)
		ELSE
			TRIGGER_PTP_SUDDEN_DEATH_AUDIO(iCurrentTime)
		ENDIF
		
		IF iCurrentTime > 30000
			timeColour = HUD_COLOUR_WHITE
		ELSE
			timeColour = HUD_COLOUR_RED
		ENDIF
		iCurrentTime = IMAX(iCurrentTime,0)
		
		BOTTOM_RIGHT_UI_EVENT_TIMER(iCurrentTime, timeColour, GET_COUNTDOWN_TIMER_NAME_FOR_EVENT())
		
	ENDIF

ENDPROC

FUNC HUD_COLOURS GET_PARCEL_PLAYER_COLOUR(BOOL bForBlip = FALSE)

	IF serverBD.piDriver != INVALID_PLAYER_INDEX()
		IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
		AND GB_GET_THIS_PLAYER_GANG_BOSS(serverBD.piDriver) = GB_GET_LOCAL_PLAYER_GANG_BOSS()
			RETURN GET_PLAYER_HUD_COLOUR(serverBD.piDriver)  //B* 2556753
		ENDIF
	ENDIF
	
	IF bForBlip
		RETURN HUD_COLOUR_NET_PLAYER2
	ENDIF
	
	RETURN HUD_COLOUR_RED

ENDFUNC

PROC DRAW_VEHICLE_MARKER()
	//Only if someone has got in the car
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
		INT colR, colG, colB, colA
		IF IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_VEHICLE_COLLECTED)
			IF IS_NET_PLAYER_OK(PLAYER_ID())
			AND IS_PARTICIPANT_ACTIVE_IN_EVENT(PARTICIPANT_ID_TO_INT())
				IF NOT AM_I_IN_THE_PARCEL_VEHICLE(TRUE)
					IF serverBD.piDriver != INVALID_PLAYER_INDEX()				
						GET_HUD_COLOUR(GET_PARCEL_PLAYER_COLOUR(), colR, colG, colB, colA)
					ELSE
						GET_HUD_COLOUR(HUD_COLOUR_GREEN, colR, colG, colB, colA)
					ENDIF					
					FM_EVENTDRAW_MARKER_ABOVE_VEHICLE(NET_TO_VEH(serverBD.niVeh), colR, colG, colB)
				ENDIF
			ENDIF
		ELSE
			GET_HUD_COLOUR(HUD_COLOUR_GREEN, colR, colG, colB, colA)
			FM_EVENTDRAW_MARKER_ABOVE_VEHICLE(NET_TO_VEH(serverBD.niVeh), colR, colG, colB)
		ENDIF
	ENDIF
ENDPROC

FUNC BLIP_SPRITE GET_SPRITE_FOR_PASS_THE_PARCEL_VEHICLE()

	SWITCH(serverBD.VehModel)
	
		CASE CADDY 
		CASE TRACTOR2 
			RETURN RADAR_TRACE_GANG_VEHICLE
			
		CASE BMX 
		CASE TRIBIKE 
		CASE SANCHEZ 
		CASE FAGGIO2 
		CASE LECTRO 
			RETURN RADAR_TRACE_GANG_BIKE
	
	ENDSWITCH

	RETURN RADAR_TRACE_GANG_BIKE

ENDFUNC

PROC HANDLE_VEHICLE_BLIP()

	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
	AND NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh))
	AND (NOT IS_NET_PLAYER_OK(PLAYER_ID())
	OR NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),NET_TO_VEH(serverBD.niVeh)))
		IF NOT DOES_BLIP_EXIST(VehBlip)
	
			VehBlip = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(serverBD.niVeh))
			
			SET_BLIP_SPRITE(VehBlip, GET_SPRITE_FOR_PASS_THE_PARCEL_VEHICLE())
			SET_BLIP_COLOUR_FROM_HUD_COLOUR(VehBlip, GET_PARCEL_PLAYER_COLOUR(TRUE))
			
			//Only show first time
			IF serverBD.eAMPP_Stage <= AMPP_GET
				SET_BLIP_FLASHES(VehBlip,TRUE)
				SET_BLIP_FLASH_TIMER(VehBlip, 7000)
			ENDIF
			
			KILL_PTP_AUDIO()
			SET_BLIP_PRIORITY(VehBlip, BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
			SET_BLIP_NAME_FROM_TEXT_FILE(VehBlip, "AMPP_BLIP")	//Hot Target
			CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === HANDLE_VEHICLE_BLIP - VEH BLIP ADDED")
		ENDIF
		IF DOES_BLIP_EXIST(VehBlip)
			IF serverBD.eAMPP_Stage >= AMPP_HOLD
				IF serverBD.piDriver = INVALID_PLAYER_INDEX()
					SET_BLIP_COLOUR_FROM_HUD_COLOUR(VehBlip, HUD_COLOUR_GREEN)
				ELSE
					SET_BLIP_COLOUR_FROM_HUD_COLOUR(VehBlip, GET_PARCEL_PLAYER_COLOUR(FALSE))
					//SET_BLIP_FLASHES_BETWEEN_COLOURS(VehBlip, HUD_COLOUR_RED, HUD_COLOUR_GREEN,blipFlashTimer)
				ENDIF
			ELSE
				SET_BLIP_COLOUR_FROM_HUD_COLOUR(VehBlip, HUD_COLOUR_NET_PLAYER2)
			ENDIF
		ENDIF
	ELSE	
		IF DOES_BLIP_EXIST(VehBlip)
			REMOVE_BLIP(VehBlip)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === HANDLE_VEHICLE_BLIP - VEH BLIP REMOVED")
		ENDIF
	ENDIF
	


ENDPROC

//PURPOSE: Controls the explode vehicle stage
PROC MAINTAIN_EXPLODE_VEH()
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niVeh)
		IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh))
			IF (VEHICLE_EXPLOSION_TIME-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.ExplodeVehTimer)) >= 0
				IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh), <<VEH_EXP_RANGE, VEH_EXP_RANGE, VEH_EXP_RANGE>>)
					DRAW_GENERIC_TIMER((g_sMPTunables.iVehicleExplosionTime-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.ExplodeVehTimer)), "HTV_DESTR", 0, TIMER_STYLE_USEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_DONTCARE, DEFAULT, HUD_COLOUR_RED,DEFAULT,DEFAULT,DEFAULT,HUD_COLOUR_RED)
					SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
				ENDIF
			ELSE
				IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh), <<VEH_EXP_RANGE, VEH_EXP_RANGE, VEH_EXP_RANGE>>)
					DRAW_GENERIC_TIMER(0, "HTV_DESTR", 0, TIMER_STYLE_USEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_DONTCARE, DEFAULT, HUD_COLOUR_RED,DEFAULT,DEFAULT,DEFAULT,HUD_COLOUR_RED)
					SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
				ENDIF
				
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niVeh)
					NETWORK_EXPLODE_VEHICLE(NET_TO_VEH(serverBD.niVeh))
					CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === MAINTAIN_EXPLODE_VEH - VEHICLE EXPLODED")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_BLIP_EXIST(VehBlip)
		REMOVE_BLIP(VehBlip)
	ENDIF
ENDPROC

PROC SET_AS_PERMANENT_PARTICIPANT()

	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_PARTICIPATED_PERMANENT)
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_PARTICIPATED_PERMANENT)
		IF iStartTime = 0
			iStartTime = NATIVE_TO_INT(GET_NETWORK_TIME())
		ENDIF
		IF NOT FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT(PLAYER_ID())
			SET_PLAYERS_CURRENT_FREEMODE_AMBIENT_EVENT_TYPE(FMMC_TYPE_HOLD_THE_WHEEL)
		ENDIF
		FM_EVENT_SET_PLAYER_IS_PERMANENT_PARTICIPANT_IN_FM_EVENT(TRUE)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === SET_AS_PERMANENT_PARTICIPANT")
	ENDIF
	
ENDPROC

FUNC BOOL HAS_PARTICIPANT_PARTICIPATED_IN_EVENT(INT iPart)

	IF iPart >= 0 AND iPart < 32
		RETURN IS_BIT_SET(playerBD[iPart].iClientBitSet,CLIENT_BS_PARTICIPATED_PERMANENT)
			OR IS_BIT_SET(playerBD[iPart].iClientBitSet,CLIENT_BS_PARTICIPATED_EVER)	
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC HANDLE_PARTICIPATION_FROM_RANGE()
	IF SERVERBD.eAMPP_Stage >= AMPP_HOLD
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
				FLOAT fDistance = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),NET_TO_ENT(serverBD.niVeh))
				IF fDistance <= g_sMPTunables.iFREEMODE_EVENT_UI_ENTRY_DISTANCE
					IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_PARTICIPATED_TEMPORARY)
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_PARTICIPATED_TEMPORARY)
						CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === SET CLIENT_BS_PARTICIPATED_TEMPORARY due to range.")
					ENDIF
				ELIF fDistance > g_sMPTunables.iFREEMODE_EVENT_UI_EXIT_DISTANCE
					IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_PARTICIPATED_TEMPORARY)
						CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_PARTICIPATED_TEMPORARY)
						CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === CLEAR CLIENT_BS_PARTICIPATED_TEMPORARY due to range.")
					ENDIF
				ENDIF
				
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),NET_TO_ENT(serverBD.niVeh)) <= GET_REWARD_PARTICIPATION_RANGE()
					IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_PARTICIPATED_EVER)
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_PARTICIPATED_EVER)
						IF iStartTime = 0
							iStartTime = NATIVE_TO_INT(GET_NETWORK_TIME())
						ENDIF
						CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === SET CLIENT_BS_PARTICIPATED_EVER due to range.")
					ENDIF
				ELSE
					IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_PARTICIPATED_EVER)
						CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_PARTICIPATED_EVER)
						CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === CLEAR CLIENT_BS_PARTICIPATED_EVER due to range.")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_WANTED_AND_MENTAL()

	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
		VECTOR vLoc = GET_ENTITY_COORDS(NET_TO_ENT(serverBD.niVeh))
		MAINTAIN_FREEMODE_AMBIENT_EVENT_DISTANT_CHECKS(FMMC_TYPE_HOLD_THE_WHEEL,vLoc,bMultipliersActive, TO_FLOAT(g_sMPTunables.iFREEMODE_EVENT_UI_ENTRY_DISTANCE), DEFAULT, bCustomSpawn)
	ENDIF

ENDPROC

PROC HANDLE_VEHICLE_STUCK()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niVeh)
			IF IS_VEHICLE_STUCK_TIMER_UP(NET_TO_VEH(serverBD.niVeh), VEH_STUCK_JAMMED, g_sMPTunables.iFm_Events_Vehicle_stuck_Check)
			OR IS_VEHICLE_STUCK_TIMER_UP(NET_TO_VEH(serverBD.niVeh), VEH_STUCK_HUNG_UP, HUNG_UP_TIME)
			OR IS_VEHICLE_STUCK_TIMER_UP(NET_TO_VEH(serverBD.niVeh), VEH_STUCK_ON_SIDE, SIDE_TIME)	
			OR IS_VEHICLE_STUCK_TIMER_UP(NET_TO_VEH(serverBD.niVeh), VEH_STUCK_ON_ROOF, ROOF_TIME)
			
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_VEHICLE_STUCK)
				CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === HANDLE_VEHICLE_STUCK - Vehicle stuck")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_RESPAWNING()

	IF serverBD.eAMPP_Stage = AMPP_HOLD //Event has started
	AND IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_PARTICIPATED_TEMPORARY) //Player in range
	AND NOT FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(PLAYER_ID())
	AND NOT FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID())
	AND IS_IT_SAFE_TO_DISPLAY_FM_EVENT_UI()	
	AND NOT IS_PLAYER_IN_CORONA()
	AND NOT IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), FALSE)
	AND NOT IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
		SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_NEAR_SPECIFIC_COORDS)
		VECTOR vTargetCoords
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
			IF NOT IS_ENTITY_DEAD(NET_TO_ENT(serverBD.niVeh))
				vTargetCoords = GET_ENTITY_COORDS(NET_TO_ENT(serverBD.niVeh))
				FLOAT fRadius = 250.0
				FLOAT fMinDist = FMEVENT_MINIMUM_SPAWN_DISTANCE_FROM_ACTION
				IF serverBD.VehModel = CADDY
					fRadius = 100.0
					fMinDist = 50
				ENDIF
				
				SETUP_SPECIFIC_SPAWN_LOCATION(vTargetCoords, 0.0, fRadius, TRUE, fMinDist,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
				SET_PLAYER_WILL_SPAWN_FACING_COORDS(vTargetCoords, TRUE, FALSE)
			ENDIF
		ENDIF
		IF bCustomSpawn = FALSE
			bCustomSpawn = TRUE
		ENDIF
	ELSE
		CLEAR_PLAYER_NEXT_RESPAWN_LOCATION()		
		CLEAR_SPECIFIC_SPAWN_LOCATION_FROM_THIS_SCRIPT()
		IF bCustomSpawn
			bCustomSpawn = FALSE
		ENDIF
	ENDIF

ENDPROC

PROC HANDLE_OBJECTIVE_TEXT()

	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND IS_PARTICIPANT_ACTIVE_IN_EVENT(PARTICIPANT_ID_TO_INT())
	AND NOT FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(PLAYER_ID())
	AND NOT FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID())
	AND IS_IT_SAFE_TO_DISPLAY_FM_EVENT_UI() 
		IF playerBD[PARTICIPANT_ID_TO_INT()].eAMPP_Stage = AMPP_HOLD
			STRING sHold
			IF NOT IS_THIS_MODEL_A_BIKE(serverBD.VehModel)
				sHold = "AMPP_HOLD"
			ELSE
				sHold = "AMPP_HOLD2"
			ENDIF
			Print_Objective_Text(sHold)
		ELIF playerBD[PARTICIPANT_ID_TO_INT()].eAMPP_Stage = AMPP_GET
		AND serverBD.eAMPP_Stage = AMPP_HOLD
			IF serverBD.piDriver != INVALID_PLAYER_INDEX()
			AND IS_NET_PLAYER_OK(serverBD.piDriver)
				IF GB_ARE_PLAYERS_IN_SAME_GANG(PLAYER_ID(),serverBD.piDriver)
					Print_Objective_Text_With_Player_Name_And_String_Coloured("AMPP_GBOWNER",serverBD.piDriver,"AMPP_VEHICLE",DEFAULT,GET_PARCEL_PLAYER_COLOUR(FALSE))
				ELSE
					Print_Objective_Text_With_User_Created_String("AMPP_OWNER", GET_PLAYER_NAME(serverBD.piDriver))
				ENDIF
			ELSE
				IF NOT AM_I_IN_THE_PARCEL_VEHICLE(TRUE)
					STRING sGet
					IF IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_SUDDEN_DEATH)
						IF NOT IS_THIS_MODEL_A_BIKE(serverBD.VehModel)
							sGet = "AMPP_NOWNERSD"
						ELSE
							sGet = "AMPP_NOWNER2SD"
						ENDIF
					ELSE
						IF NOT IS_THIS_MODEL_A_BIKE(serverBD.VehModel)
							sGet = "AMPP_NOWNER"
						ELSE
							sGet = "AMPP_NOWNER2"
						ENDIF
					ENDIF
					Print_Objective_Text(sGet)
				ELSE
					Clear_Any_Objective_Text_From_This_Script()
				ENDIF
			ENDIF
		ELSE
			Clear_Any_Objective_Text_From_This_Script()
		ENDIF
	ELSE
		Clear_Any_Objective_Text_From_This_Script()
	ENDIF

ENDPROC

PROC HANDLE_EVENT_CRITICALITY()
	
	IF playerBD[PARTICIPANT_ID_TO_INT()].eAMPP_Stage = AMPP_HOLD
		IF NOT FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT(PLAYER_ID())
			FM_EVENT_SET_PLAYER_IS_CRITICAL_TO_FM_EVENT(TRUE)
		ENDIF
	ELSE
		IF FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT(PLAYER_ID())
			FM_EVENT_SET_PLAYER_IS_CRITICAL_TO_FM_EVENT(FALSE)
		ENDIF
	ENDIF

ENDPROC

PROC HANDLE_VEHICLE_IN_SHOP()

	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
		VECTOR vVehPos = GET_ENTITY_COORDS(NET_TO_ENT(serverBD.niVeh),FALSE)
		SHOP_NAME_ENUM closestShop = GET_CLOSEST_SHOP_OF_TYPE(vVehPos)

		VECTOR vShopCoords = GET_SHOP_COORDS(closestShop)

		INTERIOR_INSTANCE_INDEX interiorShop = GET_INTERIOR_AT_COORDS_WITH_TYPE(vShopCoords, GET_SHOP_INTERIOR_TYPE(closestShop))
		INTERIOR_INSTANCE_INDEX interiorVeh = GET_INTERIOR_FROM_ENTITY(NET_TO_ENT(serverBD.niVeh))
		IF interiorShop != NULL
		AND interiorShop = interiorVeh
			IF NOT MPGlobalsAmbience.bKeepShopDoorsOpen
				MPGlobalsAmbience.bKeepShopDoorsOpen = TRUE
				CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === MPGlobalsAmbience.bKeepShopDoorsOpenForParcelVehicle = TRUE")
			ENDIF
			EXIT
		ENDIF
	ENDIF
	
	IF MPGlobalsAmbience.bKeepShopDoorsOpen
		MPGlobalsAmbience.bKeepShopDoorsOpen = FALSE
		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === MPGlobalsAmbience.bKeepShopDoorsOpenForParcelVehicle = FALSE")
	ENDIF

ENDPROC

PROC HANDLE_PLAYER_ENTERING_VEHICLE()

	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
		IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.niVeh))
			IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID())
			OR FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(PLAYER_ID())
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(NET_TO_VEH(serverBD.niVeh),FALSE)
				IF AM_I_IN_THE_PARCEL_VEHICLE()
					TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh))
					PRINTLN("=== AM_PASS_THE_PARCEL === Player is restricted and in the vehicle, tasked with leaving.")
				ENDIF
			ELSE
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(NET_TO_VEH(serverBD.niVeh),TRUE)
			ENDIF
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL IS_PLAYER_NEAR_PTP_VEHICLE(BOOL bCountIfRestricted = FALSE)

	IF NOT bCountIfRestricted
	AND (FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(PLAYER_ID())
	OR FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID()))
		RETURN FALSE
	ENDIF

	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
			IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.niVeh))
				RETURN GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),NET_TO_ENT(serverBD.niVeh)) <= 10
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC


//----------------------
//	Server Functions
//----------------------

FUNC BOOL IS_PTP_VEHICLE_DISABLED(INT iVariant)

	SWITCH(iVariant)
	
		CASE 0	RETURN g_sMPTunables.bPass_the_Parcel_Disable_variant_1    
		CASE 1	RETURN g_sMPTunables.bPass_the_Parcel_Disable_variant_2
		CASE 2	RETURN g_sMPTunables.bPass_the_Parcel_Disable_variant_3
		CASE 3	RETURN (g_sMPTunables.bPass_the_Parcel_Disable_variant_4 AND g_sMPTunables.bPass_the_Parcel_Disable_variant_5)
		CASE 4	RETURN g_sMPTunables.bPass_the_Parcel_Disable_variant_6
		CASE 5	RETURN g_sMPTunables.bPass_the_Parcel_Disable_variant_7
		CASE 6	RETURN g_sMPTunables.bPass_the_Parcel_Disable_variant_8
	
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_PTP_LOCATION_DISABLED(INT iRoute)

	SWITCH(serverBD.VehModel)
		
		CASE SANCHEZ
			SWITCH(iRoute)
				CASE 0	RETURN g_sMPTunables.bPass_the_Parcel_Disable_variant_4
				CASE 1	RETURN g_sMPTunables.bPass_the_Parcel_Disable_variant_5
			ENDSWITCH
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

FUNC INT GET_TOTAL_VEHICLE_ROUTES(MODEL_NAMES model)

	SWITCH(model)
	
		CASE SANCHEZ	RETURN TOTAL_ROUTES_SANCHEZ
	
	ENDSWITCH

	RETURN 1

ENDFUNC

FUNC INT GET_TYPE_FROM_VEH()
	SWITCH serverBD.VehModel 
		CASE CADDY 		RETURN	0 
		CASE BMX 		RETURN	1 
		CASE TRIBIKE 	RETURN	2 
		CASE SANCHEZ 	RETURN	3 
		CASE FAGGIO2 	RETURN	4 
		CASE LECTRO 	RETURN	5 
		CASE TRACTOR2 	RETURN	6 
	ENDSWITCH
	RETURN 0
ENDFUNC
INT iAttempt
//PURPOSE: Sets the data for this instance of Destroy Veh
PROC GET_RANDOM_VEHICLE()
	
	INT iVeh 
	
	#IF IS_DEBUG_BUILD
	IF MPGlobalsAmbience.iPassTheParcelVeh >= 0
		iVeh = MPGlobalsAmbience.iPassTheParcelVeh
	ELSE
	#ENDIF
		iVeh = GET_RANDOM_INT_IN_RANGE(0, TOTAL_PTP_VEHICLES)
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF
	
	IF (IS_PTP_VEHICLE_DISABLED(iVeh)
	OR FM_EVENT_IS_VARIATION_IN_HISTORY_LIST(FMMC_TYPE_HOLD_THE_WHEEL, iVeh))
	#IF IS_DEBUG_BUILD
	AND NOT (MPGlobalsAmbience.iPassTheParcelVeh >= 0)
	#ENDIF
	//Because this function is recursive then have a break out. 
	AND iAttempt < 100
		//Ignore debug var to prevent instruction overflow
		#IF IS_DEBUG_BUILD
		MPGlobalsAmbience.iPassTheParcelVeh = -1
		MPGlobalsAmbience.iPassTheParcelRoute = -1
		#ENDIF
		iAttempt++
		GET_RANDOM_VEHICLE()
	ELSE
		iAttempt = 0
		SWITCH(iVeh)
		
			CASE 0 serverBD.VehModel = CADDY BREAK
			CASE 1 serverBD.VehModel = BMX BREAK
			CASE 2 serverBD.VehModel = TRIBIKE BREAK
			CASE 3 serverBD.VehModel = SANCHEZ BREAK
			CASE 4 serverBD.VehModel = FAGGIO2 BREAK
			CASE 5 serverBD.VehModel = LECTRO BREAK
			CASE 6 serverBD.VehModel = TRACTOR2 BREAK
		
		ENDSWITCH
		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === GET_RANDOM_VEHICLE - Model is ",GET_MODEL_NAME_STRING(serverBD.VehModel))
	ENDIF
	
ENDPROC

//PURPOSE: Sets the data for this instance of Destroy Veh
PROC GET_RANDOM_START_LOCATION()
	
	INT iRoute
	
	#IF IS_DEBUG_BUILD
	IF MPGlobalsAmbience.iPassTheParcelRoute >= 0
		iRoute = MPGlobalsAmbience.iPassTheParcelRoute
	ELSE
	#ENDIF
		iRoute = GET_RANDOM_INT_IN_RANGE(0, GET_TOTAL_VEHICLE_ROUTES(serverBD.VehModel))
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF
	

	SWITCH(serverBD.VehModel)
	
		CASE CADDY
			serverBD.vStartLocation = <<-1343.6919, 131.8600, 55.2396>>
			serverBD.fStartHeading = 274.9769
		BREAK
		
		CASE BMX
			serverBD.vStartLocation = <<271.5271, -207.4667, 60.5707>>
			serverBD.fStartHeading = 69.8638 
		BREAK
		
		CASE TRIBIKE
			serverBD.vStartLocation = <<-1858.1632, -1229.8375, 12.0171>> 
			serverBD.fStartHeading = 265.1099 
		BREAK
		
		CASE SANCHEZ
			SWITCH(iRoute)
				
				CASE 0
					serverBD.vStartLocation = <<2395.7415, 3307.7502, 46.5735>>
					serverBD.fStartHeading = 130.8036 
				BREAK
				
				CASE 1
					serverBD.vStartLocation = <<-1207.1279, -2790.5757, 12.9522>>
					serverBD.fStartHeading = 105.9907
					serverBD.bOpenAirportGates = TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FAGGIO2 
			serverBD.vStartLocation = <<-3033.5991, 550.3338, 6.5127>>
			serverBD.fStartHeading = 253.5783 
		BREAK
		
		CASE LECTRO
			serverBD.vStartLocation = <<-1819.1906, 785.0829, 136.8941>>
			serverBD.fStartHeading = 223.8026 
		BREAK
		
		CASE TRACTOR2
			serverBD.vStartLocation = <<2358.6633, 4891.2314, 41.0573>>
			serverBD.fStartHeading = 157.8569
		BREAK
			
	ENDSWITCH
	
	IF IS_PTP_LOCATION_DISABLED(iRoute)
	OR NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION(serverBD.vStartLocation)	
	#IF IS_DEBUG_BUILD
	AND NOT (MPGlobalsAmbience.iPassTheParcelVeh >= 0)
	#ENDIF

		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === GET_RANDOM_START_LOCATION - Location ",iRoute," ",serverBD.vStartLocation," is disabled or not suitable for spawning, grabbing another.")
	
		//Ignore debug var to prevent instruction overflow
		#IF IS_DEBUG_BUILD
		MPGlobalsAmbience.iPassTheParcelVeh = -1
		MPGlobalsAmbience.iPassTheParcelRoute = -1
		#ENDIF
		
		GET_RANDOM_VEHICLE()
		GET_RANDOM_START_LOCATION()
	ELSE
		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === GET_RANDOM_START_LOCATION - ",serverBD.vStartLocation," ",serverBD.fStartHeading)
	ENDIF
	
ENDPROC

//PURPOSE: Creates the Veh
FUNC BOOL SPAWN_PTP_VEHICLE()

	IF REQUEST_LOAD_MODEL(serverBD.VehModel)
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niVeh)
			IF CREATE_NET_VEHICLE(serverBD.niVeh, serverBD.VehModel, serverBD.vStartLocation, serverBD.fStartHeading, DEFAULT, DEFAULT, DEFAULT, TRUE, FALSE)	
				
									
				SET_VEHICLE_DOORS_LOCKED(NET_TO_VEH(serverBD.niVeh), VEHICLELOCK_UNLOCKED)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_VEH(serverBD.niVeh), TRUE)
				SET_VEHICLE_TYRES_CAN_BURST(NET_TO_VEH(serverBD.niVeh),FALSE)
				SET_ENTITY_INVINCIBLE(NET_TO_ENT(serverBD.niVeh),TRUE)
				SET_VEHICLE_OCCUPANTS_TAKE_EXPLOSIVE_DAMAGE(NET_TO_VEH(serverBD.niVeh), TRUE)
				
				INT iMaxHealth = GET_ENTITY_MAX_HEALTH(NET_TO_VEH(serverBD.niVeh))
				SET_ENTITY_MAX_HEALTH(NET_TO_VEH(serverBD.niVeh),ROUND(GET_VEHICLE_HEALTH_MODIFIER() * (iMaxHealth * 2)))
				SET_ENTITY_HEALTH(NET_TO_VEH(serverBD.niVeh),ROUND(GET_VEHICLE_HEALTH_MODIFIER() * (iMaxHealth * 2)))
				SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(NET_TO_VEH(serverBD.niVeh),FALSE)
				SET_VEHICLE_DOORS_LOCKED_FOR_NON_SCRIPT_PLAYERS(NET_TO_VEH(serverBD.niVeh), TRUE)
				
				IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh", DECOR_TYPE_INT)
                    DECOR_SET_INT(NET_TO_VEH(serverBD.niVeh), "Not_Allow_As_Saved_Veh", MP_NOT_ALLOWED_AS_SAVED_VEHICLE_DEC_SIMEON)
					PRINTLN("     ---------->     PASS THE PARCEL - DECORATOR 'Not_Allow_As_Saved_Veh' SET")
  				ENDIF
				
				CALL_SET_ALLOW_RAMMING_SOOP_OR_RAMP(NET_TO_VEH(serverBD.niVeh))
				
				//Prevents the vehicle from being used for activities that use passive mode - MJM 
				IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
					INT iDecoratorValue
					IF DECOR_EXIST_ON(NET_TO_VEH(serverBD.niVeh), "MPBitset")
						iDecoratorValue = DECOR_GET_INT(NET_TO_VEH(serverBD.niVeh), "MPBitset")
					ENDIF
					SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_PASSIVE_MODE_VEHICLE)
					SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_MODDABLE_VEHICLE)
					SET_BIT(iDecoratorValue, MP_DECORATOR_BS_HOLD_THE_WHEEL_VEHICLE)
					DECOR_SET_INT(NET_TO_VEH(serverBD.niVeh), "MPBitset", iDecoratorValue)
				ENDIF
				
				SET_MODEL_AS_NO_LONGER_NEEDED(serverBD.VehModel)
				
				CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === SPAWN_PTP_VEHICLE - Created vehicle at  ",serverBD.vStartLocation)
			ENDIF
		ENDIF
		
		//Check we have created the vehicle
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niVeh)
			//NET_PRINT_TIME() NET_PRINT("     ---------->     PASS THE PARCEL -  FAILING ON VEH CREATION") NET_NL()
			RETURN FALSE
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE: Check if a reward has been given to any player
///    
/// RETURNS: TRUE if any player has received the reward, FALSE otherwise
///    
FUNC BOOL SERVER_HAS_GIVEN_REWARD()

	BOOL bRewardGiven = FALSE
	INT iParticipant
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX( iParticipant))	
			PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX( iParticipant))
			IF NOT FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(playerID)
				IF NOT IS_BIT_SET(playerBD[iParticipant].iClientBitSet,CLIENT_BS_ENDING_SHOWN)
				OR playerBD[iParticipant].eAMPP_Stage != AMPP_END
					RETURN FALSE
				ELSE		
					IF IS_BIT_SET(playerBD[iParticipant].iClientBitSet,CLIENT_BS_REWARD_GIVEN)
					OR serverBD.piDriver = INVALID_PLAYER_INDEX() //No Winner
					OR IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_VEH_DESTROYED) //Vehicle destroyed
						bRewardGiven = TRUE
						CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === Server given reward to participant ",iParticipant)
					ENDIF
				ENDIF
			ELSE
				bRewardGiven = TRUE
				CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_CHALLENGES === Participant has hidden event, not giving reward ",iParticipant)
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN bRewardGiven

ENDFUNC

FUNC BOOL IS_PLAYER_IN_PTP_VEHICLE(PLAYER_INDEX playerID)

	PED_INDEX pedID = GET_PLAYER_PED(playerID)

	IF NOT IS_PED_INJURED(pedID)
		IF IS_PED_IN_VEHICLE(pedID,NET_TO_VEH(serverBD.niVeh))
			IF GET_SEAT_PED_IS_IN(pedID) = VS_DRIVER
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_DRIVER_A_PLAYER()
	PED_INDEX ped
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
		IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh))
			IF GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(serverBD.niVeh), VS_DRIVER) != NULL
				ped = GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(serverBD.niVeh), VS_DRIVER)
				IF IS_PED_A_PLAYER(ped)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_PARTICIPANT_LOOP()
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niVeh)
	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
	AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.niVeh))
	
		IF (IS_VEHICLE_SEAT_FREE(NET_TO_VEH(serverBD.niVeh)) OR NOT IS_DRIVER_A_PLAYER())
		OR (serverBD.piDriver != INVALID_PLAYER_INDEX()
		AND NOT IS_NET_PLAYER_OK(serverBD.piDriver))
			IF serverBD.piDriver != INVALID_PLAYER_INDEX()
			
				//Send a ticker to all players to inform them there is no driver
//				SCRIPT_EVENT_DATA_TICKER_MESSAGE message
//				message.TickerEvent = TICKER_EVENT_PLAYER_LOST_PARCEL
//				message.playerID = serverBD.piDriver
//				BROADCAST_TICKER_EVENT(message,ALL_PLAYERS_ON_SCRIPT())

				CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === Server - No Driver.")
				CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === IS_VEHICLE_SEAT_FREE(NET_TO_VEH(serverBD.niVeh)) = ", IS_VEHICLE_SEAT_FREE(NET_TO_VEH(serverBD.niVeh)), 
											" - IS_DRIVER_A_PLAYER() = ", IS_DRIVER_A_PLAYER(),
											" - serverBD.piDriver != INVALID_PLAYER_INDEX() = ", serverBD.piDriver != INVALID_PLAYER_INDEX(),
											" - IS_NET_PLAYER_OK(serverBD.piDriver) = ", IS_NET_PLAYER_OK(serverBD.piDriver))
				serverBD.piDriver = INVALID_PLAYER_INDEX()
			ENDIF
		ELSE

			//Maintain current driver of vehicle
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iServerPart))
				PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iServerPart))
								
				IF IS_NET_PLAYER_OK(playerID)
					IF IS_PLAYER_IN_PTP_VEHICLE(playerID)
						IF serverBD.piDriver != playerID
							serverBD.iNumTimesVehicleExchangeHands++
							serverBD.piDriver = playerID
							SET_BIT(serverBD.iServerBitSet, SERVER_BS_VEHICLE_COLLECTED)
							CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === Server - Driver is now ",GET_PLAYER_NAME(serverBD.piDriver))
							
							//Send a ticker to all players to inform them of driver change
//							IF serverBD.piDriver != INVALID_PLAYER_INDEX()
//							
//								BOOL bToDriver = serverBD.eAMPP_Stage >= AMPP_HOLD
//							
//								SCRIPT_EVENT_DATA_TICKER_MESSAGE message
//								message.TickerEvent = TICKER_EVENT_PLAYER_HAS_PARCEL
//								message.playerID = serverBD.piDriver
//								IF bToDriver
//									message.dataInt = 1
//								ENDIF
//								BROADCAST_TICKER_EVENT(message,ALL_PLAYERS_ON_SCRIPT())
//							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(playerBD[iServerPart].iClientBitSet, CLIENT_BS_VEHICLE_STUCK)
		SET_BIT(serverBD.iServerBitSet, SERVER_BS_VEHICLE_STUCK)
	ENDIF
	
	iServerPart++
	IF iServerPart >= NETWORK_GET_MAX_NUM_PARTICIPANTS()
		iServerPart = 0
	ENDIF
	


ENDPROC

FUNC BOOL SHOULD_PASS_THE_PARCEL_END()

	IF IS_ENTITY_DEAD(NET_TO_ENT(serverBD.niVeh))
	OR IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_VEHICLE_STUCK)
				
		SET_BIT(serverBD.iServerBitSet,SERVER_BS_VEH_DESTROYED)
		
		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === SHOULD_PASS_THE_PARCEL_END - Vehicle dead")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_LOW_PARTICIPANTS)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === SHOULD_PASS_THE_PARCEL_END - Low participants.")
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

//----------------------
//	Event Processing
//----------------------

PROC PROCESS_ENTITY_DAMAGED_EVENT(INT iCount)

	STRUCT_ENTITY_DAMAGE_EVENT sei 
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sei, SIZE_OF(sei))
	
		IF DOES_ENTITY_EXIST(sei.DamagerIndex)
			IF IS_ENTITY_A_PED(sei.DamagerIndex) 
				IF GET_PED_INDEX_FROM_ENTITY_INDEX(sei.DamagerIndex) =  PLAYER_PED_ID()
					IF DOES_ENTITY_EXIST(sei.VictimIndex)

						IF IS_ENTITY_A_PED(sei.VictimIndex)
							PED_INDEX pedID = GET_PED_INDEX_FROM_ENTITY_INDEX(sei.VictimIndex)
							IF IS_PED_A_PLAYER(pedID)
								IF GET_PED_INDEX_FROM_ENTITY_INDEX(sei.VictimIndex) != GET_PED_INDEX_FROM_ENTITY_INDEX(sei.DamagerIndex)
									PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedID)
									IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerID)
										PARTICIPANT_INDEX partID = NETWORK_GET_PARTICIPANT_INDEX(playerID)
										IF NETWORK_IS_PARTICIPANT_ACTIVE(partID)
											INT iPart = NATIVE_TO_INT(partID)
											
											//If killed the driver
											IF playerID = serverBD.piDriver
												CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === PROCESS_ENTITY_DAMAGE_EVENT - Damaged driver")
												IF sei.VictimDestroyed
													// Fix for url:bugstar:2414367
													playerBD[PARTICIPANT_ID_TO_INT()].iClientKillCounter ++
													IF NOT (playerBD[PARTICIPANT_ID_TO_INT()].iClientKillCounter > GET_RP_REWARD_KILL_CARRIER_CAP())
														INT iRPGiven = GET_RP_REWARD_KILL_CARRIER()
														GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD,PLAYER_PED_ID(),"",XPTYPE_COMPLETE,XPCATEGORY_COMPLETE_CHALLENGES,iRPGiven)
														CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === PROCESS_ENTITY_DAMAGE_EVENT - Awarding ",iRPGiven," RP.")
													ENDIF
												ENDIF
											ENDIF
											
											//Mark local player as particpating if damaged participant
											IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_PARTICIPATED_PERMANENT)
												IF IS_PARTICIPANT_ACTIVE_IN_EVENT(iPart)
													SET_AS_PERMANENT_PARTICIPANT()
													CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === CLIENT_BS_PARTICIPATED_PERMANENT due to damaging participant.")
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
						ELIF IS_ENTITY_A_VEHICLE(sei.VictimIndex)
							IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niVeh)
								//Mark local player as particpating if damaged mission vehicle
								IF sei.VictimIndex = NET_TO_ENT(serverBD.niVeh)
									IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_PARTICIPATED_PERMANENT)
										SET_AS_PERMANENT_PARTICIPANT()
										CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === CLIENT_BS_PARTICIPATED_PERMANENT due to damaging vehicle")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_PTP_TICKER_EVENTS(INT iEventID)

	SCRIPT_EVENT_DATA_TICKER_MESSAGE Event	
		
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
	
		PLAYER_INDEX playerWhoSent = Event.Details.FromPlayerIndex
	
		IF IS_NET_PLAYER_OK(playerWhoSent, FALSE, FALSE)
		
			SWITCH Event.TickerEvent
		
				CASE TICKER_EVENT_PLAYER_HAS_PARCEL
					IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE)
						IF Event.playerID != INVALID_PLAYER_INDEX()
							IF Event.playerID = PLAYER_ID()
								IF Event.dataInt = 1	//Not showing big message
									CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === Ticker - I have Parcel.")
									PRINT_TICKER("AMPP_ME")
								ENDIF
							ELSE
								IF Event.dataInt = 1	//Not showing big message
									CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === Ticker - ",GET_PLAYER_NAME(Event.playerID)," has Parcel.")
									PRINT_TICKER_WITH_PLAYER_NAME("AMPP_OTHER",Event.playerID)
								ENDIF
							ENDIF
							
						ENDIF
					ENDIF
				BREAK
				
				CASE TICKER_EVENT_PLAYER_LOST_PARCEL
					IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE)
						IF Event.playerID != INVALID_PLAYER_INDEX()
							IF Event.playerID = PLAYER_ID()
								CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === Ticker - I lost Parcel.")
								PRINT_TICKER("AMPP_MEL")
							ELSE
								CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === Ticker - ",GET_PLAYER_NAME(Event.playerID)," lost Parcel.")
								PRINT_TICKER_WITH_PLAYER_NAME("AMPP_OTHERL",Event.playerID)
							ENDIF
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF


ENDPROC

PROC PROCESS_PTP_EVENTS()

	INT iEventID
	EVENT_NAMES ThisScriptEvent
	STRUCT_EVENT_COMMON_DETAILS Details

	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iEventID
	
		ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iEventID)
					
		SWITCH ThisScriptEvent			
				
			CASE EVENT_NETWORK_DAMAGE_ENTITY
				IF IS_IT_SAFE_TO_DISPLAY_FM_EVENT_UI()
					PROCESS_ENTITY_DAMAGED_EVENT(iEventID)
				ENDIF
			BREAK
				
			CASE EVENT_NETWORK_SCRIPT_EVENT	
			
				GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Details, SIZE_OF(Details))
			
				SWITCH Details.Type

					CASE SCRIPT_EVENT_TICKER_MESSAGE
						IF IS_PARTICIPANT_ACTIVE_IN_EVENT(PARTICIPANT_ID_TO_INT())
						AND NOT FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(PLAYER_ID())
						AND IS_IT_SAFE_TO_DISPLAY_FM_EVENT_UI()
							PROCESS_PTP_TICKER_EVENTS(iEventID)
						ENDIF
					BREAK
				ENDSWITCH

			BREAK
				
		ENDSWITCH
		
	ENDREPEAT
ENDPROC

PROC CLEANUP_PTP_VEHICLE()

	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.niVeh)
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverbd.niVeh)
			ENTITY_INDEX vehID = NET_TO_VEH(serverBD.niVeh)
			IF NOT IS_ENTITY_DEAD(vehID)
				SET_ENTITY_INVINCIBLE(vehID ,FALSE)
				SET_VEHICLE_OCCUPANTS_TAKE_EXPLOSIVE_DAMAGE(NET_TO_VEH(serverBD.niVeh), FALSE)
				SET_VEHICLE_TYRES_CAN_BURST(NET_TO_VEH(serverBD.niVeh),TRUE)
				SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(NET_TO_VEH(serverBD.niVeh),TRUE)
							
				//Prevents the vehicle from being used for activities that use passive mode - MJM 
				IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
					INT iDecoratorValue
					IF DECOR_EXIST_ON(vehId, "MPBitset")
						iDecoratorValue = DECOR_GET_INT(vehId, "MPBitset")
					ENDIF
					CLEAR_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_PASSIVE_MODE_VEHICLE)
					CLEAR_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_MODDABLE_VEHICLE)
					CLEAR_BIT(iDecoratorValue, MP_DECORATOR_BS_HOLD_THE_WHEEL_VEHICLE)
					DECOR_SET_INT(vehId, "MPBitset", iDecoratorValue)
				ENDIF
			ENDIF
			SET_ENTITY_AS_NO_LONGER_NEEDED(vehId)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === CLEANUP_PTP_VEHICLE")
		ENDIF
	ENDIF
	
	IF DOES_BLIP_EXIST(VehBlip)
		REMOVE_BLIP(VehBlip)
	ENDIF
ENDPROC

//----------------------
//	Main Functions
//----------------------

//Helper proc for setting client stage
PROC SET_LOCAL_AMPP_STAGE(AMPP_RUN_STAGE stage)
	playerBD[PARTICIPANT_ID_TO_INT()].eAMPP_Stage = stage
	CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === playerBD.eAMPP_Stage = ",GET_AMPP_STAGE_STRING(stage))
ENDPROC

//Helper proc for setting server stage
PROC SET_SERVER_AMPP_STAGE(AMPP_RUN_STAGE stage)
	serverBD.eAMPP_Stage = stage
	CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === serverBD.eAMPP_Stage = ",GET_AMPP_STAGE_STRING(stage))
ENDPROC

PROC MAINTAIN_DPAD_DOWN_LEADERBOARD()

	DRAW_CHALLENGE_DPAD_LBD(chDPStruct.challengeLbdStruct, chDPStruct.siDpadMovie,SUB_CHALLENGE, chDPStruct.dpadVariables, chDPStruct.fmDpadStruct)
	
ENDPROC

PROC CLIENT_MAINTAIN_PARTICIPANT_LOOP()

	IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iClientPart))
		PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iClientPart))
		IF NETWORK_IS_PLAYER_ACTIVE(playerID)
			FM_EVENT_MAINTAIN_ACTIVE_PLAYER_CHECK(playerID)
			
			IF HAS_PARTICIPANT_PARTICIPATED_IN_EVENT(iClientPart)
				iQualifiedCount++	
			ENDIF
			iPlayerCount++
		ENDIF
	ENDIF
	
	iClientPart++
	IF iClientPart >= NETWORK_GET_MAX_NUM_PARTICIPANTS()
	
		IF iPlayerCount > iPeakParticipants
			iPeakParticipants = iPlayerCount
		ELSE
			iLeftInProgress = iPeakParticipants - iPlayerCount
		ENDIF
		
		
		IF iQualifiedCount > iPeakQualified
			iPeakQualified = iQualifiedCount
		ENDIF
		
		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === iPeakParticipants = ",iPeakParticipants)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === iPeakQualified = ",iPeakQualified)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === iLeftInProgress = ",iLeftInProgress)
		iPlayerCount = 0
		iClientPart = 0
		iQualifiedCount = 0
	ENDIF

ENDPROC

PROC MAINTAIN_SAFE_LOCATION_CHECKS()
	IF SHOULD_FM_EVENT_NET_ENTITY_WARP_TO_SAFE_LOCATION()
		HANDLE_WARP_FM_EVENT_NET_ENTITY_TO_SAFE_LOCATION(serverBD.niVeh)
	ENDIF
ENDPROC

PROC SETUP_SPAWN_OCCLUSION_AREAS()

	//url:bugstar:2603809 - Golf Course Fences
	IF serverBD.VehModel = CADDY
	
		VECTOR vNorthPos1 = <<-1113.746582,251.730103,96.323669>>
		VECTOR vNorthPos2 = <<-1379.407959,200.220276,51.664063>> 
		FLOAT fNorthWidth = 60.000000
		
		VECTOR vEastPos1 = <<-906.969299,-85.490051,69.111557>>
		VECTOR vEastPos2 = <<-1122.559448,267.069733,35.508194>> 
		FLOAT fEastWidth = 75.000000
		
		VECTOR vSouthPos1 = <<-879.095215,-102.131203,69.180756>>
		VECTOR vSouthPos2 = <<-1043.268677,-168.021088,7.363800>> 
		FLOAT fSouthWidth = 60.500000
		
		VECTOR vWestPos1 = <<-1046.612671,-168.735321,68.866058>>
		VECTOR vWestPos2 = <<-1306.752197,-54.279999,17.777527>> 
		FLOAT fWestWidth = 60.0000
		
		VECTOR vNorthWestPos1 = <<-1447.600220,160.853027,88.646133>>
		VECTOR vNorthWestPos2 = <<-1414.079834,-73.488495,22.508041>> 
		FLOAT fNorthWestWidth = 52.000000
		
		VECTOR vNorthNorthWestPos1 = <<-1421.793579,147.406876,85.787933>>
		VECTOR vNorthNorthWestPos2 = <<-1388.487427,233.189987,28.659195>> 
		FLOAT fNorthNorthWestWidth = 60.0000
		
		ADD_MISSION_SPAWN_OCCLUSION_ANGLED_AREA(vNorthPos1,vNorthPos2,fNorthWidth,TRUE)
		ADD_MISSION_SPAWN_OCCLUSION_ANGLED_AREA(vEastPos1,vEastPos2,fEastWidth,TRUE)
		ADD_MISSION_SPAWN_OCCLUSION_ANGLED_AREA(vSouthPos1,vSouthPos2,fSouthWidth,TRUE)
		ADD_MISSION_SPAWN_OCCLUSION_ANGLED_AREA(vWestPos1,vWestPos2,fWestWidth,TRUE)
		ADD_MISSION_SPAWN_OCCLUSION_ANGLED_AREA(vNorthWestPos1,vNorthWestPos2,fNorthWestWidth,TRUE)
		ADD_MISSION_SPAWN_OCCLUSION_ANGLED_AREA(vNorthNorthWestPos1,vNorthNorthWestPos2,fNorthNorthWestWidth,TRUE)

		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === SETUP_SPAWN_OCCLUSION_AREAS")
	ENDIF

ENDPROC

/// PURPOSE: Main client processing loop
///    
PROC CLIENT_PROCESSING()
	IF NOT FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(PLAYER_ID())
	AND IS_IT_SAFE_TO_DISPLAY_FM_EVENT_UI()
//		IF FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT(PLAYER_ID())
//			HANDLE_MAP_EXITING_PLAYERS()
//		ENDIF
		
		IF DRAW_CHALLENGE_DPAD_FOR_DEV_SPECTATOR()
			MAINTAIN_DPAD_DOWN_LEADERBOARD()
		ENDIF
		
		IF serverBD.piDriver != INVALID_PLAYER_INDEX()
			IF NOT IS_BIT_SET(iLocalBitset,LOCAL_BS_DONE_SETUP)
				COMMON_FREEMODE_AMBIENT_EVENTS_SET_UP(FMMC_TYPE_HOLD_THE_WHEEL)
				SET_BIT(iLocalBitset,LOCAL_BS_DONE_SETUP)
			ENDIF
			DO_HELP_TEXT_AND_MESSAGES(8)
		ENDIF
		
		SWITCH playerBD[PARTICIPANT_ID_TO_INT()].eAMPP_Stage
		
			CASE AMPP_START
				IF serverBD.eAMPP_Stage > AMPP_START
					IF serverBD.bOpenAirportGates
						UNLOCK_AIRPORT_GATES()
					ENDIF
					FLASH_MINIMAP_DISPLAY()
					SETUP_SPAWN_OCCLUSION_AREAS()
					
					SET_LOCAL_AMPP_STAGE(AMPP_GET)
				ENDIF	
			BREAK 
			
			CASE AMPP_GET
				DO_HELP_TEXT_AND_MESSAGES(13)
				
				IF IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iFmAmbientEventBitSet, ciSET_PLAYER_OPTED_IN)
					SET_AS_PERMANENT_PARTICIPANT()
				ENDIF
				
				IF telemetryStruct.m_startTime = 0
					telemetryStruct.m_startTime	= GET_CLOUD_TIME_AS_INT()
				ENDIF
				IF serverBD.eAMPP_Stage > AMPP_HOLD
					SET_LOCAL_AMPP_STAGE(AMPP_REWARDS)
				ELIF serverBD.piDriver = PLAYER_ID()
					IF telemetryStruct.m_timeTakenForObjective = 0
						telemetryStruct.m_timeTakenForObjective = GET_CLOUD_TIME_AS_INT()  
					ENDIF
					TRIGGER_PTP_AUDIO()
					SET_LOCAL_AMPP_STAGE(AMPP_HOLD)
				ELSE
					IF IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_SUDDEN_DEATH)
						IF IS_PARTICIPANT_ACTIVE_IN_EVENT(PARTICIPANT_ID_TO_INT())
							DO_HELP_TEXT_AND_MESSAGES(5)
						ENDIF
					ENDIF
					IF serverBD.eAMPP_Stage = AMPP_GET
					AND IS_PLAYER_NEAR_PTP_VEHICLE()
						DO_HELP_TEXT_AND_MESSAGES(6)
					ENDIF
					KILL_PTP_AUDIO()
				ENDIF
				
				RESET_HELP_OR_MESSAGE(0)
				HANDLE_VEHICLE_BLIP()
				HANDLE_OBJECTIVE_IN_GARAGE(serverBD.niVeh, iGarageCheckTimer, iGarageSyncDelayTimer, TRUE)
				HANDLE_VEHICLE_STUCK()
				DRAW_EVENT_TIMER()
				DRAW_VEHICLE_MARKER()
				HANDLE_WANTED_AND_MENTAL()
				HANDLE_PARTICIPATION_FROM_RANGE()
				MAINTAIN_DPAD_DOWN_LEADERBOARD()
			BREAK
		
			CASE AMPP_HOLD
				IF serverBD.eAMPP_Stage > AMPP_HOLD
//					IF IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_VEH_DESTROYED)
//						IF NOT DISABLE_BADSPORT_TRACKER_FMEVENTS(FMMC_TYPE_HOLD_THE_WHEEL)
//							INCREMENT_BY_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT, 1)
//						ENDIF
//					ENDIF
					SET_LOCAL_AMPP_STAGE(AMPP_REWARDS)
				ELIF serverBD.piDriver != PLAYER_ID()
					CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_FM_EVENT_VEHICLE_GAINED)
					SET_LOCAL_AMPP_STAGE(AMPP_GET)
				ELSE
					IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_PARTICIPATED_PERMANENT)
						SET_AS_PERMANENT_PARTICIPANT()
						CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === CLIENT_BS_PARTICIPATED_PERMANENT due to entering vehicle")
					ENDIF
					IF telemetryStruct.m_timeTakenForObjective = 0
						telemetryStruct.m_timeTakenForObjective = GET_CLOUD_TIME_AS_INT()  
					ENDIF
				ENDIF
				IF NOT IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_SUDDEN_DEATH)
					DO_HELP_TEXT_AND_MESSAGES(0)
				ENDIF
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AMPP_NEAR")
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AMPP_NEAR2")
					CLEAR_HELP()
				ENDIF
				HANDLE_VEHICLE_BLIP()
				HANDLE_OBJECTIVE_IN_GARAGE(serverBD.niVeh, iGarageCheckTimer, iGarageSyncDelayTimer, TRUE)
				HANDLE_VEHICLE_STUCK()
				DRAW_EVENT_TIMER()
				HANDLE_WANTED_AND_MENTAL()
				MAINTAIN_DPAD_DOWN_LEADERBOARD()
			BREAK 
			
			CASE AMPP_REWARDS
				IF MAINTAIN_FM_EVENTS_END_UI(sEndUiVars,FALSE)
				OR IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_LOW_PARTICIPANTS)
					SET_LOCAL_AMPP_STAGE(AMPP_END)
				ENDIF
				IF serverBD.piDriver = PLAYER_ID()
					IF telemetryStruct.m_timeTakenForObjective = 0
						telemetryStruct.m_timeTakenForObjective = GET_CLOUD_TIME_AS_INT()  
					ENDIF
				ENDIF
				KILL_AMBIENT_EVENT_COUNTDOWN_AUDIO()
				KILL_PTP_SUDDEN_DEATH_AUDIO()
				GIVE_REWARDS()
				SHOW_END_MESSAGES()
				KILL_PTP_AUDIO()
				CLEANUP_PTP_VEHICLE()
				MAINTAIN_DPAD_DOWN_LEADERBOARD()
			BREAK
			
			CASE AMPP_EXPLODE
				IF serverBD.eAMPP_Stage > AMPP_EXPLODE
					SET_LOCAL_AMPP_STAGE(AMPP_END)
				ELSE
					MAINTAIN_EXPLODE_VEH()
				ENDIF
			BREAK
			
			CASE AMPP_END
				
			BREAK
			

		ENDSWITCH
		
	ELSE
		IF DOES_BLIP_EXIST(VehBlip)
			REMOVE_BLIP(VehBlip)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === HANDLE_VEHICLE_BLIP - VEH BLIP REMOVED - UI HIDDEN")
		ENDIF
	ENDIF
		
	HANDLE_OBJECTIVE_TEXT()
	HANDLE_RESPAWNING()
	HANDLE_EVENT_CRITICALITY()
	HANDLE_VEHICLE_IN_SHOP()
	HANDLE_PLAYER_ENTERING_VEHICLE()
	CLIENT_MAINTAIN_PARTICIPANT_LOOP()
	MAINTAIN_SAFE_LOCATION_CHECKS()
ENDPROC

/// PURPOSE: Main server processing loop
///    
PROC SERVER_PROCESSING()

	IF SHOULD_BAIL_FM_EVENT_DUE_TO_LOW_SESSION_NUMBERS(DEFAULT,DEFAULT,FALSE)
		IF NOT IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_LOW_PARTICIPANTS)
			serverBD.iEndTime = NATIVE_TO_INT(GET_NETWORK_TIME())
			SET_BIT(serverBD.iServerBitSet,SERVER_BS_LOW_PARTICIPANTS)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === SET_BIT(serverBD.iServerBitSet,SERVER_BS_LOW_PARTICIPANTS)")
		ENDIF
	ENDIF
	
	SWITCH serverBD.eAMPP_Stage
		
		CASE AMPP_START
			IF SPAWN_PTP_VEHICLE()
				SET_SERVER_AMPP_STAGE(AMPP_GET)
			ENDIF
		BREAK 
	
		CASE AMPP_GET
			MAINTAIN_PARTICIPANT_LOOP()
			IF serverBD.piDriver != INVALID_PLAYER_INDEX()
				REINIT_NET_TIMER(serverBD.eventTimer)
				CLEAR_AREA_OF_PROJECTILES(serverBD.vStartLocation, 5.0, TRUE)
				SET_SERVER_AMPP_STAGE(AMPP_HOLD)
			ENDIF
			IF HAS_NET_TIMER_EXPIRED(serverBD.timeoutTimer, g_sMPTunables.iPass_the_Parcel_Event_Expiry_Time)
			OR SHOULD_PASS_THE_PARCEL_END()
				SET_SERVER_AMPP_STAGE(AMPP_REWARDS)
			ENDIF
		BREAK 
		
		CASE AMPP_HOLD
			IF SHOULD_PASS_THE_PARCEL_END()
			OR (IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_SUDDEN_DEATH)
			AND serverBD.piDriver != INVALID_PLAYER_INDEX())
				serverBD.iEndTime = NATIVE_TO_INT(GET_NETWORK_TIME())
				SET_SERVER_AMPP_STAGE(AMPP_REWARDS)
			ELIF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.eventTimer) >= GET_CHALLENGE_TIME_LIMIT()
			#IF IS_DEBUG_BUILD
			OR ((bSkipTime OR MPGlobalsAmbience.bEndCurrentFreemodeEvent)
			AND NOT IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_SUDDEN_DEATH))
			#ENDIF
				IF NOT IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_SUDDEN_DEATH)
					IF serverBD.piDriver = INVALID_PLAYER_INDEX()
						IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.eventTimer) >= GET_CHALLENGE_TIME_LIMIT() + 250
							SET_BIT(serverBD.iServerBitSet,SERVER_BS_SUDDEN_DEATH)
							REINIT_NET_TIMER(serverBD.eventTimer)
						ENDIF
					ELSE
						serverBD.iEndTime = NATIVE_TO_INT(GET_NETWORK_TIME())
						SET_SERVER_AMPP_STAGE(AMPP_REWARDS)
					ENDIF
					
				ELSE
					serverBD.iEndTime = NATIVE_TO_INT(GET_NETWORK_TIME())
					SET_SERVER_AMPP_STAGE(AMPP_REWARDS)
				ENDIF
			ENDIF
			MAINTAIN_PARTICIPANT_LOOP()
		BREAK 
		
		CASE AMPP_REWARDS
			IF HAS_NET_TIMER_EXPIRED(serverBD.rewardTimer,PP_REWARD_TIME) //Timeout
			OR SERVER_HAS_GIVEN_REWARD() //Given rewards and shown end messages
				REINIT_NET_TIMER(serverBD.ExplodeVehTimer)
				SET_SERVER_AMPP_STAGE(AMPP_END)		
			ENDIF
		BREAK
		
		CASE AMPP_EXPLODE
			IF IS_ENTITY_DEAD(NET_TO_ENT(serverBD.niVeh))
				SET_SERVER_AMPP_STAGE(AMPP_END)	
			ENDIF
		BREAK
		
		CASE AMPP_END
		
		BREAK

	ENDSWITCH

ENDPROC

/// PURPOSE: Initialises the client data
///    
/// RETURNS: TRUE if initialised, FALSE otherwise
///    
FUNC BOOL INIT_CLIENT()

	#IF IS_DEBUG_BUILD
	CREATE_WIDGETS()
	#ENDIF
	
	FM_EVENT_BLOCK_SPECIFIC_GANG_ATTACKS(FMMC_TYPE_HOLD_THE_WHEEL, GET_TYPE_FROM_VEH())

	RETURN TRUE
ENDFUNC

/// PURPOSE: Initialises the server data
///    
/// RETURNS: TRUE if initialised, FALSE otherwise
///   
FUNC BOOL INIT_SERVER()
			
	GET_RANDOM_VEHICLE()
	GET_RANDOM_START_LOCATION()
		
	serverBD.piDriver = INVALID_PLAYER_INDEX()

	RETURN TRUE
ENDFUNC

/// PURPOSE: Help fucntion to get a debug string for the mission state
///    
/// PARAMS:
///    iState - The mission state
/// RETURNS: The name of the state
///    
FUNC STRING GET_MISSION_STATE_STRING(INT iState)

	SWITCH(iState)
		CASE GAME_STATE_INIT 				RETURN "GAME_STATE_INIT"
		CASE GAME_STATE_RUNNING 			RETURN "GAME_STATE_RUNNING"
		CASE GAME_STATE_TERMINATE_DELAY 	RETURN "GAME_STATE_TERMINATE_DELAY"
		CASE GAME_STATE_END 				RETURN "GAME_STATE_END"
	ENDSWITCH

	RETURN "UNKNOWN STATE!"

ENDFUNC

//Helper function to get a clients game/mission state
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iClientGameState
ENDFUNC

//Helper function to get the servers game/mission state
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC

//Helper function to set a clients game/mission state
PROC SET_CLIENT_MISSION_STATE(INT iPlayer,INT iState)
	playerBD[iPlayer].iClientGameState = iState
	CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === SET_CLIENT_MISSION_STATE = ",GET_MISSION_STATE_STRING(iState))
ENDPROC

//Helper function to set the servers game/mission state
PROC SET_SERVER_MISSION_STATE(INT iState)
	serverBD.iServerGameState = iState
	CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === SET_SERVER_MISSION_STATE = ",GET_MISSION_STATE_STRING(iState))
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------

PROC SCRIPT_CLEANUP()

	CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === SCRIPT_CLEANUP")
	
	KILL_PTP_AUDIO(TRUE)
	KILL_PTP_SUDDEN_DEATH_AUDIO()
	
	Clear_Any_Objective_Text_From_This_Script()
	
	IF DOES_BLIP_EXIST(VehBlip)
		REMOVE_BLIP(VehBlip)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === CLEANUP - VEH BLIP REMOVED")
	ENDIF	
	
	//Last one out, kill the vehicle
	IF NETWORK_GET_NUM_PARTICIPANTS() <= 1
		CLEANUP_PTP_VEHICLE()
	ENDIF
		
	CLEAR_PLAYER_NEXT_RESPAWN_LOCATION()		
	CLEAR_SPECIFIC_SPAWN_LOCATION_FROM_THIS_SCRIPT()
	
	COMMON_FREEMODE_AMBIENT_EVENTS_CLEAN_UP(FMMC_TYPE_HOLD_THE_WHEEL,DEFAULT,(serverBD.eAMPP_Stage = AMPP_END))
	
	IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		telemetryStruct.m_endReason             =AE_END_FORCED
	ELIF IS_BIT_SET(serverBD.iServerBitSet,SERVER_BS_LOW_PARTICIPANTS)
		telemetryStruct.m_endReason             =AE_END_LOW_NUMBERS
	ELSE
		IF telemetryStruct.m_endReason       	!=AE_END_WON
			telemetryStruct.m_endReason         =AE_END_LOST
		ENDIF
	ENDIF
	
	telemetryStruct.m_uid0 = serverBD.iHashedMac
	telemetryStruct.m_uid1 = serverBD.iMatchHistoryId
	telemetryStruct.m_playersLeftInProgress = iLeftInProgress
	telemetryStruct.m_playersParticipating = iPeakQualified
	telemetryStruct.m_timeTakenToComplete	= GET_CLOUD_TIME_AS_INT() - telemetryStruct.m_startTime
	
	IF NOT g_sMPTunables.bPass_the_Parcel_Disable_Share_Cash
		IF telemetryStruct.m_cashEarned > 0		
			IF telemetryStruct.m_endReason       	=AE_END_WON
				SET_DO_FM_EVENT_SHARE_CASH_HELP_IF_NEEDED()
			ENDIF
		ENDIF
	ENDIF

	PROCESS_CURRENT_AMBIENT_EVENT_PLAYSTATS(telemetryStruct, serverBD.iNumTimesVehicleExchangeHands)
	
	TERMINATE_THIS_THREAD()
	
ENDPROC

FUNC BOOL SHOULD_PASS_THE_PARCEL_TERMINATE()

	IF IS_KILL_CURRENT_FREEMODE_EVENT_GLOBAL_SET()
		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === SHOULD_PASS_THE_PARCEL_TERMINATE - IS_KILL_CURRENT_FREEMODE_EVENT_GLOBAL_SET")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)

	CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === PROCESS_PRE_GAME")
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission), missionScriptArgs)
	
	//Reserve Entities
	RESERVE_NETWORK_MISSION_OBJECTS(0)
	RESERVE_NETWORK_MISSION_VEHICLES(GET_AML_NUM_VEHICLES_REQUIRED(AML_SCRIPT_PASS_THE_PARCEL))
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	//common event proccess pre game
	COMMON_FREEMODE_AMBIENT_EVENTS_PROCESS_PRE_GAME(FMMC_TYPE_HOLD_THE_WHEEL)
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD,SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD,SIZE_OF(playerBD))
	
	IF !Wait_For_First_Network_Broadcast()
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBD.iHashedMac, serverBD.iMatchHistoryId)
	ENDIF
	
	telemetryStruct.m_playersNotified = NETWORK_GET_TOTAL_NUM_PLAYERS()
	
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
		
	RETURN TRUE
	
ENDFUNC

//----------------------
//	MAIN SCRIPT
//----------------------

SCRIPT(MP_MISSION_DATA missionScriptArgs)
	

	CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === START")
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NOT PROCESS_PRE_GAME(missionScriptArgs)
			SCRIPT_CLEANUP()
		ENDIF	
	ENDIF
	
	WHILE (TRUE)
	
		MP_LOOP_WAIT_ZERO()
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		OR SHOULD_PASS_THE_PARCEL_TERMINATE()
			CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")
			SCRIPT_CLEANUP()
		ENDIF
		
		//Call the common FM Events every frame callse
		FM_EVENT_COMMON_EVERY_FRAME_CALLS()
		
		PROCESS_PTP_EVENTS()
		
		SWITCH(GET_CLIENT_MISSION_STATE(NETWORK_PLAYER_ID_TO_INT()))
		
			CASE GAME_STATE_INIT
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					IF INIT_CLIENT()
						SET_CLIENT_MISSION_STATE(NETWORK_PLAYER_ID_TO_INT(),GAME_STATE_RUNNING)
					ENDIF
				ENDIF
			BREAK
			
			CASE GAME_STATE_RUNNING
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					CLIENT_PROCESSING()
					
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					#IF IS_DEBUG_BUILD OR bEndScriptLocal #ENDIF
					
					SET_CLIENT_MISSION_STATE(NETWORK_PLAYER_ID_TO_INT(),GAME_STATE_END)
				ENDIF
			BREAK
						
			CASE GAME_STATE_END
				SCRIPT_CLEANUP()
			BREAK
			
		ENDSWITCH
	
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
			SWITCH(GET_SERVER_MISSION_STATE())
			
				CASE GAME_STATE_INIT
					IF INIT_SERVER()
						BROADCAST_SET_LAST_FM_EVENT_VARIATION(FM_EVENT_GET_VARIATION_FROM_EVENT_TYPE_AND_SUB_TYPE(FMMC_TYPE_HOLD_THE_WHEEL, GET_TYPE_FROM_VEH()))
						SET_SERVER_MISSION_STATE(GAME_STATE_RUNNING)
					ENDIF
				BREAK
				
				CASE GAME_STATE_RUNNING
					IF serverBD.eAMPP_Stage = AMPP_END
					#IF IS_DEBUG_BUILD 
					OR bEndScript
					OR MPGlobalsAmbience.bKillActiveEvent
						MPGlobalsAmbience.bKillActiveEvent = FALSE
						#ENDIF
						SET_SERVER_MISSION_STATE(GAME_STATE_TERMINATE_DELAY)
					ELSE
						SERVER_PROCESSING()
					ENDIF
				BREAK
				
				CASE GAME_STATE_TERMINATE_DELAY
					IF HAS_NET_TIMER_EXPIRED(iServerTerminateTimer,SERVER_TERMINATE_DELAY)					
						SET_SERVER_MISSION_STATE(GAME_STATE_END)
					ENDIF
				BREAK
				
				CASE GAME_STATE_END
					SCRIPT_CLEANUP()
				BREAK
				
			ENDSWITCH
		
		ENDIF
		
	ENDWHILE
	

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT

