//////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_KILL_LIST.sc														//
// Description: Players enter an attack vehicle and have to destroy MerryWeather patrols//
// Written by:  Ryan Baker																//
// Date: 26/03/2015																		//
//////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "net_mission.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_event.sch"
USING "commands_physics.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"

//
USING "net_scoring_common.sch"
USING "net_ambience.sch"

//USING "help_at_location.sch"
USING "commands_object.sch"

USING "net_xp_animation.sch"

USING "net_wait_zero.sch"
USING "am_common_ui.sch"

USING "DM_Leaderboard.sch"
USING"net_challenges.sch"
USING "freemode_events_header.sch"
USING "net_rank_ui.sch"
#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF

// Game States. 
CONST_INT GAME_STATE_INI 				0
CONST_INT GAME_STATE_RUNNING			1
CONST_INT GAME_STATE_LEAVE				2
CONST_INT GAME_STATE_TERMINATE_DELAY	3
CONST_INT GAME_STATE_END				4

ENUM KILL_LIST_STAGE_ENUM
	eAD_IDLE,
	eAD_ATTACK,
	eAD_OFF_MISSION,
	eAD_EXPLODE_VEH,
	eAD_CLEANUP
ENDENUM

//CONST_INT VT_LAND	0
//CONST_INT VT_AIR	1
//CONST_INT VT_MAX	2

CONST_INT MAX_NUM_WAVES						10

CONST_INT WAVE_TYPE_NONE					1
CONST_INT WAVE_TYPE_AIRCRAFT_ONLY			2
CONST_INT WAVE_TYPE_GROUND_PEDS_ONLY		3
CONST_INT WAVE_TYPE_GROUND_PEDS_AND_VEHS	4
CONST_INT WAVE_TYPE_BOSS					5


CONST_INT VEH_EXP_RANGE	50
CONST_INT MAX_PLAYERS	4

CONST_INT MAX_PLAYER_VEH	4


CONST_INT MAX_MW_PEDS	24
CONST_INT MAX_MW_VEHS	5
CONST_INT MAX_MW_ON_FOOT_PEDS	4
CONST_INT MAX_BOSS_PEDS	4

CONST_INT MAX_MW_PED_SPAWN_LOC	10
CONST_INT MAX_MW_VEH_SPAWN_LOC	5
CONST_INT MAX_MW_HEL_SPAWN_LOC	5


CONST_INT MAX_MW_PEDS_PER_VEH	4

CONST_INT MAX_VEH_AT_ONCE		4

CONST_INT TIME_MISSION_START	180000 //180000 //60000

CONST_INT TIME_EXPOLDE_VEH		30000

CONST_INT TIME_MISSION_TIMEOUT	600000 //600000 

CONST_INT TIME_DURATION			600000//600000 //600000


//Server Bitset Data
CONST_INT biS_AllVehiclesDestroyed		0
CONST_INT biS_AllVehiclePedsDead		1
CONST_INT biS_AllOnFootPedsDead			2
//CONST_INT biS_AtLeastOnePlayerInVeh		3
CONST_INT biS_MissionStartExpired		4
//CONST_INT biS_AtLeastTwoPlayersInVeh	5
CONST_INT biS_DurationExpired			6
CONST_INT biS_DurationStarted			7
CONST_INT biS_Competitive				8
CONST_INT biS_MinNumberOfPlayersInVehs	9
CONST_INT biS_GotUWType					10
CONST_INT biS_AllPlayersFinished		11
CONST_INT biS_TimeoutExpired			12
CONST_INT biS_SetupComplete				13
CONST_INT biS_TooFewValid				14
CONST_INT biS_EveryoneFailed			15
CONST_INT biS_AllVehiclesEmpty			16
CONST_INT biS_CleanupVehASAllDead		17
CONST_INT biS_EnemySavageEmpty			18
CONST_INT biS_CreatedAtLeastOneVeh		19	
CONST_INT biS_CleanupUnusedVehicles		20

CONST_INT PED_AI_STATE_CREATED			0
CONST_INT PED_AI_STATE_GOTO_PLAYERS		1
CONST_INT PED_AI_STATE_ATTACK_PLAYERS	2
CONST_INT PED_AI_STATE_CLEAN_UP			3
CONST_INT PED_AI_STATE_FINISHED			4

STRUCT STRUCT_LEADERBOARD_DATA
	INT iTeam
	INT iKills
	INT iPlayer
	INT iTeamKills
ENDSTRUCT
// The server broadcast data.
// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData
	INT iServerGameState
	
	INT iHistorySubType = -1
	INT iHistoryVariation = -1
	INT iServerBitSet
	
	BOOL bOpenAirportGates
	
	SCRIPT_TIMER inVehicleTimer
	
	NETWORK_INDEX niVeh[MAX_PLAYER_VEH]
	INT iNumPlayerVeh = 0
	INT iVehicleFullBitSet
	INT iAtLeastOnePlayerBitSet
	INT iAtLeastTwoPlayersBitSet
	
	INT iPlayerLockedOutBitset
	
	PLAYER_INDEX piPlayers[MAX_PLAYERS]
	
	PLAYER_INDEX playerDriving[MAX_PLAYER_VEH]
	
	MODEL_NAMES VehModel 	= RHINO
	INT iVehicleSeats = 1
	//INT iVehicleType		= VT_LAND
	
	INT iLocation = -1
	VECTOR vStartLocation[MAX_PLAYER_VEH]
	FLOAT fStartHeading[MAX_PLAYER_VEH]
	
	NETWORK_INDEX niMwPed[MAX_MW_PEDS]
	NETWORK_INDEX niMwVeh[MAX_MW_VEHS]
	
	NETWORK_INDEX niBossVeh
	NETWORK_INDEX niBossPeds[MAX_BOSS_PEDS]
	INT iPedState[MAX_MW_PEDS]
	INT iPedPlayerTargetServer[MAX_MW_VEHS]
	INT iVehSeats
	
	VECTOR vMwPedSpawn[MAX_MW_PED_SPAWN_LOC]
	FLOAT fMwPedSpawn[MAX_MW_PED_SPAWN_LOC]
	VECTOR vMwVehSpawn[MAX_MW_VEH_SPAWN_LOC]
	FLOAT fMwVehSpawn[MAX_MW_PED_SPAWN_LOC]
	SCRIPT_TIMER timeLastSpawn[MAX_MW_PED_SPAWN_LOC]
	
	SCRIPT_TIMER timeInitialSpawn[MAX_MW_PED_SPAWN_LOC]
	INT iTimeInitialSpawn[MAX_MW_PED_SPAWN_LOC]
//	VECTOR vMwHelSpawn[MAX_MW_HEL_SPAWN_LOC]
//	FLOAT fMwHelSpawn[MAX_MW_PED_SPAWN_LOC]
	
	MODEL_NAMES MwPedModel 	= S_M_Y_BLACKOPS_01
	MODEL_NAMES MwVehModel 	= INSURGENT
	
	KILL_LIST_STAGE_ENUM eStage = eAD_IDLE
	
	INT iMaxVehicles = MAX_MW_VEHS
	INT iMaxOnFloorPeds = MAX_MW_ON_FOOT_PEDS
	
	INT iClearAreaBitSet
	
	INT iCreateMwVehBitSet
	INT iMwPedsToSpawn = 8
	INT iMwPedsSpawned
	INT iMwVehiclesToSpawn = 8
	INT iMwTotalVehiclesSpawned
	INT iTotalUwPedsSpawned
	INT iVehiclesSpawned = 0
	INT iVehicleSpawnedThisWave = 0
	INT iVehCleanedUpBitset
	INT iTotalKills = 0
	INT iKillGoal = -1
	
	INT iKillsPerVehicle[MAX_PLAYER_VEH]
	INT iMissingKillsPerVehicle[MAX_PLAYER_VEH]
	INT iNumPlayersInTeam[MAX_PLAYER_VEH]
	
	INT iTargetVehDestroyedBitSet
	INT iServerTargetPedKilledBitset[2]
	
	INT iPendingPedCleanupBitset[2]
	
	INT iNumOfWaves = 0
	INT iCurrentWave = 0
	INT iWaveType[MAX_NUM_WAVES]
	INT iNumVehiclesInWave[MAX_NUM_WAVES]
	INT iVehKillsThisWave
	
	INT iVehEmptyBitSet
	
	INT iPlayerVehNoDriverBitSet
	INT iNumOnFootPedsInWave[MAX_NUM_WAVES]
	INT iPedOnFootKillsThisWave
	INT iPedOnFootSpawnedThisWave
	INT iTotalOnFootPedSpawns
	
	INT iVehExplodeTooFewPlayers
	
	SCRIPT_TIMER MissionTerminateDelayTimer
	SCRIPT_TIMER DestroyCheckDelayTimer
	SCRIPT_TIMER ExplodeVehTimer
	SCRIPT_TIMER timeMissionStart
	SCRIPT_TIMER timeCleanup
	SCRIPT_TIMER timeMissionTimeout
	SCRIPT_TIMER timeDuration
	SCRIPT_TIMER timeChooseMode
	SCRIPT_TIMER timeCleanupVeh[MAX_MW_VEHS]
	SCRIPT_TIMER timeFailsafeAllDead
	SCRIPT_TIMER timeExplodeTooFewPlayers[MAX_MW_VEHS]
	SCRIPT_TIMER timeBeforeFailTooFewPlayers
	SCRIPT_TIMER timeInitialWait
	SCRIPT_TIMER timeCleanupPeds[MAX_MW_PEDS]
	
	INT iStartTimeLimit = -1
	
	
	SCRIPT_TIMER timePedRespawn[MAX_MW_PEDS] 
	INT iMinPlayers = -1
	
	INT iPedRetaskGotoBitSet
	
	STRUCT_LEADERBOARD_DATA sLeaderboardData[NUM_NETWORK_PLAYERS]
	STRUCT_LEADERBOARD_DATA sLeaderboardDataTeams[MAX_PLAYER_VEH]
	
	INT iHashedMac
	INT iMatchHistoryId
	INT iPlayersLeftInProgress 
	INT iPeakParticipants
	INT iPeakQualifiers
	
	INT iLockVehicleDoorsDueToNoPlayers
	
	#IF IS_DEBUG_BUILD
	BOOL bAllowWith2Players = FALSE
	
	#ENDIF
ENDSTRUCT
ServerBroadcastData serverBD

scrFmEventAmbientMission telemetryStruct

INT iTempKillsPerVehicle[MAX_PLAYER_VEH] 

CONST_INT biP_Player1				0
CONST_INT biP_Player2				1
CONST_INT biP_Player3				2
CONST_INT biP_Player4				3
CONST_INT biP_InVehicle				4
CONST_INT biP_LockedDoors			5
CONST_INT biP_MissionStartExpired	6
CONST_INT biP_DisabledMode			7
CONST_INT biP_DoneReward			8
CONST_INT biP_HidingEvent			9
CONST_INT biP_ApproachingOOB		10
CONST_INT biP_IamOOB				11
CONST_INT biP_NoConsiderVeh			12
// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	INT iGameState

	INT iInVehicleBitSet
	INT iPlayerBitSet
	INT iPlayerTargetPedKilledBitset[2]
	INT iPlayerDestVehBitSet
	INT iPlayerVehNoDriverBitSet
	INT iMyKills
	INT iMyUwTeam = -1
	INT iVehTooFewPlayers = -1
	INT iMyUwVehDamageTime[MAX_MW_VEHS]
	
	KILL_LIST_STAGE_ENUM eStage = eAD_IDLE
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

///// MISSION VARIABLES /////
INT iBoolsBitSet
CONST_INT biHelp1Done			0
CONST_INT biHelp2Done			1
CONST_INT biHelp3Done			2
//CONST_INT biP_NoConsiderVeh		3
CONST_INT biWantedLevel			4
CONST_INT biTimeHelpDOne		5
CONST_INT biExitVehHelpDone		6
CONST_INT biSHouldDoExitVehHelp	7
CONST_INT biDoneValkHelp		8
CONST_INT biDoneMusic			9
CONST_INT biResetMusic			10
CONST_INT biDoneNotAvailHelp	11
CONST_INT biDoneExplodeHelp		12
CONST_INT biCompletedOK			13
CONST_INT biMinPlayerVeh		14
CONST_INT biUWStartedTick		15
CONST_INT biUWWinTick1			16
CONST_INT biUWWinTick2			17
CONST_INT biUWWinTick3			18
CONST_INT biUWWinTick4			19
CONST_INT biFlashedBlip			20
CONST_INT biDiedDuringEvent		21
CONST_INT biDisabledCorona		22
CONST_INT biVehDestroyed		23
CONST_INT biInitVoiceChat		24
CONST_INT biResetVoiceChat		25
CONST_INT biWaitForShard		26
CONST_INT biExplodeSOund		27
CONST_INT biExplodeSOund2		28
CONST_INT biExplodeSOund3		29
CONST_INT biExplodeSOund4		30
CONST_INT biClearConsider		31

INT iBoolsBitSet2
CONST_INT bi2_StartDurationSOund 	0
CONST_INT bi2_SetFriendlyFire		1
CONST_INT bi2_PassiveModeHelp		2
CONST_INT bi2_FlashedRadar			3
CONST_INT bi2_ResetFFOption			4
CONST_INT bi2_NotEnoughValidP		5
CONST_INT bi2_TooFewPlayersInVeh	6
CONST_INT bi2_SetPedsCanBeTargeted	7
CONST_INT bi2_SetPedLockedOutVeh	8
CONST_INT bi2_VehOkAfterKnockedOut	9
CONST_INT bi2_MyVehDestroyedAfterKO 10	
CONST_INT bi2_DoPlayerMightSTillWin	11
CONST_INT bi2_DonePlayerMightSTillW	12	
CONST_INT bi2_SetAsGhost			13
CONST_INT bi2_EventHiddenHelp		14
CONST_INT bi2_UsingKLVeh			15
CONST_INT bi2_USeStillDoEndShard	16
CONST_INT bi2_DoneOffMissionShard	17
CONST_INT bi2_AlreadyDonePartCashAndRp	18
CONST_INT bi2_ValkStartNoGunner		19

//BOOL bUpdatedPilotTarget[MAX_MW_VEHS]
INT iPedPlayerTargetClient[MAX_MW_VEHS]			

INT iStaggeredParticipant

INT iStaggeredClientPart

INT iMyMaxWantedLevel
FLOAT fMyVoiceChatProx = -100.0 

BLIP_INDEX VehBlip[MAX_PLAYER_VEH]
//BLIP_INDEX MwPedBlip[MAX_MW_PEDS]
//BLIP_INDEX MwVehBlip[0][MAX_MW_VEHS]

AI_BLIP_STRUCT MwPedBlipData[MAX_MW_PEDS]

//--Relationship groups

REL_GROUP_HASH relUWPlayer
REL_GROUP_HASH relUWAi 
REL_GROUP_HASH relMyFmGroup

//BOOL bIsCompetitiveVariation = TRUE//TRUE
//SCRIPT_TIMER OutOfVehTimer

INT iNumPlayersInVeh[MAX_PLAYER_VEH]
BOOL bMinReqNumPlayersInVeh[MAX_PLAYER_VEH]
//BOOL bVehHasDriver[MAX_PLAYER_VEH] 

SCRIPT_TIMER timeInVehStart

SCALEFORM_INDEX scDpadDownLeaderBoardMovie
THE_LEADERBOARD_STRUCT sDpadDownLeaderBoardData[NUM_NETWORK_PLAYERS]
DPAD_VARS sDPadVars
FM_DPAD_LBD_STRUCT sFmDPadLbData

STRUCT_FM_EVENTS_END_UI sEndUiVars

//SCRIPT_TIMER timeNoDriverCheck

INT iMwPedsSpawnedLocal
INT iLocalPlayerVehCount = -1
//INT iLocalKills = -1
INT iMyUwPlayerVeh = -1

INT iExplodeSOund[MAX_PLAYER_VEH]

INT iEventDurationExpireSound

INT iLocalSetVehRelGroupBitset
INT iLocalSetPedRelGroupBitSet

INT iLocalPedTooFarFromTarget

INT iStoredTimeRemaining

SCRIPT_TIMER timeLastUseSpawnCoords[MAX_MW_PED_SPAWN_LOC]
SCRIPT_TIMER timeMyOobCheck
SCRIPT_TIMER timeExpOob
SCRIPT_TIMER timeNoDriver
SCRIPT_TIMER timeMyParticipation
SCRIPT_TIMER timeTargetDestroyed
SCRIPT_TIMER timeVehEmpty[MAX_MW_VEHS]
SCRIPT_TIMER timeResults
SCRIPT_TIMER timeCleanupUnusedVeh

INT iMyPartiticpationTime = 0

VECTOR vMyOobCoord

INT iLocalLockVehicleDoorsDueToNoPlayers
INT iActivePlayersGreyBlipBitset

INT iMyUWVehSeat = -1
// Widgets & Debug.
#IF IS_DEBUG_BUILD
	BOOL bHostEndMissionNow
	BOOL bWarpToVeh
	//BOOL bAllow2Players
	BOOL bWdDrawSpawnPoints
	BOOL bWdDoScoreDebug
	BOOL bWdShowPedsStillToKill
	BOOL bWdDebugMissionEnd
	INT iWdWaveType
	BOOL bWdDOWaveDebug
	BOOL bWdDoLeaderboardDebug
	BOOL bWdTestTicker
	BOOL bWdForceWin
	BOOL bWdMyReward
	BOOL bWdTimedDebug
	BOOL bWdKillDriver
	BOOL bWdCheckLastDamager
	SCRIPT_TIMER timeDebug
	SCRIPT_TIMER timeSCTVDebug
	INT iDbgEventStartTime = -1
#ENDIF

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MAIN PROCS                            ////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Helper function to get the servers game/mission state
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC

FUNC BOOL IS_COMPETITIVE_UW()
	
	//RETURN bIsCompetitiveVariation
	
	RETURN IS_BIT_SET(serverBD.iServerBitSet, biS_Competitive)
ENDFUNC

FUNC INT GET_EVENT_DURATION()
//	RETURN 30000
//	RETURN 1200000 

	IF IS_COMPETITIVE_UW()
		RETURN (g_sMPTunables.iComp_Kill_List_Event_Time_Limit) // 600000
	ELSE
		RETURN (g_sMPTunables.iKill_List_Event_Time_Limit) // 600000
	ENDIF
ENDFUNC

FUNC INT GET_EVENT_EXPIRY_TIME()
//	RETURN 20000
	IF IS_COMPETITIVE_UW()
		RETURN g_sMPTunables.icompetitive_Kill_List_Event_Expiry_Time_ //60000
	ELSE
		RETURN (g_sMPTunables.iKill_List_Event_Expiry_Time_) // 600000
	ENDIF
ENDFUNC

FUNC INT GET_WAIT_FOR_OTHER_PLAYERS_TIME()
//	RETURN 10000
//	RETURN 5000
	#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_TestSavage")
			RETURN 5000
		ENDIF
		
		IF iDbgEventStartTime <> -1
			RETURN iDbgEventStartTime
		ENDIF
	#ENDIF
	
	IF IS_COMPETITIVE_UW()
		RETURN g_sMPTunables.icompetitive_Kill_List_Player_Wait_Timer_2_player_vehicles  //=  180000  
	ELSE
		RETURN(g_sMPTunables.iKill_List_Player_Wait_Timer_2_player_vehicles) // 180000
	ENDIF
	//RETURN 5000
ENDFUNC

FUNC INT GET_MINIMUM_PARTICIPANTS_FOR_COMPETITIVE_VARIATION()
	RETURN(g_sMPTunables.iComp_Kill_List_Minimum_Participants_Launch) // 5
ENDFUNC

FUNC INT GET_NUMBER_OF_PARTICIPANTS_FOR_2_PLAYER_VEHICLES() // 15
	RETURN(g_sMPTunables.iComp_Kill_List_Comp_Min_Player_2_Vehicle_Threshold)
ENDFUNC

FUNC INT GET_NUMBER_OF_PARTICIPANTS_FOR_3_PLAYER_VEHICLES() // 25
	RETURN(g_sMPTunables.iComp_Kill_List_Comp_Min_Player_3_Vehicle_Threshold)
ENDFUNC

FUNC INT GET_VEHICLE_DETONATION_TIME()
	IF IS_COMPETITIVE_UW()
		RETURN(g_sMPTunables.icompetitive_Kill_List_Detonation_Time) // 30000
	ELSE
		RETURN(g_sMPTunables.iKill_List_Detonation_Time) // 30000
	ENDIF
ENDFUNC

FUNC BOOL IS_VEHICLE_VARIATION_DISABLED(MODEL_NAMES mVeh)
	IF NOT IS_COMPETITIVE_UW()
		SWITCH mVeh
			CASE RHINO 		RETURN(g_sMPTunables.bKill_List_Disable_Car_Rhino)		BREAK
			CASE HYDRA 		RETURN(g_sMPTunables.bKill_List_Disable_Heli_Hydra)		BREAK
			CASE SAVAGE 	RETURN(g_sMPTunables.bKill_List_Disable_Heli_Savage)	BREAK
			CASE BUZZARD 	RETURN(g_sMPTunables.bKill_List_Disable_Heli_Buzzard)	BREAK
			CASE VALKYRIE 	RETURN(g_sMPTunables.bKill_List_Disable_Heli_Valkyrie)	BREAK
		ENDSWITCH
	ELSE
		SWITCH mVeh
			CASE RHINO 		RETURN(g_sMPTunables.bcompetitive_Kill_List_Disable_Car_Rhino)		BREAK
			CASE HYDRA 		RETURN(g_sMPTunables.bcompetitive_Kill_List_Disable_Heli_Hydra)		BREAK
			CASE SAVAGE 	RETURN(g_sMPTunables.bcompetitive_Kill_List_Disable_Heli_Savage)	BREAK
			CASE BUZZARD 	RETURN(g_sMPTunables.bcompetitive_Kill_List_Disable_Heli_Buzzard)	BREAK
			CASE VALKYRIE 	RETURN(g_sMPTunables.bcompetitive_Kill_List_Disable_Heli_Valkyrie)	BREAK
		ENDSWITCH
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC INT GET_BOSS_HEALTH()
	IF IS_COMPETITIVE_UW()
		RETURN g_sMPTunables.icompetitive_Kill_List_Boss_Health  //3000
	ELSE
		RETURN (g_sMPTunables.iKill_List_Boss_Health) // 3000
	ENDIF
ENDFUNC



FUNC INT GET_PLAYER_VEH_START_HEALTH_HELI()
	IF IS_COMPETITIVE_UW()
		RETURN g_sMPTunables.icompetitive_Kill_List_Vehicle_Health_HELI  //20000   
	ELSE
		RETURN (g_sMPTunables.iKill_List_Vehicle_Health_HELI ) // 20000
	ENDIF
ENDFUNC

FUNC INT GET_PLAYER_VEH_START_HEALTH_TANK()
	IF IS_COMPETITIVE_UW()
		RETURN g_sMPTunables.icompetitive_Kill_List_Vehicle_Health_tank //20000   
	ELSE
		RETURN (g_sMPTunables.iKill_List_Vehicle_Health_tank) // 20000
	ENDIF
ENDFUNC

FUNC INT GET_PLAYER_VEH_START_HEALTH_PLANE()
	IF IS_COMPETITIVE_UW()
		RETURN g_sMPTunables.icompetitive_Kill_List_Vehicle_Health_plane //20000   
	ELSE
		RETURN (g_sMPTunables.iKill_List_Vehicle_Health_plane) // 20000
	ENDIF
ENDFUNC

FUNC FLOAT GET_ENEMY_PED_HEALTH_MULTIPLIER()
	IF IS_COMPETITIVE_UW()
		RETURN g_sMPTunables.fcompetitive_Kill_List_Enemy_Health //1.0 
	ELSE
		RETURN (g_sMPTunables.fKill_List_Enemy_Health)
	ENDIF
ENDFUNC

FUNC FLOAT GET_ENEMY_ACCURACY_MULTIPLIER()
	IF IS_COMPETITIVE_UW()
		RETURN g_sMPTunables.fcompetitive_Kill_List_Enemy_Gun_Accuracy //1.0  
	ELSE
		RETURN(g_sMPTunables.fKill_List_Enemy_Gun_Accuracy) // 1.0
	ENDIF
ENDFUNC
FUNC FLOAT GET_ENEMY_MISSILE_ACCURACY_MULTIPLIER()
	IF IS_COMPETITIVE_UW()
		RETURN g_sMPTunables.fcompetitive_Kill_List_Enemy_Missile_Accuracy //1.0
	ELSE
		RETURN(g_sMPTunables.fKill_List_Enemy_Missile_Accuracy) // 1.0
	ENDIF
ENDFUNC


//-- Not currently Used!
FUNC INT GET_MAX_NUMBER_OF_CONCURRENT_ON_FOOT_PEDS()
	IF IS_COMPETITIVE_UW()
		RETURN g_sMPTunables.icompetitive_Kill_List_Max_Foot_Enemies_at_a_Time //4
	ELSE
		RETURN (g_sMPTunables.iKill_List_Max_Foot_Enemies_at_a_Time)
	ENDIF
ENDFUNC

FUNC INT GET_NUMBER_OF_GROUND_PEDS_TUNABLE_FOR_WAVE_TYPE(INT iWavetype)
	INT iNumEnemies = 0

	SWITCH iWavetype
		CASE WAVE_TYPE_GROUND_PEDS_AND_VEHS
		CASE WAVE_TYPE_GROUND_PEDS_ONLY
			IF IS_COMPETITIVE_UW()
				iNumEnemies = g_sMPTunables.icompetitive_Kill_List_Wave_Type_Foot_Enemies   // 4 
			ELSE
				iNumEnemies = g_sMPTunables.iKill_List_Wave_Type_Foot_Enemies   // 4
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN iNumEnemies
ENDFUNC


FUNC INT GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(INT iWaveType)
	INT iNumEnemies = 0
	SWITCH iWavetype
		CASE WAVE_TYPE_AIRCRAFT_ONLY	
			IF IS_COMPETITIVE_UW()
				iNumEnemies = g_sMPTunables.icompetitive_Kill_List_Wave_Type_Aircraft_Enemies  // 3
			ELSE
				iNumEnemies = g_sMPTunables.iKill_List_Wave_Type_Aircraft_Enemies  // 3
			ENDIF
		BREAK
		
		CASE WAVE_TYPE_BOSS
			IF IS_COMPETITIVE_UW()
				iNumEnemies = g_sMPTunables.icompetitive_Kill_List_Wave_Type_Boss_Enemies // 1
			ELSE
				iNumEnemies = g_sMPTunables.iKill_List_Wave_Type_Boss_Enemies // 1
			ENDIF
		BREAK
		
		CASE WAVE_TYPE_GROUND_PEDS_AND_VEHS
			IF IS_COMPETITIVE_UW()
				iNumEnemies = g_sMPTunables.icompetitive_Kill_List_Wave_Type_Ground_Vehicle_Enemies // 3
			ELSE
				iNumEnemies = g_sMPTunables.iKill_List_Wave_Type_Ground_Vehicle_Enemies // 3
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN iNumEnemies
ENDFUNC

///	PURPOSE: Need specific relationship groups for players taking part. Don't want AI to attack non-mission players
PROC SETUP_UW_REL_GROUPS()
	PRINTLN("     ---------->     KILL LIST - SETUP_UW_REL_GROUPS")
	
	ADD_RELATIONSHIP_GROUP("relUWPlayer", relUWPlayer)
	ADD_RELATIONSHIP_GROUP("relUWAi", relUWAi)
	
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		
		//Like Player
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rgFM_Team[i], relUWPlayer)	
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relUWPlayer, rgFM_Team[i])
		
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rgFM_Team[i], relUWAi)	
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relUWAi, rgFM_Team[i])
		
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relUWPlayer, RELGROUPHASH_COP)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_COP, relUWPlayer)
		
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relUWPlayer, rgFM_AiAmbientGangMerc[5])
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rgFM_AiAmbientGangMerc[5], relUWPlayer)
		
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relUWPlayer, rgFM_AiLike)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rgFM_AiLike, relUWPlayer)
		
	ENDREPEAT
	
	//Hate UW players
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relUWPlayer, relUWAi)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relUWAi, relUWPlayer)
	
	// Like Cops
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rgFM_Team[i], rgFM_AiLikePlyrLikeCops)
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rgFM_AiLikePlyrLikeCops, rgFM_Team[i])
//	//Dislike Player Like Cops
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE, rgFM_Team[i], rgFM_AiDislikePlyrLikeCops)
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE, rgFM_AiDislikePlyrLikeCops, rgFM_Team[i])
//	//Hate Player Like Cops
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rgFM_Team[i], rgFM_AiHatePlyrLikeCops)
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rgFM_AiHatePlyrLikeCops, rgFM_Team[i])
	
	//SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rgFM_Team[i], rgFM_AiHatedByCopsAndMercs)
	

	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_AMBIENT_GANG_CULT, relUWAi)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE , RELGROUPHASH_AMBIENT_GANG_CULT, relUWPlayer)

	
	
	//-- Cops
	SET_AMBIENT_COP_REL_TO_THIS_RELGROUP(ACQUAINTANCE_TYPE_PED_LIKE, relUWAi)
	
	//**AMBIENT GANGS
	SET_AMBIENT_GANG_LIKES_THIS_RELGROUP(relUWAi)
	SET_AMBIENT_GANG_LIKES_THIS_RELGROUP(relUWPlayer)
ENDPROC

PROC REMOVE_UW_REL_GROUPS()
	REMOVE_RELATIONSHIP_GROUP(relUWAi)
	REMOVE_RELATIONSHIP_GROUP(relUWPlayer)
ENDPROC

PROC SET_ALL_UW_ENTITIES_AS_ONLY_DAMAGED_BY_REL_GROUP()
	#IF IS_DEBUG_BUILD
		PRINTLN("     ---------->     KILL LIST -  [SET_ALL_UW_ENTITIES_AS_ONLY_DAMAGED_BY_REL_GROUP]  -  CALLED. Callstack...")
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	INT i
	
	REPEAT MAX_MW_PEDS i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niMwPed[i])
		//	SET_ENTITY_ONLY_DAMAGED_BY_RELATIONSHIP_GROUP(NET_TO_PED(serverBD.niMwPed[i]), TRUE, relUWPlayer)
			PRINTLN("     ---------->     KILL LIST -  [SET_ALL_UW_ENTITIES_AS_ONLY_DAMAGED_BY_REL_GROUP]  -  CALLED. Setting ped ", i)
		ENDIF
	ENDREPEAT
	
	i = 0
	
	REPEAT MAX_MW_VEHS i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niMwVeh[i])
		//	SET_ENTITY_ONLY_DAMAGED_BY_RELATIONSHIP_GROUP(NET_TO_PED(serverBD.niMwVeh[i]), TRUE, relUWPlayer)
			PRINTLN("     ---------->     KILL LIST -  [SET_ALL_UW_ENTITIES_AS_ONLY_DAMAGED_BY_REL_GROUP]  -  CALLED. Setting veh ", i)
		ENDIF
	ENDREPEAT
ENDPROC


PROC REMOVE_VEHICLE_BLIP()
	INT i
	//NET_PRINT_TIME() NET_PRINT("     ----------> KILL LIST - REMOVE_VEHICLE_BLIPS")
	//DEBUG_PRINTCALLSTACK()
	REPEAT MAX_PLAYER_VEH i
		IF DOES_BLIP_EXIST(VehBlip[i])
			REMOVE_BLIP(VehBlip[i])
			
			PRINTLN("     ---------->     KILL LIST -  [ADD_VEH_BLIP] [REMOVE_VEHICLE_BLIP]  -  VEHICLE BLIP REMOVED ", i)
		ENDIF
	ENDREPEAT
	
ENDPROC

FUNC BOOL CHECK_FOR_MATCHING_VEH_COORDS(INT iCurrent, VECTOR vToCheck)
	INT i
	VECTOR vCoords
	REPEAT MAX_PLAYER_VEH i
		IF i <> iCurrent
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
				
				vCoords = GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niVeh[i]), FALSE)
				IF ARE_VECTORS_EQUAL(vToCheck, vCoords)
					IF DOES_BLIP_EXIST(VehBlip[i])
					//	PRINTLN("     ---------->     KILL LIST -  [ADD_VEH_BLIP] [CHECK_FOR_MATCHING_VEH_COORDS] RETURNING TRUE FOR iCurrent = ", iCurrent, " vToCheck = ", vToCheck, " Same coords as veh ", i, " AT COORDS ", vCoords)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC


FUNC BOOL SHOULD_UI_BE_HIDDEN()
	BOOL bSHouldHide
	VEHICLE_INDEX vehTemp
	IF SHOULD_HIDE_AMBIENT_EVENT(ciPI_HIDE_MENU_ITEM_AMBIENT_EVENTS_KILL_LIST)
		bSHouldHide = TRUE
	ENDIF
	

	IF NOT IS_FM_TYPE_UNLOCKED(FMMC_TYPE_KILL_LIST)
		bSHouldHide = TRUE
	ENDIF
	
	
	IF NOT IS_IT_SAFE_TO_DISPLAY_FM_EVENT_UI()
		PRINTLN("     ---------->     KILL LIST[SHOULD_UI_BE_HIDDEN] TRUE IS_IT_SAFE_TO_DISPLAY_FM_EVENT_UI  <---------- ")
		bSHouldHide = TRUE
	ENDIF
	
	IF bSHouldHide 
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HidingEvent)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				vehTemp = GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())
				IF NOT IS_VEHICLE_DRIVEABLE(vehTemp)
				
					PRINTLN("2425383, SET_HIDE_STATS, FORCE_DRAW_CHALLENGE_DPAD_LBD, CALL TO FALSE  ---------->     KILL LIST[SHOULD_UI_BE_HIDDEN] TRUE IS_IT_SAFE_TO_DISPLAY_FM_EVENT_UI  <---------- ")
					FORCE_DRAW_CHALLENGE_DPAD_LBD(FALSE, -1)
					
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HidingEvent)
					CPRINTLN(DEBUG_NET_AMBIENT, "     ---------->     KILL LIST [SHOULD_UI_BE_HIDDEN] SET biP_HidingEvent  <---------- ")
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HidingEvent)
			CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HidingEvent)
			CPRINTLN(DEBUG_NET_AMBIENT, "     ---------->     KILL LIST [SHOULD_UI_BE_HIDDEN] CLEAR biP_HidingEvent  <---------- ")
		ENDIF
	ENDIF
	
	IF g_bCelebrationScreenIsActive
		PRINTLN("     ---------->     KILL LIST [SHOULD_UI_BE_HIDDEN] TRUE g_bCelebrationScreenIsActive")
		bSHouldHide = TRUE
	ENDIF
	
	IF IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
		PRINTLN("     ---------->     KILL LIST [SHOULD_UI_BE_HIDDEN] TRUE IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE")
		bSHouldHide = TRUE
	ENDIF
	
	IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_RACETOPOINT)
		PRINTLN("     ---------->     KILL LIST [SHOULD_UI_BE_HIDDEN] TRUE MPAM_TYPE_RACETOPOINT")
		bSHouldHide = TRUE
	ENDIF
	
	IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(PLAYER_ID())
		PRINTLN("     ---------->     KILL LIST [SHOULD_UI_BE_HIDDEN] TRUE FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION")
		bSHouldHide = TRUE
	ENDIF
	
	RETURN bSHouldHide
ENDFUNC

PROC MAINTAIN_BLIPS_FOR_SCTV_PLAYERS(INT iMySpecPart)
	INT i
	REPEAT MAX_PLAYER_VEH i
		IF NOT DOES_BLIP_EXIST(VehBlip[i])
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
				IF NOT IS_BIT_SET(serverBD.iVehicleFullBitSet, i)
					IF NOT IS_BIT_SET(playerBD[iMySpecPart].iInVehicleBitSet, i)
						VehBlip[i] = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(serverBD.niVeh[i]))
						SET_BLIP_SPRITE(VehBlip[i], RADAR_TRACE_TEMP_1) 
						SET_BLIP_COLOUR_FROM_HUD_COLOUR(VehBlip[i], HUD_COLOUR_NET_PLAYER2)
						
						IF IS_COMPETITIVE_UW()
							SET_BLIP_NAME_FROM_TEXT_FILE(VehBlip[i], "UW_BLIPC")	//Kill List Competitive
						ELSE
							SET_BLIP_NAME_FROM_TEXT_FILE(VehBlip[i], "UW_BLIP")		//Urban Warfare
						ENDIF
						
						SET_BLIP_PRIORITY(VehBlip[i], BLIPPRIORITY_HIGHEST)
						
						PRINTLN("     ---------->     KILL LIST -  [ADD_VEH_BLIP] MAINTAIN_BLIPS_FOR_SCTV_PLAYERS ADDED BLIP FOR VEH ", i)
					ELSE
						IF DOES_BLIP_EXIST(VehBlip[i])
							REMOVE_BLIP(VehBlip[i])
							PRINTLN("     ---------->     KILL LIST -  [ADD_VEH_BLIP] MAINTAIN_BLIPS_FOR_SCTV_PLAYERS REMOVE BLIP FOR VEH ", i, " AS iInVehicleBitSet")
						ENDIF
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(VehBlip[i])
						REMOVE_BLIP(VehBlip[i])
						PRINTLN("     ---------->     KILL LIST -  [ADD_VEH_BLIP] MAINTAIN_BLIPS_FOR_SCTV_PLAYERS REMOVE BLIP FOR VEH ", i, " AS iVehicleFullBitSet")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

//PURPOSE: Adds a blip to the Veh
PROC ADD_VEH_BLIP()
	INT i
	
	IF IS_BIT_SET(serverBD.iServerBitSet ,biS_TimeoutExpired)
		REMOVE_VEHICLE_BLIP()
	ENDIF
	//BOOL bRemoveBlips
	IF IS_COMPETITIVE_UW()
		IF iLocalPlayerVehCount <> serverBD.iNumPlayerVeh
			PRINTLN("     ---------->     KILL LIST -  [ADD_VEH_BLIP] - REMOVING PLAYER VEH BLIPS AS iLocalPlayerVehCount = ",iLocalPlayerVehCount, " <> serverBD.iNumPlayerVeh = ",serverBD.iNumPlayerVeh ) 
			iLocalPlayerVehCount = serverBD.iNumPlayerVeh
			REMOVE_VEHICLE_BLIP()
		ENDIF
	ENDIF
	VECTOR vCoords 

	IF NOT SHOULD_UI_BE_HIDDEN()
		REPEAT MAX_PLAYER_VEH i
			IF NOT DOES_BLIP_EXIST(VehBlip[i])
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
					IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[i])
					//	PRINTLN("     ---------->     KILL LIST -  [ADD_VEH_BLIP] - Think this veh is drivable ", i)
						vCoords = GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niVeh[i]), FALSE)
						IF NOT CHECK_FOR_MATCHING_VEH_COORDS(i,vCoords)
							IF NOT IS_BIT_SET(serverBD.iVehicleFullBitSet, i)
							//	Block_All_MissionsAtCoords_Missions(FALSE)
							//	HIDE_ALL_SHOP_BLIPS(FALSE)
							//	Unblock_All_MissionsAtCoords_Specific_Gang_Attacks()
								PRINTLN("     ---------->     KILL LIST - [ADD_VEH_BLIP] Adding player veh blip for veh ", i, " veh coords = ", vCoords)
								VehBlip[i] = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(serverBD.niVeh[i]))
								SET_BLIP_SPRITE(VehBlip[i], RADAR_TRACE_TEMP_1) 
							//	SET_BLIP_COLOUR(VehBlip[i], BLIP_COLOUR_PURPLE)
								SET_BLIP_COLOUR_FROM_HUD_COLOUR(VehBlip[i], HUD_COLOUR_NET_PLAYER2)
								//SHOW_HEIGHT_ON_BLIP(VehBlip[i], TRUE)
								IF NOT IS_BIT_SET(iBoolsBitSet, biFlashedBlip)
									SET_BLIP_FLASHES(VehBlip[i], TRUE)
									SET_BLIP_FLASH_TIMER(VehBlip[i], 7000)
									SET_BIT(iBoolsBitSet, biFlashedBlip)
								ENDIF
								SET_BLIP_PRIORITY(VehBlip[i], BLIPPRIORITY_HIGHEST)
								IF IS_COMPETITIVE_UW()
									SET_BLIP_NAME_FROM_TEXT_FILE(VehBlip[i], "UW_BLIPC")	//Kill List Competitive
								ELSE
									SET_BLIP_NAME_FROM_TEXT_FILE(VehBlip[i], "UW_BLIP")		//Urban Warfare
								ENDIF
								IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_FlashedRadar)
									FLASH_MINIMAP_DISPLAY()
									SET_BIT(iBoolsBitSet2, bi2_FlashedRadar)
								ENDIF

								NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  VEH BLIP ADDED ") NET_NL()
							ELSE
							//	PRINTLN("     ---------->     KILL LIST - [ADD_VEH_BLIP] NOT ADDING BLIP BECAUSE iVehicleFullBitSet IS SET ", i)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
					IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[i])
						REMOVE_BLIP(VehBlip[i])
						PRINTLN("     ---------->     KILL LIST - [ADD_VEH_BLIP] Removing player veh blip as IS_NET_VEHICLE_DRIVEABLE for veh ", i)
					ELIF IS_BIT_SET(serverBD.iVehicleFullBitSet, i)
						REMOVE_BLIP(VehBlip[i])
						PRINTLN("     ---------->     KILL LIST - [ADD_VEH_BLIP] Removing player veh blip as vehicle full for veh ", i)
					ENDIF
				ELSE
					REMOVE_BLIP(VehBlip[i])
					PRINTLN("     ---------->     KILL LIST - [ADD_VEH_BLIP] Removing player veh blip as NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID for veh ", i)
				ENDIF
			ENDIF
		ENDREPEAT
	ELSE
		REMOVE_VEHICLE_BLIP()
	ENDIF
	
ENDPROC

FUNC BOOL DOES_MY_UW_VEH_HAVE_MORE_THAN_ONE_PLAYER(INT iVeh)
	BOOL bAnotherPlayerInMyVeh
	INT iParticipant


	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NOT bAnotherPlayerInMyVeh
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
				IF iParticipant <> PARTICIPANT_ID_TO_INT()
					IF IS_BIT_SET(playerBD[iParticipant].iInVehicleBitSet, iVeh)
						bAnotherPlayerInMyVeh = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN  bAnotherPlayerInMyVeh
ENDFUNC

PROC SET_UW_VOICE_CHAT_OVERRIDE(BOOL bEnable, INT iMyVeh)
	PRINTLN("     ---------->     KILL LIST - [MAINTAIN_UW_VOICE_CHAT] [SET_R2P_VOICE_CHAT_OVERRIDE] CALLED WITH bEnable = ", bEnable, " iMyVeh = ", iMyVeh)
	
	INT iParticipant

	PLAYER_INDEX playerId

	iParticipant = 0
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			playerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
			
			IF IS_NET_PLAYER_OK(PlayerId, FALSE, TRUE)
			AND playerId != PLAYER_ID()
				IF IS_BIT_SET(playerBD[iParticipant].iInVehicleBitSet, iMyVeh)
				OR NOT bEnable
					NETWORK_OVERRIDE_CHAT_RESTRICTIONS(playerId, TRUE)
					PRINTLN("     ---------->     KILL LIST - [MAINTAIN_UW_VOICE_CHAT] [SET_R2P_VOICE_CHAT_OVERRIDE] OVERRIDE VOICE CHAT FOR PLAYER ", GET_PLAYER_NAME(playerId))
				ELSE
					IF bEnable
						NETWORK_OVERRIDE_CHAT_RESTRICTIONS(playerId, FALSE)
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ENDREPEAT
ENDPROC

FUNC INT GET_TEAM_VEHICLE_INDEX()
	INT index = -1
	INT i
	
	REPEAT MAX_PLAYER_VEH i
		IF index = -1
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
				IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[i])
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh[i]))
							index = i
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN index
ENDFUNC
PROC MAINTAIN_PLAYER_OUT_OF_BOUNDS()
	FLOAT fDist
	INT iVeh
	INT iOOBSound
	IF playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet <> 0
		IF serverBD.vehModel = RHINO
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_IamOOB)
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ApproachingOOB)
					IF NOT HAS_NET_TIMER_STARTED(timeMyOobCheck)
					OR (HAS_NET_TIMER_STARTED(timeMyOobCheck) AND HAS_NET_TIMER_EXPIRED(timeMyOobCheck, 5000))
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF ARE_VECTORS_EQUAL(vMyOobCoord, << 0.0, 0.0, 0.0>>)
								vMyOobCoord = serverBD.vStartLocation[0]
								PRINTLN("     ---------->     KILL LIST - [MAINTAIN_PLAYER_OUT_OF_BOUNDS] SETTING MY vMyOobCoord = ", vMyOobCoord)
							ENDIF
							fDist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vMyOobCoord)
							PRINTLN("     ---------->     KILL LIST - [MAINTAIN_PLAYER_OUT_OF_BOUNDS] Checking from Coord ", vMyOobCoord, " my dist = ", fDist)
							IF fDist > 750.0
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ApproachingOOB)
								PRINTLN("     ---------->     KILL LIST - [MAINTAIN_PLAYER_OUT_OF_BOUNDS] I'm approaching OOB!")
							ENDIF
						ENDIF
						RESET_NET_TIMER(timeMyOobCheck)
						START_NET_TIMER(timeMyOobCheck)
					ENDIF
				ELSE
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF NOT Is_This_The_Current_Objective_Text("UW_OOB")
							Print_Objective_Text("UW_OOB")
						ENDIF
						IF NOT HAS_NET_TIMER_STARTED(timeExpOob)
							iOOBSound = GET_SOUND_ID()
							PLAY_SOUND_FROM_ENTITY(iOOBSound, "Explosion_Countdown", NET_TO_VEH(serverBD.niVeh[GET_TEAM_VEHICLE_INDEX()]), "GTAO_FM_Events_Soundset") 
							SET_VARIABLE_ON_SOUND(iOOBSound,  "Time", 30)
							START_NET_TIMER(timeExpOob)
							PRINTLN("     ---------->     KILL LIST - [MAINTAIN_PLAYER_OUT_OF_BOUNDS] Started timeExpOob")
						ENDIF
						
						IF HAS_NET_TIMER_STARTED(timeExpOob)
							IF (GET_VEHICLE_DETONATION_TIME()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(timeExpOob)) >= 0
								SET_MIDDLE_TITLE_POSITION_HUD_THIS_FRAME()
								DRAW_GENERIC_TIMER((GET_VEHICLE_DETONATION_TIME()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(timeExpOob)), "UW_DEST", 0, TIMER_STYLE_USEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_DONTCARE, DEFAULT, HUD_COLOUR_RED, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_RED)
							ELSE
								SET_MIDDLE_TITLE_POSITION_HUD_THIS_FRAME()
								DRAW_GENERIC_TIMER(0, "UW_DEST", 0, TIMER_STYLE_USEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_DONTCARE, DEFAULT, HUD_COLOUR_RED, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_RED)
								Clear_Any_Objective_Text_From_This_Script()
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_IamOOB)
								PRINTLN("     ---------->     KILL LIST - [MAINTAIN_PLAYER_OUT_OF_BOUNDS] set biP_IamOOB")
							ENDIF
						ENDIF	
						
						IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_IamOOB)
							fDist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), serverBD.vStartLocation[0])
							IF fDist < 750.0
								CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ApproachingOOB)
								RESET_NET_TIMER(timeExpOob)
								STOP_SOUND(iOOBSound)
							ENDIF
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_IamOOB)
		SET_PLAYER_DEFAULT_FRIENDLY_FIRE_OPTION()
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())		
			iVeh = GET_TEAM_VEHICLE_INDEX()
			IF iVeh > -1
				IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[iVeh])
				
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niVeh[iVeh])
						SET_ENTITY_INVINCIBLE(NET_TO_VEH(serverBD.niVeh[iVeh]), FALSE)
						SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(NET_TO_VEH(serverBD.niVeh[iVeh]), TRUE)
						NETWORK_EXPLODE_VEHICLE(NET_TO_VEH(serverBD.niVeh[iVeh]))
						CLEANUP_NET_ID(serverBD.niVeh[iVeh])
						PRINTLN("     ---------->     KILL LIST - [MAINTAIN_PLAYER_OUT_OF_BOUNDS] Exploded vehicle as biP_IamOOB! VEH ", iVeh)
					ENDIF
				ENDIF
			ELSE
				PRINTLN("     ---------->     KILL LIST - [MAINTAIN_PLAYER_OUT_OF_BOUNDS] WANT TO EXPLODE VEH BUT iVeh = ", iVeh)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

INT iStaggeredVehicle

PROC MAINTAIN_PLAYER_VEHICLES_FALLING_THROUGH_WORLD()
	VECTOR vCoord
	IF iStaggeredVehicle < MAX_PLAYER_VEH
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[iStaggeredVehicle])
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niVeh[iStaggeredVehicle])
				vCoord = GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niVeh[iStaggeredVehicle]), FALSE)
				IF vCoord.z < -25.0
					CPRINTLN(DEBUG_NET_AMBIENT, "     ---------->     KILL LIST [MAINTAIN_PLAYER_VEHICLES_FALLING_THROUGH_WORLD] THIS PLAYER VEH FELL THROUGH WORLD ", iStaggeredVehicle, " WILL CLEAN UP COORDS ", vCoord)
					CLEANUP_NET_ID(serverBD.niVeh[iStaggeredVehicle])
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	iStaggeredVehicle++ 
	IF iStaggeredVehicle >= MAX_PLAYER_VEH
		iStaggeredVehicle = 0
	ENDIF
ENDPROC

PROC MAINTAIN_ENOUGH_PLAYERS_IN_MY_TEAM_VEHICLE()
	PED_INDEX pedTemp
	INT iVEh
	INT i
	INT iPassCount
	IF serverBD.eStage = eAD_ATTACK
		IF playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet <> 0
			IF playerBD[PARTICIPANT_ID_TO_INT()].iVehTooFewPlayers = -1
				IF NOT HAS_NET_TIMER_STARTED(timeNoDriver)
				OR (HAS_NET_TIMER_STARTED(timeNoDriver) AND HAS_NET_TIMER_EXPIRED(timeNoDriver, 5000))
					IF serverBD.vehModel = SAVAGE
					OR serverBD.vehModel = BUZZARD
						iVEh = iMyUwPlayerVeh //GET_TEAM_VEHICLE_INDEX()
						IF iVeh > -1
							IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[iVeh])
								pedTemp = GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(serverBD.niVeh[iVeh]))			
								IF pedTemp = NULL
									playerBD[PARTICIPANT_ID_TO_INT()].iVehTooFewPlayers = iVeh
									PRINTLN("     ---------->     KILL LIST - [MAINTAIN_ENOUGH_PLAYERS_IN_MY_TEAM_VEHICLE] VEH DRIVER IS NULL! Setting iVehTooFewPlayers = ", iVeh)
								ELIF IS_PED_INJURED(pedTemp)
									playerBD[PARTICIPANT_ID_TO_INT()].iVehTooFewPlayers = iVeh
									PRINTLN("     ---------->     KILL LIST - [MAINTAIN_ENOUGH_PLAYERS_IN_MY_TEAM_VEHICLE] VEH DRIVER IS INJURED! Setting iVehTooFewPlayers = ", iVeh)
								ELIF NOT IS_PED_A_PLAYER(pedTemp) 
									playerBD[PARTICIPANT_ID_TO_INT()].iVehTooFewPlayers = iVeh
									PRINTLN("     ---------->     KILL LIST - [MAINTAIN_ENOUGH_PLAYERS_IN_MY_TEAM_VEHICLE] VEH DRIVER IS NOT A PLAYER! Setting iVehTooFewPlayers = ", iVeh)
								ENDIF
							ENDIF
						ENDIF
					ELIF serverBD.vehModel = VALKYRIE
						iVEh = iMyUwPlayerVeh //GET_TEAM_VEHICLE_INDEX()
						IF iVeh > -1
							IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[iVeh])
								pedTemp = GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(serverBD.niVeh[iVeh]))			
								IF pedTemp = NULL
									playerBD[PARTICIPANT_ID_TO_INT()].iVehTooFewPlayers = iVeh
									PRINTLN("     ---------->     KILL LIST - [MAINTAIN_ENOUGH_PLAYERS_IN_MY_TEAM_VEHICLE] VALKYRIE VEH DRIVER IS NULL! Setting iVehTooFewPlayers = ", iVeh)
								ELIF IS_PED_INJURED(pedTemp)
									playerBD[PARTICIPANT_ID_TO_INT()].iVehTooFewPlayers = iVeh
									PRINTLN("     ---------->     KILL LIST - [MAINTAIN_ENOUGH_PLAYERS_IN_MY_TEAM_VEHICLE] VALKYRIE  VEH DRIVER IS INJURED! Setting iVehTooFewPlayers = ", iVeh)
								ELIF NOT IS_PED_A_PLAYER(pedTemp) 
									playerBD[PARTICIPANT_ID_TO_INT()].iVehTooFewPlayers = iVeh
									PRINTLN("     ---------->     KILL LIST - [MAINTAIN_ENOUGH_PLAYERS_IN_MY_TEAM_VEHICLE] VALKYRIEV EH DRIVER IS NOT A PLAYER! Setting iVehTooFewPlayers = ", iVeh)
								ENDIF
								
								IF playerBD[PARTICIPANT_ID_TO_INT()].iVehTooFewPlayers = -1
									//-- Check for at least two players in veh
									INT iNumSeats = GET_VEHICLE_MODEL_NUMBER_OF_SEATS(VALKYRIE)
									
									
									//--Count passengers
									REPEAT iNumSeats i
										pedTemp = GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(serverBD.niVeh[iVeh]), INT_TO_ENUM(VEHICLE_SEAT, i))
										
										IF pedTemp <> NULL
											IF NOT IS_PED_INJURED(pedTemp)
												IF IS_PED_A_PLAYER(pedTemp)
													iPassCount++
												ENDIF
											ENDIF
										ENDIF
									ENDREPEAT
									
									IF iPassCount = 0
										playerBD[PARTICIPANT_ID_TO_INT()].iVehTooFewPlayers = iVeh
										PRINTLN("     ---------->     KILL LIST - [MAINTAIN_ENOUGH_PLAYERS_IN_MY_TEAM_VEHICLE] VALKYRIE  NO PASSENGERS! Setting iVehTooFewPlayers = ", iVeh)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					RESET_NET_TIMER(timeNoDriver)
					START_NET_TIMER(timeNoDriver)
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
	
	i = 0
	REPEAT MAX_PLAYER_VEH i
		IF IS_BIT_SET(serverBD.iVehExplodeTooFewPlayers, i)
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niVeh[i])
				IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh[i]))
					IF (GET_VEHICLE_DETONATION_TIME()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeExplodeTooFewPlayers[i])) >= 0
						IF playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet <> 0
						//	IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh[i]), <<VEH_EXP_RANGE, VEH_EXP_RANGE, VEH_EXP_RANGE>>) 
							IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh[i]))
								IF NOT SHOULD_UI_BE_HIDDEN()
									SET_MIDDLE_TITLE_POSITION_HUD_THIS_FRAME()
									DRAW_GENERIC_TIMER((GET_VEHICLE_DETONATION_TIME()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeExplodeTooFewPlayers[i])), "UW_DEST", 0, TIMER_STYLE_USEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_DONTCARE, DEFAULT, HUD_COLOUR_RED, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_RED)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet <> 0
						//	IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh[i]), <<VEH_EXP_RANGE, VEH_EXP_RANGE, VEH_EXP_RANGE>>)
							IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh[i]))
								IF NOT SHOULD_UI_BE_HIDDEN()
									SET_MIDDLE_TITLE_POSITION_HUD_THIS_FRAME()
									DRAW_GENERIC_TIMER(0, "UW_DEST", 0, TIMER_STYLE_USEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_DONTCARE, DEFAULT, HUD_COLOUR_RED, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_RED)
								ENDIF
							ENDIF
						ENDIF
						
						
						
						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niVeh[i])
							SET_ENTITY_INVINCIBLE(NET_TO_VEH(serverBD.niVeh[i]), FALSE)
							SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(NET_TO_VEH(serverBD.niVeh[i]), TRUE)
							
							IF NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(NET_TO_VEH(serverBD.niVeh[i])))
								NETWORK_EXPLODE_VEHICLE(NET_TO_VEH(serverBD.niVeh[i]))
								CLEANUP_NET_ID(serverBD.niVeh[i])
								PRINTLN("     ---------->     KILL LIST - EXPLODE THE VEHICLE - NETWORK_EXPLODE_VEHICLE DONE")
							ELSE
								NETWORK_EXPLODE_HELI(NET_TO_VEH(serverBD.niVeh[i]))
								CLEANUP_NET_ID(serverBD.niVeh[i])
								PRINTLN("     ---------->     KILL LIST - EXPLODE THE VEHICLE - NETWORK_EXPLODE_HELI DONE")
							ENDIF
							
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			
			
			
			
			IF playerBD[PARTICIPANT_ID_TO_INT()].iVehTooFewPlayers = i
				//-- Too Few players help text
				IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_TooFewPlayersInVeh)
					IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
						IF IS_COMPETITIVE_UW()
							IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_ValkStartNoGunner)
								PRINT_HELP_NO_SOUND("UW_NOPILC", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) /// There aren't enough players left in your vehicle to continue with the Competitive Urban Warfare event.
							ELSE	
								PRINT_HELP_NO_SOUND("UW_NOGUN", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) // You need a gunner to take part in Kill List Competitive in a Valkyrie.
							ENDIF
						ELSE
							PRINT_HELP_NO_SOUND("UW_NOPIL", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) // 
							
						ENDIF
						SET_FREEMODE_EVENT_HELP_BACKGROUND()
						SET_BIT(iBoolsBitSet2, bi2_TooFewPlayersInVeh)
					ELSE
						IF IS_BIT_SET(iBoolsBitSet2, bi2_ValkStartNoGunner)
							IF IS_HELP_MESSAGE_BEING_DISPLAYED()
								CLEAR_HELP()
							ENDIF
						ENDIF
						PRINTLN("     ---------->     KILL LIST - IS_OK_TO_PRINT_FREEMODE_HELP NOT OK bi2_TooFewPlayersInVeh ")
					ENDIF
				ENDIF
				
				//-- Exit vehicle objective text
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					INT iVehToCheck
					iVehToCheck = playerBD[PARTICIPANT_ID_TO_INT()].iVehTooFewPlayers
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[iVehToCheck])
				//	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh[iVehToCheck]))
							IF NOT IS_COMPETITIVE_UW()
								IF NOT Is_This_The_Current_Objective_Text("UW_EXPL")
									Print_Objective_Text("UW_EXPL")
								ENDIF
							ELSE
								IF NOT Is_This_The_Current_Objective_Text("UW_EXPLC")
									Print_Objective_Text("UW_EXPLC")
								ENDIF
							ENDIF
						ELSE
							IF Is_This_The_Current_Objective_Text("UW_EXPL")
							OR Is_This_The_Current_Objective_Text("UW_EXPLC")
								Clear_Any_Objective_Text_From_This_Script()
							ENDIF
						ENDIF
					ELSE
						IF Is_This_The_Current_Objective_Text("UW_EXPL")
						OR Is_This_The_Current_Objective_Text("UW_EXPLC")
							Clear_Any_Objective_Text_From_This_Script()
						ENDIF
					ENDIF
				ENDIF
			
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC
//Cleanup Mission Data
PROC SCRIPT_CLEANUP()
	INT iTemp
	REMOVE_VEHICLE_BLIP()
		
	IF GET_SERVER_MISSION_STATE() = GAME_STATE_END
	AND serverBD.VehModel != DUMMY_MODEL_FOR_SCRIPT
		SET_VEHICLE_MODEL_IS_SUPPRESSED(serverBD.VehModel, FALSE)
		NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  SET_VEHICLE_MODEL_IS_SUPPRESSED - FALSE      <----------     ") NET_NL()
	ENDIF
	
	CLEAR_BIT(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_PLAYER_USING_DESTROY_VEH)
	
	SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_KILL_LIST, FALSE)
	SET_FREEMODE_FLOW_COMMS_DISABLED(FALSE)
	IF PARTICIPANT_ID_TO_INT() != -1
		COMMON_FREEMODE_AMBIENT_EVENTS_CLEAN_UP(FMMC_TYPE_KILL_LIST, DEFAULT, (IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneReward)))
	ENDIF
	
	CALL_FREEMODE_AMBIENT_EVENTS_TELEMETRY(FMMC_TYPE_KILL_LIST)
	
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PreventAutoShuffleToDriversSeat, FALSE)
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_CanActivateRagdollWhenVehicleUpsideDown, TRUE)
	
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	IF iMyMaxWantedLevel > 0
		SET_MAX_WANTED_LEVEL(iMyMaxWantedLevel)
	ENDIF
	IF NATIVE_TO_INT(PLAYER_ID()) != -1
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_KILL_LIST
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = -1
		ENDIF
	ENDIF
	IF GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()) = relUWPlayer
		SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), relMyFmGroup)
		NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  CLEANUP MISSION - RESTORED MY FM REL GROUP     <----------     ") NET_NL()
	ENDIF
	REMOVE_UW_REL_GROUPS()
	
	FORCE_DRAW_CHALLENGE_DPAD_LBD(FALSE, FMMC_TYPE_KILL_LIST)
	
	//FM_EVENT_SET_PLAYER_IS_CRITICAL_TO_FM_EVENT(FALSE)
	
	IF IS_BIT_SET(iBoolsBitSet, biDisabledCorona)
		Clear_MissionsAtCoords_Local_Disabled_Area()
		CLEAR_BIT(iBoolsBitSet, biDisabledCorona)
		PRINTLN("     ---------->     KILL LIST - CLEAR biDisabledCorona - 2")
	ENDIF
	IF IS_BIT_SET(iBoolsBitSet, biDoneMusic)
		SET_AUDIO_FLAG("DisableFlightMusic", FALSE) 
		NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  CLEANUP MISSION - DisableFlightMusic - FALSE     <----------     ") NET_NL()	
		
		IF NOT IS_BIT_SET(iBoolsBitSet, biResetMusic)
			TRIGGER_MUSIC_EVENT("KILL_LIST_STOP_MUSIC")
			
			NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  CLEANUP MISSION - KILL_LIST_STOP_MUSIC     <----------     ") NET_NL()	
		ENDIF
	ENDIF
	
	IF fMyVoiceChatProx <> -100.0 
		SET_UW_VOICE_CHAT_OVERRIDE(FALSE, 0)
		NETWORK_SET_TALKER_PROXIMITY(fMyVoiceChatProx)
		PRINTLN("     ---------->     KILL LIST - CLEANUP MISSION RESET NETWORK_SET_TALKER_PROXIMITY TO ", fMyVoiceChatProx)
	ENDIF
	
	FM_EVENT_SET_VOICE_CHAT_DISABLED_FOR_PLAYER_FOR_KILL_LIST(FALSE)
	
	IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		telemetryStruct.m_endReason             =AE_END_FORCED
	ELIF SHOULD_BAIL_FM_EVENT_DUE_TO_LOW_SESSION_NUMBERS(DEFAULT, DEFAULT, FALSE)
		telemetryStruct.m_endReason             =AE_END_LOW_NUMBERS
	ELSE
		IF telemetryStruct.m_endReason       	!=AE_END_WON
			telemetryStruct.m_endReason         =AE_END_LOST
		ENDIF
	ENDIF
	
	STOP_SOUND(iEventDurationExpireSound)
	
	telemetryStruct.m_uid0 = serverBD.iHashedMac
	telemetryStruct.m_uid1 = serverBD.iMatchHistoryId
	telemetryStruct.m_playersLeftInProgress = serverBD.iPlayersLeftInProgress
	telemetryStruct.m_playersParticipating = serverBD.iPeakQualifiers
	IF telemetryStruct.m_startTime > 0
		telemetryStruct.m_timeTakenToComplete = GET_CLOUD_TIME_AS_INT() - telemetryStruct.m_startTime
	ENDIF
	
	IF (!g_sMPTunables.bkill_list_disable_share_cash AND !IS_BIT_SET(serverBD.iServerBitSet, biS_Competitive))
	OR (!g_sMPTunables.bkill_list_competitive_disable_share_cash AND IS_BIT_SET(serverBD.iServerBitSet, biS_Competitive))
		IF telemetryStruct.m_cashEarned > 0
	//		SET_LAST_JOB_DATA(LAST_JOB_URBAN_WARFARE, telemetryStruct.m_cashEarned)
	//		IF telemetryStruct.m_endReason             =AE_END_WON
	//			SET_DO_FM_EVENT_SHARE_CASH_HELP_IF_NEEDED()
	//		ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_Competitive)
		iTemp = 1
	ENDIF
	
	IF IS_BIT_SET(iBoolsBitSet2, bi2_SetFriendlyFire)
		SET_PLAYER_DEFAULT_FRIENDLY_FIRE_OPTION()
		SET_LOCAL_PLAYER_AS_GHOST(FALSE)
	ENDIF
	
	IF iTemp = 0
		PROCESS_CURRENT_AMBIENT_EVENT_PLAYSTATS(telemetryStruct, ENUM_TO_INT(serverBD.VehModel), serverbd.iCurrentWave, iTemp)
	ELIF PARTICIPANT_ID_TO_INT() != -1
		PROCESS_CURRENT_AMBIENT_EVENT_PLAYSTATS(telemetryStruct, serverbd.iCurrentWave, playerBD[PARTICIPANT_ID_TO_INT()].iMyKills, iTemp)
	ENDIF
	
	IF FM_EVENT_IS_PLAYER_WAITING_FOR_FM_EVENT_TO_START(PLAYER_ID())
		FM_EVENT_SET_PLAYER_WAITING_FOR_FM_EVENT_TO_START(FALSE)
	ENDIF
	
	SET_LOCAL_PLAYER_DOING_KILL_LIST(FALSE)
	
	// 2420366: Reset disconnected player peds flying off with UW vehicles
	SET_LEFT_PLAYERS_FLY_HELIS_INTO_SUNSET(TRUE)
		
	NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  CLEANUP MISSION      <----------     ") NET_NL()
	#IF IS_DEBUG_BUILD NET_LOG("SCRIPT_CLEANUP")	#ENDIF
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

//FUNC BOOL DOES_VARIATION_HAVE_BOSS_BATTLE()
//	IF serverBD.VehModel = RHINO
//	OR serverBD.VehModel = HYDRA
//		RETURN FALSE
//	ENDIF
//	
//	RETURN TRUE
//ENDFUNC
//
//FUNC BOOL CREATE_BOSS_VEHICLE_AND_PEDS()
//	INT i
//	BOOL bGotSpawnCoords
//	VECTOR vSpawn
//	FLOAT fHeading
//	
//	MODEL_NAMES mBoss = SAVAGE
//	VEHICLE_SEAT vsSeatToUse
//	
//	#IF IS_DEBUG_BUILD
//		TEXT_LABEL_15 tl15Name
//
//	#ENDIF
//	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niBossVeh)
//		IF REQUEST_LOAD_MODEL(mBoss)
//			REPEAT serverBD.iMaxVehicles i
//				IF NOT ARE_VECTORS_EQUAL(serverBD.vMwVehSpawn[i], << 0.0, 0.0, 0.0>>)
//					IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(	serverBD.vMwVehSpawn[i], // VECTOR vPoint, 
//														DEFAULT, 						 // FLOAT fVehRadius = 6.0, 
//														DEFAULT, 						 // FLOAT fPedRadius = 1.0, 
//														DEFAULT,						 // FLOAT fObjRadius = 1.0,
//														DEFAULT,						 // FLOAT fViewRadius = 5.0,
//														DEFAULT,						 // BOOL bCheckThisPlayerSight = TRUE,
//														DEFAULT,						 // BOOL bDoAnyPlayerSeePointCheck = TRUE,
//														DEFAULT,						 // BOOL bCheckVisibilityForOwnTeam = TRUE,
//														fMinVisDist,					 // FLOAT fVisibleDistance = 120.0,
//														DEFAULT, 						 // BOOL bIgnoreLocalPlayerChecks=FALSE, 
//														DEFAULT,						 // INT iTeamToIgnore=-1,
//														DEFAULT,						 // BOOL bDoFireCheck=TRUE,
//														fMinPlayerDist) 				// FLOAT fPlayerRadius = 0.0,
//						
//						vSpawn = serverBD.vMwVehSpawn[i]							  
//						fHeading = serverBD.fMwVehSpawn[i]							  
//						bGotSpawnCoords = TRUE		
//					ENDIF
//				ENDIF
//			ENDREPEAT
//			
//			IF bGotSpawnCoords
//				IF CREATE_NET_VEHICLE(serverBD.niBossVeh, mBoss, vSpawn, fHeading, DEFAULT, DEFAULT, DEFAULT, DEFAULT, FALSE)
//					SET_VEHICLE_DOORS_LOCKED(NET_TO_VEH(serverBD.niBossVeh), VEHICLELOCK_LOCKED)
//					SET_VEHICLE_ENGINE_ON(NET_TO_VEH(serverBD.niBossVeh), TRUE, TRUE)
//					SET_VEHICLE_INFLUENCES_WANTED_LEVEL(NET_TO_VEH(serverBD.niBossVeh), FALSE)
//					SET_HELI_BLADES_FULL_SPEED(NET_TO_VEH(serverBD.niBossVeh))
//					SET_ENTITY_DYNAMIC(NET_TO_VEH(serverBD.niBossVeh), TRUE)
//					ACTIVATE_PHYSICS(NET_TO_VEH(serverBD.niBossVeh))
//					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_VEH(serverBD.niBossVeh), FALSE)
//					
//					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [CREATE_BOSS_VEHICLE_AND_PEDS] CREATED BOSS VEH AT COORDS ", vSpawn, " AND HEADING ", fHeading)
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	i = 0
//	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niBossVeh)
//		REPEAT MAX_BOSS_PEDS i
//			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niBossPeds[i])
//				IF REQUEST_LOAD_MODEL(serverBD.MwPedModel)
//					vsSeatToUse =  INT_TO_ENUM(VEHICLE_SEAT, i-1)
//					IF CREATE_NET_PED_IN_VEHICLE(serverBD.niBossPeds[i], serverBD.niBossVeh, PEDTYPE_CRIMINAL, serverBD.MwPedModel,vsSeatToUse)
//						SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(serverBD.niBossPeds[i]), relUWAi)
//						SET_ENTITY_IS_TARGET_PRIORITY(NET_TO_PED(serverBD.niBossPeds[i]), TRUE)
//						SET_PED_RANDOM_COMPONENT_VARIATION(NET_TO_PED(serverBD.niBossPeds[i]))
//						GIVE_DELAYED_WEAPON_TO_PED(NET_TO_PED(serverBD.niBossPeds[i]), WEAPONTYPE_APPISTOL, 25000, FALSE)
//						SET_PED_ACCURACY(NET_TO_PED(serverBD.niBossPeds[i]), 30)	
//						
//						SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.niBossPeds[i]), CA_DO_DRIVEBYS, TRUE)
//						SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.niBossPeds[i]), CA_LEAVE_VEHICLES, FALSE)
//						SET_PED_COMBAT_ABILITY(NET_TO_PED(serverBD.niBossPeds[i]), CAL_AVERAGE)
//						SET_PED_COMBAT_MOVEMENT(NET_TO_PED(serverBD.niBossPeds[i]), CM_WILLADVANCE)
//						SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.niBossPeds[i]), CA_USE_COVER, TRUE)
//						SET_PED_TARGET_LOSS_RESPONSE(NET_TO_PED(serverBD.niBossPeds[i]), TLR_NEVER_LOSE_TARGET)
//						SET_PED_HIGHLY_PERCEPTIVE(NET_TO_PED(serverBD.niBossPeds[i]), TRUE)
//						
//						SET_ENTITY_HEALTH(NET_TO_PED(serverBD.niBossPeds[i]), ROUND(200*g_sMPTunables.fAiHealthModifier))
//						
//						SET_PED_AS_ENEMY(NET_TO_PED(serverBD.niBossPeds[i]), TRUE)
//						
//						SET_PED_KEEP_TASK(NET_TO_PED(serverBD.niBossPeds[i]), TRUE)
//						
//						ET_PED_CONFIG_FLAG(NET_TO_PED(serverBD.niBossPeds[i]), PCF_DontInfluenceWantedLevel, TRUE)
//						
//						SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.niBossPeds[i]), CA_USE_VEHICLE_ATTACK,TRUE)
//						SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.niBossPeds[i]), CA_USE_VEHICLE_ATTACK_IF_VEHICLE_HAS_MOUNTED_GUNS,TRUE)
//						
//						SET_PED_DIES_IN_WATER(NET_TO_PED(serverBD.niBossPeds[i]), TRUE)
//						SET_PED_DIES_IN_SINKING_VEHICLE(NET_TO_PED(serverBD.niBossPeds[i]), TRUE)
//						
//						IF i = 0
//							SET_PED_SEEING_RANGE(NET_TO_PED(serverBD.niBossPeds[i]), 299)		
//							SET_PED_ACCURACY(NET_TO_PED(serverBD.niBossPeds[i]),3)
//							SET_PED_SHOOT_RATE(NET_TO_PED(serverBD.niBossPeds[i]),50)
//							SET_PED_FIRING_PATTERN(NET_TO_PED(serverBD.niBossPeds[i]), FIRING_PATTERN_BURST_FIRE_HELI)
//						ENDIF
//						
//						SET_PED_DIES_WHEN_INJURED(NET_TO_PED(serverBD.niBossPeds[i]),TRUE)
//						SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(NET_TO_PED(serverBD.niBossPeds[i]), KNOCKOFFVEHICLE_NEVER)
//						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [CREATE_BOSS_VEHICLE_AND_PEDS] CREATED BOSS PED ", i) 
//						
//						#IF IS_DEBUG_BUILD
//							tl15Name = ""
//							tl15Name = "Boss "
//							tl15Name += i
//							SET_PED_NAME_DEBUG(NET_TO_PED(serverBD.niBossPeds[i]), tl15Name)
//						#ENDIF
//							
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDREPEAT
//	ENDIF
//	
//	i = 0
//	REPEAT MAX_BOSS_PEDS i
//		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niBossPeds[i])
//			RETURN FALSE
//		ENDIF
//	ENDREPEAT
//	
//	RETURN TRUE
//ENDFUNC
FUNC INT GET_CURRENT_WAVE_TYPE()
	RETURN (serverbd.iWaveType[serverbd.iCurrentWave])
ENDFUNC


FUNC STRING GET_NAME_OF_VEHICLE_FOR_SCOREBOARD()
	STRING sName
	SWITCH serverBD.VehModel
		CASE SAVAGE 	sName = "SAVAGE"	BREAK
		CASE BUZZARD	sName = "BUZZARD"	BREAK
		CASE VALKYRIE	sName = "VALKYRIE"	BREAK
	ENDSWITCH
	
	RETURN sName
ENDFUNC

//PURPOSE: Gets a random vehicle model to use
//FUNC MODEL_NAMES GET_RANDOM_VEH_MODEL()
//	INT iRand = GET_RANDOM_INT_IN_RANGE(0, 5)	//5)
//	#IF IS_DEBUG_BUILD
//		IF MPGlobalsAmbience.iUrbanWarfareVeh > -1
//		AND MPGlobalsAmbience.iUrbanWarfareVeh < 5
//			iRand = MPGlobalsAmbience.iUrbanWarfareVeh
//			PRINTLN("     ---------->     KILL LIST - DEBUG - MPGlobalsAmbience.iUrbanWarfareVeh = ", MPGlobalsAmbience.iUrbanWarfareVeh)
//		ENDIF
//	#ENDIF
//	
//	//DEBUG TEST
////	RETURN SAVAGE //RHINO //HYDRA
//	
////	iRand = 2
//	
//	SWITCH iRand
//		CASE 0	serverBD.iVehicleSeats = 1	PRINTLN("     ---------->     KILL LIST - VEHICLE = RHINO") 	RETURN RHINO
//		CASE 1	serverBD.iVehicleSeats = 1	PRINTLN("     ---------->     KILL LIST - VEHICLE = HYDRA") 	RETURN HYDRA
//		CASE 2	serverBD.iVehicleSeats = 4	PRINTLN("     ---------->     KILL LIST - VEHICLE = SAVAGE") 	RETURN SAVAGE
//		CASE 3	serverBD.iVehicleSeats = 4	PRINTLN("     ---------->     KILL LIST - VEHICLE = VALKYRIE") 	RETURN VALKYRIE
//		CASE 4	serverBD.iVehicleSeats = 4	PRINTLN("     ---------->     KILL LIST - VEHICLE = BUZZARD") 	RETURN BUZZARD
//	ENDSWITCH
//	
//	PRINTLN("     ---------->     KILL LIST - INVALID OPTION - BACKUP VEHICLE = RHINO") 
//	RETURN RHINO
//ENDFUNC

FUNC BOOL GET_PLAYER_VEHICLE_MODEL(MODEL_NAMES &mToUse)
	INT iRand = GET_RANDOM_INT_IN_RANGE(0, 5)
	
	#IF IS_DEBUG_BUILD
		IF MPGlobalsAmbience.iUrbanWarfareVeh > -1
		AND MPGlobalsAmbience.iUrbanWarfareVeh < 5
			iRand = MPGlobalsAmbience.iUrbanWarfareVeh
			PRINTLN("     ---------->     KILL LIST - DEBUG - MPGlobalsAmbience.iUrbanWarfareVeh = ", MPGlobalsAmbience.iUrbanWarfareVeh)
		ENDIF
	#ENDIF
	
	
	
	SWITCH iRand
		CASE 0	
			IF NOT IS_VEHICLE_VARIATION_DISABLED(RHINO)
				serverBD.iVehicleSeats = 1
				mToUse = RHINO
				PRINTLN("     ---------->     KILL LIST - VEHICLE = RHINO") 
				
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE 1
			IF NOT IS_VEHICLE_VARIATION_DISABLED(HYDRA)
				serverBD.iVehicleSeats = 1
				mToUse = HYDRA
				PRINTLN("     ---------->     KILL LIST - VEHICLE = HYDRA") 
				serverBD.bOpenAirportGates = TRUE
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE 2
			IF NOT IS_VEHICLE_VARIATION_DISABLED(SAVAGE)
				serverBD.iVehicleSeats = 4
				mToUse = SAVAGE
				PRINTLN("     ---------->     KILL LIST - VEHICLE = SAVAGE") 
				serverBD.bOpenAirportGates = TRUE
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE 3
			IF NOT IS_VEHICLE_VARIATION_DISABLED(VALKYRIE)
				serverBD.iVehicleSeats = 4
				mToUse = VALKYRIE
				PRINTLN("     ---------->     KILL LIST - VEHICLE = VALKYRIE") 
				
				RETURN TRUE
			ENDIF
		BREAK
		
		
		CASE 4
			IF NOT IS_VEHICLE_VARIATION_DISABLED(BUZZARD)
				serverBD.iVehicleSeats = 4
				mToUse = BUZZARD
				PRINTLN("     ---------->     KILL LIST - VEHICLE = BUZZARD") 
				
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC
FUNC INT GET_NUMBER_OF_PLAYERS_REQUIRED_IN_UW_VEHICLE()
	RETURN serverBD.iVehicleSeats
ENDFUNC

FUNC BOOL IS_VALID_KILL_LIST_VEHICLE_MODEL(MODEL_NAMES mVeh)
	RETURN (mVeh = INSURGENT
			OR mVeh = LAZER)
			OR mVeh = BUZZARD
			OR mVeh = MESA3
ENDFUNC


FUNC INT GET_KILL_LIST_VEHICLE_CAPACITY(MODEL_NAMES mVeh)
	SWITCH mVeh
		CASE INSURGENT 	RETURN 2	BREAK
		CASE LAZER		RETURN 1	BREAK
		CASE BUZZARD	RETURN 4	BREAK
		CASE MESA3		RETURN 4	BREAK
		CASE SAVAGE		RETURN 4	BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC INT GET_MIN_NUMBER_OF_PLAYERS_REQUIRED_FOR_VALID_UW_VEHICLE(MODEL_NAMES mVeh)
	SWITCH mVeh
		CASE RHINO		RETURN 1 // RHINO
		CASE HYDRA		RETURN 1 // HYDRA
		CASE SAVAGE		RETURN 1 // SAVAGE
		CASE VALKYRIE	RETURN 2 // VALKYRIE
		CASE BUZZARD	RETURN 1 // BUZZARD
	ENDSWITCH
	
	RETURN 99
ENDFUNC

FUNC MODEL_NAMES GET_VEHICLE_MODEL_FOR_WAVE_TYPE(INT iWaveType)
	MODEL_NAMES mWave
//	PRINTLN("     ---------->     KILL LIST - GET_VEHICLE_MODEL_FOR_WAVE_TYPE called with iWaveType = ", iWaveType)
	SWITCH iWaveType
		CASE WAVE_TYPE_AIRCRAFT_ONLY	
			IF serverBD.VehModel = HYDRA
				mWave = LAZER
			ELSE
				mWave = BUZZARD
			ENDIF
		BREAK
		
		CASE WAVE_TYPE_GROUND_PEDS_AND_VEHS
			IF serverBD.VehModel = RHINO
				mWave = INSURGENT
			ELSE
				mWave = MESA3
			ENDIF
		BREAK
		
		CASE WAVE_TYPE_BOSS
			mWave = SAVAGE
		BREAK
		
		DEFAULT 
			IF serverBD.VehModel = RHINO
				mWave = INSURGENT
			ELIF serverBD.VehModel = HYDRA
				mWave =  LAZER
			ELSE
				mWave = BUZZARD
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN mWave
ENDFUNC

FUNC INT GET_KILL_GOAL()
	INT iGoal
	INT i
	
	IF serverBD.iKillGoal <> -1
		RETURN serverBD.iKillGoal
	ENDIF
	
	IF serverbd.iNumOfWaves = 0
		RETURN ( MAX_MW_PEDS_PER_VEH * g_sMPTunables.iUrbanWarfareEnemyVehSpawns ) + g_sMPTunables.iUrbanWarfareEnemyOnFootSpawns
	ENDIF
	
	INT iWaveType 		= GET_CURRENT_WAVE_TYPE()
	MODEL_NAMES mWave 	= GET_VEHICLE_MODEL_FOR_WAVE_TYPE(iWaveType)
	INT iCap			= GET_KILL_LIST_VEHICLE_CAPACITY(mWave)
	
	SWITCH serverBD.VehModel
		CASE RHINO
		CASE SAVAGE
		CASE HYDRA
		CASE VALKYRIE
		CASE BUZZARD
		//	REPEAT MAX_NUM_WAVES i
			REPEAT serverbd.iNumOfWaves i
				iGoal += ((serverbd.iNumVehiclesInWave[i] * iCap) + serverbd.iNumOnFootPedsInWave[i])
			ENDREPEAT
		BREAK
	ENDSWITCH
	
	RETURN iGoal
ENDFUNC


FUNC INT GET_TOTAL_NUMBER_OF_ON_FOOT_PED_SPAWNS_FOR_THIS_VARIATION()
	INT iNumPeds
	INT i
	REPEAT serverbd.iNumOfWaves i
		iNumPeds += serverbd.iNumOnFootPedsInWave[i]
	ENDREPEAT
	
	RETURN iNumPeds
ENDFUNC

FUNC INT GET_MAX_NUMBER_OF_ON_FOOT_SPAWNS_SO_FAR()
	INT iNumPeds
	INT i
	
	IF serverBD.iCurrentWave = 0
		iNumPeds = serverbd.iNumOnFootPedsInWave[0]
	ELSE
		FOR i = 0 TO serverBD.iCurrentWave
			iNumPeds += serverbd.iNumOnFootPedsInWave[i]
		ENDFOR
	ENDIF
	
	RETURN iNumPeds
ENDFUNC
PROC GET_SPAWN_COORDS_FOR_MERRYWEATHER_VEHICLE_TYPE(MODEL_NAMES mEnemy, INT iCandidate, VECTOR &vSpawn, FLOAT &fSpawn)

	
	BOOL bAircraft = (IS_THIS_MODEL_A_PLANE(mEnemy) OR IS_THIS_MODEL_A_HELI(mEnemy))
	
	SWITCH serverBD.VehModel
		CASE RHINO
			IF NOT ARE_VECTORS_EQUAL(serverBD.vMwVehSpawn[iCandidate], << 0.0, 0.0, 0.0>>)
				vSpawn = serverBD.vMwVehSpawn[iCandidate]
				fSpawn = serverBD.fMwVehSpawn[iCandidate]
			ELSE
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	 "    ---------->     KILL LIST [GET_SPAWN_COORDS_FOR_MERRYWEATHER_VEHICLE_TYPE] RHINO - LOCATION IS AT ORIGIN iCandidate = ", iCandidate) 
			ENDIF
		BREAK
		
		CASE HYDRA
			IF NOT ARE_VECTORS_EQUAL(serverBD.vMwVehSpawn[iCandidate], << 0.0, 0.0, 0.0>>)
				vSpawn = serverBD.vMwVehSpawn[iCandidate]
				fSpawn = serverBD.fMwVehSpawn[iCandidate]
			ELSE
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	 "    ---------->     KILL LIST [GET_SPAWN_COORDS_FOR_MERRYWEATHER_VEHICLE_TYPE] HYDRA - LOCATION IS AT ORIGIN iCandidate = ", iCandidate) 
			ENDIF
		BREAK
		
		CASE SAVAGE
			SWITCH serverBD.iLocation
				CASE 0
					IF bAircraft
						SWITCH iCandidate
							CASE 0	vSpawn =<<-1292.1892, -2572.8677, 22.9452>>	fSpawn = 	332.6443		BREAK
							CASE 1	vSpawn =<<-1062.8428, -3502.9131, 23.1484>>	fSpawn = 	326.4740		BREAK
							CASE 2	vSpawn =<<-1842.1886, -3149.9641, 22.9444>>	fSpawn =	177.2589 		BREAK
							CASE 3	vSpawn =<<-1271.3334, -2267.1489, 12.9454>>	fSpawn =	243.2036 		BREAK

						ENDSWITCH
					ELSE
						SWITCH iCandidate
							CASE 0	vSpawn =<<-892.8904, -2935.9421, 12.9444>>		fSpawn = 	50.7848   		BREAK
							CASE 1	vSpawn =<<-1290.7130, -2658.8442, 13.0516>>		fSpawn = 	 152.2952 		BREAK
							CASE 2	vSpawn =<<-1157.6818, -2725.7065, 12.9534>>		fSpawn =	 187.5114  		BREAK
							CASE 3	vSpawn =<<-949.6019, -3072.9910, 12.9444>>		fSpawn =	63.8501   		BREAK

						ENDSWITCH
						
					ENDIF
				BREAK
				
				CASE 1
					IF bAircraft
						SWITCH iCandidate
							CASE 0	vSpawn =<<2884.9856, 4600.0435, 56.9465>>	fSpawn = 	80.1844 		BREAK
							CASE 1	vSpawn =<<2259.9600, 5608.5337, 63.3660>>	fSpawn = 	171.5600		BREAK
							CASE 2	vSpawn =<<1042.1166, 4282.7842, 47.2770>>	fSpawn =	284.7637 		BREAK
							CASE 3	vSpawn =<<2884.9856, 4600.0435, 56.9465>>	fSpawn =	80.1844  		BREAK

						ENDSWITCH
					ELSE
						SWITCH iCandidate
							CASE 0	vSpawn =<<1914.9537, 4635.8574, 38.7005>>	fSpawn = 	344.9064 		BREAK
							CASE 1	vSpawn =<<2439.7029, 4993.8311, 44.9932>>	fSpawn = 	136.3770 		BREAK
							CASE 2	vSpawn =<<1972.4869, 4843.9814, 43.1300>>	fSpawn =	315.4388  		BREAK
							CASE 3	vSpawn =<<1914.9537, 4635.8574, 38.7005>>	fSpawn =	344.9064 		BREAK

						ENDSWITCH
						
					ENDIF
				
				BREAK
				
				CASE 2
					IF bAircraft
						SWITCH iCandidate
							CASE 0	vSpawn = <<-822.0636, 5744.9072, 16.2672>>	fSpawn = 	305.7653		BREAK
							CASE 1	vSpawn = <<22.3280, 6898.0957, 21.2300>> 	fSpawn = 	151.4785		BREAK
							CASE 2	vSpawn = <<-596.7572, 5293.4839, 79.2145>>	fSpawn =	339.4657 		BREAK
							CASE 3	vSpawn = <<-596.7572, 5293.4839, 79.2145>>	fSpawn =	339.4657 		BREAK

						ENDSWITCH
					ELSE
						SWITCH iCandidate
							CASE 0	vSpawn =<<-561.0425, 5638.1782, 38.9797>>	fSpawn = 	 16.7516  		BREAK
							CASE 1	vSpawn =<<-38.6174, 6281.1758, 30.2043>>	fSpawn = 	32.7307   		BREAK
							CASE 2	vSpawn =<<-313.5567, 6436.6514, 11.6953>>	fSpawn =	 129.4952  		BREAK
							CASE 3	vSpawn =<<-561.0425, 5638.1782, 38.9797>>	fSpawn =	 16.7516  		BREAK

						ENDSWITCH
						
					ENDIF
				BREAK
				
				CASE 3
					IF bAircraft
						SWITCH iCandidate
							CASE 0	vSpawn = <<1490.8843, -1955.9698, 79.7973>>		fSpawn = 	 78.8897 		BREAK
							CASE 1	vSpawn = <<594.3433, -1979.4663, 28.9706>>		fSpawn = 	319.8794 		BREAK
							CASE 2	vSpawn = <<347.9640, -1427.1937, 85.1742>>		fSpawn =	230.1411  		BREAK
							CASE 3	vSpawn = <<347.9640, -1427.1937, 85.1742>>		fSpawn =	230.1411  		BREAK

						ENDSWITCH
					ELSE
						SWITCH iCandidate
							CASE 0	vSpawn =<<983.6414, -1848.7975, 30.0987>>	fSpawn = 	178.6463  		BREAK
							CASE 1	vSpawn =<<992.1669, -1506.8453, 30.4275>>	fSpawn = 	90.1264   		BREAK
							CASE 2	vSpawn =<<771.2441, -1620.7864, 29.9058>>	fSpawn =	313.4008   		BREAK
							CASE 3	vSpawn =<<840.4948, -1950.5101, 27.9516>>	fSpawn =	87.2509   		BREAK

						ENDSWITCH
						
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE VALKYRIE
			SWITCH serverBD.iLocation
				CASE 0
					IF bAircraft
						SWITCH iCandidate
							CASE 0	vSpawn =<<-619.1071, -1856.8110, 38.0408>>		fSpawn = 	10.5487 		BREAK
							CASE 1	vSpawn =<<-273.1338, -1627.7793, 40.8488>>		fSpawn = 	77.2743 		BREAK
							CASE 2	vSpawn =<<-1157.9587, -1697.1130, 40.3479>>		fSpawn =	299.9692 		BREAK
							CASE 3	vSpawn =<<-1150.7485, -1708.6348, 40.2485>>		fSpawn =	295.9259 		BREAK
							CASE 4	vSpawn =<<-1001.7541, -731.3828, 85.5368>>		fSpawn =	232.7072 		BREAK

						ENDSWITCH
					ELSE
						SWITCH iCandidate
							CASE 0	vSpawn =<<-648.1588, -1632.1196, 23.9175>>		fSpawn = 	 145.9387		BREAK
							CASE 1	vSpawn =<<-850.9597, -1194.9482, 4.7312>>		fSpawn = 	2.3024   		BREAK
							CASE 2	vSpawn =<<-513.3444, -967.2039, 22.5742>>		fSpawn =	111.3599  		BREAK
							CASE 3	vSpawn =<<-720.7520, -1143.7450, 9.6125>>		fSpawn =	126.5029 		BREAK

						ENDSWITCH
						
					ENDIF
				BREAK
				
				CASE 1
					IF bAircraft
						SWITCH iCandidate
							CASE 0	vSpawn =<<1384.4634, 3175.8621, 49.4674>>		fSpawn = 	107.9207		BREAK
							CASE 1	vSpawn =<<848.3471, 3587.8320, 42.0217>>		fSpawn = 	117.8402 		BREAK
							CASE 2	vSpawn =<<-390.1283, 2557.1567, 99.8288>>		fSpawn =	292.5476 		BREAK
							CASE 3	vSpawn =<<1084.2472, 2367.0271, 53.6989>>		fSpawn =	53.4222  		BREAK


						ENDSWITCH
					ELSE
						SWITCH iCandidate
							CASE 0	vSpawn =<<654.4302, 2752.4014, 40.9669>>		fSpawn = 	192.1139 		BREAK
							CASE 1	vSpawn =<<91.7939, 3432.5725, 38.5904>> 		fSpawn = 	246.3809 		BREAK
							CASE 2	vSpawn =<<-361.9220, 2954.9551, 24.1579>>		fSpawn =	 273.6807 		BREAK
							CASE 3	vSpawn =<<-180.3412, 2508.9280, 71.8451>>		fSpawn =	 7.1243 		BREAK
							CASE 4	vSpawn =<<142.7533, 2271.5667, 93.6005>>		fSpawn =	255.3593		BREAK


						ENDSWITCH
						
					ENDIF
				BREAK
				
				CASE 2
					IF bAircraft
						SWITCH iCandidate
							CASE 0	vSpawn =<<-394.2870, 4340.0435, 65.1832>>			fSpawn = 	263.3693		BREAK
							CASE 1	vSpawn =<<1002.4834, 3175.8672, 48.6918>>			fSpawn = 	0.4970    		BREAK
							CASE 2	vSpawn =<<2325.4722, 3922.2959, 45.2575>>			fSpawn =	57.4066  		BREAK
							CASE 3	vSpawn =<<1962.9498, 4785.8359, 51.8872>>			fSpawn =	134.6506 		BREAK


						ENDSWITCH
					ELSE
						SWITCH iCandidate
							CASE 0	vSpawn =<<1398.0179, 4520.3931, 52.8047>>	fSpawn = 	 105.7989	BREAK
							CASE 1	vSpawn =<<366.9009, 4432.0269, 61.6888>>	fSpawn = 	204.5592  	BREAK
							CASE 2	vSpawn =<<128.2849, 4454.8784, 80.3457>>	fSpawn =	226.3244 	BREAK
							CASE 3	vSpawn =<<-132.4943, 4295.4653, 41.7475>>	fSpawn =	 276.3481	BREAK
							CASE 4	vSpawn =<<1908.4247, 4607.0000, 36.8242>>	fSpawn =	 174.8533	BREAK


						ENDSWITCH
						
					ENDIF
				BREAK
				
				CASE 3
					IF bAircraft
						SWITCH iCandidate
							CASE 0	vSpawn =<<3841.8979, 4234.1172, 15.4291>> 			fSpawn = 	336.9579		BREAK
							CASE 1	vSpawn =<<2836.9578, 5957.4653, 362.3982>>			fSpawn = 	225.8444 		BREAK
							CASE 2	vSpawn =<<2624.2356, 5310.0190, 52.1567>>			fSpawn =	215.6300		BREAK
							CASE 3	vSpawn =<<2780.4465, 4758.9004, 55.0574>>			fSpawn =	326.9340		BREAK
							CASE 4	vSpawn =<<3201.1272, 4657.4092, 190.8286>>			fSpawn =	359.7683		BREAK

						ENDSWITCH
					ELSE
						SWITCH iCandidate
							CASE 0	vSpawn =<<3756.6355, 4434.7852, 11.7586>>	fSpawn = 	138.6266	BREAK
							CASE 1	vSpawn =<<3680.5488, 4492.8032, 23.5887>>	fSpawn = 	113.1337 	BREAK
							CASE 2	vSpawn =<<3337.6394, 5482.4844, 19.0418>>	fSpawn =	157.3183	BREAK
							CASE 3	vSpawn =<<2766.2378, 4974.9858, 32.7262>>	fSpawn =	225.2302	BREAK
							CASE 4	vSpawn =<<2797.0950, 4923.6660, 33.1334>>	fSpawn =	34.0949 	BREAK


						ENDSWITCH
						
					ENDIF
				BREAK
				
				CASE 4
					IF bAircraft
						SWITCH iCandidate
							CASE 0	vSpawn =<<-1988.4795, -219.2565, 42.6251>>			fSpawn = 	325.2252 		BREAK
							CASE 1	vSpawn =<<-1567.1648, -555.6394, 123.4493>>			fSpawn = 	 24.7442   		BREAK
							CASE 2	vSpawn =<<-713.2371, 195.5196, 149.7540>> 			fSpawn =	75.5420 		BREAK
							CASE 3	vSpawn =<<-509.8761, 1187.8242, 333.8082>>			fSpawn =	128.2553  		BREAK
							CASE 4	vSpawn =<<-1265.7891, 1493.7218, 203.9673>>			fSpawn =	 187.8592		BREAK

						ENDSWITCH
					ELSE
						SWITCH iCandidate
							CASE 0	vSpawn = <<-1570.4761, 1366.4535, 127.6559>>		fSpawn = 	 115.4139	BREAK
							CASE 1	vSpawn = <<-1581.1268, 955.6403, 157.1481>>			fSpawn = 	39.5031    	BREAK
							CASE 2	vSpawn = <<-1813.6829, 799.5704, 137.5134>>			fSpawn =	188.8774  	BREAK
							CASE 3	vSpawn = <<-1912.6982, 509.3147, 110.6628>>			fSpawn =	70.3926   	BREAK
							CASE 4	vSpawn = <<-2298.4968, 441.8974, 173.4667>>			fSpawn =	1.8886  	BREAK


						ENDSWITCH
						
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE BUZZARD
			SWITCH serverBD.iLocation
				CASE 0
					IF bAircraft
						SWITCH iCandidate
							CASE 0	vSpawn =<<1351.4414, -641.5107, 83.2342>>	fSpawn = 	186.1368		BREAK
							CASE 1	vSpawn =<<636.2282, -434.6195, 33.6320>>	fSpawn = 	203.6348 		BREAK
							CASE 2	vSpawn =<<352.4392, -590.8399, 83.1657>>	fSpawn =	209.9170  		BREAK
							CASE 3	vSpawn =<<375.2908, -1020.4328, 66.5363>>	fSpawn =	271.2396  		BREAK
							CASE 4	vSpawn =<<345.1868, -1426.6221, 85.1742>>	fSpawn =	284.6382 		BREAK

						ENDSWITCH
					ELSE
						SWITCH iCandidate
							CASE 0	vSpawn =<<1248.4901, -1131.3379, 37.2874>>			fSpawn = 	 153.8083		BREAK
							CASE 1	vSpawn =<<1396.4141, -1518.3896, 56.7703>>			fSpawn = 	 134.0967 		BREAK
							CASE 2	vSpawn =<<972.8618, -1464.8285, 30.3558>>			fSpawn =	0.6090     		BREAK
							CASE 3	vSpawn =<<935.6596, -909.6719, 39.5752>>			fSpawn =	271.1971  		BREAK
							CASE 4	vSpawn =<<1156.6870, -768.8879, 56.4831>>			fSpawn =	275.3533		BREAK
							
						ENDSWITCH
						
					ENDIF
				BREAK
				
				CASE 1
					IF bAircraft
						SWITCH iCandidate
							CASE 0	vSpawn =<<-137.5082, -168.3033, 102.7024>>		fSpawn = 	 160.3978		BREAK
							CASE 1	vSpawn =<<-174.8280, -214.6927, 156.6312>>		fSpawn = 	 258.2675 		BREAK
							CASE 2	vSpawn =<<404.7129, -24.3335, 169.3952>>		fSpawn =	95.0683    		BREAK
							CASE 3	vSpawn =<<168.3323, 668.6285, 215.7082>>		fSpawn =	172.5901   		BREAK
							CASE 4	vSpawn =<<-608.7920, 660.9065, 165.6164>>		fSpawn =	 207.7868		BREAK

						ENDSWITCH
					ELSE
						SWITCH iCandidate
							CASE 0	vSpawn =<<-166.1217, -214.9006, 45.8637>>			fSpawn = 	 257.5103	BREAK
							CASE 1	vSpawn =<<33.7482, -95.8814, 55.3626>>				fSpawn = 	 73.0413 	BREAK
							CASE 2	vSpawn =<<36.5680, 47.4427, 71.3139>>				fSpawn =	158.1279 	BREAK
							CASE 3	vSpawn =<<-501.5253, -67.3569, 38.6901>>			fSpawn =	155.0279 	BREAK
							CASE 4	vSpawn =<<-430.6903, -423.6864, 31.7928>>			fSpawn =	 352.9770	BREAK
							
						ENDSWITCH
						
					ENDIF
				BREAK
				
				CASE 2
					IF bAircraft
						SWITCH iCandidate
							CASE 0	vSpawn = <<-468.3117, 2034.2810, 225.8829>>		fSpawn = 	 183.5655		BREAK
							CASE 1	vSpawn = <<-1572.2275, 838.9957, 193.9097>>		fSpawn = 	 278.9509  		BREAK
							CASE 2	vSpawn = <<-175.6009, -162.3506, 102.7024>>		fSpawn =	12.7537     	BREAK
							CASE 3	vSpawn = <<494.7073, 706.4063, 205.6965>>		fSpawn =	75.8614     	BREAK
							CASE 4	vSpawn = <<821.3636, 1308.9093, 372.5396>>		fSpawn =	113.4670 		BREAK

						ENDSWITCH
					ELSE
						SWITCH iCandidate
							CASE 0	vSpawn =<<-803.5167, 1295.3257, 257.4832>>				fSpawn = 	 209.5666	BREAK
							CASE 1	vSpawn =<<-925.5961, 1124.7699, 218.7065>>				fSpawn = 	 215.2912	BREAK
							CASE 2	vSpawn =<<-404.5761, 833.0574, 224.6128>>				fSpawn =	218.3907 	BREAK
							CASE 3	vSpawn =<<-303.0683, 1010.3021, 232.2905>>				fSpawn =	 78.2628 	BREAK
							CASE 4	vSpawn =<<56.7959, 1080.6508, 220.6167>> 				fSpawn =	18.8188  	BREAK
							
						ENDSWITCH
						
					ENDIF
				BREAK
				
				CASE 3
					IF bAircraft
						SWITCH iCandidate
							CASE 0	vSpawn =<<377.1972, 4405.9380, 71.4024>>		fSpawn = 	157.1671 		BREAK
							CASE 1	vSpawn =<<-210.0587, 4223.3462, 53.7504>>		fSpawn = 	 206.9778  		BREAK
							CASE 2	vSpawn =<<-318.9394, 3790.7725, 77.5459>>		fSpawn =	 258.8020    	BREAK
							CASE 3	vSpawn =<<-236.0281, 3088.2373, 47.2290>>		fSpawn =	 304.6536   	BREAK
							CASE 4	vSpawn =<<312.2873, 2783.2725, 54.9498>>		fSpawn =	10.4182  		BREAK

						ENDSWITCH
					ELSE
						SWITCH iCandidate
							CASE 0	vSpawn =<<1111.4447, 3430.1528, 33.0281>>				fSpawn = 	 351.5004	BREAK
							CASE 1	vSpawn =<<940.4324, 3582.7927, 32.0891>>				fSpawn = 	71.8698  	BREAK
							CASE 2	vSpawn =<<-177.7739, 3695.6689, 43.0132>>				fSpawn =	 195.3546	BREAK
							CASE 3	vSpawn =<<-241.2375, 3905.4534, 37.3939>>				fSpawn =	 207.6580	BREAK
							CASE 4	vSpawn =<<-77.8371, 3185.7061, 37.1225>>				fSpawn =	280.9105 	BREAK
							
						ENDSWITCH
						
					ENDIF
				BREAK
				
				CASE 4
					IF bAircraft
						SWITCH iCandidate
							CASE 0	vSpawn =<<815.4335, 1311.4552, 372.1200>>			fSpawn = 	 258.8483		BREAK
							CASE 1	vSpawn =<<609.2955, 205.9871, 146.2221>>			fSpawn = 	316.1886   		BREAK
							CASE 2	vSpawn =<<1150.0144, 130.2795, 90.7203>>			fSpawn =	340.2898      	BREAK
							CASE 3	vSpawn =<<1876.2203, 285.6447, 171.7790>>			fSpawn =	 18.4441    	BREAK
							CASE 4	vSpawn =<<2105.8916, 1586.2098, 99.9212>>			fSpawn =	 107.1124		BREAK

						ENDSWITCH
					ELSE
						SWITCH iCandidate
							CASE 0	vSpawn =<<1199.8656, 1852.8563, 77.9143>>				fSpawn = 	147.2836 	BREAK
							CASE 1	vSpawn =<<1526.9591, 1717.2666, 108.9733>>				fSpawn = 	 116.5075	BREAK
							CASE 2	vSpawn =<<1962.4127, 1297.0952, 166.6211>>				fSpawn =	 14.5081 	BREAK
							CASE 3	vSpawn =<<958.8857, 1717.1743, 163.6494>>				fSpawn =	 280.2715	BREAK
							CASE 4	vSpawn =<<703.9550, 777.3345, 204.8183>> 				fSpawn =	 308.1731	BREAK
							
						ENDSWITCH
						
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	 "    ---------->     KILL LIST [GET_SPAWN_COORDS_FOR_MERRYWEATHER_VEHICLE_TYPE] mEnemy = ", ENUM_TO_INT(mEnemy), " iCandidate = ", iCandidate, " vSpawn = ", vSpawn ," fSpawn = ", fSpawn)
ENDPROC

FUNC INT GET_NUMBER_OF_UW_PLAYER_VEHICLES()
	
	IF serverBD.iNumPlayerVeh <> 0	
		RETURN serverBD.iNumPlayerVeh
	ENDIF
	
	//- Always 1 vehicle in non-competitive UW
	IF NOT IS_COMPETITIVE_UW()
		serverBD.iNumPlayerVeh = 1
		RETURN serverBD.iNumPlayerVeh
	ENDIF
	
	//-- Debug force 4 vehicles
	#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_uw4Teams")
			serverBD.iNumPlayerVeh = 4
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - GET_NUMBER_OF_UW_PLAYER_VEHICLES SET TO 4 AS RUNNING WITH sc_uw4Teams ")
			RETURN serverBD.iNumPlayerVeh
		ENDIF
	#ENDIF
	
	INT i
	INT iPlayerCount
	PLAYER_INDEX playerTemp
	REPEAT NUM_NETWORK_PLAYERS i 
		playerTemp = INT_TO_PLAYERINDEX(i)
		IF NETWORK_IS_PLAYER_ACTIVE(playerTemp)
			IF NOT IS_PLAYER_SCTV(playerTemp)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - GET_NUMBER_OF_UW_PLAYER_VEHICLES Think this player is valid for event ", GET_PLAYER_NAME(playerTemp))
				iPlayerCount++
			ENDIF
		ENDIF
	ENDREPEAT		
	
	IF iPlayerCount < GET_NUMBER_OF_PARTICIPANTS_FOR_2_PLAYER_VEHICLES()
		serverBD.iNumPlayerVeh = 2
	ELIF iPlayerCount < GET_NUMBER_OF_PARTICIPANTS_FOR_3_PLAYER_VEHICLES() //25
		serverBD.iNumPlayerVeh = 3
	ELSE
		serverBD.iNumPlayerVeh = 4
	ENDIF
	
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - GET_NUMBER_OF_UW_PLAYER_VEHICLES serverBD.iNumPlayerVeh = ", serverBD.iNumPlayerVeh)
	
	RETURN serverBD.iNumPlayerVeh
	
ENDFUNC

FUNC BOOL GET_SPAWN_COORDS_FOR_IN_FOOT_MERRYWEATHER_PED(INT& index)
	INT iRandom = GET_RANDOM_INT_IN_RANGE(0, 10)
	IF NOT ARE_VECTORS_EQUAL(serverBD.vMwPedSpawn[iRandom], <<0.0, 0.0, 0.0>>)
		IF NOT HAS_NET_TIMER_STARTED(timeLastUseSpawnCoords[iRandom])
		OR (HAS_NET_TIMER_STARTED(timeLastUseSpawnCoords[iRandom]) AND HAS_NET_TIMER_EXPIRED(timeLastUseSpawnCoords[iRandom], 10000))
			index = iRandom
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	 "    ---------->     KILL LIST [GET_SPAWN_COORDS_FOR_IN_FOOT_MERRYWEATHER_PED] Candidate = ", iRandom, " vSpawn = ", serverBD.vMwPedSpawn[iRandom], " fSpawn = ", serverBD.fMwPedSpawn[iRandom])
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC INT GET_NUMBER_OF_LOCATIONS_FOR_VEHICLE_TYPE(MODEL_NAMES mVeh)
	IF NOT IS_COMPETITIVE_UW()
		SWITCH mVeh
			CASE RHINO		RETURN 4 // RHINO
			CASE HYDRA		RETURN 5 // HYDRA
			CASE SAVAGE		RETURN 3 // SAVAGE
			CASE VALKYRIE	RETURN 5 // VALKYRIE
			CASE BUZZARD	RETURN 5 // BUZZARD
		ENDSWITCH
	ELSE
		SWITCH mVeh
			CASE RHINO		RETURN 4 // RHINO
			CASE HYDRA		RETURN 3 // HYDRA
			CASE SAVAGE		RETURN 3 // SAVAGE
			CASE VALKYRIE	RETURN 4 // VALKYRIE
			CASE BUZZARD	RETURN 4 // BUZZARD
		ENDSWITCH
	ENDIF
	RETURN 5

ENDFUNC
//PURPOSE: Get Random Location for Veh to be Parked at
PROC GET_RANDOM_LOCATION()
	/*
		Locations taken from
			//depot/gta5/docs/Design/Multiplayer/Freemode Events/Urban Warfare - Master.docx
	*/
	
	IF serverBD.iLocation = -1
		INT iMaxRand = GET_NUMBER_OF_LOCATIONS_FOR_VEHICLE_TYPE(serverBD.VehModel )
		serverBD.iLocation = GET_RANDOM_INT_IN_RANGE(0, iMaxRand)
		
		IF IS_COMPETITIVE_UW()
			IF serverBD.VehModel = HYDRA  // Only use locatuion 0, 1, 4 for cometitive variation for Hydra
				IF serverBD.iLocation = 2
					serverBD.iLocation = 4
				ENDIF
			ELIF serverBD.VehModel = SAVAGE  // 1, 2, 3 valid
				IF serverBD.iLocation = 0
					serverBD.iLocation = 1
				ELIF serverBD.iLocation = 1
					serverBD.iLocation = 2
				ELIF serverBD.iLocation = 2
					serverBD.iLocation = 3
				ENDIF
			ELIF serverBD.VehModel = VALKYRIE // 0, 1, 3, 4 
				IF serverBD.iLocation = 2
					serverBD.iLocation = 3
				ELIF serverBD.iLocation = 3
					serverBD.iLocation = 4
				ENDIF
			ELIF serverBD.VehModel = BUZZARD // 1, 2, 3, 4
				IF serverBD.iLocation = 0
					serverBD.iLocation = 1
				ELIF serverBD.iLocation = 1
					serverBD.iLocation = 2
				ELIF serverBD.iLocation = 2
					serverBD.iLocation = 3
				ELIF serverBD.iLocation = 3
					serverBD.iLocation = 4
				ENDIF
			ENDIF
		ELSE
			IF serverBD.VehModel = SAVAGE  // 1, 2, 3 valid
				IF serverBD.iLocation = 0
					serverBD.iLocation = 1
				ELIF serverBD.iLocation = 1
					serverBD.iLocation = 2
				ELIF serverBD.iLocation = 2
					serverBD.iLocation = 3
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	
	#IF IS_DEBUG_BUILD
		IF MPGlobalsAmbience.iUrbanWarfareLocation > -1
	//	AND MPGlobalsAmbience.iUrbanWarfareLocation < GET_NUMBER_OF_LOCATIONS_FOR_VEHICLE_TYPE(serverBD.VehModel )
			serverBD.iLocation = MPGlobalsAmbience.iUrbanWarfareLocation
			PRINTLN("     ---------->     KILL LIST - MPGlobalsAmbience.iUrbanWarfareLocation = ", MPGlobalsAmbience.iUrbanWarfareLocation)
		ENDIF
	#ENDIF
			
	SWITCH serverBD.VehModel
		CASE RHINO
			serverbd.iWaveType[0] = WAVE_TYPE_GROUND_PEDS_AND_VEHS
			serverbd.iNumVehiclesInWave[0] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS) //3
			serverbd.iNumOnFootPedsInWave[0] = GET_NUMBER_OF_GROUND_PEDS_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS) //MAX_MW_ON_FOOT_PEDS
			
			serverbd.iWaveType[1] = WAVE_TYPE_GROUND_PEDS_AND_VEHS
			serverbd.iNumVehiclesInWave[1] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS) //3
			serverbd.iNumOnFootPedsInWave[1] = GET_NUMBER_OF_GROUND_PEDS_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS) //MAX_MW_ON_FOOT_PEDS
			
			serverbd.iWaveType[2] = WAVE_TYPE_GROUND_PEDS_AND_VEHS
			serverbd.iNumVehiclesInWave[2] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS) //3
			serverbd.iNumOnFootPedsInWave[2] = GET_NUMBER_OF_GROUND_PEDS_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS) //MAX_MW_ON_FOOT_PEDS
				
			IF serverBD.iNumPlayerVeh <= 2
			OR NOT IS_COMPETITIVE_UW()
				serverbd.iNumOfWaves = 3
				
				
			ELIF serverBD.iNumPlayerVeh = 3
				serverbd.iWaveType[3] = WAVE_TYPE_GROUND_PEDS_AND_VEHS
				serverbd.iNumVehiclesInWave[3] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS) //3
				serverbd.iNumOnFootPedsInWave[3] = GET_NUMBER_OF_GROUND_PEDS_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS) //MAX_MW_ON_FOOT_PEDS
				
				serverbd.iNumOfWaves = 4
			ELSE
				serverbd.iWaveType[3] = WAVE_TYPE_GROUND_PEDS_AND_VEHS
				serverbd.iNumVehiclesInWave[3] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS) //3
				serverbd.iNumOnFootPedsInWave[3] = GET_NUMBER_OF_GROUND_PEDS_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS) //MAX_MW_ON_FOOT_PEDS
				
				serverbd.iWaveType[4] = WAVE_TYPE_GROUND_PEDS_AND_VEHS
				serverbd.iNumVehiclesInWave[4] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS) //3
				serverbd.iNumOnFootPedsInWave[4] = GET_NUMBER_OF_GROUND_PEDS_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS)
				serverbd.iNumOfWaves = 5
			ENDIF
			

			
			serverBD.iKillGoal = GET_KILL_GOAL() //( MAX_MW_PEDS_PER_VEH * g_sMPTunables.iUrbanWarfareEnemyVehSpawns ) + g_sMPTunables.iUrbanWarfareEnemyOnFootSpawns
			IF NOT IS_COMPETITIVE_UW()
				serverbd.iHistorySubType = 0
			ELSE
				serverbd.iHistorySubType = 5
			ENDIF
			serverbd.iHistoryVariation = serverBD.iLocation
			
			SWITCH serverBD.iLocation
				CASE 0	//Redwood Lights Track
					serverBD.vStartLocation[0] = <<1016.7343, 2360.8303, 50.4559>> 	serverBD.fStartHeading[0] = 269.4140
					serverBD.vStartLocation[1] = <<1002.5259, 2390.6399, 50.5438>> 	serverBD.fStartHeading[1] =  3.5491 
					serverBD.vStartLocation[2] = <<986.0139, 2301.5920, 48.8235>>  	serverBD.fStartHeading[2] = 54.4509 
					serverBD.vStartLocation[3] = <<977.1004, 2371.7144, 51.0115>>  	serverBD.fStartHeading[3] = 178.1948
					
					
					
					serverBD.vMwVehSpawn[0] = <<1028.3246, 2436.4551, 44.2365>>		serverBD.fMwVehSpawn[0] = 270.8322
					serverBD.vMwVehSpawn[1] = <<1166.4983, 2403.0205, 56.6620>>		serverBD.fMwVehSpawn[1] = 177.3506
					serverBD.vMwVehSpawn[2] = <<892.8997, 2465.8809, 49.7859>>		serverBD.fMwVehSpawn[2] = 318.1089
					serverBD.vMwVehSpawn[3] = <<1078.9882, 2246.8794, 43.7942>>		serverBD.fMwVehSpawn[3] = 272.0801
					serverBD.vMwVehSpawn[4] = <<1016.7836, 2259.4470, 43.8512>>		serverBD.fMwVehSpawn[4] = 104.0591
					
					serverBD.vMwPedSpawn[0] = <<1042.5040, 2280.4973, 48.6315>>	serverBD.fMwPedSpawn[0] = 296.4225
					serverBD.vMwPedSpawn[1] = <<991.9136, 2278.3528, 48.1838>>	serverBD.fMwPedSpawn[1] = 164.9695
					serverBD.vMwPedSpawn[2] = <<1029.7627, 2447.0415, 44.5979>>	serverBD.fMwPedSpawn[2] = 136.0596
					serverBD.vMwPedSpawn[3] = <<971.0186, 2422.7649, 50.6913>>	serverBD.fMwPedSpawn[3] = 116.3790
					serverBD.vMwPedSpawn[4] = <<1017.2642, 2432.0085, 45.2054>>	serverBD.fMwPedSpawn[4] = 86.6586	 
					serverBD.vMwPedSpawn[5] = <<1072.7511, 2237.5986, 43.3802>>	serverBD.fMwPedSpawn[5] = 74.6822	
					serverBD.vMwPedSpawn[6] = <<944.4364, 2407.3918, 50.0925>>	serverBD.fMwPedSpawn[6] = 206.8863 
					serverBD.vMwPedSpawn[7] = <<1059.8036, 2280.7842, 48.5613>>	serverBD.fMwPedSpawn[7] = 267.8581
					serverBD.vMwPedSpawn[8] = <<1004.2717, 2372.3445, 54.5719>>	serverBD.fMwPedSpawn[8] = 267.5856
					serverBD.vMwPedSpawn[9] = <<972.3055, 2235.8213, 53.1094>>	serverBD.fMwPedSpawn[9] =  325.0964
					
					
				BREAK
				CASE 1	//Sandy Shores Airfield
					serverBD.vStartLocation[0] = <<1733.1344, 3302.5454, 40.2235>> 	serverBD.fStartHeading[0] = 194.8062
					serverBD.vStartLocation[1] = <<1761.2813, 3309.8794, 40.1489>> 	serverBD.fStartHeading[1] = 238.9372
					serverBD.vStartLocation[2] = <<1755.7526, 3236.4895, 41.0341>> 	serverBD.fStartHeading[2] = 16.3071 
					serverBD.vStartLocation[3] = <<1676.7595, 3279.7344, 39.8901>> 	serverBD.fStartHeading[3] = 209.9883
					
					serverBD.vMwPedSpawn[0] = <<1758.4493, 3309.2473, 40.1417>>	serverBD.fMwPedSpawn[0] = 221.3913
					serverBD.vMwPedSpawn[1] = <<1746.4453, 3323.7502, 40.1528>>	serverBD.fMwPedSpawn[1] = 246.3415
					serverBD.vMwPedSpawn[2] = <<1775.0573, 3332.9429, 40.3398>>	serverBD.fMwPedSpawn[2] = 213.4087
					serverBD.vMwPedSpawn[3] = <<1713.2401, 3302.4575, 40.1851>>	serverBD.fMwPedSpawn[3] = 100.5405
					serverBD.vMwPedSpawn[4] = <<1693.8173, 3293.1633, 40.1465>>	serverBD.fMwPedSpawn[4] = 289.6017 
					serverBD.vMwPedSpawn[5] = <<1697.4490, 3176.3118, 44.0261>>	serverBD.fMwPedSpawn[5] = 285.6456
					serverBD.vMwPedSpawn[6] = <<1710.5647, 3180.9087, 43.2884>>	serverBD.fMwPedSpawn[6] = 310.9245
					serverBD.vMwPedSpawn[7] = <<1627.4359, 3198.5100, 39.9059>>	serverBD.fMwPedSpawn[7] = 46.7117
					serverBD.vMwPedSpawn[8] = <<1788.4015, 3208.9639, 43.0201>>	serverBD.fMwPedSpawn[8] = 35.9192
					serverBD.vMwPedSpawn[9] = <<1698.3079, 3289.2542, 47.8594>>	serverBD.fMwPedSpawn[9] = 207.5300
  					


					serverBD.vMwVehSpawn[0] = <<1669.9315, 3132.5491, 42.2874>>		serverBD.fMwVehSpawn[0] = 286.6127
					serverBD.vMwVehSpawn[1] = <<1943.3209, 3263.1458, 44.7265>>		serverBD.fMwVehSpawn[1] = 124.1428
					serverBD.vMwVehSpawn[2] = <<1151.2240, 3172.3477, 40.6704>>		serverBD.fMwVehSpawn[2] = 275.0147
					serverBD.vMwVehSpawn[3] = <<1134.7161, 3169.2302, 40.6443>>		serverBD.fMwVehSpawn[3] = 287.9100
					serverBD.vMwVehSpawn[4] = <<1311.3516, 2963.9622, 40.0443>>		serverBD.fMwVehSpawn[4] = 270.2870
				BREAK
				CASE 2	//Tyler Power Station
					serverBD.vStartLocation[0] = <<2703.2813, 1576.7003, 23.5270>>   	serverBD.fStartHeading[0] = 172.4390
					serverBD.vStartLocation[1] = <<2702.7808, 1512.7255, 23.5199>> 	serverBD.fStartHeading[1] = 53.9805 
					serverBD.vStartLocation[2] = <<2678.4321, 1518.7335, 23.5295>> 	serverBD.fStartHeading[2] = 303.2142
					serverBD.vStartLocation[3] = <<2677.5085, 1566.5251, 23.5010>> 	serverBD.fStartHeading[3] = 218.5974
					
					serverBD.vMwPedSpawn[0] = <<2621.7964, 1695.8693, 26.5983>>	serverBD.fMwPedSpawn[0] =264.5842
					serverBD.vMwPedSpawn[1] = <<2746.8464, 1567.6827, 23.5010>>	serverBD.fMwPedSpawn[1] =133.7932
					serverBD.vMwPedSpawn[2] = <<2664.2727, 1599.2006, 23.7604>>	serverBD.fMwPedSpawn[2] =205.1215
					serverBD.vMwPedSpawn[3] = <<2757.2554, 1576.9390, 23.5010>>	serverBD.fMwPedSpawn[3] =66.1065
					serverBD.vMwPedSpawn[4] = <<2714.1755, 1480.4452, 23.5007>>	serverBD.fMwPedSpawn[4] =7.8064	 
					serverBD.vMwPedSpawn[5] = <<2713.3640, 1450.2719, 23.6191>>	serverBD.fMwPedSpawn[5] =358.7563
					serverBD.vMwPedSpawn[6] = <<2739.8616, 1463.7190, 23.5015>>	serverBD.fMwPedSpawn[6] =142.1748
					serverBD.vMwPedSpawn[7] = <<2676.4033, 1589.2241, 26.2548>>	serverBD.fMwPedSpawn[7] =247.816
					serverBD.vMwPedSpawn[8] = <<2703.0854, 1685.5682, 23.4887>>	serverBD.fMwPedSpawn[8] =124.4666
					serverBD.vMwPedSpawn[9] = <<2665.0217, 1409.4331, 23.5380>>	serverBD.fMwPedSpawn[9] =348.6588
					

					serverBD.vMwVehSpawn[0] = <<2737.1147, 1697.5780, 23.6755>>		serverBD.fMwVehSpawn[0] = 89.8563 
					serverBD.vMwVehSpawn[1] = <<2778.5981, 1458.2183, 23.5382>>		serverBD.fMwVehSpawn[1] = 163.8004
				//	serverBD.vMwVehSpawn[2] = <<2463.0012, 1550.9220, 33.9201>>		serverBD.fMwVehSpawn[2] = 268.0641
					serverBD.vMwVehSpawn[2] = <<2529.7788, 1641.1132, 27.9752>>  	serverBD.fMwVehSpawn[2] = 270.0754
				//	serverBD.vMwVehSpawn[3] = <<2449.0198, 1551.8517, 33.8710>>		serverBD.fMwVehSpawn[3] = 266.5420
					serverBD.vMwVehSpawn[3] = <<2665.3721, 1702.3788, 23.4882>>		serverBD.fMwVehSpawn[3] = 184.9752
					serverBD.vMwVehSpawn[4] = <<2726.6233, 1640.2986, 23.5607>>		serverBD.fMwVehSpawn[4] = 90.4555 
				BREAK
				CASE 3	//Los Santos Docks
				CASE 4
					serverBD.vStartLocation[0] = <<1088.3834, -3024.9194, 4.9010>> 	serverBD.fStartHeading[0] = 0.5044
					serverBD.vStartLocation[1] = <<1086.8459, -2975.5339, 4.9012>> 	serverBD.fStartHeading[1] = 179.2507
					serverBD.vStartLocation[2] = <<986.4979, -3024.6797, 4.9008>> 	serverBD.fStartHeading[2] = 0.3066   
					serverBD.vStartLocation[3] = <<986.1768, -2972.3940, 4.9012>> 	serverBD.fStartHeading[3] = 180.2973 
					
					serverBD.vMwPedSpawn[0] = <<991.8500, -2969.7922, 4.9008>> 	serverBD.fMwPedSpawn[0] =	336.7541	
					serverBD.vMwPedSpawn[1] = <<1178.1550, -3022.3298, 4.9021>>	serverBD.fMwPedSpawn[1] =	 6.0929	
					serverBD.vMwPedSpawn[2] = <<1107.6794, -2983.9917, 4.9010>>	serverBD.fMwPedSpawn[2] =	 4.2803	
					serverBD.vMwPedSpawn[3] = <<1043.8685, -2979.5972, 4.9010>>	serverBD.fMwPedSpawn[3] =	 274.9850	
					serverBD.vMwPedSpawn[4] = <<1042.9515, -3031.7595, 4.9010>>	serverBD.fMwPedSpawn[4] =	 267.2555	
					serverBD.vMwPedSpawn[5] = <<1095.3485, -3077.4922, 4.8877>>	serverBD.fMwPedSpawn[5] =	 6.5529	
					serverBD.vMwPedSpawn[6] = <<1114.9236, -3031.3193, 4.9010>>	serverBD.fMwPedSpawn[6] =	 89.1724	
					serverBD.vMwPedSpawn[7] = <<1048.0206, -3077.8140, 4.9010>>	serverBD.fMwPedSpawn[7] =	 348.2790	
					serverBD.vMwPedSpawn[8] = <<995.4651, -3036.8967, 4.9008>> 	serverBD.fMwPedSpawn[8] =	267.8075	
				//	serverBD.vMwPedSpawn[9] = <<1019.3149, -2895.8711, 14.2205>> serverBD.fMwPedSpawn[9] =	 191.5996
					serverBD.vMwPedSpawn[9] = <<1101.3469, -2968.8943, 13.3329>> serverBD.fMwPedSpawn[9] =	 0.5237


					serverBD.vMwVehSpawn[0] = <<1254.1913, -3126.9082, 4.8012>>		serverBD.fMwVehSpawn[0] = 357.9713
					serverBD.vMwVehSpawn[1] = <<856.4759, -3118.9695, 4.9008>>		serverBD.fMwVehSpawn[1] = 269.3234
					serverBD.vMwVehSpawn[2] = <<838.2247, -3118.7476, 4.9008>>		serverBD.fMwVehSpawn[2] = 269.3055
					serverBD.vMwVehSpawn[3] = <<1197.5948, -2983.8716, 4.9021>>		serverBD.fMwVehSpawn[3] = 359.1497
					serverBD.vMwVehSpawn[4] = <<962.6804, -3016.1587, 4.9017>>		serverBD.fMwVehSpawn[4] = 270.0873
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SAVAGE
			serverBD.MwVehModel 	= INSURGENT //BUZZARD
		//	serverBD.iKillGoal = ( MAX_MW_PEDS_PER_VEH * g_sMPTunables.iUrbanWarfareEnemyVehSpawns ) + g_sMPTunables.iUrbanWarfareEnemyOnFootSpawns
			
			
			
			serverbd.iWaveType[0] = WAVE_TYPE_AIRCRAFT_ONLY //WAVE_TYPE_AIRCRAFT_ONLY //
			serverbd.iNumVehiclesInWave[0] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_AIRCRAFT_ONLY)// 3 //3
			
			serverbd.iWaveType[1] = WAVE_TYPE_GROUND_PEDS_AND_VEHS
			serverbd.iNumVehiclesInWave[1] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS) //3
			serverbd.iNumOnFootPedsInWave[1] = GET_NUMBER_OF_GROUND_PEDS_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS) //MAX_MW_ON_FOOT_PEDS
			
			
			serverbd.iWaveType[2] = WAVE_TYPE_AIRCRAFT_ONLY
			serverbd.iNumVehiclesInWave[2] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_AIRCRAFT_ONLY) //3
			
			IF serverBD.iNumPlayerVeh <= 2
			OR NOT IS_COMPETITIVE_UW()
				serverbd.iWaveType[3] = WAVE_TYPE_BOSS
				serverbd.iNumVehiclesInWave[3] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_BOSS) //1
				
				serverbd.iNumOfWaves = 4
			ELIF serverBD.iNumPlayerVeh = 3
				serverbd.iWaveType[3] = WAVE_TYPE_GROUND_PEDS_AND_VEHS
				serverbd.iNumVehiclesInWave[3] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS) //3
				serverbd.iNumOnFootPedsInWave[3] = GET_NUMBER_OF_GROUND_PEDS_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS) 
				
				serverbd.iWaveType[4] = WAVE_TYPE_AIRCRAFT_ONLY //WAVE_TYPE_AIRCRAFT_ONLY //
				serverbd.iNumVehiclesInWave[4] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_AIRCRAFT_ONLY)
				
				serverbd.iWaveType[5] = WAVE_TYPE_BOSS
				serverbd.iNumVehiclesInWave[5] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_BOSS)
				
				serverbd.iNumOfWaves = 6
			ELSE
				serverbd.iWaveType[3] = WAVE_TYPE_GROUND_PEDS_AND_VEHS
				serverbd.iNumVehiclesInWave[3] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS) //3
				serverbd.iNumOnFootPedsInWave[3] = GET_NUMBER_OF_GROUND_PEDS_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS) 
				
				serverbd.iWaveType[4] = WAVE_TYPE_AIRCRAFT_ONLY //WAVE_TYPE_AIRCRAFT_ONLY //
				serverbd.iNumVehiclesInWave[4] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_AIRCRAFT_ONLY)
				
				serverbd.iWaveType[5] = WAVE_TYPE_GROUND_PEDS_AND_VEHS
				serverbd.iNumVehiclesInWave[5] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS) //3
				serverbd.iNumOnFootPedsInWave[5] = GET_NUMBER_OF_GROUND_PEDS_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS) 
				
				serverbd.iWaveType[6] = WAVE_TYPE_BOSS
				serverbd.iNumVehiclesInWave[6] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_BOSS)
				
				serverbd.iNumOfWaves = 7
			ENDIF
			
			#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_TestSavage")
					PRINTLN("     ---------->     KILL LIST - [GET_RANDOM_LOCATION] running with sc_TestSavage")
					serverbd.iNumOfWaves = 1
			
					serverbd.iWaveType[0] = WAVE_TYPE_BOSS 
					serverbd.iNumVehiclesInWave[0] = 1
				ENDIF
			#ENDIF
//			
			serverBD.iKillGoal = GET_KILL_GOAL() //serverbd.iNumOnFootPedsInWave[0] + serverbd.iNumOnFootPedsInWave[1] + serverbd.iNumOnFootPedsInWave[2]
			
			IF NOT IS_COMPETITIVE_UW()
				serverbd.iHistorySubType = 2
			ELSE
				serverbd.iHistorySubType = 7
			ENDIF
			serverbd.iHistoryVariation = serverBD.iLocation
			
			SWITCH serverBD.iLocation
				CASE 0	//LSIA
					serverBD.vStartLocation[0] = <<-1177.6504, -2845.5422, 12.9458>>	serverBD.fStartHeading[0] = 331.7400
					serverBD.vStartLocation[1] = <<-1082.4794, -2900.1914, 12.9478>>	serverBD.fStartHeading[1] = 327.7335
					serverBD.vStartLocation[2] = <<-1202.0188, -2873.6702, 12.9454>> 	serverBD.fStartHeading[2] = 149.1604 
					serverBD.vStartLocation[3] = <<-1126.5802, -2918.4158, 12.9454>> 	serverBD.fStartHeading[3] = 150.6753 
					
//					IF GET_CURRENT_WAVE_TYPE() = WAVE_TYPE_AIRCRAFT_ONLY
//					OR GET_CURRENT_WAVE_TYPE() = WAVE_TYPE_BOSS
//						serverBD.vMwVehSpawn[0] = <<-1292.1892, -2572.8677, 22.9452>>	serverBD.fMwVehSpawn[0] =  332.6443   
//						serverBD.vMwVehSpawn[1] = <<-1062.8428, -3502.9131, 23.1484>>	serverBD.fMwVehSpawn[1] =  326.4740  
//						serverBD.vMwVehSpawn[2] = <<-1842.1886, -3149.9641, 22.9444>>	serverBD.fMwVehSpawn[2] =  177.2589  
//						serverBD.vMwVehSpawn[3] = <<-1271.3334, -2267.1489, 12.9454>>	serverBD.fMwVehSpawn[3] =  243.2036 
//						
//					//	serverBD.iMaxVehicles = 4
//					ELSE
//					
//						serverBD.vMwVehSpawn[0] = <<-892.8904, -2935.9421, 12.9444>>	serverBD.fMwVehSpawn[0] =  50.7848     
//						serverBD.vMwVehSpawn[1] = <<-1290.7130, -2658.8442, 13.0516>>	serverBD.fMwVehSpawn[1] =   152.2952  
//						serverBD.vMwVehSpawn[2] = <<-1157.6818, -2725.7065, 12.9534>>	serverBD.fMwVehSpawn[2] =   187.5114  
//						serverBD.vMwVehSpawn[3] = <<-949.6019, -3072.9910, 12.9444>>	serverBD.fMwVehSpawn[3] =  63.8501  
//						
//					//	serverBD.iMaxVehicles = 4
//					ENDIF
					
					serverBD.vMwPedSpawn[0] = <<-985.8373, -2942.9734, 12.9451>>	serverBD.fMwPedSpawn[0] = 147.2177
					serverBD.vMwPedSpawn[1] = <<-1051.4122, -2880.8284, 29.3631>>	serverBD.fMwPedSpawn[1] =  103.9461 // (rocket troop)
					serverBD.vMwPedSpawn[2] = <<-1154.5397, -2821.0911, 29.3631>>	serverBD.fMwPedSpawn[2] =  160.8220   
					serverBD.vMwPedSpawn[3] = <<-1211.8983, -2789.1250, 12.9523>>	serverBD.fMwPedSpawn[3] =  205.8375  
					serverBD.vMwPedSpawn[4] = <<-1199.4928, -2724.7090, 12.9771>>	serverBD.fMwPedSpawn[4] =  124.7328  
				BREAK
				
				
				
				CASE 1 // Mackenzie Airfield
				//	serverBD.vStartLocation[0] = <<2121.8389, 4812.8896, 40.1959>>   	serverBD.fStartHeading[0] = 226.9563 GANG ATTACK Loc
					serverBD.vStartLocation[0] = <<1930.0668, 4710.3389, 40.1633>>   	serverBD.fStartHeading[0] = 344.2504
					serverBD.vStartLocation[1] = <<1954.5598, 4724.1812, 40.0604>>		serverBD.fStartHeading[1] = 340.3425
					serverBD.vStartLocation[2] = <<2004.9131, 4748.0400, 40.0604>> 		serverBD.fStartHeading[2] = 351.5693 
					serverBD.vStartLocation[3] = <<2027.9282, 4757.3872, 40.0595>> 		serverBD.fStartHeading[3] = 354.2446 
						

					serverBD.vMwPedSpawn[0] = <<1954.2092, 4653.2324, 39.7136>>	serverBD.fMwPedSpawn[0] = 245.2865
					serverBD.vMwPedSpawn[1] = <<2116.1770, 4761.9927, 40.2281>>	serverBD.fMwPedSpawn[1] = 74.1185
					serverBD.vMwPedSpawn[2] = <<2123.4141, 4784.4067, 39.9703>>	serverBD.fMwPedSpawn[2] = 10.2312
					serverBD.vMwPedSpawn[3] = <<1925.1587, 4817.9248, 43.9706>>	serverBD.fMwPedSpawn[3] = 222.4017
					serverBD.vMwPedSpawn[4] = <<2009.5798, 4802.5054, 41.0090>>	serverBD.fMwPedSpawn[4] = 150.2763
					serverBD.vMwPedSpawn[5] = <<2133.4719, 4834.5278, 40.3470>>	serverBD.fMwPedSpawn[5] = 105.4526
					serverBD.vMwPedSpawn[6] = <<2139.0581, 4792.0840, 39.9703>>	serverBD.fMwPedSpawn[6] = 88.9429 
					serverBD.vMwPedSpawn[7] = <<2014.2765, 4721.0620, 40.6111>>	serverBD.fMwPedSpawn[7] = 51.1733
					serverBD.vMwPedSpawn[8] = <<2060.4175, 4847.6914, 40.8344>>	serverBD.fMwPedSpawn[8] = 169.7083
					serverBD.vMwPedSpawn[9] = <<2151.4336, 4789.4458, 39.9594>>	serverBD.fMwPedSpawn[9] = 29.5849
					
 
					
					serverbd.iMaxOnFloorPeds = 3
				BREAK
				
				CASE 2 // Paleto Bay police station
					IF NOT IS_COMPETITIVE_UW()
						serverBD.vStartLocation[0] = <<206.2382, 7031.6694, 1.1068>>   		serverBD.fStartHeading[0] = 319.1530
					ELSE
						serverBD.vStartLocation[0] = <<206.2382, 7031.6694, 1.1068>>   		serverBD.fStartHeading[0] = 319.1530
						serverBD.vStartLocation[1] = <<218.7712, 7018.6538, 1.0042>>		serverBD.fStartHeading[1] = 316.7079
						serverBD.vStartLocation[2] = <<230.1561, 7001.5889, 1.2508>> 		serverBD.fStartHeading[2] = 316.9623 
						serverBD.vStartLocation[3] = <<242.3287, 6987.2627, 1.7354>> 		serverBD.fStartHeading[3] = 314.2003 
					ENDIF
					
   
					serverBD.vMwPedSpawn[0] = <<131.5444, 6956.8867, 12.1839>>	serverBD.fMwPedSpawn[0] = 326.4292
					serverBD.vMwPedSpawn[1] = <<201.7152, 6842.8643, 21.2216>>	serverBD.fMwPedSpawn[1] = 6.8118	
					serverBD.vMwPedSpawn[2] = <<66.3015, 6924.1460, 12.2944>>	serverBD.fMwPedSpawn[2] = 32.8776	
					serverBD.vMwPedSpawn[3] = <<42.4962, 7055.1904, 1.3986>> 	serverBD.fMwPedSpawn[3] = 8.3870	
					serverBD.vMwPedSpawn[4] = <<146.7828, 6849.0391, 17.5710>>	serverBD.fMwPedSpawn[4] = 291.5504
					serverBD.vMwPedSpawn[5] = <<290.2660, 6946.9175, 3.1171>>	serverBD.fMwPedSpawn[5] = 333.1838
					serverBD.vMwPedSpawn[6] = <<50.5669, 7102.2134, 2.0034>> 	serverBD.fMwPedSpawn[6] = 203.8353
					serverBD.vMwPedSpawn[7] = <<274.1516, 6856.5063, 15.8452>>	serverBD.fMwPedSpawn[7] = 32.2329	
					serverBD.vMwPedSpawn[8] = <<88.9963, 7113.4717, 24.5763>>	serverBD.fMwPedSpawn[8] = 195.4847
					serverBD.vMwPedSpawn[9] = <<131.6119, 6956.0815, 12.2413>>	serverBD.fMwPedSpawn[9] = 327.4941
					
					serverbd.iMaxOnFloorPeds = 4
				BREAK
				
				CASE 3 // Fridgit Helipad
					IF NOT IS_COMPETITIVE_UW()
						serverBD.vStartLocation[0] = <<704.1097, -1579.9468, 8.6915>>   	serverBD.fStartHeading[0] = 156.0209
					ELSE
						serverBD.vStartLocation[0] = <<704.1097, -1579.9468, 8.6915>>   	serverBD.fStartHeading[0] = 156.0209
						serverBD.vStartLocation[1] = <<699.7866, -1637.3372, 8.7086>>		serverBD.fStartHeading[1] = 41.8571
						serverBD.vStartLocation[2] = <<663.7250, -1634.0522, 8.7086>> 		serverBD.fStartHeading[2] = 305.7411 
						serverBD.vStartLocation[3] = <<662.7472, -1596.4681, 8.7086>> 		serverBD.fStartHeading[3] = 223.2489 
					ENDIF
					
					IF GET_CURRENT_WAVE_TYPE() = WAVE_TYPE_AIRCRAFT_ONLY
					OR GET_CURRENT_WAVE_TYPE() = WAVE_TYPE_BOSS

						
						serverBD.iMaxVehicles = 3
					ELSE
						
 
						
						serverBD.iMaxVehicles = 4
					ENDIF


					
					serverBD.vMwPedSpawn[0] = <<585.7374, -1450.2639, 19.0430>>	serverBD.fMwPedSpawn[0] =  197.3456
					serverBD.vMwPedSpawn[1] = <<784.6684, -1526.1722, 9.5498>>	serverBD.fMwPedSpawn[1] = 140.7211	
					serverBD.vMwPedSpawn[2] = <<790.8864, -1603.6653, 30.2421>>	serverBD.fMwPedSpawn[2] =  98.9528	
					serverBD.vMwPedSpawn[3] = <<782.0420, -1487.9630, 19.2726>>	serverBD.fMwPedSpawn[3] =  107.0238	
					serverBD.vMwPedSpawn[4] = <<698.9001, -1443.5546, 21.3382>>	serverBD.fMwPedSpawn[4] =  202.1290
					serverBD.vMwPedSpawn[5] = <<566.2017, -1506.3856, 27.1027>>	serverBD.fMwPedSpawn[5] =  230.1207
					serverBD.vMwPedSpawn[6] = <<752.1943, -1491.9399, 19.4421>>	serverBD.fMwPedSpawn[6] =  36.1940 
					serverBD.vMwPedSpawn[7] = <<702.7894, -1452.9290, 30.3215>>	serverBD.fMwPedSpawn[7] =  48.2522	
					serverBD.vMwPedSpawn[8] = <<592.6577, -1452.3037, 28.8626>>	serverBD.fMwPedSpawn[8] =  296.1140
					serverBD.vMwPedSpawn[9] = <<794.9213, -1660.6195, 43.1470>>	serverBD.fMwPedSpawn[9] =  3.1632
					
					/*
						Other Merryweather Ped Spawns  
					Player Position = <<968.0074, -1640.7981, 29.1107>>, 180.4064  
					Player Position = <<1012.7549, -1841.5769, 30.5733>>, 336.9663 
					
					*/


				BREAK
			ENDSWITCH
			
		BREAK
		
		CASE HYDRA
			
			serverBD.MwVehModel 	= LAZER
			
			
			
			serverbd.iWaveType[0] = WAVE_TYPE_AIRCRAFT_ONLY
			serverbd.iNumVehiclesInWave[0] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_AIRCRAFT_ONLY) //3
			
			serverbd.iWaveType[1] = WAVE_TYPE_AIRCRAFT_ONLY
			serverbd.iNumVehiclesInWave[1] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_AIRCRAFT_ONLY) //3
			
			serverbd.iWaveType[2] = WAVE_TYPE_AIRCRAFT_ONLY
			serverbd.iNumVehiclesInWave[2] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_AIRCRAFT_ONLY) //3
			
			IF serverBD.iNumPlayerVeh <= 2
			OR NOT IS_COMPETITIVE_UW()
				serverbd.iNumOfWaves = 3
			ELIF serverBD.iNumPlayerVeh = 3
				serverbd.iWaveType[3] = WAVE_TYPE_AIRCRAFT_ONLY
				serverbd.iNumVehiclesInWave[3] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_AIRCRAFT_ONLY) //3
				
				serverbd.iWaveType[4] = WAVE_TYPE_AIRCRAFT_ONLY
				serverbd.iNumVehiclesInWave[4] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_AIRCRAFT_ONLY) //3
				
				serverbd.iNumOfWaves = 5
			ELSE
				serverbd.iWaveType[3] = WAVE_TYPE_AIRCRAFT_ONLY
				serverbd.iNumVehiclesInWave[3] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_AIRCRAFT_ONLY) //3
				
				serverbd.iWaveType[4] = WAVE_TYPE_AIRCRAFT_ONLY
				serverbd.iNumVehiclesInWave[4] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_AIRCRAFT_ONLY) //3
				
				serverbd.iWaveType[5] = WAVE_TYPE_AIRCRAFT_ONLY
				serverbd.iNumVehiclesInWave[5] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_AIRCRAFT_ONLY) 
				
				serverbd.iNumOfWaves = 6
			ENDIF
			
			serverBD.iKillGoal = GET_KILL_GOAL() //serverbd.iNumVehiclesInWave[0] + serverbd.iNumVehiclesInWave[1] + serverbd.iNumVehiclesInWave[2]
			
			IF NOT IS_COMPETITIVE_UW()
				serverbd.iHistorySubType = 1
			ELSE
				serverbd.iHistorySubType = 6
			ENDIF
			serverbd.iHistoryVariation = serverBD.iLocation
			
			SWITCH serverBD.iLocation
				CASE 0	//Merryweather Docks
					IF NOT IS_COMPETITIVE_UW()
						serverBD.vStartLocation[0] = <<145.8397, -2790.1968, 5.0002>> 	serverBD.fStartHeading[0] = 197.4435
					ELSE
						serverBD.vStartLocation[0] = <<145.8397, -2790.1968, 5.0002>> 	serverBD.fStartHeading[0] = 197.4435
						serverBD.vStartLocation[1] = <<132.5468, -2805.1807, 5.0002>>  	serverBD.fStartHeading[1] = 197.1133
						serverBD.vStartLocation[2] = <<123.1125, -2823.6650, 5.0002>>  	serverBD.fStartHeading[2] = 272.2065
						serverBD.vStartLocation[3] = <<163.5494, -2795.9202, 5.0002>>  	serverBD.fStartHeading[3] = 142.9410
					ENDIF
					
					serverBD.vMwVehSpawn[0] = <<887.9951, -3884.4497, 54.3252>>			serverBD.fMwVehSpawn[0] = 34.1490
					serverBD.vMwVehSpawn[1] =  <<-255.1527, -4101.9810, 100.1964>>		serverBD.fMwVehSpawn[1] =  -26.0182
					serverBD.vMwVehSpawn[2] = <<1271.1887, -3196.2434, 104.9035>>		serverBD.fMwVehSpawn[2] = 88.3998  
					
					serverBD.vMwVehSpawn[3] = <<-879.6379, -2210.0242, 245.7120>>		serverBD.fMwVehSpawn[3] = -141.7949 
					 
					serverBD.iTimeInitialSpawn[0] = 10000
					serverBD.iTimeInitialSpawn[1] = 10000
					serverBD.iTimeInitialSpawn[2] = 10000
					serverBD.iTimeInitialSpawn[3] = 10000

					
					serverBD.vMwPedSpawn[0] = <<546.2563, -3289.3162, 17.714>>		serverBD.fMwPedSpawn[0] = 143.4012
					serverBD.vMwPedSpawn[1] = <<596.1830, -3288.1736, 17.714>>		serverBD.fMwPedSpawn[1] = 212.1136
					serverBD.vMwPedSpawn[2] = <<500.3511, -3338.8940, 23.591>>		serverBD.fMwPedSpawn[2] = 95.8248 
					serverBD.vMwPedSpawn[3] = <<480.8767, -3238.4136, 5.0696>>		serverBD.fMwPedSpawn[3] = 113.3084
					serverBD.vMwPedSpawn[4] = <<444.4339, -3187.9558, 5.0703>>		serverBD.fMwPedSpawn[4] = 260.1329  
					
					serverBD.iMaxVehicles = 4
				BREAK
				
				CASE 1	//Del Perro Helipad
					IF NOT IS_COMPETITIVE_UW()
						serverBD.vStartLocation[0] = <<-1762.4076, -786.7469, 8.0750>> 		serverBD.fStartHeading[0] =  131.0993
					ELSE
						serverBD.vStartLocation[0] = <<-1762.4076, -786.7469, 8.0750>> 		serverBD.fStartHeading[0] =  131.0993
						serverBD.vStartLocation[1] = <<-1734.9775, -796.1480, 8.3998>>  	serverBD.fStartHeading[1] = 127.8408
						serverBD.vStartLocation[2] = <<-1762.0950, -758.1465, 8.6368>>  	serverBD.fStartHeading[2] = 131.1571
						serverBD.vStartLocation[3] = <<-1792.5006, -751.6967, 8.1782>>  	serverBD.fStartHeading[3] = 132.9430
					ENDIF
					

					serverBD.vMwVehSpawn[0] = <<-1876.4971, -665.6128, 109.1172>>		serverBD.fMwVehSpawn[0] = 316.9068
					serverBD.vMwVehSpawn[1] = <<-1099.2975, 20.5145, 150.0107>>		serverBD.fMwVehSpawn[1] = 142.3075
					serverBD.vMwVehSpawn[2] = <<-803.6566, -866.0300, 137.4505>>		serverBD.fMwVehSpawn[2] = 93.1231
					
					serverBD.vMwVehSpawn[3] = <<-1697.5353, -1306.9553, 127.8882>>		serverBD.fMwVehSpawn[3] = -9.0334

					serverBD.iTimeInitialSpawn[0] = 10000
					serverBD.iTimeInitialSpawn[1] = 10000
					serverBD.iTimeInitialSpawn[2] = 10000
					serverBD.iTimeInitialSpawn[3] = 10000
					/*
						Other spawns
						
						Player Position = <<-1200.5165, -566.6124, 26.3105>>, 128.6166
						Player Position = <<-1777.7445, -374.5820, 44.0157>>, 328.6901
						Player Position = <<-1450.8756, -629.7714, 29.4039>>, 34.3026  
						Player Position = <<-1311.4540, -285.2619, 38.3859>>, 120.2927   

					*/
					serverBD.vMwPedSpawn[0] = <<-1468.4458, -546.4570, 84.0730>>	serverBD.fMwPedSpawn[0] = 236.3566
					serverBD.vMwPedSpawn[1] = <<-1288.0239, -439.2769, 102.470>>	serverBD.fMwPedSpawn[1] = 212.7533
					serverBD.vMwPedSpawn[2] = <<-1556.8313, -589.7376, 32.9884>>	serverBD.fMwPedSpawn[2] = 299.3619
					serverBD.vMwPedSpawn[3] = <<-1353.2806, -514.1478, 22.2694>>	serverBD.fMwPedSpawn[3] = 115.7556
					serverBD.vMwPedSpawn[4] = <<-1314.6932, -570.6011, 28.3023>>	serverBD.fMwPedSpawn[4] = 191.3862  
					
					serverBD.iMaxVehicles = 4
				BREAK
				
				CASE 2	//Vinewood Radio Tower
					serverBD.vStartLocation[0] = <<737.7632, 1292.3484, 359.2960>>	serverBD.fStartHeading[0] = 176.2440
					
					serverBD.vMwVehSpawn[0] = <<737.1152, 1296.3625, 459.2960 >>	serverBD.fMwVehSpawn[0] = 93.8354 
					serverBD.vMwVehSpawn[1] = <<-425.5261, 1185.3806, 324.6416>>	serverBD.fMwVehSpawn[1] = 250.5173
					serverBD.vMwVehSpawn[2] = <<1157.6166, 116.7023, 180.3293  >>	serverBD.fMwVehSpawn[2] = 337.8961
					serverBD.vMwVehSpawn[3] = <<1604.7471, 2094.8589, 184.1727 >>	serverBD.fMwVehSpawn[3] = 142.1369
					
					serverBD.iTimeInitialSpawn[0] = 10000
					serverBD.iTimeInitialSpawn[1] = 10000
					serverBD.iTimeInitialSpawn[2] = 10000
					serverBD.iTimeInitialSpawn[3] = 10000
					
					/*
						Other veh spawns
						
						Player Position = <<314.6902, 1176.1937, 235.4831>>, 189.5848   
						Player Position = <<355.8669, 760.2024, 180.8263>>, 304.1530  
						Player Position = <<1028.4452, 928.3929, 215.0935>>, 350.1159
						Player Position = <<596.9350, 1404.2166, 313.4470>>, 267.1877  

					*/
					
					serverBD.vMwPedSpawn[0] = <<662.4390, 1284.1891, 359.2960>>		serverBD.fMwPedSpawn[0] = 208.2624
					serverBD.vMwPedSpawn[1] = <<782.1017, 1279.3444, 359.2967>>		serverBD.fMwPedSpawn[1] = 349.0480
					serverBD.vMwPedSpawn[2] = <<769.1426, 1300.7440, 359.2967>>		serverBD.fMwPedSpawn[2] = 69.8162
					serverBD.vMwPedSpawn[3] = <<750.6609, 1209.1548, 326.5541>>		serverBD.fMwPedSpawn[3] = 286.6181
  
					
					serverBD.iMaxVehicles = 4 
					serverBD.iMaxOnFloorPeds = 4
				BREAK
				
				CASE 3	//NOOSE Government Facility
					serverBD.vStartLocation[0] = <<2521.1213, -471.1436, 91.9971>> 	serverBD.fStartHeading[0] = 292.0822
					serverBD.vStartLocation[1] =<<2510.2734, -473.5832, 91.9929>>  	serverBD.fStartHeading[1] = 43.3907
					serverBD.vStartLocation[2] =<<2481.3901, -320.2297, 91.9927>>  	serverBD.fStartHeading[2] = 124.2279
					serverBD.vStartLocation[3] =<<2560.3145, -438.3987, 91.9926>>  	serverBD.fStartHeading[3] = 129.3366
					
					serverBD.vMwVehSpawn[0] = <<2870.5044, 368.2049, 101.6602>>		serverBD.fMwVehSpawn[0] = 137.5025
					serverBD.vMwVehSpawn[1] = <<1859.5959, 272.0942, 162.159>>		serverBD.fMwVehSpawn[1] = 227.9336
					serverBD.vMwVehSpawn[2] = <<2666.0332, -931.6996, 100.6606>>	serverBD.fMwVehSpawn[2] = 6.0299  
					serverBD.vMwVehSpawn[3] = <<1336.4745, -345.6518, 424.9653>>	serverBD.fMwVehSpawn[3] = -87.5596

					serverBD.iTimeInitialSpawn[0] = 10000
					serverBD.iTimeInitialSpawn[1] = 10000
					serverBD.iTimeInitialSpawn[2] = 10000
					serverBD.iTimeInitialSpawn[3] = 10000
					
					serverBD.iMaxVehicles = 4 
					/*
						Other veh spawns
						Player Position = <<2657.8354, -759.8398, 36.5650>>, 330.2291   
						Player Position = <<2314.9465, -378.0281, 74.8138>>, 203.6772   
						Player Position = <<2008.7487, -913.3941, 78.4732>>, 311.5874  
						Player Position = <<2535.4385, -453.8173, 91.9929>>, 311.9730  

					*/
					
					serverBD.vMwPedSpawn[0] = <<2528.3760, -424.9132, 113.0900>>		serverBD.fMwPedSpawn[0] = 338.8487
					serverBD.vMwPedSpawn[1] = <<2473.8284, -384.0913, 108.6521>>		serverBD.fMwPedSpawn[1] = 272.5443
					serverBD.vMwPedSpawn[2] = <<2535.3215, -335.4323, 113.0844>>		serverBD.fMwPedSpawn[2] = 223.0158
					serverBD.vMwPedSpawn[3] = <<2478.7522, -416.0706, 92.7351 >>		serverBD.fMwPedSpawn[3] = 355.6682
					serverBD.vMwPedSpawn[4] = <<2482.1758, -353.0269, 92.7351 >>		serverBD.fMwPedSpawn[4] = 179.3658  

					
				BREAK
				
				CASE 4	//LSIA Hangar
					IF NOT IS_COMPETITIVE_UW()
						serverBD.vStartLocation[0] =<<-1822.3245, -3167.5879, 12.9444>> 	serverBD.fStartHeading[0] = 331.7771
					ELSE
						serverBD.vStartLocation[0] =<<-1822.3245, -3167.5879, 12.9444>> 	serverBD.fStartHeading[0] = 331.7771
						serverBD.vStartLocation[1] =<<-1841.3795, -3156.6663, 12.9444>> 	serverBD.fStartHeading[1] = 324.1843
						serverBD.vStartLocation[2] =<<-1860.8583, -3143.9216, 12.9444>> 	serverBD.fStartHeading[2] = 330.0586
						serverBD.vStartLocation[3] =<<-1885.1311, -3129.9482, 12.9444>> 	serverBD.fStartHeading[3] = 329.5413
					ENDIF
					
					serverBD.vMwVehSpawn[0] = <<-1185.2023, -2364.7261, 112.9452>>		serverBD.fMwVehSpawn[0] = 146.9594
					serverBD.vMwVehSpawn[1] = <<-382.1712, -2495.0500, 105.0008  >>		serverBD.fMwVehSpawn[1] = 137.6555
					serverBD.vMwVehSpawn[2] = <<-412.0741, -2847.3071, 105.0004  >>		serverBD.fMwVehSpawn[2] = 114.7478
					serverBD.vMwVehSpawn[3] = <<-2538.4470, -3165.7581, 133.9589>>		serverBD.fMwVehSpawn[3] = -84.6269

					serverBD.iTimeInitialSpawn[0] = 10000
					serverBD.iTimeInitialSpawn[1] = 10000
					serverBD.iTimeInitialSpawn[2] = 10000
					serverBD.iTimeInitialSpawn[3] = 10000
					
					serverBD.iMaxVehicles = 4 
					
					/*
						Other veh spawns
						
						Player Position = <<-943.3812, -2895.8623, 12.9541>>, 63.1608 
						Player Position = <<-1688.8308, -3107.9377, 12.9448>>, 329.5252  
						Player Position = <<-1290.1958, -2657.1580, 13.0516>>, 150.8662   
						Player Position = <<-1227.4806, -2691.7659, 12.9449>>, 150.7545 
						Player Position = <<-1156.3643, -2726.3772, 12.9533>>, 176.7792 
						Player Position = <<-893.9465, -3018.2344, 12.9444>>, 151.1322  

						
					*/
					
					
					serverBD.vMwPedSpawn[0] = <<-1051.5664, -2880.6667, 29.3631>>		serverBD.fMwPedSpawn[0] = 153.0449 
					serverBD.vMwPedSpawn[1] = <<-1154.1666, -2821.0889, 29.3631>>		serverBD.fMwPedSpawn[1] = 153.1271 
					serverBD.vMwPedSpawn[2] = <<-1441.8662, -3280.6755, 12.9449>>		serverBD.fMwPedSpawn[2] = 331.3771 
					serverBD.vMwPedSpawn[3] = <<-1209.3198, -2789.0227, 12.9523>>		serverBD.fMwPedSpawn[3] = 183.2579 
					serverBD.vMwPedSpawn[4] = <<-1239.1879, -2688.9426, 12.9449>>		serverBD.fMwPedSpawn[4] = 149.3518   

					
				BREAK
				
				CASE 5 // Arcadius building
					serverBD.vStartLocation[0] = <<-144.0471, -593.1377, 210.7752>>	serverBD.fStartHeading[0] = 184.7817
					
					serverBD.vMwVehSpawn[0] = <<449.0346, -981.1837, 142.6917>> 		serverBD.fMwVehSpawn[0] =  91.7761   
					serverBD.vMwVehSpawn[1] = <<-665.6040, -932.7220, 145.3242>>		serverBD.fMwVehSpawn[1] =  269.7376   
					serverBD.vMwVehSpawn[2] = <<-686.7255, 217.4708, 136.5301>>			serverBD.fMwVehSpawn[1] =  211.4614   
					
					serverBD.vMwVehSpawn[3] = <<314.4621, -130.8896, 222.2372>>			serverBD.fMwVehSpawn[3] =  148.9204  

					
					serverBD.iTimeInitialSpawn[0] = 10000
					serverBD.iTimeInitialSpawn[1] = 10000
					serverBD.iTimeInitialSpawn[2] = 10000
					serverBD.iTimeInitialSpawn[3] = 10000
					
					serverBD.iMaxVehicles = 4
					
					/*
					Other vehicles
					Player Position = <<-43.9393, -721.7862, 32.0634>>, 164.6539 
					Player Position = <<124.6335, -571.6476, 30.5840>>, 71.3263   
					Player Position = <<105.0714, -699.1705, 32.1230>>, 69.2682

					*/
					
					serverBD.vMwPedSpawn[0] = <<-159.1301, -600.2693, 200.7354>>	serverBD.fMwPedSpawn[0] =  113.2980   
					serverBD.vMwPedSpawn[1] = <<-145.0832, -578.3243, 200.7354>>	serverBD.fMwPedSpawn[1] =  353.0292  
					serverBD.vMwPedSpawn[2] = <<-135.3186, -606.2405, 200.7354>>	serverBD.fMwPedSpawn[2] =  216.1793  
					serverBD.vMwPedSpawn[3] = <<-208.5985, -725.5820, 219.5222>>	serverBD.fMwPedSpawn[3] =  347.4008 // (Rooftop rocket troops)
					
					serverbd.iMaxOnFloorPeds = 4
				BREAK
			ENDSWITCH
		BREAK
		
		CASE VALKYRIE
			serverBD.MwVehModel 	= BUZZARD
			
			
			
			serverbd.iWaveType[0] = WAVE_TYPE_AIRCRAFT_ONLY
			serverbd.iNumVehiclesInWave[0] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_AIRCRAFT_ONLY) //3
			serverbd.iNumOnFootPedsInWave[0] = GET_NUMBER_OF_GROUND_PEDS_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS) //MAX_MW_ON_FOOT_PEDS
			
			serverbd.iWaveType[1] = WAVE_TYPE_GROUND_PEDS_AND_VEHS
			serverbd.iNumVehiclesInWave[1] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS) //3
			serverbd.iNumOnFootPedsInWave[1] = GET_NUMBER_OF_GROUND_PEDS_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS) //MAX_MW_ON_FOOT_PEDS
			
			
			serverbd.iWaveType[2] = WAVE_TYPE_AIRCRAFT_ONLY
			serverbd.iNumVehiclesInWave[2] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_AIRCRAFT_ONLY) //3
			serverbd.iNumOnFootPedsInWave[2] = GET_NUMBER_OF_GROUND_PEDS_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS) //MAX_MW_ON_FOOT_PEDS
			
			IF serverBD.iNumPlayerVeh <= 2
			OR NOT IS_COMPETITIVE_UW()
			
				serverbd.iWaveType[3] = WAVE_TYPE_BOSS
				serverbd.iNumVehiclesInWave[3] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_BOSS) //1
				
				serverbd.iNumOfWaves = 4
			
			ELIF serverBD.iNumPlayerVeh = 3
				serverbd.iWaveType[3] = WAVE_TYPE_GROUND_PEDS_AND_VEHS
				serverbd.iNumVehiclesInWave[3] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS) //3
				serverbd.iNumOnFootPedsInWave[3] = GET_NUMBER_OF_GROUND_PEDS_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS) //MAX_MW_ON_FOOT_PEDS
				
				
				serverbd.iWaveType[4] = WAVE_TYPE_AIRCRAFT_ONLY
				serverbd.iNumVehiclesInWave[4] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_AIRCRAFT_ONLY) //3
				
				serverbd.iWaveType[5] = WAVE_TYPE_BOSS
				serverbd.iNumVehiclesInWave[5] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_BOSS)
				
				serverbd.iNumOfWaves = 6
			ELSE
				serverbd.iWaveType[3] = WAVE_TYPE_GROUND_PEDS_AND_VEHS
				serverbd.iNumVehiclesInWave[3] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS) //3
				serverbd.iNumOnFootPedsInWave[3] = GET_NUMBER_OF_GROUND_PEDS_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS) //MAX_MW_ON_FOOT_PEDS
				
				
				serverbd.iWaveType[4] = WAVE_TYPE_AIRCRAFT_ONLY
				serverbd.iNumVehiclesInWave[4] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_AIRCRAFT_ONLY)
				
				serverbd.iWaveType[5] = WAVE_TYPE_GROUND_PEDS_AND_VEHS
				serverbd.iNumVehiclesInWave[5] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS) //3
				serverbd.iNumOnFootPedsInWave[5] = GET_NUMBER_OF_GROUND_PEDS_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS)
				
				serverbd.iWaveType[6] = WAVE_TYPE_BOSS
				serverbd.iNumVehiclesInWave[6] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_BOSS)
				
				serverbd.iNumOfWaves = 7
			ENDIF
			
			serverBD.iKillGoal = GET_KILL_GOAL() //( MAX_MW_PEDS_PER_VEH * g_sMPTunables.iUrbanWarfareEnemyVehSpawns ) + g_sMPTunables.iUrbanWarfareEnemyOnFootSpawns
			
			IF NOT IS_COMPETITIVE_UW()
				serverbd.iHistorySubType = 3
			ELSE
				serverbd.iHistorySubType = 8
			ENDIF
			serverbd.iHistoryVariation = serverBD.iLocation
			
			SWITCH serverBD.iLocation 
				CASE 0 // Heliport
					IF NOT IS_COMPETITIVE_UW()
						serverBD.vStartLocation[0] = <<-758.7451, -1488.6119, 4.0005>> 		serverBD.fStartHeading[0] = 290.0732
					ELSE

						serverBD.vStartLocation[0] = <<-758.7451, -1488.6119, 4.0005>>  	serverBD.fStartHeading[0] = 290.0732
						serverBD.vStartLocation[1] = <<-754.8721, -1442.6840, 4.0005>>  	serverBD.fStartHeading[1] = 231.5867
						serverBD.vStartLocation[2] = <<-712.4144, -1459.3308, 4.0005>>  	serverBD.fStartHeading[2] = 49.6234
						serverBD.vStartLocation[3] = <<-729.9052, -1418.7335, 4.0005>>  	serverBD.fStartHeading[3] = 229.3317
					ENDIF
					

					serverBD.vMwPedSpawn[0] = <<-692.5144, -1384.7744, 4.1503>>		serverBD.fMwPedSpawn[0] = 230.0049	
					serverBD.vMwPedSpawn[1] = <<-724.5416, -1374.0242, 0.6002>>		serverBD.fMwPedSpawn[1] = 147.9421	
					serverBD.vMwPedSpawn[2] = <<-763.0113, -1531.6372, 4.4284>>		serverBD.fMwPedSpawn[2] = 22.3204	
					serverBD.vMwPedSpawn[3] = <<-622.6572, -1346.9967, 10.7793>>	serverBD.fMwPedSpawn[3] =  80.7676	
					serverBD.vMwPedSpawn[4] = <<-650.5557, -1313.3373, 9.8688>>		serverBD.fMwPedSpawn[4] = 162.0507	
					serverBD.vMwPedSpawn[5] = <<-687.8698, -1314.5190, 4.1019>>		serverBD.fMwPedSpawn[5] = 225.9002	
					serverBD.vMwPedSpawn[6] = <<-719.8957, -1300.3997, 4.1019>>		serverBD.fMwPedSpawn[6] = 140.0515
					serverBD.vMwPedSpawn[7] = <<-661.9195, -1213.2766, 9.7043>>		serverBD.fMwPedSpawn[7] = 90.6966	
					serverBD.vMwPedSpawn[8] = <<-758.4667, -1332.7220, 8.6000>>		serverBD.fMwPedSpawn[8] = 226.0420	
					serverBD.vMwPedSpawn[9] = <<-597.2482, -1415.3240, 11.0923>>	serverBD.fMwPedSpawn[9] =  79.7294
			
					serverbd.iMaxOnFloorPeds = 4
				BREAK
				
				CASE 1 // Stoner Cement Works
					serverBD.vStartLocation[0] = <<307.4523, 2835.2383, 42.4361>>			serverBD.fStartHeading[0] = 122.9714
					serverBD.vStartLocation[1] =<<358.6979, 2863.1399, 42.0979>>  		serverBD.fStartHeading[1] = 118.6931 
					serverBD.vStartLocation[2] =<<356.6689, 2806.7412, 52.9669>>  		serverBD.fStartHeading[2] = 26.5272 
					serverBD.vStartLocation[3] =<<310.0119, 2875.7124, 42.5068>>  		serverBD.fStartHeading[3] = 211.0638
					
					serverBD.vMwPedSpawn[0] = <<346.1450, 2861.5652, 42.4603>>		serverBD.fMwPedSpawn[0] = 203.4084
					serverBD.vMwPedSpawn[1] = <<264.2803, 2872.2751, 42.6105>>		serverBD.fMwPedSpawn[1] = 123.7895
					serverBD.vMwPedSpawn[2] = <<290.4762, 2853.8899, 42.6424>>		serverBD.fMwPedSpawn[2] = 115.9053
					serverBD.vMwPedSpawn[3] = <<290.6060, 2810.9065, 47.9461>>		serverBD.fMwPedSpawn[3] = 330.5807
					serverBD.vMwPedSpawn[4] = <<341.4013, 2863.1812, 42.4361>>		serverBD.fMwPedSpawn[4] = 116.3110
					serverBD.vMwPedSpawn[5] = <<254.7670, 2880.2561, 42.5168>>		serverBD.fMwPedSpawn[5] = 145.8550
					serverBD.vMwPedSpawn[6] = <<301.1250, 2814.3276, 42.4361>>		serverBD.fMwPedSpawn[6] = 41.3530	
					serverBD.vMwPedSpawn[7] = <<306.2540, 2865.7981, 42.5755>>		serverBD.fMwPedSpawn[7] = 266.0158
					serverBD.vMwPedSpawn[8] = <<195.3600, 2808.6360, 44.3806>>		serverBD.fMwPedSpawn[8] = 290.3963
					serverBD.vMwPedSpawn[9] = <<269.1239, 2866.5085, 73.1797>>		serverBD.fMwPedSpawn[9] = 243.2495
					
					
					serverbd.iMaxOnFloorPeds = 4

				BREAK
				
				CASE 2 // Heist pickup locations
					serverBD.vStartLocation[0] = <<714.8820, 4175.2964, 39.7092>>			serverBD.fStartHeading[0] = 280.2862
					serverBD.vStartLocation[1] =<<765.1945, 4182.7959, 39.7152>>  		serverBD.fStartHeading[1] = 32.9157 
					serverBD.vStartLocation[2] =<<750.6640, 4238.8496, 54.7686>>  		serverBD.fStartHeading[2] = 106.2954
					serverBD.vStartLocation[3] =<<569.2263, 4178.3862, 37.2124>>  		serverBD.fStartHeading[3] = 272.3807
					
					serverBD.vMwPedSpawn[0] = <<724.4003, 4198.8877, 39.7092>>		serverBD.fMwPedSpawn[0] = 254.5411
					serverBD.vMwPedSpawn[1] = <<792.2949, 4184.1860, 39.5336>>		serverBD.fMwPedSpawn[1] = 183.2270
					serverBD.vMwPedSpawn[2] = <<754.1003, 4181.8291, 39.7131>>		serverBD.fMwPedSpawn[2] = 338.6039
					serverBD.vMwPedSpawn[3] = <<849.6327, 4224.7793, 49.5859>>		serverBD.fMwPedSpawn[3] = 71.8258
					serverBD.vMwPedSpawn[4] = <<856.3030, 4257.4663, 49.4800>>		serverBD.fMwPedSpawn[4] = 179.6626
					serverBD.vMwPedSpawn[5] = <<611.6555, 4222.1318, 52.4756>>		serverBD.fMwPedSpawn[5] = 272.7491
					serverBD.vMwPedSpawn[6] = <<643.0641, 4213.7539, 52.6579>>		serverBD.fMwPedSpawn[6] = 270.8252	
					serverBD.vMwPedSpawn[7] = <<665.9061, 4263.5132, 53.7219>>		serverBD.fMwPedSpawn[7] = 180.4937
					serverBD.vMwPedSpawn[8] = <<876.0669, 4247.9053, 48.9739>>		serverBD.fMwPedSpawn[8] = 107.4297
					serverBD.vMwPedSpawn[9] = <<509.1728, 4239.3579, 54.4925>>		serverBD.fMwPedSpawn[9] = 247.5728
					 

				//	serverbd.iMaxOnFloorPeds = 4
				BREAK
				
				CASE 3 // El gordo Lighthouse
					serverBD.vStartLocation[0] = <<3279.4973, 5204.4556, 17.3168>>			serverBD.fStartHeading[0] = 117.7311
					serverBD.vStartLocation[1] =<<3280.4561, 5166.3433, 17.9564>> 		serverBD.fStartHeading[1] =46.3958
					serverBD.vStartLocation[2] =<<3258.5093, 5225.8696, 17.2293>> 		serverBD.fStartHeading[2] =189.2091
					serverBD.vStartLocation[3] =<<3302.8962, 5136.2441, 17.3101>> 		serverBD.fStartHeading[3] =50.0778

					serverBD.vMwPedSpawn[0] = <<3318.2151, 5186.1089, 17.4503>>		serverBD.fMwPedSpawn[0] =  229.5714	
					serverBD.vMwPedSpawn[1] = <<3309.2542, 5167.5259, 17.4491>>		serverBD.fMwPedSpawn[1] =  136.4963	
					serverBD.vMwPedSpawn[2] = <<3372.6060, 5183.9141, 0.4652>>		serverBD.fMwPedSpawn[2] = 88.3908	
					serverBD.vMwPedSpawn[3] = <<3222.6841, 5187.7344, 30.0174>>		serverBD.fMwPedSpawn[3] =  271.8922	
					serverBD.vMwPedSpawn[4] = <<3217.3506, 5145.0298, 18.4178>>		serverBD.fMwPedSpawn[4] =  316.4374	
					serverBD.vMwPedSpawn[5] = <<3155.5615, 5285.2456, 28.0707>>		serverBD.fMwPedSpawn[5] =  246.1521	
					serverBD.vMwPedSpawn[6] = <<3311.9353, 5176.2471, 18.6196>>		serverBD.fMwPedSpawn[6] =  198.8985		
					serverBD.vMwPedSpawn[7] = <<3230.1418, 5200.8008, 22.8278>>		serverBD.fMwPedSpawn[7] =  257.2332	
					serverBD.vMwPedSpawn[8] = <<3420.0693, 5169.1333, 4.8574>>		serverBD.fMwPedSpawn[8] = 104.7808	
					serverBD.vMwPedSpawn[9] = <<3242.8682, 5035.1655, 20.0110>>		serverBD.fMwPedSpawn[9] =  339.2293
					
					/*	
						Other ped spawn  
						Player Position = <<2922.3525, 5050.2080, 29.4470>>, 252.5801  
					*/

					
				BREAK
				
				CASE 4 // Los Santos University
					serverBD.vStartLocation[0] = <<-1736.9886, 162.0228, 63.3710>>			serverBD.fStartHeading[0] = 306.7662
					serverBD.vStartLocation[1] =<<-1736.9382, 137.1991, 63.3710>> 		serverBD.fStartHeading[1] =303.3918
					serverBD.vStartLocation[2] =<<-1756.2722, 172.5584, 63.3711>> 		serverBD.fStartHeading[2] =302.3650
					serverBD.vStartLocation[3] =<<-1710.7477, 129.7035, 63.3716>> 		serverBD.fStartHeading[3] =304.9788
					
					serverBD.vMwPedSpawn[0] = <<-1674.8676, 141.7950, 62.4627>>		serverBD.fMwPedSpawn[0] = 30.9787		
					serverBD.vMwPedSpawn[1] = <<-1668.2645, 190.8298, 60.7573>>		serverBD.fMwPedSpawn[1] = 97.3626		
					serverBD.vMwPedSpawn[2] = <<-1661.1346, 236.6263, 61.3910>>		serverBD.fMwPedSpawn[2] = 218.2636	
					serverBD.vMwPedSpawn[3] = <<-1706.4431, 186.8354, 62.9277>>		serverBD.fMwPedSpawn[3] = 257.6324		
					serverBD.vMwPedSpawn[4] = <<-1722.3949, 227.3620, 60.7408>>		serverBD.fMwPedSpawn[4] = 244.6777		
					serverBD.vMwPedSpawn[5] = <<-1623.3435, 165.8969, 59.7796>>		serverBD.fMwPedSpawn[5] = 12.7450		
					serverBD.vMwPedSpawn[6] = <<-1627.1880, 242.7915, 58.6480>>		serverBD.fMwPedSpawn[6] = 196.3743			
					serverBD.vMwPedSpawn[7] = <<-1796.2153, 150.6246, 67.7735>>		serverBD.fMwPedSpawn[7] = 283.2556		
					serverBD.vMwPedSpawn[8] = <<-1774.0328, 105.8673, 69.3384>>		serverBD.fMwPedSpawn[8] = 243.4268		
					serverBD.vMwPedSpawn[9] = <<-1594.7167, 210.4007, 73.3379>>		serverBD.fMwPedSpawn[9] = 27.7876
					


					
					/*	
						Other ped spawn  
					Player Position = <<-1682.4446, 346.6223, 82.8029>>, 156.9911   
					Player Position = <<-1786.3470, 312.9610, 88.3714>>, 221.4491				  
					Player Position = <<-1721.0135, 224.9218, 60.7408>>, 232.5652  
					*/

					
				BREAK
			ENDSWITCH
		BREAK
		
		CASE BUZZARD
			serverBD.MwVehModel 	= BUZZARD
			
			serverbd.iWaveType[0] = WAVE_TYPE_AIRCRAFT_ONLY
			serverbd.iNumVehiclesInWave[0] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_AIRCRAFT_ONLY) //3
				
			serverbd.iWaveType[1] = WAVE_TYPE_GROUND_PEDS_AND_VEHS
			serverbd.iNumVehiclesInWave[1] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS) //3
			serverbd.iNumOnFootPedsInWave[1] = GET_NUMBER_OF_GROUND_PEDS_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS) // MAX_MW_ON_FOOT_PEDS 
			
			
			serverbd.iWaveType[2] = WAVE_TYPE_AIRCRAFT_ONLY
			serverbd.iNumVehiclesInWave[2] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_AIRCRAFT_ONLY) //3
			
			
			IF serverBD.iNumPlayerVeh <= 2
			OR NOT IS_COMPETITIVE_UW()
				
				
				serverbd.iWaveType[3] = WAVE_TYPE_BOSS
				serverbd.iNumVehiclesInWave[3] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_BOSS) //1
				
				serverbd.iNumOfWaves = 4
			ELIF serverBD.iNumPlayerVeh = 3
				serverbd.iWaveType[3] = WAVE_TYPE_GROUND_PEDS_AND_VEHS
				serverbd.iNumVehiclesInWave[3] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS) //3
				serverbd.iNumOnFootPedsInWave[3] = GET_NUMBER_OF_GROUND_PEDS_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS) // MAX_MW_ON_FOOT_PEDS 
				
				
				serverbd.iWaveType[4] = WAVE_TYPE_AIRCRAFT_ONLY
				serverbd.iNumVehiclesInWave[4] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_AIRCRAFT_ONLY)
				
				serverbd.iWaveType[5] = WAVE_TYPE_BOSS
				serverbd.iNumVehiclesInWave[5] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_BOSS)
				
				serverbd.iNumOfWaves = 6
			ELSE
				serverbd.iWaveType[3] = WAVE_TYPE_GROUND_PEDS_AND_VEHS
				serverbd.iNumVehiclesInWave[3] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS) //3
				serverbd.iNumOnFootPedsInWave[3] = GET_NUMBER_OF_GROUND_PEDS_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS) // MAX_MW_ON_FOOT_PEDS 
				
				
				serverbd.iWaveType[4] = WAVE_TYPE_AIRCRAFT_ONLY
				serverbd.iNumVehiclesInWave[4] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_AIRCRAFT_ONLY)
				
				serverbd.iWaveType[5] = WAVE_TYPE_GROUND_PEDS_AND_VEHS
				serverbd.iNumVehiclesInWave[5] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS) //3
				serverbd.iNumOnFootPedsInWave[5] = GET_NUMBER_OF_GROUND_PEDS_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS) 
				
				serverbd.iWaveType[6] = WAVE_TYPE_BOSS
				serverbd.iNumVehiclesInWave[6] = GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_BOSS)
				
				serverbd.iNumOfWaves = 7
			ENDIF
			
			serverBD.iKillGoal = GET_KILL_GOAL() //( MAX_MW_PEDS_PER_VEH * g_sMPTunables.iUrbanWarfareEnemyVehSpawns ) + g_sMPTunables.iUrbanWarfareEnemyOnFootSpawns
			
			IF NOT IS_COMPETITIVE_UW()
				serverbd.iHistorySubType = 4
			ELSE
				serverbd.iHistorySubType = 9
			ENDIF
			serverbd.iHistoryVariation = serverBD.iLocation
			
			SWITCH serverBD.iLocation 
				CASE 0  // Lester's waregouse
					serverBD.vStartLocation[0] =<<1139.9434, -1289.1946, 33.6127>>  		serverBD.fStartHeading[0] = 180.7904 
					serverBD.vStartLocation[1] =<<1156.3580, -1327.4016, 33.7312>>  		serverBD.fStartHeading[1] = 269.3001 
					serverBD.vStartLocation[2] =<<1204.9634, -1265.3337, 34.2267>>  		serverBD.fStartHeading[2] = 180.3900 
					serverBD.vStartLocation[3] =<<1220.9541, -1214.8834, 34.9485>>  		serverBD.fStartHeading[3] = 276.7351 
					
//					IF GET_CURRENT_WAVE_TYPE() = WAVE_TYPE_AIRCRAFT_ONLY
//					OR GET_CURRENT_WAVE_TYPE() = WAVE_TYPE_BOSS
//						serverBD.vMwVehSpawn[0] = <<1351.4414, -641.5107, 83.2342>>			serverBD.fMwVehSpawn[0] =  186.1368
//						serverBD.vMwVehSpawn[1] = <<636.2282, -434.6195, 33.6320>>			serverBD.fMwVehSpawn[1] =  203.6348  
//						serverBD.vMwVehSpawn[2] = <<352.4392, -590.8399, 83.1657>>			serverBD.fMwVehSpawn[2] =  209.9170   
//						serverBD.vMwVehSpawn[3] = <<375.2908, -1020.4328, 66.5363>>			serverBD.fMwVehSpawn[3] =  271.2396  
//						serverBD.vMwVehSpawn[4] = <<345.1868, -1426.6221, 85.1742>>			serverBD.fMwVehSpawn[4] =  284.6382
//					ELSE
//						serverBD.vMwVehSpawn[0] =  <<1248.4901, -1131.3379, 37.2874>>			serverBD.fMwVehSpawn[0] =   153.8083
//						serverBD.vMwVehSpawn[1] =  <<1396.4141, -1518.3896, 56.7703>>			serverBD.fMwVehSpawn[1] =   134.0967  
//						serverBD.vMwVehSpawn[2] =  <<972.8618, -1464.8285, 30.3558>>			serverBD.fMwVehSpawn[2] =  0.6090      
//						serverBD.vMwVehSpawn[3] =  <<935.6596, -909.6719, 39.5752>>				serverBD.fMwVehSpawn[3] =  271.1971  
//						serverBD.vMwVehSpawn[4] =  <<1156.6870, -768.8879, 56.4831>>			serverBD.fMwVehSpawn[4] =  275.3533
//					ENDIF
					
 

					
					serverBD.vMwPedSpawn[0] = <<1195.5559, -1277.9141, 34.3601>>		serverBD.fMwPedSpawn[0] = 329.9165	
					serverBD.vMwPedSpawn[1] = <<1197.8986, -1297.3856, 34.1985>>		serverBD.fMwPedSpawn[1] = 264.7702	
					serverBD.vMwPedSpawn[2] = <<1168.4347, -1382.3054, 33.7840>>		serverBD.fMwPedSpawn[2] = 359.5203	
					serverBD.vMwPedSpawn[3] = <<1160.7958, -1253.6257, 33.5659>>		serverBD.fMwPedSpawn[3] = 111.3101		
					serverBD.vMwPedSpawn[4] = <<1178.2004, -1272.3988, 33.8277>>		serverBD.fMwPedSpawn[4] = 176.3058		
					serverBD.vMwPedSpawn[5] = <<1185.2825, -1355.4935, 33.9506>>		serverBD.fMwPedSpawn[5] = 213.1969	
					serverBD.vMwPedSpawn[6] = <<1217.7041, -1381.6592, 34.3114>>		serverBD.fMwPedSpawn[6] = 275.2275			
					serverBD.vMwPedSpawn[7] = <<1193.3881, -1257.5640, 34.2036>>		serverBD.fMwPedSpawn[7] = 220.0349		
					serverBD.vMwPedSpawn[8] = <<1174.8335, -1381.0643, 41.8297>>		serverBD.fMwPedSpawn[8] = 359.1500		
					serverBD.vMwPedSpawn[9] = <<1154.6473, -1338.6132, 33.7034>>		serverBD.fMwPedSpawn[9] = 83.4212
					


					
				BREAK
				
				CASE 1 // Rockfod Plaza
					serverBD.vStartLocation[0] =<<129.1966, -368.0656, 42.2570>>  			serverBD.fStartHeading[0] =  247.4892 
					serverBD.vStartLocation[1] =<<147.9662, -363.0619, 42.2570>> 			serverBD.fStartHeading[1] =  66.1290 
					serverBD.vStartLocation[2] =<<116.6446, -436.4488, 40.1294>> 			serverBD.fStartHeading[2] =  340.0713 
					serverBD.vStartLocation[3] =<<90.0879, -385.0907, 40.2506>> 			serverBD.fStartHeading[3] = 245.7466


					
//					IF GET_CURRENT_WAVE_TYPE() = WAVE_TYPE_AIRCRAFT_ONLY
//					OR GET_CURRENT_WAVE_TYPE() = WAVE_TYPE_BOSS
//						serverBD.vMwVehSpawn[0] = <<-137.5082, -168.3033, 102.7024>>		serverBD.fMwVehSpawn[0] =   160.3978
//						serverBD.vMwVehSpawn[1] = <<-174.8280, -214.6927, 56.6312>>			serverBD.fMwVehSpawn[1] =   258.2675  
//						serverBD.vMwVehSpawn[2] = <<404.7129, -24.3335, 169.3952>>			serverBD.fMwVehSpawn[2] =  95.0683     
//						serverBD.vMwVehSpawn[3] = <<168.3323, 668.6285, 215.7082>>			serverBD.fMwVehSpawn[3] =  172.5901   
//						serverBD.vMwVehSpawn[4] = <<-608.7920, 660.9065, 165.6164>>			serverBD.fMwVehSpawn[4] =   207.7868
//					ELSE
//						serverBD.vMwVehSpawn[0] = <<-166.1217, -214.9006, 45.8637>>			serverBD.fMwVehSpawn[0] =  257.5103
//						serverBD.vMwVehSpawn[1] = <<33.7482, -95.8814, 55.3626>>			serverBD.fMwVehSpawn[1] =  73.0413   
//						serverBD.vMwVehSpawn[2] = <<36.5680, 47.4427, 71.3139>>				serverBD.fMwVehSpawn[2] = 158.1279    
//						serverBD.vMwVehSpawn[3] = <<-501.5253, -67.3569, 38.6901>>			serverBD.fMwVehSpawn[3] = 155.0279   
//						serverBD.vMwVehSpawn[4] = <<-430.6903, -423.6864, 31.7928>>			serverBD.fMwVehSpawn[4] =  352.9770
//					ENDIF
					

					
					serverBD.vMwPedSpawn[0] = <<47.0236, -401.9681, 38.9218>> 		serverBD.fMwPedSpawn[0] =  252.6017		
					serverBD.vMwPedSpawn[1] = <<36.9340, -438.6162, 38.9206>> 		serverBD.fMwPedSpawn[1] =  245.2620		
					serverBD.vMwPedSpawn[2] = <<69.7929, -461.2950, 38.9215>> 		serverBD.fMwPedSpawn[2] =  44.7945		
					serverBD.vMwPedSpawn[3] = <<48.4389, -397.2337, 54.2900>> 		serverBD.fMwPedSpawn[3] =  255.6518 			
					serverBD.vMwPedSpawn[4] = <<204.0561, -439.3400, 41.9848>>		serverBD.fMwPedSpawn[4] =  124.8391			
					serverBD.vMwPedSpawn[5] = <<142.2612, -291.4343, 45.3084>>		serverBD.fMwPedSpawn[5] =  188.9979		
					serverBD.vMwPedSpawn[6] = <<163.2065, -291.4264, 45.1553>>		serverBD.fMwPedSpawn[6] =  159.3613				
					serverBD.vMwPedSpawn[7] = <<115.7637, -459.3909, 40.1269>>		serverBD.fMwPedSpawn[7] =  5.7762			
					serverBD.vMwPedSpawn[8] = <<76.8378, -337.4840, 66.2022>>		serverBD.fMwPedSpawn[8] =  89.3388			
					serverBD.vMwPedSpawn[9] = <<134.5330, -346.3627, 101.6362>>		serverBD.fMwPedSpawn[9] =  85.9158
					


					
					/*
						Other ped spawn  
					Player Position = <<-352.5666, -226.6665, 36.2891>>, 35.2190   
					Player Position = <<-376.3163, -454.6380, 29.8139>>, 281.4965 
					Player Position = <<31.6746, -374.1486, 72.9457>>, 79.2721   
					Player Position = <<15.4054, -244.6372, 46.6656>>, 226.5208  
					Player Position = <<-105.5749, -65.3772, 55.3657>>, 116.2859   
					
					*/


				BREAK
				
				CASE 2 // Observatory outlook
					serverBD.vStartLocation[0] = <<-509.9767, 1186.8542, 323.8415>>     	serverBD.fStartHeading[0] = 310.6241 
					serverBD.vStartLocation[1] =<<-398.0129, 1230.5077, 324.6416>>  		serverBD.fStartHeading[1] = 166.0191 
					serverBD.vStartLocation[2] =<<-455.6342, 1141.9984, 324.9044>>  		serverBD.fStartHeading[2] = 343.1715 
					serverBD.vStartLocation[3] =<<-412.4245, 1133.0317, 324.9044>>  		serverBD.fStartHeading[3] = 342.4984
					
//					IF GET_CURRENT_WAVE_TYPE() = WAVE_TYPE_AIRCRAFT_ONLY
//					OR GET_CURRENT_WAVE_TYPE() = WAVE_TYPE_BOSS
//						serverBD.vMwVehSpawn[0] = <<-468.3117, 2034.2810, 225.8829>>		serverBD.fMwVehSpawn[0] =  183.5655
//						serverBD.vMwVehSpawn[1] = <<-1572.2275, 838.9957, 193.9097>>		serverBD.fMwVehSpawn[1] =  278.9509 
//						serverBD.vMwVehSpawn[2] = <<-175.6009, -162.3506, 102.7024>>		serverBD.fMwVehSpawn[2] = 12.7537    
//						serverBD.vMwVehSpawn[3] = <<494.7073, 706.4063, 205.6965>>			serverBD.fMwVehSpawn[3] = 75.8614   
//						serverBD.vMwVehSpawn[4] = <<821.3636, 1308.9093, 372.5396>>			serverBD.fMwVehSpawn[4] = 113.4670 
//					ELSE
//						serverBD.vMwVehSpawn[0] =<<-803.5167, 1295.3257, 257.4832>>			serverBD.fMwVehSpawn[0] =  209.5666
//						serverBD.vMwVehSpawn[1] =<<-925.5961, 1124.7699, 218.7065>>			serverBD.fMwVehSpawn[1] =  215.2912
//						serverBD.vMwVehSpawn[2] =<<-404.5761, 833.0574, 224.6128>>			serverBD.fMwVehSpawn[2] = 218.3907  
//						serverBD.vMwVehSpawn[3] =<<-303.0683, 1010.3021, 232.2905>>			serverBD.fMwVehSpawn[3] =  78.2628 
//						serverBD.vMwVehSpawn[4] =<<56.7959, 1080.6508, 220.6167>> 			serverBD.fMwVehSpawn[4] = 18.8188  
//					ENDIF
					

					serverBD.vMwPedSpawn[0] =	<<-426.3299, 1216.6597, 324.7585>>	serverBD.fMwPedSpawn[0] =  248.8594		
					serverBD.vMwPedSpawn[1] =	<<-363.2418, 1266.7606, 329.8745>>	serverBD.fMwPedSpawn[1] =  195.7758		
					serverBD.vMwPedSpawn[2] =	<<-453.9799, 1074.9916, 326.6897>>	serverBD.fMwPedSpawn[2] =  28.6127			
					serverBD.vMwPedSpawn[3] =	<<-392.4850, 1078.5618, 323.2558>>	serverBD.fMwPedSpawn[3] =  296.7476 			
					serverBD.vMwPedSpawn[4] =	<<-413.6003, 1086.8307, 326.6821>>	serverBD.fMwPedSpawn[4] =  240.9422			
					serverBD.vMwPedSpawn[5] =	<<-381.0179, 1141.2689, 321.6503>>	serverBD.fMwPedSpawn[5] =  2.4730			
					serverBD.vMwPedSpawn[6] =	<<-482.3448, 1118.6459, 319.0963>>	serverBD.fMwPedSpawn[6] =  343.6776				
					serverBD.vMwPedSpawn[7] =	<<-397.3531, 1127.5273, 321.7288>>	serverBD.fMwPedSpawn[7] =  0.2495			
					serverBD.vMwPedSpawn[8] =	<<-435.3902, 1092.1620, 331.5411>>	serverBD.fMwPedSpawn[8] =  72.7889				
					serverBD.vMwPedSpawn[9] =	<<-424.5132, 1225.0759, 324.7585>>	serverBD.fMwPedSpawn[9] =  238.4196
					

					/* 
						Other ped spawn
						
						Player Position = <<-390.5703, 1079.4224, 323.3598>>, 320.0006   
						Player Position = <<-456.6933, 1093.8395, 326.6821>>, 66.7556   
						Player Position = <<-427.7571, 1212.6632, 324.7585>>, 250.0823   
					*/
					

				BREAK
				
				CASE 3 // Sandy shores
					serverBD.vStartLocation[0] = <<341.0539, 3569.7888, 32.4743>>        	serverBD.fStartHeading[0] = 351.0324 
					serverBD.vStartLocation[1] = <<373.5018, 3512.3215, 33.1732>>  			serverBD.fStartHeading[1] = 3.5452 
					serverBD.vStartLocation[2] = <<353.5068, 3483.8394, 34.0848>>  			serverBD.fStartHeading[2] = 7.3237 
					serverBD.vStartLocation[3] = <<245.8774, 3580.8813, 32.9612>>  			serverBD.fStartHeading[3] = 332.0844
					
					serverBD.vMwPedSpawn[0] =	<<447.4214, 3566.3169, 32.2386>>	serverBD.fMwPedSpawn[0] =   2.6416		
					serverBD.vMwPedSpawn[1] =	<<445.3309, 3512.0876, 32.9825>>	serverBD.fMwPedSpawn[1] =   171.8638	
					serverBD.vMwPedSpawn[2] =	<<353.2316, 3396.2119, 35.4033>>	serverBD.fMwPedSpawn[2] =   257.0332		
					serverBD.vMwPedSpawn[3] =	<<454.5527, 3530.2419, 32.4254>>	serverBD.fMwPedSpawn[3] =   91.7869				
					serverBD.vMwPedSpawn[4] =	<<395.8559, 3577.0251, 32.2922>>	serverBD.fMwPedSpawn[4] =   4.2016			
					serverBD.vMwPedSpawn[5] =	<<380.2437, 3560.9915, 32.3420>>	serverBD.fMwPedSpawn[5] =   339.7290		
					serverBD.vMwPedSpawn[6] =	<<408.4627, 3458.3130, 32.6961>>	serverBD.fMwPedSpawn[6] =   359.2033			
					serverBD.vMwPedSpawn[7] =	<<476.5523, 3557.3276, 32.2386>>	serverBD.fMwPedSpawn[7] =   333.6298		
					serverBD.vMwPedSpawn[8] =	<<325.6323, 3390.6663, 35.4033>>	serverBD.fMwPedSpawn[8] =   73.2788				
					serverBD.vMwPedSpawn[9] =	<<461.6116, 3535.2573, 32.3180>>	serverBD.fMwPedSpawn[9] =   96.8181
					
					
  					
					/*
						Other ped spawn					
						Player Position = <<49.7837, 3630.8267, 38.6720>>, 231.0595  
						Player Position = <<439.0486, 3442.5535, 50.4609>>, 78.1103   
					*/

				BREAK
				
				CASE 4 // Madrazo Ranch
					serverBD.vStartLocation[0] = <<1493.1655, 1177.5554, 113.2209>>     serverBD.fStartHeading[0] = 42.1535 
					serverBD.vStartLocation[1] = <<1433.4757, 1179.5436, 113.1842>>  	serverBD.fStartHeading[1] = 315.0261
					serverBD.vStartLocation[2] = <<1471.4857, 1108.9673, 113.3343>>  	serverBD.fStartHeading[2] = 1.2527
					serverBD.vStartLocation[3] = <<1448.9137, 1110.6216, 113.3363>>  	serverBD.fStartHeading[3] = 1.9403
					
					
					serverBD.vMwPedSpawn[0] =	<<1485.6221, 1047.3883, 113.3340>>	serverBD.fMwPedSpawn[0] =   352.9674	
					serverBD.vMwPedSpawn[1] =	<<1461.5406, 1083.6578, 113.3344>>	serverBD.fMwPedSpawn[1] =   88.8285		
					serverBD.vMwPedSpawn[2] =	<<1411.8738, 1126.9602, 113.3341>>	serverBD.fMwPedSpawn[2] =   177.2297		
					serverBD.vMwPedSpawn[3] =	<<1442.4966, 1148.4688, 113.3342>>	serverBD.fMwPedSpawn[3] =   273.1487			
					serverBD.vMwPedSpawn[4] =	<<1406.4763, 1256.6952, 106.6555>>	serverBD.fMwPedSpawn[4] =   225.3940		
					serverBD.vMwPedSpawn[5] =	<<1464.0770, 1134.8076, 113.3227>>	serverBD.fMwPedSpawn[5] =   175.5423		
					serverBD.vMwPedSpawn[6] =	<<1410.3141, 1162.3165, 113.3342>>	serverBD.fMwPedSpawn[6] =   191.1842 			
					serverBD.vMwPedSpawn[7] =	<<1487.4830, 1102.2426, 113.3346>>	serverBD.fMwPedSpawn[7] =   269.6831		
					serverBD.vMwPedSpawn[8] =	<<1391.0343, 1154.2233, 113.4433>>	serverBD.fMwPedSpawn[8] =   29.4203				
					serverBD.vMwPedSpawn[9] =	<<1484.8704, 1039.5530, 113.2318>>	serverBD.fMwPedSpawn[9] =   287.6770
 					
					/*
						Other ped spawn pos
						Player Position = <<1242.0621, 1233.6487, 144.1610>>, 258.0149   
					*/

				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH

	PRINTLN("     ---------->     KILL LIST - GET_RANDOM_LOCATIONS - vStartLocation[0] = ", serverBD.vStartLocation[0], " fStartHeading[0] = ", serverBD.fStartHeading[0], " iLocation = ", serverBD.iLocation, " serverBD.iKillGoal = ", serverBD.iKillGoal, " serverBD.iNumPlayerVeh = ",  serverBD.iNumPlayerVeh, " serverbd.iNumOfWaves = ",serverbd.iNumOfWaves, " Competitive = ", IS_COMPETITIVE_UW())
	PRINTLN("     ---------->     KILL LIST - GET_RANDOM_LOCATIONS -  serverbd.iHistorySubType = ", serverbd.iHistorySubType, " serverbd.iHistoryVariation = ", serverbd.iHistoryVariation) 
ENDPROC



FUNC BOOL ARE_PLAYER_VEH_SPAWN_POINTS_OK()
	INT iNumSpawnPoints = GET_NUMBER_OF_UW_PLAYER_VEHICLES()
	INT i
	
	#IF IS_DEBUG_BUILD
		IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_forceUWspawnCheck")
			IF MPGlobalsAmbience.iUrbanWarfareLocation > -1
				PRINTLN("     ---------->     KILL LIST [ARE_PLAYER_VEH_SPAWN_POINTS_OK] TRUE AS DEBUG LAUNCHED")
				RETURN TRUE
			ENDIF
		ENDIF
	#ENDIF
	
	PRINTLN("     ---------->     KILL LIST [ARE_PLAYER_VEH_SPAWN_POINTS_OK] iNumSpawnPoints = ",iNumSpawnPoints)
	REPEAT iNumSpawnPoints i
	
		IF Expensive_Is_Vector_Near_An_Active_Gang_Attack(serverBD.vStartLocation[i])
			PRINTLN("     ---------->     KILL LIST [ARE_PLAYER_VEH_SPAWN_POINTS_OK] FAILING (Expensive_Is_Vector_Near_An_Active_Gang_Attack) serverBD.vStartLocation[",i, "] = ", serverBD.vStartLocation[i])
			RETURN FALSE
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_forceUWspawnCheck")
				IF MPGlobalsAmbience.iUrbanWarfareLocation > -1
					PRINTLN("     ---------->     KILL LIST [ARE_PLAYER_VEH_SPAWN_POINTS_OK] TRUE AS DEBUG LAUNCHED")
					RETURN TRUE
				ENDIF
			ENDIF
		#ENDIF
		
		IF NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION(	serverBD.vStartLocation[i], // VECTOR vPoint, 
														DEFAULT, 						 // FLOAT fVehRadius = 6.0, 
														DEFAULT, 						 // FLOAT fPedRadius = 1.0, 
														DEFAULT,						 // FLOAT fObjRadius = 1.0,
														DEFAULT,						 // FLOAT fViewRadius = 5.0,
														DEFAULT,						 // BOOL bCheckThisPlayerSight = TRUE,
														DEFAULT,						 // BOOL bDoAnyPlayerSeePointCheck = TRUE,
														DEFAULT,						 // BOOL bCheckVisibilityForOwnTeam = TRUE,
														DEFAULT,					 // FLOAT fVisibleDistance = 120.0,
														DEFAULT, 						 // BOOL bIgnoreLocalPlayerChecks=FALSE, 
														DEFAULT,						 // INT iTeamToIgnore=-1,
														DEFAULT,						 // BOOL bDoFireCheck=TRUE,
														50.0) 						// FLOAT fPlayerRadius = 0.0,
														
														
			PRINTLN("     ---------->     KILL LIST [ARE_PLAYER_VEH_SPAWN_POINTS_OK] FAILING serverBD.vStartLocation[",i, "] = ", serverBD.vStartLocation[i])
			RETURN FALSE

		ENDIF
		
	ENDREPEAT
	RETURN TRUE
ENDFUNC

//PURPOSE: Sets the data for this instance of Destroy Veh
FUNC BOOL GET_RANDOM_SETUP_DETAILS()
	IF GET_PLAYER_VEHICLE_MODEL(serverBD.VehModel)
		IF serverBD.bOpenAirportGates		
			UNLOCK_AIRPORT_GATES()
		ENDIF
		GET_RANDOM_LOCATION()
		RETURN TRUE
	ELSE
		PRINTLN("     ---------->     KILL LIST - GET_RANDOM_SETUP_DETAILS WAITING FOR GET_PLAYER_VEHICLE_MODEL")
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Returns the Cash reward for completing the objective
FUNC INT GET_CASH_REWARD(BOOL bForHelp = FALSE)
	IF NOT IS_COMPETITIVE_UW()
		RETURN ROUND(TO_FLOAT(g_sMPTunables.iKILL_LIST_DEFAULT_CASH) * g_sMPTunables.fKill_List_Event_Multiplier_Cash)         
	ELSE
		IF bForHelp
			RETURN ROUND(TO_FLOAT(g_sMPTunables.iCOMP_KILL_LIST_DEFAULT_CASH) * g_sMPTunables.fKill_List_Competitive_Event_Multiplier_Cash)
		ELSE
			IF (IS_BIT_SET(serverBD.iServerBitSet, biS_AllVehiclesDestroyed)
			AND IS_BIT_SET(serverBD.iServerBitSet, biS_AllVehiclePedsDead)
			AND IS_BIT_SET(serverBD.iServerBitSet, biS_AllOnFootPedsDead))
				PRINTLN("     ---------->     KILL LIST - [CHECK_FOR_REWARD] [GET_CASH_REWARD] FULL AMOUNT AS IS_COMPETITIVE AND ALL TARGETS KILLED")
				RETURN ROUND(TO_FLOAT(g_sMPTunables.iCOMP_KILL_LIST_DEFAULT_CASH) * g_sMPTunables.fKill_List_Competitive_Event_Multiplier_Cash)
			ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_DurationExpired)
				PRINTLN("     ---------->     KILL LIST - [CHECK_FOR_REWARD] [GET_CASH_REWARD] FULL AMOUNT AS IS_COMPETITIVE AND DURATION EXPIRED")
				RETURN ROUND(TO_FLOAT(g_sMPTunables.iCOMP_KILL_LIST_DEFAULT_CASH) * g_sMPTunables.fKill_List_Competitive_Event_Multiplier_Cash)
			ELSE
				PRINTLN("     ---------->     KILL LIST - [CHECK_FOR_REWARD] [GET_CASH_REWARD] RETURNING 0 AS DURATION HASN'T EXPIRED, AND DIDN'T KILL EVERYONE")
				RETURN 0
			ENDIF
		ENDIF
	ENDIF
ENDFUNC


PROC INIT_MY_PARTICIPATION_TIME_MULTIPLIER()
	FLOAT fPartTime
	
	IF iMyPartiticpationTime <> 0
		EXIT
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(timeMyParticipation)
		iMyPartiticpationTime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(timeMyParticipation)
	ENDIF

	
	PRINTLN("     ---------->     KILL LIST - [GET_PARTICIPATION_CASH] [INIT_MY_PARTICIPATION_TIME_MULTIPLIER] iMyPartiticpationTime (ms) = ", iMyPartiticpationTime)
	
	fPartTime = TO_FLOAT(iMyPartiticpationTime)
	fPartTime /= 60000 
	
	iMyPartiticpationTime = FLOOR(fPartTime) 
	
	
	
	IF iMyPartiticpationTime >= 1
		PRINTLN("     ---------->     KILL LIST - [GET_PARTICIPATION_CASH] [INIT_MY_PARTICIPATION_TIME_MULTIPLIER] iMyPartiticpationTime (min) = ", iMyPartiticpationTime)
	ELSE
		iMyPartiticpationTime = 1
		PRINTLN("     ---------->     KILL LIST - [GET_PARTICIPATION_CASH] [INIT_MY_PARTICIPATION_TIME_MULTIPLIER] iMyPartiticpationTime Was < 1 (min) = ", iMyPartiticpationTime)
	ENDIF
	
	IF iMyPartiticpationTime > g_sMPTunables.iParticipation_T_Cap                         
		iMyPartiticpationTime = g_sMPTunables.iParticipation_T_Cap
		PRINTLN("     ---------->     KILL LIST - [GET_PARTICIPATION_CASH] [INIT_MY_PARTICIPATION_TIME_MULTIPLIER] SET iMyPartiticpationTime TO TUNABLE CAP AS iParticipation_T_Cap = ", g_sMPTunables.iParticipation_T_Cap)
	ENDIF
	
ENDPROC 

FUNC INT GET_PARTICIPATION_CASH(BOOL bScaleForTIme = TRUE)
	PRINTLN("     ---------->     KILL LIST - [GET_PARTICIPATION_CASH] bScaleForTIme = ", bScaleForTIme)
	
	
	IF IS_BIT_SET(iBoolsBitSet2, bi2_AlreadyDonePartCashAndRp)
		PRINTLN("     ---------->     KILL LIST - [GET_PARTICIPATION_CASH] 0 as bi2_AlreadyDonePartCashAndRp")
		RETURN 0
	ENDIF
//	IF IS_BIT_SET(iBoolsBitSet2, bi2_TooFewPlayersInVeh)
//		PRINTLN("     ---------->     KILL LIST - [GET_PARTICIPATION_CASH] 0 as bi2_TooFewPlayersInVeh")
//		RETURN 0
//	ENDIF
//	
	INT iMyPartCash
	IF NOT bScaleForTIme
		IF IS_COMPETITIVE_UW()
			RETURN g_sMPTunables.iCOMP_KILL_LIST_MINIMUM_PARTICIPATION_CASH //1000
		ELSE
			RETURN g_sMPTunables.iKILL_LIST_MINIMUM_PARTICIPATION_CASH          
		ENDIF
	ENDIF
		
	INIT_MY_PARTICIPATION_TIME_MULTIPLIER()
	
	IF IS_COMPETITIVE_UW()
		iMyPartCash = g_sMPTunables.iCOMP_KILL_LIST_MINIMUM_PARTICIPATION_CASH * iMyPartiticpationTime
	ELSE
		iMyPartCash = g_sMPTunables.iKILL_LIST_MINIMUM_PARTICIPATION_CASH * iMyPartiticpationTime     
	ENDIF
	
	PRINTLN("     ---------->     KILL LIST - [GET_PARTICIPATION_CASH]  iMyPartCash = ", iMyPartCash, " iMyPartiticpationTime = ", iMyPartiticpationTime)
	
	RETURN iMyPartCash

ENDFUNC

FUNC INT GET_KILL_CAP()
	IF IS_COMPETITIVE_UW()
		RETURN g_sMPTunables.iCOMP_KILL_LIST_AI_ENEMY_KILL_CAP
	ELSE
		RETURN g_sMPTunables.iKILL_LIST_ENEMY_KILL_CAP     
	ENDIF
ENDFUNC

FUNC INT GET_TARGET_KILL_RP_REWARD()
//	RETURN 50
	IF IS_COMPETITIVE_UW()
		RETURN g_sMPTunables.iCOMP_KILL_LIST_AI_ENEMY_KILL_RP
	ELSE
		RETURN g_sMPTunables.iKILL_LIST_ENEMY_KILL_RP     
	ENDIF
ENDFUNC

FUNC INT GET_PARTICIPATION_RP(BOOL bScaleForTIme = TRUE)
	PRINTLN("     ---------->     KILL LIST - [GET_PARTICIPATION_RP] bScaleForTIme = ", bScaleForTIme)
	
	INT iMyPartRp
	
	IF IS_BIT_SET(iBoolsBitSet2, bi2_AlreadyDonePartCashAndRp)
		PRINTLN("     ---------->     KILL LIST - [GET_PARTICIPATION_RP] 0 as bi2_AlreadyDonePartCashAndRp")
		RETURN 0
	ENDIF
	
//	IF IS_BIT_SET(iBoolsBitSet2, bi2_TooFewPlayersInVeh)
//		PRINTLN("     ---------->     KILL LIST - [GET_PARTICIPATION_RP] 0 as bi2_TooFewPlayersInVeh")
//		RETURN 0
//	ENDIF
	
	IF NOT bScaleForTIme
		IF IS_COMPETITIVE_UW()
			RETURN g_sMPTunables.iCOMP_KILL_LIST_MINIMUM_PARTICIPATION_RP //100
		ELSE
			RETURN g_sMPTunables.iKILL_LIST_MINIMUM_PARTICIPATION_RP  
		ENDIF
	ENDIF
	
	INIT_MY_PARTICIPATION_TIME_MULTIPLIER()
	
	IF IS_COMPETITIVE_UW()
		iMyPartRp =  g_sMPTunables.iCOMP_KILL_LIST_MINIMUM_PARTICIPATION_RP * iMyPartiticpationTime
	ELSE
		iMyPartRp =  g_sMPTunables.iKILL_LIST_MINIMUM_PARTICIPATION_RP  * iMyPartiticpationTime
	ENDIF
		
	PRINTLN("     ---------->     KILL LIST - [GET_PARTICIPATION_CASH]  iMyPartRp = ", iMyPartRp, " iMyPartiticpationTime = ", iMyPartiticpationTime)
	
	RETURN iMyPartRp
ENDFUNC

FUNC INT GET_RP_REWARD()
	IF NOT IS_COMPETITIVE_UW()
		RETURN ROUND((TO_FLOAT(g_sMPTunables.iKILL_LIST_DEFAULT_RP) * g_sMPTunables.fKill_List_Event_Multiplier_RP))      //1500
	ELSE
	//	RETURN 2500
		IF (IS_BIT_SET(serverBD.iServerBitSet, biS_AllVehiclesDestroyed)
		AND IS_BIT_SET(serverBD.iServerBitSet, biS_AllVehiclePedsDead)
		AND IS_BIT_SET(serverBD.iServerBitSet, biS_AllOnFootPedsDead))
			PRINTLN("     ---------->     KILL LIST - [CHECK_FOR_REWARD] [GET_RP_REWARD] FULL AMOUNT AS IS_COMPETITIVE AND ALL TARGETS KILLED")
			RETURN ROUND((TO_FLOAT(g_sMPTunables.iCOMP_KILL_LIST_DEFAULT_RP) * g_sMPTunables.fKill_List_Competitive_Event_Multiplier_RP))
		ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_DurationExpired)
			PRINTLN("     ---------->     KILL LIST - [CHECK_FOR_REWARD] [GET_RP_REWARD] FULL AMOUNT AS IS_COMPETITIVE AND DURATION EXPIRED")
			RETURN ROUND((TO_FLOAT(g_sMPTunables.iCOMP_KILL_LIST_DEFAULT_RP) * g_sMPTunables.fKill_List_Competitive_Event_Multiplier_RP))
		ELSE
			PRINTLN("     ---------->     KILL LIST - [CHECK_FOR_REWARD] [GET_RP_REWARD] RETURNING 0 AS DURATION HASN'T EXPIRED, AND DIDN'T KILL EVERYONE")
			RETURN 0	
		ENDIF
	ENDIF
ENDFUNC
PROC SHOULD_RUN_COMPETITIVE()
//	IF MPGlobalsAmbience.iUrbanWarfareLocation > -1
//		IF MPGlobalsAmbience.bIsCompetitive
//			SET_BIT(serverBD.iServerBitSet, biS_Competitive)
//			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     KILL LIST - SET AS COMPETITIVE AS GLOBAL SET")
//		ELSE
//			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     KILL LIST - NOT SETTING AS COMPETITIVE - FROM MENU")
//		ENDIF
//	ELSE
//		INT iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
//		IF iRand = 0
//			SET_BIT(serverBD.iServerBitSet, biS_Competitive)
//			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     KILL LIST - SET AS COMPETITIVE AS RANDOM CHACE SAYS SO")
//		ELSE
//			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     KILL LIST - NOT SETTING AS COMPETITIVE")
//		ENDIF
//	ENDIF
ENDPROC

FUNC BOOL GET_NUMBER_OF_PLAYERS_FOR_LAUNCH_TYPE(INT &iNumPlayers)
	INT iCount
	IF NOT HAS_NET_TIMER_STARTED(serverBD.timeChooseMode)
		START_NET_TIMER(serverBD.timeChooseMode)
	ELSE
		IF HAS_NET_TIMER_EXPIRED(serverBD.timeChooseMode, 3000)
			REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iCount
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iCount))
					IF NOT IS_PLAYER_SCTV(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iCount)))
						IF NOT IS_BIT_SET(playerBD[iCount].iPlayerBitSet ,biP_DisabledMode)
							iNumPlayers++
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	 "    ---------->     KILL LIST [GOT_UW_TYPE] [GET_NUMBER_OF_PLAYERS_FOR_LAUNCH_TYPE] FINISHED COUNTING PLAYERS iNumPlayers = ", iNumPlayers)
			
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Determine, based on the number of players, whether or not the competitive variation should run
FUNC BOOL GOT_UW_TYPE()
	INT iNumPlayers
	
	IF NOT HAS_NET_TIMER_STARTED(serverBD.timeInitialWait)
		START_NET_TIMER(serverBD.timeInitialWait)
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     KILL LIST [GOT_UW_TYPE] STARTED timeInitialWait")
		RETURN FALSE
	ELIF NOT HAS_NET_TIMER_EXPIRED(serverBD.timeInitialWait, 3000)
		RETURN FALSE
	ENDIF
	
	
	
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_GotUWType)
		#IF IS_DEBUG_BUILD
		IF MPGlobalsAmbience.iUrbanWarfareLocation > -1
	
			//-- Chosen from debug menu
			IF MPGlobalsAmbience.bIsCompetitive
				SET_BIT(serverBD.iServerBitSet, biS_Competitive)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     KILL LIST [GOT_UW_TYPE] - SET AS COMPETITIVE AS GLOBAL SET")
			ELSE
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     KILL LIST [GOT_UW_TYPE] - NOT SETTING AS COMPETITIVE - FROM MENU")
			ENDIF
			
			SET_BIT(serverBD.iServerBitSet, biS_GotUWType)
		ELSE
		#ENDIF
			IF GET_NUMBER_OF_PLAYERS_FOR_LAUNCH_TYPE(iNumPlayers)
				IF iNumPlayers < GET_MINIMUM_PARTICIPANTS_FOR_COMPETITIVE_VARIATION() //10
					//-- Less than 10 valid players, definitely picking non-competitive
					SET_BIT(serverBD.iServerBitSet, biS_GotUWType)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     KILL LIST [GOT_UW_TYPE] - NOT SETTING AS COMPETITIVE < GET_MINIMUM_PARTICIPANTS_FOR_COMPETITIVE_VARIATION PLAYERS- iNumPlayers = ", iNumPlayers)
				ELSE
					//-- 10 or more players, pick a type at random
					INT iRand = GET_RANDOM_INT_IN_RANGE(0, 100)
					//If it's 0 and it's not disabled then it's competative
					IF (iRand < ((100/2) * g_sMPTunables.fcompetitive_Kill_List_Weighthing_Sub_Type)
					AND (NOT g_sMPTunables.bcompetitive_Kill_List_Disable #IF IS_DEBUG_BUILD OR g_AML_serverBD.bLaunchedFromMissionMenuForScript #ENDIF))
					//normal is disabled
					OR (g_sMPTunables.bKILL_LIST_Disable #IF IS_DEBUG_BUILD AND NOT g_AML_serverBD.bLaunchedFromMissionMenuForScript #ENDIF)
						SET_BIT(serverBD.iServerBitSet, biS_Competitive)
						SET_BIT(serverBD.iServerBitSet, biS_GotUWType)
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     KILL LIST [GOT_UW_TYPE] - SETTING AS COMPETITIVE iNumPlayers = ", iNumPlayers)
					ELSE
						SET_BIT(serverBD.iServerBitSet, biS_GotUWType)
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     KILL LIST [GOT_UW_TYPE] - NOT SETTING AS COMPETITIVE - iNumPlayers = ")
					ENDIF
				ENDIF
			ENDIF
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF

	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_GotUWType)
		IF serverBD.iNumPlayerVeh = 0	
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     KILL LIST [GOT_UW_TYPE] FALSE  serverBD.iNumPlayerVeh = 0")
			GET_NUMBER_OF_UW_PLAYER_VEHICLES()
			RETURN FALSE
		ENDIF
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SetupComplete)
			IF GET_RANDOM_SETUP_DETAILS()
				IF NOT FM_EVENT_IS_VARIATION_IN_HISTORY_LIST(FMMC_TYPE_KILL_LIST, serverbd.iHistorySubType, serverbd.iHistoryVariation)
			#IF IS_DEBUG_BUILD OR MPGlobalsAmbience.iUrbanWarfareLocation > -1 #ENDIF
					IF ARE_PLAYER_VEH_SPAWN_POINTS_OK()
						SET_BIT(serverBD.iServerBitSet, biS_SetupComplete)
					ELSE
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     KILL LIST [GOT_UW_TYPE] - SpAWN POINTS AREN'T OK serverBD.iLocation = ", serverBD.iLocation, " WILL RESET")
						serverBD.iLocation = -1
					ENDIF
				ELSE
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     KILL LIST [GOT_UW_TYPE] - FAILED FM_EVENT_IS_VARIATION_IN_HISTORY_LIST serverBD.iLocation = ", serverBD.iLocation, " serverbd.iHistorySubType = ", serverbd.iHistorySubType, " serverbd.iHistoryVariation = ",serverbd.iHistoryVariation, " WILL RESET")
					serverBD.iLocation = -1
				ENDIF
			ELSE
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     KILL LIST [GOT_UW_TYPE] - WAITING FOR GET_RANDOM_SETUP_DETAILS")
			ENDIF
		ENDIF
	ENDIF
	
	RETURN IS_BIT_SET(serverBD.iServerBitSet, biS_SetupComplete)
ENDFUNC

//Do necessary pre game start ini.
PROC PROCESS_PRE_GAME( MP_MISSION_DATA missionScriptArgs )
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission) ,  missionScriptArgs)	//NUM_NETWORK_PLAYERS, missionScriptArgs)	//
	//NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, PARTICIPANT_ID_TO_INT())  
	
	//Reserve Entities
	RESERVE_NETWORK_MISSION_PEDS(MAX_MW_PEDS)
	RESERVE_NETWORK_MISSION_VEHICLES(MAX_MW_VEHS + MAX_PLAYER_VEH)

	// This makes sure the net script is active, waits untill it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	//common event proccess pre game
	COMMON_FREEMODE_AMBIENT_EVENTS_PROCESS_PRE_GAME(FMMC_TYPE_KILL_LIST)
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	DISABLE_MENTAL_STATE_BLIP_COLOURS(TRUE)
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		#IF IS_DEBUG_BUILD NET_LOG("KILL LIST -  FAILED TO RECEIVE INITIAL NETWORK BROADCAST")	#ENDIF
		NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  MISSION END - FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP D     <----------     ") NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
	
	INT i
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		// This script will not be paused if another script calls PAUSE_GAME
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		//	SHOULD_RUN_COMPETITIVE()
			
			
			
			REPEAT MAX_PLAYERS i
				serverBD.piPlayers[i] = INVALID_PLAYER_INDEX()
			ENDREPEAT
			
			i = 0
			REPEAT MAX_PLAYER_VEH i
				serverBD.playerDriving[i] = INVALID_PLAYER_INDEX()
				serverBD.iKillsPerVehicle[i] = -1
				serverBD.iPedPlayerTargetServer[i] = -1
			ENDREPEAT
			
			PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBD.iHashedMac, serverBD.iMatchHistoryId)
		
			
			
		ENDIF
		
		i = 0
		REPEAT MAX_PLAYER_VEH i
			iPedPlayerTargetClient[i] = -1
		ENDREPEAT
			
		telemetryStruct.m_playersNotified = NETWORK_GET_TOTAL_NUM_PLAYERS()
		telemetryStruct.m_notifiedTime	= GET_CLOUD_TIME_AS_INT()
		
		SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_KILL_LIST, TRUE)
		
		SETUP_UW_REL_GROUPS()
		
		IF SHOULD_UI_BE_HIDDEN()
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet ,biP_DisabledMode)
			NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  EVENT DISABLED VIA PI MENU     <----------     ") NET_NL()
		ENDIF
		
		playerBD[PARTICIPANT_ID_TO_INT()].iMyUwTeam = -1
		
		MPGlobalsAmbience.iUrbanWarfareType = -1
		
		playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_INI
		NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  PRE_GAME DONE     <----------     ") NET_NL()
	ELSE
		#IF IS_DEBUG_BUILD NET_LOG("KILL LIST -  NETWORK_IS_GAME_IN_PROGRESS = FALSE")	#ENDIF
		NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  MISSION END - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP Q     <----------     ") NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_PedCheck")
			NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST - PRE_GAME RUNNING WITH sc_PedCheck  <----------     ") NET_NL()
		ENDIF
		
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_uw4Teams")
			NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST - PRE_GAME RUNNING WITH sc_uw4Teams  <----------     ") NET_NL()
		ENDIF
		
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_TestSavage")
			PRINTLN("     ---------->     KILL LIST - [PRE_GAME] running with sc_TestSavage")
		ENDIF
		
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_VehCheck")
			PRINTLN("     ---------->     KILL LIST - [PRE_GAME] running with sc_VehCheck")
		ENDIF
		
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_forceUWspawnCheck")
			PRINTLN("     ---------->     KILL LIST - [PRE_GAME] running with sc_forceUWspawnCheck")
		ENDIF
		
	//	PRINTLN("     ---------->     KILL LIST - [PRE_GAME] REMOVED SET_ENTITY_ONLY_DAMAGED_BY_RELATIONSHIP_GROUP")
	#ENDIF
	  
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - STARTING *************************************************")
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST")
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - TUNABLES:")
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - (C) GET_EVENT_DURATION() 								- g_sMPTunables.icompetitive_Kill_List_Event_Time_Limit = ", g_sMPTunables.icomp_Kill_List_Event_Time_Limit)
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - GET_EVENT_DURATION() 									- g_sMPTunables.iKill_List_Event_Time_Limit = ", g_sMPTunables.iKill_List_Event_Time_Limit)
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - GET_EVENT_EXPIRY_TIME() 								- g_sMPTunables.iKill_List_Event_Expiry_Time_ = ", GET_EVENT_EXPIRY_TIME())
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - GET_WAIT_FOR_OTHER_PLAYERS_TIME() 					- g_sMPTunables.iKill_List_Player_Wait_Timer_2_player_vehicles = ", GET_WAIT_FOR_OTHER_PLAYERS_TIME())
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - GET_VEHICLE_DETONATION_TIME() 						- g_sMPTunables.iKill_List_Detonation_Time = ", GET_VEHICLE_DETONATION_TIME())
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - GET_MINIMUM_PARTICIPANTS_FOR_COMPETITIVE_VARIATION() 	- g_sMPTunables.iComp_Kill_List_Minimum_Participants_Launch = ", GET_MINIMUM_PARTICIPANTS_FOR_COMPETITIVE_VARIATION())
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - GET_NUMBER_OF_PARTICIPANTS_FOR_2_PLAYER_VEHICLES() 	- g_sMPTunables.iComp_Kill_List_Comp_Min_Player_2_Vehicle_Threshold = ", GET_NUMBER_OF_PARTICIPANTS_FOR_2_PLAYER_VEHICLES())
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - GET_NUMBER_OF_PARTICIPANTS_FOR_3_PLAYER_VEHICLES() 	- g_sMPTunables.iComp_Kill_List_Comp_Min_Player_3_Vehicle_Threshold = ", GET_NUMBER_OF_PARTICIPANTS_FOR_3_PLAYER_VEHICLES())
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - IS_VEHICLE_VARIATION_DISABLED(RHINO) 					- g_sMPTunables.bKill_List_Disable_Car_Rhino = ", IS_VEHICLE_VARIATION_DISABLED(RHINO))
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - IS_VEHICLE_VARIATION_DISABLED(HYDRA) 					- g_sMPTunables.bKill_List_Disable_Heli_Hydra = ", IS_VEHICLE_VARIATION_DISABLED(HYDRA))
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - IS_VEHICLE_VARIATION_DISABLED(SAVAGE) 				- g_sMPTunables.bKill_List_Disable_Heli_Savage = ", IS_VEHICLE_VARIATION_DISABLED(SAVAGE))
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - IS_VEHICLE_VARIATION_DISABLED(BUZZARD) 				- g_sMPTunables.bKill_List_Disable_Heli_Buzzard = ", IS_VEHICLE_VARIATION_DISABLED(BUZZARD))
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - IS_VEHICLE_VARIATION_DISABLED(VALKYRIE) 				- g_sMPTunables.bKill_List_Disable_Heli_Valkyrie = ", IS_VEHICLE_VARIATION_DISABLED(VALKYRIE))
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - GET_BOSS_HEALTH() 									- g_sMPTunables.iKill_List_Boss_Health = ", GET_BOSS_HEALTH())
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - GET_ENEMY_PED_HEALTH_MULTIPLIER() 					- g_sMPTunables.fKill_List_Enemy_Health = ", GET_ENEMY_PED_HEALTH_MULTIPLIER())
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - GET_PLAYER_VEH_START_HEALTH_TANK() 						- g_sMPTunables.iKill_List_Vehicle_Health = ", GET_PLAYER_VEH_START_HEALTH_TANK())
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - GET_PLAYER_VEH_START_HEALTH_PLANE() 						- g_sMPTunables.iKill_List_Vehicle_Health = ", GET_PLAYER_VEH_START_HEALTH_PLANE())
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - GET_PLAYER_VEH_START_HEALTH_HELI() 						- g_sMPTunables.iKill_List_Vehicle_Health = ", GET_PLAYER_VEH_START_HEALTH_HELI())
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - GET_ENEMY_ACCURACY_MULTIPLIER() 						- g_sMPTunables.fKill_List_Enemy_Gun_Accuracy = ", GET_ENEMY_ACCURACY_MULTIPLIER())
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - GET_ENEMY_MISSILE_ACCURACY_MULTIPLIER() 				- g_sMPTunables.fKill_List_Enemy_Missile_Accuracy = ", GET_ENEMY_MISSILE_ACCURACY_MULTIPLIER())
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_AIRCRAFT_ONLY) 			- g_sMPTunables.iKill_List_Wave_Type_Aircraft_Enemies = ", GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_AIRCRAFT_ONLY))
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS) 		- g_sMPTunables.iKill_List_Wave_Type_Ground_Vehicle_Enemies = ", GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS))
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_BOSS) 						- g_sMPTunables.iKill_List_Wave_Type_Boss_Enemies = ", GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_BOSS))
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - GET_NUMBER_OF_GROUND_PEDS_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS) 	- g_sMPTunables.iKill_List_Wave_Type_Foot_Enemies = ", GET_NUMBER_OF_VEHICLES_TUNABLE_FOR_WAVE_TYPE(WAVE_TYPE_GROUND_PEDS_AND_VEHS))
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - GET_MAX_NUMBER_OF_CONCURRENT_ON_FOOT_PEDS() 	- g_sMPTunables.iKill_List_Max_Foot_Enemies_at_a_Time = ", GET_MAX_NUMBER_OF_CONCURRENT_ON_FOOT_PEDS())
	#ENDIF
ENDPROC


PROC MOVE_TO_EXPLODE_VEHICLE_STAGE()
	IF serverBD.eStage != eAD_EXPLODE_VEH
///		IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh[0]))
			//Send Ticker
//			SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
//			TickerEventData.TickerEvent = TICKER_EVENT_UW_EXPLODE	
//			TickerEventData.dataInt = 30
//			BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_IN_RANGE_OF_POINT(GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niVeh[0])), VEH_EXP_RANGE, TRUE))
//		ENDIF
		
		Clear_Any_Objective_Text_From_This_Script()
		
		REINIT_NET_TIMER(serverBD.ExplodeVehTimer)
		PRINTLN("     ---------->     KILL LIST - ExplodeVehTimer STARTED")

		serverBD.eStage = eAD_EXPLODE_VEH
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - serverBD.eStage = eAD_EXPLODE_VEH ")
	ENDIF
ENDPROC				

FUNC BOOL ARE_ALL_PEDS_CLEANED_UP()
	INT i
	
	IF NOT HAS_NET_TIMER_STARTED(serverbd.timeCleanup)
		START_NET_TIMER(serverbd.timeCleanup)
	ELSE
		IF HAS_NET_TIMER_EXPIRED(serverbd.timeCleanup, 20000)
			PRINTLN("     ---------->     KILL LIST ARE_ALL_PEDS_CLEANED_UP TRUE BECAUSE =timeCleanup")
			RETURN TRUE
		ENDIF
	ENDIF
	REPEAT MAX_MW_PEDS i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.niMwPed[i])
			IF serverbd.iPedState[i] < PED_AI_STATE_FINISHED
				PRINTLN("     ---------->     KILL LIST ARE_ALL_PEDS_CLEANED_UP FALSE PED = ", i, " iPedState = ", serverbd.iPedState[i])
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC BOOL ARE_ALL_PLAYER_VEHICLES_DEAD()
	INT i
	
	REPEAT MAX_PLAYER_VEH i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
			IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh[i]))
				#IF IS_DEBUG_BUILD	
					VECTOR vTemp = GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niVeh[i]), FALSE)
					IF bWdDebugMissionEnd
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->  KILL LIST MISSION_END_CHECK... This player veh not dead ", i, " Coords are ", vTemp)
					ENDIF
				#ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC BOOL ARE_ALL_PLAYER_VEHICLES_EMPTY()
	INT i
	
	REPEAT MAX_PLAYER_VEH i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
			IF NOT IS_VEHICLE_EMPTY(NET_TO_VEH(serverBD.niVeh[i]), TRUE)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC
FUNC BOOL MISSION_END_CHECK()
	#IF IS_DEBUG_BUILD	
		IF bWdDebugMissionEnd
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ----------> MISSION_END_CHECK... serverBD.eStage = ", serverBD.eStage)
		ENDIF
	#ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllVehiclesDestroyed)
	AND IS_BIT_SET(serverBD.iServerBitSet, biS_AllVehiclePedsDead)
	AND IS_BIT_SET(serverBD.iServerBitSet, biS_AllOnFootPedsDead)
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AllPlayersFinished)
			#IF IS_DEBUG_BUILD	
				IF bWdDebugMissionEnd
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ----------> MISSION_END_CHECK...FALSE AS  biS_AllPlayersFinished", serverBD.eStage)
				ENDIF
			#ENDIF
			//-- Will delay moving to expldoe vehicle stage until end shard / LB is done
			RETURN FALSE
		ENDIF
	ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_DurationExpired)
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AllPlayersFinished)
			#IF IS_DEBUG_BUILD	
			//	IF bWdDebugMissionEnd
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ----------> MISSION_END_CHECK...FALSE AS  biS_DurationExpired / NOT biS_AllPlayersFinished", serverBD.eStage)
			//	ENDIF
			#ENDIF
			
			//-- If duration expired, will delay moving to expldoe vehicle stage until end shard / LB is done
			RETURN FALSE
		ENDIF
	ENDIF
	IF serverBD.eStage > eAD_IDLE
		IF serverBD.iServerGameState != GAME_STATE_END
			IF ARE_ALL_PLAYER_VEHICLES_DEAD()
				#IF IS_DEBUG_BUILD	
					IF bWdDebugMissionEnd
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ----------> MISSION_END_CHECK... ARE_ALL_PLAYER_VEHICLES_DEAD is TRUE ")
					ENDIF
				#ENDIF
	
				IF serverBD.eStage = eAD_EXPLODE_VEH
					#IF IS_DEBUG_BUILD	
						IF bWdDebugMissionEnd
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ----------> MISSION_END_CHECK... serverBD.eStage = eAD_EXPLODE_VEH ")
						ENDIF
					#ENDIF
				
					IF ARE_ALL_PEDS_CLEANED_UP()
						IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllPlayersFinished)
							NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  MISSION END - VEH EXPLODED AT MISSION END   <----------     ") NET_NL()
							RETURN TRUE
						ELSE
							#IF IS_DEBUG_BUILD	
								IF bWdDebugMissionEnd
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ----------> MISSION_END_CHECK... ARE_ALL_PLAYER_VEHICLES_DEAD - WAITING FOR biS_AllPlayersFinished ")
								ENDIF
							#ENDIF
						ENDIF
					ELSE
						
						#IF IS_DEBUG_BUILD	
							IF bWdDebugMissionEnd
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ----------> MISSION_END_CHECK... WAITING FOR ARE_ALL_PEDS_CLEANED_UP ")
							ENDIF
						#ENDIF
					ENDIF
				ELSE
					IF HAS_NET_TIMER_EXPIRED(serverBD.DestroyCheckDelayTimer, 3000)	//6000
						IF ARE_ALL_PEDS_CLEANED_UP()
							NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  MISSION END - VEH DESTROYED     <----------     ") NET_NL()
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD	
					IF bWdDebugMissionEnd
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ----------> MISSION_END_CHECK... ARE_ALL_PLAYER_VEHICLES_DEAD is FALSE ")
					ENDIF
				#ENDIF
			ENDIF
		ENDIF
		
		//All Players are Off Mission
		IF serverBD.eStage < eAD_EXPLODE_VEH
			#IF IS_DEBUG_BUILD	
				IF bWdDebugMissionEnd
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ----------> MISSION_END_CHECK... serverBD.eStage < eAD_EXPLODE_VEH, serverBD.eStage = ", serverBD.eStage)
				ENDIF
			#ENDIF
			
			BOOL bPlayersOff = FALSE
			INT i
			IF serverBD.eStage >= eAD_ATTACK
				bPlayersOff = TRUE
				REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
					IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
						IF NOT IS_PLAYER_SCTV(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i)))
							IF playerBD[i].eStage < eAD_OFF_MISSION
								bPlayersOff = FALSE
								
								#IF IS_DEBUG_BUILD	
									IF bWdDebugMissionEnd
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ----------> MISSION_END_CHECK - Think this player ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))),  " has eStage = ", playerBD[i].eStage)
									ENDIF
								#ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
			IF bPlayersOff = TRUE
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST -  MISSION END - DRIVER AND GUNNER ARE OFF MISSION    <----------     ") 
				
				IF NOT (IS_BIT_SET(serverBD.iServerBitSet, biS_AllVehiclesDestroyed)
						AND IS_BIT_SET(serverBD.iServerBitSet, biS_AllVehiclePedsDead)
						AND IS_BIT_SET(serverBD.iServerBitSet, biS_AllOnFootPedsDead))
					AND NOT (IS_BIT_SET(serverBD.iServerBitSet, biS_DurationExpired))
					AND NOT (IS_BIT_SET(serverBD.iServerBitSet, biS_TooFewValid))
						//-- No Winning conditions. Everyone must've failed
						SET_BIT(serverBD.iServerBitSet, biS_EveryoneFailed)
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST -  MISSION END - SET biS_EveryoneFailed (1)   <----------     ") 
						
				ENDIF
				MOVE_TO_EXPLODE_VEHICLE_STAGE()
		//		RETURN TRUE
			ELSE
				IF serverBD.eStage < eAD_EXPLODE_VEH
					IF ARE_ALL_PLAYER_VEHICLES_EMPTY()
						SET_BIT(serverBD.iServerBitSet, biS_AllVehiclesEmpty)
						SET_BIT(serverBD.iServerBitSet, biS_EveryoneFailed)
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST -  MISSION END - SET biS_EveryoneFailed (2)   <----------     ") 
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST -  MISSION END - NOBODY IN VEHICLE    <----------     ") 
						MOVE_TO_EXPLODE_VEHICLE_STAGE()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	#IF IS_DEBUG_BUILD	
		IF bWdDebugMissionEnd
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->	 KILL LIST - MISSION_END_CHECK - Returning False at end of func ")
		ENDIF
	#ENDIF
	RETURN FALSE
ENDFUNC

//Server only function that checks to see if the game mode should now end. For this test script, it if time has passed.
FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET()
	
	#IF IS_DEBUG_BUILD
		IF bHostEndMissionNow
			RETURN TRUE
		ENDIF
	#ENDIF	
	
	IF MISSION_END_CHECK()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC




//Helper function to get a clients game/mission state
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iGameState
ENDFUNC

#IF IS_DEBUG_BUILD

PROC CREATE_WIDGETS()
	INT iPlayer
	TEXT_LABEL_15 tl63

	START_WIDGET_GROUP("KILL LIST")  
		ADD_WIDGET_BOOL("Warp to Veh", bWarpToVeh)
		ADD_WIDGET_BOOL("Draw Spawn Points",bWdDrawSpawnPoints)
		ADD_WIDGET_BOOL("Score Debug", bWdDoScoreDebug)
		ADD_WIDGET_BOOL("Ticker test", bWdTestTicker)
		ADD_WIDGET_BOOL("Reward debug", bWdMyReward)
		ADD_WIDGET_BOOL("Force Draw", g_b_ForceDrawChallengeDpadLbd)
		ADD_WIDGET_INT_SLIDER("Event Start Time", iDbgEventStartTime, -1, 60000, 1)
		// Gameplay debug, sever only.
		START_WIDGET_GROUP("Server Only Gameplay") 
			//ADD_WIDGET_BOOL("Allow with 2 players?", bAllow2Players)
			ADD_WIDGET_INT_SLIDER("Min Players", serverBD.iMinPlayers, -1, 4, 1)
			ADD_WIDGET_BOOL("Output still alive", bWdShowPedsStillToKill)
			ADD_WIDGET_BOOL("Wave Debug", bWdDOWaveDebug)
			ADD_WIDGET_BOOL("Force Win", bWdForceWin)
			ADD_WIDGET_BOOL("Kill Savage Driver", bWdKillDriver)
			ADD_WIDGET_BOOL("Assisted Damage", bWdCheckLastDamager)
			ADD_WIDGET_BOOL("End mission", bHostEndMissionNow)			
		STOP_WIDGET_GROUP()		
		
		// Data about the server
		START_WIDGET_GROUP("Server BD") 
			ADD_WIDGET_INT_SLIDER("S. Game state", serverBD.iServerGameState,-1, HIGHEST_INT,1)
			ADD_WIDGET_INT_READ_ONLY("Location", serverBD.iLocation)
			ADD_WIDGET_INT_READ_ONLY("Num seats", serverBD.iVehicleSeats)
			ADD_WIDGET_INT_READ_ONLY("Current wave", serverBD.iCurrentWave)
			ADD_WIDGET_INT_READ_ONLY("Wave Type", iWdWaveType)
			ADD_WIDGET_INT_READ_ONLY("Veh Kills this wave", serverBD.iVehKillsThisWave)
			ADD_WIDGET_INT_READ_ONLY("Veh spawned this wave", serverBD.iVehicleSpawnedThisWave)
			ADD_WIDGET_BOOL("Debug mission End", bWdDebugMissionEnd)
		STOP_WIDGET_GROUP()	

		// Data about the clients. * = You.
		START_WIDGET_GROUP("Client BD")  				
			REPEAT NUM_NETWORK_PLAYERS iPlayer
				tl63 = "Player "
				tl63 += iPlayer
				IF iPlayer = PARTICIPANT_ID_TO_INT()
					tl63 += "*"
				ENDIF
				START_WIDGET_GROUP(tl63)
					ADD_WIDGET_INT_SLIDER("Game state", playerBD[iPlayer].iGameState,-1, HIGHEST_INT,1)
					ADD_WIDGET_INT_READ_ONLY("My total kills", playerBD[iPlayer].iMyKills)
				STOP_WIDGET_GROUP()
			ENDREPEAT				
		STOP_WIDGET_GROUP()	
		
	STOP_WIDGET_GROUP()
ENDPROC		


PROC DISPLAY_KILL_LIST_STRING_AND_NUMBER(FLOAT fX, FLOAT fY, STRING sDisplay, INT iDisplay)
	INT iR, iG, iB, iA
	GET_HUD_COLOUR(HUD_COLOUR_RED, iR, iG, iB, iA)
	
	SET_TEXT_COLOUR(iR, iG, iB, iA)
	SET_TEXT_SCALE(0.0000, 0.5)
	DISPLAY_TEXT_WITH_LITERAL_STRING(fX, fY, "STRING", sDisplay)
	
	SET_TEXT_COLOUR(iR, iG, iB, iA)
	SET_TEXT_SCALE(0.0000, 0.5)
	DISPLAY_TEXT_WITH_NUMBER((fX + 0.2), fY, "NUMBER", iDisplay)	
ENDPROC

PROC UPDATE_WIDGETS()
	INT i
	INT iR, iG, iB, iA
	
	//Warp player to Veh
	IF bWarpToVeh = TRUE
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[0])
			IF NET_WARP_TO_COORD(GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niVeh[0]))+<<0, 0, 3>>, 0, TRUE, FALSE)
				NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  UPDATE_WIDGETS - bWarpToVeh DONE    <----------     ") NET_NL()
				bWarpToVeh = FALSE
			ELSE
				NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  UPDATE_WIDGETS - bWarpToVeh IN PROGRESS    <----------     ") NET_NL()
			ENDIF
		ENDIF
	ENDIF
	
	IF serverbd.iMinPlayers > 0
		serverBD.iVehicleSeats = serverbd.iMinPlayers
	ENDIF
	
//	IF bAllow2Players
//		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
//			serverbd.bAllowWith2Players = TRUE
//			serverBD.iVehSeats = 2
//			bAllow2Players = FALSE	
//		ENDIF
//	ENDIF
	
	IF bWdDrawSpawnPoints
		GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
		
		REPEAT MAX_MW_VEH_SPAWN_LOC i
			IF NOT ARE_VECTORS_EQUAL(serverBD.vMwVehSpawn[i], << 0.0, 0.0, 0.0 >>)
				DRAW_MARKER( MARKER_CYLINDER,
				serverBD.vMwVehSpawn[i],
				<<0,0,0>>,
				<<0,0,0>>,
				<<3.0, 3.0,100.0>>,
				iR,
				iG,
				iB,
				iA )
			ENDIF
			
		ENDREPEAT
		
		i = 0
		GET_HUD_COLOUR(HUD_COLOUR_BLUE, iR, iG, iB, iA)
		
		REPEAT MAX_MW_PED_SPAWN_LOC i
			IF NOT ARE_VECTORS_EQUAL(serverBD.vMwPedSpawn[i], << 0.0, 0.0, 0.0 >>)
				DRAW_MARKER( MARKER_CYLINDER,
				serverBD.vMwPedSpawn[i],
				<<0,0,0>>,
				<<0,0,0>>,
				<<3.0, 3.0,100.0>>,
				iR,
				iG,
				iB,
				iA )
			ENDIF
			
		ENDREPEAT
		
	ENDIF
	
	INT iVehToUse = -1
	INT iVeh
	INT iNumPass
	PED_INDEX pedTemp
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_9, KEYBOARD_MODIFIER_SHIFT, "SKIP")	
		REPEAT MAX_MW_VEHS iVeh
			IF iVehToUse = -1
				IF IS_NET_VEHICLE_DRIVEABLE(serverbd.niMwVeh[iVeh])
					IF NOT IS_VEHICLE_EMPTY(NET_TO_VEH(serverBD.niMwVeh[iVeh]), TRUE)
					//	iVehToUse = iVeh
						NETWORK_EXPLODE_VEHICLE(NET_TO_VEH(serverBD.niMwVeh[iVeh]))
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF iVehToUse <> -1
			iNumPass = GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(NET_TO_VEH(serverBD.niMwVeh[iVehToUse]))
			i = 0
			iNumPass += 1
			
			REPEAT iNumPass i
				pedTemp = GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(serverBD.niMwVeh[iVehToUse]),  INT_TO_ENUM(VEHICLE_SEAT, i-1))
				IF pedTemp <> NULL
					IF NOT IS_PED_INJURED(pedTemp)
						SET_ENTITY_HEALTH(pedTemp, 5)
					ENDIF
				ENDIF
			ENDREPEAT	
		ENDIF
	ENDIF
	
	IF bWdTestTicker
		PRINT_TICKER_WITH_ONE_CUSTOM_STRING("UW_WONC", "UW_TMVALK")
		bWdTestTicker = FALSE
	ENDIF
	
	IF bWdKillDriver
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niMwPed[0])
			IF NOT IS_NET_PED_INJURED(serverBD.niMwPed[0])
				SET_ENTITY_HEALTH(NET_TO_PED(serverBD.niMwPed[0]), 0)
				bWdKillDriver = FALSE
				CPRINTLN(DEBUG_NET_AMBIENT, "     ---------->     KILL LIST [bWdKillDriver] KILLED DRIVER")
			ENDIF
		ENDIF
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niMwPed[4])
			IF NOT IS_NET_PED_INJURED(serverBD.niMwPed[4])
				SET_ENTITY_HEALTH(NET_TO_PED(serverBD.niMwPed[4]), 0)
				bWdKillDriver = FALSE
				CPRINTLN(DEBUG_NET_AMBIENT, "     ---------->     KILL LIST [bWdKillDriver] KILLED DRIVER")
			ENDIF
		ENDIF
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niMwPed[8])
			IF NOT IS_NET_PED_INJURED(serverBD.niMwPed[8])
				SET_ENTITY_HEALTH(NET_TO_PED(serverBD.niMwPed[8]), 0)
				bWdKillDriver = FALSE
				CPRINTLN(DEBUG_NET_AMBIENT, "     ---------->     KILL LIST [bWdKillDriver] KILLED DRIVER")
			ENDIF
		ENDIF
	ENDIF
	
	PLAYER_INDEX playerKillAssist
	INT iKillAssist
	IF bWdCheckLastDamager
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niMwVeh[0])
			IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niMwVeh[0])
				IF NETWORK_GET_ASSISTED_DAMAGE_OF_ENTITY(playerKillAssist, NET_TO_VEH(serverBD.niMwVeh[0]), iKillAssist)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"KILL LIST - [playerKillAssist] GOT DAMAGE ASSIST PLAYER ", GET_PLAYER_NAME(playerKillAssist), " DAMAGE ", iKillAssist)
				ELSE
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"KILL LIST - [playerKillAssist] FAILED TO GET GET DAMAGE ASSIST ")
				ENDIF
				
				bWdCheckLastDamager = FALSE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

#ENDIF

FUNC STRING GET_VEHICLE_TYPE_STRING()
	RETURN GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(serverBD.VehModel)
ENDFUNC

FUNC STRING GET_VEH_BLIP_STRING()
	IF serverBD.VehModel = RHINO
		RETURN "ABLIP_TANK"			//"~BLIP_TEMP_6~"
	ELIF IS_THIS_MODEL_A_PLANE(serverBD.VehModel)
		RETURN "ABLIP_PLANE"		//"~BLIP_TEMP_6~"
	ENDIF
	
	RETURN "ABLIP_HELI"				//"~BLIP_TEMP_6~"*/
ENDFUNC



PROC DO_HELP_TEXT(INT iHelp)
	IF SHOULD_UI_BE_HIDDEN()
		EXIT
	ENDIF
	
	SWITCH iHelp
		CASE 1
			IF NOT IS_BIT_SET(iBoolsBitSet, biHelp1Done)
				IF HAS_PLAYER_BEEN_IN_SESSION_THIS_LONG(CRATE_DROP_DISPLAY_DELAY)
				AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
					IF NOT IS_COMPETITIVE_UW()
						PRINT_HELP_WITH_STRING_AND_NUMBER_NO_SOUND("UW_HELP1", GET_VEH_BLIP_STRING(), GET_CASH_REWARD(TRUE), DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)	//A ~p~Hot Target~s~ ~p~~a~ ~s~is available. Enter it and destroy Merryweather patrols for $~1~.
					ELSE
						PRINT_HELP_WITH_STRING_AND_NUMBER_NO_SOUND("UW_HELP1C", GET_VEH_BLIP_STRING(), GET_CASH_REWARD(TRUE), DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)	//A ~p~Hot Target~s~ ~p~~a~ ~s~is available. Enter it and destroy Merryweather patrols for $~1~.
					ENDIF
					SET_FREEMODE_EVENT_HELP_BACKGROUND(FALSE)
					PLAY_SOUND_FRONTEND(-1, "Event_Start_Text", "GTAO_FM_Events_Soundset", FALSE)
					SET_BIT(iBoolsBitSet, biHelp1Done)
					PRINTLN("     ---------->     KILL LIST -  HELP TEXT - biHelp1Done SET    <----------     ")
				ENDIF
			ENDIF
		BREAK
		/*CASE 2
			IF NOT IS_BIT_SET(iBoolsBitSet, biHelp2Done)
				IF HAS_PLAYER_BEEN_IN_SESSION_THIS_LONG(CRATE_DROP_DISPLAY_DELAY)
				AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_PAUSE_MENU_ACTIVE()
					PRINT_HELP_WITH_STRING_NO_SOUND("UW_HELP2", GET_VEHICLE_TYPE_STRING())	//You are a ~r~Hot Target~s~. Other players will be rewarded for killing you while you are delivering the ~a~.
					SET_BIT(iBoolsBitSet, biHelp2Done)
					PRINTLN("     ---------->     KILL LIST - HELP TEXT - biHelp2Done SET    <----------     ")
				ENDIF
			ENDIF
		BREAK*/
	ENDSWITCH
ENDPROC


/// PURPOSE:
///    Only trigger the target-destroyed audio at most once every couple of seconds, otherwise it sounds weir 
PROC DO_TARGET_DESTROYED_AUDIO()
	IF NOT HAS_NET_TIMER_STARTED(timeTargetDestroyed)
	OR (HAS_NET_TIMER_STARTED(timeTargetDestroyed) AND HAS_NET_TIMER_EXPIRED(timeTargetDestroyed, 2000))
		PLAY_SOUND_FRONTEND(-1, "Kill_List_Counter", "GTAO_FM_Events_Soundset", FALSE)
		RESET_NET_TIMER(timeTargetDestroyed)
		START_NET_TIMER(timeTargetDestroyed)
	ENDIF
ENDPROC

FUNC BOOL WAS_LOCAL_PLAYER_LAST_PLAYER_TO_DAMAGE_UW_VEH(INT iUwVeh)
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] [WAS_LOCAL_PLAYER_LAST_PLAYER_TO_DAMAGE_UW_VEH] CALLED WITH VEH ", iUwVeh, " MY TIME ", playerBD[PARTICIPANT_ID_TO_INT()].iMyUwVehDamageTime[iUwVeh])
	INT i
	
	IF playerBD[PARTICIPANT_ID_TO_INT()].iMyUwVehDamageTime[iUwVeh] = 0
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] [WAS_LOCAL_PLAYER_LAST_PLAYER_TO_DAMAGE_UW_VEH] FALSE AS playerBD[PARTICIPANT_ID_TO_INT()].iMyUwVehDamageTime[iUwVeh] = 0")
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PLAYER_INDEX playerTemp
	#ENDIF
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			#IF IS_DEBUG_BUILD
				playerTemp = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			#ENDIF
			
			IF i <> PARTICIPANT_ID_TO_INT()
				IF playerBD[i].iMyUwVehDamageTime[iUwVeh] >  playerBD[PARTICIPANT_ID_TO_INT()].iMyUwVehDamageTime[iUwVeh]
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] [WAS_LOCAL_PLAYER_LAST_PLAYER_TO_DAMAGE_UW_VEH] THINK PLAYER ", GET_PLAYER_NAME(playerTemp), "DAMAGE VEH MOIRE RECENTLY. THEIR TIME ", playerBD[i].iMyUwVehDamageTime[iUwVeh])
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] [WAS_LOCAL_PLAYER_LAST_PLAYER_TO_DAMAGE_UW_VEH] RETURNING TRUE ")
	
	RETURN TRUE
ENDFUNC

FUNC BOOL DID_ANY_PLAYER_DO_DAMAGE_TO_UW_VEH_TIME_CHECK(INT iUwVeh)
	INT i
	
	#IF IS_DEBUG_BUILD
		PLAYER_INDEX playerTemp
	#ENDIF
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			#IF IS_DEBUG_BUILD
				playerTemp = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			#ENDIF
			
			IF playerBD[i].iMyUwVehDamageTime[iUwVeh] > 0
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] [DID_ANY_PLAYER_DO_DAMAGE_TO_UW_VEH_TIME_CHECK] TRUE THIS PLAYER DAMAGED IT ", GET_PLAYER_NAME(playerTemp), " AT TIME ", playerBD[i].iMyUwVehDamageTime[iUwVeh])
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL DID_ANY_PLAYER_DO_DAMAGE_TO_UW_VEH_TRACKING_CHECK(INT iUwVeh)
	INT iKillAssist
	INT i
	PLAYER_INDEX playerTemp
	
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			playerTemp = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] [DID_ANY_PLAYER_DO_DAMAGE_TO_UW_VEH_TRACKING_CHECK] CHECKING PLAYER ", GET_PLAYER_NAME(playerTemp))
			
			
			IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niMwVeh[iUwVeh]))
				IF NETWORK_GET_ASSISTED_DAMAGE_OF_ENTITY(playerTemp, NET_TO_VEH(serverBD.niMwVeh[iUwVeh]), iKillAssist)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] [DID_ANY_PLAYER_DO_DAMAGE_TO_UW_VEH_TRACKING_CHECK] TRUE VIA ASSISTED DAMAGE VEH ",iUwVeh, " PLAYER ", GET_PLAYER_NAME(playerTemp))
					RETURN TRUE
				ENDIF
			ELSE
				IF NETWORK_GET_ASSISTED_KILL_OF_ENTITY(playerTemp, NET_TO_VEH(serverBD.niMwVeh[iUwVeh]), iKillAssist)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] [DID_ANY_PLAYER_DO_DAMAGE_TO_UW_VEH_TRACKING_CHECK] TRUE VIA ASSISTED KILL VEH ",iUwVeh, " PLAYER ", GET_PLAYER_NAME(playerTemp))
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] [DID_ANY_PLAYER_DO_DAMAGE_TO_UW_VEH_TRACKING_CHECK] FALSE VEH ",iUwVeh)
	
	RETURN FALSE
	
ENDFUNC

STRUCT SCRIPT_EVENT_DATA_UW_VEH_DAMAGEABLE_BY_REL_GROUP
	STRUCT_EVENT_COMMON_DETAILS Details
	INT iVeh
ENDSTRUCT

PROC BROADCAST_KILL_LIST_SET_VEHICLE_DAMAGEABLE_BY_REL_GROUP(INT iveh, INT iPlayerFlags)
	SCRIPT_EVENT_DATA_UW_VEH_DAMAGEABLE_BY_REL_GROUP Event
	Event.Details.Type = SCRIPT_EVENT_UW_VEH_DAMAGEABLE_BY_REL_GROUP
	Event.Details.FromPlayerIndex = PLAYER_ID()

	Event.iVeh = iveh

	
	IF NOT (iPlayerFlags = 0)
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ----->    KILL LIST -  BROADCAST_KILL_LIST_SET_VEHICLE_DAMAGEABLE_BY_REL_GROUP - SENT BY ", GET_PLAYER_NAME(PLAYER_ID())," at ",GET_CLOUD_TIME_AS_INT(), " iveh = ", iveh)
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iPlayerFlags)	
	ELSE
		NET_PRINT("BROADCAST_KILL_LIST_SET_VEHICLE_DAMAGEABLE_BY_REL_GROUP - playerflags = 0 so not broadcasting") NET_NL()		
	ENDIF
ENDPROC

PROC PROCESS_EVENT_KILL_LIST_SET_VEH_DAMAGEABLE_BY_REL_GROUP(INT iEventID)
	SCRIPT_EVENT_DATA_UW_VEH_DAMAGEABLE_BY_REL_GROUP Event
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
		PRINTLN("KILL LIST - PROCESS_EVENT_KILL_LIST_SET_VEH_DAMAGEABLE_BY_REL_GROUP - called... Event.iVeh = ", Event.iVeh)
		SET_BIT(iLocalSetVehRelGroupBitset, Event.iVeh)
		
	ELSE	
		SCRIPT_ASSERT("PROCESS_EVENT_KILL_LIST_SET_VEH_DAMAGEABLE_BY_REL_GROUP - could not retrieve data.")
	ENDIF
ENDPROC

STRUCT SCRIPT_EVENT_DATA_UW_PED_DAMAGEABLE_BY_REL_GROUP
	STRUCT_EVENT_COMMON_DETAILS Details
	INT iPed
ENDSTRUCT

PROC BROADCAST_KILL_LIST_SET_PED_DAMAGEABLE_BY_REL_GROUP(INT iPed, INT iPlayerFlags)
	SCRIPT_EVENT_DATA_UW_PED_DAMAGEABLE_BY_REL_GROUP Event
	Event.Details.Type = SCRIPT_EVENT_UW_PED_DAMAGEABLE_BY_REL_GROUP
	Event.Details.FromPlayerIndex = PLAYER_ID()

	Event.iPed = iPed

	
	IF NOT (iPlayerFlags = 0)
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ----->    KILL LIST -  BROADCAST_KILL_LIST_SET_PED_DAMAGEABLE_BY_REL_GROUP - SENT BY ", GET_PLAYER_NAME(PLAYER_ID())," at ",GET_CLOUD_TIME_AS_INT(), " iPed = ", iPed)
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iPlayerFlags)	
	ELSE
		NET_PRINT("BROADCAST_KILL_LIST_SET_PED_DAMAGEABLE_BY_REL_GROUP - playerflags = 0 so not broadcasting") NET_NL()		
	ENDIF
ENDPROC

PROC PROCESS_EVENT_KILL_LIST_SET_PED_DAMAGEABLE_BY_REL_GROUP(INT iEventID)
	SCRIPT_EVENT_DATA_UW_PED_DAMAGEABLE_BY_REL_GROUP Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
		PRINTLN("KILL LIST - PROCESS_EVENT_KILL_LIST_SET_PED_DAMAGEABLE_BY_REL_GROUP - called... Event.iPed = ", Event.iPed)
		SET_BIT(iLocalSetPedRelGroupBitSet, Event.iPed)

		
	ELSE	
		SCRIPT_ASSERT("PROCESS_EVENT_KILL_LIST_SET_PED_DAMAGEABLE_BY_REL_GROUP - could not retrieve data.")
	ENDIF
ENDPROC

STRUCT SCRIPT_EVENT_DATA_UW_PED_TOO_FAR_FROM_TARGETS
	STRUCT_EVENT_COMMON_DETAILS Details
	INT iPed
ENDSTRUCT

PROC BROADCAST_KILL_LIST_PED_TOO_FAR_FROM_TARGETS(INT iPed, INT iPlayerFlags)
	SCRIPT_EVENT_DATA_UW_PED_TOO_FAR_FROM_TARGETS Event
	Event.Details.Type = SCRIPT_EVENT_UW_PED_TOO_FAR_FROM_TARGETS
	Event.Details.FromPlayerIndex = PLAYER_ID()

	Event.iPed = iPed

	
	IF NOT (iPlayerFlags = 0)
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ----->    KILL LIST -  BROADCAST_KILL_LIST_PED_TOO_FAR_FROM_TARGETS - SENT BY ", GET_PLAYER_NAME(PLAYER_ID())," at ",GET_CLOUD_TIME_AS_INT(), " iPed = ", iPed)
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iPlayerFlags)	
	ELSE
		NET_PRINT("BROADCAST_KILL_LIST_PED_TOO_FAR_FROM_TARGETS - playerflags = 0 so not broadcasting") NET_NL()		
	ENDIF
ENDPROC

PROC PROCESS_EVENT_KILL_LIST_PED_TOO_FAR_FROM_TARGETS(INT iEventID)
	SCRIPT_EVENT_DATA_UW_PED_DAMAGEABLE_BY_REL_GROUP Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
		IF NETWORK_GET_HOST_OF_SCRIPT("am_kill_list") != INVALID_PLAYER_INDEX()
			PRINTLN("KILL LIST - PROCESS_EVENT_KILL_LIST_PED_TOO_FAR_FROM_TARGETS - called... Event.iPed = ", Event.iPed, " Host - ", GET_PLAYER_NAME(NETWORK_GET_HOST_OF_SCRIPT("am_kill_list")) )
		ENDIF
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			PRINTLN("KILL LIST - PROCESS_EVENT_KILL_LIST_PED_TOO_FAR_FROM_TARGETS - called... I've set iPedRetaskGotoBitSet bit ", Event.iPed) 
			SET_BIT(serverBD.iPedRetaskGotoBitSet,  Event.iPed)
		ENDIF
	ELSE	
		SCRIPT_ASSERT("PROCESS_EVENT_KILL_LIST_PED_TOO_FAR_FROM_TARGETS - could not retrieve data.")
	ENDIF
ENDPROC

FUNC BOOL CHECK_FOR_PLAYER_DESTROYED_VEHICLE_BITSET_RESET(INT iVeh)
	INT i
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			IF IS_BIT_SET(playerBD[i].iPlayerDestVehBitSet, iVeh)
				PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER think this player ", GET_PLAYER_NAME(PlayerId), " still has iPlayerDestVehBitSet set for veh ", iVeh)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

STRUCT SCRIPT_EVENT_DATA_UW_VEH_EMPTY
	STRUCT_EVENT_COMMON_DETAILS Details
	INT iVeh
ENDSTRUCT

PROC BROADCAST_KILL_LIST_VEH_EMPTY(INT iPlayerFlags, INT iSavage)
	SCRIPT_EVENT_DATA_UW_VEH_EMPTY Event
	Event.Details.Type = SCRIPT_EVENT_UW_KILL_LIST_VEH_EMPTY
	Event.Details.FromPlayerIndex = PLAYER_ID()
	Event.iVeh = iSavage
	
	IF NOT (iPlayerFlags = 0)
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ----->    KILL LIST -  [PROCESS_UW_DAMAGE_EVENT] BROADCAST_KILL_LIST_VEH_EMPTY - SENT BY ", GET_PLAYER_NAME(PLAYER_ID())," at ",GET_CLOUD_TIME_AS_INT(), " iSavage = ", iSavage)
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iPlayerFlags)	
	ELSE
		NET_PRINT("BROADCAST_KILL_LIST_VEH_EMPTY - playerflags = 0 so not broadcasting") NET_NL()		
	ENDIF
ENDPROC

PROC PROCESS_EVENT_KILL_LIST_VEH_EMPTY(INT iEventID)
	SCRIPT_EVENT_DATA_UW_VEH_EMPTY Event
	INT iCapacity
	INT irp
//	PLAYER_INDEX playerKillAssist
	INT iKillAssist
	INT iVeh
	INT iPilot
	BOOL bIshouldClaimReward
	
	PLAYER_INDEX playerWithMostKills
	PLAYER_INDEX playerDriving
	PED_INDEX pedDriving
	MODEL_NAMES mTarget
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
		iVeh = Event.iVeh
		PRINTLN("KILL LIST -[PROCESS_UW_DAMAGE_EVENT] [PROCESS_EVENT_KILL_LIST_VEH_EMPTY] - called. iVeh = ", iVeh)
		
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niMwVeh[iVeh])
			PRINTLN("KILL LIST -[PROCESS_UW_DAMAGE_EVENT] [PROCESS_EVENT_KILL_LIST_VEH_EMPTY] - EXIT AS NI DOESN'T EXIST! VEH ", iVeh)
			EXIT
		ENDIF
		
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerDestVehBitSet, iVeh)
			IF NOT IS_BIT_SET(serverBD.iTargetVehDestroyedBitSet, iVeh)
				IF DID_ANY_PLAYER_DO_DAMAGE_TO_UW_VEH_TRACKING_CHECK(iVeh)
					IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niMwVeh[iVeh]))
						IF NETWORK_GET_ASSISTED_DAMAGE_OF_ENTITY(PLAYER_ID(), NET_TO_VEH(serverBD.niMwVeh[iVeh]), iKillAssist)
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"KILL LIST - [PROCESS_UW_DAMAGE_EVENT] [PROCESS_EVENT_KILL_LIST_VEH_EMPTY] I DAMAGED THIS Veh MOST RECENTLY ", iVeh, " VIA NETWORK_GET_ASSISTED_DAMAGE_OF_ENTITY WILL CLAIM REWARD ")
							bIshouldClaimReward = TRUE
						ELSE
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"KILL LIST - [PROCESS_UW_DAMAGE_EVENT] [PROCESS_EVENT_KILL_LIST_VEH_EMPTY] NETWORK_GET_ASSISTED_DAMAGE_OF_ENTITY RETURNED NO RESULT!")
						ENDIF
					ELSE
						IF NETWORK_GET_ASSISTED_KILL_OF_ENTITY(PLAYER_ID(), NET_TO_VEH(serverBD.niMwVeh[iVeh]), iKillAssist)
							
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"KILL LIST - [PROCESS_UW_DAMAGE_EVENT] [PROCESS_EVENT_KILL_LIST_VEH_EMPTY] I DAMAGED THIS Veh MOST RECENTLY ", iVeh, " VIA NETWORK_GET_ASSISTED_KILL_OF_ENTITY WILL CLAIM REWARD ")
							bIshouldClaimReward = TRUE
							
						ELSE
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"KILL LIST - [PROCESS_UW_DAMAGE_EVENT] [PROCESS_EVENT_KILL_LIST_VEH_EMPTY] NETWORK_GET_ASSISTED_KILL_OF_ENTITY RETURNED NO RESULT!")
						ENDIF
					ENDIF
				ELSE
					//-- If nobody did any damage, and it's not competitive uw, give kill to player with highest number of kills (or the pilot)
					IF NOT IS_COMPETITIVE_UW() 
						IF serverBD.sLeaderboardData[0].iKills > 0
							playerWithMostKills = INT_TO_PLAYERINDEX(serverBD.sLeaderboardData[0].iPlayer)
							IF playerWithMostKills <> INVALID_PLAYER_INDEX()
								IF playerWithMostKills = PLAYER_ID()
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"KILL LIST - [PROCESS_UW_DAMAGE_EVENT] [PROCESS_EVENT_KILL_LIST_VEH_EMPTY] VEH ", iVeh, " NOBODY CAN CLAIM KILL I'M IN FIRST PLACE, WILL CLAIM_REWARD")
									bIshouldClaimReward = TRUE
								ELSE
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"KILL LIST - [PROCESS_UW_DAMAGE_EVENT] [PROCESS_EVENT_KILL_LIST_VEH_EMPTY] VEH ", iVeh, " NOBODY CAN CLAIM KILL THIS PLAYER IN FIRST PLACE ", GET_PLAYER_NAME(playerWithMostKills))
								ENDIF
							ELSE
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"KILL LIST - [PROCESS_UW_DAMAGE_EVENT] [PROCESS_EVENT_KILL_LIST_VEH_EMPTY] VEH ", iVeh, " NOBODY CAN CLAIM KILL AND PLAYER IN FIRST PLACE INVALID!!!")
							ENDIF
						ELSE
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"KILL LIST - [PROCESS_UW_DAMAGE_EVENT] [PROCESS_EVENT_KILL_LIST_VEH_EMPTY] VEH ", iVeh, " NOBODY CAN CLAIM KILL AND NOBODY SCORED ANY KILLS. WILL AWARD TO PILOT")
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])
								IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[0])
									pedDriving = GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(serverBD.niVeh[0]))
									IF pedDriving <> NULL
										IF IS_PED_A_PLAYER(pedDriving)
											playerDriving = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedDriving)
											IF playerDriving = PLAYER_ID()
												CPRINTLN(DEBUG_NET_AMBIENT_LR,	"KILL LIST - [PROCESS_UW_DAMAGE_EVENT] [PROCESS_EVENT_KILL_LIST_VEH_EMPTY] VEH ", iVeh, " NOBODY CAN CLAIM KILL I'M DRIVING, WILL CLAIM_REWARD")
												bIshouldClaimReward = TRUE
											ELSE
												CPRINTLN(DEBUG_NET_AMBIENT_LR,	"KILL LIST - [PROCESS_UW_DAMAGE_EVENT] [PROCESS_EVENT_KILL_LIST_VEH_EMPTY] VEH ", iVeh, " NOBODY CAN CLAIM KILL THIS PLAYER IS DRIVING")
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF bIshouldClaimReward
			IF playerBD[PARTICIPANT_ID_TO_INT()].eStage < eAD_OFF_MISSION
				iRp = GET_TARGET_KILL_RP_REWARD()
				mTarget = GET_ENTITY_MODEL(NET_TO_VEH(serverBD.niMwVeh[iVeh]))
				iPilot = iVeh * 4
				
				INT iKillsToAward
				INT iPed
				
				IF IS_VALID_KILL_LIST_VEHICLE_MODEL(mTarget)														
					//-- Try to figure out how many kills to award based on how many of the vehicle inhabitants haven't been cleaned up
					iCapacity = GET_KILL_LIST_VEHICLE_CAPACITY(mTarget)
					
					FOR iPed = iPilot TO (iPilot + (iCapacity - 1))
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niMwPed[iPed])
						AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
							iKillsToAward++
							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] [PROCESS_EVENT_KILL_LIST_VEH_EMPTY] SET iPlayerTargetPedKilledBitset FOR PED ", iPed, " iKillsToAward = ",iKillsToAward, " iCapacity = ",iCapacity)
						ENDIF
					ENDFOR
					
					playerBD[PARTICIPANT_ID_TO_INT()].iMyKills += iKillsToAward
					iRp *= iKillsToAward
				ELSE
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] [PROCESS_EVENT_KILL_LIST_VEH_EMPTY]  IS_VALID_KILL_LIST_VEHICLE_MODEL IS FALSE!")
					playerBD[PARTICIPANT_ID_TO_INT()].iMyKills++
					
				ENDIF
				

				IF telemetryStruct.m_timeTakenForObjective = 0
					telemetryStruct.m_timeTakenForObjective = GET_CLOUD_TIME_AS_INT()  
				ENDIF
				
				IF playerBD[PARTICIPANT_ID_TO_INT()].iMyKills <=  GET_KILL_CAP()
					telemetryStruct.m_rpEarned +=iRp
					GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "",XPTYPE_SKILL, XPCATEGORY_KILL_LIST_KILL, iRp)
					DO_TARGET_DESTROYED_AUDIO()
				ENDIF
				
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] [PROCESS_EVENT_KILL_LIST_VEH_EMPTY] I CLAIMED KILL FOR  THIS VEHICLE = ", iVeh, " iKillsToAward was ", iKillsToAward, " Given RP ", iRp, " my kills ", playerBD[PARTICIPANT_ID_TO_INT()].iMyKills)
				
			ELSE
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] [PROCESS_EVENT_KILL_LIST_VEH_EMPTY] I DESTROYED THIS VEHICLE = ", iVeh, " BUT MY STAGE IS ", playerBD[PARTICIPANT_ID_TO_INT()].eStage)
			ENDIF
			
			
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerDestVehBitSet, iVeh)
		ENDIF
	ENDIF
ENDPROC

STRUCT SCRIPT_EVENT_DATA_UW_SAVAGE_EMPTY
	STRUCT_EVENT_COMMON_DETAILS Details
	INT iVeh
ENDSTRUCT

PROC BROADCAST_KILL_LIST_SAVAGE_EMPTY(INT iPlayerFlags, INT iSavage)
	SCRIPT_EVENT_DATA_UW_SAVAGE_EMPTY Event
	Event.Details.Type = SCRIPT_EVENT_UW_KILL_LIST_SAVAGE_EMPTY
	Event.Details.FromPlayerIndex = PLAYER_ID()
	Event.iVeh = iSavage
	
	IF NOT (iPlayerFlags = 0)
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ----->    KILL LIST -  [PROCESS_UW_DAMAGE_EVENT] BROADCAST_KILL_LIST_SAVAGE_EMPTY - SENT BY ", GET_PLAYER_NAME(PLAYER_ID())," at ",GET_CLOUD_TIME_AS_INT(), " iSavage = ", iSavage)
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iPlayerFlags)	
	ELSE
		NET_PRINT("BROADCAST_KILL_LIST_SAVAGE_EMPTY - playerflags = 0 so not broadcasting") NET_NL()		
	ENDIF
ENDPROC

PROC PROCESS_EVENT_KILL_LIST_SAVAGE_EMPTY(INT iEventID)
	SCRIPT_EVENT_DATA_UW_SAVAGE_EMPTY Event
	INT iCapacity
	INT irp
//	PLAYER_INDEX playerKillAssist
	INT iKillAssist
	INT iSavage
	INT iSavagePilot
	BOOL bIshouldClaimReward
	
	PLAYER_INDEX playerWithMostKills
	PLAYER_INDEX playerDriving
	PED_INDEX pedDriving
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
		iSavage = Event.iVeh
		PRINTLN("KILL LIST -[PROCESS_UW_DAMAGE_EVENT] [PROCESS_EVENT_KILL_LIST_SAVAGE_EMPTY] - called. iSavage = ", iSavage)
		
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerDestVehBitSet, iSavage)
			IF NOT IS_BIT_SET(serverBD.iTargetVehDestroyedBitSet, iSavage)
				IF WAS_LOCAL_PLAYER_LAST_PLAYER_TO_DAMAGE_UW_VEH(iSavage)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"KILL LIST - [PROCESS_UW_DAMAGE_EVENT] [PROCESS_EVENT_KILL_LIST_SAVAGE_EMPTY] I DAMAGED THIS SAVAGE MOST RECENTLY ", iSavage, "  VIA WAS_LOCAL_PLAYER_LAST_PLAYER_TO_DAMAGE_UW_VEH WILL CLAIM REWARD ")
					bIshouldClaimReward = TRUE
				ELSE
					IF NOT DID_ANY_PLAYER_DO_DAMAGE_TO_UW_VEH_TIME_CHECK(iSavage)
						IF DID_ANY_PLAYER_DO_DAMAGE_TO_UW_VEH_TRACKING_CHECK(iSavage)
							IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niMwVeh[iSavage]))
								IF NETWORK_GET_ASSISTED_DAMAGE_OF_ENTITY(PLAYER_ID(), NET_TO_VEH(serverBD.niMwVeh[iSavage]), iKillAssist)
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"KILL LIST - [PROCESS_UW_DAMAGE_EVENT] [PROCESS_EVENT_KILL_LIST_SAVAGE_EMPTY] I DAMAGED THIS SAVAGE MOST RECENTLY ", iSavage, " VIA NETWORK_GET_ASSISTED_DAMAGE_OF_ENTITY WILL CLAIM REWARD ")
									bIshouldClaimReward = TRUE
								ELSE
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"KILL LIST - [PROCESS_UW_DAMAGE_EVENT] [PROCESS_EVENT_KILL_LIST_SAVAGE_EMPTY] NETWORK_GET_ASSISTED_DAMAGE_OF_ENTITY RETURNED NO RESULT!")
								ENDIF
							ELSE
								IF NETWORK_GET_ASSISTED_KILL_OF_ENTITY(PLAYER_ID(), NET_TO_VEH(serverBD.niMwVeh[iSavage]), iKillAssist)

									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"KILL LIST - [PROCESS_UW_DAMAGE_EVENT] [PROCESS_EVENT_KILL_LIST_SAVAGE_EMPTY] I DAMAGED THIS SAVAGE MOST RECENTLY ", iSavage, " VIA NETWORK_GET_ASSISTED_KILL_OF_ENTITY WILL CLAIM REWARD ")
									bIshouldClaimReward = TRUE

								ELSE
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"KILL LIST - [PROCESS_UW_DAMAGE_EVENT] [PROCESS_EVENT_KILL_LIST_SAVAGE_EMPTY] NETWORK_GET_ASSISTED_KILL_OF_ENTITY RETURNED NO RESULT!")
								ENDIF
							ENDIF
						ELSE
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"KILL LIST - [PROCESS_UW_DAMAGE_EVENT] [PROCESS_EVENT_KILL_LIST_SAVAGE_EMPTY] NOBODY ASSISTED DAMAGE SAVAGE VEH ", iSavage)
							
							//-- If nobody did any damage, and it's not competitive uw, give kill to player with highest number of kills (or the pilot)
							IF NOT IS_COMPETITIVE_UW() 
								IF serverBD.sLeaderboardData[0].iKills > 0
									playerWithMostKills = INT_TO_PLAYERINDEX(serverBD.sLeaderboardData[0].iPlayer)
									IF playerWithMostKills <> INVALID_PLAYER_INDEX()
										IF playerWithMostKills = PLAYER_ID()
											CPRINTLN(DEBUG_NET_AMBIENT_LR,	"KILL LIST - [PROCESS_UW_DAMAGE_EVENT] [PROCESS_EVENT_KILL_LIST_SAVAGE_EMPTY] VEH ", iSavage, " NOBODY CAN CLAIM KILL I'M IN FIRST PLACE, WILL CLAIM_REWARD")
											bIshouldClaimReward = TRUE
										ELSE
											CPRINTLN(DEBUG_NET_AMBIENT_LR,	"KILL LIST - [PROCESS_UW_DAMAGE_EVENT] [PROCESS_EVENT_KILL_LIST_SAVAGE_EMPTY] VEH ", iSavage, " NOBODY CAN CLAIM KILL THIS PLAYER IN FIRST PLACE ", GET_PLAYER_NAME(playerWithMostKills))
										ENDIF
									ELSE
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"KILL LIST - [PROCESS_UW_DAMAGE_EVENT] [PROCESS_EVENT_KILL_LIST_SAVAGE_EMPTY] VEH ", iSavage, " NOBODY CAN CLAIM KILL AND PLAYER IN FIRST PLACE INVALID!!!")
									ENDIF
								ELSE
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"KILL LIST - [PROCESS_UW_DAMAGE_EVENT] [PROCESS_EVENT_KILL_LIST_SAVAGE_EMPTY] VEH ", iSavage, " NOBODY CAN CLAIM KILL AND NOBODY SCORED ANY KILLS. WILL AWARD TO PILOT")
									IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])
										IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[0])
											pedDriving = GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(serverBD.niVeh[0]))
											IF pedDriving <> NULL
												IF IS_PED_A_PLAYER(pedDriving)
													playerDriving = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedDriving)
													IF playerDriving = PLAYER_ID()
														CPRINTLN(DEBUG_NET_AMBIENT_LR,	"KILL LIST - [PROCESS_UW_DAMAGE_EVENT] [PROCESS_EVENT_KILL_LIST_SAVAGE_EMPTY] VEH ", iSavage, " NOBODY CAN CLAIM KILL I'M DRIVING, WILL CLAIM_REWARD")
														bIshouldClaimReward = TRUE
													ELSE
														CPRINTLN(DEBUG_NET_AMBIENT_LR,	"KILL LIST - [PROCESS_UW_DAMAGE_EVENT] [PROCESS_EVENT_KILL_LIST_SAVAGE_EMPTY] VEH ", iSavage, " NOBODY CAN CLAIM KILL THIS PLAYER IS DRIVING")
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF bIshouldClaimReward
					IF playerBD[PARTICIPANT_ID_TO_INT()].eStage < eAD_OFF_MISSION
																			
						iCapacity = GET_KILL_LIST_VEHICLE_CAPACITY(SAVAGE)
						iSavagePilot = iSavage * 4
						INT iPed
						INT iKillsToAward = 0
						
						//-- Award kills based on number of peds in savage, but don't want to count peds that have already been counted
						FOR iPed = iSavagePilot TO (iSavagePilot + (iCapacity - 1))
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niMwPed[iPed])
							AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
								iKillsToAward++
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] [PROCESS_EVENT_KILL_LIST_SAVAGE_EMPTY] SET iPlayerTargetPedKilledBitset FOR PED ", iPed, " iKillsToAward = ",iKillsToAward, " iCapacity = ",iCapacity)
							ENDIF
						ENDFOR
						
						playerBD[PARTICIPANT_ID_TO_INT()].iMyKills += iKillsToAward
						iRp *= iKillsToAward
						
						IF playerBD[PARTICIPANT_ID_TO_INT()].iMyKills <= GET_KILL_CAP()
							iRp = GET_TARGET_KILL_RP_REWARD()
							iRp *= iCapacity
							telemetryStruct.m_rpEarned +=iRp	
						
							GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "",XPTYPE_SKILL, XPCATEGORY_KILL_LIST_KILL, iRp)
							DO_TARGET_DESTROYED_AUDIO()
								
							IF telemetryStruct.m_timeTakenForObjective = 0
								telemetryStruct.m_timeTakenForObjective = GET_CLOUD_TIME_AS_INT()  
							ENDIF
						ENDIF
						
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerDestVehBitSet, iSavage)
					ENDIF
				ENDIF
			ELSE
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"KILL LIST - [PROCESS_UW_DAMAGE_EVENT] PROCESS_EVENT_KILL_LIST_SAVAGE_EMPTY - server.iTargetVehDestroyedBitSet bit ALREADY SET!")
			ENDIF
		ELSE
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"KILL LIST - [PROCESS_UW_DAMAGE_EVENT] PROCESS_EVENT_KILL_LIST_SAVAGE_EMPTY - iPlayerDestVehBitSet bit ALREADY SET!")
		ENDIF
	ELSE	
		SCRIPT_ASSERT("PROCESS_EVENT_KILL_LIST_SAVAGE_EMPTY - could not retrieve data.")
	ENDIF
ENDPROC

STRUCT SCRIPT_EVENT_DATA_UW_TARGET_VEH_RESET
	STRUCT_EVENT_COMMON_DETAILS Details
	INT iVeh
ENDSTRUCT

PROC BROADCAST_KILL_LIST_VEHICLE_RESET(INT iveh, INT iPlayerFlags)
	SCRIPT_EVENT_DATA_UW_TARGET_VEH_RESET Event
	Event.Details.Type = SCRIPT_EVENT_UW_TARGET_VEH_RESET
	Event.Details.FromPlayerIndex = PLAYER_ID()

	Event.iVeh = iveh

	
	IF NOT (iPlayerFlags = 0)
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ----->    KILL LIST -  BROADCAST_KILL_LIST_VEHICLE_RESET - SENT BY ", GET_PLAYER_NAME(PLAYER_ID())," at ",GET_CLOUD_TIME_AS_INT(), " iveh = ", iveh)
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iPlayerFlags)	
	ELSE
		NET_PRINT("BROADCAST_KILL_LIST_VEHICLE_RESET - playerflags = 0 so not broadcasting") NET_NL()		
	ENDIF
ENDPROC

PROC PROCESS_EVENT_KILL_LIST_VEHICLE_RESET(INT iEventID)
	SCRIPT_EVENT_DATA_UW_TARGET_VEH_RESET Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
		PRINTLN("KILL LIST - PROCESS_EVENT_KILL_LIST_VEHICLE_RESET - called...")
		
		INT iBit = Event.iveh
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerDestVehBitSet, iBit)
			CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerDestVehBitSet, iBit)
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] PROCESS_EVENT_KILL_LIST_VEHICLE_RESET PLAYER BIT WAS SET FOR VEH ", Event.iveh)
		ENDIF
		
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	" PROCESS_EVENT_KILL_LIST_VEHICLE_RESET - KILL LIST - iveh = ", Event.iveh)
	ELSE	
		SCRIPT_ASSERT("PROCESS_EVENT_KILL_LIST_VEHICLE_RESET - could not retrieve data.")
	ENDIF
ENDPROC

STRUCT SCRIPT_EVENT_DATA_UW_TARGET_ON_FOOT_RESET
	STRUCT_EVENT_COMMON_DETAILS Details
	INT iPed
ENDSTRUCT

PROC BROADCAST_KILL_LIST_TARGET_ON_FOOT_RESET(INT iPed, INT iPlayerFlags)
	SCRIPT_EVENT_DATA_UW_TARGET_ON_FOOT_RESET Event
	Event.Details.Type = SCRIPT_EVENT_UW_TARGET_ON_FOOT_RESET
	Event.Details.FromPlayerIndex = PLAYER_ID()

	Event.iPed = iPed

	
	IF NOT (iPlayerFlags = 0)
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ----->   KILL LIST BROADCAST_KILL_LIST_TARGET_ON_FOOT_RESET - SENT BY ", GET_PLAYER_NAME(PLAYER_ID())," at ",GET_CLOUD_TIME_AS_INT(), " iPed = ", iPed)
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iPlayerFlags)	
	ELSE
		NET_PRINT("BROADCAST_KILL_LIST_TARGET_ON_FOOT_RESET - playerflags = 0 so not broadcasting") NET_NL()		
	ENDIF
ENDPROC

PROC PROCESS_EVENT_KILL_LIST_TARGET_ON_FOOT_RESET(INT iEventID)
	SCRIPT_EVENT_DATA_UW_TARGET_ON_FOOT_RESET Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
		PRINTLN("PROCESS_EVENT_KILL_LIST_TARGET_ON_FOOT_RESET - called...")
		
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(Event.iPed)], GET_LONG_BITSET_BIT(Event.iPed) )
			CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(Event.iPed)], GET_LONG_BITSET_BIT(Event.iPed) )
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] PROCESS_EVENT_KILL_LIST_TARGET_ON_FOOT_RESET PLAYER BIT WAS SET FOR PED ", Event.iPed)
		ENDIF
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	" PROCESS_EVENT_KILL_LIST_TARGET_ON_FOOT_RESET - KILL LIST - iPed = ", Event.iPed)
	ELSE	
		SCRIPT_ASSERT("PROCESS_EVENT_KILL_LIST_TARGET_ON_FOOT_RESET - could not retrieve data.")
	ENDIF
ENDPROC


PROC MAINTAIN_UW_VEHICLE_AND_PEDS_ONLY_DAMAGEABLE_BY_RELGROUPS()
	INT i
	INT j
	FOR i = 0 TO 31
		j = 0
		
		IF IS_BIT_SET(iLocalSetVehRelGroupBitset, i)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niMwVeh[i])
				PRINTLN("KILL LIST [MAINTAIN_UW_VEHICLE_AND_PEDS_ONLY_DAMAGEABLE_BY_RELGROUPS] - setting for veh = ", i)
			//	SET_ENTITY_ONLY_DAMAGED_BY_RELATIONSHIP_GROUP(NET_TO_PED(serverBD.niMwVeh[i]), TRUE, relUWPlayer)
					
				REPEAT serverBD.iVehSeats j
					IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niMwPed[(i*MAX_MW_PEDS_PER_VEH)+j])
						IF NOT IS_NET_PED_INJURED(serverBD.niMwPed[(i*MAX_MW_PEDS_PER_VEH)+j])
							PRINTLN("KILL LIST [MAINTAIN_UW_VEHICLE_AND_PEDS_ONLY_DAMAGEABLE_BY_RELGROUPS] - setting for in-veh ped = ", (i*MAX_MW_PEDS_PER_VEH)+j)
				//			SET_ENTITY_ONLY_DAMAGED_BY_RELATIONSHIP_GROUP(NET_TO_PED(serverBD.niMwPed[(i*MAX_MW_PEDS_PER_VEH)+j]), TRUE, relUWPlayer)
						ENDIF
					ENDIF
				ENDREPEAT
				CLEAR_BIT(iLocalSetVehRelGroupBitset, i)
			ELSE
			//	CLEAR_BIT(iLocalSetVehRelGroupBitset, i)
				PRINTLN("KILL LIST [MAINTAIN_UW_VEHICLE_AND_PEDS_ONLY_DAMAGEABLE_BY_RELGROUPS] - NI doesn't exist for veh = ", i)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(iLocalSetPedRelGroupBitSet, i)

			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niMwPed[i])
				PRINTLN("KILL LIST [MAINTAIN_UW_VEHICLE_AND_PEDS_ONLY_DAMAGEABLE_BY_RELGROUPS] - setting for ped = ", i)
				IF NOT IS_NET_PED_INJURED(serverBD.niMwPed[i])
				//	SET_ENTITY_ONLY_DAMAGED_BY_RELATIONSHIP_GROUP(NET_TO_PED(serverBD.niMwPed[i]), TRUE, relUWPlayer)
				ENDIF
				CLEAR_BIT(iLocalSetPedRelGroupBitSet, i)
			ELSE
			//	CLEAR_BIT(iLocalSetPedRelGroupBitSet, i)
				PRINTLN("KILL LIST [MAINTAIN_UW_VEHICLE_AND_PEDS_ONLY_DAMAGEABLE_BY_RELGROUPS] - NI doesn't exist for ped = ", i)
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC MAINTAIN_RESET_MY_FF_OPTION()
	IF NOT IS_BIT_SET(iBoolsBitSet2,bi2_ResetFFOption)
		IF playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet <> 0
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					SET_PLAYER_DEFAULT_FRIENDLY_FIRE_OPTION()
					SET_BIT(iBoolsBitSet2,bi2_ResetFFOption)
					CPRINTLN(DEBUG_NET_AMBIENT_LR, "  KILL LIST [MAINTAIN_RESET_MY_FF_OPTION] Set bi2_ResetFFOption")
				ENDIF
			ENDIF
		ENDIF
	ENDIF		
ENDPROC

PROC MAINTAIN_LOCAL_PLAYER_AS_GHOST()
//	IF NOT IS_COMPETITIVE_UW()
//		EXIT
//	ENDIF
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
		EXIT
	ENDIF
	
	IF playerBD[PARTICIPANT_ID_TO_INT()].eStage	= eAD_ATTACK
		IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_SetAsGhost )
			SET_BIT(iBoolsBitSet2, bi2_SetAsGhost )
			SET_LOCAL_PLAYER_AS_GHOST(TRUE)
			CPRINTLN(DEBUG_NET_AMBIENT_LR, "  KILL LIST [MAINTAIN_LOCAL_PLAYER_AS_GHOST] Set bi2_SetAsGhost")
		ENDIF
	ELSE
		IF IS_BIT_SET(iBoolsBitSet2, bi2_SetAsGhost )
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					SET_LOCAL_PLAYER_AS_GHOST(FALSE)
					CLEAR_BIT(iBoolsBitSet2, bi2_SetAsGhost )
					CPRINTLN(DEBUG_NET_AMBIENT_LR, "  KILL LIST [MAINTAIN_LOCAL_PLAYER_AS_GHOST] Clear bi2_SetAsGhost")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

INT iDamageTrackStaggeredPedCheck
INT iLocalActivateDamagePedCheckID[MAX_MW_PEDS]

INT iDamageTrackStaggeredVehCheck
INT iLocalActivateDamageVehCheckID[MAX_MW_VEHS]

BOOL bDamageTrackBoss

/// PURPOSE:
///    Activate damage tracking on on-foot peds, Boss and Hydras. Every player needs to turn it on
PROC MAINTAIN_ACTIVATE_DAMAGE_TRACKING_CLIENT()

	INT iObjID
	
	//-- On-foot checks
	IF serverBD.VehModel <> HYDRA
		IF iDamageTrackStaggeredPedCheck = 0 
			iDamageTrackStaggeredPedCheck = (MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH) // On Foot peds only
		ENDIF
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niMwPed[iDamageTrackStaggeredPedCheck])
			IF NOT IS_NET_PED_INJURED(serverBD.niMwPed[iDamageTrackStaggeredPedCheck])
				iObjID = NETWORK_ENTITY_GET_OBJECT_ID(NET_TO_PED(serverBD.niMwPed[iDamageTrackStaggeredPedCheck]))
				IF iLocalActivateDamagePedCheckID[iDamageTrackStaggeredPedCheck] <> iObjID
					iLocalActivateDamagePedCheckID[iDamageTrackStaggeredPedCheck] = iObjID
					
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [MAINTAIN_ACTIVATE_DAMAGE_TRACKING_CLIENT] ACTIVATE DAMAGE TRACK ON PED ", iDamageTrackStaggeredPedCheck, " NETWORK INDEX ", iObjID) 
					ACTIVATE_DAMAGE_TRACKER_ON_NETWORK_ID(serverBD.niMwPed[iDamageTrackStaggeredPedCheck], TRUE)
					
					 
				ENDIF
			ENDIF
		ENDIF
		
		iDamageTrackStaggeredPedCheck++
		
		IF iDamageTrackStaggeredPedCheck = ( MAX_MW_PEDS -1)
			iDamageTrackStaggeredPedCheck = (MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)
		ENDIF
	ENDIF
	
	INT iVehCount
	//-- Boss check
	IF serverBD.VehModel <> HYDRA
	AND serverBD.VehModel <> RHINO
		IF NOT bDamageTrackBoss
			IF GET_CURRENT_WAVE_TYPE() = WAVE_TYPE_BOSS
				REPEAT MAX_MW_VEHS iVehCount
			
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niMwVeh[iVehCount])
						IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niMwVeh[iVehCount])
							IF GET_ENTITY_MODEL(NET_TO_VEH(serverBD.niMwVeh[iVehCount])) = SAVAGE
								ACTIVATE_DAMAGE_TRACKER_ON_NETWORK_ID(serverBD.niMwVeh[iVehCount], TRUE)
								bDamageTrackBoss = TRUE
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [MAINTAIN_ACTIVATE_DAMAGE_TRACKING_CLIENT] ACTIVATE DAMAGE TRACK ON BOSS VEH ", iVehCount) 	
							ENDIF
						ENDIF
					ENDIF
				
				ENDREPEAT
			ENDIF
		ENDIF
	ENDIF
	
	//-- Hydra variation
//	IF serverBD.VehModel = HYDRA
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niMwVeh[iDamageTrackStaggeredVehCheck])
			IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niMwVeh[iDamageTrackStaggeredVehCheck])
				iObjID = NETWORK_ENTITY_GET_OBJECT_ID(NET_TO_PED(serverBD.niMwVeh[iDamageTrackStaggeredVehCheck]))
				IF iLocalActivateDamageVehCheckID[iDamageTrackStaggeredVehCheck] <> iObjID
					iLocalActivateDamageVehCheckID[iDamageTrackStaggeredVehCheck] = iObjID
					
					IF NOT bDamageTrackBoss
						ACTIVATE_DAMAGE_TRACKER_ON_NETWORK_ID(serverBD.niMwVeh[iDamageTrackStaggeredVehCheck], TRUE)
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [MAINTAIN_ACTIVATE_DAMAGE_TRACKING_CLIENT] ACTIVATE DAMAGE TRACK ON VEH ", iDamageTrackStaggeredVehCheck, " NETWORK INDEX ", iObjID) 
					ELSE
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [MAINTAIN_ACTIVATE_DAMAGE_TRACKING_CLIENT] NOT ACTIVATE DAMAGE TRACK ON VEH ", iDamageTrackStaggeredVehCheck, " NETWORK INDEX ", iObjID, " AS bDamageTrackBoss") 
					ENDIF
				ENDIF
			ENDIF
		ENDIF 
		
		iDamageTrackStaggeredVehCheck++
		
		IF iDamageTrackStaggeredVehCheck = MAX_MW_VEHS
			iDamageTrackStaggeredVehCheck = 0
		ENDIF
//	ENDIF
	
ENDPROC

PROC MAINTAIN_PLAYER_KILLED_TARGET_CLIENT()
	INT i
	PLAYER_INDEX playerKilledTarget
	WEAPON_TYPE wPlayer
//	INT iCash
//	SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerData
	REPEAT MAX_MW_PEDS i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.niMwPed[i])
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i) )
				playerKilledTarget = NETWORK_GET_DESTROYER_OF_NETWORK_ID(serverbd.niMwPed[i], wPlayer)
				IF playerKilledTarget = PLAYER_ID()
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(i)],   GET_LONG_BITSET_BIT(i) )
					
				//	iCash = GET_INDIVIDUAL_TARGET_CASH_AWARD()
				//	NETWORK_EARN_FROM_AMBIENT_JOB(iCash,"am_multi_target",amData)
					
					playerBD[PARTICIPANT_ID_TO_INT()].iMyKills++
					
					
					
				//	TickerData.TickerEvent = TICKER_EVENT_MTA_KILLED
				//	TickerData.playerID = PLAYER_ID()
				//	BROADCAST_TICKER_EVENT(TickerData,ALL_PLAYERS_ON_SCRIPT(TRUE))
					
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MAINTAIN_PLAYER_KILLED_TARGET_CLIENT I KILLED A TARGET PED = ", i, " MY TOTAL KILLS = ", playerBD[PARTICIPANT_ID_TO_INT()].iMyKills)
				ELSE
					IF playerKilledTarget <> INVALID_PLAYER_INDEX()
						//-- Check for someone other than me killing tarrget
						IF NETWORK_IS_PLAYER_ACTIVE(playerKilledTarget)

//							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MAINTAIN_PLAYER_KILLED_TARGET_CLIENT I THINK PLAYER ", GET_PLAYER_NAME(playerKilledTarget), " KILLED TARGET PED = ", i)							
//							IF NOT IS_BIT_SET(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetKilledOffMissionBitset, i)
//								IF NOT NETWORK_IS_PLAYER_A_PARTICIPANT(playerKilledTarget)
//									SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetKilledOffMissionBitset, i)
//									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MAINTAIN_PLAYER_KILLED_TARGET_CLIENT TARGET PED = ", i , " KILLED BY NON_PARTICIPANT ", GET_PLAYER_NAME(playerKilledTarget))
//								ENDIF
//							ENDIF
						ENDIF
					ENDIF		
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	i = 0
	
	REPEAT MAX_MW_VEHS i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.niMwVeh[i])
			IF IS_ENTITY_DEAD(NET_TO_VEH(serverbd.niMwVeh[i]))
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MAINTAIN_PLAYER_KILLED_TARGET_CLIENT IS_ENTITY_DEAD TRUE FOR VEH = ", i)
			ENDIF
			
			IF NOT IS_NET_VEHICLE_DRIVEABLE(serverbd.niMwVeh[i])
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MAINTAIN_PLAYER_KILLED_TARGET_CLIENT IS_NET_VEHICLE_DRIVEABLE FALSE FOR VEH = ", i)
			ENDIF
			playerKilledTarget = NETWORK_GET_DESTROYER_OF_NETWORK_ID(serverbd.niMwVeh[i], wPlayer)
			IF playerKilledTarget = PLAYER_ID()
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerDestVehBitSet, i)
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerDestVehBitSet, i)
					playerBD[PARTICIPANT_ID_TO_INT()].iMyKills++
					
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MAINTAIN_PLAYER_KILLED_TARGET_CLIENT I DESTROYED A VEH = ", i, " MY TOTAL KILLS = ", playerBD[PARTICIPANT_ID_TO_INT()].iMyKills)
				ENDIF
			ELSE
				IF playerKilledTarget = INVALID_PLAYER_INDEX()
					IF IS_ENTITY_DEAD(NET_TO_VEH(serverbd.niMwVeh[i]))
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MAINTAIN_PLAYER_KILLED_TARGET_CLIENT IS_ENTITY_DEAD TRUE AND playerKilledTarget = INVALID_PLAYER_INDEX() FOR VEH = ", i)
					ENDIF
					
					IF NOT IS_NET_VEHICLE_DRIVEABLE(serverbd.niMwVeh[i])
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MAINTAIN_PLAYER_KILLED_TARGET_CLIENT IS_NET_VEHICLE_DRIVEABLE FALSE AND playerKilledTarget = INVALID_PLAYER_INDEX() FOR VEH = ", i)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC



FUNC INT WHICH_PED_IS_THIS(PED_INDEX pedToCheck)
	INT iPed = -1
	INT i
	
	REPEAT MAX_MW_PEDS i
		IF iPed = -1
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niMwPed[i])
		//		IF NOT IS_NET_PED_INJURED(serverBD.niMwPed[i])
					IF NET_TO_PED(serverBD.niMwPed[i]) = pedToCheck
						iPed = i
					ENDIF
			//	ENDIF
			ENDIF
		ENDIF				
	ENDREPEAT
	
	RETURN iPed
ENDFUNC


PROC PROCESS_UW_DAMAGE_EVENT(INT iCount)
	//SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerData
	STRUCT_ENTITY_DAMAGE_EVENT sei 
	INT i
	INT iCapacity
	INT iTime
	MODEL_NAMES mTarget
	PED_INDEX pedKiller
	PED_INDEX pedTemp
	INT iRp
	INT iPedIndex
	INT iSeatCount
	
	INT iSavagePilot
	
	PLAYER_INDEX playerKillAssist
	INT iKillAssist
	
	// Grab the event data.
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sei, SIZE_OF(sei))
		
			REPEAT MAX_MW_VEHS i
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerDestVehBitSet, i)
					IF NOT IS_BIT_SET(serverBD.iTargetVehDestroyedBitSet, i)
					
						IF DOES_ENTITY_EXIST(sei.VictimIndex)				
							IF IS_ENTITY_A_VEHICLE(sei.VictimIndex)		
								IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niMwVeh[i])
									IF DOES_ENTITY_EXIST(NET_TO_VEH(serverBD.niMwVeh[i]))						
										IF GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sei.VictimIndex) = NET_TO_VEH(serverBD.niMwVeh[i])
											CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] THIS VEHICLE HAS BEEN DAMAGED BY SOMEONE = ", i)
											IF DOES_ENTITY_EXIST(sei.DamagerIndex)
												IF IS_ENTITY_A_PED(sei.DamagerIndex)							
													IF GET_PED_INDEX_FROM_ENTITY_INDEX(sei.DamagerIndex) = PLAYER_PED_ID()
														CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] THIS VEHICLE HAS BEEN DAMAGED BY ME. STILL DRIVEABLE?  ", i)
														IF sei.VictimDestroyed
															mTarget = GET_ENTITY_MODEL(sei.VictimIndex)
															IF serverBD.VehModel = HYDRA
																IF playerBD[PARTICIPANT_ID_TO_INT()].eStage < eAD_OFF_MISSION
																	iRp = GET_TARGET_KILL_RP_REWARD()
																	IF IS_VALID_KILL_LIST_VEHICLE_MODEL(mTarget)														
																		iCapacity = GET_KILL_LIST_VEHICLE_CAPACITY(mTarget)
																		playerBD[PARTICIPANT_ID_TO_INT()].iMyKills += iCapacity
																		iRp *= iCapacity
																		
																	ELSE
																		playerBD[PARTICIPANT_ID_TO_INT()].iMyKills++
																	ENDIF
																	IF telemetryStruct.m_timeTakenForObjective = 0
																		telemetryStruct.m_timeTakenForObjective = GET_CLOUD_TIME_AS_INT()  
																	ENDIF
																	
																	IF playerBD[PARTICIPANT_ID_TO_INT()].iMyKills <=  GET_KILL_CAP()
																		telemetryStruct.m_rpEarned +=iRp
																	//	GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD,PLAYER_PED_ID(),"",XPTYPE_SKILL,XPCATEGORY_KILL_LIST_KILL,iRp)
																		GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "",XPTYPE_SKILL, XPCATEGORY_KILL_LIST_KILL, iRp)
																		DO_TARGET_DESTROYED_AUDIO()
																	ENDIF
																	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] I DESTROYED THIS VEHICLE = ", i, " Capacity was ", iCapacity, " Given RP ", iRp, " my kills ", playerBD[PARTICIPANT_ID_TO_INT()].iMyKills)
																ELSE
																	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] I DESTROYED THIS VEHICLE = ", i, " BUT MY STAGE IS ", playerBD[PARTICIPANT_ID_TO_INT()].eStage)
																ENDIF
																
																
																SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerDestVehBitSet, i)
															ELSE
																//-- Vehicle dstroyed by me, I'm not in a hydra
																//-- Need to try to handle peds in vehicles who are still alive inspite of the fact that there vehicle is destroyed
																CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] I DESTROYED THIS VEHICLE = ", i, " NOT IN IN HYDRA. WILL CHECK FOR BUZZARD TARGET")
																IF mTarget = BUZZARD
																	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] I DESTROYED THIS VEHICLE = ", i, " NOT IN IN HYDRA. TARGET WAS A BUZZARD. WILL CHECK PEDS IF BUZZARD NOT WRECKED")
																	//IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niMwVeh[i]))
																		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] I DESTROYED THIS VEHICLE = ", i, " NOT IN IN HYDRA. WILL CHECK PEDS IN VEH - BUZZARD NOT DEAD")
																		iSeatCount = -1
																		FOR iSeatCount = -1 TO 3
																			pedTemp = GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(serverBD.niMwVeh[i]), INT_TO_ENUM(VEHICLE_SEAT, iSeatCount))
																			IF pedTemp <> NULL
																			//	IF NOT IS_PED_INJURED(pedTemp)
																					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] I DESTROYED THIS VEHICLE = ", i, " NOT IN IN HYDRA. WILL CHECK PEDS IN VEH - WHICH PED IS IT? ")
																					iPedIndex = WHICH_PED_IS_THIS(pedTemp)
																					IF iPedIndex > -1
																						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] I DESTROYED THIS VEHICLE = ", i, " NOT IN IN HYDRA. WILL CHECK PEDS IN VEH - PED IN SEAT ", iSeatCount ," NOT INJURED! THINK ITS PED ", iPedIndex, " WILL CLAIM KILL")
																						IF NOT IS_BIT_SET(serverBD.iServerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iPedIndex)], GET_LONG_BITSET_BIT(iPedIndex))
																							IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iPedIndex)], GET_LONG_BITSET_BIT(iPedIndex) )
																								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iPedIndex)], GET_LONG_BITSET_BIT(iPedIndex) )
																								iRp = GET_TARGET_KILL_RP_REWARD()
																								playerBD[PARTICIPANT_ID_TO_INT()].iMyKills++
																								IF playerBD[PARTICIPANT_ID_TO_INT()].iMyKills <=  GET_KILL_CAP()
																									telemetryStruct.m_rpEarned +=iRp
																									GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "",XPTYPE_SKILL, XPCATEGORY_KILL_LIST_KILL, iRp)
																									DO_TARGET_DESTROYED_AUDIO()
																								ENDIF
																								IF DOES_PED_HAVE_AI_BLIP(NET_TO_PED(serverBD.niMwPed[iPedIndex]))
																								//	SET_PED_HAS_AI_BLIP(NET_TO_PED(serverBD.niMwPed[iPedIndex]), FALSE)
																									CLEANUP_AI_PED_BLIP(MwPedBlipData[iPedIndex])
																									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] I DESTROYED THIS VEHICLE = ", i, " NOT IN IN HYDRA. I HAVE CLAIMED KILL (REMOVED BLIP) FOR PED ", iPedIndex)	
																								ELSE
																									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] I DESTROYED THIS VEHICLE = ", i, " NOT IN IN HYDRA. I HAVE CLAIMED KILL FOR PED ", iPedIndex)	
																								ENDIF
																								
																							ENDIF
																						ENDIF
																					ENDIF

																			//	ENDIF
																			ENDIF
																		ENDFOR
																	//ELSE
																//		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] I DESTROYED THIS VEHICLE = ", i, " NOT IN IN HYDRA. WILL CHECK PEDS IN VEH - BUZZARD IS DEAD!")
																//	ENDIF
																	
																	SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerDestVehBitSet, i)
																ELSE
																	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] I DESTROYED THIS VEHICLE = ", i, " NOT IN IN HYDRA. TARGET WASN'T A BUZZARD. TARGET MODEL = ", ENUM_TO_INT(mTarget))
																ENDIF
															ENDIF	
																
															
															
														ELSE
															IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.niMwVeh[i])
																CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] I DAMAGED BUT NOT DESTROYED THIS VEHICLE = ", i)
															//	SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerDestVehBitSet, i)
															//	playerBD[PARTICIPANT_ID_TO_INT()].iMyKills++
															ELSE
																CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] THIS VEHICLE HAS BEEN DAMAGED BY ME. YES STILL DRIVEABLE. ", i)
															ENDIF
														ENDIF
													ELSE
														//-- Not damaged by me
														CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] THIS VEHICLE HAS BEEN DAMAGED BY A SOMEONE OTHER THAN ME ", i)
														IF sei.VictimDestroyed
															IF IS_PED_A_PLAYER(GET_PED_INDEX_FROM_ENTITY_INDEX(sei.DamagerIndex))
																CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] THIS VEHICLE HAS BEEN DESTROYED BY A PLAYER OTHER THAN ME ", i)
															ELSE
																CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] THIS VEHICLE HAS BEEN DESTROYED BY A NON PLAYER ", i)
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
											
										ENDIF
									ENDIF
								ENDIF
								
							ENDIF
						ENDIF
					ELSE
						
					ENDIF
				ELSE
					
				ENDIF
			ENDREPEAT
		
		
		IF serverBD.VehModel <> HYDRA
			//-- Player not in a hyrda
			FOR i = 0 TO ((MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)) - 1
				IF NOT IS_BIT_SET(serverBD.iServerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i))
					IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i) )
						IF DOES_ENTITY_EXIST(sei.VictimIndex)				
							IF IS_ENTITY_A_PED(sei.VictimIndex)
								IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niMwPed[i])
									IF DOES_ENTITY_EXIST(NET_TO_VEH(serverBD.niMwPed[i]))						
										IF GET_PED_INDEX_FROM_ENTITY_INDEX(sei.VictimIndex) = NET_TO_PED(serverBD.niMwPed[i])
											CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] THIS IN VEH PED HAS BEEN DAMAGED BY SOMEONE = ", i)
											IF sei.VictimDestroyed
												IF DOES_ENTITY_EXIST(sei.DamagerIndex)
													IF IS_ENTITY_A_PED(sei.DamagerIndex)	
														CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] THIS IN VEH PED HAS BEEN DESTROYED BY SOMEONE = ", i)
														pedKiller = GET_PED_INDEX_FROM_ENTITY_INDEX(sei.DamagerIndex)
														IF IS_PED_A_PLAYER(pedKiller)
															IF pedKiller = PLAYER_PED_ID()
																
																IF sei.VictimDestroyed
																	iRp = GET_TARGET_KILL_RP_REWARD()
																	
																	IF playerBD[PARTICIPANT_ID_TO_INT()].eStage < eAD_OFF_MISSION
																		playerBD[PARTICIPANT_ID_TO_INT()].iMyKills++
																		
																		IF playerBD[PARTICIPANT_ID_TO_INT()].iMyKills <= GET_KILL_CAP()
																			GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "",XPTYPE_SKILL, XPCATEGORY_KILL_LIST_KILL, iRp)
																			telemetryStruct.m_rpEarned +=iRp
																			DO_TARGET_DESTROYED_AUDIO()
																		ENDIF
																		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] I DESTROYED THIS IN VEH PED = ", i, " my kills = ", playerBD[PARTICIPANT_ID_TO_INT()].iMyKills, " Given RP ", iRp)
																	ELSE
																		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] I DESTROYED THIS IN VEH PED = ", i, " BUT MY STAGE = ",  playerBD[PARTICIPANT_ID_TO_INT()].eStage)
																	ENDIF
																	IF telemetryStruct.m_timeTakenForObjective = 0
																		telemetryStruct.m_timeTakenForObjective = GET_CLOUD_TIME_AS_INT()  
																	ENDIF
																	SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i) )
																ENDIF
															ENDIF
														ELSE
															CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] THIS IN VEH PED HAS BEEN DESTROYED BY SOMEONE OTHER THAN A PLAYER = ", i)
														ENDIF
													ENDIF
												ELSE
													CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] IN VEH PED KILLED BUT DON'T KNOW DAMAGER!= ", i)
												//	SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i) )
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
																	
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				
				//-- Additional Boss-vehicle checks to track who has done most damage
				IF i < MAX_MW_VEHS
					IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerDestVehBitSet, i)
						IF NOT IS_BIT_SET(serverBD.iTargetVehDestroyedBitSet, i)			
							IF DOES_ENTITY_EXIST(sei.VictimIndex)	
								IF IS_ENTITY_A_VEHICLE(sei.VictimIndex)
									IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niMwVeh[i])
										IF DOES_ENTITY_EXIST(NET_TO_VEH(serverBD.niMwVeh[i]))	
											IF GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sei.VictimIndex) = NET_TO_VEH(serverBD.niMwVeh[i])
												IF GET_ENTITY_MODEL(sei.VictimIndex) = SAVAGE
													IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i) )
														CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] THIS SAVAGE HAS BEEN DAMAGED BY SOMEONE = ", i)
														IF sei.VictimDestroyed
															CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] THIS SAVAGE HAS BEEN DESTROYED BY SOMEONE = ", i)
															IF DOES_ENTITY_EXIST(sei.DamagerIndex)
																CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] THIS SAVAGE HAS BEEN DESTROYED BY SOMEONE DAMAGER EXISTS = ", i)
																IF IS_ENTITY_A_PED(sei.DamagerIndex)							
																	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] THIS SAVAGE HAS BEEN DESTROYED BY A PED = ", i)
																	pedKiller = GET_PED_INDEX_FROM_ENTITY_INDEX(sei.DamagerIndex)
																		
																	IF IS_PED_A_PLAYER(pedKiller)
																		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] THIS SAVAGE HAS BEEN DESTROYED BY A PLAYER = ", i)

																		IF pedKiller = PLAYER_PED_ID()
																			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] THIS SAVAGE HAS BEEN DESTROYED BY LOCAL_PLAYER= ", i)	
																			IF sei.VictimDestroyed
																				IF playerBD[PARTICIPANT_ID_TO_INT()].eStage < eAD_OFF_MISSION
																					
																					iCapacity = GET_KILL_LIST_VEHICLE_CAPACITY(SAVAGE)
																					playerBD[PARTICIPANT_ID_TO_INT()].iMyKills += iCapacity
																					
																					iRp = GET_TARGET_KILL_RP_REWARD()
																					iRp *= iCapacity
																					
																					IF playerBD[PARTICIPANT_ID_TO_INT()].iMyKills <= GET_KILL_CAP()
																						GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "",XPTYPE_SKILL, XPCATEGORY_KILL_LIST_KILL, iRp)
																						telemetryStruct.m_rpEarned +=iRp 
																						DO_TARGET_DESTROYED_AUDIO()
																						IF telemetryStruct.m_timeTakenForObjective = 0
																							telemetryStruct.m_timeTakenForObjective = GET_CLOUD_TIME_AS_INT()  
																						ENDIF
																					ENDIF
																					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] I DESTROYED THIS SAVAGE = ", i, " Given RP ", iRp, " playerBD[PARTICIPANT_ID_TO_INT()].iMyKills = ", playerBD[PARTICIPANT_ID_TO_INT()].iMyKills, " will set savage ped bitsets")
																					
																					iSavagePilot = i * 4
																					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] SETTING iPlayerTargetPedKilledBitset FOR PEDS ", iSavagePilot, " TO ", iSavagePilot + 3)
																					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iSavagePilot)], GET_LONG_BITSET_BIT(iSavagePilot))
																					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iSavagePilot+1)], GET_LONG_BITSET_BIT(iSavagePilot+1))
																					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iSavagePilot+2)], GET_LONG_BITSET_BIT(iSavagePilot+2))
																					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iSavagePilot+3)], GET_LONG_BITSET_BIT(iSavagePilot+3))
																					
																					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerDestVehBitSet, i)
																				ENDIF
																			ENDIF
																		ENDIF
																	ELSE
																		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] THIS SAVAGE HAS BEEN DESTROYED BY PED WHO IS NOT A PLAYER ", i)
																		IF WAS_LOCAL_PLAYER_LAST_PLAYER_TO_DAMAGE_UW_VEH(i)
																			IF playerBD[PARTICIPANT_ID_TO_INT()].eStage < eAD_OFF_MISSION
																				iCapacity = GET_KILL_LIST_VEHICLE_CAPACITY(SAVAGE)
																				playerBD[PARTICIPANT_ID_TO_INT()].iMyKills += iCapacity
																				
																				IF playerBD[PARTICIPANT_ID_TO_INT()].iMyKills <= GET_KILL_CAP()
																					iRp = GET_TARGET_KILL_RP_REWARD()
																					iRp *= iCapacity
																					telemetryStruct.m_rpEarned +=iRp	
																				
																					GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "",XPTYPE_SKILL, XPCATEGORY_KILL_LIST_KILL, iRp)
																					DO_TARGET_DESTROYED_AUDIO()
																						
																					IF telemetryStruct.m_timeTakenForObjective = 0
																						telemetryStruct.m_timeTakenForObjective = GET_CLOUD_TIME_AS_INT()  
																					ENDIF
																				ENDIF
																				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] I DAMAGED THIS SAVAGE MOST RECENTLY ", i, " Given RP ", iRp, " playerBD[PARTICIPANT_ID_TO_INT()].iMyKills = ", playerBD[PARTICIPANT_ID_TO_INT()].iMyKills, " will set savage ped bitsets")
																				
																				iSavagePilot = i * 4
																				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] SETTING iPlayerTargetPedKilledBitset FOR PEDS ", iSavagePilot, " TO ", iSavagePilot + 3)
																				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iSavagePilot)], GET_LONG_BITSET_BIT(iSavagePilot))
																				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iSavagePilot+1)], GET_LONG_BITSET_BIT(iSavagePilot+1))
																				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iSavagePilot+2)], GET_LONG_BITSET_BIT(iSavagePilot+2))
																				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iSavagePilot+3)], GET_LONG_BITSET_BIT(iSavagePilot+3))
																						
																					
																				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerDestVehBitSet, i)
																			ENDIF
																		ELSE
																			IF DID_ANY_PLAYER_DO_DAMAGE_TO_UW_VEH_TIME_CHECK(i)
																			ELSE
																				IF NETWORK_GET_ASSISTED_KILL_OF_ENTITY(playerKillAssist, sei.VictimIndex, iKillAssist)
																					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] [NETWORK_GET_ASSISTED_KILL_OF_ENTITY] TRUE FOR SAVAGE ", i, " playerKillAssist = ", GET_PLAYER_NAME(playerKillAssist), " iKillAssist = ", iKillAssist)
																					IF playerKillAssist = PLAYER_ID()
																						IF playerBD[PARTICIPANT_ID_TO_INT()].eStage < eAD_OFF_MISSION
																							iCapacity = GET_KILL_LIST_VEHICLE_CAPACITY(SAVAGE)
																							playerBD[PARTICIPANT_ID_TO_INT()].iMyKills += iCapacity
																							
																							IF playerBD[PARTICIPANT_ID_TO_INT()].iMyKills <= GET_KILL_CAP()
																								iRp = GET_TARGET_KILL_RP_REWARD()
																								iRp *= iCapacity
																								telemetryStruct.m_rpEarned +=iRp	
																							
																								GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "",XPTYPE_SKILL, XPCATEGORY_KILL_LIST_KILL, iRp)
																								DO_TARGET_DESTROYED_AUDIO()
																									
																								IF telemetryStruct.m_timeTakenForObjective = 0
																									telemetryStruct.m_timeTakenForObjective = GET_CLOUD_TIME_AS_INT()  
																								ENDIF
																							ENDIF
																							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] I DAMAGED THIS SAVAGE MOST RECENTLY VIA NETWORK_GET_ASSISTED_KILL_OF_ENTITY", i, " Given RP ", iRp, " playerBD[PARTICIPANT_ID_TO_INT()].iMyKills = ", playerBD[PARTICIPANT_ID_TO_INT()].iMyKills, " will set savage ped bitsets")
																							
																							iSavagePilot = i * 4
																							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] SETTING iPlayerTargetPedKilledBitset FOR PEDS ", iSavagePilot, " TO ", iSavagePilot + 3)
																							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iSavagePilot)], GET_LONG_BITSET_BIT(iSavagePilot))
																							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iSavagePilot+1)], GET_LONG_BITSET_BIT(iSavagePilot+1))
																							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iSavagePilot+2)], GET_LONG_BITSET_BIT(iSavagePilot+2))
																							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iSavagePilot+3)], GET_LONG_BITSET_BIT(iSavagePilot+3))
																					
																								
																								
																							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerDestVehBitSet, i)
																						ENDIF
																					ENDIF
																				ELSE
																					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] [NETWORK_GET_ASSISTED_KILL_OF_ENTITY] FALSE FOR SAVAGE ")
																				ENDIF
																			
																			ENDIF
																		ENDIF
																	ENDIF
																ENDIF
															ENDIF
														ELSE
															IF DOES_ENTITY_EXIST(sei.DamagerIndex)
																IF IS_ENTITY_A_PED(sei.DamagerIndex)
																	pedKiller = GET_PED_INDEX_FROM_ENTITY_INDEX(sei.DamagerIndex)
																	IF IS_PED_A_PLAYER(pedKiller)
																		IF pedKiller = PLAYER_PED_ID()
																			iTime = GET_CLOUD_TIME_AS_INT()
																			playerBD[PARTICIPANT_ID_TO_INT()].iMyUwVehDamageTime[i] = iTime
																			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] I HAVE DAMAGED SAVAGE ", i, " AT TIME ", iTime)
																		ENDIF
																	ENDIF
																ENDIF
															ENDIF
															
															IF NOT IS_VEHICLE_DRIVEABLE(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sei.VictimIndex))
																CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] NOT DRIVABLE - SAVAGE ", i)
															ELSE
																CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] STILL DRIVABLE - SAVAGE ", i)
															
															ENDIF
														ENDIF
													ELSE
														CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] SKIPPING SAVAGE CHECK AS iPlayerTargetPedKilledBitset SET")
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
			
			
			
			//-- On foot peds
			FOR i =(MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH) TO MAX_MW_PEDS -1
				IF NOT IS_BIT_SET(serverBD.iServerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i))
					IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i) )
						IF DOES_ENTITY_EXIST(sei.VictimIndex)				
							IF IS_ENTITY_A_PED(sei.VictimIndex)
								IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niMwPed[i])
									IF DOES_ENTITY_EXIST(NET_TO_VEH(serverBD.niMwPed[i]))						
										IF GET_PED_INDEX_FROM_ENTITY_INDEX(sei.VictimIndex) = NET_TO_PED(serverBD.niMwPed[i])
											CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] THIS ON-FOOT PED HAS BEEN DAMAGED BY SOMEONE = ", i)
											IF sei.VictimDestroyed
												CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] THIS ON-FOOT PED HAS BEEN DAMAGED BY SOMEONE = ", i)
												IF DOES_ENTITY_EXIST(sei.DamagerIndex)
													CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] THIS ON-FOOT PED HAS BEEN DESTROYED BY SOMEONE = ", i)
													IF IS_ENTITY_A_PED(sei.DamagerIndex)							
														pedKiller = GET_PED_INDEX_FROM_ENTITY_INDEX(sei.DamagerIndex)
														IF IS_PED_A_PLAYER(pedKiller)
															IF pedKiller = PLAYER_PED_ID()
																
																IF sei.VictimDestroyed
																	IF playerBD[PARTICIPANT_ID_TO_INT()].eStage < eAD_OFF_MISSION
																		playerBD[PARTICIPANT_ID_TO_INT()].iMyKills++
																		
																		IF playerBD[PARTICIPANT_ID_TO_INT()].iMyKills <= GET_KILL_CAP()
																			iRp = GET_TARGET_KILL_RP_REWARD()																				
																			//GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD,PLAYER_PED_ID(),"",XPTYPE_SKILL,XPCATEGORY_KILL_LIST_KILL,iRp)
																			telemetryStruct.m_rpEarned +=iRp
																			IF telemetryStruct.m_timeTakenForObjective = 0
																				telemetryStruct.m_timeTakenForObjective = GET_CLOUD_TIME_AS_INT()  
																			ENDIF
																			GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "",XPTYPE_SKILL, XPCATEGORY_KILL_LIST_KILL, iRp)
																			DO_TARGET_DESTROYED_AUDIO()
																		ENDIF
																		
																		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] I DESTROYED THIS ON_FOOT PED = ", i, " My kills now = ", playerBD[PARTICIPANT_ID_TO_INT()].iMyKills, " Given RP ", iRp)
																	ELSE
																		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] I DESTROYED THIS ON_FOOT PED = ", i, " BUT MY STAGE = ", playerBD[PARTICIPANT_ID_TO_INT()].eStage)
																	ENDIF
																	SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i) )
																	
																	
																ENDIF
															ENDIF
														ELSE
															CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] THIS ON-FOOT PED HAS BEEN DESTROYED BY A PED OTHER THAN THE PLAYER = ", i)
														ENDIF
													ENDIF
												ELSE
													CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] ON FOOT PED KILLED BUT DON'T KNOW DAMAGER!= ", i)
													SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i) )
												ENDIF
											
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niMwPed[i])
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [ON_FOOT_DAMAGE_DEBUG] NI Exitsts! iServerTargetPedKilledBitset STill set for ped ", i)
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
	ENDIF
//	i = 0
//	
//	IF playerBD[PARTICIPANT_ID_TO_INT()].iPlayerDestVehBitSet <> 0
//		REPEAT MAX_MW_VEHS i
//			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niMwVeh[i])
//				IF  IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerDestVehBitSet, i)
//					CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerDestVehBitSet, i)
//					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] PLAYER BIT WAS SET BUT VEH NO LONGER EXISTS  ", i)
//				ENDIF
//			ENDIF
//			IF  IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerDestVehBitSet, i)
//				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] iPlayerDestVehBitSet set for Target Veh   ", i)
//			ENDIF
//		ENDREPEAT	
//	ENDIF
ENDPROC

PROC PROCESS_UW_SCRIPT_EVENT(INT iCount)

    STRUCT_EVENT_COMMON_DETAILS Details
                            
    GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Details, SIZE_OF(Details))
    
    SWITCH Details.Type
		CASE SCRIPT_EVENT_UW_TARGET_VEH_RESET
			PROCESS_EVENT_KILL_LIST_VEHICLE_RESET(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_UW_TARGET_ON_FOOT_RESET
			PROCESS_EVENT_KILL_LIST_TARGET_ON_FOOT_RESET(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_UW_PED_DAMAGEABLE_BY_REL_GROUP
			PROCESS_EVENT_KILL_LIST_SET_PED_DAMAGEABLE_BY_REL_GROUP(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_UW_VEH_DAMAGEABLE_BY_REL_GROUP
			PROCESS_EVENT_KILL_LIST_SET_VEH_DAMAGEABLE_BY_REL_GROUP(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_UW_PED_TOO_FAR_FROM_TARGETS
			PROCESS_EVENT_KILL_LIST_PED_TOO_FAR_FROM_TARGETS(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_UW_KILL_LIST_SAVAGE_EMPTY
			PROCESS_EVENT_KILL_LIST_SAVAGE_EMPTY(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_UW_KILL_LIST_VEH_EMPTY
			PROCESS_EVENT_KILL_LIST_VEH_EMPTY(iCount)
		BREAK
	ENDSWITCH
ENDPROC

PROC PROCESS_EVENTS()
	
	INT iCount
	EVENT_NAMES ThisScriptEvent
	
	//process the events
	
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount
		
		ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)
		
		SWITCH ThisScriptEvent
			
			CASE EVENT_NETWORK_DAMAGE_ENTITY
				
			//	IF IS_COMPETITIVE_UW()
					PROCESS_UW_DAMAGE_EVENT(iCount)
			//	ENDIF
				
				
			BREAK
			
			
			
			CASE EVENT_NETWORK_SCRIPT_EVENT 
			
				PROCESS_UW_SCRIPT_EVENT(iCount)
				
			BREAK
		ENDSWITCH
		
	ENDREPEAT
	
		
//	INT i
//	REPEAT MAX_MW_VEHS i
//		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niMwVeh[i])
//			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [VEH_CHECK]")
//			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [VEH_CHECK] NI EXISTS ", i)
//			IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niMwVeh[i])
//				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [VEH_CHECK] Veh still drivable ", i, " Doing stuck checks...")
//				IF IS_VEHICLE_STUCK_TIMER_UP(NET_TO_VEH(serverBD.niMwVeh[i]), VEH_STUCK_ON_ROOF, 10000)
//					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [VEH_CHECK] Stuck on roof! ", i)
//				ENDIF
//				
//				IF IS_VEHICLE_STUCK_TIMER_UP(NET_TO_VEH(serverBD.niMwVeh[i]), VEH_STUCK_ON_SIDE, 10000)
//					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [VEH_CHECK] VEH_STUCK_ON_SIDE! ", i)
//				ENDIF
//				
//				IF IS_VEHICLE_STUCK_TIMER_UP(NET_TO_VEH(serverBD.niMwVeh[i]), VEH_STUCK_HUNG_UP, 10000)
//					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [VEH_CHECK] VEH_STUCK_HUNG_UP! ", i)
//				ENDIF
//			ELSE
//				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [VEH_CHECK] Veh not drivable ", i)
//			ENDIF
//		ENDIF
//	ENDREPEAT	
ENDPROC




//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  ENTITY CREATION                  /////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////
///    			VEHICLE   				///
///////////////////////////////////////////
//PURPOSE: Create the Veh
FUNC BOOL CREATE_VEH()
	INT i
	
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_15 tlName
	#ENDIF
	
	
	REPEAT serverBD.iNumPlayerVeh i
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niVeh[i])
			IF REQUEST_LOAD_MODEL(serverBD.VehModel)
				IF NOT IS_BIT_SET(serverBD.iClearAreaBitSet, i)
					PRINTLN("     ---------->     KILL LIST CLEAR_AREA AT COORDS ", serverBD.vStartLocation[i])
					CLEAR_AREA(serverBD.vStartLocation[i], 5.0, TRUE, DEFAULT, DEFAULT, TRUE)
					SET_BIT(serverBD.iClearAreaBitSet, i)
				ENDIF
				IF CREATE_NET_VEHICLE(serverBD.niVeh[i], serverBD.VehModel, serverBD.vStartLocation[i], serverBD.fStartHeading[i], DEFAULT, DEFAULT, DEFAULT, TRUE, FALSE)	
					NETWORK_FADE_IN_ENTITY(NET_TO_ENT(serverBD.niVeh[i]),TRUE)
					SET_VEHICLE_DOORS_LOCKED(NET_TO_VEH(serverBD.niVeh[i]), VEHICLELOCK_UNLOCKED)
					SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.niVeh[i]), TRUE)
					SET_VEHICLE_CAN_BE_USED_BY_FLEEING_PEDS(NET_TO_VEH(serverBD.niVeh[i]), FALSE)
					SET_VEHICLE_AUTOMATICALLY_ATTACHES(NET_TO_VEH(serverBD.niVeh[i]), FALSE)
					
					FREEZE_ENTITY_POSITION(NET_TO_VEH(serverBD.niVeh[i]), FALSE)
					SET_ENTITY_DYNAMIC(NET_TO_VEH(serverBD.niVeh[i]), TRUE)
					ACTIVATE_PHYSICS(NET_TO_VEH(serverBD.niVeh[i]))
					SET_VEHICLE_ENGINE_ON(NET_TO_VEH(serverBD.niVeh[i]), TRUE, TRUE)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_VEH(serverBD.niVeh[i]), TRUE)
					SET_VEHICLE_INFLUENCES_WANTED_LEVEL(NET_TO_VEH(serverBD.niVeh[i]), FALSE)
					
					FLOAT fHealth
					IF IS_THIS_MODEL_A_HELI(serverBD.VehModel)
					OR IS_THIS_MODEL_A_PLANE(serverBD.VehModel)
					//	SET_HELI_BLADES_FULL_SPEED(NET_TO_VEH(serverBD.niVeh[i]))
					//	SET_VEHICLE_ENGINE_ON(NET_TO_VEH(serverBD.niVeh[i]), TRUE, TRUE)
					//	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_VEH(serverBD.niVeh[i]), FALSE)
					
					//	SET_PLANE_RESIST_TO_EXPLOSION(NET_TO_VEH(serverBD.niVeh[i]),TRUE)
						SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(NET_TO_VEH(serverBD.niVeh[i]),FALSE)
				//		SET_ENTITY_PROOFS
				//		SET_ENTITY_PROOFS(NET_TO_VEH(serverBD.niVeh[i]), FALSE, FALSE, TRUE, FALSE, FALSE)
				
				
						IF IS_THIS_MODEL_A_HELI(serverBD.VehModel)
							fHealth = TO_FLOAT(GET_PLAYER_VEH_START_HEALTH_HELI())
						ELIF IS_THIS_MODEL_A_PLANE(serverBD.VehModel)
							fHealth = TO_FLOAT(GET_PLAYER_VEH_START_HEALTH_PLANE())
						ELSE
							fHealth = TO_FLOAT(GET_PLAYER_VEH_START_HEALTH_TANK())
						ENDIF
						
						SET_ENTITY_HEALTH(NET_TO_VEH(serverBD.niVeh[i]),ROUND(fHealth))
						SET_ENTITY_MAX_HEALTH(NET_TO_VEH(serverBD.niVeh[i]),ROUND(fHealth) )
						SET_VEHICLE_PETROL_TANK_HEALTH(NET_TO_VEH(serverBD.niVeh[i]),fHealth)
						SET_VEHICLE_BODY_HEALTH(NET_TO_VEH(serverBD.niVeh[i]),fHealth)
						SET_VEHICLE_ENGINE_HEALTH(NET_TO_VEH(serverBD.niVeh[i]),fHealth)
						
						IF IS_THIS_MODEL_A_PLANE(serverBD.VehModel)
							SET_PLANE_RESIST_TO_EXPLOSION(NET_TO_VEH(serverBD.niVeh[i]),TRUE)
						ENDIF
						
						PRINTLN("     ---------->     KILL LIST - Set UW veh health ", fHealth)
						
						IF IS_THIS_MODEL_A_HELI(serverBD.VehModel)
							IF fHealth > 2000.0
								fHealth = 2000.0
							ENDIF
							SET_HELI_MAIN_ROTOR_HEALTH(NET_TO_VEH(serverBD.niVeh[i]),fHealth)
							SET_HELI_TAIL_ROTOR_HEALTH(NET_TO_VEH(serverBD.niVeh[i]),fHealth)
							
							PRINTLN("     ---------->     KILL LIST - Set UW veh rotor health ", fHealth)
						ENDIF
						
					ELSE
					
						IF IS_THIS_MODEL_A_HELI(serverBD.VehModel)
							fHealth = TO_FLOAT(GET_PLAYER_VEH_START_HEALTH_HELI())
						ELIF IS_THIS_MODEL_A_PLANE(serverBD.VehModel)
							fHealth = TO_FLOAT(GET_PLAYER_VEH_START_HEALTH_PLANE())
						ELSE
							fHealth = TO_FLOAT(GET_PLAYER_VEH_START_HEALTH_TANK())
						ENDIF
					
						SET_ENTITY_HEALTH(NET_TO_VEH(serverBD.niVeh[i]),ROUND(fHealth))
						SET_ENTITY_MAX_HEALTH(NET_TO_VEH(serverBD.niVeh[i]),ROUND(fHealth) )
						SET_VEHICLE_PETROL_TANK_HEALTH(NET_TO_VEH(serverBD.niVeh[i]),fHealth)
						SET_VEHICLE_BODY_HEALTH(NET_TO_VEH(serverBD.niVeh[i]),fHealth)
						SET_VEHICLE_ENGINE_HEALTH(NET_TO_VEH(serverBD.niVeh[i]),fHealth)
						PRINTLN("     ---------->     KILL LIST - Set UW veh health ", fHealth)
					ENDIF
					
					
					SET_VEHICLE_STRONG(NET_TO_VEH(serverBD.niVeh[i]), TRUE)
					
					IF serverBD.VehModel = SAVAGE
						//-- Lock co-pilot's seat in Savage helicopter as anyone sat in this seat can't do anything.
					//	SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(NET_TO_VEH(serverBD.niVeh[i]), 1, VEHICLELOCK_LOCKED)
					ENDIF
					
					IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh", DECOR_TYPE_INT)
	                    DECOR_SET_INT(NET_TO_VEH(serverBD.niVeh[i]), "Not_Allow_As_Saved_Veh", MP_NOT_ALLOWED_AS_SAVED_VEHICLE_DEC_SIMEON)
						PRINTLN("     ---------->     KILL LIST - DECORATOR 'Not_Allow_As_Saved_Veh' SET")
	  				ENDIF
					
					//Prevents the vehicle from being used for activities that use passive mode - MJM 
					IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
						INT iDecoratorValue
						IF DECOR_EXIST_ON(NET_TO_VEH(serverBD.niVeh[i]), "MPBitset")
							iDecoratorValue = DECOR_GET_INT(NET_TO_VEH(serverBD.niVeh[i]), "MPBitset")
						ENDIF
						SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_PASSIVE_MODE_VEHICLE) 
						SET_BIT(iDecoratorValue, MP_DECORATOR_BS_KILL_LIST_PLAYER_VEH)
						DECOR_SET_INT(NET_TO_VEH(serverBD.niVeh[i]), "MPBitset", iDecoratorValue)
					ENDIF
					
					SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(NET_TO_VEH(serverBD.niVeh[i]), FALSE, relUWPlayer)
					INT j
					REPEAT NUM_NETWORK_PLAYERS j
						SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(NET_TO_VEH(serverBD.niVeh[i]), FALSE, rgFM_Team[j])
					ENDREPEAT
					
					SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(NET_TO_VEH(serverBD.niVeh[i]), FALSE)
					SET_VEHICLE_CAN_BE_TARGETTED(NET_TO_VEH(serverBD.niVeh[i]), FALSE)
					
					SET_ENTITY_INVINCIBLE(NET_TO_VEH(serverBD.niVeh[i]), TRUE)
					SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(NET_TO_VEH(serverBD.niVeh[i]), FALSE)
					
					
					SET_ALLOW_RAMMING_SOOP_OR_RAMP(NET_TO_VEH(serverBD.niVeh[i]), FALSE)
					#IF IS_DEBUG_BUILD
						tlName = ""
						tlName += "Veh "
						tlName += i
						SET_VEHICLE_NAME_DEBUG(NET_TO_VEH(serverBD.niVeh[i]), tlName)
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - CREATED VEH ", i, " at coords ", serverBD.vStartLocation[i])
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	i = 0
	
	REPEAT serverBD.iNumPlayerVeh i
		//Check we have created the vehicle
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niVeh[i])
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - CREATE_VEH waiting on player veh... ", i)
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

//PURPOSE: Creates the Veh and Driver that will deliver the Vehicle
FUNC BOOL CREATE_MAIN_VEHICLE()
	GET_NUMBER_OF_UW_PLAYER_VEHICLES()
	IF REQUEST_LOAD_MODEL(serverBD.VehModel)
		IF CREATE_VEH()
			SET_MODEL_AS_NO_LONGER_NEEDED(serverBD.VehModel)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_VEHICLE_SPAWN_TIMER_EXPIRED(INT iSpawn)
	IF serverBD.iTimeInitialSpawn[iSpawn] > 0
		IF NOT HAS_NET_TIMER_STARTED(serverBD.timeInitialSpawn[iSpawn])
			START_NET_TIMER(serverBD.timeInitialSpawn[iSpawn])
			
			RETURN FALSE
		ELSE
			IF NOT HAS_NET_TIMER_EXPIRED(serverBD.timeInitialSpawn[iSpawn], serverBD.iTimeInitialSpawn[iSpawn])
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(serverBD.timeLastSpawn[iSpawn])
		IF NOT  HAS_NET_TIMER_EXPIRED(serverBD.timeLastSpawn[iSpawn], 20000)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

///////////////////////////////////////////
///    MERRYWEATHER VEHICLE AND PEDS    ///
///////////////////////////////////////////
//PURPOSE: Create the Vehicle
FUNC BOOL CREATE_AI_VEHICLE(INT thisVeh)

	INT iWaveType = GET_CURRENT_WAVE_TYPE()
	serverBD.MwVehModel = GET_VEHICLE_MODEL_FOR_WAVE_TYPE(iWaveType)
	FLOAT fHealth
	INT iSeatCount
	INT iPedToCheck

	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niMwVeh[thisVeh])
			
		//-- In-veh peds re-use ped indexes. SHouldn't ever try to recreate a vehicle if the ped indexes of the peds
		//-- that are going to fill the vehilce already exist from the previous time this veh was created.
		//-- The peds should have been cleaned up before getting in here
	
		REPEAT serverBD.iVehSeats iSeatCount
			iPedToCheck = (thisVeh*MAX_MW_PEDS_PER_VEH)+iSeatCount
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niMwPed[iPedToCheck])
				#IF IS_DEBUG_BUILD
					
					VECTOR vTempCoord
					IF IS_NET_PED_INJURED(serverBD.niMwPed[iPedToCheck])
						vTempCoord = GET_ENTITY_COORDS(NET_TO_PED(serverBD.niMwPed[iPedToCheck]), FALSE)
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [CREATE_AI_VEHICLE] TRYING TO CREATE VEH ", thisVeh, " BUT IN VEH PED ", iPedToCheck, " ALREADY EXISTS! PED IS INJURED. PED COORDS ",vTempCoord)
					ELSE
						vTempCoord = GET_ENTITY_COORDS(NET_TO_PED(serverBD.niMwPed[iPedToCheck]))
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [CREATE_AI_VEHICLE] TRYING TO CREATE VEH ", thisVeh, " BUT IN VEH PED ", iPedToCheck, " ALREADY EXISTS! PED NOT INJURED. PED COORDS ",vTempCoord)
					ENDIF		
				#ENDIF
				
				RETURN FALSE
			ENDIF
		ENDREPEAT
		

		IF REQUEST_LOAD_MODEL(serverBD.MwVehModel)
		AND NOT IS_BIT_SET(serverBD.iCreateMwVehBitSet, thisVeh)
		//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - CREATE_AI_VEHICLE... 2")
			VECTOR vSpawn
			FLOAT fHeading
			BOOL bCreationOkay
			
			FLOAT fMinPlayerDist
			FLOAT fMinVisDist = 300.0
			
			IF IS_THIS_MODEL_A_HELI(serverBD.MwVehModel)
			OR IS_THIS_MODEL_A_PLANE(serverBD.MwVehModel)
				fMinPlayerDist = 300.0
			ELSE
				fMinPlayerDist = 100.0
			ENDIF
			
			IF IS_THIS_MODEL_A_PLANE(serverBD.MwVehModel)
				fMinVisDist = 800.0
			ENDIF
			
			//Check if creation point is okay
			INT i
			VECTOR vSpawnCand
			FLOAT fSpawnCand
			
			REPEAT serverBD.iMaxVehicles i
			//	IF (NOT HAS_NET_TIMER_STARTED(serverBD.timeLastSpawn[i])
			//		OR (HAS_NET_TIMER_STARTED(serverBD.timeLastSpawn[i]) AND HAS_NET_TIMER_EXPIRED(serverBD.timeLastSpawn[i], 20000)))
				IF HAS_VEHICLE_SPAWN_TIMER_EXPIRED(i)
					
					GET_SPAWN_COORDS_FOR_MERRYWEATHER_VEHICLE_TYPE(serverBD.MwVehModel, i, vSpawnCand, fSpawnCand)
					
					IF NOT ARE_VECTORS_EQUAL(vSpawnCand, << 0.0, 0.0, 0.0>>)
				//		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - CREATE_AI_VEHICLE... 3")
						IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(	vSpawnCand, // VECTOR vPoint, 
														DEFAULT, 						 // FLOAT fVehRadius = 6.0, 
														DEFAULT, 						 // FLOAT fPedRadius = 1.0, 
														DEFAULT,						 // FLOAT fObjRadius = 1.0,
														DEFAULT,						 // FLOAT fViewRadius = 5.0,
														DEFAULT,						 // BOOL bCheckThisPlayerSight = TRUE,
														DEFAULT,						 // BOOL bDoAnyPlayerSeePointCheck = TRUE,
														DEFAULT,						 // BOOL bCheckVisibilityForOwnTeam = TRUE,
														fMinVisDist,					 // FLOAT fVisibleDistance = 120.0,
														DEFAULT, 						 // BOOL bIgnoreLocalPlayerChecks=FALSE, 
														DEFAULT,						 // INT iTeamToIgnore=-1,
														DEFAULT,						 // BOOL bDoFireCheck=TRUE,
														fMinPlayerDist) 				// FLOAT fPlayerRadius = 0.0,
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - CREATE_AI_VEHICLE... 4")
														
							vSpawn = vSpawnCand							  
							fHeading = fSpawnCand							  
							bCreationOkay = TRUE										  
							REINIT_NET_TIMER(serverBD.timeLastSpawn[i])
							PRINTLN("     ---------->     KILL LIST - CREATE_VEHICLE - bCreationOkay = TRUE - LOCATION ", i)
							i = serverBD.iMaxVehicles
						ELSE
						//	PRINTLN("     ---------->     KILL LIST - CREATE_VEHICLE - Failing IS_POINT_OK_FOR_NET_ENTITY_CREATION! fMinPlayerDist = ", fMinPlayerDist, " fMinVisDist = ", fMinVisDist)
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF bCreationOkay = TRUE
				BOOL bFreezeWaitingOnCollision = TRUE
				IF IS_THIS_MODEL_A_HELI(serverBD.MwVehModel)
				OR IS_THIS_MODEL_A_PLANE(serverBD.MwVehModel)
					bFreezeWaitingOnCollision = FALSE
				ENDIF
				IF CHECK_FOR_PLAYER_DESTROYED_VEHICLE_BITSET_RESET(thisVeh)
					IF CREATE_NET_VEHICLE(serverBD.niMwVeh[thisVeh], serverBD.MwVehModel, vSpawn, fHeading, DEFAULT, DEFAULT, DEFAULT, DEFAULT, bFreezeWaitingOnCollision)
					
						//SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(NET_TO_VEH(serverBD.niMwVeh[thisVeh]), TRUE)
						//SET_VEHICLE_TYRES_CAN_BURST(NET_TO_VEH(serverBD.niMwVeh[thisVeh]), FALSE)
						
						//SET_ENTITY_HEALTH(NET_TO_VEH(serverBD.niMwVeh[thisVeh]), serverBD.CrateDropData.LocationData[iLocation].Vehicle[0].iHealth)
						//SET_VEHICLE_ENGINE_HEALTH(NET_TO_VEH(serverBD.niMwVeh[thisVeh]), TO_FLOAT(serverBD.CrateDropData.LocationData[iLocation].Vehicle[0].iHealth))
						//SET_VEHICLE_PETROL_TANK_HEALTH(NET_TO_VEH(serverBD.niMwVeh[thisVeh]), TO_FLOAT(serverBD.CrateDropData.LocationData[iLocation].Vehicle[0].iHealth))
						
					//	SET_VEHICLE_DOORS_LOCKED_FOR_NON_SCRIPT_PLAYERS(NET_TO_VEH(serverBD.niMwVeh[thisVeh]), TRUE)
					
						SET_VEHICLE_DOORS_LOCKED(NET_TO_VEH(serverBD.niMwVeh[thisVeh]), VEHICLELOCK_LOCKED)
						SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(NET_TO_VEH(serverBD.niMwVeh[thisVeh]), TRUE) 
						SET_VEHICLE_AUTOMATICALLY_ATTACHES(NET_TO_VEH(serverBD.niMwVeh[thisVeh]), FALSE)


						SET_VEHICLE_DOORS_LOCKED(NET_TO_VEH(serverBD.niMwVeh[thisVeh]), VEHICLELOCK_LOCKED)
						SET_VEHICLE_ENGINE_ON(NET_TO_VEH(serverBD.niMwVeh[thisVeh]), TRUE, TRUE)
						SET_VEHICLE_INFLUENCES_WANTED_LEVEL(NET_TO_VEH(serverBD.niMwVeh[thisVeh]), FALSE)
						
						SET_ALLOW_RAMMING_SOOP_OR_RAMP(NET_TO_VEH(serverBD.niMwVeh[thisVeh]), FALSE)
						
						IF IS_THIS_MODEL_A_HELI(serverBD.MwVehModel)
							SET_HELI_BLADES_FULL_SPEED(NET_TO_VEH(serverBD.niMwVeh[thisVeh]))
							SET_ENTITY_DYNAMIC(NET_TO_VEH(serverBD.niMwVeh[thisVeh]), TRUE)
							ACTIVATE_PHYSICS(NET_TO_VEH(serverBD.niMwVeh[thisVeh]))
							SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_VEH(serverBD.niMwVeh[thisVeh]), FALSE)
							
						ELIF IS_THIS_MODEL_A_PLANE(serverBD.MwVehModel)
							CONTROL_LANDING_GEAR(NET_TO_VEH(serverBD.niMwVeh[thisVeh]),LGC_RETRACT_INSTANT)
							SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_VEH(serverBD.niMwVeh[thisVeh]), FALSE)
							SET_ENTITY_DYNAMIC(NET_TO_VEH(serverBD.niMwVeh[thisVeh]), TRUE)
							ACTIVATE_PHYSICS(NET_TO_VEH(serverBD.niMwVeh[thisVeh]))
							SET_VEHICLE_FORWARD_SPEED(NET_TO_VEH(serverBD.niMwVeh[thisVeh]), 30.0)  
							SET_PLANE_RESIST_TO_EXPLOSION(NET_TO_VEH(serverBD.niMwVeh[thisVeh]),TRUE)
							SET_VEHICLE_BROKEN_PARTS_DONT_AFFECT_AI_HANDLING(NET_TO_VEH(serverBD.niMwVeh[thisVeh]),TRUE)
							
							fHealth =500.0 
							SET_ENTITY_HEALTH(NET_TO_VEH(serverBD.niMwVeh[thisVeh]),ROUND(fHealth))
						//	SET_ENTITY_MAX_HEALTH(NET_TO_VEH(serverBD.niMwVeh[thisVeh]),ROUND(fHealth) )
						//	SET_VEHICLE_PETROL_TANK_HEALTH(NET_TO_VEH(serverBD.niMwVeh[thisVeh]),fHealth)
						//	SET_VEHICLE_BODY_HEALTH(NET_TO_VEH(serverBD.niMwVeh[thisVeh]),fHealth)
						//	SET_VEHICLE_ENGINE_HEALTH(NET_TO_VEH(serverBD.niMwVeh[thisVeh]),fHealth)
							
							PRINTLN("     ---------->     KILL LIST - CREATED VEHICLE AI PLANE SET SET_PLANE_RESIST_TO_EXPLOSION VEH ", thisVeh, " HEALTH ",fHealth)
						ELSE
							SET_ENTITY_LOAD_COLLISION_FLAG(NET_TO_VEH(serverBD.niMwVeh[thisVeh]), TRUE)
						ENDIF
						
						IF GET_CURRENT_WAVE_TYPE() = WAVE_TYPE_BOSS
							fHealth = TO_FLOAT(GET_BOSS_HEALTH()) // 3000.0
							SET_ENTITY_HEALTH(NET_TO_VEH(serverBD.niMwVeh[thisVeh]),ROUND(fHealth))
							SET_ENTITY_MAX_HEALTH(NET_TO_VEH(serverBD.niMwVeh[thisVeh]),ROUND(fHealth) )
							SET_VEHICLE_PETROL_TANK_HEALTH(NET_TO_VEH(serverBD.niMwVeh[thisVeh]),fHealth)
							SET_VEHICLE_BODY_HEALTH(NET_TO_VEH(serverBD.niMwVeh[thisVeh]),fHealth)
							SET_VEHICLE_ENGINE_HEALTH(NET_TO_VEH(serverBD.niMwVeh[thisVeh]),fHealth)
							SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(NET_TO_VEH(serverBD.niMwVeh[thisVeh]),FALSE)
							IF fHealth > 2000.0
								fHealth = 2000.0
							ENDIF
							SET_HELI_MAIN_ROTOR_HEALTH(NET_TO_VEH(serverBD.niMwVeh[thisVeh]),fHealth)
							SET_HELI_TAIL_ROTOR_HEALTH(NET_TO_VEH(serverBD.niMwVeh[thisVeh]),fHealth)
							PRINTLN("     ---------->     KILL LIST - [CREATE_AI_VEHICLE] SET VEHICLE HIGH HEALTH AS BOSS VEH ")
						ENDIF
						
						#IF IS_DEBUG_BUILD
							FLOAT fDist = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niMwVeh[thisVeh]))
							//NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST - CREATED VEHICLE  - thisVeh = " , thisVeh " Location ") NET_PRINT_VECTOR(vSpawn) NET_PRINT(" DISTANCE FROM PLAYER = ")NET_PRINT_FLOAT(fDist)  NET_NL()
							PRINTLN("     ---------->     KILL LIST - CREATED VEHICLE  - VEH = " , thisVeh, " Location = ", vSpawn, " DISTANCE FROM PLAYER = ", fDist)
						#ENDIF
						
						IF HAS_NET_TIMER_STARTED(serverBD.timeCleanupVeh[thisVeh])
							RESET_NET_TIMER(serverBD.timeCleanupVeh[thisVeh])
						ENDIF
					
					//	SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(NET_TO_VEH(serverBD.niMwVeh[thisVeh]), TRUE)
					//	SET_ENTITY_CAN_ONLY_BE_DAMAGED_BY_SCRIPT_PARTICIPANTS(NET_TO_VEH(serverBD.niMwVeh[thisVeh]), TRUE)
						CLEAR_BIT(serverbd.iVehCleanedUpBitset, thisVeh)
						SET_BIT(serverBD.iCreateMwVehBitSet, thisVeh) 
						
						SET_ENTITY_ONLY_DAMAGED_BY_RELATIONSHIP_GROUP(NET_TO_VEH(serverBD.niMwVeh[thisVeh]), TRUE, relUWPlayer)
						
						IF IS_BIT_SET(serverBD.iTargetVehDestroyedBitSet, thisVeh)
							CLEAR_BIT(serverBD.iTargetVehDestroyedBitSet, thisVeh)
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] SERVER BIT WAS SET BUT VEH RECREATED  ", thisVeh)
						ENDIF
						
						IF IS_BIT_SET(serverBD.iVehEmptyBitSet,thisVeh )
							CLEAR_BIT(serverBD.iVehEmptyBitSet, thisVeh)
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] SERVER EMPTY BIT WAS SET BUT VEH RECREATED  ", thisVeh)
						ENDIF
						
						IF serverBD.MwVehModel = HYDRA
						OR serverBD.MwVehModel = SAVAGE
						//	ACTIVATE_DAMAGE_TRACKER_ON_NETWORK_ID(serverBD.niMwVeh[thisVeh], TRUE)
						//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - ACTIVATE_DAMAGE_TRACKER_ON_NETWORK_ID veh ", thisVeh)
						ENDIF
						#IF IS_DEBUG_BUILD
							TEXT_LABEL_15 tl15VehName = "Veh "
							tl15VehName += thisVeh
							SET_VEHICLE_NAME_DEBUG(NET_TO_VEH(serverBD.niMwVeh[thisVeh]), tl15VehName)
						#ENDIF
						PRINTLN("     ---------->     KILL LIST - iCreateMwVehBitSet ", thisVeh, " SET")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Check we have created the vehicle
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niMwVeh[thisVeh])
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL DOES_UW_VEHICLE_HAVE_DRIVER(INT iVeh)
	IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[iVeh])
		IF GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(serverBD.niVeh[iVeh])) <> NULL
			#IF IS_DEBUG_BUILD
			//	PED_INDEX pedTemp  = GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(serverBD.niVeh[iVeh]))
			//	PLAYER_INDEX playerTemp
			//	IF IS_PED_A_PLAYER(pedTemp)
			//		playerTemp = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedTemp)
			//		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [DOES_UW_VEHICLE_HAVE_DRIVER] THIS PLAYER IS DRIVER ", GET_PLAYER_NAME(playerTemp))
			//	ENDIF
			#ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [DOES_UW_VEHICLE_HAVE_DRIVER] FALSE")
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_MY_VALKYRIE_HAVE_ENOUGH_PLAYERS()
	IF serverBD.VehModel <> VALKYRIE
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [DOES_MY_VALKYRIE_HAVE_ENOUGH_PLAYERS] TRUE - NOT A VALKYIRE")
		RETURN TRUE
	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [DOES_MY_VALKYRIE_HAVE_ENOUGH_PLAYERS] FALSE - NOT IN A VEHICLE")
		RETURN FALSE
	ENDIF
	
	VEHICLE_INDEX vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	INT iNumSeats = GET_VEHICLE_MODEL_NUMBER_OF_SEATS(VALKYRIE)
	INT i
	INT iCount
	
	PED_INDEX pedTemp
	
	REPEAT iNumSeats i
		pedTemp = GET_PED_IN_VEHICLE_SEAT(vehTemp, INT_TO_ENUM(VEHICLE_SEAT, i)) 
		IF pedTemp <> NULL
			IF NOT IS_PED_INJURED(pedTemp)
				IF IS_PED_A_PLAYER(pedTemp)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [DOES_MY_VALKYRIE_HAVE_ENOUGH_PLAYERS] FOUND A PED IN SEAT ", i, " PASS COUNT ", iCount)
					iCount++
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF iCount > 0
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [DOES_MY_VALKYRIE_HAVE_ENOUGH_PLAYERS] TRUE - iCount = ", iCount)
		RETURN TRUE
	ENDIF
	
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [DOES_MY_VALKYRIE_HAVE_ENOUGH_PLAYERS] FALSE - iCount = ", iCount)
	RETURN FALSE
ENDFUNC
FUNC BOOL ARE_ALL_UW_PLAYER_VEHICLES_FULL()
	INT iNumVeh = GET_NUMBER_OF_UW_PLAYER_VEHICLES()
	INT i = 0
	
	REPEAT iNumVeh i
		IF NOT DOES_UW_VEHICLE_HAVE_DRIVER(i)
			RESET_NET_TIMER(serverBD.inVehicleTimer)
			RETURN FALSE
		ENDIF
		IF NOT IS_BIT_SET(serverBD.iVehicleFullBitSet, i)
			RESET_NET_TIMER(serverBD.inVehicleTimer)
			RETURN FALSE
		ELSE
			IF IS_VEHICLE_EMPTY(NET_TO_VEH(serverBD.niVeh[i]), TRUE)
				RESET_NET_TIMER(serverBD.inVehicleTimer)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF NOT HAS_NET_TIMER_STARTED(serverBD.inVehicleTimer)
		START_NET_TIMER(serverBD.inVehicleTimer)
	ELSE
		IF HAS_NET_TIMER_EXPIRED(serverBD.inVehicleTimer, 1500)	
			RETURN TRUE
		ENDIF
	ENDIF
		RETURN FALSE
ENDFUNC

//PURPOSE: Load and Create the Peds in the Vehicle
FUNC BOOL CREATE_IN_VEHICLE_PEDS(INT thisVeh)
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_15 tl15Name
	#ENDIF
	
	FLOAT fBreakLockAngle			
	FLOAT fBreakLockAngleClose		
	FLOAT fBreakLockCloseDistance	
	FLOAT fTurnRateModifier
	VEHICLE_SEAT vsSeatToUse
	FLOAT fAccuracy
	
	IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niMwVeh[thisVeh])
	AND IS_BIT_SET(serverBD.iCreateMwVehBitSet, thisVeh)
		IF TAKE_CONTROL_OF_NET_ID(serverBD.niMwVeh[thisVeh])
			IF serverBD.iVehSeats <= 0
				IF GET_ENTITY_MODEL(NET_TO_VEH(serverBD.niMwVeh[thisVeh])) = INSURGENT
					serverBD.iVehSeats = 2
					PRINTLN("     ---------->     KILL LIST INSURGENT - iVehSeats = ", serverBD.iVehSeats)
				ELSE
					serverBD.iVehSeats = (GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(NET_TO_VEH(serverBD.niMwVeh[thisVeh])) + 1)
					IF serverBD.iVehSeats > MAX_MW_PEDS_PER_VEH
						serverBD.iVehSeats = MAX_MW_PEDS_PER_VEH
					ENDIF
					PRINTLN("     ---------->     KILL LIST - iVehSeats = ", serverBD.iVehSeats)
				ENDIF
			ENDIF
			
			INT i	
			REPEAT serverBD.iVehSeats i
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i])
					IF REQUEST_LOAD_MODEL(serverBD.MwPedModel)
						vsSeatToUse =  INT_TO_ENUM(VEHICLE_SEAT, i-1)
						IF GET_ENTITY_MODEL(NET_TO_VEH(serverBD.niMwVeh[thisVeh])) = INSURGENT
							IF vsSeatToUse = VS_FRONT_RIGHT
								//-- Use the gun turret
								vsSeatToUse = VS_EXTRA_LEFT_3
							ENDIF
						ENDIF
						
						IF CREATE_NET_PED_IN_VEHICLE(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i], serverBD.niMwVeh[thisVeh], PEDTYPE_CRIMINAL, serverBD.MwPedModel,vsSeatToUse)
					//		SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), rgFM_AiHate)	//PROBABLY NEED TO SET UP NEW RELGROUPS FOR PLAYERS AND MW PEDS FOR THIS MISSION
							SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), relUWAi)
							SET_ENTITY_IS_TARGET_PRIORITY(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), TRUE)
							SET_PED_RANDOM_COMPONENT_VARIATION(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]))
							
							GIVE_DELAYED_WEAPON_TO_PED(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), WEAPONTYPE_APPISTOL, 25000, FALSE)
							IF GET_RANDOM_BOOL()
								//GIVE_DELAYED_WEAPON_TO_PED(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), WEAPONTYPE_RPG, 25000, TRUE)
								GIVE_DELAYED_WEAPON_TO_PED(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), WEAPONTYPE_ADVANCEDRIFLE, 25000, TRUE)
							ELSE
								GIVE_DELAYED_WEAPON_TO_PED(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), WEAPONTYPE_ADVANCEDRIFLE, 25000, TRUE)
							ENDIF
							
							fAccuracy = 10.0
							fAccuracy *= GET_ENEMY_ACCURACY_MULTIPLIER()
							SET_PED_ACCURACY(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), ROUND(fAccuracy))	//TUNABLE?
							
							//Set Behaviours
							SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), CA_DO_DRIVEBYS, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), CA_LEAVE_VEHICLES, FALSE)
							SET_PED_COMBAT_ABILITY(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), CAL_AVERAGE)
							SET_PED_COMBAT_MOVEMENT(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), CM_WILLADVANCE)
							SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), CA_USE_COVER, TRUE)
							SET_PED_TARGET_LOSS_RESPONSE(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), TLR_NEVER_LOSE_TARGET)
							SET_PED_HIGHLY_PERCEPTIVE(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), TRUE)
							
							SET_ENTITY_HEALTH(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), ROUND(200*g_sMPTunables.fAiHealthModifier))
							
							SET_PED_AS_ENEMY(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), TRUE)
							
							SET_PED_KEEP_TASK(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), TRUE)
					//		TASK_COMBAT_HATED_TARGETS_IN_AREA(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), serverBD.vStartLocation[0], 250)
							
							SET_PED_CONFIG_FLAG(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), PCF_DontInfluenceWantedLevel, TRUE)
							
							SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), CA_USE_VEHICLE_ATTACK,TRUE)
							SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), CA_USE_VEHICLE_ATTACK_IF_VEHICLE_HAS_MOUNTED_GUNS,TRUE)
							
							SET_PED_DIES_IN_WATER(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), TRUE)
							SET_PED_DIES_INSTANTLY_IN_WATER(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), TRUE)
							SET_PED_DIES_IN_SINKING_VEHICLE(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), TRUE)
							
							SET_PED_CONFIG_FLAG(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), PCF_CanActivateRagdollWhenVehicleUpsideDown, FALSE)
							
					//		SET_ENTITY_INVINCIBLE(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), TRUE)
							IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(NET_TO_VEH(serverBD.niMwVeh[thisVeh])))
								IF i = 0
									SET_PED_SEEING_RANGE(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), 299)
									
									fAccuracy = 3.0
									fAccuracy *= GET_ENEMY_MISSILE_ACCURACY_MULTIPLIER()
									SET_PED_ACCURACY(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]),ROUND(fAccuracy))
									SET_PED_SHOOT_RATE(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]),50)
									SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), CA_USE_VEHICLE_ATTACK,TRUE)
									SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), CA_USE_VEHICLE_ATTACK_IF_VEHICLE_HAS_MOUNTED_GUNS,TRUE)
								
									SET_PED_FIRING_PATTERN(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), FIRING_PATTERN_BURST_FIRE_HELI)
									
									
								
						//			NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST - SET PED FIRING PATTERN FIRING_PATTERN_BURST_FIRE_HELI - ") NET_PRINT(" PED ") NET_PRINT_INT((thisVeh*MAX_MW_PEDS_PER_VEH)+i) NET_NL()
								ENDIF
								
						//		Fill_MP_Float_Tunable(TUNE_CONTEXT_MP_GLOBALS, TUNE_CONTEXT_FM_TYPE_CONTENT_MODIFIER_NONE, HASH( "CCF_HOMING_ROCKET_TURN_RATE_MODIFIER"), g_sMPTunables.fCCF_HOMING_ROCKET_TURN_RATE_MODIFIER)
						//		Fill_MP_Float_Tunable(TUNE_CONTEXT_MP_GLOBALS, TUNE_CONTEXT_FM_TYPE_CONTENT_MODIFIER_NONE, HASH( "HOMING_ROCKET_BREAK_LOCK_ANGLE"), g_sMPTunables.fHOMING_ROCKET_BREAK_LOCK_ANGLE )
						//		Fill_MP_Float_Tunable(TUNE_CONTEXT_MP_GLOBALS, TUNE_CONTEXT_FM_TYPE_CONTENT_MODIFIER_NONE, HASH( "HOMING_ROCKET_BREAK_LOCK_ANGLE_CLOSE"), g_sMPTunables.fHOMING_ROCKET_BREAK_LOCK_ANGLE_CLOSE)
						//		Fill_MP_Float_Tunable(TUNE_CONTEXT_MP_GLOBALS, TUNE_CONTEXT_FM_TYPE_CONTENT_MODIFIER_NONE, HASH( "HOMING_ROCKET_BREAK_LOCK_CLOSE_DISTANCE"), g_sMPTunables.fHOMING_ROCKET_BREAK_LOCK_CLOSE_DISTANCE)
								


								fBreakLockAngle				= g_sMPTunables.furbanw_homing_rocket_break_lock_angle 			
								fBreakLockAngleClose		= g_sMPTunables.furbanw_homing_rocket_break_lock_angle_close		
								fBreakLockCloseDistance		= g_sMPTunables.furbanw_homing_rocket_break_lock_close_distance	
								fTurnRateModifier			= g_sMPTunables.furbanw_homing_rocket_turn_rate_modifier    		
								
								SET_COMBAT_FLOAT(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), CCF_HOMING_ROCKET_BREAK_LOCK_ANGLE, fBreakLockAngle)
								SET_COMBAT_FLOAT(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), CCF_HOMING_ROCKET_BREAK_LOCK_ANGLE_CLOSE, fBreakLockAngleClose)
								SET_COMBAT_FLOAT(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), CCF_HOMING_ROCKET_BREAK_LOCK_CLOSE_DISTANCE, fBreakLockCloseDistance)
								SET_COMBAT_FLOAT(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), CCF_HOMING_ROCKET_TURN_RATE_MODIFIER, fTurnRateModifier)
								
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [CREATE_IN_VEHICLE_PEDS] PED ", (thisVeh*MAX_MW_PEDS_PER_VEH)+i, " CREATED IN HELI VEH ", thisVeh, " fBreakLockAngle = ", fBreakLockAngle, " fBreakLockAngleClose = ", fBreakLockAngleClose, " fBreakLockCloseDistance = ", fBreakLockCloseDistance, " fTurnRateModifier = ", fTurnRateModifier)
							ELIF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(NET_TO_VEH(serverBD.niMwVeh[thisVeh])))
								SET_PED_SEEING_RANGE(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), 299)
								
								fAccuracy = 3.0
								fAccuracy *= GET_ENEMY_MISSILE_ACCURACY_MULTIPLIER()
								SET_PED_ACCURACY(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]),ROUND(fAccuracy))
								SET_PED_SHOOT_RATE(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]),50)
								SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), CA_USE_VEHICLE_ATTACK,TRUE)
								SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), CA_USE_VEHICLE_ATTACK_IF_VEHICLE_HAS_MOUNTED_GUNS,TRUE)
			
//								Fill_MP_Float_Tunable(TUNE_CONTEXT_MP_GLOBALS, TUNE_CONTEXT_FM_TYPE_CONTENT_MODIFIER_NONE, HASH( "CCF_HOMING_ROCKET_TURN_RATE_MODIFIER"), g_sMPTunables.fCCF_HOMING_ROCKET_TURN_RATE_MODIFIER)
//								Fill_MP_Float_Tunable(TUNE_CONTEXT_MP_GLOBALS, TUNE_CONTEXT_FM_TYPE_CONTENT_MODIFIER_NONE, HASH( "HOMING_ROCKET_BREAK_LOCK_ANGLE"), g_sMPTunables.fHOMING_ROCKET_BREAK_LOCK_ANGLE )
//								Fill_MP_Float_Tunable(TUNE_CONTEXT_MP_GLOBALS, TUNE_CONTEXT_FM_TYPE_CONTENT_MODIFIER_NONE, HASH( "HOMING_ROCKET_BREAK_LOCK_ANGLE_CLOSE"), g_sMPTunables.fHOMING_ROCKET_BREAK_LOCK_ANGLE_CLOSE)
//								Fill_MP_Float_Tunable(TUNE_CONTEXT_MP_GLOBALS, TUNE_CONTEXT_FM_TYPE_CONTENT_MODIFIER_NONE, HASH( "HOMING_ROCKET_BREAK_LOCK_CLOSE_DISTANCE"), g_sMPTunables.fHOMING_ROCKET_BREAK_LOCK_CLOSE_DISTANCE)

								fBreakLockAngle				= g_sMPTunables.furbanw_homing_rocket_break_lock_angle 			
								fBreakLockAngleClose		= g_sMPTunables.furbanw_homing_rocket_break_lock_angle_close	
								fBreakLockCloseDistance		= g_sMPTunables.furbanw_homing_rocket_break_lock_close_distance	
								fTurnRateModifier			= g_sMPTunables.furbanw_homing_rocket_turn_rate_modifier    	
								
								SET_COMBAT_FLOAT(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), CCF_HOMING_ROCKET_BREAK_LOCK_ANGLE, fBreakLockAngle)
								SET_COMBAT_FLOAT(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), CCF_HOMING_ROCKET_BREAK_LOCK_ANGLE_CLOSE, fBreakLockAngleClose)
								SET_COMBAT_FLOAT(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), CCF_HOMING_ROCKET_BREAK_LOCK_CLOSE_DISTANCE, fBreakLockCloseDistance)
								SET_COMBAT_FLOAT(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), CCF_HOMING_ROCKET_TURN_RATE_MODIFIER, fTurnRateModifier)
								
					//			PRINTLN("     ---------->     KILL LIST - ped homing missile iPedNumber = ", ((thisVeh*MAX_MW_PEDS_PER_VEH)+i), " CCF_HOMING_ROCKET_BREAK_LOCK_ANGLE = ", fBreakLockAngle, " CCF_HOMING_ROCKET_BREAK_LOCK_ANGLE_CLOSE = ", fBreakLockAngleClose,  " CCF_HOMING_ROCKET_BREAK_LOCK_CLOSE_DISTANCE = ", fBreakLockAngleClose, " CCF_HOMING_ROCKET_TURN_RATE_MODIFIER = ", fTurnRateModifier) 
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [CREATE_IN_VEHICLE_PEDS] PED ", (thisVeh*MAX_MW_PEDS_PER_VEH)+i, " CREATED IN PLANE VEH ", thisVeh, " fBreakLockAngle = ", fBreakLockAngle, " fBreakLockAngleClose = ", fBreakLockAngleClose, " fBreakLockCloseDistance = ", fBreakLockCloseDistance, " fTurnRateModifier = ", fTurnRateModifier)
							ENDIF
							
							INT iPedIndex = (thisVeh*MAX_MW_PEDS_PER_VEH)+i
							IF IS_BIT_SET(serverBD.iServerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iPedIndex)], GET_LONG_BITSET_BIT(iPedIndex))
								CLEAR_BIT(serverBD.iServerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iPedIndex)], GET_LONG_BITSET_BIT(iPedIndex))
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] SERVER BIT WAS SET BUT IN VEH PED RECREATED  ", iPedIndex)
							ENDIF
					
							SET_PED_DIES_WHEN_INJURED(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]),TRUE)
							SET_PED_KEEP_TASK(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]),TRUE)
							
					
					//		SET_ENTITY_CAN_ONLY_BE_DAMAGED_BY_SCRIPT_PARTICIPANTS(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), TRUE)
					//		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), TRUE)
							SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), KNOCKOFFVEHICLE_NEVER)
							
							SET_ENTITY_ONLY_DAMAGED_BY_RELATIONSHIP_GROUP(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), TRUE, relUWPlayer)
							
							serverBD.iTotalUwPedsSpawned++
							#IF IS_DEBUG_BUILD
								tl15Name = ""
								tl15Name = "Ped "
								tl15Name += (thisVeh*MAX_MW_PEDS_PER_VEH)+i
								SET_PED_NAME_DEBUG(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), tl15Name)
							#ENDIF
							
							RESET_NET_TIMER(serverBD.timePedRespawn[(thisVeh*MAX_MW_PEDS_PER_VEH)+i])
							START_NET_TIMER(serverBD.timePedRespawn[(thisVeh*MAX_MW_PEDS_PER_VEH)+i])
							
							IF HAS_NET_TIMER_STARTED(serverBD.timeCleanupPeds[(thisVeh*MAX_MW_PEDS_PER_VEH)+i])
								RESET_NET_TIMER(serverBD.timeCleanupPeds[(thisVeh*MAX_MW_PEDS_PER_VEH)+i])
							ENDIF
							
					//		NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST - CREATED PED IN VEHICLE - ") NET_PRINT(" PED ") NET_PRINT_INT((thisVeh*MAX_MW_PEDS_PER_VEH)+i) NET_PRINT(" VEH ")NET_PRINT_INT(thisVeh)NET_NL()
							PRINTLN("     ---------->     KILL LIST - CREATED PED ", (thisVeh*MAX_MW_PEDS_PER_VEH)+i, " IN VEH ", thisVeh, " TOTAL PEDS SPAWNED iTotalUwPedsSpawned = ", serverBD.iTotalUwPedsSpawned)
						ENDIF
						
					ENDIF
				ELSE
			//		PRINTLN("     ---------->     KILL LIST CREATE_IN_VEHICLE_PEDS Failing ")
				ENDIF
			ENDREPEAT
			
			i = 0
			
			//Check we have created all Peds
			REPEAT serverBD.iVehSeats i
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i])
					RETURN FALSE
				ELSE
					IF IS_NET_PED_INJURED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i])
						PRINTLN("     ---------->     KILL LIST CREATE_IN_VEHICLE_PEDS Returning False as ped is injured ped = ", (thisVeh*MAX_MW_PEDS_PER_VEH)+i, " serverBD.iVehSeat = ", serverBD.iVehSeats)
						RETURN FALSE
					ELSE 
						IF NOT IS_PED_SITTING_IN_VEHICLE(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+i]), NET_TO_VEH(serverBD.niMwVeh[thisVeh]))
							PRINTLN("     ---------->       ped = ", (thisVeh*MAX_MW_PEDS_PER_VEH)+i, " serverBD.iVehSeat = ", serverBD.iVehSeats, " thisVeh = ", thisVeh)
							RETURN FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
			PRINTLN("     ---------->     KILL LIST CREATE_IN_VEHICLE_PEDS Returning True thisVeh = ", thisVeh, "- iVehSeats = ", serverBD.iVehSeats)
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC RESET_CURRENT_WAVE()
	#IF IS_DEBUG_BUILD
		PRINTLN("     ---------->     KILL LIST RESET_CURRENT_WAVE called. Callstack... ")
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	serverbd.iVehicleSpawnedThisWave = 0
	serverbd.iVehKillsThisWave = 0
	
	serverbd.iPedOnFootSpawnedThisWave = 0
	serverBD.iPedOnFootKillsThisWave = 0
ENDPROC


FUNC INT GET_NUMBER_OF_VEHICLES_IN_CURRENT_WAVE()
	RETURN (serverbd.iNumVehiclesInWave[serverbd.iCurrentWave])
ENDFUNC

FUNC INT GET_NUMBER_OF_GROUND_PEDS_IN_CURRENT_WAVE()
	RETURN (serverbd.iNumOnFootPedsInWave[serverbd.iCurrentWave])
ENDFUNC

//PURPOSE: Creates the Vehicle and the Peds
FUNC BOOL CREATE_VEHICLE_AND_PEDS(INT thisVeh)
	IF CAN_REGISTER_MISSION_ENTITIES(4, 1, 0, 0)
		IF CREATE_AI_VEHICLE(thisVeh)
			IF CREATE_IN_VEHICLE_PEDS(thisVeh)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Creates the Veh and Peds inside
FUNC BOOL CREATE_MW_VEH_AND_PEDS(INT thisVeh)
	INT iMaxVehAtOnce = MAX_VEH_AT_ONCE
	INT iNumInCurrentWave
	//CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - CREATE_MW_VEH_AND_PEDS iMaxVehAtOnce = ", iMaxVehAtOnce, " iMwTotalVehiclesSpawned = ", serverBD.iMwTotalVehiclesSpawned)
	IF GET_CURRENT_WAVE_TYPE() = WAVE_TYPE_GROUND_PEDS_ONLY
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - CREATE_MW_VEH_AND_PEDS  Exit 3 WAVE_TYPE_GROUND_PEDS_ONLY ")
		RETURN FALSE
	ENDIF
	
	IF serverbd.iMaxVehicles < iMaxVehAtOnce
	//	iMaxVehAtOnce = serverbd.iMaxVehicles
	ENDIF
	
//	IF GET_CURRENT_WAVE_TYPE() > WAVE_TYPE_NONE
//		IF serverbd.iMaxVehicles > GET_NUMBER_OF_VEHICLES_IN_CURRENT_WAVE()
//			IF GET_NUMBER_OF_VEHICLES_IN_CURRENT_WAVE() > 0
//				iMaxVehAtOnce = GET_NUMBER_OF_VEHICLES_IN_CURRENT_WAVE()
//			ENDIF
//		ENDIF
//	ENDIF
	
	IF serverbd.iVehiclesSpawned >= iMaxVehAtOnce
	//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - CREATE_MW_VEH_AND_PEDS  Exit 1 serverbd.iVehiclesSpawned = ", serverbd.iVehiclesSpawned, "  >= iMaxVehAtOnce = ", iMaxVehAtOnce)
		RETURN FALSE
	ENDIF
	
	
	IF GET_CURRENT_WAVE_TYPE() > WAVE_TYPE_NONE
		iNumInCurrentWave = GET_NUMBER_OF_VEHICLES_IN_CURRENT_WAVE()
		IF serverbd.iVehicleSpawnedThisWave >= iNumInCurrentWave
		//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - CREATE_MW_VEH_AND_PEDS  Exit 2 serverbd.iVehicleSpawnedThisWave = ", serverbd.iVehicleSpawnedThisWave, "  >= iNumInCurrentWave = ", iNumInCurrentWave)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF REQUEST_LOAD_MODEL(serverBD.MwVehModel)
	AND REQUEST_LOAD_MODEL(serverBD.MwPedModel)
		IF CREATE_VEHICLE_AND_PEDS(thisVeh)
			IF IS_BIT_SET(serverBD.iCreateMwVehBitSet, thisVeh)
				serverBD.iMwTotalVehiclesSpawned++
				serverbd.iVehiclesSpawned++
				serverbd.iVehicleSpawnedThisWave++
				PRINTLN("     ---------->     KILL LIST - iMwTotalVehiclesSpawned = ", serverBD.iMwTotalVehiclesSpawned, " serverbd.iVehiclesSpawned = ", serverbd.iVehiclesSpawned, " iVehicleSpawnedThisWave = ", serverBD.iVehicleSpawnedThisWave)
				CLEAR_BIT(serverBD.iCreateMwVehBitSet, thisVeh)
				PRINTLN("     ---------->     KILL LIST - iCreateMwVehBitSet ", thisVeh, " CLEARED")
				
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_CreatedAtLeastOneVeh)
					SET_BIT(serverBD.iServerBitSet, biS_CreatedAtLeastOneVeh)
					PRINTLN("     ---------->     KILL LIST - [CREATE_MW_VEH_AND_PEDS] SET biS_CreatedAtLeastOneVeh") 
				ENDIF
		//		BROADCAST_KILL_LIST_SET_VEHICLE_DAMAGEABLE_BY_REL_GROUP(thisVeh, ALL_PLAYERS_ON_SCRIPT())
			ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL CREATE_ON_FOOT_PED(INT thisPed)
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_15 tl15Name
	#ENDIF
	
	INT iNextWave
	INT iNextWaveType
	BOOL bNextWaveBoss
	
	//-- Don't create any on foot peds if its an aircraft / boss only wave type
	//-- unless there still some waiting to spawn from previous on-ground wave types
	IF serverbd.iTotalOnFootPedSpawns = 0
	OR serverbd.iTotalOnFootPedSpawns = GET_TOTAL_NUMBER_OF_ON_FOOT_PED_SPAWNS_FOR_THIS_VARIATION()
		IF GET_CURRENT_WAVE_TYPE() = WAVE_TYPE_AIRCRAFT_ONLY
		OR GET_CURRENT_WAVE_TYPE() = WAVE_TYPE_BOSS
			#IF IS_DEBUG_BUILD
				IF bWdTimedDebug
					PRINTLN("     ---------->     KILL LIST CREATE_ON_FOOT_PED FALSE 1 ")
				ENDIF
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF GET_CURRENT_WAVE_TYPE() = WAVE_TYPE_GROUND_PEDS_ONLY
	OR GET_CURRENT_WAVE_TYPE() = WAVE_TYPE_GROUND_PEDS_AND_VEHS
		IF serverbd.iPedOnFootSpawnedThisWave >= GET_NUMBER_OF_GROUND_PEDS_IN_CURRENT_WAVE()
			//-- For 2476688
			//-- Won't update to WAVE_TYPE_BOSS if there's more peds still to come
			//-- So if the next wave is a Boss, ignore this check so more peds can be spawned.
			iNextWave = serverbd.iCurrentWave + 1
			IF iNextWave < MAX_NUM_WAVES
				iNextWaveType = serverBD.iWaveType[iNextWave]
				IF iNextWaveType = WAVE_TYPE_BOSS
					bNextWaveBoss = TRUE
				ENDIF
			ENDIF
			
			IF NOT bNextWaveBoss
				#IF IS_DEBUG_BUILD
					IF bWdTimedDebug
						PRINTLN("     ---------->     KILL LIST CREATE_ON_FOOT_PED FALSE 2 ")
					ENDIF
				#ENDIF
				
				RETURN FALSE
			ENDIF
		ENDIF
	ELSE
		//-- Allow a max of 2 on-foot ped spawns for Aircraft wavetypes
		IF serverBD.iPedOnFootSpawnedThisWave = 2
			//-- For 2480956
			//-- Won't update to WAVE_TYPE_BOSS if there's more peds still to come
			//-- So if the next wave is a Boss, ignore this check so more peds can be spawned.
			iNextWave = serverbd.iCurrentWave + 1
			IF iNextWave < MAX_NUM_WAVES
				iNextWaveType = serverBD.iWaveType[iNextWave]
				IF iNextWaveType = WAVE_TYPE_BOSS
					bNextWaveBoss = TRUE
				ENDIF
			ENDIF
			
			IF NOT bNextWaveBoss
				#IF IS_DEBUG_BUILD
					IF bWdTimedDebug
						PRINTLN("     ---------->     KILL LIST CREATE_ON_FOOT_PED FALSE 3 ")
					ENDIF
				#ENDIF
				
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF serverbd.iTotalOnFootPedSpawns = GET_TOTAL_NUMBER_OF_ON_FOOT_PED_SPAWNS_FOR_THIS_VARIATION()
		#IF IS_DEBUG_BUILD
			IF bWdTimedDebug
				PRINTLN("     ---------->     KILL LIST CREATE_ON_FOOT_PED FALSE 4 ")
			ENDIF
		#ENDIF
			
		RETURN FALSE
	ENDIF
	
	IF serverbd.iTotalOnFootPedSpawns = GET_MAX_NUMBER_OF_ON_FOOT_SPAWNS_SO_FAR()
		#IF IS_DEBUG_BUILD
			IF bWdTimedDebug
				PRINTLN("     ---------->     KILL LIST CREATE_ON_FOOT_PED FALSE 6 ")
			ENDIF
		#ENDIF
			
		RETURN FALSE
	ENDIF
	
	IF GET_CURRENT_WAVE_TYPE() = WAVE_TYPE_NONE
		#IF IS_DEBUG_BUILD
			IF bWdTimedDebug
				PRINTLN("     ---------->     KILL LIST CREATE_ON_FOOT_PED FALSE 5 ")
			ENDIF
		#ENDIF
			
		RETURN FALSE
	ENDIF
	
	
	INT iSpawnIndex
	INT iHEalth
	FLOAT fHealthMult
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niMwPed[(MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed])
		IF REQUEST_LOAD_MODEL(serverBD.MwPedModel)
		AND CAN_REGISTER_MISSION_ENTITIES(0, 1, 0, 0)
			IF GET_SPAWN_COORDS_FOR_IN_FOOT_MERRYWEATHER_PED(iSpawnIndex)
			 	IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(serverBD.vMwPedSpawn[iSpawnIndex])
					IF CREATE_NET_PED(serverBD.niMwPed[(MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed], PEDTYPE_CRIMINAL, serverBD.MwPedModel, serverBD.vMwPedSpawn[iSpawnIndex], serverBD.fMwPedSpawn[iSpawnIndex])
					//	SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(serverBD.niMwPed[(MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed]), rgFM_AiHate)
						SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(serverBD.niMwPed[(MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed]), relUWAi)
						SET_ENTITY_IS_TARGET_PRIORITY(NET_TO_PED(serverBD.niMwPed[(MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed]), TRUE)
						SET_PED_RANDOM_COMPONENT_VARIATION(NET_TO_PED(serverBD.niMwPed[(MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed]))
						
					//	GIVE_DELAYED_WEAPON_TO_PED(NET_TO_PED(serverBD.niMwPed[(MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed]), WEAPONTYPE_APPISTOL, 25000, FALSE)
						IF iSpawnIndex = 8
						OR iSpawnIndex = 9
							GIVE_WEAPON_TO_PED(NET_TO_PED(serverBD.niMwPed[(MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed]), WEAPONTYPE_RPG, 25000, TRUE)
							PRINTLN("     ---------->     KILL LIST - PED GIVEN RPG ", (MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed)
						ELSE
							GIVE_DELAYED_WEAPON_TO_PED(NET_TO_PED(serverBD.niMwPed[(MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed]), WEAPONTYPE_ADVANCEDRIFLE, 25000, TRUE)
						ENDIF
						
						FLOAT fAccuracy = 30.0 * GET_ENEMY_ACCURACY_MULTIPLIER()
						SET_PED_ACCURACY(NET_TO_PED(serverBD.niMwPed[(MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed]), ROUND(fAccuracy))	
						
						//Set Behaviours
						SET_PED_COMBAT_ABILITY(NET_TO_PED(serverBD.niMwPed[(MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed]), CAL_AVERAGE)
						SET_PED_COMBAT_MOVEMENT(NET_TO_PED(serverBD.niMwPed[(MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed]), CM_WILLADVANCE)
						SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.niMwPed[(MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed]), CA_USE_COVER, TRUE)
						SET_PED_TARGET_LOSS_RESPONSE(NET_TO_PED(serverBD.niMwPed[(MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed]), TLR_NEVER_LOSE_TARGET)
						SET_PED_HIGHLY_PERCEPTIVE(NET_TO_PED(serverBD.niMwPed[(MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed]), TRUE)
						
						SET_PED_CONFIG_FLAG(NET_TO_PED(serverBD.niMwPed[(MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed]), PCF_DontInfluenceWantedLevel, TRUE)
						
						//IF serverBD.CrateDropData.LocationData[iLocation].Peds[i].fDefensiveRadius > 0
						//	SET_PED_SPHERE_DEFENSIVE_AREA(NET_TO_PED(serverBD.niMwPed[(MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed]), serverBD.CrateDropData.LocationData[iLocation].Peds[i].vDefensivePos, serverBD.CrateDropData.LocationData[iLocation].Peds[i].fDefensiveRadius+25)
						//ENDIF
						
						SET_ENTITY_HEALTH(NET_TO_PED(serverBD.niMwPed[(MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed]), ROUND(200*g_sMPTunables.fAiHealthModifier))
						
						//SET_PED_SPHERE_DEFENSIVE_AREA(NET_TO_PED(serverBD.niMwPed[(MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed]), serverBD.CrateDropData.LocationData[iLocation].vLocation, 50)
						
						SET_PED_AS_ENEMY(NET_TO_PED(serverBD.niMwPed[(MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed]), TRUE)
							
						SET_PED_KEEP_TASK(NET_TO_PED(serverBD.niMwPed[(MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed]), TRUE)
						SET_PED_DIES_WHEN_INJURED(NET_TO_PED(serverBD.niMwPed[(MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed]),TRUE)
						
						SET_PED_CONFIG_FLAG(NET_TO_PED(serverBD.niMwPed[(MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed]), PCF_CanActivateRagdollWhenVehicleUpsideDown, FALSE)
						
						iHEalth = GET_ENTITY_HEALTH(NET_TO_PED(serverBD.niMwPed[(MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed]))
						fHealthMult = GET_ENEMY_PED_HEALTH_MULTIPLIER()
						iHealth = ROUND(fHealthMult * TO_FLOAT(iHEalth))
						
						SET_ENTITY_HEALTH(NET_TO_PED(serverBD.niMwPed[(MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed]), iHealth)
						PRINTLN("     ---------->     KILL LIST - ON FOOT PED CREATED - PED ", (MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed, " SET health to ", iHealth, " fHealthMult = ", fHealthMult)
					//	TASK_COMBAT_HATED_TARGETS_IN_AREA(NET_TO_PED(serverBD.niMwPed[(MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed]), serverBD.vStartLocation[0], 250)
						
					
					//	SET_ENTITY_CAN_ONLY_BE_DAMAGED_BY_SCRIPT_PARTICIPANTS(NET_TO_PED(serverBD.niMwPed[(MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed]), TRUE)
					//	SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(NET_TO_PED(serverBD.niMwPed[(MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed]), TRUE)
						INT iPedIndex = (MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed
						IF IS_BIT_SET(serverBD.iServerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iPedIndex)], GET_LONG_BITSET_BIT(iPedIndex))
							CLEAR_BIT(serverBD.iServerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iPedIndex)], GET_LONG_BITSET_BIT(iPedIndex))
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] SERVER BIT WAS SET BUT ON-FOOT PED RECREATED  ", iPedIndex)
						ENDIF
						#IF IS_DEBUG_BUILD
							tl15Name = ""
							tl15Name = "Ped "
							tl15Name += (MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed
							SET_PED_NAME_DEBUG(NET_TO_PED(serverBD.niMwPed[(MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed]), tl15Name)
						#ENDIF
						
						serverBD.iMwPedsSpawned++
						serverBD.iPedOnFootSpawnedThisWave++
						serverBD.iTotalUwPedsSpawned++
						serverBD.iTotalOnFootPedSpawns++
						
						RESET_NET_TIMER(serverBD.timePedRespawn[iPedIndex])
						START_NET_TIMER(serverBD.timePedRespawn[iPedIndex])
						
						IF HAS_NET_TIMER_STARTED(serverBD.timeCleanupPeds[iPedIndex])
							RESET_NET_TIMER(serverBD.timeCleanupPeds[iPedIndex])
						ENDIF
						
						START_NET_TIMER(timeLastUseSpawnCoords[iSpawnIndex])
						SET_ENTITY_ONLY_DAMAGED_BY_RELATIONSHIP_GROUP(NET_TO_PED(serverBD.niMwPed[iPedIndex]), TRUE, relUWPlayer)
						
					//	ACTIVATE_DAMAGE_TRACKER_ON_NETWORK_ID(serverBD.niMwPed[iPedIndex], TRUE)
						
			//			BROADCAST_KILL_LIST_SET_PED_DAMAGEABLE_BY_REL_GROUP(iPedIndex, ALL_PLAYERS_ON_SCRIPT())
						
						PRINTLN("     ---------->     KILL LIST - (DAMAGE TRACKING) ON FOOT PED CREATED - PED ", (MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed, " at coord ", serverBD.vMwPedSpawn[iSpawnIndex], " iMwPedsSpawned = ", serverBD.iMwPedsSpawned, " serverBD.iPedOnFootSpawnedThisWave = ", serverBD.iPedOnFootSpawnedThisWave, " iTotalOnFootPedSpawns = ", serverBD.iTotalOnFootPedSpawns, " GET_TOTAL_NUMBER_OF_ON_FOOT_PED_SPAWNS_FOR_THIS_VARIATION() = ", GET_TOTAL_NUMBER_OF_ON_FOOT_PED_SPAWNS_FOR_THIS_VARIATION(), " GET_MAX_NUMBER_OF_ON_FOOT_SPAWNS_SO_FAR() = ", GET_MAX_NUMBER_OF_ON_FOOT_SPAWNS_SO_FAR()," iTotalUwPedsSpawned = ", serverBD.iTotalUwPedsSpawned)
						
					//	NET_PRINT_TIME() NET_PRINT("     ---------->  KILL LIST   ON FOOT PED CREATED - ") NET_PRINT(" PED ") NET_PRINT_INT(thisPed) NET_NL()
					ENDIF
				ELSE
					PRINTLN("     ---------->     KILL LIST - POINT NOT OK FOR ON-FOOT SPAWNING! PED ", thisPed)
				ENDIF
			ENDIF	
		ENDIF
	ENDIF

	//Check we have created all Peds
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niMwPed[(MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed])
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_PED_VEHICLE_DRIVABLE(INT iPed)
	IF NOT IS_PED_IN_ANY_VEHICLE(NET_TO_PED(serverbd.niMwPed[iPed]))
		PRINTLN("     ---------->     KILL LIST - IS_PED_VEHICLE_DRIVABLE - ped not in a vehicle ", iPed)
		RETURN FALSE
	ENDIF
	
	VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(NET_TO_PED(serverbd.niMwPed[iPed]))
	
	IF NOT IS_VEHICLE_DRIVEABLE(veh)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC


FUNC BOOL IS_PED_VEHICLE_DEAD(INT iPed)
	IF NOT IS_PED_IN_ANY_VEHICLE(NET_TO_PED(serverbd.niMwPed[iPed]))
		PRINTLN("     ---------->     KILL LIST - IS_PED_VEHICLE_DRIVABLE - ped not in a vehicle ", iPed)
		RETURN TRUE
	ENDIF
	
	VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(NET_TO_PED(serverbd.niMwPed[iPed]))
	
	IF IS_ENTITY_DEAD(veh)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_CLEAN_UP_UW_PED_DUE_TO_PED_VEHICLE_STATE(INT iPed)
//	IF NOT IS_COMPETITIVE_UW()
//		RETURN (NOT IS_PED_VEHICLE_DRIVABLE(iPed))
//	ENDIF
	
	RETURN IS_PED_VEHICLE_DEAD(iPed)
ENDFUNC

FUNC INT GET_MAX_NUMBER_OF_VEHICLE_SPAWNS()
	INT i
	INT iNumVehSpawns
	SWITCH serverBD.VehModel 
		CASE RHINO
		CASE SAVAGE
		CASE VALKYRIE
		CASE BUZZARD
			//iNumVehSpawns = g_sMPTunables.iUrbanWarfareEnemyVehSpawns
			REPEAT MAX_NUM_WAVES i
				iNumVehSpawns += serverbd.iNumVehiclesInWave[i]
			ENDREPEAT
		BREAK
		
		CASE HYDRA
			REPEAT MAX_NUM_WAVES i
				iNumVehSpawns += serverbd.iNumVehiclesInWave[i]
			ENDREPEAT
		BREAK
	ENDSWITCH
	
	RETURN iNumVehSpawns
	
ENDFUNC

FUNC INT GET_MAX_NUMBER_OF_ON_FOOT_PED_SPAWNS()
	INT i
	INT iNumPedSpawns
	SWITCH serverBD.VehModel 
//		CASE RHINO
//		CASE HYDRA
//		CASE VALKYRIE
//		CASE BUZZARD
//			iNumPedSpawns = g_sMPTunables.iUrbanWarfareEnemyOnFootSpawns
//		BREAK
		
		CASE SAVAGE
		CASE RHINO
		CASE HYDRA
		CASE VALKYRIE
		CASE BUZZARD
			REPEAT MAX_NUM_WAVES i
				iNumPedSpawns += serverbd.iNumOnFootPedsInWave[i]
			ENDREPEAT
		BREAK
	ENDSWITCH
	
	RETURN iNumPedSpawns
	
ENDFUNC

FUNC INT GET_NUMBER_OF_ACTIVE_ENEMY_VEHICLES(#IF IS_DEBUG_BUILD BOOL bSHowCoords = FALSE #ENDIF)
	INT iCOunt
	INT i
	REPEAT MAX_MW_VEHS i
		IF IS_NET_VEHICLE_DRIVEABLE(serverbd.niMwVeh[i])
			iCOunt++
			#IF IS_DEBUG_BUILD
				IF bSHowCoords
					VECTOR vCoords
					vCoords = GET_ENTITY_COORDS(NET_TO_VEH(serverbd.niMwVeh[i]), FALSE)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [GET_NUMBER_OF_ACTIVE_ENEMY_VEHICLES] THIS VEHICLE STILL DRIVABLE ", i, " COORDS ARE ", vCoords)
				ENDIF
			#ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN iCOunt
ENDFUNC

FUNC INT GET_NUMBER_OF_ACTIVE_ENEMY_GROUND_PEDS()
	INT iStartGroundPed = MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH
	INT iCount
	
	FOR iStartGroundPed = (MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH) TO MAX_MW_PEDS-1
		IF NOT IS_NET_PED_INJURED(serverBD.niMwPed[iStartGroundPed])
		//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - GROUND PED NOT INJURED ", iStartGroundPed)
			iCount++
		ENDIF
	ENDFOR
	
	RETURN iCount
ENDFUNC

FUNC INT GET_NUMBER_OF_ACTIVE_ENEMIES()
	RETURN(GET_NUMBER_OF_ACTIVE_ENEMY_VEHICLES() + GET_NUMBER_OF_ACTIVE_ENEMY_GROUND_PEDS())
ENDFUNC

FUNC INT GET_MIN_NUMBER_OF_REMAINING_VEHICLES_FOR_WAVE_UPDATE()
	INT iNextWave = serverbd.iCurrentWave + 1
	INT iNextWaveType
	IF iNextWave < MAX_NUM_WAVES
		iNextWaveType = serverBD.iWaveType[iNextWave]
		SWITCH iNextWaveType
			CASE WAVE_TYPE_NONE
				RETURN 99
			BREAK
			
			CASE WAVE_TYPE_AIRCRAFT_ONLY
				RETURN 1
			BREAK
			
			CASE WAVE_TYPE_GROUND_PEDS_AND_VEHS
				IF GET_CURRENT_WAVE_TYPE() = WAVE_TYPE_GROUND_PEDS_AND_VEHS
					RETURN 2
				ELIF GET_CURRENT_WAVE_TYPE() = WAVE_TYPE_AIRCRAFT_ONLY
					RETURN 1
				ENDIF
			BREAK
			
			CASE WAVE_TYPE_BOSS
				RETURN 0
			BREAK
		ENDSWITCH
	ELSE
		RETURN 99
	ENDIF
	
	RETURN 99
ENDFUNC

FUNC INT GET_MIN_NUMBER_OF_REMAINING_GROUND_PEDS_FOR_WAVE_UPDATE()
	INT iNextWave = serverbd.iCurrentWave + 1
	INT iNextWaveType
	IF iNextWave < MAX_NUM_WAVES
		iNextWaveType = serverBD.iWaveType[iNextWave]
		SWITCH iNextWaveType
			CASE WAVE_TYPE_NONE
				RETURN 99
			BREAK
			
			CASE WAVE_TYPE_AIRCRAFT_ONLY
				RETURN 1
			BREAK
			
			CASE WAVE_TYPE_GROUND_PEDS_AND_VEHS
				IF GET_CURRENT_WAVE_TYPE() = WAVE_TYPE_GROUND_PEDS_AND_VEHS
					RETURN 2
				ELIF GET_CURRENT_WAVE_TYPE() = WAVE_TYPE_AIRCRAFT_ONLY
					RETURN 1
				ENDIF
			BREAK
			
			CASE WAVE_TYPE_BOSS
				RETURN 0
			BREAK
		ENDSWITCH
	ELSE
		RETURN 99
	ENDIF
	
	RETURN 99
ENDFUNC
PROC UPDATE_CURRENT_WAVE()
	#IF IS_DEBUG_BUILD
		INT iWaveType
		IF bWdDOWaveDebug
		OR bWdTimedDebug
			iWaveType = GET_CURRENT_WAVE_TYPE()
			
			PRINTLN("     ---------->     KILL LIST [UPDATE_CURRENT_WAVE] [bWdDOWaveDebug] ")
			PRINTLN("     ---------->     KILL LIST [UPDATE_CURRENT_WAVE] [bWdDOWaveDebug] iNumOfWaves = ", serverbd.iNumOfWaves)
			PRINTLN("     ---------->     KILL LIST [UPDATE_CURRENT_WAVE] [bWdDOWaveDebug] iWaveType = ", iWaveType)
			PRINTLN("     ---------->     KILL LIST [UPDATE_CURRENT_WAVE] [bWdDOWaveDebug] iCurrentWave = ", serverbd.iCurrentWave)
			PRINTLN("     ---------->     KILL LIST [UPDATE_CURRENT_WAVE] [bWdDOWaveDebug] iVehicleSpawnedThisWave = ", serverbd.iVehicleSpawnedThisWave)
			PRINTLN("     ---------->     KILL LIST [UPDATE_CURRENT_WAVE] [bWdDOWaveDebug] iVehKillsThisWave = ", serverbd.iVehKillsThisWave)
			PRINTLN("     ---------->     KILL LIST [UPDATE_CURRENT_WAVE] [bWdDOWaveDebug] GET_NUMBER_OF_ACTIVE_ENEMY_GROUND_PEDS = ", GET_NUMBER_OF_ACTIVE_ENEMY_GROUND_PEDS())
			PRINTLN("     ---------->     KILL LIST [UPDATE_CURRENT_WAVE] [bWdDOWaveDebug] GET_NUMBER_OF_ACTIVE_ENEMY_VEHICLES = ", GET_NUMBER_OF_ACTIVE_ENEMY_VEHICLES(TRUE))
			PRINTLN("     ---------->     KILL LIST [UPDATE_CURRENT_WAVE] [bWdDOWaveDebug] iPedOnFootKillsThisWave = ", serverbd.iPedOnFootKillsThisWave)
			PRINTLN("     ---------->     KILL LIST [UPDATE_CURRENT_WAVE] [bWdDOWaveDebug] iPedOnFootSpawnedThisWave = ", serverbd.iPedOnFootSpawnedThisWave) 
			PRINTLN("     ---------->     KILL LIST [UPDATE_CURRENT_WAVE] [bWdDOWaveDebug] iTotalOnFootPedSpawns = ", serverbd.iTotalOnFootPedSpawns)  
			PRINTLN("     ---------->     KILL LIST [UPDATE_CURRENT_WAVE] [bWdDOWaveDebug] GET_NUMBER_OF_GROUND_PEDS_IN_CURRENT_WAVE() = ", GET_NUMBER_OF_GROUND_PEDS_IN_CURRENT_WAVE())
			PRINTLN("     ---------->     KILL LIST [UPDATE_CURRENT_WAVE] [bWdDOWaveDebug] GET_TOTAL_NUMBER_OF_ON_FOOT_PED_SPAWNS_FOR_THIS_VARIATION() = ", GET_TOTAL_NUMBER_OF_ON_FOOT_PED_SPAWNS_FOR_THIS_VARIATION())
			PRINTLN("     ---------->     KILL LIST [UPDATE_CURRENT_WAVE] [bWdDOWaveDebug] GET_MAX_NUMBER_OF_ON_FOOT_SPAWNS_SO_FAR() = ", GET_MAX_NUMBER_OF_ON_FOOT_SPAWNS_SO_FAR())
			
		ENDIF
	#ENDIF
	
	
	INT iNextWave
	INT iNextWaveType
	IF GET_CURRENT_WAVE_TYPE() = WAVE_TYPE_NONE
		EXIT
	ENDIF
	
	IF GET_CURRENT_WAVE_TYPE() = WAVE_TYPE_AIRCRAFT_ONLY
		IF serverbd.iVehicleSpawnedThisWave > 0
			IF serverbd.iVehKillsThisWave > 0
				IF serverbd.iVehKillsThisWave >= (serverbd.iVehicleSpawnedThisWave - 1)
					IF GET_NUMBER_OF_ACTIVE_ENEMY_VEHICLES() <= GET_MIN_NUMBER_OF_REMAINING_VEHICLES_FOR_WAVE_UPDATE()
						IF GET_NUMBER_OF_ACTIVE_ENEMY_GROUND_PEDS() = 0
							iNextWave = serverbd.iCurrentWave + 1
										
							IF iNextWave < MAX_NUM_WAVES
								iNextWaveType = serverBD.iWaveType[iNextWave]
								IF iNextWaveType <> WAVE_TYPE_BOSS
									PRINTLN("     ---------->     KILL LIST - UPDATE_CURRENT_WAVE - WAVE_TYPE_AIRCRAFT_ONLY serverbd.iVehKillsThisWave = ", serverbd.iVehKillsThisWave, " >=  serverbd.iVehicleSpawnedThisWave = ", serverbd.iVehicleSpawnedThisWave, " New wave number = ", (serverbd.iCurrentWave + 1))
									RESET_CURRENT_WAVE()
									
									serverbd.iCurrentWave++
									
									GET_RANDOM_LOCATION()
								ELSE
									IF serverbd.iTotalOnFootPedSpawns >= GET_TOTAL_NUMBER_OF_ON_FOOT_PED_SPAWNS_FOR_THIS_VARIATION()
										PRINTLN("     ---------->     KILL LIST - UPDATE_CURRENT_WAVE - WAVE_TYPE_AIRCRAFT_ONLY NEXT WAVE BOSS serverbd.iPedOnFootKillsThisWave = ", serverbd.iPedOnFootKillsThisWave, " >=  serverbd.iPedOnFootSpawnedThisWave = ", serverbd.iPedOnFootSpawnedThisWave, " New wave number = ", (serverbd.iCurrentWave + 1))
										RESET_CURRENT_WAVE()
										
										serverbd.iCurrentWave++
										
										GET_RANDOM_LOCATION()
									ELSE
										#IF IS_DEBUG_BUILD
											IF bWdTimedDebug
												PRINTLN("     ---------->     KILL LIST [UPDATE_CURRENT_WAVE] NEXT WAVE BOSS WAVE_TYPE_AIRCRAFT_ONLY NOT UPDATING AS serverbd.iTotalOnFootPedSpawns ", serverbd.iTotalOnFootPedSpawns, " < GET_TOTAL_NUMBER_OF_ON_FOOT_PED_SPAWNS_FOR_THIS_VARIATION() ",GET_TOTAL_NUMBER_OF_ON_FOOT_PED_SPAWNS_FOR_THIS_VARIATION())
											ENDIF
										#ENDIF
									ENDIF
								ENDIF
							ENDIF	
										
							
							
						ELSE
							
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							IF bWdTimedDebug
								PRINTLN("     ---------->     KILL LIST [UPDATE_CURRENT_WAVE] WAVE_TYPE_AIRCRAFT_ONLY NOT UPDATING AS GET_NUMBER_OF_ACTIVE_ENEMY_VEHICLES() ", GET_NUMBER_OF_ACTIVE_ENEMY_VEHICLES(), " > GET_MIN_NUMBER_OF_REMAINING_VEHICLES_FOR_WAVE_UPDATE() ", GET_MIN_NUMBER_OF_REMAINING_VEHICLES_FOR_WAVE_UPDATE())
							ENDIF
						#ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						IF bWdTimedDebug
							PRINTLN("     ---------->     KILL LIST [UPDATE_CURRENT_WAVE] WAVE_TYPE_AIRCRAFT_ONLY NOT UPDATING AS serverbd.iVehKillsThisWave = ", serverbd.iVehKillsThisWave, " < (serverbd.iVehicleSpawnedThisWave - 1) = ", (serverbd.iVehicleSpawnedThisWave - 1))
						ENDIF
					#ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					IF bWdTimedDebug
						PRINTLN("     ---------->     KILL LIST [UPDATE_CURRENT_WAVE] WAVE_TYPE_AIRCRAFT_ONLY NOT UPDATING AS serverbd.iVehKillsThisWave = ", serverbd.iVehKillsThisWave)
					ENDIF
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				IF bWdTimedDebug
					PRINTLN("     ---------->     KILL LIST [UPDATE_CURRENT_WAVE] WAVE_TYPE_AIRCRAFT_ONLY NOT UPDATING AS serverbd.iVehicleSpawnedThisWave = ", serverbd.iVehicleSpawnedThisWave)
				ENDIF
			#ENDIF
		ENDIF
	ELIF GET_CURRENT_WAVE_TYPE() = WAVE_TYPE_GROUND_PEDS_ONLY
		IF serverbd.iPedOnFootSpawnedThisWave > 0	
			IF serverbd.iPedOnFootKillsThisWave > 0
				IF serverbd.iPedOnFootKillsThisWave >= serverbd.iPedOnFootSpawnedThisWave
					IF serverbd.iPedOnFootSpawnedThisWave >= GET_NUMBER_OF_GROUND_PEDS_IN_CURRENT_WAVE()
						PRINTLN("     ---------->     KILL LIST - UPDATE_CURRENT_WAVE - WAVE_TYPE_GROUND_PEDS_ONLY serverbd.iPedOnFootKillsThisWave = ", serverbd.iPedOnFootKillsThisWave, " >=  serverbd.iPedOnFootSpawnedThisWave = ", serverbd.iPedOnFootSpawnedThisWave, " New wave number = ", (serverbd.iCurrentWave + 1), " GET_NUMBER_OF_GROUND_PEDS_IN_CURRENT_WAVE = ", GET_NUMBER_OF_GROUND_PEDS_IN_CURRENT_WAVE())
						
						RESET_CURRENT_WAVE()
						
						serverbd.iCurrentWave++
						
						GET_RANDOM_LOCATION()
						
						
					ENDIF
				ENDIF
			ENDIF
		ENDIF 
	ELIF GET_CURRENT_WAVE_TYPE() = WAVE_TYPE_GROUND_PEDS_AND_VEHS
		IF serverbd.iPedOnFootKillsThisWave > 0
			IF serverbd.iVehKillsThisWave > 0
				IF serverbd.iPedOnFootSpawnedThisWave > 0	
					IF serverbd.iPedOnFootKillsThisWave >= (serverbd.iPedOnFootSpawnedThisWave - 2)
						IF serverbd.iVehicleSpawnedThisWave > 0
							IF serverbd.iVehKillsThisWave >= (serverbd.iVehicleSpawnedThisWave - 2)
								IF GET_NUMBER_OF_ACTIVE_ENEMY_VEHICLES() <= GET_MIN_NUMBER_OF_REMAINING_VEHICLES_FOR_WAVE_UPDATE()
									IF GET_NUMBER_OF_ACTIVE_ENEMY_GROUND_PEDS() <= GET_MIN_NUMBER_OF_REMAINING_GROUND_PEDS_FOR_WAVE_UPDATE()
										iNextWave = serverbd.iCurrentWave + 1
										
										IF iNextWave < MAX_NUM_WAVES
											iNextWaveType = serverBD.iWaveType[iNextWave]
											IF iNextWaveType <> WAVE_TYPE_BOSS
												PRINTLN("     ---------->     KILL LIST - UPDATE_CURRENT_WAVE - WAVE_TYPE_GROUND_PEDS_AND_VEHS NEXT WAVE NOT BOSS serverbd.iPedOnFootKillsThisWave = ", serverbd.iPedOnFootKillsThisWave, " >=  serverbd.iPedOnFootSpawnedThisWave = ", serverbd.iPedOnFootSpawnedThisWave, " New wave number = ", (serverbd.iCurrentWave + 1))
												RESET_CURRENT_WAVE()
												
												serverbd.iCurrentWave++
												
												GET_RANDOM_LOCATION()
											ELSE
												IF serverbd.iTotalOnFootPedSpawns >= GET_TOTAL_NUMBER_OF_ON_FOOT_PED_SPAWNS_FOR_THIS_VARIATION()
													PRINTLN("     ---------->     KILL LIST - UPDATE_CURRENT_WAVE - WAVE_TYPE_GROUND_PEDS_AND_VEHS NEXT WAVE BOSS serverbd.iPedOnFootKillsThisWave = ", serverbd.iPedOnFootKillsThisWave, " >=  serverbd.iPedOnFootSpawnedThisWave = ", serverbd.iPedOnFootSpawnedThisWave, " New wave number = ", (serverbd.iCurrentWave + 1))
													RESET_CURRENT_WAVE()
													
													serverbd.iCurrentWave++
													
													GET_RANDOM_LOCATION()
												ELSE
													#IF IS_DEBUG_BUILD
														IF bWdTimedDebug
															PRINTLN("     ---------->     KILL LIST [UPDATE_CURRENT_WAVE] WAVE_TYPE_GROUND_PEDS_AND_VEHS NOT UPDATING AS serverbd.iTotalOnFootPedSpawns ", serverbd.iTotalOnFootPedSpawns, " < GET_TOTAL_NUMBER_OF_ON_FOOT_PED_SPAWNS_FOR_THIS_VARIATION() ",GET_TOTAL_NUMBER_OF_ON_FOOT_PED_SPAWNS_FOR_THIS_VARIATION())
														ENDIF
													#ENDIF
												ENDIF
											ENDIF
										ENDIF		
											
									ELSE
										#IF IS_DEBUG_BUILD
											IF bWdTimedDebug
												PRINTLN("     ---------->     KILL LIST [UPDATE_CURRENT_WAVE] WAVE_TYPE_GROUND_PEDS_AND_VEHS NOT UPDATING AS GET_NUMBER_OF_ACTIVE_ENEMY_GROUND_PEDS() ", GET_NUMBER_OF_ACTIVE_ENEMY_GROUND_PEDS(), " > GET_MIN_NUMBER_OF_REMAINING_GROUND_PEDS_FOR_WAVE_UPDATE() ", GET_MIN_NUMBER_OF_REMAINING_GROUND_PEDS_FOR_WAVE_UPDATE())
											ENDIF
										#ENDIF
									ENDIF
								ELSE
									#IF IS_DEBUG_BUILD
										IF bWdTimedDebug
											PRINTLN("     ---------->     KILL LIST [UPDATE_CURRENT_WAVE] WAVE_TYPE_GROUND_PEDS_AND_VEHS NOT UPDATING AS GET_NUMBER_OF_ACTIVE_ENEMY_VEHICLES() ", GET_NUMBER_OF_ACTIVE_ENEMY_VEHICLES(), " >  GET_MIN_NUMBER_OF_REMAINING_VEHICLES_FOR_WAVE_UPDATE() ",  GET_MIN_NUMBER_OF_REMAINING_VEHICLES_FOR_WAVE_UPDATE())
										ENDIF
									#ENDIF
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
									IF bWdTimedDebug
										PRINTLN("     ---------->     KILL LIST [UPDATE_CURRENT_WAVE] WAVE_TYPE_GROUND_PEDS_AND_VEHS NOT UPDATING AS serverbd.iVehKillsThisWave ", serverbd.iVehKillsThisWave, " < (serverbd.iVehicleSpawnedThisWave - 2) ", (serverbd.iVehicleSpawnedThisWave - 2))
									ENDIF
								#ENDIF
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
								IF bWdTimedDebug
									PRINTLN("     ---------->     KILL LIST [UPDATE_CURRENT_WAVE] WAVE_TYPE_GROUND_PEDS_AND_VEHS NOT UPDATING AS serverbd.iVehicleSpawnedThisWave = ", serverbd.iVehicleSpawnedThisWave)
								ENDIF
							#ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							IF bWdTimedDebug
								PRINTLN("     ---------->     KILL LIST [UPDATE_CURRENT_WAVE] WAVE_TYPE_GROUND_PEDS_AND_VEHS NOT UPDATING AS serverbd.iPedOnFootKillsThisWave ", serverbd.iPedOnFootKillsThisWave, " < (serverbd.iPedOnFootSpawnedThisWave - 2) ", (serverbd.iPedOnFootSpawnedThisWave - 2))
							ENDIF
						#ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						IF bWdTimedDebug
							PRINTLN("     ---------->     KILL LIST [UPDATE_CURRENT_WAVE] WAVE_TYPE_GROUND_PEDS_AND_VEHS NOT UPDATING AS serverbd.iPedOnFootSpawnedThisWave = ", serverbd.iPedOnFootSpawnedThisWave)
						ENDIF
					#ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					IF bWdTimedDebug
						PRINTLN("     ---------->     KILL LIST [UPDATE_CURRENT_WAVE] WAVE_TYPE_GROUND_PEDS_AND_VEHS NOT UPDATING AS serverbd.iVehKillsThisWave = ", serverbd.iVehKillsThisWave)
					ENDIF
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				IF bWdTimedDebug
					PRINTLN("     ---------->     KILL LIST [UPDATE_CURRENT_WAVE] WAVE_TYPE_GROUND_PEDS_AND_VEHS NOT UPDATING AS serverbd.iPedOnFootKillsThisWave = ", serverbd.iPedOnFootKillsThisWave)
				ENDIF
			#ENDIF
		ENDIF
	ELIF GET_CURRENT_WAVE_TYPE() = WAVE_TYPE_BOSS
		IF serverbd.iVehicleSpawnedThisWave > 0
			IF serverbd.iVehKillsThisWave > 0
				IF serverbd.iVehKillsThisWave >= (serverbd.iVehicleSpawnedThisWave - 1)
					IF GET_NUMBER_OF_ACTIVE_ENEMY_VEHICLES() <= GET_MIN_NUMBER_OF_REMAINING_VEHICLES_FOR_WAVE_UPDATE()
						IF GET_NUMBER_OF_ACTIVE_ENEMY_GROUND_PEDS() = 0
							PRINTLN("     ---------->     KILL LIST - UPDATE_CURRENT_WAVE - WAVE_TYPE_BOSS serverbd.iVehKillsThisWave = ", serverbd.iVehKillsThisWave, " >=  serverbd.iVehicleSpawnedThisWave = ", serverbd.iVehicleSpawnedThisWave, " New wave number = ", serverbd.iCurrentWave)
							RESET_CURRENT_WAVE()
							
							serverbd.iCurrentWave++
							
							GET_RANDOM_LOCATION()
							
						ELSE
							#IF IS_DEBUG_BUILD
								IF bWdTimedDebug
									PRINTLN("     ---------->     KILL LIST [UPDATE_CURRENT_WAVE] WAVE_TYPE_BOSS NOT UPDATING AS  GET_NUMBER_OF_ACTIVE_ENEMY_GROUND_PEDS() =  ",  GET_NUMBER_OF_ACTIVE_ENEMY_GROUND_PEDS())
								ENDIF
							#ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							IF bWdTimedDebug
								PRINTLN("     ---------->     KILL LIST [UPDATE_CURRENT_WAVE] WAVE_TYPE_BOSS NOT UPDATING AS GET_NUMBER_OF_ACTIVE_ENEMY_VEHICLES() ", GET_NUMBER_OF_ACTIVE_ENEMY_VEHICLES(), " > GET_MIN_NUMBER_OF_REMAINING_VEHICLES_FOR_WAVE_UPDATE() ", GET_MIN_NUMBER_OF_REMAINING_VEHICLES_FOR_WAVE_UPDATE())
							ENDIF
						#ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						IF bWdTimedDebug
							PRINTLN("     ---------->     KILL LIST [UPDATE_CURRENT_WAVE] WAVE_TYPE_BOSS NOT UPDATING AS serverbd.iVehKillsThisWave ", serverbd.iVehKillsThisWave, " < (serverbd.iVehicleSpawnedThisWave - 1) ", (serverbd.iVehicleSpawnedThisWave - 1))
						ENDIF
					#ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					IF bWdTimedDebug
						PRINTLN("     ---------->     KILL LIST [UPDATE_CURRENT_WAVE] WAVE_TYPE_BOSS NOT UPDATING AS serverbd.iVehKillsThisWave = ", serverbd.iVehKillsThisWave)
					ENDIF
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				IF bWdTimedDebug
					PRINTLN("     ---------->     KILL LIST [UPDATE_CURRENT_WAVE] WAVE_TYPE_BOSS NOT UPDATING AS serverbd.iVehicleSpawnedThisWave = ", serverbd.iVehicleSpawnedThisWave)
				ENDIF
			#ENDIF
		ENDIF		
	ENDIF
ENDPROC



FUNC BOOL UW_VEHICLE_OK(INT iVeh)
//	IF NOT IS_COMPETITIVE_UW()
//		RETURN (IS_NET_VEHICLE_DRIVEABLE(serverBD.niMwVeh[iVeh]))
//	ENDIF
	
	MODEL_NAMES mVeh = GET_ENTITY_MODEL(NET_TO_VEH(serverBD.niMwVeh[iVeh]))
	
	IF IS_THIS_MODEL_A_PLANE(mVeh)
	OR IS_THIS_MODEL_A_HELI(mVeh)
		RETURN (NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niMwVeh[iVeh])))
	ENDIF
	
	RETURN (NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niMwVeh[iVeh])))
//	RETURN (IS_NET_VEHICLE_DRIVEABLE(serverBD.niMwVeh[iVeh]))
ENDFUNC


/// PURPOSE:
///    For some vehicles, need to wait until a player has claimed the kill before cleaning up the vehicle.
///    Generally because the peds inside don't die when the vehicle becomes undrivable, so need to track the vehicle kill rather than the ped kill
FUNC BOOL WAIT_FOR_VEHICLE_DESTROYED_BITSET_UPDATE(INT iVeh)
	
	
//	IF NOT IS_COMPETITIVE_UW()
//		RETURN TRUE
//	ENDIF
	
	MODEL_NAMES mVeh
	mVeh = GET_ENTITY_MODEL(NET_TO_VEH(serverBD.niMwVeh[iVeh]))
	
	IF mVeh = SAVAGE
		IF IS_BIT_SET(serverBD.iTargetVehDestroyedBitSet, iVeh)
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [WAIT_FOR_VEHICLE_DESTROYED_BITSET_UPDATE] SAVAGE CHECK TRUE iVeh = ", iVeh)
			RETURN TRUE
		ENDIF
	ELIF mVeh = BUZZARD
		IF IS_BIT_SET(serverBD.iTargetVehDestroyedBitSet, iVeh)
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [WAIT_FOR_VEHICLE_DESTROYED_BITSET_UPDATE] BUZZARD CHECK TRUE iVeh = ", iVeh)
			RETURN TRUE
		ENDIF
	ELSE
		IF serverBD.VehModel <> HYDRA
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [WAIT_FOR_VEHICLE_DESTROYED_BITSET_UPDATE] serverBD.VehModel <> HYDRA CHECK TRUE iVeh = ", iVeh)
			RETURN TRUE
		ENDIF
		
		IF serverBD.VehModel = HYDRA
			IF IS_BIT_SET(serverBD.iTargetVehDestroyedBitSet, iVeh)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [WAIT_FOR_VEHICLE_DESTROYED_BITSET_UPDATE] serverBD.VehModel = HYDRA CHECK TRUE iVeh = ", iVeh)
				RETURN TRUE
			ENDIF
		ENDIF
	
	ENDIF
	
	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL WAIT_FOR_PED_KILLED_BITSET_UPDATE(INT iPed)
	IF serverBD.VehModel = HYDRA
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [WAIT_FOR_PED_KILLED_BITSET_UPDATE] TRUE - HYDRA PED ", iPed)
		RETURN TRUE
	ENDIF
	
//	IF NOT IS_COMPETITIVE_UW()	
//		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [WAIT_FOR_PED_KILLED_BITSET_UPDATE] TRUE - NOT IS_COMPETITIVE_UW PED ", iPed)
//		RETURN TRUE
//	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [WAIT_FOR_PED_KILLED_BITSET_UPDATE] TRUE - iServerTargetPedKilledBitset is set PED ", iPed)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEANUP_ALL_DEAD_VEHS_AND_PEDS()
	INT i
	PRINTLN("---------->     KILL LIST [CLEANUP_ALL_DEAD_VEHS_AND_PEDS] CALLED ")
	REPEAT MAX_MW_VEHS i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niMwVeh[i])
			IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.niMwVeh[i])
				PRINTLN("---------->     KILL LIST [CLEANUP_ALL_DEAD_VEHS_AND_PEDS] CLEANING UP VEH ", i)
				CLEANUP_NET_ID(serverBD.niMwVeh[i])
				
			ENDIF
		ENDIF
	ENDREPEAT
	
	i = 0
	REPEAT MAX_MW_PEDS i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niMwPed[i])
			IF IS_NET_PED_INJURED(serverBD.niMwPed[i])
				PRINTLN("---------->     KILL LIST [CLEANUP_ALL_DEAD_VEHS_AND_PEDS] CLEANING UP PED ", i)
				CLEANUP_NET_ID(serverBD.niMwPed[i])
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

//PURPOSE: Controls the creation of Merryweather vehicles and peds. //MAY WANT TO STAGGER THESE CHECKS
PROC MAINTAIN_MW_CREATION()
	INT thisVeh, thisPed
	
	BOOL bUseFailSafe = TRUE
	BOOL bSomeoneSTillAlive = FALSE
	INT i
	
	#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_PedCheck")
			bUseFailSafe = FALSE
		ENDIF
		
		VECTOR vTempFSCoords
	#ENDIF
	
	IF serverbd.iNumOfWaves > 0
		IF serverbd.iCurrentWave >= serverbd.iNumOfWaves
			IF serverbd.iTotalKills >= serverBD.iKillGoal
				SET_BIT(serverBD.iServerBitSet, biS_AllVehiclesDestroyed)
				SET_BIT(serverBD.iServerBitSet, biS_AllVehiclePedsDead)
				SET_BIT(serverBD.iServerBitSet, biS_AllOnFootPedsDead)
				
				PRINTLN("---------->     KILL LIST [MAINTAIN_MW_CREATION] ALL WAVES CLEARED serverbd.iCurrentWave = ", serverbd.iCurrentWave, " >= serverbd.iNumOfWaves = ", serverbd.iNumOfWaves)
				
				EXIT
			ENDIF
		ENDIF
	ENDIF
	
	INT iTempTotal = 0
	IF bUseFailSafe
	//	IF IS_COMPETITIVE_UW()
			IF serverbd.iNumOfWaves > 0
				IF serverbd.iCurrentWave >= serverbd.iNumOfWaves
					IF serverbd.iKillsPerVehicle[0] >= 0 iTempTotal += serverbd.iKillsPerVehicle[0] ENDIF
					IF serverbd.iKillsPerVehicle[1] >= 0 iTempTotal += serverbd.iKillsPerVehicle[1] ENDIF
					IF serverbd.iKillsPerVehicle[2] >= 0 iTempTotal += serverbd.iKillsPerVehicle[2] ENDIF
					IF serverbd.iKillsPerVehicle[3] >= 0 iTempTotal += serverbd.iKillsPerVehicle[3] ENDIF
					
					IF iTempTotal >= serverBD.iKillGoal
						i = 0
						REPEAT MAX_MW_PEDS i
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niMwPed[i])
								IF NOT IS_NET_PED_INJURED(serverBD.niMwPed[i])
									bSomeoneSTillAlive = TRUE
									
									#IF IS_DEBUG_BUILD
										vTempFSCoords = GET_ENTITY_COORDS(NET_TO_PED(serverBD.niMwPed[i]))
										PRINTLN("---------->     KILL LIST [MAINTAIN_MW_CREATION] WANT TO TRIGGER FAIL SAFE AS iTempTotal = ", iTempTotal, " >= serverBD.iKillGoal = ", serverBD.iKillGoal) 
										PRINTLN("---------->     KILL LIST [MAINTAIN_MW_CREATION] WANT TO TRIGGER FAIL SAFE BUT THIS PED ", i, " STILL ALIVE. COORDS = ", vTempFSCoords)
										
									#ENDIF
								ENDIF
							ENDIF
						ENDREPEAT
						
						IF NOT bSomeoneSTillAlive
							SET_BIT(serverBD.iServerBitSet, biS_AllVehiclesDestroyed)
							SET_BIT(serverBD.iServerBitSet, biS_AllVehiclePedsDead)
							SET_BIT(serverBD.iServerBitSet, biS_AllOnFootPedsDead)
							
							PRINTLN("---------->     KILL LIST [MAINTAIN_MW_CREATION] ALL WAVES CLEARED VIA FAIL SAFE serverbd.iTotalKills = ", serverbd.iTotalKills, " Team 1 score = ", serverBD.iKillsPerVehicle[0], " Team 2 score = ",   serverBD.iKillsPerVehicle[1], " Team 3 score = ",   serverBD.iKillsPerVehicle[2], " Team 4 score = ",   serverBD.iKillsPerVehicle[3])
							
							EXIT
						ENDIF
					ENDIF

					
					bSomeoneSTillAlive = FALSE
					i = 0
					REPEAT MAX_MW_PEDS i
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niMwPed[i])
							IF NOT IS_NET_PED_INJURED(serverBD.niMwPed[i])
								bSomeoneSTillAlive = TRUE
								
								#IF IS_DEBUG_BUILD
									IF bWdShowPedsStillToKill
									OR bWdTimedDebug
										PRINTLN("---------->     KILL LIST [MAINTAIN_MW_CREATION] ALL WAVES *NOT* CLEARED VIA FAIL SAFE (2), THIS PED STILL ALIVE PED ", i) 
									ENDIF
								#ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
					
					#IF IS_DEBUG_BUILD
						IF GET_COMMANDLINE_PARAM_EXISTS("sc_TestSavage")
							bSomeoneSTillAlive = TRUE
						ENDIF
					#ENDIF
					
					IF NOT bSomeoneSTillAlive
						IF NOT HAS_NET_TIMER_STARTED(serverBD.timeFailsafeAllDead)
							START_NET_TIMER(serverBD.timeFailsafeAllDead)
							PRINTLN("---------->     KILL LIST [MAINTAIN_MW_CREATION] ALL WAVES CLEARED VIA FAIL SAFE (2) STARTED timeFailsafeAllDead")
						ELSE
							IF HAS_NET_TIMER_EXPIRED(serverBD.timeFailsafeAllDead, 3000)
								SET_BIT(serverBD.iServerBitSet, biS_AllVehiclesDestroyed)
								SET_BIT(serverBD.iServerBitSet, biS_AllVehiclePedsDead)
								SET_BIT(serverBD.iServerBitSet, biS_AllOnFootPedsDead)
						
								PRINTLN("---------->     KILL LIST [MAINTAIN_MW_CREATION] ALL WAVES CLEARED VIA FAIL SAFE (2) serverbd.iTotalKills = ", serverbd.iTotalKills, " Team 1 score = ", serverBD.iKillsPerVehicle[0], " Team 2 score = ",   serverBD.iKillsPerVehicle[1], " Team 3 score = ",   serverBD.iKillsPerVehicle[2], " Team 4 score = ",   serverBD.iKillsPerVehicle[3])
								
								EXIT
							ENDIF
						
						ENDIF

					ENDIF

							
				ENDIF
			ENDIF
	//	ENDIF
	ENDIF
	
	
	#IF IS_DEBUG_BUILD
		IF bWdForceWin
			SET_BIT(serverBD.iServerBitSet, biS_AllVehiclesDestroyed)
			SET_BIT(serverBD.iServerBitSet, biS_AllVehiclePedsDead)
			SET_BIT(serverBD.iServerBitSet, biS_AllOnFootPedsDead)
	
			PRINTLN("---------->     KILL LIST [MAINTAIN_MW_CREATION] ALL WAVES CLEARED VIA bWdForceWin")
			
			EXIT
		ENDIF
		
		IF MPGlobalsAmbience.bEndCurrentFreemodeEvent
			SET_BIT(serverBD.iServerBitSet, biS_AllVehiclesDestroyed)
			SET_BIT(serverBD.iServerBitSet, biS_AllVehiclePedsDead)
			SET_BIT(serverBD.iServerBitSet, biS_AllOnFootPedsDead)
			IF NOT Is_This_The_Current_Objective_Text("UW_EXPL")
			AND NOT Is_This_The_Current_Objective_Text("UW_EXPLC")
				Clear_Any_Objective_Text_From_This_Script()
			ENDIF
			PRINTLN("---------->     KILL LIST [MAINTAIN_MW_CREATION] ALL WAVES CLEARED VIA bEndCurrentFreemodeEvent")
			
			EXIT
		ENDIF
	#ENDIF
	
	
	#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_VehCheck")
		OR bWdTimedDebug
			PRINTLN("[MAINTAIN_MW_CREATION] [sc_VehCheck] Running.... ")
		ENDIF
	#ENDIF
	
	//VEHICLES WITH PEDS
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AllVehiclesDestroyed)
	OR NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AllVehiclePedsDead)
		
	
	
		IF serverBD.iMwTotalVehiclesSpawned >= GET_MAX_NUMBER_OF_VEHICLE_SPAWNS()
			SET_BIT(serverBD.iServerBitSet, biS_AllVehiclesDestroyed)
			SET_BIT(serverBD.iServerBitSet, biS_AllVehiclePedsDead)
			
			#IF IS_DEBUG_BUILD
		//	IF bWdShowPedsStillToKill
		//	OR bWdTimedDebug
				PRINTLN("[MAINTAIN_MW_CREATION] [bWdShowPedsStillToKill] Set biS_AllVehiclesDestroyed / biS_AllVehiclePedsDead iMwTotalVehiclesSpawned = ", serverBD.iMwTotalVehiclesSpawned, " >= GET_MAX_NUMBER_OF_VEHICLE_SPAWNS() = ", GET_MAX_NUMBER_OF_VEHICLE_SPAWNS())
		//	ENDIF
			#ENDIF
		ELSE
			
			#IF IS_DEBUG_BUILD
			IF bWdShowPedsStillToKill
			OR bWdTimedDebug
				PRINTLN("[MAINTAIN_MW_CREATION] [bWdShowPedsStillToKill] NOT Setting biS_AllVehiclesDestroyed / biS_AllVehiclePedsDead iMwTotalVehiclesSpawned = ", serverBD.iMwTotalVehiclesSpawned, " <GET_MAX_NUMBER_OF_VEHICLE_SPAWNS() = ", GET_MAX_NUMBER_OF_VEHICLE_SPAWNS())
			ENDIF
			#ENDIF
			
		ENDIF
		
		#IF IS_DEBUG_BUILD
		VECTOR vTempCoord 
		INT iR, iG, iB, iA
		#ENDIF
		
		REPEAT serverBD.iMaxVehicles thisVeh	
			#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_VehCheck")
				OR bWdTimedDebug
					PRINTLN("[MAINTAIN_MW_CREATION] [sc_VehCheck] Checking thisVeh = ", thisVeh, " serverBD.iMaxVehicles = ", serverBD.iMaxVehicles)
				ENDIF
			#ENDIF
	
	//		PRINTLN("     ---------->     KILL LIST - MAINTAIN_MW_CREATION serverBD.iMaxVehicles = ", serverBD.iMaxVehicles, " thisVeh = ",thisVeh )
			//Dead Checks
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niMwVeh[thisVeh])
				#IF IS_DEBUG_BUILD
					IF GET_COMMANDLINE_PARAM_EXISTS("sc_VehCheck")
					OR bWdTimedDebug
						PRINTLN("[MAINTAIN_MW_CREATION] [sc_VehCheck] NI Exists thisVeh = ", thisVeh)
					ENDIF
				#ENDIF
				
				
				
				
				IF UW_VEHICLE_OK(thisVeh)
					#IF IS_DEBUG_BUILD
						IF GET_COMMANDLINE_PARAM_EXISTS("sc_VehCheck")
						OR bWdTimedDebug
							PRINTLN("[MAINTAIN_MW_CREATION] [sc_VehCheck] UW_VEHICLE_OK thisVeh = ", thisVeh)
						ENDIF
					#ENDIF
				
					IF NOT IS_BIT_SET(serverBD.iCreateMwVehBitSet, thisVeh) // Wait for the veh to actually be created and populated
						IF (serverBD.VehModel <> HYDRA AND NOT IS_VEHICLE_EMPTY(NET_TO_VEH(serverBD.niMwVeh[thisVeh]), TRUE))
						OR serverBD.VehModel = HYDRA
							CLEAR_BIT(serverBD.iServerBitSet, biS_AllVehiclesDestroyed)
							IF HAS_NET_TIMER_STARTED(timeVehEmpty[thisVeh])
								RESET_NET_TIMER(timeVehEmpty[thisVeh])
							ENDIF
							#IF IS_DEBUG_BUILD
							IF serverBD.VehModel = HYDRA
								IF IS_VEHICLE_EMPTY(NET_TO_VEH(serverBD.niMwVeh[thisVeh]), TRUE)
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - HYDRA VEHICLE IS EMPTY! VEH ",thisVeh) 
								ENDIF
							ENDIF 
							
							IF bWdShowPedsStillToKill
							OR bWdTimedDebug
								
								vTempCoord = GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niMwVeh[thisVeh]))
								GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
								
								IF bWdShowPedsStillToKill
									DRAW_MARKER( MARKER_CYLINDER,
									vTempCoord,
									<<0,0,0>>,
									<<0,0,0>>,
									<<3.0, 3.0,100.0>>,
									iR,
									iG,
									iB,
									iA )
								ENDIF
								PRINTLN("[MAINTAIN_MW_CREATION] [bWdShowPedsStillToKill] Cleared biS_AllVehiclesDestroyed - 1 - veh still alive = ", thisVeh, " Coord = ", vTempCoord)
								
							ENDIF
							#ENDIF
							
							
						ELSE
							//-- Not hydra variation, vehicle is alive, but enpty
							IF IS_VEHICLE_EMPTY(NET_TO_VEH(serverBD.niMwVeh[thisVeh]), TRUE)
							AND GET_ENTITY_MODEL(NET_TO_VEH(serverBD.niMwVeh[thisVeh])) = SAVAGE
								IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllVehiclesDestroyed)
									CLEAR_BIT(serverBD.iServerBitSet, biS_AllVehiclesDestroyed)
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - (SAVAGE) [MAINTAIN_MW_CREATION] [bWdShowPedsStillToKill] Cleared biS_AllVehiclesDestroyed SAVAGE EMPTY VEH ", thisVeh)
								ENDIF
								
								IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_EnemySavageEmpty)
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - (SAVAGE) MW VEH WANT TO CLEAN UP VEH ", thisVeh, " AS IS_VEHICLE_EMPTY(). WILL SEND EVENT")
									
									BROADCAST_KILL_LIST_SAVAGE_EMPTY(ALL_PLAYERS_ON_SCRIPT(), thisVeh)
									
									SET_BIT(serverBD.iServerBitSet, biS_EnemySavageEmpty)

								ENDIF
							ELSE
								//-- Vehicle is empty. Did anybody damage it?
								IF DID_ANY_PLAYER_DO_DAMAGE_TO_UW_VEH_TRACKING_CHECK(thisVeh)
								OR NOT IS_COMPETITIVE_UW()
									//-- Still need to clean up the empty veh at some point. Do it when server detects who killed / last damaged the veh
									IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllVehiclesDestroyed)
										CLEAR_BIT(serverBD.iServerBitSet, biS_AllVehiclesDestroyed)
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - (SAVAGE) [MAINTAIN_MW_CREATION] [bWdShowPedsStillToKill] Cleared biS_AllVehiclesDestroyed EMPTY VEH ", thisVeh)
									ENDIF
									
									IF NOT IS_BIT_SET(serverBD.iVehEmptyBitSet,thisVeh )
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MW VEH WANT TO CLEAN UP VEH ", thisVeh, " AS IS_VEHICLE_EMPTY(). WILL SEND EVENT")
										BROADCAST_KILL_LIST_VEH_EMPTY(ALL_PLAYERS_ON_SCRIPT(), thisVeh)
										SET_BIT(serverBD.iVehEmptyBitSet,thisVeh )
									ENDIF
								ELSE
									//-- Vehicle is empty, and nobody damaged it. Just clean up
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - VEH EMPTY AND DID_ANY_PLAYER_DO_DAMAGE_TO_UW_VEH_TRACKING_CHECK FALSE WILL CLEANUP MW VEH ", thisVeh)
									CLEANUP_NET_ID(serverBD.niMwVeh[thisVeh])
									IF NOT IS_BIT_SET(serverbd.iVehCleanedUpBitset, thisVeh)
										
										serverbd.iVehiclesSpawned--
										
										IF GET_CURRENT_WAVE_TYPE() > WAVE_TYPE_NONE
											IF serverbd.iVehicleSpawnedThisWave > 0
												serverbd.iVehKillsThisWave++
												BROADCAST_KILL_LIST_VEHICLE_RESET(thisVeh, ALL_PLAYERS_ON_SCRIPT())
											ENDIF
										ENDIF
										SET_BIT(serverbd.iVehCleanedUpBitset, thisVeh)
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MW VEH ", thisVeh, " CLEANED UP - IS_VEHICLE_EMPTY Set bit ", thisVeh, " serverbd.iVehiclesSpawned = ", serverbd.iVehiclesSpawned)
									ENDIF
									
									RESET_NET_TIMER(timeVehEmpty[thisVeh])
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MW VEH ", thisVeh, " CLEANED UP - IS_VEHICLE_EMPTY - ")
								ENDIF
							ENDIF		
								
							
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							IF GET_COMMANDLINE_PARAM_EXISTS("sc_VehCheck")
							OR bWdTimedDebug
								PRINTLN("[MAINTAIN_MW_CREATION] [sc_VehCheck] WAITNG FOR iCreateMwVehBitSet thisVeh = ", thisVeh)
							ENDIF
						#ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						IF GET_COMMANDLINE_PARAM_EXISTS("sc_VehCheck")
						OR bWdTimedDebug
							PRINTLN("[MAINTAIN_MW_CREATION] [sc_VehCheck] UW_VEHICLE_OK FAILING thisVeh = ", thisVeh)
						ENDIF
					#ENDIF
					
					IF WAIT_FOR_VEHICLE_DESTROYED_BITSET_UPDATE(thisVeh)
						
						IF NOT IS_BIT_SET(serverbd.iVehCleanedUpBitset, thisVeh)
							serverbd.iVehiclesSpawned--
							
							IF GET_CURRENT_WAVE_TYPE() > WAVE_TYPE_NONE
								IF serverbd.iVehicleSpawnedThisWave > 0
									serverbd.iVehKillsThisWave++
									BROADCAST_KILL_LIST_VEHICLE_RESET(thisVeh, ALL_PLAYERS_ON_SCRIPT())
								ENDIF
							ENDIF
//							MODEL_NAMES mTarget = GET_ENTITY_MODEL(NET_TO_VEH(serverBD.niMwVeh[thisVeh]))
//							serverbd.iTotalKills+=GET_KILL_LIST_VEHICLE_CAPACITY(mTarget)	
						//	IF IS_COMPETITIVE_UW()
								IF serverBD.VehModel = HYDRA
									serverbd.iTotalKills++
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MW VEH ", thisVeh, " CLEANED UP - NOT UW_VEHICLE_OK INCREASING iTotalKills = ", serverbd.iTotalKills)
								ENDIF
						//	ENDIF
							SET_BIT(serverbd.iVehCleanedUpBitset, thisVeh)
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MW VEH ", thisVeh, " CLEANED UP - NOT UW_VEHICLE_OK Set bit ", thisVeh, " serverbd.iVehiclesSpawned = ", serverbd.iVehiclesSpawned, " serverbd.iTotalKills = ", serverbd.iTotalKills, " Team 1 score = ", serverBD.iKillsPerVehicle[0], " Team 2 score = ",   serverBD.iKillsPerVehicle[1])
						ENDIF
						IF HAS_NET_TIMER_STARTED(serverBD.timeCleanupVeh[thisVeh])
							RESET_NET_TIMER(serverBD.timeCleanupVeh[thisVeh])
						ENDIF
						CLEANUP_NET_ID(serverBD.niMwVeh[thisVeh])
						PRINTLN("     ---------->     KILL LIST - MW VEH ", thisVeh, " CLEANED UP - NOT DRIVABLE ")
					ELSE
						//-- Nobody claimed vehicle kill yet. Start a timeout so it gets cleaned up eventually.
						IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllVehiclesDestroyed)
							CLEAR_BIT(serverBD.iServerBitSet, biS_AllVehiclesDestroyed)
							#IF IS_DEBUG_BUILD
								PRINTLN("---------->     KILL LIST  CLEAR biS_AllVehiclesDestroyed as WAIT_FOR_VEHICLE_DESTROYED_BITSET_UPDATE veh ", thisVeh)
							#ENDIF
						ENDIF
						
						IF NOT IS_BIT_SET(serverBD.iTargetVehDestroyedBitSet, thisVeh)
							PRINTLN("     ---------->     KILL LIST - MW VEH WANT TO CLEAN UP VEH ", thisVeh, " BUT iTargetVehDestroyedBitSet NOT SET")
							IF NOT HAS_NET_TIMER_STARTED(serverBD.timeCleanupVeh[thisVeh])
								START_NET_TIMER(serverBD.timeCleanupVeh[thisVeh])
								PRINTLN("     ---------->     KILL LIST - MW VEH WANT TO CLEAN UP VEH ", thisVeh, " STARTED timeCleanupVeh" )
							ELSE
								IF HAS_NET_TIMER_EXPIRED(serverBD.timeCleanupVeh[thisVeh], 5000)
									//-- Still nobody claimed th keill. Just clean up
									PRINTLN("     ---------->     KILL LIST - MW VEH WANT TO CLEAN UP VEH ", thisVeh, " timeCleanupVeh HAS EXPIRED WILL SET BIT ", thisVeh)
									SET_BIT(serverBD.iTargetVehDestroyedBitSet, thisVeh)
								ELIF HAS_NET_TIMER_EXPIRED(serverBD.timeCleanupVeh[thisVeh], 3000)
									//-- Try sending the vehicle empty event to run the damage checks to catch cases when there's no damage event
									IF GET_ENTITY_MODEL(NET_TO_VEH(serverBD.niMwVeh[thisVeh])) = SAVAGE
										//-- seperate check for savage boss
										IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_EnemySavageEmpty)
											CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - (SAVAGE) MW VEH WANT TO CLEAN UP VEH ", thisVeh, " NOBOBY HAS CLAIMED KILL YET. WILL TRY SENDING EMPTY EVENT")
											BROADCAST_KILL_LIST_SAVAGE_EMPTY(ALL_PLAYERS_ON_SCRIPT(), thisVeh)
											SET_BIT(serverBD.iServerBitSet, biS_EnemySavageEmpty)
										ENDIF
									ELSE
										IF DID_ANY_PLAYER_DO_DAMAGE_TO_UW_VEH_TRACKING_CHECK(thisVeh)
										OR NOT IS_COMPETITIVE_UW()										
											IF NOT IS_BIT_SET(serverBD.iVehEmptyBitSet,thisVeh )
												CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MW VEH WANT TO CLEAN UP VEH ", thisVeh, " NOBOBY HAS CLAIMED KILL YET. WILL TRY SENDING EMPTY EVENT")
												BROADCAST_KILL_LIST_VEH_EMPTY(ALL_PLAYERS_ON_SCRIPT(), thisVeh)
												SET_BIT(serverBD.iVehEmptyBitSet,thisVeh )
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							PRINTLN("     ---------->     KILL LIST - MW VEH WANT TO CLEAN UP VEH ", thisVeh, " BUT CAN'T")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			thisPed = 0
			INT iBitToCheck
			
			
			REPEAT serverBD.iVehSeats thisPed
				#IF IS_DEBUG_BUILD
					IF GET_COMMANDLINE_PARAM_EXISTS("sc_PedCheck")
						PRINTLN("[MAINTAIN_MW_CREATION] [sc_PedCheck] Checking in-vehicle peds. Checking ped ",(thisVeh*MAX_MW_PEDS_PER_VEH)+thisPed, " thisVeh = ", thisVeh, " thisPed = ", thisPed)
					ENDIF
				#ENDIF
			
		//		PRINTLN("     ---------->     KILL LIST - MAINTAIN_MW_CREATION serverBD.iVehSeats = ", serverBD.iVehSeats, " thisPed = ",thisPed, " MAX_MW_PEDS_PER_VEH = ", MAX_MW_PEDS_PER_VEH )
				IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+thisPed])
					iBitToCheck = (thisVeh*MAX_MW_PEDS_PER_VEH)+thisPed
				
					IF NOT IS_NET_PED_INJURED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+thisPed])
						IF IS_PED_IN_ANY_VEHICLE(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+thisPed]))
						//	IF IS_PED_VEHICLE_DRIVABLE((thisVeh*MAX_MW_PEDS_PER_VEH)+thisPed)
							IF NOT SHOULD_CLEAN_UP_UW_PED_DUE_TO_PED_VEHICLE_STATE((thisVeh*MAX_MW_PEDS_PER_VEH)+thisPed)
								CLEAR_BIT(serverBD.iServerBitSet, biS_AllVehiclesDestroyed)
								
								#IF IS_DEBUG_BUILD
								IF bWdShowPedsStillToKill
								OR bWdTimedDebug
									PRINTLN("[MAINTAIN_MW_CREATION] [bWdShowPedsStillToKill] Cleared biS_AllVehiclesDestroyed - 2 - ped still alive = ", (thisVeh*MAX_MW_PEDS_PER_VEH)+thisPed)
								ENDIF
								#ENDIF
							ELSE 


								IF WAIT_FOR_PED_KILLED_BITSET_UPDATE(iBitToCheck)
									IF NOT HAS_NET_TIMER_EXPIRED(serverBD.timePedRespawn[(thisVeh*MAX_MW_PEDS_PER_VEH)+thisPed], 5000)
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - 	TRYING TO CLEAN UP PED ", (thisVeh*MAX_MW_PEDS_PER_VEH)+thisPed, " BUT TIMER HASN'T EXPIRED (1)")
										net_script_assert("Timer 1!")
									ENDIF
									
									IF DOES_PED_HAVE_AI_BLIP(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+thisPed]))
									//	SET_PED_HAS_AI_BLIP(NET_TO_PED(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+thisPed]), FALSE)
										CLEANUP_AI_PED_BLIP(MwPedBlipData[(thisVeh*MAX_MW_PEDS_PER_VEH)+thisPed])
										PRINTLN("     ---------->     KILL LIST - IN VEH PED ", (thisVeh*MAX_MW_PEDS_PER_VEH)+thisPed, " REMOVED AI BLIP AS ABOUT TO CLEAN UP")
									ENDIF
									
									CLEANUP_NET_ID(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+thisPed])
									

									IF IS_BIT_SET(serverBD.iServerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iBitToCheck)], GET_LONG_BITSET_BIT(iBitToCheck))
										IF serverbd.iPedState[(thisVeh*MAX_MW_PEDS_PER_VEH)+thisPed] > PED_AI_STATE_CREATED
											BROADCAST_KILL_LIST_TARGET_ON_FOOT_RESET((thisVeh*MAX_MW_PEDS_PER_VEH)+thisPed, ALL_PLAYERS_ON_SCRIPT())
											serverbd.iTotalKills++
											PRINTLN("     ---------->     KILL LIST - IN VEH PED ", (thisVeh*MAX_MW_PEDS_PER_VEH)+thisPed, " CLEANED UP AS IS_PED_VEHICLE_DRIVABLE Increasing total kills iTotalKills = ", serverbd.iTotalKills, " PEd state = ", serverbd.iPedState[(thisVeh*MAX_MW_PEDS_PER_VEH)+thisPed], " Team 1 score = ", serverBD.iKillsPerVehicle[0], " Team 2 score = ",   serverBD.iKillsPerVehicle[1])
										ENDIF
									ENDIF
									serverbd.iPedState[(thisVeh*MAX_MW_PEDS_PER_VEH)+thisPed] = PED_AI_STATE_CREATED
									
									IF HAS_NET_TIMER_STARTED(serverBD.timeCleanupPeds[iBitToCheck])
										RESET_NET_TIMER(serverBD.timeCleanupPeds[iBitToCheck])
									ENDIF
									
									IF IS_BIT_SET(serverBD.iPendingPedCleanupBitset[GET_LONG_BITSET_INDEX(iBitToCheck)], GET_LONG_BITSET_BIT(iBitToCheck))
										CLEAR_BIT(serverBD.iPendingPedCleanupBitset[GET_LONG_BITSET_INDEX(iBitToCheck)], GET_LONG_BITSET_BIT(iBitToCheck))
									ENDIF
									
									
									PRINTLN("     ---------->     KILL LIST - IN VEH PED ", (thisVeh*MAX_MW_PEDS_PER_VEH)+thisPed, " CLEANED UP AS IS_PED_VEHICLE_DRIVABLE iTotalKills = ", serverbd.iTotalKills, " Team 1 score = ", serverBD.iKillsPerVehicle[0], " Team 2 score = ",   serverBD.iKillsPerVehicle[1])
								ELSE
									//-- Ped injured but nobody has yet claimed the kill....
									IF NOT IS_BIT_SET(serverBD.iServerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iBitToCheck)], GET_LONG_BITSET_BIT(iBitToCheck))
										PRINTLN("     ---------->     KILL LIST - WANT TO CLEAN UP IN VEH PED ", iBitToCheck, " AS SHOULD_CLEAN_UP_UW_PED_DUE_TO_PED_VEHICLE_STATE BUT iServerTargetPedKilledBitset NOT SET. WILL START TIMEOUT")
										IF NOT HAS_NET_TIMER_STARTED(serverBD.timeCleanupPeds[iBitToCheck])
											START_NET_TIMER(serverBD.timeCleanupPeds[iBitToCheck])
											PRINTLN("     ---------->     KILL LIST - WANT TO CLEAN UP IN VEH PED ", iBitToCheck, " AS SHOULD_CLEAN_UP_UW_PED_DUE_TO_PED_VEHICLE_STATE BUT iServerTargetPedKilledBitset NOT SET. TIMER STARTED")
										ELSE
											IF HAS_NET_TIMER_EXPIRED(serverBD.timeCleanupPeds[iBitToCheck], 7000)
												PRINTLN("     ---------->     KILL LIST - WANT TO CLEAN UP IN VEH PED ", iBitToCheck, " AS SHOULD_CLEAN_UP_UW_PED_DUE_TO_PED_VEHICLE_STATE BUT iServerTargetPedKilledBitset NOT SET. TIMEOUT EXPIRED. WILL SET BIT")
												SET_BIT(serverBD.iServerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iBitToCheck)], GET_LONG_BITSET_BIT(iBitToCheck))
												RESET_NET_TIMER(serverBD.timeCleanupPeds[iBitToCheck])
											ENDIF
										ENDIF
									ELSE
										PRINTLN("     ---------->     KILL LIST - WANT TO CLEAN UP IN VEH PED ", iBitToCheck, " AS SHOULD_CLEAN_UP_UW_PED_DUE_TO_PED_VEHICLE_STATE BUT DON'T KNOW WHY")
									ENDIF
									
									IF NOT IS_BIT_SET(serverBD.iPendingPedCleanupBitset[GET_LONG_BITSET_INDEX(iBitToCheck)], GET_LONG_BITSET_BIT(iBitToCheck))
										SET_BIT(serverBD.iPendingPedCleanupBitset[GET_LONG_BITSET_INDEX(iBitToCheck)], GET_LONG_BITSET_BIT(iBitToCheck))
									ENDIF
								ENDIF
							
							ENDIF
						ELSE
							PRINTLN("     ---------->     KILL LIST - IN VEH PED ", (thisVeh*MAX_MW_PEDS_PER_VEH)+thisPed, " Not in a vehicle! ****** POSSIBLE FAIL ******")										
//							CLEANUP_NET_ID(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+thisPed])
						ENDIF
					
					ELSE
						IF IS_BIT_SET(serverBD.iServerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iBitToCheck)], GET_LONG_BITSET_BIT(iBitToCheck))
					//	OR NOT IS_COMPETITIVE_UW()
						OR serverBD.VehModel = HYDRA
							IF NOT HAS_NET_TIMER_EXPIRED(serverBD.timePedRespawn[(thisVeh*MAX_MW_PEDS_PER_VEH)+thisPed], 5000)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - 	TRYING TO CLEAN UP PED ", (thisVeh*MAX_MW_PEDS_PER_VEH)+thisPed, " BUT TIMER HASN'T EXPIRED (2)")
								net_script_assert("Timer 2!")
							ENDIF
							CLEANUP_NET_ID(serverBD.niMwPed[(thisVeh*MAX_MW_PEDS_PER_VEH)+thisPed])
							

							IF IS_BIT_SET(serverBD.iServerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iBitToCheck)], GET_LONG_BITSET_BIT(iBitToCheck))
							
								IF serverbd.iPedState[(thisVeh*MAX_MW_PEDS_PER_VEH)+thisPed] > PED_AI_STATE_CREATED
									serverbd.iTotalKills++
									BROADCAST_KILL_LIST_TARGET_ON_FOOT_RESET((thisVeh*MAX_MW_PEDS_PER_VEH)+thisPed, ALL_PLAYERS_ON_SCRIPT())
									PRINTLN("     ---------->     KILL LIST - IN VEH PED ", (thisVeh*MAX_MW_PEDS_PER_VEH)+thisPed, " CLEANED UP AS IS_NET_PED_INJURED Increasing total kills iTotalKills = ", serverbd.iTotalKills, " PEd state = ", serverbd.iPedState[(thisVeh*MAX_MW_PEDS_PER_VEH)+thisPed], " Team 1 score = ", serverBD.iKillsPerVehicle[0], " Team 2 score = ",   serverBD.iKillsPerVehicle[1])
								ENDIF
							ENDIF
							serverbd.iPedState[(thisVeh*MAX_MW_PEDS_PER_VEH)+thisPed] = PED_AI_STATE_CREATED
							
							IF IS_BIT_SET(serverBD.iPendingPedCleanupBitset[GET_LONG_BITSET_INDEX(iBitToCheck)], GET_LONG_BITSET_BIT(iBitToCheck))
								CLEAR_BIT(serverBD.iPendingPedCleanupBitset[GET_LONG_BITSET_INDEX(iBitToCheck)], GET_LONG_BITSET_BIT(iBitToCheck))
							ENDIF
									
							PRINTLN("     ---------->     KILL LIST - IN VEH PED ", (thisVeh*MAX_MW_PEDS_PER_VEH)+thisPed, " CLEANED UP AS IS_NET_PED_INJURED iTotalKills = ", serverbd.iTotalKills, " Team 1 score = ", serverBD.iKillsPerVehicle[0], " Team 2 score = ",   serverBD.iKillsPerVehicle[1])
						ELSE
							IF NOT IS_BIT_SET(serverBD.iServerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iBitToCheck)], GET_LONG_BITSET_BIT(iBitToCheck))
								
								IF NOT HAS_NET_TIMER_STARTED(serverBD.timeCleanupPeds[iBitToCheck])
									START_NET_TIMER(serverBD.timeCleanupPeds[iBitToCheck])
									PRINTLN("     ---------->     KILL LIST - WANT TO CLEAN UP IN VEH PED ", iBitToCheck, " AS PED INJURED BUT iServerTargetPedKilledBitset NOT SET.  WILL START TIMEOUT. TIMEOUT STARTED")
								ELSE
									IF HAS_NET_TIMER_EXPIRED(serverBD.timeCleanupPeds[iBitToCheck], 7000)
										PRINTLN("     ---------->     KILL LIST - WANT TO CLEAN UP IN VEH PED ", iBitToCheck, " AS PED INJURED BUT iServerTargetPedKilledBitset NOT SET. TIMEOUT EXPIRED. WILL SET BIT")
										SET_BIT(serverBD.iServerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iBitToCheck)], GET_LONG_BITSET_BIT(iBitToCheck))
										RESET_NET_TIMER(serverBD.timeCleanupPeds[iBitToCheck])
									ENDIF
								ENDIF
							ELSE
								PRINTLN("     ---------->     KILL LIST - WANT TO CLEAN UP IN VEH PED ", iBitToCheck, " AS PED INJURED BUT DON'T KNOW WHY")
							ENDIF
							
							IF NOT IS_BIT_SET(serverBD.iPendingPedCleanupBitset[GET_LONG_BITSET_INDEX(iBitToCheck)], GET_LONG_BITSET_BIT(iBitToCheck))
								SET_BIT(serverBD.iPendingPedCleanupBitset[GET_LONG_BITSET_INDEX(iBitToCheck)], GET_LONG_BITSET_BIT(iBitToCheck))
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
			//Spawn Veh with Peds
			IF serverBD.iMwTotalVehiclesSpawned < GET_MAX_NUMBER_OF_VEHICLE_SPAWNS()
				//IF TRUE	//SHOULD_SPAWN_THIS_VEHICLE()
					IF CREATE_MW_VEH_AND_PEDS(thisVeh)
						//
					ENDIF
				//ENDIF
				CLEAR_BIT(serverBD.iServerBitSet, biS_AllVehiclesDestroyed)
				
				#IF IS_DEBUG_BUILD
				IF bWdShowPedsStillToKill
				OR bWdTimedDebug
					PRINTLN("[MAINTAIN_MW_CREATION] [bWdShowPedsStillToKill] Cleared biS_AllVehiclesDestroyed serverBD.iMwTotalVehiclesSpawned = ", serverBD.iMwTotalVehiclesSpawned, " < GET_MAX_NUMBER_OF_VEHICLE_SPAWNS = ", GET_MAX_NUMBER_OF_VEHICLE_SPAWNS() )
				ENDIF
				#ENDIF
			ENDIF
		ENDREPEAT
	ELSE
		#IF IS_DEBUG_BUILD
			IF bWdTimedDebug
				INT iDebug
				REPEAT serverBD.iMaxVehicles iDebug
					IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niMwVeh[iDebug])
						PRINTLN("[MAINTAIN_MW_CREATION] NI EXISTS ", iDebug, " BUT biS_AllVehiclesDestroyed = ", IS_BIT_SET(serverBD.iServerBitSet, biS_AllVehiclesDestroyed), " biS_AllVehiclePedsDead = ", IS_BIT_SET(serverBD.iServerBitSet, biS_AllVehiclePedsDead))
					ENDIF
				ENDREPEAT
			ENDIF
		#ENDIF
	ENDIF
	
	//ON FOOT PEDS
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AllOnFootPedsDead)
		IF serverBD.iMwPedsSpawned >= g_sMPTunables.iUrbanWarfareEnemyOnFootSpawns 
			SET_BIT(serverBD.iServerBitSet, biS_AllOnFootPedsDead)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_PedCheck")
				PRINTLN("[MAINTAIN_MW_CREATION] [sc_PedCheck] About to check on-foot peds ")
			ENDIF
		#ENDIF
				
	//	IF GET_CURRENT_WAVE_TYPE() <> WAVE_TYPE_AIRCRAFT_ONLY
	//	AND GET_CURRENT_WAVE_TYPE() <> WAVE_TYPE_BOSS
	
			thisPed = 0
			
			REPEAT serverBD.iMaxOnFloorPeds thisPed
				#IF IS_DEBUG_BUILD
					IF GET_COMMANDLINE_PARAM_EXISTS("sc_PedCheck")
						PRINTLN("[MAINTAIN_MW_CREATION] [sc_PedCheck] Checking on-foot peds thisPed = ", thisPed, " actual ped ", (MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed, " serverBD.iMaxOnFloorPeds = ", serverBD.iMaxOnFloorPeds)
					ENDIF
				#ENDIF
				
			//	PRINTLN("     ---------->     KILL LIST - MAINTAIN_MW_CREATION ON FOOT PEDS serverBD.iMaxOnFloorPeds = ", serverBD.iMaxOnFloorPeds, " thisPed = ",thisPed, " MAX_MW_PEDS_PER_VEH = ", MAX_MW_PEDS_PER_VEH, " MAX_MW_VEHS = ", MAX_MW_VEHS  )
				//Dead Checks
				IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niMwPed[(MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed])
					INT iBitToCheck = (MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed
					
					IF NOT IS_ENTITY_DEAD(NET_TO_PED(serverBD.niMwPed[(MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed]))
						CLEAR_BIT(serverBD.iServerBitSet, biS_AllOnFootPedsDead)
						
						#IF IS_DEBUG_BUILD
							IF IS_BIT_SET(serverBD.iServerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iBitToCheck)], GET_LONG_BITSET_BIT(iBitToCheck))
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - ON FOOT PED ", iBitToCheck, " is alive but server killed bit is set!!!! iBitToCheck = ", iBitToCheck, " (MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed = ", (MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed)
							ENDIF
						#ENDIF
					ELSE
						
						
						IF IS_BIT_SET(serverBD.iServerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iBitToCheck)], GET_LONG_BITSET_BIT(iBitToCheck))
					//	OR NOT IS_COMPETITIVE_UW()
							IF NOT HAS_NET_TIMER_EXPIRED(serverBD.timePedRespawn[(MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed], 5000)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - 	TRYING TO CLEAN UP PED ", (MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed, " BUT TIMER HASN'T EXPIRED (3)")
								net_script_assert("Timer 3!")
							ENDIF
							
							IF HAS_NET_TIMER_STARTED(serverBD.timeCleanupPeds[iBitToCheck])
								RESET_NET_TIMER(serverBD.timeCleanupPeds[iBitToCheck])
							ENDIF
							
							CLEANUP_NET_ID(serverBD.niMwPed[(MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed])
							
							IF serverbd.iPedState[( (MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed)] > PED_AI_STATE_CREATED
								serverbd.iTotalKills++
								
								IF GET_CURRENT_WAVE_TYPE() > WAVE_TYPE_NONE
									IF serverbd.iPedOnFootSpawnedThisWave > 0
										serverbd.iPedOnFootKillsThisWave++
										
									ENDIF
								ENDIF
								BROADCAST_KILL_LIST_TARGET_ON_FOOT_RESET((MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed, ALL_PLAYERS_ON_SCRIPT())
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - ON FOOT PED ", (MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed, " CLEANED UP INCREASING serverbd.iTotalKills = ", serverbd.iTotalKills, " Team 1 score = ", serverBD.iKillsPerVehicle[0], " Team 2 score = ",   serverBD.iKillsPerVehicle[1])
							ELSE

							ENDIF
							
							serverbd.iPedState[ (MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH)+thisPed] = PED_AI_STATE_CREATED
							IF IS_BIT_SET(serverBD.iPendingPedCleanupBitset[GET_LONG_BITSET_INDEX(iBitToCheck)], GET_LONG_BITSET_BIT(iBitToCheck))
								CLEAR_BIT(serverBD.iPendingPedCleanupBitset[GET_LONG_BITSET_INDEX(iBitToCheck)], GET_LONG_BITSET_BIT(iBitToCheck))
							ENDIF
							
						ELSE
							IF NOT IS_BIT_SET(serverBD.iServerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iBitToCheck)], GET_LONG_BITSET_BIT(iBitToCheck))
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - ON FOOT PED WANT TO CLEAN UP ON FOOT PED ",  iBitToCheck, " BUT iServerTargetPedKilledBitset NOT SET! WILL START TIMEOUT")
								IF NOT HAS_NET_TIMER_STARTED(serverBD.timeCleanupPeds[iBitToCheck])
									START_NET_TIMER(serverBD.timeCleanupPeds[iBitToCheck])
								ELSE
									IF HAS_NET_TIMER_EXPIRED(serverBD.timeCleanupPeds[iBitToCheck], 7000)
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - ON FOOT PED WANT TO CLEAN UP ON-FOOT PED ",  iBitToCheck, " BUT iServerTargetPedKilledBitset NOT SET! TIMEOUT EXPIRED")
										SET_BIT(serverBD.iServerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iBitToCheck)], GET_LONG_BITSET_BIT(iBitToCheck))
										RESET_NET_TIMER(serverBD.timeCleanupPeds[iBitToCheck])
									ENDIF
								ENDIF
							ELSE
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - ON FOOT PED WANT TO CLEAN UP ON-FOOT PED ",  iBitToCheck, " BUT DON'T KNOW WHY!")
							ENDIF
							
							
							
							IF NOT IS_BIT_SET(serverBD.iPendingPedCleanupBitset[GET_LONG_BITSET_INDEX(iBitToCheck)], GET_LONG_BITSET_BIT(iBitToCheck))
								SET_BIT(serverBD.iPendingPedCleanupBitset[GET_LONG_BITSET_INDEX(iBitToCheck)], GET_LONG_BITSET_BIT(iBitToCheck))
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//Spawn On Foot Peds
				IF serverBD.iMwPedsSpawned < GET_MAX_NUMBER_OF_ON_FOOT_PED_SPAWNS() // g_sMPTunables.iUrbanWarfareEnemyOnFootSpawns 
//					IF TRUE	//SHOULD_SPAWN_THIS_PED()
						IF CREATE_ON_FOOT_PED(thisPed)
							//
//						ENDIF
					ENDIF
					CLEAR_BIT(serverBD.iServerBitSet, biS_AllOnFootPedsDead)
					
					#IF IS_DEBUG_BUILD
					IF bWdShowPedsStillToKill
					OR bWdTimedDebug
						PRINTLN("[MAINTAIN_MW_CREATION] [bWdShowPedsStillToKill] Cleared biS_AllOnFootPedsDead serverBD.iMwPedsSpawned = ", serverBD.iMwPedsSpawned, " < GET_MAX_NUMBER_OF_ON_FOOT_PED_SPAWNS() = ", GET_MAX_NUMBER_OF_ON_FOOT_PED_SPAWNS() )
					ENDIF
					#ENDIF
				ENDIF
			ENDREPEAT
	//	ENDIF
	ENDIF
	
	UPDATE_CURRENT_WAVE()
	
	#IF IS_DEBUG_BUILD
		BOOL bDbAllVehDead = IS_BIT_SET(serverBD.iServerBitSet, biS_AllVehiclesDestroyed)
		BOOL bDbAllVehPedsDead = IS_BIT_SET(serverBD.iServerBitSet, biS_AllVehiclePedsDead)
		BOOL bDbAllOnFootPedsDead = IS_BIT_SET(serverBD.iServerBitSet, biS_AllOnFootPedsDead)
		
		IF bWdShowPedsStillToKill
		OR bWdTimedDebug
			PRINTLN("[MAINTAIN_MW_CREATION] [bWdShowPedsStillToKill] bDbAllVehDead = ", bDbAllVehDead, " bDbAllVehPedsDead = ", bDbAllVehPedsDead, " bDbAllOnFootPedsDead = ", bDbAllOnFootPedsDead)
		ENDIF
		
		IF serverBD.iPendingPedCleanupBitset[0] <> 0
		OR serverBD.iPendingPedCleanupBitset[1] <> 0
			PRINTLN("     ---------->     [PENDING_CLEANUP] These peds are pending cleanup. Flags are bDbAllVehDead = ",bDbAllVehDead, " bDbAllVehPedsDead = ", bDbAllVehPedsDead ," bDbAllOnFootPedsDead = ", bDbAllOnFootPedsDead)
			
			INT iPend
			VECTOR vTempCoords
			REPEAT MAX_MW_PEDS iPend
				IF IS_BIT_SET(serverBD.iPendingPedCleanupBitset[GET_LONG_BITSET_INDEX(iPend)], GET_LONG_BITSET_BIT(iPend))
					PRINTLN("     ---------->     [PENDING_CLEANUP] Ped pending cleanup... ", iPend)
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niMwPed[iPend])
						vTempCoords = GET_ENTITY_COORDS(NET_TO_PED(serverBD.niMwPed[iPend]), FALSE)
						PRINTLN("     ---------->     [PENDING_CLEANUP] NETWORK INDEX EXISTS ", iPend, " COORDS ARE ", vTempCoords)
						
						IF NOT IS_NET_PED_INJURED(serverBD.niMwPed[iPend])
							PRINTLN("     ---------->     [PENDING_CLEANUP] PED IS NOT INJURED ", iPend)
							
							IF IS_PED_IN_ANY_VEHICLE(NET_TO_PED(serverbd.niMwPed[iPend]))
								PRINTLN("     ---------->     [PENDING_CLEANUP] PED IS IN A VEHICLE ", iPend)
								IF SHOULD_CLEAN_UP_UW_PED_DUE_TO_PED_VEHICLE_STATE(iPend)
									PRINTLN("     ---------->     [PENDING_CLEANUP] SHOULD_CLEAN_UP_UW_PED_DUE_TO_PED_VEHICLE_STATE TRUE ", iPend)
								ELSE
									PRINTLN("     ---------->     [PENDING_CLEANUP] SHOULD_CLEAN_UP_UW_PED_DUE_TO_PED_VEHICLE_STATE FALSE ", iPend)
								ENDIF
							ENDIF
						ELSE
							PRINTLN("     ---------->     [PENDING_CLEANUP] PED IS INJURED ", iPend)
						ENDIF
						
						IF IS_BIT_SET(serverBD.iServerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iPend)], GET_LONG_BITSET_BIT(iPend))
							PRINTLN("     ---------->     [PENDING_CLEANUP] iServerTargetPedKilledBitset is SET FOR PED ", iPend)
						ELSE
							PRINTLN("     ---------->     [PENDING_CLEANUP] iServerTargetPedKilledBitset is NOT SET FOR PED ", iPend)
						ENDIF
					ENDIF
					PRINTLN("     ---------->     [PENDING_CLEANUP] ")
				ENDIF
			ENDREPEAT
			
			
		ENDIF
	#ENDIF
ENDPROC



PROC SORT_LEADERBOARD_DATA_TEAMS()
	
	INT iCount, iCount2
	
	FOR iCount = (MAX_PLAYER_VEH - 1) TO 0 STEP -1
		
		#IF IS_DEBUG_BUILD
		IF bWdDoLeaderboardDebug
			PRINTLN("")
			PRINTLN("")
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST -  - SORT_LEADERBOARD_DATA_TEAMS *********** PARTICIPANT ", iCount, " ***********")
		ENDIF
		#ENDIF
		
		FOR iCount2 = (MAX_PLAYER_VEH - 1) TO 0 STEP -1
			
			IF ( (iCount2 - 1) >= 0 )
			
				#IF IS_DEBUG_BUILD
				
				INT iCount3 = iCount2 - 1
				
				IF bWdDoLeaderboardDebug
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST -  - SORT_LEADERBOARD_DATA_TEAMS iCount2 = ", iCount2, ", iCount2-1 = ", iCount3)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST -  - SORT_LEADERBOARD_DATA_TEAMS iCount2 Kills = ", serverBD.sLeaderboardDataTeams[iCount2].iKills, ", iCount2-1 Kills = ", serverBD.sLeaderboardDataTeams[iCount2-1].iKills)
				ENDIF
				
				#ENDIF
				
				IF serverBD.sLeaderboardDataTeams[iCount2].iKills > serverBD.sLeaderboardDataTeams[iCount2-1].iKills
					
					#IF IS_DEBUG_BUILD
					IF bWdDoLeaderboardDebug
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST -  - SORT_LEADERBOARD_DATA_TEAMS iCount2 time > than iCount2-1 time - doing swap!")
					ENDIF
					#ENDIF
					
					INT iTempTime = serverBD.sLeaderboardDataTeams[iCount2].iKills
					INT iTempPartId = serverBD.sLeaderboardDataTeams[iCount2].iTeam
					INT iTempPlayer = serverBD.sLeaderboardDataTeams[iCount2].iPlayer

					serverBD.sLeaderboardDataTeams[iCount2].iKills = serverBD.sLeaderboardDataTeams[iCount2-1].iKills
					serverBD.sLeaderboardDataTeams[iCount2].iTeam = serverBD.sLeaderboardDataTeams[iCount2-1].iTeam
					serverBD.sLeaderboardDataTeams[iCount2].iPlayer = serverBD.sLeaderboardDataTeams[iCount2-1].iPlayer

					
					serverBD.sLeaderboardDataTeams[iCount2-1].iKills = iTempTime
					serverBD.sLeaderboardDataTeams[iCount2-1].iTeam = iTempPartId
					serverBD.sLeaderboardDataTeams[iCount2-1].iPlayer = iTempPlayer

					

					
					#IF IS_DEBUG_BUILD
					IF bWdDoLeaderboardDebug
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST -  - SORT_LEADERBOARD_DATA_TEAMS swapped. iCount2 kills = ", serverBD.sLeaderboardDataTeams[iCount2].iKills, ", iCount2-1 kills = ", serverBD.sLeaderboardDataTeams[iCount2-1].iKills)
					ENDIF
					#ENDIF
					
				ENDIF
				
			ENDIF
			
		ENDFOR
		
	ENDFOR
	
ENDPROC

PROC SORT_LEADERBOARD_DATA_PLAYERS()
	
	INT iCount, iCount2
	
	FOR iCount = (NUM_NETWORK_PLAYERS - 1) TO 0 STEP -1
		
		#IF IS_DEBUG_BUILD
		IF bWdDoLeaderboardDebug
			PRINTLN("")
			PRINTLN("")
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST -  - SORT_LEADERBOARD_DATA_PLAYERS *********** PARTICIPANT ", iCount, " ***********")
		ENDIF
		#ENDIF
		
		FOR iCount2 = (NUM_NETWORK_PLAYERS - 1) TO 0 STEP -1
			
			IF ( (iCount2 - 1) >= 0 )
			
				#IF IS_DEBUG_BUILD
				
				INT iCount3 = iCount2 - 1
				
				IF bWdDoLeaderboardDebug
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST -  - SORT_LEADERBOARD_DATA_PLAYERS iCount2 = ", iCount2, ", iCount2-1 = ", iCount3)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST -  - SORT_LEADERBOARD_DATA_PLAYERS iCount2 Kills = ", serverBD.sLeaderboardData[iCount2].iKills, ", iCount2-1 Kills = ", serverBD.sLeaderboardData[iCount2-1].iKills)
				ENDIF
				
				#ENDIF
				
				IF serverBD.sLeaderboardData[iCount2].iKills > serverBD.sLeaderboardData[iCount2-1].iKills
					
					#IF IS_DEBUG_BUILD
					IF bWdDoLeaderboardDebug
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST -  - SORT_LEADERBOARD_DATA_PLAYERS iCount2 time > than iCount2-1 time - doing swap!")
					ENDIF
					#ENDIF
					
					INT iTempTime = serverBD.sLeaderboardData[iCount2].iKills
					INT iTempPartId = serverBD.sLeaderboardData[iCount2].iTeam
					INT iTempPlayer = serverBD.sLeaderboardData[iCount2].iPlayer

					serverBD.sLeaderboardData[iCount2].iKills = serverBD.sLeaderboardData[iCount2-1].iKills
					serverBD.sLeaderboardData[iCount2].iTeam = serverBD.sLeaderboardData[iCount2-1].iTeam
					serverBD.sLeaderboardData[iCount2].iPlayer = serverBD.sLeaderboardData[iCount2-1].iPlayer

					
					serverBD.sLeaderboardData[iCount2-1].iKills = iTempTime
					serverBD.sLeaderboardData[iCount2-1].iTeam = iTempPartId
					serverBD.sLeaderboardData[iCount2-1].iPlayer = iTempPlayer

					

					
					#IF IS_DEBUG_BUILD
					IF bWdDoLeaderboardDebug
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST -  - SORT_LEADERBOARD_DATA_PLAYERS swapped. iCount2 kills = ", serverBD.sLeaderboardData[iCount2].iKills, ", iCount2-1 kills = ", serverBD.sLeaderboardData[iCount2-1].iKills)
					ENDIF
					#ENDIF
					
				ENDIF
				
			ENDIF
			
		ENDFOR
		
	ENDFOR
	
ENDPROC

PROC FILL_PLAYER_LEADERBOARD_DATA(INT iTeam, INT iKills, INT iPlayer)
	
	serverBD.sLeaderboardData[iPlayer].iTeam = iTeam
	serverBD.sLeaderboardData[iPlayer].iKills = iKills
	serverBD.sLeaderboardData[iPlayer].iPlayer = iPlayer
	
	
ENDPROC

PROC CLEAR_PLAYER_LEADERBOARD_DATA(INT iPlayer)
	
	serverBD.sLeaderboardData[iPlayer].iTeam = -1
	serverBD.sLeaderboardData[iPlayer].iPlayer = -1
	serverBD.sLeaderboardData[iPlayer].iKills = -1
	
ENDPROC

PROC FILL_TEAM_LEADERBOARD_DATA(INT iTeam, INT iKills,  INT iPlayer)
	
	serverBD.sLeaderboardDataTeams[iTeam].iTeam = iTeam
	serverBD.sLeaderboardDataTeams[iTeam].iKills = iKills
	serverBD.sLeaderboardDataTeams[iTeam].iPlayer = iPlayer
	
	
ENDPROC

PROC CLEAR_TEAM_LEADERBOARD_DATA(INT iTeam)
	
	serverBD.sLeaderboardDataTeams[iTeam].iTeam = -1
	serverBD.sLeaderboardDataTeams[iTeam].iKills = -1
	serverBD.sLeaderboardDataTeams[iTeam].iPlayer = -1
	
ENDPROC


PROC UPDATE_KILL_LIST_LEADERBOARD()
	INT i

	REPEAT MAX_PLAYER_VEH i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
			CLEAR_TEAM_LEADERBOARD_DATA(i)
			
		//	IF IS_COMPETITIVE_UW()
			//	DEBUG_PRINTCALLSTACK()
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [UPDATE_KILL_LIST_LEADERBOARD] Call FILL_LEADERBOARD_DATA with veh = ", i, " iKills = ", serverBD.iKillsPerVehicle[i], " Player Int = ", NATIVE_TO_INT(serverBD.playerDriving[i]))
				FILL_TEAM_LEADERBOARD_DATA(i, serverBD.iKillsPerVehicle[i], NATIVE_TO_INT(serverBD.playerDriving[i]))
		//	ELSE
		//		FILL_TEAM_LEADERBOARD_DATA(i, serverBD.iTotalKills, NATIVE_TO_INT(serverBD.playerDriving[i]))
		//	ENDIF
		ELSE
			IF serverBD.sLeaderboardDataTeams[i].iKills <> -1
				CLEAR_TEAM_LEADERBOARD_DATA(i)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [UPDATE_KILL_LIST_LEADERBOARD] Call CLEAR_LEADERBOARD_DATA with veh = ", i, " as NI doesn't exist")
			ENDIF
		ENDIF
	ENDREPEAT
	SORT_LEADERBOARD_DATA_TEAMS()
	
	
	i = 0
	
	INT iPlayerUWTeam
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			CLEAR_PLAYER_LEADERBOARD_DATA(i)
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [UPDATE_KILL_LIST_LEADERBOARD] Checking player ", GET_PLAYER_NAME(PlayerId), " iInVehicleBitSet = ", playerBD[i].iInVehicleBitSet, " iMyUwTeam = ", playerBD[i].iMyUwTeam)
			IF playerBD[i].iInVehicleBitSet <> 0
				IF playerBD[i].iMyUwTeam > -1
				//	IF IS_COMPETITIVE_UW()
					//	DEBUG_PRINTCALLSTACK()
						
						iPlayerUWTeam = playerBD[i].iMyUwTeam
						
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [UPDATE_KILL_LIST_LEADERBOARD] Call FILL_PLAYER_LEADERBOARD_DATA with team = ", iPlayerUWTeam, " iKills = ",  playerBD[i].iMyKills, " Player Int = ", NATIVE_TO_INT(PlayerId), " who is player ", GET_PLAYER_NAME(playerID), " THEIR TEAM = ", playerBD[i].iMyUwTeam)
						FILL_PLAYER_LEADERBOARD_DATA(iPlayerUWTeam, playerBD[i].iMyKills,  NATIVE_TO_INT(PlayerId))
				//	ELSE
				//		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [UPDATE_KILL_LIST_LEADERBOARD] (NON-Competitive) Call FILL_PLAYER_LEADERBOARD_DATA with team = ", 0, " iKills = ",  playerBD[i].iMyKills, " Player Int = ", NATIVE_TO_INT(PlayerId), " who is player ", GET_PLAYER_NAME(playerID))
				//		FILL_PLAYER_LEADERBOARD_DATA(0, playerBD[i].iMyKills,  NATIVE_TO_INT(PlayerId))
				//	ENDIF
				ENDIF
			ELSE
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [UPDATE_KILL_LIST_LEADERBOARD] Setting score to -1 for player ", GET_PLAYER_NAME(PlayerId))
				FILL_PLAYER_LEADERBOARD_DATA(-1, -1,  NATIVE_TO_INT(PlayerId))
			ENDIF
		ELSE
			IF serverBD.sLeaderboardData[i].iKills <> -1
				CLEAR_PLAYER_LEADERBOARD_DATA(i)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [UPDATE_KILL_LIST_LEADERBOARD] Call CLEAR_PLAYER_LEADERBOARD_DATA with player = ", i, " as NI doesn't exist")
			ENDIF
		ENDIF
	ENDREPEAT
	
	SORT_LEADERBOARD_DATA_PLAYERS()
ENDPROC

#IF IS_DEBUG_BUILD
PROC OUTPUT_PLAYER_LB()
	INT i
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [OUTPUT_PLAYER_LB] ")
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [OUTPUT_PLAYER_LB] ")

	REPEAT NUM_NETWORK_PLAYERS i
		IF serverBD.sLeaderboardData[i].iPlayer > -1
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [OUTPUT_PLAYER_LB] serverBD.sLeaderboardData[",i,"].iTeam = ", serverBD.sLeaderboardData[i].iTeam, "  serverBD.sLeaderboardData[",i,"].iPlayer = ", serverBD.sLeaderboardData[i].iPlayer, " (", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(serverBD.sLeaderboardData[i].iPlayer)),") serverBD.sLeaderboardData[",i,"].iKills = ",serverBD.sLeaderboardData[i].iKills)	
		ELSE
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [OUTPUT_PLAYER_LB] serverBD.sLeaderboardData[",i,"].iTeam = ", serverBD.sLeaderboardData[i].iTeam, "  serverBD.sLeaderboardData[",i,"].iPlayer = ", serverBD.sLeaderboardData[i].iPlayer," serverBD.sLeaderboardData[",i,"].iKills = ",serverBD.sLeaderboardData[i].iKills)	
		ENDIF
	ENDREPEAT
ENDPROC

PROC OUTPUT_TEAM_LB()
	INT i
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [OUTPUT_TEAM_LB] ")
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [OUTPUT_TEAM_LB] ")

	REPEAT MAX_PLAYER_VEH i
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [OUTPUT_TEAM_LB] serverBD.sLeaderboardDataTeams[",i,"].iTeam = ", serverBD.sLeaderboardDataTeams[i].iTeam, "  serverBD.sLeaderboardDataTeams[",i,"].iPlayer = ", serverBD.sLeaderboardDataTeams[i].iPlayer, " serverBD.sLeaderboardDataTeams[",i,"].iKills = ",serverBD.sLeaderboardDataTeams[i].iKills)	
	ENDREPEAT
ENDPROC
#ENDIF //IS_DEBUG_BUILD


FUNC PLAYER_INDEX FIND_A_PLAYER_ON_TEAM(INT iTeam, BOOL bCheckInVeh = TRUE)
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [FIND_A_PLAYER_ON_TEAM] CALLED WITH iTeam = ", iTeam)
	PLAYER_INDEX playerTeam = INVALID_PLAYER_INDEX()
	PLAYER_INDEX playerCand
	PED_INDEX pedTeam
	INT iPlayerCount
	
	IF iTeam >= 0
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iPlayerCount
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayerCount))
				IF playerTeam = INVALID_PLAYER_INDEX()
					IF playerBD[iPlayerCount].eStage = eAD_ATTACK
						IF IS_BIT_SET(playerBD[iPlayerCount].iInVehicleBitSet, iTeam)
							IF NOT bCheckInVeh
								playerTeam = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPlayerCount))
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [FIND_A_PLAYER_ON_TEAM] THIS PLAYER ", GET_PLAYER_NAME(playerTeam), " IS ON TEAM ", iTeam)
							ELSE
								playerCand = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPlayerCount))
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [FIND_A_PLAYER_ON_TEAM] THIS PLAYER ", GET_PLAYER_NAME(playerCand), " IS ON TEAM ", iTeam, " WILL CHECK THEY ARE IN THEIR VEHICLE")
								IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[iTeam])
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [FIND_A_PLAYER_ON_TEAM] THIS PLAYER ", GET_PLAYER_NAME(playerCand), " VEHICLE IS OK")
									pedTeam = GET_PLAYER_PED(playerCand)
									IF NOT IS_PED_INJURED(pedTeam)
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [FIND_A_PLAYER_ON_TEAM] THIS PLAYER ", GET_PLAYER_NAME(playerCand), " IS NOT INJURED")
										IF IS_PED_IN_VEHICLE(pedTeam, NET_TO_VEH(serverBD.niVeh[iTeam]))
											playerTeam = playerCand
											CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [FIND_A_PLAYER_ON_TEAM] THIS PLAYER ", GET_PLAYER_NAME(playerCand), " IS IN VEHICLE. WILL USE PLAYER")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ELSE
		NET_SCRIPT_ASSERT("FIND_A_PLAYER_ON_TEAM iTeam >= 0 TELL DAVE W ")
	ENDIF
	
	
	RETURN playerTeam 
ENDFUNC 

PROC LOCK_ALL_VEHICLES_FOR_PLAYER(PLAYER_INDEX playerLock, BOOL bLock)
	INT i
	REPEAT MAX_PLAYER_VEH i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
			SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.niVeh[i]), playerLock, bLock)
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER [LOCK_ALL_VEHICLES_FOR_PLAYER] CALEED WITH bLock = ", bLock, " FOR VEH ", i, " FOR PLAYER ", GET_PLAYER_NAME(playerLock))	
		ENDIF
	ENDREPEAT
ENDPROC

PROC SERVER_MAINTAIN_LOCK_DOORS_FOR_PART(INT iPart, PLAYER_INDEX playerCurrent)
	//-- Lock doors
	BOOL bPlayerInVeh
	INT i
	
	IF NOT IS_BIT_SET(serverBD.iPlayerLockedOutBitset,iPart)
		IF IS_BIT_SET(playerBD[iPart].iPlayerBitSet, biP_NoConsiderVeh)	
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER LOCKING DOORS ON ALL PLAYER VEH FOR PLAYER ", GET_PLAYER_NAME(playerCurrent), " AS biP_NoConsiderVeh SET")
			LOCK_ALL_VEHICLES_FOR_PLAYER(playerCurrent, TRUE)
			SET_BIT(serverBD.iPlayerLockedOutBitset,iPart)
		ELIF serverBD.eStage >= eAD_ATTACK
			IF playerBD[iPart].iInVehicleBitSet = 0
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER LOCKING DOORS ON ALL PLAYER VEH FOR PLAYER ", GET_PLAYER_NAME(playerCurrent), " AS iInVehicleBitSet = 0")
				LOCK_ALL_VEHICLES_FOR_PLAYER(playerCurrent, TRUE)
				SET_BIT(serverBD.iPlayerLockedOutBitset,iPart)
			ELSE
				IF playerBD[iPart].eStage >= eAD_OFF_MISSION
					REPEAT MAX_PLAYER_VEH i
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
							IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[i])
								IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(playerCurrent), NET_TO_VEH(serverBD.niVeh[i]))
									bPlayerInVeh = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
					
					IF NOT bPlayerInVeh
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER LOCKING DOORS ON ALL PLAYER VEH FOR PLAYER  ", GET_PLAYER_NAME(playerCurrent), " AS eStage >= eAD_OFF_MISSION")
						LOCK_ALL_VEHICLES_FOR_PLAYER(playerCurrent, TRUE)
						SET_BIT(serverBD.iPlayerLockedOutBitset,iPart)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF serverBD.eStage < eAD_ATTACK	
			IF NOT IS_BIT_SET(playerBD[iPart].iPlayerBitSet, biP_NoConsiderVeh)		
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER UNLOCKING DOORS ON ALL PLAYER VEH  FOR PLAYER ", GET_PLAYER_NAME(playerCurrent), " AS biP_NoConsiderVeh NOT SET")
				LOCK_ALL_VEHICLES_FOR_PLAYER(playerCurrent, FALSE)
				CLEAR_BIT(serverBD.iPlayerLockedOutBitset,iPart)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PLAYER_VEHICLE_PART_CHECK_SERVER(INT iPart, INT iVeh, PLAYER_INDEX playerCurrent)
	PLAYER_INDEX playerTemp
	PED_INDEX pedTemp
	IF IS_BIT_SET(playerBD[iPart].iInVehicleBitSet, iVeh)
		iNumPlayersInVeh[iVeh]++
		IF iTempKillsPerVehicle[iVeh] < 0
			IF playerBD[iPart].iMyKills >= 0
				IF serverBD.iMissingKillsPerVehicle[iVeh] > 0
					//-- If a player gained some kills and then left, need to take into account their kills before summing remaining player's kills - see B* 2488539
					iTempKillsPerVehicle[iVeh] = serverBD.iMissingKillsPerVehicle[iVeh]
			//		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER [PLAYER_VEHICLE_PART_CHECK_SERVER] iTempKillsPerVehicle[", iVeh, "] init to iMissingKillsPerVehicle = ", serverBD.iMissingKillsPerVehicle[iVeh])
				ELSE
					iTempKillsPerVehicle[iVeh] = 0
			//		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER [PLAYER_VEHICLE_PART_CHECK_SERVER] iTempKillsPerVehicle[", iVeh, "] init to 0")
				ENDIF
			ENDIF
		ENDIF
		
		
		iTempKillsPerVehicle[iVeh] += playerBD[iPart].iMyKills
		

		IF serverBD.eStage >= eAD_ATTACK
			IF serverBD.playerDriving[iVeh] = INVALID_PLAYER_INDEX()
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[iVeh])
					IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[iVeh])
						pedTemp = GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(serverBD.niVeh[iVeh]))
						IF pedTemp <> NULL
							IF IS_PED_A_PLAYER(pedTemp) 
								playerTemp = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedTemp)
								IF playerTemp = playerCurrent
									serverBD.playerDriving[iVeh] = playerCurrent
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER serverBD.playerDriving[", iVeh, "] now this player ", GET_PLAYER_NAME(playerCurrent))
									UPDATE_KILL_LIST_LEADERBOARD()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF			
			ELSE
				
			ENDIF
			
			
		ENDIF

		
		IF NOT IS_BIT_SET(serverBD.iVehExplodeTooFewPlayers, iVeh)
			IF playerBD[iPart].iVehTooFewPlayers > -1
				SET_BIT(serverBD.iVehExplodeTooFewPlayers, iVeh)
				START_NET_TIMER(serverBD.timeExplodeTooFewPlayers[iVeh])
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER SETTING THIS PLAYER VEH ", iVeh, " SHOULD EXPLODE BECAUSE THIS PLAYER ", GET_PLAYER_NAME(playerCurrent), " SAYS THERE ARENT ENOUGH PLAYERS INSIDE")
			ENDIF
		ENDIF
	ENDIF
	
	IF serverBD.playerDriving[iVeh] <> INVALID_PLAYER_INDEX()
		IF NOT NETWORK_IS_PLAYER_ACTIVE(serverBD.playerDriving[iVeh])
			playerTemp = FIND_A_PLAYER_ON_TEAM(iVeh)
			IF playerTemp <> INVALID_PLAYER_INDEX()
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER serverBD.playerDriving[", iVeh, "] now ", GET_PLAYER_NAME(playerTemp), " AS PILOT OF TEAM ", iVeh, " IS NO LONGER ACTIVE")
				serverBD.playerDriving[iVeh] = playerTemp
			ELSE
				serverBD.playerDriving[iVeh] = INVALID_PLAYER_INDEX()
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER serverBD.playerDriving[", iVeh, "] now Invalid")
			ENDIF
			
		ELIF IS_PLAYER_SCTV(serverBD.playerDriving[iVeh])
			serverBD.playerDriving[iVeh] = INVALID_PLAYER_INDEX()
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER serverBD.playerDriving[", iVeh, "] now Invalid AS SCTV")
			
			UPDATE_KILL_LIST_LEADERBOARD()
		ELSE
			//-- Check ped is actually in the vehicle
			pedTemp = GET_PLAYER_PED(serverBD.playerDriving[iVeh])
			IF NOT IS_PED_INJURED(pedTemp)
				IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[iVeh])
					IF NOT IS_PED_IN_VEHICLE(pedTemp, NET_TO_VEH(serverBD.niVeh[iVeh]))
						playerTemp = FIND_A_PLAYER_ON_TEAM(iVeh)
						IF playerTemp <> INVALID_PLAYER_INDEX()
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER serverBD.playerDriving[", iVeh, "] now ", GET_PLAYER_NAME(playerTemp), " AS PREVIOUS TEAM MEMBER ON TEAM ", iVeh, " IS NO LONGER IN VEH")
							serverBD.playerDriving[iVeh] = playerTemp
						ELSE
							serverBD.playerDriving[iVeh] = INVALID_PLAYER_INDEX()
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER serverBD.playerDriving[", iVeh, "] now Invalid AS PREVIOUS NO LONGER IN VEHICLE")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	
ENDPROC

PROC ENEMY_VEHICLE_PART_CHECK_SERVER(INT iPart, INT iVeh)
	#IF IS_DEBUG_BUILD
	PLAYER_INDEX playerTemp
	#ENDIF
	IF NOT IS_BIT_SET(serverBD.iTargetVehDestroyedBitSet, iVeh) 
		IF IS_BIT_SET(playerBD[iPart].iPlayerDestVehBitSet, iVeh)
			SET_BIT(serverBD.iTargetVehDestroyedBitSet, iVeh)
			
			#IF IS_DEBUG_BUILD
			playerTemp = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart))
			#ENDIF
			IF playerBD[iPart].eStage < eAD_OFF_MISSION
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER think this player ", GET_PLAYER_NAME(playerTemp), " destroyed this veh ", iVeh)
			ELSE
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER think this player ", GET_PLAYER_NAME(playerTemp), " destroyed this veh ", iVeh, " but their stage is >= eAD_OFF_MISSION")
			ENDIF
			
			IF IS_BIT_SET(serverBD.iVehEmptyBitSet, iVeh) 
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [PROCESS_UW_DAMAGE_EVENT] ENEMY_VEHICLE_PART_CHECK_SERVER iVehEmptyBitSet WAS SET FOR VEH ",iVeh, " WILL CLEAN UP")
				
				IF NOT IS_BIT_SET(serverbd.iVehCleanedUpBitset, iVeh)
					serverbd.iVehiclesSpawned--
					
					IF GET_CURRENT_WAVE_TYPE() > WAVE_TYPE_NONE
						IF serverbd.iVehicleSpawnedThisWave > 0
							serverbd.iVehKillsThisWave++
							BROADCAST_KILL_LIST_VEHICLE_RESET(iVeh, ALL_PLAYERS_ON_SCRIPT())
						ENDIF
					ENDIF

						

					SET_BIT(serverbd.iVehCleanedUpBitset, iVeh)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [ENEMY_VEHICLE_PART_CHECK_SERVER] - MW VEH ", iVeh, " CLEANED UP - VEHICLE WAS EMPTY, SOMEBODY CLAIMED KILL Set bit ", iVeh, " serverbd.iVehiclesSpawned = ", serverbd.iVehiclesSpawned, " serverbd.iTotalKills = ", serverbd.iTotalKills, " Team 1 score = ", serverBD.iKillsPerVehicle[0], " Team 2 score = ",   serverBD.iKillsPerVehicle[1])
				ENDIF
				IF HAS_NET_TIMER_STARTED(serverBD.timeCleanupVeh[iVeh])
					RESET_NET_TIMER(serverBD.timeCleanupVeh[iVeh])
				ENDIF
						
				CLEANUP_NET_ID(serverBD.niMwVeh[iVeh])
				CLEAR_BIT(serverBD.iVehEmptyBitSet, iVeh)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC ENEMY_ON_FOOT_PED_CHECK_SERVER(INT iPart, INT iPed)
	#IF IS_DEBUG_BUILD
	PLAYER_INDEX playerTemp
	#ENDIF
	IF NOT IS_BIT_SET(serverBD.iServerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
		IF IS_BIT_SET(playerBD[iPart].iPlayerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed) )
			SET_BIT(serverBD.iServerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
			#IF IS_DEBUG_BUILD
			playerTemp = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart))
			#ENDIF
			IF playerBD[iPart].eStage < eAD_OFF_MISSION
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER think this player ", GET_PLAYER_NAME(playerTemp), " killed this on foot ped ", iPed)
			ELSE
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER think this player ", GET_PLAYER_NAME(playerTemp), " killed this on foot ped ", iPed, " but their stage is >= eAD_OFF_MISSION")
			ENDIF
			
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niMwPed[iPed])
				IF NOT IS_NET_PED_INJURED(serverBD.niMwPed[iPed])
					IF NOT SHOULD_CLEAN_UP_UW_PED_DUE_TO_PED_VEHICLE_STATE(iPed)
						CLEAR_BIT(serverBD.iServerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER think this player ", GET_PLAYER_NAME(playerTemp), " killed this on foot ped ", iPed, " PED ALREADY RECREATED!")
					ELSE
						IF NOT IS_PED_IN_ANY_VEHICLE(NET_TO_PED(serverBD.niMwPed[iPed]))
							IF iPed >= (MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH) // on foot ped
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER think this player ", GET_PLAYER_NAME(playerTemp), " killed this on foot ped ", iPed, " PED ALREADY RECREATED! (2)")
								CLEAR_BIT(serverBD.iServerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PROC CHECK_FOR_PILOT_TARGET_PLAYER_UPDATE(INT iPart, INT iPed)
//	INT iThisPliot
//	
//
//	#IF IS_DEBUG_BUILD
//		PLAYER_INDEX playerTemp
//		PLAYER_INDEX playerTarget
//	#ENDIF
//	
//	//IF iPed % 4 = 0
//		iThisPliot = iPed
//		IF iThisPliot < MAX_MW_VEHS
//			IF serverBD.iPedPlayerTargetServer[iThisPliot] = -1
//				IF playerBD[iPart].iPedPlayerTargetClient[iThisPliot] <> -1
//					serverBD.iPedPlayerTargetServer[iThisPliot] = playerBD[iPart].iPedPlayerTargetClient[iThisPliot]
//					
//					#IF IS_DEBUG_BUILD
//						playerTemp = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart))
//						playerTarget = INT_TO_PLAYERINDEX(playerBD[iPart].iPedPlayerTargetClient[iThisPliot])
//						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER ] [CHECK_FOR_PILOT_TARGET_PLAYER_UPDATE] serverBD.iPedPlayerTargetServer[", iThisPliot, "] updated to ", playerBD[iPart].iPedPlayerTargetClient[iThisPliot], " FROM PLAYER ", GET_PLAYER_NAME(playerTemp), " NEW TARGET ", GET_PLAYER_NAME(playerTarget))
//					#ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
////	ENDIF
//					
//ENDPROC





FUNC TEXT_LABEL_15 GET_TEXT_LABEL_FOR_UW_VEHICLE()
	TEXT_LABEL_15 tl15Name
	SWITCH serverBD.VehModel
		CASE RHINO		tl15Name = "UW_TMRHN"	BREAK
		CASE HYDRA		tl15Name = "UW_TMHYD"	BREAK
		CASE SAVAGE		tl15Name = "UW_TMSAV"	BREAK
		CASE VALKYRIE	tl15Name = "UW_TMVALK"	BREAK
		CASE BUZZARD	tl15Name = "UW_TMBUZ"	BREAK
	ENDSWITCH
	
	
	RETURN tl15Name
ENDFUNC

INT iTeamId[MAX_PLAYER_VEH]
INT iTeamMemberId[MAX_PLAYER_VEH]

FUNC INT GET_PLAYER_WITH_CRITERIA(INT iTeam, INT iIndexToLoopFrom)
	INT i
	INT iReturn
	iReturn = -1
	
//	PRINTLN("GET_PLAYER_WITH_CRITERIA, of iTeam = ", iTeam , ", started at: ", iIndexToLoopFrom, ", NETWORK_GET_NUM_PARTICIPANTS() = ", NETWORK_GET_NUM_PARTICIPANTS())
	
	FOR i = iIndexToLoopFrom TO ( NETWORK_GET_NUM_PARTICIPANTS() - 1 )
	
//		PRINTLN("GET_PLAYER_WITH_CRITERIA, sDpadDownLeaderBoardData[", i, "].iTeam  = ", sDpadDownLeaderBoardData[i].iTeam)
	
		IF iTeam = sDpadDownLeaderBoardData[i].iTeam //( sDpadDownLeaderBoardData[i].iTeam - 1 )
			iReturn = i
			i = NETWORK_GET_NUM_PARTICIPANTS()
		ENDIF
		
	ENDFOR
	
//	PRINTLN("GET_PLAYER_WITH_CRITERIA, of iReturn = ", iReturn, ", iTeam  = ", iTeam, ", started at: ", iIndexToLoopFrom)
	
	RETURN iReturn
ENDFUNC

PROC SWAP_PLAYERS(INT iIndex, INT i)

	IF iIndex = -1
	OR i = -1
		
		EXIT
	ENDIF

	THE_LEADERBOARD_STRUCT tempRow = sDpadDownLeaderBoardData[i]
	sDpadDownLeaderBoardData[i] = sDpadDownLeaderBoardData[iIndex]
	sDpadDownLeaderBoardData[iIndex] = tempRow
	
//	PRINTLN("SWAP_PLAYERS, of i = ", i, " iIndex = ", iIndex)
ENDPROC

PROC TEAM_SORT()

	INT iTeam
	INT iMember
	INT iIndex
	
	iTeamId[0] = serverBD.sLeaderboardDataTeams[0].iTeam
	iTeamId[1] = serverBD.sLeaderboardDataTeams[1].iTeam
	iTeamId[2] = serverBD.sLeaderboardDataTeams[2].iTeam
	iTeamId[3] = serverBD.sLeaderboardDataTeams[3].iTeam
	
	INT iWinningTeam			
	INT iSecondPlaceTeam		
	INT iThirdPlaceTeam			
	INT iFourthPlaceTeam	
	
	INT iWinningTeamCount		
	INT iSecondPlaceTeamCount 	
	INT iThirdPlaceTeamCount 	
	INT iFourthPlaceTeamCount
	
	iWinningTeam				= serverBD.sLeaderboardDataTeams[0].iTeam
	iSecondPlaceTeam			= serverBD.sLeaderboardDataTeams[1].iTeam
	iThirdPlaceTeam				= serverBD.sLeaderboardDataTeams[2].iTeam
	iFourthPlaceTeam			= serverBD.sLeaderboardDataTeams[3].iTeam
	
	IF iWinningTeam <> -1
		iWinningTeamCount		= serverBD.iNumPlayersInTeam[iWinningTeam] 
	ENDIF
	IF iSecondPlaceTeam <> -1
		iSecondPlaceTeamCount	= serverBD.iNumPlayersInTeam[iSecondPlaceTeam]
	ENDIF
	IF iThirdPlaceTeam <> -1
		iThirdPlaceTeamCount	= serverBD.iNumPlayersInTeam[iThirdPlaceTeam]
	ENDIF
	IF iFourthPlaceTeam <> -1
		iFourthPlaceTeamCount	= serverBD.iNumPlayersInTeam[iFourthPlaceTeam]
	ENDIF

	iTeamMemberId[0] = iWinningTeamCount	
	iTeamMemberId[1] = iSecondPlaceTeamCount
	iTeamMemberId[2] = iThirdPlaceTeamCount 
	iTeamMemberId[3] = iFourthPlaceTeamCount
	
//	PRINTLN("TEAM_SORT, iWinningTeamCount = ", iWinningTeamCount)
//	PRINTLN("TEAM_SORT, iSecondPlaceTeamCount = ", iSecondPlaceTeamCount)
//	PRINTLN("TEAM_SORT, iThirdPlaceTeamCount = ", iThirdPlaceTeamCount)
//	PRINTLN("TEAM_SORT, iFourthPlaceTeamCount = ", iFourthPlaceTeamCount)
//	PRINTLN("TEAM_SORT, iWinningTeam = ", iWinningTeam)
//	PRINTLN("TEAM_SORT, iSecondPlaceTeam = ", iSecondPlaceTeam)
//	PRINTLN("TEAM_SORT, iThirdPlaceTeam = ", iThirdPlaceTeam)
//	PRINTLN("TEAM_SORT, iFourthPlaceTeam = ", iFourthPlaceTeam)
	
	INT i
	REPEAT NETWORK_GET_NUM_PARTICIPANTS() i
		
		iIndex = GET_PLAYER_WITH_CRITERIA(iTeamId[iTeam], i)
		
		IF iIndex != -1
			// If the slot is valid, update our score with the team score
			sDpadDownLeaderBoardData[iIndex].iScore = serverBD.sLeaderboardDataTeams[iTeam].iKills
		ENDIF
		
		SWAP_PLAYERS(iIndex, i)
		
		iMember++
		
		IF iMember >= iTeamMemberId[iTeam]
		
//			PRINTLN("TEAM_SORT, looping to next team. [current] iTeam = ", iTeam, ", iMember = ", iMember)
		
			iTeam++
			iMember = 0
			
//			PRINTLN("TEAM_SORT, looping to next team. [new] iTeam = ", iTeam, ", iMember = ", iMember)
		ENDIF
		
		// If iTeam = MAX_PLAYER_VEH that's the end condition for the loop so bail
		IF iTeam >= MAX_PLAYER_VEH
			
			EXIT
		ENDIF
		
	ENDREPEAT
ENDPROC

PROC MAINTAIN_DPAD_DOWN_LEADERBOARD(BOOL bForce = FALSE)
	INT iCount
	TEXT_LABEL_15 tl15Name = GET_TEXT_LABEL_FOR_UW_VEHICLE()
	//TEXT_LABEL_15 tlTeam = "UW_TM"
	
	BOOL bListPlayers = FALSE

	IF serverBD.VehModel = RHINO
	OR serverBD.VehModel = HYDRA	
		bListPlayers = TRUE
	ENDIF
	
//	PRINTLN("[CS_DPAD] 2420576 MAINTAIN_DPAD_DOWN_LEADERBOARD, YES ")
		
	//////////////////////////////////////////////////

	IF bListPlayers
		REPEAT NUM_NETWORK_PLAYERS iCount
			sDpadDownLeaderBoardData[iCount].iParticipant = -1
			sDpadDownLeaderBoardData[iCount].playerID = INVALID_PLAYER_INDEX()
			sDpadDownLeaderBoardData[iCount].iScore = 0
			sDpadDownLeaderBoardData[iCount].iFinishTime = -1
		
			IF serverBD.sLeaderboardData[iCount].iKills > (-1)
				sDpadDownLeaderBoardData[iCount].playerID = INT_TO_NATIVE(PLAYER_INDEX, serverBD.sLeaderboardData[iCount].iPlayer)
				sDpadDownLeaderBoardData[iCount].iScore = serverBD.sLeaderboardData[iCount].iKills
			ENDIF
		ENDREPEAT
		
		IF bForce
			IF NOT SHOULD_FORCE_DRAW_CHALLENGE_DPAD_LBD()
				IF sDPadVars.iCachedEventType != FMMC_TYPE_KILL_LIST
					sDPadVars.iCachedEventType = FMMC_TYPE_KILL_LIST
				ELSE
					PRINTLN("2425383, MAINTAIN_DPAD_DOWN_LEADERBOARD, FORCE_DRAW_CHALLENGE_DPAD_LBD, CALL TO TRUE 1 ")
					FORCE_DRAW_CHALLENGE_DPAD_LBD(TRUE, FMMC_TYPE_KILL_LIST)
				ENDIF
			ENDIF
		ELSE
			IF sDPadVars.iCachedEventType != FMMC_TYPE_KILL_LIST
				sDPadVars.iCachedEventType = FMMC_TYPE_KILL_LIST
			ENDIF
		ENDIF
		
//		PRINTLN("[DM_LEADERBOARD] [2469220] - DRAW_CHALLENGE_DPAD_LBD 1 ")
		DRAW_CHALLENGE_DPAD_LBD(sDpadDownLeaderBoardData, scDpadDownLeaderBoardMovie, SUB_CHALLENGE, sDPadVars, sFmDPadLbData, DEFAULT)
		
		IF playerBD[PARTICIPANT_ID_TO_INT()].eStage	= eAD_ATTACK
			IF sDpadDownLeaderBoardData[0].playerID <> PLAYER_ID()
				IF serverBD.sLeaderboardData[0].iKills > 0
					SET_DO_FM_EVENT_FIRST_PLACE_HELP_IF_NEEDED()
				ENDIF
			ENDIF
		ENDIF
	ELSE
		// TEAM LEADERBOARD //////////////////////////////////////////
		
		REPEAT NUM_NETWORK_PLAYERS iCount
		
			//tlTeam = "UW_TM"
			sDpadDownLeaderBoardData[iCount].iParticipant = -1
			sDpadDownLeaderBoardData[iCount].playerID = INVALID_PLAYER_INDEX()
			sDpadDownLeaderBoardData[iCount].iScore = 0
			sDpadDownLeaderBoardData[iCount].iFinishTime = -1
			sDpadDownLeaderBoardData[iCount].iTl15TeamCount = -1
			
			// Sort
		//	IF iCount < MAX_PLAYER_VEH
				IF serverBD.sLeaderboardData[iCount].iKills > (-1)
				//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [MAINTAIN_DPAD_DOWN_LEADERBOARD] Checking iCount = ", iCount, " player = ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iCount)), " Their team = ", serverBD.sLeaderboardData[iCount].iTeam, " THEIR SCORE = ", serverBD.sLeaderboardData[iCount].iKills )		
					sDpadDownLeaderBoardData[iCount].iTl15TeamCount = (serverBD.sLeaderboardData[iCount].iTeam + 1)
					sDpadDownLeaderBoardData[iCount].playerID = INT_TO_PLAYERINDEX(serverBD.sLeaderboardData[iCount].iPlayer) // INT_TO_PLAYERINDEX(iCount)
					sDpadDownLeaderBoardData[iCount].iDpadVarInt = serverBD.sLeaderboardData[iCount].iKills
					sDpadDownLeaderBoardData[iCount].iScore = serverBD.sLeaderboardData[iCount].iKills
					sDpadDownLeaderBoardData[iCount].iTeam = serverBD.sLeaderboardData[iCount].iTeam 
					
				//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - [MAINTAIN_DPAD_DOWN_LEADERBOARD] playerID = ", GET_PLAYER_NAME(sDpadDownLeaderBoardData[iCount].playerID), " iCount = ", iCount, " tlTeam = ", tlTeam, " iScore = ", serverBD.sLeaderboardData[iCount].iKills)
				ENDIF
		//	ENDIF
			
		ENDREPEAT
		
		// Sorting the teams for the dpad team lbd
		TEAM_SORT()
		
		IF playerBD[PARTICIPANT_ID_TO_INT()].eStage	= eAD_ATTACK
			IF serverBD.sLeaderboardDataTeams[0].iTeam > 0
				IF serverBD.sLeaderboardDataTeams[0].iTeam <> playerBD[PARTICIPANT_ID_TO_INT()].iMyUwTeam
					IF serverBD.iKillsPerVehicle[serverBD.sLeaderboardDataTeams[0].iTeam] > 0
						SET_DO_FM_EVENT_FIRST_PLACE_HELP_IF_NEEDED()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		// Drawing the team lbd
//		PRINTLN("[DM_LEADERBOARD] [2469220] - DRAW_CHALLENGE_DPAD_LBD 2 ")
		DRAW_CHALLENGE_DPAD_LBD(sDpadDownLeaderBoardData, scDpadDownLeaderBoardMovie, SUB_CHALLENGE, sDPadVars, sFmDPadLbData, DEFAULT, TRUE)
		
		IF bForce
			IF NOT SHOULD_FORCE_DRAW_CHALLENGE_DPAD_LBD()
				IF sDPadVars.iCachedEventType != FMMC_TYPE_KILL_LIST
					sDPadVars.iCachedEventType = FMMC_TYPE_KILL_LIST
				ELSE
					PRINTLN("2425383, MAINTAIN_DPAD_DOWN_LEADERBOARD, FORCE_DRAW_CHALLENGE_DPAD_LBD, CALL TO TRUE 2 ")
					FORCE_DRAW_CHALLENGE_DPAD_LBD(TRUE, FMMC_TYPE_KILL_LIST)
				ENDIF
			ENDIF
		ELSE
			IF sDPadVars.iCachedEventType != FMMC_TYPE_KILL_LIST
				sDPadVars.iCachedEventType = FMMC_TYPE_KILL_LIST
			ENDIF
		ENDIF	
	ENDIF	
	
	UNUSED_PARAMETER(tl15Name)
ENDPROC

//PURPOSE: Runs through the max num participents and does relevant checks
PROC MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
	INT i, iTempPeakParts, iTempPeakQualifiers 
	MODEL_NAMES mVeh
	BOOL bUpdateLB
	BOOL bSomeoneNotFinished = FALSE
	
	REPEAT MAX_PLAYER_VEH i
		iNumPlayersInVeh[i] = 0
		iTempKillsPerVehicle[i] = -2
		bMinReqNumPlayersInVeh[i] = FALSE
	//	bVehHasDriver[i] = FALSE
	ENDREPEAT 
	
	
	INT iNumValidPlayersToLaunch
	i = 0
	
	//For now we only need to do this check once the Veh is destroyed.
	IF serverBD.iServerGameState != GAME_STATE_END
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iStaggeredParticipant
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iStaggeredParticipant))
				PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iStaggeredParticipant))
				//PED_INDEX PlayerPedId = GET_PLAYER_PED(PlayerId)
				
				IF NOT IS_PLAYER_SCTV(PlayerId)
					//** CHECKS THAT DON'T NEED THE PLAYER TO BE ALIVE **//
					IF playerBD[iStaggeredParticipant].iInVehicleBitSet <> 0 
						iTempPeakQualifiers++
					ENDIF
					iTempPeakParts++
					REPEAT MAX_PLAYER_VEH i
						PLAYER_VEHICLE_PART_CHECK_SERVER(iStaggeredParticipant, i, PlayerId)
					ENDREPEAT	
					
					i = 0
					
					REPEAT MAX_MW_VEHS i
						ENEMY_VEHICLE_PART_CHECK_SERVER(iStaggeredParticipant, i)
					ENDREPEAT
					
					
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_TooFewValid)
						IF NOT IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_HidingEvent)
							iNumValidPlayersToLaunch++
						ENDIF
					ENDIF
					
				//	FOR i =(MAX_MW_VEHS*MAX_MW_PEDS_PER_VEH) TO MAX_MW_PEDS -1
					FOR i = 0 TO MAX_MW_PEDS -1
						ENEMY_ON_FOOT_PED_CHECK_SERVER(iStaggeredParticipant, i)
						
						
					ENDFOR
					
					IF NOT bSomeoneNotFinished
						IF NOT IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_DoneReward)
							bSomeoneNotFinished = TRUE
							#IF IS_DEBUG_BUILD	
								IF bWdDebugMissionEnd
								OR serverBD.eStage >= eAD_EXPLODE_VEH
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ----------> KILL LIST MISSION_END_CHECK [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] - SERVER THINKS THIS PLAYER NOT FINISHED ", GET_PLAYER_NAME(PlayerId))
								ENDIF
							#ENDIF
						ENDIF
					ENDIF

					SERVER_MAINTAIN_LOCK_DOORS_FOR_PART(iStaggeredParticipant, PlayerId)
					
					//** CHECKS THAT REQUIRE THE PLAYER TO BE ALIVE **//
					IF IS_NET_PLAYER_OK(PlayerId)
						
						i = 0
						REPEAT MAX_PLAYERS i
							IF serverBD.piPlayers[i] = INVALID_PLAYER_INDEX()
								IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_Player1+i)
									serverBD.piPlayers[i] = PlayerId
									PRINTLN("     ---------->     KILL LIST - serverBD.piPlayers[", i, "] = ", GET_PLAYER_NAME(PlayerId))
								ENDIF
							ELIF serverBD.piPlayers[i] = PlayerId
							AND serverBD.eStage = eAD_IDLE
								IF NOT IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_Player1+i)
									serverBD.piPlayers[i] = INVALID_PLAYER_INDEX()
									PRINTLN("     ---------->     KILL LIST - serverBD.piPlayers[", i, "] = INVALID_PLAYER_INDEX")
								ENDIF
							ENDIF
						ENDREPEAT

					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	i = 0
	
	REPEAT MAX_PLAYER_VEH i
		IF NOT IS_BIT_SET(serverBD.iVehicleFullBitSet, i)
			IF iNumPlayersInVeh[i] = GET_NUMBER_OF_PLAYERS_REQUIRED_IN_UW_VEHICLE()
			AND DOES_UW_VEHICLE_HAVE_DRIVER(i)
				SET_BIT(serverBD.iVehicleFullBitSet, i)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER think this player veh is full ", i)
			ENDIF
			
			IF NOT IS_BIT_SET(serverBD.iAtLeastOnePlayerBitSet, i)
				IF iNumPlayersInVeh[i] >= 1
				AND DOES_UW_VEHICLE_HAVE_DRIVER(i)
					SET_BIT(serverBD.iAtLeastOnePlayerBitSet, i)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER Set iAtLeastOnePlayerBitSet for veh ", i)
				ENDIF
			ELSE
				IF iNumPlayersInVeh[i] = 0
				OR NOT DOES_UW_VEHICLE_HAVE_DRIVER(i)
					IF IS_BIT_SET(serverBD.iAtLeastOnePlayerBitSet, i)
						CLEAR_BIT(serverBD.iAtLeastOnePlayerBitSet, i)
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER Clear iAtLeastOnePlayerBitSet for veh ", i)
						IF iNumPlayersInVeh[i] = 0
							IF serverBD.eStage >= eAD_ATTACK
								IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
									CLEANUP_NET_ID(serverBD.niVeh[i])
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER Cleaning up player veh as empty veh ", i)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(serverBD.iAtLeastTwoPlayersBitSet, i)
			AND DOES_UW_VEHICLE_HAVE_DRIVER(i)
				IF iNumPlayersInVeh[i] >= 2
					SET_BIT(serverBD.iAtLeastTwoPlayersBitSet, i)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER Set iAtLeastTwoPlayersBitSet for veh ", i)
				ENDIF
			ELSE
				IF iNumPlayersInVeh[i] <= 1
				OR NOT DOES_UW_VEHICLE_HAVE_DRIVER(i)
					IF IS_BIT_SET(serverBD.iAtLeastTwoPlayersBitSet, i)
						CLEAR_BIT(serverBD.iAtLeastTwoPlayersBitSet, i)
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER Clear iAtLeastTwoPlayersBitSet for veh ", i)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF iNumPlayersInVeh[i] < GET_NUMBER_OF_PLAYERS_REQUIRED_IN_UW_VEHICLE()
			OR NOT DOES_UW_VEHICLE_HAVE_DRIVER(i)
				CLEAR_BIT(serverBD.iVehicleFullBitSet, i)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER clear iVehicleFullBitSet as no longer full veh ", i)
			ENDIF
		ENDIF
		
		IF serverBD.iNumPlayersInTeam[i] <> iNumPlayersInVeh[i]
			serverBD.iNumPlayersInTeam[i] = iNumPlayersInVeh[i]
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER serverBD.iNumPlayersInTeam[",i,"] UPDATED TO ", iNumPlayersInVeh[i])
			
			IF serverBD.eStage = eAD_ATTACK
				IF serverBD.iNumPlayersInTeam[i] = 0
					IF NOT IS_BIT_SET(serverBD.iLockVehicleDoorsDueToNoPlayers, i)
						SET_BIT(serverBD.iLockVehicleDoorsDueToNoPlayers, i)
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER SET serverBD.iLockVehicleDoorsDueToNoPlayers for veh ", i)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//-- Figure out if there's enough players in enough vehicles to start Vs Urban Warfare
		IF serverBD.eStage = eAD_IDLE
			IF IS_COMPETITIVE_UW()
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
					mVeh = GET_ENTITY_MODEL(NET_TO_VEH(serverBD.niVeh[i]))
					IF iNumPlayersInVeh[i] >= GET_MIN_NUMBER_OF_PLAYERS_REQUIRED_FOR_VALID_UW_VEHICLE(mVeh)
					AND DOES_UW_VEHICLE_HAVE_DRIVER(i)
						bMinReqNumPlayersInVeh[i] = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		
		INT iDiff
		IF serverBD.eStage = eAD_ATTACK
			
			
			IF serverBD.iKillsPerVehicle[i] <> iTempKillsPerVehicle[i]
				IF iTempKillsPerVehicle[i] >= 0
					IF iTempKillsPerVehicle[i] > serverBD.iKillsPerVehicle[i]
						serverBD.iKillsPerVehicle[i] = iTempKillsPerVehicle[i]
						bUpdateLB = TRUE
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER iKillsPerVehicle now ", serverBD.iKillsPerVehicle[i], " For player veh ", i, " serverbd.iTotalKills = ",serverbd.iTotalKills, " Team 1 score = ", serverBD.iKillsPerVehicle[0], " Team 2 score = ",   serverBD.iKillsPerVehicle[1], " Team 3 score = ",   serverBD.iKillsPerVehicle[2], " Team 4 score = ",   serverBD.iKillsPerVehicle[3])
					ELSE
						//-- Fewer kills for this vehicle compared to last update. One of the players in the vehicle who gained the kills probably quit the session - see B* 2488539.
						//-- Need to store these kills seperately, and take them into account next update.
						iDiff = serverBD.iKillsPerVehicle[i] - iTempKillsPerVehicle[i]
						IF serverBD.iMissingKillsPerVehicle[i] <> iDiff
							serverBD.iMissingKillsPerVehicle[i] = iDiff
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER iMissingKillsPerVehicle now ", serverBD.iMissingKillsPerVehicle[i], " For player veh ", i, " iKillsPerVehicle is ", serverBD.iKillsPerVehicle[i])
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT 

	IF bUpdateLB
		UPDATE_KILL_LIST_LEADERBOARD()
	ENDIF
	
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AllPlayersFinished)
		IF NOT bSomeoneNotFinished
			SET_BIT(serverBD.iServerBitSet, biS_AllPlayersFinished)
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SET biS_AllPlayersFinished") 
		ENDIF
	ENDIF
				
	i = 0
	INT iValidCount = 0
	//-- Figure out if there's enough players in enough vehicles to start Vs Urban Warfare
	IF serverBD.eStage = eAD_IDLE	
		IF IS_COMPETITIVE_UW()
			REPEAT MAX_PLAYER_VEH i
				IF bMinReqNumPlayersInVeh[i] iValidCount++	ENDIF
			ENDREPEAT
			
			
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_MinNumberOfPlayersInVehs)
				IF iValidCount >= 2
					SET_BIT(serverBD.iServerBitSet, biS_MinNumberOfPlayersInVehs)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SET biS_MinNumberOfPlayersInVehs as iValidCount = ", iValidCount)
				ENDIF
			ELSE
				IF iValidCount < 2
					CLEAR_BIT(serverBD.iServerBitSet, biS_MinNumberOfPlayersInVehs)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] CLEAR biS_MinNumberOfPlayersInVehs as iValidCount = ", iValidCount)
				
				ENDIF
			ENDIF
			
		//	PRINTLN("     ---------->     KILL LIST [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] iValidCount = ", iValidCount)
		ENDIF
	ENDIF
	
	
	//-- FIgure out if there's enough players left who haven't hidden the event
	IF serverBD.eStage = eAD_IDLE	
		IF NOT HAS_NET_TIMER_STARTED(serverBD.timeBeforeFailTooFewPlayers)
			START_NET_TIMER(serverBD.timeBeforeFailTooFewPlayers)
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] Started timeBeforeFailTooFewPlayers")
		ELSE
			IF HAS_NET_TIMER_EXPIRED(serverBD.timeBeforeFailTooFewPlayers, 5000)
				#IF IS_DEBUG_BUILD
				IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_AllowLowSessionNumbersFMEvents")
				#ENDIF
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_TooFewValid)
					IF IS_COMPETITIVE_UW()
						IF serverBD.VehModel = VALKYRIE
							IF iNumValidPlayersToLaunch < 4
								SET_BIT(serverBD.iServerBitSet, biS_TooFewValid)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] Set biS_TooFewValid as Competitive, Valkyrie, iNumValidPlayersToLaunch = ", iNumValidPlayersToLaunch)
							ENDIF
						ELSE
							IF iNumValidPlayersToLaunch < 2
								SET_BIT(serverBD.iServerBitSet, biS_TooFewValid)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] Set biS_TooFewValid as Competitive, iNumValidPlayersToLaunch = ", iNumValidPlayersToLaunch)
							ENDIF
						ENDIF
					ELSE 
						IF serverBD.VehModel = VALKYRIE
							IF iNumValidPlayersToLaunch < 2
								SET_BIT(serverBD.iServerBitSet, biS_TooFewValid)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] Set biS_TooFewValid as Non-Competitive, Valkyrie, iNumValidPlayersToLaunch = ", iNumValidPlayersToLaunch)
							ENDIF
						ELSE
							IF iNumValidPlayersToLaunch < 1
								SET_BIT(serverBD.iServerBitSet, biS_TooFewValid)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] Set biS_TooFewValid as Non-Competitive, iNumValidPlayersToLaunch = ", iNumValidPlayersToLaunch)
							ENDIF
						ENDIF
					ENDIF
//					UNUSED_PARAMETER(iNumValidPlayersToLaunch)
				ENDIF
				#IF IS_DEBUG_BUILD
				ENDIF
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF iTempPeakParts > serverBD.iPeakParticipants
		serverBD.iPeakParticipants = iTempPeakParts
		CPRINTLN(DEBUG_NET_AMBIENT, "     ---------->     KILL LIST [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] iTempPeakParts (",iTempPeakParts,") is GREATER than player's participating count so far (",serverBD.iPeakParticipants,"), updating value.")
	ELSE
		serverBD.iPlayersLeftInProgress = serverBD.iPeakParticipants - iTempPeakParts
	ENDIF
	
	IF iTempPeakQualifiers > serverBD.iPeakQualifiers
		serverBD.iPeakQualifiers = iTempPeakQualifiers
		CPRINTLN(DEBUG_NET_AMBIENT, "     ---------->     KILL LIST [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] iTempPeak (",iTempPeakQualifiers,") is GREATER than player's qualifying count so far (",serverBD.iPeakQualifiers,"), updating value.")
	ENDIF
ENDPROC


PROC SET_MY_UW_VEHICLE_INVINCIBLE(BOOL bSet)
	#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_TestSavage")
			EXIT
		ENDIF
	#ENDIF
	
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [SET_MY_UW_VEHICLE_INVINCIBLE] CALLED WITH bSet = ", bSet)
	VEHICLE_INDEX vehTemp
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [SET_MY_UW_VEHICLE_INVINCIBLE] CALLED 2 WITH bSet = ", bSet)
		vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF NETWORK_HAS_CONTROL_OF_ENTITY(vehTemp)
	//		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [SET_MY_UW_VEHICLE_INVINCIBLE] CALLED 3 WITH bSet = ", bSet)
			SET_ENTITY_INVINCIBLE(vehTemp, bSet)
			SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(vehTemp, !bset)
//			SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [SET_MY_UW_VEHICLE_INVINCIBLE] I'VE SET MY UW VEH INVINCIBLE bSet = ", bSet)
		ENDIF
	ELSE
	//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [SET_MY_UW_VEHICLE_INVINCIBLE] CALLED 4 WITH bSet = ", bSet)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_MISSION_START_TIMER_START()
	BOOL bTimerShouldStart
	
	IF NOT IS_COMPETITIVE_UW()
		IF (serverBD.VehModel = HYDRA)
		OR (serverBD.VehModel = RHINO)
			RETURN FALSE 
		ENDIF
	ELSE
		IF (serverBD.VehModel = HYDRA)
		OR (serverBD.VehModel = RHINO)
			IF GET_NUMBER_OF_UW_PLAYER_VEHICLES() = 2
				RETURN FALSE 
			ENDIF
		ENDIF
	ENDIF
	IF NOT IS_COMPETITIVE_UW()
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.niVeh[0])
			IF GET_ENTITY_MODEL(NET_TO_VEH(serverbd.niVeh[0])) = VALKYRIE
				bTimerShouldStart = (serverBD.iAtLeastTwoPlayersBitSet <> 0) //IS_BIT_SET(serverbd.iServerBitSet, biS_AtLeastTwoPlayersInVeh)
			ELSE
				bTimerShouldStart = (serverBD.iAtLeastOnePlayerBitSet <> 0) // IS_BIT_SET(serverbd.iServerBitSet, biS_AtLeastOnePlayerInVeh)
			ENDIF
		ENDIF
	ELSE
		bTimerShouldStart = IS_BIT_SET(serverBD.iServerBitSet, biS_MinNumberOfPlayersInVehs)
	ENDIF
	
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
		IF bTimerShouldStart
		AND NOT SHOULD_UI_BE_HIDDEN()
			IF IS_COMPETITIVE_UW()
				IF NOT Is_This_The_Current_Objective_Text("UW_ABTSC")
					print_objective_text("UW_ABTSC") // Kill List Competitive is about to start
				ENDIF
			ELSE
				IF NOT Is_This_The_Current_Objective_Text("UW_ABTS")
					print_objective_text("UW_ABTS") // Kill List  is about to start
				ENDIF
			ENDIF
		ELSE
			IF Is_This_The_Current_Objective_Text("UW_ABTSC")
			OR Is_This_The_Current_Objective_Text("UW_ABTS")
				Clear_Any_Objective_Text_From_This_Script()
			ENDIF
		ENDIF
	ENDIF
	RETURN bTimerShouldStart
ENDFUNC

PROC DISABLE_PLAYER_MISSION_VEHICLE_CONTROLS_THIS_FRAME(BOOL bDisableExit = TRUE)
	
	//PRINTLN("DISABLE_PLAYER_MISSION_VEHICLE_CONTROLS_THIS_FRAME")
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HANDBRAKE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_UD)
	IF bDisableExit
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
	ENDIF
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SPECIAL)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_DROP_PROJECTILE)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ROLL_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_PITCH_UD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_YAW_LEFT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_YAW_LEFT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_UP)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_DOWN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_UNDERCARRIAGE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_NEXT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_PREV_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_TARGET_LEFT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_TARGET_RIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_VERTICAL_FLIGHT_MODE)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CONTEXT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
	
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_DOWN)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_UP)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_LEFT)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_RIGHT)
	
//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - DISABLED......")
ENDPROC

PROC STOP_PLAYER_LEAVING_VEHICLE_THIS_FRAME()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
	ENDIF
ENDPROC


FUNC BOOL IS_PLAYER_IN_ANY_UW_VEHICLE()
	INT i
	
	IF serverBD.iNumPlayerVeh = 1
	OR NOT IS_COMPETITIVE_UW()	
		IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh[0]))
			RETURN TRUE
		ENDIF
	ENDIF
	
	REPEAT MAX_PLAYER_VEH i
		IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh[i]))
			RETURN TRUE
		ENDIF
			
	ENDREPEAT
	RETURN FALSE
ENDFUNC


//
//PROC SET_ALL_UW_VEHICLES_LOCKED(BOOL bLocked)
//	INT i
//	PRINTLN("     ---------->     KILL LIST - SET_ALL_UW_VEHICLES_LOCKED() CALLED bLocked = ", bLocked)
//	REPEAT MAX_PLAYER_VEH i
//		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
//			IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[i])
//				//SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(NET_TO_VEH(serverBD.niVeh[i]), bConsider)
//				SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.niVeh[i]), PLAYER_ID(), bLocked)
//				PRINTLN("     ---------->     KILL LIST - SET_ALL_UW_VEHICLES_LOCKED() CALLED, LOCK VEH ", i, " bLocked = ", bLocked)
//			ENDIF
//		ENDIF
//	ENDREPEAT	
//ENDPROC

//PURPOSE: Checks for the Veh (destroyed, stuck, etc)
PROC CONTROL_VEH_BODY(INT iPlayerVeh)
	INT iMySeat
	BOOL bDisableExit
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niVeh[iPlayerVeh])
		IF NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.niVeh[iPlayerVeh]))
		//	PRINTLN("     ---------->     KILL LIST -  CONTROL_VEH_BODY - NOT IS_VEHICLE_DRIVEABLE")
			IF serverBD.eStage = eAD_IDLE
	//			PRINTLN("     ---------->     KILL LIST -  CONTROL_VEH_BODY REMOVING VEHICLE BLIP FOR PLAYER VEH ", iPlayerVeh)
	//			REMOVE_VEHICLE_BLIP()
			ENDIF
		ENDIF
//		IF IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh[iPlayerVeh]))
//			PRINTLN("     ---------->     KILL LIST -  CONTROL_VEH_BODY - IS_ENTITY_DEAD")
//		ENDIF
//		CPRINTLN(DEBUG_NET_AMBIENT, "---------->     KILL LIST - UW  - CONTROL_VEH_BODY")
		IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh[iPlayerVeh]))
			//Got in Veh
			IF NOT SHOULD_UI_BE_HIDDEN()
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet, iPlayerVeh)
					//Driver
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_UsingKLVeh)
							IF GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()) = NET_TO_VEH(serverBD.niVeh[iPlayerVeh])
								SET_BIT(iBoolsBitSet2, bi2_UsingKLVeh)
							//	PRINTLN("     ---------->     KILL LIST - SET bi2_UsingKLVeh AS PLAYER USING VEH ", iPlayerVeh)
							ENDIF
						ENDIF
					//	IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh[iPlayerVeh]))
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh[iPlayerVeh]))
							IF IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED("UW_HELP2", GET_VEH_BLIP_STRING())
							OR IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED("UW_HELP2C", GET_VEH_BLIP_STRING())
								CLEAR_HELP()
								PRINTLN("     ---------->     KILL LIST -  CONTROL_VEH_BODY CLEAR ENTER VEHICLE HELP")
							ENDIF
							
							IF NOT FM_EVENT_IS_PLAYER_WAITING_FOR_FM_EVENT_TO_START(PLAYER_ID())
								FM_EVENT_SET_PLAYER_WAITING_FOR_FM_EVENT_TO_START(TRUE)
							ENDIF
							iMySeat = ENUM_TO_INT(GET_SEAT_PED_IS_IN(PLAYER_PED_ID()))
							
							IF IS_THIS_HELP_MESSAGE_WITH_STRING_AND_NUMBER_BEING_DISPLAYED("UW_HELP1", GET_VEH_BLIP_STRING(), GET_CASH_REWARD(TRUE))
							OR IS_THIS_HELP_MESSAGE_WITH_STRING_AND_NUMBER_BEING_DISPLAYED("UW_HELP1C", GET_VEH_BLIP_STRING(), GET_CASH_REWARD(TRUE))
								CLEAR_HELP()
							ENDIF
							
								
							IF serverbd.iMinPlayers <> -1
								IF iMySeat > (serverbd.iMinPlayers - 2 )
									iMySeat = (serverbd.iMinPlayers - 2)
									IF iMySeat < -1
										iMySeat = -1	
										PRINTLN("     ---------->     KILL LIST -  CONTROL_VEH_BODY - RESETTING iMySeat = ", iMySeat)
									ENDIF
									PRINTLN("     ---------->     KILL LIST -  CONTROL_VEH_BODY - Overriding number of players, iMySeat = ", iMySeat, " serverbd.iMinPlayers = ", serverbd.iMinPlayers)
								ENDIF
							ENDIF
							
							
							
							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Player1+(iMySeat+1))
						//	SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InVehicle)
							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet, iPlayerVeh)
							playerBD[PARTICIPANT_ID_TO_INT()].iMyUwTeam = iPlayerVeh
							SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PlayersDontDragMeOutOfCar, TRUE)
							
							REMOVE_VEHICLE_BLIP()
							
							iMyUWVehSeat = iMySeat
							
							
							
							PRINTLN("     ---------->     KILL LIST - PCF_PlayersDontDragMeOutOfCar SET")

							PRINTLN("     ---------->     KILL LIST - biP_Player1 + ", (iMySeat+1), " SET, iMySeat = ", iMySeat, " SET BIT ", (iMySeat+1))
						//	PRINTLN("     ---------->     KILL LIST - biP_InVehicle SET")
						ELSE
							//-- Help text about getting in the vehicle
							IF serverBD.eStage = eAD_IDLE
								IF NOT IS_BIT_SET(iBoolsBitSet, biHelp2Done)
									//IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
										IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
											IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niVeh[iPlayerVeh]))) < 2500
												IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
													IF NOT IS_COMPETITIVE_UW() 
														PRINT_HELP_WITH_STRING_NO_SOUND("UW_HELP2", GET_VEH_BLIP_STRING(), DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) //Enter the vehicle ~HUD_COLOUR_NET_PLAYER2~~a~ ~s~to start Urban Warfare.
													ELSE
														PRINT_HELP_WITH_STRING_NO_SOUND("UW_HELP2C", GET_VEH_BLIP_STRING(), DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
													ENDIF
													SET_FREEMODE_EVENT_HELP_BACKGROUND()
													SET_BIT(iBoolsBitSet, biHelp2Done)
													PRINTLN("     ---------->     KILL LIST - SET biHelp2Done ")
												ELSE
													PRINTLN("     ---------->     KILL LIST - IS_OK_TO_PRINT_FREEMODE_HELP NOT OK 1 ")
												ENDIF
											ENDIF
										ENDIF
									//ENDIF
								ENDIF
								
								IF NOT IS_MP_PASSIVE_MODE_ENABLED()
									IF IS_BIT_SET(iBoolsBitSet2, bi2_PassiveModeHelp)
										CLEAR_BIT(iBoolsBitSet2, bi2_PassiveModeHelp)
									ENDIF
									IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
										IF NOT IS_BIT_SET(iBoolsBitSet, biWantedLevel)
											IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_NoConsiderVeh) 
											//	SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(NET_TO_VEH(serverBD.niVeh[iPlayerVeh]), FALSE)
									//			SET_ALL_UW_VEHICLES_LOCKED(TRUE)
												SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_NoConsiderVeh)
												PRINTLN("     ---------->     KILL LIST - I'm Not considering vehicle anymore - wanted level")
											ENDIF
										
											IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
												IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niVeh[iPlayerVeh]))) < 2500
													IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
														IF NOT IS_COMPETITIVE_UW()
															PRINT_HELP_NO_SOUND("UW_COPS", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) // Lose the Cops before starting Urban Warfare.
														ELSE
															PRINT_HELP_NO_SOUND("UW_COPSC", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
														ENDIF
														SET_FREEMODE_EVENT_HELP_BACKGROUND()
														SET_BIT(iBoolsBitSet, biWantedLevel)
													ELSE
														PRINTLN("     ---------->     KILL LIST - IS_OK_TO_PRINT_FREEMODE_HELP NOT OK 2 ")
													ENDIF
												ENDIF
											ENDIF
										ELSE
											IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niVeh[iPlayerVeh]))) > 10000
												CLEAR_BIT(iBoolsBitSet, biWantedLevel)
												PRINTLN("     ---------->     KILL LIST - Cleared biWantedLevel - far away ")
											ENDIF
										ENDIF
									ELSE
										IF IS_BIT_SET(iBoolsBitSet, biWantedLevel)
											CLEAR_BIT(iBoolsBitSet, biWantedLevel)
											PRINTLN("     ---------->     KILL LIST - Cleared biWantedLevel - no wanted level ")
										ENDIF
										
										IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_NoConsiderVeh) 
											//SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(NET_TO_VEH(serverBD.niVeh[iPlayerVeh]), TRUE)
										//	SET_ALL_UW_VEHICLES_LOCKED(FALSE)
											CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_NoConsiderVeh)
											PRINTLN("     ---------->     KILL LIST - Cleared biP_NoConsiderVeh - no wanted level iPlayerVeh = ",iPlayerVeh)
										ENDIF
									ENDIF
								ELSE
									IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_NoConsiderVeh)
									//	SET_ALL_UW_VEHICLES_LOCKED(TRUE)
										SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_NoConsiderVeh)
										PRINTLN("     ---------->     KILL LIST - I'm Not considering vehicle anymore - PASSIVE")
									ENDIF
									
									IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_PassiveModeHelp)
										IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niVeh[iPlayerVeh]))) < 2500
											IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
												IF NOT IS_COMPETITIVE_UW()
													PRINT_HELP_NO_SOUND("UW_PASSMD", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) // Lose the Cops before starting Urban Warfare.
												ELSE
													PRINT_HELP_NO_SOUND("UW_PASSMD", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) // You can't participate in this event while Passive Mode is enabled.
												ENDIF
												SET_FREEMODE_EVENT_HELP_BACKGROUND()
												SET_BIT(iBoolsBitSet2, bi2_PassiveModeHelp)
											ELSE
												PRINTLN("     ---------->     KILL LIST - IS_OK_TO_PRINT_FREEMODE_HELP NOT OK 3 ")
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
							ENDIF
							
//							IF serverBD.eStage = eAD_ATTACK
//								IF NOT IS_BIT_SET(iBoolsBitSet, biP_NoConsiderVeh) 
//									SET_ALL_UW_VEHICLES_LOCKED(TRUE)
//									SET_BIT(iBoolsBitSet, biP_NoConsiderVeh) 
//									PRINTLN("     ---------->     KILL LIST - I'm Not considering vehicle anymore - event started")
//								ENDIF
//								
//								
//							ENDIF
							
						ENDIF
					ENDIF
				ELSE
					//Exit before start
					IF playerBD[PARTICIPANT_ID_TO_INT()].eStage	= eAD_IDLE
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							//Driver
						//	IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh[iPlayerVeh]))
							IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh[iPlayerVeh]))
								PRINTLN("     ---------->     KILL LIST - Trying to clear - Player no longer in veh!")
								INT i
								REPEAT serverBD.iVehicleSeats i
									//PRINTLN("     ---------->     KILL LIST - Trying to clear - Checking seats bits, i = ", i, " serverBD.iVehicleSeats = ", serverBD.iVehicleSeats)
									IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Player1+i)
								//		PRINTLN("     ---------->     KILL LIST - Trying to clear - Bit is set! i = ", i)
										Clear_Any_Objective_Text_From_This_Script()
										CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Player1+i)
										PRINTLN("     ---------->     KILL LIST - biP_Player1 + ", i, " CLEARED")
									ELSE
									//	PRINTLN("     ---------->     KILL LIST - Trying to clear - Bit NOT  set! i = ", i)
									ENDIF
								ENDREPEAT
								
		//						IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InVehicle)
		//							CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InVehicle)
		//							PRINTLN("     ---------->     KILL LIST - CLEARED biP_InVehicle  - Player no longer in veh!")
		//						ENDIF
								
								IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet, iPlayerVeh)
									CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet, iPlayerVeh)
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST - CONTROL_VEH_BODY Cleared iInVehicleBitSet for veh ",iPlayerVeh)
								ENDIF
								
								IF playerBD[PARTICIPANT_ID_TO_INT()].iMyUwTeam <> -1
									playerBD[PARTICIPANT_ID_TO_INT()].iMyUwTeam = -1
								ENDIF
								IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("UW_TIMEL")
									CLEAR_HELP()
									PRINTLN("     ---------->     KILL LIST - CLEARED TIMER HELP")
								ENDIF
								
								IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("UW_TIMELA")
									CLEAR_HELP()
									PRINTLN("     ---------->     KILL LIST - CLEARED TIMER HELP")
								ENDIF
								
								IF FM_EVENT_IS_PLAYER_WAITING_FOR_FM_EVENT_TO_START(PLAYER_ID())
									FM_EVENT_SET_PLAYER_WAITING_FOR_FM_EVENT_TO_START(FALSE)
								ENDIF
								SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PlayersDontDragMeOutOfCar, FALSE)
								PRINTLN("     ---------->     KILL LIST - PCF_PlayersDontDragMeOutOfCar CLEARED 1")
							ELSE
								IF serverBD.eStage = eAD_IDLE
								AND NOT IS_BIT_SET(serverbd.iServerBitSet, biS_MissionStartExpired)
									iMySeat = ENUM_TO_INT(GET_SEAT_PED_IS_IN(PLAYER_PED_ID()))
									
									IF serverbd.iMinPlayers <> -1
										IF iMySeat > (serverbd.iMinPlayers - 2 )
											iMySeat = (serverbd.iMinPlayers - 2)
										//	PRINTLN("     ---------->     KILL LIST -  CONTROL_VEH_BODY - Overriding number of players, iMySeat = ", iMySeat, " serverbd.iMinPlayers = ", serverbd.iMinPlayers)
										ENDIF
									ENDIF
								
									IF iMySeat <> iMyUWVehSeat
										CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet, iPlayerVeh)
										CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Player1+(iMyUWVehSeat+1))
										PRINTLN("     ---------->     KILL LIST - CLEAR iPlayerVeh BIT ", iPlayerVeh, " AS iMySeat = ", iMySeat, " <> iMyUWVehSeat = ", iMyUWVehSeat, " CLEARED BIT ", (iMyUWVehSeat+1)) 
									ENDIF
								ELSE
									PRINTLN("     ---------->     KILL LIST - NOT CHECKING 	biP_Player1 BITS AS ABOU TO START!")
								ENDIF
								bDisableExit = FALSE
								
								IF SHOULD_MISSION_START_TIMER_START()

									IF serverbd.iStartTimeLimit > -1

										INT iTimeDiff = (serverbd.iStartTimeLimit-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeMissionStart))
										IF iTimeDiff < 2000
											bDisableExit = TRUE
										ENDIF
									ENDIF
								ENDIF
								
								DISABLE_PLAYER_MISSION_VEHICLE_CONTROLS_THIS_FRAME(bDisableExit)
								//Only disable the turret if it's the rino
								IF GET_ENTITY_MODEL(NET_TO_VEH(serverBD.niVeh[iPlayerVeh])) = RHINO
									DISABLE_VEHICLE_TURRET_MOVEMENT_THIS_FRAME(NET_TO_VEH(serverBD.niVeh[iPlayerVeh]))
								ENDIF
								//SET_VEHICLE_TURRET_SPEED_THIS_FRAME(NET_TO_VEH(serverBD.niVeh[iPlayerVeh]), 0.0)
								//SET_VEHICLE_TANK_TURRET_POSITION(NET_TO_VEH(serverBD.niVeh[iPlayerVeh]), 0.0)
								IF NOT HAS_NET_TIMER_STARTED(timeInVehStart)
									START_NET_TIMER(timeInVehStart)
								ELIF HAS_NET_TIMER_EXPIRED(timeInVehStart, 2000)
								
									IF IS_BIT_SET(serverBD.iServerBitSet, biS_MinNumberOfPlayersInVehs)
									OR NOT IS_COMPETITIVE_UW()
										IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("UW_MINV")
										//	CLEAR_HELP()
										ENDIF
										//-- Help text for event start
										IF NOT IS_BIT_SET(iBoolsBitSet, biTimeHelpDOne)
											IF serverBD.iVehicleSeats > 1
											OR (serverBD.iVehicleSeats = 1 AND GET_NUMBER_OF_UW_PLAYER_VEHICLES() > 1)
												IF HAS_NET_TIMER_STARTED(serverbd.timeMissionStart)
													IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
														IF GET_NUMBER_OF_UW_PLAYER_VEHICLES() > 1
															PRINT_HELP_NO_SOUND("UW_TIMELA", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) // Urban Warfare will start when all the vehicles are full or when the time expires.
														ELSE
															PRINT_HELP_NO_SOUND("UW_TIMEL", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) // Urban Warfare will start when the vehicle is full or when the time expires.
														ENDIF
														SET_FREEMODE_EVENT_HELP_BACKGROUND()
														SET_BIT(iBoolsBitSet, biTimeHelpDOne)
														PRINTLN("     ---------->     KILL LIST - SET biTimeHelpDOne")
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ELSE
										//-- Help text regarding having at least 2 vehicles with players in them before Vs Urban Warfare will start
										IF IS_COMPETITIVE_UW()
											IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_MinNumberOfPlayersInVehs)
												IF GET_ENTITY_MODEL(NET_TO_VEH(serverBD.niVeh[iPlayerVeh])) <> VALKYRIE
													IF NOT IS_BIT_SET(iBoolsBitSet, biMinPlayerVeh)
														IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
															PRINT_HELP_NO_SOUND("UW_MINV", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) // Players are required in at least two Urban Warfare vehicles before the mode will start.
															SET_FREEMODE_EVENT_HELP_BACKGROUND()
															SET_BIT(iBoolsBitSet, biMinPlayerVeh)
															PRINTLN("     ---------->     KILL LIST - SET biMinPlayerVeh")
														ENDIF
													ENDIF
												ENDIf
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
					//	PRINTLN("     ---------->     KILL LIST - Trying to clear - Player stage not idle!")
		
					ENDIF
					
					
					//-- Valkyrie help text explaining that > 1 players are required
					IF serverBD.eStage = eAD_IDLE
						IF GET_ENTITY_MODEL(NET_TO_VEH(serverBD.niVeh[iPlayerVeh])) = VALKYRIE
							
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("UW_VALK")
							AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("UW_VALKC")
								CLEAR_ALL_HELP_MESSAGES()
								IF IS_BIT_SET(iBoolsBitSet, biDoneValkHelp)
									CLEAR_BIT(iBoolsBitSet, biDoneValkHelp)
								ENDIF
							ENDIF
						
							
							IF NOT IS_BIT_SET(iBoolsBitSet, biDoneValkHelp)
								IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
									INT iMaxSeats
									INT iSeat
									INT iPedCount = 0
									INT i
									iMaxSeats = (GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(NET_TO_VEH(serverBD.niVeh[iPlayerVeh])) + 1) 
									REPEAT iMaxSeats iSeat
										IF NOT IS_VEHICLE_SEAT_FREE(NET_TO_VEH(serverBD.niVeh[iPlayerVeh]), INT_TO_ENUM(VEHICLE_SEAT, iSeat-1))
											iPedCount++
										ENDIF
									ENDREPEAT
									
									IF iPedCount = 1
										IF NOT IS_COMPETITIVE_UW()
											PRINT_HELP_NO_SOUND("UW_VALK", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) // At least two players are required to start Urban Warfare in a Valkyrie.
										ELSE
											PRINT_HELP_NO_SOUND("UW_VALKC", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) // At least two players are required in at least two Valkyrie helicopters to start Urban Warfare.
										ENDIF
										SET_FREEMODE_EVENT_HELP_BACKGROUND()
										SET_BIT(iBoolsBitSet, biDoneValkHelp)
										PRINTLN("     ---------->     KILL LIST - SET biDoneValkHelp")
									ENDIF
									
									IF NOT IS_BIT_SET(iBoolsBitSet, biDoneValkHelp)
										//-- Figure out if there's at least one more Valkyrie with at least 2 players in it
										IF IS_COMPETITIVE_UW()
											i = 0
											BOOL bFoundAnotherValidHeli
											REPEAT MAX_PLAYER_VEH i
												IF NOT bFoundAnotherValidHeli
													IF i <> iPlayerVeh
														iSeat = 0
														iPedCount = 0
														IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
															IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[i])
																REPEAT iMaxSeats iSeat
																	IF NOT IS_VEHICLE_SEAT_FREE(NET_TO_VEH(serverBD.niVeh[i]), INT_TO_ENUM(VEHICLE_SEAT, iSeat-1))
																		iPedCount++
																	ENDIF
																ENDREPEAT
																
																IF iPedCount > 1
																	bFoundAnotherValidHeli = TRUE // Found another Valkrie with at least 2 players
																ENDIF
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDREPEAT
											
											IF NOT bFoundAnotherValidHeli 
												PRINT_HELP_NO_SOUND("UW_VALKC", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)//  At least two players are required in at least two Valkyrie helicopters to start Urban Warfare.
												SET_FREEMODE_EVENT_HELP_BACKGROUND()
												SET_BIT(iBoolsBitSet, biDoneValkHelp)
												PRINTLN("     ---------->     KILL LIST - SET biDoneValkHelp 2") 
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF		
						ENDIF
					ENDIF
					
					//-- Help text explaining disabled vehicle exit
					IF serverBD.eStage = eAD_ATTACK
					//	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InVehicle) 
						IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet, iPlayerVeh)
							IF NOT IS_BIT_SET(iBoolsBitSet, biExitVehHelpDone)
								IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[iPlayerVeh])
									IF NOT IS_PED_INJURED(PLAYER_PED_ID())
										IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh[iPlayerVeh]))
											IF NOT IS_BIT_SET(iBoolsBitSet, biSHouldDoExitVehHelp)
												IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
												OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
												OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
													SET_BIT(iBoolsBitSet, biSHouldDoExitVehHelp)
													PRINTLN("     ---------->     KILL LIST - SET biSHouldDoExitVehHelp")
												ENDIF
											
											ENDIF
											
											IF IS_BIT_SET(iBoolsBitSet,biSHouldDoExitVehHelp)
												IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
													IF IS_COMPETITIVE_UW()
														PRINT_HELP_NO_SOUND("UW_EXITVC", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) // You cannot exit the vehicle while Competitive Urban Warfare is active.
													ELSE
														PRINT_HELP_NO_SOUND("UW_EXITV", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) // You cannot exit the vehicle while Urban Warfare is active.
													ENDIF
													SET_FREEMODE_EVENT_HELP_BACKGROUND()
													SET_BIT(iBoolsBitSet, biExitVehHelpDone)
													PRINTLN("     ---------->     KILL LIST - SET biExitVehHelpDone")
												ENDIF
											ENDIF

										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					
//					IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerVehNoDriverBitSet, iPlayerVeh)
//						IF NOT DOES_UW_VEHICLE_HAVE_DRIVER(iPlayerVeh)
//							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerVehNoDriverBitSet, iPlayerVeh)
//							PRINTLN("     ---------->     KILL LIST SET iPlayerVehNoDriverBitSet FOR VEH ", iPlayerVeh)
//						ENDIF
//					ELSE
//						IF DOES_UW_VEHICLE_HAVE_DRIVER(iPlayerVeh)
//							CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerVehNoDriverBitSet, iPlayerVeh)
//							PRINTLN("     ---------->     KILL LIST CLEAR iPlayerVehNoDriverBitSet FOR VEH ", iPlayerVeh)
//						ENDIF
//					ENDIF
				ENDIF
			ELSE
				CPRINTLN(DEBUG_NET_AMBIENT, "---------->     KILL LIST - UW  - CONTROL_VEH_BODY 2")
				
				INT iCharStat, iCount
				BOOL bPass
				REPEAT MAX_PLAYER_VEH iCount
					IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niVeh[iCount])
						IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.niVeh[iCount]))
							IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh[iCount]))
								bPass = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
				
				IF bPass	
					CPRINTLN(DEBUG_NET_AMBIENT, "---------->     KILL LIST - UW  - CONTROL_VEH_BODY IS_PED_IN_VEHICLE")
					IF SHOULD_HIDE_AMBIENT_EVENT(ciPI_HIDE_MENU_ITEM_AMBIENT_EVENTS_KILL_LIST)
						iCharStat = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_PIM_HIDE_AMBIENT)
					 	CLEAR_BIT(iCharStat, ciPI_HIDE_MENU_ITEM_AMBIENT_EVENTS_KILL_LIST)	
						SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_PIM_HIDE_AMBIENT, iCharStat)
						KILL_INTERACTION_MENU()
						IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HidingEvent)
							CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HidingEvent)
							PRINTLN("     ---------->     KILL LIST [] CLEAR biP_HidingEvent  <---------- ")
						ENDIF
						CPRINTLN(DEBUG_NET_AMBIENT, "---------->     KILL LIST - UW  - Reset Hide Current")
					ENDIF
				ELSE 
					 //-- CHose to ignore event
					IF playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet <> 0
						playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet = 0 
						PRINTLN("     ---------->     KILL LIST - playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet = 0  as UI SHOULD BE HIDDEN")
					ENDIF
					IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_NoConsiderVeh)
					//	SET_ALL_UW_VEHICLES_LOCKED(TRUE)
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_NoConsiderVeh)
						PRINTLN("     ---------->     KILL LIST - I'm Not considering vehicle anymore - event disabled")
					ENDIF
				ENDIF
				
				
				// Help text for approavhing vehicles when event is hidden / not unlocked
				IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_EventHiddenHelp)
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[iPlayerVeh])
							IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niVeh[iPlayerVeh]))) < 2500	
								IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
									IF NOT IS_FM_TYPE_UNLOCKED(FMMC_TYPE_KILL_LIST)
										
										IF NOT IS_COMPETITIVE_UW()
											PRINT_HELP_NO_SOUND("UW_TUT") // You are unable to take part in the Kill List event until you have completed the tutorial.
										ELSE
											PRINT_HELP_NO_SOUND("UW_TUTC") // You are unable to take part in the Kill List event until you have completed the tutorial.
										ENDIF
										SET_FREEMODE_EVENT_HELP_BACKGROUND()
										SET_BIT(iBoolsBitSet2, bi2_EventHiddenHelp)
										PRINTLN("     ---------->     KILL LIST - SET bi2_EventHiddenHelp - TUTORIAL")
									ELSE
										IF NOT IS_COMPETITIVE_UW()
											PRINT_HELP_NO_SOUND("UW_HIDE") // You are unable to take part in the Kill List event as you have chosen to hide the event.
										ELSE
											PRINT_HELP_NO_SOUND("UW_HIDEC") //You are unable to take part in the Kill List event as you have chosen to hide the event.
										ENDIF
										SET_FREEMODE_EVENT_HELP_BACKGROUND()
										SET_BIT(iBoolsBitSet2, bi2_EventHiddenHelp)
										PRINTLN("     ---------->     KILL LIST - SET bi2_EventHiddenHelp - HIDDEN")
									ENDIF
								ELSE
									PRINTLN("     ---------->     KILL LIST - IS_OK_TO_PRINT_FREEMODE_HELP FAIL TUTORIAL / HIDDEN")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//Set Disable Mod Shop Flag
			IF IS_NET_PLAYER_OK(PLAYER_ID())
			AND playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet <> 0 //IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh[iPlayerVeh]))
				IF NOT IS_BIT_SET(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_PLAYER_USING_DESTROY_VEH)
					SET_BIT(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_PLAYER_USING_DESTROY_VEH)
					PRINTLN("     ---------->     KILL LIST - iNGABI_PLAYER_USING_DESTROY_VEH SET")
				ENDIF
			ELSE
				IF IS_BIT_SET(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_PLAYER_USING_DESTROY_VEH)
					CLEAR_BIT(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_PLAYER_USING_DESTROY_VEH)
					PRINTLN("     ---------->     KILL LIST - iNGABI_PLAYER_USING_DESTROY_VEH CLEARED")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC CONTROL_UW_VEH_BODY()
	INT i
	
	CLEAR_BIT(iBoolsBitSet2, bi2_UsingKLVeh)

	REPEAT MAX_PLAYER_VEH i
		CONTROL_VEH_BODY(i)
	ENDREPEAT
	
	IF IS_BIT_SET(iBoolsBitSet2, bi2_UsingKLVeh)
		IF NOT IS_BIT_SET(g_FM_EVENT_VARS.iEventBitSet, ciFM_EVENT_USING_KILL_LIST_VEHICLE)
			SET_BIT(g_FM_EVENT_VARS.iEventBitSet, ciFM_EVENT_USING_KILL_LIST_VEHICLE)
			PRINTLN("     ---------->     KILL LIST -  [CONTROL_UW_VEH_BODY] SET ciFM_EVENT_USING_KILL_LIST_VEHICLE")
		ENDIF
	ELSE
		IF IS_BIT_SET(g_FM_EVENT_VARS.iEventBitSet, ciFM_EVENT_USING_KILL_LIST_VEHICLE)
			CLEAR_BIT(g_FM_EVENT_VARS.iEventBitSet, ciFM_EVENT_USING_KILL_LIST_VEHICLE)
			PRINTLN("     ---------->     KILL LIST -  [CONTROL_UW_VEH_BODY] CLEAR ciFM_EVENT_USING_KILL_LIST_VEHICLE")
		ENDIF
	ENDIF

	
	
ENDPROC


//PURPOSE: Controls the explode vehicle stage
PROC MAINTAIN_EXPLODE_VEH()
//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST -  MAINTAIN_EXPLODE_VEH...")
	INT I
	BOOL bDrawingTimer
	IF NOT Is_This_The_Current_Objective_Text("UW_EXPL")
	AND NOT Is_This_The_Current_Objective_Text("UW_EXPLC")
		Clear_Any_Objective_Text_From_This_Script()
	ENDIF
	
	INT iSoundLoop
	REPEAT MAX_PLAYER_VEH iSoundLoop
		IF NOT IS_BIT_SET(iBoolsBitSet, biExplodeSOund + iSoundLoop)
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niVeh[iSoundLoop])
				IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh[iSoundLoop]))
						SET_BIT(iBoolsBitSet, biExplodeSOund + iSoundLoop)
						iExplodeSOund[iSoundLoop] = GET_SOUND_ID()
						PLAY_SOUND_FROM_ENTITY(iExplodeSOund[iSoundLoop], "Explosion_Countdown", NET_TO_VEH(serverBD.niVeh[iSoundLoop]), "GTAO_FM_Events_Soundset") 
						SET_VARIABLE_ON_SOUND(iExplodeSOund[iSoundLoop],  "Time", 30)
						PRINTLN("     ---------->     KILL LIST - EXPLODE THE VEHICLE - STARTED SOUND ON VEH ", iSoundLoop)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF NOT IS_COMPETITIVE_UW()
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niVeh[0])
			IF NOT IS_BIT_SET(serverBD.iVehExplodeTooFewPlayers, 0)
				IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh[0]))
					IF (GET_VEHICLE_DETONATION_TIME()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.ExplodeVehTimer)) >= 0
						IF playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet <> 0
						OR IS_BIT_SET(serverBD.iServerBitSet, biS_TooFewValid)
							IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh[0]), <<VEH_EXP_RANGE, VEH_EXP_RANGE, VEH_EXP_RANGE>>) 
								IF NOT SHOULD_UI_BE_HIDDEN()
									SET_MIDDLE_TITLE_POSITION_HUD_THIS_FRAME()
									DRAW_GENERIC_TIMER((GET_VEHICLE_DETONATION_TIME()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.ExplodeVehTimer)), "UW_DEST", 0, TIMER_STYLE_USEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_DONTCARE, DEFAULT, HUD_COLOUR_RED, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_RED)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet <> 0
						OR IS_BIT_SET(serverBD.iServerBitSet, biS_TooFewValid)
							IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh[0]), <<VEH_EXP_RANGE, VEH_EXP_RANGE, VEH_EXP_RANGE>>)
								IF NOT SHOULD_UI_BE_HIDDEN()
									SET_MIDDLE_TITLE_POSITION_HUD_THIS_FRAME()
									DRAW_GENERIC_TIMER(0, "UW_DEST", 0, TIMER_STYLE_USEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_DONTCARE, DEFAULT, HUD_COLOUR_RED, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_RED)
								ENDIF
							ENDIF
						ENDIF
						
						IF playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet <> 0
							SET_PLAYER_DEFAULT_FRIENDLY_FIRE_OPTION()
						ENDIF
						
						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niVeh[0])
							SET_ENTITY_INVINCIBLE(NET_TO_VEH(serverBD.niVeh[0]), FALSE)
							SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(NET_TO_VEH(serverBD.niVeh[0]), TRUE)
							
							IF NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(NET_TO_VEH(serverBD.niVeh[0])))
								NETWORK_EXPLODE_VEHICLE(NET_TO_VEH(serverBD.niVeh[0]))
								CLEANUP_NET_ID(serverBD.niVeh[0])
								PRINTLN("     ---------->     KILL LIST - EXPLODE THE VEHICLE - NETWORK_EXPLODE_VEHICLE DONE")
							ELSE
								NETWORK_EXPLODE_HELI(NET_TO_VEH(serverBD.niVeh[0]))
								CLEANUP_NET_ID(serverBD.niVeh[0])
								PRINTLN("     ---------->     KILL LIST - EXPLODE THE VEHICLE - NETWORK_EXPLODE_HELI DONE")
							ENDIF
							
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		REMOVE_VEHICLE_BLIP()
		
		IF NOT SHOULD_UI_BE_HIDDEN()
			IF NOT IS_BIT_SET(iBoolsBitSet, biDoneExplodeHelp)
				IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[0])
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh[0]))
						//	IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
							//	PRINT_HELP("UW_EXPL") // Exit the Urban Warfare vehicle before it explodes.
								IF NOT IS_COMPETITIVE_UW()
									IF NOT Is_This_The_Current_Objective_Text("UW_EXPL")
										Print_Objective_Text("UW_EXPL")
									ENDIF
								ELSE
									IF NOT Is_This_The_Current_Objective_Text("UW_EXPLC")
										Print_Objective_Text("UW_EXPLC")
									ENDIF
								ENDIF
								SET_BIT(iBoolsBitSet, biDoneExplodeHelp)
								PRINTLN("     ---------->     KILL LIST - EXPLODE THE VEHICLE - SET biDoneExplodeHelp")
						//	ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF Is_This_The_Current_Objective_Text("UW_EXPL")
				OR Is_This_The_Current_Objective_Text("UW_EXPLC")
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
							Clear_Any_Objective_Text_From_This_Script()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		REPEAT MAX_PLAYER_VEH i
		//	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet, i)
				IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niVeh[i])
					IF NOT IS_BIT_SET(serverBD.iVehExplodeTooFewPlayers, i)
					//	IF MAINTAIN_OWNERSHIP_OF_NETWORK_ID(serverBD.niVeh[i])
						IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh[i]))
							IF (GET_VEHICLE_DETONATION_TIME()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.ExplodeVehTimer)) >= 0
								IF playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet <> 0
								OR IS_BIT_SET(serverBD.iServerBitSet, biS_TooFewValid)
									IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh[i]), <<VEH_EXP_RANGE, VEH_EXP_RANGE, VEH_EXP_RANGE>>) 
										IF NOT SHOULD_UI_BE_HIDDEN()
											IF NOT bDrawingTimer
										//		PRINTLN("     ---------->     KILL LIST - EXPLODE THE VEHICLE - Hit 9 DRAWING TIMER FOR VEH ", i)
												SET_MIDDLE_TITLE_POSITION_HUD_THIS_FRAME()
												DRAW_GENERIC_TIMER((GET_VEHICLE_DETONATION_TIME()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.ExplodeVehTimer)), "UW_DEST", 0, TIMER_STYLE_USEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_DONTCARE, DEFAULT, HUD_COLOUR_RED, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_RED)
												bDrawingTimer = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet <> 0
								OR IS_BIT_SET(serverBD.iServerBitSet, biS_TooFewValid)
									IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh[i]), <<VEH_EXP_RANGE, VEH_EXP_RANGE, VEH_EXP_RANGE>>)
									
										IF NOT SHOULD_UI_BE_HIDDEN()
											IF NOT bDrawingTimer
												SET_MIDDLE_TITLE_POSITION_HUD_THIS_FRAME()
												DRAW_GENERIC_TIMER(0, "UW_DEST", 0, TIMER_STYLE_USEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_DONTCARE, DEFAULT, HUD_COLOUR_RED, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_RED)
												bDrawingTimer = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								IF playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet <> 0
									SET_PLAYER_DEFAULT_FRIENDLY_FIRE_OPTION()
								ENDIF
								
								IF MAINTAIN_OWNERSHIP_OF_NETWORK_ID(serverBD.niVeh[i])
									IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niVeh[i])
										SET_ENTITY_INVINCIBLE(NET_TO_VEH(serverBD.niVeh[i]), FALSE)
										SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(NET_TO_VEH(serverBD.niVeh[i]), TRUE)
										IF NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(NET_TO_VEH(serverBD.niVeh[i])))
											NETWORK_EXPLODE_VEHICLE(NET_TO_VEH(serverBD.niVeh[i]))
											CLEANUP_NET_ID(serverBD.niVeh[i])
											PRINTLN("     ---------->     KILL LIST - EXPLODE THE VEHICLE - NETWORK_EXPLODE_VEHICLE DONE VEH = ", i)
										ELSE
											NETWORK_EXPLODE_HELI(NET_TO_VEH(serverBD.niVeh[i]))
											CLEANUP_NET_ID(serverBD.niVeh[i])
											PRINTLN("     ---------->     KILL LIST - EXPLODE THE VEHICLE - NETWORK_EXPLODE_HELI DONE VEH = ", i)
										ENDIF
									ELSE
										PRINTLN("     ---------->     KILL LIST - EXPLODE THE VEHICLE WANT TO EXPLODE VEH BUT I DON'T HAVE CONTROL!")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
					ENDIF
				
						
					IF NOT IS_BIT_SET(iBoolsBitSet, biDoneExplodeHelp)
						IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[i])
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh[i]))
									//IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
										IF NOT SHOULD_UI_BE_HIDDEN()
									//		PRINT_HELP("UW_EXPL") // Exit the Urban Warfare vehicle before it explodes.
											IF NOT IS_COMPETITIVE_UW()
												IF NOT Is_This_The_Current_Objective_Text("UW_EXPL")
													Print_Objective_Text("UW_EXPL")
												ENDIF
											ELSE
												IF NOT Is_This_The_Current_Objective_Text("UW_EXPLC")
													Print_Objective_Text("UW_EXPLC")
												ENDIF
											ENDIF
											SET_BIT(iBoolsBitSet, biDoneExplodeHelp)
											PRINTLN("     ---------->     KILL LIST - EXPLODE THE VEHICLE - SET biDoneExplodeHelp")
										ENDIF
									//ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF Is_This_The_Current_Objective_Text("UW_EXPL")
						OR Is_This_The_Current_Objective_Text("UW_EXPLC")
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
									Clear_Any_Objective_Text_From_This_Script()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				
				ENDIF
		//	ENDIF
		ENDREPEAT
	ENDIF
ENDPROC



PROC GET_MY_VEHICLE_KILL_DETAILS(INT &iMyVeh, INT &iMyKills)
	INT i
	REPEAT MAX_PLAYER_VEH i
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet, i)
			iMyVeh = i
			iMyKills = serverBD.iKillsPerVehicle[i] 
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL IS_MY_UW_VEHICLE_DRIVABLE()
	INT iMyVeh, iMyKills
	GET_MY_VEHICLE_KILL_DETAILS(iMyVeh, iMyKills)

	
	IF iMyUwPlayerVeh > -1
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[iMyUwPlayerVeh])
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_SITTING_IN_THEIR_UW_VEHICLE()
	IF iMyUwPlayerVeh > -1
	//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [IS_LOCAL_PLAYER_SITTING_IN_THEIR_UW_VEHICLE] iMyUwPlayerVeh > -1")
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[iMyUwPlayerVeh])
		//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [IS_LOCAL_PLAYER_SITTING_IN_THEIR_UW_VEHICLE] IS_NET_VEHICLE_DRIVEABLE")
			IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh[iMyUwPlayerVeh]))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	//CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [IS_LOCAL_PLAYER_SITTING_IN_THEIR_UW_VEHICLE] FALSE")
	RETURN FALSE
ENDFUNC
FUNC BOOL DID_MY_UW_TEAM_WIN()
	INT i
	INT iMyTeamVeh
	INT iMyTeamKills
	
	GET_MY_VEHICLE_KILL_DETAILS(iMyTeamVeh, iMyTeamKills)
	
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [DID_MY_UW_TEAM_WIN] iMyTeamVeh = ", iMyTeamVeh, " iMyTeamKills = ", iMyTeamKills )
	REPEAT MAX_PLAYER_VEH i
		IF i <> iMyTeamVeh
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [DID_MY_UW_TEAM_WIN] COMPARING WITH TEAM VEH ", i," THEIR TOTAL KILLS = ", serverBD.iKillsPerVehicle[i])
			IF serverBD.iKillsPerVehicle[i] > iMyTeamKills
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [DID_MY_UW_TEAM_WIN] My Team Won!")
	RETURN TRUE
ENDFUNC

FUNC BOOL CHECK_FOR_DRAW(BOOL bLocalPlayerWon = TRUE)
	INT i
	INT iMyTeamVeh
	INT iMyTeamKills
	
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [CHECK_FOR_DRAW] bLocalPlayerWon = ", bLocalPlayerWon) 
	IF bLocalPlayerWon
		GET_MY_VEHICLE_KILL_DETAILS(iMyTeamVeh, iMyTeamKills)
		
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [CHECK_FOR_DRAW] iMyTeamVeh = ", iMyTeamVeh, " iMyTeamKills = ", iMyTeamKills )
		
		REPEAT MAX_PLAYER_VEH i
			IF i <> iMyTeamVeh
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [CHECK_FOR_DRAW] COMPARING WITH TEAM VEH ", i," THEIR TOTAL KILLS = ", serverBD.iKillsPerVehicle[i])
				IF serverBD.iKillsPerVehicle[i] = iMyTeamKills
					RETURN TRUE
				ENDIF
			ENDIF
		ENDREPEAT
	ELSE
		IF serverBD.vehModel = RHINO
		OR serverBD.vehModel = HYDRA
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [CHECK_FOR_DRAW] LOCAL PLAYER DIDN'T WIN. COMPARING serverBD.sLeaderboardData[0].iKills = ", serverBD.sLeaderboardData[0].iKills, " with serverBD.sLeaderboardData[1].iKills = ", serverBD.sLeaderboardData[1].iKills ) 
			RETURN (serverBD.sLeaderboardData[0].iKills = serverBD.sLeaderboardData[1].iKills)
		ELSE
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [CHECK_FOR_DRAW] LOCAL PLAYER DIDN'T WIN. COMPARING serverBD.sLeaderboardDataTeams[0].iKills = ", serverBD.sLeaderboardDataTeams[0].iKills, " with serverBD.sLeaderboardDataTeams[1].iKills = ", serverBD.sLeaderboardDataTeams[1].iKills ) 
			RETURN (serverBD.sLeaderboardDataTeams[0].iKills = serverBD.sLeaderboardDataTeams[1].iKills)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CHECK_FOR_NO_SCORE_DRAW()
//	IF NOT IS_COMPETITIVE_UW()
//		RETURN FALSE
//	ENDIF
	
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_DurationExpired)
	AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_EveryoneFailed)
		RETURN FALSE
	ENDIF
	
	INT i
	REPEAT MAX_PLAYER_VEH i
		IF serverBD.iKillsPerVehicle[i] > 0
			PRINTLN("     ---------->     KILL LIST -  CHECK_FOR_REWARD [CHECK_FOR_NO_SCORE_DRAW] FALSE serverBD.iKillsPerVehicle[", i, "] = ", serverBD.iKillsPerVehicle[i])
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	PRINTLN("     ---------->     KILL LIST -  CHECK_FOR_REWARD [CHECK_FOR_NO_SCORE_DRAW] TRUE!")
	RETURN TRUE
	
ENDFUNC

FUNC BOOL IS_WINNER_IN_MY_GANG(PLAYER_INDEX playerWon)
	IF NOT GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
		RETURN FALSE
	ENDIF
	
	IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerWon, PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


//PURPOSE: Checks to see if this is the end and the player should be rewarded.
PROC CHECK_FOR_REWARD()
	TEXT_LABEL_15 tl15Team 
	TEXT_LABEL_15 tl15Team2
	//TEXT_LABEL_15 tl15Veh
	PLAYER_INDEX playerWon
	PLAYER_INDEX playerDraw[2]
	INT iPlayerDrawCount
	INT iWinningTeam
	INT iDrawingTeam[MAX_PLAYER_VEH]
	INT iDrawingTeamCount
	
	#IF IS_DEBUG_BUILD
	INT iWinningTeams[MAX_PLAYER_VEH]	
	#ENDIF
	
	INT iNumWinningTeams = 1
	
	BOOL bDoDefaultDrawnShard
	
	INT i
	INT iRp
	int iRpSplit
	INT iCash
	INT iCashSplit
	
	#IF IS_DEBUG_BUILD
		IF bWdMyReward
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ----->    KILL LIST [CHECK_FOR_REWARD] [bWdMyReward]") 
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ----->    KILL LIST [CHECK_FOR_REWARD] [bWdMyReward] biP_DoneReward = ",IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneReward))
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ----->    KILL LIST [CHECK_FOR_REWARD] [bWdMyReward] biWaitForShard = ",IS_BIT_SET(iBoolsBitSet, biWaitForShard))
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ----->    KILL LIST [CHECK_FOR_REWARD] [bWdMyReward] iInVehicleBitSet = ",playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet)
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ----->    KILL LIST [CHECK_FOR_REWARD] [bWdMyReward] SHOULD_UI_BE_HIDDEN = ", SHOULD_UI_BE_HIDDEN())
		ENDIF
	#ENDIF
	IF playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet <> 0
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneReward)
			IF NOT IS_BIT_SET(iBoolsBitSet, biWaitForShard)
				IF (IS_BIT_SET(serverBD.iServerBitSet, biS_AllVehiclesDestroyed)
				AND IS_BIT_SET(serverBD.iServerBitSet, biS_AllVehiclePedsDead)
				AND IS_BIT_SET(serverBD.iServerBitSet, biS_AllOnFootPedsDead))
				OR (IS_COMPETITIVE_UW() AND IS_BIT_SET(serverBD.iServerBitSet, biS_DurationExpired))
				OR (IS_COMPETITIVE_UW() AND IS_BIT_SET(serverBD.iServerBitSet, biS_EveryoneFailed))
				
					IF NOT HAS_NET_TIMER_STARTED(timeResults)
						START_NET_TIMER(timeResults)
						PRINTLN("     ---------->     KILL LIST - [CHECK_FOR_REWARD] STARTED timeResults")
					ENDIF
					
					// Brief delay for server rewsults to update
					IF HAS_NET_TIMER_EXPIRED(timeResults, 1000)
						#IF IS_DEBUG_BUILD
							OUTPUT_PLAYER_LB()
							OUTPUT_TEAM_LB()
						#ENDIF
						
						IF Is_This_The_Current_Objective_Text("UW_ATTK")
							Clear_Any_Objective_Text_From_This_Script()
						ENDIF
						
						CLEANUP_ALL_DEAD_VEHS_AND_PEDS()
						
						IF CHECK_FOR_NO_SCORE_DRAW()
							//-- No teams scored any kills
							iCash = GET_PARTICIPATION_CASH()
							IF iCash > 0
								telemetryStruct.m_cashEarned += iCash
								IF (!g_sMPTunables.bkill_list_disable_share_cash AND !IS_BIT_SET(serverBD.iServerBitSet, biS_Competitive))
								OR (!g_sMPTunables.bkill_list_competitive_disable_share_cash AND IS_BIT_SET(serverBD.iServerBitSet, biS_Competitive))
									IF telemetryStruct.m_cashEarned > 0
										SET_LAST_JOB_DATA(LAST_JOB_URBAN_WARFARE, telemetryStruct.m_cashEarned)
									ENDIF
								ENDIF

								g_i_cashForEndEventShard = iCash
								IF USE_SERVER_TRANSACTIONS()
									INT iScriptTransactionIndex
									TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_AMBIENT_JOB_URBAN_WARFARE, iCash, iScriptTransactionIndex, FALSE, TRUE) //Add new service
								ELSE
									AMBIENT_JOB_DATA amData
									NETWORK_EARN_FROM_AMBIENT_JOB(iCash, "AM_KILL_LIST", amData)
								ENDIF
								
							ENDIF
							
							iRp = GET_PARTICIPATION_RP() 
							NEXT_RP_ADDITION_SHOW_RANKBAR()
							telemetryStruct.m_rpEarned +=iRp
							GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD,PLAYER_PED_ID(),"",XPTYPE_COMPLETE,XPCATEGORY_KILL_LIST_KILL, iRp)
							
							telemetryStruct.m_endReason       	=AE_END_LOST	
							SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_FAIL,"UW_BIGF","UW_DRAWZ", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME) 
							
							PRINTLN("     ---------->     KILL LIST - [CHECK_FOR_REWARD] NO SCORE DRAW! iCash = ", iCash, " iRp = ", iRp)
						ELSE
							IF NOT IS_COMPETITIVE_UW()
								iCash = GET_CASH_REWARD()
								
									GB_HANDLE_GANG_BOSS_CUT(iCash)
								
								iCash += GET_PARTICIPATION_CASH()

								
								IF iCash > 0

									
									telemetryStruct.m_cashEarned += iCash
									IF !g_sMPTunables.bkill_list_disable_share_cash
										IF telemetryStruct.m_cashEarned > 0
											SET_LAST_JOB_DATA(LAST_JOB_URBAN_WARFARE, telemetryStruct.m_cashEarned)
										ENDIF
									ENDIF
									g_i_cashForEndEventShard = iCash
									IF USE_SERVER_TRANSACTIONS()
										INT iScriptTransactionIndex
										TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_AMBIENT_JOB_URBAN_WARFARE, iCash, iScriptTransactionIndex, FALSE, TRUE) //Add new service
									ELSE
										AMBIENT_JOB_DATA amData
										NETWORK_EARN_FROM_AMBIENT_JOB(iCash, "AM_KILL_LIST", amData)
									ENDIF
								ENDIF
								
							//	SET_LAST_JOB_DATA(LAST_JOB_URBAN_WARFARE, GET_CASH_REWARD())
								
							//	GIVE_LOCAL_PLAYER_XP(eXPTYPE_CO_OP_AMBIENTFM, "XPT_IMPEXP",XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_IMPORT_EXPORT_DELIVERY, GET_RP_REWARD())
							
								iRp = GET_PARTICIPATION_RP() 
								iRp += GET_RP_REWARD()
								telemetryStruct.m_rpEarned +=iRp
								NEXT_RP_ADDITION_SHOW_RANKBAR()
								GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD,PLAYER_PED_ID(),"",XPTYPE_COMPLETE,XPCATEGORY_KILL_LIST_KILL, iRp)
								
								PRINTLN("     ---------->     KILL LIST -  CHECK_FOR_REWARD - REWARD GIVEN $", GET_CASH_REWARD(), " RP GIVEN ", GET_RP_REWARD())
								PRINTLN("     ---------->     KILL LIST -  CHECK_FOR_REWARD - serverbd.iTotalKills = ", serverbd.iTotalKills, " serverBD.iKillGoal = ", serverBD.iKillGoal, " Team 1 score = ", serverBD.iKillsPerVehicle[0], " Team 2 score = ",   serverBD.iKillsPerVehicle[1])
								//RP
								//GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "XPT_KAIE", XPTYPE_SKILL, XPCATEGORY_COMPLETE_HOLD_UP, g_sMPTunables.iUrbanWarfareRpReward, 1)
								telemetryStruct.m_endReason       	=AE_END_WON
							//	SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_GENERIC_WINNER, "", "UW_STRAP", "UW_BIGM", DEFAULT, GET_CASH_REWARD())	//Urban Warfare Completed 		$~1~
								SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_FM_EVENT_WIN, "", "UW_BIG_BMEXP", "UW_BIGM", ciFM_EVENTS_END_UI_SHARD_TIME)
							ELSE
								IF DID_MY_UW_TEAM_WIN()
									IF NOT CHECK_FOR_DRAW()
										IF NOT Is_This_The_Current_Objective_Text("UW_EXPL")
										AND NOT Is_This_The_Current_Objective_Text("UW_EXPLC")
											Clear_Any_Objective_Text_From_This_Script()
										ENDIF
										
										iCash = GET_CASH_REWARD()
								
											GB_HANDLE_GANG_BOSS_CUT(iCash)
										
										iCash += GET_PARTICIPATION_CASH()
										
										telemetryStruct.m_cashEarned += iCash
										IF !g_sMPTunables.bkill_list_competitive_disable_share_cash
											IF telemetryStruct.m_cashEarned > 0
												SET_LAST_JOB_DATA(LAST_JOB_URBAN_WARFARE, telemetryStruct.m_cashEarned)
											ENDIF
										ENDIF
										g_i_cashForEndEventShard = iCash
										IF iCash > 0
											IF USE_SERVER_TRANSACTIONS()
												INT iScriptTransactionIndex
												TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_AMBIENT_JOB_URBAN_WARFARE, iCash, iScriptTransactionIndex, FALSE, TRUE) //Add new service
											ELSE
												AMBIENT_JOB_DATA amData
												NETWORK_EARN_FROM_AMBIENT_JOB(iCash, "AM_KILL_LIST", amData)
											ENDIF
										ENDIF
										
										IF serverBD.vehModel = RHINO
										OR serverBD.vehModel = HYDRA
											SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_FM_EVENT_WIN, "", "UW_WON1P", "UW_BIGM", ciFM_EVENTS_END_UI_SHARD_TIME)
										ELSE
											SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_FM_EVENT_WIN, "", "UW_WON", "UW_BIGM", ciFM_EVENTS_END_UI_SHARD_TIME)
										ENDIF
										
										telemetryStruct.m_endReason       	=AE_END_WON
										iRp = GET_RP_REWARD()
										iRp += GET_PARTICIPATION_RP()
										telemetryStruct.m_rpEarned +=iRp
										NEXT_RP_ADDITION_SHOW_RANKBAR()
										GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD,PLAYER_PED_ID(),"",XPTYPE_SKILL,XPCATEGORY_COMPLETE_COMPETITIVE_KILL_LIST, iRp)
										
										PRINTLN("     ---------->     KILL LIST (C) -  CHECK_FOR_REWARD - REWARD GIVEN $", iCash, " GIVEN RP ", iRp)
									ELSE
										// I was in winning team, but there was a draw
										iCash = GET_PARTICIPATION_CASH()
										iRp = GET_PARTICIPATION_RP()
										iNumWinningTeams = 0
										
										//-- Figure out which player / Team we drew with
										FOR i = 0 TO MAX_PLAYER_VEH - 1
											IF serverBD.sLeaderboardData[i].iKills = serverBD.sLeaderboardData[0].iKills
												
												#IF IS_DEBUG_BUILD
												iWinningTeams[i] = serverBD.sLeaderboardData[i].iTeam
												PRINTLN("     ---------->     KILL LIST (C) -  CHECK_FOR_REWARD - EVENT DRAWN, THIS TEAM ", iWinningTeams[i]," HAS SAME SCORE = ", serverBD.sLeaderboardData[i].iKills)
												#ENDIF

												iNumWinningTeams++
												
												iDrawingTeam[i] = -1
												
												IF serverBD.VehModel = HYDRA
												OR serverBD.VehModel = RHINO
													IF serverBD.sLeaderboardData[i].iPlayer > -1
														IF serverBD.sLeaderboardData[i].iPlayer <> NATIVE_TO_INT(PLAYER_ID())
															IF NETWORK_IS_PLAYER_ACTIVE(INT_TO_PLAYERINDEX(serverBD.sLeaderboardData[i].iPlayer))
																playerDraw[iPlayerDrawCount] = INT_TO_PLAYERINDEX(serverBD.sLeaderboardData[i].iPlayer)
																PRINTLN("     ---------->     KILL LIST (C) -  CHECK_FOR_REWARD - EVENT DRAWN SETTING playerDraw[", iPlayerDrawCount,"] TO PLAYER ", GET_PLAYER_NAME(playerDraw[iPlayerDrawCount]))
																iPlayerDrawCount++
															ENDIF
														ENDIF
													ENDIF
												ELSE
													IF iMyUwPlayerVeh > -1
														IF iMyUwPlayerVeh != serverBD.sLeaderboardData[i].iTeam
															iDrawingTeam[iDrawingTeamCount] = serverBD.sLeaderboardData[i].iTeam
															PRINTLN("     ---------->     KILL LIST (C) -  CHECK_FOR_REWARD - EVENT DRAWN SETTING iDrawingTeam[", iDrawingTeamCount,"] TO TEAM ", serverBD.sLeaderboardData[i].iTeam)
															iDrawingTeamCount++
															
														ENDIF
													ENDIF
												ENDIF
												
											ENDIF
										ENDFOR
										
										PRINTLN("     ---------->     KILL LIST (C) -  CHECK_FOR_REWARD - EVENT DRAWN, THERE WAS THIS MANY WINNING TEAMS ", iNumWinningTeams)
										
										
										IF serverBD.VehModel = HYDRA
										OR serverBD.VehModel = RHINO
											
											IF iNumWinningTeams > 2 
												PRINTLN("     ---------->     KILL LIST (C) -  CHECK_FOR_REWARD - EVENT DRAWN NOT LISTING PLAYERS AS iNumWinningTeams = ", iNumWinningTeams)
												bDoDefaultDrawnShard = TRUE
											ELSE
												IF playerDraw[0] = INVALID_PLAYER_INDEX()
													PRINTLN("     ---------->     KILL LIST (C) -  CHECK_FOR_REWARD - EVENT DRAWN WANT TO LIST PLAYERS BUT playerDraw[0] = INVALID_PLAYER_INDEX() ")
													bDoDefaultDrawnShard = TRUE
												ELSE
													PRINTLN("     ---------->     KILL LIST (C) -  CHECK_FOR_REWARD - EVENT DRAWN I DREW WITH THIS PLAYER ", GET_PLAYER_NAME(playerDraw[0]))
													// Your vehicle tied with ~a~ to win the Urban Warfare event
													//SETUP_NEW_BIG_MESSAGE_WITH_PLAYER_AND_STRING(BIG_MESSAGE_FM_EVENT_DRAW, playerDraw[0], "", "UW_DRAWP", "UW_BIGO", ciFM_EVENTS_END_UI_SHARD_TIME, DEFAULT, DEFAULT, DEFAULT, DEFAULT)
													SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_FM_EVENT_DRAW, "UW_DRAWP", GET_PLAYER_NAME(playerDraw[0]), DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME, "UW_BIGO")
												ENDIF
											ENDIF
										ELSE
											IF iNumWinningTeams > 2 
												PRINTLN("     ---------->     KILL LIST (C) -  CHECK_FOR_REWARD - EVENT DRAWN NOT LISTING TEAMS AS iNumWinningTeams = ", iNumWinningTeams)
												bDoDefaultDrawnShard = TRUE
											ELSE
												IF iDrawingTeam[0] = -1
													PRINTLN("     ---------->     KILL LIST (C) -  CHECK_FOR_REWARD - EVENT DRAWN WANT TO LIST TEAMS BUT iDrawingTeam[0] = -1 ")
												ELSE
			//										IF serverBD.VehModel = SAVAGE
			//											tl15Veh = "UW_TMSAV"
			//										ELIF serverBD.VehModel = BUZZARD
			//											tl15Veh = "UW_TMBUZ"
			//										ELIF serverBD.VehModel = VALKYRIE 
			//											tl15Veh = "UW_TMVALK"
			//										ENDIF
													
													tl15Team = "UW_TM"
													tl15Team += (iDrawingTeam[0] + 1)
													PRINTLN("     ---------->     KILL LIST (C) -  CHECK_FOR_REWARD - MY TEAM DREW WITH THIS TEAM ",iDrawingTeam[0])
													SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_FM_EVENT_DRAW, tl15Team, "UW_DRAWP", "UW_BIGO", ciFM_EVENTS_END_UI_SHARD_TIME, DEFAULT, DEFAULT)
												ENDIF
										
											ENDIF
											
										ENDIF
										
										IF bDoDefaultDrawnShard
											IF NOT IS_COMPETITIVE_UW()
												SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_DRAW ,"UW_BIGO","UW_DRAW", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME) // The Urban Warfare event was drawn.
											ELSE
												SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_DRAW ,"UW_BIGO","UW_DRAWC", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME) // The Urban Warfare event was drawn.
											ENDIF
										ENDIF
										
										iCashSplit =  ROUND(TO_FLOAT(GET_CASH_REWARD()) / TO_FLOAT(iNumWinningTeams ))
										iRpSplit = ROUND(TO_FLOAT(GET_RP_REWARD()) / TO_FLOAT(iNumWinningTeams ))
										PRINTLN("     ---------->     KILL LIST (C) -  CHECK_FOR_REWARD - EVENT DRAWN CASH SPLIT = ", iCashSplit, " RP SPLIT = ", iRpSplit)
										
										GB_HANDLE_GANG_BOSS_CUT(iCashSplit)
										
										iCash += iCashSplit
										
										iRp += iRpSplit
										telemetryStruct.m_cashEarned += iCash
										IF !g_sMPTunables.bkill_list_competitive_disable_share_cash
											IF telemetryStruct.m_cashEarned > 0
												SET_LAST_JOB_DATA(LAST_JOB_URBAN_WARFARE, telemetryStruct.m_cashEarned)
											ENDIF
										ENDIF
										g_i_cashForEndEventShard = iCash
										IF iCash > 0
											IF USE_SERVER_TRANSACTIONS()
												INT iScriptTransactionIndex
												TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_AMBIENT_JOB_URBAN_WARFARE, iCash, iScriptTransactionIndex, FALSE, TRUE) //Add new service
											ELSE
												AMBIENT_JOB_DATA amData
												NETWORK_EARN_FROM_AMBIENT_JOB(iCash, "AM_KILL_LIST", amData)
											ENDIF
										ENDIF
										
										
										
										telemetryStruct.m_rpEarned +=iRp
										NEXT_RP_ADDITION_SHOW_RANKBAR()
										GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD,PLAYER_PED_ID(),"",XPTYPE_SKILL,XPCATEGORY_COMPLETE_COMPETITIVE_KILL_LIST, iRp)
										
										
										PRINTLN("     ---------->     KILL LIST (C) -  CHECK_FOR_REWARD - REWARD GIVEN $", iCash, " GIVEN RP ", iRp)
									ENDIF
								ELSE
									// I wasn't in winning team
									FM_EVENT_SETUP_DO_MINIMUM_PARTICIPATION_HELP(TRUE)
									iCash = GET_PARTICIPATION_CASH()
									iRp = GET_PARTICIPATION_RP()
									telemetryStruct.m_cashEarned += iCash
									IF !g_sMPTunables.bkill_list_competitive_disable_share_cash
										IF telemetryStruct.m_cashEarned > 0
											SET_LAST_JOB_DATA(LAST_JOB_URBAN_WARFARE, telemetryStruct.m_cashEarned)
										ENDIF
									ENDIF
									g_i_cashForEndEventShard = iCash
									IF iCash > 0
										IF USE_SERVER_TRANSACTIONS()
											INT iScriptTransactionIndex
											TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_AMBIENT_JOB_URBAN_WARFARE, iCash, iScriptTransactionIndex, FALSE, TRUE) //Add new service
										ELSE
											AMBIENT_JOB_DATA amData
											NETWORK_EARN_FROM_AMBIENT_JOB(iCash, "AM_KILL_LIST", amData)
										ENDIF
									ENDIF

									telemetryStruct.m_rpEarned +=iRp
									NEXT_RP_ADDITION_SHOW_RANKBAR()
									GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD,PLAYER_PED_ID(),"",XPTYPE_SKILL,XPCATEGORY_COMPLETE_COMPETITIVE_KILL_LIST, iRp)
									PRINTLN("     ---------->     KILL LIST (C) -  CHECK_FOR_REWARD I WASN'T IN WINNING TEAM - REWARD GIVEN $", iCash, " GIVEN RP ", iRp)
										
										
									IF NOT CHECK_FOR_DRAW(FALSE)
										//-- I wasn't in winning team, and there wasn't a draw
										iWinningTeam = serverBD.sLeaderboardData[0].iTeam
										IF NOT Is_This_The_Current_Objective_Text("UW_EXPL")
										AND NOT Is_This_The_Current_Objective_Text("UW_EXPLC")
											Clear_Any_Objective_Text_From_This_Script()
										ENDIF
										
										IF serverBD.VehModel = HYDRA
										OR serverBD.VehModel = RHINO
										//	BOOL bDoBossShard
											BOOL bDoGangShard
											playerWon = INT_TO_PLAYERINDEX(serverBD.sLeaderboardData[0].iPlayer)
											
												INT iTempGangID
												STRING sOrganization
												HUD_COLOURS hcTempGang
													
												IF GB_IS_PLAYER_MEMBER_OF_A_GANG(playerWon)
													PRINTLN("     ---------->     KILL LIST (C) -  CHECK_FOR_REWARD - THINK WINNING PLAYER ", GET_PLAYER_NAME(playerWon), " IS A GANG MEMBER") 
													iTempGangID = GET_GANG_ID_FOR_PLAYER(playerWon)
													IF iTempGangID > -1
														hcTempGang = GET_HUD_COLOUR_FOR_GANG_ID(iTempGangID)
														sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(playerWon)
														
														bDoGangShard = TRUE
														PRINTLN("     ---------->     KILL LIST (C) -  CHECK_FOR_REWARD - THINK WINNING PLAYER ", GET_PLAYER_NAME(playerWon), " IS A GANG MEMBER hcTempGang = ", ENUM_TO_INT(hcTempGang), " sOrganization = ", sOrganization ) 
													ENDIF
												ENDIF
											
											IF playerWon <> INVALID_PLAYER_INDEX()
												IF NOT bDoGangShard
													IF NOT IS_COMPETITIVE_UW()
														//SETUP_NEW_BIG_MESSAGE_WITH_PLAYER_AND_STRING(BIG_MESSAGE_FM_EVENT_FAIL, playerWon, "", "UW_FWONP", "UW_BIGF", ciFM_EVENTS_END_UI_SHARD_TIME, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_RED)
														SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_FM_EVENT_FAIL, "UW_FWONP", GET_PLAYER_NAME(playerWon), HUD_COLOUR_RED, ciFM_EVENTS_END_UI_SHARD_TIME, "UW_BIGF")
													ELSE
														//SETUP_NEW_BIG_MESSAGE_WITH_PLAYER_AND_STRING(BIG_MESSAGE_FM_EVENT_FAIL, playerWon, "", "UW_FWONPC", "UW_BIGF", ciFM_EVENTS_END_UI_SHARD_TIME, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_RED)
														SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_FM_EVENT_FAIL, "UW_FWONPC", GET_PLAYER_NAME(playerWon), HUD_COLOUR_RED, ciFM_EVENTS_END_UI_SHARD_TIME, "UW_BIGF")
													ENDIF
													PRINTLN("     ---------->     KILL LIST (C) -  CHECK_FOR_REWARD - MY TEAM DIDN'T WIN, THINK THIS TEAM WON ",iWinningTeam, " WHICH IS PLAYER ", GET_PLAYER_NAME(playerWon))
												ELSE
													PRINTLN("     ---------->     KILL LIST (C) -  CHECK_FOR_REWARD - DOING GAANG SHARD PLAYER ", GET_PLAYER_NAME(playerWon), " IS A GANG MEMBER hcTempGang = ", ENUM_TO_INT(hcTempGang), " sOrganization = ", sOrganization ) 
													IF NOT IS_COMPETITIVE_UW()
												//		SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_FM_EVENT_FAIL, "UW_FWONG", GET_PLAYER_NAME(playerWon), HUD_COLOUR_RED, ciFM_EVENTS_END_UI_SHARD_TIME, "UW_BIGF")
														SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_FM_EVENT_FINISHED, "PEN_OVR", "UW_FWONG", sOrganization, hcTempGang)
													ELSE
												//		SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_FM_EVENT_FAIL, "UW_FWONGC", GET_PLAYER_NAME(playerWon), HUD_COLOUR_RED, ciFM_EVENTS_END_UI_SHARD_TIME, "UW_BIGF")
														SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_FM_EVENT_FINISHED, "PEN_OVR", "UW_FWONGC", sOrganization, hcTempGang)
													ENDIF
													PRINTLN("     ---------->     KILL LIST (C) -  CHECK_FOR_REWARD - MY TEAM DIDN'T WIN, THINK THIS TEAM WON ",iWinningTeam, " WHICH IS MY GOON ", GET_PLAYER_NAME(playerWon))
												
												ENDIF
											ELSE
												#IF IS_DEBUG_BUILD
													PRINTLN("     ---------->     KILL LIST (C) -  CHECK_FOR_REWARD - MY TEAM DIDN'T WIN, THINK THIS TEAM WON ",iWinningTeam, " BUT DON'T KNOW WHICH PLAYER! LEADERBOARD...")
													OUTPUT_PLAYER_LB()
													OUTPUT_TEAM_LB()
												#ENDIF
											ENDIF
										ELSE
			//								IF serverBD.VehModel = SAVAGE
			//									tl15Veh = "UW_TMSAV"
			//								ELIF serverBD.VehModel = BUZZARD
			//									tl15Veh = "UW_TMBUZ"
			//								ELIF serverBD.VehModel = VALKYRIE 
			//									tl15Veh = "UW_TMVALK"
			//								ENDIF
											
											tl15Team = "UW_TM"
											tl15Team += (iWinningTeam + 1)
											PRINTLN("     ---------->     KILL LIST (C) -  CHECK_FOR_REWARD - MY TEAM DIDN'T WIN, THINK THIS TEAM WON ",iWinningTeam)
											IF NOT IS_COMPETITIVE_UW()
												SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_FM_EVENT_FAIL, tl15Team, "UW_FWONT", "UW_BIGF", ciFM_EVENTS_END_UI_SHARD_TIME, DEFAULT, DEFAULT, "", HUD_COLOUR_RED)
											ELSE
												SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_FM_EVENT_FAIL, tl15Team, "UW_FWONTC", "UW_BIGF", ciFM_EVENTS_END_UI_SHARD_TIME, DEFAULT, DEFAULT, "", HUD_COLOUR_RED)
											ENDIF
										ENDIF
										
										//SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_GENERIC_LOSER, "", "UW_FWON", "UW_BIGF", DEFAULT, (TIME_EXPOLDE_VEH/1000))

									ELSE
									 	//-- I wasn't in winning team, there was a draw
										IF NOT Is_This_The_Current_Objective_Text("UW_EXPL")
										AND NOT Is_This_The_Current_Objective_Text("UW_EXPLC")
											Clear_Any_Objective_Text_From_This_Script()
										ENDIF
										
										#IF IS_DEBUG_BUILD
										iWinningTeams[0] = serverBD.sLeaderboardData[0].iTeam
										PRINTLN("     ---------->     KILL LIST (C) -  CHECK_FOR_REWARD - EVENT DRAWN, THIS TEAM IN FIRST PLACE ", iWinningTeams[0])
										#ENDIF
										
										FOR i = 1 TO MAX_PLAYER_VEH - 1
											IF serverBD.sLeaderboardData[i].iKills = serverBD.sLeaderboardData[0].iKills
												#IF IS_DEBUG_BUILD
												iWinningTeams[i] = serverBD.sLeaderboardData[i].iTeam
												PRINTLN("     ---------->     KILL LIST (C) -  CHECK_FOR_REWARD - EVENT DRAWN, THIS TEAM ", iWinningTeams[i]," HAS SAME SCORE = ", serverBD.sLeaderboardData[i].iKills)
												#ENDIF
												
												iNumWinningTeams++
												
												IF serverBD.VehModel = HYDRA
												OR serverBD.VehModel = RHINO
													IF serverBD.sLeaderboardData[i].iPlayer > -1

														IF NETWORK_IS_PLAYER_ACTIVE(INT_TO_PLAYERINDEX(serverBD.sLeaderboardData[i].iPlayer))
															playerDraw[iPlayerDrawCount] = INT_TO_PLAYERINDEX(serverBD.sLeaderboardData[i].iPlayer)
															PRINTLN("     ---------->     KILL LIST (C) -  CHECK_FOR_REWARD - EVENT DRAWN SETTING playerDraw[", iPlayerDrawCount,"] TO PLAYER ", GET_PLAYER_NAME(playerDraw[iPlayerDrawCount]))
															iPlayerDrawCount++
														ENDIF
														
													ENDIF
												ELSE
													IF iMyUwPlayerVeh > -1
														IF iMyUwPlayerVeh != serverBD.sLeaderboardData[i].iTeam
															iDrawingTeam[iDrawingTeamCount] = serverBD.sLeaderboardData[i].iTeam
															PRINTLN("     ---------->     KILL LIST (C) -  CHECK_FOR_REWARD - EVENT DRAWN SETTING iDrawingTeam[", iDrawingTeamCount,"] TO TEAM ", serverBD.sLeaderboardData[i].iTeam)
															iDrawingTeamCount++
															
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDFOR
										PRINTLN("     ---------->     KILL LIST (C) -  CHECK_FOR_REWARD - EVENT DRAWN, THERE WAS THIS MANY WINNING TEAMS ", iNumWinningTeams)
										
										IF serverBD.VehModel = HYDRA
										OR serverBD.VehModel = RHINO
											IF iNumWinningTeams = 2 
												IF playerDraw[0] != INVALID_PLAYER_INDEX()
												AND NETWORK_IS_PLAYER_ACTIVE(playerDraw[0])
													IF playerDraw[1] != INVALID_PLAYER_INDEX()
													AND NETWORK_IS_PLAYER_ACTIVE(playerDraw[1])
														PRINTLN("     ---------->     KILL LIST (C) -  CHECK_FOR_REWARD - I DIDN'T WIN EVENT DRAWN BY THESE PLAYERS ", GET_PLAYER_NAME(playerDraw[0]), " AND ", GET_PLAYER_NAME(playerDraw[1]))
														SETUP_NEW_BIG_MESSAGE_WITH_2_PLAYERS(BIG_MESSAGE_FM_EVENT_DRAW, INVALID_PLAYER_INDEX(), INVALID_PLAYER_INDEX(), DEFAULT, "UW_DRAW2P", "UW_BIGF", GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE(), ciFM_EVENTS_END_UI_SHARD_TIME, GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE(), GET_PLAYER_NAME(playerDraw[0]), GET_PLAYER_NAME(playerDraw[1]))
													ELSE
														PRINTLN("     ---------->     KILL LIST (C) -  CHECK_FOR_REWARD - I DIDN'T WIN EVENT DRAWN WANT TO LIST PLAYERS BUT playerDraw[1] = INVALID_PLAYER_INDEX() ")
														bDoDefaultDrawnShard = TRUE
													ENDIF
												ELSE
													PRINTLN("     ---------->     KILL LIST (C) -  CHECK_FOR_REWARD - I DIDN'T WIN EVENT DRAWN WANT TO LIST PLAYERS BUT playerDraw[0] = INVALID_PLAYER_INDEX() ")
													bDoDefaultDrawnShard = TRUE
												ENDIF
											ELSE
												bDoDefaultDrawnShard = TRUE
												PRINTLN("     ---------->     KILL LIST (C) -  CHECK_FOR_REWARD - EVENT DRAWN (2) NOT LISTING TEAMS AS iNumWinningTeams = ", iNumWinningTeams)
											ENDIF
											
											
										ELSE
											IF iNumWinningTeams = 2 
												
												
												tl15Team = "UW_TM"
												tl15Team += (iDrawingTeam[0] + 1)
												
												tl15Team2 = "UW_TM"
												tl15Team2 += (iDrawingTeam[1] + 1) 
												
												PRINTLN("     ---------->     KILL LIST (C) -  CHECK_FOR_REWARD - I DIDN'T WIN EVENT DRAWN WANT TO LIST TEAMS FIRST DRAW TEAM ", tl15Team, " SECOND DRAW TEAM ", tl15Team2)
												
												SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_MISSION_OVER, tl15Team, "UW_DRAW2P", "UW_BIGO", ciFM_EVENTS_END_UI_SHARD_TIME, DEFAULT, DEFAULT, tl15Team2)
												
												bDoDefaultDrawnShard = TRUE
											ELSE
												PRINTLN("     ---------->     KILL LIST (C) -  CHECK_FOR_REWARD - I DIDN'T WIN EVENT DRAWN NOT LISTING TEAMS AS iNumWinningTeams = ", iNumWinningTeams)
												bDoDefaultDrawnShard = TRUE
											ENDIF
											
										ENDIF
										
										IF bDoDefaultDrawnShard
											SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_DRAW ,"UW_BIGO","UW_DRAW", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME) // The Urban Warfare event was drawn.
										ENDIF
										
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						SET_LOCAL_PLAYER_DOING_KILL_LIST(FALSE)
						IF playerBD[PARTICIPANT_ID_TO_INT()].eStage < eAD_OFF_MISSION
							playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_OFF_MISSION
							NET_NL() NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  PLAYER STAGE = eAD_OFF_MISSION - REWARD DONE    <----------     ") NET_NL()
						ENDIF
						SET_BIT(iBoolsBitSet,biCompletedOK)
						SET_BIT(iBoolsBitSet, biWaitForShard)
						PRINTLN("     ---------->     KILL LIST - biWaitForShard SET 1")
					ENDIF
				ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_DurationExpired)
				OR (NOT IS_COMPETITIVE_UW() AND IS_BIT_SET(serverBD.iServerBitSet, biS_EveryoneFailed))
					IF playerBD[PARTICIPANT_ID_TO_INT()].iVehTooFewPlayers <> -1
						//-- Abandoned?
						PRINTLN("     ---------->     KILL LIST - [CHECK_FOR_REWARD] NON-C DOING ABANDONED SHARD AS playerBD[PARTICIPANT_ID_TO_INT()].iVehTooFewPlayers = ", playerBD[PARTICIPANT_ID_TO_INT()].iVehTooFewPlayers)
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_FINISHED,"UW_BIGO","UW_ABAND", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME) 
					ELSE	
						SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_FM_EVENT_FAIL, "", "UW_FAIL", "UW_BIGF", ciFM_EVENTS_END_UI_SHARD_TIME, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_RED)
					ENDIF
					
					
					SET_LOCAL_PLAYER_DOING_KILL_LIST(FALSE)
					SET_BIT(iBoolsBitSet, biWaitForShard)
					PRINTLN("     ---------->     KILL LIST - biWaitForShard SET 2")
					iCash = GET_PARTICIPATION_CASH()
					telemetryStruct.m_cashEarned += iCash
					IF !g_sMPTunables.bkill_list_disable_share_cash
						IF telemetryStruct.m_cashEarned > 0
							SET_LAST_JOB_DATA(LAST_JOB_URBAN_WARFARE, telemetryStruct.m_cashEarned)
						ENDIF
					ENDIF
					g_i_cashForEndEventShard = iCash
					IF iCash > 0
						IF USE_SERVER_TRANSACTIONS()
							INT iScriptTransactionIndex
							TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_AMBIENT_JOB_URBAN_WARFARE, iCash, iScriptTransactionIndex, FALSE, TRUE) //Add new service
						ELSE
							AMBIENT_JOB_DATA amData
							NETWORK_EARN_FROM_AMBIENT_JOB(iCash, "AM_KILL_LIST", amData)
						ENDIF
					ENDIF
					
					iRp = GET_PARTICIPATION_RP() 

					telemetryStruct.m_rpEarned +=iRp
					NEXT_RP_ADDITION_SHOW_RANKBAR()
					GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD,PLAYER_PED_ID(),"",XPTYPE_COMPLETE,XPCATEGORY_KILL_LIST_KILL, iRp)
					
					telemetryStruct.m_endReason       	=AE_END_LOST	
						
					IF playerBD[PARTICIPANT_ID_TO_INT()].eStage < eAD_OFF_MISSION
						playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_OFF_MISSION
						NET_NL()  NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  PLAYER STAGE = eAD_OFF_MISSION - AS biS_DurationExpired / biS_EveryoneFailed    <----------     ") NET_NL()
					ENDIF
				ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_TooFewValid)
					SET_LOCAL_PLAYER_DOING_KILL_LIST(FALSE)
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneReward) // Don't want to wait for shard as it will block the explosion timer
					PRINTLN("     ---------->     KILL LIST - [CHECK_FOR_REWARD] SET biP_DoneReward AS biS_TooFewValid")
					IF playerBD[PARTICIPANT_ID_TO_INT()].eStage < eAD_EXPLODE_VEH
						playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_EXPLODE_VEH
						NET_NL()  NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  PLAYER STAGE = eAD_EXPLODE_VEH - AS biS_TooFewValid    <----------     ") NET_NL()
					ENDIF
			//		SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_MISSION_OVER,"UW_BIGO", "UW_TFEW", GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE())
				ELIF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_IamOOB)	
					SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_FM_EVENT_FAIL, "", "UW_FAILD", "UW_BIGF", ciFM_EVENTS_END_UI_SHARD_TIME, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_RED) 
					SET_LOCAL_PLAYER_DOING_KILL_LIST(FALSE)
					SET_BIT(iBoolsBitSet, biWaitForShard)
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneReward)
					
					iCash = GET_PARTICIPATION_CASH()
					telemetryStruct.m_cashEarned += iCash
					IF !g_sMPTunables.bkill_list_disable_share_cash
						IF telemetryStruct.m_cashEarned > 0
							SET_LAST_JOB_DATA(LAST_JOB_URBAN_WARFARE, telemetryStruct.m_cashEarned)
						ENDIF
					ENDIF
					
					g_i_cashForEndEventShard = iCash
					IF iCash > 0
						IF USE_SERVER_TRANSACTIONS()
							INT iScriptTransactionIndex
							TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_AMBIENT_JOB_URBAN_WARFARE, iCash, iScriptTransactionIndex, FALSE, TRUE) //Add new service
						ELSE
							AMBIENT_JOB_DATA amData
							NETWORK_EARN_FROM_AMBIENT_JOB(iCash, "AM_KILL_LIST", amData)
						ENDIF
					ENDIF
					
					iRp = GET_PARTICIPATION_RP() 

					telemetryStruct.m_rpEarned +=iRp
					NEXT_RP_ADDITION_SHOW_RANKBAR()
					GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD,PLAYER_PED_ID(),"",XPTYPE_COMPLETE,XPCATEGORY_KILL_LIST_KILL, iRp)
					
					telemetryStruct.m_endReason       	=AE_END_LOST	
						
					IF playerBD[PARTICIPANT_ID_TO_INT()].eStage < eAD_OFF_MISSION
						playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_OFF_MISSION
						NET_NL()  NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  PLAYER STAGE = eAD_OFF_MISSION - AS biP_IamOOB    <----------     ") NET_NL()
					ENDIF
				ELIF IS_BIT_SET(serverBD.iServerBitSet ,biS_TimeoutExpired)
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneReward)
					PRINTLN("     ---------->     KILL LIST (C) -  CHECK_FOR_REWARD SET biP_DoneReward AS biS_TimeoutExpired")
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneReward)
				IF IS_BIT_SET(iBoolsBitSet, biWaitForShard)
					IF Is_This_The_Current_Objective_Text("UW_ATTK")
						Clear_Any_Objective_Text_From_This_Script()
					ENDIF
					
					IF MAINTAIN_FM_EVENTS_END_UI(sEndUiVars)
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneReward)
						PRINTLN("     ---------->     KILL LIST (C) -  CHECK_FOR_REWARD SET biP_DoneReward")
					ELSE
						PRINTLN("     ---------->     KILL LIST (C) -  CHECK_FOR_REWARD WAITING FOR sEndUiVars")
					ENDIF
					
	//				IF NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GENERIC_LOSER)
	//				AND NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_MISSION_OVER)
	//				AND NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GENERIC_WINNER)
					IF NOT Is_This_The_Current_Objective_Text("UW_EXPL")
					AND NOT Is_This_The_Current_Objective_Text("UW_EXPLC")
						IF playerBD[PARTICIPANT_ID_TO_INT()].iMyUwTeam > -1
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[playerBD[PARTICIPANT_ID_TO_INT()].iMyUwTeam])
								IF NOT IS_PED_INJURED(PLAYER_PED_ID())
									IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[playerBD[PARTICIPANT_ID_TO_INT()].iMyUwTeam])
										IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh[playerBD[PARTICIPANT_ID_TO_INT()].iMyUwTeam]))
									//		Print_Objective_Text("UW_EXPL") // Exit the Urban Warfare vehicle before it explodes.
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneReward)
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneReward)
			PRINTLN("     ---------->     KILL LIST (C) -  CHECK_FOR_REWARD SET biP_DoneReward I DIDN'T TAKE PART")
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Controls the ped blips displaying for when they shoot
PROC CONTROL_PED_LOOP(BOOL bOnMission = TRUE)
	INT i
	INT iThisHydra
	REPEAT MAX_MW_PEDS i
		
		IF bOnMission
			UPDATE_ENEMY_NET_PED_BLIP(serverBD.niMwPed[i], MwPedBlipData[i], DEFAULT, DEFAULT, TRUE, FALSE)
		ELSE
			UPDATE_ENEMY_NET_PED_BLIP(serverBD.niMwPed[i], MwPedBlipData[i], DEFAULT, DEFAULT, TRUE, FALSE, DEFAULT,BLIP_COLOUR_GREY)
		ENDIF
		
		IF DOES_BLIP_EXIST(MwPedBlipData[i].blipId)
			SET_BLIP_NAME_FROM_TEXT_FILE(MwPedBlipData[i].blipId, "UW_BLIP2")
			IF bOnMission
			//	SET_BLIP_COLOUR(MwPedBlipData[i].blipId, BLIP_COLOUR_RED)
			ELSE
			//	SET_BLIP_COLOUR(MwPedBlipData[i].blipId, BLIP_COLOUR_GREY)
				SET_BLIP_AS_SHORT_RANGE(MwPedBlipData[i].blipId, TRUE)
			ENDIF
		ENDIF
		
		IF DOES_BLIP_EXIST(MwPedBlipData[i].VehicleBlipID)
			SET_BLIP_NAME_FROM_TEXT_FILE(MwPedBlipData[i].VehicleBlipID, "UW_BLIP2")
			IF bOnMission
			//	SET_BLIP_COLOUR(MwPedBlipData[i].VehicleBlipID, BLIP_COLOUR_RED)
			ELSE
			//	SET_BLIP_COLOUR(MwPedBlipData[i].VehicleBlipID, BLIP_COLOUR_GREY)
				SET_BLIP_AS_SHORT_RANGE(MwPedBlipData[i].VehicleBlipID, TRUE)
			ENDIF
		ENDIF
		
		IF serverBD.vehModel <> HYDRA
			IF IS_BIT_SET(serverBD.iServerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i))
			OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetPedKilledBitset[GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i) )
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niMwPed[i])
					CLEANUP_AI_PED_BLIP(MwPedBlipData[i])
				ENDIF
			ENDIF
		ELSE
			//-- If we're about to clean up a hyrda, remove the blip
			IF i % 4 = 0 // hydra pilots are peds 0, 4, 8 etc
				iThisHydra = i / 4
				IF IS_BIT_SET(serverBD.iTargetVehDestroyedBitSet, iThisHydra)
				OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerDestVehBitSet, iThisHydra)
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niMwPed[i])
						CLEANUP_AI_PED_BLIP(MwPedBlipData[i])
						PRINTLN("     ---------->     KILL LIST [CONTROL_PED_LOOP] CLEANUP_AI_PED_BLIP(NET_TO_PED(serverBD.niMwPed[i]), FALSE) as  hydra and destroy veh bit set ped ", i)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC REMOVE_AI_BLIPS()
	INT i
	REPEAT MAX_MW_PEDS i
		IF DOES_BLIP_EXIST(MwPedBlipData[i].blipId)
			REMOVE_BLIP(MwPedBlipData[i].blipId)
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"---------->     KILL LIST Removing ped blip ", i)
		ENDIF
		
		IF DOES_BLIP_EXIST(MwPedBlipData[i].VehicleBlipID)
			REMOVE_BLIP(MwPedBlipData[i].VehicleBlipID)
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"---------->     KILL LIST Removing veh blip ", i)
		ENDIF
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niMwPed[i])
			IF NOT IS_NET_PED_INJURED(serverBD.niMwPed[i])
				IF DOES_PED_HAVE_AI_BLIP(NET_TO_PED(serverBD.niMwPed[i]))
					//SET_PED_HAS_AI_BLIP(NET_TO_PED(serverBD.niMwPed[i]), FALSE)
					CLEANUP_AI_PED_BLIP(MwPedBlipData[i])
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"---------->     KILL LIST CLEANUP_AI_PED_BLIP ", i)
				ENDIF
			ENDIF
		ENDIF

	ENDREPEAT
	
ENDPROC




/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MISSION PROCS                  //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE:
///    Start a countdown when at least one player is in the vehicle. Mission will automatically start when timer ends
PROC MAINTAIN_START_MISSION_TIMER_SERVER()
	BOOL bTimerShouldStart
	
	IF NOT IS_COMPETITIVE_UW()
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.niVeh[0])
			IF GET_ENTITY_MODEL(NET_TO_VEH(serverbd.niVeh[0])) = VALKYRIE
				bTimerShouldStart = (serverBD.iAtLeastTwoPlayersBitSet <> 0) //IS_BIT_SET(serverbd.iServerBitSet, biS_AtLeastTwoPlayersInVeh)
			ELSE
				bTimerShouldStart = (serverBD.iAtLeastOnePlayerBitSet <> 0) // IS_BIT_SET(serverbd.iServerBitSet, biS_AtLeastOnePlayerInVeh)
			ENDIF
		ENDIF
	ELSE
		bTimerShouldStart = IS_BIT_SET(serverBD.iServerBitSet, biS_MinNumberOfPlayersInVehs)
	ENDIF
	
	IF bTimerShouldStart //IS_BIT_SET(serverbd.iServerBitSet, biS_AtLeastOnePlayerInVeh)
		IF serverbd.iStartTimeLimit = -1
			serverbd.iStartTimeLimit = GET_WAIT_FOR_OTHER_PLAYERS_TIME()
		ENDIF
		
		IF NOT HAS_NET_TIMER_STARTED(serverbd.timeMissionStart)
			START_NET_TIMER(serverbd.timeMissionStart)
			NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  START timeMissionStart    <----------     ") NET_NL()
		ELSE
			IF NOT IS_BIT_SET(serverbd.iServerBitSet, biS_MissionStartExpired)
				IF (serverbd.iStartTimeLimit -GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeMissionStart)) < 0
					
					SET_BIT(serverbd.iServerBitSet, biS_MissionStartExpired)
					NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  MISSION START TIME EXPIRED (2)    <----------     ") NET_NL()
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF HAS_NET_TIMER_STARTED(serverbd.timeMissionStart)
			RESET_NET_TIMER(serverbd.timeMissionStart)
			serverbd.iStartTimeLimit  = (serverbd.iStartTimeLimit - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeMissionStart))
			PRINTLN("     ---------->     KILL LIST - RESET timeMissionStart -serverbd.iStartTimeLimit = ", serverbd.iStartTimeLimit)
			
		ENDIF
	ENDIF
ENDPROC



PROC MAINTAIN_START_MISSION_TIMER_CLIENT()

	BOOL bTimerShouldStart = SHOULD_MISSION_START_TIMER_START()
	
	
	
	IF bTimerShouldStart //IS_BIT_SET(serverbd.iServerBitSet, biS_AtLeastOnePlayerInVeh)
//		IF NOT FM_EVENT_IS_PLAYER_WAITING_FOR_FM_EVENT_TO_START(PLAYER_ID())
//			FM_EVENT_SET_PLAYER_WAITING_FOR_FM_EVENT_TO_START(TRUE)
//		ENDIF
		IF serverbd.iStartTimeLimit > -1
			
			
			IF (serverbd.iStartTimeLimit-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeMissionStart)) >= 0
				IF NOT SHOULD_UI_BE_HIDDEN()
					HANDLE_AMBIENT_EVENT_START_COUNTDOWN_AUDIO((serverbd.iStartTimeLimit-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeMissionStart)))
					BOTTOM_RIGHT_UI_STARTING_EVENT_TIMER((serverbd.iStartTimeLimit-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeMissionStart)), GET_START_TIMER_NAME_FOR_EVENT())
				ENDIF
				

			ELSE
				IF NOT SHOULD_UI_BE_HIDDEN()
					BOTTOM_RIGHT_UI_STARTING_EVENT_TIMER(0, GET_START_TIMER_NAME_FOR_EVENT())
				ENDIF
				IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
					SET_BIT(serverbd.iServerBitSet, biS_MissionStartExpired)
					NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  MISSION START TIME EXPIRED    <----------     ") NET_NL()
				ENDIF
			ENDIF
			
//			IF (serverbd.iStartTimeLimit-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeMissionStart)) < 2
//				STOP_PLAYER_LEAVING_VEHICLE_THIS_FRAME()
//			ENDIF
		ENDIF
	ELSE
//		IF FM_EVENT_IS_PLAYER_WAITING_FOR_FM_EVENT_TO_START(PLAYER_ID())
//			FM_EVENT_SET_PLAYER_WAITING_FOR_FM_EVENT_TO_START(FALSE)
//		ENDIF
	ENDIF		
ENDPROC

PROC MAINTAIN_MISSION_DURATION_TIMER_SERVER()
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_CreatedAtLeastOneVeh)
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST  - [MAINTAIN_MISSION_DURATION_TIMER_SERVER] WAITING FOR biS_CreatedAtLeastOneVeh")
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_DurationExpired)
		IF NOT HAS_NET_TIMER_STARTED(serverbd.timeDuration)
			START_NET_TIMER(serverbd.timeDuration)
			SET_BIT(serverBD.iServerBitSet, biS_DurationStarted)
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST  - [MAINTAIN_MISSION_DURATION_TIMER_SERVER] STARTED timeDuration ")
		ELSE
			IF HAS_NET_TIMER_EXPIRED(serverbd.timeDuration, GET_EVENT_DURATION())
				SET_BIT(serverBD.iServerBitSet, biS_DurationExpired)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST  - [MAINTAIN_MISSION_DURATION_TIMER_SERVER] SET biS_DurationExpired ")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_MISSION_DURATION_TIMER_CLIENT()
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_DurationStarted)
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_DurationExpired)
			IF (GET_EVENT_DURATION()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeDuration)) > 10000
				SET_MIDDLE_TITLE_POSITION_HUD_THIS_FRAME()
				DRAW_GENERIC_TIMER((GET_EVENT_DURATION()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeDuration)), "UW_DUR", 0, TIMER_STYLE_USEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_DONTCARE, DEFAULT, HUD_COLOUR_WHITE)
			ELIF (GET_EVENT_DURATION()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeDuration)) >= 0
				
				
				SET_MIDDLE_TITLE_POSITION_HUD_THIS_FRAME()
				DRAW_GENERIC_TIMER((GET_EVENT_DURATION()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeDuration)), "UW_DUR", 0, TIMER_STYLE_USEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_DONTCARE, DEFAULT, HUD_COLOUR_RED, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_RED)
				
			ELSE
				SET_MIDDLE_TITLE_POSITION_HUD_THIS_FRAME()
				DRAW_GENERIC_TIMER(0, 
									"UW_DUR", 
									0, 
									TIMER_STYLE_USEMILLISECONDS, 
									DEFAULT, 
									DEFAULT, 
									HUDORDER_DONTCARE, 
									DEFAULT, 
									HUD_COLOUR_RED,
									DEFAULT,
									DEFAULT,
									DEFAULT,
									HUD_COLOUR_RED)
				
				
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_NUMBER_OF_PLAYER_VEHS()
	IF NOT IS_COMPETITIVE_UW()
		EXIT
	ENDIF
	
	INT i
	INT iVehCount
	INT iVehOk
	
	REPEAT MAX_PLAYER_VEH i	
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
			IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[i])
				iVehCount++
				iVehOk = i
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF iVehCount <  serverBD.iNumPlayerVeh
		serverBD.iNumPlayerVeh = iVehCount
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST  - [MAINTAIN_NUMBER_OF_PLAYER_VEHS] NUMBER OF PLAYER VEHICLES DECREASED! NEW VALUE = ", serverBD.iNumPlayerVeh)
	ENDIF
	
	IF serverBD.iNumPlayerVeh <= 1
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST  - [MAINTAIN_NUMBER_OF_PLAYER_VEHS] NO LONGER RUNNING COMPETITIVE VARIATION AS NUMBER OF PLAYER VEHICLES = ", serverBD.iNumPlayerVeh)
		CLEAR_BIT(serverBD.iServerBitSet, biS_Competitive)
		
		IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[0])
			IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[iVehOk])
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST  - [MAINTAIN_NUMBER_OF_PLAYER_VEHS] CLEANING UP VEH 0, THINK THIS VEH IS OK ", iVehOk)
				CLEANUP_NET_ID(serverBD.niVeh[0])
				serverBD.niVeh[0] = serverBD.niVeh[iVehOk]
			//	CLEANUP_NET_ID(serverBD.niVeh[iVehOk])
			ENDIF
		ENDIF
	ENDIF
	
		
ENDPROC

//FUNC BOOL SHOULD_TIMER_RESET()
//
////	IF HAS_NET_TIMER_STARTED(serverbd.timeMissionStart)
////		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST  - [SHOULD_TIMER_RESET] RETURN TRUE")
////		RETURN TRUE
////	ENDIF
////	
////	
////	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST  - [SHOULD_TIMER_RESET] RETURN FALSE")
////	RETURN FALSE
//
////	IF serverBD.iAtLeastOnePlayerBitSet = 0
////		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST  - [SHOULD_TIMER_RESET] serverBD.iAtLeastOnePlayerBitSet = 0 - RETURN FALSE")
////		RETURN FALSE
////	ENDIF
////	
////	IF IS_COMPETITIVE_UW()
////		IF serverBD.VehModel != VALKYRIE
////			IF serverBD.iAtLeastTwoPlayersBitSet <> 0
////				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST  - [SHOULD_TIMER_RESET] iAtLeastTwoPlayersBitSet <> 0 - RETURN TRUE")
////				RETURN TRUE
////			ENDIF
////		ELSE
////			INT i
////			INT iPlayerInVeh
////			REPEAT MAX_PLAYER_VEH  i
////				IF bMinReqNumPlayersInVeh[i]
////					iPlayerInVeh++
////				ENDIF
////			ENDREPEAT
////			
////			IF iPlayerInVeh >= 2
////				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST  - [SHOULD_TIMER_RESET] iPlayerInVeh: ", iPlayerInVeh, " - RETURN TRUE")
////				RETURN TRUE
////			ENDIF
////			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST  - [SHOULD_TIMER_RESET] iPlayerInVeh: ", iPlayerInVeh, " - RETURN FALSE")
////		ENDIF
////	ELSE
////		IF serverBD.VehModel != VALKYRIE
////			IF serverBD.iAtLeastOnePlayerBitSet <> 0
////				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST  - [SHOULD_TIMER_RESET] iAtLeastOnePlayerBitSet <> 0 - RETURN TRUE")
////				RETURN TRUE
////			ENDIF
////		ELSE
////			IF serverBD.iAtLeastTwoPlayersBitSet <> 0
////				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST  - [SHOULD_TIMER_RESET] iAtLeastTwoPlayersBitSet <> 0 - RETURN TRUE")
////				RETURN TRUE
////			ENDIF
////		ENDIF
////	ENDIF
////	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST  - [SHOULD_TIMER_RESET] RETURN FALSE")
////	RETURN FALSE
//ENDFUNC

PROC DRAW_SCORES()
	INT iNumScoresToShow = GET_NUMBER_OF_UW_PLAYER_VEHICLES()
	INT i
	HUD_COLOURS scoreColour
	HUDORDER position 
	INT iPosition = ENUM_TO_INT(HUDORDER_FOURTHBOTTOM)
	iPosition += iNumScoresToShow
	position = INT_TO_ENUM(HUDORDER, iPosition)
	
	
	INT iScore
	TEXT_LABEL_15 sTitle
	
	#IF IS_DEBUG_BUILD
		IF bWdDoScoreDebug
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     -----> KILL LIST [DRAW_SCORES] iNumScoresToShow = ", iNumScoresToShow)
		ENDIF
	#ENDIF
	
	
	REPEAT iNumScoresToShow i //  
		//-- My vehicle?
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet, i)
			scoreColour = HUD_COLOUR_BLUE
		ELSE
			scoreColour = HUD_COLOUR_RED
		ENDIF
		
		iScore = serverBD.iKillsPerVehicle[i]
		
		sTitle = "UW_TM"
		sTitle += (i+ 1)
		
		//DRAW_GENERIC_SCORE(INT Number, STRING NumberTitle, INT iFlashingTime = -1, HUD_COLOURS aColour = HUD_COLOUR_WHITE, HUDORDER WhichOrder = HUDORDER_DONTCARE, BOOL isPlayer = FALSE, STRING NumberString = NULL, BOOL isFloat = FALSE, FLOAT FloatValue = 0.0, HUDFLASHING ColourFlashType = HUDFLASHING_NONE, INT ColourFlash = 0, HUD_COLOURS TitleColour = HUD_COLOUR_PURE_WHITE
		DRAW_GENERIC_SCORE(iScore, sTitle, DEFAULT, scoreColour, position, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, scoreColour)
		
		iPosition--
		
		IF iPosition < ENUM_TO_INT(HUDORDER_FOURTHBOTTOM)
			iPosition = ENUM_TO_INT(HUDORDER_FOURTHBOTTOM)
		ENDIF
		
		position = INT_TO_ENUM(HUDORDER, iPosition)
	ENDREPEAT
	
	
ENDPROC

PROC MAINTAIN_UW_MUSIC()
	
	IF NOT IS_BIT_SET(iBoolsBitSet, biDoneMusic)
		IF serverBD.eStage = eAD_ATTACK
			IF playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet <> 0
				IF TRIGGER_MUSIC_EVENT("KILL_LIST_START_MUSIC")
					SET_BIT(iBoolsBitSet, biDoneMusic)
					NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  MAINTAIN_UW_MUSIC - MP_MC_ACTION    <----------     ") NET_NL()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(iBoolsBitSet, biResetMusic)
		IF IS_BIT_SET(iBoolsBitSet, biDoneMusic)
			IF playerBD[PARTICIPANT_ID_TO_INT()].eStage > eAD_ATTACK
				IF playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet <> 0
					IF IS_PED_INJURED(PLAYER_PED_ID())
						IF TRIGGER_MUSIC_EVENT("MP_MC_FAIL")
							SET_BIT(iBoolsBitSet, biResetMusic)						
							NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  MAINTAIN_UW_MUSIC - MP_MC_FAIL    <----------     ") NET_NL()
						ENDIF
					ELSE
						IF TRIGGER_MUSIC_EVENT("KILL_LIST_STOP_MUSIC")
							SET_BIT(iBoolsBitSet, biResetMusic)						
							NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  MAINTAIN_UW_MUSIC - KILL_LIST_STOP_MUSIC    <----------     ") NET_NL()
							KILL_AMBIENT_EVENT_COUNTDOWN_AUDIO()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DRAW_KILLS_BAR()
	//DRAW_GENERIC_BIG_DOUBLE_NUMBER((serverBD.iKillGoal-serverBD.iTotalKills), serverBD.iKillGoal, "GHO_KILLB")
	INT iCurrentKills
	INT i
	INT iEventTime = GET_EVENT_DURATION()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeDuration)
	
	
	//-- Freeze the time if the end shard is displaying (2423411)
	IF iStoredTimeRemaining = 0
		IF IS_BIT_SET(iBoolsBitSet, biWaitForShard)
			IF (IS_BIT_SET(serverBD.iServerBitSet, biS_AllVehiclesDestroyed)
			AND IS_BIT_SET(serverBD.iServerBitSet, biS_AllVehiclePedsDead)
			AND IS_BIT_SET(serverBD.iServerBitSet, biS_AllOnFootPedsDead))
				iStoredTimeRemaining  = GET_EVENT_DURATION()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeDuration)
				iEventTime = iStoredTimeRemaining 
				CPRINTLN(DEBUG_NET_AMBIENT, "     ---------->     KILL LIST FREEZEING TIME iEventTime = ", iEventTime)

				
			ENDIF
		ENDIF
	ELSE
		iEventTime = iStoredTimeRemaining
	ENDIF
	
	
	HUD_COLOURS eColour
	STRING sGamerTag[MAX_PLAYER_VEH]
	BOOL bUpdateLB	
	
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_IamOOB)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ApproachingOOB)
		EXIT
	ENDIF
	
	IF playerBD[PARTICIPANT_ID_TO_INT()].iVehTooFewPlayers >= 0
		EXIT
	ENDIF
	
	IF IS_COMPETITIVE_UW()
		iCurrentKills = serverBD.iKillsPerVehicle[0] + serverBD.iKillsPerVehicle[1] + serverBD.iKillsPerVehicle[2] + serverBD.iKillsPerVehicle[3]
	ELSE
//		IF iLocalKills <> serverBD.iTotalKills
//			iLocalKills = serverBD.iTotalKills
//			bUpdateLB = TRUE
//		ENDIF
//		
//		iCurrentKills = serverBD.iTotalKills

		iCurrentKills = serverBD.iKillsPerVehicle[0]
	ENDIF
	
	HANDLE_AMBIENT_EVENT_COUNTDOWN_AUDIO(iEventTime)
	
	IF iEventTime > 30000
		eColour = HUD_COLOUR_WHITE
	ELSE
		eColour = HUD_COLOUR_RED
	ENDIF
	
	
	INT iScore[MAX_PLAYER_VEH]
	INT iTeam[MAX_PLAYER_VEH]
	INT iBlue
	PLAYER_INDEX playerDriving
	IF IS_COMPETITIVE_UW()
		
		REPEAT MAX_PLAYER_VEH i
			playerDriving = INT_TO_PLAYERINDEX(serverBD.sLeaderboardDataTeams[i].iPlayer)
			IF playerDriving <> INVALID_PLAYER_INDEX()
				
				
				IF NETWORK_IS_PLAYER_ACTIVE(playerDriving)
					sGamerTag[i] = GET_PLAYER_NAME(playerDriving)
					iScore[i] = serverBD.sLeaderboardDataTeams[i].iKills //serverBD.iKillsPerVehicle[i]
					iTeam[i] = serverBD.sLeaderboardDataTeams[i].iTeam
					IF playerBD[PARTICIPANT_ID_TO_INT()].iMyUwTeam = iTeam[i]
						iBlue = iTeam[i]
						iBlue += 1
					ENDIF
					
					iTeam[i] += 1
					
				ELSE
					sGamerTag[i] = ""
					iScore[i] = -1
				ENDIF
			ELSE
				sGamerTag[i] = ""
				iScore[i] = -1
			ENDIF
		ENDREPEAT
		
		IF (GET_EVENT_DURATION()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeDuration)) >= 0
			IF serverBD.VehModel = RHINO
			OR serverBD.VehModel = HYDRA
				
				BOTTOM_RIGHT_UI_GAMERTAG_COMPETITIVE_ENEMIES_TIMER(sGamerTag[0], iTeam[0], 
																	sGamerTag[1], iTeam[1], 
																	sGamerTag[2],iTeam[2], 
																	sGamerTag[3], iTeam[3],
																	iScore[0], iScore[1], iScore[2], iScore[3], 
																	iEventTime,
																	eColour, 
																	GET_COUNTDOWN_TIMER_NAME_FOR_EVENT(), 
																	iBlue)
			ELSE
				
				BOTTOM_RIGHT_UI_VEHTEAM_COMPETITIVE_ENEMIES_TIMER(iScore[0], iTeam[0], iScore[1], iTeam[1], iScore[2], iTeam[2],  iScore[3], iTeam[3],  iEventTime, eColour, GET_COUNTDOWN_TIMER_NAME_FOR_EVENT(), iBlue)
			ENDIF
		ENDIF
	ELSE
		IF iEventTime >= 0
			IF iCurrentKills > serverBD.iKillGoal
				iCurrentKills = serverBD.iKillGoal
			ENDIF
			IF serverBD.VehModel = RHINO
			OR serverBD.VehModel = HYDRA
				BOTTOM_RIGHT_UI_GAMERTAG_ENEMIES_TIMER(GET_PLAYER_NAME(PLAYER_ID()), iCurrentKills, iCurrentKills, serverBD.iKillGoal, iEventTime, eColour, DEFAULT, GET_COUNTDOWN_TIMER_NAME_FOR_EVENT())
			ELSE
				BOTTOM_RIGHT_UI_VEHTEAM_ENEMIES_TIMER(iCurrentKills, iCurrentKills, serverBD.iKillGoal, iEventTime, eColour, DEFAULT, GET_COUNTDOWN_TIMER_NAME_FOR_EVENT())
			ENDIF
			
			IF bUpdateLB
				UPDATE_KILL_LIST_LEADERBOARD()
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC MAINTAIN_KILL_LIST_NO_LONGER_AVAILABLE()
	IF NOT IS_BIT_SET(iBoolsBitSet, biDoneNotAvailHelp)
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_DurationExpired)
			IF NOT IS_BIT_SET(iBoolsBitSet,biCompletedOK)
				IF FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID()) <> FMMC_TYPE_KILL_LIST
					IF playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet <> 0
						IF NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_FM_EVENT_FAIL)
						AND NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_FM_EVENT_FINISHED)
							IF IS_BIT_SET(iBoolsBitSet2, bi2_DoPlayerMightSTillWin)
								IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_DonePlayerMightSTillW)
									IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
										IF IS_COMPETITIVE_UW()
											PRINT_HELP_NO_SOUND("UW_HELIMC") //Although you were eliminated from Kill List Competitive and are unable to return to the vehicle, your team could still win and claim the reward.
										ELSE
											PRINT_HELP_NO_SOUND("UW_HELIM")
										ENDIF
										SET_FREEMODE_EVENT_HELP_BACKGROUND()
										SET_BIT(iBoolsBitSet2, bi2_DonePlayerMightSTillW)
										PRINTLN("     ---------->     KILL LIST - [MAINTAIN_OFF_KILL_LIST_SHARD] - SET bi2_DonePlayerMightSTillW")
									ELSE
										PRINTLN("     ---------->     KILL LIST - IS_OK_TO_PRINT_FREEMODE_HELP FALSE bi2_DonePlayerMightSTillW")
									ENDIF
								ENDIF
							ELSE
								IF NOT (serverBD.VehModel = RHINO OR serverBD.VehModel = HYDRA)
									IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
										IF NOT IS_COMPETITIVE_UW()
											PRINT_HELP_NO_SOUND("UW_TAVAIL", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) // Urban Warfare is no longer available
										ELSE
											PRINT_HELP_NO_SOUND("UW_TAVAILC", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
										ENDIF
										SET_FREEMODE_EVENT_HELP_BACKGROUND()
										SET_BIT(iBoolsBitSet, biDoneNotAvailHelp)
										PRINTLN("     ---------->     KILL LIST - Set biDoneNotAvailHelp ")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
			
ENDPROC

FUNC PLAYER_INDEX GET_NTH_PLAYER_IN_UW_PLAYER_VEHICLE(INT iPlayer, INT iVeh)
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [GET_NTH_PLAYER_IN_UW_PLAYER_VEHICLE] CALLED WITH iPlayer = ", iPlayer, " iVeh = ", iVeh)
	PLAYER_INDEX playerTemp = INVALID_PLAYER_INDEX()
	PED_INDEX pedTemp
	VEHICLE_SEAT vsToCheck = INT_TO_ENUM(VEHICLE_SEAT, (iPlayer-1))
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niVeh[iVeh])
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [GET_NTH_PLAYER_IN_UW_PLAYER_VEHICLE] EXISTS ")
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[iVeh])
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [GET_NTH_PLAYER_IN_UW_PLAYER_VEHICLE] DRIVABLE - CHECK SEAT ", ENUM_TO_INT(vsToCheck))
			pedTemp = GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(serverBD.niVeh[iVeh]), vsToCheck)
			IF pedTemp <> NULL
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [GET_NTH_PLAYER_IN_UW_PLAYER_VEHICLE] FOUND PED ")
				IF IS_PED_A_PLAYER(pedTemp)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [GET_NTH_PLAYER_IN_UW_PLAYER_VEHICLE] FOUND PLAYER ")
					playerTemp = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedTemp)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN playerTemp		
ENDFUNC

FUNC BOOL CAN_MY_TEAM_STILL_WIN()
	BOOL bMyVehStiilAlive
	//INT iTeam
	INT i
	INT iTotalKillsSoFar
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[iMyUwPlayerVeh])
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[iMyUwPlayerVeh])
			bMyVehStiilAlive = TRUE
		ENDIF
	ENDIF
	
	REPEAT MAX_PLAYER_VEH i
		PRINTLN("     ---------->     KILL LIST - [MAINTAIN_OFF_KILL_LIST_SHARD] [CHECK_FOR_REWARD] [CAN_MY_TEAM_STILL_WIN] serverBD.iKillsPerVehicle[",i, "] = ", serverBD.iKillsPerVehicle[i])
		IF serverBD.iKillsPerVehicle[i] > 0
			iTotalKillsSoFar++
		ENDIF
	ENDREPEAT
	
	INT iMyTeamScore = serverBD.iKillsPerVehicle[iMyUwPlayerVeh]
	

	
	PRINTLN("     ---------->     KILL LIST - [MAINTAIN_OFF_KILL_LIST_SHARD] [CHECK_FOR_REWARD] [CAN_MY_TEAM_STILL_WIN] iMyUwPlayerVeh = ", iMyUwPlayerVeh , " bMyVehStiilAlive = ", bMyVehStiilAlive, " iMyTeamScore = ", iMyTeamScore, " Team in first place = ", serverBD.sLeaderboardData[0].iTeam) 
	PRINTLN("     ---------->     KILL LIST - [MAINTAIN_OFF_KILL_LIST_SHARD] [CHECK_FOR_REWARD] [CAN_MY_TEAM_STILL_WIN] iTotalKillsSoFar = ", iTotalKillsSoFar, " iKillGoal = ", serverBD.iKillGoal)
	
	IF NOT bMyVehStiilAlive
		IF serverBD.sLeaderboardDataTeams[0].iTeam <> iMyUwPlayerVeh
			PRINTLN("     ---------->     KILL LIST - [MAINTAIN_OFF_KILL_LIST_SHARD] [CHECK_FOR_REWARD] [CAN_MY_TEAM_STILL_WIN] FALSE AS my veh dead and serverBD.sLeaderboardDataTeams[0].iTeam = ", serverBD.sLeaderboardDataTeams[0].iTeam, " <> iMyUwPlayerVeh = ", iMyUwPlayerVeh) 
			RETURN FALSE
		ELSE
			IF iMyTeamScore > 0
				PRINTLN("     ---------->     KILL LIST - [MAINTAIN_OFF_KILL_LIST_SHARD] [CHECK_FOR_REWARD] [CAN_MY_TEAM_STILL_WIN] TRUE AS my veh dead and serverBD.sLeaderboardDataTeams[0].iTeam = ", serverBD.sLeaderboardDataTeams[0].iTeam, " = iMyUwPlayerVeh = ", iMyUwPlayerVeh) 
				RETURN TRUE
			ELSE
				PRINTLN("     ---------->     KILL LIST - [MAINTAIN_OFF_KILL_LIST_SHARD] [CHECK_FOR_REWARD] [CAN_MY_TEAM_STILL_WIN] FALSE AS my veh dead AND iMyTeamScore = ", iMyTeamScore)
				RETURN FALSE
			ENDIF
		ENDIF
	ELSE
		IF serverBD.sLeaderboardDataTeams[0].iTeam = iMyUwPlayerVeh
			PRINTLN("     ---------->     KILL LIST - [MAINTAIN_OFF_KILL_LIST_SHARD] [CHECK_FOR_REWARD] [CAN_MY_TEAM_STILL_WIN] TRUE AS my veh OK and serverBD.sLeaderboardDataTeams[0].iTeam = ", serverBD.sLeaderboardDataTeams[0].iTeam, " = iMyUwPlayerVeh = ", iMyUwPlayerVeh) 
			RETURN TRUE
		ELSE
			INT iTopTeamScore = serverBD.iKillsPerVehicle[serverBD.sLeaderboardDataTeams[0].iTeam]
			INT iDiffFromTopScore = iTopTeamScore  - iMyTeamScore
			INT iPossibleKillsRemaining = serverBD.iKillGoal - iTotalKillsSoFar
			
			PRINTLN("     ---------->     KILL LIST - [MAINTAIN_OFF_KILL_LIST_SHARD] [CHECK_FOR_REWARD] [CAN_MY_TEAM_STILL_WIN] MY VEH OK iTopTeamScore = ", iTopTeamScore, " iMyTeamScore = ", iMyTeamScore, " iDiffFromTopScore = ", iDiffFromTopScore,  " iPossibleKillsRemaining = ", iPossibleKillsRemaining) 
			IF iDiffFromTopScore > iPossibleKillsRemaining
				PRINTLN("     ---------->     KILL LIST - [MAINTAIN_OFF_KILL_LIST_SHARD] [CHECK_FOR_REWARD] [CAN_MY_TEAM_STILL_WIN] FALSE AS DiffFromTopScore = ", iDiffFromTopScore, " > iPossibleKillsRemaining = ", iPossibleKillsRemaining )
				RETURN FALSE
			ELSE
				PRINTLN("     ---------->     KILL LIST - [MAINTAIN_OFF_KILL_LIST_SHARD] [CHECK_FOR_REWARD] [CAN_MY_TEAM_STILL_WIN] TRU AS DiffFromTopScore = ", iDiffFromTopScore, " <= iPossibleKillsRemaining = ", iPossibleKillsRemaining )
				RETURN TRUE
			ENDIF
			
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


PROC MAINTAIN_OFF_KILL_LIST_SHARD()
	IF IS_BIT_SET(iBoolsBitSet,biCompletedOK)
		EXIT
	ENDIF
	

	BOOL bExpired 	= IS_BIT_SET(serverBD.iServerBitSet, biS_DurationExpired) // Mission duration expired?
	BOOL bDied		= IS_BIT_SET(iBoolsBitSet, biDiedDuringEvent) // Player died during event?
	BOOL bWrecked 	= IS_BIT_SET(iBoolsBitSet, biVehDestroyed) // Player vehicle wrecked during event?
	BOOL bAbandoned = playerBD[PARTICIPANT_ID_TO_INT()].iVehTooFewPlayers <> -1	
	
	IF bAbandoned
		bDied = FALSE
		bWrecked = FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_DOING_KILL_LIST()
//		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneReward)
		IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_DoneOffMissionShard)
	
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				OR bWrecked
					
					IF NOT SHOULD_UI_BE_HIDDEN()
						IF NOT IS_COMPETITIVE_UW()
							IF bExpired
								PRINTLN("     ---------->     KILL LIST - [MAINTAIN_OFF_KILL_LIST_SHARD] [CHECK_FOR_REWARD] bExpired")
								SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_FAIL,"UW_BIGF","UW_FAILNX", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME) // You failed to destroy all the Merryweather patrols.
							ELIF bDied
								
								IF serverbd.vehmodel = RHINO
								OR serverbd.vehmodel = Hydra
									PRINTLN("     ---------->     KILL LIST - [MAINTAIN_OFF_KILL_LIST_SHARD] [CHECK_FOR_REWARD] bDied - Hydra / Rhino")
									SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_FAIL,"UW_BIGF","UW_LSELIM", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME) // you were eliminated
								ELSE
									IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])
									AND IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[0])
										SET_BIT(iBoolsBitSet2, bi2_DoPlayerMightSTillWin)
										SET_BIT(iBoolsBitSet2, bi2_USeStillDoEndShard)
										PRINTLN("     ---------->     KILL LIST - [MAINTAIN_OFF_KILL_LIST_SHARD] [CHECK_FOR_REWARD] bDied - NOT Hydra / Rhino, veh still alive - set bi2_DoPlayerMightSTillWin")
										SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_FINISHED,"UW_BIGE","UW_LSELIM", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME) 
									ELSE
										PRINTLN("     ---------->     KILL LIST - [MAINTAIN_OFF_KILL_LIST_SHARD] [CHECK_FOR_REWARD] bDied - NOT Hydra / Rhino, veh NOT still alive")
										SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_FAIL,"UW_BIGF","UW_LSELIM", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME) 
									ENDIF
								ENDIF
							ELIF bWrecked
								PRINTLN("     ---------->     KILL LIST - [MAINTAIN_OFF_KILL_LIST_SHARD] [CHECK_FOR_REWARD] bWrecked")
								SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_FAIL,"UW_BIGF","UW_LSELIM", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME) 
							ELIF bAbandoned
							OR IS_BIT_SET(serverBD.iServerBitSet, biS_AllVehiclesEmpty)
								PRINTLN("     ---------->     KILL LIST - [MAINTAIN_OFF_KILL_LIST_SHARD] [CHECK_FOR_REWARD] bAbandoned")

								SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_FINISHED,"UW_BIGO","UW_ABAND", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME) 
								
							ENDIF
						ELSE
							IF bExpired
								PRINTLN("     ---------->     KILL LIST - [MAINTAIN_OFF_KILL_LIST_SHARD] [CHECK_FOR_REWARD] bExpired")
								SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_FAIL,"UW_BIGF","UW_FAILNX", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME) // You failed to destroy all the Merryweather patrols.
							ELIF bDied
								
								IF serverbd.vehmodel = RHINO
								OR serverbd.vehmodel = Hydra
									IF CAN_MY_TEAM_STILL_WIN()
										PRINTLN("     ---------->     KILL LIST - [MAINTAIN_OFF_KILL_LIST_SHARD] [CHECK_FOR_REWARD] bDied - Hydra / Rhino - Competitive - Can still win")
										SET_BIT(iBoolsBitSet2, bi2_DoPlayerMightSTillWin)
										SET_BIT(iBoolsBitSet2, bi2_USeStillDoEndShard)
										SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_FINISHED,"UW_BIGE","UW_LSELIMC", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME)
									ELSE
										PRINTLN("     ---------->     KILL LIST - [MAINTAIN_OFF_KILL_LIST_SHARD] [CHECK_FOR_REWARD] bDied - Hydra / Rhino - Competitive - Can't win")
										SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_FAIL,"UW_BIGF","UW_LSELIM", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME) // you were eliminated
									ENDIF
								ELSE
									IF CAN_MY_TEAM_STILL_WIN()
										PRINTLN("     ---------->     KILL LIST - [MAINTAIN_OFF_KILL_LIST_SHARD] [CHECK_FOR_REWARD] bDied - NOT Hydra / Rhino, veh MY TEAM CAN STILL WIN - SET bi2_DoPlayerMightSTillWin")
										SET_BIT(iBoolsBitSet2, bi2_DoPlayerMightSTillWin)
										SET_BIT(iBoolsBitSet2, bi2_USeStillDoEndShard)
										SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_FINISHED,"UW_BIGE","UW_LSELIMC", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME)
									ELSE
										PRINTLN("     ---------->     KILL LIST - [MAINTAIN_OFF_KILL_LIST_SHARD] [CHECK_FOR_REWARD] bDied - NOT Hydra / Rhino, veh MY TEAM CAN NOT STILL WIN")
										SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_FAIL,"UW_BIGF","UW_LSELIMC", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME)
									ENDIF
								ENDIF
							ELIF bWrecked
								IF CAN_MY_TEAM_STILL_WIN()
									PRINTLN("     ---------->     KILL LIST - [MAINTAIN_OFF_KILL_LIST_SHARD] [CHECK_FOR_REWARD] bWrecked -  veh MY TEAM CAN STILL WIN set bi2_DoPlayerMightSTillWin")
									SET_BIT(iBoolsBitSet2, bi2_DoPlayerMightSTillWin)
									SET_BIT(iBoolsBitSet2, bi2_USeStillDoEndShard)
									SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_FINISHED,"UW_BIGE","UW_LSELIMC", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME)
								ELSE
									PRINTLN("     ---------->     KILL LIST - [MAINTAIN_OFF_KILL_LIST_SHARD] [CHECK_FOR_REWARD] bWrecked -  veh MY TEAM CAN NOT STILL WIN")
									SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_FAIL,"UW_BIGF","UW_LSELIMC", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME)
								ENDIF
							ELIF bAbandoned
							OR IS_BIT_SET(serverBD.iServerBitSet, biS_AllVehiclesEmpty)
								PRINTLN("     ---------->     KILL LIST - [MAINTAIN_OFF_KILL_LIST_SHARD] [CHECK_FOR_REWARD] bAbandoned")

								SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_FINISHED,"UW_BIGO","UW_ABAND", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME) 

							ENDIF
						ENDIF
					ENDIF
					
					INT iCash = GET_PARTICIPATION_CASH()

					telemetryStruct.m_cashEarned += iCash
					IF !g_sMPTunables.bkill_list_disable_share_cash
						IF telemetryStruct.m_cashEarned > 0
							SET_LAST_JOB_DATA(LAST_JOB_URBAN_WARFARE, telemetryStruct.m_cashEarned)
						ENDIF
					ENDIF
					g_i_cashForEndEventShard = iCash
					IF iCash > 0
						IF USE_SERVER_TRANSACTIONS()
							INT iScriptTransactionIndex
							TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_AMBIENT_JOB_URBAN_WARFARE, iCash, iScriptTransactionIndex, FALSE, TRUE) //Add new service
						ELSE
							AMBIENT_JOB_DATA amData
							NETWORK_EARN_FROM_AMBIENT_JOB(iCash, "AM_KILL_LIST", amData)
						ENDIF
					ENDIF
					
					INT iRp = GET_PARTICIPATION_RP() 

					telemetryStruct.m_rpEarned +=iRp
					NEXT_RP_ADDITION_SHOW_RANKBAR()
					GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD,PLAYER_PED_ID(),"",XPTYPE_COMPLETE,XPCATEGORY_KILL_LIST_KILL, iRp)
					
					telemetryStruct.m_endReason       	=AE_END_LOST	
					
					PRINTLN("     ---------->     KILL LIST - [MAINTAIN_OFF_KILL_LIST_SHARD] [CHECK_FOR_REWARD] iCash = ", iCash, " iRp = ", iRp)
				//	SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_GENERIC_LOSER, "", "UW_FAIL", "UW_BIGO", DEFAULT, (30000/1000))
				//	SET_LOCAL_PLAYER_DOING_KILL_LIST(FALSE)
					SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PlayersDontDragMeOutOfCar, FALSE)
					
					IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_USeStillDoEndShard)
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneReward)
						PRINTLN("     ---------->     KILL LIST - [MAINTAIN_OFF_KILL_LIST_SHARD] [CHECK_FOR_REWARD] SET biP_DoneReward AS NOT DOING END SHARD")
					ELSE
						PRINTLN("     ---------->     KILL LIST - [MAINTAIN_OFF_KILL_LIST_SHARD] [CHECK_FOR_REWARD] NOT SETTING biP_DoneReward  DOING END SHARD")
					ENDIF
					
					SET_BIT(iBoolsBitSet2, bi2_DoneOffMissionShard)
					SET_BIT(iBoolsBitSet2, bi2_AlreadyDonePartCashAndRp)
					PRINTLN("     ---------->     KILL LIST - PCF_PlayersDontDragMeOutOfCar CLEARED 2")
					PRINTLN("     ---------->     KILL LIST - MAINTAIN_OFF_MISSION_TICKER -DONE TICKER")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	

		
ENDPROC


PROC MAINTAIN_TOO_MANY_PLAYERS_HID_EVENT_HELP()
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_TooFewValid)
		IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_NotEnoughValidP)
			IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
				IF IS_COMPETITIVE_UW()
					PRINT_HELP_NO_SOUND("UW_TFEWC", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) // Competitive Urban Warfare is no longer available as there aren't enough valid players.
				ELSE
					PRINT_HELP_NO_SOUND("UW_TFEW", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
				ENDIF
				SET_FREEMODE_EVENT_HELP_BACKGROUND()
				SET_BIT(iBoolsBitSet2, bi2_NotEnoughValidP)
			ELSE
				PRINTLN("     ---------->     KILL LIST - IS_OK_TO_PRINT_FREEMODE_HELP NOT OK bi2_NotEnoughValidP ")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_UW_VOICE_CHAT()
	INT iMyKills
	INT iMyVeh = -1
	
	IF serverBD.VehModel = HYDRA
	OR serverBD.VehModel = RHINO
		//--Only player, don't restrict
		EXIT
	ENDIF
	
	
	IF NOT IS_BIT_SET(iBoolsBitSet, biInitVoiceChat)
		IF playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_ATTACK
			GET_MY_VEHICLE_KILL_DETAILS(iMyVeh, iMyKills)
			IF DOES_MY_UW_VEH_HAVE_MORE_THAN_ONE_PLAYER(iMyVeh)
				IF iMyVeh > -1
					IF serverBD.playerDriving[iMyVeh] <> INVALID_PLAYER_INDEX()	
						fMyVoiceChatProx = NETWORK_GET_TALKER_PROXIMITY()
						NETWORK_SET_TALKER_PROXIMITY(-1.0)
						SET_UW_VOICE_CHAT_OVERRIDE(TRUE, iMyVeh) 
						SET_BIT(iBoolsBitSet, biInitVoiceChat)
						FM_EVENT_SET_VOICE_CHAT_DISABLED_FOR_PLAYER_FOR_KILL_LIST(TRUE)
						PRINTLN("     ---------->     KILL LIST - [MAINTAIN_UW_VOICE_CHAT] - SET biInitVoiceChat - MY VOICE CHAT PROX = ", fMyVoiceChatProx)
					ENDIF
				ENDIF
			ELSE
				SET_BIT(iBoolsBitSet, biInitVoiceChat)
				SET_BIT(iBoolsBitSet, biResetVoiceChat)
				PRINTLN("     ---------->     KILL LIST - [MAINTAIN_UW_VOICE_CHAT] - SET biInitVoiceChat I'm ONLY PLAYER")
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(iBoolsBitSet, biResetVoiceChat)
		IF IS_BIT_SET(iBoolsBitSet, biInitVoiceChat)
			IF playerBD[PARTICIPANT_ID_TO_INT()].eStage > eAD_ATTACK
				NETWORK_SET_TALKER_PROXIMITY(fMyVoiceChatProx)
				GET_MY_VEHICLE_KILL_DETAILS(iMyVeh, iMyKills)
		//		PRINTLN("     ---------->     KILL LIST - [MAINTAIN_UW_VOICE_CHAT] 3 iMyVeh = ", iMyVeh)
				IF iMyVeh > -1
			//		PRINTLN("     ---------->     KILL LIST - [MAINTAIN_UW_VOICE_CHAT] 4 iMyVeh = ", iMyVeh)	
					SET_UW_VOICE_CHAT_OVERRIDE(FALSE, iMyVeh) 
					SET_BIT(iBoolsBitSet, biResetVoiceChat)
					FM_EVENT_SET_VOICE_CHAT_DISABLED_FOR_PLAYER_FOR_KILL_LIST(FALSE)
					PRINTLN("     ---------->     KILL LIST - [MAINTAIN_UW_VOICE_CHAT] - SET biResetVoiceChat - MY VOICE CHAT PROX = ", fMyVoiceChatProx)
				ENDIF
			ENDIF

		ENDIF
	ENDIF
	
ENDPROC
PROC MAINTAIN_TICKERS()
	IF NETWORK_IS_IN_TUTORIAL_SESSION()
		EXIT
	ENDIF
	
	PLAYER_INDEX playerTemp
	INT i
	//-- Players who didn't taeke part get a ticker indicating that the event has started
	IF NOT IS_BIT_SET(iBoolsBitSet, biUWStartedTick)
		IF playerBD[PARTICIPANT_ID_TO_INT()].eStage	= eAD_OFF_MISSION
			IF playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet = 0
				IF serverBD.playerDriving[0] <> INVALID_PLAYER_INDEX()
					IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE, FALSE)
						IF NOT IS_COMPETITIVE_UW()
							IF serverBD.VehModel = RHINO
							OR serverBD.VehModel = HYDRA
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [MAINTAIN_TICKERS] DOING START TICKER 1P AS EITHER RHINO OR HYDRA")
								IF NOT IS_COMPETITIVE_UW()
									PRINT_HELP_WITH_PLAYER_NAME_NO_SOUND("UW_START1P", GET_PLAYER_NAME(serverBD.playerDriving[0]), HUD_COLOUR_WHITE, DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
							//		PRINT_TICKER_WITH_PLAYER_NAME_WITH_HUD_COLOUR("UW_START1P", serverBD.playerDriving[0], HUD_COLOUR_RED)
								ELSE
									PRINT_HELP_WITH_PLAYER_NAME_NO_SOUND("UW_START1PC", GET_PLAYER_NAME(serverBD.playerDriving[0]), HUD_COLOUR_WHITE, DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
							//		PRINT_TICKER_WITH_PLAYER_NAME_WITH_HUD_COLOUR("UW_START1PC", serverBD.playerDriving[0], HUD_COLOUR_RED)
								ENDIF
								SET_FREEMODE_EVENT_HELP_BACKGROUND()
							//	PRINT_HELP_WITH_PLAYER_NAME_NO_SOUND("UW_START1P", GET_PLAYER_NAME(serverBD.playerDriving[0]), HUD_COLOUR_RED)
							ELSE
								IF IS_BIT_SET(serverBD.iAtLeastTwoPlayersBitSet, 0)
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [MAINTAIN_TICKERS] DOING START TICKER - NOT IS_COMPETITIVE_UW - iAtLeastTwoPlayersBitSet IS SET")
									IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_EXITING_PROPERTY)
										IF NOT IS_COMPETITIVE_UW()
											//PRINT_TICKER("UW_STARTM") // Urban Warfare has started.
											PRINT_HELP_NO_SOUND("UW_STARTM", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
										ELSE
											//PRINT_TICKER("UW_STARTMC")
											PRINT_HELP_NO_SOUND("UW_STARTMC", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
										ENDIF
										SET_FREEMODE_EVENT_HELP_BACKGROUND()
									ENDIF
								ELSE
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [MAINTAIN_TICKERS] DOING START TICKER - NOT IS_COMPETITIVE_UW - iAtLeastTwoPlayersBitSet IS NOT SET")
								//	PRINT_TICKER_WITH_PLAYER_NAME_WITH_HUD_COLOUR("UW_START1P", serverBD.playerDriving[0], HUD_COLOUR_RED)
									PRINT_HELP_WITH_PLAYER_NAME_NO_SOUND("UW_START1P", GET_PLAYER_NAME(serverBD.playerDriving[0]), HUD_COLOUR_WHITE, DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
									SET_FREEMODE_EVENT_HELP_BACKGROUND()
								ENDIF
							ENDIF
						ELSE
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [MAINTAIN_TICKERS] DOING START TICKER - IS_COMPETITIVE_UW - MULTIPLE VEHICLES")
							IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_EXITING_PROPERTY)
								IF NOT IS_COMPETITIVE_UW()
								//	PRINT_TICKER("UW_STARTM") // Urban Warfare has started.
									PRINT_HELP_NO_SOUND("UW_STARTM", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
								ELSE
									//PRINT_TICKER("UW_STARTMC")
									PRINT_HELP_NO_SOUND("UW_STARTMC", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
								ENDIF
								SET_FREEMODE_EVENT_HELP_BACKGROUND()
							ENDIF
						ENDIF
						SET_BIT(iBoolsBitSet, biUWStartedTick)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//-- Ticker for my team vehicle being dsetriyed after being knocked out
	IF playerBD[PARTICIPANT_ID_TO_INT()].eStage	= eAD_OFF_MISSION
		IF serverBD.eStage = eAD_ATTACK
			IF serverBD.vehModel <> HYDRA
			AND serverBD.vehModel <> RHINO
				IF playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet <> 0
					IF iMyUwPlayerVeh >= 0
						IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_MyVehDestroyedAfterKO)
							IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_VehOkAfterKnockedOut)
								IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[iMyUwPlayerVeh])
									IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[iMyUwPlayerVeh])
										SET_BIT(iBoolsBitSet2, bi2_VehOkAfterKnockedOut)
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [MAINTAIN_TICKERS] SET bi2_VehOkAfterKnockedOut")
									ENDIF
								ENDIF
							ELSE
								IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[iMyUwPlayerVeh])
									IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[iMyUwPlayerVeh])
										PRINT_TICKER("UW_TEAMV") // Your team vehicle was destroyed.
										SET_BIT(iBoolsBitSet2, bi2_MyVehDestroyedAfterKO)
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [MAINTAIN_TICKERS] SET bi2_MyVehDestroyedAfterKO")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [MAINTAIN_TICKERS] VEHICLE KNOCKED OUT iMyUwPlayerVeh = ", iMyUwPlayerVeh)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
					
	
	i = 0
	//-- Player's who didn't take part get a ticker saying which players completed the event (max 4)
	IF NOT IS_COMPETITIVE_UW()
		REPEAT 4 i
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_DurationExpired)
				IF NOT IS_BIT_SET(iBoolsBitSet, biUWWinTick1+i)
					IF playerBD[PARTICIPANT_ID_TO_INT()].eStage	>= eAD_OFF_MISSION
						IF playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet = 0
							IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllVehiclesDestroyed)
							AND IS_BIT_SET(serverBD.iServerBitSet, biS_AllVehiclePedsDead)
							AND IS_BIT_SET(serverBD.iServerBitSet, biS_AllOnFootPedsDead)
								IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niVeh[0])
									IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[0])
										IF GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(serverBD.niVeh[0]), VS_DRIVER) <> NULL
											playerTemp = GET_NTH_PLAYER_IN_UW_PLAYER_VEHICLE(i, 0)
											IF playerTemp <> INVALID_PLAYER_INDEX()
												CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [MAINTAIN_TICKERS] DOING COMPLETE TICKER - ", i,"th PLAYER IN VEH IS ", GET_PLAYER_NAME(playerTemp))
												PRINT_TICKER_WITH_PLAYER_NAME_WITH_HUD_COLOUR("UW_COMP", playerTemp, HUD_COLOUR_WHITE) // completed Urban Warfare.
											ELSE
												CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [MAINTAIN_TICKERS] DOING COMPLETE TICKER - FAILED TO FIND ", i,"th PLAYER IN VEH ")
											ENDIF
											SET_BIT(iBoolsBitSet, biUWWinTick1+i)
										ELSE
											CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST [MAINTAIN_TICKERS] WAITING FOR GET_PED_IN_VEHICLE_SEAT") 
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF	
					ENDIF		
								
				ENDIF
			ENDIF
		ENDREPEAT
	ELSE
		
	ENDIF
ENDPROC

/// PURPOSE:
///    Called every frame during the start of UW to prevent the vehicles being moved.
PROC FIX_UW_VEH_POSITION()

	INT i
	REPEAT MAX_PLAYER_VEH i
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niVeh[i])
		AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niVeh[i])

			VEHICLE_INDEX vehIndex = NET_TO_VEH(serverBD.niVeh[i])
			
			// Confirm entity is ok
			IF DOES_ENTITY_EXIST(vehIndex)
			AND NOT IS_ENTITY_DEAD(vehIndex)
				SET_USE_KINEMATIC_PHYSICS(vehIndex, TRUE)
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC

/// PURPOSE:
///    When a vehicle is empty, lock it's doors and clean it up
PROC MAINTAIN_LOCK_VEHICLE_DOORS_WITH_NO_PLAYERS()
	INT i
	IF iLocalLockVehicleDoorsDueToNoPlayers <> serverBD.iLockVehicleDoorsDueToNoPlayers
		REPEAT MAX_PLAYER_VEH i
			IF IS_BIT_SET(serverBD.iLockVehicleDoorsDueToNoPlayers, i)
				IF NOT IS_BIT_SET(iLocalLockVehicleDoorsDueToNoPlayers, i)
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niVeh[i])
							SET_VEHICLE_DOORS_LOCKED(NET_TO_VEH(serverBD.niVeh[i]), VEHICLELOCK_LOCKED)
							CLEANUP_NET_ID(serverBD.niVeh[i])
							SET_BIT(iLocalLockVehicleDoorsDueToNoPlayers, i)
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	 "    ---------->     KILL LIST [MAINTAIN_LOCK_VEHICLE_DOORS_WITH_NO_PLAYERS] I HAVE CLEANED UP AND LOCKED VEH DOORS PLAYER VEH ", i)
						ENDIF
					ELSE
						SET_BIT(iLocalLockVehicleDoorsDueToNoPlayers, i)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT	
	ENDIF
ENDPROC

PROC MAINTAIN_CLIENT_PARTICIPANT_LOOP()

	PARTICIPANT_INDEX partID = INT_TO_PARTICIPANTINDEX(iStaggeredClientPart)
	IF NETWORK_IS_PARTICIPANT_ACTIVE(partID)
		PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX(partID)
		IF NETWORK_IS_PLAYER_ACTIVE(playerID)
			IF iStaggeredClientPart != PARTICIPANT_ID_TO_INT()
				IF NOT IS_BIT_SET(iActivePlayersGreyBlipBitset,  iStaggeredClientPart)
					IF NOT SHOULD_UI_BE_HIDDEN()	
						IF playerBD[iStaggeredClientPart].eStage = eAD_ATTACK
							SET_BIT(iActivePlayersGreyBlipBitset,  iStaggeredClientPart)
							SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(playerID,BLIP_COLOUR_GREY,TRUE)
						ENDIF
					ENDIF
				ELSE
					IF SHOULD_UI_BE_HIDDEN()
						SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(playerID,BLIP_COLOUR_GREY,FALSE)
						CLEAR_BIT(iActivePlayersGreyBlipBitset,  iStaggeredClientPart)
					ELSE
						IF playerBD[iStaggeredClientPart].eStage > eAD_ATTACK 
							CLEAR_BIT(iActivePlayersGreyBlipBitset,  iStaggeredClientPart)
							SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(playerID,BLIP_COLOUR_GREY,FALSE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	iStaggeredClientPart++
	
	IF iStaggeredClientPart >= NETWORK_GET_MAX_NUM_PARTICIPANTS()
		iStaggeredClientPart = 0
	ENDIF

ENDPROC

//PURPOSE: Process the Urban Warfare stages for the Client
PROC PROCESS_KILL_LIST_CLIENT()
	IF IS_PLAYER_SCTV(PLAYER_ID())
		EXIT
	ENDIF
	
	//TEXT_LABEL_15 tl15Veh
	TEXT_LABEL_15 tl15Team
	INT iTempVeh
	INT iTempKills
	
	SWITCH playerBD[PARTICIPANT_ID_TO_INT()].eStage		
		CASE eAD_IDLE
			IF playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet = 0
				IF serverBD.eStage = eAD_IDLE
					ADD_VEH_BLIP()
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(iBoolsBitSet, biDisabledCorona)
				IF serverBD.vehModel = RHINO
				AND serverBD.iLocation = 2
					Set_MissionsAtCoords_Local_Disabled_Area(serverBD.vStartLocation[0])
				
					SET_BIT(iBoolsBitSet, biDisabledCorona)
					PRINTLN("     ---------->     KILL LIST - SET biDisabledCorona at power plant <<2728.1499, 1422.9033, 23.4888>>")
				ELSE
					IF NOT ARE_VECTORS_EQUAL(serverBD.vStartLocation[0], << 0.0, 0.0, 0.0 >>)
						Set_MissionsAtCoords_Local_Disabled_Area(serverBD.vStartLocation[0])
					
						SET_BIT(iBoolsBitSet, biDisabledCorona)
						PRINTLN("     ---------->     KILL LIST - SET biDisabledCorona coords = ", serverBD.vStartLocation[0])
					ENDIF
				ENDIF
			ENDIF
			
			// Prevent vehicle from being moved during the setup of UW
			FIX_UW_VEH_POSITION()
			
			CONTROL_UW_VEH_BODY()
			
			IF serverBD.eStage = eAD_IDLE
				DO_HELP_TEXT(1)
			ENDIF
			
			SET_LEFT_PLAYERS_FLY_HELIS_INTO_SUNSET(FALSE) // 2420366: Prevent disconnected player peds flying off with UW vehicles
			
			//-- Wait for other players
			IF serverBD.eStage = eAD_IDLE
			//	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InVehicle)
				IF NOT (serverBD.VehModel = HYDRA)
				AND NOT (serverBD.VehModel = RHINO)
					MAINTAIN_START_MISSION_TIMER_CLIENT()
				ELIF IS_COMPETITIVE_UW()
					MAINTAIN_START_MISSION_TIMER_CLIENT()
				ENDIF
				IF playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet <> 0
					IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_SetFriendlyFire)
						SET_PLAYERS_CURRENT_FREEMODE_AMBIENT_EVENT_TYPE(FMMC_TYPE_KILL_LIST) // Needed for critical check to work
						FM_EVENT_SET_PLAYER_IS_CRITICAL_TO_FM_EVENT(TRUE)
						SET_BIT(iBoolsBitSet2, bi2_SetFriendlyFire)
						NETWORK_SET_FRIENDLY_FIRE_OPTION(FALSE)
						PRINTLN("     ---------->     KILL LIST -  CLIENT SET bi2_SetFriendlyFire")
					ENDIF
					
					
					IF NOT IS_BIT_SET(playerbd[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_MissionStartExpired)
						IF serverBD.iVehicleSeats > 1
						OR (serverBD.iVehicleSeats = 1 AND GET_NUMBER_OF_UW_PLAYER_VEHICLES() > 1)
							
													
							//-- Timer expired, clear biP_InVehicle so re-count can happen with fewer number of required players
							IF IS_BIT_SET(serverbd.iServerBitSet, biS_MissionStartExpired)
							//	IF NOT IS_BIT_SET(playerbd[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_MissionStartExpired)
							//		CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InVehicle)
									playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet = 0
									SET_BIT(playerbd[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_MissionStartExpired)
									NET_NL() NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  CLIENT SET biP_MissionStartExpired   <----------     ") NET_NL()
							//	ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF IS_BIT_SET(iBoolsBitSet2, bi2_SetFriendlyFire)
						CLEAR_BIT(iBoolsBitSet2, bi2_SetFriendlyFire)
						SET_PLAYER_DEFAULT_FRIENDLY_FIRE_OPTION()
						CLEAR_PLAYERS_CURRENT_FREEMODE_AMBIENT_EVENT_TYPE()
						FM_EVENT_SET_PLAYER_IS_CRITICAL_TO_FM_EVENT(FALSE)
						PRINTLN("     ---------->     KILL LIST -  CLIENT CLEAR bi2_SetFriendlyFire")
					ENDIF
				ENDIF
			ENDIF
			
			
			//Check if we should move on
			IF serverBD.eStage = eAD_ATTACK
			//	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InVehicle)
				GET_MY_VEHICLE_KILL_DETAILS(iTempVeh, iTempKills)
				iMyUwPlayerVeh = iTempVeh
				SET_MY_UW_VEHICLE_INVINCIBLE(FALSE)
				
				IF playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet <> 0
					IF NOT DOES_MY_VALKYRIE_HAVE_ENOUGH_PLAYERS()
						SET_BIT(iBoolsBitSet2, bi2_ValkStartNoGunner) //-- Different help text when failing due to not enough players in veh
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	 "    ---------->     KILL LIST SET bi2_ValkStartNoGunner")
					ENDIF
					
					IF Is_This_The_Current_Objective_Text("UW_ABTSC")
					OR Is_This_The_Current_Objective_Text("UW_ABTS")
						Clear_Any_Objective_Text_From_This_Script()
					ENDIF
					
					NETWORK_SET_FRIENDLY_FIRE_OPTION(FALSE)
					
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("UW_MINV")
					OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("UW_TIMELA")
					OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("UW_TIMEL")
						CLEAR_HELP()
					ENDIF
					REMOVE_VEHICLE_BLIP()
					
					COMMON_FREEMODE_AMBIENT_EVENTS_SET_UP(FMMC_TYPE_KILL_LIST, 0.0, 0.0, TRUE, TRUE, TRUE)
					
					FM_EVENT_SET_PLAYER_IS_PERMANENT_PARTICIPANT_IN_FM_EVENT(TRUE) 
					
					STOP_PLAYER_LEAVING_VEHICLE_THIS_FRAME()
					SET_WANTED_LEVEL_MULTIPLIER(0.1)
					iMyMaxWantedLevel = GET_MAX_WANTED_LEVEL()
					SET_MAX_WANTED_LEVEL(0)
					SET_FREEMODE_FLOW_COMMS_DISABLED(TRUE)
					SET_LOCAL_PLAYER_DOING_KILL_LIST(TRUE)
					
					IF serverBD.VehModel != RHINO
						GIVE_AND_EQUIP_PLAYERS_PARACHUTE()
						PRINTLN("     ---------->     KILL LIST - PLAYER STAGE = eAD_ATTACK - PARACHUTE GIVEN")
					ENDIF
					
					relMyFmGroup = GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID())
					SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), relUWPlayer)
					
					SET_AUDIO_FLAG("DisableFlightMusic", TRUE) 
					TRIGGER_MUSIC_EVENT("MP_MC_START")
					
					SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PreventAutoShuffleToDriversSeat, TRUE)
					SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_CanActivateRagdollWhenVehicleUpsideDown, FALSE) // 2485988

					FM_EVENT_SET_PLAYER_WAITING_FOR_FM_EVENT_TO_START(FALSE)				
					
					playerBD[PARTICIPANT_ID_TO_INT()].iMyUwTeam = iMyUwPlayerVeh
				//	TURN_OFF_PASSIVE_MODE_OPTION(TRUE)
					IF NOT IS_COMPETITIVE_UW()
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_START,"UW_TITLE","UW_DESC", GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE(), DEFAULT, GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE())	
					ELSE
						IF serverBD.VehModel = HYDRA
						OR serverBD.VehModel = RHINO
						
							SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_START,"UW_TITLEC","UW_DESCC", GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE(), DEFAULT, GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE())	
						ELSE
							
							iTempVeh++
							
//							IF serverBD.VehModel = SAVAGE
//								tl15Veh = "UW_TMSAV"
//							ELIF serverBD.VehModel = BUZZARD
//								tl15Veh = "UW_TMBUZ"
//							ELIF serverBD.VehModel = VALKYRIE 
//								tl15Veh = "UW_TMVALK"
//							ENDIF
							
							tl15Team = "UW_TM"
							tl15Team += iTempVeh
							
							SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_FM_EVENT_START, tl15Team, "UW_DESCCT", "UW_TITLE", ciFM_EVENTS_END_UI_SHARD_TIME, DEFAULT, DEFAULT, "", GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE(), GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE())
						ENDIF
					ENDIF
					
					
					telemetryStruct.m_startTime = GET_CLOUD_TIME_AS_INT()
					
					
					FM_EVENT_SET_PLAYER_WAITING_FOR_FM_EVENT_TO_START(FALSE)
					
					DISABLE_MP_PASSIVE_MODE(PMER_DEATHSCREEN, FALSE)
					
					START_NET_TIMER(timeMyParticipation)

				//	Print_Objective_Text_With_String("UW_ATTK", GET_VEHICLE_TYPE_STRING())	//Destroy the ~r~Merryweather patrols.
				
				//	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_KILL_LIST
					playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_ATTACK
					NET_NL() NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  PLAYER STAGE = eAD_ATTACK    <----------     ") NET_NL()
					PRINTLN("     ---------->     KILL LIST - PLAYER STAGE = eAD_ATTACK - TAKING PART - MY VEH ", iMyUwPlayerVeh) 

				ELSE
					REMOVE_VEHICLE_BLIP()
				//	SET_ALL_UW_VEHICLES_LOCKED(TRUE)
					
					IF Is_This_The_Current_Objective_Text("UW_ABTSC")
					OR Is_This_The_Current_Objective_Text("UW_ABTS")
						Clear_Any_Objective_Text_From_This_Script()
					ENDIF
					
					IF IS_THIS_HELP_MESSAGE_WITH_STRING_AND_NUMBER_BEING_DISPLAYED("UW_HELP1", GET_VEH_BLIP_STRING(), GET_CASH_REWARD(TRUE))
					OR IS_THIS_HELP_MESSAGE_WITH_STRING_AND_NUMBER_BEING_DISPLAYED("UW_HELP1C", GET_VEH_BLIP_STRING(), GET_CASH_REWARD(TRUE))
						CLEAR_HELP()
					ENDIF
					playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_OFF_MISSION
					
					FM_EVENT_SET_PLAYER_WAITING_FOR_FM_EVENT_TO_START(FALSE)
					
					
					IF playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet = 0
						NET_NL() NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  PLAYER STAGE = eAD_OFF_MISSION AS .iInVehicleBitSet <> 0   <----------     ") NET_NL()
					ELIF NOT IS_LOCAL_PLAYER_SITTING_IN_THEIR_UW_VEHICLE()
						NET_NL() NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  PLAYER STAGE = eAD_OFF_MISSION AS IS_LOCAL_PLAYER_SITTING_IN_THEIR_UW_VEHICLE   <----------     ") NET_NL()
					ENDIF
				ENDIF
				
			ELIF serverBD.eStage = eAD_EXPLODE_VEH
				FM_EVENT_SET_PLAYER_WAITING_FOR_FM_EVENT_TO_START(FALSE)
				
				IF IS_THIS_HELP_MESSAGE_WITH_STRING_AND_NUMBER_BEING_DISPLAYED("UW_HELP1", GET_VEH_BLIP_STRING(), GET_CASH_REWARD(TRUE))
				OR IS_THIS_HELP_MESSAGE_WITH_STRING_AND_NUMBER_BEING_DISPLAYED("UW_HELP1C", GET_VEH_BLIP_STRING(), GET_CASH_REWARD(TRUE))
					CLEAR_HELP()
				ENDIF
				
				// 2420580: Prevent non-participant players entering soon-to-explode vehicles
				IF NOT IS_LOCAL_PLAYER_DOING_KILL_LIST()
				//	SET_ALL_UW_VEHICLES_LOCKED(TRUE)
				ENDIF
				
				IF Is_This_The_Current_Objective_Text("UW_ABTSC")
				OR Is_This_The_Current_Objective_Text("UW_ABTS")
					Clear_Any_Objective_Text_From_This_Script()
				ENDIF
				
				REMOVE_VEHICLE_BLIP()
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_EXPLODE_VEH
				NET_NL() NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  PLAYER STAGE = eAD_EXPLODE_VEH  3  <----------     ") NET_NL()
			ELIF serverBD.eStage > eAD_EXPLODE_VEH
				IF Is_This_The_Current_Objective_Text("UW_ABTSC")
				OR Is_This_The_Current_Objective_Text("UW_ABTS")
					Clear_Any_Objective_Text_From_This_Script()
				ENDIF
				FM_EVENT_SET_PLAYER_WAITING_FOR_FM_EVENT_TO_START(FALSE)
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CLEANUP
				NET_NL() NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  PLAYER STAGE = eAD_CLEANUP    <----------     ") NET_NL()
			ELSE
				IF playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet <> 0
					PAUSE_BOUNTY_TIMER_THIS_FRAME()
					HIDE_BOUNTY_BLIP_THIS_FRAME()
					IF ARE_ALL_UW_PLAYER_VEHICLES_FULL()
						STOP_PLAYER_LEAVING_VEHICLE_THIS_FRAME()
					ENDIF
				ENDIF
				IF IS_COMPETITIVE_UW()
					IF SHOULD_BAIL_FM_EVENT_DUE_TO_LOW_SESSION_NUMBERS()
						IF Is_This_The_Current_Objective_Text("UW_ABTSC")
						OR Is_This_The_Current_Objective_Text("UW_ABTS")
							Clear_Any_Objective_Text_From_This_Script()
						ENDIF
						FM_EVENT_SET_PLAYER_WAITING_FOR_FM_EVENT_TO_START(FALSE)
						
						playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CLEANUP
			
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST -  PLAYER STAGE = eAD_CLEANUP AS SHOULD_BAIL_FM_EVENT_DUE_TO_LOW_SESSION_NUMBERS  1  <----------      ")
					ENDIF
				ELSE
					IF serverBD.VehModel = VALKYRIE
						IF SHOULD_BAIL_FM_EVENT_DUE_TO_LOW_SESSION_NUMBERS() 
							IF Is_This_The_Current_Objective_Text("UW_ABTSC")
							OR Is_This_The_Current_Objective_Text("UW_ABTS")
								Clear_Any_Objective_Text_From_This_Script()
							ENDIF
					
							FM_EVENT_SET_PLAYER_WAITING_FOR_FM_EVENT_TO_START(FALSE)
							playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CLEANUP
			
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST -  PLAYER STAGE = eAD_CLEANUP AS SHOULD_BAIL_FM_EVENT_DUE_TO_LOW_SESSION_NUMBERS  2  <----------      ")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE eAD_ATTACK
			CONTROL_PED_LOOP()
			CONTROL_UW_VEH_BODY()
			CHECK_FOR_REWARD()
			DRAW_KILLS_BAR()
			
			MAINTAIN_UW_MUSIC()
			MAINTAIN_TICKERS()
			
			MAINTAIN_DPAD_DOWN_LEADERBOARD()
			
			MAINTAIN_UW_VOICE_CHAT()
			
			MAINTAIN_PLAYER_OUT_OF_BOUNDS()
			
			PAUSE_BOUNTY_TIMER_THIS_FRAME()
			HIDE_BOUNTY_BLIP_THIS_FRAME()
			
			//MAINTAIN_MISSION_DURATION_TIMER_CLIENT()
			
			
//			IF IS_COMPETITIVE_UW()
//				DRAW_SCORES()
//			ENDIF
			
			//-- Objective text
			IF NOT IS_BIT_SET(iBoolsBitSet, biWaitForShard)
				IF NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_START_OF_JOB)
					IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_IamOOB)
						IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ApproachingOOB)
							IF playerBD[PARTICIPANT_ID_TO_INT()].iVehTooFewPlayers = -1
								IF NOT IS_COMPETITIVE_UW()
									IF NOT Is_This_The_Current_Objective_Text("UW_ATTK")
										Print_Objective_Text("UW_ATTK") // Destroy the ~r~Merryweather patrols.
									ENDIF
								ELSE
									IF serverBD.VehModel = HYDRA
									OR serverBD.VehModel = RHINO
										IF NOT Is_This_The_Current_Objective_Text("UW_ATTK")
											Print_Objective_Text("UW_ATTK") // Destroy the ~r~Merryweather patrols.
										ENDIF
									ELSE
									//	IF NOT Is_This_The_Current_Objective_Text("UW_ATTKT")
										IF NOT Is_This_The_Current_Objective_Text("UW_ATTK")
											GET_MY_VEHICLE_KILL_DETAILS(iTempVeh, iTempKills)
											iTempVeh++
											
			//								IF serverBD.VehModel = SAVAGE
			//									tl15Veh = "UW_TMSAV"
			//								ELIF serverBD.VehModel = BUZZARD
			//									tl15Veh = "UW_TMBUZ"
			//								ELIF serverBD.VehModel = VALKYRIE 
			//									tl15Veh = "UW_TMVALK"
			//								ENDIF
											
											tl15Team = "UW_TM"
											tl15Team += iTempVeh
											
											//Print_Objective_Text_With_Two_String("UW_ATTKT", tl15Veh, tl15Team)
											Print_Objective_Text("UW_ATTK") // Destroy the ~r~Merryweather patrols.
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF Is_This_The_Current_Objective_Text("UW_ATTK")
									Clear_Any_Objective_Text_From_This_Script()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			
			MAINTAIN_ENOUGH_PLAYERS_IN_MY_TEAM_VEHICLE()
			
		//	IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.niVeh[0]))
			IF playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet <> 0
				IF playerBD[PARTICIPANT_ID_TO_INT()].iVehTooFewPlayers = -1
					STOP_PLAYER_LEAVING_VEHICLE_THIS_FRAME()
				ENDIF
			ENDIF
			
			//-- Move on if killed
			IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_OFF_MISSION
				Clear_Any_Objective_Text_From_This_Script()
				SET_BIT(iBoolsBitSet, biDiedDuringEvent)
			//	SET_ALL_UW_VEHICLES_LOCKED(TRUE)
				NET_NL() NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  PLAYER STAGE = eAD_OFF_MISSION - DEAD    <----------     ") NET_NL()
			ELIF IS_PLAYER_IN_CORONA()
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_OFF_MISSION
				Clear_Any_Objective_Text_From_This_Script()
			//	SET_ALL_UW_VEHICLES_LOCKED(TRUE)
				NET_NL() NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  PLAYER STAGE = eAD_OFF_MISSION - CORONA    <----------     ") NET_NL()
				
			ENDIF
			
			//-- Move on if player vehicle destroyed
			IF playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_ATTACK
				IF NOT IS_MY_UW_VEHICLE_DRIVABLE()
					SET_BIT(iBoolsBitSet, biVehDestroyed)
					Clear_Any_Objective_Text_From_This_Script()
					
					IF iMyUwPlayerVeh > -1
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[iMyUwPlayerVeh])
							PRINTLN("     ---------->     KILL LIST - CLEANING UP MY VEH AS VEH DESTROYED - MY VEH ", iMyUwPlayerVeh) 
							CLEANUP_NET_ID(serverBD.niVeh[iMyUwPlayerVeh])
						ENDIF
					ENDIF
					playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_OFF_MISSION
					PRINTLN("     ---------->     KILL LIST - PLAYER STAGE = eAD_OFF_MISSION - VEH DESTROYED - MY VEH ", iMyUwPlayerVeh) 
				ENDIF
			ENDIF
			
			
			//--Move on if server says so
			IF serverBD.eStage = eAD_EXPLODE_VEH
				SET_PLAYER_DEFAULT_FRIENDLY_FIRE_OPTION()
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_EXPLODE_VEH
				NET_NL() NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  PLAYER STAGE = eAD_EXPLODE_VEH  1  <----------     ") NET_NL()
			ELIF serverBD.eStage > eAD_EXPLODE_VEH
				SET_PLAYER_DEFAULT_FRIENDLY_FIRE_OPTION()
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CLEANUP
				NET_NL() NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  PLAYER STAGE = eAD_CLEANUP    <----------     ") NET_NL()
			ELSE 
				IF IS_COMPETITIVE_UW() 
					IF SHOULD_BAIL_FM_EVENT_DUE_TO_LOW_SESSION_NUMBERS() 
						playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CLEANUP
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST -  PLAYER STAGE = eAD_CLEANUP AS SHOULD_BAIL_FM_EVENT_DUE_TO_LOW_SESSION_NUMBERS  3  <----------      ")
					ENDIF
				ELSE
//					IF serverBD.VehModel = VALKYRIE
//						IF SHOULD_BAIL_FM_EVENT_DUE_TO_LOW_SESSION_NUMBERS() 
//							playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CLEANUP
//			
//							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST -  PLAYER STAGE = eAD_CLEANUP AS SHOULD_BAIL_FM_EVENT_DUE_TO_LOW_SESSION_NUMBERS  4  <----------      ")
//						ENDIF
//					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE eAD_OFF_MISSION
			IF serverBD.eStage = eAD_EXPLODE_VEH
					playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_EXPLODE_VEH
					NET_NL() NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  PLAYER STAGE = eAD_EXPLODE_VEH  2  <----------     ") NET_NL()
			ELIF serverBD.eStage > eAD_EXPLODE_VEH
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CLEANUP
				NET_NL() NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  PLAYER STAGE = eAD_CLEANUP    <----------     ") NET_NL()
			ENDIF
			
		//	MAINTAIN_OFF_KILL_LIST_SHARD(DEFAULT, IS_BIT_SET(serverBD.iServerBitSet, biS_DurationExpired))
			
			
			MAINTAIN_OFF_KILL_LIST_SHARD()
			
			IF IS_LOCAL_PLAYER_DOING_KILL_LIST()
			//	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_KILL_LIST
			//		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = -1
				IF FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID()) = FMMC_TYPE_KILL_LIST
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					AND NOT IS_RADAR_HIDDEN()
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PlayersDontDragMeOutOfCar, FALSE)
					
					
						IF IS_BIT_SET(iBoolsBitSet, biDisabledCorona)
							Clear_MissionsAtCoords_Local_Disabled_Area()
							CLEAR_BIT(iBoolsBitSet, biDisabledCorona)
							PRINTLN("     ---------->     KILL LIST - CLEAR biDisabledCorona - 1")
						ENDIF
						
				//		COMMON_FREEMODE_AMBIENT_EVENTS_CLEAN_UP(FMMC_TYPE_KILL_LIST)
						CLEAR_PLAYERS_CURRENT_FREEMODE_AMBIENT_EVENT_TYPE()
						
						FM_EVENTS_CLEANUP_WANTED_OVERRIDES()
						
						Block_All_MissionsAtCoords_Missions(FALSE)
						
						HIDE_ALL_SHOP_BLIPS(FALSE)
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PreventAutoShuffleToDriversSeat, FALSE)
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_CanActivateRagdollWhenVehicleUpsideDown, TRUE) // 2485988
						
						FM_EVENT_SET_PLAYER_IS_CRITICAL_TO_FM_EVENT(FALSE)
						SET_PLAYER_DEFAULT_FRIENDLY_FIRE_OPTION()
					ENDIF
					
					INIT_MY_PARTICIPATION_TIME_MULTIPLIER()
				ENDIF
			ELSE 
				IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_SetPedLockedOutVeh)
					// 2420580: Prevent non-participant players entering soon-to-explode vehicles
				//	SET_ALL_UW_VEHICLES_LOCKED(TRUE)
					PRINTLN("     ---------->     KILL LIST - ", GET_PLAYER_NAME(PLAYER_ID()), " is off mission. Vehicles considered = false")
					SET_BIT(iBoolsBitSet2, bi2_SetPedLockedOutVeh)
				ENDIF
			ENDIF
			
			IF NOT SHOULD_UI_BE_HIDDEN()
				MAINTAIN_KILL_LIST_NO_LONGER_AVAILABLE()
				
				MAINTAIN_UW_MUSIC()
				
				CONTROL_PED_LOOP(FALSE)
				
				MAINTAIN_TICKERS()
				
				MAINTAIN_DPAD_DOWN_LEADERBOARD(TRUE)
				
				MAINTAIN_UW_VOICE_CHAT()
				
				IF playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet <> 0
				//	DRAW_KILLS_BAR()
				ENDIF
			ELSE
				REMOVE_AI_BLIPS()
			ENDIF
			
			MAINTAIN_CLIENT_PARTICIPANT_LOOP()
			MAINTAIN_RESET_MY_FF_OPTION()
			
			CHECK_FOR_REWARD()
			
			//If we've not set that we can't target peds when we're not on the event then do so
			IF NOT IS_BIT_SET(iBoolsBitSet2, bi2_SetPedsCanBeTargeted)
				INT i
				//Loop all normal peds
				REPEAT MAX_MW_PEDS i
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.niMwPed[i])
						IF NOT IS_NET_PED_INJURED(serverbd.niMwPed[i])
							SET_PED_CAN_BE_TARGETTED_BY_PLAYER(NET_TO_PED(serverbd.niMwPed[i]), PLAYER_ID(), FALSE)
							CPRINTLN(DEBUG_NET_AMBIENT_LR, "     ---------->     KILL LIST SET_PED_CAN_BE_TARGETTED_BY_PLAYER PED ", i)
						ENDIF
					ENDIF
				ENDREPEAT
				//loop all boos peds
				REPEAT MAX_BOSS_PEDS i
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.niBossPeds[i])
						IF NOT IS_NET_PED_INJURED(serverbd.niBossPeds[i])
							SET_PED_CAN_BE_TARGETTED_BY_PLAYER(NET_TO_PED(serverbd.niBossPeds[i]), PLAYER_ID(),  FALSE)
						ENDIF
					ENDIF
				ENDREPEAT
				SET_BIT(iBoolsBitSet2, bi2_SetPedsCanBeTargeted)
			//If more peds have been spawned then clear the bit and do it again. 
			ELIF serverBD.iMwPedsSpawned != iMwPedsSpawnedLocal
				iMwPedsSpawnedLocal = serverBD.iMwPedsSpawned
				CLEAR_BIT(iBoolsBitSet2, bi2_SetPedsCanBeTargeted)	
				CPRINTLN(DEBUG_NET_AMBIENT_LR, "     ---------->     KILL LIST SET_PED_CAN_BE_TARGETTED_BY_PLAYER CLEARED bi2_SetPedsCanBeTargeted iMwPedsSpawned = ", serverBD.iMwPedsSpawned)
			ENDIF
			
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_IamOOB)
				CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_IamOOB)
			ENDIF
			
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ApproachingOOB)
				CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ApproachingOOB)
			ENDIF
			
			IF GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()) = relUWPlayer
				SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), relMyFmGroup)
				NET_NL() NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  eAD_OFF_MISSION - RESTORED MY FM REL GROUP     <----------     ") NET_NL()
			ENDIF
		BREAK
		
		CASE eAD_EXPLODE_VEH
			IF NOT IS_BIT_SET(iBoolsBitSet, biClearConsider)
				IF IS_BIT_SET(serverBD.iServerBitSet ,biS_TimeoutExpired)
					REMOVE_VEHICLE_BLIP()
					
					SET_BIT(iBoolsBitSet, biClearConsider)
					//SET_ALL_UW_VEHICLES_LOCKED(TRUE)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	 "    ---------->     KILL LIST SET_ALL_UW_VEHICLES_LOCKED(TRUE) AS biS_TimeoutExpired")
				ENDIF
			ENDIF
			MAINTAIN_ENOUGH_PLAYERS_IN_MY_TEAM_VEHICLE()
			
			MAINTAIN_EXPLODE_VEH()
			
			IF NOT SHOULD_UI_BE_HIDDEN()
				
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_TooFewValid) 
				AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_TimeoutExpired)
					MAINTAIN_DPAD_DOWN_LEADERBOARD(TRUE)
				ENDIF
				
				MAINTAIN_TICKERS()

				MAINTAIN_UW_MUSIC()
				
				MAINTAIN_UW_VOICE_CHAT()
				
				MAINTAIN_TOO_MANY_PLAYERS_HID_EVENT_HELP()
			ENDIF
			
			MAINTAIN_OFF_KILL_LIST_SHARD()
			
			MAINTAIN_RESET_MY_FF_OPTION()
			
			CHECK_FOR_REWARD()
				
			IF serverBD.eStage > eAD_EXPLODE_VEH
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CLEANUP
				NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  PLAYER STAGE = eAD_CLEANUP    <----------     ") NET_NL()
			ENDIF
		BREAK
		
		CASE eAD_CLEANUP
			NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  SCRIPT CLEANUP B     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		BREAK
	ENDSWITCH
	
	MAINTAIN_LOCK_VEHICLE_DOORS_WITH_NO_PLAYERS()
ENDPROC

PROC PROCESS_SCTV_PLAYER_KILL_LIST_CLIENT()
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
		EXIT
	ENDIF
	
	IF DRAW_CHALLENGE_DPAD_FOR_DEV_SPECTATOR()
		MAINTAIN_DPAD_DOWN_LEADERBOARD()
	ENDIF
	
	PLAYER_INDEX playerSPec = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget
	PARTICIPANT_INDEX partSpec
	INT iPartSpec
	KILL_LIST_STAGE_ENUM partSpecStage
	#IF IS_DEBUG_BUILD
		BOOL bPlayerValid
	#ENDIF
	
	IF playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet <> 0
		playerBD[PARTICIPANT_ID_TO_INT()].iInVehicleBitSet = 0
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     KILL LIST -  [PROCESS_SCTV_PLAYER_KILL_LIST_CLIENT] SET playerBD[PARTICIPANT_ID_TO_INT].iInVehicleBitSet = 0 AS IM SCTV")
	ENDIF
	
	IF playerSPec <> INVALID_PLAYER_INDEX()
		IF NETWORK_IS_PLAYER_ACTIVE(playerSPec)
			IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerSPec)
				partSpec = NETWORK_GET_PARTICIPANT_INDEX(playerSPec)
				iPartSpec = NATIVE_TO_INT(partSpec)
				partSpecStage = playerBD[iPartSpec].eStage
				
				#IF IS_DEBUG_BUILD
					bPlayerValid = TRUE
				#ENDIF
				
				SWITCH partSpecStage
					CASE eAD_IDLE
						MAINTAIN_START_MISSION_TIMER_CLIENT()
						
						MAINTAIN_BLIPS_FOR_SCTV_PLAYERS(iPartSpec)
					BREAK
					
					CASE eAD_ATTACK
						REMOVE_VEHICLE_BLIP()
						
						DRAW_KILLS_BAR()
						
						CONTROL_PED_LOOP()
					BREAK
					
					CASE eAD_OFF_MISSION
						CONTROL_PED_LOOP(FALSE)
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF NOT HAS_NET_TIMER_STARTED(timeSCTVDebug)
		OR (HAS_NET_TIMER_STARTED(timeSCTVDebug) AND HAS_NET_TIMER_EXPIRED(timeSCTVDebug, 5000))
			IF bPlayerValid
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     KILL LIST -  [PROCESS_SCTV_PLAYER_KILL_LIST_CLIENT]   I'm spectating player ", GET_PLAYER_NAME(playerSPec), " participant index = ", iPartSpec, " Their stage = ", ENUM_TO_INT(partSpecStage))
			ENDIF
			RESET_NET_TIMER(timeSCTVDebug)
			START_NET_TIMER(timeSCTVDebug)
		ENDIF
	#ENDIF
ENDPROC

FUNC BOOL ENTITY_DISTANCE_TO_ANY_UW_PLAYER(ENTITY_INDEX ped, FLOAT fDist)
	INT i
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			IF playerBD[i].iInVehicleBitSet <> 0
				IF playerBD[i].eStage = eAD_ATTACK
					PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
					PED_INDEX playerPed = GET_PLAYER_PED(PlayerId)
					
					IF NOT IS_PED_INJURED(playerPed)
						IF GET_DISTANCE_BETWEEN_ENTITIES(ped, playerPed) < fDist
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Finds a player who is in the urban warfare mission vehicle 
FUNC PLAYER_INDEX GET_ANY_PLAYER_IN_MISSION_VEHICLE()
	PLAYER_INDEX playerInVeh = INVALID_PLAYER_INDEX()
	INT i
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF playerInVeh = INVALID_PLAYER_INDEX()
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
				IF playerBD[i].iInVehicleBitSet <> 0
					IF playerBD[i].eStage = eAD_ATTACK
						playerInVeh = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

//	REPEAT MAX_PLAYERS i
//		IF serverBD.piPlayers[i] <> INVALID_PLAYER_INDEX()
//			IF IS_NET_PLAYER_OK(serverBD.piPlayers[i])
//				IF IS_NET_VEHICLE_DRIVEABLE(serverbd.niVeh[0])
//					IF IS_PED_SITTING_IN_VEHICLE(GET_PLAYER_PED(serverBD.piPlayers[i]), NET_TO_VEH(serverbd.niVeh[0]))
//						playerInVeh = serverBD.piPlayers[i]
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		IF playerInVeh <> INVALID_PLAYER_INDEX()
			PRINTLN("---------->     KILL LIST [GET_ANY_PLAYER_IN_MISSION_VEHICLE] Found a player... ", GET_PLAYER_NAME(playerInVeh))
		ELSE
			PRINTLN("---------->     KILL LIST [GET_ANY_PLAYER_IN_MISSION_VEHICLE] Failed to find a player!")
		ENDIF
	#ENDIF
	
	RETURN playerInVeh
ENDFUNC

FUNC BOOL IS_PILOT_TARGET_VALID_SERVER(INT iPed)
	INT iPilotPlane
	PLAYER_INDEX playerTarget
	PARTICIPANT_INDEX partTarget
	INT iTarget
	IF iPed % 4 = 0
		iPilotPlane = iPed / 4
		IF iPilotPlane < MAX_MW_VEHS
			IF serverBD.iPedPlayerTargetServer[iPilotPlane] <> -1
				iTarget = serverBD.iPedPlayerTargetServer[iPilotPlane]
				playerTarget = INT_TO_PLAYERINDEX(iTarget)
				IF NETWORK_IS_PLAYER_ACTIVE(playerTarget)
					IF IS_NET_PLAYER_OK(playerTarget)
						IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerTarget)
							partTarget = NETWORK_GET_PARTICIPANT_INDEX(playerTarget)
							iTarget = NATIVE_TO_INT(partTarget)
							IF playerBD[iTarget].eStage <> eAD_ATTACK
								CPRINTLN(DEBUG_NET_AMBIENT_LR, "     ---------->     KILL LIST - [IS_PILOT_TARGET_VALID] FALSE AS playerBD[iTarget].eStage <> eAD_ATTACK playerTarget = ", GET_PLAYER_NAME(playerTarget))
								RETURN FALSE
							ENDIF
						ELSE
							CPRINTLN(DEBUG_NET_AMBIENT_LR, "     ---------->     KILL LIST - [IS_PILOT_TARGET_VALID] FALSE AS NETWORK_IS_PLAYER_A_PARTICIPANT playerTarget = ", GET_PLAYER_NAME(playerTarget))
							RETURN FALSE
						ENDIF
					ELSE
						CPRINTLN(DEBUG_NET_AMBIENT_LR, "     ---------->     KILL LIST - [IS_PILOT_TARGET_VALID] FALSE AS TARGET PLAYER NOT OK playerTarget = ", GET_PLAYER_NAME(playerTarget))
						RETURN FALSE
					ENDIF
				ELSE
					CPRINTLN(DEBUG_NET_AMBIENT_LR, "     ---------->     KILL LIST - [IS_PILOT_TARGET_VALID] FALSE AS NETWORK_IS_PLAYER_ACTIVE iPilotPlane = ", iPilotPlane)
					RETURN FALSE
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_AI_PED_BRAIN()
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		EXIT
	ENDIF
	INT iThisPilotVeh
	INT i
	
	PLAYER_INDEX playerTarget
	PED_INDEX pedPlayer
	
	VEHICLE_INDEX vehPed 
	REPEAT MAX_MW_PEDS i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.niMwPed[i])
			IF NOT IS_NET_PED_INJURED(serverbd.niMwPed[i])
				SWITCH serverbd.iPedState[i]
					CASE PED_AI_STATE_CREATED
						IF NOT IS_PED_IN_ANY_VEHICLE(NET_TO_PED(serverbd.niMwPed[i]))
							serverbd.iPedState[i] = PED_AI_STATE_GOTO_PLAYERS
							PRINTLN("---------->     KILL LIST Ped ", i ," moving to PED_AI_STATE_GOTO_PLAYERS as ped not in a vehicle")
						ELSE
							IF (IS_PED_IN_ANY_VEHICLE(NET_TO_PED(serverbd.niMwPed[i])) AND IS_PED_VEHICLE_DRIVABLE(i))
								serverbd.iPedState[i] = PED_AI_STATE_GOTO_PLAYERS
								PRINTLN("---------->     KILL LIST Ped ", i ," moving to PED_AI_STATE_GOTO_PLAYERS as ped in vehicle and vehicle is driveable")
							ENDIF
						ENDIF
					BREAK
				
					CASE PED_AI_STATE_GOTO_PLAYERS
						IF serverBD.eStage = eAD_ATTACK
							IF NOT ARE_ALL_PLAYER_VEHICLES_DEAD() //IS_ENTITY_DEAD(NET_TO_VEH(serverbd.niVeh[0]))
								IF IS_PED_IN_ANY_VEHICLE(NET_TO_PED(serverbd.niMwPed[i]))
									vehPed = GET_VEHICLE_PED_IS_IN(NET_TO_PED(serverbd.niMwPed[i]))


									IF ENTITY_DISTANCE_TO_ANY_UW_PLAYER(vehPed, 250.0)
										serverbd.iPedState[i] = PED_AI_STATE_ATTACK_PLAYERS
										PRINTLN("---------->     KILL LIST Ped ", i ," moving to PED_AI_STATE_ATTACK_PLAYERS as  dist is OK")
									ELSE
										IF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehPed))
											IF i % 4 = 0
												iThisPilotVeh = i / 4
												IF iThisPilotVeh < MAX_MW_VEHS
													IF serverBD.iPedPlayerTargetServer[iThisPilotVeh] = -1
													OR NOT IS_PILOT_TARGET_VALID_SERVER(i)
														playerTarget = GET_ANY_PLAYER_IN_MISSION_VEHICLE()
														IF playerTarget <> INVALID_PLAYER_INDEX()
															pedPlayer = GET_PLAYER_PED(playerTarget)
															IF NOT IS_PED_INJURED(pedPlayer)
																IF GET_DISTANCE_BETWEEN_ENTITIES(vehPed, pedPlayer) > 250.0
																	serverBD.iPedPlayerTargetServer[iThisPilotVeh] = NATIVE_TO_INT(playerTarget)
																	PRINTLN("---------->     KILL LIST [PROCESS_AI_PED_BRAIN] serverBD.iPedPlayerTargetServer[", iThisPilotVeh, "] UPDATED TO ", NATIVE_TO_INT(playerTarget), " WHO IS PLAYER ", GET_PLAYER_NAME(playerTarget))
																ENDIF
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF		
										ENDIF
									ENDIF
									
								ELSE
									serverbd.iPedState[i] = PED_AI_STATE_ATTACK_PLAYERS
									PRINTLN("---------->     KILL LIST Ped ", i ," moving to PED_AI_STATE_ATTACK_PLAYERS as not in a vehicle ")
								ENDIF
							ELSE
								serverbd.iPedState[i] = PED_AI_STATE_CLEAN_UP
								PRINTLN("---------->     KILL LIST Ped ", i ," moving to PED_AI_STATE_CLEAN_UP from PED_AI_STATE_GOTO_PLAYERS as mission veh dead ")
							ENDIF
						ELSE
							serverbd.iPedState[i] = PED_AI_STATE_CLEAN_UP
							PRINTLN("---------->     KILL LIST Ped ", i ," moving to PED_AI_STATE_CLEAN_UP from PED_AI_STATE_GOTO_PLAYERS as serverBD.eStage = ", serverBD.eStage )
						ENDIF
						
					BREAK
					
					CASE PED_AI_STATE_ATTACK_PLAYERS
						IF serverBD.eStage = eAD_ATTACK
							IF ARE_ALL_PLAYER_VEHICLES_DEAD() //IS_ENTITY_DEAD(NET_TO_VEH(serverbd.niVeh[0]))
								serverbd.iPedState[i] = PED_AI_STATE_CLEAN_UP
								PRINTLN("---------->     KILL LIST Ped ", i ," moving to PED_AI_STATE_CLEAN_UP from PED_AI_STATE_ATTACK_PLAYERS as mission veh dead ")
							ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_DurationExpired)
								serverbd.iPedState[i] = PED_AI_STATE_CLEAN_UP
								PRINTLN("---------->     KILL LIST Ped ", i ," moving to PED_AI_STATE_CLEAN_UP from PED_AI_STATE_ATTACK_PLAYERS as biS_DurationExpired ")
							ELIF IS_BIT_SET(serverBD.iPedRetaskGotoBitSet, i)
								serverbd.iPedState[i] = PED_AI_STATE_GOTO_PLAYERS
								CLEAR_BIT(serverBD.iPedRetaskGotoBitSet, i)
								PRINTLN("---------->     KILL LIST Ped ", i ," moving to PED_AI_STATE_GOTO_PLAYERS from PED_AI_STATE_ATTACK_PLAYERS as iPedRetaskGotoBitSet ")
							ENDIF
						ELSE
							serverbd.iPedState[i] = PED_AI_STATE_CLEAN_UP
							PRINTLN("---------->     KILL LIST Ped ", i ," moving to PED_AI_STATE_CLEAN_UP from PED_AI_STATE_ATTACK_PLAYERS as serverbd.iPedState[i] = ", serverbd.iPedState[i])
						ENDIF
						
					BREAK
					
					CASE PED_AI_STATE_CLEAN_UP
						IF IS_PED_FLEEING(NET_TO_PED(serverbd.niMwPed[i]))
							CLEANUP_NET_ID(serverbd.niMwPed[i])
							serverbd.iPedState[i] = PED_AI_STATE_FINISHED
							PRINTLN("---------->     KILL LIST Ped ", i ," moving to PED_AI_STATE_FINISHED as fleeing ")
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC INT GET_CLOSEST_PLAYER_VEH_TO_PED(PED_INDEX ped, FLOAT fMaxDist = 275.0)
	INT iClosest = -1
	FLOAT fClosestDist = 999999999.0
	FLOAT fDist
	INT i
	VECTOR vVehCoord
	PED_INDEX pedTemp
	REPEAT MAX_PLAYER_VEH i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
			IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[i])
				pedTemp = GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(serverBD.niVeh[i]))
				IF NOT IS_PED_INJURED(pedTemp)
					IF IS_PED_A_PLAYER(pedTemp)
						vVehCoord = GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niVeh[i]))	
						fDist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(ped, vVehCoord) 
						IF fDist < fClosestDist
							IF fDist < fMaxDist
								fClosestDist = fDist
								iClosest = i
							ENDIF
						ENDIF	
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN iClosest
ENDFUNC




PROC MAINTAIN_KILL_PEDS_IN_NON_DRIVABLE_AIRCRAFT(INT iPed)	
	EXIT
	
	VEHICLE_INDEX vehTemp
	#IF IS_DEBUG_BUILD
	VECTOR vCoords
	#ENDIF
	IF NOT IS_NET_PED_INJURED(serverbd.niMwPed[iPed])
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverbd.niMwPed[iPed])
			IF IS_PED_SITTING_IN_ANY_VEHICLE(NET_TO_PED(serverbd.niMwPed[iPed]))
				vehTemp = GET_VEHICLE_PED_IS_IN(NET_TO_PED(serverbd.niMwPed[iPed]))
				IF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehTemp))
				OR IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehTemp))
					IF NOT IS_VEHICLE_DRIVEABLE(vehTemp)
						SET_ENTITY_HEALTH(NET_TO_PED(serverbd.niMwPed[iPed]), 5)
						#IF IS_DEBUG_BUILD
						vCoords = GET_ENTITY_COORDS(vehTemp, FALSE)
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"---------->     KILL LIST [MAINTAIN_KILL_PEDS_IN_NON_DRIVABLE_AIRCRAFT] Set ped as injured as their aircraft isn't drivble, PED = ", iPed, " Veh coords ", vCoords)
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC VECTOR GET_COORDS_OF_ANY_PLAYER_VEH()
	INT i
	REPEAT MAX_PLAYER_VEH i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.niVeh[i])
			RETURN (GET_ENTITY_COORDS(NET_TO_VEH(serverbd.niVeh[i]), FALSE))
		ENDIF
	ENDREPEAT
	
	//RETURN << 0.0, 0.0, 0.0 >>
	
	//--Use start location if nio player veh is found
	RETURN serverBD.vStartLocation[0]
ENDFUNC



//FUNC BOOL IS_PILOT_TARGET_VALID_CLIENT(INT iPed, INT &iTargetPlayer)
//	INT iPilotPlane
//		IF iPed % 4 = 0
//			iPilotPlane = iPed / 4
//			IF iPilotPlane < MAX_MW_VEHS
//				IF serverBD.iPedPlayerTargetServer[iPilotPlane] <> -1
//				AND iPedPlayerTargetClient[iPilotPlane] <> serverBD.iPedPlayerTargetServer[iPilotPlane]
//					iTargetPlayer = serverBD.iPedPlayerTargetServer[iPilotPlane]
//					CPRINTLN(DEBUG_NET_AMBIENT_LR, "     ---------->     KILL LIST - [IS_PILOT_TARGET_VALID] FALSE iPilotPlane = ", iPilotPlane, " iPedPlayerTargetClient[", iPilotPlane, "] = ", iPedPlayerTargetClient[iPilotPlane], " serverBD.iPedPlayerTargetServer[", iPilotPlane, "] = ", serverBD.iPedPlayerTargetServer[iPilotPlane], " iTargetPlayer NOW ", iTargetPlayer, " WHO IS PLAYER ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iTargetPlayer)))
//					RETURN FALSE
//				ENDIF
//			ENDIF
//		ENDIF
//	RETURN TRUE
//ENDFUNC

PROC PROCESS_AI_PED_BODY()
	INT i
	MODEL_NAMES mPedVeh
	VEHICLE_INDEX vehPed 
	VECTOR vPlayerCoords
	VECTOR vVehCoords
	PLAYER_INDEX playerTarget
	PED_INDEX pedPlayer
	FLOAT fDist
	INT iClosest
	//INT iThisPilotVeh
	INT iTargetPlayer = -1
	
	INT iIndexToCheck
	
	BOOL bPedIsPilot
	
	
	REPEAT MAX_MW_PEDS i
		IF IS_BIT_SET(iLocalPedTooFarFromTarget, i)
			IF serverbd.iPedState[i] <> PED_AI_STATE_ATTACK_PLAYERS	
				CLEAR_BIT(iLocalPedTooFarFromTarget, i)
				PRINTLN("---------->     KILL LIST PROCESS_AI_PED_BODY I have cleared iLocalPedTooFarFromTarget for ped ", i)
			ENDIF
		ENDIF
		
		
			
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.niMwPed[i])
		//	IF NOT IS_COMPETITIVE_UW()
				MAINTAIN_KILL_PEDS_IN_NON_DRIVABLE_AIRCRAFT(i)
		//	ENDIF
			
			IF NOT IS_NET_PED_INJURED(serverbd.niMwPed[i])
				SWITCH serverbd.iPedState[i]
					CASE PED_AI_STATE_GOTO_PLAYERS
						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverbd.niMwPed[i])

							IF IS_PED_IN_ANY_VEHICLE(NET_TO_PED(serverbd.niMwPed[i]))
								vehPed = GET_VEHICLE_PED_IS_IN(NET_TO_PED(serverbd.niMwPed[i]))
								mPedVeh = GET_ENTITY_MODEL(vehPed)
								IF IS_THIS_MODEL_A_HELI(mPedVeh)
									IF (GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niMwPed[i]), SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK
									AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niMwPed[i]), SCRIPT_TASK_VEHICLE_MISSION) != WAITING_TO_START_TASK)
													
										IF GET_PED_IN_VEHICLE_SEAT(vehPed) = NET_TO_PED(serverbd.niMwPed[i])
											playerTarget = GET_ANY_PLAYER_IN_MISSION_VEHICLE()
											IF playerTarget <> INVALID_PLAYER_INDEX()
												pedPlayer = GET_PLAYER_PED(playerTarget)
												IF NOT IS_PED_INJURED(pedPlayer)
													IF GET_DISTANCE_BETWEEN_ENTITIES(vehPed, pedPlayer) > 250.0
														vPlayerCoords = GET_ENTITY_COORDS(pedPlayer)
													
														TASK_HELI_MISSION(NET_TO_PED(serverbd.niMwPed[i]), 
																		vehPed,
																		NULL,
																		pedPlayer,
																		vPlayerCoords, 
																		MISSION_GOTO, 
																		50.0, 5, -1.0, 0, 100, -1, HF_MaintainHeightAboveTerrain)
														
														PRINTLN("---------->     KILL LIST PROCESS_AI_PED_BODY I have told ped ", i ," to go to the players while in a heli coords = ", vPlayerCoords)
														
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ELIF IS_THIS_MODEL_A_PLANE(mPedVeh)
									IF GET_PED_IN_VEHICLE_SEAT(vehPed) = NET_TO_PED(serverbd.niMwPed[i])
										bPedIsPilot = TRUE
										iIndexToCheck = i / 4
									ENDIF
									
									IF (GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niMwPed[i]), SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK
									AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niMwPed[i]), SCRIPT_TASK_VEHICLE_MISSION) != WAITING_TO_START_TASK)
									OR (bPedIsPilot AND iIndexToCheck < MAX_MW_VEHS AND iPedPlayerTargetClient[iIndexToCheck] <> serverBD.iPedPlayerTargetServer[iIndexToCheck])
										
										IF GET_PED_IN_VEHICLE_SEAT(vehPed) = NET_TO_PED(serverbd.niMwPed[i])
											iTargetPlayer = serverBD.iPedPlayerTargetServer[iIndexToCheck] 
											IF iTargetPlayer > -1
												playerTarget = INT_TO_PLAYERINDEX(iTargetPlayer)
												
												IF playerTarget <> INVALID_PLAYER_INDEX()
													pedPlayer = GET_PLAYER_PED(playerTarget)
													IF NOT IS_PED_INJURED(pedPlayer)

														vPlayerCoords = GET_ENTITY_COORDS(pedPlayer)
																								
														TASK_PLANE_MISSION( NET_TO_PED(serverbd.niMwPed[i]), 
																			vehPed, 
																			NULL, 
																			pedPlayer, 
																			vPlayerCoords, 
																			MISSION_GOTO,
																			50.0,
																			5.0, 
																			-1, 
																			100, 
																			50)
														PRINTLN("---------->     KILL LIST PROCESS_AI_PED_BODY I have told ped ", i ," to go to the players while in a plane vPlayerCoords = ",vPlayerCoords, " TARGET PLAYER IS ", GET_PLAYER_NAME(playerTarget))
														
														IF iPedPlayerTargetClient[iIndexToCheck] <> serverBD.iPedPlayerTargetServer[iIndexToCheck]
															iPedPlayerTargetClient[iIndexToCheck] = serverBD.iPedPlayerTargetServer[iIndexToCheck]
															PRINTLN("---------->     KILL LIST PROCESS_AI_PED_BODY iPedPlayerTargetClient[", iIndexToCheck, "] NOW ", iPedPlayerTargetClient[iIndexToCheck])
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ELSE	
									IF (GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niMwPed[i]), SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK
									AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niMwPed[i]), SCRIPT_TASK_VEHICLE_MISSION) != WAITING_TO_START_TASK
									AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niMwPed[i]), SCRIPT_TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS_WITH_CRUISE_SPEED) != WAITING_TO_START_TASK
									AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niMwPed[i]), SCRIPT_TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS_WITH_CRUISE_SPEED) != PERFORMING_TASK)
										IF GET_PED_IN_VEHICLE_SEAT(vehPed) = NET_TO_PED(serverbd.niMwPed[i])
											playerTarget = GET_ANY_PLAYER_IN_MISSION_VEHICLE()
											IF playerTarget <> INVALID_PLAYER_INDEX()
												pedPlayer = GET_PLAYER_PED(playerTarget)
												IF NOT IS_PED_INJURED(pedPlayer)
													fDist = GET_DISTANCE_BETWEEN_ENTITIES(vehPed, pedPlayer) 
													IF fDist > 500.0
														TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS_WITH_CRUISE_SPEED( NET_TO_PED(serverbd.niMwPed[i]),
																													GET_ENTITY_COORDS(pedPlayer), 
																													PEDMOVEBLENDRATIO_RUN, 
																													vehPed, 
																													TRUE, // use long range
																													DRIVINGMODE_AVOIDCARS, 
																													DEFAULT, 
																													DEFAULT, 
																													DEFAULT, 
																													DEFAULT, 
																													30.0)
														
														PRINTLN("---------->     KILL LIST PROCESS_AI_PED_BODY I have told ped ", i ," to go to the players while in a vehicle via TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS_WITH_CRUISE_SPEED")
													ELIF fDist > 250.0

														TASK_VEHICLE_MISSION_PED_TARGET( NET_TO_PED(serverbd.niMwPed[i]),
																				vehPed, 
																				pedPlayer, 
																				MISSION_GOTO, 
																				30.0, 
																				DRIVINGMODE_AVOIDCARS,
																				5.0,
																				5.0)
														
														PRINTLN("---------->     KILL LIST PROCESS_AI_PED_BODY I have told ped ", i ," to go to the players while in a vehicle")
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF

							
						ENDIF
					BREAK
					
					CASE PED_AI_STATE_ATTACK_PLAYERS
						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverbd.niMwPed[i])
						//	IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverbd.niVeh[0]))
							IF NOT ARE_ALL_PLAYER_VEHICLES_DEAD()
								//-- Attack player's vehicle
								IF (GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niMwPed[i]), SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) != PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niMwPed[i]), SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) != WAITING_TO_START_TASK)
									IF IS_PED_IN_ANY_VEHICLE(NET_TO_PED(serverBD.niMwPed[i]))
										IF NOT IS_BIT_SET(iLocalPedTooFarFromTarget, i)
											iClosest = GET_CLOSEST_PLAYER_VEH_TO_PED(NET_TO_PED(serverBD.niMwPed[i]))
											IF iClosest > -1
												TASK_COMBAT_HATED_TARGETS_AROUND_PED(NET_TO_PED(serverBD.niMwPed[i]), 299 )
												PRINTLN("---------->     KILL LIST PROCESS_AI_PED_BODY I have told ped ", i ," to attack the players")
											ELSE
												IF NOT IS_BIT_SET(iLocalPedTooFarFromTarget, i)
													BROADCAST_KILL_LIST_PED_TOO_FAR_FROM_TARGETS(i, ALL_PLAYERS_ON_SCRIPT())	
													SET_BIT(iLocalPedTooFarFromTarget, i)
													PRINTLN("---------->     KILL LIST PROCESS_AI_PED_BODY I have set iLocalPedTooFarFromTarget for ped ", i," AS GET_CLOSEST_PLAYER_VEH_TO_PED")
												ENDIF
											ENDIF
										ENDIF
									ELSE
										TASK_COMBAT_HATED_TARGETS_AROUND_PED(NET_TO_PED(serverBD.niMwPed[i]), 299 )
										PRINTLN("---------->     KILL LIST PROCESS_AI_PED_BODY I have told ped ", i ," to attack the players")
									ENDIF
								ENDIF


							ENDIF
						ENDIF
					BREAK
					
					CASE PED_AI_STATE_CLEAN_UP
						//-- Mission Vehicle is destroyed, flee
						IF GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niMwPed[i]), SCRIPT_TASK_SMART_FLEE_POINT) != PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niMwPed[i]), SCRIPT_TASK_SMART_FLEE_POINT) != WAITING_TO_START_TASK
							vVehCoords = GET_COORDS_OF_ANY_PLAYER_VEH()
							CLEAR_PED_TASKS(NET_TO_PED(serverbd.niMwPed[i]))
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(serverBD.niMwPed[i]), TRUE)
							TASK_SMART_FLEE_COORD(NET_TO_PED(serverBD.niMwPed[i]), vVehCoords, 10000.0, 999999)
							PRINTLN("---------->     KILL LIST PROCESS_AI_PED_BODY (PED_AI_STATE_ATTACK_PLAYERS) I have told ped ", i ," to flee coord ", vVehCoords)
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
		
//		IF i < MAX_MW_VEHS
//			IF serverBD.iPedPlayerTargetServer[i] <> -1
//				IF iPedPlayerTargetClient[i] <> serverBD.iPedPlayerTargetServer[i]
//					
//					iPedPlayerTargetClient[i] = serverBD.iPedPlayerTargetServer[i]
//					
//					#IF IS_DEBUG_BUILD
//						playerTargetDebug = INT_TO_PLAYERINDEX(serverBD.iPedPlayerTargetServer[i])
//						PRINTLN("---------->     KILL LIST PROCESS_AI_PED_BODY [CHECK_FOR_PILOT_TARGET_PLAYER_UPDATE] iPedPlayerTargetClient[", i, "] UPDATED TO ", serverBD.iPedPlayerTargetServer[i], " WHO IS PLAYER ", GET_PLAYER_NAME(playerTargetDebug))
//					#ENDIF
//
//				ENDIF
//			ENDIF
//		ENDIF
	ENDREPEAT
	
	
//	MAINTAIN_UW_VEHICLE_AND_PEDS_ONLY_DAMAGEABLE_BY_RELGROUPS()
ENDPROC

PROC CLEANUP_UNUSED_UW_PLAYER_VEHICLES()
	IF IS_BIT_SET(serverbd.iServerBitSet, biS_CleanupUnusedVehicles)
		EXIT
	ENDIF
	
	//-- Wait until the server has had a chance to lock out all players
	IF NOT HAS_NET_TIMER_STARTED(timeCleanupUnusedVeh)
		START_NET_TIMER(timeCleanupUnusedVeh)
		PRINTLN("---------->     KILL LIST [CLEANUP_UNUSED_UW_PLAYER_VEHICLES] STARTED timeCleanupUnusedVeh ")
		EXIT
	ENDIF
	
	//-- Wait until the server has had a chance to lock out all players
	IF NOT HAS_NET_TIMER_EXPIRED(timeCleanupUnusedVeh, 5000)
		EXIT
	ENDIF
	
	
	INT iNumVeh = GET_NUMBER_OF_UW_PLAYER_VEHICLES()
	INT i = 0
	
	REPEAT iNumVeh i
		IF IS_VEHICLE_EMPTY(NET_TO_VEH(serverBD.niVeh[i]), TRUE)
			CLEANUP_NET_ID(serverBD.niVeh[i])

			PRINTLN("---------->     KILL LIST [CLEANUP_UNUSED_UW_PLAYER_VEHICLES] Cleaned up this player veh ", i, " serverBD.iNumPlayerVeh now ", serverBD.iNumPlayerVeh)
		ENDIF
	ENDREPEAT
	
	SET_BIT(serverbd.iServerBitSet, biS_CleanupUnusedVehicles)
ENDPROC


//PURPOSE: Process the stages for the Server
PROC PROCESS_KILL_LIST_SERVER()
//	BOOL VehicleFull = TRUE
//	BOOL bAtLeastOnePlayer
	INT i
	INT iPlayerCount
	
//	#IF IS_DEBUG_BUILD
//		IF serverBD.bAllowWith2Players
//			IF serverBD.iVehicleSeats > 2
//				serverBD.iVehicleSeats = 2
//			ENDIF
//		ENDIF
	//	PRINTLN("     ---------->     KILL LIST - serverbd.iMinPlayers = ", serverbd.iMinPlayers, " serverBD.iVehicleSeats = ", serverBD.iVehicleSeats)
	
		IF serverbd.iMinPlayers > 0
			IF serverBD.iVehicleSeats <> serverbd.iMinPlayers
				serverBD.iVehicleSeats  = serverbd.iMinPlayers
				NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  PROCESS_KILL_LIST_SERVER OVERRIDING iVehSeats = ") NET_PRINT_INT(serverBD.iVehicleSeats)  NET_NL()
			ENDIF		
		ENDIF
//	#ENDIF
	
	SWITCH serverBD.eStage
		CASE eAD_IDLE
	//	bAtLeastOnePlayer = FALSE
			iPlayerCount = 0
			
			REPEAT serverBD.iVehicleSeats i
				IF serverBD.piPlayers[i] = INVALID_PLAYER_INDEX()
			//		VehicleFull = FALSE
			//		PRINTLN("     ---------->     KILL LIST - VehicleFull = FALSE i = ", i, " serverBD.iVehicleSeats = ", serverBD.iVehicleSeats)
				ELSE
					iPlayerCount++
					
					IF IS_BIT_SET(serverbd.iServerBitSet, biS_MissionStartExpired)
						PRINTLN("     ---------->     KILL LIST iPlayerCount NOW ", iPlayerCount, " as serverBD.piPlayers[", i,"] = ", GET_PLAYER_NAME(serverBD.piPlayers[i] ))
					ENDIF
			//		bAtLeastOnePlayer = TRUE
				ENDIF
			ENDREPEAT
			

			
			MAINTAIN_START_MISSION_TIMER_SERVER()
			
			MAINTAIN_NUMBER_OF_PLAYER_VEHS()
			
			IF IS_BIT_SET(serverbd.iServerBitSet, biS_MissionStartExpired)
			//	IF serverbd.iMinPlayers = -1	
					serverbd.iMinPlayers = iPlayerCount
			//	ENDIF
			ENDIF
			
			SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
			IF ARE_ALL_UW_PLAYER_VEHICLES_FULL()

				
				UPDATE_KILL_LIST_LEADERBOARD()
				serverBD.eStage = eAD_ATTACK
				NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  SERVER STAGE = eAD_ATTACK  as ARE_ALL_UW_PLAYER_VEHICLES_FULL  <----------     ") NET_NL()

			ELIF (IS_BIT_SET(serverbd.iServerBitSet, biS_MissionStartExpired) AND GET_NUMBER_OF_UW_PLAYER_VEHICLES() > 1)
				CLEANUP_UNUSED_UW_PLAYER_VEHICLES()
				UPDATE_KILL_LIST_LEADERBOARD()
				serverBD.eStage = eAD_ATTACK
				NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  SERVER STAGE = eAD_ATTACK  as biS_MissionStartExpired  <----------     ") NET_NL()
			ENDIF
			
			IF NOT IS_COMPETITIVE_UW()
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])
					IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[0])
						TickerEventData.TickerEvent = TICKER_EVENT_UW_FULL
						BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_NOT_IN_VEHICLE(NET_TO_VEH(serverBD.niVeh[0])))
						serverBD.eStage = eAD_OFF_MISSION
						NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  SERVER STAGE = eAD_OFF_MISSION as veh destroyed    <----------     ") NET_NL()
					ENDIF
				ENDIF
			ENDIF
			
			IF serverBD.eStage = eAD_IDLE
				IF NOT HAS_NET_TIMER_STARTED(serverBD.timeMissionTimeout)
				//	IF NOT IS_BIT_SET(serverbd.iServerBitSet, biS_AtLeastOnePlayerInVeh)
					//IF serverBD.iAtLeastOnePlayerBitSet = 0
					IF NOT HAS_NET_TIMER_STARTED(serverbd.timeMissionStart)
						START_NET_TIMER(serverBD.timeMissionTimeout)
						PRINTLN("     ---------->     KILL LIST STARTED timeMissionTimeout ")
					ENDIF
					
					
				ELSE
					IF HAS_NET_TIMER_STARTED(serverbd.timeMissionStart)
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST  - RESET TIMER 1")
						RESET_NET_TIMER(serverBD.timeMissionTimeout)
					ELSE
						IF HAS_NET_TIMER_EXPIRED(serverbd.timeMissionTimeout, GET_EVENT_EXPIRY_TIME())
							TickerEventData.TickerEvent = TICKER_EVENT_UW_FULL
//							BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_NOT_IN_VEHICLE(NET_TO_VEH(serverBD.niVeh[0])))
							BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT())
							SET_BIT(serverBD.iServerBitSet ,biS_TimeoutExpired)
				//			serverBD.eStage = eAD_OFF_MISSION
							MOVE_TO_EXPLODE_VEHICLE_STAGE()
							NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  SERVER STAGE = eAD_EXPLODE_VEH as nobody started mission    <----------     ") NET_NL()
						ENDIF
					ENDIF

				ENDIF
				
				IF IS_BIT_SET(serverBD.iServerBitSet, biS_TooFewValid)
				//	serverBD.eStage = eAD_EXPLODE_VEH
					MOVE_TO_EXPLODE_VEHICLE_STAGE()
					NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  SERVER STAGE = eAD_EXPLODE_VEH as biS_TooFewValid   <----------     ") NET_NL()
					
				ENDIF
			ENDIF
		BREAK
		
		CASE eAD_ATTACK
			MAINTAIN_MW_CREATION()
			
			MAINTAIN_MISSION_DURATION_TIMER_SERVER()
			
			CLEANUP_UNUSED_UW_PLAYER_VEHICLES()
			
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_DurationExpired)
				serverBD.eStage = eAD_OFF_MISSION
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     KILL LIST  - serverBD.eStage = eAD_OFF_MISSION as  biS_DurationExpired")
			ENDIF
		BREAK
		
		CASE eAD_OFF_MISSION
		BREAK
		CASE eAD_EXPLODE_VEH
		BREAK
		CASE eAD_CLEANUP
		BREAK
	ENDSWITCH
ENDPROC

#IF IS_DEBUG_BUILD
BOOL bWdToggle1
BOOL bWdToggle2
BOOL bWdToggle3
PROC DO_UW_ON_SCREEN_DEBUG()
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_1, KEYBOARD_MODIFIER_SHIFT, "SKIP")	
		IF NOT bWdToggle1
			bWdToggle1 = TRUE
		ELSE
			bWdToggle1 = FALSE
		ENDIF
	ENDIF
	
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_2, KEYBOARD_MODIFIER_SHIFT, "SKIP")	
		IF NOT bWdToggle2
			bWdToggle2 = TRUE
		ELSE
			bWdToggle2 = FALSE
		ENDIF
	ENDIF
	
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_3, KEYBOARD_MODIFIER_SHIFT, "SKIP")	
		IF NOT bWdToggle3
			bWdToggle3 = TRUE
		ELSE
			bWdToggle3 = FALSE
		ENDIF
	ENDIF
	IF bWdToggle1
		DISPLAY_KILL_LIST_STRING_AND_NUMBER(0.1, 0.2, "iCurrentWave", serverbd.iCurrentWave)
		DISPLAY_KILL_LIST_STRING_AND_NUMBER(0.1, 0.25, "Num Veh in Wave", GET_NUMBER_OF_VEHICLES_IN_CURRENT_WAVE())
		DISPLAY_KILL_LIST_STRING_AND_NUMBER(0.1, 0.3, "iVehicleSpawnedThisWave", serverbd.iVehicleSpawnedThisWave)
		DISPLAY_KILL_LIST_STRING_AND_NUMBER(0.1, 0.35, "Total Veh Spawn", serverbd.iMwTotalVehiclesSpawned)
		DISPLAY_KILL_LIST_STRING_AND_NUMBER(0.1, 0.4, "iVehKillsThisWave", serverbd.iVehKillsThisWave)
		DISPLAY_KILL_LIST_STRING_AND_NUMBER(0.1, 0.45, "Num Active Veh", GET_NUMBER_OF_ACTIVE_ENEMY_VEHICLES(TRUE)) 
		DISPLAY_KILL_LIST_STRING_AND_NUMBER(0.1, 0.5, "Min Veh B4 Next Wave", GET_MIN_NUMBER_OF_REMAINING_VEHICLES_FOR_WAVE_UPDATE())
		DISPLAY_KILL_LIST_STRING_AND_NUMBER(0.1, 0.55, "Total Ped Spawn", serverBD.iTotalUwPedsSpawned)
		DISPLAY_KILL_LIST_STRING_AND_NUMBER(0.1, 0.6, "Actual Total Kill", serverbd.iTotalKills)
		
	ENDIF
	
	IF bWdToggle2
		DISPLAY_KILL_LIST_STRING_AND_NUMBER(0.5, 0.2, "iCurrentWave", serverbd.iCurrentWave)
		DISPLAY_KILL_LIST_STRING_AND_NUMBER(0.5, 0.25, "Num Active On Foot", GET_NUMBER_OF_ACTIVE_ENEMY_GROUND_PEDS())
		DISPLAY_KILL_LIST_STRING_AND_NUMBER(0.5, 0.3, "On Ft Spwn This Wave", serverbd.iPedOnFootSpawnedThisWave)
		DISPLAY_KILL_LIST_STRING_AND_NUMBER(0.5, 0.35, "iMwPedsSpawned", serverbd.iMwPedsSpawned)
		DISPLAY_KILL_LIST_STRING_AND_NUMBER(0.5, 0.4, "iPedOnFootKillsThisWave", serverbd.iPedOnFootKillsThisWave)
		DISPLAY_KILL_LIST_STRING_AND_NUMBER(0.5, 0.45, "iTotalOnFootPedSpawns", serverbd.iTotalOnFootPedSpawns)
		DISPLAY_KILL_LIST_STRING_AND_NUMBER(0.5, 0.5, "Num On Ft in Wave",GET_NUMBER_OF_GROUND_PEDS_IN_CURRENT_WAVE()) 
		DISPLAY_KILL_LIST_STRING_AND_NUMBER(0.5, 0.55, "Max Num So Far", GET_MAX_NUMBER_OF_ON_FOOT_SPAWNS_SO_FAR())
		DISPLAY_KILL_LIST_STRING_AND_NUMBER(0.5, 0.6, "Exp. On Foot total", GET_TOTAL_NUMBER_OF_ON_FOOT_PED_SPAWNS_FOR_THIS_VARIATION())
	ENDIF
	
	IF bWdToggle3
		DISPLAY_KILL_LIST_STRING_AND_NUMBER(0.5, 0.2, "Veh 0 Kills", serverbd.iKillsPerVehicle[0])
		DISPLAY_KILL_LIST_STRING_AND_NUMBER(0.5, 0.25, "Veh 1 Kills", serverbd.iKillsPerVehicle[1])
	ENDIF
ENDPROC
#ENDIF

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                      //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT(MP_MISSION_DATA missionScriptArgs)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
		#IF IS_DEBUG_BUILD	
			NET_PRINT_TIME() NET_PRINT("MP: Starting mission ") NET_NL()
		#ENDIF
		
		// Vehry out all the initial game starting duties. 
		PROCESS_PRE_GAME(missionScriptArgs)	
		
		// Setup widgets. Make sure this is done after PROCESS_PRE_GAME()
		#IF IS_DEBUG_BUILD
			CREATE_WIDGETS()
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD NET_LOG("KILL LIST -  NETWORK_IS_GAME_IN_PROGRESS = FALSE")	#ENDIF
		NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  MISSION END - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP P     <----------     ") NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
	
	// Main loop
	WHILE TRUE

		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
				
		// If we have a match end event, bail
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			#IF IS_DEBUG_BUILD NET_LOG("SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")	#ENDIF
			NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  MISSION END - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE - SCRIPT CLEANUP B     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		ELIF SHOULD_MP_AMBIENT_SCRIPT_TERMINATE(MPAM_TYPE_KILL_LIST)
			#IF IS_DEBUG_BUILD NET_LOG("SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")	#ENDIF
			NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  MISSION END - SHOULD_MP_AMBIENT_SCRIPT_TERMINATE - SCRIPT CLEANUP      <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		
		IF IS_KILL_CURRENT_FREEMODE_EVENT_GLOBAL_SET() 
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     KILL LIST -  MISSION END - IS_KILL_CURRENT_FREEMODE_EVENT_GLOBAL_SET - SCRIPT CLEANUP F     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		
		//Call the common FM Events every frame callse
		FM_EVENT_COMMON_EVERY_FRAME_CALLS()
		
		//Check if player is in a tutorial session and end
//		IF NETWORK_IS_IN_TUTORIAL_SESSION()
//			#IF IS_DEBUG_BUILD NET_LOG("PLAYER IN TUT SESSION")	#ENDIF
//			NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  MISSION END - PLAYER IN TUTORIAL SESSION - SCRIPT CLEANUP C     <----------     ") NET_NL()
//			SCRIPT_CLEANUP()
//		ENDIF
		
		// Deal with the debug
		#IF IS_DEBUG_BUILD		
			UPDATE_WIDGETS()
			DO_UW_ON_SCREEN_DEBUG()
			
			IF NOT HAS_NET_TIMER_STARTED(timeDebug)
				START_NET_TIMER(timeDebug)
			ENDIF
			
			IF HAS_NET_TIMER_EXPIRED(timeDebug, 10000)
				bWdTimedDebug = TRUE
				CPRINTLN(DEBUG_NET_AMBIENT, "     ----------> SET bWdTimedDebug")
			ENDIF
				
		#ENDIF		
	

		// If we need to leave the mission, make sure we just straight to the end stage and terminate the script - DON'T NEED FOR AMBIENT MISSION
		/*IF SHOULD_PLAYER_LEAVE_MP_MISSION() 
			#IF IS_DEBUG_BUILD
			NET_PRINT_TIME() NET_PRINT( GET_MP_MISSION_NAME(thisMission), " Leave mission = TRUE \n")
			#ENDIF
			playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_LEAVE
			#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_LEAVE")	#ENDIF
		ENDIF*/
		
		
		#IF IS_DEBUG_BUILD
			IF bWdMyReward
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ----->    KILL LIST [CHECK_FOR_REWARD] [bWdMyReward] SET") 
			ENDIF
		#ENDIF
		
		// -----------------------------------
		// Process your game logic.....
		SWITCH GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT())
			
			// Wait untill the server gives the all go before moving on
			CASE GAME_STATE_INI
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
				AND serverbd.iHistorySubType != -1
				AND serverbd.iHistoryVariation != -1
					
					ADD_VEH_BLIP()
					
					FM_EVENT_BLOCK_SPECIFIC_GANG_ATTACKS(FMMC_TYPE_KILL_LIST, serverbd.iHistorySubType, serverbd.iHistoryVariation)
					
					IF MPGlobalsAmbience.iUrbanWarfareType = -1 
						IF NOT IS_COMPETITIVE_UW()
							MPGlobalsAmbience.iUrbanWarfareType = 0
						ELSE
							MPGlobalsAmbience.iUrbanWarfareType = 1
						ENDIF
					ENDIF
					
				//	SET_ALL_UW_VEHICLES_LOCKED(FALSE)
					
					FM_EVENT_SET_ACTIVE_FM_EVENT_LOCAL_ACTION_VECTOR(serverBD.vStartLocation[0])
					
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING
					NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_RUNNING")	#ENDIF
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 1    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY")	#ENDIF
				ENDIF
			BREAK
			
			// Main gameplay state.
			CASE GAME_STATE_RUNNING
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					
					
					PROCESS_AI_PED_BODY()
					
					MAINTAIN_ACTIVATE_DAMAGE_TRACKING_CLIENT()
					
					PROCESS_EVENTS()
					
					PROCESS_KILL_LIST_CLIENT()
					
					PROCESS_SCTV_PLAYER_KILL_LIST_CLIENT()
					
					MAINTAIN_LOCAL_PLAYER_AS_GHOST()
					
					MAINTAIN_PLAYER_VEHICLES_FALLING_THROUGH_WORLD()
					
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 2   <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY")	#ENDIF
				ENDIF
			BREAK
			
			CASE GAME_STATE_TERMINATE_DELAY 
				SET_MISSION_FINISHED(serverBD.MissionTerminateDelayTimer)
		      	IF IS_MISSION_READY_TO_CLEANUP(serverBD.MissionTerminateDelayTimer)
		            playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 3    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 3")	#ENDIF
		      	ENDIF
			BREAK
			
			// The game stage the local player is placed when we want him to leave a mission
			CASE GAME_STATE_LEAVE
				//LEAVE_MY_MP_MISSION(thisMission)
				playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
				NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 2    <----------     ") NET_NL()
				#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 2")	#ENDIF
			FALLTHRU
			
			//Cleans up then terminates the mission
			CASE GAME_STATE_END
				NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  SCRIPT CLEANUP A     <----------     ") NET_NL()
				SCRIPT_CLEANUP()
			BREAK

		ENDSWITCH
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			
			SWITCH GET_SERVER_MISSION_STATE()
				
				// For CnC group missions, we may want to wait for all players to have launched the script
				CASE GAME_STATE_INI
					IF GOT_UW_TYPE()
						IF CREATE_MAIN_VEHICLE()
							
							BROADCAST_SET_LAST_FM_EVENT_VARIATION(FM_EVENT_GET_VARIATION_FROM_EVENT_TYPE_AND_SUB_TYPE(FMMC_TYPE_KILL_LIST, serverbd.iHistorySubType, serverbd.iHistoryVariation))
							
							serverBD.iServerGameState = GAME_STATE_RUNNING
							
							PRINTLN("     ---------->     KILL LIST -  serverBD.iServerGameState = GAME_STATE_RUNNING    <----------   serverBD.iKillGoal =  ", serverBD.iKillGoal)
							#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_RUNNING")	#ENDIF
						ENDIF
					ENDIF
				BREAK
				
				// Look for game end conditions
				CASE GAME_STATE_RUNNING
					
					MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
					PROCESS_KILL_LIST_SERVER()
					PROCESS_AI_PED_BRAIN()
					
					#IF IS_DEBUG_BUILD
						IF bHostEndMissionNow
							MPGlobalsAmbience.bKillActiveEvent = FALSE
							serverBD.iServerGameState = GAME_STATE_END
							NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  serverBD.iServerGameState = GAME_STATE_END bHostEndMissionNow    <----------     ") NET_NL()
							#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 1")	#ENDIF
						ELIF MPGlobalsAmbience.bKillActiveEvent

							MPGlobalsAmbience.bKillActiveEvent = FALSE
							serverBD.iServerGameState = GAME_STATE_END
							NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  serverBD.iServerGameState = GAME_STATE_END MPGlobalsAmbience.bKillActiveEvent    <----------     ") NET_NL()
							
						ELIF IS_DEBUG_KEY_JUST_PRESSED(KEY_X, KEYBOARD_MODIFIER_SHIFT, "SKIP")
							MPGlobalsAmbience.bKillActiveEvent = FALSE
							serverBD.iServerGameState = GAME_STATE_END
							NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  serverBD.iServerGameState = GAME_STATE_END IS_DEBUG_KEY_JUST_PRESSED    <----------     ") NET_NL()
							
						ENDIF
					#ENDIF	
					
					IF HAVE_MISSION_END_CONDITIONS_BEEN_MET()
						serverBD.iServerGameState = GAME_STATE_END
						NET_PRINT_TIME() NET_PRINT("     ---------->     KILL LIST -  serverBD.iServerGameState = GAME_STATE_END 4    <----------     ") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 4")	#ENDIF
					ENDIF
					
				BREAK
				
				CASE GAME_STATE_END
				BREAK
				
			ENDSWITCH
			
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF bWdTimedDebug
				CPRINTLN(DEBUG_NET_AMBIENT, "     ----------> CLEAR bWdTimedDebug")
				bWdTimedDebug = FALSE
				RESET_NET_TIMER(timeDebug)
				START_NET_TIMER(timeDebug)
			ENDIF
		#ENDIF
	ENDWHILE
	
	
ENDSCRIPT
