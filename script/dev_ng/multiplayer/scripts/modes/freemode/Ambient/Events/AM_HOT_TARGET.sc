//////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_MOVING_TARGET.sc														//
// Description: Controls a Vehicle that needs to be Delivered by one player.			//
// Written by:  Steve Tiley																//
// Date: 09/03/2015																		//
//////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "net_mission.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "DM_Leaderboard.sch"
USING "net_challenges.sch"
USING "net_scoring_common.sch"
USING "net_ambience.sch"
USING "net_missions_at_coords_public.sch"
USING "commands_object.sch"
USING "net_xp_animation.sch"
USING "net_wait_zero.sch"
USING "am_common_ui.sch"
USING "freemode_events_header.sch"
USING "net_rank_ui.sch"
USING "net_gang_boss.sch"

#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF

// Game States. 
CONST_INT GAME_STATE_INI 				0
CONST_INT GAME_STATE_RUNNING			1
CONST_INT GAME_STATE_LEAVE				2
CONST_INT GAME_STATE_TERMINATE_DELAY	3
CONST_INT GAME_STATE_END				4

ENUM HOT_TARGET_STAGE_ENUM
	eAD_IDLE,
	eAD_DELIVER,
	eAD_KILL,
	eAD_OFF_MISSION,
	eAD_END,
	eAD_CLEANUP
ENDENUM

ENUM HOT_TARGET_CASH_RP_TYPE
	eHT_CASH_RP_DELIVERER,
	eHT_CASH_RP_CHASER,
	eHT_CASH_RP_PARTICIPANT
ENDENUM

CONST_INT VT_CAR	0
//CONST_INT VT_BOAT	1
CONST_INT VT_BIKE	1
CONST_INT VT_HELI	2
CONST_INT VT_MAX	3

CONST_INT NUM_CAR_MODELS 7
CONST_INT NUM_BIKE_MODELS 3
CONST_INT NUM_HELI_MODELS 5

CONST_INT VEH_EXP_RANGE	50

CONST_INT VEH_CLOSE_RANGE	20

CONST_INT TIME_MISSION_TIMEOUT 	600000
CONST_INT INDESTRUCTIBLE_TIMER 	20000
CONST_INT TIME_DELIVERY_TIMEOUT 600000
CONST_INT TIME_DEBUG			30000

//Server Bitset Data
CONST_INT biS_GunnerVehicle					0
CONST_INT biS_WaitingForGunner				1
CONST_INT biS_MissionTimedOut				2
CONST_INT biS_VehicleCanBeKilled			3
CONST_INT biS_PlayerShouldBePassive			4
CONST_INT biS_StartTimer					5
CONST_INT biS_VehicleDelivered				6
CONST_INT biS_HotTargetsOffMission			7
CONST_INT biS_DriverKilled					8
CONST_INT biS_GunnerKilled					9
CONST_INT biS_StartedWithoutGunner			10
CONST_INT biS_MissionEndTimerHit			11
CONST_INT biS_MissionShouldCleanup			12
CONST_INT biS_VehicleDestroyedBeforeStart	13
CONST_INT biS_HotTargetsKilled				14
CONST_INT biS_HotTargetsLeft				15
CONST_INT biS_VehicleStuck					16
CONST_INT biS_DriverLeft					17
CONST_INT biS_GunnerLeft					18
CONST_INT biS_MinPlayersBailServer			19
CONST_INT biS_EventEnded					20
CONST_INT biS_EventStarted					21
CONST_INT biS_ExplodeImmediately			22


// The server broadcast data.
// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData
	INT iServerGameState
		
	INT iServerBitSet
	
	NETWORK_INDEX niVeh
	
	PLAYER_INDEX piDriver
	PLAYER_INDEX piGunner
	PLAYER_INDEX piDeliverer
	PLAYER_INDEX piDriverKiller
	PLAYER_INDEX piGunnerKiller
	
	MODEL_NAMES VehModel 	= ZENTORNO
	INT iVehicleType		= VT_CAR
	INT iRoute
	INT iVariation
	
	VECTOR vStartLocation
	FLOAT fStartHeading
	VECTOR vDeliveryLocation
	FLOAT fDeliveryHeading
	VECTOR vDeliveryDimensions
		
	INT iExplosionTimeRemaining
	
	HOT_TARGET_STAGE_ENUM eStage = eAD_IDLE
	
	SCRIPT_TIMER MissionTerminateDelayTimer
	SCRIPT_TIMER DestroyCheckDelayTimer
	SCRIPT_TIMER ExplodeVehTimer
	SCRIPT_TIMER MissionTimeoutTimer
	SCRIPT_TIMER IndestructibleTimer
	SCRIPT_TIMER DeliverVehicleTimer
	SCRIPT_TIMER MissionEndStageTimer
	
	INT iHashedMac
	INT iMatchHistoryId
	
	INT iPlayersLeftInProgress 
	INT iPeakParticipants
	INT iPeakQualifiers
	
ENDSTRUCT

ServerBroadcastData serverBD
CHALLENGE_DPAD_STRUCT chDPStruct
scrFmEventAmbientMission telemetryStruct
//STRUCT_FM_EVENTS_END_UI sEndUiVars

CONST_INT biP_DeliveredVeh			0
CONST_INT biP_Driver				1
CONST_INT biP_Gunner				2
CONST_INT biP_LockedDoors			3
CONST_INT biP_OutOfVehicleFail		4
CONST_INT biP_NeedToWaitGunner		5
CONST_INT biP_ReceivedReward		6
CONST_INT biP_PlayerIsJIP			7
CONST_INT biP_IHaveLeft				8
CONST_INT biP_WithinQualifyDistance	9
CONST_INT biP_PlayerIsCriticalWhileHidden	10
CONST_INT biP_MinPlayersBailClient			11
CONST_INT biP_ShouldEndPlayersAbandoned		12
CONST_INT biP_ParticipantViaDistance		13
CONST_INT biP_ExplodeImmediately			14
CONST_INT biP_AbandonedForBossWork			15

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	INT iGameState
	
	INT iPlayerBitSet
	
	HOT_TARGET_STAGE_ENUM eStage = eAD_IDLE
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

///// MISSION VARIABLES /////
INT iBoolsBitSet
INT iBoolsBitSet2
INT iBoolsBitSet3
CONST_INT biHelp0Done			0
CONST_INT biHelp1Done			1
CONST_INT biHelp2Done			2
CONST_INT biHelp3Done			3
CONST_INT biHelp4Done			4
CONST_INT biHelp5Done			5
CONST_INT biHelp6Done			6
CONST_INT biHelp7Done			7
CONST_INT biHelp8Done			8
CONST_INT biHelp9Done			9
CONST_INT biHelp10Done			10
CONST_INT biHelp11Done			11
CONST_INT biHelp12Done			12
CONST_INT biHelp13Done			13
CONST_INT biHelp14Done			14
CONST_INT biBigM0Done			0
CONST_INT biBigM1Done			1
CONST_INT biBigM2Done			2
CONST_INT biBigM3Done			3
CONST_INT biBigM4Done			4
CONST_INT biBigM5Done			5
CONST_INT biBigM6Done			6
CONST_INT biBigM7Done			7
CONST_INT biBigM8Done			8
CONST_INT biBigM9Done			9
CONST_INT biBigM10Done			10
CONST_INT biBigM11Done			11
CONST_INT biBigM12Done			12
CONST_INT biBigM13Done			13
CONST_INT biBigM14Done			14
CONST_INT biBigM15Done			15
CONST_INT biBigM16Done			16
CONST_INT biEndTickerDone		0
CONST_INT biStartShardDone		1

INT iWaitForOtherOccupantTime =	60000

INT iStaggeredParticipant
INT iStaggeredCheck

INT iExplosionSound

INT iParticipationTime = 0

INT iTimeRemaining

BOOL bOnOff
BOOL bSetSpawnLocations
BOOL bStartSound = FALSE
BOOL bDriverLeft = FALSE
BOOL bGunnerLeft = FALSE
BOOL bHaveBeenInVeh = FALSE

BLIP_INDEX VehBlip
BLIP_INDEX PlayerBlip
BLIP_INDEX DeliveryBlip
CHECKPOINT_INDEX DeliveryCP

SCRIPT_TIMER OutOfVehTimer
SCRIPT_TIMER WaitForOtherTimer
SCRIPT_TIMER ParticipationTimer
SCRIPT_TIMER PostponeShard
SCRIPT_TIMER ExplosionSoundDelay

SCRIPT_TIMER iGarageCheckTimer
SCRIPT_TIMER iGarageSyncDelayTimer

// Widgets & Debug.
#IF IS_DEBUG_BUILD
	BOOL bCreateWidgets
	INT iWidgetStage
	WIDGET_GROUP_ID htWidgetGroup
	
	BOOL bHostEndMissionNow
	BOOL bWarpToVeh
	BOOL bDebugChangeTime = FALSE
#ENDIF

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MAIN PROCS                            ////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE:
///    Block or Unblock the blips for missions and jobs.
/// PARAMS:
///    block - if true, block, if false, unblock
PROC BLOCK_BLIPS(BOOL block)
	IF block
		PRINTLN("[AM MOVING TARGET] -  BLOCK_BLIPS - TRUE - Block_All_MissionsAtCoords_Missions(true)")
		Block_All_MissionsAtCoords_Missions(TRUE)
	ELSE
		PRINTLN("[AM MOVING TARGET] -  BLOCK_BLIPS - FALSE - Block_All_MissionsAtCoords_Missions(false)")
		Block_All_MissionsAtCoords_Missions(FALSE)
	ENDIF
ENDPROC

PROC REMOVE_BLIPS()
	IF DOES_BLIP_EXIST(DeliveryBlip)
		REMOVE_BLIP(DeliveryBlip)
		DELETE_CHECKPOINT(DeliveryCP)
	ENDIF
	IF DOES_BLIP_EXIST(VehBlip)
		REMOVE_BLIP(VehBlip)
	ENDIF
	IF DOES_BLIP_EXIST(PlayerBlip)
		REMOVE_BLIP(PlayerBlip)		
	ENDIF
	
	IF Is_There_Any_Current_Objective_Text_From_This_Script()
		Clear_Any_Objective_Text()
	ENDIF
ENDPROC

///// PURPOSE:
/////    Sets the player's global broadcast data for their current mission
///// PARAMS:
/////    IsHotTarget - If true, set current mission to hot target, else set default
//PROC SET_LOCAL_PLAYER_FMMC_MISSION_TYPE_HOT_TARGET(BOOL IsHotTarget)
//	IF IsHotTarget
//		PRINTLN("[AM MOVING TARGET] -  SET_LOCAL_PLAYER_FMMC_MISSION_TYPE_HOT_TARGET - previous type:   iCurrentMissionType: ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType)
//		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MOVING_TARGET
//		PRINTLN("[AM MOVING TARGET] -  SET_LOCAL_PLAYER_FMMC_MISSION_TYPE_HOT_TARGET - new type:   iCurrentMissionType: ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType)
//	ENDIF
//ENDPROC

//Helper function to get the servers game/mission state
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC


//Cleanup Mission Data
PROC SCRIPT_CLEANUP()
	
	BLOCK_BLIPS(FALSE)	
	
	REMOVE_BLIPS()
	
//	IF PLAYER_ID() = serverBD.piDriver
//		IF serverBD.piGunner != INVALID_PLAYER_INDEX()
//			BROADCAST_LEAVE_VEHICLE(SPECIFIC_PLAYER(serverBD.piGunner), FALSE, 0, 0, FALSE, FALSE)
//		ENDIF
//	ENDIF
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
	AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		CLEAR_ALL_PED_VEHICLE_FORCED_SEAT_USAGE(PLAYER_PED_ID())
	ENDIF
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NETWORK_IS_PLAYER_IN_MP_CUTSCENE(PLAYER_ID())
			SET_NETWORK_CUTSCENE_ENTITIES(FALSE)
		ENDIF
	ENDIF
	IF GET_SERVER_MISSION_STATE() = GAME_STATE_END
	AND serverBD.VehModel != DUMMY_MODEL_FOR_SCRIPT
		SET_VEHICLE_MODEL_IS_SUPPRESSED(serverBD.VehModel, FALSE)
		NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  SET_VEHICLE_MODEL_IS_SUPPRESSED - FALSE      <----------     ") NET_NL()
	ENDIF
	
	CLEAR_BIT(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_PLAYER_USING_DESTROY_VEH)
	IF bSetSpawnLocations
		//Clear the spawn locations
		PRINTLN("     ---------->     [AM MOVING TARGET] - CLEAR_PLAYER_NEXT_RESPAWN_LOCATION   <----------     ")
		CLEAR_PLAYER_NEXT_RESPAWN_LOCATION()
		CLEAR_SPECIFIC_SPAWN_LOCATION_FROM_THIS_SCRIPT()
	ENDIF
	
	IF IS_INTERACTION_MENU_DISABLED()
		PRINTLN("[AM MOVING TARGET] - SCRIPT_CLEANUP - enabling interaction menu as it was previously disabled")
		ENABLE_INTERACTION_MENU()
	ENDIF
	
	SET_FREEMODE_FLOW_COMMS_DISABLED(FALSE)
	
	SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_MOVING_TARGET, FALSE)
	
//	IF IS_FM_PLAYER_CRITICAL_TO_THIS_FM_EVENT(PLAYER_ID(), FMMC_TYPE_MOVING_TARGET)
//		FM_EVENT_SET_PLAYER_IS_CRITICAL_TO_FM_EVENT(FALSE)
//	ENDIF
//	IF FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_FM_EVENT(PLAYER_ID())
//		FM_EVENT_SET_PLAYER_IS_PERMANENT_PARTICIPANT_IN_FM_EVENT(FALSE)
//	ENDIF
	
	Clear_MissionsAtCoords_Local_Disabled_Area()
	
	IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		telemetryStruct.m_endReason             =AE_END_FORCED
	ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_MinPlayersBailServer)
		telemetryStruct.m_endReason             =AE_END_LOW_NUMBERS
	ELSE
		IF telemetryStruct.m_endReason             !=AE_END_WON
			telemetryStruct.m_endReason             =AE_END_LOST
		ENDIF
	ENDIF
	
	telemetryStruct.m_uid0 = serverBD.iHashedMac
	telemetryStruct.m_uid1 = serverBD.iMatchHistoryId
	telemetryStruct.m_playersLeftInProgress = serverBD.iPlayersLeftInProgress
	telemetryStruct.m_playersParticipating = serverBD.iPeakQualifiers
	
	telemetryStruct.m_timeTakenToComplete = GET_CLOUD_TIME_AS_INT() - telemetryStruct.m_startTime
	
	IF NOT g_sMPTunables.bHot_Target_Disable_Share_Cash
		IF telemetryStruct.m_cashEarned > 0	
//			SET_LAST_JOB_DATA(LAST_JOB_HOT_TARGET, telemetryStruct.m_cashEarned)
			IF telemetryStruct.m_endReason             =AE_END_WON
				SET_DO_FM_EVENT_SHARE_CASH_HELP_IF_NEEDED()
			ENDIF
		ENDIF
	ENDIF
	
	PROCESS_CURRENT_AMBIENT_EVENT_PLAYSTATS(telemetryStruct, serverBD.iVehicleType)
	
	#IF IS_DEBUG_BUILD
		IF bHostEndMissionNow
		OR MPGlobalsAmbience.bKillActiveEvent
			MPGlobalsAmbience.bKillActiveEvent = FALSE
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
				IF TAKE_CONTROL_OF_NET_ID(serverBD.niVeh)
					DELETE_NET_ID(serverBD.niVeh)
					NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  SCRIPT_CLEANUP - DEBUG KILLED, DELETE_NET_ID called    <----------     ") NET_NL()
				ENDIF				
			ENDIF
			NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  SCRIPT_CLEANUP - DEBUG KILLED, DELETING VEHICLE    <----------     ") NET_NL()
			#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 1")	#ENDIF
		ENDIF
	#ENDIF	
	
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
		IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh))
			IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh), TRUE)
				TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh))
				PRINTLN("[AM MOVING TARGET] -  SCRIPT_CLEANUP - Player is in the vehicle, tasked with leaving.")
			ENDIF
		ENDIF
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NATIVE_TO_INT(PLAYER_ID()) != -1
			CLEAR_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iFmAmbientEventBitSet, ciSET_PLAYER_IN_MOVING_TARGET_VEHICLE)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_EventEnded)
		PRINTLN("[AM MOVING TARGET] - SCRIPT_CLEANUP - biS_EventEnded SET, players is cleaning with:  COMMON_FREEMODE_AMBIENT_EVENTS_CLEAN_UP(FMMC_TYPE_MOVING_TARGET, FALSE, TRUE)")
		COMMON_FREEMODE_AMBIENT_EVENTS_CLEAN_UP(FMMC_TYPE_MOVING_TARGET, FALSE, TRUE)
	ELSE
		PRINTLN("[AM MOVING TARGET] - SCRIPT_CLEANUP - biS_EventEnded ** NOT ** SET, players is cleaning with:  COMMON_FREEMODE_AMBIENT_EVENTS_CLEAN_UP(FMMC_TYPE_MOVING_TARGET, FALSE, FALSE)")
		COMMON_FREEMODE_AMBIENT_EVENTS_CLEAN_UP(FMMC_TYPE_MOVING_TARGET, FALSE, FALSE)
	ENDIF
	
	NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  CLEANUP MISSION      <----------     ") NET_NL()
	#IF IS_DEBUG_BUILD NET_LOG("SCRIPT_CLEANUP")	#ENDIF
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

FUNC INT GET_MISSION_TIME()

	#IF IS_DEBUG_BUILD
		IF MPGlobalsAmbience.bEndCurrentFreemodeEvent
			RETURN TIME_DEBUG
		ENDIF	
	#ENDIF

	RETURN g_sMPTunables.iHot_Target_Event_Time_Limit

ENDFUNC

FUNC BOOL HAS_PLAYER_FULLY_SPAWNED()
	IF IS_SCREEN_FADED_IN()
	AND GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState = RESPAWN_STATE_PLAYING
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Getsa  random vehicl model to use
FUNC MODEL_NAMES GET_RANDOM_VEH_MODEL()
	INT iRand
	MODEL_NAMES modelToReturn = PANTO
	SWITCH serverBD.iVehicleType
		CASE VT_CAR
			iRand = GET_RANDOM_INT_IN_RANGE(0, 7)
			#IF IS_DEBUG_BUILD
				IF MPGlobalsAmbience.iHotTargetVeh > -1
				AND MPGlobalsAmbience.iHotTargetVeh < 7
					iRand = MPGlobalsAmbience.iHotTargetVeh
					PRINTLN("[AM MOVING TARGET] - CAR - DEBUG - MPGlobalsAmbience.iHotTargetVeh = ", MPGlobalsAmbience.iHotTargetVeh)
				ENDIF
			#ENDIF
			
			SWITCH iRand
				CASE 0	modelToReturn = ZENTORNO	BREAK
				CASE 1	modelToReturn = DUKES2		BREAK
				CASE 2	modelToReturn = KURUMA2		BREAK
				CASE 3	modelToReturn = GUARDIAN	BREAK
				CASE 4	modelToReturn = PANTO		BREAK
				CASE 5	modelToReturn = INSURGENT	BREAK
				CASE 6	modelToReturn = TECHNICAL	BREAK
			ENDSWITCH
		BREAK
		CASE VT_BIKE
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			#IF IS_DEBUG_BUILD
				IF MPGlobalsAmbience.iHotTargetVeh > -1
				AND MPGlobalsAmbience.iHotTargetVeh < 3
					iRand = MPGlobalsAmbience.iHotTargetVeh
					PRINTLN("[AM MOVING TARGET] - BIKE - DEBUG - MPGlobalsAmbience.iHotTargetVeh = ", MPGlobalsAmbience.iHotTargetVeh)
				ENDIF
			#ENDIF
			
			SWITCH iRand
				CASE 0	modelToReturn = ENDURO		BREAK
				CASE 1	modelToReturn = LECTRO		BREAK
				CASE 2	modelToReturn = VINDICATOR		BREAK
			ENDSWITCH
		BREAK
		CASE VT_HELI
			iRand = GET_RANDOM_INT_IN_RANGE(0, 5)
			#IF IS_DEBUG_BUILD
				IF MPGlobalsAmbience.iHotTargetVeh > -1
				AND MPGlobalsAmbience.iHotTargetVeh < 5
					iRand = MPGlobalsAmbience.iHotTargetVeh
					PRINTLN("[AM MOVING TARGET] - HELI - DEBUG - MPGlobalsAmbience.iHotTargetVeh = ", MPGlobalsAmbience.iHotTargetVeh)
				ENDIF
			#ENDIF
			
			SWITCH iRand
				CASE 0	modelToReturn = FROGGER		BREAK
				CASE 1	modelToReturn = MAVERICK	BREAK
				CASE 2	modelToReturn = SWIFT		BREAK
				CASE 3 	modelToReturn = BUZZARD		BREAK
				CASE 4	modelToReturn = VALKYRIE	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	// If change vehicle using the M menu, make sure the last route used is 0 so that we dont jump routes
	#IF IS_DEBUG_BUILD
		IF MPGlobalsAmbience.iHotTargetVeh != MPGlobalsAmbience.iHotTargetLastVeh
			MPGlobalsAmbience.iHotTargetLastVeh = MPGlobalsAmbience.iHotTargetVeh
			MPGlobalsAmbience.iHotTargetLastRoute = -1
		ENDIF
	#ENDIF

	RETURN modelToReturn
ENDFUNC

FUNC INT GET_NUM_VEHICLES_FOR_TYPE()
	
	IF serverBD.iVehicleType = VT_CAR
		RETURN NUM_CAR_MODELS
	ELIF serverBD.iVehicleType = VT_BIKE
		RETURN NUM_BIKE_MODELS
	ELSE
		RETURN NUM_HELI_MODELS
	ENDIF

ENDFUNC

FUNC MODEL_NAMES GET_VALID_VEHICLE_MODEL_FROM_TYPE()

	MODEL_NAMES modelToReturn
	INT i
	INT iVehicleDisabled[7]
	INT iValidVehicle[7]			// make 7 in size because theres no more than 7 vehicles per type
	MODEL_NAMES eVehicle[7]
	INT iValidCount

	SWITCH serverBD.iVehicleType	
		CASE VT_CAR
			PRINTLN("[AM MOVING TARGET] - vehicle type: VT_CAR")
			iVehicleDisabled[0] = BOOL_TO_INT(g_sMPTunables.bHot_Target_Disable_Car_Zentorono)
			eVehicle[0]			= ZENTORNO
			PRINTLN("[AM MOVING TARGET] - VT_CAR - g_sMPTunables.bHot_Target_Disable_Car_Zentorono = ", g_sMPTunables.bHot_Target_Disable_Car_Zentorono)
			iVehicleDisabled[1] = BOOL_TO_INT(g_sMPTunables.bHot_Target_Disable_Car_Duke_O_Death)
			eVehicle[1]			= DUKES2
			PRINTLN("[AM MOVING TARGET] - VT_CAR - g_sMPTunables.bHot_Target_Disable_Car_Duke_O_Death = ", g_sMPTunables.bHot_Target_Disable_Car_Duke_O_Death)
			iVehicleDisabled[2] = BOOL_TO_INT(g_sMPTunables.bHot_Target_Disable_Car_Kuruma)
			eVehicle[2]			= KURUMA2			
			PRINTLN("[AM MOVING TARGET] - VT_CAR - g_sMPTunables.bHot_Target_Disable_Car_Kuruma = ", g_sMPTunables.bHot_Target_Disable_Car_Kuruma)
			iVehicleDisabled[3] = BOOL_TO_INT(g_sMPTunables.bHot_Target_Disable_Car_Guardian)
			eVehicle[3]			= GUARDIAN			
			PRINTLN("[AM MOVING TARGET] - VT_CAR - g_sMPTunables.bHot_Target_Disable_Car_Guardian = ", g_sMPTunables.bHot_Target_Disable_Car_Guardian)
			iVehicleDisabled[4] = BOOL_TO_INT(g_sMPTunables.bHot_Target_Disable_Car_Panto)
			eVehicle[4]			= PANTO			
			PRINTLN("[AM MOVING TARGET] - VT_CAR - g_sMPTunables.bHot_Target_Disable_Car_Panto = ", g_sMPTunables.bHot_Target_Disable_Car_Panto)
			iVehicleDisabled[5] = BOOL_TO_INT(g_sMPTunables.bHot_Target_Disable_Car_Insurgent)
			eVehicle[5]			= INSURGENT			
			PRINTLN("[AM MOVING TARGET] - VT_CAR - g_sMPTunables.bHot_Target_Disable_Car_Insurgent = ", g_sMPTunables.bHot_Target_Disable_Car_Insurgent)
			iVehicleDisabled[6] = BOOL_TO_INT(g_sMPTunables.bHot_Target_Disable_Car_Technical)
			eVehicle[6]			= TECHNICAL		
			PRINTLN("[AM MOVING TARGET] - VT_CAR - g_sMPTunables.bHot_Target_Disable_Car_Technical = ", g_sMPTunables.bHot_Target_Disable_Car_Technical)
		BREAK		
		CASE VT_BIKE
			PRINTLN("[AM MOVING TARGET] - vehicle type: VT_BIKE")
			iVehicleDisabled[0] = BOOL_TO_INT(g_sMPTunables.bHot_Target_Disable_Bike_Enduro)
			eVehicle[0]			= ENDURO		
			PRINTLN("[AM MOVING TARGET] - VT_BIKE - g_sMPTunables.bHot_Target_Disable_Bike_Enduro = ", g_sMPTunables.bHot_Target_Disable_Bike_Enduro)
			iVehicleDisabled[1] = BOOL_TO_INT(g_sMPTunables.bHot_Target_Disable_Bike_Lectro)
			eVehicle[1]			= LECTRO			
			PRINTLN("[AM MOVING TARGET] - VT_BIKE - g_sMPTunables.bHot_Target_Disable_Bike_Lectro = ", g_sMPTunables.bHot_Target_Disable_Bike_Lectro)
			iVehicleDisabled[2] = BOOL_TO_INT(g_sMPTunables.bHot_Target_Disable_Bike_Vindicator)
			eVehicle[2]			= VINDICATOR
			PRINTLN("[AM MOVING TARGET] - VT_BIKE - g_sMPTunables.bHot_Target_Disable_Bike_Vindicator = ", g_sMPTunables.bHot_Target_Disable_Bike_Vindicator)
		BREAK		
		CASE VT_HELI
			PRINTLN("[AM MOVING TARGET] - vehicle type: VT_HELI")
			iVehicleDisabled[0] = BOOL_TO_INT(g_sMPTunables.bHot_Target_Disable_Heli_Frogger)
			eVehicle[0]			= FROGGER			
			PRINTLN("[AM MOVING TARGET] - VT_HELI - g_sMPTunables.bHot_Target_Disable_Heli_Frogger = ", g_sMPTunables.bHot_Target_Disable_Heli_Frogger)
			iVehicleDisabled[1] = BOOL_TO_INT(g_sMPTunables.bHot_Target_Disable_Heli_Maverick)
			eVehicle[1]			= MAVERICK			
			PRINTLN("[AM MOVING TARGET] - VT_HELI - g_sMPTunables.bHot_Target_Disable_Heli_Maverick = ", g_sMPTunables.bHot_Target_Disable_Heli_Maverick)
			iVehicleDisabled[2] = BOOL_TO_INT(g_sMPTunables.bHot_Target_Disable_Heli_Swift)
			eVehicle[2]			= SWIFT			
			PRINTLN("[AM MOVING TARGET] - VT_HELI - g_sMPTunables.bHot_Target_Disable_Heli_Swift = ", g_sMPTunables.bHot_Target_Disable_Heli_Swift)
			iVehicleDisabled[3] = BOOL_TO_INT(g_sMPTunables.bHot_Target_Disable_Heli_Buzzard)
			eVehicle[3]			= BUZZARD			
			PRINTLN("[AM MOVING TARGET] - VT_HELI - g_sMPTunables.bHot_Target_Disable_Heli_Buzzard = ", g_sMPTunables.bHot_Target_Disable_Heli_Buzzard)
			iVehicleDisabled[4] = BOOL_TO_INT(g_sMPTunables.bHot_Target_Disable_Heli_Valkyrie)
			eVehicle[4]			= VALKYRIE
			PRINTLN("[AM MOVING TARGET] - VT_HELI - g_sMPTunables.bHot_Target_Disable_Heli_Valkyrie = ", g_sMPTunables.bHot_Target_Disable_Heli_Valkyrie)
		BREAK	
	ENDSWITCH
		
	#IF IS_DEBUG_BUILD
		IF MPGlobalsAmbience.iHotTargetVeh > -1
		AND MPGlobalsAmbience.iHotTargetVeh < GET_NUM_VEHICLES_FOR_TYPE()
			RETURN eVehicle[MPGlobalsAmbience.iHotTargetVeh]
			PRINTLN("[AM MOVING TARGET] - CAR - DEBUG - MPGlobalsAmbience.iHotTargetVeh = ", MPGlobalsAmbience.iHotTargetVeh)
		ENDIF
	#ENDIF
	
	i = 0
		
	REPEAT GET_NUM_VEHICLES_FOR_TYPE() i
		IF iVehicleDisabled[i] = 0
			iValidVehicle[iValidCount] = i
			iValidCount++
			PRINTLN("[AM MOVING TARGET] - GET_VALID_VEHICLE_MODEL_FROM_TYPE - Adding iVehicleDisabled[", i,"] to valid route array element [", iValidCount,"] - total valid routes: ", iValidCount)
		ELSE
			PRINTLN("[AM MOVING TARGET] - GET_VALID_VEHICLE_MODEL_FROM_TYPE - Vehicle iVehicleDisabled[", i,"] has been disabled")
		ENDIF
	ENDREPEAT		

	IF iValidCount > 0	
		i = iValidVehicle[GET_RANDOM_INT_IN_RANGE(0, iValidCount)]
		modelToReturn = eVehicle[i]
		PRINTLN("[AM MOVING TARGET] - GET_VALID_VEHICLE_MODEL_FROM_TYPE - Found suitable model - model selected randomly for valid list is ", GET_MODEL_NAME_FOR_DEBUG(modelToReturn))
	ELSE
		IF serverBD.iVehicleType = VT_CAR
			modelToReturn = PANTO
		ELIF serverBD.iVehicleType = VT_BIKE
			modelToReturn = ENDURO
		ELSE
			modelToReturn = FROGGER
		ENDIF
		PRINTLN("[AM MOVING TARGET] - GET_VALID_VEHICLE_MODEL_FROM_TYPE - Failed to find a suitable model - all routes blocked by debug - returning ", modelToReturn," to launch by default")
	ENDIF
			
			// If change vehicle using the M menu, make sure the last route used is 0 so that we dont jump routes
	#IF IS_DEBUG_BUILD
		IF MPGlobalsAmbience.iHotTargetVeh != MPGlobalsAmbience.iHotTargetLastVeh
			MPGlobalsAmbience.iHotTargetLastVeh = MPGlobalsAmbience.iHotTargetVeh
			MPGlobalsAmbience.iHotTargetLastRoute = -1
		ENDIF
	#ENDIF
	
	RETURN modelToReturn

ENDFUNC

//PURPOSE: Get Random Location for Veh to be Parked at
PROC GET_RANDOM_LOCATIONS(VECTOR &vStartLocation, FLOAT &fStartHeading, VECTOR &vDelLocation, FLOAT &fDelHeading, VECTOR &vDelDimensions, INT &iRoute, INT &iVariation)
	
	SWITCH serverBD.iVehicleType
		CASE VT_CAR
						
			iVariation = GET_RANDOM_INT_IN_RANGE(0, 5)
			#IF IS_DEBUG_BUILD
				IF MPGlobalsAmbience.iHotTargetLastRoute > 4
					MPGlobalsAmbience.iHotTargetLastRoute = -1
				ENDIF
			
				IF MPGlobalsAmbience.iHotTargetRoute > -1
				AND MPGlobalsAmbience.iHotTargetRoute < 5
					iVariation = MPGlobalsAmbience.iHotTargetRoute
					PRINTLN("[AM MOVING TARGET] - CAR - DEBUG - MPGlobalsAmbience.iHotTargetRoute = ", MPGlobalsAmbience.iHotTargetRoute)
				ELIF MPGlobalsAmbience.iHotTargetRoute = -2
					IF MPGlobalsAmbience.iHotTargetLastRoute = -1
					OR MPGlobalsAmbience.eHotTargetLastModel != serverBD.VehModel
						iVariation = 0
						MPGlobalsAmbience.iHotTargetLastRoute = 0
						MPGlobalsAmbience.eHotTargetLastModel = serverBD.VehModel
					ELSE
						iVariation = MPGlobalsAmbience.iHotTargetLastRoute
					ENDIF
					PRINTLN("[AM MOVING TARGET] - CAR - DEBUG - M MENU - Using route ", iVariation)
				ENDIF
			#ENDIF
						
			SWITCH iRoute
				CASE 0	// ZENTORNO
					SWITCH iVariation
						CASE 0
							vStartLocation = <<770.3509, -2943.8650, 4.8007>>	fStartHeading = 114.4992									//
							vDelLocation = <<145.7373, 6603.7178, 30.8530>>		fDelHeading = 177.3552		vDelDimensions = <<5, 5, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 1
							vStartLocation = <<-830.3174, -2492.2834, 12.8306>> 	fStartHeading = 105.6033									//
							vDelLocation = <<-696.6201, 5780.8242, 16.3310>> 		fDelHeading = 155.9795		vDelDimensions = <<5, 5, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 2
							vStartLocation = <<-1889.7056, 2047.2561, 139.8729>>	fStartHeading = 250.5478									//VINEYARD
							vDelLocation = <<3324.7908, 5146.7754, 17.2660>>		fDelHeading = 101.5424		vDelDimensions = <<5, 5, 3>>	//LIGHTHOUSE
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 3
							vStartLocation = <<-675.6837, 901.9085, 229.5803>> 	fStartHeading = 324.4914									//
							vDelLocation = <<1598.1444, 6566.4819, 12.5579>>		fDelHeading = 185.8277		vDelDimensions = <<5, 5, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 4
							vStartLocation = <<3542.6687, 3796.4587, 29.2704>> 	fStartHeading = 179.2647									//
							vDelLocation = <<-3052.7883, 139.7021, 10.5705>>	fDelHeading = 268.2207		vDelDimensions = <<7, 7, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
					ENDSWITCH
					
					#IF IS_DEBUG_BUILD MPGlobalsAmbience.eHotTargetLastModel = ZENTORNO	#ENDIF
				BREAK
				CASE 1 // DUKES2
					SWITCH iVariation
						CASE 0
							vStartLocation = <<758.0045, 2522.6257, 72.1734>> 	fStartHeading = 90.2108									//
							vDelLocation = <<-1583.3655, 5165.6621, 18.5750>>	fDelHeading = 194.4020		vDelDimensions = <<7, 7, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 1
							vStartLocation = <<2528.7915, 4963.0664, 43.3141>> 	fStartHeading = 21.8785									//
							vDelLocation = <<1731.7380, -1671.2439, 111.5825>>	fDelHeading = 99.1351		vDelDimensions = <<7, 7, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 2
							vStartLocation = <<727.8987, 1299.9961, 359.2960>>	fStartHeading = 197.8385									//
							vDelLocation = <<2117.7415, 4770.1973, 40.1357>>	fDelHeading = 109.7547		vDelDimensions = <<7, 7, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 3
							vStartLocation = <<-441.7706, 1588.5844, 357.0363>>	fStartHeading = 273.1774									//JUICE STAND
							vDelLocation = <<1540.2043, 6336.0454, 23.0745>>	fDelHeading = 61.0701		vDelDimensions = <<7, 7, 3>>	//DIGNITY VILLAGE
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 4
							vStartLocation = <<-2551.2908, 2324.7578, 32.0599>>	fStartHeading = 274.1860									//
							vDelLocation = <<2790.4546, 2847.2524, 35.2290>>	fDelHeading = 139.5728		vDelDimensions = <<7, 7, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
					ENDSWITCH
					
					#IF IS_DEBUG_BUILD MPGlobalsAmbience.eHotTargetLastModel = DUKES2	#ENDIF
				BREAK
				CASE 2 // KURUMA2
					SWITCH iVariation
						CASE 0
							vStartLocation = <<1663.6041, 2.3129, 172.7751>> 	fStartHeading = 178.7987									//LAND ACT DAM
							vDelLocation = <<3816.7937, 4476.1919, 2.3695>>		fDelHeading = 116.6818  	vDelDimensions = <<7, 7, 3>>	//EL GORDO FISHING VILLAGE
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 1
							vStartLocation = <<2546.9568, 4646.5625, 33.0768>>	fStartHeading = 314.3235									//
							vDelLocation = <<-2244.1152, 4324.3506, 46.2968>>	fDelHeading = 149.5415		vDelDimensions = <<7, 7, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 2
							vStartLocation = <<964.2011, -1856.4625, 30.1970>>	fStartHeading = 83.0263									//
							vDelLocation = <<329.3773, 2870.8928, 42.4489>>		fDelHeading = 154.0668		vDelDimensions = <<7, 7, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 3
							vStartLocation = <<146.6418, -3183.7488, 4.8576>>	fStartHeading = 180.1581									//
							vDelLocation = <<2885.9097, 4473.3159, 47.1125>>	fDelHeading = 67.58			vDelDimensions = <<7, 7, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 4
							vStartLocation = <<-1134.2776, -2036.7109, 12.2110>>	fStartHeading = 315.0979									//
							vDelLocation = <<-313.4081, 2730.7900, 67.2082>> 		fDelHeading = 117.4930		vDelDimensions = <<7, 7, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
					ENDSWITCH
					
					#IF IS_DEBUG_BUILD MPGlobalsAmbience.eHotTargetLastModel = KURUMA2	#ENDIF
				BREAK				
				CASE 3 // GUARDIAN
					SWITCH iVariation
						CASE 0
							vStartLocation = <<2081.8496, 2334.1157, 93.3748>> 	fStartHeading = 53.7173										//
							vDelLocation = <<-1029.5, 4920.8, 205.4>>			fDelHeading = 173.7321		vDelDimensions = <<7, 7, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 1
							vStartLocation = <<-59.8025, 1961.2294, 189.1861>>	fStartHeading = 109.2191									//
							vDelLocation = <<-465.6203, -2761.0583, 5.0004>>	fDelHeading = 45.3497		vDelDimensions = <<7, 7, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 2
							vStartLocation = <<259.2158, 2589.4929, 43.9541>>	fStartHeading = 10.5362										//
							vDelLocation = <<-384.7157, 6098.6377, 30.4445>>	fDelHeading = 300.0612		vDelDimensions = <<7, 7, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 3
							vStartLocation = <<-1364.2094, 733.9630, 182.9186>> 	fStartHeading = 277.8043									//TREVOR OUTLOOK
							vDelLocation = <<715.4648, 4171.4795, 39.7092>>			fDelHeading = 291.0038		vDelDimensions = <<7, 7, 3>>	//ALAMO SEA VILLAGE
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 4
							vStartLocation = <<-573.6541, -1636.1146, 18.5734>>	fStartHeading = 160.7541									//
							vDelLocation = <<2865.1838, 1513.8206, 23.5675>>	fDelHeading = 180.2175		vDelDimensions = <<7, 7, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
					ENDSWITCH
					
					#IF IS_DEBUG_BUILD MPGlobalsAmbience.eHotTargetLastModel = GUARDIAN	#ENDIF
				BREAK
				CASE 4 // PANTO
					SWITCH iVariation
						CASE 0
							vStartLocation = <<-1124.8699, -1448.5769, 4.0208>> 	fStartHeading = 306.4299									//
							vDelLocation = <<684.5225, 604.2839, 127.9110>>			fDelHeading = 338.3539		vDelDimensions = <<7, 7, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 1
							vStartLocation = <<-378.5530, 281.2126, 83.8296>> 	fStartHeading = 16.4178										//
							vDelLocation = <<857.4160, -2496.8396, 27.3186>>	fDelHeading = 81.4812		vDelDimensions = <<7, 7, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 2
							vStartLocation = <<-1177.8929, -1772.3972, 2.8466>> 	fStartHeading = 287.3427									//COAST GUARD TOWER
							vDelLocation = <<-399.0450, 1203.6542, 324.6416>>	 	fDelHeading = 163.0898  	vDelDimensions = <<7, 7, 3>>	//OBSERVATORY
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 3
							vStartLocation = <<-1051.7864, -2635.6609, 35.6050>>	fStartHeading = 329.3701									//
							vDelLocation = <<-2352.9165, 267.3562, 164.4113>> 		fDelHeading = 23.1567		vDelDimensions = <<7, 7, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 4
							vStartLocation = <<-3249.6047, 986.9637, 11.4887>>	fStartHeading = 2.3514										//
							vDelLocation = <<230.9669, 1224.7356, 224.4599>>	fDelHeading = 191.1091		vDelDimensions = <<7, 7, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
					ENDSWITCH
					
					#IF IS_DEBUG_BUILD MPGlobalsAmbience.eHotTargetLastModel = PANTO	#ENDIF
				BREAK
				CASE 5 // INSURGENT
					SWITCH iVariation
						CASE 0
							vStartLocation = <<474.3275, -3093.1389, 5.0701>> 	fStartHeading = 332.7368 									//MERRYWEATHER DOCKS
							vDelLocation = <<2409.4363, 3034.4258, 47.1526>> 	fDelHeading = 359.6219   	vDelDimensions = <<7, 7, 3>>	//BONEYARD
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 1
							vStartLocation = <<-1563.8376, 2781.1487, 16.5122>> 	fStartHeading = 149.7295									//
							vDelLocation = <<2506.1201, -449.3372, 91.9929>> 		fDelHeading = 47.1668		vDelDimensions = <<7, 7, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 2
							vStartLocation = <<431.2834, 6466.7563, 27.7665>> 	fStartHeading = 49.7541										//
							vDelLocation = <<1135.3479, 86.6063, 79.7561>>		fDelHeading = 207.9940		vDelDimensions = <<7, 7, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 3
							vStartLocation = <<2969.7417, 2782.8555, 38.0754>> 	fStartHeading = 52.2547										//
							vDelLocation = <<-1069.9233, -854.9824, 3.8672>>	fDelHeading = 56.7178		vDelDimensions = <<7, 7, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 4
							vStartLocation = <<872.2369, -1350.2898, 25.3143>> 	fStartHeading = 88.8760										//
							vDelLocation = <<-1492.0404, 4967.1377, 62.9025>>	fDelHeading = 67.3250		vDelDimensions = <<7, 7, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
					ENDSWITCH
					
					#IF IS_DEBUG_BUILD MPGlobalsAmbience.eHotTargetLastModel = INSURGENT	#ENDIF
				BREAK
				CASE 6 // TECHNICAL
					SWITCH iVariation
						CASE 0
							vStartLocation = <<-546.5273, 5280.1738, 73.1005>> 	fStartHeading = 162.9733									//
							vDelLocation = <<506.5762, -2259.5911, 4.9673>>		fDelHeading = 242.8024		vDelDimensions = <<7, 7, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 1
							vStartLocation = <<1976.8610, 3827.0579, 31.3693>> 	fStartHeading = 298.9980									//
							vDelLocation = <<-82.6414, -2230.8848, 6.8117>>		fDelHeading = 272.3671		vDelDimensions = <<7, 7, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 2
							vStartLocation = <<1906.7747, 4927.8643, 47.9255>>	fStartHeading = 338.1263									//
							vDelLocation = <<2782.4673, -715.6125, 4.1759>>		fDelHeading = 80.5577		vDelDimensions = <<7, 7, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 3
							vStartLocation = <<38.8742, -2658.2788, 5.0057>> 	fStartHeading = 30.0011									//
							vDelLocation = <<966.1800, 2413.8596, 50.6192>>		fDelHeading = 194.4604		vDelDimensions = <<7, 7, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 4
							vStartLocation = <<2195.1746, 5602.5464, 52.5938>> 	fStartHeading = 344.0306  									//GRAPESEED FARM
							vDelLocation = <<-657.7287, -1723.7771, 23.7824>> 	fDelHeading = 350.3859		vDelDimensions = <<7, 7, 3>>	//CONTAINER UNDERPASS STORAGE
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
					ENDSWITCH
					
					#IF IS_DEBUG_BUILD MPGlobalsAmbience.eHotTargetLastModel = TECHNICAL	#ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE VT_BIKE
						
			iVariation = GET_RANDOM_INT_IN_RANGE(0, 5)
			#IF IS_DEBUG_BUILD
				IF MPGlobalsAmbience.iHotTargetLastRoute > 4
					MPGlobalsAmbience.iHotTargetLastRoute = -1
				ENDIF
			
				IF MPGlobalsAmbience.iHotTargetRoute > -1
				AND MPGlobalsAmbience.iHotTargetRoute < 5
					iVariation = MPGlobalsAmbience.iHotTargetRoute
					PRINTLN("[AM MOVING TARGET] - BIKE - DEBUG - MPGlobalsAmbience.iHotTargetRoute = ", MPGlobalsAmbience.iHotTargetRoute)
				ELIF MPGlobalsAmbience.iHotTargetRoute = -2
					IF MPGlobalsAmbience.iHotTargetLastRoute = -1
					OR MPGlobalsAmbience.eHotTargetLastModel != serverBD.VehModel
						iVariation = 0
						MPGlobalsAmbience.iHotTargetLastRoute = 0
						MPGlobalsAmbience.eHotTargetLastModel = serverBD.VehModel
					ELSE
						iVariation = MPGlobalsAmbience.iHotTargetLastRoute
					ENDIF
					PRINTLN("[AM MOVING TARGET] - BIKE - DEBUG - M MENU - Using route ", iVariation)
				ENDIF
			#ENDIF
			
			
			SWITCH iRoute
				CASE 0 // ENDURO
					SWITCH iVariation
						CASE 0
							vStartLocation = <<-1006.2579, 4837.8525, 268.7452>> 	fStartHeading = 191.5686									//ALTRURIST RADIO
							vDelLocation = <<2490.7910, 3729.7029, 42.1578>>		fDelHeading = 46.1533		vDelDimensions = <<7, 7, 3>>	//ALIEN HIPPY CAMP
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 1
							vStartLocation = <<510.8304, 5566.1851, 781.6024>> 	fStartHeading = 355.4307									//
							vDelLocation = <<1148.6143, 2345.5005, 53.0474>>	fDelHeading = 14.6300		vDelDimensions = <<7, 7, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 2
							vStartLocation = <<-375.0399, 1254.5638, 327.1631>> 	fStartHeading = 295.0018									//
							vDelLocation = <<-885.4050, 6041.1152, 40.8978>> 		fDelHeading = 195.2351		vDelDimensions = <<7, 7, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 3
							vStartLocation = <<-795.6297, 5413.1143, 33.2287>> 	fStartHeading = 54.4618									//
							vDelLocation = <<1541.8236, 1708.2806, 108.9070>>	fDelHeading = 68.0742		vDelDimensions = <<7, 7, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 4
							vStartLocation = <<735.2815, 1196.6517, 325.4270>>	 	fStartHeading = 302.6810									//
							vDelLocation = <<644.2736, -1850.7010, 8.1245>> 		fDelHeading = 26.4924		vDelDimensions = <<7, 7, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
					ENDSWITCH
					
					#IF IS_DEBUG_BUILD MPGlobalsAmbience.eHotTargetLastModel = ENDURO	#ENDIF
				BREAK
				CASE 1 // LECTRO
					SWITCH iVariation
						CASE 0
							vStartLocation = <<-1110.1967, -1689.0549, 3.3754>> 	fStartHeading = 219.2166  									//VESPUCCI BIKE RENTAL
							vDelLocation = <<1643.8560, 19.7556, 172.7744>>			fDelHeading = 124.0341  	vDelDimensions = <<7, 7, 3>>	//LAND ACT DAM
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 1
							vStartLocation = <<136.0471, -1049.2809, 28.1518>>	 	fStartHeading = 158.1811									//
							vDelLocation =  <<-2711.7, 1500.2, 106.2>>				fDelHeading = 230.9591		vDelDimensions = <<7, 7, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 2
							vStartLocation = <<273.4656, -180.5726, 60.5708>> 		fStartHeading = 162.0009									//
							vDelLocation = <<2536.3877, 2579.4316, 36.9449>> 		fDelHeading = 290.2541		vDelDimensions = <<7, 7, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 3
							vStartLocation = <<-2189.7417, -411.1201, 12.1754>> 	fStartHeading = 313.2105									//
							vDelLocation = <<810.9952, 2168.9697, 51.2859>> 		fDelHeading = 330.9763		vDelDimensions = <<7, 7, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 4
							vStartLocation = <<853.6739, -2178.9563, 29.6051>>		fStartHeading = 172.6983									//
							vDelLocation = <<564.8764, 2736.3650, 41.0602>> 		fDelHeading = 181.7749		vDelDimensions = <<7, 7, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
					ENDSWITCH
					
					#IF IS_DEBUG_BUILD MPGlobalsAmbience.eHotTargetLastModel = LECTRO	#ENDIF
				BREAK
				
				CASE 2 // VINDICATOR
					SWITCH iVariation
						CASE 0
							vStartLocation = <<1832.6866, 2542.2368, 44.8856>> 	fStartHeading = 269.8558  									//
							vDelLocation = <<-444.9892, 6055.6841, 30.3405>> 	fDelHeading = 203.0806		vDelDimensions = <<7, 7, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 1
							vStartLocation = <<-780.9092, 5583.3862, 32.4857>> 	fStartHeading = 223.1516									//
							vDelLocation = <<-1612.3148, 184.4401, 58.7645>>	fDelHeading = 205.0401		vDelDimensions = <<7, 7, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 2
							vStartLocation = <<-1068.0642, -503.9960, 35.0985>> 	fStartHeading = 326.4963									//
							vDelLocation = <<1398.1602, 1117.3784, 113.8377>> 		fDelHeading = 86.2978		vDelDimensions = <<7, 7, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 3
							vStartLocation = <<1359.8176, -608.6523, 73.3379>> 		fStartHeading =0.8104 										//
							vDelLocation = <<-3171.4092, 1098.9897, 19.7834>>		fDelHeading = 230.2205		vDelDimensions = <<7, 7, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
						CASE 4
							vStartLocation = <<2586.7263, 363.8747, 107.4573>> 		fStartHeading = 353.6129									//
							vDelLocation = <<-1494.3888, 1531.9575, 113.3838>> 		fDelHeading = 205.8995		vDelDimensions = <<7, 7, 3>>	//
							#IF IS_DEBUG_BUILD MPGlobalsAmbience.iHotTargetLastRoute++ #ENDIF
						BREAK
					ENDSWITCH
					
					#IF IS_DEBUG_BUILD MPGlobalsAmbience.eHotTargetLastModel = VINDICATOR	#ENDIF
				BREAK					
			ENDSWITCH
		
		BREAK
		
		CASE VT_HELI
		
			SWITCH serverBD.VehModel
				CASE FROGGER 	iVariation = 0 BREAK
				CASE MAVERICK 	iVariation = 1 BREAK
				CASE SWIFT 		iVariation = 2 BREAK
				CASE BUZZARD 	iVariation = 3 BREAK
				CASE VALKYRIE 	iVariation = 4 BREAK			
			ENDSWITCH
			
			#IF IS_DEBUG_BUILD
				IF MPGlobalsAmbience.iHotTargetVeh > -1
				AND MPGlobalsAmbience.iHotTargetVeh < 5
					iVariation = MPGlobalsAmbience.iHotTargetVeh
					PRINTLN("[AM MOVING TARGET] - HELI - DEBUG - iRoute = ", MPGlobalsAmbience.iHotTargetVeh)
				ENDIF
			#ENDIF
			
			SWITCH iRoute			
				CASE 0 // FROGGER
					SWITCH iVariation
						CASE 0
							vStartLocation 	= <<1223.9175, -2442.3801, 43.4816>> 	fStartHeading = 322.8489									//
							vDelLocation 	= <<168.3500, 2754.5403, 42.3956>>  	fDelHeading = 3.12			vDelDimensions = <<7, 7, 3>>	//					
						BREAK						
					ENDSWITCH
				BREAK
				
				CASE 1 //  MAVERICK
					SWITCH iVariation						
						CASE 1
							vStartLocation 	= <<-1900.7531, -3005.1567, 20.4377>> 	fStartHeading = 339.8940									//
							vDelLocation 	= <<1575.3425, 2221.1704, 77.4555>>  	fDelHeading = 3.12			vDelDimensions = <<7, 7, 3>>	//
						BREAK						
					ENDSWITCH
				BREAK
				
				CASE 2 //  SWIFT
					SWITCH iVariation						
						CASE 2
							vStartLocation 	= <<-1606.7867, 106.3727, 60.1145>>		fStartHeading = 231.6390									//
							vDelLocation 	= <<2940.5527, 4647.9336, 47.5449>> 	fDelHeading = 139.9407		vDelDimensions = <<7, 7, 3>>	//
						BREAK																												  
						
					ENDSWITCH
				BREAK
				
				CASE 3 //  BUZZARD
					SWITCH iVariation																																			  
						CASE 3																													  
							vStartLocation 	= <<1855.3910, 3713.8350, 32.2290>>	 	fStartHeading = 276.1101										//PALETO PIER											  
							vDelLocation 	= <<-1331.7573, -1507.7650, 3.3143>>	fDelHeading = 261.1253		vDelDimensions = <<7, 7, 3>>	//ALAMO SEA VILLAGE
						BREAK						
					ENDSWITCH
				BREAK
				
				CASE 4 // VALKYRIE
					SWITCH iVariation
						CASE 4
							vStartLocation 	= <<59.1623, 7191.3462, 2.0320>> 		fStartHeading = 188.1712									//
							vDelLocation 	= <<609.1902, -3192.6875, 5.0695>> 		fDelHeading = 201.3000		vDelDimensions = <<7, 7, 3>>	//
						BREAK
					ENDSWITCH
				BREAK			
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	vDelDimensions = <<5,5,3>>
		
	PRINTLN("[AM MOVING TARGET] - GET_RANDOM_LOCATIONS - vStartLocation = ", vStartLocation, " fStartHeading = ", fStartHeading)
	PRINTLN("[AM MOVING TARGET] - GET_RANDOM_LOCATIONS - vDelLocation = ", vDelLocation, " fDelHeading = ", fDelHeading, " vDelDimensions = ", vDelDimensions)
ENDPROC

FUNC INT GET_ROUTE_VALUE_FROM_MODEL()
	SWITCH serverBD.VehModel
		CASE ZENTORNO	RETURN 0
		CASE DUKES2		RETURN 1
		CASE KURUMA2	RETURN 2
		CASE GUARDIAN	RETURN 3
		CASE PANTO		RETURN 4
		CASE INSURGENT	RETURN 5
		CASE TECHNICAL	RETURN 6
		CASE ENDURO		RETURN 0
		CASE LECTRO		RETURN 1
		CASE VINDICATOR	RETURN 2
		CASE FROGGER	RETURN 0
		CASE MAVERICK	RETURN 1
		CASE SWIFT		RETURN 2
		CASE BUZZARD	RETURN 3
		CASE VALKYRIE	RETURN 4
	ENDSWITCH
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Checks if all cars have been disabled via tunable
/// RETURNS:
///    
FUNC BOOL CARS_DISABLED()
	IF g_sMPTunables.bHot_Target_Disable_Car_Zentorono
	AND g_sMPTunables.bHot_Target_Disable_Car_Duke_O_Death
	AND g_sMPTunables.bHot_Target_Disable_Car_Kuruma
	AND g_sMPTunables.bHot_Target_Disable_Car_Guardian
	AND g_sMPTunables.bHot_Target_Disable_Car_Panto
	AND g_sMPTunables.bHot_Target_Disable_Car_Insurgent
	AND g_sMPTunables.bHot_Target_Disable_Car_Technical
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if all bikes have been disabled via tunable
/// RETURNS:
///    
FUNC BOOL BIKES_DISABLED()
	IF g_sMPTunables.bHot_Target_Disable_Bike_Enduro
	AND g_sMPTunables.bHot_Target_Disable_Bike_Lectro
	AND g_sMPTunables.bHot_Target_Disable_Bike_Vindicator
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if all helis have been disabled via tunable
/// RETURNS:
///    
FUNC BOOL HELIS_DISABLED()
	IF g_sMPTunables.bHot_Target_Disable_Heli_Frogger
	AND g_sMPTunables.bHot_Target_Disable_Heli_Maverick
	AND g_sMPTunables.bHot_Target_Disable_Heli_Swift
	AND g_sMPTunables.bHot_Target_Disable_Heli_Buzzard
	AND g_sMPTunables.bHot_Target_Disable_Heli_Valkyrie
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Queries tunables to get a valid vehicle type based on whether the vehicles have been disabled
/// RETURNS:
///    A vehicle type that hasnt been fully disabled by tunable.
FUNC INT GET_VALID_VEHICLE_TYPE()	
	IF CARS_DISABLED() 
		IF BIKES_DISABLED() AND NOT HELIS_DISABLED()
			PRINTLN("[AM MOVING TARGET] - GET_VALID_VEHICLE_TYPE - Cars are disabled, bikes are disabled. Returning heli")
			RETURN VT_HELI
		ELIF HELIS_DISABLED() AND NOT BIKES_DISABLED()
			PRINTLN("[AM MOVING TARGET] - GET_VALID_VEHICLE_TYPE - Cars are disabled, helis are disabled. Returning bike")
			RETURN VT_BIKE
		ELSE
			PRINTLN("[AM MOVING TARGET] - GET_VALID_VEHICLE_TYPE - Cars are disabled, returning bike or heli randomly")
			RETURN GET_RANDOM_INT_IN_RANGE(1, VT_MAX)
		ENDIF		
	ELIF BIKES_DISABLED()
		IF CARS_DISABLED() AND NOT HELIS_DISABLED()
			PRINTLN("[AM MOVING TARGET] - GET_VALID_VEHICLE_TYPE - Bikes are disabled, cars are disabled. Returning heli")
			RETURN VT_HELI
		ELIF HELIS_DISABLED() AND NOT CARS_DISABLED()
			PRINTLN("[AM MOVING TARGET] - GET_VALID_VEHICLE_TYPE - Bikes are disabled, helis are disabled. Returning car")
			RETURN VT_CAR
		ELSE
			INT iTemp = GET_RANDOM_INT_IN_RANGE(1, VT_MAX)
			IF iTemp = 1
				iTemp = 0
			ENDIF
			PRINTLN("[AM MOVING TARGET] - GET_VALID_VEHICLE_TYPE - Bikes are disabled, returning car or heli randomly")
			RETURN iTemp
		ENDIF		
	ELIF HELIS_DISABLED()
		IF CARS_DISABLED() AND NOT BIKES_DISABLED()
			PRINTLN("[AM MOVING TARGET] - GET_VALID_VEHICLE_TYPE - Helis are disabled, cars are disabled. Returning bike")
			RETURN VT_BIKE
		ELIF BIKES_DISABLED() AND NOT CARS_DISABLED()
			PRINTLN("[AM MOVING TARGET] - GET_VALID_VEHICLE_TYPE - Helis are disabled, bikes are disabled. Returning car")
			RETURN VT_CAR
		ELSE
			PRINTLN("[AM MOVING TARGET] - GET_VALID_VEHICLE_TYPE - Helis disabled, returning car or bike randomly")
			RETURN GET_RANDOM_INT_IN_RANGE(0, 2)
		ENDIF				
	ELSE
		PRINTLN("[AM MOVING TARGET] - CARS_DISABLED - No vehicle types fully disabled, returning random vehicle type.")
		RETURN GET_RANDOM_INT_IN_RANGE(0, VT_MAX)
	ENDIF
ENDFUNC

//PURPOSE: Sets the data for this instance of Destroy Veh
PROC GET_RANDOM_SETUP_DETAILS()
//	serverBD.iVehicleType = GET_RANDOM_INT_IN_RANGE(0, VT_MAX)
	serverBD.iVehicleType = GET_VALID_VEHICLE_TYPE()
	#IF IS_DEBUG_BUILD
		IF MPGlobalsAmbience.iHotTargetType > -1
		AND MPGlobalsAmbience.iHotTargetType < VT_MAX
			serverBD.iVehicleType = MPGlobalsAmbience.iHotTargetType
			PRINTLN("[AM MOVING TARGET] - TYPE - DEBUG - MPGlobalsAmbience.iHotTargetType = ", MPGlobalsAmbience.iHotTargetType)
		ENDIF
	#ENDIF
	PRINTLN("[AM MOVING TARGET] - GET_RANDOM_SETUP_DETAILS - iVehicleType = ", serverBD.iVehicleType)
	
	//serverBD.iVehicleType = VT_CAR  //TEST DEBUG
	
	//serverBD.VehModel = GET_RANDOM_VEH_MODEL()
	serverBD.VehModel = GET_VALID_VEHICLE_MODEL_FROM_TYPE()
	serverBD.iRoute = GET_ROUTE_VALUE_FROM_MODEL()
	IF serverBD.VehModel = INSURGENT
	OR serverBD.VehModel = TECHNICAL
	OR serverBD.VehModel = VALKYRIE
		SET_BIT(serverBD.iServerBitSet, biS_GunnerVehicle)
		PRINTLN("[AM MOVING TARGET] - GET_RANDOM_SETUP_DETAILS - biS_GunnerVehicle SET")
	ENDIF
	
	GET_RANDOM_LOCATIONS(serverBD.vStartLocation, serverBD.fStartHeading, serverBD.vDeliveryLocation, serverBD.fDeliveryHeading, serverBD.vDeliveryDimensions, serverBD.iRoute, serverBD.iVariation)
	
	//Double check we aren't doing the same route as last time
	INT i
	REPEAT 10 i
		IF NOT FM_EVENT_IS_VARIATION_IN_HISTORY_LIST(FMMC_TYPE_MOVING_TARGET, serverBD.iRoute, serverBD.iVariation, serverBD.iVehicleType)
			BROADCAST_SET_LAST_FM_EVENT_VARIATION(FM_EVENT_GET_VARIATION_FROM_EVENT_TYPE_AND_SUB_TYPE(FMMC_TYPE_MOVING_TARGET, serverBD.iRoute, serverBD.iVariation, serverBD.iVehicleType))
			i = 10
			PRINTLN("[AM MOVING TARGET] - ROUTE IS DIFFERENT TO THE LAST ONE")
		ELSE
			PRINTLN("[AM MOVING TARGET] - REGRAB ROUTE - CURRENT IS THE SAME AS THE LAST ONE")
			GET_RANDOM_LOCATIONS(serverBD.vStartLocation, serverBD.fStartHeading, serverBD.vDeliveryLocation, serverBD.fDeliveryHeading, serverBD.vDeliveryDimensions, serverBD.iRoute, serverBD.iVariation)
		ENDIF
	ENDREPEAT
	
ENDPROC

//PURPOSE: Returns TRUE if the Plane is stuck or not int the air
FUNC BOOL IS_VEHICLE_STUCK()
		
	IF IS_VEHICLE_STUCK_TIMER_UP(NET_TO_VEH(serverBD.niVeh), VEH_STUCK_ON_ROOF, (ROOF_TIME * 3))				// check if stuck on roof for 30 seconds, dont want to deny kills
		CPRINTLN(DEBUG_AMBIENT, "[AM MOVING TARGET] - IS_VEHICLE_STUCK - TRUE - VEH_STUCK_ON_ROOF ")
		RETURN TRUE
	ENDIF
	
	IF IS_VEHICLE_STUCK_TIMER_UP(NET_TO_VEH(serverBD.niVeh), VEH_STUCK_ON_SIDE, SIDE_TIME)
		CPRINTLN(DEBUG_AMBIENT, "[AM MOVING TARGET] - IS_VEHICLE_STUCK - TRUE - VEH_STUCK_ON_SIDE ")
		RETURN TRUE
	ENDIF
	
	IF IS_VEHICLE_STUCK_TIMER_UP(NET_TO_VEH(serverBD.niVeh), VEH_STUCK_JAMMED, JAMMED_TIME)
		CPRINTLN(DEBUG_AMBIENT, "[AM MOVING TARGET] - IS_VEHICLE_STUCK - TRUE - VEH_STUCK_JAMMED ")
		RETURN TRUE
	ENDIF
	
	IF IS_VEHICLE_STUCK_TIMER_UP(NET_TO_VEH(serverBD.niVeh), VEH_STUCK_HUNG_UP, HUNG_UP_TIME)
		CPRINTLN(DEBUG_AMBIENT, "[AM MOVING TARGET] - IS_VEHICLE_STUCK - TRUE -  VEH_STUCK_HUNG_UP")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns if the player is one of the hot targets    
FUNC BOOL AM_I_A_HOT_TARGET()	
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Driver)
	OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Gunner)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE	
ENDFUNC

FUNC BOOL AM_I_THE_DRIVER()
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Driver)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL AM_I_THE_GUNNER()
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Gunner)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_A_HOT_TARGET(PLAYER_INDEX player)	
	IF IS_BIT_SET(playerBD[NATIVE_TO_INT(player)].iPlayerBitSet, biP_Driver)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(playerBD[NATIVE_TO_INT(player)].iPlayerBitSet, biP_Gunner)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_THE_DRIVER(PLAYER_INDEX player)	
	IF IS_BIT_SET(playerBD[NATIVE_TO_INT(player)].iPlayerBitSet, biP_Driver)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_THE_GUNNER(PLAYER_INDEX player)
	IF IS_BIT_SET(playerBD[NATIVE_TO_INT(player)].iPlayerBitSet, biP_Gunner)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL AM_I_SPECTATING_A_HOT_TARGET()
	PED_INDEX driverPed
	PED_INDEX gunnerPed

	IF IS_PLAYER_SCTV(PLAYER_ID())
		IF serverBD.piDriver != INVALID_PLAYER_INDEX()
			IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piDriver)
				driverPed =	GET_PLAYER_PED(serverBD.piDriver)
			ENDIF
		ENDIF
		IF serverBD.piGunner != INVALID_PLAYER_INDEX()
			IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piGunner)
				gunnerPed =	GET_PLAYER_PED(serverBD.piGunner)
			ENDIF
		ENDIF
		IF GET_SPECTATOR_CURRENT_FOCUS_PED() = driverPed
		OR GET_SPECTATOR_CURRENT_FOCUS_PED() = gunnerPed
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_SPECTATOR_FOCUS_PED_IN_HOT_TARGET_VEHICLE()

	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
		IF IS_PLAYER_SCTV(PLAYER_ID())
			IF GET_SPECTATOR_CURRENT_FOCUS_PED() != NULL
				IF IS_PED_IN_THIS_VEHICLE(GET_SPECTATOR_CURRENT_FOCUS_PED(), NET_TO_VEH(serverBD.niVeh))
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL SHOULD_UI_BE_HIDDEN()

	// Dont Hide UI if local is spectating
	IF AM_I_SPECTATING_A_HOT_TARGET()
		RETURN FALSE
	ENDIF
	
	IF NOT IS_IT_SAFE_TO_DISPLAY_FM_EVENT_UI()
		RETURN TRUE
	ENDIF
	
	IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	IF g_bCelebrationScreenIsActive = TRUE
		RETURN TRUE
	ENDIF
	
	IF IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID()) = TRUE
		RETURN TRUE
	ENDIF
	
	IF GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
	OR GB_IS_PLAYER_PERMANENT_PARTICIPANT(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAVE_HOT_TARGETS_BEEN_KILLED()
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_HotTargetsKilled)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_MINIMUM_PARTICIPATION_MULTIPLIER()
	FLOAT fAmount = TO_FLOAT((iParticipationTime / 60000))
	
	IF fAmount > TO_FLOAT(g_sMPTunables.iParticipation_T_Cap)
		fAmount = TO_FLOAT(g_sMPTunables.iParticipation_T_Cap)
	ENDIF
	
	IF fAmount < 2
		PRINTLN("[AM MOVING TARGET] - GET_MINIMUM_PARTICIPATION_MULTIPLIER - Participant has been in event less than 2 minutes, returning 1")
		RETURN 1
	ENDIF
	
	PRINTLN("[AM MOVING TARGET] - GET_MINIMUM_PARTICIPATION_MULTIPLIER - Participant has been in session for ", FLOOR(fAmount)," minutes. Returning this as multiplier")
	RETURN FLOOR(fAmount)
ENDFUNC

FUNC INT HELI_DELIVER_CASH_REWARD()
	RETURN ROUND((g_sMPTunables.iHELICOPTER_HOT_TARGET_HOT_TARGET_DEFAULT_CASH * g_sMPTunables.fHeli_Moving_Target_Event_Multiplier_Cash))
ENDFUNC

FUNC INT NORMAL_DELIVER_CASH_REWARD()
	RETURN ROUND((g_sMPTunables.iHOT_TARGET_HOT_TARGET_DEFAULT_CASH * g_sMPTunables.fMoving_Target_Event_Multiplier_Cash))
ENDFUNC

FUNC INT HELI_CHASE_CASH_REWARD()
	RETURN ROUND((g_sMPTunables.iHELICOPTER_HOT_TARGET_CHASER_DEFAULT_CASH * g_sMPTunables.fHeli_Moving_Target_Event_Multiplier_Cash))
ENDFUNC

FUNC INT NORMAL_CHASE_CASH_REWARD()
	RETURN ROUND((g_sMPTunables.iHOT_TARGET_CHASER_DEFAULT_CASH * g_sMPTunables.fMoving_Target_Event_Multiplier_Cash))
ENDFUNC

//PURPOSE: Returns the Cash reward for completing the objective
FUNC INT GET_CASH_REWARD(HOT_TARGET_CASH_RP_TYPE eHTType, BOOL bIsForHelpTextOrBigMessage = FALSE)
	
	INT toReturn = 0
	
	IF serverBD.iVehicleType = VT_HELI
	
		SWITCH eHTType
		
			CASE eHT_CASH_RP_DELIVERER
				IF bIsForHelpTextOrBigMessage = TRUE
					toReturn = HELI_DELIVER_CASH_REWARD()
				ELSE
					toReturn = HELI_DELIVER_CASH_REWARD()
				ENDIF
			BREAK
			
			CASE eHT_CASH_RP_CHASER
				toReturn = HELI_CHASE_CASH_REWARD()
			BREAK
			
			CASE eHT_CASH_RP_PARTICIPANT
				toReturn = (g_sMPTunables.iHELICOPTER_HOT_TARGET_MINIMUM_PARTICIPATION_CASH * GET_MINIMUM_PARTICIPATION_MULTIPLIER())
			BREAK
		
		ENDSWITCH
	
	ELSE
	
		SWITCH eHTType
		
			CASE eHT_CASH_RP_DELIVERER
				IF bIsForHelpTextOrBigMessage = TRUE
					toReturn = NORMAL_DELIVER_CASH_REWARD()
				ELSE
					toReturn = NORMAL_DELIVER_CASH_REWARD()
				ENDIF
			BREAK
			
			CASE eHT_CASH_RP_CHASER
				toReturn = NORMAL_CHASE_CASH_REWARD()
			BREAK
			
			CASE eHT_CASH_RP_PARTICIPANT
				toReturn = (g_sMPTunables.iHOT_TARGET_MINIMUM_PARTICIPATION_CASH * GET_MINIMUM_PARTICIPATION_MULTIPLIER())
			BREAK
		
		ENDSWITCH
	
	ENDIF	
	
	IF toReturn = 0
		PRINTLN("[AM MOVING TARGET] - GET_CASH_REWARD - amout of cash is returning 0 - contact Steve Tiley")
	ENDIF	
	RETURN toReturn
	
ENDFUNC

FUNC INT HELI_DELIVER_RP_REWARD()
	RETURN ROUND((g_sMPTunables.iHELICOPTER_HOT_TARGET_HOT_TARGET_DEFAULT_RP * g_sMPTunables.fHeli_Moving_Target_Event_Multiplier_RP))
ENDFUNC

FUNC INT NORMAL_DELIVER_RP_REWARD()
	RETURN ROUND((g_sMPTunables.iHOT_TARGET_HOT_TARGET_DEFAULT_RP * g_sMPTunables.fMoving_Target_Event_Multiplier_RP))
ENDFUNC

FUNC INT HELI_CHASE_RP_REWARD()
	RETURN ROUND((g_sMPTunables.iHELICOPTER_HOT_TARGET_CHASER_DEFAULT_RP * g_sMPTunables.fHeli_Moving_Target_Event_Multiplier_RP))
ENDFUNC

FUNC INT NORMAL_CHASE_RP_REWARD()
	RETURN ROUND((g_sMPTunables.iHOT_TARGET_CHASER_DEFAULT_RP * g_sMPTunables.fMoving_Target_Event_Multiplier_RP))
ENDFUNC

//PURPOSE: Returns the RP reward for completing the objective
FUNC INT GET_RP_REWARD(HOT_TARGET_CASH_RP_TYPE eHTType, BOOL bIsForHelpTextOrBigMessage = FALSE)

	INT toReturn = 0

	IF serverBD.iVehicleType = VT_HELI
	
		SWITCH eHTType
		
			CASE eHT_CASH_RP_DELIVERER
				IF bIsForHelpTextOrBigMessage = TRUE
					toReturn = HELI_DELIVER_RP_REWARD()
				ELSE
					toReturn = (HELI_DELIVER_RP_REWARD() + (g_sMPTunables.iHELICOPTER_HOT_TARGET_MINIMUM_PARTICIPATION_RP * GET_MINIMUM_PARTICIPATION_MULTIPLIER()))
				ENDIF
			BREAK
			
			CASE eHT_CASH_RP_CHASER
				toReturn = (HELI_CHASE_RP_REWARD() + (g_sMPTunables.iHELICOPTER_HOT_TARGET_MINIMUM_PARTICIPATION_RP * GET_MINIMUM_PARTICIPATION_MULTIPLIER()))
			BREAK
			
			CASE eHT_CASH_RP_PARTICIPANT
				toReturn = (g_sMPTunables.iHELICOPTER_HOT_TARGET_MINIMUM_PARTICIPATION_RP * GET_MINIMUM_PARTICIPATION_MULTIPLIER())
			BREAK
		
		ENDSWITCH
	
	ELSE
	
		SWITCH eHTType
		
			CASE eHT_CASH_RP_DELIVERER
				IF bIsForHelpTextOrBigMessage = TRUE
					toReturn = NORMAL_DELIVER_RP_REWARD()
				ELSE
					toReturn = (NORMAL_DELIVER_RP_REWARD() + (g_sMPTunables.iHOT_TARGET_MINIMUM_PARTICIPATION_RP * GET_MINIMUM_PARTICIPATION_MULTIPLIER()))
				ENDIF
			BREAK
			
			CASE eHT_CASH_RP_CHASER
				toReturn = (NORMAL_CHASE_RP_REWARD() + (g_sMPTunables.iHOT_TARGET_MINIMUM_PARTICIPATION_RP * GET_MINIMUM_PARTICIPATION_MULTIPLIER()))
			BREAK
			
			CASE eHT_CASH_RP_PARTICIPANT
				toReturn = (g_sMPTunables.iHOT_TARGET_MINIMUM_PARTICIPATION_RP * GET_MINIMUM_PARTICIPATION_MULTIPLIER())
			BREAK
		
		ENDSWITCH
	
	ENDIF	
	
	IF toReturn = 0
		PRINTLN("[AM MOVING TARGET] - GET_RP_REWARD - amout of cash is returning 0 - contact Steve Tiley")
	ENDIF
	RETURN toReturn

ENDFUNC

//Do necessary pre game start ini.
PROC PROCESS_PRE_GAME( MP_MISSION_DATA missionScriptArgs )
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission) ,  missionScriptArgs)	//NUM_NETWORK_PLAYERS, missionScriptArgs)	//
	//NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, PARTICIPANT_ID_TO_INT())  
	
	//Reserve Entities
	RESERVE_NETWORK_MISSION_VEHICLES(1)

	// This makes sure the net script is active, waits untill it is.
	HANDLE_NET_SCRIPT_INITIALISATION()

	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		#IF IS_DEBUG_BUILD NET_LOG("HOT TARGET -  FAILED TO RECEIVE INITIAL NETWORK BROADCAST")	#ENDIF
		NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  MISSION END - FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP D     <----------     ") NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		// This script will not be paused if another script calls PAUSE_GAME
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			GET_RANDOM_SETUP_DETAILS()
			serverBD.piDriver = INVALID_PLAYER_INDEX()
			serverBD.piGunner = INVALID_PLAYER_INDEX()
			serverBD.piDeliverer = INVALID_PLAYER_INDEX()
			serverBD.piDriverKiller = INVALID_PLAYER_INDEX()
			serverBD.piGunnerKiller = INVALID_PLAYER_INDEX()
			PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBD.iHashedMac, serverBD.iMatchHistoryId)
		ENDIF
		
		IF serverBD.VehModel = MAVERICK
			UNLOCK_AIRPORT_GATES()
			PRINTLN("[AM MOVING TARGET] - Vehicle is maverick, unlocking airport gates. ")
		ENDIF
		
		telemetryStruct.m_playersNotified = NETWORK_GET_TOTAL_NUM_PLAYERS()
		telemetryStruct.m_notifiedTime = GET_CLOUD_TIME_AS_INT()
		telemetryStruct.m_startTime	= GET_CLOUD_TIME_AS_INT()
		
		COMMON_FREEMODE_AMBIENT_EVENTS_PROCESS_PRE_GAME(FMMC_TYPE_MOVING_TARGET)
		
		SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_MOVING_TARGET, TRUE)
				
		IF serverBD.iServerGameState = GAME_STATE_RUNNING
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_PlayerIsJIP)
			PRINTLN("[AM MOVING TARGET] - Player is JIP ")
		ENDIF
		
		playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_INI
		NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  PRE_GAME DONE     <----------     ") NET_NL()
	ELSE
		#IF IS_DEBUG_BUILD NET_LOG("HOT TARGET -  NETWORK_IS_GAME_IN_PROGRESS = FALSE")	#ENDIF
		NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  MISSION END - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP Q     <----------     ") NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
ENDPROC


FUNC STRING GET_VEHICLE_TYPE_STRING()
//	IF IS_THIS_MODEL_A_BIKE(serverBD.VehModel)
//		RETURN "VN_BIKE"
////	ELIF serverBD.VehModel = DINGHY
////	OR IS_THIS_MODEL_A_JETSKI(serverBD.VehModel)
////	OR IS_THIS_MODEL_A_BOAT(serverBD.VehModel)
////		RETURN "VN_BOAT"
//	ELIF IS_THIS_MODEL_A_TURRET_VEHICLE(serverBD.VehModel)
//	OR serverBD.VehModel = GUARDIAN
//		RETURN "VN_TRUCK"
//	ELIF IS_THIS_MODEL_A_HELI(serverBD.VehModel)
//		RETURN "VN_HELI"
//	ENDIF
	
	RETURN "VN_VEH"
ENDFUNC

/// PURPOSE:
///    Returns the minimum range for participation given the type of hot target vehicle
/// RETURNS:
///    
FUNC INT GET_HOT_TARGET_VEH_TYPE_PARTICIPATE_RANGE()
	IF serverBD.iVehicleType = VT_HELI
		RETURN g_sMPTunables.iHELICOPTER_HOT_TARGET_PARTICIPATION_THRESHOLD_DISTANCE
	ENDIF
	
	RETURN g_sMPTunables.iHOT_TARGET_PARTICIPATION_THRESHOLD_DISTANCE
ENDFUNC

/// PURPOSE: Check if a player has entered the vehicle to start the event
///    
/// RETURNS: TRUE if a player has entered, FALSE otherwise
///    
FUNC BOOL HAVE_PLAYERS_ENTERED_TO_START_EVENT()

	IF IS_BIT_SET(serverBD.iServerBitSet, biS_EventStarted)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Checks if the local player killed the driver   
FUNC BOOL DID_I_KILL_THE_DRIVER()
	IF serverBD.piDriver != INVALID_PLAYER_INDEX()
		IF serverBD.piDriverKiller != INVALID_PLAYER_INDEX()
			IF serverBD.piDriverKiller = PLAYER_ID()
				PRINTLN("[AM MOVING TARGET] - DID_I_KILL_THE_DRIVER - TRUE, I killed the driver.")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the local player killed the gunner 
FUNC BOOL DID_I_KILL_THE_GUNNER()
	IF serverBD.piGunner != INVALID_PLAYER_INDEX()
		IF serverBD.piGunnerKiller != INVALID_PLAYER_INDEX()
			IF serverBD.piGunnerKiller = PLAYER_ID()
				PRINTLN("[AM MOVING TARGET] - DID_I_KILL_THE_GUNNER - TRUE, I killed the gunner.")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the local player killed one or the other hot targets (for 2 man veh reward split)   
FUNC BOOL DID_I_KILL_ONLY_ONE_HOT_TARGET()
	IF DID_I_KILL_THE_DRIVER() 
	AND NOT DID_I_KILL_THE_GUNNER()
		PRINTLN("[AM MOVING TARGET] - DID_I_KILL_ONLY_ONE_HOT_TARGET - TRUE - I killed the driver but NOT the gunner")
		RETURN TRUE
	ENDIF
	
	IF DID_I_KILL_THE_GUNNER() 
	AND NOT DID_I_KILL_THE_DRIVER()
		PRINTLN("[AM MOVING TARGET] - DID_I_KILL_ONLY_ONE_HOT_TARGET - TRUE - I killed the gunner but NOT the driver")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the local player killed the driver and the gunner (for 2 man veh reward split)
FUNC BOOL DID_I_KILL_BOTH_HOT_TARGETS()
	IF DID_I_KILL_THE_DRIVER()
	AND DID_I_KILL_THE_GUNNER()
		PRINTLN("[AM MOVING TARGET] - DID_I_KILL_BOTH_HOT_TARGETS - TRUE - I killed the driver AND the gunner")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the local player destroyed the hot target vehicle after the event started  
FUNC BOOL DID_I_DESTROY_THE_VEHICLE()
	IF HAVE_PLAYERS_ENTERED_TO_START_EVENT()
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niVeh)
			IF IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh))
				WEAPON_TYPE tempWeapon
				IF PLAYER_ID() = NETWORK_GET_DESTROYER_OF_NETWORK_ID(serverBD.niVeh, tempWeapon)
				OR PLAYER_ID() = NETWORK_GET_DESTROYER_OF_ENTITY(NET_TO_VEH(serverBD.niVeh), tempWeapon)
					PRINTLN("[AM MOVING TARGET] - DID_I_DESTROY_THE_VEHICLE - TRUE - I destroyed the vehicle")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns whether the player is within the threshold to receive a reward at the end of event
FUNC BOOL HAVE_I_MET_REWARD_CONDITIONS()

	IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(PLAYER_ID())
		PRINTLN("[AM MOVING TARGET] - HAVE_I_MET_REWARD_CONDITIONS - *** NO *** - FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION")
		RETURN FALSE
	ENDIF
	
	IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID())
		PRINTLN("[AM MOVING TARGET] - HAVE_I_MET_REWARD_CONDITIONS - *** NO *** - FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE")
		RETURN FALSE
	ENDIF
	
	IF AM_I_A_HOT_TARGET()
		PRINTLN("[AM MOVING TARGET] - HAVE_I_MET_REWARD_CONDITIONS - *** YES *** - AM_I_A_HOT_TARGET")
		RETURN TRUE
	ENDIF
	
	IF DID_I_KILL_THE_DRIVER()
		PRINTLN("[AM MOVING TARGET] - HAVE_I_MET_REWARD_CONDITIONS - *** YES *** - DID_I_KILL_THE_DRIVER")
		RETURN TRUE
	ENDIF
	
	IF DID_I_KILL_THE_GUNNER()
		PRINTLN("[AM MOVING TARGET] - HAVE_I_MET_REWARD_CONDITIONS - *** YES *** - DID_I_KILL_THE_GUNNER")
		RETURN TRUE
	ENDIF	
	
	IF DID_I_DESTROY_THE_VEHICLE()
		PRINTLN("[AM MOVING TARGET] - HAVE_I_MET_REWARD_CONDITIONS - *** YES *** - DID_I_DESTROY_THE_VEHICLE")
		RETURN TRUE
	ENDIF	
	
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WithinQualifyDistance)
		PRINTLN("[AM MOVING TARGET] - HAVE_I_MET_REWARD_CONDITIONS - *** YES *** - IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WithinQualifyDistance)")
		RETURN TRUE
	ENDIF
	
	IF FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_FM_EVENT(PLAYER_ID())
		PRINTLN("[AM MOVING TARGET] - HAVE_I_MET_REWARD_CONDITIONS - *** YES *** - FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_FM_EVENT")
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the player is a permanent participant or within UI range
/// RETURNS:
///    
FUNC BOOL AM_I_PARTICIPATING_IN_EVENT(BOOL bIgnorePermanentCheck = FALSE)
	
	IF bIgnorePermanentCheck = FALSE
		IF FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_FM_EVENT(PLAYER_ID())
			RETURN TRUE
		ENDIF
	ENDIF
	
	// Player is within 100m of hot target vehicle
	IF IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerBitSet, biP_ParticipantViaDistance)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iFmAmbientEventBitSet, ciSET_PLAYER_OPTED_IN)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the player has been restricted by Hide/passive/SCTV
/// RETURNS:
///    True if so
FUNC BOOL AM_I_RESTRICTED()
	IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_SCTV(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Server only function that checks to see if the game mode should now end. For this test script, it if time has passed.
FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET()
	
	#IF IS_DEBUG_BUILD
		IF bHostEndMissionNow
			RETURN TRUE
		ENDIF
	#ENDIF	
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_MissionShouldCleanup)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


//Helper function to get a clients game/mission state
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iGameState
ENDFUNC

#IF IS_DEBUG_BUILD

PROC UPDATE_WIDGETS()
	//Warp player to Veh
	IF bWarpToVeh = TRUE
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh)
			IF NET_WARP_TO_COORD(GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niVeh))+<<0, 0, 3>>, 0, TRUE, FALSE)
				NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  UPDATE_WIDGETS - bWarpToVeh DONE    <----------     ") NET_NL()
				bWarpToVeh = FALSE
			ELSE
				NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  UPDATE_WIDGETS - bWarpToVeh IN PROGRESS    <----------     ") NET_NL()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_WIDGETS()
	INT iPlayer
	TEXT_LABEL_15 tl63
	
	SWITCH iWidgetStage
	
		CASE 0
		
		htWidgetGroup = START_WIDGET_GROUP("Moving Target")
			ADD_WIDGET_BOOL("Create Moving Target Widgets", bCreateWidgets)
		STOP_WIDGET_GROUP()
		
		iWidgetStage++
		
		BREAK
		
		CASE 1
		
			IF bCreateWidgets
				
				DELETE_WIDGET_GROUP(htWidgetGroup)
				
				htWidgetGroup = START_WIDGET_GROUP("Moving Target") 
				
					ADD_WIDGET_BOOL("Warp to Veh", bWarpToVeh)
							
					// Gameplay debug, sever only.
					START_WIDGET_GROUP("Server Only Gameplay") 
						ADD_WIDGET_BOOL("End mission", bHostEndMissionNow)			
					STOP_WIDGET_GROUP()		
					
					// Data about the server
					START_WIDGET_GROUP("Server BD") 
						ADD_WIDGET_INT_SLIDER("S. Game state", serverBD.iServerGameState,-1, HIGHEST_INT,1)
					STOP_WIDGET_GROUP()	

					// Data about the clients. * = You.
					START_WIDGET_GROUP("Client BD")  				
						REPEAT NUM_NETWORK_PLAYERS iPlayer
							tl63 = "Player "
							tl63 += iPlayer
							IF iPlayer = PARTICIPANT_ID_TO_INT()
								tl63 += "*"
							ENDIF
							START_WIDGET_GROUP(tl63)
								ADD_WIDGET_INT_SLIDER("Game state", playerBD[iPlayer].iGameState,-1, HIGHEST_INT,1)
							STOP_WIDGET_GROUP()
						ENDREPEAT				
				STOP_WIDGET_GROUP()
				
				PRINTLN("[AM MOVING TARGET] -  Created Widgets")
				
				iWidgetStage++
			
			ENDIF
		
		BREAK
		
		CASE 2
			UPDATE_WIDGETS()
		BREAK
	
	ENDSWITCH
ENDPROC		


#ENDIF

FUNC STRING GET_VEH_BLIP_STRING()
	IF playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_KILL
		RETURN "ABLIP_HTP"			//"~BLIP_TEMP_5~"
	ENDIF
	
	RETURN "ABLIP_HTV"			//"~BLIP_TEMP_4~"
ENDFUNC

//PURPOSE: Adds a blip to the Veh
PROC ADD_VEH_BLIP(BOOL bPurpleBlip = TRUE)
	IF SHOULD_UI_BE_HIDDEN()
		EXIT
	ENDIF
	IF DOES_BLIP_EXIST(DeliveryBlip)
		REMOVE_BLIP(DeliveryBlip)
		DELETE_CHECKPOINT(DeliveryCP)
		Clear_Any_Objective_Text()
	ENDIF
	IF NOT DOES_BLIP_EXIST(VehBlip)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
			IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh))
				VehBlip = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(serverBD.niVeh))

				SET_BLIP_SPRITE(VehBlip, RADAR_TRACE_TEMP_4)
				IF bPurpleBlip = TRUE
					SET_BLIP_COLOUR_FROM_HUD_COLOUR(VehBlip, HUD_COLOUR_NET_PLAYER2)
				ELSE
					SET_BLIP_COLOUR(VehBlip, BLIP_COLOUR_BLUE)
				ENDIF
				//SHOW_HEIGHT_ON_BLIP(VehBlip, TRUE)
				SET_BLIP_FLASH_TIMER(VehBlip, 7000)
				SET_BLIP_PRIORITY(VehBlip, BLIPPRIORITY_HIGH_HIGHEST)
				SET_BLIP_NAME_FROM_TEXT_FILE(VehBlip, "HTV_BLIP")	//Hot Target
				NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  VEH BLIP ADDED ") NET_NL()
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

PROC ADD_PLAYER_BLIP()
	IF SHOULD_UI_BE_HIDDEN()
		EXIT
	ENDIF
	IF DOES_BLIP_EXIST(VehBlip)
		REMOVE_BLIP(VehBlip)
	ELSE
		IF NOT DOES_BLIP_EXIST(PlayerBlip)		
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
				IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh))
					PlayerBlip = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(serverBD.niVeh))
					SET_BLIP_SPRITE(PlayerBlip, RADAR_TRACE_TEMP_5)
					SET_BLIP_FLASH_TIMER(PlayerBlip, 7000)
					SET_BLIP_PRIORITY(PlayerBlip, BLIP_PRIORITY_HIGHEST_SPECIAL_LOW)
					SET_BLIP_NAME_FROM_TEXT_FILE(PlayerBlip, "HTV_BLIP")	//Hot Target
					NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  PLAYER BLIP ADDED ") NET_NL()
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_PLAYER_BLIP_COLOUR()
	IF DOES_BLIP_EXIST(PlayerBlip)
		IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
		
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
				IF serverBD.piDriver != INVALID_PLAYER_INDEX()
				AND serverBD.piGunner != INVALID_PLAYER_INDEX()
					IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(serverBD.piDriver, GB_GET_LOCAL_PLAYER_GANG_BOSS())
					AND GB_IS_PLAYER_MEMBER_OF_THIS_GANG(serverBD.piGunner, GB_GET_LOCAL_PLAYER_GANG_BOSS())
						// Both players are in my gang
						IF GET_BLIP_COLOUR(PlayerBlip) != GET_BLIP_COLOUR_FROM_HUD_COLOUR(GET_PLAYER_HUD_COLOUR(serverBD.piDriver))
							SET_BLIP_COLOUR_FROM_HUD_COLOUR(PlayerBlip, GET_PLAYER_HUD_COLOUR(serverBD.piDriver))
						ENDIF
					ELIF GB_GET_LOCAL_PLAYER_GANG_BOSS() = serverBD.piDriver
					AND NOT GB_IS_PLAYER_MEMBER_OF_THIS_GANG(serverBD.piGunner, GB_GET_LOCAL_PLAYER_GANG_BOSS())
						// Driver is my boss
						IF GET_BLIP_COLOUR(PlayerBlip) != GET_BLIP_COLOUR_FROM_HUD_COLOUR(GET_PLAYER_HUD_COLOUR(serverBD.piDriver))
							SET_BLIP_COLOUR_FROM_HUD_COLOUR(PlayerBlip, GET_PLAYER_HUD_COLOUR(serverBD.piDriver))
						ENDIF
					ELIF GB_GET_LOCAL_PLAYER_GANG_BOSS() = serverBD.piGunner
					AND NOT GB_IS_PLAYER_MEMBER_OF_THIS_GANG(serverBD.piDriver, GB_GET_LOCAL_PLAYER_GANG_BOSS())
						// Gunner is my boss
						IF GET_BLIP_COLOUR(PlayerBlip) != GET_BLIP_COLOUR_FROM_HUD_COLOUR(GET_PLAYER_HUD_COLOUR(serverBD.piGunner))
							SET_BLIP_COLOUR_FROM_HUD_COLOUR(PlayerBlip, GET_PLAYER_HUD_COLOUR(serverBD.piGunner))
						ENDIF
					ELSE
						// Kill them
						IF GET_BLIP_COLOUR(PlayerBlip) != BLIP_COLOUR_RED
							SET_BLIP_COLOUR(PlayerBlip, BLIP_COLOUR_RED)
						ENDIF
					ENDIF					
				ENDIF
			ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_DriverLeft)
				IF serverBD.piGunner != INVALID_PLAYER_INDEX()
					IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(serverBD.piGunner, GB_GET_LOCAL_PLAYER_GANG_BOSS())
						// Gunner is in my gang
						IF GET_BLIP_COLOUR(PlayerBlip) != GET_BLIP_COLOUR_FROM_HUD_COLOUR(GET_PLAYER_HUD_COLOUR(serverBD.piGunner))
							SET_BLIP_COLOUR_FROM_HUD_COLOUR(PlayerBlip, GET_PLAYER_HUD_COLOUR(serverBD.piGunner))
						ENDIF
					ELSE
						// Kill them
						IF GET_BLIP_COLOUR(PlayerBlip) != BLIP_COLOUR_RED
							SET_BLIP_COLOUR(PlayerBlip, BLIP_COLOUR_RED)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF serverBD.piDriver != INVALID_PLAYER_INDEX()
					IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(serverBD.piDriver, GB_GET_LOCAL_PLAYER_GANG_BOSS())
						// Driver is in my gang
						IF GET_BLIP_COLOUR(PlayerBlip) != GET_BLIP_COLOUR_FROM_HUD_COLOUR(GET_PLAYER_HUD_COLOUR(serverBD.piDriver))
							SET_BLIP_COLOUR_FROM_HUD_COLOUR(PlayerBlip, GET_PLAYER_HUD_COLOUR(serverBD.piDriver))
						ENDIF
					ELSE
						// Kill them
						IF GET_BLIP_COLOUR(PlayerBlip) != BLIP_COLOUR_RED
							SET_BLIP_COLOUR(PlayerBlip, BLIP_COLOUR_RED)
						ENDIF
					ENDIF
				ENDIF
			
			ENDIF
		
		ELSE
			IF GET_BLIP_COLOUR(PlayerBlip) != BLIP_COLOUR_RED
				SET_BLIP_COLOUR(PlayerBlip, BLIP_COLOUR_RED)
			ENDIF
		ENDIF
		
	ENDIF
ENDPROC

//PURPOSE: Adds a blip for the delivery location
PROC ADD_DELIVERY_BLIP()
	IF SHOULD_UI_BE_HIDDEN()
		EXIT
	ENDIF
	IF DOES_BLIP_EXIST(VehBlip)
		REMOVE_BLIP(VehBlip)
	ENDIF
	IF NOT DOES_BLIP_EXIST(DeliveryBlip)
		DeliveryBlip = ADD_BLIP_FOR_COORD(serverBD.vDeliveryLocation)
		SET_BLIP_COLOUR(DeliveryBlip, BLIP_COLOUR_YELLOW)
		SHOW_HEIGHT_ON_BLIP(DeliveryBlip, TRUE)
		SET_BLIP_FLASH_TIMER(DeliveryBlip, 7000)
		SET_BLIP_PRIORITY(DeliveryBlip, BLIPPRIORITY_HIGH_HIGHEST)
		SET_BLIP_NAME_FROM_TEXT_FILE(DeliveryBlip, "HTV_BLIP2")	//Hot Target Delivery
		SET_BLIP_ROUTE(DeliveryBlip, TRUE)
		NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  DELIVERY BLIP ADDED ") NET_NL()
		
		//Checkpoint
		INT iR, iG, iB, iA
		GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
		DeliveryCP = CREATE_CHECKPOINT(CHECKPOINT_RACE_GROUND_FLAG, serverBD.vDeliveryLocation+<<0, 0, 3>>, <<0, 0, 0>>, serverBD.vDeliveryDimensions.x, iR, iG, iB, 75)
	ENDIF
ENDPROC

PROC DO_HELP_TEXT(INT iHelp)
	IF SHOULD_UI_BE_HIDDEN()
		EXIT
	ENDIF
	SWITCH iHelp
		CASE 0
			IF NOT IS_BIT_SET(iBoolsBitSet, biHelp0Done)
				IF  NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_RADAR_HIDDEN()
				AND IS_NET_PLAYER_OK(PLAYER_ID())
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP(TRUE)
					ENDIF
					PRINT_HELP_NO_SOUND("HTV_HELP0", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)	// You cannot enter the ~HUD_COLOUR_NET_PLAYER2~Hot Target~s~ vehicle in passive mode.
					SET_FREEMODE_EVENT_HELP_BACKGROUND()
					SET_BIT(iBoolsBitSet, biHelp0Done)
					PRINTLN("[AM MOVING TARGET] -  HELP TEXT - biHelp0Done SET    <----------     ")
				ENDIF
			ENDIF
		BREAK
		CASE 1
			IF NOT IS_BIT_SET(iBoolsBitSet, biHelp1Done)
				IF NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_RADAR_HIDDEN()
				AND IS_NET_PLAYER_OK(PLAYER_ID())
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP(TRUE)
					ENDIF
					PRINT_HELP_WITH_STRING_AND_NUMBER_NO_SOUND("HTV_HELP1", GET_VEH_BLIP_STRING(), GET_CASH_REWARD(eHT_CASH_RP_DELIVERER, TRUE), DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)	//A ~r~Hot Target~s~ ~r~~a~ ~s~is available. Enter it and take it to its destination for $~1~.
					SET_FREEMODE_EVENT_HELP_BACKGROUND(FALSE)
					SET_BIT(iBoolsBitSet, biHelp1Done)
					PLAY_SOUND_FRONTEND(-1, "Event_Start_Text", "GTAO_FM_Events_Soundset", FALSE)
					PRINTLN("[AM MOVING TARGET] -  HELP TEXT - biHelp1Done SET    <----------     ")
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF NOT IS_BIT_SET(iBoolsBitSet, biHelp2Done)
				IF NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_RADAR_HIDDEN()
				AND NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_FM_EVENT_START)
				AND IS_NET_PLAYER_OK(PLAYER_ID())
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP(TRUE)
					ENDIF
					IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
						PRINT_HELP_WITH_STRING_NO_SOUND("HTV_HELP2", GET_VEHICLE_TYPE_STRING(), DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)	//You are a ~r~Hot Target~s~. Other players will be rewarded for killing you while you are delivering the ~a~.
					ELSE
						PRINT_HELP_WITH_STRING_NO_SOUND("HTV_HELP2b", GET_VEHICLE_TYPE_STRING(), DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)	//You are the ~r~Hot Target~s~. Other players will be rewarded for killing you while you are delivering the ~a~.
					ENDIF
					SET_FREEMODE_EVENT_HELP_BACKGROUND()
					SET_BIT(iBoolsBitSet, biHelp2Done)
					PRINTLN("[AM MOVING TARGET] - HELP TEXT - biHelp2Done SET    <----------     ")
				ENDIF
			ENDIF
		BREAK
//		CASE 3
//			IF NOT IS_BIT_SET(iBoolsBitSet, biHelp3Done)
//				IF HAS_PLAYER_BEEN_IN_SESSION_THIS_LONG(CRATE_DROP_DISPLAY_DELAY)
//				AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
//				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
//				AND NOT IS_PAUSE_MENU_ACTIVE()
//					PRINT_HELP_WITH_STRING("HTV_HELP3", GET_VEH_BLIP_STRING())	//You have been moved off mission because you have passive mode enabled. Disable passive mode to participate in the event.
//					SET_BIT(iBoolsBitSet, biHelp3Done)
//					PRINTLN("[AM MOVING TARGET] - HELP TEXT - biHelp3Done SET - OUT OF VEHICLE    <----------     ")
//				ENDIF
//			ENDIF
//		BREAK
		CASE 4
			IF NOT IS_BIT_SET(iBoolsBitSet, biHelp4Done)
				IF NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_RADAR_HIDDEN()
				AND IS_NET_PLAYER_OK(PLAYER_ID())
				AND HAS_PLAYER_FULLY_SPAWNED()
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP(TRUE)
					ENDIF
					PRINT_HELP_WITH_STRING_NO_SOUND("HTV_HELP4", GET_VEHICLE_TYPE_STRING(), DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) // You were killed trying to deliver the ~a~~s~. You are unable to re-enter the vehicle as you are now off-mission.
					SET_FREEMODE_EVENT_HELP_BACKGROUND()
					SET_BIT(iBoolsBitSet, biHelp4Done)
					PRINTLN("[AM MOVING TARGET] - HELP TEXT - biHelp4Done SET    <----------     ")
				ENDIF
			ENDIF
		BREAK
		CASE 5
			IF NOT IS_BIT_SET(iBoolsBitSet, biHelp5Done)
				IF  NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_PAUSE_MENU_ACTIVE()	
				AND NOT IS_RADAR_HIDDEN()
				AND IS_NET_PLAYER_OK(PLAYER_ID())
					PRINT_HELP_NO_SOUND("HTV_HELP5", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)  // You are unable to leave the vehicle while you are the ~r~Hot Target~s~.
					SET_FREEMODE_EVENT_HELP_BACKGROUND()
					SET_BIT(iBoolsBitSet, biHelp5Done)
					PRINTLN("[AM MOVING TARGET] - HELP TEXT - biHelp5Done SET    <----------     ")
				ENDIF
			ENDIF
		BREAK
		CASE 6
			IF NOT IS_BIT_SET(iBoolsBitSet, biHelp6Done)
				IF NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_RADAR_HIDDEN()
				AND IS_NET_PLAYER_OK(PLAYER_ID())
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP(TRUE)
					ENDIF
					IF IS_THIS_MODEL_A_HELI(serverBD.VehModel)
						PRINT_HELP_NO_SOUND("HTV_HELP6b", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) // This vehicle requires a pilot and a gunner. Please wait for a gunner or until the countdown expires.
					ELSE
						PRINT_HELP_NO_SOUND("HTV_HELP6", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) // This vehicle requires a driver and a gunner. Please wait for a gunner or until the countdown expires.
					ENDIF
					SET_FREEMODE_EVENT_HELP_BACKGROUND()
					SET_BIT(iBoolsBitSet, biHelp6Done)
					PRINTLN("[AM MOVING TARGET] - HELP TEXT - biHelp6Done SET    <----------     ")
				ENDIF
			ENDIF
		BREAK
		CASE 7
			IF NOT IS_BIT_SET(iBoolsBitSet, biHelp7Done)
				IF NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_RADAR_HIDDEN()
				AND IS_NET_PLAYER_OK(PLAYER_ID())
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP(TRUE)
					ENDIF
					IF IS_THIS_MODEL_A_HELI(serverBD.VehModel)
						PRINT_HELP_NO_SOUND("HTV_HELP7b", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) // This vehicle requires a pilot and a gunner. Enter the pilot seat and wait for a gunner, or wait for a pilot to arrive.
					ELSE
						PRINT_HELP_NO_SOUND("HTV_HELP7", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) // This vehicle requires a driver and a gunner. Enter the pilot seat and wait for a gunner, or wait for a pilot to arrive.
					ENDIF
					SET_FREEMODE_EVENT_HELP_BACKGROUND()
					SET_BIT(iBoolsBitSet, biHelp7Done)
					PRINTLN("[AM MOVING TARGET] - HELP TEXT - biHelp7Done SET    <----------     ")
				ENDIF
			ENDIF
		BREAK
		CASE 8
			IF NOT IS_BIT_SET(iBoolsBitSet, biHelp8Done)
				IF NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_RADAR_HIDDEN()
				AND IS_NET_PLAYER_OK(PLAYER_ID())
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP(TRUE)
					ENDIF
					PRINT_HELP_NO_SOUND("HTV_HELP8", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) // ~HUD_COLOUR_NET_PLAYER2~Hot Target ~s~is no longer available.
					SET_FREEMODE_EVENT_HELP_BACKGROUND()
					SET_BIT(iBoolsBitSet, biHelp8Done)
					PRINTLN("[AM MOVING TARGET] - HELP TEXT - biHelp8Done SET    <----------     ")
				ENDIF
			ENDIF
		BREAK
		
		CASE 9
			IF NOT IS_BIT_SET(iBoolsBitSet, biHelp9Done)
			AND NOT IS_BIT_SET(iBoolsBitSet, biHelp2Done)
				IF NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_RADAR_HIDDEN()
				AND IS_NET_PLAYER_OK(PLAYER_ID())
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP(TRUE)
					ENDIF
					IF IS_THIS_MODEL_A_BIKE(serverBD.VehModel)
						PRINT_HELP_NO_SOUND("HTV_HELP9b", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)  //Enter the Vehicle
					ELSE
						PRINT_HELP_NO_SOUND("HTV_HELP9", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)  //Enter the Vehicle
					ENDIF
					SET_FREEMODE_EVENT_HELP_BACKGROUND()
					SET_BIT(iBoolsBitSet, biHelp9Done)
					PRINTLN("[AM MOVING TARGET] - HELP TEXT - biHelp9Done SET    <----------     ")
				ENDIF
			ENDIF
		BREAK
		
		CASE 10
			IF NOT IS_BIT_SET(iBoolsBitSet, biHelp10Done)
				IF NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_RADAR_HIDDEN()
				AND IS_NET_PLAYER_OK(PLAYER_ID())
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP(TRUE)
					ENDIF
					PRINT_HELP_NO_SOUND("HTV_HELP10", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)	//You have been moved off mission because you have passive mode enabled. Disable passive mode to participate in the event.
					SET_FREEMODE_EVENT_HELP_BACKGROUND()
					SET_BIT(iBoolsBitSet, biHelp10Done)
					PRINTLN("[AM MOVING TARGET] - HELP TEXT - biHelp10Done SET - OUT OF VEHICLE    <----------     ")
				ENDIF
			ENDIF
		BREAK
		
		CASE 11
			IF NOT IS_BIT_SET(iBoolsBitSet, biHelp11Done)
				IF NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_RADAR_HIDDEN()
				AND IS_NET_PLAYER_OK(PLAYER_ID())
				AND HAS_PLAYER_FULLY_SPAWNED()
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP(TRUE)
					ENDIF
					PRINT_HELP_WITH_STRING_NO_SOUND("HTV_HELP11", GET_VEHICLE_TYPE_STRING(), DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) // You have abandoned the ~a~~s~. You are unable to re-enter the vehicle as you are now off-mission.
					SET_FREEMODE_EVENT_HELP_BACKGROUND()
					SET_BIT(iBoolsBitSet, biHelp11Done)
					PRINTLN("[AM MOVING TARGET] - HELP TEXT - biHelp11Done SET    <----------     ")
				ENDIF
			ENDIF
		BREAK
		
		CASE 12
			IF NOT IS_BIT_SET(iBoolsBitSet, biHelp12Done)
				IF NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_RADAR_HIDDEN()
				AND IS_NET_PLAYER_OK(PLAYER_ID())
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP(TRUE)
					ENDIF
					IF IS_THIS_MODEL_A_HELI(serverBD.VehModel)
						PRINT_HELP_WITH_STRING_NO_SOUND("HTV_HELP12b", GET_VEHICLE_TYPE_STRING(), DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) // The driver is currently waiting for a gunner. Enter the gunner position and help them deliver the ~a~ ~s~as another ~HUD_COLOUR_NET_PLAYER2~Hot Target.~s~
					ELSE
						PRINT_HELP_WITH_STRING_NO_SOUND("HTV_HELP12", GET_VEHICLE_TYPE_STRING(), DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) // The driver is currently waiting for a gunner. Enter the gunner position and help them deliver the ~a~ ~s~as another ~HUD_COLOUR_NET_PLAYER2~Hot Target.~s~
					ENDIF
					SET_FREEMODE_EVENT_HELP_BACKGROUND()
					SET_BIT(iBoolsBitSet, biHelp12Done)
					PRINTLN("[AM MOVING TARGET] - HELP TEXT - biHelp12Done SET    <----------     ")
				ENDIF
			ENDIF
		BREAK
		CASE 13
			IF NOT IS_BIT_SET(iBoolsBitSet, biHelp13Done)
				IF NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_RADAR_HIDDEN()
				AND IS_NET_PLAYER_OK(PLAYER_ID())
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP(TRUE)
					ENDIF
					IF IS_THIS_MODEL_A_HELI(serverBD.VehModel)
						PRINT_HELP_NO_SOUND("HTV_HELP13b", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) // The pilot has abandoned the event. Enter the pilot seat and deliver the vehicle.
					ELSE
						PRINT_HELP_NO_SOUND("HTV_HELP13", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) // The driver has abandoned the event. Enter the driver seat and deliver the vehicle.
					ENDIF
					SET_FREEMODE_EVENT_HELP_BACKGROUND()
					SET_BIT(iBoolsBitSet, biHelp13Done)
					PRINTLN("[AM MOVING TARGET] - HELP TEXT - biHelp13Done SET    <----------     ")
				ENDIF
			ENDIF
		BREAK
		CASE 14
			IF NOT IS_BIT_SET(iBoolsBitSet, biHelp14Done)
				IF NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_RADAR_HIDDEN()
				AND IS_NET_PLAYER_OK(PLAYER_ID())
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP(TRUE)
					ENDIF
					IF IS_THIS_MODEL_A_HELI(serverBD.VehModel)
						PRINT_HELP_NO_SOUND("HTV_HELP14b", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) // The pilot has been killed. Enter the pilot seat and deliver the vehicle.
					ELSE
						PRINT_HELP_NO_SOUND("HTV_HELP14", DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL) // The driver has been killed. Enter the driver seat and deliver the vehicle.
					ENDIF
					SET_FREEMODE_EVENT_HELP_BACKGROUND()
					SET_BIT(iBoolsBitSet, biHelp14Done)
					PRINTLN("[AM MOVING TARGET] - HELP TEXT - biHelp14Done SET    <----------     ")
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL POSTPONE_SHARD()

	IF NOT HAS_NET_TIMER_STARTED(PostponeShard)
		START_NET_TIMER(PostponeShard)
	ELSE
		IF HAS_NET_TIMER_EXPIRED(PostponeShard, 1000)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Draws big messages at the start and end of the event
/// PARAMS:
///    iBigMessage - the index of the big message to draw
PROC DO_BIG_MESSAGE(INT iBigMessage)
	IF SHOULD_UI_BE_HIDDEN()
		EXIT
	ENDIF
	SWITCH iBigMessage
		CASE 0
			// GO. You are the Hot Target. Deliver the vehicle
			IF NOT IS_BIT_SET(iBoolsBitSet2, biBigM0Done)
			AND NOT IS_BIT_SET(iBoolsBitSet3, biStartShardDone)
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_BIT_SET(iBoolsBitSet2, biBigM1Done)
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP()
					ENDIF
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_START, "HT_BMT0", "HT_BMS0", GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE(), DEFAULT, GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE())
					SET_BIT(iBoolsBitSet2, biBigM0Done)
					SET_BIT(iBoolsBitSet3, biStartShardDone)
					PRINTLN("[AM MOVING TARGET] - BIG MESSAGE - biBigM0Done SET    <----------     ")
				ENDIF
			ENDIF
		BREAK
		CASE 1
			// GO. You are a Hot Target. Deliver the vehicle
			IF NOT IS_BIT_SET(iBoolsBitSet2, biBigM1Done)
			AND NOT IS_BIT_SET(iBoolsBitSet3, biStartShardDone)
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_BIT_SET(iBoolsBitSet2, biBigM0Done)
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP()
					ENDIF
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_START, "HT_BMT0", "HT_BMS1", GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE(), DEFAULT, GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE())
					SET_BIT(iBoolsBitSet2, biBigM1Done)
					SET_BIT(iBoolsBitSet3, biStartShardDone)
					PRINTLN("[AM MOVING TARGET] - BIG MESSAGE - biBigM1Done SET    <----------     ")
				ENDIF
			ENDIF
		BREAK
		CASE 2
			// HOT TARGET. ~a~ is the Hot Target. Take them out
			IF NOT IS_BIT_SET(iBoolsBitSet2, biBigM2Done)
			AND NOT IS_BIT_SET(iBoolsBitSet3, biStartShardDone)
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_BIT_SET(iBoolsBitSet2, biBigM3Done)
					IF serverBD.piDriver != INVALID_PLAYER_INDEX()
						IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piDriver)
							IF IS_HELP_MESSAGE_BEING_DISPLAYED()
								CLEAR_HELP()
							ENDIF
							
							IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
								IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(serverBD.piDriver, GB_GET_LOCAL_PLAYER_GANG_BOSS())
									SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_FM_EVENT_START, "HT_BMS2b", GET_PLAYER_NAME(serverBD.piDriver), GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE(), DEFAULT, "HT_BMT1", GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE())
								ELSE
									SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_FM_EVENT_START, "HT_BMS2", GET_PLAYER_NAME(serverBD.piDriver), GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE(), DEFAULT, "HT_BMT1", GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE())
								ENDIF
							ELSE
								SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_FM_EVENT_START, "HT_BMS2", GET_PLAYER_NAME(serverBD.piDriver), GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE(), DEFAULT, "HT_BMT1", GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE())
							ENDIF
							
						
							SET_BIT(iBoolsBitSet2, biBigM2Done)
							SET_BIT(iBoolsBitSet3, biStartShardDone)
							PRINTLN("[AM MOVING TARGET] - BIG MESSAGE - biBigM2Done SET    <----------     ")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 3
			// HOT TARGET. ~a~ and ~a~ are the Hot Targets. Take them out
			IF NOT IS_BIT_SET(iBoolsBitSet2, biBigM3Done)
			AND NOT IS_BIT_SET(iBoolsBitSet3, biStartShardDone)
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_BIT_SET(iBoolsBitSet2, biBigM2Done)
					IF serverBD.piDriver != INVALID_PLAYER_INDEX()
					AND serverBD.piGunner != INVALID_PLAYER_INDEX()
						IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piDriver)
						AND NETWORK_IS_PLAYER_ACTIVE(serverBD.piGunner)
							IF IS_HELP_MESSAGE_BEING_DISPLAYED()
								CLEAR_HELP()
							ENDIF
							
							IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
							
								IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(serverBD.piDriver, GB_GET_LOCAL_PLAYER_GANG_BOSS())
								AND GB_IS_PLAYER_MEMBER_OF_THIS_GANG(serverBD.piGunner, GB_GET_LOCAL_PLAYER_GANG_BOSS())
									// driver and gunner are in my gang
									SETUP_NEW_BIG_MESSAGE_WITH_2_PLAYERS(BIG_MESSAGE_FM_EVENT_START, INVALID_PLAYER_INDEX(), INVALID_PLAYER_INDEX(), -1, "HT_BMS3b", "HT_BMT1", GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE(), DEFAULT, GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE(), GET_PLAYER_NAME(serverBD.piDriver), GET_PLAYER_NAME(serverBD.piGunner))
								ELIF GB_GET_LOCAL_PLAYER_GANG_BOSS() = serverBD.piDriver
								AND NOT GB_IS_PLAYER_MEMBER_OF_THIS_GANG(serverBD.piGunner, GB_GET_LOCAL_PLAYER_GANG_BOSS())
									// Driver is my boss, gunner is not in gang
									SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_FM_EVENT_START, "HT_BMS2c", GET_PLAYER_NAME(serverBD.piDriver), DEFAULT, -1, "HT_BMT1", DEFAULT)
								ELIF GB_GET_LOCAL_PLAYER_GANG_BOSS() = serverBD.piGunner
								AND NOT GB_IS_PLAYER_MEMBER_OF_THIS_GANG(serverBD.piDriver, GB_GET_LOCAL_PLAYER_GANG_BOSS())
									// Gunner is boss, driver is not in gang
									SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_FM_EVENT_START, "HT_BMS2c", GET_PLAYER_NAME(serverBD.piGunner), DEFAULT, -1, "HT_BMT1", DEFAULT)
								ELSE
									// Kill any others, even other goons
									SETUP_NEW_BIG_MESSAGE_WITH_2_PLAYERS(BIG_MESSAGE_FM_EVENT_START, INVALID_PLAYER_INDEX(), INVALID_PLAYER_INDEX(), -1, "HT_BMS3", "HT_BMT1", GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE(), DEFAULT, GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE(), GET_PLAYER_NAME(serverBD.piDriver), GET_PLAYER_NAME(serverBD.piGunner))
								ENDIF
							
							ELSE
								SETUP_NEW_BIG_MESSAGE_WITH_2_PLAYERS(BIG_MESSAGE_FM_EVENT_START, INVALID_PLAYER_INDEX(), INVALID_PLAYER_INDEX(), -1, "HT_BMS3", "HT_BMT1", GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE(), DEFAULT, GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE(), GET_PLAYER_NAME(serverBD.piDriver), GET_PLAYER_NAME(serverBD.piGunner))
							ENDIF
							
							SET_BIT(iBoolsBitSet2, biBigM3Done)
							SET_BIT(iBoolsBitSet3, biStartShardDone)
							PRINTLN("[AM MOVING TARGET] - BIG MESSAGE - biBigM3Done SET    <----------     ")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 4
			// WINNER. You delivered the vehicle. It will explode in ~1~ seconds
			IF NOT IS_BIT_SET(iBoolsBitSet2, biBigM4Done)
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
					IF POSTPONE_SHARD()
						IF AM_I_THE_DRIVER()
							SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_FM_EVENT_WIN, "", "HT_BMS4", "HT_BMT2", ciFM_EVENTS_END_UI_SHARD_TIME, (g_sMPTunables.iVehicleExplosionTime/1000))
						ELSE
							// If you died as the hot target but other delivered, say other delivered
							IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerKilled)
							OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_OutOfVehicleFail)
								SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_FM_EVENT_WIN, "", "HT_BMS4b", "HT_BMT2", ciFM_EVENTS_END_UI_SHARD_TIME, (g_sMPTunables.iVehicleExplosionTime/1000))
							ELSE					
								SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_FM_EVENT_WIN, "", "HT_BMS4", "HT_BMT2", ciFM_EVENTS_END_UI_SHARD_TIME, (g_sMPTunables.iVehicleExplosionTime/1000))
							ENDIF
						ENDIF
						SET_BIT(iBoolsBitSet2, biBigM4Done)
						PRINTLN("[AM MOVING TARGET] - BIG MESSAGE - biBigM4Done SET    <----------     ")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 5
			// EVENT OVER. ~a~ delivered the Hot Target vehicle
			IF NOT IS_BIT_SET(iBoolsBitSet2, biBigM5Done)
			AND NOT IS_BIT_SET(iBoolsBitSet3, biEndTickerDone)
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
					IF POSTPONE_SHARD()
						IF serverBD.piDriver != INVALID_PLAYER_INDEX()
							IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piDriver)
								CLEAR_ALL_BIG_MESSAGES()
								
								IF GB_IS_PLAYER_MEMBER_OF_A_GANG(serverBD.piDriver)
									SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_FM_EVENT_FINISHED, "HT_BMS5b", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(serverBD.piDriver), DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME, "HT_BMT3")
								ELSE
									SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_FM_EVENT_FINISHED, "HT_BMS5", GET_PLAYER_NAME(serverBD.piDriver), DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME, "HT_BMT3")
								ENDIF
								
								SET_BIT(iBoolsBitSet2, biBigM5Done)
								SET_BIT(iBoolsBitSet2, biBigM9Done)
								PRINTLN("[AM MOVING TARGET] - BIG MESSAGE - biBigM5Done SET    <----------     ")
								PRINTLN("[AM MOVING TARGET] - BIG MESSAGE - biBigM9Done ALSO SET (prevent multi shard)   <----------     ")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 6
			// EVENT OVER. You failed to deliver the vehicle
			IF NOT IS_BIT_SET(iBoolsBitSet2, biBigM6Done)
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
					IF POSTPONE_SHARD()
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_FAIL, "HT_BMT3", "HT_BMS6", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME)
						SET_BIT(iBoolsBitSet2, biBigM6Done)
						PRINTLN("[AM MOVING TARGET] - BIG MESSAGE - biBigM6Done SET    <----------     ")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 7
			// EVENT OVER. The Hot Target failed to deliver the vehicle
			IF NOT IS_BIT_SET(iBoolsBitSet2, biBigM7Done)
			AND NOT IS_BIT_SET(iBoolsBitSet3, biEndTickerDone)
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
					IF POSTPONE_SHARD()
						CLEAR_ALL_BIG_MESSAGES()
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_FINISHED, "HT_BMT3", "HT_BMS7", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME)
						SET_BIT(iBoolsBitSet2, biBigM7Done)
						PRINTLN("[AM MOVING TARGET] - BIG MESSAGE - biBigM7Done SET    <----------     ")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 8
			// EVENT OVER. The Hot Targets failed to deliver the vehicle
			IF NOT IS_BIT_SET(iBoolsBitSet2, biBigM8Done)
			AND NOT IS_BIT_SET(iBoolsBitSet3, biEndTickerDone)
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
					IF POSTPONE_SHARD()
						CLEAR_ALL_BIG_MESSAGES()
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_FINISHED, "HT_BMT3", "HT_BMS8", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME)
						SET_BIT(iBoolsBitSet2, biBigM8Done)
						PRINTLN("[AM MOVING TARGET] - BIG MESSAGE - biBigM8Done SET    <----------     ")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 9
			// EVENT OVER. ~a~ and ~a~ delivered the Hot Target vehicle
			IF NOT IS_BIT_SET(iBoolsBitSet2, biBigM9Done)
			AND NOT IS_BIT_SET(iBoolsBitSet3, biEndTickerDone)
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
					IF serverBD.piDriver != INVALID_PLAYER_INDEX()
					AND serverBD.piGunner != INVALID_PLAYER_INDEX()
						IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piDriver)
						AND NETWORK_IS_PLAYER_ACTIVE(serverBD.piGunner)
							IF POSTPONE_SHARD()
								CLEAR_ALL_BIG_MESSAGES()
								
								// Driver and Gunner are both in gangs
								IF GB_IS_PLAYER_MEMBER_OF_A_GANG(serverBD.piDriver)
								AND GB_IS_PLAYER_MEMBER_OF_A_GANG(serverBD.piGunner)
									// IF they're in the same gang
									IF GB_GET_THIS_PLAYER_GANG_BOSS(serverBD.piDriver) = GB_GET_THIS_PLAYER_GANG_BOSS(serverBD.piGunner)
										SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_FM_EVENT_FINISHED, "HT_BMS5", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(serverBD.piDriver), DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME, "HT_BMT3")
									ELSE
										SETUP_NEW_BIG_MESSAGE_WITH_2_PLAYERS(BIG_MESSAGE_FM_EVENT_FINISHED, INVALID_PLAYER_INDEX(), INVALID_PLAYER_INDEX(), -1, "HT_BMS9", "HT_BMT3", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME, DEFAULT, GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(serverBD.piDriver), GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(serverBD.piGunner))
									ENDIF
								// IF driver is in a gang, but gunner isnt
								ELIF GB_IS_PLAYER_MEMBER_OF_A_GANG(serverBD.piDriver)
								AND NOT GB_IS_PLAYER_MEMBER_OF_A_GANG(serverBD.piGunner)
									SETUP_NEW_BIG_MESSAGE_WITH_2_PLAYERS(BIG_MESSAGE_FM_EVENT_FINISHED, INVALID_PLAYER_INDEX(), INVALID_PLAYER_INDEX(), -1, "HT_BMS9", "HT_BMT3", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME, DEFAULT, GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(serverBD.piDriver), GET_PLAYER_NAME(serverBD.piGunner))
								// If gunner is in a gang, but the driver isnt
								ELIF GB_IS_PLAYER_MEMBER_OF_A_GANG(serverBD.piGunner)
								AND NOT GB_IS_PLAYER_MEMBER_OF_A_GANG(serverBD.piDriver)
									SETUP_NEW_BIG_MESSAGE_WITH_2_PLAYERS(BIG_MESSAGE_FM_EVENT_FINISHED, INVALID_PLAYER_INDEX(), INVALID_PLAYER_INDEX(), -1, "HT_BMS9", "HT_BMT3", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME, DEFAULT, GET_PLAYER_NAME(serverBD.piDriver), GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(serverBD.piGunner))
								ELSE
									SETUP_NEW_BIG_MESSAGE_WITH_2_PLAYERS(BIG_MESSAGE_FM_EVENT_FINISHED, INVALID_PLAYER_INDEX(), INVALID_PLAYER_INDEX(), -1, "HT_BMS9", "HT_BMT3", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME, DEFAULT, GET_PLAYER_NAME(serverBD.piDriver), GET_PLAYER_NAME(serverBD.piGunner))
								ENDIF
								
								SET_BIT(iBoolsBitSet2, biBigM9Done)
								SET_BIT(iBoolsBitSet2, biBigM5Done)
								PRINTLN("[AM MOVING TARGET] - BIG MESSAGE - biBigM9Done SET    <----------     ")
								PRINTLN("[AM MOVING TARGET] - BIG MESSAGE - biBigM5Done ALSO SET (prevent multi shard)   <----------     ")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 10
			// EVENT OVER. The Hot Targets abandoned the event
			IF NOT IS_BIT_SET(iBoolsBitSet2, biBigM10Done)
			AND NOT IS_BIT_SET(iBoolsBitSet3, biEndTickerDone)
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
					IF POSTPONE_SHARD()
						CLEAR_ALL_BIG_MESSAGES()
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_FINISHED, "HT_BMT3", "HT_BMS10", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME)
						SET_BIT(iBoolsBitSet2, biBigM10Done)
						SET_BIT(iBoolsBitSet2, biBigM11Done)
						PRINTLN("[AM MOVING TARGET] - BIG MESSAGE - biBigM10Done SET    <----------     ")
						PRINTLN("[AM MOVING TARGET] - BIG MESSAGE - biBigM11Done ALSO SET (prevent multi shard)   <----------     ")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 11
			// EVENT OVER. The Hot Target abandoned the event
			IF NOT IS_BIT_SET(iBoolsBitSet2, biBigM11Done)
			AND NOT IS_BIT_SET(iBoolsBitSet3, biEndTickerDone)
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
					IF POSTPONE_SHARD()
						CLEAR_ALL_BIG_MESSAGES()
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_FINISHED, "HT_BMT3", "HT_BMS11", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME)
						SET_BIT(iBoolsBitSet2, biBigM11Done)
						SET_BIT(iBoolsBitSet2, biBigM10Done)
						PRINTLN("[AM MOVING TARGET] - BIG MESSAGE - biBigM11Done SET    <----------     ")
						PRINTLN("[AM MOVING TARGET] - BIG MESSAGE - biBigM10Done ALSO SET (prevent multi shard)   <----------     ")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 12
			// WINNER. You eliminated a Hot Target
			IF NOT IS_BIT_SET(iBoolsBitSet2, biBigM12Done)
			AND NOT IS_BIT_SET(iBoolsBitSet3, biEndTickerDone)
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
					IF POSTPONE_SHARD()
						CLEAR_ALL_BIG_MESSAGES()
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_WIN, "HT_BMT2", "HT_BMS12", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME)
						SET_BIT(iBoolsBitSet2, biBigM12Done)
						PRINTLN("[AM MOVING TARGET] - BIG MESSAGE - biBigM12Done SET    <----------     ")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 13
			// WINNER. You eliminated the Hot Target
			IF NOT IS_BIT_SET(iBoolsBitSet2, biBigM13Done)
			AND NOT IS_BIT_SET(iBoolsBitSet3, biEndTickerDone)
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
					IF POSTPONE_SHARD()
						CLEAR_ALL_BIG_MESSAGES()
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_WIN, "HT_BMT2", "HT_BMS13", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME)
						SET_BIT(iBoolsBitSet2, biBigM13Done)
						PRINTLN("[AM MOVING TARGET] - BIG MESSAGE - biBigM13Done SET    <----------     ")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 14
			// WINNER. You eliminated the Hot Targets
			IF NOT IS_BIT_SET(iBoolsBitSet2, biBigM14Done)
			AND NOT IS_BIT_SET(iBoolsBitSet3, biEndTickerDone)
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
					IF POSTPONE_SHARD()
						CLEAR_ALL_BIG_MESSAGES()
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_WIN, "HT_BMT2", "HT_BMS14", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME)
						SET_BIT(iBoolsBitSet2, biBigM14Done)
						PRINTLN("[AM MOVING TARGET] - BIG MESSAGE - biBigM14Done SET    <----------     ")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 15
			// EVENT OVER. The Hot Targets have been eliminated
			IF NOT IS_BIT_SET(iBoolsBitSet2, biBigM15Done)
			AND NOT IS_BIT_SET(iBoolsBitSet3, biEndTickerDone)
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
					IF POSTPONE_SHARD()
						CLEAR_ALL_BIG_MESSAGES()
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_FINISHED, "HT_BMT3", "HT_BMS15", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME)
						SET_BIT(iBoolsBitSet2, biBigM15Done)
						PRINTLN("[AM MOVING TARGET] - BIG MESSAGE - biBigM15Done SET    <----------     ")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 16
			// EVENT OVER. The Hot Target has been eliminated
			IF NOT IS_BIT_SET(iBoolsBitSet2, biBigM16Done)
			AND NOT IS_BIT_SET(iBoolsBitSet3, biEndTickerDone)
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
					IF POSTPONE_SHARD()
						CLEAR_ALL_BIG_MESSAGES()
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_FM_EVENT_FINISHED, "HT_BMT3", "HT_BMS16", DEFAULT, ciFM_EVENTS_END_UI_SHARD_TIME )
						SET_BIT(iBoolsBitSet2, biBigM16Done)
						PRINTLN("[AM MOVING TARGET] - BIG MESSAGE - biBigM16Done SET    <----------     ")
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC SET_HOT_TARGET_VEHICLE_MODS()
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niVeh)
		IF DOES_ENTITY_EXIST(NET_TO_VEH(serverBD.niVeh))
			IF GET_NUM_MOD_KITS(NET_TO_VEH(serverBD.niVeh)) > 0
				SET_VEHICLE_MOD_KIT(NET_TO_VEH(serverBD.niVeh), 0)
				INT iModSet = GET_RANDOM_INT_IN_RANGE(0,3)
				SWITCH serverBD.VehModel
					CASE ZENTORNO
						IF iModSet = 0
							// Red and Black
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_SPOILER, 4) // Carbon Wing
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_SKIRT, 1)	// Secondary Colour
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_EXHAUST, 1)	// Single Hexa Exhaust
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_CHASSIS, 1)	// Secondary Colour Side Panel
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_BONNET, 1)	// Secondary Colour Stripe
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_WHEELS, 8)	// 6-Spoke Super
							SET_VEHICLE_COLOURS(NET_TO_VEH(serverBD.niVeh), 27, 21)
							PRINTLN("[AM MOVING TARGET] - SET_HOT_TARGET_VEHICLE_MODS - Red/Black Zentorno")
						ELIF iModSet = 1
							// Matte Black
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_SPOILER, 6) // Race
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_SKIRT, 2)	// Carbon
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_EXHAUST, 0)	// Twin Round Exhaust
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_WHEELS, 18)	// Carbon
							SET_VEHICLE_MOD_COLOR_1(NET_TO_VEH(serverBD.niVeh), MCT_MATTE, 0, 0)	// Matte Black
							SET_VEHICLE_MOD_COLOR_2(NET_TO_VEH(serverBD.niVeh), MCT_MATTE, 0)		// Matte Black
							PRINTLN("[AM MOVING TARGET] - SET_HOT_TARGET_VEHICLE_MODS - Matte Black Zentorno")
						ELSE
							// Green
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_SPOILER, 2) // Carbon Standard Spoiler
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_EXHAUST, 1)	// Single Hexa Exhaust
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_CHASSIS, 1)	// Secondary Colour Side Panel
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_BONNET, 1)	// Smooth (No Stripe)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_WHEELS, 4)	// 6-Spoke Super
							SET_VEHICLE_COLOURS(NET_TO_VEH(serverBD.niVeh), 57, 21)	
							PRINTLN("[AM MOVING TARGET] - SET_HOT_TARGET_VEHICLE_MODS - Green Zentorno")
						ENDIF
					BREAK
					
					CASE DUKES2
						// Leave the dukes alone for the classic Mad Max look
						PRINTLN("[AM MOVING TARGET] - SET_HOT_TARGET_VEHICLE_MODS - Standard Mad Max Dukes")
					BREAK
					
					CASE KURUMA2
						IF iModSet = 0
							// Leave the first mod set to be the default Matte black appearance
							PRINTLN("[AM MOVING TARGET] - SET_HOT_TARGET_VEHICLE_MODS - Matte Black Armoured kuruma")
						ELIF iModSet = 1
							// Dark Red and Black
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_BUMPER_F, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_SKIRT, 5)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_EXHAUST, 1)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_SUSPENSION, 3)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_WHEELS, 15)
							SET_VEHICLE_COLOURS(NET_TO_VEH(serverBD.niVeh), 33, 0)
							PRINTLN("[AM MOVING TARGET] - SET_HOT_TARGET_VEHICLE_MODS - Red/Black Armoured Kuruma")
						ELSE
							// Matte Blue and Black
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_SPOILER, 1)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_BUMPER_F, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_SKIRT, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_EXHAUST, 1)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_WHEELS, 10)
							SET_VEHICLE_COLOURS(NET_TO_VEH(serverBD.niVeh), 83, 12)
							PRINTLN("[AM MOVING TARGET] - SET_HOT_TARGET_VEHICLE_MODS - Blue/Black Armoured Kuruma")
						ENDIF
					BREAK
					
					CASE GUARDIAN
						IF iModSet = 0
							// Blue slammed
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_EXHAUST, 0)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_ENGINE, 3)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_BRAKES, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_GEARBOX, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_SUSPENSION, 4)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_ARMOUR, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_WHEELS, 0)
							SET_VEHICLE_COLOURS(NET_TO_VEH(serverBD.niVeh), 75, 12)
							PRINTLN("[AM MOVING TARGET] - SET_HOT_TARGET_VEHICLE_MODS - Dark Blue Guardian")
						ELIF iModSet = 1
							// Matte Lime Green Slammed
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_EXHAUST, 0)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_ENGINE, 3)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_BRAKES, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_GEARBOX, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_SUSPENSION, 4)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_ARMOUR, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_WHEELS, 19)
							SET_VEHICLE_COLOURS(NET_TO_VEH(serverBD.niVeh), 55, 12)
							PRINTLN("[AM MOVING TARGET] - SET_HOT_TARGET_VEHICLE_MODS - Lime Green Guardian")
						ELSE
							// Default Guardian with Rims
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_WHEELS, 11)
							PRINTLN("[AM MOVING TARGET] - SET_HOT_TARGET_VEHICLE_MODS - Matte Black Guardian")
						ENDIF
					BREAK
					
					CASE PANTO
						IF iModSet = 0
							// Red w/ Carbon Spoiler
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_SPOILER, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_BUMPER_F, 1)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_SKIRT, 3)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_EXHAUST, 4)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_GRILL, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_ENGINE, 3)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_BRAKES, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_GEARBOX, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_SUSPENSION, 4)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_WHEELS, 20)
							SET_VEHICLE_COLOURS(NET_TO_VEH(serverBD.niVeh), 30, 30)
							PRINTLN("[AM MOVING TARGET] - SET_HOT_TARGET_VEHICLE_MODS - Red Panto")
						ELIF iModSet = 1
							// Yellow with Stickers
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_SPOILER, 4)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_BUMPER_F, 3)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_BUMPER_R, 1)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_SKIRT, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_EXHAUST, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_GRILL, 1)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_ENGINE, 3)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_BRAKES, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_GEARBOX, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_SUSPENSION, 4)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_WHEELS, 20)
							SET_VEHICLE_COLOURS(NET_TO_VEH(serverBD.niVeh), 88, 24)
							PRINTLN("[AM MOVING TARGET] - SET_HOT_TARGET_VEHICLE_MODS - Yellow/Stickered Panto")
						ELSE
							// Matte Black with Carbon
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_SPOILER, 1)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_BUMPER_F, 1)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_BUMPER_R, 0)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_SKIRT, 3)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_EXHAUST, 3)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_GRILL, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_ENGINE, 3)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_BRAKES, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_GEARBOX, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_SUSPENSION, 4)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_ARMOUR, 4)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_WHEELS, 13)
							SET_VEHICLE_MOD_COLOR_1(NET_TO_VEH(serverBD.niVeh), MCT_MATTE, 0, 0)	// Matte Black
							SET_VEHICLE_MOD_COLOR_2(NET_TO_VEH(serverBD.niVeh), MCT_MATTE, 0)		// Matte Black
							PRINTLN("[AM MOVING TARGET] - SET_HOT_TARGET_VEHICLE_MODS - Matte Black Panto")
						ENDIF
					BREAK
					
					CASE INSURGENT
						IF iModSet = 0
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_ENGINE, 3)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_SUSPENSION, 4)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_WHEELS, 11)
							PRINTLN("[AM MOVING TARGET] - SET_HOT_TARGET_VEHICLE_MODS - Standard Insurgent w/ Rims")
						ELIF iModSet = 1
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_ENGINE, 3)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_SUSPENSION, 4)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_WHEELS, 7)
							SET_VEHICLE_MOD_COLOR_1(NET_TO_VEH(serverBD.niVeh), MCT_MATTE, 0, 0)	// Matte Black
							SET_VEHICLE_MOD_COLOR_2(NET_TO_VEH(serverBD.niVeh), MCT_MATTE, 0)		// Matte Black
							PRINTLN("[AM MOVING TARGET] - SET_HOT_TARGET_VEHICLE_MODS - Matte Black Insurgent")
						ELSE
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_ENGINE, 3)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_SUSPENSION, 4)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_WHEELS, 3)
							SET_VEHICLE_COLOURS(NET_TO_VEH(serverBD.niVeh), 126, 12)
							PRINTLN("[AM MOVING TARGET] - SET_HOT_TARGET_VEHICLE_MODS - Gold/Bronze Insurgent")
						ENDIF
					BREAK
					
					CASE TECHNICAL
						IF iModSet = 0
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_ENGINE, 3)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_SUSPENSION, 4)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_WHEELS, 6)
							SET_VEHICLE_COLOURS(NET_TO_VEH(serverBD.niVeh), 132, 12)
							PRINTLN("[AM MOVING TARGET] - SET_HOT_TARGET_VEHICLE_MODS")
						ELIF iModSet = 1
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_ENGINE, 3)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_SUSPENSION, 4)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_WHEELS, 0)
							SET_VEHICLE_COLOURS(NET_TO_VEH(serverBD.niVeh), 73, 12)
							PRINTLN("[AM MOVING TARGET] - SET_HOT_TARGET_VEHICLE_MODS")
						ELSE
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_ENGINE, 3)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_SUSPENSION, 4)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_WHEELS, 9)
							SET_VEHICLE_COLOURS(NET_TO_VEH(serverBD.niVeh), 132, 12)
							PRINTLN("[AM MOVING TARGET] - SET_HOT_TARGET_VEHICLE_MODS")
						ENDIF
					BREAK
					
					CASE ENDURO
						IF iModSet = 0
							// Yellow
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_EXHAUST, 1)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_ENGINE, 3)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_BRAKES, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_GEARBOX, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_ARMOUR, 4)
							SET_VEHICLE_COLOURS(NET_TO_VEH(serverBD.niVeh), 42, 0)
							PRINTLN("[AM MOVING TARGET] - SET_HOT_TARGET_VEHICLE_MODS - Yellow Enduro")
						ELIF iModSet = 1
							// Green
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_EXHAUST, 0)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_ENGINE, 3)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_BRAKES, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_GEARBOX, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_ARMOUR, 4)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_WHEELS, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_REAR_WHEELS, 7)
							SET_VEHICLE_COLOURS(NET_TO_VEH(serverBD.niVeh), 92, 0)
							PRINTLN("[AM MOVING TARGET] - SET_HOT_TARGET_VEHICLE_MODS - Lime Green Enduro")
						ELSE
							// White
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_ENGINE, 3)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_BRAKES, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_GEARBOX, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_ARMOUR, 4)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_WHEELS, 6)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_REAR_WHEELS, 6)
							SET_VEHICLE_COLOURS(NET_TO_VEH(serverBD.niVeh), 111, 0)
							PRINTLN("[AM MOVING TARGET] - SET_HOT_TARGET_VEHICLE_MODS - Ice White Enduro")
						ENDIF
					BREAK
					
					CASE LECTRO
						IF iModSet = 0
							// Orange/Black
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_ARMOUR, 4)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_WHEELS, 7)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_REAR_WHEELS, 7)
							SET_VEHICLE_COLOURS(NET_TO_VEH(serverBD.niVeh), 36, 36)
							PRINTLN("[AM MOVING TARGET] - SET_HOT_TARGET_VEHICLE_MODS - Orange/Black Lectro")
						ELIF iModSet = 1
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_ARMOUR, 4)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_WHEELS, 5)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_REAR_WHEELS, 8)
							SET_VEHICLE_COLOURS(NET_TO_VEH(serverBD.niVeh), 111, 28)
							PRINTLN("[AM MOVING TARGET] - SET_HOT_TARGET_VEHICLE_MODS - Ice White Lectro")
						ELSE
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_ARMOUR, 4)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_WHEELS, 4)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_REAR_WHEELS, 4)
							SET_VEHICLE_MOD_COLOR_1(NET_TO_VEH(serverBD.niVeh), MCT_MATTE, 0, 0)	// Matte Black
							SET_VEHICLE_MOD_COLOR_2(NET_TO_VEH(serverBD.niVeh), MCT_MATTE, 0)		// Matte Black
							PRINTLN("[AM MOVING TARGET] - SET_HOT_TARGET_VEHICLE_MODS - Matte Black Lectro")
						ENDIF
					BREAK
					CASE VINDICATOR
						IF iModSet = 0
							// Lime Green/Black
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_ENGINE, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_BRAKES, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_GEARBOX, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_ARMOUR, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_WHEELS, 5)
							SET_VEHICLE_COLOURS(NET_TO_VEH(serverBD.niVeh), 55, 0)
							PRINTLN("[AM MOVING TARGET] - SET_HOT_TARGET_VEHICLE_MODS - Lime Green/Black Vindicator")
						ELIF iModSet = 1
							// Orange
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_ENGINE, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_BRAKES, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_GEARBOX, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_ARMOUR, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_WHEELS, 7)
							SET_VEHICLE_COLOURS(NET_TO_VEH(serverBD.niVeh), 41, 41)
							PRINTLN("[AM MOVING TARGET] - SET_HOT_TARGET_VEHICLE_MODS - Orange Vindicator")
						ELSE
							// Black
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_ENGINE, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_BRAKES, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_GEARBOX, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_ARMOUR, 2)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_WHEELS, 10)
							SET_VEHICLE_MOD(NET_TO_VEH(serverBD.niVeh), MOD_REAR_WHEELS, 10)
							SET_VEHICLE_COLOURS(NET_TO_VEH(serverBD.niVeh), 55, 0)
							PRINTLN("[AM MOVING TARGET] - SET_HOT_TARGET_VEHICLE_MODS - Black Vindicator")
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  ENTITY CREATION                  /////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////
///    		VEH AND DRIVER    			///
///////////////////////////////////////////

PROC LOCK_VEHICLE_DOORS()
	SWITCH serverBD.VehModel
		CASE ZENTORNO
		CASE PANTO
		CASE DUKES2
		CASE INSURGENT
		CASE TECHNICAL
		CASE KURUMA2
		CASE GUARDIAN
		CASE MAVERICK
		CASE FROGGER
		CASE BUZZARD
		CASE SWIFT
			SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(NET_TO_VEH(serverBD.niVeh),ENUM_TO_INT(SC_DOOR_FRONT_RIGHT), VEHICLELOCK_LOCKED)
		BREAK	
	ENDSWITCH
ENDPROC

FUNC BOOL CREATE_VEH()
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niVeh)
		IF REQUEST_LOAD_MODEL(serverBD.VehModel)
			IF NETWORK_IS_PLAYER_IN_MP_CUTSCENE(PLAYER_ID())
				SET_NETWORK_CUTSCENE_ENTITIES(TRUE)
			ENDIF
			IF CREATE_NET_VEHICLE(serverBD.niVeh, serverBD.VehModel, serverBD.vStartLocation, serverBD.fStartHeading, DEFAULT, DEFAULT, DEFAULT, TRUE, FALSE)	
				NETWORK_FADE_IN_ENTITY(NET_TO_VEH(serverBD.niVeh), TRUE)
					
				// Set vehicle mods if a car or bike
				IF serverBD.iVehicleType = VT_CAR
				OR serverBD.iVehicleType = VT_BIKE
					PRINTLN("[AM MOVING TARGET] - SETTING HOT TARGET VEHICLE MODIFICATIONS")
					SET_HOT_TARGET_VEHICLE_MODS()
					SET_VEHICLE_TYRES_CAN_BURST(NET_TO_VEH(serverBD.niVeh), FALSE)
				ELIF serverBD.VehModel = SWIFT
					PRINTLN("[AM MOVING TARGET] - SWIFT being used for Hot Target - Setting Livery and Colours to classic Black")
					SET_VEHICLE_LIVERY(NET_TO_VEH(serverBD.niVeh), 0)
					SET_VEHICLE_COLOURS(NET_TO_VEH(serverBD.niVeh), 0, 0)
				ENDIF
								
				LOCK_VEHICLE_DOORS()
				FREEZE_ENTITY_POSITION(NET_TO_VEH(serverBD.niVeh), FALSE)
				SET_ENTITY_DYNAMIC(NET_TO_VEH(serverBD.niVeh), TRUE)
				ACTIVATE_PHYSICS(NET_TO_VEH(serverBD.niVeh))
				SET_VEHICLE_ENGINE_ON(NET_TO_VEH(serverBD.niVeh), TRUE, TRUE)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_VEH(serverBD.niVeh), TRUE)				
				SET_ENTITY_INVINCIBLE(NET_TO_VEH(serverBD.niVeh), TRUE)
				SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(NET_TO_VEH(serverBD.niVeh), FALSE)
				SET_VEHICLE_DOORS_LOCKED_FOR_NON_SCRIPT_PLAYERS(NET_TO_VEH(serverBD.niVeh), TRUE)
				SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.niVeh), TRUE)				
				SET_VEHICLE_AUTOMATICALLY_ATTACHES(NET_TO_VEH(serverBD.niVeh), FALSE)				
				SET_VEHICLE_DISABLE_TOWING(NET_TO_VEH(serverBD.niVeh), TRUE)
				SET_VEHICLE_ON_GROUND_PROPERLY(NET_TO_VEH(serverBD.niVeh))
				SET_VEHICLE_CAN_BE_USED_BY_FLEEING_PEDS(NET_TO_VEH(serverBD.niVeh), FALSE)
				SET_VEHICLE_WILL_NOT_PASS_ON_INSURANCE_DAMAGE_FROM_FIRE(NET_TO_VEH(serverBD.niVeh), TRUE)
				NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_VEH(serverBD.niVeh), TRUE)
				//SET_NETWORK_ID_VISIBLE_IN_CUTSCENE(serverBD.niVeh, TRUE, TRUE)
											
				IF IS_THIS_MODEL_A_HELI(serverBD.VehModel)
					SET_VEHICLE_ENGINE_ON(NET_TO_VEH(serverBD.niVeh), FALSE, TRUE)
					SET_ENTITY_HEALTH(NET_TO_VEH(serverBD.niVeh), g_sMPTunables.iHotTargetHeliHealth)
					SET_VEHICLE_ENGINE_HEALTH(NET_TO_VEH(serverBD.niVeh), TO_FLOAT(g_sMPTunables.iHotTargetHeliHealth))
					SET_VEHICLE_PETROL_TANK_HEALTH(NET_TO_VEH(serverBD.niVeh), TO_FLOAT(g_sMPTunables.iHotTargetHeliHealth))
					SET_VEHICLE_BODY_HEALTH(NET_TO_VEH(serverBD.niVeh), TO_FLOAT(g_sMPTunables.iHotTargetHeliHealth))
					SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(NET_TO_VEH(serverBD.niVeh), FALSE)
					SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(NET_TO_VEH(serverBD.niVeh), TRUE)
					IF (g_sMPTunables.iHotTargetHeliHealth * 0.9) > 900
						OVERRIDE_PLANE_DAMAGE_THREHSOLD(NET_TO_VEH(serverBD.niVeh), (g_sMPTunables.iHotTargetHeliHealth * 0.9))
					ENDIF
					PRINTLN("[AM MOVING TARGET] - SET_ENTITY_HEALTH(",g_sMPTunables.iHotTargetHeliHealth,")")
				ELSE
					FLOAT fHealth = (1000 * g_sMPTunables.fHot_Target_Vehicle_Health_Modifier)
					SET_ENTITY_HEALTH(NET_TO_VEH(serverBD.niVeh), ROUND(fHealth))
					SET_VEHICLE_ENGINE_HEALTH(NET_TO_VEH(serverBD.niVeh),  fHealth)
					SET_VEHICLE_PETROL_TANK_HEALTH(NET_TO_VEH(serverBD.niVeh), fHealth)
					SET_VEHICLE_BODY_HEALTH(NET_TO_VEH(serverBD.niVeh), fHealth)
				ENDIF
				
				IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh", DECOR_TYPE_INT)
                    DECOR_SET_INT(NET_TO_VEH(serverBD.niVeh), "Not_Allow_As_Saved_Veh", MP_NOT_ALLOWED_AS_SAVED_VEHICLE_DEC_SIMEON)
					PRINTLN("[AM MOVING TARGET] - DECORATOR 'Not_Allow_As_Saved_Veh' SET")
  				ENDIF

				//Prevents the vehicle from being used for activities that use passive mode - MJM 
				IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
					INT iDecoratorValue
					IF DECOR_EXIST_ON(NET_TO_VEH(serverBD.niVeh), "MPBitset")
						iDecoratorValue = DECOR_GET_INT(NET_TO_VEH(serverBD.niVeh), "MPBitset")
					ENDIF
					SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_PASSIVE_MODE_VEHICLE)
					SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_MODDABLE_VEHICLE)
					DECOR_SET_INT(NET_TO_VEH(serverBD.niVeh), "MPBitset", iDecoratorValue)
				ENDIF		
				
				CALL_SET_ALLOW_RAMMING_SOOP_OR_RAMP(NET_TO_VEH(serverBD.niVeh))
				
				NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  CREATED VEH - ") NET_PRINT_VECTOR(serverBD.vStartLocation) NET_NL()
			ENDIF
		ENDIF
	ENDIF
	
	//Check we have created the vehicle
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niVeh)
		//NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  FAILING ON VEH CREATION") NET_NL()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

//PURPOSE: Creates the Veh and Driver that will deliver the Vehicle
FUNC BOOL CREATE_VEH_AND_DRIVER()
	IF REQUEST_LOAD_MODEL(serverBD.VehModel)
		IF CREATE_VEH()
			SET_MODEL_AS_NO_LONGER_NEEDED(serverBD.VehModel)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns true if the passed participant has killed the driver
FUNC BOOL HAS_PARTICIPANT_KILLED_DRIVER(INT iParticipant)
	IF serverBD.piDriver != INVALID_PLAYER_INDEX()
		IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piDriver)
			WEAPON_TYPE TempWeapon
			PLAYER_INDEX tempPlayer = NETWORK_GET_KILLER_OF_PLAYER(serverBD.piDriver, TempWeapon)
			IF tempPlayer != INVALID_PLAYER_INDEX()
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
					IF INT_TO_PLAYERINDEX(iParticipant) = tempPlayer			
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns true if the server knows of the driver's killer 
FUNC BOOL HAS_DRIVER_BEEN_KILLED()
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_DriverKilled)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns true if the passed participant has killed the gunner
FUNC BOOL HAS_PARTICIPANT_KILLED_GUNNER(INT iParticipant)
	IF serverBD.piGunner != INVALID_PLAYER_INDEX()
		IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piGunner)
			WEAPON_TYPE TempWeapon
			PLAYER_INDEX tempPlayer = NETWORK_GET_KILLER_OF_PLAYER(serverBD.piGunner, TempWeapon)
			IF tempPlayer != INVALID_PLAYER_INDEX()
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
					IF INT_TO_PLAYERINDEX(iParticipant) = tempPlayer		
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns true if the server knows of the gunner's killer
FUNC BOOL HAS_GUNNER_BEEN_KILLED()
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerKilled)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_DRIVER_OFF_MISSION()
	IF serverBD.piDriver != INVALID_PLAYER_INDEX()
		IF IS_BIT_SET(playerBD[NATIVE_TO_INT(serverBD.piDriver)].iPlayerBitSet, biP_OutOfVehicleFail)
		OR IS_BIT_SET(serverBD.iServerBitSet, biS_DriverKilled)
			RETURN TRUE
		ENDIF
	ENDIF	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_GUNNER_OFF_MISSION()
	IF serverBD.piGunner != INVALID_PLAYER_INDEX()
		IF IS_BIT_SET(playerBD[NATIVE_TO_INT(serverBD.piGunner)].iPlayerBitSet, biP_OutOfVehicleFail)
		OR IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerKilled)
			RETURN TRUE
		ENDIF
	ENDIF	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_HOT_TARGETS_OFF_MISSION()
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_HotTargetsOffMission)
		RETURN TRUE
	ENDIF

	IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
		IF IS_DRIVER_OFF_MISSION()
		AND IS_GUNNER_OFF_MISSION()
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_HotTargetsOffMission)
				SET_BIT(serverBD.iServerBitSet, biS_HotTargetsOffMission)
			ENDIF
			RETURN TRUE
		ENDIF
	ELSE
		IF IS_DRIVER_OFF_MISSION()
		OR IS_BIT_SET(serverBD.iServerBitSet, biS_HotTargetsOffMission)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_HOT_TARGET_UNDRIVEABLE()
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
		VEHICLE_INDEX tempVeh = NET_TO_VEH(serverBD.niVeh)

		IF NOT IS_VEHICLE_DRIVEABLE(tempVeh)
		OR (IS_ENTITY_DEAD(tempVeh)
		AND IS_ENTITY_IN_WATER(tempVeh))
			RETURN TRUE
		ENDIF		
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Runs through the max num participents and does relevant checks
PROC MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
//	INT iParticipant
//	
//	//Initialise Staggered Loop
//	IF iStaggeredParticipant = 0
//		//bDoEndStaggeredLoopChecks = FALSE
//		//NET_PRINT_TIME() NET_PRINT(" ---> HOT TARGET -  MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - iStaggeredParticipant RESET") NET_NL()
//	ENDIF
	
	SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
	INT iTempPeakParts, iTempPeakQualifiers
	//For now we only need to do this check once the Veh is destroyed.
	//IF NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.niVeh))	//IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh)
		IF serverBD.iServerGameState != GAME_STATE_END
			REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iStaggeredParticipant
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iStaggeredParticipant))
					PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iStaggeredParticipant))
					//PED_INDEX PlayerPedId = GET_PLAYER_PED(PlayerId)
					IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_ParticipantViaDistance)
					OR FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_FM_EVENT(PlayerId)
						iTempPeakQualifiers++
					ENDIF
					iTempPeakParts++
					//** CHECKS THAT DON'T NEED THE PLAYER TO BE ALIVE **// -------------------------------------------------------------------
										
					//DELIVERY CHECK
					IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_DeliveredVeh)
						IF serverBD.eStage != eAD_END
							SET_BIT(serverBD.iServerBitSet, biS_VehicleDelivered)
							PRINTLN("[AM MOVING TARGET] - ", GET_PLAYER_NAME(PlayerId), " DELIVERED THE VEH")
						ENDIF
						
						EXIT
						
					//KILL CHECKS
					ELSE
						IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_MinPlayersBailServer)
							IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_MinPlayersBailClient)
								SET_BIT(serverBD.iServerBitSet, biS_MinPlayersBailServer)
								PRINTLN("[AM MOVING TARGET] - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - Client bit biP_MinPlayersBailClient is SET, setting server bit biS_MinPlayersBailServer")
								PRINTLN("[AM MOVING TARGET] - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - Not enough players.")
							ENDIF
						ENDIF
					
						IF HAVE_PLAYERS_ENTERED_TO_START_EVENT()
						
							IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_HotTargetsOffMission)
								IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_ShouldEndPlayersAbandoned)
									PRINTLN("[AM MOVING TARGET] - ", GET_PLAYER_NAME(PlayerId), " reporting that driver and gunner have abandoned/left. Event should end.")
									SET_BIT(serverBD.iServerBitSet, biS_HotTargetsOffMission)
									IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_ExplodeImmediately)
										SET_BIT(serverBD.iServerBitSet, biS_ExplodeImmediately)
									ENDIF
								ENDIF
							ENDIF
						
							IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_DriverLeft)
								IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_IHaveLeft)
									IF IS_PLAYER_THE_DRIVER(PlayerId)
										SET_BIT(serverBD.iServerBitSet, biS_DriverLeft)
										serverBD.piDriver = INVALID_PLAYER_INDEX()
										IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
											CLEAR_BIT(serverBD.iServerBitSet, biS_GunnerVehicle)
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF HAS_GUNNER_BEEN_KILLED()
									SET_BIT(serverBD.iServerBitSet, biS_HotTargetsKilled)
								ENDIF
							ENDIF
						
							//Only check if event is still running							
							IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_EventEnded)
								// Has the driver been killed
								IF serverBD.piDriver != INVALID_PLAYER_INDEX()
									IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(serverBD.piDriver)].iPlayerBitSet, biP_OutOfVehicleFail)
										IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_DriverKilled)
											IF HAS_PARTICIPANT_KILLED_DRIVER(iStaggeredParticipant)
												// If the killer is not the driver (i.e. suicide)
												IF (NATIVE_TO_INT(serverBD.piDriver)) != iStaggeredParticipant
													serverBD.piDriverKiller = PlayerId
													//Send Ticker
													TickerEventData.TickerEvent = TICKER_EVENT_HT_KILLED_BY_PLAYER
													TickerEventData.playerID = serverBD.piDriverKiller								
													TickerEventData.playerID2 = serverBD.piDriver
													BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT_EXCEPT_THIS_PLAYER(serverBD.piDriver))
												ENDIF
												SET_BIT(serverBD.iServerBitSet, biS_DriverKilled)
												PRINTLN("[AM MOVING TARGET] - ", GET_PLAYER_NAME(PlayerId), " KILLED THE DRIVER")
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							
												
								// Has the gunner been killed
								IF serverBD.piGunner != INVALID_PLAYER_INDEX()
									IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(serverBD.piGunner)].iPlayerBitSet, biP_OutOfVehicleFail)
										IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerKilled)
											IF HAS_PARTICIPANT_KILLED_GUNNER(iStaggeredParticipant)
												// If the killer is not the gunner (i.e. suicide)
												IF (NATIVE_TO_INT(serverBD.piGunner)) != iStaggeredParticipant
													serverBD.piGunnerKiller = PlayerId
													//Send Ticker
													TickerEventData.TickerEvent = TICKER_EVENT_HT_KILLED_BY_PLAYER
													TickerEventData.playerID = serverBD.piGunnerKiller								
													TickerEventData.playerID2 = serverBD.piGunner
													BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT_EXCEPT_THIS_PLAYER(serverBD.piGunner))
												ENDIF
												SET_BIT(serverBD.iServerBitSet, biS_GunnerKilled)
												PRINTLN("[AM MOVING TARGET] - ", GET_PLAYER_NAME(PlayerId), " KILLED THE GUNNER")
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
								IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_OutOfVehicleFail)
									CLEAR_BIT(serverBD.iServerBitSet, biS_GunnerVehicle)
								ENDIF
							ENDIF
							
							IF HAS_DRIVER_BEEN_KILLED()
								IF HAS_GUNNER_BEEN_KILLED()
								OR NOT IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
									SET_BIT(serverBD.iServerBitSet, biS_HotTargetsKilled)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					//** CHECKS THAT REQUIRE THE PLAYER TO BE ALIVE **// ---------------------------------------------------------------------------------
					IF IS_NET_PLAYER_OK(PlayerId)
						IF serverBD.piDriver = INVALID_PLAYER_INDEX()
							IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_Driver)
								IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_NeedToWaitGunner)	// If the driver is waiting for a gunner
									SET_BIT(serverBD.iServerBitSet, biS_WaitingForGunner)							// Server knows to wait for gunner
								ENDIF
								serverBD.piDriver = PlayerId
								PRINTLN("[AM MOVING TARGET] - serverBD.piDriver = ", GET_PLAYER_NAME(PlayerId))
							ENDIF
						ELIF serverBD.piDriver = PlayerId
						AND serverBD.eStage = eAD_IDLE
							IF NOT IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_Driver)
								IF NOT IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_NeedToWaitGunner)	// If no longer the driver and was waiting for gunner
									CLEAR_BIT(serverBD.iServerBitSet, biS_WaitingForGunner)								// Server knows to not wait for gunner
								ENDIF
								serverBD.piDriver = INVALID_PLAYER_INDEX()
								PRINTLN("[AM MOVING TARGET] - serverBD.piDriver = INVALID_PLAYER_INDEX")
							ELSE
								// Player is the driver and no longer needs to wait for gunner
								IF NOT IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_NeedToWaitGunner)
								AND (iWaitForOtherOccupantTime - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(WaitForOtherTimer)) <= 0
									CLEAR_BIT(serverBD.iServerBitSet, biS_WaitingForGunner)
									SET_BIT(serverBD.iServerBitSet, biS_StartedWithoutGunner)
									CLEAR_BIT(serverBD.iServerBitSet, biS_GunnerVehicle)
								ENDIF
							ENDIF
						ENDIF
											
						IF serverBD.piGunner = INVALID_PLAYER_INDEX()
							IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_Gunner)
								serverBD.piGunner = PlayerId
								PRINTLN("[AM MOVING TARGET] - serverBD.piGunner = ", GET_PLAYER_NAME(PlayerId))
								IF serverBD.piDriver != INVALID_PLAYER_INDEX()
									IF IS_BIT_SET(serverBD.iServerBitSet, biS_WaitingForGunner)		// If there is now a gunner as well as a driver
										CLEAR_BIT(serverBD.iServerBitSet, biS_WaitingForGunner)		// No longer waiting for a gunner
									ENDIF
								ENDIF
							ENDIF
						ELIF serverBD.piGunner = PlayerId
						AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_EventStarted)
							IF NOT IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_Gunner)
								serverBD.piGunner = INVALID_PLAYER_INDEX()
								PRINTLN("[AM MOVING TARGET] - serverBD.piGunner = INVALID_PLAYER_INDEX")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
		IF iTempPeakParts > serverBD.iPeakParticipants
			PRINTLN("[HOT_TARGET] - [PARTICIPATION] - iTempPeakParts (",iTempPeakParts,") is GREATER than peak participant count so far (",serverBD.iPeakParticipants,"), updating value.")
			serverBD.iPeakParticipants = iTempPeakParts
		ELSE
			INT iTemp = serverBD.iPeakParticipants  - iTempPeakParts
			IF serverBD.iPlayersLeftInProgress < iTemp OR serverBD.iPlayersLeftInProgress > iTemp
				serverBD.iPlayersLeftInProgress = iTemp
			ENDIF
		ENDIF
		
		IF iTempPeakQualifiers > serverBD.iPeakQualifiers
			PRINTLN("[HOT_TARGET] - [PARTICIPATION] - iTempPeakQualifiers (",iTempPeakQualifiers,") is GREATER than peak participant count so far (",serverBD.iPeakQualifiers,"), updating value.")
			serverBD.iPeakQualifiers = iTempPeakQualifiers
		ENDIF
		
	//ENDIF
	
//	iStaggeredParticipant++
//	
//	//Reset Staggered Loop
//	IF iStaggeredParticipant = NETWORK_GET_MAX_NUM_PARTICIPANTS()
//		iStaggeredParticipant = 0
//		//bDoEndStaggeredLoopChecks = TRUE
//		//NET_PRINT_TIME() NET_PRINT(" ---> HOT TARGET -  MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - iStaggeredParticipant RESET") NET_NL()
//	ENDIF
ENDPROC

/// PURPOSE:
///    Maintains the timer that displays informing the players 
///    of the available time remaining to deliver the package
PROC MAINTAIN_HOT_TARGET_EVENT_TIMER()
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_StartTimer)
	AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_VehicleDelivered)
	AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_MissionEndTimerHit)
	AND NOT IS_HOT_TARGET_UNDRIVEABLE()
	AND NOT HAS_NET_TIMER_STARTED(serverBD.ExplodeVehTimer)
		IF NOT HAVE_HOT_TARGETS_BEEN_KILLED()
			IF HAS_NET_TIMER_STARTED(serverBD.DeliverVehicleTimer)
				iTimeRemaining = (GET_MISSION_TIME() - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.DeliverVehicleTimer))
				// Start the timer
				HUD_COLOURS timeColour
				IF NOT SHOULD_UI_BE_HIDDEN()
					HANDLE_AMBIENT_EVENT_COUNTDOWN_AUDIO(iTimeRemaining)
				ENDIF
				IF iTimeRemaining > 30000
					timeColour = HUD_COLOUR_WHITE
				ELSE
					timeColour = HUD_COLOUR_RED
				ENDIF
										
				IF iTimeRemaining > 0				
					IF NOT SHOULD_UI_BE_HIDDEN()
						BOTTOM_RIGHT_UI_EVENT_TIMER(iTimeRemaining, timeColour, GET_COUNTDOWN_TIMER_NAME_FOR_EVENT())	
					ENDIF
				ELSE
					IF NOT SHOULD_UI_BE_HIDDEN()
						// Time's up					
						DRAW_GENERIC_TIMER(0, "HTV_EVENTEND_R", 0, TIMER_STYLE_USEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_DONTCARE, DEFAULT, HUD_COLOUR_RED)
						SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
					ENDIF
					// TIME HAS EXPIRED, EVENT NO LONGER AVAILABLE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if the gunner seat of the vehicle is free
/// RETURNS:
///    TRUE if the seat is free
FUNC BOOL IS_GUNNER_SEAT_FREE()	
	IF serverBD.VehModel = INSURGENT
		IF IS_VEHICLE_SEAT_FREE(NET_TO_VEH(serverBD.niVeh), VS_EXTRA_LEFT_3)
			RETURN TRUE
		ENDIF
	ELIF serverBD.VehModel = TECHNICAL
		IF IS_VEHICLE_SEAT_FREE(NET_TO_VEH(serverBD.niVeh), VS_BACK_LEFT)
			RETURN TRUE
		ENDIF
	ELIF serverBD.VehModel = VALKYRIE
		IF IS_VEHICLE_SEAT_FREE(NET_TO_VEH(serverBD.niVeh), VS_FRONT_RIGHT)
		OR IS_VEHICLE_SEAT_FREE(NET_TO_VEH(serverBD.niVeh), VS_BACK_RIGHT)
		OR IS_VEHICLE_SEAT_FREE(NET_TO_VEH(serverBD.niVeh), VS_EXTRA_LEFT_1)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE	
ENDFUNC

FUNC BOOL GET_SEAT_PED_IS_IN_FOR_HOT_TARGET(PED_INDEX PedID, VEHICLE_SEAT &eSeat, BOOL bConsiderEnteringAsInVehicle = FALSE)
	VEHICLE_INDEX CarID
	INT iNoOfSeats
	INT iSeat
	VEHICLE_SEAT eSeatTemp
	IF NOT IS_PED_INJURED(PedID)
		IF IS_PED_IN_ANY_VEHICLE(PedID, bConsiderEnteringAsInVehicle)
			CarID = GET_VEHICLE_PED_IS_IN(PedID, bConsiderEnteringAsInVehicle)	
			IF DOES_ENTITY_EXIST(CarID)
				IF NOT IS_ENTITY_DEAD(CarID)
					iNoOfSeats = GET_VEHICLE_MODEL_NUMBER_OF_SEATS(GET_ENTITY_MODEL(CarID))
					REPEAT iNoOfSeats iSeat
						eSeatTemp = INT_TO_ENUM(VEHICLE_SEAT, iSeat-1)
						IF NOT IS_VEHICLE_SEAT_FREE(CarID, eSeatTemp )
							IF (GET_PED_IN_VEHICLE_SEAT(CarID, eSeatTemp) = PedID)
								eSeat = eSeatTemp
								RETURN TRUE
							ENDIF
						ENDIF
					ENDREPEAT
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the passed ped is in a gunner position in the vehicle
/// PARAMS:
///    PlayerPedId - The ped to check if in the vehicle seat
/// RETURNS:
///    TRUE: if ped is in a gunner seat
FUNC BOOL IS_PED_IN_VEHICLE_GUNNER_POSITION(PED_INDEX PlayerPedId)
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
		IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
			VEHICLE_SEAT eSeat
			IF GET_SEAT_PED_IS_IN_FOR_HOT_TARGET(PlayerPedId, eSeat, TRUE)
				IF IS_TURRET_SEAT(NET_TO_VEH(serverBD.niVeh), eSeat)
					RETURN TRUE
				ENDIF
				
				IF serverBD.VehModel = VALKYRIE
					IF IS_PED_IN_VEHICLE(PlayerPedId, NET_TO_VEH(serverBD.niVeh))
						IF eSeat = VS_FRONT_RIGHT
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANY_PLAYER_ENTERING_THE_VEHICLE()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
		IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh))
			INT i	
			REPEAT NUM_NETWORK_PLAYERS i
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, i))
					PLAYER_INDEX player = INT_TO_NATIVE(PLAYER_INDEX, i) 				
					// Dont check local
					IF player != PLAYER_ID()
						PED_INDEX ped = GET_PLAYER_PED(player)
						IF NOT IS_ENTITY_DEAD(ped)
							IF IS_PED_IN_THIS_VEHICLE(ped, NET_TO_VEH(serverBD.niVeh), TRUE)
								RETURN TRUE					
							ENDIF
						ENDIF
					ENDIF
				ENDIF		
			ENDREPEAT
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Draws markers based on vehicle model
PROC DRAW_MARKER_FOR_HOT_TARGET_VEHICLE(BOOL bBeforeStart = FALSE)
	IF SHOULD_UI_BE_HIDDEN()
		EXIT
	ENDIF
	INT	r, g, b, a
	
	IF bBeforeStart
		GET_HUD_COLOUR(HUD_COLOUR_GREEN, r, g, b, a)
	ELSE
	
		IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
		
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
				IF serverBD.piDriver != INVALID_PLAYER_INDEX()
				AND serverBD.piGunner != INVALID_PLAYER_INDEX()
					IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(serverBD.piDriver, GB_GET_LOCAL_PLAYER_GANG_BOSS())
					AND GB_IS_PLAYER_MEMBER_OF_THIS_GANG(serverBD.piGunner, GB_GET_LOCAL_PLAYER_GANG_BOSS())
						// Both players are in my gang
						GET_HUD_COLOUR(GET_PLAYER_HUD_COLOUR(serverBD.piDriver), r, g, b, a)
					ELIF GB_GET_LOCAL_PLAYER_GANG_BOSS() = serverBD.piDriver
					AND NOT GB_IS_PLAYER_MEMBER_OF_THIS_GANG(serverBD.piGunner, GB_GET_LOCAL_PLAYER_GANG_BOSS())
						// Driver is my boss
						GET_HUD_COLOUR(GET_PLAYER_HUD_COLOUR(serverBD.piDriver), r, g, b, a)
					ELIF GB_GET_LOCAL_PLAYER_GANG_BOSS() = serverBD.piGunner
					AND NOT GB_IS_PLAYER_MEMBER_OF_THIS_GANG(serverBD.piDriver, GB_GET_LOCAL_PLAYER_GANG_BOSS())
						// Gunner is my boss
						GET_HUD_COLOUR(GET_PLAYER_HUD_COLOUR(serverBD.piGunner), r, g, b, a)
					ELSE
						// Kill them
						GET_HUD_COLOUR(HUD_COLOUR_RED, r, g, b, a)
					ENDIF					
				ENDIF
			ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_DriverLeft)
				IF serverBD.piGunner != INVALID_PLAYER_INDEX()
					IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(serverBD.piGunner, GB_GET_LOCAL_PLAYER_GANG_BOSS())
						// Gunner is in my gang
						GET_HUD_COLOUR(GET_PLAYER_HUD_COLOUR(serverBD.piGunner), r, g, b, a)
					ELSE
						// Kill them
						GET_HUD_COLOUR(HUD_COLOUR_RED, r, g, b, a)
					ENDIF
				ENDIF
			ELSE
				IF serverBD.piDriver != INVALID_PLAYER_INDEX()
					IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(serverBD.piDriver, GB_GET_LOCAL_PLAYER_GANG_BOSS())
						// Driver is in my gang
						GET_HUD_COLOUR(GET_PLAYER_HUD_COLOUR(serverBD.piDriver), r, g, b, a)
					ELSE
						// Kill them
						GET_HUD_COLOUR(HUD_COLOUR_RED, r, g, b, a)
					ENDIF
				ENDIF
			
			ENDIF
		
		ELSE
			GET_HUD_COLOUR(HUD_COLOUR_RED, r, g, b, a)
		ENDIF
	
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
		FM_EVENTDRAW_MARKER_ABOVE_VEHICLE(NET_TO_VEH(serverBD.niVeh), r, g, b)
	ENDIF
ENDPROC


/// PURPOSE:
///    Draws a marker above the hot target vehicle if someone is in the driver seat and its not you
PROC MAINTAIN_VEHICLE_MARKER()
	IF SHOULD_UI_BE_HIDDEN()
		EXIT
	ENDIF
	//If we ahve a vehicle
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
		IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.niVeh))
		AND NOT IS_PED_INJURED(PLAYER_PED_ID())
			//If we're close to the vehcile
			VECTOR vTargetCoords = GET_ENTITY_COORDS(NET_TO_ENT(serverBD.niVeh))
			IF AM_I_PARTICIPATING_IN_EVENT(TRUE)
				IF HAVE_PLAYERS_ENTERED_TO_START_EVENT()
					IF NOT AM_I_A_HOT_TARGET()
						IF NOT IS_PLAYER_SCTV(PLAYER_ID())
							IF IS_NET_PLAYER_OK(PLAYER_ID())
								IF NOT IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh))
									DRAW_MARKER_FOR_HOT_TARGET_VEHICLE()
								ENDIF
							ENDIF
							IF NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(),MPAM_TYPE_TIME_TRIAL)
							AND NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_RC_TIME_TRIAL)
							AND NOT IS_PLAYER_IN_CORONA()
							AND NOT IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), FALSE)
							AND NOT IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
								//Set up the span locations
								bSetSpawnLocations = TRUE
								SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_NEAR_SPECIFIC_COORDS)
								SETUP_SPECIFIC_SPAWN_LOCATION(vTargetCoords, 0.0, 250.0, TRUE, FMEVENT_MINIMUM_SPAWN_DISTANCE_FROM_ACTION, FALSE, TRUE, 65, TRUE, TRUE)
							ELSE
								PRINTLN("     ---------->     [AM MOVING TARGET] - CLEAR_PLAYER_NEXT_RESPAWN_LOCATION - Player is on time trial  <----------     ")
								CLEAR_PLAYER_NEXT_RESPAWN_LOCATION()
								CLEAR_SPECIFIC_SPAWN_LOCATION_FROM_THIS_SCRIPT()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF NOT AM_I_A_HOT_TARGET()
					IF bSetSpawnLocations
						//Clear the spawn locations
						PRINTLN("     ---------->     [AM MOVING TARGET] - CLEAR_PLAYER_NEXT_RESPAWN_LOCATION   <----------     ")
						CLEAR_PLAYER_NEXT_RESPAWN_LOCATION()
						CLEAR_SPECIFIC_SPAWN_LOCATION_FROM_THIS_SCRIPT()
						bSetSpawnLocations = FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Runs the abandon vehicle timer for 2 person vehicle
PROC RUN_ABANDON_TIMER()
	//DO_HELP_TEXT(3)
	INT iTime, r, g, b, a
	IF NOT HAS_NET_TIMER_STARTED(OutOfVehTimer)
		IF NOT SHOULD_UI_BE_HIDDEN()
			ADD_VEH_BLIP(FALSE)
			IF serverBD.iVehicleType != VT_BIKE
				Print_Objective_Text_With_String("HTV_HELP3", GET_VEHICLE_TYPE_STRING())	//Get back into the ~b~Hot Target~s~ ~a~.
			ELSE
				Print_Objective_Text_With_String("HTV_HELP3B", GET_VEHICLE_TYPE_STRING())
			ENDIF
		ENDIF
		REINIT_NET_TIMER(OutOfVehTimer)
		PRINTLN("[AM MOVING TARGET] - START OUT OF VEH TIMER   <----------     ")
	ELSE
		iTime = (g_sMPTunables.iGetBackInVehicleTime-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(OutOfVehTimer))
		IF iTime >= 0
			IF NOT SHOULD_UI_BE_HIDDEN()
				DRAW_GENERIC_TIMER(iTime, "HTV_ABAN", 0, TIMER_STYLE_DONTUSEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_DONTCARE, DEFAULT, HUD_COLOUR_RED)
				SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
			ENDIF
		ELSE
			IF NOT SHOULD_UI_BE_HIDDEN()
				DRAW_GENERIC_TIMER(0, "HTV_ABAN", 0, TIMER_STYLE_DONTUSEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_DONTCARE, DEFAULT, HUD_COLOUR_RED)
				SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
			ENDIF
			
			//Send Ticker
			SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
			TickerEventData.playerID = PLAYER_ID()
			TickerEventData.TickerEvent = TICKER_EVENT_HT_ABANDONED
			TickerEventData.dataString = GET_VEHICLE_TYPE_STRING()
			BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS())
			
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_DriverLeft)
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ShouldEndPlayersAbandoned)
			ELSE
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_OutOfVehicleFail)
			ENDIF
			NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  biP_OutOfVehicleFail SET    <----------     ") NET_NL()
			IF PLAYER_ID() = serverBD.piDriver
				serverBD.piDriver = INVALID_PLAYER_INDEX()
			ELIF PLAYER_ID() = serverBD.piGunner
				serverBD.piGunner = INVALID_PLAYER_INDEX()
			ENDIF
			Clear_Any_Objective_Text_From_This_Script()
			playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_OFF_MISSION
			FM_EVENT_SET_PLAYER_IS_CRITICAL_TO_FM_EVENT(FALSE)
			BLOCK_BLIPS(FALSE)								
			NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  PLAYER STAGE = eAD_OFF_MISSION - OUT OF VEH    <----------     ") NET_NL()
		ENDIF
	ENDIF
	GET_HUD_COLOUR(HUD_COLOUR_BLUE, r, g, b, a)
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
		IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh))
			FM_EVENTDRAW_MARKER_ABOVE_VEHICLE(NET_TO_VEH(serverBD.niVeh), r, g, b)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_DRIVER_ABANDONED_EVENT()
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_DriverLeft)
		RETURN TRUE
	ENDIF

	IF serverBD.piDriver != INVALID_PLAYER_INDEX()
		IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piDriver)
			IF IS_BIT_SET(playerBD[NATIVE_TO_INT(serverBD.piDriver)].iPlayerBitSet, biP_OutOfVehicleFail)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Runs the explosion timer if the player falls off or gets out during the event.
PROC RUN_EXPLOSION_TIMER()
	INT time, r, g, b, a
	//DO_HELP_TEXT(3)
	IF NOT HAS_NET_TIMER_STARTED(OutOfVehTimer)
	
		IF NOT SHOULD_UI_BE_HIDDEN()
			IF IS_THIS_MODEL_A_BIKE(serverBD.VehModel)
				Print_Objective_Text("HTV_GET_ON")
			ELSE
				Print_Objective_Text("HTV_GET_IN")
			ENDIF
		ENDIF
	
		START_NET_TIMER(OutOfVehTimer)
		IF bStartSound = FALSE
			iExplosionSound = GET_SOUND_ID()
			PLAY_SOUND_FROM_ENTITY(iExplosionSound,"Explosion_Countdown",NET_TO_VEH(serverBD.niVeh),"GTAO_FM_Events_Soundset")
			SET_VARIABLE_ON_SOUND(iExplosionSound,"Time",TO_FLOAT(g_sMPTunables.iGetBackInVehicleTime/1000))
			bStartSound = TRUE
		ENDIF
		PRINTLN("[AM MOVING TARGET] - START OUT OF VEH TIMER   <----------     ")
	ELSE
		IF HAS_DRIVER_ABANDONED_EVENT()
			DO_HELP_TEXT(13)
		ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_DriverKilled)
			DO_HELP_TEXT(14)
		ENDIF
		SET_MIDDLE_TITLE_POSITION_HUD_THIS_FRAME()
		time = (g_sMPTunables.iGetBackInVehicleTime-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(OutOfVehTimer))
		
		IF time >= 0
			IF NOT SHOULD_UI_BE_HIDDEN()
				DRAW_GENERIC_TIMER(time, "HTV_DESTR", 0, TIMER_STYLE_DONTUSEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_DONTCARE, DEFAULT, HUD_COLOUR_RED)
				SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
			ENDIF
		ELSE
			IF NOT SHOULD_UI_BE_HIDDEN()
				DRAW_GENERIC_TIMER(0, "HTV_DESTR", 0, TIMER_STYLE_DONTUSEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_DONTCARE, DEFAULT, HUD_COLOUR_RED)
				SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
			ENDIF
			
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ShouldEndPlayersAbandoned)
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ExplodeImmediately)
			
			NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  biP_OutOfVehicleFail SET    <----------     ") NET_NL()
						
			Clear_Any_Objective_Text_From_This_Script()
			playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_OFF_MISSION
			FM_EVENT_SET_PLAYER_IS_CRITICAL_TO_FM_EVENT(FALSE)
			BLOCK_BLIPS(FALSE)								
			NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  PLAYER STAGE = eAD_OFF_MISSION - OUT OF VEH    <----------     ") NET_NL()
		ENDIF
	ENDIF
	GET_HUD_COLOUR(HUD_COLOUR_BLUE, r, g, b, a)
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
		IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh))
			FM_EVENTDRAW_MARKER_ABOVE_VEHICLE(NET_TO_VEH(serverBD.niVeh), r, g, b)
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Checks for the Veh (destroyed, stuck, etc)
PROC CONTROL_VEH_BODY()
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niVeh)

		IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh))	
								
			// If the vehicle is empty, check if I enter it
			IF serverBD.piDriver = INVALID_PLAYER_INDEX()
				
				IF bHaveBeenInVeh = TRUE
					bHaveBeenInVeh = FALSE
				ENDIF
				
				// Help text when player gets close to Vehicle
				IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh))
				AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					IF IS_ENTITY_AT_ENTITY(NET_TO_VEH(serverBD.niVeh),PLAYER_PED_ID(),<<VEH_CLOSE_RANGE,VEH_CLOSE_RANGE,VEH_CLOSE_RANGE>>, FALSE )
						DO_HELP_TEXT(9) 
					ENDIF
				ENDIF
				
				//Driver
				IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh), TRUE)
					IF GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(serverBD.niVeh), VS_DRIVER) = PLAYER_PED_ID()
						PRINTLN("[AM MOVING TARGET] - biP_Driver SET")
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Driver)
						SET_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iFmAmbientEventBitSet, ciSET_PLAYER_IN_MOVING_TARGET_VEHICLE)
						
						IF serverBD.piGunner = INVALID_PLAYER_INDEX()
						AND IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
							IF NOT SHOULD_UI_BE_HIDDEN()
								IF IS_GUNNER_SEAT_FREE()
									Print_Objective_Text("HTV_GWAITG")	//Wait for a gunner.
									DO_HELP_TEXT(6)
								ENDIF
							ENDIF
							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_NeedToWaitGunner)						
						ENDIF
					ENDIF
				ENDIF
				
				//Gunner
				IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
					IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh))
						IF IS_PED_IN_VEHICLE_GUNNER_POSITION(PLAYER_PED_ID())
							IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Gunner)
								PRINTLN("[AM MOVING TARGET] - biP_Gunner SET")
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Gunner)
								SET_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iFmAmbientEventBitSet, ciSET_PLAYER_IN_MOVING_TARGET_VEHICLE)
								IF serverBD.piDriver = INVALID_PLAYER_INDEX()
								AND NOT SHOULD_UI_BE_HIDDEN()
									IF IS_THIS_MODEL_A_HELI(serverBD.VehModel)
										Print_Objective_Text("HTV_GWAITD2")
									ELSE								
										Print_Objective_Text("HTV_GWAITD")	//Wait for a driver.
									ENDIF
									DO_HELP_TEXT(7)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF Is_There_Any_Current_Objective_Text_From_This_Script()
							Clear_Any_Objective_Text_From_This_Script()
						ENDIF
						
						IF IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iFmAmbientEventBitSet, ciSET_PLAYER_IN_MOVING_TARGET_VEHICLE)
							CLEAR_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iFmAmbientEventBitSet, ciSET_PLAYER_IN_MOVING_TARGET_VEHICLE)
						ENDIF
						
						IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Gunner)
							CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Gunner)
						ENDIF
					ENDIF
				ENDIF	
			// Vehicle has someone in it
			ELSE
				// If the driver should wait for a gunner
				IF IS_BIT_SET(serverBD.iServerBitSet, biS_WaitingForGunner)
					IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh), TRUE)
					
						// Per-frame checks
						// Prevent the player from driving the vehicle
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_UP)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_DOWN)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_YAW_LEFT)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_YAW_RIGHT)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ROLL_LR)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_PITCH_UD)
												
						// Start the timer
						IF NOT HAS_NET_TIMER_STARTED(WaitForOtherTimer)
							REINIT_NET_TIMER(WaitForOtherTimer)
							PRINTLN("[AM MOVING TARGET] - START WAIT FOR OTHER OCCUPANT TIMER   <----------     ")
						ELSE						
							INT iTimeLeft = (g_sMPTunables.iHot_Target_Player_Wait_Timer_2_player_vehicles - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(WaitForOtherTimer))
							HANDLE_AMBIENT_EVENT_START_COUNTDOWN_AUDIO(iTimeLeft)
							// While timer is above 0
							IF iTimeLeft >= 10
								IF NOT SHOULD_UI_BE_HIDDEN()
									DRAW_GENERIC_TIMER(iTimeLeft, GET_START_TIMER_NAME_FOR_EVENT(), 0, TIMER_STYLE_DONTUSEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_DONTCARE, DEFAULT, HUD_COLOUR_WHITE)
									SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
								ENDIF
																
								// If I get in the gunner seat while there's a driver waiting for a gunner
								IF IS_PED_IN_VEHICLE_GUNNER_POSITION(PLAYER_PED_ID())
									SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Gunner)
								ENDIF
								
								IF iTimeLeft <= 3
								OR IS_ANY_PLAYER_ENTERING_THE_VEHICLE()
									DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
								ENDIF
							ELSE
								IF NOT SHOULD_UI_BE_HIDDEN()
									// Time's up					
									DRAW_GENERIC_TIMER(0, GET_START_TIMER_NAME_FOR_EVENT(), 0, TIMER_STYLE_DONTUSEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_DONTCARE, DEFAULT)
									SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
								ENDIF	
								
								// No longer wait for other player, start the event.
								CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_NeedToWaitGunner)
								NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  biS_WaitingForGunner CLEARED as timer expired    <----------     ") NET_NL()
								
								Clear_Any_Objective_Text_From_This_Script()
							ENDIF
						ENDIF
					ELSE
						// If I'm the person who is waiting
						IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_NeedToWaitGunner)
							IF DOES_BLIP_EXIST(DeliveryBlip)
								REMOVE_BLIP(DeliveryBlip)
								DELETE_CHECKPOINT(DeliveryCP)					
							ENDIF	
							
							// Player has left the vehicle while waiting for gunner, player is no longer driver
							IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Driver)
								CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Driver)
							ENDIF
												
							Clear_Any_Objective_Text()
														
							// Reset the timer
							IF HAS_NET_TIMER_STARTED(WaitForOtherTimer)
								RESET_NET_TIMER(WaitForOtherTimer)
								PRINTLN("[AM MOVING TARGET] - HELP TEXT - PLAYER LEFT VEHICLE WHILST WAITING FOR OTHER OCCUPANT    <----------     ")
							ENDIF
							
							// No longer waiting for another occupant
							CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_NeedToWaitGunner)
							
							bHaveBeenInVeh = TRUE
							
							CLEAR_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iFmAmbientEventBitSet, ciSET_PLAYER_IN_MOVING_TARGET_VEHICLE)
						ELSE
							IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Driver)
								CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Driver)
							ENDIF
							
							IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Gunner)
								CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Gunner)
							ENDIF
						
							// I'm not waiting
							IF NOT bHaveBeenInVeh
								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh), FALSE) < 20
								AND NOT HAVE_PLAYERS_ENTERED_TO_START_EVENT()
								AND NOT FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID())
									DO_HELP_TEXT(12)
								ENDIF
							ENDIF
						ENDIF						
					ENDIF
				// We're not waiting for a gunner
				ELSE
					//Exit before start
					IF playerBD[PARTICIPANT_ID_TO_INT()].eStage	= eAD_IDLE
						IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
							//Driver
							IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Driver)
								IF GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(serverBD.niVeh), VS_DRIVER) != PLAYER_PED_ID()
									Clear_Any_Objective_Text_From_This_Script()
									CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Driver)
									PRINTLN("[AM MOVING TARGET] - biP_Driver CLEARED")
								ENDIF
							ENDIF
							//Gunner
							IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Gunner)
								IF NOT IS_PED_IN_VEHICLE_GUNNER_POSITION(PLAYER_PED_ID())
									Clear_Any_Objective_Text_From_This_Script()
									CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Gunner)
									PRINTLN("[AM MOVING TARGET] - biP_Gunner CLEARED")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					// Update if player enters vehicle seat
					IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
						IF NOT IS_VEHICLE_SEAT_FREE(NET_TO_VEH(serverBD.niVeh), VS_DRIVER)	// Both seats have a player in						
							//Driver
							IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Driver)
								IF GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(serverBD.niVeh), VS_DRIVER) = PLAYER_PED_ID()
									//Clear_Any_Objective_Text_From_This_Script()
									SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Driver)
									IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Gunner)
										CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Gunner)
									ENDIF
									PRINTLN("[AM MOVING TARGET] - biP_Driver SET")
								ENDIF
							ENDIF
						ENDIF
						IF NOT IS_GUNNER_SEAT_FREE()
							//Gunner
							IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Gunner)
								IF IS_PED_IN_VEHICLE_GUNNER_POSITION(PLAYER_PED_ID())
									//Clear_Any_Objective_Text_From_This_Script()
									SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Gunner)
									IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Driver)
										CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Driver)
									ENDIF
									PRINTLN("[AM MOVING TARGET] - biP_Gunner SET")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					//Delivered Vehicle
					IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DeliveredVeh)
						IF GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(serverBD.niVeh), VS_DRIVER) = PLAYER_PED_ID()
						OR IS_PED_IN_VEHICLE_GUNNER_POSITION(PLAYER_PED_ID())
							IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), serverBD.vDeliveryLocation, serverBD.vDeliveryDimensions, TRUE)
//								PLAY_SOUND_FRONTEND(-1, "Checkpoint_Hit", "GTAO_FM_Events_Soundset", FALSE) // Removed for url:bugstar:2454878.
								BLOCK_BLIPS(FALSE)								
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DeliveredVeh)
								PRINTLN("[AM MOVING TARGET] - biP_DeliveredVeh SET")
							ENDIF
						ENDIF
					ENDIF
					
					//Get Back in Vehicle
					IF IS_NET_PLAYER_OK(PLAYER_ID())
					AND playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_DELIVER
					AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DeliveredVeh)
						IF NOT IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh), TRUE)
							IF NOT IS_PLAYER_SCTV(PLAYER_ID())
								IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
									RUN_ABANDON_TIMER()
								ELSE
									RUN_EXPLOSION_TIMER()
								ENDIF
							ENDIF
						ELSE
							IF HAS_NET_TIMER_STARTED(OutOfVehTimer)
								ADD_DELIVERY_BLIP()
								RESET_NET_TIMER(OutOfVehTimer)
								IF Is_This_The_Current_Objective_Text("HTV_GET_ON")
									Clear_This_Objective_Text("HTV_GET_ON")
								ENDIF
								IF Is_This_The_Current_Objective_Text("HTV_GET_IN")
									Clear_This_Objective_Text("HTV_GET_IN")
								ENDIF
								IF Is_This_The_Current_Objective_Text("HTV_HELP3")
									Clear_This_Objective_Text("HTV_HELP3")
								ENDIF
								IF Is_This_The_Current_Objective_Text("HTV_HELP3B")
									Clear_This_Objective_Text("HTV_HELP3B")
								ENDIF
								
								IF IS_BIT_SET(iBoolsBitSet, biHelp5Done)
									CLEAR_BIT(iBoolsBitSet, biHelp5Done)
								ENDIF
								
								IF bStartSound = TRUE
									STOP_SOUND(iExplosionSound)
									RELEASE_SOUND_ID(iExplosionSound)
									bStartSound = FALSE
								ENDIF
								PRINTLN("[AM MOVING TARGET] - HELP TEXT - BACK IN VEHICLE    <----------     ")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//Set Disable Mod Shop Flag
			IF IS_NET_PLAYER_OK(PLAYER_ID())
			AND IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh))
				IF NOT IS_BIT_SET(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_PLAYER_USING_DESTROY_VEH)
					SET_BIT(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_PLAYER_USING_DESTROY_VEH)
					PRINTLN("[AM MOVING TARGET] - iNGABI_PLAYER_USING_DESTROY_VEH SET")
				ENDIF
			ELSE
				IF IS_BIT_SET(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_PLAYER_USING_DESTROY_VEH)
					CLEAR_BIT(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_PLAYER_USING_DESTROY_VEH)
					PRINTLN("[AM MOVING TARGET] - iNGABI_PLAYER_USING_DESTROY_VEH CLEARED")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_DPAD_DOWN_LEADERBOARD()
	DRAW_CHALLENGE_DPAD_LBD(chDPStruct.challengeLbdStruct, chDPStruct.siDpadMovie, SUB_CHALLENGE, chDPStruct.dpadVariables, chDPStruct.fmDpadStruct)
ENDPROC
 // #IF FEATURE_NEW_AMBIENT_EVENTS

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MISSION PROCS                  //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

PROC MAINTAIN_SPAWN_LOCATIONS()
	VECTOR vTargetCoords
	IF serverBD.piDriver != INVALID_PLAYER_INDEX()
	AND NETWORK_IS_PLAYER_ACTIVE(serverBD.piDriver)
	AND NOT IS_PED_INJURED(GET_PLAYER_PED(serverBD.piDriver))
		vTargetCoords = GET_ENTITY_COORDS(GET_PLAYER_PED(serverBD.piDriver))
		//Set up the span locations
		IF NOT IS_PLAYER_IN_CORONA()
		AND NOT IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), FALSE)
		AND NOT IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
			bSetSpawnLocations = TRUE
			SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_NEAR_SPECIFIC_COORDS)
			SETUP_SPECIFIC_SPAWN_LOCATION(vTargetCoords, 0.0, 250.0, TRUE, FMEVENT_MINIMUM_SPAWN_DISTANCE_FROM_ACTION, FALSE, TRUE, 65, TRUE, TRUE)
		ELSE
			IF bSetSpawnLocations
				//Clear the spawn locations
				PRINTLN("[AM MOVING TARGET] - CLEAR_PLAYER_NEXT_RESPAWN_LOCATION   <----------     ")
				CLEAR_PLAYER_NEXT_RESPAWN_LOCATION()
				CLEAR_SPECIFIC_SPAWN_LOCATION_FROM_THIS_SCRIPT()
				bSetSpawnLocations = FALSE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_DAMAGE(INT iCount)
	VEHICLE_INDEX VictimVehID
	PED_INDEX DamagerPedID, VictimPedID
	PLAYER_INDEX DamagerPlayerID, VictimPlayerID
	PARTICIPANT_INDEX DamagerParticipantID, VictimParticipantID	

	STRUCT_ENTITY_DAMAGE_EVENT sEntityID
	
	GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sEntityID, SIZE_OF(sEntityID))
	
	IF NOT FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_FM_EVENT(PLAYER_ID())
		// IF the victim exists
		IF DOES_ENTITY_EXIST(sEntityID.VictimIndex)
			// If the victom is a ped
			IF IS_ENTITY_A_PED(sEntityID.VictimIndex)
				// If the Damager exists
				IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
					// If the Damager is a ped
					IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
						//Get the two peds
						VictimPedID = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)
						DamagerPedID = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
						//If they are both players
						IF IS_PED_A_PLAYER(VictimPedID)
						AND IS_PED_A_PLAYER(DamagerPedID)
							//Get the two players
							VictimPlayerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(VictimPedID)
							DamagerPlayerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(DamagerPedID)
							
							// If the Damager is a participant
							IF NETWORK_IS_PLAYER_A_PARTICIPANT(VictimPlayerID)
							AND NETWORK_IS_PLAYER_A_PARTICIPANT(DamagerPlayerID)
								
								VictimParticipantID = NETWORK_GET_PARTICIPANT_INDEX(VictimPlayerID)
								DamagerParticipantID = NETWORK_GET_PARTICIPANT_INDEX(DamagerPlayerID)
								
								//If they are both active
								IF NETWORK_IS_PARTICIPANT_ACTIVE(VictimParticipantID)
								AND NETWORK_IS_PARTICIPANT_ACTIVE(DamagerParticipantID)
									IF DamagerPlayerID = PLAYER_ID()
										
										IF serverBD.piDriver != INVALID_PLAYER_INDEX()
										AND VictimPlayerID = serverBD.piDriver
											IF NOT HAS_NET_TIMER_STARTED(ParticipationTimer)
												START_NET_TIMER(ParticipationTimer)
											ENDIF
											FM_EVENT_SET_PLAYER_IS_PERMANENT_PARTICIPANT_IN_FM_EVENT(TRUE)
											CPRINTLN(DEBUG_AMBIENT, "[AM MOVING TARGET] -  PROCESS_DAMAGE - Damager has attacked the driver, now a permanent participant")
										ENDIF
										IF serverBD.piGunner != INVALID_PLAYER_INDEX()
										AND VictimPlayerID = serverBD.piGunner
											IF NOT HAS_NET_TIMER_STARTED(ParticipationTimer)
												START_NET_TIMER(ParticipationTimer)
											ENDIF
											FM_EVENT_SET_PLAYER_IS_PERMANENT_PARTICIPANT_IN_FM_EVENT(TRUE)
											CPRINTLN(DEBUG_AMBIENT, "[AM MOVING TARGET] -  PROCESS_DAMAGE - Damager has attacked the gunner, now a permanent participant")
										ENDIF
									
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELIF IS_ENTITY_A_VEHICLE(sEntityID.VictimIndex)
			
				// Get the vehicle index
				VictimVehID = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)
			
				// If the Damager exists
				IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
				
					// If the Damager is a ped
					IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
						
						DamagerPedID = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
						
						//If they are both players
						IF IS_PED_A_PLAYER(DamagerPedID)
							
							DamagerPlayerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(DamagerPedID)
							
							//If they are both active
							IF NETWORK_IS_PLAYER_ACTIVE(DamagerPlayerID)
								IF DamagerPlayerID = PLAYER_ID()
								
									IF VictimVehID = NET_TO_VEH(serverBD.niVeh)
										IF NOT HAS_NET_TIMER_STARTED(ParticipationTimer)
											START_NET_TIMER(ParticipationTimer)
										ENDIF
										FM_EVENT_SET_PLAYER_IS_PERMANENT_PARTICIPANT_IN_FM_EVENT(TRUE)
										CPRINTLN(DEBUG_AMBIENT, "[AM MOVING TARGET] -  PROCESS_DAMAGE - Damager has attacked the vehicle, now a permanent participant")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_HOT_TARGET_DAMAGE_EVENTS()
	INT iCount
	EVENT_NAMES ThisScriptEvent
	
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount
	
		ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)
		
		// Handle the damage events
		IF ThisScriptEvent = EVENT_NETWORK_DAMAGE_ENTITY
			PROCESS_DAMAGE(iCount)
		ENDIF
		
	ENDREPEAT
ENDPROC

PROC HANDLE_VEHICLE_IN_SHOP()

	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
		VECTOR vVehPos = GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niVeh),FALSE)
		SHOP_NAME_ENUM closestShop = GET_CLOSEST_SHOP_OF_TYPE(vVehPos)

		VECTOR vShopCoords = GET_SHOP_COORDS(closestShop)

		INTERIOR_INSTANCE_INDEX interiorShop = GET_INTERIOR_AT_COORDS_WITH_TYPE(vShopCoords, GET_SHOP_INTERIOR_TYPE(closestShop))
		INTERIOR_INSTANCE_INDEX interiorVeh = GET_INTERIOR_FROM_ENTITY(NET_TO_VEH(serverBD.niVeh))
		IF interiorShop != NULL
		AND interiorShop = interiorVeh
			IF NOT MPGlobalsAmbience.bKeepShopDoorsOpen
				MPGlobalsAmbience.bKeepShopDoorsOpen = TRUE
				CPRINTLN(DEBUG_NET_AMBIENT, "[AM MOVING TARGET] -  MPGlobalsAmbience.bKeepShopDoorsOpenForParcelVehicle = TRUE")
			ENDIF
			EXIT
		ENDIF
	ENDIF
	
	IF MPGlobalsAmbience.bKeepShopDoorsOpen
		MPGlobalsAmbience.bKeepShopDoorsOpen = FALSE
		CPRINTLN(DEBUG_NET_AMBIENT, "[AM MOVING TARGET] -  MPGlobalsAmbience.bKeepShopDoorsOpenForParcelVehicle = FALSE")
	ENDIF

ENDPROC

FUNC BOOL IS_DEAD_PED_IN_VEHICLE_DRIVER_SEAT()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
		IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh))
			IF GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(serverBD.niVeh), VS_DRIVER) != NULL
				IF IS_PED_DEAD_OR_DYING(GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(serverBD.niVeh), VS_DRIVER))
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_VEHICLE_LOCKING_AND_INVINCIBILITY()	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
		IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh))
			IF GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
				IF NOT GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.niVeh), PLAYER_ID())
					SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.niVeh), PLAYER_ID(), TRUE)
					PRINTLN("[AM MOVING TARGET] - MAINTAIN_VEHICLE_LOCKING_AND_INVINCIBILITY - Player is critical on boss work - locking the vehicle for me!")
				ENDIF
			ELSE
				IF NOT SHOULD_UI_BE_HIDDEN()
				AND NOT AM_I_RESTRICTED()
					IF GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.niVeh), PLAYER_ID())
						SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.niVeh), PLAYER_ID(), FALSE)
						PRINTLN("[AM MOVING TARGET] - MAINTAIN_VEHICLE_LOCKING_AND_INVINCIBILITY - Player is NOT critical on boss work - vehicle should not be locked for me!")
					ENDIF
				ENDIF
			ENDIF
		
			IF HAVE_PLAYERS_ENTERED_TO_START_EVENT()
			
				IF GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
					IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh), TRUE)
						IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_VEHICLE) != PERFORMING_TASK
							TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh))
							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_AbandonedForBossWork)
							PRINTLN("[AM MOVING TARGET] - MAINTAIN_VEHICLE_LOCKING_AND_INVINCIBILITY - Booting player out of vehicle as they are critical to boss work")
						ENDIF
					ENDIF
				ENDIF
			
				IF telemetryStruct.m_timeTakenForObjective = 0
					telemetryStruct.m_timeTakenForObjective = GET_CLOUD_TIME_AS_INT() 
				ENDIF
				IF NETWORK_HAS_CONTROL_OF_ENTITY(NET_TO_VEH(serverBD.niVeh))
					SET_ENTITY_INVINCIBLE(NET_TO_VEH(serverBD.niVeh), FALSE)
					SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(NET_TO_VEH(serverBD.niVeh), TRUE)
				ENDIF
			
				IF NOT AM_I_A_HOT_TARGET()
					IF IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iFmAmbientEventBitSet, ciSET_PLAYER_IN_MOVING_TARGET_VEHICLE)
						CLEAR_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iFmAmbientEventBitSet, ciSET_PLAYER_IN_MOVING_TARGET_VEHICLE)
					ENDIF
					
					// Lock the doors to players in passive mode
					IF NOT GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.niVeh), PLAYER_ID())
						SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.niVeh), PLAYER_ID(), TRUE)
						PRINTLN("[AM MOVING TARGET] - MAINTAIN_VEHICLE_LOCKING_AND_INVINCIBILITY - Locking vehicle as player is not a hot target")
					ENDIF
					
					IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh), TRUE)
						IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_VEHICLE) != PERFORMING_TASK
							TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh))
							PRINTLN("[AM MOVING TARGET] - MAINTAIN_VEHICLE_LOCKING_AND_INVINCIBILITY - Booting player out of vehicle as I'm not a hot target")
						ENDIF
					ENDIF
				ELSE
					IF NOT AM_I_RESTRICTED()
						IF playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_DELIVER
							IF NOT FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT(PLAYER_ID())
								FM_EVENT_SET_PLAYER_IS_CRITICAL_TO_FM_EVENT(TRUE)
							ENDIF
							IF NOT FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_FM_EVENT(PLAYER_ID())
								FM_EVENT_SET_PLAYER_IS_PERMANENT_PARTICIPANT_IN_FM_EVENT(TRUE)
							ENDIF
						ELSE
							IF FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT(PLAYER_ID())
								FM_EVENT_SET_PLAYER_IS_CRITICAL_TO_FM_EVENT(FALSE)
							ENDIF
						ENDIF
					ENDIF
				
					IF playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_DELIVER
						IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
							IF IS_BIT_SET(serverBD.iServerBitSet, biS_DriverLeft)
							OR IS_BIT_SET(playerBD[NATIVE_TO_INT(serverBD.piDriver)].iPlayerBitSet, biP_OutOfVehicleFail)
								DO_HELP_TEXT(13)
							ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_DriverKilled)
								DO_HELP_TEXT(14)
							ENDIF
						ENDIF
					
						//Lock doors once event has started					
						IF GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(serverBD.niVeh), VS_DRIVER) != NULL
						AND NOT IS_DEAD_PED_IN_VEHICLE_DRIVER_SEAT()
							IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Driver)
							OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Gunner)
								IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())								
									SET_PED_CAN_BE_DRAGGED_OUT(PLAYER_PED_ID(), FALSE)
									SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PlayersDontDragMeOutOfCar, TRUE)
								ENDIF
								DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
								//PRINTLN("[AM MOVING TARGET] -  CONTROL_VEH_BODY - SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER - Driver")
								IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh), FALSE)
									IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
									OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
										DO_HELP_TEXT(5)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
							
					IF playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_OFF_MISSION
						IF NOT GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.niVeh), PLAYER_ID())
							SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.niVeh), PLAYER_ID(), TRUE)
							PRINTLN("[AM MOVING TARGET] - MAINTAIN_VEHICLE_LOCKING_AND_INVINCIBILITY - Hot Target is off mission - locking the vehicle for me!")
						ENDIF
					ENDIF
					
					IF IS_BIT_SET(serverBD.iServerBitSet, biS_DriverLeft)
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
							IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh))
							AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								SET_PED_VEHICLE_FORCED_SEAT_USAGE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh), 0, ENUM_TO_INT(VC_FORCE_USE_FRONT_SEATS))
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF SHOULD_UI_BE_HIDDEN()
					IF NOT GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.niVeh), PLAYER_ID())
						SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.niVeh), PLAYER_ID(), TRUE)
						PRINTLN("[AM MOVING TARGET] - MAINTAIN_VEHICLE_LOCKING_AND_INVINCIBILITY - Player has event hidden - locking the vehicle for me!")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_PARTICIPATION_TIME()
	RETURN GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(ParticipationTimer)
ENDFUNC

/// PURPOSE:
///    Gives the local player Cash and RP for delivering the Hot Target vehicle
PROC GIVE_LOCAL_PLAYER_DELIVERER_CASH_RP()
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ReceivedReward)
		IF HAS_PLAYER_FULLY_SPAWNED()
			IF HAVE_I_MET_REWARD_CONDITIONS()
			
				IF HAS_NET_TIMER_STARTED(ParticipationTimer)
					iParticipationTime = GET_PARTICIPATION_TIME()
				ENDIF
			
				NEXT_RP_ADDITION_SHOW_RANKBAR()
				GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "XPT_KAIE", XPTYPE_SKILL, XPCATEGORY_COMPLETE_HOT_TARGET, GET_RP_REWARD(eHT_CASH_RP_DELIVERER), 1)
				
				INT iCash = GET_CASH_REWARD(eHT_CASH_RP_DELIVERER)
				
				PRINTLN("[AM MOVING TARGET][MAGNATE_GANG_BOSS] - Cash before boss cut: $", iCash)
				GB_HANDLE_GANG_BOSS_CUT(iCash)
				PRINTLN("[AM MOVING TARGET][MAGNATE_GANG_BOSS] - Cash after boss cut: $", iCash)
				
				iCash += GET_CASH_REWARD(eHT_CASH_RP_PARTICIPANT)
				PRINTLN("[AM MOVING TARGET] - GIVE_LOCAL_PLAYER_DELIVERER_CASH_RP - Participation cash: $", GET_CASH_REWARD(eHT_CASH_RP_PARTICIPANT))
				PRINTLN("[AM MOVING TARGET] - GIVE_LOCAL_PLAYER_DELIVERER_CASH_RP - Total cash to give player: $", iCash)
				
				IF iCash > 0
					IF USE_SERVER_TRANSACTIONS()
						INT iScriptTransactionIndex
						TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_AMBIENT_JOB_HOT_TARGET_DELIVER, iCash, iScriptTransactionIndex, FALSE, TRUE) //Add new service
					ELSE
						AMBIENT_JOB_DATA amData
						NETWORK_EARN_FROM_AMBIENT_JOB(iCash, "AM_HOT_TARGET_DELIVER", amData)
					ENDIF
					
					g_i_cashForEndEventShard = iCash
						
					telemetryStruct.m_cashEarned += iCash
				ENDIF
				
				telemetryStruct.m_rpEarned += GET_RP_REWARD(eHT_CASH_RP_DELIVERER)
				
				IF NOT g_sMPTunables.bHot_Target_Disable_Share_Cash
					IF telemetryStruct.m_cashEarned > 0	
						SET_LAST_JOB_DATA(LAST_JOB_HOT_TARGET, telemetryStruct.m_cashEarned)
					ENDIF
				ENDIF
				
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ReceivedReward)
				PRINTLN("[AM MOVING TARGET] -  GIVE_LOCAL_PLAYER_DELIVERER_CASH_RP")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Gives the local player Cash and RP for killing the Hot Target/ Vehicle if eligible
/// PARAMS:
///    bTwoManVeh - TRUE if the vehicle was a Insurgent, Technical or Valkyrie and there were 2 hot targets
PROC GIVE_LOCAL_PLAYER_CHASER_CASH_RP(BOOL bTwoManVeh = FALSE)
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ReceivedReward)
		IF HAS_PLAYER_FULLY_SPAWNED()
			IF HAVE_I_MET_REWARD_CONDITIONS()
			
				IF HAS_NET_TIMER_STARTED(ParticipationTimer)
					iParticipationTime = GET_PARTICIPATION_TIME()
				ENDIF
				
				NEXT_RP_ADDITION_SHOW_RANKBAR()
				GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "XPT_KAIE", XPTYPE_SKILL, XPCATEGORY_KILL_HOT_TARGET, GET_RP_REWARD(eHT_CASH_RP_CHASER), 1)
				
				INT iCash = GET_CASH_REWARD(eHT_CASH_RP_CHASER)
				
				IF bTwoManVeh
					iCash = (iCash/2)
				ENDIF
				
				PRINTLN("[AM MOVING TARGET][MAGNATE_GANG_BOSS] - Cash before boss cut: $", iCash)
				GB_HANDLE_GANG_BOSS_CUT(iCash)
				PRINTLN("[AM MOVING TARGET][MAGNATE_GANG_BOSS] - Cash after boss cut: $", iCash)
				
				iCash += GET_CASH_REWARD(eHT_CASH_RP_PARTICIPANT)
				PRINTLN("[AM MOVING TARGET] - GIVE_LOCAL_PLAYER_CHASER_CASH_RP - Participation cash: $", GET_CASH_REWARD(eHT_CASH_RP_PARTICIPANT))
				PRINTLN("[AM MOVING TARGET] - GIVE_LOCAL_PLAYER_CHASER_CASH_RP - Total cash to give player: $", iCash)
				
				// Do not perform any EARN commands if the cash is 0 (2611826)
				IF iCash > 0
					IF USE_SERVER_TRANSACTIONS()
						INT iScriptTransactionIndex
						TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_AMBIENT_JOB_HOT_TARGET_KILL, iCash, iScriptTransactionIndex, FALSE, TRUE) //Add new service
					ELSE
						AMBIENT_JOB_DATA amData
						NETWORK_EARN_FROM_AMBIENT_JOB(iCash, "AM_HOT_TARGET_KILL", amData)
					ENDIF
				ENDIF
				
				g_i_cashForEndEventShard = iCash
				
				telemetryStruct.m_cashEarned += iCash
				telemetryStruct.m_rpEarned += GET_RP_REWARD(eHT_CASH_RP_CHASER)
				
				IF NOT g_sMPTunables.bHot_Target_Disable_Share_Cash
					IF telemetryStruct.m_cashEarned > 0	
						SET_LAST_JOB_DATA(LAST_JOB_HOT_TARGET, telemetryStruct.m_cashEarned)
					ENDIF
				ENDIF
				
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ReceivedReward)
				PRINTLN("[AM MOVING TARGET] -  GIVE_LOCAL_PLAYER_CHASER_CASH_RP")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Gives the local player Cash and RP for participating in Hot Target if eligible
PROC GIVE_LOCAL_PLAYER_MINIMUM_PARTICIPATION_CASH_RP(BOOL bTwoManVeh = FALSE)
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ReceivedReward)
		IF HAS_PLAYER_FULLY_SPAWNED()
			// Reward other players who participated with some cash
			IF HAVE_I_MET_REWARD_CONDITIONS()
				FM_EVENT_SETUP_DO_MINIMUM_PARTICIPATION_HELP(TRUE)
				IF HAS_NET_TIMER_STARTED(ParticipationTimer)
					iParticipationTime = GET_PARTICIPATION_TIME()
				ENDIF
			
				NEXT_RP_ADDITION_SHOW_RANKBAR()
				GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "XPT_KAIE", XPTYPE_SKILL, XPCATEGORY_ASSIST_HOT_TARGET_KILLER, GET_RP_REWARD(eHT_CASH_RP_PARTICIPANT), 1)
				
				INT iCash = GET_CASH_REWARD(eHT_CASH_RP_PARTICIPANT)
				
				IF bTwoManVeh
					iCash = (iCash/2)
				ENDIF
				
				IF iCash > 0
					IF USE_SERVER_TRANSACTIONS()
						INT iScriptTransactionIndex
						TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_AMBIENT_JOB_HOT_TARGET_KILL, iCash, iScriptTransactionIndex, FALSE, TRUE) //Add new service
					ELSE
						AMBIENT_JOB_DATA amData
						NETWORK_EARN_FROM_AMBIENT_JOB(iCash, "AM_HOT_TARGET_KILL", amData)
					ENDIF
				ENDIF
				
				g_i_cashForEndEventShard = iCash
				telemetryStruct.m_cashEarned += iCash
				telemetryStruct.m_rpEarned += GET_RP_REWARD(eHT_CASH_RP_PARTICIPANT)
				
				IF NOT g_sMPTunables.bHot_Target_Disable_Share_Cash
					IF telemetryStruct.m_cashEarned > 0	
						SET_LAST_JOB_DATA(LAST_JOB_HOT_TARGET, telemetryStruct.m_cashEarned)
					ENDIF
				ENDIF
				
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ReceivedReward)
				PRINTLN("[AM MOVING TARGET] -  GIVE_LOCAL_PLAYER_MINIMUM_PARTICIPATION_CASH_RP")
			ELSE
				FM_EVENT_SETUP_DO_MINIMUM_PARTICIPATION_HELP(FALSE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_EVENT_END_REWARDS_AND_MESSAGES()
	// If the player hasnt already received reward at end
	
	// EVENT OVER ---------------------------------------------------------------------------------
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_MinPlayersBailServer)
		GIVE_LOCAL_PLAYER_MINIMUM_PARTICIPATION_CASH_RP()
		EXIT
	ENDIF
	
	// THE TIMER HAS EXPIRED OR THE HOT TARGETS ARE OFF MISSION
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_MissionEndTimerHit)
	OR IS_BIT_SET(serverBD.iServerBitSet, biS_VehicleStuck)
	OR (IS_BIT_SET(serverBD.iServerBitSet, biS_DriverLeft) AND IS_GUNNER_OFF_MISSION())
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_VehicleDelivered)
			IF AM_I_A_HOT_TARGET()
				DO_BIG_MESSAGE(6)
				GIVE_LOCAL_PLAYER_MINIMUM_PARTICIPATION_CASH_RP()
			ELSE
				IF AM_I_PARTICIPATING_IN_EVENT()
					IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
					AND serverBD.piGunner != INVALID_PLAYER_INDEX()
						DO_BIG_MESSAGE(8)
					ELSE
						DO_BIG_MESSAGE(7)
					ENDIF				
				ELSE
					IF NOT IS_BIT_SET(iBoolsBitSet3, biEndTickerDone)
						IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
							IF serverBD.piDriver != INVALID_PLAYER_INDEX()
							AND serverBD.piGunner != INVALID_PLAYER_INDEX()
								IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piDriver)
								AND NETWORK_IS_PLAYER_ACTIVE(serverBD.piGunner)
									PRINT_TICKER_WITH_TWO_PLAYER_NAMES("HTV_TCHTF1", serverBD.piDriver, serverBD.piGunner)
								ENDIF
							ENDIF
						ELSE
							IF IS_BIT_SET(serverBD.iServerBitSet, biS_DriverLeft)
							OR (NOT IS_BIT_SET(serverBD.iServerBitSet, biS_DriverLeft) AND IS_DRIVER_OFF_MISSION())
								IF serverBD.piGunner != INVALID_PLAYER_INDEX()
									IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piGunner)
										PRINT_TICKER_WITH_PLAYER_NAME("HTV_TCHTF2", serverBD.piGunner)
									ENDIF
								ENDIF
							ELSE
								IF serverBD.piDriver != INVALID_PLAYER_INDEX()
									IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piDriver)
										PRINT_TICKER_WITH_PLAYER_NAME("HTV_TCHTF2", serverBD.piDriver)
									ENDIF
								ENDIF
							ENDIF
						ENDIF					
						SET_BIT(iBoolsBitSet3, biEndTickerDone)
					ENDIF
				ENDIF
				GIVE_LOCAL_PLAYER_MINIMUM_PARTICIPATION_CASH_RP()
				FM_EVENT_SET_PLAYER_IS_CRITICAL_TO_FM_EVENT(FALSE)
			ENDIF
			
			// make sure we dont reach the next case
			EXIT
		ENDIF			
	ENDIF
	
	// THE HOT TARGETS LEFT THE SESSION
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_HotTargetsLeft)
		IF AM_I_A_HOT_TARGET()
			DO_BIG_MESSAGE(6)
			GIVE_LOCAL_PLAYER_MINIMUM_PARTICIPATION_CASH_RP()
		ELSE
			IF AM_I_PARTICIPATING_IN_EVENT()
				IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
					DO_BIG_MESSAGE(10)
				ELSE
					DO_BIG_MESSAGE(11)
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(iBoolsBitSet3, biEndTickerDone)
					IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
						PRINT_TICKER("HTV_TCHTLF2")
					ELSE
						PRINT_TICKER("HTV_TCHTLF1")
					ENDIF
					SET_BIT(iBoolsBitSet3, biEndTickerDone)
				ENDIF
			ENDIF
			GIVE_LOCAL_PLAYER_MINIMUM_PARTICIPATION_CASH_RP()
			FM_EVENT_SET_PLAYER_IS_CRITICAL_TO_FM_EVENT(FALSE)
		ENDIF
		
		// make sure we dont reach the next case
		EXIT
	ENDIF
	
	// --------------------------------------------------------------------------------------------
	
	// WINNERS / LOSERS ---------------------------------------------------------------------------
	
	// THE VEHICLE HAS BEEN DELIVERED
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_VehicleDelivered)
		IF AM_I_A_HOT_TARGET()
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
				IF AM_I_THE_DRIVER()
					DO_BIG_MESSAGE(4)
					GIVE_LOCAL_PLAYER_DELIVERER_CASH_RP()
				ENDIF
				IF AM_I_THE_GUNNER()
					IF HAS_GUNNER_BEEN_KILLED()
					OR IS_GUNNER_OFF_MISSION()
						DO_BIG_MESSAGE(4)
						GIVE_LOCAL_PLAYER_MINIMUM_PARTICIPATION_CASH_RP()
					ELSE
						DO_BIG_MESSAGE(4)
						GIVE_LOCAL_PLAYER_DELIVERER_CASH_RP()
					ENDIF
				ENDIF
			ELSE
				telemetryStruct.m_endReason = AE_END_WON
				DO_BIG_MESSAGE(4)
				GIVE_LOCAL_PLAYER_DELIVERER_CASH_RP()
			ENDIF
			
			telemetryStruct.m_endReason = AE_END_WON
			// Show exit vehicle objective text if the explosion timer has started.
			IF HAS_NET_TIMER_STARTED(serverBD.ExplodeVehTimer)
				IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh))
					Print_Objective_Text("HTV_LEAVE_EX")
				ELSE
					IF Is_There_Any_Current_Objective_Text_From_This_Script()
						Clear_Any_Objective_Text_From_This_Script()
					ENDIF
				ENDIF
			ENDIF
		ELSE
			GIVE_LOCAL_PLAYER_MINIMUM_PARTICIPATION_CASH_RP()
			IF AM_I_PARTICIPATING_IN_EVENT()
				IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
					IF HAS_GUNNER_BEEN_KILLED()
					OR IS_GUNNER_OFF_MISSION()
						DO_BIG_MESSAGE(5)
					ELSE
						DO_BIG_MESSAGE(9)
					ENDIF
				ELSE
					DO_BIG_MESSAGE(5)
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(iBoolsBitSet3, biEndTickerDone)
					IF (IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
					AND (NOT HAS_GUNNER_BEEN_KILLED()
					AND NOT IS_GUNNER_OFF_MISSION()))
						IF serverBD.piDriver != INVALID_PLAYER_INDEX()
						AND serverBD.piGunner != INVALID_PLAYER_INDEX()
							IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piDriver)
							AND NETWORK_IS_PLAYER_ACTIVE(serverBD.piGunner)
								PRINT_TICKER_WITH_TWO_PLAYER_NAMES("HTV_TCDEL2", serverBD.piDriver, serverBD.piGunner)
							ENDIF
						ENDIF
					ELSE
						IF IS_BIT_SET(serverBD.iServerBitSet, biS_DriverLeft)
						OR (NOT IS_BIT_SET(serverBD.iServerBitSet, biS_DriverLeft) AND IS_DRIVER_OFF_MISSION())
							IF serverBD.piGunner != INVALID_PLAYER_INDEX()
								IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piGunner)
									PRINT_TICKER_WITH_PLAYER_NAME("HTV_TCDEL", serverBD.piGunner)
								ENDIF
							ENDIF
						ELSE
							IF serverBD.piDriver != INVALID_PLAYER_INDEX()
								IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piDriver)
									PRINT_TICKER_WITH_PLAYER_NAME("HTV_TCDEL", serverBD.piDriver)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					SET_BIT(iBoolsBitSet3, biEndTickerDone)
				ENDIF
			ENDIF
		ENDIF
		FM_EVENT_SET_PLAYER_IS_CRITICAL_TO_FM_EVENT(FALSE)
		
		// Make sure we dont reach the next case and reward again
		EXIT
	ENDIF
	
	// THE HOT TARGET(S) HAS BEEN KILLED
	IF HAVE_HOT_TARGETS_BEEN_KILLED()
		IF AM_I_A_HOT_TARGET()
			DO_BIG_MESSAGE(6)
			GIVE_LOCAL_PLAYER_MINIMUM_PARTICIPATION_CASH_RP()
		ELSE
			telemetryStruct.m_endReason = AE_END_WON
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
				IF AM_I_PARTICIPATING_IN_EVENT()
					IF AM_I_RESTRICTED()
						DO_BIG_MESSAGE(15)
					ELSE
						// If you killed driver and not gunner OR you killed the gunner and not driver
						IF DID_I_KILL_ONLY_ONE_HOT_TARGET()
							DO_BIG_MESSAGE(12)
							GIVE_LOCAL_PLAYER_CHASER_CASH_RP(TRUE)
						// If you killed both 
						ELIF DID_I_KILL_BOTH_HOT_TARGETS()
							DO_BIG_MESSAGE(14)
							GIVE_LOCAL_PLAYER_CHASER_CASH_RP()
						// If you killed neither
						ELSE
							DO_BIG_MESSAGE(15)
							GIVE_LOCAL_PLAYER_MINIMUM_PARTICIPATION_CASH_RP()
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_BIT_SET(iBoolsBitSet3, biEndTickerDone)
						GIVE_LOCAL_PLAYER_MINIMUM_PARTICIPATION_CASH_RP()
						IF serverBD.piDriver != INVALID_PLAYER_INDEX()
						AND serverBD.piGunner != INVALID_PLAYER_INDEX()
							IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piDriver)
							AND NETWORK_IS_PLAYER_ACTIVE(serverBD.piGunner)
								PRINT_TICKER_WITH_TWO_PLAYER_NAMES("HTV_TCHTEL1", serverBD.piDriver, serverBD.piGunner)
							ENDIF
						ENDIF
						SET_BIT(iBoolsBitSet3, biEndTickerDone)
					ENDIF
				ENDIF
			ELSE
				IF AM_I_PARTICIPATING_IN_EVENT()
					IF AM_I_RESTRICTED()
						DO_BIG_MESSAGE(16)
					ELSE
						IF DID_I_KILL_THE_DRIVER()
							DO_BIG_MESSAGE(13)
							GIVE_LOCAL_PLAYER_CHASER_CASH_RP()
						ELSE
							DO_BIG_MESSAGE(16)
							GIVE_LOCAL_PLAYER_MINIMUM_PARTICIPATION_CASH_RP()
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_BIT_SET(iBoolsBitSet3, biEndTickerDone)
						GIVE_LOCAL_PLAYER_MINIMUM_PARTICIPATION_CASH_RP()
						IF IS_BIT_SET(serverBD.iServerBitSet, biS_DriverLeft)
						OR (NOT IS_BIT_SET(serverBD.iServerBitSet, biS_DriverLeft) AND IS_DRIVER_OFF_MISSION())
							IF serverBD.piGunner != INVALID_PLAYER_INDEX()
								IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piGunner)
									PRINT_TICKER_WITH_PLAYER_NAME("HTV_TCHTEL2", serverBD.piGunner)
								ENDIF
							ENDIF
						ELSE
							IF serverBD.piDriver != INVALID_PLAYER_INDEX()
								IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piDriver)
									PRINT_TICKER_WITH_PLAYER_NAME("HTV_TCHTEL2", serverBD.piDriver)
								ENDIF
							ENDIF
						ENDIF
						SET_BIT(iBoolsBitSet3, biEndTickerDone)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		FM_EVENT_SET_PLAYER_IS_CRITICAL_TO_FM_EVENT(FALSE)
		
		// Make sure we dont reach the next case and reward again
		EXIT
	ENDIF
		
	IF ARE_HOT_TARGETS_OFF_MISSION()
		IF AM_I_A_HOT_TARGET()
			DO_BIG_MESSAGE(6)
			GIVE_LOCAL_PLAYER_MINIMUM_PARTICIPATION_CASH_RP()
		ELSE
			IF AM_I_PARTICIPATING_IN_EVENT()
				IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
				AND serverBD.piGunner != INVALID_PLAYER_INDEX()
					DO_BIG_MESSAGE(10)
				ELSE
					DO_BIG_MESSAGE(11)
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(iBoolsBitSet3, biEndTickerDone)
					IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
						IF serverBD.piDriver != INVALID_PLAYER_INDEX()
						AND serverBD.piGunner != INVALID_PLAYER_INDEX()
							IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piDriver)
							AND NETWORK_IS_PLAYER_ACTIVE(serverBD.piGunner)
								PRINT_TICKER_WITH_TWO_PLAYER_NAMES("HTV_TCHTAB1", serverBD.piDriver, serverBD.piGunner)
							ENDIF
						ENDIF
					ELSE
						IF IS_BIT_SET(serverBD.iServerBitSet, biS_DriverLeft)
						OR (NOT IS_BIT_SET(serverBD.iServerBitSet, biS_DriverLeft) AND IS_DRIVER_OFF_MISSION())
							IF serverBD.piGunner != INVALID_PLAYER_INDEX()
								IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piGunner)
									PRINT_TICKER_WITH_PLAYER_NAME("HTV_TCHTAB2", serverBD.piGunner)
								ENDIF
							ENDIF
						ELSE
							IF serverBD.piDriver != INVALID_PLAYER_INDEX()
								IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piDriver)
									PRINT_TICKER_WITH_PLAYER_NAME("HTV_TCHTAB2", serverBD.piDriver)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					SET_BIT(iBoolsBitSet3, biEndTickerDone)
				ENDIF
			ENDIF
			GIVE_LOCAL_PLAYER_MINIMUM_PARTICIPATION_CASH_RP()
		ENDIF
		FM_EVENT_SET_PLAYER_IS_CRITICAL_TO_FM_EVENT(FALSE)
		
		// make sure we dont reach the next case
		EXIT
	ENDIF
	
	// Vehicle has been destroyed on route but not by a player.
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_VehicleDelivered)
	AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_MissionEndTimerHit)
	AND HAVE_PLAYERS_ENTERED_TO_START_EVENT()
	AND NOT HAVE_HOT_TARGETS_BEEN_KILLED()
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
			IF IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh))
				IF AM_I_A_HOT_TARGET()
					DO_BIG_MESSAGE(6)
					GIVE_LOCAL_PLAYER_MINIMUM_PARTICIPATION_CASH_RP()
				ELSE
					IF AM_I_PARTICIPATING_IN_EVENT()
						IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
						AND serverBD.piGunner != INVALID_PLAYER_INDEX()
							DO_BIG_MESSAGE(8)
						ELSE
							DO_BIG_MESSAGE(7)
						ENDIF
					ELSE
						IF NOT IS_BIT_SET(iBoolsBitSet3, biEndTickerDone)
							IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
								IF serverBD.piDriver != INVALID_PLAYER_INDEX()
								AND serverBD.piGunner != INVALID_PLAYER_INDEX()
									IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piDriver)
									AND NETWORK_IS_PLAYER_ACTIVE(serverBD.piGunner)
										PRINT_TICKER_WITH_TWO_PLAYER_NAMES("HTV_TCHTF1", serverBD.piDriver, serverBD.piGunner)
									ENDIF
								ENDIF
							ELSE
								IF IS_BIT_SET(serverBD.iServerBitSet, biS_DriverLeft)
								OR (NOT IS_BIT_SET(serverBD.iServerBitSet, biS_DriverLeft) AND IS_DRIVER_OFF_MISSION())
									IF serverBD.piGunner != INVALID_PLAYER_INDEX()
										IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piGunner)
											PRINT_TICKER_WITH_PLAYER_NAME("HTV_TCHTF2", serverBD.piGunner)
										ENDIF
									ENDIF
								ELSE
									IF serverBD.piDriver != INVALID_PLAYER_INDEX()
										IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piDriver)
											PRINT_TICKER_WITH_PLAYER_NAME("HTV_TCHTF2", serverBD.piDriver)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							SET_BIT(iBoolsBitSet3, biEndTickerDone)
						ENDIF
					ENDIF	
					GIVE_LOCAL_PLAYER_MINIMUM_PARTICIPATION_CASH_RP()
				ENDIF
				FM_EVENT_SET_PLAYER_IS_CRITICAL_TO_FM_EVENT(FALSE)
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

PROC MAINTAIN_PASSIVE_MODE_CHECKS()

	// Exit if the vehicle does not exist
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
		IF HAVE_PLAYERS_ENTERED_TO_START_EVENT()
			MAINTAIN_FREEMODE_AMBIENT_EVENT_PASSIVE_MODE_WARNING_FLAG(GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niVeh)), TO_FLOAT(g_sMPTunables.iFREEMODE_EVENT_UI_ENTRY_DISTANCE))
		ENDIF
		
		IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID())		
			IF HAVE_PLAYERS_ENTERED_TO_START_EVENT()			
				IF NOT AM_I_A_HOT_TARGET()
					IF playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_KILL
						playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_OFF_MISSION
						PRINTLN("[AM MOVING TARGET] - MAINTAIN_PASSIVE_MODE_CHECKS - setting player stage to OFF_MISSION as they are in passive")
					ENDIF
				ENDIF
			ELSE
				// Lock the doors to players in passive mode
				IF NOT GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.niVeh), PLAYER_ID())
					SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.niVeh), PLAYER_ID(), TRUE)
					PRINTLN("[AM MOVING TARGET] - MAINTAIN_PASSIVE_MODE_CHECKS - Locking vehicle as player is passive")
				ENDIF			
				
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh), FALSE) < 20
					IF IS_PED_TRYING_TO_ENTER_A_LOCKED_VEHICLE(PLAYER_PED_ID())
						DO_HELP_TEXT(0)
					ENDIF
				ENDIF			
			ENDIF
		ELSE		
			IF HAVE_PLAYERS_ENTERED_TO_START_EVENT()
				IF NOT FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT(PLAYER_ID())
				AND NOT AM_I_A_HOT_TARGET()
					IF playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_OFF_MISSION
						playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_KILL
						PRINTLN("[AM MOVING TARGET] - MAINTAIN_PASSIVE_MODE_CHECKS - Setting player to KILL as passive mode is no longer enabled")
					ENDIF
				ENDIF
			ELSE
				IF NOT SHOULD_UI_BE_HIDDEN()
					// Unlock the doors to players not in passive mode
					IF GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.niVeh), PLAYER_ID())
						SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.niVeh), PLAYER_ID(), FALSE)
						PRINTLN("[AM MOVING TARGET] - MAINTAIN_PASSIVE_MODE_CHECKS - Unlocking vehicle as player is not passive")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Clear the "Wait for driver/gunner" text
PROC CLEAR_WAIT_TEXT()
	IF Is_This_The_Current_Objective_Text("HTV_GWAITG")
		Clear_This_Objective_Text("HTV_GWAITG")
	ENDIF
	IF Is_This_The_Current_Objective_Text("HTV_GWAITD2")
		Clear_This_Objective_Text("HTV_GWAITD2")
	ENDIF
	IF Is_This_The_Current_Objective_Text("HTV_GWAITD")
		Clear_This_Objective_Text("HTV_GWAITD")
	ENDIF
ENDPROC

/// PURPOSE:
///    Clear the objective text shown to the hot targets
PROC CLEAR_DELIVERY_OBJECTIVE_TEXT()
	IF Is_This_The_Current_Objective_Text("HTV_GDEL2")
		Clear_This_Objective_Text("HTV_GDEL2")
	ENDIF
	IF Is_This_The_Current_Objective_Text("HTV_GDEL")
		Clear_This_Objective_Text("HTV_GDEL")
	ENDIF
ENDPROC

/// PURPOSE:
///    Clear the objective text shown to chasers/participants (not hot target)
PROC CLEAR_KILL_OBJECTIVE_TEXT()
	IF Is_This_The_Current_Objective_Text("HPV_PERMHLDR")
		Clear_This_Objective_Text("HPV_PERMHLDR")
	ENDIF
	IF Is_This_The_Current_Objective_Text("HPV_PERMHLDRb")
		Clear_This_Objective_Text("HPV_PERMHLDRb")
	ENDIF
	IF Is_This_The_Current_Objective_Text("HPV_PERMHLDR2")
		Clear_This_Objective_Text("HPV_PERMHLDR2")
	ENDIF
	IF Is_This_The_Current_Objective_Text("HPV_PERMHLDR2b")
		Clear_This_Objective_Text("HPV_PERMHLDR2b")
	ENDIF
	IF Is_This_The_Current_Objective_Text("HPV_PERMHLDR2c")
		Clear_This_Objective_Text("HPV_PERMHLDR2c")
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintains the objective text seen by the Hot Targets
PROC MAINTAIN_DELIVERY_OBJECTIVE_TEXT()
	CLEAR_WAIT_TEXT()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
		IF AM_I_A_HOT_TARGET()
		OR AM_I_SPECTATING_A_HOT_TARGET()
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh))
				OR IS_SPECTATOR_FOCUS_PED_IN_HOT_TARGET_VEHICLE()
					IF GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(serverBD.niVeh), VS_DRIVER) = PLAYER_PED_ID()
					OR (AM_I_SPECTATING_A_HOT_TARGET() AND GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(serverBD.niVeh), VS_DRIVER) = GET_SPECTATOR_CURRENT_FOCUS_PED())
						IF Is_This_The_Current_Objective_Text("HTV_GDEL2")
							Clear_This_Objective_Text("HTV_GDEL2")
						ENDIF
						IF NOT Is_There_Any_Current_Objective_Text_From_This_Script()
							Print_Objective_Text_With_String("HTV_GDEL", GET_VEHICLE_TYPE_STRING())
						ENDIF
					ELIF (IS_PED_IN_VEHICLE_GUNNER_POSITION(PLAYER_PED_ID())
					OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Gunner))
					OR (AM_I_SPECTATING_A_HOT_TARGET() AND IS_PED_IN_VEHICLE_GUNNER_POSITION(GET_SPECTATOR_CURRENT_FOCUS_PED()))
						IF NOT Is_There_Any_Current_Objective_Text_From_This_Script()
							Print_Objective_Text_With_String("HTV_GDEL2", GET_VEHICLE_TYPE_STRING())
						ENDIF
					ENDIF
				ELSE
					CLEAR_DELIVERY_OBJECTIVE_TEXT()
				ENDIF
			ELSE
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh))
				OR IS_SPECTATOR_FOCUS_PED_IN_HOT_TARGET_VEHICLE()
					IF GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(serverBD.niVeh), VS_DRIVER) = PLAYER_PED_ID()
					OR (AM_I_SPECTATING_A_HOT_TARGET() AND GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(serverBD.niVeh), VS_DRIVER) = GET_SPECTATOR_CURRENT_FOCUS_PED())
						IF NOT Is_There_Any_Current_Objective_Text_From_This_Script()
							Print_Objective_Text_With_String("HTV_GDEL", GET_VEHICLE_TYPE_STRING())
						ENDIF
					ENDIF
				ELSE
					CLEAR_DELIVERY_OBJECTIVE_TEXT()
				ENDIF
			ENDIF
		ELSE
			CLEAR_DELIVERY_OBJECTIVE_TEXT()
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintains the objective text shown to non-Hot Target participants
PROC MAINTAIN_KILL_OBJECTIVE_TEXT()
	CLEAR_DELIVERY_OBJECTIVE_TEXT()
	IF HAVE_PLAYERS_ENTERED_TO_START_EVENT()		
		IF AM_I_PARTICIPATING_IN_EVENT()
			IF NOT IS_PLAYER_SCTV(PLAYER_ID())
				IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
					IF (serverBD.piDriver != INVALID_PLAYER_INDEX() AND NOT HAS_DRIVER_BEEN_KILLED())
					AND (serverBD.piGunner != INVALID_PLAYER_INDEX() AND NOT HAS_GUNNER_BEEN_KILLED())
						IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piDriver)
						AND NETWORK_IS_PLAYER_ACTIVE(serverBD.piGunner)
						AND IS_PED_IN_VEHICLE(GET_PLAYER_PED(serverBD.piDriver), NET_TO_VEH(serverBD.niVeh))
						AND IS_PED_IN_VEHICLE(GET_PLAYER_PED(serverBD.piGunner), NET_TO_VEH(serverBD.niVeh))
						
							IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
								IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(serverBD.piDriver, GB_GET_LOCAL_PLAYER_GANG_BOSS())
								AND GB_IS_PLAYER_MEMBER_OF_THIS_GANG(serverBD.piGunner, GB_GET_LOCAL_PLAYER_GANG_BOSS())
									// Both goons are in my gang
									IF NOT Has_This_MP_Objective_Text_With_Two_User_Created_Strings_Been_Received("HPV_PERMHLDR2b", GET_PLAYER_NAME(serverBD.piDriver), GET_PLAYER_NAME(serverBD.piGunner))
										Print_Objective_Text_With_Two_User_Created_Strings_And_String("HPV_PERMHLDR2b", GET_PLAYER_NAME(serverBD.piDriver), GET_PLAYER_NAME(serverBD.piGunner), "HPV_MTARG2", GET_PLAYER_HUD_COLOUR(serverBD.piDriver))
									ENDIF
								ELIF GB_GET_LOCAL_PLAYER_GANG_BOSS() = serverBD.piDriver
								AND NOT GB_IS_PLAYER_MEMBER_OF_THIS_GANG(serverBD.piGunner, GB_GET_LOCAL_PLAYER_GANG_BOSS())
									// Driver is the boss of my gang, gunner is not in my gang
									Print_Objective_Text_With_Player_Name_And_String_Coloured("HPV_PERMHLDRc", serverBD.piDriver, "HPV_MTARG2", DEFAULT, GET_PLAYER_HUD_COLOUR(serverBD.piDriver))
								ELIF GB_GET_LOCAL_PLAYER_GANG_BOSS() = serverBD.piGunner
								AND NOT GB_IS_PLAYER_MEMBER_OF_THIS_GANG(serverBD.piDriver, GB_GET_LOCAL_PLAYER_GANG_BOSS())
									// Gunner is the boss of my gang, driver is not in my gang
									Print_Objective_Text_With_Player_Name_And_String_Coloured("HPV_PERMHLDRc", serverBD.piGunner, "HPV_MTARG2", DEFAULT, GET_PLAYER_HUD_COLOUR(serverBD.piGunner))
								ELSE
									// Kill them both
									IF NOT Has_This_MP_Objective_Text_With_Two_User_Created_Strings_Been_Received("HPV_PERMHLDR2", GET_PLAYER_NAME(serverBD.piDriver), GET_PLAYER_NAME(serverBD.piGunner))
										Print_Objective_Text_With_Two_User_Created_Strings("HPV_PERMHLDR2", GET_PLAYER_NAME(serverBD.piDriver), GET_PLAYER_NAME(serverBD.piGunner))
									ENDIF
								ENDIF
							ELSE
								IF NOT Has_This_MP_Objective_Text_With_Two_User_Created_Strings_Been_Received("HPV_PERMHLDR2", GET_PLAYER_NAME(serverBD.piDriver), GET_PLAYER_NAME(serverBD.piGunner))
									Print_Objective_Text_With_Two_User_Created_Strings("HPV_PERMHLDR2", GET_PLAYER_NAME(serverBD.piDriver), GET_PLAYER_NAME(serverBD.piGunner))
								ENDIF
							ENDIF
						ENDIF
					ELIF (serverBD.piDriver != INVALID_PLAYER_INDEX() OR NOT HAS_DRIVER_BEEN_KILLED())
					AND (serverBD.piGunner = INVALID_PLAYER_INDEX() OR HAS_GUNNER_BEEN_KILLED())
						IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piDriver)
						AND IS_PED_IN_VEHICLE(GET_PLAYER_PED(serverBD.piDriver), NET_TO_VEH(serverBD.niVeh))
						
							IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
								IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(serverBD.piDriver, GB_GET_LOCAL_PLAYER_GANG_BOSS())
									Print_Objective_Text_With_Player_Name_And_String_Coloured("HPV_PERMHLDRb", serverBD.piDriver, "HPV_MTARG", DEFAULT, GET_PLAYER_HUD_COLOUR(serverBD.piDriver))
								ELSE
									Print_Objective_Text_With_User_Created_String("HPV_PERMHLDR", GET_PLAYER_NAME(serverBD.piDriver))
								ENDIF
							ELSE
								Print_Objective_Text_With_User_Created_String("HPV_PERMHLDR", GET_PLAYER_NAME(serverBD.piDriver))
							ENDIF
							
						ENDIF
					ELIF (serverBD.piDriver = INVALID_PLAYER_INDEX() OR HAS_DRIVER_BEEN_KILLED())
					AND (serverBD.piGunner != INVALID_PLAYER_INDEX() OR NOT HAS_GUNNER_BEEN_KILLED())
						IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piGunner)
						AND IS_PED_IN_VEHICLE(GET_PLAYER_PED(serverBD.piGunner), NET_TO_VEH(serverBD.niVeh))
						
							IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
								IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(serverBD.piGunner, GB_GET_LOCAL_PLAYER_GANG_BOSS())
									Print_Objective_Text_With_Player_Name_And_String_Coloured("HPV_PERMHLDRb", serverBD.piGunner, "HPV_MTARG", DEFAULT, GET_PLAYER_HUD_COLOUR(serverBD.piGunner))
								ELSE
									Print_Objective_Text_With_User_Created_String("HPV_PERMHLDR", GET_PLAYER_NAME(serverBD.piGunner))
								ENDIF
							ELSE
								Print_Objective_Text_With_User_Created_String("HPV_PERMHLDR", GET_PLAYER_NAME(serverBD.piGunner))
							ENDIF
							
						ENDIF
					ELSE
						CLEAR_KILL_OBJECTIVE_TEXT()
					ENDIF
				ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_DriverLeft)
					IF serverBD.piGunner != INVALID_PLAYER_INDEX()
						IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piGunner)
						
							IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
								IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(serverBD.piGunner, GB_GET_LOCAL_PLAYER_GANG_BOSS())
									Print_Objective_Text_With_Player_Name_And_String_Coloured("HPV_PERMHLDRb", serverBD.piGunner, "HPV_MTARG", DEFAULT, GET_PLAYER_HUD_COLOUR(serverBD.piGunner))
								ELSE
									Print_Objective_Text_With_User_Created_String("HPV_PERMHLDR", GET_PLAYER_NAME(serverBD.piGunner))
								ENDIF
							ELSE
								Print_Objective_Text_With_User_Created_String("HPV_PERMHLDR", GET_PLAYER_NAME(serverBD.piGunner))
							ENDIF
							
						ELSE
							CLEAR_KILL_OBJECTIVE_TEXT()
						ENDIF
					ELSE
						CLEAR_KILL_OBJECTIVE_TEXT()
					ENDIF
				ELSE
					IF serverBD.piDriver != INVALID_PLAYER_INDEX()
						IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piDriver)
						
							IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
								IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(serverBD.piDriver, GB_GET_LOCAL_PLAYER_GANG_BOSS())
									Print_Objective_Text_With_Player_Name_And_String_Coloured("HPV_PERMHLDRb", serverBD.piDriver, "HPV_MTARG", DEFAULT, GET_PLAYER_HUD_COLOUR(serverBD.piDriver))
								ELSE
									Print_Objective_Text_With_User_Created_String("HPV_PERMHLDR", GET_PLAYER_NAME(serverBD.piDriver))
								ENDIF
							ELSE
								Print_Objective_Text_With_User_Created_String("HPV_PERMHLDR", GET_PLAYER_NAME(serverBD.piDriver))
							ENDIF
							
						ELSE
							CLEAR_KILL_OBJECTIVE_TEXT()
						ENDIF
					ELSE
						CLEAR_KILL_OBJECTIVE_TEXT()
					ENDIF
				ENDIF
			ELSE
				CLEAR_KILL_OBJECTIVE_TEXT()
			ENDIF
		ELSE
			CLEAR_KILL_OBJECTIVE_TEXT()
		ENDIF
	ELSE
		CLEAR_KILL_OBJECTIVE_TEXT()
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if the current help message is from the current script
/// RETURNS:
///    
FUNC BOOL IS_HELP_MESSAGE_FROM_THIS_SCRIPT_BEING_DISPLAYED()

	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HTV_HELP0")
	OR IS_THIS_HELP_MESSAGE_WITH_STRING_AND_NUMBER_BEING_DISPLAYED("HTV_HELP1", GET_VEH_BLIP_STRING(), GET_CASH_REWARD(eHT_CASH_RP_DELIVERER, TRUE))
	OR IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED("HTV_HELP2", GET_VEHICLE_TYPE_STRING())
	OR IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED("HTV_HELP2b", GET_VEHICLE_TYPE_STRING())
	OR IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED("HTV_HELP4", GET_VEHICLE_TYPE_STRING())
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HTV_HELP5")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HTV_HELP6")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HTV_HELP6b")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HTV_HELP7")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HTV_HELP7b")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HTV_HELP9")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HTV_HELP9b")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HTV_HELP10")
	OR IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED("HTV_HELP11", GET_VEHICLE_TYPE_STRING())
	OR IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED("HTV_HELP12", GET_VEHICLE_TYPE_STRING())
	OR IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED("HTV_HELP12b", GET_VEHICLE_TYPE_STRING())
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HTV_HELP13")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HTV_HELP13b")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HTV_HELP14")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HTV_HELP14b")
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Maintain the event UI and blips
PROC MAINTAIN_BLIPS_AND_UI()

	SWITCH playerBD[PARTICIPANT_ID_TO_INT()].eStage
	
		CASE eAD_IDLE
			IF SHOULD_UI_BE_HIDDEN()
				IF IS_HELP_MESSAGE_FROM_THIS_SCRIPT_BEING_DISPLAYED()
					CLEAR_HELP()
				ENDIF
				REMOVE_BLIPS()
			ELSE
				ADD_VEH_BLIP()
			ENDIF
		BREAK
		
		CASE eAD_DELIVER
			IF SHOULD_UI_BE_HIDDEN()
				IF IS_HELP_MESSAGE_FROM_THIS_SCRIPT_BEING_DISPLAYED()
					CLEAR_HELP()
				ENDIF
				REMOVE_BLIPS()
			ELSE
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
					IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh))
						ADD_DELIVERY_BLIP()
						MAINTAIN_DELIVERY_OBJECTIVE_TEXT()
					ELSE
						ADD_VEH_BLIP(FALSE)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE eAD_KILL
			IF AM_I_SPECTATING_A_HOT_TARGET()
				IF IS_A_SPECTATOR_CAM_RUNNING()
				AND NOT IS_SPECTATOR_HUD_HIDDEN()
					IF DOES_BLIP_EXIST(PlayerBlip)
						REMOVE_BLIP(PlayerBlip)
					ENDIF
					ADD_DELIVERY_BLIP()
					MAINTAIN_DELIVERY_OBJECTIVE_TEXT()
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(DeliveryBlip)
					REMOVE_BLIP(DeliveryBlip)
				ENDIF
				IF SHOULD_UI_BE_HIDDEN()
					IF IS_HELP_MESSAGE_FROM_THIS_SCRIPT_BEING_DISPLAYED()
						CLEAR_HELP()
					ENDIF
					REMOVE_BLIPS()
				ELSE
					ADD_PLAYER_BLIP()
					MAINTAIN_PLAYER_BLIP_COLOUR()
					MAINTAIN_KILL_OBJECTIVE_TEXT()
				ENDIF
			ENDIF
		BREAK
		
		CASE eAD_OFF_MISSION
			IF AM_I_SPECTATING_A_HOT_TARGET()
				IF IS_A_SPECTATOR_CAM_RUNNING()
				AND NOT IS_SPECTATOR_HUD_HIDDEN()
					IF DOES_BLIP_EXIST(PlayerBlip)
						REMOVE_BLIP(PlayerBlip)
					ENDIF
					ADD_DELIVERY_BLIP()
					MAINTAIN_DELIVERY_OBJECTIVE_TEXT()
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(DeliveryBlip)
					REMOVE_BLIP(DeliveryBlip)
				ENDIF
				IF SHOULD_UI_BE_HIDDEN()
					IF IS_HELP_MESSAGE_FROM_THIS_SCRIPT_BEING_DISPLAYED()
						CLEAR_HELP()
					ENDIF
					REMOVE_BLIPS()
				ELSE
					ADD_PLAYER_BLIP()
					MAINTAIN_PLAYER_BLIP_COLOUR()
				ENDIF
			ENDIF
		BREAK
	
	ENDSWITCH

ENDPROC

PROC MAINTAIN_QUALIFICATION_CHECKS()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WithinQualifyDistance)
			IF HAVE_PLAYERS_ENTERED_TO_START_EVENT()
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.niVeh)), FALSE) < TO_FLOAT(GET_HOT_TARGET_VEH_TYPE_PARTICIPATE_RANGE())
					IF NOT HAS_NET_TIMER_STARTED(ParticipationTimer)
						START_NET_TIMER(ParticipationTimer)
					ENDIF
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WithinQualifyDistance)
					PRINTLN("[AM MOVING TARGET] - MAINTAIN_QUALIFICATION_CHECKS - Player has gotten within 100m of the package, player now qualifies for min participation reward.")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_LOCAL_PLAYER_PARTICIPATION()
	FLOAT fDistance
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niVeh)
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ParticipantViaDistance)
			fDistance = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), NET_TO_OBJ(serverBD.niVeh))
			IF fDistance > g_sMPTunables.iFREEMODE_EVENT_UI_EXIT_DISTANCE
				CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ParticipantViaDistance)
				CPRINTLN(DEBUG_AMBIENT, "[AM MOVING TARGET] - MAINTAIN_LOCAL_PLAYER_PARTICIPATION biP_ParticipantViaDistance has been cleared.")
			ENDIF
		ELSE
			fDistance = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), NET_TO_OBJ(serverBD.niVeh))
			IF fDistance <= g_sMPTunables.iFREEMODE_EVENT_UI_ENTRY_DISTANCE
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ParticipantViaDistance)
				CPRINTLN(DEBUG_AMBIENT, "[AM MOVING TARGET] - MAINTAIN_LOCAL_PLAYER_PARTICIPATION biP_ParticipantViaDistance has been set.")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_DRIVER_OR_GUNNER_CRITICAL_WHILE_HIDDEN()

	IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
		IF serverBD.piDriver != INVALID_PLAYER_INDEX()
			IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piDriver)
				IF IS_BIT_SET(playerBD[NATIVE_TO_INT(serverBD.piDriver)].iPlayerBitSet, biP_PlayerIsCriticalWhileHidden)
					PRINTLN("[AM MOVING TARGET] - IS_DRIVER_OR_GUNNER_CRITICAL_WHILE_HIDDEN - TRUE, driver is critical with events hidden")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		IF serverBD.piGunner != INVALID_PLAYER_INDEX()
			IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piGunner)
				IF IS_BIT_SET(playerBD[NATIVE_TO_INT(serverBD.piGunner)].iPlayerBitSet, biP_PlayerIsCriticalWhileHidden)
					PRINTLN("[AM MOVING TARGET] - IS_DRIVER_OR_GUNNER_CRITICAL_WHILE_HIDDEN - TRUE, gunner is critical with events hidden.")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF serverBD.piDriver != INVALID_PLAYER_INDEX()
			IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piDriver)
				IF IS_BIT_SET(playerBD[NATIVE_TO_INT(serverBD.piDriver)].iPlayerBitSet, biP_PlayerIsCriticalWhileHidden)
					PRINTLN("[AM MOVING TARGET] - IS_DRIVER_OR_GUNNER_CRITICAL_WHILE_HIDDEN - TRUE, driver is critical with events hidden")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Checks to see if we should do the countdown before exploding the vehicle
FUNC BOOL DO_EXPLODE_TIMER()

	// Explode immediately
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_ExplodeImmediately)
		RETURN FALSE
	ENDIF

	// Run the explosion timer to ensure driver doesnt get killed without notification.
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_MinPlayersBailServer)
		RETURN TRUE
	ENDIF

	// Vehicle delivered, definitely run timer to give deliverer time to escape.
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_VehicleDelivered)		
		RETURN TRUE
	ENDIF

	IF IS_BIT_SET(serverBD.iServerBitSet, biS_HotTargetsLeft)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_MissionEndTimerHit)
		RETURN TRUE
	ENDIF
	
	IF HAVE_HOT_TARGETS_BEEN_KILLED()
		RETURN TRUE
	ENDIF
	
	IF ARE_HOT_TARGETS_OFF_MISSION()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Controls the explode vehicle stage
PROC MAINTAIN_EXPLODE_VEH()
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niVeh)
		IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh))
			IF DO_EXPLODE_TIMER()
				IF HAS_NET_TIMER_STARTED(serverBD.ExplodeVehTimer)
					INT iTime = (g_sMPTunables.iVehicleExplosionTime - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.ExplodeVehTimer))
					IF iTime >= 0
						IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh), <<VEH_EXP_RANGE, VEH_EXP_RANGE, VEH_EXP_RANGE>>)
							IF NOT SHOULD_UI_BE_HIDDEN()
							AND AM_I_A_HOT_TARGET()
								DRAW_GENERIC_TIMER(iTime, "HTV_DESTR_R", 0, TIMER_STYLE_USEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_DONTCARE, DEFAULT, HUD_COLOUR_RED)
								SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
							ENDIF
						ENDIF
					ELSE
						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niVeh)
							SET_ENTITY_CAN_BE_DAMAGED(NET_TO_VEH(serverBD.niVeh), TRUE)
							NETWORK_EXPLODE_VEHICLE(NET_TO_VEH(serverBD.niVeh))
							PRINTLN("[AM MOVING TARGET] - EXPLODE THE VEHICLE - DONE - A")
						ENDIF
						IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh), <<VEH_EXP_RANGE, VEH_EXP_RANGE, VEH_EXP_RANGE>>)
							IF NOT SHOULD_UI_BE_HIDDEN()
							AND AM_I_A_HOT_TARGET()
								DRAW_GENERIC_TIMER(0, "HTV_DESTR_R", 0, TIMER_STYLE_USEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_DONTCARE, DEFAULT, HUD_COLOUR_RED)
								SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
							ENDIF
						ENDIF
					ENDIF
					IF bStartSound = FALSE
						IF IS_ANY_FM_EVENTS_BIG_MESSAGE_BEING_DISPLAYED()
							IF NOT HAS_NET_TIMER_STARTED(ExplosionSoundDelay)
								START_NET_TIMER(ExplosionSoundDelay)
							ELSE
								IF HAS_NET_TIMER_EXPIRED(ExplosionSoundDelay, 1000)
									iExplosionSound = GET_SOUND_ID()
									PLAY_SOUND_FROM_ENTITY(iExplosionSound,"Explosion_Countdown",NET_TO_VEH(serverBD.niVeh),"GTAO_FM_Events_Soundset", FALSE, 25)
									SET_VARIABLE_ON_SOUND(iExplosionSound,"Time",TO_FLOAT((iTime/1000) + 1))
									bStartSound = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niVeh)
					SET_ENTITY_CAN_BE_DAMAGED(NET_TO_VEH(serverBD.niVeh), TRUE)
					NETWORK_EXPLODE_VEHICLE(NET_TO_VEH(serverBD.niVeh))
					PRINTLN("[AM MOVING TARGET] - EXPLODE THE VEHICLE - DONE - B")
				ENDIF
			ENDIF
		ELSE
			IF bStartSound
				IF NOT HAS_SOUND_FINISHED(iExplosionSound)
					STOP_SOUND(iExplosionSound)
					RELEASE_SOUND_ID(iExplosionSound)
				ENDIF
				bStartSound = FALSE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_DISPLAY_MT_LBD()

	IF playerBD[PARTICIPANT_ID_TO_INT()].eStage != eAD_END
	AND playerBD[PARTICIPANT_ID_TO_INT()].eStage != eAD_CLEANUP
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_SAFE_LOCATION_CHECKS()
	IF SHOULD_FM_EVENT_NET_ENTITY_WARP_TO_SAFE_LOCATION()
		HANDLE_WARP_FM_EVENT_NET_ENTITY_TO_SAFE_LOCATION(serverBD.niVeh)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_EVENT_START()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
		IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
			IF serverBD.piDriver != INVALID_PLAYER_INDEX()
			AND serverBD.piGunner != INVALID_PLAYER_INDEX()
				IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piDriver)
				AND NETWORK_IS_PLAYER_ACTIVE(serverBD.piGunner)
					PED_INDEX driverPed = GET_PLAYER_PED(serverBD.piDriver)
					PED_INDEX gunnerPed = GET_PLAYER_PED(serverBD.piGunner)
					IF IS_PED_IN_THIS_VEHICLE(driverPed, NET_TO_VEH(serverBD.niVeh), FALSE)
					AND IS_PED_IN_THIS_VEHICLE(gunnerPed, NET_TO_VEH(serverBD.niVeh), FALSE)
						IF GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(serverBD.niVeh), VS_DRIVER) = driverPed
						AND IS_PED_IN_VEHICLE_GUNNER_POSITION(gunnerPed)
							PRINTLN("[AM MOVING TARGET] - SHOULD_EVENT_START - TWO PERSON VEHICLE - TRUE")
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF		
			ENDIF		
		ELSE
			IF serverBD.piDriver != INVALID_PLAYER_INDEX()
				IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piDriver)
					PED_INDEX driverPed = GET_PLAYER_PED(serverBD.piDriver)
					IF IS_PED_IN_THIS_VEHICLE(driverPed, NET_TO_VEH(serverBD.niVeh), FALSE)
						IF GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(serverBD.niVeh), VS_DRIVER) = driverPed
							PRINTLN("[AM MOVING TARGET] - SHOULD_EVENT_START - ONE PERSON VEHICLE - TRUE")
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF		
			ENDIF		
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_CLOSE_PI_MENU_CHECKS()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
		IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh), TRUE)
			IF NOT FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT(PLAYER_ID())
				IF IS_INTERACTION_MENU_OPEN()
					PRINTLN("[AM MOVING TARGET] - MAINTAIN_CLOSE_PI_MENU_CHECKS - IS_INTERACTION_MENU_OPEN - killing and disabling interaction menu")
					KILL_INTERACTION_MENU()
					DISABLE_INTERACTION_MENU()
				ENDIF
				SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.niVeh), PLAYER_ID(), FALSE)
				PRINTLN("[AM MOVING TARGET] - MAINTAIN_CLOSE_PI_MENU_CHECKS - SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(veh, PLAYER_ID(), FALSE)")
				IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID())
					PRINTLN("[AM MOVING TARGET] - MAINTAIN_CLOSE_PI_MENU_CHECKS - Player is in Hot Target vehicle and is passive, disabling passive mode and clearing restriction.")
					DISABLE_MP_PASSIVE_MODE(PMER_START_OF_A_MATCH, FALSE)
					CLEAR_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iFmAmbientEventBitSet, ciSET_PLAYER_IS_RESTRICTED_WITH_PASSIVE)
				ENDIF
			ELSE
				IF IS_INTERACTION_MENU_DISABLED()
					PRINTLN("[AM MOVING TARGET] - MAINTAIN_CLOSE_PI_MENU_CHECKS - IS_INTERACTION_MENU_DISABLED - enabling as player is now critical")
					ENABLE_INTERACTION_MENU()
				ENDIF
			ENDIF
		ELSE
			IF IS_INTERACTION_MENU_DISABLED()
				PRINTLN("[AM MOVING TARGET] - MAINTAIN_CLOSE_PI_MENU_CHECKS - IS_INTERACTION_MENU_DISABLED - enabling as player is not in the vehicle")
				ENABLE_INTERACTION_MENU()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Process the JOYRIDER stages for the Client
PROC PROCESS_HOT_TARGET_CLIENT()

	MAINTAIN_CLOSE_PI_MENU_CHECKS()
	MAINTAIN_HOT_TARGET_EVENT_TIMER()
	HANDLE_VEHICLE_IN_SHOP()
	MAINTAIN_PASSIVE_MODE_CHECKS()
	MAINTAIN_BLIPS_AND_UI()
	MAINTAIN_VEHICLE_LOCKING_AND_INVINCIBILITY()
	MAINTAIN_QUALIFICATION_CHECKS()
	MAINTAIN_LOCAL_PLAYER_PARTICIPATION()
	MAINTAIN_SAFE_LOCATION_CHECKS()
	
	
//	IF SHOULD_DISPLAY_MT_LBD()
		MAINTAIN_DPAD_DOWN_LEADERBOARD()
//	ENDIF
	
	 //#IF FEATURE_NEW_AMBIENT_EVENTS
	
	SWITCH playerBD[PARTICIPANT_ID_TO_INT()].eStage		
		CASE eAD_IDLE
			CONTROL_VEH_BODY()
		
			//Check if we should become Deliverer or Destroyer
			IF SHOULD_EVENT_START()
				//common event proccess pre game
				COMMON_FREEMODE_AMBIENT_EVENTS_SET_UP(FMMC_TYPE_MOVING_TARGET, 0, 0.25)
				FM_EVENT_SET_PLAYER_WAITING_FOR_FM_EVENT_TO_START(FALSE)
				IF serverBD.piDriver = PLAYER_ID()
				OR serverBD.piGunner = PLAYER_ID()
					IF NOT HAS_NET_TIMER_STARTED(ParticipationTimer)
						START_NET_TIMER(ParticipationTimer)
					ENDIF
					IF NOT AM_I_RESTRICTED()
						FM_EVENT_SET_PLAYER_IS_PERMANENT_PARTICIPANT_IN_FM_EVENT(TRUE)
						FM_EVENT_SET_PLAYER_IS_CRITICAL_TO_FM_EVENT(TRUE)
					ENDIF
					BLOCK_BLIPS(TRUE)
					IF NOT IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iFmAmbientEventBitSet, ciSET_PLAYER_IN_MOVING_TARGET_VEHICLE)
						SET_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iFmAmbientEventBitSet, ciSET_PLAYER_IN_MOVING_TARGET_VEHICLE)
					ENDIF
					IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
					AND serverBD.piGunner != INVALID_PLAYER_INDEX()
						DO_BIG_MESSAGE(1)
					ELSE
						DO_BIG_MESSAGE(0)
					ENDIF
					SET_FREEMODE_FLOW_COMMS_DISABLED(TRUE)
					IF FM_EVENT_HAS_PLAYER_HAS_BLOCKED_CURRENT_FM_EVENT(PLAYER_ID())
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_PlayerIsCriticalWhileHidden)
						NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  biP_PlayerIsCriticalWhileHidden   <----------     ") NET_NL()
					ELSE
						playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_DELIVER
						NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  PLAYER STAGE = eAD_DELIVER    <----------     ") NET_NL()
					ENDIF
				ELSE
					IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Driver)
						CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Driver)
					ENDIF
					IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Gunner)
						CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Gunner)
					ENDIF
					IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID())
						playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_OFF_MISSION
						PRINTLN("[AM MOVING TARGET] -  PLAYER STAGE = eAD_OFF_MISSION (Passive Enabled)    <----------     ")
					ELSE
						playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_KILL
						PRINTLN("[AM MOVING TARGET] -  PLAYER STAGE = eAD_KILL    <----------     ")
					ENDIF					
				ENDIF
			ELSE
				DRAW_MARKER_FOR_HOT_TARGET_VEHICLE(TRUE)
				FM_EVENT_SET_PLAYER_WAITING_FOR_FM_EVENT_TO_START(TRUE)
				DO_HELP_TEXT(1)
			ENDIF
			
			IF serverBD.eStage = eAD_END
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_END
				NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  PLAYER STAGE = eAD_END    <----------     ") NET_NL()
			ELIF serverBD.eStage = eAD_CLEANUP
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CLEANUP
				NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  PLAYER STAGE = eAD_CLEANUP    <----------     ") NET_NL()
			ENDIF
		BREAK
		
		CASE eAD_DELIVER
			CONTROL_VEH_BODY()
			HANDLE_OBJECTIVE_IN_GARAGE(serverBD.niVeh, iGarageCheckTimer, iGarageSyncDelayTimer, TRUE)
			MAINTAIN_SPAWN_LOCATIONS()
			DO_HELP_TEXT(2)
			IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
				CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_FM_EVENT_START)
				CLEAR_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iFmAmbientEventBitSet, ciSET_PLAYER_IN_MOVING_TARGET_VEHICLE)
				Clear_Any_Objective_Text_From_This_Script()
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_OFF_MISSION
				NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  PLAYER STAGE = eAD_OFF_MISSION - DEAD    <----------     ") NET_NL()
			ENDIF
			IF serverBD.eStage = eAD_END
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_END
				NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  PLAYER STAGE = eAD_END    <----------     ") NET_NL()
			ELIF serverBD.eStage = eAD_CLEANUP
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CLEANUP
				NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  PLAYER STAGE = eAD_CLEANUP    <----------     ") NET_NL()
			ENDIF
		BREAK
		
		CASE eAD_KILL
			PROCESS_HOT_TARGET_DAMAGE_EVENTS()
			MAINTAIN_VEHICLE_MARKER()
			CONTROL_VEH_BODY()
			IF serverBD.eStage != eAD_END
			AND serverBD.eStage != eAD_CLEANUP
				IF AM_I_SPECTATING_A_HOT_TARGET()
					IF IS_A_SPECTATOR_CAM_RUNNING()
					AND NOT IS_SPECTATOR_HUD_HIDDEN()
						IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
						AND serverBD.piGunner != INVALID_PLAYER_INDEX()
							DO_BIG_MESSAGE(1)
						ELSE
							DO_BIG_MESSAGE(0)
						ENDIF
					ENDIF
				ELSE
					IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
					AND serverBD.piGunner != INVALID_PLAYER_INDEX()
						DO_BIG_MESSAGE(3)
					ELSE
						DO_BIG_MESSAGE(2)
					ENDIF
				ENDIF
			ENDIF
			IF serverBD.eStage = eAD_END
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_END
				NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  PLAYER STAGE = eAD_END    <----------     ") NET_NL()
			ELIF serverBD.eStage = eAD_CLEANUP
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CLEANUP
				NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  PLAYER STAGE = eAD_CLEANUP    <----------     ") NET_NL()
			ENDIF
		BREAK
		
		CASE eAD_OFF_MISSION
			IF DOES_BLIP_EXIST(DeliveryBlip)
				REMOVE_BLIP(DeliveryBlip)
			ENDIF
			IF AM_I_A_HOT_TARGET()
				IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE)
					IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_OutOfVehicleFail)
						DO_HELP_TEXT(11)
					ELSE
						IF (AM_I_THE_DRIVER() AND IS_BIT_SET(serverBD.iServerBitSet, biS_DriverKilled))
						OR (AM_I_THE_GUNNER() AND IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerKilled))
							DO_HELP_TEXT(4)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID())
					DO_HELP_TEXT(10)
				ENDIF
			ENDIF
			IF NOT SHOULD_UI_BE_HIDDEN()
				DRAW_MARKER_FOR_HOT_TARGET_VEHICLE()
			ENDIF
			IF serverBD.eStage = eAD_END
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_END
				NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  PLAYER STAGE = eAD_END    <----------     ") NET_NL()
			ELIF serverBD.eStage = eAD_CLEANUP
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CLEANUP
				NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  PLAYER STAGE = eAD_CLEANUP    <----------     ") NET_NL()
			ENDIF
		BREAK
		
		CASE eAD_END
//			IF MAINTAIN_FM_EVENTS_END_UI(sEndUiVars, FALSE)
//				// Call to maintain to manage shard timers - not needed for cleanup as this is managed by server
//			ENDIF
			KILL_AMBIENT_EVENT_COUNTDOWN_AUDIO()
			IF IS_HELP_MESSAGE_FROM_THIS_SCRIPT_BEING_DISPLAYED()
				CLEAR_HELP()
			ENDIF
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_MissionTimedOut)
			OR IS_BIT_SET(serverBD.iServerBitSet, biS_VehicleDestroyedBeforeStart)
			OR IS_DRIVER_OR_GUNNER_CRITICAL_WHILE_HIDDEN()
				IF NOT IS_BIT_SET(iBoolsBitSet3, biEndTickerDone)
					IF IS_NET_PLAYER_OK(PLAYER_ID())
						PRINT_TICKER("HTV_TCNLA")
						SET_BIT(iBoolsBitSet3, biEndTickerDone)
					ENDIF
				ENDIF
				REMOVE_BLIPS()
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
					IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh))
						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niVeh)
							SET_ENTITY_CAN_BE_DAMAGED(NET_TO_VEH(serverBD.niVeh), TRUE)
							NETWORK_EXPLODE_VEHICLE(NET_TO_VEH(serverBD.niVeh))
							PRINTLN("[AM MOVING TARGET] - biS_MissionTimedOut SET, destroying vehicle")
						ENDIF
					ENDIF
				ENDIF
				FM_EVENT_SET_PLAYER_IS_CRITICAL_TO_FM_EVENT(FALSE)
			ELSE
				REMOVE_BLIPS()
				MAINTAIN_EXPLODE_VEH()
				PROCESS_EVENT_END_REWARDS_AND_MESSAGES()
			ENDIF
			IF serverBD.eStage = eAD_CLEANUP
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CLEANUP
				NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  PLAYER STAGE = eAD_CLEANUP    <----------     ") NET_NL()
			ENDIF
		BREAK
		
		CASE eAD_CLEANUP
			NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  SCRIPT CLEANUP B     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		BREAK
	ENDSWITCH
ENDPROC

PROC MAINTAIN_TIMEOUT_TIMER()
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_StartTimer)
		// Run 10 minute event timeout
		IF NOT HAS_NET_TIMER_STARTED(serverBD.MissionTimeoutTimer)
			IF NOT HAVE_PLAYERS_ENTERED_TO_START_EVENT()
				START_NET_TIMER(serverBD.MissionTimeoutTimer)
				PRINTLN("[AM MOVING TARGET] MissionTimeoutTimer has started ")
			ENDIF
		ELSE
			IF HAVE_PLAYERS_ENTERED_TO_START_EVENT()
				RESET_NET_TIMER(serverBD.MissionTimeoutTimer)
				SET_BIT(serverBD.iServerBitSet, biS_StartTimer)
				PRINTLN("[AM MOVING TARGET] - biS_StartTimer SET")
				PRINTLN("[AM MOVING TARGET] MissionTimeoutTimer has been reset as HAVE_PLAYERS_ENTERED_TO_START_EVENT is TRUE")
			ELSE
				// Vehicle has blown up before event started
				IF DOES_ENTITY_EXIST(NET_TO_VEH(serverBD.niVeh))
					IF IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh))
					OR IS_HOT_TARGET_UNDRIVEABLE()
						PRINTLN("[AM MOVING TARGET] -  PROCESS_HOT_TARGET_SERVER - IS_ENTITY_DEAD or IS_HOT_TARGET_UNDRIVEABLE")
						SET_BIT(serverBD.iServerBitSet, biS_VehicleDestroyedBeforeStart)
					ENDIF
				ENDIF
			
				IF HAS_NET_TIMER_EXPIRED(serverbd.MissionTimeoutTimer, g_sMPTunables.iHot_Target_Event_Expiry_Time)
					SET_BIT(serverBD.iServerBitSet, biS_MissionTimedOut)
					SET_BIT(serverBD.iServerBitSet, biS_EventEnded)
					PRINTLN("[AM MOVING TARGET] - MissionTimeoutTimer has expired - biS_MissionTimedOut SET   <----------     ")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_SERVER_EVENT_TIMER()
	
	IF NOT HAS_NET_TIMER_STARTED(serverBD.DeliverVehicleTimer)
		IF IS_BIT_SET(serverBD.iServerBitSet, biS_StartTimer)
			START_NET_TIMER(serverBD.DeliverVehicleTimer)
		ENDIF
	ELSE
		IF iTimeRemaining <= 0
			PRINTLN("[AM MOVING TARGET] - iTimeRemaining <= 0, biS_MissionEndTimerHit SET")
			SET_BIT(serverBD.iServerBitSet, biS_MissionEndTimerHit)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_HOT_TARGET_ACTIVE_CHECKS()

	IF HAVE_PLAYERS_ENTERED_TO_START_EVENT()
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_VehicleDelivered)
		AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_MissionEndTimerHit)		
		AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_MissionTimedOut)
		AND NOT HAVE_HOT_TARGETS_BEEN_KILLED()
		AND NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh))
			
			// Check if the driver and/or gunner have left the session in progress.
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
				
				IF bDriverLeft = FALSE
					IF serverBD.piDriver != INVALID_PLAYER_INDEX()
						IF NOT NETWORK_IS_PLAYER_A_PARTICIPANT(serverBD.piDriver)
							bDriverLeft = TRUE
							PRINTLN("[AM MOVING TARGET] - MAINTAIN_HOT_TARGET_ACTIVE_CHECKS - Gunner Veh - bDriverLeft = TRUE")
						ENDIF
					ENDIF
				ENDIF
				
				IF bGunnerLeft = FALSE
					IF serverBD.piGunner != INVALID_PLAYER_INDEX()
						IF NOT NETWORK_IS_PLAYER_A_PARTICIPANT(serverBD.piGunner)
							bGunnerLeft = TRUE
							PRINTLN("[AM MOVING TARGET] - MAINTAIN_HOT_TARGET_ACTIVE_CHECKS - Gunner Veh - bGunnerLeft = TRUE")
						ENDIF
					ENDIF
				ENDIF
				
				IF bDriverLeft
				AND bGunnerLeft
					serverBD.piDriver = INVALID_PLAYER_INDEX()
					SET_BIT(serverBD.iServerBitSet, biS_HotTargetsLeft)
					PRINTLN("[AM MOVING TARGET] - MAINTAIN_HOT_TARGET_ACTIVE_CHECKS - Gunner Veh - biS_HotTargetsLeft SET")
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_DriverLeft)
					IF serverBD.piDriver != INVALID_PLAYER_INDEX()
						IF NOT NETWORK_IS_PLAYER_A_PARTICIPANT(serverBD.piDriver)
							SET_BIT(serverBD.iServerBitSet, biS_HotTargetsLeft)
							PRINTLN("[AM MOVING TARGET] - MAINTAIN_HOT_TARGET_ACTIVE_CHECKS - biS_HotTargetsLeft SET")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF	
	ENDIF

ENDPROC

FUNC BOOL SHOULD_MOVE_TO_EVENT_END()
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_VehicleStuck)
		PRINTLN("[AM MOVING TARGET] - SHOULD_MOVE_TO_EVENT_END - biS_VehicleStuck")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_MinPlayersBailServer)
		PRINTLN("[AM MOVING TARGET] - SHOULD_MOVE_TO_EVENT_END - biS_MinPlayersBailServer")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_VehicleDestroyedBeforeStart)
		PRINTLN("[AM MOVING TARGET] - SHOULD_MOVE_TO_EVENT_END - biS_VehicleDestroyedBeforeStart")
		RETURN TRUE
	ENDIF

	IF IS_BIT_SET(serverBD.iServerBitSet, biS_VehicleDelivered)
		PRINTLN("[AM MOVING TARGET] - SHOULD_MOVE_TO_EVENT_END - biS_VehicleDelivered")
		RETURN TRUE
	ENDIF
	
	IF HAVE_HOT_TARGETS_BEEN_KILLED()
		PRINTLN("[AM MOVING TARGET] - SHOULD_MOVE_TO_EVENT_END - HAVE_HOT_TARGETS_BEEN_KILLED")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_MissionEndTimerHit)
		PRINTLN("[AM MOVING TARGET] - SHOULD_MOVE_TO_EVENT_END - biS_MissionEndTimerHit")
		RETURN TRUE
	ENDIF
	
	IF IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh))
		PRINTLN("[AM MOVING TARGET] - SHOULD_MOVE_TO_EVENT_END - IS_ENTITY_DEAD(vehicle)")
		RETURN TRUE
	ENDIF
	
	IF IS_HOT_TARGET_UNDRIVEABLE()
		PRINTLN("[AM MOVING TARGET] - SHOULD_MOVE_TO_EVENT_END - IS_HOT_TARGET_UNDRIVEABLE")
		RETURN TRUE
	ENDIF
	
	IF ARE_HOT_TARGETS_OFF_MISSION()
		PRINTLN("[AM MOVING TARGET] - SHOULD_MOVE_TO_EVENT_END - ARE_HOT_TARGETS_OFF_MISSION")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_MissionTimedOut)
		PRINTLN("[AM MOVING TARGET] - SHOULD_MOVE_TO_EVENT_END - biS_MissionTimedOut")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_HotTargetsLeft)
		PRINTLN("[AM MOVING TARGET] - SHOULD_MOVE_TO_EVENT_END - biS_HotTargetsLeft")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_DriverLeft)
	AND IS_GUNNER_OFF_MISSION()
		PRINTLN("[AM MOVING TARGET] - SHOULD_MOVE_TO_EVENT_END - Driver left and gunner has been killed or is off-mission")
		RETURN TRUE
	ENDIF
		
		// Player has managed to enter the vehicle while hidden
	IF serverBD.piDriver != INVALID_PLAYER_INDEX()
		IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piDriver)
			IF IS_BIT_SET(playerBD[NATIVE_TO_INT(serverBD.piDriver)].iPlayerBitSet, biP_PlayerIsCriticalWhileHidden)
				PRINTLN("[AM MOVING TARGET] - SHOULD_MOVE_TO_EVENT_END - serverBD.piDriver - biP_PlayerIsCriticalWhileHidden")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF serverBD.piGunner != INVALID_PLAYER_INDEX()
		IF NETWORK_IS_PLAYER_ACTIVE(serverBD.piGunner)
			IF IS_BIT_SET(playerBD[NATIVE_TO_INT(serverBD.piGunner)].iPlayerBitSet, biP_PlayerIsCriticalWhileHidden)
				PRINTLN("[AM MOVING TARGET] - SHOULD_MOVE_TO_EVENT_END - serverBD.piGunner - biP_PlayerIsCriticalWhileHidden")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_BOSS_WORK_ABANDON_CHECKS()
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
		IF NATIVE_TO_INT(serverBD.piDriver) != -1
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(NATIVE_TO_INT(serverBD.piDriver)))
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_DriverLeft)
					IF IS_BIT_SET(playerBD[NATIVE_TO_INT(serverBD.piDriver)].iPlayerBitSet, biP_AbandonedForBossWork)
						PRINTLN("[AM MOVING TARGET] - PROCESS_HOT_TARGET_SERVER - Driver has abandoned vehicle for boss work, ending.")
						SET_BIT(serverBD.iServerBitSet, biS_DriverLeft)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF NATIVE_TO_INT(serverBD.piGunner) != -1
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(NATIVE_TO_INT(serverBD.piGunner)))
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerLeft)
					IF IS_BIT_SET(playerBD[NATIVE_TO_INT(serverBD.piGunner)].iPlayerBitSet, biP_AbandonedForBossWork)
						PRINTLN("[AM MOVING TARGET] - PROCESS_HOT_TARGET_SERVER - Gunner has abandoned vehicle for boss work, ending.")
						SET_BIT(serverBD.iServerBitSet, biS_GunnerLeft)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NATIVE_TO_INT(serverBD.piDriver) != -1
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(NATIVE_TO_INT(serverBD.piDriver)))
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_DriverLeft)
					IF IS_BIT_SET(playerBD[NATIVE_TO_INT(serverBD.piDriver)].iPlayerBitSet, biP_AbandonedForBossWork)
						PRINTLN("[AM MOVING TARGET] - PROCESS_HOT_TARGET_SERVER - Driver has abandoned vehicle for boss work, ending.")
						SET_BIT(serverBD.iServerBitSet, biS_HotTargetsLeft)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
ENDPROC

//PURPOSE: Process the JOYRIDER stages for the Server
PROC PROCESS_HOT_TARGET_SERVER()
	
	MAINTAIN_TIMEOUT_TIMER()
	MAINTAIN_SERVER_EVENT_TIMER()
	MAINTAIN_HOT_TARGET_ACTIVE_CHECKS()
	
	SWITCH serverBD.eStage		
	
		CASE eAD_IDLE
			// Checks to perform once the event has started
			IF NOT HAVE_PLAYERS_ENTERED_TO_START_EVENT()
				IF SHOULD_EVENT_START()
					IF IS_BIT_SET(serverBD.iServerBitSet, biS_WaitingForGunner)
						CLEAR_BIT(serverBD.iServerBitSet, biS_WaitingForGunner)
					ENDIF
					SET_BIT(serverBD.iServerBitSet, biS_EventStarted)
				ENDIF
			ELSE
			
				MAINTAIN_BOSS_WORK_ABANDON_CHECKS()
			
				// Check for vehicle being stuck
				IF IS_VEHICLE_STUCK()
					SET_BIT(serverBD.iServerBitSet, biS_VehicleStuck)
				ENDIF
				
				// Remove projectiles form area when players enter
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_VehicleCanBeKilled)
					CLEAR_AREA_OF_PROJECTILES(GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niVeh)), 30.0, TRUE)
					PRINTLN("[AM MOVING TARGET] -  PROCESS_HOT_TARGET_SERVER - players have entered, vehicle can now be killed. Clearing area of projectiles.")
					SET_BIT(serverBD.iServerBitSet, biS_VehicleCanBeKilled)
				ENDIF
				
				// Driver is no longer active
				IF NOT NETWORK_IS_PLAYER_ACTIVE(serverBD.piDriver)
					SET_BIT(serverBD.iServerBitSet, biS_DriverLeft)
					IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
						CLEAR_BIT(serverBD.iServerBitSet, biS_GunnerVehicle)
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
					// Driver is no longer active
					IF NOT NETWORK_IS_PLAYER_ACTIVE(serverBD.piGunner)
						SET_BIT(serverBD.iServerBitSet, biS_GunnerLeft)
						IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
							CLEAR_BIT(serverBD.iServerBitSet, biS_GunnerVehicle)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
			IF SHOULD_MOVE_TO_EVENT_END()
				START_NET_TIMER(serverBD.ExplodeVehTimer)
				SET_BIT(serverBD.iServerBitSet, biS_EventEnded)
				serverBD.eStage = eAD_END
			ENDIF
		BREAK
	
		CASE eAD_END
			IF NOT HAS_NET_TIMER_STARTED(serverBD.MissionEndStageTimer)
				START_NET_TIMER(serverBD.MissionEndStageTimer)
			ELSE
				IF IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh))
					IF HAS_NET_TIMER_EXPIRED(serverBD.MissionEndStageTimer, ((g_sMPTunables.iVehicleExplosionTime / 3) * 2))
						PRINTLN("[AM MOVING TARGET] -  PROCESS_HOT_TARGET_SERVER - SHOULD_MOVE_TO_EVENT_END(TRUE) - serverBD.eStage = eAD_END")
						SET_BIT(serverBD.iServerBitSet, biS_MissionShouldCleanup)
					ENDIF
				ELSE					
					IF HAS_NET_TIMER_EXPIRED(serverBD.MissionEndStageTimer, g_sMPTunables.iVehicleExplosionTime)
						PRINTLN("[AM MOVING TARGET] -  PROCESS_HOT_TARGET_SERVER - SHOULD_MOVE_TO_EVENT_END(TRUE) - serverBD.eStage = eAD_END")
						SET_BIT(serverBD.iServerBitSet, biS_MissionShouldCleanup)
					ENDIF
				ENDIF
			ENDIF
		BREAK
						
		CASE eAD_CLEANUP
			
		BREAK
	ENDSWITCH
ENDPROC

PROC MAINTAIN_ACTIVE_PLAYER_CHECK()

	IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iStaggeredCheck))
		PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iStaggeredCheck))
			
		IF NETWORK_IS_PLAYER_ACTIVE(PlayerId)
			FM_EVENT_MAINTAIN_ACTIVE_PLAYER_CHECK(PlayerId)
		ENDIF
	ENDIF
	
	iStaggeredCheck++
	
	IF iStaggeredCheck >= NUM_NETWORK_PLAYERS
		iStaggeredCheck = 0
	ENDIF

ENDPROC


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                      //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT(MP_MISSION_DATA missionScriptArgs)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
		#IF IS_DEBUG_BUILD	
			NET_PRINT_TIME() NET_PRINT("MP: Starting mission ") NET_NL()
		#ENDIF
		
		// Vehry out all the initial game starting duties. 
		PROCESS_PRE_GAME(missionScriptArgs)	
		
	ELSE
		#IF IS_DEBUG_BUILD NET_LOG("HOT TARGET -  NETWORK_IS_GAME_IN_PROGRESS = FALSE")	#ENDIF
		NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  MISSION END - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP P     <----------     ") NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
	
	// Main loop
	WHILE TRUE

		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
		
		// If we have a match end event, bail
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			#IF IS_DEBUG_BUILD NET_LOG("SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")	#ENDIF
			NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  MISSION END - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE - SCRIPT CLEANUP B     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		
		IF IS_KILL_CURRENT_FREEMODE_EVENT_GLOBAL_SET()
			PRINTLN("[AM MOVING TARGET] - IS_KILL_CURRENT_FREEMODE_EVENT_GLOBAL_SET - TRUE - calling script cleanup to kill Moving Target.")
			SCRIPT_CLEANUP()
		ENDIF
	
		MAINTAIN_ACTIVE_PLAYER_CHECK()
		
		//Call the common FM Events every frame callse
		FM_EVENT_COMMON_EVERY_FRAME_CALLS()
		
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_MinPlayersBailClient)
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_VehicleDelivered)
				IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
					IF SHOULD_BAIL_FM_EVENT_DUE_TO_LOW_SESSION_NUMBERS(3)
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_MinPlayersBailClient)
						PRINTLN("[AM MOVING TARGET] - SHOULD_BAIL_FM_EVENT_DUE_TO_LOW_SESSION_NUMBERS returning true (A), biP_MinPlayersBailClient SET")
					ENDIF
				ELSE
					IF SHOULD_BAIL_FM_EVENT_DUE_TO_LOW_SESSION_NUMBERS()
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_MinPlayersBailClient)
						PRINTLN("[AM MOVING TARGET] - SHOULD_BAIL_FM_EVENT_DUE_TO_LOW_SESSION_NUMBERS returning true (B), biP_MinPlayersBailClient SET")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niVeh)
		AND NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh))
			MAINTAIN_FREEMODE_AMBIENT_EVENT_DISTANT_CHECKS(FMMC_TYPE_MOVING_TARGET, GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niVeh)), bOnOff, TO_FLOAT(g_sMPTunables.iFREEMODE_EVENT_UI_ENTRY_DISTANCE), DEFAULT, bSetSpawnLocations)
		ENDIF
				
		
		//Check if player is in a tutorial session and end
		IF HAVE_PLAYERS_ENTERED_TO_START_EVENT()
			IF AM_I_A_HOT_TARGET()
				IF IS_PLAYER_IN_CORONA()
					#IF IS_DEBUG_BUILD NET_LOG("IS_PLAYER_IN_CORONA")	#ENDIF
					NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  MISSION END - IS_PLAYER_IN_CORONA - SCRIPT CLEANUP C     <----------     ") NET_NL()
					SCRIPT_CLEANUP()
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_IHaveLeft)
				IF IS_PLAYER_IN_CORONA()
				AND AM_I_A_HOT_TARGET()
					IF AM_I_THE_DRIVER()
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
							IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh))
								IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh), TRUE)
									TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh))	
									SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_IHaveLeft)
									PRINTLN("[AM MOVING TARGET] -  Player is Driver and is in corona, tasked with leaving vehicle.")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		// Deal with the debug
		#IF IS_DEBUG_BUILD		
			MAINTAIN_WIDGETS()
			
			IF MPGlobalsAmbience.bEndCurrentFreemodeEvent
				IF bDebugChangeTime = FALSE
					IF HAS_NET_TIMER_STARTED(serverBD.DeliverVehicleTimer)
						REINIT_NET_TIMER(serverBD.DeliverVehicleTimer)
					ENDIF
					PRINTLN("[AM MOVING TARGET] -  MPGlobalsAmbience.bEndCurrentFreemodeEvent TRUE, serverBD.DeliverVehicleTimer reinitialised.")
					bDebugChangeTime = TRUE
				ENDIF
			ENDIF
		#ENDIF		
	
		// If we need to leave the mission, make sure we just straight to the end stage and terminate the script - DON'T NEED FOR AMBIENT MISSION
		/*IF SHOULD_PLAYER_LEAVE_MP_MISSION() 
			#IF IS_DEBUG_BUILD
			NET_PRINT_TIME() NET_PRINT( GET_MP_MISSION_NAME(thisMission), " Leave mission = TRUE \n")
			#ENDIF
			playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_LEAVE
			#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_LEAVE")	#ENDIF
		ENDIF*/
		
		// -----------------------------------
		// Process your game logic.....
		SWITCH GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT())
			
			// Wait untill the server gives the all go before moving on
			CASE GAME_STATE_INI
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
				
					// Clear any gang attacks near vehicle spawn
					Set_MissionsAtCoords_Local_Disabled_Area(serverBD.vStartLocation)	
					
					IF NOT HAVE_PLAYERS_ENTERED_TO_START_EVENT()
						IF NOT SHOULD_UI_BE_HIDDEN()
							ADD_VEH_BLIP()
							FLASH_MINIMAP_DISPLAY() // 2369127
							IF DOES_BLIP_EXIST(VehBlip)
								SET_BLIP_FLASHES(VehBlip, TRUE)
		                    	SET_BLIP_FLASH_TIMER(VehBlip, 7000)
							ENDIF
						ENDIF
					ENDIF
					
					FM_EVENT_BLOCK_SPECIFIC_GANG_ATTACKS(FMMC_TYPE_MOVING_TARGET, serverBD.iRoute, serverBD.iVariation, serverBD.iVehicleType)
					
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_GunnerVehicle)
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
							IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
							AND NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh))
								SET_PED_VEHICLE_FORCED_SEAT_USAGE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh), 0, ENUM_TO_INT(VC_FORCE_USE_FRONT_SEATS))
							ENDIF
						ENDIF
					ENDIF
					
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING
					NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_RUNNING")	#ENDIF
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 1    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY")	#ENDIF
				ENDIF
			BREAK
			
			// Main gameplay state.
			CASE GAME_STATE_RUNNING
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					PROCESS_HOT_TARGET_CLIENT()
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 2   <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY")	#ENDIF
				ENDIF
			BREAK
			
			CASE GAME_STATE_TERMINATE_DELAY 
				SET_MISSION_FINISHED(serverBD.MissionTerminateDelayTimer)
		      	IF IS_MISSION_READY_TO_CLEANUP(serverBD.MissionTerminateDelayTimer)
		            playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 3    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 3")	#ENDIF
		      	ENDIF
			BREAK
			
			// The game stage the local player is placed when we want him to leave a mission
			CASE GAME_STATE_LEAVE
				//LEAVE_MY_MP_MISSION(thisMission)
				playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
				NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 2    <----------     ") NET_NL()
				#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 2")	#ENDIF
			FALLTHRU
			
			//Cleans up then terminates the mission
			CASE GAME_STATE_END
				NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  SCRIPT CLEANUP A     <----------     ") NET_NL()
				SCRIPT_CLEANUP()
			BREAK

		ENDSWITCH
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			
			SWITCH GET_SERVER_MISSION_STATE()
				
				// For CnC group missions, we may want to wait for all players to have launched the script
				CASE GAME_STATE_INI
					IF CREATE_VEH_AND_DRIVER()
						serverBD.iServerGameState = GAME_STATE_RUNNING
						NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  serverBD.iServerGameState = GAME_STATE_RUNNING    <----------     ") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_RUNNING")	#ENDIF
					ENDIF
				BREAK
				
				// Look for game end conditions
				CASE GAME_STATE_RUNNING
					
					MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
					PROCESS_HOT_TARGET_SERVER()
					
					#IF IS_DEBUG_BUILD
						IF bHostEndMissionNow
						OR MPGlobalsAmbience.bKillActiveEvent
							MPGlobalsAmbience.bKillActiveEvent = FALSE
							serverBD.iServerGameState = GAME_STATE_END
							NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  serverBD.iServerGameState = GAME_STATE_END 1    <----------     ") NET_NL()
							#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 1")	#ENDIF
						ENDIF
					#ENDIF	
					
					IF HAVE_MISSION_END_CONDITIONS_BEEN_MET()
						serverBD.iServerGameState = GAME_STATE_END
						NET_PRINT_TIME() NET_PRINT("[AM MOVING TARGET] -  serverBD.iServerGameState = GAME_STATE_END 4    <----------     ") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 4")	#ENDIF
					ENDIF
					
				BREAK
				
				CASE GAME_STATE_END
				BREAK
				
			ENDSWITCH
			
		ENDIF
		
	ENDWHILE
	
	
ENDSCRIPT
