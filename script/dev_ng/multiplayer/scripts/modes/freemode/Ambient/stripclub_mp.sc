//Strip club activities when playing freemode
//Author: Evan Lawson

//Game Headers
USING "rage_builtins.sch"
USING "globals.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "commands_graphics.sch"
USING "commands_camera.sch"
USING "commands_streaming.sch"
USING "commands_interiors.sch"
USING "commands_object.sch"
USING "script_drawing.sch"
USING "lineActivation.sch"
USING "drunk_public.sch"
USING "dialogue_public.sch"
USING "player_ped_public.sch"
USING "script_blips.sch"
USING "clubs_public.sch"
USING "minigame_UIInputs.sch"
USING "friendActivity_public.sch"
USING "cutscene_public.sch"
USING "rgeneral_include.sch"
USING "flow_processing_game.sch"
USING "transition_common.sch"
USING "freemode_header.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"
USING "net_include.sch"
USING "net_scoring_common.sch"
USING "net_race_to_point.sch"
USING "cheat_handler.sch"
USING "load_queue_public.sch"

// CnC Headers
USING "net_mission.sch"

USING "net_ambience.sch"
USING "commands_object.sch"

USING "net_wait_zero.sch"

//Stripclub Headers
//*

CONST_INT MP_STRIPCLUB 1
CONST_INT USE_STRIPCLUB_DEBUG 1
//USING "stripclub_public.sch"
//USING "stripclub.sch"
//USING "stripclub_common.sch"
//USING "stripclub_anim.sch"
//USING "stripclub_mp.sch"
//USING "stripclub_ai.sch"
//USING "stripclub_task.sch"
//USING "stripclub_helpers.sch"
//USING "stripclub_stage.sch"
//USING "stripclub_cutscenes.sch"
//USING "stripclub_hotspots.sch"
//USING "stripclub_workers.sch"
//USING "stripclub_lapdance.sch"
USING "stripclub_setup.sch"
//*/

LoadQueueLarge sLoadQueue

BOOL bRequestAudio = FALSE

FUNC BOOL CHECK_IS_STRIPPER_IN_USE(INT iPlayerIndex, INT iStripperIndex)
	
	INT iStriperInUse, iSecondaryStripperInUse
	
	IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayerIndex))
		iStriperInUse = GET_PLAYER_STRIPPER_IN_USE(clubPlayerBD[iPlayerIndex], iSecondaryStripperInUse)
		
		IF (iStriperInUse = iStripperIndex) OR (iSecondaryStripperInUse = iStripperIndex)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


#IF IS_DEBUG_BUILD
PROC DEBUG_DRAW_STRIPPER_TASKING()

	INT index
	TEXT_LABEL_31 text, text2, text3, text4

	DRAW_DEBUG_TEXT_2D("Strippers Available", <<0.05, 0.05, 0>>)
	
	REPEAT GET_MAX_NUMBER_OF_NAMED_STRIPPERS() index
		text = "Stripper " text += index
		text2 = "Local stand loc "
		text3 = "Server AI state "
		text4 = "Local AI state "
		IF IS_STRIPPER_AVAILABLE(index)
			
			IF CHECK_IS_STRIPPER_IN_USE(PARTICIPANT_ID_TO_INT(), index)
				text +=  " in use by me"
			ELSE
				text +=  " available"
			ENDIF
			
		ELSE
			text += " in use by another"
		ENDIF
		text2 += namedStripper[index].iCurrentStandPos
		text3 += ENUM_TO_INT(serverBD.stripperAIState[index])
		text4 += ENUM_TO_INT(clubPlayerBD[PARTICIPANT_ID_TO_INT()].stripperAIState[index])
		
		DRAW_DEBUG_TEXT_2D(text, <<0.05, 0.1+index*0.05, 0>>)
		DRAW_DEBUG_TEXT_2D(text2, <<0.25, 0.1+index*0.05, 0>>)
		DRAW_DEBUG_TEXT_2D(text3, <<0.50, 0.1+index*0.05, 0>>)
		DRAW_DEBUG_TEXT_2D(text4, <<0.75, 0.1+index*0.05, 0>>)
	ENDREPEAT

	text = "Server stand loc "
	DRAW_DEBUG_TEXT_2D(text, <<0.05, 0.1+5*0.05, 0>>)
	
	text = "Stripper to sync "
	text += GET_PLAYER_BROADCAST_STRIPPER_STATE_TO_SYNC(clubPlayerBD[PARTICIPANT_ID_TO_INT()])
	DRAW_DEBUG_TEXT_2D(text, <<0.05, 0.1+6*0.05, 0>>)
	
	INT iBit
	index = 0
	REPEAT NUM_RAND_STAND_LOCS iBit
		text = iBit
		IF IS_BIT_SET(serverBD.iWalkLocationsInUse, iBit)
			DRAW_DEBUG_TEXT_2D(text, <<0.175 + index*0.025, 0.1+5*0.05, 0>>)
			index++
		ENDIF
	ENDREPEAT
	
ENDPROC
#ENDIF

PROC STRIPCLUB_SEVER_SYNC_PED(PED_INDEX &pedIndex, NETWORK_INDEX &ServerEntity)

	IF DOES_ENTITY_EXIST(pedIndex)
		IF NOT IS_ENTITY_A_MISSION_ENTITY(pedIndex)
			SET_ENTITY_AS_MISSION_ENTITY(pedIndex)
		ENDIF
		
		IF IS_ENTITY_A_MISSION_ENTITY(pedIndex)
			ServerEntity = NETWORK_GET_NETWORK_ID_FROM_ENTITY(pedIndex)
		ENDIF
	ELSE
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(ServerEntity) //saved in server but not localy, player bacame host suddenly
			pedIndex = GET_PED_INDEX_FROM_ENTITY_INDEX(NETWORK_GET_ENTITY_FROM_NETWORK_ID(ServerEntity))
		ENDIF
	ENDIF

ENDPROC

PROC STRIPCLUB_SEVER_SYNC_OBJECT(OBJECT_INDEX &objIndex, NETWORK_INDEX &ServerEntity)

	IF DOES_ENTITY_EXIST(objIndex)
		IF NOT IS_ENTITY_A_MISSION_ENTITY(objIndex)
			SET_ENTITY_AS_MISSION_ENTITY(objIndex)
		ENDIF
		
		IF IS_ENTITY_A_MISSION_ENTITY(objIndex)
			ServerEntity = NETWORK_GET_NETWORK_ID_FROM_ENTITY(objIndex)
		ENDIF
	ELSE
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(ServerEntity) //saved in server but not localy, player bacame host suddenly
			objIndex = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(NETWORK_GET_ENTITY_FROM_NETWORK_ID(ServerEntity))
		ENDIF
	ENDIF

ENDPROC

PROC SYNC_STRIPCLUB_SERVER()
	
	INT index, iStripperIndex
	TEXT_LABEL_31 text
	text = text
	
	SET_PLAYER_BROADCAST_IN_CLUB(clubPlayerBD[PARTICIPANT_ID_TO_INT()], IS_IN_CLUB_FLAG_SET())
	
	#IF IS_DEBUG_BUILD
		DEBUG_DRAW_STRIPPER_TASKING()
	#ENDIF
	
	BOOL bOwnsAllPedsLocal = TRUE
	
	SWITCH stripServerSyncState
		CASE SYNC_CLUB_OBJECTS
			STRIPCLUB_SEVER_SYNC_OBJECT(objShotGlass, serverBD.netGlass)
			STRIPCLUB_SEVER_SYNC_OBJECT(objDrinkBottle, serverBD.netBottle)
			stripServerSyncState = SYNC_CLUB_POLEDANCERS
		BREAK
		CASE SYNC_CLUB_POLEDANCERS
			stripServerSyncState = SYNC_CLUB_WORKERS
		BREAK
		CASE SYNC_CLUB_WORKERS
			//sync club staff peds
			REPEAT MAX_NUMBER_CLUB_STAFF index
				STRIPCLUB_SEVER_SYNC_PED(clubStaffPed[index], serverBD.clubWorkers[index])
			ENDREPEAT
			stripServerSyncState = SYNC_CLUB_STRIPPERS
		BREAK
		CASE SYNC_CLUB_STRIPPERS
			//sync stripper peds
			REPEAT GET_MAX_NUMBER_OF_NAMED_STRIPPERS() index
				STRIPCLUB_SEVER_SYNC_PED(namedStripper[index].ped, serverBD.namedStrippers[index])
			ENDREPEAT
			stripServerSyncState = SYNC_CLUB_OBJECTS
		BREAK
	ENDSWITCH
	
	SYNC_STRIPPER_AI_STATES_SERVER()
	SYNC_STRIPPER_AI_STATES_CLIENT()
	
//	CCPRINTLN(DEBUG_SCLUB,DEBUG_SCLUB, " iCurrentStripperForDance ", iCurrentStripperForDance, " iSecondaryStripper ", iSecondaryStripper)
	
	//Set if this player is using any strippers
	BOOL bUsingStripper
	INT iPlayerUsingStripper[MAX_NUMBER_NAMED_STRIPPERS_MP]
	REPEAT GET_MAX_NUMBER_OF_NAMED_STRIPPERS() iStripperIndex
		bUsingStripper = FALSE
		IF (iCurrentStripperForDance = iStripperIndex OR iStripperForApproach = iStripperIndex OR iSecondaryStripper = iStripperIndex)
		AND IS_STRIPPER_AVAILABLE(iStripperIndex)
			bUsingStripper = TRUE
		ENDIF
		SET_PLAYER_STRIPPER_IN_USE(clubPlayerBD[PARTICIPANT_ID_TO_INT()], iStripperIndex, bUsingStripper)
		iPlayerUsingStripper[iStripperIndex] = -1
	ENDREPEAT
	
	//Check to see who is in the club and who is going hostile
	PARTICIPANT_INDEX participantIndex = INT_TO_PARTICIPANTINDEX(iServerSyncPlayers)
	IF NETWORK_IS_PARTICIPANT_ACTIVE(participantIndex)
		//CPRINTLN(DEBUG_SCLUB,"Participant ", iServerSyncPlayers, " is active")
		IF IS_PLAYER_BROADCAST_IN_CLUB(clubPlayerBD[iServerSyncPlayers])
			bAnyoneInClub  = TRUE
		ENDIF
		
		IF IS_PLAYER_BROADCAST_WENT_HOSTILE(clubPlayerBD[iServerSyncPlayers])
			//CPRINTLN(DEBUG_SCLUB,"Participant ", iServerSyncPlayers, " went hostile")
			bAnyoneWentHostile = TRUE
		ENDIF
		
		REPEAT GET_MAX_NUMBER_OF_NAMED_STRIPPERS() iStripperIndex
			IF CHECK_IS_STRIPPER_IN_USE(iServerSyncPlayers, iStripperIndex)
				iPlayerUsingStripper[iStripperIndex] = iServerSyncPlayers
			ENDIF
		ENDREPEAT
	ENDIF
	
	iServerSyncPlayers++
	IF iServerSyncPlayers >= MP_STRIPCLUB_MAX_PLAYERS
		serverBD.bIsAnyoneInClub = bAnyoneInClub
		serverBD.bClubHostile = bAnyoneWentHostile
		
		bAnyoneWentHostile = FALSE
		bAnyoneInClub = FALSE
		iServerSyncPlayers = 0
	ENDIF
	
	//check which strippers are being used by other players
	REPEAT GET_MAX_NUMBER_OF_NAMED_STRIPPERS() iStripperIndex
		IF serverBD.iStripperInUseBy[iStripperIndex] != -1 //someone is using the stripper
			//if the stripper isn't being used by the curent player
			IF serverBD.iStripperInUseBy[iStripperIndex] != PARTICIPANT_ID_TO_INT()
				//see if the last person to be using the stripper is still using it
				IF CHECK_IS_STRIPPER_IN_USE(serverBD.iStripperInUseBy[iStripperIndex], iStripperIndex)
					//set the availablitity of the stripper, if another player is using the stripper, shes not available
					SET_STRIPPER_AVAILABLE(iStripperIndex, FALSE)
				ELSE
					//Hes not using it anymore revert
					serverBD.iStripperInUseBy[iStripperIndex] = -1
					SET_STRIPPER_AVAILABLE(iStripperIndex, TRUE)
				ENDIF
			ELSE
				//Verify server is using stripper
				IF CHECK_IS_STRIPPER_IN_USE(serverBD.iStripperInUseBy[iStripperIndex], iStripperIndex)
				
				ELSE
					//Hes not using it, revert
					serverBD.iStripperInUseBy[iStripperIndex] = -1
				ENDIF
				SET_STRIPPER_AVAILABLE(iStripperIndex, TRUE) //I am in control of stripper so she is available to me
			ENDIF
		ELSE
			//Server doesnt think anyone is in control, set it to the asynchronous check
			serverBD.iStripperInUseBy[iStripperIndex] = iPlayerUsingStripper[iStripperIndex]
			SET_STRIPPER_AVAILABLE(iStripperIndex, TRUE)
		ENDIF
		
		//If the player has locked the stripper for a lapdance. Make sure they are actaully in the dance
		IF IS_BITMASK_AS_ENUM_SET(namedStripper[iStripperIndex].stripperFlags, STRIPPER_FLAG_LOCKED_CONTROL)
			IF stripClubStage != STAGE_GO_TO_VIP_ROOM
			AND stripClubStage != STAGE_DANCE
			AND PLAYER_PED_ID() != GET_PED_FOR_DANCE()
			OR NOT IS_STRIPPER_AVAILABLE(index)
				SET_STRIPPER_CAN_MIGRATE(iStripperIndex, TRUE)
				CLEAR_BITMASK_AS_ENUM(namedStripper[iStripperIndex].stripperFlags, STRIPPER_FLAG_LOCKED_CONTROL)
			ENDIF
		ENDIF
	ENDREPEAT
	
	INT iBit
	BOOL bLocUsed
	//Sync the used stripper stand locations
	REPEAT NUM_RAND_STAND_LOCS iBit
		bLocUsed = FALSE
		
		REPEAT GET_MAX_NUMBER_OF_NAMED_STRIPPERS() index
			IF namedStripper[index].iCurrentStandPos = iBit
				bLocUsed = TRUE
			ENDIF
		ENDREPEAT
		
		IF bLocUsed
			SET_BIT(serverBD.iWalkLocationsInUse, iBit)
		ELIF IS_BIT_SET(serverBD.iWalkLocationsInUse, iBit)
			CLEAR_BIT(serverBD.iWalkLocationsInUse, iBit)
		ENDIF
	ENDREPEAT
	
	IF stripClubStage != STAGE_DANCE //don't sync club hostile locally if you are in the middle of a dance
	
		IF serverBD.bClubHostile AND stripClubStage != STAGE_CLUB_HOSTILE
			SEND_CLUB_HOSTILE(TRUE)
			stripClubStage = STAGE_CLUB_HOSTILE
		ENDIF
		
		IF !bPlayerResetAfterHostile AND IS_BITMASK_AS_ENUM_SET(iStripClubBits_General, STRIP_CLUB_GENERAL_PLAYER_CONTROL_REMOVED)
		AND serverBD.bClubHostile
			STRIPCLUB_REGAIN_PLAYER()
			bPlayerResetAfterHostile = TRUE
		ENDIF
	ELIF serverBD.bClubHostile //club went hostile while you are getting a dance, let your dance continue but still do hostile stuff for otehr club workers.
		DO_STAGE_CLUB_HOSTILE()
	ENDIF
	
		//sync club staff peds
	REPEAT MAX_NUMBER_CLUB_STAFF index
		IF DOES_ENTITY_EXIST(clubStaffPed[index])			
			IF index != ENUM_TO_INT(STAFF_BARMAN) AND !serverBD.bClubHostile
				IF NOT STRIPCLUB_PLAYER_DOES_CONTROL_PED(serverBD.clubWorkers[index], clubStaffPed[index])
					bOwnsAllPedsLocal = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT GET_MAX_NUMBER_OF_NAMED_STRIPPERS() index
		IF DOES_ENTITY_EXIST(namedStripper[index].ped)
			IF IS_STRIPPER_AVAILABLE(index) AND !serverBD.bClubHostile
				IF NOT STRIPCLUB_PLAYER_DOES_CONTROL_PED(serverBD.namedStrippers[index], namedStripper[index].ped)
					bOwnsAllPedsLocal = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	bOwnsAllPeds = bOwnsAllPedsLocal
	
//	#IF IS_DEBUG_BUILD
//	IF bOwnsAllPeds
//	text = "owns all peds"	
//	DRAW_DEBUG_TEXT_2D(text, <<0.75, 0.1, 0>>)		
//	ENDIF
//	#ENDIF
	

ENDPROC

PROC SYNC_STRIPCLUB_CLIENT()
	
	INT index
	
	SET_PLAYER_BROADCAST_IN_CLUB(clubPlayerBD[PARTICIPANT_ID_TO_INT()], IS_IN_CLUB_FLAG_SET())
	
	#IF IS_DEBUG_BUILD
		DEBUG_DRAW_STRIPPER_TASKING()
	#ENDIF
	
	IF stripClubStage != STAGE_DANCE //don't sync club hostile if you are in the middle of a dance
	
		IF serverBD.bClubHostile AND stripClubStage != STAGE_CLUB_HOSTILE
			SEND_CLUB_HOSTILE(TRUE)
			stripClubStage = STAGE_CLUB_HOSTILE
		ENDIF
	
		IF !bPlayerResetAfterHostile AND IS_BITMASK_AS_ENUM_SET(iStripClubBits_General, STRIP_CLUB_GENERAL_PLAYER_CONTROL_REMOVED)
		AND serverBD.bClubHostile AND stripClubStage != STAGE_DANCE
			STRIPCLUB_REGAIN_PLAYER()
			bPlayerResetAfterHostile = TRUE
		ENDIF
		
	ELIF serverBD.bClubHostile //club went hostile while you are getting a dance, let your dance continue but still do hostile stuff for otehr club workers.
		DO_STAGE_CLUB_HOSTILE()
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.netGlass)
		objShotGlass = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(NETWORK_GET_ENTITY_FROM_NETWORK_ID(serverBD.netGlass))
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.netBottle)
		objDrinkBottle = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(NETWORK_GET_ENTITY_FROM_NETWORK_ID(serverBD.netBottle))
	ENDIF

	//sync club staff peds
	REPEAT MAX_NUMBER_CLUB_STAFF index
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.clubWorkers[index])
			clubStaffPed[index] = GET_PED_INDEX_FROM_ENTITY_INDEX(NETWORK_GET_ENTITY_FROM_NETWORK_ID(serverBD.clubWorkers[index]))
		ENDIF
	ENDREPEAT
	
	//sync stripper peds
	REPEAT GET_MAX_NUMBER_OF_NAMED_STRIPPERS() index
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.namedStrippers[index])
			namedStripper[index].ped = GET_PED_INDEX_FROM_ENTITY_INDEX(NETWORK_GET_ENTITY_FROM_NETWORK_ID(serverBD.namedStrippers[index]))
		ENDIF
	ENDREPEAT

//	CCPRINTLN(DEBUG_SCLUB,DEBUG_SCLUB, " iCurrentStripperForDance ", iCurrentStripperForDance, " iSecondaryStripper ", iSecondaryStripper, " iStripperForApproach ", iStripperForApproach)
	
	BOOL bUsingStripper
	REPEAT GET_MAX_NUMBER_OF_NAMED_STRIPPERS() index
		bUsingStripper = FALSE
		IF (iCurrentStripperForDance = index OR iStripperForApproach = index OR iSecondaryStripper = index)
		AND IS_STRIPPER_AVAILABLE(index)
			bUsingStripper = TRUE
		ENDIF
		SET_PLAYER_STRIPPER_IN_USE(clubPlayerBD[PARTICIPANT_ID_TO_INT()], index, bUsingStripper)
	ENDREPEAT
	
	SYNC_STRIPPER_AI_STATES_CLIENT()
	
	//sync stripper in use
	REPEAT GET_MAX_NUMBER_OF_NAMED_STRIPPERS() index
	
		IF serverBD.iStripperInUseBy[index] = -1
			SET_STRIPPER_AVAILABLE(index, TRUE)
		ELSE
			//if the stripper isn't being used by the curent player
			IF serverBD.iStripperInUseBy[index] != PARTICIPANT_ID_TO_INT()
				//set the availablitity of the stripper, if another player is using the stripper, shes not available
				SET_STRIPPER_AVAILABLE(index, FALSE)
			ENDIF
		ENDIF
		
		//If the player has locked the stripper for a lapdance. Make sure they are actaully in the dance
		IF IS_BITMASK_AS_ENUM_SET(namedStripper[index].stripperFlags, STRIPPER_FLAG_LOCKED_CONTROL)
			IF stripClubStage != STAGE_GO_TO_VIP_ROOM
			AND stripClubStage != STAGE_DANCE
			OR NOT IS_STRIPPER_AVAILABLE(index)
				SET_STRIPPER_CAN_MIGRATE(index, TRUE)
				CLEAR_BITMASK_AS_ENUM(namedStripper[index].stripperFlags, STRIPPER_FLAG_LOCKED_CONTROL)
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC HANDLE_INSIDE_ASSETS_SERVER()
	HANDLE_INSIDE_ASSETS(sLoadQueue, serverBD.bIsAnyoneInClub, (!serverBD.bIsAnyoneInClub AND bOwnsAllPeds AND !serverBD.bClubHostile), serverBD.bIsAnyoneInClub, serverBD.iStartTime)
ENDPROC

PROC HANDLE_INSIDE_ASSETS_CLIENT()
	SWITCH stripclubInside
		CASE SC_INSIDE_INIT
			IF stripClubInterior = NULL
				stripClubInterior = GET_INTERIOR_AT_COORDS(GET_STRIP_CLUB_CENTRE_POSITION(thisStripClub))
			ENDIF	
			
			IF IS_PLAYER_IN_CLUB()
				// get interior
				stripClubInterior = GET_INTERIOR_AT_COORDS(GET_STRIP_CLUB_CENTRE_POSITION(thisStripClub))
				g_Interior_Instance_Strip_Club_Low = stripClubInterior
				
				IF NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_Streaming ,STRIP_CLUB_PLAYER_CONTROLS_STREAMED)
					SET_BITMASK_AS_ENUM(iStripClubBits_Streaming ,STRIP_CLUB_PLAYER_CONTROLS_STREAMED)
				ENDIF
				
				IF NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_General, STRIP_CLUB_GENERAL_PLAYER_VISITED)
					//BAWSAQ_INCREMENT_MODIFIER(BSMF_STRIPCLUBVISIT)
					SET_BITMASK_AS_ENUM(iStripClubBits_General, STRIP_CLUB_GENERAL_PLAYER_VISITED)					
				ENDIF
				
				//if first visit get some inital impressions of the player
				MODIFY_STRIPPER_LIKE_ON_FIRST_VISIT()
				
				// load inside models and anims
				REQUEST_STRIPPER_IDLES()
				LOAD_QUEUE_LARGE_ADD_ANIM_DICT(sLoadQueue, GET_STRIP_CLUB_ANIM_DICT_BAR_DRINK(SCBA_IDLE))
				LOAD_QUEUE_LARGE_ADD_ANIM_DICT(sLoadQueue, "facials@gen_female@variations@happy")
				
				IF NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_Streaming, STRIP_CLUB_BIT_INSIDE_PROP_STREAMED)
					CPRINTLN(DEBUG_SCLUB,"request models")
					REQUEST_WORKER_MODELS(sLoadQueue)
					LOAD_QUEUE_LARGE_ADD_MODEL(sLoadQueue, GET_STRIPCLUB_DRINK_GLASS_MODEL())
					LOAD_QUEUE_LARGE_ADD_MODEL(sLoadQueue, GET_STRIPCLUB_DRINK_BOTTLE_MODEL())
					SET_BITMASK_AS_ENUM(iStripClubBits_Streaming, STRIP_CLUB_BIT_INSIDE_PROP_STREAMED)							
				ENDIF
				
				SET_PED_NON_CREATION_AREA(<<111.036621,-1282.914917,26.262775>>, <<114.787239,-1289.966309,29.260988>> ) //vip entryway
				SET_PED_NON_CREATION_AREA(<<120.53, -1302.49, 28.27>>, <<109.50, -1301.97, 28.27>>) //don't let peds spawn in the vip room
				SET_PED_NON_CREATION_AREA(<<111.036621,-1282.914917,26.262775>>, <<114.787239,-1289.966309,29.260988>> ) //no peds near rails
					
				
				STRIPCLUB_DISABLE_NAVMESH()
				
				SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_STRIPCLUB, TRUE)
				
				SET_BITMASK_AS_ENUM( g_StripclubGlobals.iStripclubFlags, GSF_IN_CLUB )
				
				stripclubInside = SC_INSIDE_STREAMING
			ENDIF		
		BREAK
		
		CASE SC_INSIDE_STREAMING
			REQUEST_STRIPPER_MODELS(sLoadQueue)
		
			IF  HAVE_STRIPPER_MODELS_LOADED(sLoadQueue)
			AND HAVE_WORKER_MODELS_LOADED(sLoadQueue)
			AND HAVE_STRIPPER_IDLES_LOADED()
//			AND IS_INTERIOR_READY(stripClubInterior)
				// initialise stage
				iAnnouncerTime = -1
				iRailFlirtTime = GET_GAME_TIMER() + 15000
				
				// initialise rail segments
				IF NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_General, STRIP_CLUB_GENERAL_INIT_RAILS)
					INIT_STRIP_CLUB_RAIL_SEGMENTS()
					SET_BITMASK_AS_ENUM(iStripClubBits_General, STRIP_CLUB_GENERAL_INIT_RAILS)
				ENDIF
				
				IF NOT IS_BEEN_IN_CLUB_FLAG_SET()
					// setup initial available and approach times
					SETUP_INITIAL_STRIPPER_TIMES()
					SET_BEEN_IN_CLUB_FLAG(TRUE)
				ENDIF
				
				//INIT_STRIP_CLUB_VIP_SEATS()
				
				// create inside peds
				CREATE_NAMED_STRIPPERS(FALSE, serverBD.iStartTime)
				SET_BITMASK_AS_ENUM(iStripClubBits_General, STRIP_CLUB_GENERAL_POLE_DANCERS_CREATED)
				//CREATE_INSIDE_CLUB_WORKERS()
				
				//IF NOT IS_STRIP_CLUB_INTRO_CINEMATIC_PLAYING()
					//CREATE_GENERIC_STRIPPERS()
				//ENDIF
				
				//DISABLE_NAVMESH_IN_AREA(<<114.13, -1296.64, 27.42>>, <<118.13, -1292.64, 31.42>>, TRUE)
				
				SET_BITMASK_AS_ENUM(iStripClubBits_General, STRIP_CLUB_GENERAL_DISABLED_DOOR_NAVMESH)
				
				stripclubInside = SC_INSIDE_MANAGE
			ENDIF
		BREAK
		
		CASE SC_INSIDE_MANAGE
			IF stripClubStage != STAGE_CLUB_HOSTILE
				CLEAR_AREA_OF_PROJECTILES(<<115.52, -1287.22, 27.26>>, 7.25, TRUE)
			ENDIF
			
			IF IS_IN_CLUB_FLAG_SET()
				
				SETUP_PC_CONTROLS()
				
				IF NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_Workers, STRIP_CLUB_BIT_WORKER_ADD_INSIDE_BOUNCER_DIALOGUE)
					IF DOES_ENTITY_EXIST(clubStaffPed[ENUM_TO_INT(STAFF_BOUNCER_VIP)])
					AND DOES_ENTITY_EXIST(clubStaffPed[ENUM_TO_INT(STAFF_BOUNCER_PATROL)])
						ADD_PED_FOR_DIALOGUE(stripClubConversation, ENUM_TO_INT(SPEAKER_BOUNCER_VIP), clubStaffPed[ENUM_TO_INT(STAFF_BOUNCER_VIP)], "Bouncer")
						ADD_PED_FOR_DIALOGUE(stripClubConversation, ENUM_TO_INT(SPEAKER_BOUNCER_PATROL), clubStaffPed[ENUM_TO_INT(STAFF_BOUNCER_PATROL)], "Bouncer2")	
						SET_BITMASK_AS_ENUM(iStripClubBits_Workers, STRIP_CLUB_BIT_WORKER_ADD_INSIDE_BOUNCER_DIALOGUE)
					ENDIF
				ENDIF
				IF NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_Workers, STRIP_CLUB_BIT_WORKER_ADD_INSIDE_ANNOUNCER_DIALOGUE)
					IF DOES_ENTITY_EXIST(clubStaffPed[ENUM_TO_INT(STAFF_ANNOUNCER)])	//something about them being inside makes it take a frame to create				
						ADD_PED_FOR_DIALOGUE(stripClubConversation, ENUM_TO_INT(SPEAKER_ANNOUNCER), clubStaffPed[ENUM_TO_INT(STAFF_ANNOUNCER)], "ClubAnnouncer")
						SET_BITMASK_AS_ENUM(iStripClubBits_Workers, STRIP_CLUB_BIT_WORKER_ADD_INSIDE_ANNOUNCER_DIALOGUE)
					ENDIF
				ENDIF
				
				IF IS_BITMASK_AS_ENUM_SET(iStripClubBits_Streaming, STRIP_CLUB_BIT_INSIDE_PROP_STREAMED)
					SET_MODEL_AS_NO_LONGER_NEEDED(GET_STRIPCLUB_DRINK_GLASS_MODEL())
					SET_MODEL_AS_NO_LONGER_NEEDED(GET_STRIPCLUB_DRINK_BOTTLE_MODEL())
					SET_MODEL_AS_NO_LONGER_NEEDED(GET_WORKER_MODEL(STAFF_ANNOUNCER))
					SET_MODEL_AS_NO_LONGER_NEEDED(GET_WORKER_MODEL(STAFF_BARMAN))
					SET_MODEL_AS_NO_LONGER_NEEDED(GET_WORKER_MODEL(STAFF_BOUNCER_PATROL))
					SET_STRIPPER_MODELS_AS_NO_LONGER_NEEDED()
					CLEAR_BITMASK_AS_ENUM(iStripClubBits_Streaming, STRIP_CLUB_BIT_INSIDE_PROP_STREAMED)
				ENDIF
				
				MANAGE_STRIP_CLUB_PEDS(TRUE)
			ELSE
				ABANDON_STRIPPERS()
				
				SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_STRIPCLUB, FALSE)
				
				CLEAR_BITMASK_AS_ENUM( g_StripclubGlobals.iStripclubFlags, GSF_IN_CLUB )
				
				CLEANUP_PC_CONTROLS()
				
				IF !serverBD.bIsAnyoneInClub AND !serverBD.bClubHostile
					stripclubInside = SC_INSIDE_CLEANUP
				ENDIF
				
			ENDIF
		BREAK
		
		CASE SC_INSIDE_CLEANUP
			CLEANUP_STRIPCLUB_INSIDE(sLoadQueue, FALSE)
			
			stripclubInside = SC_INSIDE_INIT	
		BREAK
	ENDSWITCH	
ENDPROC


FUNC BOOL IS_STRIPCLUB_CLEAR_OF_HOSTILES()
	INT index
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() index
		
		PARTICIPANT_INDEX participantIndex = INT_TO_PARTICIPANTINDEX(index)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(participantIndex)
		
			PED_INDEX pedIndex = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(participantIndex))
			
			CPRINTLN(DEBUG_SCLUB,"Player ", index)
			
			IF IS_PLAYER_BROADCAST_WENT_HOSTILE(clubPlayerBD[index])
				CPRINTLN(DEBUG_SCLUB,"Has hostile set")
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(pedIndex)
				CPRINTLN(DEBUG_SCLUB,"Is not dead")
			ENDIF
			
			IF IS_PLAYER_BROADCAST_WENT_HOSTILE(clubPlayerBD[index]) AND NOT IS_ENTITY_DEAD(pedIndex)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT

	CPRINTLN(DEBUG_SCLUB,"Clear of hostiles")
	RETURN TRUE
ENDFUNC

PROC DO_STAGE_RETURN_FROM_HOSTILE()

ENDPROC

SCRIPT(MP_MISSION_DATA missionScriptArgs)

	CPRINTLN(DEBUG_SCLUB, "Start MP Stripclub, Dev Next Gen")
	missionScriptArgs = missionScriptArgs

	// This marks the script as a net script, and handles any instancing setup. 
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(MP_STRIPCLUB_MAX_PLAYERS, FALSE)
	
	// This makes sure the net script is active, waits untull it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	//RESERVE_NETWORK_MISSION_OBJECTS(5)
	CPRINTLN(DEBUG_SCLUB,"Reserving peds for stripclub")
	RESERVE_NETWORK_MISSION_PEDS(MAX_NUMBER_OF_CLUB_EMPLOYEES)
	RESERVE_NETWORK_MISSION_OBJECTS(MAX_NUMBER_OF_STRIPCLUB_CASH_OBJECTS + 2) //plus two is for bar objects
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(clubPlayerBD, SIZE_OF(clubPlayerBD))
	// This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	// KGM: Wait for the first network broadcast before moving on - call this as the last instruction in pre-game
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		CPRINTLN(DEBUG_SCLUB,"Strip club mp. Failed to receive initial network broadcast. Cleaning up.")
		CLEANUP_STRIP_CLUB(sLoadQueue)
	ENDIF
	
	//SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	
	//In mp use the tunables for price
	DANCE_COST = g_sMPTunables.istripbar_dance_price_modifier
	
	CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(GET_PLAYER_INDEX())
	SET_STRIPCLUB_GLOBAL_THROWN_OUT(FALSE)
	//SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
   	INT index
	
	bPCCOntrolsSetup = FALSE
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		CPRINTLN(DEBUG_SCLUB,"Started stripclub script as host!")
		IF serverBD.iStartTime = -1
			serverBD.iStartTime = GET_CLOCK_HOURS()
		ENDIF
		
		REPEAT GET_MAX_NUMBER_OF_NAMED_STRIPPERS() index
			serverBD.iPoleDancerNetSceneID[index] = -1
		ENDREPEAT
		serverBD.serverPedForPrivateDance = INT_TO_NATIVE(PARTICIPANT_INDEX, -1)
		STRIPCLUB_CLOSE_BACK_DOOR(TRUE, 0)
		STRIPCLUB_CLOSE_DRESSING_ROOM_DOOR(TRUE, 1.0)
	ENDIF
	
	REPEAT COUNT_OF(iPoleDancerNextNetSceneID) index
		iPoleDancerNextNetSceneID[index] = -1
	ENDREPEAT
	
	//Make sure the stripper ids are set correctly
	namedStripper[0].stripperID = CONVERT_LOCAL_STRIPPER_INDEX_TO_NAME(0, serverBD.iStartTime )
	namedStripper[1].stripperID = CONVERT_LOCAL_STRIPPER_INDEX_TO_NAME(1, serverBD.iStartTime )
	namedStripper[2].stripperID = CONVERT_LOCAL_STRIPPER_INDEX_TO_NAME(2, serverBD.iStartTime )
	
	IF NETWORK_IS_ACTIVITY_SPECTATOR() OR IS_PLAYER_SCTV(PLAYER_ID())
		CPRINTLN(DEBUG_SCLUB, "Joined stripclub mp as spectator. This is bad.")
		iStartPostHeistCutsceneTime = GET_GAME_TIMER() + 15000
		SET_PLAYER_STARTED_STRICLUB_HIEST_SCENE()
		
		//Wait a bit before exiting script in case post heist scene is active.
		WHILE TRUE
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(500)
			ENDIF
			IF iStartPostHeistCutsceneTime < GET_GAME_TIMER()
				CPRINTLN(DEBUG_SCLUB,"Ending sripclub mp as spectator")
				CLEANUP_STRIP_CLUB(sLoadQueue)
			ENDIF
			WAIT(0)
		ENDWHILE
	ENDIF
	
    WHILE TRUE
        MP_LOOP_WAIT_ZERO()
		
		// Replay Camera Movement Blocking (Strip Club)
		IF stripclubInside <> SC_INSIDE_MANAGE
			IF IS_REPLAY_RECORDING()
				IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)	//Fix for bugs 2209274, 2198600, 2195133, 2194867
        		OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG)
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), <<115.93143, -1290.48279, 27.94359>>) < (70.0*70.0)
						REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2209617
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		STRIPCLUB_MANAGE_END_CUTSCENE_TO_FIRST_PERSON_CAMERA_EFFECT()
		UPDATE_LOAD_QUEUE_LARGE(sLoadQueue)
		
		#IF IS_DEBUG_BUILD
			MONITOR_BOOTY_CALL_DEBUG_ACTIVATION_MP()
		#ENDIF
		
		IF IS_PLAYER_PED_SWITCH_IN_PROGRESS()
		OR IS_SKYSWOOP_MOVING() OR IS_SKYSWOOP_IN_SKY() OR IS_TRANSITION_ACTIVE()
//			CCPRINTLN(DEBUG_SCLUB,DEBUG_SCLUB, "Transitioning in club, clear help")
			CLEAR_ALL_STRIP_CLUB_HELP()
			CLEAR_ALL_STRIP_CLUB_PRINT()
		ENDIF
			
		IF (IS_IN_CLUB_FLAG_SET() OR IS_PLAYER_UNDER_CANOPY())
		AND stripClubStage != STAGE_CLUB_HOSTILE
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
		ENDIF
		
		UPDATE_STRIPPER_DISTANCE_TO_PLAYER()
		
		MONITOR_STRIPCLUB_CLOTHS_CHANGE()
		
		IF IS_PED_IN_STRIPCLUB_INTERIOR(PLAYER_PED_ID())
			IF stripClubStage != STAGE_CLUB_HOSTILE
				STRIPCLUB_SUPPRESS_PLAYERS_WEAPON()
				IF IS_PLAYER_WEARING_PILOT_HELMET()
					REMOVE_STRIPCLUB_TEETH()
				ENDIF
				REMOVE_STRIPCLUB_PLAYER_HELMET()
			ELSE
				SET_STRIPCLUB_PLAYER_HELMET()
				SET_STRIPCLUB_TEETH()
			ENDIF
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = PERFORMING_TASK 
					DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
				ENDIF
			
				SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableAgitationTriggers, TRUE)
				SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableAgitation, TRUE)
				SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableTalk, TRUE)
			ENDIF
			
			DISABLE_RACE_TO_POINT()
		ELSE
			SET_STRIPCLUB_PLAYER_HELMET()
			SET_STRIPCLUB_TEETH()
			ENABLE_RACE_TO_POINT()
		ENDIF
		VECTOR vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
		
		
		IF VDIST2(vPlayerCoords, <<114.64, -1290.34, 29.68>>) < (STRIPCLUB_MP_ACTIVATION_RANGE*STRIPCLUB_MP_ACTIVATION_RANGE*1.3)
		AND NOT SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		AND NOT IS_PLAYER_ON_ANY_DEATHMATCH(PLAYER_ID())
		AND NOT NETWORK_IS_IN_TUTORIAL_SESSION()
		AND NOT NETWORK_IS_ACTIVITY_SESSION()
		//AND CNC_FLOW_Get_flow_Flag_State(ciCNC_FLOW_STRIP_CLUB_ACTIVE)
			
			#IF IS_DEBUG_BUILD
				DO_DEBUG_SKIPS()
				
				IF bTestPostHeistScene
					CPRINTLN(DEBUG_SCLUB, "bTestPostHeistScene ", NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
					g_TransitionSessionNonResetVars.sGlobalCelebrationData.bReadyForStripClubPostScene = TRUE
					g_TransitionSessionNonResetVars.sGlobalCelebrationData.iStripclubPlayerNameHash[iTestPostHeistIndex] = NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID())
					DO_SCREEN_FADE_OUT(500)
					bTestPostHeistScene = FALSE
				ENDIF
			#ENDIF
			
			IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bBlockStripClubPedsForCongratsScreen
			AND NOT bLocalBlockStripClubPedsForCongratsScreen
				CPRINTLN(DEBUG_SCLUB, "Setting BlockStripClub Peds")
				bLocalBlockStripClubPedsForCongratsScreen = TRUE
			ENDIF
			
			IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bReadyForStripClubPostScene
				FREEZE_MICROPHONE()
				IF IS_SCREEN_FADED_OUT()
					IF CAN_STRIPCLUB_POST_HEIST_SCENE_PLAY()
						SETUP_STRIPCLUB_END_HEIST_SCENE()
						g_TransitionSessionNonResetVars.sGlobalCelebrationData.enumPostMissionScenePossibleResult = PMSP_PASSED
					ELSE
						g_TransitionSessionNonResetVars.sGlobalCelebrationData.enumPostMissionScenePossibleResult = PMSP_FAILED
						SET_LOCAL_PLAYER_DOING_STRIP_CLUB_POST_MISSION_SCENE(FALSE)
					ENDIF
					
					CLEANUP_STRIPCLUB_POST_HEIST_CUTSCENE_DATA()
				ENDIF
			ENDIF
			
			UPDATE_STRIPCLUB_POST_HEIST_CUTSCENE()
			HANDLE_MAKE_PEDS_INVISIBLE_DURING_CUTSCENE()
			
			// test is in club and can approach
			IF (stripClubStage = STAGE_DANCE)  // bit hacky but avoids random issue where player someone was determined not in the club
				SET_IN_CLUB_FLAG(TRUE)
			ELIF (stripClubStage = STAGE_GO_TO_VIP_ROOM) //same
			AND (gotoStage > STRIPCLUB_GOTO_ROOM_TRIGGER_CUTSCENE)
				SET_IN_CLUB_FLAG(TRUE)
			ELSE
				SET_IN_CLUB_FLAG(IS_PLAYER_IN_CLUB())
			ENDIF
			
			IF IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_MINIGAME_FRIENDS)
			OR IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())
			OR IS_PED_INJURED(PLAYER_PED_ID())
				SET_HIGH_FUNCTIONALITY(FALSE)
			ELSE
				SET_HIGH_FUNCTIONALITY(TRUE)
			ENDIF
			
			IF NOT IS_HIGH_FUNCTIONALITY()
			AND IS_BITMASK_AS_ENUM_SET(iStripClubBits_General, STRIP_CLUB_GENERAL_DISABLED_DOOR_NAVMESH)
				//DISABLE_NAVMESH_IN_AREA(<<120.88, -1293.98, 27.27>>, <<124.88, -1289.98, 31.27>>, FALSE)
				//DISABLE_NAVMESH_IN_AREA(<<114.13, -1296.64, 27.42>>, <<118.13, -1292.64, 31.42>>, FALSE)					
				CLEAR_BITMASK_AS_ENUM(iStripClubBits_General, STRIP_CLUB_GENERAL_DISABLED_DOOR_NAVMESH)
			ENDIF
			
			IF stripClubStage != STAGE_CLUB_HOSTILE
			    IF SHOULD_CLUB_GO_HOSTILE()
					SET_BITMASK_ENUM_AS_ENUM(iStripClubBits_Workers, STRIP_CLUB_BIT_WORKER_BOUNCER_AGGRO)
					IF NOT IS_ENTITY_IN_STRIPCLUB(PLAYER_PED_ID()) AND IS_HOSTILE_FROM_MELEE()
						CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(GET_PLAYER_INDEX())
						SET_HOSTILE_FROM_MELEE(FALSE)
					ELSE
						CPRINTLN(DEBUG_SCLUB,"Send club hostile")
						SET_PLAYER_BROADCAST_WENT_HOSTILE(clubPlayerBD[PARTICIPANT_ID_TO_INT()], TRUE)
			        	SEND_CLUB_HOSTILE(IS_HOSTILE_FROM_MELEE())
					ENDIF
			    ENDIF
			ENDIF
			
			SET_CAN_APPROACH_FLAG(CAN_APPROACH_STRIPPER())
			
			IF stripClubStage > STAGE_INIT_CLUB
			AND NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bBlockStripClubAssets
				// handle load inside assets
				IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
					HANDLE_INSIDE_ASSETS_SERVER()
				ELSE
					HANDLE_INSIDE_ASSETS_CLIENT()
				ENDIF
			ENDIF
			
			IF stripClubStage > STAGE_INIT_CLUB
			AND IS_ENTITY_IN_STRIPCLUB(PLAYER_PED_ID())
				IF bRequestAudio = FALSE
					IF REQUEST_SCRIPT_AUDIO_BANK("STRIP_CLUB")
						PRINTLN("[SCLUB7] - STRIP_CLUB audio bank has been requested")
						bRequestAudio = TRUE
					ENDIF
				ENDIF
			ENDIF
			
            // run appropriate stage
            SWITCH stripClubStage
				CASE STAGE_INIT_CLUB					 
					IF INIT_STRIP_CLUB(TRUE)
						stripClubStage = STAGE_WANDER_CLUB
					ENDIF
				BREAK
			
				CASE STAGE_WANDER_CLUB
					DO_STAGE_WANDER_CLUB()
				BREAK
				
				CASE STAGE_GO_TO_VIP_ROOM
					DO_STAGE_GO_TO_ROOM_MP()
				BREAK
				
				CASE STAGE_DANCE						
					DO_STAGE_DANCE()
				BREAK
									
                CASE STAGE_CLUB_HOSTILE
					SET_PLAYER_BROADCAST_WENT_HOSTILE(clubPlayerBD[PARTICIPANT_ID_TO_INT()], TRUE)
                	DO_STAGE_CLUB_HOSTILE()
											
//					//all hostile players have been delt with
//					IF IS_STRIPCLUB_CLEAR_OF_HOSTILES(playerBD)
//						stripClubStage = STAGE_RETURN_FROM_HOSTILE
//						
//						IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
//							serverBD.bClubHostile = FALSE
//						ENDIF
//					ENDIF
                BREAK
				
				CASE STAGE_RETURN_FROM_HOSTILE
					DO_STAGE_RETURN_FROM_HOSTILE()
				BREAK
				
				CASE STAGE_GO_HOME_WITH_STRIPPER
				BREAK
            ENDSWITCH
			
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				//CPRINTLN(DEBUG_SCLUB,"Sync data server")
				//DRAW_DEBUG_SPHERE(<<114.64, -1290.34, 29.68>>, 100, 0, 0 ,255, 125)
				SYNC_STRIPCLUB_SERVER()
			ELSE
				//CPRINTLN(DEBUG_SCLUB,"Sync data client")
				SYNC_STRIPCLUB_CLIENT()
			ENDIF
			
			CLEAR_BITMASK_AS_ENUM(iStripClubBits_General2, STRIP_CLUB_GENERAL2_SET_SYNC_CAMERA_THIS_FRAME)
        ELSE
			
			IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
				CPRINTLN(DEBUG_SCLUB,"Stripclub mp end: SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")
				HANDLE_TELEPORT_OUTSIDE_STRIPCLUB_DURING_FORCE_CLEANUP(sLoadQueue, SHOULD_TRANSITION_SESSION_KILL_ALL_MP_SCRIPTS() OR IS_TRANSITION_SESSION_LAUNCHING())
			ELSE
	            // cleanup if too far away
				IF VDIST2(vPlayerCoords, <<114.64, -1290.34, 29.68>>) > (STRIPCLUB_MP_ACTIVATION_RANGE*STRIPCLUB_MP_ACTIVATION_RANGE*1.3)
					CPRINTLN(DEBUG_SCLUB,"Player is too far away, cleaning up script")
				ENDIF
				
				IF IS_PLAYER_ON_ANY_DEATHMATCH(PLAYER_ID())
					CPRINTLN(DEBUG_SCLUB,"Stripclub mp end: IS_PLAYER_ON_ANY_DEATHMATCH()")
				ENDIF
				
				IF NETWORK_IS_IN_TUTORIAL_SESSION()
	           		CPRINTLN(DEBUG_SCLUB,"Stripclub mp end: NETWORK_IS_IN_TUTORIAL_SESSION")
					STRIPCLUB_CLOSE_FRONT_DOOR(TRUE, 0)
					STRIPCLUB_CLOSE_BACK_DOOR(TRUE, 0)
				ENDIF
			ENDIF
			
            CLEANUP_STRIP_CLUB(sLoadQueue)
        ENDIF
    ENDWHILE

ENDSCRIPT
