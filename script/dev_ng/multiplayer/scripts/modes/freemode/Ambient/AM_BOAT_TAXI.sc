//////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_BOAT_TAXI.sc															//
// Description: Player calls for a boat taxi when stranded in the water.				//
// Written by:  Ryan Baker																//
// Date: 04/01/2013																		//
//////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"

// Game Headers
USING "script_player.sch"
USING "commands_event.sch"
USING "commands_network.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_gang_angry.sch"

USING "net_mission.sch"
USING "net_scoring_common.sch"
USING "net_ambience.sch"

USING "commands_object.sch"
USING "net_boat_taxi.sch"
USING "Cheat_Handler.sch"

USING "net_contact_requests.sch"

USING "net_wait_zero.sch"

#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF

// Game States. 
CONST_INT GAME_STATE_INI 				0
CONST_INT GAME_STATE_RUNNING			1
CONST_INT GAME_STATE_LEAVE				2
CONST_INT GAME_STATE_TERMINATE_DELAY	3
CONST_INT GAME_STATE_END				4

CONST_FLOAT BOAT_CREATION_RADIUS		100.0
CONST_FLOAT BOAT_CREATION_HEIGHT		10.0
CONST_INT BOAT_SPEED					12
CONST_FLOAT LEAVE_RANGE					200.0
CONST_FLOAT BOAT_ARRIVED_RANGE			10.0	//25.0
CONST_INT STICK_DEAD_ZONE				28
CONST_INT ENTER_BOAT_AS_PASSENGER_TIME 	275
CONST_INT DIALOGUE_RANGE				15
CONST_INT CREATION_DELAY				10000
CONST_INT SHAPE_TEST_RANGE				10
CONST_INT SHAPE_TEST_RANGE_HEIGHT		25

ENUM BOAT_TAXI_STAGE_ENUM
	eBT_GO_TO_PLAYER,
	eBT_ENTER_BOAT,
	eBT_START_BOAT,
	eBT_GO_TO_DROPOFF,
	eBT_EXIT_BOAT,
	eBT_CLEANUP
ENDENUM

ENUM BOAT_STATE
	BOAT_STATE_WAIT,
	BOAT_STATE_FLEE,
	BOAT_STATE_GO_TO_PLAYER,
	BOAT_STATE_STOP_IN_PLACE,
	BOAT_STATE_GO_TO_DESTINATION,
	BOAT_STATE_ARRIVED_AT_DESTINATION,
	BOAT_STATE_LEAVE,
	BOAT_STATE_CLEANUP	
ENDENUM

//Server Bitset Data
CONST_INT biS_AnyCrewArrested			0
CONST_INT biS_AllCrewArrested			1
CONST_INT biS_AllCrewDead				2
CONST_INT biS_AllCrewLeftStartArea		3
CONST_INT biS_AnyCrewHasFinished 		4
CONST_INT biS_AllCrewHaveFinished 		5
CONST_INT biS_AllCrewHaveLeft 			6
CONST_INT biS_BoatJourneySkipStarted	7
CONST_INT biS_ReachPickup				8
CONST_INT biS_BoatGoStarted				9
CONST_INT biS_ReachDropoff				10
CONST_INT biS_BoatJourneySkipDone		11
CONST_INT biS_GetUpdatedDetails			12

// The server broadcast data.
// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData

	INT iServerBitSet
	INT iServerGameState
	
	NETWORK_INDEX 	niBoat
	NETWORK_INDEX 	niDriver
	PLAYER_INDEX 	MainPlayer
	
	INT 	iZone
	FLOAT 	fDropHeading
	VECTOR 	vDropLocation
	VECTOR 	vPickupLocation
	VECTOR 	vLeaveDestination
	
	SCRIPT_TIMER CreationDelayTimer
	SCRIPT_TIMER MissionTerminateDelayTimer
	
	BOAT_STATE eBoatState 				= BOAT_STATE_GO_TO_PLAYER
	BOAT_TAXI_STAGE_ENUM eBoatTaxiStage = eBT_GO_TO_PLAYER
	
ENDSTRUCT
ServerBroadcastData serverBD


CONST_INT biP_GettingInTaxi			0
CONST_INT biP_ClientinBoatWarp		1
CONST_INT biP_SetupTaxiWarp			2
CONST_INT biP_EndTaxiWarp			3
CONST_INT biP_PressedSkip			4
CONST_INT biP_PressedStop			5
CONST_INT biP_PressedGo				6
CONST_INT biP_DriverFleeing			7

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	INT iGameState
	
	INT iPlayerBitSet
	
	BOAT_TAXI_STAGE_ENUM eBoatTaxiStage = eBT_GO_TO_PLAYER
	
	#IF IS_DEBUG_BUILD
		INT iDebugPassFail = 0
	#ENDIF
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

///// MISSION VARIABLES /////
INT iBoolsBitSet
INT iIdleDialogueBitSet
CONST_INT biInitialSetup			0
CONST_INT biHelpDisplayed			1
CONST_INT biShowEnterButton			2
CONST_INT biExitBoat				3
CONST_INT biArrivalDialogue			4
CONST_INT biBoatNearDestDialogue	5
CONST_INT biPlayerInDialogue		6
CONST_INT biPlayerGetsOutDialogue	7
CONST_INT biPlayerJumpedOutDialogue	8
CONST_INT biPlayerIdleDialogue		9
CONST_INT biStartedShapeTest		10

BLIP_INDEX 		BoatBlip

SEQUENCE_INDEX 	seqFlee
SEQUENCE_INDEX 	seqBoatLeave
SEQUENCE_INDEX 	seqBoatGoToDrop

//BOOL bStickInUseUp

SCALEFORM_INDEX buttonMovie
SCALEFORM_INSTRUCTIONAL_BUTTONS instructionalButtons

SHAPETEST_INDEX ShapeTestIndex
INT iShapeTestResult
VECTOR vBoatCreationPos
FLOAT fBoatCreationHeading
			
structPedsForConversation thisSpeech
BOOL bInTutorialSession

CONST_INT IDLE_TIME	30000
SCRIPT_TIMER IdleTimer

INT iRandomChatDelay = 15000
SCRIPT_TIMER RandomChatTimer

FLOAT fMinX, fMaxX, fMinY, fMaxY

// Widgets & Debug.
#IF IS_DEBUG_BUILD
	BOOL bHostEndMissionNow
	//BLIP_INDEX DropoffBlipIndex[BOAT_TAXI_DROPOFF_LOCATONS]
#ENDIF

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MAIN PROCS                            ////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//PURPOSE: Create the sequences used in this script
PROC CREATE_SEQUENCES()
	IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niBoat)
		
		OPEN_SEQUENCE_TASK(seqBoatGoToDrop)
			TASK_BOAT_MISSION(NULL, NET_TO_VEH(serverBD.niBoat), NULL, NULL, serverBD.vDropLocation, MISSION_GOTO, BOAT_SPEED, DRIVINGMODE_AVOIDCARS, BOAT_ARRIVED_RANGE, BCF_BOATTAXISETTINGS)
		CLOSE_SEQUENCE_TASK(seqBoatGoToDrop)
		
		OPEN_SEQUENCE_TASK(seqBoatLeave)
			TASK_VEHICLE_MISSION_COORS_TARGET(NULL, NET_TO_VEH(serverBD.niBoat), serverBD.vLeaveDestination, MISSION_GOTO, BOAT_SPEED, DRIVINGMODE_AVOIDCARS, 0.0, 0.0)
			TASK_VEHICLE_DRIVE_WANDER(NULL, NET_TO_VEH(serverBD.niBoat), BOAT_SPEED, DRIVINGMODE_AVOIDCARS)
		CLOSE_SEQUENCE_TASK(seqBoatLeave)
		
		OPEN_SEQUENCE_TASK(seqFlee)
			TASK_LEAVE_ANY_VEHICLE(NULL, 0, ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)
			//TASK_SMART_FLEE_COORD(NULL, GET_ENTITY_COORDS(NET_TO_PED(serverBD.niDriver)), 300.0, INFINITE_TASK_TIME)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 300.0, INFINITE_TASK_TIME)
			ELSE
				TASK_WANDER_STANDARD(NULL)
			ENDIF
		CLOSE_SEQUENCE_TASK(seqFlee)
		
	ENDIF
		
	NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - SEQUENCES CREATED ") NET_PRINT_INT(200) NET_NL()
ENDPROC

//PURPOSE: Destroys all the sequences created for this script
PROC CLEAR_SEQUENCES()
	CLEAR_SEQUENCE_TASK(seqFlee)
	CLEAR_SEQUENCE_TASK(seqBoatLeave)
	CLEAR_SEQUENCE_TASK(seqBoatGoToDrop)
ENDPROC

/// PURPOSE:
///    Returns closest drop off location for specified map zone (map quarter).
/// PARAMS:
///    iZone - Map zone, 0 - north east quarter, 1 - south east quarter, 2 - south west quarter, 3 - north west quarter.
/// RETURNS:
///    Vector that is the closest drop off location for specified map zone.
FUNC VECTOR	GET_CLOSEST_DROP_LOCATION(INT iZone)

	INT 	i
	INT		iClosestPosition
	VECTOR 	vClosestPosition
	FLOAT 	fCurrentDistance	= 0.0
	FLOAT	fShortestDistance	= 10000.0
	
	SWITCH iZone
		CASE 0	//1st quarter of the map, clockwise
		
			FOR i = 0 TO 4
			
				fCurrentDistance = GET_DISTANCE_BETWEEN_COORDS(serverBD.vPickupLocation, GET_BOAT_TAXI_DROPOFF_VECTOR(i))
				
				IF ( fCurrentDistance < fShortestDistance )
					iClosestPosition 	= i
					fShortestDistance 	= fCurrentDistance
				ENDIF
			
			ENDFOR

			vClosestPosition 			= GET_BOAT_TAXI_DROPOFF_VECTOR(iClosestPosition)
			serverBD.fDropHeading 		= GET_BOAT_TAXI_DROPOFF_HEADING(iClosestPosition)
			serverBD.fDropHeading 		= GET_BOAT_TAXI_DROPOFF_HEADING(iClosestPosition)
			serverBD.vLeaveDestination 	= << serverBD.vPickupLocation.x, 8000.0, 0.0 >>
			
			NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - GET_CLOSEST_DROP_LOCATION - ZONE 0: LOCATION: ") NET_PRINT_INT(iClosestPosition) NET_NL()
			
			RETURN vClosestPosition
			
		BREAK
		
		CASE 1	//2nd quarter of the map, clockwise
			
			FOR i = 5 TO 9
			
				fCurrentDistance = GET_DISTANCE_BETWEEN_COORDS(serverBD.vPickupLocation, GET_BOAT_TAXI_DROPOFF_VECTOR(i))
				
				IF ( fCurrentDistance < fShortestDistance )
					iClosestPosition 	= i
					fShortestDistance 	= fCurrentDistance
				ENDIF
			
			ENDFOR
			
			vClosestPosition 			= GET_BOAT_TAXI_DROPOFF_VECTOR(iClosestPosition)
			serverBD.fDropHeading 		= GET_BOAT_TAXI_DROPOFF_HEADING(iClosestPosition)
			serverBD.fDropHeading 		= GET_BOAT_TAXI_DROPOFF_HEADING(iClosestPosition)			
			serverBD.vLeaveDestination 	= << 5000.0, serverBD.vPickupLocation.y, 0.0 >>
			
			NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - GET_CLOSEST_DROP_LOCATION - ZONE 1: LOCATION: ") NET_PRINT_INT(iClosestPosition) NET_NL()
			
			RETURN vClosestPosition
		
		BREAK

		CASE 2	//3rd quarter of the map, clockwise
		
			FOR i = 10 TO 14
			
				fCurrentDistance = GET_DISTANCE_BETWEEN_COORDS(serverBD.vPickupLocation, GET_BOAT_TAXI_DROPOFF_VECTOR(i))
				
				IF ( fCurrentDistance < fShortestDistance )
					iClosestPosition 	= i
					fShortestDistance 	= fCurrentDistance
				ENDIF
			
			ENDFOR
			
			vClosestPosition 			= GET_BOAT_TAXI_DROPOFF_VECTOR(iClosestPosition)
			serverBD.fDropHeading 		= GET_BOAT_TAXI_DROPOFF_HEADING(iClosestPosition)
			serverBD.fDropHeading 		= GET_BOAT_TAXI_DROPOFF_HEADING(iClosestPosition)	
			serverBD.vLeaveDestination 	= << serverBD.vPickupLocation.x, -5000.0, 0.0 >>
			
			NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - GET_CLOSEST_DROP_LOCATION - ZONE 2: LOCATION: ") NET_PRINT_INT(iClosestPosition) NET_NL()
			
			RETURN vClosestPosition
		
		BREAK
		
		CASE 3	//4th quarter of the map, clockwise
		
			FOR i = 15 TO 20
			
				fCurrentDistance = GET_DISTANCE_BETWEEN_COORDS(serverBD.vPickupLocation, GET_BOAT_TAXI_DROPOFF_VECTOR(i))
				
				IF ( fCurrentDistance < fShortestDistance )
					iClosestPosition 	= i
					fShortestDistance 	= fCurrentDistance
				ENDIF
			
			ENDFOR
			
			vClosestPosition 			= GET_BOAT_TAXI_DROPOFF_VECTOR(iClosestPosition)
			serverBD.fDropHeading 		= GET_BOAT_TAXI_DROPOFF_HEADING(iClosestPosition)
			serverBD.fDropHeading 		= GET_BOAT_TAXI_DROPOFF_HEADING(iClosestPosition)	
			serverBD.vLeaveDestination 	= << -5000.0, serverBD.vPickupLocation.y, 0.0 >>
			
			NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - GET_CLOSEST_DROP_LOCATION - ZONE 3: LOCATION: ") NET_PRINT_INT(iClosestPosition) NET_NL()
			
			RETURN vClosestPosition
		
		BREAK
		
	ENDSWITCH
	
	NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - GET_CLOSEST_DROP_LOCATION - DIDN'T FIND LOCATION - USING DEFAULT ") NET_NL()
	RETURN <<-1392.4283, -1661.6100, 0.0>>
ENDFUNC

// Do necessary pre game start ini.
// Returns FALSE if the script fails to receive an initial network broadcast.
FUNC BOOL PROCESS_PRE_GAME( MP_MISSION_DATA missionScriptArgs )
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(NUM_NETWORK_PLAYERS, missionScriptArgs)	//GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mission) ,  missionScriptArgs)
	//NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, PARTICIPANT_ID_TO_INT())  
	
	//Reserve Entities
	RESERVE_NETWORK_MISSION_PEDS(1)
	RESERVE_NETWORK_MISSION_VEHICLES(1)

	// This makes sure the net script is active, waits untill it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		RETURN FALSE
	ENDIF
			
	// This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		serverBD.MainPlayer = PLAYER_ID()
		serverBD.iZone = GET_PLAYER_BOAT_TAXI_ZONE()
		serverBD.vPickupLocation = (GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)+<<0, 5, 0>>)
		serverBD.vDropLocation = GET_CLOSEST_DROP_LOCATION(serverBD.iZone)

		
		NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - MainPlayer = ") NET_PRINT(GET_PLAYER_NAME(serverBD.MainPlayer)) NET_NL()
		NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - iZone = ") NET_PRINT_INT(serverBD.iZone) NET_NL()
		NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - vPickupLocation = ") NET_PRINT_VECTOR(serverBD.vPickupLocation) NET_NL()
		NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - vDropLocation = ") NET_PRINT_VECTOR(serverBD.vDropLocation) NET_NL()
	ENDIF
	
	SET_BIT(iBoolsBitSet, biPlayerJumpedOutDialogue)
	bInTutorialSession = NETWORK_IS_IN_TUTORIAL_SESSION()
	
	playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_INI
	NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - PRE_GAME DONE - dd ") NET_NL()
	
	RETURN TRUE
ENDFUNC

//PURPOSE: Ends the script if the player is no longer at a valid pickup point (i.e. on land)
PROC END_SCRIPT_IF_PLAYER_AT_INVALID_PICKUP_POINT()
	IF serverBD.eBoatTaxiStage <= eBT_START_BOAT
	OR serverBD.eBoatState = BOAT_STATE_GO_TO_PLAYER
	OR serverBD.eBoatState = BOAT_STATE_STOP_IN_PLACE
		VECTOR vPlyrPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		FLOAT fTemp
		IF NOT GET_WATER_HEIGHT_NO_WAVES(vPlyrPos, fTemp)
		OR AT_BOAT_TAXI_DROPOFF()
		OR BOAT_TAXI_REQUESTED_IN_LAKE()
			serverBD.iServerGameState = GAME_STATE_END
			NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - serverBD.iServerGameState = GAME_STATE_END 9") NET_NL()
			#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 9")	#ENDIF
		ENDIF
	ENDIF
ENDPROC

//Player leaves area
FUNC BOOL MISSION_END_CHECK()

	//player entered a job corona
	IF IS_PLAYER_IN_CORONA()
		IF serverBD.eBoatTaxiStage >= eBT_GO_TO_PLAYER
		OR serverBD.eBoatTaxiStage <= eBT_GO_TO_DROPOFF
			NETWORK_REFUND_CASH_TYPE(GET_CONTACT_REQUEST_COST(REQUEST_BOAT_PICKUP), MP_REFUND_TYPE_BOAT_PICKUP, MP_REFUND_REASON_NOT_USED, TRUE)
		ENDIF
		NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - MISSION END - PLAYER IN JOB CORONA ") NET_NL()
		RETURN TRUE
	ENDIF
		
	IF serverBD.eBoatTaxiStage >= eBT_GO_TO_PLAYER
	OR serverBD.eBoatTaxiStage <= eBT_GO_TO_DROPOFF
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niBoat)
			
			//boat is dead
			IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.niBoat)
			
				NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - MISSION END - BOAT NOT DRIVEABLE ") NET_NL()
				
				WEAPON_TYPE eCurrentWeapon
				IF NETWORK_GET_DESTROYER_OF_NETWORK_ID(serverBD.niBoat, eCurrentWeapon) = PLAYER_ID()
					SET_GANG_ANGRY_AT_PLAYER(GANG_CALL_TYPE_SPECIAL_MERRYWEATHER, GANG_ANGRY_TIME_LONG)
				ENDIF
				
				RETURN TRUE
			ENDIF
		ELSE
		
			//boat does no exist, was cleaned up
			RETURN TRUE
		ENDIF
		
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niDriver)
		
			//driver is dead
			IF IS_NET_PED_INJURED(serverBD.niDriver)
				NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - MISSION END - DRIVER DEAD ") NET_NL()
				
				WEAPON_TYPE eCurrentWeapon
				IF NETWORK_GET_DESTROYER_OF_NETWORK_ID(serverBD.niDriver, eCurrentWeapon) = PLAYER_ID()
					SET_GANG_ANGRY_AT_PLAYER(GANG_CALL_TYPE_SPECIAL_MERRYWEATHER, GANG_ANGRY_TIME_LONG)
				ENDIF
				
				RETURN TRUE
			ENDIF
		ELSE
		
			//driver does no exist, was cleaned up
			RETURN TRUE
		ENDIF
		
		//player is dead
		IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
			NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - MISSION END - PLAYER DEAD ") NET_NL()
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Server only function that checks to see if the game mode should now end. For this test script, it if time has passed.
FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET()
	
	#IF IS_DEBUG_BUILD
		IF bHostEndMissionNow
			RETURN TRUE
		ENDIF
	#ENDIF	
	
	IF MISSION_END_CHECK()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


//Helper function to get a clients game/mission state
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iGameState
ENDFUNC

//Helper function to get the servers game/mission state
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC

#IF IS_DEBUG_BUILD

	PROC CREATE_WIDGETS()
		INT iPlayer
		TEXT_LABEL_15 tl63

		START_WIDGET_GROUP("BOAT TAXI")  
		
			ADD_WIDGET_INT_READ_ONLY("iZone", serverBD.iZone)
			ADD_WIDGET_VECTOR_SLIDER("vPickupLocation", serverBD.vPickupLocation, -99999, 99999, 0)
			ADD_WIDGET_VECTOR_SLIDER("vDropLocation", serverBD.vDropLocation, -99999, 99999, 0)
			
			// Gameplay debug, sever only.
			START_WIDGET_GROUP("Server Only Gameplay") 
				ADD_WIDGET_BOOL("End mission", bHostEndMissionNow)			
			STOP_WIDGET_GROUP()		
			
			// Data about the server
			START_WIDGET_GROUP("Server BD") 
					ADD_WIDGET_INT_SLIDER("S. Game state", serverBD.iServerGameState,-1, HIGHEST_INT,1)
			STOP_WIDGET_GROUP()	

			// Data about the clients. * = You.
			START_WIDGET_GROUP("Client BD")  				
				REPEAT NUM_NETWORK_PLAYERS iPlayer
					tl63 = "Player "
					tl63 += iPlayer
					IF iPlayer = PARTICIPANT_ID_TO_INT()
						tl63 += "*"
					ENDIF
					START_WIDGET_GROUP(tl63)
						ADD_WIDGET_INT_SLIDER("Game state", playerBD[iPlayer].iGameState,-1, HIGHEST_INT,1)
					STOP_WIDGET_GROUP()
				ENDREPEAT				
			STOP_WIDGET_GROUP()	
			
		STOP_WIDGET_GROUP()
	ENDPROC		

	PROC UPDATE_WIDGETS()
		/*
		//draw all possible dropoff positions
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)

		INT k

		FOR k = 0 TO ( BOAT_TAXI_DROPOFF_LOCATONS - 1 )
			DRAW_DEBUG_SPHERE(GET_BOAT_TAXI_DROPOFF_VECTOR(k), 2.0, 0, 255, 0)
			DRAW_DEBUG_TEXT_ABOVE_COORDS(GET_BOAT_TAXI_DROPOFF_VECTOR(k), GET_STRING_FROM_INT(k), 6.0)
			DRAW_DEBUG_TEXT_ABOVE_COORDS(GET_BOAT_TAXI_DROPOFF_VECTOR(k), GET_STRING_FROM_VECTOR(GET_BOAT_TAXI_DROPOFF_VECTOR(k)), 2.5)
			IF NOT DOES_BLIP_EXIST(DropoffBlipIndex[k])	DropoffBlipIndex[k] = ADD_BLIP_FOR_COORD(GET_BOAT_TAXI_DROPOFF_VECTOR(k))ENDIF
		ENDFOR
		
		//draw line from boat to selected drop off position
		IF serverBD.eBoatState = BOAT_STATE_GO_TO_DESTINATION
			IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.niBoat))
				DRAW_DEBUG_LINE(GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niBoat)), serverBD.vDropLocation, 0, 255, 0)
			ENDIF	
		ENDIF
		*/
		
	ENDPROC

#ENDIF

/// PURPOSE:
///    Controls boat radio allowing player to change radio stations when sitting in passenger seat of the boat.
PROC CONTROL_BOAT_RADIO()

	SET_AUDIO_FLAG("MobileRadioInGame", FALSE)
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF 	IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niBoat))
		AND	NOT IS_PED_SITTING_IN_VEHICLE_SEAT(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niBoat), VS_DRIVER)
			SET_AUDIO_FLAG("MobileRadioInGame", TRUE)
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Controls boat driver dialogue.
PROC CONTROL_DRIVER_DIALOGUE()

	//ARRIVE AT PLAYER
	IF NOT IS_BIT_SET(iBoolsBitSet, biArrivalDialogue)
		IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), NET_TO_PED(serverBD.niDriver), <<DIALOGUE_RANGE, DIALOGUE_RANGE, DIALOGUE_RANGE>>)
			ADD_PED_FOR_DIALOGUE(thisSpeech, 8, NET_TO_PED(serverBD.niDriver), "FM_DRIVER_BOAT")
			IF CREATE_CONVERSATION_USING_V_CONTENT_IN_DLC(thisSpeech, "FMAMBAU", "FMA_BPDA", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)
				TASK_LOOK_AT_ENTITY(NET_TO_PED(serverBD.niDriver), PLAYER_PED_ID(), 5000)
				SET_BIT(iBoolsBitSet, biArrivalDialogue)
			ENDIF
		ENDIF
	ENDIF
	
	//PLAYER GETS IN BOAT
	IF NOT IS_BIT_SET(iBoolsBitSet, biPlayerInDialogue)
		IF serverBD.eBoatState = BOAT_STATE_GO_TO_DESTINATION
			ADD_PED_FOR_DIALOGUE(thisSpeech, 8, NET_TO_PED(serverBD.niDriver), "FM_DRIVER_BOAT")
			IF CREATE_CONVERSATION_USING_V_CONTENT_IN_DLC(thisSpeech, "FMAMBAU", "FMA_BPDA3", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)
				TASK_LOOK_AT_ENTITY(NET_TO_PED(serverBD.niDriver), PLAYER_PED_ID(), 5000)
				SET_BIT(iBoolsBitSet, biPlayerInDialogue)
			ENDIF
		ENDIF
	ENDIF
	
	//BOAT NEAR DESTINATION
	IF NOT IS_BIT_SET(iBoolsBitSet, biBoatNearDestDialogue)
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), serverBD.vDropLocation, <<BOAT_ARRIVED_RANGE*2, BOAT_ARRIVED_RANGE*2, BOAT_ARRIVED_RANGE*2>>)
			ADD_PED_FOR_DIALOGUE(thisSpeech, 8, NET_TO_PED(serverBD.niDriver), "FM_DRIVER_BOAT")
			IF CREATE_CONVERSATION_USING_V_CONTENT_IN_DLC(thisSpeech, "FMAMBAU", "FMA_BPDA6", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)
				TASK_LOOK_AT_ENTITY(NET_TO_PED(serverBD.niDriver), PLAYER_PED_ID(), 5000)
				SET_BIT(iBoolsBitSet, biBoatNearDestDialogue)
			ENDIF
		ENDIF
	ENDIF
	
	//PLAYER GETS OUT AT DESTINATION
	IF NOT IS_BIT_SET(iBoolsBitSet, biPlayerGetsOutDialogue)
		IF serverBD.eBoatState = BOAT_STATE_ARRIVED_AT_DESTINATION
			ADD_PED_FOR_DIALOGUE(thisSpeech, 8, NET_TO_PED(serverBD.niDriver), "FM_DRIVER_BOAT")
			IF CREATE_CONVERSATION_USING_V_CONTENT_IN_DLC(thisSpeech, "FMAMBAU", "FMA_BPDA7", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)
				TASK_LOOK_AT_ENTITY(NET_TO_PED(serverBD.niDriver), PLAYER_PED_ID(), 5000)
				SET_BIT(iBoolsBitSet, biPlayerGetsOutDialogue)	
			ENDIF
		ENDIF
	ENDIF
	
	//PLAYER JUMPS OUT MID JOURNEY
	IF NOT IS_BIT_SET(iBoolsBitSet, biPlayerJumpedOutDialogue)
		ADD_PED_FOR_DIALOGUE(thisSpeech, 8, NET_TO_PED(serverBD.niDriver), "FM_DRIVER_BOAT")
		IF CREATE_CONVERSATION_USING_V_CONTENT_IN_DLC(thisSpeech, "FMAMBAU", "FMA_BPDA4", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)
			TASK_LOOK_AT_ENTITY(NET_TO_PED(serverBD.niDriver), PLAYER_PED_ID(), 5000)
			SET_BIT(iBoolsBitSet, biPlayerJumpedOutDialogue)
		ENDIF
	ENDIF
	
	//PLAYER IDLE LINE
	IF serverBD.eBoatState = BOAT_STATE_STOP_IN_PLACE
		IF HAS_NET_TIMER_EXPIRED(IdleTimer, IDLE_TIME)
			ADD_PED_FOR_DIALOGUE(thisSpeech, 8, NET_TO_PED(serverBD.niDriver), "FM_DRIVER_BOAT")
			IF CREATE_CONVERSATION_USING_V_CONTENT_IN_DLC(thisSpeech, "FMAMBAU", "FMA_BPDA2", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)
				TASK_LOOK_AT_ENTITY(NET_TO_PED(serverBD.niDriver), PLAYER_PED_ID(), 5000)
				REINIT_NET_TIMER(IdleTimer)
			ENDIF
		ENDIF
	ENDIF
	
	//RANDOM IDLE DIALOGUE WHEN GOING TO DESTINATION
	IF serverBD.eBoatState = BOAT_STATE_GO_TO_DESTINATION
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) <= 0
			IF HAS_NET_TIMER_EXPIRED(RandomChatTimer, iRandomChatDelay)
			
				INT iRand 					= GET_RANDOM_INT_IN_RANGE(0, 17)
				TEXT_LABEL_15 IdleChatRoot	= ""
				
				IF NOT IS_BIT_SET(iIdleDialogueBitSet, iRand)
				
					SWITCH iRand
						CASE 0
							IdleChatRoot = "FMA_BPDA5"
						BREAK
						CASE 1
							IdleChatRoot = "FMA_BPDA5b"
						BREAK
						CASE 2
							IdleChatRoot = "FMA_BPDA5c"
						BREAK
						CASE 3
							IdleChatRoot = "FMA_BPDA5d"
						BREAK
						CASE 4
							IdleChatRoot = "FMA_BPDA5e"
						BREAK
						CASE 5
							IdleChatRoot = "FMA_BPDA5f"
						BREAK
						CASE 6
							IdleChatRoot = "FMA_BPDA5g"
						BREAK
						CASE 7
							IdleChatRoot = "FMA_BPDA5h"
						BREAK
						CASE 8
							IdleChatRoot = "FMA_BPDA5i"
						BREAK
						CASE 9
							IdleChatRoot = "FMA_BPDA5j"
						BREAK
						CASE 10
							IdleChatRoot = "FMA_BPDA5k"
						BREAK
						CASE 11
							IdleChatRoot = "FMA_BPDA5l"
						BREAK
						CASE 12
							IdleChatRoot = "FMA_BPDA5m"
						BREAK
						CASE 13
							IdleChatRoot = "FMA_BPDA5n"
						BREAK
						CASE 14
							IdleChatRoot = "FMA_BPDA5o"
						BREAK
						CASE 15
							IdleChatRoot = "FMA_BPDA5p"
						BREAK
						CASE 16
							IdleChatRoot = "FMA_BPDA5q"
						BREAK
					ENDSWITCH
	
					IF NOT IS_STRING_NULL_OR_EMPTY(IdleChatRoot)
						ADD_PED_FOR_DIALOGUE(thisSpeech, 8, NET_TO_PED(serverBD.niDriver), "FM_DRIVER_BOAT")
						IF CREATE_CONVERSATION_USING_V_CONTENT_IN_DLC(thisSpeech, "FMAMBAU", IdleChatRoot, CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)
							
							REINIT_NET_TIMER(RandomChatTimer)
							
							SET_BIT(iIdleDialogueBitSet, iRand)
							
							TASK_LOOK_AT_ENTITY(NET_TO_PED(serverBD.niDriver), PLAYER_PED_ID(), 5000)
						
							iRandomChatDelay = GET_RANDOM_INT_IN_RANGE(20000, 30001) + (iRand * 500)
							
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
	
	//AMBIENT SPEECH FOR DRIVER WHEN PLAYER ATTACKS THE DRIVER OR THE BOAT
	IF NOT IS_NET_PED_INJURED(serverBD.niDriver)
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niBoat)
			IF 	NOT IS_AMBIENT_SPEECH_PLAYING(NET_TO_PED(serverBD.niDriver))
			AND	NOT IS_SCRIPTED_SPEECH_PLAYING(NET_TO_PED(serverBD.niDriver))
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(NET_TO_VEH(serverBD.niBoat), PLAYER_PED_ID())
				OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(NET_TO_PED(serverBD.niDriver), PLAYER_PED_ID())
					SWITCH GET_RANDOM_INT_IN_RANGE(0, 6)
						CASE 0
							PLAY_PED_AMBIENT_SPEECH(NET_TO_PED(serverBD.niDriver), "GENERIC_SHOCKED_MED", SPEECH_PARAMS_FORCE)
						BREAK
						CASE 1
							PLAY_PED_AMBIENT_SPEECH(NET_TO_PED(serverBD.niDriver), "GENERIC_SHOCKED_HIGH", SPEECH_PARAMS_FORCE)
						BREAK
						CASE 2
							PLAY_PED_AMBIENT_SPEECH(NET_TO_PED(serverBD.niDriver), "GENERIC_FRIGHTENED_MED", SPEECH_PARAMS_FORCE)
						BREAK
						CASE 3
							PLAY_PED_AMBIENT_SPEECH(NET_TO_PED(serverBD.niDriver), "GENERIC_FRIGHTENED_HIGH", SPEECH_PARAMS_FORCE)
						BREAK
						CASE 4
							PLAY_PED_AMBIENT_SPEECH(NET_TO_PED(serverBD.niDriver), "GENERIC_CURSE_MED", SPEECH_PARAMS_FORCE)
						BREAK
						CASE 5
							PLAY_PED_AMBIENT_SPEECH(NET_TO_PED(serverBD.niDriver), "GENERIC_CURSE_HIGH", SPEECH_PARAMS_FORCE)
						BREAK
					ENDSWITCH
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(NET_TO_VEH(serverBD.niBoat))
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(NET_TO_PED(serverBD.niDriver))
				ENDIF
			ELSE
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(NET_TO_VEH(serverBD.niBoat))
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(NET_TO_PED(serverBD.niDriver))
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  ENTITY CREATION                  /////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE:
///    Gets a random vector within a donut centred at vPos with a radius of fRadius and a height of fHeight.
/// PARAMS:
///    vPos - The sphere disc
///    fInnerRadius - Radius of the hole in the middle
///    fOuterRadius - Radius of the donut
///    fHeight - Height of the disc (goes both above and below vPos)
/// RETURNS:
///    A random position vector within fRadius of vPos.
FUNC VECTOR GET_RANDOM_POINT_IN_DONUT(VECTOR vThisPos, FLOAT fInnerRadius, FLOAT fOuterRadius, FLOAT fHeight)
 
	// Pick a random unit direction vector.
	VECTOR vDir = << GET_RANDOM_FLOAT_IN_RANGE(-1,1) , GET_RANDOM_FLOAT_IN_RANGE(-1,1), 0 >>
	FLOAT fHalfHeight = fHeight / 2.0
	
	// Project this vector into the disc some random distance.
	vDir = GET_VECTOR_OF_LENGTH(vDir, GET_RANDOM_FLOAT_IN_RANGE(fInnerRadius, fOuterRadius))
	
	// Grab a random height and add it on.
	vDir.z = GET_RANDOM_FLOAT_IN_RANGE(-fHalfHeight, fHalfHeight)
	
	RETURN vThisPos + vDir
ENDFUNC

//PURPOSE: Calculates a position to Create the boat at
PROC GET_BOAT_CREATION_POINT(VECTOR &vThisPos, FLOAT &fThisHeading)
	vThisPos = GET_RANDOM_POINT_IN_DONUT(serverBD.vPickupLocation, BOAT_CREATION_RADIUS/2, BOAT_CREATION_RADIUS, BOAT_CREATION_HEIGHT)
	fThisHeading = GET_HEADING_FROM_VECTOR_2D(serverBD.vPickupLocation.x-vThisPos.x, serverBD.vPickupLocation.y-vThisPos.y) //GET_HEADING_BETWEEN_VECTORS_2D(vPos, serverBD.vDropLocation)
ENDPROC

PROC CALCULATE_PATH_NODES_AREA(VECTOR vPosition, VECTOR vDestination)

	IF ( vPosition.x < vDestination.x )
		fMinX = vPosition.x - 25.0
		fMaxX = vDestination.x + 25.0
	ELIF ( vPosition.x > vDestination.x )
		fMinX = vDestination.x - 25.0
		fMaxX = vPosition.x + 25.0
	ENDIF
	
	IF ( vPosition.y < vDestination.y )
		fMinY = vPosition.y - 25.0
		fMaxY = vDestination.y + 25.0
	ELIF ( vPosition.y > vDestination.y )
		fMinY = vDestination.y - 25.0
		fMaxY = vPosition.y + 25.0
	ENDIF

ENDPROC

///////////////////////////////////////////
///    		BOAT AND DRIVER    			///
///////////////////////////////////////////
//PURPOSE: Create the Boat
FUNC BOOL CREATE_BOAT()
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niBoat)
		IF REQUEST_LOAD_MODEL(DINGHY2)
			
			
			VECTOR vTemp
			ENTITY_INDEX hitEntity
			
			IF NOT IS_BIT_SET(iBoolsBitSet, biStartedShapeTest)
			
				GET_BOAT_CREATION_POINT(vBoatCreationPos, fBoatCreationHeading)
			
				//Make sure creation point is on the water
				//GET_WATER_HEIGHT(vBoatCreationPos, vBoatCreationPos.z)
				IF TEST_PROBE_AGAINST_WATER(vBoatCreationPos+<<0, 0, 2>>, vBoatCreationPos-<<0, 0, BOAT_CREATION_HEIGHT>>, vBoatCreationPos)
					IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(vBoatCreationPos, 6, 1, 1, 5)
						
						VECTOR vTestPos = (vBoatCreationPos+<<0, 0, (SHAPE_TEST_RANGE_HEIGHT/2)>>)
						ShapeTestIndex = START_SHAPE_TEST_BOX(vTestPos, <<SHAPE_TEST_RANGE, SHAPE_TEST_RANGE, SHAPE_TEST_RANGE_HEIGHT>>, <<0, 0, -1>>, EULER_YXZ, SCRIPT_INCLUDE_MOVER)
						NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - CREATE_BOAT - START_SHAPE_TEST_BOX - DONE AT") NET_PRINT_VECTOR(vTestPos) NET_NL()
						
						IF ShapeTestIndex = NULL
							NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - CREATE_BOAT - FAILED TO START SHAPE TEST - ShapeTestIndex = 0") NET_NL()
							RETURN FALSE
						ELSE
							SET_BIT(iBoolsBitSet, biStartedShapeTest)
							NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - CREATE_BOAT - STARTED SHAPE TEST") NET_NL()
						ENDIF
					ENDIF
				ENDIF
				
			//Check Shape Test
			ELSE
				IF GET_SHAPE_TEST_RESULT(ShapeTestIndex, iShapeTestResult, vTemp, vTemp, hitEntity) = SHAPETEST_STATUS_RESULTS_READY
					ShapeTestIndex  = NULL
					
					IF iShapeTestResult = 0
						NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - CREATE_BOAT - TRUE - iShapeTestResult = ") NET_PRINT_INT(iShapeTestResult)  NET_NL()
						IF CREATE_NET_VEHICLE(serverBD.niBoat, DINGHY2, vBoatCreationPos, fBoatCreationHeading)
							//SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(NET_TO_VEH(serverBD.niBoat), TRUE)
							//SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.niBoat), PLAYER_ID(), FALSE)
							SET_VEHICLE_DOORS_LOCKED_FOR_NON_SCRIPT_PLAYERS(NET_TO_VEH(serverBD.niBoat), TRUE)
							//SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.niBoat), PLAYER_ID(), FALSE)
							//SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(NET_TO_VEH(serverBD.niBoat), ENUM_TO_INT(SC_DOOR_FRONT_LEFT), VEHICLELOCK_LOCKED)
							
							SET_VEHICLE_ENGINE_ON(NET_TO_VEH(serverBD.niBoat), TRUE, TRUE)
							SET_VEHICLE_FORWARD_SPEED(NET_TO_VEH(serverBD.niBoat), BOAT_SPEED) 
							
							SET_DISABLE_PRETEND_OCCUPANTS(NET_TO_VEH(serverBD.niBoat), TRUE)
							
							SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_VEH(serverBD.niBoat), FALSE)
							
							NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - CREATE_BOAT - CREATED BOAT") NET_NL()
						ENDIF
					ELSE
						NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - CREATE_BOAT - FALSE - iShapeTestResult = ") NET_PRINT_INT(iShapeTestResult)  NET_NL()
						iShapeTestResult = 0
						CLEAR_BIT(iBoolsBitSet, biStartedShapeTest)
					ENDIF
				ELIF GET_SHAPE_TEST_RESULT(ShapeTestIndex, iShapeTestResult, vTemp, vTemp, hitEntity) = SHAPETEST_STATUS_NONEXISTENT
					NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - CREATE_BOAT - FALSE - iShapeTestResult = SHAPETEST_STATUS_NONEXISTENT ") NET_PRINT_INT(iShapeTestResult)  NET_NL()
					ShapeTestIndex = NULL
					CLEAR_BIT(iBoolsBitSet, biStartedShapeTest)
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
	
	//Check we have created the vehicle
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niBoat)
		NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - FAILING ON BOAT CREATION") NET_NL()
		RETURN FALSE
	ENDIF
	
	SET_MODEL_AS_NO_LONGER_NEEDED(DINGHY2)
	RETURN TRUE
ENDFUNC

//PURPOSE: Clears the boats task
PROC CLEAR_BOAT_TASK()
	IF NOT IS_NET_PED_INJURED(serverBD.niDriver)
		CLEAR_PED_TASKS(NET_TO_PED(serverBD.niDriver))
		NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - CLEAR_BOAT_TASK - DONE ") NET_NL()
	ENDIF
ENDPROC

//PURPOSE: Create the Driver
FUNC BOOL CREATE_DRIVER()
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niDriver)
	AND REQUEST_LOAD_MODEL(S_M_Y_BLACKOPS_01)
	AND NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niBoat)
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niBoat)
			IF CREATE_NET_PED_IN_VEHICLE(serverBD.niDriver, serverBD.niBoat, PEDTYPE_CRIMINAL, S_M_Y_BLACKOPS_01, VS_DRIVER)
			
				SET_PED_COMPONENT_VARIATION(NET_TO_PED(serverBD.niDriver), PED_COMP_HEAD, 		0, 1)
				SET_PED_COMPONENT_VARIATION(NET_TO_PED(serverBD.niDriver), PED_COMP_HAIR, 		1, 0)
				SET_PED_COMPONENT_VARIATION(NET_TO_PED(serverBD.niDriver), PED_COMP_TORSO, 		0, 1)
				SET_PED_COMPONENT_VARIATION(NET_TO_PED(serverBD.niDriver), PED_COMP_LEG, 		0, 2)
				SET_PED_COMPONENT_VARIATION(NET_TO_PED(serverBD.niDriver), PED_COMP_SPECIAL, 	1, 0)
				SET_PED_COMPONENT_VARIATION(NET_TO_PED(serverBD.niDriver), PED_COMP_SPECIAL2, 	0, 0)
			
				IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType != FMMC_TYPE_MISSION 
                	SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(serverBD.niDriver), rgFM_AiLike) 
                ELSE 
               		SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(serverBD.niDriver), rgFM_AiPed[2][2][2][2][0])  
                ENDIF
				
				SET_PED_KEEP_TASK(NET_TO_PED(serverBD.niDriver), TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(serverBD.niDriver), TRUE)
				
				SET_ENTITY_HEALTH(NET_TO_PED(serverBD.niDriver), ROUND(200*g_sMPTunables.fAiHealthModifier))
				
				SET_PED_CAN_BE_DRAGGED_OUT(NET_TO_PED(serverBD.niDriver), FALSE)
				SET_PED_CONFIG_FLAG(NET_TO_PED(serverBD.niDriver), PCF_AIDriverAllowFriendlyPassengerSeatEntry, TRUE)
		
				NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - CREATED DRIVER ") NET_NL()
			ENDIF
		ENDIF
	ENDIF
	
	//Check we have created the ped
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niDriver)
		NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - FAILING ON DRIVER CREATION") NET_NL()
		RETURN FALSE
	ENDIF
	
	SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_BLACKOPS_01)
	RETURN TRUE
ENDFUNC

//PURPOSE: Creates the boat and boat driver.
FUNC BOOL CREATE_BOAT_AND_DRIVER()
	IF CAN_REGISTER_MISSION_ENTITIES(1, 1, 0, 0)
		IF 	REQUEST_LOAD_MODEL(DINGHY2)
		AND REQUEST_LOAD_MODEL(S_M_Y_BLACKOPS_01)
			IF 	CREATE_BOAT()
			AND CREATE_DRIVER()
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Adds a blip to the Boat
PROC ADD_BOAT_BLIP()
	IF NOT DOES_BLIP_EXIST(BoatBlip)
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niBoat)
			BoatBlip = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(serverBD.niBoat))
			SET_BLIP_SPRITE(BoatBlip, RADAR_TRACE_DOCK)
			SET_BLIP_NAME_FROM_TEXT_FILE(BoatBlip, "BTX_BLIP")
			SET_BLIP_FLASHES(BoatBlip, TRUE)
			SET_BLIP_CATEGORY(BoatBlip, BLIP_CATEGORY_ACTIVITY)	//For Bug 1929689
			NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - ADD_BOAT_BLIP DONE  ") NET_NL()
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Removes the Boat Blip if it exists
PROC REMOVE_BOAT_BLIP()
	IF DOES_BLIP_EXIST(BoatBlip)
		REMOVE_BLIP(BoatBlip)
		NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - REMOVE_BOAT_BLIP DONE  ") NET_NL()
	ENDIF
ENDPROC

//PURPOSE: Controls the display of help text
PROC CONTROL_HELP_TEXT()
	IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		INT iStat = GET_MP_INT_CHARACTER_STAT(MP_STAT_TUTORIAL_BITSET)
		IF NOT IS_BIT_SET(iStat, TUTBS_BOAT_TAXI_HELP_1)
			PRINT_HELP("BTX_HELP1")	//~s~The boat ~BLIP_DOCK~ will take you to the nearest drop off location.
			SET_BIT(iStat, TUTBS_BOAT_TAXI_HELP_1)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_TUTORIAL_BITSET, iStat)
			NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - TUTBS_BOAT_TAXI_HELP_1 ") NET_NL()
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_NEAREST_PASSENGER_SIDE(VEHICLE_INDEX ThisCar)
	VECTOR char_pos
	VECTOR right_pos
	VECTOR left_pos
	VECTOR right_vec
	VECTOR left_vec
	CONST_INT LEFT_PASSENGER 1
	CONST_INT RIGHT_PASSENGER 2
	
	IF IS_VEHICLE_DRIVEABLE(ThisCar)
		
		// get player coords
		IF IS_PLAYER_PLAYING(PLAYER_ID())
			char_pos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		ELSE
			RETURN(-1)
		ENDIF

		// right side
		right_pos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ThisCar, <<1.0, -0.5, 0.0>>)
		right_vec = right_pos - char_pos

		// left side
		left_pos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ThisCar, <<-1.0, -0.5, 0.0>>)
		left_vec = left_pos - char_pos 

		// choose the smaller of the two
		IF VMAG(right_vec)  < VMAG(left_vec)
			RETURN(RIGHT_PASSENGER)
		ELSE
			RETURN(LEFT_PASSENGER)
		ENDIF

	ELSE
		RETURN(-1)
	ENDIF
ENDFUNC

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////               BOAT BODY AND BRAIN               //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

//PURPOSE: Controls what the Boat should be doing
PROC CONTROL_BOAT_BODY()
	
	//Check if I have control of the Guard
	IF 	NOT IS_NET_PED_INJURED(serverBD.niDriver)
	AND IS_NET_VEHICLE_DRIVEABLE(serverBD.niBoat)
	
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niDriver)
		OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.niDriver) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
		
			CONTROL_BOAT_RADIO()
			CONTROL_DRIVER_DIALOGUE()
			
			IF TAKE_CONTROL_OF_NET_ID(serverBD.niDriver)
				SET_PED_RESET_FLAG(NET_TO_PED(serverBD.niDriver), PRF_DisablePedEnteredMyVehicleEvents, TRUE)
			ENDIF
			
			SWITCH serverBD.eBoatState
				CASE BOAT_STATE_WAIT
					//do nothing here
				BREAK
								
				CASE BOAT_STATE_GO_TO_PLAYER
				
					IF 	GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niDriver), SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niDriver), SCRIPT_TASK_VEHICLE_MISSION) != WAITING_TO_START_TASK
						TASK_BOAT_MISSION(NET_TO_PED(serverBD.niDriver), NET_TO_VEH(serverBD.niBoat), NULL, PLAYER_PED_ID(), <<0, 0, 0>>, MISSION_GOTO, BOAT_SPEED, DRIVINGMODE_AVOIDCARS, BOAT_ARRIVED_RANGE, BCF_BOATTAXISETTINGS)
						NET_PRINT_TIME() NET_PRINT(" ---> BOAT - CONTROL_BOAT_BODY - BOAT_STATE_GO_TO_PLAYER") NET_NL()
					ENDIF
					
				BREAK
				
				CASE BOAT_STATE_GO_TO_DESTINATION
					CALCULATE_PATH_NODES_AREA(GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niBoat), FALSE), serverBD.vDropLocation)
					REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(fMinX, fMinY, fMaxX, fMaxY)
					
					IF ARE_NODES_LOADED_FOR_AREA(fMinX, fMinY, fMaxX, fMaxY)
						IF 	GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niDriver), SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niDriver), SCRIPT_TASK_PERFORM_SEQUENCE) != WAITING_TO_START_TASK
							TASK_PERFORM_SEQUENCE(NET_TO_PED(serverBD.niDriver),  seqBoatGoToDrop)
							NET_PRINT_TIME() NET_PRINT(" ---> BOAT - CONTROL_BOAT_BODY - BOAT_STATE_GO_TO_DESTINATION") NET_NL()
						ENDIF
					ENDIF
					
				BREAK
				
				CASE BOAT_STATE_ARRIVED_AT_DESTINATION
				
					IF 	GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niDriver), SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niDriver), SCRIPT_TASK_VEHICLE_MISSION) != WAITING_TO_START_TASK
						TASK_VEHICLE_MISSION_COORS_TARGET(NET_TO_PED(serverBD.niDriver), NET_TO_VEH(serverBD.niBoat), GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niBoat)), MISSION_STOP, 5.0, DrivingMode_StopForCars|DF_UseSwitchedOffNodes|DF_ForceJoinInRoadDirection, 2.0, 1.0)
						NET_PRINT_TIME() NET_PRINT(" ---> BOAT - CONTROL_TAXI_BODY - BOAT_STATE_ARRIVED_AT_DESTINATION ") NET_NL()
					ENDIF
					
				BREAK
				
				CASE BOAT_STATE_STOP_IN_PLACE
				
					IF 	GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niDriver), SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niDriver), SCRIPT_TASK_VEHICLE_MISSION) != WAITING_TO_START_TASK
						TASK_VEHICLE_MISSION_COORS_TARGET(NET_TO_PED(serverBD.niDriver), NET_TO_VEH(serverBD.niBoat), GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niBoat)), MISSION_STOP, 5.0, DrivingMode_StopForCars|DF_UseSwitchedOffNodes|DF_ForceJoinInRoadDirection, 2.0, 1.0)
						NET_PRINT_TIME() NET_PRINT(" ---> BOAT - CONTROL_TAXI_BODY - BOAT_STATE_STOP_IN_PLACE ") NET_NL()
					ENDIF
					
				BREAK
				
				CASE BOAT_STATE_FLEE
				
					IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DriverFleeing)
												
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niBoat))
						OR IS_PED_ON_SPECIFIC_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niBoat))
						
							SET_PED_DIES_IN_WATER(NET_TO_PED(serverBD.niDriver), FALSE)
							SET_PED_DIES_INSTANTLY_IN_WATER(NET_TO_PED(serverBD.niDriver), FALSE)
							SET_PED_FLEE_ATTRIBUTES(NET_TO_PED(serverBD.niDriver), FA_USE_VEHICLE, FALSE)
							
							TASK_PERFORM_SEQUENCE(NET_TO_PED(serverBD.niDriver), seqFlee)
						
						ELSE
						
							SET_PED_DIES_IN_WATER(NET_TO_PED(serverBD.niDriver), FALSE)
							SET_PED_DIES_INSTANTLY_IN_WATER(NET_TO_PED(serverBD.niDriver), FALSE)
							SET_PED_FLEE_ATTRIBUTES(NET_TO_PED(serverBD.niDriver), FA_USE_VEHICLE, TRUE)
							
							TASK_PERFORM_SEQUENCE(NET_TO_PED(serverBD.niDriver), seqBoatLeave)
						
						ENDIF
						
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DriverFleeing)	
					ENDIF
					
				BREAK
				
				CASE BOAT_STATE_LEAVE
				
					IF 	GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niDriver), SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niDriver), SCRIPT_TASK_PERFORM_SEQUENCE) != WAITING_TO_START_TASK
						TASK_PERFORM_SEQUENCE(NET_TO_PED(serverBD.niDriver),  seqBoatLeave)
						NET_PRINT_TIME() NET_PRINT(" ---> BOAT - CONTROL_BOAT_BODY - TASK LEAVE") NET_NL()
					ENDIF
					
				BREAK				
				
				CASE BOAT_STATE_CLEANUP	
				
					IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niBoat)
						CLEANUP_NET_ID(serverBD.niBoat)
						NET_PRINT_TIME() NET_PRINT(" ---> BOAT - CONTROL_BOAT_BODY - C - CLEANUP BOAT ") NET_NL()
					ENDIF
					IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niDriver)
						SET_PED_KEEP_TASK(NET_TO_PED(serverBD.niDriver), TRUE)
						CLEANUP_NET_ID(serverBD.niDriver)
						NET_PRINT_TIME() NET_PRINT(" ---> BOAT - CONTROL_BOAT_BODY - C - CLEANUP DRIVER ") NET_NL()
					ENDIF
					
				BREAK
			ENDSWITCH
		ENDIF

	ENDIF
	
ENDPROC


//PURPOSE: Controls what the Boat should be doing
PROC CONTROL_BOAT_BRAIN()
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		EXIT
	ENDIF
	
	IF serverBD.eBoatState != BOAT_STATE_CLEANUP
		IF IS_NET_PED_INJURED(serverBD.niDriver)
		OR NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.niBoat)
			serverBD.eBoatState = BOAT_STATE_CLEANUP
			NET_PRINT_TIME() NET_PRINT(" ---> BOAT - CONTROL_BOAT_BRAIN - BOAT OR DRIVER NOT ALIVE - eBoatState = BOAT_STATE_CLEANUP") NET_NL()
		ELSE
			IF NOT IS_PED_IN_VEHICLE(NET_TO_PED(serverBD.niDriver), NET_TO_VEH(serverBD.niBoat))
				serverBD.eBoatState = BOAT_STATE_CLEANUP
				NET_PRINT_TIME() NET_PRINT(" ---> BOAT - CONTROL_BOAT_BRAIN - DRIVER NOT IN BOAT - eBoatState = BOAT_STATE_CLEANUP") NET_NL()
			ENDIF
		ENDIF
	ENDIF
	
	IF 	NOT IS_NET_PED_INJURED(serverBD.niDriver)
	AND IS_NET_VEHICLE_DRIVEABLE(serverBD.niBoat)
	
		IF serverBD.eBoatState != BOAT_STATE_FLEE
			IF 	GET_ENTITY_HEALTH(NET_TO_VEH(serverBD.niBoat)) <= 600
			OR	GET_ENTITY_HEALTH(NET_TO_PED(serverBD.niDriver)) <= 150
				serverBD.eBoatState = BOAT_STATE_FLEE
				NET_PRINT_TIME() NET_PRINT(" ---> BOAT - CONTROL_BOAT_BRAIN - BOAT_STATE_GO_TO_PLAYER - eBoatState = BOAT_STATE_GO_TO_DESTINATION") NET_NL()
			ENDIF
		ENDIF
	
		SWITCH serverBD.eBoatState
			CASE BOAT_STATE_GO_TO_PLAYER
				IF serverBD.eBoatTaxiStage = eBT_GO_TO_DROPOFF
					serverBD.eBoatState = BOAT_STATE_GO_TO_DESTINATION
					NET_PRINT_TIME() NET_PRINT(" ---> BOAT - CONTROL_BOAT_BRAIN - BOAT_STATE_GO_TO_PLAYER - eBoatState = BOAT_STATE_GO_TO_DESTINATION") NET_NL()
				ELIF serverBD.eBoatTaxiStage = eBT_CLEANUP
					serverBD.eBoatState = BOAT_STATE_CLEANUP
					NET_PRINT_TIME() NET_PRINT(" ---> BOAT - CONTROL_BOAT_BRAIN - BOAT_STATE_GO_TO_PLAYER - eBoatState = BOAT_STATE_CLEANUP - C") NET_NL()
				ELIF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_BoatJourneySkipStarted)	//bSkipToDestination = FALSE	//NEED TO MAKE SERVER BD
				AND IS_ENTITY_AT_ENTITY(NET_TO_PED(serverBD.niDriver), PLAYER_PED_ID(), <<BOAT_ARRIVED_RANGE, BOAT_ARRIVED_RANGE, BOAT_ARRIVED_RANGE>>)
					serverBD.eBoatState = BOAT_STATE_STOP_IN_PLACE
					NET_PRINT_TIME() NET_PRINT(" ---> BOAT - CONTROL_BOAT_BRAIN - BOAT_STATE_GO_TO_PLAYER - eBoatState = BOAT_STATE_STOP_IN_PLACE") NET_NL()
				ENDIF
			BREAK
			
			CASE BOAT_STATE_GO_TO_DESTINATION
				SET_BIT(iBoolsBitSet, biPlayerJumpedOutDialogue)
				IF GET_VEHICLE_NUMBER_OF_PASSENGERS(NET_TO_VEH(serverBD.niBoat)) <= 0
					serverBD.eBoatState = BOAT_STATE_STOP_IN_PLACE
					CLEAR_BIT(iBoolsBitSet, biPlayerJumpedOutDialogue)
					NET_PRINT_TIME() NET_PRINT(" ---> BOAT - CONTROL_BOAT_BRAIN - BOAT_STATE_GO_TO_DESTINATION - eBoatState = BOAT_STATE_STOP_IN_PLACE") NET_NL()
				ELIF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_BoatJourneySkipStarted)	//bSkipToDestination = FALSE	//NEED TO MAKE SERVER BD
				AND IS_ENTITY_AT_COORD(NET_TO_PED(serverBD.niDriver), serverBD.vDropLocation, <<BOAT_ARRIVED_RANGE, BOAT_ARRIVED_RANGE, BOAT_ARRIVED_RANGE>>)
					serverBD.eBoatState = BOAT_STATE_ARRIVED_AT_DESTINATION
					NET_PRINT_TIME() NET_PRINT(" ---> BOAT - CONTROL_BOAT_BRAIN - BOAT_STATE_GO_TO_DESTINATION - eBoatState = BOAT_STATE_ARRIVED_AT_DESTINATION") NET_NL()
				ENDIF
			BREAK
			
			CASE BOAT_STATE_ARRIVED_AT_DESTINATION
				IF 	GET_VEHICLE_NUMBER_OF_PASSENGERS(NET_TO_VEH(serverBD.niBoat)) <= 0
				AND	NOT IS_PED_ON_SPECIFIC_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niBoat))
				AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niBoat)) >= 10
					serverBD.eBoatState = BOAT_STATE_LEAVE
					NET_PRINT_TIME() NET_PRINT(" ---> BOAT - CONTROL_BOAT_BRAIN - BOAT_STATE_ARRIVED_AT_DESTINATION - eBoatState = BOAT_STATE_LEAVE - A") NET_NL()
				ENDIF
			BREAK
			
			CASE BOAT_STATE_STOP_IN_PLACE
				IF GET_VEHICLE_NUMBER_OF_PASSENGERS(NET_TO_VEH(serverBD.niBoat)) >= 1
					serverBD.eBoatState = BOAT_STATE_GO_TO_DESTINATION
					NET_PRINT_TIME() NET_PRINT(" ---> BOAT - CONTROL_BOAT_BRAIN - BOAT_STATE_STOP_IN_PLACE - eBoatState = BOAT_STATE_GO_TO_DESTINATION") NET_NL()
				ENDIF				
			BREAK
			
			CASE BOAT_STATE_LEAVE
				IF 	GET_VEHICLE_NUMBER_OF_PASSENGERS(NET_TO_VEH(serverBD.niBoat)) <= 0
				AND	NOT IS_PED_ON_SPECIFIC_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niBoat))
				AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niBoat)) >= 10
					IF GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niDriver), SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
						serverBD.eBoatState = BOAT_STATE_CLEANUP
						NET_PRINT_TIME() NET_PRINT(" ---> BOAT - CONTROL_BOAT_BRAIN - BOAT_STATE_LEAVE - eBoatState = BOAT_STATE_CLEANUP - A") NET_NL()
					ENDIF
				ENDIF
			BREAK
			
			CASE BOAT_STATE_FLEE
				IF 	GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niDriver), SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
				AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), NET_TO_PED(serverBD.niDriver)) >= 10
					serverBD.eBoatState = BOAT_STATE_CLEANUP
					NET_PRINT_TIME() NET_PRINT(" ---> BOAT - CONTROL_BOAT_BRAIN - BOAT_STATE_ARRIVED_AT_DESTINATION - eBoatState = BOAT_STATE_CLEANUP - A") NET_NL()
				ENDIF
			BREAK
						
			CASE BOAT_STATE_CLEANUP
				//do nothing here
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

//PURPOSE: Draws the instructional buttons when necessary
PROC PROCESS_INSTRUCTIONAL_BUTTONS()
	
	IF HAS_SCALEFORM_MOVIE_LOADED(buttonMovie)	
		IF SAFE_TO_DO_INPUT_CHECK()
			IF playerBD[PARTICIPANT_ID_TO_INT()].eBoatTaxiStage = eBT_ENTER_BOAT
			OR playerBD[PARTICIPANT_ID_TO_INT()].eBoatTaxiStage = eBT_START_BOAT
			OR playerBD[PARTICIPANT_ID_TO_INT()].eBoatTaxiStage = eBT_GO_TO_DROPOFF
			OR playerBD[PARTICIPANT_ID_TO_INT()].eBoatTaxiStage = eBT_EXIT_BOAT
				
				BOOL bDrawButtons = TRUE
				
				SPRITE_PLACEMENT aSprite = GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
				REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(instructionalButtons)
				
				IF playerBD[PARTICIPANT_ID_TO_INT()].eBoatTaxiStage = eBT_ENTER_BOAT
					IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niBoat)
						IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niBoat), <<10.0, 10.0, 3.0>>)
						AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						AND NOT IS_VEHICLE_FULL(NET_TO_VEH(serverBD.niBoat))
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_ENTER), "BTX_ENTER", instructionalButtons)	//Hold to Enter
						ENDIF
					ENDIF
				//ELIF playerBD[PARTICIPANT_ID_TO_INT()].eBoatTaxiStage = eBT_START_BOAT
					//ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT), "BTX_SKIP", instructionalButtons)		//Skip
					//ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL), "BTX_START", instructionalButtons)	//Go
				
				//ELIF playerBD[PARTICIPANT_ID_TO_INT()].eBoatTaxiStage = eBT_GO_TO_DROPOFF
				//	ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT), "BTX_SKIP", instructionalButtons)		//Skip
					//ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL), "BTX_STOP", instructionalButtons)		//Stop
				
				//ELIF playerBD[PARTICIPANT_ID_TO_INT()].eBoatTaxiStage = eBT_EXIT_BOAT
				//	ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_Y), "BTX_EXIT", instructionalButtons)		//Exit
				ELSE
					bDrawButtons = FALSE
				ENDIF
				
				IF bDrawButtons = TRUE
					SET_IDLE_KICK_DISABLED_THIS_FRAME()
					SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
					RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(buttonMovie, aSprite, instructionalButtons, SHOULD_REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(instructionalButtons))
				ENDIF
			ENDIF
		ENDIF
	ELSE
		buttonMovie = REQUEST_SCALEFORM_MOVIE("instructional_buttons")
	ENDIF
	/*ELSE
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(buttonMovie)
	ENDIF*/
ENDPROC


/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MISSION PROCS                  //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Cleanup Mission Data
PROC SCRIPT_CLEANUP()
	
	REMOVE_BOAT_BLIP()
	
	//CLEANUP CHECKS
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF serverBD.eBoatState = BOAT_STATE_CLEANUP
			IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niBoat)
			AND NOT IS_NET_PED_INJURED(serverBD.niDriver)
			
				TASK_PERFORM_SEQUENCE(NET_TO_PED(serverBD.niDriver),  seqBoatLeave)
				NET_PRINT_TIME() NET_PRINT(" ---> BOAT - CONTROL_TAXI_BODY - BOAT_STATE_CLEANUP - TASK LEAVE - B") NET_NL()
			ENDIF
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niBoat)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niBoat)
					CLEANUP_NET_ID(serverBD.niBoat)
					NET_PRINT_TIME() NET_PRINT(" ---> BOAT - CONTROL_BOAT_BODY - C - CLEANUP BOAT ") NET_NL()
				ENDIF
			ENDIF
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niDriver)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niDriver)
					CLEANUP_NET_ID(serverBD.niDriver)
					NET_PRINT_TIME() NET_PRINT(" ---> BOAT - CONTROL_BOAT_BODY - C - CLEANUP DRIVER ") NET_NL()
				ENDIF
			ENDIF
			//BROADCAST_TELL_SERVER_CONTACT_REQUEST_COMPLETE(ALL_PLAYERS(),REQUEST_BOAT_PICKUP,INVALID_PLAYER_INDEX())
		ENDIF
	ENDIF
	
	BROADCAST_TELL_SERVER_CONTACT_REQUEST_COMPLETE(ALL_PLAYERS(),REQUEST_BOAT_PICKUP,INVALID_PLAYER_INDEX())
	
	CLEAR_SEQUENCES()
		
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(buttonMovie)
	
	NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - CLEANUP MISSION  ") NET_NL()
	#IF IS_DEBUG_BUILD NET_LOG("SCRIPT_CLEANUP")	#ENDIF
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC


//PURPOSE: Process the BOAT TAXI stages for the Client
PROC PROCESS_BOAT_TAXI_CLIENT()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
	
		IF serverBD.eBoatTaxiStage = eBT_CLEANUP
			playerBD[PARTICIPANT_ID_TO_INT()].eBoatTaxiStage = eBT_CLEANUP
			NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - PLAYER STAGE = eBT_CLEANUP - A") NET_NL()
		ENDIF
		
		SWITCH playerBD[PARTICIPANT_ID_TO_INT()].eBoatTaxiStage		
			CASE eBT_GO_TO_PLAYER
				CONTROL_HELP_TEXT()
				IF serverBD.eBoatTaxiStage > eBT_GO_TO_PLAYER
					playerBD[PARTICIPANT_ID_TO_INT()].eBoatTaxiStage = eBT_ENTER_BOAT
					NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - PLAYER STAGE = eBT_ENTER_BOAT - A") NET_NL()
				ENDIF
			BREAK
			
			CASE eBT_ENTER_BOAT
				IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niBoat)
					ADD_BOAT_BLIP()
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niBoat))
						REMOVE_BOAT_BLIP()
						playerBD[PARTICIPANT_ID_TO_INT()].eBoatTaxiStage = eBT_GO_TO_DROPOFF	//eBT_START_BOAT
						CLEAR_PED_TASKS(PLAYER_PED_ID())
						CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_GettingInTaxi)
						NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - PLAYER STAGE = eBT_GO_TO_DROPOFF") NET_NL()
					ENDIF
				ENDIF
			BREAK
			
			CASE eBT_START_BOAT
				IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niBoat)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niBoat))

						playerBD[PARTICIPANT_ID_TO_INT()].eBoatTaxiStage = eBT_GO_TO_DROPOFF
						CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_PressedStop)
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_PressedGo)
						NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - biP_PressedGo SET") NET_NL()
						NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - PLAYER STAGE = eBT_GO_TO_DROPOFF") NET_NL()

						
						IF serverBD.eBoatTaxiStage >= eBT_EXIT_BOAT
							playerBD[PARTICIPANT_ID_TO_INT()].eBoatTaxiStage = eBT_EXIT_BOAT
							NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - PLAYER STAGE = eBT_EXIT_BOAT - B") NET_NL()
						ENDIF
					ELSE
						playerBD[PARTICIPANT_ID_TO_INT()].eBoatTaxiStage = eBT_ENTER_BOAT
						NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - PLAYER STAGE = eBT_ENTER_BOAT - B") NET_NL()
					ENDIF
				
				ENDIF
			BREAK
			
			CASE eBT_GO_TO_DROPOFF
				IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niBoat)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niBoat))
						IF serverBD.eBoatTaxiStage >= eBT_EXIT_BOAT
							playerBD[PARTICIPANT_ID_TO_INT()].eBoatTaxiStage = eBT_EXIT_BOAT
							NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - PLAYER STAGE = eBT_EXIT_BOAT - A") NET_NL()
						ENDIF
						SET_IDLE_KICK_DISABLED_THIS_FRAME()
					ELSE
						playerBD[PARTICIPANT_ID_TO_INT()].eBoatTaxiStage = eBT_ENTER_BOAT
						NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - PLAYER STAGE = eBT_ENTER_BOAT - C") NET_NL()
					ENDIF
				ENDIF
			BREAK
			
			CASE eBT_EXIT_BOAT
				IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niBoat)
					//Exit
					IF NOT IS_BIT_SET(iBoolsBitSet, biExitBoat)
						IF GET_ENTITY_SPEED(NET_TO_VEH(serverBD.niBoat)) < 3.0
							TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
							SET_BIT(iBoolsBitSet, biExitBoat)
							NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - eBT_EXIT_BOAT - TASK LEAVE BOAT") NET_NL()
						ENDIF
					ENDIF
										
					//Move to cleanup if not in boat
					IF 	NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niBoat))
					AND	NOT IS_PED_ON_SPECIFIC_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niBoat))
					AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niBoat)) >= 10
						playerBD[PARTICIPANT_ID_TO_INT()].eBoatTaxiStage = eBT_CLEANUP
						NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - PLAYER STAGE = eBT_CLEANUP") NET_NL()
					ENDIF
				ENDIF
			BREAK
			
			CASE eBT_CLEANUP
				NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - SCRIPT CLEANUP B ") NET_NL()
				SCRIPT_CLEANUP()
			BREAK
		ENDSWITCH
		
		PROCESS_INSTRUCTIONAL_BUTTONS()
	ENDIF
ENDPROC

//PURPOSE: Process the BOAT TAXI stages for the Server
PROC PROCESS_BOAT_TAXI_SERVER()
	SWITCH serverBD.eBoatTaxiStage		
		CASE eBT_GO_TO_PLAYER
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_ReachPickup)
			OR GET_VEHICLE_NUMBER_OF_PASSENGERS(NET_TO_VEH(serverBD.niBoat)) > 0
				serverBD.eBoatTaxiStage	 = eBT_ENTER_BOAT
				NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - SERVER STAGE = eBT_ENTER_BOAT") NET_NL()
			ENDIF
		BREAK
		
		CASE eBT_ENTER_BOAT
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niBoat)
				IF GET_VEHICLE_NUMBER_OF_PASSENGERS(NET_TO_VEH(serverBD.niBoat)) > 0
					serverBD.eBoatTaxiStage	 = eBT_START_BOAT
					NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - SERVER STAGE = eBT_START_BOAT") NET_NL()
				ENDIF
			ENDIF
		BREAK
		
		CASE eBT_START_BOAT
			//IF IS_BIT_SET(serverBD.iServerBitSet, biS_BoatGoStarted)
			IF GET_VEHICLE_NUMBER_OF_PASSENGERS(NET_TO_VEH(serverBD.niBoat)) > 0
				serverBD.eBoatTaxiStage	 = eBT_GO_TO_DROPOFF
				NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - SERVER STAGE = eBT_GO_TO_DROPOFF") NET_NL()
			ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_ReachDropoff)
				serverBD.eBoatTaxiStage	 = eBT_EXIT_BOAT
				NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - SERVER STAGE = eBT_EXIT_BOAT") NET_NL()
			ENDIF
		BREAK
		
		CASE eBT_GO_TO_DROPOFF
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niBoat)
				IF GET_VEHICLE_NUMBER_OF_PASSENGERS(NET_TO_VEH(serverBD.niBoat)) <= 0
					serverBD.eBoatTaxiStage	 = eBT_ENTER_BOAT
					NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - SERVER STAGE = eBT_ENTER_BOAT - B") NET_NL()
				ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_ReachDropoff)
					serverBD.eBoatTaxiStage	 = eBT_EXIT_BOAT
					NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - SERVER STAGE = eBT_EXIT_BOAT") NET_NL()
				ENDIF
			ENDIF
		BREAK
		
		CASE eBT_EXIT_BOAT
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niBoat)
				IF 	GET_VEHICLE_NUMBER_OF_PASSENGERS(NET_TO_VEH(serverBD.niBoat)) <= 0
				AND	NOT IS_PED_ON_SPECIFIC_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niBoat))
				AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niBoat)) >= 10
					serverBD.eBoatTaxiStage	 = eBT_CLEANUP
					NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - SERVER STAGE = eBT_CLEANUP") NET_NL()
				ENDIF
			ENDIF
		BREAK
		
		CASE eBT_CLEANUP
			//Do nothing
		BREAK
	ENDSWITCH
ENDPROC


//PURPOSE: Runs through the max num participents and does relevant checks
PROC MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
	INT iParticipant
	
	//Set Bits that can then be cleared in the repeat if necessary
	CLEAR_BIT(serverBD.iServerBitSet, biS_BoatGoStarted)
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
			//PED_INDEX PlayerPedId = GET_PLAYER_PED(PlayerId)
			
			//** CHECKS THAT DON'T NEED THE PLAYER TO BE ALIVE **//
			
			
			//** CHECKS THAT REQUIRE THE PLAYER TO BE ALIVE **//
			IF IS_NET_PLAYER_OK(PlayerId)
				
				IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niBoat)
					//Check if boat has reached Pickup
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ReachPickup)
						IF IS_ENTITY_AT_COORD(NET_TO_VEH(serverBD.niBoat), serverBD.vPickupLocation, <<BOAT_ARRIVED_RANGE*1.5, BOAT_ARRIVED_RANGE*1.5, BOAT_ARRIVED_RANGE*1.5>>)
							SET_BIT(serverBD.iServerBitSet, biS_ReachPickup)
							NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - biS_ReachPickup SET") NET_NL()
						ENDIF
					ENDIF
					
					//Check if boat has reached DropOff
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_BoatJourneySkipStarted)
					OR IS_BIT_SET(serverBD.iServerBitSet, biS_BoatJourneySkipDone)
						IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ReachDropoff)
							IF IS_ENTITY_AT_COORD(NET_TO_VEH(serverBD.niBoat), serverBD.vDropLocation, <<BOAT_ARRIVED_RANGE*1.5, BOAT_ARRIVED_RANGE*1.5, BOAT_ARRIVED_RANGE*1.5>>)
								SET_BIT(serverBD.iServerBitSet, biS_ReachDropoff)
								NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - biS_ReachDropoff SET") NET_NL()
							ENDIF
						ENDIF
					ENDIF
					
					//Check if any player has made Boat leave
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_BoatGoStarted)
						//IF playerBD[iParticipant].eBoatTaxiStage = eBT_GO_TO_DROPOFF
						IF IS_BIT_SET(playerBD[iParticipant].iPlayerBitSet, biP_PressedGo)
							SET_BIT(serverBD.iServerBitSet, biS_BoatGoStarted)
							NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - biS_BoatGoStarted SET") NET_NL()
							CLEAR_BIT(playerBD[iParticipant].iPlayerBitSet, biP_PressedGo)
						ENDIF
					ENDIF
					
					//Check if any player has told the Boat to Stop
					IF serverBD.eBoatState != BOAT_STATE_STOP_IN_PLACE	
						IF IS_BIT_SET(playerBD[iParticipant].iPlayerBitSet, biP_PressedStop)
							serverBD.eBoatState = BOAT_STATE_STOP_IN_PLACE
							NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - serverBD.eBoatState = BOAT_STATE_STOP_IN_PLACE") NET_NL()
							CLEAR_BIT(playerBD[iParticipant].iPlayerBitSet, biP_PressedStop)
						ENDIF
					ENDIF
					
				ENDIF
				
			ELSE
				//CLEAR_BIT(serverBD.iServerBitSet, biS_AllCrewArrested)
			ENDIF
			
		ENDIF
	ENDREPEAT
ENDPROC

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                      //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT(MP_MISSION_DATA missionScriptArgs)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		#IF IS_DEBUG_BUILD	
			NET_PRINT_TIME() NET_PRINT("MP: Starting mission ") NET_NL()
		#ENDIF
		
		// Carry out all the initial game starting duties. 
		IF NOT PROCESS_PRE_GAME(missionScriptArgs)	
			#IF IS_DEBUG_BUILD NET_LOG("FAILED TO RECEIVE INITIAL NETWORK BROADCAST")	#ENDIF
			NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - MISSION END - FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP D ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		
		// Setup widgets. Make sure this is done after PROCESS_PRE_GAME()
		#IF IS_DEBUG_BUILD
			CREATE_WIDGETS()
		#ENDIF
	ENDIF
	
	// Main loop
	WHILE TRUE

		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
				
		// If we have a match end event, bail
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		//OR NOT IS_NET_PLAYER_OK(PLAYER_ID(), TRUE, TRUE)	//FALSE, TRUE)
			#IF IS_DEBUG_BUILD NET_LOG("SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")	#ENDIF
			NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - MISSION END - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE - SCRIPT CLEANUP B ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		
		//Check if BOAT TAXI shouldn't start
		/*IF NOT CAN_DO_BOAT_TAXI(FALSE)
			#IF IS_DEBUG_BUILD NET_LOG("CAN_DO_BOAT_TAXI")	#ENDIF
			NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - MISSION END - CAN_DO_BOAT_TAXI = FALSE - SCRIPT CLEANUP D ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF*/
		
		//Check if player has swapped from/to tutorial session and end
		IF NETWORK_IS_IN_TUTORIAL_SESSION() != bInTutorialSession
			#IF IS_DEBUG_BUILD NET_LOG("PLAYER IN TUT SESSION")	#ENDIF
			NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - MISSION END - PLAYER IN TUTORIAL SESSION - SCRIPT CLEANUP C ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
			
		
		// Deal with the debug
		#IF IS_DEBUG_BUILD		
			UPDATE_WIDGETS()
		#ENDIF		
	
		// If we need to leave the mission, make sure we just straight to the end stage and terminate the script - DON'T NEED FOR AMBIENT MISSION
		/*IF SHOULD_PLAYER_LEAVE_MP_MISSION() 
			#IF IS_DEBUG_BUILD
			NET_PRINT_TIME() NET_PRINT( GET_MP_MISSION_NAME(thisMission), " Leave mission = TRUE \n")
			#ENDIF
			playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_LEAVE
			#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_LEAVE")	#ENDIF
		ENDIF*/
		
		// -----------------------------------
		// Process your game logic.....
		SWITCH GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT())
			
			// Wait untill the server gives the all go before moving on
			CASE GAME_STATE_INI
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					CREATE_SEQUENCES()
					ADD_BOAT_BLIP()
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING
					NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_RUNNING")	#ENDIF
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 1") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY")	#ENDIF
				ENDIF
			BREAK
			
			// Main gameplay state.
			CASE GAME_STATE_RUNNING
				
				CONTROL_BOAT_BODY()
				
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					PROCESS_BOAT_TAXI_CLIENT()
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 2") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY")	#ENDIF
				ENDIF
				
				// Make the player end the mission if they leave the Boat
				IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niBoat)
					IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niBoat)), <<LEAVE_RANGE, LEAVE_RANGE, LEAVE_RANGE>>)
						playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
						NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 6 - MISSION END - PLAYER NOT NEAR BOAT ") NET_PRINT_VECTOR(serverBD.vDropLocation) NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 6")	#ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE GAME_STATE_TERMINATE_DELAY 
				SET_MISSION_FINISHED(serverBD.MissionTerminateDelayTimer)
		      	IF IS_MISSION_READY_TO_CLEANUP(serverBD.MissionTerminateDelayTimer)
		            playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 3") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 3")	#ENDIF
		      	ENDIF
			BREAK
			
			// The game stage the local player is placed when we want him to leave a mission
			CASE GAME_STATE_LEAVE
				//LEAVE_MY_MP_MISSION(thisMission)
				playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
				NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 2") NET_NL()
				#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 2")	#ENDIF
			FALLTHRU
			
			//Cleans up then terminates the mission
			CASE GAME_STATE_END
				NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - SCRIPT CLEANUP A ") NET_NL()
				SCRIPT_CLEANUP()
			BREAK

		ENDSWITCH
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			
			SWITCH GET_SERVER_MISSION_STATE()
				
				// For CnC group missions, we may want to wait for all players to have launched the script
				CASE GAME_STATE_INI
					IF HAS_NET_TIMER_EXPIRED(serverBD.CreationDelayTimer, CREATION_DELAY)
					#IF IS_DEBUG_BUILD OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J) #ENDIF
						
						//Grab Updated Details
						IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_GetUpdatedDetails)
							serverBD.MainPlayer = PLAYER_ID()
							serverBD.iZone = GET_PLAYER_BOAT_TAXI_ZONE()
							serverBD.vPickupLocation = (GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)+<<0, 5, 0>>)
							serverBD.vDropLocation = GET_CLOSEST_DROP_LOCATION(serverBD.iZone)
							SET_BIT(serverBD.iServerBitSet, biS_GetUpdatedDetails)
							NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - UPDATE - MainPlayer = ") NET_PRINT(GET_PLAYER_NAME(serverBD.MainPlayer)) NET_NL()
							NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - UPDATE - iZone = ") NET_PRINT_INT(serverBD.iZone) NET_NL()
							NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - UPDATE - vPickupLocation = ") NET_PRINT_VECTOR(serverBD.vPickupLocation) NET_NL()
							NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - UPDATE - vDropLocation = ") NET_PRINT_VECTOR(serverBD.vDropLocation) NET_NL()
						ENDIF
						
						IF CREATE_BOAT_AND_DRIVER()
							CREATE_SEQUENCES()
							serverBD.iServerGameState = GAME_STATE_RUNNING
							NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - serverBD.iServerGameState = GAME_STATE_RUNNING") NET_NL()
							#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_RUNNING")	#ENDIF
						ENDIF
					ENDIF
					
					END_SCRIPT_IF_PLAYER_AT_INVALID_PICKUP_POINT()
				BREAK
				
				// Look for game end conditions
				CASE GAME_STATE_RUNNING
					
					MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
					PROCESS_BOAT_TAXI_SERVER()
					CONTROL_BOAT_BRAIN()
					END_SCRIPT_IF_PLAYER_AT_INVALID_PICKUP_POINT()
					
					#IF IS_DEBUG_BUILD
						IF bHostEndMissionNow
							serverBD.iServerGameState = GAME_STATE_END
							NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - serverBD.iServerGameState = GAME_STATE_END 1") NET_NL()
							#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 1")	#ENDIF
						ENDIF
					#ENDIF	
					
					IF HAVE_MISSION_END_CONDITIONS_BEEN_MET()
						serverBD.iServerGameState = GAME_STATE_END
						NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - serverBD.iServerGameState = GAME_STATE_END 4") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 4")	#ENDIF
					ENDIF
					
				BREAK
				
				CASE GAME_STATE_END
				BREAK
				
			ENDSWITCH
			
		ENDIF
		
	ENDWHILE
	
ENDSCRIPT
