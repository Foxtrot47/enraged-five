/// Freemode hold up tutorial
///    Dave W



USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_camera.sch"
USING "commands_hud.sch"

// Network Headers

USING "net_events.sch"

// CnC Headers

USING "net_mission.sch"
USING "net_scoring_common.sch"

USING "commands_path.sch"
USING "net_blips.sch"
USING "commands_zone.sch"


USING "hud_drawing.sch"
USING "net_ambience.sch"


USING "shop_public.sch"
USING "net_garages.sch"

USING "net_wait_zero.sch"


USING "net_mission_details_overlay.sch"

USING "net_hud_displays.sch"

USING "fm_hold_up_tut.sch"

USING "net_mission_joblist.sch"

#IF IS_DEBUG_BUILD
	USING "profiler.sch"
#ENDIF

// Game States
CONST_INT GAME_STATE_INI 		0
CONST_INT GAME_STATE_INI_SPAWN	1
CONST_INT GAME_STATE_RUNNING	2
CONST_INT GAME_STATE_LEAVE		3
CONST_INT GAME_STATE_FAILED		4
CONST_INT GAME_STATE_TERMINATE_DELAY 5
CONST_INT GAME_STATE_END		6

//MP_MISSION thisMission = eFM_HOLD_UP_TUT

CONST_INT biS_AllPlayersFinished		0

INT MAX_TIME_GET_TO_HOLD_UP	= 600000
TWEAK_INT RETRY_AFTER_TIMEOUT_KICK		30000

// Everyone can read this data, only the server can update it. 
STRUCT ServerBroadcastData
	INT iServerGameState
	INT iServerBitSet
	INT iRecentlyAllocated
	SCRIPT_TIMER timeTerminate
	HOLD_UP_TUT_SERVER_STRUCT holdUpTutServer
ENDSTRUCT
ServerBroadcastData serverBD


INT iServerStaggeredLoopCount



STRUCT PlayerBroadcastData
	INT iGameState
ENDSTRUCT
PlayerBroadcastData PlayerBD[NUM_NETWORK_PLAYERS]

FM_PLAYER_HOLD_UP_TUT_STRUCT fmHoldUpTut 


SCRIPT_TIMER timeWaitForHelp
SCRIPT_TIMER timeBetweenPhoneCallAttempts
SCRIPT_TIMER timeSnacksHelp
//SCRIPT_TIMER timeHoldUpReset
INT iBoolsBitSet

CONST_INT biL_ServerLoopedThroughEveryone		0
CONST_INT biL_StaggeredAllPlayersFinished		1
CONST_INT biL_DoneIntroPhoneCall				2
CONST_INT biL_WaitForCallFinishing				3
CONST_INT biL_AddedPedForPhoneCall				4
CONST_INT biL_PhonecallFailed					5
CONST_INT biL_DoneSnacksHelp					6
CONST_INT biL_DonePassiveHelp					7
CONST_INT biL_OpenedUpHoldUp					8
CONST_INT biL_DoUnlockTextMessage				9
CONST_INT BIL_CheckedForNearbyPlayer			10
CONST_INT biL_ReduceTimeoutAsPartnerAtStore		11
CONST_INT biL_FriendlyFireOff					12
CONST_INT biL_QuickAcceptInviteHelp				13
CONST_INT biL_OtherPlayerBlipHelp				14
CONST_INT biL_FoundPlayerBlip					15
CONST_INT biL_DoneInviteHelp1					16
CONST_INT biL_PassiveModeHelp					17
CONST_INT biL_PlayerApproachedAtSpeed			18

structPedsForConversation speechHoldUpTut

INT iInitialShopsHeldUpCount = -1

SCRIPT_TIMER timeAcceptInvite

SCRIPT_TIMER timeCheckForCompletedHoldups
//INT iLocalMeetLoc
#IF IS_DEBUG_BUILD
	SCRIPT_TIMER timeDebug
	BOOL bDisableAllDebug = FALSE
	PROC NET_DW_PRINT(STRING sText)
		IF NOT bDisableAllDebug
			NET_PRINT_TIME() NET_PRINT("[FM_HoldUpTut] [DSW] ") NET_PRINT_STRINGS(" ", sText) NET_NL()
		ENDIF
	ENDPROC 

	PROC NET_DW_PRINT_STRING_INT(STRING sText1, INT i)
		IF NOT bDisableAllDebug
			NET_PRINT_TIME() NET_PRINT("[FM_HoldUpTut] [DSW] ") NET_PRINT_STRING_INT(sText1, i) NET_NL()
		ENDIF
	ENDPROC

	PROC NET_DW_PRINT_STRING_FLOAT(STRING sText1, FLOAT f)
		IF NOT bDisableAllDebug
			NET_PRINT_TIME() NET_PRINT("[FM_HoldUpTut] [DSW] ") NET_PRINT_STRING_FLOAT(sText1, f) NET_NL()
		ENDIF
	ENDPROC

	PROC NET_DW_PRINT_STRING_VECTOR(STRING sText1, VECTOR v)
		IF NOT bDisableAllDebug
			NET_PRINT_TIME() NET_PRINT("[FM_HoldUpTut] [DSW] ") NET_PRINT_STRING_VECTOR(sText1, v) NET_NL()
		ENDIF
	ENDPROC
	
	PROC NET_DW_PRINT_STRINGS(STRING sText1, STRING sText2)
		IF NOT bDisableAllDebug
			NET_PRINT_TIME() NET_PRINT("[FM_HoldUpTut] [DSW] ") NET_PRINT_STRINGS(sText1, sText2) NET_NL()
		ENDIF
	ENDPROC
	
	PROC NET_DW_PRINT_STRING_WITH_PLAYER_NAME(STRING sText, PLAYER_INDEX player)
		IF NOT bDisableAllDebug
			NET_PRINT_TIME() NET_PRINT("[FM_HoldUpTut] [DSW] ") NET_PRINT_STRINGS(" ", sText) NET_PRINT_STRINGS(" ", GET_PLAYER_NAME(player)) NET_NL()
		ENDIF
	ENDPROC
	
	INT iWdPartner
	INT iWdLoc
	INT iWdTutProg
	BOOL bWdShowPlayerInfo
	BOOL bResetTut
	BOOL bWdCheckInUse
	BOOL bWdCheckLocValid
	BOOL bWdDoStaggeredDebug
	BOOL bWdResetMyHoldUp
	BOOL bWdStillValid
	BOOL bWdDoValidStoreDebug
	BOOL bWdBlipAllMeetLoc
	BOOL bWdGetSpeed
	BOOL bWdShowDist
	FLOAT fWdSpeed
	FLOAT fWdDist
	BLIP_INDEX blipAllMeetLoc[MAX_HOLD_UP_TUT_MEET_LOC]
	
	PROC CREATE_WIDGETS()
		START_WIDGET_GROUP("Hold up Tut")
			//Profiling widgets
			#IF SCRIPT_PROFILER_ACTIVE
			CREATE_SCRIPT_PROFILER_WIDGET()
			#ENDIF		
			ADD_WIDGET_BOOL("Blip All meet loc", bWdBlipAllMeetLoc)
			ADD_WIDGET_FLOAT_READ_ONLY("Speed", fWdSpeed)
			ADD_WIDGET_BOOL("Show speed", bWdGetSpeed)
			ADD_WIDGET_FLOAT_READ_ONLY("Dist", fWdDist)
			ADD_WIDGET_BOOL("Show Dist", bWdShowDist)
			START_WIDGET_GROUP("Player")
				ADD_WIDGET_BOOL("SHow player info", bWdShowPlayerInfo)
				ADD_WIDGET_INT_READ_ONLY("Tut Prog", iWdTutProg)
				ADD_WIDGET_INT_READ_ONLY("Partner", iWdPartner)
				ADD_WIDGET_INT_READ_ONLY("Loc", iWdLoc)
				ADD_WIDGET_INT_SLIDER("Reach holdup timout",MAX_TIME_GET_TO_HOLD_UP, 1000, 99999999, 1)
				ADD_WIDGET_INT_SLIDER("Rejoin after timout kick",RETRY_AFTER_TIMEOUT_KICK, 1000, 99999999, 1)
				ADD_WIDGET_BOOL("Clear stat", bResetTut)
				ADD_WIDGET_BOOL("Still valid?", bWdStillValid)
				ADD_WIDGET_BOOL("Reset holdup", bWdResetMyHoldUp)
				ADD_WIDGET_BOOL("Do valid store debug", bWdDoValidStoreDebug)
				ADD_BIT_FIELD_WIDGET("iFmHoldUpTutBitset", fmHoldUpTutStruct.iFmHoldUpTutBitset)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Server")
				ADD_WIDGET_BOOL("Loc in use", bWdCheckInUse)
				ADD_WIDGET_BOOL("Loc Valid", bWdCheckLocValid)
				ADD_WIDGET_BOOL("Staggered debug ", bWdDoStaggeredDebug)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
	ENDPROC
	
	
#ENDIF

FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iGameState
ENDFUNC

FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC
/// PURPOSE:
///    Handles the cleanup of the script
PROC CLEANUP_SCRIPT()

	#IF IS_DEBUG_BUILD
		NET_DW_PRINT("Running cleanup")
	#ENDIF
	
//	mpglobalsAmbience.iHoldUpTutBlipLongRange = -1
//	mpglobalsAmbience.iHoldUPTutBlipFlash = -1
	SET_IGNORE_NO_GPS_FLAG_UNTIL_FIRST_NORMAL_NODE(FALSE)
	Clear_Any_Objective_Text_From_This_Script()
	SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_HOLD_UP_TUT, FALSE)
	
	NETWORK_SET_FRIENDLY_FIRE_OPTION(TRUE)
	
	IF IS_PLAYER_ABOUT_TO_START_HOLD_UP_TUTORIAL(PLAYER_ID())
		SET_LOCAL_PLAYER_ABOUT_TO_START_HOLD_UP_TUTORIAL(FALSE)
	ENDIF

	//SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), FALSE)
	//TERMINATE_THIS_THREAD()
	//RESET_FMMC_MISSION_VARIABLES(FALSE, FALSE, ciFMMC_END_OF_MISSION_STATUS_PASSED)
	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

/// PURPOSE:
///    Do necessary pre game start ini.
PROC PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission) , missionScriptArgs)
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	// KGM: Wait for the first network broadcast before moving on - call this as the last instruction in pre-game
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		#IF IS_DEBUG_BUILD
			NET_DW_PRINT("Failed to receive initial network broadcast. Cleaning up.")
		#ENDIF	
		CLEANUP_SCRIPT()			
	ENDIF
	
	CLEAR_BIT(fmHoldUpTutStruct.iFmHoldUpTutBitset, biHT_StartHoldUpTut)
	CLEAR_BIT(fmHoldUpTutStruct.iFmHoldUpTutBitset, biHT_InTriggeredHoldUp)
	CLEAR_BIT(fmHoldUpTutStruct.iFmHoldUpTutBitset, biHT_CompletedWithPartner)
	SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_HOLD_UP_TUT, TRUE)
	
	IF NOT IS_PLAYER_ABOUT_TO_START_HOLD_UP_TUTORIAL(PLAYER_ID())
		SET_LOCAL_PLAYER_ABOUT_TO_START_HOLD_UP_TUTORIAL(TRUE)
	ENDIF
	
	//mpglobalsAmbience.iHoldUpTutBlipLongRange = -1
	//mpglobalsAmbience.iHoldUPTutBlipFlash = -1
	#IF IS_DEBUG_BUILD
		PRINT_HOLD_UP_STRING("Launching....")
	#ENDIF
ENDPROC

FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET()
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllPlayersFinished)
		#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("[HAVE_MISSION_END_CONDITIONS_BEEN_MET] True because biS_AllPlayersFinished") #ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


PROC SET_HOLD_UP_RECENTLY_ALLOCATED(INT iHoldUp)
	#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING_INT("[HoldUpTutServer] SET_HOLD_UP_RECENTLY_ALLOCATED() called with iHoldUp = ", iHoldUp, TRUE)#ENDIF
	SET_BIT(serverBD.iRecentlyAllocated, iHoldUp)
ENDPROC

PROC CLEAR_ALL_HOLD_UP_RECENTLY_ALLOCATED()
	#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("[HoldUpTutServer] CLEAR_ALL_HOLD_UP_RECENTLY_ALLOCATED() called", TRUE) #ENDIF
	serverBD.iRecentlyAllocated = 0 
ENDPROC

FUNC BOOL WAS_HOLD_UP_RECENTLY_ALLOCATED(INT iHoldUp)
	RETURN (IS_BIT_SET(serverBD.iRecentlyAllocated, iHoldUp))
ENDFUNC
/// PURPOSE:
///    Hold up locations are allocated to players doing the tutorial. Need to check if they are still required as players may leave / finish the tutorial etc. 
/// PARAMS:
///    iHoldUP - 
FUNC BOOL IS_HOLD_UP_LOC_STILL_REQUIRED(INT iHoldUP)
	
	BOOL bStillRequired
	INT i
	#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING_INT("[IS_HOLD_UP_LOC_STILL_REQUIRED] Starting check for hold up = ", iHoldUP) #ENDIF
	
	IF WAS_HOLD_UP_RECENTLY_ALLOCATED(iHoldUP)
		#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING_INT("[IS_HOLD_UP_LOC_STILL_REQUIRED] True because WAS_HOLD_UP_RECENTLY_ALLOCATED() hold up = ", iHoldUP) #ENDIF
		RETURN TRUE
	ENDIF
	
	FOR i = 0 TO NUM_NETWORK_PLAYERS - 1
		#IF IS_DEBUG_BUILD 
	//		PRINT_HOLD_UP_STRING_INT("[IS_HOLD_UP_LOC_STILL_REQUIRED] Checking player int... ", i)
	//		PRINT_HOLD_UP_STRING_INT("[IS_HOLD_UP_LOC_STILL_REQUIRED]  serverBD.holdUpTutServer.iHoldUpToUse[i] = ",  serverBD.holdUpTutServer.iHoldUpToUse[i])
		#ENDIF
		
		PLAYER_INDEX player = INT_TO_NATIVE(PLAYER_INDEX, i)
		IF serverBD.holdUpTutServer.iHoldUpToUse[i] = iHoldUP
			#IF IS_DEBUG_BUILD
				PRINT_HOLD_UP_STRING("[IS_HOLD_UP_LOC_STILL_REQUIRED] Found a match!")
				PRINT_HOLD_UP_STRING_INT("[IS_HOLD_UP_LOC_STILL_REQUIRED] Hold up... ", iHoldUP)
				PRINT_HOLD_UP_STRING_INT("[IS_HOLD_UP_LOC_STILL_REQUIRED] Player... ", i)
			#ENDIF
			IF IS_NET_PLAYER_OK(player, FALSE)
				IF NOT IS_BIT_SET(GlobalplayerBD_FM[i].iHoldUpTutBitset, biHT_CompletedHoldUpTut)
					IF IS_BIT_SET(GlobalplayerBD_FM[i].iHoldUpTutBitset, biHT_DoingHoldUpTut)
						bStillRequired = TRUE
						#IF IS_DEBUG_BUILD 
							PRINT_HOLD_UP_STRING_INT("[IS_HOLD_UP_LOC_STILL_REQUIRED] This hold up loc still required... ", iHoldUP)
							PRINT_HOLD_UP_STRING_WITH_PLAYER_NAME("[IS_HOLD_UP_LOC_STILL_REQUIRED] This player needs it... ", player) 
						#ENDIF
					ELSE
						//-- Gets set when local player detects they've been allocated a store.
						#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("[IS_HOLD_UP_LOC_STILL_REQUIRED] matched player bit not set biHT_DoingHoldUpTut") #ENDIF
					ENDIF
				ELSE	
					#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("[IS_HOLD_UP_LOC_STILL_REQUIRED] matched player bit set biHT_CompletedHoldUpTut") #ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("[IS_HOLD_UP_LOC_STILL_REQUIRED] matched player not ok!") #ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	#IF IS_DEBUG_BUILD 
		IF NOT bStillRequired
			PRINT_HOLD_UP_STRING_INT("[IS_HOLD_UP_LOC_STILL_REQUIRED] This hold up loc no longer required... ", iHoldUP)
		ENDIF
	#ENDIF
							
	RETURN bStillRequired
ENDFUNC
FUNC INT GET_HOLD_UP_TO_USE_FOR_PLAYER_V2(INT iPlayerInt, INT &iInUseBitSet)
	//--Check distance from player and partner to each hold up
	//-- Use first one > 200m from both players not currently in use.
	
	BOOL bFoundHoldUP
	INT i
	INT iHoldupToUse = -1
	INT iMyPartner = serverBD.holdUpTutServer.iHoldUpPartner[iPlayerInt]
	
	FLOAT fDist1 = 0.0
	FLOAT fDist2 = 0.0
	
	FLOAT fMinDist2 = 200.0 * 200.0
	VECTOR vHoldUpLoc
	PLAYER_INDEX player1 = INT_TO_NATIVE(PLAYER_INDEX, iPlayerInt)
	PED_INDEX pedPLayer1 = GET_PLAYER_PED(player1)
	PLAYER_INDEX player2
	PED_INDEX pedPLayer2
	Crim_HOLD_UP_POINT myHoldUp
	
	IF iMyPartner > -1
		IF iMyPartner <> iPlayerInt
			IF (serverBD.holdUpTutServer.iHoldUpToUse[iMyPartner] = -1)
				player2 =  INT_TO_NATIVE(PLAYER_INDEX, iMyPartner)
				pedPLayer2 = GET_PLAYER_PED(player2)
				FOR i = 0 TO (MAX_HOLD_UP_TUT_MEET_LOC - 1)
					IF NOT bFoundHoldUP
						IF IS_BIT_SET(iInUseBitSet, i)
							IF NOT IS_HOLD_UP_LOC_STILL_REQUIRED(i)
								CLEAR_BIT(iInUseBitSet, i)
							ENDIF
						ENDIF
						IF IS_HOLD_UP_STILL_VALID(i #IF IS_DEBUG_BUILD, TRUE #ENDIF)
							myHoldUp = GET_HOLD_UP_FROM_MEET_LOC(i)
							IF myHoldUp <> eCRIM_HUP_INVALID
								IF GlobalServerBD_HoldUp.HoldUpServerData.iNumPlayersInThisStore[ENUM_TO_INT(myHoldUp)] = 0
									IF NOT IS_BIT_SET(iInUseBitSet, i)
										IF NOT IS_PED_INJURED(pedPlayer1)
											IF NOT IS_PED_INJURED(pedPlayer2)
												//-- Not using hold up
												
												#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING_INT("[GET_HOLD_UP_TO_USE_FOR_PLAYER] Checking hold up loc ", i, TRUE) #ENDIF
												vHoldUpLoc = GET_HOLD_UP_MEET_LOCATION(i)
												fDist1 = VDIST2(GET_ENTITY_COORDS(pedPlayer1), vHoldUpLoc)
												#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING_FLOAT("[GET_HOLD_UP_TO_USE_FOR_PLAYER] Player 1 distance  ", fDist1, TRUE) #ENDIF
												IF fDist1 > fMinDist2
													fDist2 = VDIST2(GET_ENTITY_COORDS(pedPlayer2), vHoldUpLoc)
													#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING_FLOAT("[GET_HOLD_UP_TO_USE_FOR_PLAYER] Player 2 distance  ", fDist2, TRUE) #ENDIF
													IF fDist2 > fMinDist2
														iHoldupToUse = i
														bFoundHoldUP = TRUE
														SET_BIT(iInUseBitSet, i)
														#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Distance is good", TRUE) #ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
														
									ENDIF
								ELSE
									#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING_INT("[GET_HOLD_UP_TO_USE_FOR_PLAYER] (with partner check) There's someone already in this hold up: ", i, TRUE) #ENDIF
								ENDIF
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING_INT("[GET_HOLD_UP_TO_USE_FOR_PLAYER] (with partner check) Hold up loc not valid! ", i, TRUE) #ENDIF
						ENDIF
					ENDIF
				ENDFOR	
			ELSE	
				#IF IS_DEBUG_BUILD
					PRINT_HOLD_UP_STRING_INT("My partner already has a destination, so using theirs... ", serverBD.holdUpTutServer.iHoldUpToUse[iMyPartner], TRUE)
				#ENDIF
				iHoldupToUse = serverBD.holdUpTutServer.iHoldUpToUse[iMyPartner]
			ENDIF
		ENDIF
	ELSE	
		#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("[GET_HOLD_UP_TO_USE_FOR_PLAYER] Trying to find location for just myself", TRUE) #ENDIF
		//-- Doing tutorial solo, for now
		FOR i = 0 TO (MAX_HOLD_UP_TUT_MEET_LOC - 1)
			IF NOT bFoundHoldUP
				IF IS_HOLD_UP_STILL_VALID(i #IF IS_DEBUG_BUILD, TRUE #ENDIF)
					myHoldUp = GET_HOLD_UP_FROM_MEET_LOC(i)
					IF myHoldUp <> eCRIM_HUP_INVALID
						IF GlobalServerBD_HoldUp.HoldUpServerData.iNumPlayersInThisStore[ENUM_TO_INT(myHoldUp)] = 0
							IF NOT IS_BIT_SET(iInUseBitSet, i)
								IF NOT IS_PED_INJURED(pedPlayer1)
									
									//-- Not using hold up
									
									#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING_INT("[GET_HOLD_UP_TO_USE_FOR_PLAYER] (Solo) Checking hold up loc ", i, TRUE) #ENDIF
									vHoldUpLoc = GET_HOLD_UP_MEET_LOCATION(i)
									fDist1 = VDIST2(GET_ENTITY_COORDS(pedPlayer1), vHoldUpLoc)
									#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING_FLOAT("[GET_HOLD_UP_TO_USE_FOR_PLAYER] (Solo) Player 1 distance  ", fDist1, TRUE) #ENDIF
									IF fDist1 > fMinDist2
										
										iHoldupToUse = i
										bFoundHoldUP = TRUE
										SET_BIT(iInUseBitSet, i)
										#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("(Solo) Distance is good", TRUE) #ENDIF
										
									ENDIF
									
								ENDIF
												
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING_INT("[GET_HOLD_UP_TO_USE_FOR_PLAYER] (NO partner check) There's someone already in this hold up: ", i, TRUE) #ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR	
	ENDIF
	
	RETURN iHoldupToUse
ENDFUNC

PROC MAINTAIN_SERVER_STAGGERED_LOOP()
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF iServerStaggeredLoopCount = 0
			CLEAR_BIT(iBoolsBitSet, biL_ServerLoopedThroughEveryone)
			SET_BIT(iBoolsBitSet, biL_StaggeredAllPlayersFinished)
			#IF IS_DEBUG_BUILD
				IF bWdDoStaggeredDebug
					PRINT_HOLD_UP_STRING("[ServStag] At Start of server staggered loop")
				ENDIF
			#ENDIF
		ENDIF
		
		PLAYER_INDEX playerPartner
		PLAYER_INDEX player = INT_TO_NATIVE(PLAYER_INDEX, iServerStaggeredLoopCount )
		INT iPartner = serverBD.holdUpTutServer.iHoldUpPartner[iServerStaggeredLoopCount]
	//	INT iMeetLoc = serverBD.holdUpTutServer.iHoldUpToUse[iServerStaggeredLoopCount]
		
		IF IS_NET_PLAYER_OK(player, FALSE)
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[iServerStaggeredLoopCount].iHoldUpTutBitset, biHT_CompletedHoldUpTut)
				CLEAR_BIT(iBoolsBitSet, biL_StaggeredAllPlayersFinished)
				#IF IS_DEBUG_BUILD
					IF bWdDoStaggeredDebug
						PRINT_HOLD_UP_STRING_WITH_PLAYER_NAME("[ServStag] Clearing biL_StaggeredAllPlayersFinished because this player not finished ", player)
					ENDIF
				#ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				//	PRINT_HOLD_UP_STRING_WITH_PLAYER_NAME("[ServStag] Player has finished ", player)
				#ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(GlobalplayerBD_FM[iServerStaggeredLoopCount].iHoldUpTutBitset, biHT_ReadyForHoldUpTut)
			
			IF IS_NET_PLAYER_OK(player, FALSE)
				IF iPartner > -1
					playerPartner = INT_TO_NATIVE(PLAYER_INDEX, iPartner )
					IF NOT IS_NET_PLAYER_OK(playerPartner, FALSE)
						serverBD.holdUpTutServer.iHoldUpPartner[iServerStaggeredLoopCount] = -1
						#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING_WITH_PLAYER_NAME("[ServStag] Partner not ok. Clearing partner for player....", player, TRUE) #ENDIF
					ENDIF
				ENDIF
				
				
				
				
			ELSE
				
				IF serverBD.holdUpTutServer.iHoldUpPartner[iServerStaggeredLoopCount] <> -1
					#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING_INT("[ServStag] Setting my partner = -1 as player not ok ", iServerStaggeredLoopCount, TRUE)  #ENDIF
					serverBD.holdUpTutServer.iHoldUpPartner[iServerStaggeredLoopCount] = -1
				ENDIF
				IF serverBD.holdUpTutServer.iHoldUpToUse[iServerStaggeredLoopCount] <> -1 
					#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING_INT("[ServStag] Setting my iHoldUpToUse = -1 as player not ok ", iServerStaggeredLoopCount, TRUE)  #ENDIF
					serverBD.holdUpTutServer.iHoldUpToUse[iServerStaggeredLoopCount] = -1
				ENDIF
			/*	IF iPartner > -1
					serverBD.holdUpTutServer.iHoldUpPartner[iPartner] = -1
					#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING_INT("Setting my partner = -1 as player not ok ", iServerStaggeredLoopCount)  #ENDIF
				ENDIF */
			ENDIF
		ELSE	
			//-- Deal with player's kicked for taking too long
			IF IS_NET_PLAYER_OK(player, FALSE)
				IF IS_BIT_SET(GlobalplayerBD_FM[iServerStaggeredLoopCount].iHoldUpTutBitset, biHT_TookTooLongToGetToHoldUpLoc)
					IF serverBD.holdUpTutServer.iHoldUpPartner[iServerStaggeredLoopCount] <> -1
						#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING_WITH_PLAYER_NAME("[ServStag] Setting partner = -1 as player kicked for taking too long ", player, TRUE)  #ENDIF
						serverBD.holdUpTutServer.iHoldUpPartner[iServerStaggeredLoopCount] = -1
					ENDIF
					IF serverBD.holdUpTutServer.iHoldUpToUse[iServerStaggeredLoopCount] <> -1 
						#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING_WITH_PLAYER_NAME("[ServStag] Setting iHoldUpToUse = -1 as player kicked for taking too long ", player, TRUE)  #ENDIF
						serverBD.holdUpTutServer.iHoldUpToUse[iServerStaggeredLoopCount] = -1
					ENDIF
					
					//-- If this player was kicked, then need to tell this player's partner that they're no longer valid
					IF iPartner > -1
						playerPartner = INT_TO_NATIVE(PLAYER_INDEX, iPartner )
						IF IS_NET_PLAYER_OK(playerPartner, FALSE)
							serverBD.holdUpTutServer.iHoldUpPartner[iPartner] = -1
							#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING_WITH_PLAYER_NAME("[ServStag] Player was kicked for taking to long, there partner was player ", playerPartner, TRUE) #ENDIF
						ELSE
							#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Partner 2", TRUE)#ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Partner 1", TRUE)#ENDIF
					ENDIF
					
				ENDIF
				
				
			ENDIF
		ENDIF
		
		iServerStaggeredLoopCount++
		IF iServerStaggeredLoopCount = NUM_NETWORK_PLAYERS
			#IF IS_DEBUG_BUILD
				IF bWdDoStaggeredDebug
					PRINT_HOLD_UP_STRING("[ServStag] At End of server staggered loop")
				ENDIF
			#ENDIF
			IF IS_BIT_SET(iBoolsBitSet, biL_StaggeredAllPlayersFinished)
				SET_BIT(serverBD.iServerBitSet, biS_AllPlayersFinished)
				#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("[ServStag] Setting biS_AllPlayersFinished because all players finished", TRUE) #ENDIF
			ENDIF
			SET_BIT(iBoolsBitSet, biL_ServerLoopedThroughEveryone)
			iServerStaggeredLoopCount = 0
		ENDIF
	ENDIF
ENDPROC	

FUNC INT GET_HOLD_UP_PARTNER_FOR_PLAYER(INT iPlayerInt, INT &iPlayersReadyForHoldup)
	INT i
	INT iPartner = -1
	FOR i = 0 TO NUM_NETWORK_PLAYERS - 1
		IF iPartner = -1
			IF i <> iPlayerInt
				IF IS_BIT_SET(iPlayersReadyForHoldup, i)
					IF IS_BIT_SET(GlobalplayerBD_FM[i].iHoldUpTutBitset, biHT_ReadyForHoldUpTut)
						IF (NOT IS_BIT_SET(GlobalplayerBD_FM[i].iHoldUpTutBitset, biHT_DoingHoldUpTut))
					//	OR IS_BIT_SET(GlobalplayerBD_FM[i].iHoldUpTutBitset, biHT_NeedNewPartner) - Can't match up with players already doing hold up, as stores will differ
							IF NOT IS_BIT_SET(GlobalplayerBD_FM[i].iHoldUpTutBitset, biHT_ReachedMeetLoc)
								IF NOT IS_BIT_SET(GlobalplayerBD_FM[i].iHoldUpTutBitset, biHT_DoingHoldUpSolo)
									IF serverBD.holdUpTutServer.iHoldUpPartner[i] = -1
										iPartner = i
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN iPartner
ENDFUNC

PROC MAINTAIN_HOLD_UP_TUTORIAL_SERVER_V2()
	INT i
	//PARTICIPANT_INDEX ParticipantID
	PLAYER_INDEX PlayerID
	
//	INT iNumPlayersInSession = 0
//	INT iNumPlayersReadyForHoldup = 0
//	INT iPlayersReadyForHoldUpTut = 0
	INT iTemp = 0
	
	
//	BOOL bFoundAPairing
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		SWITCH serverBD.holdUpTutServer.iHoldUpTutServerProg
			CASE SERVER_HOLD_UP_TUT_STAGE_WAIT_FOR_EVENT
				//-- Initialise partner list
				IF NOT IS_BIT_SET(serverBD.holdUpTutServer.iHoldUpTutorialBitset, biHTs_Initialised)
					FOR i=0 TO NUM_NETWORK_PLAYERS - 1
						serverBD.holdUpTutServer.iHoldUpPartner[i] = -1
						serverBD.holdUpTutServer.iHoldUpToUse[i] = -1
						
					ENDFOR
					SET_BIT(serverBD.holdUpTutServer.iHoldUpTutorialBitset, biHTs_Initialised)
					
					#IF IS_DEBUG_BUILD
						PRINT_HOLD_UP_STRING("[HoldUpTutServer] Initialised server", TRUE)
					#ENDIF
				ENDIF
				
				IF MPGlobalsAmbience.bSomeoneWantsToStartHoldUpTut
					SET_BIT(serverBD.holdUpTutServer.iHoldUpTutorialBitset, biHTs_SomeoneWantsToStartHoldUpTut)
					MPGlobalsAmbience.bSomeoneWantsToStartHoldUpTut = FALSE
					#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("[HoldUpTutServer] Setting server bit as MPGlobalsAmbience.bSomeoneWantsToStartHoldUpTut is set", TRUE) #ENDIF
				ENDIF
				
				IF IS_BIT_SET(serverBD.holdUpTutServer.iHoldUpTutorialBitset, biHTs_Initialised)
					IF IS_BIT_SET(serverBD.holdUpTutServer.iHoldUpTutorialBitset, biHTs_SomeoneWantsToStartHoldUpTut)
						//-- Someone needs a partner for the holdups
						IF NOT HAS_NET_TIMER_STARTED(serverBD.holdUpTutServer.timeCheckForPlayers)
						OR HAS_NET_TIMER_EXPIRED(serverBD.holdUpTutServer.timeCheckForPlayers, 5000)
							IF serverBD.holdUpTutServer.iHoldUpTutorialServerLoopCount = 0
								serverBD.holdUpTutServer.iNumPlayersReadyForTut = 0 
								CLEAR_BIT(serverBD.holdUpTutServer.iHoldUpTutorialBitset, biHTs_FoundAPairing)
								CLEAR_BIT(serverBD.holdUpTutServer.iHoldUpTutorialBitset, biHts_FoundAMeetLoc)
							ENDIF
							
							//FOR i=0 TO NUM_NETWORK_PLAYERS - 1
							i = serverBD.holdUpTutServer.iHoldUpTutorialServerLoopCount
							PlayerID = INT_TO_NATIVE(PLAYER_INDEX, i)
						//	ParticipantID = INT_TO_NATIVE(PARTICIPANT_INDEX, i)
							

						//	IF NETWORK_IS_PARTICIPANT_ACTIVE(ParticipantID)
							IF NETWORK_IS_PLAYER_ACTIVE(PlayerID)
								//PlayerID = NETWORK_GET_PLAYER_INDEX(ParticipantID) 
								
								
								
								IF IS_BIT_SET(GlobalplayerBD_FM[i].iHoldUpTutBitset, biHT_ReadyForHoldUpTut)
									SET_BIT(serverBD.holdUpTutServer.iPlayersReadyForHoldUpTutBitset, NATIVE_TO_INT(PlayerID))
									
									serverBD.holdUpTutServer.iNumPlayersReadyForTut++
									
									#IF IS_DEBUG_BUILD 
										PRINT_HOLD_UP_STRING_WITH_PLAYER_NAME("[HoldUpTutServer] Server thinks this player ready for holdup tut... ", PlayerID, TRUE) 
										PRINT_HOLD_UP_STRING_INT("[HoldUpTutServer] That player's Int is... ", NATIVE_TO_INT(PlayerID), TRUE)
									#ENDIF
									
									IF IS_BIT_SET(GlobalplayerBD_FM[i].iHoldUpTutBitset, biHT_NeedNewPartner)
										serverBD.holdUpTutServer.iHoldUpPartner[i] = -1
										#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING_WITH_PLAYER_NAME("[HoldUpTutServer] biHT_NeedNewPartner set for player. Clearing partner for player....", PlayerID, TRUE) #ENDIF
									ENDIF
								ENDIF
							ENDIF
							//ENDFOR
							
							serverBD.holdUpTutServer.iHoldUpTutorialServerLoopCount++
							
							IF serverBD.holdUpTutServer.iHoldUpTutorialServerLoopCount = NUM_NETWORK_PLAYERS
								RESET_NET_TIMER(serverBD.holdUpTutServer.timeCheckForPlayers)
								START_NET_TIMER(serverBD.holdUpTutServer.timeCheckForPlayers)
								#IF IS_DEBUG_BUILD
									
									PRINT_HOLD_UP_STRING_INT("[HoldUpTutServer] Server thinks this many players ready for hold up tut... ", serverBD.holdUpTutServer.iNumPlayersReadyForTut, TRUE)
								#ENDIF
								
								
								
								IF serverBD.holdUpTutServer.iNumPlayersReadyForTut >= 1
									serverBD.holdUpTutServer.iHoldUpTutServerProg = SERVER_HOLD_UP_TUT_STAGE_FIND_PARTNERS
									CLEAR_BIT(serverBD.holdUpTutServer.iHoldUpTutorialBitset, biHTs_FoundAPairing)
									#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING_INT("[HoldUpTutServer] Server moving to SERVER_HOLD_UP_TUT_STAGE_FIND_PARTNERS as iNumPlayersReadyForHoldup = ",serverBD.holdUpTutServer.iNumPlayersReadyForTut, TRUE) #ENDIF
								ENDIF
							
								serverBD.holdUpTutServer.iHoldUpTutorialServerLoopCount = 0
							ENDIF
							
							
							
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE SERVER_HOLD_UP_TUT_STAGE_FIND_PARTNERS
				//-- Find a partner
			//	bFoundAPairing = FALSE
				
			//	FOR i=0 TO NUM_NETWORK_PLAYERS - 1
				IF serverBD.holdUpTutServer.iHoldUpTutorialServerLoopCount = 0
					CLEAR_BIT(serverBD.holdUpTutServer.iHoldUpTutorialBitset, biHTs_FoundAPairing)
				ENDIF
				
				i = serverBD.holdUpTutServer.iHoldUpTutorialServerLoopCount
				IF IS_BIT_SET(serverBD.holdUpTutServer.iPlayersReadyForHoldUpTutBitset, i)
					IF IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, i))
						IF NOT IS_BIT_SET(GlobalplayerBD_FM[i].iHoldUpTutBitset, biHT_DoingHoldUpSolo)
							//-- Check actually still ready
							IF IS_BIT_SET(GlobalplayerBD_FM[i].iHoldUpTutBitset, biHT_ReadyForHoldUpTut)
								#IF IS_DEBUG_BUILD
									PRINT_HOLD_UP_STRING_WITH_PLAYER_NAME("[HoldUpTutServer] Trying to find hold up partner for player... ", INT_TO_NATIVE(PLAYER_INDEX, i), TRUE)		
								#ENDIF
								
								IF serverBD.holdUpTutServer.iHoldUpPartner[i] = -1
								//OR IS_BIT_SET(GlobalplayerBD_FM[i].iHoldUpTutBitset, biHT_NeedNewPartner)
									IF IS_BIT_SET(GlobalplayerBD_FM[i].iHoldUpTutBitset, biHT_NeedNewPartner)
										#IF IS_DEBUG_BUILD
											PRINT_HOLD_UP_STRING_WITH_PLAYER_NAME("[HoldUpTutServer] biHT_NeedNewPartner is set for player ", INT_TO_NATIVE(PLAYER_INDEX, i), TRUE)
											IF serverBD.holdUpTutServer.iHoldUpPartner[i] > -1
												PRINT_HOLD_UP_STRING_INT("[HoldUpTutServer] There old partner had player int = ", serverBD.holdUpTutServer.iHoldUpPartner[i])
												IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(serverBD.holdUpTutServer.iHoldUpPartner[i]))
													PRINT_HOLD_UP_STRING_WITH_PLAYER_NAME("[HoldUpTutServer] which was player",INT_TO_PLAYERINDEX(serverBD.holdUpTutServer.iHoldUpPartner[i]), TRUE)
												ELSE
													PRINT_HOLD_UP_STRING("[HoldUpTutServer] Their old partner is no longer ok", TRUE)
												ENDIF
											ENDIF
										#ENDIF
										serverBD.holdUpTutServer.iHoldUpPartner[i] = -1
									ENDIF
									iTemp = GET_HOLD_UP_PARTNER_FOR_PLAYER(i, serverBD.holdUpTutServer.iPlayersReadyForHoldUpTutBitset)
									
									IF iTemp > -1

										#IF IS_DEBUG_BUILD
											PRINT_HOLD_UP_STRING_WITH_PLAYER_NAME("[HoldUpTutServer] Found a hold up partner... ", INT_TO_NATIVE(PLAYER_INDEX, iTemp), TRUE)	
											PRINT_HOLD_UP_STRING_INT("[HoldUpTutServer] Found a hold up partner... ", iTemp, TRUE)
										#ENDIF
										serverBD.holdUpTutServer.iHoldUpPartner[i] = iTemp
										serverBD.holdUpTutServer.iHoldUpPartner[iTemp] = i
										SET_BIT(serverBD.holdUpTutServer.iHoldUpTutorialBitset, biHTs_FoundAPairing)

									ELSE	
										#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("[HoldUpTutServer] Failed to find a hold up partner!", TRUE) #ENDIF
									ENDIF
								ELSE	
									SET_BIT(serverBD.holdUpTutServer.iHoldUpTutorialBitset, biHTs_FoundAPairing)
									#IF IS_DEBUG_BUILD
										PRINT_HOLD_UP_STRING_WITH_PLAYER_NAME("[HoldUpTutServer] Already has a partner... ", INT_TO_NATIVE(PLAYER_INDEX, serverBD.holdUpTutServer.iHoldUpPartner[i]), TRUE)
									#ENDIF
								ENDIF	
							ENDIF
						ENDIF
					ENDIF
				ENDIF
		//		ENDFOR
				serverBD.holdUpTutServer.iHoldUpTutorialServerLoopCount++
				
				IF serverBD.holdUpTutServer.iHoldUpTutorialServerLoopCount = NUM_NETWORK_PLAYERS
					IF IS_BIT_SET(serverBD.holdUpTutServer.iHoldUpTutorialBitset, biHTs_FoundAPairing)
						#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("[HoldUpTutServer] Server found a pairing!", TRUE) #ENDIF
						
						
						
					ELSE	
						#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("[HoldUpTutServer] Server didn't find a pairing!", TRUE) #ENDIF
					ENDIF
					
					serverBD.holdUpTutServer.iHoldUpTutorialServerLoopCount = 0
					serverBD.holdUpTutServer.iHoldUpTutServerProg = SERVER_HOLD_UP_TUT_STAGE_FIND_MEET_LOC
					CLEAR_BIT(serverBD.holdUpTutServer.iHoldUpTutorialBitset, biHts_FoundAMeetLoc)
					CLEAR_ALL_HOLD_UP_RECENTLY_ALLOCATED()
					#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("[HoldUpTutServer] Server stage moving to SERVER_HOLD_UP_TUT_STAGE_FIND_MEET_LOC", TRUE) #ENDIF
				ENDIF
			BREAK
			
			CASE SERVER_HOLD_UP_TUT_STAGE_FIND_MEET_LOC
				//--Find a hold up meet location
				
				/*
					Will be simpler to only deal with the hold up already being done once the player(s) have
					got to the meet loc.
				*/
				
				INT iMyPartner
			//	BOOL bFoundMeetLoc
			//	FOR i=0 TO NUM_NETWORK_PLAYERS - 1
			
				IF serverBD.holdUpTutServer.iHoldUpTutorialServerLoopCount = 0
					CLEAR_BIT(serverBD.holdUpTutServer.iHoldUpTutorialBitset, biHts_FoundAMeetLoc)
				ENDIF
				
				i = serverBD.holdUpTutServer.iHoldUpTutorialServerLoopCount
				IF IS_BIT_SET(serverBD.holdUpTutServer.iPlayersReadyForHoldUpTutBitset, i)
					//-- CHeck still ready
					IF IS_BIT_SET(GlobalplayerBD_FM[i].iHoldUpTutBitset, biHT_ReadyForHoldUpTut)
						IF IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, i))
							#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING_WITH_PLAYER_NAME("[HoldUpTutServer] Trying to find hold up meet loc for player... ", INT_TO_NATIVE(PLAYER_INDEX, i), TRUE)#ENDIF
							IF serverBD.holdUpTutServer.iHoldUpToUse[i] = -1
					/*		OR IS_BIT_SET(GlobalplayerBD_FM[i].iHoldUpTutBitset, biHT_NeedNewHoldUpLoc)
								IF IS_BIT_SET(GlobalplayerBD_FM[i].iHoldUpTutBitset, biHT_NeedNewHoldUpLoc)	
									serverBD.holdUpTutServer.iHoldUpToUse[i] = -1
									#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING_WITH_PLAYER_NAME("This player needs a new hold up loc ", INT_TO_NATIVE(PLAYER_INDEX, i)) #ENDIF
								ENDIF
					*/			
								
								iMyPartner = serverBD.holdUpTutServer.iHoldUpPartner[i]
								IF iMyPartner > -1
									//-- Check partner still ready.
									IF IS_BIT_SET(GlobalplayerBD_FM[iMyPartner].iHoldUpTutBitset, biHT_ReadyForHoldUpTut)
										
										IF serverBD.holdUpTutServer.iHoldUpToUse[i] = -1
											
											iTemp = GET_HOLD_UP_TO_USE_FOR_PLAYER_V2(i, serverBD.holdUpTutServer.iHoldUpInUseBitset)
											#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING_INT("[HoldUpTutServer] Hold up to use...", iTemp, TRUE) #ENDIF
											IF iTemp > -1
												serverBD.holdUpTutServer.iHoldUpToUse[i] = iTemp
												serverBD.holdUpTutServer.iHoldUpToUse[iMyPartner] = iTemp
												SET_BIT(serverBD.holdUpTutServer.iHoldUpTutorialBitset, biHts_FoundAMeetLoc)
												SET_HOLD_UP_RECENTLY_ALLOCATED(iTemp)
											ENDIF
										ELSE
											//--Need to check that, if player suppsedly already has a hold up, it matches their partners
											IF serverBD.holdUpTutServer.iHoldUpToUse[i] = serverBD.holdUpTutServer.iHoldUpToUse[iMyPartner]
											OR serverBD.holdUpTutServer.iHoldUpToUse[iMyPartner] = -1
												SET_BIT(serverBD.holdUpTutServer.iHoldUpTutorialBitset, biHts_FoundAMeetLoc)
												#IF IS_DEBUG_BUILD
													PRINT_HOLD_UP_STRING_INT("[HoldUpTutServer] Already have a meet loc = ", serverBD.holdUpTutServer.iHoldUpToUse[i], TRUE)		
												#ENDIF
											ELSE
												#IF IS_DEBUG_BUILD
													PRINT_HOLD_UP_STRING_WITH_PLAYER_NAME("[HoldUpTutServer] Player already has a meet loc, but doesn't match partners! ", INT_TO_NATIVE(PLAYER_INDEX, i))
													PRINT_HOLD_UP_STRING_INT("[HoldUpTutServer] Player's meet loc = ", serverBD.holdUpTutServer.iHoldUpToUse[i], TRUE)	
													PRINT_HOLD_UP_STRING_INT("[HoldUpTutServer] Partner's meet loc = ", serverBD.holdUpTutServer.iHoldUpToUse[iMyPartner], TRUE)
												#ENDIF
												
												IF NOT IS_BIT_SET(GlobalplayerBD_FM[i].iHoldUpTutBitset, biHT_DoingHoldUpTut)
													#IF IS_DEBUG_BUILD
														PRINT_HOLD_UP_STRING_WITH_PLAYER_NAME("[HoldUpTutServer] Player hasn't started hold up tut, will use partners loc", INT_TO_NATIVE(PLAYER_INDEX, i))
														PRINT_HOLD_UP_STRING_INT("[HoldUpTutServer] Their loc is... ", serverBD.holdUpTutServer.iHoldUpToUse[iMyPartner])
													#ENDIF
													serverBD.holdUpTutServer.iHoldUpToUse[i] = serverBD.holdUpTutServer.iHoldUpToUse[iMyPartner]
												ELSE
													#IF IS_DEBUG_BUILD
														PRINT_HOLD_UP_STRING_WITH_PLAYER_NAME("[HoldUpTutServer] Player has already started hold up tut, will check partner ", INT_TO_NATIVE(PLAYER_INDEX, i))
													#ENDIF
													
													IF NOT IS_BIT_SET(GlobalplayerBD_FM[iMyPartner].iHoldUpTutBitset, biHT_DoingHoldUpTut)
														#IF IS_DEBUG_BUILD
															PRINT_HOLD_UP_STRING_WITH_PLAYER_NAME("[HoldUpTutServer] Partner hasn't started hold up tut, partner is ", INT_TO_NATIVE(PLAYER_INDEX, iMyPartner))
															PRINT_HOLD_UP_STRING_INT("[HoldUpTutServer] Will set partner's loc to ", serverBD.holdUpTutServer.iHoldUpToUse[i])
														#ENDIF
														serverBD.holdUpTutServer.iHoldUpToUse[iMyPartner] = serverBD.holdUpTutServer.iHoldUpToUse[i]
													ELSE
														#IF IS_DEBUG_BUILD
															PRINT_HOLD_UP_STRING("[HoldUpTutServer] partner has started hold up tut, will probably break.....")
														#ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ELSE
										#IF IS_DEBUG_BUILD 
											PRINT_HOLD_UP_STRING_WITH_PLAYER_NAME("[HoldUpTutServer] Trying to find a hold up loc, but partner no longer ready! Will reset partner. Partner is ",  INT_TO_NATIVE(PLAYER_INDEX, iMyPartner), TRUE) 
										#ENDIF
										
										serverBD.holdUpTutServer.iHoldUpPartner[i] = -1
										serverBD.holdUpTutServer.iHoldUpPartner[iMyPartner] = -1
									ENDIF
								ELSE
									//-- I don't currently have a partner
									iTemp = GET_HOLD_UP_TO_USE_FOR_PLAYER_V2(i, serverBD.holdUpTutServer.iHoldUpInUseBitset)
									#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING_INT("[HoldUpTutServer] [Solo) Hold up to use...", iTemp, TRUE) #ENDIF
									IF iTemp > -1
										SET_BIT(serverBD.holdUpTutServer.iHoldUpTutorialBitset, biHts_FoundAMeetLoc)
										serverBD.holdUpTutServer.iHoldUpToUse[i] = iTemp
										SET_HOLD_UP_RECENTLY_ALLOCATED(iTemp)
									ENDIF
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING_INT("[HoldUpTutServer] Already has a hold up loc...", serverBD.holdUpTutServer.iHoldUpToUse[i], TRUE) #ENDIF
								
								SET_BIT(serverBD.holdUpTutServer.iHoldUpTutorialBitset, biHts_FoundAMeetLoc)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			//	ENDFOR
				serverBD.holdUpTutServer.iHoldUpTutorialServerLoopCount++
				
				IF serverBD.holdUpTutServer.iHoldUpTutorialServerLoopCount = NUM_NETWORK_PLAYERS
					IF IS_BIT_SET(serverBD.holdUpTutServer.iHoldUpTutorialBitset, biHts_FoundAMeetLoc)
						#IF IS_DEBUG_BUILD 
							PRINT_HOLD_UP_STRING("[HoldUpTutServer] Broadcasting...", TRUE) 
							PRINT_HOLD_UP_STRING("[HoldUpTutServer] Server stage moving to SERVER_HOLD_UP_TUT_STAGE_WAIT_FOR_EVENT", TRUE) 
						#ENDIF
						BROADCAST_START_HOLDUP_TUTORIAL()
						CLEAR_BIT(serverBD.holdUpTutServer.iHoldUpTutorialBitset, biHTs_SomeoneWantsToStartHoldUpTut)
						serverBD.holdUpTutServer.iHoldUpTutServerProg = SERVER_HOLD_UP_TUT_STAGE_WAIT_FOR_EVENT
					ELSE
						#IF IS_DEBUG_BUILD
							PRINT_HOLD_UP_STRING("[HoldUpTutServer] NOT Broadcasting as didn't find meet loc...", TRUE)  
							PRINT_HOLD_UP_STRING("[HoldUpTutServer] Server stage moving to SERVER_HOLD_UP_TUT_STAGE_WAIT_FOR_EVENT",TRUE) 
						#ENDIF
						
						CLEAR_BIT(serverBD.holdUpTutServer.iHoldUpTutorialBitset, biHTs_SomeoneWantsToStartHoldUpTut)
						serverBD.holdUpTutServer.iHoldUpTutServerProg = SERVER_HOLD_UP_TUT_STAGE_WAIT_FOR_EVENT
					ENDIF
					serverBD.holdUpTutServer.iHoldUpTutorialServerLoopCount = 0
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC
FUNC BOOL IS_OK_TO_PRINT_HELP()
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
	OR IS_PED_INJURED(PLAYER_PED_ID())
	OR NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
	OR  IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
	OR IS_SCRIPT_HUD_DISPLAYING(HUDPART_MISSIONBRIEF)
	OR IS_MP_MISSION_DETAILS_OVERLAY_ON_DISPLAY()
	OR IS_SCRIPT_HUD_DISPLAYING(HUDPART_MISSIONBOX)
	OR IS_HELP_MESSAGE_BEING_DISPLAYED()
	OR IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
	OR IS_FM_MISSION_LAUNCH_IN_PROGRESS()
	OR IS_CUSTOM_MENU_ON_SCREEN() 
	OR IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
	OR IS_MISSION_SUMMARY_SCREEN_DISPLAYED()
	OR IS_PAUSE_MENU_ACTIVE()
	OR NETWORK_IS_IN_TUTORIAL_SESSION()
	OR IS_LOCAL_PLAYER_RUNNING_PASSIVE_MODE_CUTSCENE()
		RETURN(FALSE)
	ENDIF
	
	
	RETURN(TRUE)
ENDFUNC

PROC RESET_MY_HOLDUP()
	IF serverBD.holdUpTutServer.iHoldUpToUse[NATIVE_TO_INT(PLAYER_ID())] > -1
		Crim_HOLD_UP_POINT myHoldUp = GET_HOLD_UP_FROM_MEET_LOC(serverBD.holdUpTutServer.iHoldUpToUse[NATIVE_TO_INT(PLAYER_ID())])
		
		BROADCAST_MAKE_HOLD_UP_ACTIVE(ENUM_TO_INT(myHoldUp), TRUE, SPECIFIC_PLAYER(NETWORK_GET_HOST_OF_SCRIPT(FREEMODE_SCRIPT())))
		#IF IS_DEBUG_BUILD 
			DEBUG_PRINTCALLSTACK()
			PRINT_HOLD_UP_STRING("[RESET_MY_HOLDUP] Called... ")
			PLAYER_INDEX playerHost = NETWORK_GET_HOST_OF_SCRIPT(FREEMODE_SCRIPT())
			IF playerHost <> INVALID_PLAYER_INDEX()
				PRINT_HOLD_UP_STRING_WITH_PLAYER_NAME("[RESET_MY_HOLDUP] Call BROADCAST_MAKE_HOLD_UP_ACTIVE, sending to ", playerHost)
			ELSE
				PRINT_HOLD_UP_STRING("[RESET_MY_HOLDUP] Calling BROADCAST_MAKE_HOLD_UP_ACTIVE, but don't know who host of freemode is! ")
			ENDIF
		#ENDIF
	ELSE	
		#IF IS_DEBUG_BUILD 
			DEBUG_PRINTCALLSTACK()
			PRINT_HOLD_UP_STRING("[RESET_MY_HOLDUP] Called but my loc is -1! ") 
		#ENDIF
	ENDIF
ENDPROC


FUNC BOOL HAS_MY_PARTNER_COMPLETED_HOLD_UP()
	INT iPartner = serverBD.holdUpTutServer.iHoldUpPartner[NATIVE_TO_INT(PLAYER_ID())]
	IF iPartner > -1
	//	IF IS_MY_PARTNER_STILL_VALID(iPartner, FALSE)
			IF IS_BIT_SET(GlobalplayerBD_FM[iPartner].iHoldUpTutBitset, biHT_DidAHoldUp)
				RETURN TRUE
			ENDIF
	//	ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_MY_PARTNER_INVALIDATED_HOLD_UP()
	INT iPartner = serverBD.holdUpTutServer.iHoldUpPartner[NATIVE_TO_INT(PLAYER_ID())]
	IF iPartner > -1
	//	IF IS_MY_PARTNER_STILL_VALID(iPartner, FALSE)
			IF IS_BIT_SET(GlobalplayerBD_FM[iPartner].iHoldUpTutBitset, biHT_InvalidatedHoldup)
				RETURN TRUE
			ENDIF
	//	ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_HOLD_UP_TUT_CLIENT_PARTNER_CHECKS_V2(FM_PLAYER_HOLD_UP_TUT_STRUCT &holdUpTut)
	INT iMyPartner
	INT iMyPlayerInt
	
	
	iMyPlayerInt = NATIVE_TO_INT(PLAYER_ID())
	iMyPartner = serverBD.holdUpTutServer.iHoldUpPartner[iMyPlayerInt]
	
	IF NOT IS_BIT_SET(holdUpTut.iHoldUpTutClientBitset, biL_HoldUp_MeetSolo) // GOing to the meet place on my own (have no partner)
	AND NOT IS_BIT_SET(GlobalplayerBD_FM[iMyPlayerInt].iHoldUpTutBitset, biHT_DoingHoldUpSolo) // Doing the actual hold up on my own
		//-- I'm supposed to be doing the hold up with a partner. Check partner is ok.
		
		IF iMyPartner > -1
			
			
			IF IS_BIT_SET(GlobalplayerBD_FM[iMyPlayerInt].iHoldUpTutBitset, biHT_NeedNewPartner)
				CLEAR_BIT(GlobalplayerBD_FM[iMyPlayerInt].iHoldUpTutBitset, biHT_NeedNewPartner)
				#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("[MAINTAIN_HOLD_UP_TUT_CLIENT_PARTNER_CHECKS] Clearing biHT_NeedNewPartner as I have a New partner") #ENDIF
			ENDIF
			
			IF NOT IS_MY_PARTNER_STILL_VALID(iMyPartner #IF IS_DEBUG_BUILD, TRUE #ENDIF)
				CLEAR_BIT(holdUpTut.iHoldUpTutClientBitset, biL_HoldUp_GetToMeet)
				CLEAR_BIT(holdUpTut.iHoldUpTutClientBitset, biL_ToldToHoldUpStore)
			ENDIF
			
		ENDIF
	ELSE	
		IF holdUpTut.iHoldUpTutProg < HOLD_UP_TUT_STAGE_DO_HOLD_UP
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[iMyPlayerInt].iHoldUpTutBitset, biHT_NeedNewPartner)
				SET_BIT(GlobalplayerBD_FM[iMyPlayerInt].iHoldUpTutBitset, biHT_NeedNewPartner)
				#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("[MAINTAIN_HOLD_UP_TUT_CLIENT_PARTNER_CHECKS] Setting biHT_NeedNewPartner") #ENDIF
			ENDIF
		ELSE	
			IF IS_BIT_SET(GlobalplayerBD_FM[iMyPlayerInt].iHoldUpTutBitset, biHT_NeedNewPartner)
				CLEAR_BIT(GlobalplayerBD_FM[iMyPlayerInt].iHoldUpTutBitset, biHT_NeedNewPartner)
				#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("[MAINTAIN_HOLD_UP_TUT_CLIENT_PARTNER_CHECKS] Clearing biHT_NeedNewPartner as holdUpTut.iHoldUpTutProg >= HOLD_UP_TUT_STAGE_DO_HOLD_UP") #ENDIF
			ENDIF
		ENDIF
		
		IF iMyPartner > -1
			IF IS_MY_PARTNER_STILL_VALID(iMyPartner)
				CLEAR_BIT(holdUpTut.iHoldUpTutClientBitset, biL_HoldUp_MeetSolo)
				CLEAR_BIT(holdUpTut.iHoldUpTutClientBitset, biL_HoldUp_GetToMeet)
				CLEAR_BIT(GlobalplayerBD_FM[iMyPlayerInt].iHoldUpTutBitset, biHT_NeedNewPartner)
				#IF IS_DEBUG_BUILD 
					PRINT_HOLD_UP_STRING("[MAINTAIN_HOLD_UP_TUT_CLIENT_PARTNER_CHECKS] Clearing biL_HoldUp_MeetSolo as I have a partner") 
					PRINT_HOLD_UP_STRING("[MAINTAIN_HOLD_UP_TUT_CLIENT_PARTNER_CHECKS] Clearing biHT_NeedNewPartner as I have a partner")
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC
FUNC BOOL IS_MY_HOLD_UP_STILL_VALID()

	INT iMyHoldUpLoc = serverBD.holdUpTutServer.iHoldUpToUse[NATIVE_TO_INT(PLAYER_ID())]
	
	INT iMyHoldUp = ENUM_TO_INT(GET_HOLD_UP_FROM_MEET_LOC(iMyHoldUpLoc))
	
	IF IS_BIT_SET(GlobalServerBD_HoldUp.HoldUpServerData.iHoldUpFakeDoneBitSet, iMyHoldUp) 
		
		RETURN TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	//	PRINT_HOLD_UP_STRING("[IS_MY_HOLD_UP_STILL_VALID] bit NOT set!")
	#ENDIF 
	
	IF iMyHoldUpLoc > -1
		IF IS_HOLD_UP_STILL_VALID(iMyHoldUpLoc)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
	
ENDFUNC
FUNC BOOL IS_LOCAL_PLAYER_NEAR_HOLD_UP_LOCATION(FLOAT fDistance = 7.5)
	INT iMyPlayerInt = NATIVE_TO_INT(PLAYER_ID())
	VECTOR vHoldUp 
	VECTOR vPlayerLoc
	
	IF iMyPlayerInt > -1
		vHoldUp = GET_HOLD_UP_BLIP_LOCATION_FROM_MEET_LOC( serverBD.holdUpTutServer.iHoldUpToUse[iMyPlayerInt])
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			vPlayerLoc = GET_ENTITY_COORDS(PLAYER_PED_ID())
			IF VDIST2(vPlayerLoc, vHoldUp) < (fDistance * fDistance)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_APPROACHING_HOLD_UP_LOCATION_AT_SPEED(FLOAT fMinSpeed = 20.0, FLOAT fDistance = 75.0)


	IF NOT IS_PED_INJURED(PLAYER_PED_ID())

		IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF GET_ENTITY_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) > fMinSpeed
				IF IS_LOCAL_PLAYER_NEAR_HOLD_UP_LOCATION(fDistance)
					IF NOT IS_BIT_SET(iBoolsBitSet, biL_PlayerApproachedAtSpeed)
						SET_BIT(iBoolsBitSet, biL_PlayerApproachedAtSpeed)
						#IF IS_DEBUG_BUILD
							PRINT_HOLD_UP_STRING("[IS_LOCAL_PLAYER_APPROACHING_HOLD_UP_LOCATION_AT_SPEED] Set biL_PlayerApproachedAtSpeed")
						#ENDIF
					ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	RETURN FALSE
ENDFUNC




FUNC BOOL IS_LOCAL_PLAYER_IN_HOLD_UP_LOCATION()
	INTERIOR_INSTANCE_INDEX interior
	VECTOR v = GET_HOLD_UP_BLIP_LOCATION_FROM_MEET_LOC( serverBD.holdUpTutServer.iHoldUpToUse[NATIVE_TO_INT(PLAYER_ID())])
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF NOT ARE_VECTORS_EQUAL(v, <<0.0, 0.0, 0.0>>)	
			interior = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
			IF interior <> NULL
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), v) < 25.0
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_HOLD_UP_TUT_MEET_LOC_CHECKS(FM_PLAYER_HOLD_UP_TUT_STRUCT &holdUpTut #IF IS_DEBUG_BUILD, BOOL bDoDebug = FALSE #ENDIF)
	INT iHoldUpCount
	
	//-- Check for partner completeing hold up before I get there
	IF HAS_MY_PARTNER_COMPLETED_HOLD_UP() 
		IF holdUpTut.iHoldUpTutProg <= HOLD_UP_TUT_STAGE_GET_TO_HOLD_UP
			IF NOT IS_BIT_SET(holdUpTut.iHoldUpTutClientBitset ,biL_PartCompleteEarly)
				SET_BIT(holdUpTut.iHoldUpTutClientBitset ,biL_PartCompleteEarly)
				#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Partner completed hold up before I got there!") #ENDIF
			ENDIF
		ENDIF
	ENDIF
	#IF IS_DEBUG_BUILD
	INT iMyHoldUpLoc = serverBD.holdUpTutServer.iHoldUpToUse[NATIVE_TO_INT(PLAYER_ID())]
	#ENDIF
	
	
	INT iStatHoldUpCount
	
	//-- Deal with the player taking part in hold ups other than the one they've been sent to
	IF IS_LOCAL_PLAYER_DOING_HOLD_UP_TUTORIAL()
		IF NOT HAS_NET_TIMER_STARTED(timeCheckForCompletedHoldups)
		OR HAS_NET_TIMER_EXPIRED(timeCheckForCompletedHoldups, 5000)
			
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHT_ReachedMeetLoc)
				IF iInitialShopsHeldUpCount > -1
					iStatHoldUpCount = GET_MP_INT_CHARACTER_STAT(MP_STAT_TOTAL_NO_SHOPS_HELD_UP )
					
					
					
					IF iInitialShopsHeldUpCount <> iStatHoldUpCount
						#IF IS_DEBUG_BUILD
							PRINT_HOLD_UP_STRING("[MAINTAIN_HOLD_UP_TUT_MEET_LOC_CHECKS] I'm not at hold up, and iInitialShopsHeldUpCount <> MP_STAT_TOTAL_NO_SHOPS_HELD_UP. Will update")
							PRINT_HOLD_UP_STRING_INT("[MAINTAIN_HOLD_UP_TUT_MEET_LOC_CHECKS] MP_STAT_TOTAL_NO_SHOPS_HELD_UP = ", iStatHoldUpCount)
							PRINT_HOLD_UP_STRING_INT("[MAINTAIN_HOLD_UP_TUT_MEET_LOC_CHECKS] iInitialShopsHeldUpCount = ", iInitialShopsHeldUpCount)
						#ENDIF
						
						iInitialShopsHeldUpCount = iStatHoldUpCount
						
					ENDIF
					
					RESET_NET_TIMER(timeCheckForCompletedHoldups)
					START_NET_TIMER(timeCheckForCompletedHoldups)
				
				ENDIF
			
			ENDIF
		ENDIF
	
	ENDIF
	
	
	
	//-- Check for the hold up still being valid
	IF NOT IS_BIT_SET(holdUpTut.iHoldUpTutClientBitset ,biL_MySToreNotValid)
		
		IF NOT IS_MY_HOLD_UP_STILL_VALID() //IS_HOLD_UP_STILL_VALID(iMyHoldUpLoc)
			#IF IS_DEBUG_BUILD
				IF bDoDebug
					PRINT_HOLD_UP_STRING("[MAINTAIN_HOLD_UP_TUT_MEET_LOC_CHECKS]Hold up not valid, biL_MySToreNotValid NOT set")
				ENDIF
			#ENDIF
			iHoldUpCount = GET_MP_INT_CHARACTER_STAT(MP_STAT_TOTAL_NO_SHOPS_HELD_UP )
			IF iInitialShopsHeldUpCount > -1
			AND iHoldUpCount = iInitialShopsHeldUpCount
				
				IF (NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHT_ReachedMeetLoc)
				AND (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_HOLD_UP")) = 0 
				AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHt_ToldToLoseWanted)
				AND NOT IS_LOCAL_PLAYER_USING_TUTORIAL_HOLD_UP_STORE()))
					//-- Reset the hold up because I'm not there yet, and I'm not near the store with a wanted level
					
					#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING_INT("My hold up is no longer valid, and I'm not at meet loc. Will reset iMyHoldUpLoc = ",iMyHoldUpLoc) #ENDIF
					SET_BIT(holdUpTut.iHoldUpTutClientBitset ,biL_NeedToResetStore)	
					SET_BIT(holdUpTut.iHoldUpTutClientBitset ,biL_MySToreNotValid)
				ELIF holdUpTut.iHoldUpTutProg >= HOLD_UP_TUT_STAGE_GET_TO_HOLD_UP
					//-- Reset if I'm at the meet loc, not inside the store, and my partner didn't do the hold up before I got to the meet
					
					IF NOT IS_LOCAL_PLAYER_IN_HOLD_UP_LOCATION()
					AND NOT IS_BIT_SET(holdUpTut.iHoldUpTutClientBitset ,biL_PartCompleteEarly)
					AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_HOLD_UP")) = 0
						#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING_INT("My hold up is no longer valid, I've reached the meet loc, but not in the store. Will reset iMyHoldUpLoc = ",iMyHoldUpLoc) #ENDIF
						
						SET_BIT(holdUpTut.iHoldUpTutClientBitset ,biL_NeedToResetStore)	
						SET_BIT(holdUpTut.iHoldUpTutClientBitset ,biL_MySToreNotValid)
					ELSE	
						//-- Don't reset 
						#IF IS_DEBUG_BUILD 
							PRINT_HOLD_UP_STRING_INT("My hold up is no longer valid, but NOT Resetting iMyHoldUpLoc = ",iMyHoldUpLoc) 
							PRINT_HOLD_UP_STRING("Not resetting because...")
							IF IS_LOCAL_PLAYER_IN_HOLD_UP_LOCATION()
								PRINT_HOLD_UP_STRING("... I'm in the hold up location")
							ENDIF
							IF IS_BIT_SET(holdUpTut.iHoldUpTutClientBitset ,biL_PartCompleteEarly)
								PRINT_HOLD_UP_STRING("... my partner completed early")
							ENDIF
							
							// 1299310
							IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_HOLD_UP")) > 0
								PRINT_HOLD_UP_STRING("... I'm running hold up script")
							ENDIF
						#ENDIF
						SET_BIT(holdUpTut.iHoldUpTutClientBitset ,biL_MySToreNotValid)
					ENDIF
				ENDIF
			ELSE	
				#IF IS_DEBUG_BUILD 
					PRINT_HOLD_UP_STRING_INT("[MAINTAIN_HOLD_UP_TUT_MEET_LOC_CHECKS] iHoldUpCount <> iInitialShopsHeldUpCount! iInitialShopsHeldUpCount = ", iInitialShopsHeldUpCount)
					PRINT_HOLD_UP_STRING_INT("[MAINTAIN_HOLD_UP_TUT_MEET_LOC_CHECKS] iHoldUpCount <> iInitialShopsHeldUpCount! MP_STAT_TOTAL_NO_SHOPS_HELD_UP = ",  iHoldUpCount) 
				#ENDIF
			ENDIF
		ENDIF	
		
	ELSE		
		// Hold up not valid, biL_MySToreNotValid is set
		#IF IS_DEBUG_BUILD
			IF bDoDebug
				PRINT_HOLD_UP_STRING("[MAINTAIN_HOLD_UP_TUT_MEET_LOC_CHECKS]Hold up not valid, biL_MySToreNotValid is set")
			ENDIF
		#ENDIF
		
		//-- Check for the store becoming valid again
		IF IS_BIT_SET(holdUpTut.iHoldUpTutClientBitset ,biL_SToreReset)
			IF IS_MY_HOLD_UP_STILL_VALID()
				IF IS_BIT_SET(holdUpTut.iHoldUpTutClientBitset ,biL_MySToreNotValid)
					#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Clearing biL_MySToreNotValid as my store is valid again.") #ENDIF
					CLEAR_BIT(holdUpTut.iHoldUpTutClientBitset ,biL_MySToreNotValid)
					CLEAR_BIT(holdUpTut.iHoldUpTutClientBitset ,biL_NeedToResetStore)
					CLEAR_BIT(holdUpTut.iHoldUpTutClientBitset ,biL_SToreReset)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//--If it needs to be reset, don't reset it until I get there
	IF IS_BIT_SET(holdUpTut.iHoldUpTutClientBitset ,biL_NeedToResetStore)		
		IF NOT IS_BIT_SET(holdUpTut.iHoldUpTutClientBitset ,biL_SToreReset)		
			IF holdUpTut.iHoldUpTutProg > HOLD_UP_TUT_STAGE_GET_TO_HOLD_UP	
			//	IF IS_LOCAL_PLAYER_IN_HOLD_UP_LOCATION()
				IF IS_LOCAL_PLAYER_NEAR_HOLD_UP_LOCATION(10.0)
					PLAYER_INDEX playerHost = NETWORK_GET_HOST_OF_SCRIPT(FREEMODE_SCRIPT())
					IF playerHost <> INVALID_PLAYER_INDEX()
						#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING_INT("My hold up is no longer valid,  Resetting iMyHoldUpLoc = ",iMyHoldUpLoc) #ENDIF
						CLEAR_BIT(holdUpTut.iHoldUpTutClientBitset ,biL_NeedToResetStore)
						SET_BIT(holdUpTut.iHoldUpTutClientBitset ,biL_SToreReset)
						RESET_MY_HOLDUP()
					#IF IS_DEBUG_BUILD 
					ELSE
						PRINT_HOLD_UP_STRING_INT("Trying to Reset My Hold Up, but can't get valid Host for Freemode. iMyHoldUpLoc = ",iMyHoldUpLoc)
					#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	//-- Handle friendly fire while in the store
	IF NOT IS_BIT_SET(iBoolsBitSet, biL_FriendlyFireOff)
		IF IS_LOCAL_PLAYER_IN_HOLD_UP_LOCATION()
			NETWORK_SET_FRIENDLY_FIRE_OPTION(FALSE)
			SET_BIT(iBoolsBitSet, biL_FriendlyFireOff)
			#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("NETWORK_SET_FRIENDLY_FIRE_OPTION - Turning OFF friendly fire as in store " ) #ENDIF
			
		ENDIF
	ELSE
		IF NOT IS_LOCAL_PLAYER_IN_HOLD_UP_LOCATION()
			NETWORK_SET_FRIENDLY_FIRE_OPTION(TRUE)
			CLEAR_BIT(iBoolsBitSet, biL_FriendlyFireOff)
			#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("NETWORK_SET_FRIENDLY_FIRE_OPTION - Turning ON friendly fire as not in store ")  #ENDIF
			
		ENDIF
	ENDIF
	
	//-- To control the setting of biHT_InTriggeredHoldUp in am_hold_up.sc
	IF IS_LOCAL_PLAYER_NEAR_HOLD_UP_LOCATION(25.0)
		IF NOT IS_LOCAL_PLAYER_USING_TUTORIAL_HOLD_UP_STORE()
			SET_LOCAL_PLAYER_USING_TUTORIAL_HOLD_UP_STORE(TRUE)
		ENDIF
	ELSE
		IF IS_LOCAL_PLAYER_USING_TUTORIAL_HOLD_UP_STORE()
			SET_LOCAL_PLAYER_USING_TUTORIAL_HOLD_UP_STORE(FALSE)
		ENDIF
	ENDIF
ENDPROC


FUNC BOOL SHOULD_HOLD_UP_TUTORIAL_COMPLETE(FM_PLAYER_HOLD_UP_TUT_STRUCT &holdUpTut)
	INT iPartner = serverBD.holdUpTutServer.iHoldUpPartner[NATIVE_TO_INT(PLAYER_ID())]
	PLAYER_INDEX playerPartner
	
	//-- DOne a hold up?
	IF IS_BIT_SET(GlobalplayerBD_FM[PARTICIPANT_ID_TO_INT()].iHoldUpTutBitset, biHT_ReachedMeetLoc)
		IF GET_MP_INT_CHARACTER_STAT(MP_STAT_TOTAL_NO_SHOPS_HELD_UP ) > iInitialShopsHeldUpCount //0
			#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("[SHOULD_HOLD_UP_TUTORIAL_COMPLETE] True because MP_STAT_TOTAL_NO_SHOPS_HELD_UP > iInitialShopsHeldUpCount") #ENDIF
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHT_DidAHoldUp)
			IF iPartner > -1
				playerPartner = INT_TO_NATIVE(PLAYER_INDEX, iPartner)
				IF NETWORK_IS_PLAYER_ACTIVE(playerPartner)
					IF IS_NET_PLAYER_OK(playerPartner)
						IF IS_BIT_SET(GlobalplayerBD_FM[iPartner].iHoldUpTutBitset, biHT_ReachedMeetLoc)
							fmHoldUpTutStruct.iPartner = iPartner 
							SET_BIT(fmHoldUpTutStruct.iFmHoldUpTutBitset, biHT_CompletedWithPartner)
							
							#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING_WITH_PLAYER_NAME("[SHOULD_HOLD_UP_TUTORIAL_COMPLETE] Setting fmHoldUpTutStruct.iPartner to ", playerPartner) #ENDIF
						ELSE
							#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING_WITH_PLAYER_NAME("[SHOULD_HOLD_UP_TUTORIAL_COMPLETE] (1) I have a partner but they haven't reached the hold up store yet", playerPartner) #ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	

//	IF GET_PACKED_STAT_BOOL(PACKED_MP_STARTED_HOLDUPS)	// added by Kevin Wong to replace money stats, gets set when MP_STAT_TOTAL_NO_SHOPS_HELD_UP increases
//		#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("[SHOULD_HOLD_UP_TUTORIAL_COMPLETE] True because MP_STAT_MONEY_PICKED_FROM_HOLDUPS > 0") #ENDIF
//		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHT_DidAHoldUp)
//		IF iPartner > -1
//			playerPartner = INT_TO_NATIVE(PLAYER_INDEX, iPartner)
//			IF NETWORK_IS_PLAYER_ACTIVE(playerPartner)
//				IF IS_NET_PLAYER_OK(playerPartner)
//					IF IS_BIT_SET(GlobalplayerBD_FM[iPartner].iHoldUpTutBitset, biHT_ReachedMeetLoc)
//						fmHoldUpTutStruct.iPartner = iPartner 
//						SET_BIT(fmHoldUpTutStruct.iFmHoldUpTutBitset, biHT_CompletedWithPartner)
//						
//						#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING_WITH_PLAYER_NAME("[SHOULD_HOLD_UP_TUTORIAL_COMPLETE] Setting fmHoldUpTutStruct.iPartner to ", playerPartner) #ENDIF
//					ELSE
//						#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING_WITH_PLAYER_NAME("[SHOULD_HOLD_UP_TUTORIAL_COMPLETE] (2) I have a partner but they haven't reached the hold up store yet", playerPartner) #ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//		RETURN TRUE
//	ENDIF
	

	
	IF holdUpTut.iHoldUpTutProg > HOLD_UP_TUT_STAGE_GET_TO_HOLD_UP
		IF HAS_MY_PARTNER_COMPLETED_HOLD_UP()
			IF NOT IS_BIT_SET(holdUpTut.iHoldUpTutClientBitset ,biL_PartCompleteEarly) // Did partner complete before I got there?
				#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("[SHOULD_HOLD_UP_TUTORIAL_COMPLETE] True because my partner completed hold up.") #ENDIF
				IF iPartner > -1
					playerPartner = INT_TO_NATIVE(PLAYER_INDEX, iPartner)
					IF NETWORK_IS_PLAYER_ACTIVE(playerPartner)
						IF IS_NET_PLAYER_OK(playerPartner)
							fmHoldUpTutStruct.iPartner = iPartner 
							SET_BIT(fmHoldUpTutStruct.iFmHoldUpTutBitset, biHT_CompletedWithPartner)
							
							#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING_WITH_PLAYER_NAME("[SHOULD_HOLD_UP_TUTORIAL_COMPLETE] Setting fmHoldUpTutStruct.iPartner to ", playerPartner) #ENDIF
						ENDIF
					ENDIF
				ENDIF
				RETURN TRUE
			ENDIF
		ELIF HAS_MY_PARTNER_INVALIDATED_HOLD_UP()
			IF NOT IS_BIT_SET(holdUpTut.iHoldUpTutClientBitset ,biL_PartCompleteEarly) // Did partner complete before I got there?
				#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("[SHOULD_HOLD_UP_TUTORIAL_COMPLETE] True because my partner invalidated hold up.") #ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	
	IF IS_BIT_SET(holdUpTut.iHoldUpTutClientBitset ,biL_MySToreNotValid)
	AND NOT IS_BIT_SET(holdUpTut.iHoldUpTutClientBitset ,biL_SToreReset)
	AND NOT IS_BIT_SET(holdUpTut.iHoldUpTutClientBitset ,biL_NeedToResetStore)
		IF NOT HAS_NET_TIMER_STARTED(holdUpTut.timeInvalidDelay)
			START_NET_TIMER(holdUpTut.timeInvalidDelay)
			#IF IS_DEBUG_BUILD 
				 PRINT_HOLD_UP_STRING("[SHOULD_HOLD_UP_TUTORIAL_COMPLETE] Started timeInvalidDelay")
			#ENDIF
		ELSE
			IF HAS_NET_TIMER_EXPIRED(holdUpTut.timeInvalidDelay, 3000)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHT_InvalidatedHoldup)
				#IF IS_DEBUG_BUILD 
					 PRINT_HOLD_UP_STRING("[SHOULD_HOLD_UP_TUTORIAL_COMPLETE] True because my store not valid")
				#ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD 
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S) 
			 PRINT_HOLD_UP_STRING("[SHOULD_HOLD_UP_TUTORIAL_COMPLETE] True because S-Passed")
			 INT iMyPlayerInt = NATIVE_TO_INT(PLAYER_ID())
			 INT iMyHoldUpLoc = serverBD.holdUpTutServer.iHoldUpToUse[iMyPlayerInt]
			 INT iMyHoldUp = ENUM_TO_INT(GET_HOLD_UP_FROM_MEET_LOC(iMyHoldUpLoc))
			 IF iMyHoldUp = 0 ENDIF
			 BROADCAST_HOLD_UP_FAKE_DONE(iMyHoldUp, FALSE, ALL_PLAYERS())
			 RETURN TRUE
		ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC



#IF IS_DEBUG_BUILD
	PROC DO_HOLD_UP_TUT_J_SKIPS(FM_PLAYER_HOLD_UP_TUT_STRUCT &holdUpTut)
		IF holdUpTut.iHoldUpTutProg > HOLD_UP_TUT_STAGE_INIT
			INT iMyMeetLocLoc 
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
				IF IS_NET_PLAYER_OK(PLAYER_ID())
					PRINT_HOLD_UP_STRING_INT("Player ok for j-skip, hold up stage... ", holdUpTut.iHoldUpTutProg)
					SWITCH holdUpTut.iHoldUpTutProg
						CASE HOLD_UP_TUT_STAGE_GET_TO_HOLD_UP
							iMyMeetLocLoc = serverBD.holdUpTutServer.iHoldUpToUse[NATIVE_TO_INT(PLAYER_ID())]
							IF iMyMeetLocLoc > -1
								J_SKIP_MP(GET_HOLD_UP_MEET_LOCATION(iMyMeetLocLoc), <<2.0, 2.0, 2.0>>)
							ENDIF
							//J_SKIP_MP
						BREAK
					ENDSWITCH
				ENDIF
			ENDIF
		ENDIF
	ENDPROC
#ENDIF

PROC MAINTAIN_HOLD_UP_TUT_HELP_TEXT(FM_PLAYER_HOLD_UP_TUT_STRUCT &holdUpTut)
	INT iStat
	//INT iStatInt
	SWITCH holdUpTut.iHelpTextProg
		CASE 0
			IF IS_BIT_SET(holdUpTut.iHoldUpTutClientBitset, biL_DoHelpText)
				IF NOT IS_BIT_SET(holdUpTut.iHoldUpTutClientBitset, biL_SkipFriendHelpText)
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						IF NOT IS_PAUSE_MENU_ACTIVE()
							IF IS_OK_TO_PRINT_HELP()
								//-- DO interaction menu help here if weare doing tut with another player
								//PRINT_HELP("FM_HTUT_FRN") //You can befriend a Player using the 'Interaction menu'. Target a Player until the menu appears then press ~INPUT_FRONTEND_ACCEPT~. 
							//	iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT3)
							//	SET_BIT(iStatInt, biNmh3_InteractionHelp)
							//	SET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT3, iStatInt)
								holdUpTut.iHelpTextProg++
								#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING(" holdUpTut.iHelpTextProg = 1") #ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					holdUpTut.iHelpTextProg++
					#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING(" holdUpTut.iHelpTextProg = 1, skipped befriend help") #ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			//	iStat = GET_MP_INT_CHARACTER_STAT(MP_STAT_TUTORIAL_BITSET)
			//	IF NOT IS_BIT_SET(iStat, TUTBS_HOLD_UP_HELP_1)
				IF NOT IS_PAUSE_MENU_ACTIVE()
					IF IS_OK_TO_PRINT_HELP()
							PRINT_HELP("FHU_HELP1") //~s~Stores that can be held up are marked by the ~BLIP_CRIM_HOLDUPS~ icon.~n~Enter a store and aim a weapon at the owner to begin a hold up.
						//	
						//	
							holdUpTut.iHelpTextProg++
							#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING_INT("holdUpTut.iHelpTextProg = ", holdUpTut.iHelpTextProg) #ENDIF
							
							iStat = GET_MP_INT_CHARACTER_STAT(MP_STAT_TUTORIAL_BITSET)
							IF NOT IS_BIT_SET(iStat, TUTBS_HOLD_UP_HELP_1)
								SET_BIT(iStat, TUTBS_HOLD_UP_HELP_1)
								SET_MP_INT_CHARACTER_STAT(MP_STAT_TUTORIAL_BITSET, iStat)
								#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING(" Set TUTBS_HOLD_UP_HELP_1") #ENDIF
							ENDIF
							#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING(" holdUpTut.iHelpTextProg = 2") #ENDIF
						ENDIF
					ENDIF
			//	ENDIF
			ELSE	
//				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_HTUT_FRN")
//					IF IS_CUSTOM_MENU_ON_SCREEN()
//						CLEAR_HELP()
//					ENDIF
//				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()

				IF NOT IS_PAUSE_MENU_ACTIVE()
					IF IS_OK_TO_PRINT_HELP()
						IF IS_LOCAL_PLAYER_NEAR_HOLD_UP_LOCATION()
							IF NOT IS_MP_PASSIVE_MODE_ENABLED()
								
								PRINT_HELP("FHU_HELP5") //Threaten the store clerk with a weapon to steal from the cash register.
								holdUpTut.iHelpTextProg++
								#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING_INT("holdUpTut.iHelpTextProg = ", holdUpTut.iHelpTextProg) #ENDIF
							ELSE
								IF NOT IS_BIT_SET(iBoolsBitSet, biL_DonePassiveHelp)
								OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
								//	PRINT_HELP("FM_IHELP_NPA") // The weapon wheel is disabled while Passive Mode is active.
									PRINT_HELP("FM_PASS_DBL") // Disable Passive Mode before robbing the Store.
									SET_BIT(iBoolsBitSet, biL_DonePassiveHelp)
									#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Doing passive mode help") #ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK	
	ENDSWITCH
ENDPROC

PROC MAINTAIN_HOLD_UP_TUT_STORE_BLIP(FM_PLAYER_HOLD_UP_TUT_STRUCT &holdUpTut)
	INT iMyHoldUpLoc
	VECTOR vHoldUpBlipLoc
	IF IS_BIT_SET(holdUpTut.iHoldUpTutClientBitset, biL_DoStoreBlip)
		IF NOT DOES_BLIP_EXIST(holdUpTut.blipMyHoldup)
			IF NOT IS_LOCAL_PLAYER_IN_HOLD_UP_LOCATION()
			//	SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), TRUE)
				iMyHoldUpLoc = serverBD.holdUpTutServer.iHoldUpToUse[NATIVE_TO_INT(PLAYER_ID())]
				IF iMyHoldUpLoc > -1
					vHoldUpBlipLoc = GET_HOLD_UP_BLIP_LOCATION_FROM_MEET_LOC(iMyHoldUpLoc)
					IF NOT ARE_VECTORS_EQUAL(vHoldUpBlipLoc, <<0.0, 0.0, 0.0>>)
						IF NOT DOES_BLIP_EXIST(holduptut.blipMyHoldup)
							SET_IGNORE_NO_GPS_FLAG_UNTIL_FIRST_NORMAL_NODE(TRUE) // 1454736
							holduptut.blipMyHoldup = ADD_BLIP_FOR_COORD(vHoldUpBlipLoc)
							SET_BLIP_SPRITE(holduptut.blipMyHoldup, RADAR_TRACE_CRIM_HOLDUPS)
							SET_BLIP_SCALE(holduptut.blipMyHoldup, BLIP_SIZE_NETWORK_COORD)	
							SET_BLIP_PRIORITY(holduptut.blipMyHoldup, BLIPPRIORITY_HIGH)
							SET_BLIP_ROUTE(holduptut.blipMyHoldup, TRUE)
							#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Added hold up blip") #ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("vHoldUpBlipLoc is zero!") #ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_LOCAL_PLAYER_IN_HOLD_UP_LOCATION()
				IF DOES_BLIP_EXIST(holduptut.blipMyHoldup)
					REMOVE_BLIP(holduptut.blipMyHoldup)
					#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Removed hold up blip") #ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL OK_TO_DO_HOLD_UP_TUTORIAL_UNLOCK_PHONECALL()

	IF IS_CUSTOM_MENU_ON_SCREEN()
		
		RETURN FALSE
	ENDIF
	
	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
		RETURN FALSE
	ENDIF
	
	IF IS_PED_INJURED(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		RETURN FALSE
	ENDIF
	
	IF NOT IS_SCREEN_FADED_IN()
		RETURN FALSE
	ENDIF
	
	IF IS_CELLPHONE_DISABLED()
		RETURN FALSE
	ENDIF
	
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_FM_MISSION_LAUNCH_IN_PROGRESS()
		RETURN FALSE
	ENDIF
	
	IF IS_MISSION_SUMMARY_SCREEN_DISPLAYED()
		RETURN FALSE
	ENDIF
	
	IF IS_PAUSE_MENU_ACTIVE()
		RETURN FALSE
	ENDIF
	
	IF Is_There_A_Recent_Invite()
		RETURN FALSE
	ENDIF
	
	IF GET_IS_LEADERBOARD_CAM_ACTIVE_OR_INTERPING() 
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_CINEMA)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC


FUNC BOOL DO_HOLD_UP_UNLOCK_PHONECALL()
	IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneIntroPhoneCall)
		IF NOT IS_BIT_SET(iBoolsBitSet, BIL_CheckedForNearbyPlayer)
			IF IS_ANY_NEARBY_PLAYER_ABOUT_TO_START_HOLD_UP_TUTORIAL()
				SET_BIT(iBoolsBitSet, biL_DoUnlockTextMessage)
				#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("[DO_HOLD_UP_UNLOCK_PHONECALL] Need to do text message") #ENDIF
			ENDIF
			SET_BIT(iBoolsBitSet, BIL_CheckedForNearbyPlayer)
			#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("[DO_HOLD_UP_UNLOCK_PHONECALL] Checked for nearby players") #ENDIF
		ELSE
			IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoUnlockTextMessage)
			
				IF NOT IS_BIT_SET(iBoolsBitSet, biL_WaitForCallFinishing)
					IF NOT IS_BIT_SET(iBoolsBitSet, biL_PhonecallFailed)
					OR (IS_BIT_SET(iBoolsBitSet, biL_PhonecallFailed) AND HAS_NET_TIMER_EXPIRED(timeBetweenPhoneCallAttempts, 60000))
						IF OK_TO_DO_HOLD_UP_TUTORIAL_UNLOCK_PHONECALL()
							IF NOT IS_BIT_SET(iBoolsBitSet, biL_AddedPedForPhoneCall)
								ADD_CONTACT_TO_PHONEBOOK (CHAR_LAMAR, MULTIPLAYER_BOOK, FALSE)
								ADD_PED_FOR_DIALOGUE(speechHoldUpTut, 1, NULL, "Lamar")
								SET_BIT(iBoolsBitSet, biL_AddedPedForPhoneCall)
								#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("[DO_HOLD_UP_UNLOCK_PHONECALL] Added Lamar") #ENDIF
							ELSE
								INT phonecallModifiers
								SET_BIT(phonecallModifiers, MP_COMMS_MODIFIER_FORCE_PLAYER_TO_ANSWER)
								SET_BIT(phonecallModifiers, MP_COMMS_MODIFIER_PRIORITY_PHONECALL)
								IF Request_MP_Comms_Message(speechHoldUpTut, CHAR_LAMAR, "FM_1AU", "FM_HOLD", phonecallModifiers)
									SET_BIT(iBoolsBitSet, biL_WaitForCallFinishing)
									CLEAR_BIT(iBoolsBitSet, biL_PhonecallFailed) 
									#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("[DO_HOLD_UP_UNLOCK_PHONECALL] Requested phone call") #ENDIF
								ENDIF
							ENDIF
						ELSE
					//		#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("[DO_HOLD_UP_UNLOCK_PHONECALL] OK_TO_DO_HOLD_UP_TUTORIAL_UNLOCK_PHONECALL() is False!") #ENDIF
						ENDIF
					ENDIF
				ELSE	
					IF NOT Is_MP_Comms_Still_Playing()
						IF Did_MP_Comms_Complete_Successfully()
							#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("[DO_HOLD_UP_UNLOCK_PHONECALL] Phonecall completed ok") #ENDIF
							SET_BIT(iBoolsBitSet, biL_DoneIntroPhoneCall)
							SET_LOCAL_PLAYER_ABOUT_TO_START_HOLD_UP_TUTORIAL(FALSE)
						ELSE
							#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("[DO_HOLD_UP_UNLOCK_PHONECALL] Phonecall Failed to complete ok") #ENDIF
							RESET_NET_TIMER(timeBetweenPhoneCallAttempts)
							START_NET_TIMER(timeBetweenPhoneCallAttempts)
							CLEAR_BIT(iBoolsBitSet, biL_WaitForCallFinishing)
							SET_BIT(iBoolsBitSet, biL_PhonecallFailed)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				//-- Need to do text message
				IF OK_TO_DO_HOLD_UP_TUTORIAL_UNLOCK_PHONECALL()

					IF Request_MP_Comms_Txtmsg(CHAR_LAMAR, "CELL_HOLD")
						SET_BIT(iBoolsBitSet, biL_DoneIntroPhoneCall)
						SET_LOCAL_PLAYER_ABOUT_TO_START_HOLD_UP_TUTORIAL(FALSE)
						#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("[DO_HOLD_UP_UNLOCK_PHONECALL] Requested text message") #ENDIF
					ENDIF
					
				ELSE
			//		#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("[DO_HOLD_UP_UNLOCK_PHONECALL] OK_TO_DO_HOLD_UP_TUTORIAL_UNLOCK_PHONECALL() is False!") #ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN (IS_BIT_SET(iBoolsBitSet, biL_DoneIntroPhoneCall))
ENDFUNC


FUNC BOOL DO_HOLD_UP_INVITE(FM_PLAYER_HOLD_UP_TUT_STRUCT& holdUpTut)
	SWITCH holdUpTut.iHoldUpInviteProg
		CASE 0
			IF NOT HAS_NET_TIMER_STARTED(holdUpTut.timeInviteDelay)
				START_NET_TIMER(holdUpTut.timeInviteDelay)
			ELSE	
				IF HAS_NET_TIMER_EXPIRED(holdUpTut.timeInviteDelay, 7000)
					IF IS_SKYSWOOP_AT_GROUND()
						IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
							IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHt_ToldToLoseWanted)
								CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHt_ToldToLoseWanted)
							ENDIF
							IF NOT IS_PHONE_ONSCREEN()
						
	// KEITH 20/6/13: Replaced with a new non-blocking type of Invite
	//							Store_Fake_Joblist_Invite_For_Tutorial(CHAR_LAMAR, FMMC_TYPE_HOLD_UPS, "")
	
								Store_Basic_Invite_Details_For_Joblist_From_NPC(CHAR_LAMAR, FMMC_TYPE_HOLD_UPS, "", FALSE)
							//	PRINT_HELP("FM_IHELP_INV", 10000) // You will be invited onto jobs by other players through your cell phone. You can also invite other player to join you.
								PRINT_HELP("FM_IHELP_QNV") // ~s~Hold ~INPUT_CELLPHONE_UP~ and press ~INPUT_FRONTEND_ACCEPT~ to quickly accept an Invite.
								holdUpTut.iHoldUpInviteProg++
								#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING_INT("[DO_HOLD_UP_INVITE holdUpTut.iHoldUpInviteProg = ", holdUpTut.iHoldUpInviteProg) #ENDIF
							ENDIF
						ELSE
							IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHt_ToldToLoseWanted)
								#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Client set biL_ToldToLoseWanted as I have a wanted level - 1") #ENDIF
								Print_Objective_Text("FM_IHELP_LCP") // lose the cops
								SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHt_ToldToLoseWanted)
								IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_IHELP_INV2")
								OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_IHELP_INV")
									CLEAR_HELP()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
// KEITH 20/6/13: Replaced with a new non-blocking type of Invite
//			IF (Has_Player_Accepted_Fake_Joblist_Invite())
			IF (Has_Player_Accepted_Basic_Invite_From_NPC(CHAR_LAMAR, FMMC_TYPE_HOLD_UPS))
				CLEAR_HELP()
				holdUpTut.iHoldUpInviteProg++
				#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING_INT("[DO_HOLD_UP_INVITE holdUpTut.iHoldUpInviteProg = ", holdUpTut.iHoldUpInviteProg) #ENDIF
			ELSE
				IF IS_SKYSWOOP_AT_GROUND()
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
						IF Is_This_The_Current_Objective_Text("FM_IHELP_LCP")
							Clear_Any_Objective_Text_From_This_Script()
						ENDIF
						IF NOT HAS_NET_TIMER_STARTED(timeAcceptInvite)
						OR HAS_NET_TIMER_EXPIRED(timeAcceptInvite, 20000)
						OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHt_ToldToLoseWanted)
							IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHt_ToldToLoseWanted)
								CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHt_ToldToLoseWanted)
							ENDIF
							
							IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneInviteHelp1)
								IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_IHELP_INV")
									IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_IHELP_QNV")
										IF IS_OK_TO_PRINT_HELP()
											PRINT_HELP("FM_IHELP_INV", 10000) // You will be invited onto jobs by other players through your cell phone. You can also invite other player to join you.
											SET_BIT(iBoolsBitSet, biL_DoneInviteHelp1)
											#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Done Invited onto jobs help - 1") #ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_IHELP_INV")
									IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_IHELP_INV2")
										IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
											IF IS_OK_TO_PRINT_HELP()
												PRINT_HELP("FM_IHELP_INV2") //Go to your phone and use ~INPUT_FRONTEND_ACCEPT~ to accept the Race Invite from Lamar.
												RESET_NET_TIMER(timeAcceptInvite)
												START_NET_TIMER(timeAcceptInvite)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHt_ToldToLoseWanted)
							#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Client set biL_ToldToLoseWanted as I have a wanted level - 2") #ENDIF
							Print_Objective_Text("FM_IHELP_LCP") // lose the cops
							SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHt_ToldToLoseWanted)
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_IHELP_INV2")
							OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_IHELP_INV")
							OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_IHELP_QNV")
								CLEAR_HELP()
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_IHELP_INV2")
					OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_IHELP_INV")
					OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_IHELP_QNV")
						CLEAR_HELP()
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
	
ENDFUNC

PROC DO_SNACKS_HELP_TEXT()
//	IF IS_BIT_SET(holdUpTut.iHoldUpTutClientBitset, biL_DoneAcceptInvHelp) 
//		IF NOT IS_BIT_SET(iBoolsBitSet, biL_QuickAcceptInviteHelp)
//			IF IS_OK_TO_PRINT_FREEMODE_HELP()
//				SET_BIT(iBoolsBitSet, biL_QuickAcceptInviteHelp)
//				#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Set biL_QuickAcceptInviteHelp") #ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
	
	//-- If we didn't get the invite help before accepting the invite, do it here
	IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneInviteHelp1) 
		
			
		IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
			PRINT_HELP("FM_IHELP_INV", 10000) // You will be invited onto jobs by other players through your cell phone. You can also invite other player to join you.
			SET_BIT(iBoolsBitSet, biL_DoneInviteHelp1)
			#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Done Invited onto jobs help - 2") #ENDIF
		ENDIF
		
	ENDIF
			
	IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneSnacksHelp)
		IF IS_BIT_SET(iBoolsBitSet, biL_QuickAcceptInviteHelp)
			IF NOT HAS_NET_TIMER_STARTED(timeSnacksHelp)
				START_NET_TIMER(timeSnacksHelp)
				#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Started timeSnacksHelp") #ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(timeSnacksHelp, 15000)
					IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
					//	#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Printing snacks help....") #ENDIF
					//	PRINT_HELP("FM_IHELP_SNK") // You can replenish your health by purchasing snacks from shops.
						SET_BIT(iBoolsBitSet, biL_DoneSnacksHelp)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DO_OTHER_PLAYER_BLIP_HELP_TEXT()
	INT i
	PLAYER_INDEX PlayerID
	IF NOT IS_BIT_SET(iBoolsBitSet, biL_OtherPlayerBlipHelp)
		IF NOT IS_BIT_SET(iBoolsBitSet, biL_FoundPlayerBlip)
			FOR i = 0 TO NUM_NETWORK_PLAYERS - 1
				PlayerID = INT_TO_PLAYERINDEX(i)
				IF IS_NET_PLAYER_OK(PlayerId)
					IF PlayerId <> PLAYER_ID() 
						IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerId)
							IF IS_NET_PLAYER_BLIPPED(PlayerId)
								SET_BIT(iBoolsBitSet, biL_FoundPlayerBlip)
								#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Found other player blip") #ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ELSE
			IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
				#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Printing other player blip help....") #ENDIF
				PRINT_HELP("FM_IHELP_BLP") // Other players are shown on the radar by ~BLIP_LEVEL~. If they are your friend it will have a blue outline.
				SET_BIT(iBoolsBitSet, biL_OtherPlayerBlipHelp)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_IN_SAME_CAR_AS_PARTNER()
	INT iMyPlayerInt = NATIVE_TO_INT(PLAYER_ID())
	INT iMyPartner = serverBD.holdUpTutServer.iHoldUpPartner[iMyPlayerInt]
	VEHICLE_INDEX veh
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
			veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF iMyPartner > -1
			AND IS_MY_PARTNER_STILL_VALID(iMyPartner)	
				PLAYER_INDEX playerPartner = INT_TO_NATIVE(PLAYER_INDEX, iMyPartner)
				PED_INDEX pedPartner
				IF NETWORK_IS_PLAYER_ACTIVE(playerPartner)
					IF IS_NET_PLAYER_OK(playerPartner, FALSE)
						pedPartner = GET_PLAYER_PED(playerPartner)
						IF NOT IS_PED_INJURED(pedPartner)
							IF IS_PED_SITTING_IN_ANY_VEHICLE(pedPartner)
								IF GET_VEHICLE_PED_IS_IN(pedPartner) = veh
									#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("[IS_PLAYER_IN_SAME_CAR_AS_PARTNER] True") #ENDIF
									RETURN TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("[IS_PLAYER_IN_SAME_CAR_AS_PARTNER] FALSE") #ENDIF
	RETURN FALSE
ENDFUNC
FUNC BOOL DO_HOLD_UP_TUT_CLIENT_V2(FM_PLAYER_HOLD_UP_TUT_STRUCT &holdUpTut)
	/*
		From Ryan:
		
		x:\gta5\script\dev\multiplayer\scripts\modes\CnC\Ambient\AM_HOLD_UP.sc
		
		Check hold up is active
		AND IS_BIT_SET(HoldUpServerData.iHoldUpTimerDoneBitSet, HoldUpData.iHoldUpLoop) 
		        AND NOT IS_BIT_SET(HoldUpServerData.iHoldUpDoneBitSet, HoldUpData.iHoldUpLoop)
				
		Hold up loc:
		in net_hold_ups.sch
		PROC HOLD_UP_DETAILS(

	*/
	
	INT iMyPartner
	INT iMyHoldUpLoc
	INT iMyHoldUp
	INT iMyPlayerInt
	PLAYER_INDEX playerPartner
	GAMER_HANDLE handlePlayer
//	VECTOR vHoldUpBlipLoc
	
	
	IF MPGlobalsAmbience.bSomeoneWantsToStartHoldUpTut
		IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
			MPGlobalsAmbience.bSomeoneWantsToStartHoldUpTut = FALSE
		//	#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Clearing bSomeoneWantsToStartHoldUpTut as I'm not host") #ENDIF
		ENDIF
	ENDIF
	
	IF holdUpTut.iHoldUpTutProg > HOLD_UP_TUT_STAGE_INIT
	
		
		SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
		
		IF NOT IS_BIT_SET(holdUpTut.iHoldUpTutClientBitset, biL_HoldUp_TextWasCleared)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				//IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
				IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
				OR IS_FM_MISSION_LAUNCH_IN_PROGRESS()
				OR GET_LOCAL_PLAYER_CORONA_POS_STATE() <> 0
					#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Clearing objective text as player in corona / on mission") #ENDIF
					CLEAR_HOLD_UP_TUT_OBJECTIVE_TEXT(holdUpTut)
					SET_BIT(holdUpTut.iHoldUpTutClientBitset, biL_HoldUp_TextWasCleared)
					CLEAR_BIT(holdUpTut.iHoldUpTutClientBitset, biL_DoKickTimer)
					CLEAR_BIT(iBoolsBitSet, biL_ReduceTimeoutAsPartnerAtStore)
				ENDIF
			ENDIF
		ELSE	
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
					IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
						IF NOT IS_FM_MISSION_LAUNCH_IN_PROGRESS()
							IF GET_LOCAL_PLAYER_CORONA_POS_STATE() = 0
								CLEAR_BIT(holdUpTut.iHoldUpTutClientBitset, biL_HoldUp_TextWasCleared)
								CLEAR_BIT(holdUpTut.iHoldUpTutClientBitset, biL_HoldUp_MeetSolo)
								CLEAR_BIT(holdUpTut.iHoldUpTutClientBitset, biL_HoldUp_GetToMeet)
								CLEAR_BIT(holdUpTut.iHoldUpTutClientBitset, biL_ToldToHoldUpStore)
								CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHT_ReachedMeetLoc)
								
								IF holdUpTut.iHoldUpTutProg > HOLD_UP_TUT_STAGE_GET_TO_HOLD_UP
									holdUpTut.iHoldUpTutProg = HOLD_UP_TUT_STAGE_GET_TO_HOLD_UP
									#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Resetting iHoldUpTutProg to HOLD_UP_TUT_STAGE_GET_TO_HOLD_UP as control is back on")#ENDIF
								ENDIF
								#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Clearing biL_HoldUp_TextWasCleared as IS_PLAYER_CONTROL_ON is True") #ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		iMyPlayerInt = NATIVE_TO_INT(PLAYER_ID())
		
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[iMyPlayerInt].iHoldUpTutBitset, biHT_OnMission)
			IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
			OR IS_FM_MISSION_LAUNCH_IN_PROGRESS()
				SET_BIT(GlobalplayerBD_FM[iMyPlayerInt].iHoldUpTutBitset, biHT_OnMission)	
				holdUpTut.iHoldUpTutProg = HOLD_UP_TUT_STAGE_INIT
				SET_LOCAL_PLAYER_READY_FOR_HOLD_UP_TUT(FALSE)
				SET_LOCAL_PLAYER_DOING_HOLD_UP_TUT(FALSE)
				SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_HOLD_UP_TUT, FALSE)
				CLEAR_BIT(holdUpTut.iHoldUpTutClientBitset, biL_HoldUp_GetToMeet)
				CLEAR_HOLD_UP_TUT_OBJECTIVE_TEXT(holdUpTut)
				#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Setting biHT_OnMission as I'm on a mission ") #ENDIF
			ENDIF	
		ENDIF
		
		IF holdUpTut.iHoldUpTutProg < HOLD_UP_TUT_STAGE_DO_HOLD_UP
			//-- Keep broadcasting if I don't have a partner
			IF serverBD.holdUpTutServer.iHoldUpPartner[NATIVE_TO_INT(PLAYER_ID())] = -1
				IF HAS_NET_TIMER_EXPIRED(HoldUpTut.timeBroadcastReady, 15000)
					#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Broadcasting ready for tut as I don't have a partner") #ENDIF
					BROADCAST_PLAYER_READY_FOR_HOLDUP_TUTORIAL()	
					RESET_NET_TIMER(HoldUpTut.timeBroadcastReady)
					START_NET_TIMER(HoldUpTut.timeBroadcastReady)
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHT_NeedNewHoldUpLoc)
			IF HAS_NET_TIMER_EXPIRED(HoldUpTut.timeBroadcastReady, 5000)
				#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Broadcasting ready for tut as I need a new loc") #ENDIF
				BROADCAST_PLAYER_READY_FOR_HOLDUP_TUTORIAL()	
				RESET_NET_TIMER(HoldUpTut.timeBroadcastReady)
				START_NET_TIMER(HoldUpTut.timeBroadcastReady)
			ENDIF
		ENDIF
	ENDIF
	
	
	INT iStat
	
	SWITCH holdUpTut.iHoldUpTutProg 
		CASE HOLD_UP_TUT_STAGE_INIT
			iMyPlayerInt = NATIVE_TO_INT(PLAYER_ID())
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[iMyPlayerInt].iHoldUpTutBitset, biHT_WaitForResetAfterKick)
				//-- Initial bit gets set when Hold up help is done
				IF IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet, BI_FM_NMH_HOLDUP)
				OR IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet3, BI_FM_NMH3_LAUNCH_HOLD_UP)
					IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHT_ReadyForHoldUpTut)
						IF NOT Is_MP_Comms_Still_Playing()
						//	IF Did_MP_Comms_Complete_Successfully()
								IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
									IF NOT IS_FM_MISSION_LAUNCH_IN_PROGRESS()
										IF GET_LOCAL_PLAYER_CORONA_POS_STATE() = 0
											IF DO_HOLD_UP_UNLOCK_PHONECALL()
												IF DO_HOLD_UP_INVITE(holdUpTut)
													fmHoldUpTutStruct.iPartner = -1
													SET_LOCAL_PLAYER_READY_FOR_HOLD_UP_TUT(TRUE)
													#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Client doing initial broadcast") #ENDIF
													BROADCAST_PLAYER_READY_FOR_HOLDUP_TUTORIAL()
													IF IS_BIT_SET(GlobalplayerBD_FM[iMyPlayerInt].iHoldUpTutBitset, biHT_OnMission)
														CLEAR_BIT(GlobalplayerBD_FM[iMyPlayerInt].iHoldUpTutBitset, biHT_OnMission)
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
						//	ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(GlobalplayerBD_FM[iMyPlayerInt].iHoldUpTutBitset, biHT_ReadyForHoldUpTut)
					//IF DO_HOLD_UP_UNLOCK_PHONECALL()
						//IF NOT Is_MP_Comms_Still_Playing()
							//IF DO_HOLD_UP_INVITE(holdUpTut)
								IF NOT IS_BIT_SET(holdUpTut.iHoldUpTutClientBitset, biL_DoneAcceptInvHelp) 
									IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
										PRINT_HELP("FM_IHELP_ACPI")// You've accepted a Job for Lamar.
										SET_BIT(holdUpTut.iHoldUpTutClientBitset, biL_DoneAcceptInvHelp)
										#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Set biL_DoneAcceptInvHelp") #ENDIF
									ENDIF
								ENDIF
								
								IF IS_BIT_SET(fmHoldUpTutStruct.iFmHoldUpTutBitset, biHT_StartHoldUpTut)
									iMyPlayerInt = NATIVE_TO_INT(PLAYER_ID())
									iMyPartner = serverBD.holdUpTutServer.iHoldUpPartner[iMyPlayerInt]
									
									
									iMyHoldUpLoc = serverBD.holdUpTutServer.iHoldUpToUse[iMyPlayerInt]
									IF iMyHoldUpLoc > -1
									//	iLocalMeetLoc = iMyHoldUpLoc
										#IF IS_DEBUG_BUILD
											PRINT_HOLD_UP_STRING("")
											PRINT_HOLD_UP_STRING("************************** GO! *****************************")
											PRINT_HOLD_UP_STRING("")
										#ENDIF
										
										iMyHoldUp = ENUM_TO_INT(GET_HOLD_UP_FROM_MEET_LOC(iMyHoldUpLoc))
										IF iMyHoldUp = 0 ENDIF
										BROADCAST_HOLD_UP_FAKE_DONE(iMyHoldUp, TRUE, ALL_PLAYERS())
										#IF IS_DEBUG_BUILD
											PRINT_HOLD_UP_STRING_INT("Calling BROADCAST_HOLD_UP_FAKE_DONE(iMyHoldUp, TRUE) with iMyHoldUp = ", iMyHoldUp)
										#ENDIF
										RESET_NET_TIMER(HoldUpTut.timeBroadcastReady)
										START_NET_TIMER(HoldUpTut.timeBroadcastReady)
										SET_LOCAL_PLAYER_DOING_HOLD_UP_TUT(TRUE)
								//		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_HOLD_UP_TUT
										SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_HOLD_UP_TUT, TRUE)
										
										
										
										iStat = GET_MP_INT_CHARACTER_STAT(MP_STAT_TUTORIAL_BITSET)
										CLEAR_BIT(iStat, TUTBS_HOLD_UP_HELP_1)
										CLEAR_BIT(iStat, TUTBS_HOLD_UP_HELP_2)
										CLEAR_BIT(iStat, TUTBS_HOLD_UP_HELP_3)
										CLEAR_BIT(iStat, TUTBS_HOLD_UP_HELP_5)
										CLEAR_BIT(iStat, TUTBS_HOLD_UP_HELP_6)
										holdUpTut.iHoldUpTutProg = HOLD_UP_TUT_STAGE_GET_TO_HOLD_UP
										SET_MP_INT_CHARACTER_STAT(MP_STAT_TUTORIAL_BITSET, iStat)
										
										iInitialShopsHeldUpCount = GET_MP_INT_CHARACTER_STAT(MP_STAT_TOTAL_NO_SHOPS_HELD_UP )
										#IF IS_DEBUG_BUILD
											
											PRINT_HOLD_UP_STRING_INT("iInitialShopsHeldUpCount = ", iInitialShopsHeldUpCount)
											PRINT_HOLD_UP_STRING("Client reset MP_STAT_TUTORIAL_BITSET hold up help bitset ")
											PRINT_HOLD_UP_STRING("Client moving to stage HOLD_UP_TUT_STAGE_GET_TO_HOLD_UP")
											IF iMyPartner > -1
												PRINT_HOLD_UP_STRING_WITH_PLAYER_NAME("My Partner is... ", INT_TO_NATIVE(PLAYER_INDEX, iMyPartner))
											ELSE
												PRINT_HOLD_UP_STRING("I don't have a partner")
											ENDIF
											PRINT_HOLD_UP_STRING_INT("My Hold up loc is ", iMyHoldUpLoc)
											PRINT_HOLD_UP_STRING_VECTOR("Which is at coords ", GET_HOLD_UP_MEET_LOCATION(iMyHoldUpLoc))
										#ENDIF
									ELSE
										IF NOT HAS_NET_TIMER_STARTED(HoldUpTut.timeBroadcastReady)						
										OR HAS_NET_TIMER_EXPIRED(HoldUpTut.timeBroadcastReady, 5000)
											#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Client about to re-broadcast ready for hold up tut as I don't have a loc") #ENDIF
											BROADCAST_PLAYER_READY_FOR_HOLDUP_TUTORIAL()	
											RESET_NET_TIMER(HoldUpTut.timeBroadcastReady)
											START_NET_TIMER(HoldUpTut.timeBroadcastReady)
										ENDIF
									ENDIF
									
								ELSE
									IF NOT HAS_NET_TIMER_STARTED(HoldUpTut.timeBroadcastReady)						
									OR HAS_NET_TIMER_EXPIRED(HoldUpTut.timeBroadcastReady, 30000)
										#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Client about to broadcast ready for hold up tut") #ENDIF
										BROADCAST_PLAYER_READY_FOR_HOLDUP_TUTORIAL()	
										RESET_NET_TIMER(HoldUpTut.timeBroadcastReady)
										START_NET_TIMER(HoldUpTut.timeBroadcastReady)
									ENDIF
								ENDIF
						//	ENDIF
					//	ELSE	
					//		#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Waiting for Is_MP_Comms_Still_Playing") #ENDIF
					//	ENDIF
					//ENDIF
				ENDIF
			ELSE
				//-- I was on the tutorial, but I took too long to get to the meet loc
				IF IS_BIT_SET(GlobalplayerBD_FM[iMyPlayerInt].iHoldUpTutBitset, biHT_ReadyForHoldUpTut)
					#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Setting no longer ready for hold up as I took too long to get tto the meet") #ENDIF
					CLEAR_BIT(GlobalplayerBD_FM[iMyPlayerInt].iHoldUpTutBitset, biHT_ReadyForHoldUpTut)
					CLEAR_BIT(holdUpTut.iHoldUpTutClientBitset, biL_HoldUp_GetToMeet)
					CLEAR_BIT(GlobalplayerBD_FM[iMyPlayerInt].iHoldUpTutBitset, biHT_ReachedMeetLoc)
					CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHt_ToldToLoseWanted)
					SET_LOCAL_PLAYER_READY_FOR_HOLD_UP_TUT(FALSE)
				ENDIF 
				
				
				IF IS_BIT_SET(GlobalplayerBD_FM[iMyPlayerInt].iHoldUpTutBitset, biHT_TookTooLongToGetToHoldUpLoc)
					//-- Wait for server to reset me
					IF serverBD.holdUpTutServer.iHoldUpToUse[iMyPlayerInt] = -1
					AND serverBD.holdUpTutServer.iHoldUpPartner[iMyPlayerInt] = -1
						CLEAR_BIT(GlobalplayerBD_FM[iMyPlayerInt].iHoldUpTutBitset, biHT_TookTooLongToGetToHoldUpLoc)
						#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Clearing biHT_TookTooLongToGetToHoldUpLoc as server reset me") #ENDIF
					ENDIF
				ELSE
					//-- Delay before rejoining
					IF NOT HAS_NET_TIMER_STARTED(HoldUpTut.timeRetryAfterKickForTimeout)
						START_NET_TIMER(HoldUpTut.timeRetryAfterKickForTimeout)
					ELSE
						IF HAS_NET_TIMER_EXPIRED(HoldUpTut.timeRetryAfterKickForTimeout, RETRY_AFTER_TIMEOUT_KICK)
							CLEAR_BIT(GlobalplayerBD_FM[iMyPlayerInt].iHoldUpTutBitset, biHT_WaitForResetAfterKick)
							RESET_NET_TIMER(HoldUpTut.timeBroadcastReady)
							#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Clearing biHT_WaitForResetAfterKick as RETRY_AFTER_TIMEOUT_KICK expired") #ENDIF
						ENDIF
					ENDIF
				ENDIF			
			ENDIF
		BREAK
		
		CASE HOLD_UP_TUT_STAGE_GET_TO_HOLD_UP
			iMyPlayerInt = NATIVE_TO_INT(PLAYER_ID())
			iMyHoldUpLoc = serverBD.holdUpTutServer.iHoldUpToUse[iMyPlayerInt]
			iMyPartner = serverBD.holdUpTutServer.iHoldUpPartner[iMyPlayerInt]
			
			
			

			
			DO_SNACKS_HELP_TEXT()
			
			IF NOT IS_BIT_SET(holdUpTut.iHoldUpTutClientBitset, biL_HoldUp_GetToMeet)
				
				IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHt_ToldToLoseWanted)
					CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHt_ToldToLoseWanted)
				ENDIF
				IF IS_BIT_SET(holdUpTut.iHoldUpTutClientBitset, biL_ToldToHelpPartnerLose) 
					CLEAR_BIT(holdUpTut.iHoldUpTutClientBitset, biL_ToldToHelpPartnerLose)
				ENDIF
				IF NOT DOES_BLIP_EXIST(holdUpTut.blipHoldUpTutMeetLoc)
					IF iMyHoldUpLoc >= 0
						holdUpTut.blipHoldUpTutMeetLoc = ADD_BLIP_FOR_COORD(GET_HOLD_UP_MEET_LOCATION(iMyHoldUpLoc))
						SET_BLIP_ROUTE(holdUpTut.blipHoldUpTutMeetLoc, TRUE)
						SET_BLIP_PRIORITY(holduptut.blipHoldUpTutMeetLoc, BLIPPRIORITY_HIGHEST)
						#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING_INT("Added blip for hold up loc ", iMyHoldUpLoc) #ENDIF
					ENDIF
					
				
				ENDIF
				IF iMyPartner > -1
				AND IS_MY_PARTNER_STILL_VALID(iMyPartner)					
					playerPartner = INT_TO_NATIVE(PLAYER_INDEX, iMyPartner)
					IF NETWORK_IS_PLAYER_ACTIVE(playerPartner)
						IF IS_NET_PLAYER_OK(playerPartner, FALSE)
							IF NOT IS_BIT_SET(GlobalplayerBD_FM[iMyPartner].iHoldUpTutBitset, biHT_CompletedHoldUpTut)
							AND NOT IS_BIT_SET(GlobalplayerBD_FM[iMyPartner].iHoldUpTutBitset, biHT_OnMission)
						//		Print_Objective_Text_With_Player_Name("FM_HTUT_GOP", playerPartner)
								Print_Objective_Text("FM_HTUT_GO")
								SET_BIT(holdUpTut.iHoldUpTutClientBitset, biL_HoldUp_GetToMeet)
								#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Done go to objective text with partner") #ENDIF
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Partner player not OK!") #ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Partner player not active!") #ENDIF
					ENDIF
					
				ENDIF
				IF NOT IS_BIT_SET(holdUpTut.iHoldUpTutClientBitset, biL_HoldUp_GetToMeet)
					Print_Objective_Text("FM_HTUT_GO")
					SET_BIT(holdUpTut.iHoldUpTutClientBitset, biL_HoldUp_MeetSolo)
					#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Done go to objective text without partner") #ENDIF
				ELSE	
					RESET_NET_TIMER(holdUpTut.timeGetToMeetLoc)
					START_NET_TIMER(holdUpTut.timeGetToMeetLoc) // Have a max time to get to the meet loc, if I'm meeting someone
					#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Started get to meet timeout timer") #ENDIF
				ENDIF
				
				SET_BIT(holdUpTut.iHoldUpTutClientBitset, biL_HoldUp_GetToMeet)
				
			ELSE
				//-- Told to get to meet
				IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHt_ToldToLoseWanted)
					CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHt_ToldToLoseWanted)
				ENDIF
				IF IS_BIT_SET(holdUpTut.iHoldUpTutClientBitset, biL_ToldToHelpPartnerLose) 
					CLEAR_BIT(holdUpTut.iHoldUpTutClientBitset, biL_ToldToHelpPartnerLose)
				ENDIF
				IF NOT IS_BIT_SET(GlobalplayerBD_FM[iMyPlayerInt].iHoldUpTutBitset, biHT_ReachedMeetLoc)	
				//	IF NOT IS_BIT_SET(fmHoldUpTutStruct.iFmHoldUpTutBitset, biHT_InTriggeredHoldUp)
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_HOLD_UP_MEET_LOCATION(iMyHoldUpLoc)) < (15.0 * 15.0)
							OR (iMyPartner = -1 AND IS_LOCAL_PLAYER_NEAR_HOLD_UP_LOCATION(25.0))
							OR IS_LOCAL_PLAYER_APPROACHING_HOLD_UP_LOCATION_AT_SPEED()
								IF DOES_BLIP_EXIST(holdUpTut.blipHoldUpTutMeetLoc)
									REMOVE_BLIP(holdUpTut.blipHoldUpTutMeetLoc)
								ENDIF
								IF iMyPartner > -1
								AND IS_MY_PARTNER_STILL_VALID(iMyPartner)
									playerPartner = INT_TO_NATIVE(PLAYER_INDEX, iMyPartner)
									IF NOT IS_BIT_SET(GlobalplayerBD_FM[iMyPartner].iHoldUpTutBitset, biHT_ReachedMeetLoc)
										IF NETWORK_IS_PLAYER_ACTIVE(playerPartner)
											IF IS_NET_PLAYER_OK(playerPartner)
												IF NOT IS_PLAYER_IN_SAME_CAR_AS_PARTNER()
													handlePlayer = GET_GAMER_HANDLE_PLAYER(playerPartner)
													IF NETWORK_IS_FRIEND(handlePlayer)
														Print_Objective_Text_With_Player_Name("FM_HTUT_FWAT", playerPartner, FALSE)
													 	#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Doing hold up with friend") #ENDIF
													ELSE
														Print_Objective_Text_With_Player_Name("FM_HTUT_WAT", playerPartner, FALSE) // Wait for
														#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Doing hold up NOT with friend") #ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								SET_BIT(GlobalplayerBD_FM[iMyPlayerInt].iHoldUpTutBitset, biHT_ReachedMeetLoc)
								
								#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Setting biHT_ReachedMeetLoc as I'm at meet") #ENDIF
								
								
							ENDIF
						ENDIF
				//	ENDIF
				ELSE	
					//-- Already reached meet loc
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF NOT IS_BIT_SET(holdUpTut.iHoldUpTutClientBitset, biL_HoldUp_TextWasCleared) // Don't reset if the text has been cleared (player control off etc)
							IF (NOT IS_BIT_SET(iBoolsBitSet, biL_PlayerApproachedAtSpeed)
								AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_HOLD_UP_MEET_LOCATION(iMyHoldUpLoc)) > (35.0 * 35.0))
							OR (IS_BIT_SET(iBoolsBitSet, biL_PlayerApproachedAtSpeed)
								AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_HOLD_UP_MEET_LOCATION(iMyHoldUpLoc)) > (90.0 * 90.0))
								
								CLEAR_BIT(holdUpTut.iHoldUpTutClientBitset, biL_HoldUp_GetToMeet)
								CLEAR_BIT(GlobalplayerBD_FM[iMyPlayerInt].iHoldUpTutBitset, biHT_ReachedMeetLoc)
								CLEAR_BIT(iBoolsBitSet, biL_PlayerApproachedAtSpeed)
								#IF IS_DEBUG_BUILD 
								PRINT_HOLD_UP_STRING("Client resetting biL_HoldUp_GetToMeet, biL_PlayerApproachedAtSpeed as I'm too far from meet") #ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_BIT_SET(fmHoldUpTutStruct.iFmHoldUpTutBitset, biHT_InTriggeredHoldUp)
						IF Is_This_The_Current_Objective_Text("FM_HTUT_WAT")
						OR Is_This_The_Current_Objective_Text("FM_HTUT_FWAT")
							Clear_Any_Objective_Text_From_This_Script()
							#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Clearing wait for partner objective text as biHT_InTriggeredHoldUp") #ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF

			
			IF iMyPartner > -1
			AND IS_MY_PARTNER_STILL_VALID(iMyPartner)
				IF IS_BIT_SET(GlobalplayerBD_FM[iMyPartner].iHoldUpTutBitset, biHT_ReachedMeetLoc)
					IF NOT IS_BIT_SET(GlobalplayerBD_FM[iMyPlayerInt].iHoldUpTutBitset, biHT_ReachedMeetLoc)
						//-- Check for taking too long to get to hold up loc
						IF NOT IS_BIT_SET(GlobalplayerBD_FM[iMyPlayerInt].iHoldUpTutBitset, biHT_TookTooLongToGetToHoldUpLoc)
							IF HAS_NET_TIMER_STARTED(holdUpTut.timeGetToMeetLoc)
								IF HAS_NET_TIMER_EXPIRED(holdUpTut.timeGetToMeetLoc, MAX_TIME_GET_TO_HOLD_UP)
									SET_BIT(GlobalplayerBD_FM[iMyPlayerInt].iHoldUpTutBitset, biHT_TookTooLongToGetToHoldUpLoc)
									SET_BIT(GlobalplayerBD_FM[iMyPlayerInt].iHoldUpTutBitset, biHT_WaitForResetAfterKick)
									IF DOES_BLIP_EXIST(holdUpTut.blipHoldUpTutMeetLoc)
										REMOVE_BLIP(holdUpTut.blipHoldUpTutMeetLoc)
									ENDIF
									Clear_Any_Objective_Text_From_This_Script()
									holdUpTut.iHoldUpTutProg = HOLD_UP_TUT_STAGE_INIT
									CLEAR_BIT(holdUpTut.iHoldUpTutClientBitset, biL_DoKickTimer)
									CLEAR_BIT(iBoolsBitSet, biL_ReduceTimeoutAsPartnerAtStore)
									#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("I took too long to get to hold up! holdUpTut.iHoldUpTutProg = HOLD_UP_TUT_STAGE_INIT") #ENDIF
								ELSE
									IF NOT IS_BIT_SET(holdUpTut.iHoldUpTutClientBitset, biL_HoldUp_TextWasCleared)
										IF NOT IS_BIT_SET(holdUpTut.iHoldUpTutClientBitset, biL_DoKickTimer)
											IF HAS_NET_TIMER_EXPIRED(holdUpTut.timeGetToMeetLoc, (MAX_TIME_GET_TO_HOLD_UP - 60000))
												SET_BIT(holdUpTut.iHoldUpTutClientBitset, biL_DoKickTimer)
												#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Setting biL_DoKickTimer as 60 seconds until kick") #ENDIF
											ELSE 
												// Kick time will be less if partner is already at the store
												IF NOT IS_BIT_SET(iBoolsBitSet, biL_ReduceTimeoutAsPartnerAtStore)
													IF IS_BIT_SET(GlobalplayerBD_FM[iMyPartner].iHoldUpTutBitset, biHT_ReachedMeetLoc)
														IF NOT HAS_NET_TIMER_EXPIRED(holdUpTut.timeGetToMeetLoc, (MAX_TIME_GET_TO_HOLD_UP - (7*60000)))
															RESET_NET_TIMER(holdUpTut.timeGetToMeetLoc)
															START_NET_TIMER(holdUpTut.timeGetToMeetLoc)
															SET_BIT(iBoolsBitSet, biL_ReduceTimeoutAsPartnerAtStore)
															MAX_TIME_GET_TO_HOLD_UP = 180000 
															
															#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Setting MAX_TIME_GET_TO_HOLD_UP to 3 minutes as my partner at hold up") #ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										
										ENDIF
										
									ENDIF
									
									
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_BIT_SET(GlobalplayerBD_FM[iMyPlayerInt].iHoldUpTutBitset, biHT_TookTooLongToGetToHoldUpLoc)
					IF IS_BIT_SET(GlobalplayerBD_FM[iMyPlayerInt].iHoldUpTutBitset, biHT_ReachedMeetLoc)
					AND IS_BIT_SET(GlobalplayerBD_FM[iMyPartner].iHoldUpTutBitset, biHT_ReachedMeetLoc)
					
						CLEAR_BIT(holdUpTut.iHoldUpTutClientBitset, biL_DoKickTimer)
						CLEAR_BIT(iBoolsBitSet, biL_ReduceTimeoutAsPartnerAtStore)
						SET_BIT(holdUpTut.iHoldUpTutClientBitset, biL_DoHelpText)
						
						
										
						holdUpTut.iHoldUpTutProg = HOLD_UP_TUT_STAGE_DO_HOLD_UP
						CLEAR_HOLD_UP_TUT_OBJECTIVE_TEXT(holdUpTut)
						START_NET_TIMER(timeWaitForHelp)
						SET_BIT(holdUpTut.iHoldUpTutClientBitset, biL_DoStoreBlip)
						SET_LOCAL_PLAYER_READY_TO_DO_TUT_HOLD_UP(TRUE)
					//	SET_FM_UNLOCKS_BIT_SET() // Unlock Holdups
						START_NET_TIMER(timeWaitForHelp)
						
						IF IS_MY_PARTNER_STILL_VALID(iMyPartner)
							IF NOT IS_BIT_SET(holdUpTut.iHoldUpTutClientBitset, biL_TargetBuddyDisabled)
								IF SET_MY_HOLD_UP_TUT_PARTNER_CAN_TARGET_ME(iMyPartner, FALSE)
									SET_BIT(holdUpTut.iHoldUpTutClientBitset, biL_TargetBuddyDisabled)
								ENDIF
							ENDIF
//							playerPartner = INT_TO_NATIVE(PLAYER_INDEX, iMyPartner)
//							IF NOT IS_BIT_SET(holdUpTut.iHoldUpTutClientBitset, biL_TargetBuddyDisabled)
//								IF NETWORK_IS_PLAYER_ACTIVE(playerPartner)
//									IF IS_NET_PLAYER_OK(playerPartner)
//										IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//											SET_PED_CAN_BE_TARGETTED_BY_PLAYER(PLAYER_PED_ID(), playerPartner , FALSE)
//											SET_BIT(holdUpTut.iHoldUpTutClientBitset, biL_TargetBuddyDisabled)
//											#IF IS_DEBUG_BUILD NET_DW_PRINT("I've been set to not target my partner") #ENDIF
//										ELSE	
//											#IF IS_DEBUG_BUILD NET_DW_PRINT("Want to not target my partner, but they're injured!") #ENDIF
//										ENDIF
//									ELSE
//										#IF IS_DEBUG_BUILD NET_DW_PRINT("Want to not target my partner, but they're not ok") #ENDIF
//									ENDIF
//								ELSE	
//									#IF IS_DEBUG_BUILD NET_DW_PRINT("Want to not target my partner, but they're not active!") #ENDIF
//								ENDIF
//							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Want to not target my partner, but they're not valid!") #ENDIF
						ENDIF
						//-- Set hold up blip to flash
						//Crim_HOLD_UP_POINT myHoldUp
					
						
						#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Client moving to stage HOLD_UP_TUT_FRIEND_HELP as I'm at meet with partner") #ENDIF
					ELSE
						//-- Either myself or my partner hasn't reached meet loc.
						
						#IF IS_DEBUG_BUILD
							IF IS_BIT_SET(GlobalplayerBD_FM[iMyPlayerInt].iHoldUpTutBitset, biHT_ReachedMeetLoc)
								IF NOT HAS_NET_TIMER_STARTED(timeDebug)
									START_NET_TIMER(timeDebug)
									PRINT_HOLD_UP_STRING("Started timeDebug")
								ELSE
									IF HAS_NET_TIMER_EXPIRED(timeDebug, 6000)
										PRINT_HOLD_UP_STRING("[timeDebug] I'm still waiting for my partner....")
										PRINT_HOLD_UP_STRING_INT("[timeDebug] serverBD.holdUpTutServer.iHoldUpPartner[iMyPlayerInt] = ", serverBD.holdUpTutServer.iHoldUpPartner[iMyPlayerInt])
										IF IS_MY_PARTNER_STILL_VALID(iMyPartner)
											PRINT_HOLD_UP_STRING("[timeDebug] My partner is still valid")
											
											playerPartner = INT_TO_NATIVE(PLAYER_INDEX, iMyPartner)
											IF IS_NET_PLAYER_OK(playerPartner, FALSE)
												PRINT_HOLD_UP_STRING_WITH_PLAYER_NAME("[timeDebug] My partner is... ", playerPartner)
												
												
												VECTOR vTemp
												IF NOT IS_PED_INJURED(GET_PLAYER_PED(playerPartner))
													vTemp = GET_ENTITY_COORDS(GET_PLAYER_PED(playerPartner))
													PRINT_HOLD_UP_STRING_VECTOR("My partner's location is ", vTemp)
												ENDIF
												
											ELSE
												PRINT_HOLD_UP_STRING("[timeDebug] My Partner is stil valid, but they're not ok!")
											ENDIF
										ELSE
											PRINT_HOLD_UP_STRING("[timeDebug] Still waiting for my partner, but they're not valid!")
										ENDIF
										
										RESET_NET_TIMER(timeDebug)
										START_NET_TIMER(timeDebug)
									ENDIF
								ENDIF
							ENDIF
						#ENDIF
						
						IF IS_BIT_SET(GlobalplayerBD_FM[iMyPlayerInt].iHoldUpTutBitset, biHT_ReachedMeetLoc)
							IF NOT IS_BIT_SET(GlobalplayerBD_FM[iMyPartner].iHoldUpTutBitset, biHT_ReachedMeetLoc)
								DO_OTHER_PLAYER_BLIP_HELP_TEXT()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE	
				//-- No partner
				IF IS_BIT_SET(GlobalplayerBD_FM[iMyPlayerInt].iHoldUpTutBitset, biHT_ReachedMeetLoc)
				
					//-- Set hold up blip to flash
				//	Crim_HOLD_UP_POINT myHoldUp
				
					SET_BIT(holdUpTut.iHoldUpTutClientBitset, biL_DoHelpText)
					SET_BIT(holdUpTut.iHoldUpTutClientBitset, biL_SkipFriendHelpText)
					SET_BIT(holdUpTut.iHoldUpTutClientBitset, biL_DoStoreBlip)
				
					
					CLEAR_HOLD_UP_TUT_OBJECTIVE_TEXT(holdUpTut)
					
					SET_LOCAL_PLAYER_READY_TO_DO_TUT_HOLD_UP(TRUE)
					//SET_FM_UNLOCKS_BIT_SET() // Unlock Holdups
					holdUpTut.iHoldUpTutProg = HOLD_UP_TUT_STAGE_DO_HOLD_UP
					
					
						
					#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Client moving to stage HOLD_UP_TUT_STAGE_DO_HOLD_UP as I'm at meet and I have no partner") #ENDIF
					
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(holdUpTut.iHoldUpTutClientBitset, biL_DoKickTimer)
				DRAW_HOLD_UP_KICK_PLAYER_TIMER(MAX_TIME_GET_TO_HOLD_UP, holdUpTut.timeGetToMeetLoc)
			//	IF NOT IS_BIT_SET(holdUpTut.iHoldUpTutClientBitset, biL_TimeExpireObj)
				//	Print_Objective_Text("FM_HTUT_GOT") //Go to the ~y~store~s~ before the time expires.
				//	SET_BIT(holdUpTut.iHoldUpTutClientBitset, biL_TimeExpireObj)
				//	#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Doing time expire objectivbe text") #ENDIF
			//	ENDIF
			ENDIF
		BREAK
		
		CASE HOLD_UP_TUT_FRIEND_HELP
			IF HAS_NET_TIMER_EXPIRED(timeWaitForHelp, 1000)
				holdUpTut.iHoldUpTutProg = HOLD_UP_TUT_STAGE_DO_HOLD_UP
				CLEAR_HOLD_UP_TUT_OBJECTIVE_TEXT(holdUpTut)
				SET_LOCAL_PLAYER_READY_TO_DO_TUT_HOLD_UP(TRUE)
				START_NET_TIMER(timeWaitForHelp)
				//SET_FM_UNLOCKS_BIT_SET() // Unlock Holdups

				#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Client moving to stage HOLD_UP_TUT_STAGE_DO_HOLD_UP as I'm at meet with partner") #ENDIF
			ENDIF
		BREAK
		
		CASE HOLD_UP_TUT_STAGE_DO_HOLD_UP

			
			iMyPlayerInt = NATIVE_TO_INT(PLAYER_ID())
			iMyPartner = serverBD.holdUpTutServer.iHoldUpPartner[iMyPlayerInt]
			iMyHoldUpLoc = serverBD.holdUpTutServer.iHoldUpToUse[iMyPlayerInt]
			IF NOT IS_BIT_SET(iBoolsBitSet, biL_OpenedUpHoldUp)
				IF IS_LOCAL_PLAYER_NEAR_HOLD_UP_LOCATION(35.0)
					iMyHoldUp = ENUM_TO_INT(GET_HOLD_UP_FROM_MEET_LOC(iMyHoldUpLoc))
					IF iMyHoldUp = 0 ENDIF
					BROADCAST_HOLD_UP_FAKE_DONE(iMyHoldUp, FALSE, ALL_PLAYERS())
					SET_BIT(iBoolsBitSet, biL_OpenedUpHoldUp)
					#IF IS_DEBUG_BUILD
						PRINT_HOLD_UP_STRING_INT("Calling BROADCAST_HOLD_UP_FAKE_DONE(iMyHoldUp, FALSE) with iMyHoldUp = ", iMyHoldUp)
					#ENDIF
				ENDIF
			ENDIF
					
		//	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
		//	OR IS_LOCAL_PLAYER_IN_HOLD_UP_LOCATION()
				IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHt_ToldToLoseWanted)
					CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHt_ToldToLoseWanted)
				ENDIF
				IF NOT IS_BIT_SET(holdUpTut.iHoldUpTutClientBitset, biL_ToldToHoldUpStore)
					IF NOT IS_BIT_SET(holdUpTut.iHoldUpTutClientBitset, biL_ToldToHelpPartnerLose)	
						IF NOT IS_LOCAL_PLAYER_IN_HOLD_UP_LOCATION()
							
							IF iMyPartner > -1
							AND IS_MY_PARTNER_STILL_VALID(iMyPartner)
								playerPartner = INT_TO_NATIVE(PLAYER_INDEX, iMyPartner)
								IF NETWORK_IS_PLAYER_ACTIVE(playerPartner)
									IF IS_NET_PLAYER_OK(playerPartner)
										IF NOT IS_BIT_SET(GlobalplayerBD_FM[iMyPartner].iHoldUpTutBitset, biHt_ToldToLoseWanted)
											handlePlayer = GET_GAMER_HANDLE_PLAYER(playerPartner)
											IF NETWORK_IS_FRIEND(handlePlayer)
												Print_Objective_Text_With_Player_Name("FM_HTUT_FHLD", playerPartner, FALSE)
												#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Displaying do hold up objective text with partner - friend") #ENDIF
											ELSE
												Print_Objective_Text_With_Player_Name("FM_HTUT_HLD", playerPartner, FALSE) //Hold up the Store ~BLIP_CRIM_HOLDUPS~ with ~a~.
												#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Displaying do hold up objective text with partner - not friend") #ENDIF
											ENDIF
											SET_BIT(holdUpTut.iHoldUpTutClientBitset, biL_ToldToHoldUpStore)
											
										ELSE
											//-- my partner has a wanted level
											IF NOT IS_BIT_SET(holdUpTut.iHoldUpTutClientBitset, biL_ToldToHelpPartnerLose) 
												//-- My partner has a wanted leve
												SET_BIT(holdUpTut.iHoldUpTutClientBitset, biL_ToldToHelpPartnerLose)	
												Print_Objective_Text_With_Player_Name("FM_HTUT_HLC", playerPartner) //Help ~a~ lose the Cops.
												#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("I've been told to help my partner loses their wanted level") #ENDIF
											ENDIF
										ENDIF
										
									ENDIF
								ENDIF
							ENDIF
							
							IF NOT IS_BIT_SET(holdUpTut.iHoldUpTutClientBitset, biL_ToldToHoldUpStore)
							AND NOT IS_BIT_SET(holdUpTut.iHoldUpTutClientBitset, biL_ToldToHelpPartnerLose)	
								//-- Partner not valid
								Print_Objective_Text("FM_HTUT_HLS") // Hold up the Store ~BLIP_CRIM_HOLDUPS~.
								SET_BIT(holdUpTut.iHoldUpTutClientBitset, biL_ToldToHoldUpStore)
								SET_BIT(GlobalplayerBD_FM[iMyPlayerInt].iHoldUpTutBitset, biHT_DoingHoldUpSolo)
								#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Setting biHT_DoingHoldUpSolo as I'm doing hold up on my own") #ENDIF
							ENDIF

						ENDIF
					ELSE
						//-- Told to help partner lose wanted level.
						iMyPlayerInt = NATIVE_TO_INT(PLAYER_ID())
						iMyPartner = serverBD.holdUpTutServer.iHoldUpPartner[iMyPlayerInt]
						IF iMyPartner > -1
						AND IS_MY_PARTNER_STILL_VALID(iMyPartner)
							IF NOT IS_BIT_SET(GlobalplayerBD_FM[iMyPartner].iHoldUpTutBitset, biHt_ToldToLoseWanted)
								CLEAR_BIT(holdUpTut.iHoldUpTutClientBitset, biL_ToldToHelpPartnerLose)
								#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("I was told to help my partner lose their wanted level, but they're no longer have a wanted level") #ENDIF
						
							ENDIF
						ELSE
							CLEAR_BIT(holdUpTut.iHoldUpTutClientBitset, biL_ToldToHelpPartnerLose)
							#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("I was told to help my partner lose thier wanted level, but they're no longer valid") #ENDIF
						ENDIF
					ENDIF
					
				ELSE	
					//--- told to hold up store
					IF IS_LOCAL_PLAYER_IN_HOLD_UP_LOCATION()
						IF IS_BIT_SET(holdUpTut.iHoldUpTutClientBitset, biL_ToldToHoldUpStore)
						//	CLEAR_HOLD_UP_TUT_OBJECTIVE_TEXT(holdUpTut)
							CLEAR_BIT(holdUpTut.iHoldUpTutClientBitset, biL_ToldToHoldUpStore)
							#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Clearing biL_ToldToHoldUpStore as I'm in hold up store") #ENDIF
						ENDIF
					ELSE
						// Not in store, check for partner having wanted level
						IF iMyPartner > -1
						AND IS_MY_PARTNER_STILL_VALID(iMyPartner)
							IF IS_BIT_SET(GlobalplayerBD_FM[iMyPartner].iHoldUpTutBitset, biHt_ToldToLoseWanted)
								CLEAR_BIT(holdUpTut.iHoldUpTutClientBitset, biL_ToldToHoldUpStore)
								#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("I've been told to hold up the store, but my partner now has a wanted level") #ENDIF
							ENDIF
						ENDIF	
					ENDIF
				ENDIF
				
				//--Passive mode help
//				IF NOT IS_BIT_SET(iBoolsBitSet, biL_PassiveModeHelp)
//					IF IS_MP_PASSIVE_MODE_ENABLED()
//						IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
//							SET_BIT(iBoolsBitSet, biL_PassiveModeHelp)
//							PRINT_HELP("FM_PASS_DBL")
//							#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("Doing passive help as player has passive mode enabled") #ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
		BREAK
		
		CASE HOLD_UP_TUT_STAGE_DONE
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	IF holdUpTut.iHoldUpTutProg > HOLD_UP_TUT_STAGE_INIT
		
		//-- Check partner is still valid
		MAINTAIN_HOLD_UP_TUT_CLIENT_PARTNER_CHECKS_V2(holdUpTut)
		
		MAINTAIN_HOLD_UP_TUT_MEET_LOC_CHECKS(holdUpTut #IF IS_DEBUG_BUILD, bWdDoValidStoreDebug #ENDIF)
		
		MAINTAIN_HOLD_UP_TUT_HELP_TEXT(holdUpTut)
		
		MAINTAIN_HOLD_UP_TUT_STORE_BLIP(holdUpTut)
		
		IF NOT HAS_FM_HOLD_UP_TUT_BEEN_DONE()
		//	IF GET_MP_INT_CHARACTER_STAT(MP_STAT_TOTAL_NO_SHOPS_HELD_UP ) > 0
		//	#IF IS_DEBUG_BUILD OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S) #ENDIF
			IF SHOULD_HOLD_UP_TUTORIAL_COMPLETE(holdUpTut)
				IF DOES_BLIP_EXIST(holduptut.blipHoldUpTutMeetLoc)
					REMOVE_BLIP(holduptut.blipHoldUpTutMeetLoc)
				ENDIF
				IF DOES_BLIP_EXIST(holduptut.blipMyHoldup)
					REMOVE_BLIP(holduptut.blipMyHoldup)
				ENDIF
			//	mpglobalsAmbience.iHoldUpTutBlipLongRange = -1
			//	mpglobalsAmbience.iHoldUPTutBlipFlash = -1
				IF IS_BIT_SET(holdUpTut.iHoldUpTutClientBitset, biL_TargetBuddyDisabled)
					iMyPartner = serverBD.holdUpTutServer.iHoldUpPartner[NATIVE_TO_INT(PLAYER_ID())]
					IF SET_MY_HOLD_UP_TUT_PARTNER_CAN_TARGET_ME(iMyPartner, TRUE)
						
					ENDIF
				ENDIF
				
				//-- Need to make the police less aggressive if you gain a wanted level from the hold up. DOne in 
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
					SET_LOCAL_PLAYER_HAD_WANTED_LEVEL_WHEN_COMPLETEING_HOLD_UP_TUT(TRUE)
					#IF IS_DEBUG_BUILD 
					
						PRINT_HOLD_UP_STRING("I have a wanted level when completing") 
					#ENDIF 
				ENDIF
				
				IF IS_BIT_SET(iBoolsBitSet, biL_FriendlyFireOff)
					NETWORK_SET_FRIENDLY_FIRE_OPTION(TRUE)
					CLEAR_BIT(iBoolsBitSet, biL_FriendlyFireOff)
					#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("NETWORK_SET_FRIENDLY_FIRE_OPTION - Turning ON friendly fire as about to go to stage HOLD_UP_TUT_STAGE_DONE")  #ENDIF
			
				ENDIF
							
				Clear_Any_Objective_Text_From_This_Script()
				SET_LOCAL_PLAYER_COMPLETED_HOLD_UP_TUT(TRUE)
				SET_LOCAL_PLAYER_DOING_HOLD_UP_TUT(FALSE)
				SET_LOCAL_PLAYER_READY_FOR_HOLD_UP_TUT(FALSE)
				SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_HOLD_UP_TUT, FALSE)
			
				SET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_HOLDTUT_DONE, TRUE)
			//	SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), FALSE)
//				REQUEST_SAVE(STAT_SAVETYPE_PROLOGUE)
				SET_FM_UNLOCKS_BIT_SET()
				holdUpTut.iHoldUpTutProg = HOLD_UP_TUT_STAGE_DONE
				#IF IS_DEBUG_BUILD 
					g_bDebugLaunchHoldupTut = FALSE
					PRINT_HOLD_UP_STRING("Client moving to stage HOLD_UP_TUT_STAGE_DONE as hold up tut finished") 
				#ENDIF 
			ENDIF 
		ELSE
			
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		DO_HOLD_UP_TUT_J_SKIPS(holdUpTut)
	#ENDIF
	RETURN FALSE
ENDFUNC	

#IF IS_DEBUG_BUILD
	PROC UPDATE_WIDGETS(FM_PLAYER_HOLD_UP_TUT_STRUCT &holdUpTut)
		INT iMyPlayerInt
		INT i
		IF bWdShowPlayerInfo
			iMyPlayerInt = NATIVE_TO_INT(PLAYER_ID())
			iWdPartner = serverBD.holdUpTutServer.iHoldUpPartner[iMyPlayerInt] 
			iWdLoc = serverBD.holdUpTutServer.iHoldUpToUse[iMyPlayerInt]
			iWdTutProg = holdUpTut.iHoldUpTutProg
			bWdStillValid = IS_HOLD_UP_STILL_VALID(serverBD.holdUpTutServer.iHoldUpToUse[NATIVE_TO_INT(PLAYER_ID())], TRUE)
		ENDIF
		
		IF bResetTut
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_HOLDTUT_DONE, FALSE)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_TOTAL_NO_SHOPS_HELD_UP, 0 )
			//holdUpTut.iHoldUpTutProg = 0
			//holdUpTut.iHoldUpTutClientBitset = 0
			//RESET_NET_TIMER(holdUpTut.)
			bResetTut = FALSE
		ENDIF
		
		IF bWdResetMyHoldUp
			bWdResetMyHoldUp = FALSE
			IF serverBD.holdUpTutServer.iHoldUpToUse[NATIVE_TO_INT(PLAYER_ID())] > -1
				PLAYER_INDEX playerHost = NETWORK_GET_HOST_OF_SCRIPT(FREEMODE_SCRIPT())
				IF playerHost <> INVALID_PLAYER_INDEX()
					Crim_HOLD_UP_POINT myHoldUp = GET_HOLD_UP_FROM_MEET_LOC(serverBD.holdUpTutServer.iHoldUpToUse[NATIVE_TO_INT(PLAYER_ID())])
					
					BROADCAST_MAKE_HOLD_UP_ACTIVE(ENUM_TO_INT(myHoldUp), TRUE, SPECIFIC_PLAYER(NETWORK_GET_HOST_OF_SCRIPT(FREEMODE_SCRIPT())))
				ENDIF
			ENDIF
		ENDIF
		
		IF bWdCheckInUse
			bWdCheckInUse = FALSE
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				FOR i = 0 TO MAX_HOLD_UP_TUT_MEET_LOC - 1
					IS_HOLD_UP_LOC_STILL_REQUIRED(i)
				ENDFOR
			ENDIF
		ENDIF
		
		IF bWdCheckLocValid
			bWdCheckLocValid = FALSE
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				FOR i = 0 TO MAX_HOLD_UP_TUT_MEET_LOC - 1
					IF IS_HOLD_UP_STILL_VALID(i)
						PRINT_HOLD_UP_STRING_INT("Hold up still valid ", i)
					ELSE
						PRINT_HOLD_UP_STRING_INT("Hold up not valid ", i)
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
		
		IF bWdBlipAllMeetLoc
			FOR i = 0 TO MAX_HOLD_UP_TUT_MEET_LOC - 1
				blipAllMeetLoc[i] = ADD_BLIP_FOR_COORD(GET_HOLD_UP_MEET_LOCATION(i))
			ENDFOR
			IF DOES_BLIP_EXIST(blipAllMeetLoc[0]) ENDIF
			bWdBlipAllMeetLoc = FALSE
		ENDIF
		
		IF bWdGetSpeed
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
					fWdSpeed = GET_ENTITY_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				ENDIF
			ENDIF
		ENDIF
		
		VECTOR vHoldUp
		IF bWdShowDist
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				vHoldUp = GET_HOLD_UP_BLIP_LOCATION_FROM_MEET_LOC( serverBD.holdUpTutServer.iHoldUpToUse[NATIVE_TO_INT(PLAYER_ID())])
				fWdDist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vHoldUp)
			ENDIF
		ENDIF
	ENDPROC
#ENDIF
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                          ///////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

SCRIPT(MP_MISSION_DATA missionScriptArgs)
	
	
	PROCESS_PRE_GAME(missionScriptArgs)	
	
	#IF IS_DEBUG_BUILD
		CREATE_WIDGETS()	
	#ENDIF
	
	
	
	WHILE TRUE
	
		MP_LOOP_WAIT_ZERO()
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			SCRIPT_PROFILER_START_OF_FRAME()
		#ENDIF
		#ENDIF		
		
		

		// If we have a match end event, bail.
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		OR NOT IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
			CLEANUP_SCRIPT()
		ENDIF	
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER( "Cleanup checks")
		#ENDIF
		#ENDIF 
		
		#IF IS_DEBUG_BUILD
			UPDATE_WIDGETS(fmHoldUpTut)
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER( "UPDATE_WIDGETS")
			#ENDIF
		#ENDIF
		
		
// *****************************************************************************************************************************************************************	
// *****************************************************************************************************************************************************************	
//												LOCAL PROCESSING
// *****************************************************************************************************************************************************************	
// *****************************************************************************************************************************************************************
		/*
		IF NOT HAS_FM_HOLD_UP_TUT_BEEN_DONE()
			IF DO_HOLD_UP_TUT_CLIENT_V2(fmHoldUpTut)
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHT_CompletedHoldUpTut)
				#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("I've already completed hold up tut") #ENDIF
				SET_LOCAL_PLAYER_COMPLETED_HOLD_UP_TUT(TRUE)
				SET_LOCAL_PLAYER_READY_FOR_HOLD_UP_TUT(FALSE)
				
			ENDIF
		ENDIF
		*/
		
		// -----------------------------------
		// Process your game logic.....
		SWITCH GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT())
			
			// Wait untill the server gives the all go before moving on
			CASE GAME_STATE_INI
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING
					#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_INI >  GAME_STATE_RUNNING") #ENDIF
					
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					
					#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_INI >  GAME_STATE_TERMINATE_DELAY") #ENDIF
					
				ENDIF
			BREAK
			
			// Main gameplay state.
			CASE GAME_STATE_RUNNING
				
				
				
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					IF NOT HAS_FM_HOLD_UP_TUT_BEEN_DONE()
					#IF IS_DEBUG_BUILD OR g_bDebugLaunchHoldupTut #ENDIF
						IF DO_HOLD_UP_TUT_CLIENT_V2(fmHoldUpTut)
						ENDIF
					ELSE
						IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHT_CompletedHoldUpTut)
							#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("I've already completed hold up tut") #ENDIF
							SET_LOCAL_PLAYER_COMPLETED_HOLD_UP_TUT(TRUE)
							SET_LOCAL_PLAYER_READY_FOR_HOLD_UP_TUT(FALSE)
							
						//	mpglobalsAmbience.iHoldUpTutBlipLongRange = -1
						//	mpglobalsAmbience.iHoldUPTutBlipFlash = -1
							Clear_Any_Objective_Text_From_This_Script()
						ENDIF
						SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_HOLD_UP_TUT, FALSE)
					ENDIF
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_RUNNING >  GAME_STATE_TERMINATE_DELAY") #ENDIF
				ENDIF
				
				/*
				// Make the player end the mission if they leave the Boat
				IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niBoat)
					IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niBoat)), <<LEAVE_RANGE, LEAVE_RANGE, LEAVE_RANGE>>)
						playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
						NET_PRINT_TIME() NET_PRINT(" ---> BOAT TAXI - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 6 - MISSION END - PLAYER NOT NEAR BOAT ") NET_PRINT_VECTOR(serverBD.vDropLocation) NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 6")	#ENDIF
					ENDIF
				ENDIF
				*/
			BREAK
			
			CASE GAME_STATE_TERMINATE_DELAY 
				SET_MISSION_FINISHED(serverBD.timeTerminate)
		      	IF IS_MISSION_READY_TO_CLEANUP(serverBD.timeTerminate)
		            playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_TERMINATE_DELAY >  GAME_STATE_END") #ENDIF
		      	ENDIF
			BREAK
			
			// The game stage the local player is placed when we want him to leave a mission
			CASE GAME_STATE_LEAVE

				playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
				#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_LEAVE >  GAME_STATE_END") #ENDIF

			FALLTHRU
			
			//Cleans up then terminates the mission
			CASE GAME_STATE_END
				#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("About to run cleanup from GAME_STATE_END") #ENDIF
				CLEANUP_SCRIPT()
			BREAK

		ENDSWITCH
		
	 	

		
// *****************************************************************************************************************************************************************	
// *****************************************************************************************************************************************************************	
//												SERVER PROCESSING
// *****************************************************************************************************************************************************************	
// *****************************************************************************************************************************************************************	
		/*
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			// server processing
			MAINTAIN_SERVER_STAGGERED_LOOP()
			MAINTAIN_HOLD_UP_TUTORIAL_SERVER_V2()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_HOLD_UP_TUTORIAL_SERVER")
			#ENDIF
			#ENDIF		
		ENDIF
		*/
		
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			
			SWITCH GET_SERVER_MISSION_STATE()
				
				// For CnC group missions, we may want to wait for all players to have launched the script
				CASE GAME_STATE_INI
					
					serverBD.iServerGameState = GAME_STATE_RUNNING
					#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("serverBD.iServerGameState = GAME_STATE_RUNNING") #ENDIF
						
				BREAK
				
				// Look for game end conditions
				CASE GAME_STATE_RUNNING
					MAINTAIN_SERVER_STAGGERED_LOOP()
					MAINTAIN_HOLD_UP_TUTORIAL_SERVER_V2()
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
						ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_HOLD_UP_TUTORIAL_SERVER")
					#ENDIF
					#ENDIF		

					IF HAVE_MISSION_END_CONDITIONS_BEEN_MET()
						serverBD.iServerGameState = GAME_STATE_END
						#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING ("serverBD.iServerGameState GAME_STATE_RUNNING > GAME_STATE_END ")#ENDIF
						
					ENDIF
					
				BREAK
				
				CASE GAME_STATE_END
				BREAK
				
			ENDSWITCH
			
		ENDIF
		
		
		
		#IF IS_DEBUG_BUILD 
			#IF SCRIPT_PROFILER_ACTIVE
				SCRIPT_PROFILER_END_OF_FRAME()
			#ENDIF
		#ENDIF			
	
	ENDWHILE

ENDSCRIPT

