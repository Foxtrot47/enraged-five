//////////////////////////////////////////////////////////////////////////////////////////
// Name:        GB_FIVESTAR.sc															//
// Description: Gang Boss Challenge: Survive longest with 5 star wanted level			//
// Written by:  Ryan Elliott															//
// Date: 		07/03/2015																//
//////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "net_mission.sch"


//----------------------
//	INCLUDES
//----------------------
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "flow_public_core_override.sch"
USING "net_wait_zero.sch"
USING "net_mission.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"
USING "DM_Leaderboard.sch"
USING "net_gang_boss.sch"
USING "net_gang_boss_spectate.sch"

USING "am_common_ui.sch"

//----------------------
//	**TEMP HARD CODED**
//----------------------

//----------------------
//	ENUM
//----------------------

ENUM GBFIVESTAR_END_REASON
	GBFIVESTAR_ENDREASON_NOWINNERS,
	GBFIVESTAR_ENDREASON_GOONWON,
	GBFIVESTAR_ENDREASON_TIMEUP,
	GBFIVESTAR_ENDREASON_BOSS_LEFT,
	GBFIVESTAR_ENDREASON_FAIL,
	
	GBFIVESTAR_ENDREASON_MAX 	// Always keep last
ENDENUM

//----------------------
//	CONSTANTS
//----------------------

CONST_INT REWARD_TIMEOUT	10000

//----------------------
//	Local Bitset
//----------------------

INT iLocalBitset

CONST_INT GBFIVESTAR_BS_SHARD_DONE				0
CONST_INT GBFIVESTAR_BS_INTRO_DONE				1
CONST_INT GBFIVESTAR_BS_KNOCKOUT_DONE			2
CONST_INT GBFIVESTAR_BS_INIT_WANTED				3
CONST_INT GBFIVESTAR_BS_INIT_SETUP				4
CONST_INT GBFIVESTAR_BS_ON_YACHT				5
CONST_INT GBFIVESTAR_BS_YACHT_WARN				6
CONST_INT GBFIVESTAR_BS_IN_AIR_VEHICLE			7
CONST_INT GBFIVESTAR_BS_IN_SUB					8
CONST_INT GBFIVESTAR_BS_COUNTDOWN_PLAYING		9
CONST_INT GBFIVESTAR_BS_TIMEOUT_PLAYING			10
CONST_INT GBFIVESTAR_BS_END_SHARD_DONE			11
CONST_INT GBFIVESTAR_BS_USING_PARACHUTE			12
CONST_INT GBFIVESTAR_BS_ADD_FOR_WINNER			13
CONST_INT GBFIVESTAR_BS_ADD_FOR_LDB_WINNER		14
CONST_INT GBFIVESTAR_BS_TIME_POSTED				15
CONST_INT GBFIVESTAR_BS_SAVED_PLAYERS_BIKE		16
CONST_INT GBFIVESTAR_BS_NOT_IN_VEHICLE			17
CONST_INT GBFIVESTAR_BS_IN_WRONG_VEHICLE		18
CONST_INT GBFIVESTAR_BS_BIKE_DESTROYED			19
CONST_INT GBFIVESTAR_BS_WON_WHILE_TIMEOUT		20

//----------------------
//	STRUCT
//----------------------

//----------------------
//	VARIABLES
//----------------------

PLAYER_INDEX piOtherPlayers[GB_MAX_GANG_SIZE_INCLUDING_BOSS]
SCRIPT_TIMER stInteriorTimeout
SCRIPT_TIMER stInteriorDelay
SCRIPT_TIMER stStartDelay
SCRIPT_TIMER stSurvivalStartTime

VEHICLE_INDEX viLocalPlayersBike
BLIP_INDEX biLocalPlayersBikeBlip
BOOL bIsThisABikerVariation = FALSE

GB_MAINTAIN_SPECTATE_VARS sSpecVars

BOOL bSet

INT iInteriorSound = -1
INT iCountdownSound = -1
INT iTimerCount = -1

INT iCurrentTime = -1
INT iCurrentTimer

INT iLocalTimeSurvived

SCRIPT_TIMER stKillstripTimer

GB_COUNTDOWN_MUSIC_STRUCT cmStruct

GB_STRUCT_BOSS_END_UI sEndUI
//----------------------
//	GAME STATE
//----------------------

CONST_INT GAME_STATE_INIT 							0
CONST_INT GAME_STATE_RUNNING						1
CONST_INT GAME_STATE_TERMINATE_DELAY				2
CONST_INT GAME_STATE_END							3

ENUM GBFIVESTAR_RUN_STAGE
	GBFIVESTAR_INIT = 0,
	GBFIVESTAR_SURVIVE,
	GBFIVESTAR_KNOCKEDOUT,
	GBFIVESTAR_REWARDS,
	GBFIVESTAR_END,
	
	GBFIVESTAR_MAX_STAGES			/* Always have at the end */
ENDENUM

//----------------------
//	BROADCAST VARIABLES
//----------------------

// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData

	INT iServerGameState = GAME_STATE_INIT
	
	INT iServerBitSet
	
	PLAYER_INDEX piRemainingPlayers[GB_MAX_GANG_SIZE_INCLUDING_BOSS]
	PLAYER_INDEX piPlayers[GB_MAX_GANG_SIZE_INCLUDING_BOSS]
	
	INT iPlayersRemaining
	
	SCRIPT_TIMER stChallengeTimer
	SCRIPT_TIMER stRewardTimeout
	
	GBFIVESTAR_RUN_STAGE eGBFIVESTAR_Stage = GBFIVESTAR_INIT
	GBFIVESTAR_END_REASON eGBFIVESTAR_EndReason = GBFIVESTAR_ENDREASON_GOONWON
	
	INT iMatchId1
	INT iMatchId2
	
ENDSTRUCT

//Server bitset
CONST_INT SERVER_BS_WORK_STARTED			0
CONST_INT SERVER_BS_GOON_WON				1
CONST_INT SERVER_BS_NO_WINNERS				2
CONST_INT SERVER_BS_TIME_UP					3
CONST_INT SERVER_BS_BOSS_LEFT				4
CONST_INT SERVER_BS_FINAL_LIST_INIT			5
CONST_INT SERVER_BS_PARTICIPANTS_SAVED		6
CONST_INT SERVER_BS_REWARDS_GIVEN			7
CONST_INT SERVER_BS_OUTRO_FINISHED			8

ServerBroadcastData serverBD

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	
	INT iClientGameState = GAME_STATE_INIT
	
	int iClientBitSet
	
	INT iTimeSurvived
	
	GBFIVESTAR_RUN_STAGE eGBFIVESTAR_Stage = GBFIVESTAR_INIT
ENDSTRUCT

//Client bitset
CONST_INT CLIENT_BS_START_TIMER				0
CONST_INT CLIENT_BS_LOCAL_WON				1
CONST_INT CLIENT_BS_LOCAL_KNOCKED_OUT		2
CONST_INT CLIENT_BS_LOCAL_IN_INTERIOR		3
CONST_INT CLIENT_BS_LOCAL_INTERIOR_KO		4
CONST_INT CLIENT_BS_LOCAL_BOSS_SPEC_KO		5
CONST_INT CLIENT_BS_TIME_UP					6
CONST_INT CLIENT_BS_JOINED_LATE				7
CONST_INT CLIENT_BS_REWARDS_GIVEN			8
CONST_INT CLIENT_BS_OUTRO_FINISHED			9

PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

//----------------------
//	DBG FUNCTIONS
//----------------------

#IF IS_DEBUG_BUILD
BOOL bKillScript, bUseDebugValues, bUseSMS

/// PURPOSE: Create widgets for GB_FIVESTAR
PROC CREATE_WIDGETS()

	START_WIDGET_GROUP("Gang Boss Challenge: Longest 5 Star Wanted Level")
		ADD_WIDGET_BOOL("Replace tickers with SMS", bUseSMS) 
	
		START_WIDGET_GROUP("Set up Script (Gang Boss Only)")
			ADD_WIDGET_BOOL("Use Debug Values", bUseDebugValues)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Server Only")
			ADD_WIDGET_BOOL("Kill Script", bKillScript)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	
	CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR ===  [CREATE_WIDGETS] - Widgets created.")
ENDPROC

#ENDIF

//----------------------
//	TUNABLES
//----------------------

/// PURPOSE: 600000 - Get overall duration of the challenge.
FUNC INT GET_CHALLENGE_DURATION()
	IF bIsThisABikerVariation
		RETURN (g_sMPTunables.iBIKER_ON_THE_RUN_TIME_LIMIT * 1000)
	ENDIF
	
	RETURN g_sMPTunables.igb_mostwanted_time_limit  
ENDFUNC

/// PURPOSE: 15000 - Get time before player is knocked out when in an interior
FUNC INT GB_GET_INTERIOR_TIMEOUT( BOOL bOffBike = FALSE )
	IF bOffBike
		RETURN (g_sMPTunables.igb_mostwanted_time_in_tunnels + 5000)
	ENDIF
	
	RETURN g_sMPTunables.igb_mostwanted_time_in_tunnels 
ENDFUNC

/// PURPOSE: Get number of stars given to the participants at the start of the mode
FUNC INT GB_GET_FORCED_WANTED_LEVEL()
	RETURN g_sMPTunables.igb_mostwanted_stars 
ENDFUNC

// -----------------------------------------------------------------------------------------------------------
//		Telemetry
// -----------------------------------------------------------------------------------------------------------
PROC PROCESS_TELEMETRY(BOOL bStraightToCleanup = FALSE)
	BOOL bWon
	INT iEndReason = GB_TELEMETRY_END_LOST
	
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_KNOCKED_OUT)
		bWon = TRUE
		iEndReason = GB_TELEMETRY_END_WON
	ENDIF
	
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_JOINED_LATE)
		bWon = FALSE
		iEndReason = GB_TELEMETRY_END_NONE
		playerBD[PARTICIPANT_ID_TO_INT()].iTimeSurvived = 0
		iLocalTimeSurvived = 0
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_BOSS_LEFT)
		bWon = FALSE
		iEndReason = GB_TELEMETRY_END_BOSS_LEFT
	ENDIF
	
	IF bStraightToCleanup
		playerBD[PARTICIPANT_ID_TO_INT()].iTimeSurvived = 0
		iLocalTimeSurvived = 0
		iEndReason = GB_TELEMETRY_END_LEFT
		bWon = FALSE
	ENDIF
	IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
		GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iActiveMission = FMMC_TYPE_BIKER_ON_THE_RUN
		GB_SET_COMMON_TELEMETRY_DATA_ON_END(bWon, iEndReason)
		GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iActiveMission = FMMC_TYPE_GB_CHAL_FIVESTAR
	ELSE
		GB_SET_COMMON_TELEMETRY_DATA_ON_END(bWon, iEndReason)
		PROCESS_CURRENT_BOSS_CHALLENGE_PLAYSTATS(g_sGb_Telemetry_data.sdata, playerBD[PARTICIPANT_ID_TO_INT()].iTimeSurvived)
	ENDIF
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------

PROC SCRIPT_CLEANUP(BOOL bStraightToCleanup = FALSE)
	CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [SCRIPT_CLEANUP]")

	Clear_Any_Objective_Text_From_This_Script()
	
	CLEAR_PERSONAL_VEHICLE_BLIP_COLOUR()
	
	g_sGb_Telemetry_data.sdata.m_match1 = serverBD.iMatchId1
	g_sGb_Telemetry_data.sdata.m_match2 = serverBD.iMatchId2
	
	IF bIsThisABikerVariation
		CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === BIKER Variation. Cleanup countdown music")
		GB_STOP_COUNTDOWN_MUSIC(cmStruct)
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	
		PROCESS_TELEMETRY(bStraightToCleanup)
		
		IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState != RESPAWN_STATE_PLAYING
			SET_MANUAL_RESPAWN_STATE(MRS_EXIT_SPECTATOR_AFTERLIFE)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === RESET_GAME_STATE_ON_DEATH 1")
			RESET_GAME_STATE_ON_DEATH()
		ELSE
			GB_SPECTATE_DO_SCREEN_FADE_IN_IF_NEEDED(sSpecVars)
		ENDIF
		IF GET_GAME_STATE_ON_DEATH() = AFTERLIFE_SET_SPECTATORCAM
			RESET_GAME_STATE_ON_DEATH()
			CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === RESET_GAME_STATE_ON_DEATH 2")
		ENDIF
		
		IF GET_MAX_WANTED_LEVEL() < 5
			SET_MAX_WANTED_LEVEL(5)
		ENDIF
		
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontClearLocalPassengersWantedLevel, FALSE)
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_FallsOutOfVehicleWhenKilled, FALSE) 
		ENDIF
		
		IF IS_ENTITY_A_GHOST(PLAYER_PED_ID())
			SET_LOCAL_PLAYER_AS_GHOST(FALSE)
			SET_PLAYER_DEFAULT_FRIENDLY_FIRE_OPTION()
			CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [SCRIPT_CLEANUP] - SET_LOCAL_PLAYER_AS_GHOST(FALSE)")
		ENDIF
		
		GB_TIDYUP_SPECTATOR_CAM()
		
		GB_COMMON_BOSS_MISSION_CLEANUP()
	
	ENDIF
	
	TERMINATE_THIS_THREAD()
ENDPROC

//----------------------
//	GET/SET FUNCTIONS
//----------------------

FUNC STRING GET_MISSION_STATE_STRING(INT iState)

	SWITCH(iState)
		CASE GAME_STATE_INIT 				RETURN "GAME_STATE_INIT"
		CASE GAME_STATE_RUNNING 			RETURN "GAME_STATE_RUNNING"
		CASE GAME_STATE_TERMINATE_DELAY 	RETURN "GAME_STATE_TERMINATE_DELAY"
		CASE GAME_STATE_END 				RETURN "GAME_STATE_END"
	ENDSWITCH

	RETURN "UNKNOWN STATE!"

ENDFUNC

//Helper function to get a clients game/mission state
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iClientGameState
ENDFUNC

//Helper function to get the servers game/mission state
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC

//Helper function to set the servers game/mission state
PROC SET_SERVER_MISSION_STATE(INT iState)
	serverBD.iServerGameState = iState
	CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === SET_SERVER_MISSION_STATE = ",GET_MISSION_STATE_STRING(iState))
ENDPROC

//Helper function to set a clients game/mission state
PROC SET_CLIENT_MISSION_STATE(INT iPlayer,INT iState)
	playerBD[iPlayer].iClientGameState = iState
	CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === SET_CLIENT_MISSION_STATE = ",GET_MISSION_STATE_STRING(iState))
ENDPROC

FUNC STRING GET_MISSION_STAGE_STRING(GBFIVESTAR_RUN_STAGE eState)

	SWITCH(eState)
		CASE GBFIVESTAR_INIT						RETURN "GBFIVESTAR_INIT"
		CASE GBFIVESTAR_REWARDS 					RETURN "GBFIVESTAR_REWARDS"
		CASE GBFIVESTAR_END		 					RETURN "GBFIVESTAR_END"
	ENDSWITCH

	RETURN "UNKNOWN STATE!"

ENDFUNC

FUNC STRING GET_MISSION_END_REASON_STRING(GBFIVESTAR_END_REASON eEndReason)

	SWITCH(eEndReason)
		CASE GBFIVESTAR_ENDREASON_GOONWON						RETURN "GBFIVESTAR_ENDREASON_GOONWON"
		CASE GBFIVESTAR_ENDREASON_FAIL		 					RETURN "GBFIVESTAR_ENDREASON_FAIL"
		CASE GBFIVESTAR_ENDREASON_BOSS_LEFT		 				RETURN "GBFIVESTAR_ENDREASON_BOSS_LEFT"
		CASE GBFIVESTAR_ENDREASON_TIMEUP		 				RETURN "GBFIVESTAR_ENDREASON_TIMEUP"
		CASE GBFIVESTAR_ENDREASON_NOWINNERS						RETURN "GBFIVESTAR_ENDREASON_NOWINNERS"
	ENDSWITCH

	RETURN "UNKNOWN STATE!"

ENDFUNC

/// PURPOSE: Return server mission stage.
FUNC  GBFIVESTAR_RUN_STAGE GET_SERVER_MISSION_STAGE()
	RETURN serverBD.eGBFIVESTAR_Stage
ENDFUNC

/// PURPOSE: SERVER ONLY: Set server mission stage.
PROC SET_SERVER_MISSION_STAGE(GBFIVESTAR_RUN_STAGE eStage)
	CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR ===  [SET_SERVER_MISSION_STAGE] - serverBD.eGBFIVESTAR_Stage = ", GET_MISSION_STAGE_STRING(eStage))
	serverBD.eGBFIVESTAR_Stage = eStage
ENDPROC


/// PURPOSE: Get server mission end reason.
FUNC GBFIVESTAR_END_REASON GET_SERVER_MISSION_END_REASON()
	RETURN serverBD.eGBFIVESTAR_EndReason
ENDFUNC

/// PURPOSE: SERVER ONLY: Set server mission end reason.
/// GBFIVESTAR_ENDREASON_FLED
/// GBFIVESTAR_ENDREASON_TIMEUP
PROC SET_SERVER_MISSION_END_REASON(GBFIVESTAR_END_REASON eGBEndReason)
	serverBD.eGBFIVESTAR_EndReason = eGBEndReason
	CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR ===  [SET_SERVER_MISSION_END_REASON] - serverBD.eGBFIVESTAR_EndReason = ", GET_MISSION_END_REASON_STRING(eGBEndReason))
ENDPROC


/// PURPOSE: Return server mission stage.
FUNC GBFIVESTAR_RUN_STAGE GET_CLIENT_MISSION_STAGE(INT partID)
	RETURN playerBD[partID].eGBFIVESTAR_Stage
ENDFUNC

/// PURPOSE: SERVER ONLY: Set server mission stage.
PROC SET_CLIENT_MISSION_STAGE(GBFIVESTAR_RUN_STAGE eStage)
	playerBD[PARTICIPANT_ID_TO_INT()].eGBFIVESTAR_Stage = eStage
	CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR ===  [SET_CLIENT_MISSION_STAGE] - playerBD[", PARTICIPANT_ID_TO_INT(), "].iClientGameState = ", GET_MISSION_STAGE_STRING(eStage))
ENDPROC

/// PURPOSE: Return the goon that survived the longest.
FUNC PLAYER_INDEX GBFIVESTAR_GET_WINNER()
	RETURN serverBD.piRemainingPlayers[0]
ENDFUNC

/// PURPOSE: Return the number of players remaining in the challenge (not knocked out).
FUNC INT GBFIVESTAR_GET_NUM_PLAYERS_REMAINING()
	RETURN serverBD.iPlayersRemaining
ENDFUNC

/// PURPOSE: SERVER ONLY: Set the number of players remaining in the challenge (not knocked out).
PROC GBFIVESTAR_SET_NUM_PLAYERS_REMAINING(INT iPlayers)
	serverBD.iPlayersRemaining = iPlayers
ENDPROC

/// PURPOSE: Return whether the knocked out shard should be displayed.
FUNC BOOL SHOULD_SKIP_KNOCKOUT_SHARD()
	IF GBFIVESTAR_GET_NUM_PLAYERS_REMAINING() <= 1
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

//----------------------
//	FUNCTIONS
//----------------------

/// PURPOSE: Initialise script with target and method of kill selected by gang boss.
FUNC BOOL INIT_GBFIVESTAR()
	
	#IF IS_DEBUG_BUILD
		// Set target and preferred kill method via RAG widget. Must be launched via M menu on the gang boss to use.
		IF bUseDebugValues
			
		ENDIF
	#ENDIF
	
	INT iIndex
	REPEAT GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS() iIndex
		serverBD.piPlayers[iIndex] = INVALID_PLAYER_INDEX()
		serverBD.piRemainingPlayers[iIndex] = INVALID_PLAYER_INDEX()
		piOtherPlayers[iIndex] = INVALID_PLAYER_INDEX()
	ENDREPEAT
	
	IF GB_LAUNCH_CHALLENGE_WITH_SPEC_CAM()
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_JOINED_LATE)
	ENDIF
	
	PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBD.iMatchId1, serverBD.iMatchId2)
	
	CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR ===  [INIT_GBFIVESTAR] - PASS - Data OK. Continuing script.")

	RETURN TRUE
ENDFUNC

FUNC BOOL GB_SHOULD_EVENT_BE_HIDDEN()
	IF GB_SHOULD_HIDE_GANG_BOSS_EVENT(FMMC_TYPE_GB_CHAL_FIVESTAR)
	OR GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GB_SHOULD_UI_BE_HIDDEN()
	IF NOT MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
	OR GB_SHOULD_EVENT_BE_HIDDEN()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC UPDATE_TIME_SURVIVED()
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_KNOCKED_OUT)
	AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_JOINED_LATE)
		IF GET_SERVER_MISSION_STAGE() < GBFIVESTAR_REWARDS
			iLocalTimeSurvived = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.stChallengeTimer)
		ELIF NOT IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_ADD_FOR_WINNER)
			IF GBFIVESTAR_GET_NUM_PLAYERS_REMAINING() = 1
			AND GBFIVESTAR_GET_WINNER() = PLAYER_ID()
				iLocalTimeSurvived += 1000
				CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR ===  [UPDATE_TIME_SURVIVED] - Local was winner. Adding a second.")
			ENDIF
			playerBD[PARTICIPANT_ID_TO_INT()].iTimeSurvived = iLocalTimeSurvived
			CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR ===  [UPDATE_TIME_SURVIVED] - Local player wasn't knocked out. Local time added to broadcast. Time Survived: ", playerBD[PARTICIPANT_ID_TO_INT()].iTimeSurvived)
			SET_BIT(iLocalBitset, GBFIVESTAR_BS_ADD_FOR_WINNER)
		ENDIF
	ELIF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_KNOCKED_OUT)
		IF NOT IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_TIME_POSTED)
			playerBD[PARTICIPANT_ID_TO_INT()].iTimeSurvived = iLocalTimeSurvived
			CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR ===  [UPDATE_TIME_SURVIVED] - Local player was knocked out. Local time added to broadcast. Time Survived: ", playerBD[PARTICIPANT_ID_TO_INT()].iTimeSurvived)
			SET_BIT(iLocalBitset, GBFIVESTAR_BS_TIME_POSTED)
		ENDIF
	ENDIF
ENDPROC

CONST_INT ciNUM_GANG_MEMBERS 8
STRUCT GB_P2P_LB_DATA
	INT iTimeSurvived
	PLAYER_INDEX PlayerId
ENDSTRUCT 

GB_P2P_LB_DATA sLbData[ciNUM_GANG_MEMBERS]

PROC DRAW_UI_TIMER(SCRIPT_TIMER& stTimer, INT iDuration)
	IF GB_SHOULD_UI_BE_HIDDEN()
		EXIT
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_WORK_STARTED)
	AND IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_PARTICIPANTS_SAVED)
	
		IF HAS_NET_TIMER_STARTED(stTimer)
			HUD_COLOURS timeColour
			STRING strLabel
			BOOL bForceRebuild
			
			UPDATE_TIME_SURVIVED()

			IF GET_SERVER_MISSION_STAGE() < GBFIVESTAR_REWARDS
				iCurrentTimer = (iDuration - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stTimer))
			ELSE
				IF IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_WON_WHILE_TIMEOUT)
					iCurrentTimer = (iDuration - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stTimer))
					CLEAR_BIT(iLocalBitset, GBFIVESTAR_BS_WON_WHILE_TIMEOUT)
				ENDIF
			ENDIF
			
			IF iCurrentTimer > 30000
				timeColour = HUD_COLOUR_WHITE
			ELSE
				timeColour = HUD_COLOUR_RED
			ENDIF
			
			iCurrentTimer = IMAX(iCurrentTimer, 0)
			
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_IN_INTERIOR)
			AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_KNOCKED_OUT)
			AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_JOINED_LATE)
			AND GET_SERVER_MISSION_STAGE() < GBFIVESTAR_REWARDS
				IF IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_ON_YACHT)
					strLabel = "GB_SFS_KOTIMER1"
				ELIF IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_IN_AIR_VEHICLE)
					strLabel = "GB_SFS_KOTIMER2"
				ELIF IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_IN_SUB)
					strLabel = "GB_SFS_KOTIMER3"
				ELIF IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_USING_PARACHUTE)
					strLabel = "GB_SFS_KOTIMER4"
				ELIF IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_NOT_IN_VEHICLE)
				OR IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_IN_WRONG_VEHICLE)
					strLabel = "GB_OTR_KOTIMER5"
				ELSE
					strLabel = "GB_SFS_KOTIMER"
				ENDIF
			ELSE
				strLabel = "GB_CHAL_END"
			ENDIF
			
			
			INT iIndex
			
			REPEAT GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS() iIndex
				sLbData[iIndex].iTimeSurvived = -1
				sLbData[iIndex].PlayerId = INVALID_PLAYER_INDEX()
			
				IF serverBD.piPlayers[iIndex] != INVALID_PLAYER_INDEX()
				AND IS_NET_PLAYER_OK(serverBD.piPlayers[iIndex], FALSE)
					INT iPart = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(serverBD.piPlayers[iIndex]))
					IF iPart <> -1
						IF IS_BIT_SET(playerBD[iPart].iClientBitSet, CLIENT_BS_LOCAL_KNOCKED_OUT)
						OR (GET_SERVER_MISSION_STAGE() >= GBFIVESTAR_REWARDS
						AND GBFIVESTAR_GET_NUM_PLAYERS_REMAINING() <= 1)
						
							sLbData[iIndex].iTimeSurvived = playerBD[iPart].iTimeSurvived
							sLbData[iIndex].PlayerId = serverBD.piPlayers[iIndex]
							
							IF NOT IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_ADD_FOR_LDB_WINNER)
								IF GBFIVESTAR_GET_NUM_PLAYERS_REMAINING() = 1
								AND GBFIVESTAR_GET_WINNER() = serverBD.piPlayers[iIndex]
									sLbData[iIndex].iTimeSurvived+=1000
									SET_BIT(iLocalBitset, GBFIVESTAR_BS_ADD_FOR_LDB_WINNER)
									CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR ===  [DRAW_UI_TIMER] - Player in first was definitive winner. Add one second to score.")
								ENDIF
							ENDIF
							
							#IF IS_DEBUG_BUILD
							IF sLbData[iIndex].PlayerId != INVALID_PLAYER_INDEX()
								CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR ===  [DRAW_UI_TIMER] - sLbData[", iIndex, "].PlayerId = ", GET_PLAYER_NAME(sLbData[iIndex].PlayerId))
								CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR ===  [DRAW_UI_TIMER] - sLbData[", iIndex, "].iTimeSurvived = ", sLbData[iIndex].iTimeSurvived)
								CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR ===  [DRAW_UI_TIMER] - Was player knocked out: ", IS_BIT_SET(playerBD[iPart].iClientBitSet, CLIENT_BS_LOCAL_KNOCKED_OUT))
								CPRINTLN(DEBUG_NET_AMBIENT, "")
							ELSE
								CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR ===  [DRAW_UI_TIMER] - sLbData[", iIndex, "].PlayerId = INVALID_PLAYER_INDEX()")
								CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR ===  [DRAW_UI_TIMER] - sLbData[", iIndex, "].iTimeSurvived = ", sLbData[iIndex].iTimeSurvived)
								CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR ===")
							ENDIF
							#ENDIF
						ELSE
							sLbData[iIndex].iTimeSurvived = HIGHEST_INT
							sLbData[iIndex].PlayerId = INVALID_PLAYER_INDEX()
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
			INT i, j
			GB_P2P_LB_DATA sLbDataTemp
			FOR i = 0 TO ciNUM_GANG_MEMBERS-1 STEP 1
				FOR j = i TO ciNUM_GANG_MEMBERS-1 STEP 1
					IF sLbData[i].iTimeSurvived < sLbData[j].iTimeSurvived
					AND	sLbData[j].iTimeSurvived != -1
						sLbDataTemp = sLbData[i]
						sLbData[i] = sLbData[j]
						sLbData[j] = sLbDataTemp	
					ENDIF	
				ENDFOR
			ENDFOR
			
			#IF IS_DEBUG_BUILD
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_MostWantedLeaderboardDebug")
				INT d
				FOR d = 0 TO ciNUM_GANG_MEMBERS-1
					IF sLbData[d].PlayerId != INVALID_PLAYER_INDEX()
						CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR ===  [DRAW_UI_TIMER] - sLbData[", d, "].PlayerId = ", GET_PLAYER_NAME(sLbData[d].PlayerId))
						CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR ===  [DRAW_UI_TIMER] - sLbData[", d, "].iTimeSurvived = ", sLbData[d].iTimeSurvived)
					ELSE
						CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR ===  [DRAW_UI_TIMER] - sLbData[", d, "].PlayerId =  INVALID_PLAYER_INDEX()")
						CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR ===  [DRAW_UI_TIMER] - sLbData[", d, "].iTimeSurvived = ", sLbData[d].iTimeSurvived)
					ENDIF
					CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR ===")
				ENDFOR
			ENDIF
			#ENDIF
			
			IF iCurrentTimer > 0
				BOTTOM_RIGHT_UI_1TIME_2TIME_3TIME_4TIME_MYTIME_TIMER(sLbData[0].PlayerId, sLbData[1].PlayerId, sLbData[2].PlayerId, sLbData[3].PlayerId,
																	sLbData[0].iTimeSurvived, sLbData[1].iTimeSurvived, sLbData[2].iTimeSurvived, sLbData[3].iTimeSurvived,
																	iLocalTimeSurvived, iCurrentTimer,
																	bForceRebuild, DEFAULT, timeColour, "GB_SFS_SRVTIME", strLabel,
																	DEFAULT, DEFAULT, DEFAULT, DEFAULT, GB_SHOULD_RUN_SPEC_CAM())
			ELSE												  
				BOTTOM_RIGHT_UI_1TIME_2TIME_3TIME_4TIME_MYTIME_TIMER(sLbData[0].PlayerId, sLbData[1].PlayerId, sLbData[2].PlayerId, sLbData[3].PlayerId,
																	sLbData[0].iTimeSurvived, sLbData[1].iTimeSurvived, sLbData[2].iTimeSurvived, sLbData[3].iTimeSurvived,
																	iLocalTimeSurvived, 0,
																	bForceRebuild, DEFAULT, timeColour, "GB_SFS_SRVTIME", strLabel,
																	DEFAULT, DEFAULT, DEFAULT, DEFAULT, GB_SHOULD_RUN_SPEC_CAM())											  
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_SHARD_HELP_INTRO()
	IF GB_SHOULD_UI_BE_HIDDEN()
		EXIT
	ENDIF
	
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_JOINED_LATE)
		IF NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
			STRING txtJoinedLateHelp = "GB_SFS_HLATE"
			
			IF bIsThisABikerVariation
				txtJoinedLateHelp = "GB_OTR_HLATE"
			ENDIF
			
			PRINT_HELP_NO_SOUND(txtJoinedLateHelp)
			SET_BIT(iLocalBitset, GBFIVESTAR_BS_SHARD_DONE)
			SET_BIT(iLocalBitset, GBFIVESTAR_BS_INTRO_DONE)
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_SHARD_DONE)
			IF NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
				// The Most Wanted Challenge has started. Compete against your Organization to survive the longest with a five star Wanted Level to earn cash, RP and JP rewards.
				STRING txtStartHelp = "GB_SFS_START"

				IF bIsThisABikerVariation
					txtStartHelp = "GB_OTR_START"
				ENDIF
			
				PRINT_HELP_NO_SOUND(txtStartHelp)
				GB_SET_GANG_BOSS_HELP_BACKGROUND()
				CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [HANDLE_SHARD_HELP_INTRO] - GBFIVESTAR_SURVIVE - Printing start help.")
				
				SET_BIT(iLocalBitset, GBFIVESTAR_BS_INTRO_DONE)
			ENDIF
		ELSE
			STRING txtStartTitle = "GB_SFS_TSTART"
			STRING txtStartStrapline = "GB_SFS_SSTART"

			IF bIsThisABikerVariation
				txtStartTitle = "GB_OTR_TSTART"
				txtStartStrapline = "GB_OTR_SSTART"
			ENDIF
				
			SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_START_OF_JOB, txtStartTitle, txtStartStrapline)
			
			SET_BIT(iLocalBitset, GBFIVESTAR_BS_SHARD_DONE)
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_START_TIMER)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [HANDLE_SHARD_HELP_INTRO] - GBFIVESTAR_SURVIVE - Display start shard.")
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_KNOCKOUT()
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_KNOCKED_OUT)
	AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_JOINED_LATE)
	AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_BOSS_SPEC_KO)
	AND NOT GB_SHOULD_RUN_SPEC_CAM()
	AND NOT IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_KNOCKOUT_DONE)
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_INTERIOR_KO)
		AND IS_NET_PLAYER_OK(PLAYER_ID())
			STRING txtStartTitle = "GB_SFS_TKNOCK"
			STRING txtStartStrapline = "GB_SFS_SKNOCK"

			IF bIsThisABikerVariation
				txtStartTitle = "GB_SFS_TKNOCK"
				IF IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_BIKE_DESTROYED)
					txtStartStrapline = "GB_OTR_SKNOCKBK"
				ELSE
					txtStartStrapline = "GB_OTR_SKNOCK"
				ENDIF
			ENDIF
			
			Clear_Any_Objective_Text_From_This_Script()
			SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_GENERIC, txtStartTitle, txtStartStrapline)
			SET_BIT(iLocalBitset, GBFIVESTAR_BS_KNOCKOUT_DONE)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [HANDLE_KNOCKOUT] - GBFIVESTAR_SURVIVE - Local player knocked out. Showing shard.")
		ENDIF
		
		IF NOT SHOULD_SKIP_KNOCKOUT_SHARD()
		AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_IN_INTERIOR)
		AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_INTERIOR_KO)
		AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_JOINED_LATE)
		AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_BOSS_SPEC_KO)
		AND NOT GB_SHOULD_RUN_SPEC_CAM()
			IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_WASTED)
				IF HAS_NET_TIMER_EXPIRED(stKillstripTimer, 4000)
					Clear_Any_Objective_Text_From_This_Script()
					SET_GAME_STATE_ON_DEATH(AFTERLIFE_SET_SPECTATORCAM)
					GB_SET_ITS_SAFE_FOR_SPEC_CAM()
					GB_SET_RUN_SPEC_CAM()
					SET_BIT(iLocalBitset, GBFIVESTAR_BS_KNOCKOUT_DONE)
					CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [HANDLE_KNOCKOUT] - GBFIVESTAR_SURVIVE - Local player knocked out. Go to spectate.")
				ENDIF
			ENDIF
		ELSE
			SET_BIT(iLocalBitset, GBFIVESTAR_BS_KNOCKOUT_DONE)
		ENDIF
	ENDIF

//	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_KNOCKED_OUT)
//	AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_JOINED_LATE)
//		IF NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
//			GB_SET_SPEC_CAM_ADD_CONTEXT_AGAIN()
//		ENDIF
//	ENDIF
ENDPROC

PROC HANDLE_SHARD_OUTRO()
	IF GB_SHOULD_UI_BE_HIDDEN()
		SET_BIT(iLocalBitset, GBFIVESTAR_BS_END_SHARD_DONE)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_FINAL_LIST_INIT)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [HANDLE_SHARD_OUTRO] - GBFIVESTAR_REWARDS - SERVER_BS_FINAL_LIST_INIT not set. Waiting.")
		EXIT
	ENDIF 
	
	IF Is_There_Any_Current_Objective_Text()
		Clear_Any_Objective_Text_From_This_Script()
		CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [HANDLE_SHARD_OUTRO] - GBFIVESTAR_REWARDS - Clearing objective text.")
	ENDIF
	
	IF IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_END_SHARD_DONE)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [HANDLE_SHARD_OUTRO] - GBFIVESTAR_REWARDS - Shard already done.")
		EXIT
	ENDIF
	
	STRING txtStartTitle
	STRING txtStartStrapline
	
	// Your Boss left the session
	IF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_BOSS_LEFT)
		SET_BIT(iLocalBitset, GBFIVESTAR_BS_END_SHARD_DONE)
	// Nobody won. Usually means final player was in an interior	
	ELIF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_NO_WINNERS)
		txtStartTitle = "GB_CHAL_OVER"
		txtStartStrapline = "GB_SFS_SNOWIN"
		
		IF bIsThisABikerVariation
			txtStartTitle = "GB_CHAL_OVER"
			txtStartStrapline = "GB_OTR_SNOWIN"
		ENDIF
		
		SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, txtStartTitle, txtStartStrapline)
		SET_BIT(iLocalBitset, GBFIVESTAR_BS_END_SHARD_DONE)
	// If the timer expired...
	ELSE
		// If the local player is not knocked out, seperate him from the others for shards (prevent their own name appearing).
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_KNOCKED_OUT)
		AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_JOINED_LATE)
			INT iIndex, iNum
			REPEAT 4 iIndex
				IF serverBD.piRemainingPlayers[iIndex] <> PLAYER_ID()
				AND serverBD.piRemainingPlayers[iIndex] <> INVALID_PLAYER_INDEX()
					piOtherPlayers[iNum] = serverBD.piRemainingPlayers[iIndex]
					CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [HANDLE_SHARD_OUTRO] - GBFIVESTAR_REWARDS - ", GET_PLAYER_NAME(piOtherPlayers[iNum]) , " put in array position ", iNum, " from index ", iIndex)
					iNum++
				ENDIF
			ENDREPEAT
		ENDIF
	
		// Get the number of remaining players and decide on which shard to show, depending on number remaining and whether player was knocked out.
		CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [HANDLE_SHARD_OUTRO] - GBFIVESTAR_REWARDS - GBFIVESTAR_GET_NUM_PLAYERS_REMAINING = ", GBFIVESTAR_GET_NUM_PLAYERS_REMAINING())
		SWITCH GBFIVESTAR_GET_NUM_PLAYERS_REMAINING()
			CASE 1
				// You won the Most Wanted Challenge by surviving the longest
				IF PLAYER_ID() = GBFIVESTAR_GET_WINNER()
					txtStartTitle = "GB_WINNER"
					txtStartStrapline = "GB_SFS_SWON"

					IF bIsThisABikerVariation
						txtStartTitle = "GB_WINNER"
						txtStartStrapline = "GB_OTR_SWON"
					ENDIF
					
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, txtStartTitle, txtStartStrapline)
					CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [HANDLE_SHARD_OUTRO] - GBFIVESTAR_REWARDS - Local player won.")
				
				// <C>~a~</C> won the Most Wanted Challenge by surviving the longest
				ELSE
					txtStartTitle = "GB_CHAL_OVER"
					txtStartStrapline = "GB_SFS_SLOST"

					IF bIsThisABikerVariation
						txtStartTitle = "GB_CHAL_OVER"
						txtStartStrapline = "GB_OTR_SLOST"
					ENDIF
					
					SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GB_END_OF_JOB_OVER, GBFIVESTAR_GET_WINNER(), -1, txtStartStrapline, txtStartTitle)
					CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [HANDLE_SHARD_OUTRO] - GBFIVESTAR_REWARDS - ", GET_PLAYER_NAME(GBFIVESTAR_GET_WINNER()) , " won.")
					
				ENDIF
			BREAK
			CASE 2
				// <C>~a~</C> and <C>~a~</C> tied to win the Most Wanted Challenge
				IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_KNOCKED_OUT)
				OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_JOINED_LATE)
				OR (IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_IN_INTERIOR)
				 AND NOT GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()   )
				
					txtStartTitle = "GB_CHAL_OVER"
					txtStartStrapline = "GB_SFS_SLOST2"

					IF bIsThisABikerVariation
						txtStartTitle = "GB_CHAL_OVER"
						txtStartStrapline = "GB_OTR_SLOST2"
					ENDIF
					
					SETUP_NEW_BIG_MESSAGE_WITH_2_PLAYERS(BIG_MESSAGE_GB_END_OF_JOB_OVER, 
														serverBD.piRemainingPlayers[0], serverBD.piRemainingPlayers[1], 
														-1, txtStartStrapline, txtStartTitle)
														
				// You tied with <C>~a~</C> to win the Most Wanted Challenge
				ELSE
					txtStartTitle = "GB_CHAL_OVER"
					txtStartStrapline = "GB_SFS_STIE"

					IF bIsThisABikerVariation
						txtStartTitle = "BK_DRAW"
						txtStartStrapline = "GB_OTR_STIE"
					ENDIF
					
					SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GB_END_OF_JOB_OVER, 
														piOtherPlayers[0], 
														-1, txtStartStrapline, txtStartTitle)
														
				ENDIF
			BREAK
			
			CASE 3
				// <C>~a~</C>, <C>~a~</C> and <C>~a~</C> tied to win the Most Wanted Challenge
				IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_KNOCKED_OUT)
				OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_JOINED_LATE)
				OR (IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_IN_INTERIOR)
				 AND NOT GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()   )
					txtStartTitle = "GB_CHAL_OVER"
					txtStartStrapline = "GB_SFS_SLOST3"

					IF bIsThisABikerVariation
						txtStartTitle = "GB_CHAL_OVER"
						txtStartStrapline = "GB_OTR_SLOST3"
					ENDIF
					
					SETUP_NEW_BIG_MESSAGE_WITH_3_PLAYERS(BIG_MESSAGE_GB_END_OF_JOB_OVER, 
														serverBD.piRemainingPlayers[0], serverBD.piRemainingPlayers[1], serverBD.piRemainingPlayers[2], 
														-1, txtStartStrapline, txtStartTitle)
														
				// You tied with <C>~a~</C> and <C>~a~</C> to win the Most Wanted Challenge
				ELSE
					txtStartTitle = "GB_CHAL_OVER"
					txtStartStrapline = "GB_SFS_STIE2"

					IF bIsThisABikerVariation
						txtStartTitle = "BK_DRAW"
						txtStartStrapline = "GB_OTR_STIE2"
					ENDIF
					
					SETUP_NEW_BIG_MESSAGE_WITH_2_PLAYERS(BIG_MESSAGE_GB_END_OF_JOB_OVER, 
														piOtherPlayers[0], piOtherPlayers[1],
														-1, txtStartStrapline, txtStartTitle, DEFAULT, -1, DEFAULT)
																					
				ENDIF
			BREAK
			
			CASE 4
				// Other members of your Motorcycle Club tied to win the On The Run Challenge
				IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_KNOCKED_OUT)
				OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_JOINED_LATE)
				OR (IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_IN_INTERIOR)
				 AND NOT GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()   )
					IF bIsThisABikerVariation
						txtStartTitle = "GB_CHAL_OVER"
						
						IF GBFIVESTAR_GET_NUM_PLAYERS_REMAINING() = GB_GET_NUM_GOONS_IN_LOCAL_GANG()
							txtStartStrapline = "GB_OTR_STIEREST"	// The rest of your Motorcycle Club tied to win the On The Run Challenge
							CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [HANDLE_SHARD_OUTRO] - GBFIVESTAR_REWARDS - Everyone won except you.")
						ELSE
							txtStartStrapline = "GB_OTR_STIEOTHR"	// Other members of your Motorcycle Club tied to win the On The Run Challenge
							CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [HANDLE_SHARD_OUTRO] - GBFIVESTAR_REWARDS - Some members tied, but not you.")
						ENDIF
					
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, txtStartTitle, txtStartStrapline)
					ENDIF
														
				
				ELSE
					txtStartTitle = "GB_CHAL_OVER"
					txtStartStrapline = "GB_SFS_STIE3"

					IF bIsThisABikerVariation
						txtStartTitle = "BK_DRAW"
						txtStartStrapline = "GB_OTR_STIE3"
					ENDIF
					
					// You tied with <C>~a~</C>, <C>~a~</C> and <C>~a~</C> to win the Most Wanted Challenge
					SETUP_NEW_BIG_MESSAGE_WITH_3_PLAYERS(BIG_MESSAGE_GB_END_OF_JOB_OVER, 
														piOtherPlayers[0], piOtherPlayers[1], piOtherPlayers[2],
														-1, txtStartStrapline, txtStartTitle)
				ENDIF
																				
			BREAK
			CASE 5
			CASE 6
			CASE 7
				// Other members of your Motorcycle Club tied to win the On The Run Challenge
				IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_KNOCKED_OUT)
				OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_JOINED_LATE)
				OR (IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_IN_INTERIOR)
				 AND NOT GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()   )
					IF bIsThisABikerVariation
						txtStartTitle = "GB_CHAL_OVER"
						
						IF GBFIVESTAR_GET_NUM_PLAYERS_REMAINING() = GB_GET_NUM_GOONS_IN_LOCAL_GANG()
							txtStartStrapline = "GB_OTR_STIEREST"	// The rest of your Motorcycle Club tied to win the On The Run Challenge
							CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [HANDLE_SHARD_OUTRO] - GBFIVESTAR_REWARDS - Everyone won except you.")
						ELSE
							txtStartStrapline = "GB_OTR_STIEOTHR"	// Other members of your Motorcycle Club tied to win the On The Run Challenge
							CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [HANDLE_SHARD_OUTRO] - GBFIVESTAR_REWARDS - Some members tied, but not you.")
						ENDIF
					
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, txtStartTitle, txtStartStrapline)
					ENDIF
														
				
				ELSE
					IF bIsThisABikerVariation
						txtStartTitle = "BK_DRAW"
						txtStartStrapline = ""
						
						IF GBFIVESTAR_GET_NUM_PLAYERS_REMAINING() = (GB_GET_NUM_GOONS_IN_LOCAL_GANG()+1)
							txtStartStrapline = "GB_OTR_STIEURST"	// You tied with the rest of your Motorcycle Club to win the On The Run Challenge
							CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [HANDLE_SHARD_OUTRO] - GBFIVESTAR_REWARDS - All gang members survived.")
						ELSE
							txtStartStrapline = "GB_OTR_STIEUOT"	// You tied with other members of your Motorcycle Club to win the On The Run Challenge
							CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [HANDLE_SHARD_OUTRO] - GBFIVESTAR_REWARDS - You tied with some of your motorcycle club.")
						ENDIF
					
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, txtStartTitle, txtStartStrapline)
					ENDIF
																					
				ENDIF
			BREAK
			CASE 8
				// You tied with the rest of your Motorcycle Club to win the On The Run Challenge
				IF bIsThisABikerVariation
					txtStartTitle = "BK_DRAW"
					txtStartStrapline = "GB_OTR_STIEURST"
					
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, txtStartTitle, txtStartStrapline)
					CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [HANDLE_SHARD_OUTRO] - GBFIVESTAR_REWARDS - All gang members survived.")
				ENDIF
			BREAK
		ENDSWITCH
		SET_BIT(iLocalBitset, GBFIVESTAR_BS_END_SHARD_DONE)
	ENDIF
ENDPROC

PROC MAINTAIN_COUNTDOWN_AUDIO()
	IF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_WORK_STARTED)
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_IN_INTERIOR)
		AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_KNOCKED_OUT)
		AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_JOINED_LATE)
			IF NOT IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_TIMEOUT_PLAYING)
				BOOL bOffBike = FALSE
				
				IF IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_NOT_IN_VEHICLE)
				OR IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_IN_WRONG_VEHICLE)
					bOffBike = TRUE
				ENDIF
			
				IF iCurrentTime < GB_GET_INTERIOR_TIMEOUT(  bOffBike  )
					iTimerCount = (iCurrentTime - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stInteriorTimeout))
				ELSE
					iTimerCount = (GB_GET_INTERIOR_TIMEOUT(  bOffBike  ) - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stInteriorTimeout))
				ENDIF
				
				iInteriorSound = GET_SOUND_ID()
				PLAY_SOUND_FRONTEND(iInteriorSound, "Out_of_Bounds", "MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
				SET_VARIABLE_ON_SOUND(iInteriorSound, "Time", (TO_FLOAT(iTimerCount)/1000.0))
				SET_BIT(iLocalBitset, GBFIVESTAR_BS_TIMEOUT_PLAYING)
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_COUNTDOWN_PLAYING)
			IF iCurrentTime <= 5000
				iCountdownSound = GET_SOUND_ID()
				PLAY_SOUND_FRONTEND(iCountdownSound, "5s", "MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
				SET_BIT(iLocalBitset, GBFIVESTAR_BS_COUNTDOWN_PLAYING)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_OBJECTIVE_TEXT_AND_TIMER()
	IF GB_SHOULD_UI_BE_HIDDEN()
		IF Is_There_Any_Current_Objective_Text()
			Clear_Any_Objective_Text_From_This_Script()
			CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [MAINTAIN_OBJECTIVE_TEXT_AND_TIMER] - GB_SHOULD_UI_BE_HIDDEN - Clearing objective text.")
		ENDIF
		EXIT
	ENDIF
	
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_IN_INTERIOR)
	AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_KNOCKED_OUT)
	AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_JOINED_LATE)
	AND GET_SERVER_MISSION_STAGE() < GBFIVESTAR_REWARDS
	
		BOOL bOffBike = FALSE
	
		IF IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_ON_YACHT)
			Print_Objective_Text("GB_SFS_YACHT")
		ELIF IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_IN_AIR_VEHICLE)
			Print_Objective_Text("GB_SFS_AIRCRAFT")
		ELIF IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_IN_SUB)
			Print_Objective_Text("GB_SFS_SUBMAR")
		ELIF IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_USING_PARACHUTE)
			Print_Objective_Text("GB_SFS_PARA")
		ELIF IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_NOT_IN_VEHICLE)
		OR IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_IN_WRONG_VEHICLE)
			Print_Objective_Text("GB_OTR_BCK2BKE")
			bOffBike = TRUE
		ELSE
			Print_Objective_Text("GB_SFS_RETURN")
		ENDIF
		
		IF iCurrentTime < GB_GET_INTERIOR_TIMEOUT(  bOffBike  )
			DRAW_UI_TIMER(stInteriorTimeout, iCurrentTime)
		ELSE
			DRAW_UI_TIMER(stInteriorTimeout, GB_GET_INTERIOR_TIMEOUT(  bOffBike  ))
		ENDIF
	ELSE
		iCurrentTime = (GET_CHALLENGE_DURATION() - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.stChallengeTimer))
		
		IF IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_TIMEOUT_PLAYING)
			STOP_SOUND(iInteriorSound)
			CLEAR_BIT(iLocalBitset, GBFIVESTAR_BS_TIMEOUT_PLAYING)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [MAINTAIN_OBJECTIVE_TEXT_AND_TIMER] - Stop playing timeout audio.")
		ENDIF
		
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_KNOCKED_OUT)
		AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_JOINED_LATE)
		OR GB_SHOULD_RUN_SPEC_CAM()
			STRING txtObjectiveText = "GB_SFS_SURVIVE"

			IF bIsThisABikerVariation
				txtObjectiveText = "GB_OTR_SURVIVE"
			ENDIF
					
			Print_Objective_Text(txtObjectiveText)
		ELSE
			IF Is_There_Any_Current_Objective_Text()
				Clear_Any_Objective_Text_From_This_Script()
				CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [MAINTAIN_OBJECTIVE_TEXT_AND_TIMER] - GB_SHOULD_UI_BE_HIDDEN - Clearing objective text as knocked out and not spectator.")
			ENDIF
		ENDIF
		DRAW_UI_TIMER(serverBD.stChallengeTimer, GET_CHALLENGE_DURATION())
	ENDIF
ENDPROC

PROC POPULATE_PARTICIPANT_LIST()
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_PARTICIPANTS_SAVED)
		INT iCount, iIndex
		PLAYER_INDEX piPlayer
		PARTICIPANT_INDEX piPart
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iIndex
			piPart = INT_TO_PARTICIPANTINDEX(iIndex)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(piPart)
				piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)
				IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(piPlayer, GB_GET_LOCAL_PLAYER_MISSION_HOST())
				AND NOT IS_BIT_SET(playerBD[iIndex].iClientBitSet, CLIENT_BS_JOINED_LATE)
					serverBD.piPlayers[iCount] = piPlayer
					CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [POPULATE_PARTICIPANT_LIST] - Participant ", iIndex, " = ", GET_PLAYER_NAME(piPlayer), " - iCount = ", iCount)
					
					iCount++
				ENDIF
			ENDIF
		ENDREPEAT
		SET_BIT(serverBD.iServerBitSet, SERVER_BS_PARTICIPANTS_SAVED)
	ENDIF
ENDPROC

/// PURPOSE: SERVER ONLY: Used to check client states.
PROC MAINTAIN_ACTIVE_PLAYER_CHECKS()
	INT iCount, iFinalCount
	
	INT iIndex
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iIndex
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iIndex))
			IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(INT_TO_PLAYERINDEX(iIndex), GB_GET_LOCAL_PLAYER_MISSION_HOST())
				IF GET_SERVER_MISSION_STAGE() < GBFIVESTAR_REWARDS
					IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(iIndex), FALSE)
						IF NOT IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_FINAL_LIST_INIT)
						
							IF NOT IS_BIT_SET(playerBD[iIndex].iClientBitSet, CLIENT_BS_LOCAL_KNOCKED_OUT)
							AND NOT IS_BIT_SET(playerBD[iIndex].iClientBitSet, CLIENT_BS_JOINED_LATE)
								serverBD.piRemainingPlayers[iCount] = INT_TO_PLAYERINDEX(iIndex)
								CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [MAINTAIN_ACTIVE_PLAYER_CHECKS] - Remaining player = ", GET_PLAYER_NAME(serverBD.piRemainingPlayers[iCount]))
								
								iCount++
								CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [MAINTAIN_ACTIVE_PLAYER_CHECKS] - Players remaining = ", iCount)
							ENDIF
						ENDIF
						
						IF IS_BIT_SET(playerBD[iIndex].iClientBitSet, CLIENT_BS_START_TIMER)
							SET_BIT(serverBD.iServerBitSet, SERVER_BS_WORK_STARTED)
						ENDIF
					ENDIF
					
					IF IS_BIT_SET(playerBD[iIndex].iClientBitSet, CLIENT_BS_TIME_UP)
						SET_BIT(serverBD.iServerBitSet, SERVER_BS_TIME_UP)
					ENDIF
				ELIF GET_SERVER_MISSION_STAGE() = GBFIVESTAR_REWARDS
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_FINAL_LIST_INIT)
						IF NOT IS_BIT_SET(playerBD[iIndex].iClientBitSet, CLIENT_BS_LOCAL_KNOCKED_OUT)
						AND NOT IS_BIT_SET(playerBD[iIndex].iClientBitSet, CLIENT_BS_JOINED_LATE)
							IF NOT IS_BIT_SET(playerBD[iIndex].iClientBitSet, CLIENT_BS_LOCAL_IN_INTERIOR)
							OR GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
								serverBD.piRemainingPlayers[iFinalCount] = INT_TO_PLAYERINDEX(iIndex)
								CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [MAINTAIN_ACTIVE_PLAYER_CHECKS] - Final remaining player = ", GET_PLAYER_NAME(serverBD.piRemainingPlayers[iFinalCount]))
								
								iFinalCount++
								GBFIVESTAR_SET_NUM_PLAYERS_REMAINING(iFinalCount)
								
								CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [MAINTAIN_ACTIVE_PLAYER_CHECKS] - Final players remaining = ", iFinalCount)
							ENDIF
						ENDIF
					ENDIF
				
					IF IS_BIT_SET(playerBD[iIndex].iClientBitSet, CLIENT_BS_OUTRO_FINISHED)
						iCount++
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Check all players are in the same state.
	IF GET_SERVER_MISSION_STAGE() = GBFIVESTAR_SURVIVE
		GBFIVESTAR_SET_NUM_PLAYERS_REMAINING(iCount)
		IF iCount = 1
			IF IS_BIT_SET(playerBD[NATIVE_TO_INT(GBFIVESTAR_GET_WINNER())].iClientBitSet, CLIENT_BS_LOCAL_IN_INTERIOR)
			AND NOT GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
				SET_BIT(serverBD.iServerBitSet, SERVER_BS_NO_WINNERS)
			ELSE
				SET_BIT(serverBD.iServerBitSet, SERVER_BS_GOON_WON)
				CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [MAINTAIN_ACTIVE_PLAYER_CHECKS] - Winner = ", GET_PLAYER_NAME(GBFIVESTAR_GET_WINNER()))
			ENDIF
		ENDIF
	ELIF GET_SERVER_MISSION_STAGE() = GBFIVESTAR_REWARDS
		SET_BIT(serverBD.iServerBitSet, SERVER_BS_FINAL_LIST_INIT)
		
		IF iCount = NETWORK_GET_NUM_PARTICIPANTS()
			SET_BIT(serverBD.iServerBitSet, SERVER_BS_OUTRO_FINISHED)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [MAINTAIN_ACTIVE_PLAYER_CHECKS] - SERVER_BS_OUTRO_FINISHED = SET")
		ELSE
			IF HAS_NET_TIMER_EXPIRED(serverBD.stRewardTimeout, GET_TIME_FOR_BIG_MESSAGE(BIG_MESSAGE_GB_GENERIC)+ 6000)
				SET_BIT(serverBD.iServerBitSet, SERVER_BS_OUTRO_FINISHED)
				CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [MAINTAIN_ACTIVE_PLAYER_CHECKS] - SERVER_BS_OUTRO_FINISHED = SET. stRewardTimeout expired after waiting ", (REWARD_TIMEOUT/1000), " seconds.")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_WARN_NEAR_YACHT()
	IF NOT IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_YACHT_WARN)
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_JOINED_LATE)
		AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_KNOCKED_OUT)
			IF IS_PLAYER_NEAR_ANY_YACHT(PLAYER_ID())
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					// Yachts are off limits during the Most Wanted Challenge. You will not be able to compete if you are on it.
					STRING txtObjectiveText = "GB_SFS_HYACHT"

					IF bIsThisABikerVariation
						txtObjectiveText = "GB_OTR_HYACHT"
					ENDIF
			
					PRINT_HELP_NO_SOUND(txtObjectiveText) 
					GB_SET_GANG_BOSS_HELP_BACKGROUND()
					
					SET_BIT(iLocalBitset, GBFIVESTAR_BS_YACHT_WARN)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_IN_UNSUITABLE_VEHICLE()
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
		OR IS_PED_IN_ANY_SUB(PLAYER_PED_ID())
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PLAYER_TIMEOUT(INTERIOR_INSTANCE_INDEX interiorIndex)
	IF (interiorIndex <> NULL
	AND NOT IS_INTERIOR_A_ROAD_TUNNEL(interiorIndex))
		CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [SHOULD_PLAYER_TIMEOUT] - TRUE - Player is a non-road tunnel.")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_ON_ANY_YACHT(PLAYER_ID())
		CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [SHOULD_PLAYER_TIMEOUT] - TRUE - Player is on a Yacht.")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_IN_UNSUITABLE_VEHICLE()
		CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [SHOULD_PLAYER_TIMEOUT] - TRUE - Player is in an unsuitable vehicle.")
		RETURN TRUE
	ENDIF
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
	AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
				CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [BIKER] [SHOULD_PLAYER_TIMEOUT] - TRUE - Player is not in a vehicle on biker variation.")
				RETURN TRUE
			ENDIF
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
			AND GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE) != viLocalPlayersBike
				CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [BIKER] [SHOULD_PLAYER_TIMEOUT] - TRUE - Player is not on their bike on biker variation.")
				RETURN TRUE
			ENDIF
		ENDIF
	
		IF GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_PARACHUTING
			CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [SHOULD_PLAYER_TIMEOUT] - TRUE - Player is parachuting.")
			RETURN TRUE
		ENDIF
	
		VECTOR vShackPos1 = <<454.351440,5580.700195,784.183167>>
		VECTOR vShackPos2 = <<454.246613,5562.972656,780.184082>> 
		FLOAT fShackWidth = 6.250000
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vShackPos1, vShackPos2, fShackWidth)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [SHOULD_PLAYER_TIMEOUT] - TRUE - Player is in the naughty shack.")
			RETURN TRUE
		ENDIF
	
		VECTOR vSawMillRoomPos1 = <<-565.732056,5300.804199,71.973228>>
		VECTOR vSawMillRoomPos2 = <<-553.114319,5334.937988,80.427750>> 
		FLOAT fSawMillRoomWidth = 15.000000
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vSawMillRoomPos1, vSawMillRoomPos2, fSawMillRoomWidth)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [SHOULD_PLAYER_TIMEOUT] - TRUE - Player is in saw mill.")
			RETURN TRUE
		ENDIF
		
		VECTOR vSawMillSmallTunnelPos1 = <<-557.923767,5287.209473,74.920731>>
		VECTOR vSawMillSmallTunnelPos2 = <<-561.968323,5288.791016,77.545723>> 
		FLOAT fSawMillSmallTunnelWidth = 3.250000
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vSawMillSmallTunnelPos1, vSawMillSmallTunnelPos2, fSawMillSmallTunnelWidth)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [SHOULD_PLAYER_TIMEOUT] - TRUE - Player is in saw mill tiny tunnel.")
			RETURN TRUE
		ENDIF
		
		VECTOR vSawMillTunnelPos1 = <<-506.418091,5315.669434,84.533051>>
		VECTOR vSawMillTunnelPos2 = <<-547.272400,5330.761719,75.466728>> 
		FLOAT fSawMillTunnelWidth = 4.750000
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vSawMillTunnelPos1, vSawMillTunnelPos2, fSawMillTunnelWidth)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [SHOULD_PLAYER_TIMEOUT] - TRUE - Player is in saw mill tunnel.")
			RETURN TRUE
		ENDIF
	
	ENDIF
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_INTERIOR_TIMEOUT()
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_JOINED_LATE)
		INTERIOR_INSTANCE_INDEX interiorIndex = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
		
		IF SHOULD_PLAYER_TIMEOUT(interiorIndex)
			RESET_NET_TIMER(stInteriorDelay)
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_IN_INTERIOR)
			
			BOOL bOffBike = FALSE
			
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				IF IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
					SET_BIT(iLocalBitset, GBFIVESTAR_BS_IN_AIR_VEHICLE)
				ELIF IS_PED_IN_ANY_SUB(PLAYER_PED_ID())
					SET_BIT(iLocalBitset, GBFIVESTAR_BS_IN_SUB)
				ELIF IS_PLAYER_ON_ANY_YACHT(PLAYER_ID())
					SET_BIT(iLocalBitset, GBFIVESTAR_BS_ON_YACHT)
				ELIF GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_PARACHUTING
					SET_BIT(iLocalBitset, GBFIVESTAR_BS_USING_PARACHUTE)
				ELIF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
						SET_BIT(iLocalBitset, GBFIVESTAR_BS_NOT_IN_VEHICLE)
						bOffBike = TRUE
					ENDIF
					
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
					AND GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE) != viLocalPlayersBike
						SET_BIT(iLocalBitset, GBFIVESTAR_BS_IN_WRONG_VEHICLE)
						bOffBike = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF HAS_NET_TIMER_EXPIRED(stInteriorTimeout, GB_GET_INTERIOR_TIMEOUT(  bOffBike  ))
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_INTERIOR_KO)
			ENDIF
		ELSE
			CLEAR_BIT(iLocalBitset, GBFIVESTAR_BS_IN_SUB)
			CLEAR_BIT(iLocalBitset, GBFIVESTAR_BS_IN_AIR_VEHICLE)
			CLEAR_BIT(iLocalBitset, GBFIVESTAR_BS_ON_YACHT)
			CLEAR_BIT(iLocalBitset, GBFIVESTAR_BS_USING_PARACHUTE)
			CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_IN_INTERIOR)
			IF HAS_NET_TIMER_EXPIRED(stInteriorDelay, 5000)
				RESET_NET_TIMER(stInteriorTimeout)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: SERVER ONLY: Check it's okay to start the hit.
FUNC BOOL SHOULD_JOB_START()
	IF HAS_NET_TIMER_EXPIRED(stStartDelay, 1500)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [SHOULD_JOB_START] - PASS - Goons: ", GB_GET_NUM_GOONS_IN_PLAYER_GANG(GB_GET_LOCAL_PLAYER_MISSION_HOST()), " - Participants: ", NETWORK_GET_NUM_PARTICIPANTS())
		RETURN TRUE
	ENDIF
	
	IF HAS_NET_TIMER_EXPIRED(stStartDelay, 15000)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [SHOULD_JOB_START] - FAIL - 15 seconds has passed without any goon coming on script. Cleaning up.")
		SET_SERVER_MISSION_STATE(GAME_STATE_END)
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE: Check if the job should end.
PROC MAINTAIN_END_CONDITION_CHECKS()
	IF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_WORK_STARTED)
		IF HAS_NET_TIMER_EXPIRED(serverBD.stChallengeTimer, GET_CHALLENGE_DURATION())
			SET_BIT(serverBD.iServerBitSet, SERVER_BS_TIME_UP)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_TIME_UP)
		SET_SERVER_MISSION_END_REASON(GBFIVESTAR_ENDREASON_TIMEUP)
		SET_SERVER_MISSION_STAGE(GBFIVESTAR_REWARDS)
	ENDIF

	IF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_GOON_WON)
		SET_SERVER_MISSION_END_REASON(GBFIVESTAR_ENDREASON_GOONWON)
		SET_SERVER_MISSION_STAGE(GBFIVESTAR_REWARDS)
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_NO_WINNERS)
		SET_SERVER_MISSION_END_REASON(GBFIVESTAR_ENDREASON_NOWINNERS)
		SET_SERVER_MISSION_STAGE(GBFIVESTAR_REWARDS)
	ENDIF
	
	IF NOT IS_NET_PLAYER_OK(GB_GET_LOCAL_PLAYER_MISSION_HOST(), FALSE, FALSE)
		SET_BIT(serverBD.iServerBitSet, SERVER_BS_BOSS_LEFT)
		SET_SERVER_MISSION_END_REASON(GBFIVESTAR_ENDREASON_BOSS_LEFT)
		SET_SERVER_MISSION_STAGE(GBFIVESTAR_REWARDS)
	ENDIF
ENDPROC

/// PURPOSE: Give out rewards when hit is over.
PROC GIVE_REWARDS()
	BOOL bWinner = FALSE
	GANG_BOSS_MANAGE_REWARDS_DATA structGangBossManageRewardsData
	
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_REWARDS_GIVEN)
	
		IF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_NO_WINNERS)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [GIVE_REWARDS] - Nobody won. Someone must have been in an interior at the end.")
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_REWARDS_GIVEN)
			EXIT
		ENDIF
		
		
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_IN_INTERIOR)
		AND NOT GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
			CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [GIVE_REWARDS] - Local player in an interior at the end. No reward.")
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_REWARDS_GIVEN)
			EXIT
		ENDIF
	
		IF GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE()
			CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [GIVE_REWARDS] - Local player is passive. No reward.")
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_REWARDS_GIVEN)
			EXIT
		ENDIF
		
		IF GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
			CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [GIVE_REWARDS] - Local player is hiding boss work. No reward.")
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_REWARDS_GIVEN)
			EXIT
		ENDIF
		
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_KNOCKED_OUT)
		AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_JOINED_LATE)
		AND NOT IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_BOSS_LEFT)
			bWinner = TRUE
		ENDIF
		
		INT iBonusCash
		// If they are the winner deal with bonus cash
		IF bWinner
			INT iTimeMs = GET_ABS_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stSurvivalStartTime)
			INT iMins = FLOOR((TO_FLOAT(iTimeMs) * 0.001) / 60)
			
			CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [GIVE_REWARDS] - Local player survived for: ", iMins, " minutes")
			
			IF iMins > g_sMPTunables.igb_mostwanted_bonus_cash_max_time
				iMins = g_sMPTunables.igb_mostwanted_bonus_cash_max_time
				CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [GIVE_REWARDS] - Cap the total time as exceeds tunable of: ", g_sMPTunables.igb_mostwanted_bonus_cash_max_time)
			ENDIF
			
			INT iCashPerMinute = g_sMPTunables.igb_mostwanted_bonus_cash_reward_default
			IF bIsThisABikerVariation
				iCashPerMinute = g_sMPTunables.iBIKER_ON_THE_RUN_BONUS_CASH                                          
			ENDIF
			
			iBonusCash = (iMins * iCashPerMinute)
			
			CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [GIVE_REWARDS] - Local player receives bonuns cash: $", iBonusCash)
		ENDIF
		
		structGangBossManageRewardsData.iExtraCash = iBonusCash
		structGangBossManageRewardsData.iTiedPlayers = GBFIVESTAR_GET_NUM_PLAYERS_REMAINING()
		
		INT iFmmcType = FMMC_TYPE_GB_CHAL_FIVESTAR
		IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
			iFmmcType = FMMC_TYPE_BIKER_ON_THE_RUN
		ENDIF
		GANG_BOSS_MANAGE_REWARDS(iFmmcType, bWinner, structGangBossManageRewardsData)
		
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_REWARDS_GIVEN)
	ENDIF
ENDPROC

FUNC BOOL PLAYER_SHOULD_BE_KNOCKED_OUT()
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
		CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [PLAYER_SHOULD_BE_KNOCKED_OUT] - TRUE - NOT IS_NET_PLAYER_OK(PLAYER_ID())")
		SET_PLAYER_HAS_BEEN_KNOCKED_OUT_OF_MODE(TRUE)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_INTERIOR_KO)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [PLAYER_SHOULD_BE_KNOCKED_OUT] - TRUE - CLIENT_BS_LOCAL_INTERIOR_KO")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_BOSS_SPEC_KO)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [PLAYER_SHOULD_BE_KNOCKED_OUT] - TRUE - CLIENT_BS_LOCAL_BOSS_SPEC_KO")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), FALSE)	
		CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [PLAYER_SHOULD_BE_KNOCKED_OUT] - TRUE - IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), FALSE)")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_IN_SHOP_OF_TYPE(SHOP_TYPE_CARMOD)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [PLAYER_SHOULD_BE_KNOCKED_OUT] - TRUE - IS_PLAYER_IN_SHOP_OF_TYPE(SHOP_TYPE_CARMOD)")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_IN_CORONA()
		CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [PLAYER_SHOULD_BE_KNOCKED_OUT] - TRUE - IS_PLAYER_IN_CORONA()")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_CINEMA)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [PLAYER_SHOULD_BE_KNOCKED_OUT] - TRUE - IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_CINEMA)")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
		CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [PLAYER_SHOULD_BE_KNOCKED_OUT] - TRUE - IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()")
		RETURN TRUE
	ENDIF
	
	IF NETWORK_IS_IN_MP_CUTSCENE()
		CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [PLAYER_SHOULD_BE_KNOCKED_OUT] - TRUE - NETWORK_IS_IN_MP_CUTSCENE()")
		RETURN TRUE
	ENDIF

	IF IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_BIKE_DESTROYED)		
		CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [BIKER] [PLAYER_SHOULD_BE_KNOCKED_OUT] - TRUE - GBFIVESTAR_BS_BIKE_DESTROYED")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_KNOCKED_OUT_CHECK()
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_KNOCKED_OUT)
	AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_JOINED_LATE)
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			IF PLAYER_SHOULD_BE_KNOCKED_OUT()
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_BOSS_SPEC_KO)
					SCRIPT_EVENT_DATA_TICKER_MESSAGE tickerData
					tickerData.playerID = PLAYER_ID()
					tickerData.playerID2 = GB_GET_LOCAL_PLAYER_MISSION_HOST()
					tickerData.TickerEvent = TICKER_EVENT_KNOCKED_OUT_FIVESTAR
					BROADCAST_TICKER_EVENT(tickerData, ALL_PLAYERS(FALSE, TRUE))
				ENDIF
				
				IF NOT SHOULD_SKIP_KNOCKOUT_SHARD()
				AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_IN_INTERIOR)
				AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_INTERIOR_KO)
				AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_JOINED_LATE)
				AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_BOSS_SPEC_KO)
				AND NOT GB_SHOULD_RUN_SPEC_CAM()
					SET_GAME_STATE_ON_DEATH(AFTERLIFE_SET_SPECTATORCAM)
				ENDIF
				
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_KNOCKED_OUT)
				CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [MAINTAIN_KNOCKED_OUT_CHECK] - CLIENT_BS_LOCAL_KNOCKED_OUT = SET")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_WANTED_LEVEL()
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_KNOCKED_OUT)
	AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_JOINED_LATE)
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			IF GET_MAX_WANTED_LEVEL() < GB_GET_FORCED_WANTED_LEVEL()
				SET_MAX_WANTED_LEVEL(GB_GET_FORCED_WANTED_LEVEL())
			ENDIF
			
			IF NOT IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_INIT_WANTED)
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), GB_GET_FORCED_WANTED_LEVEL())
				SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
				
				IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
				AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontClearLocalPassengersWantedLevel, TRUE)
					SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_FallsOutOfVehicleWhenKilled, TRUE) 
				ENDIF
				
				SET_BIT(iLocalBitset, GBFIVESTAR_BS_INIT_WANTED)
				CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_FIVESTAR === [MAINTAIN_WANTED_LEVEL] - Local bit GBFIVESTAR_BS_INIT_WANTED set.")
			ELSE
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < GB_GET_FORCED_WANTED_LEVEL()
					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), GB_GET_FORCED_WANTED_LEVEL())
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_FIVESTAR === [MAINTAIN_WANTED_LEVEL] - Wanted level somehow dropped during mode. Giving local player wanted level again.")
				ENDIF
			
				UPDATE_WANTED_POSITION_THIS_FRAME(PLAYER_ID())
				SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID())
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_PLAYER_GHOSTING()
	IF GET_SERVER_MISSION_STAGE() = GBFIVESTAR_SURVIVE
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_KNOCKED_OUT)
		AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_JOINED_LATE)
			IF NOT IS_ENTITY_A_GHOST(PLAYER_PED_ID())
				SET_LOCAL_PLAYER_AS_GHOST(TRUE)
				NETWORK_SET_FRIENDLY_FIRE_OPTION(FALSE)
				CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [MAINTAIN_PLAYER_GHOSTING] - SET_LOCAL_PLAYER_AS_GHOST(TRUE)")
			ENDIF
		ELSE
			IF IS_ENTITY_A_GHOST(PLAYER_PED_ID())
				SET_LOCAL_PLAYER_AS_GHOST(FALSE)
				SET_PLAYER_DEFAULT_FRIENDLY_FIRE_OPTION()
				CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [MAINTAIN_PLAYER_GHOSTING] - SET_LOCAL_PLAYER_AS_GHOST(FALSE)")
			ENDIF
		ENDIF
	ELSE
		IF IS_ENTITY_A_GHOST(PLAYER_PED_ID())
			SET_LOCAL_PLAYER_AS_GHOST(FALSE)
			SET_PLAYER_DEFAULT_FRIENDLY_FIRE_OPTION()
			CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [MAINTAIN_PLAYER_GHOSTING] - GET_SERVER_MISSION_STAGE() != GBFIVESTAR_SURVIVE - SET_LOCAL_PLAYER_AS_GHOST(FALSE)")
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_BOSS_SPECTATE()
	IF GB_SHOULD_RUN_SPEC_CAM()
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_BOSS_SPEC_KO)
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_BOSS_SPEC_KO)
		ENDIF
	ENDIF
	
	IF PLAYER_ID() = GB_GET_LOCAL_PLAYER_MISSION_HOST()
		GB_MAINTAIN_SPECTATE(sSpecVars, TRUE, FALSE, FALSE, FALSE)
	ELSE
		GB_MAINTAIN_SPECTATE(sSpecVars, FALSE, FALSE, FALSE, FALSE)
	ENDIF
ENDPROC

/// PURPOSE: Maintain distance checks
PROC MAINTAIN_DISTANCE_CHECKS()
	IF GBFIVESTAR_GET_NUM_PLAYERS_REMAINING() > 0
		IF IS_NET_PLAYER_OK(GBFIVESTAR_GET_WINNER())
		AND NOT IS_ENTITY_DEAD(GET_PLAYER_PED(GBFIVESTAR_GET_WINNER()))
			GB_MAINTAIN_BOSS_WORK_DISTANT_CHECKS(FMMC_TYPE_GB_CHAL_FIVESTAR, GET_ENTITY_COORDS(GET_PLAYER_PED(GBFIVESTAR_GET_WINNER())), bSet)
		ENDIF
	ENDIF
ENDPROC



/// PURPOSE: Save the vehicle index of the bike that the player started the challenge in
PROC SAVE_LOCAL_PLAYERS_BIKE_AS_MISSION_BIKE()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
		viLocalPlayersBike = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)
		SET_BIT(iLocalBitset, GBFIVESTAR_BS_SAVED_PLAYERS_BIKE)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [BIKER] [SAVE_LOCAL_PLAYERS_BIKE_AS_MISSION_BIKE] - Saved local player's bike. Vehicle Index: ", NATIVE_TO_INT(viLocalPlayersBike))
	ENDIF
ENDPROC

/// PURPOSE: Check that the player's vehicle is still driveable
PROC MAINTAIN_PLAYER_BIKE_CHECKS()
	IF NOT IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_BIKE_DESTROYED)
		IF IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_SAVED_PLAYERS_BIKE)
			IF NOT DOES_ENTITY_EXIST(viLocalPlayersBike)
			OR NOT IS_VEHICLE_DRIVEABLE(viLocalPlayersBike)
				SET_BIT(iLocalBitset, GBFIVESTAR_BS_BIKE_DESTROYED)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Blip the player's vehicle if they are not on it
PROC MAINTAIN_PLAYER_BIKE_BLIP()
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_KNOCKED_OUT)
	AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_JOINED_LATE)
	
		IF NOT IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_BIKE_DESTROYED)
			IF IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_SAVED_PLAYERS_BIKE)
				IF DOES_ENTITY_EXIST(viLocalPlayersBike)
				AND IS_VEHICLE_DRIVEABLE(viLocalPlayersBike)
				AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viLocalPlayersBike)
					
					IF NOT IS_VEHICLE_MY_CURRENT_PERSONAL_VEHICLE(viLocalPlayersBike)
						IF NOT DOES_BLIP_EXIST(biLocalPlayersBikeBlip)
							biLocalPlayersBikeBlip = ADD_BLIP_FOR_ENTITY(viLocalPlayersBike)
							SET_BLIP_SPRITE(biLocalPlayersBikeBlip, RADAR_TRACE_GANG_BIKE)
							SET_BLIP_COLOUR_FROM_HUD_COLOUR(biLocalPlayersBikeBlip, HUD_COLOUR_BLUE)
							SET_BLIP_ROUTE(biLocalPlayersBikeBlip, FALSE)
							SET_BLIP_PRIORITY(biLocalPlayersBikeBlip, BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
						ENDIF
					ELSE
						SET_PERSONAL_VEHICLE_BLIP_COLOUR(HUD_COLOUR_BLUE)
					ENDIF
					
				ELSE
					IF DOES_BLIP_EXIST(biLocalPlayersBikeBlip)
						REMOVE_BLIP(biLocalPlayersBikeBlip)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ELSE
		CLEAR_PERSONAL_VEHICLE_BLIP_COLOUR()
		IF DOES_BLIP_EXIST(biLocalPlayersBikeBlip)
			REMOVE_BLIP(biLocalPlayersBikeBlip)
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_PLAYERS_BIKE_BLIP()
	IF DOES_BLIP_EXIST(biLocalPlayersBikeBlip)
		REMOVE_BLIP(biLocalPlayersBikeBlip)
	ENDIF
ENDPROC

PROC MAINTAIN_AFTERLIFE_RESET_CHECKS()
	IF IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_KNOCKOUT_DONE)
		IF MPGlobals.GameDeathState = AFTERLIFE_SET_SPECTATORCAM
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				RESET_GAME_STATE_ON_DEATH()
			ENDIF
		ENDIF
	ENDIF
ENDPROC


//----------------------
//	SCRIPT PROCESSING
//----------------------

PROC CLIENT_PROCESSING()

	#IF IS_DEBUG_BUILD
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_TIME_UP)
		CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_FIVESTAR === [CLIENT_PROCESSING] - DEBUG KEY PRESSED: S - TIME UP!")
	ENDIF
	#ENDIF
	
	MAINTAIN_AFTERLIFE_RESET_CHECKS()
	
	IF bIsThisABikerVariation

		INT iRemainingTime = (GET_CHALLENGE_DURATION() - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.stChallengeTimer))
		BOOL bEnded = GET_SERVER_MISSION_STAGE() > GBFIVESTAR_SURVIVE
	
		GB_MAINTAIN_COUNTDOWN_MUSIC(iRemainingTime, bEnded, cmStruct)
	ENDIF
	

	SWITCH GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT())
	
		CASE GBFIVESTAR_INIT
			IF HAS_NET_TIMER_STARTED(serverBD.stChallengeTimer)
			AND GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.stChallengeTimer) >= 10000
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_JOINED_LATE)
			ENDIF
			
			IF NOT IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_INIT_SETUP)
				GB_COMMON_BOSS_MISSION_SETUP(FMMC_TYPE_GB_CHAL_FIVESTAR)
				GB_SET_LOCAL_PLAYER_CRITICAL_TO_JOB(TRUE)
				GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
				SET_BIT(iLocalBitset, GBFIVESTAR_BS_INIT_SETUP)
			ENDIF
			
			GB_SET_ITS_SAFE_FOR_SPEC_CAM()
			IF GB_LAUNCH_CHALLENGE_WITH_SPEC_CAM_IS_ACTIVE(sSpecVars)
				IF GET_SERVER_MISSION_STAGE() > GBFIVESTAR_INIT
					GB_SET_COMMON_TELEMETRY_DATA_ON_START(DEFAULT, DEFAULT, GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG(), FMMC_TYPE_BIKER_ON_THE_RUN)
					SET_CLIENT_MISSION_STAGE(GBFIVESTAR_SURVIVE)
				ENDIF
			ENDIF
		BREAK
		
		CASE GBFIVESTAR_SURVIVE
			IF NOT IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_SAVED_PLAYERS_BIKE)
				IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
					bIsThisABikerVariation = TRUE
					SAVE_LOCAL_PLAYERS_BIKE_AS_MISSION_BIKE()
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_INTRO_DONE)
				HANDLE_SHARD_HELP_INTRO()
			ENDIF
			
			// UI
			MAINTAIN_OBJECTIVE_TEXT_AND_TIMER()
			MAINTAIN_WARN_NEAR_YACHT()
			MAINTAIN_PLAYER_BIKE_BLIP()
			
			// Knock out logic and UI
			HANDLE_KNOCKOUT()
			
			// Audio
			MAINTAIN_COUNTDOWN_AUDIO()
			
			// Logic
			MAINTAIN_WANTED_LEVEL()
			MAINTAIN_KNOCKED_OUT_CHECK()
			MAINTAIN_PLAYER_GHOSTING()
			MAINTAIN_INTERIOR_TIMEOUT()
			MAINTAIN_DISTANCE_CHECKS()
			MAINTAIN_PLAYER_BIKE_CHECKS()
			
			DRAW_CHALLENGE_DPAD_LBD(g_GBLeaderboardStruct.challengeLbdStruct, g_GBLeaderboardStruct.siDpadMovie, SUB_BOSS, g_GBLeaderboardStruct.dpadVariables, 
									g_GBLeaderboardStruct.fmDpadStruct)
			
			IF GET_SERVER_MISSION_STAGE() > GBFIVESTAR_SURVIVE
			
				IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
				OR GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState != RESPAWN_STATE_PLAYING
					SET_MANUAL_RESPAWN_STATE(MRS_EXIT_SPECTATOR_AFTERLIFE)
					RESET_GAME_STATE_ON_DEATH()
					CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [CLIENT_PROCESSING] - Stuck on spectator afterlife. Manual exit.")
				ENDIF
				IF GB_SHOULD_RUN_SPEC_CAM()
					GB_SET_SHOULD_CLOSE_SPECTATE()
				ENDIF
				
				IF IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_TIMEOUT_PLAYING)
					STOP_SOUND(iInteriorSound)
					SET_BIT(iLocalBitset, GBFIVESTAR_BS_WON_WHILE_TIMEOUT)
					CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === [CLIENT_PROCESSING] - Stop playing timeout audio.")
				ENDIF
						
				SET_CLIENT_MISSION_STAGE(GBFIVESTAR_REWARDS)
			ENDIF
		BREAK
	
		CASE GBFIVESTAR_REWARDS
			GANG_BOSS_MANAGE_WANTED_CLEAN_UP(TRUE, GBFIVESTAR_GET_WINNER()) 
			HANDLE_SHARD_OUTRO()
			GIVE_REWARDS()
			
			IF IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
				MAINTAIN_OBJECTIVE_TEXT_AND_TIMER()
			ENDIF
			
			REMOVE_PLAYERS_BIKE_BLIP()
			
			MAINTAIN_PLAYER_GHOSTING()

			IF IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_END_SHARD_DONE)
			AND IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_REWARDS_GIVEN)
			AND NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
			AND GB_MAINTAIN_BOSS_END_UI(sEndUI, FALSE, bIsThisABikerVariation)
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_OUTRO_FINISHED)
			ENDIF
		
			IF GET_SERVER_MISSION_STAGE() > GBFIVESTAR_REWARDS
				SET_CLIENT_MISSION_STAGE(GBFIVESTAR_END)
			ENDIF
		BREAK
		
		CASE GBFIVESTAR_END
		
		BREAK

	ENDSWITCH

ENDPROC

PROC SERVER_PROCESSING()
	
	SWITCH serverBD.eGBFIVESTAR_Stage
	
		CASE GBFIVESTAR_INIT
			IF SHOULD_JOB_START()
				SET_SERVER_MISSION_STAGE(GBFIVESTAR_SURVIVE)
			ENDIF
		BREAK
		
		CASE GBFIVESTAR_SURVIVE
			IF IS_BIT_SET(iLocalBitset, GBFIVESTAR_BS_INIT_SETUP)
				POPULATE_PARTICIPANT_LIST()
			ENDIF
			MAINTAIN_ACTIVE_PLAYER_CHECKS()
			MAINTAIN_END_CONDITION_CHECKS()
		BREAK
		
		CASE GBFIVESTAR_REWARDS
			IF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_OUTRO_FINISHED)
				SET_SERVER_MISSION_STAGE(GBFIVESTAR_END)
			ENDIF
			
			MAINTAIN_ACTIVE_PLAYER_CHECKS()
		BREAK
		
		CASE GBFIVESTAR_END
			
		BREAK

	ENDSWITCH

ENDPROC

FUNC BOOL INIT_CLIENT()

	// Begin our survival timer
	START_NET_TIMER(stSurvivalStartTime)

	RETURN TRUE
ENDFUNC

FUNC BOOL INIT_SERVER()
	IF INIT_GBFIVESTAR()
		
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)

	CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === PROCESS_PRE_GAME")
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission), missionScriptArgs)
	
	//Reserve Entities
	RESERVE_NETWORK_MISSION_OBJECTS(0)
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD,SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD,SIZE_OF(playerBD))
	
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	GB_COMMON_BOSS_MISSION_PREGAME()
	
	#IF IS_DEBUG_BUILD
		CREATE_WIDGETS()
	#ENDIF
	
	RETURN TRUE
	
ENDFUNC

//----------------------
//	MAIN SCRIPT
//----------------------

SCRIPT(MP_MISSION_DATA missionScriptArgs)


	CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === START")
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NOT PROCESS_PRE_GAME(missionScriptArgs)
			SCRIPT_CLEANUP()
		ENDIF	
	ENDIF
	
	WHILE (TRUE)
	
		MP_LOOP_WAIT_ZERO()
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_FIVESTAR === SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")
			SCRIPT_CLEANUP(TRUE)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF bKillScript
			SET_SERVER_MISSION_STATE(GAME_STATE_END)
		ENDIF
		#ENDIF
		
		MAINTAIN_BOSS_SPECTATE()
			
		SWITCH(GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT()))
		
			CASE GAME_STATE_INIT
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					IF INIT_CLIENT()
						SET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT(),GAME_STATE_RUNNING)
					ENDIF
				ENDIF
			BREAK
			
			CASE GAME_STATE_RUNNING
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					IF NOT GB_SHOULD_EVENT_BE_HIDDEN()
						CLIENT_PROCESSING()
					ENDIF
					IF GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION()
						IF GB_SHOULD_RUN_SPEC_CAM()
							GB_SET_SHOULD_CLOSE_SPECTATE()
						ENDIF
						SET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT(),GAME_STATE_END)
					ENDIF
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					IF GB_SHOULD_RUN_SPEC_CAM()
						GB_SET_SHOULD_CLOSE_SPECTATE()
					ENDIF
					SET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT(),GAME_STATE_END)
				ENDIF
			BREAK
			
			CASE GAME_STATE_END
				IF NOT GB_SHOULD_RUN_SPEC_CAM()
					SCRIPT_CLEANUP()
				ENDIF
			BREAK
			
		ENDSWITCH
	
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
			IF GB_SHOULD_KILL_ACTIVE_BOSS_MISSION()
				SET_SERVER_MISSION_STATE(GAME_STATE_END)
			ENDIF
		
			SWITCH(GET_SERVER_MISSION_STATE())
			
				CASE GAME_STATE_INIT
					IF INIT_SERVER()
						SET_SERVER_MISSION_STATE(GAME_STATE_RUNNING)
					ENDIF
				BREAK
				
				CASE GAME_STATE_RUNNING
					IF GET_SERVER_MISSION_STAGE() = GBFIVESTAR_END
						SET_SERVER_MISSION_STATE(GAME_STATE_END)
					ELSE
						SERVER_PROCESSING()
					ENDIF
				BREAK
				
				CASE GAME_STATE_END
					//SCRIPT_CLEANUP()
				BREAK
				
			ENDSWITCH
		
		ENDIF
		
	ENDWHILE

	
	
// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT

