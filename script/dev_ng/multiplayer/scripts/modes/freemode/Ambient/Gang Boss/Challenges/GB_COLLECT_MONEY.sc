//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        GB_COLLECT_MONEY.sc			/			Protection Racket						   				//
// Description: Boss Challenge wherein the boss and goons compete to reclaim the most protection				//
//				money from businesses in Los Santons/Blaine County. Player who drops off the most cash wins.	//
// Written by:  Steve Tiley																						//
// Date: 		01/09/2015																						//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "net_mission.sch"


//----------------------
//	INCLUDES
//----------------------
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "flow_public_core_override.sch"
USING "net_wait_zero.sch"
USING "net_mission.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"
USING "am_common_ui.sch"

USING "net_gang_boss.sch"
USING "net_gang_boss_spectate.sch"
USING "DM_Leaderboard.sch"

//----------------------
//  DEBUG
//----------------------
#IF IS_DEBUG_BUILD
WIDGET_GROUP_ID CollectMoneyChallengeWidget
BOOL bCreateWidgets
INT iWidgetState
BOOL bHostEndMissionNow
BOOL bSkipIntro
BOOL bExpireTimer
#ENDIF

//----------------------
//	GAME STATE
//----------------------

CONST_INT GAME_STATE_INIT 									0
CONST_INT GAME_STATE_RUNNING								1
CONST_INT GAME_STATE_TERMINATE_DELAY						2
CONST_INT GAME_STATE_END									3

//----------------------
//	ENUM
//----------------------

ENUM GB_PR_STAGE_ENUM
	eMS_INIT = 0,
	eMS_COLLECT,
	eMS_DELIVER,
	eMS_WAIT,
	eMS_END,
	eMS_CLEANUP
ENDENUM

ENUM PROTECTION_RACKET_DAMAGE_TYPE
	PR_DAMAGE_TYPE_DEATH = 0,
	PR_DAMAGE_TYPE_FROM_PED,
	PR_DAMAGE_TYPE_FROM_PLAYER,
	PR_DAMAGE_TYPE_FROM_FALL,
	PR_DAMAGE_TYPE_IN_VEHICLE,
	PR_DAMAGE_TYPE_FROM_VEHICLE_COLLISION
ENDENUM

ENUM FINAL_COUNTDOWN_STAGE_ENUM
	eFCOUNTDOWN_PREP = 0,
	eFCOUNTDOWN_START,
	eFCOUNTDOWN_END_OR_KILL
ENDENUM

//----------------------
//	CONSTANTS
//----------------------

// Time

CONST_INT ONE_SECOND					1000
CONST_INT TWO_SECONDS					ONE_SECOND * 2
CONST_INT TEN_SECONDS					ONE_SECOND * 10
CONST_INT TWELVE_SECONDS				TWO_SECONDS * 6
CONST_INT FIFTEEN_SECONDS				ONE_SECOND * 15			// 15 seconds
CONST_INT ONE_MINUTE 					FIFTEEN_SECONDS * 4		// 1 Min
CONST_INT TWO_MINUTES					ONE_MINUTE * 2			// 2 Mins
CONST_INT TEN_MINUTES					TWO_MINUTES * 5			// 10 Mins
CONST_INT FIFTEEN_MINUTES				ONE_MINUTE * 15			// 15 Mins

// Bag info
CONST_INT MAX_NUM_BAGS					4
CONST_INT NUM_MAP_QUADRANTS				4
CONST_INT NUM_DROP_OFF_LOCATIONS		6
CONST_INT NUM_START_AREAS_PER_QUADRANT	5
CONST_INT BAG_CASH_TOTAL_VALUE			30000

CONST_INT DELIVERY_DISTANCE				5
CONST_INT NUM_MINI_LEADERBOARD_PLAYERS	4

// Damage
CONST_INT DAMAGE_CASH					50	// $50 dropped if hurt
CONST_INT DEATH_DAMAGE_CASH				200 // $200 dropped if you die

CONST_INT ZERO_SCORE					0

//----------------------
//	Local Bitset
//----------------------
INT iHelpBitSet
INT iBigMessageBitSet

//----------------------
//	STRUCT
//----------------------
GB_MAINTAIN_SPECTATE_VARS sSpecVars
GANG_BOSS_MANAGE_REWARDS_DATA sRewardsData
GB_STRUCT_BOSS_END_UI sEndUI

STRUCT PROTECTION_RACKET_TELEMETRY_DATA
	INT iBagsCollected
	INT iBagsDelivered
	INT iPickupQuadrant
	INT iDeliveryQuadrant
ENDSTRUCT
PROTECTION_RACKET_TELEMETRY_DATA sProtectionRacketTelemetryStruct

//----------------------
//	VARIABLES
//----------------------
BOOL bBlipsBlocked = FALSE
BOOL bClearWanted = FALSE
BOOL bLowNumbers = FALSE

INT iDropOffBlipR, iDropOffBlipG, iDropOffBlipB, iDropOffBlipA, iSetup
INT iMarkerR, iMarkerG, iMarkerB, iMarkerA, iDeadMarkerR, iDeadMarkerG, iDeadMarkerB, iDeadMarkerA
INT iLeaderboardStagger = 0
INT localSyncID
INT serverStaggeredPlayerLoop
INT iCleanupBossSpecStep
INT iTimeRemaining

VECTOR vDropOffLocations[NUM_DROP_OFF_LOCATIONS]

SCRIPT_TIMER localDamageTimer
SCRIPT_TIMER AircraftTimer

//----------------------
//	INDICES
//----------------------
BLIP_INDEX BagBlip[MAX_NUM_BAGS]
BLIP_INDEX DropOffBlip

//----------------------
//	MUSIC
//----------------------

FINAL_COUNTDOWN_STAGE_ENUM eFinalCountdownStage
INT iFinalCountdownBitSet
CONST_INT iFCBS_TriggeredPreCountdown	0
CONST_INT iFCBS_TriggeredCountdown		1
CONST_INT iFCBS_ReEnabledRadioControl	2
CONST_INT iFCBS_TriggeredCountdownKill	3
CONST_INT iFCBS_Trigger5s				4


//------------------------------------------------------------------------------------------------------------
//	BROADCAST VARIABLES
//------------------------------------------------------------------------------------------------------------

CONST_INT biS_ChallengeEnded 		0
CONST_INT biS_BossLeft				1
CONST_INT biS_AllBagsCollected		2
CONST_INT biS_ChallengeTied			3
CONST_INT biS_ChallengeNoScore		4
CONST_INT biS_StandardWinner		5

// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData

	INT 				iServerGameState = GAME_STATE_INIT
	
	INT 				iServerBitSet
	INT 				iPlayerBitSet
	INT					iCollectedBagBitSet
	
	INT					iServerSyncID
	INT					iServerEndReason = -1
	
	INT					iReservedObjects = MAX_NUM_BAGS
	INT					iBagsCollected = 0
	INT					iBagsDelivered = 0
	INT					iDropArea = -1
	INT 				iQuadrant = -1
	INT					iStartArea = -1
	INT 				iScoreboardPlayerScore[NUM_NETWORK_PLAYERS]
	
	INT 				iBossDeathCount
	INT					iGoonDeathCount
	
	PLAYER_INDEX 		piGangBoss
	PLAYER_INDEX 		piWinner
	PLAYER_INDEX		piTiedWinner
	
	VECTOR				vDropOffCoords
	
	BLIP_SPRITE			BagBlipSprite = RADAR_TRACE_DEAD_DROP
	NETWORK_INDEX 		niBag[MAX_NUM_BAGS]
	MODEL_NAMES 		BagModel = PROP_POLY_BAG_MONEY
	
	SCRIPT_TIMER 		ChallengeDuration
	SCRIPT_TIMER 		ChallengeEnd
		
	AMBIENT_ACTIVITY_LEADERBOARD_STRUCT sSortedLeaderboard[GB_MAX_GANG_SIZE_INCLUDING_BOSS_EXEC]
	
	INT					iMatchId1
	INT					iMatchId2
		
	GB_PR_STAGE_ENUM 	eStage = eMS_COLLECT
ENDSTRUCT
ServerBroadcastData serverBD

CONST_INT biP_RewardGiven			0
CONST_INT biP_EndCashGivenToBoss	1
CONST_INT biP_CollectedBag			2
CONST_INT biP_DeliveredBag			3
CONST_INT biP_IsWinner				4

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	
	INT 	iPlayerGameState = GAME_STATE_INIT
	
	INT 	iPlayerBitSet
	INT		iCashCollected
	INT		iCashDelivered
	
	GB_PR_STAGE_ENUM 	eStage 	= eMS_INIT
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

// -----------------------------------------------------------------------------------------------------------
//		TERMINATION
// -----------------------------------------------------------------------------------------------------------


// -----------------------------------------------------------------------------------------------------------
//		FUNCTIONS
// -----------------------------------------------------------------------------------------------------------


// TUNABLES ----------------------------------------------------------- >|

// Amount of cash in the bag
FUNC INT PR_TUNABLE_BAG_VALUE()
	RETURN g_sMPTunables.igb_protectionr_bag_value
ENDFUNC

// Whether or not a percentage of the players cash should be taken when they die
FUNC BOOL PR_TUNABLE_SHOULD_USE_PERCENTAGE_DEATH_DEDUCTION()
	RETURN g_sMPTunables.bgb_protectionracket_death_deduction_percentage_override
ENDFUNC

// The % of cash that should be taken when they die
FUNC FLOAT PR_TUNABLE_DEATH_PERCENTAGE_VALUE()
	RETURN g_sMPTunables.fgb_protectionracket_death_deduction_percentage
ENDFUNC

// The flat amount of cash taken when the player dies
FUNC INT PR_TUNABLE_DEATH_FLAT_VALUE()
	RETURN g_sMPTunables.igb_protectionracket_death_deduction_flat_value
ENDFUNC

// The amount of cash to be taken from the player when they die
FUNC INT PR_TUNABLE_DAMAGE_FROM_DEATH()

	// X% of their current cash
	IF PR_TUNABLE_SHOULD_USE_PERCENTAGE_DEATH_DEDUCTION()
		RETURN CEIL(((playerBD[PARTICIPANT_ID_TO_INT()].iCashCollected /100 ) * PR_TUNABLE_DEATH_PERCENTAGE_VALUE()))
	ENDIF

	// Flat value
	RETURN PR_TUNABLE_DEATH_FLAT_VALUE()
ENDFUNC

// Amount of cash the player drops on foot when shot by a ped
FUNC INT PR_TUNABLE_DAMAGE_FROM_PED()
	RETURN g_sMPTunables.igb_protectionracket_cash_dropped_damage_from_ped 
ENDFUNC

// Amount of cash the player drops on foot when shot by a player
FUNC INT PR_TUNABLE_DAMAGE_FROM_PLAYER()
	RETURN g_sMPTunables.igb_protectionracket_cash_dropped_damage_from_player
ENDFUNC

// Amount of cash the player drops when they take fall damage
FUNC INT PR_TUNABLE_DAMAGE_FROM_FALL()
	RETURN g_sMPTunables.igb_protectionracket_cash_dropped_damage_from_fall
ENDFUNC

// Amount of cash the player drops when shot by a ped in vehicle, or taking environment damage (crash into lamp post etc)
FUNC INT PR_TUNABLE_DAMAGE_IN_VEHICLE(BOOL bAircraft = FALSE)
	IF bAircraft
		RETURN g_sMPTunables.igb_protectionracket_cash_dropped_damage_in_vehicle * 2
	ENDIF

	RETURN g_sMPTunables.igb_protectionracket_cash_dropped_damage_in_vehicle
ENDFUNC

// Amount of cash the player drops when colliding with another vehicle
FUNC INT PR_TUNABLE_DAMAGE_FROM_VEHICLE_COLLISION(BOOL bAircraft = FALSE)
	IF bAircraft
		RETURN g_sMPTunables.igb_protectionracket_cash_dropped_damage_vehicle_to_vehicle * 2
	ENDIF

	RETURN g_sMPTunables.igb_protectionracket_cash_dropped_damage_vehicle_to_vehicle
ENDFUNC

// The amount of cash the player drops in a helicopter or plane (influenced by PR_TUNABLE_AIRCRAFT_TIME())
FUNC INT PR_TUNABLE_DAMAGE_AIRCRAFT()
	RETURN g_sMPTunables.igb_protectionracket_cash_dropped_flying_aircraft
ENDFUNC

// Generic cash loss from damage (not used, but may be useful)
FUNC INT PR_TUNABLE_GENERIC_DAMAGE_VALUE()
	RETURN 50
ENDFUNC

// The challenge wanted level when collecting a bag
FUNC INT PR_TUNABLE_GET_WANTED()
	RETURN g_sMPTunables.igb_protectionracket_get_wanted
ENDFUNC

// Duration
FUNC INT PR_TUNABLE_CHALLENGE_DURATION()
	RETURN g_sMPTunables.igb_protectionr_time_limit
ENDFUNC

// The radius of the drop off
FUNC INT PR_TUNABLE_GET_DELIVERY_DISTANCE()
	RETURN g_sMPTunables.igb_protectionracket_delivery_distance
ENDFUNC

// The rate that players can lose cash (e.g. $150 per X ms)
FUNC INT PR_TUNABLE_DAMAGE_RESET_TIME()
	RETURN g_sMPTunables.igb_protectionracket_cash_dropped_damage_frequency
ENDFUNC

// Number of bags
FUNC INT PR_TUNABLE_NUM_BAGS()
	RETURN GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS()
ENDFUNC

// Least amount of cash the player can carry
FUNC INT PR_TUNABLE_MINIMUM_CASH()
	RETURN g_sMPTunables.igb_protectionracket_minimum_cash
ENDFUNC

// The rate that player should drop cash in an aircraft
FUNC INT PR_TUNABLE_AIRCRAFT_TIME()
	RETURN g_sMPTunables.igb_protectionracket_cash_dropped_flying_aircraft_time
ENDFUNC

// How fast the players need to be going before they start losing cash in an aircraft 
// (when on ground, avoid players driving in planes on roads then taking off infrequently)
FUNC INT PR_TUNABLE_AIRCRAFT_SPEED_THRESHOLD()
	RETURN g_sMPTunables.igb_protectionracket_cash_dropped_flying_aircraft_speed_threshold 
ENDFUNC

FUNC INT PR_TUNABLE_MAX_COMBINED_TOTAL()
	RETURN g_sMPTunables.igb_protectionracket_max_combined_total
ENDFUNC

// -------------------------------------------------------------------- |<

FUNC BOOL SHOULD_UI_BE_HIDDEN()
	IF GB_SHOULD_HIDE_GANG_BOSS_EVENT(FMMC_TYPE_GB_CHAL_COLLECT_MONEY)
	OR GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
	OR NOT MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_MISSION_STATE_STRING(INT iState)
	SWITCH(iState)
		CASE GAME_STATE_INIT 				RETURN "GAME_STATE_INIT"
		CASE GAME_STATE_RUNNING 			RETURN "GAME_STATE_RUNNING"
		CASE GAME_STATE_TERMINATE_DELAY 	RETURN "GAME_STATE_TERMINATE_DELAY"
		CASE GAME_STATE_END 				RETURN "GAME_STATE_END"
	ENDSWITCH

	RETURN "UNKNOWN STATE!"

ENDFUNC

FUNC STRING GET_MISSION_STAGE_STRING(GB_PR_STAGE_ENUM thisStage)
	SWITCH thisStage
		CASE eMS_INIT				RETURN "eMS_INIT"
		CASE eMS_COLLECT			RETURN "eMS_COLLECT"
		CASE eMS_END				RETURN "eMS_END"
		CASE eMS_WAIT				RETURN "eMS_WAIT"
		CASE eMS_CLEANUP			RETURN "eMS_CLEANUP"
	ENDSWITCH
	
	RETURN "UNKNOWN STAGE!"
ENDFUNC

// ------------------------------------------------------ |<

// Package Management ----------------------------------- >|
FUNC STRING GB_GET_BAG_BLIP_NAME()
	RETURN "GB_PR_BLIP_GEN"	//	Protection Money
ENDFUNC

PROC ADD_DROP_OFF_BLIP()
	IF NOT DOES_BLIP_EXIST(DropOffBlip)
		DropOffBlip = CREATE_BLIP_FOR_COORD(serverBD.vDropOffCoords, TRUE)
		SHOW_HEIGHT_ON_BLIP(DropOffBlip, TRUE)
		SET_BLIP_PRIORITY(DropOffBlip, BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
		SET_BLIP_SCALE(DropOffBlip, GB_BLIP_SIZE)
		SET_BLIP_NAME_FROM_TEXT_FILE(DropOffBlip, "GB_SNG_BLIP0")
	ENDIF
ENDPROC

PROC REMOVE_DROP_OFF_BLIP()
	IF DOES_BLIP_EXIST(DropOffBlip)
		REMOVE_BLIP(DropOffBlip)
	ENDIF
ENDPROC


PROC ADD_BAG_BLIPS()
	IF SHOULD_UI_BE_HIDDEN()
		EXIT
	ENDIF
	
	INT i
	REPEAT PR_TUNABLE_NUM_BAGS() i
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niBag[i])
			IF NOT DOES_BLIP_EXIST(BagBlip[i])
				BagBlip[i] = ADD_BLIP_FOR_ENTITY(NET_TO_OBJ(serverBD.niBag[i]))
				SHOW_HEIGHT_ON_BLIP(BagBlip[i], TRUE)
				SET_BLIP_SPRITE(BagBlip[i], serverBD.BagBlipSprite)
				SET_BLIP_EXTENDED_HEIGHT_THRESHOLD(BagBlip[i], TRUE)
				SET_BLIP_FLASH_TIMER(BagBlip[i], 7000)
				SET_BLIP_PRIORITY(BagBlip[i], BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
				SET_BLIP_NAME_FROM_TEXT_FILE(BagBlip[i], GB_GET_BAG_BLIP_NAME())
				SET_BLIP_COLOUR(BagBlip[i], BLIP_COLOUR_GREEN)
				SET_BLIP_SCALE(BagBlip[i], GB_BLIP_SIZE)
				PRINTLN("[GB PROTECTION RACKET] [STMGB] - ADD_PACKAGE_BLIPS - Blip added for package ", i)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC REMOVE_BLIP_FROM_THIS_BAG(INT thisBag)
	IF DOES_BLIP_EXIST(BagBlip[thisBag])
		REMOVE_BLIP(BagBlip[thisBag])
	ENDIF
ENDPROC

PROC REMOVE_ALL_BAG_BLIPS()
	INT i
	REPEAT PR_TUNABLE_NUM_BAGS() i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niBag[i])
			IF DOES_BLIP_EXIST(BagBlip[i])
				REMOVE_BLIP(BagBlip[i])
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC BLOCK_BLIPS(BOOL block)
	IF block
		IF bBlipsBlocked = FALSE
			PRINTLN("[GB PROTECTION RACKET] [STMGB] - BLOCK_BLIPS - TRUE - Block_All_MissionsAtCoords_Missions(true)")
			Block_All_MissionsAtCoords_Missions(TRUE)
			bBlipsBlocked = TRUE
		ENDIF
	ELSE
		IF bBlipsBlocked = TRUE
			PRINTLN("[AM PROTECTION RACKET] - BLOCK_BLIPS - FALSE - Block_All_MissionsAtCoords_Missions(false)")
			Block_All_MissionsAtCoords_Missions(FALSE)
			bBlipsBlocked = FALSE
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAVE_ALL_BAGS_BEEN_COLLECTED()
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllBagsCollected)
		RETURN TRUE
	ENDIF

	IF GB_IS_THIS_PLAYER_USING_SPECTATE(serverBD.piGangBoss)
		IF serverBD.iBagsCollected = GB_GET_NUM_GOONS_IN_PLAYER_GANG(serverBD.piGangBoss)
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

// ------------------------------------------------------ |<

// Server ----------------------------------------------- >|

//Helper function to get the servers game/mission state
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC

//Helper function to set the servers game/mission state
PROC SET_SERVER_MISSION_STATE(INT iState)
	serverBD.iServerGameState = iState
	PRINTLN("[GB PROTECTION RACKET] [STMGB] [SERVER] - SET_SERVER_MISSION_STATE = ",GET_MISSION_STATE_STRING(iState))
ENDPROC

FUNC GB_PR_STAGE_ENUM GET_SERVER_MISSION_STAGE()
	RETURN serverBD.eStage
ENDFUNC

PROC SET_SERVER_MISSION_STAGE(GB_PR_STAGE_ENUM newStage)
	PRINTLN("[GB PROTECTION RACKET] [STMGB] [SERVER] - SET_SERVER_MISSION_STAGE - setting new server stage to ", GET_MISSION_STAGE_STRING(newStage))
	serverBD.eStage = newStage
ENDPROC

// ------------------------------------------------------ |<

// Client ----------------------------------------------- >|

//Helper function to get a clients game/mission state
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iPlayerGameState
ENDFUNC

//Helper function to set a clients game/mission state
PROC SET_CLIENT_MISSION_STATE(INT iPlayer,INT iState)
	playerBD[iPlayer].iPlayerGameState = iState
	PRINTLN("[GB PROTECTION RACKET] [STMGB] - SET_CLIENT_MISSION_STATE = ",GET_MISSION_STATE_STRING(iState))
ENDPROC

FUNC GB_PR_STAGE_ENUM GET_CLIENT_MISSION_STAGE(INT iPlayer)
	RETURN playerBD[iPlayer].eStage
ENDFUNC

PROC SET_CLIENT_MISSION_STAGE(INT iPlayer, GB_PR_STAGE_ENUM newStage)
	PRINTLN("[GB PROTECTION RACKET] [STMGB] - SET_CLIENT_MISSION_STAGE - setting new client stage to ", GET_MISSION_STAGE_STRING(newStage))
	playerBD[iPlayer].eStage = newStage
ENDPROC

// ------------------------------------------------------ |<

// Gang Info -------------------------------------------- >|
FUNC PLAYER_INDEX GET_GANG_BOSS_PLAYER_INDEX()
	RETURN serverBD.piGangBoss
ENDFUNC

PROC SET_GANG_BOSS_PLAYER_INDEX(PLAYER_INDEX thisPlayer)
	IF thisPlayer != INVALID_PLAYER_INDEX()
		IF NETWORK_IS_PLAYER_ACTIVE(thisPlayer)
			PRINTLN("[GB PROTECTION RACKET] [STMGB] - SET_GANG_BOSS_PLAYER_INDEX - gang boss is ", GET_PLAYER_NAME(thisPlayer))
			serverBD.piGangBoss = thisPlayer
		ELSE
			PRINTLN("[GB PROTECTION RACKET] [STMGB] - SET_GANG_BOSS_PLAYER_INDEX - player is not active")
		ENDIF
	ELSE
		PRINTLN("[GB PROTECTION RACKET] [STMGB] - SET_GANG_BOSS_PLAYER_INDEX - invalid player index passed.")
	ENDIF
ENDPROC

FUNC BOOL AM_I_THE_GANG_BOSS()
	IF PLAYER_ID() = GET_GANG_BOSS_PLAYER_INDEX()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_PLAYER_THE_GANG_BOSS(PLAYER_INDEX thisPlayer)
	IF thisPlayer = GET_GANG_BOSS_PLAYER_INDEX()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_PLAYER_A_GANG_GOON(PLAYER_INDEX thisPlayer)
	IF thisPlayer != INVALID_PLAYER_INDEX()
		IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(thisPlayer, GET_GANG_BOSS_PLAYER_INDEX(), FALSE)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC PLAYER_INDEX GET_WINNER()
	RETURN serverBD.piWinner
ENDFUNC

FUNC PLAYER_INDEX GET_TIED_WINNER()
	RETURN serverBD.piTiedWinner
ENDFUNC

FUNC BOOL IS_PLAYER_WANTED(INT iPlayer)
	IF GET_PLAYER_WANTED_LEVEL(INT_TO_PLAYERINDEX(iPlayer)) > 0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_PLAYER_CASH_COLLECTED(INT iPlayer)
	RETURN playerBD[iPlayer].iCashCollected
ENDFUNC

FUNC INT GET_LOCAL_PLAYER_CASH_COLLECTED()
	RETURN playerBD[PARTICIPANT_ID_TO_INT()].iCashCollected
ENDFUNC

FUNC INT GET_PLAYER_CASH_DELIVERED(INT iPlayer)
	RETURN playerBD[iPlayer].iCashDelivered
ENDFUNC

FUNC INT GET_LOCAL_PLAYER_CASH_DELIVERED()
	RETURN playerBD[PARTICIPANT_ID_TO_INT()].iCashDelivered
ENDFUNC

FUNC BOOL HAS_PLAYER_COLLECTED_A_BAG(INT thisPlayer)
	RETURN IS_BIT_SET(playerBD[thisPlayer].iPlayerBitSet, biP_CollectedBag)
ENDFUNC

FUNC BOOL HAS_PLAYER_DELIVERED_A_BAG(INT thisPlayer)
	RETURN IS_BIT_SET(playerBD[thisPlayer].iPlayerBitSet, biP_DeliveredBag)
ENDFUNC

PROC SET_LOCAL_WANTED(BOOL bWanted)
	IF bWanted
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < PR_TUNABLE_GET_WANTED()
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), PR_TUNABLE_GET_WANTED(), FALSE)
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			SET_MAX_WANTED_LEVEL(PR_TUNABLE_GET_WANTED())
		ENDIF
	ELSE
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0, FALSE)
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			SET_MAX_WANTED_LEVEL(5)
			SET_WANTED_LEVEL_MULTIPLIER(1.0)
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_DAMAGE_VALUE(PROTECTION_RACKET_DAMAGE_TYPE thisType)
	SWITCH thisType
		CASE PR_DAMAGE_TYPE_DEATH 					RETURN PR_TUNABLE_DAMAGE_FROM_DEATH()
		CASE PR_DAMAGE_TYPE_FROM_PED 				RETURN PR_TUNABLE_DAMAGE_FROM_PED()
		CASE PR_DAMAGE_TYPE_FROM_PLAYER				RETURN PR_TUNABLE_DAMAGE_FROM_PLAYER()
		CASE PR_DAMAGE_TYPE_FROM_FALL				RETURN PR_TUNABLE_DAMAGE_FROM_FALL()
		CASE PR_DAMAGE_TYPE_IN_VEHICLE				RETURN PR_TUNABLE_DAMAGE_IN_VEHICLE()
		CASE PR_DAMAGE_TYPE_FROM_VEHICLE_COLLISION	RETURN PR_TUNABLE_DAMAGE_FROM_VEHICLE_COLLISION()
	ENDSWITCH
	
	RETURN PR_TUNABLE_GENERIC_DAMAGE_VALUE()
ENDFUNC

FUNC STRING GET_MY_POSITION()
	IF serverBD.sSortedLeaderboard[0].playerID = PLAYER_ID()
		RETURN "GB_SNG_FIRST"
	ELIF serverBD.sSortedLeaderboard[1].playerID = PLAYER_ID()
		RETURN "GB_SNG_SECOND"
	ELIF serverBD.sSortedLeaderboard[2].playerID = PLAYER_ID()
		RETURN "GB_SNG_THIRD"
	ENDIF
	
	RETURN "GB_SNG_FOURTH"
ENDFUNC

// ------------------------------------------------------ |<

// Help / Big Message functions ------------------------- >|

FUNC BOOL IS_SAFE_FOR_HELP(BOOL bIncludeOtherHelp = FALSE)
	IF  NOT IS_CUSTOM_MENU_ON_SCREEN()
	AND NOT IS_PAUSE_MENU_ACTIVE()
	AND NOT IS_RADAR_HIDDEN()
	AND IS_NET_PLAYER_OK(PLAYER_ID())
	AND NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
		IF bIncludeOtherHelp
		 	IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				RETURN TRUE
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEAR_THIS_HELP(INT thisHelp)
	TEXT_LABEL_63 help = "GB_PR_HELP"
	help += thisHelp
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(help)
		CLEAR_HELP()
	ENDIF
ENDPROC

PROC CLEAR_ANY_HELP()
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GB_PR_HELP1")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GB_PR_HELP2")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GB_PR_HELP3")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GB_PR_HELP4")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GB_PR_HELP5")
		CLEAR_HELP()
	ENDIF
ENDPROC

PROC DO_HELP_TEXT(INT iHelp)

	IF SHOULD_UI_BE_HIDDEN()
		CLEAR_ANY_HELP()
		EXIT
	ENDIF

	SWITCH iHelp
		CASE 1		// - Your boss has launched Protection Racket. Compete against other members of your organization to deliver the most protection money reclaimed from businesses in ~a~.
			IF NOT IS_BIT_SET(iHelpBitSet, iHelp)
				IF IS_SAFE_FOR_HELP(TRUE)
					PRINT_HELP_WITH_LITERAL_STRING_WITH_HUD_COLOUR_NO_SOUND("GB_PR_HELP1", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(GET_GANG_BOSS_PLAYER_INDEX()), GET_PLAYER_HUD_COLOUR(GET_GANG_BOSS_PLAYER_INDEX()))
					GB_SET_GANG_BOSS_HELP_BACKGROUND()
					SET_BIT(iHelpBitSet, iHelp)
					PRINTLN("[GB PROTECTION RACKET] [STMGB] [CLIENT] - DO_HELP_TEXT - Help ", iHelp, " done: ", GET_FILENAME_FOR_AUDIO_CONVERSATION("GB_PR_HELP1"))
				ENDIF
			ENDIF
		BREAK
		
		CASE 2		// - You have collected a bag but the cops are on to you. Get to the drop off as quickly as possible to avoid losing cash as you escape.
			IF NOT IS_BIT_SET(iHelpBitSet, iHelp)
				IF IS_SAFE_FOR_HELP(TRUE)
					PRINT_HELP_NO_SOUND("GB_PR_HELP2")
					GB_SET_GANG_BOSS_HELP_BACKGROUND()
					SET_BIT(iHelpBitSet, iHelp)
					PRINTLN("[GB PROTECTION RACKET] [STMGB] [CLIENT] - DO_HELP_TEXT - Help ", iHelp, " done: ", GET_FILENAME_FOR_AUDIO_CONVERSATION("GB_PR_HELP2"))
				ENDIF
			ENDIF
		BREAK
		
		CASE 3		// - Deliver more cash than other members of your organization to win.
			IF NOT IS_BIT_SET(iHelpBitSet, iHelp)
				IF IS_SAFE_FOR_HELP(TRUE)
					PRINT_HELP_NO_SOUND("GB_PR_HELP3")
					GB_SET_GANG_BOSS_HELP_BACKGROUND()
					SET_BIT(iHelpBitSet, iHelp)
					PRINTLN("[GB PROTECTION RACKET] [STMGB] [CLIENT] - DO_HELP_TEXT - Help ", iHelp, " done: ", GET_FILENAME_FOR_AUDIO_CONVERSATION("GB_PR_HELP3"))
				ENDIF
			ENDIF
		BREAK
		
		CASE 4		// - You successfully delivered your cash. The cops can now be evaded as you no longer carrying contraband.
			IF NOT IS_BIT_SET(iHelpBitSet, iHelp)
				IF IS_SAFE_FOR_HELP(TRUE)
					PRINT_HELP_NO_SOUND("GB_PR_HELP4")
					GB_SET_GANG_BOSS_HELP_BACKGROUND()
					SET_BIT(iHelpBitSet, iHelp)
					PRINTLN("[GB PROTECTION RACKET] [STMGB] [CLIENT] - DO_HELP_TEXT - Help ", iHelp, " done: ", GET_FILENAME_FOR_AUDIO_CONVERSATION("GB_PR_HELP4"))
				ENDIF
			ENDIF
		BREAK
		
		CASE 5
			IF NOT IS_BIT_SET(iHelpBitSet, iHelp)
				IF IS_SAFE_FOR_HELP(TRUE)
					PRINT_HELP_NO_SOUND("GB_PR_HELP5")
					GB_SET_GANG_BOSS_HELP_BACKGROUND()
					SET_BIT(iHelpBitSet, iHelp)
					PRINTLN("[GB PROTECTION RACKET] [STMGB] [CLIENT] - DO_HELP_TEXT - Help ", iHelp, " done: ", GET_FILENAME_FOR_AUDIO_CONVERSATION("GB_PR_HELP5"))
				ENDIF
			ENDIF
		BREAK
		
		CASE 6
			IF NOT IS_BIT_SET(iHelpBitSet, iHelp)
				IF IS_SAFE_FOR_HELP(TRUE)
					PRINT_HELP_NO_SOUND("GB_PR_HELP6")
					GB_SET_GANG_BOSS_HELP_BACKGROUND()
					SET_BIT(iHelpBitSet, iHelp)
					PRINTLN("[GB PROTECTION RACKET] [STMGB] [CLIENT] - DO_HELP_TEXT - Help ", iHelp, " done: ", GET_FILENAME_FOR_AUDIO_CONVERSATION("GB_PR_HELP6"))
				ENDIF
			ENDIF
		BREAK
	
	ENDSWITCH

ENDPROC

/// PURPOSE:
///    Performs general checks for it being safe to display the shard
/// RETURNS:
///    True if safe
FUNC BOOL IS_SAFE_FOR_BIG_MESSAGE()	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND NOT IS_PAUSE_MENU_ACTIVE()
	AND NOT IS_CUSTOM_MENU_ON_SCREEN()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE				
ENDFUNC

#IF IS_DEBUG_BUILD
/// PURPOSE:
///    Prints information about the big message that has just been requested to draw
/// PARAMS:
///    iBigMessage - the index of the message that was drawn
///    title - the title of the message
///    strapline - the subtitle of the message
PROC PRINT_BIG_MESSAGE_DEBUG_INFORMATION(INT iBigMessage, STRING title, STRING strapline)
	PRINTLN("[GB PROTECTION RACKET] [STMGB] [CLIENT] - PRINT_BIG_MESSAGE_DEBUG_INFORMATION - Big Message:	", iBigMessage)
	PRINTLN("[GB PROTECTION RACKET] [STMGB] [CLIENT] - PRINT_BIG_MESSAGE_DEBUG_INFORMATION - Title: 		", GET_FILENAME_FOR_AUDIO_CONVERSATION(title))
	PRINTLN("[GB PROTECTION RACKET] [STMGB] [CLIENT] - PRINT_BIG_MESSAGE_DEBUG_INFORMATION - Strapline:	", GET_FILENAME_FOR_AUDIO_CONVERSATION(strapline))
ENDPROC
#ENDIF


PROC DO_BIG_MESSAGE(INT iBigMessage)

	IF SHOULD_UI_BE_HIDDEN()
		CLEAR_ALL_BIG_MESSAGES()
		EXIT
	ENDIF

	SWITCH iBigMessage
		CASE 0		// BOSS CHALLENGE - Reclaim and deliver protection money
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_HELP()
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_START_OF_JOB, "GB_BIGM_PR_T", "GB_PR_BMS0")
					SET_BIT(iBigMessageBitSet, iBigMessage)
					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_BIGM_PR_T", "GB_PR_BMS0") #ENDIF
				ENDIF
			ENDIF
		BREAK
		
//		CASE 1		// CHALLENGE OVER - You came ~a~ in Protection Racket by reclaiming and delivering $~1~
//			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
//				IF IS_SAFE_FOR_BIG_MESSAGE()
//					CLEAR_HELP()
//					SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_GB_END_OF_JOB_OVER, GET_MY_POSITION(), "GB_PR_BMS1", "GB_CHAL_OVER", DEFAULT, GET_PLAYER_CASH_COLLECTED(PARTICIPANT_ID_TO_INT()))
//					SET_BIT(iBigMessageBitSet, iBigMessage)
//					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_CHAL_OVER", "GB_PR_BMS1") #ENDIF
//				ENDIF
//			ENDIF
//		BREAK
		
		CASE 2		// WINNER - You won Protection Racket by reclaiming and delivering $~1~
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_HELP()
					SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, GET_LOCAL_PLAYER_CASH_DELIVERED(), "GB_PR_BMS2", "GB_WINNER")
					SET_BIT(iBigMessageBitSet, iBigMessage)
					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_WINNER", "GB_PR_BMS2") #ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 3		// WINNER - You and <C>~a~</C> won Protection Racket by reclaiming and delivering $~1~
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_HELP()
					IF GET_WINNER() = PLAYER_ID()
						IF GET_TIED_WINNER() != INVALID_PLAYER_INDEX()
							IF NETWORK_IS_PLAYER_ACTIVE(GET_TIED_WINNER())
								SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, GET_TIED_WINNER(), GET_LOCAL_PLAYER_CASH_DELIVERED(), "GB_PR_BMS3", "GB_WINNER")
							ENDIF
						ENDIF
					ELSE
						IF GET_WINNER() != INVALID_PLAYER_INDEX()
							IF NETWORK_IS_PLAYER_ACTIVE(GET_WINNER())
								SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, GET_WINNER(), GET_LOCAL_PLAYER_CASH_DELIVERED(), "GB_PR_BMS3", "GB_WINNER")
							ENDIF
						ENDIF
					ENDIF
					SET_BIT(iBigMessageBitSet, iBigMessage)
					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_WINNER", "GB_PR_BMS3") #ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 4		// WINNER - You didn't deliver any cash. ~a~ won by delivering $~1~ 				(If no tie)
					// WINNER - You didn't deliver any cash. ~a~ and ~a~ won by delivering $~1~			(If tie)
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_HELP()
					IF IS_BIT_SET(serverBD.iServerBitSet, biS_ChallengeTied)
						IF GET_WINNER() != INVALID_PLAYER_INDEX()
						AND GET_TIED_WINNER() != INVALID_PLAYER_INDEX()
							IF NETWORK_IS_PLAYER_ACTIVE(GET_WINNER())
							AND NETWORK_IS_PLAYER_ACTIVE(GET_TIED_WINNER())
								SETUP_NEW_BIG_MESSAGE_WITH_2_PLAYERS(BIG_MESSAGE_GB_END_OF_JOB_OVER, GET_WINNER(), GET_TIED_WINNER(), GET_PLAYER_CASH_DELIVERED(NATIVE_TO_INT(GET_WINNER())), "GB_PR_BMS4b", "GB_CHAL_OVER")
								SET_BIT(iBigMessageBitSet, iBigMessage)
								#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_CHAL_OVER", "GB_PR_BMS4b") #ENDIF
							ENDIF
						ENDIF
					ELSE
						IF GET_WINNER() != INVALID_PLAYER_INDEX()
							IF NETWORK_IS_PLAYER_ACTIVE(GET_WINNER())
								SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GB_END_OF_JOB_OVER, GET_WINNER(), GET_PLAYER_CASH_DELIVERED(NATIVE_TO_INT(GET_WINNER())), "GB_PR_BMS4", "GB_CHAL_OVER")
								SET_BIT(iBigMessageBitSet, iBigMessage)
								#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_CHAL_OVER", "GB_PR_BMS4") #ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 5		// CHALLENGE OVER - No protection money was delivered
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_HELP()
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_START_OF_JOB, "GB_CHAL_OVER", "GB_PR_BMS5")
					SET_BIT(iBigMessageBitSet, iBigMessage)
					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_CHAL_OVER", "GB_PR_BMS5") #ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 6		// CHALLENGE OVER - Your Boss has left the session
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_HELP()
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_START_OF_JOB, "GB_CHAL_OVER", "GB_PR_BMS6")
					SET_BIT(iBigMessageBitSet, iBigMessage)
					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_CHAL_OVER", "GB_PR_BMS6") #ENDIF
				ENDIF
			ENDIF
		BREAK

	ENDSWITCH
ENDPROC

PROC DO_COLLECT_TICKER()
	SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
	TickerEventData.TickerEvent = TICKER_EVENT_GB_PR_COLLECT_CASH
	TickerEventData.playerID = PLAYER_ID()
	TickerEventData.playerID2 = GET_GANG_BOSS_PLAYER_INDEX()
	BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT(TRUE))
ENDPROC

// Sends a ticker to all players informing them of a delivery
PROC DO_DELIVER_TICKER(INT iCash)
	SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
	TickerEventData.TickerEvent = TICKER_EVENT_GB_PR_DELIVER_CASH
	TickerEventData.playerID = PLAYER_ID()
	TickerEventData.playerID2 = GET_GANG_BOSS_PLAYER_INDEX()
	TickerEventData.dataInt = iCash
	BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT(TRUE))
ENDPROC

// ------------------------------------------------------ |<

PROC DRAW_BAG_MARKERS()
	IF NOT HAS_PLAYER_COLLECTED_A_BAG(PARTICIPANT_ID_TO_INT())
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niBag[0])
			IF NOT IS_ENTITY_ATTACHED(NET_TO_ENT(serverBD.niBag[0]))
				IF IS_NET_PLAYER_OK(PLAYER_ID())
					DRAW_MARKER(MARKER_ARROW, GET_ENTITY_COORDS(NET_TO_ENT(serverBD.niBag[0]))+<<0,0,1.0>>, <<0,0,0>>, <<180,0,0>>, <<0.5,0.5,0.5>>, iMarkerR, iMarkerG, iMarkerB, 100, TRUE, TRUE) 
				ELSE
					DRAW_MARKER(MARKER_ARROW, GET_ENTITY_COORDS(NET_TO_ENT(serverBD.niBag[0]))+<<0,0,1.0>>, <<0,0,0>>, <<180,0,0>>, <<0.5,0.5,0.5>>, iDeadMarkerR, iDeadMarkerG, iDeadMarkerB, 100, TRUE, TRUE) 
				ENDIF
			ENDIF
		ENDIF
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niBag[1])
			IF NOT IS_ENTITY_ATTACHED(NET_TO_ENT(serverBD.niBag[1]))
				IF IS_NET_PLAYER_OK(PLAYER_ID())
					DRAW_MARKER(MARKER_ARROW, GET_ENTITY_COORDS(NET_TO_ENT(serverBD.niBag[1]))+<<0,0,1.0>>, <<0,0,0>>, <<180,0,0>>, <<0.5,0.5,0.5>>, iMarkerR, iMarkerG, iMarkerB, 100, TRUE, TRUE) 
				ELSE
					DRAW_MARKER(MARKER_ARROW, GET_ENTITY_COORDS(NET_TO_ENT(serverBD.niBag[1]))+<<0,0,1.0>>, <<0,0,0>>, <<180,0,0>>, <<0.5,0.5,0.5>>, iDeadMarkerR, iDeadMarkerG, iDeadMarkerB, 100, TRUE, TRUE) 
				ENDIF
			ENDIF
		ENDIF
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niBag[2])
			IF NOT IS_ENTITY_ATTACHED(NET_TO_ENT(serverBD.niBag[2]))
				IF IS_NET_PLAYER_OK(PLAYER_ID())
					DRAW_MARKER(MARKER_ARROW, GET_ENTITY_COORDS(NET_TO_ENT(serverBD.niBag[2]))+<<0,0,1.0>>, <<0,0,0>>, <<180,0,0>>, <<0.5,0.5,0.5>>, iMarkerR, iMarkerG, iMarkerB, 100, TRUE, TRUE) 
				ELSE
					DRAW_MARKER(MARKER_ARROW, GET_ENTITY_COORDS(NET_TO_ENT(serverBD.niBag[2]))+<<0,0,1.0>>, <<0,0,0>>, <<180,0,0>>, <<0.5,0.5,0.5>>, iDeadMarkerR, iDeadMarkerG, iDeadMarkerB, 100, TRUE, TRUE) 
				ENDIF
			ENDIF
		ENDIF
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niBag[3])
			IF NOT IS_ENTITY_ATTACHED(NET_TO_ENT(serverBD.niBag[3]))
				IF IS_NET_PLAYER_OK(PLAYER_ID())
					DRAW_MARKER(MARKER_ARROW, GET_ENTITY_COORDS(NET_TO_ENT(serverBD.niBag[3]))+<<0,0,1.0>>, <<0,0,0>>, <<180,0,0>>, <<0.5,0.5,0.5>>, iMarkerR, iMarkerG, iMarkerB, 100, TRUE, TRUE) 
				ELSE
					DRAW_MARKER(MARKER_ARROW, GET_ENTITY_COORDS(NET_TO_ENT(serverBD.niBag[3]))+<<0,0,1.0>>, <<0,0,0>>, <<180,0,0>>, <<0.5,0.5,0.5>>, iDeadMarkerR, iDeadMarkerG, iDeadMarkerB, 100, TRUE, TRUE) 
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CONTROL_BAG_DELETION(INT iBag)
	IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niBag[iBag])
		DELETE_NET_ID(serverBD.niBag[iBag])
		PRINTLN("[GB PROTECTION RACKET] [STMGB] [CLIENT] - CONTROL_BAG_DELETION - DELETE_NET_ID has been called on bag ", iBag)
	ENDIF
ENDPROC

PROC CONTROL_BAG_COLLECTION_CLIENT()
	IF NOT HAS_PLAYER_COLLECTED_A_BAG(PARTICIPANT_ID_TO_INT())
		INT i
		REPEAT PR_TUNABLE_NUM_BAGS() i
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niBag[i])
				IF IS_ENTITY_ATTACHED_TO_ENTITY(NET_TO_OBJ(serverBD.niBag[i]), PLAYER_PED_ID())
					
					DO_HELP_TEXT(2)
					
					DO_COLLECT_TICKER()
					
					PRINTLN("[GB PROTECTION RACKET] [STMGB] [CLIENT] - CONTROL_BAG_COLLECTION_CLIENT - I collected a bag! It was bag ", i)
					
					playerBD[PARTICIPANT_ID_TO_INT()].iCashCollected += PR_TUNABLE_BAG_VALUE()
					
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_CollectedBag)
					
					GB_CLEAR_ITS_SAFE_FOR_SPEC_CAM()
					
					GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
					
					sProtectionRacketTelemetryStruct.iBagsCollected++
					
					CONTROL_BAG_DELETION(i)
					SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(FALSE)
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

FUNC VECTOR GET_DROP_OFF_COORDS()
	RETURN serverBD.vDropOffCoords
ENDFUNC

FUNC BOOL IS_PLAYER_NEAR_DROP_OFF()
	IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), GET_DROP_OFF_COORDS()) < PR_TUNABLE_GET_DELIVERY_DISTANCE()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_SAFE_TO_DELIVER_CASH()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_ALL_CASH_BEEN_DELIVERED_APART_FROM_MINE()
	// Only check if all packages have been collected
	IF HAVE_ALL_BAGS_BEEN_COLLECTED()
		INT i, iNumDel
		
		IF GB_IS_THIS_PLAYER_USING_SPECTATE(GET_GANG_BOSS_PLAYER_INDEX())
			REPEAT NUM_NETWORK_PLAYERS i
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
					IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(INT_TO_PLAYERINDEX(i), GET_GANG_BOSS_PLAYER_INDEX(), FALSE)
						IF GET_PLAYER_CASH_DELIVERED(i) > 0
							PRINTLN("[GB PROTECTION RACKET] [CLIENT] - HAS_ALL_CASH_BEEN_DELIVERED_APART_FROM_MINE - IF ", iNumDel, " >= (GB_GET_NUM_GOONS_IN_PLAYER_GANG(serverBD.piGangBoss) - 1)")
							iNumDel++
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			IF iNumDel >= (GB_GET_NUM_GOONS_IN_PLAYER_GANG(serverBD.piGangBoss) - 1)
				RETURN TRUE
			ENDIF
		ELSE
			REPEAT NUM_NETWORK_PLAYERS i
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
					IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(INT_TO_PLAYERINDEX(i), GET_GANG_BOSS_PLAYER_INDEX())
						IF GET_PLAYER_CASH_DELIVERED(i) > 0
							iNumDel++
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			IF iNumDel >= (GB_GET_NUM_GOONS_IN_PLAYER_GANG(serverBD.piGangBoss))
				PRINTLN("[GB PROTECTION RACKET] [CLIENT] - HAS_ALL_CASH_BEEN_DELIVERED_APART_FROM_MINE - IF ", iNumDel, " >= (GB_GET_NUM_GOONS_IN_PLAYER_GANG(serverBD.piGangBoss))")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL IS_THIS_BPACKAGE_GOING_TO_END_THE_MODE()
	IF HAS_NET_TIMER_EXPIRED(serverBD.ChallengeDuration, PR_TUNABLE_CHALLENGE_DURATION() - 2000)
	OR HAS_ALL_CASH_BEEN_DELIVERED_APART_FROM_MINE()
		PRINTLN("[GB PROTECTION RACKET] [CLIENT] - IS_THIS_BPACKAGE_GOING_TO_END_THE_MODE")
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC HANDLE_CASH_DELIVERY()
	IF NOT HAS_NET_TIMER_EXPIRED(serverBD.ChallengeDuration, PR_TUNABLE_CHALLENGE_DURATION())
		IF NOT HAS_PLAYER_DELIVERED_A_BAG(PARTICIPANT_ID_TO_INT())
			IF HAS_PLAYER_COLLECTED_A_BAG(PARTICIPANT_ID_TO_INT())
				IF GET_PLAYER_CASH_COLLECTED(PARTICIPANT_ID_TO_INT()) > 0
					IF IS_PLAYER_NEAR_DROP_OFF()
						IF IS_SAFE_TO_DELIVER_CASH()
							IF NOT IS_THIS_BPACKAGE_GOING_TO_END_THE_MODE()
								CLEAR_HELP()
								DO_HELP_TEXT(4)
							ENDIF
							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DeliveredBag)
							
							sProtectionRacketTelemetryStruct.iBagsDelivered++
											
							DO_DELIVER_TICKER(GET_PLAYER_CASH_COLLECTED(PARTICIPANT_ID_TO_INT()))
							playerBD[PARTICIPANT_ID_TO_INT()].iCashDelivered += GET_LOCAL_PLAYER_CASH_COLLECTED()
							PRINTLN("[GB PROTECTION RACKET] [CLIENT] - HANDLE_CASH_DELIVERY - Cash delivered! - ", GET_PLAYER_CASH_DELIVERED(PARTICIPANT_ID_TO_INT()))
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DRAW_DROP_OFF_MARKER()
	IF GET_PLAYER_CASH_COLLECTED(PARTICIPANT_ID_TO_INT()) > 0
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			DRAW_MARKER(MARKER_CYLINDER, GET_DROP_OFF_COORDS(), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<DELIVERY_DISTANCE, DELIVERY_DISTANCE, 0.5>>, iDropOffBlipR, iDropOffBlipG, iDropOffBlipB)
		ELSE
			DRAW_MARKER(MARKER_CYLINDER, GET_DROP_OFF_COORDS(), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<DELIVERY_DISTANCE, DELIVERY_DISTANCE, 0.5>>, iDeadMarkerR, iDeadMarkerG, iDeadMarkerB)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_PLAYER_FULLY_SPAWNED()
	IF IS_SCREEN_FADED_IN()
	AND GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState = RESPAWN_STATE_PLAYING
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_SAFE_FOR_END_MESSAGES()
	IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE)
	AND NOT IS_RADAR_HIDDEN()
	AND HAS_PLAYER_FULLY_SPAWNED()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_CHALLENGE_END_REWARDS_AND_MESSAGES()
	
	IF IS_SAFE_FOR_END_MESSAGES()
		IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerBitSet, biP_RewardGiven)
	
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_BossLeft)
				GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_CHAL_COLLECT_MONEY, FALSE, sRewardsData)
				GANG_BOSS_MANAGE_WANTED_CLEAN_UP(TRUE, INVALID_PLAYER_INDEX())
				SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerBitSet, biP_RewardGiven)
				GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_BOSS_LEFT)
				
				EXIT
			ENDIF
			
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_ChallengeNoScore)
				DO_BIG_MESSAGE(5)
				GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_CHAL_COLLECT_MONEY, FALSE, sRewardsData)
				GANG_BOSS_MANAGE_WANTED_CLEAN_UP(TRUE, INVALID_PLAYER_INDEX())
				SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerBitSet, biP_RewardGiven)
				GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_TIME_OUT)
				
				EXIT
			ENDIF
			
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_ChallengeTied)
				IF PLAYER_ID() = GET_WINNER()
				OR PLAYER_ID() = GET_TIED_WINNER()
					DO_BIG_MESSAGE(3)
					GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_CHAL_COLLECT_MONEY, TRUE, sRewardsData)
					GANG_BOSS_MANAGE_WANTED_CLEAN_UP(TRUE, INVALID_PLAYER_INDEX())
					GB_SET_COMMON_TELEMETRY_DATA_ON_END(TRUE, GB_TELEMETRY_END_WON)
					
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_IsWinner)
				ELSE
//					IF GET_PLAYER_CASH_DELIVERED(PARTICIPANT_ID_TO_INT()) > 0
//						DO_BIG_MESSAGE(1)
//					ELSE
						DO_BIG_MESSAGE(4)
//					ENDIF
					GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_CHAL_COLLECT_MONEY, FALSE, sRewardsData)
					GANG_BOSS_MANAGE_WANTED_CLEAN_UP(TRUE, INVALID_PLAYER_INDEX())
					GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST)
					
				ENDIF
			
				SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerBitSet, biP_RewardGiven)
			
				EXIT
			ENDIF
			
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_StandardWinner)
				IF PLAYER_ID() = GET_WINNER()
					DO_BIG_MESSAGE(2)
					GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_CHAL_COLLECT_MONEY, TRUE, sRewardsData)
					GANG_BOSS_MANAGE_WANTED_CLEAN_UP(TRUE, INVALID_PLAYER_INDEX())
					GB_SET_COMMON_TELEMETRY_DATA_ON_END(TRUE, GB_TELEMETRY_END_WON)
					
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_IsWinner)
				ELSE
//					IF GET_PLAYER_CASH_DELIVERED(PARTICIPANT_ID_TO_INT()) > 0
//						DO_BIG_MESSAGE(1)
//					ELSE
						DO_BIG_MESSAGE(4)
//					ENDIF
					GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_CHAL_COLLECT_MONEY, FALSE, sRewardsData)
					GANG_BOSS_MANAGE_WANTED_CLEAN_UP(TRUE, INVALID_PLAYER_INDEX())
					GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST)
					
				ENDIF
			
				SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerBitSet, biP_RewardGiven)
			
				EXIT
			ENDIF	
		ENDIF
	ENDIF
	
ENDPROC

PROC GIVE_END_CASH_TO_WINNER()
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ChallengeNoScore)
		IF GET_WINNER() = PLAYER_ID()
		OR GET_TIED_WINNER() = PLAYER_ID()
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_EndCashGivenToBoss)
				INT i, iTotalCash
				
				REPEAT NUM_NETWORK_PLAYERS i
					IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, i))
						PARTICIPANT_INDEX PartID = INT_TO_NATIVE(PARTICIPANT_INDEX, i)
						PLAYER_INDEX PlayerID = NETWORK_GET_PLAYER_INDEX(PartID)
						
						IF IS_THIS_PLAYER_A_GANG_GOON(PlayerID)
						OR IS_THIS_PLAYER_THE_GANG_BOSS(PlayerID)
							IF GET_PLAYER_CASH_DELIVERED(i) > 0
								iTotalCash += GET_PLAYER_CASH_DELIVERED(i)
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
				
				IF iTotalCash > PR_TUNABLE_MAX_COMBINED_TOTAL()
					iTotalCash = PR_TUNABLE_MAX_COMBINED_TOTAL()
				ENDIF
				
				IF iTotalCash > 0
					IF IS_BIT_SET(serverBD.iServerBitSet, biS_ChallengeTied)
						sRewardsData.iExtraCash += (iTotalCash/2)
					ELSE					
						sRewardsData.iExtraCash += iTotalCash
					ENDIF
					PRINTLN("[GB PROTECTION RACKET] [STMGB] [CLIENT] [BOSS ONLY] - GIVE_END_CASH_TO_WINNER - Extra cash given to boss: $", iTotalCash)
				ELSE
					PRINTLN("[GB PROTECTION RACKET] [STMGB] [CLIENT] [BOSS ONLY] - GIVE_END_CASH_TO_WINNER - iTotalCash = 0, no cash being given to boss")
				ENDIF
				
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_EndCashGivenToBoss)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_INTRO_MESSAGE()
	DO_BIG_MESSAGE(0)
	DO_HELP_TEXT(1)
ENDPROC

PROC RESTART_DAMAGE_TIMER()
	IF NOT HAS_NET_TIMER_STARTED(localDamageTimer)
		START_NET_TIMER(localDamageTimer)
		PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - Starting damage timer.")
	ELSE
		REINIT_NET_TIMER(localDamageTimer)
		START_NET_TIMER(localDamageTimer)
		PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - Resetting damage timer.")
	ENDIF
ENDPROC

FUNC BOOL IS_SAFE_TO_DEDUCT_CASH()
	IF HAS_PLAYER_COLLECTED_A_BAG(PARTICIPANT_ID_TO_INT())
	AND NOT HAS_PLAYER_DELIVERED_A_BAG(PARTICIPANT_ID_TO_INT())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_DEDUCT_CASH_FROM_DAMAGE()
	
	IF GET_PLAYER_CASH_COLLECTED(PARTICIPANT_ID_TO_INT()) <= PR_TUNABLE_MINIMUM_CASH()
		RETURN FALSE
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(localDamageTimer)
		IF NOT HAS_NET_TIMER_EXPIRED(localDamageTimer, PR_TUNABLE_DAMAGE_RESET_TIME())
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_DEATH_DAMAGE()
	IF IS_SAFE_TO_DEDUCT_CASH()
	AND CAN_DEDUCT_CASH_FROM_DAMAGE()
		PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - PROCESS_DEATH_DAMAGE - ***************************************************************")
		PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - PROCESS_DEATH_DAMAGE - Local player has been killed, cash before $", GET_PLAYER_CASH_COLLECTED(PARTICIPANT_ID_TO_INT()))
		PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - PROCESS_DEATH_DAMAGE - Local player has been killed, dropping $", GET_DAMAGE_VALUE(PR_DAMAGE_TYPE_DEATH))
		playerBD[PARTICIPANT_ID_TO_INT()].iCashCollected -= GET_DAMAGE_VALUE(PR_DAMAGE_TYPE_DEATH)
		RESTART_DAMAGE_TIMER()
		IF playerBD[PARTICIPANT_ID_TO_INT()].iCashCollected < PR_TUNABLE_MINIMUM_CASH()
			PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - PROCESS_DEATH_DAMAGE - player cash has dropped below $", PR_TUNABLE_MINIMUM_CASH(), ", keeping at $", PR_TUNABLE_MINIMUM_CASH())
			playerBD[PARTICIPANT_ID_TO_INT()].iCashCollected = PR_TUNABLE_MINIMUM_CASH()
		ENDIF
		PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - PROCESS_DEATH_DAMAGE - Local player has been killed, remaining cash is now $", GET_PLAYER_CASH_COLLECTED(PARTICIPANT_ID_TO_INT()))
		PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - PROCESS_DEATH_DAMAGE - ***************************************************************")
	ENDIF
ENDPROC

PROC PROCESS_FALL_DAMAGE()
	IF IS_SAFE_TO_DEDUCT_CASH()
	AND CAN_DEDUCT_CASH_FROM_DAMAGE()
		PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - PROCESS_FALL_DAMAGE - ***************************************************************")
		PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - PROCESS_FALL_DAMAGE - Local player has fallen, cash before $", GET_PLAYER_CASH_COLLECTED(PARTICIPANT_ID_TO_INT()))
		PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - PROCESS_FALL_DAMAGE - Local player has fallen, dropping $", GET_DAMAGE_VALUE(PR_DAMAGE_TYPE_DEATH))
		playerBD[PARTICIPANT_ID_TO_INT()].iCashCollected -= GET_DAMAGE_VALUE(PR_DAMAGE_TYPE_FROM_FALL)
		RESTART_DAMAGE_TIMER()
		IF playerBD[PARTICIPANT_ID_TO_INT()].iCashCollected < PR_TUNABLE_MINIMUM_CASH()
			PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - PROCESS_FALL_DAMAGE - player cash has dropped below $", PR_TUNABLE_MINIMUM_CASH(), ", keeping at $", PR_TUNABLE_MINIMUM_CASH())
			playerBD[PARTICIPANT_ID_TO_INT()].iCashCollected = PR_TUNABLE_MINIMUM_CASH()
		ENDIF
		PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - PROCESS_FALL_DAMAGE - Local player has fallen, remaining cash is now $", GET_PLAYER_CASH_COLLECTED(PARTICIPANT_ID_TO_INT()))
		PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - PROCESS_FALL_DAMAGE - ***************************************************************")
	ENDIF
ENDPROC

PROC PROCESS_PED_DAMAGE()
	//Hurt by an NPC
	IF IS_SAFE_TO_DEDUCT_CASH()
	AND CAN_DEDUCT_CASH_FROM_DAMAGE()
		PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - PROCESS_PED_DAMAGE - ***************************************************************")
		PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - PROCESS_PED_DAMAGE - Damaged by PED, cash before $", GET_PLAYER_CASH_COLLECTED(PARTICIPANT_ID_TO_INT()))
		PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - PROCESS_PED_DAMAGE - Damaged by PED, dropping $", GET_DAMAGE_VALUE(PR_DAMAGE_TYPE_FROM_PED))
		playerBD[PARTICIPANT_ID_TO_INT()].iCashCollected -= GET_DAMAGE_VALUE(PR_DAMAGE_TYPE_FROM_PED)
		RESTART_DAMAGE_TIMER()
		IF playerBD[PARTICIPANT_ID_TO_INT()].iCashCollected < PR_TUNABLE_MINIMUM_CASH()
			PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - PROCESS_PED_DAMAGE - player cash has dropped below $", PR_TUNABLE_MINIMUM_CASH(), ", keeping at $", PR_TUNABLE_MINIMUM_CASH())
			playerBD[PARTICIPANT_ID_TO_INT()].iCashCollected = PR_TUNABLE_MINIMUM_CASH()
		ENDIF
		PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - PROCESS_PED_DAMAGE - Damaged by PED, remaining cash is now $", GET_PLAYER_CASH_COLLECTED(PARTICIPANT_ID_TO_INT()))
		PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - PROCESS_PED_DAMAGE - ***************************************************************")
	ENDIF
ENDPROC

PROC PROCESS_PLAYER_DAMAGE()
	IF IS_SAFE_TO_DEDUCT_CASH()
	AND	CAN_DEDUCT_CASH_FROM_DAMAGE()
		PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - PROCESS_PLAYER_DAMAGE - ***************************************************************")
		PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - PROCESS_PLAYER_DAMAGE - Damaged by PLAYER, cash before $", GET_PLAYER_CASH_COLLECTED(PARTICIPANT_ID_TO_INT()))
		PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - PROCESS_PLAYER_DAMAGE - Damaged by PLAYER, dropping $", GET_DAMAGE_VALUE(PR_DAMAGE_TYPE_FROM_PLAYER))
		playerBD[PARTICIPANT_ID_TO_INT()].iCashCollected -= GET_DAMAGE_VALUE(PR_DAMAGE_TYPE_FROM_PLAYER)
		RESTART_DAMAGE_TIMER()
		IF playerBD[PARTICIPANT_ID_TO_INT()].iCashCollected < PR_TUNABLE_MINIMUM_CASH()
			PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - PROCESS_PLAYER_DAMAGE - player cash has dropped below $", PR_TUNABLE_MINIMUM_CASH(), ", keeping at $", PR_TUNABLE_MINIMUM_CASH())
			playerBD[PARTICIPANT_ID_TO_INT()].iCashCollected = PR_TUNABLE_MINIMUM_CASH()
		ENDIF
		PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - PROCESS_PLAYER_DAMAGE - Damaged by PLAYER, remaining cash is now $", GET_PLAYER_CASH_COLLECTED(PARTICIPANT_ID_TO_INT()))
		PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - PROCESS_PLAYER_DAMAGE - ***************************************************************")
	ENDIF
ENDPROC

PROC PROCESS_CRASH_DAMAGE()
	// Hit by a vehicle
	IF IS_SAFE_TO_DEDUCT_CASH()
	AND CAN_DEDUCT_CASH_FROM_DAMAGE()
		PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - PROCESS_CRASH_DAMAGE - ***************************************************************")
		PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - PROCESS_CRASH_DAMAGE - Player hit by car or crashed, cash before $", GET_PLAYER_CASH_COLLECTED(PARTICIPANT_ID_TO_INT()))
		IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
		OR IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
			PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - PROCESS_CRASH_DAMAGE - Player hit by car or crashed [air], dropping $", PR_TUNABLE_DAMAGE_FROM_VEHICLE_COLLISION(TRUE))
			playerBD[PARTICIPANT_ID_TO_INT()].iCashCollected -= PR_TUNABLE_DAMAGE_FROM_VEHICLE_COLLISION(TRUE)
		ELSE
			PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - PROCESS_CRASH_DAMAGE - Player hit by car or crashed, dropping $", GET_DAMAGE_VALUE(PR_DAMAGE_TYPE_FROM_VEHICLE_COLLISION))
			playerBD[PARTICIPANT_ID_TO_INT()].iCashCollected -= GET_DAMAGE_VALUE(PR_DAMAGE_TYPE_FROM_VEHICLE_COLLISION)
		ENDIF
		RESTART_DAMAGE_TIMER()
		IF playerBD[PARTICIPANT_ID_TO_INT()].iCashCollected < PR_TUNABLE_MINIMUM_CASH()
			PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - PROCESS_CRASH_DAMAGE - player cash has dropped below $", PR_TUNABLE_MINIMUM_CASH(), ", keeping at $", PR_TUNABLE_MINIMUM_CASH())
			playerBD[PARTICIPANT_ID_TO_INT()].iCashCollected = PR_TUNABLE_MINIMUM_CASH()
		ENDIF
		PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - PROCESS_CRASH_DAMAGE - Player hit by car or crashed, remaining cash is now $", GET_PLAYER_CASH_COLLECTED(PARTICIPANT_ID_TO_INT()))
		PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - PROCESS_CRASH_DAMAGE - ***************************************************************")
	ENDIF
ENDPROC

PROC PROCESS_VEHICLE_DAMAGE()
	// Hit by a vehicle
	IF IS_SAFE_TO_DEDUCT_CASH()
	AND CAN_DEDUCT_CASH_FROM_DAMAGE()
		PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - PROCESS_VEHICLE_DAMAGE - ***************************************************************")
		PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - PROCESS_VEHICLE_DAMAGE - Player's car has been hit, cash before $", GET_PLAYER_CASH_COLLECTED(PARTICIPANT_ID_TO_INT()))
		IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
		OR IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
			PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - PROCESS_VEHICLE_DAMAGE - Player's air vehicle has been hit, dropping $", PR_TUNABLE_DAMAGE_IN_VEHICLE(TRUE))
			playerBD[PARTICIPANT_ID_TO_INT()].iCashCollected -= PR_TUNABLE_DAMAGE_IN_VEHICLE(TRUE)
		ELSE
			PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - PROCESS_VEHICLE_DAMAGE - Player's car has been hit, dropping $", GET_DAMAGE_VALUE(PR_DAMAGE_TYPE_IN_VEHICLE))
			playerBD[PARTICIPANT_ID_TO_INT()].iCashCollected -= GET_DAMAGE_VALUE(PR_DAMAGE_TYPE_IN_VEHICLE)
		ENDIF
		RESTART_DAMAGE_TIMER()
		IF playerBD[PARTICIPANT_ID_TO_INT()].iCashCollected < PR_TUNABLE_MINIMUM_CASH()
			PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - PROCESS_VEHICLE_DAMAGE - player cash has dropped below $", PR_TUNABLE_MINIMUM_CASH(), ", keeping at $", PR_TUNABLE_MINIMUM_CASH())
			playerBD[PARTICIPANT_ID_TO_INT()].iCashCollected = PR_TUNABLE_MINIMUM_CASH()
		ENDIF
		PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - PROCESS_VEHICLE_DAMAGE - Player's car has been hit, remaining cash is now $", GET_PLAYER_CASH_COLLECTED(PARTICIPANT_ID_TO_INT()))
		PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - PROCESS_VEHICLE_DAMAGE - ***************************************************************")
	ENDIF
ENDPROC

PROC PROCESS_DAMAGE(INT iCount)
	
	PED_INDEX VictimPedID	
	PED_INDEX DamagerPedID	
	PLAYER_INDEX VictimPlayerID
	VEHICLE_INDEX VictimVehicleID
	VEHICLE_INDEX DamagerVehicleID

	STRUCT_ENTITY_DAMAGE_EVENT sEntityID
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sEntityID, SIZE_OF(sEntityID))
		// IF the victim exists
		IF DOES_ENTITY_EXIST(sEntityID.VictimIndex)
			// If the victom is a ped
			IF IS_ENTITY_A_PED(sEntityID.VictimIndex)
				VictimPedID = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)
				IF IS_PED_A_PLAYER(VictimPedID)
					VictimPlayerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(VictimPedID)
					IF NETWORK_IS_PLAYER_ACTIVE(VictimPlayerID)
						IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
							IF sEntityID.VictimDestroyed
								IF IS_THIS_PLAYER_A_GANG_GOON(VictimPlayerID)
									serverBD.iGoonDeathCount++
								ENDIF
								IF IS_THIS_PLAYER_THE_GANG_BOSS(VictimPlayerID)
									serverBD.iBossDeathCount++
								ENDIF
							ENDIF
						ENDIF
					
						IF VictimPlayerID = PLAYER_ID()
							// Local player was killed
							IF sEntityID.VictimDestroyed
								PROCESS_DEATH_DAMAGE()
							// Local player has fallen
							ELIF sEntityID.WeaponUsed = ENUM_TO_INT(WEAPONTYPE_FALL)
								PROCESS_FALL_DAMAGE()
							ELSE
								// Is there a damager?
								IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
									IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
										// damaged by a ped
										DamagerPedID = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
										IF IS_PED_A_PLAYER(DamagerPedID)
											PROCESS_PLAYER_DAMAGE()
										ELSE
											IF IS_PED_IN_ANY_VEHICLE(DamagerPedID)
												PROCESS_CRASH_DAMAGE()
											ELSE
												PROCESS_PED_DAMAGE()
											ENDIF
										ENDIF
									ELIF IS_ENTITY_A_VEHICLE(sEntityID.DamagerIndex)
										PROCESS_CRASH_DAMAGE()
									ENDIF						
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			// IF the victim is a vehicle
			ELIF IS_ENTITY_A_VEHICLE(sEntityID.VictimIndex)
				VictimVehicleID = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)
				// If I'm driving the vehicle thats been hit
				IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), VictimVehicleID)
					IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
						IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
							DamagerPedID = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
							IF DamagerPedID = PLAYER_PED_ID()
								PROCESS_CRASH_DAMAGE()
							ELSE
								PROCESS_VEHICLE_DAMAGE()
							ENDIF
						ELSE
							PROCESS_CRASH_DAMAGE()
						ENDIF
					ENDIF
				// If i'm not driving the vehicle thats been hit
				ELSE
					IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
						IF IS_ENTITY_A_VEHICLE(sEntityID.DamagerIndex)
							DamagerVehicleID = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
							// If i'm driving the vehicle that hit the victim
							IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), DamagerVehicleID)
								PROCESS_CRASH_DAMAGE()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SORT_PROTECTION_RACKET_PLAYER_SCORES(PLAYER_INDEX playerID, INT iPlayerScore, AMBIENT_ACTIVITY_LEADERBOARD_SORT_TYPE sort = AAL_SORT_NON_UNIFORM_SCORING)
	SORT_AMBIENT_ACTIVITY_LEADERBOARD(serverBD.sSortedLeaderboard, playerID, iPlayerScore, sort)
	
	serverBD.iServerSyncID++
ENDPROC

PROC PROCESS_PLAYER_JOIN(INT iCount)
	
	STRUCT_PLAYER_SCRIPT_EVENTS data 
	
	GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, data, SIZE_OF(data))

	// Check player is valid
	IF data.PlayerIndex != INVALID_PLAYER_INDEX()

		// If we haven't yet had knowledge of this player then loop over threads ensuring they have joined this script
		IF NOT IS_BIT_SET(serverBD.iPlayerBitSet, NATIVE_TO_INT(data.PlayerIndex))
			INT iThread
			REPEAT data.NumThreads iThread

				IF data.Threads[iThread] = GET_ID_OF_THIS_THREAD()
					
					SORT_PROTECTION_RACKET_PLAYER_SCORES(data.PlayerIndex, 0)	
					
					SET_BIT(serverBD.iPlayerBitSet, NATIVE_TO_INT(data.PlayerIndex))
				ENDIF       
			ENDREPEAT
		ENDIF
	ENDIF

ENDPROC

PROC PROCESS_EVENTS()
	INT iCount
	EVENT_NAMES ThisScriptEvent
	
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount
	
		ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)
		
		IF ThisScriptEvent = EVENT_NETWORK_DAMAGE_ENTITY
			IF GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT()) = GAME_STATE_RUNNING
				PROCESS_DAMAGE(iCount)
			ENDIF
		ENDIF
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			IF ThisScriptEvent = EVENT_NETWORK_PLAYER_JOIN_SCRIPT
				PROCESS_PLAYER_JOIN(iCount)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC


// Maintain Functions ----------------------------------- >|

PROC MAINTAIN_BLIPS()
	SWITCH GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT())
		CASE eMS_INIT
		
		BREAK
		
		CASE eMS_COLLECT
			BLOCK_BLIPS(TRUE)
			ADD_BAG_BLIPS()
		BREAK
		
		CASE eMS_DELIVER
			ADD_DROP_OFF_BLIP()
			REMOVE_ALL_BAG_BLIPS()
		BREAK
		
		CASE eMS_WAIT
			REMOVE_ALL_BAG_BLIPS()
			REMOVE_DROP_OFF_BLIP()
		BREAK
		
		CASE eMS_END
			REMOVE_DROP_OFF_BLIP()
			REMOVE_ALL_BAG_BLIPS()
		BREAK
		
		CASE eMS_CLEANUP
			BLOCK_BLIPS(FALSE)
		BREAK
	
	ENDSWITCH
ENDPROC

PROC MAINTAIN_BOTTOM_RIGHT_UI()
	IF HAS_NET_TIMER_STARTED(serverBD.ChallengeDuration)
		
		INT iCombinedTotal = -1
		HUD_COLOURS timeColour
		BOOL bForce = FALSE
			
		iTimeRemaining = PR_TUNABLE_CHALLENGE_DURATION() - (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.ChallengeDuration))
		
		IF iTimeRemaining > 30000
			timeColour = HUD_COLOUR_WHITE
		ELSE
			timeColour = HUD_COLOUR_RED
		ENDIF
		
		IF iTimeRemaining <= 0
			iTimeRemaining = 0
		ENDIF		
		
		IF serverBD.sSortedLeaderboard[0].playerID != INVALID_PLAYER_INDEX()
		AND serverBD.sSortedLeaderboard[0].iScore > 0
			IF serverBD.sSortedLeaderboard[1].playerID != INVALID_PLAYER_INDEX()
			AND serverBD.sSortedLeaderboard[1].iScore > 0
				DO_HELP_TEXT(6)
			ENDIF
		ENDIF
		
		IF serverBD.sSortedLeaderboard[0].iScore > -1
			iCombinedTotal += 1 // Make it no longer equal to -1
			iCombinedTotal += serverBD.sSortedLeaderboard[0].iScore
		ENDIF
		IF serverBD.sSortedLeaderboard[1].iScore > -1
			iCombinedTotal += serverBD.sSortedLeaderboard[1].iScore
		ENDIF
		IF serverBD.sSortedLeaderboard[2].iScore > -1
			iCombinedTotal += serverBD.sSortedLeaderboard[2].iScore
		ENDIF
		IF serverBD.sSortedLeaderboard[3].iScore > -1
			iCombinedTotal += serverBD.sSortedLeaderboard[3].iScore
		ENDIF
		
		BOTTOM_RIGHT_UI_1STCASH_2NDCASH_3RDCASH_4THCASH_CASH1_CASH2_TIMER(	serverBD.sSortedLeaderboard[0].playerID,
																			serverBD.sSortedLeaderboard[1].playerID,
																			serverBD.sSortedLeaderboard[2].playerID,
																			serverBD.sSortedLeaderboard[3].playerID,
																			serverBD.sSortedLeaderboard[0].iScore,
																			serverBD.sSortedLeaderboard[1].iScore,
																			serverBD.sSortedLeaderboard[2].iScore,
																			serverBD.sSortedLeaderboard[3].iScore,
																			iCombinedTotal,
																			serverBD.iScoreboardPlayerScore[PARTICIPANT_ID_TO_INT()],
																			iTimeRemaining, bForce, timeColour, "", "CARJCK_CSH1TTL", "GB_SNG_HT1",
																			DEFAULT, DEFAULT, DEFAULT, DEFAULT, IS_BIT_SET(serverBD.iServerBitSet, biS_ChallengeEnded))
		
	ENDIF
ENDPROC

PROC MAINTAIN_OBJECTIVE_TEXT()	
	SWITCH GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT())
		CASE eMS_INIT
		BREAK 
		
		CASE eMS_COLLECT
			IF NOT HAS_THIS_MP_OBJECTIVE_TEXT_BEEN_RECEIVED("GB_PR_OT0")
				CLEAR_ANY_OBJECTIVE_TEXT_FROM_THIS_SCRIPT()
				PRINT_OBJECTIVE_TEXT("GB_PR_OT0")
			ENDIF
		BREAK 
		
		CASE eMS_DELIVER
			IF NOT HAS_THIS_MP_OBJECTIVE_TEXT_BEEN_RECEIVED("GB_PR_OT1")
				CLEAR_ANY_OBJECTIVE_TEXT_FROM_THIS_SCRIPT()
				PRINT_OBJECTIVE_TEXT("GB_PR_OT1")
			ENDIF
		BREAK
		
		CASE eMS_WAIT
			IF NOT HAS_THIS_MP_OBJECTIVE_TEXT_BEEN_RECEIVED("GB_PR_OT2")
				CLEAR_ANY_OBJECTIVE_TEXT_FROM_THIS_SCRIPT()
				PRINT_OBJECTIVE_TEXT_WITH_COLOURED_STRING("GB_PR_OT2", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(GET_GANG_BOSS_PLAYER_INDEX()), GET_PLAYER_HUD_COLOUR(GET_GANG_BOSS_PLAYER_INDEX()))
			ENDIF
		BREAK
		
		CASE eMS_END
			IF IS_THERE_ANY_CURRENT_OBJECTIVE_TEXT_FROM_THIS_SCRIPT()
				CLEAR_ANY_OBJECTIVE_TEXT_FROM_THIS_SCRIPT()
			ENDIF
		BREAK 
		
		CASE eMS_CLEANUP
			IF IS_THERE_ANY_CURRENT_OBJECTIVE_TEXT_FROM_THIS_SCRIPT()
				CLEAR_ANY_OBJECTIVE_TEXT_FROM_THIS_SCRIPT()
			ENDIF
		BREAK 
	ENDSWITCH
ENDPROC

PROC MAINTAIN_CHANGE_STAGE_CHECKS()
	
	IF GET_SERVER_MISSION_STAGE() = eMS_END
		IF GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT()) != eMS_END
		
			GB_SET_LOCAL_PLAYER_CRITICAL_TO_JOB(FALSE)
		
			SET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT(), eMS_END)
		ENDIF
	ELIF GET_SERVER_MISSION_STAGE() = eMS_CLEANUP
		IF GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT()) != eMS_CLEANUP
		
			GB_SET_LOCAL_PLAYER_CRITICAL_TO_JOB(FALSE)
		
			SET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT(), eMS_CLEANUP)
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Controls how many entities are reserved based on serverBD
PROC MAINTAIN_RESERVED_ENTITIES()
	INT iCurrentlyReserved = GET_NUM_RESERVED_MISSION_OBJECTS(FALSE)
	IF serverBD.iReservedObjects != iCurrentlyReserved
		IF serverBD.iReservedObjects < iCurrentlyReserved
		OR CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(serverBD.iReservedObjects, FALSE, TRUE)
			IF serverBD.iReservedObjects >= GET_NUM_CREATED_MISSION_OBJECTS()
				RESERVE_NETWORK_MISSION_OBJECTS(serverBD.iReservedObjects)
				PRINTLN("[GB PROTECTION RACKET] [STMGB] - MAINTAIN_RESERVED_ENTITIES - RESERVED OBJECTS = ", serverBD.iReservedObjects)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_WANTED_LEVEL()
	IF HAS_PLAYER_COLLECTED_A_BAG(PARTICIPANT_ID_TO_INT())
	AND NOT HAS_PLAYER_DELIVERED_A_BAG(PARTICIPANT_ID_TO_INT())
		SET_LOCAL_WANTED(TRUE)
		UPDATE_WANTED_POSITION_THIS_FRAME(PLAYER_ID())
		SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID())
	ELIF HAS_PLAYER_DELIVERED_A_BAG(PARTICIPANT_ID_TO_INT())
		IF bClearWanted = FALSE
			SET_LOCAL_WANTED(FALSE)
			bClearWanted = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_MAXIMUM_CASH()
	IF HAS_PLAYER_COLLECTED_A_BAG(PARTICIPANT_ID_TO_INT())
		IF NOT HAS_PLAYER_DELIVERED_A_BAG(PARTICIPANT_ID_TO_INT())
			IF playerBD[PARTICIPANT_ID_TO_INT()].iCashCollected > PR_TUNABLE_BAG_VALUE()
				PRINTLN("[GB PROTECTION RACKET] [STMGB] - MAINTAIN_MAXIMUM_CASH - player has more than $", PR_TUNABLE_BAG_VALUE(), ", setting player cash to $", PR_TUNABLE_BAG_VALUE())
				playerBD[PARTICIPANT_ID_TO_INT()].iCashCollected = PR_TUNABLE_BAG_VALUE()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_LOSING_CASH_IN_AIRCRAFT()
	IF NOT HAS_PLAYER_DELIVERED_A_BAG(PARTICIPANT_ID_TO_INT())
		VEHICLE_INDEX thisVeh
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			IF IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
			OR IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
				thisVeh = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
				IF IS_ENTITY_IN_AIR(thisVeh)
				OR GET_ENTITY_SPEED(PLAYER_PED_ID()) >= PR_TUNABLE_AIRCRAFT_SPEED_THRESHOLD()
					IF NOT HAS_NET_TIMER_STARTED(AircraftTimer)
						START_NET_TIMER(AircraftTimer)
						PRINTLN("[GB PROTECTION RACKET] [STMGB] - MAINTAIN_LOSING_CASH_IN_AIRCRAFT - Player has entered aircraft and is now in air, starting timer")
					ELSE
						IF HAS_NET_TIMER_EXPIRED(AircraftTimer, PR_TUNABLE_AIRCRAFT_TIME())
							DO_HELP_TEXT(5)
							PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - MAINTAIN_LOSING_CASH_IN_AIRCRAFT - ***************************************************************")
							PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - MAINTAIN_LOSING_CASH_IN_AIRCRAFT - Dropping cash in aircraft, cash before $", GET_PLAYER_CASH_COLLECTED(PARTICIPANT_ID_TO_INT()))
							PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - MAINTAIN_LOSING_CASH_IN_AIRCRAFT - Dropping cash in aircraft, dropping $", PR_TUNABLE_DAMAGE_AIRCRAFT())
							playerBD[PARTICIPANT_ID_TO_INT()].iCashCollected -= PR_TUNABLE_DAMAGE_AIRCRAFT()
							IF playerBD[PARTICIPANT_ID_TO_INT()].iCashCollected < PR_TUNABLE_MINIMUM_CASH()
								PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - MAINTAIN_LOSING_CASH_IN_AIRCRAFT - player cash has dropped below $", PR_TUNABLE_MINIMUM_CASH(), ", keeping at $", PR_TUNABLE_MINIMUM_CASH())
								playerBD[PARTICIPANT_ID_TO_INT()].iCashCollected = PR_TUNABLE_MINIMUM_CASH()
							ENDIF
							PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - MAINTAIN_LOSING_CASH_IN_AIRCRAFT - Dropping cash in aircraft, remaining cash is now $", GET_PLAYER_CASH_COLLECTED(PARTICIPANT_ID_TO_INT()))
							PRINTLN("[PROTECTION_RACKET_DAMAGE_EVENT] [CLIENT] - MAINTAIN_LOSING_CASH_IN_AIRCRAFT - ***************************************************************")
							REINIT_NET_TIMER(AircraftTimer)
						ENDIF
					ENDIF
				ELSE
					IF HAS_NET_TIMER_STARTED(AircraftTimer)
						RESET_NET_TIMER(AircraftTimer)
						PRINTLN("[GB PROTECTION RACKET] [STMGB] - MAINTAIN_LOSING_CASH_IN_AIRCRAFT - Player is still in aircraft but no longer in the air or moving, resetting timer")
					ENDIF
				ENDIF
			ELSE
				IF HAS_NET_TIMER_STARTED(AircraftTimer)
					RESET_NET_TIMER(AircraftTimer)
					PRINTLN("[GB PROTECTION RACKET] [STMGB] - MAINTAIN_LOSING_CASH_IN_AIRCRAFT - Player is no longer in an aircraft, resetting timer")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_PROTECTION_RACKET_SORTED_PARTICIPANTS()

	// No need to update if we're in sync with the server
	IF localSyncID = serverBD.iServerSyncID
		EXIT
	ENDIF

	g_GBLeaderboardStruct.dpadVariables.eDpadVarType = DPAD_VAR_CASH
	
	INT i
	REPEAT GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS() i
		g_GBLeaderboardStruct.challengeLbdStruct[i].playerID = serverBD.sSortedLeaderboard[i].playerID
		g_GBLeaderboardStruct.challengeLbdStruct[i].iScore = serverBD.sSortedLeaderboard[i].iScore
		g_GBLeaderboardStruct.challengeLbdStruct[i].iFinishTime = -1
	ENDREPEAT
	
	// Update our sync ID to be the same as the servers
	localSyncID = serverBD.iServerSyncID
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[PROTECTION RACKET LBD UPDATE] [STMGB] [CLIENT] - MAINTAIN_PROTECTION_RACKET_SORTED_PARTICIPANTS - Performing a leaderboard update. LocalSyncID updated to ", localSyncID)
	INT iDebugLoop
	REPEAT GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS()	iDebugLoop
		IF g_GBLeaderboardStruct.challengeLbdStruct[iDebugLoop].playerID != INVALID_PLAYER_INDEX()
			//IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(g_GBLeaderboardStruct.challengeLbdStruct[iDebugLoop].iParticipant))
				PRINTLN("[PROTECTION RACKET LBD UPDATE] [STMGB] [CLIENT] - MAINTAIN_PROTECTION_RACKET_SORTED_PARTICIPANTS - |--------------------------- >| ")
				PRINTLN("[PROTECTION RACKET LBD UPDATE] [STMGB] [CLIENT] - MAINTAIN_PROTECTION_RACKET_SORTED_PARTICIPANTS - Position: ", iDebugLoop)
				PRINTLN("[PROTECTION RACKET LBD UPDATE] [STMGB] [CLIENT] - MAINTAIN_PROTECTION_RACKET_SORTED_PARTICIPANTS - Player: ", GET_PLAYER_NAME(g_GBLeaderboardStruct.challengeLbdStruct[iDebugLoop].playerID))
				PRINTLN("[PROTECTION RACKET LBD UPDATE] [STMGB] [CLIENT] - MAINTAIN_PROTECTION_RACKET_SORTED_PARTICIPANTS - Score: ", g_GBLeaderboardStruct.challengeLbdStruct[iDebugLoop].iScore)
				PRINTLN("[PROTECTION RACKET LBD UPDATE] [STMGB] [CLIENT] - MAINTAIN_PROTECTION_RACKET_SORTED_PARTICIPANTS - |--------------------------- |< ")
			//ENDIF
		ENDIF
	ENDREPEAT
	#ENDIF

ENDPROC

PROC MAINTAIN_DPAD_LEADERBOARD()
	DRAW_CHALLENGE_DPAD_LBD(g_GBLeaderboardStruct.challengeLbdStruct, g_GBLeaderboardStruct.siDpadMovie, SUB_BOSS, g_GBLeaderboardStruct.dpadVariables, 
							g_GBLeaderboardStruct.fmDpadStruct)
ENDPROC

PROC MAINTAIN_COUNTDOWN()
	IF HAS_NET_TIMER_STARTED(serverBD.ChallengeDuration)
		IF iTimeRemaining  <= 35000
			SWITCH eFinalCountdownStage
				CASE eFCOUNTDOWN_PREP
					IF GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT()) = eMS_COLLECT
					OR GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT()) = eMS_DELIVER
						// IF within a second of hitting the end prep period
						IF iTimeRemaining <= (35000)
						AND iTimeRemaining >= (29000)
							IF PREPARE_MUSIC_EVENT("APT_PRE_COUNTDOWN_STOP")
								PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_YACHT_ATTACK_MUSIC - eFCOUNTDOWN_PREP - APT_PRE_COUNTDOWN_STOP has been triggered")
								TRIGGER_MUSIC_EVENT("APT_PRE_COUNTDOWN_STOP")
								eFinalCountdownStage = eFCOUNTDOWN_START
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE eFCOUNTDOWN_START
					IF NOT IS_BIT_SET(iFinalCountdownBitSet, iFCBS_TriggeredCountdown)
						IF iTimeRemaining <= 30000
							IF PREPARE_MUSIC_EVENT("APT_COUNTDOWN_30S")
								SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE)
								SET_USER_RADIO_CONTROL_ENABLED(FALSE)
								TRIGGER_MUSIC_EVENT("APT_COUNTDOWN_30S")
								PREPARE_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
								PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_YACHT_ATTACK_MUSIC - eFCOUNTDOWN_START - APT_COUNTDOWN_30S has been triggered")
								SET_BIT(iFinalCountdownBitSet, iFCBS_TriggeredCountdown)
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_BIT_SET(iFinalCountdownBitSet, iFCBS_ReEnabledRadioControl)
							IF iTimeRemaining <= 27000
								SET_USER_RADIO_CONTROL_ENABLED(TRUE)
								SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
								PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_YACHT_ATTACK_MUSIC - eFCOUNTDOWN_START - Radio control re-enabled")
								SET_BIT(iFinalCountdownBitSet, iFCBS_ReEnabledRadioControl)
								eFinalCountdownStage = eFCOUNTDOWN_END_OR_KILL
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE eFCOUNTDOWN_END_OR_KILL
					IF NOT IS_BIT_SET(iFinalCountdownBitSet, iFCBS_TriggeredCountdownKill)
						IF NOT IS_BIT_SET(iFinalCountdownBitSet, iFCBS_Trigger5s)
							IF iTimeRemaining <= 5000
								PLAY_SOUND_FRONTEND(-1, "5s", "MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
								SET_BIT(iFinalCountdownBitSet, iFCBS_Trigger5s)
							ENDIF
						ENDIF
						
						IF iTimeRemaining <= 0
							CANCEL_MUSIC_EVENT("APT_COUNTDOWN_30S")
							IF PREPARE_MUSIC_EVENT("APT_FADE_IN_RADIO")
								TRIGGER_MUSIC_EVENT("APT_FADE_IN_RADIO")
								CANCEL_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
								PREPARE_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
								PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_YACHT_ATTACK_MUSIC - eFCOUNTDOWN_END_OR_KILL - Time ran out, APT_FADE_IN_RADIO has been triggered")
								SET_BIT(iFinalCountdownBitSet, iFCBS_TriggeredCountdownKill)
							ENDIF
						ELSE
							IF IS_BIT_SET(serverBD.iServerBitSet, biS_BossLeft)
								IF PREPARE_MUSIC_EVENT("APT_FADE_IN_RADIO")
									CANCEL_MUSIC_EVENT("APT_COUNTDOWN_30S")
									TRIGGER_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
									TRIGGER_MUSIC_EVENT("APT_FADE_IN_RADIO")
									PREPARE_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
									PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_YACHT_ATTACK_MUSIC - eFCOUNTDOWN_END_OR_KILL - Mode ended before timeout, APT_COUNTDOWN_30S_KILL and APT_FADE_IN_RADIO have been triggered")
									SET_BIT(iFinalCountdownBitSet, iFCBS_TriggeredCountdownKill)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

// ------------------------------------------------------ |<

PROC PROCESS_CLIENT()

	MAINTAIN_MAXIMUM_CASH()
	MAINTAIN_LOSING_CASH_IN_AIRCRAFT()
	MAINTAIN_PROTECTION_RACKET_SORTED_PARTICIPANTS()
	MAINTAIN_DPAD_LEADERBOARD()
	MAINTAIN_COUNTDOWN()
	
	MAINTAIN_BLIPS()
	MAINTAIN_WANTED_LEVEL()
	MAINTAIN_OBJECTIVE_TEXT()
	MAINTAIN_BOTTOM_RIGHT_UI()
	MAINTAIN_RESERVED_ENTITIES()
	MAINTAIN_CHANGE_STAGE_CHECKS()
	
	SWITCH GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT())
	
		CASE eMS_INIT
			IF GET_SERVER_MISSION_STAGE() = eMS_COLLECT
				IF GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT()) != eMS_COLLECT
				
					GB_SET_LOCAL_PLAYER_CRITICAL_TO_JOB(TRUE)
					SET_FREEMODE_FLOW_COMMS_DISABLED(TRUE)
					GB_SET_COMMON_TELEMETRY_DATA_ON_START()
				
					SET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT(), eMS_COLLECT)
				ENDIF
			ENDIF
		BREAK 
		
		CASE eMS_COLLECT
			HANDLE_INTRO_MESSAGE()
			CONTROL_BAG_COLLECTION_CLIENT()
			DRAW_BAG_MARKERS()
			IF HAS_PLAYER_COLLECTED_A_BAG(PARTICIPANT_ID_TO_INT())
				SET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT(), eMS_DELIVER)
			ENDIF
		BREAK
		
		CASE eMS_DELIVER
			DO_HELP_TEXT(3)
			
			HANDLE_CASH_DELIVERY()
			DRAW_DROP_OFF_MARKER()
			IF HAS_PLAYER_DELIVERED_A_BAG(PARTICIPANT_ID_TO_INT())
				SET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT(), eMS_WAIT)
			ENDIF
		BREAK
		
		CASE eMS_WAIT
		BREAK
		
		CASE eMS_END
			IF IS_BIT_SET(g_GB_WORK_VARS.iEventBitSet, ciGB_BOSS_SHARD_SAFE_TO_DISPLAY)
				GIVE_END_CASH_TO_WINNER()
				PROCESS_CHALLENGE_END_REWARDS_AND_MESSAGES()
			ENDIF
			IF GB_MAINTAIN_BOSS_END_UI(sEndUI)
				SET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT(), GAME_STATE_END)
			ENDIF
		BREAK 
		
		CASE eMS_CLEANUP
		BREAK 

	ENDSWITCH

ENDPROC

PROC HANDLE_BOSS_LEAVE_SESSION()
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_BossLeft)
		IF NOT NETWORK_IS_PLAYER_ACTIVE(GET_GANG_BOSS_PLAYER_INDEX())
			SET_BIT(serverBD.iServerBitSet, biS_BossLeft)
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_CLIENT_BAG_COLLECTION()
	IF NOT HAVE_ALL_BAGS_BEEN_COLLECTED()
		INT i
		REPEAT GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS() i
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
				IF NOT IS_BIT_SET(serverBD.iCollectedBagBitSet, i)
					IF IS_BIT_SET(playerBD[i].iPlayerBitSet, biP_CollectedBag)
						PRINTLN("[GB PROTECTION RACKET] [STMGB] [SERVER] - HANDLE_CLIENT_BAG_COLLECTION - ", GET_PLAYER_NAME(INT_TO_NATIVE(PLAYER_INDEX, i)), " has collected a package, adding them to sorted leaderboard.")
						SORT_PROTECTION_RACKET_PLAYER_SCORES(NETWORK_GET_PLAYER_INDEX(INT_TO_NATIVE(PARTICIPANT_INDEX, i)), GET_PLAYER_CASH_COLLECTED(i))
						serverBD.iScoreboardPlayerScore[i] = GET_PLAYER_CASH_COLLECTED(i)
						serverBD.iBagsCollected++
						SET_BIT(serverBD.iCollectedBagBitSet, i)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

PROC HANDLE_ALL_PACKAGES_COLLECTED()
	
	IF GB_IS_THIS_PLAYER_USING_SPECTATE(GET_GANG_BOSS_PLAYER_INDEX())
		INT i
		REPEAT NUM_NETWORK_PLAYERS i
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
				PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
				IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerID, GET_GANG_BOSS_PLAYER_INDEX(), FALSE)
					IF NOT IS_BIT_SET(playerBD[i].iPlayerBitSet, biP_CollectedBag)
						IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllBagsCollected)
							CLEAR_BIT(serverBD.iServerBitSet, biS_AllBagsCollected)
						ENDIF	
						EXIT
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ELSE
		INT i
		REPEAT NUM_NETWORK_PLAYERS i
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
				PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
				IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerID, GET_GANG_BOSS_PLAYER_INDEX())
					IF NOT IS_BIT_SET(playerBD[i].iPlayerBitSet, biP_CollectedBag)
						IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllBagsCollected)
							CLEAR_BIT(serverBD.iServerBitSet, biS_AllBagsCollected)
						ENDIF	
						EXIT
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AllBagsCollected)
		SET_BIT(serverBD.iServerBitSet, biS_AllBagsCollected)
	ENDIF
	
ENDPROC

FUNC BOOL DETERMINE_WINNER()

	INT i
	INT iHighscore = 0
	REPEAT NUM_NETWORK_PLAYERS i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			PLAYER_INDEX Player = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(Player, GET_GANG_BOSS_PLAYER_INDEX())
				IF GET_PLAYER_CASH_DELIVERED(i) > 0		
					serverBD.iBagsDelivered++
				ENDIF
				IF GET_PLAYER_CASH_DELIVERED(i) > iHighscore		// New winner
					iHighscore = GET_PLAYER_CASH_DELIVERED(i)
					serverBD.piWinner = Player
					serverBD.piTiedWinner = INVALID_PLAYER_INDEX()
				//ELIF GET_PLAYER_CASH_DELIVERED(i) = iHighscore		// Winner has same highscore that isnt 0
				//AND iHighscore > 0
				//	serverBD.piTiedWinner = serverBD.piWinner
				//	serverBD.piWinner = Player
				//	serverBD.iBagsDelivered++
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
		
	REPEAT NUM_NETWORK_PLAYERS i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			PLAYER_INDEX Player = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(Player, GET_GANG_BOSS_PLAYER_INDEX())
				IF NOT HAS_PLAYER_DELIVERED_A_BAG(i)
					IF Player != serverBD.piWinner
					AND Player != serverBD.piTiedWinner
						SORT_PROTECTION_RACKET_PLAYER_SCORES(Player, 0)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
		
	// No one scored any cash
	IF iHighscore = 0
		PRINTLN("[GB PROTECTION RACKET] [STMGB] - DETERMINE_WINNER - biS_ChallengeNoScore set - no players collected more than $0")
		SET_BIT(serverBD.iServerBitSet, biS_ChallengeNoScore)
	ELIF (iHighscore > 0) AND (GET_WINNER() != INVALID_PLAYER_INDEX())	AND (GET_TIED_WINNER() != INVALID_PLAYER_INDEX())
		PRINTLN("[GB PROTECTION RACKET] [STMGB] - DETERMINE_WINNER - biS_ChallengeTied - two players collected the same amount of cash, they were ", GET_PLAYER_NAME(GET_WINNER()), " and ", GET_PLAYER_NAME(GET_TIED_WINNER()))
		SET_BIT(serverBD.iServerBitSet, biS_ChallengeTied)
	ELSE
		PRINTLN("[GB PROTECTION RACKET] [STMGB] - DETERMINE_WINNER - biS_StandardWinner - there was a winner, they were ", GET_PLAYER_NAME(GET_WINNER()))
		SET_BIT(serverBD.iServerBitSet, biS_StandardWinner)
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL HAS_ALL_CASH_BEEN_DELIVERED()
	// Only check if all packages have been collected
	IF HAVE_ALL_BAGS_BEEN_COLLECTED()
		INT i
		
		IF GB_IS_THIS_PLAYER_USING_SPECTATE(GET_GANG_BOSS_PLAYER_INDEX())
			REPEAT NUM_NETWORK_PLAYERS i
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
					IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(INT_TO_PLAYERINDEX(i), GET_GANG_BOSS_PLAYER_INDEX(), FALSE)
						IF GET_PLAYER_CASH_DELIVERED(i) = 0
							RETURN FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ELSE
			REPEAT NUM_NETWORK_PLAYERS i
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
					IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(INT_TO_PLAYERINDEX(i), GET_GANG_BOSS_PLAYER_INDEX())
						IF GET_PLAYER_CASH_DELIVERED(i) = 0
							RETURN FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC HANDLE_PLAYER_LEAVING_LEADERBOARD_UPDATE()
	IF MAINTAIN_PLAYER_LEAVING_AMBIENT_ACTIVITY_LEADERBOARD(serverBD.sSortedLeaderboard, serverStaggeredPlayerLoop, AAL_SORT_NON_UNIFORM_SCORING)
		serverBD.iServerSyncId++
	ENDIF
ENDPROC

PROC HANDLE_LEADERBOARD_UPDATES()

	REPEAT NUM_NETWORK_PLAYERS iLeaderboardStagger
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iLeaderboardStagger))
			PLAYER_INDEX PlayerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iLeaderboardStagger))
			IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PlayerID, GET_GANG_BOSS_PLAYER_INDEX())
				IF HAS_PLAYER_COLLECTED_A_BAG(iLeaderboardStagger)
					IF serverBD.iScoreboardPlayerScore[iLeaderboardStagger] != GET_PLAYER_CASH_COLLECTED(iLeaderboardStagger)
						PRINTLN("[GB PROTECTION RACKET] [STMGB] [SERVER] - HANDLE_LEADERBOARD_UPDATES - Player ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iLeaderboardStagger))), "'s score has changed, updating leaderboard")
						SORT_PROTECTION_RACKET_PLAYER_SCORES(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iLeaderboardStagger)), GET_PLAYER_CASH_COLLECTED(iLeaderboardStagger))
						serverBD.iScoreboardPlayerScore[iLeaderboardStagger] = GET_PLAYER_CASH_COLLECTED(iLeaderboardStagger)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

//	
//	iLeaderboardStagger++
//	
//	IF iLeaderboardStagger >= GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS()
//		iLeaderboardStagger = ZERO_SCORE
//	ENDIF
ENDPROC

FUNC BOOL SHOULD_MOVE_TO_CHALLENGE_END()
	#IF IS_DEBUG_BUILD
	IF bExpireTimer
		PRINTLN("[GB PROTECTION RACKET] [STMGB] [SERVER] - SHOULD_MOVE_TO_CHALLENGE_END - DEBUG - bExpireTimer is TRUE, debug skipped to end of timer.")
		RETURN TRUE
	ENDIF
	#ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_BossLeft)
		PRINTLN("[GB PROTECTION RACKET] [STMGB] [SERVER] - SHOULD_MOVE_TO_CHALLENGE_END - biS_BossLeft is SET, boss has left the session")
		RETURN TRUE
	ENDIF
	
	IF NETWORK_GET_NUM_PARTICIPANTS() >= 2
			
		IF HAS_ALL_CASH_BEEN_DELIVERED()
			PRINTLN("[GB PROTECTION RACKET] [STMGB] [SERVER] - SHOULD_MOVE_TO_CHALLENGE_END - HAS_ALL_CASH_BEEN_DELIVERED, all packages have been collected and all collected cash delivered")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_SERVER()
	
	HANDLE_BOSS_LEAVE_SESSION()
	
	SWITCH GET_SERVER_MISSION_STAGE()
	
		CASE eMS_INIT
		BREAK 
		
		CASE eMS_COLLECT
			IF NOT HAS_NET_TIMER_STARTED(serverBD.ChallengeDuration)
				PRINTLN("[GB PROTECTION RACKET] [STMGB] [SERVER] - PROCESS_SERVER - ChallengeDuration timer has been started")
				PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBD.iMatchId1, serverBD.iMatchId2)
				START_NET_TIMER(serverBD.ChallengeDuration)
			ELSE
				HANDLE_PLAYER_LEAVING_LEADERBOARD_UPDATE()
				HANDLE_CLIENT_BAG_COLLECTION()
				HANDLE_LEADERBOARD_UPDATES()
				HANDLE_ALL_PACKAGES_COLLECTED()
										
				IF HAS_NET_TIMER_EXPIRED(serverBD.ChallengeDuration, PR_TUNABLE_CHALLENGE_DURATION())
				OR SHOULD_MOVE_TO_CHALLENGE_END()
					IF DETERMINE_WINNER()
						SET_BIT(serverBD.iServerBitSet, biS_ChallengeEnded)
						PRINTLN("[GB PROTECTION RACKET] [STMGB] [SERVER] - PROCESS_SERVER - ChallengeDuration timer has expired")
						SET_SERVER_MISSION_STAGE(eMS_END)
					ENDIF
				ENDIF
			ENDIF
		BREAK 
		
		CASE eMS_END
//			IF NOT HAS_NET_TIMER_STARTED(serverBD.ChallengeEnd)
//				PRINTLN("[GB PROTECTION RACKET] [STMGB] [SERVER] - PROCESS_SERVER - ChallengeEnd timer has been started")
//				START_NET_TIMER(serverBD.ChallengeEnd)
//			ELSE
//				IF HAS_NET_TIMER_EXPIRED(serverBD.ChallengeEnd, TWELVE_SECONDS)
//					PRINTLN("[GB PROTECTION RACKET] [STMGB] [SERVER] - PROCESS_SERVER - ChallengeEnd timer has expired")
//					SET_SERVER_MISSION_STAGE(eMS_CLEANUP)
//				ENDIF
//			ENDIF
		BREAK 
		
		CASE eMS_CLEANUP
			SET_SERVER_MISSION_STATE(GAME_STATE_END)
		BREAK  

	ENDSWITCH
ENDPROC

FUNC VECTOR GET_BAG_CREATION_LOCATION(INT iBag)
	SWITCH serverBD.iQuadrant
		CASE 0
			SWITCH serverBD.iStartArea
				CASE 0
					SWITCH iBag
						CASE 0	RETURN <<196.8648, 306.5313, 104.439>>
						CASE 1 	RETURN <<178.5707, 294.8304, 104.3693>>
						CASE 2 	RETURN <<133.0388, 271.8213, 108.9741>>
						CASE 3 	RETURN <<120.2636, 318.7129, 111.1490>>
					ENDSWITCH
				BREAK
				
				CASE 1
					SWITCH iBag
						CASE 0	RETURN <<381.0913, -330.5866, 45.7751>>
						CASE 1 	RETURN <<356.8358, -345.9093, 45.6993>>
						CASE 2 	RETURN <<383.5171, -371.5749, 45.9010>>
						CASE 3 	RETURN <<410.2855, -346.3357, 46.0961>>
					ENDSWITCH
				BREAK
				
				CASE 2
					SWITCH iBag
						CASE 0	RETURN <<1169.7893, -426.8626, 66.0957>> 
						CASE 1 	RETURN <<1162.4958, -455.5855, 65.9844>> 
						CASE 2 	RETURN <<1126.6486, -470.1475, 65.4874>> 
						CASE 3 	RETURN <<1172.5502, -376.8397, 67.2645>>  
					ENDSWITCH
				BREAK
				
				CASE 3
					SWITCH iBag
						CASE 0	RETURN <<257.6294, -829.1342, 28.6134>> 
						CASE 1 	RETURN <<308.1159, -906.4449,  28.2937>>
						CASE 2 	RETURN <<346.0308, -977.9252,  28.3693>>
						CASE 3 	RETURN <<267.8598, -973.4060, 28.3289>> 
					ENDSWITCH
				BREAK
				
				CASE 4
					SWITCH iBag
						CASE 0	RETURN <<1154.8313, -1662.8439, 35.6013>>
						CASE 1 	RETURN <<1169.6006, -1641.7179, 35.9196>>
						CASE 2 	RETURN <<1226.1891, -1601.8246, 50.9889>>
						CASE 3 	RETURN <<1239.2124, -1602.1007, 51.6289>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE 1
			SWITCH serverBD.iStartArea
				CASE 0
					SWITCH iBag
						CASE 0	RETURN <<-1445.0811, -258.1274, 45.2077>>
						CASE 1 	RETURN <<-1417.8315, -255.1478, 45.3791>> 
						CASE 2 	RETURN <<-1471.9943, -327.5423, 43.8052>>
						CASE 3 	RETURN <<-1446.6648, -350.9199, 43.7134>>
					ENDSWITCH
				BREAK
				
				CASE 1
					SWITCH iBag
						CASE 0	RETURN <<-534.3468, -45.9880, 41.4476>>
						CASE 1 	RETURN <<-512.4194, -40.7504, 43.5177>>
						CASE 2 	RETURN <<-487.0814, -61.7978, 38.9942>>
						CASE 3 	RETURN <<-459.5929, -59.5144, 43.5184>>
					ENDSWITCH
				BREAK
				
				CASE 2
					SWITCH iBag
						CASE 0	RETURN <<-1264.1821, -657.2690, 25.6779>>
						CASE 1 	RETURN <<-1237.1492, -691.6292, 22.8884>>
						CASE 2 	RETURN <<-1199.4276, -736.7727, 19.7697>>
						CASE 3 	RETURN <<-1166.3737, -701.0263, 20.7938>> 
					ENDSWITCH
				BREAK
				
				CASE 3
					SWITCH iBag
						CASE 0	RETURN <<-693.7319, -814.6637, 22.8756>>
						CASE 1 	RETURN <<-715.4158, -866.6309, 22.2478>> 
						CASE 2 	RETURN <<-761.3768, -866.6451, 20.2405>>
						CASE 3 	RETURN <<-761.6913, -908.4113, 18.6235>>
					ENDSWITCH
				BREAK
				
				CASE 4
					SWITCH iBag
						CASE 0	RETURN <<-830.8759, -1115.6724, 7.9698>>
						CASE 1 	RETURN <<-841.8519, -1127.8085, 5.9695>>
						CASE 2 	RETURN <<-914.8298, -1169.0981, 3.8773>>
						CASE 3 	RETURN <<-924.6203, -1162.3802, 3.7942>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE 2
			SWITCH serverBD.iStartArea
				CASE 0
					SWITCH iBag
						CASE 0	RETURN <<-176.5558, 6379.3940, 30.4825>>
						CASE 1 	RETURN <<-213.5764, 6359.0361, 30.4923>>
						CASE 2 	RETURN <<-168.3186, 6311.2954, 30.3631>>
						CASE 3 	RETURN <<-126.2959, 6343.3218, 30.4904>>
					ENDSWITCH
				BREAK
				
				CASE 1
					SWITCH iBag
						CASE 0	RETURN <<102.7475, 6611.0820, 30.8621>>
						CASE 1 	RETURN <<79.3845, 6643.7290, 30.9189>>
						CASE 2 	RETURN <<124.6892, 6644.6733, 30.7884>>
						CASE 3 	RETURN <<176.5681, 6642.6064, 30.5445>>
					ENDSWITCH
				BREAK
				
				CASE 2
					SWITCH iBag
						CASE 0	RETURN <<-687.3630, 5827.3296, 16.3313>>
						CASE 1 	RETURN <<-679.7361, 5799.8242, 16.3310>>
						CASE 2 	RETURN <<-687.5652, 5789.1230, 16.3310>>
						CASE 3 	RETURN <<-703.2263, 5789.8145, 16.5268>>
					ENDSWITCH
				BREAK
				
				CASE 3
					SWITCH iBag
						CASE 0	RETURN <<104.3236, 3626.6160, 38.7549>>
						CASE 1 	RETURN <<98.8521, 3597.7566, 38.7934>>
						CASE 2 	RETURN <<64.0722, 3604.3054, 38.8482>>
						CASE 3 	RETURN <<37.1284, 3623.2690, 39.0005>>
					ENDSWITCH
				BREAK
				
				CASE 4
					SWITCH iBag
						CASE 0	RETURN <<-2173.4287, 4281.2617, 48.0967>>
						CASE 1 	RETURN <<-2178.2112, 4259.8320, 47.9569>>
						CASE 2 	RETURN <<-2192.9714, 4253.2192, 47.0714>>
						CASE 3 	RETURN <<-2228.9270, 4222.2231, 45.9063>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE 3
			SWITCH serverBD.iStartArea
				CASE 0
					SWITCH iBag
						CASE 0	RETURN <<1481.7599, 6409.4233, 21.4856>>
						CASE 1 	RETURN <<1582.6449, 6464.3086, 24.3172>>
						CASE 2 	RETURN <<1460.4758, 6538.7290, 13.9413>>
						CASE 3 	RETURN <<1469.5619, 6554.4570, 12.9881>>
					ENDSWITCH
				BREAK
				
				CASE 1
					SWITCH iBag
						CASE 0	RETURN <<1645.4393, 4844.1558, 41.0223>>
						CASE 1 	RETURN <<1696.0229, 4785.2974, 41.0176>>
						CASE 2 	RETURN <<1680.1492, 4663.0874, 42.3712>>
						CASE 3 	RETURN <<1719.3732, 4712.1060, 41.2466>>
					ENDSWITCH
				BREAK
				
				CASE 2
					SWITCH iBag
						CASE 0	RETURN <<2520.8442, 4113.3120, 37.6307>>
						CASE 1 	RETURN <<2506.3149, 4081.5896, 37.6597>>
						CASE 2 	RETURN <<2466.7051, 4101.6362, 37.0647>>
						CASE 3 	RETURN <<2439.2432, 4068.1868, 37.0647>>
					ENDSWITCH
				BREAK
				
				CASE 3
					SWITCH iBag
						CASE 0	RETURN <<1864.3402, 3793.0598, 31.8395>>
						CASE 1 	RETURN <<1879.7584, 3763.6511, 31.9295>>
						CASE 2 	RETURN <<1894.0667, 3713.6611, 31.7826>>
						CASE 3 	RETURN <<1930.8267, 3720.7332, 31.8437>>
					ENDSWITCH
				BREAK
				
				CASE 4
					SWITCH iBag
						CASE 0	RETURN <<2223.3792, 5604.2012, 53.7326>>
						CASE 1 	RETURN <<2220.0378, 5614.4346, 53.7207>>
						CASE 2 	RETURN <<2195.8948, 5601.1919, 52.6195>>
						CASE 3 	RETURN <<2233.3013, 5610.1265, 53.6152>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN <<0,0,0>>
ENDFUNC

FUNC BOOL CREATE_BAGS()
	INT i
	REPEAT PR_TUNABLE_NUM_BAGS() i
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niBag[i])
			IF CAN_REGISTER_MISSION_OBJECTS(1)
				IF NOT IS_VECTOR_ZERO(GET_BAG_CREATION_LOCATION(i))
					serverBD.niBag[i] = OBJ_TO_NET(CREATE_PORTABLE_PICKUP(PICKUP_PORTABLE_CRATE_FIXED_INCAR, GET_BAG_CREATION_LOCATION(i), TRUE, serverBD.BagModel))
					SET_ENTITY_COORDS_NO_OFFSET(NET_TO_OBJ(serverBD.niBag[i]), GET_BAG_CREATION_LOCATION(i))
					
					#IF IS_DEBUG_BUILD
					VECTOR tempVec = GET_BAG_CREATION_LOCATION(i)
					PRINTLN("[GB PROTECTION RACKET] [STMGB] - CREATE_BAGS - Bag ", i, " has been created at ", tempVec, " with model ", GET_MODEL_NAME_FOR_DEBUG(serverBD.BagModel))
					#ENDIF
					
					NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_OBJ(serverBD.niBag[i]), TRUE)
					SET_ENTITY_INVINCIBLE(NET_TO_OBJ(serverBD.niBag[i]), TRUE)
					SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(NET_TO_OBJ(serverBD.niBag[i]), TRUE)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_OBJ(serverBD.niBag[i]), FALSE)
					ACTIVATE_PHYSICS(NET_TO_OBJ(serverBD.niBag[i]))
					SET_ENTITY_VISIBLE(NET_TO_OBJ(serverBD.niBag[i]), TRUE)
					SET_ENTITY_LOD_DIST(NET_TO_OBJ(serverBD.niBag[i]), 1200)
					SET_ENTITY_ALPHA(NET_TO_OBJ(serverBD.niBag[i]), 255, TRUE)
					
				ELSE
					PRINTLN("[GB PROTECTION RACKET] [STMGB] [SERVER] - CREATE_BAGS - Attempting to create bag at <<0,0,0>>, not creating bag ", i)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	i = 0
	
	REPEAT PR_TUNABLE_NUM_BAGS() i
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niBag[i])
			PRINTLN("[GB PROTECTION RACKET] [STMGB] [SERVER] - CREATE_BAGS - Bag ", i, " has not been created. Returning false")
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CREATE_PROTECTION_RACKET_BAGS()
	IF REQUEST_LOAD_MODEL(serverBD.BagModel)
		IF CREATE_BAGS()
			SET_MODEL_AS_NO_LONGER_NEEDED(serverBD.BagModel)
			PRINTLN("[GB PROTECTION RACKET] [STMGB] [SERVER] - CREATE_PROTECTION_RACKET_BAGS - Bags have been created! ")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DETERMINE_DROP_OFF_LOCATION()
	INT temp
	SWITCH serverBD.iQuadrant
		CASE 0
		CASE 1
			temp =  GET_RANDOM_INT_IN_RANGE(0,3)
			serverBD.vDropOffCoords = vDropOffLocations[temp]
			serverBD.iDropArea = temp
		BREAK
		
		CASE 2
		CASE 3
			temp = GET_RANDOM_INT_IN_RANGE(3,6)
			serverBD.vDropOffCoords = vDropOffLocations[temp]
			serverBD.iDropArea = temp
		BREAK
		
	ENDSWITCH
	
	IF IS_VECTOR_ZERO(serverBD.vDropOffCoords)
	OR serverBD.iDropArea = -1
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SELECT_AREA_WITHIN_QUADRANT()
	IF serverBD.iStartArea = -1
		serverBD.iStartArea = GET_RANDOM_INT_IN_RANGE(0, NUM_START_AREAS_PER_QUADRANT)
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL GET_VECTORS_FOR_QUADRANT_CHECK(INT iQuadrant, VECTOR& v1, VECTOR& v2)
	SWITCH iQuadrant	
		CASE 0 	
			v1 =  <<283.0, -3946.0, 0.0>> 
			v2 =  <<4500, 1924.0,0.0>> 
			RETURN TRUE
		
		CASE 1 	
			v1 =  <<-3747.0, -3946.00, 0.0>> 
			v2 =  <<550.0, 1868.00,0.0>> 
			RETURN TRUE
		
		CASE 2
			v1 =  <<-3747.00, 1540.00, 0.0>> 
			v2 =  <<452.5, 8022.00,0.0>> 
			RETURN TRUE
		
		CASE 3
			v1 =  <<268.0, 1946.0, 0.0>> 
			v2 =  <<4500.0, 8022.0,0.0>> 
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


FUNC BOOL DETERMINE_QUADRANT()
	INT iQuadrant
	VECTOR vVecOne, vVecTwo
	REPEAT NUM_MAP_QUADRANTS iQuadrant
		IF GET_VECTORS_FOR_QUADRANT_CHECK(iQuadrant, vVecOne, vVecTwo)
			IF IS_ENTITY_IN_AREA(PLAYER_PED_ID(), vVecOne, vVecTwo, FALSE, FALSE)
				IF serverBD.iQuadrant != iQuadrant
					serverBD.iQuadrant = iQuadrant
				ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

///
FUNC BOOL SETUP_DATA()
	
	// For Quadrants 0 and 1 (SE + SW)
	vDropOffLocations[0] = <<-2200.3828, 4256.8320, 46.8195>>
	vDropOffLocations[1] = <<1279.5879, 3626.9907, 32.0389>>
	vDropOffLocations[2] = <<3333.4221, 5159.9893, 17.3034>>
	
	// For Quadrants 2 and 3 (NW + NE)
	vDropOffLocations[3] = <<-1110.4000, -1517.9730, 3.4613>>
	vDropOffLocations[4] = <<-82.3170, -1403.1591, 28.3208>>
	vDropOffLocations[5] = <<930.7050, -1826.3840, 29.8506>>
	
	INT i
	REPEAT NUM_DROP_OFF_LOCATIONS i
		IF IS_VECTOR_ZERO(vDropOffLocations[i])
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Performs all server initialisation
/// RETURNS:
///    
FUNC BOOL INIT_SERVER()

	SWITCH iSetup
		// Initialise data
		CASE 0
			IF SETUP_DATA()
				PRINTLN("[GB PROTECTION RACKET] [STMGB] [SERVER] - INIT_SERVER - Data has been set up. Moving to DETERMINE_QUADRANT.")
				iSetup++
			ELSE
				PRINTLN("[GB PROTECTION RACKET] [STMGB] [SERVER] - INIT_SERVER - failing to setup data.")
			ENDIF
		BREAK
		
		// Determine which quadrant to create the bags in (wherever the gang boss/host is)
		CASE 1
			IF DETERMINE_QUADRANT()
				PRINTLN("[GB PROTECTION RACKET] [STMGB] [SERVER] - INIT_SERVER - Starting quadrant is ", serverBD.iQuadrant, ". Moving to SELECT_AREA_WITHIN_QUADRANT.")
				iSetup++
			ELSE
				PRINTLN("[GB PROTECTION RACKET] [STMGB] [SERVER] - INIT_SERVER - Unable to determine map quadrant")
			ENDIF
		BREAK
		
		// Select one of the four random starting points within the quadrant
		CASE 2
			IF SELECT_AREA_WITHIN_QUADRANT()
				PRINTLN("[GB PROTECTION RACKET] [STMGB] [SERVER] - INIT_SERVER - Starting area within quadrant is ", serverBD.iStartArea, ". Moving to DETERMINE_DROP_OFF_LOCATION.")
				iSetup++
			ELSE
				PRINTLN("[GB PROTECTION RACKET] [STMGB] [SERVER] - INIT_SERVER - Unable to select an area within the starting quadrant")
			ENDIF
		BREAK
		
		// Determine where the drop off should be, given the selected quadrant and start area
		CASE 3
			IF DETERMINE_DROP_OFF_LOCATION()
				PRINTLN("[GB PROTECTION RACKER] [STMGB] [SERVER] - INIT_SERVER - Selected Drop Off Coord is at ", serverBD.vDropOffCoords)
				PRINTLN("[GB PROTECTION RACKET] [STMGB] [SERVER] - INIT_SERVER - Selected Drop Off Coord is no. ", serverBD.iDropArea)
				PRINTLN("[GB PROTECTION RACKET] [STMGB] [SERVER] - INIT_SERVER - Moving to CREATE_PROTECTION_RACKET_BAGS.")
				iSetup++
			ELSE
				PRINTLN("[GB PROTECTION RACKET] [STMGB] [SERVER] - INIT_SERVER - Unable to determine a suitable drop off location")
			ENDIF
		BREAK
		
		// Create the bags and finish initialisation
		CASE 4
			IF CREATE_PROTECTION_RACKET_BAGS()
				PRINTLN("[GB PROTECTION RACKET] [STMGB] [SERVER] - INIT_SERVER - Bags have been created. Performing rest of setup.")
				
				INITIALISE_AMBIENT_ACTIVITY_LEADERBOARD(serverBD.sSortedLeaderboard)
				
				SORT_PROTECTION_RACKET_PLAYER_SCORES(PLAYER_ID(), 0)	// Add local to leaderboard first
											
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Performs all initilisation required per client
/// RETURNS:
///    
FUNC BOOL INIT_CLIENT()
	
	GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iDropOffBlipR, iDropOffBlipG, iDropOffBlipB, iDropOffBlipA)
	GET_HUD_COLOUR(HUD_COLOUR_GREEN, iMarkerR, iMarkerG, iMarkerB, iMarkerA)
	GET_HUD_COLOUR(HUD_COLOUR_WHITE, iDeadMarkerR, iDeadMarkerB, iDeadMarkerB, iDeadMarkerA)

	GB_COMMON_BOSS_MISSION_SETUP(FMMC_TYPE_GB_CHAL_COLLECT_MONEY)
	
	GB_SET_ITS_SAFE_FOR_SPEC_CAM()
	
	SET_MAX_NUM_PORTABLE_PICKUPS_CARRIED_BY_PLAYER(serverBD.BagModel, 1)
	SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(TRUE)
	PRINTLN("[GB PROTECTION RACKET] [STMGB] - SET_MAX_NUM_PORTABLE_PICKUPS_CARRIED_BY_PLAYER(serverBD.PackageModel, 1)")
		
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		GB_COMMON_BOSS_MISSION_SETUP(FMMC_TYPE_GB_CHAL_COLLECT_MONEY)
		SET_GANG_BOSS_PLAYER_INDEX(PLAYER_ID())
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC HANDLE_CLEANUP_TELEMETRY()

	// If I've not reached the end of the event and we've not cleaned up due to low numbers, then I've left
	IF PARTICIPANT_ID_TO_INT() != -1
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_RewardGiven)
		AND bLowNumbers = FALSE
		AND serverBD.iServerEndReason = -1
			IF NOT NETWORK_IS_PLAYER_ACTIVE(GET_GANG_BOSS_PLAYER_INDEX())
				GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_BOSS_LEFT)
			ELSE
				GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LEFT)
			ENDIF
		ENDIF
	ENDIF
	
	sProtectionRacketTelemetryStruct.iBagsCollected = serverBD.iBagsCollected
	sProtectionRacketTelemetryStruct.iBagsDelivered = serverBD.iBagsDelivered
	sProtectionRacketTelemetryStruct.iPickupQuadrant = serverBD.iQuadrant
	sProtectionRacketTelemetryStruct.iDeliveryQuadrant = serverBD.iDropArea
	
	PROCESS_CURRENT_BOSS_CHALLENGE_PLAYSTATS(	g_sGb_Telemetry_data.sdata,
												sProtectionRacketTelemetryStruct.iBagsCollected,
												sProtectionRacketTelemetryStruct.iBagsDelivered,
												sProtectionRacketTelemetryStruct.iPickupQuadrant,
												sProtectionRacketTelemetryStruct.iDeliveryQuadrant)
ENDPROC

FUNC BOOL IS_SAFE_TO_CLEANUP()
	IF AM_I_THE_GANG_BOSS()
		SWITCH iCleanupBossSpecStep
			CASE 0
				IF GB_SHOULD_RUN_SPEC_CAM()
					PRINTLN("[GB PROTECTION RACKET] [STMGB] - IS_SAFE_TO_CLEANUP - Boss - Player is spectating, calling GB_SET_SHOULD_CLOSE_SPECTATE, moving to next stage")
					GB_SET_SHOULD_CLOSE_SPECTATE()
					iCleanupBossSpecStep = 2
				ELSE
					iCleanupBossSpecStep = 2
				ENDIF
			BREAK
			
			CASE 1
				IF NOT GB_SHOULD_RUN_SPEC_CAM()
					PRINTLN("[GB PROTECTION RACKET] [STMGB] - IS_SAFE_TO_CLEANUP - Boss - NOT GB_SET_SHOULD_CLOSE_SPECTATE, moving to next stage")
					iCleanupBossSpecStep = 2
				ENDIF
			BREAK
			
			CASE 2
				PRINTLN("[GB PROTECTION RACKET] [STMGB] - IS_SAFE_TO_CLEANUP - Boss - Return true")
				RETURN TRUE
			BREAK
		ENDSWITCH
	ELSE
		PRINTLN("[GB PROTECTION RACKET] [STMGB] - IS_SAFE_TO_CLEANUP - Goon - Return true")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SCRIPT_CLEANUP(BOOL bImmediately = FALSE)
	IF bImmediately
	OR IS_SAFE_TO_CLEANUP()
		PRINTLN("[GB PROTECTION RACKET] [STMGB] - SCRIPT_CLEANUP")
		
		g_sGb_Telemetry_data.sdata.m_match1 = serverBD.iMatchId1
		g_sGb_Telemetry_data.sdata.m_match2 = serverBD.iMatchId2
		
		HANDLE_CLEANUP_TELEMETRY()
		
		GB_COMMON_BOSS_MISSION_CLEANUP()
		
		SET_FREEMODE_FLOW_COMMS_DISABLED(FALSE)
		
		GB_TIDYUP_SPECTATOR_CAM()
		//RELEASE_CONTEXT_INTENTION(sSpecVars.iGbSpecContext)
		
		SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(TRUE)
		
		TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
	ENDIF
ENDPROC

FUNC BOOL IS_LOCAL_PLAYER_NO_LONGER_IN_GANG()
	IF playerBD[PARTICIPANT_ID_TO_INT()].iPlayerGameState > GAME_STATE_INIT
		IF serverBD.piGangBoss != INVALID_PLAYER_INDEX()
			IF NOT GB_IS_LOCAL_PLAYER_MEMBER_OF_THIS_GANG(serverBD.piGangBoss)
				PRINTLN("[GB PROTECTION RACKET] [STMGB] [CLIENT] - SHOULD_CLEANUP_SCRIPT - Yes - Local player is not in the gang")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_CLEANUP_CHECKS()

	IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		PRINTLN("[GB PROTECTION RACKET] [STMGB] - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")
		GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_FORCED)
		SCRIPT_CLEANUP(TRUE)
	ENDIF
	
	IF GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION()
		PRINTLN("[GB PROTECTION RACKET] [STMGB] - GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION - True, calling script cleanup")
		GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_FORCED)
		SCRIPT_CLEANUP(TRUE)
	ENDIF
	
	IF IS_LOCAL_PLAYER_NO_LONGER_IN_GANG()
		PRINTLN("[GB PROTECTION RACKET] [STMGB] [CLIENT] - IS_LOCAL_PLAYER_NO_LONGER_IN_GANG - TRUE, calling script cleanup")
		IF NATIVE_TO_INT(PLAYER_ID()) != -1
			IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerBitSet, biP_RewardGiven)
				GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_CHAL_COLLECT_MONEY, FALSE, sRewardsData)
			ENDIF
		ENDIF
		GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_FORCED)
		SCRIPT_CLEANUP()
	ENDIF
	
	IF GET_GANG_BOSS_PLAYER_INDEX() != INVALID_PLAYER_INDEX()
		IF GB_SHOULD_BAIL_BOSS_CHALLENGE_DUE_TO_NO_OR_LOW_GOONS(GET_GANG_BOSS_PLAYER_INDEX())
			PRINTLN("[GB PROTECTION RACKET] [STMGB] [CLIENT] - GB_SHOULD_BAIL_BOSS_CHALLENGE_DUE_TO_NO_OR_LOW_GOONS - TRUE, calling script cleanup")
			IF NATIVE_TO_INT(PLAYER_ID()) != -1
				IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerBitSet, biP_RewardGiven)
					GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_CHAL_COLLECT_MONEY, FALSE, sRewardsData)
				ENDIF
			ENDIF
			GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_FORCED)
			bLowNumbers = TRUE
			SCRIPT_CLEANUP()
		ENDIF
	ENDIF

ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		PRE GAME
// -----------------------------------------------------------------------------------------------------------
FUNC BOOL PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)

	PRINTLN("[GB PROTECTION RACKET] [STMGB] - PROCESS_PRE_GAME")
	
	SET_GANG_BOSS_PLAYER_INDEX(INVALID_PLAYER_INDEX())
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission), missionScriptArgs)
	
	//Reserve Entities
	RESERVE_NETWORK_MISSION_OBJECTS(MAX_NUM_BAGS)
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD,SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD,SIZE_OF(playerBD))
	
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		PRINTLN("[GB PROTECTION RACKET] [STMGB] - PROCESS_PRE_GAME - FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP called.")
		SCRIPT_CLEANUP(TRUE)
	ENDIF
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		serverBD.iReservedObjects = (MAX_NUM_BAGS)
	ENDIF
	
	GB_COMMON_BOSS_MISSION_PREGAME()
	
	RETURN TRUE
	
ENDFUNC

//----------------------
//	DEBUG FUNCTIONS
//----------------------
#IF IS_DEBUG_BUILD

PROC UPDATE_WIDGETS()
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF bHostEndMissionNow
			PRINTLN("[GB PROTECTION RACKET] [STMGB] - UPDATE_WIDGETS - DEBUG - bHostEndMissionNow is TRUE, killing challenge.")
			SET_SERVER_MISSION_STATE(GAME_STATE_END)
			serverBD.iServerEndReason = GB_TELEMETRY_END_FORCED
			bHostEndMissionNow = FALSE
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_WIDGETS()
	INT iPlayer
	TEXT_LABEL_63 tl63

	SWITCH iWidgetState
	
		CASE 0
		
		CollectMoneyChallengeWidget = START_WIDGET_GROUP("GB Protection Racket")
			ADD_WIDGET_BOOL("Create Protection Racket Widgets", bCreateWidgets)
		STOP_WIDGET_GROUP()
		
		iWidgetState++
		
		BREAK
		
		CASE 1
		
			IF bCreateWidgets
				
				CLEAR_CURRENT_WIDGET_GROUP(CollectMoneyChallengeWidget)				
				DELETE_WIDGET_GROUP(CollectMoneyChallengeWidget)
				
				CollectMoneyChallengeWidget = START_WIDGET_GROUP("GB Protection Racket") 
				
					ADD_WIDGET_BOOL("Skip intro countdown", bSkipIntro)
					
					// Gameplay debug, sever only.
					START_WIDGET_GROUP("Server Only Gameplay") 
						ADD_WIDGET_BOOL("End Mission: Debug end no reason (immediate)", bHostEndMissionNow)
						ADD_WIDGET_BOOL("End Mission: End Timer Hit", bExpireTimer)
					STOP_WIDGET_GROUP()
										
					// Data about the server
					START_WIDGET_GROUP("Server BD") 
						ADD_WIDGET_INT_SLIDER("S. Game state", serverBD.iServerGameState,-1, HIGHEST_INT,1)
					STOP_WIDGET_GROUP()	

					// Data about the clients. * = You.
					START_WIDGET_GROUP("Client BD")  				
						REPEAT NUM_NETWORK_PLAYERS iPlayer
							tl63 = "Player "
							tl63 += iPlayer
							IF iPlayer = PARTICIPANT_ID_TO_INT()
								tl63 += "*"
							ENDIF
							START_WIDGET_GROUP(tl63)
								ADD_WIDGET_INT_SLIDER("Game state", playerBD[iPlayer].iPlayerGameState,-1, HIGHEST_INT,1)
							STOP_WIDGET_GROUP()
						ENDREPEAT
					STOP_WIDGET_GROUP()
				STOP_WIDGET_GROUP()
				
				PRINTLN("[GB PROTECTION RACKET] [STMGB] -  Created Widgets")
				
				iWidgetState++
			
			ENDIF
		
		BREAK
		
		CASE 2
			UPDATE_WIDGETS()
		BREAK
	
	ENDSWITCH
ENDPROC

PROC MAINTAIN_DEBUG()
	MAINTAIN_WIDGETS()
	
	IF GB_SHOULD_KILL_ACTIVE_BOSS_MISSION()
		PRINTLN("[GB PROTECTION RACKET] [STMGB] - GB_SHOULD_KILL_ACTIVE_BOSS_MISSION - TRUE, moving server to cleanup")
		serverBD.iServerEndReason = GB_TELEMETRY_END_FORCED
		SET_SERVER_MISSION_STATE(GAME_STATE_END)
	ENDIF
ENDPROC

#ENDIF	// Debug
	// Gang Boss

//----------------------
//	MAIN SCRIPT
//----------------------

SCRIPT(MP_MISSION_DATA missionScriptArgs)


	PRINTLN("[GB PROTECTION RACKET] [STMGB] - START")
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NOT PROCESS_PRE_GAME(missionScriptArgs)
			SCRIPT_CLEANUP(TRUE)
		ENDIF	
	ENDIF

	GB_COMMON_BOSS_MISSION_SETUP(FMMC_TYPE_GB_CHAL_COLLECT_MONEY)
	
	WHILE (TRUE)
	
		MP_LOOP_WAIT_ZERO()
		
		MAINTAIN_CLEANUP_CHECKS()
		
		GB_MAINTAIN_SPECTATE(sSpecVars)
	
		#IF IS_DEBUG_BUILD
		MAINTAIN_DEBUG()
		#ENDIF
		
		PROCESS_EVENTS()
		
		SWITCH(GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT()))
		
			CASE GAME_STATE_INIT
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					GB_SET_ITS_SAFE_FOR_SPEC_CAM()
					IF GB_LAUNCH_CHALLENGE_WITH_SPEC_CAM_IS_ACTIVE(sSpecVars)
						IF INIT_CLIENT()
							SET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT(),GAME_STATE_RUNNING)
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE GAME_STATE_RUNNING
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					PROCESS_CLIENT()
					
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					SET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT(),GAME_STATE_END)
				ENDIF
			BREAK
			
			CASE GAME_STATE_END
				SCRIPT_CLEANUP()
			BREAK
			
		ENDSWITCH
	
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
			SWITCH(GET_SERVER_MISSION_STATE())
			
				CASE GAME_STATE_INIT					
					IF INIT_SERVER()
						SET_SERVER_MISSION_STATE(GAME_STATE_RUNNING)
					ENDIF
				BREAK
				
				CASE GAME_STATE_RUNNING
					PROCESS_SERVER()
				BREAK
				
				CASE GAME_STATE_END
				BREAK
				
			ENDSWITCH
		
		ENDIF
		
	ENDWHILE

	
	
// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT

