
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        x:\gta5\script\dev_ng\multiplayer\scripts\modes\freemode\Ambient\Gang Boss\Challenges\GB_CARJACKING.sc	//
// Description: Players must steal a specific class of vehicle for the gang boss.										//
// Written by:  William Kennedy																							//
// Date: 		09/03/2015																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "net_mission.sch"
USING "net_common_functions.sch"


//----------------------
//	INCLUDES
//----------------------
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "flow_public_core_override.sch"
USING "net_wait_zero.sch"
USING "net_mission.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"

USING "net_gang_boss.sch"

USING "am_common_ui.sch"
USING "DM_Leaderboard.sch"
USING "net_gang_boss_spectate.sch"


//----------------------
//	ENUM
//----------------------

ENUM CARJACKING_STATE
	eCARJACKING_STATE_INIT = 0,
	eCARJACKING_STATE_RUN,
	eCARJACKING_STATE_REWARDS,
	eCARJACKING_STATE_END
ENDENUM

ENUM eSERVER_BITSET_0
	eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS,
	eSERVERBITSET0_END // Keep this at the end.
ENDENUM

ENUM eCLIENT_BITSET_0
	eCLIENTBITSET0_REQUEST_DELIVER_VEHICLE_TO_DROP_OFF = 0,
	eCLIENTBITSET0_EXITED_DELIVERED_VEHICLE,
	eCLIENTBITSET0_COMPLETED_REWARDS,
	eCLIENTBITSET0_ALLOWED_TO_DROP_OFF_VEHICLE,
	eCLIENTBITSET0_NOT_ALLOWED_TO_DROP_OFF_VEHICLE_DROP_FULL,
	eCLIENTBITSET0_END // Keep this at the end.
ENDENUM

ENUM eLOCAL_BITSET_0
	eLOCALBITSET0_IS_FOCUS_PLAYER_IN_ANY_VEHICLE = 0, // 0
	eLOCALBITSET0_IS_FOCUS_PLAYER_IN_VALID_VEHICLE_AS_DRIVER, // 1
	eLOCALBITSET0_IS_PLAYER_IN_VALID_VEHICLE_AS_PASSENGER, // 2
	eLOCALBITSET0_FOCUS_PLAYER_CURRENT_VEHICLE_IS_DRIVEABLE, // 3
	eLOCALBITSET0_CALLED_COMMON_SETUP, // 4
	eLOCALBITSET0_CALLED_SAFE_FOR_SPEC_CAM_SETUP, // 5
	eLOCALBITSET0_FOCUS_PLAYER_IS_IN_OWN_PERSONAL_VEHICLE, // 6
	eLOCALBITSET0_FOCUS_PLAYER_IS_IN_A_PEGASUS_VEHICLE, // 7
	eLOCALBITSET0_FOCUS_PLAYER_IS_IN_AN_ARMOURED_LIMO_VEHICLE, // 8
	eLOCALBITSET0_FOCUS_PLAYER_IS_IN_A_POLICE_VEHICLE, // 9
	eLOCALBITSET0_PRINTED_VEHICLE_PASSENGER_HELP, // 10
	eLOCALBITSET0_PRINTED_VEHICLE_LIMO_HELP, // 11
	eLOCALBITSET0_PRINTED_PERSONAL_VEHICLE_HELP, // 12
	eLOCALBITSET0_PRINTED_PEGASUS_VEHICLE_HELP, // 13
	eLOCALBITSET0_PRINTED_POLICE_VEHICLE_HELP, // 14
	eLOCALBITSET0_SETUP_JOB_START_BIG_MESSAGE, // 15
	eLOCALBITSET0_SHOWN_JOB_START_BIG_MESSAGE, // 16
	eLOCALBITSET0_HANDLED_SETTING_PLAYER_CRITICAL, // 17
	eLOCALBITSET0_PRINTED_EXPLNATORY_HELP_1, // 18
	eLOCALBITSET0_PRINTED_EXPLNATORY_HELP_2, // 19
	eLOCALBITSET0_PRINTED_EXPLNATORY_HELP_3, // 20
	eLOCALBITSET0_CAR_MOD_HELP_TRIGGERED, // 21
	eLOCALBITSET0_PRINTED_CAR_MOD_HELP, // 22
	eLOCALBITSET0_PRINTED_DROP_OFF_CHANGE_HELP, // 23
	eLOCALBITSET0_FOCUS_PLAYER_IS_IN_AN_EMERGENCY_VEHICLE, // 24
	eLOCALBITSET0_PRINTED_EMERGENCY_VEHICLE_HELP, // 25
	eLOCALBITSET0_EXECUTED_END_TELEMTRY, // 26 
	eLOCALBITSET0_ELEGANTLY_HANDLED_SPEC_CAM, // 27
	eLOCALBITSET0_COMBINED_TOTAL_HELP_TRIGGERED, // 28
	eLOCALBITSET0_PRINTED_COMBINED_TOTAL_HELP, // 29
	eLOCALBITSET0_END // Keep this at the end.
ENDENUM

ENUM eSAVE_MY_DELIVERED_VEHICLE_ID_STATE
	eSAVE_MY_DELIVERED_VEHICLE_ID_STATE_NO_RESULT = 0,
	eSAVE_MY_DELIVERED_VEHICLE_ID_STATE_SUCCESS,
	eSAVE_MY_DELIVERED_VEHICLE_ID_STATE_FAILED
ENDENUM

#IF IS_DEBUG_BUILD

ENUM eSERVER_DEBUG_BITSET_0
	eSERVERDEBUGBITSET0_TEMP = 0,
	eSERVERDEBUGBITSET0_END // Keep this at the end.
ENDENUM

ENUM eCLIENT_DEBUG_BITSET_0
	eCLIENTDEBUGBITSET0_I_HAVE_S_PASSED = 0,
	eCLIENTDEBUGBITSET0_I_HAVE_F_FAILED,
	eCLIENTDEBUGBITSET0_END // Keep this at the end.
ENDENUM

ENUM eLOCAL_DEBUG_BITSET_0
	eLOCALDEBUGBITSET0_TERMINATE_SCRIPT_NOW = 0,
	eLOCALDEBUGBITSET0_END // Keep this at the end.
ENDENUM

#ENDIF

//FUNC VEHICLE_CLASS GET_INVALID_TARGET_VEHICLE_CLASS()
//	RETURN VC_RAIL
//ENDFUNC

ENUM eEND_REASON
	eENDREASON_NO_REASON_YET = 0,
	eENDREASON_TIME_UP,
	eENDREASON_NO_BOSS_LEFT,
	eENDREASON_ALL_GOONS_LEFT
ENDENUM


//----------------------
//	CONSTANTS
//----------------------

CONST_FLOAT DELIVER_ACCEPT_SPEED						1.0

CONST_INT GAME_STATE_INIT 								0
CONST_INT GAME_STATE_RUNNING							1
CONST_INT GAME_STATE_TERMINATE_DELAY					2
CONST_INT GAME_STATE_END								3


CONST_INT TRACK_GOON_DELAY								1000*10
CONST_INT NEARLY_FINISHED_TIME							1000*30

CONST_INT MAX_NUM_CARJACKING_DROP_OFF_LOCATES			5
CONST_INT MAX_NUM_CARJACKING_ACTIVE_DROP_OFF_IN_AREA	5

CONST_FLOAT CAR_MOD_HELP_DISTANCE						20.0


//----------------------
//	STRUCT
//----------------------

STRUCT VEHICLES_DELIVERED_PARTICIPANT_DATA
	PARTICIPANT_INDEX participantId
	//INT iNumberVehiclesDelivered
	FLOAT fTotalValueOfVehiclesDelivered
	TEXT_LABEL_63 tl63Name
ENDSTRUCT

STRUCT STRUCT_PARTICIPANT_ID_DATA
	PARTICIPANT_INDEX participantId
	INT iPlayerId
	 TEXT_LABEL_63 tl63Name
	BOOL bIsBoss
	BOOL bIsGoon
ENDSTRUCT

STRUCT STRUCT_PLAYER_ID_DATA
	PLAYER_INDEX playerId
	INT iParticipantId
	PED_INDEX playerPedId
ENDSTRUCT

STRUCT STRUCT_DELIVERED_VEHICLE_DATA
	PARTICIPANT_INDEX delivererPartId
	VEHICLE_INDEX myDeliveredVehicle
//	MODEL_NAMES vehicleModel
	FLOAT fVehicleValue
	TEXT_LABEL_63 tl63DelivererName
	INT iDropOffArea = -1
	INT iDropOff = -1
//	SCRIPT_TIMER deliveryTime
ENDSTRUCT

STRUCT STRUCT_VEHICLE_DELIVERED_BITSET_DATA
	INT iOneVehicleDeliveredBitset0
	INT iTwoVehiclesDeliveredBitset0
	INT iThreeVehiclesDeliveredBitset0
	INT iFourVehiclesDeliveredBitset0
	INT iOneVehicleDeliveredBitset1
	INT iTwoVehiclesDeliveredBitset1
	INT iThreeVehiclesDeliveredBitset1
	INT iFourVehiclesDeliveredBitset1
ENDSTRUCT


//----------------------
//	LOCAL VARIABLES
//----------------------

STRUCT_PARTICIPANT_ID_DATA sParticipant[GB_MAX_GANG_SIZE_INCLUDING_BOSS_EXEC]
STRUCT_PLAYER_ID_DATA sPlayer[NUM_NETWORK_PLAYERS]
GANG_BOSS_MANAGE_REWARDS_DATA sGangBossManageRewardsData
GB_MAINTAIN_SPECTATE_VARS sSpecVars
GB_STRUCT_BOSS_END_UI sEndUI

PLAYER_INDEX previousFirstPlacePlayerId = INT_TO_NATIVE(PLAYER_INDEX, -1)
TIME_DATATYPE timeLeaveEvent

INT iLocalBitset0

INT iPlayerOkBitset
INT iParticipantActiveBitset
INT iPlayerDeadBitset
INT iUpdateServerMyDeliveredVehicleStage

INT iCurrentGpsDropOffArea = -1
INT iCurrentGpsDropOff = -1

INT iFocusParticipant

INT iDropOffActiveBitsetCopy[MAX_NUM_CARJAKCING_DROP_OFF_AREAS]
INT iDropOffChangedHelpStage

INT iElegantSpecCamStage

INT iBlipAlphaDropOffAreaCount

BOOL bForceRebuildNames

VEHICLE_INDEX currentVehicle
MODEL_NAMES eCurrentVehicleModel
FLOAT fCurrentVehicleValue

//VECTOR vMyBossCoords
VECTOR vFocusParticipantCoords
INT iNumGoonsOnScript
//FLOAT fMyDistanceFromBoss

STRUCT_DELIVERED_VEHICLE_DATA sMyDeliveredVehicleData

SCRIPT_TIMER serverDeliveredVehicleHandshakeTimer

#IF IS_DEBUG_BUILD
INT iLocalDebugBitset
#ENDIF


//----------------------
//	BROADCAST VARIABLES
//----------------------

// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData

	INT iServerGameState = GAME_STATE_INIT
	INT iServerBitSet
	CARJACKING_STATE eState
	SCRIPT_TIMER modeTimer
	VEHICLES_DELIVERED_PARTICIPANT_DATA sVehiclesDeliveredParticipantData[GB_MAX_GANG_SIZE_INCLUDING_BOSS_EXEC]
	eEND_REASON eEndReason
	INT iAddedParticipantToDeliveredVehiclesArray
	STRUCT_VEHICLE_DELIVERED_BITSET_DATA sNumberVehiclesDeliveredBitsets
	INT iDropOffActiveBitset[MAX_NUM_CARJAKCING_DROP_OFF_AREAS]
	INT iDropOffFullBitset[MAX_NUM_CARJAKCING_DROP_OFF_AREAS]
	INT iTotalOrganisationScore
	
	#IF IS_DEBUG_BUILD
	INT iDebugBitset
	INT iSPassPart = -1
	INT iFFailPart = -1
	BOOL bFakeEndOfModeTimer
	#ENDIF
	
	INT					iMatchId1
	INT					iMatchId2
	
ENDSTRUCT

ServerBroadcastData serverBD

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	
	INT iClientGameState = GAME_STATE_INIT
	INT iClientBitSet
	
	#IF IS_DEBUG_BUILD
	INT iDebugBitset
	#ENDIF
	
ENDSTRUCT

PlayerBroadcastData playerBD[GB_MAX_GANG_SIZE_INCLUDING_BOSS_EXEC]


//----------------------
//	TUNEABLE FUNCTIONS
//----------------------

FUNC INT GET_CARJACKING_TUNEABLE_TIME_LIMIT()
	RETURN g_sMPTunables.igb_carjacking_time_limit                                           //	INT	Overwrite	600000	Time limit for Carjacking (milliseconds)
ENDFUNC

FUNC INT GET_CARJACKING_TUNEABLE_VEHICLE_VALUE_CAP()
	RETURN g_sMPTunables.igb_carjacking_car_value_cap                                        //	INT	Overwrite	500000	Cap on the value of each car delivered
ENDFUNC

FUNC INT GET_CARJACKING_TUNEABLE_DROP_OFF_MAX_VEHICLES()
	RETURN g_sMPTunables.igb_carjacking_delivery_point_max_deliveries                        //	INT	Overwrite	4	Number of cars that can be delivered to a point before it becomes defunct
ENDFUNC

FUNC INT GET_CARJACKING_TUNEABLE_NUM_DROP_OFF_IN_AREA()
	RETURN g_sMPTunables.igb_carjacking_delivery_points_active                               //	INT	Overwrite	5	Number of delivery points within each area active
ENDFUNC

FUNC INT GET_CARJACKING_BONUS_CASH_REWARD_MAX()
	RETURN g_sMPTunables.igb_carjacking_bonus_cash_reward_max
ENDFUNC

FUNC FLOAT GET_CARJACKING_BONUS_CASH_REWARD_PERCENTAGE_MAX()
	RETURN g_sMPTunables.fgb_carjacking_bonus_cash_reward_pecentage
ENDFUNC


//----------------------
//	FUNCTIONS
//----------------------

#IF IS_DEBUG_BUILD
FUNC STRING GET_SERVER_BIT0_NAME(eSERVER_BITSET_0 eBitset)
	
	SWITCH eBitset
		CASE eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS		RETURN "eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS"
		CASE eSERVERBITSET0_END										RETURN "eSERVERBITSET0_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
	
ENDFUNC
#ENDIF

FUNC BOOL IS_SERVER_BIT0_SET(eSERVER_BITSET_0 eBitset)
	RETURN IS_BIT_SET(serverBD.iServerBitSet, ENUM_TO_INT(eBitset))
ENDFUNC

PROC SET_SERVER_BIT0(eSERVER_BITSET_0 eBitset #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_SERVER_BIT0_NAME(eBitset)
			PRINTLN("[GBCARJCK] GET_SERVER_BIT0_NAME - ", strTemp)
		ENDIF
	ENDIF
	#ENDIF
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		#IF IS_DEBUG_BUILD
		SCRIPT_ASSERT("[GBCARJCK] calling SET_SERVER_BIT0 on a non host.")
		PRINTLN("")
		#ENDIF
		EXIT
	ENDIF
	
	SET_BIT(serverBD.iServerBitSet, ENUM_TO_INT(eBitset))
	
ENDPROC

PROC CLEAR_SERVER_BIT0(eSERVER_BITSET_0 eBitset #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF IS_BIT_SET(serverBD.iServerBitSet, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_SERVER_BIT0_NAME(eBitset)
			PRINTLN("[GBCARJCK] CLEAR_SERVER_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	#ENDIF
	
	CLEAR_BIT(serverBD.iServerBitSet, ENUM_TO_INT(eBitset))
	
ENDPROC

#IF IS_DEBUG_BUILD
FUNC STRING GET_CLIENT_BIT0_NAME(eCLIENT_BITSET_0 eBitset)
	
	SWITCH eBitset
		CASE eCLIENTBITSET0_REQUEST_DELIVER_VEHICLE_TO_DROP_OFF			RETURN "eCLIENTBITSET0_REQUEST_DELIVER_VEHICLE_TO_DROP_OFF"
		CASE eCLIENTBITSET0_EXITED_DELIVERED_VEHICLE					RETURN "eCLIENTBITSET0_EXITED_DELIVERED_VEHICLE"
		CASE eCLIENTBITSET0_ALLOWED_TO_DROP_OFF_VEHICLE					RETURN "eCLIENTBITSET0_ALLOWED_TO_DROP_OFF_VEHICLE"
		CASE eCLIENTBITSET0_NOT_ALLOWED_TO_DROP_OFF_VEHICLE_DROP_FULL	RETURN "eCLIENTBITSET0_NOT_ALLOWED_TO_DROP_OFF_VEHICLE_DROP_FULL"
		CASE eCLIENTBITSET0_END											RETURN "eCLIENTBITSET0_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
	
ENDFUNC
#ENDIF

FUNC BOOL IS_CLIENT_BIT0_SET(PARTICIPANT_INDEX partId, eCLIENT_BITSET_0 eBitset)
	RETURN IS_BIT_SET(playerBD[NATIVE_TO_INT(partId)].iClientBitSet, ENUM_TO_INT(eBitset))
ENDFUNC

PROC SET_CLIENT_BIT0(eCLIENT_BITSET_0 eBitset #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_CLIENT_BIT0_NAME(eBitset)
			PRINTLN("[GBCARJCK] SET_CLIENT_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	#ENDIF
	
	SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, ENUM_TO_INT(eBitset))
	
ENDPROC

PROC CLEAR_CLIENT_BIT0(eCLIENT_BITSET_0 eBitset #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_CLIENT_BIT0_NAME(eBitset)
			PRINTLN("[GBCARJCK] CLEAR_CLIENT_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	#ENDIF
	
	CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, ENUM_TO_INT(eBitset))
	
ENDPROC

#IF IS_DEBUG_BUILD
FUNC STRING GET_LOCAL_BIT0_NAME(eLOCAL_BITSET_0 eBitset)
	
	SWITCH eBitset
		CASE eLOCALBITSET0_IS_FOCUS_PLAYER_IN_ANY_VEHICLE						RETURN "eLOCALBITSET0_IS_FOCUS_PLAYER_IN_ANY_VEHICLE"
		CASE eLOCALBITSET0_IS_FOCUS_PLAYER_IN_VALID_VEHICLE_AS_DRIVER			RETURN "eLOCALBITSET0_IS_FOCUS_PLAYER_IN_VALID_VEHICLE_AS_DRIVER"
		CASE eLOCALBITSET0_IS_PLAYER_IN_VALID_VEHICLE_AS_PASSENGER				RETURN "eLOCALBITSET0_IS_PLAYER_IN_VALID_VEHICLE_AS_PASSENGER"
		CASE eLOCALBITSET0_FOCUS_PLAYER_CURRENT_VEHICLE_IS_DRIVEABLE			RETURN "eLOCALBITSET0_FOCUS_PLAYER_CURRENT_VEHICLE_IS_DRIVEABLE"
		CASE eLOCALBITSET0_CALLED_COMMON_SETUP									RETURN "eLOCALBITSET0_CALLED_COMMON_SETUP"
		CASE eLOCALBITSET0_CALLED_SAFE_FOR_SPEC_CAM_SETUP						RETURN "eLOCALBITSET0_CALLED_SAFE_FOR_SPEC_CAM_SETUP"
		CASE eLOCALBITSET0_FOCUS_PLAYER_IS_IN_OWN_PERSONAL_VEHICLE				RETURN "eLOCALBITSET0_FOCUS_PLAYER_IS_IN_OWN_PERSONAL_VEHICLE"
		CASE eLOCALBITSET0_FOCUS_PLAYER_IS_IN_A_PEGASUS_VEHICLE					RETURN "eLOCALBITSET0_FOCUS_PLAYER_IS_IN_A_PEGASUS_VEHICLE"
		CASE eLOCALBITSET0_FOCUS_PLAYER_IS_IN_AN_ARMOURED_LIMO_VEHICLE			RETURN "eLOCALBITSET0_FOCUS_PLAYER_IS_IN_AN_ARMOURED_LIMO_VEHICLE"
		CASE eLOCALBITSET0_FOCUS_PLAYER_IS_IN_A_POLICE_VEHICLE					RETURN "eLOCALBITSET0_FOCUS_PLAYER_IS_IN_A_POLICE_VEHICLE"
		CASE eLOCALBITSET0_PRINTED_VEHICLE_PASSENGER_HELP						RETURN "eLOCALBITSET0_PRINTED_VEHICLE_PASSENGER_HELP"
		CASE eLOCALBITSET0_PRINTED_VEHICLE_LIMO_HELP							RETURN "eLOCALBITSET0_PRINTED_VEHICLE_LIMO_HELP"
		CASE eLOCALBITSET0_PRINTED_PERSONAL_VEHICLE_HELP						RETURN "eLOCALBITSET0_PRINTED_PERSONAL_VEHICLE_HELP"
		CASE eLOCALBITSET0_PRINTED_PEGASUS_VEHICLE_HELP							RETURN "eLOCALBITSET0_PRINTED_PEGASUS_VEHICLE_HELP"
		CASE eLOCALBITSET0_PRINTED_POLICE_VEHICLE_HELP							RETURN "eLOCALBITSET0_PRINTED_POLICE_VEHICLE_HELP"
		CASE eLOCALBITSET0_SETUP_JOB_START_BIG_MESSAGE							RETURN "eLOCALBITSET0_SETUP_JOB_START_BIG_MESSAGE"
		CASE eLOCALBITSET0_SHOWN_JOB_START_BIG_MESSAGE							RETURN "eLOCALBITSET0_SHOWN_JOB_START_BIG_MESSAGE"
		CASE eLOCALBITSET0_HANDLED_SETTING_PLAYER_CRITICAL						RETURN "eLOCALBITSET0_HANDLED_SETTING_PLAYER_CRITICAL"
		CASE eLOCALBITSET0_PRINTED_EXPLNATORY_HELP_1							RETURN "eLOCALBITSET0_PRINTED_EXPLNATORY_HELP_1"
		CASE eLOCALBITSET0_PRINTED_EXPLNATORY_HELP_2							RETURN "eLOCALBITSET0_PRINTED_EXPLNATORY_HELP_2"
		CASE eLOCALBITSET0_PRINTED_EXPLNATORY_HELP_3							RETURN "eLOCALBITSET0_PRINTED_EXPLNATORY_HELP_3"
		CASE eLOCALBITSET0_CAR_MOD_HELP_TRIGGERED								RETURN "eLOCALBITSET0_CAR_MOD_HELP_TRIGGERED"
		CASE eLOCALBITSET0_PRINTED_CAR_MOD_HELP									RETURN "eLOCALBITSET0_PRINTED_CAR_MOD_HELP"
		CASE eLOCALBITSET0_PRINTED_DROP_OFF_CHANGE_HELP							RETURN "eLOCALBITSET0_PRINTED_DROP_OFF_CHANGE_HELP"
		CASE eLOCALBITSET0_FOCUS_PLAYER_IS_IN_AN_EMERGENCY_VEHICLE				RETURN "eLOCALBITSET0_FOCUS_PLAYER_IS_IN_AN_EMERGENCY_VEHICLE"
		CASE eLOCALBITSET0_PRINTED_EMERGENCY_VEHICLE_HELP						RETURN "eLOCALBITSET0_PRINTED_EMERGENCY_VEHICLE_HELP"
		CASE eLOCALBITSET0_EXECUTED_END_TELEMTRY 								RETURN "eLOCALBITSET0_EXECUTED_END_TELEMTRY"
		CASE eLOCALBITSET0_ELEGANTLY_HANDLED_SPEC_CAM							RETURN "eLOCALBITSET0_ELEGANTLY_HANDLED_SPEC_CAM"
		CASE eLOCALBITSET0_COMBINED_TOTAL_HELP_TRIGGERED						RETURN "eLOCALBITSET0_COMBINED_TOTAL_HELP_TRIGGERED"
		CASE eLOCALBITSET0_PRINTED_COMBINED_TOTAL_HELP							RETURN "eLOCALBITSET0_PRINTED_COMBINED_TOTAL_HELP"
	ENDSWITCH
	
	RETURN "***INVALID***"
	
ENDFUNC
#ENDIF

FUNC BOOL IS_LOCAL_BIT0_SET(eLOCAL_BITSET_0 eBitset)
	RETURN IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eBitset))
ENDFUNC

PROC SET_LOCAL_BIT0(eLOCAL_BITSET_0 eBitset #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF NOT IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_LOCAL_BIT0_NAME(eBitset)
			PRINTLN("[GBCARJCK] SET_LOCAL_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	#ENDIF
	
	SET_BIT(iLocalBitset0, ENUM_TO_INT(eBitset))
	
ENDPROC

PROC CLEAR_LOCAL_BIT0(eLOCAL_BITSET_0 eBitset #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_LOCAL_BIT0_NAME(eBitset)
			PRINTLN("[GBCARJCK] CLEAR_LOCAL_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	#ENDIF
	
	CLEAR_BIT(iLocalBitset0, ENUM_TO_INT(eBitset))
	
ENDPROC


#IF IS_DEBUG_BUILD

FUNC STRING GET_SERVER_DEBUG_BIT0_NAME(eSERVER_DEBUG_BITSET_0 eBitset)
	
	SWITCH eBitset
		CASE eSERVERDEBUGBITSET0_TEMP	RETURN "eSERVERDEBUGBITSET0_TEMP"
		CASE eSERVERDEBUGBITSET0_END	RETURN "eSERVERDEBUGBITSET0_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
	
ENDFUNC

FUNC BOOL IS_SERVER_DEBUG_BIT0_SET(eSERVER_DEBUG_BITSET_0 eBitset)
	RETURN IS_BIT_SET(serverBD.iDebugBitset, ENUM_TO_INT(eBitset))
ENDFUNC

PROC SET_SERVER_DEBUG_BIT0(eSERVER_DEBUG_BITSET_0 eBitset, BOOL bBlockLogPrints = FALSE)
	
	IF NOT bBlockLogPrints
		IF NOT IS_BIT_SET(serverBD.iDebugBitset, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_SERVER_DEBUG_BIT0_NAME(eBitset)
			PRINTLN("[GBCARJCK] GET_SERVER_DEBUG_BIT0_NAME - ", strTemp)
		ENDIF
	ENDIF
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		SCRIPT_ASSERT("[GBCARJCK] calling SET_SERVER_DEBUG_BIT0 on a non host.")
		PRINTLN("[GBCARJCK] calling SET_SERVER_DEBUG_BIT0 on a non host.")
		EXIT
	ENDIF
	
	SET_BIT(serverBD.iDebugBitset, ENUM_TO_INT(eBitset))
	
ENDPROC

PROC CLEAR_SERVER_DEBUG_BIT0(eCLIENT_DEBUG_BITSET_0 eBitset, BOOL bBlockLogPrints = FALSE)

	IF NOT bBlockLogPrints
		IF IS_BIT_SET(serverBD.iDebugBitset, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_SERVER_DEBUG_BIT0_NAME(eBitset)
			PRINTLN("[GBCARJCK] CLEAR_SERVER_DEBUG_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	
	CLEAR_BIT(serverBD.iDebugBitset, ENUM_TO_INT(eBitset))
	
ENDPROC

FUNC STRING GET_CLIENT_DEBUG_BIT0_NAME(eCLIENT_DEBUG_BITSET_0 eBitset)
	
	SWITCH eBitset
		CASE eCLIENTDEBUGBITSET0_I_HAVE_S_PASSED	RETURN "eCLIENTDEBUGBITSET0_I_HAVE_S_PASSED"
		CASE eCLIENTDEBUGBITSET0_I_HAVE_F_FAILED	RETURN "eCLIENTDEBUGBITSET0_I_HAVE_F_FAILED"
		CASE eCLIENTDEBUGBITSET0_END				RETURN "eCLIENTDEBUGBITSET0_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
	
ENDFUNC

FUNC BOOL IS_CLIENT_DEBUG_BIT0_SET(PARTICIPANT_INDEX partId, eCLIENT_DEBUG_BITSET_0 eBitset)
	RETURN IS_BIT_SET(playerBD[NATIVE_TO_INT(partId)].iDebugBitset, ENUM_TO_INT(eBitset))
ENDFUNC

PROC SET_CLIENT_DEBUG_BIT0(eCLIENT_DEBUG_BITSET_0 eBitset, BOOL bBlockLogPrints = FALSE)

	IF NOT bBlockLogPrints
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iDebugBitset, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_CLIENT_DEBUG_BIT0_NAME(eBitset)
			PRINTLN("[GBCARJCK] SET_CLIENT_DEBUG_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	
	SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iDebugBitset, ENUM_TO_INT(eBitset))
	
ENDPROC

PROC CLEAR_CLIENT_DEBUG_BIT0(eCLIENT_DEBUG_BITSET_0 eBitset, BOOL bBlockLogPrints = FALSE)

	IF NOT bBlockLogPrints
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iDebugBitset, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_CLIENT_DEBUG_BIT0_NAME(eBitset)
			PRINTLN("[GBCARJCK] CLEAR_CLIENT_DEBUG_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	
	CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iDebugBitset, ENUM_TO_INT(eBitset))
	
ENDPROC

FUNC STRING GET_LOCAL_DEBUG_BIT0_NAME(eLOCAL_DEBUG_BITSET_0 eBitset)
	
	SWITCH eBitset
		CASE eLOCALDEBUGBITSET0_TERMINATE_SCRIPT_NOW	RETURN "eLOCALDEBUGBITSET0_TERMINATE_SCRIPT_NOW"
		CASE eLOCALDEBUGBITSET0_END						RETURN "eLOCALBITSET0_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
	
ENDFUNC

FUNC BOOL IS_LOCAL_DEBUG_BIT0_SET(eLOCAL_DEBUG_BITSET_0 eBitset)
	RETURN IS_BIT_SET(iLocalDebugBitset, ENUM_TO_INT(eBitset))
ENDFUNC

PROC SET_LOCAL_DEBUG_BIT0(eLOCAL_DEBUG_BITSET_0 eBitset, BOOL bBlockLogPrints = FALSE)

	IF NOT bBlockLogPrints
		IF NOT IS_BIT_SET(iLocalDebugBitset, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_LOCAL_DEBUG_BIT0_NAME(eBitset)
			PRINTLN("[GBCARJCK] SET_LOCAL_DEBUG_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	
	SET_BIT(iLocalDebugBitset, ENUM_TO_INT(eBitset))
	
ENDPROC

PROC CLEAR_LOCAL_DEBUG_BIT0(eLOCAL_DEBUG_BITSET_0 eBitset, BOOL bBlockLogPrints = FALSE)

	IF NOT bBlockLogPrints
		IF IS_BIT_SET(iLocalDebugBitset, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_LOCAL_BIT0_NAME(eBitset)
			PRINTLN("[GBCARJCK] CLEAR_LOCAL_DEBUG_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	
	CLEAR_BIT(iLocalDebugBitset, ENUM_TO_INT(eBitset))
	
ENDPROC

#ENDIF

FUNC BOOL INIT_CLIENT()
	RETURN TRUE
ENDFUNC

FUNC BOOL INIT_SERVER()
	RETURN TRUE
ENDFUNC

FUNC STRING GET_GAME_STATE_NAME(INT iState)

	SWITCH(iState)
		CASE GAME_STATE_INIT 				RETURN "GAME_STATE_INIT"
		CASE GAME_STATE_RUNNING 			RETURN "GAME_STATE_RUNNING"
		CASE GAME_STATE_TERMINATE_DELAY 	RETURN "GAME_STATE_TERMINATE_DELAY"
		CASE GAME_STATE_END 				RETURN "GAME_STATE_END"
	ENDSWITCH

	RETURN "***INVALID***"

ENDFUNC

//Helper function to get a clients game/mission state
FUNC INT GET_CLIENT_GAME_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iClientGameState
ENDFUNC

//Helper function to get the servers game/mission state
FUNC INT GET_SERVER_GAME_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC

//Helper function to set a clients game/mission state
PROC SET_CLIENT_GAME_STATE(INT iPlayer, INT iState)
	playerBD[iPlayer].iClientGameState = iState
	PRINTLN("[GBCARJCK] SET_CLIENT_GAME_STATE = ",GET_GAME_STATE_NAME(iState))
ENDPROC

//Helper function to set the servers game/mission state
PROC SET_SERVER_GAME_STATE(INT iState)
	serverBD.iServerGameState = iState
	PRINTLN("[GBCARJCK] SET_SERVER_GAME_STATE = ",GET_GAME_STATE_NAME(iState))
ENDPROC

/*
FUNC STRING GET_VEHICLE_CLASS_NAME(VEHICLE_CLASS eClass)
	
	SWITCH eClass
		CASE VC_COMPACT 		RETURN "CARJCK_COMPCT"
	    CASE VC_SEDAN 			RETURN "CARJCK_SUV"
	    CASE VC_SUV 			RETURN "CARJCK_SEDAN"
	    CASE VC_COUPE 			RETURN "CARJCK_COUPE"
	   	CASE VC_MUSCLE 			RETURN "CARJCK_MUSCLE"
	    CASE VC_SPORT_CLASSIC	RETURN "CARJCK_SPTCLS"
	    CASE VC_SPORT 			RETURN "CARJCK_SPORT"
	    CASE VC_SUPER 			RETURN "CARJCK_SUPER"
	    CASE VC_MOTORCYCLE 		RETURN "CARJCK_MTRCYC"
	    CASE VC_OFF_ROAD 		RETURN "CARJCK_OFFRD"
	    CASE VC_INDUSTRIAL 		RETURN "CARJCK_INDUST"
	    CASE VC_UTILITY 		RETURN "CARJCK_UTIL"
	    CASE VC_VAN 			RETURN "CARJCK_VAN"
	    CASE VC_CYCLE 			RETURN "CARJCK_CYCLE"
	    CASE VC_BOAT 			RETURN "CARJCK_BOAT"
	    CASE VC_HELICOPTER 		RETURN "CARJCK_HELI"
	    CASE VC_PLANE 			RETURN "CARJCK_PLANE"
		CASE VC_SERVICE 		RETURN "CARJCK_SERV"
		CASE VC_EMERGENCY 		RETURN "CARJCK_EMERG"
		CASE VC_MILITARY 		RETURN "CARJCK_MILI"
		CASE VC_COMMERCIAL 		RETURN "CARJCK_COMMER"
		CASE VC_RAIL 			RETURN "CARJCK_RAIL"
	ENDSWITCH
	
	RETURN "***INVALID***"
	
ENDFUNC

FUNC VEHICLE_CLASS GET_TARGET_VEHICLE_CLASS()
	RETURN serverBD.sVehicleData.eTargetVehicleClass
ENDFUNC

PROC SET_TARGET_VEHICLE_CLASS(VEHICLE_CLASS eClass)
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
	
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 errorMsg = "[GBCARJCK] - Client set target class. Client = "
		errorMsg += PARTICIPANT_ID_TO_INT()
		SCRIPT_ASSERT(errorMsg)
		#ENDIF
		
		EXIT
		
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF eClass != GET_TARGET_VEHICLE_CLASS()
		STRING strStateName = GET_VEHICLE_CLASS_NAME(eClass)
		PRINTLN("[GBCARJCK] - setting target vehicle class to ", strStateName)
		DEBUG_PRINTCALLSTACK()
	ENDIF
	#ENDIF
	
	serverBD.sVehicleData.eTargetVehicleClass = eClass	
	
ENDPROC
*/

#IF IS_DEBUG_BUILD 
FUNC STRING GET_CARJACKING_STATE_NAME(CARJACKING_STATE eState)
	
	SWITCH eState
		CASE eCARJACKING_STATE_INIT		RETURN "eCARJACKING_STATE_INIT"
		CASE eCARJACKING_STATE_RUN		RETURN "eCARJACKING_STATE_RUN"
		CASE eCARJACKING_STATE_REWARDS	RETURN "eCARJACKING_STATE_REWARDS"
		CASE eCARJACKING_STATE_END		RETURN "eCARJACKING_STATE_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
	
ENDFUNC
#ENDIF

FUNC CARJACKING_STATE GET_CARJACKING_STATE()
	RETURN serverBD.eState
ENDFUNC

PROC SET_CARJACKING_STATE(CARJACKING_STATE eState)
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
	
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 errorMsg = "[GBCARJCK] - Client set mode state. Client = "
		errorMsg += PARTICIPANT_ID_TO_INT()
		SCRIPT_ASSERT(errorMsg)
		#ENDIF
		
		EXIT
		
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF eState != GET_CARJACKING_STATE()
		STRING strStateName = GET_CARJACKING_STATE_NAME(eState)
		PRINTLN("[GBCARJCK] - mode state going to ", strStateName)
		DEBUG_PRINTCALLSTACK()
	ENDIF
	#ENDIF
	
	serverBD.eState = eState	
	
ENDPROC

#IF IS_DEBUG_BUILD 
FUNC STRING GET_END_REASON_NAME(eEND_REASON eEndReason)
	
	SWITCH eEndReason
		CASE eENDREASON_NO_REASON_YET						RETURN "eENDREASON_NO_REASON_YET"
		CASE eENDREASON_TIME_UP								RETURN "TIME_UP"
		CASE eENDREASON_NO_BOSS_LEFT						RETURN "NO_BOSS_LEFT"
		CASE eENDREASON_ALL_GOONS_LEFT						RETURN "ALL_GOONS_LEFT"
	ENDSWITCH
	
	RETURN "***INVALID***"
	
ENDFUNC
#ENDIF

FUNC eEND_REASON GET_END_REASON()
	RETURN serverBD.eEndReason
ENDFUNC

PROC SET_END_REASON(eEND_REASON eEndReason)
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
	
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 errorMsg = "[GBCARJCK] - Client set end reason. Client = "
		errorMsg += PARTICIPANT_ID_TO_INT()
		SCRIPT_ASSERT(errorMsg)
		#ENDIF
		
		EXIT
		
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF eEndReason != GET_END_REASON()
		STRING strReason = GET_END_REASON_NAME(eEndReason)
		PRINTLN("[GBCARJCK] - set end reason to ", strReason)
		DEBUG_PRINTCALLSTACK()
	ENDIF
	#ENDIF
	
	serverBD.eEndReason = eEndReason	
	
ENDPROC

FUNC INT GET_DROP_OFF_ID(INT iDropOffArea, INT iDropOff)
	
	SWITCH iDropOffArea
		CASE 0
			SWITCH iDropOff
				CASE 0 RETURN 0
				CASE 1 RETURN 1
				CASE 2 RETURN 2
				CASE 3 RETURN 3
				CASE 4 RETURN 4
				CASE 5 RETURN 5
				CASE 6 RETURN 6
				CASE 7 RETURN 7
				CASE 8 RETURN 8
				CASE 9 RETURN 9
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH iDropOff
				CASE 0 RETURN 10
				CASE 1 RETURN 11
				CASE 2 RETURN 12
				CASE 3 RETURN 13
				CASE 4 RETURN 14
				CASE 5 RETURN 15
				CASE 6 RETURN 16
				CASE 7 RETURN 17
				CASE 8 RETURN 18
				CASE 9 RETURN 19
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH iDropOff
				CASE 0 RETURN 20
				CASE 1 RETURN 21
				CASE 2 RETURN 22
				CASE 3 RETURN 23
				CASE 4 RETURN 24
				CASE 5 RETURN 25
				CASE 6 RETURN 26
				CASE 7 RETURN 27
				CASE 8 RETURN 28
				CASE 9 RETURN 29
			ENDSWITCH
		BREAK
		CASE 3
			SWITCH iDropOff
				CASE 0 RETURN 30
				CASE 1 RETURN 31
				CASE 2 RETURN 32
				CASE 3 RETURN 33
				CASE 4 RETURN 34
				CASE 5 RETURN 35
				CASE 6 RETURN 36
				CASE 7 RETURN 37
				CASE 8 RETURN 38
				CASE 9 RETURN 39
			ENDSWITCH
		BREAK
		CASE 4
			SWITCH iDropOff
				CASE 0 RETURN 40
				CASE 1 RETURN 41
				CASE 2 RETURN 42
				CASE 3 RETURN 43
				CASE 4 RETURN 44
				CASE 5 RETURN 45
				CASE 6 RETURN 46
				CASE 7 RETURN 47
				CASE 8 RETURN 48
				CASE 9 RETURN 49
			ENDSWITCH
		BREAK
		CASE 5
			SWITCH iDropOff
				CASE 0 RETURN 50
				CASE 1 RETURN 51
				CASE 2 RETURN 52
				CASE 3 RETURN 53
				CASE 4 RETURN 54
				CASE 5 RETURN 55
				CASE 6 RETURN 56
				CASE 7 RETURN 57
				CASE 8 RETURN 58
				CASE 9 RETURN 59
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("Carjakcing - GET_DROP_OFF_ID invalid request. Returning -1.")
	RETURN -1
	
ENDFUNC

FUNC INT GET_NUM_VEHICLES_DELIVERED_TO_DROP_OFF(INT iDropOffArea, INT iDropOff)
	
	INT iDropOffId = GET_DROP_OFF_ID(iDropOffArea, iDropOff)
	
	IF iDropOffId <= 31
		
		IF IS_BIT_SET(serverBd.sNumberVehiclesDeliveredBitsets.iFourVehiclesDeliveredBitset0, iDropOffId)
			RETURN 4
		ELIF IS_BIT_SET(serverBd.sNumberVehiclesDeliveredBitsets.iThreeVehiclesDeliveredBitset0, iDropOffId)
			RETURN 3
		ELIF IS_BIT_SET(serverBd.sNumberVehiclesDeliveredBitsets.iTwoVehiclesDeliveredBitset0, iDropOffId)
			RETURN 2
		ELIF IS_BIT_SET(serverBd.sNumberVehiclesDeliveredBitsets.iOneVehicleDeliveredBitset0, iDropOffId)
			RETURN 1
		ELSE
			RETURN 0
		ENDIF
		
	ELSE
		
		iDropOffId -= 32
		
		IF IS_BIT_SET(serverBd.sNumberVehiclesDeliveredBitsets.iFourVehiclesDeliveredBitset1, iDropOffId)
			RETURN 4
		ELIF IS_BIT_SET(serverBd.sNumberVehiclesDeliveredBitsets.iThreeVehiclesDeliveredBitset1, iDropOffId)
			RETURN 3
		ELIF IS_BIT_SET(serverBd.sNumberVehiclesDeliveredBitsets.iTwoVehiclesDeliveredBitset1, iDropOffId)
			RETURN 2
		ELIF IS_BIT_SET(serverBd.sNumberVehiclesDeliveredBitsets.iOneVehicleDeliveredBitset1, iDropOffId)
			RETURN 1
		ELSE
			RETURN 0
		ENDIF
		
	ENDIF
	
	RETURN 0
	
ENDFUNC

PROC SET_NUM_VEHICLES_DELIVERED_TO_DROP_OFF(INT iDropOffArea, INT iDropOff, INT iNumberVehicleDelivered)
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
	
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 errorMsg = "[GBCARJCK] - Client set num vehs del to drop. Client = "
		errorMsg += PARTICIPANT_ID_TO_INT()
		SCRIPT_ASSERT(errorMsg)
		#ENDIF
		
		EXIT
		
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF GET_NUM_VEHICLES_DELIVERED_TO_DROP_OFF(iDropOffArea, iDropOff) != iNumberVehicleDelivered
		PRINTLN("[GBCARJCK] - SET_NUM_VEHICLES_DELIVERED_TO_DROP_OFF - set iDropOffArea ", iDropOffArea, ", iDropOff ", iDropOff, " to num vehicles delivered = ", iNumberVehicleDelivered)
		DEBUG_PRINTCALLSTACK()
	ENDIF
	#ENDIF
	
	INT iDropOffId = GET_DROP_OFF_ID(iDropOffArea, iDropOff)
	
	IF iDropOffId <= 31
		
		IF iNumberVehicleDelivered = 4
			SET_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iFourVehiclesDeliveredBitset0, iDropOffId)
			CLEAR_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iThreeVehiclesDeliveredBitset0, iDropOffId)
			CLEAR_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iTwoVehiclesDeliveredBitset0, iDropOffId)
			CLEAR_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iOneVehicleDeliveredBitset0, iDropOffId)
		ELIF iNumberVehicleDelivered = 3
			CLEAR_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iFourVehiclesDeliveredBitset0, iDropOffId)
			SET_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iThreeVehiclesDeliveredBitset0, iDropOffId)
			CLEAR_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iTwoVehiclesDeliveredBitset0, iDropOffId)
			CLEAR_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iOneVehicleDeliveredBitset0, iDropOffId)
		ELIF iNumberVehicleDelivered = 2
			CLEAR_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iFourVehiclesDeliveredBitset0, iDropOffId)
			CLEAR_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iThreeVehiclesDeliveredBitset0, iDropOffId)
			SET_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iTwoVehiclesDeliveredBitset0, iDropOffId)
			CLEAR_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iOneVehicleDeliveredBitset0, iDropOffId)
		ELIF iNumberVehicleDelivered = 1
			CLEAR_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iFourVehiclesDeliveredBitset0, iDropOffId)
			CLEAR_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iThreeVehiclesDeliveredBitset0, iDropOffId)
			CLEAR_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iTwoVehiclesDeliveredBitset0, iDropOffId)
			SET_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iOneVehicleDeliveredBitset0, iDropOffId)
		ELIF iNumberVehicleDelivered = 0
			CLEAR_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iFourVehiclesDeliveredBitset0, iDropOffId)
			CLEAR_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iThreeVehiclesDeliveredBitset0, iDropOffId)
			CLEAR_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iTwoVehiclesDeliveredBitset0, iDropOffId)
			CLEAR_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iOneVehicleDeliveredBitset0, iDropOffId)
		ELSE
			SCRIPT_ASSERT("Carjacking - SET_NUM_VEHICLES_DELIVERED_TO_DROP_OFF - invalid drop off. Not setting count.")
		ENDIF
		
	ELSE
		
		iDropOffId -= 32
		
		IF iNumberVehicleDelivered = 4
			SET_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iFourVehiclesDeliveredBitset1, iDropOffId)
			CLEAR_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iThreeVehiclesDeliveredBitset1, iDropOffId)
			CLEAR_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iTwoVehiclesDeliveredBitset1, iDropOffId)
			CLEAR_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iOneVehicleDeliveredBitset1, iDropOffId)
		ELIF iNumberVehicleDelivered = 3
			CLEAR_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iFourVehiclesDeliveredBitset1, iDropOffId)
			SET_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iThreeVehiclesDeliveredBitset1, iDropOffId)
			CLEAR_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iTwoVehiclesDeliveredBitset1, iDropOffId)
			CLEAR_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iOneVehicleDeliveredBitset1, iDropOffId)
		ELIF iNumberVehicleDelivered = 2
			CLEAR_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iFourVehiclesDeliveredBitset1, iDropOffId)
			CLEAR_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iThreeVehiclesDeliveredBitset1, iDropOffId)
			SET_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iTwoVehiclesDeliveredBitset1, iDropOffId)
			CLEAR_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iOneVehicleDeliveredBitset1, iDropOffId)
		ELIF iNumberVehicleDelivered = 1
			CLEAR_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iFourVehiclesDeliveredBitset1, iDropOffId)
			CLEAR_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iThreeVehiclesDeliveredBitset1, iDropOffId)
			CLEAR_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iTwoVehiclesDeliveredBitset1, iDropOffId)
			SET_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iOneVehicleDeliveredBitset1, iDropOffId)
		ELIF iNumberVehicleDelivered = 0
			CLEAR_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iFourVehiclesDeliveredBitset1, iDropOffId)
			CLEAR_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iThreeVehiclesDeliveredBitset1, iDropOffId)
			CLEAR_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iTwoVehiclesDeliveredBitset1, iDropOffId)
			CLEAR_BIT(serverBd.sNumberVehiclesDeliveredBitsets.iOneVehicleDeliveredBitset1, iDropOffId)
		ELSE
			SCRIPT_ASSERT("Carjacking - SET_NUM_VEHICLES_DELIVERED_TO_DROP_OFF - invalid drop off. Not setting count.")
		ENDIF
		
	ENDIF
	
ENDPROC

PROC INCREMENT_NUM_VEHICLES_DELIVERED_TO_DROP_OFF(INT iDropOffArea, INT iDropOff)
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		#IF IS_DEBUG_BUILD
		SCRIPT_ASSERT("[GBCARJCK] non host calling INCREMENT_NUM_VEHICLES_DELIVERED_TO_DROP_OFF")
		PRINTLN("[GBCARJCK] non host calling INCREMENT_NUM_VEHICLES_DELIVERED_TO_DROP_OFF")
		#ENDIF
		EXIT
	ENDIF
	
	INT iNumberDelivered = (GET_NUM_VEHICLES_DELIVERED_TO_DROP_OFF(iDropOffArea, iDropOff) + 1)
	
	#IF IS_DEBUG_BUILD
	IF GET_NUM_VEHICLES_DELIVERED_TO_DROP_OFF(iDropOffArea, iDropOff) != iNumberDelivered
		PRINTLN("[GBCARJCK] - INCREMENT_NUM_VEHICLES_DELIVERED_TO_DROP_OFF - set iDropOffArea ", iDropOffArea, ", iDropOff ", iDropOff, " to num vehicles delivered = ", iNumberDelivered)
		DEBUG_PRINTCALLSTACK()
	ENDIF
	#ENDIF
	
	SET_NUM_VEHICLES_DELIVERED_TO_DROP_OFF(iDropOffArea, iDropOff, iNumberDelivered)
	
ENDPROC

//PROC SET_NUMBER_OF_VEHICLES_DELIVERED_IN_SLOT(INT iSlot, INT iNumberDelivered)
//	
//	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
//		#IF IS_DEBUG_BUILD
//		SCRIPT_ASSERT("[GBCARJCK] non host calling SET_NUMBER_OF_VEHICLES_DELIVERED_IN_SLOT")
//		PRINTLN("[GBCARJCK] non host calling SET_NUMBER_OF_VEHICLES_DELIVERED_IN_SLOT")
//		#ENDIF
//		EXIT
//	ENDIF
//	
//	#IF IS_DEBUG_BUILD
//	IF serverBD.sVehiclesDeliveredParticipantData[iSlot].iNumberVehiclesDelivered != iNumberDelivered
//		PRINTLN("[GBCARJCK] SET_NUMBER_OF_VEHICLES_DELIVERED_IN_SLOT iSlot = ", iSlot, ", iNumberVehiclesDelivered = ", iNumberDelivered)
//	ENDIF
//	#ENDIF
//	
//	serverBD.sVehiclesDeliveredParticipantData[iSlot].iNumberVehiclesDelivered = iNumberDelivered
//	
//ENDPROC
//
//PROC INCREMENT_NUMBER_OF_VEHICLES_DELIVERED_IN_SLOT(INT iSlot)
//	
//	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
//		#IF IS_DEBUG_BUILD
//		SCRIPT_ASSERT("[GBCARJCK] non host calling INCREMENT_NUMBER_OF_VEHICLES_DELIVERED_IN_SLOT")
//		PRINTLN("[GBCARJCK] non host calling INCREMENT_NUMBER_OF_VEHICLES_DELIVERED_IN_SLOT")
//		#ENDIF
//		EXIT
//	ENDIF
//	
//	INT iNumberDelivered = (serverBD.sVehiclesDeliveredParticipantData[iSlot].iNumberVehiclesDelivered + 1)
//	
//	#IF IS_DEBUG_BUILD
//	IF serverBD.sVehiclesDeliveredParticipantData[iSlot].iNumberVehiclesDelivered != iNumberDelivered
//		PRINTLN("[GBCARJCK] INCREMENT_NUMBER_OF_VEHICLES_DELIVERED_IN_SLOT iSlot = ", iSlot, ", iNumberVehiclesDelivered = ", iNumberDelivered)
//	ENDIF
//	#ENDIF
//	
//	SET_NUMBER_OF_VEHICLES_DELIVERED_IN_SLOT(iSlot, iNumberDelivered)
//	
//ENDPROC
//
//FUNC INT GET_NUMBER_OF_VEHICLES_DELIVERED_IN_SLOT(INT iSlot)
//	
//	RETURN serverBD.sVehiclesDeliveredParticipantData[iSlot].iNumberVehiclesDelivered
//	
//ENDFUNC

FUNC FLOAT GET_DROP_OFF_LOCATION_RADIUS(INT iDropOffGroup, INT iDropoff)
	
	SWITCH	iDropOffGroup					
		CASE 	0				
			SWITCH	iDropOff			
				CASE	0	RETURN	10.0
				CASE 	1	RETURN	10.0
				CASE 	2	RETURN	10.0
				CASE 	3	RETURN	10.0
				CASE 	4	RETURN	7.5
				CASE 	5	RETURN	10.0
				CASE 	6	RETURN	12.0
				CASE 	7	RETURN	12.0
				CASE 	8	RETURN	10.0
				CASE 	9	RETURN	10.0
			ENDSWITCH				
		BREAK					
		CASE 	1				
			SWITCH	iDropOff			
				CASE	0	RETURN	10.0
				CASE 	1	RETURN	10.0
				CASE 	2	RETURN	10.0
				CASE 	3	RETURN	10.0
				CASE 	4	RETURN	10.0
				CASE 	5	RETURN	10.0
				CASE 	6	RETURN	10.0
				CASE 	7	RETURN	10.0
				CASE 	8	RETURN	10.0
				CASE 	9	RETURN	10.0
			ENDSWITCH				
		BREAK					
		CASE 	2				
			SWITCH	iDropOff			
				CASE	0	RETURN	10.0
				CASE 	1	RETURN	10.0
				CASE 	2	RETURN	10.0
				CASE 	3	RETURN	10.0
				CASE 	4	RETURN	8.5
				CASE 	5	RETURN	10.0
				CASE 	6	RETURN	10.0
				CASE 	7	RETURN	10.0
				CASE 	8	RETURN	9.0
				CASE 	9	RETURN	10.0
			ENDSWITCH				
		BREAK					
		CASE 	3				
			SWITCH	iDropOff			
				CASE	0	RETURN	10.0
				CASE 	1	RETURN	10.0
				CASE 	2	RETURN	10.0
				CASE 	3	RETURN	10.0
				CASE 	4	RETURN	10.0
				CASE 	5	RETURN	10.0
				CASE 	6	RETURN	10.0
				CASE 	7	RETURN	10.0
				CASE 	8	RETURN	10.0
				CASE 	9	RETURN	10.0
			ENDSWITCH				
		BREAK					
		CASE 	4				
			SWITCH	iDropOff			
				CASE	0	RETURN	10.0
				CASE 	1	RETURN	10.0
				CASE 	2	RETURN	10.0
				CASE 	3	RETURN	10.0
				CASE 	4	RETURN	10.0
				CASE 	5	RETURN	10.0
				CASE 	6	RETURN	10.0
				CASE 	7	RETURN	10.0
				CASE 	8	RETURN	10.0
				CASE 	9	RETURN	10.0
			ENDSWITCH				
		BREAK					
		CASE 	5				
			SWITCH	iDropOff			
				CASE	0	RETURN	10.0
				CASE 	1	RETURN	10.0
				CASE 	2	RETURN	10.0
				CASE 	3	RETURN	10.0
				CASE 	4	RETURN	10.0
				CASE 	5	RETURN	10.0
				CASE 	6	RETURN	10.0
				CASE 	7	RETURN	10.0
				CASE 	8	RETURN	10.0
				CASE 	9	RETURN	10.0
			ENDSWITCH				
		BREAK					
	ENDSWITCH						
	
	SCRIPT_ASSERT("[GBCARJCK] - GET_DROP_OFF_LOCATION_RADIUS invalid drop off looked up.")
	RETURN 0.0
	
ENDFUNC

FUNC BOOL GET_IS_DROP_OFF_CITY_LOCATION(INT iDropoff)
	
	SWITCH iDropoff
		CASE 0	RETURN TRUE
		CASE 1 	RETURN TRUE
		CASE 2	RETURN TRUE
		CASE 3	RETURN FALSE
		CASE 4	RETURN FALSE
		CASE 5	RETURN FALSE
	ENDSWITCH
	
	SCRIPT_ASSERT("[GBCARJCK] - GET_IS_DROP_OFF_CITY_LOCATION invalid drop off looked up.")
	RETURN FALSE
	
ENDFUNC

/*
FUNC VECTOR GET_DROP_OFF_LOCATE_MIN_COORDS(INT iDropoff, INT iLocate)
	
	SWITCH iDropoff
		CASE 0 	
			SWITCH iLocate
				CASE 0	RETURN << -754.277161,352.240540,86.873856 >>
	//			CASE 1	RETURN 
	//			CASE 2	RETURN 
	//			CASE 3	RETURN 
	//			CASE 4	RETURN 
			ENDSWITCH
		BREAK
		CASE 1 	 	
			SWITCH iLocate
				CASE 0	RETURN << 1214.482300,-1262.085205,34.391621 >>
	//			CASE 1	RETURN 
	//			CASE 2	RETURN 
	//			CASE 3	RETURN 
	//			CASE 4	RETURN 
			ENDSWITCH
		BREAK
		CASE 2 	 	 	 	
			SWITCH iLocate
				CASE 0	RETURN << -985.918823,-2224.450684,7.888162 >>
	//			CASE 1	RETURN 
	//			CASE 2	RETURN 
	//			CASE 3	RETURN 
	//			CASE 4	RETURN 
			ENDSWITCH
		BREAK
		CASE 3 	 	 	 	
			SWITCH iLocate
				CASE 0	RETURN <<-97.408463,2811.512451,52.251236>>
	//			CASE 1	RETURN 
	//			CASE 2	RETURN 
	//			CASE 3	RETURN 
	//			CASE 4	RETURN 
			ENDSWITCH
		BREAK
		CASE 4 	 	 	 	
			SWITCH iLocate
				CASE 0	RETURN <<2889.386963,4488.860352,46.979511>>
				CASE 1	RETURN << 2909.042480,4474.371582,47.123962 >>
	//			CASE 2	RETURN 
	//			CASE 3	RETURN 
	//			CASE 4	RETURN 
			ENDSWITCH
		BREAK
		CASE 5 	  	 	
			SWITCH iLocate	
				CASE 0	RETURN << 147.161972,6366.667480,30.529045 >>
	//			CASE 1	RETURN 
	//			CASE 2	RETURN 
	//			CASE 3	RETURN 
	//			CASE 4	RETURN 
			ENDSWITCH
		BREAK
	ENDSWITCH
	
//	SCRIPT_ASSERT("[GBCARJCK] - GET_DROP_OFF_LOCATE_MIN_COORDS invalid drop off or locate looked up.")
	RETURN << 0.0, 0.0, 0.0 >>
	
ENDFUNC

FUNC VECTOR GET_DROP_OFF_LOCATE_MAX_COORDS(INT iDropoff, INT iLocate)
	
	SWITCH iDropoff
		CASE 0 	 	 	
			SWITCH iLocate
				CASE 0	RETURN << -754.341003,362.492157,90.863335 >>
	//			CASE 1	RETURN 
	//			CASE 2	RETURN 
	//			CASE 3	RETURN 
	//			CASE 4	RETURN 
			ENDSWITCH
		BREAK
		CASE 1 	  	 	
			SWITCH iLocate	
				CASE 0	RETURN << 1190.591553,-1261.475952,39.255779 >>
	//			CASE 1	RETURN 
	//			CASE 2	RETURN 
	//			CASE 3	RETURN 
	//			CASE 4	RETURN 
			ENDSWITCH
		BREAK
		CASE 2 	 	 	 	
			SWITCH iLocate
				CASE 0	RETURN << -976.226257,-2234.406982,12.861696 >>
	//			CASE 1	RETURN 
	//			CASE 2	RETURN 
	//			CASE 3	RETURN 
	//			CASE 4	RETURN 
			ENDSWITCH
		BREAK
		CASE 3 	 	 	 	
			SWITCH iLocate
				CASE 0	RETURN <<-84.947311,2805.445557,57.341919>>
	//			CASE 1	RETURN 
	//			CASE 2	RETURN 
	//			CASE 3	RETURN 
	//			CASE 4	RETURN 
			ENDSWITCH
		BREAK
		CASE 4 	  	 	
			SWITCH iLocate	
				CASE 0	RETURN <<2884.201660,4477.755371,52.069050>>
				CASE 1	RETURN << 2901.061279,4465.985840,52.141098 >>
	//			CASE 2	RETURN 
	//			CASE 3	RETURN 
	//			CASE 4	RETURN 
			ENDSWITCH
		BREAK
		CASE 5 	 	 	
			SWITCH iLocate 	
				CASE 0	RETURN << 141.739471,6363.890625,35.411064 >>
	//			CASE 1	RETURN 
	//			CASE 2	RETURN 
	//			CASE 3	RETURN 
	//			CASE 4	RETURN 
			ENDSWITCH
		BREAK
	ENDSWITCH
	
//	SCRIPT_ASSERT("[GBCARJCK] - GET_DROP_OFF_LOCATE_MAX_COORDS invalid drop off or locate looked up.")
	RETURN << 0.0, 0.0, 0.0 >>
	
ENDFUNC

FUNC FLOAT GET_DROP_OFF_LOCATE_WIDTH(INT iDropoff, INT iLocate)
	
	SWITCH iDropoff
		CASE 0 	 	 	
			SWITCH iLocate
				CASE 0	RETURN 12.000000
	//			CASE 1	RETURN 
	//			CASE 2	RETURN 
	//			CASE 3	RETURN 
	//			CASE 4	RETURN 
			ENDSWITCH
		BREAK
		CASE 1 	  	 	
			SWITCH iLocate	
				CASE 0	RETURN 18.000000
	//			CASE 1	RETURN 
	//			CASE 2	RETURN 
	//			CASE 3	RETURN 
	//			CASE 4	RETURN 
			ENDSWITCH
		BREAK
		CASE 2 	  	 	
			SWITCH iLocate	
				CASE 0	RETURN 13.000000
	//			CASE 1	RETURN 
	//			CASE 2	RETURN 
	//			CASE 3	RETURN 
	//			CASE 4	RETURN 
			ENDSWITCH
		BREAK
		CASE 3 	  	 	
			SWITCH iLocate	
				CASE 0	RETURN 14.000000
	//			CASE 1	RETURN 
	//			CASE 2	RETURN 
	//			CASE 3	RETURN 
	//			CASE 4	RETURN 
			ENDSWITCH
		BREAK
		CASE 4 	 	 	
			SWITCH iLocate 	
				CASE 0	RETURN 28.000000
				CASE 1	RETURN 20.000000
	//			CASE 2	RETURN 
	//			CASE 3	RETURN 
	//			CASE 4	RETURN 
			ENDSWITCH
		BREAK
		CASE 5 	 	 	 	
			SWITCH iLocate
				CASE 0	RETURN 16.000000
	//			CASE 1	RETURN 
	//			CASE 2	RETURN 
	//			CASE 3	RETURN 
	//			CASE 4	RETURN 
			ENDSWITCH
		BREAK
	ENDSWITCH
	
//	SCRIPT_ASSERT("[GBCARJCK] - GET_DROP_OFF_LOCATE_WIDTH invalid drop off or locate looked up.")
	RETURN 0.0
	
ENDFUNC
*/

PROC SET_TOTAL_VALUE_OF_VEHICLES_DELIVERED_IN_SLOT(INT iSlot, FLOAT fTotalValue)
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		#IF IS_DEBUG_BUILD
		SCRIPT_ASSERT("[GBCARJCK] non host calling SET_TOTAL_VALUE_OF_VEHICLES_DELIVERED_IN_SLOT")
		PRINTLN("[GBCARJCK] non host calling SET_TOTAL_VALUE_OF_VEHICLES_DELIVERED_IN_SLOT")
		#ENDIF
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF serverBD.sVehiclesDeliveredParticipantData[iSlot].fTotalValueOfVehiclesDelivered != fTotalValue
		PRINTLN("[GBCARJCK] SET_TOTAL_VALUE_OF_VEHICLES_DELIVERED_IN_SLOT iSlot = ", iSlot, ", fTotalValue = ", fTotalValue)
	ENDIF
	#ENDIF
	
	serverBD.sVehiclesDeliveredParticipantData[iSlot].fTotalValueOfVehiclesDelivered = fTotalValue
	
ENDPROC

FUNC FLOAT GET_TOTAL_VALUE_OF_VEHICLES_DELIVERED_IN_SLOT(INT iSlot)
	
	RETURN serverBD.sVehiclesDeliveredParticipantData[iSlot].fTotalValueOfVehiclesDelivered
	
ENDFUNC

PROC ADD_TO_TOTAL_VALUE_OF_VEHICLES_DELIVERED_IN_SLOT(INT iSlot, FLOAT fAmountToAdd)
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		#IF IS_DEBUG_BUILD
		SCRIPT_ASSERT("[GBCARJCK] non host calling ADD_TO_TOTAL_VALUE_OF_VEHICLES_DELIVERED_IN_SLOT")
		PRINTLN("[GBCARJCK] non host calling ADD_TO_TOTAL_VALUE_OF_VEHICLES_DELIVERED_IN_SLOT")
		#ENDIF
		EXIT
	ENDIF
	
	FLOAT fCurrentTotalValue = GET_TOTAL_VALUE_OF_VEHICLES_DELIVERED_IN_SLOT(iSlot)
	
	IF fCurrentTotalValue = 0
		fCurrentTotalValue = 0
	ENDIF
	
	FLOAT fTotalValue = (fCurrentTotalValue + fAmountToAdd)
	
	#IF IS_DEBUG_BUILD
	IF fAmountToAdd != 0.0
		PRINTLN("[GBCARJCK] ADD_TO_TOTAL_VALUE_OF_VEHICLES_DELIVERED_IN_SLOT iSlot = ", iSlot, ", fAmountToAdd = ", fAmountToAdd, ", (current total value +  = fAmountToAdd) = ", fTotalValue)
	ENDIF
	#ENDIF
	
	SET_TOTAL_VALUE_OF_VEHICLES_DELIVERED_IN_SLOT(iSlot, fTotalValue)
	
ENDPROC

PROC SET_DROP_OFF_ACTIVE(INT iDropOffArea, INT iDropOff, BOOL bActive)
	
	#IF IS_DEBUG_BUILD
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		SCRIPT_ASSERT("[GBCARJCK] - SET_DROP_OFF_ACTIVE - been called by client, not host.")
		DEBUG_PRINTCALLSTACK()
	ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bActive != IS_BIT_SET(serverBd.iDropOffActiveBitset[iDropOffArea], iDropOff)
		IF bActive
			PRINTLN("[GBCARJCK] - SET_DROP_OFF_ACTIVE - set bit serverBd.iDropOffActiveBitset[", iDropOffArea, "], ", iDropOff)
		ELSE
			PRINTLN("[GBCARJCK] - SET_DROP_OFF_ACTIVE - cleared bit serverBd.iDropOffActiveBitset[", iDropOffArea, "], ", iDropOff)
		ENDIF
	ENDIF
	#ENDIF
	
	IF bActive
		SET_BIT(serverBd.iDropOffActiveBitset[iDropOffArea], iDropOff)
	ELSE
		CLEAR_BIT(serverBd.iDropOffActiveBitset[iDropOffArea], iDropOff)
	ENDIF
	
ENDPROC

FUNC BOOL IS_DROP_OFF_ACTIVE(INT iDropOffArea, INT iDropOff)
	
	RETURN IS_BIT_SET(serverBd.iDropOffActiveBitset[iDropOffArea], iDropOff)
	
ENDFUNC

PROC SET_DROP_OFF_FULL(INT iDropOffArea, INT iDropOff, BOOL bFull)
	
	#IF IS_DEBUG_BUILD
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		SCRIPT_ASSERT("[GBCARJCK] - SET_DROP_OFF_FULL - been called by client, not host.")
		DEBUG_PRINTCALLSTACK()
	ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bFull != IS_BIT_SET(serverBd.iDropOffFullBitset[iDropOffArea], iDropOff)
		IF bFull
			PRINTLN("[GBCARJCK] - SET_DROP_OFF_FULL - set bit serverBd.iDropOffFullBitset[", iDropOffArea, "], ", iDropOff)
		ELSE
			PRINTLN("[GBCARJCK] - SET_DROP_OFF_FULL - cleared bit serverBd.iDropOffFullBitset[", iDropOffArea, "], ", iDropOff)
		ENDIF
	ENDIF
	#ENDIF
	
	IF bFull
		SET_BIT(serverBd.iDropOffFullBitset[iDropOffArea], iDropOff)
	ELSE
		CLEAR_BIT(serverBd.iDropOffFullBitset[iDropOffArea], iDropOff)
	ENDIF
	
ENDPROC

FUNC BOOL IS_DROP_OFF_FULL(INT iDropOffArea, INT iDropOff)
	
	RETURN IS_BIT_SET(serverBd.iDropOffFullBitset[iDropOffArea], iDropOff)
	
ENDFUNC

PROC MAINTAIN_DROP_OFF_CHANGE_HELP()
	
	INT iDropOffArea, iDropOffArea2
	
	// If the active bitset has changed, a drop off has changed and we need to show help.
	// Note if we are comparing a zero value with thr server bitset, this means this is the first time the drop off has been activated, so no need for help.
	IF iDropOffChangedHelpStage >= 1
		REPEAT GET_CARJACKING_TUNEABLE_NUM_ACTIVE_DROP_OFF_AREAS() iDropOffArea
			IF iDropOffActiveBitsetCopy[iDropOffArea] != serverBd.iDropOffActiveBitset[iDropOffArea]
				IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_PRINTED_DROP_OFF_CHANGE_HELP)
					IF iDropOffActiveBitsetCopy[iDropOffArea] != 0
						IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
							IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
								IF IS_LOCAL_BIT0_SET(eLOCALBITSET0_PRINTED_EXPLNATORY_HELP_3)
									PRINTLN("[GBCARJCK] - MAINTAIN_DROP_OFF_CHANGE_HELP - iDropOffActiveBitsetCopy[", iDropOffArea, "] = ", iDropOffActiveBitsetCopy[iDropOffArea])
									PRINTLN("[GBCARJCK] - MAINTAIN_DROP_OFF_CHANGE_HELP - serverBd.iDropOffActiveBitset[", iDropOffArea, "] = ", serverBd.iDropOffActiveBitset[iDropOffArea])
									PRINTLN("[GBCARJCK] - MAINTAIN_DROP_OFF_CHANGE_HELP - these are different, printing help 'CARJCK_DROPCHG'")
									PRINT_HELP("CARJCK_DROPCHG")
									GB_SET_GANG_BOSS_HELP_BACKGROUND()
									SET_LOCAL_BIT0(eLOCALBITSET0_PRINTED_DROP_OFF_CHANGE_HELP)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					iDropOffActiveBitsetCopy[iDropOffArea] = serverBd.iDropOffActiveBitset[iDropOffArea]
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	// Track the help being shown, move on and wait for it to stop being shown, reset and wait for the logic above to show it again.
	SWITCH iDropOffChangedHelpStage
		CASE 0
			REPEAT GET_CARJACKING_TUNEABLE_NUM_ACTIVE_DROP_OFF_AREAS() iDropOffArea2
				iDropOffActiveBitsetCopy[iDropOffArea2] = serverBd.iDropOffActiveBitset[iDropOffArea2]
			ENDREPEAT
			iDropOffChangedHelpStage++
			PRINTLN("[GBCARJCK] - MAINTAIN_DROP_OFF_CHANGE_HELP - did initial copy setup. Setting iDropOffChangedHelpStage = ", iDropOffChangedHelpStage)
		BREAK
		CASE 1
			IF IS_LOCAL_BIT0_SET(eLOCALBITSET0_PRINTED_DROP_OFF_CHANGE_HELP)
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CARJCK_DROPCHG")
					iDropOffChangedHelpStage++
					PRINTLN("[GBCARJCK] - MAINTAIN_DROP_OFF_CHANGE_HELP - help 'CARJCK_DROPCHG' is being shown. Setting iDropOffChangedHelpStage = ", iDropOffChangedHelpStage)
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CARJCK_DROPCHG")
				CLEAR_LOCAL_BIT0(eLOCALBITSET0_PRINTED_DROP_OFF_CHANGE_HELP)
				iDropOffChangedHelpStage = 1
				PRINTLN("[GBCARJCK] - MAINTAIN_DROP_OFF_CHANGE_HELP - help 'CARJCK_DROPCHG' has finished showing. Setting iDropOffChangedHelpStage = ", iDropOffChangedHelpStage)
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROc

#IF IS_DEBUG_BUILD
PROC MAINTAIN_J_SKIPS()
	
	IF NOT IS_CLIENT_DEBUG_BIT0_SET(PARTICIPANT_ID(), eCLIENTDEBUGBITSET0_I_HAVE_S_PASSED)
		IF IS_DEBUG_KEY_JUST_RELEASED(KEY_S, KEYBOARD_MODIFIER_NONE, "S Pass")
			PRINTLN("[GBCARJCK] KEY_S, KEYBOARD_MODIFIER_NONE just released")
			SET_CLIENT_DEBUG_BIT0(eCLIENTDEBUGBITSET0_I_HAVE_S_PASSED)
		ENDIF
	ENDIF
	
	IF NOT IS_CLIENT_DEBUG_BIT0_SET(PARTICIPANT_ID(), eCLIENTDEBUGBITSET0_I_HAVE_S_PASSED)
		IF IS_DEBUG_KEY_JUST_RELEASED(KEY_F, KEYBOARD_MODIFIER_NONE, "F Fail")
			PRINTLN("[GBCARJCK] KEY_F, KEYBOARD_MODIFIER_NONE just released")
			SET_CLIENT_DEBUG_BIT0(eCLIENTDEBUGBITSET0_I_HAVE_F_FAILED)
		ENDIF
	ENDIF
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF serverBD.iSPassPart > (-1)
			PRINTLN("[GBCARJCK] serverBD.iSPassPart > (-1)")
			IF GET_CARJACKING_STATE() < eCARJACKING_STATE_REWARDS
				SET_END_REASON(eENDREASON_TIME_UP)
				SET_CARJACKING_STATE(eCARJACKING_STATE_REWARDS)
			ENDIF
		ELIF serverBD.iFFailPart > (-1)
			PRINTLN("[GBCARJCK] serverBD.iFFailPart > (-1)")
			IF GET_CARJACKING_STATE() < eCARJACKING_STATE_REWARDS
				SET_END_REASON(eENDREASON_TIME_UP)
				SET_CARJACKING_STATE(eCARJACKING_STATE_REWARDS)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC
#ENDIF

/*
FUNC STRING GET_STEAL_VEHICLE_OBJECTIVE_TEXT_LABEL()
	
	SWITCH GET_TARGET_VEHICLE_CLASS()
		CASE VC_COMPACT 		RETURN "CARJCK_STCOMP"
	    CASE VC_SEDAN 			RETURN "CARJCK_STSED"
	    CASE VC_SUV 			RETURN "CARJCK_STSUV"
	    CASE VC_COUPE 			RETURN "CARJCK_STCOUP"
	   	CASE VC_MUSCLE 			RETURN "CARJCK_STMUSC"
	    CASE VC_SPORT_CLASSIC	RETURN "CARJCK_STSPCL"
	    CASE VC_SPORT 			RETURN "CARJCK_STSPT"
	    CASE VC_SUPER 			RETURN "CARJCK_STSUPR"
	    CASE VC_MOTORCYCLE 		RETURN "CARJCK_STMCYC"
	    CASE VC_OFF_ROAD 		RETURN "CARJCK_STOFFR"
	    CASE VC_INDUSTRIAL 		RETURN "CARJCK_STIND"
	    CASE VC_UTILITY 		RETURN "CARJCK_STUTIL"
	    CASE VC_VAN 			RETURN "CARJCK_STVAN"
	    CASE VC_CYCLE 			RETURN "CARJCK_STCYC"
	    CASE VC_BOAT 			RETURN "CARJCK_STBOAT"
	    CASE VC_HELICOPTER 		RETURN "CARJCK_STHELI"
	    CASE VC_PLANE 			RETURN "CARJCK_STPLN"
		CASE VC_SERVICE 		RETURN "CARJCK_STSERV"
		CASE VC_EMERGENCY 		RETURN "CARJCK_STEMRG"
		CASE VC_MILITARY 		RETURN "CARJCK_STMILI"
		CASE VC_COMMERCIAL 		RETURN "CARJCK_STCOMR"
		CASE VC_RAIL 			RETURN "CARJCK_STRAIL"
	ENDSWITCH
	
	RETURN "***INVALID***"
	
ENDFUNC
*/

FUNC FLOAT GET_THIS_VEHICLE_VALUE(VEHICLE_INDEX veh, MODEL_NAMES model)
	
	VEHICLE_VALUE_DATA sValueData
	FLOAT fValue
	
    IF GET_VEHICLE_VALUE(sValueData, model, IS_VEHICLE_ALT_VERSION(veh))
        fValue = TO_FLOAT(sValueData.iStandardPrice)
//        PRINTLN("[GBCARJCK] GET_VEHICLE_VALUE - fValue = ", fValue)
    ENDIF
                      
    IF fValue <= 0
        fValue = TO_FLOAT(GET_VEHICLE_MODEL_VALUE(model))
//        PRINTLN("[GBCARJCK] GET_VEHICLE_VALUE - No vehicle value, using = GET_VEHICLE_MODEL_VALUE - fValue = ", fValue)
    ENDIF
	
	IF fValue <= 0
		fValue = 20000
//        PRINTLN("[GBCARJCK] GET_VEHICLE_VALUE - No vehicle value, using default value - fValue = ", fValue)
	ENDIF
	
	RETURN fValue
	
ENDFUNC

FUNC BOOL SAVE_MY_DELIVERED_VEHICLE_DATA()
	
	IF DOES_ENTITY_EXIST(sMyDeliveredVehicleData.myDeliveredVehicle)
		RETURN TRUE
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_ENTITY_DEAD(PLAYER_PED_ID())
		RETURN FALSE 
	ENDIF
	
	IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_IS_FOCUS_PLAYER_IN_ANY_VEHICLE)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_FOCUS_PLAYER_CURRENT_VEHICLE_IS_DRIVEABLE)
		RETURN FALSE
	ENDIF
	
	sMyDeliveredVehicleData.myDeliveredVehicle = currentVehicle
	
	sMyDeliveredVehicleData.delivererPartId = PARTICIPANT_ID()
	PRINTLN("[GBCARJCK] SAVE_MY_DELIVERED_VEHICLE_DATA - sMyDeliveredVehicleData.delivererPartId = ", NATIVE_TO_INT(sMyDeliveredVehicleData.delivererPartId))
	
	sMyDeliveredVehicleData.fVehicleValue = fCurrentVehicleValue
	PRINTLN("[GBCARJCK] SAVE_MY_DELIVERED_VEHICLE_DATA - final sMyDeliveredVehicleData.fVehicleValue = ", sMyDeliveredVehicleData.fVehicleValue)
	
	sMyDeliveredVehicleData.tl63DelivererName = GET_PLAYER_NAME(PLAYER_ID())
	PRINTLN("[GBCARJCK] SAVE_MY_DELIVERED_VEHICLE_DATA - sMyDeliveredVehicleData.tl63DelivererName = ", sMyDeliveredVehicleData.tl63DelivererName)
	
	sMyDeliveredVehicleData.iDropOffArea = GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iClosestCarjackingDropOffArea
	PRINTLN("[GBCARJCK] SAVE_MY_DELIVERED_VEHICLE_DATA - sMyDeliveredVehicleData.iDropOffArea = ", sMyDeliveredVehicleData.iDropOffArea)
	
	sMyDeliveredVehicleData.iDropOff = GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iClosestCarjackingDropOffinArea
	PRINTLN("[GBCARJCK] SAVE_MY_DELIVERED_VEHICLE_DATA - sMyDeliveredVehicleData.iDropOff = ", sMyDeliveredVehicleData.iDropOff)
	
//	IF NOT HAS_NET_TIMER_STARTED(sMyDeliveredVehicleData.deliveryTime)
//		START_NET_TIMER(sMyDeliveredVehicleData.deliveryTime)
//	ENDIF
										
	RETURN TRUE
	
ENDFUNC

PROC RESET_MY_DELIVERED_VEHICLE_DATA()
	
	VEHICLE_INDEX tempVeh
	TEXT_LABEL_63 tl63Temp
	//SCRIPT_TIMER tempTimer
	
	sMyDeliveredVehicleData.myDeliveredVehicle = tempVeh
	sMyDeliveredVehicleData.delivererPartId = INVALID_PARTICIPANT_INDEX()
	//sMyDeliveredVehicleData.vehicleModel = DUMMY_MODEL_FOR_SCRIPT
	sMyDeliveredVehicleData.fVehicleValue = 0.0
	sMyDeliveredVehicleData.tl63DelivererName = tl63Temp
	//sMyDeliveredVehicleData.deliveryTime = tempTimer
	sMyDeliveredVehicleData.iDropOffArea = -1
	sMyDeliveredVehicleData.iDropOff = -1
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[GBCARJCK] - RESET_MY_DELIVERED_VEHICLE_DATA been called:")
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	
ENDPROC

STRUCT STRUCT_SCRIPT_EVENT_ACQUIRE_VEHICLE_DELIVERED_VEHICLE
	STRUCT_EVENT_COMMON_DETAILS Details
	STRUCT_DELIVERED_VEHICLE_DATA sDeliveredVehicleData	
ENDSTRUCT
 
STRUCT STRUCT_SCRIPT_EVENT_SERVER_RECEIVED_ACQUIRE_VEHICLE_DELIVERED_VEHICLE_EVENT
	STRUCT_EVENT_COMMON_DETAILS Details
	BOOL bAllowedDropOff
ENDSTRUCT

PROC BROADCAST_GB_SCRIPT_EVENT_ACQUIRE_VEHICLE_DELIVERED_VEHICLE(PLAYER_INDEX playerToSendTo, FLOAT fVehicleValue, TEXT_LABEL_63 delivererName, INT iAreaDroppedOffTo, INT iDropOffDeliveredTo) //(PLAYER_INDEX playerToSendTo, MODEL_NAMES vehModel, FLOAT fVehicleValue, TEXT_LABEL_63 delivererName, SCRIPT_TIMER deliveryTime)
	
	STRUCT_SCRIPT_EVENT_ACQUIRE_VEHICLE_DELIVERED_VEHICLE sEvent
	
	sEvent.Details.Type 							= SCRIPT_EVENT_ACQUIRE_VEHICLE_DELIVERED_VEHICLE
	sEvent.Details.FromPlayerIndex 					= PLAYER_ID()
	
	sEvent.sDeliveredVehicleData.delivererPartId	= PARTICIPANT_ID()
	//sEvent.sDeliveredVehicleData.vehicleModel		= vehModel
	sEvent.sDeliveredVehicleData.fVehicleValue		= fVehicleValue
	sEvent.sDeliveredVehicleData.tl63DelivererName 	= delivererName
	//sEvent.sDeliveredVehicleData.deliveryTime		= deliveryTime
	sEvent.sDeliveredVehicleData.iDropOffArea		= iAreaDroppedOffTo
	sEvent.sDeliveredVehicleData.iDropOff 			= iDropOffDeliveredTo
	
	IF playerToSendTo != INVALID_PLAYER_INDEX()
		IF NETWORK_IS_PLAYER_ACTIVE(playerToSendTo)
			PRINTLN("     ----->    [GBCARJCK] BROADCAST_GB_SCRIPT_EVENT_ACQUIRE_VEHICLE_DELIVERED_VEHICLE - SENT BY ", GET_PLAYER_NAME(PLAYER_ID())," at GET_CLOUD_TIME_AS_INT = ", GET_CLOUD_TIME_AS_INT())
			PRINTLN("[GBCARJCK] BROADCAST_GB_SCRIPT_EVENT_ACQUIRE_VEHICLE_DELIVERED_VEHICLE - sEvent.sDeliveredVehicleData.delivererPartId = ", NATIVE_TO_INT(sEvent.sDeliveredVehicleData.delivererPartId))
			//PRINTLN("[GBCARJCK] BROADCAST_GB_SCRIPT_EVENT_ACQUIRE_VEHICLE_DELIVERED_VEHICLE - sEvent.sDeliveredVehicleData.vehicleModel = ", ENUM_TO_INT(sEvent.sDeliveredVehicleData.vehicleModel))
			PRINTLN("[GBCARJCK] BROADCAST_GB_SCRIPT_EVENT_ACQUIRE_VEHICLE_DELIVERED_VEHICLE - sEvent.sDeliveredVehicleData.fVehicleValue = ", sEvent.sDeliveredVehicleData.fVehicleValue)
			PRINTLN("[GBCARJCK] BROADCAST_GB_SCRIPT_EVENT_ACQUIRE_VEHICLE_DELIVERED_VEHICLE - sEvent.sDeliveredVehicleData.tl63DelivererName = ", sEvent.sDeliveredVehicleData.tl63DelivererName)
//			#IF IS_DEBUG_BUILD
//			INT iTemp = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sEvent.sDeliveredVehicleData.deliveryTime)
//			PRINTLN("[GBCARJCK] GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sEvent.sDeliveredVehicleData.deliveryTime) = ", iTemp)
//			#ENDIF
			PRINTLN("[GBCARJCK] BROADCAST_GB_SCRIPT_EVENT_ACQUIRE_VEHICLE_DELIVERED_VEHICLE - sEvent.sDeliveredVehicleData.iDropOffArea = ", sEvent.sDeliveredVehicleData.iDropOffArea)
			PRINTLN("[GBCARJCK] BROADCAST_GB_SCRIPT_EVENT_ACQUIRE_VEHICLE_DELIVERED_VEHICLE - sEvent.sDeliveredVehicleData.iDropOff = ", sEvent.sDeliveredVehicleData.iDropOff)
			#IF IS_DEBUG_BUILD
			DEBUG_PRINTCALLSTACK()
			#ENDIF
			SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, sEvent, SIZE_OF(sEvent), SPECIFIC_PLAYER(playerToSendTo))	
		ELSE
			PRINTLN("[GBCARJCK] BROADCAST_GB_SCRIPT_EVENT_ACQUIRE_VEHICLE_DELIVERED_VEHICLE - NETWORK_IS_PLAYER_ACTIVE = FALSE so not broadcasting")	
		ENDIF
	ELSE
		PRINTLN("[GBCARJCK] BROADCAST_GB_SCRIPT_EVENT_ACQUIRE_VEHICLE_DELIVERED_VEHICLE - playerToSendTo = INVALID_PLAYER_INDEX() so not broadcasting")	
	ENDIF
	
ENDPROC

PROC BROADCAST_GB_SCRIPT_EVENT_SERVER_RECEIVED_ACQUIRE_VEHICLE_DELIVERED_VEHICLE_EVENT(PLAYER_INDEX playerToSendTo, BOOL bbAllowedDropOff)
	
	STRUCT_SCRIPT_EVENT_SERVER_RECEIVED_ACQUIRE_VEHICLE_DELIVERED_VEHICLE_EVENT sEvent
	
	sEvent.Details.Type 							= SCRIPT_EVENT_SERVER_RECEIVED_ACQUIRE_VEHICLE_DELIVERED_VEHICLE_EVENT
	sEvent.Details.FromPlayerIndex 					= PLAYER_ID()
	sEvent.bAllowedDropOff 							= bbAllowedDropOff
	
	IF playerToSendTo != INVALID_PLAYER_INDEX()
		IF NETWORK_IS_PLAYER_ACTIVE(playerToSendTo)
			PRINTLN("     ----->    [GBCARJCK] BROADCAST_GB_SCRIPT_EVENT_SERVER_RECEIVED_ACQUIRE_VEHICLE_DELIVERED_VEHICLE_EVENT - SENT BY ", GET_PLAYER_NAME(PLAYER_ID())," at GET_CLOUD_TIME_AS_INT = ", GET_CLOUD_TIME_AS_INT())
			SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, sEvent, SIZE_OF(sEvent), SPECIFIC_PLAYER(playerToSendTo))	
		ELSE
			PRINTLN("[GBCARJCK] BROADCAST_GB_SCRIPT_EVENT_SERVER_RECEIVED_ACQUIRE_VEHICLE_DELIVERED_VEHICLE_EVENT - NETWORK_IS_PLAYER_ACTIVE = FALSE so not broadcasting")	
		ENDIF
	ELSE
		PRINTLN("[GBCARJCK] BROADCAST_GB_SCRIPT_EVENT_SERVER_RECEIVED_ACQUIRE_VEHICLE_DELIVERED_VEHICLE_EVENT - playerToSendTo = INVALID_PLAYER_INDEX() so not broadcasting")	
	ENDIF
	
ENDPROC

PROC SORT_DELIVERED_VEHICLE_DATA()
	
	INT iCount, iCount2
	BOOL bDoSwap
	
	FOR iCount = (GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS() - 1) TO 0 STEP -1
		
		#IF IS_DEBUG_BUILD
			PRINTLN("[GBCARJCK] - SORT_DELIVERED_VEHICLE_DATA ")
			PRINTLN("[GBCARJCK] - SORT_DELIVERED_VEHICLE_DATA ")
			PRINTLN("[GBCARJCK] - SORT_DELIVERED_VEHICLE_DATA *********** VEHICLE ", iCount, " ***********")
		#ENDIF
		
		FOR iCount2 = (GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS() - 1) TO 0 STEP -1
			
			bDoSwap = FALSE
			
			IF ( (iCount2 - 1) >= 0 )
			
				#IF IS_DEBUG_BUILD
				INT iCount3 = iCount2 - 1
				PRINTLN("[GBCARJCK] - SORT_DELIVERED_VEHICLE_DATA iCount2 = ", iCount2, ", iCount2-1 = ", iCount3)
				PRINTLN("[GBCARJCK] - SORT_DELIVERED_VEHICLE_DATA iCount2 partId = ", NATIVE_TO_INT(serverBD.sVehiclesDeliveredParticipantData[iCount2].participantId), ", iCount2-1 partId = ", NATIVE_TO_INT(serverBD.sVehiclesDeliveredParticipantData[iCount2-1].participantId))
//				PRINTLN("[GBCARJCK] - SORT_DELIVERED_VEHICLE_DATA iCount2 num vehs del = ", serverBD.sVehiclesDeliveredParticipantData[iCount2].iNumberVehiclesDelivered, ", iCount2-1 num vehs del = ", serverBD.sVehiclesDeliveredParticipantData[iCount2-1].iNumberVehiclesDelivered)
				PRINTLN("[GBCARJCK] - SORT_DELIVERED_VEHICLE_DATA iCount2 tl63Name = ", serverBD.sVehiclesDeliveredParticipantData[iCount2].tl63Name, ", iCount2-1 tl63Name = ", serverBD.sVehiclesDeliveredParticipantData[iCount2-1].tl63Name)
				PRINTLN("[GBCARJCK] - SORT_DELIVERED_VEHICLE_DATA value = ", serverBD.sVehiclesDeliveredParticipantData[iCount2].fTotalValueOfVehiclesDelivered, ", iCount2-1 value = ", serverBD.sVehiclesDeliveredParticipantData[iCount2-1].fTotalValueOfVehiclesDelivered)
				#ENDIF
				
				IF serverBD.sVehiclesDeliveredParticipantData[iCount2].fTotalValueOfVehiclesDelivered > serverBD.sVehiclesDeliveredParticipantData[iCount2-1].fTotalValueOfVehiclesDelivered
				
					bDoSwap = TRUE
					
					#IF IS_DEBUG_BUILD
					PRINTLN("[GBCARJCK] - SORT_DELIVERED_VEHICLE_DATA iCount2 value ($", serverBD.sVehiclesDeliveredParticipantData[iCount2].fTotalValueOfVehiclesDelivered, ") > than iCount2-1 value ($", serverBD.sVehiclesDeliveredParticipantData[iCount2-1].fTotalValueOfVehiclesDelivered, ") - doing swap!")
					#ENDIF
//					
//				ELIF serverBD.sDeliveredVehicleData[iCount2].fVehicleValue = serverBD.sDeliveredVehicleData[iCount2-1].fVehicleValue 
//				
//					IF GET_NET_TIMER_DIFFERENCE(serverBD.sDeliveredVehicleData[iCount2].deliveryTime, serverBD.sDeliveredVehicleData[iCount2-1].deliveryTime) < 0
//						
//						bDoSwap = TRUE
//					
//						#IF IS_DEBUG_BUILD
//						PRINTLN("[GBCARJCK] - SORT_DELIVERED_VEHICLE_DATA iCount2 value = iCount-1 value. iCount delivery time is sooner than iCount-1 delivery time though, doing swap!")
//						#ENDIF
//					
//					ENDIF
//					
				ELSE
				
					#IF IS_DEBUG_BUILD
					PRINTLN("[GBCARJCK] - SORT_DELIVERED_VEHICLE_DATA not swapping iCount2 with icount2-1. icount2-1 is either more valuable or of the same value but delivered earlier.")
					PRINTLN("[GBCARJCK] - SORT_DELIVERED_VEHICLE_DATA ")
					#ENDIF
				
				ENDIF
				
				IF bDoSwap
					
					PARTICIPANT_INDEX partIdTemp = serverBd.sVehiclesDeliveredParticipantData[iCount2].participantId
//					INT iNumVehiclesTemp = serverBD.sVehiclesDeliveredParticipantData[iCount2].iNumberVehiclesDelivered
					FLOAT fValueTemp = serverBD.sVehiclesDeliveredParticipantData[iCount2].fTotalValueOfVehiclesDelivered
					TEXT_LABEL_63 tl63NameTemp =  serverBD.sVehiclesDeliveredParticipantData[iCount2].tl63Name
					
					serverBd.sVehiclesDeliveredParticipantData[iCount2].participantId = serverBd.sVehiclesDeliveredParticipantData[iCount2-1].participantId
//					serverBD.sVehiclesDeliveredParticipantData[iCount2].iNumberVehiclesDelivered = serverBD.sVehiclesDeliveredParticipantData[iCount2-1].iNumberVehiclesDelivered
					serverBD.sVehiclesDeliveredParticipantData[iCount2].fTotalValueOfVehiclesDelivered = serverBD.sVehiclesDeliveredParticipantData[iCount2-1].fTotalValueOfVehiclesDelivered
					serverBD.sVehiclesDeliveredParticipantData[iCount2].tl63Name = serverBD.sVehiclesDeliveredParticipantData[iCount2-1].tl63Name
					
					serverBd.sVehiclesDeliveredParticipantData[iCount2-1].participantId = partIdTemp
//					serverBD.sVehiclesDeliveredParticipantData[iCount2-1].iNumberVehiclesDelivered = iNumVehiclesTemp
					serverBD.sVehiclesDeliveredParticipantData[iCount2-1].fTotalValueOfVehiclesDelivered = fValueTemp
					serverBD.sVehiclesDeliveredParticipantData[iCount2-1].tl63Name = tl63NameTemp
					
					#IF IS_DEBUG_BUILD
					PRINTLN("[GBCARJCK] - SORT_DELIVERED_VEHICLE_DATA swapped. iCount2 partId = ", NATIVE_TO_INT(serverBD.sVehiclesDeliveredParticipantData[iCount2].participantId), ", iCount2-1 partId = ", NATIVE_TO_INT(serverBD.sVehiclesDeliveredParticipantData[iCount2-1].participantId))
//					PRINTLN("[GBCARJCK] - SORT_DELIVERED_VEHICLE_DATA swapped. iCount2 num vehs del = ", serverBD.sVehiclesDeliveredParticipantData[iCount2].iNumberVehiclesDelivered, ", iCount2-1 num vehs del = ", serverBD.sVehiclesDeliveredParticipantData[iCount2-1].iNumberVehiclesDelivered)
					PRINTLN("[GBCARJCK] - SORT_DELIVERED_VEHICLE_DATA swapped. iCount2 tl63Name = ", serverBD.sVehiclesDeliveredParticipantData[iCount2].tl63Name, ", iCount2-1 tl63Name = ", serverBD.sVehiclesDeliveredParticipantData[iCount2-1].tl63Name)
					PRINTLN("[GBCARJCK] - SORT_DELIVERED_VEHICLE_DATA swapped. iCount2 value = ", serverBD.sVehiclesDeliveredParticipantData[iCount2].fTotalValueOfVehiclesDelivered, ", iCount2-1 value = ", serverBD.sVehiclesDeliveredParticipantData[iCount2-1].fTotalValueOfVehiclesDelivered)
					PRINTLN("[GBCARJCK] - SORT_DELIVERED_VEHICLE_DATA ")
					#ENDIF
					
				ENDIF
				
			ENDIF
			
		ENDFOR
		
	ENDFOR
	
	serverBD.iTotalOrganisationScore = 0
	PRINTLN("[GBCARJCK] - SORT_DELIVERED_VEHICLE_DATA serverBD.iTotalOrganisationScore = 0 in prep for re-calculating total oragnisation score")
	
	INT iTemp
	REPEAT GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS() iCount
		iTemp = CEIL(serverBD.sVehiclesDeliveredParticipantData[iCount].fTotalValueOfVehiclesDelivered)
		IF iTemp < 0
			iTemp = 0
		ENDIF
		serverBD.iTotalOrganisationScore += iTemp
		PRINTLN("[GBCARJCK] - SORT_DELIVERED_VEHICLE_DATA CEIL(serverBD.sVehiclesDeliveredParticipantData[", iCount, "].fTotalValueOfVehiclesDelivered) = ", iTemp, ", serverBD.iTotalOrganisationScore = ", serverBD.iTotalOrganisationScore)
	ENDREPEAT
	
	IF serverBD.iTotalOrganisationScore < 0
		serverBD.iTotalOrganisationScore = 0
		PRINTLN("[GBCARJCK] - SORT_DELIVERED_VEHICLE_DATA final serverBD.iTotalOrganisationScore < 0, capping at ", serverBD.iTotalOrganisationScore)
	ENDIF
	
	PRINTLN("[GBCARJCK] - SORT_DELIVERED_VEHICLE_DATA final serverBD.iTotalOrganisationScore = ", serverBD.iTotalOrganisationScore)
	PRINTLN("[GBCARJCK] - SORT_DELIVERED_VEHICLE_DATA ")
	PRINTLN("[GBCARJCK] - SORT_DELIVERED_VEHICLE_DATA ")
	
ENDPROC

PROC MAINTAIN_LEADERBOARD()
	
	INT iCount
	
	REPEAT GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS() iCount
		g_GBLeaderboardStruct.challengeLbdStruct[iCount].playerID = INVALID_PLAYER_INDEX()
		g_GBLeaderboardStruct.challengeLbdStruct[iCount].iScore = 0
		g_GBLeaderboardStruct.challengeLbdStruct[iCount].iFinishTime = -1
	ENDREPEAT
	
	REPEAT GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS() iCount
//		PRINTLN("deebug 0 - iCount = ", iCount)
//		IF serverBD.sVehiclesDeliveredParticipantData[iCount].participantId != INVALID_PARTICIPANT_INDEX()
		IF serverBD.sVehiclesDeliveredParticipantData[iCount].fTotalValueOfVehiclesDelivered != (-1)
//			PRINTLN("deebug 1 - participantId != INVALID_PARTICIPANT_INDEX() - iCount = ", iCount)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(serverBD.sVehiclesDeliveredParticipantData[iCount].participantId)
				g_GBLeaderboardStruct.challengeLbdStruct[iCount].playerID = NETWORK_GET_PLAYER_INDEX(serverBD.sVehiclesDeliveredParticipantData[iCount].participantId)
				g_GBLeaderboardStruct.challengeLbdStruct[iCount].iScore = CEIL(serverBD.sVehiclesDeliveredParticipantData[iCount].fTotalValueOfVehiclesDelivered)
				g_GBLeaderboardStruct.challengeLbdStruct[iCount].iFinishTime = -1
//				PRINTLN("deebug 2 - playerID = ", NATIVE_TO_INT(g_GBLeaderboardStruct.challengeLbdStruct[iCount].playerID), " - iCount = ", iCount)
//				PRINTLN("deebug 2 - iScore = ", g_GBLeaderboardStruct.challengeLbdStruct[iCount].iScore, " - iCount = ", iCount)
//				PRINTLN("deebug 2 - iFinishTime = ", g_GBLeaderboardStruct.challengeLbdStruct[iCount].iFinishTime, " - iCount = ", iCount)
			ENDIF
		ENDIF
	ENDREPEAT
	
	g_GBLeaderboardStruct.dpadVariables.eDpadVarType = DPAD_VAR_CASH
	
	DRAW_CHALLENGE_DPAD_LBD(g_GBLeaderboardStruct.challengeLbdStruct, g_GBLeaderboardStruct.siDpadMovie, SUB_BOSS, g_GBLeaderboardStruct.dpadVariables, g_GBLeaderboardStruct.fmDpadStruct)		
	
ENDPROC

PROC DRAW_CLOSEST_DROP_OFF_CORONA()
	
	INT iR, iG, iB, iA
	FLOAT iRadius = GET_DROP_OFF_LOCATION_RADIUS(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iClosestCarjackingDropOffArea, GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iClosestCarjackingDropOffinArea)
	
	IF DOES_BLIP_EXIST(MPGlobalsAmbience.sMagnateGangBossData.dropOffBlipId[GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iClosestCarjackingDropOffArea][GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iClosestCarjackingDropOffinArea])
		
		GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)	
//		
//		PRINTLN("[GBCARJCK] - DRAW_CLOSEST_DROP_OFF_CORONA - calling DRAW_MARKER(MARKER_CYLINDER, ", MPGlobalsAmbience.sMagnateGangBossData.dropOffBlipId[GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iClosestCarjackingDropOffinArea], ", 
//						<<0,0,0>>,
//						<<0,0,0>>,
//						<<0.6, 0.6, 1.0 >>, ",
//						iR, ", ",
//						iG, ", ", 
//						iB, ", ", 
//						"100)")
//		
		VECTOR vCoords = GB_GET_CARJACKING_DROP_OFF_BLIP_COORDS(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iClosestCarjackingDropOffArea, GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iClosestCarjackingDropOffinArea)
		vCoords.z -= 0.7
		DRAW_MARKER(	MARKER_CYLINDER,
						vCoords,
						<<0,0,0>>,
						<<0,0,0>>,
						<<iRadius*1.8, iRadius*1.8, 2.5 >>,
						iR,
						iG,
						iB,
						150)
	ENDIF
						
ENDPROC

PROC ADD_PARTICIPANT_TO_DELIVERED_VEHICLES_DATA(PARTICIPANT_INDEX partId, TEXT_LABEL_63 tl63PartName, FLOAT fValue)
	
	INT i
	INT iSlot = -1
	
	REPEAT GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS() i
		IF serverBd.sVehiclesDeliveredParticipantData[i].participantId = partId
			IF iSlot = (-1)
				iSlot = i
				PRINTLN("[GBCARJCK] PROCESS_GB_SCRIPT_EVENT_ACQUIRE_VEHICLE_DELIVERED_VEHICLE - serverBd.sVehiclesDeliveredParticipantData[", i, "].participantId = partId - increment data this slot.")
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF iSlot = (-1)
		PRINTLN("[GBCARJCK] PROCESS_GB_SCRIPT_EVENT_ACQUIRE_VEHICLE_DELIVERED_VEHICLE - iSlot still = (-1), searching for slot with invalid participant index.")
		REPEAT GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS() i
			IF serverBd.sVehiclesDeliveredParticipantData[i].participantId = INVALID_PARTICIPANT_INDEX()
				IF iSlot = (-1)
					iSlot = i
					PRINTLN("[GBCARJCK] PROCESS_GB_SCRIPT_EVENT_ACQUIRE_VEHICLE_DELIVERED_VEHICLE - serverBd.sDeliveredVehicleData[", i, "].delivererPartId = INVALID_PARTICIPANT_INDEX() - slot is free.")
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	IF iSlot > (-1)
	AND iSlot < GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS()
		
		serverBD.sVehiclesDeliveredParticipantData[iSlot].participantId = partId
		serverBD.sVehiclesDeliveredParticipantData[iSlot].tl63Name = tl63PartName
//				INCREMENT_NUMBER_OF_VEHICLES_DELIVERED_IN_SLOT(iSlot)
		IF fValue > 0.0
			ADD_TO_TOTAL_VALUE_OF_VEHICLES_DELIVERED_IN_SLOT(iSlot, fValue)
		ELSE
			SET_TOTAL_VALUE_OF_VEHICLES_DELIVERED_IN_SLOT(iSlot, fValue)
		ENDIF
		
		PRINTLN("[GBCARJCK] ADD_PARTICIPANT_TO_DELIVERED_VEHICLES_DATA - fValue = ", fValue)
		PRINTLN("[GBCARJCK] ADD_PARTICIPANT_TO_DELIVERED_VEHICLES_DATA - serverBd.sVehiclesDeliveredParticipantData[", iSlot, "].participantId = ", NATIVE_TO_INT(serverBd.sVehiclesDeliveredParticipantData[iSlot].participantId))
//				PRINTLN("[GBCARJCK] PROCESS_GB_SCRIPT_EVENT_ACQUIRE_VEHICLE_DELIVERED_VEHICLE - serverBd.sVehiclesDeliveredParticipantData[", iSlot, "].iNumberVehiclesDelivered = ", serverBd.sVehiclesDeliveredParticipantData[iSlot].iNumberVehiclesDelivered)
		PRINTLN("[GBCARJCK] ADD_PARTICIPANT_TO_DELIVERED_VEHICLES_DATA - serverBd.sVehiclesDeliveredParticipantData[", iSlot, "].fTotalValueOfVehiclesDelivered = ", serverBd.sVehiclesDeliveredParticipantData[iSlot].fTotalValueOfVehiclesDelivered)
		PRINTLN("[GBCARJCK] ADD_PARTICIPANT_TO_DELIVERED_VEHICLES_DATA - serverBd.sVehiclesDeliveredParticipantData[", iSlot, "].tl63Name = ", serverBd.sVehiclesDeliveredParticipantData[iSlot].tl63Name)
		
		SORT_DELIVERED_VEHICLE_DATA() // Sort data now we have a new entry in the array.
	
	ENDIF
	
ENDPROC

PROC REMOVE_PARTICIPANT_FROM_DELIVERED_VEHICLES_DATA(PARTICIPANT_INDEX partId)
	
	INT i
	TEXT_LABEL_63 tl63Temp
	
	REPEAT GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS() i
		IF serverBd.sVehiclesDeliveredParticipantData[i].participantId = partId
			serverBd.sVehiclesDeliveredParticipantData[i].participantId = INVALID_PARTICIPANT_INDEX()
			serverBd.sVehiclesDeliveredParticipantData[i].fTotalValueOfVehiclesDelivered = (-1)
			serverBd.sVehiclesDeliveredParticipantData[i].tl63Name = tl63Temp
			PRINTLN("[GBCARJCK] REMOVE_PARTICIPANT_FROM_DELIVERED_VEHICLES_DATA - found participant to remove in slot ", i)
			PRINTLN("[GBCARJCK] REMOVE_PARTICIPANT_FROM_DELIVERED_VEHICLES_DATA - set serverBd.sVehiclesDeliveredParticipantData[", i, "].participantId = ", NATIVE_TO_INT(serverBd.sVehiclesDeliveredParticipantData[i].participantId))
			PRINTLN("[GBCARJCK] REMOVE_PARTICIPANT_FROM_DELIVERED_VEHICLES_DATA - set serverBd.sVehiclesDeliveredParticipantData[", i, "].fTotalValueOfVehiclesDelivered = ", serverBd.sVehiclesDeliveredParticipantData[i].fTotalValueOfVehiclesDelivered)
			PRINTLN("[GBCARJCK] REMOVE_PARTICIPANT_FROM_DELIVERED_VEHICLES_DATA - set serverBd.sVehiclesDeliveredParticipantData[", i, "].tl63Name = ", serverBd.sVehiclesDeliveredParticipantData[i].tl63Name)
		ENDIF
	ENDREPEAT
	
	SORT_DELIVERED_VEHICLE_DATA() // Sort data now we have removed an entry from the array.
	
ENDPROC

PROC PROCESS_GB_SCRIPT_EVENT_ACQUIRE_VEHICLE_DELIVERED_VEHICLE(INT iEventID)
	
	STRUCT_SCRIPT_EVENT_ACQUIRE_VEHICLE_DELIVERED_VEHICLE Event
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
			
			PRINTLN("[GBCARJCK] PROCESS_GB_SCRIPT_EVENT_ACQUIRE_VEHICLE_DELIVERED_VEHICLE - called...")
			
			PRINTLN("[GBCARJCK] PROCESS_GB_SCRIPT_EVENT_ACQUIRE_VEHICLE_DELIVERED_VEHICLE - delivererPartId = ", NATIVE_TO_INT(Event.sDeliveredVehicleData.delivererPartId))
			PRINTLN("[GBCARJCK] PROCESS_GB_SCRIPT_EVENT_ACQUIRE_VEHICLE_DELIVERED_VEHICLE - tl63DelivererName = ", Event.sDeliveredVehicleData.tl63DelivererName)
			PRINTLN("[GBCARJCK] PROCESS_GB_SCRIPT_EVENT_ACQUIRE_VEHICLE_DELIVERED_VEHICLE - fVehicleValue = ", Event.sDeliveredVehicleData.fVehicleValue)
			PRINTLN("[GBCARJCK] PROCESS_GB_SCRIPT_EVENT_ACQUIRE_VEHICLE_DELIVERED_VEHICLE - iDropOffArea = ", Event.sDeliveredVehicleData.iDropOffArea)
			PRINTLN("[GBCARJCK] PROCESS_GB_SCRIPT_EVENT_ACQUIRE_VEHICLE_DELIVERED_VEHICLE - iDropOff = ", Event.sDeliveredVehicleData.iDropOff)
			
			INT iNumVehiclesDeliveredtoDropOff = GET_NUM_VEHICLES_DELIVERED_TO_DROP_OFF(Event.sDeliveredVehicleData.iDropOffArea, Event.sDeliveredVehicleData.iDropOff)
			
			IF iNumVehiclesDeliveredtoDropOff < GET_CARJACKING_TUNEABLE_DROP_OFF_MAX_VEHICLES()
				IF IS_DROP_OFF_ACTIVE(Event.sDeliveredVehicleData.iDropOffArea, Event.sDeliveredVehicleData.iDropOff)	
					
					PRINTLN("[GBCARJCK] GET_NUM_VEHICLES_DELIVERED_TO_DROP_OFF = ", iNumVehiclesDeliveredtoDropOff, ", allowed delivery")
					
					ADD_PARTICIPANT_TO_DELIVERED_VEHICLES_DATA(Event.sDeliveredVehicleData.delivererPartId, Event.sDeliveredVehicleData.tl63DelivererName, Event.sDeliveredVehicleData.fVehicleValue)
					
					// PRINT_TICKER_WITH_PLAYER_NAME_WITH_HUD_COLOUR_AND_INT("CARJCK_DELTCK", Event.Details.FromPlayerIndex, HUD_COLOUR_WHITE, CEIL(Event.sDeliveredVehicleData.fVehicleValue))
					
					INCREMENT_NUM_VEHICLES_DELIVERED_TO_DROP_OFF(Event.sDeliveredVehicleData.iDropOffArea, Event.sDeliveredVehicleData.iDropOff)
					
					BROADCAST_GB_SCRIPT_EVENT_SERVER_RECEIVED_ACQUIRE_VEHICLE_DELIVERED_VEHICLE_EVENT(Event.Details.FromPlayerIndex, TRUE)
					
				ELSE
					
					PRINTLN("[GBCARJCK] IS_DROP_OFF_ACTIVE = FALSE, not allowed delivery")
					BROADCAST_GB_SCRIPT_EVENT_SERVER_RECEIVED_ACQUIRE_VEHICLE_DELIVERED_VEHICLE_EVENT(Event.Details.FromPlayerIndex, FALSE)
				
				ENDIF
			ELSE
				
				PRINTLN("[GBCARJCK] GET_NUM_VEHICLES_DELIVERED_TO_DROP_OFF = ", iNumVehiclesDeliveredtoDropOff, ", not allowed delivery")
				BROADCAST_GB_SCRIPT_EVENT_SERVER_RECEIVED_ACQUIRE_VEHICLE_DELIVERED_VEHICLE_EVENT(Event.Details.FromPlayerIndex, FALSE)
				
			ENDIF
			
		ELSE	
			SCRIPT_ASSERT("[GBCARJCK] PROCESS_GB_SCRIPT_EVENT_ACQUIRE_VEHICLE_DELIVERED_VEHICLE - could not retrieve data.")
		ENDIF
	
	ELSE
		
		PRINTLN("[GBCARJCK] PROCESS_GB_SCRIPT_EVENT_ACQUIRE_VEHICLE_DELIVERED_VEHICLE - I am not host, cannot process. Migration since event was sent to host?")
		
	ENDIF
	
ENDPROC

PROC PROCESS_GB_SCRIPT_EVENT_SERVER_RECEIVED_ACQUIRE_VEHICLE_DELIVERED_VEHICLE_EVENT(INT iEventID)

	STRUCT_SCRIPT_EVENT_SERVER_RECEIVED_ACQUIRE_VEHICLE_DELIVERED_VEHICLE_EVENT Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
		
		PRINTLN("[GBCARJCK] PROCESS_GB_SCRIPT_EVENT_SERVER_RECEIVED_ACQUIRE_VEHICLE_DELIVERED_VEHICLE_EVENT - called...")
		
		IF Event.bAllowedDropOff
			PRINTLN("[GBCARJCK] PROCESS_GB_SCRIPT_EVENT_SERVER_RECEIVED_ACQUIRE_VEHICLE_DELIVERED_VEHICLE_EVENT - Event.bAllowedDropOff = TRUE")
			SET_CLIENT_BIT0(eCLIENTBITSET0_ALLOWED_TO_DROP_OFF_VEHICLE) 
		ELSE
			PRINTLN("[GBCARJCK] PROCESS_GB_SCRIPT_EVENT_SERVER_RECEIVED_ACQUIRE_VEHICLE_DELIVERED_VEHICLE_EVENT - Event.bAllowedDropOff = FALSE")
			SET_CLIENT_BIT0(eCLIENTBITSET0_NOT_ALLOWED_TO_DROP_OFF_VEHICLE_DROP_FULL)
		ENDIF
		
	ELSE	
		SCRIPT_ASSERT("[GBCARJCK] PROCESS_GB_SCRIPT_EVENT_SERVER_RECEIVED_ACQUIRE_VEHICLE_DELIVERED_VEHICLE_EVENT - could not retrieve data.")
	ENDIF
	
ENDPROC

PROC MAINTAIN_UPDATING_SERVER_WITH_MY_DELIVERED_VEHICLE_DATA()
	
	SWITCH iUpdateServerMyDeliveredVehicleStage
		
		// Send event to server.
		CASE 0
			IF IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_REQUEST_DELIVER_VEHICLE_TO_DROP_OFF)
				SCRIPT_TIMER tempTimer
				serverDeliveredVehicleHandshakeTimer = tempTimer
				BROADCAST_GB_SCRIPT_EVENT_ACQUIRE_VEHICLE_DELIVERED_VEHICLE(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_HOST_OF_THIS_SCRIPT()), /*sMyDeliveredVehicleData.vehicleModel,*/ sMyDeliveredVehicleData.fVehicleValue, sMyDeliveredVehicleData.tl63DelivererName /*, sMyDeliveredVehicleData.deliveryTime*/, sMyDeliveredVehicleData.iDropOffArea, sMyDeliveredVehicleData.iDropOff)
				iUpdateServerMyDeliveredVehicleStage++
				PRINTLN("[GBCARJCK] MAINTAIN_UPDATING_SERVER_WITH_MY_DELIVERED_VEHICLE_DATA - iUpdateServerMyDeliveredVehicleStage = ", iUpdateServerMyDeliveredVehicleStage)
			ENDIF
		BREAK
		
		// Wait for server to say it has received my update, if we don;t get word back, resend.
		CASE 1
			IF IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_EXITED_DELIVERED_VEHICLE) 
			OR IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_NOT_ALLOWED_TO_DROP_OFF_VEHICLE_DROP_FULL)
				iUpdateServerMyDeliveredVehicleStage++ // My data is stored, done.
				PRINTLN("[GBCARJCK] MAINTAIN_UPDATING_SERVER_WITH_MY_DELIVERED_VEHICLE_DATA - received handshake - iUpdateServerMyDeliveredVehicleStage = ", iUpdateServerMyDeliveredVehicleStage)	
			ELSE
				IF HAS_NET_TIMER_STARTED(serverDeliveredVehicleHandshakeTimer)
					IF HAS_NET_TIMER_EXPIRED(serverDeliveredVehicleHandshakeTimer, 8000)
						iUpdateServerMyDeliveredVehicleStage = 0 // Resend data, it failed before. Possibly server migration?
						PRINTLN("[GBCARJCK] MAINTAIN_UPDATING_SERVER_WITH_MY_DELIVERED_VEHICLE_DATA - not received handshake in time - iUpdateServerMyDeliveredVehicleStage = ", iUpdateServerMyDeliveredVehicleStage)
					ENDIF
				ELSE
					START_NET_TIMER(serverDeliveredVehicleHandshakeTimer)
					PRINTLN("[GBCARJCK] MAINTAIN_UPDATING_SERVER_WITH_MY_DELIVERED_VEHICLE_DATA - started timer serverDeliveredVehicleHandshakeTimer")
				ENDIF
			ENDIF
		BREAK
		
		// Complete.
		CASE 2
			
		BREAK
		
	ENDSWITCH
	
ENDPROC

FUNC BOOL IS_SAFE_FOR_BIG_MESSAGE()	

	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND NOT IS_PAUSE_MENU_ACTIVE()
	AND NOT IS_CUSTOM_MENU_ON_SCREEN()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC INT GET_END_REASON_AS_TELEMTRY_TYPE(eEND_REASON eEndReason, BOOL bWon, BOOL bNobodyWon, BOOL bExternallyForcedEndOfMode)
	
	IF bExternallyForcedEndOfMode
		PRINTLN("[GBCARJCK] - GET_END_REASON_AS_TELEMTRY_TYPE - bExternallyForcedEndOfMode = TRUE, returning GB_TELEMETRY_END_FORCED")
		RETURN GB_TELEMETRY_END_FORCED
	ENDIF
	
	SWITCH eEndReason
		
		CASE eENDREASON_NO_BOSS_LEFT
			PRINTLN("[GBCARJCK] - [TELEMETRY] - GET_END_REASON_AS_TELEMTRY_TYPE - eEndReason = eENDREASON_NO_BOSS_LEFT, returning GB_TELEMETRY_END_BOSS_LEFT")
			RETURN GB_TELEMETRY_END_BOSS_LEFT	
		
		CASE eENDREASON_ALL_GOONS_LEFT	
			PRINTLN("[GBCARJCK] - [TELEMETRY] - GET_END_REASON_AS_TELEMTRY_TYPE - eEndReason = eENDREASON_ALL_GOONS_LEFT, returning GB_TELEMETRY_END_LOW_NUMBERS")
			RETURN GB_TELEMETRY_END_LOW_NUMBERS
		
		CASE eENDREASON_TIME_UP
			IF bWon	
				PRINTLN("[GBCARJCK] - [TELEMETRY] - GET_END_REASON_AS_TELEMTRY_TYPE - eEndReason = eENDREASON_TIME_UP and bWon = TRUE, returning GB_TELEMETRY_END_WON")
				RETURN GB_TELEMETRY_END_WON		
			ELIF bNobodyWon
				PRINTLN("[GBCARJCK] - [TELEMETRY] - GET_END_REASON_AS_TELEMTRY_TYPE - eEndReason = eENDREASON_TIME_UP and bNobodyWon = TRUE, returning GB_TELEMETRY_END_TIME_OUT")
				RETURN GB_TELEMETRY_END_TIME_OUT	
			ELSE
				PRINTLN("[GBCARJCK] - [TELEMETRY] - GET_END_REASON_AS_TELEMTRY_TYPE - eEndReason = eENDREASON_TIME_UP and bWon = FALSE and bNobodyWon = FALSE, returning GB_TELEMETRY_END_LOST")
				RETURN GB_TELEMETRY_END_LOST
			ENDIF
		BREAK
		
	ENDSWITCH
	
	PRINTLN("[GBCARJCK] - [TELEMETRY] - GET_END_REASON_AS_TELEMTRY_TYPE - all other telemetry type checks failed, must be leaving of own choice, returning GB_TELEMETRY_END_LEFT")
	RETURN GB_TELEMETRY_END_LEFT
	
ENDFUNC

PROC EXECUTE_COMMON_END_TELEMTRY(BOOL bIwon, BOOL bExternallyForcedEndOfMode)
	
	IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_EXECUTED_END_TELEMTRY)
		
		BOOL bNobodyWon = (serverBD.sVehiclesDeliveredParticipantData[0].fTotalValueOfVehiclesDelivered <= 0)
		INT iEndType = GET_END_REASON_AS_TELEMTRY_TYPE(GET_END_REASON(), bIWon, bNobodyWon, bExternallyForcedEndOfMode)
		
		PRINTLN("[GBCARJCK] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - bExternallyForcedEndOfMode = ", bExternallyForcedEndOfMode)
		PRINTLN("[GBCARJCK] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - bIWon = ", bIWon)
		PRINTLN("[GBCARJCK] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - bNobodyWon = ", bNobodyWon)
		PRINTLN("[GBCARJCK] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - iEndType = ", iEndType)
		
		GB_SET_COMMON_TELEMETRY_DATA_ON_END(bIWon, iEndType)
		PRINTLN("[GBCARJCK] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - called GB_SET_COMMON_TELEMETRY_DATA_ON_END(", bIWon,", ", iEndType, ")")
		
		SET_LOCAL_BIT0(eLOCALBITSET0_EXECUTED_END_TELEMTRY)
		
	ENDIF
	
ENDPROC

FUNC BOOL ELEGANTLY_HANDLED_SPEC_CAM()
	
	BOOL bComplete
	
	SWITCH iElegantSpecCamStage
		
		CASE 0
			
			IF GB_SHOULD_RUN_SPEC_CAM()
				iElegantSpecCamStage++
				PRINTLN("[GBCARJCK] - ELEGANTLY_HANDLED_SPEC_CAM - GB_SHOULD_RUN_SPEC_CAM() = TRUE, iElegantSpecCamStage = ", iElegantSpecCamStage)
			ELSE
				bComplete = TRUE
				PRINTLN("[GBCARJCK] - ELEGANTLY_HANDLED_SPEC_CAM - GB_SHOULD_RUN_SPEC_CAM() = FALSE, RETURNING TRUE")
			ENDIF
			
		BREAK
		
		CASE 1
			
			GB_SET_SHOULD_CLOSE_SPECTATE()
			iElegantSpecCamStage++
			PRINTLN("[GBCARJCK] - ELEGANTLY_HANDLED_SPEC_CAM - called GB_SET_SHOULD_CLOSE_SPECTATE() once, iElegantSpecCamStage = ", iElegantSpecCamStage)
			
		BREAK
		
		CASE 2
			
			IF NOT GB_SHOULD_RUN_SPEC_CAM()
				iElegantSpecCamStage++
				PRINTLN("[GBCARJCK] - ELEGANTLY_HANDLED_SPEC_CAM - GB_SHOULD_RUN_SPEC_CAM() = FALSE, iElegantSpecCamStage = ", iElegantSpecCamStage)
			ENDIF
			
		BREAK
		
		CASE 3
			
			bComplete = TRUE
			PRINTLN("[GBCARJCK] - ELEGANTLY_HANDLED_SPEC_CAM - complete, RETURNING TRUE")
			
		BREAK
		
	ENDSWITCH
	
	RETURN bComplete
	
ENDFUNC

PROC HANDLE_REWARDS()
	
	BOOL bDoPlacementShard
	BOOL bIWon
	
	IF GET_END_REASON() != eENDREASON_NO_REASON_YET
		
		IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_ELEGANTLY_HANDLED_SPEC_CAM)
			
			IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
				
				IF NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
				AND NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE()
				AND NOT GB_SHOULD_HIDE_GANG_BOSS_EVENT(FMMC_TYPE_GB_CHAL_CARJACKING)
				
					IF IS_SAFE_FOR_BIG_MESSAGE()
						
						// Ended for no boss left on script.
						IF GET_END_REASON() = eENDREASON_NO_BOSS_LEFT
						
							PRINTLN("[GBCARJCK] - no boss left on script, ending without giving rewards.")
							CLEAR_HELP()
//							SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_GENERIC, "GB_CHAL_OVER", "CARJCK_BSSLFT")
						
						// Ended for no goons left on script.
						ELIF GET_END_REASON() = eENDREASON_ALL_GOONS_LEFT
						
							PRINTLN("[GBCARJCK] - no goons left on script, ending without giving rewards.")
							CLEAR_HELP()
							SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_GENERIC, "GB_CHAL_OVER", "CARJCK_GNSLFT")
						
						// Ended for other reason than player not being present.
						ELSE
							
							// If nobody delivered a vehicle.
							IF serverBD.sVehiclesDeliveredParticipantData[0].fTotalValueOfVehiclesDelivered <= 0.0
								
								SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_GENERIC, "GB_CHAL_OVER", "CARJCK_NODEL1")
							
							// If we have a valid vehicle delivered or time ran out.
							ELSE
								
								IF GET_END_REASON() = eENDREASON_TIME_UP
									bDoPlacementShard = TRUE
								ENDIF
								
							ENDIF
							
						ENDIF
						
						IF bDoPlacementShard
							PRINTLN("[GBCARJCK] - bDoPlacementShard = TRUE.")
							STRING strBlank
							IF serverBD.sVehiclesDeliveredParticipantData[0].participantId = PARTICIPANT_ID()
								PRINTLN("[GBCARJCK] - local participant in 1st place.")
								bIWon = TRUE
								SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, CEIL(serverBD.sVehiclesDeliveredParticipantData[0].fTotalValueOfVehiclesDelivered), "CARJCK_WON", "GB_WINNER", DEFAULT)
							ELIF serverBD.sVehiclesDeliveredParticipantData[1].participantId = PARTICIPANT_ID()
								PRINTLN("[GBCARJCK] - local participant in 2nd place.")
//								SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_AND_INT(BIG_MESSAGE_GB_END_OF_JOB_OVER, CEIL(serverBD.sVehiclesDeliveredParticipantData[0].fTotalValueOfVehiclesDelivered), "CARJCK_LOST", serverBD.sVehiclesDeliveredParticipantData[0].tl63Name, "GB_CHAL_OVER", GET_PLAYER_HUD_COLOUR(PLAYER_ID()))
								SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "GB_CHAL_OVER", "CARJCK_LOST", serverBD.sVehiclesDeliveredParticipantData[0].tl63Name, GET_PLAYER_HUD_COLOUR(PLAYER_ID()), strBlank, CEIL(serverBD.sVehiclesDeliveredParticipantData[0].fTotalValueOfVehiclesDelivered))
							ELIF serverBD.sVehiclesDeliveredParticipantData[2].participantId = PARTICIPANT_ID()
								PRINTLN("[GBCARJCK] - local participant not in 3rd place.")
//								SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_AND_INT(BIG_MESSAGE_GB_END_OF_JOB_OVER, CEIL(serverBD.sVehiclesDeliveredParticipantData[0].fTotalValueOfVehiclesDelivered), "CARJCK_LOST", serverBD.sVehiclesDeliveredParticipantData[0].tl63Name, "GB_CHAL_OVER", GET_PLAYER_HUD_COLOUR(PLAYER_ID()))
								SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "GB_CHAL_OVER", "CARJCK_LOST", serverBD.sVehiclesDeliveredParticipantData[0].tl63Name, GET_PLAYER_HUD_COLOUR(PLAYER_ID()), strBlank, CEIL(serverBD.sVehiclesDeliveredParticipantData[0].fTotalValueOfVehiclesDelivered))
							ELSE
								PRINTLN("[GBCARJCK] - local participant not in 4th place.")
//								SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_AND_INT(BIG_MESSAGE_GB_END_OF_JOB_OVER, CEIL(serverBD.sVehiclesDeliveredParticipantData[0].fTotalValueOfVehiclesDelivered), "CARJCK_LOST", serverBD.sVehiclesDeliveredParticipantData[0].tl63Name, "GB_CHAL_OVER", GET_PLAYER_HUD_COLOUR(PLAYER_ID()))
								SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "GB_CHAL_OVER", "CARJCK_LOST", serverBD.sVehiclesDeliveredParticipantData[0].tl63Name, GET_PLAYER_HUD_COLOUR(PLAYER_ID()), strBlank, CEIL(serverBD.sVehiclesDeliveredParticipantData[0].fTotalValueOfVehiclesDelivered))
							ENDIF
						ENDIF
						
					ENDIF
					
				ENDIF
				
			ENDIF
			
			PLAYER_INDEX playerId = INVALID_PLAYER_INDEX()
			
			IF serverBD.sVehiclesDeliveredParticipantData[0].participantId != INVALID_PARTICIPANT_INDEX()
				IF NETWORK_IS_PARTICIPANT_ACTIVE(serverBD.sVehiclesDeliveredParticipantData[0].participantId)
					playerId = NETWORK_GET_PLAYER_INDEX(serverBD.sVehiclesDeliveredParticipantData[0].participantId)
				ENDIF
			ENDIF
			
			GANG_BOSS_MANAGE_WANTED_CLEAN_UP(TRUE, playerId)
			
			IF bIWon
				PRINTLN("[GBCARJCK] - bIWon = TRUE")
				FLOAT fCombinedScore = TO_FLOAT(serverBD.iTotalOrganisationScore)
				PRINTLN("[GBCARJCK] - fCombinedScore = TO_FLOAT(serverBD.iTotalOrganisationScore) = ", fCombinedScore)
				IF fCombinedScore > TO_FLOAT(GET_CARJACKING_BONUS_CASH_REWARD_MAX())
					PRINTLN("[GBCARJCK] - fCombinedScore > GET_CARJACKING_BONUS_CASH_REWARD_MAX()")
					fCombinedScore = TO_FLOAT(GET_CARJACKING_BONUS_CASH_REWARD_MAX())
					PRINTLN("[GBCARJCK] - fCombinedScore capped to GET_CARJACKING_BONUS_CASH_REWARD_MAX()")
				ENDIF
				fCombinedScore *= GET_CARJACKING_BONUS_CASH_REWARD_PERCENTAGE_MAX()
				PRINTLN("[GBCARJCK] - fCombinedScore * GET_CARJACKING_BONUS_CASH_REWARD_PERCENTAGE_MAX() = ", fCombinedScore)
				sGangBossManageRewardsData.iExtraCash = CEIL(fCombinedScore)
				PRINTLN("[GBCARJCK] - sGangBossManageRewardsData.iExtraCash = CEIL(fCombinedScore) = ", sGangBossManageRewardsData.iExtraCash)
			ENDIF
			
			GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_CHAL_CARJACKING, bIWon, sGangBossManageRewardsData)
			
			EXECUTE_COMMON_END_TELEMTRY(bIWon, FALSE)
			
			SET_LOCAL_BIT0(eLOCALBITSET0_ELEGANTLY_HANDLED_SPEC_CAM)
			
		ENDIF
		
	ENDIF
	
	IF ELEGANTLY_HANDLED_SPEC_CAM()
	AND IS_LOCAL_BIT0_SET(eLOCALBITSET0_ELEGANTLY_HANDLED_SPEC_CAM)
	AND GB_MAINTAIN_BOSS_END_UI(sEndUI)
		SET_CLIENT_BIT0(eCLIENTBITSET0_COMPLETED_REWARDS)
	ENDIF
	
ENDPROC

PROC DRAW_BOTTOM_RIGHT_HUD()
	
	INT iEventTime
	HUD_COLOURS eColour
	PLAYER_INDEX playerId[4]
	INT iCount
	
	IF iFocusParticipant = (-1)
		EXIT
	ENDIF
	
//	IF SHOULD_HIDE_THIS_AMBIENT_EVENT()
//		EXIT
//	ENDIF
	
	IF NOT MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
		EXIT
	ENDIF
	
	iEventTime =  (GET_CARJACKING_TUNEABLE_TIME_LIMIT() - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.ModeTimer))
	
	IF iEventTime < 0
		iEventTime = 0
	ENDIF
	
	IF iEventTime > NEARLY_FINISHED_TIME
		eColour = HUD_COLOUR_WHITE
	ELSE
		eColour = HUD_COLOUR_RED
	ENDIF
	
	REPEAT GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS() iCount
		IF serverBD.sVehiclesDeliveredParticipantData[iCount].participantId != INVALID_PARTICIPANT_INDEX()
			IF NETWORK_IS_PARTICIPANT_ACTIVE(serverBD.sVehiclesDeliveredParticipantData[iCount].participantId)
				playerId[iCount] = NETWORK_GET_PLAYER_INDEX(serverBD.sVehiclesDeliveredParticipantData[iCount].participantId)
			ENDIF
		ENDIF
	ENDREPEAT
	
	STRING strTemp
	
	BOTTOM_RIGHT_UI_1STCASH_2NDCASH_3RDCASH_4THCASH_CASH1_CASH2_TIMER(	playerId[0], playerId[1], playerId[2], playerId[3],
																		CEIL(serverBD.sVehiclesDeliveredParticipantData[0].fTotalValueOfVehiclesDelivered), 
																		CEIL(serverBD.sVehiclesDeliveredParticipantData[1].fTotalValueOfVehiclesDelivered), 
																		CEIL(serverBD.sVehiclesDeliveredParticipantData[2].fTotalValueOfVehiclesDelivered),
																		CEIL(serverBD.sVehiclesDeliveredParticipantData[3].fTotalValueOfVehiclesDelivered),
																		serverBD.iTotalOrganisationScore, CEIL(fCurrentVehicleValue), iEventTime, bForceRebuildNames, eColour, strTemp, "CARJCK_CSH1TTL", "CARJCK_CSH2TTL")
	
ENDPROC

FUNC INT GET_RANDOM_INACTIVE_DROP_OFF_IN_AREA(INT iDropOffArea)
	
	// Get random inactive drop off.
	INT iArrayOfInactiveDropOffs[(MAX_NUM_CARJACKING_DROP_OFFS_IN_AREA-MAX_NUM_CARJACKING_ACTIVE_DROP_OFF_IN_AREA)]
	INT iCount
	INT iNewDropOff
	INT iDropOff
	
	REPEAT MAX_NUM_CARJACKING_DROP_OFFS_IN_AREA iDropOff
		IF NOT IS_DROP_OFF_ACTIVE(iDropOffArea, iDropOff)
			IF NOT IS_DROP_OFF_FULL(iDropOffArea, iDropOff)
				iArrayOfInactiveDropOffs[iCount] = iDropOff
				iCount++
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Turn on new random inactive drop off.
	iNewDropOff = GET_RANDOM_INT_IN_RANGE(0, iCount)
	
	RETURN iArrayOfInactiveDropOffs[iNewDropOff]
	
ENDFUNC

PROC INITIALISE_DROP_OFFS()
	
	INT iDropOffArea
	
	REPEAT GET_CARJACKING_TUNEABLE_NUM_ACTIVE_DROP_OFF_AREAS() iDropOffArea
		
		IF GET_CARJACKING_TUNEABLE_NUM_DROP_OFF_IN_AREA() >= 1
			SET_DROP_OFF_ACTIVE(iDropOffArea, GET_RANDOM_INT_IN_RANGE(0, 2), TRUE)
		ENDIF
		
		IF GET_CARJACKING_TUNEABLE_NUM_DROP_OFF_IN_AREA() >= 2
			SET_DROP_OFF_ACTIVE(iDropOffArea, GET_RANDOM_INT_IN_RANGE(2, 4), TRUE)
		ENDIF
		
		IF GET_CARJACKING_TUNEABLE_NUM_DROP_OFF_IN_AREA() >= 3
			SET_DROP_OFF_ACTIVE(iDropOffArea, GET_RANDOM_INT_IN_RANGE(4, 6), TRUE)
		ENDIF
		
		IF GET_CARJACKING_TUNEABLE_NUM_DROP_OFF_IN_AREA() >= 4
			SET_DROP_OFF_ACTIVE(iDropOffArea, GET_RANDOM_INT_IN_RANGE(6, 8), TRUE)
		ENDIF
		
		IF GET_CARJACKING_TUNEABLE_NUM_DROP_OFF_IN_AREA() >= 5
			SET_DROP_OFF_ACTIVE(iDropOffArea, GET_RANDOM_INT_IN_RANGE(8, 10), TRUE)
		ENDIF
		
	ENDREPEAT
	
ENDPROC

PROC RESET_FULL_DROP_OFFS_IN_AREA(INT iDropOffArea)
	
	INT iDropOff
	
	REPEAT GET_CARJACKING_TUNEABLE_NUM_ACTIVE_DROP_OFF_AREAS() iDropOffArea
		REPEAT MAX_NUM_CARJACKING_DROP_OFFS_IN_AREA iDropOff
			IF NOT IS_DROP_OFF_ACTIVE(iDropOffArea, iDropOff)
				SET_DROP_OFF_FULL(iDropOffArea, iDropOff, FALSE)
			ENDIF
		ENDREPEAT
	ENDREPEAT
	
ENDPROC

PROC MAINTAIN_ACTIVE_DROP_OFFS()
	
	INT iDropOffArea, iDropOff, iNewDropOff, iNumFullDropOffsInArea
	
	REPEAT GET_CARJACKING_TUNEABLE_NUM_ACTIVE_DROP_OFF_AREAS() iDropOffArea
		
		// Reset to zero for each loop.
		iNumFullDropOffsInArea = 0
		
		REPEAT MAX_NUM_CARJACKING_DROP_OFFS_IN_AREA iDropOff
			
			IF IS_DROP_OFF_ACTIVE(iDropOffArea, iDropOff)
				
				// We have met our quota for the drop off
				IF GET_NUM_VEHICLES_DELIVERED_TO_DROP_OFF(iDropOffArea, iDropOff) >= GET_CARJACKING_TUNEABLE_DROP_OFF_MAX_VEHICLES()
					
					SET_DROP_OFF_FULL(iDropOffArea, iDropOff, TRUE)
					
					iNewDropOff = GET_RANDOM_INACTIVE_DROP_OFF_IN_AREA(iDropOffArea)
					SET_DROP_OFF_ACTIVE(iDropOffArea, iNewDropOff, TRUE)
					SET_NUM_VEHICLES_DELIVERED_TO_DROP_OFF(iDropOffArea, iNewDropOff, 0)
					
					// Turn off drop off that was just filled.
					SET_DROP_OFF_ACTIVE(iDropOffArea, iDropOff, FALSE)
					SET_NUM_VEHICLES_DELIVERED_TO_DROP_OFF(iDropOffArea, iDropOff, 0)
					
				ENDIF
				
			ENDIF
			
			// Count number of full drop offs.
			IF IS_DROP_OFF_FULL(iDropOffArea, iDropOff)
				iNumFullDropOffsInArea++
			ENDIF
			
			// If we have used up our pool of drop off areas, reset them all, apartform the currently active ones, and start using the pool again.
			IF iNumFullDropOffsInArea >= (MAX_NUM_CARJACKING_DROP_OFFS_IN_AREA - GET_CARJACKING_TUNEABLE_NUM_DROP_OFF_IN_AREA())
				RESET_FULL_DROP_OFFS_IN_AREA(iDropOffArea)
			ENDIF
					
		ENDREPEAT
	ENDREPEAT
	
ENDPROC

FUNC BOOL IS_PLAYER_IN_DROP_OFF_IN_VALID_VEHICLE_ON_GROUND_AT_CORRECT_SPEED()
	
	INT iDropOffArea, iDropOff
	FLOAT fRadius
	
	IF iFocusParticipant > (-1)
		IF NOT IS_ENTITY_IN_AIR(currentVehicle)
			IF GET_ENTITY_SPEED(currentVehicle) < DELIVER_ACCEPT_SPEED
				REPEAT GET_CARJACKING_TUNEABLE_NUM_ACTIVE_DROP_OFF_AREAS() iDropOffArea
					REPEAT MAX_NUM_CARJACKING_DROP_OFFS_IN_AREA iDropOff
						IF IS_DROP_OFF_ACTIVE(iDropOffArea, iDropOff)
							fRadius = GET_DROP_OFF_LOCATION_RADIUS(iDropOffArea, iDropOff)
//							IF IS_ENTITY_IN_ANGLED_AREA(sPlayer[sParticipant[iFocusParticipant].iPlayerId].playerPedId, GET_DROP_OFF_LOCATE_MIN_COORDS(iDropOff, iLocate), GET_DROP_OFF_LOCATE_MAX_COORDS(iDropOff, iLocate), GET_DROP_OFF_LOCATE_WIDTH(iDropOff, iLocate))
							IF IS_ENTITY_AT_COORD(sPlayer[sParticipant[iFocusParticipant].iPlayerId].playerPedId, GB_GET_CARJACKING_DROP_OFF_BLIP_COORDS(iDropOffArea, iDropOff), << fRadius*0.8, fRadius*0.8, 3.5 >>, FALSE)
								RETURN TRUE
							ENDIF
						ENDIF
					ENDREPEAT
				ENDREPEAT
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC CALCULATE_CLOSEST_DROP_OFF_AREA()
	
	INT iDropOffArea
	INT iNearestDropOffArea
	FLOAT fNearestDropOffAreaDistance = 999999.0
	FLOAT fMyDistanceFromDropOffArea
	
	REPEAT GET_CARJACKING_TUNEABLE_NUM_ACTIVE_DROP_OFF_AREAS() iDropOffArea
		
		fMyDistanceFromDropOffArea = GET_DISTANCE_BETWEEN_COORDS(vFocusParticipantCoords, GB_GET_CARJACKING_DROP_OFF_BLIP_COORDS(iDropOffArea, 0))
		
		IF fMyDistanceFromDropOffArea < fNearestDropOffAreaDistance
			iNearestDropOffArea = iDropOffArea
			fNearestDropOffAreaDistance = fMyDistanceFromDropOffArea
		ENDIF
		
	ENDREPEAT
	
	GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iClosestCarjackingDropOffArea = iNearestDropOffArea
	
ENDPROC

PROC CALCULATE_CLOSEST_DROP_OFF_IN_CLOSEST_DROP_OFF_AREA()
	
	INT iDropoff
	INT iNearestDropOff
	FLOAT fNearestDropOffDistance = 999999.0
	FLOAT fMyDistanceFromDropOff
	
	REPEAT MAX_NUM_CARJACKING_DROP_OFFS_IN_AREA iDropOff
		
		IF IS_DROP_OFF_ACTIVE(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iClosestCarjackingDropOffArea, iDropOff)
			
			fMyDistanceFromDropOff = GET_DISTANCE_BETWEEN_COORDS(vFocusParticipantCoords, GB_GET_CARJACKING_DROP_OFF_BLIP_COORDS(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iClosestCarjackingDropOffArea, iDropOff))
			
			IF fMyDistanceFromDropOff < fNearestDropOffDistance
				iNearestDropOff = iDropOff
				fNearestDropOffDistance = fMyDistanceFromDropOff
			ENDIF
			
		ENDIF
		
	ENDREPEAT
	
	GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iClosestCarjackingDropOffinArea = iNearestDropOff
	
ENDPROC

PROC DRAW_DROP_OFF_BLIPS()
	
	INT iDropoffArea, iDropOff
	
	REPEAT GET_CARJACKING_TUNEABLE_NUM_ACTIVE_DROP_OFF_AREAS() iDropoffArea
		REPEAT MAX_NUM_CARJACKING_DROP_OFFS_IN_AREA iDropOff
			IF IS_DROP_OFF_ACTIVE(iDropoffArea, iDropOff)
				IF NOT DOES_BLIP_EXIST(MPGlobalsAmbience.sMagnateGangBossData.dropOffBlipId[iDropoffArea][iDropOff])
					MPGlobalsAmbience.sMagnateGangBossData.dropOffBlipId[iDropoffArea][iDropOff] = ADD_BLIP_FOR_COORD(GB_GET_CARJACKING_DROP_OFF_BLIP_COORDS(iDropoffArea, iDropoff))
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(MPGlobalsAmbience.sMagnateGangBossData.dropOffBlipId[iDropoffArea][iDropOff])
					REMOVE_BLIP(MPGlobalsAmbience.sMagnateGangBossData.dropOffBlipId[iDropoffArea][iDropOff])
				ENDIF
			ENDIF
		ENDREPEAT
	ENDREPEAT
	
ENDPROC

PROC REMOVE_DROP_OFF_BLIPS()
	
	INT iDropoffArea, iDropOff
	
	REPEAT GET_CARJACKING_TUNEABLE_NUM_ACTIVE_DROP_OFF_AREAS() iDropoffArea
		REPEAT MAX_NUM_CARJACKING_DROP_OFFS_IN_AREA iDropOff
			IF DOES_BLIP_EXIST(MPGlobalsAmbience.sMagnateGangBossData.dropOffBlipId[iDropoffArea][iDropOff])
				REMOVE_BLIP(MPGlobalsAmbience.sMagnateGangBossData.dropOffBlipId[iDropoffArea][iDropOff])
			ENDIF
		ENDREPEAT
	ENDREPEAT
	
	// Remove radius area blip.
	IF DOES_BLIP_EXIST(MPGlobalsAmbience.sMagnateGangBossData.closestDropOffRadiusBlipId)
		REMOVE_BLIP(MPGlobalsAmbience.sMagnateGangBossData.closestDropOffRadiusBlipId)
	ENDIF
		
	iCurrentGpsDropOffArea = -1
	iCurrentGpsDropOff = -1
	
ENDPROC

PROC MAINTAIN_DROP_OFF_GPS_AND_RADIUS_BLIP()
	
	INT iDropoffArea, iDropOff
		
	// If the current gps drop off does not equal the current closest drop off, update blip routes.
	IF iCurrentGpsDropOffArea != GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iClosestCarjackingDropOffArea
	OR iCurrentGpsDropOff != GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iClosestCarjackingDropOffinArea
		
		PRINTLN("[GBCARJCK] - MAINTAIN_DROP_OFF_GPS_AND_RADIUS_BLIP - iCurrentGpsDropOffArea = ", iCurrentGpsDropOffArea)
		PRINTLN("[GBCARJCK] - MAINTAIN_DROP_OFF_GPS_AND_RADIUS_BLIP - iCurrentGpsDropOffArea = ", GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iClosestCarjackingDropOffArea)
		PRINTLN("[GBCARJCK] - MAINTAIN_DROP_OFF_GPS_AND_RADIUS_BLIP - iCurrentGpsDropOff = ", iCurrentGpsDropOff)
		PRINTLN("[GBCARJCK] - MAINTAIN_DROP_OFF_GPS_AND_RADIUS_BLIP - GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iClosestCarjackingDropOffinArea = ", GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iClosestCarjackingDropOffinArea)
		
		// Clear blip routes.
		REPEAT GET_CARJACKING_TUNEABLE_NUM_ACTIVE_DROP_OFF_AREAS() iDropoffArea
			REPEAT MAX_NUM_CARJACKING_DROP_OFFS_IN_AREA iDropOff
				IF DOES_BLIP_EXIST(MPGlobalsAmbience.sMagnateGangBossData.dropOffBlipId[iDropoffArea][iDropOff])
					IF iDropOff != GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iClosestCarjackingDropOffinArea
						SET_BLIP_ROUTE(MPGlobalsAmbience.sMagnateGangBossData.dropOffBlipId[iDropoffArea][iDropOff], FALSE)
						PRINTLN("[GBCARJCK] - MAINTAIN_DROP_OFF_GPS_AND_RADIUS_BLIP - cleared blip route to drop off area ", iDropoffArea, ", iDropOff ", iDropOff)
					ENDIF
				ENDIF
			ENDREPEAT
		ENDREPEAT
		
		// Remove radius area blip.
		IF DOES_BLIP_EXIST(MPGlobalsAmbience.sMagnateGangBossData.closestDropOffRadiusBlipId)
			REMOVE_BLIP(MPGlobalsAmbience.sMagnateGangBossData.closestDropOffRadiusBlipId)
		ENDIF
		
		// Set gps to closest drop off.
		IF DOES_BLIP_EXIST(MPGlobalsAmbience.sMagnateGangBossData.dropOffBlipId[GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iClosestCarjackingDropOffArea][GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iClosestCarjackingDropOffinArea])
			SET_BLIP_ROUTE(MPGlobalsAmbience.sMagnateGangBossData.dropOffBlipId[GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iClosestCarjackingDropOffArea][GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iClosestCarjackingDropOffinArea], TRUE)
			PRINTLN("[GBCARJCK] - MAINTAIN_DROP_OFF_GPS_AND_RADIUS_BLIP - set blip route to closest drop off area ", GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iClosestCarjackingDropOffArea, ", drop off ", GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iClosestCarjackingDropOffinArea)
		ENDIF
		
		// Add radius area blip.
		IF NOT DOES_BLIP_EXIST(MPGlobalsAmbience.sMagnateGangBossData.closestDropOffRadiusBlipId)
			FLOAT fRadius = GET_DROP_OFF_LOCATION_RADIUS(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iClosestCarjackingDropOffArea, GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iClosestCarjackingDropOffinArea)
			fRadius *= 1.2
			MPGlobalsAmbience.sMagnateGangBossData.closestDropOffRadiusBlipId = ADD_BLIP_FOR_RADIUS(GB_GET_CARJACKING_DROP_OFF_BLIP_COORDS(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iClosestCarjackingDropOffArea, GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iClosestCarjackingDropOffinArea), fRadius)
			SET_BLIP_COLOUR_FROM_HUD_COLOUR(MPGlobalsAmbience.sMagnateGangBossData.closestDropOffRadiusBlipId, HUD_COLOUR_YELLOW)
			SET_BLIP_ALPHA(MPGlobalsAmbience.sMagnateGangBossData.closestDropOffRadiusBlipId, g_sMPTunables.iblip_area_alpha)
			PRINTLN("[GBCARJCK] - MAINTAIN_DROP_OFF_GPS_AND_RADIUS_BLIP - added radius blip for closest drop off area ", GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iClosestCarjackingDropOffArea, ", drop off ", GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iClosestCarjackingDropOffinArea)
		ENDIF
		
		// Save current closest drop off.
		iCurrentGpsDropOffArea = GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iClosestCarjackingDropOffArea
		iCurrentGpsDropOff = GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iClosestCarjackingDropOffinArea
		PRINTLN("[GBCARJCK] - MAINTAIN_DROP_OFF_GPS_AND_RADIUS_BLIP - set iCurrentGpsDropOffArea = GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iClosestCarjackingDropOffinArea = ", iCurrentGpsDropOffArea)
		PRINTLN("[GBCARJCK] - MAINTAIN_DROP_OFF_GPS_AND_RADIUS_BLIP - set iCurrentGpsDropOff = GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iClosestCarjackingDropOffinArea = ", iCurrentGpsDropOff)
		
	ENDIF
	
ENDPROC

PROC MAINTAIN_DROP_BLIPS_ALPHA()
	
	INT iDropOff
	
	REPEAT MAX_NUM_CARJACKING_DROP_OFFS_IN_AREA iDropOff
		IF DOES_BLIP_EXIST(MPGlobalsAmbience.sMagnateGangBossData.dropOffBlipId[iBlipAlphaDropOffAreaCount][iDropOff])
			IF IS_PAUSE_MENU_ACTIVE()
				SET_BLIP_ALPHA(MPGlobalsAmbience.sMagnateGangBossData.dropOffBlipId[iBlipAlphaDropOffAreaCount][iDropOff], 255)
			ELSE
				SET_BLIP_ALPHA_BASED_ON_DIST_FROM_PLAYER(MPGlobalsAmbience.sMagnateGangBossData.dropOffBlipId[iBlipAlphaDropOffAreaCount][iDropOff], 25)
			ENDIF
		ENDIF
	ENDREPEAT
	
	iBlipAlphaDropOffAreaCount++
	
	IF iBlipAlphaDropOffAreaCount >= (GET_CARJACKING_TUNEABLE_NUM_ACTIVE_DROP_OFF_AREAS() - 1)
		iBlipAlphaDropOffAreaCount = 0
	ENDIF
	
ENDPROC

PROC MAINTAIN_CAR_MOD_HELP()
	
	SHOP_NAME_ENUM eShop
	
	IF iFocusParticipant = PARTICIPANT_ID_TO_INT()
		
		IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_CAR_MOD_HELP_TRIGGERED)
			
			// If nearby an available mod shop.
			eShop = GET_CLOSEST_SHOP_OF_TYPE(vFocusParticipantCoords, SHOP_TYPE_CARMOD, 100, TRUE)
			
			IF eShop > EMPTY_SHOP
				IF GET_DISTANCE_BETWEEN_PLAYER_AND_SHOP(eShop) <= CAR_MOD_HELP_DISTANCE
					PRINTLN("[GBCARJCK] MAINTAIN_CAR_MOD_HELP - GET_DISTANCE_BETWEEN_PLAYER_AND_SHOP(GET_DISTANCE_BETWEEN_PLAYER_AND_SHOP()) <= ", CAR_MOD_HELP_DISTANCE)
					SET_LOCAL_BIT0(eLOCALBITSET0_CAR_MOD_HELP_TRIGGERED)
				ENDIF
			ENDIF
			
			// If in a car that was modded before the mode started.
			IF IS_LOCAL_BIT0_SET(eLOCALBITSET0_FOCUS_PLAYER_CURRENT_VEHICLE_IS_DRIVEABLE)
				IF DECOR_IS_REGISTERED_AS_TYPE("Veh_Modded_By_Player", DECOR_TYPE_INT)
					IF DECOR_EXIST_ON(currentVehicle, "Veh_Modded_By_Player")
						PRINTLN("[GBCARJCK] MAINTAIN_CAR_MOD_HELP - DECOR_EXIST_ON(currentVehicle, 'Veh_Modded_By_Player') = TRUE ")
						SET_LOCAL_BIT0(eLOCALBITSET0_CAR_MOD_HELP_TRIGGERED)
					ENDIF
				ENDIF
			ENDIF
			
		ELSE
			
			// Display help at first opportunity.
			IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_PRINTED_CAR_MOD_HELP)
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					PRINT_HELP("CARJCK_MODHLP")
					GB_SET_GANG_BOSS_HELP_BACKGROUND()
					SET_LOCAL_BIT0(eLOCALBITSET0_PRINTED_CAR_MOD_HELP)
				ENDIF
			ENDIF
			
		ENDIF
		
	ENDIF
	
ENDPROC

PROC MAINTAIN_COMBINED_TOTAL_HELP()
	
	IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_COMBINED_TOTAL_HELP_TRIGGERED)
		
		INT iCount
		INT iTotalEntries
		
		REPEAT GB_MAX_GANG_SIZE_INCLUDING_BOSS_EXEC iCount
			IF serverBD.sVehiclesDeliveredParticipantData[iCount].fTotalValueOfVehiclesDelivered > 0
				iTotalEntries++
			ENDIF
		ENDREPEAT
		
		IF iTotalEntries > 1
			SET_LOCAL_BIT0(eLOCALBITSET0_COMBINED_TOTAL_HELP_TRIGGERED)
		ENDIF
	
	ELIF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_PRINTED_COMBINED_TOTAL_HELP)
		
		IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_PRINTED_COMBINED_TOTAL_HELP)
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				PRINT_HELP("CARJCK_EXPHP4")
				GB_SET_GANG_BOSS_HELP_BACKGROUND()
				SET_LOCAL_BIT0(eLOCALBITSET0_PRINTED_COMBINED_TOTAL_HELP)
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

PROC CLIENT_PROCESSING()
	
	#IF IS_DEBUG_BUILD
	MAINTAIN_J_SKIPS()
	#ENDIF
	
	SWITCH GET_CARJACKING_STATE()
	
		CASE eCARJACKING_STATE_INIT
			
		BREAK
		
		CASE eCARJACKING_STATE_RUN
			
			// IF GET_TARGET_VEHICLE_CLASS() != GET_INVALID_TARGET_VEHICLE_CLASS() // We know the server has updated the target vehicle class to a valid type, we can now process the main gameplay.
				
				// Do common setup.
				IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_CALLED_COMMON_SETUP)
					GB_COMMON_BOSS_MISSION_SETUP(FMMC_TYPE_GB_CHAL_CARJACKING)
					SET_LOCAL_BIT0(eLOCALBITSET0_CALLED_COMMON_SETUP)
				ELSE
					IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_CALLED_SAFE_FOR_SPEC_CAM_SETUP)
						IF HAS_NET_TIMER_EXPIRED(serverBD.modeTimer, TRACK_GOON_DELAY)
							GB_SET_ITS_SAFE_FOR_SPEC_CAM()
							SET_LOCAL_BIT0(eLOCALBITSET0_CALLED_SAFE_FOR_SPEC_CAM_SETUP)
						ENDIF
					ENDIF
				ENDIF
				
				// Calculate the closest drop off so we know which one to check in our delivery tracking.
				CALCULATE_CLOSEST_DROP_OFF_AREA()
				CALCULATE_CLOSEST_DROP_OFF_IN_CLOSEST_DROP_OFF_AREA()
				
				// Handle sending my delivered vehicle data to the server so it can be stored out.
				MAINTAIN_UPDATING_SERVER_WITH_MY_DELIVERED_VEHICLE_DATA()
				
				// Help explaining when a drop changes.
				MAINTAIN_DROP_OFF_CHANGE_HELP()
				
				// Fade yellow blips when far away from player.
				MAINTAIN_DROP_BLIPS_ALPHA()
				
				IF NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
				AND NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE()
				AND NOT GB_SHOULD_HIDE_GANG_BOSS_EVENT(FMMC_TYPE_GB_CHAL_CARJACKING)
					
					DRAW_BOTTOM_RIGHT_HUD()
					MAINTAIN_LEADERBOARD()
					DRAW_CLOSEST_DROP_OFF_CORONA()
					
					IF GET_END_REASON() = eENDREASON_NO_REASON_YET
						
						IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
							
							IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_SETUP_JOB_START_BIG_MESSAGE)
								
								// Setup start of job bug message.
								SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_START_OF_JOB, "GB_BIGM_CJ_T", "CARJCK_GETVEHb")
								GB_SET_COMMON_TELEMETRY_DATA_ON_START()
								PRINTLN("[GBCARJCK] - [TELEMETRY] - called GB_SET_COMMON_TELEMETRY_DATA_ON_START()")
								SET_LOCAL_BIT0(eLOCALBITSET0_SETUP_JOB_START_BIG_MESSAGE)
								
							ELSE
							
								IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_SHOWN_JOB_START_BIG_MESSAGE)
									
									// Wait for start of job big message to show.
									IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GB_START_OF_JOB)
										SET_LOCAL_BIT0(eLOCALBITSET0_SHOWN_JOB_START_BIG_MESSAGE)
									ENDIF
									
								ELSE
								
									IF NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GB_START_OF_JOB)
										
										// Once start of job big message has been displayed, show help explaining mode.
										IF iFocusParticipant = PARTICIPANT_ID_TO_INT()
											IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_PRINTED_EXPLNATORY_HELP_1) // This does not show until in a valid vehicle, since it has a blip in it that only appars once in a valid vehicle.
												IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
													PRINT_HELP_NO_SOUND("CARJCK_EXPHP1")
													GB_SET_GANG_BOSS_HELP_BACKGROUND()
													SET_LOCAL_BIT0(eLOCALBITSET0_PRINTED_EXPLNATORY_HELP_1)
												ENDIF
											ELSE
												IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_PRINTED_EXPLNATORY_HELP_2) // Explain about each vehicle value being added to your score and win conditions.
													IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
														PRINT_HELP_NO_SOUND("CARJCK_EXPHP2")
														GB_SET_GANG_BOSS_HELP_BACKGROUND()
														SET_LOCAL_BIT0(eLOCALBITSET0_PRINTED_EXPLNATORY_HELP_2)
													ENDIF
												ELSE
													IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_PRINTED_EXPLNATORY_HELP_3) // Explain about vehicle damage reducing delivery value.
														IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
															PRINT_HELP_NO_SOUND("CARJCK_EXPHP3")
															GB_SET_GANG_BOSS_HELP_BACKGROUND()
															SET_LOCAL_BIT0(eLOCALBITSET0_PRINTED_EXPLNATORY_HELP_3)
														ENDIF
													ELSE
														MAINTAIN_CAR_MOD_HELP() // Do car mod help after we've explained the mode rules.
														MAINTAIN_COMBINED_TOTAL_HELP() // Do combined total help once there are >= 2 entries on the bottom right leaderboard.
													ENDIF
												ENDIF
											ENDIF
										ENDIF
										
									ENDIF
									
								ENDIF
							ENDIF
							
						ENDIF
						
//						// Set boss critical.
//						IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_HANDLED_SETTING_PLAYER_CRITICAL)
//							IF sParticipant[NATIVE_TO_INT(PARTICIPANT_ID())].bIsBoss
//								GB_SET_LOCAL_PLAYER_CRITICAL_TO_JOB(TRUE)
//							ENDIF
//							SET_LOCAL_BIT0(eLOCALBITSET0_HANDLED_SETTING_PLAYER_CRITICAL)
//						ENDIF
						
						// Handle client delivery objective.
//						IF NOT sParticipant[NATIVE_TO_INT(PARTICIPANT_ID())].bIsBoss
							
							// Display different objectives based on if player is in target vehicle class as a passenger or driver, or not in a target vehicle at all.
							// Track if player delivers the vehicle to his boss.
							IF IS_LOCAL_BIT0_SET(eLOCALBITSET0_IS_FOCUS_PLAYER_IN_VALID_VEHICLE_AS_DRIVER)
								
								IF NOT IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_REQUEST_DELIVER_VEHICLE_TO_DROP_OFF)
									IF IS_PLAYER_IN_DROP_OFF_IN_VALID_VEHICLE_ON_GROUND_AT_CORRECT_SPEED()
										
										REMOVE_DROP_OFF_BLIPS()
										
										IF iFocusParticipant = PARTICIPANT_ID_TO_INT()
											IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
												IF SAVE_MY_DELIVERED_VEHICLE_DATA()
													
													SET_CLIENT_BIT0(eCLIENTBITSET0_REQUEST_DELIVER_VEHICLE_TO_DROP_OFF)
													PRINTLN("[GBCARJCK] - I have delivered a vehicle to the boss. Set eCLIENTBITSET0_REQUEST_DELIVER_VEHICLE_TO_DROP_OFF.")
													
												ENDIF
											ENDIF
										ENDIF
										
									ELSE
										
										IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
											
											DRAW_DROP_OFF_BLIPS()
											
											// Update the gps so it leads to the closest drop off.
											MAINTAIN_DROP_OFF_GPS_AND_RADIUS_BLIP()
				
											Print_Objective_Text("CARJCK_DELVEH")
											
										ELSE
											REMOVE_DROP_OFF_BLIPS()
											Clear_Any_Objective_Text_From_This_Script()
										ENDIF
										
									ENDIF
								ENDIF
								
							ELIF IS_LOCAL_BIT0_SET(eLOCALBITSET0_FOCUS_PLAYER_IS_IN_OWN_PERSONAL_VEHICLE)
								
								REMOVE_DROP_OFF_BLIPS()
								
								IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
									// Print_Objective_Text("CARJCK_EXTVEH")
									Print_Objective_Text("CARJCK_GETVEH")
								ELSE
									Clear_Any_Objective_Text_From_This_Script()
								ENDIF
								
								IF iFocusParticipant = PARTICIPANT_ID_TO_INT()
									IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_PRINTED_PERSONAL_VEHICLE_HELP)
										IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
											IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
												IF IS_LOCAL_BIT0_SET(eLOCALBITSET0_PRINTED_EXPLNATORY_HELP_3)
													PRINT_HELP_NO_SOUND("CARJCK_PERHLP")
													GB_SET_GANG_BOSS_HELP_BACKGROUND()
													SET_LOCAL_BIT0(eLOCALBITSET0_PRINTED_PERSONAL_VEHICLE_HELP)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
							ELIF IS_LOCAL_BIT0_SET(eLOCALBITSET0_FOCUS_PLAYER_IS_IN_A_PEGASUS_VEHICLE)
							
								REMOVE_DROP_OFF_BLIPS()
								
								IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
									// Print_Objective_Text("CARJCK_EXTVEH")
									Print_Objective_Text("CARJCK_GETVEH")
								ELSE
									Clear_Any_Objective_Text_From_This_Script()
								ENDIF
								
								IF iFocusParticipant = PARTICIPANT_ID_TO_INT()
									IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_PRINTED_PEGASUS_VEHICLE_HELP)
										IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
											IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
												IF IS_LOCAL_BIT0_SET(eLOCALBITSET0_PRINTED_EXPLNATORY_HELP_3)
													PRINT_HELP_NO_SOUND("CARJCK_PEGHLP")
													GB_SET_GANG_BOSS_HELP_BACKGROUND()
													SET_LOCAL_BIT0(eLOCALBITSET0_PRINTED_PEGASUS_VEHICLE_HELP)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
							ELIF IS_LOCAL_BIT0_SET(eLOCALBITSET0_FOCUS_PLAYER_IS_IN_AN_ARMOURED_LIMO_VEHICLE)
								
								REMOVE_DROP_OFF_BLIPS()
								
								IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
									// Print_Objective_Text("CARJCK_EXTVEH")
									Print_Objective_Text("CARJCK_GETVEH")
								ELSE
									Clear_Any_Objective_Text_From_This_Script()
								ENDIF
								
								IF iFocusParticipant = PARTICIPANT_ID_TO_INT()
									IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_PRINTED_VEHICLE_PASSENGER_HELP)
										IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
											IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
												IF IS_LOCAL_BIT0_SET(eLOCALBITSET0_PRINTED_EXPLNATORY_HELP_3)
													PRINT_HELP_NO_SOUND("CARJCK_LIMHLP")
													GB_SET_GANG_BOSS_HELP_BACKGROUND()
													SET_LOCAL_BIT0(eLOCALBITSET0_PRINTED_VEHICLE_PASSENGER_HELP)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
							ELIF IS_LOCAL_BIT0_SET(eLOCALBITSET0_FOCUS_PLAYER_IS_IN_A_POLICE_VEHICLE)
								
								REMOVE_DROP_OFF_BLIPS()
								
								IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
									// Print_Objective_Text("CARJCK_EXTVEH")
									Print_Objective_Text("CARJCK_GETVEH")
								ELSE
									Clear_Any_Objective_Text_From_This_Script()
								ENDIF
								
								IF iFocusParticipant = PARTICIPANT_ID_TO_INT()
									IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_PRINTED_POLICE_VEHICLE_HELP)
										IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
											IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
												IF IS_LOCAL_BIT0_SET(eLOCALBITSET0_PRINTED_EXPLNATORY_HELP_3)
													PRINT_HELP_NO_SOUND("CARJCK_POLHLP")
													GB_SET_GANG_BOSS_HELP_BACKGROUND()
													SET_LOCAL_BIT0(eLOCALBITSET0_PRINTED_POLICE_VEHICLE_HELP)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
							ELIF IS_LOCAL_BIT0_SET(eLOCALBITSET0_IS_PLAYER_IN_VALID_VEHICLE_AS_PASSENGER)
								
								REMOVE_DROP_OFF_BLIPS()
								
								IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
									// Print_Objective_Text("CARJCK_EXTVEH")
									Print_Objective_Text("CARJCK_GETVEH")
								ELSE
									Clear_Any_Objective_Text_From_This_Script()
								ENDIF
								
								IF iFocusParticipant = PARTICIPANT_ID_TO_INT()
									IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_PRINTED_VEHICLE_LIMO_HELP)
										IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
											IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
												IF IS_LOCAL_BIT0_SET(eLOCALBITSET0_PRINTED_EXPLNATORY_HELP_3)
													PRINT_HELP_NO_SOUND("CARJCK_PASHLP")
													GB_SET_GANG_BOSS_HELP_BACKGROUND()
													SET_LOCAL_BIT0(eLOCALBITSET0_PRINTED_VEHICLE_LIMO_HELP)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
							ELIF IS_LOCAL_BIT0_SET(eLOCALBITSET0_FOCUS_PLAYER_IS_IN_AN_EMERGENCY_VEHICLE)
								
								
								REMOVE_DROP_OFF_BLIPS()
								
								IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
									// Print_Objective_Text("CARJCK_EXTVEH")
									Print_Objective_Text("CARJCK_GETVEH")
								ELSE
									Clear_Any_Objective_Text_From_This_Script()
								ENDIF
								
								IF iFocusParticipant = PARTICIPANT_ID_TO_INT()
									IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_PRINTED_EMERGENCY_VEHICLE_HELP)
										IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
											IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
												IF IS_LOCAL_BIT0_SET(eLOCALBITSET0_PRINTED_EXPLNATORY_HELP_3)
													PRINT_HELP_NO_SOUND("CARJCK_EMGHLP")
													GB_SET_GANG_BOSS_HELP_BACKGROUND()
													SET_LOCAL_BIT0(eLOCALBITSET0_PRINTED_EMERGENCY_VEHICLE_HELP)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
							ELSE
								
								REMOVE_DROP_OFF_BLIPS()
								
								IF NOT IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_REQUEST_DELIVER_VEHICLE_TO_DROP_OFF)
									IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
										Print_Objective_Text("CARJCK_GETVEH")
									ELSE
										Clear_Any_Objective_Text_From_This_Script()
									ENDIF
								ENDIF
								
							ENDIF
							
							BOOL bClearDeliveredData
							
							// If I have delivered a vehicle, get out of the vehicle.
							IF iFocusParticipant = PARTICIPANT_ID_TO_INT()

								IF IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_ALLOWED_TO_DROP_OFF_VEHICLE)
	
									IF NOT IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_EXITED_DELIVERED_VEHICLE)
										Clear_Any_Objective_Text_From_This_Script()
										IF EMPTY_AND_LOCK_VEHICLE(sMyDeliveredVehicleData.myDeliveredVehicle, timeLeaveEvent)
											IF IS_VEHICLE_A_PERSONAL_VEHICLE(sMyDeliveredVehicleData.myDeliveredVehicle)
												IF NOT IS_VEHICLE_MY_PERSONAL_VEHICLE(sMyDeliveredVehicleData.myDeliveredVehicle)
													BROADCAST_SET_PERSONAL_VEHICLE_AS_DESTROYED(sMyDeliveredVehicleData.myDeliveredVehicle)
												ENDIF
											ENDIF
											SET_CARJACK_MISSION_REMOVAL_PARAMETERS(2, 40.0)
											SET_VEHICLE_REMOVE_AGGRESSIVE_CARJACK_MISSION(sMyDeliveredVehicleData.myDeliveredVehicle)
											GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
											SET_CLIENT_BIT0(eCLIENTBITSET0_EXITED_DELIVERED_VEHICLE)
											bClearDeliveredData = TRUE
											PRINTLN("[GBCARJCK] - EMPTY_AND_LOCK_VEHICLE = TRUE, setting bClearDeliveredData = TRUE")
										ENDIF
									ELSE
										
										Clear_Any_Objective_Text_From_This_Script()
										
									ENDIF
									
								ELIF IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_NOT_ALLOWED_TO_DROP_OFF_VEHICLE_DROP_FULL)
									
									bClearDeliveredData = TRUE
									PRINTLN("[GBCARJCK] - bit eCLIENTBITSET0_NOT_ALLOWED_TO_DROP_OFF_VEHICLE_DROP_FULL is set, setting bClearDeliveredData = TRUE, need to find a different drop off.")
									
								ENDIF
								
								IF bClearDeliveredData
									PRINTLN("[GBCARJCK] - bClearDeliveredData = TRUE, clearing delivered vehicle data")
									CLEAR_CLIENT_BIT0(eCLIENTBITSET0_REQUEST_DELIVER_VEHICLE_TO_DROP_OFF)
									CLEAR_CLIENT_BIT0(eCLIENTBITSET0_EXITED_DELIVERED_VEHICLE)
									CLEAR_CLIENT_BIT0(eCLIENTBITSET0_ALLOWED_TO_DROP_OFF_VEHICLE)
									CLEAR_CLIENT_BIT0(eCLIENTBITSET0_NOT_ALLOWED_TO_DROP_OFF_VEHICLE_DROP_FULL)
									iUpdateServerMyDeliveredVehicleStage = 0
									RESET_MY_DELIVERED_VEHICLE_DATA()
								ENDIF
								
							ENDIF
							
//						ENDIF
						
					ELSE
						REMOVE_DROP_OFF_BLIPS()
						Clear_Any_Objective_Text_From_This_Script()
					ENDIF
						
				ELSE
					REMOVE_DROP_OFF_BLIPS()
					Clear_Any_Objective_Text_From_This_Script()
				ENDIF
				
//			ENDIF
			
		BREAK
		
		CASE eCARJACKING_STATE_REWARDS
			
			REMOVE_DROP_OFF_BLIPS()
			Clear_Any_Objective_Text_From_This_Script()
			DRAW_BOTTOM_RIGHT_HUD()
			MAINTAIN_LEADERBOARD()
					
			HANDLE_REWARDS()
			
		BREAK
		
		CASE eCARJACKING_STATE_END
			
		BREAK
		
	ENDSWITCH

ENDPROC

PROC SERVER_PROCESSING()
	
	SWITCH GET_CARJACKING_STATE()
	
		CASE eCARJACKING_STATE_INIT
			
			INT i
			TEXT_LABEL_63 tl63Temp
			
//			// Setup the vehicle to be stolen.
//			IF GB_GET_GLOBAL_STEAL_VEHICLE_TARGET_CLASS() = VC_RAIL
//				SCRIPT_ASSERT("Magnate - acquire vehicle, class = VC_RAIL, has class been set?")
//				GB_SET_GLOBAL_STEAL_VEHICLE_TARGET_CLASS(VC_MOTORCYCLE)
//			ENDIF
//			SET_TARGET_VEHICLE_CLASS(GB_GET_GLOBAL_STEAL_VEHICLE_TARGET_CLASS())
			
			REPEAT GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS() i
				serverBd.sVehiclesDeliveredParticipantData[i].participantId = INVALID_PARTICIPANT_INDEX()
				serverBd.sVehiclesDeliveredParticipantData[i].tl63Name = tl63Temp
//				serverBD.sVehiclesDeliveredParticipantData[i].iNumberVehiclesDelivered = 0
				serverBD.sVehiclesDeliveredParticipantData[i].fTotalValueOfVehiclesDelivered = -1
				PRINTLN("[GBCARJCK] initialised serverBd.sVehiclesDeliveredParticipantData[[", i, "].participantId = ", NATIVE_TO_INT(serverBd.sVehiclesDeliveredParticipantData[i].participantId))
				PRINTLN("[GBCARJCK] initialised serverBd.sVehiclesDeliveredParticipantData[[", i, "].tl63Name = ", serverBd.sVehiclesDeliveredParticipantData[i].tl63Name)
//				PRINTLN("[GBCARJCK] initialised serverBD.sVehiclesDeliveredParticipantData[[", i, "].iNumberVehiclesDelivered = ", serverBD.sVehiclesDeliveredParticipantData[i].iNumberVehiclesDelivered)
				PRINTLN("[GBCARJCK] initialised serverBD.sVehiclesDeliveredParticipantData[", i, "].fTotalValueOfVehiclesDelivered = ", serverBD.sVehiclesDeliveredParticipantData[i].fTotalValueOfVehiclesDelivered)
			ENDREPEAT
			
			// Inisitialise random set of drop offs.
			INITIALISE_DROP_OFFS()
			
			// Move onto next stage.
			SET_CARJACKING_STATE(eCARJACKING_STATE_RUN)
			
			// Get MatchIds for Telemetry
			PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBD.iMatchId1, serverBD.iMatchId2)
			
		BREAK
		
		CASE eCARJACKING_STATE_RUN
		
			IF GET_END_REASON() = eENDREASON_NO_REASON_YET
				#IF IS_DEBUG_BUILD
				IF serverBd.bFakeEndOfModeTimer
					PRINTLN("[GBCARJCK] serverBd.bFakeEndOfModeTimer = TRUE.")
					SET_END_REASON(eENDREASON_TIME_UP)
				ENDIF
				#ENDIF
				IF HAS_NET_TIMER_STARTED(serverBd.modeTimer)
					IF HAS_NET_TIMER_EXPIRED(serverBd.modeTimer, GET_CARJACKING_TUNEABLE_TIME_LIMIT())
						SET_END_REASON(eENDREASON_TIME_UP)
					ENDIF
				ELSE
					START_NET_TIMER(serverBd.modeTimer)
				ENDIF
			ELSE
				SET_CARJACKING_STATE(eCARJACKING_STATE_REWARDS)
			ENDIF
			
		BREAK
		
		CASE eCARJACKING_STATE_REWARDS
			
			IF IS_SERVER_BIT0_SET(eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS)
				PRINTLN("[KINGCASTLE] - [REWARDS] - all participants completed rewards.")
				SET_CARJACKING_STATE(eCARJACKING_STATE_END)
			ENDIF
			
		BREAK
		
		CASE eCARJACKING_STATE_END
			SET_SERVER_GAME_STATE(GAME_STATE_END)
		BREAK
		
	ENDSWITCH

ENDPROC


// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------


PROC SCRIPT_CLEANUP()

	PRINTLN("[GBCARJCK] SCRIPT_CLEANUP")
	
	g_sGb_Telemetry_data.sdata.m_match1 = serverBD.iMatchId1
	g_sGb_Telemetry_data.sdata.m_match2 = serverBD.iMatchId2
	PROCESS_CURRENT_BOSS_CHALLENGE_PLAYSTATS(g_sGb_Telemetry_data.sdata, serverBD.iTotalOrganisationScore)
	PRINTLN("[GBCARJCK] - [TELEMETRY] - called PROCESS_CURRENT_BOSS_CHALLENGE_PLAYSTATS with a total score of ", serverBD.iTotalOrganisationScore)
	
	GB_COMMON_BOSS_MISSION_CLEANUP()	
	
	REMOVE_DROP_OFF_BLIPS()
	Clear_Any_Objective_Text_From_This_Script()
	
	GB_TIDYUP_SPECTATOR_CAM()
	
	TERMINATE_THIS_THREAD()
	
ENDPROC

FUNC BOOL PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)

	PRINTLN("[GBCARJCK] PROCESS_PRE_GAME")
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GB_MAX_GANG_SIZE_INCLUDING_BOSS_EXEC, missionScriptArgs)
	
	//Reserve Entities
	RESERVE_NETWORK_MISSION_OBJECTS(0)
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD,SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD,SIZE_OF(playerBD))
	
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	GB_COMMON_BOSS_MISSION_PREGAME()
	
	RESET_MY_DELIVERED_VEHICLE_DATA()
	
	RETURN TRUE
	
ENDFUNC

PROC PROCESS_EVENTS()

    INT iCount
    EVENT_NAMES ThisScriptEvent
	STRUCT_EVENT_COMMON_DETAILS Details
   
    REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount

        ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)
       	
        SWITCH ThisScriptEvent
            
			CASE EVENT_NETWORK_SCRIPT_EVENT
				
				GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Details, SIZE_OF(Details))
				
				SWITCH Details.Type
				
					CASE SCRIPT_EVENT_ACQUIRE_VEHICLE_DELIVERED_VEHICLE
						PROCESS_GB_SCRIPT_EVENT_ACQUIRE_VEHICLE_DELIVERED_VEHICLE(iCount)
					BREAK
					
					CASE SCRIPT_EVENT_SERVER_RECEIVED_ACQUIRE_VEHICLE_DELIVERED_VEHICLE_EVENT 
						PROCESS_GB_SCRIPT_EVENT_SERVER_RECEIVED_ACQUIRE_VEHICLE_DELIVERED_VEHICLE_EVENT(iCount)
					BREAK
					
				ENDSWITCH
				
			BREAK
			
        ENDSWITCH
            
    ENDREPEAT
    
ENDPROC

FUNC BOOL IS_PED_IN_VEHICLE_SEAT_NOT_ME(VEHICLE_INDEX vehicle, VEHICLE_SEAT eSeat)
	
	PED_INDEX pedId
	
	pedId = GET_PED_IN_VEHICLE_SEAT(vehicle, eSeat)
	
	IF DOES_ENTITY_EXIST(pedId)
		IF NOT IS_ENTITY_DEAD(pedId)
			IF pedId != PLAYER_PED_ID()
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_ANOTHER_PLAYER_IN_VEHILCE_WITH_LOCAL_PLAYER(VEHICLE_INDEX vehicle)
	
	IF IS_PED_IN_VEHICLE_SEAT_NOT_ME(vehicle, VS_DRIVER)
		RETURN TRUE
	ENDIF
	
	IF IS_PED_IN_VEHICLE_SEAT_NOT_ME(vehicle, VS_FRONT_RIGHT)
		RETURN TRUE
	ENDIF
	
	IF IS_PED_IN_VEHICLE_SEAT_NOT_ME(vehicle, VS_BACK_LEFT)
		RETURN TRUE
	ENDIF
	
	IF IS_PED_IN_VEHICLE_SEAT_NOT_ME(vehicle, VS_BACK_RIGHT)
		RETURN TRUE
	ENDIF
	
	IF IS_PED_IN_VEHICLE_SEAT_NOT_ME(vehicle, VS_EXTRA_LEFT_1)
		RETURN TRUE
	ENDIF
	
	IF IS_PED_IN_VEHICLE_SEAT_NOT_ME(vehicle, VS_EXTRA_RIGHT_1)
		RETURN TRUE
	ENDIF
	
	IF IS_PED_IN_VEHICLE_SEAT_NOT_ME(vehicle, VS_EXTRA_LEFT_2)
		RETURN TRUE
	ENDIF
	
	IF IS_PED_IN_VEHICLE_SEAT_NOT_ME(vehicle, VS_EXTRA_RIGHT_2)
		RETURN TRUE
	ENDIF
	
	IF IS_PED_IN_VEHICLE_SEAT_NOT_ME(vehicle, VS_EXTRA_LEFT_3)
		RETURN TRUE
	ENDIF
	
	IF IS_PED_IN_VEHICLE_SEAT_NOT_ME(vehicle, VS_EXTRA_RIGHT_3)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC ADJUST_VEHICLE_VALUE_FOR_DAMAGE(VEHICLE_INDEX vehicleId, FLOAT &fBaseValue)
	
	FLOAT fHealthPercentage = GET_VEHICLE_HEALTH_PERCENTAGE(vehicleId)
	FLOAT fDoorHealthPercentage = 30.0 // 6 doors
	FLOAT fFinalPercentage
	INT iDoorBitset, iDoorCount
	
	// Scale to accounting for 70% of the vehicle health.
	fHealthPercentage = ((70.0/100.0)*fHealthPercentage)
	
	// Cap to  - shouldn't happen because scaling above, but just ot be sure.
	IF fHealthPercentage > 70.0
		fHealthPercentage = 70.0
	ENDIF
	
	// Adjust for damaged doors.
	CHECK_THE_AVAILABLE_DOORS(vehicleId, iDoorBitset)
	
	REPEAT COUNT_OF(SC_DOOR_LIST) iDoorCount
		IF INT_TO_ENUM(SC_DOOR_LIST, iDoorCount) != SC_DOOR_INVALID
			IF IS_BIT_SET(iDoorBitset, iDoorCount)
				IF IS_VEHICLE_DOOR_DAMAGED(vehicleId, INT_TO_ENUM(SC_DOOR_LIST, iDoorCount))
					fDoorHealthPercentage -= 5.0
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Add for final percentage damage.
	fFinalPercentage = fDoorHealthPercentage + fHealthPercentage
	// PRINTLN("deebug - fFinalPercentage = fDoorHealthPercentage + fHealthPercentage ", fFinalPercentage, fDoorHealthPercentage, fHealthPercentage)
	
	// Get new value.
	fBaseValue = (fBaseValue/100.0)
	fBaseValue = fBaseValue*fFinalPercentage
	
ENDPROC

PROC PROCESS_PARTICIPANT_LOOP()
	
	INT iParticipant
	INT iPlayer
	PLAYER_INDEX playerTemp
	PED_INDEX pedTemp
	TEXT_LABEL_63 tl63Temp
	VEHICLE_INDEX vehTemp
	BOOL bNoBossOnScript = TRUE
	BOOL bAllCompletedRewards = TRUE
	INT iGangMemberCount
	
	// Reset data.
	REPEAT GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS() iPlayer
		sPlayer[iPlayer].playerId = INVALID_PLAYER_INDEX()
		sPlayer[iPlayer].iParticipantId = (-1)
		sPlayer[iPlayer].playerPedId = pedTemp
		CLEAR_BIT(iPlayerOkBitset, iPlayer)
		CLEAR_BIT(iPlayerDeadBitset, iPlayer)
	ENDREPEAT
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		sParticipant[iParticipant].participantId = INVALID_PARTICIPANT_INDEX()
		sParticipant[iParticipant].iPlayerId = (-1)
		CLEAR_BIT(iParticipantActiveBitset, iParticipant)
		sParticipant[iParticipant].tl63Name = tl63Temp
		sParticipant[iParticipant].bIsBoss = FALSE
		sParticipant[iParticipant].bIsGoon = FALSE
	ENDREPEAT
	
	CLEAR_LOCAL_BIT0(eLOCALBITSET0_IS_FOCUS_PLAYER_IN_ANY_VEHICLE #IF IS_DEBUG_BUILD, TRUE #ENDIF)
	CLEAR_LOCAL_BIT0(eLOCALBITSET0_IS_FOCUS_PLAYER_IN_VALID_VEHICLE_AS_DRIVER #IF IS_DEBUG_BUILD, TRUE #ENDIF)
	CLEAR_LOCAL_BIT0(eLOCALBITSET0_IS_PLAYER_IN_VALID_VEHICLE_AS_PASSENGER #IF IS_DEBUG_BUILD, TRUE #ENDIF)
	CLEAR_LOCAL_BIT0(eLOCALBITSET0_FOCUS_PLAYER_CURRENT_VEHICLE_IS_DRIVEABLE #IF IS_DEBUG_BUILD, TRUE #ENDIF)
	CLEAR_LOCAL_BIT0(eLOCALBITSET0_FOCUS_PLAYER_IS_IN_OWN_PERSONAL_VEHICLE #IF IS_DEBUG_BUILD, TRUE #ENDIF)
	CLEAR_LOCAL_BIT0(eLOCALBITSET0_FOCUS_PLAYER_IS_IN_A_PEGASUS_VEHICLE #IF IS_DEBUG_BUILD, TRUE #ENDIF)
	CLEAR_LOCAL_BIT0(eLOCALBITSET0_FOCUS_PLAYER_IS_IN_AN_ARMOURED_LIMO_VEHICLE #IF IS_DEBUG_BUILD, TRUE #ENDIF)
	CLEAR_LOCAL_BIT0(eLOCALBITSET0_FOCUS_PLAYER_IS_IN_A_POLICE_VEHICLE #IF IS_DEBUG_BUILD, TRUE #ENDIF)
	CLEAR_LOCAL_BIT0(eLOCALBITSET0_FOCUS_PLAYER_IS_IN_AN_EMERGENCY_VEHICLE #IF IS_DEBUG_BUILD, TRUE #ENDIF)
	
	currentVehicle = vehTemp
	eCurrentVehicleModel = DUMMY_MODEL_FOR_SCRIPT
	fCurrentVehicleValue = 0.0
//	vMyBossCoords = << 0.0, 0.0, 0.0 >>
	vFocusParticipantCoords = << 0.0, 0.0, 0.0 >>
	iNumGoonsOnScript = 0
//	fMyDistanceFromBoss = 999999.0
	
	// Refill data.
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant))
			
			sParticipant[iParticipant].participantId = INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant)
			SET_BIT(iParticipantActiveBitset, iParticipant)
			
			playerTemp = NETWORK_GET_PLAYER_INDEX(sParticipant[iParticipant].participantId)
			iPlayer = NATIVE_TO_INT(playerTemp)
			
			IF IS_NET_PLAYER_OK(playerTemp, FALSE)
				
				sPlayer[iPlayer].playerId = playerTemp
				sParticipant[iParticipant].iPlayerId = iPlayer
				sPlayer[iPlayer].iParticipantId = iParticipant
				sPlayer[iPlayer].playerPedId = GET_PLAYER_PED(playerTemp)
				sParticipant[iParticipant].tl63Name = GET_PLAYER_NAME(playerTemp)
				
				SET_BIT(iPlayerOkBitset, iPlayer)
				
				IF IS_ENTITY_DEAD(sPlayer[iPlayer].playerPedId)
					SET_BIT(iPlayerDeadBitset, iPlayer)
				ENDIF
				
				// Track if boss or a goon.
				IF GB_IS_PLAYER_BOSS_OF_A_GANG(sPlayer[iPlayer].playerId)
					sParticipant[iParticipant].bIsBoss = TRUE
				ELIF GB_IS_PLAYER_MEMBER_OF_A_GANG(sPlayer[iPlayer].playerId, FALSE)
					sParticipant[iParticipant].bIsGoon = TRUE
				ENDIF
					
				// Track if in a target vehicle.
//				IF sPlayer[iPlayer].playerPedId = PLAYER_PED_ID()
				IF iFocusParticipant = iParticipant
					vFocusParticipantCoords = GET_ENTITY_COORDS(sPlayer[iPlayer].playerPedId)
					IF IS_PED_IN_ANY_VEHICLE(sPlayer[iPlayer].playerPedId)
						SET_LOCAL_BIT0(eLOCALBITSET0_IS_FOCUS_PLAYER_IN_ANY_VEHICLE #IF IS_DEBUG_BUILD, TRUE #ENDIF)
						currentVehicle = GET_VEHICLE_PED_IS_IN(sPlayer[iPlayer].playerPedId)
						eCurrentVehicleModel = GET_ENTITY_MODEL(currentVehicle)
						IF (IS_VEHICLE_A_PERSONAL_VEHICLE(currentVehicle) AND IS_VEHICLE_MY_PERSONAL_VEHICLE(currentVehicle))
							SET_LOCAL_BIT0(eLOCALBITSET0_FOCUS_PLAYER_IS_IN_OWN_PERSONAL_VEHICLE #IF IS_DEBUG_BUILD, TRUE #ENDIF)
						ELIF IS_THIS_VEHICLE_A_PEGASUS_VEHICLE(currentVehicle)
							SET_LOCAL_BIT0(eLOCALBITSET0_FOCUS_PLAYER_IS_IN_A_PEGASUS_VEHICLE #IF IS_DEBUG_BUILD, TRUE #ENDIF)
						ELIF GB_IS_THIS_VEHICLE_A_GANG_BOSS_LIMO(currentVehicle)
							SET_LOCAL_BIT0(eLOCALBITSET0_FOCUS_PLAYER_IS_IN_AN_ARMOURED_LIMO_VEHICLE #IF IS_DEBUG_BUILD, TRUE #ENDIF)
						ELIF IS_MODEL_POLICE_VEHICLE(eCurrentVehicleModel)
							SET_LOCAL_BIT0(eLOCALBITSET0_FOCUS_PLAYER_IS_IN_A_POLICE_VEHICLE #IF IS_DEBUG_BUILD, TRUE #ENDIF)
						ELIF (eCurrentVehicleModel = AMBULANCE) OR (eCurrentVehicleModel = FIRETRUK)
							SET_LOCAL_BIT0(eLOCALBITSET0_FOCUS_PLAYER_IS_IN_AN_EMERGENCY_VEHICLE #IF IS_DEBUG_BUILD, TRUE #ENDIF)
						ELSE
							// IF GET_VEHICLE_CLASS(currentVehicle) = GET_TARGET_VEHICLE_CLASS()
							IF IS_THIS_MODEL_A_BICYCLE(eCurrentVehicleModel)
							OR IS_THIS_MODEL_A_BIKE(eCurrentVehicleModel)
							OR IS_THIS_MODEL_A_CAR(eCurrentVehicleModel)
							OR IS_THIS_MODEL_A_QUADBIKE(eCurrentVehicleModel)
								IF GET_PED_IN_VEHICLE_SEAT(currentVehicle, VS_DRIVER) = sPlayer[iPlayer].playerPedId
									SET_LOCAL_BIT0(eLOCALBITSET0_IS_FOCUS_PLAYER_IN_VALID_VEHICLE_AS_DRIVER #IF IS_DEBUG_BUILD, TRUE #ENDIF)
								ELSE
									IF IS_ANOTHER_PLAYER_IN_VEHILCE_WITH_LOCAL_PLAYER(currentVehicle)
										SET_LOCAL_BIT0(eLOCALBITSET0_IS_PLAYER_IN_VALID_VEHICLE_AS_PASSENGER #IF IS_DEBUG_BUILD, TRUE #ENDIF)
									ENDIF
								ENDIF
							ENDIF
							IF IS_VEHICLE_DRIVEABLE(currentVehicle)
								SET_LOCAL_BIT0(eLOCALBITSET0_FOCUS_PLAYER_CURRENT_VEHICLE_IS_DRIVEABLE #IF IS_DEBUG_BUILD, TRUE #ENDIF)
							ENDIF
						ENDIF
						IF IS_LOCAL_BIT0_SET(eLOCALBITSET0_IS_FOCUS_PLAYER_IN_VALID_VEHICLE_AS_DRIVER) // Get vehicle value if we are in a valid vehicle and can deliver it. If not, leave value at zero. 
						AND IS_LOCAL_BIT0_SET(eLOCALBITSET0_FOCUS_PLAYER_CURRENT_VEHICLE_IS_DRIVEABLE)
							fCurrentVehicleValue = GET_THIS_VEHICLE_VALUE(currentVehicle, eCurrentVehicleModel)
							IF fCurrentVehicleValue > GET_CARJACKING_TUNEABLE_VEHICLE_VALUE_CAP()
								fCurrentVehicleValue = TO_FLOAT(GET_CARJACKING_TUNEABLE_VEHICLE_VALUE_CAP())
							ENDIF
							ADJUST_VEHICLE_VALUE_FOR_DAMAGE(currentVehicle, fCurrentVehicleValue)
						ENDIF
					ENDIF
				ENDIF
				
				IF sParticipant[iParticipant].bIsBoss
					bNoBossOnScript = FALSE
//					vMyBossCoords = GET_ENTITY_COORDS(sPlayer[iPlayer].playerPedId, FALSE) // Track my boss coords.
					IF NOT IS_BIT_SET(iPlayerDeadBitset, iPlayer)
					AND NOT IS_BIT_SET(iPlayerDeadBitset, NATIVE_TO_INT(PLAYER_ID()))
//						fMyDistanceFromBoss = GET_DISTANCE_BETWEEN_COORDS(vFocusParticipantCoords, vMyBossCoords)
					ENDIF
				ELIF sParticipant[iParticipant].bIsGoon
					iNumGoonsOnScript++ // Track number of goons from my gang on this script.
				ENDIF
				
				IF NOT IS_CLIENT_BIT0_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET0_COMPLETED_REWARDS)
					bAllCompletedRewards = FALSE
				ENDIF
					
				IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
					
					IF GET_CARJACKING_STATE() >= eCARJACKING_STATE_RUN
					AND GET_END_REASON() = eENDREASON_NO_REASON_YET
						IF NOT IS_BIT_SET(serverBD.iAddedParticipantToDeliveredVehiclesArray, iParticipant)
							ADD_PARTICIPANT_TO_DELIVERED_VEHICLES_DATA(sParticipant[iParticipant].participantId, sParticipant[iParticipant].tl63Name, 0.0)
							SET_BIT(serverBD.iAddedParticipantToDeliveredVehiclesArray, iParticipant)
							PRINTLN("[GBCARJCK] added participant ", iParticipant, " to vehicles delivered array with an initial score of 0.")
						ENDIF
					ENDIF
					
					#IF IS_DEBUG_BUILD
					IF serverBD.iSPassPart = (-1)
					AND serverBD.iFFailPart = (-1)
						IF IS_CLIENT_DEBUG_BIT0_SET(sParticipant[iParticipant].participantId, eCLIENTDEBUGBITSET0_I_HAVE_S_PASSED)
							serverBD.iSPassPart = iParticipant
							PRINTLN("[GBCARJCK] participant ", iParticipant, " has S passed. Setting serverBD.iSPassPart = ", iParticipant)
						ELIF IS_CLIENT_DEBUG_BIT0_SET(sParticipant[iParticipant].participantId, eCLIENTDEBUGBITSET0_I_HAVE_F_FAILED)
							serverBD.iFFailPart = iParticipant
							PRINTLN("[GBCARJCK] participant ", iParticipant, " has F failed. Setting serverBD.iFFailPart = ", iParticipant)
						ENDIF
					ENDIF
					#ENDIF
					
				ENDIF
				
			ENDIF
			
		ELSE
			
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				IF GET_CARJACKING_STATE() >= eCARJACKING_STATE_RUN
				AND GET_END_REASON() = eENDREASON_NO_REASON_YET
					IF IS_BIT_SET(serverBD.iAddedParticipantToDeliveredVehiclesArray, iParticipant)
						REPEAT GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS() iGangMemberCount
							IF NATIVE_TO_INT(serverBD.sVehiclesDeliveredParticipantData[iGangMemberCount].participantId) = iParticipant
								REMOVE_PARTICIPANT_FROM_DELIVERED_VEHICLES_DATA(INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant))
								CLEAR_BIT(serverBD.iAddedParticipantToDeliveredVehiclesArray, iParticipant)
								PRINTLN("[GBCARJCK] remeoved participant ", iParticipant, " from vehicles delivered array with an initial score of 0.")
							ENDIF
						ENDREPEAT
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
		
	ENDREPEAT
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
		IF bAllCompletedRewards
			IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS)
				SET_SERVER_BIT0(eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS)
			ENDIF
		ELSE
			IF IS_SERVER_BIT0_SET(eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS)
				CLEAR_SERVER_BIT0(eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS)
			ENDIF
		ENDIF
		
		IF GET_END_REASON() = eENDREASON_NO_REASON_YET
		
			IF bNoBossOnScript
				SET_END_REASON(eENDREASON_NO_BOSS_LEFT)
			ELIF iNumGoonsOnScript = 0
				IF HAS_NET_TIMER_STARTED(serverBD.modeTimer)
					IF HAS_NET_TIMER_EXPIRED(serverBD.modeTimer, TRACK_GOON_DELAY) // Wait for little bit before ending script for lack of tgoons, to give them a chance to join the script before it terminates for them not being on it. 
						SET_END_REASON(eENDREASON_ALL_GOONS_LEFT)
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
		
	ENDIF
	
ENDPROC

#IF IS_DEBUG_BUILD

TEXT_WIDGET_ID twIdServerGameState
TEXT_WIDGET_ID twIdClientGameState[GB_MAX_GANG_SIZE_INCLUDING_BOSS_EXEC]
TEXT_WIDGET_ID twIdClientName[GB_MAX_GANG_SIZE_INCLUDING_BOSS_EXEC]
TEXT_WIDGET_ID twIdModeState
BOOL bTerminateScriptNow

PROC UPDATE_SERVER_GAME_STATE_WIDGET()
	SET_CONTENTS_OF_TEXT_WIDGET(twIdServerGameState, GET_GAME_STATE_NAME(GET_SERVER_GAME_STATE()))
	SET_CONTENTS_OF_TEXT_WIDGET(twIdModeState, GET_CARJACKING_STATE_NAME(GET_CARJACKING_STATE()))
ENDPROC

PROC UPDATE_CLIENT_GAME_STATE_WIDGET(INT iParticipant)
	
	STRING strName
	
	SET_CONTENTS_OF_TEXT_WIDGET(twIdClientGameState[iParticipant], "NOT ACTIVE")
	SET_CONTENTS_OF_TEXT_WIDGET(twIdClientName[iParticipant], "NOT ACTIVE")
	
	IF IS_BIT_SET(iParticipantActiveBitset, iParticipant)
		IF IS_BIT_SET(iPlayerOkBitset, sParticipant[iParticipant].iPlayerId)
			SET_CONTENTS_OF_TEXT_WIDGET(twIdClientGameState[iParticipant], GET_GAME_STATE_NAME(GET_CLIENT_GAME_STATE(iParticipant)))
			strName = GET_PLAYER_NAME(INT_TO_NATIVE(PLAYER_INDEX, sParticipant[iParticipant].iPlayerId))
			SET_CONTENTS_OF_TEXT_WIDGET(twIdClientName[iParticipant], strName)
		ENDIF
	ENDIF
	
ENDPROC

PROC CREATE_WIDGETS()
	
	INT iParticipant
	TEXT_LABEL_63 tl63Temp
	
	START_WIDGET_GROUP("GB Carjacking")
		ADD_WIDGET_BOOL("Locally terminate Script Now", bTerminateScriptNow)
		ADD_WIDGET_BOOL("Fake End Of Mode Timer", serverBd.bFakeEndOfModeTimer)
		START_WIDGET_GROUP("Game State")
			START_WIDGET_GROUP("Server")
				twIdServerGameState = ADD_TEXT_WIDGET("Game State")
				twIdModeState = ADD_TEXT_WIDGET("Mode State")
				UPDATE_SERVER_GAME_STATE_WIDGET()
				START_WIDGET_GROUP("Num Vehicles Delivered Widgets")
					START_WIDGET_GROUP("1 Vehicle Dropped Off Bit")
						ADD_BIT_FIELD_WIDGET("bit 0 (drops 0 -31)", serverBD.sNumberVehiclesDeliveredBitsets.iOneVehicleDeliveredBitset0)
						ADD_BIT_FIELD_WIDGET("bit 1 (rops 31 - 63)", serverBD.sNumberVehiclesDeliveredBitsets.iOneVehicleDeliveredBitset1)
					STOP_WIDGET_GROUP()
					START_WIDGET_GROUP("2 Vehicles Dropped Off Bit")
						ADD_BIT_FIELD_WIDGET("bit 0 (drops 0 -31)", serverBD.sNumberVehiclesDeliveredBitsets.iTwoVehiclesDeliveredBitset0)
						ADD_BIT_FIELD_WIDGET("bit 1 (rops 31 - 63)", serverBD.sNumberVehiclesDeliveredBitsets.iTwoVehiclesDeliveredBitset1)
					STOP_WIDGET_GROUP()
					START_WIDGET_GROUP("3 Vehicles Dropped Off Bit")
						ADD_BIT_FIELD_WIDGET("bit 0 (drops 0 -31)", serverBD.sNumberVehiclesDeliveredBitsets.iThreeVehiclesDeliveredBitset0)
						ADD_BIT_FIELD_WIDGET("bit 1 (rops 31 - 63)", serverBD.sNumberVehiclesDeliveredBitsets.iThreeVehiclesDeliveredBitset1)
					STOP_WIDGET_GROUP()
					START_WIDGET_GROUP("4 Vehicles Dropped Off Bit")
						ADD_BIT_FIELD_WIDGET("bit 0 (drops 0 -31)", serverBD.sNumberVehiclesDeliveredBitsets.iFourVehiclesDeliveredBitset0)
						ADD_BIT_FIELD_WIDGET("bit 1 (rops 31 - 63)", serverBD.sNumberVehiclesDeliveredBitsets.iFourVehiclesDeliveredBitset1)
					STOP_WIDGET_GROUP()
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Client")
				REPEAT GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS() iParticipant
					tl63Temp = ""
					tl63Temp +="Part "
					tl63Temp += iParticipant
					tl63Temp += " Name"
					twIdClientName[iParticipant] = ADD_TEXT_WIDGET(tl63Temp)
					tl63Temp = ""
					tl63Temp += "Part "
					tl63Temp += iParticipant
					tl63Temp += " Game State"
					twIdClientGameState[iParticipant] = ADD_TEXT_WIDGET(tl63Temp)
					tl63Temp = ""
					tl63Temp +="Part "
					tl63Temp += iParticipant
					tl63Temp += " Player ID"
					ADD_WIDGET_INT_READ_ONLY(tl63Temp, sParticipant[iParticipant].iPlayerId)
					tl63Temp = ""
					tl63Temp +="Part "
					tl63Temp += iParticipant
					tl63Temp += " Player stored Part ID"
					ADD_WIDGET_INT_READ_ONLY(tl63Temp, sPlayer[sParticipant[iParticipant].iPlayerId].iParticipantId)
					UPDATE_CLIENT_GAME_STATE_WIDGET(iParticipant)
				ENDREPEAT
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	
	PRINTLN("[GBCARJCK] - created widgets.")
	
ENDPROC		

PROC UPDATE_WIDGETS()
	
	INT iPartcount
	
	UPDATE_SERVER_GAME_STATE_WIDGET()
	
	REPEAT GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS() iPartcount
		UPDATE_CLIENT_GAME_STATE_WIDGET(iPartcount)
	ENDREPEAT
	
	IF NOT IS_LOCAL_DEBUG_BIT0_SET(eLOCALDEBUGBITSET0_TERMINATE_SCRIPT_NOW)
		IF bTerminateScriptNow
			PRINTLN("[GBCARJCK] widget bTerminateScriptNow = TRUE.")
			SET_LOCAL_DEBUG_BIT0(eLOCALDEBUGBITSET0_TERMINATE_SCRIPT_NOW)
		ENDIF
	ENDIF
	
ENDPROC

#ENDIF


PROC UPDATE_FOCUS_PARTICIPANT()

	// Save out the current focus participant. Stays at -1 if not the local player or not spectating a script participant.
	iFocusParticipant = (-1)
	
	IF IS_BIT_SET(iPlayerOkBitset, NATIVE_TO_INT(PLAYER_ID()))
		IF IS_BIT_SET(iParticipantActiveBitset, PARTICIPANT_ID_TO_INT())
			IF NOT IS_PLAYER_SPECTATING(PLAYER_ID())
				iFocusParticipant = PARTICIPANT_ID_TO_INT()
			ELSE
				IF IS_A_SPECTATOR_CAM_RUNNING()
				AND NOT IS_SPECTATOR_HUD_HIDDEN()
					PED_INDEX specTargetPed = GET_SPECTATOR_SELECTED_PED()
					IF IS_PED_A_PLAYER(specTargetPed)
						PLAYER_INDEX specPlayerTemp = NETWORK_GET_PLAYER_INDEX_FROM_PED(specTargetPed)
						IF NETWORK_IS_PLAYER_A_PARTICIPANT(specPlayerTemp)
							PARTICIPANT_INDEX specTargetParticipant = NETWORK_GET_PARTICIPANT_INDEX(specPlayerTemp)
							iFocusParticipant = NATIVE_TO_INT(specTargetParticipant)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC MAINTAIN_SCOREBOARD_SOUNDS()
	
	PLAYER_INDEX playerId
	
	IF serverBD.sVehiclesDeliveredParticipantData[0].participantId != INVALID_PARTICIPANT_INDEX()
		IF NETWORK_IS_PARTICIPANT_ACTIVE(serverBD.sVehiclesDeliveredParticipantData[0].participantId)
			playerId = NETWORK_GET_PLAYER_INDEX(serverBD.sVehiclesDeliveredParticipantData[0].participantId)
			IF playerId != previousFirstPlacePlayerId
			AND serverBD.sVehiclesDeliveredParticipantData[0].fTotalValueOfVehiclesDelivered > 0.0
				IF previousFirstPlacePlayerId = PLAYER_ID()
					INT iSoundId = GET_SOUND_ID()
					PLAY_SOUND_FRONTEND(iSoundId, "Lose_1st", "GTAO_Magnate_Boss_Modes_Soundset", FALSE)
				ELIF playerId = PLAYER_ID()
					INT iSoundId = GET_SOUND_ID()
					PLAY_SOUND_FRONTEND(iSoundId, "Enter_1st", "GTAO_Magnate_Boss_Modes_Soundset", FALSE)
				ENDIF
				previousFirstPlacePlayerId = playerId
			ENDIF
		ENDIF
	ENDIF
		
ENDPROC


//----------------------
//	MAIN SCRIPT
//----------------------

SCRIPT(MP_MISSION_DATA missionScriptArgs)


	PRINTLN("[GBCARJCK] START")
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
			
			IF NOT PROCESS_PRE_GAME(missionScriptArgs)
				EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
				SCRIPT_CLEANUP()
			ENDIF
			// Setup widgets. Make sure this is done after PROCESS_PRE_GAME()
			#IF IS_DEBUG_BUILD
				CREATE_WIDGETS()
			#ENDIF
		ELSE
			
			PRINTLN("[GBCARJCK] calling SCRIPT_CLEANUP() - IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE) = FALSE")
			SCRIPT_CLEANUP()
			
		ENDIF
	ELSE
		
		PRINTLN("[GBCARJCK] calling SCRIPT_CLEANUP() - NETWORK_IS_GAME_IN_PROGRESS() = FALSE")
		EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
		SCRIPT_CLEANUP()
		
	ENDIF
	
	WHILE (TRUE)
	
		MP_LOOP_WAIT_ZERO()
		
		#IF IS_DEBUG_BUILD
		IF IS_LOCAL_DEBUG_BIT0_SET(eLOCALDEBUGBITSET0_TERMINATE_SCRIPT_NOW)
			PRINTLN("[GBCARJCK] eLOCALDEBUGBITSET0_TERMINATE_SCRIPT_NOW is set")
			EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
			SCRIPT_CLEANUP()
		ENDIF
		#ENDIF
		
		IF GB_SHOULD_KILL_ACTIVE_BOSS_MISSION()
			PRINTLN("[GBCARJCK] - GB_SHOULD_KILL_ACTIVE_BOSS_MISSION = TRUE")
			EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
			SCRIPT_CLEANUP()
		ENDIF
		
		IF GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION()
			PRINTLN("[GBCARJCK] - GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION = TRUE")
			EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("[GBCARJCK] SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")
			EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
			SCRIPT_CLEANUP()
		ENDIF
		
		UPDATE_FOCUS_PARTICIPANT()
		PROCESS_PARTICIPANT_LOOP()
		PROCESS_EVENTS()
		MAINTAIN_ACTIVE_DROP_OFFS()
		MAINTAIN_SCOREBOARD_SOUNDS()
		
		GB_MAINTAIN_SPECTATE(sSpecVars)
		
		// Deal with the debug
		#IF IS_DEBUG_BUILD		
			UPDATE_WIDGETS()
		#ENDIF
		
		SWITCH(GET_CLIENT_GAME_STATE(PARTICIPANT_ID_TO_INT()))
		
			CASE GAME_STATE_INIT
				IF GET_SERVER_GAME_STATE() = GAME_STATE_RUNNING
					SET_CLIENT_GAME_STATE(PARTICIPANT_ID_TO_INT(),GAME_STATE_RUNNING)
				ENDIF
			BREAK
			
			CASE GAME_STATE_RUNNING
				IF GET_SERVER_GAME_STATE() = GAME_STATE_RUNNING
				
					CLIENT_PROCESSING()
					
				ELIF GET_SERVER_GAME_STATE() = GAME_STATE_END
					SET_CLIENT_GAME_STATE(PARTICIPANT_ID_TO_INT(), GAME_STATE_END)
				ENDIF
			BREAK
			
			CASE GAME_STATE_END
				SCRIPT_CLEANUP()
			BREAK
			
		ENDSWITCH
	
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
			SWITCH(GET_SERVER_GAME_STATE())
			
				CASE GAME_STATE_INIT
					IF INIT_SERVER()
						SET_SERVER_GAME_STATE(GAME_STATE_RUNNING)
					ENDIF
				BREAK
				
				CASE GAME_STATE_RUNNING
					//IF FALSE
					//	SET_SERVER_GAME_STATE(GAME_STATE_END)
					//ELSE
						
						SERVER_PROCESSING()
						
					//ENDIF
				BREAK
				
				CASE GAME_STATE_END
					SCRIPT_CLEANUP()
				BREAK
				
			ENDSWITCH
		
		ENDIF
		
	ENDWHILE

	
	
// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT


