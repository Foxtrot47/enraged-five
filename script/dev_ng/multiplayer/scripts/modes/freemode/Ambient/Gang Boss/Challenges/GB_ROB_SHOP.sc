//////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        GB_ROB_SHOP.sc																   	//
// Description: Boss Challenge wherein the boss and goons compete to hold up the most stores   	//
//				and deliver the stolen cash. Player who drops off the most cash wins			//
// Written by:  Steve Tiley																		//
// Date: 		01/09/2015																		//
//////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "net_mission.sch"


//----------------------
//	INCLUDES
//----------------------
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "flow_public_core_override.sch"
USING "net_wait_zero.sch"
USING "net_mission.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"
USING "net_hold_ups.sch"
USING "net_gang_boss.sch"
USING "am_common_ui.sch"
USING "net_gang_boss_spectate.sch"
USING "DM_Leaderboard.sch"

//----------------------
//	DEBUG
//----------------------

#IF IS_DEBUG_BUILD
WIDGET_GROUP_ID SmashAndGrabWidget
BOOL bCreateWidgets
INT iWidgetState
BOOL bHostEndMissionNow
BOOL bExpireTimer
#ENDIF

//----------------------
//	GAME STATE
//----------------------

CONST_INT GAME_STATE_INIT 									0
CONST_INT GAME_STATE_RUNNING								1
CONST_INT GAME_STATE_TERMINATE_DELAY						2
CONST_INT GAME_STATE_END									3

//----------------------
//	ENUM
//----------------------

ENUM GB_SMASH_AND_GRAB_STAGE_ENUM
	eGBSNG_INIT = 0,
	eGBSNG_STEAL_CASH,
	eGBSNG_END,
	eGBSNG_CLEANUP
ENDENUM

ENUM GB_STEAL_CASH_SUBSTAGE_ENUM
	eSCS_IDLE,
	eSCS_HOLDUP
ENDENUM

ENUM FINAL_COUNTDOWN_STAGE_ENUM
	eFCOUNTDOWN_PREP = 0,
	eFCOUNTDOWN_START,
	eFCOUNTDOWN_END_OR_KILL
ENDENUM

//----------------------
//	CONSTANTS
//----------------------

CONST_INT ONE_SECOND				1000
CONST_INT TWELVE_SECONDS			ONE_SECOND * 12
CONST_INT FIFTEEN_SECONDS			ONE_SECOND * 15	// 15 seconds
CONST_INT ONE_MINUTE 				FIFTEEN_SECONDS * 4	// 1 Min
CONST_INT TWO_MINUTES				ONE_MINUTE * 2	// 2 Mins
CONST_INT TEN_MINUTES				TWO_MINUTES * 5
CONST_INT FIFTEEN_MINUTES			ONE_MINUTE * 15	// 15 Mins
CONST_INT CHALLENGE_DURATION		TEN_MINUTES

CONST_INT DELIVERY_DISTANCE	5
CONST_INT NUM_MINI_LEADERBOARD_PLAYERS 4

//----------------------
//	Local Bitset
//----------------------

INT iHelpBitSet
INT iBigMessageBitSet

//----------------------
//	STRUCT
//----------------------
GB_MAINTAIN_SPECTATE_VARS sSpecVars
GB_STRUCT_BOSS_END_UI sEndUI

STRUCT SMASH_AND_GRAB_TELEMETRY_DATA
	INT iCashLost = 0
	INT iStoresHeldUp = 0
	INT iDeliveriesMade = 0
	INT iCashCollected = 0
ENDSTRUCT
SMASH_AND_GRAB_TELEMETRY_DATA sSmashAndGrabTelemetryData

//----------------------
//	VARIABLES
//----------------------
BOOL bCoronaBlipsBlocked = FALSE
BOOL bBeenKilled = FALSE
BOOL bLowNumbers = FALSE

INT iR, iG, iB, iA 
INT iTempCash
INT serverStaggeredPlayerLoop
INT localSyncID
INT iEndReason
INT iCleanupBossSpecStep
INT iMyLastPosition = -1
INT iTimeRemaining
INT iOvertime
//INT iMyLastScore = -1

//----------------------
//	MUSIC
//----------------------

FINAL_COUNTDOWN_STAGE_ENUM eFinalCountdownStage
INT iFinalCountdownBitSet
CONST_INT iFCBS_TriggeredPreCountdown	0
CONST_INT iFCBS_TriggeredCountdown		1
CONST_INT iFCBS_ReEnabledRadioControl	2
CONST_INT iFCBS_TriggeredCountdownKill	3
CONST_INT iFCBS_SuppressWanted			4
CONST_INT iFCBS_Trigger5s				5

//----------------------
//	BROADCAST VARIABLES
//----------------------

CONST_INT biS_ChallengeTied			0
CONST_INT biS_ChallengeNoScore		1
CONST_INT biS_StandardWinner		2
CONST_INT biS_BossLeft				3
CONST_INT biS_ChallengeEnded		4

// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData

	INT 			iServerGameState = GAME_STATE_INIT
	INT 			iServerBitSet
	
	PLAYER_INDEX 	piGangBoss
	PLAYER_INDEX 	piWinner
	PLAYER_INDEX 	piTiedWinner
	
	INT 			iWinnerScore
	INT 			iTotalGivenToBoss
	
	INT 			iBossDeathCount
	INT				iGoonDeathCount
	INT				iServerEndReason = -1
	
	GB_SMASH_AND_GRAB_STAGE_ENUM eStage = eGBSNG_STEAL_CASH
	
	SCRIPT_TIMER 	ChallengeStart
	SCRIPT_TIMER 	ChallengeDuration
	SCRIPT_TIMER 	ChallengeEnd
	
	// Leaderboard stuff
	INT				iPlayerBitSet
	INT				iServerSyncID
	INT 			leaderboardPlayerScore[NUM_NETWORK_PLAYERS]
	AMBIENT_ACTIVITY_LEADERBOARD_STRUCT sSortedLeaderboard[GB_MAX_GANG_SIZE_INCLUDING_BOSS_EXEC]
	
	INT				iMatchId1
	INT				iMatchId2
	
ENDSTRUCT
ServerBroadcastData serverBD

CONST_INT biP_RewardGiven			0
CONST_INT biP_EndCashGivenToBoss	1
CONST_INT biP_MadeADelivery			2
CONST_INT biP_BeenKilledWithCash	3
CONST_INT biP_IsWinner				4
CONST_INT biP_Participated			5

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	
	INT iPlayerGameState = GAME_STATE_INIT
	INT iPlayerBitSet
	
	INT iCurrentCash 		= 0
	
	GB_SMASH_AND_GRAB_STAGE_ENUM eStage 	= eGBSNG_INIT
	GB_STEAL_CASH_SUBSTAGE_ENUM eStealCashSubstage	= eSCS_IDLE
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

GANG_BOSS_MANAGE_REWARDS_DATA sRewardsData

// -----------------------------------------------------------------------------------------------------------
//		TERMINATION
// -----------------------------------------------------------------------------------------------------------

//----------------------
//	FUNCTIONS
//----------------------

// Tunable accessors

FUNC INT SNG_TUNABLE_GET_CHALLENGE_DURATION()
	RETURN g_sMPTunables.igb_smashandgrab_time_limit
ENDFUNC

FUNC INT SNG_TUNABLE_GET_CASH_DROP_PERCENTAGE()
	RETURN g_sMPTunables.igb_smashandgrab_cash_dropped_on_death
ENDFUNC

FUNC INT SNG_TUNABLE_GET_WANTED_CAP()
	RETURN g_sMPTunables.igb_smashandgrab_wanted_cap
ENDFUNC

// Non tunable, still needed
FUNC BOOL SNG_TUNABLE_SHOULD_USE_PERCENTAGE()
	RETURN TRUE
ENDFUNC

FUNC INT SNG_TUNABLE_MAX_COMBINED_TOTAL()
	RETURN g_sMPTunables.igb_smashandgrab_max_combined_total
ENDFUNC


// ---

FUNC BOOL SHOULD_UI_BE_HIDDEN()
	IF GB_SHOULD_HIDE_GANG_BOSS_EVENT(FMMC_TYPE_GB_CHAL_ROB_SHOP)
	OR GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
	OR NOT MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC RESET_SMASH_AND_GRAB_GLOBALS(BOOL bCleanup = FALSE)
	MPGlobalsAmbience.sSmashAndGrabStruct.iCashStolen = 0
	MPGlobalsAmbience.sSmashAndGrabStruct.bCollectedCash = FALSE
	MPGlobalsAmbience.sSmashAndGrabStruct.bInsideHoldupStore = FALSE
	MPGlobalsAmbience.sSmashAndGrabStruct.bManualHoldup = FALSE
	MPGlobalsAmbience.sSmashAndGrabStruct.bHoldupComplete = FALSE
	MPGlobalsAmbience.sSmashAndGrabStruct.bShouldEmptyRegister = FALSE
	
	IF bCleanup
		MPGlobalsAmbience.sSmashAndGrabStruct.bOnSmashGrab = FALSE
	ENDIF
ENDPROC

FUNC STRING GET_MISSION_STATE_STRING(INT iState)
	SWITCH(iState)
		CASE GAME_STATE_INIT 				RETURN "GAME_STATE_INIT"
		CASE GAME_STATE_RUNNING 			RETURN "GAME_STATE_RUNNING"
		CASE GAME_STATE_TERMINATE_DELAY 	RETURN "GAME_STATE_TERMINATE_DELAY"
		CASE GAME_STATE_END 				RETURN "GAME_STATE_END"
	ENDSWITCH

	RETURN "UNKNOWN MISSION STATE!"
ENDFUNC

FUNC STRING GET_MISSION_STAGE_STRING(GB_SMASH_AND_GRAB_STAGE_ENUM thisStage)
	SWITCH(thisStage)
		CASE eGBSNG_INIT				RETURN "eGBSNG_INIT"
		CASE eGBSNG_STEAL_CASH 			RETURN "eGBSNG_STEAL_CASH"
		CASE eGBSNG_END 				RETURN "eGBSNG_END"
		CASE eGBSNG_CLEANUP 			RETURN "eGBSNG_CLEANUP"
	ENDSWITCH

	RETURN "UNKNOWN MISSION STAGE!"
ENDFUNC

FUNC STRING GET_STEAL_CASH_SUBSTAGE_STRING(GB_STEAL_CASH_SUBSTAGE_ENUM thisStage)
	SWITCH thisStage
		CASE eSCS_IDLE			RETURN "eSCS_IDLE"
		CASE eSCS_HOLDUP		RETURN "eSCS_HOLDUP"
	ENDSWITCH
	
	RETURN "UNKNOWN STEAL CASH SUBSTAGE!"
ENDFUNC

// Server ----------------------------------------------- >|
//Helper function to get the server's game/mission state
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC

//Helper function to set the server's game/mission state
PROC SET_SERVER_MISSION_STATE(INT iState)
	PRINTLN("[GB SMASH AND GRAB] [STMGB] - SET_SERVER_MISSION_STATE - new server state: ", GET_MISSION_STATE_STRING(iState))
	serverBD.iServerGameState = iState
ENDPROC

//Helper function to get the server's game/mission stage
FUNC GB_SMASH_AND_GRAB_STAGE_ENUM GET_SERVER_MISSION_STAGE()
	RETURN serverBD.eStage
ENDFUNC

//Helper function to set the server's game/mission stage
PROC SET_SERVER_MISSION_STAGE(GB_SMASH_AND_GRAB_STAGE_ENUM newStage)
	PRINTLN("[GB SMASH AND GRAB] [STMGB] - SET_SERVER_MISSION_STAGE - new server stage: ", GET_MISSION_STAGE_STRING(newStage))
	serverBD.eStage = newStage
ENDPROC
// ------------------------------------------------------ |<

// Client ----------------------------------------------- >|
//Helper function to get a clients game/mission state
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iPlayerGameState
ENDFUNC

//Helper function to set a clients game/mission state
PROC SET_CLIENT_MISSION_STATE(INT iPlayer,INT iState)
	PRINTLN("[GB SMASH AND GRAB] [STMGB] - SET_CLIENT_MISSION_STATE - new client state: ", GET_MISSION_STATE_STRING(iState))
	playerBD[iPlayer].iPlayerGameState = iState
ENDPROC

//Helper function to get a client's game/mission stage
FUNC GB_SMASH_AND_GRAB_STAGE_ENUM GET_CLIENT_MISSION_STAGE(INT iPlayer)
	RETURN playerBD[iPlayer].eStage
ENDFUNC

//Helper function to set a clients game/mission stage
PROC SET_CLIENT_MISSION_STAGE(INT iPlayer, GB_SMASH_AND_GRAB_STAGE_ENUM newStage)
	PRINTLN("[GB SMASH AND GRAB] [STMGB] - SET_CLIENT_MISSION_STAGE - new client stage: ", GET_MISSION_STAGE_STRING(newStage))
	playerBD[iPlayer].eStage = newStage
ENDPROC

//Helper function to get a client's current steal cash substage
FUNC GB_STEAL_CASH_SUBSTAGE_ENUM GET_CLIENT_STEAL_CASH_SUBSTAGE(INT iPlayer)
	RETURN playerBD[iPlayer].eStealCashSubstage
ENDFUNC

//Helper function to set a client's new steal cash substage 
PROC SET_CLIENT_STEAL_CASH_SUBSTAGE(INT iPlayer, GB_STEAL_CASH_SUBSTAGE_ENUM newStage)
	PRINTLN("[GB SMASH AND GRAB] [STMGB] - SET_CLIENT_STEAL_CASH_SUBSTAGE - new steal cash substage: ", GET_STEAL_CASH_SUBSTAGE_STRING(newStage))
	playerBD[iPlayer].eStealCashSubstage = newStage
ENDPROC

PROC SET_LOCAL_PLAYER_END_REASON(INT iReason)
	iEndReason = iReason
ENDPROC

FUNC INT GET_LOCAL_PLAYER_END_REASON()
	RETURN iEndReason
ENDFUNC

/// PURPOSE:
///    Returns if the local player is inside a holdup store
/// RETURNS:
///    
FUNC BOOL IS_LOCAL_PLAYER_IN_ANY_HOLDUP_STORE()
	RETURN MPGlobalsAmbience.sSmashAndGrabStruct.bInsideHoldupStore
ENDFUNC
// ------------------------------------------------------ |<

// Gang info -------------------------------------------- >|

/// PURPOSE:
///    Set the server gang boss player ID to the passed player (THIS SHOULD ONLY BE DONE IN INIT_SERVER!!)
/// PARAMS:
///    thisPlayer - 
PROC SET_GANG_BOSS_PLAYER_INDEX(PLAYER_INDEX thisPlayer)
	IF thisPlayer != INVALID_PLAYER_INDEX()
		IF NETWORK_IS_PLAYER_ACTIVE(thisPlayer)
			PRINTLN("[GB SMASH AND GRAB] [STMGB] - INITIALISATION - SET_GANG_BOSS_PLAYER_INDEX - gang boss is ", GET_PLAYER_NAME(thisPlayer))
			serverBD.piGangBoss = thisPlayer
		ELSE
			PRINTLN("[GB SMASH AND GRAB] [STMGB] - INITIALISATION - SET_GANG_BOSS_PLAYER_INDEX - player is not active")
		ENDIF
	ELSE
		PRINTLN("[GB SMASH AND GRAB] [STMGB] - INITIALISATION - SET_GANG_BOSS_PLAYER_INDEX - invalid player index passed.")
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns the gang boss' player index
/// RETURNS:
///    
FUNC PLAYER_INDEX GET_GANG_BOSS_PLAYER_INDEX()
	RETURN serverBD.piGangBoss
ENDFUNC

/// PURPOSE:
///    Returns if the local player is the gang boss of the challenge
/// RETURNS:
///    
FUNC BOOL AM_I_THE_GANG_BOSS()
	IF PLAYER_ID() = GET_GANG_BOSS_PLAYER_INDEX()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the passed player is the gang boss
/// PARAMS:
///    thisPlayer - player to check
/// RETURNS:
///    
FUNC BOOL IS_THIS_PLAYER_THE_GANG_BOSS(PLAYER_INDEX thisPlayer)
	IF thisPlayer = GET_GANG_BOSS_PLAYER_INDEX()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_PLAYER_A_GANG_GOON(PLAYER_INDEX thisPlayer)
	IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(thisPlayer, GET_GANG_BOSS_PLAYER_INDEX(), FALSE)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Return the player ID of the winner
/// RETURNS:
///    
FUNC PLAYER_INDEX GET_WINNER()
	RETURN serverBD.piWinner
ENDFUNC

/// PURPOSE:
///    Return the player ID of the player who tied for first
/// RETURNS:
///    
FUNC PLAYER_INDEX GET_TIED_WINNER()
	RETURN serverBD.piTiedWinner
ENDFUNC

/// PURPOSE:
///    Returns the coords of the local player
/// RETURNS:
///    
FUNC VECTOR GET_LOCAL_PLAYER_COORDS()
	RETURN GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
ENDFUNC

/// PURPOSE:
///    Returns if the passed player has a wanted level
/// PARAMS:
///    iPlayer - player to check
/// RETURNS:
///    
FUNC BOOL IS_PLAYER_WANTED(INT iPlayer)
	IF GET_PLAYER_WANTED_LEVEL(INT_TO_PLAYERINDEX(iPlayer)) > 0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE	
ENDFUNC

/// PURPOSE:
///    Checks if the local player has collected hold up cash
/// RETURNS:
///    
FUNC BOOL HAS_LOCAL_PLAYER_COLLECTED_HOLDUP_CASH()
	RETURN MPGlobalsAmbience.sSmashAndGrabStruct.bCollectedCash
ENDFUNC

FUNC BOOL IS_THIS_A_MANUAL_HOLDUP()
	RETURN MPGlobalsAmbience.sSmashAndGrabStruct.bManualHoldup
ENDFUNC

/// PURPOSE:
///    Returns the passed players amount of cash currently held
/// PARAMS:
///    iPlayer - player to check
/// RETURNS:
///    
FUNC INT GET_PLAYER_CASH_COLLECTED(INT iPlayer)
	RETURN playerBD[iPlayer].iCurrentCash
ENDFUNC

FUNC STRING GET_MY_POSITION()
	IF serverBD.sSortedLeaderboard[0].playerID = PLAYER_ID()
		RETURN "GB_SNG_FIRST"
	ELIF serverBD.sSortedLeaderboard[1].playerID = PLAYER_ID()
		RETURN "GB_SNG_SECOND"
	ELIF serverBD.sSortedLeaderboard[2].playerID = PLAYER_ID()
		RETURN "GB_SNG_THIRD"
	ENDIF
	
	RETURN "GB_SNG_FOURTH"
ENDFUNC

FUNC BOOL HAS_PLAYER_FULLY_SPAWNED()
	IF IS_SCREEN_FADED_IN()
	AND GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState = RESPAWN_STATE_PLAYING
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// ------------------------------------------------------ |<

// Blips ------------------------------------------------ >|

// ------------------------------------------------------ |<

// Help / Big Message functions ------------------------- >|

FUNC BOOL IS_SAFE_FOR_HELP(BOOL bIncludeOtherHelp = FALSE)
	IF  NOT IS_CUSTOM_MENU_ON_SCREEN()
	AND NOT IS_PAUSE_MENU_ACTIVE()
	AND NOT IS_RADAR_HIDDEN()
	AND IS_NET_PLAYER_OK(PLAYER_ID())
	AND NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
		IF bIncludeOtherHelp
		 	IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				RETURN TRUE
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEAR_THIS_HELP(INT thisHelp)
	TEXT_LABEL_63 help = "GB_SNG_HELP"
	help += thisHelp
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(help)
		CLEAR_HELP()
	ENDIF
ENDPROC

PROC CLEAR_ANY_HELP()
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GB_SNG_HELP0")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GB_SNG_HELP1")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GB_SNG_HELP2")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GB_SNG_HELP3")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GB_SNG_HELP4")
		CLEAR_HELP()
	ENDIF
ENDPROC

PROC DO_HELP_TEXT(INT iHelp)

	IF SHOULD_UI_BE_HIDDEN()
		CLEAR_ANY_HELP()
		EXIT
	ENDIF

	SWITCH iHelp
//		CASE 0		// You have started Smash-and-Grab. Compete against your Goons to steal the most cash from convenience stores ~BLIP_CRIM_HOLDUPS~
//			IF NOT IS_BIT_SET(iHelpBitSet, iHelp)
//				IF IS_SAFE_FOR_HELP()
//					PRINT_HELP_NO_SOUND("GB_SNG_HELP0")
//					GB_SET_GANG_BOSS_HELP_BACKGROUND()
//					SET_BIT(iHelpBitSet, iHelp)
//					PRINTLN("[GB SMASH AND GRAB] [STMGB] - DO_HELP_TEXT - Help ", iHelp, " done: ", GET_FILENAME_FOR_AUDIO_CONVERSATION("GB_SNG_HELP0"))
//				ENDIF
//			ENDIF
//		BREAK
		
		CASE 1		// Your Boss has started Smash-and-Grab. Compete against other members of your Organization to steal the most cash from convenience stores ~BLIP_CRIM_HOLDUPS~
			IF NOT IS_BIT_SET(iHelpBitSet, iHelp)
				IF IS_SAFE_FOR_HELP(TRUE)
					PRINT_HELP_NO_SOUND("GB_SNG_HELP1")
					PRINT_HELP_WITH_LITERAL_STRING_WITH_HUD_COLOUR_NO_SOUND("GB_SNG_HELP1", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(GET_GANG_BOSS_PLAYER_INDEX()), GET_PLAYER_HUD_COLOUR(GET_GANG_BOSS_PLAYER_INDEX()))
					GB_SET_GANG_BOSS_HELP_BACKGROUND()
					SET_BIT(iHelpBitSet, iHelp)
					PRINTLN("[GB SMASH AND GRAB] [STMGB] - DO_HELP_TEXT - Help ", iHelp, " done: ", GET_FILENAME_FOR_AUDIO_CONVERSATION("GB_SNG_HELP1"))
				ENDIF
			ENDIF
		BREAK
		
		CASE 2		// Threaten the store clerk with a weapon to start the hold up. Intimidate them by shooting shop items or shouting into the microphone to make them grab cash faster.
			IF NOT IS_BIT_SET(iHelpBitSet, iHelp)
				IF IS_SAFE_FOR_HELP(TRUE)
					PRINT_HELP_NO_SOUND("GB_SNG_HELP2")
					GB_SET_GANG_BOSS_HELP_BACKGROUND()
					SET_BIT(iHelpBitSet, iHelp)
					PRINTLN("[GB SMASH AND GRAB] [STMGB] - DO_HELP_TEXT - Help ", iHelp, " done: ", GET_FILENAME_FOR_AUDIO_CONVERSATION("GB_SNG_HELP2"))
				ENDIF
			ENDIF
		BREAK
		
		CASE 3		// You can either lose any wanted rating you have between each store to keep discreet, or remain wanted and go loud.
			IF NOT IS_BIT_SET(iHelpBitSet, iHelp)
				IF IS_SAFE_FOR_HELP(TRUE)
					PRINT_HELP_NO_SOUND("GB_SNG_HELP3")
					GB_SET_GANG_BOSS_HELP_BACKGROUND()
					SET_BIT(iHelpBitSet, iHelp)
					PRINTLN("[GB SMASH AND GRAB] [STMGB] - DO_HELP_TEXT - Help ", iHelp, " done: ", GET_FILENAME_FOR_AUDIO_CONVERSATION("GB_SNG_HELP3"))
				ENDIF
			ENDIF
		BREAK
		
		CASE 4		// When you die, some of your stolen cash is lost. Try to stay alive to maximise your score.
			IF NOT IS_BIT_SET(iHelpBitSet, iHelp)
				IF IS_SAFE_FOR_HELP(TRUE)
					PRINT_HELP_NO_SOUND("GB_SNG_HELP4")
					GB_SET_GANG_BOSS_HELP_BACKGROUND()
					SET_BIT(iHelpBitSet, iHelp)
					PRINTLN("[GB SMASH AND GRAB] [STMGB] - DO_HELP_TEXT - Help ", iHelp, " done: ", GET_FILENAME_FOR_AUDIO_CONVERSATION("GB_SNG_HELP4"))
				ENDIF
			ENDIF
		BREAK
		
		CASE 5
			IF NOT IS_BIT_SET(iHelpBitSet, iHelp)
				IF IS_SAFE_FOR_HELP(TRUE)
					PRINT_HELP_NO_SOUND("GB_SNG_HELP5")
					GB_SET_GANG_BOSS_HELP_BACKGROUND()
					SET_BIT(iHelpBitSet, iHelp)
					PRINTLN("[GB SMASH AND GRAB] [STMGB] - DO_HELP_TEXT - Help ", iHelp, " done: ", GET_FILENAME_FOR_AUDIO_CONVERSATION("GB_SNG_HELP5"))
				ENDIF
			ENDIF
		BREAK
			
	ENDSWITCH

ENDPROC

/// PURPOSE:
///    Performs general checks for it being safe to display the shard
/// RETURNS:
///    True if safe
FUNC BOOL IS_SAFE_FOR_BIG_MESSAGE()	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND NOT IS_PAUSE_MENU_ACTIVE()
	AND NOT IS_CUSTOM_MENU_ON_SCREEN()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE				
ENDFUNC

#IF IS_DEBUG_BUILD
/// PURPOSE:
///    Prints information about the big message that has just been requested to draw
/// PARAMS:
///    iBigMessage - the index of the message that was drawn
///    title - the title of the message
///    strapline - the subtitle of the message
PROC PRINT_BIG_MESSAGE_DEBUG_INFORMATION(INT iBigMessage, STRING title, STRING strapline)
	PRINTLN("[GB SMASH AND GRAB] [STMGB] - PRINT_BIG_MESSAGE_DEBUG_INFORMATION - Big Message:	", iBigMessage)
	PRINTLN("[GB SMASH AND GRAB] [STMGB] - PRINT_BIG_MESSAGE_DEBUG_INFORMATION - Title: 		", GET_FILENAME_FOR_AUDIO_CONVERSATION(title))
	PRINTLN("[GB SMASH AND GRAB] [STMGB] - PRINT_BIG_MESSAGE_DEBUG_INFORMATION - Strapline:		", GET_FILENAME_FOR_AUDIO_CONVERSATION(strapline))
ENDPROC
#ENDIF


PROC DO_BIG_MESSAGE(INT iBigMessage)

	IF SHOULD_UI_BE_HIDDEN()
		CLEAR_ALL_BIG_MESSAGES()
		EXIT
	ENDIF

	SWITCH iBigMessage
		CASE 0		//	SMASH AND GRAB - Steal and deliver the most cash to win
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_HELP()
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_START_OF_JOB, "GB_BIGM_SG_T", "GB_SNG_BMS0")
					SET_BIT(iBigMessageBitSet, iBigMessage)
					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_BIGM_SG_T", "GB_SNG_BMS0") #ENDIF
				ENDIF
			ENDIF
		BREAK
		
//		CASE 1		// CHALLENGE OVER - You came ~a~ in Smash and Grab by stealing and delivering $~1~
//			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
//				IF IS_SAFE_FOR_BIG_MESSAGE()
//					CLEAR_HELP()
//					SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_GB_END_OF_JOB_OVER, GET_MY_POSITION(), "GB_SNG_BMS1", "GB_CHAL_OVER", DEFAULT, GET_PLAYER_CASH_COLLECTED(PARTICIPANT_ID_TO_INT()))
//					SET_BIT(iBigMessageBitSet, iBigMessage)
//					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_CHAL_OVER", "GB_SNG_BMS1") #ENDIF
//				ENDIF
//			ENDIF
//		BREAK
		
		CASE 2		// WINNER - You won Smash and Grab by stealing and delivering $~1~
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_HELP()
					SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, GET_PLAYER_CASH_COLLECTED(PARTICIPANT_ID_TO_INT()), "GB_SNG_BMS2", "GB_WINNER")
					SET_BIT(iBigMessageBitSet, iBigMessage)
					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_WINNER", "GB_SNG_BMS2") #ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 3		// WINNER - You and <C>~a~</C> won Smash and Grab by stealing and delivering $~1~
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_HELP()
					IF GET_WINNER() = PLAYER_ID()
						IF GET_TIED_WINNER() != INVALID_PLAYER_INDEX()
							IF NETWORK_IS_PLAYER_ACTIVE(GET_TIED_WINNER())
								SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, GET_TIED_WINNER(), GET_PLAYER_CASH_COLLECTED(PARTICIPANT_ID_TO_INT()), "GB_SNG_BMS3", "GB_WINNER")
							ENDIF
						ENDIF
					ELSE
						IF GET_WINNER() != INVALID_PLAYER_INDEX()
							IF NETWORK_IS_PLAYER_ACTIVE(GET_WINNER())
								SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, GET_WINNER(), GET_PLAYER_CASH_COLLECTED(PARTICIPANT_ID_TO_INT()), "GB_SNG_BMS3", "GB_WINNER")
							ENDIF
						ENDIF
					ENDIF
					SET_BIT(iBigMessageBitSet, iBigMessage)
					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_WINNER", "GB_SNG_BMS3") #ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 4
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_HELP()
					IF IS_BIT_SET(serverBD.iServerBitSet, biS_ChallengeTied)
						IF GET_WINNER() != INVALID_PLAYER_INDEX()
						AND GET_TIED_WINNER() != INVALID_PLAYER_INDEX()
							IF NETWORK_IS_PLAYER_ACTIVE(GET_WINNER())
							AND NETWORK_IS_PLAYER_ACTIVE(GET_TIED_WINNER())
								SETUP_NEW_BIG_MESSAGE_WITH_2_PLAYERS(BIG_MESSAGE_GB_END_OF_JOB_OVER, GET_WINNER(), GET_TIED_WINNER(), GET_PLAYER_CASH_COLLECTED(NATIVE_TO_INT(GET_WINNER())), "GB_SNG_BMS4b", "GB_CHAL_OVER")
								SET_BIT(iBigMessageBitSet, iBigMessage)
								#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_CHAL_OVER", "GB_SNG_BMS4b") #ENDIF
							ENDIF
						ENDIF
					ELSE
						IF GET_WINNER() != INVALID_PLAYER_INDEX()
							IF NETWORK_IS_PLAYER_ACTIVE(GET_WINNER())
								SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GB_END_OF_JOB_OVER, GET_WINNER(), GET_PLAYER_CASH_COLLECTED(NATIVE_TO_INT(GET_WINNER())), "GB_SNG_BMS4", "GB_CHAL_OVER")
								SET_BIT(iBigMessageBitSet, iBigMessage)
								#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_CHAL_OVER", "GB_SNG_BMS4") #ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 5		// CHALLENGE OVER - No cash was delivered
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_HELP()
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_CHAL_OVER", "GB_SNG_BMS5")
					SET_BIT(iBigMessageBitSet, iBigMessage)
					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_CHAL_OVER", "GB_SNG_BMS5") #ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 6		// CHALLENGE OVER - Your Boss has left the session
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_HELP()
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_CHAL_OVER", "GB_SNG_BMS6")
					SET_BIT(iBigMessageBitSet, iBigMessage)
					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_CHAL_OVER", "GB_SNG_BMS6") #ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC DO_COLLECT_TICKER(INT iCash)
	SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
	TickerEventData.TickerEvent = TICKER_EVENT_GB_SNG_COLLECT_CASH
	TickerEventData.playerID = PLAYER_ID()
	TickerEventData.dataInt = iCash
	BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT())
ENDPROC

PROC DO_DELIVER_TICKER(INT iCash)
	SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
	TickerEventData.TickerEvent = TICKER_EVENT_GB_SNG_DELIVER_CASH
	TickerEventData.playerID = PLAYER_ID()
	TickerEventData.dataInt = iCash
	BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT())
ENDPROC

PROC DO_DROP_TICKER(INT iCash)
	SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
	TickerEventData.TickerEvent = TICKER_EVENT_GB_SNG_DROP_CASH
	TickerEventData.playerID = PLAYER_ID()
	TickerEventData.dataInt = iCash
	BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT())
ENDPROC

// ------------------------------------------------------ |<

FUNC BOOL IS_SAFE_TO_DELIVER_CASH()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
//	AND NOT IS_PLAYER_WANTED(PARTICIPANT_ID_TO_INT())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_CASH_TO_DROP(INT iInitialCash)
	RETURN (iInitialCash / 100) * SNG_TUNABLE_GET_CASH_DROP_PERCENTAGE()
ENDFUNC

PROC HANDLE_LOSE_CASH_UPON_DEATH()
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
		IF bBeenKilled = FALSE
			PRINTLN("[GB SMASH AND GRAB] [STMGB] - HANDLE_LOSE_CASH_UPON_DEATH - local player has died.")
			INT iCash = GET_CASH_TO_DROP( GET_PLAYER_CASH_COLLECTED(PARTICIPANT_ID_TO_INT()) )
			PRINTLN("[GB SMASH AND GRAB] [STMGB] - HANDLE_LOSE_CASH_UPON_DEATH - cash to drop = ", iCash)
			IF iCash > 0
				//Consider using NETWORK_CAN_SPEND_MONEY
				IF USE_SERVER_TRANSACTIONS()
					INT iTransactionSlot
					TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_CASH_DROP, iCash, iTransactionSlot)
				ELSE
					NETWORK_SPENT_CASH_DROP(iCash)
				ENDIF
				
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_BeenKilledWithCash)
				DO_DROP_TICKER(iCash)
				playerBD[PARTICIPANT_ID_TO_INT()].iCurrentCash -= iCash
				sSmashAndGrabTelemetryData.iCashLost += iCash
			ENDIF
			bBeenKilled = TRUE
		ENDIF
	ELSE
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_BeenKilledWithCash)
			IF HAS_PLAYER_FULLY_SPAWNED()
				IF bBeenKilled = TRUE
					PRINTLN("[GB SMASH AND GRAB] [STMGB] - HANDLE_LOSE_CASH_UPON_DEATH - local player has fully respawned, showing help")
					DO_HELP_TEXT(4)
					bBeenKilled = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_CHALLENGE()

	HANDLE_LOSE_CASH_UPON_DEATH()
	
	SWITCH GET_CLIENT_STEAL_CASH_SUBSTAGE(PARTICIPANT_ID_TO_INT())
	
		CASE eSCS_IDLE
			DO_HELP_TEXT(1)
			IF IS_LOCAL_PLAYER_IN_ANY_HOLDUP_STORE()
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Participated)
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Participated)
				ENDIF
				SET_CLIENT_STEAL_CASH_SUBSTAGE(PARTICIPANT_ID_TO_INT(), eSCS_HOLDUP)
			ENDIF
		BREAK
		
		CASE eSCS_HOLDUP
			IF IS_LOCAL_PLAYER_IN_ANY_HOLDUP_STORE()
				IF IS_THIS_A_MANUAL_HOLDUP()
					DO_HELP_TEXT(2)
					IF MPGlobalsAmbience.sSmashAndGrabStruct.iCashStolen > 0
						playerBD[PARTICIPANT_ID_TO_INT()].iCurrentCash += MPGlobalsAmbience.sSmashAndGrabStruct.iCashStolen
						iTempCash += MPGlobalsAmbience.sSmashAndGrabStruct.iCashStolen
						MPGlobalsAmbience.sSmashAndGrabStruct.iCashStolen = 0
					ENDIF
					IF HAS_LOCAL_PLAYER_COLLECTED_HOLDUP_CASH()
						IF iTempCash > 0
							DO_COLLECT_TICKER(iTempCash)
						ENDIF
						
						sSmashAndGrabTelemetryData.iStoresHeldUp += 1
						sSmashAndGrabTelemetryData.iCashCollected += iTempCash
						
						MPGlobalsAmbience.sSmashAndGrabStruct.bCollectedCash = FALSE
						MPGlobalsAmbience.sSmashAndGrabStruct.bManualHoldup = FALSE
						MPGlobalsAmbience.sSmashAndGrabStruct.iCashStolen = 0
						iTempCash = 0
						
						GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
						SET_CLIENT_STEAL_CASH_SUBSTAGE(PARTICIPANT_ID_TO_INT(), eSCS_IDLE)
					ENDIF
				ELSE
					DO_HELP_TEXT(2)
					IF HAS_LOCAL_PLAYER_COLLECTED_HOLDUP_CASH()
						DO_COLLECT_TICKER(MPGlobalsAmbience.sSmashAndGrabStruct.iCashStolen)
						
						sSmashAndGrabTelemetryData.iStoresHeldUp += 1
						sSmashAndGrabTelemetryData.iCashCollected += MPGlobalsAmbience.sSmashAndGrabStruct.iCashStolen
						
						playerBD[PARTICIPANT_ID_TO_INT()].iCurrentCash += MPGlobalsAmbience.sSmashAndGrabStruct.iCashStolen
						MPGlobalsAmbience.sSmashAndGrabStruct.bCollectedCash = FALSE
						MPGlobalsAmbience.sSmashAndGrabStruct.iCashStolen = 0
						
						GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
						SET_CLIENT_STEAL_CASH_SUBSTAGE(PARTICIPANT_ID_TO_INT(), eSCS_IDLE)
					ENDIF
				ENDIF
			ELSE
				IF GET_PLAYER_CASH_COLLECTED(PARTICIPANT_ID_TO_INT()) > 0
					DO_HELP_TEXT(3)
				ENDIF
				IF MPGlobalsAmbience.sSmashAndGrabStruct.bHoldupComplete = TRUE
					MPGlobalsAmbience.sSmashAndGrabStruct.bHoldupComplete = FALSE
				ENDIF
				SET_CLIENT_STEAL_CASH_SUBSTAGE(PARTICIPANT_ID_TO_INT(), eSCS_IDLE)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC CLEAR_WANTED()
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
		SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
		SET_WANTED_LEVEL_MULTIPLIER(1.0)
		PRINTLN("[GB SMASH AND GRAB] [STMGB] - CLEAR_WANTED - local player wanted level has been cleared")
	ENDIF
ENDPROC

FUNC BOOL IS_SAFE_FOR_END_MESSAGES()
	IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE)
	AND NOT IS_RADAR_HIDDEN()
	AND HAS_PLAYER_FULLY_SPAWNED()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_CHALLENGE_END_REWARDS_AND_MESSAGES()

	IF GB_SHOULD_RUN_SPEC_CAM()
		PRINTLN("[GB SMASH AND GRAB] [STMGB] - PROCESS_CHALLENGE_END_REWARDS_AND_MESSAGES - Boss - Player is spectating, calling GB_SET_SHOULD_CLOSE_SPECTATE, moving to next stage")
		GB_SET_SHOULD_CLOSE_SPECTATE()
	ENDIF
	
	IF IS_SAFE_FOR_END_MESSAGES()
		IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerBitSet, biP_RewardGiven)
			
			// Boss left the session
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_BossLeft)
				GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_CHAL_ROB_SHOP, FALSE, sRewardsData)
				GANG_BOSS_MANAGE_WANTED_CLEAN_UP(TRUE, INVALID_PLAYER_INDEX())
				GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_BOSS_LEFT)
				SET_LOCAL_PLAYER_END_REASON(GB_TELEMETRY_END_BOSS_LEFT)
				SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerBitSet, biP_RewardGiven)
				
				EXIT	// Exit to prevent processing further cases
			ENDIF

			// Challenge ended without players delivering any cash
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_ChallengeNoScore)
				DO_BIG_MESSAGE(5)
				GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_CHAL_ROB_SHOP, FALSE, sRewardsData)
				GANG_BOSS_MANAGE_WANTED_CLEAN_UP(TRUE, INVALID_PLAYER_INDEX())
				GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_TIME_OUT)
				SET_LOCAL_PLAYER_END_REASON(GB_TELEMETRY_END_TIME_OUT)
				SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerBitSet, biP_RewardGiven)
				
				EXIT	// Exit to prevent processing further cases
			ENDIF

			// Challenge tied between two players
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_ChallengeTied)
				IF PLAYER_ID() = GET_WINNER()
				OR PLAYER_ID() = GET_TIED_WINNER()
					DO_BIG_MESSAGE(3)
					GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_CHAL_ROB_SHOP, TRUE, sRewardsData)
					GANG_BOSS_MANAGE_WANTED_CLEAN_UP(TRUE, INVALID_PLAYER_INDEX())
					GB_SET_COMMON_TELEMETRY_DATA_ON_END(TRUE, GB_TELEMETRY_END_WON)
					SET_LOCAL_PLAYER_END_REASON(GB_TELEMETRY_END_WON)
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_IsWinner)
				ELSE
					IF GET_PLAYER_CASH_COLLECTED(PARTICIPANT_ID_TO_INT()) > 0
						DO_BIG_MESSAGE(1)
					ELSE
						DO_BIG_MESSAGE(4)
					ENDIF
					GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_CHAL_ROB_SHOP, FALSE, sRewardsData)
					GANG_BOSS_MANAGE_WANTED_CLEAN_UP(TRUE, INVALID_PLAYER_INDEX())
					GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST)
					SET_LOCAL_PLAYER_END_REASON(GB_TELEMETRY_END_LOST)
				ENDIF
				
				SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerBitSet, biP_RewardGiven)
				
				EXIT	// Exit to prevent processing further cases
			ENDIF
			
			// Standard completion, there was a winner
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_StandardWinner)
				IF PLAYER_ID() = GET_WINNER()
					DO_BIG_MESSAGE(2)
					GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_CHAL_ROB_SHOP, TRUE, sRewardsData)
					GANG_BOSS_MANAGE_WANTED_CLEAN_UP(TRUE, INVALID_PLAYER_INDEX())
					GB_SET_COMMON_TELEMETRY_DATA_ON_END(TRUE, GB_TELEMETRY_END_WON)
					SET_LOCAL_PLAYER_END_REASON(GB_TELEMETRY_END_WON)
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_IsWinner)
				ELSE
//					IF GET_PLAYER_CASH_COLLECTED(PARTICIPANT_ID_TO_INT()) > 0
//						DO_BIG_MESSAGE(1)
//					ELSE
						DO_BIG_MESSAGE(4)
//					ENDIF
					GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_CHAL_ROB_SHOP, FALSE, sRewardsData)
					GANG_BOSS_MANAGE_WANTED_CLEAN_UP(TRUE, INVALID_PLAYER_INDEX())
					GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST)
					SET_LOCAL_PLAYER_END_REASON(GB_TELEMETRY_END_LOST)
				ENDIF
				
				SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerBitSet, biP_RewardGiven)
				
				EXIT	// Exit to prevent processing further cases
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC GIVE_END_CASH_TO_WINNER()
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ChallengeNoScore)
		IF serverBD.piWinner = PLAYER_ID()
		OR serverBD.piTiedWinner = PLAYER_ID()
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_EndCashGivenToBoss)
				INT i, iTotalCash
				
				REPEAT NUM_NETWORK_PLAYERS i
					IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, i))
						PARTICIPANT_INDEX PartID = INT_TO_NATIVE(PARTICIPANT_INDEX, i)
						PLAYER_INDEX PlayerID = NETWORK_GET_PLAYER_INDEX(PartID)
						
						IF IS_THIS_PLAYER_A_GANG_GOON(PlayerID)
						OR IS_THIS_PLAYER_THE_GANG_BOSS(PlayerID)
							IF GET_PLAYER_CASH_COLLECTED(i) > 0
								iTotalCash += GET_PLAYER_CASH_COLLECTED(i)
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
				
				IF iTotalCash > SNG_TUNABLE_MAX_COMBINED_TOTAL()
					iTotalCash = SNG_TUNABLE_MAX_COMBINED_TOTAL()
				ENDIF
				
				IF iTotalCash > 0
					IF IS_BIT_SET(serverBD.iServerBitSet, biS_ChallengeTied)
						sRewardsData.iExtraCash += (iTotalCash/2)
					ELSE
						sRewardsData.iExtraCash += iTotalCash
					ENDIF
					PRINTLN("[GB SMASH AND GRAB] [STMGB] - GIVE_END_CASH_TO_WINNER - sRewardsData.iExtraCash = $", iTotalCash)
				ELSE
					PRINTLN("[GB SMASH AND GRAB] [STMGB] - GIVE_END_CASH_TO_WINNER - iTotalCash = 0, no cash being given to boss")
				ENDIF
				
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_EndCashGivenToBoss)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//----------------------
//	MAINTAIN FUNCTIONS
//----------------------
PROC MAINTAIN_CHALLENGE_BOTTOM_RIGHT_UI()
	IF HAS_NET_TIMER_STARTED(serverBD.ChallengeDuration)
	
		INT iCombinedTotal = -1
		BOOL bForce = FALSE
		HUD_COLOURS timeColour
		
		iTimeRemaining = SNG_TUNABLE_GET_CHALLENGE_DURATION() - (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.ChallengeDuration))
		
		IF iTimeRemaining > 30000
			timeColour = HUD_COLOUR_WHITE
		ELSE
			timeColour = HUD_COLOUR_RED
		ENDIF
		
		IF iTimeRemaining <= 0
			iOvertime = iTimeRemaining
			iTimeRemaining = 0
		ENDIF
		
		IF serverBD.sSortedLeaderboard[0].playerID != INVALID_PLAYER_INDEX()
		AND serverBD.sSortedLeaderboard[0].iScore > 0
			IF serverBD.sSortedLeaderboard[1].playerID != INVALID_PLAYER_INDEX()
			AND serverBD.sSortedLeaderboard[1].iScore > 0
				DO_HELP_TEXT(5)
			ENDIF
		ENDIF
		
		IF serverBD.sSortedLeaderboard[0].iScore > 0
			iCombinedTotal += 1
			iCombinedTotal += serverBD.sSortedLeaderboard[0].iScore
		ENDIF
		IF serverBD.sSortedLeaderboard[1].iScore > 0
			iCombinedTotal += serverBD.sSortedLeaderboard[1].iScore
		ENDIF
		IF serverBD.sSortedLeaderboard[2].iScore > 0
			iCombinedTotal += serverBD.sSortedLeaderboard[2].iScore
		ENDIF
		IF serverBD.sSortedLeaderboard[3].iScore > 0
			iCombinedTotal += serverBD.sSortedLeaderboard[3].iScore
		ENDIF
		HUD_COLOURS MyScoreColour = HUD_COLOUR_PURE_WHITE
		IF GB_IS_THIS_PLAYER_USING_SPECTATE(PLAYER_ID())
			MyScoreColour = HUD_COLOUR_GREY
		ENDIF
		BOTTOM_RIGHT_UI_1STCASH_2NDCASH_3RDCASH_4THCASH_CASH1_CASH2_TIMER(	serverBD.sSortedLeaderboard[0].playerID,
																			serverBD.sSortedLeaderboard[1].playerID,
																			serverBD.sSortedLeaderboard[2].playerID,
																			serverBD.sSortedLeaderboard[3].playerID,
																			serverBD.sSortedLeaderboard[0].iScore,
																			serverBD.sSortedLeaderboard[1].iScore,
																			serverBD.sSortedLeaderboard[2].iScore,
																			serverBD.sSortedLeaderboard[3].iScore,
																			iCombinedTotal,
																			serverBD.leaderboardPlayerScore[PARTICIPANT_ID_TO_INT()],
																			iTimeRemaining, bForce, timeColour, "", "CARJCK_CSH1TTL", "GB_SNG_HT1",
																			DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, MyScoreColour)
	ENDIF
ENDPROC

PROC MAINTAIN_SMASH_AND_GRAB_SORTED_PARTICIPANTS()

	IF localSyncID = serverBD.iServerSyncID
		EXIT
	ENDIF
	
	g_GBLeaderboardStruct.dpadVariables.eDpadVarType = DPAD_VAR_CASH
	
	INT i
	REPEAT NUM_MINI_LEADERBOARD_PLAYERS i
		IF serverBD.sSortedLeaderboard[i].playerID != INVALID_PLAYER_INDEX()
			g_GBLeaderboardStruct.challengeLbdStruct[i].playerID = serverBD.sSortedLeaderboard[i].playerID
			g_GBLeaderboardStruct.challengeLbdStruct[i].iScore = serverBD.sSortedLeaderboard[i].iScore
		ENDIF
	ENDREPEAT
	
//	IF iMyLastScore != GET_PLAYER_CASH_COLLECTED(PARTICIPANT_ID_TO_INT())
//		PLAY_SOUND_FRONTEND(-1,)
//		iMyLastScore = GET_PLAYER_CASH_COLLECTED(PARTICIPANT_ID_TO_INT())
//	ENDIF
	
	IF g_GBLeaderboardStruct.challengeLbdStruct[0].playerID = PLAYER_ID()
		IF iMyLastPosition != 0 // If I've moved up leaderboard
		OR localSyncID = 0		// Or i was first to collect
			PLAY_SOUND_FRONTEND(-1, "Enter_1st", "GTAO_Magnate_Boss_Modes_Soundset", FALSE)
			iMyLastPosition = 0
		ENDIF
	ELSE
		IF iMyLastPosition = 0
			PLAY_SOUND_FRONTEND(-1, "Lose_1st", "GTAO_Magnate_Boss_Modes_Soundset", FALSE)
			iMyLastPosition = -1
		ENDIF
	ENDIF
	
	// Sync us with the server
	localSyncID = serverBD.iServerSyncID
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[SMASH AND GRAB LEADERBOARD UPDATE] [STMGB] [CLIENT] - MAINTAIN_PROTECTION_RACKET_SORTED_PARTICIPANTS - Performing a leaderboard update. LocalSyncID updated to ", localSyncID)
	INT iDebugLoop
	REPEAT GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS()	iDebugLoop
		IF g_GBLeaderboardStruct.challengeLbdStruct[iDebugLoop].iParticipant > -1
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(g_GBLeaderboardStruct.challengeLbdStruct[iDebugLoop].iParticipant))
				PRINTLN("[SMASH AND GRAB LEADERBOARD UPDATE] [STMGB] [CLIENT] - MAINTAIN_PROTECTION_RACKET_SORTED_PARTICIPANTS - |--------------------------- >| ")
				PRINTLN("[SMASH AND GRAB LEADERBOARD UPDATE] [STMGB] [CLIENT] - MAINTAIN_PROTECTION_RACKET_SORTED_PARTICIPANTS - Position: ", iDebugLoop)
				PRINTLN("[SMASH AND GRAB LEADERBOARD UPDATE] [STMGB] [CLIENT] - MAINTAIN_PROTECTION_RACKET_SORTED_PARTICIPANTS - Player: ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(g_GBLeaderboardStruct.challengeLbdStruct[iDebugLoop].iParticipant))))
				PRINTLN("[SMASH AND GRAB LEADERBOARD UPDATE] [STMGB] [CLIENT] - MAINTAIN_PROTECTION_RACKET_SORTED_PARTICIPANTS - Score: ", g_GBLeaderboardStruct.challengeLbdStruct[iDebugLoop].iScore)
				PRINTLN("[SMASH AND GRAB LEADERBOARD UPDATE] [STMGB] [CLIENT] - MAINTAIN_PROTECTION_RACKET_SORTED_PARTICIPANTS - |--------------------------- |< ")
			ENDIF
		ENDIF
	ENDREPEAT
	#ENDIF

ENDPROC

PROC MAINTAIN_DPAD_LEADERBOARD()
	DRAW_CHALLENGE_DPAD_LBD(g_GBLeaderboardStruct.challengeLbdStruct, g_GBLeaderboardStruct.siDpadMovie, SUB_BOSS, g_GBLeaderboardStruct.dpadVariables, 
						g_GBLeaderboardStruct.fmDpadStruct)
ENDPROC

PROC MAINTAIN_BLIPS()
	SWITCH GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT())
		CASE eGBSNG_INIT
		CASE eGBSNG_STEAL_CASH
			IF bCoronaBlipsBlocked = FALSE
				BLOCK_ALL_MISSIONSATCOORDS_MISSIONS(TRUE)
				bCoronaBlipsBlocked = TRUE
			ENDIF
		BREAK
		
		CASE eGBSNG_END
			IF bCoronaBlipsBlocked
				BLOCK_ALL_MISSIONSATCOORDS_MISSIONS(FALSE)
				bCoronaBlipsBlocked = FALSE
			ENDIF
		BREAK
		
		CASE eGBSNG_CLEANUP
			
		BREAK
	ENDSWITCH
ENDPROC

PROC MAINTAIN_WANTED()
	IF SNG_TUNABLE_GET_WANTED_CAP() < 5
	AND GET_MAX_WANTED_LEVEL() != SNG_TUNABLE_GET_WANTED_CAP()
		SET_MAX_WANTED_LEVEL(SNG_TUNABLE_GET_WANTED_CAP())
	ELSE
		IF GET_MAX_WANTED_LEVEL() < 5
			SET_MAX_WANTED_LEVEL(5)
		ENDIF
	ENDIF
ENDPROC


FUNC BOOL SHOULD_DISPLAY_STORE_OBJECTIVE_TEXT()
	IF MPGlobalsAmbience.sSmashAndGrabStruct.bHoldupComplete
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC

FUNC BOOL SHOULD_DISPLAY_EMPTY_REGISTER_OBJECTIVE_TEXT()
	RETURN MPGlobalsAmbience.sSmashAndGrabStruct.bShouldEmptyRegister
ENDFUNC

PROC MAINTAIN_OBJECTIVE_TEXT()
	SWITCH GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT())
		CASE eGBSNG_INIT
		BREAK
		
		CASE eGBSNG_STEAL_CASH
			SWITCH GET_CLIENT_STEAL_CASH_SUBSTAGE(PARTICIPANT_ID_TO_INT())
				CASE eSCS_IDLE
					IF GET_PLAYER_CASH_COLLECTED(PARTICIPANT_ID_TO_INT()) = 0
						IF NOT HAS_THIS_MP_OBJECTIVE_TEXT_BEEN_RECEIVED("GB_SNG_OT0")
							CLEAR_ANY_OBJECTIVE_TEXT_FROM_THIS_SCRIPT()
							PRINT_OBJECTIVE_TEXT("GB_SNG_OT0")
						ENDIF
					ELSE
						IF NOT HAS_THIS_MP_OBJECTIVE_TEXT_BEEN_RECEIVED("GB_SNG_OT3")
							CLEAR_ANY_OBJECTIVE_TEXT_FROM_THIS_SCRIPT()
							PRINT_OBJECTIVE_TEXT("GB_SNG_OT3")
						ENDIF
					ENDIF
				BREAK
				
				CASE eSCS_HOLDUP
					IF SHOULD_DISPLAY_STORE_OBJECTIVE_TEXT()
						IF IS_THIS_A_MANUAL_HOLDUP()
							IF NOT HAS_THIS_MP_OBJECTIVE_TEXT_BEEN_RECEIVED("GB_SNG_OT2")
								CLEAR_ANY_OBJECTIVE_TEXT_FROM_THIS_SCRIPT()
								PRINT_OBJECTIVE_TEXT("GB_SNG_OT2")
							ENDIF
						ELSE
							IF NOT HAS_THIS_MP_OBJECTIVE_TEXT_BEEN_RECEIVED("GB_SNG_OT1")
								CLEAR_ANY_OBJECTIVE_TEXT_FROM_THIS_SCRIPT()
								PRINT_OBJECTIVE_TEXT("GB_SNG_OT1")
							ENDIF
						ENDIF
					ELSE
						IF NOT HAS_THIS_MP_OBJECTIVE_TEXT_BEEN_RECEIVED("GB_SNG_OT3")
							CLEAR_ANY_OBJECTIVE_TEXT_FROM_THIS_SCRIPT()
							PRINT_OBJECTIVE_TEXT("GB_SNG_OT3")
						ENDIF
					ENDIF
				BREAK
			
			ENDSWITCH
		BREAK
		
		CASE eGBSNG_END
			IF IS_THERE_ANY_CURRENT_OBJECTIVE_TEXT_FROM_THIS_SCRIPT()
				CLEAR_ANY_OBJECTIVE_TEXT_FROM_THIS_SCRIPT()
			ENDIF
		BREAK
		
		CASE eGBSNG_CLEANUP
			IF IS_THERE_ANY_CURRENT_OBJECTIVE_TEXT_FROM_THIS_SCRIPT()
				CLEAR_ANY_OBJECTIVE_TEXT_FROM_THIS_SCRIPT()
			ENDIF
		BREAK
	
	ENDSWITCH
ENDPROC

PROC MAINTAIN_CHANGE_STAGE_CHECKS()
	IF GET_SERVER_MISSION_STAGE() = eGBSNG_STEAL_CASH
		IF GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT()) != eGBSNG_STEAL_CASH
			
			// Initialise event start leading into gameplay stage
			GB_SET_LOCAL_PLAYER_CRITICAL_TO_JOB(TRUE)			
			SET_FREEMODE_FLOW_COMMS_DISABLED(TRUE)
			GB_SET_COMMON_TELEMETRY_DATA_ON_START()
			g_sGb_Telemetry_data.sdata.m_timeStart = GET_CLOUD_TIME_AS_INT()
			
			SET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT(), eGBSNG_STEAL_CASH)
		ENDIF
	ELIF GET_SERVER_MISSION_STAGE() = eGBSNG_END
		IF GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT()) != eGBSNG_END
		
			GB_SET_LOCAL_PLAYER_CRITICAL_TO_JOB(FALSE)
			g_sGb_Telemetry_data.sdata.m_timeEnd = GET_CLOUD_TIME_AS_INT()
		
			SET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT(), eGBSNG_END)
		ENDIF
	ELIF GET_SERVER_MISSION_STAGE() = eGBSNG_CLEANUP
		IF GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT()) != eGBSNG_CLEANUP
		
			GB_SET_LOCAL_PLAYER_CRITICAL_TO_JOB(FALSE)
		
			SET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT(), eGBSNG_CLEANUP)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_COUNTDOWN()
	IF HAS_NET_TIMER_STARTED(serverBD.ChallengeDuration)
		IF iTimeRemaining  <= 35000
			SWITCH eFinalCountdownStage
				CASE eFCOUNTDOWN_PREP
					IF GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT()) = eGBSNG_STEAL_CASH
						// IF within a second of hitting the end prep period
						IF iTimeRemaining <= (35000)
						AND iTimeRemaining >= (33000)
							IF PREPARE_MUSIC_EVENT("APT_PRE_COUNTDOWN_STOP")
								PRINTLN("[GB SMASH AND GRAB] [STMGB] - MAINTAIN_YACHT_ATTACK_MUSIC - eFCOUNTDOWN_PREP - APT_PRE_COUNTDOWN_STOP has been triggered")
								SET_AUDIO_FLAG("DisableFlightMusic", TRUE)
								TRIGGER_MUSIC_EVENT("APT_PRE_COUNTDOWN_STOP")
								eFinalCountdownStage = eFCOUNTDOWN_START
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE eFCOUNTDOWN_START
					IF NOT IS_BIT_SET(iFinalCountdownBitSet, iFCBS_TriggeredCountdown)
						IF NOT IS_BIT_SET(iFinalCountdownBitSet, iFCBS_SuppressWanted)
							IF iTimeRemaining <= 33000
							AND iTimeRemaining > 30000
								SET_AUDIO_FLAG("WantedMusicDisabled", TRUE)
								PRINTLN("[GB SMASH AND GRAB] [STMGB] - MAINTAIN_YACHT_ATTACK_MUSIC - eFCOUNTDOWN_START - suppressing wanted music")
								SET_BIT(iFinalCountdownBitSet, iFCBS_SuppressWanted)
							ENDIF
						ENDIF
						IF iTimeRemaining <= 30000
							IF PREPARE_MUSIC_EVENT("APT_COUNTDOWN_30S")
								SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE)
								SET_USER_RADIO_CONTROL_ENABLED(FALSE)
								TRIGGER_MUSIC_EVENT("APT_COUNTDOWN_30S")
								PREPARE_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
								PRINTLN("[GB SMASH AND GRAB] [STMGB] - MAINTAIN_YACHT_ATTACK_MUSIC - eFCOUNTDOWN_START - APT_COUNTDOWN_30S has been triggered")
								SET_BIT(iFinalCountdownBitSet, iFCBS_TriggeredCountdown)
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_BIT_SET(iFinalCountdownBitSet, iFCBS_ReEnabledRadioControl)
							IF iTimeRemaining <= 27000
								SET_USER_RADIO_CONTROL_ENABLED(TRUE)
								SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
								PRINTLN("[GB SMASH AND GRAB] [STMGB] - MAINTAIN_YACHT_ATTACK_MUSIC - eFCOUNTDOWN_START - Radio control re-enabled")
								SET_BIT(iFinalCountdownBitSet, iFCBS_ReEnabledRadioControl)
								eFinalCountdownStage = eFCOUNTDOWN_END_OR_KILL
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE eFCOUNTDOWN_END_OR_KILL
					IF NOT IS_BIT_SET(iFinalCountdownBitSet, iFCBS_TriggeredCountdownKill)
						IF NOT IS_BIT_SET(iFinalCountdownBitSet, iFCBS_Trigger5s)
							IF iTimeRemaining <= 5000
								PLAY_SOUND_FRONTEND(-1, "5s", "MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
								SET_BIT(iFinalCountdownBitSet, iFCBS_Trigger5s)
							ENDIF
						ENDIF
					
						IF iTimeRemaining <= 0
							IF PREPARE_MUSIC_EVENT("APT_FADE_IN_RADIO")
								TRIGGER_MUSIC_EVENT("APT_FADE_IN_RADIO")
								CANCEL_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
								PRINTLN("[GB SMASH AND GRAB] [STMGB] - MAINTAIN_YACHT_ATTACK_MUSIC - eFCOUNTDOWN_END_OR_KILL - Time ran out, APT_FADE_IN_RADIO has been triggered")
								SET_BIT(iFinalCountdownBitSet, iFCBS_TriggeredCountdownKill)
							ENDIF
						ELSE
							IF IS_BIT_SET(serverBD.iServerBitSet, biS_BossLeft)
								IF PREPARE_MUSIC_EVENT("APT_FADE_IN_RADIO")
									CANCEL_MUSIC_EVENT("APT_COUNTDOWN_30S")
									TRIGGER_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
									TRIGGER_MUSIC_EVENT("APT_FADE_IN_RADIO")
									PRINTLN("[GB SMASH AND GRAB] [STMGB] - MAINTAIN_YACHT_ATTACK_MUSIC - eFCOUNTDOWN_END_OR_KILL - Mode ended before timeout, APT_COUNTDOWN_30S_KILL and APT_FADE_IN_RADIO have been triggered")
									SET_BIT(iFinalCountdownBitSet, iFCBS_TriggeredCountdownKill)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF IS_BIT_SET(iFinalCountdownBitSet, iFCBS_SuppressWanted)
						IF iOvertime <= -2000
							SET_AUDIO_FLAG("WantedMusicDisabled", FALSE)
							SET_AUDIO_FLAG("DisableFlightMusic", FALSE)
							PRINTLN("[GB SMASH AND GRAB] [STMGB] - MAINTAIN_YACHT_ATTACK_MUSIC - eFCOUNTDOWN_END_OR_KILL - unsuppressing wanted music")
							CLEAR_BIT(iFinalCountdownBitSet, iFCBS_SuppressWanted)
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

//----------------------
//	CORE FUNCTIONS
//----------------------

PROC PROCESS_CLIENT()

	MAINTAIN_SMASH_AND_GRAB_SORTED_PARTICIPANTS()
	MAINTAIN_DPAD_LEADERBOARD()
	MAINTAIN_COUNTDOWN()

	MAINTAIN_BLIPS()
	MAINTAIN_OBJECTIVE_TEXT()
	MAINTAIN_CHANGE_STAGE_CHECKS()

	SWITCH GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT())
	
		CASE eGBSNG_INIT
			// dont need to do anything here
		BREAK
	
		CASE eGBSNG_STEAL_CASH
			IF MPGlobalsAmbience.sSmashAndGrabStruct.bOnSmashGrab = FALSE
				MPGlobalsAmbience.sSmashAndGrabStruct.bOnSmashGrab = TRUE
			ENDIF
			DO_BIG_MESSAGE(0)
			PROCESS_CHALLENGE()
			MAINTAIN_WANTED()
			MAINTAIN_CHALLENGE_BOTTOM_RIGHT_UI()
		BREAK 
		
		CASE eGBSNG_END
			MAINTAIN_CHALLENGE_BOTTOM_RIGHT_UI()
			IF IS_BIT_SET(g_GB_WORK_VARS.iEventBitSet, ciGB_BOSS_SHARD_SAFE_TO_DISPLAY)
				GIVE_END_CASH_TO_WINNER()
				PROCESS_CHALLENGE_END_REWARDS_AND_MESSAGES()
			ENDIF
			IF GB_MAINTAIN_BOSS_END_UI(sEndUI)
				SET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT(), GAME_STATE_END)
			ENDIF
		BREAK

		CASE eGBSNG_CLEANUP
			IF MPGlobalsAmbience.sSmashAndGrabStruct.bOnSmashGrab = TRUE
				MPGlobalsAmbience.sSmashAndGrabStruct.bOnSmashGrab = FALSE
			ENDIF
		BREAK
		
	ENDSWITCH

ENDPROC

// -------
// Leaderboard
// -------

PROC SORT_SMASH_AND_GRAB_PLAYER_SCORES(PLAYER_INDEX playerID, INT iPlayerScore)
	SORT_AMBIENT_ACTIVITY_LEADERBOARD(serverBD.sSortedLeaderboard, playerID, iPlayerScore, AAL_SORT_NON_UNIFORM_SCORING)
	
	serverBD.iServerSyncID++
ENDPROC

PROC HANDLE_PLAYER_LEAVING_LEADERBOARD_UPDATE()
	IF MAINTAIN_PLAYER_LEAVING_AMBIENT_ACTIVITY_LEADERBOARD(serverBD.sSortedLeaderboard, serverStaggeredPlayerLoop, AAL_SORT_NON_UNIFORM_SCORING)
		serverBD.iServerSyncId++
	ENDIF
ENDPROC

PROC HANDLE_LEADERBOARD_UPDATES()
	INT iLeaderboardStagger
	REPEAT NUM_NETWORK_PLAYERS iLeaderboardStagger
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iLeaderboardStagger))
			PLAYER_INDEX PlayerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iLeaderboardStagger))
			IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PlayerID, GET_GANG_BOSS_PLAYER_INDEX())
				IF serverBD.leaderboardPlayerScore[iLeaderboardStagger] != GET_PLAYER_CASH_COLLECTED(iLeaderboardStagger)
					PRINTLN("[GB SMASH AND GRAB] [STMGB] [SERVER] - HANDLE_LEADERBOARD_UPDATES - Player ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iLeaderboardStagger))), "'s score has changed, updating leaderboard")
					SORT_SMASH_AND_GRAB_PLAYER_SCORES(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iLeaderboardStagger)), GET_PLAYER_CASH_COLLECTED(iLeaderboardStagger))
					serverBD.leaderboardPlayerScore[iLeaderboardStagger] = GET_PLAYER_CASH_COLLECTED(iLeaderboardStagger)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC PROCESS_DAMAGE_EVENT(INT iCount)

	PED_INDEX ped
	PLAYER_INDEX player

	STRUCT_ENTITY_DAMAGE_EVENT data
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, data, SIZE_OF(data))
		IF DOES_ENTITY_EXIST(data.VictimIndex)
			IF IS_ENTITY_A_PED(data.VictimIndex)
				ped = GET_PED_INDEX_FROM_ENTITY_INDEX(data.VictimIndex)
				IF IS_PED_A_PLAYER(ped)
					player = NETWORK_GET_PLAYER_INDEX_FROM_PED(ped)
					IF IS_THIS_PLAYER_THE_GANG_BOSS(player)
						IF data.VictimDestroyed
							serverBD.iBossDeathCount++
							PRINTLN("[GB PROTECTION RACKET] [STMGB] - PROCESS_DAMAGE_EVENT - boss has been killed, iBossDeathCount = ", serverBD.iBossDeathCount)
						ENDIF
					ELIF IS_THIS_PLAYER_A_GANG_GOON(player)
						IF data.VictimDestroyed
							serverBD.iGoonDeathCount++
							PRINTLN("[GB PROTECTION RACKET] [STMGB] - PROCESS_DAMAGE_EVENT - a goon has been killed, iGoonDeathCount = ", serverBD.iGoonDeathCount)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_PLAYER_JOIN(INT iCount)
	
	STRUCT_PLAYER_SCRIPT_EVENTS data 
	
	GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, data, SIZE_OF(data))

	// Check player is valid
	IF data.PlayerIndex != INVALID_PLAYER_INDEX()

		// If we haven't yet had knowledge of this player then loop over threads ensuring they have joined this script
		IF NOT IS_BIT_SET(serverBD.iPlayerBitSet, NATIVE_TO_INT(data.PlayerIndex))
			INT iThread
			REPEAT data.NumThreads iThread

				IF data.Threads[iThread] = GET_ID_OF_THIS_THREAD()
					
					SORT_SMASH_AND_GRAB_PLAYER_SCORES(data.PlayerIndex, 0)	
					
					SET_BIT(serverBD.iPlayerBitSet, NATIVE_TO_INT(data.PlayerIndex))
				ENDIF       
			ENDREPEAT
		ENDIF
	ENDIF
ENDPROC

PROC SERVER_PROCESS_EVENTS()
	INT iCount
	EVENT_NAMES ThisScriptEvent
	
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount
	
		ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)
		
		IF ThisScriptEvent = EVENT_NETWORK_DAMAGE_ENTITY
			PROCESS_DAMAGE_EVENT(iCount)
		ENDIF
			
		IF ThisScriptEvent = EVENT_NETWORK_PLAYER_JOIN_SCRIPT
			PROCESS_PLAYER_JOIN(iCount)
		ENDIF
		
	ENDREPEAT
ENDPROC


PROC HANDLE_BOSS_LEAVE_SESSION()
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_BossLeft)
		IF NOT NETWORK_IS_PLAYER_ACTIVE(GET_GANG_BOSS_PLAYER_INDEX())
			SET_BIT(serverBD.iServerBitSet, biS_BossLeft)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DETERMINE_WINNER()
	INT i
	INT iTempHighscore = 0
	REPEAT NUM_NETWORK_PLAYERS i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			PLAYER_INDEX Player = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(Player, GET_GANG_BOSS_PLAYER_INDEX())
				IF GET_PLAYER_CASH_COLLECTED(i) > iTempHighscore		// New winner
					iTempHighscore = GET_PLAYER_CASH_COLLECTED(i)
					serverBD.piWinner = Player
					serverBD.piTiedWinner = INVALID_PLAYER_INDEX()
				ELIF GET_PLAYER_CASH_COLLECTED(i) = iTempHighscore		// Winner has same highscore that isnt 0
				AND iTempHighscore > 0
					serverBD.piTiedWinner = serverBD.piWinner
					serverBD.piWinner = Player					
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
		
	// No one scored any cash
	IF iTempHighscore = 0
		PRINTLN("[GB SMASH AND GRAB] [STMGB] - DETERMINE_WINNER - biS_ChallengeNoScore set - no players returned more than $0")
		SET_BIT(serverBD.iServerBitSet, biS_ChallengeNoScore)
	ELIF (iTempHighscore > 0) AND (serverBD.piWinner != INVALID_PLAYER_INDEX())	AND (serverBD.piTiedWinner != INVALID_PLAYER_INDEX())
		PRINTLN("[GB SMASH AND GRAB] [STMGB] - DETERMINE_WINNER - biS_ChallengeTied - two players returned the same amount of cash, they were ", GET_PLAYER_NAME(GET_WINNER()), " and ", GET_PLAYER_NAME(GET_TIED_WINNER()))
		SET_BIT(serverBD.iServerBitSet, biS_ChallengeTied)
	ELSE
		PRINTLN("[GB SMASH AND GRAB] [STMGB] - DETERMINE_WINNER - biS_StandardWinner - there was a winner, they were ", GET_PLAYER_NAME(GET_WINNER()))
		SET_BIT(serverBD.iServerBitSet, biS_StandardWinner)
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_MOVE_TO_CHALLENGE_END()

	#IF IS_DEBUG_BUILD
	IF bExpireTimer
		PRINTLN("[GB SMASH AND GRAB] [STMGB] - SHOULD_MOVE_TO_CHALLENGE_END - DEBUG - bExpireTimer is TRUE, debug skipped to end of timer.")
		RETURN TRUE
	ENDIF
	#ENDIF
		
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_BossLeft)
		PRINTLN("[GB SMASH AND GRAB] [STMGB] - SHOULD_MOVE_TO_CHALLENGE_END - biS_BossLeft is SET, boss has left the session")
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC PROCESS_SERVER()

	SERVER_PROCESS_EVENTS()
	HANDLE_BOSS_LEAVE_SESSION()
		
	SWITCH GET_SERVER_MISSION_STAGE()
	
		CASE eGBSNG_INIT
		BREAK
	
		CASE eGBSNG_STEAL_CASH
			IF NOT HAS_NET_TIMER_STARTED(serverBD.ChallengeDuration)
				PRINTLN("[GB SMASH AND GRAB] [STMGB] - PROCESS_SERVER - ChallengeDuration timer has been started")
				START_NET_TIMER(serverBD.ChallengeDuration)
			ELSE
				HANDLE_PLAYER_LEAVING_LEADERBOARD_UPDATE()
				HANDLE_LEADERBOARD_UPDATES()
			
				IF HAS_NET_TIMER_EXPIRED(serverBD.ChallengeDuration, SNG_TUNABLE_GET_CHALLENGE_DURATION())
				OR SHOULD_MOVE_TO_CHALLENGE_END()
					IF DETERMINE_WINNER()
						SET_BIT(serverBD.iServerBitSet, biS_ChallengeEnded)
						PRINTLN("[GB SMASH AND GRAB] [STMGB] - PROCESS_SERVER - ChallengeDuration timer has expired")
						SET_SERVER_MISSION_STAGE(eGBSNG_END)
					ENDIF
				ENDIF
			ENDIF
		BREAK 
		
		CASE eGBSNG_END
//			IF NOT HAS_NET_TIMER_STARTED(serverBD.ChallengeEnd)
//				PRINTLN("[GB SMASH AND GRAB] [STMGB] - PROCESS_SERVER - ChallengeEnd timer has been started")
//				START_NET_TIMER(serverBD.ChallengeEnd)
//			ELSE
//				IF HAS_NET_TIMER_EXPIRED(serverBD.ChallengeEnd, TWELVE_SECONDS)
//					PRINTLN("[GB SMASH AND GRAB] [STMGB] - PROCESS_SERVER - ChallengeEnd timer has expired")
//					SET_SERVER_MISSION_STAGE(eGBSNG_CLEANUP)
//				ENDIF
//			ENDIF
		BREAK

		CASE eGBSNG_CLEANUP
			SET_SERVER_MISSION_STATE(GAME_STATE_END)
		BREAK

	ENDSWITCH

ENDPROC

FUNC BOOL INIT_SERVER()

	PRINTLN("[GB SMASH AND GRAB] [STMGB] -------------------------------- INIT SERVER START -------------------------------- ")
	
	INT iPlayer
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		serverBD.leaderboardPlayerScore[iPlayer] = -1
	ENDREPEAT
	
	GB_COMMON_BOSS_MISSION_SETUP(FMMC_TYPE_GB_CHAL_ROB_SHOP)

	SET_GANG_BOSS_PLAYER_INDEX(PLAYER_ID())
	
	// Initialise the leaderboard
	INITIALISE_AMBIENT_ACTIVITY_LEADERBOARD(serverBD.sSortedLeaderboard)
	
	// Add host as player 1
	SORT_SMASH_AND_GRAB_PLAYER_SCORES(PLAYER_ID(), 0)
	
	PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBD.iMatchId1, serverBD.iMatchId2)
	
	PRINTLN("[GB SMASH AND GRAB] [STMGB] -------------------------------- INIT SERVER END -------------------------------- ")

	RETURN TRUE
ENDFUNC

FUNC BOOL INIT_CLIENT()
	GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
	GB_COMMON_BOSS_MISSION_SETUP(FMMC_TYPE_GB_CHAL_ROB_SHOP)
	RESET_SMASH_AND_GRAB_GLOBALS()
		
	RETURN TRUE
ENDFUNC

PROC HANDLE_CLEANUP_TELEMETRY()

	IF NETWORK_IS_PARTICIPANT_ACTIVE(PARTICIPANT_ID())
		// If I've not reached the end of the event and we've not cleaned up due to low numbers, then I've left
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_RewardGiven)
		AND bLowNumbers = FALSE
		AND serverBD.iServerEndReason = -1
			IF NOT NETWORK_IS_PLAYER_ACTIVE(GET_GANG_BOSS_PLAYER_INDEX())
				GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_BOSS_LEFT)
			ELSE
				GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LEFT)
			ENDIF
		ENDIF
		
		PROCESS_CURRENT_BOSS_CHALLENGE_PLAYSTATS(	g_sGb_Telemetry_data.sdata, 
													sSmashAndGrabTelemetryData.iStoresHeldUp, 
													sSmashAndGrabTelemetryData.iDeliveriesMade,
													sSmashAndGrabTelemetryData.iCashLost,
													sSmashAndGrabTelemetryData.iCashCollected)
	ENDIF
ENDPROC

FUNC BOOL IS_SAFE_TO_CLEANUP()
	IF AM_I_THE_GANG_BOSS()
		SWITCH iCleanupBossSpecStep
			CASE 0
				IF GB_SHOULD_RUN_SPEC_CAM()
					PRINTLN("[GB SMASH AND GRAB] [STMGB] - IS_SAFE_TO_CLEANUP - Boss - Player is spectating, calling GB_SET_SHOULD_CLOSE_SPECTATE, moving to next stage")
					GB_SET_SHOULD_CLOSE_SPECTATE()
					iCleanupBossSpecStep = 1
				ELSE
					iCleanupBossSpecStep = 2
				ENDIF
			BREAK
			
			CASE 1
				IF NOT GB_SHOULD_RUN_SPEC_CAM()
					PRINTLN("[GB SMASH AND GRAB] [STMGB] - IS_SAFE_TO_CLEANUP - Boss - NOT GB_SET_SHOULD_CLOSE_SPECTATE, moving to next stage")
					iCleanupBossSpecStep = 2
				ENDIF
			BREAK
			
			CASE 2
				PRINTLN("[GB SMASH AND GRAB] [STMGB] - IS_SAFE_TO_CLEANUP - Boss - Return true")
				RETURN TRUE
			BREAK
		ENDSWITCH
	ELSE
		PRINTLN("[GB SMASH AND GRAB] [STMGB] - IS_SAFE_TO_CLEANUP - Goon - Return true")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SCRIPT_CLEANUP(BOOL bImmediately = FALSE)
	IF bImmediately
	OR IS_SAFE_TO_CLEANUP()
		PRINTLN("[GB SMASH AND GRAB] [STMGB] - SCRIPT_CLEANUP - initiated.")
		
		RESET_SMASH_AND_GRAB_GLOBALS(TRUE)
		MPGlobalsAmbience.sSmashAndGrabStruct.bOnSmashGrab = FALSE
		
		g_sGb_Telemetry_data.sdata.m_match1 = serverBD.iMatchId1
		g_sGb_Telemetry_data.sdata.m_match2 = serverBD.iMatchId2
		HANDLE_CLEANUP_TELEMETRY()
		
		BLOCK_ALL_MISSIONSATCOORDS_MISSIONS(FALSE)
		
		GB_TIDYUP_SPECTATOR_CAM()
		//RELEASE_CONTEXT_INTENTION(sSpecVars.iGbSpecContext)
		
		GB_COMMON_BOSS_MISSION_CLEANUP()
		
				
		SET_FREEMODE_FLOW_COMMS_DISABLED(FALSE)
		
		TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
	ENDIF
ENDPROC


FUNC BOOL IS_LOCAL_PLAYER_NO_LONGER_IN_GANG()
	IF playerBD[PARTICIPANT_ID_TO_INT()].iPlayerGameState > GAME_STATE_INIT
		IF serverBD.piGangBoss != INVALID_PLAYER_INDEX()
			IF NOT GB_IS_LOCAL_PLAYER_MEMBER_OF_THIS_GANG(serverBD.piGangBoss)
				PRINTLN("[GB SMASH AND GRAB] [STMGB] - IS_LOCAL_PLAYER_NO_LONGER_IN_GANG - Yes - Local player is not in the gang")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_CLEANUP_CHECKS()

	IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		PRINTLN("[GB SMASH AND GRAB] [STMGB] - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE - TRUE, calling script cleanup")
		SCRIPT_CLEANUP(TRUE)
	ENDIF
	
	IF GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION()
		PRINTLN("[GB SMASH AND GRAB] [STMGB] - GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION - True, calling script cleanup")
		SCRIPT_CLEANUP(TRUE)
	ENDIF
	
	IF IS_LOCAL_PLAYER_NO_LONGER_IN_GANG()
		PRINTLN("[GB SMASH AND GRAB] [STMGB] - IS_LOCAL_PLAYER_NO_LONGER_IN_GANG - TRUE, calling script cleanup")
		GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_CHAL_ROB_SHOP, FALSE, sRewardsData)
		SCRIPT_CLEANUP()
	ENDIF
	
	IF GET_GANG_BOSS_PLAYER_INDEX() != INVALID_PLAYER_INDEX()
		IF GB_SHOULD_BAIL_BOSS_CHALLENGE_DUE_TO_NO_OR_LOW_GOONS(GET_GANG_BOSS_PLAYER_INDEX())
			bLowNumbers = TRUE
			GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOW_NUMBERS)
			PRINTLN("[GB SMASH AND GRAB] - GB_SHOULD_BAIL_BOSS_CHALLENGE_DUE_TO_NO_OR_LOW_GOONS - TRUE, calling script cleanup")
			SCRIPT_CLEANUP()
		ENDIF
	ENDIF

ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Pre Game
// -----------------------------------------------------------------------------------------------------------

FUNC BOOL PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)

	PRINTLN("[GB SMASH AND GRAB] [STMGB] - PROCESS_PRE_GAME - Rob Shop Challenge is preparing to run.")
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission), missionScriptArgs)
	
	//Reserve Entities
	RESERVE_NETWORK_MISSION_OBJECTS(0)
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD,SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD,SIZE_OF(playerBD))
	
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		PRINTLN("[GB SMASH AND GRAB] [STMGB] - PROCESS_PRE_GAME - FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP called.")
		SCRIPT_CLEANUP(TRUE)
	ENDIF
	
	GB_COMMON_BOSS_MISSION_PREGAME()
	
	RETURN TRUE
	
ENDFUNC

// -----------------------------------------------------------------------------------------------------------
//		Debug Functions
// -----------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD

PROC UPDATE_WIDGETS()
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF bHostEndMissionNow
			PRINTLN("[GB SMASH AND GRAB] [STMGB] - UPDATE_WIDGETS - DEBUG - bHostEndMissionNow is TRUE, killing challenge.")
			serverBD.iServerEndReason = GB_TELEMETRY_END_FORCED
			SET_SERVER_MISSION_STATE(GAME_STATE_END)
			bHostEndMissionNow = FALSE
		ENDIF
	ENDIF	
ENDPROC

PROC MAINTAIN_WIDGETS()
	INT iPlayer
	TEXT_LABEL_63 tl63

	SWITCH iWidgetState
	
		CASE 0
		
		SmashAndGrabWidget = START_WIDGET_GROUP("GB Smash and Grab")
			ADD_WIDGET_BOOL("Create Smash and Grab Widgets", bCreateWidgets)
		STOP_WIDGET_GROUP()
		
		iWidgetState++
		
		BREAK
		
		CASE 1
		
			IF bCreateWidgets
				
				CLEAR_CURRENT_WIDGET_GROUP(SmashAndGrabWidget)				
				DELETE_WIDGET_GROUP(SmashAndGrabWidget)
				
				SmashAndGrabWidget = START_WIDGET_GROUP("GB Smash and Grab") 
				
					ADD_WIDGET_BOOL("End Mission: Debug end no reason (immediate)", bHostEndMissionNow)
				
					// Gameplay debug, sever only.
					START_WIDGET_GROUP("Server Only Gameplay") 
						ADD_WIDGET_BOOL("End Mission: End Timer Hit", bExpireTimer)
					STOP_WIDGET_GROUP()
									
					// Data about the server
					START_WIDGET_GROUP("Server BD") 
						ADD_WIDGET_INT_SLIDER("S. Game state", serverBD.iServerGameState,-1, HIGHEST_INT,1)
					STOP_WIDGET_GROUP()	

					// Data about the clients. * = You.
					START_WIDGET_GROUP("Client BD") 
						REPEAT NUM_NETWORK_PLAYERS iPlayer
							tl63 = "Player "
							tl63 += iPlayer
							IF iPlayer = PARTICIPANT_ID_TO_INT()
								tl63 += "*"
							ENDIF
							START_WIDGET_GROUP(tl63)
								ADD_WIDGET_INT_SLIDER("Game state", playerBD[iPlayer].iPlayerGameState,-1, HIGHEST_INT,1)
							STOP_WIDGET_GROUP()
						ENDREPEAT
					STOP_WIDGET_GROUP()
				STOP_WIDGET_GROUP()
				
				PRINTLN("[GB SMASH AND GRAB] [STMGB] -  Created Widgets")
				
				iWidgetState++
			
			ENDIF
		
		BREAK
		
		CASE 2
			UPDATE_WIDGETS()
		BREAK
	
	ENDSWITCH
ENDPROC

PROC MAINTAIN_DEBUG()
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF GB_SHOULD_KILL_ACTIVE_BOSS_MISSION()
			PRINTLN("[GB SMASH AND GRAB] [STMGB] - MAINTAIN_DEBUG - GB_SHOULD_KILL_ACTIVE_BOSS_MISSION - moving server to cleanup.")
			serverBD.iServerEndReason = GB_TELEMETRY_END_FORCED
			SET_SERVER_MISSION_STATE(GAME_STATE_END)
		ENDIF
	ENDIF
ENDPROC

#ENDIF  // Debug
	// Gang Boss

//----------------------
//	MAIN SCRIPT
//----------------------

SCRIPT(MP_MISSION_DATA missionScriptArgs)


	
	PRINTLN("[GB SMASH AND GRAB] [STMGB] - GB_ROB_SHOP - challenge has launched.")
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NOT PROCESS_PRE_GAME(missionScriptArgs)
			SCRIPT_CLEANUP(TRUE)
		ENDIF	
	ENDIF
	
	WHILE (TRUE)
	
		MP_LOOP_WAIT_ZERO()
		
		MAINTAIN_CLEANUP_CHECKS()
		
		GB_MAINTAIN_SPECTATE(sSpecVars)
		
		#IF IS_DEBUG_BUILD
			MAINTAIN_WIDGETS()
			MAINTAIN_DEBUG()
		#ENDIF
		
		SWITCH(GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT()))
		
			CASE GAME_STATE_INIT
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					GB_SET_ITS_SAFE_FOR_SPEC_CAM()
					IF GB_LAUNCH_CHALLENGE_WITH_SPEC_CAM_IS_ACTIVE(sSpecVars)
						IF INIT_CLIENT()
							SET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT(),GAME_STATE_RUNNING)
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE GAME_STATE_RUNNING
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					PROCESS_CLIENT()
					
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					SET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT(),GAME_STATE_END)
				ENDIF
			BREAK
			
			CASE GAME_STATE_END
				SCRIPT_CLEANUP()
			BREAK
			
		ENDSWITCH
	
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
			SWITCH(GET_SERVER_MISSION_STATE())
			
				CASE GAME_STATE_INIT
					IF INIT_SERVER()
						SET_SERVER_MISSION_STATE(GAME_STATE_RUNNING)
					ENDIF
				BREAK
				
				CASE GAME_STATE_RUNNING
					PROCESS_SERVER()
				BREAK
				
				CASE GAME_STATE_END
				BREAK
				
			ENDSWITCH
		
		ENDIF
		
	ENDWHILE

	
	
// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT

