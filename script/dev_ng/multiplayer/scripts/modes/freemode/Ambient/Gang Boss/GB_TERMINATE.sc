//////////////////////////////////////////////////////////////////////////////////////////
// Name:        GB_TERMINATE.sc																//
// Description: Gang boss selects a target for their goons to kill						//
// Written by:  Ryan Elliott															//
// Date: 09/03/2015																		//
//////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "net_mission.sch"


//----------------------
//	INCLUDES
//----------------------
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "flow_public_core_override.sch"
USING "net_wait_zero.sch"
USING "net_mission.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"

USING "am_common_ui.sch"
USING "DM_Leaderboard.sch"
USING "net_gang_boss.sch"

//----------------------
//	**TEMP HARD CODED**
//----------------------

//----------------------
//	ENUM
//----------------------

ENUM GBTER_END_REASON
	GBTER_ENDREASON_KILL = 0,
	GBTER_ENDREASON_FLED,
	GBTER_ENDREASON_TIMEUP,
	GBTER_ENDREASON_BOSS_LEFT,
	
	GBTER_ENDREASON_MAX 	// Always keep last
ENDENUM

//----------------------
//	CONSTANTS
//----------------------
CONST_INT REWARD_TIMEOUT		10000

//----------------------
//	Local Bitset
//----------------------

INT iLocalBitset

CONST_INT GBTER_BS_SHARD_DONE				0
CONST_INT GBTER_BS_HELP0_DONE				1
CONST_INT GBTER_BS_INTRO_DONE				2
CONST_INT GBTER_BS_BLIP_UPDATED				3

//----------------------
//	STRUCT
//----------------------

//----------------------
//	VARIABLES
//----------------------

BOOL bSet

//----------------------
//	GAME STATE
//----------------------

CONST_INT GAME_STATE_INIT 							0
CONST_INT GAME_STATE_RUNNING						1
CONST_INT GAME_STATE_TERMINATE_DELAY				2
CONST_INT GAME_STATE_END							3

ENUM GBTER_RUN_STAGE
	GBTER_INIT = 0,
	GBTER_KILL,
	GBTER_REWARDS,
	GBTER_END,
	
	GBTER_MAX_STAGES			/* Always have at the end */

ENDENUM

//----------------------
//	BROADCAST VARIABLES
//----------------------

// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData

	INT iServerGameState = GAME_STATE_INIT
	
	INT iServerBitSet
	
	PLAYER_INDEX iGBTERTarget
	PLAYER_INDEX iGBTERKiller
	
	SCRIPT_TIMER stWorkTimer
	SCRIPT_TIMER stRewardTimeout
	
	GBTER_RUN_STAGE eGBTER_Stage = GBTER_INIT
	GBTER_END_REASON eGBTER_EndReason = GBTER_ENDREASON_KILL
	
	BOOL bTargetAbandonedClub = FALSE
	
ENDSTRUCT

//Server bitset
CONST_INT SERVER_BS_WORK_STARTED			0
CONST_INT SERVER_BS_TARGET_KILLED			1
CONST_INT SERVER_BS_TARGET_FLED				2
CONST_INT SERVER_BS_BOSS_LEFT				3
CONST_INT SERVER_BS_TIME_UP					4
CONST_INT SERVER_BS_REWARDS_GIVEN			5
CONST_INT SERVER_BS_SERVER_INITIALISED		6
CONST_INT SERVER_BS_KILLED_BY_HIT_SQUAD		7

ServerBroadcastData serverBD

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	
	INT iClientGameState = GAME_STATE_INIT
	
	int iClientBitSet
	
	GBTER_RUN_STAGE eGBTER_Stage = GBTER_INIT
	GBTER_END_REASON eGBTER_EndReason = GBTER_ENDREASON_KILL
	
ENDSTRUCT

//Client bitset

CONST_INT CLIENT_BS_WORK_STARTED			0
CONST_INT CLIENT_BS_LOCAL_KILLED_TARGET		1
CONST_INT CLIENT_BS_TIME_UP					2
CONST_INT CLIENT_BS_REWARDS_GIVEN			3
CONST_INT CLIENT_BS_KILLED_BY_HIT_SQUAD		4

PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

//----------------------
//	DBG FUNCTIONS
//----------------------

#IF IS_DEBUG_BUILD
INT iDebugTarget
BOOL bKillScript, bUseDebugValues

/// PURPOSE: Create widgets for GB_TERMINATE
PROC CREATE_WIDGETS()

	START_WIDGET_GROUP("Gang Boss Hit")
		START_WIDGET_GROUP("Set up Script (Gang Boss Only)")
			ADD_WIDGET_BOOL("Use Debug Values", bUseDebugValues)
			ADD_WIDGET_INT_SLIDER("Set Target", iDebugTarget, -1, NETWORK_GET_MAX_NUM_PARTICIPANTS(), 1)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Server Only")
			ADD_WIDGET_BOOL("Kill Script", bKillScript)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	
	CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE ===  [CREATE_WIDGETS] - Widgets created.")
ENDPROC

#ENDIF

//----------------------
//	TUNABLES
//----------------------

/// PURPOSE: 300000 - Get overall duration of the hit.
FUNC INT GET_TERMINATE_DURATION()
	IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
		RETURN 120000
	ELSE
		RETURN 300000
	ENDIF
ENDFUNC

//----------------------
//	GET/SET FUNCTIONS
//----------------------

FUNC STRING GET_MISSION_STAGE_STRING(GBTER_RUN_STAGE eState)

	SWITCH(eState)
		CASE GBTER_INIT						RETURN "GBTER_INIT"
		CASE GBTER_KILL						RETURN "GBTER_KILL"
		CASE GBTER_REWARDS 					RETURN "GBTER_REWARDS"
		CASE GBTER_END		 				RETURN "GBTER_END"
	ENDSWITCH

	RETURN "UNKNOWN STATE!"

ENDFUNC

FUNC STRING GET_MISSION_END_REASON_STRING(GBTER_END_REASON eEndReason)

	SWITCH(eEndReason)
		CASE GBTER_ENDREASON_KILL						RETURN "GBTER_ENDREASON_KILL"
		CASE GBTER_ENDREASON_FLED						RETURN "GBTER_ENDREASON_FLED"
		CASE GBTER_ENDREASON_TIMEUP 					RETURN "GBTER_ENDREASON_TIMEUP"
		CASE GBTER_ENDREASON_BOSS_LEFT		 			RETURN "GBTER_ENDREASON_BOSS_LEFT"
	ENDSWITCH

	RETURN "UNKNOWN END REASON!"

ENDFUNC

/// PURPOSE: Return server mission stage.
FUNC GBTER_RUN_STAGE GET_SERVER_MISSION_STAGE()
	RETURN serverBD.eGBTER_Stage
ENDFUNC

/// PURPOSE: SERVER ONLY: Set server mission stage.
PROC SET_SERVER_MISSION_STAGE(GBTER_RUN_STAGE eStage)
	CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE ===  [SET_SERVER_MISSION_STAGE] - serverBD.eGBTER_Stage = ", GET_MISSION_STAGE_STRING(eStage))
	serverBD.eGBTER_Stage = eStage
ENDPROC


/// PURPOSE: Get server mission end reason.
FUNC GBTER_END_REASON GET_SERVER_MISSION_END_REASON()
	RETURN serverBD.eGBTER_EndReason
ENDFUNC

/// PURPOSE: SERVER ONLY: Set server mission end reason.
/// GBTER_ENDREASON_KILL
/// GBTER_ENDREASON_FLED
/// GBTER_ENDREASON_TIMEUP
PROC SET_SERVER_MISSION_END_REASON(GBTER_END_REASON eGBEndReason)
	serverBD.eGBTER_EndReason = eGBEndReason
	CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE === [SET_SERVER_MISSION_END_REASON] - serverBD.eGBTER_EndReason = ", GET_MISSION_END_REASON_STRING(eGBEndReason))
ENDPROC


/// PURPOSE: Return server mission stage.
FUNC GBTER_RUN_STAGE GET_CLIENT_MISSION_STAGE()
	RETURN playerBD[PARTICIPANT_ID_TO_INT()].eGBTER_Stage
ENDFUNC

/// PURPOSE: SERVER ONLY: Set server mission stage.
PROC SET_CLIENT_MISSION_STAGE(GBTER_RUN_STAGE eStage)
	playerBD[PARTICIPANT_ID_TO_INT()].eGBTER_Stage = eStage
	CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE === [SET_CLIENT_MISSION_STAGE] - playerBD[", PARTICIPANT_ID_TO_INT(), "].iClientGameState = ", GET_MISSION_STAGE_STRING(eStage))
ENDPROC

/// PURPOSE: Return the hit target set by teh gang boss.
FUNC PLAYER_INDEX GBTER_GET_TARGET()
	RETURN serverBD.iGBTERTarget
ENDFUNC

/// PURPOSE: SERVER ONLY: Set the hit target set by the gang boss.
PROC GBTER_SET_TARGET(PLAYER_INDEX piTarget)
	serverBD.iGBTERTarget = piTarget
	IF piTarget = INVALID_PLAYER_INDEX()
		CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE === [GBTER_SET_TARGET] - serverBD.iGBTERTarget: INVALID_PLAYER_INDEX()")
	ELSE
		CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE === [GBTER_SET_TARGET] - serverBD.iGBTERTarget: ", GET_PLAYER_NAME(piTarget))
	ENDIF
ENDPROC


/// PURPOSE: Return the killer of the target.
FUNC PLAYER_INDEX GET_GBTER_KILLER()
	RETURN serverBD.iGBTERKiller
ENDFUNC

/// PURPOSE: SERVER ONLY: Set the killer of the target.
PROC SET_GBTER_KILLER(PLAYER_INDEX piKiller)
	serverBD.iGBTERKiller = piKiller
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------

PROC SCRIPT_CLEANUP()
	CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE === [SCRIPT_CLEANUP]")
	
	Clear_Any_Objective_Text_From_This_Script()
	
	IF IS_NET_PLAYER_OK(GBTER_GET_TARGET(), FALSE)
		SET_FIXED_BLIP_SCALE_FOR_PLAYER(GBTER_GET_TARGET(), 1.0, TRUE)
		BROADCAST_GB_SET_REMOTE_PLAYER_AS_TERMINATE_TARGET(SPECIFIC_PLAYER(GBTER_GET_TARGET()), FALSE)
	ENDIF
	g_MyTerminateTarget = INVALID_PLAYER_INDEX()
	GB_COMMON_BOSS_MISSION_CLEANUP()
	
	TERMINATE_THIS_THREAD()
ENDPROC

//----------------------
//	CHECKS
//----------------------

FUNC BOOL GB_SHOULD_EVENT_BE_HIDDEN()
	IF GB_SHOULD_HIDE_GANG_BOSS_EVENT(FMMC_TYPE_GB_TERMINATE)
	OR GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GB_SHOULD_UI_BE_HIDDEN()
	IF NOT MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
	OR GB_SHOULD_EVENT_BE_HIDDEN()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_MISSION_STATE_STRING(INT iState)

	SWITCH(iState)
		CASE GAME_STATE_INIT 				RETURN "GAME_STATE_INIT"
		CASE GAME_STATE_RUNNING 			RETURN "GAME_STATE_RUNNING"
		CASE GAME_STATE_TERMINATE_DELAY 	RETURN "GAME_STATE_TERMINATE_DELAY"
		CASE GAME_STATE_END 				RETURN "GAME_STATE_END"
	ENDSWITCH

	RETURN "UNKNOWN STATE!"

ENDFUNC

//Helper function to get a clients game/mission state
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iClientGameState
ENDFUNC

//Helper function to get the servers game/mission state
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC

//Helper function to set the servers game/mission state
PROC SET_SERVER_MISSION_STATE(INT iState)
	serverBD.iServerGameState = iState
	CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE === SET_SERVER_MISSION_STATE = ",GET_MISSION_STATE_STRING(iState))
ENDPROC

//----------------------
//	FUNCTIONS
//----------------------

/// PURPOSE: Initialise script with target and method of kill selected by gang boss.
FUNC BOOL INIT_GBTER()
	CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE === [INIT_GBTER] called...")
	
	IF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_SERVER_INITIALISED)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE === [INIT_GBTER] - SERVER_BS_SERVER_INITIALISED - Moving on.")
		RETURN TRUE
	ENDIF
	
	GBTER_SET_TARGET(INVALID_PLAYER_INDEX())
	
	IF MPGlobalsAmbience.sMagnateGangBossData.hitTargetPlayer <> INVALID_PLAYER_INDEX()
		CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE === [INIT_GBTER] - Target passed to script is ", GET_PLAYER_NAME(MPGlobalsAmbience.sMagnateGangBossData.hitTargetPlayer))
	ELSE
		CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE === [INIT_GBTER] - Target passed to script is INVALID_PLAYER_INDEX()")
		SET_SERVER_MISSION_STATE(GAME_STATE_END)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		// Set target and preferred kill method via RAG widget. Must be launched via M menu on the gang boss to use.
		IF bUseDebugValues
			MPGlobalsAmbience.sMagnateGangBossData.hitTargetPlayer = INT_TO_PLAYERINDEX(iDebugTarget)
		ENDIF
	#ENDIF
	
	// Check the target player is okay, and not part of the gang.
	// Player should no longer be part of the gang by this point if this is a gang termination hit.
	IF IS_NET_PLAYER_OK(MPGlobalsAmbience.sMagnateGangBossData.hitTargetPlayer, FALSE)
		IF NOT GB_IS_PLAYER_MEMBER_OF_THIS_GANG(MPGlobalsAmbience.sMagnateGangBossData.hitTargetPlayer, PLAYER_ID())
			GBTER_SET_TARGET(MPGlobalsAmbience.sMagnateGangBossData.hitTargetPlayer)
		
			CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE === [INIT_GBTER] - Target is set in script. Target: ", GET_PLAYER_NAME(GBTER_GET_TARGET()))
		ELSE
			CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE === [INIT_GBTER] - FAIL - GB_IS_PLAYER_MEMBER_OF_THIS_GANG - Target: ", GET_PLAYER_NAME(MPGlobalsAmbience.sMagnateGangBossData.hitTargetPlayer))
		ENDIF
	ELSE
		CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE === [INIT_GBTER] - FAIL - NET_PLAYER_OK = FALSE.")
	ENDIF

	// If data is okay, set ready for goons to come on script.
	IF GBTER_GET_TARGET() != INVALID_PLAYER_INDEX()
	
		IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
			IF GB_IS_GLOBAL_CLIENT_BIT1_SET(GBTER_GET_TARGET(), eGB_GLOBAL_CLIENT_BITSET_1_ABANDONED_BIKER_GANG)
				serverBD.bTargetAbandonedClub = TRUE
			ENDIF
		ENDIF
	
		// Reset gang boss globals now that we have valid data on script
		BROADCAST_GB_SET_REMOTE_PLAYER_AS_TERMINATE_TARGET(SPECIFIC_PLAYER(GBTER_GET_TARGET()), TRUE)
		MPGlobalsAmbience.sMagnateGangBossData.hitTargetPlayer = INVALID_PLAYER_INDEX()
		
		SET_BIT(serverBD.iServerBitSet, SERVER_BS_SERVER_INITIALISED)
		
		CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE ===  [INIT_GBTER] - PASS - Data OK. Continuing script.")
		RETURN TRUE
	ELSE
		IF GBTER_GET_TARGET() = INVALID_PLAYER_INDEX()
			CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE ===  [INIT_GBTER] - FAIL - GBTER_GET_TARGET() = INVALID_PLAYER_INDEX()")
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC HANDLE_SHARD_HELP_INTRO()
	IF GB_SHOULD_UI_BE_HIDDEN()
		EXIT
	ENDIF
	
	IF PLAYER_ID() != GB_GET_LOCAL_PLAYER_MISSION_HOST()
		IF IS_BIT_SET(iLocalBitset, GBTER_BS_SHARD_DONE)
			IF NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
				STRING strHelp = "GBTER_START"
				IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
					strHelp = "GBTER_BSTART"
					IF serverBD.bTargetAbandonedClub
						strHelp = "GBTER_AB_START"
					ENDIF
				ENDIF

				PRINT_HELP_WITH_PLAYER_NAME_NO_SOUND(strHelp, GET_PLAYER_NAME(GBTER_GET_TARGET()), HUD_COLOUR_PURE_WHITE)
				
				GB_SET_GANG_BOSS_HELP_BACKGROUND()
				SET_BIT(iLocalBitset, GBTER_BS_INTRO_DONE)

				CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE === [HANDLE_SHARD_HELP_INTRO] - Printing GBTER_START help message. GBTER_BS_INTRO_DONE SET.")
			ENDIF
		ELSE
			STRING strTitle = "GBTER_BIG"
			STRING strStrapline = "GBTER_BIG_GS"
			IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
				strTitle = "GBTER_B_BIG"
				strStrapline = "GBTER_B_SS"
				IF serverBD.bTargetAbandonedClub
					strTitle = "GBTER_B_BIG"
					strStrapline = "GBTER_AB_SS"
				ENDIF
			ENDIF
			
			CLEAR_ALL_BIG_MESSAGES()
			
			
			// ~a~ has been marked for termination. Take them out
			SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GB_START_OF_JOB, GBTER_GET_TARGET(),
											 -1, strStrapline, strTitle)
											 
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_WORK_STARTED)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE === [HANDLE_SHARD_HELP_INTRO] - Display shard with GBTER_BIG_GS.")
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBitset, GBTER_BS_SHARD_DONE)
			IF NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
				STRING strHelp = "GBTER_BOSS" 							// You have marked a Bodyguard for termination. Kill the target.
				IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
					strHelp = "GBTER_BBOSS"								// You have marked a Club Member for termination. Kill the target.
					IF serverBD.bTargetAbandonedClub
						strHelp = "GBTER_AB_BOSS"						// A Member has abandoned your Motorcycle Club. Kill the target.
					ENDIF
				ENDIF
				
				PRINT_HELP_WITH_PLAYER_NAME_NO_SOUND(strHelp, GET_PLAYER_NAME(GBTER_GET_TARGET()), HUD_COLOUR_PURE_WHITE) 
				
				SET_BIT(iLocalBitset, GBTER_BS_INTRO_DONE)
				CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE === [HANDLE_SHARD_HELP_INTRO] - Printing GBTER_BOSS help message. GBTER_BS_INTRO_DONE SET.")
				GB_SET_GANG_BOSS_HELP_BACKGROUND()
			ENDIF
		ELSE
			STRING strTitle = "GBTER_BIG"
			STRING strStrapline = "GBTER_BIG_GS"
			IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
				strTitle = "GBTER_B_BIG"
				strStrapline = "GBTER_B_BSS"
				IF serverBD.bTargetAbandonedClub
					strTitle = "GBTER_B_BIG"
					strStrapline = "GBTER_AB_SS"
				ENDIF
			ENDIF
			
			CLEAR_ALL_BIG_MESSAGES()
			// You have marked ~a~ for termination
			SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GB_START_OF_JOB, GBTER_GET_TARGET(),
											 -1, strStrapline, strTitle)
											 
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_WORK_STARTED)								 
			CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE === [HANDLE_SHARD_HELP_INTRO] - Display shard with GBTER_BIG_BS.")
		ENDIF
	ENDIF
	
	SET_BIT(iLocalBitset, GBTER_BS_SHARD_DONE)
ENDPROC

PROC MAINTAIN_OBJECTIVE_TEXT()
	IF GB_SHOULD_UI_BE_HIDDEN()
		IF Is_There_Any_Current_Objective_Text()
			Clear_Any_Objective_Text_From_This_Script()
			CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE === [MAINTAIN_OBJECTIVE_TEXT] - GB_SHOULD_UI_BE_HIDDEN - Clearing objective text.")
		ENDIF
		EXIT
	ENDIF
	
	STRING strKill = "GBTER_KILL"
	IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
		strKill = "GBTER_B_KILL"
		IF serverBD.bTargetAbandonedClub
			strKill = "GBTER_AB_KILL"
		ENDIF
	ENDIF
	
	IF NOT Is_This_The_Current_Objective_Text(strKill)
		Print_Objective_Text_With_Player_Name(strKill, GBTER_GET_TARGET())
	ENDIF
	
ENDPROC

PROC HANDLE_SHARD_OUTRO()
	IF GB_SHOULD_UI_BE_HIDDEN()
		EXIT
	ENDIF
	
	IF Is_There_Any_Current_Objective_Text()
		Clear_Any_Objective_Text_From_This_Script()
	ENDIF
	IF IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
		CLEAR_ALL_BIG_MESSAGES()
	ENDIF
	
	STRING strTitle = "GB_WORK_OVER"
	STRING strStrapline = "GB_WORK_OVER"
	IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
		strTitle = "BK_RUN_OVER"
	ENDIF

	IF NOT IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_TARGET_FLED)
	AND NOT IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_TIME_UP)
	AND NOT IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_BOSS_LEFT)
				
		// 1: If the hit squad killed them
		IF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_KILLED_BY_HIT_SQUAD)								
			strStrapline = "GBTER_B_SSH"
			IF serverBD.bTargetAbandonedClub
				strStrapline = "GBTER_B_SSH_A"
			ENDIF
			
			IF serverBD.bTargetAbandonedClub
				// The Hit Squad sent <C>~a~</C>~s~ to their death for abandoning ~a~~s~
				SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION_WITH_PLAYERS(BIG_MESSAGE_GB_BUY_END_OF_JOB_SUCCESS_ALT, strTitle, strStrapline, 
																GB_GET_ORGANIZATION_NAME_AS_A_STRING(), GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID()),  
																GBTER_GET_TARGET(), INVALID_PLAYER_INDEX(), INVALID_PLAYER_INDEX())
			ELSE
				// The Hit Squad sent <C>~a~</C>~s~ to their death
				SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, GBTER_GET_TARGET(),
													-1, strStrapline, strTitle)
			ENDIF
		ELIF GET_GBTER_KILLER() != PLAYER_ID()
			strStrapline = "GBTER_BIG_COMS"
			IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
				strStrapline = "GBTER_B_SSO"
				IF serverBD.bTargetAbandonedClub
					strStrapline = "GBTER_B_SSO_A"
				ENDIF
			ENDIF

			IF serverBD.bTargetAbandonedClub
				// <C>~a~</C>~s~ sent <C>~a~</C>~s~ to their death for abandoning ~a~~s~
				SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION_WITH_PLAYERS(BIG_MESSAGE_GB_BUY_END_OF_JOB_SUCCESS_ALT, strTitle, strStrapline, 
																GB_GET_ORGANIZATION_NAME_AS_A_STRING(), GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID()),  
																GET_GBTER_KILLER(), GBTER_GET_TARGET(), INVALID_PLAYER_INDEX())
			ELSE	
				// <C>~a~</C>~s~ sent <C>~a~</C>~s~ to their death
				SETUP_NEW_BIG_MESSAGE_WITH_2_PLAYERS(BIG_MESSAGE_GB_GENERIC, GET_GBTER_KILLER(), GBTER_GET_TARGET(),
													-1, strStrapline, strTitle)
			ENDIF
		// 1: If the local player is the killer
		ELSE
			strStrapline = "GBTER_BIG_WINS"
			IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
				strStrapline = "GBTER_B_SSW"
				IF serverBD.bTargetAbandonedClub
					strStrapline = "GBTER_B_SSW_A"
				ENDIF
			ENDIF
			
			IF serverBD.bTargetAbandonedClub
				// You sent <C>~a~</C>~s~ to their death for abandoning ~a~~s~
				SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION_WITH_PLAYERS(BIG_MESSAGE_GB_BUY_END_OF_JOB_SUCCESS_ALT, "GB_WINNER", strStrapline, 
																GB_GET_ORGANIZATION_NAME_AS_A_STRING(), GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID()),  
																GBTER_GET_TARGET(), INVALID_PLAYER_INDEX(), INVALID_PLAYER_INDEX())
			ELSE	
				// You sent <C>~a~</C>~s~ to their death													
				SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS,  GBTER_GET_TARGET(),
											 -1, strStrapline, "GB_WINNER")
			ENDIF
		ENDIF										
		CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE === [HANDLE_SHARD_OUTRO] - Target was killed, displayed shard.")
		
	// Target became unavailable (left session, disconnected etc.)
	ELSE
		IF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_BOSS_LEFT)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE === [HANDLE_SHARD_OUTRO] - Work ended. Boss left.")
		ELSE
			strStrapline = "GBTER_BIG_FLEE"
				IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
					strStrapline = "GBTER_B_BFLEE"
				ENDIF
			
			// The target survived
			SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, strTitle, strStrapline)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE === [HANDLE_SHARD_OUTRO] - Work ended. Target left.")
			
			IF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_TIME_UP)
			OR IS_PLAYER_IN_PROPERTY(GBTER_GET_TARGET(), TRUE)
				IF PLAYER_ID() = GB_GET_LOCAL_PLAYER_MISSION_HOST()
					// Broadcast ticker/shard to target
					SCRIPT_EVENT_DATA_TICKER_MESSAGE tickerData
					tickerData.playerID = GB_GET_LOCAL_PLAYER_MISSION_HOST()
					tickerData.TickerEvent = TICKER_EVENT_GB_TARGET_SAFE
					BROADCAST_TICKER_EVENT(tickerData, SPECIFIC_PLAYER(GBTER_GET_TARGET()))
					
					CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE === [HANDLE_SHARD_OUTRO] - Ticker event broadcast to: ", GET_PLAYER_NAME(GBTER_GET_TARGET()))
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE === [HANDLE_SHARD_OUTRO] - Killer:  ", GET_PLAYER_NAME(GET_GBTER_KILLER()))
ENDPROC

/// PURPOSE: Blip selected target.
PROC MAINTAIN_TARGET_BLIP()
	IF NOT IS_BIT_SET(iLocalBitset, GBTER_BS_BLIP_UPDATED)
		IF IS_NET_PLAYER_OK(GBTER_GET_TARGET())
		AND NETWORK_IS_PLAYER_ACTIVE(GBTER_GET_TARGET())
			SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(GBTER_GET_TARGET(), GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_RED), TRUE)
			SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(GBTER_GET_TARGET(), RADAR_TRACE_TEMP_4, TRUE)
			FORCE_BLIP_PLAYER(GBTER_GET_TARGET(), TRUE, TRUE)
			SET_FIXED_BLIP_SCALE_FOR_PLAYER(GBTER_GET_TARGET(), g_sMPTunables.fgangboss_Job_blip_scale, TRUE)
			
			DISABLE_SECONDARY_OUTLINES_FOR_PLAYER_BLIP(GBTER_GET_TARGET(), TRUE)
			
			//Only show first time
			IF GET_SERVER_MISSION_STAGE() <= GBTER_KILL
				FLASH_BLIP_FOR_PLAYER(GBTER_GET_TARGET(), TRUE, 7000)
				CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE ===  [MAINTAIN_TARGET_BLIP] - Flash blip")
			ENDIF
			
			SET_BIT(iLocalBitset, GBTER_BS_BLIP_UPDATED)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE ===  [MAINTAIN_TARGET_BLIP] - GBTER_BS_BLIP_UPDATED = SET")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_TARGET_BEEN_KILLED_BY_HIT_SQUAD(ENTITY_INDEX entityId)
	IF GET_ENTITY_MODEL(entityId) = DUMMY_MODEL_FOR_SCRIPT//S_M_Y_XMECH_02_MP
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Check if the target has been killed by a member of the gang.
PROC MAINTAIN_TERMINATE_COMPLETE_CHECK(INT iEventID)
	STRUCT_ENTITY_DAMAGE_EVENT seDamageEvent
	
	// Check for damage event
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, seDamageEvent, SIZE_OF(seDamageEvent))
		// Check a victim exists, is a ped, and is the the target.
		IF DOES_ENTITY_EXIST(seDamageEvent.VictimIndex)
		AND IS_ENTITY_A_PED(seDamageEvent.VictimIndex) 
		AND GET_PED_INDEX_FROM_ENTITY_INDEX(seDamageEvent.VictimIndex) = GET_PLAYER_PED(GBTER_GET_TARGET())
			
			// If the victim was killed
			IF seDamageEvent.VictimDestroyed
			
				CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE ===  [MAINTAIN_TERMINATE_COMPLETE_CHECK] - Target killed.")
			
				// Check a damager exists, is a ped, and is the local player.
				IF DOES_ENTITY_EXIST(seDamageEvent.DamagerIndex)
				AND IS_ENTITY_A_PED(seDamageEvent.DamagerIndex) 
					CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE ===  [MAINTAIN_TERMINATE_COMPLETE_CHECK] - Damager is ped.")
					
					IF GET_PED_INDEX_FROM_ENTITY_INDEX(seDamageEvent.DamagerIndex) = PLAYER_PED_ID()
						//#IF FEATURE_BIKER
						//IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
							//BIK_ADD_POINTS_FOR_THIS(BIK_ADD_POINTS_MFD_KILL)
						//ENDIF
						//#ENDIF
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitset, CLIENT_BS_LOCAL_KILLED_TARGET)
						CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE ===  [MAINTAIN_TERMINATE_COMPLETE_CHECK] - Killed by local player.")
					ELSE
						CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE ===  [MAINTAIN_TERMINATE_COMPLETE_CHECK] - Killed by not local player.")
					ENDIF
				ENDIF
			ENDIF
		ENDIF					
	ENDIF
	
ENDPROC

/// PURPOSE: Listen for a damage event, and use the data to check if the target has been killed.
PROC MAINTAIN_GBTER_DAMAGE_EVENTS()
	INT iEventID
	EVENT_NAMES ThisScriptEvent

	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iEventID
		ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iEventID)
					
		SWITCH ThisScriptEvent			
			CASE EVENT_NETWORK_DAMAGE_ENTITY
				MAINTAIN_TERMINATE_COMPLETE_CHECK(iEventID)
			BREAK					
		ENDSWITCH
	ENDREPEAT
ENDPROC

/// PURPOSE: Maintain distance checks
PROC MAINTAIN_DISTANCE_CHECKS()
	IF IS_NET_PLAYER_OK(GBTER_GET_TARGET())
	AND NOT IS_ENTITY_DEAD(GET_PLAYER_PED(GBTER_GET_TARGET()))
		GB_MAINTAIN_BOSS_WORK_DISTANT_CHECKS(FMMC_TYPE_GB_TERMINATE, GET_ENTITY_COORDS(GET_PLAYER_PED(GBTER_GET_TARGET())), bSet)
	ENDIF
ENDPROC

/// PURPOSE: SERVER ONLY: Used to check client states.
PROC MAINTAIN_ACTIVE_PLAYER_CHECKS()
	INT iCount
	INT iMaxPart = NETWORK_GET_MAX_NUM_PARTICIPANTS()
	
	INT iIndex
	REPEAT iMaxPart iIndex
		IF GET_SERVER_MISSION_STAGE() < GBTER_REWARDS
			IF IS_BIT_SET(playerBD[iIndex].iClientBitSet, CLIENT_BS_LOCAL_KILLED_TARGET)
				SET_GBTER_KILLER(INT_TO_PLAYERINDEX(iIndex))
				SET_BIT(serverBD.iServerBitSet, SERVER_BS_TARGET_KILLED)
			ENDIF
			
			IF IS_BIT_SET(playerBD[iIndex].iClientBitSet, CLIENT_BS_KILLED_BY_HIT_SQUAD)
				SET_GBTER_KILLER(GB_GET_LOCAL_PLAYER_GANG_BOSS())
				SET_BIT(serverBD.iServerBitSet, SERVER_BS_KILLED_BY_HIT_SQUAD)
			ENDIF
			
			IF IS_BIT_SET(playerBD[iIndex].iClientBitSet, CLIENT_BS_WORK_STARTED)
				SET_BIT(serverBD.iServerBitSet, SERVER_BS_WORK_STARTED)
			ENDIF
			
			IF IS_BIT_SET(playerBD[iIndex].iClientBitSet, CLIENT_BS_TIME_UP)
				SET_BIT(serverBD.iServerBitSet, SERVER_BS_TIME_UP)
			ENDIF
		ELIF GET_SERVER_MISSION_STAGE() = GBTER_REWARDS
			IF IS_BIT_SET(playerBD[iIndex].iClientBitSet, CLIENT_BS_REWARDS_GIVEN)
				iCount++
			ENDIF
		ENDIF
			
	ENDREPEAT
	
	// Check all players are in the same state.
	IF GET_SERVER_MISSION_STAGE() = GBTER_REWARDS
		IF iCount = NETWORK_GET_NUM_PARTICIPANTS()
			SET_BIT(serverBD.iServerBitSet, SERVER_BS_REWARDS_GIVEN)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE === [MAINTAIN_ACTIVE_PLAYER_CHECKS] - SERVER_BS_REWARDS_GIVEN = SET. iCount = NETWORK_GET_NUM_PARTICIPANTS()")
		ELSE
			IF HAS_NET_TIMER_EXPIRED(serverBD.stRewardTimeout, REWARD_TIMEOUT)
				SET_BIT(serverBD.iServerBitSet, SERVER_BS_REWARDS_GIVEN)
				CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE === [MAINTAIN_ACTIVE_PLAYER_CHECKS] - SERVER_BS_REWARDS_GIVEN = SET. stRewardTimeout expired after waiting ", (REWARD_TIMEOUT/1000), " seconds.")
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE: SERVER ONLY: Check it's okay to start the hit.
FUNC BOOL SHOULD_JOB_START()	
	CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE === [SHOULD_JOB_START] - PASS")
	RETURN TRUE
ENDFUNC

/// PURPOSE: Check if the job should end.
PROC MAINTAIN_END_CONDITION_CHECKS()
	IF NOT GB_IS_PLAYER_VALID_GBTERMINATE_TARGET(GBTER_GET_TARGET())
		SET_SERVER_MISSION_END_REASON(GBTER_ENDREASON_FLED)
		SET_SERVER_MISSION_STAGE(GBTER_REWARDS)
		SET_BIT(serverBD.iServerBitSet, SERVER_BS_TARGET_FLED)
	ENDIF
	
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS() = INVALID_PLAYER_INDEX()
	OR NOT IS_NET_PLAYER_OK(GB_GET_LOCAL_PLAYER_GANG_BOSS(), FALSE)
		SET_SERVER_MISSION_END_REASON(GBTER_ENDREASON_BOSS_LEFT)
		SET_SERVER_MISSION_STAGE(GBTER_REWARDS)
		SET_BIT(serverBD.iServerBitSet, SERVER_BS_BOSS_LEFT)
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_WORK_STARTED)
		IF HAS_NET_TIMER_EXPIRED(serverBD.stWorkTimer, GET_TERMINATE_DURATION())
			SET_BIT(serverBD.iServerBitSet, SERVER_BS_TIME_UP)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_TIME_UP)
		SET_SERVER_MISSION_END_REASON(GBTER_ENDREASON_TIMEUP)
		SET_SERVER_MISSION_STAGE(GBTER_REWARDS)
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_TARGET_KILLED)
	OR IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_KILLED_BY_HIT_SQUAD)
		SET_SERVER_MISSION_END_REASON(GBTER_ENDREASON_KILL)
		SET_SERVER_MISSION_STAGE(GBTER_REWARDS)
	ENDIF
ENDPROC

/// PURPOSE: Give out rewards when hit is over.
PROC GIVE_REWARDS()
	GANG_BOSS_MANAGE_REWARDS_DATA structGangBossManageRewardsData
	
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_REWARDS_GIVEN)
	
		// Target left or time expired, give no rewards
		IF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_TARGET_FLED)
		OR IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_TIME_UP)
		OR IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_TIME_UP)
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_REWARDS_GIVEN)
			EXIT
		ENDIF
	
		IF GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE()
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_REWARDS_GIVEN)
			EXIT
		ENDIF
		
		IF GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_REWARDS_GIVEN)
			EXIT
		ENDIF
			
		IF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_KILLED_BY_HIT_SQUAD)
			GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_TERMINATE, TRUE, structGangBossManageRewardsData)
		ELSE
			GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_TERMINATE, (IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_KILLED_TARGET)), structGangBossManageRewardsData)
		ENDIF
		
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_REWARDS_GIVEN)
	ENDIF
ENDPROC


PROC DRAW_UI_TIMER()
	IF GB_SHOULD_UI_BE_HIDDEN()
		EXIT
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_WORK_STARTED)
		IF HAS_NET_TIMER_STARTED(serverBD.stWorkTimer)
			HUD_COLOURS timeColour
			INT iCurrentTime = (GET_TERMINATE_DURATION() - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBd.stWorkTimer))
			
			IF iCurrentTime > 30000
				timeColour = HUD_COLOUR_WHITE
			ELSE
				timeColour = HUD_COLOUR_RED
			ENDIF
			iCurrentTime = IMAX(iCurrentTime,0)
			
			STRING strTimer = "GB_WORK_END"
			IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
				strTimer = "BK_TIME"
			ENDIF
			
			IF iCurrentTime > 0
				DRAW_GENERIC_TIMER(iCurrentTime, strTimer, DEFAULT, TIMER_STYLE_DONTUSEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, timeColour)
			ELSE
				DRAW_GENERIC_TIMER(0, strTimer, DEFAULT, TIMER_STYLE_DONTUSEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, timeColour)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//----------------------
//	SCRIPT PROCESSING
//----------------------

PROC CLIENT_PROCESSING()

	#IF IS_DEBUG_BUILD
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOCAL_KILLED_TARGET)
		CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_TERMINATE === [CLIENT_PROCESSING] - DEBUG KEY PRESSED: S - DEBUG LOCAL KILLED TARGET")
	ENDIF
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_TIME_UP)
		CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_TERMINATE === [CLIENT_PROCESSING] - DEBUG KEY PRESSED: F - TIME UP!")
	ENDIF
	#ENDIF

	SWITCH GET_CLIENT_MISSION_STAGE()
	
		CASE GBTER_INIT
			GB_COMMON_BOSS_MISSION_SETUP(FMMC_TYPE_GB_TERMINATE)
			GB_SET_LOCAL_PLAYER_CRITICAL_TO_JOB(TRUE)
			
			IF GET_SERVER_MISSION_STAGE() > GBTER_INIT
				SET_CLIENT_MISSION_STAGE(GBTER_KILL)
			ENDIF
		BREAK
		
		CASE GBTER_KILL
			IF NOT IS_BIT_SET(iLocalBitset, GBTER_BS_INTRO_DONE)
				HANDLE_SHARD_HELP_INTRO()
			ENDIF
			
			IF g_MyTerminateTarget = INVALID_PLAYER_INDEX() 
				IF NETWORK_IS_PLAYER_ACTIVE(GBTER_GET_TARGET())
					g_MyTerminateTarget = GBTER_GET_TARGET()
					CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_TERMINATE [MC_POINTS] g_MyTerminateTarget = ", GET_PLAYER_NAME(g_MyTerminateTarget))
				ENDIF
			ENDIF
			
			DRAW_UI_TIMER()
			MAINTAIN_OBJECTIVE_TEXT()
			MAINTAIN_TARGET_BLIP()
			
			MAINTAIN_GBTER_DAMAGE_EVENTS()
			//MAINTAIN_MEMBER_OF_GANG_CHECKS()
			MAINTAIN_DISTANCE_CHECKS()
			
			DRAW_CHALLENGE_DPAD_LBD(g_GBLeaderboardStruct.challengeLbdStruct, g_GBLeaderboardStruct.siDpadMovie, SUB_BOSS, g_GBLeaderboardStruct.dpadVariables, 
									g_GBLeaderboardStruct.fmDpadStruct)
			
			IF GET_SERVER_MISSION_STAGE() > GBTER_KILL
				SET_CLIENT_MISSION_STAGE(GBTER_REWARDS)
			ENDIF
		BREAK
	
		CASE GBTER_REWARDS
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_REWARDS_GIVEN)
				GANG_BOSS_MANAGE_WANTED_CLEAN_UP(FALSE, INVALID_PLAYER_INDEX()) 
				HANDLE_SHARD_OUTRO()
				GIVE_REWARDS()
			ENDIF
			
			IF GET_SERVER_MISSION_STAGE() > GBTER_REWARDS
				SET_CLIENT_MISSION_STAGE(GBTER_END)
			ENDIF
		BREAK
		
		CASE GBTER_END
		
		BREAK

	ENDSWITCH

ENDPROC

PROC SERVER_PROCESSING()
	
	SWITCH serverBD.eGBTER_Stage
	
		CASE GBTER_INIT
			IF SHOULD_JOB_START()
				SET_SERVER_MISSION_STAGE(GBTER_KILL)
			ENDIF
			
			MAINTAIN_ACTIVE_PLAYER_CHECKS()
		BREAK
		
		CASE GBTER_KILL
			MAINTAIN_ACTIVE_PLAYER_CHECKS()
			MAINTAIN_END_CONDITION_CHECKS()
		BREAK
		
		CASE GBTER_REWARDS
			IF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_REWARDS_GIVEN)
				SET_SERVER_MISSION_STAGE(GBTER_END)
			ENDIF
			
			MAINTAIN_ACTIVE_PLAYER_CHECKS()
		BREAK
		
		CASE GBTER_END
			
		BREAK

	ENDSWITCH

ENDPROC

FUNC BOOL INIT_CLIENT()

	RETURN TRUE
ENDFUNC

FUNC BOOL INIT_SERVER()
	IF INIT_GBTER()
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

//Helper function to set a clients game/mission state
PROC SET_CLIENT_MISSION_STATE(INT iPlayer,INT iState)
	playerBD[iPlayer].iClientGameState = iState
	CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE === SET_CLIENT_MISSION_STATE = ",GET_MISSION_STATE_STRING(iState))
ENDPROC

FUNC BOOL PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)

	CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE === PROCESS_PRE_GAME")
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission), missionScriptArgs)
	
	//Reserve Entities
	RESERVE_NETWORK_MISSION_OBJECTS(0)
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD,SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD,SIZE_OF(playerBD))
	
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	GB_COMMON_BOSS_MISSION_PREGAME()
	
	#IF IS_DEBUG_BUILD
		CREATE_WIDGETS()
	#ENDIF
	
	RETURN TRUE
	
ENDFUNC

//----------------------
//	MAIN SCRIPT
//----------------------

SCRIPT(MP_MISSION_DATA missionScriptArgs)


	CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE === START")
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NOT PROCESS_PRE_GAME(missionScriptArgs)
			SCRIPT_CLEANUP()
		ENDIF	
	ENDIF
	
	WHILE (TRUE)
	
		MP_LOOP_WAIT_ZERO()
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			CPRINTLN(DEBUG_NET_AMBIENT, "=== GB_TERMINATE === SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")
			SCRIPT_CLEANUP()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF bKillScript
			SET_SERVER_MISSION_STATE(GAME_STATE_END)
		ENDIF
		#ENDIF
		
		
		SWITCH(GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT()))
		
			CASE GAME_STATE_INIT
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					IF INIT_CLIENT()
						SET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT(),GAME_STATE_RUNNING)
					ENDIF
				ENDIF
			BREAK
			
			CASE GAME_STATE_RUNNING
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					IF NOT GB_SHOULD_EVENT_BE_HIDDEN()
						CLIENT_PROCESSING()
					ENDIF
					
					IF GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION()
						SET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT(),GAME_STATE_END)
					ENDIF
					
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					SET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT(),GAME_STATE_END)
				ENDIF
			BREAK
			
			CASE GAME_STATE_END
				SCRIPT_CLEANUP()
			BREAK
			
		ENDSWITCH
	
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
			IF GB_SHOULD_KILL_ACTIVE_BOSS_MISSION()
				SET_SERVER_MISSION_STATE(GAME_STATE_END)
			ENDIF
		
			SWITCH(GET_SERVER_MISSION_STATE())
			
				CASE GAME_STATE_INIT
					IF INIT_SERVER()
						SET_SERVER_MISSION_STATE(GAME_STATE_RUNNING)
					ELSE
//						IF HAS_NET_TIMER_EXPIRED(stInitTimeout, 15000)
//							SET_SERVER_MISSION_STATE(GAME_STATE_END)
//						ENDIF
					ENDIF
				BREAK
				
				CASE GAME_STATE_RUNNING
					IF GET_SERVER_MISSION_STAGE() = GBTER_END
						SET_SERVER_MISSION_STATE(GAME_STATE_END)
					ELSE
						SERVER_PROCESSING()
					ENDIF
				BREAK
				
				CASE GAME_STATE_END
					SCRIPT_CLEANUP()
				BREAK
				
			ENDSWITCH
		
		ENDIF
		
	ENDWHILE

	
	
// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT

