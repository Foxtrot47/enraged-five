//////////////////////////////////////////////////////////////////////////////////////////
// Name:        GB_POINT_TO_POINT.sc													//
// Description: The Boss sets a location on the map to race to using the challenge 		//
//				launch menu. Whichever Goon gets there first wins.						//
// Written by:  Bobby Wright															//
// Date: 08/10/2015																		//
//////////////////////////////////////////////////////////////////////////////////////////
///    
USING "globals.sch"
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "flow_public_core_override.sch"
USING "net_wait_zero.sch"
USING "net_mission.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "minigames_helpers.sch"
USING "net_spawn.sch"
USING "net_gang_boss.sch"
USING "net_gang_boss_spectate.sch"
USING "DM_Leaderboard.sch"
USING "am_common_ui.sch"

//	CONSTANTS

CONST_FLOAT DISQUALIFIED_RANGE				70.0
CONST_FLOAT R2P_DESTINATION_RADIUS			9.0
CONST_FLOAT R2P_RADIUS_FOR_POINT			2.0
CONST_FLOAT R2P_DESTINATION_RADIUS_HEIGHT	5.0
CONST_INT 	ciP2P_TIMER_DROP 				30000
CONST_INT 	RESEND_EVENT_DELAY				250
CONST_INT 	GB_P2P_MIN_WAGER				50
CONST_FLOAT R2P_START_RADIUS 				350.0
CONST_INT 	SERVER_PROCESS_WINNER_DELAY		1
CONST_INT 	SAFE_NODE_SEARCH_RADIUS			25	//Range to check for Road Nodes
CONST_INT 	SAFE_COORDS_SEARCH_RADIUS		150	//Range to do GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY 

//	Local Bitset
INT iBoolsBitSet
CONST_INT biStartSetup 						0
CONST_INT biWagerSet 						1
CONST_INT biReachedEndDestination 			2
CONST_INT biDisqualified 					3
CONST_INT biIntroShard 						4
CONST_INT biIntroShardOnDisplay				5
CONST_INT biClearHelpAtStartOfScript		6
CONST_INT biSOUND_BIT_SLIP_STREAM			7
CONST_INT biWinningTicker0					8
CONST_INT biWinningTicker1					9
CONST_INT biWinningTicker2					10
CONST_INT biGotToEndShard					11

INT iHelpsBitSet
CONST_INT biH_SetUpHelpBoss					0
CONST_INT biH_WaitHelp						1
CONST_INT biH_SetUpHelpGoon					2
CONST_INT biH_SetUpHelpAfterGo				3

//	GAME STATE
CONST_INT GAME_STATE_INIT 					0
CONST_INT GAME_STATE_RUNNING				1
CONST_INT GAME_STATE_TERMINATE_DELAY		2
CONST_INT GAME_STATE_END					3

ENUM GEN_RUN_STAGE
	eGB_P2P_STAGE_START = 0,
	eGB_P2P_STAGE_COUNTDOWN,
	eGB_P2P_STAGE_RACE,
	eGB_P2P_STAGE_REWARD,
	eGB_P2P_STAGE_CLEANUP
ENDENUM

//	BROADCAST VARIABLES
CONST_INT biS_GetWaypointVector 		0 
CONST_INT biS_WaypointVectorFound 		1
CONST_INT biS_AllCrewDead 				2
CONST_INT biS_AllCrewLeftStartArea 		3
CONST_INT biS_AllCrewHaveLeft 			4
CONST_INT biS_WaypointSearchStarted 	5
CONST_INT biS_FirstWaypointVectorFound 	6
CONST_INT biS_WaitForSpecActive		 	7
CONST_INT biS_UseWayPoint		 		8
CONST_INT biS_TimerDrop				 	9
CONST_INT biS_AllPlayersAtEnd		 	10

CONST_INT ciNUM_GANG_MEMBERS 4
SCRIPT_TIMER stWaterTimer

// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData
	INT iServerGameState = GAME_STATE_INIT
	INT iServerBitSet
	TIME_DATATYPE StartTime
	VECTOR vStart, vDestination
	GEN_RUN_STAGE eGEN_Stage = eGB_P2P_STAGE_START
	INT iWinnerPart[ciNUM_GANG_MEMBERS]
	SCRIPT_TIMER stPickStartPointTimer
	SCRIPT_TIMER stRaceOverTimer
	INT iNumParticpants
	
	INT iMatchId1 
	INT iMatchId2
ENDSTRUCT
ServerBroadcastData serverBD

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	INT iClientGameState = GAME_STATE_INIT
	INT iClientBitSet
	INT iReachedDestinationTime
	GEN_RUN_STAGE eGEN_Stage = eGB_P2P_STAGE_START
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

CONST_INT biP_ReachedDestination 0
GB_STRUCT_BOSS_END_UI sGB_End_Struct

//	Local VARS
INT iSlipSoundId
COUNTDOWN_UI Race_CountDownUI
SCRIPT_TIMER iServerProcessWinnerTimer
BLIP_INDEX DestinationBlip, RadiusBlip[NUM_NETWORK_PLAYERS]
VECTOR vPlayers[NUM_NETWORK_PLAYERS]

CHECKPOINT_INDEX DestinationCheckpoint
//VECTOR vLocalStart
GANG_BOSS_MANAGE_REWARDS_DATA sRewardsData
GB_MAINTAIN_SPECTATE_VARS sSpecVars
#IF IS_DEBUG_BUILD
BOOL bDoJSkip, bJorSSkipDone 
#ENDIF

//	FUNCTIONS
//PURPOSE: Adds a blip to show the area that the Destination can't be in
PROC ADD_MIN_DISTANCE_RADIUS_BLIP()
	INT iParticipant
	PLAYER_INDEX PlayerId
	PED_INDEX PlayerPedId
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		//If they are active
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
			PlayerPedId = GET_PLAYER_PED(PlayerId)
			vPlayers[iParticipant] = GET_ENTITY_COORDS(PlayerPedId, FALSE)
			IF NOT DOES_BLIP_EXIST(RadiusBlip[iParticipant])
				RadiusBlip[iParticipant] = ADD_BLIP_FOR_RADIUS(vPlayers[iParticipant], TO_FLOAT(g_sMPTunables.igb_pointtopoint_destination_radius))
				SET_BLIP_COLOUR(RadiusBlip[iParticipant], BLIP_COLOUR_RED)
				SET_BLIP_ALPHA(RadiusBlip[iParticipant], 150)
				SET_BLIP_NAME_FROM_TEXT_FILE(RadiusBlip[iParticipant], "GB_P2P_DEST")
				SET_BLIP_DISPLAY(RadiusBlip[iParticipant], DISPLAY_MAP)
				CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - ADD_MIN_DISTANCE_RADIUS_BLIP - iParticipant = ", iParticipant)
			ELSE
				SET_BLIP_COORDS(RadiusBlip[iParticipant], vPlayers[iParticipant])
			ENDIF
		ELIF DOES_BLIP_EXIST(RadiusBlip[iParticipant])
			REMOVE_BLIP(RadiusBlip[iParticipant])
		ENDIF
	ENDREPEAT
ENDPROC

//PURPOSE: Removes the radius blip
PROC REMOVE_MIN_DISTANCE_RADIUS_BLIP()
	INT iParticipant
	REPEAT NUM_NETWORK_PLAYERS iParticipant
		IF DOES_BLIP_EXIST(RadiusBlip[iParticipant])
			REMOVE_BLIP(RadiusBlip[iParticipant])
			CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - REMOVE_MIN_DISTANCE_RADIUS_BLIP - iParticipant = ", iParticipant)
		ENDIF
	ENDREPEAT
ENDPROC	
FUNC STRING GET_MISSION_STATE_STRING(INT iState)
	SWITCH(iState)
		CASE GAME_STATE_INIT 				RETURN "GAME_STATE_INIT"
		CASE GAME_STATE_RUNNING 			RETURN "GAME_STATE_RUNNING"
		CASE GAME_STATE_TERMINATE_DELAY 	RETURN "GAME_STATE_TERMINATE_DELAY"
		CASE GAME_STATE_END 				RETURN "GAME_STATE_END"
	ENDSWITCH
	RETURN "UNKNOWN STATE!"
ENDFUNC
//Helper function to get a clients game/mission state
FUNC INT GET_CLIENT_MISSION_STATE(INT iPart)
	RETURN playerBD[iPart].iClientGameState
ENDFUNC
//Helper function to get the servers game/mission state
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC
//Helper function to set a clients game/mission state
PROC SET_CLIENT_MISSION_STATE(INT iPart,INT iState)
	playerBD[iPart].iClientGameState = iState
	CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] SET_CLIENT_MISSION_STATE = ",GET_MISSION_STATE_STRING(iState))
ENDPROC
//Helper function to set the servers game/mission state
PROC SET_SERVER_MISSION_STATE(INT iState)
	serverBD.iServerGameState = iState
	CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] SET_SERVER_MISSION_STATE = ",GET_MISSION_STATE_STRING(iState))
ENDPROC

PROC CLEANUP_RACES_SLIPSTREAM_SCENE(BOOL bForce = FALSE)	
	IF bForce
	OR IS_BIT_SET(iBoolsBitSet, biSOUND_BIT_SLIP_STREAM) //*1597674
		CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] CLEANUP_RACES_SLIPSTREAM_SCENE")
		IF iSlipSoundId != 0
		AND NOT HAS_SOUND_FINISHED(iSlipSoundId)
			CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] STOP_SOUND(iSlipSoundId)")
			STOP_SOUND(iSlipSoundId)
		ENDIF
		iSlipSoundId = 0
		STOP_AUDIO_SCENE("RACES_SLIPSTREAM_SCENE")
		CLEAR_BIT(iBoolsBitSet, biSOUND_BIT_SLIP_STREAM)
	ENDIF
ENDPROC

//		Mission Cleanup
PROC SCRIPT_CLEANUP()
	CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] SCRIPT_CLEANUP")
	
	g_sGb_Telemetry_data.sdata.m_match1 = serverBD.iMatchId1
	g_sGb_Telemetry_data.sdata.m_match2 = serverBD.iMatchId2
	PROCESS_CURRENT_BOSS_CHALLENGE_PLAYSTATS(g_sGb_Telemetry_data.sdata)
	GB_COMMON_BOSS_MISSION_CLEANUP()
	REMOVE_MIN_DISTANCE_RADIUS_BLIP()
	CLEANUP_RACES_SLIPSTREAM_SCENE(TRUE)
	SET_MINIMAP_BLOCK_WAYPOINT(FALSE)
	SET_ENABLE_VEHICLE_SLIPSTREAMING(FALSE)
	SET_USE_SET_DESTINATION_IN_PAUSE_MAP(FALSE)
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("MP_RACES_SLIPSTREAM")
	Clear_Any_Objective_Text_From_This_Script()
	//RELEASE_CONTEXT_INTENTION(sSpecVars.iGbSpecContext)
	GB_TIDYUP_SPECTATOR_CAM()
	SET_PLAYER_RESPAWN_IN_VEHICLE(FALSE) 
	CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] * END")
	TERMINATE_THIS_THREAD()
ENDPROC

FUNC BOOL GB_P2P_IS_RACE_OVER() 
	RETURN IS_BIT_SET(serverBD.iServerBitSet, biS_AllPlayersAtEnd)
ENDFUNC

FUNC STRING GET_GB_P2P_HELP_STRING(INT iHelpBitToSet)
	SWITCH iHelpBitToSet
		CASE biH_SetUpHelpBoss 		RETURN "GB_P2P_HT0"
		CASE biH_SetUpHelpGoon 		RETURN "GB_P2P_HT0"
		CASE biH_SetUpHelpAfterGo 	RETURN "GB_P2P_HT2"		
	ENDSWITCH
	RETURN ""
ENDFUNC

//		MAINTAIN HELP
PROC DO_GB_P2P_HELP_STRING(INT iHelpBitToSet)
	SWITCH iHelpBitToSet
		CASE biH_SetUpHelpBoss 
			PRINT_HELP_NO_SOUND(GET_GB_P2P_HELP_STRING(biH_SetUpHelpBoss))
			//PRINT_HELP_WITH_LITERAL_STRING_WITH_HUD_COLOUR_NO_SOUND(GET_GB_P2P_HELP_STRING(biH_SetUpHelpBoss), GB_GET_ORGANIZATION_NAME_AS_A_STRING(), GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID()))
			GB_SET_GANG_BOSS_HELP_BACKGROUND()
		BREAK
		CASE biH_SetUpHelpGoon 
			//PRINT_HELP_WITH_LITERAL_STRING_WITH_HUD_COLOUR_NO_SOUND(GET_GB_P2P_HELP_STRING(biH_SetUpHelpBoss), GB_GET_ORGANIZATION_NAME_AS_A_STRING(), GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID()))
			PRINT_HELP_NO_SOUND(GET_GB_P2P_HELP_STRING(biH_SetUpHelpGoon))
			GB_SET_GANG_BOSS_HELP_BACKGROUND()
		BREAK
		CASE biH_SetUpHelpAfterGo 
			PRINT_HELP_NO_SOUND(GET_GB_P2P_HELP_STRING(biH_SetUpHelpBoss))
			GB_SET_GANG_BOSS_HELP_BACKGROUND()
		BREAK
		
		CASE biH_WaitHelp
		//NO
		BREAK
	ENDSWITCH
ENDPROC

PROC DO_GB_P2P_GOD_TEXT(INT iHelpBitToSet)
	SWITCH iHelpBitToSet
		CASE biH_SetUpHelpBoss 
			Print_Objective_Text("GB_P2P_GOD2") 
		BREAK
		CASE biH_WaitHelp
			Print_Objective_Text("GB_P2P_GOD3") 
		BREAK
	ENDSWITCH
ENDPROC

PROC MAINTAIN_GB_P2P_HELP(INT iHelpBitToSet, BOOL bSkipSafeChecks = FALSE)
	IF NOT IS_BIT_SET(iHelpsBitSet, iHelpBitToSet)
		IF bSkipSafeChecks
		OR (CAN_PRINT_GANG_BOSS_HELP_MSG()
		AND MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI)
			SET_BIT(iHelpsBitSet, iHelpBitToSet)
			DO_GB_P2P_HELP_STRING(iHelpBitToSet)
			CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - MAINTAIN_GB_P2P_HELP = PRINT_HELP - ", GET_GB_P2P_HELP_STRING(iHelpBitToSet))
			DO_GB_P2P_GOD_TEXT(iHelpBitToSet)
		ENDIF
	ENDIF
ENDPROC

PROC DRAW_AND_BLIP_DESTINATION()//BOOL &bResetMenuNow)
	VECTOR vDrawPosition = serverBD.vDestination
//	FLOAT fReturnZ
	INT iR, iG, iB, iA
	IF NOT IS_BIT_SET(iBoolsBitSet, biReachedEndDestination)
		//Check if it needs updating
		IF DOES_BLIP_EXIST(DestinationBlip)
			IF NOT ARE_VECTORS_EQUAL(vDrawPosition, GET_BLIP_COORDS(DestinationBlip))
				REMOVE_BLIP(DestinationBlip)
				DELETE_CHECKPOINT(DestinationCheckpoint)
				CLEAR_BIT(iBoolsBitSet, biWagerSet)
				CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - BLIP AND CHECKPOINT REMOVED - NEEDS UPDATING  ")
			ENDIF
		ENDIF
		
		//Add Blip and Checkpoint
		IF NOT DOES_BLIP_EXIST(DestinationBlip)
			IF NOT ARE_VECTORS_EQUAL(vDrawPosition, <<0, 0, 0>>)
			AND NOT ARE_VECTORS_EQUAL(vDrawPosition, <<0, 0, -2000>>)
			AND (NOT IS_BIT_SET(serverBD.iServerBitSet, biS_GetWaypointVector) OR IS_BIT_SET(serverBD.iServerBitSet, biS_WaypointVectorFound) )
				GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
				DestinationBlip = ADD_BLIP_FOR_COORD(vDrawPosition)
				SET_BLIP_SPRITE(DestinationBlip, RADAR_TRACE_RACEFLAG)
				SET_BLIP_SCALE(DestinationBlip, GB_BLIP_SIZE)
				SET_BLIP_PRIORITY(DestinationBlip, BLIPPRIORITY_HIGHEST)
				SET_BLIP_NAME_FROM_TEXT_FILE(DestinationBlip, "R2P_BLIP")
				SET_BLIP_COLOUR(DestinationBlip, BLIP_COLOUR_HUDCOLOUR_YELLOW)
				//SET_IGNORE_NO_GPS_FLAG(TRUE)
				SET_IGNORE_NO_GPS_FLAG_UNTIL_FIRST_NORMAL_NODE(TRUE)
				SET_BLIP_ROUTE(DestinationBlip, TRUE)
				DestinationCheckpoint = CREATE_CHECKPOINT(CHECKPOINT_RACE_GROUND_FLAG, vDrawPosition+<<0.0, 0.0, 4.0>>, vDrawPosition, 10.0 ,iR, iG, iB, 75)
				SET_CHECKPOINT_CYLINDER_HEIGHT(DestinationCheckpoint, 7.5, 7.5, 100)
				//bResetMenuNow = TRUE
				CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - BLIP ADDED AT ", vDrawPosition)
			ENDIF
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(DestinationBlip)
			REMOVE_BLIP(DestinationBlip)
			DELETE_CHECKPOINT(DestinationCheckpoint)
			CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - BLIP AND CHECKPOINT REMOVED - RACE ENDED  ")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL OKAY_TO_SEND_REACHED_DESTINATION_EVENT(PED_INDEX passedInPed)
	IF IS_PED_IN_ANY_VEHICLE(passedInPed)
		VEHICLE_INDEX thisVeh  = GET_VEHICLE_PED_IS_IN(passedInPed)
		IF IS_VEHICLE_DRIVEABLE(thisVeh)
			PED_INDEX thisPed
			INT i
			REPEAT (GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(thisVeh)+1) i
				thisPed = GET_PED_IN_VEHICLE_SEAT(thisVeh, INT_TO_ENUM(VEHICLE_SEAT, i-1))
				IF DOES_ENTITY_EXIST(thisPed)
					IF NOT IS_PED_INJURED(thisPed)
						IF IS_PED_A_PLAYER(thisPed)
							
							PLAYER_INDEX l_player = NETWORK_GET_PLAYER_INDEX_FROM_PED(thisPed)
							IF l_player != NETWORK_GET_PLAYER_INDEX_FROM_PED(passedInPed)
								IF NETWORK_IS_PLAYER_A_PARTICIPANT(l_player)
									IF NETWORK_IS_PARTICIPANT_ACTIVE(NETWORK_GET_PARTICIPANT_INDEX(l_player))
										CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - OKAY_TO_SEND_REACHED_DESTINATION_EVENT - FALSE - NOT FIRST PARTICIPANT IN VEHICLE")
										RETURN FALSE
									ENDIF
								ENDIF
							ELSE
								CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - OKAY_TO_SEND_REACHED_DESTINATION_EVENT - TRUE - FIRST PARTICIPANT IN VEHICLE")
								RETURN TRUE
							ENDIF
							
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
		ENDIF
	ENDIF
	
	CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - OKAY_TO_SEND_REACHED_DESTINATION_EVENT - TRUE")
	RETURN TRUE
ENDFUNC

PROC PROCESS_REACHING_DESTINATION()
	IF NOT IS_BIT_SET(iBoolsBitSet, biReachedEndDestination)
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), serverBD.vDestination+<<0, 0, (R2P_DESTINATION_RADIUS_HEIGHT/2)-1.5>>, <<R2P_DESTINATION_RADIUS, R2P_DESTINATION_RADIUS, R2P_DESTINATION_RADIUS_HEIGHT+2>>)
				IF OKAY_TO_SEND_REACHED_DESTINATION_EVENT(PLAYER_PED_ID())
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biP_ReachedDestination)
					playerBD[PARTICIPANT_ID_TO_INT()].iReachedDestinationTime = GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), serverBD.StartTime)
					SET_BIT(iBoolsBitSet, biReachedEndDestination)
					CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - PROCESS_REACHING_DESTINATION - REACHED DESTINATION - SENT EVENT - TIME = ",playerBD[PARTICIPANT_ID_TO_INT()].iReachedDestinationTime)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC MAINTAIN_COUNT_DOWN_TIMERS(STRING StTimeString, INT iTime, SCRIPT_TIMER &stTimer)

	INT iTimeRemaining = (iTime - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stTimer))
					
	HUD_COLOURS timeColour
	IF iTimeRemaining > 30000
		timeColour = HUD_COLOUR_WHITE
	ELSE
		timeColour = HUD_COLOUR_RED
	ENDIF
	SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME() 
	IF iTimeRemaining > 0
		DRAW_GENERIC_TIMER(iTimeRemaining, StTimeString, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, timeColour)
	ELSE
		DRAW_GENERIC_TIMER(0, StTimeString, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, timeColour)
	ENDIF 
ENDPROC


STRUCT GB_P2P_LB_DATA
	FLOAT fDist
	PLAYER_INDEX PlayerId
ENDSTRUCT 

GB_P2P_LB_DATA sLbData[ciNUM_GANG_MEMBERS]


FUNC BOOL CORRECT_DISTANCE_BASED_ON_PLACE(FLOAT &fDist, INT iParticipant)
	INT iLoop
	REPEAT ciNUM_GANG_MEMBERS iLoop
		IF iParticipant = serverBD.iWinnerPart[iLoop]
			CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - CORRECT_DISTANCE_BASED_ON_PLACE - ", iParticipant, " = serverBD.iWinnerPart[", iLoop, "]")
			fDist = 0.001 * (iLoop+1)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC INT GB_P2P_GET_END_TIME()
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_TimerDrop)
		RETURN ciP2P_TIMER_DROP
	ENDIF
	RETURN g_sMPTunables.igb_pointtopoint_time_limit
ENDFUNC

PROC MAINTTAIN_FIRST_PLACE_UI(BOOL bRunLb = TRUE)

	INT iParticipant, iPartCount, i, j, iTimeRemaining
	PED_INDEX PlayerPedId, thisPed
	GB_P2P_LB_DATA sLbDataTemp
	VEHICLE_INDEX thisVeh
	PLAYER_INDEX PlayerIdTemp
	
	IF bRunLb
		REPEAT ciNUM_GANG_MEMBERS i
			sLbData[i].PlayerId = INVALID_PLAYER_INDEX()
		ENDREPEAT
		
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
			//If they are active
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
				PlayerIdTemp = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
				IF NOT GB_IS_THIS_PLAYER_USING_SPECTATE(PlayerIdTemp)
				AND NOT IS_PLAYER_SCTV(PlayerIdTemp)
					sLbData[iPartCount].PlayerId = PlayerIdTemp
					PlayerPedId = GET_PLAYER_PED(sLbData[iPartCount].PlayerId)
					//And not injured
					IF NOT IS_PED_INJURED(PlayerPedId)
						IF NOT CORRECT_DISTANCE_BASED_ON_PLACE(sLbData[iPartCount].fDist, iParticipant)
							VECTOR vPos = GET_ENTITY_COORDS(PlayerPedId)
							IF vPos.z < -40.0
							AND sLbData[iPartCount].PlayerId != PLAYER_ID()
								 vPos =  NETWORK_GET_LAST_PLAYER_POS_RECEIVED_OVER_NETWORK(sLbData[iPartCount].PlayerId)
							ENDIF
							sLbData[iPartCount].fDist = GET_DISTANCE_BETWEEN_COORDS(vPos, serverBD.vDestination)
							//If we're in a car
							IF IS_PED_IN_ANY_VEHICLE(PlayerPedId)
								//Get the car
								thisVeh  = GET_VEHICLE_PED_IS_IN(PlayerPedId)
								//If you can drive it
								IF IS_VEHICLE_DRIVEABLE(thisVeh)
									//Get the driver
									thisPed = GET_PED_IN_VEHICLE_SEAT(thisVeh, VS_DRIVER)
									//If the driver is not the PED we're looking at
									IF thisPed != PlayerPedId
										//If they are ok
										IF DOES_ENTITY_EXIST(thisPed)
										AND NOT IS_PED_INJURED(thisPed)
											//If they are are player
											IF IS_PED_A_PLAYER(thisPed)
												PLAYER_INDEX l_player = NETWORK_GET_PLAYER_INDEX_FROM_PED(thisPed)
												//And they are a participant
												IF NETWORK_IS_PLAYER_A_PARTICIPANT(l_player)
												AND NETWORK_IS_PARTICIPANT_ACTIVE(NETWORK_GET_PARTICIPANT_INDEX(l_player))
													CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - MAINTTAIN_FIRST_PLACE_UI - fSameCarDisadvantage = 1.0")
													sLbData[iPartCount].fDist += 0.001		
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						iPartCount++
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		FOR i = 0 TO ciNUM_GANG_MEMBERS-1 STEP 1
			FOR j = i TO ciNUM_GANG_MEMBERS-1 STEP 1
				IF sLbData[i].fDist > sLbData[j].fDist
				AND	sLbData[j].fDist != 0
					sLbDataTemp = sLbData[i]
					sLbData[i] = sLbData[j]
					sLbData[j] = sLbDataTemp
				ENDIF
			ENDFOR
		ENDFOR
	ENDIF
		
	iTimeRemaining = (GB_P2P_GET_END_TIME() - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.stRaceOverTimer))

	IF iTimeRemaining < 0 
	OR NOT bRunLb
		iTimeRemaining = 0
	ENDIF
	
	HUD_COLOURS EventTimerColour = HUD_COLOUR_WHITE
	
	IF iTimeRemaining < ciP2P_TIMER_DROP
		EventTimerColour = HUD_COLOUR_RED
	ENDIF
	
	DRAW_CHALLENGE_DPAD_LBD(g_GBLeaderboardStruct.challengeLbdStruct, g_GBLeaderboardStruct.siDpadMovie, SUB_BOSS, g_GBLeaderboardStruct.dpadVariables, 
									g_GBLeaderboardStruct.fmDpadStruct)
	//If wthe player is active
	IF sLbData[0].PlayerId != INVALID_PLAYER_INDEX()
	AND NETWORK_IS_PLAYER_ACTIVE(sLbData[0].PlayerId)
	AND MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
		//Draw the bottom right UI
		STRING NumberString = "HUD_METNUM"
		FLOAT fDist[ciNUM_GANG_MEMBERS]
		IF SHOULD_USE_METRIC_MEASUREMENTS()
			REPEAT ciNUM_GANG_MEMBERS i
				fDist[i] = sLbData[i].fDist
			ENDREPEAT
		ELSE
			REPEAT ciNUM_GANG_MEMBERS i
				fDist[i] = CONVERT_METERS_TO_FEET(sLbData[i].fDist)
			ENDREPEAT
			NumberString = "HUD_FTNUM"
		ENDIF
		//SET_DECIMAL_POINTS_FOR_BOTTOM_RIGHT_HUD_THIS_FRAME(2)
		BOOL bForceRebuildNames
		//DRAW_GENERIC_SCORE(0, tl63WinningPlayer, DEFAULT, HUD_COLOUR_GOLD, HUDORDER_FIFTHBOTTOM, TRUE, NumberString, TRUE, fClosest, DEFAULT, DEFAULT, HUD_COLOUR_GOLD)	
		BOTTOM_RIGHT_UI_1STFLOAT_2NDFLOAT_3RDFLOAT_4THFLOAT_ATTEMPT_LOCALFLOAT_TIMER(	sLbData[0].PlayerId, 		//PLAYER_INDEX First_Player_Index, 
																						sLbData[1].PlayerId, 		//PLAYER_INDEX Second_Player_Index, 
																						sLbData[2].PlayerId, 		//PLAYER_INDEX Third_Player_Index, 
																						sLbData[3].PlayerId, 		//PLAYER_INDEX Third_Player_Index, 
																						fDist[0], 					//FLOAT First_Player_Score, 
																						fDist[1], 					//FLOAT Second_Player_Score, 
																						fDist[2], 					//FLOAT Third_Player_Score, 
																						fDist[3], 					//FLOAT Attempt_Display, 
																						0.0, 						//FLOAT iLocal_Score_Display, 
																						0.0, 						//FLOAT iLocal_Score_Display, 
																						iTimeRemaining, 			//INT Event_Timer_Display,
																						NumberString,				//STRING UnitString,
																						bForceRebuildNames,			//BOOL& bForceRebuildNames,
																						EventTimerColour, 			//HUD_COLOURS EventTimerColour = HUD_COLOUR_WHITE,
																						FALSE,						//BOOL bDisplayAttempt = TRUE, 
																						DEFAULT,					//INT FlashBackgroundtimerMS = 0,
																						"GB_CHAL_END",				//STRING ModeName = NULL, 
																						FMEVENT_SCORETITLE_NONE)	//FMEVENT_SCORETITLE LocalDisplayType = FMEVENT_SCORETITLE_GAMERTAG)
	ENDIF
ENDPROC


PROC MAINTAIN_P2P_MINIDISTANCE()
	FLOAT fPlayerDistance = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), serverBD.vDestination) 
	// Mark player as qualifying if we get within accepted distance of key area 
	GB_PROCESS_MIN_PARTICIPANT_DISTANT_CHECKS(FMMC_TYPE_GB_CHAL_P2P, fPlayerDistance) 
ENDPROC

FUNC BOOL GB_P2P_SHOULD_CLEAR_MIN_PARTICIPANT_DISTANT_CHECKS()
	FLOAT fDist = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), serverBD.vDestination)
	CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] GB_P2P_SHOULD_CLEAR_MIN_PARTICIPANT_DISTANT_CHECKS - fDist    = ", fDist)
	CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] GB_P2P_SHOULD_CLEAR_MIN_PARTICIPANT_DISTANT_CHECKS - min Dist = ", g_sMPTunables.igb_pointtopoint_participation_threshold_distance)
	RETURN fDist < g_sMPTunables.igb_pointtopoint_participation_threshold_distance
ENDFUNC

PROC MAINTAIN_RACES_SLIPSTREAM_SCENE()
	IF NOT g_sMPTunables.bgb_pointtopoint_disable_slipstream
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					IF GET_VEHICLE_CURRENT_TIME_IN_SLIP_STREAM(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) > 0.0
					AND GET_ENTITY_SPEED(PLAYER_PED_ID()) > 0
						IF NOT IS_BIT_SET(iBoolsBitSet, biSOUND_BIT_SLIP_STREAM)
							CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] START_AUDIO_SCENE(\"RACES_SLIPSTREAM_SCENE\")")
							START_AUDIO_SCENE("RACES_SLIPSTREAM_SCENE")
							IF iSlipSoundId = 0
								iSlipSoundId = GET_SOUND_ID()
								PLAY_SOUND_FRONTEND(iSlipSoundId, "SLIPSTREAM_MASTER")	
							ENDIF
							SET_BIT(iBoolsBitSet, biSOUND_BIT_SLIP_STREAM)
						ENDIF
					ELSE
						CLEANUP_RACES_SLIPSTREAM_SCENE()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_PRINT_WINNING_TICKER()
	INT iLoop
	TEXT_LABEL_15 tlTicker
	FOR iLoop = 0 TO (ciNUM_GANG_MEMBERS - 2)	
		IF serverBD.iWinnerPart[iLoop] != -1
		AND NOT IS_BIT_SET(iBoolsBitSet, biWinningTicker0 + iLoop)
			CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - MAINTAIN_PRINT_WINNING_TICKER")
			PLAYER_INDEX piWinningPlayer
			PARTICIPANT_INDEX piWinnerPart = INT_TO_PARTICIPANTINDEX(serverBD.iWinnerPart[iLoop])
			IF NETWORK_IS_PARTICIPANT_ACTIVE(piWinnerPart)
				CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - NETWORK_IS_PARTICIPANT_ACTIVE(serverBD.iWinnerPart[0])")
				piWinningPlayer = NETWORK_GET_PLAYER_INDEX(piWinnerPart)
				IF NETWORK_IS_PLAYER_ACTIVE(piWinningPlayer)
					CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - NETWORK_IS_PLAYER_ACTIVE(serverBD.iWinnerPart[0]) - player = ", GET_PLAYER_NAME(piWinningPlayer), " - PRINT_TICKER_WITH_PLAYER_NAME called")
					tlTicker = "GB_P2P_WT"
					tlTicker += iLoop
					PRINT_TICKER_WITH_PLAYER_NAME(tlTicker, piWinningPlayer)
				ENDIF	
			ENDIF	
			SET_BIT(iBoolsBitSet, biWinningTicker0 + iLoop)
		ENDIF	
	ENDFOR
ENDPROC

FUNC INT GB_P2P_GET_PART_FINISH_POS(INT iParticipant)
	INT iLoop
	REPEAT ciNUM_GANG_MEMBERS iLoop
		IF iParticipant = serverBD.iWinnerPart[iLoop]
			RETURN iLoop
		ENDIF
	ENDREPEAT
	RETURN -1
ENDFUNC

FUNC INT GB_GET_LAST_PLACE()
	IF GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID()) != INVALID_PLAYER_INDEX()
	AND GB_IS_THIS_PLAYER_USING_SPECTATE(PLAYER_ID())
		RETURN serverBD.iNumParticpants - 1
	ENDIF
	RETURN serverBD.iNumParticpants
ENDFUNC

PROC MAINTAIN_CROSSING_FINISH_LINE_SHARD()
	IF NOT IS_BIT_SET(iBoolsBitSet, biGotToEndShard)
	AND	IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biP_ReachedDestination)
	AND NOT GB_P2P_IS_RACE_OVER() 
		INT iPos = GB_P2P_GET_PART_FINISH_POS(PARTICIPANT_ID_TO_INT())
		IF iPos != -1
		AND iPos < GB_GET_LAST_PLACE()
			IF (GB_P2P_GET_END_TIME() - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.stRaceOverTimer)) > 2000
				TEXT_LABEL_15 tlTicker = "GB_P2P_WIN"
				tlTicker += iPos
				TEXT_LABEL_15 tlPos = "GB_WINNER"
				IF iPos != 0
					tlPos = "GB_WORK_OVER"
				ENDIF
				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, tlPos, tlTicker)
			ENDIF
			SET_BIT(iBoolsBitSet, biGotToEndShard)
		ENDIF
	ENDIF
ENDPROC

PROC CLIENT_PROCESSING()
	SWITCH playerBD[PARTICIPANT_ID_TO_INT()].eGEN_Stage
		CASE eGB_P2P_STAGE_START
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			DRAW_AND_BLIP_DESTINATION()
			REQUEST_MINIGAME_COUNTDOWN_UI(Race_CountDownUI)
			NETWORK_PREVENT_SCRIPT_HOST_MIGRATION()
			
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				IF IS_PLAYER_IN_CORONA()
					CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - MISSION END - PLAYER IN VOTE CORONA - SCRIPT CLEANUP D ")
					SCRIPT_CLEANUP()
				ENDIF
			ENDIF
			//Set up the intro shard
			IF NOT IS_BIT_SET(iBoolsBitSet, biIntroShard)
				IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
					CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - CLEAR_HELP() - serverBD.eGEN_Stage > eGB_P2P_STAGE_COUNTDOWN")
					CLEAR_HELP()
					CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_START_OF_JOB, \"GB_P2P_GO1\", \"GB_P2P_GO2\")")
					//SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_START_OF_JOB, "GB_BIGM_P2P_T", "GB_P2P_GO2")
					//SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_GB_START_OF_JOB, "GB_P2P_GO2", GB_GET_ORGANIZATION_NAME_AS_A_STRING(), DEFAULT, DEFAULT, "GB_BIGM_P2P_T")
					SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_START_OF_JOB, "GB_BIGM_P2P_T", "GB_P2P_GO2", GB_GET_ORGANIZATION_NAME_AS_A_STRING(), GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID()))
					SET_BIT(iBoolsBitSet, biIntroShard)
					CLEAR_HELP()
					GB_SET_COMMON_TELEMETRY_DATA_ON_START()
					CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - SERVER_PROCESSING - CLEAR_HELP")
					IF NOT GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
						MAINTAIN_GB_P2P_HELP(biH_SetUpHelpGoon, TRUE)
					ELSE
						MAINTAIN_GB_P2P_HELP(biH_SetUpHelpBoss, TRUE)
					ENDIF
				ENDIF 
			ENDIF 
			
			//If we've loaded the countdown UI and the message has gone
			IF serverBD.eGEN_Stage > eGB_P2P_STAGE_START
			AND IS_BIT_SET(serverBD.iServerBitSet, biS_WaypointVectorFound)
				GB_SET_ITS_SAFE_FOR_SPEC_CAM()
				//Clean up an on call if I'm starting it
				IF AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()
					SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW()
				ENDIF
				REMOVE_MIN_DISTANCE_RADIUS_BLIP()
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_GB_P2P_HELP_STRING(biH_SetUpHelpBoss))
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_GB_P2P_HELP_STRING(biH_SetUpHelpGoon))
					CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - eGB_P2P_STAGE_START - CLEAR_HELP")
					CLEAR_HELP()
				ENDIF
				Clear_Any_Objective_Text_From_This_Script()
				SET_USER_RADIO_CONTROL_ENABLED(TRUE)
				playerBD[PARTICIPANT_ID_TO_INT()].eGEN_Stage = eGB_P2P_STAGE_COUNTDOWN
				CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - PLAYER STAGE = eR2P_COUNTDOWN")
			ENDIF
		BREAK
		
		CASE eGB_P2P_STAGE_COUNTDOWN
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			REQUEST_MINIGAME_COUNTDOWN_UI(Race_CountDownUI)
			
			IF NOT IS_BIT_SET(iBoolsBitSet, biStartSetup)
				DELETE_WAYPOINTS_FROM_THIS_PLAYER()
				//vLocalStart = GET_ENTITY_COORDS(PLAYER_PED_ID())
				DISABLE_MP_PASSIVE_MODE(PMER_START_OF_A_MATCH, FALSE)
				SET_BIT(iBoolsBitSet, biStartSetup)
				CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - SET_BIT(iBoolsBitSet, biStartSetup) - biStartSetup DONE")
			ENDIF
			
			IF serverBD.eGEN_Stage > eGB_P2P_STAGE_COUNTDOWN
				IF HAS_MINIGAME_COUNTDOWN_UI_LOADED(Race_CountDownUI)
					CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_GB_START_OF_JOB)
					//do the 321
					SET_BITMASK_AS_ENUM(Race_CountDownUI.iBitFlags, CNTDWN_UI_Played_3)
					SET_BITMASK_AS_ENUM(Race_CountDownUI.iBitFlags, CNTDWN_UI_Played_2)
					SET_BITMASK_AS_ENUM(Race_CountDownUI.iBitFlags, CNTDWN_UI_Played_1)
					IF UPDATE_MINIGAME_COUNTDOWN_UI(Race_CountDownUI, TRUE, FALSE, TRUE, 0, TRUE, TRUE)
					//Or if we debug skip it
					#IF IS_DEBUG_BUILD
					OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
					OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
					OR bJorSSkipDone
					#ENDIF
						IF NOT g_sMPTunables.bgb_pointtopoint_disable_slipstream
							SET_ENABLE_VEHICLE_SLIPSTREAMING(TRUE)
						ENDIF
						HINT_MISSION_AUDIO_BANK("MP_RACES_SLIPSTREAM",FALSE)
						Print_Objective_Text("GB_P2P_GOD1") 
						Race_CountDownUI.iBitFlags = 0
						SET_PLAYER_RESPAWN_IN_VEHICLE(TRUE, FAGGIO2)
				      	CANCEL_TIMER(Race_CountDownUI.CountdownTimer)
				      	RELEASE_MINIGAME_COUNTDOWN_UI(Race_CountDownUI)
						playerBD[PARTICIPANT_ID_TO_INT()].eGEN_Stage = eGB_P2P_STAGE_RACE
						CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - PLAYER STAGE = eR2P_RACE")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE eGB_P2P_STAGE_RACE
			DRAW_AND_BLIP_DESTINATION()
			PROCESS_REACHING_DESTINATION()
			MAINTAIN_RACES_SLIPSTREAM_SCENE()
			
			MAINTAIN_GB_P2P_HELP(biH_SetUpHelpAfterGo)
			
			MAINTTAIN_FIRST_PLACE_UI()
			MAINTAIN_P2P_MINIDISTANCE()
			
			IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
				MAINTAIN_PRINT_WINNING_TICKER()
				MAINTAIN_CROSSING_FINISH_LINE_SHARD()
				IF GB_P2P_IS_RACE_OVER()
				OR HAS_NET_TIMER_EXPIRED(serverBD.stRaceOverTimer, GB_P2P_GET_END_TIME())
					IF NOT IS_BIT_SET(iBoolsBitSet, biReachedEndDestination)
					AND NOT GB_P2P_SHOULD_CLEAR_MIN_PARTICIPANT_DISTANT_CHECKS()
						GB_CLEAR_PLAYER_AS_QUALIFYING_PARTICIPANT()
						GB_CLEAR_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BG_BITSET_0_MIN_PARTICIPATION_INITIALISED)
					ENDIF
					IF serverBD.iWinnerPart[0] > -1
						IF GB_SHOULD_RUN_SPEC_CAM()
							CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - GB_SHOULD_RUN_SPEC_CAM - calling GB_SET_SHOULD_CLOSE_SPECTATE")
							GB_SET_SHOULD_CLOSE_SPECTATE()
						ENDIF
						GANG_BOSS_MANAGE_WANTED_CLEAN_UP(TRUE, INVALID_PLAYER_INDEX())
						CLEANUP_RACES_SLIPSTREAM_SCENE()
						Clear_Any_Objective_Text_From_This_Script()
						IF serverBD.iWinnerPart[0] = PARTICIPANT_ID_TO_INT()
							GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_CHAL_P2P, TRUE, sRewardsData)
							GB_SET_COMMON_TELEMETRY_DATA_ON_END(TRUE, GB_TELEMETRY_END_WON)
							SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "GB_WINNER", "GB_P2P_WIN3")
						ELSE
							GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_CHAL_P2P, FALSE, sRewardsData)
							GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST)
							
							IF GB_SHOULD_RUN_SPEC_CAM()
							AND NETWORK_IS_PLAYER_ACTIVE(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.iWinnerPart[0])))
								SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GB_END_OF_JOB_FAIL, NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.iWinnerPart[0])), -1, "GB_P2P_LOS4", "GB_CHAL_OVER")
							ELSE
								SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_FAIL, "GB_CHAL_OVER", "GB_P2P_LOS2")
							ENDIF
						ENDIF
						playerBD[PARTICIPANT_ID_TO_INT()].eGEN_Stage = eGB_P2P_STAGE_REWARD
						CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - PLAYER STAGE = eGB_P2P_STAGE_REWARD")
					ELSE
						GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_CHAL_P2P, FALSE, sRewardsData)
						//SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_CHAL_OVER", "GB_P2P_LOS3")
						//SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_GB_START_OF_JOB, "GB_P2P_LOS3", GB_GET_ORGANIZATION_NAME_AS_A_STRING(), DEFAULT, DEFAULT, "GB_CHAL_OVER")
						SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_CHAL_OVER", "GB_P2P_LOS3", GB_GET_ORGANIZATION_NAME_AS_A_STRING(), GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID()))
						GANG_BOSS_MANAGE_WANTED_CLEAN_UP(TRUE, INVALID_PLAYER_INDEX())
						Clear_Any_Objective_Text_From_This_Script()
						GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_TIME_OUT)
						IF GB_SHOULD_RUN_SPEC_CAM()
							CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - GB_SHOULD_RUN_SPEC_CAM - calling GB_SET_SHOULD_CLOSE_SPECTATE")
							GB_SET_SHOULD_CLOSE_SPECTATE()
						ENDIF
						playerBD[PARTICIPANT_ID_TO_INT()].eGEN_Stage = eGB_P2P_STAGE_REWARD
						CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - PLAYER STAGE = eGB_P2P_STAGE_REWARD HAS_NET_TIMER_EXPIRED")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE eGB_P2P_STAGE_REWARD
			IF GB_SHOULD_RUN_SPEC_CAM()
				CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - PLAYER STAGE = eGB_P2P_STAGE_CLEANUP - GB_SHOULD_RUN_SPEC_CAM")
			ELSE
				MAINTTAIN_FIRST_PLACE_UI(FALSE)
				IF GB_MAINTAIN_BOSS_END_UI(sGB_End_Struct)
					playerBD[PARTICIPANT_ID_TO_INT()].eGEN_Stage = eGB_P2P_STAGE_CLEANUP
					CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - PLAYER STAGE = eGB_P2P_STAGE_CLEANUP")
				ENDIF
			ENDIF
		BREAK
		
		CASE eGB_P2P_STAGE_CLEANUP
			CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - SCRIPT CLEANUP B ")
			SCRIPT_CLEANUP()
		BREAK
	ENDSWITCH
ENDPROC
PROC RESET_P2P_TIMER_IF_NEEDED()
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_TimerDrop)
		IF g_sMPTunables.igb_pointtopoint_time_limit - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.stRaceOverTimer) > ciP2P_TIMER_DROP
			REINIT_NET_TIMER(serverBD.stRaceOverTimer)
			SET_BIT(serverBD.iServerBitSet, biS_TimerDrop)
		ENDIF
	ENDIF
ENDPROC

PROC GB_P2P_SET_PARRT_POS(INT iParticipant)
	INT iLoop
	REPEAT ciNUM_GANG_MEMBERS iLoop
		IF serverBD.iWinnerPart[iLoop] = iParticipant
			EXIT
		ENDIF
		IF serverBD.iWinnerPart[iLoop] = -1
			serverBD.iWinnerPart[iLoop] = iParticipant
			CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - serverBD.iWinnerPart[", iLoop, "] = ", iParticipant)
			#IF IS_DEBUG_BUILD
			IF iLoop = 0
				CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - WINNER PLAYER  = ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iParticipant)))
			ENDIF
			#ENDIF
			EXIT
		ENDIF
	ENDREPEAT
ENDPROC


//PURPOSE: Runs through the max num participents and does relevant checks
PROC MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
	INT iParticipant, iFinished, iNotFinished
	
	IF iNotFinished = 0
	
	ENDIF
	
	IF NOT GB_P2P_IS_RACE_OVER()
		//Set Bits that can then be cleared in the repeat if necessary
		SET_BIT(serverBD.iServerBitSet, biS_AllCrewDead)
		SET_BIT(serverBD.iServerBitSet, biS_AllCrewLeftStartArea)
		SET_BIT(serverBD.iServerBitSet, biS_AllCrewHaveLeft)
		serverBD.iNumParticpants = 0
		
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
				PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
				PED_INDEX PlayerPedId = GET_PLAYER_PED(PlayerId)

				serverBD.iNumParticpants++

				//Check if all on the mission have left the start area
				IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllCrewLeftStartArea)
					IF IS_ENTITY_AT_COORD(PlayerPedId, serverBD.vStart, <<R2P_START_RADIUS, R2P_START_RADIUS, R2P_START_RADIUS>>)
						CLEAR_BIT(serverBD.iServerBitSet, biS_AllCrewLeftStartArea)
					ENDIF
				ENDIF
					
				IF IS_NET_PLAYER_OK(PlayerId)
					CLEAR_BIT(serverBD.iServerBitSet, biS_AllCrewDead)
					//Check if all on the mission have left the start area
					IF serverBD.eGEN_Stage = eGB_P2P_STAGE_RACE
					AND NOT GB_IS_THIS_PLAYER_USING_SPECTATE(PlayerId)
				
						//iFinishTime = MPGlobalsAmbience.R2Pdata.iReachedDestinationTime[iPlayer]
						IF IS_BIT_SET(playerBD[iParticipant].iClientBitSet, biP_ReachedDestination)
						AND HAS_NET_TIMER_EXPIRED(iServerProcessWinnerTimer, SERVER_PROCESS_WINNER_DELAY)
							iFinished++
							GB_P2P_SET_PARRT_POS(iParticipant)
							RESET_P2P_TIMER_IF_NEEDED()
						ELSE
							iNotFinished++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF serverBD.eGEN_Stage = eGB_P2P_STAGE_RACE
			IF serverBD.iNumParticpants - iFinished <= 1
			#IF IS_DEBUG_BUILD
			AND NOT GET_COMMANDLINE_PARAM_EXISTS("sc_IgnoreBossChallengeNoGoonCheck")
			#ENDIF
				CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - SET_BIT(serverBD.iServerBitSet, biS_AllPlayersAtEnd)")
				SET_BIT(serverBD.iServerBitSet, biS_AllPlayersAtEnd)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Clears the variables being used to get Waypoint vector
PROC CLEAR_WAYPOINT_VECTOR_LOADING() 
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_GetWaypointVector)
		IF NETWORK_IS_GAME_IN_PROGRESS()
			REMOVE_NAVMESH_REQUIRED_REGIONS()
		ENDIF
		CLEAR_BIT(serverBD.iServerBitSet, biS_GetWaypointVector)
		SPAWNPOINTS_CANCEL_SEARCH()
		SET_MINIMAP_BLOCK_WAYPOINT(FALSE)
		SET_USE_SET_DESTINATION_IN_PAUSE_MAP(FALSE)
		CPRINTLN(DEBUG_NET_AMBIENT, "[GBP2P] - SET_MINIMAP_BLOCK_WAYPOINT - FALSE - D")
		CPRINTLN(DEBUG_NET_AMBIENT, "[GBP2P] - CLEAR_WAYPOINT_VECTOR_LOADING - biS_GetWaypointVector")
	ENDIF
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_WaypointSearchStarted)
		SPAWNPOINTS_CANCEL_SEARCH()
		CLEAR_BIT(serverBD.iServerBitSet, biS_WaypointSearchStarted)
		CPRINTLN(DEBUG_NET_AMBIENT, "[GBP2P] - CLEAR_WAYPOINT_VECTOR_LOADING - biS_WaypointSearchStarted")
	ENDIF
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_WaypointVectorFound)
		CLEAR_BIT(serverBD.iServerBitSet, biS_WaypointVectorFound)
		CPRINTLN(DEBUG_NET_AMBIENT, "[GBP2P] - CLEAR_WAYPOINT_VECTOR_LOADING - biS_WaypointVectorFound")
	ENDIF
ENDPROC
FUNC BOOL ARE_ANY_PARTICIPANTS_AT_LOCATION(VECTOR vWaypoint)
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_SkipLocCheck")
		RETURN FALSE
	ENDIF
	#ENDIF
	INT iLoop
	BOOL bAllVectorsZero = TRUE
	REPEAT NUM_NETWORK_PLAYERS iLoop 
		IF NOT IS_VECTOR_ZERO(vPlayers[iLoop])
			IF GET_DISTANCE_BETWEEN_COORDS(vPlayers[iLoop], vWaypoint) < g_sMPTunables.igb_pointtopoint_destination_radius
				RETURN TRUE
			ENDIF
			bAllVectorsZero = FALSE
		ENDIF
	ENDREPEAT
	IF bAllVectorsZero
	AND GET_DISTANCE_BETWEEN_COORDS(serverBD.vStart, vWaypoint) < g_sMPTunables.igb_pointtopoint_destination_radius
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC VECTOR GET_DESTINATION_FROM_MENU()		
	VECTOR vWaypoint
	IF HAS_NET_TIMER_EXPIRED(serverBD.stPickStartPointTimer, g_sMPTunables.igb_pointtopoint_destination_select_time_limit)
		vWaypoint = <<GET_RANDOM_FLOAT_IN_RANGE(-2000.0, 2000.0), GET_RANDOM_FLOAT_IN_RANGE(-3500.0, 5500.0), 200>>
	ELIF IS_WAYPOINT_ACTIVE()
		vWaypoint = GET_BLIP_INFO_ID_COORD(GET_FIRST_BLIP_INFO_ID(GET_WAYPOINT_BLIP_ENUM_ID()))
	ENDIF
	IF NOT IS_VECTOR_ZERO(vWaypoint)			
		vWaypoint.z = GET_APPROX_HEIGHT_FOR_AREA(vWaypoint.x-5, vWaypoint.y-5, vWaypoint.x+5, vWaypoint.y+5)
		IF NOT ARE_ANY_PARTICIPANTS_AT_LOCATION(vWaypoint)
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_GetWaypointVector)
				CLEAR_WAYPOINT_VECTOR_LOADING()
				
				
				SET_MINIMAP_BLOCK_WAYPOINT(TRUE)
				CPRINTLN(DEBUG_NET_AMBIENT, "[GBP2P] - SET_MINIMAP_BLOCK_WAYPOINT - TRUE - A") 
				
				SET_BIT(serverBD.iServerBitSet, biS_GetWaypointVector)
				CPRINTLN(DEBUG_NET_AMBIENT, "[GBP2P] - GET_DESTINATION_FROM_MENU - START LOOKING FOR WAYPOINT VECTOR - vWaypoint = ", vWaypoint) 
				
			ELSE
				CPRINTLN(DEBUG_NET_AMBIENT, "[GBP2P] - GET_DESTINATION_FROM_MENU - biS_GetWaypointVector - ALREADY SET") 
			ENDIF
		
			RETURN vWaypoint
		ENDIF
	ENDIF
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

//PURPOSE: Check to see if the Host 
FUNC BOOL HAS_RACE_BEEN_STARTED(VECTOR &vStart)
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_GetWaypointVector)
	AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_WaypointVectorFound)
		RETURN FALSE
	ENDIF
	
	VECTOR vPos = GET_DESTINATION_FROM_MENU()
	IF ARE_ANY_PARTICIPANTS_AT_LOCATION(vPos)  
		CPRINTLN(DEBUG_NET_AMBIENT, "[GBP2P] - HAS_RACE_BEEN_STARTED = FALSE - TOO CLOSE") 
		RETURN FALSE
	ENDIF
	
	IF ARE_VECTORS_EQUAL(vPos, <<0, 0, 0>>)
	OR ARE_VECTORS_EQUAL(vPos, <<0, 0, -2000>>)
		//CPRINTLN(DEBUG_NET_AMBIENT, "[GBP2P] - HAS_RACE_BEEN_STARTED = FALSE - NOT VALID") 
		RETURN FALSE
	ENDIF

	IF IS_WAYPOINT_ACTIVE()
	OR (HAS_NET_TIMER_EXPIRED(serverBD.stPickStartPointTimer, g_sMPTunables.igb_pointtopoint_destination_select_time_limit) AND IS_BIT_SET(serverBD.iServerBitSet, biS_GetWaypointVector))
		CPRINTLN(DEBUG_NET_AMBIENT, "[GBP2P] - HAS_RACE_BEEN_STARTED = TRUE") 
		vStart = vPos
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC	
FUNC BOOL IS_BLOCKED_WATER_AREA(VECTOR vLocation)
	//FUN FAIR PIER
	IF IS_POINT_IN_ANGLED_AREA(vLocation, <<-1836.619019,-1262.521606,-42.180431>>, <<-1664.868164,-1060.218994,119.500671>>, 150)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

//PURPOSE: Checks if there is a suitable road node in the area and uses that.
FUNC BOOL GET_SUITABLE_ROAD_NODE()
	REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(serverBD.vDestination.x-SAFE_NODE_SEARCH_RADIUS, serverBD.vDestination.y-SAFE_NODE_SEARCH_RADIUS, serverBD.vDestination.x+SAFE_NODE_SEARCH_RADIUS, serverBD.vDestination.y+SAFE_NODE_SEARCH_RADIUS)
	IF ARE_NODES_LOADED_FOR_AREA(serverBD.vDestination.x-SAFE_NODE_SEARCH_RADIUS, serverBD.vDestination.y-SAFE_NODE_SEARCH_RADIUS, serverBD.vDestination.x+SAFE_NODE_SEARCH_RADIUS, serverBD.vDestination.y+SAFE_NODE_SEARCH_RADIUS)
		VECTOR vTemp
		GET_CLOSEST_VEHICLE_NODE(serverBD.vDestination, vTemp)
		IF GET_DISTANCE_BETWEEN_COORDS(serverBD.vDestination, vTemp, FALSE) <= SAFE_NODE_SEARCH_RADIUS
			CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - GET_SUITABLE_ROAD_NODE - serverBD.vDestination = ", serverBD.vDestination)
			CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - GET_SUITABLE_ROAD_NODE - FOUND NODE = ", vTemp)
			serverBD.vDestination = vTemp
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
PROC FIND_WAYPOINT_VECTOR_DONE()
	CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - FIND_WAYPOINT_VECTOR_DONE - WAYPOINT VECTOR FOUND - USING SPAWNPOINTS - serverBD.vDestination = ", serverBD.vDestination)
	DELETE_WAYPOINTS_FROM_THIS_PLAYER()
	SET_BIT(serverBD.iServerBitSet, biS_WaypointVectorFound)
	SET_BIT(serverBD.iServerBitSet, biS_FirstWaypointVectorFound)
ENDPROC

PROC FIND_WAYPOINT_VECTOR()//BOOL &bResetMenuNow)
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_WaypointVectorFound)
		IF IS_BIT_SET(serverBD.iServerBitSet, biS_WaypointSearchStarted)
			FLOAT fTemp
			VECTOR vTempWater = serverBD.vDestination
			vTempWater.z += 100.0
			serverBD.vDestination.z = (GET_APPROX_FLOOR_FOR_AREA(serverBD.vDestination.x-R2P_RADIUS_FOR_POINT, serverBD.vDestination.y-R2P_RADIUS_FOR_POINT, serverBD.vDestination.x+R2P_RADIUS_FOR_POINT, serverBD.vDestination.y+R2P_RADIUS_FOR_POINT) + 14)
			CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - FIND_WAYPOINT_VECTOR - serverBD.vDestination = ", serverBD.vDestination)
			VECTOR vTemp = serverBD.vDestination
			SPAWN_SEARCH_PARAMS SpawnSearchParams
			SpawnSearchParams.bPreferPointsCloserToRoads=TRUE
			SpawnSearchParams.fMinDistFromPlayer=0.0
			SpawnSearchParams.bCloseToOriginAsPossible=TRUE
			IF NOT HAS_NET_TIMER_STARTED(stWaterTimer)
				START_NET_TIMER(stWaterTimer)
			ENDIF
			IF GET_SUITABLE_ROAD_NODE()			//Check for Road Node   -		Below (Check for Safe Entity Coords)			
				CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - FIND_WAYPOINT_VECTOR - GET_SUITABLE_ROAD_NODE")
				FIND_WAYPOINT_VECTOR_DONE()
			ELIF GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY(vTemp, SAFE_COORDS_SEARCH_RADIUS, serverBD.vDestination, fTemp, SpawnSearchParams)
				CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - FIND_WAYPOINT_VECTOR - GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY")
				FIND_WAYPOINT_VECTOR_DONE()
			ELIF HAS_NET_TIMER_EXPIRED(stWaterTimer, 1000)
				IF(GET_WATER_HEIGHT_NO_WAVES(vTempWater, fTemp)	AND NOT IS_BLOCKED_WATER_AREA(vTemp)) //Check if point is on water
					VECTOR vTempForWater
					IF TEST_PROBE_AGAINST_WATER(<<serverBD.vDestination.x, serverBD.vDestination.y, serverBD.vDestination.z + 10>>, <<serverBD.vDestination.x, serverBD.vDestination.y, serverBD.vDestination.z-16>>, vTempForWater)
						CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - FIND_WAYPOINT_VECTOR - GET_WATER_HEIGHT_NO_WAVES- serverBD.vDestination.z = ",fTemp)
						serverBD.vDestination.z = fTemp
					ENDIF
					CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - FIND_WAYPOINT_VECTOR - GET_WATER_HEIGHT_NO_WAVES")
					FIND_WAYPOINT_VECTOR_DONE()
				ENDIF
			ENDIF
		ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_GetWaypointVector)
			SET_BIT(serverBD.iServerBitSet, biS_WaypointSearchStarted)
			CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - START LOOKING FOR WAYPOINT SPAWN - SEARCH AT ", serverBD.vDestination)
		ENDIF
	ENDIF
ENDPROC
PROC MOVE_TO_SERVER_STATE_STAGE_COUNTDOWN()
	CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - SERVER_PROCESSING - MOVE_TO_SERVER_STATE_STAGE_COUNTDOWN")
	IF NOT IS_PLAYER_IN_CORONA()
		SET_FRONTEND_ACTIVE(FALSE)
	ENDIF
	SET_USE_SET_DESTINATION_IN_PAUSE_MAP(FALSE)
	START_NET_TIMER(serverBD.stRaceOverTimer)
	serverBD.eGEN_Stage = eGB_P2P_STAGE_COUNTDOWN
	CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - SERVER_PROCESSING - serverBD.eGEN_Stage = eGB_P2P_STAGE_COUNTDOWN")
ENDPROC

PROC SERVER_PROCESSING()
	VECTOR vStart
	SWITCH serverBD.eGEN_Stage
		CASE eGB_P2P_STAGE_START			
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_WaitForSpecActive)
				IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
					MAINTAIN_COUNT_DOWN_TIMERS("GB_PICK_D", g_sMPTunables.igb_pointtopoint_destination_select_time_limit, serverBD.stPickStartPointTimer)
				ENDIF
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_UseWayPoint)
					SET_BIT(serverBD.iServerBitSet, biS_UseWayPoint)
					CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - SERVER_PROCESSING - SET_USE_SET_DESTINATION_IN_PAUSE_MAP(TRUE)")
					SET_USE_SET_DESTINATION_IN_PAUSE_MAP(TRUE)
				ENDIF
				ADD_MIN_DISTANCE_RADIUS_BLIP()
				//Move on once server has started race
				IF HAS_RACE_BEEN_STARTED(vStart)
					GB_SET_ITS_SAFE_FOR_SPEC_CAM()
					serverBD.vDestination = vStart
					FIND_WAYPOINT_VECTOR()
					IF GB_LAUNCH_CHALLENGE_WITH_SPEC_CAM()
						SET_BIT(serverBD.iServerBitSet, biS_WaitForSpecActive)
						CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - SERVER_PROCESSING - SET_BIT(serverBD.iServerBitSet, biS_WaitForSpecActive)")
						//Clean up an on call if I'm starting it
						IF AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()
							SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW()
						ENDIF
						REMOVE_MIN_DISTANCE_RADIUS_BLIP()
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_GB_P2P_HELP_STRING(biH_SetUpHelpBoss))
							CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - eGB_P2P_STAGE_START - CLEAR_HELP")
							CLEAR_HELP()
						ENDIF
						Clear_Any_Objective_Text_From_This_Script()
						IF IS_PAUSE_MENU_ACTIVE()
							SET_FRONTEND_ACTIVE(FALSE)
						ENDIF
					ELSE
						MOVE_TO_SERVER_STATE_STAGE_COUNTDOWN()
					ENDIF
				ENDIF
			ELIF IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData)
				FIND_WAYPOINT_VECTOR()
				IF NOT IS_SCREEN_FADING_OUT()
				AND NOT IS_SCREEN_FADED_OUT()
				AND IS_BIT_SET(sSpecVars.iBitSet, ciGB_SPEC_BS_DONE_CLEAR)
				AND IS_BIT_SET(serverBD.iServerBitSet, biS_WaypointVectorFound)
					CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] [GBP2P] - SERVER_PROCESSING - IS_THIS_SPECTATOR_CAM_ACTIVE = TRUE")
					MOVE_TO_SERVER_STATE_STAGE_COUNTDOWN()
				ENDIF
			ENDIF
		BREAK
		
		CASE eGB_P2P_STAGE_COUNTDOWN
			FIND_WAYPOINT_VECTOR()
			serverBD.StartTime = GET_NETWORK_TIME()
			serverBD.eGEN_Stage = eGB_P2P_STAGE_RACE
			CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - SERVER_PROCESSING - serverBD.eGEN_Stage = eGB_P2P_STAGE_RACE")
		BREAK
		
		CASE eGB_P2P_STAGE_RACE
			FIND_WAYPOINT_VECTOR()
			IF GB_P2P_IS_RACE_OVER()
			OR HAS_NET_TIMER_EXPIRED(serverBD.stRaceOverTimer, GB_P2P_GET_END_TIME())
				serverBD.eGEN_Stage = eGB_P2P_STAGE_REWARD
				CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - SERVER_PROCESSING - serverBD.eGEN_Stage = eGB_P2P_STAGE_REWARD")
			ENDIF
		BREAK
		
		CASE eGB_P2P_STAGE_REWARD
			serverBD.eGEN_Stage = eGB_P2P_STAGE_CLEANUP
			CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - SERVER_PROCESSING - serverBD.eGEN_Stage = eGB_P2P_STAGE_CLEANUP")
		BREAK
		
		CASE eGB_P2P_STAGE_CLEANUP
			
		BREAK

	ENDSWITCH

ENDPROC



FUNC BOOL INIT_CLIENT()
	RETURN TRUE
ENDFUNC

FUNC BOOL INIT_SERVER()
	CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] PROCESS_PRE_GAME")
	CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] DELETE_WAYPOINTS_FROM_THIS_PLAYER")
	DELETE_WAYPOINTS_FROM_THIS_PLAYER()
	PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBD.iMatchId1, serverBD.iMatchId2)
	RETURN TRUE
ENDFUNC


FUNC BOOL PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)
	INT iLoop
	
	REPEAT ciNUM_GANG_MEMBERS iLoop
		serverBD.iWinnerPart[iLoop] = -1
	ENDREPEAT
	
	CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] PROCESS_PRE_GAME")
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission), missionScriptArgs)
	
	//Reserve Entities
	RESERVE_NETWORK_MISSION_OBJECTS(0)
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD,SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD,SIZE_OF(playerBD))
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	// KGM: Wait for the first network broadcast before moving on - call this as the last instruction in pre-game
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] Failed to receive initial network broadcast. Cleaning up.")
		SCRIPT_CLEANUP()
	ENDIF

	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		serverBD.vStart = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
		START_NET_TIMER(serverBD.stPickStartPointTimer)
	ENDIF
	
	GB_COMMON_BOSS_MISSION_PREGAME()
	
	RETURN TRUE
	
ENDFUNC

//----------------------
//	MAIN SCRIPT
//----------------------
#IF IS_DEBUG_BUILD
PROC MAINTAIN_DEBUG_PROCS()
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
		IF NOT IS_VECTOR_ZERO(serverBD.vDestination)
			CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)")
			bDoJSkip = TRUE
			bJorSSkipDone = TRUE
			CLEAR_ALL_BIG_MESSAGES()
		ENDIF
	ENDIF
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
		IF NOT IS_VECTOR_ZERO(serverBD.vDestination)
			CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)")
			bDoJSkip = TRUE
			bJorSSkipDone = TRUE
			CLEAR_ALL_BIG_MESSAGES()
		ENDIF
	ENDIF
	IF bDoJSkip
		IF NET_WARP_TO_COORD(<<serverBD.vDestination.x + 10, serverBD.vDestination.y, serverBD.vDestination.z>>, 0.0)
			CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - NET_WARP_TO_COORD(", serverBD.vDestination, ", 0.0) - bDoJSkip = FALSE")
			bDoJSkip = FALSE
		ENDIF
	ENDIF
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
		SCRIPT_CLEANUP()
	ENDIF

ENDPROC
#ENDIF

PROC MAINTAIN_SCRIPT_CLEAN_UP_CALLS()
	IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")
		GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LEFT)
		SCRIPT_CLEANUP()
	ENDIF
	IF GB_SHOULD_KILL_ACTIVE_BOSS_MISSION()
		CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] GB_SHOULD_KILL_ACTIVE_BOSS_MISSION")
		GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_FORCED)
		SCRIPT_CLEANUP()
	ENDIF
	IF GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION()
		CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION")
		IF GB_HAS_LOCAL_PLAYER_LEFT_INITIAL_MISSION_GANG(FALSE)
			GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LEFT)
		ELSE
			GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_FORCED)
		ENDIF
		SCRIPT_CLEANUP()
	ENDIF
	
	IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
		IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
		AND GB_GET_NUM_GOONS_IN_PLAYER_GANG(PLAYER_ID()) = 0
		#IF IS_DEBUG_BUILD
		AND NOT GET_COMMANDLINE_PARAM_EXISTS("sc_IgnoreGBMissionLaunchChecks")
		#ENDIF
			CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] GB_GET_NUM_GOONS_IN_PLAYER_GANG(PLAYER_ID()) =")
			SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_CHAL_OVER", "PIM_MAGM101")
			GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOW_NUMBERS)
			IF PARTICIPANT_ID_TO_INT() != -1
				IF playerBD[PARTICIPANT_ID_TO_INT()].eGEN_Stage <= eGB_P2P_STAGE_REWARD
					GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_CHAL_P2P, FALSE, sRewardsData)
				ENDIF
			ENDIF
			SCRIPT_CLEANUP()
		ELIF GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID()) = INVALID_PLAYER_INDEX()
		OR NOT NETWORK_IS_PLAYER_ACTIVE(GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID()))
			CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] NETWORK_IS_PLAYER_ACTIVE(GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID()))")
			//SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_CHAL_OVER", "GBHIT_BIG_BSLFT")
			GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_BOSS_LEFT)
			IF PARTICIPANT_ID_TO_INT() != -1
				IF playerBD[PARTICIPANT_ID_TO_INT()].eGEN_Stage <= eGB_P2P_STAGE_REWARD
					GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_CHAL_P2P, FALSE, sRewardsData)
				ENDIF
			ENDIF
			SCRIPT_CLEANUP()
		ENDIF
	ENDIF
ENDPROC
//#IF FEATURE_NEW_CORONAS
//#IF IS_DEBUG_BUILD
//INT iAlpha
//#ENDIF
//#ENDIF	
SCRIPT(MP_MISSION_DATA missionScriptArgs)

	CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] * START")
		
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NOT PROCESS_PRE_GAME(missionScriptArgs)
			SCRIPT_CLEANUP()
		ENDIF	
	ENDIF
	
	CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] SET_MINIMAP_BLOCK_WAYPOINT(FALSE)")
	SET_MINIMAP_BLOCK_WAYPOINT(FALSE)
	SET_USE_SET_DESTINATION_IN_PAUSE_MAP(FALSE)
	
	//Request the count down UI
	REQUEST_MINIGAME_COUNTDOWN_UI(Race_CountDownUI)
	
	//Set that we're a PERMANENT PARTICIPANT
	GB_SET_PLAYER_AS_PERMANENT_ON_BOSS_MISSION()
	
	WHILE (TRUE)
	
		MAINTAIN_SCRIPT_CLEAN_UP_CALLS()
		
		IF GB_MAINTAIN_SPECTATE(sSpecVars, DEFAULT, GB_SHOULD_CLOSE_SPECTATE())
			CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] GB_MAINTAIN_SPECTATE returning TRUE")
		ENDIF
		
		SWITCH(GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT()))
			CASE GAME_STATE_INIT
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					GB_COMMON_BOSS_MISSION_SETUP(FMMC_TYPE_GB_CHAL_P2P)
					SET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT(),GAME_STATE_RUNNING)
				ENDIF
			BREAK
			CASE GAME_STATE_RUNNING
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					CLIENT_PROCESSING()
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					SET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT(),GAME_STATE_END)
				ENDIF
			BREAK
			CASE GAME_STATE_END
				SCRIPT_CLEANUP()
			BREAK
		ENDSWITCH
	
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()		
			SWITCH(GET_SERVER_MISSION_STATE())	
				CASE GAME_STATE_INIT
					IF INIT_SERVER()
						SET_SERVER_MISSION_STATE(GAME_STATE_RUNNING)
					ENDIF
				BREAK
				CASE GAME_STATE_RUNNING
					MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
					SERVER_PROCESSING()
				BREAK
				CASE GAME_STATE_END
				BREAK
			ENDSWITCH
		ENDIF
		
		MP_LOOP_WAIT_ZERO()
		
		#IF IS_DEBUG_BUILD
			MAINTAIN_DEBUG_PROCS()
		#ENDIF
	ENDWHILE
	
	
	SCRIPT_CLEANUP()
	
// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
