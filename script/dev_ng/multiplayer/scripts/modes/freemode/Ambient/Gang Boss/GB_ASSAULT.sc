//////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_GB_ASSAULT.sc														//
// Description: Boss Work where players have to steal a packege and deliver it			//
// Written by:  David Watson															//
// Date: 7/10//2015																		//
//////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "net_mission.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_event.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"

//
USING "net_scoring_common.sch"
USING "net_ambience.sch"

//USING "help_at_location.sch"
USING "commands_object.sch"

USING "net_xp_animation.sch"

USING "net_wait_zero.sch"
USING "net_gang_boss.sch"

USING "freemode_events_header.sch" // for HANDLE_MAP_EXITING_PLAYERS
USING "DM_Leaderboard.sch"


#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF

// Game States. 
CONST_INT GAME_STATE_INI 				0
CONST_INT GAME_STATE_RUNNING			1
CONST_INT GAME_STATE_LEAVE				2
CONST_INT GAME_STATE_TERMINATE_DELAY	3
CONST_INT GAME_STATE_END				4

ENUM GB_ASSAULT_STAGE_ENUM
	eGB_INIT,
	eGB_GET_TO_LOCATION,
	eGB_GET_PACKAGE,
	eGB_OFF_MISSION,
//	eAD_EXPLODE_VEH,
	eAD_CLEANUP
ENDENUM




CONST_FLOAT ACTIVE_DIST2 		250000.0 //500m
CONST_FLOAT NONACTIVE_DIST2 	562500.0 //750m
CONST_INT MAX_PLAYERS	4

CONST_INT MAX_GB_ASSAULT_PEDS	15
CONST_INT MAX_GB_ASSUALT_VEHS	5


CONST_INT MAX_MW_PED_SPAWN_LOC	5
CONST_INT MAX_MW_VEH_SPAWN_LOC	5
CONST_INT MAX_MW_HEL_SPAWN_LOC	5


CONST_INT MAX_MW_PEDS_PER_VEH	4

CONST_INT MAX_VEH_AT_ONCE		3

CONST_INT TIME_MISSION_START	60000

CONST_INT TIME_EXPOLDE_VEH		30000

CONST_INT TIME_MISSION_DURATION	60000 //60000 * 30 //-- Half hour

CONST_INT TIME_FORCE_RIVALS_JOIN	600000


//Server Bitset Data
CONST_INT biS_AllPlayersFinished		0
CONST_INT biS_PackageDelivered			1
CONST_INT biS_StartDuration				2
CONST_INT biS_DurationExpired			3
CONST_INT biS_RivalGotPackage			4
CONST_INT biS_BossLaunchedQuit			5
CONST_INT biS_AiPackageHolderKilled		6
CONST_INT biS_SomeoneAtLoc				7
CONST_INT biS_VehDelivered				8
CONST_INT biS_VehicleWrecked			9
CONST_INT biS_PlayerDestroyedVeh		10
CONST_INT biS_TargetsApproached			11
CONST_INT biS_ShootNearTarget			12
CONST_INT biS_AimingAtTargets			13
CONST_INT biS_DamagedTargets			14
CONST_INT biS_NonPlayerDestroyedVeh		15
CONST_INT biS_CollectedAtLeastOnce		16
CONST_INT biS_TurnedOffGenerators		17
CONST_INT biS_NonPartDestRhino			18
CONST_INT biS_ForceSpawnPackage			19
CONST_INT biS_ForceRivalsOnMission		20 

CONST_INT PED_AI_STATE_CREATED			0
CONST_INT PED_AI_STATE_PLAYERS_APPROACH	1
CONST_INT PED_AI_STATE_GOTO_PLAYERS		2
CONST_INT PED_AI_STATE_ATTACK_PLAYERS	3
CONST_INT PED_AI_STATE_CLEAN_UP			4
CONST_INT PED_AI_STATE_FINISHED			5
// The server broadcast data.
// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData
	INT iServerGameState
		
	INT iServerBitSet
	INT iServerLockedDoorsBitset
	
	INT iVariation = -1
	INT iDropOff = -1
	
	INT iPartPackageHolder = -1
	
	INT iPlayerWinPackage = -1
	INT iPartInVeh = -1
	INT iBossPartWhoLaunched = -1
	INT iBossPlayerWhoLaunched = -1
	INT iVehClearAreaBitset
	INT iPedClearAreaBitset
	
	NETWORK_INDEX niPackage
	NETWORK_INDEX niPed[MAX_GB_ASSAULT_PEDS]
	NETWORK_INDEX niVeh[MAX_GB_ASSUALT_VEHS]
	
	INT iPedState[MAX_GB_ASSAULT_PEDS]
	
	GB_ASSAULT_STAGE_ENUM eStage = eGB_INIT
	
	SCRIPT_TIMER MissionTerminateDelayTimer
	SCRIPT_TIMER timeDuration
	SCRIPT_TIMER timeWrecked
	SCRIPT_TIMER timeSpawnPackage
	SCRIPT_TIMER timeForceRivalsJoin
	INT iMinPlayers = -1
	
	INT iMatchId1
	INT iMatchId2
	
	#IF IS_DEBUG_BUILD
	BOOL bAllowWith2Players = FALSE
	BOOL bSomeoneSPassed
	#ENDIF
ENDSTRUCT
ServerBroadcastData serverBD


//CONST_INT biP_BossWhoLaunched				0
CONST_INT biP_Finished						1
CONST_INT biP_GotThePackage					2
CONST_INT biP_PackageDelivered				3
CONST_INT biP_DoneStartBigMessage			4
CONST_INT biP_JoinedAsRival					5
CONST_INT biP_RivalGotThePackage			6
CONST_INT biP_GotToLocation					7
CONST_INT biP_InVeh							8
CONST_INT biP_VehDelivered					9
CONST_INT biP_VehWrecked					10
CONST_INT biP_DestroyedVeh					11
CONST_INT biP_ApproachedTargets				12
CONST_INT biP_ShotNearTargets				13
CONST_INT biP_AimAtTargets					14
CONST_INT biP_DestroyedByNonPlayer			15
CONST_INT biP_NonPartDestroyedRhino			16

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	INT iGameState
	
	INT iPlayerBitSet
	
	INT iDamagedAiPedBitset
	
	INT iClearPlayerBlipBitset
	INT iMyBossPart = -1
	GB_ASSAULT_STAGE_ENUM eStage = eGB_INIT
	
	#IF IS_DEBUG_BUILD
	INT iPDebugBitset
	#ENDIF
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]



///// MISSION VARIABLES /////
INT iBoolsBitSet
CONST_INT biL_FlashedBlip			0
CONST_INT biL_DoneLaunchHelp		1
CONST_INT biL_DoneRivalLaunchHelp	2
CONST_INT biL_ApproachPackage		3
CONST_INT biL_RemovedMilGatesIpl	4
CONST_INT biL_PackageFirstDropped	5
CONST_INT biL_DoneIntroTextMsg		6
CONST_INT biL_SetupSpawnCoord		7
CONST_INT biL_PlayerDied			8
CONST_INT biL_DisabledForPassive	9
CONST_INT biL_ResetMaxWanted		10
CONST_INT biL_GivenWantedNearPack	11
CONST_INT biL_DoSubHelp				12
CONST_INT biL_DoneSubHelp			13
CONST_INT biL_ExitVeh				14
CONST_INT biL_DoneStealEntityTxt	15
CONST_INT biL_SetupCustomSpawns		16
CONST_INT biL_DoneTelemetry			17
CONST_INT biL_ResetWeapon			18
CONST_INT biL_CountdownAudio		19
CONST_INT biL_InitAudio				20
CONST_INT biL_DoneStartMusic		21
CONST_INT biL_DoneCollectMusic		22
CONST_INT biL_DoneDeliverMusic		23
CONST_INT biL_ForcedOffScript		24
CONST_INT biL_StoppedCountdownMusic	25
CONST_INT biL_DoneKill30s			26
CONST_INT biL_DoneFadeInRadio		27
CONST_INT biL_Started30sCountdown	28
CONST_INT biL_ShouldStop30s			29
CONST_INT biL_DonePre30sStop		30
CONST_INT biL_DoneStart30s			31


INT iBoolsBitSet2
CONST_INT biL2_PrepareKill30s			0
CONST_INT biL2_RestoreRadio				1
CONST_INT biL2_DoneReachedGotoHelp		2
CONST_INT biL2_DoYachtWarpHelp			3
CONST_INT biL2_DoneYachtWarpHelp		4
CONST_INT biL2_GangMemberWhenStarted	5
CONST_INT biL2_WaitForShard				6
CONST_INT biL2_SHouldSuppressScenarios	7
CONST_INT biL2_ResetAudioForEndShard	8
CONST_INT biL2_ResetAllPlayerBlips		9
CONST_INT biL2_RivalsJoinedHelp			10
CONST_INT bil2_ResetAsRestricted		11
CONST_INT bil2_CriticalToFmEvent		12

INT iStaggeredParticipant

VECTOR vStripClubCoords = <<104.44887, -1320.72302, 28.26478>>

//BLIP_INDEX VehBlip
//BLIP_INDEX MwPedBlip[MAX_MW_PEDS]
//BLIP_INDEX MwVehBlip[MAX_MW_VEHS]

AI_BLIP_STRUCT gBPedBlipData[MAX_GB_ASSAULT_PEDS]

//--Relationship groups

//REL_GROUP_HASH relUWPlayer
//REL_GROUP_HASH relUWAi 
//REL_GROUP_HASH relMyFmGroup

//SCRIPT_TIMER OutOfVehTimer

VECTOR vCreateEntityCurrentAttemptedCoords
FLOAT fCreateEntityCurrentAttemptedHeading

BLIP_INDEX blipPackage
BLIP_INDEX blipDropOff
BLIP_INDEX blipLoc
BLIP_INDEX blipVeh

INT iLocalPartPackageHolder = -1
INT iLocalPartVehDriver = -1

INT iMyCurrentWantedLevel

INT iDurationSound = -1
INT iPickupSound = -1

INT iPackageOnYachtProg
INT iOnYachtCount
VECTOR vPackageAvoidSpawnCoords
VECTOR vWarpPackLoc
FLOAT fWarpPackageHeading

WEAPON_TYPE weaponPlayer
BOOL bDoneDistanceCheck

GB_STRUCT_BOSS_END_UI gbBossEndUi
//HUD_COLOURS hcMyGangColour
//INT iMyGangID
// Widgets & Debug.
#IF IS_DEBUG_BUILD
	BOOL bHostEndMissionNow
	
	CONST_INT biPDebug_PressedS		0
	
	BOOL bWdRemoveGates
	BOOL bWdAddGates
	
	INT iDebugSpawn = -1
	
	BOOL bWdShowDistFromStart
	FLOAT fWdDistFromStart
	
	BOOL bWdTestExplode
	
	SCRIPT_TIMER timeDebug
#ENDIF

//PROC FM_NEW_EVENTDRAW_MARKER_ABOVE_VEHICLE(VEHICLE_INDEX viTemp, INT r, INT g, INT b )
//	VECTOR returnMin, returnMax
//	FLOAT fOffset
//	
//
//	fOffset = 0.5
//	
//	
//	GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(viTemp), returnMin, returnMax)
//
//	
//	FLOAT fCentreHeight = (returnMax.z - returnMin.z)/2
//	FLOAT fZdiff = returnMax.z - fCentreHeight
//	
//	//-- Old version used <<0,0,((returnMax.z - returnMin.z)/2) + fOffset>> as the offset from the vehicle position
//	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - fOffset before check = ", fOffset, " returnMax.z = ", returnMax.z, " returnMin.z = ", returnMin.z, " fZdiff = ", fZdiff, " fCentreHeight = ", fCentreHeight )
//	
//	//--  But no guarantee this is > returnMax.z
//	IF fOffset <= (fZdiff + 0.1)
//		fOffset = fZdiff + 0.5
//	ENDIF
//	
//	
//	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - USING OFFSET fOffset = ", fOffset)
//	DRAW_MARKER(MARKER_ARROW, GET_ENTITY_COORDS(viTemp)+<<0,0,((returnMax.z - returnMin.z)/2) + fOffset>>, <<0,0,0>>, <<180,0,0>>, <<0.5,0.5,0.5>>, r, g, b, 100, TRUE, TRUE)
//ENDPROC

///	PURPOSE:
///    Figure out which variation (location) to launch    
FUNC INT GET_GB_ASSAULT_VARIATION()
	IF serverBD.iVariation != -1
		//-- Variation already setup
		RETURN serverBD.iVariation
	ENDIF
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF serverBD.iVariation = -1
			#IF IS_DEBUG_BUILD
			//-- Launched from debug menu
			IF MPGlobalsAmbience.iGBAssaultDebugVar != -1
				serverBD.iVariation = MPGlobalsAmbience.iGBAssaultDebugVar
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [GET_GB_ASSAULT_VARIATION] LAUNCHING VARIATION FROM DEBUG MENU ", MPGlobalsAmbience.iGBAssaultDebugVar)
			ENDIF	
			#ENDIF
			
			IF serverBD.iVariation = -1
				IF MPGlobalsAmbience.iGBAssaultVar != -1
					serverBD.iVariation = MPGlobalsAmbience.iGBAssaultVar
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [GET_GB_ASSAULT_VARIATION] SET FROM PI MENU serverBD.iVariation = ", serverBD.iVariation)
				ELSE
					serverBD.iVariation = 1
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [GET_GB_ASSAULT_VARIATION] FORCING serverBD.iVariation = ", serverBD.iVariation)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN serverBD.iVariation
ENDFUNC

//-- Tunables
FUNC INT GET_GB_ASSAULT_DURATION()

	RETURN g_sMPTunables.igb_assault_time_limit              //TIME_MISSION_DURATION
ENDFUNC


FUNC INT GET_NUMBER_OF_WANTED_STARS_FOR_VARIATION()
	SWITCH GET_GB_ASSAULT_VARIATION()
		CASE GB_ASSAULT_ZANCUDO
			RETURN g_sMPTunables.igb_assault_zancudo_wanted //5
		CASE GB_ASSAULT_AIRPORT
			RETURN g_sMPTunables.igb_assault_lsia_wanted //4
		CASE GB_ASSAULT_ALTRUIST	
			RETURN g_sMPTunables.igb_assault_altruist_wanted // 3
		CASE GB_ASSAULT_MERRYWEATHER
			RETURN g_sMPTunables.igb_assault_merryweather_wanted //3
		 
	ENDSWITCH
	
	RETURN 5
ENDFUNC

FUNC FLOAT GET_RHINO_HEALTH_MODIFIER()
	RETURN g_sMPTunables.fgb_assault_tank_health_modifier // 1.0
ENDFUNC

FUNC INT GET_GB_ASSAULT_FORCE_RIVALS_JOIN_TIME()
	//RETURN 10000
	RETURN TIME_FORCE_RIVALS_JOIN
ENDFUNC



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MAIN PROCS                            ////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Helper function to get the servers game/mission state
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC


//	PURPOSE: Need specific relationship groups for players taking part. Don't want AI to attack non-mission players
PROC SETUP_UW_REL_GROUPS()
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - SETUP_UW_REL_GROUPS")
	
//	ADD_RELATIONSHIP_GROUP("relUWPlayer", relUWPlayer)
//	ADD_RELATIONSHIP_GROUP("relUWAi", relUWAi)
	
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		
		//Like Player
//		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rgFM_Team[i], relUWPlayer)	
//		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relUWPlayer, rgFM_Team[i])
		
//		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rgFM_Team[i], relUWAi)	
//		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relUWAi, rgFM_Team[i])
		
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rgFM_Team[i], RELGROUPHASH_AGGRESSIVE_INVESTIGATE)	
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_AGGRESSIVE_INVESTIGATE, rgFM_Team[i])
	ENDREPEAT
	
	//Hate UW players
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relUWPlayer, relUWAi)
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relUWAi, relUWPlayer)
	
	// Like Cops
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rgFM_Team[i], rgFM_AiLikePlyrLikeCops)
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rgFM_AiLikePlyrLikeCops, rgFM_Team[i])
//	//Dislike Player Like Cops
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE, rgFM_Team[i], rgFM_AiDislikePlyrLikeCops)
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE, rgFM_AiDislikePlyrLikeCops, rgFM_Team[i])
//	//Hate Player Like Cops
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rgFM_Team[i], rgFM_AiHatePlyrLikeCops)
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rgFM_AiHatePlyrLikeCops, rgFM_Team[i])
	
	//SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rgFM_Team[i], rgFM_AiHatedByCopsAndMercs)
	

//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_AMBIENT_GANG_CULT, relUWAi)
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE , RELGROUPHASH_AMBIENT_GANG_CULT, relUWPlayer)

	
	
	//-- Cops
//	SET_AMBIENT_COP_REL_TO_THIS_RELGROUP(ACQUAINTANCE_TYPE_PED_LIKE, relUWAi)
	
	//**AMBIENT GANGS
//	SET_AMBIENT_GANG_LIKES_THIS_RELGROUP(relUWAi)
//	SET_AMBIENT_GANG_LIKES_THIS_RELGROUP(relUWPlayer)
ENDPROC
//
//PROC REMOVE_UW_REL_GROUPS()
//	REMOVE_RELATIONSHIP_GROUP(relUWAi)
//	REMOVE_RELATIONSHIP_GROUP(relUWPlayer)
//ENDPROC

FUNC BOOL DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
	RETURN (IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_JoinedAsRival))
ENDFUNC

PROC MAINTAIN_GB_ASSAULT_MUSIC()
	
	
	
	
	
	
	
	
	
	
	IF GB_IS_GLOBAL_CLIENT_BIT0_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_0_PERMANENT_PARTICIPANT)
	OR GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
		IF NOT IS_BIT_SET(iBoolsBitSet, biL_InitAudio)
			SET_AUDIO_FLAG("DisableFlightMusic", TRUE)
			SET_AUDIO_FLAG("WantedMusicDisabled", TRUE) 
			SET_BIT(iBoolsBitSet, biL_InitAudio)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GB_ASSAULT_MUSIC] SET biL_InitAudio")
		ENDIF
		
		IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneStartMusic)
			IF TRIGGER_MUSIC_EVENT("BG_ASSAULT_START")
				SET_BIT(iBoolsBitSet, biL_DoneStartMusic)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GB_ASSAULT_MUSIC] SET biL_DoneStartMusic")
			ENDIF
		ELSE
			IF serverBD.iPartPackageHolder = -1
			AND serverBD.iPartInVeh = -1
				IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneCollectMusic)
					IF playerBD[PARTICIPANT_ID_TO_INT()].eStage > eGB_GET_TO_LOCATION
						
						IF TRIGGER_MUSIC_EVENT("BG_ASSAULT_COLLECT")
							SET_BIT(iBoolsBitSet, biL_DoneCollectMusic)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GB_ASSAULT_MUSIC] SET biL_DoneCollectMusic")
							
							IF IS_BIT_SET(iBoolsBitSet, biL_DoneDeliverMusic)
								CLEAR_BIT(iBoolsBitSet, biL_DoneDeliverMusic)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE	
				IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneDeliverMusic)
					IF TRIGGER_MUSIC_EVENT("BG_ASSAULT_DELIVER")
						SET_BIT(iBoolsBitSet, biL_DoneDeliverMusic)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GB_ASSAULT_MUSIC] SET biL_DoneDeliverMusic")
						
						IF IS_BIT_SET(iBoolsBitSet, biL_DoneCollectMusic)
							CLEAR_BIT(iBoolsBitSet, biL_DoneCollectMusic)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
			
	ENDIF
ENDPROC

PROC STOP_30S_COUNTDOWN_AUDIO()
	
	
	
	
	
	
	
	
	
	//-- From spreadsheet
	//--  //depot/gta5/docs/Audio/Interactive_Music/DLC/Apartments_Magnate.xlsx
	IF IS_BIT_SET(iBoolsBitSet, biL_ShouldStop30s)
		IF IS_BIT_SET(iBoolsBitSet, biL_Started30sCountdown)
			IF NOT IS_BIT_SET(iBoolsBitSet, biL_StoppedCountdownMusic)
			
				//-- Restore radio control 
				IF IS_BIT_SET(iBoolsBitSet2, biL2_PrepareKill30s)	
					IF NOT IS_BIT_SET(iBoolsBitSet2, biL2_RestoreRadio)
						SET_USER_RADIO_CONTROL_ENABLED(TRUE)
						SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [STOP_30S_COUNTDOWN_AUDIO] SSET_USER_RADIO_CONTROL_ENABLED(TRUE)")
					ENDIF
				ENDIF
				
				IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneKill30s)
					IF TRIGGER_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
						SET_BIT(iBoolsBitSet, biL_DoneKill30s)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [STOP_30S_COUNTDOWN_AUDIO] SET biL_DoneKill30s")
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(iBoolsBitSet, biL_DoneKill30s)
					IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneFadeInRadio)
						IF TRIGGER_MUSIC_EVENT("APT_FADE_IN_RADIO")
							SET_BIT(iBoolsBitSet, biL_DoneFadeInRadio)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [STOP_30S_COUNTDOWN_AUDIO] SET biL_DoneFadeInRadio")
						ENDIF
					ENDIF
					
					IF IS_BIT_SET(iBoolsBitSet, biL_DoneFadeInRadio)
						SET_BIT(iBoolsBitSet, biL_StoppedCountdownMusic)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [STOP_30S_COUNTDOWN_AUDIO] SET biL_StoppedCountdownMusic")
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_30S_COUNTDOWN_MUSIC(INT iTime)
	
	
	
	
	
	
	
	
	
	//-- From spreadsheet
	//--  //depot/gta5/docs/Audio/Interactive_Music/DLC/Apartments_Magnate.xlsx
	
	IF IS_BIT_SET(iBoolsBitSet, biL_ShouldStop30s)
		STOP_30S_COUNTDOWN_AUDIO()
	//	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_30S_COUNTDOWN_MUSIC] EXIT AS biL_ShouldStop30s")
		EXIT 
	ENDIF
	
	IF NOT IS_BIT_SET(iBoolsBitSet, biL_DonePre30sStop)
		IF iTime <= 35000
			IF TRIGGER_MUSIC_EVENT("APT_PRE_COUNTDOWN_STOP")
				SET_BIT(iBoolsBitSet, biL_DonePre30sStop)
				SET_BIT(iBoolsBitSet, biL_Started30sCountdown)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_30S_COUNTDOWN_MUSIC] SET biL_DonePre30sStop")
			ELSE
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_30S_COUNTDOWN_MUSIC] TRYING TO TRIGGER APT_PRE_COUNTDOWN_STOP")
			ENDIF
		ENDIF
	ENDIF
	
	
	IF IS_BIT_SET(iBoolsBitSet, biL_Started30sCountdown)
		IF iTime <= 30000
			IF NOT IS_BIT_SET(iBoolsBitSet2, biL2_PrepareKill30s)
				IF PREPARE_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
					SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE)
					SET_USER_RADIO_CONTROL_ENABLED(FALSE) 
					SET_BIT(iBoolsBitSet2, biL2_PrepareKill30s)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_30S_COUNTDOWN_MUSIC] SET biL2_PrepareKill30s")
				ELSE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_30S_COUNTDOWN_MUSIC] TRYING TO PREPARE APT_COUNTDOWN_30S_KILL")
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneStart30s)
				IF TRIGGER_MUSIC_EVENT("APT_COUNTDOWN_30S")
					SET_BIT(iBoolsBitSet, biL_DoneStart30s)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_30S_COUNTDOWN_MUSIC] SET biL_DoneStart30s")
				ELSE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_30S_COUNTDOWN_MUSIC] TRYING TO TRIGGER APT_COUNTDOWN_30S")
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(iBoolsBitSet, biL_DoneStart30s)
				IF iTime <= 27000
					IF NOT IS_BIT_SET(iBoolsBitSet2, biL2_RestoreRadio)
						SET_USER_RADIO_CONTROL_ENABLED(TRUE)
						SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
						SET_BIT(iBoolsBitSet2, biL2_RestoreRadio) 
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_30S_COUNTDOWN_MUSIC] SET biL2_RestoreRadio")
					ENDIF
					
					IF iTime <= 500 
						IF TRIGGER_MUSIC_EVENT("APT_FADE_IN_RADIO")
							CANCEL_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_30S_COUNTDOWN_MUSIC] TRIGGERED APT_FADE_IN_RADIO")
							CLEAR_BIT(iBoolsBitSet, biL_Started30sCountdown)
						ELSE
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_30S_COUNTDOWN_MUSIC] TRYING TO TRIGGER APT_FADE_IN_RADIO")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Don't want players in passive mode collecting the package
PROC MAINTAIN_PLAYER_IN_PASSIVE_MODE()
	
	
	IF DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
			//-- Passive mode disabled for launching gang
			IF NOT IS_BIT_SET(iBoolsBitSet, biL_DisabledForPassive)
				IF IS_MP_PASSIVE_MODE_ENABLED()
				//	SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(FALSE)
					PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.niPackage), TRUE, TRUE)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT PREVENT_COLLECTION_OF_PORTABLE_PICKUP(TRUE) 4")
					
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
						IF IS_ENTITY_ATTACHED_TO_ENTITY(NET_TO_OBJ(serverBD.niPackage), PLAYER_PED_ID())
							DETACH_PORTABLE_PICKUP_FROM_PED(NET_TO_OBJ(serverBD.niPackage))
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PLAYER_IN_PASSIVE_MODE] I DROPPED PACKAGE AS PASSIVE ENABLED")
						ENDIF
					ENDIF
					
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PLAYER_IN_PASSIVE_MODE] SET biL_DisabledForPassive AS PASSIVE ENABLED")
					SET_BIT(iBoolsBitSet, biL_DisabledForPassive)
				ENDIF
			ELSE
				IF NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
					IF NOT IS_MP_PASSIVE_MODE_ENABLED()
					//	SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(TRUE)
						PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.niPackage), FALSE, TRUE)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(TRUE) 6")
						
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PLAYER_IN_PASSIVE_MODE] CLEAR biL_DisabledForPassive AS NO LONGER PASSIVE")
						CLEAR_BIT(iBoolsBitSet, biL_DisabledForPassive)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
				
ENDPROC


FUNC BOOL IS_THIS_A_STEAL_VEHICLE_VARIATION()
	INT iVar = GET_GB_ASSAULT_VARIATION()
	RETURN (iVar = GB_ASSAULT_ZANCUDO)
ENDFUNC

FUNC BOOL DO_ASSAULT_NET_IDS_EXIST()
	IF IS_THIS_A_STEAL_VEHICLE_VARIATION()
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [DO_ASSAULT_NET_IDS_EXIST] WAITING FOR niVeh[0]")
			RETURN FALSE
		ENDIF
	ELSE
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [DO_ASSAULT_NET_IDS_EXIST] WAITING FOR niPackage")
			RETURN FALSE
		ENDIF
	ENDIF
	
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [DO_ASSAULT_NET_IDS_EXIST] TRUE")
	
	RETURN TRUE
ENDFUNC
PROC MAINTAIN_ASSAULT_DAMAGE_TRACKING()
	IF GET_GB_ASSAULT_VARIATION() != GB_ASSAULT_ZANCUDO
	AND GET_GB_ASSAULT_VARIATION() != GB_ASSAULT_AIRPORT
		EXIT
	ENDIF
	 
	IF GET_GB_ASSAULT_VARIATION() != GB_ASSAULT_ZANCUDO
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])
			ACTIVATE_DAMAGE_TRACKER_ON_NETWORK_ID(serverBD.niVeh[0], TRUE)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [MAINTAIN_ASSAULT_DAMAGE_TRACKING] TURNED ON DAMAGE TRACKING ON VEH 0")
		ENDIF
	ENDIF
ENDPROC	



PROC PROCESS_ASSAULT_DAMAGE_EVENT(INT iCount)

	STRUCT_ENTITY_DAMAGE_EVENT sei 

	PED_INDEX pedKiller
	PED_INDEX pedTemp

	PLAYER_INDEX playerKiller
	PLAYER_INDEX playerVictim
	VEHICLE_INDEX vehTemp	
	INT i

	// Grab the event data.
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sei, SIZE_OF(sei))
		//-- Zancudo - destroy tank
		IF GET_GB_ASSAULT_VARIATION() = GB_ASSAULT_ZANCUDO
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DestroyedVeh)
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DestroyedByNonPlayer)
					IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_NonPartDestroyedRhino)
						IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_NonPlayerDestroyedVeh)
							IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_PlayerDestroyedVeh)
								IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])
									IF DOES_ENTITY_EXIST(sei.VictimIndex)	
										IF IS_ENTITY_A_VEHICLE(sei.VictimIndex)
											vehTemp = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sei.VictimIndex)	
											IF vehTemp = NET_TO_VEH(serverBD.niVeh[0])
												CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [PROCESS_ASSAULT_DAMAGE_EVENT] THINK VEHICLE 0 HAS BEEN DAMAGED BY SOMEONE")
												IF sei.VictimDestroyed
													CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [PROCESS_ASSAULT_DAMAGE_EVENT] THINK VEHICLE 0 HAS BEEN DESTROYED BY SOMEONE")
													IF DOES_ENTITY_EXIST(sei.DamagerIndex)
														IF IS_ENTITY_A_PED(sei.DamagerIndex)	
															pedKiller = GET_PED_INDEX_FROM_ENTITY_INDEX(sei.DamagerIndex)					
															IF IS_PED_A_PLAYER(pedKiller)
																playerKiller = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedKiller)
																CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [PROCESS_ASSAULT_DAMAGE_EVENT] THINK VEH[0] HAS BEEN DESTROYED BY PLAYER ", GET_PLAYER_NAME(playerKiller))
																IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerKiller)
																	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [PROCESS_ASSAULT_DAMAGE_EVENT] THINK VEH[0] HAS BEEN DESTROYED BY A PARTICIPANT ", GET_PLAYER_NAME(playerKiller))
																	IF playerKiller = PLAYER_ID()
																		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DestroyedVeh)
																		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [PROCESS_ASSAULT_DAMAGE_EVENT] THINK I DESTROYED VEH 0")
																		
																		IF NOT GB_IS_GLOBAL_CLIENT_BIT0_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_0_PERMANENT_PARTICIPANT)
																			IF DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
																				GB_SET_PLAYER_AS_PERMANENT_ON_BOSS_MISSION()
																				GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
																				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [PROCESS_ASSAULT_DAMAGE_EVENT] THINK I DESTROYED VEH 0 - SET AS PERMANENT")
																			ENDIF
																		ENDIF
																	ENDIF
																ELSE
																	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [PROCESS_ASSAULT_DAMAGE_EVENT] THINK VEH[0] HAS BEEN DESTROYED BY A NON-PARTICIPANT ", GET_PLAYER_NAME(playerKiller), " SET biP_NonPartDestroyedRhino" )
																	SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_NonPartDestroyedRhino)
																ENDIF
															ELSE	
																CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [PROCESS_ASSAULT_DAMAGE_EVENT] THINK VEHICLE 0 HAS BEEN DESTROYED BY A NON-PLAYER")
																SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DestroyedByNonPlayer)
															ENDIF
														ELSE
															CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [PROCESS_ASSAULT_DAMAGE_EVENT] THINK VEHICLE 0 HAS BEEN DESTROYED BY SOMETHING OTHER THAN A PED!")
														ENDIF
													ENDIF
												ELSE
													//-- Not destroyed. Set myself as permanent if I damaged it and I'm not already permanent
													IF NOT GB_IS_GLOBAL_CLIENT_BIT0_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_0_PERMANENT_PARTICIPANT)
														IF DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
															CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [PROCESS_SIGHTSEER_DAMAGE_EVENT] THINK VEH[0] HAS BEEN DAMAGED BY SOMEONE, WILL SET MYSELF AS PERMANENT IF IT WAS ME")
															IF DOES_ENTITY_EXIST(sei.DamagerIndex)
																IF IS_ENTITY_A_PED(sei.DamagerIndex)	
																	pedKiller = GET_PED_INDEX_FROM_ENTITY_INDEX(sei.DamagerIndex)					
																	IF IS_PED_A_PLAYER(pedKiller)
																		playerKiller = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedKiller)
																		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [PROCESS_SIGHTSEER_DAMAGE_EVENT] THINK VEH[0] HAS BEEN DAMAGED, BUT NOT DESTROYED BY PLAYER ", GET_PLAYER_NAME(playerKiller))
																		IF playerKiller = PLAYER_ID()
																			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [PROCESS_SIGHTSEER_DAMAGE_EVENT] THINK VEH[0] HAS BEEN DAMAGED, BUT NOT DESTROYED BY ME. SETTING AS PERMANENT ")
																			GB_SET_PLAYER_AS_PERMANENT_ON_BOSS_MISSION()
																			GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
																		ENDIF
																	ENDIF
																ENDIF
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF		
		ENDIF
		
		//-- Airport - attack peds
		IF GET_GB_ASSAULT_VARIATION() = GB_ASSAULT_AIRPORT
			REPEAT MAX_GB_ASSAULT_PEDS i
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[i])
					IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iDamagedAiPedBitset, i)
						IF DOES_ENTITY_EXIST(sei.VictimIndex)	
							IF IS_ENTITY_A_PED(sei.VictimIndex)
								pedTemp = GET_PED_INDEX_FROM_ENTITY_INDEX(sei.VictimIndex)
								IF pedTemp = NET_TO_PED(serverBD.niPed[i])
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [PROCESS_ASSAULT_DAMAGE_EVENT] THINK THIS AI PED HAS BEEN DAMAGED BY SOMEONE ", i)
									IF DOES_ENTITY_EXIST(sei.DamagerIndex)
										IF IS_ENTITY_A_PED(sei.DamagerIndex)	
											pedKiller = GET_PED_INDEX_FROM_ENTITY_INDEX(sei.DamagerIndex)					
											IF IS_PED_A_PLAYER(pedKiller)
												CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [PROCESS_ASSAULT_DAMAGE_EVENT] THINK THIS AI PED HAS BEEN DAMAGED BY A PLAYER ", i)
												playerKiller = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedKiller)
												CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [PROCESS_ASSAULT_DAMAGE_EVENT] THINK THIS AI PED ", i, "HAS BEEN DAMAGED BY THIS PLAYER ", GET_PLAYER_NAME(playerKiller))
												IF playerKiller = PLAYER_ID()
													SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iDamagedAiPedBitset, i)
													CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [PROCESS_ASSAULT_DAMAGE_EVENT] THINK THIS AI PED HAS BEEN DAMAGED BY ME ",i)
												ENDIF
											ELSE
												CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [PROCESS_ASSAULT_DAMAGE_EVENT] THINK THIS AI PED HAS BEEN DAMAGED BY SOMEONE OTHER THAN A PLAYER! ", i)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
				
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
		INT iVictimPart
		PLAYER_INDEX playerBoss
		
		//-- Set rivals as permanant participants if they damage the package holder, or if they damage someone in the key organization
		IF DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
			IF NOT GB_IS_GLOBAL_CLIENT_BIT0_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_0_PERMANENT_PARTICIPANT)
				IF DOES_ENTITY_EXIST(sei.VictimIndex)	
					IF IS_ENTITY_A_PED(sei.VictimIndex)
						pedTemp = GET_PED_INDEX_FROM_ENTITY_INDEX(sei.VictimIndex)
						IF IS_PED_A_PLAYER(pedTemp)
							playerVictim = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedTemp)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [PROCESS_ASSAULT_DAMAGE_EVENT] THINK THIS PLAYER HAS BEEN DAMAGED BY SOMEONE ", GET_PLAYER_NAME(playerVictim))
							IF DOES_ENTITY_EXIST(sei.DamagerIndex)
								IF IS_ENTITY_A_PED(sei.DamagerIndex)	
									pedKiller = GET_PED_INDEX_FROM_ENTITY_INDEX(sei.DamagerIndex)				
									IF IS_PED_A_PLAYER(pedKiller)
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [PROCESS_ASSAULT_DAMAGE_EVENT] THINK THIS PLAYER HAS BEEN DAMAGED BY A PLAYER ", GET_PLAYER_NAME(playerVictim))
										playerKiller = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedKiller)
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [PROCESS_ASSAULT_DAMAGE_EVENT] THINK THIS PLAYER ", GET_PLAYER_NAME(playerVictim), " HAS BEEN DAMAGED BY THIS PLAYER ", GET_PLAYER_NAME(playerKiller))
										IF playerKiller = PLAYER_ID()
											CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [PROCESS_ASSAULT_DAMAGE_EVENT] THINK THIS PLAYER ", GET_PLAYER_NAME(playerVictim), " HAS BEEN DAMAGED BY ME")
											
											IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerVictim)
												
												iVictimPart = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(playerVictim))
												CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [PROCESS_ASSAULT_DAMAGE_EVENT] THINK THIS PLAYER ", GET_PLAYER_NAME(playerVictim), " IS A PARTICIPANT iVictimPart = ", iVictimPart, " COMAPRING WITH iPartPackageHolder = ", serverBD.iPartPackageHolder, " AND iPartInVeh = ",serverBD.iPartInVeh)
												//-- I damaged package holder
												IF iVictimPart = serverBD.iPartPackageHolder
													CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [PROCESS_ASSAULT_DAMAGE_EVENT] SET AS PERMENANT AS I HAVE DAMAGED PACKAGE HOLDER serverBD.iPartPackageHolder = ", serverBD.iPartPackageHolder)
													GB_SET_PLAYER_AS_PERMANENT_ON_BOSS_MISSION()
													GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
												//-- I damaged vehicle driver
												ELIF iVictimPart = serverBD.iPartInVeh
													CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [PROCESS_ASSAULT_DAMAGE_EVENT] SET AS PERMENANT AS I HAVE DAMAGED VEHICLE DRIVER serverBD.iPartInVeh = ", serverBD.iPartInVeh)
													GB_SET_PLAYER_AS_PERMANENT_ON_BOSS_MISSION()
													GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
												ELSE
													//-- Have I damaged someone in launching gang?
													IF GB_IS_PLAYER_MEMBER_OF_A_GANG(playerVictim)
														CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [PROCESS_ASSAULT_DAMAGE_EVENT] VICTIM ", GET_PLAYER_NAME(playerVictim), " IS A MEMBER OF A GANG")
														IF serverBD.iBossPartWhoLaunched > -1
															playerBoss = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.iBossPartWhoLaunched))
															CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [PROCESS_ASSAULT_DAMAGE_EVENT] CHECKING IF VICTIM ", GET_PLAYER_NAME(playerVictim), " IS IN LAUNCHING BOSS'S GANG", GET_PLAYER_NAME(playerBoss))
															IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerVictim, playerBoss)
																CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [PROCESS_ASSAULT_DAMAGE_EVENT] SET AS PERMENANT AS I HAVE DAMAGED PLAYER ",GET_PLAYER_NAME(playerVictim), " WHO IS A MEMBER OF THIS PLAYER'S GANG", GET_PLAYER_NAME(playerBoss))
																GB_SET_PLAYER_AS_PERMANENT_ON_BOSS_MISSION()
																GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF

					ENDIF
				ENDIF
			ENDIF
		ENDIF																																											
	ENDIF
ENDPROC

PROC PROCESS_EVENTS()
	
	INT iCount
	EVENT_NAMES ThisScriptEvent
	
	//process the events
	
//	IF GET_GB_ASSAULT_VARIATION() != GB_ASSAULT_ZANCUDO
//	AND GET_GB_ASSAULT_VARIATION() != GB_ASSAULT_AIRPORT
//		EXIT
//	ENDIF
	
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount
		
		ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)
		
		SWITCH ThisScriptEvent
			
			CASE EVENT_NETWORK_DAMAGE_ENTITY
				

				PROCESS_ASSAULT_DAMAGE_EVENT(iCount)

				
				
			BREAK
			
		ENDSWITCH
		
	ENDREPEAT
	
	
ENDPROC




FUNC VECTOR GET_LOCATION_FOR_VARIATION()
	VECTOR vLoc
	SWITCH GET_GB_ASSAULT_VARIATION()
		CASE GB_ASSAULT_ALTRUIST		vLoc = <<-1166.2518, 4926.1826, 222.0286>>	BREAK // Altruist
		CASE GB_ASSAULT_ZANCUDO			vLoc = <<-2115.0222, 3295.7969, 31.8103>>	BREAK // Zancudo
		CASE GB_ASSAULT_AIRPORT			vLoc =  <<-1289.8530, -3388.4590, 12.9452>>	BREAK // Airport
		CASE GB_ASSAULT_MERRYWEATHER	vLoc =  <<542.5379, -3196.3611, 5.0693>>	BREAK // Merryweather
	ENDSWITCH
	
	RETURN vLoc
ENDFUNC

FUNC VECTOR GET_LOCATION_BLIP_COORD()
	VECTOR vLoc
	SWITCH GET_GB_ASSAULT_VARIATION()
		CASE GB_ASSAULT_ALTRUIST		
			vLoc = <<-1067.0702, 4907.1147, 211.5355>>	
		BREAK 
		
		CASE GB_ASSAULT_ZANCUDO			
		CASE GB_ASSAULT_AIRPORT			
		CASE GB_ASSAULT_MERRYWEATHER	
			vLoc = GET_LOCATION_FOR_VARIATION()
		BREAK
	ENDSWITCH
	
	RETURN vLoc
ENDFUNC

FUNC TEXT_LABEL_31 GET_INITIAL_OBJECTIVE_FOR_VARIATION()
	TEXT_LABEL_31 tl31Obj
	
	SWITCH GET_GB_ASSAULT_VARIATION()
		CASE GB_ASSAULT_ALTRUIST		tl31Obj	= "GB_ASLT_CUL"						BREAK // Altruist
		CASE GB_ASSAULT_ZANCUDO			tl31Obj	= "GB_ASLT_GEN"						BREAK // Zancudo
		CASE GB_ASSAULT_AIRPORT			tl31Obj	= "GB_ASLT_AIR"						BREAK // Airport
	//	CASE GB_ASSAULT_HUMANE			tl31Obj	= "GB_ASLT_HUM"						BREAK // Humane	
		CASE GB_ASSAULT_MERRYWEATHER	tl31Obj	= "GB_ASLT_MER"						BREAK // Merryweather
	ENDSWITCH
	
	RETURN tl31Obj
ENDFUNC

FUNC TEXT_LABEL_31 GET_INITIAL_HELP_FOR_VARIATION()
	TEXT_LABEL_31 tl31Obj
	IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
		SWITCH GET_GB_ASSAULT_VARIATION()
			CASE GB_ASSAULT_ALTRUIST		tl31Obj	= "GB_AST_HELP1"						BREAK // Altruist
			CASE GB_ASSAULT_ZANCUDO			tl31Obj	= "GB_AST_HELP4"						BREAK // Zancudo
			CASE GB_ASSAULT_AIRPORT			tl31Obj	= "GB_AST_HELP1"						BREAK // Airport	
			CASE GB_ASSAULT_MERRYWEATHER	tl31Obj	= "GB_AST_HELP1"						BREAK // Merryweather
		ENDSWITCH
	ELSE
		SWITCH GET_GB_ASSAULT_VARIATION()
			CASE GB_ASSAULT_ALTRUIST		tl31Obj	= "GB_AST_HELP1G"						BREAK // Altruist
			CASE GB_ASSAULT_ZANCUDO			tl31Obj	= "GB_AST_HELP4G"						BREAK // Zancudo
			CASE GB_ASSAULT_AIRPORT			tl31Obj	= "GB_AST_HELP1G"						BREAK // Airport	
			CASE GB_ASSAULT_MERRYWEATHER	tl31Obj	= "GB_AST_HELP1G"						BREAK // Merryweather
		ENDSWITCH
	ENDIF
	
	RETURN tl31Obj
ENDFUNC

FUNC TEXT_LABEL_31 GET_REACHED_GOTO_HELP_FOR_VARIATION()
	TEXT_LABEL_31 tl31Obj

	SWITCH GET_GB_ASSAULT_VARIATION()
		CASE GB_ASSAULT_ALTRUIST		tl31Obj	= "GB_AST_RPCK1"						BREAK // Altruist		The briefcase is shown on the Radar at ~HUD_COLOUR_RED~~BLIP_ASSAULT_PACKAGE~. 
		CASE GB_ASSAULT_ZANCUDO			tl31Obj	= "GB_AST_RTNK"							BREAK // Zancudo		The Rhino is shown on the Radar at ~HUD_COLOUR_BLUE~~BLIP_TANK~.
		CASE GB_ASSAULT_AIRPORT			tl31Obj	= "GB_AST_RPCK1"						BREAK // Airport		The briefcase is shown on the Radar at ~HUD_COLOUR_RED~~BLIP_ASSAULT_PACKAGE~. 
		CASE GB_ASSAULT_MERRYWEATHER	tl31Obj	= "GB_AST_RPCK2"						BREAK // Merryweather	The briefcase is shown on the Radar at ~HUD_COLOUR_GREEN~~BLIP_ASSAULT_PACKAGE~.
	ENDSWITCH

	
	RETURN tl31Obj
ENDFUNC

FUNC MODEL_NAMES GET_BOSS_MODEL_FOR_VARIATION()
	MODEL_NAMES mBoss
	
	SWITCH GET_GB_ASSAULT_VARIATION()
		CASE GB_ASSAULT_ALTRUIST		mBoss = A_M_O_ACULT_02			BREAK // Altruist
		CASE GB_ASSAULT_ZANCUDO			mBoss = S_M_M_MARINE_02			BREAK // Zancudo
		CASE GB_ASSAULT_AIRPORT			mBoss = G_M_M_ARMBOSS_01		BREAK // Airport
	//	CASE GB_ASSAULT_HUMANE			mBoss = S_M_M_HIGHSEC_01		BREAK // Humane	
		CASE GB_ASSAULT_MERRYWEATHER	mBoss = S_M_Y_BLACKOPS_02		BREAK // Merryweather
	ENDSWITCH
	
	RETURN mBoss
ENDFUNC

FUNC MODEL_NAMES GET_GUARD_MODEL_FOR_VARIATION()
	MODEL_NAMES mGuard
	
	SWITCH GET_GB_ASSAULT_VARIATION()
		CASE GB_ASSAULT_ALTRUIST		mGuard = A_M_O_ACULT_01	BREAK // Altruist 			
		CASE GB_ASSAULT_ZANCUDO			mGuard = S_M_M_MARINE_01		BREAK // Zancudo
		CASE GB_ASSAULT_AIRPORT			mGuard = G_M_M_ARMGOON_01		BREAK // Airport
	//	CASE GB_ASSAULT_HUMANE			mGuard = S_M_M_CHEMSEC_01		BREAK // Humane	
		CASE GB_ASSAULT_MERRYWEATHER	mGuard = S_M_Y_BLACKOPS_01		BREAK // Merryweather
	ENDSWITCH
	
	RETURN mGuard
ENDFUNC


PROC GET_BOSS_POS_AND_HEADING(VECTOR &vCreate, FLOAT &fHead, FLOAT &fSpehereSize)
	//-- Also used in GB_GET_ASSAULT_LOCATION_FOR_VARIATION() in x:\gta5\script\dev_ng\multiplayer\include\public\freemode\net_gang_boss.sch!!!
	
	SWITCH GET_GB_ASSAULT_VARIATION()
		CASE GB_ASSAULT_ALTRUIST		vCreate =  <<-1166.8436, 4925.4414, 221.9988>> 	fHead =  346.3493	fSpehereSize = 5.0			BREAK // Altruist
		CASE GB_ASSAULT_ZANCUDO			vCreate =  <<-2113.9648, 3287.6809, 31.8103>>	fHead =  155.4586	fSpehereSize = 5.0			BREAK // Zancudo
		CASE GB_ASSAULT_AIRPORT			vCreate =  <<-1301.8905, -3396.5117, 12.9452>> 	fHead =  235.3724	fSpehereSize = 5.0		BREAK // Airport
		CASE GB_ASSAULT_MERRYWEATHER	vCreate =  <<547.5576, -3198.2988, 5.0693>>		fHead =  22.5507	fSpehereSize = 1.0		BREAK // Merryweather
	ENDSWITCH
ENDPROC



PROC GET_NTH_GUARD_POS_AND_HEADING(INT index, VECTOR &vCreate, FLOAT &fHead, FLOAT &fSpehereSize)
	SWITCH GET_GB_ASSAULT_VARIATION()
		CASE GB_ASSAULT_ALTRUIST
			
			SWITCH index
				//--Altruist
				//CASE 0	vCreate = <<-1164.2463, 4929.9272, 222.2787>> 	fHead = 141.8682	fSpehereSize = 3.0 BREAK
				CASE 0	vCreate = <<-1170.2551, 4928.1440, 223.2858>> 	fHead = 252.5942	fSpehereSize = 1.0 BREAK
				
				//--Lost MC
			//	CASE 2	vCreate = <<-1166.3917, 4927.0039, 222.0804>>  	fHead = 163.7420	fSpehereSize = 1.0 BREAK
				CASE 1	vCreate = <<-1162.2073, 4924.0640, 221.7922>>  	fHead = 47.6051	fSpehereSize = 5.0 BREAK
				CASE 2	vCreate = <<-1159.4727, 4926.2554, 221.7704>>  	fHead = 107.9506	fSpehereSize = 5.0 BREAK
			ENDSWITCH
		BREAK
		
		CASE GB_ASSAULT_ZANCUDO
			
			SWITCH index
				CASE 0	vCreate = <<-2117.6052, 3284.5261, 31.8103>>	fHead = 323.9433	fSpehereSize = 10.0	BREAK
				CASE 1	vCreate = <<-2114.5972, 3283.4758, 31.8103>>	fHead = 357.3127	fSpehereSize = 5.0	BREAK
				CASE 2	vCreate = <<-2127.0476, 3285.2373, 37.7325>>	fHead = 219.4789	fSpehereSize = 5.0 	BREAK
				CASE 3	vCreate = <<-2113.0781, 3276.0957, 37.7325>>	fHead = 62.1130		fSpehereSize = 1.0	BREAK
		//		CASE 4	vCreate = <<-2120.8452, 3292.8618, 31.8103>>	fHead = 167.5507	fSpehereSize = 1.0	BREAK
			ENDSWITCH
		BREAK

		CASE GB_ASSAULT_AIRPORT

			SWITCH index
				CASE 0	vCreate = <<-1300.6101, -3400.8557, 12.9452>>  		fHead = 28.2234		fSpehereSize = 5.0	BREAK
				CASE 1	vCreate = <<-1300.1302, -3397.6895, 12.9452>> 		fHead = 65.8332		fSpehereSize = 5.0	BREAK
				CASE 2	vCreate = <<-1301.7921, -3392.7419, 12.9452>>  		fHead = 323.2079	fSpehereSize = 5.0	BREAK
				CASE 3	vCreate = <<-1307.2784, -3392.9644, 16.0027>> 		fHead = 296.4860	fSpehereSize = 1.0 	BREAK
			ENDSWITCH
		BREAK
		
//		CASE GB_ASSAULT_HUMANE
//			fSpehereSize = 20.0
//			SWITCH index
//				CASE 0	vCreate = <<3569.6687, 3726.8147, 35.6427>> 	fHead = 81.0265		BREAK
//				CASE 1	vCreate = <<3566.6743, 3713.5784, 35.6427>>  	fHead = 88.6719	BREAK
//			ENDSWITCH
//		BREAK
		
		
		
		/*
Guard 1 = <<572.3206, -3126.4946, 17.7686>>, 183.7487
Guard 2 = <<567.2507, -3119.3013, 17.7686>>, 10.0934
Guard 3 = <<571.0172, -3119.2388, 17.7686>>, 351.6397
Guard 4 = <<551.3553, -3119.2549, 17.7140>>, 89.2779
Guard 5 = <<541.4325, -3119.1306, 7.5660>>,  181.8911
Guard 6 = <<586.7628, -3118.9802, 17.7149>>, 271.8600
Guard 7 = <<594.2654, -3122.4016, 5.0693>>,  89.4349
Guard 8 = <<550.7189, -3131.0835, 16.3604>>, 172.4657
Guard 9 = <<587.6816, -3131.0808, 16.3706>>, 200.5195
Guard 10 = <<575.2856, -3124.2615, 17.7686>>,265.4380
		*/
		CASE GB_ASSAULT_MERRYWEATHER
			fSpehereSize = 1.0
			SWITCH index
				CASE 0	vCreate = 	 <<572.3206, -3126.4946, 17.7686>>	fHead = 183.7487		BREAK
				CASE 1	vCreate =  	  <<563.9862, -3118.1377, 17.7687>>   fHead = 91.0942 			BREAK
				CASE 2	vCreate =  	 <<580.0829, -3117.4094, 17.7686>> 	fHead = 97.2584			BREAK
				CASE 3	vCreate =  	 <<520.6996, -3119.5117, 17.7140>> 	fHead = 119.4985		BREAK
				CASE 4	vCreate =  	 <<541.4325, -3119.1306, 7.5660>> 	fHead = 181.8911		BREAK
				CASE 5	vCreate =  	 <<586.7628, -3118.9802, 17.7149>> 	fHead = 271.8600		BREAK
				CASE 6	vCreate =  	 <<594.2654, -3122.4016, 5.0693>> 	fHead = 89.4349			BREAK
				CASE 7	vCreate =  	 <<550.7189, -3131.0835, 16.3604>> 	fHead = 172.4657		BREAK
				CASE 8	vCreate =  	 <<587.6816, -3131.0808, 16.3706>> 	fHead = 200.5195		BREAK
			//	CASE 9	vCreate =  	 <<575.2856, -3124.2615, 17.7686>> 	fHead = 265.4380		BREAK
			ENDSWITCH
		BREAK
		

	ENDSWITCH
ENDPROC

FUNC BOOL DO_ALL_PEDS_EXIST_FOR_VARIATION()
	SWITCH GET_GB_ASSAULT_VARIATION()
		CASE GB_ASSAULT_ZANCUDO
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[0])
			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[1])
			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[2])
			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[3])
			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[4])
		//	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[5])
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE GB_ASSAULT_AIRPORT
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[0])
			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[1])
			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[2])
			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[3])
			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[4])
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE GB_ASSAULT_ALTRUIST
		

			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[0])
			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[1])
			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[2])
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE GB_ASSAULT_MERRYWEATHER
		//	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[0])
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[1])
			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[2])
			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[3])
			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[4])
			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[5])
			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[6])
			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[7])
			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[8])
			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[9])
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


FUNC BOOL CREATE_GUARD_PED(INT index, MODEL_NAMES mGuard, VECTOR vLoc, FLOAT fHead, FLOAT fDefenseSize, REL_GROUP_HASH relGroupGuard)
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_15 tl15Name
	#ENDIF
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[index])
		REQUEST_MODEL(mGuard)
		IF HAS_MODEL_LOADED(mGuard)
			IF CAN_REGISTER_MISSION_ENTITIES(1, 0, 0, 0)
				IF NOT IS_BIT_SET(serverBD.iPedClearAreaBitset, index)
					CLEAR_AREA(vLoc, 2.0, TRUE)
					SET_BIT(serverBD.iPedClearAreaBitset, index)
				ENDIF
				IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(	vLoc, // VECTOR vPoint, 
																1.0, 						 // FLOAT fVehRadius = 6.0, 
																DEFAULT, 						 // FLOAT fPedRadius = 1.0, 
																DEFAULT,						 // FLOAT fObjRadius = 1.0,
																DEFAULT,						 // FLOAT fViewRadius = 5.0,
																DEFAULT,						 // BOOL bCheckThisPlayerSight = TRUE,
																DEFAULT,						 // BOOL bDoAnyPlayerSeePointCheck = TRUE,
																DEFAULT,						 // BOOL bCheckVisibilityForOwnTeam = TRUE,
																DEFAULT,					 // FLOAT fVisibleDistance = 120.0,
																DEFAULT, 						 // BOOL bIgnoreLocalPlayerChecks=FALSE, 
																DEFAULT,						 // INT iTeamToIgnore=-1,
																DEFAULT,						 // BOOL bDoFireCheck=TRUE,
																50.0)
																
					IF CREATE_NET_PED(serverBD.niPed[index], PEDTYPE_MISSION, mGuard, vLoc, fHead)
						SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(serverBD.niPed[index]), relGroupGuard)
						SET_ENTITY_IS_TARGET_PRIORITY(NET_TO_PED(serverBD.niPed[index]), TRUE)
						SET_PED_RANDOM_COMPONENT_VARIATION(NET_TO_PED(serverBD.niPed[index]))

						GIVE_DELAYED_WEAPON_TO_PED(NET_TO_PED(serverBD.niPed[index]), WEAPONTYPE_CARBINERIFLE, 25000, TRUE)


						FLOAT fAccuracy = 30.0 * 1.0 //GET_ENEMY_ACCURACY_MULTIPLIER()
						SET_PED_ACCURACY(NET_TO_PED(serverBD.niPed[index]), ROUND(fAccuracy))	

						//Set Behaviours
						SET_PED_COMBAT_ABILITY(NET_TO_PED(serverBD.niPed[index]), CAL_AVERAGE)
						SET_PED_COMBAT_MOVEMENT(NET_TO_PED(serverBD.niPed[index]), CM_DEFENSIVE)
						SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.niPed[index]), CA_USE_COVER, TRUE)
						SET_PED_TARGET_LOSS_RESPONSE(NET_TO_PED(serverBD.niPed[index]), TLR_NEVER_LOSE_TARGET)
						SET_PED_HIGHLY_PERCEPTIVE(NET_TO_PED(serverBD.niPed[index]), TRUE)

						SET_PED_CONFIG_FLAG(NET_TO_PED(serverBD.niPed[index]), PCF_DontInfluenceWantedLevel, TRUE)

						SET_ENTITY_HEALTH(NET_TO_PED(serverBD.niPed[index]), 200)

						SET_PED_AS_ENEMY(NET_TO_PED(serverBD.niPed[index]), TRUE)

						SET_PED_KEEP_TASK(NET_TO_PED(serverBD.niPed[index]), TRUE)
						SET_PED_DIES_WHEN_INJURED(NET_TO_PED(serverBD.niPed[index]),TRUE)

						SET_PED_SPHERE_DEFENSIVE_AREA( NET_TO_PED(serverBD.niPed[index]), vLoc, fDefenseSize)
						
						IF GET_GB_ASSAULT_VARIATION() = GB_ASSAULT_AIRPORT
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(serverBD.niPed[index]), TRUE)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CREATE_GUARD_PED] SET_BLOCKING_OF_NON_TEMPORARY_EVENTS PED ", index)
						ENDIF
						
						#IF IS_DEBUG_BUILD
						tl15Name = "PED "
						tl15Name += index
						SET_PED_NAME_DEBUG(NET_TO_PED(serverBD.niPed[index]), tl15Name)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CREATE_GUARD_PED] CREATED GUARD PED ", index, " AT LOC ",vLoc, " fDefenseSize = ", fDefenseSize)
						#ENDIF
					ENDIF
				ELSE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CREATE_GUARD_PED] FAILING IS_POINT_OK_FOR_NET_ENTITY_CREATION index = ", index)
				ENDIF
			ELSE
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CREATE_GUARD_PED] FAILING CAN_REGISTER_MISSION_ENTITIES index = ", index)
			ENDIF
		ELSE
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CREATE_GUARD_PED] FAILING HAS_MODEL_LOADED index = ", index)
		ENDIF
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[index])
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_BOSS_PED(INT index, MODEL_NAMES mBoss, VECTOR vLoc, FLOAT fHead, FLOAT fDefenseSize, REL_GROUP_HASH relGroupBoss)
	FLOAT fAccuracy
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[index])
		REQUEST_MODEL(mBoss)
		IF HAS_MODEL_LOADED(mBoss)
			IF CAN_REGISTER_MISSION_ENTITIES(0, 1, 0, 0)
				IF NOT IS_BIT_SET(serverBD.iPedClearAreaBitset, index)
					CLEAR_AREA(vLoc, 2.0, TRUE)
					SET_BIT(serverBD.iPedClearAreaBitset, index)
				ENDIF
				IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(	vLoc, // VECTOR vPoint, 
														1.0, 						 // FLOAT fVehRadius = 6.0, 
														DEFAULT, 						 // FLOAT fPedRadius = 1.0, 
														DEFAULT,						 // FLOAT fObjRadius = 1.0,
														DEFAULT,						 // FLOAT fViewRadius = 5.0,
														DEFAULT,						 // BOOL bCheckThisPlayerSight = TRUE,
														DEFAULT,						 // BOOL bDoAnyPlayerSeePointCheck = TRUE,
														DEFAULT,						 // BOOL bCheckVisibilityForOwnTeam = TRUE,
														DEFAULT,					 // FLOAT fVisibleDistance = 120.0,
														DEFAULT, 						 // BOOL bIgnoreLocalPlayerChecks=FALSE, 
														DEFAULT,						 // INT iTeamToIgnore=-1,
														DEFAULT,						 // BOOL bDoFireCheck=TRUE,
														50.0) 						// FLOAT fPlayerRadius = 0.0,
														
					IF CREATE_NET_PED(serverBD.niPed[index], PEDTYPE_MISSION, mBoss, vLoc, fHead)
						SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(serverBD.niPed[index]), relGroupBoss)
						SET_ENTITY_IS_TARGET_PRIORITY(NET_TO_PED(serverBD.niPed[index]), TRUE)
					//	SET_PED_RANDOM_COMPONENT_VARIATION(NET_TO_PED(serverBD.niPed[index]))
						
						IF GET_GB_ASSAULT_VARIATION() = GB_ASSAULT_ZANCUDO
							SET_PED_COMPONENT_VARIATION(NET_TO_PED(serverBD.niPed[index]), PED_COMP_TORSO, 0, 0)
							SET_PED_COMPONENT_VARIATION(NET_TO_PED(serverBD.niPed[index]), PED_COMP_SPECIAL, 0, 0) // stars
							SET_PED_COMPONENT_VARIATION(NET_TO_PED(serverBD.niPed[index]), PED_COMP_DECL, 0, 0) // badges
							SET_PED_PROP_INDEX( NET_TO_PED(serverBD.niPed[index]), ANCHOR_HEAD, 1)
						ELSE
							SET_PED_RANDOM_COMPONENT_VARIATION(NET_TO_PED(serverBD.niPed[index]))
						ENDIF
						
						GIVE_DELAYED_WEAPON_TO_PED(NET_TO_PED(serverBD.niPed[index]), WEAPONTYPE_ADVANCEDRIFLE, 25000, TRUE)
						
						
						fAccuracy = 30.0 * 1.0 //GET_ENEMY_ACCURACY_MULTIPLIER()
						SET_PED_ACCURACY(NET_TO_PED(serverBD.niPed[index]), ROUND(fAccuracy))	
						
						//Set Behaviours
						SET_PED_COMBAT_ABILITY(NET_TO_PED(serverBD.niPed[index]), CAL_AVERAGE)
						SET_PED_COMBAT_MOVEMENT(NET_TO_PED(serverBD.niPed[index]), CM_DEFENSIVE)
						SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.niPed[index]), CA_USE_COVER, TRUE)
						SET_PED_TARGET_LOSS_RESPONSE(NET_TO_PED(serverBD.niPed[index]), TLR_NEVER_LOSE_TARGET)
						SET_PED_HIGHLY_PERCEPTIVE(NET_TO_PED(serverBD.niPed[index]), TRUE)
			
						SET_PED_CONFIG_FLAG(NET_TO_PED(serverBD.niPed[index]), PCF_DontInfluenceWantedLevel, TRUE)
						
						SET_ENTITY_HEALTH(NET_TO_PED(serverBD.niPed[index]), 200)
						
						SET_PED_AS_ENEMY(NET_TO_PED(serverBD.niPed[index]), TRUE)
							
						SET_PED_KEEP_TASK(NET_TO_PED(serverBD.niPed[index]), TRUE)
						SET_PED_DIES_WHEN_INJURED(NET_TO_PED(serverBD.niPed[index]),TRUE)
						
						SET_PED_SPHERE_DEFENSIVE_AREA( NET_TO_PED(serverBD.niPed[index]), vLoc, fDefenseSize)
						
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CREATE_BOSS_PED] CREATED PED ", index, " AT ",vLoc) 
					ENDIF

				ELSE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CREATE_BOSS_PED] BOSS WAITING FOR IS_POINT_OK_FOR_NET_ENTITY_CREATION")
				ENDIF
			ELSE
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CREATE_BOSS_PED] BOSS  WAITING FOR CAN_REGISTER_MISSION_ENTITIES")
			ENDIF
		ELSE
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CREATE_BOSS_PED] BOSS  WAITING FOR HAS_MODEL_LOADED")
		ENDIF
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBd.niPed[index])
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL CREATE_BOSS_AND_GUARDS()
	MODEL_NAMES mBoss =  GET_BOSS_MODEL_FOR_VARIATION()
	MODEL_NAMES mGuards = GET_GUARD_MODEL_FOR_VARIATION()
	
	VECTOR vCreate 
	FLOAT fDefSpehereSize
	FLOAT fHead 
//	FLOAT fAccuracy
	REL_GROUP_HASH relGroupAssault
	
	IF GET_GB_ASSAULT_VARIATION() = GB_ASSAULT_ALTRUIST
		relGroupAssault = rgFM_AiHate
	ELIF GET_GB_ASSAULT_VARIATION() = GB_ASSAULT_MERRYWEATHER
		relGroupAssault = RELGROUPHASH_AGGRESSIVE_INVESTIGATE
	ELSE
		relGroupAssault = rgFM_AiHatePlyrLikeCops
	ENDIF
	
	INT i

	SWITCH GET_GB_ASSAULT_VARIATION()
		CASE GB_ASSAULT_ZANCUDO
	
			GET_BOSS_POS_AND_HEADING(vCreate, fHead, fDefSpehereSize)
			
			CREATE_BOSS_PED(0, mBoss, vCreate, fHead, fDefSpehereSize, relGroupAssault)
			
			FOR i = 1 TO 4
				GET_NTH_GUARD_POS_AND_HEADING(i-1, vCreate, fHead, fDefSpehereSize)
				
				CREATE_GUARD_PED(i, mGuards, vCreate, fHead, fDefSpehereSize, relGroupAssault)
			ENDFOR
						
		BREAK
		
		
		CASE GB_ASSAULT_AIRPORT
			GET_BOSS_POS_AND_HEADING(vCreate, fHead, fDefSpehereSize)
			
			CREATE_BOSS_PED(0, mBoss, vCreate, fHead, fDefSpehereSize, relGroupAssault)
			
			FOR i = 1 TO 4
			
				GET_NTH_GUARD_POS_AND_HEADING(i-1, vCreate, fHead, fDefSpehereSize)
				IF i <= 2
					mGuards = S_M_M_FIBSEC_01	 
				ELSE	
					mGuards = GET_GUARD_MODEL_FOR_VARIATION()
				ENDIF
				CREATE_GUARD_PED(i, mGuards, vCreate, fHead, fDefSpehereSize, relGroupAssault)
			ENDFOR
		BREAK
		
		CASE GB_ASSAULT_ALTRUIST
			GET_BOSS_POS_AND_HEADING(vCreate, fHead, fDefSpehereSize)
			
			CREATE_BOSS_PED(0, mBoss, vCreate, fHead, fDefSpehereSize, relGroupAssault)
			
			
			FOR i = 1 TO 3
				IF i = 1 
					mGuards = GET_GUARD_MODEL_FOR_VARIATION()
				ELSE
					mguards = G_M_Y_LOST_03
				ENDIF
				
				GET_NTH_GUARD_POS_AND_HEADING(i-1, vCreate, fHead, fDefSpehereSize)
				CREATE_GUARD_PED(i, mGuards, vCreate, fHead, fDefSpehereSize, relGroupAssault)
			ENDFOR
			
		BREAK
		
		CASE GB_ASSAULT_MERRYWEATHER
			GET_BOSS_POS_AND_HEADING(vCreate, fHead, fDefSpehereSize)
			
//			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[0])
//				REQUEST_MODEL(mBoss)
//				IF HAS_MODEL_LOADED(mBoss)
//					IF CAN_REGISTER_MISSION_ENTITIES(0, 1, 0, 0)
//						IF NOT IS_BIT_SET(serverBD.iPedClearAreaBitset, 0)
//							CLEAR_AREA(vCreate, 2.0, TRUE)
//							SET_BIT(serverBD.iPedClearAreaBitset, 0)
//						ENDIF
//						IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(	vCreate, // VECTOR vPoint, 
//																DEFAULT, 						 // FLOAT fVehRadius = 6.0, 
//																DEFAULT, 						 // FLOAT fPedRadius = 1.0, 
//																DEFAULT,						 // FLOAT fObjRadius = 1.0,
//																DEFAULT,						 // FLOAT fViewRadius = 5.0,
//																DEFAULT,						 // BOOL bCheckThisPlayerSight = TRUE,
//																DEFAULT,						 // BOOL bDoAnyPlayerSeePointCheck = TRUE,
//																DEFAULT,						 // BOOL bCheckVisibilityForOwnTeam = TRUE,
//																DEFAULT,					 // FLOAT fVisibleDistance = 120.0,
//																DEFAULT, 						 // BOOL bIgnoreLocalPlayerChecks=FALSE, 
//																DEFAULT,						 // INT iTeamToIgnore=-1,
//																DEFAULT,						 // BOOL bDoFireCheck=TRUE,
//																50.0) 						// FLOAT fPlayerRadius = 0.0,
//																
//							IF CREATE_NET_PED(serverBD.niPed[0], PEDTYPE_MISSION, mBoss, vCreate, fHead)
//								SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(serverBD.niPed[0]), relGroupAssault)
//								SET_ENTITY_IS_TARGET_PRIORITY(NET_TO_PED(serverBD.niPed[0]), TRUE)
//							//	SET_PED_RANDOM_COMPONENT_VARIATION(NET_TO_PED(serverBD.niPed[0]))
//								
//								IF GET_GB_ASSAULT_VARIATION() = GB_ASSAULT_ZANCUDO
//									SET_PED_COMPONENT_VARIATION(NET_TO_PED(serverBD.niPed[0]), PED_COMP_TORSO, 0, 0)
//									SET_PED_COMPONENT_VARIATION(NET_TO_PED(serverBD.niPed[0]), PED_COMP_SPECIAL, 0, 0) // stars
//									SET_PED_COMPONENT_VARIATION(NET_TO_PED(serverBD.niPed[0]), PED_COMP_DECL, 0, 0) // badges
//									SET_PED_PROP_INDEX( NET_TO_PED(serverBD.niPed[0]), ANCHOR_HEAD, 1)
//								ELSE
//									SET_PED_RANDOM_COMPONENT_VARIATION(NET_TO_PED(serverBD.niPed[0]))
//								ENDIF
//								
//								GIVE_DELAYED_WEAPON_TO_PED(NET_TO_PED(serverBD.niPed[0]), WEAPONTYPE_ADVANCEDRIFLE, 25000, TRUE)
//								
//								
//								fAccuracy = 30.0 * 1.0 //GET_ENEMY_ACCURACY_MULTIPLIER()
//								SET_PED_ACCURACY(NET_TO_PED(serverBD.niPed[0]), ROUND(fAccuracy))	
//								
//								//Set Behaviours
//								SET_PED_COMBAT_ABILITY(NET_TO_PED(serverBD.niPed[0]), CAL_AVERAGE)
//								SET_PED_COMBAT_MOVEMENT(NET_TO_PED(serverBD.niPed[0]), CM_DEFENSIVE)
//								SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.niPed[0]), CA_USE_COVER, TRUE)
//								SET_PED_TARGET_LOSS_RESPONSE(NET_TO_PED(serverBD.niPed[0]), TLR_NEVER_LOSE_TARGET)
//								SET_PED_HIGHLY_PERCEPTIVE(NET_TO_PED(serverBD.niPed[0]), TRUE)
//					
//								SET_PED_CONFIG_FLAG(NET_TO_PED(serverBD.niPed[0]), PCF_DontInfluenceWantedLevel, TRUE)
//								
//								SET_ENTITY_HEALTH(NET_TO_PED(serverBD.niPed[0]), 200)
//								
//								SET_PED_AS_ENEMY(NET_TO_PED(serverBD.niPed[0]), TRUE)
//									
//								SET_PED_KEEP_TASK(NET_TO_PED(serverBD.niPed[0]), TRUE)
//								SET_PED_DIES_WHEN_INJURED(NET_TO_PED(serverBD.niPed[0]),TRUE)
//								
//								SET_PED_SPHERE_DEFENSIVE_AREA( NET_TO_PED(serverBD.niPed[0]), vCreate, fDefSpehereSize)
//								
//								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CREATE_BOSS_AND_GUARDS] CREATED PED 0 AT ",vCreate) 
//							ENDIF
//
//						ELSE
//							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CREATE_BOSS_AND_GUARDS] BOSS WAITING FOR IS_POINT_OK_FOR_NET_ENTITY_CREATION")
//						ENDIF
//					ELSE
//						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CREATE_BOSS_AND_GUARDS] BOSS  WAITING FOR CAN_REGISTER_MISSION_ENTITIES")
//					ENDIF
//				ELSE
//					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CREATE_BOSS_AND_GUARDS] BOSS  WAITING FOR HAS_MODEL_LOADED")
//				ENDIF
//			ENDIF
			
			FOR i = 1 TO 9
				GET_NTH_GUARD_POS_AND_HEADING(i-1, vCreate, fHead, fDefSpehereSize)
				
				CREATE_GUARD_PED(i, mGuards, vCreate, fHead, fDefSpehereSize, relGroupAssault)
			ENDFOR
		BREAK
	ENDSWITCH
	
	IF DO_ALL_PEDS_EXIST_FOR_VARIATION()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_AI_PEDS_FOR_VARIATION()
	SWITCH serverBD.iVariation
		CASE GB_ASSAULT_ALTRUIST	
		CASE GB_ASSAULT_ZANCUDO		
		CASE GB_ASSAULT_AIRPORT		
	//	CASE GB_ASSAULT_HUMANE		
		CASE GB_ASSAULT_MERRYWEATHER
			IF CREATE_BOSS_AND_GUARDS()
				RETURN TRUE
			ENDIF
		BREAK
			
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC
PROC MAINTAIN_AI_PED_BRAIN()
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		EXIT
	ENDIF
	
	INT i
	REPEAT MAX_GB_ASSAULT_PEDS i
		SWITCH serverBD.iPedState[i]
			CASE PED_AI_STATE_CREATED
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[i])
					IF GET_GB_ASSAULT_VARIATION() != GB_ASSAULT_AIRPORT
						serverBD.iPedState[i] = PED_AI_STATE_ATTACK_PLAYERS
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_AI_PED_BRAIN] PED ", i, " PED_AI_STATE_CREATED -> PED_AI_STATE_ATTACK_PLAYERS")
					ELSE
						serverBD.iPedState[i] = PED_AI_STATE_PLAYERS_APPROACH
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_AI_PED_BRAIN] PED ", i, " PED_AI_STATE_CREATED -> PED_AI_STATE_PLAYERS_APPROACH")
					ENDIF
				ENDIF
			BREAK
			
			CASE PED_AI_STATE_PLAYERS_APPROACH
				IF IS_BIT_SET(serverBD.iServerBitSet, biS_TargetsApproached)
					serverBD.iPedState[i] = PED_AI_STATE_ATTACK_PLAYERS
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_AI_PED_BRAIN] PED ", i, " PED_AI_STATE_PLAYERS_APPROACH -> PED_AI_STATE_ATTACK_PLAYERS AS biS_TargetsApproached")
				ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_ShootNearTarget)
					serverBD.iPedState[i] = PED_AI_STATE_ATTACK_PLAYERS
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_AI_PED_BRAIN] PED ", i, " PED_AI_STATE_PLAYERS_APPROACH -> PED_AI_STATE_ATTACK_PLAYERS AS biS_ShootNearTarget")
				ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_AimingAtTargets)
					serverBD.iPedState[i] = PED_AI_STATE_ATTACK_PLAYERS
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_AI_PED_BRAIN] PED ", i, " PED_AI_STATE_PLAYERS_APPROACH -> PED_AI_STATE_ATTACK_PLAYERS AS biS_AimingAtTargets")
				ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_DamagedTargets)
					serverBD.iPedState[i] = PED_AI_STATE_ATTACK_PLAYERS
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_AI_PED_BRAIN] PED ", i, " PED_AI_STATE_PLAYERS_APPROACH -> PED_AI_STATE_ATTACK_PLAYERS AS biS_DamagedTargets")
				
				ENDIF
			BREAK
			
			CASE PED_AI_STATE_ATTACK_PLAYERS
			
			BREAK
		ENDSWITCH
	ENDREPEAT
ENDPROC

PROC MAINTAIN_AI_PED_BODY()
	INT i
	
	REPEAT MAX_GB_ASSAULT_PEDS i
		SWITCH serverBD.iPedState[i]
			CASE PED_AI_STATE_CREATED
			BREAK
			
			CASE PED_AI_STATE_PLAYERS_APPROACH
			BREAK
			
			CASE PED_AI_STATE_ATTACK_PLAYERS
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[i])
					IF MAINTAIN_OWNERSHIP_OF_NETWORK_ID(serverBD.niPed[i])
						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niPed[i])
							IF NOT IS_NET_PED_INJURED(serverBD.niPed[i])
								IF (GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niPed[i]), SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) != PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niPed[i]), SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) != WAITING_TO_START_TASK)
									IF GET_GB_ASSAULT_VARIATION() = GB_ASSAULT_AIRPORT
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(serverBD.niPed[i]), FALSE)
									ENDIF
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(NET_TO_PED(serverBD.niPed[i]), 299 )
						//			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_AI_PED_BRAIN] I HAVE TOLD PED ", i, " TO ATTACK THE PLAYERS")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF	
			BREAK
		ENDSWITCH
		
		IF i > 0 // Ignore guy with package
			UPDATE_ENEMY_NET_PED_BLIP(serverBD.niPed[i], gBPedBlipData[i])
		ENDIF
	ENDREPEAT
ENDPROC

PROC GB_ASSAULT_SET_MERRYWEATHER_GATES_OPEN()
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [GB_ASSAULT_SET_MERRYWEATHER_GATES_OPEN] CALLED ")
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(MP_DOOR_MERRYWEATHER_BASE_1)
		DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_MERRYWEATHER_BASE_1,DOORSTATE_LOCKED, FALSE)
		DOOR_SYSTEM_SET_OPEN_RATIO(MP_DOOR_MERRYWEATHER_BASE_1,1.0,FALSE, FALSE)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [GB_ASSAULT_SET_MERRYWEATHER_GATES_OPEN] SET MP_DOOR_MERRYWEATHER_BASE_1 OPEN ")
	ENDIF
	
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(MP_DOOR_MERRYWEATHER_BASE_2)
		DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_MERRYWEATHER_BASE_2,DOORSTATE_LOCKED, FALSE)
		DOOR_SYSTEM_SET_OPEN_RATIO(MP_DOOR_MERRYWEATHER_BASE_2,1.0,FALSE, FALSE)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [GB_ASSAULT_SET_MERRYWEATHER_GATES_OPEN] SET MP_DOOR_MERRYWEATHER_BASE_2 OPEN ")
	ENDIF
	
ENDPROC

PROC GB_ASSAULT_SET_MERRYWEATHER_GATES_CLOSED()
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [GB_ASSAULT_SET_MERRYWEATHER_GATES_CLOSED] CALLED ")
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(MP_DOOR_MERRYWEATHER_BASE_1)
		DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_MERRYWEATHER_BASE_1,DOORSTATE_UNLOCKED, FALSE)
		DOOR_SYSTEM_SET_OPEN_RATIO(MP_DOOR_MERRYWEATHER_BASE_1,0.0,FALSE, FALSE)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [GB_ASSAULT_SET_MERRYWEATHER_GATES_CLOSED] SET MP_DOOR_MERRYWEATHER_BASE_1 CLOSED ")
	ENDIF
	
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(MP_DOOR_MERRYWEATHER_BASE_2)
		DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_MERRYWEATHER_BASE_2,DOORSTATE_UNLOCKED, FALSE)
		DOOR_SYSTEM_SET_OPEN_RATIO(MP_DOOR_MERRYWEATHER_BASE_2,0.0,FALSE, FALSE)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [GB_ASSAULT_SET_MERRYWEATHER_GATES_CLOSED] SET MP_DOOR_MERRYWEATHER_BASE_2 CLOSED ")
	ENDIF
	
ENDPROC


FUNC INT GET_PACKAGE_DROP_OFF_FOR_VARIATION()
		/*
	CONST_INT GB_ASSAULT_ALTRUIST			0
CONST_INT GB_ASSAULT_ZANCUDO			1
CONST_INT GB_ASSAULT_AIRPORT			2	
CONST_INT GB_ASSAULT_HUMANE				3
CONST_INT GB_ASSAULT_MERRYWEATHER		4
	*/
	
	IF serverBD.iDropOff != -1
		RETURN serverBD.iDropOff
	ENDIF
	
	INT iRand = 0
	INT iVar = GET_GB_ASSAULT_VARIATION()
	SWITCH iVar
		CASE GB_ASSAULT_ALTRUIST	
		CASE GB_ASSAULT_ZANCUDO		
		CASE GB_ASSAULT_AIRPORT		
	//	CASE GB_ASSAULT_HUMANE		
		CASE GB_ASSAULT_MERRYWEATHER
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
		BREAK
	ENDSWITCH
	
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [GET_PACKAGE_DROP_OFF_FOR_VARIATION] USING DROP-OFF ", irand, " AS VARIATION IS ", iVar)
	

	serverBD.iDropOff = iRand
	
	RETURN serverBD.iDropOff
ENDFUNC

PROC BLOCK_GANG_ATTACK_FOR_ASSAULT_VARIATION()

	//-- Altruist, drop off 0
	IF GET_GB_ASSAULT_VARIATION() = GB_ASSAULT_ALTRUIST
		IF GET_PACKAGE_DROP_OFF_FOR_VARIATION() = 0
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [BLOCK_GANG_ATTACK_FOR_ASSAULT_VARIATION] BLOCKING GANG ATTACK ALTRUIST DROP-OFF 0")
			Block_MissionsAtCoords_Specific_Gang_Attack("PY8hdiWoAkudg9SpK8G2xQ")
		ENDIF
	ENDIF
ENDPROC


PROC UNBLOCK_GANG_ATTACK_FOR_ASSAULT_VARIATION()
	TEXT_LABEL_23 tl23ContentId 
	
	//-- Altruist, drop off 0
	IF GET_GB_ASSAULT_VARIATION() = GB_ASSAULT_ALTRUIST
		IF GET_PACKAGE_DROP_OFF_FOR_VARIATION() = 0
			tl23ContentId = "PY8hdiWoAkudg9SpK8G2xQ"
			Unblock_MissionsAtCoords_Specific_Gang_Attack(tl23ContentId)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [UNBLOCK_GANG_ATTACK_FOR_ASSAULT_VARIATION] UNBLOCKING GANG ATTACK ALTRUIST DROP-OFF 0")
	
		ENDIF
	ENDIF
ENDPROC

FUNC VECTOR GET_PACKAGE_DROP_OFF_COORDS()
	INT iVar =  GET_GB_ASSAULT_VARIATION()
	INT iDropOFf = GET_PACKAGE_DROP_OFF_FOR_VARIATION()
	VECTOR vLoc = << 0.0, 0.0, 0.0 >>

	SWITCH iVar
		CASE GB_ASSAULT_ZANCUDO 
			//-- Fort Zancudo
			SWITCH iDropOFf
				CASE 0	vLoc = <<-575.3725, 5373.0488, 69.2404>>	BREAK
				CASE 1	vLoc = <<-3172.1350, 1110.6611, 19.8375>>	BREAK
				CASE 2	vLoc = <<872.9173, 2863.3372, 55.7587>>		BREAK
			ENDSWITCH		
		BREAK
		
		CASE GB_ASSAULT_ALTRUIST 
			SWITCH iDropOFf
				CASE 0	vLoc = <<2308.7039, 4886.4038, 40.8082>>	BREAK
				CASE 1	vLoc = <<1209.8130, 1859.0686, 77.9116>>	BREAK
				CASE 2	vLoc = <<-1928.4946, 2061.4182, 139.8375>>	BREAK
			ENDSWITCH		
		BREAK
	
		CASE GB_ASSAULT_AIRPORT
			SWITCH iDropOFf
				CASE 0	vLoc = <<-3020.8167, 84.6034, 10.6835>>		BREAK
				CASE 1	vLoc = <<-1041.6373, 796.9575, 166.2528>>	BREAK
				CASE 2	vLoc = <<690.2265, 602.1689, 127.9112>>		BREAK
			ENDSWITCH	
		BREAK

		CASE GB_ASSAULT_MERRYWEATHER
			SWITCH iDropOFf
				CASE 0	vLoc = <<1530.0585, 1727.6393, 108.9429>>	BREAK
				CASE 1	vLoc = <<783.2252, 1278.7393, 359.2967>>	BREAK
				CASE 2	vLoc = <<-1796.5653, 407.1885, 112.4107>>	BREAK
			ENDSWITCH		
		BREAK
	ENDSWITCH
	
	//CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [GET_PACKAGE_DROP_OFF_COORDS] USING COORDS ", vLoc, " AS VARIATION IS ", iVar)
	
	#IF IS_DEBUG_BUILD
	IF IS_VECTOR_ZERO(vLoc)
		NET_SCRIPT_ASSERT("GB_ASSAULT - GET_PACKAGE_DROP_OFF_COORDS is at origin! Please tell Dave W")
	ENDIF
	#ENDIF
	
	RETURN vLoc
ENDFUNC

PROC TURN_OFF_DROP_OFF_GENERATORS()
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [TURN_OFF_DROP_OFF_GENERATORS] CALLED.... ")
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_TurnedOffGenerators)
		VECTOR vOff = << 200.0, 200.0, 200.0>>
		VECTOR vDropOff = GET_PACKAGE_DROP_OFF_COORDS()
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA((vDropOff - vOff), (vDropOff + vOff), FALSE )
		SET_BIT(serverBD.iServerBitSet, biS_TurnedOffGenerators)
		
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [TURN_OFF_DROP_OFF_GENERATORS] SET ")
	ENDIF
ENDPROC

PROC TURN_ON_DROP_OFF_GENERATORS()
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF IS_BIT_SET(serverBD.iServerBitSet, biS_TurnedOffGenerators)
			VECTOR vOff = << 200.0, 200.0, 200.0>>
			VECTOR vDropOff = GET_PACKAGE_DROP_OFF_COORDS()
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA((vDropOff - vOff), (vDropOff + vOff), TRUE )
			CLEAR_BIT(serverBD.iServerBitSet, biS_TurnedOffGenerators)
			
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [TURN_ON_DROP_OFF_GENERATORS] SET ")
		ENDIF
	ENDIF
ENDPROC

PROC RESET_ALL_PLAYER_BLIPS()
	INT i
	IF NOT IS_BIT_SET(iBoolsBitSet2, biL2_ResetAllPlayerBlips)
		
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [RESET_ALL_PLAYER_BLIPS] SET biL2_ResetAllPlayerBlips")
		SET_BIT(iBoolsBitSet2, biL2_ResetAllPlayerBlips)
		
		REPEAT NUM_NETWORK_PLAYERS i
			PLAYER_INDEX PlayerId = INT_TO_PLAYERINDEX(i)
			IF NETWORK_IS_PLAYER_ACTIVE(PlayerId)
				IF PlayerId != PLAYER_ID()
					IF IS_THIS_A_STEAL_VEHICLE_VARIATION()
						SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(PlayerId, RADAR_TRACE_TANK, FALSE)
					ELSE
						SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(PlayerId, RADAR_TRACE_ASSAULT_PACKAGE, FALSE)
					ENDIF
					
					SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(PlayerId, BLIP_COLOUR_RED, FALSE)
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC


/// PURPOSE:
///    Store my boss's participant ID in playerBD, so I don't have to calculate it every time
FUNC INT GET_MY_BOSS_PARTICPANT_ID()
	
	IF playerBD[PARTICIPANT_ID_TO_INT()].iMyBossPart != -1
		//-- Already stored
		RETURN playerBD[PARTICIPANT_ID_TO_INT()].iMyBossPart
	ENDIF
	
	PLAYER_INDEX playerBoss
	
	IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
		//-- I'm a a Boss, store my participant ID
		playerBD[PARTICIPANT_ID_TO_INT()].iMyBossPart = PARTICIPANT_ID_TO_INT()
		
		IF PARTICIPANT_ID_TO_INT() = serverBD.iBossPartWhoLaunched	
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [GET_MY_BOSS_PARTICPANT_ID] SETTING MYSELF CRITICAL AS I'M THE BOSS WHO LAUNCHED")
			GB_SET_LOCAL_PLAYER_CRITICAL_TO_JOB(TRUE)
		ENDIF
		
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [GET_MY_BOSS_PARTICPANT_ID] SETTING iMyBossPart TO ", PARTICIPANT_ID_TO_INT(), " (MYSELF) AS I'M A BOSS")
	ELSE
		//-- I'm not a boss
		IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
			playerBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
			IF playerBoss != INVALID_PLAYER_INDEX()
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [GET_MY_BOSS_PARTICPANT_ID] THINK MY BOSS IS ", GET_PLAYER_NAME(playerBoss))
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerBoss)
					playerBD[PARTICIPANT_ID_TO_INT()].iMyBossPart = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(playerBoss))
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [GET_MY_BOSS_PARTICPANT_ID] SETTING iMyBossPart TO ", playerBD[PARTICIPANT_ID_TO_INT()].iMyBossPart, " WHO IS PLAYER ",GET_PLAYER_NAME(playerBoss))
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN playerBD[PARTICIPANT_ID_TO_INT()].iMyBossPart
ENDFUNC

/// PURPOSE:
///    Determine if it was my boss that launched the assault Boss Work
FUNC BOOL DID_MY_BOSS_LAUNCH_GB_ASSAULT()
	INT iMyBossPart = GET_MY_BOSS_PARTICPANT_ID()
	IF iMyBossPart > -1
		IF serverBD.iBossPartWhoLaunched = iMyBossPart
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///     PURPOSE: Determine if cleanup is running because launcher has told us to quit, i.e. forced off, or if because a mission end goal has been reached
///    
FUNC BOOL IS_PLAYER_BEING_FORCED_OFF_SCRIPT()
	IF IS_BIT_SET(iBoolsBitSet, biL_ForcedOffScript)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [IS_PLAYER_BEING_FORCED_OFF_SCRIPT TRUE AS biL_ForcedOffScript")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_PackageDelivered)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [IS_PLAYER_BEING_FORCED_OFF_SCRIPT FALSE AS biS_PackageDelivered")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_DurationExpired)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [IS_PLAYER_BEING_FORCED_OFF_SCRIPT FALSE AS biS_DurationExpired")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_RivalGotPackage)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [IS_PLAYER_BEING_FORCED_OFF_SCRIPT FALSE AS biS_RivalGotPackage")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_BossLaunchedQuit)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [IS_PLAYER_BEING_FORCED_OFF_SCRIPT FALSE AS biS_BossLaunchedQuit")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_VehDelivered)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [IS_PLAYER_BEING_FORCED_OFF_SCRIPT FALSE AS biS_VehDelivered")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_VehicleWrecked)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [IS_PLAYER_BEING_FORCED_OFF_SCRIPT FALSE AS biS_VehicleWrecked")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_PlayerDestroyedVeh)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [IS_PLAYER_BEING_FORCED_OFF_SCRIPT FALSE AS biS_PlayerDestroyedVeh")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_NonPlayerDestroyedVeh)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [IS_PLAYER_BEING_FORCED_OFF_SCRIPT FALSE AS biS_NonPlayerDestroyedVeh")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_NonPartDestRhino)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [IS_PLAYER_BEING_FORCED_OFF_SCRIPT FALSE AS biS_NonPartDestRhino")
		RETURN FALSE
	ENDIF
	
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [IS_PLAYER_BEING_FORCED_OFF_SCRIPT TRUE AS FALLTHRU")
	
	RETURN TRUE
ENDFUNC

//Cleanup Mission Data
PROC SCRIPT_CLEANUP()
	
	ENTITY_INDEX ent

	
//	SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_GB_ASSAULT, FALSE)
//	SET_FREEMODE_FLOW_COMMS_DISABLED(FALSE)

	//--Leave the rhino if I'm in it and I'm no longer a gang member
	IF IS_THIS_A_STEAL_VEHICLE_VARIATION()
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])
			IF IS_BIT_SET(iBoolsBitSet2, biL2_GangMemberWhenStarted)
				IF NOT GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
					IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[0])
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh[0]))
								TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  CLEANUP MISSION TASK_LEAVE_ANY_VEHICLE")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF		
	ENDIF
	
	
//	IF GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()) = relUWPlayer
//		SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), relMyFmGroup)
//		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  CLEANUP MISSION - RESTORED MY FM REL GROUP     <----------     ") NET_NL()
//	ENDIF
	
//	REMOVE_UW_REL_GROUPS()
	
	TURN_ON_DROP_OFF_GENERATORS()
	
	SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(TRUE)
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(TRUE) 7")
	
	GB_SET_LOCAL_PLAYER_CRITICAL_TO_JOB(FALSE)
	
	
	IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneTelemetry)
		IF NETWORK_IS_GAME_IN_PROGRESS()
			IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
			OR DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
				SET_BIT(iBoolsBitSet, biL_DoneTelemetry)
				g_sGb_Telemetry_data.sdata.m_match1 = serverBD.iMatchId1
				g_sGb_Telemetry_data.sdata.m_match2 = serverBD.iMatchId2
				GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LEFT)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  CLEANUP - HAVEN'T SET TELEMETRY. ASSUME LEFT")
			ELSE
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  CLEANUP - DIDN'T SEND TELEMETRY AS DID_MY_BOSS_LAUNCH_GB_ASSAULT / DID_LOCAL_PLAYER_JOIN_AS_RIVAL")
			ENDIF
		ELSE
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  CLEANUP - NO TELEMETRY - NETWORK_IS_GAME_IN_PROGRESS")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iBoolsBitSet, biL_DoneTelemetry)
		PROCESS_CURRENT_BOSS_WORK_PLAYSTATS( GET_GB_ASSAULT_VARIATION())
	ENDIF
	
	//-- Reset Zancudo IPL
	IF IS_BIT_SET(iBoolsBitSet, biL_RemovedMilGatesIpl)
	//	ADD_IPL_WHEN_NO_LOAD_SCENE_IS_ACTIVE("CS3_07_MPGates")
		GB_SET_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_GB_RESTORE_MY_ZANCUDO_IPL)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  CLEANUP MISSION RE-ADDED MILITARY GATES IPL")
	ENDIF
	
	
	//-- Reset audio
	IF IS_BIT_SET(iBoolsBitSet, biL_InitAudio)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  CLEANUP [MAINTAIN_GB_ASSAULT_MUSIC] MISSION RESETTING AUDIO")
		TRIGGER_MUSIC_EVENT("BG_ASSAULT_STOP")
		
		SET_AUDIO_FLAG("DisableFlightMusic", FALSE)
		SET_AUDIO_FLAG("WantedMusicDisabled", FALSE) 
	ENDIF
	
	IF serverBD.iVariation = 1
		SET_VEHICLE_MODEL_IS_SUPPRESSED(RHINO, FALSE)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - CLEANUP UNSUPPRESSED RHINO")
	ENDIF
	
	SET_MAX_WANTED_LEVEL(5)
	
	IF NOT IS_PLAYER_BEING_FORCED_OFF_SCRIPT()
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niPackage)
				
				ent = NET_TO_ENT(serverBD.niPackage)
				DELETE_ENTITY(ent)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - CLEANUP DELETED PACKAGE")
			ENDIF
		ENDIF
	ELSE
		SET_BIT(iBoolsBitSet, biL_ShouldStop30s)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - SET biL_ShouldStop30s as IS_PLAYER_BEING_FORCED_OFF_SCRIPT")
	ENDIF
	
	IF IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
		SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY(FALSE)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - CLEANUP SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY = FALSE")
	ENDIF
	
	GB_COMMON_BOSS_MISSION_CLEANUP()
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  CLEANUP MISSION      <----------     ") NET_NL()
	
	IF MPGlobalsAmbience.iGBAssaultVar != -1
		MPGlobalsAmbience.iGBAssaultVar = -1
	ENDIF
			
	RESET_UNLOCK_AIRPORT_GATES()
	
	IF GET_GB_ASSAULT_VARIATION() = GB_ASSAULT_MERRYWEATHER
		GB_ASSAULT_SET_MERRYWEATHER_GATES_CLOSED()
	ENDIF
	
	//-- Turn radio control back on if not already done so
	IF IS_BIT_SET(iBoolsBitSet2, biL2_PrepareKill30s)
		IF NOT IS_BIT_SET(iBoolsBitSet2, biL2_RestoreRadio)
			SET_USER_RADIO_CONTROL_ENABLED(TRUE)
			SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
			CANCEL_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - CLEANUP SET_USER_RADIO_CONTROL_ENABLED(TRUE)")
		ENDIF
	ENDIF
	
	STOP_30S_COUNTDOWN_AUDIO()
	
	UNBLOCK_GANG_ATTACK_FOR_ASSAULT_VARIATION()
	
	IF IS_BIT_SET(iBoolsBitSet, biL_CountdownAudio)
//		iDurationSound = -1
		STOP_SOUND(iDurationSound)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - CLEANUP STOP_SOUND(iDurationSound)")
	ENDIF
			
	IF IS_BIT_SET(iBoolsBitSet, biL_SetupCustomSpawns)
		CLEAR_CUSTOM_SPAWN_POINTS()
		USE_CUSTOM_SPAWN_POINTS(FALSE)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - CLEANUP USE_CUSTOM_SPAWN_POINTS(FALSE)")
	ENDIF
	
	#IF IS_DEBUG_BUILD 
	NET_LOG("SCRIPT_CLEANUP")	
	IF serverBD.bSomeoneSPassed
		CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
	ENDIF
	
	IF MPGlobalsAmbience.iGBAssaultDebugVar != -1
		MPGlobalsAmbience.iGBAssaultDebugVar = -1
	ENDIF
	#ENDIF
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

FUNC BOOL SHOULD_MOVE_OFF_MISSION()
	BOOl bShouldMoveToIdleState
	IF SHOULD_FM_EVENT_DROP_TO_IDLE_STATE()
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [SHOULD_MOVE_OFF_MISSION] TRUE AS SHOULD_FM_EVENT_DROP_TO_IDLE_STATE")
		bShouldMoveToIdleState = TRUE
	ELIF AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE() 
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [SHOULD_MOVE_OFF_MISSION] TRUE AS AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE")
		bShouldMoveToIdleState = TRUE
	ELIF GET_PLAYER_CORONA_STATUS(PLAYER_ID()) > CORONA_STATUS_IDLE
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [SHOULD_MOVE_OFF_MISSION] TRUE AS GET_PLAYER_CORONA_STATUS")
		bShouldMoveToIdleState = TRUE
	ENDIF
	
	IF bShouldMoveToIdleState
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
			IF IS_ENTITY_ATTACHED_TO_ENTITY(NET_TO_OBJ(serverBD.niPackage), PLAYER_PED_ID())
				IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_GotThePackage)
					CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_GotThePackage)
				ENDIF
				//SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(FALSE)
				PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.niPackage), TRUE, TRUE)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT PREVENT_COLLECTION_OF_PORTABLE_PICKUP(TRUE) 8")
				DETACH_PORTABLE_PICKUP_FROM_PED(NET_TO_OBJ(serverBD.niPackage))
				SET_ENTITY_VISIBLE(NET_TO_OBJ(serverBD.niPackage), TRUE)
				HIDE_PORTABLE_PICKUP_WHEN_DETACHED(NET_TO_OBJ(serverBD.niPackage), FALSE)
				
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [SHOULD_MOVE_OFF_MISSION] TRUE AS SHOULD_MOVE_OFF_MISSION")

			ENDIF
		ENDIF
	ENDIF
	
	RETURN bShouldMoveToIdleState
ENDFUNC



//PURPOSE: Returns the Cash reward for completing the objective
FUNC INT GET_CASH_REWARD()
	RETURN g_sMPTunables.iUrbanWarfareCashReward
ENDFUNC

//Do necessary pre game start ini.
PROC PROCESS_PRE_GAME( MP_MISSION_DATA missionScriptArgs )
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission) ,  missionScriptArgs)	//NUM_NETWORK_PLAYERS, missionScriptArgs)	//
	//NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, PARTICIPANT_ID_TO_INT())  
	
	//Reserve Entities
	RESERVE_NETWORK_MISSION_OBJECTS(1)
	RESERVE_NETWORK_MISSION_PEDS(MAX_GB_ASSAULT_PEDS)
	RESERVE_NETWORK_MISSION_VEHICLES(MAX_GB_ASSUALT_VEHS)

	// This makes sure the net script is active, waits untill it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		#IF IS_DEBUG_BUILD NET_LOG("GB_ASSAULT -  FAILED TO RECEIVE INITIAL NETWORK BROADCAST")	#ENDIF
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  MISSION END - FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP D     <----------     ") NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		// This script will not be paused if another script calls PAUSE_GAME
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()

//			INT i
//			REPEAT MAX_PLAYERS i
//				serverBD.piPlayers[i] = INVALID_PLAYER_INDEX()
//			ENDREPEAT
		ENDIF
		
	//	SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_GB_ASSAULT, TRUE)
		
		SETUP_UW_REL_GROUPS()
		
		GB_COMMON_BOSS_MISSION_PREGAME() 
	
		//
		
		IF GB_IS_GLOBAL_NON_BD_BIT0_SET(eGB_GLOBAL_NON_BD_BITSET_0_GB_RESTORE_MY_ZANCUDO_IPL)
			GB_CLEAR_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_GB_RESTORE_MY_ZANCUDO_IPL)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - CLEAR eGB_GLOBAL_NON_BD_BITSET_0_GB_RESTORE_MY_ZANCUDO_IPL AS WAS SET BUT RELAUNCHING")
		ENDIF
		
		playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_INI
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - TUNABLES....")
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - TUNABLES.... g_sMPTunables.igb_assault_time_limit 			= ", GET_GB_ASSAULT_DURATION())
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - TUNABLES.... g_sMPTunables.igb_assault_altruist_wanted      	= ", g_sMPTunables.igb_assault_altruist_wanted)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - TUNABLES.... g_sMPTunables.igb_assault_zancudo_wanted       	= ", g_sMPTunables.igb_assault_zancudo_wanted)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - TUNABLES.... g_sMPTunables.igb_assault_merryweather_wanted  	= ", g_sMPTunables.igb_assault_merryweather_wanted)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - TUNABLES.... g_sMPTunables.igb_assault_lsia_wanted          	= ", g_sMPTunables.igb_assault_lsia_wanted)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - TUNABLES.... g_sMPTunables.igb_assault_merryweather_wanted	= ", GET_RHINO_HEALTH_MODIFIER())
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - ")
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  PRE_GAME DONE     <----------     ") NET_NL()
	ELSE
		#IF IS_DEBUG_BUILD NET_LOG("GB_ASSAULT -  NETWORK_IS_GAME_IN_PROGRESS = FALSE")	#ENDIF
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  MISSION END - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP Q     <----------     ") NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
ENDPROC


PROC MOVE_TO_EXPLODE_VEHICLE_STAGE()
//	IF serverBD.eStage != eAD_EXPLODE_VEH
//		IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh))
//			//Send Ticker
//			SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
//			TickerEventData.TickerEvent = TICKER_EVENT_UW_EXPLODE	//TICKER_EVENT_UW_EXPLODE
//			TickerEventData.dataInt = 30
//			BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_IN_RANGE_OF_POINT(GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niVeh)), VEH_EXP_RANGE, TRUE))
//		ENDIF
//		
//		Clear_Any_Objective_Text_From_This_Script()
//		
////		REINIT_NET_TIMER(serverBD.ExplodeVehTimer)
//		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - ExplodeVehTimer STARTED")
//
//		serverBD.eStage = eAD_EXPLODE_VEH
//		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - serverBD.eStage = eAD_EXPLODE_VEH ")
//	ENDIF
ENDPROC				

FUNC BOOL ARE_ALL_PEDS_CLEANED_UP()
	
	
	RETURN TRUE
ENDFUNC

FUNC BOOL MISSION_END_CHECK()

	
	IF serverBD.eStage > eGB_INIT
		IF serverBD.iServerGameState != GAME_STATE_END
//			IF IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh))
//				IF serverBD.eStage = eAD_EXPLODE_VEH
//					IF ARE_ALL_PEDS_CLEANED_UP()
//						NET_PRINT_TIME() NET_PRINT("     ---------->     GB_ASSAULT -  MISSION END - VEH EXPLODED AT MISSION END   <----------     ") NET_NL()
//						RETURN TRUE
//					ENDIF
//				ELSE
//					IF HAS_NET_TIMER_EXPIRED(serverBD.DestroyCheckDelayTimer, 3000)	//6000
//						IF ARE_ALL_PEDS_CLEANED_UP()
//							NET_PRINT_TIME() NET_PRINT("     ---------->     GB_ASSAULT -  MISSION END - VEH DESTROYED     <----------     ") NET_NL()
//							RETURN TRUE
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
		ENDIF
		
		//All Players are Off Mission
//		IF serverBD.eStage < eAD_EXPLODE_VEH
//			BOOL bPlayersOff = TRUE
//			INT i
//			REPEAT MAX_PLAYERS i
//				IF serverBD.piPlayers[i] != INVALID_PLAYER_INDEX()
//					IF IS_NET_PLAYER_OK(serverBD.piPlayers[i], FALSE)
//						IF NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(serverBD.piPlayers[i])) >= 0
//							IF playerBD[NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(serverBD.piPlayers[i]))].eStage != eGB_OFF_MISSION
//								bPlayersOff = FALSE
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDREPEAT
//			IF bPlayersOff = TRUE
//				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  MISSION END - DRIVER AND GUNNER ARE OFF MISSION    <----------     ") NET_NL()
//				MOVE_TO_EXPLODE_VEHICLE_STAGE()
//				//RETURN TRUE
//			ELSE
//				IF serverBD.eStage < eAD_EXPLODE_VEH
//					IF IS_VEHICLE_EMPTY(NET_TO_VEH(serverBD.niVeh), TRUE)
//						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  MISSION END - NOBODY IN VEHICLE    <----------     ") NET_NL()
//						MOVE_TO_EXPLODE_VEHICLE_STAGE()
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Server only function that checks to see if the game mode should now end. For this test script, it if time has passed.
FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET()
	
	#IF IS_DEBUG_BUILD
		IF bHostEndMissionNow
			RETURN TRUE
		ENDIF
		
		IF ServerBD.bSomeoneSPassed
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [HAVE_MISSION_END_CONDITIONS_BEEN_MET] TRUE AS bSomeoneSPassed")
			RETURN TRUE
		ENDIF
	#ENDIF	
	
	IF MISSION_END_CHECK()
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllPlayersFinished)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [HAVE_MISSION_END_CONDITIONS_BEEN_MET] TRUE AS biS_AllPlayersFinished")
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC



//Helper function to get a clients game/mission state
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iGameState
ENDFUNC



FUNC BOOL SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD()
	IF NOT MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI 
		RETURN FALSE
	ENDIF
	
	IF GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
		IF NOT IS_BIT_SET(iBoolsBitSet2, bil2_ResetAsRestricted)
			IF NOT IS_THIS_A_STEAL_VEHICLE_VARIATION()
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
					SET_BIT(iBoolsBitSet2, bil2_ResetAsRestricted)
					PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.niPackage), TRUE, TRUE)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD - SET bil2_ResetAsRestricted / package exists")
				ENDIF
			ELSE
				SET_BIT(iBoolsBitSet2, bil2_ResetAsRestricted)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD - SET bil2_ResetAsRestricted / NOT IS_THIS_A_STEAL_VEHICLE_VARIATION")
			ENDIF
		ENDIF
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD - FALSE AS GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE")
		RETURN FALSE
	ENDIF
	
	IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) < GB_UI_LEVEL_BACKGROUND
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD - FALSE AS GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())  = ", ENUM_TO_INT(GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) ))
		RETURN FALSE
	ENDIF
	
	IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
//		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD TRUE DID_MY_BOSS_LAUNCH_GB_ASSAULT")
		RETURN TRUE
	ENDIF
	
	IF DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
//		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD TRUE DID_LOCAL_PLAYER_JOIN_AS_RIVAL")
		RETURN TRUE
	ENDIF
	
	
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
PROC CREATE_WIDGETS()
	INT iPlayer
	TEXT_LABEL_15 tl63

	START_WIDGET_GROUP("GB_ASSAULT")  
		ADD_WIDGET_BOOL("Remove Gates", bWdRemoveGates)
		ADD_WIDGET_BOOL("Add Gates", bWdAddGates)
		
		ADD_WIDGET_BOOL("Show Dist from start", bWdShowDistFromStart)
		ADD_WIDGET_FLOAT_SLIDER("Dist", fWdDistFromStart, 0.0, 10000.0, 0.1)
		
		ADD_WIDGET_BOOL("Explode", bWdTestExplode)
		// Gameplay debug", sever only.
		START_WIDGET_GROUP("Server Only Gameplay") 

			ADD_WIDGET_BOOL("End mission", bHostEndMissionNow)			
		STOP_WIDGET_GROUP()		
		
		// Data about the server
		START_WIDGET_GROUP("Server BD") 
			ADD_WIDGET_INT_SLIDER("S. Game state", serverBD.iServerGameState,-1, HIGHEST_INT,1)

		STOP_WIDGET_GROUP()	

		// Data about the clients. * = You.
		START_WIDGET_GROUP("Client BD")  				
			REPEAT NUM_NETWORK_PLAYERS iPlayer
				tl63 = "Player "
				tl63 += iPlayer
				IF iPlayer = PARTICIPANT_ID_TO_INT()
					tl63 += "*"
				ENDIF
				START_WIDGET_GROUP(tl63)
					ADD_WIDGET_INT_SLIDER("Game state", playerBD[iPlayer].iGameState,-1, HIGHEST_INT,1)
				STOP_WIDGET_GROUP()
			ENDREPEAT				
		STOP_WIDGET_GROUP()	
		
	STOP_WIDGET_GROUP()
ENDPROC		

PROC UPDATE_WIDGETS()
	IF bWdRemoveGates
		bWdRemoveGates = FALSE
		REMOVE_IPL("CS3_07_MPGates")
	ENDIF
	
	IF bWdAddGates
		bWdAddGates = FALSE
		REQUEST_IPL("CS3_07_MPGates")
	ENDIF
	
	IF bWdShowDistFromStart
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF serverBD.iVariation != -1
				fWdDistFromStart = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_LOCATION_FOR_VARIATION(), FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	IF bWdTestExplode
		bWdTestExplode = FALSE
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - EXPLODING!")
		NETWORK_EXPLODE_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
	ENDIF
		
ENDPROC

PROC MAINTAIN_DEBUG_KEYS()
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPDebugBitset, PARTICIPANT_ID_TO_INT())
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S) 
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPDebugBitset, PARTICIPANT_ID_TO_INT())
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_DEBUG_KEYS] I S-PASSED ")
		ENDIF
	ENDIF
ENDPROC

PROC GB_ASSAULT_TIMED_DEBUG()
	IF NOT HAS_NET_TIMER_STARTED(timeDebug)
		START_NET_TIMER(timeDebug)
	ENDIF
		
	IF HAS_NET_TIMER_EXPIRED(timeDebug, 10000)
		GB_UI_LEVEL gbUiLevel = GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())
		BOOL bPackageBlipExists = DOES_BLIP_EXIST(blipPackage)
		BOOL bDropOffBlipExists = DOES_BLIP_EXIST(blipDropOff)
		BOOL bVehBlipExists	= DOES_BLIP_EXIST(blipVeh)
		BOOL bPackageExists = NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
		BOOL bVehicleExists = NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])
		BOOL bShouldSeeHud = SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD()
		BOOL bIsPermanent =  GB_IS_GLOBAL_CLIENT_BIT0_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_0_PERMANENT_PARTICIPANT)
		BOOL bJoinedAsRival = DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
		
		PLAYER_INDEX playerWithEntity
		PLAYER_INDEX playerBossLaunch
		
		IF serverBD.iBossPlayerWhoLaunched > -1
			IF NETWORK_IS_PLAYER_ACTIVE(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
				playerBossLaunch = INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched)
			ENDIF
		ENDIF
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->   [GB_ASSAULT_TIMED_DEBUG]")
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->   [GB_ASSAULT_TIMED_DEBUG] OUTPUT....")
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->   [GB_ASSAULT_TIMED_DEBUG] 	Variation = ", GET_GB_ASSAULT_VARIATION())
		IF serverBD.iBossPlayerWhoLaunched > -1
			IF NETWORK_IS_PLAYER_ACTIVE(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
				playerBossLaunch = INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->   [GB_ASSAULT_TIMED_DEBUG] BOSS WHO LAUNCHED ",GET_PLAYER_NAME(playerBossLaunch))
			ENDIF
		ENDIF 
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->   [GB_ASSAULT_TIMED_DEBUG] 	Join as rival? ", bJoinedAsRival)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->   [GB_ASSAULT_TIMED_DEBUG] 	bIsPermanent? ", bIsPermanent)
		
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->   [GB_ASSAULT_TIMED_DEBUG]	playerBD[PARTICIPANT_ID_TO_INT()].eStage = ", playerBD[PARTICIPANT_ID_TO_INT()].eStage)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->   [GB_ASSAULT_TIMED_DEBUG]")
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->   [GB_ASSAULT_TIMED_DEBUG]	GB_GET_PLAYER_UI_LEVEL() = ", ENUM_TO_INT(gbUiLevel))
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->   [GB_ASSAULT_TIMED_DEBUG]	SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD() = ", bShouldSeeHud)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->   [GB_ASSAULT_TIMED_DEBUG]")
		
		IF NOT IS_THIS_A_STEAL_VEHICLE_VARIATION()
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->   [GB_ASSAULT_TIMED_DEBUG]	Package exist? ", bPackageExists)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->   [GB_ASSAULT_TIMED_DEBUG]	iLocalPartPackageHolder = ", iLocalPartPackageHolder)
			
			IF serverBD.iPartPackageHolder = -1
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->   [GB_ASSAULT_TIMED_DEBUG]	serverBD.iPartPackageHolder = ", serverBD.iPartPackageHolder)
			ELSE	
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(serverBD.iPartPackageHolder))
					playerWithEntity = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.iPartPackageHolder))
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->   [GB_ASSAULT_TIMED_DEBUG]	serverBD.iPartPackageHolder = ", serverBD.iPartPackageHolder, " PLAYER ", GET_PLAYER_NAME(playerWithEntity))
				ENDIF
			ENDIF
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->   [GB_ASSAULT_TIMED_DEBUG]	bPackageBlipExists = ", bPackageBlipExists)
		ELSE
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->   [GB_ASSAULT_TIMED_DEBUG]	Vehicle exist? ", bVehicleExists)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->   [GB_ASSAULT_TIMED_DEBUG]	iLocalPartVehDriver = ", iLocalPartVehDriver)		
			IF serverBD.iPartInVeh = -1
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->   [GB_ASSAULT_TIMED_DEBUG]	serverBD.iPartInVeh = ", serverBD.iPartInVeh)
			ELSE	
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(serverBD.iPartInVeh))
					playerWithEntity = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.iPartInVeh))
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->   [GB_ASSAULT_TIMED_DEBUG]	serverBD.iPartInVeh = ", serverBD.iPartPackageHolder, " PLAYER ", GET_PLAYER_NAME(playerWithEntity))
				ENDIF
			ENDIF
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->   [GB_ASSAULT_TIMED_DEBUG]	bVehBlipExists = ", bVehBlipExists)
		ENDIF
		
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->   [GB_ASSAULT_TIMED_DEBUG]	Drop-off blip exist? ", bDropOffBlipExists)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->   [GB_ASSAULT_TIMED_DEBUG]")
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->   [GB_ASSAULT_TIMED_DEBUG]	iServerBitSet ", serverBd.iServerBitSet)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->   [GB_ASSAULT_TIMED_DEBUG]	iPlayerBitSet ", playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->   [GB_ASSAULT_TIMED_DEBUG]	iBoolsBitSet ", iBoolsBitSet)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->   [GB_ASSAULT_TIMED_DEBUG]")
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->   [GB_ASSAULT_TIMED_DEBUG]	END")
		RESET_NET_TIMER(timeDebug)
	ENDIF
ENDPROC
#ENDIF

FUNC STRING GET_VEHICLE_TYPE_STRING()
	RETURN "" //GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(serverBD.VehModel)
ENDFUNC

FUNC STRING GET_VEH_BLIP_STRING()
//	IF serverBD.VehModel = RHINO
//		RETURN "ABLIP_TANK"			//"~BLIP_TEMP_6~"
//	ELIF IS_THIS_MODEL_A_PLANE(serverBD.VehModel)
//		RETURN "ABLIP_PLANE"		//"~BLIP_TEMP_6~"
//	ENDIF
//	
//	RETURN "ABLIP_HELI"				//"~BLIP_TEMP_6~"*/
	RETURN ""
ENDFUNC

//PURPOSE: Adds a blip to the Veh
PROC ADD_VEH_BLIP()
//	IF NOT DOES_BLIP_EXIST(VehBlip)
//		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
//			IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh))
//				VehBlip = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(serverBD.niVeh))
//				SET_BLIP_SPRITE(VehBlip, RADAR_TRACE_TANK) 
//			//	SET_BLIP_COLOUR(VehBlip, BLIP_COLOUR_PURPLE)
//				SET_BLIP_COLOUR_FROM_HUD_COLOUR(VehBlip, HUD_COLOUR_NET_PLAYER2)
//				//SHOW_HEIGHT_ON_BLIP(VehBlip, TRUE)
//				SET_BLIP_FLASH_TIMER(VehBlip, 7000)
//				SET_BLIP_PRIORITY(VehBlip, BLIPPRIORITY_HIGHEST)
//				SET_BLIP_NAME_FROM_TEXT_FILE(VehBlip, "UW_BLIP")	//GB_ASSAULT
//				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  VEH BLIP ADDED ") NET_NL()
//			ENDIF
//		ENDIF
//	ENDIF
ENDPROC

PROC REMOVE_VEHICLE_BLIP()
//	IF DOES_BLIP_EXIST(VehBlip)
//		REMOVE_BLIP(VehBlip)
//		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  VEHICLE BLIP REMOVED    <----------     ") NET_NL()
//	ENDIF
ENDPROC

PROC DO_HELP_TEXT(INT iHelp)
	
	SWITCH iHelp
		CASE 1
//			IF NOT IS_BIT_SET(iBoolsBitSet, biHelp1Done)
//				IF HAS_PLAYER_BEEN_IN_SESSION_THIS_LONG(CRATE_DROP_DISPLAY_DELAY)
//				AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
//				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
//				AND NOT IS_PAUSE_MENU_ACTIVE()
//					PRINT_HELP_WITH_STRING_AND_NUMBER("UW_HELP1", GET_VEH_BLIP_STRING(), GET_CASH_REWARD())	//A ~p~Hot Target~s~ ~p~~a~ ~s~is available. Enter it and destroy Merryweather patrols for $~1~.
//					SET_BIT(iBoolsBitSet, biHelp1Done)
//					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  HELP TEXT - biHelp1Done SET    <----------     ")
//				ENDIF
//			ENDIF
		BREAK
		/*CASE 2
			IF NOT IS_BIT_SET(iBoolsBitSet, biHelp2Done)
				IF HAS_PLAYER_BEEN_IN_SESSION_THIS_LONG(CRATE_DROP_DISPLAY_DELAY)
				AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_PAUSE_MENU_ACTIVE()
					PRINT_HELP_WITH_STRING("UW_HELP2", GET_VEHICLE_TYPE_STRING())	//You are a ~r~Hot Target~s~. Other players will be rewarded for killing you while you are delivering the ~a~.
					SET_BIT(iBoolsBitSet, biHelp2Done)
					PRINTLN("     ---------->     GB_ASSAULT - HELP TEXT - biHelp2Done SET    <----------     ")
				ENDIF
			ENDIF
		BREAK*/
	ENDSWITCH
ENDPROC

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  ENTITY CREATION                  /////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////
///    			VEHICLE   				///
///////////////////////////////////////////
//PURPOSE: Create the Veh
FUNC BOOL CREATE_VEH()

	
	RETURN TRUE
ENDFUNC

//PURPOSE: Creates the Veh and Driver that will deliver the Vehicle
FUNC BOOL CREATE_MAIN_VEHICLE()

	
	RETURN FALSE
ENDFUNC






FUNC VECTOR GET_PACKAGE_START_COORDS()
	INT iVar =  GET_GB_ASSAULT_VARIATION()
	VECTOR vLoc = << 0.0, 0.0, 0.0 >>
	
	SWITCH iVar
		CASE GB_ASSAULT_ALTRUIST 		vLoc = <<-1166.8096, 4928.6948, 222.1781>>	BREAK 
		CASE GB_ASSAULT_ZANCUDO 		vLoc = <<-2117.4412, 3301.3374, 31.8103>>	BREAK // Zancudo
		CASE GB_ASSAULT_AIRPORT 		vLoc = <<-1298.0688, -3398.0505, 12.9452>>	BREAK 
	//	CASE GB_ASSAULT_HUMANE 			vLoc = <<3572.0518, 3721.3396, 35.6427>>	BREAK 
		CASE GB_ASSAULT_MERRYWEATHER 	vLoc = <<567.0107, -3125.8000, 17.7686>>		BREAK 
	ENDSWITCH
	
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [GET_PACKAGE_START_COORDS] USING COORDS ", vLoc, " AS VARIATION IS ", iVar)
	
	#IF IS_DEBUG_BUILD
	IF IS_VECTOR_ZERO(vLoc)
		NET_SCRIPT_ASSERT("GB_ASSAULT - GET_PACKAGE_START_COORDS is at origin! Please tell Dave W")
	ENDIF
	#ENDIF
	
	RETURN vLoc
	
ENDFUNC

FUNC BOOL CREATE_PACKAGE(BOOL bAttach = FALSE, INT iPedToAttachTo = 0, BOOL bForceCoords = FALSE)
	MODEL_NAMES mPackage = prop_ld_case_01
	SPAWN_SEARCH_PARAMS SpawnSearchParams
	VECTOR vCreate
	SpawnSearchParams.bConsiderInteriors = FALSE
	SpawnSearchParams.fMinDistFromPlayer = 10.0
	
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
		REQUEST_MODEL(mPackage)
		IF HAS_MODEL_LOADED(mPackage)
			IF CAN_REGISTER_MISSION_OBJECTS(1)
				vCreate = GET_PACKAGE_START_COORDS()
				IF NOT HAS_NET_TIMER_STARTED(serverBD.timeSpawnPackage)
					START_NET_TIMER(serverBD.timeSpawnPackage)
				ENDIF
				
				IF GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY(vCreate, 5.0, vCreateEntityCurrentAttemptedCoords, fCreateEntityCurrentAttemptedHeading, SpawnSearchParams)
				OR IS_BIT_SET(serverBD.iServerBitSet, biS_ForceSpawnPackage)
					IF IS_BIT_SET(serverBD.iServerBitSet, biS_ForceSpawnPackage)
						vCreateEntityCurrentAttemptedCoords = vCreate
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [CREATE_PACKAGE] - USING vCreateEntityCurrentAttemptedCoords = vCreate = ", vCreateEntityCurrentAttemptedCoords, " AS biS_ForceSpawnPackage")
					ENDIF
					serverBD.niPackage = OBJ_TO_NET(CREATE_PORTABLE_PICKUP(PICKUP_PORTABLE_CRATE_FIXED_INCAR, vCreateEntityCurrentAttemptedCoords, TRUE, mPackage))
					SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(serverBD.niPackage, TRUE)
				//	NETWORK_FADE_IN_ENTITY(NET_TO_OBJ(serverBD.niPackage), TRUE)
					SET_ENTITY_COORDS_NO_OFFSET(NET_TO_OBJ(serverBD.niPackage), vCreateEntityCurrentAttemptedCoords+<< 0.0, 0.0, 0.5 >>)
					SET_MODEL_AS_NO_LONGER_NEEDED(mPackage)
					SET_ENTITY_INVINCIBLE(NET_TO_OBJ(serverBD.niPackage), TRUE)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_OBJ(serverBD.niPackage), TRUE)
				//	NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_OBJ(serverBD.niPackage), TRUE)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [CREATE_PACKAGE] PACKAGE CREATED AT ", vCreateEntityCurrentAttemptedCoords)
					
					IF bAttach
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[iPedToAttachTo])
							IF NOT IS_NET_PED_INJURED(serverBD.niPed[iPedToAttachTo])
								ATTACH_PORTABLE_PICKUP_TO_PED(NET_TO_OBJ(serverBD.niPackage),NET_TO_PED(serverBD.niPed[iPedToAttachTo])) 
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [CREATE_PACKAGE] PACKAGE ATTACHED TO PED ", iPedToAttachTo)
							ELSE
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [CREATE_PACKAGE] WANT TO ATTACH PACKAGE TO AI PED ", iPedToAttachTo, " BUT PED INJURED")
							ENDIF
						ELSE
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [CREATE_PACKAGE] WANT TO ATTACH PACKAGE TO AI PED ", iPedToAttachTo, " BUT PED DOESN'T EXIST")
						ENDIF
					ENDIF
					
					IF bForceCoords
						SET_ENTITY_COORDS_NO_OFFSET(NET_TO_OBJ(serverBD.niPackage), vCreate+<< 0.0, 0.0, 0.5 >>)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [CREATE_PACKAGE] - FORCED COORDS vCreate = ", vCreate)
					ENDIF
				ELSE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [CREATE_PACKAGE] WAITING FOR GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY")
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ForceSpawnPackage)
						IF HAS_NET_TIMER_EXPIRED(serverBD.timeSpawnPackage, 15000)	
							SET_BIT(serverBD.iServerBitSet, biS_ForceSpawnPackage)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [CREATE_PACKAGE] SET biS_ForceSpawnPackage")
						ENDIF
					ENDIF
							
					
				ENDIF
			ELSE
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [CREATE_PACKAGE] WAITING FOR CAN_REGISTER_MISSION_OBJECTS")
			ENDIF
		ELSE
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [CREATE_PACKAGE] WAITING FOR HAS_MODEL_LOADED")
		ENDIF
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL CREATE_PACKAGES_FOR_VARIATION()
	SWITCH serverBD.iVariation
		CASE GB_ASSAULT_ZANCUDO
			RETURN TRUE
		CASE GB_ASSAULT_ALTRUIST
		CASE GB_ASSAULT_AIRPORT
	//	CASE GB_ASSAULT_HUMANE
		
			IF CREATE_PACKAGE(TRUE, 0)
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE GB_ASSAULT_MERRYWEATHER
			IF CREATE_PACKAGE(DEFAULT, DEFAULT, TRUE)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


FUNC BOOL DO_ALL_VEHICLES_EXIST_FOR_VARIATION()
	SWITCH GET_GB_ASSAULT_VARIATION()
		CASE GB_ASSAULT_ZANCUDO
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])
			
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE GB_ASSAULT_AIRPORT
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])
			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[1])
			
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE GB_ASSAULT_ALTRUIST
		

			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])
			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[1])
		//	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[2])
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE GB_ASSAULT_MERRYWEATHER
			RETURN TRUE	
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_ASSAULT_VEHICLE(INT index, MODEL_NAMES mVeh, VECTOR vLoc, FLOAT fHead, BOOL bLockDoors = TRUE)

	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[index])
		REQUEST_MODEL(mVeh)
		IF HAS_MODEL_LOADED(mVeh)
			IF CAN_REGISTER_MISSION_ENTITIES(0, 1, 0, 0)
				IF NOT IS_BIT_SET(serverBD.iVehClearAreaBitset, index)
					CLEAR_AREA(vLoc, 5.0, TRUE)
					SET_BIT(serverBD.iVehClearAreaBitset, index)
				ENDIF
				IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(	vLoc, // VECTOR vPoint, 
														1.0, 							 // FLOAT fVehRadius = 6.0, 
														DEFAULT, 						 // FLOAT fPedRadius = 1.0, 
														DEFAULT,						 // FLOAT fObjRadius = 1.0,
														DEFAULT,						 // FLOAT fViewRadius = 5.0,
														DEFAULT,						 // BOOL bCheckThisPlayerSight = TRUE,
														DEFAULT,						 // BOOL bDoAnyPlayerSeePointCheck = TRUE,
														DEFAULT,						 // BOOL bCheckVisibilityForOwnTeam = TRUE,
														DEFAULT,					 // FLOAT fVisibleDistance = 120.0,
														DEFAULT, 						 // BOOL bIgnoreLocalPlayerChecks=FALSE, 
														DEFAULT,						 // INT iTeamToIgnore=-1,
														DEFAULT,						 // BOOL bDoFireCheck=TRUE,
														50.0) 						// FLOAT fPlayerRadius = 0.0,
					IF CREATE_NET_VEHICLE(serverBD.niVeh[index], mVeh, vLoc,fHead)
						NETWORK_FADE_IN_ENTITY(NET_TO_ENT(serverBD.niVeh[index]),TRUE)
						IF bLockDoors
							SET_VEHICLE_DOORS_LOCKED(NET_TO_VEH(serverBD.niVeh[index]), VEHICLELOCK_LOCKED)
						ELSE
							SET_VEHICLE_DOORS_LOCKED(NET_TO_VEH(serverBD.niVeh[index]), VEHICLELOCK_UNLOCKED)
						ENDIF
						SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.niVeh[index]), TRUE)
						SET_VEHICLE_CAN_BE_USED_BY_FLEEING_PEDS(NET_TO_VEH(serverBD.niVeh[index]), FALSE)
						SET_VEHICLE_AUTOMATICALLY_ATTACHES(NET_TO_VEH(serverBD.niVeh[index]), FALSE)
						
						SET_VEHICLE_DOORS_LOCKED_FOR_NON_SCRIPT_PLAYERS(NET_TO_VEH(serverBD.niVeh[index]), TRUE)
						FREEZE_ENTITY_POSITION(NET_TO_VEH(serverBD.niVeh[index]), FALSE)
						SET_ENTITY_DYNAMIC(NET_TO_VEH(serverBD.niVeh[index]), TRUE)
						ACTIVATE_PHYSICS(NET_TO_VEH(serverBD.niVeh[index]))
						SET_VEHICLE_ENGINE_ON(NET_TO_VEH(serverBD.niVeh[index]), TRUE, TRUE)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_VEH(serverBD.niVeh[index]), TRUE)
						SET_VEHICLE_INFLUENCES_WANTED_LEVEL(NET_TO_VEH(serverBD.niVeh[index]), FALSE)
						
						IF GET_GB_ASSAULT_VARIATION() = GB_ASSAULT_ZANCUDO
							IF mveh = RHINO
								FLOAT fHealth = TO_FLOAT(GET_ENTITY_HEALTH(NET_TO_VEH(serverBD.niVeh[index])))
								FLOAT fHealthMod = GET_RHINO_HEALTH_MODIFIER()
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CREATE_VEHICLES_FOR_VARIATION] [CREATE_ASSAULT_VEHICLE] RHINO HEALTH BEFOPRE MODIFIER = ", fHealth)
								
								fHealth *= fHealthMod
								
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CREATE_VEHICLES_FOR_VARIATION] [CREATE_ASSAULT_VEHICLE] RHINO HEALTH AFTER MODIFIER = ", fHealth, " MODIFIER = ", fHealthMod)
								
								SET_ENTITY_HEALTH(NET_TO_VEH(serverBD.niVeh[index]),ROUND(fHealth))
								SET_ENTITY_MAX_HEALTH(NET_TO_VEH(serverBD.niVeh[index]),ROUND(fHealth) )
								SET_VEHICLE_PETROL_TANK_HEALTH(NET_TO_VEH(serverBD.niVeh[index]),fHealth)
								SET_VEHICLE_BODY_HEALTH(NET_TO_VEH(serverBD.niVeh[index]),fHealth)
								SET_VEHICLE_ENGINE_HEALTH(NET_TO_VEH(serverBD.niVeh[index]),fHealth)
							ENDIF
						ENDIF
						
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CREATE_VEHICLES_FOR_VARIATION] [CREATE_ASSAULT_VEHICLE] CREATED VEH ", index)
					ENDIF
				ELSE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CREATE_VEHICLES_FOR_VARIATION] [CREATE_ASSAULT_VEHICLE] FAILING IS_POINT_OK_FOR_NET_ENTITY_CREATION index ", index)
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[index])
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC


FUNC BOOL CREATE_VEHICLES_FOR_VARIATION()
	 
	SWITCH serverBD.iVariation
		CASE GB_ASSAULT_ZANCUDO
			CREATE_ASSAULT_VEHICLE(0, RHINO, 	<<-2117.1023, 3292.8535, 31.8103>>,		174.0669, FALSE)

		BREAK
		
		CASE GB_ASSAULT_AIRPORT

			CREATE_ASSAULT_VEHICLE(0, FBI, 		<<-1297.8341, -3399.3682, 12.9452>>,	109.6593, FALSE)
			CREATE_ASSAULT_VEHICLE(1, superd, 	<<-1303.5046, -3396.0212, 12.9452>>,	152.3324, FALSE)
		BREAK
		
		CASE GB_ASSAULT_ALTRUIST
			CREATE_ASSAULT_VEHICLE(0, HEXER, 	<<-1057.8958, 4903.1143, 210.3096>>,	127.0391, FALSE)
			CREATE_ASSAULT_VEHICLE(1, HEXER, 	<<-1060.5990, 4903.0059, 210.6420>>,	126.9637, FALSE)
		//	CREATE_ASSAULT_VEHICLE(2, HEXER, 	<<-1062.8536, 4903.1758, 210.9118>>,	125.0162)
		BREAK
		
		CASE GB_ASSAULT_MERRYWEATHER
			RETURN TRUE
	ENDSWITCH
	
	IF NOT DO_ALL_VEHICLES_EXIST_FOR_VARIATION()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC











PROC MAINTAIN_MISSION_CRITICAL_VEH_WRECKED()
	IF IS_THIS_A_STEAL_VEHICLE_VARIATION()
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_VehWrecked)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])
				IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[0])
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_VehWrecked)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MISSION_CRITICAL_VEH_WRECKED] SET biP_VehWrecked")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC
//
//PROC MAINTAIN_MAX_PARTICIPANT_CHECKS_CLIENT()
//	INT i
//
//	IF serverBD.iServerGameState != GAME_STATE_END
//		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
//			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
//				PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
//				PED_INDEX pedPlayer = GET_PLAYER_PED(PlayerId)
//				
//				IF NOT IS_PLAYER_SCTV(PlayerId)
//				ENDIF
//			ENDIF
//		ENDREPEAT
//	ENDIF
//ENDPROC

/// PURPOSE:
///    Want to force rivals onto job after 10 minutes or so, to prevent launching gang holding up boss work launching
PROC MAINTAIN_FORCE_RIVALS_ON_MISSION_SERVER()
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_CollectedAtLeastOnce)
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ForceRivalsOnMission)
			IF NOT HAS_NET_TIMER_STARTED(serverBD.timeForceRivalsJoin)
				START_NET_TIMER(serverBD.timeForceRivalsJoin)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_FORCE_RIVALS_ON_MISSION_SERVER] STARTED timeForceRivalsJoin") 
			ELSE
				IF HAS_NET_TIMER_EXPIRED(serverBD.timeForceRivalsJoin, GET_GB_ASSAULT_FORCE_RIVALS_JOIN_TIME())
					SET_BIT(serverBD.iServerBitSet, biS_ForceRivalsOnMission)			
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_FORCE_RIVALS_ON_MISSION_SERVER] SET biS_ForceRivalsOnMission")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handle the mission duration (server side)
PROC MAINTAIN_MISSION_DURATION_SERVER()
	IF NOT HAS_NET_TIMER_STARTED(serverBD.timeDuration)
		IF IS_BIT_SET(serverBD.iServerBitSet, biS_StartDuration)
			START_NET_TIMER(serverBD.timeDuration)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] [MAINTAIN_MISSION_DURATION_SERVER] STARTED timeDuration")
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_RivalGotPackage)
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_DurationExpired)
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_PackageDelivered)
					IF HAS_NET_TIMER_EXPIRED(serverBD.timeDuration, GET_GB_ASSAULT_DURATION())
						SET_BIT(serverBD.iServerBitSet, biS_DurationExpired)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] [MAINTAIN_MISSION_DURATION_SERVER] SET biS_DurationExpired")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
				
ENDPROC

PROC MAINTAIN_PLAYER_CRITICAL_ON_FM_EVENT()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
		IF NOT IS_BIT_SET(iBoolsBitSet2, bil2_CriticalToFmEvent)
			IF FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT(PLAYER_ID())
				SET_BIT(iBoolsBitSet2, bil2_CriticalToFmEvent)
				PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.niPackage), TRUE, TRUE)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [MAINTAIN_PLAYER_CRITICAL_ON_FM_EVENT] PREVENT_COLLECTION_OF_PORTABLE_PICKUP(TRUE) - FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT")
			ELIF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) < GB_UI_LEVEL_BACKGROUND
				SET_BIT(iBoolsBitSet2, bil2_CriticalToFmEvent)
				PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.niPackage), TRUE, TRUE)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [MAINTAIN_PLAYER_CRITICAL_ON_FM_EVENT] PREVENT_COLLECTION_OF_PORTABLE_PICKUP(TRUE)- PREVENT_COLLECTION_OF_PORTABLE_PICKUP")
			ENDIF
		ELSE
			IF NOT FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT(PLAYER_ID())
				IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_BACKGROUND
					PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.niPackage), FALSE, TRUE)
					CLEAR_BIT(iBoolsBitSet2, bil2_CriticalToFmEvent)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [MAINTAIN_PLAYER_CRITICAL_ON_FM_EVENT] PREVENT_COLLECTION_OF_PORTABLE_PICKUP(FALSE)")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handle the mission duration (client side). Actually displays the timer
PROC MAINTAIN_MISSION_DURATION_CLIENT() 
	IF NOT HAS_NET_TIMER_STARTED(serverBD.timeDuration)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iBoolsBitSet2, biL2_WaitForShard)
		//-- Don't show timer if waiting for shard
		EXIT
	ENDIF
	
	INT iTimeLeft = GET_GB_ASSAULT_DURATION()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeDuration)
	
	IF NOT SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD()
		EXIT
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_PackageDelivered)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Finished)
		EXIT
	ENDIF
	
	IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) < GB_UI_LEVEL_FULL
		EXIT
	ENDIF
	
	
	IF iTimeLeft <= 10000
		IF NOT IS_BIT_SET(iBoolsBitSet, biL_CountdownAudio)
			iDurationSound = GET_SOUND_ID()
			PLAY_SOUND_FRONTEND(iDurationSound, "10s", "MP_MISSION_COUNTDOWN_SOUNDSET", FALSE) 
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MISSION_DURATION_CLIENT] STARTING COUNTDOWN AUDIO... iDurationSound = ", iDurationSound)
			SET_BIT(iBoolsBitSet, biL_CountdownAudio)
		ENDIF
	ENDIF
	
	MAINTAIN_30S_COUNTDOWN_MUSIC(iTimeLeft)
	
	IF iTimeLeft > 30000
		DRAW_GENERIC_TIMER(iTimeLeft, "GB_WORK_END",0, TIMER_STYLE_DONTUSEMILLISECONDS, 0, PODIUMPOS_NONE, HUDORDER_BOTTOM)
	ELSE
		IF iTimeLeft >= 0
			DRAW_GENERIC_TIMER(iTimeLeft, "GB_WORK_END",0, TIMER_STYLE_DONTUSEMILLISECONDS, 0, PODIUMPOS_NONE, HUDORDER_BOTTOM, DEFAULT, HUD_COLOUR_RED, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_RED)
		ELSE
			DRAW_GENERIC_TIMER(0, "GB_WORK_END",0, TIMER_STYLE_DONTUSEMILLISECONDS, 0, PODIUMPOS_NONE, HUDORDER_BOTTOM, DEFAULT, HUD_COLOUR_RED, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_RED)
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Lock the doors of the RHino for players not on the initiating gang
/// PARAMS:
///    iPart - Participant to lock the doors for
///    iVehicleIndex - serverBD.iveh[] array index
PROC MAINTAIN_LOCK_DOORS_FOR_RIVAL_PLAYERS(INT iPart, INT iVehicleIndex = 0)
	PLAYER_INDEX playerLock
	PLAYER_INDEX playerBoss
	BOOL bShouldLock
	IF GET_GB_ASSAULT_VARIATION() = GB_ASSAULT_ZANCUDO
		IF NOT IS_BIT_SET(serverBD.iServerLockedDoorsBitset, iPart)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[iVehicleIndex])
				IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[iVehicleIndex])
					playerLock = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart))
					IF NOT GB_IS_PLAYER_MEMBER_OF_A_GANG(playerLock)
						//-- Not in any gang - lock
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PLAYER_CHECKS_SERVER] [MAINTAIN_LOCK_DOORS_FOR_RIVAL_PLAYERS] SHOULD LOCK DOORS FOR PLAYER ", GET_PLAYER_NAME(playerLock), " AS NOT GB_IS_PLAYER_MEMBER_OF_A_GANG")
						bShouldLock = TRUE
					ELSE
						IF serverBD.iBossPlayerWhoLaunched > -1
							playerBoss = INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched)
							IF NETWORK_IS_PLAYER_ACTIVE(playerBoss)
								IF NOT GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerLock, playerBoss) 
									//-- Not in the same gang as the boss who launched - lock
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PLAYER_CHECKS_SERVER] [MAINTAIN_LOCK_DOORS_FOR_RIVAL_PLAYERS] SHOULD LOCK DOORS FOR PLAYER ", GET_PLAYER_NAME(playerLock), " BECAUSE THEY ARE NOT IN ", GET_PLAYER_NAME(playerBoss), "'s GANG")
									bShouldLock = TRUE
								ELSE	
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PLAYER_CHECKS_SERVER] [MAINTAIN_LOCK_DOORS_FOR_RIVAL_PLAYERS] NOT LOCKING DOORS FOR PLAYER ", GET_PLAYER_NAME(playerLock), " BECAUSE THEY ARE IN ", GET_PLAYER_NAME(playerBoss), "'s GANG")
									SET_BIT(serverBD.iServerLockedDoorsBitset, iPart)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF bShouldLock
						
						SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.niVeh[iVehicleIndex]), playerLock, TRUE)
						SET_BIT(serverBD.iServerLockedDoorsBitset, iPart)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PLAYER_CHECKS_SERVER] [MAINTAIN_LOCK_DOORS_FOR_RIVAL_PLAYERS] LOCKED ZANCUDO DOORS FOR PLAYER ", GET_PLAYER_NAME(playerLock))
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_MAX_PLAYER_CHECKS_CLIENT()
	INT i
	IF serverBD.iServerGameState != GAME_STATE_END
		REPEAT NUM_NETWORK_PLAYERS i
			PLAYER_INDEX PlayerId = INT_TO_PLAYERINDEX(i)
			IF NETWORK_IS_PLAYER_ACTIVE(PlayerId)
				IF PlayerId != PLAYER_ID()
					IF NETWORK_IS_PLAYER_A_PARTICIPANT(PlayerId)
						//-- For 2604085 - reset the custom sprite blip for all players who join the scripts
						IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClearPlayerBlipBitset, i)
							IF IS_THIS_A_STEAL_VEHICLE_VARIATION()
								SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(PlayerId, RADAR_TRACE_TANK, FALSE)
							ELSE
								SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(PlayerId, RADAR_TRACE_ASSAULT_PACKAGE, FALSE)
							ENDIF
							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClearPlayerBlipBitset, i)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PLAYER_CHECKS_CLIENT] CLEARED CUSTOM PLAYER BLIP FOR PLAYER ", GET_PLAYER_NAME(PlayerId))
						ENDIF
					ELSE
						IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClearPlayerBlipBitset, i)
							IF IS_THIS_A_STEAL_VEHICLE_VARIATION()
								SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(PlayerId, RADAR_TRACE_TANK, FALSE)
							ELSE
								SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(PlayerId, RADAR_TRACE_ASSAULT_PACKAGE, FALSE)
							ENDIF
							CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClearPlayerBlipBitset, i)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PLAYER_CHECKS_CLIENT] CLEARED CUSTOM PLAYER BLIP ", i, " AS NOT A PARTICIPANT")
						ENDIF
					ENDIF
					
				ENDIF
			ELSE
				IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClearPlayerBlipBitset, i)
					CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClearPlayerBlipBitset, i)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PLAYER_CHECKS_CLIENT] CLEARED CUSTOM PLAYER BLIP ", i, " AS NO LONGER ACTIVE")
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

PROC MAINTAIN_MAX_PLAYER_CHECKS_SERVER()
	
	INT iTempPartWithPackage = -1
	
	INT iTempPartInVeh = -1
	BOOL bSomeoneNotFinished
	INT i
	
	
	//For now we only need to do this check once the Veh is destroyed.
	IF serverBD.iServerGameState != GAME_STATE_END
		REPEAT NUM_NETWORK_PLAYERS i
			PLAYER_INDEX PlayerId = INT_TO_PLAYERINDEX(i)
			IF NETWORK_IS_PLAYER_ACTIVE(PlayerId)
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(PlayerId)
					iStaggeredParticipant =  NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(PlayerId))
		
					//** CHECKS THAT DON'T NEED THE PLAYER TO BE ALIVE **//
					
					
					//** CHECKS THAT REQUIRE THE PLAYER TO BE ALIVE **//
					IF IS_NET_PLAYER_OK(PlayerId)
						//-- Lock Rhino doors
						MAINTAIN_LOCK_DOORS_FOR_RIVAL_PLAYERS(iStaggeredParticipant)
						
					ENDIF
					
					//-- Start mission duration?
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_StartDuration)
					//	IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_DoneStartBigMessage)
							SET_BIT(serverBD.iServerBitSet, biS_StartDuration)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PLAYER_CHECKS_SERVER] SET biS_StartDuration AS SERVER THINKS THIS PLAYER DONE INTRO SHARD ", GET_PLAYER_NAME(PlayerId))
					//	ENDIF
					ENDIF
					
					//--Reached initial package location?
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneAtLoc)
						IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_GotToLocation)
							SET_BIT(serverBD.iServerBitSet, biS_SomeoneAtLoc)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PLAYER_CHECKS_SERVER] SET biS_SomeoneAtLoc AS SERVER THINKS THIS PLAYER AT START LOC ", GET_PLAYER_NAME(PlayerId))
						ENDIF
					ENDIF
					
					//-- Player approached AI targets?
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_TargetsApproached)
						IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_ApproachedTargets)
							SET_BIT(serverBD.iServerBitSet, biS_TargetsApproached)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PLAYER_CHECKS_SERVER] SET biS_TargetsApproached AS SERVER THINKS THIS PLAYER APPROACHED TARGETS ", GET_PLAYER_NAME(PlayerId))
						ENDIF
					ENDIF
					
					//-- Player shooting near the targets?
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ShootNearTarget)
						IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_ShotNearTargets)
							SET_BIT(serverBD.iServerBitSet, biS_ShootNearTarget)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PLAYER_CHECKS_SERVER] SET biS_ShootNearTarget AS SERVER THINKS THIS PLAYER SHOT NEAR TARGETS ", GET_PLAYER_NAME(PlayerId))
						ENDIF
					ENDIF
					
					//-- Player aimed at targets?
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AimingAtTargets)
						IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_AimAtTargets)
							SET_BIT(serverBD.iServerBitSet, biS_AimingAtTargets)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PLAYER_CHECKS_SERVER] SET biS_AimingAtTargets AS SERVER THINKS THIS PLAYER AIMED AT TARGETS ", GET_PLAYER_NAME(PlayerId))
						ENDIF
					ENDIF
					
					//-- Damaged Targets?
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_DamagedTargets)
						IF playerBD[iStaggeredParticipant].iDamagedAiPedBitset != 0
							SET_BIT(serverBD.iServerBitSet, biS_DamagedTargets)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PLAYER_CHECKS_SERVER] SET biS_DamagedTargets AS SERVER THINKS THIS PLAYER DAMAGED TARGETS ", GET_PLAYER_NAME(PlayerId))
						ENDIF
					ENDIF
					
					// Who has the package?
					IF iTempPartWithPackage = -1
						IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_GotThePackage)
							iTempPartWithPackage = iStaggeredParticipant
						ENDIF
					ENDIF
					
					//-- Package delivered?
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_RivalGotPackage)
						IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_PackageDelivered)
							IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_PackageDelivered)
								serverBD.iPlayerWinPackage = NATIVE_TO_INT(PlayerId)
								SET_BIT(serverBD.iServerBitSet, biS_PackageDelivered)
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PLAYER_CHECKS_SERVER] SET biS_PackageDelivered AS SERVER THINKS THIS PLAYER DELIVERED THE PACKAGE ", GET_PLAYER_NAME(PlayerId), " PLAYER INT = ", serverBD.iPlayerWinPackage)
							ENDIF
						ENDIF
					ENDIF
					
					
					//-- Who is driving the vehicle?
					IF iTempPartInVeh = -1
						IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_InVeh)
							iTempPartInVeh = iStaggeredParticipant
						ENDIF
					ENDIF
					
					//-- Vehicle delivered?
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_VehDelivered)
						IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_VehDelivered)
							serverBD.iPlayerWinPackage = NATIVE_TO_INT(PlayerId)
							SET_BIT(serverBD.iServerBitSet, biS_VehDelivered)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PLAYER_CHECKS_SERVER] SET biS_VehDelivered AS SERVER THINKS THIS PLAYER DELIVERED THE VEHICLE ", GET_PLAYER_NAME(PlayerId), " PLAYER INT = ", serverBD.iPlayerWinPackage)
						ENDIF
					ENDIF
					
					//-- Vehicle Destroyed by A Player?
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_PlayerDestroyedVeh)
						IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_DestroyedVeh)
							IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_JoinedAsRival)
								serverBD.iPlayerWinPackage = NATIVE_TO_INT(PlayerId)
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PLAYER_CHECKS_SERVER] SET biS_PlayerDestroyedVeh AS THIS RIVAL PLAYER SAYS THEY DESTROYED THE VEHICLE ", GET_PLAYER_NAME(PlayerId), " PLAYER INT = ", serverBD.iPlayerWinPackage)
							ELSE
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PLAYER_CHECKS_SERVER] SET biS_PlayerDestroyedVeh AS THIS NON-RIVAL PLAYER SAYS THEY DESTROYED THE VEHICLE ", GET_PLAYER_NAME(PlayerId))
							ENDIF
							
							SET_BIT(serverBD.iServerBitSet, biS_PlayerDestroyedVeh)
							
							
						ENDIF
					ENDIF
					
					//--Vehicle destroyed by someone other than a player?
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_NonPlayerDestroyedVeh)
						IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_DestroyedByNonPlayer)
							SET_BIT(serverBD.iServerBitSet, biS_NonPlayerDestroyedVeh)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PLAYER_CHECKS_SERVER] SET biS_NonPlayerDestroyedVeh AS THIS PLAYER THINKS THE VEHICLE WAS DESTROYED BY SOMEONE OTHER THAN A PLAYER ", GET_PLAYER_NAME(PlayerId))
						ENDIF
					ENDIF
					
					//-- Vehicle destroyed by a non-participant
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_NonPartDestRhino)
						IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_NonPartDestroyedRhino)
							SET_BIT(serverBD.iServerBitSet, biS_NonPartDestRhino)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PLAYER_CHECKS_SERVER] SET biS_NonPartDestRhino AS THIS PLAYER THINKS THE VEHICLE WAS DESTROYED BY A NON-PARTICIPANT ", GET_PLAYER_NAME(PlayerId))
						ENDIF
					ENDIF
					
					//-- Everyone finished?
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AllPlayersFinished)
						IF NOT bSomeoneNotFinished
							IF IS_BIT_SET(serverBD.iServerBitSet, biS_PackageDelivered)
							OR IS_BIT_SET(serverBD.iServerBitSet, biS_DurationExpired)
							OR IS_BIT_SET(serverBD.iServerBitSet, biS_RivalGotPackage)
							OR IS_BIT_SET(serverBD.iServerBitSet, biS_BossLaunchedQuit)
							OR IS_BIT_SET(serverBD.iServerBitSet, biS_VehDelivered)
							OR IS_BIT_SET(serverBD.iServerBitSet, biS_VehicleWrecked)
							OR IS_BIT_SET(serverBD.iServerBitSet, biS_PlayerDestroyedVeh)
							OR IS_BIT_SET(serverBD.iServerBitSet, biS_NonPlayerDestroyedVeh) 
							OR IS_BIT_SET(serverBD.iServerBitSet, biS_NonPartDestRhino)
								IF NOT IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_Finished)
									bSomeoneNotFinished = TRUE
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PLAYER_CHECKS_SERVER] SERVER THINKS THIS PLAYER NOT FINISHED ", GET_PLAYER_NAME(PlayerId))
								ENDIF
							ELSE
								bSomeoneNotFinished = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					
					#IF IS_DEBUG_BUILD
					//-- Debug s-skip
					IF NOT ServerBD.bSomeoneSPassed
						IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPDebugBitset, biPDebug_PressedS)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PLAYER_CHECKS_SERVER] SERVER THINKS THIS PLAYER S-PASSED ", GET_PLAYER_NAME(PlayerId))
							ServerBD.bSomeoneSPassed = TRUE
						ENDIF
					ENDIF
					#ENDIF
				ELSE
//					//-- Part not active
//					
					//-- CHeck for Boss who launched the job quiting the session
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_BossLaunchedQuit)
						IF serverBD.iBossPlayerWhoLaunched > -1
							IF i = serverBD.iBossPlayerWhoLaunched
								SET_BIT(serverBD.iServerBitSet, biS_BossLaunchedQuit)
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PLAYER_CHECKS_SERVER] SERVER THINKS BOSS WHO LAUNCHED WORK QUIT SCRIPT (1) serverBD.iBossPartWhoLaunched = ", serverBD.iBossPartWhoLaunched)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE	
				//-- CHeck for Boss who launched the job quiting the session
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_BossLaunchedQuit)
					IF serverBD.iBossPlayerWhoLaunched > -1
						IF i = serverBD.iBossPlayerWhoLaunched
							SET_BIT(serverBD.iServerBitSet, biS_BossLaunchedQuit)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PLAYER_CHECKS_SERVER] SERVER THINKS BOSS WHO LAUNCHED WORK QUIT SESSION (2) serverBD.iBossPartWhoLaunched = ", serverBD.iBossPartWhoLaunched)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		
		//-- Rhino wrecked by someone other than a participant?
//		IF IS_THIS_A_STEAL_VEHICLE_VARIATION()
//			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])
//				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_PlayerDestroyedVeh)
//					IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.niveh[0])
//						IF NOT HAS_NET_TIMER_STARTED(serverBD.timeWrecked)	
//							START_NET_TIMER(serverBD.timeWrecked)
//							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PLAYER_CHECKS_SERVER] STARTED timeWrecked")
//						ELSE
//							IF HAS_NET_TIMER_EXPIRED(serverBD.timeWrecked)
//								SET_BIT(serverBD.iServerBitSet, biS_VehicleWrecked)
//								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PLAYER_CHECKS_SERVER] Set biS_VehicleWrecked")
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
							 
		//-- Update who has the package
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_PackageDelivered)
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_RivalGotPackage)
				IF serverBD.iPartPackageHolder != iTempPartWithPackage
					serverBD.iPartPackageHolder = iTempPartWithPackage
					IF serverBD.iPartPackageHolder = -1
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PLAYER_CHECKS_SERVER] serverBD.iPartPackageHolder UPDATED TO ", serverBD.iPartPackageHolder)
					ELSE
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PLAYER_CHECKS_SERVER] serverBD.iPartPackageHolder UPDATED TO ", serverBD.iPartPackageHolder, " WHO IS PLAYER ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.iPartPackageHolder))))
						IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_CollectedAtLeastOnce)
							SET_BIT(serverBD.iServerBitSet, biS_CollectedAtLeastOnce)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PLAYER_CHECKS_SERVER] SET biS_CollectedAtLeastOnce - package")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//-- Update who is driving the vehicle
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_VehDelivered)

			IF serverBD.iPartInVeh != iTempPartInVeh
				serverBD.iPartInVeh = iTempPartInVeh
				IF serverBD.iPartInVeh = -1
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PLAYER_CHECKS_SERVER] serverBD.iPartInVeh UPDATED TO ", serverBD.iPartInVeh)
				ELSE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PLAYER_CHECKS_SERVER] serverBD.iPartInVeh UPDATED TO ", serverBD.iPartInVeh, " WHO IS PLAYER ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.iPartInVeh))))
					
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_CollectedAtLeastOnce)
						SET_BIT(serverBD.iServerBitSet, biS_CollectedAtLeastOnce)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PLAYER_CHECKS_SERVER SET biS_CollectedAtLeastOnce - vehicle")
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
		

		
		//--All finished?
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AllPlayersFinished)
			IF NOT bSomeoneNotFinished
				SET_BIT(serverBD.iServerBitSet, biS_AllPlayersFinished)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PLAYER_CHECKS_SERVER] SET biS_AllPlayersFinished")
			ENDIF
		ENDIF
	ENDIF

ENDPROC

//PURPOSE: Runs through the max num participents and does relevant checks
PROC MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
	INT iTempPartWithPackage = -1
	
	INT iTempPartInVeh = -1
	BOOL bSomeoneNotFinished
	
	
	
	//For now we only need to do this check once the Veh is destroyed.
	IF serverBD.iServerGameState != GAME_STATE_END
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iStaggeredParticipant
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iStaggeredParticipant))
				PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iStaggeredParticipant))
				//PED_INDEX PlayerPedId = GET_PLAYER_PED(PlayerId)
				
				//** CHECKS THAT DON'T NEED THE PLAYER TO BE ALIVE **//
				
				
				//** CHECKS THAT REQUIRE THE PLAYER TO BE ALIVE **//
				IF IS_NET_PLAYER_OK(PlayerId)
					
					
				ENDIF
				
				//-- Start mission duration?
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_StartDuration)
				//	IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_DoneStartBigMessage)
						SET_BIT(serverBD.iServerBitSet, biS_StartDuration)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SET biS_StartDuration AS SERVER THINKS THIS PLAYER DONE INTRO SHARD ", GET_PLAYER_NAME(PlayerId))
				//	ENDIF
				ENDIF
				
				//--Reached initial package location?
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneAtLoc)
					IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_GotToLocation)
						SET_BIT(serverBD.iServerBitSet, biS_SomeoneAtLoc)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SET biS_SomeoneAtLoc AS SERVER THINKS THIS PLAYER AT START LOC ", GET_PLAYER_NAME(PlayerId))
					ENDIF
				ENDIF
				
				//-- Player approached AI targets?
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_TargetsApproached)
					IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_ApproachedTargets)
						SET_BIT(serverBD.iServerBitSet, biS_TargetsApproached)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SET biS_TargetsApproached AS SERVER THINKS THIS PLAYER APPROACHED TARGETS ", GET_PLAYER_NAME(PlayerId))
					ENDIF
				ENDIF
				
				//-- Player shooting near the targets?
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ShootNearTarget)
					IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_ShotNearTargets)
						SET_BIT(serverBD.iServerBitSet, biS_ShootNearTarget)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SET biS_ShootNearTarget AS SERVER THINKS THIS PLAYER SHOT NEAR TARGETS ", GET_PLAYER_NAME(PlayerId))
					ENDIF
				ENDIF
				
				//-- Player aimed at targets?
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AimingAtTargets)
					IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_AimAtTargets)
						SET_BIT(serverBD.iServerBitSet, biS_AimingAtTargets)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SET biS_AimingAtTargets AS SERVER THINKS THIS PLAYER AIMED AT TARGETS ", GET_PLAYER_NAME(PlayerId))
					ENDIF
				ENDIF
				
				//-- Damaged Targets?
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_DamagedTargets)
					IF playerBD[iStaggeredParticipant].iDamagedAiPedBitset != 0
						SET_BIT(serverBD.iServerBitSet, biS_DamagedTargets)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SET biS_DamagedTargets AS SERVER THINKS THIS PLAYER DAMAGED TARGETS ", GET_PLAYER_NAME(PlayerId))
					ENDIF
				ENDIF
				
				// Who has the package?
				IF iTempPartWithPackage = -1
					IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_GotThePackage)
						iTempPartWithPackage = iStaggeredParticipant
					ENDIF
				ENDIF
				
				//-- Package delivered?
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_RivalGotPackage)
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_PackageDelivered)
						IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_PackageDelivered)
							serverBD.iPlayerWinPackage = NATIVE_TO_INT(PlayerId)
							SET_BIT(serverBD.iServerBitSet, biS_PackageDelivered)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SET biS_PackageDelivered AS SERVER THINKS THIS PLAYER DELIVERED THE PACKAGE ", GET_PLAYER_NAME(PlayerId), " PLAYER INT = ", serverBD.iPlayerWinPackage)
						ENDIF
					ENDIF
				ENDIF
				
				
				//-- Who is driving the vehicle?
				IF iTempPartInVeh = -1
					IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_InVeh)
						iTempPartInVeh = iStaggeredParticipant
					ENDIF
				ENDIF
				
				//-- Vehicle delivered?
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_VehDelivered)
					IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_VehDelivered)
						serverBD.iPlayerWinPackage = NATIVE_TO_INT(PlayerId)
						SET_BIT(serverBD.iServerBitSet, biS_VehDelivered)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SET biS_VehDelivered AS SERVER THINKS THIS PLAYER DELIVERED THE VEHICLE ", GET_PLAYER_NAME(PlayerId), " PLAYER INT = ", serverBD.iPlayerWinPackage)
					ENDIF
				ENDIF
				
				//-- Vehicle Destroyed by A Player?
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_PlayerDestroyedVeh)
					IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_DestroyedVeh)
						IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_JoinedAsRival)
							serverBD.iPlayerWinPackage = NATIVE_TO_INT(PlayerId)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SET biS_PlayerDestroyedVeh AS THIS RIVAL PLAYER SAYS THEY DESTROYED THE VEHICLE ", GET_PLAYER_NAME(PlayerId), " PLAYER INT = ", serverBD.iPlayerWinPackage)
						ELSE
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SET biS_PlayerDestroyedVeh AS THIS NON-RIVAL PLAYER SAYS THEY DESTROYED THE VEHICLE ", GET_PLAYER_NAME(PlayerId))
						ENDIF
						
						SET_BIT(serverBD.iServerBitSet, biS_PlayerDestroyedVeh)
						
						
					ENDIF
				ENDIF
				

				
			

				//-- Everyone finished?
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AllPlayersFinished)
					IF NOT bSomeoneNotFinished
						IF IS_BIT_SET(serverBD.iServerBitSet, biS_PackageDelivered)
						OR IS_BIT_SET(serverBD.iServerBitSet, biS_DurationExpired)
						OR IS_BIT_SET(serverBD.iServerBitSet, biS_RivalGotPackage)
						OR IS_BIT_SET(serverBD.iServerBitSet, biS_BossLaunchedQuit)
						OR IS_BIT_SET(serverBD.iServerBitSet, biS_VehDelivered)
						OR IS_BIT_SET(serverBD.iServerBitSet, biS_VehicleWrecked)
						OR IS_BIT_SET(serverBD.iServerBitSet, biS_PlayerDestroyedVeh)
							IF NOT IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_Finished)
								bSomeoneNotFinished = TRUE
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SERVER THINKS THIS PLAYER NOT FINISHED ", GET_PLAYER_NAME(PlayerId))
							ENDIF
						ELSE
							bSomeoneNotFinished = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				
				#IF IS_DEBUG_BUILD
				//-- Debug s-skip
				IF NOT ServerBD.bSomeoneSPassed
					IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPDebugBitset, biPDebug_PressedS)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SERVER THINKS THIS PLAYER S-PASSED ", GET_PLAYER_NAME(PlayerId))
						ServerBD.bSomeoneSPassed = TRUE
					ENDIF
				ENDIF
				#ENDIF
			ELSE
				//-- Part not active
				
				//-- CHeck for Boss who launched the job quiting the session
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_BossLaunchedQuit)
					IF serverBD.iBossPartWhoLaunched > -1
						IF iStaggeredParticipant = serverBD.iBossPartWhoLaunched
							SET_BIT(serverBD.iServerBitSet, biS_BossLaunchedQuit)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SERVER THINKS BOSS WHO LAUNCHED WORK QUIT SESSION erverBD.iBossPartWhoLaunched = ", serverBD.iBossPartWhoLaunched)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		//-- Update who has the package
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_PackageDelivered)
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_RivalGotPackage)
				IF serverBD.iPartPackageHolder != iTempPartWithPackage
					serverBD.iPartPackageHolder = iTempPartWithPackage
					IF serverBD.iPartPackageHolder = -1
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] serverBD.iPartPackageHolder UPDATED TO ", serverBD.iPartPackageHolder)
					ELSE
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] serverBD.iPartPackageHolder UPDATED TO ", serverBD.iPartPackageHolder, " WHO IS PLAYER ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.iPartPackageHolder))))
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//-- Update who is driving the vehicle
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_VehDelivered)

			IF serverBD.iPartInVeh != iTempPartInVeh
				serverBD.iPartInVeh = iTempPartInVeh
				IF serverBD.iPartInVeh = -1
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] serverBD.iPartInVeh UPDATED TO ", serverBD.iPartInVeh)
				ELSE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] serverBD.iPartInVeh UPDATED TO ", serverBD.iPartInVeh, " WHO IS PLAYER ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.iPartInVeh))))
				ENDIF
			ENDIF
			
		ENDIF
		

		
		//--All finished?
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AllPlayersFinished)
			IF NOT bSomeoneNotFinished
				SET_BIT(serverBD.iServerBitSet, biS_AllPlayersFinished)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SET biS_AllPlayersFinished")
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC DISABLE_PLAYER_MISSION_VEHICLE_CONTROLS_THIS_FRAME(BOOL bDisableExit = TRUE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HANDBRAKE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_UD)
	IF bDisableExit
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
	ENDIF
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ROLL_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_PITCH_UD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_YAW_LEFT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_YAW_LEFT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_UP)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_DOWN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_UNDERCARRIAGE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_NEXT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_PREV_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_TARGET_LEFT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_TARGET_RIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_VERTICAL_FLIGHT_MODE)
ENDPROC

PROC STOP_PLAYER_LEAVING_VEHICLE_THIS_FRAME()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
	ENDIF
ENDPROC

/// PURPOSE:
///    Store the text label for the end shard based on variation and whether or not I won / lost
PROC GET_REWARD_TEXT_LABELS(TEXT_LABEL_15 &tlIWonBoss, 		// I completed the objective
							TEXT_LABEL_15 &tlIWonGang, 		// Someone on my gang completed the objective
							TEXT_LABEL_15 &tlILostGang, 	// My gang failed to complete the objective
							TEXT_LABEL_15 &tlIWonNonGang, 	// I won, and I'm not in a gang
							TEXT_LABEL_15 &tlILostNonGang,	// I lost, and I'm not in a gang
							TEXT_LABEL_15 &tlINobodyDel)	// Nobody completed the objective (time expired)
							
	SWITCH GET_GB_ASSAULT_VARIATION()
		CASE GB_ASSAULT_ALTRUIST
		CASE GB_ASSAULT_AIRPORT
		CASE GB_ASSAULT_MERRYWEATHER
			tlIWonBoss = "BIGM_ASLTYD" // You delivered the package
			tlIWonGang = "BIGM_ASLTWD" //~a~ delivered the package
			tlILostGang = "BIGM_ASLTWRF"
			tlIWonNonGang = "BIGM_ASLTYD" // You delivered the package
			tlILostNonGang = "BIGM_ASLTWRF2"
			tlINobodyDel = "BIGM_ASLTFN" // Nobody delivereed the package
		BREAK
		
		CASE GB_ASSAULT_ZANCUDO
			tlIWonBoss = "BIGM_ASLTYV" // You delivered the RHINO
			tlIWonGang = "BIGM_ASLTWV" //~a~ delivered the RHINO
			tlILostGang = "BIGM_ASLTWFV"
			tlIWonNonGang = "BIGM_ASLTYV" // You delivered the RHINO
			tlILostNonGang = "BIGM_ASLTWFV2"
			tlINobodyDel = "BIGM_ASLTFN2" // Nobody delivereed the package
		BREAK
	ENDSWITCH
ENDPROC




//PURPOSE: Checks to see if this is the end and the player should be rewarded.
PROC CHECK_FOR_REWARD()
//	BOOL bEventCompleted
	PLAYER_INDEX playerPackage = INVALID_PLAYER_INDEX()
	PLAYER_INDEX playerBoss
	GANG_BOSS_MANAGE_REWARDS_DATA gbRewards
	
	TEXT_LABEL_15 tlIWonBoss 
	TEXT_LABEL_15 tlIWonGang 
	TEXT_LABEL_15 tlILostGang 
	TEXT_LABEL_15 tlIWonNonGang
	TEXT_LABEL_15 tlILostNonGang
	TEXT_LABEL_15 tlINobodyDel
	
	STRING sOrganization
	HUD_COLOURS hclPlayer
	
	
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Finished)
		IF NOT IS_BIT_SET(iBoolsBitSet2, biL2_WaitForShard)
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_PackageDelivered) 
			OR IS_BIT_SET(serverBD.iServerBitSet, biS_VehDelivered)
				GET_REWARD_TEXT_LABELS(tlIWonBoss, tlIWonGang, tlILostGang, tlIWonNonGang, tlILostNonGang, tlINobodyDel)
				
				//-- Package was delivered
				IF SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD()
					playerPackage = INT_TO_PLAYERINDEX(serverBD.iPlayerWinPackage)
					IF playerPackage = PLAYER_ID()
						//-- I delivered the package
						IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) > GB_UI_LEVEL_BACKGROUND
							SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "GB_WINNER", tlIWonBoss) // You delivered the package
						ENDIF
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] biS_PackageDelivered AND I DELIVERED THE PACKAGE playerPackage = ", GET_PLAYER_NAME(playerPackage))
						
						IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneTelemetry)
							SET_BIT(iBoolsBitSet, biL_DoneTelemetry)
							GB_SET_COMMON_TELEMETRY_DATA_ON_END(TRUE, GB_TELEMETRY_END_WON)
						ENDIF
						
						GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_ASSAULT, TRUE, gbRewards)
						CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
					ELSE
						IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
							playerBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
							IF (playerBoss != INVALID_PLAYER_INDEX()
							AND GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerPackage, playerBoss ))
								//-- My gang delivered package
								IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) > GB_UI_LEVEL_BACKGROUND
									sOrganization = GB_GET_ORGANIZATION_NAME_AS_A_STRING()
									hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID())
									SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "GB_WINNER", tlIWonGang, sOrganization, hclPlayer) 
								//	SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "GB_WINNER", tlIWonGang) 
								ENDIF
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] biS_PackageDelivered AND MY GANG DELIVERED THE PACKAGE NEW SHARD playerPackage = ", GET_PLAYER_NAME(playerPackage))
								
								IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneTelemetry)
									SET_BIT(iBoolsBitSet, biL_DoneTelemetry)
									GB_SET_COMMON_TELEMETRY_DATA_ON_END(TRUE, GB_TELEMETRY_END_WON)
								ENDIF
								IF GET_MY_BOSS_PARTICPANT_ID() = serverBD.iBossPartWhoLaunched
									GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_ASSAULT, TRUE, gbRewards)
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] biS_PackageDelivered - FULL REWARD AS MY GANG LAUNCHED")
								ELSE
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] biS_PackageDelivered - NO REWARD AS MY GANG DIDN't LAUNCH")
									GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_ASSAULT, FALSE, gbRewards)
								ENDIF
								CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
							ELSE
								//-- My gang didn't deliver the package / Rhino
								IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) > GB_UI_LEVEL_BACKGROUND
									//sOrganization = GB_GET_ORGANIZATION_NAME_AS_A_STRING()
									//hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID())
									IF GB_IS_PLAYER_MEMBER_OF_A_GANG(playerPackage) 
										sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(playerPackage)
										hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(playerPackage)
									ELSE
										sOrganization = GET_PLAYER_NAME(playerPackage)
										hclPlayer = HUD_COLOUR_WHITE
									ENDIF
									
									IF GET_GB_ASSAULT_VARIATION() != GB_ASSAULT_ZANCUDO
										 
										SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", tlILostGang, sOrganization, hclPlayer)

									ELSE
										IF IS_BIT_SET(serverBD.iServerBitSet, biS_VehDelivered)
											SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_ASL_DESFG", sOrganization, hclPlayer)
										ENDIF
									ENDIF
									
								ENDIF
								
								GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_ASSAULT, FALSE, gbRewards)
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] biS_PackageDelivered AND MY GANG FAILED TO DELIVER THE PACKAGE")
								
								IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneTelemetry)
									SET_BIT(iBoolsBitSet, biL_DoneTelemetry)
									GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST)
								ENDIF
								
								GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_ASSAULT, FALSE, gbRewards)
							ENDIF
						ELSE
							//-- Not a gang member
	//						IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) > GB_UI_LEVEL_BACKGROUND
	//							IF GET_GB_ASSAULT_VARIATION() != GB_ASSAULT_ZANCUDO
	//								SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", tlILostNonGang) //You failed to retrieve the package
	//							ELSE
	//								SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_ASL_DESF")
	//							ENDIF
	//						ENDIF
							IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) > GB_UI_LEVEL_BACKGROUND
								//sOrganization = GB_GET_ORGANIZATION_NAME_AS_A_STRING()
								//hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID())
								IF GB_IS_PLAYER_MEMBER_OF_A_GANG(playerPackage) 
									sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(playerPackage)
									hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(playerPackage)
								ELSE
									sOrganization = GET_PLAYER_NAME(playerPackage)
									hclPlayer = HUD_COLOUR_WHITE
								ENDIF
								
								IF GET_GB_ASSAULT_VARIATION() != GB_ASSAULT_ZANCUDO
									 
									SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", tlILostGang, sOrganization, hclPlayer)

								ELSE
									IF IS_BIT_SET(serverBD.iServerBitSet, biS_VehDelivered)
										SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_ASL_DESFG", sOrganization, hclPlayer)
									ENDIF
								ENDIF
								
							ENDIF
								
							IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneTelemetry)
								SET_BIT(iBoolsBitSet, biL_DoneTelemetry)
								GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST)
							ENDIF
							GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_ASSAULT, FALSE, gbRewards)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] biS_PackageDelivered AND I'M NOT IN A GANG")
						
						ENDIF
					ENDIF
				ENDIF
								
			//	bEventCompleted = TRUE
			//	SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Finished)
				SET_BIT(iBoolsBitSet2, biL2_WaitForShard)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] SET biL2_WaitForShard AS biS_PackageDelivered")
			ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_PlayerDestroyedVeh)
				//-- Rival destroyed the vehicle

				IF serverBD.iPlayerWinPackage > -1
					playerPackage = INT_TO_PLAYERINDEX(serverBD.iPlayerWinPackage)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] biS_PlayerDestroyedVeh IS SET serverBD.iPlayerWinPackage = ", serverBD.iPlayerWinPackage, " WHO IS PLAYER ", GET_PLAYER_NAME(playerPackage))
				ELSE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] biS_PlayerDestroyedVeh IS SET BUT serverBD.iPlayerWinPackage = ", serverBD.iPlayerWinPackage)
				ENDIF
				
				IF SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD()
					IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
						//-- I was on the gang that launched the boss work so I definitely didn't win
						IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) > GB_UI_LEVEL_BACKGROUND
							IF NETWORK_IS_PLAYER_ACTIVE(playerPackage)
								IF GB_IS_PLAYER_MEMBER_OF_A_GANG(playerPackage)
									sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(playerPackage)
									hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(playerPackage)
								ELSE
									sOrganization = GET_PLAYER_NAME(playerPackage)
									hclPlayer = HUD_COLOUR_WHITE
								ENDIF
								SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_ASL_DESFG2", sOrganization, hclPlayer)
								
							ELSE
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] biS_PlayerDestroyedVeh IS SET MY GANG LAUNCHED BUT DON'T KNOW WHO DESTRPYED THE TANK!")
								SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_ASL_DESFL") //The Rhino was destroyed
							ENDIF
						ENDIF
						
						IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
						OR DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
							IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneTelemetry)
								SET_BIT(iBoolsBitSet, biL_DoneTelemetry)
								GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST)
							ENDIF
						ENDIF
						
						GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_ASSAULT, FALSE, gbRewards)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] biS_PlayerDestroyedVeh IS SET MY GANG LAUNCHED BUT RIVALS GOT PACKAGE")
					ELSE
						IF serverBD.iPlayerWinPackage > -1
							IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
								playerBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
								IF ( playerBoss != INVALID_PLAYER_INDEX() 
								AND GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerPackage, playerBoss ))
									//-- The player who destroyed the vehicle was on my gang
									
									IF playerPackage =  PLAYER_ID()
										IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) > GB_UI_LEVEL_BACKGROUND
											SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "GB_WINNER", "BIGM_ASL_DESY") // You destroyed the Rhino
										ENDIF
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] biS_PlayerDestroyedVeh SET AND I RETRIEVED THE PACKAGE")
									ELSE
										IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) > GB_UI_LEVEL_BACKGROUND
										//	SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "GB_WINNER", "BIGM_ASL_DESG") // Your Organization destroyed the Rhino
											hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID())
											sOrganization = GB_GET_ORGANIZATION_NAME_AS_A_STRING()
	 
											SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "GB_WINNER", "BIGM_ASL_DESG", sOrganization, hclPlayer)
										ENDIF
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] biS_PlayerDestroyedVeh SET AND PLAYER ", GET_PLAYER_NAME(playerPackage), " IS IN MY GANG")
									ENDIF
									
									IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
									OR DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
										IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneTelemetry)
											SET_BIT(iBoolsBitSet, biL_DoneTelemetry)
											GB_SET_COMMON_TELEMETRY_DATA_ON_END(TRUE, GB_TELEMETRY_END_WON)
										ENDIF
									ENDIF
									
									IF serverBD.iPlayerWinPackage = NATIVE_TO_INT(PLAYER_ID())
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] biS_PlayerDestroyedVeh FULL REWARD AS I DESTROYED RHINO")
										GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_ASSAULT, TRUE, gbRewards)
									ELSE
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] biS_PlayerDestroyedVeh NO REWARD AS MY GANG DESTROYED RHINO BUT IT WASN'T ME WHO DESTROYED IT")
										GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_ASSAULT, TRUE, gbRewards)
									ENDIF
								ELSE
									//-- The player who destroyed the vehicle wasn't on my gang
									IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) > GB_UI_LEVEL_BACKGROUND
									
									//	sOrganization = GB_GET_ORGANIZATION_NAME_AS_A_STRING()
									//	hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID())
										IF GB_IS_PLAYER_MEMBER_OF_A_GANG(playerPackage)
											sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(playerPackage)
											hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(playerPackage)
										ELSE
											sOrganization = GET_PLAYER_NAME(playerPackage)
											hclPlayer = HUD_COLOUR_WHITE
										ENDIF
										
										SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_ASL_DESFG2", sOrganization, hclPlayer)
									ENDIF
									
									IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
									OR DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
										IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneTelemetry)
											SET_BIT(iBoolsBitSet, biL_DoneTelemetry)
											GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST)
										ENDIF
									ENDIF
									
									GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_ASSAULT, FALSE, gbRewards)
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] biS_PlayerDestroyedVeh IS SET BUT PLAYER ", GET_PLAYER_NAME(playerPackage), " IS NOT IN MY GANG")
								ENDIF
							ELSE
								//-- I'm not in any gang
								IF playerPackage =  PLAYER_ID()
									IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) > GB_UI_LEVEL_BACKGROUND
										SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "GB_WINNER", "BIGM_ASL_DESY")
									ENDIF
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] biS_PlayerDestroyedVeh SET AND I'M NOT IN A GANG AND I RETRIEVED THE PACKAGE")
									
									IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
									OR DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
										IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneTelemetry)
											SET_BIT(iBoolsBitSet, biL_DoneTelemetry)
											GB_SET_COMMON_TELEMETRY_DATA_ON_END(TRUE, GB_TELEMETRY_END_WON)
										ENDIF
									ENDIF
									
									GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_ASSAULT, TRUE, gbRewards)
								ELSE
									IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) > GB_UI_LEVEL_BACKGROUND
								//		SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_ASL_DESF") // You failed to destroy the Rhino
										
										IF GB_IS_PLAYER_MEMBER_OF_A_GANG(playerPackage)
											sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(playerPackage)
											hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(playerPackage)
										ELSE
											sOrganization = GET_PLAYER_NAME(playerPackage)
											hclPlayer = HUD_COLOUR_WHITE
										ENDIF
										
										SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_ASL_DESFG2", sOrganization, hclPlayer)
									
									ENDIF
									
									IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
									OR DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
										IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneTelemetry)
											SET_BIT(iBoolsBitSet, biL_DoneTelemetry)
											GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST)
										ENDIF
									ENDIF
									
									GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_ASSAULT, FALSE, gbRewards)
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] biS_PlayerDestroyedVeh IS SET BUT I'M NOT IN ANY GANG")
								ENDIF
								
							ENDIF
						ELSE
							//-- No winner
							IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
								IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) > GB_UI_LEVEL_BACKGROUND
								//	SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_ASL_DESFG") // Your organization failed to destroy the Rhino
									sOrganization = GB_GET_ORGANIZATION_NAME_AS_A_STRING()
									hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID()) 
									SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_ASL_DESMG", sOrganization, hclPlayer)	
								ENDIF
								
								IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
								OR DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
									IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneTelemetry)
										SET_BIT(iBoolsBitSet, biL_DoneTelemetry)
										GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST)
									ENDIF
								ENDIF
								
								GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_ASSAULT, FALSE, gbRewards)
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] I'm IN A GANG biS_PlayerDestroyedVeh IS SET BUT serverBD.iPlayerWinPackage = ", serverBD.iPlayerWinPackage)
							ELSE
								IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) > GB_UI_LEVEL_BACKGROUND
									SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_ASL_DESF") // You failed to destroy the Rhino
								ENDIF
								
								IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
								OR DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
									IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneTelemetry)
										SET_BIT(iBoolsBitSet, biL_DoneTelemetry)
										GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST)
									ENDIF
								ENDIF
								
								GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_ASSAULT, FALSE, gbRewards)
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] I'm NOT IN A GANG biS_PlayerDestroyedVeh IS SET BUT serverBD.iPlayerWinPackage = ", serverBD.iPlayerWinPackage)
								
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			//	SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Finished)
				
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] SET biL2_WaitForShard AS biS_PlayerDestroyedVeh")
				SET_BIT(iBoolsBitSet2, biL2_WaitForShard)
			//	bEventCompleted = TRUE
			ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_NonPlayerDestroyedVeh)
				//-- Vehicle was destroyed by someone other than a player
				
				IF SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD()
					IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
						//-- I was on the gang that launched the boss work so I definitely didn't win
						IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) > GB_UI_LEVEL_BACKGROUND
							SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_ASL_DESFL") //The Rhino was destroyed
						ENDIF
						
						IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
						OR DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
							IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneTelemetry)
								SET_BIT(iBoolsBitSet, biL_DoneTelemetry)
								GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST)
							ENDIF
						ENDIF
								
						GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_ASSAULT, FALSE, gbRewards)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] biS_NonPlayerDestroyedVeh IS SET MY GANG LAUNCHED BUT TANK DESTROYED")
					ELSE
						//-- No winner
						IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
							IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) > GB_UI_LEVEL_BACKGROUND
							//	SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_ASL_DESFG") // Your organization failed to destroy the Rhino
								sOrganization = GB_GET_ORGANIZATION_NAME_AS_A_STRING()
								hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID())  
								SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_ASL_DESMG", sOrganization, hclPlayer)			
							ENDIF
							
							IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
							OR DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
								IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneTelemetry)
									SET_BIT(iBoolsBitSet, biL_DoneTelemetry)
									GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST)
								ENDIF
							ENDIF
							GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_ASSAULT, FALSE, gbRewards)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] I'm IN A GANG biS_PlayerDestroyedVeh IS SET BUT serverBD.iPlayerWinPackage = ", serverBD.iPlayerWinPackage)
						ELSE
							IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) > GB_UI_LEVEL_BACKGROUND
								SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_ASL_DESF") // You failed to destroy the Rhino
							ENDIF
							
							IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
							OR DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
								IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneTelemetry)
									SET_BIT(iBoolsBitSet, biL_DoneTelemetry)
									GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST)
								ENDIF
							ENDIF
							GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_ASSAULT, FALSE, gbRewards)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] I'm NOT IN A GANG biS_PlayerDestroyedVeh IS SET BUT serverBD.iPlayerWinPackage = ", serverBD.iPlayerWinPackage)
							
						ENDIF
					ENDIF
				ENDIF
					
			//	SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Finished)
				SET_BIT(iBoolsBitSet2, biL2_WaitForShard)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] SET biL2_WaitForShard AS biS_NonPlayerDestroyedVeh")
			//	bEventCompleted = TRUE
			ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_NonPartDestRhino)
				//-- Vehicle was destroyed by a non-participant
				
				IF SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD()
					IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
						//-- I was on the gang that launched the boss work so I definitely didn't win
						IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) > GB_UI_LEVEL_BACKGROUND
							SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_ASL_DESFL") //The Rhino was destroyed
						ENDIF
						
						IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
						OR DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
							IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneTelemetry)
								SET_BIT(iBoolsBitSet, biL_DoneTelemetry)
								GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST)
							ENDIF
						ENDIF
								
						GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_ASSAULT, FALSE, gbRewards)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] biS_NonPartDestRhino IS SET MY GANG LAUNCHED BUT TANK DESTROYED")
					ELSE
						//-- No winner
						IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
							IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) > GB_UI_LEVEL_BACKGROUND
							//	SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_ASL_DESFG") // Your organization failed to destroy the Rhino
								sOrganization = GB_GET_ORGANIZATION_NAME_AS_A_STRING()
								hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID())  
								SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_ASL_DESMG", sOrganization, hclPlayer)			
							ENDIF
							
							IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
							OR DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
								IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneTelemetry)
									SET_BIT(iBoolsBitSet, biL_DoneTelemetry)
									GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST)
								ENDIF
							ENDIF
							
							GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_ASSAULT, FALSE, gbRewards)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] I'm IN A GANG biS_NonPartDestRhino IS SET BUT serverBD.iPlayerWinPackage = ", serverBD.iPlayerWinPackage)
						ELSE
							IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) > GB_UI_LEVEL_BACKGROUND
								SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_ASL_DESF") // You failed to destroy the Rhino
							ENDIF
							
							IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
							OR DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
								IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneTelemetry)
									SET_BIT(iBoolsBitSet, biL_DoneTelemetry)
									GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST)
								ENDIF
							ENDIF
							
							GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_ASSAULT, FALSE, gbRewards)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] I'm NOT IN A GANG biS_NonPartDestRhino IS SET BUT serverBD.iPlayerWinPackage = ", serverBD.iPlayerWinPackage)
							
						ENDIF
					ENDIF
				ENDIF
					
			//	SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Finished)
				SET_BIT(iBoolsBitSet2, biL2_WaitForShard)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] SET biL2_WaitForShard AS biS_NonPlayerDestroyedVeh")
			//	bEventCompleted = TRUE
			ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_DurationExpired)
				//-- Event duration expired
				
				GET_REWARD_TEXT_LABELS(tlIWonBoss, tlIWonGang, tlILostGang, tlIWonNonGang, tlILostNonGang, tlINobodyDel)
				
				IF SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD()
					IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
						IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) > GB_UI_LEVEL_BACKGROUND
							SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", tlINobodyDel) // Nobody delivered the briefcase
						ENDIF
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] biS_DurationExpired AND MY GANG FAILED TO DELIVER THE PACKAGE")
					ELSE
						IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) > GB_UI_LEVEL_BACKGROUND
							SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER",tlINobodyDel) // Nobody delivered the briefcase
						ENDIF
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] biS_DurationExpired AND I'M NOT IN A GANG")
					ENDIF
				ELSE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] biS_DurationExpired BUT NOT SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD")
				ENDIF
				
				IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
				OR DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
					IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneTelemetry)
						SET_BIT(iBoolsBitSet, biL_DoneTelemetry)
						GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_TIME_OUT)
					ENDIF
				ENDIF
				
				GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_ASSAULT, FALSE, gbRewards)
				SET_BIT(iBoolsBitSet2, biL2_WaitForShard)
			//	bEventCompleted = TRUE
			ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_BossLaunchedQuit)
				//-- Boss who launched quit
				
				IF SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD()
					IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
						IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) > GB_UI_LEVEL_BACKGROUND
						//	SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_ASLTBQ") // Your Boss quit the session
						ENDIF
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] biS_BossLaunchedQuit AND MY BOSS QUIT")
					ELSE
						IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) > GB_UI_LEVEL_BACKGROUND
							SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_ASLTRBQ") // The rival Boss quit the session
						ENDIF
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] biS_BossLaunchedQuit AND RIVAL BOSS")
					ENDIF
				ELSE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] biS_BossLaunchedQuit BUT NOT SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD")
				ENDIF
				
				IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
				OR DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
					IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneTelemetry)
						SET_BIT(iBoolsBitSet, biL_DoneTelemetry)
						GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_BOSS_LEFT)
					ENDIF
				ENDIF
				
				GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_ASSAULT, FALSE, gbRewards)
				SET_BIT(iBoolsBitSet2, biL2_WaitForShard)
			//	bEventCompleted = TRUE
			ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_VehicleWrecked)
				//-- Vehicle wrecked
				IF SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD()
					IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
						IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) > GB_UI_LEVEL_BACKGROUND
							SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_ASLWRK") // Your Boss quit the session
						ENDIF
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] biS_BossLaunchedQuit AND MY BOSS QUIT")
					ELSE
						IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) > GB_UI_LEVEL_BACKGROUND
							SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_ASLWRK") // The rival Boss quit the session
						ENDIF
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] biS_BossLaunchedQuit AND RIVAL BOSS")
					ENDIF
				ELSE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] biS_BossLaunchedQuit BUT NOT SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD")
				ENDIF
				
				IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
				OR DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
					IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneTelemetry)
						SET_BIT(iBoolsBitSet, biL_DoneTelemetry)
						GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST)
					ENDIF
				ENDIF
				
				GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_ASSAULT, FALSE, gbRewards)
				SET_BIT(iBoolsBitSet2, biL2_WaitForShard)
			//	bEventCompleted = TRUE
			ENDIF
		ENDIF
		
		//IF bEventCompleted
		IF IS_BIT_SET(iBoolsBitSet2, biL2_WaitForShard)
			IF NOT IS_BIT_SET(iBoolsBitSet2, biL2_ResetAudioForEndShard)
				IF IS_BIT_SET(iBoolsBitSet, biL_CountdownAudio)
					iDurationSound = -1
					STOP_SOUND(iDurationSound)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] STOP_SOUND(iDurationSound)")
				ENDIF
				
				Clear_Any_Objective_Text_From_This_Script()
				SET_BIT(iBoolsBitSet2, biL2_ResetAudioForEndShard)
				
				IF IS_BIT_SET(iBoolsBitSet, biL_InitAudio)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  [CHECK_FOR_REWARD] [MAINTAIN_GB_ASSAULT_MUSIC] MISSION RESETTING AUDIO")
					TRIGGER_MUSIC_EVENT("BG_ASSAULT_STOP")
					
					SET_AUDIO_FLAG("DisableFlightMusic", FALSE)
					SET_AUDIO_FLAG("WantedMusicDisabled", FALSE) 
				ENDIF
			ENDIF
				
			IF DOES_BLIP_EXIST(blipVeh)
				REMOVE_BLIP(blipVeh)
			ENDIF
			
			IF DOES_BLIP_EXIST(blipLoc)
				REMOVE_BLIP(blipLoc)
			ENDIF
			
			IF DOES_BLIP_EXIST(blipPackage)
				REMOVE_BLIP(blipPackage)
			ENDIF
			
			RESET_ALL_PLAYER_BLIPS()
			
			
			IF NOT IS_BIT_SET(iBoolsBitSet, biL_ShouldStop30s)
				SET_BIT(iBoolsBitSet, biL_ShouldStop30s)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] SET biL_ShouldStop30s ")
			ENDIF
			
			IF GB_MAINTAIN_BOSS_END_UI(gbBossEndUi) 
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Finished)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] SET biP_Finished ")
			#IF IS_DEBUG_BUILD
			ELSE
				IF GET_FRAME_COUNT() % 100 = 0
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_REWARD] WAITING FOR GB_MAINTAIN_BOSS_END_UI ")
				ENDIF
			#ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Controls the ped blips displaying for when they shoot
PROC CONTROL_PED_LOOP()
//	INT i
//	REPEAT MAX_MW_PEDS i
//		UPDATE_ENEMY_NET_PED_BLIP(serverBD.niMwPed[i], MwPedBlipData[i], DEFAULT, DEFAULT, TRUE, FALSE)
//		IF DOES_BLIP_EXIST(MwPedBlipData[i].blipId)
//			SET_BLIP_NAME_FROM_TEXT_FILE(MwPedBlipData[i].blipId, "UW_BLIP2")
//		ENDIF
//		
//		IF DOES_BLIP_EXIST(MwPedBlipData[i].VehicleBlipID)
//			SET_BLIP_COLOUR(MwPedBlipData[i].VehicleBlipID, BLIP_COLOUR_RED)
//		ENDIF
//	ENDREPEAT
ENDPROC





/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MISSION PROCS                  //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE:
///    Start a countdown when at least one player is in the vehicle. Mission will automatically start when timer ends
PROC MAINTAIN_START_MISSION_TIMER_SERVER()
//	BOOL bTimerShouldStart
//	
//	IF GET_ENTITY_MODEL(NET_TO_VEH(serverbd.niveh)) = VALKYRIE
//		bTimerShouldStart = IS_BIT_SET(serverbd.iServerBitSet, biS_AtLeastTwoPlayersInVeh)
//	ELSE
//		bTimerShouldStart = IS_BIT_SET(serverbd.iServerBitSet, biS_AtLeastOnePlayerInVeh)
//	ENDIF
//	
//	IF bTimerShouldStart //IS_BIT_SET(serverbd.iServerBitSet, biS_AtLeastOnePlayerInVeh)
//		IF NOT HAS_NET_TIMER_STARTED(serverbd.timeMissionStart)
//			START_NET_TIMER(serverbd.timeMissionStart)
//			NET_PRINT_TIME() NET_PRINT("     ---------->     GB_ASSAULT -  START timeMissionStart    <----------     ") NET_NL()
//		ENDIF
//	ELSE
//		IF HAS_NET_TIMER_STARTED(serverbd.timeMissionStart)
//			RESET_NET_TIMER(serverbd.timeMissionStart)
//			NET_PRINT_TIME() NET_PRINT("     ---------->     GB_ASSAULT -  RESET timeMissionStart    <----------     ") NET_NL()
//		ENDIF
//	ENDIF
ENDPROC


PROC MAINTAIN_START_MISSION_TIMER_CLIENT()
	
//	BOOL bTimerShouldStart
//	
//	IF GET_ENTITY_MODEL(NET_TO_VEH(serverbd.niveh)) = VALKYRIE
//		bTimerShouldStart = IS_BIT_SET(serverbd.iServerBitSet, biS_AtLeastTwoPlayersInVeh)
//	ELSE
//		bTimerShouldStart = IS_BIT_SET(serverbd.iServerBitSet, biS_AtLeastOnePlayerInVeh)
//	ENDIF
//	
//	IF bTimerShouldStart //IS_BIT_SET(serverbd.iServerBitSet, biS_AtLeastOnePlayerInVeh)
//		IF (TIME_MISSION_START-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeMissionStart)) >= 0
//			SET_MIDDLE_TITLE_POSITION_HUD_THIS_FRAME()
//			DRAW_GENERIC_TIMER((TIME_MISSION_START-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeMissionStart)), "UW_WAIT", 0, TIMER_STYLE_USEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_DONTCARE, DEFAULT, HUD_COLOUR_WHITE)
//		ELSE
//			SET_MIDDLE_TITLE_POSITION_HUD_THIS_FRAME()
//			DRAW_GENERIC_TIMER(0, "UW_WAIT", 0, TIMER_STYLE_USEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_DONTCARE, DEFAULT, HUD_COLOUR_WHITE)
//			
//			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
//				SET_BIT(serverbd.iServerBitSet, biS_MissionStartExpired)
//				NET_PRINT_TIME() NET_PRINT("     ---------->     GB_ASSAULT -  MISSION START TIME EXPIRED    <----------     ") NET_NL()
//			ENDIF
//		ENDIF
//	ENDIF		
ENDPROC

PROC MAINTAIN_UW_MUSIC()
	
//	IF NOT IS_BIT_SET(iBoolsBitSet, biDoneMusic)
//		IF serverBD.eStage = eGB_GET_PACKAGE
//			IF TRIGGER_MUSIC_EVENT("MP_MC_ACTION")
//				SET_BIT(iBoolsBitSet, biDoneMusic)
//				NET_PRINT_TIME() NET_PRINT("     ---------->     GB_ASSAULT -  MAINTAIN_UW_MUSIC - MP_MC_ACTION    <----------     ") NET_NL()
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	IF NOT IS_BIT_SET(iBoolsBitSet, biResetMusic)
//		IF IS_BIT_SET(iBoolsBitSet, biDoneMusic)
//			IF serverBD.eStage > eGB_GET_PACKAGE
//				IF IS_PED_INJURED(PLAYER_PED_ID())
//					IF TRIGGER_MUSIC_EVENT("MP_MC_FAIL")
//						SET_BIT(iBoolsBitSet, biResetMusic)						
//						NET_PRINT_TIME() NET_PRINT("     ---------->     GB_ASSAULT -  MAINTAIN_UW_MUSIC - MP_MC_FAIL    <----------     ") NET_NL()
//					ENDIF
//				ELSE
//					IF TRIGGER_MUSIC_EVENT("MP_MC_STOP")
//						SET_BIT(iBoolsBitSet, biResetMusic)						
//						NET_PRINT_TIME() NET_PRINT("     ---------->     GB_ASSAULT -  MAINTAIN_UW_MUSIC - MP_MC_STOP    <----------     ") NET_NL()
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
ENDPROC

PROC DRAW_KILLS_BAR()
	
//	DRAW_GENERIC_BIG_DOUBLE_NUMBER((serverBD.iKillGoal-serverBD.iTotalKills), serverBD.iKillGoal, "GHO_KILLB")
ENDPROC

FUNC STRING GET_TO_LOCATION_TEXT()
	SWITCH GET_GB_ASSAULT_VARIATION()
		CASE GB_ASSAULT_ALTRUIST		RETURN "GB_ASLT_GO0"
		CASE GB_ASSAULT_ZANCUDO			RETURN "GB_ASLT_GO1"	
		CASE GB_ASSAULT_AIRPORT			RETURN "GB_ASLT_GO2"
	//	CASE GB_ASSAULT_HUMANE			RETURN "GB_ASLT_GO3"
		CASE GB_ASSAULT_MERRYWEATHER	RETURN "GB_ASLT_GO4"
	ENDSWITCH
	RETURN ""
ENDFUNC
FUNC STRING GET_INTRO_TEXT_MESSAGE_TEXT()
	SWITCH GET_GB_ASSAULT_VARIATION()
		CASE GB_ASSAULT_ALTRUIST		RETURN "GB_ASLT_MALT"
		CASE GB_ASSAULT_ZANCUDO			RETURN "GB_ASLT_MZAN"	
		CASE GB_ASSAULT_AIRPORT			RETURN "GB_ASLT_MAIR"
		CASE GB_ASSAULT_MERRYWEATHER	RETURN "GB_ASLT_MMER"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC STRING GET_STOLE_ENTITY_TEXT_MESSAGE_TEXT()
	SWITCH GET_GB_ASSAULT_VARIATION()
		CASE GB_ASSAULT_ALTRUIST		RETURN "GB_ASLT_MALT2"
//		CASE GB_ASSAULT_ZANCUDO			RETURN "GB_ASLT_MZAN"	
//		CASE GB_ASSAULT_AIRPORT			RETURN "GB_ASLT_MAIR"
		CASE GB_ASSAULT_MERRYWEATHER	RETURN "GB_ASLT_MMER2"
	ENDSWITCH
	RETURN ""

ENDFUNC
PROC MAINTAIN_TEXT_MESSAGE()
	//-- Intro text message
	IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneIntroTextMsg)
		IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
			IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_BACKGROUND
				IF Request_MP_Comms_Txtmsg(CHAR_MP_RAY_LAVOY , GET_INTRO_TEXT_MESSAGE_TEXT(), DEFAULT, DEFAULT)
					SET_BIT(iBoolsBitSet, biL_DoneIntroTextMsg)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_TEXT_MESSAGE] SET biL_DoneIntroTextMsg")
				ENDIF
			ENDIF
		ELSE
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_TEXT_MESSAGE] SET biL_DoneIntroTextMsg, MY BOSS DIDN'T LAUNCH")
			SET_BIT(iBoolsBitSet, biL_DoneIntroTextMsg)
		ENDIF
	ENDIF
	
	//-- Text message when package / vehicle is taken
	IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneStealEntityTxt)
		IF GET_GB_ASSAULT_VARIATION() = GB_ASSAULT_ALTRUIST
		OR GET_GB_ASSAULT_VARIATION() = GB_ASSAULT_MERRYWEATHER
			IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
				IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_BACKGROUND
					IF serverBD.iPartPackageHolder != -1
					OR serverBD.iPartInVeh != -1
						IF Request_MP_Comms_Txtmsg(CHAR_MP_RAY_LAVOY , GET_STOLE_ENTITY_TEXT_MESSAGE_TEXT(), DEFAULT, DEFAULT)
							SET_BIT(iBoolsBitSet, biL_DoneStealEntityTxt)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_TEXT_MESSAGE] SET biL_DoneStealEntityTxt")
						ENDIF
					ENDIF
				ENDIF
			ELSE
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_TEXT_MESSAGE] SET biL_DoneStealEntityTxt, MY BOSS DIDN'T LAUNCH")
				SET_BIT(iBoolsBitSet, biL_DoneStealEntityTxt)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_HELP_TEXT()
	TEXT_LABEL_31 tl31Help 
	//-- Help text - starting gang launch help
	IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneLaunchHelp)
		IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
			IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_BACKGROUND
				IF NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GB_START_OF_JOB)
					IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
						tl31Help = GET_INITIAL_HELP_FOR_VARIATION()
						PRINT_HELP_NO_SOUND(tl31Help) // Retrieve the package and deliver it to the drop-off. Your gang will fail if the package is taken by a rival gang.
						GB_SET_GANG_BOSS_HELP_BACKGROUND()
						SET_BIT(iBoolsBitSet, biL_DoneLaunchHelp)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_HELP_TEXT] SET biL_DoneLaunchHelp - MY BOSS LAUNCHED WORK")
					ELSE
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_HELP_TEXT] NOT OK TO PRINT HELP - biL_DoneLaunchHelp")				
					ENDIF
				ENDIF
			ENDIF
		ELSE
			SET_BIT(iBoolsBitSet, biL_DoneLaunchHelp)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GET_TO_LOCATION_CLIENT] SET biL_DoneLaunchHelp - MY BOSS DIDN'T LAUNCH WORK")
		ENDIF
	ENDIF
	
	//-- Help text when reaching location for joining gang
	IF NOT IS_BIT_SET(iBoolsBitSet2, biL2_DoneReachedGotoHelp)
		IF IS_BIT_SET(iBoolsBitSet, biL_DoneLaunchHelp)
			IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
				IF playerBD[PARTICIPANT_ID_TO_INT()].eStage > eGB_GET_TO_LOCATION
					IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
						
						IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
							tl31Help = GET_REACHED_GOTO_HELP_FOR_VARIATION()
							PRINT_HELP_NO_SOUND(tl31Help) 
							GB_SET_GANG_BOSS_HELP_BACKGROUND()
							SET_BIT(iBoolsBitSet2, biL2_DoneReachedGotoHelp)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_HELP_TEXT] SET biL_DoneLaunchHelp SET biL2_DoneReachedGotoHelp")
						ELSE
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_HELP_TEXT] NOT OK TO PRINT HELP - biL2_DoneReachedGotoHelp")				
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//-- Launching gang told that rivals have joined
	IF NOT IS_BIT_SET(iBoolsBitSet2, biL2_RivalsJoinedHelp)
		IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
			IF serverBD.iPartPackageHolder != -1
			OR serverBD.iPartInVeh != -1
			OR IS_BIT_SET(serverBD.iServerBitSet, biS_CollectedAtLeastOnce)
			OR IS_BIT_SET(serverBD.iServerBitSet, biS_ForceRivalsOnMission)
				IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
					PRINT_HELP_NO_SOUND("GB_SGHT_RVL") // Other players in the session have been alerted to your activities. These players can now come after your Organization to earn cash and RP.
					GB_SET_GANG_BOSS_HELP_BACKGROUND()
					SET_BIT(iBoolsBitSet2, biL2_RivalsJoinedHelp)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_HELP_TEXT] , SET biL2_RivalsJoinedHelp")
						
				ELSE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_HELP_TEXT] NOT IS_OK_TO_PRINT_FREEMODE_HELP - biL2_RivalsJoinedHelp")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_PLAYER_APPROACH_ENTITY()
	

	VECTOR vEntityLoc = << 0.0, 0.0, 0.0>>
	VECTOR vPlayerLoc
	FLOAT fDist2
	
	IF NOT DID_MY_BOSS_LAUNCH_GB_ASSAULT()
	AND NOT DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
		EXIT
	ENDIF
	
	IF IS_THIS_A_STEAL_VEHICLE_VARIATION()
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])
			IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[0])
				vEntityLoc = GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niVeh[0]))
			ENDIF
		ENDIF
	ELSE
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
			vEntityLoc = GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.niPackage))
		ENDIF
	ENDIF
	
	IF NOT ARE_VECTORS_EQUAL(vEntityLoc, << 0.0, 0.0, 0.0>>)
		IF SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD()
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				
				vPlayerLoc = GET_ENTITY_COORDS(PLAYER_PED_ID())
				fDist2 = VDIST2(vEntityLoc, vPlayerLoc) 
				IF NOT IS_BIT_SET(iBoolsBitSet, biL_ApproachPackage)
					IF fDist2 <= ACTIVE_DIST2
						SET_BIT(iBoolsBitSet, biL_ApproachPackage)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT SET biL_ApproachPackage vDist2 = ", fDist2, " ACTIVE_DIST2 = ", ACTIVE_DIST2)
					ELIF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AiPackageHolderKilled)
						IF IS_COORD_IN_SPECIFIED_AREA(GET_ENTITY_COORDS( PLAYER_PED_ID(), FALSE ), AC_MILITARY_BASE, 1000 )
							SET_BIT(iBoolsBitSet, biL_ApproachPackage)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT SET biL_ApproachPackage AS I'M IN MILITARY BASE")
						ENDIF
					ENDIF
				ELSE
					IF fDist2 > NONACTIVE_DIST2
						IF IS_BIT_SET(serverBD.iServerBitSet, biS_AiPackageHolderKilled)
						OR (NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AiPackageHolderKilled) AND NOT IS_COORD_IN_SPECIFIED_AREA(GET_ENTITY_COORDS( PLAYER_PED_ID(), FALSE ), AC_MILITARY_BASE, 1000 ))
							CLEAR_BIT(iBoolsBitSet, biL_ApproachPackage)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT CLEAR biL_ApproachPackage vDist2 = ", fDist2, " ACTIVE_DIST2 = ", NONACTIVE_DIST2)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/*
SETUP_SPECIFIC_SPAWN_LOCATION(// VECTOR vCoords, 
							//	FLOAT fHeading, 
							//	FLOAT fSearchRadius=100.0, 
							//	BOOL bDoVisibleChecks=TRUE, 
							//	FLOAT fMinDistFromCoords=0.0, 
							//	BOOL bConsiderInteriors = FALSE, 
							//	BOOL bDoNearARoadChecks=TRUE, 
							//	FLOAT MinDistFromOtherPlayers=65.0, 
							//	BOOL bNearCentrePoint=TRUE, 
							//	BOOL bIgnoreGlobalExclusionZones=FALSE)

*/
//PROC MAINTAIN_GB_ASSAULT_SPAWNING()
//	MAINTAIN_PLAYER_APPROACH_ENTITY()
//	VECTOR vPlayerCoords
//	VECTOR vPackageCoords
//	
//	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
//	AND SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD()
//	AND IS_BIT_SET(iBoolsBitSet, biL_ApproachPackage)
//	AND NOT IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), FALSE)
//	AND NOT IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
//		vPlayerCoords = GET_ENTITY_COORDS( PLAYER_PED_ID(), FALSE )
//		vPackageCoords = GET_ENTITY_COORDS( NET_TO_OBJ(serverBD.niPackage), FALSE )
//		IF NOT IS_COORD_IN_SPECIFIED_AREA(vPlayerCoords, AC_MILITARY_BASE, 1000 )
//			//-- I'm not in the military base - Don't spawn in the base
//			
//			#IF IS_DEBUG_BUILD
//			IF iDebugSpawn != 0
//				iDebugSpawn = 0
//				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GB_ASSAULT_SPAWNING] UPDATE iDebugSpawn = 0 - I'm NOT IN MILITARY BASE vPlayerCoords = ", vPlayerCoords, " vPackageCoords = ", vPackageCoords)
//			ENDIF
//			#ENDIF
//			
//			SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_NEAR_SPECIFIC_COORDS)	
//				
//			SETUP_SPECIFIC_SPAWN_LOCATION(GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.niPackage)), // VECTOR vCoords, 
//											0.0, 											 //	FLOAT fHeading, 
//											250.0, 											 //	FLOAT fSearchRadius=100.0, 
//											TRUE, 											 //	BOOL bDoVisibleChecks=TRUE, 
//											FMEVENT_MINIMUM_SPAWN_DISTANCE_FROM_ACTION, 	 //	FLOAT fMinDistFromCoords=0.0, 
//											FALSE, 											 //	BOOL bConsiderInteriors = FALSE, 
//											TRUE, 											 //	BOOL bDoNearARoadChecks=TRUE, 
//											65, 											 //	FLOAT MinDistFromOtherPlayers=65.0, 
//											TRUE, 											 //	BOOL bNearCentrePoint=TRUE, 
//											FALSE)											 //	BOOL bIgnoreGlobalExclusionZones=FALSE)
//											
//			SET_PLAYER_WILL_SPAWN_FACING_COORDS(vPackageCoords, TRUE, FALSE)
//		ELSE
//			//-- I'm in the military base
//			IF IS_COORD_IN_SPECIFIED_AREA(vPackageCoords, AC_MILITARY_BASE, 1000 )
//				//-- Package is in the military base - spawn in the base
//				
//				#IF IS_DEBUG_BUILD
//				IF iDebugSpawn != 1
//					iDebugSpawn = 1
//					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GB_ASSAULT_SPAWNING] UPDATE iDebugSpawn = 1 - I'm IN MILITARY BASE SO IS PACKAGE vPlayerCoords = ", vPlayerCoords, " vPackageCoords = ", vPackageCoords)
//				ENDIF
//				#ENDIF
//			
//				SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_NEAR_SPECIFIC_COORDS)	
//				
//				SETUP_SPECIFIC_SPAWN_LOCATION(<<-2125.3035, 3130.6968, 31.8101>>, // VECTOR vCoords, 
//												0.0,  //	FLOAT fHeading, 
//												250.0, //	FLOAT fSearchRadius=100.0, 
//												TRUE,  //	BOOL bDoVisibleChecks=TRUE, 
//												100,	 	//	FLOAT fMinDistFromCoords=0.0, 
//												FALSE, 	 //	BOOL bConsiderInteriors = FALSE, 
//												TRUE, //	BOOL bDoNearARoadChecks=TRUE, 
//												65, 	 //	FLOAT MinDistFromOtherPlayers=65.0, 
//												TRUE, 	//	BOOL bNearCentrePoint=TRUE, 
//												TRUE)	 //	BOOL bIgnoreGlobalExclusionZones=FALSE)
//				
//				SET_PLAYER_WILL_SPAWN_FACING_COORDS(vPackageCoords, TRUE, FALSE)
//			ELSE
//				//-- Package not in the military base - don't spawn in the base
//				
//				#IF IS_DEBUG_BUILD
//				IF iDebugSpawn != 2
//					iDebugSpawn = 2
//					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GB_ASSAULT_SPAWNING] UPDATE iDebugSpawn = 2 - I'm IN MILITARY BASE BUT PACKAGE IS NOT vPlayerCoords = ", vPlayerCoords, " vPackageCoords = ", vPackageCoords)
//				ENDIF
//				#ENDIF
//				
//				SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_NEAR_SPECIFIC_COORDS)	
//				
//				SETUP_SPECIFIC_SPAWN_LOCATION(GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.niPackage)), // VECTOR vCoords, 
//												0.0, 											 //	FLOAT fHeading, 
//												250.0, 											 //	FLOAT fSearchRadius=100.0, 
//												TRUE, 											 //	BOOL bDoVisibleChecks=TRUE, 
//												FMEVENT_MINIMUM_SPAWN_DISTANCE_FROM_ACTION, 	 //	FLOAT fMinDistFromCoords=0.0, 
//												FALSE, 											 //	BOOL bConsiderInteriors = FALSE, 
//												TRUE, 											 //	BOOL bDoNearARoadChecks=TRUE, 
//												65, 											 //	FLOAT MinDistFromOtherPlayers=65.0, 
//												TRUE, 											 //	BOOL bNearCentrePoint=TRUE, 
//												FALSE)											 //	BOOL bIgnoreGlobalExclusionZones=FALSE)
//				SET_PLAYER_WILL_SPAWN_FACING_COORDS(vPackageCoords, TRUE, FALSE)
//			ENDIF
//		ENDIF
//		IF NOT IS_BIT_SET(iBoolsBitSet, biL_SetupSpawnCoord)
//			SET_BIT(iBoolsBitSet, biL_SetupSpawnCoord)
//		ENDIF
//
//	ELSE
//		IF IS_BIT_SET(iBoolsBitSet, biL_SetupSpawnCoord)
//			CLEAR_PLAYER_NEXT_RESPAWN_LOCATION()
//			CLEAR_SPECIFIC_SPAWN_LOCATION_FROM_THIS_SCRIPT()
//		ENDIF
//		
//		#IF IS_DEBUG_BUILD
//		IF iDebugSpawn != -1
//			iDebugSpawn = -1
//			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GB_ASSAULT_SPAWNING] UPDATE iDebugSpawn = -1 - RESET vPlayerCoords = ", vPlayerCoords, " vPackageCoords = ", vPackageCoords)
//		ENDIF
//		#ENDIF
//	ENDIF
//	
//ENDPROC












PROC SETUP_GB_ASSAULT_CUSTOM_SPAWN_LOCATIONS()
	INT iVar = GET_GB_ASSAULT_VARIATION()
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GB_ASSAULT_SPAWNING] [SETUP_GB_ASSAULT_CUSTOM_SPAWN_LOCATIONS] CALLED VARIATION ", iVar)
	
	SWITCH iVar
		CASE GB_ASSAULT_ALTRUIST
			ADD_CUSTOM_SPAWN_POINT(<<-1018.5450, 4933.3311, 200.4811>>, 131.9232)
			ADD_CUSTOM_SPAWN_POINT(<<-998.4789, 4962.4619, 192.9872>>, 136.2000)
			ADD_CUSTOM_SPAWN_POINT(<<-1003.2529, 4975.8813, 191.8856>>, 146.6270)
			ADD_CUSTOM_SPAWN_POINT(<<-1024.0352, 4964.9849, 198.0934>>, 166.4202)
			ADD_CUSTOM_SPAWN_POINT(<<-1031.5637, 4972.5967, 198.9790>>, 191.1345)
			ADD_CUSTOM_SPAWN_POINT(<<-1038.2605, 4969.6372, 200.8855>>, 199.1591)
			ADD_CUSTOM_SPAWN_POINT(<<-985.6907, 4953.9287, 203.5606>>, 113.9255)
			ADD_CUSTOM_SPAWN_POINT(<<-1013.7069, 4968.0093, 194.3295>>, 162.9687)
			ADD_CUSTOM_SPAWN_POINT(<<-1032.8268, 4939.0532, 202.4380>>, 167.0185)
			ADD_CUSTOM_SPAWN_POINT(<<-1006.2885, 4951.0757, 196.4628>>, 131.5821)
		BREAK
		
		CASE GB_ASSAULT_ZANCUDO
			ADD_CUSTOM_SPAWN_POINT(<<-2268.4927, 3209.1802, 31.8102>>, 273.8885)
			ADD_CUSTOM_SPAWN_POINT(<<-2257.8369, 3235.4700, 31.8102>>, 265.6924)
			ADD_CUSTOM_SPAWN_POINT(<<-2249.2693, 3271.2939, 31.8102>>, 249.5277)
			ADD_CUSTOM_SPAWN_POINT(<<-2080.9207, 3261.6973, 31.8103>>, 151.5588)
			ADD_CUSTOM_SPAWN_POINT(<<-2070.7957, 3247.1206, 31.8103>>, 90.5367)
			ADD_CUSTOM_SPAWN_POINT(<<-2098.8892, 3234.0071, 31.8103>>, 149.9890)
			ADD_CUSTOM_SPAWN_POINT(<<-2243.7937, 3279.5837, 31.8102>>, 241.0335)
			ADD_CUSTOM_SPAWN_POINT(<<-2293.5439, 3161.9028, 31.8269>>, 290.6908)
			ADD_CUSTOM_SPAWN_POINT(<<-2160.6785, 3141.5039, 31.8101>>, 355.4720)
			ADD_CUSTOM_SPAWN_POINT(<<-2211.0190, 3163.2822, 31.8101>>, 319.4941)
		BREAK
		
		CASE GB_ASSAULT_AIRPORT		
			ADD_CUSTOM_SPAWN_POINT(<<-1331.1711, -3363.2397, 12.9452>>, 313.5740)
			ADD_CUSTOM_SPAWN_POINT(<<-1326.5015, -3368.4075, 12.9471>>, 322.2009)
			ADD_CUSTOM_SPAWN_POINT(<<-1236.7800, -3414.9827, 12.9452>>, 333.1109)
			ADD_CUSTOM_SPAWN_POINT(<<-1231.4263, -3418.5237, 12.9452>>, 334.4677)
			ADD_CUSTOM_SPAWN_POINT(<<-1177.3115, -3375.8845, 12.9450>>, 83.8491)
			ADD_CUSTOM_SPAWN_POINT(<<-1186.2430, -3377.9194, 12.9450>>, 74.4569)
			ADD_CUSTOM_SPAWN_POINT(<<-1216.4106, -3378.8621, 12.9450>>, 70.7863)
			ADD_CUSTOM_SPAWN_POINT(<<-1288.7705, -3328.0593, 12.9450>>, 232.0976)
			ADD_CUSTOM_SPAWN_POINT(<<-1309.3745, -3295.1907, 12.9445>>, 221.8010)
			ADD_CUSTOM_SPAWN_POINT(<<-1308.3480, -3285.8477, 12.9451>>, 214.9276)
		BREAK
		
		CASE GB_ASSAULT_MERRYWEATHER
			ADD_CUSTOM_SPAWN_POINT(<<445.1494, -3025.9456, 5.0696>>, 222.6693)
			ADD_CUSTOM_SPAWN_POINT(<<448.0641, -3001.9409, 5.0157>>, 207.0258)
			ADD_CUSTOM_SPAWN_POINT(<<465.6537, -3021.4722, 5.0018>>, 197.7798)
			ADD_CUSTOM_SPAWN_POINT(<<481.5974, -3008.1887, 5.0445>>, 173.8646)
			ADD_CUSTOM_SPAWN_POINT(<<514.1252, -2991.0349, 5.0445>>, 158.1715)
			ADD_CUSTOM_SPAWN_POINT(<<529.0361, -2988.6333, 5.0445>>, 146.6360)
			ADD_CUSTOM_SPAWN_POINT(<<466.7868, -3000.2622, 5.0445>>, 191.0695)
			ADD_CUSTOM_SPAWN_POINT(<<601.0988, -3151.3479, 5.0693>>, 12.3165)
			ADD_CUSTOM_SPAWN_POINT(<<445.6588, -3053.0239, 5.0696>>, 277.6793)
			ADD_CUSTOM_SPAWN_POINT(<<444.9568, -3034.3518, 5.0696>>, 241.1481)
		BREAK
		
	ENDSWITCH
ENDPROC

FUNC VECTOR GET_ASSAULT_ENTITY_COORDS()
	VECTOR vLoc = << 0.0, 0.0,0.0>>
	IF IS_THIS_A_STEAL_VEHICLE_VARIATION()
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])
			IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[0])
				vLoc = GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niVeh[0]))
			ENDIF
		ENDIF
	ELSE
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
			vLoc = GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niPackage))
		ENDIF
	ENDIF
	
	
	RETURN vLoc
ENDFUNC

PROC MAINTAIN_GB_ASSAULT_SPAWNING()
	MAINTAIN_PLAYER_APPROACH_ENTITY()
	VECTOR vPlayerCoords
	
	VECTOR vEntityCoords
	
	FLOAT fPlayerDist
	FLOAT fDistToUseForCust
	VECTOR vLocation = GET_LOCATION_FOR_VARIATION()
	
	BOOL bUseEntityCoordSpawning

	BOOL bEntityExists 
	
	IF NOT IS_THIS_A_STEAL_VEHICLE_VARIATION()
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
			bEntityExists = TRUE
		ENDIF
	ELSE	
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])
			bEntityExists = TRUE
		ENDIF
	ENDIF
	
	IF bEntityExists
	AND SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD()
	AND IS_BIT_SET(iBoolsBitSet, biL_ApproachPackage)
	AND NOT IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), FALSE)

		//-- If I'm on the launching gang then use custom spawn points near the location, unt il the package / vehicle has been collected
		IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()

			
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_CollectedAtLeastOnce)
				IF NOT IS_BIT_SET(iBoolsBitSet, biL_SetupCustomSpawns)
					fPlayerDist = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vLocation , FALSE)
					IF GET_GB_ASSAULT_VARIATION() = GB_ASSAULT_ZANCUDO
						fDistToUseForCust = 350.0
					ELSE
						fDistToUseForCust = 200
					ENDIF
					
					IF fPlayerDist  < fDistToUseForCust
						USE_CUSTOM_SPAWN_POINTS(TRUE)
						SETUP_GB_ASSAULT_CUSTOM_SPAWN_LOCATIONS()
						SET_BIT(iBoolsBitSet, biL_SetupCustomSpawns)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GB_ASSAULT_SPAWNING] SET biL_SetupCustomSpawns fPlayerDist = ", fPlayerDist, " fDistToUseForCust = ", fDistToUseForCust, " vLocation = ", vLocation)
					ENDIF
				ELSE
					fPlayerDist = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vLocation, FALSE)
					
					IF GET_GB_ASSAULT_VARIATION() = GB_ASSAULT_ZANCUDO
						fDistToUseForCust = 350.0
					ELSE
						fDistToUseForCust = 200
					ENDIF
					
					IF fPlayerDist > fDistToUseForCust
						CLEAR_BIT(iBoolsBitSet, biL_SetupCustomSpawns)
						CLEAR_CUSTOM_SPAWN_POINTS()
						USE_CUSTOM_SPAWN_POINTS(FALSE)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GB_ASSAULT_SPAWNING] CLEAR biL_SetupCustomSpawns AS fPlayerDist = ", fPlayerDist, " fDistToUseForCust = ", fDistToUseForCust, " vLocation = ", vLocation)
					ENDIF
				ENDIF
			ELSE	
				IF IS_BIT_SET(iBoolsBitSet, biL_SetupCustomSpawns)
					CLEAR_BIT(iBoolsBitSet, biL_SetupCustomSpawns)
					CLEAR_CUSTOM_SPAWN_POINTS()
					USE_CUSTOM_SPAWN_POINTS(FALSE)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GB_ASSAULT_SPAWNING] CLEAR biL_SetupCustomSpawns AS biS_CollectedAtLeastOnce")
				ENDIF
				
				bUseEntityCoordSpawning = TRUE
			ENDIF
		ELSE	
			bUseEntityCoordSpawning = TRUE
		ENDIF
		
		IF NOT bUseEntityCoordSpawning
			EXIT
		ENDIF
		
		vEntityCoords = GET_ASSAULT_ENTITY_COORDS()
		
		IF ARE_VECTORS_EQUAL(vEntityCoords, << 0.0, 0.0, 0.0>>)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GB_ASSAULT_SPAWNING] EXIT AS vEntityCoords = ", vEntityCoords)
			EXIT
		ENDIF
		
		//-- If I'm on a rival gang, or if the package/vehicle has been collected at least once, spawn near the package / vehicle
		
		
		vPlayerCoords = GET_ENTITY_COORDS( PLAYER_PED_ID(), FALSE )

		SWITCH GET_GB_ASSAULT_VARIATION()
			CASE GB_ASSAULT_ZANCUDO
				IF NOT IS_COORD_IN_SPECIFIED_AREA(vPlayerCoords, AC_MILITARY_BASE, 1000 )
					//-- I'm not in the military base - Don't spawn in the base
					
					#IF IS_DEBUG_BUILD
					IF iDebugSpawn != 0
						iDebugSpawn = 0
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GB_ASSAULT_SPAWNING] UPDATE iDebugSpawn = 0 - I'm NOT IN MILITARY BASE vPlayerCoords = ", vPlayerCoords, " vEntityCoords = ", vEntityCoords)
					ENDIF
					#ENDIF
			
					SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_NEAR_SPECIFIC_COORDS)	
				
					SETUP_SPECIFIC_SPAWN_LOCATION(vEntityCoords, // VECTOR vCoords, 
													0.0, 											 //	FLOAT fHeading, 
													250.0, 											 //	FLOAT fSearchRadius=100.0, 
													TRUE, 											 //	BOOL bDoVisibleChecks=TRUE, 
													FMEVENT_MINIMUM_SPAWN_DISTANCE_FROM_ACTION, 	 //	FLOAT fMinDistFromCoords=0.0, 
													FALSE, 											 //	BOOL bConsiderInteriors = FALSE, 
													TRUE, 											 //	BOOL bDoNearARoadChecks=TRUE, 
													65, 											 //	FLOAT MinDistFromOtherPlayers=65.0, 
													TRUE, 											 //	BOOL bNearCentrePoint=TRUE, 
													FALSE)											 //	BOOL bIgnoreGlobalExclusionZones=FALSE)
													
					SET_PLAYER_WILL_SPAWN_FACING_COORDS(vEntityCoords, TRUE, FALSE)
				ELSE
					//-- I'm in the military base
					IF IS_COORD_IN_SPECIFIED_AREA(vEntityCoords, AC_MILITARY_BASE, 1000 )
						//-- Package is in the military base - spawn in the base
						
						#IF IS_DEBUG_BUILD
						IF iDebugSpawn != 1
							iDebugSpawn = 1
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GB_ASSAULT_SPAWNING] UPDATE iDebugSpawn = 1 - I'm IN MILITARY BASE SO IS PACKAGE vPlayerCoords = ", vPlayerCoords, " vEntityCoords = ", vEntityCoords)
						ENDIF
						#ENDIF
				
						SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_NEAR_SPECIFIC_COORDS)	
					
						SETUP_SPECIFIC_SPAWN_LOCATION(<<-2125.3035, 3130.6968, 31.8101>>, // VECTOR vCoords, 
														0.0,  //	FLOAT fHeading, 
														250.0, //	FLOAT fSearchRadius=100.0, 
														TRUE,  //	BOOL bDoVisibleChecks=TRUE, 
														100,	 	//	FLOAT fMinDistFromCoords=0.0, 
														FALSE, 	 //	BOOL bConsiderInteriors = FALSE, 
														TRUE, //	BOOL bDoNearARoadChecks=TRUE, 
														65, 	 //	FLOAT MinDistFromOtherPlayers=65.0, 
														TRUE, 	//	BOOL bNearCentrePoint=TRUE, 
														TRUE)	 //	BOOL bIgnoreGlobalExclusionZones=FALSE)
						
						SET_PLAYER_WILL_SPAWN_FACING_COORDS(vEntityCoords, TRUE, FALSE)
					ELSE
						//-- Package not in the military base - don't spawn in the base
						
						#IF IS_DEBUG_BUILD
						IF iDebugSpawn != 2
							iDebugSpawn = 2
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GB_ASSAULT_SPAWNING] UPDATE iDebugSpawn = 2 - I'm IN MILITARY BASE BUT PACKAGE IS NOT vPlayerCoords = ", vPlayerCoords, " vEntityCoords = ", vEntityCoords)
						ENDIF
						#ENDIF
						
						SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_NEAR_SPECIFIC_COORDS)	
						
						SETUP_SPECIFIC_SPAWN_LOCATION(vEntityCoords, // VECTOR vCoords, 
														0.0, 											 //	FLOAT fHeading, 
														250.0, 											 //	FLOAT fSearchRadius=100.0, 
														TRUE, 											 //	BOOL bDoVisibleChecks=TRUE, 
														FMEVENT_MINIMUM_SPAWN_DISTANCE_FROM_ACTION, 	 //	FLOAT fMinDistFromCoords=0.0, 
														FALSE, 											 //	BOOL bConsiderInteriors = FALSE, 
														TRUE, 											 //	BOOL bDoNearARoadChecks=TRUE, 
														65, 											 //	FLOAT MinDistFromOtherPlayers=65.0, 
														TRUE, 											 //	BOOL bNearCentrePoint=TRUE, 
														FALSE)											 //	BOOL bIgnoreGlobalExclusionZones=FALSE)
						SET_PLAYER_WILL_SPAWN_FACING_COORDS(vEntityCoords, TRUE, FALSE)
					ENDIF
				ENDIF
			BREAK
			
			
			CASE GB_ASSAULT_AIRPORT
				IF NOT IS_COORD_IN_SPECIFIED_AREA(vPlayerCoords, AC_AIRPORT_AIRSIDE, 1000 )
					//-- I'm not in the airport - Don't spawn in the airport
					
					#IF IS_DEBUG_BUILD
					IF iDebugSpawn != 0
						iDebugSpawn = 0
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GB_ASSAULT_SPAWNING] UPDATE iDebugSpawn = 0 - I'm NOT IN AIRPORT vPlayerCoords = ", vPlayerCoords, " vEntityCoords = ", vEntityCoords)
					ENDIF
					#ENDIF
			
					SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_NEAR_SPECIFIC_COORDS)	
				
					SETUP_SPECIFIC_SPAWN_LOCATION(vEntityCoords, // VECTOR vCoords, 
													0.0, 											 //	FLOAT fHeading, 
													250.0, 											 //	FLOAT fSearchRadius=100.0, 
													TRUE, 											 //	BOOL bDoVisibleChecks=TRUE, 
													FMEVENT_MINIMUM_SPAWN_DISTANCE_FROM_ACTION, 	 //	FLOAT fMinDistFromCoords=0.0, 
													FALSE, 											 //	BOOL bConsiderInteriors = FALSE, 
													TRUE, 											 //	BOOL bDoNearARoadChecks=TRUE, 
													65, 											 //	FLOAT MinDistFromOtherPlayers=65.0, 
													TRUE, 											 //	BOOL bNearCentrePoint=TRUE, 
													FALSE)											 //	BOOL bIgnoreGlobalExclusionZones=FALSE)
													
					SET_PLAYER_WILL_SPAWN_FACING_COORDS(vEntityCoords, TRUE, FALSE)
				ELSE
					//-- I'm in the airport
					IF IS_COORD_IN_SPECIFIED_AREA(vEntityCoords, AC_AIRPORT_AIRSIDE, 1000 )
						//-- Package is in the airport - spawn in the base
						
						#IF IS_DEBUG_BUILD
						IF iDebugSpawn != 1
							iDebugSpawn = 1
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GB_ASSAULT_SPAWNING] UPDATE iDebugSpawn = 1 - I'm IN AIRPORTE SO IS PACKAGE vPlayerCoords = ", vPlayerCoords, " vEntityCoords = ", vEntityCoords)
						ENDIF
						#ENDIF
				
						SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_NEAR_SPECIFIC_COORDS)	
					
						SETUP_SPECIFIC_SPAWN_LOCATION(<<-1294.8274, -3110.4224, 12.9469>>, // VECTOR vCoords, 
														0.0,  //	FLOAT fHeading, 
														250.0, //	FLOAT fSearchRadius=100.0, 
														TRUE,  //	BOOL bDoVisibleChecks=TRUE, 
														100,	 	//	FLOAT fMinDistFromCoords=0.0, 
														FALSE, 	 //	BOOL bConsiderInteriors = FALSE, 
														TRUE, //	BOOL bDoNearARoadChecks=TRUE, 
														65, 	 //	FLOAT MinDistFromOtherPlayers=65.0, 
														TRUE, 	//	BOOL bNearCentrePoint=TRUE, 
														TRUE)	 //	BOOL bIgnoreGlobalExclusionZones=FALSE)
						
						SET_PLAYER_WILL_SPAWN_FACING_COORDS(vEntityCoords, TRUE, FALSE)
					ELSE
						//-- Package not in the AIRPORT - don't spawn in the base
						
						#IF IS_DEBUG_BUILD
						IF iDebugSpawn != 2
							iDebugSpawn = 2
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GB_ASSAULT_SPAWNING] UPDATE iDebugSpawn = 2 - I'm IN AIRPORT BUT PACKAGE IS NOT vPlayerCoords = ", vPlayerCoords, " vEntityCoords = ", vEntityCoords)
						ENDIF
						#ENDIF
						
						SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_NEAR_SPECIFIC_COORDS)	
						
						SETUP_SPECIFIC_SPAWN_LOCATION(vEntityCoords, // VECTOR vCoords, 
														0.0, 											 //	FLOAT fHeading, 
														250.0, 											 //	FLOAT fSearchRadius=100.0, 
														TRUE, 											 //	BOOL bDoVisibleChecks=TRUE, 
														FMEVENT_MINIMUM_SPAWN_DISTANCE_FROM_ACTION, 	 //	FLOAT fMinDistFromCoords=0.0, 
														FALSE, 											 //	BOOL bConsiderInteriors = FALSE, 
														TRUE, 											 //	BOOL bDoNearARoadChecks=TRUE, 
														65, 											 //	FLOAT MinDistFromOtherPlayers=65.0, 
														TRUE, 											 //	BOOL bNearCentrePoint=TRUE, 
														FALSE)											 //	BOOL bIgnoreGlobalExclusionZones=FALSE)
						SET_PLAYER_WILL_SPAWN_FACING_COORDS(vEntityCoords, TRUE, FALSE)
					ENDIF
				ENDIF
			BREAK
			CASE GB_ASSAULT_ALTRUIST
			
			CASE GB_ASSAULT_MERRYWEATHER
			
				#IF IS_DEBUG_BUILD
				IF iDebugSpawn != 0
					iDebugSpawn = 0
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GB_ASSAULT_SPAWNING] UPDATE iDebugSpawn = 0 - SPAWNING NEAR PACKAGE vPlayerCoords = ", vPlayerCoords, " vEntityCoords = ", vEntityCoords)
				ENDIF
				#ENDIF
					
				//-- Spawn near package
				SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_NEAR_SPECIFIC_COORDS)	
				
				SETUP_SPECIFIC_SPAWN_LOCATION(vEntityCoords, // VECTOR vCoords, 
												0.0, 											 //	FLOAT fHeading, 
												250.0, 											 //	FLOAT fSearchRadius=100.0, 
												TRUE, 											 //	BOOL bDoVisibleChecks=TRUE, 
												FMEVENT_MINIMUM_SPAWN_DISTANCE_FROM_ACTION, 	 //	FLOAT fMinDistFromCoords=0.0, 
												FALSE, 											 //	BOOL bConsiderInteriors = FALSE, 
												TRUE, 											 //	BOOL bDoNearARoadChecks=TRUE, 
												65, 											 //	FLOAT MinDistFromOtherPlayers=65.0, 
												TRUE, 											 //	BOOL bNearCentrePoint=TRUE, 
												FALSE)											 //	BOOL bIgnoreGlobalExclusionZones=FALSE)
												
				SET_PLAYER_WILL_SPAWN_FACING_COORDS(vEntityCoords, TRUE, FALSE)
			BREAK
		ENDSWITCH	
	ELSE	
		//-- Player not valid for spawn coords
		
		IF IS_BIT_SET(iBoolsBitSet, biL_SetupCustomSpawns)
			CLEAR_BIT(iBoolsBitSet, biL_SetupCustomSpawns)
			CLEAR_CUSTOM_SPAWN_POINTS()
			USE_CUSTOM_SPAWN_POINTS(FALSE)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GB_ASSAULT_SPAWNING] CLEAR biL_SetupCustomSpawns AS PLAYER NOT VALID FOR SPAWN COORDS")
		ENDIF
				
		IF IS_BIT_SET(iBoolsBitSet, biL_SetupSpawnCoord)
			CLEAR_PLAYER_NEXT_RESPAWN_LOCATION()
			CLEAR_SPECIFIC_SPAWN_LOCATION_FROM_THIS_SCRIPT()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF iDebugSpawn != -1
			iDebugSpawn = -1
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GB_ASSAULT_SPAWNING] UPDATE iDebugSpawn = -1 - RESET vPlayerCoords = ", vPlayerCoords, " vEntityCoords = ", vEntityCoords)
		ENDIF
		#ENDIF
	ENDIF
ENDPROC

FUNC TEXT_LABEL_31 GET_RIVAL_INTRO_SHARD_TEXT(BOOL bIsGangMember)
	TEXT_LABEL_31 tl31Obj
	
	IF bIsGangMember
		SWITCH GET_GB_ASSAULT_VARIATION()
			CASE GB_ASSAULT_ALTRUIST		tl31Obj	= "BIGM_ASLTDR"						BREAK // Altruist
			CASE GB_ASSAULT_ZANCUDO			tl31Obj	= "BIGM_ASLV"						BREAK // Zancudo
			CASE GB_ASSAULT_AIRPORT			tl31Obj	= "BIGM_ASLTDR"						BREAK // Airport	
			CASE GB_ASSAULT_MERRYWEATHER	tl31Obj	= "BIGM_ASLTDR"						BREAK // Merryweather
		ENDSWITCH
	ELSE
		SWITCH GET_GB_ASSAULT_VARIATION()
			CASE GB_ASSAULT_ALTRUIST		tl31Obj	= "BIGM_ASLTDO"						BREAK // Altruist
			CASE GB_ASSAULT_ZANCUDO			tl31Obj	= "BIGM_ASLVO"						BREAK // Zancudo
			CASE GB_ASSAULT_AIRPORT			tl31Obj	= "BIGM_ASLTDO"						BREAK // Airport	
			CASE GB_ASSAULT_MERRYWEATHER	tl31Obj	= "BIGM_ASLTDO"						BREAK // Merryweather
		ENDSWITCH
	ENDIF
	
	RETURN tl31Obj
ENDFUNC

FUNC TEXT_LABEL_31 GET_INTRO_SHARD_TEXT()
	TEXT_LABEL_31 tl31Obj
	

	SWITCH GET_GB_ASSAULT_VARIATION()
		CASE GB_ASSAULT_ALTRUIST		tl31Obj	= "BIGM_ASLTD"						BREAK // Altruist
		CASE GB_ASSAULT_ZANCUDO			tl31Obj	= "BIGM_ASLDV"						BREAK // Zancudo
		CASE GB_ASSAULT_AIRPORT			tl31Obj	= "BIGM_ASLTD"						BREAK // Airport	
		CASE GB_ASSAULT_MERRYWEATHER	tl31Obj	= "BIGM_ASLTD"						BREAK // Merryweather
	ENDSWITCH

	
	
	RETURN tl31Obj
ENDFUNC

/// PURPOSE:
///    	Rival gangs will join when someone initially collects the package
PROC MAINTAIN_PLAYER_JOINING_AS_RIVAL()
	IF GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
		EXIT
	ENDIF
	
	IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) = GB_UI_LEVEL_NONE
		EXIT
	ENDIF
	
	TEXT_LABEL_31 tlIntroShard
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_JoinedAsRival)
		IF NOT DID_MY_BOSS_LAUNCH_GB_ASSAULT()
		//	IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
				IF serverBD.iPartPackageHolder != -1
				OR serverBD.iPartInVeh != -1
				OR IS_BIT_SET(serverBD.iServerBitSet, biS_CollectedAtLeastOnce)
				OR IS_BIT_SET(serverBD.iServerBitSet, biS_ForceRivalsOnMission)
					#IF IS_DEBUG_BUILD
					IF IS_BIT_SET(serverBD.iServerBitSet, biS_ForceRivalsOnMission)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [MAINTAIN_PLAYER_JOINING_AS_RIVAL] biS_ForceRivalsOnMission IS SET!")
					ENDIF
					#ENDIF
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_JoinedAsRival)
					

					SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(TRUE)
					IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niPackage)
						PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.niPackage), FALSE, TRUE)
					ENDIF
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(TRUE) / PREVENT_COLLECTION_OF_PORTABLE_PICKUP 1")

					STRING sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
					HUD_COLOURS hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
							
					IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
						tlIntroShard = GET_RIVAL_INTRO_SHARD_TEXT(TRUE)
						IF SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD()
							IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) > GB_UI_LEVEL_NONE
								SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_START_OF_JOB, "BIGM_ASLTN", tlIntroShard, sOrganization, hclPlayer) 
								
							ENDIF
						ENDIF
						
						GB_SET_COMMON_TELEMETRY_DATA_ON_START()
					ELSE
						tlIntroShard = GET_RIVAL_INTRO_SHARD_TEXT(FALSE)
						IF SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD()
							IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) > GB_UI_LEVEL_NONE
								SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_START_OF_JOB, "BIGM_ASLTN", tlIntroShard, sOrganization, hclPlayer) 
							ENDIF
						ENDIF
						
						GB_SET_COMMON_TELEMETRY_DATA_ON_START()
						
					ENDIF
					
					
					
					
					Block_All_MissionsAtCoords_Missions(TRUE)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [MAINTAIN_PLAYER_JOINING_AS_RIVAL] SET biP_JoinedAsRival - Block_All_MissionsAtCoords_Missions(TRUE)")
				ENDIF
		//	ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC ADD_BLIP_FOR_PACKAGE()
	IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) < GB_UI_LEVEL_BACKGROUND
		IF DOES_BLIP_EXIST(blipPackage)
			REMOVE_BLIP(blipPackage)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [ADD_BLIP_FOR_PACKAGE] REMOVE PACKAGE BLIP AS GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) = ", GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()))
		ENDIF
		
		EXIT
	ENDIF
	
	IF DOES_BLIP_EXIST(blipPackage)
		EXIT
	ENDIF
	
	
	
	
	blipPackage = ADD_BLIP_FOR_ENTITY(NET_TO_ENT(serverBD.niPackage))
	
	
	SET_BLIP_PRIORITY(blipPackage, BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
	SET_BLIP_SPRITE(blipPackage,  RADAR_TRACE_ASSAULT_PACKAGE)
	SET_BLIP_EXTENDED_HEIGHT_THRESHOLD(blipPackage, TRUE)
	SHOW_HEIGHT_ON_BLIP(blipPackage, TRUE)
	SET_BLIP_SCALE(blipPackage,g_sMPTunables.fgangboss_Job_blip_scale)
	SET_BLIP_NAME_FROM_TEXT_FILE(blipPackage, "GB_AST_BLIP")
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_AiPackageHolderKilled)
		IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
			SET_BLIP_COLOUR_FROM_HUD_COLOUR(blipPackage, HUD_COLOUR_GREEN)
		ELSE
			SET_BLIP_COLOUR_FROM_HUD_COLOUR(blipPackage, HUD_COLOUR_GREEN)
		ENDIF
	ELSE
		SET_BLIP_COLOUR_FROM_HUD_COLOUR(blipPackage, HUD_COLOUR_RED)
	ENDIF
	
//	SET_BLIP_ROUTE(blipPackage, TRUE)
	IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
		IF NOT IS_BIT_SET(iBoolsBitSet, biL_FlashedBlip)
			SET_BLIP_FLASHES(blipPackage, TRUE)
			SET_BLIP_FLASH_TIMER(blipPackage, 7000)
			FLASH_MINIMAP_DISPLAY()
			SET_BIT(iBoolsBitSet, biL_FlashedBlip)
			
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] [ADD_BLIP_FOR_PACKAGE] FLASHED PACKAGE BLIP / RADAR")
		ENDIF
	ENDIF
	
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] [ADD_BLIP_FOR_PACKAGE] ADDED BLIP")
ENDPROC

PROC ADD_BLIP_FOR_VEHICLE()
	IF DOES_BLIP_EXIST(blipVeh)
		EXIT
	ENDIF
	
	IF NOT DID_MY_BOSS_LAUNCH_GB_ASSAULT()
	AND NOT DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
		EXIT
	ENDIF
	
	IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) < GB_UI_LEVEL_BACKGROUND
		IF DOES_BLIP_EXIST(blipVeh)
			REMOVE_BLIP(blipVeh)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [ADD_BLIP_FOR_VEHICLE] REMOVE VEH BLIP AS GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) = ", GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()))
		ENDIF
		
		EXIT
	ENDIF
	
	
	blipVeh = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(serverBD.niVeh[0]))
	
	SET_BLIP_EXTENDED_HEIGHT_THRESHOLD(blipVeh, TRUE)
	SHOW_HEIGHT_ON_BLIP(blipVeh, TRUE)
	SET_BLIP_PRIORITY(blipVeh, BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
	SET_BLIP_SPRITE(blipVeh,   RADAR_TRACE_TANK)
	SET_BLIP_SCALE(blipVeh,g_sMPTunables.fgangboss_Job_blip_scale)
	SET_BLIP_NAME_FROM_TEXT_FILE(blipVeh, "GB_AST_RHN")
	
	IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
		SET_BLIP_COLOUR_FROM_HUD_COLOUR(blipVeh, HUD_COLOUR_BLUE)
	ELSE
		SET_BLIP_COLOUR_FROM_HUD_COLOUR(blipVeh, HUD_COLOUR_RED)
	ENDIF
	
	
	
//	SET_BLIP_ROUTE(blipVeh, TRUE)
	IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
		IF NOT IS_BIT_SET(iBoolsBitSet, biL_FlashedBlip)
			SET_BLIP_FLASHES(blipVeh, TRUE)
			SET_BLIP_FLASH_TIMER(blipVeh, 7000)
			FLASH_MINIMAP_DISPLAY()
			SET_BIT(iBoolsBitSet, biL_FlashedBlip)
			
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [ADD_BLIP_FOR_VEHICLE] [ADD_BLIP_FOR_VEHICLE] FLASHED VEHICLE BLIP / RADAR")
		ENDIF
	ENDIF
	
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [ADD_BLIP_FOR_VEHICLE] [ADD_BLIP_FOR_VEHICLE] ADDED BLIP")
ENDPROC

PROC ADD_BLIP_FOR_START_LOCATION()
	
	IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) < GB_UI_LEVEL_BACKGROUND
		IF DOES_BLIP_EXIST(blipLoc)
			REMOVE_BLIP(blipLoc)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GET_TO_LOCATION_CLIENT] [ADD_BLIP_FOR_START_LOCATION] REMOVED BLIP AS UI LEVEL = ", ENUM_TO_INT(GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())))
		ENDIF
		
		EXIT
	ENDIF
	
	VECTOR vLoc = GET_LOCATION_BLIP_COORD()
	
	IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
		IF NOT ARE_VECTORS_EQUAL(vLoc, << 0.0, 0.0, 0.0>>)
			blipLoc = ADD_BLIP_FOR_COORD(vLoc)	
			SET_BLIP_ROUTE(blipLoc, TRUE)
			
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GET_TO_LOCATION_CLIENT] [ADD_BLIP_FOR_START_LOCATION] ADDED BLIP vLoc = ", vLoc, " VARIATION = ", GET_GB_ASSAULT_VARIATION())
			IF NOT IS_BIT_SET(iBoolsBitSet, biL_FlashedBlip)
				SET_BLIP_FLASHES(blipLoc, TRUE)
				SET_BLIP_FLASH_TIMER(blipLoc, 7000)
				FLASH_MINIMAP_DISPLAY()
				SET_BIT(iBoolsBitSet, biL_FlashedBlip)
				
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GET_TO_LOCATION_CLIENT] [ADD_BLIP_FOR_START_LOCATION] FLASHED PACKAGE BLIP / RADAR")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC PLAYER_INDEX GET_PLAYER_WITH_PACKAGE()
	PLAYER_INDEX playerPackage = INVALID_PLAYER_INDEX()
	INT iPartWithPackage = serverBD.iPartPackageHolder
	
	IF iPartWithPackage != -1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartWithPackage))
			playerPackage = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartWithPackage))
		ENDIF
	ENDIF
	
	RETURN playerPackage
ENDFUNC

FUNC PLAYER_INDEX GET_PLAYER_DRIVING_VEH()
	PLAYER_INDEX playerVeh = INVALID_PLAYER_INDEX()
	INT iPartInVeh = serverBD.iPartInVeh
	
	IF iPartInVeh != -1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartInVeh))
			playerVeh = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartInVeh))
		ENDIF
	ENDIF
	
	RETURN playerVeh
ENDFUNC

FUNC BOOL IS_PACKAGE_HOLDER_IN_MY_GANG()
	PLAYER_INDEX playerPackage = GET_PLAYER_WITH_PACKAGE()
	PLAYER_INDEX playerBoss
	IF playerPackage = INVALID_PLAYER_INDEX()
		RETURN FALSE
	ENDIF
	
	IF playerPackage = PLAYER_ID() 
		RETURN TRUE
	ENDIF
	
	playerBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
	IF playerBoss != INVALID_PLAYER_INDEX()
		IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerPackage, playerBoss)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VEHICLE_HOLDER_IN_MY_GANG()
	PLAYER_INDEX playerPackage = GET_PLAYER_DRIVING_VEH()
	PLAYER_INDEX playerBoss
	IF playerPackage = INVALID_PLAYER_INDEX()
		RETURN FALSE
	ENDIF
	
	IF playerPackage = PLAYER_ID() 
		RETURN TRUE
	ENDIF
	
	playerBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
	
	IF playerBoss != INVALID_PLAYER_INDEX()
		IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerPackage, playerBoss)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ENTITY_HOLDER_IN_MY_GANG()
	SWITCH GET_GB_ASSAULT_VARIATION()
		
		CASE GB_ASSAULT_ZANCUDO	
			RETURN (IS_VEHICLE_HOLDER_IN_MY_GANG())
			
		CASE GB_ASSAULT_ALTRUIST		
		CASE GB_ASSAULT_AIRPORT			
	//	CASE GB_ASSAULT_HUMANE			
		CASE GB_ASSAULT_MERRYWEATHER	
			RETURN (IS_PACKAGE_HOLDER_IN_MY_GANG())
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC
/// PURPOSE:
///    Listen for the player with the package entering the drop off 
PROC CHECK_FOR_LOCAL_PLAYER_DELIVERING_PACKAGE()
//	IF NOT DID_MY_BOSS_LAUNCH_GB_ASSAULT()
//		EXIT
//	ENDIF
	
	PLAYER_INDEX playerPackage = GET_PLAYER_WITH_PACKAGE()
	VECTOR vLocation = GET_PACKAGE_DROP_OFF_COORDS()
	
	INT iR, iG, iB, iA
	
	IF DOES_BLIP_EXIST(blipDropOff)
		GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
		
		DRAW_MARKER(	MARKER_CYLINDER,
						vLocation,
						<<0,0,0>>,
						<<0,0,0>>,
						<<0.6, 0.6, 1.0 >>,
						iR,
						iG,
						iB,
						100)
	ENDIF
	
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_PackageDelivered)
		IF playerPackage = PLAYER_ID()
			IF NOT IS_PED_INJURED(GET_PLAYER_PED(playerPackage))
				IF IS_ENTITY_AT_COORD( GET_PLAYER_PED(playerPackage), vLocation, <<LOCATE_SIZE_ANY_MEANS,LOCATE_SIZE_ANY_MEANS,LOCATE_SIZE_HEIGHT>>, FALSE)
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_PackageDelivered)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_ANY_PLAYER_DELIVERING_PACKAGE] I THINK THIS PLAYER ", GET_PLAYER_NAME(playerPackage), " DELIVBERED THE PACKAGE")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC
TIME_DATATYPE timeLeaveEvent
/// PURPOSE:
///    Listen for the player driving the vehicle entering the drop off 
PROC CHECK_FOR_LOCAL_PLAYER_DELIVERING_VEHICLE()

	
	PLAYER_INDEX playerVeh = GET_PLAYER_DRIVING_VEH()
	VECTOR vLocation = GET_PACKAGE_DROP_OFF_COORDS()
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niVeh[0])
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_VehDelivered)
				IF playerVeh = PLAYER_ID()
					IF NOT IS_PED_INJURED(GET_PLAYER_PED(playerVeh))
					//	IF NOT IS_BIT_SET(iBoolsBitSet, biL_ExitVeh)
							IF IS_ENTITY_AT_COORD( GET_PLAYER_PED(playerVeh), vLocation, <<LOCATE_SIZE_ANY_MEANS,LOCATE_SIZE_ANY_MEANS,LOCATE_SIZE_HEIGHT>>, TRUE)
								EMPTY_AND_LOCK_VEHICLE(NET_TO_VEH(serverBD.niVeh[0]), timeLeaveEvent)
								
								IF NETWORK_HAS_CONTROL_OF_ENTITY(NET_TO_VEH(serverBD.niVeh[0]))
									SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(NET_TO_VEH(serverBD.niVeh[0]), TRUE)
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_LOCAL_PLAYER_DELIVERING_VEHICLE] I HAVE LOCKED THE VEHICLE")
								ELSE	
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_LOCAL_PLAYER_DELIVERING_VEHICLE] I DON'T HAVE CONTROL!")
								ENDIF
								
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_VehDelivered)
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [CHECK_FOR_LOCAL_PLAYER_DELIVERING_VEHICLE] SET biP_VehDelivered")
								
							ENDIF
					//	ELSE
					//		
					//	ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC


FUNC BOOL HAS_PLAYER_REACHED_GB_ASSAULT_LOCATION()
	/*
	CONST_INT GB_ASSAULT_ALTRUIST			0
CONST_INT GB_ASSAULT_ZANCUDO			1
CONST_INT GB_ASSAULT_AIRPORT			2	
CONST_INT GB_ASSAULT_HUMANE				3
CONST_INT GB_ASSAULT_MERRYWEATHER		4
	*/
	INT iVar = GET_GB_ASSAULT_VARIATION()

	VECTOR vPlayerCoords
	
	SWITCH iVar
		CASE GB_ASSAULT_ALTRUIST
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
				IF GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, GET_LOCATION_FOR_VARIATION()) < 121.0
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE GB_ASSAULT_ZANCUDO
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
				IF IS_COORD_IN_SPECIFIED_AREA(vPlayerCoords, AC_MILITARY_BASE, 1000 )
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE GB_ASSAULT_AIRPORT
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
				IF IS_COORD_IN_SPECIFIED_AREA(vPlayerCoords, AC_AIRPORT_AIRSIDE, 1000 )
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
//		CASE GB_ASSAULT_HUMANE
//			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//				vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
//				IF IS_COORD_IN_SPECIFIED_AREA(vPlayerCoords, AC_BIOTECH, 1000 )
//					RETURN TRUE
//				ENDIF
//			ENDIF
//		BREAK
		
		CASE GB_ASSAULT_MERRYWEATHER
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
				IF GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, GET_LOCATION_FOR_VARIATION()) < 200.0
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_WAIT_FOR_PACKAGE_BEFORE_GIVING_WANTED()
	SWITCH GET_GB_ASSAULT_VARIATION()
		CASE GB_ASSAULT_ALTRUIST	
		CASE GB_ASSAULT_MERRYWEATHER
		
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_WAIT_FOR_AI_ENGAGED_BEFORE_GIVING_WANTED()
	SWITCH GET_GB_ASSAULT_VARIATION()
		CASE GB_ASSAULT_AIRPORT
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC	

FUNC BOOL HAVE_AI_PEDS_BEEN_ENGAGED()
	RETURN(IS_BIT_SET(serverBD.iServerBitSet, biS_TargetsApproached)
		OR IS_BIT_SET(serverBD.iServerBitSet, biS_ShootNearTarget)
		OR IS_BIT_SET(serverBD.iServerBitSet, biS_AimingAtTargets)
		OR IS_BIT_SET(serverBD.iServerBitSet, biS_DamagedTargets))
ENDFUNC


/// PURPOSE:
///    Keep wanted level after respawning after death
PROC MAINTAIN_GB_ASSAULT_WANTED_LEVEL()
	
	IF NOT DID_MY_BOSS_LAUNCH_GB_ASSAULT()
	AND NOT DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
		EXIT
	ENDIF
	
	IF NOT SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD()
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(iBoolsBitSet, biL_ResetMaxWanted)	
		IF serverBD.eStage > eGB_GET_TO_LOCATION
			
			
			IF HAS_PLAYER_REACHED_GB_ASSAULT_LOCATION()
				IF NOT SHOULD_WAIT_FOR_PACKAGE_BEFORE_GIVING_WANTED()
					IF NOT SHOULD_WAIT_FOR_AI_ENGAGED_BEFORE_GIVING_WANTED()
						SET_MAX_WANTED_LEVEL( GET_NUMBER_OF_WANTED_STARS_FOR_VARIATION())
						SET_BIT(iBoolsBitSet, biL_ResetMaxWanted)
						SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), GET_NUMBER_OF_WANTED_STARS_FOR_VARIATION())
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GB_ASSAULT_WANTED_LEVEL] SET WANTED RATING AS HAS_PLAYER_REACHED_GB_ASSAULT_LOCATION ", GET_NUMBER_OF_WANTED_STARS_FOR_VARIATION())
					ELSE
						IF HAVE_AI_PEDS_BEEN_ENGAGED()
							SET_MAX_WANTED_LEVEL( GET_NUMBER_OF_WANTED_STARS_FOR_VARIATION())
							SET_BIT(iBoolsBitSet, biL_ResetMaxWanted)
							SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), GET_NUMBER_OF_WANTED_STARS_FOR_VARIATION())
							SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GB_ASSAULT_WANTED_LEVEL] SET WANTED RATING AS HAVE_AI_PEDS_BEEN_ENGAGED ", GET_NUMBER_OF_WANTED_STARS_FOR_VARIATION())
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
	//		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GB_ASSAULT_WANTED_LEVEL] SET biL_ResetMaxWanted")
		ENDIF
	ELSE
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
			IF HAS_PLAYER_REACHED_GB_ASSAULT_LOCATION()
				UPDATE_WANTED_POSITION_THIS_FRAME(PLAYER_ID())
				SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID())
			ENDIF
		ENDIF
	ENDIF
	
	
	IF serverBD.iPartPackageHolder != -1
		IF NOT IS_BIT_SET(iBoolsBitSet, biL_GivenWantedNearPack)
			IF serverBD.iPartPackageHolder = PARTICIPANT_ID_TO_INT()
				SET_MAX_WANTED_LEVEL( GET_NUMBER_OF_WANTED_STARS_FOR_VARIATION())
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), GET_NUMBER_OF_WANTED_STARS_FOR_VARIATION())
				SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
				SET_BIT(iBoolsBitSet, biL_GivenWantedNearPack)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GB_ASSAULT_WANTED_LEVEL] SET biL_GivenWantedNearPack - I HAVE PACKAGE WANTED LEVEL GIVEN ", GET_NUMBER_OF_WANTED_STARS_FOR_VARIATION())
			ELSE
				IF IS_PACKAGE_HOLDER_IN_MY_GANG()
					PLAYER_INDEX playerPackage = GET_PLAYER_WITH_PACKAGE()
					IF playerPackage != INVALID_PLAYER_INDEX()
						IF NOT IS_BIT_SET(iBoolsBitSet, biL_ResetMaxWanted)
							SET_MAX_WANTED_LEVEL( GET_NUMBER_OF_WANTED_STARS_FOR_VARIATION())
							SET_BIT(iBoolsBitSet, biL_ResetMaxWanted)
						ENDIF
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF NOT IS_PED_INJURED(GET_PLAYER_PED(playerPackage))
								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), GET_PLAYER_PED(playerPackage)) <= 200.0
									
									SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), GET_NUMBER_OF_WANTED_STARS_FOR_VARIATION())
									SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
									SET_BIT(iBoolsBitSet, biL_GivenWantedNearPack)
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GB_ASSAULT_WANTED_LEVEL] SET biL_GivenWantedNearPack - I'M NEAR PACKAGE HOLDER ", GET_PLAYER_NAME(playerPackage), " WANTED LEVEL GIVEN ", GET_NUMBER_OF_WANTED_STARS_FOR_VARIATION())
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_PACKAGE_HOLDER_IN_MY_GANG()
				CLEAR_BIT(iBoolsBitSet, biL_GivenWantedNearPack)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GB_ASSAULT_WANTED_LEVEL] CLEAR biL_GivenWantedNearPack - PACKAGE HOLDER NOT IN MY GANG")
			ENDIF
	
		ENDIF
	ELSE
		IF IS_BIT_SET(iBoolsBitSet, biL_GivenWantedNearPack)
			CLEAR_BIT(iBoolsBitSet, biL_GivenWantedNearPack)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GB_ASSAULT_WANTED_LEVEL] CLEAR biL_GivenWantedNearPack - iPartPackageHolder = -1")
		ENDIF
	ENDIF
	
	
	INT iTempWanted
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF NOT IS_BIT_SET(iBoolsBitSet, biL_PlayerDied)
			iTempWanted = GET_PLAYER_WANTED_LEVEL(PLAYER_ID())
			IF iMyCurrentWantedLevel != iTempWanted
				iMyCurrentWantedLevel = iTempWanted
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GB_ASSAULT_WANTED_LEVEL] iMyCurrentWantedLevel UPDATED TO ", iMyCurrentWantedLevel)
			ENDIF
			
			IF iTempWanted > 0
				IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
					FLOAT fKeepWantedDist
					fKeepWantedDist = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_LOCATION_FOR_VARIATION())
					IF fKeepWantedDist < 150.0
						UPDATE_WANTED_POSITION_THIS_FRAME(PLAYER_ID())
						SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID())
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
				IF iMyCurrentWantedLevel > 0
					SET_MAX_WANTED_LEVEL( GET_NUMBER_OF_WANTED_STARS_FOR_VARIATION())
					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), iMyCurrentWantedLevel)
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
				ENDIF
				
				CLEAR_BIT(iBoolsBitSet, biL_PlayerDied)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GB_ASSAULT_WANTED_LEVEL] CLEAR biL_PlayerDied iMyCurrentWantedLevel = ", iMyCurrentWantedLevel)
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(iBoolsBitSet, biL_PlayerDied)
			SET_BIT(iBoolsBitSet, biL_PlayerDied)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GB_ASSAULT_WANTED_LEVEL] SET biL_PlayerDied, iMyCurrentWantedLevel = ", iMyCurrentWantedLevel) 
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Put permanent participant's previous weapon back in their hnd after they die
PROC MAINTAIN_GB_ASSAULT_RESTORE_WEAPON()
	IF NOT DID_MY_BOSS_LAUNCH_GB_ASSAULT()
	AND NOT DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
		EXIT
	ENDIF
	
	IF NOT GB_IS_GLOBAL_CLIENT_BIT0_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_0_PERMANENT_PARTICIPANT)
		EXIT
	ENDIF
	
	WEAPON_TYPE weaponPlayerTemp
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_BIT_SET(iBoolsBitSet, biL_ResetWeapon)
			IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), weaponPlayer)
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), weaponPlayer)
				CLEAR_BIT(iBoolsBitSet, biL_ResetWeapon)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GB_ASSAULT_RESTORE_WEAPON] CLEAR biL_ResetWeapon RESTORED MY WEAPON")
			ELSE
				CLEAR_BIT(iBoolsBitSet, biL_ResetWeapon)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GB_ASSAULT_RESTORE_WEAPON] WANT TO RESTORE WEAPON BUT DON'T HAVE WEAPON!")
			ENDIF
		ELSE
			IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), weaponPlayerTemp)
				IF weaponPlayer != weaponPlayerTemp
					weaponPlayer = weaponPlayerTemp
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GB_ASSAULT_RESTORE_WEAPON] weaponPlayer UPDATED AS PLAYER CHANGED WEAPON")
				ENDIF
			ENDIF
		ENDIF
	ELSE	
		IF NOT IS_BIT_SET(iBoolsBitSet, biL_ResetWeapon)
			SET_BIT(iBoolsBitSet, biL_ResetWeapon)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GB_ASSAULT_RESTORE_WEAPON] SET biL_ResetWeapon AS I'm DEAD")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Track local player getting to the package location
PROC MAINTAIN_GET_TO_LOCATION_CLIENT()
	//TEXT_LABEL_31 tlHelp
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_GotToLocation)
	
		//-- Blip
		IF NOT DOES_BLIP_EXIST(blipLoc)
			ADD_BLIP_FOR_START_LOCATION()
		ENDIF
		
		//--Objective text
		IF SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD()
			IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) > GB_UI_LEVEL_MINIMAL
				IF NOT Is_This_The_Current_Objective_Text("GB_ASLT_GO")
					Print_Objective_Text_With_String("GB_ASLT_GO", GET_TO_LOCATION_TEXT()) // Get to the ~y~location.
				ENDIF
			ELSE
				Clear_Any_Objective_Text_From_This_Script()
			ENDIF
		ELSE
			Clear_Any_Objective_Text_From_This_Script()
			
		ENDIF
		
		
		
		
		IF HAS_PLAYER_REACHED_GB_ASSAULT_LOCATION()
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_GotToLocation)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_GET_TO_LOCATION_CLIENT] SET biP_GotToLocation")
		ENDIF
	ENDIF
	
	
	MAINTAIN_TEXT_MESSAGE()
ENDPROC

PROC MAINTAIN_PLAYER_APPROACH_TARGETS()
	IF GET_GB_ASSAULT_VARIATION() != GB_ASSAULT_AIRPORT
		EXIT
	ENDIF
	
	INT i
	
	//-- Approaching targets?
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_TargetsApproached)
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ApproachedTargets)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-1271.592163,-3358.280273,12.070152>>, <<-1300.657959,-3408.515137,20.945152>>, 48.875000)
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ApproachedTargets)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PLAYER_APPROACH_TARGETS] I APPROACHED THE TARGETS")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//-- Shooting near targets?
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ShootNearTarget)
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ShotNearTargets)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_BULLET_IN_AREA( <<-1301.7042, -3398.3372, 13.0508>>, 50.0)
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ShotNearTargets)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PLAYER_APPROACH_TARGETS] I SHOT NEAR THE TARGETS")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//-- Aim at targets?
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AimingAtTargets)
		REPEAT MAX_GB_ASSAULT_PEDS i
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.niPed[i])
				IF NOT IS_NET_PED_INJURED(serverbd.niPed[i])
					IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_AimAtTargets)
						IF IS_PLAYER_AIMING_AT_PED(NET_TO_PED(serverbd.niPed[i]))
							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_AimAtTargets)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PLAYER_APPROACH_TARGETS] I AIMED AT PED ", i)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

PROC MAINTAIN_SUPPRESS_ASSAULT_SCENARIOS()
	VECTOR vPlayerCoords
	
	//-- 2593683
	IF GET_GB_ASSAULT_VARIATION() = GB_ASSAULT_ALTRUIST
		vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
		IF NOT IS_BIT_SET(iBoolsBitSet2, biL2_SHouldSuppressScenarios)
			IF GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, GET_LOCATION_FOR_VARIATION()) < 200.0
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
					SET_BIT(iBoolsBitSet2, biL2_SHouldSuppressScenarios)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_SUPPRESS_ASSAULT_SCENARIOS] SET biL2_SHouldSuppressScenarios")
				ENDIF
			ENDIF
		ELSE
			IF GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, GET_LOCATION_FOR_VARIATION()) < 121.0
				SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0, 0.0)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Draws a marker above the package when it has been dropped
PROC MAINTAIN_PACKAGE_MARKER()
	IF NOT SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD()
		EXIT
	ENDIF
	INT r, g, b, a
	
	IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_BACKGROUND
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niPackage)
		AND NOT IS_ENTITY_DEAD(NET_TO_OBJ(serverBD.niPackage))
			IF NOT IS_ENTITY_ATTACHED_TO_ANY_PED(NET_TO_OBJ(serverBD.niPackage))
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_PackageDelivered)
				AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_PackageDelivered)
					GET_HUD_COLOUR(HUD_COLOUR_GREEN, r, g, b, a)
					DRAW_MARKER(MARKER_ARROW, GET_ENTITY_COORDS(NET_TO_ENT(serverBD.niPackage))+<<0,0,1>>, <<0,0,0>>, <<180,0,0>>, <<0.5,0.5,0.5>>, r, g, b, 100, TRUE, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC MAINTAIN_PLAYER_WITH_PACKAGE_ON_YACHT()

//	IF iPackageOnYachtProg < 1
//		DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.14, "STRING", "NOT ON YACHT")
//	ELSE
//		DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.14, "STRING", "ON YACHT")
//	ENDIF
	
	VECTOR vPackageLoc
	BOOL bFindCoords
	
	SWITCH iPackageOnYachtProg
		CASE 0
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
				vPackageLoc = GET_ENTITY_COORDS(NET_TO_ENT(serverBD.niPackage))
				IF NOT ARE_VECTORS_EQUAL(vPackageAvoidSpawnCoords, <<0.0, 0.0, 0.0>>)
					vPackageAvoidSpawnCoords = <<0.0, 0.0, 0.0>>
				ENDIF
		//	IF serverBD.iPartPackageHolder = PARTICIPANT_ID_TO_INT()
			//	IF IS_PLAYER_ON_YACHT(PLAYER_ID(), iOnYachtCount)
				IF IS_POINT_IN_YACHT_BOUNDING_BOX(vPackageLoc, iOnYachtCount)
					iPackageOnYachtProg++
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [MAINTAIN_PLAYER_WITH_PACKAGE_ON_YACHT] PACKAGE ON A YACHT ", iOnYachtCount, " iPackageOnYachtProg = ", iPackageOnYachtProg)
				ENDIF
				
				IF iPackageOnYachtProg = 0
					iOnYachtCount++ 
				ENDIF
				
				IF iOnYachtCount >= NUMBER_OF_PRIVATE_YACHTS
					iOnYachtCount = 0
				ENDIF
			//	ENDIF
			ENDIF
		BREAK
		
		CASE 1
			
			IF serverBD.iPartPackageHolder = -1
				//-- Package isn't attached to anyone, whoever has control of it can warp it
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niPackage)
					bFindCoords = TRUE
					IF ARE_VECTORS_EQUAL(vPackageAvoidSpawnCoords, <<0.0, 0.0, 0.0>>)
						vPackageAvoidSpawnCoords = GET_ENTITY_COORDS(NET_TO_ENT(serverBD.niPackage))
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [MAINTAIN_PLAYER_WITH_PACKAGE_ON_YACHT] PACKAGE ON A YACHT COORDS (1) vPackageAvoidSpawnCoords = ", vPackageAvoidSpawnCoords)
					ENDIF
				ENDIF
			ELSE
				//-- SOmoene has the package. Whoever has it can warp iy
				IF serverBD.iPartPackageHolder = PARTICIPANT_ID_TO_INT()
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niPackage)
						IF ARE_VECTORS_EQUAL(vPackageAvoidSpawnCoords, <<0.0, 0.0, 0.0>>)
							vPackageAvoidSpawnCoords = GET_ENTITY_COORDS(NET_TO_ENT(serverBD.niPackage))
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [MAINTAIN_PLAYER_WITH_PACKAGE_ON_YACHT] PACKAGE ON A YACHT COORDS (2) vPackageAvoidSpawnCoords = ", vPackageAvoidSpawnCoords)
						ENDIF
					
						bFindCoords = TRUE
					ELSE
						NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.niPackage)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [MAINTAIN_PLAYER_WITH_PACKAGE_ON_YACHT] PACKAGE ATTACHED TO ME  - REQUESTING CONTROL ")
					ENDIF
				ENDIF
			ENDIF
			
			IF bFindCoords
				SPAWN_SEARCH_PARAMS SpawnSearchParams
				spawnSearchParams.bConsiderInteriors = FALSE
				SpawnSearchParams.fMinDistFromPlayer = 1.0
				SpawnSearchParams.vAvoidCoords[0] = vPackageAvoidSpawnCoords
				SpawnSearchParams.fAvoidRadius[0] = 8.0
				SpawnSearchParams.fAvoidRadius[0] = 100.0
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niPackage)
					IF GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY(vPackageAvoidSpawnCoords, 20.0, vWarpPackLoc, fWarpPackageHeading, SpawnSearchParams)
						vWarpPackLoc += << 0.0, 0.0, 0.5 >>
						IF IS_ENTITY_ATTACHED_TO_ENTITY(NET_TO_OBJ(serverBD.niPackage), PLAYER_PED_ID())
							DETACH_PORTABLE_PICKUP_FROM_PED(NET_TO_OBJ(serverBD.niPackage))
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [MAINTAIN_PLAYER_WITH_PACKAGE_ON_YACHT] PACKAGE ATTACHED TO ME - DETACHING")
						ENDIF
						SET_ENTITY_COORDS_NO_OFFSET(NET_TO_OBJ(serverBD.niPackage), vWarpPackLoc)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_OBJ(serverBD.niPackage), TRUE)
						
						SET_ENTITY_VISIBLE(NET_TO_OBJ(serverBD.niPackage), TRUE, TRUE)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [MAINTAIN_PLAYER_WITH_PACKAGE_ON_YACHT] I'VE WARPED PACKAGE TO ", vWarpPackLoc)
						
						SET_BIT(iBoolsBitSet2, biL2_DoYachtWarpHelp)
						iPackageOnYachtProg = 0
					ENDIF
				ENDIF
											
			ELSE
				IF serverBD.iPartPackageHolder != PARTICIPANT_ID_TO_INT() 
					vPackageLoc = GET_ENTITY_COORDS(NET_TO_ENT(serverBD.niPackage))
					IF NOT IS_POINT_IN_YACHT_BOUNDING_BOX(vPackageLoc, iOnYachtCount)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [MAINTAIN_PLAYER_WITH_PACKAGE_ON_YACHT] IM NOT LOOKING FOR NEW COORDS BUT PACKAGE NO LONGER ON YACHT ID ", iOnYachtCount)
						iPackageOnYachtProg = 0
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
		BREAK
	ENDSWITCH
	
	IF IS_BIT_SET(iBoolsBitSet2, biL2_DoYachtWarpHelp)
		
			IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
		//		SET_BIT(iBoolsBitSet2, biL2_DoneYachtWarpHelp)
				CLEAR_BIT(iBoolsBitSet2, biL2_DoYachtWarpHelp)
				PRINT_HELP("GB_HLP_WRP") // The briefcase will be removed if you climb aboard a Yacht.
			ELSE
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT [MAINTAIN_PLAYER_WITH_PACKAGE_ON_YACHT NOT IS_OK_TO_PRINT_FREEMODE_HELP biL2_DoneYachtWarpHelp")
			ENDIF
		//ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_VEHICLE_MARKER()
	IF NOT SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD()
		EXIT
	ENDIF
	
	IF NOT DID_MY_BOSS_LAUNCH_GB_ASSAULT()
	AND NOT DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
		EXIT
	ENDIF
	
	INT iR, iG, iB, iA
	
	PLAYER_INDEX playerBoss
	PLAYER_INDEX playerVeh
	
	BOOL bDoMarker = FALSE
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[0])
			IF serverBD.iPartInVeh = -1
				IF NOT DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
					GET_HUD_COLOUR(HUD_COLOUR_BLUE, iR, iG, iB, iA)
				ELSE
					GET_HUD_COLOUR(HUD_COLOUR_RED, iR, iG, iB, iA)
				ENDIF
				bDoMarker = TRUE
			ELSE	
				playerVeh = GET_PLAYER_DRIVING_VEH()
				IF playerVeh != INVALID_PLAYER_INDEX()
					IF NOT GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
						GET_HUD_COLOUR(HUD_COLOUR_RED, iR, iG, iB, iA)
						bDoMarker = TRUE
					ELSE
						
						playerBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
						IF (playerBoss != INVALID_PLAYER_INDEX() 
						AND NOT GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerVeh, playerBoss))
							GET_HUD_COLOUR(HUD_COLOUR_RED, iR, iG, iB, iA)
							bDoMarker = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF bDoMarker
				FM_EVENTDRAW_MARKER_ABOVE_VEHICLE(NET_TO_VEH(serverBD.niVeh[0]), iR, iG, iB)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_DISTANCE_CHECKS_FOR_VARIATION()
	IF NOT DID_MY_BOSS_LAUNCH_GB_ASSAULT()
	AND NOT DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
		EXIT
	ENDIF
	
	VECTOR vAction
	
	IF GET_GB_ASSAULT_VARIATION() = GB_ASSAULT_ZANCUDO
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])
			vAction = GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niVeh[0]), FALSE)
		ENDIF
	ELSE
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
			vAction = GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niPackage), FALSE)
		ENDIF
	ENDIF
	
	IF NOT ARE_VECTORS_EQUAL(vAction, <<0.0, 0.0, 0.0>>)
		GB_MAINTAIN_BOSS_WORK_DISTANT_CHECKS(FMMC_TYPE_GB_ASSAULT, vAction, bDoneDistanceCheck)
	ENDIF
ENDPROC

/// PURPOSE:
///    Track who is holding the package, client side
PROC MAINTAIN_PACKAGE_CLIENT()
	PLAYER_INDEX playerPackage
	PLAYER_INDEX playerBoss
	
	PARTICIPANT_INDEX partPackage
	
	IF serverBD.iVariation = GB_ASSAULT_ZANCUDO
		//-- No packages
		EXIT
	ENDIF
	
	IF NOT SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD()
		IF iLocalPartPackageHolder != -2
			IF iLocalPartPackageHolder > -1
				partPackage = INT_TO_PARTICIPANTINDEX(iLocalPartPackageHolder)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(partPackage)
					playerPackage = NETWORK_GET_PLAYER_INDEX(partPackage)
					IF playerPackage != PLAYER_ID()
						SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(playerPackage, RADAR_TRACE_ASSAULT_PACKAGE, FALSE)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] RESET PLAYER BLIP FOR PLAYER ", GET_PLAYER_NAME(playerPackage), " AS SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD")
						IF NOT DID_MY_BOSS_LAUNCH_GB_ASSAULT() 
							SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(playerPackage, BLIP_COLOUR_RED, FALSE)
							FORCE_BLIP_PLAYER(playerPackage, FALSE, FALSE)
							SET_PLAYER_BLIP_AS_LONG_RANGE(playerPackage, FALSE)
							SET_FIXED_BLIP_SCALE_FOR_PLAYER(playerPackage, g_sMPTunables.fgangboss_Job_blip_scale, FALSE)
						ELSE
							IF serverbd.iBossPlayerWhoLaunched > -1
								playerBoss = INT_TO_PLAYERINDEX(serverbd.iBossPlayerWhoLaunched)
								IF NETWORK_IS_PLAYER_ACTIVE(playerBoss)
									IF NOT GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerPackage, playerBoss)
										SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(playerPackage, BLIP_COLOUR_RED, FALSE)
										FORCE_BLIP_PLAYER(playerPackage, FALSE, FALSE)
										SET_PLAYER_BLIP_AS_LONG_RANGE(playerPackage, FALSE)
										SET_FIXED_BLIP_SCALE_FOR_PLAYER(playerPackage, g_sMPTunables.fgangboss_Job_blip_scale, FALSE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(blipPackage)
					REMOVE_BLIP(blipPackage)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] REMOVED VEH BLIP AS SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD")
				ENDIF
			ENDIF
			
			IF DOES_BLIP_EXIST(blipDropOff)
				REMOVE_BLIP(blipDropOff)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] REMOVED blipDropOff AS SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD")
			ENDIF
			iLocalPartPackageHolder = -2
			Clear_Any_Objective_Text_From_This_Script()
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] Clear_Any_Objective_Text_From_This_Script as NOT SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD")
		ENDIF
		
		EXIT
	ENDIF
	
	INT iR, iG, iB, iA
	
	//-- My local copy of who has the package
	IF iLocalPartPackageHolder != serverBD.iPartPackageHolder 
		IF serverBD.iPartPackageHolder = -1
			IF SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD()
				IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_BACKGROUND
					//-- Remove player blip if no-one has the package, and someone previously had it
					partPackage = INT_TO_PARTICIPANTINDEX(iLocalPartPackageHolder)
					IF NETWORK_IS_PARTICIPANT_ACTIVE(partPackage)
						playerPackage = NETWORK_GET_PLAYER_INDEX(partPackage)
						IF playerPackage != PLAYER_ID()
							SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(playerPackage, RADAR_TRACE_ASSAULT_PACKAGE, FALSE)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] RESET PLAYER BLIP FOR PLAYER ", GET_PLAYER_NAME(playerPackage))
							IF NOT DID_MY_BOSS_LAUNCH_GB_ASSAULT()
								SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(playerPackage, BLIP_COLOUR_RED, FALSE)
								FORCE_BLIP_PLAYER(playerPackage, FALSE, FALSE)
								SET_PLAYER_BLIP_AS_LONG_RANGE(playerPackage, FALSE)
								SET_FIXED_BLIP_SCALE_FOR_PLAYER(playerPackage, g_sMPTunables.fgangboss_Job_blip_scale, FALSE)
							ELSE
								IF serverbd.iBossPlayerWhoLaunched > -1
									playerBoss = INT_TO_PLAYERINDEX(serverbd.iBossPlayerWhoLaunched)
									IF NETWORK_IS_PLAYER_ACTIVE(playerBoss)
										IF NOT GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerPackage, playerBoss)
											SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(playerPackage, BLIP_COLOUR_RED, FALSE)
											FORCE_BLIP_PLAYER(playerPackage, FALSE, FALSE)
											SET_PLAYER_BLIP_AS_LONG_RANGE(playerPackage, FALSE)
											SET_FIXED_BLIP_SCALE_FOR_PLAYER(playerPackage, g_sMPTunables.fgangboss_Job_blip_scale, FALSE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
					ENDIF
					
					IF IS_BIT_SET(serverBD.iServerBitSet, biS_AiPackageHolderKilled)
						PRINT_TICKER("GB_AST_TCKD")  // The package was dropped.
					ENDIF
				ENDIF
			ENDIF
		ELSE
			//-- Deal with package holder updating without ever being -1
			IF iLocalPartPackageHolder != -1
				IF iLocalPartPackageHolder > -1
					partPackage = INT_TO_PARTICIPANTINDEX(iLocalPartPackageHolder)
					IF NETWORK_IS_PARTICIPANT_ACTIVE(partPackage)
						playerPackage = NETWORK_GET_PLAYER_INDEX(partPackage)
						IF playerPackage != PLAYER_ID()
							IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_BACKGROUND
								SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(playerPackage, RADAR_TRACE_ASSAULT_PACKAGE, FALSE)
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] RESET PLAYER BLIP (2) FOR PLAYER ", GET_PLAYER_NAME(playerPackage))
								IF NOT DID_MY_BOSS_LAUNCH_GB_ASSAULT()
									SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(playerPackage, BLIP_COLOUR_RED, FALSE)
									FORCE_BLIP_PLAYER(playerPackage, FALSE, FALSE)
									SET_PLAYER_BLIP_AS_LONG_RANGE(playerPackage, FALSE)
									SET_FIXED_BLIP_SCALE_FOR_PLAYER(playerPackage, g_sMPTunables.fgangboss_Job_blip_scale, FALSE)
								ELSE
								//	SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(playerPackage, hcMyGangColour, FALSE)
								ENDIF
							ENDIF
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF
					
			partPackage = INT_TO_PARTICIPANTINDEX(serverBD.iPartPackageHolder)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(partPackage)
				IF SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD()
					IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_BACKGROUND
						//-- Blip package holder

						playerPackage = NETWORK_GET_PLAYER_INDEX(partPackage)
						
						IF IS_NET_PLAYER_OK(playerPackage)
							IF DOES_BLIP_EXIST(blipPackage)
								REMOVE_BLIP(blipPackage)
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] REMOVE PACKAGE BLIP AS ABOUT TO BLIP PLAYER (UNLESS ITS ME) ", GET_PLAYER_NAME(playerPackage))
							ENDIF
							
							IF playerPackage != PLAYER_ID()
								IF NOT DID_MY_BOSS_LAUNCH_GB_ASSAULT()
									IF NOT IS_PACKAGE_HOLDER_IN_MY_GANG()
										//-- I'm a rival gang / player, package holder isn't in my gang
										SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(playerPackage, RADAR_TRACE_ASSAULT_PACKAGE, TRUE)
										FORCE_BLIP_PLAYER(playerPackage, TRUE, TRUE)
										SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(playerPackage, BLIP_COLOUR_RED, TRUE)
										SET_PLAYER_BLIP_AS_LONG_RANGE(playerPackage, TRUE)
										SET_FIXED_BLIP_SCALE_FOR_PLAYER(playerPackage, g_sMPTunables.fgangboss_Job_blip_scale, TRUE)
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] MY BOSS DIDN'T LAUNCH AND PACKAGE HOLDER NOT IN MY GANG SET PACKAGE PLAYER BLIP FOR PLAYER ", GET_PLAYER_NAME(playerPackage))
									ELSE
										SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(playerPackage, RADAR_TRACE_ASSAULT_PACKAGE, TRUE)
										SET_FIXED_BLIP_SCALE_FOR_PLAYER(playerPackage, g_sMPTunables.fgangboss_Job_blip_scale, TRUE)
									//	SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(playerPackage, hcMyGangColour, TRUE)
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] MY BOSS DIDN'T LAUNCH AND PACKAGE HOLDER IN MY GANG SET PACKAGE PLAYER BLIP FOR PLAYER ", GET_PLAYER_NAME(playerPackage))
									ENDIF
								ELSE
									IF IS_PACKAGE_HOLDER_IN_MY_GANG()
										SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(playerPackage, RADAR_TRACE_ASSAULT_PACKAGE, TRUE)
										SET_FIXED_BLIP_SCALE_FOR_PLAYER(playerPackage, g_sMPTunables.fgangboss_Job_blip_scale, TRUE)
									//	SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(playerPackage, hcMyGangColour, TRUE)
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] MY BOSS LAUNCHED AND PACKAGE HOLDER IN MY GANG SET PACKAGE PLAYER BLIP FOR PLAYER ", GET_PLAYER_NAME(playerPackage))
									ELSE
										SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(playerPackage, RADAR_TRACE_ASSAULT_PACKAGE, TRUE)
										FORCE_BLIP_PLAYER(playerPackage, TRUE, TRUE)
										SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(playerPackage, BLIP_COLOUR_RED, TRUE)
										SET_PLAYER_BLIP_AS_LONG_RANGE(playerPackage, TRUE)
										SET_FIXED_BLIP_SCALE_FOR_PLAYER(playerPackage, g_sMPTunables.fgangboss_Job_blip_scale, TRUE)
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] MY BOSS LAUNCHED BUT PACKAGE HOLDER NOT IN MY GANG - SET PACKAGE PLAYER BLIP FOR PLAYER ", GET_PLAYER_NAME(playerPackage))
									
									ENDIF
								ENDIF
							ENDIF
						
						
							IF playerPackage != PLAYER_ID()
								PRINT_TICKER_WITH_PLAYER_NAME("GB_AST_TCKC", playerPackage) // ~a~ collected the package.
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		iLocalPartPackageHolder = serverBD.iPartPackageHolder
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] iLocalPartPackageHolder UPDATED TO ", iLocalPartPackageHolder, " HOST IS ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_HOST_OF_THIS_SCRIPT())))
	ENDIF
	
	//-- Update whether or not I've got the package
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_GotThePackage)
				IF IS_ENTITY_ATTACHED_TO_ENTITY(NET_TO_OBJ(serverBD.niPackage), PLAYER_PED_ID())
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_GotThePackage)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] SET biP_GotThePackage")
					GB_SET_LOCAL_PLAYER_CRITICAL_TO_JOB(TRUE)
					
					GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
					IF DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
						GB_SET_PLAYER_AS_PERMANENT_ON_BOSS_MISSION()
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] SET eGB_GLOBAL_CLIENT_BITSET_0_PERMANENT_PARTICIPANT")
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_RivalGotThePackage)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] SET biP_RivalGotThePackage")
					ENDIF
					
					IF NOT IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
						SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY(TRUE)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY = TRUE")
					ENDIF
					
					IF IS_PED_IN_ANY_SUB(PLAYER_PED_ID())
						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niPackage)
							DETACH_ENTITY(NET_TO_OBJ(serverBD.niPackage))
							SET_BIT(iBoolsBitSet, biL_DoSubHelp)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] Player is in a sub with the package, detaching package")
						ENDIF
					ELSE
						IF IS_BIT_SET(iBoolsBitSet, biL_DoSubHelp)
							CLEAR_BIT(iBoolsBitSet, biL_DoSubHelp)
							CLEAR_BIT(iBoolsBitSet, biL_DoneSubHelp)
						ENDIF
					ENDIF
					
					//-- Pickup sound
					iPickupSound = GET_SOUND_ID()
					IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
						PLAY_SOUND_FRONTEND(iPickupSound, "Pickup_Briefcase", "GTAO_Biker_Modes_Soundset", FALSE) 
					ELSE
						PLAY_SOUND_FRONTEND(iPickupSound, "Pickup_Briefcase", "GTAO_Magnate_Boss_Modes_Soundset", FALSE) 
					ENDIF
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] DOING COLLECT PICKUP AUDIO... iPickupSound = ", iPickupSound)
			
					// Move package outside if you enter the strip club
					IF IS_ENTITY_IN_STRIPCLUB(PLAYER_PED_ID())
						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niPackage)
							DETACH_ENTITY(NET_TO_OBJ(serverBD.niPackage))
							SET_ENTITY_COORDS_NO_OFFSET(NET_TO_OBJ(serverBD.niPackage), vStripClubCoords)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] IS_ENTITY_IN_STRIPCLUB - TRUE (1) - detaching package and moving to ",vStripClubCoords)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(NET_TO_OBJ(serverBD.niPackage), PLAYER_PED_ID())
					CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_GotThePackage)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] CLEARED biP_GotThePackage AS IS_ENTITY_ATTACHED_TO_ENTITY")
					
					IF IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
						SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY(FALSE)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY = FALSE")
					ENDIF
					
					IF PARTICIPANT_ID_TO_INT() != serverBD.iBossPartWhoLaunched
						//-- Set myself as not critical if I don't have the package, if I'm not the boss who launched the boss work
						GB_SET_LOCAL_PLAYER_CRITICAL_TO_JOB(FALSE)
					ENDIF
					
					IF IS_BIT_SET(iBoolsBitSet, biL_DoSubHelp)
						CLEAR_BIT(iBoolsBitSet, biL_DoSubHelp)
						CLEAR_BIT(iBoolsBitSet, biL_DoneSubHelp)
					ENDIF
				ELSE
					IF IS_ENTITY_IN_STRIPCLUB(PLAYER_PED_ID())
						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niPackage)
							DETACH_ENTITY(NET_TO_OBJ(serverBD.niPackage))
							SET_ENTITY_COORDS_NO_OFFSET(NET_TO_OBJ(serverBD.niPackage), vStripClubCoords)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] IS_ENTITY_IN_STRIPCLUB - TRUE (2) - detaching package and moving to ",vStripClubCoords)
						ENDIF
					ENDIF
					
					//-- Help text for being in a submersible
					IF IS_BIT_SET(iBoolsBitSet, biL_DoSubHelp)
						IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneSubHelp)
							IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
								PRINT_HELP("GB_ASLT_SUB") // You cannot deliver the briefcase in a submersible vehicle.
								SET_BIT(iBoolsBitSet, biL_DoneSubHelp)
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] SET biL_DoneSubHelp")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_GotThePackage)
			CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_GotThePackage)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] CLEARED biP_GotThePackage AS IS_PED_INJURED")
		ENDIF
	ENDIF
	
	
	//-- Add / Remove blip for the package and drop-off 
	IF iLocalPartPackageHolder = -1
		IF SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD()
			IF NOT DOES_BLIP_EXIST(blipPackage)
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
					ADD_BLIP_FOR_PACKAGE()
				ENDIF
			ELSE
				//-- WHen AI-ped with the package is killed, need to remove the blip so it can be re-added with the correct colour
				IF NOT IS_BIT_SET(iBoolsBitSet, biL_PackageFirstDropped)
					IF IS_BIT_SET(serverBD.iServerBitSet, biS_AiPackageHolderKilled)
						SET_BIT(iBoolsBitSet, biL_PackageFirstDropped)
						REMOVE_BLIP(blipPackage)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] SET biL_PackageFirstDropped")
					ENDIF
				ENDIF
			ENDIF
			
			//-- If AI ped still has the package - Blip Red
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AiPackageHolderKilled)
				IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_BACKGROUND
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[0])
						IF NOT IS_NET_PED_INJURED(serverBD.niPed[0])
							GET_HUD_COLOUR(HUD_COLOUR_RED, iR, iG, iB, iA)
							DRAW_MARKER(MARKER_ARROW, GET_ENTITY_COORDS(NET_TO_PED(serverBD.niPed[0]))+<<0,0,2.0>>, <<0,0,0>>, <<180,0,0>>, <<0.5,0.5,0.5>>, iR, iG, iB, 100, TRUE, TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF DOES_BLIP_EXIST(blipDropOff)
			REMOVE_BLIP(blipDropOff)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] REMOVED DROP-OFF BLIP AS iLocalPartPackageHolder = ", iLocalPartPackageHolder)
		ENDIF
		
		
					
	ELIF iLocalPartPackageHolder = PARTICIPANT_ID_TO_INT()
		IF DOES_BLIP_EXIST(blipPackage)
			REMOVE_BLIP(blipPackage)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] REMOVE BLIP AS iLocalPartPackageHolder = ", iLocalPartPackageHolder)
		ENDIF
		
		//-- Drop off
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_PackageDelivered)
			IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_BACKGROUND
				IF NOT DOES_BLIP_EXIST(blipDropOff)
					blipDropOff = ADD_BLIP_FOR_COORD(GET_PACKAGE_DROP_OFF_COORDS())
					SET_BLIP_ROUTE(blipDropOff, TRUE)
					PRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] ADDED DROP OFF BLIP")
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(blipDropOff)
					REMOVE_BLIP(blipDropOff)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] REMOVED DROP-OFF BLIP AS GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) = ", ENUM_TO_INT(GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())))
				ENDIF
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(blipDropOff)
				REMOVE_BLIP(blipDropOff)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] REMOVED DROP-OFF BLIP AS biS_PackageDelivered")
			ENDIF
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(blipPackage)
			REMOVE_BLIP(blipPackage)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] REMOVE BLIP AS iLocalPartPackageHolder = ", iLocalPartPackageHolder)
		ENDIF
		
		//-- Blip / unblip drop-off
		playerPackage = GET_PLAYER_WITH_PACKAGE()
		IF playerPackage != INVALID_PLAYER_INDEX()
			IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
				playerBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
				IF playerBoss != INVALID_PLAYER_INDEX()
					IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerPackage, playerBoss)
						
						
						//-- Drop off
						IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_PackageDelivered)
							IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_BACKGROUND
								IF NOT DOES_BLIP_EXIST(blipDropOff)
									blipDropOff = ADD_BLIP_FOR_COORD(GET_PACKAGE_DROP_OFF_COORDS())
									SET_BLIP_ROUTE(blipDropOff, TRUE)
									PRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] ADDED DROP OFF BLIP")
								ENDIF
							ENDIF
						ELSE
							IF DOES_BLIP_EXIST(blipDropOff)
								REMOVE_BLIP(blipDropOff)
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] REMOVED DROP-OFF BLIP AS biS_PackageDelivered")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		BOOL bDoMarker = FALSE
		//--Marker above package carrier
		IF SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD()
			IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_BACKGROUND
				IF IS_BIT_SET(serverBD.iServerBitSet, biS_AiPackageHolderKilled)
					IF playerPackage != INVALID_PLAYER_INDEX()
						playerBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
						IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
							IF NETWORK_IS_PLAYER_ACTIVE(playerBoss)
								IF (playerBoss != INVALID_PLAYER_INDEX() AND
									GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerPackage, playerBoss))
									//-- Someone on my gang has the package - NO arrow
								//	GET_HUD_COLOUR(HUD_COLOUR_GREEN, iR, iG, iB, iA)
								ELSE
									//-- Someone on my gang has the package - Red arrow
									GET_HUD_COLOUR(HUD_COLOUR_RED, iR, iG, iB, iA)
									bDoMarker = TRUE
								ENDIF
							ENDIF
						ELSE
							//-- Package holder not on any gang - Red arrow
							GET_HUD_COLOUR(HUD_COLOUR_RED, iR, iG, iB, iA)
							bDoMarker = TRUE
						ENDIF
						
						IF bDoMarker
							IF NOT IS_PED_INJURED(GET_PLAYER_PED(playerPackage)) 
								IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(playerPackage))
								AND IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(playerPackage), TRUE))
									FM_EVENTDRAW_MARKER_ABOVE_VEHICLE(GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(playerPackage), TRUE), iR, iG, iB)
								ELSE
									DRAW_MARKER(MARKER_ARROW, GET_ENTITY_COORDS(GET_PLAYER_PED(playerPackage))+<<0,0,2.0>>, <<0,0,0>>, <<180,0,0>>, <<0.5,0.5,0.5>>, iR, iG, iB, 100, TRUE, TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	//-- Objective text
//	IF NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GB_START_OF_JOB)
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneStartBigMessage)
			//-- For mission duration
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneStartBigMessage)
		ENDIF

	TEXT_LABEL_31 tl31Obj
	INT iTempGangID
	HUD_COLOURS hcTempGang
	STRING sOrganization
	
	IF SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD()
		IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) > GB_UI_LEVEL_MINIMAL
			IF NOT IS_BIT_SET(iBoolsBitSet2, biL2_WaitForShard)
				IF IS_BIT_SET(serverBD.iServerBitSet, biS_AiPackageHolderKilled)
					IF iLocalPartPackageHolder = -1
						//-- No one has the package
						IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
							IF NOT Is_This_The_Current_Objective_Text("GB_AST_RETP")
								Print_Objective_Text("GB_AST_RETP") // Retrieve the ~HUD_COLOUR_GANG_BOSS~Package.
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] DOING RETRIEVE PACKAGE OBJ TEXT")
							ENDIF
						ELSE
							IF DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
								IF NOT Is_This_The_Current_Objective_Text("GB_AST_RETP")
									Print_Objective_Text("GB_AST_RETP") // Collect the ~HUD_COLOUR_GREEN~package and deliver it to the dropoff.
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] DOING RIVAL RETRIEVE PACKAGE OBJ TEXT")
								ENDIF
							ENDIF
						ENDIF
					ELIF iLocalPartPackageHolder = PARTICIPANT_ID_TO_INT()
						//-- I've got the package
						IF NOT Is_This_The_Current_Objective_Text("GB_AST_DROP")
							Print_Objective_Text("GB_AST_DROP") // Deliver the package to the ~y~Drop-off.
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] DOING DROP-OFF OBJ TEXT")
						ENDIF

					ELSE
						//-- Someone else has teh package
						playerPackage = GET_PLAYER_WITH_PACKAGE()
						IF playerPackage != INVALID_PLAYER_INDEX()
							IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
								playerBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
								IF (playerBoss != INVALID_PLAYER_INDEX() 
								AND GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerPackage, playerBoss))
									
									IF NOT Is_This_The_Current_Objective_Text("GB_AST_DROPB")
								//		Print_Objective_Text_With_Player_Name("GB_AST_DROPB", playerPackage) // Help ~a~ deliver the Package to the ~y~Drop-off
										iTempGangID = GET_GANG_ID_FOR_PLAYER(playerPackage)
										IF iTempGangID > -1
											hcTempGang = GET_HUD_COLOUR_FOR_GANG_ID(iTempGangID)
											Print_Objective_Text_With_Player_Name_And_String_Coloured("GB_AST_DROPB", playerPackage, "GB_AST_BRIEF", DEFAULT, hcTempGang)
										//	Print_Objective_Text_With_Coloured_Player_Name("GB_AST_DROPB", playerPackage, hcTempGang) 
										ENDIF
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] DOING BUDDY DROP-OFF OBJ TEXT playerPackage ", GET_PLAYER_NAME(playerPackage))
									ENDIF
								ELSE
									IF NOT GB_IS_PLAYER_MEMBER_OF_A_GANG(playerPackage)
										IF NOT Is_This_The_Current_Objective_Text("GB_AST_RETNG")	
											Print_Objective_Text_With_Player_Name("GB_AST_RETNG", playerPackage) //<C>~a~</C> has the ~r~briefcase.~s~Take it from them.
											CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] DOING RIVAL RETRIEVE PACKAGE OBJ TEXT - 2")
										ENDIF
									ELSE 
										IF NOT Is_This_The_Current_Objective_Text("GB_AST_RETGR")	
											sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(playerPackage)
											Print_Objective_Text_With_Coloured_String("GB_AST_RETGR", sOrganization, HUD_COLOUR_WHITE)
											CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] DOING RIVAL RETRIEVE PACKAGE OBJ TEXT - 4")
										ENDIF
									ENDIF
								ENDIF
							ELSE
								//-- I'm not in a gang
								IF NOT GB_IS_PLAYER_MEMBER_OF_A_GANG(playerPackage)
									IF NOT Is_This_The_Current_Objective_Text("GB_AST_RETNG")
										Print_Objective_Text_With_Player_Name("GB_AST_RETNG", playerPackage)
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] DOING RIVAL RETRIEVE PACKAGE OBJ TEXT - 3")
									ENDIF
								ELSE
									IF NOT Is_This_The_Current_Objective_Text("GB_AST_RETGR")	
										sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(playerPackage)
										Print_Objective_Text_With_Coloured_String("GB_AST_RETGR", sOrganization, HUD_COLOUR_WHITE)
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] DOING RIVAL RETRIEVE PACKAGE OBJ TEXT - 5")
									ENDIF
								ENDIF
							ENDIF
						ENDIF

					ENDIF
				ELSE
					tl31Obj = GET_INITIAL_OBJECTIVE_FOR_VARIATION()
					//-- Ai package holder still alive
					IF NOT Is_This_The_Current_Objective_Text(tl31Obj)
						Print_Objective_Text(tl31Obj) //The General has the ~r~briefcase. ~s~Take it from them.
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] RETRIEVE PACKAGE OBJ TEXT AS AI PED STILL ALIVE")
					ENDIF
				ENDIF
			ENDIF
		ELSE
			Clear_Any_Objective_Text_From_This_Script()
			
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] CLEAR OBJECTIVE TEXT AS GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) = ", ENUM_TO_INT(GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())))
		ENDIF
	ELSE
		Clear_Any_Objective_Text_From_This_Script()
			
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] CLEAR OBJECTIVE TEXT AS SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD")
		
	ENDIF
		
	
	
	//-- Help text - rival gang launch help
	IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneRivalLaunchHelp)
		IF DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
			IF NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GB_START_OF_JOB)
				IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_BACKGROUND
					IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
						sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
						HUD_COLOURS hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
						
						IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
					//		PRINT_HELP_NO_SOUND("GB_AST_HELP2") // Retrieve the package before the rival gang gang can deliver it to the drop off.
							IF serverBD.iPartPackageHolder > -1
								PRINT_HELP_WITH_LITERAL_STRING_WITH_HUD_COLOUR_NO_SOUND("GB_AST_HELP2", sOrganization, hclPlayer)
							ELSE
								PRINT_HELP_WITH_LITERAL_STRING_WITH_HUD_COLOUR_NO_SOUND("GB_AST_HELP22", sOrganization, hclPlayer)
							ENDIF
						ELSE
						//	PRINT_HELP_NO_SOUND("GB_AST_HELP5") // Retrieve the package before the rival gang gang can deliver it to the drop off.
							IF serverBD.iPartPackageHolder > -1
								PRINT_HELP_WITH_LITERAL_STRING_WITH_HUD_COLOUR_NO_SOUND("GB_AST_HELP5", sOrganization, hclPlayer)
							ELSE
								PRINT_HELP_WITH_LITERAL_STRING_WITH_HUD_COLOUR_NO_SOUND("GB_AST_HELP52", sOrganization, hclPlayer)
							ENDIF
						ENDIF	
						GB_SET_GANG_BOSS_HELP_BACKGROUND()
						SET_BIT(iBoolsBitSet, biL_DoneRivalLaunchHelp)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] SET biL_DoneRivalLaunchHelp - I JOINED AS RIVAL")
					ELSE
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_PACKAGE_CLIENT] NOT OK TO PRINT HELP - biL_DoneRivalLaunchHelp")				
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	//-- Map exit?
//	IF IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
//		HANDLE_MAP_EXITING_PLAYERS()
//	ENDIF
	
	
	MAINTAIN_PLAYER_WITH_PACKAGE_ON_YACHT()
	
	CHECK_FOR_LOCAL_PLAYER_DELIVERING_PACKAGE()
	
	MAINTAIN_PACKAGE_MARKER()
ENDPROC

/// PURPOSE:
///    Handles client-side stealing of vehicles for the variation
PROC MAINTAIN_STEAL_VEHICLE_CLIENT()
	INT iTempGangID
	HUD_COLOURS hcTempGang
	
	PLAYER_INDEX playerVeh
	PLAYER_INDEX playerBoss
	PARTICIPANT_INDEX partVeh
	
	IF serverBD.iVariation != GB_ASSAULT_ZANCUDO
		//-- No vehicles to deliver
		EXIT
	ENDIF
	
	IF NOT SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD()
		IF iLocalPartVehDriver != -2
			IF iLocalPartVehDriver > -1
				partVeh = INT_TO_PARTICIPANTINDEX(iLocalPartVehDriver)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(partVeh)
					playerVeh = NETWORK_GET_PLAYER_INDEX(partVeh)
					IF playerVeh != PLAYER_ID()
						SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(playerVeh,  RADAR_TRACE_TANK, FALSE)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_STEAL_VEHICLE_CLIENT] RESET PLAYER BLIP FOR PLAYER ", GET_PLAYER_NAME(playerVeh), " AS SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD")
						IF NOT DID_MY_BOSS_LAUNCH_GB_ASSAULT()
							SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(playerVeh, BLIP_COLOUR_RED, FALSE)
							FORCE_BLIP_PLAYER(playerVeh, FALSE, FALSE)
							SET_PLAYER_BLIP_AS_LONG_RANGE(playerVeh, FALSE)
							SET_FIXED_BLIP_SCALE_FOR_PLAYER(playerVeh, g_sMPTunables.fgangboss_Job_blip_scale, FALSE)

						ENDIF
					ENDIF
					
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(blipVeh)
					REMOVE_BLIP(blipVeh)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_STEAL_VEHICLE_CLIENT] REMOVED blipDropOff AS SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD")
				ENDIF
			ENDIF
			
			IF DOES_BLIP_EXIST(blipDropOff)
				REMOVE_BLIP(blipDropOff)
			ENDIF
			
			iLocalPartVehDriver = -2
			Clear_Any_Objective_Text_From_This_Script()
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_STEAL_VEHICLE_CLIENT] Clear_Any_Objective_Text_From_This_Script as NOT SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD")
		ENDIF
		
		EXIT
	ENDIF
	
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])	
		//-- Am in driving the vehicle?
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[0])
				IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh[0]))
					IF GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(serverBD.niVeh[0])) = PLAYER_PED_ID()
						IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InVeh)
							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InVeh)
							GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
							GB_SET_PLAYER_AS_PERMANENT_ON_BOSS_MISSION()
							IF DOES_BLIP_EXIST(blipVeh)
								REMOVE_BLIP(blipVeh)
							ENDIF
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_STEAL_VEHICLE_CLIENT] SET biP_InVeh")
						ENDIF
					ELSE
						IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InVeh)
							CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InVeh)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_STEAL_VEHICLE_CLIENT] CLEAR biP_InVeh - NOT DRIVER")
						ENDIF
					ENDIF
				ELSE
					IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InVeh)
						CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InVeh)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_STEAL_VEHICLE_CLIENT] CLEAR biP_InVeh - NOT IN VEH")
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InVeh)
					CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InVeh)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_STEAL_VEHICLE_CLIENT] CLEAR biP_InVeh - VEH NOT DRIVABLE")
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InVeh)
				CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InVeh)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_STEAL_VEHICLE_CLIENT] CLEAR biP_InVeh - I'M INJURED")
			ENDIF
		ENDIF
		
		
		//-- Update my local copy of who is driving the vehicle
		IF iLocalPartVehDriver != serverBD.iPartInVeh
			IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[0])
				IF serverBD.iPartInVeh = -1
					IF SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD()
						IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_BACKGROUND
							//-- Remove player blip if no-one has the vehicle, and someone previously had it
							partVeh = INT_TO_PARTICIPANTINDEX(iLocalPartVehDriver)
							IF NETWORK_IS_PARTICIPANT_ACTIVE(partVeh)
								playerVeh = NETWORK_GET_PLAYER_INDEX(partVeh)
								IF playerVeh != PLAYER_ID()
									SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(playerVeh,  RADAR_TRACE_TANK, FALSE)
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_STEAL_VEHICLE_CLIENT] RESET PLAYER BLIP FOR PLAYER ", GET_PLAYER_NAME(playerVeh))
									IF NOT DID_MY_BOSS_LAUNCH_GB_ASSAULT()
										SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(playerVeh, BLIP_COLOUR_RED, FALSE)
										FORCE_BLIP_PLAYER(playerVeh, FALSE, FALSE)
										SET_PLAYER_BLIP_AS_LONG_RANGE(playerVeh, FALSE)
										SET_FIXED_BLIP_SCALE_FOR_PLAYER(playerVeh, g_sMPTunables.fgangboss_Job_blip_scale, FALSE)

									ENDIF
								ENDIF
								
							ENDIF
							
						ENDIF
					ENDIF
					
					ADD_BLIP_FOR_VEHICLE()
					
				ELSE
					//-- server thinks someone is in the vehicle
					partVeh = INT_TO_PARTICIPANTINDEX(serverBD.iPartInVeh)
					IF NETWORK_IS_PARTICIPANT_ACTIVE(partVeh)
						IF SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD()
							IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_BACKGROUND
								//-- Blip vehilce holder

								playerVeh = NETWORK_GET_PLAYER_INDEX(partVeh)
								
								IF DOES_BLIP_EXIST(blipVeh)
									REMOVE_BLIP(blipVeh)
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_STEAL_VEHICLE_CLIENT] REMOVE VEH BLIP AS ABOUT TO BLIP PLAYER (UNLESS ITS ME) ", GET_PLAYER_NAME(playerVeh))
								ENDIF
								
								IF playerVeh != PLAYER_ID()
									IF NOT DID_MY_BOSS_LAUNCH_GB_ASSAULT()
										IF NOT IS_ENTITY_HOLDER_IN_MY_GANG()
											//-- I'm a rival gang / player, package holder isn't in my gang
											SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(playerVeh,  RADAR_TRACE_TANK, TRUE)
											FORCE_BLIP_PLAYER(playerVeh, TRUE, TRUE)
											SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(playerVeh, BLIP_COLOUR_RED, TRUE)
											SET_PLAYER_BLIP_AS_LONG_RANGE(playerVeh, TRUE)
											SET_FIXED_BLIP_SCALE_FOR_PLAYER(playerVeh, g_sMPTunables.fgangboss_Job_blip_scale, TRUE)
											CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_STEAL_VEHICLE_CLIENT] MY BOSS DIDN'T LAUNCH AND PACKAGE HOLDER NOT IN MY GANG SET PACKAGE PLAYER BLIP FOR PLAYER ", GET_PLAYER_NAME(playerVeh))
										ELSE
											SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(playerVeh,  RADAR_TRACE_TANK, TRUE)
											SET_FIXED_BLIP_SCALE_FOR_PLAYER(playerVeh, g_sMPTunables.fgangboss_Job_blip_scale, TRUE)
											CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_STEAL_VEHICLE_CLIENT] MY BOSS DIDN'T LAUNCH AND PACKAGE HOLDER IN MY GANG SET PACKAGE PLAYER BLIP FOR PLAYER ", GET_PLAYER_NAME(playerVeh))
										ENDIF
									ELSE
										IF IS_ENTITY_HOLDER_IN_MY_GANG()
										
											SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(playerVeh,  RADAR_TRACE_TANK, TRUE)
											SET_FIXED_BLIP_SCALE_FOR_PLAYER(playerVeh, g_sMPTunables.fgangboss_Job_blip_scale, TRUE)
											CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_STEAL_VEHICLE_CLIENT] MY BOSS LAUNCHED AND PACKAGE HOLDER IN MY GANG SET PACKAGE PLAYER BLIP FOR PLAYER ", GET_PLAYER_NAME(playerVeh))
										ELSE
											SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(playerVeh,  RADAR_TRACE_TANK, TRUE)
											FORCE_BLIP_PLAYER(playerVeh, TRUE, TRUE)
											SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(playerVeh, BLIP_COLOUR_RED, TRUE)
											SET_PLAYER_BLIP_AS_LONG_RANGE(playerVeh, TRUE)
											SET_FIXED_BLIP_SCALE_FOR_PLAYER(playerVeh, g_sMPTunables.fgangboss_Job_blip_scale, TRUE)
											CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_STEAL_VEHICLE_CLIENT] MY BOSS LAUNCHED BUT PACKAGE HOLDER NOT IN MY GANG - SET PACKAGE PLAYER BLIP FOR PLAYER ", GET_PLAYER_NAME(playerVeh))
										
										ENDIF
									ENDIF
								ENDIF
							
							
								IF playerVeh != PLAYER_ID()
									PRINT_TICKER_WITH_PLAYER_NAME("GB_AST_TCKR", playerVeh) // ~a~ took the rhino.
								ENDIF
							ENDIF
						ELSE
							
						ENDIF
					ENDIF
				ENDIF
				
				
			
				iLocalPartVehDriver = serverBD.iPartInVeh
				IF iLocalPartVehDriver = -1
					
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_STEAL_VEHICLE_CLIENT] iLocalPartVehDriver updated to ",iLocalPartVehDriver)
				ELSE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_STEAL_VEHICLE_CLIENT] iLocalPartVehDriver updated to ",iLocalPartVehDriver, " WHO IS PLAYER ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iLocalPartVehDriver)))
				ENDIF
			ENDIF
		ENDIF
		
		
		//--Objective text
		IF SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD()
			IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) > GB_UI_LEVEL_MINIMAL
				IF iLocalPartVehDriver = -1
					//-- No one is in the vehicle
					IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
						IF NOT Is_This_The_Current_Objective_Text("GB_AST_RETV")
							Print_Objective_Text("GB_AST_RETV") // Retrieve the ~HUD_COLOUR_GANG_BOSS~Vehicle.
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_STEAL_VEHICLE_CLIENT] DOING RETRIEVE Vehicle OBJ TEXT")
						ENDIF
					ELSE
						IF DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
							IF NOT Is_This_The_Current_Objective_Text("GB_AST_DESR")
								Print_Objective_Text("GB_AST_DESR") // Destroy the ~r~Rhino.
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_STEAL_VEHICLE_CLIENT] DOING RIVAL DESTROY Vehicle OBJ TEXT")
							ENDIF
						ENDIF
					ENDIF
				ELIF iLocalPartVehDriver = PARTICIPANT_ID_TO_INT()
					//-- I'm in the vehicle
					IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
						IF NOT Is_This_The_Current_Objective_Text("GB_AST_DROPV")
							Print_Objective_Text("GB_AST_DROPV") // Deliver the package to the ~y~Drop-off.
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_STEAL_VEHICLE_CLIENT] DOING DROP-OFF OBJ TEXT")
						ENDIF
					ENDIF

				ELSE
					//-- Someone else has is in the vehicle
					playerVeh = GET_PLAYER_DRIVING_VEH()
					IF playerVeh != INVALID_PLAYER_INDEX()
						IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
							playerBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
							IF (playerBoss != INVALID_PLAYER_INDEX() 
							AND GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerVeh, playerBoss))
								IF NOT Is_This_The_Current_Objective_Text("GB_AST_DROPBV")
									iTempGangID = GET_GANG_ID_FOR_PLAYER(playerVeh)
									IF iTempGangID > -1
										hcTempGang = GET_HUD_COLOUR_FOR_GANG_ID(iTempGangID)
									//	Print_Objective_Text_With_Coloured_Player_Name("GB_AST_DROPBV", playerVeh, hcTempGang) // Help ~a~ deliver the Vehicle to the ~y~Drop-off
										Print_Objective_Text_With_Player_Name_And_String_Coloured("GB_AST_DROPBV", playerVeh, "GB_AST_RNO", DEFAULT, hcTempGang)
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_STEAL_VEHICLE_CLIENT] DOING BUDDY DROP-OFF OBJ TEXT playerVeh ", GET_PLAYER_NAME(playerVeh))
									ENDIF
								ENDIF
							ELSE
//								IF NOT Is_This_The_Current_Objective_Text("GB_AST_RETVR")
//									
//									Print_Objective_Text_With_Player_Name("GB_AST_RETVR", playerVeh) //<C>~a~</C> has the ~r~Vehicle.~s~Take it from them.
//									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_STEAL_VEHICLE_CLIENT] DOING RIVAL RETRIEVE Vehicle OBJ TEXT - 2")
//								ENDIF
								IF NOT Is_This_The_Current_Objective_Text("GB_AST_DESR")
									Print_Objective_Text("GB_AST_DESR") // Destroy the ~r~Rhino.
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_STEAL_VEHICLE_CLIENT] DOING RIVAL DESTROY Vehicle OBJ TEXT (2)")
								ENDIF
							ENDIF
						ELSE
//							IF NOT Is_This_The_Current_Objective_Text("GB_AST_RETVR")
//								Print_Objective_Text_With_Player_Name("GB_AST_RETVR", playerVeh)
//								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_STEAL_VEHICLE_CLIENT] DOING RIVAL RETRIEVE Vehicle OBJ TEXT - 3")
//							ENDIF
							
							IF NOT Is_This_The_Current_Objective_Text("GB_AST_DESR")
								Print_Objective_Text("GB_AST_DESR") // Destroy the ~r~Rhino.
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_STEAL_VEHICLE_CLIENT] DOING RIVAL DESTROY Vehicle OBJ TEXT (3)")
							ENDIF
						ENDIF
					ENDIF

				ENDIF
			ENDIF
		ELSE

		ENDIF
		

		//--Drop off
		IF iLocalPartVehDriver = -1
			IF DOES_BLIP_EXIST(blipDropOff)
				REMOVE_BLIP(blipDropOff)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_STEAL_VEHICLE_CLIENT] REMOVED blipDropOff iLocalPartVehDriver = -1")
			ENDIF
			
			ADD_BLIP_FOR_VEHICLE()
		ELIF iLocalPartVehDriver = PARTICIPANT_ID_TO_INT()
			IF DOES_BLIP_EXIST(blipVeh)
				REMOVE_BLIP(blipVeh)
			ENDIF
			
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_VehDelivered)
				IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_BACKGROUND
					IF NOT DOES_BLIP_EXIST(blipDropOff)
						blipDropOff = ADD_BLIP_FOR_COORD(GET_PACKAGE_DROP_OFF_COORDS())
						SET_BLIP_ROUTE(blipDropOff, TRUE)
						PRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_STEAL_VEHICLE_CLIENT] ADDED DROP OFF BLIP")
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(blipDropOff)
						REMOVE_BLIP(blipDropOff)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_STEAL_VEHICLE_CLIENT] REMOVED DROP-OFF BLIP AS GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) = ", ENUM_TO_INT(GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())))
					ENDIF
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(blipDropOff)
					REMOVE_BLIP(blipDropOff)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_STEAL_VEHICLE_CLIENT] REMOVED DROP-OFF BLIP AS biS_PackageDelivered")
				ENDIF
			ENDIF
		
		ELSE
			IF DOES_BLIP_EXIST(blipVeh)
				REMOVE_BLIP(blipVeh)
			ENDIF
			
			playerVeh = GET_PLAYER_DRIVING_VEH()
			IF playerVeh != INVALID_PLAYER_INDEX()
				IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
					playerBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
					IF playerBoss != INVALID_PLAYER_INDEX()
						IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerVeh, playerBoss)

							//-- Drop off
							IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_PackageDelivered)
								IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_BACKGROUND
									IF NOT DOES_BLIP_EXIST(blipDropOff)
										blipDropOff = ADD_BLIP_FOR_COORD(GET_PACKAGE_DROP_OFF_COORDS())
										SET_BLIP_ROUTE(blipDropOff, TRUE)
										PRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_STEAL_VEHICLE_CLIENT] ADDED DROP OFF BLIP")
									ENDIF
								ENDIF
							ELSE
								IF DOES_BLIP_EXIST(blipDropOff)
									REMOVE_BLIP(blipDropOff)
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_STEAL_VEHICLE_CLIENT] REMOVED DROP-OFF BLIP AS biS_PackageDelivered")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//-- Help text - rival gang launch help
		IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneRivalLaunchHelp)
			IF DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
				IF NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GB_START_OF_JOB)
					IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_BACKGROUND
						IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
							STRING sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
							HUD_COLOURS hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
						
							IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
							//	PRINT_HELP_NO_SOUND("GB_AST_HELP3") // Retrieve the vehicle before the rival Organization can deliver it to the drop-off.
								PRINT_HELP_WITH_LITERAL_STRING_WITH_HUD_COLOUR_NO_SOUND("GB_AST_HELP3", sOrganization, hclPlayer)
							ELSE
							//	PRINT_HELP_NO_SOUND("GB_AST_HELP6")
								PRINT_HELP_WITH_LITERAL_STRING_WITH_HUD_COLOUR_NO_SOUND("GB_AST_HELP6", sOrganization, hclPlayer)
							ENDIF
							GB_SET_GANG_BOSS_HELP_BACKGROUND()
							SET_BIT(iBoolsBitSet, biL_DoneRivalLaunchHelp)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_STEAL_VEHICLE_CLIENT] SET biL_DoneRivalLaunchHelp - I JOINED AS RIVAL")
						ELSE
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [MAINTAIN_STEAL_VEHICLE_CLIENT] NOT OK TO PRINT HELP - biL_DoneRivalLaunchHelp")				
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		CHECK_FOR_LOCAL_PLAYER_DELIVERING_VEHICLE()
	ENDIF
	
	MAINTAIN_VEHICLE_MARKER()
	
ENDPROC
//PURPOSE: Process the GB_ASSAULT stages for the Client
PROC PROCESS_GB_ASSAULT_CLIENT()
	TEXT_LABEL_31 tlIntroShard
	
	
	SWITCH playerBD[PARTICIPANT_ID_TO_INT()].eStage		
		CASE eGB_INIT
			
		//	IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
				IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
					tlIntroShard = GET_INTRO_SHARD_TEXT()
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_START_OF_JOB, "BIGM_ASLTN", tlIntroShard)
					GB_SET_COMMON_TELEMETRY_DATA_ON_START()
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - eGB_INIT DOING START SHARD")
					
					playerBD[PARTICIPANT_ID_TO_INT()].eStage = eGB_GET_TO_LOCATION
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  PLAYER STAGE eGB_INIT -> eGB_GET_TO_LOCATION - MY GANG LAUNCHED")
					
				ELSE
					playerBD[PARTICIPANT_ID_TO_INT()].eStage = eGB_GET_PACKAGE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  PLAYER STAGE eGB_INIT -> eGB_GET_PACKAGE - I'M RIVAL / NOT A GANG MEMBER")
				ENDIF
				
				
	//		ELSE
	//			playerBD[PARTICIPANT_ID_TO_INT()].eStage = eGB_OFF_MISSION
	//			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Finished)
	//			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  PLAYER STAGE eGB_INIT -> eGB_OFF_MISSION - I'M NOT A GANG MEMBER / BOSS - SET biP_Finished")
			
	//		ENDIF
		BREAK
		
		CASE eGB_GET_TO_LOCATION
			MAINTAIN_MISSION_DURATION_CLIENT()
			
			MAINTAIN_HELP_TEXT()
			
			MAINTAIN_PLAYER_IN_PASSIVE_MODE()
			
			MAINTAIN_GB_ASSAULT_SPAWNING()
			
			MAINTAIN_GET_TO_LOCATION_CLIENT()
			
			MAINTAIN_PLAYER_APPROACH_TARGETS()
			
			MAINTAIN_MISSION_CRITICAL_VEH_WRECKED()
			
			MAINTAIN_GB_ASSAULT_MUSIC()
			
			CHECK_FOR_REWARD() // Needed here for boss quit session / duration expired end conditions
			IF serverBD.eStage = eGB_GET_PACKAGE
				IF DOES_BLIP_EXIST(blipLoc)
					REMOVE_BLIP(blipLoc)
				ENDIF
				
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eGB_GET_PACKAGE
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  PLAYER STAGE eGB_GET_TO_LOCATION -> eGB_GET_PACKAGE AS SERVER SAYS SO")
			ELIF serverBD.eStage = eGB_OFF_MISSION
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eGB_OFF_MISSION
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  PLAYER STAGE eGB_GET_PACKAGE -> eGB_OFF_MISSION AS SERVER SAYS SO")
			ELIF serverBD.eStage = eAD_CLEANUP
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CLEANUP
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  PLAYER STAGE eGB_GET_PACKAGE -> eAD_CLEANUP AS SERVER SAYS SO")
			ENDIF
		BREAK
		
		
		CASE eGB_GET_PACKAGE
			MAINTAIN_PLAYER_CRITICAL_ON_FM_EVENT()
			
			MAINTAIN_MISSION_DURATION_CLIENT()
			
			MAINTAIN_PLAYER_JOINING_AS_RIVAL()
			
			MAINTAIN_HELP_TEXT()
			
			MAINTAIN_PLAYER_IN_PASSIVE_MODE()
			
			MAINTAIN_PACKAGE_CLIENT()
			
			MAINTAIN_STEAL_VEHICLE_CLIENT()
			
			MAINTAIN_GB_ASSAULT_SPAWNING()
			
			MAINTAIN_GB_ASSAULT_WANTED_LEVEL()
			
			MAINTAIN_GB_ASSAULT_RESTORE_WEAPON()
			
			MAINTAIN_PLAYER_APPROACH_TARGETS()
			
			MAINTAIN_SUPPRESS_ASSAULT_SCENARIOS()
			
			MAINTAIN_MISSION_CRITICAL_VEH_WRECKED()
			
			MAINTAIN_TEXT_MESSAGE()
			
			MAINTAIN_GB_ASSAULT_MUSIC()
			
			CHECK_FOR_REWARD()
			
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Finished)
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eGB_OFF_MISSION
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  PLAYER STAGE eGB_GET_PACKAGE -> eGB_OFF_MISSION AS biP_Finished")
			ELIF serverBD.eStage = eGB_OFF_MISSION
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eGB_OFF_MISSION
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  PLAYER STAGE eGB_GET_PACKAGE -> eGB_OFF_MISSION AS SERVER SAYS SO")
			ELIF serverBD.eStage = eAD_CLEANUP
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CLEANUP
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  PLAYER STAGE eGB_GET_PACKAGE -> eAD_CLEANUP AS SERVER SAYS SO")
			ELIF SHOULD_MOVE_OFF_MISSION()
				SET_BIT(iBoolsBitSet, biL_ForcedOffScript)
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CLEANUP
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  PLAYER STAGE eGB_GET_PACKAGE -> eAD_CLEANUP AS SHOULD_MOVE_OFF_MISSION")
			ENDIF


		BREAK
		
		CASE eGB_OFF_MISSION
			IF serverBD.eStage = eAD_CLEANUP
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CLEANUP
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  PLAYER STAGE eGB_OFF_MISSION -> eAD_CLEANUP AS SERVER SAYS SO")
			ENDIF
		BREAK
		

		
		CASE eAD_CLEANUP
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  SCRIPT CLEANUP B     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		BREAK
	ENDSWITCH
	
	IF playerBD[PARTICIPANT_ID_TO_INT()].eStage	< eGB_OFF_MISSION
		MAINTAIN_DISTANCE_CHECKS_FOR_VARIATION()
	ENDIF
ENDPROC




//PURPOSE: Process the stages for the Server
PROC PROCESS_GB_ASSAULT_SERVER()

	
	SWITCH serverBD.eStage
		CASE eGB_INIT
			
			
			serverBD.eStage = eGB_GET_TO_LOCATION
		BREAK
		
		CASE eGB_GET_TO_LOCATION
			MAINTAIN_MISSION_DURATION_SERVER()
			
			//-- Has someone got to the initial package location?
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneAtLoc)
				serverBD.eStage = eGB_GET_PACKAGE
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [PROCESS_GB_ASSAULT_SERVER] serverBD.eStage  eGB_GET_TO_LOCATION -> eGB_GET_PACKAGE as biS_SomeoneAtLoc")
			ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_ForceRivalsOnMission)
				IF serverBD.iPartPackageHolder != -1
					serverBD.eStage = eGB_GET_PACKAGE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [PROCESS_GB_ASSAULT_SERVER] serverBD.eStage  eGB_GET_TO_LOCATION -> eGB_GET_PACKAGE as biS_ForceRivalsOnMission AND serverBD.iPartPackageHolder = ", serverBD.iPartPackageHolder)
				ENDIF
			ENDIF
		BREAK
		
		CASE eGB_GET_PACKAGE
			//-- Has AI package holder been killed?
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AiPackageHolderKilled)
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[0])
					IF IS_NET_PED_INJURED(serverBD.niPed[0])
						SET_BIT(serverBD.iServerBitSet, biS_AiPackageHolderKilled)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - [PROCESS_GB_ASSAULT_SERVER] SET biS_AiPackageHolderKilled")
					ENDIF
				ENDIF
			ENDIF
			
			MAINTAIN_MISSION_DURATION_SERVER()
		BREAK
		
		CASE eGB_OFF_MISSION
		BREAK
//		CASE eAD_EXPLODE_VEH
//		BREAK
		CASE eAD_CLEANUP
		BREAK
	ENDSWITCH
	
	MAINTAIN_FORCE_RIVALS_ON_MISSION_SERVER()
	
ENDPROC

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                      //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT(MP_MISSION_DATA missionScriptArgs)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
		#IF IS_DEBUG_BUILD	
			CPRINTLN(DEBUG_NET_MAGNATE, "MP: Starting mission ") NET_NL()
		#ENDIF
		
		// Vehry out all the initial game starting duties. 
		PROCESS_PRE_GAME(missionScriptArgs)	
		
		// Setup widgets. Make sure this is done after PROCESS_PRE_GAME()
		#IF IS_DEBUG_BUILD
			CREATE_WIDGETS()
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD NET_LOG("GB_ASSAULT -  NETWORK_IS_GAME_IN_PROGRESS = FALSE")	#ENDIF
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  MISSION END - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP P     <----------     ") NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
	
	// Main loop
	WHILE TRUE

		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
				
		// If we have a match end event, bail
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			#IF IS_DEBUG_BUILD NET_LOG("SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")	#ENDIF
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  MISSION END - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE - SCRIPT CLEANUP B     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
//		ELIF GB_SHOULD_KILL_ACTIVE_BOSS_MISSION() 
//			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  MISSION END - GB_SHOULD_KILL_ACTIVE_BOSS_MISSION - SCRIPT CLEANUP <----------     ") NET_NL()
//			SCRIPT_CLEANUP()
		//If we should end the active boss mission
		ELIF GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION()
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  MISSION END - GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION - SCRIPT CLEANUP <----------")
			SCRIPT_CLEANUP()
		ENDIF
		
		//Check if player is in a tutorial session and end
//		IF NETWORK_IS_IN_TUTORIAL_SESSION()
//			#IF IS_DEBUG_BUILD NET_LOG("PLAYER IN TUT SESSION")	#ENDIF
//			NET_PRINT_TIME() NET_PRINT("     ---------->     GB_ASSAULT -  MISSION END - PLAYER IN TUTORIAL SESSION - SCRIPT CLEANUP C     <----------     ") NET_NL()
//			SCRIPT_CLEANUP()
//		ENDIF
		
		// Deal with the debug
		#IF IS_DEBUG_BUILD		
			UPDATE_WIDGETS()
			
			MAINTAIN_DEBUG_KEYS()
		#ENDIF		
	
		// If we need to leave the mission, make sure we just straight to the end stage and terminate the script - DON'T NEED FOR AMBIENT MISSION
		/*IF SHOULD_PLAYER_LEAVE_MP_MISSION() 
			#IF IS_DEBUG_BUILD
			NET_PRINT_TIME() NET_PRINT( GET_MP_MISSION_NAME(thisMission), " Leave mission = TRUE \n")
			#ENDIF
			playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_LEAVE
			#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_LEAVE")	#ENDIF
		ENDIF*/
		
		g_sGb_Telemetry_data.sdata.m_match1 = serverBD.iMatchId1
		g_sGb_Telemetry_data.sdata.m_match2 = serverBD.iMatchId2
		
		// -----------------------------------
		// Process your game logic.....
		SWITCH GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT())
			
			// Wait untill the server gives the all go before moving on
			CASE GAME_STATE_INI
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					IF DO_ASSAULT_NET_IDS_EXIST()
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - CLIENT INIT  PICKUP EXISTS")
						ELSE
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - CLIENT INIT  PICKUP DOESN'T EXITS")
						ENDIF
						GET_MY_BOSS_PARTICPANT_ID()	
						
						IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
							GB_COMMON_BOSS_MISSION_SETUP(FMMC_TYPE_GB_ASSAULT)
							SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(TRUE)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(TRUE) 2")
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  I THINK MY BOSS LAUNCHED!")
						ELSE
							GB_COMMON_BOSS_MISSION_SETUP(FMMC_TYPE_GB_ASSAULT, FALSE)
						//	SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(FALSE)
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
								PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.niPackage), TRUE, TRUE)
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT PREVENT_COLLECTION_OF_PORTABLE_PICKUP(TRUE) 3")
							ENDIF
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  MY BOSS DIDN'T LAUCNH!")
						ENDIF
						
						IF GET_GB_ASSAULT_VARIATION() = GB_ASSAULT_ZANCUDO
							REMOVE_IPL("CS3_07_MPGates")
							SET_BIT(iBoolsBitSet, biL_RemovedMilGatesIpl)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  REMOVED MILITARY GATE IPL")
							
							SET_VEHICLE_MODEL_IS_SUPPRESSED(RHINO, TRUE)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  SUPPRESSED RHINO")
						ELIF GET_GB_ASSAULT_VARIATION() = GB_ASSAULT_AIRPORT
							UNLOCK_AIRPORT_GATES()
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  UNLOCK_AIRPORT_GATES")
						ELIF GET_GB_ASSAULT_VARIATION() = GB_ASSAULT_MERRYWEATHER
							GB_ASSAULT_SET_MERRYWEATHER_GATES_OPEN()
						ENDIF
						
						IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
							
						//	iMyGangID =  GET_GANG_ID_FOR_PLAYER(GB_GET_LOCAL_PLAYER_GANG_BOSS())
						//	hcMyGangColour = GET_HUD_COLOUR_FOR_GANG_ID(iMyGangID)
							SET_BIT(iBoolsBitSet2, biL2_GangMemberWhenStarted)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  STARTING - I'M IN A GANG")
						ELSE
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  STARTING - I'M NOT IN A GANG")
						ENDIF
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
							SET_ENTITY_LOD_DIST(NET_TO_OBJ(serverBD.niPackage), 1200) 
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  SET PACKAGE LOD DIST")
						ENDIF
						
						MAINTAIN_ASSAULT_DAMAGE_TRACKING()
						
						BLOCK_GANG_ATTACK_FOR_ASSAULT_VARIATION()
						
						playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING    <----------     ") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_RUNNING")	#ENDIF
					ENDIF
						
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 1    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY")	#ENDIF
				ENDIF
			BREAK
			
			// Main gameplay state.
			CASE GAME_STATE_RUNNING
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					MAINTAIN_MAX_PLAYER_CHECKS_CLIENT()
					
					MAINTAIN_AI_PED_BODY()
					
					PROCESS_GB_ASSAULT_CLIENT()
					
					PROCESS_EVENTS()
					
					
					IF DID_MY_BOSS_LAUNCH_GB_ASSAULT()
					OR DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
						DRAW_CHALLENGE_DPAD_LBD(g_GBLeaderboardStruct.challengeLbdStruct, g_GBLeaderboardStruct.siDpadMovie, SUB_BOSS, g_GBLeaderboardStruct.dpadVariables, 
												g_GBLeaderboardStruct.fmDpadStruct)
					ENDIF

				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 2   <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY")	#ENDIF
				ENDIF
				
				STOP_30S_COUNTDOWN_AUDIO()
			BREAK
			
			CASE GAME_STATE_TERMINATE_DELAY 
				SET_MISSION_FINISHED(serverBD.MissionTerminateDelayTimer)
		      	IF IS_MISSION_READY_TO_CLEANUP(serverBD.MissionTerminateDelayTimer)
		            playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 3    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 3")	#ENDIF
		      	ENDIF
				
				STOP_30S_COUNTDOWN_AUDIO()
			BREAK
			
			// The game stage the local player is placed when we want him to leave a mission
			CASE GAME_STATE_LEAVE
				//LEAVE_MY_MP_MISSION(thisMission)
				
				STOP_30S_COUNTDOWN_AUDIO()
				playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 2    <----------     ") NET_NL()
				#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 2")	#ENDIF
			FALLTHRU
			
			//Cleans up then terminates the mission
			CASE GAME_STATE_END
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  SCRIPT CLEANUP A     <----------     ") NET_NL()
				SCRIPT_CLEANUP()
			BREAK

		ENDSWITCH
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			
			SWITCH GET_SERVER_MISSION_STATE()
				
				// For CnC group missions, we may want to wait for all players to have launched the script
				CASE GAME_STATE_INI
					IF GET_GB_ASSAULT_VARIATION() != -1
						IF CREATE_AI_PEDS_FOR_VARIATION()
						AND CREATE_PACKAGES_FOR_VARIATION()
						AND CREATE_VEHICLES_FOR_VARIATION()
							#IF IS_DEBUG_BUILD
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  serverBD.iServerGameState = GAME_STATE_RUNNING PICKUP EXISTS")
							ELSE
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  serverBD.iServerGameState = GAME_STATE_RUNNING PICKUP DOESN'T EXIST")
							ENDIF
							#ENDIF
							
							
							GET_PACKAGE_DROP_OFF_FOR_VARIATION()
							TURN_OFF_DROP_OFF_GENERATORS()
							IF GET_GB_ASSAULT_VARIATION() = GB_ASSAULT_MERRYWEATHER
								SET_BIT(serverBD.iServerBitSet, biS_AiPackageHolderKilled)
							ENDIF
							
							serverBD.iServerGameState = GAME_STATE_RUNNING
							serverBD.iBossPartWhoLaunched = PARTICIPANT_ID_TO_INT()
							serverBD.iBossPlayerWhoLaunched	= NATIVE_TO_INT(PLAYER_ID())
							GB_COMMON_BOSS_MISSION_SETUP(FMMC_TYPE_GB_ASSAULT)
							
							PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBD.iMatchId1, serverBD.iMatchId2)
							
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  serverBD.iServerGameState = GAME_STATE_RUNNING  iBossPartWhoLaunched = ", serverBD.iBossPartWhoLaunched)
							
						ENDIF	
					ELSE
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT - GET_GB_ASSAULT_VARIATION() = -1!")
					ENDIF
				BREAK
				
				// Look for game end conditions
				CASE GAME_STATE_RUNNING
					
				//	MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
				
					MAINTAIN_MAX_PLAYER_CHECKS_SERVER()	
					
					MAINTAIN_AI_PED_BRAIN()
					
					PROCESS_GB_ASSAULT_SERVER()

					
					#IF IS_DEBUG_BUILD
						IF bHostEndMissionNow
						OR MPGlobalsAmbience.bKillActiveEvent
							MPGlobalsAmbience.bKillActiveEvent = FALSE
							serverBD.iServerGameState = GAME_STATE_END
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  serverBD.iServerGameState = GAME_STATE_END 1    <----------     ") NET_NL()
							#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 1")	#ENDIF
						ENDIF
					#ENDIF	
					
					IF HAVE_MISSION_END_CONDITIONS_BEEN_MET()
						serverBD.iServerGameState = GAME_STATE_END
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  serverBD.iServerGameState = GAME_STATE_END 4    <----------     ") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 4")	#ENDIF
					ELIF GB_SHOULD_KILL_ACTIVE_BOSS_MISSION() 
						serverBD.iServerGameState = GAME_STATE_END
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_ASSAULT -  serverBD.iServerGameState = GAME_STATE_END 4  AS GB_SHOULD_KILL_ACTIVE_BOSS_MISSION  <----------     ") NET_NL()
					
					ENDIF
					
				BREAK
				
				CASE GAME_STATE_END
				BREAK
				
			ENDSWITCH
			
		ENDIF
		
//		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
//			DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.14, "STRING", "EXISTS")
//		ELSE
//			DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.14, "STRING", "DOESN'T EXIST")
//		ENDIF
		
		#IF IS_DEBUG_BUILD
		GB_ASSAULT_TIMED_DEBUG()
		#ENDIF
	ENDWHILE
	
	
ENDSCRIPT
