//////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        GB_YACHT_ROB.sc																	//
// Description: Gang Boss and their goons defend the Boss' yacht from invading players.			//
// Written by:  Steve Tiley																		//
// Date: 		01/09/2015																		//
//////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "net_mission.sch"


//----------------------
//	INCLUDES
//----------------------
USING "rage_builtins.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "flow_public_core_override.sch"
USING "net_wait_zero.sch"
USING "net_mission.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"
USING "DM_Leaderboard.sch"
USING "net_gang_boss.sch"

USING "net_yacht_attack.sch"

//----------------------
//  DEBUG
//----------------------

#IF IS_DEBUG_BUILD
WIDGET_GROUP_ID YachtRobberyWidget
BOOL bCreateWidgets
INT iWidgetStage
BOOL bHostEndMissionNow
BOOL bSuccessfullyDefendDebug
BOOL bBridgeTakenDebug
BOOL bDebugHideYachtAttackUI
VECTOR vYachtCoord = <<-1620.4, -4070.3, 12.0>>
FLOAT xOffset = 0.0
FLOAT yOffset = 0.0
FLOAT zOffset = 0.0
INT iSpawnPoint = 0
BOOL bWarpToPoint = FALSE
BOOL bPrintHeading = FALSE
#ENDIF

//----------------------
//	ENUM
//----------------------

ENUM MISSION_STAGE_ENUM
	eMS_IDLE = 0,
	eMS_DEFEND,
	eMS_ATTACK,
	eMS_OFF_MISSION,
	eMS_END,
	eMS_CLEANUP
ENDENUM

ENUM DEFEND_MUSIC_STAGE_ENUM
	eDMUSIC_PREP = 0,
	eDMUSIC_START_DEFEND,
	eDMUSIC_DEFENDING,
	eDMUSIC_ACTION,
	eDMUSIC_SAFE,
	eDMUSIC_STOP
ENDENUM

ENUM ATTACK_MUSIC_STAGE_ENUM
	eAMUSIC_PREP = 0,
	eAMUSIC_START_ATTACK,
	eAMUSIC_ACTION,
	eAMUSIC_ATTACK,
	eAMUSIC_WITHDRAW,
	eAMUSIC_STOP
ENDENUM

ENUM FINAL_COUNTDOWN_STAGE_ENUM
	eFCOUNTDOWN_PREP = 0,
	eFCOUNTDOWN_START,
	eFCOUNTDOWN_END_OR_KILL
ENDENUM

//----------------------
//	GAME STATE
//----------------------

CONST_INT GAME_STATE_INIT 					0
CONST_INT GAME_STATE_RUNNING				1
CONST_INT GAME_STATE_TERMINATE_DELAY		2
CONST_INT GAME_STATE_END					3

//----------------------
//	CONSTANTS
//----------------------

CONST_INT TIME_TO_DEFEND 				600000
CONST_INT TIME_END_TIME					12000
CONST_INT TIME_TO_PREPARE				180000
CONST_INT THIRTY_SECONDS				1000 * 30

CONST_INT CLEAR_VEHICLE_RADIUS			5

//----------------------
//	Local Bitset
//----------------------

//----------------------
//	STRUCT
//----------------------
GANG_BOSS_MANAGE_REWARDS_DATA sData
GB_STRUCT_BOSS_END_UI sEndUI

//----------------------
//	VARIABLES
//----------------------
INT iHelpBitSet
INT iBigMessageBitSet
INT iTimeRemaining = 0
INT r,g,b,a
INT deadr, deadg, deadb, deada
//INT iSoundIDCountdown
INT iAttackerWarpStep
BOOL bSet
BOOL bSetSpawnLocations = FALSE
BOOL bBeenKilled = FALSE
BOOL bSetMentalStateModifier = FALSE
BOOL bGrabbedYachtInfo = FALSE
BOOL bShouldSeeHelp = TRUE
BOOL bShouldSeeShards = TRUE
BOOL bShouldSeeBackgroundHelp = FALSE
BOOL bBossMissionSetupDone = FALSE
//BOOL bSoundStarted = FALSE
BOOL bLowNumbers = FALSE
BOOL bPlayCaptureSound = FALSE
INT iAttackerSeaVehStagger = 0
INT iAttackerAirVehStagger = 0
VECTOR 	vCapDimensions 			= <<4,4,3>>

VECTOR 	vCapPointOffset 		= <<23.190, 0.000, 1.2000>>
VECTOR 	vCapZonePoint1Offset 	= <<29.520, 0.000, 1.2000>>
VECTOR 	vCapZonePoint2Offset 	= <<19.530, 0.000, 3.2000>>
FLOAT 	fCapZoneWidth			= 9.500000

VECTOR vDefenderSpawnOffset[NUM_DEFENDER_SPAWN_OFFSETS]
FLOAT fDefenderSpawnHeadingOffset[NUM_DEFENDER_SPAWN_OFFSETS]

//----------------------
//	BLIPS
//----------------------
BLIP_INDEX BridgeBlip
BLIP_INDEX YachtBlip
BLIP_INDEX AttackerSeaVehBlip[MAX_ATTACKER_SEA_VEHS]
BLIP_INDEX AttackerAirVehBlip[MAX_ATTACKER_AIR_VEHS]

//----------------------
//	MUSIC
//----------------------
BOOL bPlayedHorn = FALSE
BOOL bSuppressedWantedMusic = FALSE

DEFEND_MUSIC_STAGE_ENUM eDefendMusicStage = eDMUSIC_PREP
INT iActionMusicTriggerStagger = 0
INT iSafetyMusicTriggerStagger = 0
INT iDefendMusicBitSet
CONST_INT iDMBS_TriggeredStartDefend 	0
CONST_INT iDMBS_TriggeredDefending		1
CONST_INT iDMBS_TriggeredAction			2
CONST_INT iDMBS_TriggeredSafety			3
CONST_INT iDMBS_TriggeredStop			4

ATTACK_MUSIC_STAGE_ENUM eAttackMusicStage = eAMUSIC_PREP
INT iAttackMusicBitSet
CONST_INT iAMBS_TriggeredStartAttack		0
CONST_INT iAMBS_TriggeredAttack			1
CONST_INT iAMBS_TriggeredAction			2
CONST_INT iAMBS_TriggeredWithdraw		3
CONST_INT iAMBS_TriggeredStop			4

FINAL_COUNTDOWN_STAGE_ENUM eFinalCountdownStage = eFCOUNTDOWN_PREP
INT iFinalCountdownBitSet
CONST_INT iFCBS_TriggeredPreCountdown	0
CONST_INT iFCBS_TriggeredCountdown		1
CONST_INT iFCBS_ReEnabledRadioControl	2
CONST_INT iFCBS_TriggeredCountdownKill	3


//----------------------
//	BROADCAST VARIABLES
//----------------------

// Server ----------------------------------------------- >|
CONST_INT biS_DebugEndMissionNow 		0
CONST_INT biS_SuccessfullyDefended 		1
CONST_INT biS_BridgeTaken				2
CONST_INT biS_YachtAttackEnded			3
CONST_INT biS_GangPrepComplete			4
CONST_INT biS_GangBossAbandoned			5
CONST_INT biS_DeleteAirVehicle			6
CONST_INT biS_BossLeft					7

STRUCT ServerBroadcastData

	INT 				iServerGameState = GAME_STATE_INIT
	
	INT 				iServerBitSet
	INT 				iDeleteSeaVehBitSet
	INT					iDeleteAirVehBitSet
	
	INT 				iYachtToUse
	INT					iServerEndReason = -1
	
	INT					iNumPlayersInCaptureArea
	PLAYER_INDEX		piCapturingPlayer
	
	PLAYER_INDEX 		piGangBoss
	
	VECTOR 				vYachtCoords
	FLOAT 				fYachtHeading
	
	VECTOR 				vCapturePointLocation
	VECTOR 				vCapturePointDimensions
	VECTOR 				vCaptureZonePoint1
	VECTOR 				vCaptureZonePoint2
	FLOAT 				fCaptureZoneWidth
	
	INT					iBossDeathCount
	INT					iGoonDeathCount
	
	// Attacker Spawn
	 
	VECTOR				vAttackerSpawnLocation 
	FLOAT				fAttackerSpawnHeading
	FLOAT				fAttackerSpawnRadius
	
	// Attacker vehicles
	YACHT_ATTACK_ATTACKER_VEHICLE_STRUCT sAttackerVehStruct
	
	NETWORK_INDEX 		niAttackerSeaVeh[MAX_ATTACKER_SEA_VEHS]
	NETWORK_INDEX 		niAttackerAirVeh[MAX_ATTACKER_AIR_VEHS]
		
	SCRIPT_TIMER 		DefendTimer
	SCRIPT_TIMER 		EndTimer
	SCRIPT_TIMER 		CaptureTimer
	
	SCRIPT_TIMER 		attackerSeaVehRespawnTimer[MAX_ATTACKER_SEA_VEHS]
	SCRIPT_TIMER 		attackerAirVehRespawnTimer[MAX_ATTACKER_AIR_VEHS]
	
	MISSION_STAGE_ENUM 	eStage = eMS_DEFEND
	
	INT iMatchId1
	INT iMatchId2
	
ENDSTRUCT
ServerBroadcastData serverBD
// ------------------------------------------------------ |<

// Client ----------------------------------------------- >|
CONST_INT biP_GangIsReady 					0
CONST_INT biP_AtBridge						1
CONST_INT biP_GoonAtYacht					2
CONST_INT biP_BossCompletedPreparations		3
CONST_INT biP_RewardGiven					4
CONST_INT biP_InParticipationRange			5
CONST_INT biP_PermanentParticipant			6
CONST_INT biP_InCaptureArea					7
CONST_INT biP_GangMemberInCaptureArea		8
CONST_INT biP_HiddenEvent					9
CONST_INT biP_IsPassive						10

STRUCT PlayerBroadcastData
	
	INT iClientGameState = GAME_STATE_INIT
	
	INT iClientBitSet
	
	MISSION_STAGE_ENUM eStage = eMS_IDLE
	
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]
// ------------------------------------------------------ |<

//--------------------------------------------------------
//	Client / Server - Getters and Setters for stage/state 
//--------------------------------------------------------

FUNC STRING GET_MISSION_STATE_STRING(INT iState)

	SWITCH(iState)
		CASE GAME_STATE_INIT 				RETURN "GAME_STATE_INIT"
		CASE GAME_STATE_RUNNING 			RETURN "GAME_STATE_RUNNING"
		CASE GAME_STATE_TERMINATE_DELAY 	RETURN "GAME_STATE_TERMINATE_DELAY"
		CASE GAME_STATE_END 				RETURN "GAME_STATE_END"
	ENDSWITCH

	RETURN "UNKNOWN MISSION STATE!! - speak to Steve Tiley."

ENDFUNC

FUNC STRING GET_MISSION_STAGE_STRING(MISSION_STAGE_ENUM stage)
	SWITCH stage
		CASE eMS_IDLE				RETURN "eMS_IDLE"
		CASE eMS_DEFEND				RETURN "eMS_DEFEND"
		CASE eMS_ATTACK				RETURN "eMS_ATTACK"
		CASE eMS_OFF_MISSION		RETURN "eMS_OFF_MISSION"
		CASE eMS_END				RETURN "eMS_END"
		CASE eMS_CLEANUP			RETURN "eMS_CLEANUP"
	ENDSWITCH
	
	RETURN "UNKNOWN MISSION STAGE!! - speak to Steve Tiley."
ENDFUNC

// Server ----------------------------------------------- >|

/// PURPOSE:
///    Gets the current server stage
FUNC MISSION_STAGE_ENUM GET_SERVER_MISSION_STAGE()
	RETURN serverBD.eStage
ENDFUNC

/// PURPOSE:
///    Sets the new server stage
PROC SET_SERVER_MISSION_STAGE(MISSION_STAGE_ENUM newStage)
	PRINTLN("[GB YACHT ATTACK] [STMGB] - SET_SERVER_MISSION_STAGE - setting new server mission stage to ", GET_MISSION_STAGE_STRING(newStage))
	serverBD.eStage = newStage
ENDPROC

/// PURPOSE: Gets the server's current mission state   
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC

/// PURPOSE: Sets the server's new mission state
PROC SET_SERVER_MISSION_STATE(INT iState)
	PRINTLN("[GB YACHT ATTACK] [STMGB] - SET_SERVER_MISSION_STATE - setting new server mission state to ", GET_MISSION_STATE_STRING(iState))
	serverBD.iServerGameState = iState
ENDPROC
// ------------------------------------------------------ |<

// Client ----------------------------------------------- >|

/// PURPOSE: Gets the passed client's current stage  
FUNC MISSION_STAGE_ENUM GET_CLIENT_MISSION_STAGE(INT iPlayer)

	IF iPlayer < 0 OR iPlayer > 32
		PRINTLN("[GB YACHT ATACK] - GET_CLIENT_MISSION_STAGE - passed player int is < 0 or > -1, which means participant_id_to_int() is returning an invalid number.")
		RETURN eMS_IDLE
	ENDIF

	RETURN playerBD[iPlayer].eStage
ENDFUNC

/// PURPOSE: Sets the passed client's new stage
PROC SET_CLIENT_MISSION_STAGE(INT iPlayer, MISSION_STAGE_ENUM newStage)
	IF iPlayer > -1 and iPlayer < 32
		PRINTLN("[GB YACHT ATTACK] [STMGB] - SET_CLIENT_MISSION_STAGE - setting ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iPlayer)), "'s mission stage to ", GET_MISSION_STAGE_STRING(newStage))
		playerBD[iPlayer].eStage = newStage
	ELSE
		PRINTLN("[GB YACHT ATTACK] [STMGB] - SET_CLIENT_MISSION_STAGE - passed int is out of range ")
	ENDIF
ENDPROC

/// PURPOSE: Gets the passed client's current mission state  
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	IF iPlayer < 0 OR iPlayer > 32
		PRINTLN("[GB YACHT ATACK] - GET_CLIENT_MISSION_STATE - passed player int is < 0 or > -1, which means participant_id_to_int() is returning an invalid number.")
		RETURN -1
	ENDIF
	
	RETURN playerBD[iPlayer].iClientGameState
ENDFUNC

/// PURPOSE: Sets the passed client's new mission state 
PROC SET_CLIENT_MISSION_STATE(INT iPlayer,INT iState)
	IF iPlayer > -1 AND iPlayer < 32
		PRINTLN("[GB YACHT ATTACK] [STMGB] - SET_CLIENT_MISSION_STATE - setting ",  GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iPlayer)), "'s mission state to ", GET_MISSION_STATE_STRING(iState))
		playerBD[iPlayer].iClientGameState = iState
	ELSE
		PRINTLN("[GB YACHT ATTACK] [STMGB] - SET_CLIENT_MISSION_STATE - passed int is out of range ")
	ENDIF
ENDPROC
// ------------------------------------------------------ |<

//----------------------
//	FUNCTIONS
//----------------------

// General checks --------------------------------------- >|

/// PURPOSE:
///    Checks if the UI should be hidden for the event
/// RETURNS:
///    
FUNC BOOL SHOULD_UI_BE_HIDDEN()
	IF GB_SHOULD_HIDE_GANG_BOSS_EVENT(FMMC_TYPE_GB_YACHT_ROBBERY)
		PRINTLN("[GB YACHT ATTACK SPAM] - SHOULD_UI_BE_HIDDEN - Yes - GB_SHOULD_HIDE_GANG_BOSS_EVENT(FMMC_TYPE_GB_YACHT_ROBBERY)")
		RETURN TRUE
	ENDIF
	
	IF GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
		PRINTLN("[GB YACHT ATTACK SPAM] - SHOULD_UI_BE_HIDDEN - Yes - GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()")
		RETURN TRUE
	ENDIF
	
	IF NOT MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
		PRINTLN("[GB YACHT ATTACK SPAM] - SHOULD_UI_BE_HIDDEN - Yes - NOT MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI")
		RETURN TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bDebugHideYachtAttackUI
		PRINTLN("[GB YACHT ATTACK SPAM] - SHOULD_UI_BE_HIDDEN - Yes - DEBUG widget bool bDebugHideYachtAttackUI is TRUE")
		RETURN TRUE
	ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL ARE_THERE_ANY_ATTACKER_SEA_VEHICLES()
	IF serverBD.sAttackerVehStruct.iNumAttackerBoats > 0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_THERE_ANY_ATTACKER_AIR_VEHICLES()
	IF serverBD.sAttackerVehStruct.iNumAttackerHelis > 0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// ------------------------------------------------------ |<

// Condition checks ------------------------------------- >|

/// PURPOSE:
///    Returns if the server has registered the end of the robbery
/// RETURNS:
///    TRUE if the event has ended
FUNC BOOL HAS_YACHT_ATTACK_ENDED()
	RETURN IS_BIT_SET(serverBD.iServerBitSet, biS_YachtAttackEnded)
ENDFUNC

/// PURPOSE:
///    Wrapper for a bitset check for biS_BridgeTaken
/// RETURNS:
///    TRUE if the bridge has been taken by a player
FUNC BOOL HAS_BRIDGE_BEEN_TAKEN()
	RETURN IS_BIT_SET(serverBD.iServerBitSet, biS_BridgeTaken)
ENDFUNC

/// PURPOSE:
///    Returns if the gang have finished preparing (declared by the boss)
/// RETURNS:
///    TRUE if the boss and goons are ready for the assault
FUNC BOOL HAS_GANG_COMPLETED_PREPARATIONS()
	RETURN IS_BIT_SET(serverBD.iServerBitSet, biS_GangPrepComplete)
ENDFUNC

/// PURPOSE:
///    Returns if the boss is no longer in session or has abandoned the ship
/// RETURNS:
///    
FUNC BOOL HAS_BOSS_ABANDONED_EVENT()
	RETURN IS_BIT_SET(serverBD.iServerBitSet, biS_GangBossAbandoned)
ENDFUNC

// ------------------------------------------------------ |<

// Blips ------------------------------------------------ >|
/// PURPOSE: Returns the yacht's heading.
FUNC FLOAT GET_YACHT_HEADING()
	RETURN serverBD.fYachtHeading
ENDFUNC

PROC ADD_YACHT_BLIP()
	IF NOT DOES_BLIP_EXIST(YachtBlip)
		YachtBlip = CREATE_BLIP_FOR_COORD(serverBD.vCapturePointLocation)
		SET_BLIP_SPRITE(YachtBlip, RADAR_TRACE_YACHT)
		SET_BLIP_PRIORITY(YachtBlip, BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
		SET_BLIP_NAME_FROM_TEXT_FILE(YachtBlip, "GB_YA_BN0")
		SET_BLIP_SCALE(YachtBlip, GB_BLIP_SIZE)
	ELSE
		IF GET_BLIP_COLOUR(YachtBlip) != BLIP_COLOUR_YELLOW
			SET_BLIP_COLOUR(YachtBlip, BLIP_COLOUR_YELLOW)
		ENDIF
	ENDIF	
ENDPROC

PROC REMOVE_YACHT_BLIP()
	IF DOES_BLIP_EXIST(YachtBlip)
		REMOVE_BLIP(YachtBlip)
	ENDIF
ENDPROC

/// PURPOSE:
///    Adds a blip for the bridge
PROC ADD_BRIDGE_BLIP()
	IF NOT DOES_BLIP_EXIST(BridgeBlip)
		VECTOR vPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(serverBD.vCapturePointLocation, GET_YACHT_HEADING(), <<1.5, 0.0, 0.0>>)
		BridgeBlip = ADD_BLIP_FOR_AREA (vPos, 10.5, 10.5) 
		SET_BLIP_ROTATION(BridgeBlip, ROUND(GET_YACHT_HEADING()))
		SET_BLIP_ALPHA(BridgeBlip, 180)
		SET_BLIP_PRIORITY(BridgeBlip, BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
		SET_BLIP_NAME_FROM_TEXT_FILE(BridgeBlip, "GB_YA_BN1")
	ELSE
		IF GET_BLIP_COLOUR(BridgeBlip) != BLIP_COLOUR_YELLOW
			SET_BLIP_COLOUR(BridgeBlip, BLIP_COLOUR_YELLOW)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Remove the bridge blip
PROC REMOVE_BRIDGE_BLIP()
	IF DOES_BLIP_EXIST(BridgeBlip)
		REMOVE_BLIP(BridgeBlip)
	ENDIF
ENDPROC

PROC ADD_ATTACKER_VEH_BLIPS()
	IF NOT GB_IS_LOCAL_PLAYER_MEMBER_OF_THIS_GANG(serverBD.piGangBoss)
		INT i
		IF ARE_THERE_ANY_ATTACKER_SEA_VEHICLES()
			REPEAT serverBD.sAttackerVehStruct.iNumAttackerBoats i
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niAttackerSeaVeh[i])
					IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niAttackerSeaVeh[i]))
					AND NOT IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niAttackerSeaVeh[i]))
					AND NOT SHOULD_UI_BE_HIDDEN()
						IF NOT DOES_BLIP_EXIST(AttackerSeaVehBlip[i])
							AttackerSeaVehBlip[i] = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(serverBD.niAttackerSeaVeh[i]))
							SET_BLIP_SPRITE(AttackerSeaVehBlip[i], RADAR_TRACE_PLAYER_BOAT)
							SET_BLIP_NAME_FROM_TEXT_FILE(AttackerSeaVehBlip[i], "GB_YA_BN2")
							SET_BLIP_ROTATION(AttackerSeaVehBlip[i], ROUND(GET_ENTITY_HEADING(NET_TO_VEH(serverBD.niAttackerSeaVeh[i]))))
							SET_BLIP_PRIORITY(AttackerSeaVehBlip[i], BLIP_PRIORITY_HIGHEST_SPECIAL_LOW)
							SET_BLIP_AS_SHORT_RANGE(AttackerSeaVehBlip[i], TRUE)
						ENDIF
					ELSE
						IF DOES_BLIP_EXIST(AttackerSeaVehBlip[i])
							REMOVE_BLIP(AttackerSeaVehBlip[i])
						ENDIF
					ENDIF				
				ENDIF
			ENDREPEAT
		ENDIF
		
		i = 0
		IF ARE_THERE_ANY_ATTACKER_AIR_VEHICLES()
			REPEAT serverBD.sAttackerVehStruct.iNumAttackerHelis i
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niAttackerAirVeh[i])
					IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niAttackerAirVeh[i]))
					AND NOT IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niAttackerAirVeh[i]))
					AND NOT SHOULD_UI_BE_HIDDEN()
						IF NOT DOES_BLIP_EXIST(AttackerAirVehBlip[i])
							AttackerAirVehBlip[i] = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(serverBD.niAttackerAirVeh[i]))
							SET_BLIP_SPRITE(AttackerAirVehBlip[i], RADAR_TRACE_HELICOPTER)
							SET_BLIP_NAME_FROM_TEXT_FILE(AttackerAirVehBlip[i], "GB_YA_BN3")
							SET_BLIP_PRIORITY(AttackerAirVehBlip[i], BLIP_PRIORITY_HIGHEST_SPECIAL_LOW)
							SET_BLIP_AS_SHORT_RANGE(AttackerAirVehBlip[i], TRUE)
						ENDIF
					ELSE
						IF DOES_BLIP_EXIST(AttackerAirVehBlip[i])
							REMOVE_BLIP(AttackerAirVehBlip[i])
						ENDIF
					ENDIF				
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE: Returns if the gang boss is using a custom blip and custom colour 
FUNC BOOL IS_GANG_BOSS_USING_CUSTOM_BLIP()
	IF IS_PLAYER_USING_CUSTOM_BLIP_COLOUR(serverBD.piGangBoss)
	AND IS_PLAYER_USING_CUSTOM_SPRITE_BLIP(serverBD.piGangBoss)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Set a custom blip colour and sprite for the gang boss
PROC SET_GANG_BOSS_CUSTOM_BLIP(HUD_COLOURS colour, BLIP_SPRITE sprite)
	SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(serverBD.piGangBoss, GET_BLIP_COLOUR_FROM_HUD_COLOUR(colour), TRUE)
	SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(serverBD.piGangBoss, sprite, TRUE)
ENDPROC

/// PURPOSE: Removes the gang boss' custom blip sprite and colour if they have one
PROC REMOVE_GANG_BOSS_CUSTOM_BLIP()
ENDPROC

/// PURPOSE: Checks if the passed player is using a custom blip sprite and colour
FUNC BOOL IS_THIS_PLAYER_USING_CUSTOM_BLIP(PLAYER_INDEX thisPlayer)
	IF IS_PLAYER_USING_CUSTOM_BLIP_COLOUR(thisPlayer)
	AND IS_PLAYER_USING_CUSTOM_SPRITE_BLIP(thisPlayer)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Sets the passed players' blip sprite and colour to the passed sprite and colour. 
PROC SET_PLAYER_CUSTOM_BLIP(PLAYER_INDEX thisPlayer, HUD_COLOURS colour, BLIP_SPRITE sprite)
	SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(thisPlayer, GET_BLIP_COLOUR_FROM_HUD_COLOUR(colour), TRUE)
	SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(thisPlayer, sprite, TRUE)
ENDPROC

/// PURPOSE: Removes a custom blip sprite and colour from the passed player if they have one
PROC REMOVE_PLAYER_CUSTOM_BLIP(PLAYER_INDEX thisPlayer)
	UNUSED_PARAMETER(thisPlayer)
ENDPROC

/// PURPOSE:
///    Remove all blips created by this script
PROC REMOVE_ALL_BLIPS
	REMOVE_BRIDGE_BLIP()
	REMOVE_YACHT_BLIP()
ENDPROC

// ------------------------------------------------------ |<

// Yacht checks ----------------------------------------- >|

// YACHT

/// PURPOSE: Sets the server coords for the yacht to the vector passed.
PROC SET_YACHT_COORDS(VECTOR thisCoord)
	PRINTLN("[GB YACHT ATTACK] [STMGB] - SET_YACHT_COORDS - Yacht is located at ", thisCoord)
	serverBD.vYachtCoords = thisCoord
ENDPROC

/// PURPOSE: Returns the location of the yacht.
FUNC VECTOR GET_YACHT_COORDS()
	RETURN serverBD.vYachtCoords
ENDFUNC

/// PURPOSE:
///    Sets the yachts heading
PROC SET_YACHT_HEADING(FLOAT thisHeading)
	PRINTLN("[GB YACHT ATTACK] [STMGB] - SET_YACHT_HEADING - Yacht heading is ", thisHeading)
	serverBD.fYachtHeading = thisHeading
ENDPROC

// CIRCULAR CAPTURE POINT

// Set the coords of the capture point
PROC SET_CAPTURE_POINT_COORDS(VECTOR thisLocation)
	PRINTLN("[GB YACHT ATTACK] [STMGB] - SET_CAPTURE_POINT_COORDS - bridge coords set to: ", thisLocation)
	serverBD.vCapturePointLocation = thisLocation		
ENDPROC

// Get the coords of the capture point   
FUNC VECTOR GET_CAPTURE_POINT_COORDS()
	RETURN serverBD.vCapturePointLocation
ENDFUNC

// Set the dimensions
PROC SET_CAPTURE_POINT_DIMENSIONS(VECTOR newDimensions)
	PRINTLN("[GB YACHT ATTACK] [STMGB] - SET_CAPTURE_POINT_DIMENSIONS - bridge dimensions set to: ", newDimensions)
	serverBD.vCapturePointDimensions = newDimensions
ENDPROC

// Get the dimensions 
FUNC VECTOR GET_CAPTURE_POINT_DIMENSIONS()
	RETURN serverBD.vCapturePointDimensions
ENDFUNC

// Dimensions for the marker
FUNC VECTOR GET_CAPTURE_POINT_MARKER_DIMENSTIONS()
	RETURN <<1.0, 1.0, 1.5>>
ENDFUNC


// CAPTURE ZONE ANGLED AREA

PROC SET_CAPTURE_ZONE_POINT_1(VECTOR thisVector)
	PRINTLN("[GB YACHT ATTACK] [STMGB] - SET_CAPTURE_ZONE_POINT_1 - setting to ", thisVector)
	serverBD.vCaptureZonePoint1 = thisVector
ENDPROC

FUNC VECTOR GET_CAPTURE_ZONE_POINT_1_COORDS()
	RETURN serverBD.vCaptureZonePoint1
ENDFUNC

PROC SET_CAPTURE_ZONE_POINT_2(VECTOR thisVector)
	PRINTLN("[GB YACHT ATTACK] [STMGB] - SET_CAPTURE_ZONE_POINT_2 - setting to ", thisVector)
	serverBD.vCaptureZonePoint2 = thisVector
ENDPROC

FUNC VECTOR GET_CAPTURE_ZONE_POINT_2_COORDS()
	RETURN serverBD.vCaptureZonePoint2
ENDFUNC

PROC SET_CAPTURE_ZONE_WIDTH(FLOAT thisWidth)
	PRINTLN("[GB YACHT ATTACK] [STMGB] - SET_CAPTURE_ZONE_WIDTH - setting to ", thisWidth)
	serverBD.fCaptureZoneWidth = thisWidth
ENDPROC

FUNC FLOAT GET_CAPTURE_ZONE_WIDTH()
	RETURN serverBD.fCaptureZoneWidth
ENDFUNC

FUNC BOOL AM_I_IN_THE_CAPTURE_AREA()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		RETURN IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), GET_CAPTURE_ZONE_POINT_1_COORDS(), GET_CAPTURE_ZONE_POINT_2_COORDS(), GET_CAPTURE_ZONE_WIDTH())
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANY_MEMBER_OF_MY_ORGANIZATION_IN_CAPTURE_AREA()
	IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
		INT i
		REPEAT NUM_NETWORK_PLAYERS i
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
				PLAYER_INDEX player = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
				PED_INDEX ped = GET_PLAYER_PED(player)
				
				IF player != INVALID_PLAYER_INDEX()
					IF IS_NET_PLAYER_OK(player)
						IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(player, GB_GET_LOCAL_PLAYER_GANG_BOSS())
							IF IS_ENTITY_IN_ANGLED_AREA( ped, GET_CAPTURE_ZONE_POINT_1_COORDS(), GET_CAPTURE_ZONE_POINT_2_COORDS(), GET_CAPTURE_ZONE_WIDTH())
								PRINTLN("[GB YACHT ATTACK] - IS_ANY_MEMBER_OF_MY_ORGANIZATION_IN_CAPTURE_AREA - Yes!", GET_PLAYER_NAME(player), " is in the capture area")
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN FALSE
ENDFUNC

// ------------------------------------------------------ |<


// Spawning --------------------------------------------- >|

/// PURPOSE:
///    Returns one of the specific spawn coordinates the player should use
/// RETURNS:
///    
PROC GET_RANDOM_DEFENDER_RESPAWN_LOCATION_AND_HEADING(VECTOR& vLocation, FLOAT& fHeading)
	SWITCH GET_RANDOM_INT_IN_RANGE(0, 10)
		CASE 0
			vLocation = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_YACHT_COORDS(), GET_YACHT_HEADING(), vDefenderSpawnOffset[0])
			fHeading = (-GET_YACHT_HEADING())
		BREAK
		
		CASE 1
			vLocation = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_YACHT_COORDS(), GET_YACHT_HEADING(), vDefenderSpawnOffset[1])
			fHeading = (-GET_YACHT_HEADING())
		BREAK
		
		CASE 2
			vLocation = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_YACHT_COORDS(), GET_YACHT_HEADING(), vDefenderSpawnOffset[2])
			fHeading = (-GET_YACHT_HEADING())
		BREAK
		
		CASE 3
			vLocation = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_YACHT_COORDS(), GET_YACHT_HEADING(), vDefenderSpawnOffset[3])
			fHeading = (-GET_YACHT_HEADING())
		BREAK
		
		CASE 4
			vLocation = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_YACHT_COORDS(), GET_YACHT_HEADING(), vDefenderSpawnOffset[4])
			fHeading = (-GET_YACHT_HEADING())
		BREAK
		
		CASE 5
			vLocation = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_YACHT_COORDS(), GET_YACHT_HEADING(), vDefenderSpawnOffset[5])
			fHeading = (-GET_YACHT_HEADING())
		BREAK
		
		CASE 6
			vLocation = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_YACHT_COORDS(), GET_YACHT_HEADING(), vDefenderSpawnOffset[6])
			fHeading = (GET_YACHT_HEADING())
		BREAK
		
		CASE 7
			vLocation = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_YACHT_COORDS(), GET_YACHT_HEADING(), vDefenderSpawnOffset[7])
			fHeading = (GET_YACHT_HEADING())
		BREAK
		
		CASE 8
			vLocation = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_YACHT_COORDS(), GET_YACHT_HEADING(), vDefenderSpawnOffset[8])
			fHeading = (-GET_YACHT_HEADING())
		BREAK
		
		CASE 9
			vLocation = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_YACHT_COORDS(), GET_YACHT_HEADING(), vDefenderSpawnOffset[9])
			fHeading = (-GET_YACHT_HEADING())
		BREAK	
	ENDSWITCH
ENDPROC

// ------------------------------------------------------ |<

/// PURPOSE: Returns the player index of the gang boss
FUNC PLAYER_INDEX GET_GANG_BOSS_PLAYER_INDEX()
	RETURN serverBD.piGangBoss
ENDFUNC

FUNC PARTICIPANT_INDEX GET_GANG_BOSS_PARTICIPANT_INDEX()
	RETURN NETWORK_GET_PARTICIPANT_INDEX(serverBD.piGangBoss)
ENDFUNC

/// PURPOSE: Returns TRUE if the gang boss of the robbery is active in session
FUNC BOOL IS_GANG_BOSS_ACTIVE_IN_SESSION()
	IF GET_GANG_BOSS_PLAYER_INDEX() != INVALID_PLAYER_INDEX()
		IF NETWORK_IS_PLAYER_ACTIVE(GET_GANG_BOSS_PLAYER_INDEX())
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Returns TRUE if you are the gang boss in the yacht robbery
FUNC BOOL AM_I_THE_GANG_BOSS()
	IF PLAYER_ID() = GET_GANG_BOSS_PLAYER_INDEX()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Returns TRUE if you are one of the goons in the Boss' gang who should defend the yacht
FUNC BOOL AM_I_A_GANG_GOON()
	RETURN GB_IS_LOCAL_PLAYER_MEMBER_OF_THIS_GANG(GET_GANG_BOSS_PLAYER_INDEX(), FALSE)
ENDFUNC

/// PURPOSE:
///    Checks if the passed player is the gang boss of the Yacht Attack
/// PARAMS:
///    thisPlayer - 
/// RETURNS:
///    
FUNC BOOL IS_THIS_PLAYER_THE_GANG_BOSS(PLAYER_INDEX thisPlayer)
	IF thisPlayer != INVALID_PLAYER_INDEX()
		IF NETWORK_IS_PLAYER_ACTIVE(thisPlayer)
			IF GET_GANG_BOSS_PLAYER_INDEX() = thisPlayer
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the passed player is a gang good of the Yacht Attack
/// PARAMS:
///    thisPlayer - 
/// RETURNS:
///    
FUNC BOOL IS_THIS_PLAYER_A_GANG_GOON(PLAYER_INDEX thisPlayer)
	RETURN GB_IS_PLAYER_MEMBER_OF_THIS_GANG(thisPlayer, GET_GANG_BOSS_PLAYER_INDEX(), FALSE)
ENDFUNC

FUNC VECTOR GET_LOCAL_PLAYER_COORDS()
	RETURN GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
ENDFUNC

FUNC BOOL AM_I_WITHIN_PARTICIPATION_RANGE()

	IF AM_I_THE_GANG_BOSS()
	OR AM_I_A_GANG_GOON()
		IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), GET_YACHT_COORDS(), TRUE) < 200
		OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biP_GoonAtYacht)
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN GB_IS_GLOBAL_CLIENT_BIT0_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_0_TEMPORARY_PARTICIPANT)
ENDFUNC

FUNC BOOL AM_I_A_PERMANENT_PARTICIPANT()
	RETURN GB_IS_GLOBAL_CLIENT_BIT0_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_0_PERMANENT_PARTICIPANT)
ENDFUNC

/// PURPOSE: Returns TRUE if the local player is near the yacht
FUNC BOOL AM_I_NEAR_YACHT()
	IF VDIST2(GET_LOCAL_PLAYER_COORDS(), GET_YACHT_COORDS()) < 200
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_ON_YACHT(PLAYER_ID(), GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(GET_GANG_BOSS_PLAYER_INDEX()))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if a specific player is near the yacht
/// PARAMS:
///    thisPlayer - The player to check
/// RETURNS:
///    True if so
FUNC BOOL IS_THIS_PLAYER_NEAR_YACHT(PLAYER_INDEX thisPlayer)
	PED_INDEX playerPed = GET_PLAYER_PED(thisPlayer)
	IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(playerPed, GET_YACHT_COORDS()) < 100
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// ------------------------------------------------------ |<

// Help text, shards and tickers ------------------------ >|

PROC SET_PLAYER_SHOULD_SEE_EVENT_HELP(BOOL bShouldSee)
	IF bShouldSee = TRUE
		IF bShouldSeeHelp = FALSE
			bShouldSeeHelp = TRUE
		ENDIF
	ELSE
		IF bShouldSeeHelp = TRUE
			bShouldSeeHelp = FALSE
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_PLAYER_SEE_EVENT_HELP()
	RETURN bShouldSeeHelp
ENDFUNC

PROC SET_PLAYER_SHOULD_SEE_BACKGROUND_HELP(BOOL bShouldSee)
	IF bShouldSee = TRUE
		IF bShouldSeeBackgroundHelp = FALSE
			bShouldSeeBackgroundHelp = TRUE
		ENDIF
	ELSE
		IF bShouldSeeBackgroundHelp = TRUE
			bShouldSeeBackgroundHelp = FALSE
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_PLAYER_SEE_BACKGROUND_HELP()
	RETURN bShouldSeeBackgroundHelp
ENDFUNC

/// PURPOSE:
///    Performs general checks to see if its safe to display help text
/// RETURNS:
///    
FUNC BOOL IS_SAFE_FOR_HELP_TEXT()
	IF  NOT IS_CUSTOM_MENU_ON_SCREEN()
	AND NOT IS_PAUSE_MENU_ACTIVE()
	AND NOT IS_RADAR_HIDDEN()
	AND IS_NET_PLAYER_OK(PLAYER_ID())
	AND NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Removes any currently displaying help text so that only the shard is on screen
PROC CLEAR_ANY_HELP()
	CLEAR_HELP()
ENDPROC

PROC CLEAR_THIS_HELP(INT thisHelp)
	TEXT_LABEL_63 help = "GB_YA_HP"
	help += thisHelp
	IF thisHelp = 4
		IF IS_THIS_HELP_MESSAGE_WITH_PLAYER_NAME_BEING_DISPLAYED(help, GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(GET_GANG_BOSS_PLAYER_INDEX()))
			CLEAR_HELP()
		ENDIF
	ELSE
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(help)
			CLEAR_HELP()
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Draws help text to the screen based on the index passed in
/// PARAMS:
///    iHelp - 
PROC DO_HELP_TEXT(INT iHelp)

	// Dont do help text if UI should be hidden
	IF SHOULD_UI_BE_HIDDEN()
		IF NOT IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), FALSE)
			CLEAR_ANY_HELP()
		ENDIF
	
		EXIT
	ENDIF
	
	SWITCH iHelp
		CASE 0		// ->|		You have initiated a yacht attack. Defend your yacht with your Goons to earn cash and RP rewards.
			IF NOT IS_BIT_SET(iHelpBitSet, iHelp)
				IF IS_SAFE_FOR_HELP_TEXT()
					IF SHOULD_PLAYER_SEE_EVENT_HELP()
						PRINT_HELP_NO_SOUND("GB_YA_HP0")
						GB_SET_GANG_BOSS_HELP_BACKGROUND()
						SET_BIT(iHelpBitSet, iHelp)
						PRINTLN("[GB YACHT ATTACK] [STMGB] - DO_HELP_TEXT - Help ", iHelp, " done: ", GET_FILENAME_FOR_AUDIO_CONVERSATION("GB_YA_HP0"))
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 1		// ->|		You have initiated a yacht attack. Defend your Boss' yacht to earn cash and RP rewards.
			IF NOT IS_BIT_SET(iHelpBitSet, iHelp)
				IF IS_SAFE_FOR_HELP_TEXT()
					IF SHOULD_PLAYER_SEE_EVENT_HELP()
						IF IS_PLAYER_ON_YACHT(PLAYER_ID(), serverBD.iYachtToUse)
							PRINT_HELP_NO_SOUND("GB_YA_HP1")
						ELSE
							PRINT_HELP_NO_SOUND("GB_YA_HP1b")
						ENDIF
						GB_SET_GANG_BOSS_HELP_BACKGROUND()
						SET_BIT(iHelpBitSet, iHelp)
						PRINTLN("[GB YACHT ATTACK] [STMGB] - DO_HELP_TEXT - Help ", iHelp, " done: ", GET_FILENAME_FOR_AUDIO_CONVERSATION("GB_YA_HP1"))
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 2		// ->|		~a~ has initiated a Yacht Attack. Assault their yacht and capture the ~y~Upper Deck~s~ to earn cash and RP rewards. 
			IF NOT IS_BIT_SET(iHelpBitSet, iHelp)
				IF IS_SAFE_FOR_HELP_TEXT()
					IF SHOULD_PLAYER_SEE_EVENT_HELP()
					OR SHOULD_PLAYER_SEE_BACKGROUND_HELP()
						//PRINT_HELP_WITH_PLAYER_NAME_NO_SOUND("GB_YA_HP2b", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(GET_GANG_BOSS_PLAYER_INDEX()), GET_PLAYER_HUD_COLOUR(GET_GANG_BOSS_PLAYER_INDEX()))
						PRINT_HELP_WITH_LITERAL_STRING_WITH_HUD_COLOUR_NO_SOUND("GB_YA_HP2", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(GET_GANG_BOSS_PLAYER_INDEX()), GET_PLAYER_HUD_COLOUR(GET_GANG_BOSS_PLAYER_INDEX()))
						GB_SET_GANG_BOSS_HELP_BACKGROUND()
						SET_BIT(iHelpBitSet, iHelp)
						PRINTLN("[GB YACHT ATTACK] [STMGB] - DO_HELP_TEXT - Help ", iHelp, " done: ", GET_FILENAME_FOR_AUDIO_CONVERSATION("GB_YA_HP2"),"; ", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(GET_GANG_BOSS_PLAYER_INDEX()))
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 3		// ->|		The Upper Deck is being captured by the attackers. Eliminate them to secure it.
			IF NOT IS_BIT_SET(iHelpBitSet, iHelp)
				IF IS_SAFE_FOR_HELP_TEXT()
					IF SHOULD_PLAYER_SEE_EVENT_HELP()
						PRINT_HELP_NO_SOUND("GB_YA_HP3")
						GB_SET_GANG_BOSS_HELP_BACKGROUND()
						SET_BIT(iHelpBitSet, iHelp)
						PRINTLN("[GB YACHT ATTACK] [STMGB] - DO_HELP_TEXT - Help ", iHelp, " done: ", GET_FILENAME_FOR_AUDIO_CONVERSATION("GB_YA_HP3"))
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 4		// ->|		The Upper Deck is being captured. Keep the defenders at bay until it is fully captured.
			IF NOT IS_BIT_SET(iHelpBitSet, iHelp)
				IF IS_SAFE_FOR_HELP_TEXT()
					IF SHOULD_PLAYER_SEE_EVENT_HELP()
						//PRINT_HELP_WITH_PLAYER_NAME_NO_SOUND("GB_YA_HP4", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(GET_GANG_BOSS_PLAYER_INDEX()), GET_PLAYER_HUD_COLOUR(GET_GANG_BOSS_PLAYER_INDEX()))
						PRINT_HELP_WITH_LITERAL_STRING_WITH_HUD_COLOUR_NO_SOUND("GB_YA_HP4", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(GET_GANG_BOSS_PLAYER_INDEX()), GET_PLAYER_HUD_COLOUR(GET_GANG_BOSS_PLAYER_INDEX()))
						GB_SET_GANG_BOSS_HELP_BACKGROUND()
						SET_BIT(iHelpBitSet, iHelp)
						PRINTLN("[GB YACHT ATTACK] [STMGB] - DO_HELP_TEXT - Help ", iHelp, " done: ", GET_FILENAME_FOR_AUDIO_CONVERSATION("GB_YA_HP4"), "; ", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(GET_GANG_BOSS_PLAYER_INDEX()))
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 5
			IF NOT IS_BIT_SET(iHelpBitSet, iHelp)
				IF IS_SAFE_FOR_HELP_TEXT()
					IF SHOULD_PLAYER_SEE_BACKGROUND_HELP()
						PRINT_HELP_NO_SOUND("GB_YA_HP5")
						GB_SET_GANG_BOSS_HELP_BACKGROUND()
						SET_BIT(iHelpBitSet, iHelp)
						PRINTLN("[GB YACHT ATTACK] [STMGB] - DO_HELP_TEXT - Help ", iHelp, " done: ", GET_FILENAME_FOR_AUDIO_CONVERSATION("GB_YA_HP5"))
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 6		// ->|		Players now know where your Yacht is anchored. Speak to your Captain if you would like to move it to another location around the coast of Los Santos and Blaine County.
			IF NOT IS_BIT_SET(iHelpBitSet, iHelp)
				IF IS_SAFE_FOR_HELP_TEXT()
					IF SHOULD_PLAYER_SEE_EVENT_HELP()
						PRINT_HELP_NO_SOUND("GB_YA_HP6")
						GB_SET_GANG_BOSS_HELP_BACKGROUND()
						SET_BIT(iHelpBitSet, iHelp)
						PRINTLN("[GB YACHT ATTACK] [STMGB] - DO_HELP_TEXT - Help ", iHelp, " done: ", GET_FILENAME_FOR_AUDIO_CONVERSATION("GB_YA_HP6"))
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 7		// ->|		You have been moved to shore as the Yacht Attack has ended.
			IF NOT IS_BIT_SET(iHelpBitSet, iHelp)
				IF IS_SAFE_FOR_HELP_TEXT()
					IF SHOULD_PLAYER_SEE_EVENT_HELP()
						PRINT_HELP_NO_SOUND("GB_YA_HP7")
						GB_SET_GANG_BOSS_HELP_BACKGROUND()
						SET_BIT(iHelpBitSet, iHelp)
						PRINTLN("[GB YACHT ATTACK] [STMGB] - DO_HELP_TEXT - Help ", iHelp, " done: ", GET_FILENAME_FOR_AUDIO_CONVERSATION("GB_YA_HP7"))
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 8
			IF NOT IS_BIT_SET(iHelpBitSet, iHelp)
				IF IS_SAFE_FOR_HELP_TEXT()
					IF SHOULD_PLAYER_SEE_EVENT_HELP()
						PRINT_HELP_NO_SOUND("GB_YA_HP8")
						GB_SET_GANG_BOSS_HELP_BACKGROUND()
						SET_BIT(iHelpBitSet, iHelp)
						PRINTLN("[GB YACHT ATTACK] [STMGB] - DO_HELP_TEXT - Help ", iHelp, " done: ", GET_FILENAME_FOR_AUDIO_CONVERSATION("GB_YA_HP8"))
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 9
			IF NOT IS_BIT_SET(iHelpBitSet, iHelp)
				IF IS_SAFE_FOR_HELP_TEXT()
					IF SHOULD_PLAYER_SEE_EVENT_HELP()
						PRINT_HELP_WITH_LITERAL_STRING_WITH_HUD_COLOUR_NO_SOUND("GB_YA_HP9", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(GET_GANG_BOSS_PLAYER_INDEX()), GET_PLAYER_HUD_COLOUR(GET_GANG_BOSS_PLAYER_INDEX()))
						GB_SET_GANG_BOSS_HELP_BACKGROUND()
						SET_BIT(iHelpBitSet, iHelp)
						PRINTLN("[GB YACHT ATTACK] [STMGB] - DO_HELP_TEXT - Help ", iHelp, " done: ", GET_FILENAME_FOR_AUDIO_CONVERSATION("GB_YA_HP9"), "; ", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(GET_GANG_BOSS_PLAYER_INDEX()))
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 10
			IF NOT IS_BIT_SET(iHelpBitSet, iHelp)
				IF IS_SAFE_FOR_HELP_TEXT()
					IF SHOULD_PLAYER_SEE_BACKGROUND_HELP()
						PRINT_HELP_NO_SOUND("GB_YA_HP10")
						GB_SET_GANG_BOSS_HELP_BACKGROUND()
						SET_BIT(iHelpBitSet, iHelp)
						PRINTLN("[GB YACHT ATTACK] [STMGB] - DO_HELP_TEXT - Help ", iHelp, " done: ", GET_FILENAME_FOR_AUDIO_CONVERSATION("GB_YA_HP10"))
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

PROC SET_PLAYER_SHOULD_SEE_EVENT_SHARDS(BOOL bShouldSee)
	IF bShouldSee = TRUE
		IF bShouldSeeShards = FALSE
			bShouldSeeShards = TRUE
		ENDIF
	ELSE
		IF bShouldSeeShards = TRUE
			bShouldSeeShards = FALSE
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL GET_PLAYER_SHOULD_SEE_EVENT_SHARDS()
	RETURN bShouldSeeHelp
ENDFUNC

/// PURPOSE:
///    Performs general checks for it being safe to display the shard
/// RETURNS:
///    True if safe
FUNC BOOL IS_SAFE_FOR_BIG_MESSAGE()	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND NOT IS_PAUSE_MENU_ACTIVE()
	AND NOT IS_CUSTOM_MENU_ON_SCREEN()
	AND GET_PLAYER_SHOULD_SEE_EVENT_SHARDS()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE				
ENDFUNC

#IF IS_DEBUG_BUILD
/// PURPOSE:
///    Prints information about the big message that has just been requested to draw
/// PARAMS:
///    iBigMessage - the index of the message that was drawn
///    title - the title of the message
///    strapline - the subtitle of the message
PROC PRINT_BIG_MESSAGE_DEBUG_INFORMATION(INT iBigMessage, STRING title, STRING strapline)
	PRINTLN("[GB YACHT ATTACK] [STMGB] - PRINT_BIG_MESSAGE_DEBUG_INFORMATION - Big Message:	", iBigMessage)
	PRINTLN("[GB YACHT ATTACK] [STMGB] - PRINT_BIG_MESSAGE_DEBUG_INFORMATION - Title: 			", GET_FILENAME_FOR_AUDIO_CONVERSATION(title))
	PRINTLN("[GB YACHT_ROB} - PRINT_BIG_MESSAGE_DEBUG_INFORMATION - Strapline:		", GET_FILENAME_FOR_AUDIO_CONVERSATION(strapline))
ENDPROC
#ENDIF

/// PURPOSE:
///    Requests to draw a big message to the screen, based on the message passed through
/// PARAMS:
///    iBigMessage - 
PROC DO_BIG_MESSAGE(INT iBigMessage)

	// Dont do any big messages if UI should be hidden
	IF SHOULD_UI_BE_HIDDEN()
		IF NOT IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), FALSE)
			CLEAR_ALL_BIG_MESSAGES()
			CLEAR_ANY_HELP()
		ENDIF
		EXIT
	ENDIF
	
	SWITCH iBigMessage
		CASE 0 	// YACHT ATTACK - Defend your yacht from attackers
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_HELP()
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_START_OF_JOB, "GB_YA_BMT0", "GB_YA_BMS0")
					SET_BIT(iBigMessageBitSet, iBigMessage)
					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_YA_BMT0", "GB_YA_BMS0") #ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 1	// YACHT ATTACK - Attack the yacht and take the upper bridge
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_HELP()
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_START_OF_JOB, "GB_YA_BMT0", "GB_YA_BMS1")
					SET_BIT(iBigMessageBitSet, iBigMessage)
					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_YA_BMT0", "GB_YA_BMS1") #ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 2	// WINNER - You successfully defended the yacht
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_HELP()
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "GB_WINNER", "GB_YA_BMS2")
					SET_BIT(iBigMessageBitSet, iBigMessage)
					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_WINNER", "GB_YA_BMS2") #ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 3 // WINNER - You successfully captured the upper deck
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_HELP()
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "GB_WINNER", "GB_YA_BMS3")
					SET_BIT(iBigMessageBitSet, iBigMessage)
					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_WINNER", "GB_YA_BMS3") #ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 4	// WINNER - The Yacht was successfully captured
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_HELP()
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "GB_WINNER", "GB_YA_BMS4")
					SET_BIT(iBigMessageBitSet, iBigMessage)
					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_WINNER", "GB_YA_BMS4") #ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 5	// BOSS WORK OVER - ~a~ ~s~successfully captured the upper deck
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					IF serverBD.piCapturingPlayer != INVALID_PLAYER_INDEX()
						CLEAR_HELP()
						SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GB_END_OF_JOB_OVER, serverBD.piCapturingPlayer, -1, "GB_YA_BMS5", "GB_WORK_OVER")
						SET_BIT(iBigMessageBitSet, iBigMessage)
						#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_WORK_OVER", "GB_YA_BMS5") #ENDIF
					ELSE
						PRINTLN("[GB YACHT ATTACK] - DO_BIG_MESSAGE - 5 - invalid player index")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 6	// BOSS WORK OVER - The upper deck was captured by other players
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_HELP()
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "GB_YA_BMS6")
					SET_BIT(iBigMessageBitSet, iBigMessage)
					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_WORK_OVER", "GB_YA_BMS6") #ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 7	// BOSS WORK OVER - A member of ~a~ ~s~successfully captured the upper deck
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_HELP()
					SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "GB_YA_BMS7", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(PLAYER_ID()), GET_PLAYER_HUD_COLOUR(PLAYER_ID()))
					SET_BIT(iBigMessageBitSet, iBigMessage)
					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_WORK_OVER", "GB_YA_BMS7") #ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 8 // BOSS WORK OVER - You failed to defend the yacht
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_HELP()
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_FAIL, "GB_WORK_OVER", "GB_YA_BMS8")
					SET_BIT(iBigMessageBitSet, iBigMessage)
					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_WWORK_OVER", "GB_YA_BMS8") #ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 9 // BOSS WORK OVER - You failed to capture the upper deck
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_HELP()
					SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "GB_YA_BMS9", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(GET_GANG_BOSS_PLAYER_INDEX()), GET_PLAYER_HUD_COLOUR(GET_GANG_BOSS_PLAYER_INDEX()))
					SET_BIT(iBigMessageBitSet, iBigMessage)
					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_WWORK_OVER", "GB_YA_BMS9") #ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 10 // BOSS WORK OVER - The Organization Boss abandoned the event
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_HELP()
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "GB_YA_BMS10")
					SET_BIT(iBigMessageBitSet, iBigMessage)
					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_WWORK_OVER", "GB_YA_BMS10") #ENDIF
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

PROC DO_TICKER(INT iTicker)
	IF SHOULD_UI_BE_HIDDEN()
		EXIT
	ENDIF

	SWITCH iTicker
		CASE 0 
			PRINT_TICKER("GB_YA_TCK0") 
		BREAK
		CASE 1 
			PRINT_TICKER_WITH_CUSTOM_STRING_AND_INT("GB_YA_TCK1", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(GET_GANG_BOSS_PLAYER_INDEX()))
		BREAK
	ENDSWITCH
ENDPROC

// ------------------------------------------------------ |<

// Main Functions --------------------------------------- >|

PROC PROCESS_DAMAGE_EVENT(INT iCount)
	PED_INDEX VictimPed, DamagerPed
	PLAYER_INDEX VictimPlayer, DamagerPlayer

	STRUCT_ENTITY_DAMAGE_EVENT data
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, data, SIZE_OF(data))
		IF DOES_ENTITY_EXIST(data.VictimIndex)
			IF IS_ENTITY_A_PED(data.VictimIndex)
				VictimPed = GET_PED_INDEX_FROM_ENTITY_INDEX(data.VictimIndex)
				IF IS_PED_A_PLAYER(VictimPed)
					VictimPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(VictimPed)
					IF NETWORK_IS_PLAYER_A_PARTICIPANT(VictimPlayer)
						IF data.VictimDestroyed
							IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
								IF IS_THIS_PLAYER_A_GANG_GOON(VictimPlayer)
									serverBD.iGoonDeathCount++
								ENDIF
								IF IS_THIS_PLAYER_THE_GANG_BOSS(VictimPlayer)
									serverBD.iBossDeathCount++
								ENDIF
							ENDIF
						ENDIF
					
						IF NOT GB_IS_GLOBAL_CLIENT_BIT0_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_0_PERMANENT_PARTICIPANT)
							// damager
							IF DOES_ENTITY_EXIST(data.DamagerIndex)
								IF IS_ENTITY_A_PED(data.DamagerIndex)
									DamagerPed = GET_PED_INDEX_FROM_ENTITY_INDEX(data.DamagerIndex)
									IF IS_PED_A_PLAYER(DamagerPed)
										DamagerPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(DamagerPed)
										IF NETWORK_IS_PLAYER_A_PARTICIPANT(DamagerPlayer)
											IF DamagerPlayer = PLAYER_ID()
												IF NOT GB_IS_LOCAL_PLAYER_MEMBER_OF_THIS_GANG(GET_GANG_BOSS_PLAYER_INDEX())
													IF IS_THIS_PLAYER_A_GANG_GOON(VictimPlayer)
													OR IS_THIS_PLAYER_THE_GANG_BOSS(VictimPlayer)
														PRINTLN("[GB YACHT ATTACK] [STMGB] - PROCESS_DAMAGE_EVENT - Player has hurt a defender, they are now permanent")
														GB_SET_PLAYER_AS_PERMANENT_ON_BOSS_MISSION()
														GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_EVENTS()
	INT iCount
	EVENT_NAMES ThisScriptEvent
	
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount
	
		ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)
		
		IF ThisScriptEvent = EVENT_NETWORK_DAMAGE_ENTITY
			PROCESS_DAMAGE_EVENT(iCount)
		ENDIF
		
	ENDREPEAT
ENDPROC

FUNC BOOL HAS_PLAYER_FULLY_SPAWNED()
	IF IS_SCREEN_FADED_IN()
	AND GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState = RESPAWN_STATE_PLAYING
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_SAFE_FOR_END_MESSAGES()
	IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE)
	AND NOT IS_RADAR_HIDDEN()
	AND HAS_PLAYER_FULLY_SPAWNED()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE: The main function that handles the rewards and messages given and shown to all players at the end of the event
PROC PROCESS_END_REWARDS_AND_MESSAGES()
	
	IF IS_SAFE_FOR_END_MESSAGES()
		IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iClientBitSet, biP_RewardGiven)
		
//			DO_HELP_TEXT(5)
		
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_BossLeft)
				IF NOT AM_I_A_GANG_GOON()
				AND NOT AM_I_THE_GANG_BOSS()
					DO_BIG_MESSAGE(10)
				ENDIF
				GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_YACHT_ROBBERY, FALSE, sData)
				GANG_BOSS_MANAGE_WANTED_CLEAN_UP(FALSE, INVALID_PLAYER_INDEX())
				SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iClientBitSet, biP_RewardGiven)
				GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_BOSS_LEFT)
				
				EXIT
			ENDIF
		
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_SuccessfullyDefended)
				IF AM_I_THE_GANG_BOSS()
				OR AM_I_A_GANG_GOON()
					DO_BIG_MESSAGE(2)
					GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_YACHT_ROBBERY, TRUE, sData)
					GANG_BOSS_MANAGE_WANTED_CLEAN_UP(FALSE, PLAYER_ID())
					GB_SET_COMMON_TELEMETRY_DATA_ON_END(TRUE, GB_TELEMETRY_END_WON)
					SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iClientBitSet, biP_RewardGiven)
				ELSE
					IF AM_I_A_PERMANENT_PARTICIPANT()
					OR AM_I_WITHIN_PARTICIPATION_RANGE()
					OR GB_IS_PLAYER_A_QUALIFYING_PARTICIPANT(PLAYER_ID())
						
						DO_BIG_MESSAGE(9)
					ELSE
						DO_TICKER(1)
					ENDIF
					GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_YACHT_ROBBERY, FALSE, sData)
					GANG_BOSS_MANAGE_WANTED_CLEAN_UP(FALSE, PLAYER_ID())
					GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST)
					SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iClientBitSet, biP_RewardGiven)
				ENDIF
				
				// Exit here as we dont need to check other conditions
				EXIT
			ENDIF
			
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_BridgeTaken)
				IF AM_I_THE_GANG_BOSS()
				OR AM_I_A_GANG_GOON()
					DO_BIG_MESSAGE(8)
					GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_YACHT_ROBBERY, FALSE, sData)
					GANG_BOSS_MANAGE_WANTED_CLEAN_UP(FALSE, PLAYER_ID())
					GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST)
					SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iClientBitSet, biP_RewardGiven)
				ELSE
					// I am inside the capture area
					IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biP_InCaptureArea)
						
						DO_BIG_MESSAGE(3)
						sData.iTiedPlayers = serverBD.iNumPlayersInCaptureArea
						GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_YACHT_ROBBERY, TRUE, sData)
						GB_SET_COMMON_TELEMETRY_DATA_ON_END(TRUE, GB_TELEMETRY_END_WON)
						GANG_BOSS_MANAGE_WANTED_CLEAN_UP(FALSE, PLAYER_ID())
					// I was not in the capture area but a member of my organization was
					ELIF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biP_InCaptureArea)
					AND IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biP_GangMemberInCaptureArea)
						DO_BIG_MESSAGE(7)
						GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_YACHT_ROBBERY, FALSE, sData)
						GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST)
						GANG_BOSS_MANAGE_WANTED_CLEAN_UP(FALSE, PLAYER_ID())
					ELSE
						IF AM_I_A_PERMANENT_PARTICIPANT()
						OR AM_I_WITHIN_PARTICIPATION_RANGE()
						OR GB_IS_PLAYER_A_QUALIFYING_PARTICIPANT(PLAYER_ID())
							// IF there was only 1 player in capture area
							IF serverBD.iNumPlayersInCaptureArea = 1
								DO_BIG_MESSAGE(5)
							ELSE
								DO_BIG_MESSAGE(6)
							ENDIF
						ELSE
							DO_TICKER(0)
						ENDIF
						GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_YACHT_ROBBERY, FALSE, sData)
						GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST)
						GANG_BOSS_MANAGE_WANTED_CLEAN_UP(FALSE, PLAYER_ID())
					ENDIF
					
					SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iClientBitSet, biP_RewardGiven)
				ENDIF
				
				
				// Exit here as we dont need to check other conditions
				EXIT
			ENDIF
		ENDIF
	ENDIF
ENDPROC
// ------------------------------------------------------ |<

/// PURPOSE:
///    Returns if its safe to draw the event time in the bottom right
/// RETURNS:
///    
FUNC BOOL SHOULD_DISPLAY_TIMER()
	IF SHOULD_UI_BE_HIDDEN()
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DISPLAY_CAPTURE_BAR()
	IF AM_I_A_PERMANENT_PARTICIPANT()
	OR AM_I_WITHIN_PARTICIPATION_RANGE()
	OR GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

// Maintain functions ----------------------------------- >|

/// PURPOSE:
///    Maintains player blipping
PROC MAINTAIN_BLIPS()

	IF SHOULD_UI_BE_HIDDEN()
		REMOVE_ALL_BLIPS()
		
		IF IS_BLIP_HIDDEN_FOR_PRIVATE_YACHT(serverBD.iYachtToUse)
			SET_PRIVATE_YACHT_BLIP_AS_HIDDEN(serverBD.iYachtToUse, FALSE)
		ENDIF
		EXIT
	ENDIF

	IF HAS_NET_TIMER_STARTED(serverBD.DefendTimer)
		INT i		
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
				PLAYER_INDEX PlayerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))

				IF AM_I_THE_GANG_BOSS()
				OR AM_I_A_GANG_GOON()
					IF NOT IS_THIS_PLAYER_A_GANG_GOON(PlayerID)
					AND NOT IS_THIS_PLAYER_THE_GANG_BOSS(PlayerID)
						IF NOT IS_PLAYER_USING_CUSTOM_BLIP_COLOUR(PlayerID)
							SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(PlayerID, GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_RED), TRUE)
						ENDIF
					ENDIF
				ELSE
					IF IS_THIS_PLAYER_A_GANG_GOON(PlayerID)
					OR IS_THIS_PLAYER_THE_GANG_BOSS(PlayerID)
						IF NOT IS_PLAYER_USING_CUSTOM_BLIP_COLOUR(PlayerID)
							SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(PlayerID, GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_RED), TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF NOT IS_BLIP_HIDDEN_FOR_PRIVATE_YACHT(serverBD.iYachtToUse)
			SET_PRIVATE_YACHT_BLIP_AS_HIDDEN(serverBD.iYachtToUse, TRUE)
		ENDIF
		
		IF IS_PLAYER_ON_YACHT(PLAYER_ID(), serverBD.iYachtToUse)
			REMOVE_YACHT_BLIP()
			ADD_BRIDGE_BLIP()
		ELSE
			REMOVE_BRIDGE_BLIP()
			ADD_YACHT_BLIP()
		ENDIF
		ADD_ATTACKER_VEH_BLIPS()
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintains the objective text
PROC MAINTAIN_OBJECTIVE_TEXT()

	IF SHOULD_UI_BE_HIDDEN()
		IF IS_THERE_ANY_CURRENT_OBJECTIVE_TEXT_FROM_THIS_SCRIPT()
			CLEAR_ANY_OBJECTIVE_TEXT_FROM_THIS_SCRIPT()
		ENDIF
		
		EXIT
	ENDIF

	SWITCH GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT())
				
		CASE eMS_DEFEND
			IF NOT IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), FALSE)
				IF NOT HAS_THIS_MP_OBJECTIVE_TEXT_BEEN_RECEIVED("GB_YA_OT0")
					PRINT_OBJECTIVE_TEXT("GB_YA_OT0")
				ENDIF
			ENDIF
		BREAK
		
		CASE eMS_ATTACK
			IF NOT IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), FALSE)
				IF NOT HAS_THIS_MP_OBJECTIVE_TEXT_BEEN_RECEIVED("GB_YA_OT1")
					PRINT_OBJECTIVE_TEXT("GB_YA_OT1")
				ENDIF
			ENDIF
		BREAK
		
		CASE eMS_OFF_MISSION
			CLEAR_ANY_OBJECTIVE_TEXT_FROM_THIS_SCRIPT()
		BREAK
		
		CASE eMS_END
			CLEAR_ANY_OBJECTIVE_TEXT_FROM_THIS_SCRIPT()
		BREAK
		
		CASE eMS_CLEANUP
			CLEAR_ANY_OBJECTIVE_TEXT_FROM_THIS_SCRIPT()
		BREAK
	ENDSWITCH
ENDPROC


/// PURPOSE:
///    Checks if the server has moved to the end or cleanup stages and moves the client there if so
PROC MAINTAIN_CLIENT_MOVE_TO_END_CHECKS()
	IF GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT()) != eMS_CLEANUP
		IF GET_SERVER_MISSION_STAGE() = eMS_END
		AND GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT()) != eMS_END
		
			IF IS_THIS_PLAYER_A_GANG_GOON(PLAYER_ID())
			OR IS_THIS_PLAYER_THE_GANG_BOSS(PLAYER_ID())
				GB_SET_LOCAL_PLAYER_CRITICAL_TO_JOB(FALSE)
			ENDIF
			
			IF HAS_BRIDGE_BEEN_TAKEN()
				IF AM_I_IN_THE_CAPTURE_AREA()
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biP_InCaptureArea)
				ELIF IS_ANY_MEMBER_OF_MY_ORGANIZATION_IN_CAPTURE_AREA()
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biP_GangMemberInCaptureArea)
				ENDIF
			ENDIF
		
			PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_CLIENT_MOVE_TO_END_CHECKS - moving client to mission stage: eMS_END")
			SET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT(), eMS_END)
		ELIF GET_SERVER_MISSION_STAGE() = eMS_CLEANUP
		
			IF IS_THIS_PLAYER_A_GANG_GOON(PLAYER_ID())
			OR IS_THIS_PLAYER_THE_GANG_BOSS(PLAYER_ID())
				GB_SET_LOCAL_PLAYER_CRITICAL_TO_JOB(FALSE)
			ENDIF
		
			PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_CLIENT_MOVE_TO_END_CHECKS - moving client to mission stage: eMS_CLEANUP")
			SET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT(), eMS_CLEANUP)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Check if the local player should reset their spawn location after having been killed and fully respawned
/// RETURNS:
///    
FUNC BOOL SHOULD_RESET_SPAWN_LOCATIONS()

	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
		IF bBeenKilled = FALSE
			bBeenKilled = TRUE
		ENDIF
	ELSE
		IF HAS_PLAYER_FULLY_SPAWNED()
			IF bBeenKilled = TRUE
				bBeenKilled = FALSE
				PRINTLN("[GB YACHT ATTACK] [STMGB] - SHOULD_RESET_SPAWN_LOCATIONS - TRUE - player was killed and now has fully respawned, resetting spawn locations.")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

PROC SETUP_DEFENDER_CUSTOM_SPAWNS()
	ADD_CUSTOM_SPAWN_POINT(GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_YACHT_COORDS(), GET_YACHT_HEADING(), vDefenderSpawnOffset[0]), GET_OFFSET_HEADING_FROM_YACHT_AS_WORLD_HEADING(serverBD.iYachtToUse, fDefenderSpawnHeadingOffset[0]))
	ADD_CUSTOM_SPAWN_POINT(GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_YACHT_COORDS(), GET_YACHT_HEADING(), vDefenderSpawnOffset[1]), GET_OFFSET_HEADING_FROM_YACHT_AS_WORLD_HEADING(serverBD.iYachtToUse, fDefenderSpawnHeadingOffset[1]))
	ADD_CUSTOM_SPAWN_POINT(GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_YACHT_COORDS(), GET_YACHT_HEADING(), vDefenderSpawnOffset[2]), GET_OFFSET_HEADING_FROM_YACHT_AS_WORLD_HEADING(serverBD.iYachtToUse, fDefenderSpawnHeadingOffset[2]))
	ADD_CUSTOM_SPAWN_POINT(GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_YACHT_COORDS(), GET_YACHT_HEADING(), vDefenderSpawnOffset[3]), GET_OFFSET_HEADING_FROM_YACHT_AS_WORLD_HEADING(serverBD.iYachtToUse, fDefenderSpawnHeadingOffset[3]))
	ADD_CUSTOM_SPAWN_POINT(GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_YACHT_COORDS(), GET_YACHT_HEADING(), vDefenderSpawnOffset[4]), GET_OFFSET_HEADING_FROM_YACHT_AS_WORLD_HEADING(serverBD.iYachtToUse, fDefenderSpawnHeadingOffset[4]))
	ADD_CUSTOM_SPAWN_POINT(GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_YACHT_COORDS(), GET_YACHT_HEADING(), vDefenderSpawnOffset[5]), GET_OFFSET_HEADING_FROM_YACHT_AS_WORLD_HEADING(serverBD.iYachtToUse, fDefenderSpawnHeadingOffset[5]))
	ADD_CUSTOM_SPAWN_POINT(GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_YACHT_COORDS(), GET_YACHT_HEADING(), vDefenderSpawnOffset[6]), GET_OFFSET_HEADING_FROM_YACHT_AS_WORLD_HEADING(serverBD.iYachtToUse, fDefenderSpawnHeadingOffset[6]))
	ADD_CUSTOM_SPAWN_POINT(GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_YACHT_COORDS(), GET_YACHT_HEADING(), vDefenderSpawnOffset[7]), GET_OFFSET_HEADING_FROM_YACHT_AS_WORLD_HEADING(serverBD.iYachtToUse, fDefenderSpawnHeadingOffset[7]))
	ADD_CUSTOM_SPAWN_POINT(GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_YACHT_COORDS(), GET_YACHT_HEADING(), vDefenderSpawnOffset[8]), GET_OFFSET_HEADING_FROM_YACHT_AS_WORLD_HEADING(serverBD.iYachtToUse, fDefenderSpawnHeadingOffset[8]))
	ADD_CUSTOM_SPAWN_POINT(GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_YACHT_COORDS(), GET_YACHT_HEADING(), vDefenderSpawnOffset[9]), GET_OFFSET_HEADING_FROM_YACHT_AS_WORLD_HEADING(serverBD.iYachtToUse, fDefenderSpawnHeadingOffset[9]))		
ENDPROC

PROC MAINTAIN_SPAWN_LOCATIONS()
	IF SHOULD_UI_BE_HIDDEN()
		EXIT
	ENDIF	

	IF NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(),MPAM_TYPE_TIME_TRIAL)
	AND NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_RC_TIME_TRIAL)
	AND NOT IS_PLAYER_IN_CORONA()
	AND NOT IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), FALSE)
	AND NOT IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
		IF AM_I_WITHIN_PARTICIPATION_RANGE()
			IF AM_I_THE_GANG_BOSS()
			OR AM_I_A_GANG_GOON()
				IF bSetSpawnLocations = FALSE
				
					USE_CUSTOM_SPAWN_POINTS(TRUE)
												
					SETUP_DEFENDER_CUSTOM_SPAWNS()
					
					SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_CUSTOM_SPAWN_POINTS)
				
					#IF IS_DEBUG_BUILD
					PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_SPAWN_LOCATIONS - Adding custom spawn points for boss/goons,")
					#ENDIF
								
//					VECTOR spawnLoc
//					FLOAT spawnHeading
//					GET_RANDOM_DEFENDER_RESPAWN_LOCATION_AND_HEADING(spawnLoc, spawnHeading)
//					#IF IS_DEBUG_BUILD
//					PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_SPAWN_LOCATIONS - Setting spawn location for boss/goons to ", spawnLoc, "; facing heading: ", spawnHeading)
//					#ENDIF
//					SETUP_SPECIFIC_SPAWN_LOCATION(spawnLoc, spawnHeading, 30.0, TRUE, 20, TRUE, FALSE, 30, TRUE, TRUE)
					
					bSetSpawnLocations = TRUE
				ENDIF
			ELSE
				IF bSetSpawnLocations = FALSE
				OR SHOULD_RESET_SPAWN_LOCATIONS()
										
					SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_NEAR_SPECIFIC_COORDS)
					#IF IS_DEBUG_BUILD
					PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_SPAWN_LOCATIONS - Setting spawn area for attacker to ", serverBD.vAttackerSpawnLocation, "; facing heading: ", serverBD.fAttackerSpawnHeading)
					#ENDIF
					SETUP_SPECIFIC_SPAWN_LOCATION(serverBD.vAttackerSpawnLocation, serverBD.fAttackerSpawnHeading, serverBD.fAttackerSpawnRadius, TRUE, 40.0, TRUE, FALSE, 30, TRUE, TRUE)
					
					SET_PLAYER_WILL_SPAWN_FACING_COORDS(GET_YACHT_COORDS(), TRUE, FALSE)
					
					bSetSpawnLocations = TRUE
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[ST_DEBUG_SPAM] - Yacht Attack Spawn Spam - Not in participation range.")
			
			IF HAS_PLAYER_FULLY_SPAWNED()
				IF bSetSpawnLocations
					IF AM_I_THE_GANG_BOSS()
					OR AM_I_A_GANG_GOON()
						PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_SPAWN_LOCATIONS - Defending player is no longer within range of yacht - calling USE_CUSTOM_SPAWN_POINTS(FALSE)")
						CLEAR_CUSTOM_SPAWN_POINTS()
						USE_CUSTOM_SPAWN_POINTS(FALSE)
						bSetSpawnLocations = FALSE
					ELSE
						PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_SPAWN_LOCATIONS - Attacking player is no longer within range of yacht - calling CLEAR_PLAYER_NEXT_RESPAWN_LOCATION")
						CLEAR_PLAYER_NEXT_RESPAWN_LOCATION()
						CLEAR_SPECIFIC_SPAWN_LOCATION_FROM_THIS_SCRIPT()
						bSetSpawnLocations = FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(),MPAM_TYPE_TIME_TRIAL)
			PRINTLN("[ST_DEBUG_SPAM] - Yacht Attack Spawn Spam - IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(),MPAM_TYPE_TIME_TRIAL) - TRUE")
		ENDIF
		IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_RC_TIME_TRIAL)
			PRINTLN("[ST_DEBUG_SPAM] - Yacht Attack Spawn Spam - IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_RC_TIME_TRIAL) - TRUE")
		ENDIF
		IF IS_PLAYER_IN_CORONA()
			PRINTLN("[ST_DEBUG_SPAM] - Yacht Attack Spawn Spam - IS_PLAYER_IN_CORONA - TRUE")
		ENDIF
		IF IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), FALSE)
			PRINTLN("[ST_DEBUG_SPAM] - Yacht Attack Spawn Spam - IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), FALSE) - TRUE")
		ENDIF
		IF IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
			PRINTLN("[ST_DEBUG_SPAM] - Yacht Attack Spawn Spam - IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID()) - TRUE")
		ENDIF
	
		IF HAS_PLAYER_FULLY_SPAWNED()
			IF bSetSpawnLocations
				IF AM_I_THE_GANG_BOSS()
				OR AM_I_A_GANG_GOON()
					PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_SPAWN_LOCATIONS - Defending player is no longer within range of yacht - calling USE_CUSTOM_SPAWN_POINTS(FALSE)")
					CLEAR_CUSTOM_SPAWN_POINTS()
					USE_CUSTOM_SPAWN_POINTS(FALSE)
					bSetSpawnLocations = FALSE
				ELSE
					PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_SPAWN_LOCATIONS - Attacking player is no longer within range of yacht - calling CLEAR_PLAYER_NEXT_RESPAWN_LOCATION")
					CLEAR_PLAYER_NEXT_RESPAWN_LOCATION()
					CLEAR_SPECIFIC_SPAWN_LOCATION_FROM_THIS_SCRIPT()
					bSetSpawnLocations = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC MAINTAIN_ATTACKER_VEHICLES()
	INT iLoop
	
	IF ARE_THERE_ANY_ATTACKER_SEA_VEHICLES()
		REPEAT serverBD.sAttackerVehStruct.iNumAttackerBoats iLoop
			IF IS_BIT_SET(serverBD.iDeleteSeaVehBitSet, iLoop)
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niAttackerSeaVeh[iLoop])
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niAttackerSeaVeh[iLoop])
						DELETE_NET_ID(serverBD.niAttackerSeaVeh[iLoop])
						PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_ATTACKER_VEHICLES - deleting sea vehicle ", iLoop)
					ENDIF
				ENDIF
			ENDIF	
		ENDREPEAT
	ENDIF
		
	IF ARE_THERE_ANY_ATTACKER_AIR_VEHICLES()
		REPEAT serverBD.sAttackerVehStruct.iNumAttackerHelis iLoop
			IF IS_BIT_SET(serverBD.iDeleteAirVehBitSet, iLoop)
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niAttackerAirVeh[iLoop])
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niAttackerAirVeh[iLoop])
						DELETE_NET_ID(serverBD.niAttackerAirVeh[iLoop])
						PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_ATTACKER_VEHICLES - deleting air vehicle ", iLoop)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC
	
/// PURPOSE:
///    Draws the remaining event time in the bottom right of the screen
PROC MAINTAIN_BOTTOM_RIGHT_UI()
	IF SHOULD_DISPLAY_TIMER()		
		SWITCH GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT())
			
			CASE eMS_ATTACK
			CASE eMS_DEFEND
				IF HAS_NET_TIMER_STARTED(serverBD.DefendTimer)
					
					iTimeRemaining = (YA_TUNABLE_EVENT_DURATION() - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.DefendTimer))
					
					SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
					
					HUD_COLOURS timeColour
					IF iTimeRemaining > 30000
						timeColour = HUD_COLOUR_WHITE
					ELSE
						timeColour = HUD_COLOUR_RED
					ENDIF
					
//					IF iTimeRemaining <= 10000
//						IF bSoundStarted = FALSE
//							PLAY_SOUND_FRONTEND(iSoundIDCountdown,"10S","MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
//							bSoundStarted = TRUE
//						ENDIF
//					ENDIF
					
					IF iTimeRemaining > 0
						DRAW_GENERIC_TIMER(iTimeRemaining, "GB_WORK_END", DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, timeColour)
					ELSE
						IF bPlayedHorn = FALSE
							IF GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
							OR AM_I_A_PERMANENT_PARTICIPANT()
							OR AM_I_WITHIN_PARTICIPATION_RANGE()
							OR GB_IS_PLAYER_A_QUALIFYING_PARTICIPANT(PLAYER_ID())
								IF REQUEST_SCRIPT_AUDIO_BANK("DLC_APARTMENT/APT_Yacht_01")
									PLAY_SOUND_FROM_COORD(-1,"Horn",GET_YACHT_COORDS(),"DLC_Apt_Yacht_Ambient_Soundset", FALSE, 500)
									RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_APARTMENT/APT_Yacht_01")
									bPlayedHorn = TRUE
								ENDIF
							ENDIF
						ENDIF
						DRAW_GENERIC_TIMER(0, "GB_WORK_END", DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, timeColour)
					ENDIF
					
					IF HAS_NET_TIMER_STARTED(serverBD.CaptureTimer)
						IF SHOULD_DISPLAY_CAPTURE_BAR()
							IF AM_I_THE_GANG_BOSS()
							OR AM_I_A_GANG_GOON()
								IF bPlayCaptureSound = FALSE
									PLAY_SOUND_FRONTEND(-1, "Enemy_Capture_Start", "GTAO_Magnate_Yacht_Attack_Soundset", FALSE)
									bPlayCaptureSound = TRUE
								ENDIF
								DRAW_GENERIC_METER(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.CaptureTimer), YA_TUNABLE_CAPTURE_DURATION(), "GB_YA_HUD2", HUD_COLOUR_RED, DEFAULT, HUDORDER_TOP)
							ELSE
								IF bPlayCaptureSound = FALSE
									PLAY_SOUND_FRONTEND(-1, "Team_Capture_Start", "GTAO_Magnate_Yacht_Attack_Soundset", FALSE)
									bPlayCaptureSound = TRUE
								ENDIF
								DRAW_GENERIC_METER(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.CaptureTimer), YA_TUNABLE_CAPTURE_DURATION(), "GB_YA_HUD0", DEFAULT, DEFAULT, HUDORDER_TOP)
							ENDIF
						ELSE
							IF bPlayCaptureSound = TRUE
								bPlayCaptureSound = FALSE
							ENDIF
						ENDIF
					ELSE
						IF bPlayCaptureSound = TRUE
							bPlayCaptureSound = FALSE
						ENDIF
					ENDIF					
				ENDIF
			BREAK
			
			CASE eMS_END
				IF bPlayedHorn = FALSE
					IF GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
					OR AM_I_A_PERMANENT_PARTICIPANT()
					OR AM_I_WITHIN_PARTICIPATION_RANGE()
					OR GB_IS_PLAYER_A_QUALIFYING_PARTICIPANT(PLAYER_ID())
						IF REQUEST_SCRIPT_AUDIO_BANK("DLC_APARTMENT/APT_Yacht_01")
							PLAY_SOUND_FROM_COORD(-1,"Horn",GET_YACHT_COORDS(),"DLC_Apt_Yacht_Ambient_Soundset", FALSE, 500)
							RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_APARTMENT/APT_Yacht_01")
							bPlayedHorn = TRUE
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE eMS_CLEANUP
			BREAK
		
		ENDSWITCH
	ENDIF
ENDPROC

PROC MAINTAIN_PARTICIPATION()

	IF NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE()
	AND NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
	
		IF NOT AM_I_A_GANG_GOON()
		AND NOT AM_I_THE_GANG_BOSS()
		
			IF NOT IS_MP_PASSIVE_MODE_ENABLED()
				IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biP_IsPassive)
					PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_PARTICIPATION - Player is not passive, clearing biP_IsPassive")
					CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biP_IsPassive)
				ENDIF
			ENDIF
		
			// Qualifying participant
		
			IF NOT GB_IS_PLAYER_A_QUALIFYING_PARTICIPANT(PLAYER_ID())
				
				// Is player within 100m of yacht
				IF IS_PLAYER_ON_YACHT(PLAYER_ID(), serverBD.iYachtToUse, 100.0, TRUE)
					PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_PARTICIPATION - Player is on the yacht, they are a qualifying participant")
					GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
				ENDIF
				
				// Is player in the capture area
				IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), GET_CAPTURE_ZONE_POINT_1_COORDS(), GET_CAPTURE_ZONE_POINT_2_COORDS(), GET_CAPTURE_ZONE_WIDTH())
					GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
					PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_PARTICIPATION - player has entered capture zone, they are now a qualifying participant")
				ENDIF
			
			ENDIF
		
			// Permanent Participant

			IF NOT FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_FM_EVENT(PLAYER_ID())
				
				// 3090388 - Aleksej L - Stop audio scene, allow radios inside apartments to function.
				IF IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), FALSE)
				OR IS_PLAYER_ENTERING_PROPERTY(PLAYER_ID())
				OR IS_PLAYER_ON_YACHT_DECK()
					IF GB_IS_GLOBAL_CLIENT_BIT0_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_0_TEMPORARY_PARTICIPANT)
						GB_CLEAR_GLOBAL_CLIENT_BIT0(eGB_GLOBAL_CLIENT_BITSET_0_TEMPORARY_PARTICIPANT)
					ENDIF
					IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biP_InParticipationRange)
						CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biP_InParticipationRange)
					ENDIF
				ELSE
					// Temporary Participant, only needed whilst not permanent, as soon as we're permanent, temporary is irrelevent
					IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biP_InParticipationRange)
						IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), GET_YACHT_COORDS(), FALSE) <= 500
							PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_PARTICIPATION - Player has come within 500m of the yacht, they are now within participation range.")
							GB_SET_GLOBAL_CLIENT_BIT0(eGB_GLOBAL_CLIENT_BITSET_0_TEMPORARY_PARTICIPANT)
							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biP_InParticipationRange)
						ENDIF
					ELSE
						IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), GET_YACHT_COORDS(), FALSE) > 700
							PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_PARTICIPATION - Player is more than 700m away from the yacht, they are no longer within participation range.")
							GB_CLEAR_GLOBAL_CLIENT_BIT0(eGB_GLOBAL_CLIENT_BITSET_0_TEMPORARY_PARTICIPANT)
							CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biP_InParticipationRange)
						ENDIF
					ENDIF
				ENDIF
			
				// Check for becoming a permanent participant
				IF NOT GB_IS_GLOBAL_CLIENT_BIT0_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_0_PERMANENT_PARTICIPANT)
					IF IS_PLAYER_ON_YACHT(PLAYER_ID(), serverBD.iYachtToUse)
						PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_PARTICIPATION - Player is on the yacht, they are now a permanent participant")
						GB_SET_PLAYER_AS_PERMANENT_ON_BOSS_MISSION()
					ENDIF
					
					IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), GET_CAPTURE_ZONE_POINT_1_COORDS(), GET_CAPTURE_ZONE_POINT_2_COORDS(), GET_CAPTURE_ZONE_WIDTH())
						PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_PARTICIPATION - Player is in capture area, they are now a permanent participant")
						GB_SET_PLAYER_AS_PERMANENT_ON_BOSS_MISSION()
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biP_GoonAtYacht)
				IF AM_I_NEAR_YACHT()
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biP_GoonAtYacht)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_MP_PASSIVE_MODE_ENABLED()
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biP_IsPassive)
				PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_PARTICIPATION - Player is passive, setting biP_IsPassive")
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biP_IsPassive)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_UI()

	IF SHOULD_UI_BE_HIDDEN()
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biP_HiddenEvent)
			PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_UI - Player has UI hidden, biP_HiddenEvent set")
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biP_HiddenEvent)
		ENDIF
	ELSE
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biP_HiddenEvent)
			PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_UI - Player does not have UI hidden, biP_HiddenEvent cleared")
			CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biP_HiddenEvent)
		ENDIF
	ENDIF

	IF AM_I_THE_GANG_BOSS()
	OR AM_I_A_GANG_GOON()
		SET_PLAYER_SHOULD_SEE_EVENT_HELP(TRUE)
		SET_PLAYER_SHOULD_SEE_EVENT_SHARDS(TRUE)
		SET_PLAYER_SHOULD_SEE_BACKGROUND_HELP(FALSE)
		MAINTAIN_BLIPS()
		MAINTAIN_BOTTOM_RIGHT_UI()
		MAINTAIN_OBJECTIVE_TEXT()
	ELSE
		SWITCH GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())
			
			CASE GB_UI_LEVEL_NONE
				SET_PLAYER_SHOULD_SEE_EVENT_HELP(FALSE)
				SET_PLAYER_SHOULD_SEE_BACKGROUND_HELP(FALSE)
				SET_PLAYER_SHOULD_SEE_EVENT_SHARDS(FALSE)
				REMOVE_ALL_BLIPS()
				CLEAR_ANY_OBJECTIVE_TEXT_FROM_THIS_SCRIPT()
			BREAK
			
			CASE GB_UI_LEVEL_BACKGROUND
			CASE GB_UI_LEVEL_MINIMAL
				SET_PLAYER_SHOULD_SEE_EVENT_HELP(FALSE)
				SET_PLAYER_SHOULD_SEE_BACKGROUND_HELP(TRUE)
				SET_PLAYER_SHOULD_SEE_EVENT_SHARDS(TRUE)
				MAINTAIN_BLIPS()
			BREAK
			
			CASE GB_UI_LEVEL_FULL
				SET_PLAYER_SHOULD_SEE_EVENT_HELP(TRUE)
				SET_PLAYER_SHOULD_SEE_BACKGROUND_HELP(FALSE)
				SET_PLAYER_SHOULD_SEE_EVENT_SHARDS(TRUE)
				MAINTAIN_BLIPS()
				MAINTAIN_OBJECTIVE_TEXT()
				MAINTAIN_BOTTOM_RIGHT_UI()
			BREAK
			
		ENDSWITCH
	ENDIF
ENDPROC

PROC MAINTAIN_YACHT()
	IF MPGlobalsAmbience.iYachtAttackYacht != serverBD.iYachtToUse
		MPGlobalsAmbience.iYachtAttackYacht = serverBD.iYachtToUse
	ENDIF
ENDPROC

// ------------------------------------------------------ |<

FUNC BOOL SHOULD_I_BECOME_AN_ATTACKER()
	IF GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT()) = eMS_DEFEND
		IF NETWORK_IS_PARTICIPANT_ACTIVE(GET_GANG_BOSS_PARTICIPANT_INDEX())
			IF NOT AM_I_A_GANG_GOON()
			AND NOT AM_I_THE_GANG_BOSS()
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_I_BECOME_A_DEFENDER()
	IF GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT()) = eMS_ATTACK
		IF NETWORK_IS_PARTICIPANT_ACTIVE(GET_GANG_BOSS_PARTICIPANT_INDEX())
			IF AM_I_A_GANG_GOON()
			OR AM_I_THE_GANG_BOSS()
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DRAW_CAPTURE_POINT_MARKER()
	IF NOT SHOULD_UI_BE_HIDDEN()
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			DRAW_MARKER(MARKER_CYLINDER, GET_CAPTURE_POINT_COORDS(), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, GET_CAPTURE_POINT_MARKER_DIMENSTIONS(), r, g, b)
		ELSE
			DRAW_MARKER(MARKER_CYLINDER, GET_CAPTURE_POINT_COORDS(), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, GET_CAPTURE_POINT_MARKER_DIMENSTIONS(), deadr, deadg, deadb)
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_OVERTIME_HELP()
	IF HAS_NET_TIMER_STARTED(serverBD.DefendTimer)
		IF HAS_NET_TIMER_STARTED(serverBD.CaptureTimer)
			IF HAS_NET_TIMER_EXPIRED(serverBD.DefendTimer, YA_TUNABLE_EVENT_DURATION())
				IF AM_I_THE_GANG_BOSS()
				OR AM_I_A_GANG_GOON()
					DO_HELP_TEXT(8)
				ELSE
					DO_HELP_TEXT(9)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_PASSIVE_HELP()
	IF HAS_NET_TIMER_STARTED(serverBD.DefendTimer)
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_BridgeTaken)
		AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SuccessfullyDefended)
		AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_BossLeft)
			IF IS_PLAYER_ON_YACHT(PLAYER_ID(), serverBD.iYachtToUse)
				IF IS_MP_PASSIVE_MODE_ENABLED()
					IF NOT AM_I_THE_GANG_BOSS()
					AND NOT AM_I_A_GANG_GOON()
						DO_HELP_TEXT(10)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_WANTED_MUSIC_SUPPRESSION()
	IF AM_I_THE_GANG_BOSS()
	OR AM_I_A_GANG_GOON()
		IF NOT bSuppressedWantedMusic
			SET_AUDIO_FLAG("WantedMusicDisabled", TRUE)
			SET_AUDIO_FLAG("DisableFlightMusic", TRUE)
			START_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
			PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_YACHT_ATTACK_MUSIC - HANDLE_WANTED_MUSIC_SUPPRESSION - Wanted music suppressed for defender.")
			bSuppressedWantedMusic = TRUE
		ENDIF
	ELSE
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biP_InParticipationRange)
			IF NOT bSuppressedWantedMusic
				SET_AUDIO_FLAG("WantedMusicDisabled", TRUE)
				SET_AUDIO_FLAG("DisableFlightMusic", TRUE)
				START_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
				PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_YACHT_ATTACK_MUSIC - HANDLE_WANTED_MUSIC_SUPPRESSION - Wanted music suppressed for attacker.")
				bSuppressedWantedMusic = TRUE
			ENDIF
		ELSE
			IF bSuppressedWantedMusic
				SET_AUDIO_FLAG("WantedMusicDisabled", FALSE)
				SET_AUDIO_FLAG("DisableFlightMusic", FALSE)
				STOP_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
				PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_YACHT_ATTACK_MUSIC - HANDLE_WANTED_MUSIC_SUPPRESSION - Wanted music is no longer being suppressed for attacker, player > 700m away from yacht")
				bSuppressedWantedMusic = FALSE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL AM_I_IN_AN_ATTACKER_VEHICLE()

	IF ARE_THERE_ANY_ATTACKER_SEA_VEHICLES()
		INT i
		REPEAT serverBD.sAttackerVehStruct.iNumAttackerBoats i
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niAttackerSeaVeh[i])
				IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niAttackerSeaVeh[i]))
					RETURN TRUE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	IF ARE_THERE_ANY_ATTACKER_AIR_VEHICLES()
		INT i
		REPEAT serverBd.sAttackerVehStruct.iNumAttackerHelis i
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niAttackerAirVeh[i])
			AND NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niAttackerAirVeh[i]))
				IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niAttackerAirVeh[i]))
					RETURN TRUE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_TRIGGER_ACTION_MUSIC(BOOL bAttacker = FALSE)

	IF bAttacker = TRUE
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), GET_YACHT_COORDS(), FALSE) <= 200
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF AM_I_IN_AN_ATTACKER_VEHICLE()
			RETURN TRUE
		ENDIF
	ELSE
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iActionMusicTriggerStagger))
			PLAYER_INDEX thisPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iActionMusicTriggerStagger))
			PED_INDEX thisPed = GET_PLAYER_PED(thisPlayer)
			
			IF thisPlayer != INVALID_PLAYER_INDEX()
				IF NOT IS_THIS_PLAYER_A_GANG_GOON(thisPlayer)
				AND NOT IS_THIS_PLAYER_THE_GANG_BOSS(thisPlayer)
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(thisPed, GET_YACHT_COORDS(), FALSE) <= 500
						iActionMusicTriggerStagger = 0
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		iActionMusicTriggerStagger++
		
		IF iActionMusicTriggerStagger >= NUM_NETWORK_PLAYERS
			iActionMusicTriggerStagger = 0
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_TRIGGER_LEAVE_MUSIC(BOOL bAttacker = FALSE)

	IF bAttacker
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biP_InParticipationRange)
		AND NOT AM_I_IN_AN_ATTACKER_VEHICLE()
			RETURN TRUE
		ENDIF
	ELSE
		REPEAT NUM_NETWORK_PLAYERS iSafetyMusicTriggerStagger
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iSafetyMusicTriggerStagger))
				PLAYER_INDEX thisPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iSafetyMusicTriggerStagger))
				PED_INDEX thisPed = GET_PLAYER_PED(thisPlayer)
				
				IF thisPlayer != INVALID_PLAYER_INDEX()
					IF NOT IS_THIS_PLAYER_A_GANG_GOON(thisPlayer)
					AND NOT IS_THIS_PLAYER_THE_GANG_BOSS(thisPlayer)
						IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(thisPed, GET_YACHT_COORDS(), FALSE) < 700
							RETURN FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_STOP_MUSIC()
	IF GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT()) = eMS_END
	OR GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT()) = eMS_CLEANUP
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_YACHT_ATTACK_MUSIC()

	HANDLE_WANTED_MUSIC_SUPPRESSION()

	IF iTimeRemaining > (THIRTY_SECONDS+5000)	
		IF AM_I_THE_GANG_BOSS()
		OR AM_I_A_GANG_GOON()
			SWITCH eDefendMusicStage
				CASE eDMUSIC_PREP
					IF GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT()) = eMS_DEFEND
						IF PREPARE_MUSIC_EVENT("APT_YA_START_DEFEND")
							eDefendMusicStage = eDMUSIC_START_DEFEND
						ENDIF
					ENDIF
				BREAK
				
				CASE eDMUSIC_START_DEFEND
					IF NOT IS_BIT_SET(iDefendMusicBitSet, iDMBS_TriggeredStartDefend)
						TRIGGER_MUSIC_EVENT("APT_YA_START_DEFEND")
						PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_YACHT_ATTACK_MUSIC - eDMUSIC_START_DEFEND - APT_YA_START_DEFEND has been triggered")
						SET_BIT(iDefendMusicBitSet, iDMBS_TriggeredStartDefend)
					ENDIF
					IF SHOULD_STOP_MUSIC()
						IF PREPARE_MUSIC_EVENT("APT_YA_STOP")
							eDefendMusicStage = eDMUSIC_STOP
						ENDIF
					ENDIF
					IF PREPARE_MUSIC_EVENT("APT_YA_DEFENDING")
						CLEAR_BIT(iDefendMusicBitSet, iDMBS_TriggeredDefending)
						eDefendMusicStage = eDMUSIC_DEFENDING
					ENDIF
				BREAK
				
				CASE eDMUSIC_DEFENDING
					IF NOT IS_BIT_SET(iDefendMusicBitSet, iDMBS_TriggeredDefending)
						TRIGGER_MUSIC_EVENT("APT_YA_DEFENDING")
						PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_YACHT_ATTACK_MUSIC - eDMUSIC_DEFENDING - APT_YA_DEFENDING has been triggered")
						SET_BIT(iDefendMusicBitSet, iDMBS_TriggeredDefending)
					ENDIF
					IF SHOULD_STOP_MUSIC()
						IF PREPARE_MUSIC_EVENT("APT_YA_STOP")
							eDefendMusicStage = eDMUSIC_STOP
						ENDIF
					ENDIF
					IF SHOULD_TRIGGER_ACTION_MUSIC()
						IF PREPARE_MUSIC_EVENT("APT_YA_ACTION")
							CLEAR_BIT(iDefendMusicBitSet, iDMBS_TriggeredAction)
							eDefendMusicStage = eDMUSIC_ACTION
						ENDIF
					ENDIF
				BREAK
				
				CASE eDMUSIC_ACTION
					IF NOT IS_BIT_SET(iDefendMusicBitSet, iDMBS_TriggeredAction)
						TRIGGER_MUSIC_EVENT("APT_YA_ACTION")
						PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_YACHT_ATTACK_MUSIC - eDMUSIC_ACTION - APT_YA_ACTION has been triggered")
						SET_BIT(iDefendMusicBitSet, iDMBS_TriggeredAction)
					ENDIF
					IF SHOULD_STOP_MUSIC()
						IF PREPARE_MUSIC_EVENT("APT_YA_STOP")
							eDefendMusicStage = eDMUSIC_STOP
						ENDIF
					ENDIF
					IF SHOULD_TRIGGER_LEAVE_MUSIC()
						IF PREPARE_MUSIC_EVENT("APT_YA_STOP_LEAVE")
							eDefendMusicStage = eDMUSIC_SAFE
						ENDIF
					ENDIF
				BREAK
				
				CASE eDMUSIC_SAFE
					IF NOT SHOULD_STOP_MUSIC()
						IF PREPARE_MUSIC_EVENT("APT_YA_DEFENDING")
							TRIGGER_MUSIC_EVENT("APT_YA_STOP_LEAVE")
							CLEAR_BIT(iDefendMusicBitSet, iDMBS_TriggeredDefending)
							PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_YACHT_ATTACK_MUSIC - eDMUSIC_SAFE - APT_YA_STOP_LEAVE has been triggered")
							eDefendMusicStage = eDMUSIC_DEFENDING
						ENDIF
					ELSE
						IF PREPARE_MUSIC_EVENT("APT_YA_STOP")
							eDefendMusicStage = eDMUSIC_STOP
						ENDIF
					ENDIF
				BREAK
				
				CASE eDMUSIC_STOP
					IF NOT IS_BIT_SET(iDefendMusicBitSet, iDMBS_TriggeredStop)
						TRIGGER_MUSIC_EVENT("APT_YA_STOP")
						PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_YACHT_ATTACK_MUSIC - eDMUSIC_STOP - APT_YA_STOP has been triggered")
						SET_BIT(iDefendMusicBitSet, iDMBS_TriggeredStop)
					ENDIF
				BREAK
			ENDSWITCH
		ELSE
			IF NOT IS_BIT_SET(iDefendMusicBitSet, iDMBS_TriggeredStop)
				IF IS_BIT_SET(iDefendMusicBitSet, iDMBS_TriggeredStartDefend)
				OR IS_BIT_SET(iDefendMusicBitSet, iDMBS_TriggeredDefending)
				OR IS_BIT_SET(iDefendMusicBitSet, iDMBS_TriggeredAction)
					IF SHOULD_STOP_MUSIC()
						IF PREPARE_MUSIC_EVENT("APT_YA_STOP")
							TRIGGER_MUSIC_EVENT("APT_YA_STOP")
							PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_YACHT_ATTACK_MUSIC - Boss left, defenders now stopping music")
							SET_BIT(iDefendMusicBitSet, iDMBS_TriggeredStop)
						ENDIF 
					ENDIF
				ENDIF
			ENDIF
		
			SWITCH eAttackMusicStage
				CASE eAMUSIC_PREP
					IF GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT()) = eMS_ATTACK
						IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biP_InParticipationRange)
							IF PREPARE_MUSIC_EVENT("APT_YA_START_ATTACK")
								CLEAR_BIT(iAttackMusicBitSet, iAMBS_TriggeredStartAttack)
								eAttackMusicStage = eAMUSIC_START_ATTACK
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE eAMUSIC_START_ATTACK
					IF NOT IS_BIT_SET(iAttackMusicBitSet, iAMBS_TriggeredStartAttack)
						TRIGGER_MUSIC_EVENT("APT_YA_START_ATTACK")
						PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_YACHT_ATTACK_MUSIC - eAMUSIC_START_ATTACK - APT_YA_START_ATTACK has been triggered")
						SET_BIT(iAttackMusicBitSet, iAMBS_TriggeredStartAttack)
					ENDIF
					IF SHOULD_STOP_MUSIC()
						IF PREPARE_MUSIC_EVENT("APT_YA_STOP")
							eAttackMusicStage = eAMUSIC_STOP
						ENDIF
					ENDIF
					IF SHOULD_TRIGGER_ACTION_MUSIC(TRUE)
						IF PREPARE_MUSIC_EVENT("APT_YA_ACTION")
							CLEAR_BIT(iAttackMusicBitSet, iAMBS_TriggeredAction)
							eAttackMusicStage = eAMUSIC_ACTION
						ENDIF
					ENDIF
					IF SHOULD_TRIGGER_LEAVE_MUSIC(TRUE)
						IF PREPARE_MUSIC_EVENT("APT_YA_STOP_LEAVE")
							eAttackMusicStage = eAMUSIC_WITHDRAW
						ENDIF
					ENDIF
					IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
						IF PREPARE_MUSIC_EVENT("APT_YA_ATTACK")
							eAttackMusicStage = eAMUSIC_ATTACK
						ENDIF
					ENDIF
				BREAK
				
				CASE eAMUSIC_ATTACK
					IF IS_NET_PLAYER_OK(PLAYER_ID())
						IF HAS_PLAYER_FULLY_SPAWNED()
							TRIGGER_MUSIC_EVENT("APT_YA_ATTACK")
							PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_YACHT_ATTACK_MUSIC - eAMUSIC_ATTACK - APT_YA_ATTACK has been triggered")
							eAttackMusicStage = eAMUSIC_START_ATTACK
						ENDIF
					ENDIF
				BREAK
				
				CASE eAMUSIC_ACTION
					IF NOT IS_BIT_SET(iAttackMusicBitSet, iAMBS_TriggeredAction)
						TRIGGER_MUSIC_EVENT("APT_YA_ACTION")
						PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_YACHT_ATTACK_MUSIC - eAMUSIC_ACTION - APT_YA_ACTION has been triggered")
						SET_BIT(iAttackMusicBitSet, iAMBS_TriggeredAction)
					ENDIF
					IF SHOULD_STOP_MUSIC()
						IF PREPARE_MUSIC_EVENT("APT_YA_STOP")
							eAttackMusicStage = eAMUSIC_STOP
						ENDIF
					ENDIF
					IF SHOULD_TRIGGER_LEAVE_MUSIC(TRUE)
						IF PREPARE_MUSIC_EVENT("APT_YA_STOP_LEAVE")
							eAttackMusicStage = eAMUSIC_WITHDRAW
						ENDIF
					ENDIF
					IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
						IF PREPARE_MUSIC_EVENT("APT_YA_ATTACK")
							eAttackMusicStage = eAMUSIC_ATTACK
						ENDIF
					ENDIF
				BREAK
				
				CASE eAMUSIC_WITHDRAW
					IF NOT SHOULD_STOP_MUSIC()
						TRIGGER_MUSIC_EVENT("APT_YA_STOP_LEAVE")
						PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_YACHT_ATTACK_MUSIC - eAMUSIC_WITHDRAW - APT_YA_STOP_LEAVE has been triggered")
						eAttackMusicStage = eAMUSIC_PREP
					ELSE
						IF PREPARE_MUSIC_EVENT("APT_YA_STOP")
							eAttackMusicStage = eAMUSIC_STOP
						ENDIF
					ENDIF
				BREAK
				
				CASE eAMUSIC_STOP
					IF NOT IS_BIT_SET(iAttackMusicBitSet, iAMBS_TriggeredStop)
						TRIGGER_MUSIC_EVENT("APT_YA_STOP")
						PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_YACHT_ATTACK_MUSIC - eAMUSIC_STOP - APT_YA_STOP has been triggered")
						SET_BIT(iAttackMusicBitSet, iAMBS_TriggeredStop)
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ELSE
		SWITCH eFinalCountdownStage
			CASE eFCOUNTDOWN_PREP
				IF GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT()) = eMS_ATTACK
				OR GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT()) = eMS_DEFEND
					// IF within a second of hitting the end prep period
					IF iTimeRemaining <= (THIRTY_SECONDS+5000)
					AND iTimeRemaining >= (THIRTY_SECONDS-1000)
						IF PREPARE_MUSIC_EVENT("APT_PRE_COUNTDOWN_STOP")
							PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_YACHT_ATTACK_MUSIC - eFCOUNTDOWN_PREP - APT_PRE_COUNTDOWN_STOP has been triggered")
							TRIGGER_MUSIC_EVENT("APT_PRE_COUNTDOWN_STOP")
							eFinalCountdownStage = eFCOUNTDOWN_START
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE eFCOUNTDOWN_START
				IF NOT IS_BIT_SET(iFinalCountdownBitSet, iFCBS_TriggeredCountdown)
					IF iTimeRemaining <= THIRTY_SECONDS
						IF PREPARE_MUSIC_EVENT("APT_COUNTDOWN_30S")
							SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE)
							SET_USER_RADIO_CONTROL_ENABLED(FALSE)
							TRIGGER_MUSIC_EVENT("APT_COUNTDOWN_30S")
							PREPARE_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
							PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_YACHT_ATTACK_MUSIC - eFCOUNTDOWN_START - APT_COUNTDOWN_30S has been triggered")
							SET_BIT(iFinalCountdownBitSet, iFCBS_TriggeredCountdown)
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_BIT_SET(iFinalCountdownBitSet, iFCBS_ReEnabledRadioControl)
						IF iTimeRemaining <= 27000
							SET_USER_RADIO_CONTROL_ENABLED(TRUE)
							SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
							PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_YACHT_ATTACK_MUSIC - eFCOUNTDOWN_START - Radio control re-enabled")
							SET_BIT(iFinalCountdownBitSet, iFCBS_ReEnabledRadioControl)
							eFinalCountdownStage = eFCOUNTDOWN_END_OR_KILL
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE eFCOUNTDOWN_END_OR_KILL
				IF NOT IS_BIT_SET(iFinalCountdownBitSet, iFCBS_TriggeredCountdownKill)
					IF iTimeRemaining <= 0
						CANCEL_MUSIC_EVENT("APT_COUNTDOWN_30S")
						IF PREPARE_MUSIC_EVENT("APT_FADE_IN_RADIO")
							TRIGGER_MUSIC_EVENT("APT_FADE_IN_RADIO")
							CANCEL_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
							PREPARE_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
							PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_YACHT_ATTACK_MUSIC - eFCOUNTDOWN_END_OR_KILL - Time ran out, APT_FADE_IN_RADIO has been triggered")
							SET_BIT(iFinalCountdownBitSet, iFCBS_TriggeredCountdownKill)
						ENDIF
					ELSE
						IF IS_BIT_SET(serverBD.iServerBitSet, biS_BridgeTaken)
						OR IS_BIT_SET(serverBD.iServerBitSet, biS_BossLeft)
							IF PREPARE_MUSIC_EVENT("APT_FADE_IN_RADIO")
								CANCEL_MUSIC_EVENT("APT_COUNTDOWN_30S")
								TRIGGER_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
								TRIGGER_MUSIC_EVENT("APT_FADE_IN_RADIO")
								PREPARE_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
								PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_YACHT_ATTACK_MUSIC - eFCOUNTDOWN_END_OR_KILL - Mode ended before timeout, APT_COUNTDOWN_30S_KILL and APT_FADE_IN_RADIO have been triggered")
								SET_BIT(iFinalCountdownBitSet, iFCBS_TriggeredCountdownKill)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC MAINTAIN_HELP()
	HANDLE_OVERTIME_HELP()
	HANDLE_PASSIVE_HELP()
ENDPROC

/// PURPOSE:
///    Process the client logic
PROC PROCESS_CLIENT()

	PROCESS_EVENTS()
	MAINTAIN_YACHT()
	MAINTAIN_SPAWN_LOCATIONS()
	MAINTAIN_UI()
	MAINTAIN_CLIENT_MOVE_TO_END_CHECKS()
	MAINTAIN_PARTICIPATION()
	MAINTAIN_YACHT_ATTACK_MUSIC()
	
	SWITCH GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT())
	
		CASE eMS_IDLE
			IF AM_I_THE_GANG_BOSS()
			OR AM_I_A_GANG_GOON()
				Clear_Any_Objective_Text_From_This_Script()
				SET_FREEMODE_FLOW_COMMS_DISABLED(TRUE)
				GB_SET_LOCAL_PLAYER_CRITICAL_TO_JOB(TRUE)
				//BLOCK_ALL_MISSIONSATCOORDS_MISSIONS(TRUE)
				GB_SET_COMMON_TELEMETRY_DATA_ON_START()
				GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
				
				// Send the text informing about the attack
				SET_BIT(sGangBossLocalModeVars.iHelpTextBitSet, ciGANG_BOOS_HELP_TEXT_MESSAGE_YA)
				
				PRINTLN("[GB YACHT ATTACK] [STMGB] - PROCESS_CLIENT - AM_I_A_GANG_BOSS / GOON - TRUE - Moving local player from eMS_IDLE to eMS_GOON_PREP")
				SET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT(), eMS_DEFEND)
			ELSE
				Clear_Any_Objective_Text_From_This_Script()
				SET_FREEMODE_FLOW_COMMS_DISABLED(TRUE)
				//BLOCK_ALL_MISSIONSATCOORDS_MISSIONS(TRUE)
				GB_SET_COMMON_TELEMETRY_DATA_ON_START()
				
				PRINTLN("[GB YACHT ATTACK] [STMGB] - PROCESS_CLIENT - AM_I_THE_GANG_BOSS is false, AM_I_A_GANG_GOON is false - Moving local player from eMS_IDLE to eMS_PLAYER_WAIT")
				SET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT(), eMS_ATTACK)
			ENDIF
		BREAK
		
		CASE eMS_DEFEND
			IF HAS_NET_TIMER_STARTED(serverBD.DefendTimer)
				DO_BIG_MESSAGE(0)
				IF AM_I_THE_GANG_BOSS()
					DO_HELP_TEXT(0)
				ELSE
					DO_HELP_TEXT(1)
				ENDIF
				IF HAS_NET_TIMER_STARTED(serverBD.CaptureTimer)
					DO_HELP_TEXT(3)
				ENDIF
				
				DRAW_CAPTURE_POINT_MARKER()
				MAINTAIN_ATTACKER_VEHICLES()
				
			ENDIF
			IF SHOULD_I_BECOME_AN_ATTACKER()
				PRINTLN("[GB YACHT ATTACK] [STMGB] - PROCESS_CLIENT - I'm no longer a goon or boss, I should be an attacker")
				GB_SET_LOCAL_PLAYER_CRITICAL_TO_JOB(FALSE)
				SET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT(), eMS_ATTACK)
			ENDIF
		BREAK
		
		CASE eMS_ATTACK
			IF HAS_NET_TIMER_STARTED(serverBD.DefendTimer)
				DO_BIG_MESSAGE(1)
				DO_HELP_TEXT(2)
				
				DRAW_CAPTURE_POINT_MARKER()
				MAINTAIN_ATTACKER_VEHICLES()
				
				IF HAS_NET_TIMER_STARTED(serverBD.CaptureTimer)
					DO_HELP_TEXT(4)
				ELSE
					CLEAR_THIS_HELP(4)
				ENDIF
			ENDIF
			IF SHOULD_I_BECOME_A_DEFENDER()
				PRINTLN("[GB YACHT ATTACK] [STMGB] - PROCESS_CLIENT - I'm now a goon or the gang boss, I should be a defender")
				GB_SET_LOCAL_PLAYER_CRITICAL_TO_JOB(TRUE)
				SET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT(), eMS_DEFEND)
			ENDIF
		BREAK
		
		CASE eMS_OFF_MISSION
		BREAK 
		
		CASE eMS_END
			REMOVE_ALL_BLIPS()
			IF IS_BIT_SET(g_GB_WORK_VARS.iEventBitSet, ciGB_BOSS_SHARD_SAFE_TO_DISPLAY)
				PROCESS_END_REWARDS_AND_MESSAGES()
			ENDIF
			IF GB_MAINTAIN_BOSS_END_UI(sEndUI, FALSE)
				SET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT(), GAME_STATE_END)
			ENDIF
		BREAK
		
		CASE eMS_CLEANUP
		BREAK
	ENDSWITCH
	
	MAINTAIN_HELP()
ENDPROC

/// PURPOSE:
///    Create the specified yacht attacker vehicle
/// PARAMS:
///    iVeh - 
/// RETURNS:
///    
FUNC BOOL CREATE_ATTACKER_SEA_VEHICLE(INT iVehicle, BOOL bRespawn = FALSE)
	IF ARE_THERE_ANY_ATTACKER_SEA_VEHICLES()
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niAttackerSeaVeh[iVehicle])
			IF CAN_REGISTER_MISSION_ENTITIES(0, 1, 0, 0)
				CLEAR_AREA_OF_VEHICLES(serverBD.sAttackerVehStruct.vAttackerBoatSpawnLoc[iVehicle], CLEAR_VEHICLE_RADIUS )
				IF CREATE_NET_VEHICLE(	serverBD.niAttackerSeaVeh[iVehicle], 
										serverBD.sAttackerVehStruct.mAttackerBoatModel, 
										serverBD.sAttackerVehStruct.vAttackerBoatSpawnLoc[iVehicle], 
										serverBD.sAttackerVehStruct.fAttackerBoatSpawnHeading)
										
					PRINTLN("[GB YACHT ATTACK] [STMGB] - CREATE_ATTACKER_SEA_VEHICLE - Creating sea vehicle ", iVehicle)
					
					NETWORK_FADE_IN_ENTITY(NET_TO_VEH(serverBD.niAttackerSeaVeh[iVehicle]), TRUE)
					
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_VEH(serverBD.niAttackerSeaVeh[iVehicle]), TRUE)
					SET_BOAT_ANCHOR(NET_TO_VEH(serverBD.niAttackerSeaVeh[iVehicle]), TRUE)
					SET_ENTITY_DYNAMIC(NET_TO_VEH(serverBD.niAttackerSeaVeh[iVehicle]), TRUE)
					ACTIVATE_PHYSICS(NET_TO_VEH(serverBD.niAttackerSeaVeh[iVehicle]))
					SET_VEHICLE_ENGINE_ON(NET_TO_VEH(serverBD.niAttackerSeaVeh[iVehicle]), TRUE, TRUE)
					SET_VEHICLE_DOORS_LOCKED_FOR_NON_SCRIPT_PLAYERS(NET_TO_VEH(serverBD.niAttackerSeaVeh[iVehicle]), TRUE)
					SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.niAttackerSeaVeh[iVehicle]), TRUE)
					SET_VEHICLE_CAN_BE_USED_BY_FLEEING_PEDS(NET_TO_VEH(serverBD.niAttackerSeaVeh[iVehicle]), FALSE)
					SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(NET_TO_VEH(serverBD.niAttackerSeaVeh[iVehicle]), TRUE)
										
					IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh", DECOR_TYPE_INT)
		                DECOR_SET_INT(NET_TO_VEH(serverBD.niAttackerSeaVeh[iVehicle]), "Not_Allow_As_Saved_Veh", MP_NOT_ALLOWED_AS_SAVED_VEHICLE_DEC_SIMEON)
						PRINTLN("[AM MOVING TARGET] - DECORATOR 'Not_Allow_As_Saved_Veh' SET")
					ENDIF

					//Prevents the vehicle from being used for activities that use passive mode - MJM 
					IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
						INT iDecoratorValue
						IF DECOR_EXIST_ON(NET_TO_VEH(serverBD.niAttackerSeaVeh[iVehicle]), "MPBitset")
							iDecoratorValue = DECOR_GET_INT(NET_TO_VEH(serverBD.niAttackerSeaVeh[iVehicle]), "MPBitset")
						ENDIF
						SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_PASSIVE_MODE_VEHICLE)
						SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_MODDABLE_VEHICLE)
						DECOR_SET_INT(NET_TO_VEH(serverBD.niAttackerSeaVeh[iVehicle]), "MPBitset", iDecoratorValue)
					ENDIF
					
					IF bRespawn = TRUE
						RETURN TRUE
					ENDIF
					
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
		
		IF bRespawn = TRUE
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CREATE_ATTACKER_AIR_VEHICLE(INT iVehicle, BOOL bRespawn = FALSE)
	IF ARE_THERE_ANY_ATTACKER_AIR_VEHICLES()
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niAttackerAirVeh[iVehicle])
			IF CAN_REGISTER_MISSION_ENTITIES(0, 1, 0, 0)
				CLEAR_AREA_OF_VEHICLES(serverBD.sAttackerVehStruct.vAttackerHeliSpawnLoc[iVehicle], CLEAR_VEHICLE_RADIUS )
				IF CREATE_NET_VEHICLE(	serverBD.niAttackerAirVeh[iVehicle], 
										serverBD.sAttackerVehStruct.mAttackerHeliModel, 
										serverBD.sAttackerVehStruct.vAttackerHeliSpawnLoc[iVehicle], 
										serverBD.sAttackerVehStruct.fAttackerHeliSpawnHeading[iVehicle])
										
					PRINTLN("[GB YACHT ATTACK] [STMGB] - CREATE_ATTACKER_GROUND_VEHICLE - Creating air vehicle ", iVehicle)
					
					NETWORK_FADE_IN_ENTITY(NET_TO_VEH(serverBD.niAttackerAirVeh[iVehicle]), TRUE)
					
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_VEH(serverBD.niAttackerAirVeh[iVehicle]), TRUE)
					SET_ENTITY_DYNAMIC(NET_TO_VEH(serverBD.niAttackerAirVeh[iVehicle]), TRUE)
					ACTIVATE_PHYSICS(NET_TO_VEH(serverBD.niAttackerAirVeh[iVehicle]))
					SET_VEHICLE_ENGINE_ON(NET_TO_VEH(serverBD.niAttackerAirVeh[iVehicle]), FALSE, TRUE)
					SET_VEHICLE_ON_GROUND_PROPERLY(NET_TO_VEH(serverBD.niAttackerAirVeh[iVehicle]))
					SET_VEHICLE_DOORS_LOCKED_FOR_NON_SCRIPT_PLAYERS(NET_TO_VEH(serverBD.niAttackerAirVeh[iVehicle]), TRUE)
					SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.niAttackerAirVeh[iVehicle]), TRUE)
					SET_VEHICLE_CAN_BE_USED_BY_FLEEING_PEDS(NET_TO_VEH(serverBD.niAttackerAirVeh[iVehicle]), FALSE)
					SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(NET_TO_VEH(serverBD.niAttackerAirVeh[iVehicle]), TRUE)
										
					IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh", DECOR_TYPE_INT)
		                DECOR_SET_INT(NET_TO_VEH(serverBD.niAttackerAirVeh[iVehicle]), "Not_Allow_As_Saved_Veh", MP_NOT_ALLOWED_AS_SAVED_VEHICLE_DEC_SIMEON)
						PRINTLN("[AM MOVING TARGET] - DECORATOR 'Not_Allow_As_Saved_Veh' SET")
					ENDIF

					//Prevents the vehicle from being used for activities that use passive mode - MJM 
					IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
						INT iDecoratorValue
						IF DECOR_EXIST_ON(NET_TO_VEH(serverBD.niAttackerAirVeh[iVehicle]), "MPBitset")
							iDecoratorValue = DECOR_GET_INT(NET_TO_VEH(serverBD.niAttackerAirVeh[iVehicle]), "MPBitset")
						ENDIF
						SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_PASSIVE_MODE_VEHICLE)
						SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_MODDABLE_VEHICLE)
						DECOR_SET_INT(NET_TO_VEH(serverBD.niAttackerAirVeh[iVehicle]), "MPBitset", iDecoratorValue)
					ENDIF
					
					IF bRespawn = TRUE
						RETURN TRUE
					ENDIF
					
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
		
		IF bRespawn = TRUE
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE

ENDFUNC

FUNC BOOL CREATE_ATTACKER_VEHS()
	IF REQUEST_LOAD_MODEL(serverBD.sAttackerVehStruct.mAttackerBoatModel)
	AND REQUEST_LOAD_MODEL(serverBD.sAttackerVehStruct.mAttackerHeliModel)
		INT i
		IF ARE_THERE_ANY_ATTACKER_SEA_VEHICLES()
			REPEAT serverBD.sAttackerVehStruct.iNumAttackerBoats i
				IF NOT CREATE_ATTACKER_SEA_VEHICLE(i)
					RETURN FALSE
				ENDIF
			ENDREPEAT
		ENDIF
		
		i = 0
		IF ARE_THERE_ANY_ATTACKER_AIR_VEHICLES()
			REPEAT serverBD.sAttackerVehStruct.iNumAttackerHelis i
				IF NOT CREATE_ATTACKER_AIR_VEHICLE(i)
					RETURN FALSE
				ENDIF
			ENDREPEAT
		ENDIF
	
		PRINTLN("[GB YACHT ATTACK] [STMGB] - CREATE_ATTACKER_VEHS - All attacker vehicles have been created")
		RETURN TRUE
	ELSE
		PRINTLN("[GB YACHT ATTACK] [STMGB] - CREATE_ATTACKER_VEHS - Waiting for models to load..")
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_ATTACKER_SEA_VEHICLE_BE_RESPAWNED(INT index)

	IF IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niAttackerSeaVeh[index]))
		PRINTLN("[GB YACHT ATTACK SPAM] - SHOULD_ATTACKER_SEA_VEHICLE_BE_RESPAWNED - YES! Sea Veh ", index, " is Dead")
		RETURN TRUE
	ENDIF

	FLOAT fDist = VDIST(GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niAttackerSeaVeh[index])), serverBD.sAttackerVehStruct.vAttackerBoatSpawnLoc[index])
	
	IF IS_VEHICLE_EMPTY(NET_TO_VEH(serverBD.niAttackerSeaVeh[index]), TRUE)
	AND ( (fDist > 50) OR (HAS_NET_TIMER_STARTED(serverBD.attackerSeaVehRespawnTimer[index])) )
		PRINTLN("[GB YACHT ATTACK SPAM] - SHOULD_ATTACKER_SEA_VEHICLE_BE_RESPAWNED - YES! Sea Veh ", index, " is > 50m from spawn and empty")
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_ATTACKER_AIR_VEHICLE_BE_RESPAWNED(INT index)

	IF IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niAttackerAirVeh[index]))
		PRINTLN("[GB YACHT ATTACK SPAM] - SHOULD_ATTACKER_AIR_VEHICLE_BE_RESPAWNED - YES! Air Veh ", index, " is Dead")
		RETURN TRUE
	ENDIF
	
	FLOAT fDist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(NET_TO_VEH(serverBD.niAttackerAirVeh[index]), serverBD.sAttackerVehStruct.vAttackerHeliSpawnLoc[index], FALSE)
	
	IF IS_VEHICLE_EMPTY(NET_TO_VEH(serverBD.niAttackerAirVeh[index]), TRUE)
	AND ( (fDist > 50) OR (HAS_NET_TIMER_STARTED(serverBD.attackerAirVehRespawnTimer[index])) )
		PRINTLN("[GB YACHT ATTACK SPAM] - SHOULD_ATTACKER_AIR_VEHICLE_BE_RESPAWNED - YES! Air Veh ", index, " is > 50m from spawn and empty")
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC HANDLE_RESPAWNING_ATTACKER_SEA_VEHICLES()
	IF ARE_THERE_ANY_ATTACKER_SEA_VEHICLES()
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niAttackerSeaVeh[iAttackerSeaVehStagger])
			IF SHOULD_ATTACKER_SEA_VEHICLE_BE_RESPAWNED(iAttackerSeaVehStagger)
				IF NOT IS_BIT_SET(serverBD.iDeleteSeaVehBitSet, iAttackerSeaVehStagger)
					IF NOT HAS_NET_TIMER_STARTED(serverBD.attackerSeaVehRespawnTimer[iAttackerSeaVehStagger])
						START_NET_TIMER(serverBD.attackerSeaVehRespawnTimer[iAttackerSeaVehStagger])
						PRINTLN("[GB YACHT ATTACK] [STMGB] - HANDLE_RESPAWNING_ATTACKER_VEHICLES - Sea vehicle ", iAttackerSeaVehStagger, " has been destroyed, starting respawn timer")
					ELSE
						IF HAS_NET_TIMER_EXPIRED(serverBD.attackerSeaVehRespawnTimer[iAttackerSeaVehStagger], ATTACKER_VEH_RESPAWN_TIME)
							PRINTLN("[GB YACHT ATTACK] [STMGB] - HANDLE_RESPAWNING_ATTACKER_VEHICLES - Sea vehicle ", iAttackerSeaVehStagger, " respawn timer has expired, deleting existing vehicle to create new one")
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niAttackerSeaVeh[iAttackerSeaVehStagger])
								DELETE_NET_ID(serverBD.niAttackerSeaVeh[iAttackerSeaVehStagger])
								SET_BIT(serverBD.iDeleteSeaVehBitSet, iAttackerSeaVehStagger)
								RESET_NET_TIMER(serverBD.attackerSeaVehRespawnTimer[iAttackerSeaVehStagger])
							ELSE
								PRINTLN("[GB YACHT ATTACK] [STMGB] - HANDLE_RESPAWNING_ATTACKER_VEHICLES - Server doesnt have control of sea vehicle ", iAttackerSeaVehStagger)
								SET_BIT(serverBD.iDeleteSeaVehBitSet, iAttackerSeaVehStagger)
								RESET_NET_TIMER(serverBD.attackerSeaVehRespawnTimer[iAttackerSeaVehStagger])
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF HAS_NET_TIMER_STARTED(serverBD.attackerSeaVehRespawnTimer[iAttackerSeaVehStagger])
					RESET_NET_TIMER(serverBD.attackerSeaVehRespawnTimer[iAttackerSeaVehStagger])
				ENDIF
			ENDIF
		ELSE
			IF REQUEST_LOAD_MODEL(serverBD.sAttackerVehStruct.mAttackerBoatModel)
				IF CREATE_ATTACKER_SEA_VEHICLE(iAttackerSeaVehStagger, TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(serverBD.sAttackerVehStruct.mAttackerBoatModel)
					CLEAR_BIT(serverBD.iDeleteSeaVehBitSet, iAttackerSeaVehStagger)
					PRINTLN("[GB YACHT ATTACK] [STMGB] - HANDLE_RESPAWNING_ATTACKER_VEHICLES - Sea vehicle ", iAttackerSeaVehStagger, " has been respawned after having been destroyed")
				ENDIF
			ENDIF
		ENDIF
		
		iAttackerSeaVehStagger++
	
		IF iAttackerSeaVehStagger = serverBD.sAttackerVehStruct.iNumAttackerBoats
			iAttackerSeaVehStagger = 0
		ENDIF
		
	ENDIF
ENDPROC

PROC HANDLE_RESPAWNING_ATTACKER_AIR_VEHICLES()
	IF ARE_THERE_ANY_ATTACKER_AIR_VEHICLES()
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niAttackerAirVeh[iAttackerAirVehStagger])
			IF SHOULD_ATTACKER_AIR_VEHICLE_BE_RESPAWNED(iAttackerAirVehStagger)
				IF NOT HAS_NET_TIMER_STARTED(serverBD.attackerAirVehRespawnTimer[iAttackerAirVehStagger])
					START_NET_TIMER(serverBD.attackerAirVehRespawnTimer[iAttackerAirVehStagger])
					PRINTLN("[GB YACHT ATTACK] [STMGB] - HANDLE_RESPAWNING_ATTACKER_VEHICLES - Air vehicle ", iAttackerAirVehStagger, " has been destroyed, starting respawn timer")
				ELSE
					IF HAS_NET_TIMER_EXPIRED(serverBD.attackerAirVehRespawnTimer[iAttackerAirVehStagger], ATTACKER_VEH_RESPAWN_TIME)
						IF NOT IS_BIT_SET(serverBD.iDeleteAirVehBitSet, iAttackerAirVehStagger)
							PRINTLN("[GB YACHT ATTACK] [STMGB] - HANDLE_RESPAWNING_ATTACKER_VEHICLES - Air vehicle ", iAttackerAirVehStagger, " respawn timer has expired, deleting existing vehicle to create new one")
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niAttackerAirVeh[iAttackerAirVehStagger])
								DELETE_NET_ID(serverBD.niAttackerAirVeh[iAttackerAirVehStagger])
								SET_BIT(serverBD.iDeleteAirVehBitSet, iAttackerAirVehStagger)
								RESET_NET_TIMER(serverBD.attackerAirVehRespawnTimer[iAttackerAirVehStagger])
							ELSE
								PRINTLN("[GB YACHT ATTACK] [STMGB] - HANDLE_RESPAWNING_ATTACKER_VEHICLES - Server doesnt have control of air vehicle ", iAttackerAirVehStagger)
								SET_BIT(serverBD.iDeleteAirVehBitSet, iAttackerAirVehStagger)
								RESET_NET_TIMER(serverBD.attackerAirVehRespawnTimer[iAttackerAirVehStagger])
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF HAS_NET_TIMER_STARTED(serverBD.attackerAirVehRespawnTimer[iAttackerAirVehStagger])
					RESET_NET_TIMER(serverBD.attackerAirVehRespawnTimer[iAttackerAirVehStagger])
				ENDIF
			ENDIF
		ELSE
			IF REQUEST_LOAD_MODEL(serverBD.sAttackerVehStruct.mAttackerHeliModel)
				IF CREATE_ATTACKER_AIR_VEHICLE(iAttackerAirVehStagger, TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(serverBD.sAttackerVehStruct.mAttackerHeliModel)
					CLEAR_BIT(serverBD.iDeleteAirVehBitSet, iAttackerAirVehStagger)
					RESET_NET_TIMER(serverBD.attackerAirVehRespawnTimer[iAttackerAirVehStagger])
					PRINTLN("[GB YACHT ATTACK] [STMGB] - HANDLE_RESPAWNING_ATTACKER_VEHICLES - Air vehicle ", iAttackerAirVehStagger, " has been respawned after having been destroyed")
				ENDIF
			ENDIF
		ENDIF
		
		iAttackerAirVehStagger++
	
		IF iAttackerAirVehStagger = serverBD.sAttackerVehStruct.iNumAttackerHelis
			iAttackerAirVehStagger = 0
		ENDIF
		
	ENDIF
ENDPROC

PROC HANDLE_BOSS_ABANDON_CHECKS()
	IF NOT HAS_BOSS_ABANDONED_EVENT()
		IF GET_GANG_BOSS_PLAYER_INDEX() != INVALID_PLAYER_INDEX()
			IF NOT NETWORK_IS_PLAYER_ACTIVE(GET_GANG_BOSS_PLAYER_INDEX())
				SET_BIT(serverBD.iServerBitSet, biS_GangBossAbandoned)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL CAN_PLAYER_CAPTURE_AREA(PLAYER_INDEX thisPlayer)

	IF NOT IS_NET_PLAYER_OK(thisPlayer, TRUE)
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_SPECTATING(thisPlayer)
		RETURN FALSE
	ENDIF
		
	IF GB_IS_GLOBAL_CLIENT_BIT0_SET(thisPlayer, eGB_GLOBAL_CLIENT_BITSET_0_HAS_HIDDEN_ACTIVE_EVENT)
		RETURN FALSE
	ENDIF
	
	IF GB_IS_GLOBAL_CLIENT_BIT0_SET(thisPlayer, eGB_GLOBAL_CLIENT_BITSET_0_RESTRICTED_WITH_HIDE)
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(playerBD[NATIVE_TO_INT(thisPlayer)].iClientBitSet, biP_HiddenEvent)
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(playerBD[NATIVE_TO_INT(thisPlayer)].iClientBitSet, biP_IsPassive)
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_PASSIVE(thisPlayer)
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_BEAST(thisPlayer)
		RETURN FALSE
	ENDIF
	
	IF FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT(thisPlayer)
		RETURN FALSE
	ENDIF
	
	IF FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_FM_EVENT(thisPlayer)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_ANY_NON_GANG_PLAYER_AT_BRIDGE()
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			PLAYER_INDEX playerID = INT_TO_PLAYERINDEX(i)
			
			IF playerID != INVALID_PLAYER_INDEX()
				IF NOT IS_THIS_PLAYER_A_GANG_GOON(playerID)
				AND NOT IS_THIS_PLAYER_THE_GANG_BOSS(playerID)
					IF CAN_PLAYER_CAPTURE_AREA(playerID)
						IF IS_ENTITY_IN_ANGLED_AREA( GET_PLAYER_PED(playerID), GET_CAPTURE_ZONE_POINT_1_COORDS(), GET_CAPTURE_ZONE_POINT_2_COORDS(), GET_CAPTURE_ZONE_WIDTH())
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

PROC HANDLE_PLAYERS_AT_BRIDGE()
	IF IS_ANY_NON_GANG_PLAYER_AT_BRIDGE()
		IF NOT HAS_NET_TIMER_STARTED(serverBD.CaptureTimer)
			START_NET_TIMER(serverBD.CaptureTimer)
		ENDIF
	ELSE
		IF HAS_NET_TIMER_STARTED(serverBD.CaptureTimer)
			RESET_NET_TIMER(serverBD.CaptureTimer)
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_BOSS_LEAVING()
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_BossLeft)
		IF NOT NETWORK_IS_PLAYER_ACTIVE(GET_GANG_BOSS_PLAYER_INDEX())
			SET_BIT(serverBD.iServerBitSet, biS_BossLeft)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAVE_END_CONDITIONS_BEEN_MET()

	IF IS_ANY_NON_GANG_PLAYER_AT_BRIDGE()
		IF HAS_NET_TIMER_STARTED(serverBD.CaptureTimer)
			IF HAS_NET_TIMER_EXPIRED(serverBD.CaptureTimer, YA_TUNABLE_CAPTURE_DURATION())
				SET_BIT(serverBD.iServerBitSet, biS_BridgeTaken)
				PRINTLN("[GB YACHT ATTACK] [STMGB] - HAVE_END_CONDITIONS_BEEN_MET - HAS_NET_TIMER_EXPIRED - CaptureTimer - biS_BridgeTaken set, returning TRUE")
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		IF HAS_NET_TIMER_STARTED(serverBD.DefendTimer)
			IF HAS_NET_TIMER_EXPIRED(serverBD.DefendTimer, YA_TUNABLE_EVENT_DURATION())
				PRINTLN("[GB YACHT ATTACK] [STMGB] - HAVE_END_CONDITIONS_BEEN_MET - HAS_NET_TIMER_EXPIRED - DefendTimer - Gang has held off attackers, biS_SuccessfullyDefended is being set.")
				SET_BIT(serverBD.iServerBitSet, biS_SuccessfullyDefended)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_MOVE_TO_EVENT_END()
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_BossLeft)
		PRINTLN("[GB YACHT ATTACK] [STMGB] - SHOULD_MOVE_TO_EVENT_END - Yes - Boss left session")
		RETURN TRUE
	ENDIF
	
	IF HAVE_END_CONDITIONS_BEEN_MET()
		PRINTLN("[GB YACHT ATTACK] [STMGB] - SHOULD_MOVE_TO_EVENT_END - Yes - End conditions have been met (captured/defended)")
		RETURN TRUE
	ENDIF
	
	IF HAS_BOSS_ABANDONED_EVENT()
		PRINTLN("[GB YACHT ATTACK] [STMGB] - SHOULD_MOVE_TO_EVENT_END - Yes - HAS_BOSS_ABANDONED_EVENT, boss has abandoned event")
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL GET_NUM_PLAYERS_IN_CAPTURE_AREA()
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_SuccessfullyDefended)
		RETURN TRUE
	ELSE
		INT i = 0
		INT iCount = 0
		INT iPlayer = -1
		REPEAT NUM_NETWORK_PLAYERS i
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
				PLAYER_INDEX player = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
				PED_INDEX ped = GET_PLAYER_PED(player)
				
				IF player != INVALID_PLAYER_INDEX()
					IF NOT IS_THIS_PLAYER_A_GANG_GOON(player)
					AND NOT IS_THIS_PLAYER_THE_GANG_BOSS(player)
						IF IS_ENTITY_IN_ANGLED_AREA( ped, GET_CAPTURE_ZONE_POINT_1_COORDS(), GET_CAPTURE_ZONE_POINT_2_COORDS(), GET_CAPTURE_ZONE_WIDTH())
							iCount++
							iPlayer = i
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		// IF only one player in capture area, write who they are
		IF iCount = 1
			IF iPlayer != -1
				serverBD.piCapturingPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPlayer))
				serverBD.iNumPlayersInCaptureArea = iCount
				PRINTLN("[GB YACHT ATTACK] - GET_NUM_PLAYERS_IN_CAPTURE_AREA - there is one player in capture area, they are ", GET_PLAYER_NAME(serverBD.piCapturingPlayer))
			ENDIF
		ELIF iCount > 1
			serverBD.iNumPlayersInCaptureArea = iCount
			PRINTLN("[GB YACHT ATTACK] - GET_NUM_PLAYERS_IN_CAPTURE_AREA - there are ", serverBD.iNumPlayersInCaptureArea, " players in the capture area")
		ELSE
			serverBD.iNumPlayersInCaptureArea = 0
			PRINTLN("[GB YACHT ATTACK] - GET_NUM_PLAYERS_IN_CAPTURE_AREA - there are ", serverBD.iNumPlayersInCaptureArea, " players in the capture area, there should be some, speak to Steve Tiley")
		ENDIF
		
		RETURN TRUE
		
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Process the server
PROC PROCESS_SERVER()
	
	HANDLE_BOSS_LEAVING()

	SWITCH GET_SERVER_MISSION_STAGE()
		
		CASE eMS_DEFEND
		CASE eMS_ATTACK
			HANDLE_RESPAWNING_ATTACKER_SEA_VEHICLES()
			HANDLE_RESPAWNING_ATTACKER_AIR_VEHICLES()
			HANDLE_BOSS_ABANDON_CHECKS()
			HANDLE_PLAYERS_AT_BRIDGE()
			IF SHOULD_MOVE_TO_EVENT_END()
				IF GET_NUM_PLAYERS_IN_CAPTURE_AREA()
					SET_SERVER_MISSION_STAGE(eMS_END)
				ENDIF
			ENDIF
			IF NOT HAS_NET_TIMER_STARTED(serverBD.DefendTimer)
				START_NET_TIMER(serverBD.DefendTimer)
			ENDIF
			
		BREAK
		
		CASE eMS_END
//			IF NOT HAS_NET_TIMER_STARTED(serverBD.EndTimer)
//				SET_BIT(serverBD.iServerBitSet, biS_YachtAttackEnded)
//				PRINTLN("[GB YACHT ATTACK] [STMGB] - PROCESS_SERVER - START_NET_TIMER - EndTimer - Event end timer has started.")
//				START_NET_TIMER(serverBD.EndTimer)
//			ELSE
//				IF HAS_NET_TIMER_EXPIRED(serverBD.EndTimer, TIME_END_TIME)
//					PRINTLN("[GB YACHT ATTACK] [STMGB] - PROCESS_SERVER - HAS_NET_TIMER_EXPIRED - EndTimer - Event end timer has run out.")
//					SET_SERVER_MISSION_STAGE(eMS_CLEANUP)
//				ENDIF
//			ENDIF
		BREAK
		
		CASE eMS_CLEANUP
			SET_SERVER_MISSION_STATE(GAME_STATE_END)
		BREAK

	ENDSWITCH
ENDPROC

FUNC BOOL SETUP_DEFENDER_SPAWN_OFFSETS()
	
	vDefenderSpawnOffset[0] = <<12.480,  5.700,  2.070>>
	vDefenderSpawnOffset[1] = <<12.480, -5.700,  2.070>>
	vDefenderSpawnOffset[2] = <<41.170,  6.450, -1.730>>
	vDefenderSpawnOffset[3] = <<41.170, -6.450, -1.730>>
	vDefenderSpawnOffset[4] = <<50.100,  6.450, -4.820>>
	vDefenderSpawnOffset[5] = <<50.100, -6.450, -4.820>>
	vDefenderSpawnOffset[6] = <<68.660,  6.400, -4.820>>
	vDefenderSpawnOffset[7] = <<68.660, -6.400, -4.820>>
	vDefenderSpawnOffset[8] = <<37.070,  5.000,  1.200>>
	vDefenderSpawnOffset[9] = <<37.070, -5.000,  1.200>>
	
	fDefenderSpawnHeadingOffset[0] = 178.209839
	fDefenderSpawnHeadingOffset[1] = 178.184265
	fDefenderSpawnHeadingOffset[2] = 175.935547
	fDefenderSpawnHeadingOffset[3] = 186.976990
	fDefenderSpawnHeadingOffset[4] = 171.142029
	fDefenderSpawnHeadingOffset[5] = 187.166809
	fDefenderSpawnHeadingOffset[6] = 10.998230
	fDefenderSpawnHeadingOffset[7] = 342.088440
	fDefenderSpawnHeadingOffset[8] = 114.053215
	fDefenderSpawnHeadingOffset[9] = 247.557251
	
	INT i
	REPEAT NUM_DEFENDER_SPAWN_OFFSETS i
		IF IS_VECTOR_ZERO(vDefenderSpawnOffset[i])
			PRINTLN("[GB YACHT ATTACK] [STMGB] - INITIALISATION - SETUP_DEFENDER_SPAWN_OFFSETS - vector vDefenderSpawnOffset[", i, "] is ZERO, returning false.")
			RETURN FALSE
		ENDIF
	
	ENDREPEAT
	
	PRINTLN("[GB YACHT ATTACK] [STMGB] - INITIALISATION - SETUP_DEFENDER_SPAWN_OFFSETS - vectors set up correctly")
	RETURN TRUE
ENDFUNC

PROC SET_SERVER_DATA(VECTOR vYachtCoords, FLOAT fYachtHeading)
	SET_YACHT_COORDS(vYachtCoords)
	SET_YACHT_HEADING(fYachtHeading)
	
	SET_CAPTURE_POINT_COORDS(GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vYachtCoords, fYachtHeading, vCapPointOffset))
	SET_CAPTURE_POINT_DIMENSIONS(vCapDimensions)
	
	SET_CAPTURE_ZONE_POINT_1(GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vYachtCoords, fYachtHeading, vCapZonePoint1Offset))
	SET_CAPTURE_ZONE_POINT_2(GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vYachtCoords, fYachtHeading, vCapZonePoint2Offset))
	SET_CAPTURE_ZONE_WIDTH(fCapZoneWidth)
	
//	SET_YACHT_COORDS(<<-1579.8, -4083.6, 12.6>>)
//	SET_BRIDGE_COORDS(<<-1595.34644, -4078.53540, 10.90739>>)	// Bridge temporarily just outside of the bridge room
//	SET_BRIDGE_DIMENSIONS(<<4, 4, 3>>)
//	serverBD.vAttackerRespawnLocation = <<-1391.3132, -3418.1492, 9.7128>>
//	serverBD.fAttackerRespawnHeading = GET_HEADING_BETWEEN_VECTORS_2D(serverBD.vAttackerRespawnLocation, GET_YACHT_COORDS())
ENDPROC

FUNC BOOL SETUP_ATTACKER_VEHICLES(INT iYacht)
	IF bGrabbedYachtInfo = FALSE
		serverBD.sAttackerVehStruct = SETUP_ATTACKER_VEHICLE_DATA(iYacht)
		PRINTLN("[GB YACHT ATTACK] [STMGB] - SETUP_ATTACKER_VEHICLES - Attacker vehicle data has been acquired.")
		bGrabbedYachtInfo = TRUE
	ENDIF

	IF CREATE_ATTACKER_VEHS()
	
		// Release models from memory
		IF HAS_MODEL_LOADED(serverBD.sAttackerVehStruct.mAttackerBoatModel)
			SET_MODEL_AS_NO_LONGER_NEEDED(serverBD.sAttackerVehStruct.mAttackerBoatModel)
		ENDIF
	
		IF HAS_MODEL_LOADED(serverBD.sAttackerVehStruct.mAttackerHeliModel)
			SET_MODEL_AS_NO_LONGER_NEEDED(serverBD.sAttackerVehStruct.mAttackerHeliModel)
		ENDIF
		
		RETURN TRUE
	ELSE
		PRINTLN("[GB YACHT ATTACK] [STMGB] - SETUP_ATTACKER_VEHICLES - Waiting on attacker vehicles to be created...")
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Performs generic server initialisation. This will only be hit once, and by the initial host (the gang boss)
/// RETURNS:
///    
FUNC BOOL INIT_SERVER()

//	IF CREATE_ATTACKER_VEHS()
//	PRINTLN("[GB YACHT ATTACK] [STMGB] - INIT_SERVER - Attacker vehicles have been created, performing additional setup.")

	serverBD.iYachtToUse = GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID())
	
	PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBD.iMatchId1, serverBD.iMatchId2)

	IF IS_PRIVATE_YACHT_ID_VALID(serverBD.iYachtToUse)
		IF SETUP_ATTACKER_VEHICLES(serverBD.iYachtToUse)

			IF bBossMissionSetupDone = FALSE
				GB_COMMON_BOSS_MISSION_SETUP(FMMC_TYPE_GB_YACHT_ROBBERY)
				bBossMissionSetupDone = TRUE
			ENDIF
			
			GET_ATTACKER_SPAWN_AREA(serverBD.iYachtToUse, 
									serverBD.vAttackerSpawnLocation, 
									serverBD.fAttackerSpawnHeading, 
									serverBD.fAttackerSpawnRadius)
			
			CLEAR_AREA(serverBD.vAttackerSpawnLocation, serverBD.fAttackerSpawnRadius, TRUE, FALSE, FALSE, TRUE)
			
			PRINTLN("[GB YACHT ATTACK] [STMGB] - INIT_SERVER - Setting Boss player index and yacht coords.")
			serverBD.piGangBoss = PLAYER_ID()
					
			MP_PROP_OFFSET_STRUCT tempStruct = GET_BASE_YACHT_INTERIOR_LOCATION(GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID()))
			
			SET_SERVER_DATA(tempStruct.vLoc, tempStruct.vRot.z)
			
			GB_SET_BOSS_YACHT_BEING_USED_FOR_YACHT_ATTACK(serverBD.iYachtToUse)
			
			RETURN TRUE
		ENDIF	
	ENDIF
	
//	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Performs generic client initialisation. This will only be hit once
/// RETURNS:
///    
FUNC BOOL INIT_CLIENT()

	IF SETUP_DEFENDER_SPAWN_OFFSETS()
		GET_HUD_COLOUR(HUD_COLOUR_YELLOW, r, g, b, a)
		GET_HUD_COLOUR(HUD_COLOUR_WHITE, deadr, deadg, deadb, deada)
		
		SET_IGNORE_BAD_SPORT_RATING(TRUE)
		
		IF bBossMissionSetupDone = FALSE
			GB_COMMON_BOSS_MISSION_SETUP(FMMC_TYPE_GB_YACHT_ROBBERY)
			bBossMissionSetupDone = TRUE
		ENDIF
				
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------

PROC HANDLE_TELEMETRY_CLEANUP()
	IF PARTICIPANT_ID_TO_INT() != -1
		// If I've not reached the end of the event and we've not cleaned up due to low numbers, then I've left
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biP_RewardGiven)
		AND bLowNumbers = FALSE
		AND serverBD.iServerEndReason = -1
			GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LEFT)
		ENDIF
		g_sGb_Telemetry_data.sdata.m_match1 = serverBD.iMatchId1
		g_sGb_Telemetry_data.sdata.m_match2 = serverBD.iMatchId2
		PROCESS_CURRENT_BOSS_WORK_PLAYSTATS()
	ENDIF
ENDPROC

FUNC BOOL IS_SAFE_TO_CLEANUP()
	IF AM_I_A_GANG_GOON()
	OR AM_I_THE_GANG_BOSS()
		RETURN TRUE
	ELSE
		IF IS_PLAYER_ON_YACHT(PLAYER_ID(), serverBD.iYachtToUse)
		OR iAttackerWarpStep > 0 AND iAttackerWarpStep < 5
			SWITCH iAttackerWarpStep
				CASE 0
					IF NOT IS_SCREEN_FADED_OUT()
						SETUP_SPECIFIC_SPAWN_LOCATION(serverBD.vAttackerSpawnLocation, 0.0, 150.0, TRUE, 0, FALSE, FALSE, 65, TRUE, TRUE)
						DO_SCREEN_FADE_OUT(500)
						PRINTLN("[GB YACHT ATTACK] [STMGB] [Step ", iAttackerWarpStep, "] - IS_SAFE_TO_CLEANUP - Fading screen out")
						iAttackerWarpStep++
					ENDIF
				BREAK
				
				CASE 1
					IF IS_SCREEN_FADED_OUT()
						PRINTLN("[GB YACHT ATTACK] [STMGB] [Step ", iAttackerWarpStep, "] - IS_SAFE_TO_CLEANUP - Screen is fully faded")
						iAttackerWarpStep++
					ENDIF
				BREAK
				
				CASE 2
					IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_NEAR_SPECIFIC_COORDS, FALSE)
						PRINTLN("[GB YACHT ATTACK] [STMGB] [Step ", iAttackerWarpStep, "] - IS_SAFE_TO_CLEANUP - Player has been warped to shore")
						iAttackerWarpStep++
					ENDIF
				BREAK
				
				CASE 3
					IF NOT IS_SCREEN_FADED_IN()
						DO_SCREEN_FADE_IN(500)
						PRINTLN("[GB YACHT ATTACK] [STMGB] [Step ", iAttackerWarpStep, "] - IS_SAFE_TO_CLEANUP - Screen is still faded out, fading in")
						iAttackerWarpStep++
					ELSE
						PRINTLN("[GB YACHT ATTACK] [STMGB] [Step ", iAttackerWarpStep, "] - IS_SAFE_TO_CLEANUP - Screen is faded in, moving to next step")
						iAttackerWarpStep++
					ENDIF
				BREAK
				
				CASE 4
					IF IS_SCREEN_FADED_IN()
						PRINTLN("[GB YACHT ATTACK] [STMGB] [Step ", iAttackerWarpStep, "] - IS_SAFE_TO_CLEANUP - Screen is faded in, returning true")
						DO_HELP_TEXT(7)
						CLEAR_SPECIFIC_SPAWN_LOCATION()
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SCRIPT_CLEANUP(BOOL bImmediately = FALSE)
	IF bImmediately
	OR IS_SAFE_TO_CLEANUP()
		PRINTLN("[GB YACHT ATTACK] [STMGB] - SCRIPT_CLEANUP - this has started.")
		
		HANDLE_TELEMETRY_CLEANUP()

		IF bSetSpawnLocations
			IF AM_I_THE_GANG_BOSS()
			OR AM_I_A_GANG_GOON()
				PRINTLN("[GB YACHT ATTACK] [STMGB] - SCRIPT_CLEANUP - calling USE_CUSTOM_SPAWN_POINTS(FALSE)")
				CLEAR_CUSTOM_SPAWN_POINTS()
				USE_CUSTOM_SPAWN_POINTS(FALSE)
				bSetSpawnLocations = FALSE
			ELSE
				PRINTLN("[GB YACHT ATTACK] [STMGB] - SCRIPT_CLEANUP - calling CLEAR_PLAYER_NEXT_RESPAWN_LOCATION")
				CLEAR_PLAYER_NEXT_RESPAWN_LOCATION()
				CLEAR_SPECIFIC_SPAWN_LOCATION_FROM_THIS_SCRIPT()
				bSetSpawnLocations = FALSE
			ENDIF
		ENDIF
		
		IF NATIVE_TO_INT(PLAYER_ID()) != -1
			GB_COMMON_BOSS_MISSION_CLEANUP()
		ENDIF
		
		SET_IGNORE_BAD_SPORT_RATING(FALSE)
		
		SET_FREEMODE_FLOW_COMMS_DISABLED(FALSE)
		
		//BLOCK_ALL_MISSIONSATCOORDS_MISSIONS(FALSE)

		// Clear participation bits
		IF NATIVE_TO_INT(PLAYER_ID()) != -1
			GB_CLEAR_GLOBAL_CLIENT_BIT0(eGB_GLOBAL_CLIENT_BITSET_0_TEMPORARY_PARTICIPANT)
			GB_CLEAR_GLOBAL_CLIENT_BIT0(eGB_GLOBAL_CLIENT_BITSET_0_PERMANENT_PARTICIPANT)
		ENDIF
		
		IF IS_BLIP_HIDDEN_FOR_PRIVATE_YACHT(serverBD.iYachtToUse)
			SET_PRIVATE_YACHT_BLIP_AS_HIDDEN(serverBD.iYachtToUse, FALSE)
		ENDIF
		
		IF bSetMentalStateModifier = TRUE
			RESET_MENTAL_STATE_MULTIPLIER()
			bSetMentalStateModifier = FALSE
		ENDIF
		
		IF GET_MAX_WANTED_LEVEL() < 5
			SET_MAX_WANTED_LEVEL(5)
		ENDIF
		
		IF GET_GANG_BOSS_PLAYER_INDEX() = PLAYER_ID()
			CLEAR_HELP(TRUE)
			PRINT_HELP_NO_SOUND("GB_YA_HP6")
			GB_SET_GANG_BOSS_HELP_BACKGROUND()		
		ENDIF
		
		IF GET_GANG_BOSS_PLAYER_INDEX() = PLAYER_ID()
			GB_CLEAR_BOSS_YACHT_BEING_USED_FOR_YACHT_ATTACK()
			DO_HELP_TEXT(6)
		ENDIF
		
		IF bSuppressedWantedMusic
			SET_AUDIO_FLAG("WantedMusicDisabled", FALSE)
			SET_AUDIO_FLAG("DisableFlightMusic", FALSE)
			STOP_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
			bSuppressedWantedMusic = FALSE
		ENDIF
		
		MPGlobalsAmbience.iYachtAttackYacht = -1
		
		TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
	ENDIF
ENDPROC

PROC MAINTAIN_CLEANUP_CHECKS()
	IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_CLEANUP_CHECKS - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE - True, calling script cleanup")
		SCRIPT_CLEANUP(TRUE)
	ENDIF
	
	IF GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION()
		PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_CLEANUP_CHECKS - GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION - True, calling script cleanup")
		SCRIPT_CLEANUP(TRUE)
	ENDIF
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Pre Game
// -----------------------------------------------------------------------------------------------------------

FUNC BOOL PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)
	PRINTLN("[GB YACHT ATTACK] [STMGB] - PROCESS_PRE_GAME - this has started.")
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission), missionScriptArgs)
	
	//Reserve Entities
	RESERVE_NETWORK_MISSION_OBJECTS(0)
	RESERVE_NETWORK_MISSION_VEHICLES((MAX_ATTACKER_SEA_VEHS + MAX_ATTACKER_AIR_VEHS))
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
		
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		PRINTLN("[GB YACHT ATTACK] [STMGB] - PROCESS_PRE_GAME - FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP called.")
		SCRIPT_CLEANUP(TRUE)
	ENDIF
	
	IF serverBD.piGangBoss = INVALID_PLAYER_INDEX()
		GB_COMMON_BOSS_MISSION_PREGAME(FMMC_TYPE_GB_YACHT_ROBBERY, PLAYER_ID())
	ELSE
		GB_COMMON_BOSS_MISSION_PREGAME(FMMC_TYPE_GB_YACHT_ROBBERY, serverBD.piGangBoss)
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

// -----------------------------------------------------------------------------------------------------------
//		Debug Functions
// -----------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD

PROC MAINTAIN_DEBUG()
	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LT)
	AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
	AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RUP)
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vYachtCoord) > 20
				PRINTLN("[GB YACHT ATTACK] [STMGB] - MAINTAIN_DEBUG - player warped to yacht at ", vYachtCoord)
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vYachtCoord)
			ENDIF
		ENDIF
	ENDIF
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF GB_SHOULD_KILL_ACTIVE_BOSS_MISSION()
			PRINTLN("[GB SMASH AND GRAB] - MAINTAIN_DEBUG - GB_SHOULD_KILL_ACTIVE_BOSS_MISSION - moving server to cleanup.")
			serverBD.iServerEndReason = GB_TELEMETRY_END_FORCED
			SET_SERVER_MISSION_STAGE(eMS_CLEANUP)
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_WIDGETS()
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF bHostEndMissionNow
			PRINTLN("[GB YACHT ATTACK] [STMGB] - DEBUG - UPDATE_WIDGETS - bHostEndMissionNow is TRUE, killing event.")
			serverBD.iServerEndReason = GB_TELEMETRY_END_FORCED
			SET_SERVER_MISSION_STATE(GAME_STATE_END)
			bHostEndMissionNow = FALSE
		ENDIF
		IF bSuccessfullyDefendDebug
			PRINTLN("[GB YACHT ATTACK] [STMGB] - DEBUG - UPDATE_WIDGETS - bSuccessfullyDefendDebug is TRUE")
			serverBD.iServerEndReason = GB_TELEMETRY_END_FORCED
			SET_BIT(serverBD.iServerBitSet, biS_SuccessfullyDefended)
			bSuccessfullyDefendDebug = FALSE
		ENDIF
		IF bBridgeTakenDebug
			PRINTLN("[GB YACHT ATTACK] [STMGB] - DEBUG - UPDATE_WIDGETS - bBridgeTakenDebug is TRUE")
			serverBD.iServerEndReason = GB_TELEMETRY_END_FORCED
			SET_BIT(serverBD.iServerBitSet, biS_BridgeTaken)
			bBridgeTakenDebug = FALSE
		ENDIF
		
		IF bWarpToPoint = TRUE
			IF iSpawnPoint > -1 AND iSpawnPoint < 10
				SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_YACHT_COORDS(), GET_YACHT_HEADING(), vDefenderSpawnOffset[iSpawnPoint]))
				SET_ENTITY_HEADING(PLAYER_PED_ID(), GET_OFFSET_HEADING_FROM_YACHT_AS_WORLD_HEADING(serverBD.iYachtToUse, fDefenderSpawnHeadingOffset[iSpawnPoint]))
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			ENDIF
			bWarpToPoint = FALSE
		ENDIF
		
		IF bPrintHeading = TRUE
			PRINTLN("[ST DEBUG] [YACHT ATTACK] heading for spawn point ", iSpawnPoint, " is ", GET_HEADING_AS_OFFSET_FROM_YACHT_GIVEN_WORLD_HEADING(serverBD.iYachtToUse, GET_ENTITY_HEADING(PLAYER_PED_ID())))
			bPrintHeading = FALSE
		ENDIF
		
		VECTOR coords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_YACHT_COORDS(), GET_YACHT_HEADING(), <<xOffset, yOffset, zOffset>>)
		
		DRAW_MARKER(MARKER_CYLINDER, coords, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<0.5, 0.5, 0.2>>, r, g, b)
		
	ENDIF
ENDPROC

PROC MAINTAIN_WIDGETS()
	INT iPlayer
	TEXT_LABEL_15 tl63
	
	SWITCH iWidgetStage
	
		CASE 0
		
		YachtRobberyWidget = START_WIDGET_GROUP("GB Yacht Attack")
			ADD_WIDGET_BOOL("Create Yacht Attack Widgets", bCreateWidgets)
		STOP_WIDGET_GROUP()
		
		iWidgetStage++
		
		BREAK
		
		CASE 1
		
			IF bCreateWidgets
				
				DELETE_WIDGET_GROUP(YachtRobberyWidget)
				
				YachtRobberyWidget = START_WIDGET_GROUP("GB Yacht Attack") 
				
					START_WIDGET_GROUP("Yacht Offset Debugging (draws marker)")
						ADD_WIDGET_FLOAT_SLIDER("Offset x", xOffset, LOWEST_INT, HIGHEST_INT, 0.01)
						ADD_WIDGET_FLOAT_SLIDER("Offset y", yOffset, LOWEST_INT, HIGHEST_INT, 0.01)
						ADD_WIDGET_FLOAT_SLIDER("Offset z", zOffset, LOWEST_INT, HIGHEST_INT, 0.01)
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("Yacht Heading Debug")
						ADD_WIDGET_INT_SLIDER("Spawn Point", iSpawnPoint, 0, 9, 1)
						ADD_WIDGET_BOOL("Warp to Point", bWarpToPoint)
						ADD_WIDGET_BOOL("Print Heading", bPrintHeading)
					STOP_WIDGET_GROUP()
				
					// Gameplay debug, sever only.
					START_WIDGET_GROUP("Server Only Gameplay") 
						ADD_WIDGET_BOOL("End Mission: Debug end no reason (immediate)", bHostEndMissionNow)
						ADD_WIDGET_BOOL("End Mission: Successfully defend", bSuccessfullyDefendDebug)
						ADD_WIDGET_BOOL("End Mission: Bridge Captured", bBridgeTakenDebug)
					STOP_WIDGET_GROUP()		
					
					// Data about the server
					START_WIDGET_GROUP("Server BD") 
						ADD_WIDGET_INT_SLIDER("S. Game state", serverBD.iServerGameState,-1, HIGHEST_INT,1)
					STOP_WIDGET_GROUP()	

					// Data about the clients. * = You.
					START_WIDGET_GROUP("Client BD")  				
						REPEAT NUM_NETWORK_PLAYERS iPlayer
							tl63 = "Player "
							tl63 += iPlayer
							IF iPlayer = PARTICIPANT_ID_TO_INT()
								tl63 += "*"
							ENDIF
							START_WIDGET_GROUP(tl63)
								ADD_WIDGET_INT_SLIDER("Game state", playerBD[iPlayer].iClientGameState,-1, HIGHEST_INT,1)
							STOP_WIDGET_GROUP()
						ENDREPEAT
					STOP_WIDGET_GROUP()
					
					ADD_WIDGET_BOOL("Hide UI", bDebugHideYachtAttackUI)
				STOP_WIDGET_GROUP()
				
				PRINTLN("[GB YACHT ATTACK] [STMGB] -  Created Widgets")
				
				iWidgetStage++
			
			ENDIF
		
		BREAK
		
		CASE 2
			UPDATE_WIDGETS()
		BREAK
	
	ENDSWITCH
ENDPROC	
#ENDIF // Debug build

 // Feature Gang Boss

// -----------------------------------------------------------------------------------------------------------
//		MAIN SCRIPT
// -----------------------------------------------------------------------------------------------------------

SCRIPT(MP_MISSION_DATA missionScriptArgs)


	PRINTLN("[GB YACHT ATTACK] [STMGB] - GB_YACHT_ROB has launched! GL and HF.")
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NOT PROCESS_PRE_GAME(missionScriptArgs)
			PRINTLN("[GB YACHT ATTACK] [STMGB] - Process Pre Game failed. Calling script cleanup.")
			SCRIPT_CLEANUP(TRUE)
		ENDIF	
	ENDIF
	
	WHILE (TRUE)
	
		// One wait to rule them all. One wait to find them. One wait to bring them all, and in the darkness, bind them.
		MP_LOOP_WAIT_ZERO()
		
		MAINTAIN_CLEANUP_CHECKS()
		
		IF NOT IS_VECTOR_ZERO(GET_YACHT_COORDS())
			GB_MAINTAIN_BOSS_WORK_DISTANT_CHECKS(FMMC_TYPE_GB_YACHT_ROBBERY, GET_YACHT_COORDS(), bSet, 500, 750, bSetSpawnLocations)
		ENDIF
		
		GB_COMMON_BOSS_MISSION_EVERY_FRAME_CALLS(FMMC_TYPE_GB_YACHT_ROBBERY)
		
		#IF IS_DEBUG_BUILD
			MAINTAIN_WIDGETS()
			MAINTAIN_DEBUG()
		#ENDIF
		
		SWITCH GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT())
		
			CASE GAME_STATE_INIT
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					IF INIT_CLIENT()
						SET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT(), GAME_STATE_RUNNING)
					ENDIF
				ENDIF
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					PRINTLN("[GB YACHT ROB] - Moving CLIENT STATE from GAME_STATE_INIT to GAME_STATE_END - this should be debug only!!")
					SET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT(), GAME_STATE_END)
				ENDIF
			BREAK
			
			CASE GAME_STATE_RUNNING
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					PROCESS_CLIENT()
					
					DRAW_CHALLENGE_DPAD_LBD(g_GBLeaderboardStruct.challengeLbdStruct, g_GBLeaderboardStruct.siDpadMovie, SUB_BOSS, g_GBLeaderboardStruct.dpadVariables, 
											g_GBLeaderboardStruct.fmDpadStruct)
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					SET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT(), GAME_STATE_END)
				ENDIF
			BREAK
			
			CASE GAME_STATE_END
				PRINTLN("[GB YACHT ATTACK] - Script Cleanup has been called, natural flow.")
				SCRIPT_CLEANUP()
			BREAK
			
		ENDSWITCH
	
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
			SWITCH GET_SERVER_MISSION_STATE()
			
				CASE GAME_STATE_INIT
					IF INIT_SERVER()
						SET_SERVER_MISSION_STATE(GAME_STATE_RUNNING)
					ENDIF
				BREAK
				
				CASE GAME_STATE_RUNNING
					PROCESS_SERVER()
				BREAK
				
				CASE GAME_STATE_END
				BREAK
				
			ENDSWITCH
		
		ENDIF		
	ENDWHILE

	
	
// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT

