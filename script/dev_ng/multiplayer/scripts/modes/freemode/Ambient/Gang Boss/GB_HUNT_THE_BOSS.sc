/* ===================================================================================== *\
		MISSION NAME	:	GB_HUNT_THE_BOSS.sch
		AUTHOR			:	Alastair Costley - 2015/10/14
		DESCRIPTION		:	Standalone script for HUNT THE BOSS gang activity.
\* ===================================================================================== */

USING "globals.sch"



//----------------------
//	INCLUDES
//----------------------
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "flow_public_core_override.sch"
USING "net_wait_zero.sch"
USING "net_mission.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_gang_boss.sch"
USING "DM_Leaderboard.sch"


CONST_INT GEN_BS_DUMMY	0

CONST_INT GAME_STATE_INIT 				0
CONST_INT GAME_STATE_RUNNING			1
CONST_INT GAME_STATE_TERMINATE_DELAY	2
CONST_INT GAME_STATE_END				3

CONST_INT FIVE_SECONDS					5000
CONST_INT TEN_SECONDS					10000
CONST_INT FIFTEEN_SECONDS				15000
CONST_INT ONE_MINUTE 					60000
CONST_INT TWO_MINUTES					(ONE_MINUTE * 2)	

CONST_INT ciHTB_POSITION_SEED_NONE		0
CONST_INT ciHTB_POSITION_SEED_ONE		1
CONST_INT ciHTB_POSITION_SEED_TWO		2
CONST_INT ciHTB_POSITION_SEED_THREE		3
CONST_INT ciHTB_POSITION_SEED_FOUR		4
CONST_INT ciHTB_NUM_POSITIONS_ONE 		7
CONST_INT ciHTB_NUM_POSITIONS_TWO		7
CONST_INT ciHTB_NUM_POSITIONS_THREE 	6
CONST_INT ciHTB_NUM_POSITIONS_FOUR		5
CONST_INT ciHTB_NUM_POSITIONS_MAX		7
CONST_INT ciHTB_NUM_ZONES				4

CONST_INT ciHTB_TRIGGER_BUFFER					1
CONST_INT ciHTB_MIN_SHAKE_VALUE 				26 // Any lower is actually ultra-high frequency for some reason?
CONST_INT ciHTB_MAX_SHAKE_VALUE	 				175
CONST_INT ciHTB_RUMBLE_DURATION 				1000
CONST_INT ciHTB_ACTIVE_RANGE					500	
CONST_INT ciHTB_IDLE_BLIP_HEARTBEAT_INTERVAL	500
CONST_INT ciHTB_BLIP_ALPHA_INSIDE				70

CONST_INT biS_ModeExpired 				0
CONST_INT biS_BossLeftSession			1
CONST_INT biS_BossLeftArea				2
CONST_INT biS_BossKilled				3
CONST_INT biS_BossNoShow				4
CONST_INT biS_DoneTicker				5
CONST_INT biS_BossOutOfRange			6

#IF IS_DEBUG_BUILD
CONST_INT biS_DebugEndMode				31
#ENDIF

CONST_INT biC_ActiveParticipant			0
CONST_INT biC_OutOfRenderRange			1
CONST_INT biC_BossBlipHidden			2
CONST_INT biC_SentSMS					3
CONST_INT biC_CurrentlyIdle				4
CONST_INT biC_DoneRewards				5
CONST_INT biC_DoneWantedCleanup			6
CONST_INT biC_DoneTelemetry				7
CONST_INT biC_QualifiedParticipant		8
CONST_INT biC_PlayedBlipSound			9
CONST_INT biC_ForcedCustomBossBlip		10
CONST_INT biC_BossInvalid				11

CONST_INT biStat_One					(0 * (ciHTB_NUM_POSITIONS_MAX+1))
CONST_INT biStat_Two					(1 * (ciHTB_NUM_POSITIONS_MAX+1))
CONST_INT biStat_Three					(2 * (ciHTB_NUM_POSITIONS_MAX+1))
CONST_INT biStat_Four					(3 * (ciHTB_NUM_POSITIONS_MAX+1))

#IF IS_DEBUG_BUILD
CONST_INT biC_DebugEndMode				31
#ENDIF

CONST_FLOAT cfHTB_MIN_RUMBLE_DISTANCE 	5.0
CONST_FLOAT	cfHTB_MAX_RUMBLE_RANGE 		25.0
CONST_FLOAT	cfHTB_MAX_OVERHEAD_RANGE 	25.0
CONST_FLOAT cfHTB_ACTIVE_RANGE_MODIFER	1.5

ENUM GB_HTB_MISSION_STAGE
	eGBHTB_INIT = 0,
	eGBHTB_PLAYING,
	eGBHTB_END,
	eGBHTB_CLEANUP
ENDENUM

GB_UI_LEVEL eUserInterfaceLevel
HUD_COLOURS hudBossColour
GB_STRUCT_BOSS_END_UI sBossEndUI
BLIP_INDEX biTriggerArea
BLIP_INDEX biTriggerStart
INT iBigMessageBitSet
INT iHelpBitSet
INT iBlipAlphaCached
INT iRGBA[4]
BOOL bSetMental
BOOL bBossNetStateOK = FALSE
BOOL bPlayerShouldExit = FALSE
VECTOR vCachedBossPosition

INT 					iIdleBlipHeartbeatTracker
SCRIPT_TIMER 			IdleBlipHeartbeatTimer
SCRIPT_TIMER 			IdleBlipTrackTimer

//Sound variables
INT iSoundIDCountdown
INT iSoundBitset
TIME_DATATYPE iSoundStartTimer
CONST_INT	bit_MusicInit				0
CONST_INT	bit_EventMusicOn			1
CONST_INT	bit_EventEnded				2
CONST_INT	bit_Event35sToEnd			3
CONST_INT	bit_Event30sToEnd			4
CONST_INT	bit_Event27sToEnd			5
CONST_INT	bit_EventRadioDisabled		6
CONST_INT	bit_EventPlaying30sTimer	7
CONST_INT	bit_EventFadeInPlayed		8
CONST_INT	bit_EventEndedEarly			9
CONST_INT	bit_EventCountdownActive	10
CONST_INT	bit_AmbientMusicDisabled	11
CONST_INT	bit_EventInternalTimerStart 12
CONST_INT	bit_ActiveParticipant		13

CONST_INT	SOUND_EVENT_1_TIME			35000
CONST_INT	SOUND_EVENT_2_TIME			30000
CONST_INT	SOUND_EVENT_3_TIME			27000

// BROADCAST DATA \\
// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData
	INT 					iServerState = GAME_STATE_INIT
	GB_HTB_MISSION_STAGE 	eStage = eGBHTB_INIT
	
	INT 					iServerBitSet
	INT						iPositionSeed
	INT						iLocationCached
	PLAYER_INDEX 			piGangBoss
	PLAYER_INDEX			piWinner
	VECTOR					vTriggerPosition
	SCRIPT_TIMER 			ModeStartTimer
	SCRIPT_TIMER 			ModeDurationTimer
	SCRIPT_TIMER 			ModeEndTimer
	SCRIPT_TIMER 			OutOfRangeTimer
	SCRIPT_TIMER 			OutOfRangeBlipTimer
	
	INT						iMatchId1
	INT 					iMatchId2
ENDSTRUCT
ServerBroadcastData serverBD

// The client broadcast data.
STRUCT PlayerBroadcastData
	INT 					iClientState = GAME_STATE_INIT
	GB_HTB_MISSION_STAGE 	eStage = eGBHTB_INIT
	
	INT 					iClientBitSet
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

// DEBUG ONLY \\ 
#IF IS_DEBUG_BUILD

DEBUG_CHANNELS dbChan = DEBUG_NET_MAGNATE, dbChan2 = DEBUG_NET_MAGNATE
WIDGET_GROUP_ID wgHuntTheBossWidget
BOOL bCreateWidgets
INT iWidgetState
BOOL bHostEndMissionNow
BOOL bSkipIntro
BOOL bShowIntro
BOOL bExpireTimer
BOOL bResetLocationData

FUNC STRING GET_HUD_COLOUR_STRING(HUD_COLOURS thisColour)
	SWITCH thisColour
		DEFAULT					RETURN "Invalid Colour"
		CASE HUD_COLOUR_RED 	RETURN "Red"
		CASE HUD_COLOUR_YELLOW 	RETURN "Yellow"
		CASE HUD_COLOUR_GREEN 	RETURN "Green"
	ENDSWITCH
ENDFUNC

FUNC STRING GET_MISSION_STATE_STRING(INT iState)
	SWITCH(iState)
		DEFAULT								RETURN "UNKNOWN GAME STATE"
		CASE GAME_STATE_INIT 				RETURN "GAME_STATE_INIT"
		CASE GAME_STATE_RUNNING 			RETURN "GAME_STATE_RUNNING"
		CASE GAME_STATE_TERMINATE_DELAY 	RETURN "GAME_STATE_TERMINATE_DELAY"
		CASE GAME_STATE_END 				RETURN "GAME_STATE_END"
	ENDSWITCH
ENDFUNC

FUNC STRING GET_MISSION_STAGE_STRING(GB_HTB_MISSION_STAGE thisStage)
	SWITCH thisStage
		DEFAULT						RETURN "UNKNOWN SERVER MISSION STATE"
		CASE eGBHTB_INIT		RETURN "eGBHTB_INIT"
		CASE eGBHTB_PLAYING		RETURN "eGBHTB_PLAYING"
		CASE eGBHTB_END		RETURN "eGBHTB_END"
		CASE eGBHTB_CLEANUP	RETURN "eGBHTB_CLEANUP"
	ENDSWITCH
ENDFUNC

PROC UPDATE_WIDGETS()

	IF bResetLocationData
		SET_MP_INT_CHARACTER_STAT(MP_STAT_GB_HTB_PREV_LOC, 0)
		bResetLocationData = FALSE
		CPRINTLN(dbChan, "[GB][HTB] - DEBUG: UPDATE_WIDGETS - Reset saved location data.")
	ENDIF

	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF bHostEndMissionNow
			CPRINTLN(dbChan, "[GB][HTB] - DEBUG: UPDATE_WIDGETS - bHostEndMissionNow is TRUE, killing challenge & setting server script state to: GAME_STATE_END")
			serverBD.iServerState = GAME_STATE_END
			bHostEndMissionNow = FALSE
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_WIDGETS()
	INT iPlayer
	TEXT_LABEL_63 tl63

	SWITCH iWidgetState
	
		CASE 0
			wgHuntTheBossWidget = START_WIDGET_GROUP("GB - Hunt The Boss")
				ADD_WIDGET_BOOL("Create Widgets - Hunt The Boss", bCreateWidgets)
			STOP_WIDGET_GROUP()
			iWidgetState++
		BREAK
		
		CASE 1
			IF bCreateWidgets	
				CLEAR_CURRENT_WIDGET_GROUP(wgHuntTheBossWidget)				
				DELETE_WIDGET_GROUP(wgHuntTheBossWidget)
				wgHuntTheBossWidget = START_WIDGET_GROUP("GB Hunt The Boss") 
					ADD_WIDGET_BOOL("Skip intro countdown", bSkipIntro)
					ADD_WIDGET_BOOL("Show intro countdown", bShowIntro)
					ADD_WIDGET_BOOL("Reset Previous Location Data", bResetLocationData)

					// Gameplay debug, sever only.
					START_WIDGET_GROUP("Server Only Gameplay") 
						ADD_WIDGET_BOOL("End Mission: Debug end no reason (immediate)", bHostEndMissionNow)
						ADD_WIDGET_BOOL("End Mission: End Timer Hit", bExpireTimer)
					STOP_WIDGET_GROUP()
										
					// Data about the server
					START_WIDGET_GROUP("Server BD") 
						ADD_WIDGET_INT_SLIDER("S. Game state", serverBD.iServerState,-1, HIGHEST_INT,1)
					STOP_WIDGET_GROUP()	

					// Data about the clients. * = You.
					START_WIDGET_GROUP("Client BD")  				
						REPEAT GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS() iPlayer
							tl63 = "Player "
							tl63 += iPlayer
							IF iPlayer = PARTICIPANT_ID_TO_INT()
								tl63 += "*"
							ENDIF
							START_WIDGET_GROUP(tl63)
								ADD_WIDGET_INT_SLIDER("Game state", playerBD[iPlayer].iClientState,-1, HIGHEST_INT,1)
							STOP_WIDGET_GROUP()
						ENDREPEAT
					STOP_WIDGET_GROUP()
				STOP_WIDGET_GROUP()
				CPRINTLN(dbChan, "[GB][HTB] - DEBUG: MAINTAIN_WIDGETS - Created Widgets")
				iWidgetState++
			ENDIF
		BREAK
		
		CASE 2
			UPDATE_WIDGETS()
		BREAK
	
	ENDSWITCH
ENDPROC


PROC MAINTAIN_DEBUG()

	MAINTAIN_WIDGETS()
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_DebugEndMode)
				SET_BIT(serverBD.iServerBitSet, biS_DebugEndMode)
				CPRINTLN(dbChan, "[GB][HTB] - DEBUG: MAINTAIN_GENERAL - Force failing mode early as SERVER.")
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_DebugEndMode)
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_DebugEndMode)
				CPRINTLN(dbChan, "[GB][HTB] - DEBUG: MAINTAIN_GENERAL - Force failing mode early as CLIENT.")
			ENDIF
		ENDIF
	ENDIF
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_DebugEndMode)
			INT index
			REPEAT NUM_NETWORK_PLAYERS index
				IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(index), FALSE, FALSE)
					IF IS_BIT_SET(playerBD[index].iClientBitSet, biC_DebugEndMode)
						SET_BIT(serverBD.iServerBitSet, biS_DebugEndMode)
						CPRINTLN(dbChan, "[GB][HTB] - DEBUG: MAINTAIN_GENERAL - Ack force fail request from CLIENT, processing locally as SERVER.")
					ENDIF	
				ENDIF
			ENDREPEAT	
		ENDIF
	ENDIF
	
ENDPROC


/// PURPOSE:
///    Prints information about the big message that has just been requested to draw
/// PARAMS:
///    iBigMessage - the index of the message that was drawn
///    title - the title of the message
///    strapline - the subtitle of the message
PROC PRINT_BIG_MESSAGE_DEBUG_INFORMATION(INT iBigMessage, STRING title, STRING strapline)
	CPRINTLN(dbChan, "[GB][HTB] - PRINT_BIG_MESSAGE_DEBUG_INFORMATION - Big Message:	", iBigMessage)
	CPRINTLN(dbChan, "[GB][HTB] - PRINT_BIG_MESSAGE_DEBUG_INFORMATION - Title: 		", GET_FILENAME_FOR_AUDIO_CONVERSATION(title))
	CPRINTLN(dbChan, "[GB][HTB] - PRINT_BIG_MESSAGE_DEBUG_INFORMATION - Strapline:	", GET_FILENAME_FOR_AUDIO_CONVERSATION(strapline))
ENDPROC

FUNC STRING GET_NAME_OF_UI_LEVEL(GB_UI_LEVEL eUI)
	SWITCH eUI
		DEFAULT 					RETURN "ERROR! Unknown UI level."
		CASE GB_UI_LEVEL_NONE 		RETURN "GB_UI_LEVEL_NONE"
		CASE GB_UI_LEVEL_BACKGROUND RETURN "GB_UI_LEVEL_BACKGROUND"
		CASE GB_UI_LEVEL_FULL 		RETURN "GB_UI_LEVEL_FULL"
		CASE GB_UI_LEVEL_MINIMAL 	RETURN "GB_UI_LEVEL_MINIMAL"
	ENDSWITCH
ENDFUNC

#ENDIF

/// PURPOSE:
///    Wrapper for IS_BIT_SET, SET_BIT and CLEAR_BIT
/// RETURNS:
///    The BOOL value of the bit BEFORE changing it
FUNC BOOL mBit(INT& iBitSet, INT iBit, INT iNewVal = -1)
	//Return the value first
	BOOL bRet = IS_BIT_SET(iBitSet, iBit)
	
	//Set the bit if needed
	IF iNewVal = 0
		CLEAR_BIT(iBitSet, iBit)
	ELIF iNewVal = 1
		SET_BIT(iBitSet, iBit)
	ENDIF
	
	RETURN bRet
ENDFUNC


/// PURPOSE:
///    Return the server's script state.
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerState
ENDFUNC


/// PURPOSE:
///    Host Only - Set the server script state in serverBD.
PROC SET_SERVER_MISSION_STATE(INT iState)
	#IF IS_DEBUG_BUILD
		CPRINTLN(dbChan, "[GB][HTB] - SET_SERVER_MISSION_STATE = ",GET_MISSION_STATE_STRING(iState))
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	serverBD.iServerState = iState
ENDPROC


/// PURPOSE:
///    Return the server's mission stage.
FUNC GB_HTB_MISSION_STAGE GET_SERVER_MISSION_STAGE()
	RETURN serverBD.eStage
ENDFUNC


/// PURPOSE:
///    Host Only - Set the server's mission stage in serverBD.
PROC SET_SERVER_MISSION_STAGE(GB_HTB_MISSION_STAGE newStage)
	#IF IS_DEBUG_BUILD
		CPRINTLN(dbChan, "[GB][HTB] - SET_SERVER_MISSION_STAGE - Set server stage: ", GET_MISSION_STAGE_STRING(newStage))
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	serverBD.eStage = newStage
ENDPROC


/// PURPOSE:
///    Return the specified client's mission state.
/// PARAMS:
///    iPlayer - Client index to inspect.
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iClientState
ENDFUNC


/// PURPOSE:
///    Set the local player's script state.
PROC SET_CLIENT_MISSION_STATE(INT iState)
	#IF IS_DEBUG_BUILD
		CPRINTLN(dbChan, "[GB][HTB] - SET_CLIENT_MISSION_STATE = ",GET_MISSION_STATE_STRING(iState))
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	playerBD[PARTICIPANT_ID_TO_INT()].iClientState = iState
ENDPROC


/// PURPOSE:
///    Return the mission stage for the specified client.
FUNC GB_HTB_MISSION_STAGE GET_CLIENT_MISSION_STAGE(INT iPlayer)
	RETURN playerBD[iPlayer].eStage
ENDFUNC


/// PURPOSE:
///    Set local mission stage.
PROC SET_CLIENT_MISSION_STAGE(GB_HTB_MISSION_STAGE newStage)
	#IF IS_DEBUG_BUILD
		CPRINTLN(dbChan, "[GB][HTB] - SET_CLIENT_MISSION_STAGE - Setting client stage: ", GET_MISSION_STAGE_STRING(newStage))
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	playerBD[PARTICIPANT_ID_TO_INT()].eStage = newStage	
ENDPROC


/// PURPOSE:
///     Get the boss player Id from serverBD.
FUNC PLAYER_INDEX GET_GANG_BOSS_PLAYER_INDEX()
	RETURN serverBD.piGangBoss
ENDFUNC


/// PURPOSE:
///    Get the playerID for the winner from serverBD.
FUNC PLAYER_INDEX GET_WINNER()
	RETURN serverBD.piWinner
ENDFUNC


/// PURPOSE:
///    Check if the local player is considered the gang boss in serverBD.
FUNC BOOL AM_I_THE_GANG_BOSS()
	IF PLAYER_ID() = GET_GANG_BOSS_PLAYER_INDEX()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Check if the local player is considered the gang boss or a goon in serverBD.
/// RETURNS:
///    TRUE if local player is the gang boss or a goon.
FUNC BOOL AM_I_THE_GANG_BOSS_OR_GOON()
	IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PLAYER_ID(), GET_GANG_BOSS_PLAYER_INDEX(), TRUE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    If the boss was killed, check if the killer was a teammate.
///    For added fail reasoning, also means that the killer gets no reward.
FUNC BOOL HAS_THE_BOSS_BEEN_TEAMKILLED()
	
	IF ServerBD.piWinner = INVALID_PLAYER_INDEX()
	OR serverBD.piGangBoss = INVALID_PLAYER_INDEX()
		RETURN FALSE
	ENDIF
	
	IF NOT GB_IS_PLAYER_MEMBER_OF_THIS_GANG(ServerBD.piWinner, serverBD.piGangBoss, FALSE)
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC


/// PURPOSE:
///    Warpper to check the common GB checks for UI safety.
/// RETURNS:
///    TRUE = hide.
FUNC BOOL SHOULD_UI_BE_HIDDEN()
	IF GB_SHOULD_HIDE_GANG_BOSS_EVENT(FMMC_TYPE_GB_HUNT_THE_BOSS)
	OR GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
	OR NOT MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
	OR eUserInterfaceLevel = GB_UI_LEVEL_NONE
		
//		#IF IS_DEBUG_BUILD
//		IF GB_SHOULD_HIDE_GANG_BOSS_EVENT(FMMC_TYPE_GB_HUNT_THE_BOSS)
//			CPRINTLN(dbChan, "[GB][HTB] - SHOULD_UI_BE_HIDDEN - SPEW: GB_SHOULD_HIDE_GANG_BOSS_EVENT = TRUE")
//		ELIF GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
//			CPRINTLN(dbChan, "[GB][HTB] - SHOULD_UI_BE_HIDDEN - SPEW: GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE = TRUE")
//		ELIF NOT MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
//			CPRINTLN(dbChan, "[GB][HTB] - SHOULD_UI_BE_HIDDEN - SPEW: GB_IS_IT_SAFE_TO_DISPLAY_BOSS_MISSION_UI = FALSE")
//		ELIF eUserInterfaceLevel = GB_UI_LEVEL_NONE
//			CPRINTLN(dbChan, "[GB][HTB] - SHOULD_UI_BE_HIDDEN - SPEW: eUserInterfaceLevel = GB_UI_LEVEL_NONE")
//		ENDIF
//		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Loop through all script participant and pick out participants who have been
///    removed from the mode for being too far away / inactive.
/// RETURNS:
///    INT bitset representation of non-participant players on this script.
FUNC INT ALL_NON_ACTIVE_PARTICIPANTS()

	INT iPlayerFlags, index
	REPEAT NUM_NETWORK_PLAYERS index
		PLAYER_INDEX playerId = INT_TO_PLAYERINDEX(index)
		IF IS_NET_PLAYER_OK(playerId, FALSE, FALSE)
			IF NOT IS_BIT_SET(playerBD[index].iClientBitSet, biC_ActiveParticipant)
				SET_BIT(iPlayerFlags, NATIVE_TO_INT(PlayerID))
				CPRINTLN(dbChan, "[GB][HTB] - ALL_NON_ACTIVE_PARTICIPANTS - Found non-active participant: [",index,", ",GET_PLAYER_NAME(playerId),"]")
			ENDIF	
		ENDIF
	ENDREPEAT

	RETURN iPlayerFlags
ENDFUNC


/// PURPOSE:
///    Loop through all script participant and pick out participants who have been
///    marked as active in this mode (< 500m away)
/// RETURNS:
///    INT bitset representation of active participant players on this script.
FUNC INT ALL_ACTIVE_PARTICIPANTS(BOOL bIncludeBoss = FALSE)

	INT iPlayerFlags, index
	REPEAT NUM_NETWORK_PLAYERS index
		PLAYER_INDEX playerId = INT_TO_PLAYERINDEX(index)
		IF IS_NET_PLAYER_OK(playerId, FALSE, FALSE)
			IF bIncludeBoss
				IF IS_BIT_SET(playerBD[index].iClientBitSet, biC_ActiveParticipant)
					SET_BIT(iPlayerFlags, NATIVE_TO_INT(PlayerID))
					CPRINTLN(dbChan, "[GB][HTB] - ALL_NON_ACTIVE_PARTICIPANTS - Found active participant: [",index,", ",GET_PLAYER_NAME(playerId),"], bIncludeBoss: ", bIncludeBoss)
				ENDIF
			ELSE
				IF playerId != serverBD.piGangBoss
					IF IS_BIT_SET(playerBD[index].iClientBitSet, biC_ActiveParticipant)
						SET_BIT(iPlayerFlags, NATIVE_TO_INT(PlayerID))
						CPRINTLN(dbChan, "[GB][HTB] - ALL_NON_ACTIVE_PARTICIPANTS - Found active participant: [",index,", ",GET_PLAYER_NAME(playerId),"], bIncludeBoss: ", bIncludeBoss)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

	RETURN iPlayerFlags
ENDFUNC


/// PURPOSE:
///    Check if a player is considered an active participant of this mode. An active participant
///    is a player who is within the trigger area radius * 1.5.
/// PARAMS:
///    iParticipantID - ParticipantID of the player to check.
/// RETURNS:
///    TRUE if player is in range of the boss work.
FUNC BOOL IS_PLAYER_AN_ACTIVE_PARTICIPANT(INT iParticipantID)

	IF iParticipantID < 0
	OR iParticipantID > (NUM_NETWORK_PLAYERS-1)
		RETURN FALSE
	ENDIF

	IF NOT IS_BIT_SET(playerBD[iParticipantID].iClientBitSet, biC_ActiveParticipant)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC


/// PURPOSE:
///    Request a random start point for the given seed.
/// PARAMS:
///    iSeed - City/Countryside
/// RETURNS:
///    VECTOR - Random position picked from list of 10 or so.
FUNC VECTOR GET_GB_MODE_START_POSITION(INT iSeed)

	INT iNetworkSeed = GET_CLOUD_TIME_AS_INT()
	INT iRandCeiling, iRandNumber, iPrevLocation, iBitMarker, iMaxPositions, index
	
	SWITCH iSeed
		CASE ciHTB_POSITION_SEED_ONE 	
			iRandCeiling 	= (ciHTB_NUM_POSITIONS_ONE-1)
			iBitMarker		= biStat_One
			iMaxPositions 	= ciHTB_NUM_POSITIONS_ONE
		BREAK
		CASE ciHTB_POSITION_SEED_TWO 	
			iRandCeiling 	= (ciHTB_NUM_POSITIONS_TWO-1)
			iBitMarker 		= biStat_Two
			iMaxPositions 	= ciHTB_NUM_POSITIONS_TWO
		BREAK
		CASE ciHTB_POSITION_SEED_THREE 	
			iRandCeiling 	= (ciHTB_NUM_POSITIONS_THREE-1)
			iBitMarker 		= biStat_Three
			iMaxPositions 	= ciHTB_NUM_POSITIONS_THREE
		BREAK
		CASE ciHTB_POSITION_SEED_FOUR 	
			iRandCeiling 	= (ciHTB_NUM_POSITIONS_FOUR-1)
			iBitMarker 		= biStat_Four
			iMaxPositions 	= ciHTB_NUM_POSITIONS_FOUR
		BREAK
	ENDSWITCH
	
	NETWORK_SEED_RANDOM_NUMBER_GENERATOR(iNetworkSeed)
	iRandNumber = NETWORK_GET_RANDOM_INT_RANGED(0, iRandCeiling)
	CPRINTLN(dbChan, "[GB][HTB] - GET_GB_MODE_START_POSITION - Random generated: ",iRandNumber,", using iNetworkSeed: ", iNetworkSeed, ", Min: 0, Max: ", iRandCeiling)
	
	iPrevLocation = GET_MP_INT_CHARACTER_STAT(MP_STAT_GB_HTB_PREV_LOC)
	
	IF iMaxPositions > 1
		IF IS_BIT_SET(iPrevLocation, iBitMarker)
			CPRINTLN(dbChan, "[GB][HTB] - GET_GB_MODE_START_POSITION - Found marker in location tracker, BS value: ", iPrevLocation)
			FOR index = (iBitMarker+1) TO ((iBitMarker+1) + (iMaxPositions-1))
				IF IS_BIT_SET(iPrevLocation, index)
					CPRINTLN(dbChan, "[GB][HTB] - GET_GB_MODE_START_POSITION - Found previous location, BitSetPos:", index, ", actual: ", (index - (iBitMarker+1)))
					
					// Pick the next location instead of random again. This ensures that
					// the position is actually consistently different.
					IF ((index - (iBitMarker+1)) >= (iMaxPositions-1))
						iRandNumber = 0
					ELSE
						iRandNumber = ((index - (iBitMarker+1)) + 1)
					ENDIF
					
					CPRINTLN(dbChan, "[GB][HTB] - GET_GB_MODE_START_POSITION - New location: ", iRandNumber)
					
					// Escape the loop like a first year CS student.
					index = ((iBitMarker+1) + (iMaxPositions-1))
				ENDIF
			ENDFOR
		ENDIF
	ELSE
		CPRINTLN(dbChan, "[GB][HTB] - GET_GB_MODE_START_POSITION - Ignore previous data, only a single location is available.")
	ENDIF
		
	// Reset the bitset before saving for telemetry or stats as any old data is now 
	// invalid and will fuck up subsequent calculations.
	iPrevLocation = 0
	
	CPRINTLN(dbChan, "[GB][HTB] - GET_GB_MODE_START_POSITION - Saving new data, iBitMarker: ",iBitMarker,", BitIndex: ", ((iBitMarker+1) + iRandNumber))
	SET_BIT(iPrevLocation, ((iBitMarker+1) + iRandNumber))
	SET_BIT(iPrevLocation, iBitMarker)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_GB_HTB_PREV_LOC, iPrevLocation)

	ServerBD.iLocationCached = iPrevLocation
	CPRINTLN(dbChan, "[GB][HTB] - GET_GB_MODE_START_POSITION - Caching data for telemetry - LocationBS: ", iPrevLocation)
	
	SWITCH iSeed
		CASE ciHTB_POSITION_SEED_ONE // S-E
			SWITCH iRandNumber
				CASE 0 RETURN << -472.4150, -1689.4740, 17.9477 >>	
				CASE 1 RETURN << -1654.0920, 213.1430, 59.6413 >>	
				CASE 2 RETURN << -1008.3890, -1020.4960, 1.1503 >>	
				CASE 3 RETURN << -604.2660, -142.6840, 38.0086 >>	
				CASE 4 RETURN << -1700.5620, -322.7260, 49.0851 >>	
				CASE 5 RETURN << -1158.2800, -383.8760, 50.7161 >>	
				CASE 6 RETURN << -1191.0660, -1491.2590, 3.3797 >>
			ENDSWITCH
		BREAK
		CASE ciHTB_POSITION_SEED_TWO // S-W	
			SWITCH iRandNumber
				CASE 0 RETURN << 160.1420, -1761.7040, 28.0984 >>	
				CASE 1 RETURN << 1097.8409, -554.3190, 54.7365 >>	
				CASE 2 RETURN << 191.9320, 124.6530, 96.9401 >>	
				CASE 3 RETURN << 323.6930, -867.5270, 28.1726 >>	
				CASE 4 RETURN << 1386.7900, -1689.0020, 61.0964 >>	
				CASE 5 RETURN << 968.6230, -3067.4619, 4.9008 >>	
				CASE 6 RETURN << 928.8500, -2270.1560, 29.5096 >>
			ENDSWITCH
		BREAK
		CASE ciHTB_POSITION_SEED_THREE // N-E 
			SWITCH iRandNumber
				CASE 0 RETURN << 1667.8760, 3710.5271, 33.0008 >>	
				CASE 1 RETURN << 2459.9180, 4827.3911, 34.4214 >>	
				CASE 2 RETURN << 1506.7140, 6513.5640, 19.8804 >>	
				CASE 3 RETURN << 2501.0491, 2639.4829, 42.5508 >>	
				CASE 4 RETURN << 2658.2385, 1542.7285, 23.4847 >>	
				CASE 5 RETURN << 1415.4270, 1819.7260, 102.3258 >>
			ENDSWITCH
		BREAK
		CASE ciHTB_POSITION_SEED_FOUR // N-W
			SWITCH iRandNumber
				CASE 0 RETURN << -194.4760, 6316.8242, 30.5625 >>	
				CASE 1 RETURN << -660.5050, 5677.5620, 30.7898 >>	
				CASE 2 RETURN << -1557.1250, 4561.9639, 18.5454 >>	
				CASE 3 RETURN << -159.8030, 2713.0439, 54.3232 >>	
				CASE 4 RETURN << -3240.3850, 1106.3450, 1.5823 >>
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	// This should never get hit.
	RETURN << 0.0, 0.0, 0.0 >>
	
ENDFUNC


/// PURPOSE:
///    Performs general checks for it being safe to display the shard
/// RETURNS:
///    True if safe
FUNC BOOL IS_SAFE_FOR_BIG_MESSAGE()	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND NOT IS_PAUSE_MENU_ACTIVE()
	AND NOT IS_CUSTOM_MENU_ON_SCREEN()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE				
ENDFUNC

FUNC BOOL IS_THIS_HELP_MESSAGE_WITH_COLOURED_LITERAL_STRING_AND_COLOURED_STRING_BEING_DISPLAYED(STRING TextLabel, STRING SubStringLiteralString, HUD_COLOURS LiteralStringHudColour, STRING SubStringTextLabel, HUD_COLOURS Colour)
	BEGIN_TEXT_COMMAND_IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(TextLabel)
		IF NOT (LiteralStringHudColour = HUD_COLOUR_PURE_WHITE)
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(LiteralStringHudColour)
		ENDIF
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(SubStringLiteralString)
		IF NOT (Colour = HUD_COLOUR_PURE_WHITE)
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour)
		ENDIF
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(SubStringTextLabel)
	RETURN END_TEXT_COMMAND_IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(HELP_TEXT_SLOT_STANDARD)
ENDFUNC

FUNC BOOL IS_THIS_HELP_MESSAGE_WITH_COLOURED_STRING_BEING_DISPLAYED(STRING TextLabel, STRING SubStringTextLabel, HUD_COLOURS Colour)
	BEGIN_TEXT_COMMAND_IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(TextLabel)
		IF NOT (Colour = HUD_COLOUR_PURE_WHITE)
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour)
		ENDIF
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(SubStringTextLabel)
	RETURN END_TEXT_COMMAND_IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(HELP_TEXT_SLOT_STANDARD)
ENDFUNC
/// PURPOSE:
///    Wrapper for checking if there are reasons why help-text should not be cleared.
PROC CLEAR_ANY_HELP() // Add special exceptions here.
	IF IS_THIS_HELP_MESSAGE_WITH_LITERAL_STRING_BEING_DISPLAYED("GB_HTB_HR1", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(GET_GANG_BOSS_PLAYER_INDEX()), GB_GET_PLAYER_GANG_HUD_COLOUR(GET_GANG_BOSS_PLAYER_INDEX()))
	OR IS_THIS_HELP_MESSAGE_WITH_COLOURED_LITERAL_STRING_AND_COLOURED_STRING_BEING_DISPLAYED("GB_HTB_HR0", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(GET_GANG_BOSS_PLAYER_INDEX()), GB_GET_PLAYER_GANG_HUD_COLOUR(GET_GANG_BOSS_PLAYER_INDEX()), "GB_HTB_BLP", GB_GET_PLAYER_GANG_HUD_COLOUR(GET_GANG_BOSS_PLAYER_INDEX()))
	OR IS_THIS_HELP_MESSAGE_WITH_COLOURED_STRING_BEING_DISPLAYED("GB_HTB_HG0", "GB_HTB_BLP", GB_GET_PLAYER_GANG_HUD_COLOUR(GET_GANG_BOSS_PLAYER_INDEX()))
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GB_HTB_HELP2")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GB_HTB_HELP3")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GB_HTB_HELP4")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GB_HTB_HELP5")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GB_HTB_HELP7")
		CLEAR_HELP()
	ENDIF
ENDPROC


/// PURPOSE:
///    Overriden damage event handler. Handles the mode specific details (e.g. saving the killerID).
PROC PROCESS_GB_HTB_DAMAGE_EVENT(INT iCount)
 
 	PED_INDEX VictimPedID
	PED_INDEX AttackerPedID
	PLAYER_INDEX VictimPlayerID = INVALID_PLAYER_INDEX()
	PLAYER_INDEX AttackerPlayerID = INVALID_PLAYER_INDEX()
	STRUCT_ENTITY_DAMAGE_EVENT sEntityID
      
	GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sEntityID, SIZE_OF(sEntityID))
	
	CPRINTLN(dbChan, "[GB][HTB] - PROCESS_GB_HTB_DAMAGE_EVENT - Processing damage event, victim killed: ", sEntityID.VictimDestroyed)
	  
	// We have event data, now parse it into something useful.  
	IF DOES_ENTITY_EXIST(sEntityID.VictimIndex)
		IF IS_ENTITY_A_PED(sEntityID.VictimIndex)
			VictimPedID = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)
			IF IS_PED_A_PLAYER(VictimPedID)
				VictimPlayerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(VictimPedID)
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(VictimPlayerID)
					IF VictimPlayerID != INVALID_PLAYER_INDEX()
						IF IS_NET_PLAYER_OK(VictimPlayerID, FALSE, FALSE)
							CPRINTLN(dbChan, "[GB][HTB] - PROCESS_GB_HTB_DAMAGE_EVENT - Found victim: [",NATIVE_TO_INT(VictimPlayerID),", ",GET_PLAYER_NAME(VictimPlayerID),"]")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF VictimPlayerID = GET_GANG_BOSS_PLAYER_INDEX()
		CPRINTLN(dbChan, "[GB][HTB] - PROCESS_GB_HTB_DAMAGE_EVENT - Boss has been injured, find out by whom.")
		
		IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
			IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
				AttackerPedID = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
				IF IS_PED_A_PLAYER(AttackerPedID)
					AttackerPlayerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(AttackerPedID)
					IF NETWORK_IS_PLAYER_A_PARTICIPANT(AttackerPlayerID)
						IF IS_BIT_SET(PlayerBD[NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(AttackerPlayerID))].iClientBitSet, biC_ActiveParticipant)
							IF AttackerPlayerID != INVALID_PLAYER_INDEX()
								IF IS_NET_PLAYER_OK(AttackerPlayerID, FALSE, FALSE)
									CPRINTLN(dbChan, "[GB][HTB] - PROCESS_GB_HTB_DAMAGE_EVENT - Found attacker: [",NATIVE_TO_INT(AttackerPlayerID),", ",GET_PLAYER_NAME(AttackerPlayerID),"]")
								ENDIF
							ENDIF
						ELSE
							CPRINTLN(dbChan, "[GB][HTB] - PROCESS_GB_HTB_DAMAGE_EVENT - Attacking player is not an active participant, exit.")
							EXIT
						ENDIF
					ELSE
						CPRINTLN(dbChan, "[GB][HTB] - PROCESS_GB_HTB_DAMAGE_EVENT - Attacker is not a participant of the script, exit.")
						EXIT
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF AttackerPlayerID = PLAYER_ID()
			IF NOT GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PLAYER_ID(), GET_GANG_BOSS_PLAYER_INDEX(), TRUE)
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_QualifiedParticipant)
					GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
					CPRINTLN(dbChan, "[GB][HTB] - PROCESS_GB_HTB_DAMAGE_EVENT - Setting local client as a qualified participant.")
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_QualifiedParticipant)
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT sEntityID.VictimDestroyed
			CPRINTLN(dbChan, "[GB][HTB] - PROCESS_GB_HTB_DAMAGE_EVENT - This is NOT a fatal damage event, manage participation.")
			
			IF PLAYER_ID() = AttackerPlayerID
				IF NOT GB_IS_GLOBAL_CLIENT_BIT0_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_0_PERMANENT_PARTICIPANT)
					CPRINTLN(dbChan, "[GB][HTB] - PROCESS_GB_HTB_DAMAGE_EVENT - Setting myself as a permanent participant.")
					GB_SET_PLAYER_AS_PERMANENT_ON_BOSS_MISSION()
				ENDIF
			ENDIF
		ELSE
		
			IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
				EXIT
			ENDIF
		
			CPRINTLN(dbChan, "[GB][HTB] - PROCESS_GB_HTB_DAMAGE_EVENT(host) - This is a fatal damage event, process tracking logic.")
			
			IF AttackerPlayerID = VictimPlayerID
				CPRINTLN(dbChan, "[GB][HTB] - PROCESS_GB_HTB_DAMAGE_EVENT(host) - Killer and Victime are the same, suicides don't count.")
				EXIT
			ENDIF
			
			IF AttackerPlayerID != INVALID_PLAYER_INDEX()
				CPRINTLN(dbChan, "[GB][HTB] - PROCESS_GB_HTB_DAMAGE_EVENT(host) - Setting serverBD.piWinner to killer.")
				serverBD.piWinner = AttackerPlayerID
				SET_BIT(serverBD.iServerBitSet, biS_BossKilled)
				SET_SERVER_MISSION_STAGE(eGBHTB_END)
			ENDIF 
			
		ENDIF
		
	ELSE
		IF VictimPlayerID != INVALID_PLAYER_INDEX()
		AND GB_IS_PLAYER_MEMBER_OF_A_GANG(VictimPlayerID, FALSE)
			
			PLAYER_INDEX piTempBoss
			piTempBoss = GB_GET_THIS_PLAYER_GANG_BOSS(VictimPlayerID)
			
			IF piTempBoss = ServerBD.piGangBoss
			
				CPRINTLN(dbChan, "[GB][HTB] - PROCESS_GB_HTB_DAMAGE_EVENT(goon) - Defending org Goon has been injured.")
				
				IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
					IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
						AttackerPedID = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
						IF IS_PED_A_PLAYER(AttackerPedID)
							AttackerPlayerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(AttackerPedID)
							IF NETWORK_IS_PLAYER_A_PARTICIPANT(AttackerPlayerID)
								IF AttackerPlayerID != INVALID_PLAYER_INDEX()
									IF IS_NET_PLAYER_OK(AttackerPlayerID, FALSE, FALSE)
										CPRINTLN(dbChan, "[GB][HTB] - PROCESS_GB_HTB_DAMAGE_EVENT(goon) - Found attacker: [",NATIVE_TO_INT(AttackerPlayerID),", ",GET_PLAYER_NAME(AttackerPlayerID),"]")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF AttackerPlayerID = PLAYER_ID()
					IF NOT GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PLAYER_ID(), GET_GANG_BOSS_PLAYER_INDEX(), TRUE)
						IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_QualifiedParticipant)
							GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
							CPRINTLN(dbChan, "[GB][HTB] - PROCESS_GB_HTB_DAMAGE_EVENT(goon) - Setting local client as a qualified participant.")
							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_QualifiedParticipant)
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
			
		ENDIF
	ENDIF
		
ENDPROC


/// PURPOSE:
///    Show a message based on the passed in param. Message is shown once and not again.
///    Safe to call every frame.
/// PARAMS:
///    iBigMessage - Message ID  (see string table for details).
PROC DO_BIG_MESSAGE(INT iBigMessage)

	IF eUserInterfaceLevel != GB_UI_LEVEL_MINIMAL
	AND eUserInterfaceLevel != GB_UI_LEVEL_FULL
		CLEAR_ALL_BIG_MESSAGES()
		EXIT 
	ENDIF

	IF SHOULD_UI_BE_HIDDEN()
		CLEAR_ALL_BIG_MESSAGES()
		EXIT
	ENDIF

	SWITCH iBigMessage
		CASE 0		// HUNT THE BOSS - A wanted notice has been issued against a Boss in your session. Find them and deliver justice.
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_ANY_HELP()
					SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_START_OF_JOB, "GB_HTB_BMT0", "GB_HTB_BMS0", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(GET_GANG_BOSS_PLAYER_INDEX()), hudBossColour)
					SET_BIT(iBigMessageBitSet, iBigMessage)
					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_HTB_BMT0", "GB_HTB_BMS0") #ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 1		// HUNT THE BOSS - A wanted notice has been issued against your Boss, protect them at all costs!
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_ANY_HELP()
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_START_OF_JOB, "GB_HTB_BMT0", "GB_HTB_BMS1")
					SET_BIT(iBigMessageBitSet, iBigMessage)
					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_HTB_BMT0", "GB_HTB_BMS1") #ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 2		// HUNT THE BOSS - A wanted notice has been issued against you. Lie low in this area to survive the hunt.
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_ANY_HELP()
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_START_OF_JOB, "GB_HTB_BMT0", "GB_HTB_BMS2")
					SET_BIT(iBigMessageBitSet, iBigMessage)
					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_HTB_BMT0", "GB_HTB_BMS2") #ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 3		// WINNER - You killed the target Boss <C>~a~</C>~s~
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_ANY_HELP()
					SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, serverBD.piGangBoss, DEFAULT, "GB_HTB_BMS3", "GB_WINNER")
					SET_BIT(iBigMessageBitSet, iBigMessage)
					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_WINNER", "GB_HTB_BMS3") #ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 4		// BOSS WORK OVER - <C>~a~</C> ~s~ successfully hunted and killed the wanted Boss.
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_ANY_HELP()
					SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GB_END_OF_JOB_OVER, GET_WINNER(), DEFAULT, "GB_HTB_BMS4", "GB_WORK_OVER")
					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_WORK_OVER", "GB_HTB_BMS4") #ENDIF
					SET_BIT(iBigMessageBitSet, iBigMessage)
				ENDIF
			ENDIF
		BREAK
		CASE 5		// WINNER - The wanted Boss managed to escape the area. 
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_ANY_HELP()
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "GB_WINNER", "GB_HTB_BMS5")
					SET_BIT(iBigMessageBitSet, iBigMessage)
					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_WINNER", "GB_HTB_BMS5") #ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 6		// BOSS WORK OVER - The wanted Boss managed to escape the area. 
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_ANY_HELP()
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "GB_HTB_BMS5")
					SET_BIT(iBigMessageBitSet, iBigMessage)
					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_WORK_OVER", "GB_HTB_BMS5") #ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 7		// BOSS WORK OVER - The Boss has left.
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_ANY_HELP()
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "GB_HTB_BMS6")
					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_WORK_OVER", "GB_HTB_BMS6") #ENDIF
					SET_BIT(iBigMessageBitSet, iBigMessage)
				ENDIF
			ENDIF
		BREAK
		CASE 8		// BOSS WORK OVER - You were taken out by <C>~a~</C>~s~.
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_ANY_HELP()
					SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GB_END_OF_JOB_OVER, GET_WINNER(), DEFAULT, "GB_HTB_BMS7", "GB_WORK_OVER")
					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_WORK_OVER", "GB_HTB_BMS7") #ENDIF
					SET_BIT(iBigMessageBitSet, iBigMessage)
				ENDIF
			ENDIF
		BREAK
		CASE 9		// BOSS WORK OVER - You were taken out by fellow ~a~~s~ member <C>~a~</C>~s~.
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_ANY_HELP()
					SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION_WITH_PLAYERS(BIG_MESSAGE_GB_END_OF_JOB_OVER, 
																		"GB_WORK_OVER", "GB_HTB_BMS8", 
																		GB_GET_ORGANIZATION_NAME_AS_A_STRING(),
																		hudBossColour, GET_WINNER(), NULL, NULL)
					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_WORK_OVER", "GB_HTB_BMS8") #ENDIF
					SET_BIT(iBigMessageBitSet, iBigMessage)
				ENDIF
			ENDIF
		BREAK
		CASE 10		// WINNER - You successfully managed to survive the hunt.
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_ANY_HELP()
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "GB_WINNER", "GB_HTB_BMS9")
					SET_BIT(iBigMessageBitSet, iBigMessage)
					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_WINNER", "GB_HTB_BMS9") #ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 11		// WINNER - Your Boss successfully managed to survive the hunt.
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_ANY_HELP()
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "GB_WINNER", "GB_HTB_BMS10")
					SET_BIT(iBigMessageBitSet, iBigMessage)
					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_WINNER", "GB_HTB_BMS10") #ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 12		// BOSS WORK OVER - You left the area before the time was up.
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_ANY_HELP()
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "GB_HTB_BMS11")
					SET_BIT(iBigMessageBitSet, iBigMessage)
					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_WORK_OVER", "GB_HTB_BMS11") #ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 13		// BOSS WORK OVER - Your Boss left the area before the time was up.
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_ANY_HELP()
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "GB_HTB_BMS12")
					SET_BIT(iBigMessageBitSet, iBigMessage)
					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_WORK_OVER", "GB_HTB_BMS12") #ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 14		// BOSS WORK OVER - The Boss left the hideout area before the time was up.
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_ANY_HELP()
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "GB_HTB_BMS13")
					SET_BIT(iBigMessageBitSet, iBigMessage)
					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_WORK_OVER", "GB_HTB_BMS13") #ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 15		// BOSS WORK OVER - You killed your Boss.
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_ANY_HELP()
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "GB_HTB_BMS14")
					SET_BIT(iBigMessageBitSet, iBigMessage)
					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_WORK_OVER", "GB_HTB_BMS14") #ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 16		// BOSS WORK OVER - You failed to enter the hide-out zone in time.
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_ANY_HELP()
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "GB_HTB_BMS15")
					SET_BIT(iBigMessageBitSet, iBigMessage)
					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_WORK_OVER", "GB_HTB_BMS15") #ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 17		// BOSS WORK OVER - <C>~a~</C>~s~ killed their own Boss
			IF NOT IS_BIT_SET(iBigMessageBitSet, iBigMessage)
				IF IS_SAFE_FOR_BIG_MESSAGE()
					CLEAR_ANY_HELP()
					SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GB_END_OF_JOB_OVER, GET_WINNER(), DEFAULT, "GB_HTB_BMS16", "GB_WORK_OVER")
					#IF IS_DEBUG_BUILD PRINT_BIG_MESSAGE_DEBUG_INFORMATION(iBigMessage, "GB_WORK_OVER", "GB_HTB_BMS16") #ENDIF
					SET_BIT(iBigMessageBitSet, iBigMessage)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC


/// PURPOSE:
///    Check if it's safe to print a helptext message on screen this frame.
/// RETURNS:
///    TRUE when safe.
FUNC BOOL IS_SAFE_FOR_HELP(BOOL bIncludeOtherHelp = FALSE)
	IF  NOT IS_CUSTOM_MENU_ON_SCREEN()
	AND NOT IS_PAUSE_MENU_ACTIVE()
	AND NOT IS_RADAR_HIDDEN()
	AND IS_NET_PLAYER_OK(PLAYER_ID())
	AND NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
		IF bIncludeOtherHelp
		 	IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				RETURN TRUE
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


PROC PRINT_HELP_WITH_COLOURED_LITERAL_STRING_AND_COLOURED_STRING_NO_SOUND(STRING TextLabel, STRING SubStringLiteralString, HUD_COLOURS LiteralStringHudColour, STRING SubStringTextLabel, HUD_COLOURS Colour, INT iOverrideTime = -1)
	BEGIN_TEXT_COMMAND_DISPLAY_HELP(TextLabel)
		IF NOT (LiteralStringHudColour = HUD_COLOUR_PURE_WHITE)
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(LiteralStringHudColour)
		ENDIF
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(SubStringLiteralString)
		IF NOT (Colour = HUD_COLOUR_PURE_WHITE)
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour)
		ENDIF
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(SubStringTextLabel)
	END_TEXT_COMMAND_DISPLAY_HELP(HELP_TEXT_SLOT_STANDARD, FALSE, FALSE,iOverrideTime)
ENDPROC


/// PURPOSE:
///    Wrapper for all help-text instanced used throught this mode.
///    Safe to call every frame, messages only shown once (exception: 4 is shown multiple times).
/// PARAMS:
///    iHelp - Number indicating which helptext, see stringtable for details.
PROC DO_HELP_TEXT(INT iHelp)

	IF SHOULD_UI_BE_HIDDEN()
		CLEAR_ANY_HELP()
		EXIT
	ENDIF

	SWITCH iHelp
		CASE 0 // You have started Hunt the Boss. Enter the area ~a~~s~ and survive to earn cash, RP and JP rewards.
			IF NOT IS_BIT_SET(iHelpBitSet, iHelp)
				IF IS_SAFE_FOR_HELP()
					 PRINT_HELP_WITH_COLOURED_STRING_NO_SOUND("GB_HTB_HB0", "GB_HTB_BLP", GB_GET_PLAYER_GANG_HUD_COLOUR(GET_GANG_BOSS_PLAYER_INDEX()))
					GB_SET_GANG_BOSS_HELP_BACKGROUND()
					SET_BIT(iHelpBitSet, iHelp)
					CPRINTLN(dbChan, "[GB][HTB] - DO_HELP_TEXT - Help ", iHelp, " done: ", GET_FILENAME_FOR_AUDIO_CONVERSATION("GB_HTB_HB0"))
				ENDIF
			ENDIF
		BREAK
		CASE 1 // ~a~~s~ have started Hunt the Boss. Search the area ~a~~s~ and take out their Boss to earn cash, RP and JP rewards.
			IF NOT IS_BIT_SET(iHelpBitSet, iHelp)
				IF IS_SAFE_FOR_HELP()
					PRINT_HELP_WITH_COLOURED_LITERAL_STRING_AND_COLOURED_STRING_NO_SOUND("GB_HTB_HR0", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(GET_GANG_BOSS_PLAYER_INDEX()), GB_GET_PLAYER_GANG_HUD_COLOUR(GET_GANG_BOSS_PLAYER_INDEX()), "GB_HTB_BLP", GB_GET_PLAYER_GANG_HUD_COLOUR(GET_GANG_BOSS_PLAYER_INDEX()))
					GB_SET_GANG_BOSS_HELP_BACKGROUND()
					SET_BIT(iHelpBitSet, iHelp)
					CPRINTLN(dbChan, "[GB][HTB] - DO_HELP_TEXT - Help ", iHelp, " done: ", GET_FILENAME_FOR_AUDIO_CONVERSATION("GB_HTB_HR0"))
				ENDIF
			ENDIF
		BREAK
		CASE 2 // There is a wanted Boss on those loose somewhere in the countryside, head over to the indicated location on your map to begin the search.
			IF NOT IS_BIT_SET(iHelpBitSet, iHelp)
				IF IS_SAFE_FOR_HELP()
					PRINT_HELP_NO_SOUND("GB_HTB_HELP2")
					GB_SET_GANG_BOSS_HELP_BACKGROUND()
					SET_BIT(iHelpBitSet, iHelp)
					CPRINTLN(dbChan, "[GB][HTB] - DO_HELP_TEXT - Help ", iHelp, " done: ", GET_FILENAME_FOR_AUDIO_CONVERSATION("GB_HTB_HELP2"))
				ENDIF
			ENDIF
		BREAK
		CASE 3 // Your Boss is wanted for unspecified crimes, and is currently hiding out in the indicated area on your map. Do whatever you can to throw the hunters off the Boss's scent.
			IF NOT IS_BIT_SET(iHelpBitSet, iHelp)
				IF IS_SAFE_FOR_HELP()
					PRINT_HELP_NO_SOUND("GB_HTB_HELP3")
					GB_SET_GANG_BOSS_HELP_BACKGROUND()
					SET_BIT(iHelpBitSet, iHelp)
					CPRINTLN(dbChan, "[GB][HTB] - DO_HELP_TEXT - Help ", iHelp, " done: ", GET_FILENAME_FOR_AUDIO_CONVERSATION("GB_HTB_HELP3"))
				ENDIF
			ENDIF
		BREAK
		CASE 4 // You are now visible to all players, return to the hideout area immediately to go back into hiding. 
			IF IS_SAFE_FOR_HELP()
				PRINT_HELP_NO_SOUND("GB_HTB_HELP4")
				GB_SET_GANG_BOSS_HELP_BACKGROUND(FALSE)
				//CPRINTLN(dbChan, "[GB][HTB] - DO_HELP_TEXT - Help ", iHelp, " done: ", GET_FILENAME_FOR_AUDIO_CONVERSATION("GB_HTB_HELP4"))
			ENDIF
		BREAK
		CASE 5 // The hunt has begun, try to stay hidden until the time runs out!
			IF NOT IS_BIT_SET(iHelpBitSet, iHelp)
				IF IS_SAFE_FOR_HELP()
					PRINT_HELP_NO_SOUND("GB_HTB_HELP5")
					GB_SET_GANG_BOSS_HELP_BACKGROUND()
					SET_BIT(iHelpBitSet, iHelp)
					CPRINTLN(dbChan, "[GB][HTB] - DO_HELP_TEXT - Help ", iHelp, " done: ", GET_FILENAME_FOR_AUDIO_CONVERSATION("GB_HTB_HELP5"))
				ENDIF
			ENDIF
		BREAK
		CASE 6 // Your Boss has started Hunt the Boss. Enter the area ~a~~s~ and protect them to earn cash, RP and JP rewards.
			IF NOT IS_BIT_SET(iHelpBitSet, iHelp)
				IF IS_SAFE_FOR_HELP()
					PRINT_HELP_WITH_COLOURED_STRING_NO_SOUND("GB_HTB_HG0", "GB_HTB_BLP", GB_GET_PLAYER_GANG_HUD_COLOUR(GET_GANG_BOSS_PLAYER_INDEX()))
					GB_SET_GANG_BOSS_HELP_BACKGROUND()
					SET_BIT(iHelpBitSet, iHelp)
					CPRINTLN(dbChan, "[GB][HTB] - DO_HELP_TEXT - Help ", iHelp, " done: ", GET_FILENAME_FOR_AUDIO_CONVERSATION("GB_HTB_HG0"))
				ENDIF
			ENDIF
		BREAK
		CASE 7 // If you remain idle for too long, you will become visible to all hunters in the area.  	
			IF NOT IS_BIT_SET(iHelpBitSet, iHelp)
				IF IS_SAFE_FOR_HELP()
					PRINT_HELP_NO_SOUND("GB_HTB_HELP7")
					GB_SET_GANG_BOSS_HELP_BACKGROUND()
					SET_BIT(iHelpBitSet, iHelp)
					CPRINTLN(dbChan, "[GB][HTB] - DO_HELP_TEXT - Help ", iHelp, " done: ", GET_FILENAME_FOR_AUDIO_CONVERSATION("GB_HTB_HELP7"))
					BROADCAST_GENERAL_EVENT(GENERAL_EVENT_GB_HTB_PRINT_IDLE_HELP, ALL_ACTIVE_PARTICIPANTS())
				ENDIF
			ENDIF
		BREAK
		CASE 8 // ~a~~s~ have started Hunt the Boss. Search the area and take out their Boss to earn cash, RP and JP rewards.  	
			IF NOT IS_BIT_SET(iHelpBitSet, iHelp)
				IF IS_SAFE_FOR_HELP()
					PRINT_HELP_WITH_LITERAL_STRING_WITH_HUD_COLOUR_NO_SOUND("GB_HTB_HR1", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(GET_GANG_BOSS_PLAYER_INDEX()), GB_GET_PLAYER_GANG_HUD_COLOUR(GET_GANG_BOSS_PLAYER_INDEX()))
					GB_SET_GANG_BOSS_HELP_BACKGROUND()
					SET_BIT(iHelpBitSet, iHelp)
					CPRINTLN(dbChan, "[GB][HTB] - DO_HELP_TEXT - Help ", iHelp, " done: ", GET_FILENAME_FOR_AUDIO_CONVERSATION("GB_HTB_HR1"))
					BROADCAST_GENERAL_EVENT(GENERAL_EVENT_GB_HTB_PRINT_IDLE_HELP, ALL_ACTIVE_PARTICIPANTS())
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC


/// PURPOSE:
///    Cleanup mode music.
/// PARAMS:
///    bDisable - TRUE to disable.
PROC DISABLE_AMBIENT_MUSIC_FOR_CUSTOM_SCORE(BOOL bDisable)
	IF bDisable
		SET_AUDIO_FLAG("DisableFlightMusic", TRUE)
		SET_AUDIO_FLAG("WantedMusicDisabled", TRUE)
		mBit(iSoundBitset, bit_AmbientMusicDisabled, 1)
		CPRINTLN(dbChan2, "[GB][HTB] - DISABLE_AMBIENT_MUSIC_FOR_CUSTOM_SCORE Disabled music")
	ELSE
		SET_AUDIO_FLAG("DisableFlightMusic", FALSE)
		SET_AUDIO_FLAG("WantedMusicDisabled", FALSE)
		mBit(iSoundBitset, bit_AmbientMusicDisabled, 0)
		CPRINTLN(dbChan2, "[GB][HTB] - DISABLE_AMBIENT_MUSIC_FOR_CUSTOM_SCORE Enabled music")
	ENDIF
ENDPROC


/// PURPOSE:
///    Terminate the script thread, final cleanup.
PROC SCRIPT_CLEANUP()

	CPRINTLN(DEBUG_NET_AMBIENT, "=== HUNT THE BOSS === SCRIPT_CLEANUP")
	CPRINTLN(dbChan, "[GB][HTB] - SCRIPT_CLEANUP")
	DEBUG_PRINTCALLSTACK()
	
	IF AM_I_THE_GANG_BOSS()
		IF IS_MP_DECORATOR_BIT_SET(PLAYER_ID(), MP_DECORATOR_BS_INVALID_TO_SPECTATE)
			CLEAR_MP_DECORATOR_BIT(PLAYER_ID(), MP_DECORATOR_BS_INVALID_TO_SPECTATE)
			CPRINTLN(dbChan, "[GB][HTB] - SCRIPT_CLEANUP - Adding myself back on spectator list.")
		ENDIF
	ENDIF
	
	IF PARTICIPANT_ID_TO_INT() != -1
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_DoneTelemetry)
			CPRINTLN(dbChan2,"[GB][HTB] - SCRIPT_CLEANUP - Exiting script without having done telemetry, send basic fail reason. isBoss: ", AM_I_THE_GANG_BOSS())
			GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LEFT) 
		ENDIF
	ENDIF
	
	IF IS_PLAYER_BLOCK_FROM_PROPERTY_FLAG_SET(PPAF_GB_HUNT_THE_BOSS)
		CPRINTLN(dbChan2,"[GB][HTB] - SCRIPT_CLEANUP - Clearing property block flag: PPAF_GB_HUNT_THE_BOSS")
		CLEAR_PLAYER_BLOCK_FROM_PROPERTY_FLAG(PPAF_GB_HUNT_THE_BOSS)
	ENDIF
	
	//Make sure countdown timer is killed
	IF mBit(iSoundBitset, bit_EventCountdownActive)
		CPRINTLN(dbChan2,"[GB][HTB] - SCRIPT_CLEANUP Stopping the countdown timer audio in cleanup")
		STOP_SOUND(iSoundIDCountdown)
		iSoundIDCountdown = -1
		mBit(iSoundBitset, bit_EventCountdownActive, 0)
	ENDIF
	
	//Re-enable ambient music, if disabled
	IF mBit(iSoundBitset, bit_AmbientMusicDisabled)
		DISABLE_AMBIENT_MUSIC_FOR_CUSTOM_SCORE(FALSE)
	ENDIF
	
	g_sGb_Telemetry_data.sdata.m_match1 = serverBD.iMatchId1
	g_sGb_Telemetry_data.sdata.m_match2 = serverBD.iMatchId2
	PROCESS_CURRENT_BOSS_WORK_PLAYSTATS(ServerBD.iLocationCached)
	SET_ALL_SHOP_LOCATES_ARE_BLOCKED(FALSE)
	GB_COMMON_BOSS_MISSION_CLEANUP(FALSE)
	TERMINATE_THIS_THREAD()
	
ENDPROC


/// PURPOSE:
///    Wrapper for the wanted level cleanup to prevent hitting it every frame during the cleanup
///    phase of the mission script.
/// PARAMS:
///    piWinner - Winning player (in hostBD)
PROC GB_HTB_MANAGE_WANTED_CLEAN_UP(PLAYER_INDEX piWinner = NULL)

	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_DoneWantedCleanup)
		IF (piWinner != INVALID_PLAYER_INDEX())
			CPRINTLN(dbChan, "[GB][HTB] - GB_HTB_MANAGE_WANTED_CLEAN_UP - piWinner is valid player: ", GET_PLAYER_NAME(piWinner))
			GANG_BOSS_MANAGE_WANTED_CLEAN_UP(FALSE, piWinner)
		ELSE
			CPRINTLN(dbChan, "[GB][HTB] - GB_HTB_MANAGE_WANTED_CLEAN_UP - piWinner is invalid.")
			GANG_BOSS_MANAGE_WANTED_CLEAN_UP(FALSE, INVALID_PLAYER_INDEX())
		ENDIF
		
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_DoneWantedCleanup)
	ENDIF

ENDPROC


/// PURPOSE:
///    Monitor the server state and ensure that the client state is kept in sync.
PROC MAINTAIN_CLIENT_SERVER_STATE_SYNC()

	SWITCH GET_SERVER_MISSION_STAGE()
		CASE eGBHTB_PLAYING
			IF GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT()) != eGBHTB_PLAYING
				GB_SET_LOCAL_PLAYER_CRITICAL_TO_JOB(AM_I_THE_GANG_BOSS_OR_GOON())
				SET_CLIENT_MISSION_STAGE(eGBHTB_PLAYING)
			ENDIF
		BREAK
		CASE eGBHTB_END
			IF GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT()) != eGBHTB_END
				GB_SET_LOCAL_PLAYER_CRITICAL_TO_JOB(AM_I_THE_GANG_BOSS_OR_GOON())
				SET_CLIENT_MISSION_STAGE(eGBHTB_END)
			ENDIF
		BREAK
		CASE eGBHTB_CLEANUP
			IF GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT()) != eGBHTB_CLEANUP
				GB_SET_LOCAL_PLAYER_CRITICAL_TO_JOB(AM_I_THE_GANG_BOSS_OR_GOON())
				SET_CLIENT_MISSION_STAGE(eGBHTB_CLEANUP)
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC


/// PURPOSE:
///    Wrapper to remove all mode blips.
PROC REMOVE_BLIPS()

	IF DOES_BLIP_EXIST(biTriggerArea)
		REMOVE_BLIP(biTriggerArea)
		CPRINTLN(dbChan, "[GB][HTB] - REMOVE_BLIPS - Removed trigger area blip.")
	ENDIF
	
	IF DOES_BLIP_EXIST(biTriggerStart)
		REMOVE_BLIP(biTriggerStart)
		CPRINTLN(dbChan, "[GB][HTB] - REMOVE_BLIPS - Removed trigger coord blip.")
	ENDIF
	
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_ForcedCustomBossBlip)
		SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(ServerBD.piGangBoss, RADAR_TRACE_TEMP_4, FALSE)
		SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(ServerBD.piGangBoss, GET_BLIP_COLOUR_FROM_HUD_COLOUR(hudBossColour), FALSE)
		FORCE_BLIP_PLAYER(ServerBD.piGangBoss, FALSE, FALSE)
		CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_ForcedCustomBossBlip)
		CPRINTLN(dbChan, "[GB][HTB] - REMOVE_BLIPS - Removed custom boss blip sprite.")
	ENDIF

ENDPROC


/// PURPOSE:
///    When the Boss is stationary for a period of time, blip them. When the
///    Boss player starts moving again, remove said blip when the Boss has gone
///    a certain distance away from the idle point.
PROC MAINTAIN_BOSS_IDLE_BLIP()

	IF NOT AM_I_THE_GANG_BOSS()
		EXIT
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(serverBD.OutOfRangeTimer)
	AND HAS_NET_TIMER_STARTED(serverBD.OutOfRangeBlipTimer)		
		IF HAS_NET_TIMER_STARTED(IdleBlipTrackTimer)
			RESET_NET_TIMER(IdleBlipTrackTimer)
		ENDIF
		
		EXIT
	ENDIF
		
	IF NOT HAS_NET_TIMER_STARTED(IdleBlipHeartbeatTimer)
	AND NOT HAS_NET_TIMER_STARTED(IdleBlipTrackTimer)
	    vCachedBossPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
	    START_NET_TIMER(IdleBlipHeartbeatTimer)
		iIdleBlipHeartbeatTracker = 0
	ELSE
		
		IF HAS_NET_TIMER_STARTED(IdleBlipTrackTimer)
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_BossBlipHidden)
				HIDE_MY_PLAYER_BLIP_FROM_NON_GANG_MEMBERS(FALSE)
				CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_BOSS_IDLE_BLIP - Boss Blip: Show to all non-gang session members (IdleBlipTrackTimer).")
				CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_BossBlipHidden)
			ENDIF
			
			IF HAS_NET_TIMER_EXPIRED(IdleBlipTrackTimer, ROUND(g_sMPTunables.fgb_huntboss_boss_blipped_time))
			
				RESET_NET_TIMER(IdleBlipTrackTimer)
				CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_CurrentlyIdle)
				FLASH_MY_PLAYER_ARROW_RED(FALSE)
				CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_BOSS_IDLE_BLIP - Boss has completed mandatory blip time, remove their blip again.")
			
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_BossBlipHidden)
					HIDE_MY_PLAYER_BLIP_FROM_NON_GANG_MEMBERS(TRUE)
					CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_BOSS_IDLE_BLIP - Boss Blip: Hide from to all non-gang session members (IdleBlipTrackTimer).")
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_BossBlipHidden)
				ENDIF
			
			ENDIF
			
		ELSE
		
			IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(IdleBlipHeartbeatTimer) > (ciHTB_IDLE_BLIP_HEARTBEAT_INTERVAL * iIdleBlipHeartbeatTracker)
				
				//Check every 500ms if the player is out of camping range, if so reset and cache a new position
		        IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vCachedBossPosition) > g_sMPTunables.igb_huntboss_blippable_radius
		            
					RESET_NET_TIMER(IdleBlipHeartbeatTimer)
					
					IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_CurrentlyIdle)
						CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_BOSS_IDLE_BLIP - Clearing Boss bit biC_CurrentlyIdle and starting blip timer.")
						START_NET_TIMER(IdleBlipTrackTimer)
					ENDIF
					
				//Else if the Boss is still in the radius, and over the grace period, blip them.
	            ELIF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(IdleBlipHeartbeatTimer) > g_sMPTunables.fgb_huntboss_time_in_radius_before_blip
	            
					IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_BossBlipHidden)
						HIDE_MY_PLAYER_BLIP_FROM_NON_GANG_MEMBERS(FALSE)
						CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_BOSS_IDLE_BLIP - Boss Blip: Show to all non-gang session members.")
						CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_BossBlipHidden)
						DO_HELP_TEXT(7)
						FLASH_MY_PLAYER_ARROW_RED(TRUE, -1, HIGHEST_INT, 0)
					ENDIF
					
					IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_CurrentlyIdle)
						CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_BOSS_IDLE_BLIP - Setting Boss bit biC_CurrentlyIdle")
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_CurrentlyIdle)
					ENDIF
					
	            ENDIF
			
				iIdleBlipHeartbeatTracker++
			
			ENDIF
		
		ENDIF

	ENDIF
ENDPROC


FUNC BOOL IS_BOSS_IN_MISSION_AREA(INT radius, FLOAT &fReturnedDist, FLOAT groundHeight = 120.0, BOOL bIgnoreZ = FALSE)
	
	PED_INDEX boss = GET_PLAYER_PED(GET_GANG_BOSS_PLAYER_INDEX())
	
	IF NOT DOES_ENTITY_EXIST(boss)
		RETURN FALSE
	ENDIF
	
	fReturnedDist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(boss, serverBD.vTriggerPosition, FALSE)
	IF fReturnedDist > radius
		RETURN FALSE
	ENDIF
	
	FLOAT fHeight = GET_ENTITY_HEIGHT_ABOVE_GROUND(boss)
	fReturnedDist = FMAX(fReturnedDist, fHeight)
	
	IF fHeight > groundHeight AND NOT bIgnoreZ
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC


/// PURPOSE:
///    Check if the passed in player is within the mission area.
/// RETURNS:
///    TRUE if within the valid cuboid area.
FUNC BOOL IS_PLAYER_IN_MISSION_AREA(PLAYER_INDEX piSourcePlayer, INT radius, FLOAT &fReturnedDist, FLOAT groundHeight = 120.0, BOOL bIgnoreZ = FALSE)
	
	PED_INDEX pedPlayer = GET_PLAYER_PED(piSourcePlayer)
	
	IF NOT DOES_ENTITY_EXIST(pedPlayer)
		RETURN FALSE
	ENDIF
	
	fReturnedDist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(pedPlayer, serverBD.vTriggerPosition, FALSE)
	IF fReturnedDist > radius
		RETURN FALSE
	ENDIF
	
	FLOAT fHeight = GET_ENTITY_HEIGHT_ABOVE_GROUND(pedPlayer)
	fReturnedDist = FMAX(fReturnedDist, fHeight)
	
	IF fHeight > groundHeight AND NOT bIgnoreZ
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC



/// PURPOSE:
///    Check if the passed in player is within the mission area with a buffer between active and inactive ranges
/// RETURNS:
///    TRUE if within the valid cuboid area.
FUNC BOOL IS_PLAYER_IN_MISSION_AREA_BUFFERED(PLAYER_INDEX piSourcePlayer, INT radius, FLOAT &fReturnedDist, BOOL bWasActiveParticipant, FLOAT groundHeight = 120.0, BOOL bIgnoreZ = FALSE)
	
	PED_INDEX pedPlayer = GET_PLAYER_PED(piSourcePlayer)
	
	FLOAT fBuffer = PICK_FLOAT(bWasActiveParticipant, 30, 0)
	FLOAT fMult = 4
	
	IF NOT DOES_ENTITY_EXIST(pedPlayer)
		RETURN FALSE
	ENDIF
	
	fReturnedDist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(pedPlayer, serverBD.vTriggerPosition, FALSE)
	IF fReturnedDist > radius + fBuffer * fMult
		RETURN FALSE
	ENDIF
	
	FLOAT fHeight = GET_ENTITY_HEIGHT_ABOVE_GROUND(pedPlayer)
	fReturnedDist = FMAX(fReturnedDist, fHeight)
	
	IF fHeight > (groundHeight + fBuffer) AND NOT bIgnoreZ
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC



/// PURPOSE:
///    Return the alpha for the main area blip based on the passed in Boss's distance from the centre.
FUNC INT GET_AREA_BLIP_ALPHA(PLAYER_INDEX piGangBoss)

	FLOAT fDistance

	IF piGangBoss != INVALID_PLAYER_INDEX()
		IF NOT IS_VECTOR_ZERO(serverBD.vTriggerPosition)
			IF IS_PLAYER_IN_MISSION_AREA(piGangBoss, g_sMPTunables.igb_huntboss_area_radius, fDistance)
				RETURN ciHTB_BLIP_ALPHA_INSIDE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN g_sMPTunables.iblip_area_alpha

ENDFUNC

PROC DO_BOSS_BLIP_SHOWN_SOUND()

	IF IS_BIT_SET(playerBD[NATIVE_TO_INT(GET_GANG_BOSS_PLAYER_INDEX())].iClientBitSet, biC_CurrentlyIdle)
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,biC_PlayedBlipSound)
			IF GET_GANG_BOSS_PLAYER_INDEX() != GB_GET_LOCAL_PLAYER_GANG_BOSS()
				PLAY_SOUND_FRONTEND(-1,"Boss_Blipped","GTAO_Magnate_Hunt_Boss_SoundSet",FALSE)
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,biC_PlayedBlipSound)
				CPRINTLN(dbChan, "[GB][HTB] - DO_BOSS_BLIP_SHOWN_SOUND - Doing sound Boss Blipped.")
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,biC_PlayedBlipSound)
			CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,biC_PlayedBlipSound)
			CPRINTLN(dbChan, "[GB][HTB] - DO_BOSS_BLIP_SHOWN_SOUND - CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,biC_PlayedBlipSound)")
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Create and destroy blips. Wrapper for all blip control for the mode.
PROC MAINTAIN_BLIPS()

	IF SHOULD_UI_BE_HIDDEN()
		REMOVE_BLIPS()
		EXIT
	ENDIF
	
	FLOAT fDistance
	
	SWITCH playerBD[PARTICIPANT_ID_TO_INT()].eStage
	
		CASE eGBHTB_INIT

			IF NOT IS_VECTOR_ZERO(serverBD.vTriggerPosition)
				IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PLAYER_ID(), GET_GANG_BOSS_PLAYER_INDEX(), TRUE)
				
					IF IS_PLAYER_IN_MISSION_AREA(PLAYER_ID(), g_sMPTunables.igb_huntboss_area_radius, fDistance)
						IF DOES_BLIP_EXIST(biTriggerStart)
							REMOVE_BLIP(biTriggerStart)
							CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_BLIPS - eGBHTB_INIT - Remove hunt coord blip, distance < ",g_sMPTunables.igb_huntboss_area_radius,", value: ", fDistance)
						ENDIF
					ELSE
						IF NOT DOES_BLIP_EXIST(biTriggerStart)
							biTriggerStart = ADD_BLIP_FOR_COORD(serverBD.vTriggerPosition)
							SHOW_HEIGHT_ON_BLIP(biTriggerStart, TRUE)
							SET_BLIP_NAME_FROM_TEXT_FILE(biTriggerStart, "GB_HTB_BLIP1")
							SET_BLIP_FLASHES(biTriggerStart, TRUE)
							SET_BLIP_FLASH_TIMER(biTriggerStart, 7000)
							SET_BLIP_SPRITE(biTriggerStart, RADAR_TRACE_HUNT_THE_BOSS)
							SET_BLIP_PRIORITY(biTriggerStart, BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
							SET_BLIP_SCALE(biTriggerStart, g_sMPTunables.fgangboss_Job_blip_scale)
							SET_BLIP_COLOUR_FROM_HUD_COLOUR(biTriggerStart, hudBossColour)
							CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_BLIPS - eGBHTB_INIT - Added hunt coord blip at: ", serverBD.vTriggerPosition)
						ENDIF
					ENDIF
				
					IF NOT DOES_BLIP_EXIST(biTriggerArea)
						biTriggerArea = ADD_BLIP_FOR_RADIUS(serverBD.vTriggerPosition, TO_FLOAT(g_sMPTunables.igb_huntboss_area_radius-1))
						SET_BLIP_COLOUR_FROM_HUD_COLOUR(biTriggerArea, hudBossColour)
						SHOW_HEIGHT_ON_BLIP(biTriggerArea, TRUE)
						SET_BLIP_EXTENDED_HEIGHT_THRESHOLD(biTriggerArea, TRUE)
						SET_BLIP_NAME_FROM_TEXT_FILE(biTriggerArea, "GB_HTB_BLIP0")
						CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_BLIPS - eGBHTB_INIT - Added trigger/prep area blip at: ", serverBD.vTriggerPosition)
					ENDIF
					
					IF (GET_AREA_BLIP_ALPHA(serverBD.piGangBoss) != iBlipAlphaCached)
						CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_BLIPS - eGBHTB_INIT - Updating blip alpha from: ",iBlipAlphaCached,", to: ",GET_AREA_BLIP_ALPHA(serverBD.piGangBoss))
						iBlipAlphaCached = GET_AREA_BLIP_ALPHA(serverBD.piGangBoss)
						SET_BLIP_ALPHA(biTriggerArea, iBlipAlphaCached)
					ENDIF
					
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_BLIPS - eGBHTB_INIT - ERROR! Cannot add blip, trigger area is empty!")
			#ENDIF
			ENDIF
			
		BREAK 
		
		CASE eGBHTB_PLAYING
		
			IF AM_I_THE_GANG_BOSS()
			
				MAINTAIN_BOSS_IDLE_BLIP()
			
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_CurrentlyIdle)
					IF NOT HAS_NET_TIMER_STARTED(serverBD.OutOfRangeTimer)
					AND NOT HAS_NET_TIMER_STARTED(serverBD.OutOfRangeBlipTimer)
						IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_BossBlipHidden)
							HIDE_MY_PLAYER_BLIP_FROM_NON_GANG_MEMBERS(TRUE)
							CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_BLIPS - eGBHTB_PLAYING - Boss Blip: Hide from all non-gang session members.")
							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_BossBlipHidden)
						ENDIF
					ELSE
						IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_BossBlipHidden)
							HIDE_MY_PLAYER_BLIP_FROM_NON_GANG_MEMBERS(FALSE)
							CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_BLIPS - eGBHTB_PLAYING - Boss Blip: Show to all non-gang session members.")
							CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_BossBlipHidden)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				DO_BOSS_BLIP_SHOWN_SOUND()
				
				IF NOT GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PLAYER_ID(), GET_GANG_BOSS_PLAYER_INDEX(), FALSE)
					
					IF bBossNetStateOK
						// Make sure that the network data sync has occured, and that the boss player is actually in playing mode.
						// This is to prevent the bit not being set for 1/4 of a second, and a phantom blip appearing on network clients.
						IF NETWORK_IS_PLAYER_A_PARTICIPANT(ServerBD.piGangBoss)
						AND playerBD[NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(ServerBD.piGangBoss))].eStage = eGBHTB_PLAYING
						AND NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(ServerBD.piGangBoss))].iClientBitSet, biC_BossBlipHidden)
							
							IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_ForcedCustomBossBlip)
								CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_BLIPS - eGBHTB_PLAYING - Target boss blip is visible, force target boss blip to use custom sprite.")
								SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(ServerBD.piGangBoss, RADAR_TRACE_TEMP_4, TRUE)
								SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(ServerBD.piGangBoss, GET_BLIP_COLOUR_FROM_HUD_COLOUR(hudBossColour), TRUE)
								
								// Dirty fix for url:bugstar:2640507.
								IF IS_NET_PLAYER_OK(GET_GANG_BOSS_PLAYER_INDEX())
									FORCE_BLIP_PLAYER(ServerBD.piGangBoss, TRUE, FALSE)
								ENDIF
								
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_ForcedCustomBossBlip)
							ENDIF
						ELSE
							IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_ForcedCustomBossBlip)
								CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_BLIPS - eGBHTB_PLAYING - Target boss blip is NOT visible, remove custom sprite.")
								SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(ServerBD.piGangBoss, RADAR_TRACE_TEMP_4, FALSE)
								SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(ServerBD.piGangBoss, GET_BLIP_COLOUR_FROM_HUD_COLOUR(hudBossColour), FALSE)
								
								// Dirty fix for url:bugstar:2640507.
								IF IS_NET_PLAYER_OK(GET_GANG_BOSS_PLAYER_INDEX())
									FORCE_BLIP_PLAYER(ServerBD.piGangBoss, FALSE, FALSE)
								ENDIF
								
								CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_ForcedCustomBossBlip)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
		
			IF NOT IS_VECTOR_ZERO(serverBD.vTriggerPosition)
			
				IF IS_PLAYER_IN_MISSION_AREA(PLAYER_ID(), g_sMPTunables.igb_huntboss_area_radius, fDistance)
					IF DOES_BLIP_EXIST(biTriggerStart)
						REMOVE_BLIP(biTriggerStart)
						CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_BLIPS - eGBHTB_PLAYING - Remove hunt coord blip, distance < ",g_sMPTunables.igb_huntboss_area_radius,", value: ", fDistance)
					ENDIF
				ELSE
					IF NOT DOES_BLIP_EXIST(biTriggerStart)
						biTriggerStart = ADD_BLIP_FOR_COORD(serverBD.vTriggerPosition)
						SHOW_HEIGHT_ON_BLIP(biTriggerStart, TRUE)
						SET_BLIP_NAME_FROM_TEXT_FILE(biTriggerStart, "GB_HTB_BLIP1")
						SET_BLIP_FLASHES(biTriggerStart, TRUE)
						SET_BLIP_FLASH_TIMER(biTriggerStart, 7000)
						SET_BLIP_SPRITE(biTriggerStart, RADAR_TRACE_HUNT_THE_BOSS)
						SET_BLIP_PRIORITY(biTriggerStart, BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
						SET_BLIP_SCALE(biTriggerStart, g_sMPTunables.fgangboss_Job_blip_scale)
						SET_BLIP_COLOUR_FROM_HUD_COLOUR(biTriggerStart, hudBossColour)
						CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_BLIPS - eGBHTB_PLAYING - Added hunt coord blip at: ", serverBD.vTriggerPosition)
					ENDIF
				ENDIF
			
				IF NOT DOES_BLIP_EXIST(biTriggerArea)
					biTriggerArea = ADD_BLIP_FOR_RADIUS(serverBD.vTriggerPosition, TO_FLOAT(g_sMPTunables.igb_huntboss_area_radius-1))
					SET_BLIP_COLOUR_FROM_HUD_COLOUR(biTriggerArea, hudBossColour)
					SHOW_HEIGHT_ON_BLIP(biTriggerArea, TRUE)
					SET_BLIP_EXTENDED_HEIGHT_THRESHOLD(biTriggerArea, TRUE)
					SET_BLIP_NAME_FROM_TEXT_FILE(biTriggerArea, "GB_HTB_BLIP0")
					SET_BLIP_ALPHA(biTriggerArea, g_sMPTunables.iblip_area_alpha)
					CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_BLIPS - eGBHTB_PLAYING - Added hunt area blip at: ", serverBD.vTriggerPosition)
				ENDIF
				
				IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PLAYER_ID(), GET_GANG_BOSS_PLAYER_INDEX(), TRUE)
				OR GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), TRUE)
				
					PLAYER_INDEX piTempBoss
				
					IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), TRUE)
						piTempBoss = GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID())
					ELSE
						piTempBoss = serverBD.piGangBoss
					ENDIF
					
					IF (GET_AREA_BLIP_ALPHA(piTempBoss) != iBlipAlphaCached)
						CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_BLIPS - eGBHTB_PLAYING - Updating blip alpha from: ",iBlipAlphaCached,", to: ",GET_AREA_BLIP_ALPHA(piTempBoss))
						iBlipAlphaCached = GET_AREA_BLIP_ALPHA(piTempBoss)
						SET_BLIP_ALPHA(biTriggerArea, iBlipAlphaCached)
					ENDIF
				ENDIF
				
			#IF IS_DEBUG_BUILD
			ELSE
				CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_BLIPS - eGBHTB_PLAYING - ERROR! Cannot add blip, trigger area is empty!")
			#ENDIF
			ENDIF

		BREAK 
		
		CASE eGBHTB_END
		CASE eGBHTB_CLEANUP
		
			REMOVE_BLIPS()
			
		BREAK 

	ENDSWITCH

ENDPROC


/// PURPOSE:
///    Every frame check for damage events that might have occured since the previous frame.
PROC MAINTAIN_EVENTS()
	
	SWITCH GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT())

		CASE eGBHTB_PLAYING
	
			INT iCount
			EVENT_NAMES ThisScriptEvent
			
			REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount
		      
				ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)
		            
				IF ThisScriptEvent = EVENT_NETWORK_DAMAGE_ENTITY
					PROCESS_GB_HTB_DAMAGE_EVENT(iCount)
				ENDIF
		      
			ENDREPEAT
			
		BREAK
		
	ENDSWITCH
			
ENDPROC


/// PURPOSE:
///    Maintain the small UI element bottom right that contains the timer (and score if required).
PROC MAINTAIN_BOTTOM_RIGHT_UI()
	
	IF SHOULD_UI_BE_HIDDEN()
		//CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_BOTTOM_RIGHT_UI - SPEW: SHOULD_UI_BE_HIDDEN = TRUE")
		EXIT
	ENDIF
	
	// Only show timer UI for full.
	IF eUserInterfaceLevel != GB_UI_LEVEL_FULL
		//CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_BOTTOM_RIGHT_UI - SPEW: eUserInterfaceLevel != GB_UI_LEVEL_FULL")
		EXIT
	ENDIF
	
	INT iTime
	
	SWITCH GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT())
	
		CASE eGBHTB_INIT
		
			#IF IS_DEBUG_BUILD
			IF bShowIntro
				IF HAS_NET_TIMER_STARTED(serverBD.ModeStartTimer)
					iTime = g_sMPTunables.igb_huntboss_area_expiry_time - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.ModeStartTimer)
					IF iTime > 0
						DRAW_GENERIC_TIMER(iTime, "GB_WORK_START", DEFAULT)
					ELSE
						DRAW_GENERIC_TIMER(0, "GB_WORK_START", DEFAULT)
					ENDIF
				ENDIF
			ENDIF
			#ENDIF
		BREAK 
		
		CASE eGBHTB_PLAYING
		
			IF AM_I_THE_GANG_BOSS()
				// The out of range timer is higher display priority than the main game timer, however
				// if the player leaves the area, the main duration timer continues counting down.
				IF HAS_NET_TIMER_STARTED(serverBD.OutOfRangeTimer)
					iTime = (TEN_SECONDS * 2) - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.OutOfRangeTimer)
					IF iTime > 0
//						CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_BOTTOM_RIGHT_UI - SPEW: Calling DRAW_GENERIC_TIMER on BOSS.")
						DRAW_GENERIC_TIMER(iTime, "GB_HTB_TM2", DEFAULT, DEFAULT, (TEN_SECONDS * 2), DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_RED)
					ELSE
//						CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_BOTTOM_RIGHT_UI - SPEW: Calling DRAW_GENERIC_TIMER(0) on BOSS.")
						DRAW_GENERIC_TIMER(0, "GB_HTB_TM2", DEFAULT, DEFAULT, 0, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_RED)
					ENDIF
					
					EXIT
				ENDIF
			ENDIF
		
			IF HAS_NET_TIMER_STARTED(serverBD.ModeDurationTimer)
				iTime = g_sMPTunables.igb_huntboss_time_limit - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.ModeDurationTimer)
				IF iTime > 0
//					CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_BOTTOM_RIGHT_UI - SPEW: Calling DRAW_GENERIC_TIMER on NON-BOSS.")
					DRAW_GENERIC_TIMER(iTime, "GB_WORK_END", DEFAULT, DEFAULT, 0)
				ELSE
//					CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_BOTTOM_RIGHT_UI - SPEW: Calling DRAW_GENERIC_TIMER(0) on NON-BOSS.")
					DRAW_GENERIC_TIMER(0, "GB_WORK_END", DEFAULT, DEFAULT, 0)
				ENDIF
//					SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
			ENDIF

		BREAK 
		
		CASE eGBHTB_END
		
			IF eUserInterfaceLevel = GB_UI_LEVEL_MINIMAL
			OR eUserInterfaceLevel = GB_UI_LEVEL_FULL
		
				IF HAS_NET_TIMER_STARTED(serverBD.ModeEndTimer)
					DRAW_GENERIC_TIMER(0, "GB_WORK_END", DEFAULT)
				ENDIF
			
			ENDIF
			
		BREAK 
		
	ENDSWITCH
ENDPROC


/// PURPOSE:
///    Maintain the pad rumble intensity based on distance.
///    Further away = lower intensity.
PROC MAINTAIN_RUMBLE()

	IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PLAYER_ID(), GET_GANG_BOSS_PLAYER_INDEX(), FALSE)
		EXIT
	ENDIF

	VECTOR vGangBossPos = GET_ENTITY_COORDS(GET_PLAYER_PED(GET_GANG_BOSS_PLAYER_INDEX()), FALSE)
	VECTOR vPlayerPos = GET_ENTITY_COORDS(GET_PLAYER_PED(PLAYER_ID()), FALSE)
	FLOAT fDistance = VDIST(vGangBossPos, vPlayerPos)
	FLOAT fDelta, fIntensity

	IF (fDistance < cfHTB_MAX_RUMBLE_RANGE)
		IF fDistance < cfHTB_MIN_RUMBLE_DISTANCE
			fIntensity = ciHTB_MAX_SHAKE_VALUE
		ELSE
			fDelta = 1.0 - (CLAMP(fDistance, cfHTB_MIN_RUMBLE_DISTANCE, cfHTB_MAX_RUMBLE_RANGE) / cfHTB_MAX_RUMBLE_RANGE)
			fIntensity = LERP_FLOAT(ciHTB_MIN_SHAKE_VALUE, ciHTB_MAX_SHAKE_VALUE, fDelta)
		ENDIF
		
		SET_CONTROL_SHAKE(PLAYER_CONTROL, ciHTB_RUMBLE_DURATION, ROUND(fIntensity))
		
		#IF IS_DEBUG_BUILD
		IF (GET_FRAME_COUNT() % 60) = 0
			CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_RUMBLE - PlayerDistance: ", fDistance, ", fIntensity: ", ROUND(fIntensity))
		ENDIF
		#ENDIF
	ENDIF
	
ENDPROC


/// PURPOSE:
///    GOON ONLY
///    Wrapper for several cases where player cannot continue to be a participant in Hunt the Boss for
///    various reasons.
/// RETURNS:
///    TRUE if any premature exit condition is reached.
FUNC BOOL SHOULD_PLAYER_EXIT_MODE_PARTICIPATION(BOOL bIsBoss)

	IF bIsBoss
		RETURN FALSE
	ENDIF	

	IF IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), FALSE)	
		CPRINTLN(dbChan, "[GB][HTB] - SHOULD_PLAYER_EXIT_MODE_PARTICIPATION - IS_PLAYER_IN_MP_PROPERTY() = TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_IN_SHOP_OF_TYPE(SHOP_TYPE_CARMOD)
		CPRINTLN(dbChan, "[GB][HTB] - SHOULD_PLAYER_EXIT_MODE_PARTICIPATION - IS_PLAYER_IN_SHOP_OF_TYPE(SHOP_TYPE_CARMOD) = TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_IN_CORONA()
		CPRINTLN(dbChan, "[GB][HTB] - SHOULD_PLAYER_EXIT_MODE_PARTICIPATION - IS_PLAYER_IN_CORONA() = TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_CINEMA)
		CPRINTLN(dbChan, "[GB][HTB] - SHOULD_PLAYER_EXIT_MODE_PARTICIPATION - IS_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_CINEMA) = TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
		CPRINTLN(dbChan, "[GB][HTB] - SHOULD_PLAYER_EXIT_MODE_PARTICIPATION - IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP() = TRUE")
		RETURN TRUE
	ENDIF
	
	IF NETWORK_IS_IN_MP_CUTSCENE()
		CPRINTLN(dbChan, "[GB][HTB] - SHOULD_PLAYER_EXIT_MODE_PARTICIPATION - NETWORK_IS_IN_MP_CUTSCENE() = TRUE")
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Update local client participation level based on distance from the start point.
///    Also run the general GB distance logic for wanted/mental state/bad sport.
///    Also handles shop access for script participants.
PROC MAINTAIN_PARTICIPATION()

	FLOAT fDistance
	
	IF NOT IS_VECTOR_ZERO(serverBD.vTriggerPosition)
		IF serverBD.eStage >= eGBHTB_PLAYING
		OR GB_GET_LOCAL_PLAYER_GANG_BOSS() = ServerBD.piGangBoss
			GB_MAINTAIN_BOSS_WORK_DISTANT_CHECKS(FMMC_TYPE_GB_HUNT_THE_BOSS, serverBD.vTriggerPosition, bSetMental)
		ENDIF
		IF eUserInterfaceLevel != GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())
			#IF IS_DEBUG_BUILD
			CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_PARTICIPATION - Updating local UI level to: ",
					GET_NAME_OF_UI_LEVEL(GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())),
					", from: ",
					GET_NAME_OF_UI_LEVEL(eUserInterfaceLevel))
			#ENDIF	
			eUserInterfaceLevel = GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())
		ENDIF
	ENDIF
	
	// If the boss accepts an invite, it takes too long for the host to notice. Make sure the 
	// boss informs the server that the mode must end.
	IF AM_I_THE_GANG_BOSS()
		IF HAS_LOCAL_PLAYER_ACCEPTED_AN_INVITE()
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_BossInvalid)
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_BossInvalid)
				CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_PARTICIPATION - Boss - Setting myself as invalid as invite has been accepted.")
			ENDIF
		ENDIF
	ENDIF
	
	SWITCH GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT())

		CASE eGBHTB_PLAYING
			bPlayerShouldExit = GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE() OR SHOULD_PLAYER_EXIT_MODE_PARTICIPATION(AM_I_THE_GANG_BOSS())
			IF bPlayerShouldExit
				IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_ActiveParticipant)
					CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_ActiveParticipant)
					IF AM_I_THE_GANG_BOSS()
						SET_ALL_SHOP_LOCATES_ARE_BLOCKED(FALSE)
						SET_PLAYER_BLOCK_FROM_PROPERTY_FLAG(PPAF_GB_HUNT_THE_BOSS)
					ENDIF
					CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_PARTICIPATION - GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE = TRUE, unset active participation status.")
				ENDIF
			ELSE
				IF IS_PLAYER_IN_MISSION_AREA(PLAYER_ID(), ciHTB_ACTIVE_RANGE, fDistance)
					IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_ActiveParticipant)
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_ActiveParticipant)
						IF AM_I_THE_GANG_BOSS()
							SET_ALL_SHOP_LOCATES_ARE_BLOCKED(TRUE)
							SET_PLAYER_BLOCK_FROM_PROPERTY_FLAG(PPAF_GB_HUNT_THE_BOSS)
						ENDIF
						CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_PARTICIPATION - Setting local client as an active participant. fDistance: ", fDistance)
					ENDIF
				ELSE
					IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_ActiveParticipant)
						CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_ActiveParticipant)
						IF AM_I_THE_GANG_BOSS()
							SET_ALL_SHOP_LOCATES_ARE_BLOCKED(FALSE)
							CLEAR_PLAYER_BLOCK_FROM_PROPERTY_FLAG(PPAF_GB_HUNT_THE_BOSS)
						ENDIF
						CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_PARTICIPATION - Clearing local client as an active participant. fDistance: ", fDistance)
					ENDIF
				ENDIF
			ENDIF
				
//			url:bugstar:2623180 - Allow players who have went passive before the work started to join in
//			IF IS_MP_PASSIVE_MODE_ENABLED()
//				CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_PARTICIPATION - Local player has activated passive mode, exit the script.")
//				SET_CLIENT_MISSION_STATE(GAME_STATE_END)
//				CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_DoneTelemetry)
//				GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_FORCED) 
//			ENDIF
			
			IF NOT AM_I_THE_GANG_BOSS_OR_GOON()
				IF IS_PLAYER_IN_MISSION_AREA(PLAYER_ID(), g_sMPTunables.igb_huntboss_area_radius, fDistance)
					IF NOT GB_IS_GLOBAL_CLIENT_BIT0_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_0_PERMANENT_PARTICIPANT)
						CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_PARTICIPATION - Setting myself as a permanent participant.")
						GB_SET_PLAYER_AS_PERMANENT_ON_BOSS_MISSION()
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
	
	ENDSWITCH
	
ENDPROC


/// PURPOSE:
///    Update objective text for all clients based on client mission stage.
PROC MAINTAIN_OBJECTIVE_TEXT()	

	IF SHOULD_UI_BE_HIDDEN()
		IF IS_THERE_ANY_CURRENT_OBJECTIVE_TEXT_FROM_THIS_SCRIPT()
			CLEAR_ANY_OBJECTIVE_TEXT_FROM_THIS_SCRIPT()
		ENDIF
		
		EXIT
	ENDIF

	IF eUserInterfaceLevel != GB_UI_LEVEL_FULL
		IF IS_THERE_ANY_CURRENT_OBJECTIVE_TEXT_FROM_THIS_SCRIPT()
			CLEAR_ANY_OBJECTIVE_TEXT_FROM_THIS_SCRIPT()
		ENDIF
		
		EXIT 
	ENDIF

	SWITCH GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT())
		CASE eGBHTB_INIT
//			IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PLAYER_ID(), GET_GANG_BOSS_PLAYER_INDEX(), TRUE)
//				IF NOT HAS_THIS_MP_OBJECTIVE_TEXT_BEEN_RECEIVED("GB_HTB_OT0")
//					PRINT_OBJECTIVE_TEXT("GB_HTB_OT0")
//				ENDIF
//			ENDIF
		BREAK 
		
		CASE eGBHTB_PLAYING
			IF AM_I_THE_GANG_BOSS()
				IF HAS_NET_TIMER_STARTED(serverBD.OutOfRangeTimer)
					IF NOT HAS_THIS_MP_OBJECTIVE_TEXT_BEEN_RECEIVED("GB_HTB_OT4")
						PRINT_OBJECTIVE_TEXT("GB_HTB_OT4")
					ENDIF
				ELSE
					IF NOT HAS_THIS_MP_OBJECTIVE_TEXT_BEEN_RECEIVED("GB_HTB_OT1")
						PRINT_OBJECTIVE_TEXT("GB_HTB_OT1")
					ENDIF
				ENDIF
			ELIF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PLAYER_ID(), GET_GANG_BOSS_PLAYER_INDEX(), FALSE)
				IF eUserInterfaceLevel = GB_UI_LEVEL_FULL
					IF NOT HAS_THIS_MP_OBJECTIVE_TEXT_BEEN_RECEIVED("GB_HTB_OT2")
						Print_Objective_Text_With_Coloured_Text_Label("GB_HTB_OT2","GB_BOSS_LC",hudBossColour)
					ENDIF
				ENDIF
			ELSE
				IF IS_PLAYER_AN_ACTIVE_PARTICIPANT(PARTICIPANT_ID_TO_INT())
					IF NOT HAS_THIS_MP_OBJECTIVE_TEXT_BEEN_RECEIVED("GB_HTB_OT3")
						//PRINT_OBJECTIVE_TEXT("GB_HTB_OT3")
						IF IS_NET_PLAYER_OK(serverBD.piGangBoss, FALSE)
							Print_Objective_Text_With_Coloured_Player_Name_And_String("GB_HTB_OT3", serverBD.piGangBoss, "GB_BOSS_LC", hudBossColour)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK 
		
		CASE eGBHTB_END
			IF IS_THERE_ANY_CURRENT_OBJECTIVE_TEXT_FROM_THIS_SCRIPT()
				CLEAR_ANY_OBJECTIVE_TEXT_FROM_THIS_SCRIPT()
			ENDIF
		BREAK 
		
		CASE eGBHTB_CLEANUP
			IF IS_THERE_ANY_CURRENT_OBJECTIVE_TEXT_FROM_THIS_SCRIPT()
				CLEAR_ANY_OBJECTIVE_TEXT_FROM_THIS_SCRIPT()
			ENDIF
		BREAK 
	ENDSWITCH
ENDPROC


/// PURPOSE:
///    Update local clients with SMS messages when they get to appropriate states within the mode.
PROC MAINTAIN_SMS()

	SWITCH playerBD[PARTICIPANT_ID_TO_INT()].eStage
	
		CASE eGBHTB_PLAYING
		
			IF AM_I_THE_GANG_BOSS()
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_SentSMS)
					IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(	CHAR_MP_RAY_LAVOY,
															"GB_INTTXT_HBB",
															TXTMSG_LOCKED, 
															TXTMSG_NOT_CRITICAL, 
															TXTMSG_AUTO_UNLOCK_AFTER_READ, 
															NO_REPLY_REQUIRED) 
															
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_SentSMS)
						CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_SMS - eGBHTB_PLAYING - Sent SMS to Boss local player for start of mode. Label: GB_INTTXT_HBB")									
					ENDIF
				ENDIF
				
			ELIF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PLAYER_ID(), GET_GANG_BOSS_PLAYER_INDEX(), FALSE)
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_SentSMS)
					IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(	CHAR_MP_RAY_LAVOY,
															"GB_INTTXT_HBG",
															TXTMSG_LOCKED, 
															TXTMSG_NOT_CRITICAL, 
															TXTMSG_AUTO_UNLOCK_AFTER_READ, 
															NO_REPLY_REQUIRED) 		
															
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_SentSMS)
						CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_SMS - eGBHTB_PLAYING - Sent SMS to Goon local player for start of mode. Label: GB_INTTXT_HBG")
					ENDIF
				ENDIF
			ENDIF
		
		BREAK
	
	ENDSWITCH

ENDPROC


/// PURPOSE:
///    Inspect the server bitset and perform reward calculations if the mode ended in such
///    a way that requires it (which is never guaranteed).
PROC PROCESS_END_REWARDS()
 
 	GANG_BOSS_MANAGE_REWARDS_DATA sData
 	
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_DoneRewards)
	
	 	IF IS_BIT_SET(serverBD.iServerBitSet, biS_BossKilled)
			IF PLAYER_ID() = GET_WINNER()
				IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PLAYER_ID(), GET_GANG_BOSS_PLAYER_INDEX(), FALSE)
					GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_HUNT_THE_BOSS, FALSE, sData) // I killed the boss BUT I'm a gang member too.
				ELSE
					GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_HUNT_THE_BOSS, TRUE, sData) // I killed the boss.
				ENDIF
			ELSE
				GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_HUNT_THE_BOSS, FALSE, sData) // I didn't kill the boss.
			ENDIF
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_DoneRewards)
			CPRINTLN(dbChan, "[GB][HTB] - PROCESS_END_REWARDS - biS_BossKilled - Processed end rewards.")
			
		ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_ModeExpired)
			IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PLAYER_ID(), GET_GANG_BOSS_PLAYER_INDEX(), TRUE)
				GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_HUNT_THE_BOSS, TRUE, sData) // My gang's Boss survived (might be local player)
				CPRINTLN(dbChan, "[GB][HTB] - PROCESS_END_REWARDS - biS_ModeExpired - Processed end rewards (Boss/Goon).")
			ELSE
				GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_HUNT_THE_BOSS, FALSE, sData) // Target gang's Boss survived, I lost.
				CPRINTLN(dbChan, "[GB][HTB] - PROCESS_END_REWARDS - biS_ModeExpired - Processed end rewards (Non-gang member).")
			ENDIF
			
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_DoneRewards)
			CPRINTLN(dbChan, "[GB][HTB] - PROCESS_END_REWARDS - biS_ModeExpired - Processed end rewards.")
		
		ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_BossLeftArea)
		OR IS_BIT_SET(serverBD.iServerBitSet, biS_BossLeftSession)
			GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_HUNT_THE_BOSS, FALSE, sData) // No winners, Boss ran away.
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_DoneRewards)
			CPRINTLN(dbChan, "[GB][HTB] - PROCESS_END_REWARDS - biS_BossLeft (session/area) - Processed end rewards.")
		
		ENDIF
	ENDIF
 
ENDPROC


/// PURPOSE:
///    Send the appropriate telemetry markers to the server depending on exit state.
PROC PROCESS_END_TELEMETRY()

	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_DoneTelemetry)

		// Process the end reasons and display relevant shards.
		IF IS_BIT_SET(serverBD.iServerBitSet, biS_ModeExpired)
			IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PLAYER_ID(), GET_GANG_BOSS_PLAYER_INDEX(), TRUE)
				GB_SET_COMMON_TELEMETRY_DATA_ON_END(TRUE, GB_TELEMETRY_END_TIME_OUT)  // I'm the Boss/Goon and my Boss/I escaped.
			ELSE
				GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_TIME_OUT)  // I'm not a member of the active gang, and the active gang boss escaped.
			ENDIF
			
		ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_BossLeftSession)
			GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_BOSS_LEFT) 
	
		ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_BossLeftArea)
			GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST) 
		
		ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_BossKilled)
			IF NOT AM_I_THE_GANG_BOSS()
				IF PLAYER_ID() = GET_WINNER()
					IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PLAYER_ID(), GET_GANG_BOSS_PLAYER_INDEX(), FALSE)
						GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST) // I killed the boss BUT I'm a gang member too.
					ELSE
						GB_SET_COMMON_TELEMETRY_DATA_ON_END(TRUE, GB_TELEMETRY_END_WON) // I killed the boss.
					ENDIF
				ELSE
					GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST) // I didn't kill the boss.
				ENDIF
			ELSE
				GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST) // I died as the Boss.
			ENDIF

		ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_BossNoShow)
			GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_TIME_OUT) 
			
		ELSE
			CPRINTLN(dbChan, "[GB][HTB] - PROCESS_END_TELEMETRY - iServerBitSet has no valid end reason. Bitset: ", serverBD.iServerBitSet)
			GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_NONE) 
		ENDIF
		
		CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_DoneTelemetry)
	
	ENDIF

ENDPROC


PROC MAINTAIN_OVERHEAD_MARKERS()
	
	IF SHOULD_UI_BE_HIDDEN()
		EXIT
	ENDIF
	
	// Gang members have no reason to see an arrow above their boss.
	IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PLAYER_ID(), GET_GANG_BOSS_PLAYER_INDEX(), TRUE)
		EXIT
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_GANG_BOSS_PLAYER_INDEX()))
		EXIT
	ENDIF

	VECTOR vGangBossPos = GET_ENTITY_COORDS(GET_PLAYER_PED(GET_GANG_BOSS_PLAYER_INDEX()), FALSE)
	VECTOR vPlayerPos = GET_ENTITY_COORDS(GET_PLAYER_PED(PLAYER_ID()), FALSE)
	FLOAT fDistance = VDIST(vGangBossPos, vPlayerPos)

	IF (fDistance < cfHTB_MAX_OVERHEAD_RANGE)
		DRAW_MARKER(MARKER_ARROW, vGangBossPos+<<0,0,2.0>>, <<0,0,0>>, <<180,0,0>>, <<0.5,0.5,0.5>>, iRGBA[0], iRGBA[1], iRGBA[2], 100, TRUE, TRUE) 
	ENDIF
	
ENDPROC


/// PURPOSE:
///    Maintain participant qualification (distance based).
PROC MAINTAIN_QUALIFICATION()

	FLOAT fDistanceApart

	SWITCH playerBD[PARTICIPANT_ID_TO_INT()].eStage
	
		CASE eGBHTB_PLAYING
		
			IF IS_PLAYER_IN_MISSION_AREA(PLAYER_ID(), g_sMPTunables.igb_huntboss_area_radius-ciHTB_TRIGGER_BUFFER, fDistanceApart)
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_QualifiedParticipant)
					GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
					CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_QUALIFICATION - Setting local client as an qualified participant. fDistanceApart: ", fDistanceApart)
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_QualifiedParticipant)
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH

ENDPROC


/// PURPOSE:
///    Determine if it was my boss that launched the assault Boss Work
FUNC BOOL DID_MY_BOSS_LAUNCH_HUNT_THE_BOSS()
	RETURN GB_IS_LOCAL_PLAYER_MEMBER_OF_THIS_GANG(GET_GANG_BOSS_PLAYER_INDEX())
ENDFUNC


/// PURPOSE:
///    Play audio cues based on how close the Boss player is
PROC MAINTAIN_AUDIO_CUES()
	BOOL bParticipating = TRUE
	FLOAT fPlayerDist
	STRING sEventMusic 
	iSoundStartTimer = iSoundStartTimer
	
	//Set timing bits
	IF HAS_NET_TIMER_STARTED(serverBD.ModeDurationTimer)
		IF HAS_NET_TIMER_EXPIRED_READ_ONLY(serverBD.ModeDurationTimer,g_sMPTunables.igb_huntboss_time_limit - SOUND_EVENT_1_TIME)
			mBit(iSoundBitset, bit_Event35sToEnd, 1)
		ENDIF
		IF HAS_NET_TIMER_EXPIRED_READ_ONLY(serverBD.ModeDurationTimer,g_sMPTunables.igb_huntboss_time_limit - SOUND_EVENT_2_TIME)
			mBit(iSoundBitset, bit_Event30sToEnd, 1)
		ENDIF
		IF HAS_NET_TIMER_EXPIRED_READ_ONLY(serverBD.ModeDurationTimer,g_sMPTunables.igb_huntboss_time_limit - SOUND_EVENT_3_TIME)
			mBit(iSoundBitset, bit_Event27sToEnd, 1)
		ENDIF
		IF HAS_NET_TIMER_EXPIRED_READ_ONLY(serverBD.ModeDurationTimer,g_sMPTunables.igb_huntboss_time_limit)
			mBit(iSoundBitset, bit_EventEnded, 1)
		ENDIF
	ENDIF
	
	//Check if we're an active participant
	IF IS_PLAYER_IN_MISSION_AREA_BUFFERED(PLAYER_ID(),g_sMPTunables.igb_huntboss_area_radius, fPlayerDist, mBit(iSoundBitset, bit_ActiveParticipant))
		mBit(iSoundBitset, bit_ActiveParticipant, 1)
	ELSE
		mBit(iSoundBitset, bit_ActiveParticipant, 0)
	ENDIF
		
	IF bPlayerShouldExit OR
	(NOT DID_MY_BOSS_LAUNCH_HUNT_THE_BOSS()	AND NOT mBit(iSoundBitset, bit_ActiveParticipant))
		bParticipating = FALSE
	ENDIF
	
	//DEBUG display - could be useful
	//DISPLAY_TEXT_WITH_LITERAL_STRING(0.4, 0.7, "STRING", PICK_STRING(bParticipating,"Active","INactive"))
	
	SWITCH GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT())
	
		CASE eGBHTB_INIT
			//This is where the boss could start audio early, do nothing for now
		BREAK
		
		CASE eGBHTB_PLAYING
			
			//Start internal audio timer
			IF NOT mBit(iSoundBitset, bit_EventInternalTimerStart, 1)
				iSoundStartTimer = GET_NETWORK_TIME()
			ENDIF
			
			//Manage the general music track
			IF bParticipating AND NOT mBit(iSoundBitset, bit_Event35sToEnd)
				IF NOT mBit(iSoundBitset, bit_EventMusicOn)	//Launch the ambient music and timer
					sEventMusic = PICK_STRING(DID_MY_BOSS_LAUNCH_HUNT_THE_BOSS(), "BG_HUNT_BOSS_DEFEND_START", "BG_HUNT_BOSS_ATTACK_START")
					IF PREPARE_MUSIC_EVENT(sEventMusic) AND mBit(iSoundBitset, bit_AmbientMusicDisabled)
						TRIGGER_MUSIC_EVENT(sEventMusic)
						CPRINTLN(dbChan2, "[GB][HTB] - MAINTAIN_AUDIO_CUES Began Boss Hunt music: ",sEventMusic)
						mBit(iSoundBitset, bit_EventMusicOn, 1)
					ENDIF
					//Disable flight/wanted music 1 frame before the main music track
					IF NOT mBit(iSoundBitset, bit_AmbientMusicDisabled)
						DISABLE_AMBIENT_MUSIC_FOR_CUSTOM_SCORE(TRUE)
					ENDIF					
				ENDIF
				
			ELSE
				//If we began the audio
				IF mBit(iSoundBitset, bit_EventMusicOn) AND NOT mBit(iSoundBitset, bit_Event35sToEnd)
					IF PREPARE_MUSIC_EVENT("BG_HUNT_STOP")
						TRIGGER_MUSIC_EVENT("BG_HUNT_STOP")
						DISABLE_AMBIENT_MUSIC_FOR_CUSTOM_SCORE(FALSE)
						CPRINTLN(dbChan2,"Stopped main music event")
						mBit(iSoundBitset, bit_EventMusicOn, 0) 
					ENDIF				
				ENDIF
			ENDIF
			
			//Manage the 35 second cue
			IF mBit(iSoundBitset, bit_Event35sToEnd)
				IF mBit(iSoundBitset, bit_EventMusicOn)
					IF PREPARE_MUSIC_EVENT("APT_PRE_COUNTDOWN_STOP")
						TRIGGER_MUSIC_EVENT("APT_PRE_COUNTDOWN_STOP")
						CPRINTLN(dbChan2, "[GB][HTB] - MAINTAIN_AUDIO_CUES Triggered Pre-Countdown Stop")
						mBit(iSoundBitset, bit_EventMusicOn, 0)
					ENDIF
				ENDIF
			ENDIF
			
			//Manage the 30 second point
			IF mBit(iSoundBitset, bit_Event30sToEnd)
				//Start the 30 second cue
				IF NOT mBit(iSoundBitset, bit_EventPlaying30sTimer) AND NOT mBit(iSoundBitset,bit_Event27sToEnd)
				AND NOT (SHOULD_UI_BE_HIDDEN() OR eUserInterfaceLevel != GB_UI_LEVEL_FULL)
					IF PREPARE_MUSIC_EVENT("APT_COUNTDOWN_30S")
						SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE)
						TRIGGER_MUSIC_EVENT("APT_COUNTDOWN_30S")
						CPRINTLN(dbChan2, "[GB][HTB] - MAINTAIN_AUDIO_CUES Triggered 30s Timer")
						mBit(iSoundBitset, bit_EventPlaying30sTimer, 1)
					ENDIF
				ENDIF
				
				//Disable radio control
				IF NOT mBit(iSoundBitset, bit_EventRadioDisabled) AND NOT mBit(iSoundBitset, bit_Event27sToEnd) AND mBit(iSoundBitset, bit_EventPlaying30sTimer)
					SET_USER_RADIO_CONTROL_ENABLED(FALSE)
					CPRINTLN(dbChan2,"[GB][HTB] - MAINTAIN_AUDIO_CUES Triggered 30s Disable radio control")
					mBit(iSoundBitset, bit_EventRadioDisabled, 1)
				ENDIF
			ENDIF
			
		BREAK
		
		CASE eGBHTB_END
			//Play the fade-in
			IF NOT mBit(iSoundBitset, bit_EventFadeInPlayed)
			AND (mBit(iSoundBitset, bit_EventPlaying30sTimer))
				IF PREPARE_MUSIC_EVENT("APT_FADE_IN_RADIO")
					CPRINTLN(dbChan2, "[GB][HTB] - MAINTAIN_AUDIO_CUES Triggered radio fade-in")
					TRIGGER_MUSIC_EVENT("APT_FADE_IN_RADIO")
					mBit(iSoundBitset, bit_EventFadeInPlayed, 1)
				ENDIF
			ENDIF
			
			//If we're still playing the main music, kill early
			IF mBit(iSoundBitset, bit_EventMusicOn)
				IF PREPARE_MUSIC_EVENT("BG_HUNT_STOP")
					CPRINTLN(dbChan2, "[GB][HTB] - MAINTAIN_AUDIO_CUES Triggered normal early end cue")
					CANCEL_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
					TRIGGER_MUSIC_EVENT("BG_HUNT_STOP")
					mBit(iSoundBitset, bit_EventMusicOn, 0)
				ENDIF
			ENDIF
			
			//If we're playing the 30s timer and it ends early, kill it
			IF mBit(iSoundBitset, bit_EventPlaying30sTimer) AND NOT mBit(iSoundBitset, bit_EventEnded)
				IF PREPARE_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
					CPRINTLN(dbChan2, "[GB][HTB] - MAINTAIN_AUDIO_CUES Triggered 30s Kill for early ending")
					mBit(iSoundBitset, bit_EventPlaying30sTimer, 0)
					TRIGGER_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
				ENDIF
			ENDIF
				
			//Re-enable ambient music, if disabled
			IF mBit(iSoundBitset, bit_AmbientMusicDisabled)
				DISABLE_AMBIENT_MUSIC_FOR_CUSTOM_SCORE(FALSE)
			ENDIF
		BREAK
	
	ENDSWITCH
	
	
	//Events outside the Server state check
	
	//Re-enable radio control
	IF mBit(iSoundBitset, bit_EventRadioDisabled) 
	AND (mBit(iSoundBitset, bit_Event27sToEnd) OR GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT()) = eGBHTB_END)
		SET_USER_RADIO_CONTROL_ENABLED(TRUE)
		SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
		CPRINTLN(dbChan2, "[GB][HTB] - MAINTAIN_AUDIO_CUES Triggered 27s Radio Control enable ")
		mBit(iSoundBitset, bit_EventRadioDisabled, 0)
	ENDIF
		
	
	//Manage out of range timer
	IF HAS_NET_TIMER_STARTED(serverBD.OutOfRangeTimer)
	AND HAS_NET_TIMER_EXPIRED_READ_ONLY(serverBD.OutOfRangeTimer, TEN_SECONDS)
		IF NOT mBit(iSoundBitset, bit_EventCountdownActive)
			CPRINTLN(dbChan2,"[GB][HTB] - MAINTAIN_AUDIO_CUES Started countdown timer audio")
			iSoundIDCountdown = GET_SOUND_ID()
			PLAY_SOUND_FRONTEND(iSoundIDCountdown, "10s", "MP_MISSION_COUNTDOWN_SOUNDSET", FALSE) 
			mBit(iSoundBitset, bit_EventCountdownActive, 1)
		ENDIF
	ELSE
		IF mBit(iSoundBitset, bit_EventCountdownActive)
			CPRINTLN(dbChan2,"[GB][HTB] - MAINTAIN_AUDIO_CUES Stopping the countdown timer audio")
			STOP_SOUND(iSoundIDCountdown)
			iSoundIDCountdown = -1
			mBit(iSoundBitset, bit_EventCountdownActive, 0)
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Client only processing.
PROC CLIENT_PROCESSING()
	//Buffers whether the boss is still in the session
	bBossNetStateOK = IS_NET_PLAYER_OK(GET_GANG_BOSS_PLAYER_INDEX(),FALSE)

	MAINTAIN_CLIENT_SERVER_STATE_SYNC()
	MAINTAIN_PARTICIPATION()
	MAINTAIN_QUALIFICATION()
	MAINTAIN_EVENTS()
	MAINTAIN_BLIPS()
	MAINTAIN_BOTTOM_RIGHT_UI()
	MAINTAIN_OBJECTIVE_TEXT()
	MAINTAIN_SMS()
	MAINTAIN_AUDIO_CUES()
	
	SWITCH playerBD[PARTICIPANT_ID_TO_INT()].eStage
	
		CASE eGBHTB_INIT
		
			IF AM_I_THE_GANG_BOSS()
				DO_HELP_TEXT(0)
			ELIF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PLAYER_ID(), GET_GANG_BOSS_PLAYER_INDEX(), FALSE)
				DO_HELP_TEXT(6)
			ENDIF
		
		BREAK 
		
		CASE eGBHTB_PLAYING
//			HANDLE_MAP_EXITING_PLAYERS() 
			// Init help-text and shards. Introduce the various factions to the hunt.
			IF AM_I_THE_GANG_BOSS()
				DO_HELP_TEXT(5)
				DO_BIG_MESSAGE(2)
				
				IF bBossNetStateOK
					IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_QualifiedParticipant)
						GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_QualifiedParticipant)
						CPRINTLN(dbChan, "[GB][HTB] - CLIENT_PROCESSING - eGBHTB_PLAYING - Setting myself as a qualifying participant directly.")
					ENDIF
				ENDIF
				
				IF NOT IS_MP_DECORATOR_BIT_SET(PLAYER_ID(), MP_DECORATOR_BS_INVALID_TO_SPECTATE)
					SET_MP_DECORATOR_BIT(PLAYER_ID(), MP_DECORATOR_BS_INVALID_TO_SPECTATE)
					CPRINTLN(dbChan, "[GB][HTB] - CLIENT_PROCESSING - eGBHTB_PLAYING - Removing myself from spectate list.")
				ENDIF
				
				IF IS_BIT_SET(serverBD.iServerBitSet, biS_BossOutOfRange)
					DO_HELP_TEXT(4)
				ENDIF
				
			ELIF bBossNetStateOK
				IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PLAYER_ID(), GET_GANG_BOSS_PLAYER_INDEX(), FALSE)
					DO_HELP_TEXT(3)
					DO_BIG_MESSAGE(1)
				ELSE
					DO_BIG_MESSAGE(0)
					IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_ActiveParticipant)
						DO_HELP_TEXT(8)
					ELSE
						DO_HELP_TEXT(1)
					ENDIF
					// Only non-gang member and not the leader need the rumble, and also only when playing
					// otherwise randoms will get a rumble effect in freemode with no notification of an
					// event occuring otherwise.
					IF IS_PLAYER_AN_ACTIVE_PARTICIPANT(PARTICIPANT_ID_TO_INT())
						MAINTAIN_RUMBLE()
						MAINTAIN_OVERHEAD_MARKERS()
					ENDIF
					
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_DoneTelemetry)
				GB_SET_COMMON_TELEMETRY_DATA_ON_START()
				CPRINTLN(dbChan, "[GB][HTB] - CLIENT_PROCESSING - eGBHTB_PLAYING - Sending telemetry marker for mode start.")
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_DoneTelemetry)
			ENDIF
			
			DRAW_CHALLENGE_DPAD_LBD(g_GBLeaderboardStruct.challengeLbdStruct, g_GBLeaderboardStruct.siDpadMovie, SUB_BOSS, g_GBLeaderboardStruct.dpadVariables, 
									g_GBLeaderboardStruct.fmDpadStruct)
		
		BREAK 
		
		CASE eGBHTB_END
		
			IF NOT GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PLAYER_ID(), GET_GANG_BOSS_PLAYER_INDEX(), TRUE)
				IF NOT GB_IS_GLOBAL_CLIENT_BIT0_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_0_TEMPORARY_PARTICIPANT)
					#IF IS_DEBUG_BUILD
					IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_OutOfRenderRange)
						CPRINTLN(dbChan, "[GB][HTB] - CLIENT_PROCESSING - eGBHTB_END - You are not a gang member, and out of UI range. Move straight to cleanup.")
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_OutOfRenderRange)
					ENDIF
					#ENDIF
					//Do the end telemetry still
					PROCESS_END_TELEMETRY()
					
					EXIT
				ENDIF
			ENDIF
			
			IF AM_I_THE_GANG_BOSS()
				IF IS_MP_DECORATOR_BIT_SET(PLAYER_ID(), MP_DECORATOR_BS_INVALID_TO_SPECTATE)
					CLEAR_MP_DECORATOR_BIT(PLAYER_ID(), MP_DECORATOR_BS_INVALID_TO_SPECTATE)
					CPRINTLN(dbChan, "[GB][HTB] - CLIENT_PROCESSING - eGBHTB_END - Adding myself back on spectator list.")
				ENDIF
				
				IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_BossInvalid)
					CPRINTLN(dbChan, "[GB][HTB] - CLIENT_PROCESSING - eGBHTB_END - Boss(me) is invalid, jump to script cleanup ASAP.")
					SCRIPT_CLEANUP()
					EXIT
				ENDIF
			ENDIF
		
			PROCESS_END_REWARDS()	
			PROCESS_END_TELEMETRY()
		
			// Process the end reasons and display relevant shards.
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_ModeExpired)
			
				IF AM_I_THE_GANG_BOSS()
					DO_BIG_MESSAGE(10)
				ELSE
					IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PLAYER_ID(), GET_GANG_BOSS_PLAYER_INDEX(), FALSE)
						DO_BIG_MESSAGE(11)
					ELSE
						DO_BIG_MESSAGE(6)
					ENDIF
				ENDIF
				
				GB_HTB_MANAGE_WANTED_CLEAN_UP(serverBD.piGangBoss)
		
			ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_BossLeftSession)
			
				IF IS_PLAYER_AN_ACTIVE_PARTICIPANT(PARTICIPANT_ID_TO_INT())
					IF NOT GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PLAYER_ID(), serverBD.piGangBoss, TRUE)
						DO_BIG_MESSAGE(7)
					ENDIF
				ENDIF
				
				GB_HTB_MANAGE_WANTED_CLEAN_UP()

			ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_BossLeftArea)
			
				IF AM_I_THE_GANG_BOSS()
					DO_BIG_MESSAGE(12)
				ELSE
					IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PLAYER_ID(), GET_GANG_BOSS_PLAYER_INDEX(), FALSE)
						DO_BIG_MESSAGE(13)
					ELSE
						IF IS_PLAYER_AN_ACTIVE_PARTICIPANT(PARTICIPANT_ID_TO_INT())
							DO_BIG_MESSAGE(14)
						ENDIF
					ENDIF
				ENDIF
				
				GB_HTB_MANAGE_WANTED_CLEAN_UP()
			
			ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_BossKilled)

				IF AM_I_THE_GANG_BOSS()
					IF HAS_THE_BOSS_BEEN_TEAMKILLED()
						DO_BIG_MESSAGE(9)
					ELSE
						DO_BIG_MESSAGE(8)
					ENDIF
				ELSE
					IF serverBD.piWinner = PLAYER_ID()
						IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PLAYER_ID(), GET_GANG_BOSS_PLAYER_INDEX(), FALSE)
							DO_BIG_MESSAGE(15)
						ELSE
							DO_BIG_MESSAGE(3)
						ENDIF
					ELSE
						IF IS_PLAYER_AN_ACTIVE_PARTICIPANT(PARTICIPANT_ID_TO_INT())
							IF HAS_THE_BOSS_BEEN_TEAMKILLED()
								DO_BIG_MESSAGE(17)
							ELSE
								DO_BIG_MESSAGE(4)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				GB_HTB_MANAGE_WANTED_CLEAN_UP(serverBD.piWinner)
			
			ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_BossNoShow)
			
				IF AM_I_THE_GANG_BOSS()
					DO_BIG_MESSAGE(16)
				ELSE
					CPRINTLN(dbChan, "[GB][HTB] - CLIENT_PROCESSING - eGBHTB_END - biS_BossNoShow = TRUE, Boss failed to start the mode properly.")
				ENDIF
				
				GB_HTB_MANAGE_WANTED_CLEAN_UP()
			
			ELSE
				CPRINTLN(dbChan, "[GB][HTB] - CLIENT_PROCESSING - eGBHTB_END - iServerBitSet has no valid end reason. Bitset: ", serverBD.iServerBitSet)
			ENDIF
			
			IF GB_MAINTAIN_BOSS_END_UI(sBossEndUI)
				CPRINTLN(dbChan, "[GB][HTB] - CLIENT_PROCESSING - eGBHTB_END - GB_MAINTAIN_BOSS_END_UI has returned TRUE, force cleint to exit state.")
				SET_CLIENT_MISSION_STATE(GAME_STATE_END)
			ENDIF
		
		BREAK 
		
		CASE eGBHTB_CLEANUP
		
		BREAK 

	ENDSWITCH

ENDPROC


/// PURPOSE:
///    Inspect the server bitset and perform reward calculations if the mode ended in such
///    a way that requires it (which is never guaranteed).
PROC PROCESS_END_TICKER()

 	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_DoneTicker)
 
 		IF IS_BIT_SET(serverBD.iServerBitSet, biS_BossKilled)
		
	 		SCRIPT_EVENT_DATA_TICKER_MESSAGE sTickerEvent
	 		sTickerEvent.Details.Type = SCRIPT_EVENT_TICKER_MESSAGE
			sTickerEvent.Details.FromPlayerIndex = PLAYER_ID()
			sTickerEvent.TickerEvent = TICKER_EVENT_GB_HTB_BOSS_KILLED
			sTickerEvent.playerID = GET_WINNER()
	 		BROADCAST_TICKER_EVENT(sTickerEvent, ALL_NON_ACTIVE_PARTICIPANTS())
			SET_BIT(serverBD.iServerBitSet, biS_DoneTicker)
				
		ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_ModeExpired)
		
			SCRIPT_EVENT_DATA_TICKER_MESSAGE sTickerEvent
	 		sTickerEvent.Details.Type = SCRIPT_EVENT_TICKER_MESSAGE
			sTickerEvent.Details.FromPlayerIndex = PLAYER_ID()
			sTickerEvent.TickerEvent = TICKER_EVENT_GB_HTB_BOSS_SURVIVED
	 		BROADCAST_TICKER_EVENT(sTickerEvent, ALL_NON_ACTIVE_PARTICIPANTS())
			SET_BIT(serverBD.iServerBitSet, biS_DoneTicker)
		
		ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_BossLeftArea)
		OR IS_BIT_SET(serverBD.iServerBitSet, biS_BossLeftSession)
		
			SCRIPT_EVENT_DATA_TICKER_MESSAGE sTickerEvent
	 		sTickerEvent.Details.Type = SCRIPT_EVENT_TICKER_MESSAGE
			sTickerEvent.Details.FromPlayerIndex = PLAYER_ID()
			sTickerEvent.TickerEvent = TICKER_EVENT_GB_HTB_BOSS_LEFT
	 		BROADCAST_TICKER_EVENT(sTickerEvent, ALL_NON_ACTIVE_PARTICIPANTS())
			SET_BIT(serverBD.iServerBitSet, biS_DoneTicker)
			
		ENDIF
		
		CPRINTLN(dbChan, "[GB][HTB] - PROCESS_END_TICKER - Done ticker. iServerBitSet: ", serverBD.iServerBitSet)
 	ENDIF
	
ENDPROC


/// PURPOSE:
///    Wrapper for server bitset inspection exit states.
/// RETURNS:
///    TRUE when and exit state has been reached.
FUNC BOOL SHOULD_MODE_FINISH()

	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_DebugEndMode)
		RETURN TRUE
	ENDIF
	#ENDIF

	IF IS_BIT_SET(serverBD.iServerBitSet, biS_ModeExpired)
	OR IS_BIT_SET(serverBD.iServerBitSet, biS_BossLeftSession)
	OR IS_BIT_SET(serverBD.iServerBitSet, biS_BossLeftArea)
	OR IS_BIT_SET(serverBD.iServerBitSet, biS_BossKilled)
	OR IS_BIT_SET(serverBD.iServerBitSet, biS_BossNoShow)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC


/// PURPOSE:
///    Monitor the boss's network status. If the boss disappears from the session mid mode, then
///    move all clients straight to cleanup.
PROC MAINTAIN_BOSS_LEAVE_SESSION()
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_BossLeftSession)
		IF NOT NETWORK_IS_PLAYER_ACTIVE(GET_GANG_BOSS_PLAYER_INDEX())
			CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_BOSS_LEAVE_SESSION - Target boss/VIP has left session.")
			SET_BIT(serverBD.iServerBitSet, biS_BossLeftSession)
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_BossLeftSession)
	AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_BossLeftArea)
		INT iBossPart = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(GET_GANG_BOSS_PLAYER_INDEX()))
		IF iBossPart >= 0
		AND iBossPart < NUM_NETWORK_PLAYERS
			IF IS_BIT_SET(playerBD[iBossPart].iClientBitSet, biC_BossInvalid)
				CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_BOSS_LEAVE_SESSION - Target boss/VIP has become invalid (likely accepted an invite). iBossPart: ", iBossPart)
				SET_BIT(serverBD.iServerBitSet, biS_BossLeftArea)
				
				IF serverBD.eStage = eGBHTB_INIT
					CPRINTLN(dbChan, "[GB][HTB] - MAINTAIN_BOSS_LEAVE_SESSION - Still in server init, directly bypass playing and go to exit.")
					SET_SERVER_MISSION_STAGE(eGBHTB_CLEANUP)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Server/host/Boss only functionality.
/// RETURNS:
///    TRUE when finished.
FUNC BOOL SERVER_PROCESSING()
	
	FLOAT fDistanceApart
	
	MAINTAIN_BOSS_LEAVE_SESSION()
	
	SWITCH serverBD.eStage
	
		CASE eGBHTB_INIT
		
			IF NOT HAS_NET_TIMER_STARTED(serverBD.ModeStartTimer)
				CPRINTLN(dbChan, "[GB][HTB] - PROCESS_SERVER - eGBHTB_INIT - ModeStartTimer timer has been started")
				START_NET_TIMER(serverBD.ModeStartTimer)
			ELSE
				IF HAS_NET_TIMER_EXPIRED(serverBD.ModeStartTimer, g_sMPTunables.igb_huntboss_area_expiry_time)
				#IF IS_DEBUG_BUILD
				OR bSkipIntro = TRUE
					bSkipIntro = FALSE
					#ENDIF
					
					CPRINTLN(dbChan, "[GB][HTB] - PROCESS_SERVER - eGBHTB_INIT - Moving to playing state, bSkipIntro: ", bSkipIntro)
					SET_BIT(serverBD.iServerBitSet, biS_BossNoShow)
					SET_SERVER_MISSION_STAGE(eGBHTB_END)
				ENDIF
			ENDIF
		
			IF NOT IS_VECTOR_ZERO(serverBD.vTriggerPosition)
				
				IF IS_BOSS_IN_MISSION_AREA(g_sMPTunables.igb_huntboss_area_radius-ciHTB_TRIGGER_BUFFER, fDistanceApart)
					CPRINTLN(dbChan, "[GB][HTB] - SERVER_PROCESSING - eGBHTB_INIT - iDistanceApart < g_sMPTunables.igb_huntboss_area_radius, move to playing state. Distance: ", fDistanceApart)
					SET_SERVER_MISSION_STAGE(eGBHTB_PLAYING)
				ENDIF
			
			#IF IS_DEBUG_BUILD
			ELSE
				CPRINTLN(dbChan, "[GB][HTB] - SERVER_PROCESSING - eGBHTB_INIT - ERROR! Trigger area is empty, how did that happen??") 
				SCRIPT_ASSERT("[GB][HTB] - SERVER_PROCESSING - eGBHTB_INIT - ERROR! Trigger area is empty, how did that happen?? Add a bug for AlastairC.") 
			#ENDIF
			ENDIF
			
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_BossLeftSession)
				CPRINTLN(dbChan, "[GB][HTB] - SERVER_PROCESSING - eGBHTB_INIT - biS_BossLeftSession is set, force exit.")
				SET_SERVER_MISSION_STAGE(eGBHTB_END)
			ENDIF
			
			IF IS_THIS_PLAYER_IN_CORONA(ServerBD.piGangBoss)
			OR IS_PLAYER_IN_PROPERTY(ServerBD.piGangBoss, TRUE)
				SET_BIT(serverBD.iServerBitSet, biS_BossLeftArea)
				CPRINTLN(dbChan, "[GB][HTB] - SERVER_PROCESSING - eGBHTB_INIT - Target boss is unavailable for mode, force exit. Corona: ", IS_THIS_PLAYER_IN_CORONA(ServerBD.piGangBoss))
				SET_SERVER_MISSION_STAGE(eGBHTB_END)
			ENDIF
			
			#IF IS_DEBUG_BUILD
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_DebugEndMode)
				CPRINTLN(dbChan, "[GB][HTB] - SERVER_PROCESSING - eGBHTB_INIT - biS_DebugEndMode is set, force exit.")
				SET_SERVER_MISSION_STAGE(eGBHTB_END)
			ENDIF
			#ENDIF
		
		BREAK 
		
		CASE eGBHTB_PLAYING
		
			IF NOT HAS_NET_TIMER_STARTED(serverBD.ModeDurationTimer)
				CPRINTLN(dbChan, "[GB][HTB] - PROCESS_SERVER - eGBHTB_PLAYING - ModeDurationTimer timer has been started")
				START_NET_TIMER(serverBD.ModeDurationTimer)
			ELSE
		
				IF NOT HAS_NET_TIMER_STARTED(serverBD.OutOfRangeTimer)

					IF NOT IS_BOSS_IN_MISSION_AREA(g_sMPTunables.igb_huntboss_area_radius, fDistanceApart)
						CPRINTLN(dbChan, "[GB][HTB] - SERVER_PROCESSING - eGBHTB_PLAYING - iDistanceApart > g_sMPTunables.igb_huntboss_area_radius, triggering out of area timer. Distance: ", fDistanceApart)
						REINIT_NET_TIMER(serverBD.OutOfRangeTimer)
						SET_BIT(serverBD.iServerBitSet, biS_BossOutOfRange)
					ENDIF
					
				ELSE
					
					IF NOT IS_BOSS_IN_MISSION_AREA(g_sMPTunables.igb_huntboss_area_radius, fDistanceApart)
						IF HAS_NET_TIMER_EXPIRED(serverBD.OutOfRangeTimer, (TEN_SECONDS * 2))
							CPRINTLN(dbChan, "[GB][HTB] - SERVER_PROCESSING - eGBHTB_PLAYING - Boss left the area for more than ",(TEN_SECONDS * 2),"ms, move to end (fail).")
							SET_BIT(serverBD.iServerBitSet, biS_BossLeftArea)
						ENDIF
					ELSE
						CPRINTLN(dbChan, "[GB][HTB] - SERVER_PROCESSING - eGBHTB_PLAYING - Boss re-entered the zone, cancel timeout.")
						RESET_NET_TIMER(serverBD.OutOfRangeTimer)
						REINIT_NET_TIMER(serverBD.OutOfRangeBlipTimer)
						CLEAR_BIT(serverBD.iServerBitSet, biS_BossOutOfRange)
					ENDIF
					
				ENDIF
				
				IF HAS_NET_TIMER_STARTED(serverBD.OutOfRangeBlipTimer)
					IF HAS_NET_TIMER_EXPIRED(serverBD.OutOfRangeBlipTimer, FIVE_SECONDS)
						CPRINTLN(dbChan, "[GB][HTB] - SERVER_PROCESSING - eGBHTB_PLAYING - Blip re-entry timer has expired, resetting timer.")
						RESET_NET_TIMER(serverBD.OutOfRangeBlipTimer)
					ENDIF
				ENDIF
				
				BOOL bShouldFinish
				bShouldFinish = SHOULD_MODE_FINISH()
				
				IF HAS_NET_TIMER_EXPIRED(serverBD.ModeDurationTimer, g_sMPTunables.igb_huntboss_time_limit)
				OR bShouldFinish
					CPRINTLN(dbChan, "[GB][HTB] - SERVER_PROCESSING - eGBHTB_PLAYING - Moving to END state. bShouldFinish: ", bShouldFinish, ", iServerBitSet: ", serverBD.iServerBitSet)
					SET_SERVER_MISSION_STAGE(eGBHTB_END)
					
					IF NOT bShouldFinish
						CPRINTLN(dbChan, "[GB][HTB] - SERVER_PROCESSING - eGBHTB_PLAYING - No valid exit state, timer must have expired. Set biS_ModeExpired.")
						SET_BIT(serverBD.iServerBitSet, biS_ModeExpired)
					ELSE
						PROCESS_END_TICKER()
					ENDIF
				ENDIF
				
			ENDIF
			
			IF IS_THIS_PLAYER_IN_CORONA(ServerBD.piGangBoss)
			OR IS_PLAYER_IN_PROPERTY(ServerBD.piGangBoss, TRUE)
				SET_BIT(serverBD.iServerBitSet, biS_BossLeftArea)
				CPRINTLN(dbChan, "[GB][HTB] - SERVER_PROCESSING - eGBHTB_PLAYING - Target boss is unavailable for mode, force exit. Corona: ", IS_THIS_PLAYER_IN_CORONA(ServerBD.piGangBoss))
				SET_SERVER_MISSION_STAGE(eGBHTB_END)
			ENDIF
	
		BREAK 
		
		CASE eGBHTB_END
			
			IF NOT HAS_NET_TIMER_STARTED(serverBD.ModeEndTimer)
				CPRINTLN(dbChan, "[GB][HTB] - PROCESS_SERVER - eGBHTB_END - ModeEndTimer timer has been started")
				START_NET_TIMER(serverBD.ModeEndTimer)
			ELSE
				IF HAS_NET_TIMER_EXPIRED(serverBD.ModeEndTimer, FIFTEEN_SECONDS)
					CPRINTLN(dbChan, "[GB][HTB] - PROCESS_SERVER - eGBHTB_END - ModeEndTimer timer has expired")
					SET_SERVER_MISSION_STAGE(eGBHTB_CLEANUP)
				ENDIF
			ENDIF
		
		BREAK 
		
		CASE eGBHTB_CLEANUP RETURN TRUE
		
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC


/// PURPOSE:
///    Host/server/Boss specific setup.
/// RETURNS:
///    TRUE when finished.
FUNC BOOL INIT_SERVER()
	serverBD.piGangBoss = GB_GET_LOCAL_PLAYER_MISSION_HOST()
	serverBD.piWinner = INVALID_PLAYER_INDEX()
	
	PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBD.iMatchId1, serverBD.iMatchId2)

	VECTOR vZoneCentre[ciHTB_NUM_ZONES]
	FLOAT fDistance[ciHTB_NUM_ZONES]
	FLOAT fDistanceCache
	INT index, iSelection
	
	vZoneCentre[0] = << -1976.4810, -638.1300, 4.6219 >>
	vZoneCentre[1] = << 2306.7439, -400.6860, 86.3213 >>
	vZoneCentre[2] = << 3505.6230, 3787.0410, 28.9708 >>
	vZoneCentre[3] = << -1976.2250, 4546.2998, 56.0401 >>
	
	FOR index = 0 TO (ciHTB_NUM_ZONES-1)
		fDistance[index] = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vZoneCentre[index])
	ENDFOR
	
	fDistanceCache = fDistance[0]
	iSelection = 1 // Need to add 1 as the consts are 1 indexed instead of 0.
	
	FOR index = 0 TO (ciHTB_NUM_ZONES-1)
		IF fDistance[index] < fDistanceCache
			fDistanceCache = fDistance[index]
			iSelection = index + 1 // Need to add 1 as the consts are 1 indexed instead of 0.
		ENDIF
	ENDFOR
	
	CPRINTLN(dbChan, "[GB][HTB] - INIT_SERVER - Closest trigger quadrant distance: ", fDistanceCache, ", in slot: ", iSelection)
	serverBD.iPositionSeed = iSelection
	serverBD.vTriggerPosition = GET_GB_MODE_START_POSITION(serverBD.iPositionSeed)
	CPRINTLN(dbChan, "[GB][HTB] - INIT_SERVER - Server init complete, trigger position: ", serverBD.vTriggerPosition, ", seed: ", serverBD.iPositionSeed)
	
	RETURN TRUE
ENDFUNC


/// PURPOSE:
///    When moving from INIT to RUNNING, hit this method for 1 frame.
PROC CLIENT_ONE_FRAME_INIT()

//	url:bugstar:2623180 - Allow players who have went passive before the work started to join in
//	IF IS_MP_PASSIVE_MODE_ENABLED()
//		CPRINTLN(dbChan, "[GB][HTB] - CLIENT_ONE_FRAME_INIT - Local player has already in passive mode, exit the script.")
//		SET_CLIENT_MISSION_STATE(GAME_STATE_END)
//		CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, biC_DoneTelemetry)
//		GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_FORCED) 
//		EXIT
//	ENDIF

	eUserInterfaceLevel = GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) // Grab UI level right at the start.
	CPRINTLN(dbChan, "[GB][HTB] - CLIENT_ONE_FRAME_INIT - Init eUserInterfaceLevel: ", GET_NAME_OF_UI_LEVEL(eUserInterfaceLevel))
	GB_COMMON_BOSS_MISSION_SETUP(FMMC_TYPE_GB_HUNT_THE_BOSS)
	hudBossColour = GET_PLAYER_HUD_COLOUR(serverBD.piGangBoss)
	GET_HUD_COLOUR(hudBossColour, iRGBA[0], iRGBA[1], iRGBA[2], iRGBA[3])
ENDPROC


/// PURPOSE:
///    Basic script init wrapper.
/// RETURNS:
///    TRUE when script is network ready.
FUNC BOOL PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)

	CPRINTLN(DEBUG_NET_AMBIENT, "=== HUNT THE BOSS === PROCESS_PRE_GAME")
	CPRINTLN(dbChan, "[GB][HTB] - PROCESS_PRE_GAME")
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission), missionScriptArgs)
	
	GB_COMMON_BOSS_MISSION_PREGAME(FMMC_TYPE_GB_HUNT_THE_BOSS)
	
	//Reserve Entities
	RESERVE_NETWORK_MISSION_OBJECTS(0)
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD,SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD,SIZE_OF(playerBD))
	
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	RETURN TRUE
	
ENDFUNC


/// PURPOSE:
///    Main script loop & mode entry point.
SCRIPT(MP_MISSION_DATA missionScriptArgs)

	CPRINTLN(DEBUG_NET_AMBIENT, "=== HUNT THE BOSS === START")
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NOT PROCESS_PRE_GAME(missionScriptArgs)
			SCRIPT_CLEANUP()
		ENDIF	
	ENDIF
	
	WHILE (TRUE)
	
		MP_LOOP_WAIT_ZERO()
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			CPRINTLN(DEBUG_NET_AMBIENT, "=== HUNT THE BOSS === SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")
			CPRINTLN(dbChan, "[GB][HTB] - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")
			SCRIPT_CLEANUP()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		MAINTAIN_DEBUG()
		#ENDIF
		
		IF GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION()
			CPRINTLN(DEBUG_NET_AMBIENT, "=== HUNT THE BOSS === GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION")
			CPRINTLN(dbChan, "[GB][HTB] - GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION")
			SCRIPT_CLEANUP()
		ENDIF
		
		SWITCH(GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT()))
		
			CASE GAME_STATE_INIT
			
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					SET_CLIENT_MISSION_STATE(GAME_STATE_RUNNING)
					CLIENT_ONE_FRAME_INIT()
				ENDIF
			BREAK
			
			CASE GAME_STATE_RUNNING
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					CLIENT_PROCESSING()

				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					SET_CLIENT_MISSION_STATE(GAME_STATE_END)
				ENDIF
			BREAK
			
			CASE GAME_STATE_END
				SCRIPT_CLEANUP()
			BREAK
			
		ENDSWITCH
	
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
			IF GET_SERVER_MISSION_STATE() != GAME_STATE_END
			AND GB_SHOULD_KILL_ACTIVE_BOSS_MISSION() 
				CPRINTLN(dbChan, "[GB][INFIGHT] - PROCESS_SERVER - GB_SHOULD_KILL_ACTIVE_BOSS_MISSION = TRUE, move to cleanup immediately.")
				SET_SERVER_MISSION_STATE(GAME_STATE_END)
			ENDIF
		
			SWITCH(GET_SERVER_MISSION_STATE())
			
				CASE GAME_STATE_INIT
					IF INIT_SERVER()
						SET_SERVER_MISSION_STATE(GAME_STATE_RUNNING)
					ENDIF
				BREAK
				
				CASE GAME_STATE_RUNNING
					IF SERVER_PROCESSING()
						SET_SERVER_MISSION_STATE(GAME_STATE_END)
					ENDIF
				BREAK
				
				CASE GAME_STATE_END
					SCRIPT_CLEANUP()
				BREAK
				
			ENDSWITCH
		
		ENDIF
		
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
