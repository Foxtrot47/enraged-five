//////////////////////////////////////////////////////////////////////////////////////////
// Name:        GB_FindersKeepers.sc													//
// Description:  Boss launches the challenge and a random location is picked - e.g.		//
// Grapeseed Downtown, El Burro, etc. This is represented by an area blip on the radar.	//
// A package is created at a random hidden position within the selected location,		//
// and the area it could be in is blipped on the radar for the Boss and Goons. 			//
// The first to find it within the Challenge time limit is the winner.					//
// Written by:  Kevin Wong																//
// Date: 6/10/2015																		//
//////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"



//----------------------
//	INCLUDES
//----------------------
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "flow_public_core_override.sch"
USING "net_wait_zero.sch"
USING "net_mission.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_gang_boss.sch"
USING  "AM_Common_UI.sch"  
#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF
USING "net_gang_boss_spectate.sch"
USING "DM_Leaderboard.sch"


//----------------------
//	Local Bitset
//----------------------

CONST_INT GEN_BS_DUMMY	0
CONST_INT MAX_NUM_EXTRA_PEDS	3




//----------------------
//	VARIABLES
//----------------------


CONST_INT START_NODE		20
CONST_INT NUM_PACKAGES		20

// Record Player Boss at start
PLAYER_INDEX initial_player_boss

//----------------------
//	BLIPS
//----------------------
BLIP_INDEX PackageBlipArea
BLIP_INDEX PackageBlipAreaLongRange
BLIP_INDEX PackageBlip
//----------------------
//	GAME STATE
//----------------------

CONST_INT GAME_STATE_INIT 									0
CONST_INT GAME_STATE_RUNNING								1
CONST_INT GAME_STATE_TERMINATE_DELAY						2
CONST_INT GAME_STATE_END									3

ENUM GEN_RUN_STAGE
	GEN_START = 0,
	GEN_IN_PACKAGE_AREA = 1,
	GEN_SEARCH_FOR_PACKAGE_IN_AREA = 2
ENDENUM

ENUM FINDERS_KEEPERS_STAGE_ENUM
	//eAD_IDLE,
	eAD_START,
	eAD_GETTOPACKAGE,
	eAD_CLEANUP
ENDENUM

ENUM MAP_AREA_ZONE
	KORTZCENTRE = 0,		
	STRAWBERRYCENTRE,	
	PACIFICBLUFFSCENTRE,
	GRAPESEEDCENTRE,	
	SAWMILLCENTRE,		
	VINEWOODLAKECENTRE,	
	REDWOODCENTRE,		
	ROUTE68CENTRE,		
	WESTVINEWOODCENTRE,	
	LEGIONSQUARECENTRE,	//9
	GENERAL
ENDENUM

ENUM SERVER_ENDING_STATES
	STILL_PLAYING = 0,		
	ALL_PACKAGES_COLLECTED,
	TIME_EXPIRES
	
ENDENUM
//----------------------
//	BROADCAST VARIABLES
//----------------------

// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData
	INT iServerGameState = GAME_STATE_INIT
	INT iServerBitSet
	INT ipackagecounter	= 0
	VECTOR vPackageLocation[NUM_PACKAGES+1]
	VECTOR vareablipcoordinates
	BOOL has_package_been_pickedup
	SCRIPT_TIMER soundpackagetonetimer
	SCRIPT_TIMER Starttimer
	PLAYER_INDEX piGangBoss
	BOOL brandomcoordinatesgenerated = FALSE
	INT iServerSyncId = 0
	MAP_AREA_ZONE MapArea
	GEN_RUN_STAGE eGEN_Stage = GEN_START
	FINDERS_KEEPERS_STAGE_ENUM eFindersKeepersStage = eAD_START
	AMBIENT_ACTIVITY_LEADERBOARD_STRUCT sSortedPackagesCollected[GB_MAX_GANG_SIZE_INCLUDING_BOSS]
	SERVER_ENDING_STATES eServer_ending_stat =  STILL_PLAYING
	
	INT iMatchId1
	INT iMatchId2
	

ENDSTRUCT


SCRIPT_TIMER helptimer

ServerBroadcastData serverBD
GB_MAINTAIN_SPECTATE_VARS sSpecVars
GANG_BOSS_MANAGE_REWARDS_DATA sRewardsData


CONST_INT biP_GangIsReady 					0
CONST_INT biP_GangNearpackage				1

PICKUP_INDEX the_package
OBJECT_INDEX objFlare
PTFX_ID flare_ptfx
PTFX_ID flare_smoke_ptfx



//----------------------
//	STRUCT
//----------------------
// The server broadcast data.
// Everyone can read this data, only the server can update it


BOOL bcreatepackageareablip[NUM_PACKAGES+1]
BOOL bhas_the_package_been_created = FALSE
//INT iTargetLoop
BOOL bHasdisplayedfinalshard = FALSE
INT  iSoundfrompackage = GET_SOUND_ID()
BOOL bdisplay_blip_near_end_of_expire_time
BOOL bAddBlipforpackage
BOOL bdisplay_final_message = FALSE
//BOOL bhasfired1minutefireworks
SCRIPT_TIMER fireworkstimer 


SCRIPT_TIMER rumbletimer
CONST_FLOAT cfHTB_MIN_RUMBLE_DISTANCE 		5.0

CONST_INT ciHTB_MIN_SHAKE_VALUE 			25 // Any lower is actually ultra-high frequency for some reason?
CONST_INT ciHTB_MAX_SHAKE_VALUE	 			250
INT ciHTB_RUMBLE_DURATION 					= 1000
INT ciHTB_RUMBLE_DURATION_MULTIPLIER 		= 145
INT bRUMBLE_HELP1 							= 0
CONST_INT ciHTB_TRIGGER_RADIUS				250
CONST_INT ciHTB_BLIP_ALPHA_INSIDE			70

INT ilastgenerated_location_number	= -1
INT ilastgenerated_location_number2 = -1
INT ilastgenerated_location_number3 = -1
INT ilastgenerated_location_number4 = -1
INT initialisemusic
BOOL display_pickupmoney_help
INT iBlipAlphaCached
INT serverStaggeredPlayerLoop
BOOL bSet
INT irumbleduration = 5000
SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
GB_STRUCT_BOSS_END_UI BOSS_END_UI
// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	INT iClientGameState = GAME_STATE_INIT
	int iClientBitSet
	GEN_RUN_STAGE eGEN_Stage = GEN_START
	FINDERS_KEEPERS_STAGE_ENUM eFindersStage = eAD_GETTOPACKAGE
	BOOL this_player_has_picked_up_package
	INT iclientscore = 0
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]


FUNC PLAYER_INDEX GET_GANG_BOSS_PLAYER_INDEX()
	RETURN serverBD.piGangBoss
ENDFUNC

//Helper function to get a clients game/mission state
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iClientGameState
ENDFUNC

/// PURPOSE:
///    Start countdown timer amount.
FUNC INT GET_CHALLENGE_EXPIRE_TIME()
	RETURN g_sMPTunables.igb_finders_keepers_time_limit                                      
	// g_sMPTunables.iDestroy_Vehicle_Expiry_Time iChallenge_Event_Start_Countdown_Time
ENDFUNC


FUNC BOOL IS_GAME_A_DRAW()
	INT i
	REPEAT GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS() i
		IF i != 0
			//IF serverBD.sSortedPackagesCollected[0].iScore != 0
				IF serverBD.sSortedPackagesCollected[0].iScore = serverBD.sSortedPackagesCollected[i].iScore 
					PRINTLN("FINDERS KEEPERS IS_GAME_A_DRAW = TRUE serverBD.sSortedPackagesCollected[0].iScore = ", serverBD.sSortedPackagesCollected[0].iScore )
					PRINTLN("FINDERS KEEPERS IS_GAME_A_DRAW = TRUE serverBD.sSortedPackagesCollected[i].iScore = ", serverBD.sSortedPackagesCollected[i].iScore )
					RETURN TRUE
				ENDIF
			//ENDIF
		ELSE
			IF serverBD.sSortedPackagesCollected[1].iScore = serverBD.sSortedPackagesCollected[i].iScore 
				PRINTLN("FINDERS KEEPERS IS_GAME_A_DRAW 1 = TRUE serverBD.sSortedPackagesCollected[0].iScore = ", serverBD.sSortedPackagesCollected[0].iScore )
				PRINTLN("FINDERS KEEPERS IS_GAME_A_DRAW 1 = TRUE serverBD.sSortedPackagesCollected[i].iScore = ", serverBD.sSortedPackagesCollected[i].iScore )
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	PRINTLN("FINDERS KEEPERS IS_GAME_A_DRAW = FALSE serverBD.sSortedPackagesCollected[0].iScore = ", serverBD.sSortedPackagesCollected[0].iScore )
	RETURN FALSE
ENDFUNC


//----------------------
//	DBG FUNCTIONS
//----------------------

PROC CREATE_PACKAGE()
	IF NOT bhas_the_package_been_created
		IF NOT DOES_PICKUP_EXIST(the_package)
			INT iPlacementFlags = 0
			//INT r, g, b, a
			//VECTOR safecoords
			SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
			SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
			SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_MAP))
			SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_PLAYER_GIFT))
			
			SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_LOCAL_ONLY))
			SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
			
		 	//safecoords = GET_SAFE_PICKUP_COORDS(serverBD.vPackageLocation)
			the_package = CREATE_PICKUP(PICKUP_MONEY_SECURITY_CASE, serverBD.vPackageLocation[serverBD.ipackagecounter],  iPlacementFlags ,g_sMPTunables.igb_finders_keepers_bonus_cash_max_packages, FALSE)
			
			objFlare = create_object(PROP_FLARE_01, GET_OFFSET_FROM_COORD_IN_WORLD_COORDS(serverBD.vPackageLocation[serverBD.ipackagecounter], 0.0,<<0.0, 0.0, 1.0>>), FALSE, FALSE)
			flare_ptfx = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_finders_flare", objFlare, <<0.07, 0.0, 0.11>>, <<0.0, 0.0, 0.0>>) 
			
			flare_smoke_ptfx = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_finders_package_flare", objFlare, <<0.07, 0.0, 0.11>>, <<0.0, 0.0, 0.0>>, g_sMPTunables.fgb_finders_keepers_smoke_size_modifier) 
			//PLAY_SOUND_FROM_COORD(iSoundfrompackage, "Tone", serverBD.vPackageLocation, "LIFT_NORMAL_SOUNDSET", FALSE, 0, TRUE) 
			//CONTINUAL_BEEP
		//	PLAY_SOUND_FROM_COORD(iSoundfrompackage,spackagesound, serverBD.vPackageLocation[serverBD.ipackagecounter], "EPSILONISM_04_SOUNDSET", FALSE, 0, TRUE) 
			PLAY_SOUND_FROM_COORD(iSoundfrompackage, "Case_Beep", serverBD.vPackageLocation[serverBD.ipackagecounter], "GTAO_Magnate_Finders_Keepers_Soundset", FALSE, 0, TRUE) 
			//IF DOES_PARTICLE_FX_LOOPED_EXIST(flare_Smoke_ptfx)
				//GET_HUD_COLOUR(HUD_COLOUR_RED, r, g, b, a)
			//	SET_PARTICLE_FX_LOOPED_COLOUR(flare_Smoke_ptfx, TO_FLOAT(r)/255.0, TO_FLOAT(g)/255.0, TO_FLOAT(b)/255.0, TRUE ) 
			//ENDIF
			
			PRINTLN("     ---------->     FINDERS KEEPERS - CREATE_PACKAGE - serverBD.vPackageLocation[serverBD.ipackagecounter] = ",serverBD.vPackageLocation[serverBD.ipackagecounter])
			bhas_the_package_been_created = TRUE
			IF NOT g_sMPTunables.bgb_finders_keepers_disable_trackify                                
				ENABLE_MULTIPLAYER_TRACKIFY_APP(TRUE)
				//SET_TRACKIFY_TARGET_VECTOR ( serverBD.vPackageLocation[serverBD.ipackagecounter])
				SET_NUMBER_OF_MULTIPLE_TRACKIFY_TARGETS(1)
				SET_TRACKIFY_MULTIPLE_TARGET_VECTOR (0, serverBD.vPackageLocation[serverBD.ipackagecounter]) //Senora National Park.
			ENDIF
			gfinderskeeperslocalplayerhascollectedpickup = FALSE
			RESET_NET_TIMER(fireworkstimer)
			RESET_NET_TIMER(helptimer)
			irumbleduration = 5000
			
			INT i
			REPEAT GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS() i
				g_GBLeaderboardStruct.challengeLbdStruct[i].playerID = serverBD.sSortedPackagesCollected[i].playerID
				g_GBLeaderboardStruct.challengeLbdStruct[i].iScore = serverBD.sSortedPackagesCollected[i].iScore
				g_GBLeaderboardStruct.challengeLbdStruct[i].iFinishTime = -1
			ENDREPEAT
			//bhasfired1minutefireworks = FALSE
		ENDIF
	ENDIF
ENDPROC

PROC DELETE_PACKAGE()

	IF DOES_PICKUP_EXIST(the_package)
		REMOVE_PICKUP(the_package)
	ENDIF

	IF DOES_ENTITY_EXIST(objFlare)
		DELETE_OBJECT(objFlare)
	ENDIF
	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(flare_ptfx)
		STOP_PARTICLE_FX_LOOPED(flare_ptfx)
	endif 
	
	IF  DOES_PARTICLE_FX_LOOPED_EXIST(flare_Smoke_ptfx)
		STOP_PARTICLE_FX_LOOPED(flare_Smoke_ptfx)
	ENDIF
	
	IF NOT HAS_SOUND_FINISHED(iSoundfrompackage)
		STOP_SOUND(iSoundfrompackage)
	ENDIF
	
	//REMOVE_TRACKIFY_MULTIPLE_TARGET(iTargetLoop)
	IF serverBD.eServer_ending_stat = TIME_EXPIRES
		ADD_EXPLOSION(serverBD.vPackageLocation[serverBD.ipackagecounter], EXP_TAG_ROCKET, 1.0, TRUE, FALSE, 0.0)
	
	ENDIF
	IF NOT g_sMPTunables.bgb_finders_keepers_disable_trackify
		REMOVE_TRACKIFY_MULTIPLE_TARGET(1)
		ENABLE_MULTIPLAYER_TRACKIFY_APP(FALSE)
	ENDIF
	
	//RELEASE_SOUND_ID(iSoundfrompackage)
ENDPROC

FUNC BOOL IS_THIS_PLAYER_A_GANG_GOON(PLAYER_INDEX thisPlayer)
	RETURN GB_IS_PLAYER_MEMBER_OF_THIS_GANG(thisPlayer, GB_GET_LOCAL_PLAYER_GANG_BOSS(), FALSE)
ENDFUNC


FUNC VECTOR GET_AREA_BLIP_OFFSET(VECTOR vlocation)

	FLOAT irandom_x
	FLOAT irandom_y
	FLOAT irandom_z
	
	irandom_x = GET_RANDOM_FLOAT_IN_RANGE(-120,120)
	irandom_y = GET_RANDOM_FLOAT_IN_RANGE(-120,120)
	irandom_z = GET_RANDOM_FLOAT_IN_RANGE(0.0,2.0)

	#IF IS_DEBUG_BUILD
		VECTOR tempvect = GET_OFFSET_FROM_COORD_IN_WORLD_COORDS(vLocation,0.0,<<irandom_x, irandom_y, irandom_z>>)
		PRINTLN("     ---------->     FINDERS KEEPERS - GET_AREA_BLIP_OFFSET - vLocation  ",tempvect)//, GET_OFFSET_FROM_COORD_IN_WORLD_COORDS(vLocation,0.0,<<irandom_x, irandom_y, irandom_z>>))
	#ENDIF
	RETURN GET_OFFSET_FROM_COORD_IN_WORLD_COORDS(vLocation,0.0,<<irandom_x, irandom_y, irandom_z>>)

ENDFUNC


PROC ADD_FINDERS_KEEPERS_BLIP()

	IF NOT DOES_BLIP_EXIST(PackageBlipAreaLongRange)
		PackageBlipAreaLongRange = ADD_BLIP_FOR_COORD(serverBD.vareablipcoordinates)
		
		SET_BLIP_AS_SHORT_RANGE(PackageBlipAreaLongRange, FALSE)
		SET_BLIP_DISPLAY(PackageBlipAreaLongRange, DISPLAY_RADAR_ONLY) 
		//GET_OFFSET_FROM_COORD_IN_WORLD_COORDS(serverBD.vPackageLocation, 0.0,<<GET_RANDOM_FLOAT_IN_RANGE(-50,50), GET_RANDOM_FLOAT_IN_RANGE(-50,50), 0.0>>)
		SET_BLIP_ROUTE(PackageBlipAreaLongRange, TRUE) 
		SET_BLIP_SPRITE(PackageBlipAreaLongRange, RADAR_TRACE_FINDERS_KEEPERS) 
		SET_BLIP_COLOUR_FROM_HUD_COLOUR(PackageBlipAreaLongRange, HUD_COLOUR_YELLOW)
		SET_BLIP_PRIORITY(PackageBlipAreaLongRange, BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
		SET_BLIP_SCALE(PackageBlipAreaLongRange,g_sMPTunables.fgangboss_Job_blip_scale  )
		 MPGlobalsAmbience.vQuickGPS = serverBD.vareablipcoordinates
		MPGlobalsAmbience.bIsGPSDropOff = TRUE


		PRINTLN("     ---------->     FINDERS KEEPERS - ADD_PACKAGE_AREA_BLIP - serverBD.vPackageLocation[serverBD.ipackagecounter] = ", serverBD.vPackageLocation[serverBD.ipackagecounter])
	ENDIF

ENDPROC

/// PURPOSE: Adds a blip at the packages's location
PROC ADD_PACKAGE_AREA_BLIP()
	
	playerBD[PARTICIPANT_ID_TO_INT()].this_player_has_picked_up_package  = FALSE
	Delete_MP_Objective_Text()
	//bRUMBLE_HELP1 = 0
	//serverBD.vareablipcoordinates = GET_AREA_BLIP_OFFSET(serverBD.vPackageLocation[serverBD.ipackagecounter])
	bhas_the_package_been_created = false 	
	
	IF NOT DOES_BLIP_EXIST(PackageBlipArea)
		bcreatepackageareablip[serverBD.ipackagecounter] = TRUE
		PackageBlipArea = ADD_BLIP_FOR_RADIUS(serverBD.vareablipcoordinates, TO_FLOAT( g_sMPTunables.igb_finders_keepers_area_radius )      )
		SET_BLIP_COLOUR_FROM_HUD_COLOUR(PackageBlipArea, HUD_COLOUR_YELLOW)
		//GET_OFFSET_FROM_COORD_IN_WORLD_COORDS(serverBD.vPackageLocation, 0.0,<<GET_RANDOM_FLOAT_IN_RANGE(-50,50), GET_RANDOM_FLOAT_IN_RANGE(-50,50), 0.0>>)
		SET_BLIP_ALPHA(PackageBlipArea, g_sMPTunables.iblip_area_alpha )
		PRINTLN("     ---------->     FINDERS KEEPERS - ADD_PACKAGE_AREA_BLIP - serverBD.vPackageLocation = ", serverBD.vPackageLocation[serverBD.ipackagecounter])
		PRINTLN("     ---------->     FINDERS KEEPERS - ADD_PACKAGE_AREA_BLIP - serverBD.vareablipcoordinates = ", serverBD.vareablipcoordinates)

		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			serverBD.has_package_been_pickedup = FALSE
		ENDIF
	ENDIF
	
	ADD_FINDERS_KEEPERS_BLIP()
	
	IF serverBD.ipackagecounter != 0
		IF serverBD.ipackagecounter != NUM_PACKAGES
			PRINT_TICKER("GB_FDKP_CREPAC")
		ELSE
			PRINT_TICKER("GB_FDKP_CREPAC2")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Removes the package area blip
PROC REMOVE_PACKAGE_AREA_BLIP()
	IF DOES_BLIP_EXIST(PackageBlipArea)
		REMOVE_BLIP(PackageBlipArea)
		PRINTLN("     ---------->     FINDERS KEEPERS - REMOVE_PACKAGE_AREA_BLIP - serverBD.vPackageLocation = ", serverBD.vPackageLocation[serverBD.ipackagecounter])
	ENDIF
ENDPROC

PROC REMOVE_PACKAGE_AREA_BLIP_LONG_RANG_BLIP()
	IF DOES_BLIP_EXIST(PackageBlipAreaLongRange)
	    MPGlobalsAmbience.vQuickGPS =  <<0,0,0>> 
		MPGlobalsAmbience.bIsGPSDropOff = FALSE
		REMOVE_BLIP(PackageBlipAreaLongRange)
		PRINTLN("     ---------->     FINDERS KEEPERS - REMOVE_PACKAGE_AREA_BLIP_LONG_RANG_BLIP - serverBD.vPackageLocation = ", serverBD.vPackageLocation[serverBD.ipackagecounter])
	ENDIF
ENDPROC

/// PURPOSE: Adds a blip at the packages's location
PROC ADD_PACKAGE_BLIP()
	IF NOT DOES_BLIP_EXIST(PackageBlip)
		IF DOES_PICKUP_EXIST(the_package)
			PackageBlip = ADD_BLIP_FOR_PICKUP(the_package)
			SET_BLIP_COLOUR_FROM_HUD_COLOUR(PackageBlip, HUD_COLOUR_YELLOW)
			//SET_BLIP_ALPHA(PackageBlipArea, 200)
			PRINTLN("     ---------->     FINDERS KEEPERS - ADD_PACKAGE_BLIP - serverBD.vPackageLocation = ", serverBD.vPackageLocation[serverBD.ipackagecounter])
			SET_BLIP_SCALE(PackageBlip, GB_BLIP_SIZE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Removes the package area blip
PROC REMOVE_PACKAGE_BLIP()
	IF DOES_BLIP_EXIST(PackageBlip)
		REMOVE_BLIP(PackageBlip)
	ENDIF
ENDPROC

FUNC INT GET_AREA_BLIP_ALPHA(PLAYER_INDEX piGangBoss)

	IF piGangBoss != INVALID_PLAYER_INDEX()
		IF NOT IS_VECTOR_ZERO(serverBD.vareablipcoordinates)
			FLOAT fDistance = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(GET_PLAYER_PED(piGangBoss), serverBD.vareablipcoordinates, FALSE)
			IF (fDistance < ((ciHTB_TRIGGER_RADIUS - 2) + GB_GET_BOSS_PROXIMITY_BONUS_RADIUS()))
				RETURN ciHTB_BLIP_ALPHA_INSIDE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN g_sMPTunables.iblip_area_alpha

ENDFUNC


/// PURPOSE:
///    Maintain the pad rumble intensity based on distance.
///    Further away = lower intensity.
PROC MAINTAIN_RUMBLE()

	//IF NOT GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PLAYER_ID(), GET_GANG_BOSS_PLAYER_INDEX(), FALSE)
	//	EXIT
	//ENDIF
	
	VECTOR vPlayerPos = GET_ENTITY_COORDS(GET_PLAYER_PED(PLAYER_ID()), FALSE)
	FLOAT fDistance = VDIST(serverBD.vPackageLocation[serverBD.ipackagecounter], vPlayerPos)
	//FLOAT fDelta, fIntensity
	
	
	
		IF (ROUND(fDistance) < g_sMPTunables.igb_finders_keepers_rumble_radius)
			
			IF bRUMBLE_HELP1 = 2
				IF GET_PROFILE_SETTING (PROFILE_CONTROLLER_VIBRATION) != 0   //Vibration set to off in frontend 
					IF NOT IS_HELP_MESSAGE_ON_SCREEN()
						 bRUMBLE_HELP1 = 3
						// PRINT_HELP("GB_FDKP_RUMH1")
					ENDIF
				ELSE
					bRUMBLE_HELP1 = 3
				ENDIF
			ENDIF
			
			 
			IF fDistance < cfHTB_MIN_RUMBLE_DISTANCE
				//fIntensity = ciHTB_MAX_SHAKE_VALUE
				irumbleduration = ciHTB_RUMBLE_DURATION 
			ELSE
				//fDelta = 1.0 - (CLAMP(fDistance, cfHTB_MIN_RUMBLE_DISTANCE, TO_FLOAT(g_sMPTunables.igb_finders_keepers_rumble_radius)) / TO_FLOAT(g_sMPTunables.igb_finders_keepers_rumble_radius))
				//fIntensity = LERP_FLOAT(ciHTB_MIN_SHAKE_VALUE, ciHTB_MAX_SHAKE_VALUE, fDelta)
				
				irumbleduration = ROUND(fDistance)*ciHTB_RUMBLE_DURATION_MULTIPLIER
				//PRINTLN("FINDERS KEEPERS -  - MAINTAIN_RUMBLE - PlayerDistance: ", fDistance, ", irumbleduration ", irumbleduration)
			ENDIF
			
			
			IF HAS_NET_TIMER_EXPIRED(rumbletimer,irumbleduration)	
				RESET_NET_TIMER(rumbletimer)
				//SET_CONTROL_SHAKE(PLAYER_CONTROL, ciHTB_RUMBLE_DURATION, ROUND(fIntensity))
			ENDIF
			
			#IF IS_DEBUG_BUILD
			IF (GET_FRAME_COUNT() % 60) = 0
				//PRINTLN("FINDERS KEEPERS -  - MAINTAIN_RUMBLE - PlayerDistance: ", fDistance, ", fIntensity: ", ROUND(fIntensity))
			ENDIF
			#ENDIF
		ENDIF
	
	
ENDPROC

/// PURPOSE:
///    rumbles harder when player gets close to package
PROC MAINTAIN_PACKAGE_CLIENT_BLIPS_AND_RUMBLE()

	IF DOES_PICKUP_EXIST(the_package)
	//ENABLE_MULTIPLAYER_TRACKIFY_APP(TRUE)
		MAINTAIN_RUMBLE()
	ENDIF
	
	IF bRUMBLE_HELP1 = 0
	AND HAS_NET_TIMER_EXPIRED(helptimer,5000)
		IF NOT IS_HELP_MESSAGE_ON_SCREEN()
		AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			RESET_NET_TIMER(helptimer)
			 bRUMBLE_HELP1 = 1
			 PRINT_HELP("GB_FDKP_HELP1b")
		ENDIF
	ENDIF
	
	
	IF bRUMBLE_HELP1 = 1
	AND NOT IS_HELP_MESSAGE_ON_SCREEN()
	AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		IF HAS_NET_TIMER_EXPIRED(helptimer,10000)
			 bRUMBLE_HELP1 = 2
			IF g_sMPTunables.bgb_finders_keepers_disable_trackify  = FALSE
				 PRINT_HELP("GB_FDKP_HELP1c")
			ENDIF
			 REQUEST_DYNAMIC_UPDATE_OF_CELLPHONE_APPLIST()
		ENDIF
	ENDIF

	IF HAS_SOUND_FINISHED(iSoundfrompackage)
		IF HAS_NET_TIMER_EXPIRED(serverBD.soundpackagetonetimer,irumbleduration)
			FLOAT fDistance = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), serverBD.vPackageLocation[serverBD.ipackagecounter], FALSE)
			IF g_sMPTunables.igb_finders_keepers_sfx_radius > ROUND(fDistance    )                                 
				RESET_NET_TIMER(serverBD.soundpackagetonetimer)
				PLAY_SOUND_FROM_COORD(iSoundfrompackage, "Case_Beep", serverBD.vPackageLocation[serverBD.ipackagecounter], "GTAO_Magnate_Finders_Keepers_Soundset", FALSE, 0, TRUE) 
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(objFlare)
		IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(flare_ptfx)
			flare_ptfx = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_finders_flare", objFlare, <<0.07, 0.0, 0.11>>, <<0.0, 0.0, 0.0>>) 
		ENDIF
		
		IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(flare_Smoke_ptfx)
			flare_Smoke_ptfx = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_finders_package_flare", objFlare, <<0.07, 0.0, 0.11>>, <<0.0, 0.0, 0.0>>, g_sMPTunables.fgb_finders_keepers_smoke_size_modifier) 
			//SET_PARTICLE_FX_LOOPED_COLOUR(flare_Smoke_ptfx, 185.0, 51.0, 51.0, TRUE ) 
		
		ENDIF
		
		/*IF DOES_PARTICLE_FX_LOOPED_EXIST(flare_Smoke_ptfx)
			INT r, g, b, a
			GET_HUD_COLOUR(HUD_COLOUR_RED, r, g, b, a)
			SET_PARTICLE_FX_LOOPED_COLOUR(flare_Smoke_ptfx, TO_FLOAT(r)/255.0, TO_FLOAT(g)/255.0, TO_FLOAT(b)/255.0, TRUE ) 
		ENDIF
			*/
	ENDIF
	
	
	IF bdisplay_blip_near_end_of_expire_time = FALSE
		//IF HAS_NET_TIMER_EXPIRED(serverBD.Starttimer, timertoshowpackageblip)
		IF bAddBlipforpackage = TRUE
			REMOVE_PACKAGE_AREA_BLIP()
			REMOVE_PACKAGE_AREA_BLIP_LONG_RANG_BLIP()
			ADD_PACKAGE_BLIP()
			bdisplay_blip_near_end_of_expire_time = TRUE
			PRINT_HELP_NO_SOUND("GB_FDKP_HELP2")
			GB_SET_GANG_BOSS_HELP_BACKGROUND()
			PRINTLN("     ---------->     FINDERS KEEPERS - MAINTAIN_PACKAGE_CLIENT_BLIPS_AND_RUMBLE Net timer expired")
		ENDIF
	ENDIF
	
	IF DOES_PICKUP_EXIST(the_package)
		//IF bhasfired1minutefireworks = FALSE
			IF HAS_NET_TIMER_EXPIRED(fireworkstimer, g_sMPTunables.igb_finders_keepers_time_before_flare                               )
			OR bAddBlipforpackage = TRUE
				IF HAS_NAMED_PTFX_ASSET_LOADED("scr_indep_fireworks")
					USE_PARTICLE_FX_ASSET("scr_indep_fireworks")
					START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_indep_firework_starburst", serverBD.vPackageLocation[serverBD.ipackagecounter],<<0.0, 0.0, 0.0>>) 
					
					//INT r, g, b, a
					//GET_HUD_COLOUR(HUD_COLOUR_RED, r, g, b, a)
					//SET_PARTICLE_FX_NON_LOOPED_COLOUR(TO_FLOAT(r)/255.0, TO_FLOAT(g)/255.0, TO_FLOAT(b)/255.0 ) 
					//bhasfired1minutefireworks = TRUE
					bAddBlipforpackage= FALSE
					RESET_NET_TIMER(fireworkstimer)
				ELSE
					REQUEST_NAMED_PTFX_ASSET("scr_indep_fireworks")
				ENDIF
				
			ENDIF
		//ENDIF
	ENDIF

ENDPROC

// ---- Event Handling -----

/// PURPOSE:
///    Sort the players when a new score has been detected
PROC SORT_FINDERS_KEEPERS_PLAYER_SCORES(PLAYER_INDEX playerID, INT iPlayerScore)
	SORT_AMBIENT_ACTIVITY_LEADERBOARD(serverBD.sSortedPackagesCollected, playerID, iPlayerScore, AAL_SORT_DESCENDING)

	serverBD.iServerSyncId++
ENDPROC

/// PURPOSE:
///    
PROC MAINTAIN_FINDERS_KEEPERS_PLAYER_LEFT()
	IF MAINTAIN_PLAYER_LEAVING_AMBIENT_ACTIVITY_LEADERBOARD(serverBD.sSortedPackagesCollected, serverStaggeredPlayerLoop, AAL_SORT_DESCENDING)
		serverBD.iServerSyncId++
	ENDIF
ENDPROC

FUNC MAP_AREA_ZONE GET_PLAYERS_CURRENT_AREA()

	VECTOR	VKortzCentre			=  <<-2268.8484, 293.1660, 173.6019>>
	VECTOR	VStrawberryCentre		=  <<473.4940, -1271.3734, 28.6789>>
	VECTOR	VPacificBluffsCentre	=  <<-1633.5690, -291.0810, 50.6773>>
	VECTOR	VGrapeseedCentre		=  <<1771.2610, 4812.0752, 38.9597>>
	VECTOR	VSawmillCentre			=  <<-656.8970, 5316.9292, 59.2326>>
	VECTOR	VVinewoodLakeCentre		=  <<-101.9560, 869.2678, 235.0167>>
	VECTOR	VRedwoodCentre			=  <<1033.7780, 2334.2354, 48.6716>>
	VECTOR	VRoute68Centre			=  <<310.6670, 2708.7471, 56.3720>>
	VECTOR	VWestVinewoodCentre		=  <<-455.0913, 421.4182, 106.0092>>
	VECTOR	VLegionSquareCentre		=  <<191.0690, -925.8549, 29.6918>>

	FLOAT distancefromVKortzCentre				= VDIST(GET_ENTITY_COORDS(GET_PLAYER_PED( GB_GET_LOCAL_PLAYER_GANG_BOSS())), VKortzCentre)				
	FLOAT distancefromVStrawberryCentre			= VDIST(GET_ENTITY_COORDS(GET_PLAYER_PED( GB_GET_LOCAL_PLAYER_GANG_BOSS())), VStrawberryCentre		)
	FLOAT distancefromVPacificBluffsCentre      = VDIST(GET_ENTITY_COORDS(GET_PLAYER_PED( GB_GET_LOCAL_PLAYER_GANG_BOSS())), VPacificBluffsCentre   )
	FLOAT distancefromVGrapeseedCentre	        = VDIST(GET_ENTITY_COORDS(GET_PLAYER_PED( GB_GET_LOCAL_PLAYER_GANG_BOSS())), VGrapeseedCentre	    )
	FLOAT distancefromVSawmillCentre		    = VDIST(GET_ENTITY_COORDS(GET_PLAYER_PED( GB_GET_LOCAL_PLAYER_GANG_BOSS())), VSawmillCentre		    )
	FLOAT distancefromVVinewoodLakeCentre	    = VDIST(GET_ENTITY_COORDS(GET_PLAYER_PED( GB_GET_LOCAL_PLAYER_GANG_BOSS())), VVinewoodLakeCentre	)
	FLOAT distancefromVRedwoodCentre		    = VDIST(GET_ENTITY_COORDS(GET_PLAYER_PED( GB_GET_LOCAL_PLAYER_GANG_BOSS())), VRedwoodCentre		    )
	FLOAT distancefromVRoute68Centre		    = VDIST(GET_ENTITY_COORDS(GET_PLAYER_PED( GB_GET_LOCAL_PLAYER_GANG_BOSS())), VRoute68Centre		    )
	FLOAT distancefromVWestVinewoodCentre	    = VDIST(GET_ENTITY_COORDS(GET_PLAYER_PED( GB_GET_LOCAL_PLAYER_GANG_BOSS())), VWestVinewoodCentre	)
	FLOAT distancefromVLegionSquareCentre	    = VDIST(GET_ENTITY_COORDS(GET_PLAYER_PED( GB_GET_LOCAL_PLAYER_GANG_BOSS())), VLegionSquareCentre	)
	
	
	PRINTLN("FINDERS KEEPERS GET_PLAYERS_CURRENT_AREA   distancefromVStrawberryCentre		= ", 	distancefromVStrawberryCentre		)
	PRINTLN("FINDERS KEEPERS GET_PLAYERS_CURRENT_AREA   distancefromVPacificBluffsCentre  =  ",     distancefromVPacificBluffsCentre    )
	PRINTLN("FINDERS KEEPERS GET_PLAYERS_CURRENT_AREA   distancefromVGrapeseedCentre	  =  ",     distancefromVGrapeseedCentre	    )
	PRINTLN("FINDERS KEEPERS GET_PLAYERS_CURRENT_AREA   distancefromVSawmillCentre		  =  ",     distancefromVSawmillCentre		    )
	PRINTLN("FINDERS KEEPERS GET_PLAYERS_CURRENT_AREA   distancefromVVinewoodLakeCentre	  =  ",     distancefromVVinewoodLakeCentre	    )
	PRINTLN("FINDERS KEEPERS GET_PLAYERS_CURRENT_AREA   distancefromVRedwoodCentre		  =  ",     distancefromVRedwoodCentre		    )
	PRINTLN("FINDERS KEEPERS GET_PLAYERS_CURRENT_AREA   distancefromVRoute68Centre		  =  ",     distancefromVRoute68Centre		    )
	PRINTLN("FINDERS KEEPERS GET_PLAYERS_CURRENT_AREA   distancefromVWestVinewoodCentre	  =  ",     distancefromVWestVinewoodCentre	    )
	PRINTLN("FINDERS KEEPERS GET_PLAYERS_CURRENT_AREA   distancefromVLegionSquareCentre	  =  ",     distancefromVLegionSquareCentre	    )
	
	MAP_AREA_ZONE closestmapzone = GENERAL
	
	FLOAT ftempclosestdistance = 100000.0
	
	IF distancefromVKortzCentre < ftempclosestdistance
		closestmapzone = KORTZCENTRE
		ftempclosestdistance =  distancefromVKortzCentre
	ENDIF
	
	IF distancefromVStrawberryCentre < ftempclosestdistance
		closestmapzone = STRAWBERRYCENTRE
		ftempclosestdistance =  distancefromVStrawberryCentre
	ENDIF
	
	IF distancefromVPacificBluffsCentre < ftempclosestdistance
		closestmapzone = PACIFICBLUFFSCENTRE
		ftempclosestdistance =  distancefromVPacificBluffsCentre
	ENDIF
	
	IF distancefromVGrapeseedCentre < ftempclosestdistance
		closestmapzone = GRAPESEEDCENTRE
		ftempclosestdistance =  distancefromVGrapeseedCentre
	ENDIF
	
	IF distancefromVSawmillCentre< ftempclosestdistance
		closestmapzone = SAWMILLCENTRE
		ftempclosestdistance =  distancefromVSawmillCentre
	ENDIF
	
	IF distancefromVVinewoodLakeCentre < ftempclosestdistance
		closestmapzone = VINEWOODLAKECENTRE
		ftempclosestdistance =  distancefromVVinewoodLakeCentre
	ENDIF
	
	IF distancefromVRedwoodCentre <ftempclosestdistance
		closestmapzone = RedwoodCentre
		ftempclosestdistance =  distancefromVRedwoodCentre
	ENDIF
	
	IF distancefromVRoute68Centre <ftempclosestdistance
		closestmapzone = Route68Centre
		ftempclosestdistance =  distancefromVRoute68Centre
	ENDIF
	
	IF distancefromVWestVinewoodCentre < ftempclosestdistance
		closestmapzone = WestVinewoodCentre
		ftempclosestdistance =  distancefromVWestVinewoodCentre
	ENDIF
	IF distancefromVLegionSquareCentre < ftempclosestdistance
		closestmapzone = LegionSquareCentre
		ftempclosestdistance =  distancefromVLegionSquareCentre
	ENDIF
	
	PRINTLN("FINDERS KEEPERS GET_PLAYERS_CURRENT_AREA closestmapzone = ", ENUM_TO_INT(closestmapzone))
	PRINTLN("FINDERS KEEPERS GET_PLAYERS_CURRENT_AREA ftempclosestdistance = ", ftempclosestdistance)
	RETURN closestmapzone

ENDFUNC

FUNC VECTOR GET_AREA_BLIP_COORDINATES(MAP_AREA_ZONE mapzone)

	SWITCH mapzone
		CASE KORTZCENTRE
			RETURN <<-2268.8484, 293.1660, 173.6019>> 			//Kortz
		BREAK
		
		CASE STRAWBERRYCENTRE
			RETURN <<473.4940, -1271.3734, 28.6789>> 
			
		BREAK		

		CASE PACIFICBLUFFSCENTRE
			
			RETURN  <<-1633.5690, -291.0810, 50.6773>>	
			
		BREAK	
		
		CASE GRAPESEEDCENTRE	
			RETURN  <<1771.2610, 4812.0752, 38.9597>> 
			
		BREAK			
		
		CASE SAWMILLCENTRE
			RETURN  <<-656.8970, 5316.9292, 59.2326>> 
			
		BREAK	
		
		CASE VINEWOODLAKECENTRE
			RETURN  <<-101.9560, 869.2678, 235.0167>> 		
			
		BREAK		
		
		CASE REDWOODCENTRE
			RETURN  <<1033.7780, 2334.2354, 48.6716>> 		
			
		BREAK			
				
				
		CASE ROUTE68CENTRE
			RETURN  <<310.6670, 2708.7471, 56.3720>> 
			
		BREAK			
		
		CASE WESTVINEWOODCENTRE
			
			RETURN  <<-455.0913, 421.4182, 106.0092>>	
			
		BREAK			
				
				
				
		CASE LEGIONSQUARECENTRE
			RETURN  <<191.0690, -925.8549, 29.6918>>
		BREAK		
	ENDSWITCH
	
	
	SCRIPT_ASSERT("GET_AREA_BLIP_COORDINATES INVALID ZONE PASSED IN.")
	
	RETURN <<0.0,0.0,0.0>>
ENDFUNC


FUNC INT GENERATE_RANDOM_INTEGER_UNIQUE_IN_FOUR( )
	INT iRandomLocation
	INT number_of_locations = 30
	
	

	iRandomLocation = GET_RANDOM_INT_IN_RANGE(0,number_of_locations)
	
	
	
	// So we dont get the same coords within 4 packages

	INT i
	REPEAT number_of_locations i
	
		WHILE  iRandomLocation = ilastgenerated_location_number
		OR iRandomLocation =  ilastgenerated_location_number2 
		OR iRandomLocation =   ilastgenerated_location_number3
		OR  iRandomLocation =  ilastgenerated_location_number4
			iRandomLocation = GET_RANDOM_INT_IN_RANGE(0,number_of_locations)
			PRINTLN("FINDERS KEEPERS GENERATE_PACKAGE_LOCATION GENERATE_RANDOM_INTEGER_UNIQUE_IN_FOUR 1 creating iRandomLocation as its the same as ilastgenerated_location_number")
			PRINTLN("FINDERS KEEPERS  ilastgenerated_location_number = ", ilastgenerated_location_number)
			PRINTLN("FINDERS KEEPERS  ilastgenerated_location_number2 = ", ilastgenerated_location_number2)
			PRINTLN("FINDERS KEEPERS  ilastgenerated_location_number3 = ", ilastgenerated_location_number3)
			PRINTLN("FINDERS KEEPERS  iRandomLocation = ", iRandomLocation)
		ENDWHILE
		
	ENDREPEAT
	

	
	ilastgenerated_location_number4 = ilastgenerated_location_number3
	ilastgenerated_location_number3 = ilastgenerated_location_number2
	ilastgenerated_location_number2 = ilastgenerated_location_number
	ilastgenerated_location_number = iRandomLocation
	PRINTLN(" FINDERS KEEPERS GENERATE_PACKAGE_LOCATION GENERATE_RANDOM_INTEGER_UNIQUE_IN_FOUR  final iRandomLocation = ", iRandomLocation)
	RETURN iRandomLocation
ENDFUNC

//PURPOSE: Get Random Location for Package
FUNC VECTOR GENERATE_PACKAGE_LOCATION( MAP_AREA_ZONE players_area, INT iRandomLocation = -1)


		IF iRandomLocation = -1
			iRandomLocation = GENERATE_RANDOM_INTEGER_UNIQUE_IN_FOUR()
		ENDIF
		
		serverBD.brandomcoordinatesgenerated = TRUE
			
		SWITCH players_area
			CASE KORTZCENTRE
				serverBD.vareablipcoordinates =  GET_AREA_BLIP_COORDINATES(KORTZCENTRE)			//Kortz
				SWITCH iRandomLocation
					CASE 0		RETURN  <<-2301.4080, 179.8100, 166.6017>>	BREAK	
					CASE 1		RETURN  <<-2340.2280, 211.0260, 165.6560>>	BREAK	
					CASE 2		RETURN  <<-2403.1521, 335.7170, 156.8542>>	BREAK	
					CASE 3		RETURN  <<-2341.1050, 404.6990, 174.6687>>	BREAK	
					CASE 4		RETURN  <<-2247.7791, 404.4730, 174.2944>>	BREAK	
					CASE 5		RETURN  <<-2179.8320, 263.8490, 167.6589>>	BREAK	
					CASE 6		RETURN  <<-2174.6899, 214.1540, 169.7228>>	BREAK	
					CASE 7		RETURN  <<-2176.5640, 197.5870, 188.6015>>	BREAK	
					CASE 8		RETURN  <<-2210.0110, 200.3230, 193.6016>>	BREAK	
					CASE 9		RETURN  <<-2271.5371, 228.8720, 193.6114>>	BREAK	
					CASE 10		RETURN  <<-2213.6760, 290.5950, 183.6017>>	BREAK	
					CASE 11		RETURN  <<-2281.4707, 362.2518, 173.6017>>	BREAK	
					CASE 12		RETURN  <<-2273.5686, 279.8986, 183.6014>>	BREAK	
					CASE 13		RETURN  <<-2308.4561, 296.6700, 168.6021>>	BREAK	
					CASE 14		RETURN  <<-2347.4709, 264.0780, 163.6312>>	BREAK	
					CASE 15		RETURN  <<-2273.3831, 261.9670, 168.5860>>	BREAK	
					CASE 16		RETURN  <<-2257.0620, 321.7790, 189.3613>>	BREAK	
					CASE 17		RETURN  <<-2225.6680, 213.8490, 174.6021>>	BREAK		
					CASE 18		RETURN  <<-2192.8040, 220.7470, 173.6019>>	BREAK		
					CASE 19		RETURN  <<-2230.6211, 277.9750, 196.6014>>	BREAK	

					CASE 20		RETURN  <<-2223.9951, 340.5040, 173.6020>>	BREAK	
					CASE 21		RETURN  <<-2277.2610, 310.7320, 183.6021>>	BREAK	
					CASE 22		RETURN  <<-2276.9390, 333.3840, 173.6020>>	BREAK	
					CASE 23		RETURN  <<-2231.5930, 310.5690, 174.1023>>	BREAK	
					CASE 24		RETURN  <<-2369.5029, 325.5270, 165.4069>>	BREAK	
					CASE 25		RETURN  <<-2233.7251, 180.5660, 173.6120>>	BREAK	
					CASE 26		RETURN  <<-2271.7380, 212.3070, 181.1018>>	BREAK	
					CASE 27		RETURN  <<-2285.1990, 187.1740, 166.6017>>	BREAK	
					CASE 28		RETURN  <<-2311.8740, 248.9990, 178.6120>>	BREAK	
					CASE 29		RETURN  <<-2330.3320, 353.0110, 172.1858>>	BREAK		

				ENDSWITCH
			BREAK
			
			CASE STRAWBERRYCENTRE
				serverBD.vareablipcoordinates =  GET_AREA_BLIP_COORDINATES(STRAWBERRYCENTRE) 
				SWITCH iRandomLocation
					CASE 0		RETURN  <<457.3300, -1395.1600, 28.3659>>	BREAK	
					CASE 1		RETURN  <<474.0560, -1352.8810, 28.0651>>	BREAK	
					CASE 2		RETURN  <<403.5780, -1349.1880, 43.9035>>	BREAK	
					CASE 3		RETURN  <<467.5550, -1183.3719, 28.2918>>	BREAK	
					CASE 4		RETURN  <<478.6770, -1221.8600, 28.6224>>	BREAK	
					CASE 5		RETURN  <<450.2690, -1300.9260, 28.4005>>	BREAK	
					CASE 6		RETURN  <<463.8370, -1324.2100, 28.4366>>	BREAK	
					CASE 7		RETURN  <<305.0100, -1332.5190, 30.9421>>	BREAK	
					CASE 8		RETURN  <<578.5540, -1254.5330, 8.8044>>	BREAK	
					CASE 9		RETURN  <<554.6470, -1338.4871, 28.2492>>	BREAK	
					CASE 10		RETURN  <<472.9230, -1287.9520, 28.7228>>	BREAK	
					CASE 11		RETURN  <<626.5690, -1350.6370, 8.7239>>	BREAK	
					CASE 12		RETURN  <<489.2880, -1302.9790, 28.3485>>	BREAK	
					CASE 13		RETURN  <<510.9220, -1335.5790, 28.7217>>	BREAK	
					CASE 14		RETURN  <<367.8470, -1160.4440, 28.2918>>	BREAK	
					CASE 15		RETURN  <<341.7110, -1260.9640, 31.4580>>	BREAK	
					CASE 16		RETURN  <<453.6360, -1372.3850, 42.5535>>	BREAK	
					CASE 17		RETURN  <<442.1230, -1233.8370, 28.9423>>	BREAK		
					CASE 18		RETURN  <<395.3380, -1282.3521, 32.5537>>	BREAK		
					CASE 19		RETURN  <<348.3238, -1391.2028, 31.5093>>	BREAK		
					
					CASE 20		RETURN  <<516.5250, -1283.6588, 24.3991>>	BREAK	
					CASE 21		RETURN  <<522.8490, -1395.9150, 28.4515>>	BREAK	
					CASE 22		RETURN  <<474.8570, -1456.5040, 28.2922>>	BREAK	
					CASE 23		RETURN  <<422.6730, -1428.3590, 28.2920>>	BREAK	
					CASE 24		RETURN  <<365.3130, -1317.7321, 32.5537>>	BREAK	
					CASE 25		RETURN  <<453.1990, -1113.9600, 28.1985>>	BREAK	
					CASE 26		RETURN  <<526.0240, -1199.7980, 28.9301>>	BREAK	
					CASE 27		RETURN  <<522.1890, -1247.0680, 29.9524>>	BREAK	
					CASE 28		RETURN  <<384.8390, -1245.8621, 31.7135>>	BREAK	
					CASE 29		RETURN  <<607.8720, -1180.5870, 8.9942>>	BREAK	
				ENDSWITCH
			BREAK		

			CASE PACIFICBLUFFSCENTRE
				serverBD.vareablipcoordinates =  GET_AREA_BLIP_COORDINATES(PACIFICBLUFFSCENTRE) 
				SWITCH iRandomLocation
					CASE 0		RETURN  <<-1793.8640, -234.0000, 48.2061>>	BREAK	
					CASE 1		RETURN  <<-1754.5270, -384.2780, 45.3667>>	BREAK	
					CASE 2		RETURN  <<-1697.0580, -421.2620, 45.0284>>	BREAK	
					CASE 3		RETURN  <<-1654.4320, -414.8460, 41.0087>>	BREAK	
					CASE 4		RETURN  <<-1606.7690, -360.8080, 48.3470>>	BREAK	
					CASE 5		RETURN  <<-1668.2640, -213.1670, 54.2607>>	BREAK	
					CASE 6		RETURN  <<-1630.9030, -143.2410, 56.6885>>	BREAK	
					CASE 7		RETURN  <<-1479.3230, -184.5370, 47.8293>>	BREAK	
					CASE 8		RETURN  <<-1548.3850, -275.0620, 45.7122>>	BREAK	
					CASE 9		RETURN  <<-1506.8521, -336.0020, 45.4915>>	BREAK	
					CASE 10		RETURN  <<-1560.8540, -421.4790, 37.9164>>	BREAK	
					CASE 11		RETURN  <<-1615.1870, -197.6740, 54.0480>>	BREAK	
					CASE 12		RETURN  <<-1576.1620, -233.2010, 48.4658>>	BREAK	
					CASE 13		RETURN  <<-1760.8820, -316.1410, 44.5111>>	BREAK	
					CASE 14		RETURN  <<-1502.5250, -208.8010, 58.9649>>	BREAK	
					CASE 15		RETURN  <<-1703.8521, -274.5470, 50.9376>>	BREAK	
					CASE 16		RETURN  <<-1465.6689, -382.3540, 42.4371>>	BREAK	
					CASE 17		RETURN  <<-1795.6490, -360.1570, 43.8969>>	BREAK		
					CASE 18		RETURN  <<-1678.0310, -115.5510, 64.4399>>	BREAK		
					CASE 19		RETURN  <<-1719.7570, -192.1420, 57.3760>>	BREAK		
					
					CASE 20		RETURN  <<-1652.8850, -369.5520, 48.7813>>	BREAK	
					CASE 21		RETURN  <<-1539.5031, -389.6020, 41.4308>>	BREAK	
					CASE 22		RETURN  <<-1557.5120, -314.1740, 46.7320>>	BREAK	
					CASE 23		RETURN  <<-1593.6980, -279.1950, 51.6187>>	BREAK	
					CASE 24		RETURN  <<-1694.4620, -325.2990, 49.0703>>	BREAK	
					CASE 25		RETURN  <<-1737.1450, -284.0460, 48.3228>>	BREAK	
					CASE 26		RETURN  <<-1645.4460, -274.1050, 51.7373>>	BREAK	
					CASE 27		RETURN  <<-1673.4650, -157.9260, 56.9810>>	BREAK	
					CASE 28		RETURN  <<-1734.3680, -157.8670, 58.8432>>	BREAK	
					CASE 29		RETURN  <<-1763.0310, -262.8990, 47.1851>>	BREAK	


				ENDSWITCH
			BREAK	
			
			CASE GRAPESEEDCENTRE	
				serverBD.vareablipcoordinates =  GET_AREA_BLIP_COORDINATES(GRAPESEEDCENTRE) 
				SWITCH iRandomLocation
					CASE 0		RETURN  <<1654.9240, 4738.8892, 41.0288>>	BREAK	
					CASE 1		RETURN  <<1637.7770, 4784.9170, 41.4990>>	BREAK	
					CASE 2		RETURN  <<1642.7050, 4846.0718, 47.1139>>	BREAK	
					CASE 3		RETURN  <<1707.0520, 4924.7842, 41.0782>>	BREAK	
					CASE 4		RETURN  <<1664.1370, 4911.8130, 37.5926>>	BREAK	
					CASE 5		RETURN  <<1806.0420, 4933.4780, 43.1677>>	BREAK	
					CASE 6		RETURN  <<1778.7629, 5003.5649, 51.9615>>	BREAK	
					CASE 7		RETURN  <<1901.0270, 4929.8711, 48.5578>>	BREAK	
					CASE 8		RETURN  <<1905.7030, 4835.0762, 45.1146>>	BREAK	
					CASE 9		RETURN  <<1841.5190, 4720.3379, 31.5984>>	BREAK	
					CASE 10		RETURN  <<1794.5050, 4771.9360, 33.9436>>	BREAK	
					CASE 11		RETURN  <<1677.6600, 4882.2012, 46.3200>>	BREAK	
					CASE 12		RETURN  <<1732.9830, 4729.3691, 40.9665>>	BREAK	
					CASE 13		RETURN  <<1774.6541, 4688.4082, 41.9099>>	BREAK	
					CASE 14		RETURN  <<1862.5740, 4791.5459, 42.3429>>	BREAK	
					CASE 15		RETURN  <<1815.7090, 4834.1890, 42.0427>>	BREAK	
					CASE 16		RETURN  <<1913.8120, 4718.0342, 40.1035>>	BREAK	
					CASE 17		RETURN  <<1888.2744, 4666.7510, 37.9346>>	BREAK		
					CASE 18		RETURN  <<1888.2744, 4666.7510, 37.9346>>	BREAK		
					CASE 19		RETURN  <<1665.6320, 4662.8008, 42.3872>>	BREAK	

					CASE 20		RETURN  <<1726.5870, 4784.5869, 40.9782>>	BREAK	
					CASE 21		RETURN  <<1771.7220, 4806.0610, 38.8353>>	BREAK	
					CASE 22		RETURN  <<1924.1580, 4791.6021, 42.7122>>	BREAK	
					CASE 23		RETURN  <<1666.2469, 4971.4248, 41.2677>>	BREAK	
					CASE 24		RETURN  <<1793.3820, 4625.9302, 36.6641>>	BREAK	
					CASE 25		RETURN  <<1872.2560, 4846.7388, 43.6538>>	BREAK	
					CASE 26		RETURN  <<1854.0570, 4904.1099, 43.4852>>	BREAK	
					CASE 27		RETURN  <<1767.9790, 4877.1128, 34.7104>>	BREAK	
					CASE 28		RETURN  <<1707.6470, 4735.2031, 41.1578>>	BREAK	
					CASE 29		RETURN  <<1724.3160, 4641.9819, 42.8755>>	BREAK							
				ENDSWITCH               
			BREAK			

			
			CASE SAWMILLCENTRE
				serverBD.vareablipcoordinates =  GET_AREA_BLIP_COORDINATES(SAWMILLCENTRE) 
				SWITCH iRandomLocation
					CASE 0		RETURN  <<-546.9640, 5410.0610, 59.5694>>	BREAK	
					CASE 1		RETURN  <<-505.7530, 5358.8521, 73.5475>>	BREAK	
					CASE 2		RETURN  <<-577.1100, 5229.2490, 69.6291>>	BREAK	
					CASE 3		RETURN  <<-509.5830, 5211.7012, 82.9865>>	BREAK	
					CASE 4		RETURN  <<-570.7100, 5184.2192, 94.9967>>	BREAK	
					CASE 5		RETURN  <<-648.4540, 5190.9932, 95.6284>>	BREAK	
					CASE 6		RETURN  <<-722.3180, 5168.5200, 105.4629>>	BREAK	
					CASE 7		RETURN  <<-531.1790, 5337.4819, 79.2678>>	BREAK	
					CASE 8		RETURN  <<-682.5410, 5390.9971, 53.0597>>	BREAK	
					CASE 9		RETURN  <<-505.4330, 5286.8379, 79.5575>>	BREAK	
					CASE 10		RETURN  <<-559.1420, 5287.1680, 75.3232>>	BREAK	
					CASE 11		RETURN  <<-554.3130, 5330.1221, 72.9636>>	BREAK	
					CASE 12		RETURN  <<-635.7840, 5367.3110, 49.3918>>	BREAK	
					CASE 13		RETURN  <<-741.0900, 5302.2129, 73.9228>>	BREAK	
					CASE 14		RETURN  <<-829.0860, 5259.8110, 81.7190>>	BREAK	
					CASE 15		RETURN  <<-670.7430, 5469.8638, 49.3261>>	BREAK	
					CASE 16		RETURN  <<-622.3710, 5407.3291, 48.4316>>	BREAK	
					CASE 17		RETURN  <<-580.6800, 5351.5342, 69.2145>>	BREAK		
					CASE 18		RETURN  <<-570.6880, 5308.3311, 69.2358>>	BREAK		
					CASE 19		RETURN  <<-781.9750, 5384.7100, 33.3477>>	BREAK		
					
					CASE 20		RETURN  <<-728.1990, 5409.6851, 49.3272>>	BREAK	
					CASE 21		RETURN  <<-769.9030, 5316.5532, 74.6382>>	BREAK	
					CASE 22		RETURN  <<-756.0570, 5233.7622, 98.1908>>	BREAK	
					CASE 23		RETURN  <<-469.7680, 5325.6719, 79.6862>>	BREAK	
					CASE 24		RETURN  <<-541.9630, 5370.0771, 69.4891>>	BREAK	
					CASE 25		RETURN  <<-594.0380, 5449.9858, 58.1998>>	BREAK	
					CASE 26		RETURN  <<-706.8370, 5261.6191, 70.8839>>	BREAK	
					CASE 27		RETURN  <<-691.4200, 5346.1641, 67.1351>>	BREAK	
					CASE 28		RETURN  <<-646.7220, 5282.5371, 69.6697>>	BREAK	
					CASE 29		RETURN  <<-595.3020, 5340.6172, 69.3620>>	BREAK	

				ENDSWITCH
			BREAK	
			
			CASE VINEWOODLAKECENTRE
				serverBD.vareablipcoordinates =  GET_AREA_BLIP_COORDINATES(VINEWOODLAKECENTRE) 	
				SWITCH iRandomLocation
					CASE 0		RETURN  <<-8.3920, 736.5560, 200.7120>>		BREAK	
					CASE 1		RETURN  <<-190.6900, 820.9590, 200.1964>>	BREAK	
					CASE 2		RETURN  <<-244.3710, 979.0510, 226.5355>>	BREAK	
					CASE 3		RETURN  <<-181.8550, 974.9320, 231.1338>>	BREAK	
					CASE 4		RETURN  <<-80.9420, 940.0560, 232.0286>>	BREAK	
					CASE 5		RETURN  <<-46.8270, 778.9190, 222.3734>>	BREAK	
					CASE 6		RETURN  <<-89.3840, 820.0520, 226.7467>>	BREAK	
					CASE 7		RETURN  <<-147.2664, 845.0591, 223.4742>>	BREAK	
					CASE 8		RETURN  <<-145.2310, 885.5650, 238.0254>>	BREAK	
					CASE 9		RETURN  <<-151.7135, 910.5461, 234.6557>>	BREAK	
					CASE 10		RETURN  <<-96.4470, 889.6520, 235.2137>>	BREAK	
					CASE 11		RETURN  <<-106.3370, 999.0760, 234.7601>>	BREAK	
					CASE 12		RETURN  <<-9.5210, 1002.4110, 216.3439>>	BREAK	
					CASE 13		RETURN  <<-39.6400, 941.6110, 227.6727>>	BREAK	
					CASE 14		RETURN  <<-218.4348, 738.2919, 197.4301>>	BREAK	
					CASE 15		RETURN  <<-7.1600, 901.3360, 216.6091>>		BREAK	
					CASE 16		RETURN  <<-51.9230, 1003.4410, 233.1331>>	BREAK	
					CASE 17		RETURN  <<-45.4630, 833.1730, 234.7239>>	BREAK		
					CASE 18		RETURN  <<62.8870, 967.3640, 200.7571>>		BREAK		
					CASE 19		RETURN  <<-57.1100, 884.0860, 231.3181>>	BREAK

					CASE 20		RETURN  <<27.7380, 850.9695, 196.7696>> 	BREAK	
					CASE 21		RETURN  <<-124.3320, 954.0810, 234.4093>> 	BREAK	
					CASE 22		RETURN  <<-94.7180, 1030.9650, 234.4603>>	BREAK	
					CASE 23		RETURN  <<-1.7550, 937.6700, 205.2185>>		BREAK	
					CASE 24		RETURN  <<-201.6830, 1027.6860, 233.1841>>	BREAK	
					CASE 25		RETURN  <<-185.4590, 909.8900, 232.4636>>	BREAK	
					CASE 26		RETURN  <<-273.2910, 798.6730, 197.4136>> 	BREAK	
					CASE 27		RETURN  <<-187.5080, 786.7680, 197.1125>>	BREAK	
					CASE 28		RETURN  <<31.1070, 909.9800, 197.6170>> 	BREAK	
					CASE 29		RETURN  <<-141.6040, 989.6030, 235.4249>>	BREAK	

				ENDSWITCH
			BREAK		
			
			CASE REDWOODCENTRE
				serverBD.vareablipcoordinates =  GET_AREA_BLIP_COORDINATES(REDWOODCENTRE) 
	
				SWITCH iRandomLocation
					CASE 0		RETURN  <<1031.1140, 2448.6819, 48.5704>>	BREAK	
					CASE 1		RETURN  <<1129.4330, 2172.6560, 74.7989>>	BREAK	
					CASE 2		RETURN  <<1155.1000, 2355.9309, 52.8569>>	BREAK	
					CASE 3		RETURN  <<1150.1429, 2410.1431, 50.5989>>	BREAK	
					CASE 4		RETURN  <<1095.6710, 2316.5081, 44.5125>>	BREAK	
					CASE 5		RETURN  <<1079.4828, 2241.1138, 43.6010>>	BREAK	
					CASE 6		RETURN  <<1058.9792, 2235.9553, 42.1733>>	BREAK	
					CASE 7		RETURN  <<992.3470, 2235.2310, 50.3834>>	BREAK	
					CASE 8		RETURN  <<909.9076, 2302.2544, 45.4240>>	BREAK	
					CASE 9		RETURN  <<909.4380, 2347.5852, 51.1453>>	BREAK	
					CASE 10		RETURN  <<1071.2981, 2178.6021, 47.2536>>	BREAK	
					CASE 11		RETURN  <<870.0360, 2354.6609, 50.7088>>	BREAK	
					CASE 12		RETURN  <<969.0116, 2227.3591, 45.9831>>	BREAK	
					CASE 13		RETURN  <<1022.3260, 2396.0110, 49.4936>>	BREAK	
					CASE 14		RETURN  <<950.1810, 2405.7009, 50.0813>>	BREAK	
					CASE 15		RETURN  <<1067.1050, 2308.8391, 50.8205>>	BREAK	
					CASE 16		RETURN  <<1086.3300, 2510.4099, 50.1429>>	BREAK	
					CASE 17		RETURN  <<880.7990, 2414.8630, 52.8610>>	BREAK		
					CASE 18		RETURN  <<1215.9678, 2263.3599, 69.4385>>	BREAK		
					CASE 19		RETURN  <<1047.1370, 2524.2209, 44.0939>>	BREAK	

					CASE 20		RETURN  <<1153.0944, 2278.7861, 49.7294>>	BREAK	
					CASE 21		RETURN  <<1005.4000, 2168.1770, 49.6825>>	BREAK	
					CASE 22		RETURN  <<880.2500, 2269.5281, 48.6372>> 	BREAK	
					CASE 23		RETURN  <<914.5890, 2458.1699, 48.6957>> 	BREAK	
					CASE 24		RETURN  <<966.1300, 2480.3181, 48.6734>> 	BREAK	
					CASE 25		RETURN  <<1070.9280, 2468.3391, 48.4890>>	BREAK	
					CASE 26		RETURN  <<1126.1090, 2438.9250, 51.5697>>	BREAK	
					CASE 27		RETURN  <<988.2800, 2373.6150, 50.2218>> 	BREAK	
					CASE 28		RETURN  <<972.2970, 2334.5120, 47.4337>>	BREAK	
					CASE 29		RETURN  <<996.6350, 2288.2100, 48.5785>> 	BREAK		
		
				ENDSWITCH
			BREAK			
					
					
			CASE ROUTE68CENTRE
				serverBD.vareablipcoordinates =  GET_AREA_BLIP_COORDINATES(ROUTE68CENTRE) 
				SWITCH iRandomLocation
					CASE 0		RETURN  <<269.9960, 2875.3091, 42.6108>>	BREAK	
					CASE 1		RETURN  <<315.1450, 2891.9419, 45.4681>>	BREAK	
					CASE 2		RETURN  <<403.7480, 2764.7820, 54.8613>>	BREAK	
					CASE 3		RETURN  <<375.2860, 2733.3320, 55.7972>>	BREAK	
					CASE 4		RETURN  <<413.4450, 2583.4380, 42.5196>>	BREAK	
					CASE 5		RETURN  <<368.5990, 2567.7690, 42.5196>>	BREAK	
					CASE 6		RETURN  <<346.6700, 2608.9871, 43.5195>>	BREAK	
					CASE 7		RETURN  <<262.6540, 2591.7190, 43.9456>>	BREAK	
					CASE 8		RETURN  <<171.5740, 2615.5710, 47.2400>>	BREAK	
					CASE 9		RETURN  <<222.2880, 2573.8469, 44.9165>>	BREAK	
					CASE 10		RETURN  <<175.1980, 2708.2671, 41.1922>>	BREAK	
					CASE 11		RETURN  <<201.2270, 2766.7371, 42.4263>>	BREAK	
					CASE 12		RETURN  <<385.7840, 2847.1680, 42.8630>>	BREAK	
					CASE 13		RETURN  <<281.6240, 2736.6731, 42.8868>>	BREAK	
					CASE 14		RETURN  <<221.0710, 2800.6030, 44.8449>>	BREAK	
					CASE 15		RETURN  <<371.4820, 2701.1741, 51.5597>>	BREAK	
					CASE 16		RETURN  <<480.4720, 2645.5891, 41.9761>>	BREAK	
					CASE 17		RETURN  <<263.1340, 2647.4890, 43.6307>>	BREAK		
					CASE 18		RETURN  <<338.2270, 2802.4900, 49.9371>>	BREAK		
					CASE 19		RETURN  <<166.0300, 2799.8579, 44.6552>>	BREAK

					CASE 20		RETURN  <<223.5100, 2848.6819, 43.5906>>	BREAK	
					CASE 21		RETURN  <<191.9450, 2646.7959, 47.0016>>	BREAK	
					CASE 22		RETURN  <<318.4840, 2711.7500, 55.9070>>	BREAK	
					CASE 23		RETURN  <<305.9630, 2628.2720, 43.5177>>	BREAK	
					CASE 24		RETURN  <<452.0400, 2694.3811, 42.0812>>	BREAK	
					CASE 25		RETURN  <<465.9550, 2752.9790, 46.7751>>	BREAK	
					CASE 26		RETURN  <<296.7220, 2777.2180, 43.9755>>	BREAK	
					CASE 27		RETURN  <<221.0900, 2731.8701, 41.7913>>	BREAK	
					CASE 28		RETURN  <<469.2950, 2599.0601, 42.2732>>	BREAK	
					CASE 29		RETURN  <<408.7830, 2646.6570, 43.4928>>	BREAK	

				ENDSWITCH
			BREAK			
			
			CASE WESTVINEWOODCENTRE
				
				serverBD.vareablipcoordinates =  GET_AREA_BLIP_COORDINATES(WESTVINEWOODCENTRE) 
				SWITCH iRandomLocation
					CASE 0		RETURN  <<-367.8890, 571.7600, 126.6262>>	BREAK	
					CASE 1		RETURN  <<-535.5680, 596.9450, 116.2815>>	BREAK	
					CASE 2		RETURN  <<-605.7690, 450.3030, 108.5597>>	BREAK	
					CASE 3		RETURN  <<-604.7580, 374.6670, 92.5739>>	BREAK	
					CASE 4		RETURN  <<-604.9550, 323.2930, 81.2663>>	BREAK	
					CASE 5		RETURN  <<-565.5980, 294.9440, 82.0296>>	BREAK	
					CASE 6		RETURN  <<-430.7530, 291.2190, 85.0675>>	BREAK	
					CASE 7		RETURN  <<-360.0450, 320.1690, 106.2401>>	BREAK	
					CASE 8		RETURN  <<-404.9580, 325.1780, 107.7228>>	BREAK	
					CASE 9		RETURN  <<-445.5070, 375.5060, 103.7797>>	BREAK	
					CASE 10		RETURN  <<-373.1900, 391.4630, 107.3591>>	BREAK	
					CASE 11		RETURN  <<-422.7820, 426.5758, 112.4687>>	BREAK	
					CASE 12		RETURN  <<-562.2130, 544.8610, 109.3968>>	BREAK	
					CASE 13		RETURN  <<-473.5880, 573.6140, 124.8750>>	BREAK	
					CASE 14		RETURN  <<-456.9480, 521.6210, 120.4625>>	BREAK	
					CASE 15		RETURN  <<-495.6580, 505.0990, 109.0915>>	BREAK	
					CASE 16		RETURN  <<-456.0730, 482.5880, 113.0483>>	BREAK	
					CASE 17		RETURN  <<-361.4440, 445.8460, 112.4609>>	BREAK		
					CASE 18		RETURN  <<-304.2520, 420.0210, 108.8780>>	BREAK		
					CASE 19		RETURN  <<-350.8320, 521.5700, 119.1550>>	BREAK		
					
					CASE 20		RETURN  <<-435.6485, 585.9025, 126.2896>>	BREAK	
					CASE 21		RETURN  <<-409.9430, 509.7940, 120.5896>>	BREAK	
					CASE 22		RETURN  <<-282.7220, 488.5010, 112.1980>>	BREAK	
					CASE 23		RETURN  <<-312.9820, 353.2520, 109.0209>>	BREAK	
					CASE 24		RETURN  <<-511.2900, 315.2350, 82.2552>> 	BREAK	
					CASE 25		RETURN  <<-546.3370, 340.5590, 82.2365>> 	BREAK	
					CASE 26		RETURN  <<-554.0450, 401.7160, 99.6595>> 	BREAK	
					CASE 27		RETURN  <<-581.2870, 496.2440, 106.6480>>	BREAK	
					CASE 28		RETURN  <<-527.1080, 518.1930, 111.9369>>	BREAK	
					CASE 29		RETURN  <<-478.4680, 425.7820, 102.1218>>	BREAK		

				ENDSWITCH
			BREAK			
					
					
					
			CASE LEGIONSQUARECENTRE
				serverBD.vareablipcoordinates =  GET_AREA_BLIP_COORDINATES(LEGIONSQUARECENTRE) 
				SWITCH iRandomLocation
					CASE 0		RETURN  <<206.7920, -998.9710, 28.2918>>	BREAK	
					CASE 1		RETURN  <<342.8750, -1034.2300, 29.4955>>	BREAK	
					CASE 2		RETURN  <<262.7580, -1027.2600, 28.2140>>	BREAK	
					CASE 3		RETURN  <<189.1750, -1072.5830, 28.2795>>	BREAK	
					CASE 4		RETURN  <<174.0000, -1076.6180, 28.1962>>	BREAK	
					CASE 5		RETURN  <<119.8420, -1096.6240, 34.6653>>	BREAK	
					CASE 6		RETURN  <<130.5150, -1042.1880, 28.2942>>	BREAK	
					CASE 7		RETURN  <<52.6548, -1044.8162, 32.6961>>	BREAK	
					CASE 8		RETURN  <<55.1710, -876.1330, 29.4083>>		BREAK	
					CASE 9		RETURN  <<238.0960, -757.4810, 33.6436>>	BREAK	
					CASE 10		RETURN  <<281.0850, -810.0416, 28.3168>>	BREAK	
					CASE 11		RETURN  <<377.3070, -881.7010, 38.1639>>	BREAK	
					CASE 12		RETURN  <<145.9080, -981.3250, 28.4011>>	BREAK	
					CASE 13		RETURN  <<287.0000, -987.8730, 32.1041>>	BREAK	
					CASE 14		RETURN  <<109.3720, -820.7490, 30.3121>>	BREAK	
					CASE 15		RETURN  <<150.0340, -783.3270, 31.9353>>	BREAK	
					CASE 16		RETURN  <<214.1744, -808.5712, 29.7824>>	BREAK	
					CASE 17		RETURN  <<105.8500, -930.1200, 28.8239>>	BREAK		
					CASE 18		RETURN  <<203.5860, -882.3280, 30.4983>>	BREAK		
					CASE 19		RETURN  <<218.4650, -934.8880, 28.7917>>	BREAK	

					CASE 20		RETURN  <<302.7500, -908.7450, 28.2933>> 	BREAK	
					CASE 21		RETURN  <<251.1280, -1072.7040, 28.2963>> 	BREAK	
					CASE 22		RETURN  <<179.6410, -941.0620, 29.0919>> 	BREAK	
					CASE 23		RETURN  <<173.0760, -994.8010, 28.2918>> 	BREAK	
					CASE 24		RETURN  <<188.5510, -1050.5081, 28.3322>> 	BREAK	
					CASE 25		RETURN  <<345.8170, -977.7090, 28.3770>> 	BREAK	
					CASE 26		RETURN  <<342.0590, -833.6070, 28.2916>> 	BREAK	
					CASE 27		RETURN  <<6.8170, -937.2360, 28.9050>> 	    BREAK	
					CASE 28		RETURN  <<32.7850, -997.0830, 28.5052>> 	BREAK	
					CASE 29		RETURN  <<240.7830, -894.8540, 28.2921>> 	BREAK

				ENDSWITCH
			BREAK				
		ENDSWITCH
		SCRIPT_ASSERT("GENERATE_PACKAGE_LOCATION supplied with invalid mapzone")
		RETURN <<218.4650, -934.8880, 28.7917>>
	
ENDFUNC

/// PURPOSE:
///    Checks if a specific player is near the package
/// PARAMS:
///    thisPlayer - The player to check
/// RETURNS:
///    True if so
FUNC BOOL IS_THIS_PLAYER_NEAR_PACKAGE(PLAYER_INDEX thisPlayer)
	PED_INDEX playerPed = GET_PLAYER_PED(thisPlayer)
	IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(playerPed, serverBD.vareablipcoordinates, FALSE) < g_sMPTunables.igb_finders_keepers_area_radius                                     
		IF initialisemusic = 0
			GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
			TRIGGER_MUSIC_EVENT("BG_FINDERS_KEEPERS_START")
			initialisemusic = 1
		ENDIF
		REMOVE_PACKAGE_AREA_BLIP_LONG_RANG_BLIP()
		RETURN TRUE
	ELSE
		IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(playerPed, serverBD.vareablipcoordinates, FALSE) > g_sMPTunables.igb_finders_keepers_area_radius + 50.0                                    
			IF initialisemusic = 1
				IF AUDIO_IS_SCRIPTED_MUSIC_PLAYING()
					TRIGGER_MUSIC_EVENT("BG_FINDERS_KEEPERS_STOP")
					initialisemusic = 0
				ENDIF
			ENDIF
		ENDIF
		
		
		ADD_FINDERS_KEEPERS_BLIP()
	ENDIF
	

	
	RETURN FALSE
ENDFUNC


FUNC BOOL HAS_PACKAGE_BEEN_COLLECTED_SERVER()
	INT i
	PLAYER_INDEX player_who_collected_package
	REPEAT NUM_NETWORK_PLAYERS i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			PLAYER_INDEX Player = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			
			//IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(Player, GB_GET_LOCAL_PLAYER_GANG_BOSS(), true)
				IF playerBD[i].this_player_has_picked_up_package  = TRUE
				AND serverBD.has_package_been_pickedup= FALSE
					IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
						serverBD.has_package_been_pickedup = TRUE
						
						serverBD.ipackagecounter++
				
						player_who_collected_package  = Player
						

						IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
							TickerEventData.TickerEvent = TICKER_EVENT_FINDERSKEEPERS_PACKAGE_COLLECTED
							TickerEventData.playerID = player_who_collected_package
							
							IF  playerBD[i].iclientscore <= g_sMPTunables.igb_finders_keepers_bonus_cash_max_packages
								TickerEventData.dataInt = g_sMPTunables.igb_finders_keepers_cash_package_default_value + ( (playerBD[i].iclientscore - 1) * g_sMPTunables.igb_finders_keepers_cash_increase_per_package)
							ELSE
								TickerEventData.dataInt = g_sMPTunables.igb_finders_keepers_cash_package_default_value + (g_sMPTunables.igb_finders_keepers_bonus_cash_max_packages * g_sMPTunables.igb_finders_keepers_cash_increase_per_package) 
							ENDIF
							
							IF TickerEventData.playerID = PLAYER_ID()
								IF display_pickupmoney_help = FALSE	
									display_pickupmoney_help = TRUE
									TickerEventData.dataInt2 = 1
								ELSE
									TickerEventData.dataInt2 = 0
								ENDIF
							ENDIF
							PRINTLN("FINDERS KEEEPRS CASH REWARD = HAS_PACKAGE_BEEN_COLLECTED_SERVER TickerEventData.dataInt = ", TickerEventData.dataInt)
							BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT())
							NET_PRINT_TIME() NET_PRINT("     ---------->     FINDERS KEEPERS  - TICKER_EVENT_FINDERSKEEPERS_PACKAGE_COLLECTED     <----------     ") NET_NL()
						ENDIF
					ENDIF
					
					PRINTLN("     ---------->     FINDERS KEEPERS - HAS_PACKAGE_BEEN_COLLECTED_SERVER true PLAYER ID = ", i )
					
					//playerBD[i].iclientscore++					
					playerBD[i].this_player_has_picked_up_package = FALSE
					DELETE_PACKAGE()
					PRINTLN("     ---------->     FINDERS KEEPERS - HAS_PACKAGE_BEEN_COLLECTED_SERVER true serverBD.ipackagecounter = ", serverBD.ipackagecounter )
					
					SORT_FINDERS_KEEPERS_PLAYER_SCORES(Player,playerBD[i].iclientscore)
					
					RETURN TRUE
				ENDIF
			//ENDIF
		ENDIF
	ENDREPEAT
	
	//IF serverBD.has_package_been_pickedup= TRUE
	//	RETURN TRUE
	//ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PACKAGE_BEEN_COLLECTED_CLIENT()
	IF DOES_PICKUP_EXIST(the_package)
		if playerBD[PARTICIPANT_ID_TO_INT()].this_player_has_picked_up_package  = FALSE
			BOOL IS_PLAYER_A_PASSENGER = FALSE
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
          		VEHICLE_INDEX temp_vehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
            	IF GET_PED_IN_VEHICLE_SEAT(temp_vehicle, VS_DRIVER) != PLAYER_PED_ID()
					IS_PLAYER_A_PASSENGER = TRUE
					PRINTLN("FINDERS KEEPERS HAS_PACKAGE_BEEN_COLLECTED_CLIENT IS_PLAYER_A_PASSENGER = TRUE")
				ENDIF
			ENDIF
			
			
			IF gfinderskeeperslocalplayerhascollectedpickup = TRUE
			OR ((GET_DISTANCE_BETWEEN_COORDS(serverBD.vPackageLocation[serverBD.ipackagecounter], GET_ENTITY_COORDS(GET_PLAYER_PED(PLAYER_ID()), FALSE)) < 1.0) AND NOT IS_PLAYER_A_PASSENGER)
					gfinderskeeperslocalplayerhascollectedpickup = FALSE
					PRINTLN("     ---------->     FINDERS KEEPERS - HAS_PACKAGE_BEEN_COLLECTED_CLIENT true, Local Player PARTICIPANT_ID_TO_INT = ", PARTICIPANT_ID_TO_INT())
					playerBD[PARTICIPANT_ID_TO_INT()].iclientscore++
					playerBD[PARTICIPANT_ID_TO_INT()].this_player_has_picked_up_package = TRUE
					GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
					DELETE_PACKAGE()
				
					INT amountcash		
					IF  playerBD[PARTICIPANT_ID_TO_INT()].iclientscore <= g_sMPTunables.igb_finders_keepers_bonus_cash_max_packages
						amountcash = g_sMPTunables.igb_finders_keepers_cash_package_default_value + ( (playerBD[PARTICIPANT_ID_TO_INT()].iclientscore - 1) * g_sMPTunables.igb_finders_keepers_cash_increase_per_package)
					ELSE
						amountcash = g_sMPTunables.igb_finders_keepers_cash_package_default_value + (g_sMPTunables.igb_finders_keepers_bonus_cash_max_packages * g_sMPTunables.igb_finders_keepers_cash_increase_per_package) 
					ENDIF
					IF USE_SERVER_TRANSACTIONS()
						PRINTLN("[MAGNATE_GANG_BOSS] [GB$] TICKER_EVENT_FINDERSKEEPERS_PACKAGE_COLLECTED - TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_AMBIENT_JOB_CHECKPOINT_COLLECTION, ", 100, ", iScriptTransactionIndex, FALSE, TRUE)")
						INT iScriptTransactionIndex
						TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_AMBIENT_JOB_CHECKPOINT_COLLECTION,amountcash, iScriptTransactionIndex, FALSE, TRUE) //Add new service
					ELSE
						AMBIENT_JOB_DATA amData
						PRINTLN("[MAGNATE_GANG_BOSS] [GB$] TICKER_EVENT_FINDERSKEEPERS_PACKAGE_COLLECTED - NETWORK_EARN_FROM_AMBIENT_JOB(", 100, ",\"AM_CP_COLLECTION\",amData)")
						NETWORK_EARN_FROM_AMBIENT_JOB(amountcash,"AM_CP_COLLECTION",amData)
					ENDIF
					
					IF display_pickupmoney_help = FALSE
						PRINT_HELP_WITH_NUMBER("GB_FDKP_CASHPICK",g_sMPTunables.igb_finders_keepers_cash_increase_per_package )
						display_pickupmoney_help = TRUE
					ENDIF
					
					RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Returns true if all the gang members are near the package
/// RETURNS:
///    
FUNC BOOL ARE_ALL_GANG_MEMBERS_NEAR_PACKAGE()
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			PLAYER_INDEX PlayerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			IF IS_THIS_PLAYER_A_GANG_GOON(PlayerID)
				IF NOT IS_BIT_SET(playerBD[i].iClientBitSet, biP_GangNearpackage)
					RETURN FALSE
				ENDIF
			ENDIF		
		ENDIF	
	ENDREPEAT
	

	RETURN FALSE
ENDFUNC

PROC DRAW_BOTTOM_RIGHT_HUD()
	
	IF NOT MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
		EXIT
	ENDIF
	
	HUD_COLOURS timerColour = HUD_COLOUR_WHITE
	
	INT iTimeRemaining 
	BOOL bbForceRebuildNames = TRUE
	iTimeRemaining = GET_CHALLENGE_EXPIRE_TIME()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.Starttimer)
	iTimeRemaining = IMAX(iTimeRemaining, 0)
	
	
	IF iTimeRemaining < 30000
		timerColour = HUD_COLOUR_RED
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF IS_DEBUG_KEY_PRESSED(KEY_F11, KEYBOARD_MODIFIER_NONE, "Finders Keepers Output")
			PRINTLN("FINDERS KEEPERS DRAW_BOTTOM_RIGHT_HUD  serverBD.sSortedPackagesCollected[0].playerID  = ", GET_PLAYER_NAME(serverBD.sSortedPackagesCollected[0].playerID))
			PRINTLN("FINDERS KEEPERS DRAW_BOTTOM_RIGHT_HUD serverBD.sSortedPackagesCollected[1].playerID = ", GET_PLAYER_NAME(serverBD.sSortedPackagesCollected[1].playerID))
			PRINTLN("FINDERS KEEPERS DRAW_BOTTOM_RIGHT_HUD serverBD.sSortedPackagesCollected[2].playerID = ", GET_PLAYER_NAME(serverBD.sSortedPackagesCollected[2].playerID))
			PRINTLN("FINDERS KEEPERS DRAW_BOTTOM_RIGHT_HUD serverBD.sSortedPackagesCollected[3].playerID = ", GET_PLAYER_NAME(serverBD.sSortedPackagesCollected[3].playerID))


		ENDIF
	#ENDIF

	BOTTOM_RIGHT_UI_1STINT_2NDINT_3RDINT_4THINT_LOCALINT_TIMER(serverBD.sSortedPackagesCollected[0].playerID,
																serverBD.sSortedPackagesCollected[1].playerID, 
																serverBD.sSortedPackagesCollected[2].playerID, 
																serverBD.sSortedPackagesCollected[3].playerID, 
																serverBD.sSortedPackagesCollected[0].iScore, 
																serverBD.sSortedPackagesCollected[1].iScore,
																serverBD.sSortedPackagesCollected[2].iScore,
																serverBD.sSortedPackagesCollected[3].iScore,
																playerBD[PARTICIPANT_ID_TO_INT()].iclientscore, 
																iTimeRemaining,			
																bbForceRebuildNames, 
																timerColour,
																"GB_CHAL_END",
																 FMEVENT_SCORETITLE_YOUR_SCORE)
																 
																 
																 
										 
	
ENDPROC


// Widgets & Debug.
#IF IS_DEBUG_BUILD
	BOOL bHostEndMissionNow
	BOOL bWarpToPackage
	INT soundtype = 2
	BOOL bkillgame
	BOOL bkillsound
	
#ENDIF


#IF IS_DEBUG_BUILD
PROC CREATE_WIDGETS()
	INT iPlayer
	TEXT_LABEL_15 tl63

	START_WIDGET_GROUP("FINDERS KEEPERS")  
		ADD_WIDGET_BOOL("Warp to Package", bWarpToPackage)
		ADD_WIDGET_BOOL("Add blip for Package", bAddBlipforpackage)
		ADD_WIDGET_BOOL("End Finders Keepers", bkillgame)	
		ADD_WIDGET_BOOL("bcreatepackageareablip0",bcreatepackageareablip[0])			
		ADD_WIDGET_BOOL("bcreatepackageareablip1",bcreatepackageareablip[1])			
		ADD_WIDGET_BOOL("bcreatepackageareablip2",bcreatepackageareablip[2])			
		ADD_WIDGET_BOOL("bcreatepackageareablip3",bcreatepackageareablip[3])			
		ADD_WIDGET_BOOL("bcreatepackageareablip4",bcreatepackageareablip[4])			
		ADD_WIDGET_BOOL("bcreatepackageareablip5",bcreatepackageareablip[5])			
		ADD_WIDGET_BOOL("bcreatepackageareablip6",bcreatepackageareablip[6])			
		
		ADD_WIDGET_BOOL("serverbd.has_package_been_pickedup", serverbd.has_package_been_pickedup)	
		 
		// Gameplay debug, sever only.
		START_WIDGET_GROUP("Server Only Gameplay") 
			ADD_WIDGET_BOOL("End mission", bHostEndMissionNow)			
		STOP_WIDGET_GROUP()		
		// Rumble Vibrations
		START_WIDGET_GROUP("RUMBLE Freq") 
			//ADD_WIDGET_INT_SLIDER("ROUND(g_sMPTunables.igb_finders_keepers_rumble_radius)", g_sMPTunables.igb_finders_keepers_rumble_radius,0.0, 200.0,1.0)
			ADD_WIDGET_INT_SLIDER("ciHTB_RUMBLE_DURATION", ciHTB_RUMBLE_DURATION,0, 1000,1)
			ADD_WIDGET_INT_SLIDER("ciHTB_RUMBLE_DURATION_MULTIPLIER", ciHTB_RUMBLE_DURATION_MULTIPLIER,0, 10000,1)
			
			
		STOP_WIDGET_GROUP()	
		// Rumble Vibrations
		START_WIDGET_GROUP("PACKAGE SOUNDS") 
			//ADD_WIDGET_INT_SLIDER("Sound Freq", soundexpiretimer,0, 10000,5)
			ADD_WIDGET_INT_SLIDER("Sound String", soundtype,0, 4,1)
			ADD_WIDGET_BOOL("Kill Sound", bkillsound)	
		
		STOP_WIDGET_GROUP()	
		
		
		// Data about the server
		START_WIDGET_GROUP("Server BD") 
			ADD_WIDGET_INT_SLIDER("ipackagecounter", serverBD.ipackagecounter,-1, HIGHEST_INT,1)
			ADD_WIDGET_INT_SLIDER("S. Game state", serverBD.iServerGameState,-1, HIGHEST_INT,1)
		STOP_WIDGET_GROUP()	

		// Data about the clients. * = You.
		START_WIDGET_GROUP("Client BD")  				
			REPEAT NUM_NETWORK_PLAYERS iPlayer
				tl63 = "Player "
				tl63 += iPlayer
				IF iPlayer = PARTICIPANT_ID_TO_INT()
					tl63 += "*"
				ENDIF
				START_WIDGET_GROUP(tl63)
					ADD_WIDGET_INT_SLIDER("Game state", playerBD[iPlayer].iClientGameState,-1, HIGHEST_INT,1)
					ADD_WIDGET_INT_SLIDER("PLAYERS SCORE", playerBD[iPlayer].iclientscore,0, 10,1)
					
				STOP_WIDGET_GROUP()
			ENDREPEAT				
		STOP_WIDGET_GROUP()	
		
	STOP_WIDGET_GROUP()
ENDPROC		

PROC UPDATE_WIDGETS()
	IF bWarpToPackage = TRUE
	OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
		IF NET_WARP_TO_COORD(serverBD.vPackageLocation[serverBD.ipackagecounter]+<<1.5, 1.5, 1.5>>, 0, TRUE, FALSE)
			NET_PRINT_TIME() NET_PRINT("     ---------->     FINDERS KEEPERS -  UPDATE_WIDGETS - bWarpToPackage DONE    <----------     ") NET_NL()
			bWarpToPackage = FALSE
		ELSE
			NET_PRINT_TIME() NET_PRINT("     ---------->     FINDERS KEEPERS -  UPDATE_WIDGETS - bWarpToPackage IN PROGRESS    <----------     ") NET_NL()
		ENDIF
		
	ENDIF		
ENDPROC

#ENDIF
FUNC BOOL INIT_CLIENT()
	IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI	
		SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_START_OF_JOB, "GB_BIGM_FK_T", "GB_FDKP_GO3")		
		GB_SET_GANG_BOSS_HELP_BACKGROUND()
	ENDIF	
	PRINT_HELP("GB_FDKP_SETUP")

	GB_SET_COMMON_TELEMETRY_DATA_ON_START()
	//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_GB_CHAL_FINDERS_KEEPERS
	PRINTLN("MAGNATE_GANG_BOSS [GB FINDERS KEEPERS] - INIT_CLIENT ")
	//request_script_audio_bank("EPSILONISM_04_SOUNDSET")
	REQUEST_PTFX_ASSET()
	REQUEST_NAMED_PTFX_ASSET("scr_indep_fireworks")
	INT i
	REPEAT GB_MAX_GANG_SIZE_INCLUDING_BOSS_EXEC  i

	//	serverBD.sSortedPackagesCollected[i].iParticipant = 0
		serverBD.sSortedPackagesCollected[i].playerID = INVALID_PLAYER_INDEX()
		serverBD.sSortedPackagesCollected[i].iScore = 0
		//serverBD.sSortedPackagesCollected[i].iFinishTime = 0
				
	ENDREPEAT
	
	
	
	
	
	RETURN TRUE
ENDFUNC

FUNC BOOL INIT_SERVER()

	serverBD.piGangBoss = GB_GET_LOCAL_PLAYER_MISSION_HOST()
	RESET_NET_TIMER(serverBD.Starttimer)
	PRINTLN("FINDERS KEEPERS MAGNATE_GANG_BOSS [GB FINDERS KEEPERS] - INIT_SERVER")
	
	PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBD.iMatchId1, serverBD.iMatchId2)
	
	MAP_AREA_ZONE players_area = GET_PLAYERS_CURRENT_AREA()
	IF players_area = GENERAL
		players_area = INT_TO_ENUM(MAP_AREA_ZONE,GET_RANDOM_INT_IN_RANGE(0,10))
		PRINTLN("FINDERS KEEPERS GET_PLAYERS_CURRENT_AREA = General. Need to assign a random area. AREA = ", ENUM_TO_INT(players_area))
	ENDIF

	INITIALISE_AMBIENT_ACTIVITY_LEADERBOARD(serverBD.sSortedPackagesCollected, AAL_SORT_DESCENDING)
	
	INT i
	REPEAT NUM_PACKAGES i
		serverBD.vPackageLocation[i]= GENERATE_PACKAGE_LOCATION(players_area)
		IF i > 0
			PRINTLN("FINDERS KEEPERS GET_DISTANCE_BETWEEN_COORDS(serverBD.vPackageLocation[i-1], serverBD.vPackageLocation[i]) = ", GET_DISTANCE_BETWEEN_COORDS(serverBD.vPackageLocation[i-1], serverBD.vPackageLocation[i]) )
		ENDIF
		// make sure next package is 50 meters away
		IF i > 0
			WHILE GET_DISTANCE_BETWEEN_COORDS(serverBD.vPackageLocation[i-1], serverBD.vPackageLocation[i]) < g_sMPTunables.fgbfinderskeepers_package_distance_creation
				PRINTLN("FINDERS KEEPERS location is 50 meters too close to last package ", i)
				
				serverBD.vPackageLocation[i]= GENERATE_PACKAGE_LOCATION(players_area)
				PRINTLN("FINDERS KEEPERS GET_DISTANCE_BETWEEN_COORDS(serverBD.vPackageLocation[i-1], serverBD.vPackageLocation[i]) = ", GET_DISTANCE_BETWEEN_COORDS(serverBD.vPackageLocation[i-1], serverBD.vPackageLocation[i]) )
				//PRINTLN("FINDERS KEEPERS NEW GET_DISTANCE_BETWEEN_COORDS(serverBD.vPackageLocation[i-1], serverBD.vPackageLocation[i]) = ", GET_DISTANCE_BETWEEN_COORDS(serverBD.vPackageLocation[i-1], serverBD.vPackageLocation[i]))
			ENDWHILE
		ENDIF
		
		PRINTLN("FINDERS KEEPERS LOCATIONS FOR PACKAGES serverBD.vPackageLocation[", i,"] = ",serverBD.vPackageLocation[i] )
	ENDREPEAT
	serverBD.MapArea = players_area
	
	
	INITIALISE_AMBIENT_ACTIVITY_LEADERBOARD(serverBD.sSortedPackagesCollected)
	RETURN TRUE

ENDFUNC

FUNC BOOL PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)
	gfinderskeeperslocalplayerhascollectedpickup = FALSE
	CPRINTLN(DEBUG_NET_AMBIENT, DEBUG_NET_AMBIENT, "=== MAGNATE_GANG_BOSS FINDERS KEEPERS PROCESS_PRE_GAME")
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission), missionScriptArgs)
	
	GB_COMMON_BOSS_MISSION_PREGAME() 
	
	//Reserve Entities
	RESERVE_NETWORK_MISSION_OBJECTS(0)
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD,SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD,SIZE_OF(playerBD))
	
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	initial_player_boss = GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID())

	#IF IS_DEBUG_BUILD
	CREATE_WIDGETS()
	#ENDIF
	
	RETURN TRUE
	
ENDFUNC


PROC MAINTAIN_PACKAGE_CLIENT()
	IF serverBD.iServerGameState!= GAME_STATE_END
		IF serverBD.brandomcoordinatesgenerated = TRUE
		AND bcreatepackageareablip[serverBD.ipackagecounter] = FALSE
			// delete old ones first
			
			DELETE_PACKAGE()
			REMOVE_PACKAGE_AREA_BLIP()
			REMOVE_PACKAGE_BLIP()
			REMOVE_PACKAGE_AREA_BLIP_LONG_RANG_BLIP()
			
			ADD_PACKAGE_AREA_BLIP()
			IF Is_There_Any_Current_Objective_Text_From_This_Script()
				Delete_MP_Objective_Text()
			ENDIF

			IF serverBD.ipackagecounter != NUM_PACKAGES
				IF ( serverBD.ipackagecounter % 2= 0) // is integer even
					Print_Objective_Text("GB_FDKP_HELP1")
				ELSE
					Print_Objective_Text("GB_FDKP_HELP3")
				ENDIF
			ELSE
				Print_Objective_Text("GB_FDKP_HELP4")   // find the final package
			ENDIF
		ENDIF
		
		//If player is close to area, create package
		IF NOT bhas_the_package_been_created
			IF IS_THIS_PLAYER_NEAR_PACKAGE(PLAYER_ID())
			//	REMOVE_PACKAGE_AREA_BLIP()

				GB_SET_GANG_BOSS_HELP_BACKGROUND()
				CREATE_PACKAGE()
				REMOVE_PACKAGE_AREA_BLIP_LONG_RANG_BLIP()
				//ADD_PACKAGE_BLIP()
			ENDIF
		ELSE	
			MAINTAIN_PACKAGE_CLIENT_BLIPS_AND_RUMBLE()	
		ENDIF
		
		
		IF IS_THIS_PLAYER_NEAR_PACKAGE(PLAYER_ID())
			IF NOT IS_VECTOR_ZERO(serverBD.vareablipcoordinates )
				GB_MAINTAIN_BOSS_WORK_DISTANT_CHECKS(FMMC_TYPE_GB_CHAL_FINDERS_KEEPERS, serverBD.vareablipcoordinates, bSet, 500, 750)
			ENDIF	
		ENDIF
		
		IF DOES_BLIP_EXIST(PackageBlipArea)
			IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PLAYER_ID(), GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID()), TRUE)
			OR GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), TRUE)
				PLAYER_INDEX piTempBoss
				IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), TRUE)
					piTempBoss = GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID())
				ELSE
					piTempBoss = serverBD.piGangBoss
				ENDIF
				
				IF (GET_AREA_BLIP_ALPHA(piTempBoss) != iBlipAlphaCached)
					PRINTLN("FINDERS KEEPERS - MAINTAIN_PACKAGE_CLIENT - eGBHTB_INIT - Updating blip alpha from: ",iBlipAlphaCached,", to: ",GET_AREA_BLIP_ALPHA(serverBD.piGangBoss))
					iBlipAlphaCached = GET_AREA_BLIP_ALPHA(piTempBoss)
					SET_BLIP_ALPHA(PackageBlipArea, iBlipAlphaCached)
				ENDIF
			ENDIF

		ENDIF
		
		IF	HAS_PACKAGE_BEEN_COLLECTED_CLIENT()
			
		ENDIF
	ELSE
		PRINTLN("[FINDERS KEEPERS] SERVER HAS ENDED GAME")
		playerBD[PARTICIPANT_ID_TO_INT()].iClientGameState = GAME_STATE_END
	ENDIF
ENDPROC
  
  
PROC MAINTAIN_FINAL_SHARD()
	IF serverBD.sSortedPackagesCollected[0].playerID = PLAYER_ID()
//		IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
//			SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "GB_WORK_OVER", "GB_WORK_OVER")
//		ENDIF
		GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_CHAL_FINDERS_KEEPERS, TRUE, sRewardsData)
	ELSE
//		IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
//			SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "GB_WORK_OVER", "GB_WORK_OVER")
//		ENDIF
		GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_CHAL_FINDERS_KEEPERS, FALSE, sRewardsData )
	ENDIF
	bHasdisplayedfinalshard = TRUE
ENDPROC

PROC HANDLE_FINDERS_KEEPERS_COUNTDOWN_AUDIO(INT iRemainingTime)
	IF iRemainingTime < GET_FRAME_TIME()
		MPGlobalsAmbience.iBS_AmbientEventAudioTriggers = 0
		CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - HANDLE_FINDERS_KEEPERS_COUNTDOWN_AUDIO has finished")
	ELIF (iRemainingTime < 36000 AND iRemainingTime > 31000)
		IF initialisemusic != 4
			initialisemusic = 4
			TRIGGER_MUSIC_EVENT("APT_PRE_COUNTDOWN_STOP")
			
		ENDIF
	
	ELIF (iRemainingTime < 31000 AND iRemainingTime > 6000)
		IF initialisemusic != 3
			initialisemusic = 3
			TRIGGER_MUSIC_EVENT("APT_COUNTDOWN_30S")
		ENDIF
	ELIF iRemainingTime < 6000
		IF NOT IS_BIT_SET(MPGlobalsAmbience.iBS_AmbientEventAudioTriggers, ciAE_AUDIO_PLAY_COUNTDOWN)
			PLAY_SOUND_FRONTEND(-1, "5s", "MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
			MPGlobalsAmbience.iBS_AmbientEventAudioTriggers = 0
			SET_BIT(MPGlobalsAmbience.iBS_AmbientEventAudioTriggers, ciAE_AUDIO_PLAY_COUNTDOWN)
			CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - HANDLE_FINDERS_KEEPERS_COUNTDOWN_AUDIO ciAE_AUDIO_PLAY_COUNTDOWN")
		ENDIF
	ENDIF
ENDPROC



PROC DEAL_WITH_END_DISPLAY_MESSAGES_AND_TELEMETRY()
	IF serverBD.eServer_ending_stat = ALL_PACKAGES_COLLECTED
	OR serverBD.ipackagecounter >= NUM_PACKAGES 
		IF serverBD.sSortedPackagesCollected[0].playerID = PLAYER_ID()
		OR serverBD.sSortedPackagesCollected[1].playerID = PLAYER_ID()
		OR IS_GAME_A_DRAW()
			GANG_BOSS_MANAGE_WANTED_CLEAN_UP(TRUE, INVALID_PLAYER_INDEX())
		ENDIF
		
		// Set telemetry and big messages
		IF serverBD.sSortedPackagesCollected[0].playerID = PLAYER_ID()
			GB_SET_COMMON_TELEMETRY_DATA_ON_END(TRUE, GB_TELEMETRY_END_WON)
			SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "GB_WINNER", "GBFK_WIN")				
			PRINTLN("FINDERS KEEPERS TICKER_EVENT_FINDERSKEEPERS_COLLECTED_ALL_PACKAGES Player is winner")
			GB_SET_COMMON_TELEMETRY_DATA_ON_END(TRUE, GB_TELEMETRY_END_WON)
		ELIF IS_GAME_A_DRAW() AND (serverBD.sSortedPackagesCollected[0].playerID = PLAYER_ID() OR serverBD.sSortedPackagesCollected[1].playerID = PLAYER_ID())
			SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "GB_WORK_DRAW", "GBFK_DRAW")
			GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST)
		ELSE
			IF IS_GAME_A_DRAW()
				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "GB_WORK_DRAW", "GBFK_DRAW")
			ELSE
				SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GB_END_OF_JOB_OVER, serverBD.sSortedPackagesCollected[0].playerID,  serverBD.sSortedPackagesCollected[0].iScore, "GBFK_LOSE", "GB_CHAL_OVER" )
			ENDIF
			GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST)
		ENDIF
		

	ELIF serverBD.eServer_ending_stat = TIME_EXPIRES OR GET_CHALLENGE_EXPIRE_TIME()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.Starttimer) <= 0
	
		PRINT_TICKER("GBFK_TEXP")	//~s~You took too long to get to the ~y~hidden package.~s~
		
		IF serverBD.sSortedPackagesCollected[0].playerID = PLAYER_ID()
		OR serverBD.sSortedPackagesCollected[1].playerID = PLAYER_ID()
		OR IS_GAME_A_DRAW()
			GANG_BOSS_MANAGE_WANTED_CLEAN_UP(TRUE, INVALID_PLAYER_INDEX())
		ENDIF
		
		// Set telemetry and big messages
		IF serverBD.sSortedPackagesCollected[0].playerID = PLAYER_ID() AND !IS_GAME_A_DRAW()
			SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "GB_WINNER", "GBFK_WIN")
			GB_SET_COMMON_TELEMETRY_DATA_ON_END(TRUE, GB_TELEMETRY_END_WON)
		ELIF IS_GAME_A_DRAW() AND (serverBD.sSortedPackagesCollected[0].playerID = PLAYER_ID() OR serverBD.sSortedPackagesCollected[1].playerID = PLAYER_ID())
			SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GBFK_DRAW", "GB_CHAL_OVER")
			GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_TIME_OUT)
		ELSE
			IF IS_GAME_A_DRAW()
				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GBFK_DRAW", "GB_CHAL_OVER")
			ELSE
				SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GB_END_OF_JOB_OVER, serverBD.sSortedPackagesCollected[0].playerID, -1, "GBFK_LOSE", "GB_CHAL_OVER")
			ENDIF
			GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST)
		ENDIF
		
	ENDIF
ENDPROC

PROC CLIENT_PROCESSING()
	
	HANDLE_FINDERS_KEEPERS_COUNTDOWN_AUDIO(GET_CHALLENGE_EXPIRE_TIME()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.Starttimer))

	//MAINTAIN_FINDERS_KEEPERS_SORTED_PARTICIPANTS()
	DRAW_BOTTOM_RIGHT_HUD()
	DRAW_CHALLENGE_DPAD_LBD(g_GBLeaderboardStruct.challengeLbdStruct, g_GBLeaderboardStruct.siDpadMovie, SUB_BOSS, g_GBLeaderboardStruct.dpadVariables, 
							g_GBLeaderboardStruct.fmDpadStruct)
	
	
	
	IF bdisplay_final_message = FALSE
		IF serverBD.eServer_ending_stat != STILL_PLAYING
		OR GET_CHALLENGE_EXPIRE_TIME()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.Starttimer) <= 0
			bdisplay_final_message = TRUE
			Delete_MP_Objective_Text()
			DEAL_WITH_END_DISPLAY_MESSAGES_AND_TELEMETRY()
		ENDIF
	ENDIF
	
	
	
	
	#IF IS_DEBUG_BUILD
		UPDATE_WIDGETS()
	#ENDIF
	
	SWITCH serverBD.eGEN_Stage
		CASE GEN_START
			MAINTAIN_PACKAGE_CLIENT()
			//IF 
			IF serverBD.has_package_been_pickedup= TRUE
			AND serverBD.ipackagecounter >= NUM_PACKAGES 
			AND bHasdisplayedfinalshard = FALSE
				
				MAINTAIN_FINAL_SHARD()
			ENDIF
		BREAK 
		
		
		CASE GEN_IN_PACKAGE_AREA
			
		
		BREAK
		
		CASE GEN_SEARCH_FOR_PACKAGE_IN_AREA
		
		BREAK

	ENDSWITCH
	
	

ENDPROC

PROC SERVER_PROCESSING()
	
	SWITCH serverBD.eGEN_Stage
	
		CASE GEN_START
			MAINTAIN_FINDERS_KEEPERS_PLAYER_LEFT()
		BREAK 
		
		CASE GEN_IN_PACKAGE_AREA
			
		BREAK
		
		CASE GEN_SEARCH_FOR_PACKAGE_IN_AREA
		
		BREAK
	ENDSWITCH

ENDPROC

FUNC STRING GET_MISSION_STATE_STRING(INT iState)

	SWITCH(iState)
		CASE GAME_STATE_INIT 				RETURN "GAME_STATE_INIT"
		CASE GAME_STATE_RUNNING 			RETURN "GAME_STATE_RUNNING"
		CASE GAME_STATE_TERMINATE_DELAY 	RETURN "GAME_STATE_TERMINATE_DELAY"
		CASE GAME_STATE_END 				RETURN "GAME_STATE_END"
	ENDSWITCH

	RETURN "UNKNOWN STATE!"

ENDFUNC



//Helper function to get the servers game/mission state
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC

//Helper function to set a clients game/mission state
PROC SET_CLIENT_MISSION_STATE(INT iPart,INT iState)
	playerBD[iPart].iClientGameState = iState
	CPRINTLN(DEBUG_NET_AMBIENT, DEBUG_NET_AMBIENT, "=== MAGNATE_GANG_BOSS FINDERS KEEPERS SET_CLIENT_MISSION_STATE = ",GET_MISSION_STATE_STRING(iState))
	
ENDPROC

//Helper function to set the servers game/mission state
PROC SET_SERVER_MISSION_STATE(INT iState)
	serverBD.iServerGameState = iState
	CPRINTLN(DEBUG_NET_AMBIENT, DEBUG_NET_AMBIENT, "=== MAGNATE_GANG_BOSS FINDERS KEEPERS SET_SERVER_MISSION_STATE = ",GET_MISSION_STATE_STRING(iState))
ENDPROC

FUNC BOOL MISSION_END_CHECK_SERVER()	
	IF serverBD.ipackagecounter >= NUM_PACKAGES 
		IF bHasdisplayedfinalshard = FALSE

				
			PRINTLN("TICKER_EVENT_FINDERSKEEPERS_EXPIRY_TIME TickerEventData.playerID =", GET_PLAYER_NAME(TickerEventData.playerID))
			serverBD.eServer_ending_stat = ALL_PACKAGES_COLLECTED
				
				
			NET_PRINT_TIME() NET_PRINT("     ---------->     FINDERS KEEPERS -  MISSION END - TICKER_EVENT_FINDERSKEEPERS_COLLECTED_ALL_PACKAGES     <----------     ") NET_NL()

			PRINTLN("MAGNATE_GANG_BOSS [GB FINDERS KEEPERS] - serverBD.ipackagecounter >= NUM_PACKAGES - 1")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF HAS_NET_TIMER_EXPIRED(serverBD.Starttimer, GET_CHALLENGE_EXPIRE_TIME())

		serverBD.eServer_ending_stat = TIME_EXPIRES
			//BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT())
		NET_PRINT_TIME() NET_PRINT("     ---------->     FINDERS KEEPERS -  MISSION END - TICKER_EVENT_FINDERSKEEPERS_EXPIRY_TIME     <----------     ") NET_NL()

		PRINTLN("FINDERS KEEPERS MAGNATE_GANG_BOSS [GB FINDERS KEEPERS] - MISSION_END_CHECK_SERVER HAS_NET_TIMER_EXPIRED(serverBD.Starttimer, GET_CHALLENGE_EXPIRE_TIME()) ")
		RETURN TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF bkillgame = TRUE
		OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			PRINTLN("FINDERS KEEPERS bkillgame = TRUE")
			IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
				serverBD.eServer_ending_stat = TIME_EXPIRES
				
			ENDIF
			
			PRINTLN("FINDERS KEEPERS MAGNATE_GANG_BOSS [GB FINDERS KEEPERS] - MISSION_END_CHECK_SERVER IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S) ")
			RETURN TRUE
		ENDIF
	#ENDIF

	RETURN FALSE
ENDFUNC

//Server only function that checks to see if the game mode should now end. For this test script, it if time has passed.
FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET()
	
	#IF IS_DEBUG_BUILD
		IF bHostEndMissionNow
			RETURN TRUE
		ENDIF
	#ENDIF	
	
	IF MISSION_END_CHECK_SERVER()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------

PROC SCRIPT_CLEANUP()
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF bdisplay_final_message = FALSE
		Delete_MP_Objective_Text()
		DEAL_WITH_END_DISPLAY_MESSAGES_AND_TELEMETRY()
		ENDIF
		
		CPRINTLN(DEBUG_NET_AMBIENT, DEBUG_NET_AMBIENT, "=== MAGNATE_GANG_BOSS FINDERS KEEPERS SCRIPT_CLEANUP")
		GB_TIDYUP_SPECTATOR_CAM()

		//RELEASE_CONTEXT_INTENTION(sSpecVars.iGbSpecContext)
		IF NOT g_sMPTunables.bgb_finders_keepers_disable_trackify
			REMOVE_TRACKIFY_MULTIPLE_TARGET(1)
			SET_NUMBER_OF_MULTIPLE_TRACKIFY_TARGETS(0)
			
			ENABLE_MULTIPLAYER_TRACKIFY_APP(FALSE)
		ENDIF
		IF GET_SERVER_MISSION_STATE() = GAME_STATE_END
		ENDIF
		GB_COMMON_BOSS_MISSION_CLEANUP() 
		DELETE_PACKAGE()
		REMOVE_PACKAGE_AREA_BLIP()
		REMOVE_PACKAGE_BLIP()
		REMOVE_PACKAGE_AREA_BLIP_LONG_RANG_BLIP()
		gfinderskeeperslocalplayerhascollectedpickup = FALSE	
		g_sGb_Telemetry_data.sdata.m_match1 = serverBD.iMatchId1
		g_sGb_Telemetry_data.sdata.m_match2 = serverBD.iMatchId2
		PROCESS_CURRENT_BOSS_CHALLENGE_PLAYSTATS(g_sGb_Telemetry_data.sdata, playerBD[PARTICIPANT_ID_TO_INT()].iclientscore, ENUM_TO_INT(serverBD.MapArea))
		IF IS_CELLPHONE_TRACKIFY_IN_USE()
		AND  NOT g_sMPTunables.bgb_finders_keepers_disable_trackify
			HANG_UP_AND_PUT_AWAY_PHONE()
		ENDIF

	ENDIF
	REMOVE_PTFX_ASSET()
	TRIGGER_MUSIC_EVENT("BG_FINDERS_KEEPERS_STOP")
	TERMINATE_THIS_THREAD()
ENDPROC



//----------------------
//	MAIN SCRIPT
//----------------------

SCRIPT(MP_MISSION_DATA missionScriptArgs)
	CPRINTLN(DEBUG_NET_AMBIENT, DEBUG_NET_AMBIENT, "=== MAGNATE_GANG_BOSS FINDERS KEEPERS START")
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NOT PROCESS_PRE_GAME(missionScriptArgs)
			SCRIPT_CLEANUP()
		ENDIF	
	ENDIF
	
	
	GB_COMMON_BOSS_MISSION_SETUP(FMMC_TYPE_GB_CHAL_FINDERS_KEEPERS)
	
	WHILE (TRUE)
	
		MP_LOOP_WAIT_ZERO()
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			// Boss quites
			PRINTLN("[FINDERS KEEPERS] GB_GET_NUM_GOONS_IN_PLAYER_GANG(PLAYER_ID()) = 0")
				IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
				OR GET_GANG_BOSS_PLAYER_INDEX() = PLAYER_ID()
				OR initial_player_boss = PLAYER_ID()
					GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LEFT)
				ELSE
					GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_BOSS_LEFT)
				ENDIF
				

			CPRINTLN(DEBUG_NET_AMBIENT, DEBUG_NET_AMBIENT, "=== MAGNATE_GANG_BOSS FINDERS KEEPERS SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
			IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
			AND GB_GET_NUM_GOONS_IN_PLAYER_GANG(PLAYER_ID()) = 0
				PRINTLN("[FINDERS KEEPERS] GB_GET_NUM_GOONS_IN_PLAYER_GANG(PLAYER_ID()) = 0")
				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_CHAL_OVER", "PIM_MAGM101")
				IF bHasdisplayedfinalshard = FALSE
					GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_CHAL_FINDERS_KEEPERS, FALSE, sRewardsData )
				ENDIF
				GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOW_NUMBERS)
				SCRIPT_CLEANUP()
			ELIF GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID()) = INVALID_PLAYER_INDEX()
			OR NOT NETWORK_IS_PLAYER_ACTIVE(GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID()))
				PRINTLN("[FINDERS KEEPERS] NETWORK_IS_PLAYER_ACTIVE(GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID()))")
				IF DOES_PLAYER_OWN_OFFICE(GB_GET_LOCAL_PLAYER_GANG_BOSS())
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_CHAL_OVER", "GBTER_BIGBSLFTC")
				ELSE
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_CHAL_OVER", "GBTER_BIG_BSLFT")
				ENDIF
				IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
				OR GET_GANG_BOSS_PLAYER_INDEX() = PLAYER_ID()
				OR initial_player_boss = PLAYER_ID()
					GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LEFT)
				ELSE
					GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_BOSS_LEFT)
				ENDIF
				
				IF bHasdisplayedfinalshard = FALSE
					GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_CHAL_FINDERS_KEEPERS, FALSE, sRewardsData )
				ENDIF
				SCRIPT_CLEANUP()
			ENDIF
		ENDIF
		
		IF GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION()
			GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_FORCED)
			PRINTLN("[FINDERS KEEPERS] - GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		IF GB_SHOULD_KILL_ACTIVE_BOSS_MISSION()
			GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_FORCED)
			PRINTLN("[FINDERS KEEPERS] GB_SHOULD_KILL_ACTIVE_BOSS_MISSION")
			SCRIPT_CLEANUP()
		ENDIF
		
		
		GB_MAINTAIN_SPECTATE(sSpecVars)
		IF NETWORK_IS_GAME_IN_PROGRESS()
			SWITCH(GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT()))
			
				CASE GAME_STATE_INIT
					IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
						GB_SET_ITS_SAFE_FOR_SPEC_CAM()	
						IF GB_LAUNCH_CHALLENGE_WITH_SPEC_CAM_IS_ACTIVE(sSpecVars)
							IF INIT_CLIENT()
								SET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT(),GAME_STATE_RUNNING)
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE GAME_STATE_RUNNING
					IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
						CLIENT_PROCESSING()
						
					ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
						IF bHasdisplayedfinalshard = FALSE
							MAINTAIN_FINAL_SHARD()
						ENDIF
						// Display bottom 
						DRAW_BOTTOM_RIGHT_HUD()
						DRAW_CHALLENGE_DPAD_LBD(g_GBLeaderboardStruct.challengeLbdStruct, g_GBLeaderboardStruct.siDpadMovie, SUB_BOSS, g_GBLeaderboardStruct.dpadVariables, 
							g_GBLeaderboardStruct.fmDpadStruct)
						IF GB_MAINTAIN_BOSS_END_UI(BOSS_END_UI) 
							SET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT(),GAME_STATE_END)
						ENDIF
					ENDIF
				BREAK
				
				CASE GAME_STATE_END
					IF GB_SHOULD_RUN_SPEC_CAM()
						GB_SET_SHOULD_CLOSE_SPECTATE()
					ELSE
						SCRIPT_CLEANUP()
					ENDIF
				BREAK
				
			ENDSWITCH
		ENDIF
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
			SWITCH(GET_SERVER_MISSION_STATE())
			
				CASE GAME_STATE_INIT
					IF INIT_SERVER()
						
						SET_SERVER_MISSION_STATE(GAME_STATE_RUNNING)
						
					ENDIF
				BREAK
				
				CASE GAME_STATE_RUNNING
					IF serverbd.has_package_been_pickedup = FALSE
						IF  HAS_PACKAGE_BEEN_COLLECTED_SERVER()

						ENDIF
					ENDIF
					
					IF HAVE_MISSION_END_CONDITIONS_BEEN_MET()
						SET_SERVER_MISSION_STATE(GAME_STATE_END)
					ELSE
						SERVER_PROCESSING()
						
					ENDIF
					
					//Bug 2555965
					IF GB_SHOULD_KILL_ACTIVE_BOSS_MISSION()
						SET_SERVER_MISSION_STATE(GAME_STATE_END)
					ENDIF
				BREAK
				
				CASE GAME_STATE_END
					
				BREAK
				
			ENDSWITCH
		
		ENDIF
		
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
