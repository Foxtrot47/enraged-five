//////////////////////////////////////////////////////////////////////////////////////////
// Name:        GB_BELLYBEAST.sc														//
// Description: Steal 2 vehicles from a police building and deliver them with 4 stars	//
// Written by:  Ryan Elliott															//
// Date: 		12/10/2015																//
//////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "net_mission.sch"


//----------------------
//	INCLUDES
//----------------------
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "flow_public_core_override.sch"
USING "net_wait_zero.sch"
USING "net_mission.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"
USING "net_common_functions.sch"
USING "DM_Leaderboard.sch"
USING "net_gang_boss.sch"

//----------------------
//	**TEMP HARD CODED**
//----------------------

INT iDropOffTEMP
BOOL bSetTEMP

//----------------------
//	ENUM
//----------------------

ENUM GBBELLYBEAST_END_REASON
	GBBELLYBEAST_ENDREASON_DELIVERED,
	GBBELLYBEAST_ENDREASON_DESTROYED,
	GBBELLYBEAST_ENDREASON_NOT_ENOUGH_PLAYERS,
	GBBELLYBEAST_ENDREASON_TIMEUP,
	GBBELLYBEAST_ENDREASON_BOSS_LEFT,
	GBBELLYBEAST_ENDREASON_DELIVEREDANDDESTROYED,
	GBBELLYBEAST_ENDREASON_FAIL,
	
	GBBELLYBEAST_ENDREASON_MAX 	// Always keep last
ENDENUM

//----------------------
//	CONSTANTS
//----------------------

CONST_INT MAX_VEHICLES		4
CONST_INT MAX_DROPOFF		3
CONST_INT MAX_VARIANT		6
CONST_INT MAX_MODELS		6

CONST_INT SUPERCAR_MODELS	2

CONST_INT CITY_DEFAULT		2
CONST_INT COUNTRY_DEFAULT	4

CONST_INT DROP_BIT_OFFSET	4
CONST_INT DSTRY_BIT_OFFSET	8

CONST_INT REWARD_TIMEOUT	15000

//----------------------
//	Local Bitset
//----------------------

INT iLocalBitset

CONST_INT GBBELLYBEAST_BS_SHARD_DONE			0
CONST_INT GBBELLYBEAST_BS_FIRST_HELP_DONE		1
CONST_INT GBBELLYBEAST_BS_SOLO_WARN_DONE		2
CONST_INT GBBELLYBEAST_BS_INTRO_DONE			3
CONST_INT GBBELLYBEAST_BS_ENTERED_VEHICLE		4
CONST_INT GBBELLYBEAST_BS_VEHICLE_BLIPS_FLASHED	5
CONST_INT GBBELLYBEAST_BS_DROP_OFF_BLIPPED		6
CONST_INT GBBELLYBEAST_BS_PLAYER_DIED			7
CONST_INT GBBELLYBEAST_BS_INIT_WANTED			8
CONST_INT GBBELLYBEAST_BS_INTRO_TEXT_SENT		9
CONST_INT GBBELLYBEAST_BS_DESTROYED_HELP_DONE	10
CONST_INT GBBELLYBEAST_BS_END_SHARD_DONE		11
CONST_INT GBBELLYBEAST_BS_LOSE_COPS_HELP_DONE	12

//----------------------
//	STRUCT
//----------------------

//----------------------
//	VARIABLES
//----------------------

BLIP_INDEX biVehicles[MAX_VEHICLES]
BLIP_INDEX biDropOff[MAX_VEHICLES]
BLIP_INDEX biStation

VECTOR vLastPlayerLocation
VECTOR vClosestAction

BOOL bSet

INT iCurrentWantedLevel

SCRIPT_TIMER stClosestActionTimer

GB_STRUCT_BOSS_END_UI sEndUI

INT iCurrentTime

//----------------------
//	GAME STATE
//----------------------

CONST_INT GAME_STATE_INIT 							0
CONST_INT GAME_STATE_RUNNING						1
CONST_INT GAME_STATE_TERMINATE_DELAY				2
CONST_INT GAME_STATE_END							3

ENUM GBBELLYBEAST_RUN_STAGE
	GBBELLYBEAST_INIT = 0,
	GBBELLYBEAST_STEAL,
	GBBELLYBEAST_DESTROY,
	GBBELLYBEAST_REWARDS,
	GBBELLYBEAST_END,
	
	GBBELLYBEAST_MAX_STAGES			/* Always have at the end */
ENDENUM

//----------------------
//	BROADCAST VARIABLES
//----------------------

// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData

	INT iServerGameState = GAME_STATE_INIT
	
	INT iServerBitSet
	
	INT iVariant
	INT iDropOff
	
	BOOL bIsCity
	
	// Vehicle Data
	INT iTotalVehicles
	NETWORK_INDEX niVehicles[MAX_VEHICLES]
	MODEL_NAMES mnVehicles[MAX_VEHICLES]
	VECTOR vStartLocation[MAX_VEHICLES]
	FLOAT fStartHeading[MAX_VEHICLES]
	
	INT iDeliveredCount = 0
	INT iDestroyedCount = 0
	
	VECTOR vDropOffLocation[MAX_VEHICLES]

	SCRIPT_TIMER stWorkTimer
	SCRIPT_TIMER stRewardTimeout
	SCRIPT_TIMER stAutostart
	
	GBBELLYBEAST_RUN_STAGE eGBBELLYBEAST_Stage = GBBELLYBEAST_INIT
	GBBELLYBEAST_END_REASON eGBBELLYBEAST_EndReason = GBBELLYBEAST_ENDREASON_DELIVERED
	
	INT iMatchId1
	INT iMatchId2
	
ENDSTRUCT


//Server bitset
CONST_INT SERVER_BS_DELIVERED_1					0
CONST_INT SERVER_BS_DELIVERED_2					1
CONST_INT SERVER_BS_DELIVERED_3					2
CONST_INT SERVER_BS_DELIVERED_4					3
CONST_INT SERVER_BS_DROPOFF_USED_1				4
CONST_INT SERVER_BS_DROPOFF_USED_2				5
CONST_INT SERVER_BS_DROPOFF_USED_3				6
CONST_INT SERVER_BS_DROPOFF_USED_4				7
CONST_INT SERVER_BS_DESTROYED_1					8
CONST_INT SERVER_BS_DESTROYED_2					9
CONST_INT SERVER_BS_DESTROYED_3					10
CONST_INT SERVER_BS_DESTROYED_4					11
CONST_INT SERVER_BS_WORK_STARTED				12
CONST_INT SERVER_BS_DELIVERED					13
CONST_INT SERVER_BS_ALL_VEHICLES_HAVE_DRIVERS	14
CONST_INT SERVER_BS_ONE_VEHICLE_REMAINING		15
CONST_INT SERVER_BS_TIME_UP						16
CONST_INT SERVER_BS_VEHICLE_ENTERED				17
CONST_INT SERVER_BS_BOSS_LEFT					18
CONST_INT SERVER_BS_NOT_ENOUGH_PLAYERS			19
CONST_INT SERVER_BS_REWARDS_GIVEN				20
CONST_INT SERVER_BS_ALL_DESTROYED				21
CONST_INT SERVER_BS_WORK_OVER					22
CONST_INT SERVER_BS_OUTRO_DONE					23
CONST_INT SERVER_BS_AUTOSTART					24

ServerBroadcastData serverBD

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	
	INT iClientGameState = GAME_STATE_INIT
	
	INT iClientBitSet
	
	INT iDestroyedCount = 0
	
	GBBELLYBEAST_RUN_STAGE eGBBELLYBEAST_Stage = GBBELLYBEAST_INIT
ENDSTRUCT

//Client bitset
CONST_INT CLIENT_BS_DELIVERED_1					0
CONST_INT CLIENT_BS_DELIVERED_2					1
CONST_INT CLIENT_BS_DELIVERED_3					2
CONST_INT CLIENT_BS_DELIVERED_4					3
CONST_INT CLIENT_BS_DROPOFF_USED_1				4
CONST_INT CLIENT_BS_DROPOFF_USED_2				5
CONST_INT CLIENT_BS_DROPOFF_USED_3				6
CONST_INT CLIENT_BS_DROPOFF_USED_4				7
CONST_INT CLIENT_BS_DESTROYED_1					8
CONST_INT CLIENT_BS_DESTROYED_2					9
CONST_INT CLIENT_BS_DESTROYED_3					10
CONST_INT CLIENT_BS_DESTROYED_4					11
CONST_INT CLIENT_BS_START_TIMER					12
CONST_INT CLIENT_BS_NEAR_STATION				13
CONST_INT CLIENT_BS_IS_VEH_DRIVER				14
CONST_INT CLIENT_BS_IS_IN_VEH					15
CONST_INT CLIENT_BS_CLOSE_TO_DROPOFF			16
CONST_INT CLIENT_BS_LOST_FORCED_WANTED_LEVEL	17
CONST_INT CLIENT_BS_DELIVERED_ANY_ALREADY		18
CONST_INT CLIENT_BS_TIME_UP						19
CONST_INT CLIENT_BS_REWARDS_GIVEN				20
CONST_INT CLIENT_BS_OUTRO_DONE					21

PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

//----------------------
//	DBG FUNCTIONS
//----------------------

#IF IS_DEBUG_BUILD
BOOL bKillScript
BOOL bFakeTimeUp

/// PURPOSE: Create widgets for GB_BELLYBEAST
PROC CREATE_WIDGETS()

	START_WIDGET_GROUP("Belly of the Beast")
		START_WIDGET_GROUP("Server Only")
			ADD_WIDGET_BOOL("Kill Script", bKillScript)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	
	CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST ===  [CREATE_WIDGETS] - Widgets created.")
ENDPROC

#ENDIF

//----------------------
//	TUNABLES
//----------------------

/// PURPOSE: Get overall duration of the work.
FUNC INT GB_GET_WORK_DURATION()
	RETURN g_sMPTunables.igb_belly_time_limit
ENDFUNC

/// PURPOSE: How much cash is rewarded for delivering or destroying a car
FUNC INT GB_GET_CAR_VALUE()
	RETURN g_sMPTunables.igb_belly_default_cash_reward_per_car                               		
ENDFUNC

/// PURPOSE: How much cash is rewarded for delivering or destroying a bike
FUNC INT GB_GET_BIKE_VALUE()
	RETURN g_sMPTunables.igb_belly_default_cash_reward_per_bike                              
ENDFUNC

/// PURPOSE: How much RP is rewarded for delivering or destroying a car
FUNC INT GB_GET_CAR_RP_REWARD()
	RETURN g_sMPTunables.igb_belly_default_rp_reward_per_car                               		
ENDFUNC

/// PURPOSE: How much RP is rewarded for delivering or destroying a bike
FUNC INT GB_GET_BIKE_RP_REWARD()
	RETURN g_sMPTunables.igb_belly_default_rp_reward_per_bike                             
ENDFUNC

/// PURPOSE: HALF (2) - How much less non-gang players receive for destroying
FUNC INT GB_GET_FMPLAYER_REDUCTION()
	RETURN 2
ENDFUNC

/// PURPOSE: Wanted level that players are given when stealing a city vehicle
FUNC INT GB_GET_CITY_WANTED_LEVEL()
	RETURN g_sMPTunables.igb_belly_wanted_city 		
ENDFUNC

/// PURPOSE: Wanted level that players are given when stealing a country vehicle
FUNC INT GB_GET_COUNTRY_WANTED_LEVEL()
	RETURN g_sMPTunables.igb_belly_wanted_country		
ENDFUNC

/// PURPOSE: Wanted level that is dropped to when players near the drop off point for city vehicles
FUNC INT GB_GET_CITY_WANTED_LEVEL_DROP()
	RETURN g_sMPTunables.igb_belly_wanted_city_drop		
ENDFUNC

/// PURPOSE: Wanted level that is dropped to when players near the drop off point for country vehicles
FUNC INT GB_GET_COUNTRY_WANTED_LEVEL_DROP()
	RETURN g_sMPTunables.igb_belly_wanted_country_drop 		
ENDFUNC

/// PURPOSE: Distance required for the wanted level to become natural and drop to GB_GET_CITY_WANTED_LEVEL_DROP() or GB_GET_COUNTRY_WANTED_LEVEL_DROP()
FUNC INT GB_GET_WANTED_LEVEL_DROP_DISTANCE()
	RETURN g_sMPTunables.igb_belly_distance_wanted_drop 		
ENDFUNC

/// PURPOSE: Distance a vehicle must be from spawn 
FUNC INT GB_GET_VEHICLE_PROXIMITY_FOR_REAPPLYING_WANTED_LEVEL()
	RETURN g_sMPTunables.igb_belly_proximity_to_vehicle_wanted_applied   		
ENDFUNC

//----------------------
//	GET/SET FUNCTIONS
//----------------------

FUNC STRING GET_MISSION_STAGE_STRING(GBBELLYBEAST_RUN_STAGE eState)

	SWITCH(eState)
		CASE GBBELLYBEAST_INIT						RETURN "GBBELLYBEAST_INIT"
		CASE GBBELLYBEAST_STEAL						RETURN "GBBELLYBEAST_STEAL"
		CASE GBBELLYBEAST_DESTROY					RETURN "GBBELLYBEAST_DESTROY"
		CASE GBBELLYBEAST_REWARDS 					RETURN "GBBELLYBEAST_REWARDS"
		CASE GBBELLYBEAST_END		 				RETURN "GBBELLYBEAST_END"
	ENDSWITCH

	RETURN "UNKNOWN STATE!"

ENDFUNC

FUNC STRING GET_MISSION_END_REASON_STRING(GBBELLYBEAST_END_REASON eEndReason)

	SWITCH(eEndReason)
		CASE GBBELLYBEAST_ENDREASON_DELIVERED						RETURN "GBBELLYBEAST_ENDREASON_DELIVERED"
		CASE GBBELLYBEAST_ENDREASON_DESTROYED						RETURN "GBBELLYBEAST_ENDREASON_DESTROYED"
		CASE GBBELLYBEAST_ENDREASON_FAIL		 					RETURN "GBBELLYBEAST_ENDREASON_FAIL"
		CASE GBBELLYBEAST_ENDREASON_BOSS_LEFT		 				RETURN "GBBELLYBEAST_ENDREASON_BOSS_LEFT"
		CASE GBBELLYBEAST_ENDREASON_NOT_ENOUGH_PLAYERS				RETURN "GBBELLYBEAST_ENDREASON_NOT_ENOUGH_PLAYERS"
		CASE GBBELLYBEAST_ENDREASON_DELIVEREDANDDESTROYED			RETURN "GBBELLYBEAST_ENDREASON_DELIVEREDANDDESTROYED"
		CASE GBBELLYBEAST_ENDREASON_TIMEUP		 					RETURN "GBBELLYBEAST_ENDREASON_TIMEUP"
	ENDSWITCH

	RETURN "UNKNOWN STATE!"

ENDFUNC

/// PURPOSE: Return server mission stage.
FUNC  GBBELLYBEAST_RUN_STAGE GET_SERVER_MISSION_STAGE()
	RETURN serverBD.eGBBELLYBEAST_Stage
ENDFUNC

/// PURPOSE: SERVER ONLY: Set server mission stage.
PROC SET_SERVER_MISSION_STAGE(GBBELLYBEAST_RUN_STAGE eStage)
	CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST ===  [SET_SERVER_MISSION_STAGE] - serverBD.eGBBELLYBEAST_Stage = ", GET_MISSION_STAGE_STRING(eStage))
	serverBD.eGBBELLYBEAST_Stage = eStage
ENDPROC


/// PURPOSE: Get server mission end reason.
FUNC GBBELLYBEAST_END_REASON GET_SERVER_MISSION_END_REASON()
	RETURN serverBD.eGBBELLYBEAST_EndReason
ENDFUNC

/// PURPOSE: SERVER ONLY: Set server mission end reason.
/// GBBELLYBEAST_ENDREASON_FLED
/// GBBELLYBEAST_ENDREASON_TIMEUP
PROC SET_SERVER_MISSION_END_REASON(GBBELLYBEAST_END_REASON eGBEndReason)
	serverBD.eGBBELLYBEAST_EndReason = eGBEndReason
	CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST ===  [SET_SERVER_MISSION_END_REASON] - serverBD.eGBBELLYBEAST_EndReason = ", GET_MISSION_END_REASON_STRING(eGBEndReason))
ENDPROC


/// PURPOSE: Return server mission stage.
FUNC GBBELLYBEAST_RUN_STAGE GET_CLIENT_MISSION_STAGE()
	RETURN playerBD[PARTICIPANT_ID_TO_INT()].eGBBELLYBEAST_Stage
ENDFUNC

/// PURPOSE: SERVER ONLY: Set server mission stage.
PROC SET_CLIENT_MISSION_STAGE(GBBELLYBEAST_RUN_STAGE eStage)
	playerBD[PARTICIPANT_ID_TO_INT()].eGBBELLYBEAST_Stage = eStage
	CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST ===  [SET_CLIENT_MISSION_STAGE] - playerBD[", PARTICIPANT_ID_TO_INT(), "].iClientGameState = ", GET_MISSION_STAGE_STRING(eStage))
ENDPROC


/// PURPOSE: SERVER ONLY: 2 - Set the number of vehicles the players have to steal.
PROC GB_SET_VEHICLE_TOTAL(INT iNum)
	serverBD.iTotalVehicles = iNum
ENDPROC

/// PURPOSE: 2 - Get the number of vehicles the players have to steal.
FUNC INT GB_GET_VEHICLE_TOTAL()
	RETURN serverBD.iTotalVehicles
ENDFUNC

/// PURPOSE: SERVER ONLY: 2 - Set the model of that vehicle in the array.
PROC GB_SET_VEHICLE_MODEL(INT iVehicle, MODEL_NAMES mnModel)
	serverBD.mnVehicles[iVehicle] = mnModel
ENDPROC

/// PURPOSE: 2 - Get the model of that vehicle in the array.
FUNC MODEL_NAMES GB_GET_VEHICLE_MODEL(INT iVehicle)
	RETURN serverBD.mnVehicles[iVehicle]
ENDFUNC

/// PURPOSE: SERVER ONLY: 2 - Set the vehicle 
PROC GB_SET_VEHICLE(INT iVehicle, NETWORK_INDEX niVehicle)
	serverBD.niVehicles[iVehicle] = niVehicle
ENDPROC

/// PURPOSE: 2 - Get the vehicle 
FUNC NETWORK_INDEX GB_GET_VEHICLE(INT iVehicle)
	RETURN serverBD.niVehicles[iVehicle]
ENDFUNC

/// PURPOSE: SERVER ONLY: Set that vehicle in the array's start location
PROC GB_SET_VEHICLE_LOCATION(INT iVehicle, VECTOR vLocation)
	serverBD.vStartLocation[iVehicle] = vLocation
ENDPROC

/// PURPOSE: Get that vehicle in the array's start location
FUNC VECTOR GB_GET_VEHICLE_LOCATION(INT iVehicle)
	RETURN serverBD.vStartLocation[iVehicle]
ENDFUNC

/// PURPOSE: SERVER ONLY: Set that vehicle in the array's start heading
PROC GB_SET_VEHICLE_HEADING(INT iVehicle, FLOAT fHeading)
	serverBD.fStartHeading[iVehicle] = fHeading
ENDPROC

/// PURPOSE: Get that vehicle in the array's start heading
FUNC FLOAT GB_GET_VEHICLE_HEADING(INT iVehicle)
	RETURN serverBD.fStartHeading[iVehicle]
ENDFUNC


/// PURPOSE: SERVER ONLY: Set where the vehicles have to be dropped off
PROC GB_SET_DROPOFF_LOCATION(INT iLocation, VECTOR vLocation)
	serverBD.vDropOffLocation[iLocation] = vLocation
ENDPROC

/// PURPOSE: Get where the vehicles have to be dropped off
FUNC VECTOR GB_GET_DROPOFF_LOCATION(INT iLocation)
	RETURN serverBD.vDropOffLocation[iLocation]
ENDFUNC


/// PURPOSE: SERVER ONLY: Set the current variant (location)
PROC GB_SET_VARIANT(INT iVariant)
	serverBD.iVariant = iVariant
ENDPROC

/// PURPOSE: Get the current variant (location)
FUNC INT GB_GET_VARIANT()
	RETURN serverBD.iVariant
ENDFUNC

/// PURPOSE: SERVER ONLY: Set the current dropoff (location)
PROC GB_SET_DROPOFF(INT iDropoff)
	serverBD.iDropoff = iDropoff
ENDPROC

/// PURPOSE: Get the current dropoff (location)
FUNC INT GB_GET_DROPOFF()
	RETURN serverBD.iDropoff
ENDFUNC

/// PURPOSE: SERVER ONLY: Set readable data to check if the station is in the city
PROC GB_SET_INCITY(BOOL bIsCity)
	serverBD.bIsCity = bIsCity
ENDPROC

/// PURPOSE: Get if the police station is in the city
FUNC BOOL GB_IS_IN_CITY()
	RETURN serverBD.bIsCity
ENDFUNC

FUNC STRING GB_GET_VARIANT_STRING()
	SWITCH GB_GET_VARIANT()
		CASE GB_PS_VESPUCCI			RETURN "GB_BB_GT_PS0"	
		CASE GB_PS_LA_MESA			RETURN "GB_BB_GT_PS1"	
		CASE GB_PS_MISSION_ROW		RETURN "GB_BB_GT_PS2"	
		CASE GB_PS_VINEWOOD_HILLS 	RETURN "GB_BB_GT_PS3"	
		CASE GB_PS_SANDY_SHORES		RETURN "GB_BB_GT_PS4"	
		CASE GB_PS_PALETO_BAY		RETURN "GB_BB_GT_PS5"	
	ENDSWITCH
	RETURN "GB_BB_GT_PS0"
ENDFUNC

// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------
TIME_DATATYPE timeLeaveEvent[MAX_VEHICLES]
FUNC BOOL CLEANUP_VEHICLES()
	INT iIndex
	REPEAT GB_GET_VEHICLE_TOTAL() iIndex
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GB_GET_VEHICLE(iIndex))
			ENTITY_INDEX eiVehicle = NET_TO_ENT(GB_GET_VEHICLE(iIndex))
			IF NOT IS_ENTITY_DEAD(eiVehicle)
				IF EMPTY_AND_LOCK_VEHICLE(NET_TO_VEH(GB_GET_VEHICLE(iIndex)), timeLeaveEvent[iIndex])
					CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [SCRIPT_CLEANUP] EMPTY")
					SET_ENTITY_AS_NO_LONGER_NEEDED(eiVehicle)
				ELSE
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

PROC FILL_TELEMETRY()
	// 0 = Cars 
	// 1 = Bikes
	INT iVehicleType = 0
	IF NOT GB_IS_IN_CITY()
		iVehicleType = 1
	ENDIF
	
	g_sGb_Telemetry_data.sdata.m_match1 = serverBD.iMatchId1
	g_sGb_Telemetry_data.sdata.m_match2 = serverBD.iMatchId2
	
	PROCESS_CURRENT_BOSS_WORK_PLAYSTATS(iVehicleType, GB_GET_VARIANT(), GB_GET_DROPOFF())
ENDPROC

PROC CLEANUP_WORK_BLIPS()

	// Remove station blip
	IF DOES_BLIP_EXIST(biStation)
		REMOVE_BLIP(biStation)
	ENDIF

	INT iIndex
	REPEAT GB_GET_VEHICLE_TOTAL() iIndex
		// Remove drop off blips
		IF DOES_BLIP_EXIST(biDropOff[iIndex])
			REMOVE_BLIP(biDropOff[iIndex])
		ENDIF
		
		// Remove vehicle blips
		IF DOES_BLIP_EXIST(biVehicles[iIndex])
			REMOVE_BLIP(biVehicles[iIndex])
		ENDIF
	ENDREPEAT
ENDPROC

PROC KICK_PLAYER_OUT_VEHICLE_ON_CLEANUP()
	INT iIndex
	REPEAT GB_GET_VEHICLE_TOTAL() iIndex
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GB_GET_VEHICLE(iIndex))
			VEHICLE_INDEX viVehicle = NET_TO_VEH(GB_GET_VEHICLE(iIndex))
			IF NOT IS_ENTITY_DEAD(viVehicle)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viVehicle)
					BROADCAST_LEAVE_VEHICLE(SPECIFIC_PLAYER(PLAYER_ID()), FALSE, 0, 0, FALSE, FALSE)
					CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [KICK_PLAYER_OUT_VEHICLE_ON_CLEANUP] - Player was in vehicle on cleanup. Booting from vehicle.")
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC INT GET_VEHICLES_DESTROYED(BOOL bReturnBossCount = FALSE, BOOL bPayBossCut = FALSE)

	// Loop through local player's goons in order to take credit for their hard work
	IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
	AND NOT bPayBossCut
		IF bReturnBossCount
		OR GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
			INT iPart, iTotalDestroyed
			REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iPart
				PARTICIPANT_INDEX piParticipant = INT_TO_PARTICIPANTINDEX(iPart)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(piParticipant)
					PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(piParticipant)
					IF NETWORK_IS_PLAYER_ACTIVE(piPlayer)
						IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(piPlayer, GB_GET_LOCAL_PLAYER_GANG_BOSS())
							iTotalDestroyed += playerBD[iPart].iDestroyedCount
							CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [GET_VEHICLES_DESTROYED] - ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(piParticipant)), " is a member of local player's organisation. They destroyed ", playerBD[iPart].iDestroyedCount, " vehicles. Total gang destroyed count is currently ", iTotalDestroyed)
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			// Total number of vehicles local player's gang destroyed
			CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [GET_VEHICLES_DESTROYED] - Finished gang loop. Final destroyed count: ", iTotalDestroyed)
			RETURN iTotalDestroyed
		ELSE
			// Goons don't earn cash for destroying vehicles. Cash goes to their Boss.
			CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [GET_VEHICLES_DESTROYED] - Local player is in rival gang. Boss takes credit for destroyed vehicles. Returning 0. ")
			RETURN 0
		ENDIF
	ENDIF
	// Local player wasn't in a gang so returning their own score
	CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [GET_VEHICLES_DESTROYED] - Local player isn't in a gang or bPayBossCut is true. Destroyed count: ", playerBD[PARTICIPANT_ID_TO_INT()].iDestroyedCount)
	RETURN playerBD[PARTICIPANT_ID_TO_INT()].iDestroyedCount
ENDFUNC

PROC CALCULATE_REWARDS(INT& iCash, INT& iRP, BOOL bReturnBossCount = FALSE, BOOL bPayBossCut = FALSE)
	FLOAT fCash, fRP, fBossCash
	INT iBossCash, iVehiclesDestroyedByGang
	IF GB_IS_LOCAL_PLAYER_MEMBER_OF_THIS_GANG(GB_GET_LOCAL_PLAYER_MISSION_HOST())
		IF GB_IS_IN_CITY()
			iCash = GB_GET_CAR_VALUE() * serverBD.iDeliveredCount
			iRP = GB_GET_CAR_RP_REWARD() * serverBD.iDeliveredCount
		ELSE
			iCash = GB_GET_BIKE_VALUE() * serverBD.iDeliveredCount
			iRP = GB_GET_BIKE_RP_REWARD() * serverBD.iDeliveredCount
		ENDIF
		CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [CALCULATE_REWARDS] - KEY ORGANIZATION - iCash = ", iCash, " - iRP = ", iRP)
	ELSE
		INT iBossParticipant
		
		IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
			// If local player is the boss, get how many vehicles their gang destroyed excluding them
			IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
				iVehiclesDestroyedByGang = (GET_VEHICLES_DESTROYED(bReturnBossCount, bPayBossCut) - playerBD[PARTICIPANT_ID_TO_INT()].iDestroyedCount)
			ELSE
				iBossParticipant = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
				IF iBossParticipant != (-1)
					iVehiclesDestroyedByGang = (GET_VEHICLES_DESTROYED(bReturnBossCount, bPayBossCut) - playerBD[iBossParticipant].iDestroyedCount)
				ENDIF
			ENDIF
			
			IF GB_IS_IN_CITY()
				// Get how much money the gang earned in total
				iCash = GB_GET_CAR_VALUE() * GET_VEHICLES_DESTROYED(bReturnBossCount, bPayBossCut)
				iRP = GB_GET_CAR_RP_REWARD() * GET_VEHICLES_DESTROYED(TRUE) 
				
				// Get how much money the boss earned personally. Take that away from the total to prevent being paid twice for a vehicle. This will be 0 for Goons so should be fine unwrapped
				iBossCash = GB_GET_CAR_VALUE() * iVehiclesDestroyedByGang
				iCash = iCash - iBossCash
			ELSE
				// Get how much money the gang earned in total
				iCash = GB_GET_BIKE_VALUE() * GET_VEHICLES_DESTROYED(bReturnBossCount, bPayBossCut)
				iRP = GB_GET_BIKE_RP_REWARD() * GET_VEHICLES_DESTROYED(TRUE)
				
				// Get how much money the boss earned personally. Take that away from the total to prevent being paid twice for a vehicle. This will be 0 for Goons so should be fine unwrapped
				iBossCash = GB_GET_CAR_VALUE() * iVehiclesDestroyedByGang
				iCash = iCash - iBossCash
			ENDIF
			
			fCash = TO_FLOAT(iCash) * g_sMPTunables.fgb_rival_org_cash_reward_percentage
			fRP = TO_FLOAT(iRP) * g_sMPTunables.fgb_rival_org_rp_reward_percentage
			
			fBossCash = TO_FLOAT(iBossCash) * g_sMPTunables.fgb_rival_org_cash_reward_percentage
			
			iCash = ROUND(fCash)
			iRP = ROUND(fRP)
			
			iBossCash = ROUND(fBossCash)
			
			CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [CALCULATE_REWARDS] - RIVAL ORGANIZATION - Cash Percentage Tunable = ", g_sMPTunables.fgb_rival_org_cash_reward_percentage, " - RP Percentage Tunable = ", g_sMPTunables.fgb_rival_org_rp_reward_percentage)
			
			// Take the 10% cut of the cash made from Goons doing vehicles
			IF GB_GET_NUM_GOONS_IN_LOCAL_GANG() > 0
				GB_TAKE_AGENCY_CUT(iBossCash)
			ENDIF
			
			iCash = iCash + iBossCash
				
			CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [CALCULATE_REWARDS] - RIVAL ORGANIZATION - iCash = ", iCash, " - iRP = ", iRP)
		ELSE
			IF GB_IS_IN_CITY()
				iCash = GB_GET_CAR_VALUE() * GET_VEHICLES_DESTROYED()
				iRP = GB_GET_CAR_RP_REWARD() * GET_VEHICLES_DESTROYED()
			ELSE
				iCash = GB_GET_BIKE_VALUE() * GET_VEHICLES_DESTROYED()
				iRP = GB_GET_BIKE_RP_REWARD() * GET_VEHICLES_DESTROYED()
			ENDIF
			
			fCash = (TO_FLOAT(iCash) * g_sMPTunables.fgb_solo_cash_reward_percentage) * g_sMPTunables.fgb_belly_event_multiplier_cash
			fRP = (TO_FLOAT(iRP) * g_sMPTunables.fgb_solo_rp_reward_percentage) * g_sMPTunables.fgb_belly_event_multiplier_rp
			
			iCash = ROUND(fCash)
			iRP = ROUND(fRP)
			
			CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [CALCULATE_REWARDS] - SOLO PLAYER - Cash Percentage Tunable = ", g_sMPTunables.fgb_solo_cash_reward_percentage, " - RP Percentage Tunable = ", g_sMPTunables.fgb_solo_rp_reward_percentage)
			CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [CALCULATE_REWARDS] - SOLO PLAYER - iCash = ", iCash, " - iRP = ", iRP)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_TELEMETRY(BOOL bStraightToCleanup = FALSE)
	BOOL bWon
	INT iEndReason = GB_TELEMETRY_END_LOST
	
	IF GB_IS_LOCAL_PLAYER_MEMBER_OF_THIS_GANG(GB_GET_LOCAL_PLAYER_MISSION_HOST())
		IF serverBD.iDeliveredCount > 0
			bWon = TRUE
			iEndReason = GB_TELEMETRY_END_WON
		ENDIF
	ELSE
		IF GET_VEHICLES_DESTROYED(TRUE) > 0
			bWon = TRUE
			iEndReason = GB_TELEMETRY_END_WON
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_BOSS_LEFT)
		iEndReason = GB_TELEMETRY_END_BOSS_LEFT
	ELIF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_TIME_UP)
		iEndReason = GB_TELEMETRY_END_TIME_OUT
	ENDIF
	
	IF bStraightToCleanup
		iEndReason = GB_TELEMETRY_END_LEFT
	ENDIF
	
	GB_SET_COMMON_TELEMETRY_DATA_ON_END(bWon, iEndReason)
ENDPROC

PROC SCRIPT_CLEANUP(BOOL bStraightToCleanup = FALSE)
	CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [SCRIPT_CLEANUP]")

	Clear_Any_Objective_Text_From_This_Script()
	
	IF GET_MAX_WANTED_LEVEL() < 5
		SET_MAX_WANTED_LEVEL(5)
	ENDIF
	
	CLEANUP_WORK_BLIPS()
	
	PROCESS_TELEMETRY(bStraightToCleanup)
	FILL_TELEMETRY()
	
	IF NETWORK_GET_NUM_PARTICIPANTS() > 1
		KICK_PLAYER_OUT_VEHICLE_ON_CLEANUP()
		GB_COMMON_BOSS_MISSION_CLEANUP()
		TERMINATE_THIS_THREAD()
	ELSE
		IF CLEANUP_VEHICLES()
			GB_COMMON_BOSS_MISSION_CLEANUP()
			TERMINATE_THIS_THREAD()
		ENDIF
	ENDIF
ENDPROC

//----------------------
//	FUNCTIONS
//----------------------

FUNC VECTOR SELECT_RANDOM_DROPOFF_LOCATION(INT iLocation)
	IF iLocation = 0
		SWITCH GB_GET_DROPOFF()
			CASE 0
				// The variant is in the city, choose a country drop off and vice versa
				IF GB_IS_IN_CITY()
					RETURN <<1685.7369, 6435.9717, 31.3601>> //
				ENDIF
				RETURN <<255.4336, -3057.4063, 4.7974>> //
			BREAK
			CASE 1
				// The variant is in the city, choose a country drop off and vice versa
				IF GB_IS_IN_CITY()
					RETURN <<-1576.5239, 5159.8252, 18.7791>> //
				ENDIF
				RETURN <<-965.0558, -2072.7173, 8.4059>> //
			BREAK
			CASE 2
				// The variant is in the city, choose a country drop off and vice versa
				IF GB_IS_IN_CITY()
					RETURN <<756.7225, 4179.8389, 39.6685>> //
				ENDIF
				RETURN <<-1209.3175, -1305.5566, 3.7185>>
			BREAK
		ENDSWITCH
	ELIF iLocation = 1
		SWITCH GB_GET_DROPOFF()
			CASE 0
				// The variant is in the city, choose a country drop off and vice versa
				IF GB_IS_IN_CITY()
					RETURN <<1682.3861, 6434.0015, 31.1545>>
				ENDIF
				RETURN <<248.4273, -3064.1624, 4.7802>>
			BREAK
			CASE 1
				// The variant is in the city, choose a country drop off and vice versa
				IF GB_IS_IN_CITY()
					RETURN <<-1581.5725, 5159.5654, 18.5702>>
				ENDIF
				RETURN <<-971.9658, -2065.5261, 8.4059>>
			BREAK
			CASE 2
				// The variant is in the city, choose a country drop off and vice versa
				IF GB_IS_IN_CITY()
					RETURN <<762.5063, 4179.5767, 39.6335>>
				ENDIF
				RETURN <<-1207.2748, -1310.0059, 3.7944>>
			BREAK
		ENDSWITCH
	ELIF iLocation = 2
		SWITCH GB_GET_DROPOFF()
			CASE 0
				RETURN <<252.0648, -3060.9673, 4.7768>>
			BREAK
			CASE 1
				RETURN <<-975.4440, -2061.8416, 8.4059>>
			BREAK
			CASE 2
				RETURN <<-1222.1241, -1309.2848, 3.5209>>
			BREAK
		ENDSWITCH
	ELIF iLocation = 3
		SWITCH GB_GET_DROPOFF()
			CASE 0
				RETURN <<244.6635, -3068.0481, 4.7864>>
			BREAK
			CASE 1
				RETURN <<-968.5905, -2069.1487, 8.4059>>
			BREAK
			CASE 2
				RETURN <<-1229.6523, -1321.2296, 3.0921>>
			BREAK
		ENDSWITCH
	ENDIF
	
	CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST ===  [SELECT_RANDOM_DROPOFF_LOCATION] - Returning default. GB_GET_DROPOFF() = ", GB_GET_DROPOFF(), " - MAX_DROPOFF = ", MAX_DROPOFF, " - Is it too big?")
	RETURN <<1685.7369, 6435.9717, 31.3601>>
ENDFUNC

/// PURPOSE: Initialise vehicles with correct models, locations for the current variant
PROC SETUP_VARIANT()
	VECTOR vLoc0, vLoc1, vLoc2, vLoc3
	
	GB_SET_VEHICLE_TOTAL(CITY_DEFAULT)
	
	SWITCH (GB_GET_VARIANT())
		CASE GB_PS_VESPUCCI
			vLoc0 = <<-1039.6226, -855.3737, 3.8758>>
			vLoc1 = <<-1112.3624, -855.0657, 12.5310>>
			vLoc2 = <<0.0, 0.0, 0.0>>
			vLoc3 = <<0.0, 0.0, 0.0>>
			
			GB_SET_INCITY(TRUE)
			IF GB_GET_NUM_GOONS_IN_PLAYER_GANG(GB_GET_LOCAL_PLAYER_MISSION_HOST()) = 0
				GB_SET_VEHICLE_TOTAL(1)
			ENDIF
		
			GB_SET_VEHICLE_MODEL(0, PIGALLE)
			GB_SET_VEHICLE_HEADING(0, 237.6320)
			
			GB_SET_VEHICLE_MODEL(1, RHAPSODY)
			GB_SET_VEHICLE_HEADING(1, 218.7510)
			
			GB_SET_DROPOFF_LOCATION(0, SELECT_RANDOM_DROPOFF_LOCATION(0))
			GB_SET_DROPOFF_LOCATION(1, SELECT_RANDOM_DROPOFF_LOCATION(1))

			CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST ===  [SETUP_VARIANT] - GB_PS_VESPUCCI")
		BREAK
		
		CASE GB_PS_LA_MESA
			vLoc0 = <<861.2779, -1357.7346, 25.0962>>
			vLoc1 = <<833.7587, -1414.2885, 25.1543>>
			vLoc2 = <<0.0, 0.0, 0.0>>
			vLoc3 = <<0.0, 0.0, 0.0>>
			
			GB_SET_INCITY(TRUE)
			IF GB_GET_NUM_GOONS_IN_PLAYER_GANG(GB_GET_LOCAL_PLAYER_MISSION_HOST()) = 0
				GB_SET_VEHICLE_TOTAL(1)
			ENDIF
			
			GB_SET_VEHICLE_MODEL(0, COMET2)
			GB_SET_VEHICLE_HEADING(0, 269.7720)
			
			GB_SET_VEHICLE_MODEL(1, BANSHEE)
			GB_SET_VEHICLE_HEADING(1, 332.3690)
			
			GB_SET_DROPOFF_LOCATION(0, SELECT_RANDOM_DROPOFF_LOCATION(0))
			GB_SET_DROPOFF_LOCATION(1, SELECT_RANDOM_DROPOFF_LOCATION(1))
			
			CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST ===  [SETUP_VARIANT] - GB_PS_LA_MESA")
		BREAK
		
		CASE GB_PS_MISSION_ROW
			vLoc0 = <<471.1763, -1024.2334, 27.1807>>
			vLoc1 = <<407.7268, -993.1101, 28.2665>>
			vLoc2 = <<0.0, 0.0, 0.0>>
			vLoc3 = <<0.0, 0.0, 0.0>>
			
			GB_SET_INCITY(TRUE)
			IF GB_GET_NUM_GOONS_IN_PLAYER_GANG(GB_GET_LOCAL_PLAYER_MISSION_HOST()) = 0
				GB_SET_VEHICLE_TOTAL(1)
			ENDIF
			
			GB_SET_VEHICLE_MODEL(0, DOMINATOR)
			GB_SET_VEHICLE_HEADING(0, 95.7556)
				
			GB_SET_VEHICLE_MODEL(1, GAUNTLET)
			GB_SET_VEHICLE_HEADING(1, 231.8613)
			
			GB_SET_DROPOFF_LOCATION(0, SELECT_RANDOM_DROPOFF_LOCATION(0))
			GB_SET_DROPOFF_LOCATION(1, SELECT_RANDOM_DROPOFF_LOCATION(1))
			
			CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST ===  [SETUP_VARIANT] - GB_PS_MISSION_ROW")
		BREAK
		
		CASE GB_PS_VINEWOOD_HILLS
			vLoc0 = <<530.3759, -29.6076, 69.6295>>
			vLoc1 = <<628.4064, 23.6166, 86.6466>>
			vLoc2 = <<0.0, 0.0, 0.0>>
			vLoc3 = <<0.0, 0.0, 0.0>>
			
			GB_SET_INCITY(TRUE)
			IF GB_GET_NUM_GOONS_IN_PLAYER_GANG(GB_GET_LOCAL_PLAYER_MISSION_HOST()) = 0
				GB_SET_VEHICLE_TOTAL(1)
			ENDIF
			
			GB_SET_VEHICLE_MODEL(0, FELTZER3)
			GB_SET_VEHICLE_HEADING(0, 31.0119)
			
			GB_SET_VEHICLE_MODEL(1, COQUETTE3)
			GB_SET_VEHICLE_HEADING(1, 71.2592)
			
			GB_SET_DROPOFF_LOCATION(0, SELECT_RANDOM_DROPOFF_LOCATION(0))
			GB_SET_DROPOFF_LOCATION(1, SELECT_RANDOM_DROPOFF_LOCATION(1))
			
			CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST ===  [SETUP_VARIANT] - GB_PS_VINEWOOD_HILLS")
		BREAK
		
		CASE GB_PS_SANDY_SHORES
			vLoc0 = <<1854.0391, 3675.5242, 32.7389>>
			vLoc1 = <<1867.2257, 3694.6963, 32.6526>>
			vLoc2 = <<1866.0198, 3682.9761, 32.7085>>
			vLoc3 = <<1861.5964, 3681.0051, 32.7102>>
			
			GB_SET_INCITY(FALSE)
			GB_SET_VEHICLE_TOTAL(GB_GET_NUM_GOONS_IN_PLAYER_GANG(GB_GET_LOCAL_PLAYER_MISSION_HOST())+1)
			
			GB_SET_VEHICLE_MODEL(0, BLAZER)
			GB_SET_VEHICLE_HEADING(0, 30.2891)
			
			GB_SET_VEHICLE_MODEL(1, BLAZER)
			GB_SET_VEHICLE_HEADING(1, 209.6525)
			
			GB_SET_VEHICLE_MODEL(2, BLAZER)
			GB_SET_VEHICLE_HEADING(1, 19.0028)
			
			GB_SET_VEHICLE_MODEL(3, BLAZER)
			GB_SET_VEHICLE_HEADING(1, 209.3371)
			
			GB_SET_DROPOFF_LOCATION(0, SELECT_RANDOM_DROPOFF_LOCATION(0))
			GB_SET_DROPOFF_LOCATION(1, SELECT_RANDOM_DROPOFF_LOCATION(1))
			GB_SET_DROPOFF_LOCATION(2, SELECT_RANDOM_DROPOFF_LOCATION(2))
			GB_SET_DROPOFF_LOCATION(3, SELECT_RANDOM_DROPOFF_LOCATION(3))
			
			CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST ===  [SETUP_VARIANT] - GB_PS_SANDY_SHORES")
		BREAK
		
		CASE GB_PS_PALETO_BAY
			vLoc0 = <<-451.4124, 5998.2739, 30.3405>>
			vLoc1 = <<-461.0853, 6047.0620, 30.3405>>
			vLoc2 = <<-438.0848, 5980.2686, 30.4901>>
			vLoc3 = <<-434.6339, 5983.8965, 30.4901>>
			
			GB_SET_INCITY(FALSE)
			GB_SET_VEHICLE_TOTAL(GB_GET_NUM_GOONS_IN_PLAYER_GANG(GB_GET_LOCAL_PLAYER_MISSION_HOST())+1)
			
			GB_SET_VEHICLE_MODEL(0, LECTRO)
			GB_SET_VEHICLE_HEADING(0, 267.2382)
			
			GB_SET_VEHICLE_MODEL(1, LECTRO)
			GB_SET_VEHICLE_HEADING(1, 315.4168)
			
			GB_SET_VEHICLE_MODEL(2, LECTRO)
			GB_SET_VEHICLE_HEADING(1,  251.2614)
			
			GB_SET_VEHICLE_MODEL(3, LECTRO)
			GB_SET_VEHICLE_HEADING(1, 44.8117)
			
			GB_SET_DROPOFF_LOCATION(0, SELECT_RANDOM_DROPOFF_LOCATION(0))
			GB_SET_DROPOFF_LOCATION(1, SELECT_RANDOM_DROPOFF_LOCATION(1))
			GB_SET_DROPOFF_LOCATION(2, SELECT_RANDOM_DROPOFF_LOCATION(2))
			GB_SET_DROPOFF_LOCATION(3, SELECT_RANDOM_DROPOFF_LOCATION(3))
			
			CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST ===  [SETUP_VARIANT] - GB_PS_PALETO_BAY")
		BREAK
	ENDSWITCH
	
	IF GB_GET_VEHICLE_TOTAL() = CITY_DEFAULT
		GB_SET_VEHICLE_LOCATION(0, vLoc0)
		GB_SET_VEHICLE_LOCATION(1, vLoc1)
		CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST ===  [SETUP_VARIANT] - City location okay. Continuing script.")
	ELSE
		GB_SET_VEHICLE_LOCATION(0, vLoc0)
		GB_SET_VEHICLE_LOCATION(1, vLoc1)
		GB_SET_VEHICLE_LOCATION(2, vLoc2)
		GB_SET_VEHICLE_LOCATION(3, vLoc3)
		CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST ===  [SETUP_VARIANT] - Country location okay. Continuing script.")
	ENDIF
	
	#IF IS_DEBUG_BUILD
	VECTOR vVehicle0Location, vVehicle1Location, vDropOff0Location,  vDropOff1Location
	vVehicle0Location = GB_GET_VEHICLE_LOCATION(0)
	vVehicle1Location = GB_GET_VEHICLE_LOCATION(1)
	vDropOff0Location = GB_GET_DROPOFF_LOCATION(0)
	vDropOff1Location = GB_GET_DROPOFF_LOCATION(1)
	
	CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST ===  [SETUP_VARIANT] - Vehicle 0 - Location: ", vVehicle0Location, " - Heading: ", GB_GET_VEHICLE_HEADING(0))
	CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST ===  [SETUP_VARIANT] - Vehicle 1 - Location: ", vVehicle1Location, " - Heading: ", GB_GET_VEHICLE_HEADING(1))
	CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST ===  [SETUP_VARIANT] - DropOff 0 Location: ", vDropOff0Location)
	CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST ===  [SETUP_VARIANT] - DropOff 1 Location: ", vDropOff1Location)
	#ENDIF
ENDPROC

PROC GIVE_VEHICLE_RANDOM_PAINTJOB(VEHICLE_INDEX viVehicle)
	INT iMaxLivery = GET_VEHICLE_LIVERY_COUNT(viVehicle)
	INT iMaxColour = GET_NUMBER_OF_VEHICLE_COLOURS(viVehicle)
	INT iColour1
	
	IF iMaxLivery > 0
		INT iLivery = GET_RANDOM_INT_IN_RANGE(0, iMaxLivery)
		SET_VEHICLE_LIVERY(viVehicle, iLivery)
		CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST ===  [GIVE_VEHICLE_RANDOM_PAINTJOB] - Vehicle has livery.")
	ENDIF
	
	iColour1 = GET_RANDOM_INT_IN_RANGE(0, iMaxColour)
	SET_VEHICLE_COLOUR_COMBINATION(viVehicle, iColour1)
	
	CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST ===  [GIVE_VEHICLE_RANDOM_PAINTJOB] - Set random colour combination.")
ENDPROC

FUNC BOOL CREATE_VEHICLES()
	INT iIndex, iCount
	
	REPEAT GB_GET_VEHICLE_TOTAL() iIndex
		IF REQUEST_LOAD_MODEL(GB_GET_VEHICLE_MODEL(iIndex))
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(GB_GET_VEHICLE(iIndex))
				IF CREATE_NET_VEHICLE(serverBD.niVehicles[iIndex], GB_GET_VEHICLE_MODEL(iIndex), GB_GET_VEHICLE_LOCATION(iIndex), GB_GET_VEHICLE_HEADING(iIndex), DEFAULT, DEFAULT, DEFAULT, TRUE, FALSE)
					
					CALL_SET_ALLOW_RAMMING_SOOP_OR_RAMP(NET_TO_VEH(GB_GET_VEHICLE(iIndex)))
					GIVE_VEHICLE_RANDOM_PAINTJOB(NET_TO_VEH(GB_GET_VEHICLE(iIndex)))
				
					SET_VEHICLE_DOORS_LOCKED_FOR_NON_SCRIPT_PLAYERS(NET_TO_VEH(GB_GET_VEHICLE(iIndex)), TRUE)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_VEH(GB_GET_VEHICLE(iIndex)), TRUE)
					
					SET_VEHICLE_TYRES_CAN_BURST(NET_TO_VEH(GB_GET_VEHICLE(iIndex)), FALSE)
					SET_VEHICLE_HAS_STRONG_AXLES(NET_TO_VEH(GB_GET_VEHICLE(iIndex)), TRUE)
					SET_VEHICLE_STRONG(NET_TO_VEH(GB_GET_VEHICLE(iIndex)), TRUE)
					SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(NET_TO_VEH(GB_GET_VEHICLE(iIndex)), FALSE)
					
					SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON(NET_TO_VEH(GB_GET_VEHICLE(iIndex)), FALSE)
					SET_VEHICLE_ALLOW_NO_PASSENGERS_LOCKON(NET_TO_VEH(GB_GET_VEHICLE(iIndex)), FALSE)
					SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(NET_TO_VEH(GB_GET_VEHICLE(iIndex)), FALSE)
					
					LOCK_DOORS_WHEN_NO_LONGER_NEEDED(NET_TO_VEH(GB_GET_VEHICLE(iIndex)))
					SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(NET_TO_VEH(GB_GET_VEHICLE(iIndex)), TRUE)
					SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(NET_TO_VEH(GB_GET_VEHICLE(iIndex)), TRUE)
	
					SET_VEHICLE_AUTOMATICALLY_ATTACHES(NET_TO_VEH(GB_GET_VEHICLE(iIndex)), FALSE)
					SET_ENTITY_CAN_ONLY_BE_DAMAGED_BY_SCRIPT_PARTICIPANTS(NET_TO_VEH(GB_GET_VEHICLE(iIndex)), TRUE)
					SET_ENTITY_INVINCIBLE(NET_TO_VEH(GB_GET_VEHICLE(iIndex)), TRUE)
					
					NETWORK_FADE_IN_ENTITY(NET_TO_VEH(GB_GET_VEHICLE(iIndex)), TRUE)
					
					IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh", DECOR_TYPE_INT)
                    	DECOR_SET_INT(NET_TO_VEH(GB_GET_VEHICLE(iIndex)), "Not_Allow_As_Saved_Veh", MP_NOT_ALLOWED_AS_SAVED_VEHICLE_DEC_SIMEON)
						CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST ===  [CREATE_VEHICLES] - Vehicle ", iIndex, " set as 'Not_Allow_As_Saved_Veh'.")
  					ENDIF
					
					IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
						INT iDecoratorValue
						IF DECOR_EXIST_ON(NET_TO_VEH(GB_GET_VEHICLE(iIndex)), "MPBitset")
							iDecoratorValue = DECOR_GET_INT(NET_TO_VEH(GB_GET_VEHICLE(iIndex)), "MPBitset")
						ENDIF
						SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_PASSIVE_MODE_VEHICLE)
						SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_MODDABLE_VEHICLE)
						DECOR_SET_INT(NET_TO_VEH(GB_GET_VEHICLE(iIndex)), "MPBitset", iDecoratorValue)
						
						CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST ===  [CREATE_VEHICLES] - Vehicle ", iIndex, " set as 'MP_DECORATOR_BS_NON_PASSIVE_MODE_VEHICLE'.")
						CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST ===  [CREATE_VEHICLES] - Vehicle ", iIndex, " set as 'MP_DECORATOR_BS_NON_MODDABLE_VEHICLE'.")
					ENDIF
				
					SET_MODEL_AS_NO_LONGER_NEEDED(GB_GET_VEHICLE_MODEL(iIndex))
					
					CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [CREATE_VEHICLES] - Vehicle ", iIndex, " created at ", serverBD.vStartLocation[iIndex], " with ", GET_MODEL_NAME_FOR_DEBUG(GB_GET_VEHICLE_MODEL(iIndex)), " model.")		
				ENDIF
			ELSE
				iCount++
				CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST ===  [CREATE_VEHICLES] - ", iCount, " vehicles created. ", (GB_GET_VEHICLE_TOTAL()-iCount), " remaining.")
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF iCount = GB_GET_VEHICLE_TOTAL()
		CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST ===  [CREATE_VEHICLES] - All ", GB_GET_VEHICLE_TOTAL(), " vehicles created.")
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC STRING GET_MISSION_STATE_STRING(INT iState)

	SWITCH(iState)
		CASE GAME_STATE_INIT 				RETURN "GAME_STATE_INIT"
		CASE GAME_STATE_RUNNING 			RETURN "GAME_STATE_RUNNING"
		CASE GAME_STATE_TERMINATE_DELAY 	RETURN "GAME_STATE_TERMINATE_DELAY"
		CASE GAME_STATE_END 				RETURN "GAME_STATE_END"
	ENDSWITCH

	RETURN "UNKNOWN STATE!"

ENDFUNC

//Helper function to get a clients game/mission state
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iClientGameState
ENDFUNC

//Helper function to get the servers game/mission state
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC

//Helper function to set a clients game/mission state
PROC SET_CLIENT_MISSION_STATE(INT iPlayer,INT iState)
	playerBD[iPlayer].iClientGameState = iState
	CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === SET_CLIENT_MISSION_STATE = ",GET_MISSION_STATE_STRING(iState))
ENDPROC

//Helper function to set the servers game/mission state
PROC SET_SERVER_MISSION_STATE(INT iState)
	serverBD.iServerGameState = iState
	CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === SET_SERVER_MISSION_STATE = ",GET_MISSION_STATE_STRING(iState))
ENDPROC
/// PURPOSE: Initialise script with target and method of kill selected by gang boss.
FUNC BOOL INIT_GBBELLYBEAST()
	PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBD.iMatchId1, serverBD.iMatchId2)
	IF NOT bSetTEMP
		iDropOffTEMP = GET_RANDOM_INT_IN_RANGE(0, MAX_DROPOFF)
		CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST ===  [INIT_GBBELLYBEAST] - PASS - Drop off set to ", iDropOffTEMP, " - Variant set to ", MPGlobalsAmbience.sMagnateGangBossData.iVariant)
		
		IF MPGlobalsAmbience.sMagnateGangBossData.iVariant = -1
			SET_SERVER_MISSION_STATE(GAME_STATE_END)
			CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST ===  [INIT_GBBELLYBEAST] - FAIL - Killing script. Variant = -1. Host has probably switched during initialising.")			
			RETURN FALSE
		ENDIF
		
		GB_SET_DROPOFF(iDropOffTEMP)
		GB_SET_VARIANT(MPGlobalsAmbience.sMagnateGangBossData.iVariant)
		
		SETUP_VARIANT()
		
		bSetTEMP = TRUE
	ENDIF
	
	IF CREATE_VEHICLES()	
		CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST ===  [INIT_GBBELLYBEAST] - PASS - Vehicles set up. Continuing script.")
		RETURN TRUE
	ELSE
		CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST ===  [INIT_GBBELLYBEAST] - FAIL - Vehicles not set up yet. Not continuing yet.")
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GB_SHOULD_EVENT_BE_HIDDEN()
	IF GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
		CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [GB_SHOULD_EVENT_BE_HIDDEN] - TRUE.")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GB_SHOULD_UI_BE_HIDDEN()
	IF NOT MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
	OR GB_SHOULD_EVENT_BE_HIDDEN()
	OR GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) = GB_UI_LEVEL_NONE
		CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [GB_SHOULD_UI_BE_HIDDEN] - TRUE.")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

///////////////////////////////////////////////////// 
//              VEHICLE LOCKING				       //
///////////////////////////////////////////////////// 

PROC KICK_PLAYER_FROM_GANG_VEHICLE(PLAYER_INDEX playerID, VEHICLE_INDEX viVehicle)

	PED_INDEX pedID = GET_PLAYER_PED(playerID)
	
	IF IS_VEHICLE_DRIVEABLE(viVehicle)
		IF IS_PED_IN_VEHICLE(pedID, viVehicle,TRUE)
			BROADCAST_LEAVE_VEHICLE(SPECIFIC_PLAYER(playerID), FALSE, 0, 0, FALSE, FALSE)
			CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST ===  [KICK_PLAYER_FROM_GANG_VEHICLE] - Kicking ",GET_PLAYER_NAME(playerID)," from vehicle ", NATIVE_TO_INT(viVehicle))
		ENDIF
	ENDIF

ENDPROC

PROC SET_VEHICLE_LOCKS(VEHICLE_INDEX viVehicle)

	INT iIndex
	REPEAT NUM_NETWORK_PLAYERS iIndex
		PLAYER_INDEX playerID = INT_TO_PLAYERINDEX(iIndex)
		
		IF IS_NET_PLAYER_OK(playerID, FALSE)
		
			//If player is a member of the gang, unlock doors for them
			IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerID, GB_GET_LOCAL_PLAYER_MISSION_HOST())
			OR GB_IS_PLAYER_JOINING_THIS_GANG(playerID, GB_GET_LOCAL_PLAYER_MISSION_HOST())
				SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(viVehicle, playerID, FALSE)
				CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST ===  [SET_VEHICLE_LOCKS] - Vehicle unlocked for Boss ",GET_PLAYER_NAME(playerID)," as ",GET_PLAYER_NAME( GB_GET_LOCAL_PLAYER_MISSION_HOST()), " is their Boss.")
			ELSE				
				KICK_PLAYER_FROM_GANG_VEHICLE(playerID, viVehicle)
				SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(viVehicle, playerID, TRUE)
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC

PROC INIT_VEHICLE_LOCKS()

	INT iIndex
	REPEAT GB_GET_VEHICLE_TOTAL() iIndex
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GB_GET_VEHICLE(iIndex))
			IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(GB_GET_VEHICLE(iIndex)))
				IF NETWORK_HAS_CONTROL_OF_ENTITY(NET_TO_VEH(GB_GET_VEHICLE(iIndex)))
					IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE)
						SET_VEHICLE_LOCKS(NET_TO_VEH(GB_GET_VEHICLE(iIndex)))
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

	CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST ===  [MAINTAIN_VEHICLE_LOCKS] - Done.")
ENDPROC



PROC HANDLE_SHARD_HELP_INTRO()
	IF GB_SHOULD_UI_BE_HIDDEN()
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iLocalBitset, GBBELLYBEAST_BS_SHARD_DONE)
		IF NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
		
			// Organisation objective text
			IF GB_IS_LOCAL_PLAYER_MEMBER_OF_THIS_GANG(GB_GET_LOCAL_PLAYER_MISSION_HOST())
			
				IF IS_BIT_SET(iLocalBitset, GBBELLYBEAST_BS_FIRST_HELP_DONE)
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						IF GB_GET_VEHICLE_TOTAL() = 1
							// The Cops have the vehicle under close surveillance. You will gain a Wanted Level when the vehicle is stolen, so plan and coordinate your movements.
							PRINT_HELP_NO_SOUND("GB_BB_WARN1") 
						ELSE
							// The Police department has the vehicles under close surveillance. Your whole Organization will gain a Wanted Level when either vehicle is stolen, so plan and coordinate your movements.
							PRINT_HELP_NO_SOUND("GB_BB_WARN")
						ENDIF
						GB_SET_GANG_BOSS_HELP_BACKGROUND()
						
						CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [HANDLE_SHARD_HELP_INTRO] - Printing GB_BB_WARN help.")
						SET_BIT(iLocalBitset, GBBELLYBEAST_BS_INTRO_DONE)
					ENDIF
				ELSE
					IF PLAYER_ID() != GB_GET_LOCAL_PLAYER_MISSION_HOST()
						// Your Boss has started Belly of the Beast. Steal the vehicles with the evidence from the police station and deliver them to the drop off. Members of ~a~~s~ will earn $~1~ for each vehicle delivered.
						PRINT_HELP_NO_SOUND("GB_BB_GSTART")
						CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [HANDLE_SHARD_HELP_INTRO] - Printing GB_BB_GSTART help.")
					ELSE
						IF GB_GET_VEHICLE_TOTAL() = 1
							// You have started Belly of the Beast. Steal the vehicle with the evidence from the police station and deliver it to the drop off. You will earn $~1~ for delivering it.
							PRINT_HELP_NO_SOUND("GB_BB_BSTART1")
						ELSE
							// You have started Belly of the Beast. Steal the vehicles with the evidence from the police station and deliver them to the drop off. Members of ~a~~s~ will earn $~1~ for each vehicle delivered.
							PRINT_HELP_NO_SOUND("GB_BB_BSTART")
						ENDIF
						CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [HANDLE_SHARD_HELP_INTRO] - Printing GB_BB_BSTART help.")
					ENDIF
					GB_SET_GANG_BOSS_HELP_BACKGROUND()
		
					SET_BIT(iLocalBitset, GBBELLYBEAST_BS_FIRST_HELP_DONE)
				ENDIF
			// Freemode players
			ELSE
				IF GB_GET_VEHICLE_TOTAL() = 1
					// ~a~~s~ have started Belly of the Beast. Destroy the vehicle before they deliver it to the drop off.
					PRINT_HELP_WITH_LITERAL_STRING_WITH_HUD_COLOUR_NO_SOUND("GB_BB_FSTART1", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(GB_GET_LOCAL_PLAYER_MISSION_HOST()), GET_PLAYER_HUD_COLOUR(GB_GET_LOCAL_PLAYER_MISSION_HOST()))
				ELSE
					// ~a~~s~ have started Belly of the Beast. Destroy the vehicles before they deliver them to the drop off.
					PRINT_HELP_WITH_LITERAL_STRING_WITH_HUD_COLOUR_NO_SOUND("GB_BB_FSTART", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(GB_GET_LOCAL_PLAYER_MISSION_HOST()), GET_PLAYER_HUD_COLOUR(GB_GET_LOCAL_PLAYER_MISSION_HOST()))
				ENDIF
				GB_SET_GANG_BOSS_HELP_BACKGROUND()
	
				CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [HANDLE_SHARD_HELP_INTRO] - Printing GB_BB_FSTART help.")
				SET_BIT(iLocalBitset, GBBELLYBEAST_BS_INTRO_DONE)
			ENDIF
		ENDIF
	ELSE
		IF GB_IS_LOCAL_PLAYER_MEMBER_OF_THIS_GANG(GB_GET_LOCAL_PLAYER_MISSION_HOST())
			IF GB_GET_VEHICLE_TOTAL() = 1
				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_START_OF_JOB, "GB_BB_ST_START", "GB_BB_SS_START1")
			ELSE
				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_START_OF_JOB, "GB_BB_ST_START", "GB_BB_SS_START")
			ENDIF
		ELSE
			IF GB_GET_VEHICLE_TOTAL() = 1
				SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_START_OF_JOB, "GB_BB_ST_START", "GB_BB_SS_DSTRY1", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(GB_GET_LOCAL_PLAYER_MISSION_HOST()), GET_PLAYER_HUD_COLOUR(GB_GET_LOCAL_PLAYER_MISSION_HOST()))
			ELSE
				SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_START_OF_JOB, "GB_BB_ST_START", "GB_BB_SS_DSTRY", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(GB_GET_LOCAL_PLAYER_MISSION_HOST()), GET_PLAYER_HUD_COLOUR(GB_GET_LOCAL_PLAYER_MISSION_HOST()))
			ENDIF
		ENDIF
		
		GB_SET_COMMON_TELEMETRY_DATA_ON_START()
		SET_BIT(iLocalBitset, GBBELLYBEAST_BS_SHARD_DONE)
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_START_TIMER)
		CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [HANDLE_SHARD_HELP_INTRO] - Display start shard.")
	ENDIF
ENDPROC

PROC HANDLE_SHARD_OUTRO()
	STRING strOrganization
	HUD_COLOURS hcOrganization
	
	IF GB_SHOULD_UI_BE_HIDDEN()
		SET_BIT(iLocalBitset, GBBELLYBEAST_BS_END_SHARD_DONE)
		CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [HANDLE_SHARD_OUTRO] - GBBELLYBEAST_REWARDS - UI hidden.")
	ENDIF
	
	IF Is_There_Any_Current_Objective_Text()
		Clear_Any_Objective_Text_From_This_Script()
		CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [HANDLE_SHARD_OUTRO] - GBBELLYBEAST_REWARDS - Clearing objective text.")
	ENDIF
	
	IF IS_BIT_SET(iLocalBitset, GBBELLYBEAST_BS_END_SHARD_DONE)
		CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [HANDLE_SHARD_OUTRO] - GBBELLYBEAST_REWARDS - Shard done.")
		EXIT
	ENDIF
	
	IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
		strOrganization = GB_GET_ORGANIZATION_NAME_AS_A_STRING()
		hcOrganization = GET_PLAYER_HUD_COLOUR(PLAYER_ID())
	ENDIF
	
	// Organisation objective text
	IF GB_IS_LOCAL_PLAYER_MEMBER_OF_THIS_GANG(GB_GET_LOCAL_PLAYER_MISSION_HOST())
		IF serverBD.iDeliveredCount != (-1)
			IF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_WORK_OVER)
				INT iCash, iRP
				CALCULATE_REWARDS(iCash, iRP)
				
				IF serverBD.iDeliveredCount = 1
					// Your Organization delivered 1 vehicle for $X
					SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "GB_WINNER","GB_BB_SS_DEL1", strOrganization, hcOrganization, DEFAULT, iCash)
				ELIF serverBD.iDeliveredCount > 1
					// Your Organization delivered X vehicles for $X
					SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "GB_WINNER", "GB_BB_SS_DEL", strOrganization, hcOrganization, DEFAULT, serverBD.iDeliveredCount, iCash)
				ELIF serverBD.iDeliveredCount = 0
					IF serverBD.iDestroyedCount != GB_GET_VEHICLE_TOTAL()
						IF GB_GET_VEHICLE_TOTAL() > 1
							// Your Organization failed to deliver any vehicles
							SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "GB_BB_SS_DELL", strOrganization, hcOrganization)
						ELSE
							// Your Organization failed to deliver the vehicle
							SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "GB_BB_SS_DELL1", strOrganization, hcOrganization)
						ENDIF
					ELIF serverBD.iDestroyedCount = GB_GET_VEHICLE_TOTAL()
						IF GB_GET_VEHICLE_TOTAL() > 1
							// The vehicles were destroyed
							SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "GB_BB_SS_DEST")
						ELSE
							// The vehicle was destroyed
							SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "GB_BB_SS_DEST1")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			SET_BIT(iLocalBitset, GBBELLYBEAST_BS_END_SHARD_DONE)
		ELSE
			CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [HANDLE_SHARD_OUTRO] - GBBELLYBEAST_REWARDS - serverBD.iDeliveredCount = -1. Not showing shard.")
		ENDIF
	ELSE
		IF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_WORK_OVER)
		OR IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_TIME_UP)
			INT iDestroyed = GET_VEHICLES_DESTROYED(TRUE)
			INT iCash, iRP
			CALCULATE_REWARDS(iCash, iRP, TRUE)
			
			IF iDestroyed = 1
				IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
					// ~a~~s~ destroyed 1 vehicle for $~1~
					SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_GENERIC, "GB_WORK_OVER", "GB_BB_SS_DES1", strOrganization, hcOrganization, DEFAULT, iCash)
				ELSE
					// You destroyed 1 vehicle for $X
					SETUP_NEW_BIG_MESSAGE_WITH_2_INTS(BIG_MESSAGE_GB_GENERIC, iCash, -1, "GB_BB_SS_SDES1", DEFAULT, DEFAULT, DEFAULT, "GB_WORK_OVER")
				ENDIF
			ELIF iDestroyed > 1
				IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
					// ~a~~s~ destroyed ~1~ vehicles for $~1~
					SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_GENERIC, "GB_WORK_OVER", "GB_BB_SS_DES", strOrganization, hcOrganization, DEFAULT, iDestroyed, iCash)
				ELSE
					// You destroyed X vehicles for $X
					SETUP_NEW_BIG_MESSAGE_WITH_2_INTS(BIG_MESSAGE_GB_GENERIC, iDestroyed, iCash, "GB_BB_SS_SDES", DEFAULT, DEFAULT, DEFAULT, "GB_WORK_OVER")
				ENDIF
			ELIF iDestroyed = 0
				IF serverBD.iDestroyedCount = 0
					IF serverBD.iDeliveredCount <> GB_GET_VEHICLE_TOTAL()
						IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
							IF GB_GET_VEHICLE_TOTAL() > 1
								// ~a~~s~ failed to destroy any vehicles
								SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_GENERIC, "GB_WORK_OVER", "GB_BB_SS_DESL", strOrganization, hcOrganization, DEFAULT)
							ELSE
								// ~a~~s~ failed to destroy the vehicle
								SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_GENERIC, "GB_WORK_OVER", "GB_BB_SS_DESL1", strOrganization, hcOrganization, DEFAULT)
							ENDIF
						ELSE
							IF GB_GET_VEHICLE_TOTAL() > 1
								// You failed to destroy any vehicles
								SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "GB_BB_SS_SDESL")
							ELSE
								// You failed to destroy the vehicle
								SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "GB_BB_SS_SDESL1")
							ENDIF
						ENDIF
					ELIF serverBD.iDeliveredCount = GB_GET_VEHICLE_TOTAL()
						strOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(GB_GET_LOCAL_PLAYER_MISSION_HOST())
						hcOrganization = GET_PLAYER_HUD_COLOUR(GB_GET_LOCAL_PLAYER_MISSION_HOST())
						
						IF GB_GET_VEHICLE_TOTAL() > 1
							// ~a~ delivered the vehicles
							SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "GB_BB_SS_DELIV", strOrganization, hcOrganization)
						ELSE
							// ~a~ delivered the vehicle
							SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "GB_BB_SS_DELIV1", strOrganization, hcOrganization)
						ENDIF
					ENDIF
				ELIF serverBD.iDestroyedCount = GB_GET_VEHICLE_TOTAL()
					IF GB_GET_VEHICLE_TOTAL() > 1
						// The vehicles were destroyed
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "GB_BB_SS_DEST")
					ELSE
						// The vehicle was destroyed
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "GB_BB_SS_DEST1")
					ENDIF
				ELSE
					IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
						IF GB_GET_VEHICLE_TOTAL() > 1
							// ~a~~s~ failed to destroy any vehicles
							SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_GENERIC, "GB_WORK_OVER", "GB_BB_SS_DESL", strOrganization, hcOrganization, DEFAULT)
						ELSE
							// ~a~~s~ failed to destroy the vehicle
							SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_GENERIC, "GB_WORK_OVER", "GB_BB_SS_DESL1", strOrganization, hcOrganization, DEFAULT)
						ENDIF
					ELSE
						IF GB_GET_VEHICLE_TOTAL() > 1
							// You failed to destroy any vehicles
							SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "GB_BB_SS_SDESL")
						ELSE
							// You failed to destroy the vehicle
							SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "GB_BB_SS_SDESL1")
						ENDIF
					ENDIF
				ENDIF		
			ENDIF	
		// The Organization Boss left the session
		ELIF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_BOSS_LEFT)
			SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "GB_BB_SS_FLEFT")
		ENDIF
		
		CLEANUP_WORK_BLIPS()
		SET_BIT(iLocalBitset, GBBELLYBEAST_BS_END_SHARD_DONE)
	ENDIF
ENDPROC

PROC MAINTAIN_INTRO_TEXTS()
	IF NOT IS_BIT_SET(iLocalBitset, GBBELLYBEAST_BS_INTRO_TEXT_SENT)
		TEXT_LABEL_15 strIntroText
		SWITCH GB_GET_VARIANT()
			CASE GB_PS_VESPUCCI			strIntroText = "GB_INTTXT_BOB0"		BREAK
			CASE GB_PS_LA_MESA			strIntroText = "GB_INTTXT_BOB1"		BREAK
			CASE GB_PS_MISSION_ROW		strIntroText = "GB_INTTXT_BOB2"		BREAK	
			CASE GB_PS_VINEWOOD_HILLS 	strIntroText = "GB_INTTXT_BOB3"		BREAK	
			CASE GB_PS_SANDY_SHORES		strIntroText = "GB_INTTXT_BOB4"		BREAK	
			CASE GB_PS_PALETO_BAY		strIntroText = "GB_INTTXT_BOB5"		BREAK	
		ENDSWITCH
		
		IF GB_GET_VEHICLE_TOTAL() = 1
			strIntroText += "1"
		ENDIF
		
		IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_MP_RAY_LAVOY, strIntroText ,TXTMSG_LOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
			CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [MAINTAIN_INTRO_TEXTS] - Text sent.")
			SET_BIT(iLocalBitset, GBBELLYBEAST_BS_INTRO_TEXT_SENT)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_HELP()
	GB_COMMON_BOSS_MISSION_HELP()

	IF GB_SHOULD_UI_BE_HIDDEN()
	OR GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) <> GB_UI_LEVEL_FULL
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBitset, GBBELLYBEAST_BS_SOLO_WARN_DONE)
		IF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_VEHICLE_ENTERED)
		OR IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_AUTOSTART)
			IF GB_IS_LOCAL_PLAYER_MEMBER_OF_THIS_GANG(GB_GET_LOCAL_PLAYER_MISSION_HOST())
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					// Other players in session have been alerted to your activities. These players can now come after your Organization to earn cash and RP.
					PRINT_HELP_NO_SOUND("GB_BB_ALERT")
					SET_BIT(iLocalBitset, GBBELLYBEAST_BS_SOLO_WARN_DONE)
				ENDIF
			ELSE
				SET_BIT(iLocalBitset, GBBELLYBEAST_BS_SOLO_WARN_DONE)
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBitset, GBBELLYBEAST_BS_LOSE_COPS_HELP_DONE)
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_CLOSE_TO_DROPOFF)
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				// You are nearing the drop-off location. Lose your Wanted Level before delivering the vehicle.
				PRINT_HELP_NO_SOUND("GB_BB_LOSE")
				SET_BIT(iLocalBitset, GBBELLYBEAST_BS_LOSE_COPS_HELP_DONE)
			ENDIF
		ENDIF
	ENDIF

	IF NOT IS_BIT_SET(iLocalBitset, GBBELLYBEAST_BS_DESTROYED_HELP_DONE)
		IF GB_GET_VEHICLE_TOTAL() = 1
			SET_BIT(iLocalBitset, GBBELLYBEAST_BS_DESTROYED_HELP_DONE)
			EXIT
		ENDIF
		
		INT iIndex
		REPEAT GB_GET_VEHICLE_TOTAL() iIndex
			IF IS_BIT_SET(serverBD.iServerBitSet, iIndex+DSTRY_BIT_OFFSET)
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					IF GB_IS_LOCAL_PLAYER_MEMBER_OF_THIS_GANG(GB_GET_LOCAL_PLAYER_MISSION_HOST())
						// A vehicle has been destroyed. Your maximum potential reward has been reduced.
						PRINT_HELP_NO_SOUND("GB_BB_DES")
					ELSE
						IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
							IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
								// A vehicle has been destroyed. Destroying a vehicle will earn you cash at the end of the Boss Work.
								PRINT_HELP_NO_SOUND("GB_BB_DESF")
							ELSE
								// A vehicle has been destroyed. Destroying a vehicle will earn your Boss cash at the end of the Boss Work.
								PRINT_HELP_NO_SOUND("GB_BB_DESR")
							ENDIF
						ELSE
							// A vehicle has been destroyed. Destroying a vehicle will earn you cash at the end of the Boss Work.
							PRINT_HELP_NO_SOUND("GB_BB_DESF")
						ENDIF
					ENDIF
					GB_SET_GANG_BOSS_HELP_BACKGROUND()
					
					SET_BIT(iLocalBitset, GBBELLYBEAST_BS_DESTROYED_HELP_DONE)
					CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [MAINTAIN_DESTROYED_HELP] - A vehicle was destroyed. Showing help.")
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC



PROC MAINTAIN_OBJECTIVE_TEXT()
	IF GB_SHOULD_UI_BE_HIDDEN()
	OR GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) <> GB_UI_LEVEL_FULL
		CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [MAINTAIN_OBJECTIVE_TEXT] - Hiding objective text.")
		IF Is_There_Any_Current_Objective_Text()
			Clear_Any_Objective_Text_From_This_Script()
			CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [MAINTAIN_OBJECTIVE_TEXT] - GB_SHOULD_UI_BE_HIDDEN - Clearing objective text.")
		ENDIF
		EXIT
	ENDIF
	
	// Organisation objective text
	IF GB_IS_LOCAL_PLAYER_MEMBER_OF_THIS_GANG(GB_GET_LOCAL_PLAYER_MISSION_HOST())
	
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_NEAR_STATION)
			Print_Objective_Text_With_String("GB_BB_GT_GOTO", GB_GET_VARIANT_STRING())
		ELSE
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_CLOSE_TO_DROPOFF)
			AND IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_IS_IN_VEH)
			AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
				Print_Objective_Text("GB_BB_COPS")
			ELIF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_ONE_VEHICLE_REMAINING)
				IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_IS_IN_VEH)
					Print_Objective_Text("GB_BB_GT_UDLVR")
				ELIF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_ALL_VEHICLES_HAVE_DRIVERS)
					Print_Objective_Text("GB_BB_GT_DLVR1")
				ELSE
					Print_Objective_Text("GB_BB_GT_STEAL1")
				ENDIF
			ELSE
				IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_IS_IN_VEH)
					Print_Objective_Text("GB_BB_GT_UDLVR")
				ELIF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_ALL_VEHICLES_HAVE_DRIVERS)
					Print_Objective_Text("GB_BB_GT_DLVR")
				ELSE
					Print_Objective_Text("GB_BB_GT_STEAL")
				ENDIF
			ENDIF
		ENDIF
	// Freemode players objective text
	ELSE
		IF IS_NET_PLAYER_OK(GB_GET_LOCAL_PLAYER_MISSION_HOST(), FALSE)
			IF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_ONE_VEHICLE_REMAINING)
				Print_Objective_Text_With_Coloured_String("GB_BB_GT_DSTRY1", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(GB_GET_LOCAL_PLAYER_MISSION_HOST()), HUD_COLOUR_PURE_WHITE)
			ELSE
				Print_Objective_Text_With_Coloured_String("GB_BB_GT_DSTRY", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(GB_GET_LOCAL_PLAYER_MISSION_HOST()), HUD_COLOUR_PURE_WHITE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Sends ticker to players telling them who stole the vehicle
PROC SEND_STOLEN_TICKER(PLAYER_INDEX piPlayer)
	SCRIPT_EVENT_DATA_TICKER_MESSAGE tickerData
	tickerData.playerID = piPlayer
	tickerData.playerID2 = GB_GET_LOCAL_PLAYER_MISSION_HOST()
	tickerData.TickerEvent = TICKER_EVENT_GB_BB_STOLEN_VEH
	BROADCAST_TICKER_EVENT(tickerData, ALL_PLAYERS_ON_SCRIPT(TRUE))
ENDPROC

/// PURPOSE: Sends ticker to players telling them who destroyed the vehicle
PROC SEND_DESTROYED_TICKER(PLAYER_INDEX piPlayer)
	SCRIPT_EVENT_DATA_TICKER_MESSAGE tickerData
	tickerData.playerID = piPlayer
	tickerData.TickerEvent = TICKER_EVENT_GB_BB_DESTROYED_VEH
	BROADCAST_TICKER_EVENT(tickerData, ALL_PLAYERS_ON_SCRIPT(TRUE))
ENDPROC

PROC SET_VEHICLES_AS_VINCIBLE()
	IF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_VEHICLE_ENTERED)
	OR IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_AUTOSTART)
		INT iIndex
		REPEAT GB_GET_VEHICLE_TOTAL() iIndex
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, iIndex)
			AND NOT IS_BIT_SET(serverBD.iServerBitSet, iIndex+DSTRY_BIT_OFFSET)
				IF NETWORK_DOES_NETWORK_ID_EXIST(GB_GET_VEHICLE(iIndex))
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GB_GET_VEHICLE(iIndex))
						SET_ENTITY_INVINCIBLE(NET_TO_VEH(GB_GET_VEHICLE(iIndex)), FALSE)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

PROC HANDLE_LOCAL_IN_VEHICLE()

	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		CLEAR_BIT(iLocalBitset, GBBELLYBEAST_BS_ENTERED_VEHICLE)
		CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_IS_VEH_DRIVER)
		CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_IS_IN_VEH)
	ENDIF
	
	SCRIPT_EVENT_DATA_TICKER_MESSAGE tickerData
	INT iIndex
	
	REPEAT GB_GET_VEHICLE_TOTAL() iIndex
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GB_GET_VEHICLE(iIndex))
		
			// Check the vehicle has not been delivered, both server and client data to prevent timing issues
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, iIndex)
			AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, iIndex)
			
				IF GET_SERVER_MISSION_STAGE() < GBBELLYBEAST_REWARDS
				
					// If the player is in one of the vehicles (driver or passenger)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(GB_GET_VEHICLE(iIndex)))
					
						// Set as a qualifiying participant for entering a work vehicle if they aren't already qualified
						GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
						
						// Set data to show player is in the vehicle, and also if they're the driver
						// Mostly used for objective text and delivering the vehicle
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_IS_IN_VEH)
						IF GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(GB_GET_VEHICLE(iIndex))) = PLAYER_PED_ID()
							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_IS_VEH_DRIVER)
						ENDIF
						
						// Check if the player entered the vehicle while it was at/near it's spawn
						// Used for giving a wanted level to players again if they're delivering a second vehicle
						IF NOT IS_BIT_SET(iLocalBitset, GBBELLYBEAST_BS_ENTERED_VEHICLE)
							IF IS_ENTITY_AT_COORD(NET_TO_VEH(GB_GET_VEHICLE(iIndex)), GB_GET_VEHICLE_LOCATION(iIndex), <<GB_GET_VEHICLE_PROXIMITY_FOR_REAPPLYING_WANTED_LEVEL(), GB_GET_VEHICLE_PROXIMITY_FOR_REAPPLYING_WANTED_LEVEL(), GB_GET_VEHICLE_PROXIMITY_FOR_REAPPLYING_WANTED_LEVEL()>>)
								CLEAR_BIT(iLocalBitset, GBBELLYBEAST_BS_INIT_WANTED)
								SET_BIT(iLocalBitset, GBBELLYBEAST_BS_ENTERED_VEHICLE)
								CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [HANDLE_LOCAL_IN_VEHICLE] - Local player entered vehicle ", iIndex, " at it's spawn for first time.")
							ENDIF
						ENDIF
						
						// Loop through the available drop off points
						INT iLocation
						REPEAT GB_GET_VEHICLE_TOTAL() iLocation
						
							// Check that the drop off point is still available
							IF NOT IS_BIT_SET(serverBd.iServerBitSet, (iLocation+4))
							
								// Check that the player is close enough to the drop off to start checking IS_ENTITY_AT_COORD
								IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_CLOSE_TO_DROPOFF)
								
									// If they've lost the cops and are definitely still the driver 
									IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
									AND GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(GB_GET_VEHICLE(iIndex))) = PLAYER_PED_ID()
									
										// If the vehicle is in a locate, then halt the vehicle, set it as delivered and clear/update appropriate data
										// Again mostly for correct objective text, as well as counting cars delivered
										IF IS_ENTITY_AT_COORD(NET_TO_VEH(GB_GET_VEHICLE(iIndex)), GB_GET_DROPOFF_LOCATION(iLocation), <<2.0, 1.0, LOCATE_SIZE_HEIGHT>>, TRUE) 
											BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(NET_TO_VEH(GB_GET_VEHICLE(iIndex)), 4.0, 1, 0.5, FALSE, FALSE)
											
											// Set the vehicle as delivered
											SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, iIndex)
											// Set the drop off point as used
											SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, (iLocation+4))
											
											// Set to show the player has delivered any vehicle, for giving them a wanted level again if they deliver more than 1 vehicle
											SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_DELIVERED_ANY_ALREADY)
											CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_IS_VEH_DRIVER)
											
											CLEAR_BIT(iLocalBitset, GBBELLYBEAST_BS_ENTERED_VEHICLE)
											CLEAR_BIT(iLocalBitset, GBBELLYBEAST_BS_INIT_WANTED)
																					
											// Broadcast ticker to other members of the work saying we delivered it
											tickerData.playerID = PLAYER_ID()
											tickerData.playerID2 = GB_GET_LOCAL_PLAYER_MISSION_HOST()
											tickerData.TickerEvent = TICKER_EVENT_GB_BB_DELIVERED_VEH
											BROADCAST_TICKER_EVENT(tickerData, ALL_PLAYERS_ON_SCRIPT(TRUE))
											
											CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [HANDLE_LOCAL_IN_VEHICLE] - Local player delivered vehicle ", iIndex, " to drop off point ", iLocation, ".")
											
											// Exit repeat if local has delivered a vehicle to a drop off.
											iLocation = GB_GET_VEHICLE_TOTAL()
										ENDIF
									ELSE
										CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [HANDLE_LOCAL_IN_VEHICLE] - Local player cannot deliver. Wanted level still greater than 0.")
									ENDIF
								ELSE
									// Check if the player is close to the drop off point so their wanted level can become natural rather than forced
									IF IS_ENTITY_AT_COORD(NET_TO_VEH(GB_GET_VEHICLE(iIndex)), GB_GET_DROPOFF_LOCATION(iLocation), <<GB_GET_WANTED_LEVEL_DROP_DISTANCE(), GB_GET_WANTED_LEVEL_DROP_DISTANCE(), GB_GET_WANTED_LEVEL_DROP_DISTANCE()>>)	
										SET_AUDIO_FLAG("WantedMusicDisabled", TRUE)
										IF GB_IS_IN_CITY()
											SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), GB_GET_CITY_WANTED_LEVEL_DROP())
										ELSE
											SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), GB_GET_COUNTRY_WANTED_LEVEL_DROP())
										ENDIF
										SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())					
										
										CLEAR_BIT(iLocalBitset, GBBELLYBEAST_BS_ENTERED_VEHICLE)
										SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_CLOSE_TO_DROPOFF)
										SET_AUDIO_FLAG("WantedMusicDisabled", FALSE)
										CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [HANDLE_LOCAL_IN_VEHICLE] - Local player close to drop off. Wanted level can now be lost.")
									ENDIF
								ENDIF
							ELSE
								CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [HANDLE_LOCAL_IN_VEHICLE] - Drop off point ", iLocation, " has already been used.")
							ENDIF
						ENDREPEAT
					ENDIF
				ENDIF
			ELSE
				EMPTY_AND_LOCK_VEHICLE(NET_TO_VEH(GB_GET_VEHICLE(iIndex)), timeLeaveEvent[iIndex])
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC MAINTAIN_CLOSEST_VEHICLE()
	VECTOR vPrevCoords
	INT iIndex
	
	IF HAS_NET_TIMER_EXPIRED(stClosestActionTimer, 5000)
		REPEAT GB_GET_VEHICLE_TOTAL() iIndex
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GB_GET_VEHICLE(iIndex))
			AND NOT IS_BIT_SET(serverBD.iServerBitSet, iIndex)
				IF DOES_ENTITY_EXIST(NET_TO_VEH(GB_GET_VEHICLE(iIndex)))
				AND DOES_ENTITY_EXIST(PLAYER_PED_ID())
					IF IS_NET_PLAYER_OK(PLAYER_ID())
						vLastPlayerLocation = GET_ENTITY_COORDS(PLAYER_PED_ID())
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(NET_TO_VEH(GB_GET_VEHICLE(iIndex)))
						IF IS_VECTOR_ZERO(vPrevCoords)
							vPrevCoords = GET_ENTITY_COORDS(NET_TO_VEH(GB_GET_VEHICLE(iIndex)))
							vClosestAction = vPrevCoords
						ELSE
							IF VDIST(vLastPlayerLocation, vPrevCoords) > VDIST(vLastPlayerLocation, GET_ENTITY_COORDS(NET_TO_VEH(GB_GET_VEHICLE(iIndex))))
								vPrevCoords = GET_ENTITY_COORDS(NET_TO_VEH(GB_GET_VEHICLE(iIndex)))
								CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [MAINTAIN_CLOSEST_VEHICLE] - Vehicle ", iIndex, " is closest. Location = <<", vPrevCoords.x, ", ", vPrevCoords.y, ", ", vPrevCoords.z, ">>")
							ELSE
								CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [MAINTAIN_CLOSEST_VEHICLE] - Vehicle ", iIndex, " is not closer than previous vehicle(s). Location =  <<", vPrevCoords.x, ", ", vPrevCoords.y, ", ", vPrevCoords.z, ">>")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		vClosestAction = vPrevCoords
		CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [MAINTAIN_CLOSEST_VEHICLE] - Closest vehicle is at <<", vClosestAction.x, ", ", vClosestAction.y, ", ", vClosestAction.z, ">>")
		RESET_NET_TIMER(stClosestActionTimer)
	ENDIF
ENDPROC

PROC DRAW_VEHICLE_MARKER()
	IF GB_SHOULD_UI_BE_HIDDEN()
	OR GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) <> GB_UI_LEVEL_FULL
		EXIT
	ENDIF
	
	INT colR, colG, colB, colA

	INT iIndex
	REPEAT GB_GET_VEHICLE_TOTAL() iIndex
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, iIndex)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GB_GET_VEHICLE(iIndex))
				IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
				AND DOES_ENTITY_EXIST(NET_TO_VEH(GB_GET_VEHICLE(iIndex)))
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					AND NOT IS_ENTITY_DEAD(NET_TO_VEH(GB_GET_VEHICLE(iIndex)))
						IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(GB_GET_VEHICLE(iIndex)))
							IF GB_IS_LOCAL_PLAYER_MEMBER_OF_THIS_GANG(GB_GET_LOCAL_PLAYER_MISSION_HOST())
								GET_HUD_COLOUR(HUD_COLOUR_BLUE, colR, colG, colB, colA)
							ELSE
								GET_HUD_COLOUR(HUD_COLOUR_RED, colR, colG, colB, colA)
							ENDIF
							FM_EVENTDRAW_MARKER_ABOVE_VEHICLE(NET_TO_VEH(GB_GET_VEHICLE(iIndex)), colR, colG, colB)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC MAINTAIN_BLIPS()
	INT iLastValidBlip
	
	// Draw blip for police station at start of work
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_NEAR_STATION)
	AND GB_IS_LOCAL_PLAYER_MEMBER_OF_THIS_GANG(GB_GET_LOCAL_PLAYER_MISSION_HOST())
		IF NOT DOES_BLIP_EXIST(biStation)
			// Use vehicle 0 location, as there has to be at least one vehicle.
			biStation = ADD_BLIP_FOR_COORD(GB_GET_VEHICLE_LOCATION(0))
			SET_BLIP_COLOUR_FROM_HUD_COLOUR(biStation, HUD_COLOUR_YELLOW)
			
			SET_BLIP_ROUTE(biStation, TRUE)
			SET_BLIP_ROUTE_COLOUR(biStation, GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_YELLOW))
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(biStation)
			REMOVE_BLIP(biStation)
		ENDIF

		// Draw blips for each of the vehicles
		INT iIndex
		REPEAT GB_GET_VEHICLE_TOTAL() iIndex
		
			// Only blip the vehicle if it is alive and empty
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GB_GET_VEHICLE(iIndex))
			AND NOT IS_ENTITY_DEAD(NET_TO_VEH(GB_GET_VEHICLE(iIndex)))
			AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(GB_GET_VEHICLE(iIndex)))
			AND NOT IS_BIT_SET(serverBD.iServerBitSet, iIndex)
			AND NOT GB_SHOULD_UI_BE_HIDDEN()
				IF NOT DOES_BLIP_EXIST(biVehicles[iIndex])
					biVehicles[iIndex] = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(GB_GET_VEHICLE(iIndex)))
					
					SET_BLIP_SPRITE(biVehicles[iIndex], RADAR_TRACE_BELLY_OF_THE_BEAST)
					
					IF GB_IS_LOCAL_PLAYER_MEMBER_OF_THIS_GANG(GB_GET_LOCAL_PLAYER_MISSION_HOST())
						SET_BLIP_COLOUR_FROM_HUD_COLOUR(biVehicles[iIndex], HUD_COLOUR_BLUE)
					ELSE
						SET_BLIP_COLOUR_FROM_HUD_COLOUR(biVehicles[iIndex], HUD_COLOUR_RED)
					ENDIF
					
					SET_BLIP_MARKER_LONG_DISTANCE(biVehicles[iIndex], TRUE)
					SET_BLIP_PRIORITY(biVehicles[iIndex], BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
					SET_BLIP_NAME_FROM_TEXT_FILE(biVehicles[iIndex], "GB_BB_B_VEH")
					SET_BLIP_SCALE(biVehicles[iIndex], g_sMPTunables.fgangboss_Job_blip_scale)
					
					// Flash the vehicle blip the first time we see it
					IF NOT IS_BIT_SET(iLocalBitset, GBBELLYBEAST_BS_VEHICLE_BLIPS_FLASHED)
						SET_BLIP_FLASHES(biVehicles[iIndex], TRUE)
						SET_BLIP_FLASH_TIMER(biVehicles[iIndex], 7000)
						
						IF iIndex = GB_GET_VEHICLE_TOTAL()-1
							SET_BIT(iLocalBitset, GBBELLYBEAST_BS_VEHICLE_BLIPS_FLASHED)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				// If the vehicle is dead or occupied, remove the blip
				IF DOES_BLIP_EXIST(biVehicles[iIndex])
					REMOVE_BLIP(biVehicles[iIndex])
				ENDIF
			ENDIF
			
			// Drop off blip
			IF GB_IS_LOCAL_PLAYER_MEMBER_OF_THIS_GANG(GB_GET_LOCAL_PLAYER_MISSION_HOST())
				IF (IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_IS_IN_VEH)
				OR IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_ALL_VEHICLES_HAVE_DRIVERS))
				AND NOT IS_BIT_SET(serverBD.iServerBitSet, (iIndex+4))
				AND NOT GB_SHOULD_UI_BE_HIDDEN()
					IF NOT DOES_BLIP_EXIST(biDropOff[iIndex])
						biDropOff[iIndex] = ADD_BLIP_FOR_COORD(GB_GET_DROPOFF_LOCATION(iIndex))
						SET_BLIP_COLOUR_FROM_HUD_COLOUR(biDropOff[iIndex], HUD_COLOUR_YELLOW)
					ENDIF
					iLastValidBlip = iIndex
				ELSE
					IF DOES_BLIP_EXIST(biDropOff[iIndex])
						REMOVE_BLIP(biDropOff[iIndex])
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF DOES_BLIP_EXIST(biDropOff[iLastValidBlip])
		AND NOT DOES_BLIP_HAVE_GPS_ROUTE(biDropOff[iLastValidBlip])
			SET_BLIP_ROUTE(biDropOff[iLastValidBlip], TRUE)
			SET_BLIP_ROUTE_COLOUR(biDropOff[iLastValidBlip], GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_YELLOW))
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_WANTED_LEVEL()
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_VEHICLE_ENTERED)
		EXIT
	ENDIF

	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_CLOSE_TO_DROPOFF)
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			IF GET_MAX_WANTED_LEVEL() != GB_GET_CITY_WANTED_LEVEL()
				SET_MAX_WANTED_LEVEL(GB_GET_CITY_WANTED_LEVEL())
			ENDIF

			IF NOT IS_BIT_SET(iLocalBitset, GBBELLYBEAST_BS_INIT_WANTED)
				IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
					
					IF GB_IS_IN_CITY()
						SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), GB_GET_CITY_WANTED_LEVEL())
					ELSE
						SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), GB_GET_COUNTRY_WANTED_LEVEL())
					ENDIF
					
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					SET_BIT(iLocalBitset, GBBELLYBEAST_BS_INIT_WANTED)
					CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [MAINTAIN_WANTED_LEVEL] - Local bit GBBELLYBEAST_BS_INIT_WANTED set.")
				ENDIF
			ELSE
				UPDATE_WANTED_POSITION_THIS_FRAME(PLAYER_ID())
				SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID())
			ENDIF
		ELSE
			CLEAR_BIT(iLocalBitset, GBBELLYBEAST_BS_INIT_WANTED)
			CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [MAINTAIN_WANTED_LEVEL] - Local bit GBBELLYBEAST_BS_INIT_WANTED cleared.")
		ENDIF
	ELIF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOST_FORCED_WANTED_LEVEL)
		INT iTempWanted
		IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
			SET_BIT(iLocalBitset, GBBELLYBEAST_BS_PLAYER_DIED)
			CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [MAINTAIN_WANTED_LEVEL] - Local bit GBBELLYBEAST_BS_PLAYER_DIED set.")
		ELSE
			IF IS_BIT_SET(iLocalBitset, GBBELLYBEAST_BS_PLAYER_DIED)
				IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
					IF iCurrentWantedLevel > 0
						SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), iCurrentWantedLevel)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
						CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [MAINTAIN_WANTED_LEVEL] - Local player wanted level set to ", iCurrentWantedLevel, " stars.")
					ENDIF
				
					CLEAR_BIT(iLocalBitset, GBBELLYBEAST_BS_PLAYER_DIED)
					CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [MAINTAIN_WANTED_LEVEL] - Local bit GBBELLYBEAST_BS_PLAYER_DIED cleared.")
				ENDIF
			ELSE
				iTempWanted = GET_PLAYER_WANTED_LEVEL(PLAYER_ID())
				IF iTempWanted = 0
					CLEAR_BIT(iLocalBitset, GBBELLYBEAST_BS_INIT_WANTED)
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_LOST_FORCED_WANTED_LEVEL)
				ELIF iTempWanted <> iCurrentWantedLevel
					iCurrentWantedLevel = iTempWanted
					CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [MAINTAIN_WANTED_LEVEL] - iCurrentWantedLevel updated to ", iCurrentWantedLevel)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBitset, GBBELLYBEAST_BS_ENTERED_VEHICLE)
		AND NOT IS_BIT_SET(iLocalBitset, GBBELLYBEAST_BS_INIT_WANTED)
			IF GB_IS_IN_CITY()
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), GB_GET_CITY_WANTED_LEVEL())
			ELSE
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), GB_GET_COUNTRY_WANTED_LEVEL())
			ENDIF
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			SET_BIT(iLocalBitset, GBBELLYBEAST_BS_INIT_WANTED)
		ENDIF
	ENDIF
ENDPROC


PROC MAINTAIN_DAMAGE_EVENT(INT iEventID)
	STRUCT_ENTITY_DAMAGE_EVENT seDamageEvent
	
	// Check for damage event
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, seDamageEvent, SIZE_OF(seDamageEvent))
	
		IF DOES_ENTITY_EXIST(seDamageEvent.VictimIndex)
			IF IS_ENTITY_A_VEHICLE(seDamageEvent.VictimIndex) 
			
				INT iVehicle
				REPEAT GB_GET_VEHICLE_TOTAL() iVehicle
					// Check the vehicle isn't already destroyed or delivered
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, iVehicle)
					AND NOT IS_BIT_SET(serverBD.iServerBitSet, (iVehicle + DSTRY_BIT_OFFSET))
					
						// Check victim is one of the vehicles.
						IF GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(seDamageEvent.VictimIndex) = NET_TO_VEH(GB_GET_VEHICLE(iVehicle))
						
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GB_GET_VEHICLE(iVehicle))
							AND IS_ENTITY_DEAD(NET_TO_VEH(GB_GET_VEHICLE(iVehicle)))
								IF IS_ENTITY_IN_WATER(NET_TO_VEH(GB_GET_VEHICLE(iVehicle)))
								AND NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(GB_GET_VEHICLE(iVehicle)))
								AND IS_VEHICLE_SEAT_FREE(NET_TO_VEH(GB_GET_VEHICLE(iVehicle)))
									CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [MAINTAIN_DAMAGE_EVENT] - Vehicle ", iVehicle, " is in water and undriveable with no driver. Exiting damage event, this is handled by the mission host.")
									EXIT
								ENDIF
							ENDIF
							
							// Check a damager exists, and is a ped
							IF DOES_ENTITY_EXIST(seDamageEvent.DamagerIndex)
							AND IS_ENTITY_A_PED(seDamageEvent.DamagerIndex) 
								IF GET_PED_INDEX_FROM_ENTITY_INDEX(seDamageEvent.DamagerIndex) = PLAYER_PED_ID()
									IF GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
										IF seDamageEvent.VictimDestroyed
											SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, (iVehicle + DSTRY_BIT_OFFSET))
											SEND_DESTROYED_TICKER(INVALID_PLAYER_INDEX())
											CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [MAINTAIN_DAMAGE_EVENT] - Local player destroyed a vehicle while hiding event.")
										ENDIF
									ELSE
										IF NOT GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PLAYER_ID(), GB_GET_LOCAL_PLAYER_MISSION_HOST())
											GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
											
											IF NOT GB_IS_GLOBAL_CLIENT_BIT0_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_0_PERMANENT_PARTICIPANT)
												GB_SET_PLAYER_AS_PERMANENT_ON_BOSS_MISSION()
											ENDIF
											
											IF seDamageEvent.VictimDestroyed
												playerBD[PARTICIPANT_ID_TO_INT()].iDestroyedCount++
												SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, (iVehicle + DSTRY_BIT_OFFSET))
												CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [MAINTAIN_DAMAGE_EVENT] - Local player destroyed a vehicle. Destroyed: ", playerBD[PARTICIPANT_ID_TO_INT()].iDestroyedCount)
												SEND_DESTROYED_TICKER(PLAYER_ID())
											ENDIF
										ELSE
											IF seDamageEvent.VictimDestroyed
												SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, (iVehicle + DSTRY_BIT_OFFSET))
												SEND_DESTROYED_TICKER(INVALID_PLAYER_INDEX())
											ENDIF
										ENDIF
									ENDIF
								ELIF NOT IS_PED_A_PLAYER(GET_PED_INDEX_FROM_ENTITY_INDEX(seDamageEvent.DamagerIndex))
									IF seDamageEvent.VictimDestroyed
										SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, (iVehicle + DSTRY_BIT_OFFSET))
										SEND_DESTROYED_TICKER(INVALID_PLAYER_INDEX())
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
			// Has the local player hurt a key organization memeber
			ELIF NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
			AND IS_ENTITY_A_PED(seDamageEvent.VictimIndex)
			AND IS_PED_A_PLAYER(GET_PED_INDEX_FROM_ENTITY_INDEX(seDamageEvent.VictimIndex))
				
				IF NOT GB_IS_PLAYER_A_QUALIFYING_PARTICIPANT(PLAYER_ID())
				AND NOT GB_IS_GLOBAL_CLIENT_BIT0_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_0_PERMANENT_PARTICIPANT)
					PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_INDEX_FROM_ENTITY_INDEX(seDamageEvent.VictimIndex))
					
					IF IS_NET_PLAYER_OK(piPlayer, FALSE)
						IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(piPlayer, GB_GET_LOCAL_PLAYER_MISSION_HOST())
						
							IF DOES_ENTITY_EXIST(seDamageEvent.DamagerIndex)
							AND IS_ENTITY_A_PED(seDamageEvent.DamagerIndex) 
							
								IF GET_PED_INDEX_FROM_ENTITY_INDEX(seDamageEvent.DamagerIndex) = PLAYER_PED_ID()
									GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
									IF NOT GB_IS_GLOBAL_CLIENT_BIT0_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_0_PERMANENT_PARTICIPANT)
										GB_SET_PLAYER_AS_PERMANENT_ON_BOSS_MISSION()
									ENDIF
								ENDIF
							ENDIF
						ENDIF	
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

SCRIPT_TIMER stAllDeadCheck[MAX_VEHICLES]
/// PURPOSE: Listen for a damage event, and use the data to check if the vehicle has been destroyed.
PROC MAINTAIN_DAMAGE_EVENT_LISTEN()
	INT iEventID
	EVENT_NAMES ThisScriptEvent

	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iEventID
		ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iEventID)
					
		SWITCH ThisScriptEvent			
			CASE EVENT_NETWORK_DAMAGE_ENTITY
				MAINTAIN_DAMAGE_EVENT(iEventID)
			BREAK					
		ENDSWITCH
	ENDREPEAT
	
	INT iVehicle
	REPEAT GB_GET_VEHICLE_TOTAL() iVehicle
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, iVehicle)
		AND NOT IS_BIT_SET(serverBD.iServerBitSet, (iVehicle + DSTRY_BIT_OFFSET))
			IF PLAYER_ID() = GB_GET_LOCAL_PLAYER_MISSION_HOST()
				IF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_WORK_STARTED)
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GB_GET_VEHICLE(iVehicle))
						IF IS_ENTITY_DEAD(NET_TO_VEH(GB_GET_VEHICLE(iVehicle)))
							IF IS_ENTITY_IN_WATER(NET_TO_VEH(GB_GET_VEHICLE(iVehicle)))
							AND NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(GB_GET_VEHICLE(iVehicle)))
							AND IS_VEHICLE_SEAT_FREE(NET_TO_VEH(GB_GET_VEHICLE(iVehicle)))
								CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [MAINTAIN_DAMAGE_EVENT_LISTEN] - Vehicle ", iVehicle, " is in water and undriveable with no driver.")
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, (iVehicle + DSTRY_BIT_OFFSET))
							ELSE
								IF HAS_NET_TIMER_EXPIRED(stAllDeadCheck[iVehicle], 60000)
									SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, (iVehicle + DSTRY_BIT_OFFSET))
									CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [MAINTAIN_DAMAGE_EVENT_LISTEN] - 2 minutes expired. Vehicle ", iVehicle, " is dead, so marking as dead server side.")
								ENDIF
							ENDIF
						ELSE
							IF NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(GB_GET_VEHICLE(iVehicle)))
								IF HAS_NET_TIMER_EXPIRED(stAllDeadCheck[iVehicle], 60000)
									SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, (iVehicle + DSTRY_BIT_OFFSET))
									CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [MAINTAIN_DAMAGE_EVENT_LISTEN] - 2 minutes expired. Vehicle ", iVehicle, " not driveable, so marking as dead server side.")
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF HAS_NET_TIMER_EXPIRED(stAllDeadCheck[iVehicle], 60000)
							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, (iVehicle + DSTRY_BIT_OFFSET))
							CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [MAINTAIN_DAMAGE_EVENT_LISTEN] - 2 minutes expired. Vehicle ", iVehicle, " does not exist, so marking as dead server side.")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC MAINTAIN_NEAR_STATION_CHECK()
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_NEAR_STATION)
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GB_GET_VEHICLE_LOCATION(0), <<100.0, 100.0, 100.0>>)		
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_NEAR_STATION)
				CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [MAINTAIN_NEAR_STATION_CHECK] - Local player close to police station. Updating objective. CLIENT_BS_NEAR_STATION = SET.")
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_IS_IN_VEH)
		OR IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_ALL_VEHICLES_HAVE_DRIVERS)
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_NEAR_STATION)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_AUTOSTART_TIMEUP()
	IF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_WORK_STARTED)
	AND NOT IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_VEHICLE_ENTERED)
		IF HAS_NET_TIMER_EXPIRED(serverBD.stAutostart, 600000)
			SET_BIT(serverBD.iServerBitSet, SERVER_BS_AUTOSTART)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: SERVER ONLY: Used to check client states.
PROC MAINTAIN_ACTIVE_PLAYER_CHECKS()
	INT iCount
	
	INT iIndex
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iIndex
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iIndex))
			IF GET_SERVER_MISSION_STAGE() < GBBELLYBEAST_REWARDS
				
				INT iVeh
				REPEAT GB_GET_VEHICLE_TOTAL() iVeh
					IF IS_BIT_SET(playerBD[iIndex].iClientBitSet, iVeh)
					AND NOT IS_BIT_SET(serverBD.iServerBitSet, iVeh)
						SET_BIT(serverBD.iServerBitSet, iVeh)
						serverBD.iDeliveredCount++
						CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [MAINTAIN_ACTIVE_PLAYER_CHECKS] - ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iIndex)), " has delivered vehicle ", iVeh ,". Current deliver count: ", serverBD.iDeliveredCount)
					ENDIF
					
					// Check if drop off has been used. If so, don't blip
					IF IS_BIT_SET(playerBD[iIndex].iClientBitSet, (iVeh+4))
					AND NOT IS_BIT_SET(serverBD.iServerBitSet, (iVeh+4))
						SET_BIT(serverBD.iServerBitSet, (iVeh+4))
						CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [MAINTAIN_ACTIVE_PLAYER_CHECKS] - ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iIndex)), " used drop-off ", iVeh+4, ". Locking drop off.")
					ENDIF
					
					IF IS_BIT_SET(playerBD[iIndex].iClientBitSet, (iVeh + DSTRY_BIT_OFFSET))
					AND NOT IS_BIT_SET(serverBD.iServerBitSet, (iVeh + DSTRY_BIT_OFFSET))
						SET_BIT(serverBD.iServerBitSet, (iVeh + DSTRY_BIT_OFFSET))
						serverBD.iDestroyedCount++
						CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [MAINTAIN_ACTIVE_PLAYER_CHECKS] - ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iIndex)), " has destroyed a vehicle. Current destroy count: ", serverBD.iDestroyedCount)
					ENDIF
				ENDREPEAT
				
				IF IS_BIT_SET(playerBD[iIndex].iClientBitSet, CLIENT_BS_IS_VEH_DRIVER)
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_VEHICLE_ENTERED)
						SEND_STOLEN_TICKER(INT_TO_PLAYERINDEX(iIndex))
						SET_BIT(serverBD.iServerBitSet, SERVER_BS_VEHICLE_ENTERED)
					ENDIF
					iCount++
				ENDIF
				
			ELIF GET_SERVER_MISSION_STAGE() = GBBELLYBEAST_REWARDS
				IF IS_BIT_SET(playerBD[iIndex].iClientBitSet, CLIENT_BS_OUTRO_DONE)
					iCount++
				ENDIF
			ENDIF
		
			IF IS_BIT_SET(playerBD[iIndex].iClientBitSet, CLIENT_BS_START_TIMER)
				SET_BIT(serverBD.iServerBitSet, SERVER_BS_WORK_STARTED)
			ENDIF
			
			#IF IS_DEBUG_BUILD
			IF IS_BIT_SET(playerBD[iIndex].iClientBitSet, CLIENT_BS_TIME_UP)
				bFakeTimeUp = TRUE
			ENDIF
			#ENDIF
		ENDIF
	ENDREPEAT
	
	// Check all players are in the same state.
	IF GET_SERVER_MISSION_STAGE() < GBBELLYBEAST_REWARDS
	
		// Check if all remaining vehicles have drivers
		IF iCount = (GB_GET_VEHICLE_TOTAL() - (serverBD.iDeliveredCount+serverBD.iDestroyedCount))
			SET_BIT(serverBD.iServerBitSet, SERVER_BS_ALL_VEHICLES_HAVE_DRIVERS)
		ELIF iCount = 1
		AND IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_ONE_VEHICLE_REMAINING)
			SET_BIT(serverBD.iServerBitSet, SERVER_BS_ALL_VEHICLES_HAVE_DRIVERS)
		ELIF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_ALL_VEHICLES_HAVE_DRIVERS)
			CLEAR_BIT(serverBD.iServerBitSet, SERVER_BS_ALL_VEHICLES_HAVE_DRIVERS)
		ENDIF
		
		IF (serverBD.iDeliveredCount+serverBD.iDestroyedCount) = GB_GET_VEHICLE_TOTAL()
			CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [MAINTAIN_ACTIVE_PLAYER_CHECKS] - Work over. No vehicles remaining. Destroyed: ", serverBD.iDestroyedCount, " - Delivered: ", serverBD.iDeliveredCount, " - Total Vehicles: ", GB_GET_VEHICLE_TOTAL())
			SET_BIT(serverBD.iServerBitSet, SERVER_BS_WORK_OVER)
		ENDIF
		
		IF (GB_GET_VEHICLE_TOTAL() - (serverBD.iDeliveredCount+serverBD.iDestroyedCount)) = 1
			SET_BIT(serverBD.iServerBitSet, SERVER_BS_ONE_VEHICLE_REMAINING)
		ENDIF

		// Check if all vehicles are delivered or if one is remaining
		IF serverBD.iDeliveredCount = GB_GET_VEHICLE_TOTAL()
			SET_BIT(serverBD.iServerBitSet, SERVER_BS_DELIVERED)
			CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [MAINTAIN_ACTIVE_PLAYER_CHECKS] - All ", GB_GET_VEHICLE_TOTAL(), " vehicles delivered.")
		ENDIF
		
		// Check if all vehicles are destroyed or if one is remaining
		IF serverBD.iDestroyedCount = GB_GET_VEHICLE_TOTAL()
			SET_BIT(serverBD.iServerBitSet, SERVER_BS_ALL_DESTROYED)
			CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [MAINTAIN_ACTIVE_PLAYER_CHECKS] - All ", GB_GET_VEHICLE_TOTAL(), " vehicles destroyed.")
		ENDIF	
		
		IF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_WORK_STARTED)
			IF HAS_NET_TIMER_EXPIRED(serverBD.stWorkTimer, GB_GET_WORK_DURATION())
			#IF IS_DEBUG_BUILD
			OR bFakeTimeUp
			#ENDIF
				SET_BIT(serverBD.iServerBitSet, SERVER_BS_TIME_UP)
			ENDIF
		ENDIF
		
	ELIF GET_SERVER_MISSION_STAGE() = GBBELLYBEAST_REWARDS
		IF iCount = NETWORK_GET_NUM_PARTICIPANTS()
			SET_BIT(serverBD.iServerBitSet, SERVER_BS_REWARDS_GIVEN)
			CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [MAINTAIN_ACTIVE_PLAYER_CHECKS] - SERVER_BS_REWARDS_GIVEN = SET. iCount = NETWORK_GET_NUM_PARTICIPANTS()")
		ELSE
			IF HAS_NET_TIMER_EXPIRED(serverBD.stRewardTimeout, REWARD_TIMEOUT)
				SET_BIT(serverBD.iServerBitSet, SERVER_BS_OUTRO_DONE)
				CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [MAINTAIN_ACTIVE_PLAYER_CHECKS] - SERVER_BS_OUTRO_DONE = SET. stRewardTimeout expired after waiting ", (REWARD_TIMEOUT/1000), " seconds.")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: SERVER ONLY: Check it's okay to start the hit.
FUNC BOOL SHOULD_JOB_START()	
	CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [SHOULD_JOB_START] - PASS")
	RETURN TRUE
ENDFUNC

/// PURPOSE: Check if the job should end.
PROC MAINTAIN_END_CONDITION_CHECKS()
	
	IF IS_BIT_SET(serverBd.iServerBitSet, SERVER_BS_NOT_ENOUGH_PLAYERS)
		SET_SERVER_MISSION_END_REASON(GBBELLYBEAST_ENDREASON_NOT_ENOUGH_PLAYERS)
		SET_SERVER_MISSION_STAGE(GBBELLYBEAST_REWARDS)
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_WORK_OVER)
		SET_SERVER_MISSION_END_REASON(GBBELLYBEAST_ENDREASON_DELIVERED)
		SET_SERVER_MISSION_STAGE(GBBELLYBEAST_REWARDS)
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_ALL_DESTROYED)
		SET_SERVER_MISSION_END_REASON(GBBELLYBEAST_ENDREASON_DESTROYED)
		SET_SERVER_MISSION_STAGE(GBBELLYBEAST_REWARDS)
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_TIME_UP)
		SET_BIT(serverBD.iServerBitSet, SERVER_BS_WORK_OVER)
		SET_SERVER_MISSION_END_REASON(GBBELLYBEAST_ENDREASON_TIMEUP)
		SET_SERVER_MISSION_STAGE(GBBELLYBEAST_REWARDS)
	ENDIF

	IF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_DELIVERED)
		SET_SERVER_MISSION_END_REASON(GBBELLYBEAST_ENDREASON_DELIVERED)
		SET_SERVER_MISSION_STAGE(GBBELLYBEAST_REWARDS)
	ENDIF
	
	IF NOT IS_NET_PLAYER_OK(GB_GET_LOCAL_PLAYER_MISSION_HOST(), FALSE)
		SET_BIT(serverBD.iServerBitSet, SERVER_BS_BOSS_LEFT)
		SET_SERVER_MISSION_END_REASON(GBBELLYBEAST_ENDREASON_BOSS_LEFT)
		SET_SERVER_MISSION_STAGE(GBBELLYBEAST_REWARDS)
	ENDIF
ENDPROC

/// PURPOSE: Give out rewards when hit is over.
PROC GIVE_REWARDS()
	GANG_BOSS_MANAGE_REWARDS_DATA structGangBossManageRewardsData
	
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_REWARDS_GIVEN)
		IF GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE()
			CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [GIVE_REWARDS] - Local player is passive. No reward.")
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_REWARDS_GIVEN)
			EXIT
		ENDIF
		
		IF GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
			CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [GIVE_REWARDS] - Local player is hiding boss work. No reward.")
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,CLIENT_BS_REWARDS_GIVEN)
			EXIT
		ENDIF
		
		CALCULATE_REWARDS(structGangBossManageRewardsData.iExtraCash, structGangBossManageRewardsData.iExtraRp)
	
		IF GB_IS_LOCAL_PLAYER_MEMBER_OF_THIS_GANG(GB_GET_LOCAL_PLAYER_MISSION_HOST())
			IF serverBD.iDeliveredCount > 0
				GANG_BOSS_MANAGE_WANTED_CLEAN_UP(FALSE, PLAYER_ID())
				GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_BELLY_BEAST, TRUE, structGangBossManageRewardsData)
			ELSE
				GANG_BOSS_MANAGE_WANTED_CLEAN_UP(FALSE, INVALID_PLAYER_INDEX())
				GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_BELLY_BEAST, FALSE, structGangBossManageRewardsData)
			ENDIF
		ELSE	
			GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_BELLY_BEAST, (GET_VEHICLES_DESTROYED() > 0), structGangBossManageRewardsData)
			
			IF GB_GET_LOCAL_PLAYER_GANG_BOSS() != INVALID_PLAYER_INDEX()
			AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(GB_GET_LOCAL_PLAYER_GANG_BOSS()) != FMMC_TYPE_GB_BELLY_BEAST
				INT iBossCut, iRP
				CALCULATE_REWARDS(iBossCut, iRP, FALSE, TRUE)
				GB_HANDLE_GANG_BOSS_CUT(iBossCut)
				CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [GIVE_REWARDS] - Boss is not on script. Paying Boss Cut of $", iBossCut)
			ENDIF
		ENDIF
	
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_REWARDS_GIVEN)
	ENDIF
ENDPROC

PROC DRAW_UI_TIMER()
	IF GB_SHOULD_UI_BE_HIDDEN()
	OR GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) <> GB_UI_LEVEL_FULL
		CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [DRAW_UI_TIMER] - Hiding timer.")
		EXIT
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_WORK_STARTED)
		IF HAS_NET_TIMER_STARTED(serverBD.stWorkTimer)
			HUD_COLOURS timeColour

			IF GET_SERVER_MISSION_STAGE() < GBBELLYBEAST_REWARDS
				iCurrentTime = (GB_GET_WORK_DURATION() - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBd.stWorkTimer))
			ENDIF
			
			IF iCurrentTime > 30000
				timeColour = HUD_COLOUR_WHITE
			ELSE
				timeColour = HUD_COLOUR_RED
			ENDIF
			iCurrentTime = IMAX(iCurrentTime,0)
			
			IF iCurrentTime > 0
				DRAW_GENERIC_TIMER(iCurrentTime, "GB_WORK_END", DEFAULT, TIMER_STYLE_DONTUSEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, timeColour, DEFAULT, DEFAULT, DEFAULT, timeColour)
			ELSE
				DRAW_GENERIC_TIMER(0, "GB_WORK_END", DEFAULT, TIMER_STYLE_DONTUSEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, timeColour, DEFAULT, DEFAULT,DEFAULT, timeColour)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//----------------------
//	SCRIPT PROCESSING
//----------------------

PROC CLIENT_PROCESSING()

	#IF IS_DEBUG_BUILD
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_TIME_UP)
		CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [CLIENT_PROCESSING] - DEBUG KEY PRESSED: F - TIME UP!")
	ENDIF
	#ENDIF

	SWITCH GET_CLIENT_MISSION_STAGE()
	
		CASE GBBELLYBEAST_INIT
			IF GB_IS_LOCAL_PLAYER_MEMBER_OF_THIS_GANG(GB_GET_LOCAL_PLAYER_MISSION_HOST())
				GB_COMMON_BOSS_MISSION_SETUP(FMMC_TYPE_GB_BELLY_BEAST, TRUE)
			ELSE
				GB_COMMON_BOSS_MISSION_SETUP(FMMC_TYPE_GB_BELLY_BEAST, FALSE)
			ENDIF
			
			INIT_VEHICLE_LOCKS()
			
			IF GET_SERVER_MISSION_STAGE() > GBBELLYBEAST_INIT
				IF GB_IS_LOCAL_PLAYER_MEMBER_OF_THIS_GANG(GB_GET_LOCAL_PLAYER_MISSION_HOST())
					GB_SET_LOCAL_PLAYER_CRITICAL_TO_JOB(TRUE)
					SET_CLIENT_MISSION_STAGE(GBBELLYBEAST_STEAL)
				ELSE
					SET_CLIENT_MISSION_STAGE(GBBELLYBEAST_DESTROY)
				ENDIF
			ENDIF
		BREAK
		
		// Freemode players
		CASE GBBELLYBEAST_DESTROY
			IF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_VEHICLE_ENTERED)
			OR IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_AUTOSTART)
				IF NOT IS_BIT_SET(iLocalBitset, GBBELLYBEAST_BS_INTRO_DONE)
					HANDLE_SHARD_HELP_INTRO()
				ENDIF
				
				// UI
				DRAW_UI_TIMER()
				MAINTAIN_OBJECTIVE_TEXT()
				DRAW_VEHICLE_MARKER()
				MAINTAIN_HELP()
				
				// Logic
				SET_VEHICLES_AS_VINCIBLE()
				MAINTAIN_BLIPS()
				MAINTAIN_CLOSEST_VEHICLE()
				GB_MAINTAIN_BOSS_WORK_DISTANT_CHECKS(FMMC_TYPE_GB_BELLY_BEAST, vClosestAction, bSet)
				
				IF GET_SERVER_MISSION_STAGE() > GBBELLYBEAST_STEAL
					SET_CLIENT_MISSION_STAGE(GBBELLYBEAST_REWARDS)
				ENDIF
			ENDIF
		BREAK
		
		// Members of the organisation
		CASE GBBELLYBEAST_STEAL
			IF NOT IS_BIT_SET(iLocalBitset, GBBELLYBEAST_BS_INTRO_DONE)
				HANDLE_SHARD_HELP_INTRO()
			ENDIF
			
			// UI
			DRAW_UI_TIMER()
			MAINTAIN_OBJECTIVE_TEXT()
			MAINTAIN_BLIPS()
			MAINTAIN_INTRO_TEXTS()
			DRAW_VEHICLE_MARKER()
			MAINTAIN_HELP()
			
			// Game Logic
			SET_VEHICLES_AS_VINCIBLE()
			MAINTAIN_NEAR_STATION_CHECK()
			MAINTAIN_WANTED_LEVEL()
			HANDLE_LOCAL_IN_VEHICLE()
			
			MAINTAIN_CLOSEST_VEHICLE()
			GB_MAINTAIN_BOSS_WORK_DISTANT_CHECKS(FMMC_TYPE_GB_BELLY_BEAST, vClosestAction, bSet)
			
			DRAW_CHALLENGE_DPAD_LBD(g_GBLeaderboardStruct.challengeLbdStruct, g_GBLeaderboardStruct.siDpadMovie, SUB_BOSS, g_GBLeaderboardStruct.dpadVariables, 
									g_GBLeaderboardStruct.fmDpadStruct)
			
			// Common
			GB_COMMON_BOSS_MISSION_EVERY_FRAME_CALLS(FMMC_TYPE_GB_BELLY_BEAST)
			
			IF GET_SERVER_MISSION_STAGE() > GBBELLYBEAST_STEAL
				SET_CLIENT_MISSION_STAGE(GBBELLYBEAST_REWARDS)
			ENDIF
		BREAK
		
		CASE GBBELLYBEAST_REWARDS
			HANDLE_SHARD_OUTRO()
			GIVE_REWARDS()
			DRAW_UI_TIMER()
			HANDLE_LOCAL_IN_VEHICLE()
			
			IF IS_BIT_SET(iLocalBitset, GBBELLYBEAST_BS_END_SHARD_DONE)
			AND IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_REWARDS_GIVEN)
			AND NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
			AND GB_MAINTAIN_BOSS_END_UI(sEndUI)
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BS_OUTRO_DONE)
			ENDIF
			
			IF GET_SERVER_MISSION_STAGE() > GBBELLYBEAST_REWARDS
				SET_CLIENT_MISSION_STAGE(GBBELLYBEAST_END)
			ENDIF
		BREAK
		
		CASE GBBELLYBEAST_END
		
		BREAK

	ENDSWITCH

ENDPROC

PROC SERVER_PROCESSING()
	
	SWITCH serverBD.eGBBELLYBEAST_Stage
	
		CASE GBBELLYBEAST_INIT
			IF SHOULD_JOB_START()
				SET_SERVER_MISSION_STAGE(GBBELLYBEAST_STEAL)
			ENDIF
			
			MAINTAIN_ACTIVE_PLAYER_CHECKS()
		BREAK
		
		CASE GBBELLYBEAST_STEAL
			MAINTAIN_ACTIVE_PLAYER_CHECKS()
			MAINTAIN_END_CONDITION_CHECKS()
			MAINTAIN_AUTOSTART_TIMEUP()
		BREAK
		
		CASE GBBELLYBEAST_DESTROY
			MAINTAIN_ACTIVE_PLAYER_CHECKS()
			MAINTAIN_END_CONDITION_CHECKS()
			MAINTAIN_AUTOSTART_TIMEUP()
		BREAK
		
		CASE GBBELLYBEAST_REWARDS
			IF IS_BIT_SET(serverBD.iServerBitSet, SERVER_BS_OUTRO_DONE)
				SET_SERVER_MISSION_STAGE(GBBELLYBEAST_END)
			ENDIF
			
			MAINTAIN_ACTIVE_PLAYER_CHECKS()
		BREAK
		
		CASE GBBELLYBEAST_END
			
		BREAK

	ENDSWITCH

ENDPROC

FUNC BOOL INIT_CLIENT()

	RETURN TRUE
ENDFUNC

FUNC BOOL INIT_SERVER()
	IF INIT_GBBELLYBEAST()
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)

	CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === PROCESS_PRE_GAME")
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission), missionScriptArgs)
	
	//Reserve Entities
	RESERVE_NETWORK_MISSION_OBJECTS(0)
	RESERVE_NETWORK_MISSION_VEHICLES(GB_GET_BOSS_MISSION_NUM_VEH_REQUIRED(FMMC_TYPE_GB_BELLY_BEAST))
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD,SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD,SIZE_OF(playerBD))
	
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	GB_COMMON_BOSS_MISSION_PREGAME(FMMC_TYPE_GB_BELLY_BEAST)
	
	#IF IS_DEBUG_BUILD
		CREATE_WIDGETS()
	#ENDIF
	
	RETURN TRUE
	
ENDFUNC

//----------------------
//	MAIN SCRIPT
//----------------------

SCRIPT(MP_MISSION_DATA missionScriptArgs)


	CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === START")
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NOT PROCESS_PRE_GAME(missionScriptArgs)
			CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [SCRIPT] - NOT PROCESS_PRE_GAME(missionScriptArgs) - Calling SCRIPT_CLEANUP()")
			SCRIPT_CLEANUP()
		ENDIF	
	ENDIF
	
	WHILE (TRUE)
	
		MP_LOOP_WAIT_ZERO()
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")
			SCRIPT_CLEANUP(TRUE)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF bKillScript
			SET_SERVER_MISSION_STATE(GAME_STATE_END)
		ENDIF
		#ENDIF
		
		MAINTAIN_DAMAGE_EVENT_LISTEN()
		
		SWITCH(GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT()))
		
			CASE GAME_STATE_INIT
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					IF INIT_CLIENT()
						SET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT(),GAME_STATE_RUNNING)
					ENDIF
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					SET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT(),GAME_STATE_END)
				ENDIF
			BREAK
			
			CASE GAME_STATE_RUNNING
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					IF NOT GB_SHOULD_EVENT_BE_HIDDEN()
						CLIENT_PROCESSING()
					ELSE	
						IF Is_There_Any_Current_Objective_Text()
							Clear_Any_Objective_Text_From_This_Script()
							CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === - GB_SHOULD_EVENT_BE_HIDDEN - Clearing objective text.")
						ENDIF
						CLEANUP_WORK_BLIPS()
					ENDIF
					IF GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION()
						SET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT(),GAME_STATE_END)
					ENDIF
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					SET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT(),GAME_STATE_END)
				ENDIF
			BREAK
			
			CASE GAME_STATE_END
				CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [SCRIPT] - CLIENT - GAME_STATE_END - Calling SCRIPT_CLEANUP()")
				SCRIPT_CLEANUP()
			BREAK
			
		ENDSWITCH
	
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
			IF GB_SHOULD_KILL_ACTIVE_BOSS_MISSION()
				SET_SERVER_MISSION_STATE(GAME_STATE_END)
			ENDIF
		
			SWITCH(GET_SERVER_MISSION_STATE())
			
				CASE GAME_STATE_INIT
					IF INIT_SERVER()
						SET_SERVER_MISSION_STATE(GAME_STATE_RUNNING)
					ENDIF
				BREAK
				
				CASE GAME_STATE_RUNNING
					IF GET_SERVER_MISSION_STAGE() = GBBELLYBEAST_END
						SET_SERVER_MISSION_STATE(GAME_STATE_END)
					ELSE
						SERVER_PROCESSING()
					ENDIF
				BREAK
				
				CASE GAME_STATE_END
					CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_BELLYBEAST === [SCRIPT] - SERVER - GAME_STATE_END - Calling SCRIPT_CLEANUP()")
					SCRIPT_CLEANUP()
				BREAK
				
			ENDSWITCH
		
		ENDIF
		
	ENDWHILE

	
	
// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT

