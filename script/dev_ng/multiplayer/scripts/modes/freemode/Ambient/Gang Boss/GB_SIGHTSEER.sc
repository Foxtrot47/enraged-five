//////////////////////////////////////////////////////////////////////////////////////////
// Name:        GB_SIGHTSEER.sc															//
// Description: Boss collects packages - have to complete hacking mg to find loc		//
// Written by:  David Watson															//
// Date: 12/10/2015																		//
//////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "net_mission.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_event.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"

//
USING "net_scoring_common.sch"
USING "net_ambience.sch"

//USING "help_at_location.sch"
USING "commands_object.sch"

USING "net_xp_animation.sch"

USING "net_wait_zero.sch"

USING "hacking_public.sch"

USING "net_gang_boss.sch"
USING "DM_Leaderboard.sch"
USING "am_common_ui.sch"
#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF

// Game States. 
CONST_INT GAME_STATE_INI 				0
CONST_INT GAME_STATE_RUNNING			1
CONST_INT GAME_STATE_LEAVE				2
CONST_INT GAME_STATE_TERMINATE_DELAY	3
CONST_INT GAME_STATE_END				4

ENUM SIGHTSEER_STAGE_ENUM
	eAD_COLLECT_PACKAGES,
	eAD_ATTACK,
	eAD_OFF_MISSION,
	eAD_CLEANUP
ENDENUM


CONST_INT HACKING_IP_CONNECT			0
CONST_INT HACKING_BRUTEFORCE			1
CONST_INT HACKING_NEW_MG				2
CONST_INT MAX_NUM_HACKING_MG			3

CONST_INT VEH_EXP_RANGE	50
CONST_INT MAX_PLAYERS	4

CONST_INT MAX_MW_PEDS	24
CONST_INT MAX_MW_VEHS	5
CONST_INT MAX_MW_ON_FOOT_PEDS	4

CONST_INT MAX_MW_PED_SPAWN_LOC	5
CONST_INT MAX_MW_VEH_SPAWN_LOC	5
CONST_INT MAX_MW_HEL_SPAWN_LOC	5


CONST_INT MAX_MW_PEDS_PER_VEH	4

CONST_INT MAX_VEH_AT_ONCE		3

CONST_INT MAX_PACKAGES			3
CONST_INT MAX_PACKAGE_LOCATIONS	20

CONST_INT TIME_MISSION_DURATION 60000 * 15

CONST_INT TIME_EXPOLDE_VEH		30000

CONST_INT TIME_MISSION_TIMEOUT	600000

CONST_FLOAT MIN_DISTANCE_BETWEEN_PACKAGES		1000.0

CONST_INT TIME_FORCE_RIVALS_JOIN	600000


//Server Bitset Data
CONST_INT biS_SHouldDeletePackage		0
CONST_INT biS_AllPackagesCollected		1
CONST_INT biS_AllPlayersFinished		2
CONST_INT biS_DurationExpired			3
CONST_INT biS_BossKilled				4
CONST_INT biS_BossLaunchedQuit			5

CONST_INT PED_AI_STATE_CREATED			0
CONST_INT PED_AI_STATE_GOTO_PLAYERS		1
CONST_INT PED_AI_STATE_ATTACK_PLAYERS	2
CONST_INT PED_AI_STATE_CLEAN_UP			3
CONST_INT PED_AI_STATE_FINISHED			4
// The server broadcast data.
// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData
	INT iServerGameState
		
	INT iServerBitSet
	
	NETWORK_INDEX niVeh

	
	NETWORK_INDEX niPackage	
	
	INT iPackagesCreatedBitSet
	
	INT iPackagesCollected
	INT iMissingPackages
	INT iPedState[MAX_MW_PEDS]
	
	SIGHTSEER_STAGE_ENUM eStage = eAD_COLLECT_PACKAGES
	
	INT iBossPartWhoLaunched = -1
	INT iBossPlayerWhoLaunched = -1
	INT iPlayerKilledBoss = -1
	INT iPackageLocToUse = 0
	
	INT iLastPlayerToCollect = -1
	VECTOR vPackageLoc[MAX_PACKAGES]
	INT iMapArea = -1
	
	SCRIPT_TIMER MissionTerminateDelayTimer
	
	SCRIPT_TIMER timeDuration
	SCRIPT_TIMER timeForceRivalsJoin
	SCRIPT_TIMER timeResetAlpha
	INT iMinPlayers = -1
	
	INT iMatchId1
	INT iMatchId2
	
	#IF IS_DEBUG_BUILD
	BOOL bAllowWith2Players = FALSE
	BOOL bSomeoneSPassed
	#ENDIF
ENDSTRUCT
ServerBroadcastData serverBD



CONST_INT biP_MissionStartExpired	0
CONST_INT biP_JoinedAsRival			1
CONST_INT biP_GotThePackage			2
CONST_INT biP_JustCompHack			3
CONST_INT biP_Finished				4
CONST_INT biP_KilledBoss			5
CONST_INT biS_ForceRivalsOnMission	6
// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	INT iGameState
	
	INT iPlayerBitSet
	
	SIGHTSEER_STAGE_ENUM eStage = eAD_COLLECT_PACKAGES
	
	INT iMyBossPart = -1
	
	INT iNumCompletedHackingGames = 0
	INT iPackagesCollected = 0
	#IF IS_DEBUG_BUILD
	INT iPDebugBitset
	#ENDIF
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

///// MISSION VARIABLES /////
INT iBoolsBitSet
CONST_INT biHelp1Done			0
CONST_INT biL_DoneIntroShard	1
CONST_INT biL_DoneIntroHelp		2
CONST_INT biL_SetPackageLOD		3
CONST_INT biL_KilledWhileHack	4
CONST_INT biL_BlippedRival		5
CONST_INT biL_DoneEndTelemetry	6
CONST_INT biL_DoneAppHelp		7
CONST_INT biL_DonePackageHelp	8
CONST_INT biL_WaitForShard		9
CONST_INT biL_ResetBlipForShard	10
CONST_INT biL_SetBlipHeight		11
CONST_INT biL_RivalsJoinedHelp	12
CONST_INT biL_PreventCollection	13

INT iMusicBitset
CONST_INT biMusic_Init					0
CONST_INT biMusic_TriggerStart			1
CONST_INT biMusic_Reset					2
CONST_INT biMusic_2Packages				3
CONST_INT biMusic_1Package				4
CONST_INT biMusic_30sStarted			5
CONST_INT biMusic_StopAllCPack			6
CONST_INT biMusic_Attack				7
CONST_INT biMusic_ShouldStop30s			8
CONST_INT biMusic_Started30sCountdown	9
CONST_INT biMusic_StoppedCountdownMusic	10
CONST_INT biMusic_PrepareKill30s		11
CONST_INT biMusic_RestoreRadio			12
CONST_INT biMusic_DoneFadeInRadio		13
CONST_INT biMusic_DoneStart30s			14
CONST_INT biMusic_DonePre30sStop		15


INT iStaggeredParticipant

INT iLocalPackagesCollected

BLIP_INDEX VehBlip
//BLIP_INDEX MwPedBlip[MAX_MW_PEDS]
//BLIP_INDEX MwVehBlip[MAX_MW_VEHS]

//AI_BLIP_STRUCT MwPedBlipData[MAX_MW_PEDS]

BLIP_INDEX blipPackage

//--Relationship groups

REL_GROUP_HASH relUWPlayer
REL_GROUP_HASH relUWAi 
REL_GROUP_HASH relMyFmGroup

VECTOR vCreateEntityCurrentAttemptedCoords
FLOAT fCreateEntityCurrentAttemptedHeading

S_HACKING_DATA hackSightseer
INT iHackingProg = -1
INT iMyHackingMG = -1
INT iHackSpeed

INT iPickupSound = -1
INT iNewPackageSound = -1
//SCRIPT_TIMER OutOfVehTimer

INT iLocalCollectedPackageCountForTicker = 0

VECTOR vPackageLocations[MAX_PACKAGE_LOCATIONS]

GB_STRUCT_BOSS_END_UI gbBossEndUi

BOOL bDoneDistanceCheck
// Widgets & Debug.
#IF IS_DEBUG_BUILD
	BOOL bHostEndMissionNow
	BOOL bWdDoHack
	
	BOOL bWdAppAvail
	BOOL bWdAppTrg
	
	CONST_INT biPDebug_PressedS		0
#ENDIF

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MAIN PROCS                            ////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//--Tunables
FUNC INT GET_GB_SIGHTSEER_DURATION()
	// RETURN 45000
	RETURN g_sMPTunables.igb_sightseer_time_limit //TIME_MISSION_DURATION
ENDFUNC

FUNC INT GET_GB_SIGHTSEER_TOTAL_NUM_PACKAGES()
	//RETURN 2
	RETURN g_sMPTunables.igb_sightseer_number_of_packages     // MAX_PACKAGES
ENDFUNC

FUNC FLOAT GET_MIN_DIST_BETWEEN_PACKAGES()
	RETURN TO_FLOAT(g_sMPTunables.igb_sightseer_minimum_distance_to_next_package)
ENDFUNC

FUNC INT GET_GB_SIGHTSEER_FORCE_RIVALS_JOIN_TIME()
	RETURN TIME_FORCE_RIVALS_JOIN
ENDFUNC

FUNC BOOL SHOULD_LOCAL_PLAYER_SEE_GB_SIGHTSEER_HUD()
	IF NOT MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI 
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [SHOULD_LOCAL_PLAYER_SEE_GB_SIGHTSEER_HUD] FALSE - GB_IS_IT_SAFE_TO_DISPLAY_BOSS_MISSION_UI")
		RETURN FALSE
	ENDIF
	
	IF GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - SHOULD_LOCAL_PLAYER_SEE_GB_SIGHTSEER_HUD - FALSE AS GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
	RETURN (IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_JoinedAsRival))
ENDFUNC

PROC PROCESS_SIGHTSEER_DAMAGE_EVENT(INT iCount)

	STRUCT_ENTITY_DAMAGE_EVENT sei 

	PED_INDEX pedKiller
	PED_INDEX pedTemp
	PLAYER_INDEX playerVictim
	PLAYER_INDEX playerKiller
	PLAYER_INDEX playerBoss
	
	#IF IS_DEBUG_BUILD
	INT iVictimPart
	#ENDIF
	// Grab the event data.
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sei, SIZE_OF(sei))
		IF serverBD.iBossPlayerWhoLaunched > -1
			IF DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
				IF NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
					IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_KilledBoss)
						IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_BossKilled)
							IF DOES_ENTITY_EXIST(sei.VictimIndex)	
								IF sei.VictimDestroyed
									IF IS_ENTITY_A_PED(sei.VictimIndex)
										pedTemp = GET_PED_INDEX_FROM_ENTITY_INDEX(sei.VictimIndex)	
										IF IS_PED_A_PLAYER(pedTemp)
											playerVictim = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedTemp)
											CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [PROCESS_SIGHTSEER_DAMAGE_EVENT] THINK THIS PLAYER HAS BEEN KILLED ", GET_PLAYER_NAME(playerVictim))
											IF playerVictim = INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched)
												CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [PROCESS_SIGHTSEER_DAMAGE_EVENT] THINK THE BOSS PLAYER WHO LAUNCHED HAS BEEN KILLED BY SOMEONE ", GET_PLAYER_NAME(playerVictim))
												IF DOES_ENTITY_EXIST(sei.DamagerIndex)
													IF IS_ENTITY_A_PED(sei.DamagerIndex)	
														pedKiller = GET_PED_INDEX_FROM_ENTITY_INDEX(sei.DamagerIndex)					
														IF IS_PED_A_PLAYER(pedKiller)
															playerKiller = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedKiller)
															CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [PROCESS_SIGHTSEER_DAMAGE_EVENT] THINK THE BOSS PLAYER WHO LAUNCHED ", GET_PLAYER_NAME(playerVictim), " HAS BEEN KILLED BY PLAYER ", GET_PLAYER_NAME(playerKiller))
															IF playerKiller = PLAYER_ID()
																SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_KilledBoss)
																CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [PROCESS_SIGHTSEER_DAMAGE_EVENT] THINK I KILLED THE BOSS PLAYER WHO LAUNCHED ", GET_PLAYER_NAME(playerVictim))
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF	
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF				
	ENDIF
	
	//-- Set rivals as permanant participants if they damage someone in the key organization
		IF DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
			IF NOT GB_IS_GLOBAL_CLIENT_BIT0_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_0_PERMANENT_PARTICIPANT)
				IF DOES_ENTITY_EXIST(sei.VictimIndex)	
					IF IS_ENTITY_A_PED(sei.VictimIndex)
						pedTemp = GET_PED_INDEX_FROM_ENTITY_INDEX(sei.VictimIndex)
						IF IS_PED_A_PLAYER(pedTemp)
							playerVictim = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedTemp)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [PROCESS_SIGHTSEER_DAMAGE_EVENT] THINK THIS PLAYER HAS BEEN DAMAGED BY SOMEONE ", GET_PLAYER_NAME(playerVictim))
							IF DOES_ENTITY_EXIST(sei.DamagerIndex)
								IF IS_ENTITY_A_PED(sei.DamagerIndex)	
									pedKiller = GET_PED_INDEX_FROM_ENTITY_INDEX(sei.DamagerIndex)				
									IF IS_PED_A_PLAYER(pedKiller)
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [PROCESS_SIGHTSEER_DAMAGE_EVENT] THINK THIS PLAYER HAS BEEN DAMAGED BY A PLAYER ", GET_PLAYER_NAME(playerVictim))
										playerKiller = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedKiller)
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [PROCESS_SIGHTSEER_DAMAGE_EVENT] THINK THIS PLAYER ", GET_PLAYER_NAME(playerVictim), " HAS BEEN DAMAGED BY THIS PLAYER ", GET_PLAYER_NAME(playerKiller))
										IF playerKiller = PLAYER_ID()
											CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [PROCESS_SIGHTSEER_DAMAGE_EVENT] THINK THIS PLAYER ", GET_PLAYER_NAME(playerVictim), " HAS BEEN DAMAGED BY ME")
											
											IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerVictim)
												#IF IS_DEBUG_BUILD
												iVictimPart = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(playerVictim))
												CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [PROCESS_SIGHTSEER_DAMAGE_EVENT] THINK THIS PLAYER ", GET_PLAYER_NAME(playerVictim), " IS A PARTICIPANT iVictimPart = ", iVictimPart)
												#ENDIF

												//-- Have I damaged someone in launching gang?
												IF GB_IS_PLAYER_MEMBER_OF_A_GANG(playerVictim)
													CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [PROCESS_SIGHTSEER_DAMAGE_EVENT] VICTIM ", GET_PLAYER_NAME(playerVictim), " IS A MEMBER OF A GANG")
													IF serverBD.iBossPartWhoLaunched > -1
														playerBoss = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.iBossPartWhoLaunched))
														CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [PROCESS_SIGHTSEER_DAMAGE_EVENT] CHECKING IF VICTIM ", GET_PLAYER_NAME(playerVictim), " IS IN LAUNCHING BOSS'S GANG", GET_PLAYER_NAME(playerBoss))
														IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerVictim, playerBoss)
															CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [PROCESS_SIGHTSEER_DAMAGE_EVENT] SET AS PERMENANT AS I HAVE DAMAGED PLAYER ",GET_PLAYER_NAME(playerVictim), " WHO IS A MEMBER OF THIS PLAYER'S GANG", GET_PLAYER_NAME(playerBoss))
															GB_SET_PLAYER_AS_PERMANENT_ON_BOSS_MISSION()
															GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
														ENDIF
													ENDIF
												ENDIF
												
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF

					ENDIF
				ENDIF
			ENDIF
		ENDIF
ENDPROC

PROC PROCESS_EVENTS()
	
	INT iCount
	EVENT_NAMES ThisScriptEvent
	
	//process the events
	
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount
		
		ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)
		
		SWITCH ThisScriptEvent
			
			CASE EVENT_NETWORK_DAMAGE_ENTITY
				

				PROCESS_SIGHTSEER_DAMAGE_EVENT(iCount)

				
				
			BREAK
			
		ENDSWITCH
		
	ENDREPEAT
	
	
ENDPROC

/// PURPOSE:
///    Store my boss's participant ID in playerBD, so I don't have to calculate it every time
FUNC INT GET_MY_BOSS_PARTICPANT_ID()
	IF playerBD[PARTICIPANT_ID_TO_INT()].iMyBossPart != -1
		//-- Already stored
		RETURN playerBD[PARTICIPANT_ID_TO_INT()].iMyBossPart
	ENDIF
	
	PLAYER_INDEX playerBoss
	
	IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
		//-- I'm a a Boss, store my participant ID
		playerBD[PARTICIPANT_ID_TO_INT()].iMyBossPart = PARTICIPANT_ID_TO_INT()
		IF PARTICIPANT_ID_TO_INT() = serverBD.iBossPartWhoLaunched	
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [GET_MY_BOSS_PARTICPANT_ID] SETTING MYSELF CRITICAL AS I'M THE BOSS WHO LAUNCHED")
			GB_SET_LOCAL_PLAYER_CRITICAL_TO_JOB(TRUE)
		ENDIF
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [GET_MY_BOSS_PARTICPANT_ID] SETTING iMyBossPart TO ", PARTICIPANT_ID_TO_INT(), " (MYSELF) AS I'M A BOSS")
	ELSE
		//-- I'm not a boss
		IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
			playerBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
			IF playerBoss != INVALID_PLAYER_INDEX()
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [GET_MY_BOSS_PARTICPANT_ID] THINK MY BOSS IS ", GET_PLAYER_NAME(playerBoss))
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerBoss)
					playerBD[PARTICIPANT_ID_TO_INT()].iMyBossPart = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(playerBoss))
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [GET_MY_BOSS_PARTICPANT_ID] SETTING iMyBossPart TO ", playerBD[PARTICIPANT_ID_TO_INT()].iMyBossPart, " WHO IS PLAYER ",GET_PLAYER_NAME(playerBoss))
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN playerBD[PARTICIPANT_ID_TO_INT()].iMyBossPart
ENDFUNC

/// PURPOSE:
///    Determine if it was my boss that launched the assault Boss Work
FUNC BOOL DID_MY_BOSS_LAUNCH_GB_SIGHTSEER()
	INT iMyBossPart = GET_MY_BOSS_PARTICPANT_ID()
	IF iMyBossPart > -1
		IF serverBD.iBossPartWhoLaunched = iMyBossPart
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC STOP_30S_COUNTDOWN_AUDIO()
//	EXIT
	
	
	//-- From spreadsheet
	//--  //depot/gta5/docs/Audio/Interactive_Music/DLC/Apartments_Magnate.xlsx
	IF IS_BIT_SET(iMusicBitset, biMusic_ShouldStop30s)
		IF IS_BIT_SET(iMusicBitset, biMusic_Started30sCountdown)
			IF NOT IS_BIT_SET(iMusicBitset, biMusic_StoppedCountdownMusic)
			
				//-- Restore radio control 
				IF IS_BIT_SET(iMusicBitset, biMusic_PrepareKill30s)	
					IF NOT IS_BIT_SET(iMusicBitset, biMusic_RestoreRadio)
						SET_USER_RADIO_CONTROL_ENABLED(TRUE)
						SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [STOP_30S_COUNTDOWN_AUDIO] SSET_USER_RADIO_CONTROL_ENABLED(TRUE)")
					ENDIF
				ENDIF
				
				IF NOT IS_BIT_SET(iMusicBitset, biMusic_DonePre30sStop)
					IF TRIGGER_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
						SET_BIT(iMusicBitset, biMusic_DonePre30sStop)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [STOP_30S_COUNTDOWN_AUDIO] SET biMusic_DonePre30sStop")
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(iMusicBitset, biMusic_DonePre30sStop)
					IF NOT IS_BIT_SET(iMusicBitset, biMusic_DoneFadeInRadio)
						IF TRIGGER_MUSIC_EVENT("APT_FADE_IN_RADIO")
							SET_BIT(iMusicBitset, biMusic_DoneFadeInRadio)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [STOP_30S_COUNTDOWN_AUDIO] SET biMusic_DoneFadeInRadio")
						ENDIF
					ENDIF
					
					IF IS_BIT_SET(iMusicBitset, biMusic_DoneFadeInRadio)
						SET_BIT(iMusicBitset, biMusic_StoppedCountdownMusic)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [STOP_30S_COUNTDOWN_AUDIO] SET biMusic_StoppedCountdownMusic")
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_GB_SIGHTSEER_MUSIC()
	//EXIT
	
	
	STOP_30S_COUNTDOWN_AUDIO()
	
	IF IS_BIT_SET(iMusicBitset, biMusic_Started30sCountdown)
		//-- Don't do anything here if we're doing the 30s countdown audio
		EXIT
	ENDIF
	IF DID_MY_BOSS_LAUNCH_GB_SIGHTSEER()
		IF NOT IS_BIT_SET(iMusicBitset, biMusic_Init)
			SET_AUDIO_FLAG("DisableFlightMusic", TRUE) 
			SET_AUDIO_FLAG("WantedMusicDisabled", TRUE)
			
			SET_BIT(iMusicBitset, biMusic_Init)
			
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_GB_SIGHTSEER_MUSIC] SET biMusic_Init")
		ENDIF
		
		//-- Event start
		IF NOT IS_BIT_SET(iMusicBitset, biMusic_TriggerStart)
			IF TRIGGER_MUSIC_EVENT("BG_SIGHTSEER_START")
				SET_BIT(iMusicBitset, biMusic_TriggerStart)
			
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_GB_SIGHTSEER_MUSIC] SET biMusic_TriggerStart")
			ELSE
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_GB_SIGHTSEER_MUSIC] TRYING TO TRIGGER BG_SIGHTSEER_START")
			ENDIF
		ENDIF
		
		//-- 2 packages remaining
		IF IS_BIT_SET(iMusicBitset, biMusic_TriggerStart)
			IF NOT IS_BIT_SET(iMusicBitset, biMusic_2Packages)
				IF serverBD.iPackagesCollected = 1
					IF TRIGGER_MUSIC_EVENT("BG_SIGHTSEER_MID")
						SET_BIT(iMusicBitset, biMusic_2Packages)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_GB_SIGHTSEER_MUSIC] SET biMusic_2Packages")
					ELSE
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_GB_SIGHTSEER_MUSIC] TRYING TO TRIGGER BG_SIGHTSEER_MID")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//-- 1 package remaining
		IF IS_BIT_SET(iMusicBitset, biMusic_2Packages)
			IF NOT IS_BIT_SET(iMusicBitset, biMusic_1Package)
				IF serverBD.iPackagesCollected = 2
					IF TRIGGER_MUSIC_EVENT("BG_SIGHTSEER_FINAL")
						SET_BIT(iMusicBitset, biMusic_1Package)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_GB_SIGHTSEER_MUSIC] SET biMusic_1Package")
					ELSE
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_GB_SIGHTSEER_MUSIC] TRYING TO TRIGGER BG_SIGHTSEER_FINAL")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//-- Stop when all collected / Boss killed
		IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllPackagesCollected)
		OR IS_BIT_SET(serverBD.iServerBitSet, biS_BossKilled)
			IF IS_BIT_SET(iMusicBitset, biMusic_1Package)
				IF NOT IS_BIT_SET(iMusicBitset, biMusic_StopAllCPack)
					IF TRIGGER_MUSIC_EVENT("BG_SIGHTSEER_STOP")
						SET_BIT(iMusicBitset, biMusic_StopAllCPack)
						SET_BIT(iMusicBitset, biMusic_Reset)
						SET_AUDIO_FLAG("DisableFlightMusic", FALSE) 
						SET_AUDIO_FLAG("WantedMusicDisabled", FALSE)
						
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_GB_SIGHTSEER_MUSIC] SET biMusic_1Package")
					ELSE
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_GB_SIGHTSEER_MUSIC] TRYING TO TRIGGER BG_SIGHTSEER_STOP")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//-- Rival players
	IF DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
		IF GB_IS_GLOBAL_CLIENT_BIT0_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_0_PERMANENT_PARTICIPANT)
			IF NOT IS_BIT_SET(iMusicBitset, biMusic_Init)
				SET_AUDIO_FLAG("DisableFlightMusic", TRUE) 
				SET_AUDIO_FLAG("WantedMusicDisabled", TRUE)
				
				SET_BIT(iMusicBitset, biMusic_Init)
				
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_GB_SIGHTSEER_MUSIC] SET biMusic_Init")
			ENDIF
			
			IF NOT IS_BIT_SET(iMusicBitset, biMusic_Attack)
				IF TRIGGER_MUSIC_EVENT("BG_SIGHTSEER_START_ATTACK")
					SET_BIT(iMusicBitset, biMusic_Attack)
				
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_GB_SIGHTSEER_MUSIC] SET biMusic_Attack")
				ELSE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_GB_SIGHTSEER_MUSIC] TRYING TO TRIGGER BG_SIGHTSEER_START_ATTACK")
				ENDIF
			ENDIF
			
			//-- Stop when all collected / Boss killed
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllPackagesCollected)
			OR IS_BIT_SET(serverBD.iServerBitSet, biS_BossKilled)
				IF IS_BIT_SET(iMusicBitset, biMusic_Attack)
					IF NOT IS_BIT_SET(iMusicBitset, biMusic_StopAllCPack)
						IF TRIGGER_MUSIC_EVENT("BG_SIGHTSEER_STOP")
							SET_BIT(iMusicBitset, biMusic_StopAllCPack)
							SET_BIT(iMusicBitset, biMusic_Reset)
							SET_AUDIO_FLAG("DisableFlightMusic", FALSE) 
							SET_AUDIO_FLAG("WantedMusicDisabled", FALSE)
							
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_GB_SIGHTSEER_MUSIC] SET biMusic_1Package")
						ELSE
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_GB_SIGHTSEER_MUSIC] TRYING TO TRIGGER BG_SIGHTSEER_STOP")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC



PROC MAINTAIN_30S_COUNTDOWN_MUSIC(INT iTime)
	//EXIT
	
	
	//-- From spreadsheet
	//--  //depot/gta5/docs/Audio/Interactive_Music/DLC/Apartments_Magnate.xlsx
	
	IF IS_BIT_SET(iMusicBitset, biMusic_ShouldStop30s)
		STOP_30S_COUNTDOWN_AUDIO()
	//	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_30S_COUNTDOWN_MUSIC] EXIT AS biMusic_ShouldStop30s")
		EXIT 
	ENDIF
	
	IF NOT IS_BIT_SET(iMusicBitset, biMusic_DonePre30sStop)
		IF iTime <= 35000
			IF TRIGGER_MUSIC_EVENT("APT_PRE_COUNTDOWN_STOP")
				SET_BIT(iMusicBitset, biMusic_DonePre30sStop)
				SET_BIT(iMusicBitset, biMusic_Started30sCountdown)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_30S_COUNTDOWN_MUSIC] SET biMusic_DonePre30sStop")
			ELSE
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_30S_COUNTDOWN_MUSIC] TRYING TO TRIGGER APT_PRE_COUNTDOWN_STOP")
			ENDIF
		ENDIF
	ENDIF
	
	
	IF IS_BIT_SET(iMusicBitset, biMusic_Started30sCountdown)
		IF iTime <= 30000
			IF NOT IS_BIT_SET(iMusicBitset, biMusic_PrepareKill30s)
				IF PREPARE_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
					SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE)
					SET_USER_RADIO_CONTROL_ENABLED(FALSE) 
					SET_BIT(iMusicBitset, biMusic_PrepareKill30s)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_30S_COUNTDOWN_MUSIC] SET biMusic_PrepareKill30s")
				ELSE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_30S_COUNTDOWN_MUSIC] TRYING TO PREPARE APT_COUNTDOWN_30S_KILL")
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(iMusicBitset, biMusic_DoneStart30s)
				IF TRIGGER_MUSIC_EVENT("APT_COUNTDOWN_30S")
					SET_BIT(iMusicBitset, biMusic_DoneStart30s)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_30S_COUNTDOWN_MUSIC] SET biMusic_DoneStart30s")
				ELSE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_30S_COUNTDOWN_MUSIC] TRYING TO TRIGGER APT_COUNTDOWN_30S")
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(iMusicBitset, biMusic_DoneStart30s)
				IF iTime <= 27000
					IF NOT IS_BIT_SET(iMusicBitset, biMusic_RestoreRadio)
						SET_USER_RADIO_CONTROL_ENABLED(TRUE)
						SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
						SET_BIT(iMusicBitset, biMusic_RestoreRadio) 
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_30S_COUNTDOWN_MUSIC] SET biMusic_RestoreRadio")
					ENDIF
					
					IF iTime <= 500 
						IF TRIGGER_MUSIC_EVENT("APT_FADE_IN_RADIO")
							CANCEL_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_30S_COUNTDOWN_MUSIC] TRIGGERED APT_FADE_IN_RADIO")
							CLEAR_BIT(iMusicBitset, biMusic_Started30sCountdown)
						ELSE
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_30S_COUNTDOWN_MUSIC] TRYING TO TRIGGER APT_FADE_IN_RADIO")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DISABLE_PLAYER_MISSION_VEHICLE_CONTROLS_THIS_FRAME(BOOL bDisableExit = TRUE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HANDBRAKE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_UD)
	IF bDisableExit
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
	ENDIF
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ROLL_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_PITCH_UD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_YAW_LEFT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_YAW_LEFT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_UP)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_DOWN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_UNDERCARRIAGE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_NEXT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_PREV_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_TARGET_LEFT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_TARGET_RIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_VERTICAL_FLIGHT_MODE)
ENDPROC

PROC DO_SIGHTSEER_HACKING()
	
	BOOL bHackingComplete
	
	IF NOT IS_BIT_SET(iBoolsBitSet,biL_KilledWhileHack)
		IF iHackingProg >= 0
			IF IS_PED_INJURED(PLAYER_PED_ID())
				CLEAR_BIT(hackSightseer.bsHacking,BS_IS_HACKING)
	
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [DO_SIGHTSEER_HACKING] SET biL_KilledWhileHack iHackingProg = ", iHackingProg, " iMyHackingMG = ", iMyHackingMG)
				iHackingProg = -1
			ENDIF
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
				//	IF iHackingProg <= 2
						//--2679608
						DISABLE_PLAYER_MISSION_VEHICLE_CONTROLS_THIS_FRAME()
				//	ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ELSE
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			RESET_NEW_HACKING_MINIGAME_DATA(hackSightseer)
				
			FORCE_QUIT_FAIL_HACKING_MINIGAME(hackSightseer)
			CLEAR_BIT(iBoolsBitSet,biL_KilledWhileHack)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [DO_SIGHTSEER_HACKING] CLEAR biL_KilledWhileHack iHackingProg = ", iHackingProg, " iMyHackingMG = ", iMyHackingMG)
		ENDIF
	ENDIF
	
	SWITCH iHackingProg
		
		
		CASE 0
			REQUEST_HACKING_MINI_GAME()
			REQUEST_ADDITIONAL_TEXT("HACK", MINIGAME_TEXT_SLOT)
			
			iHackingProg++
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [DO_SIGHTSEER_HACKING] iHackingProg = ", iHackingProg, " iMyHackingMG = ",iMyHackingMG)
		BREAK
		
		CASE 1
			IF HAVE_HACKING_ASSETS_LOADED()
			AND HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT)
				SET_BIT(hackSightseer.bsHacking,BS_IS_HACKING)
				SET_BIT(hackSightseer.iBS2Hacking, BS2_LAUNCH_MG_IMMEDIATIALY)
				
				Clear_Any_Objective_Text_From_This_Script()
				
				iHackSpeed = 10
			//	iMyHackingMG = HACKING_BRUTEFORCE //HACKING_NEW_MG
				IF iMyHackingMG = HACKING_NEW_MG
					hackSightseer.bTestHacking = TRUE // For new hacking minigame
				ELIF iMyHackingMG = HACKING_BRUTEFORCE
					hackSightseer.bTestHacking = FALSE
					iHackSpeed = 77
				ELIF iMyHackingMG = HACKING_IP_CONNECT
					iHackSpeed = 50
					hackSightseer.bTestHacking = FALSE
				ENDIF
				
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_ALLOW_PLAYER_DAMAGE | NSPC_CAN_BE_TARGETTED)
				
				iHackingProg++
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [DO_SIGHTSEER_HACKING] iHackingProg = ", iHackingProg)
			ELSE
				IF NOT HAVE_HACKING_ASSETS_LOADED()
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [DO_SIGHTSEER_HACKING] WAITING FOR REQUEST_HACKING_MINI_GAME ")
				ENDIF
				IF NOT HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [DO_SIGHTSEER_HACKING] WAITING FOR HAS_ADDITIONAL_TEXT_LOADED ")
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
			
			IF NOT GB_IS_GLOBAL_NON_BD_BIT0_SET(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_DPAD_DOWN_LB)
				GB_SET_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_DPAD_DOWN_LB)
			ENDIF
			
			IF iMyHackingMG = HACKING_IP_CONNECT
				RUN_HACKING_MINIGAME_WITH_PARAMETERS(hackSightseer, 
									 				DEFAULT,			// INT iStartLives = 5, 
													DEFAULT,			// INT iMaxLives = 5, 
													iHackSpeed,			// INT iStartingSpeed = 10, 
													DEFAULT,			// INT iSpeedDecayRate = 10, 
													DEFAULT,			// INT iBruteForceColumns = 8, 
													DEFAULT,			// INT iChanceOfRedHerrings = 10,
													DEFAULT,			// BOOL bDoTutorial = FALSE, 
													TRUE,				// BOOL bJustHackConnect = FALSE, 
													DEFAULT,			// BOOL bJustBruteForce = FALSE,  
													DEFAULT)			// BOOL bRandomPassword = TRUE, 
																		// BOOL bHackConnectBeforeBrute = TRUE, 
																		// BOOL bExitButton = FALSE, 
																		// BOOL bDoDownloadOpen = FALSE, 
																		// INT iTotalTime = 60000 
																		// #IF USE_TU_CHANGES , BOOL bMP = TRUE #ENDIF )
																	
																	
			

				IF HAS_PLAYER_BEAT_HACK_CONNECT(hackSightseer, TRUE)
				#IF IS_DEBUG_BUILD
				OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_DOWN)
				#ENDIF
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [DO_SIGHTSEER_HACKING] I COMPLETED IP CONNECT MG")
					bHackingComplete = TRUE
				ENDIF
			ELIF iMyHackingMG = HACKING_BRUTEFORCE
				RUN_HACKING_MINIGAME_WITH_PARAMETERS(hackSightseer, 
									 				DEFAULT,			// INT iStartLives = 5, 
													DEFAULT,			// INT iMaxLives = 5, 
													iHackSpeed,			// INT iStartingSpeed = 10, 
													DEFAULT,			// INT iSpeedDecayRate = 10, 
													DEFAULT,			// INT iBruteForceColumns = 8, 
													DEFAULT,			// INT iChanceOfRedHerrings = 10,
													DEFAULT,			// BOOL bDoTutorial = FALSE, 
													DEFAULT,			// BOOL bJustHackConnect = FALSE, 
													TRUE,				// BOOL bJustBruteForce = FALSE,  
													DEFAULT)			// BOOL bRandomPassword = TRUE, 
																		// BOOL bHackConnectBeforeBrute = TRUE, 
																		// BOOL bExitButton = FALSE, 
																		// BOOL bDoDownloadOpen = FALSE, 
																		// INT iTotalTime = 60000 
																		// #IF USE_TU_CHANGES , BOOL bMP = TRU
		
				IF HAS_PLAYER_BEAT_BRUTEFORCE(hackSightseer, TRUE)
				#IF IS_DEBUG_BUILD
				OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_DOWN)
				#ENDIF
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [DO_SIGHTSEER_HACKING] I COMPLETED BRUTE FORCE MG")
					bHackingComplete = TRUE
				ENDIF
			ELIF iMyHackingMG = HACKING_NEW_MG
				RUN_HACKING_MINIGAME_WITH_PARAMETERS(hackSightseer, 
									 				DEFAULT,			// INT iStartLives = 5, 
													DEFAULT,			// INT iMaxLives = 5, 
													iHackSpeed,			// INT iStartingSpeed = 10, 
													DEFAULT,			// INT iSpeedDecayRate = 10, 
													DEFAULT,			// INT iBruteForceColumns = 8, 
													DEFAULT,			// INT iChanceOfRedHerrings = 10,
													DEFAULT,			// BOOL bDoTutorial = FALSE, 
													DEFAULT,			// BOOL bJustHackConnect = FALSE, 
													DEFAULT,				// BOOL bJustBruteForce = FALSE,  
													DEFAULT)			// BOOL bRandomPassword = TRUE, 
																		// BOOL bHackConnectBeforeBrute = TRUE, 
																		// BOOL bExitButton = FALSE, 
																		// BOOL bDoDownloadOpen = FALSE, 
																		// INT iTotalTime = 60000 
																		// #IF USE_TU_CHANGES , BOOL bMP = TRU
																		
				IF HAS_PLAYER_BEAT_NEW_HACKING(hackSightseer, TRUE)
				#IF IS_DEBUG_BUILD
				OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_DOWN)
				#ENDIF
					bHackingComplete = TRUE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [DO_SIGHTSEER_HACKING] I COMPLETED NEW HACKING MG")
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(hackSightseer.iBS2Hacking, BS2_QUIT_MG_IMMEDIATIALY)
				CLEAR_BIT(hackSightseer.bsHacking,BS_IS_HACKING)
				
				RESET_NEW_HACKING_MINIGAME_DATA(hackSightseer)
				
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [DO_SIGHTSEER_HACKING] QUIT! iMyHackingMG = ",iMyHackingMG)
				
				IF GB_IS_GLOBAL_NON_BD_BIT0_SET(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_DPAD_DOWN_LB)
					GB_CLEAR_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_DPAD_DOWN_LB)
				ENDIF
				
				iHackingProg = -1
			ELIF bHackingComplete
				CLEAR_BIT(hackSightseer.bsHacking,BS_IS_HACKING)
				
				RESET_NEW_HACKING_MINIGAME_DATA(hackSightseer)
				playerBD[PARTICIPANT_ID_TO_INT()].iNumCompletedHackingGames++
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_JustCompHack)
				
				iMyHackingMG++
				IF iMyHackingMG >= MAX_NUM_HACKING_MG
					iMyHackingMG = HACKING_IP_CONNECT
				ENDIF
				
				IF GB_IS_GLOBAL_NON_BD_BIT0_SET(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_DPAD_DOWN_LB)
					GB_CLEAR_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_DPAD_DOWN_LB)
				ENDIF
				
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [DO_SIGHTSEER_HACKING] SUCCESS! iNumCompletedHackingGames = ", playerBD[PARTICIPANT_ID_TO_INT()].iNumCompletedHackingGames, " iMyHackingMG = ",iMyHackingMG)
				
				iHackingProg++

			
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

//Helper function to get the servers game/mission state
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC


///	PURPOSE: Need specific relationship groups for players taking part. Don't want AI to attack non-mission players
PROC SETUP_UW_REL_GROUPS()

ENDPROC




PROC REMOVE_UW_REL_GROUPS()
	REMOVE_RELATIONSHIP_GROUP(relUWAi)
	REMOVE_RELATIONSHIP_GROUP(relUWPlayer)
ENDPROC

//Cleanup Mission Data
PROC SCRIPT_CLEANUP()
	
	#IF IS_DEBUG_BUILD
	IF serverBD.bSomeoneSPassed
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	#ENDIF

	IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneEndTelemetry)
		IF NETWORK_IS_GAME_IN_PROGRESS()
			IF DID_MY_BOSS_LAUNCH_GB_SIGHTSEER()
			OR DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
				GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LEFT)
				SET_BIT(iBoolsBitSet, biL_DoneEndTelemetry)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  CLEANUP MISSION - TELEMETRY SETTING GB_TELEMETRY_END_LEFT AS NOTHING SET YET")
			ELSE
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  CLEANUP - DIDN'T SEND TELEMETRY AS DID_MY_BOSS_LAUNCH_GB_ASSAULT / DID_LOCAL_PLAYER_JOIN_AS_RIVAL")
			ENDIF
		ELSE
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  CLEANUP - NO TELEMETRY - NETWORK_IS_GAME_IN_PROGRESS")
		ENDIF
	ENDIF
	
	
	PROCESS_CURRENT_BOSS_WORK_PLAYSTATS(3, 3)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF PARTICIPANT_ID_TO_INT() = serverBD.iBossPartWhoLaunched
			IF iHackingProg >= 0
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  CLEANUP MISSION - ENDING HACKING")
				RESET_NEW_HACKING_MINIGAME_DATA(hackSightseer)
				
				FORCE_QUIT_FAIL_HACKING_MINIGAME(hackSightseer)
			ENDIF
		ENDIF
	ENDIF	

	
	IF GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()) = relUWPlayer
		SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), relMyFmGroup)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  CLEANUP MISSION - RESTORED MY FM REL GROUP     <----------     ") NET_NL()
	ENDIF
	
	IF iHackingProg > 0
		IF IS_BIT_SET(serverbd.iServerBitSet, biS_DurationExpired)
			//-- Turn player control back on if I was in the middle of hacking
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
	ENDIF
	
	REMOVE_UW_REL_GROUPS()
	
	IF serverBD.iBossPlayerWhoLaunched != -1
		IF IS_BIT_SET(iBoolsBitSet, biL_BlippedRival)
			IF NETWORK_IS_PLAYER_ACTIVE(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
				
				SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), RADAR_TRACE_TEMP_4, FALSE)
				SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), BLIP_COLOUR_RED, FALSE)
				FORCE_BLIP_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), FALSE, FALSE)
				SET_PLAYER_BLIP_AS_LONG_RANGE(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), FALSE) 
				SET_FIXED_BLIP_SCALE_FOR_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), g_sMPTunables.fgangboss_Job_blip_scale, FALSE)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  CLEANUP MISSION - biL_BlippedRival WAS SET, RESET BLIP")
			ENDIF
		ENDIF
	ENDIF
	
	IF GB_IS_GLOBAL_NON_BD_BIT0_SET(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_DPAD_DOWN_LB)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  CLEANUP MISSION - CLEAR eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_DPAD_DOWN_LB")
		GB_CLEAR_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_DPAD_DOWN_LB)
	ENDIF
	
	//-- Reset music
	IF IS_BIT_SET(iMusicBitset, biMusic_Init)
		IF NOT IS_BIT_SET(iMusicBitset, biMusic_Reset)
			SET_AUDIO_FLAG("DisableFlightMusic", FALSE) 
			SET_AUDIO_FLAG("WantedMusicDisabled", FALSE)
			
			TRIGGER_MUSIC_EVENT("BG_SIGHTSEER_STOP")
			
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  CLEANUP MISSION - SET biMusic_Reset")
		ENDIF
	ENDIF
	
	SET_BIT(iMusicBitset, biMusic_ShouldStop30s)
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  CLEANUP MISSION - SET biMusic_ShouldStop30s")
	STOP_30S_COUNTDOWN_AUDIO()
	SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(TRUE)
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  CLEANUP MISSION - SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(TRUE)")
	GB_SET_LOCAL_PLAYER_CRITICAL_TO_JOB(FALSE)
	
	GB_COMMON_BOSS_MISSION_CLEANUP()
	
	SET_MAX_WANTED_LEVEL(5)
	
	CLEAR_BIT(BitSet_CellphoneTU, g_BSTU_MP_SIGHTSEER_APP_HAS_LAUNCHED)
	CLEAR_BIT(BitSet_CellphoneTU, g_BSTU_ENABLE_MP_SIGHTSEER_APP)
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  CLEANUP MISSION      <----------     ") NET_NL()
	#IF IS_DEBUG_BUILD NET_LOG("SCRIPT_CLEANUP")	#ENDIF
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC





//PURPOSE: Returns the Cash reward for completing the objective
FUNC INT GET_CASH_REWARD()
	RETURN g_sMPTunables.iUrbanWarfareCashReward
ENDFUNC

//Do necessary pre game start ini.
PROC PROCESS_PRE_GAME( MP_MISSION_DATA missionScriptArgs )
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission) ,  missionScriptArgs)	//NUM_NETWORK_PLAYERS, missionScriptArgs)	//
	//NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, PARTICIPANT_ID_TO_INT())  
	
	//Reserve Entities
	//RESERVE_NETWORK_MISSION_PEDS(MAX_MW_PEDS)
	//RESERVE_NETWORK_MISSION_VEHICLES(MAX_MW_VEHS + 1)
	
	RESERVE_NETWORK_MISSION_OBJECTS(MAX_PACKAGES)
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		#IF IS_DEBUG_BUILD NET_LOG("SIGHTSEER -  FAILED TO RECEIVE INITIAL NETWORK BROADCAST")	#ENDIF
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  MISSION END - FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP D     <----------     ") NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		// This script will not be paused if another script calls PAUSE_GAME
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()

			
		ENDIF
		
	//	SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_SIGHTSEER, TRUE)
		
		SETUP_UW_REL_GROUPS()
		
		GB_COMMON_BOSS_MISSION_PREGAME()		
		
		playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_INI
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - TUNABLES...")
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - TUNABLES... g_sMPTunables.igb_sightseer_time_limit						= ", g_sMPTunables.igb_sightseer_time_limit)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - TUNABLES... g_sMPTunables.igb_sightseer_number_of_packages				= ", g_sMPTunables.igb_sightseer_number_of_packages)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - TUNABLES... g_sMPTunables.igb_sightseer_minimum_distance_to_next_package	= ", g_sMPTunables.igb_sightseer_minimum_distance_to_next_package)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - TUNABLES... g_sMPTunables.bgb_sightseer_disable_quadrant_northeast		= ", g_sMPTunables.bgb_sightseer_disable_quadrant_northeast)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - TUNABLES... g_sMPTunables.bgb_sightseer_disable_quadrant_northwest		= ", g_sMPTunables.bgb_sightseer_disable_quadrant_northwest)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - TUNABLES... g_sMPTunables.bgb_sightseer_disable_quadrant_southeast		= ", g_sMPTunables.bgb_sightseer_disable_quadrant_southeast)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - TUNABLES... g_sMPTunables.bgb_sightseer_disable_quadrant_southwest		= ", g_sMPTunables.bgb_sightseer_disable_quadrant_southwest)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER")
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  PRE_GAME DONE     <----------     ") NET_NL()
	ELSE
		#IF IS_DEBUG_BUILD NET_LOG("SIGHTSEER -  NETWORK_IS_GAME_IN_PROGRESS = FALSE")	#ENDIF
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  MISSION END - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP Q     <----------     ") NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
ENDPROC


PROC MOVE_TO_EXPLODE_VEHICLE_STAGE()

ENDPROC				

FUNC BOOL ARE_ALL_PEDS_CLEANED_UP()
	
	
	RETURN TRUE
ENDFUNC

FUNC BOOL MISSION_END_CHECK()

	

	
	RETURN FALSE
ENDFUNC

//Server only function that checks to see if the game mode should now end. For this test script, it if time has passed.
FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET()
	
	#IF IS_DEBUG_BUILD
		IF bHostEndMissionNow
			RETURN TRUE
		ENDIF
		
		IF ServerBD.bSomeoneSPassed
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [HAVE_MISSION_END_CONDITIONS_BEEN_MET] TRUE AS bSomeoneSPassed")
			RETURN TRUE
		ENDIF
		
	#ENDIF	
	
	IF MISSION_END_CHECK()
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllPlayersFinished)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC




//Helper function to get a clients game/mission state
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iGameState
ENDFUNC

#IF IS_DEBUG_BUILD
PROC CREATE_WIDGETS()
	INT iPlayer
	TEXT_LABEL_15 tl63

	START_WIDGET_GROUP("GB_SIGHTSEER")  
		
		START_WIDGET_GROUP("Hacking") 
			ADD_WIDGET_BOOL("Do hack", bWdDoHack)
			ADD_WIDGET_BOOL("App available?", bWdAppAvail)
			ADD_WIDGET_BOOL("App triggered?", bWdAppTrg)
			ADD_WIDGET_INT_READ_ONLY("iHackingProg", iHackingProg)
		STOP_WIDGET_GROUP()
		
		// Gameplay debug, sever only.
		START_WIDGET_GROUP("Server Only Gameplay") 

			ADD_WIDGET_BOOL("End mission", bHostEndMissionNow)			
		STOP_WIDGET_GROUP()		
		
		// Data about the server
		START_WIDGET_GROUP("Server BD") 
			ADD_WIDGET_INT_SLIDER("S. Game state", serverBD.iServerGameState,-1, HIGHEST_INT,1)

		STOP_WIDGET_GROUP()	

		// Data about the clients. * = You.
		START_WIDGET_GROUP("Client BD")  				
			REPEAT NUM_NETWORK_PLAYERS iPlayer
				tl63 = "Player "
				tl63 += iPlayer
				IF iPlayer = PARTICIPANT_ID_TO_INT()
					tl63 += "*"
				ENDIF
				START_WIDGET_GROUP(tl63)
					ADD_WIDGET_INT_SLIDER("Game state", playerBD[iPlayer].iGameState,-1, HIGHEST_INT,1)
				STOP_WIDGET_GROUP()
			ENDREPEAT				
		STOP_WIDGET_GROUP()	
		
	STOP_WIDGET_GROUP()
ENDPROC		

PROC UPDATE_WIDGETS()
	bWdAppAvail = IS_BIT_SET(BitSet_CellphoneTU, g_BSTU_ENABLE_MP_SIGHTSEER_APP)
	bWdAppTrg = IS_BIT_SET(BitSet_CellphoneTU, g_BSTU_MP_SIGHTSEER_APP_HAS_LAUNCHED)
	
	IF bWdDoHack
		DO_SIGHTSEER_HACKING()
	ENDIF
	
ENDPROC

PROC MAINTAIN_DEBUG_KEYS()
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPDebugBitset, PARTICIPANT_ID_TO_INT())
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S) 
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPDebugBitset, PARTICIPANT_ID_TO_INT())
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_DEBUG_KEYS] I S-PASSED ")
		ENDIF
	ENDIF
ENDPROC

#ENDIF

FUNC STRING GET_VEHICLE_TYPE_STRING()
	RETURN "" //GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(serverBD.VehModel)
ENDFUNC

FUNC STRING GET_VEH_BLIP_STRING()
//	IF serverBD.VehModel = RHINO
//		RETURN "ABLIP_TANK"			//"~BLIP_TEMP_6~"
//	ELIF IS_THIS_MODEL_A_PLANE(serverBD.VehModel)
//		RETURN "ABLIP_PLANE"		//"~BLIP_TEMP_6~"
//	ENDIF
//	
//	RETURN "ABLIP_HELI"				//"~BLIP_TEMP_6~"*/
	RETURN ""
ENDFUNC

//PURPOSE: Adds a blip to the Veh
PROC ADD_VEH_BLIP()
	IF NOT DOES_BLIP_EXIST(VehBlip)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
			IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh))
				VehBlip = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(serverBD.niVeh))
				SET_BLIP_SPRITE(VehBlip, RADAR_TRACE_TEMP_1) 
			//	SET_BLIP_COLOUR(VehBlip, BLIP_COLOUR_PURPLE)
				SET_BLIP_COLOUR_FROM_HUD_COLOUR(VehBlip, HUD_COLOUR_NET_PLAYER2)
				//SHOW_HEIGHT_ON_BLIP(VehBlip, TRUE)
				SET_BLIP_FLASH_TIMER(VehBlip, 7000)
				SET_BLIP_PRIORITY(VehBlip, BLIPPRIORITY_HIGHEST)
				SET_BLIP_NAME_FROM_TEXT_FILE(VehBlip, "UW_BLIP")	//SIGHTSEER
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  VEH BLIP ADDED ") NET_NL()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_VEHICLE_BLIP()
	IF DOES_BLIP_EXIST(VehBlip)
		REMOVE_BLIP(VehBlip)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  VEHICLE BLIP REMOVED    <----------     ") NET_NL()
	ENDIF
ENDPROC



//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  ENTITY CREATION                  /////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////
///    			VEHICLE   				///
///////////////////////////////////////////
//PURPOSE: Create the Veh
FUNC BOOL CREATE_VEH()

	
	RETURN TRUE
ENDFUNC

//PURPOSE: Creates the Veh and Driver that will deliver the Vehicle
FUNC BOOL CREATE_MAIN_VEHICLE()

	
	RETURN FALSE
ENDFUNC




//PURPOSE: Runs through the max num participents and does relevant checks
PROC MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
	INT iTempTotalPackCollected = 0
	BOOL bSomeoneNotFinished = FALSE
	//For now we only need to do this check once the Veh is destroyed.
	IF serverBD.iServerGameState != GAME_STATE_END
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iStaggeredParticipant
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iStaggeredParticipant))
				PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iStaggeredParticipant))
				//PED_INDEX PlayerPedId = GET_PLAYER_PED(PlayerId)
				
				//** CHECKS THAT DON'T NEED THE PLAYER TO BE ALIVE **//
				
				
				//** CHECKS THAT REQUIRE THE PLAYER TO BE ALIVE **//
				IF IS_NET_PLAYER_OK(PlayerId)
					

				ENDIF
				
//				//-- Collected package?
//				IF NOT IS_BIT_SET(serverBD.iServerBitset, biS_SHouldDeletePackage)
//					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
//						IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_GotThePackage)
//							SET_BIT(serverBD.iServerBitset, biS_SHouldDeletePackage)
//							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SERVER THINKS THIS PLAYER HAS THE PACKAGE ", GET_PLAYER_NAME(PlayerId))
//						ENDIF
//					ENDIF
//				ENDIF
				
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AllPackagesCollected)
					IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_GotThePackage)
						IF serverBD.iLastPlayerToCollect != NATIVE_TO_INT(PlayerId)
							serverBD.iLastPlayerToCollect = NATIVE_TO_INT(PlayerId)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] serverBD.iLastPlayerToCollect UPDATED TO ", serverBD.iLastPlayerToCollect, " WHO IS PLAYER ", GET_PLAYER_NAME(PlayerId))
						ENDIF
					ENDIF
				ENDIF
				
				//-- Total collected
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AllPackagesCollected)
					IF playerBD[iStaggeredParticipant].iPackagesCollected > 0
						iTempTotalPackCollected += playerBD[iStaggeredParticipant].iPackagesCollected
					ENDIF
				ENDIF
				
				//-- Boss killed?
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_BossKilled)
					IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_KilledBoss)
						SET_BIT(serverBD.iServerBitSet, biS_BossKilled)
						serverBD.iPlayerKilledBoss = NATIVE_TO_INT(PlayerId)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SERVER THINKS THIS PLAYER KILLED THE BOSS ", GET_PLAYER_NAME(PlayerId), " serverBD.iPlayerKilledBoss = ", serverBD.iPlayerKilledBoss)
					ENDIF
				ENDIF
				
				//-- Everyone finished?
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AllPlayersFinished)
					IF NOT bSomeoneNotFinished
						IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllPackagesCollected)
						OR IS_BIT_SET(serverBD.iServerBitSet, biS_DurationExpired)
						OR IS_BIT_SET(serverBD.iServerBitSet, biS_BossKilled)
						OR IS_BIT_SET(serverBD.iServerBitSet, biS_BossLaunchedQuit)
							IF NOT IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_Finished)
								bSomeoneNotFinished = TRUE
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SERVER THINKS THIS PLAYER NOT FINISHED ", GET_PLAYER_NAME(PlayerId))
							ENDIF
						ELSE
							bSomeoneNotFinished = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				#IF IS_DEBUG_BUILD
				//-- Debug s-skip
				IF NOT ServerBD.bSomeoneSPassed
					IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPDebugBitset, biPDebug_PressedS)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SERVER THINKS THIS PLAYER S-PASSED ", GET_PLAYER_NAME(PlayerId))
						ServerBD.bSomeoneSPassed = TRUE
					ENDIF
				ENDIF
				#ENDIF
			ELSE
				//-- Part not active
				
				//-- CHeck for Boss who launched the job quiting the session
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_BossLaunchedQuit)
					IF serverBD.iBossPartWhoLaunched > -1
						IF iStaggeredParticipant = serverBD.iBossPartWhoLaunched
							SET_BIT(serverBD.iServerBitSet, biS_BossLaunchedQuit)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SERVER THINKS BOSS WHO LAUNCHED WORK QUIT SESSION erverBD.iBossPartWhoLaunched = ", serverBD.iBossPartWhoLaunched)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AllPackagesCollected)
			
			
			iTempTotalPackCollected += serverBD.iMissingPackages
			
			IF iTempTotalPackCollected >= GET_GB_SIGHTSEER_TOTAL_NUM_PACKAGES()
				SET_BIT(serverBD.iServerBitSet, biS_AllPackagesCollected)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SET biS_AllPackagesCollected AS iTempTotalPackCollected = ", iTempTotalPackCollected, " MAX_PACKAGES = ", MAX_PACKAGES)
			ENDIF
			
			IF serverBD.iPackagesCollected != iTempTotalPackCollected
				IF serverBD.iPackagesCollected < iTempTotalPackCollected
					serverBD.iPackagesCollected = iTempTotalPackCollected
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] iPackagesCollected UPDATED TO ", serverBD.iPackagesCollected)
				ELSE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] WANT TO UPDATE iPackagesCollected BUT serverBD.iPackagesCollected = ", serverBD.iPackagesCollected, " > iTempTotalPackCollected = ", iTempTotalPackCollected)
					IF serverBD.iMissingPackages != (serverBD.iPackagesCollected - iTempTotalPackCollected)
						serverBD.iMissingPackages = (serverBD.iPackagesCollected - iTempTotalPackCollected)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] WANT TO UPDATE serverBD.iMissingPackages UPDATED TO ", serverBD.iMissingPackages)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		
		//--All finished?
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AllPlayersFinished)
			IF NOT bSomeoneNotFinished
				SET_BIT(serverBD.iServerBitSet, biS_AllPlayersFinished)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SET biS_AllPlayersFinished")
			ENDIF
		ENDIF
		
	ENDIF
ENDPROC




PROC STOP_PLAYER_LEAVING_VEHICLE_THIS_FRAME()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
	ENDIF
ENDPROC






//PURPOSE: Checks to see if this is the end and the player should be rewarded.
PROC CHECK_FOR_REWARD()
//	BOOL bEventCompleted
	GANG_BOSS_MANAGE_REWARDS_DATA gbRewards
	PLAYER_INDEX playerPackage = INVALID_PLAYER_INDEX()
	PLAYER_INDEX playerKiller = INVALID_PLAYER_INDEX()
	PLAYER_INDEX playerMyBoss = INVALID_PLAYER_INDEX()
	STRING sOrganization
	HUD_COLOURS hclPlayer
	
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Finished)
		IF NOT IS_BIT_SET(iBoolsBitSet, biL_WaitForShard)
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllPackagesCollected)
				//-- All packages collected
				IF PARTICIPANT_ID_TO_INT() = serverBD.iBossPartWhoLaunched
				OR GET_MY_BOSS_PARTICPANT_ID() = serverBD.iBossPartWhoLaunched
					//-- I'm the boss who launched event
					
					IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_MINIMAL
						sOrganization = GB_GET_ORGANIZATION_NAME_AS_A_STRING()
						hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID())
						SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "GB_WINNER", "BIGM_SGHTYD", sOrganization, hclPlayer) 
					//	SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "GB_WINNER", "BIGM_SGHTYD") //Your Organization collected all the packages
					ENDIF
					
					IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneEndTelemetry)
						GB_SET_COMMON_TELEMETRY_DATA_ON_END(TRUE, GB_TELEMETRY_END_WON)
						SET_BIT(iBoolsBitSet, biL_DoneEndTelemetry)
					ENDIF
					GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_SIGHTSEER, TRUE, gbRewards)
					CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [CHECK_FOR_REWARD] I LAUNCHED AND I COLLECTED ALL PACKAGES")
	//			ELIF GET_MY_BOSS_PARTICPANT_ID() = serverBD.iBossPartWhoLaunched
					//-- My boss launched
	//				playerPackage = INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched)
	//				IF playerPackage != INVALID_PLAYER_INDEX()
	//					IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_MINIMAL
	//						SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, playerPackage, DEFAULT, "BIGM_SGHTWD", "GB_WINNER") // ~a~ collected all the packages
	//					ENDIF
	//					
	//					GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_SIGHTSEER, TRUE, gbRewards)
	//					CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
	//					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [CHECK_FOR_REWARD] MY BOSS LAUNCHED AND THEY COLLECTED ALL PACKAGES")
	//				ELSE
	//					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [CHECK_FOR_REWARD] MY BOSS LAUNCHED AND THEY COLLECTED ALL PACKAGES BUT DON'T KNOW WHO THEY ARE!")
	//				ENDIF
				ELIF DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
					//-- I was on a rival gang
					IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
						IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_MINIMAL
							sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
							hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
							SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_SGHTFD3", sOrganization, hclPlayer) 
					
						//	SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_SGHTFD3") // The rival boss collected all the packages
						ENDIF
						
						IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneEndTelemetry)
							GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST)
							SET_BIT(iBoolsBitSet, biL_DoneEndTelemetry)
						ENDIF
						GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_SIGHTSEER, FALSE, gbRewards)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [CHECK_FOR_REWARD] I WAS A RIVAL AND ALL PACKAGES WERE COLLECTED")
					ELSE
						IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_MINIMAL
						//	SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_SGHTFD5") // The rival boss collected all the packages
							sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
							hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
							SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_SGHTFD5", sOrganization, hclPlayer) 
					
						ENDIF
						
						IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneEndTelemetry)
							GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST)
							SET_BIT(iBoolsBitSet, biL_DoneEndTelemetry)
						ENDIF
						GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_SIGHTSEER, FALSE, gbRewards)
						
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [CHECK_FOR_REWARD] I'M NOT IN A GANG AND ALL PACKAGES WERE COLLECTED")
					ENDIF
					
				ENDIF
				
				
				SET_BIT(iBoolsBitSet, biL_WaitForShard)	
			//	bEventCompleted = TRUE
			ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_DurationExpired)
				//-- Mission duration expired
				
	//			IF PARTICIPANT_ID_TO_INT() = serverBD.iBossPartWhoLaunched
	//				//-- I'm the boss who launched event
	//				IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_MINIMAL
	//					sOrganization = GB_GET_ORGANIZATION_NAME_AS_A_STRING()
	//					hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID())
	//					SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_SGHTFD", sOrganization, hclPlayer) 
	//				ENDIF
	//				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [CHECK_FOR_REWARD] I LAUNCHED AND DURATION EXPIRED")
	//			ELIF GET_MY_BOSS_PARTICPANT_ID() = serverBD.iBossPartWhoLaunched
	//				//-- My boss launched
	//				
	//				playerPackage = INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched)
	//				IF playerPackage != INVALID_PLAYER_INDEX()
	//					IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_MINIMAL
	//						sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
	//						hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
	//						SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_SGHTFD2", sOrganization, hclPlayer) 
	//				
	//					ENDIF
	//					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [CHECK_FOR_REWARD]  MY BOSS LAUNCHED AND DURATION EXPIRED")
	//				ELSE
	//					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [CHECK_FOR_REWARD]  MY BOSS LAUNCHED AND DURATION EXPIRED BUT DON'T KNOW WHO THEY ARE!")
	//				ENDIF
	//				
	//				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [CHECK_FOR_REWARD] MY BOSS LAUNCHED AND DURATION EXPIRED")
	//				
	//			ELIF DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
	//				//-- I was on a rival gang
	//				IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
	//					IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_MINIMAL		
	//						sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
	//						hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
	//						SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_SGHTFD4", sOrganization, hclPlayer) 
	//					ENDIF
	//					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [CHECK_FOR_REWARD] I WAS A RIVAL AND DURATION EXPIRED")
	//				ELSE
	//					IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_MINIMAL		
	//						sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
	//						hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
	//						SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_SGHTFD6", sOrganization, hclPlayer) 
	//					ENDIF
	//					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [CHECK_FOR_REWARD] I'M NOT A GANG MEMBER AND DURATION EXPIRED")
	//				ENDIF
	//			ENDIF
				
				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_SGHTFT")
				
				IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneEndTelemetry)
					GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_TIME_OUT)
					SET_BIT(iBoolsBitSet, biL_DoneEndTelemetry)
				ENDIF
						
				GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_SIGHTSEER, FALSE, gbRewards)
				SET_BIT(iBoolsBitSet, biL_WaitForShard)
			//	bEventCompleted = TRUE
			ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_BossKilled)
				IF serverBD.iPlayerKilledBoss > -1
					playerKiller = INT_TO_PLAYERINDEX(serverBD.iPlayerKilledBoss)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [CHECK_FOR_REWARD] biS_BossKilled, think this player killed the boss ", GET_PLAYER_NAME(playerKiller))
				ELSE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [CHECK_FOR_REWARD] biS_BossKilled but serverBD.iPlayerKilledBoss = ", serverBD.iPlayerKilledBoss)
				ENDIF
				
				//-- Boss killed
				IF PARTICIPANT_ID_TO_INT() = serverBD.iBossPartWhoLaunched
					//-- I'm the boss who launched event
					IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_MINIMAL
						IF NETWORK_IS_PLAYER_ACTIVE(playerKiller)
						//	sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
						//	hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
							IF GB_IS_PLAYER_MEMBER_OF_A_GANG(playerKiller)
								sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(playerKiller)
								hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(playerKiller)
							ELSE
								sOrganization = GET_PLAYER_NAME(playerKiller)
								hclPlayer = HUD_COLOUR_WHITE
							ENDIF
										
							SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_SGHTFMK", sOrganization, hclPlayer) // You were killed by <C>~a~<C>.
						ELSE
							SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_SGHTFD2", sOrganization, hclPlayer) 
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [CHECK_FOR_REWARD] I LAUNCHED AND I WAS KILLED BY RIVAL GANG BUT DON'T KNOW WHO KILLED ME")
						ENDIF
					ENDIF
					
					GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_SIGHTSEER, FALSE, gbRewards)
					
					IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneEndTelemetry)
						GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST)
						SET_BIT(iBoolsBitSet, biL_DoneEndTelemetry)
					ENDIF
				
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [CHECK_FOR_REWARD] I LAUNCHED AND I WAS KILLED BY RIVAL GANG")
				ELIF GET_MY_BOSS_PARTICPANT_ID() = serverBD.iBossPartWhoLaunched
					//-- My boss launched
					playerPackage = INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched)
					IF playerPackage != INVALID_PLAYER_INDEX()
						IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_MINIMAL
						//	sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
						//	hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
							
							IF NETWORK_IS_PLAYER_ACTIVE(playerKiller)
								IF GB_IS_PLAYER_MEMBER_OF_A_GANG(playerKiller)
									sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(playerKiller)
									hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(playerKiller)
								ELSE
									sOrganization = GET_PLAYER_NAME(playerKiller)
									hclPlayer = HUD_COLOUR_WHITE
								ENDIF
								
								SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_SGHTGK", sOrganization, hclPlayer) // killed your Boss
								
							ELSE
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [CHECK_FOR_REWARD] MY BOSS LAUNCHED AND WAS KILLED BUT DON'T KNOW KILLER!")
								SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_SGHTFD2", sOrganization, hclPlayer) 
							ENDIF
						ENDIF
						GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_SIGHTSEER, FALSE, gbRewards)
						
						IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneEndTelemetry)
							GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST)
							SET_BIT(iBoolsBitSet, biL_DoneEndTelemetry)
						ENDIF
					
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [CHECK_FOR_REWARD] MY BOSS LAUNCHED AND WAS KILLED BY RIVAL GANG")
					ELSE
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [CHECK_FOR_REWARD] MY BOSS LAUNCHED AND THEY WERE KILLED BY RIVAL GANG BUT DON'T KNOW WHO THEY ARE!")
					ENDIF
				ELIF DID_LOCAL_PLAYER_JOIN_AS_RIVAL() 
					//-- I was on a rival gang / not in a gang

					playerKiller = INT_TO_PLAYERINDEX(serverBD.iPlayerKilledBoss)
					IF playerKiller != INVALID_PLAYER_INDEX()
						//-- I killed the rival boss
						IF playerKiller = PLAYER_ID()
							IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_MINIMAL
								IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
									SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "GB_WINNER", "BIGM_SGHTYK") //You killed the rival boss
								ELSE
									SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "GB_WINNER", "BIGM_SGHTYK2") //You killed the boss
								ENDIF
							ENDIF
							CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
							
							IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneEndTelemetry)
								GB_SET_COMMON_TELEMETRY_DATA_ON_END(TRUE, GB_TELEMETRY_END_WON)
								SET_BIT(iBoolsBitSet, biL_DoneEndTelemetry)
							ENDIF
					
							GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_SIGHTSEER, TRUE, gbRewards)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [CHECK_FOR_REWARD] I KILLED BOSS COLLECTING THE PACKAGES")
						ELSE
							IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
								 playerMyBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
								 IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerKiller, playerMyBoss)
									//-- someone on my gang killed the rival boss
									IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_MINIMAL
										SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, playerKiller, DEFAULT, "BIGM_SGHTWK", "GB_WINNER") //  ~a~ killed the rival boss
									ENDIF
									
									IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneEndTelemetry)
										GB_SET_COMMON_TELEMETRY_DATA_ON_END(TRUE, GB_TELEMETRY_END_WON)
										SET_BIT(iBoolsBitSet, biL_DoneEndTelemetry)
									ENDIF
									
									GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_SIGHTSEER, FALSE, gbRewards)
								
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [CHECK_FOR_REWARD] THE PLAYER WHO KILLED THE BOSS IS IN MY GANG PLAYER KILLER ", GET_PLAYER_NAME(playerKiller))
								ELSE
									//-- The boss was killed by a player not in my gang
									IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_MINIMAL
										IF NETWORK_IS_PLAYER_ACTIVE(playerKiller)
											IF GB_IS_PLAYER_MEMBER_OF_A_GANG(playerKiller)
												sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(playerKiller)
												hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(playerKiller)
											ELSE
												sOrganization = GET_PLAYER_NAME(playerKiller)
												hclPlayer = HUD_COLOUR_WHITE
											ENDIF
											
											SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_SGHTWK", sOrganization, hclPlayer) // killed the rival Boss
								
										ELSE
											CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [CHECK_FOR_REWARD] THE PLAYER WHO KILLED THE BOSS IS IN A RIVAL GANG BUT playerKiller NOT ACTIVE!")
											SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_SGHTRK") //The boss was killed by a rival organisation
										ENDIF
									ENDIF
									
									IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneEndTelemetry)
										GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST)
										SET_BIT(iBoolsBitSet, biL_DoneEndTelemetry)
									ENDIF
							
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [CHECK_FOR_REWARD] THE PLAYER WHO KILLED THE BOSS IS IN A RIVAL GANG PLAYER KILLER ", GET_PLAYER_NAME(playerKiller))
									GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_SIGHTSEER, FALSE, gbRewards)
								ENDIF
							ELSE
								//-- I'm not in a gang
								IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_MINIMAL
									IF NETWORK_IS_PLAYER_ACTIVE(playerKiller)
										IF GB_IS_PLAYER_MEMBER_OF_A_GANG(playerKiller)
											sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(playerKiller)
											hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(playerKiller)
										ELSE
											sOrganization = GET_PLAYER_NAME(playerKiller)
											hclPlayer = HUD_COLOUR_WHITE
										ENDIF
										
										SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_SGHTNGK", sOrganization, hclPlayer) // killed the Boss 
								
									ELSE
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [CHECK_FOR_REWARD] THE BOSS WAS KILLED BY SOMEONE ONTHER THAN ME AND I'M NOT IN A GANG BUT DON'T KNOW WHO KILLED THEM!")
										SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_SGHTRK") //The boss was killed by a rival organisation
									ENDIF
								ENDIF
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [CHECK_FOR_REWARD] THE BOSS WAS KILLED BY SOMEONE ONTHER THAN ME AND I'M NOT IN A GANG PLAYER KILLER ", GET_PLAYER_NAME(playerKiller))
								
								IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneEndTelemetry)
									GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST)
									SET_BIT(iBoolsBitSet, biL_DoneEndTelemetry)
								ENDIF
								GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_SIGHTSEER, FALSE, gbRewards)
							ENDIF
						ENDIF
					ELSE
						//-- Don't know who killed rival boss
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [CHECK_FOR_REWARD] MY BOSS LAUNCHED AND THEY COLLECTED ALL PACKAGES BUT DON'T KNOW WHO THEY ARE!")
					ENDIF
				ENDIF
				
				SET_BIT(iBoolsBitSet, biL_WaitForShard)
			//	bEventCompleted = TRUE
			ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_BossLaunchedQuit)
				//-- Boss who launched quit
				
				IF SHOULD_LOCAL_PLAYER_SEE_GB_SIGHTSEER_HUD()
					IF DID_MY_BOSS_LAUNCH_GB_SIGHTSEER()
						IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_MINIMAL
						//	SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_SGHTBQ") // Your Boss quit the session
						ENDIF
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [CHECK_FOR_REWARD] biS_BossLaunchedQuit AND MY BOSS QUIT")
					ELSE
						IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_MINIMAL
							SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_SGHTRBQ") // The rival Boss quit the session
						ENDIF
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [CHECK_FOR_REWARD] biS_BossLaunchedQuit AND RIVAL BOSS")
					ENDIF
				ELSE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [CHECK_FOR_REWARD] biS_BossLaunchedQuit BUT NOT SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD")
				ENDIF
				
				IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneEndTelemetry)
					GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_BOSS_LEFT)
					SET_BIT(iBoolsBitSet, biL_DoneEndTelemetry)
				ENDIF
								
				GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_ASSAULT, FALSE, gbRewards)
				
				SET_BIT(iBoolsBitSet, biL_WaitForShard)
			//	bEventCompleted = TRUE
			
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(iBoolsBitSet, biL_WaitForShard)
			Clear_Any_Objective_Text_From_This_Script()
			
			IF NOT IS_BIT_SET(iMusicBitset, biMusic_ShouldStop30s)
				SET_BIT(iMusicBitset, biMusic_ShouldStop30s)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [CHECK_FOR_REWARD] SET biL_ShouldStop30s ")
			ENDIF
			
			IF NOT IS_BIT_SET(iBoolsBitSet, biL_ResetBlipForShard)
				IF serverBD.iBossPlayerWhoLaunched != -1
					IF IS_BIT_SET(iBoolsBitSet, biL_BlippedRival)
						IF NETWORK_IS_PLAYER_ACTIVE(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
							
							SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), RADAR_TRACE_TEMP_4, FALSE)
							SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), BLIP_COLOUR_RED, FALSE)
							FORCE_BLIP_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), FALSE, FALSE)
							SET_PLAYER_BLIP_AS_LONG_RANGE(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), FALSE) 
							SET_FIXED_BLIP_SCALE_FOR_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), g_sMPTunables.fgangboss_Job_blip_scale, FALSE)
							SET_BIT (iBoolsBitSet, biL_ResetBlipForShard)
							
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  [CHECK_FOR_REWARD] - biL_BlippedRival WAS SET, RESET BLIP")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
	
			IF GB_MAINTAIN_BOSS_END_UI(gbBossEndUi)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [CHECK_FOR_REWARD] SET biP_Finished ")
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Finished)
			#IF IS_DEBUG_BUILD
			ELSE
				IF GET_FRAME_COUNT() % 100 = 0
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [CHECK_FOR_REWARD] WAITING FOR GB_MAINTAIN_BOSS_END_UI ")
				ENDIF
			#ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Controls the ped blips displaying for when they shoot
PROC CONTROL_PED_LOOP()
	
ENDPROC





/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MISSION PROCS                  //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE:
///    Start a countdown when at least one player is in the vehicle. Mission will automatically start when timer ends
PROC MAINTAIN_START_MISSION_TIMER_SERVER()
//	BOOL bTimerShouldStart
//	
//	IF GET_ENTITY_MODEL(NET_TO_VEH(serverbd.niveh)) = VALKYRIE
//		bTimerShouldStart = IS_BIT_SET(serverbd.iServerBitSet, biS_AtLeastTwoPlayersInVeh)
//	ELSE
//		bTimerShouldStart = IS_BIT_SET(serverbd.iServerBitSet, biS_AtLeastOnePlayerInVeh)
//	ENDIF
//	
//	IF bTimerShouldStart //IS_BIT_SET(serverbd.iServerBitSet, biS_AtLeastOnePlayerInVeh)
//		IF NOT HAS_NET_TIMER_STARTED(serverbd.timeMissionStart)
//			START_NET_TIMER(serverbd.timeMissionStart)
//			NET_PRINT_TIME() NET_PRINT("     ---------->     SIGHTSEER -  START timeMissionStart    <----------     ") NET_NL()
//		ENDIF
//	ELSE
//		IF HAS_NET_TIMER_STARTED(serverbd.timeMissionStart)
//			RESET_NET_TIMER(serverbd.timeMissionStart)
//			NET_PRINT_TIME() NET_PRINT("     ---------->     SIGHTSEER -  RESET timeMissionStart    <----------     ") NET_NL()
//		ENDIF
//	ENDIF
ENDPROC


PROC MAINTAIN_START_MISSION_TIMER_CLIENT()
	
//	BOOL bTimerShouldStart
//	
//	IF GET_ENTITY_MODEL(NET_TO_VEH(serverbd.niveh)) = VALKYRIE
//		bTimerShouldStart = IS_BIT_SET(serverbd.iServerBitSet, biS_AtLeastTwoPlayersInVeh)
//	ELSE
//		bTimerShouldStart = IS_BIT_SET(serverbd.iServerBitSet, biS_AtLeastOnePlayerInVeh)
//	ENDIF
//	
//	IF bTimerShouldStart //IS_BIT_SET(serverbd.iServerBitSet, biS_AtLeastOnePlayerInVeh)
//		IF (TIME_MISSION_START-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeMissionStart)) >= 0
//			SET_MIDDLE_TITLE_POSITION_HUD_THIS_FRAME()
//			DRAW_GENERIC_TIMER((TIME_MISSION_START-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeMissionStart)), "UW_WAIT", 0, TIMER_STYLE_USEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_DONTCARE, DEFAULT, HUD_COLOUR_WHITE)
//		ELSE
//			SET_MIDDLE_TITLE_POSITION_HUD_THIS_FRAME()
//			DRAW_GENERIC_TIMER(0, "UW_WAIT", 0, TIMER_STYLE_USEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_DONTCARE, DEFAULT, HUD_COLOUR_WHITE)
//			
//			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
//				SET_BIT(serverbd.iServerBitSet, biS_MissionStartExpired)
//				NET_PRINT_TIME() NET_PRINT("     ---------->     SIGHTSEER -  MISSION START TIME EXPIRED    <----------     ") NET_NL()
//			ENDIF
//		ENDIF
//	ENDIF		
ENDPROC

PROC MAINTAIN_UW_MUSIC()
	
//	IF NOT IS_BIT_SET(iBoolsBitSet, biDoneMusic)
//		IF serverBD.eStage = eAD_ATTACK
//			IF TRIGGER_MUSIC_EVENT("MP_MC_ACTION")
//				SET_BIT(iBoolsBitSet, biDoneMusic)
//				NET_PRINT_TIME() NET_PRINT("     ---------->     SIGHTSEER -  MAINTAIN_UW_MUSIC - MP_MC_ACTION    <----------     ") NET_NL()
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	IF NOT IS_BIT_SET(iBoolsBitSet, biResetMusic)
//		IF IS_BIT_SET(iBoolsBitSet, biDoneMusic)
//			IF serverBD.eStage > eAD_ATTACK
//				IF IS_PED_INJURED(PLAYER_PED_ID())
//					IF TRIGGER_MUSIC_EVENT("MP_MC_FAIL")
//						SET_BIT(iBoolsBitSet, biResetMusic)						
//						NET_PRINT_TIME() NET_PRINT("     ---------->     SIGHTSEER -  MAINTAIN_UW_MUSIC - MP_MC_FAIL    <----------     ") NET_NL()
//					ENDIF
//				ELSE
//					IF TRIGGER_MUSIC_EVENT("MP_MC_STOP")
//						SET_BIT(iBoolsBitSet, biResetMusic)						
//						NET_PRINT_TIME() NET_PRINT("     ---------->     SIGHTSEER -  MAINTAIN_UW_MUSIC - MP_MC_STOP    <----------     ") NET_NL()
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
ENDPROC

/// PURPOSE:
///    Want to force rivals onto job after 10 minutes or so, to prevent launching gang holding up boss work launching
PROC MAINTAIN_FORCE_RIVALS_ON_MISSION_SERVER()
	IF serverBD.iPackagesCollected = 0
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ForceRivalsOnMission)
			IF NOT HAS_NET_TIMER_STARTED(serverBD.timeForceRivalsJoin)
				START_NET_TIMER(serverBD.timeForceRivalsJoin)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_FORCE_RIVALS_ON_MISSION_SERVER] STARTED timeForceRivalsJoin") 
			ELSE
				IF HAS_NET_TIMER_EXPIRED(serverBD.timeForceRivalsJoin, GET_GB_SIGHTSEER_FORCE_RIVALS_JOIN_TIME())
					SET_BIT(serverBD.iServerBitSet, biS_ForceRivalsOnMission)			
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_FORCE_RIVALS_ON_MISSION_SERVER] SET biS_ForceRivalsOnMission")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handle the mission duration (server side)
PROC MAINTAIN_MISSION_DURATION_SERVER()
	IF NOT HAS_NET_TIMER_STARTED(serverBD.timeDuration)
		START_NET_TIMER(serverBD.timeDuration)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] [MAINTAIN_MISSION_DURATION_SERVER] STARTED timeDuration")
	ELSE
		IF HAS_NET_TIMER_EXPIRED(serverBD.timeDuration, GET_GB_SIGHTSEER_DURATION())
			SET_BIT(serverBD.iServerBitSet, biS_DurationExpired)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] [MAINTAIN_MISSION_DURATION_SERVER] SET biS_DurationExpired")
		ENDIF
	ENDIF
				
ENDPROC



PROC DRAW_KILLS_BAR()
	
//	DRAW_GENERIC_BIG_DOUBLE_NUMBER((serverBD.iKillGoal-serverBD.iTotalKills), serverBD.iKillGoal, "GHO_KILLB")
ENDPROC

PROC MAINTAIN_BOTTOM_RIGHT_HUD()
	IF NOT DID_MY_BOSS_LAUNCH_GB_SIGHTSEER()
	AND NOT DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
		EXIT
	ENDIF
	
	IF IS_BIT_SET(hackSightseer.bsHacking,BS_IS_HACKING)
		EXIT
	ENDIF
	
	IF NOT SHOULD_LOCAL_PLAYER_SEE_GB_SIGHTSEER_HUD()
		EXIT
	ENDIF
	
	GB_UI_LEVEL myUiLevel = GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())
	
	IF myUiLevel < GB_UI_LEVEL_FULL	
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [MAINTAIN_BOTTOM_RIGHT_HUD] EXIT AS myUiLevel = ", ENUM_TO_INT(myUiLevel))
		EXIT
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(serverBD.timeDuration)
		INT iTimeLeft = GET_GB_SIGHTSEER_DURATION()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeDuration)
		INT iPackagesRemaining = GET_GB_SIGHTSEER_TOTAL_NUM_PACKAGES() - serverBD.iPackagesCollected
		
		MAINTAIN_30S_COUNTDOWN_MUSIC(iTimeLeft)
		
		IF iTimeLeft > 30000
			BOTTOM_RIGHT_UI_SCORE_TIMER(iPackagesRemaining, "GB_SGHT_HUD", iTimeLeft, DEFAULT, "GB_WORK_END")
		ELSE
			IF iTimeLeft > 0	
				BOTTOM_RIGHT_UI_SCORE_TIMER(iPackagesRemaining, "GB_SGHT_HUD", iTimeLeft, HUD_COLOUR_RED, "GB_WORK_END")
			ELSE
				BOTTOM_RIGHT_UI_SCORE_TIMER(iPackagesRemaining, "GB_SGHT_HUD", 0, HUD_COLOUR_RED, "GB_WORK_END")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC ADD_BLIP_FOR_PACKAGE()
	GB_UI_LEVEL myUiLevel = GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())
	IF myUiLevel < GB_UI_LEVEL_MINIMAL
		IF DOES_BLIP_EXIST(blipPackage)
			REMOVE_BLIP(blipPackage)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [ADD_BLIP_FOR_PACKAGE] REMOVING BLIP AS myUiLevel = ", ENUM_TO_INT(myUiLevel))
		ENDIF
		EXIT
	ENDIF
	
	IF DOES_BLIP_EXIST(blipPackage)
//		IF NOT IS_BIT_SET(iBoolsBitSet, biL_SetBlipHeight)
//			SHOW_HEIGHT_ON_BLIP(blipPackage, TRUE)
//			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [ADD_BLIP_FOR_PACKAGE] SET blipPackage")
//			SET_BIT(iBoolsBitSet, biL_SetBlipHeight)
//		ENDIF
		EXIT
	ENDIF
	
	CLEAR_BIT(iBoolsBitSet, biL_SetBlipHeight)
	blipPackage = ADD_BLIP_FOR_ENTITY(NET_TO_ENT(serverBD.niPackage))
	
	
//	SET_BLIP_PRIORITY(blipPackage, BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
//	SET_BLIP_SPRITE(blipPackage,  RADAR_TRACE_SIGHTSEER)
//	SET_BLIP_FLASH_TIMER(blipPackage, 7000)
//	SET_BLIP_EXTENDED_HEIGHT_THRESHOLD(blipPackage, TRUE)
	
	
	
	SET_BLIP_PRIORITY(blipPackage, BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
	SET_BLIP_SPRITE(blipPackage,  RADAR_TRACE_SIGHTSEER)
	SET_BLIP_NAME_FROM_TEXT_FILE(blipPackage, "GB_SGHT_BLP")
	SET_BLIP_ROUTE(blipPackage, TRUE)
	SET_BLIP_SCALE(blipPackage,g_sMPTunables.fgangboss_Job_blip_scale)  
	IF DID_MY_BOSS_LAUNCH_GB_SIGHTSEER()
	//	SET_BLIP_COLOUR_FROM_HUD_COLOUR(blipPackage, HUD_COLOUR_BLUE)
		SET_BLIP_COLOUR_FROM_HUD_COLOUR(blipPackage, HUD_COLOUR_GREEN)
	ENDIF
	
	SHOW_HEIGHT_ON_BLIP(blipPackage,TRUE)
	
	
	
	
	
//	SET_BLIP_PRIORITY(blipPackage, BLIPPRIORITY_HIGHEST)
//	SET_BLIP_NAME_FROM_TEXT_FILE(blipPackage, "AMCH_AC")
//	SET_BLIP_SPRITE(blipPackage,RADAR_TRACE_SIGHTSEER) //RADAR_TRACE_HELICOPTER
//	SET_BLIP_COLOUR_FROM_HUD_COLOUR(blipPackage,HUD_COLOUR_GREEN)
//	SHOW_HEIGHT_ON_BLIP(blipPackage,TRUE)
//	
	
	
	
	//-- New package sound
	iNewPackageSound = GET_SOUND_ID()
	IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
		PLAY_SOUND_FRONTEND(iNewPackageSound, "Blip_Pickup", "GTAO_Biker_Modes_Soundset", FALSE) 
	ELSE
		PLAY_SOUND_FRONTEND(iNewPackageSound, "Blip_Pickup", "GTAO_Magnate_Boss_Modes_Soundset", FALSE) 
	ENDIF
	
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [ADD_BLIP_FOR_PACKAGE] DOING NEW PACKAGE AUDIO... iNewPackageSound = ", iNewPackageSound)
			
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_PACKAGE_CLIENT] [ADD_BLIP_FOR_PACKAGE] ADDED BLIP")
ENDPROC

PROC MAINTAIN_DISTANCE_CHECKS_FOR_VARIATION()
	IF NOT DID_MY_BOSS_LAUNCH_GB_SIGHTSEER()
	AND NOT DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
		EXIT
	ENDIF
	
	VECTOR vAction
	PLAYER_INDEX playerBoss
//	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
//		vAction = GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niPackage), FALSE)
//	ELSE
		IF ServerBD.iBossPlayerWhoLaunched > -1
			playerBoss = INT_TO_PLAYERINDEX(ServerBD.iBossPlayerWhoLaunched)
			IF NETWORK_IS_PLAYER_ACTIVE(playerBoss)
				vAction = GET_ENTITY_COORDS(GET_PLAYER_PED(playerBoss), FALSE)
			ENDIF
		ENDIF
//	ENDIF
	
	
	IF NOT ARE_VECTORS_EQUAL(vAction, <<0.0, 0.0, 0.0>>)
		GB_MAINTAIN_BOSS_WORK_DISTANT_CHECKS(FMMC_TYPE_GB_SIGHTSEER, vAction, bDoneDistanceCheck)
	ENDIF
ENDPROC

PROC MAINTAIN_PLAYER_JOINING_AS_RIVAL()
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_JoinedAsRival)
		IF NOT DID_MY_BOSS_LAUNCH_GB_SIGHTSEER()
		//	IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
			IF serverBD.iPackagesCollected > 0
			OR IS_BIT_SET(serverBD.iServerBitSet, biS_ForceRivalsOnMission)
				#IF IS_DEBUG_BUILD
				IF IS_BIT_SET(serverBD.iServerBitSet, biS_ForceRivalsOnMission)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [MAINTAIN_PLAYER_JOINING_AS_RIVAL] biS_ForceRivalsOnMission IS SET")
				ENDIF
				#ENDIF
				Block_All_MissionsAtCoords_Missions(TRUE)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [MAINTAIN_PLAYER_JOINING_AS_RIVAL] Block_All_MissionsAtCoords_Missions(TRUE)")
				
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_JoinedAsRival)
			//	GB_SET_PLAYER_AS_PERMANENT_ON_BOSS_MISSION()
				
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [MAINTAIN_PLAYER_JOINING_AS_RIVAL] SET biP_JoinedAsRival - NOT SETTING AS PERMANENT")
			ENDIF	
		//	ENDIF
		ENDIF
	ENDIF
ENDPROC



PROC MAINTAIN_SIGHTSEER_INTRO_SHARD()
	IF NOT DID_MY_BOSS_LAUNCH_GB_SIGHTSEER()
	AND NOT DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
		EXIT
	ENDIF
	
	IF NOT SHOULD_LOCAL_PLAYER_SEE_GB_SIGHTSEER_HUD()
		EXIT
	ENDIF
	
	GB_UI_LEVEL myUiLevel
	STRING sOrganization
	HUD_COLOURS hclPlayer
	
	IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneIntroShard)
		myUiLevel = GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())
		IF myUiLevel >= GB_UI_LEVEL_MINIMAL
			IF PARTICIPANT_ID_TO_INT() = serverBD.iBossPartWhoLaunched
			OR GET_MY_BOSS_PARTICPANT_ID() = serverBD.iBossPartWhoLaunched
				IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_MP_RAY_LAVOY,"GB_INTTXT_SS",TXTMSG_LOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED) 
					//-- I'm the boss who launched event
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_START_OF_JOB, "BIGM_SGHTN", "BIGM_SGHTBD") // Collect the packages hidden around the map
					SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(TRUE)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [MAINTAIN_SIGHTSEER_INTRO_SHARD] SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(TRUE)")
					
					SET_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_OF_TYPE(PLAYER_ID(), PICKUP_PORTABLE_PACKAGE, TRUE)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [MAINTAIN_SIGHTSEER_INTRO_SHARD] SET_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_OF_TYPE TRUE - 99")
					
					GB_SET_LOCAL_PLAYER_CRITICAL_TO_JOB(TRUE)
					GB_SET_COMMON_TELEMETRY_DATA_ON_START()
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [MAINTAIN_SIGHTSEER_INTRO_SHARD] I'M BOSS WHO LAUNCHED WORK")
				ENDIF
			ELSE
//				IF GET_MY_BOSS_PARTICPANT_ID() = serverBD.iBossPartWhoLaunched
//					IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_MP_RAY_LAVOY,"GB_INTTXT_SS",TXTMSG_LOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED) 
//						//-- My boss launched the work
//						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_START_OF_JOB, "BIGM_SGHTN", "BIGM_SGHTGD") // Help your boss find the packages hidden around the map
//						SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(FALSE)
//						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [MAINTAIN_SIGHTSEER_INTRO_SHARD] MY BOSS LAUNCHED WORK")
//					ENDIF
//				ELSE
					IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
						//-- Rival gang launched
					//	SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_START_OF_JOB, "BIGM_SGHTN", "BIGM_SGHTRD") // Stop the rival boss collecting all the packages
						sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
						hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
						SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_START_OF_JOB, "BIGM_SGHTN", "BIGM_SGHTRD", sOrganization, hclPlayer) 
					//	SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(FALSE)
					//	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [MAINTAIN_SIGHTSEER_INTRO_SHARD] SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(FALSE)")
						
						//-- Change pickup type for 2599775
//						SET_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_OF_TYPE(PLAYER_ID(), PICKUP_PORTABLE_PACKAGE, FALSE)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [MAINTAIN_SIGHTSEER_INTRO_SHARD] SET_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_OF_TYPE FALSE - 1")
						
						GB_SET_COMMON_TELEMETRY_DATA_ON_START()
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [MAINTAIN_SIGHTSEER_INTRO_SHARD] RIVAL BOSS LAUNCHED WORK")
					ELSE
						//-- I'm not a gang member
					//	SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_START_OF_JOB, "BIGM_SGHTN", "BIGM_SGHTRD2") // Stop the rival boss collecting all the packages
						sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
						hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
						SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_START_OF_JOB, "BIGM_SGHTN", "BIGM_SGHTRD", sOrganization, hclPlayer) 
						
					//	SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(FALSE)
						
						//-- Change pickup type for 2599775
//						SET_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_OF_TYPE(PLAYER_ID(), PICKUP_PORTABLE_PACKAGE, FALSE)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [MAINTAIN_SIGHTSEER_INTRO_SHARD] SET_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_OF_TYPE FALSE - 2")
						
						GB_SET_COMMON_TELEMETRY_DATA_ON_START()
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [MAINTAIN_SIGHTSEER_INTRO_SHARD] RIVAL BOSS LAUNCHED WORK")
					ENDIF
		//		ENDIF
			ENDIF
		ELSE
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [MAINTAIN_SIGHTSEER_INTRO_SHARD] NOT DOING SHARD BECAUSE myUiLevel = ", ENUM_TO_INT(myUiLevel))
		ENDIF

		SET_BIT(iBoolsBitSet, biL_DoneIntroShard)
	ENDIF
ENDPROC

PROC MAINTAIN_SIGHTSEER_OBJECTIVE_TEXT()
	INT iTempGangID
	HUD_COLOURS hcTempGang
	INT iBlipColour
	
	IF IS_BIT_SET(hackSightseer.bsHacking,BS_IS_HACKING)
		EXIT
	ENDIF
	
	IF NOT SHOULD_LOCAL_PLAYER_SEE_GB_SIGHTSEER_HUD()
		IF IS_BIT_SET(iBoolsBitSet, biL_BlippedRival)
			IF serverBD.iBossPlayerWhoLaunched != -1
				iTempGangID = GET_GANG_ID_FOR_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
				IF iTempGangID > -1
					IF NETWORK_IS_PLAYER_ACTIVE(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))

						
						hcTempGang = GET_HUD_COLOUR_FOR_GANG_ID(iTempGangID)
						iBlipColour = GET_BLIP_COLOUR_FROM_HUD_COLOUR(hcTempGang)
						SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), RADAR_TRACE_TEMP_4, FALSE)
						SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), iBlipColour, FALSE)
						FORCE_BLIP_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), FALSE, FALSE)
						SET_PLAYER_BLIP_AS_LONG_RANGE(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), FALSE) 
						SET_FIXED_BLIP_SCALE_FOR_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), g_sMPTunables.fgangboss_Job_blip_scale, FALSE)
						CLEAR_BIT(iBoolsBitSet, biL_BlippedRival)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_SIGHTSEER_OBJECTIVE_TEXT] CLEAR biL_BlippedRival AS SHOULD_LOCAL_PLAYER_SEE_GB_SIGHTSEER_HUD")
					ELSE
						CLEAR_BIT(iBoolsBitSet, biL_BlippedRival)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_SIGHTSEER_OBJECTIVE_TEXT] CLEAR biL_BlippedRival AS IS_NET_PLAYER_ACTIVE")
						
					ENDIF
					
				ENDIF
			ENDIF
			
			Clear_Any_Objective_Text_From_This_Script()
		ENDIF
		
		
		
		EXIT
	ENDIF
	
//	IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) < GB_UI_LEVEL_FULL
//		IF Is_This_The_Current_Objective_Text()
//		Clear_Any_Objective_Text_From_This_Script()
//		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [MAINTAIN_SIGHTSEER_OBJECTIVE_TEXT] CLEARING AS MY UI LEVEL = ", ENUM_TO_INT(GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())))
//	//	EXIT
//	ENDIF
	
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
		IF PARTICIPANT_ID_TO_INT() = serverBD.iBossPartWhoLaunched
			//-- I'm the boss who launched event
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_JustCompHack)
				IF NOT IS_BIT_SET(hackSightseer.bsHacking,BS_IS_HACKING)
					IF playerBD[PARTICIPANT_ID_TO_INT()].iPackagesCollected < MAX_PACKAGES
						IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) = GB_UI_LEVEL_FULL
							IF NOT Is_This_The_Current_Objective_Text("GB_SGHT_HCK")
								Print_Objective_Text("GB_SGHT_HCK") // Find the next package.
							ENDIF
						ELSE
							IF Is_This_The_Current_Objective_Text("GB_SGHT_HCK")
								Clear_Any_Objective_Text_From_This_Script()
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [MAINTAIN_SIGHTSEER_OBJECTIVE_TEXT] CLEARING GB_SGHT_HCK AS MY UI LEVEL = ", ENUM_TO_INT(GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())))
							ENDIF
						ENDIF
					ELSE
						Clear_Any_Objective_Text_From_This_Script()
					ENDIF
				ENDIF
			ENDIF
		ELIF GET_MY_BOSS_PARTICPANT_ID() = serverBD.iBossPartWhoLaunched
			//-- My boss launched the work
			IF serverBD.iBossPlayerWhoLaunched != -1
				IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) = GB_UI_LEVEL_FULL
					IF NOT Is_This_The_Current_Objective_Text("GB_SGHT_PROT")
					//	Print_Objective_Text_With_Player_Name("GB_SGHT_PROT", INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched)) // Protect ~HUD_COLOUR_ORANGE~<C>~a~</C>.
						iTempGangID = GET_GANG_ID_FOR_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
						IF iTempGangID > -1
							hcTempGang = GET_HUD_COLOUR_FOR_GANG_ID(iTempGangID)
							Print_Objective_Text_With_Player_Name_And_String_Coloured("GB_SGHT_PROT", INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), "GB_SGHT_BOSS", DEFAULT, hcTempGang)
						//	Print_Objective_Text_With_Coloured_Player_Name("GB_SGHT_PROT", INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), hcTempGang) 
						ENDIF
					ENDIF
				ELSE
					IF Is_This_The_Current_Objective_Text("GB_SGHT_PROT")
						Clear_Any_Objective_Text_From_This_Script()
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [MAINTAIN_SIGHTSEER_OBJECTIVE_TEXT] CLEARING GB_SGHT_PROT AS MY UI LEVEL = ", ENUM_TO_INT(GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())))
					ENDIF
				ENDIF
			ENDIF
		ELIF DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
			//-- Not part of the launching gang
			IF serverBD.iBossPlayerWhoLaunched != -1
				IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) = GB_UI_LEVEL_FULL
					IF serverBD.iBossPlayerWhoLaunched != -1
						iTempGangID = GET_GANG_ID_FOR_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
						IF iTempGangID > -1
							hcTempGang = GET_HUD_COLOUR_FOR_GANG_ID(iTempGangID)
							IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
								IF NOT Is_This_The_Current_Objective_Text("GB_SGHT_STOP")
							//		Print_Objective_Text_With_Coloured_Player_Name("GB_SGHT_STOP", INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), hcTempGang) 
									 Print_Objective_Text_With_Player_Name_And_String_Coloured("GB_SGHT_STOP", INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), "GB_SGHT_BOSS", DEFAULT, hcTempGang)
								ENDIF
							ELSE
								IF NOT Is_This_The_Current_Objective_Text("GB_SGHT_STOP")
								//	Print_Objective_Text_With_Coloured_Player_Name("GB_SGHT_STOP2", INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), hcTempGang) 
									Print_Objective_Text_With_Player_Name_And_String_Coloured("GB_SGHT_STOP", INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), "GB_SGHT_BOSS", DEFAULT, hcTempGang)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
							
				ELSE
					IF Is_This_The_Current_Objective_Text("GB_SGHT_STOP")
						Clear_Any_Objective_Text_From_This_Script()
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [MAINTAIN_SIGHTSEER_OBJECTIVE_TEXT] CLEARING GB_SGHT_STOP AS MY UI LEVEL = ", ENUM_TO_INT(GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())))
					ENDIF
					IF Is_This_The_Current_Objective_Text("GB_SGHT_STOP2")
						Clear_Any_Objective_Text_From_This_Script()
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [MAINTAIN_SIGHTSEER_OBJECTIVE_TEXT] CLEARING GB_SGHT_STOP2 AS MY UI LEVEL = ", ENUM_TO_INT(GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())))
					ENDIF
				ENDIF
				
				
				IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_MINIMAL
					IF NOT IS_BIT_SET(iBoolsBitSet, biL_BlippedRival)
						IF serverBD.iBossPlayerWhoLaunched != -1
							IF NETWORK_IS_PLAYER_ACTIVE(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
								IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
									iTempGangID = GET_GANG_ID_FOR_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
									IF iTempGangID > -1
										hcTempGang = GET_HUD_COLOUR_FOR_GANG_ID(iTempGangID)
										iBlipColour = GET_BLIP_COLOUR_FROM_HUD_COLOUR(hcTempGang)
										
										SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), RADAR_TRACE_TEMP_4, TRUE)
										SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), iBlipColour, TRUE)
										FORCE_BLIP_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), TRUE, TRUE)
										SET_PLAYER_BLIP_AS_LONG_RANGE(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), TRUE) 
										SET_FIXED_BLIP_SCALE_FOR_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), g_sMPTunables.fgangboss_Job_blip_scale, TRUE)
										SET_BIT(iBoolsBitSet, biL_BlippedRival)
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_SIGHTSEER_OBJECTIVE_TEXT] SET biL_BlippedRival")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE	
					IF IS_BIT_SET(iBoolsBitSet, biL_BlippedRival)
						IF serverBD.iBossPlayerWhoLaunched != -1
							iTempGangID = GET_GANG_ID_FOR_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
							IF iTempGangID > -1
								hcTempGang = GET_HUD_COLOUR_FOR_GANG_ID(iTempGangID)
								iBlipColour = GET_BLIP_COLOUR_FROM_HUD_COLOUR(hcTempGang)
								SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), RADAR_TRACE_TEMP_4, FALSE)
								SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), iBlipColour, FALSE)
								FORCE_BLIP_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), FALSE, FALSE)
								SET_PLAYER_BLIP_AS_LONG_RANGE(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), FALSE) 
								SET_FIXED_BLIP_SCALE_FOR_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), g_sMPTunables.fgangboss_Job_blip_scale, FALSE)
								CLEAR_BIT(iBoolsBitSet, biL_BlippedRival)
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_SIGHTSEER_OBJECTIVE_TEXT] CLEAR biL_BlippedRival AS GB_GET_PLAYER_UI_LEVEL = ", ENUM_TO_INT(GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())))
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) = GB_UI_LEVEL_FULL
			IF PARTICIPANT_ID_TO_INT() = serverBD.iBossPartWhoLaunched
				//-- I'm the boss who launched event
				IF NOT IS_BIT_SET(hackSightseer.bsHacking,BS_IS_HACKING)
					IF NOT Is_This_The_Current_Objective_Text("GB_SGHT_RETP")
						Print_Objective_Text("GB_SGHT_RETP") // Retrieve the ~HUD_COLOUR_ORANGE~package.
					ENDIF
				ENDIF
			ELIF GET_MY_BOSS_PARTICPANT_ID() = serverBD.iBossPartWhoLaunched
				//-- My boss launched the work
				IF serverBD.iBossPlayerWhoLaunched != -1
					IF NOT Is_This_The_Current_Objective_Text("GB_SGHT_PROT")
					//	Print_Objective_Text_With_Player_Name("GB_SGHT_PROT", INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched)) // Protect ~HUD_COLOUR_ORANGE~<C>~a~</C>.
						iTempGangID = GET_GANG_ID_FOR_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
						IF iTempGangID > -1
							hcTempGang = GET_HUD_COLOUR_FOR_GANG_ID(iTempGangID)
							Print_Objective_Text_With_Player_Name_And_String_Coloured("GB_SGHT_PROT", INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), "GB_SGHT_BOSS", DEFAULT, hcTempGang)
						//	Print_Objective_Text_With_Coloured_Player_Name("GB_SGHT_PROT", INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), hcTempGang) 
						ENDIF
					ENDIF
					
//					IF NOT Is_This_The_Current_Objective_Text("GB_SGHT_RETP")
//						Print_Objective_Text("GB_SGHT_RETP") // Retrieve the ~HUD_COLOUR_ORANGE~package.
//					ENDIF
				ENDIF
			ELIF DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
				//-- Not part of the launching gang
				IF serverBD.iBossPlayerWhoLaunched != -1
					iTempGangID = GET_GANG_ID_FOR_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
					IF iTempGangID > -1
						hcTempGang = GET_HUD_COLOUR_FOR_GANG_ID(iTempGangID)
						IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
							IF NOT Is_This_The_Current_Objective_Text("GB_SGHT_STOP")
						//		Print_Objective_Text_With_Coloured_Player_Name("GB_SGHT_STOP", INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), hcTempGang) 
								 Print_Objective_Text_With_Player_Name_And_String_Coloured("GB_SGHT_STOP", INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), "GB_SGHT_BOSS", DEFAULT, hcTempGang)
							ENDIF
						ELSE
							IF NOT Is_This_The_Current_Objective_Text("GB_SGHT_STOP")
							//	Print_Objective_Text_With_Coloured_Player_Name("GB_SGHT_STOP2", INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), hcTempGang) 
								Print_Objective_Text_With_Player_Name_And_String_Coloured("GB_SGHT_STOP", INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), "GB_SGHT_BOSS", DEFAULT, hcTempGang)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF Is_This_The_Current_Objective_Text("GB_SGHT_PROT")
				Clear_Any_Objective_Text_From_This_Script()
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [MAINTAIN_SIGHTSEER_OBJECTIVE_TEXT] CLEARING GB_SGHT_PROT AS MY UI LEVEL = ", ENUM_TO_INT(GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())))
			ENDIF
			IF Is_This_The_Current_Objective_Text("GB_SGHT_RETP")
				Clear_Any_Objective_Text_From_This_Script()
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [MAINTAIN_SIGHTSEER_OBJECTIVE_TEXT] CLEARING GB_SGHT_RETP AS MY UI LEVEL = ", ENUM_TO_INT(GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())))
			ENDIF
			IF Is_This_The_Current_Objective_Text("GB_SGHT_STOP")
				Clear_Any_Objective_Text_From_This_Script()
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [MAINTAIN_SIGHTSEER_OBJECTIVE_TEXT] CLEARING GB_SGHT_STOP AS MY UI LEVEL = ", ENUM_TO_INT(GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())))
			ENDIF
			IF Is_This_The_Current_Objective_Text("GB_SGHT_STOP2")
				Clear_Any_Objective_Text_From_This_Script()
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [MAINTAIN_SIGHTSEER_OBJECTIVE_TEXT] CLEARING GB_SGHT_STOP2 AS MY UI LEVEL = ", ENUM_TO_INT(GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())))
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC MAINTAIN_SIGHTSEER_HELP_TEXT()
	
	IF IS_BIT_SET(hackSightseer.bsHacking,BS_IS_HACKING)
		EXIT
	ENDIF
	
	STRING sOrganization
	HUD_COLOURS hclPlayer
	
	IF NOT SHOULD_LOCAL_PLAYER_SEE_GB_SIGHTSEER_HUD()
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GB_SGHT_HLP1")
			CLEAR_HELP()
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_SIGHTSEER_HELP_TEXT] CLEAR GB_SGHT_HLP1 AS SHOULD_LOCAL_PLAYER_SEE_GB_SIGHTSEER_HUD")
		ENDIF
		
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GB_SGHT_HLP2")
			CLEAR_HELP()
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_SIGHTSEER_HELP_TEXT] CLEAR GB_SGHT_HLP2 AS SHOULD_LOCAL_PLAYER_SEE_GB_SIGHTSEER_HUD")
		ENDIF

		IF DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
			sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
			hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
			IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
				IF IS_THIS_HELP_MESSAGE_WITH_LITERAL_STRING_AND_COLOURED_STRING_BEING_DISPLAYED("GB_SGHT_HLP3",sOrganization,  hclPlayer,  "GB_SGHT_TGT", hclPlayer)
					CLEAR_HELP()
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_SIGHTSEER_HELP_TEXT] CLEAR GB_SGHT_HLP3 AS SHOULD_LOCAL_PLAYER_SEE_GB_SIGHTSEER_HUD")
				ENDIF
			ELSE
				IF IS_THIS_HELP_MESSAGE_WITH_LITERAL_STRING_AND_COLOURED_STRING_BEING_DISPLAYED("GB_SGHT_HLP4",sOrganization,  hclPlayer,  "GB_SGHT_TGT", hclPlayer)
					CLEAR_HELP()
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_SIGHTSEER_HELP_TEXT] CLEAR GB_SGHT_HLP4 AS SHOULD_LOCAL_PLAYER_SEE_GB_SIGHTSEER_HUD")
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GB_SGHT_RVL")
			CLEAR_HELP()
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_SIGHTSEER_HELP_TEXT] CLEAR GB_SGHT_RVL AS SHOULD_LOCAL_PLAYER_SEE_GB_SIGHTSEER_HUD")
		ENDIF
		
		EXIT
	ENDIF
	
	GB_UI_LEVEL myUiLevel = GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())
	IF myUiLevel < GB_UI_LEVEL_MINIMAL
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GB_SGHT_HLP1")
			CLEAR_HELP()
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_SIGHTSEER_HELP_TEXT] CLEAR GB_SGHT_HLP1 AS myUiLevel = ", ENUM_TO_INT(myUiLevel))
		ENDIF
		
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GB_SGHT_HLP2")
			CLEAR_HELP()
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_SIGHTSEER_HELP_TEXT] CLEAR GB_SGHT_HLP2 AS myUiLevel = ", ENUM_TO_INT(myUiLevel))
		ENDIF
		
		IF DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
			sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
			hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
			IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
				IF IS_THIS_HELP_MESSAGE_WITH_LITERAL_STRING_AND_COLOURED_STRING_BEING_DISPLAYED("GB_SGHT_HLP3",sOrganization,  hclPlayer,  "GB_SGHT_TGT", hclPlayer)
					CLEAR_HELP()
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_SIGHTSEER_HELP_TEXT] CLEAR GB_SGHT_HLP3 AS myUiLevel = ", ENUM_TO_INT(myUiLevel))
				ENDIF
			ELSE
				IF IS_THIS_HELP_MESSAGE_WITH_LITERAL_STRING_AND_COLOURED_STRING_BEING_DISPLAYED("GB_SGHT_HLP4",sOrganization,  hclPlayer,  "GB_SGHT_TGT", hclPlayer)
					CLEAR_HELP()
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_SIGHTSEER_HELP_TEXT] CLEAR GB_SGHT_HLP4 AS myUiLevel = ", ENUM_TO_INT(myUiLevel))
				ENDIF
			ENDIF
		ENDIF
		
		
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GB_SGHT_RVL")
			CLEAR_HELP()
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_SIGHTSEER_HELP_TEXT] CLEAR GB_SGHT_RVL AS myUiLevel = ", ENUM_TO_INT(myUiLevel))
		ENDIF
		
		EXIT
	ENDIF
	
	//-- Intro help
	IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneIntroHelp)
		IF NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GB_START_OF_JOB)
			IF iHackingProg = -1
				IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
					IF PARTICIPANT_ID_TO_INT() = serverBD.iBossPartWhoLaunched
						//-- I'm the boss who launched event
						PRINT_HELP_NO_SOUND("GB_SGHT_HLP1") // You have started Sightseer. Retrieve all packages using the Sightseer App to earn cash, RP and JP rewards
						GB_SET_GANG_BOSS_HELP_BACKGROUND()
						SET_BIT(iBoolsBitSet, biL_DoneIntroHelp)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_SIGHTSEER_HELP_TEXT] I'M BOSS, SET biL_DoneIntroHelp")
						
					ELIF GET_MY_BOSS_PARTICPANT_ID() = serverBD.iBossPartWhoLaunched
						//-- My boss launched the work
						PRINT_HELP_NO_SOUND("GB_SGHT_HLP2") // Help your boss find the packages hidden around the map.
						GB_SET_GANG_BOSS_HELP_BACKGROUND()
						SET_BIT(iBoolsBitSet, biL_DoneIntroHelp)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_SIGHTSEER_HELP_TEXT] MY BOSS LAUNCHED, SET biL_DoneIntroHelp")
						
					ELIF DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
						sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
						hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
						IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
							//-- Not part of the launching gang
						//	PRINT_HELP_NO_SOUND("GB_SGHT_HLP3") // Stop the rival boss collecting all the packages.
						//	PRINT_HELP_WITH_COLOURED_STRING_AND_LITERAL_STRING_NO_SOUND("GB_SGHT_HLP3", "GB_SGHT_TGT", hclPlayer, sOrganization, hclPlayer)
							PRINT_HELP_WITH_LITERAL_STRING_AND_COLOURED_STRING_NO_SOUND("GB_SGHT_HLP3",sOrganization,  hclPlayer,  "GB_SGHT_TGT", hclPlayer)
							GB_SET_GANG_BOSS_HELP_BACKGROUND()
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_SIGHTSEER_HELP_TEXT] RIVAL BOSS LAUNCHED, SET biL_DoneIntroHelp")
						ELSE
							//-- Not in a gang
						//	PRINT_HELP_NO_SOUND("GB_SGHT_HLP4") // Stop the boss collecting all the packages.
						//	PRINT_HELP_WITH_COLOURED_STRING_AND_LITERAL_STRING_NO_SOUND("GB_SGHT_HLP4", "GB_SGHT_TGT", hclPlayer, sOrganization, hclPlayer)
							PRINT_HELP_WITH_LITERAL_STRING_AND_COLOURED_STRING_NO_SOUND("GB_SGHT_HLP4",sOrganization,  hclPlayer,  "GB_SGHT_TGT", hclPlayer)
							GB_SET_GANG_BOSS_HELP_BACKGROUND()
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_SIGHTSEER_HELP_TEXT] I'm not in a gang, SET biL_DoneIntroHelp")
						ENDIF
						
						SET_BIT(iBoolsBitSet, biL_DoneIntroHelp)
					ENDIF
					
					
				ELSE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_SIGHTSEER_HELP_TEXT] NOT IS_OK_TO_PRINT_FREEMODE_HELP - biL_DoneIntroHelp")
				ENDIF
			ELSE
				SET_BIT(iBoolsBitSet, biL_DoneIntroHelp)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_SIGHTSEER_HELP_TEXT] SET biL_DoneIntroHelp as already hacking")
			ENDIF
		ENDIF
	ENDIF
	
	//-- Sightseer app help
	IF IS_BIT_SET(iBoolsBitSet, biL_DoneIntroHelp)
		IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneAppHelp)
			IF iHackingProg = -1
				IF PARTICIPANT_ID_TO_INT() = serverBD.iBossPartWhoLaunched
					IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
						PRINT_HELP_NO_SOUND("GB_SGHT_APPH") // The Sightseer app on your phone will reveal the location of the next package.
						GB_SET_GANG_BOSS_HELP_BACKGROUND()
						SET_BIT(iBoolsBitSet, biL_DoneAppHelp)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_SIGHTSEER_HELP_TEXT] I'M BOSS, SET biL_DoneAppHelp")
						
					ELSE
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_SIGHTSEER_HELP_TEXT] NOT IS_OK_TO_PRINT_FREEMODE_HELP - biL_DoneAppHelp")
					ENDIF
				ELSE
					SET_BIT(iBoolsBitSet, biL_DoneAppHelp)
				ENDIF
			ELSE
				SET_BIT(iBoolsBitSet, biL_DoneAppHelp)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_SIGHTSEER_HELP_TEXT] SET biL_DoneAppHelp as already hacking")
			ENDIF
		ENDIF
	ENDIF
	
	//-- 1St package revealed.
	IF NOT IS_BIT_SET(iBoolsBitSet, biL_DonePackageHelp)
		IF DID_MY_BOSS_LAUNCH_GB_SIGHTSEER()
			IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
					IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
						PRINT_HELP_NO_SOUND("GB_SGHT_PKGH") // The package is shown on the Radar at
						GB_SET_GANG_BOSS_HELP_BACKGROUND()
						SET_BIT(iBoolsBitSet, biL_DonePackageHelp)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_SIGHTSEER_HELP_TEXT] , SET biL_DonePackageHelp")
							
					ELSE
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_SIGHTSEER_HELP_TEXT] NOT IS_OK_TO_PRINT_FREEMODE_HELP - biL_DonePackageHelp")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//-- Launching gang told that rivals have joined
	IF NOT IS_BIT_SET(iBoolsBitSet, biL_RivalsJoinedHelp)
		IF DID_MY_BOSS_LAUNCH_GB_SIGHTSEER()
			IF serverBD.iPackagesCollected > 0
			OR IS_BIT_SET(serverBD.iServerBitSet, biS_ForceRivalsOnMission)
				IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
					PRINT_HELP_NO_SOUND("GB_SGHT_RVL") // Other players in the session have been alerted to your activities. These players can now come after your Organization to earn cash and RP.
					GB_SET_GANG_BOSS_HELP_BACKGROUND()
					SET_BIT(iBoolsBitSet, biL_RivalsJoinedHelp)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_SIGHTSEER_HELP_TEXT] , SET biL_RivalsJoinedHelp")
						
				ELSE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_SIGHTSEER_HELP_TEXT] NOT IS_OK_TO_PRINT_FREEMODE_HELP - biL_RivalsJoinedHelp")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_PLAYER_HACKING()
	IF PARTICIPANT_ID_TO_INT() != serverBD.iBossPartWhoLaunched
		//-- I'm not the boss who launched - do nothing
		EXIT
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
		IF iHackingProg != -1	
			iHackingProg = -1
		ENDIF
		IF IS_BIT_SET(BitSet_CellphoneTU, g_BSTU_ENABLE_MP_SIGHTSEER_APP)
			CLEAR_BIT(BitSet_CellphoneTU, g_BSTU_ENABLE_MP_SIGHTSEER_APP)
		ENDIF
		
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(BitSet_CellphoneTU, g_BSTU_ENABLE_MP_SIGHTSEER_APP) //-- SHould icon appear
		IF NOT IS_BIT_SET(BitSet_CellphoneTU, g_BSTU_MP_SIGHTSEER_APP_HAS_LAUNCHED)
			IF iHackingProg < 0
				SET_BIT(BitSet_CellphoneTU, g_BSTU_ENABLE_MP_SIGHTSEER_APP)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_PLAYER_HACKING] SET g_BSTU_ENABLE_MP_SIGHTSEER_APP")
			ENDIF
		ENDIF
	ENDIF
		
	IF iMyHackingMG = -1
		iMyHackingMG = GET_RANDOM_INT_IN_RANGE(0,MAX_NUM_HACKING_MG)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_PLAYER_HACKING] iMyHackingMG = ", iMyHackingMG)
	ENDIF
	
	IF iHackingProg < 0
		IF IS_BIT_SET(BitSet_CellphoneTU, g_BSTU_MP_SIGHTSEER_APP_HAS_LAUNCHED)
		#IF IS_DEBUG_BUILD
		OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_DOWN)
		#ENDIF
			CLEAR_BIT(BitSet_CellphoneTU, g_BSTU_MP_SIGHTSEER_APP_HAS_LAUNCHED) // Selected phone icon
			iHackingProg = 0
		ENDIF
	ENDIF
	
	
	
	DO_SIGHTSEER_HACKING()
ENDPROC

/// PURPOSE:
///    Draws a marker above the package when it has been dropped
PROC MAINTAIN_PACKAGE_MARKER()
	IF NOT SHOULD_LOCAL_PLAYER_SEE_GB_SIGHTSEER_HUD()
		EXIT
	ENDIF
	INT r, g, b, a
	
	IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_BACKGROUND
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niPackage)
		AND NOT IS_ENTITY_DEAD(NET_TO_OBJ(serverBD.niPackage))
			IF NOT IS_ENTITY_ATTACHED_TO_ANY_PED(NET_TO_OBJ(serverBD.niPackage))

				GET_HUD_COLOUR(HUD_COLOUR_GREEN, r, g, b, a)
				DRAW_MARKER(MARKER_ARROW, GET_ENTITY_COORDS(NET_TO_ENT(serverBD.niPackage))+<<0,0,1>>, <<0,0,0>>, <<180,0,0>>, <<0.5,0.5,0.5>>, r, g, b, 100, TRUE, TRUE)
				
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PROC MAINTAIN_PLAYER_COLLECT_PACKAGES()
//	
//	
//	
//	ENTITY_INDEX ent
//	//-- Update whether or not I've got the package
//	IF PARTICIPANT_ID_TO_INT() = serverBD.iBossPartWhoLaunched
//		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
//				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_GotThePackage)
//					IF IS_ENTITY_ATTACHED_TO_ENTITY(NET_TO_OBJ(serverBD.niPackage), PLAYER_PED_ID())
//						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_GotThePackage)
//						
//						
//						CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_JustCompHack)
//						
//						playerBD[PARTICIPANT_ID_TO_INT()].iPackagesCollected++
//						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_PLAYER_COLLECT_PACKAGES] SET biP_GotThePackage iPackagesCollected = ", playerBD[PARTICIPANT_ID_TO_INT()].iPackagesCollected)
//					//	GB_SET_LOCAL_PLAYER_CRITICAL_TO_JOB(TRUE)
//
//					ENDIF
//				ELSE
//					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niPackage)
//						ent = NET_TO_ENT(serverBD.niPackage)
//						DELETE_ENTITY(ent)
//						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_PLAYER_COLLECT_PACKAGES] I DELETED THE PACKAGE")
//					ELSE
//						NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.niPackage)
//						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_PLAYER_COLLECT_PACKAGES] REQUESTING CONTROL OF THE PACKAGE")
//					ENDIF
//				ENDIF
//			ELSE
//				IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_GotThePackage)
//					CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_GotThePackage)
//					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_PLAYER_COLLECT_PACKAGES] CLEARED biP_GotThePackage AS NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID")
//				ENDIF
//			ENDIF
//		ELSE
//			
//		ENDIF
//	ENDIF
//	
//	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
//		
//		IF DID_MY_BOSS_LAUNCH_GB_SIGHTSEER()
//			ADD_BLIP_FOR_PACKAGE()
//		ENDIF
//		
//		IF NOT IS_BIT_SET(iBoolsBitSet, biL_SetPackageLOD)
//			SET_ENTITY_LOD_DIST(NET_TO_OBJ(serverBD.niPackage), 1200) 
//			SET_BIT(iBoolsBitSet, biL_SetPackageLOD)
//			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  [MAINTAIN_PLAYER_COLLECT_PACKAGES] SET PACKAGE LOD DIST")
//		ENDIF
//	ELSE
//		IF IS_BIT_SET(iBoolsBitSet, biL_SetPackageLOD)
//			CLEAR_BIT(iBoolsBitSet, biL_SetPackageLOD)
//			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  [MAINTAIN_PLAYER_COLLECT_PACKAGES] CLEAR biL_SetPackageLOD")
//		ENDIF
//		
//		IF DOES_BLIP_EXIST(blipPackage)
//			REMOVE_BLIP(blipPackage)
//			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  [MAINTAIN_PLAYER_COLLECT_PACKAGES] REMOVED PACKAGE BLIP")
//		ENDIF
//	ENDIF
//					
//	MAINTAIN_PACKAGE_MARKER()
//ENDPROC

PROC MAINTAIN_PLAYER_COLLECT_PACKAGES()
	
	
	
	ENTITY_INDEX ent
	//-- Update whether or not I've got the package

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_GotThePackage)
				IF IS_ENTITY_ATTACHED_TO_ENTITY(NET_TO_OBJ(serverBD.niPackage), PLAYER_PED_ID())
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_GotThePackage)
					
					
					//-- Pickup sound
					iPickupSound = GET_SOUND_ID()
					IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
						PLAY_SOUND_FRONTEND(iPickupSound, "Pickup_Briefcase", "GTAO_Biker_Modes_Soundset", FALSE) 
					ELSE
						PLAY_SOUND_FRONTEND(iPickupSound, "Pickup_Briefcase", "GTAO_Magnate_Boss_Modes_Soundset", FALSE) 
					ENDIF
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_PLAYER_COLLECT_PACKAGES] DOING COLLECT PICKUP AUDIO... iPickupSound = ", iPickupSound)
			
				//	CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_JustCompHack)
					
					playerBD[PARTICIPANT_ID_TO_INT()].iPackagesCollected++
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_PLAYER_COLLECT_PACKAGES] SET biP_GotThePackage iPackagesCollected = ", playerBD[PARTICIPANT_ID_TO_INT()].iPackagesCollected)

				ENDIF

			ENDIF
		ENDIF
	ENDIF
	
	IF iLocalPackagesCollected != serverBD.iPackagesCollected
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_JustCompHack)
			CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_JustCompHack)
		ENDIF
		
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_GotThePackage)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niPackage)
				ent = NET_TO_ENT(serverBD.niPackage)
				DELETE_ENTITY(ent)
				
				CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_GotThePackage)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_PLAYER_COLLECT_PACKAGES] I DELETED THE PACKAGE")
				
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_PLAYER_COLLECT_PACKAGES] (2) iLocalPackagesCollected UPDATED TO ", iLocalPackagesCollected)
			ELSE
				NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.niPackage)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_PLAYER_COLLECT_PACKAGES] REQUESTING CONTROL OF THE PACKAGE")
			ENDIF
		ELSE	
			iLocalPackagesCollected = serverBD.iPackagesCollected
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [MAINTAIN_PLAYER_COLLECT_PACKAGES] (1) iLocalPackagesCollected UPDATED TO ", iLocalPackagesCollected)
		ENDIF
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
		
//		IF DID_MY_BOSS_LAUNCH_GB_SIGHTSEER()
//			ADD_BLIP_FOR_PACKAGE()
//		ENDIF
		
		IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
			IF DID_MY_BOSS_LAUNCH_GB_SIGHTSEER()
				ADD_BLIP_FOR_PACKAGE()
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(iBoolsBitSet, biL_PreventCollection)
			IF NOT DID_MY_BOSS_LAUNCH_GB_SIGHTSEER()
			OR NOT GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
				PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.niPackage), TRUE, TRUE)
				SET_BIT(iBoolsBitSet, biL_PreventCollection)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  [MAINTAIN_PLAYER_COLLECT_PACKAGES] SET biL_PreventCollection")
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(iBoolsBitSet, biL_SetPackageLOD)
			SET_ENTITY_LOD_DIST(NET_TO_OBJ(serverBD.niPackage), 1200) 
			SET_BIT(iBoolsBitSet, biL_SetPackageLOD)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  [MAINTAIN_PLAYER_COLLECT_PACKAGES] SET PACKAGE LOD DIST")
		ENDIF
	ELSE
		IF IS_BIT_SET(iBoolsBitSet, biL_PreventCollection)
			CLEAR_BIT(iBoolsBitSet, biL_PreventCollection)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  [MAINTAIN_PLAYER_COLLECT_PACKAGES] CLEAR biL_PreventCollection serverBD.iPackagesCollected = ", serverBD.iPackagesCollected)
		ENDIF
		IF IS_BIT_SET(iBoolsBitSet, biL_SetPackageLOD)
			CLEAR_BIT(iBoolsBitSet, biL_SetPackageLOD)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  [MAINTAIN_PLAYER_COLLECT_PACKAGES] CLEAR biL_SetPackageLOD")
		ENDIF
		
		IF DOES_BLIP_EXIST(blipPackage)
			REMOVE_BLIP(blipPackage)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  [MAINTAIN_PLAYER_COLLECT_PACKAGES] REMOVED PACKAGE BLIP")
		ENDIF
	ENDIF
					
	MAINTAIN_PACKAGE_MARKER()
ENDPROC

PROC MAINTAIN_TICKERS()
	PLAYER_INDEX playerPackage
	INT iTempGangID
	HUD_COLOURS hcTempGang
	
	//-- Ticker for boss collecting packages
	IF iLocalCollectedPackageCountForTicker != serverBD.iPackagesCollected
		IF serverBD.iBossPlayerWhoLaunched != -1
			IF SHOULD_LOCAL_PLAYER_SEE_GB_SIGHTSEER_HUD()
				IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_BACKGROUND
					IF serverBD.iPackagesCollected > 0
						IF serverBD.iLastPlayerToCollect > -1
							playerPackage = INT_TO_PLAYERINDEX(serverBD.iLastPlayerToCollect )
							IF NETWORK_IS_PLAYER_ACTIVE(playerPackage)
								IF playerPackage != PLAYER_ID()
									IF GB_IS_PLAYER_MEMBER_OF_A_GANG(playerPackage)
										iTempGangID = GET_GANG_ID_FOR_PLAYER(playerPackage)
										IF iTempGangID > -1
											hcTempGang = GET_HUD_COLOUR_FOR_GANG_ID(iTempGangID)
											PRINT_TICKER_WITH_PLAYER_NAME_WITH_HUD_COLOUR("GB_SGHT_TCKC", playerPackage, hcTempGang) // ~a~ collected the package.
											CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  [MAINTAIN_TICKERS] PLAYER COLLECTED PACKAGE TICKER ", GET_PLAYER_NAME(playerPackage), " iTempGangID = ", iTempGangID)
										ENDIF
									ELSE	
										PRINT_TICKER_WITH_PLAYER_NAME_WITH_HUD_COLOUR("GB_SGHT_TCKC", playerPackage, HUD_COLOUR_WHITE) // ~a~ collected the package.
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  [MAINTAIN_TICKERS] PLAYER COLLECTED PACKAGE TICKER ", GET_PLAYER_NAME(playerPackage))
										
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			
			iLocalCollectedPackageCountForTicker = serverBD.iPackagesCollected
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  [MAINTAIN_TICKERS] iLocalCollectedPackageCountForTicker UPDATED TO ", iLocalCollectedPackageCountForTicker)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_SIGHTSEER_COLLECT_PACKAGES_CLIENT()
	MAINTAIN_PLAYER_JOINING_AS_RIVAL()
	
	MAINTAIN_SIGHTSEER_INTRO_SHARD()
	
	MAINTAIN_SIGHTSEER_OBJECTIVE_TEXT()
	
	MAINTAIN_SIGHTSEER_HELP_TEXT()
	
	MAINTAIN_PLAYER_HACKING()
	
	MAINTAIN_PLAYER_COLLECT_PACKAGES()
	
	MAINTAIN_BOTTOM_RIGHT_HUD()
	
	MAINTAIN_TICKERS()
ENDPROC

//PURPOSE: Process the SIGHTSEER stages for the Client
PROC PROCESS_SIGHTSEER_CLIENT()
	
	SWITCH playerBD[PARTICIPANT_ID_TO_INT()].eStage		
		CASE eAD_COLLECT_PACKAGES
			MAINTAIN_SIGHTSEER_COLLECT_PACKAGES_CLIENT()
			
			CHECK_FOR_REWARD()
			IF serverBD.eStage = eAD_OFF_MISSION
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_OFF_MISSION
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  PLAYER STAGE eAD_COLLECT_PACKAGES -> eAD_OFF_MISSION AS SERVER SAYS SO   <----------     ") NET_NL()
			ELIF serverBD.eStage = eAD_CLEANUP
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CLEANUP
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  PLAYER STAGE eAD_COLLECT_PACKAGES -> eAD_CLEANUP AS SERVER SAYS SO   <----------     ") NET_NL()
			ENDIF
		BREAK
		
		
		CASE eAD_OFF_MISSION
			CHECK_FOR_REWARD()
			
			IF serverBD.eStage = eAD_CLEANUP
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CLEANUP
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  PLAYER STAGE eAD_OFF_MISSION -> eAD_CLEANUP AS SERVER SAYS SO   <----------     ") NET_NL()
			ENDIF
			

		BREAK
		
		CASE eAD_CLEANUP
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  SCRIPT CLEANUP B     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		BREAK
		
	ENDSWITCH
ENDPROC


PROC INIT_PACKAGE_COORDS()
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [INIT_PACKAGE_COORDS] CALLED serverBD.iMapArea = ", serverBD.iMapArea)
	SWITCH serverBD.iMapArea
		CASE 0 // Bottom right
			vPackageLocations[0] = 	<<959.4771, -1673.8391, 29.0562>>
			vPackageLocations[1] = 	<<711.3848, -1230.5692, 25.5556>>
			vPackageLocations[2] = 	<<282.5148, -217.2699, 62.4801>>
			vPackageLocations[3] = 	<<388.7341, -356.1966, 47.0230>>
			vPackageLocations[4] = 	<<1112.4032, -645.8195, 55.8211>>
			vPackageLocations[5] = 	<<1112.4032, -645.8195, 55.8211>>
			vPackageLocations[6] = 	<<175.7449, 557.3550, 179.1420>>
			vPackageLocations[7] = 	<<1061.6794, 214.6577, 83.9904>>
			vPackageLocations[8] = 	<<768.4620, 1279.4580, 359.2967>>
			vPackageLocations[9] = 	<<2590.4685, 486.6694, 107.6900>>
			vPackageLocations[10] = <<2820.6589, -760.0440, 1.4262>>
			vPackageLocations[11] = <<1169.6370, -1382.5081, 33.7916>>
			vPackageLocations[12] = <<1590.3009, -1691.7795, 87.1030>>
			vPackageLocations[13] = <<857.6887, -2497.0981, 27.3195>>
			vPackageLocations[14] = <<1292.7371, -3097.0681, 4.9066>>
			vPackageLocations[15] = <<590.4111, -2902.5349, 5.2464>>
			vPackageLocations[16] = <<491.5606, -2122.3984, 4.9175>>
			vPackageLocations[17] = <<210.1278, -937.7384, 23.1416>>
			vPackageLocations[18] = <<483.4742, -1480.0090, 28.2904>>
			vPackageLocations[19] = <<2288.8538, 1727.7062, 67.0398>>
		BREAK
		
		CASE 1 // Bottom left
			vPackageLocations[0] = 	<<-884.5568, -2477.5503, 12.9877>>
			vPackageLocations[1] = 	<<-81.4004, -2707.4028, 5.4398>>
			vPackageLocations[2] = 	<<-176.0256, -633.2856, 47.9812>>
			vPackageLocations[3] = 	<<-777.4452, 20.5786, 39.6541>>
			vPackageLocations[4] = 	<<-2333.4585, 237.6722, 168.6020>>
			vPackageLocations[5] = 	<<-1096.0421, -323.1984, 36.8237>>
			vPackageLocations[6] = 	<<-822.8544, -1222.8884, 6.3704>>
			vPackageLocations[7] = 	<<-590.8558, -911.0305, 22.8747>>
			vPackageLocations[8] = 	<<-110.7806, 356.4308, 111.6961>>
			vPackageLocations[9] = 	<<-330.9974, -1314.5732, 30.5497>>
			vPackageLocations[10] = <<-330.9974, -1314.5732, 30.5497>>
			vPackageLocations[11] = <<-255.1383, -1543.0918, 30.9032>>
			vPackageLocations[12] = <<-1659.0140, 235.4190, 61.3910>>
			vPackageLocations[13] = <<-3276.4460, 967.6040, 7.3522>>
			vPackageLocations[14] = <<-1997.6960, -257.5920, 28.0968>>
			vPackageLocations[15] = <<-388.8625, -2282.6709, 6.6082>>
			vPackageLocations[16] = <<-1048.5988, -1120.1794, 1.1586>>
			vPackageLocations[17] = <<-287.5583, 11.2136, 53.7525>>
			vPackageLocations[18] = <<-481.1961, 1101.5277, 324.8545>>
			vPackageLocations[19] = <<-1521.9890, 1493.5560, 110.5930>>
		BREAK
		
		CASE 2 // Top left
			vPackageLocations[0] = 	<<-1585.6285, 2084.2061, 71.3231>>
			vPackageLocations[1] = 	<<-2188.0344, 4249.6064, 47.9395>>
			vPackageLocations[2] = 	<<-685.6700, 5794.1553, 16.3310>>
			vPackageLocations[3] = 	<<-329.5496, 6150.0020, 31.3133>>
			vPackageLocations[4] = 	<<-275.9802, 6637.0508, 6.4552>>
			vPackageLocations[5] = 	<<-800.4709, 5392.4214, 33.4985>>
			vPackageLocations[6] = 	<<-59.0130, 4416.5596, 55.8893>>
			vPackageLocations[7] = 	<<-207.2786, 3658.0266, 50.7524>>
			vPackageLocations[8] = 	<<-596.9946, 2091.1721, 130.4128>>
			vPackageLocations[9] = 	<<-2080.8223, 2608.6394, 2.0890>>
			vPackageLocations[10] = <<-1622.2966, 3191.9233, 29.2567>>
			vPackageLocations[11] = <<-1098.1659, 2725.3384, 17.8007>>
			vPackageLocations[12] = <<-31.1729, 1953.4202, 189.1861>>
			vPackageLocations[13] = <<-281.9674, 2847.1807, 52.8867>>
			vPackageLocations[14] = <<-1633.1860, 4736.7275, 52.2963>>
			vPackageLocations[15] = <<-520.7480, 5307.1763, 79.2448>>
			vPackageLocations[16] = <<50.1324, 7117.7813, 2.1957>>
			vPackageLocations[17] = <<163.8223, 6654.2598, 30.5682>>
			vPackageLocations[18] = <<59.0807, 6310.7725, 30.3764>>
			vPackageLocations[19] = <<-1901.7557, 1911.1028, 165.2867>>
		BREAK
		
		CASE 3 // Top right
			vPackageLocations[0] = 	<<721.9799, 2314.4768, 49.4146>>
			vPackageLocations[1] = 	<<376.9765, 2628.4302, 43.6444>>
			vPackageLocations[2] = 	<<1535.2140, 6597.8652, 2.1543>>
			vPackageLocations[3] = 	<<2446.7566, 4963.8252, 45.5766>>
			vPackageLocations[4] = 	<<2467.8208, 4080.6055, 37.0648>>
			vPackageLocations[5] = 	<<1648.7114, 3811.4292, 37.6556>>
			vPackageLocations[6] = 	<<1297.2715, 4342.0190, 40.3207>>
			vPackageLocations[7] = 	<<1541.1450, 6335.5781, 23.0751>>
			vPackageLocations[8] = 	<<2014.1868, 4721.4053, 40.6090>>
			vPackageLocations[9] = 	<<3800.8481, 4475.0537, 4.9977>>
			vPackageLocations[10] = <<2661.8586, 3467.0842, 54.8171>>
			vPackageLocations[11] = <<430.2967, 6467.5132, 27.7718>>
			vPackageLocations[12] = <<2930.0720, 4623.0176, 47.7272>>
			vPackageLocations[13] = <<2973.6848, 3480.1155, 70.4423>>
			vPackageLocations[14] = <<1989.0813, 3781.6111, 31.1808>>
			vPackageLocations[15] = <<908.6296, 3648.4580, 35.1522>>
			vPackageLocations[16] = <<1560.5808, 2167.0127, 77.7169>>
			vPackageLocations[17] = <<1230.1204, 2742.6177, 37.0054>>
			vPackageLocations[18] = <<2545.2432, 2588.2490, 36.9449>>
			vPackageLocations[19] = <<260.4010, 3180.3582, 41.6992>>
		BREAK
									
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Check if a map quadrant is disabled, based on the corresponding tunable
/// RETURNS:
///    
FUNC BOOL IS_MAP_QUADRANT_DISABLED(INT iQuad)
	/*
	 0 // Bottom right		g_sMPTunables.bgb_sightseer_disable_quadrant_southeast 
	 1 // Bottom left		g_sMPTunables.bgb_sightseer_disable_quadrant_southwest 
	 2 // Top left			g_sMPTunables.bgb_sightseer_disable_quadrant_northwest
	 3 // Top right			g_sMPTunables.bgb_sightseer_disable_quadrant_northeast
	*/
	SWITCH iQuad
		CASE 0 		RETURN g_sMPTunables.bgb_sightseer_disable_quadrant_southeast 
		CASE 1 		RETURN g_sMPTunables.bgb_sightseer_disable_quadrant_southwest 
		CASE 2 		RETURN g_sMPTunables.bgb_sightseer_disable_quadrant_northwest 
		CASE 3 		RETURN g_sMPTunables.bgb_sightseer_disable_quadrant_northeast 
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_MAP_AREA()
//	serverBD.iMapArea = 0
	IF serverBD.iMapArea != -1
		RETURN serverBD.iMapArea
	ENDIF
	
	
	INT iArea = GET_RANDOM_INT_IN_RANGE(0, 4)
	
	
	IF NOT IS_MAP_QUADRANT_DISABLED(iArea)
		serverBD.iMapArea = iArea
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [GET_PACKAGE_LOCATIONS] [GET_MAP_AREA] serverBD.iMapArea = ", serverBD.iMapArea)
	
		INIT_PACKAGE_COORDS()
	ELSE
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [GET_PACKAGE_LOCATIONS] [GET_MAP_AREA] REJECTING AREA ", iArea, " AS QUADRANT DISABLED")
	ENDIF
	
	RETURN serverBD.iMapArea
ENDFUNC

FUNC BOOL IS_PACKAGE_LOCATION_VALID(INT index)
	
	IF ARE_VECTORS_EQUAL(vPackageLocations[index], <<0.0, 0.0, 0.0>>)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [GET_PACKAGE_LOCATIONS] REJECTING POINT ", index ," AT COORD ", vPackageLocations[index], " AS IS ZERO VECTOR")
		RETURN FALSE
	ENDIF
	
	INT i
	FLOAT fDist
	REPEAT MAX_PACKAGES i
		IF i < index	
			IF NOT ARE_VECTORS_EQUAL(serverBD.vPackageLoc[i], <<0.0, 0.0, 0.0>>)
				fDist = GET_DISTANCE_BETWEEN_COORDS(serverBD.vPackageLoc[i], vPackageLocations[index])
				IF fDist < GET_MIN_DIST_BETWEEN_PACKAGES()
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [GET_PACKAGE_LOCATIONS] REJECTING POINT ", index ," AT COORD ", vPackageLocations[index], " BECAUSE DISTANCE TO PACKAGE ", i, " IS ", fDist)
					RETURN FALSE
				ELSE	
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [GET_PACKAGE_LOCATIONS] POINT ", index ," AT COORD ", vPackageLocations[index], " IS OK, DIST TO PACKAGE ", i, " IS ", fDist) 
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC BOOL GET_PACKAGE_LOCATIONS()
	INT i
	INT iRandom
	
//	serverBD.vPackageLoc[0] = 	<<721.9799, 2314.4768, 49.4146>>
//	serverBD.vPackageLoc[1] = 	<<376.9765, 2628.4302, 43.6444>>
//	serverBD.vPackageLoc[2] = 	<<1535.2140, 6597.8652, 2.1543>>
	IF GET_MAP_AREA() != -1
		REPEAT MAX_PACKAGES i
			IF ARE_VECTORS_EQUAL(serverBD.vPackageLoc[i], << 0.0, 0.0, 0.0 >>)
				iRandom = GET_RANDOM_INT_IN_RANGE(0, MAX_PACKAGE_LOCATIONS)	
				IF IS_PACKAGE_LOCATION_VALID(iRandom)
					serverBD.vPackageLoc[i] = vPackageLocations[iRandom]
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [GET_PACKAGE_LOCATIONS] SETTING serverBD.vPackageLoc[", i, "] = vPackageLocations[", iRandom, "] = ", vPackageLocations[iRandom])
					vPackageLocations[iRandom] = << 0.0, 0.0, 0.0>>
				ENDIF
			ENDIF
		ENDREPEAT
	ELSE
		RETURN FALSE
	ENDIF
	
	i = 0
	REPEAT MAX_PACKAGES i
		IF ARE_VECTORS_EQUAL(serverBD.vPackageLoc[i], << 0.0, 0.0, 0.0 >>)
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
	i = 0
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [GET_PACKAGE_LOCATIONS]  ")
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [GET_PACKAGE_LOCATIONS]  GOT PACKAGE LOCATIONS....")
	REPEAT MAX_PACKAGES i
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - [GET_PACKAGE_LOCATIONS] serverBD.vPackageLoc[", i, "] = ", serverBD.vPackageLoc[i])
	ENDREPEAT
	#ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC VECTOR GET_PACKAGE_START_COORDS()
	RETURN serverBD.vPackageLoc[serverBD.iPackageLocToUse]
	
ENDFUNC

FUNC BOOL CREATE_PACKAGE()
	MODEL_NAMES mPackage = prop_ld_case_01
	SPAWN_SEARCH_PARAMS SpawnSearchParams
	VECTOR vCreate
	SpawnSearchParams.bConsiderInteriors = FALSE
	SpawnSearchParams.fMinDistFromPlayer = 10.0
	
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
		REQUEST_MODEL(mPackage)
		IF HAS_MODEL_LOADED(mPackage)
			IF CAN_REGISTER_MISSION_OBJECTS(1)
				vCreate = GET_PACKAGE_START_COORDS()
				IF GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY(vCreate, 5.0, vCreateEntityCurrentAttemptedCoords, fCreateEntityCurrentAttemptedHeading, SpawnSearchParams)
					
					//-- Change pickup type for 2599775 - Used to be PICKUP_PORTABLE_CRATE_FIXED_INCAR
					serverBD.niPackage = OBJ_TO_NET(CREATE_PORTABLE_PICKUP(PICKUP_PORTABLE_PACKAGE, vCreateEntityCurrentAttemptedCoords, TRUE, mPackage))
					
				//	NETWORK_FADE_IN_ENTITY(NET_TO_OBJ(serverBD.niPackage), TRUE)
					SET_ENTITY_COORDS_NO_OFFSET(NET_TO_OBJ(serverBD.niPackage), vCreateEntityCurrentAttemptedCoords+<< 0.0, 0.0, 0.5 >>)
					SET_MODEL_AS_NO_LONGER_NEEDED(mPackage)
					SET_ENTITY_INVINCIBLE(NET_TO_OBJ(serverBD.niPackage), TRUE)
				//	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_OBJ(serverBD.niPackage), TRUE)
					
					SET_OBJECT_FORCE_VEHICLES_TO_AVOID(NET_TO_OBJ(serverBD.niPackage), TRUE)
					SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(NET_TO_OBJ(serverBD.niPackage), TRUE)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_OBJ(serverBD.niPackage), FALSE)
					ACTIVATE_PHYSICS(NET_TO_OBJ(serverBD.niPackage))
					
					START_NET_TIMER(serverBD.timeResetAlpha)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [CREATE_PACKAGE] PACKAGE_CREATED AT ", vCreateEntityCurrentAttemptedCoords)
				ELSE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [CREATE_PACKAGE] WAITING FOR GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY")
				ENDIF
			ELSE
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [CREATE_PACKAGE] WAITING FOR CAN_REGISTER_MISSION_OBJECTS")
			ENDIF
		ELSE
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [CREATE_PACKAGE] WAITING FOR HAS_MODEL_LOADED")
		ENDIF
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Create package at a location based on how many times the player has completed the hacking MG
PROC MAINTAIN_PACKAGE_CREATION()

	IF serverBD.iBossPartWhoLaunched < 0
		EXIT
	ENDIF
	
	INT iPackageBitToCheck
	
	IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(serverBD.iBossPartWhoLaunched))
		IF playerBD[serverBD.iBossPartWhoLaunched].iNumCompletedHackingGames > 0
			iPackageBitToCheck = playerBD[serverBD.iBossPartWhoLaunched].iNumCompletedHackingGames - 1 
			IF NOT IS_BIT_SET(serverBD.iPackagesCreatedBitSet, iPackageBitToCheck)
				serverBD.iPackageLocToUse = iPackageBitToCheck
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [MAINTAIN_PACKAGE_CREATION] ATTEMPTING TO CREATE PACKAGE ", iPackageBitToCheck) 
				
				IF CREATE_PACKAGE()
					SET_BIT(serverBD.iPackagesCreatedBitSet, iPackageBitToCheck)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [MAINTAIN_PACKAGE_CREATION] CREATED PACKAGE ", iPackageBitToCheck)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

//PURPOSE: Process the stages for the Server
PROC PROCESS_SIGHTSEER_SERVER()

	
	SWITCH serverBD.eStage
		CASE eAD_COLLECT_PACKAGES
			
			
		
			MAINTAIN_PACKAGE_CREATION()
			
			MAINTAIN_MISSION_DURATION_SERVER()
			
			MAINTAIN_FORCE_RIVALS_ON_MISSION_SERVER()
			
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllPackagesCollected)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [PROCESS_SIGHTSEER_SERVER] serverBD.eStage eAD_COLLECT_PACKAGES -> eAD_OFF_MISSION AS biS_AllPackagesCollected")
				serverBD.eStage = eAD_OFF_MISSION 
			ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_DurationExpired)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [PROCESS_SIGHTSEER_SERVER] serverBD.eStage eAD_COLLECT_PACKAGES -> eAD_OFF_MISSION AS biS_DurationExpired")
				serverBD.eStage = eAD_OFF_MISSION 
			ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_BossKilled)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [PROCESS_SIGHTSEER_SERVER] serverBD.eStage eAD_COLLECT_PACKAGES -> eAD_OFF_MISSION AS biS_BossKilled")
				serverBD.eStage = eAD_OFF_MISSION 
			ENDIF
		BREAK

		
		CASE eAD_OFF_MISSION
		BREAK

		CASE eAD_CLEANUP
		BREAK
	ENDSWITCH
ENDPROC

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                      //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT(MP_MISSION_DATA missionScriptArgs)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
		#IF IS_DEBUG_BUILD	
			CPRINTLN(DEBUG_NET_MAGNATE, "MP: Starting mission ") NET_NL()
		#ENDIF
		
		// Vehry out all the initial game starting duties. 
		PROCESS_PRE_GAME(missionScriptArgs)	
		
		// Setup widgets. Make sure this is done after PROCESS_PRE_GAME()
		#IF IS_DEBUG_BUILD
			CREATE_WIDGETS()
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD NET_LOG("SIGHTSEER -  NETWORK_IS_GAME_IN_PROGRESS = FALSE")	#ENDIF
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  MISSION END - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP P     <----------     ") NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
	
	// Main loop
	WHILE TRUE

		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
				
		// If we have a match end event, bail
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			#IF IS_DEBUG_BUILD NET_LOG("SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")	#ENDIF
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  MISSION END - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE - SCRIPT CLEANUP B     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
//		ELIF GB_SHOULD_KILL_ACTIVE_BOSS_MISSION() 
//			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  MISSION END - GB_SHOULD_KILL_ACTIVE_BOSS_MISSION - SCRIPT CLEANUP <----------     ") NET_NL()
//			SCRIPT_CLEANUP()
		ENDIF
		
		//Check if player is in a tutorial session and end
//		IF NETWORK_IS_IN_TUTORIAL_SESSION()
//			#IF IS_DEBUG_BUILD NET_LOG("PLAYER IN TUT SESSION")	#ENDIF
//			NET_PRINT_TIME() NET_PRINT("     ---------->     SIGHTSEER -  MISSION END - PLAYER IN TUTORIAL SESSION - SCRIPT CLEANUP C     <----------     ") NET_NL()
//			SCRIPT_CLEANUP()
//		ENDIF
		
		// Deal with the debug
		#IF IS_DEBUG_BUILD		
			UPDATE_WIDGETS()
			
			MAINTAIN_DEBUG_KEYS()
		#ENDIF		
	
		// If we need to leave the mission, make sure we just straight to the end stage and terminate the script - DON'T NEED FOR AMBIENT MISSION
		/*IF SHOULD_PLAYER_LEAVE_MP_MISSION() 
			#IF IS_DEBUG_BUILD
			NET_PRINT_TIME() NET_PRINT( GET_MP_MISSION_NAME(thisMission), " Leave mission = TRUE \n")
			#ENDIF
			playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_LEAVE
			#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_LEAVE")	#ENDIF
		ENDIF*/
		
		g_sGb_Telemetry_data.sdata.m_match1 = serverBD.iMatchId1
		g_sGb_Telemetry_data.sdata.m_match2 = serverBD.iMatchId2
		
		// -----------------------------------
		// Process your game logic.....
		SWITCH GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT())
			
			// Wait untill the server gives the all go before moving on
			CASE GAME_STATE_INI
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					GET_MY_BOSS_PARTICPANT_ID()
					
					
					IF DID_MY_BOSS_LAUNCH_GB_SIGHTSEER()
						GB_COMMON_BOSS_MISSION_SETUP(FMMC_TYPE_GB_SIGHTSEER)
						SET_MAX_WANTED_LEVEL(3)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - SET_MAX_WANTED_LEVEL(3)")
						
						IF NOT GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
							SET_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_OF_TYPE(PLAYER_ID(), PICKUP_PORTABLE_PACKAGE, FALSE)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [MAINTAIN_SIGHTSEER_INTRO_SHARD] SET_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_OF_TYPE FALSE - GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG")
						ENDIF
					ELSE
						GB_COMMON_BOSS_MISSION_SETUP(FMMC_TYPE_GB_SIGHTSEER, FALSE)
						
//						SET_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_OF_TYPE(PLAYER_ID(), PICKUP_PORTABLE_PACKAGE, FALSE)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER [MAINTAIN_SIGHTSEER_INTRO_SHARD] SET_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_OF_TYPE FALSE - 23")
					ENDIF
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_RUNNING")	#ENDIF
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 1    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY")	#ENDIF
				ENDIF
			BREAK
			
			// Main gameplay state.
			CASE GAME_STATE_RUNNING
				IF GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION()
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_LEAVE
				ENDIF
			
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					PROCESS_SIGHTSEER_CLIENT()
					
					PROCESS_EVENTS()
					
					IF DID_MY_BOSS_LAUNCH_GB_SIGHTSEER()
					OR DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
						DRAW_CHALLENGE_DPAD_LBD(g_GBLeaderboardStruct.challengeLbdStruct, g_GBLeaderboardStruct.siDpadMovie, SUB_BOSS, g_GBLeaderboardStruct.dpadVariables, 
												g_GBLeaderboardStruct.fmDpadStruct)
					ENDIF
					
					MAINTAIN_DISTANCE_CHECKS_FOR_VARIATION()
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 2   <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY")	#ENDIF
				ENDIF
				
				MAINTAIN_GB_SIGHTSEER_MUSIC()
			BREAK
			
			CASE GAME_STATE_TERMINATE_DELAY 
				SET_MISSION_FINISHED(serverBD.MissionTerminateDelayTimer)
		      	IF IS_MISSION_READY_TO_CLEANUP(serverBD.MissionTerminateDelayTimer)
		            playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 3    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 3")	#ENDIF
		      	ENDIF
			BREAK
			
			// The game stage the local player is placed when we want him to leave a mission
			CASE GAME_STATE_LEAVE
				//LEAVE_MY_MP_MISSION(thisMission)
				playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 2    <----------     ") NET_NL()
				#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 2")	#ENDIF
			FALLTHRU
			
			//Cleans up then terminates the mission
			CASE GAME_STATE_END
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  SCRIPT CLEANUP A     <----------     ") NET_NL()
				SCRIPT_CLEANUP()
			BREAK

		ENDSWITCH
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			
			SWITCH GET_SERVER_MISSION_STATE()
				
				// For CnC group missions, we may want to wait for all players to have launched the script
				CASE GAME_STATE_INI
				//	IF CREATE_MAIN_VEHICLE()
					IF GET_PACKAGE_LOCATIONS()
						PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBD.iMatchId1, serverBD.iMatchId2)
						serverBD.iServerGameState = GAME_STATE_RUNNING
						serverBD.iBossPartWhoLaunched = PARTICIPANT_ID_TO_INT()
						serverBD.iBossPlayerWhoLaunched = NATIVE_TO_INT(PLAYER_ID())
						GB_COMMON_BOSS_MISSION_SETUP(FMMC_TYPE_GB_SIGHTSEER)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  serverBD.iServerGameState = GAME_STATE_RUNNING    <---------- ")
						#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_RUNNING")	#ENDIF
					ELSE
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - WAITING FOR GET_PACKAGE_LOCATIONS")
					ENDIF
				BREAK
				
				// Look for game end conditions
				CASE GAME_STATE_RUNNING
					
					MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
					
					PROCESS_SIGHTSEER_SERVER()
				
//					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPackage)
//						IF HAS_NET_TIMER_STARTED(serverBD.timeResetAlpha)
//							IF HAS_NET_TIMER_EXPIRED(serverBD.timeResetAlpha, 5000)
//								RESET_ENTITY_ALPHA(NET_TO_OBJ(serverBD.niPackage))
//								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER - RESET_ENTITY_ALPHA") 
//								RESET_NET_TIMER(serverBD.timeResetAlpha)
//							ENDIF
//						ENDIF
//					ENDIF
							
					#IF IS_DEBUG_BUILD
						IF bHostEndMissionNow
						OR MPGlobalsAmbience.bKillActiveEvent
							MPGlobalsAmbience.bKillActiveEvent = FALSE
							serverBD.iServerGameState = GAME_STATE_END
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  serverBD.iServerGameState = GAME_STATE_END 1    <----------     ") NET_NL()
							#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 1")	#ENDIF
						ENDIF
					#ENDIF	
					
					IF HAVE_MISSION_END_CONDITIONS_BEEN_MET()
						serverBD.iServerGameState = GAME_STATE_END
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  serverBD.iServerGameState = GAME_STATE_END 4    <----------     ") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 4")	#ENDIF
					ELIF GB_SHOULD_KILL_ACTIVE_BOSS_MISSION() 
						serverBD.iServerGameState = GAME_STATE_END
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     SIGHTSEER -  serverBD.iServerGameState = GAME_STATE_END 4  AS GB_SHOULD_KILL_ACTIVE_BOSS_MISSION  <----------     ") NET_NL()
					ENDIF
					
				BREAK
				
				CASE GAME_STATE_END
				BREAK
				
			ENDSWITCH
			
		ENDIF
		
	ENDWHILE
	
	
ENDSCRIPT
