USING "globals.sch"
USING "net_prints.sch"
USING "net_include.sch"
USING "commands_misc.sch"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Written by:  William Kennedy, Ryan Elliott & Martin McMillan.														//
// Date: 		25/05/2017																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////
/// MISSION ENTITY SPAWN COORDS ///
////////////////////////////////////

FUNC eFM_GANGOPS_ITEM_TYPE GET_GANGOPS_ITEM_TYPE_FOR_VARIATION(GANGOPS_VARIATION eVariation, BOOL bAlternateMissionCondition = FALSE)
	SWITCH eVariation
		CASE GOV_DEAD_DROP			RETURN eFHIT_DUFFEL_BAG_DD
		CASE GOV_FORCED_ENTRY		RETURN eFHIT_DUFFEL_BAG_FE
		CASE GOV_AUTO_SALVAGE		
			IF bAlternateMissionCondition
				RETURN eFHIT_WASTELANDER
			ENDIF
			RETURN eFHIT_FLATBED
		CASE GOV_CAR_COLLECTOR		RETURN eFHIT_DELUXO
		CASE GOV_MOB_MENTALITY		RETURN eFHIT_RIOT_TRUCK
		CASE GOV_PARAMEDIC			RETURN eFHIT_AMBULANCE
		CASE GOV_AQUALUNGS			RETURN eFHIT_DUFFEL_BAG_AQ
		CASE GOV_UNDER_CONTROL		RETURN eFHIT_STEALTH_HELI
		CASE GOV_BURGLARY_JOB		RETURN eFHIT_LAPTOP
		CASE GOV_FLARE_UP			RETURN eFHIT_STEALTH_GEAR
		CASE GOV_DAYLIGHT_ROBBERY	RETURN eFHIT_BRIEFCASE
		CASE GOV_FLIGHT_RECORDER	RETURN eFHIT_FLIGHT_RECORDER
		CASE GOV_GONE_BALLISTIC		RETURN eFHIT_CHERNOBOG
	ENDSWITCH
	
	RETURN eFHIT_INVALID
ENDFUNC

FUNC MODEL_NAMES GET_GANGOPS_ENTITY_MODEL(eFM_GANGOPS_ITEM_TYPE eItemType #IF IS_DEBUG_BUILD , BOOL bIgnoreAssert = FALSE #ENDIF )
	
	SWITCH eItemType
		CASE eFHIT_DUFFEL_BAG_DD				RETURN xm_prop_x17_bag_01a
		CASE eFHIT_DUFFEL_BAG_FE				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_bag_01b"))
		CASE eFHIT_DUFFEL_BAG_AQ				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_bag_01c"))
		CASE eFHIT_LAPTOP						RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xm_Prop_x17_LAP_TOP_01"))
		CASE eFHIT_FLATBED						RETURN FLATBED
		CASE eFHIT_DELUXO						RETURN DELUXO
		CASE eFHIT_RIOT_TRUCK					RETURN RIOT2
		CASE eFHIT_STEALTH_GEAR					RETURN RIOT
		CASE eFHIT_AMBULANCE					RETURN AMBULANCE
		CASE eFHIT_STEALTH_HELI					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("AKULA"))
		CASE eFHIT_BRIEFCASE					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_ld_case_01"))
		CASE eFHIT_WASTELANDER					RETURN WASTELANDER
		CASE eFHIT_FLIGHT_RECORDER				RETURN xm_prop_x17_Flight_Rec_01a
		CASE eFHIT_CHERNOBOG					RETURN CHERNOBOG
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	IF bIgnoreAssert
		SCRIPT_ASSERT("GET_GANGOPS_ENTITY_MODEL returning DUMMY_MODEL_FOR_SCRIPT - invalid data passed or mission not setup correctly")
	ENDIF
	#ENDIF
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC MODEL_NAMES GET_GANGOPS_ENTITY_MODEL_FROM_VARIATION(GANGOPS_VARIATION eVariation)
	RETURN GET_GANGOPS_ENTITY_MODEL(GET_GANGOPS_ITEM_TYPE_FOR_VARIATION(eVariation) #IF IS_DEBUG_BUILD , TRUE #ENDIF )
ENDFUNC

FUNC VECTOR GET_GANGOPS_ENTITY_SPAWN_COORDS(GANGOPS_VARIATION eVariation, GANGOPS_LOCATION eLocation, INT iMissionEntity, BOOL bForLaunchingChecks = FALSE)
	
	SWITCH eVariation
		CASE GOV_FLARE_UP						
			SWITCH eLocation
				CASE GangOpsLocation_FlareUp_Chamberlain_Hills			
					SWITCH iMissionEntity
						CASE 0			RETURN <<-318.6971, -1485.7524, 29.5487>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Morningwood
					SWITCH iMissionEntity
						CASE 0			RETURN <<-1440.7810, -262.0631, 45.2077>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_La_Puerta
					SWITCH iMissionEntity
						CASE 0			RETURN <<-514.7826, -1217.7290, 17.4336>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_El_burro
					SWITCH iMissionEntity
						CASE 0			RETURN <<1210.8304, -1391.0676, 34.2269>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Davis
					SWITCH iMissionEntity
						CASE 0			RETURN <<-59.9472, -1754.7018, 28.2206>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Sandy
					SWITCH iMissionEntity
						CASE 0			RETURN <<1996.4983, 3773.8975, 31.1808>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Paleto
					SWITCH iMissionEntity
						CASE 0			RETURN <<177.1732, 6616.2417, 30.8169>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Capital
					SWITCH iMissionEntity
						CASE 0			RETURN <<281.9708, -1269.2504, 28.2315>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Clinton
					SWITCH iMissionEntity
						CASE 0			RETURN <<633.4514, 256.5882, 102.0777>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Richman
					SWITCH iMissionEntity
						CASE 0			RETURN <<817.1460, -1038.5050, 25.5750>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE GOV_DEAD_DROP
			SWITCH eLocation				
				CASE GangOpsLocation_DeadDrop_Location1
					SWITCH iMissionEntity
						CASE 0			RETURN <<-1047.6469, -2724.3997, 12.7632>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DeadDrop_Location2
					SWITCH iMissionEntity
						CASE 0			RETURN <<-1028.3914, -2729.5454, 12.7632>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DeadDrop_Location3
					SWITCH iMissionEntity
						CASE 0			RETURN <<-1025.7341, -2743.2275, 12.7638>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DeadDrop_Location4
					SWITCH iMissionEntity
						CASE 0			RETURN <<-1024.2188, -2738.4873, 19.1759>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DeadDrop_Location5
					SWITCH iMissionEntity
						CASE 0			RETURN <<-1041.3860, -2729.5867, 19.1760>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DeadDrop_Location6
					SWITCH iMissionEntity
						CASE 0			RETURN <<-1047.9641, -2725.1045, 19.1760>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DeadDrop_Location7
					SWITCH iMissionEntity
						CASE 0			RETURN <<-533.3788, -1302.9424, 25.0389>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DeadDrop_Location8
					SWITCH iMissionEntity
						CASE 0			RETURN <<-536.5132, -1274.3457, 25.9132>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DeadDrop_Location9
					SWITCH iMissionEntity
						CASE 0			RETURN <<-543.2349, -1288.3101, 25.9130>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DeadDrop_Location10
					SWITCH iMissionEntity
						CASE 0			RETURN <<277.6671, -1204.7220, 37.9009>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DeadDrop_Location11
					SWITCH iMissionEntity
						CASE 0			RETURN <<299.6654, -1204.7434, 37.8992>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DeadDrop_Location12
					SWITCH iMissionEntity
						CASE 0			RETURN <<253.5355, -1206.7177, 28.2958>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DeadDrop_Location13
					SWITCH iMissionEntity
						CASE 0			RETURN <<286.2535, -1199.8542, 28.2978>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DeadDrop_Location14
					SWITCH iMissionEntity
						CASE 0			RETURN <<-216.5044, -1042.9503, 29.1474>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DeadDrop_Location15
					SWITCH iMissionEntity
						CASE 0			RETURN <<-210.7528, -1024.9828, 29.1475>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_FORCED_ENTRY
			SWITCH eLocation			
				CASE GangOpsLocation_ForcedEntry_Location_Fudge
					SWITCH iMissionEntity
						CASE 0			RETURN <<593.9870, 4747.3809, -59.9160>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Magellan
					SWITCH iMissionEntity
						CASE 0			RETURN <<602.7316, 4744.5913, -60.0020>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Forum
					SWITCH iMissionEntity
						CASE 0			RETURN <<600.6630, 4751.5508, -59.5590>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Roy_Lowenstein
					SWITCH iMissionEntity
						CASE 0			RETURN <<598.0420, 4750.7290, -59.5560>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Eastbourne
					SWITCH iMissionEntity
						CASE 0			RETURN <<605.9200, 4752.3110, -59.0330>>
					ENDSWITCH
				BREAK
				
				CASE GangOpsLocation_ForcedEntry_Location_V_Magellan
					SWITCH iMissionEntity
						CASE 0			RETURN <<598.8277, 4752.3271, -59.5730>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Alta
					SWITCH iMissionEntity
						CASE 0			RETURN <<596.3543, 4748.4478, -59.5503>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Forum_Drive
					SWITCH iMissionEntity
						CASE 0			RETURN <<601.6105, 4743.6577, -59.2896>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Spanish
					SWITCH iMissionEntity
						CASE 0			RETURN <<600.8610, 4746.5083, -59.5811>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Tahitian
					SWITCH iMissionEntity
						CASE 0			RETURN <<603.9465, 4752.4658, -59.0332>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_AUTO_SALVAGE
			SWITCH iMissionEntity
				CASE 0			RETURN <<154.4349, -1394.3147, 28.3049>>
				CASE 1			RETURN <<-1079.6779, -762.9799, 18.3532>>
				CASE 2			RETURN <<2544.9453, 1641.4114, 28.2342>>
				CASE 3			RETURN <<928.2330, 3538.2896, 33.0595>>
				CASE 4			RETURN <<140.0595, 1649.1329, 227.9944>>
				CASE 5			RETURN <<-1512.4373, 2150.0671, 54.9681>>
				CASE 6			RETURN <<360.6623, 137.3782, 102.1033>>
				CASE 7			RETURN <<2180.4883, 4748.8252, 40.1334>>
				CASE 8			RETURN <<-227.0002, -402.5585, 29.8592>>
				CASE 9			RETURN <<-2167.1389, -349.0966, 12.1762>>
			ENDSWITCH
		BREAK
		
		CASE GOV_CAR_COLLECTOR
			SWITCH iMissionEntity
				CASE 0			RETURN <<623.5459, 4751.0234, -60.0000>>
				CASE 1			RETURN <<634.4553, 4750.9209, -60.0000>>
				CASE 2			RETURN <<623.1746, 4754.3896, -60.0000>>
				CASE 3			RETURN <<630.6428, 4751.9131, -60.0000>>
			ENDSWITCH
		BREAK
		
		CASE GOV_MOB_MENTALITY
			SWITCH eLocation
				CASE GangOpsLocation_MobMentality_Morningwood
					SWITCH iMissionEntity
						CASE 0			RETURN <<-1309.4790, -318.1090, 36.0590>>
					ENDSWITCH
				BREAK	
				CASE GangOpsLocation_MobMentality_Grapeseed
					SWITCH iMissionEntity
						CASE 0			RETURN <<1999.4960, 4631.9121, 40.0270>>
					ENDSWITCH
				BREAK	
				CASE GangOpsLocation_MobMentality_Alta
					SWITCH iMissionEntity
						CASE 0			RETURN <<377.8470, -289.4810, 52.4382>>
					ENDSWITCH
				BREAK	
				CASE GangOpsLocation_MobMentality_MissionRow
					SWITCH iMissionEntity
						CASE 0			RETURN <<196.5589, -1360.7983, 28.2283>>
					ENDSWITCH
				BREAK	
				CASE GangOpsLocation_MobMentality_HippyCamp
					SWITCH iMissionEntity
						CASE 0			RETURN <<2439.7300, 2504.4619, 41.1000>>
					ENDSWITCH
				BREAK	
				CASE GangOpsLocation_MobMentality_SanAndreas
					SWITCH iMissionEntity
						CASE 0			RETURN <<-1260.0084, -1296.0114, 2.9503>>
					ENDSWITCH
				BREAK	
				CASE GangOpsLocation_MobMentality_MirrorPark
					SWITCH iMissionEntity
						CASE 0			RETURN <<1201.7253, -789.7348, 55.6508>>
					ENDSWITCH
				BREAK	
				CASE GangOpsLocation_MobMentality_Strawberry
					SWITCH iMissionEntity
						CASE 0			RETURN <<-71.3445, -1701.6957, 28.1263>>
					ENDSWITCH
				BREAK	
				CASE GangOpsLocation_MobMentality_MirrorParkTavern
					SWITCH iMissionEntity
						CASE 0			RETURN <<1228.8542, -530.1227, 66.2322>>
					ENDSWITCH
				BREAK	
				CASE GangOpsLocation_MobMentality_ULSA
					SWITCH iMissionEntity
						CASE 0			RETURN <<-1434.1749, 307.3669, 60.7799>>
					ENDSWITCH
				BREAK	
			ENDSWITCH
		BREAK
		
		CASE GOV_PARAMEDIC
			SWITCH eLocation
				CASE GangOpsLocation_Paramedic_Central_LS_Medical_Center
					SWITCH iMissionEntity
						CASE 0			RETURN <<401.7640, -1441.0909, 28.3770>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Pillbox_Hill_Medical_Center
					SWITCH iMissionEntity
						CASE 0			RETURN <<362.5320, -589.8670, 27.6740>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_ST_Fiacre_Hospital
					SWITCH iMissionEntity
						CASE 0			RETURN <<1156.0763, -1515.1456, 33.6926>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Sandy_Shores_Medical_Center
					SWITCH iMissionEntity
						CASE 0			RETURN <<-493.0043, -335.9916, 33.3720>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Paleto_Bay_Care_Center
					SWITCH iMissionEntity
						CASE 0			RETURN <<-239.6075, 6334.2710, 31.4025>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Del_Perro_Freeway
					SWITCH iMissionEntity
						CASE 0			RETURN <<-1495.1840, -764.5830, 10.2110>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Mount_Gordo
					SWITCH iMissionEntity
						CASE 0			RETURN <<1924.4030, 6291.7148, 41.4710>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Great_Chaparral
					SWITCH iMissionEntity
						CASE 0			RETURN <<-762.9950, 1703.9550, 199.9540>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Cassidy_Creek
					SWITCH iMissionEntity
						CASE 0			RETURN <<-899.4231, 4430.8774, 19.8059>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Power_Station
					SWITCH iMissionEntity
						CASE 0			RETURN <<-2421.2227, 1027.9628, 194.2694>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_AQUALUNGS
			SWITCH eLocation
				CASE GangOpsLocation_Aqualungs_Elysian_Island_1
					SWITCH iMissionEntity
						CASE 0			RETURN <<-517.9140, -3244.0969, 1.8109>>
						CASE 1			RETURN <<-518.7392, -3242.1265, 2.3897>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Paleto_Bay
					SWITCH iMissionEntity
						CASE 0			RETURN <<722.5775, 6992.1191, 1.4670>>
						CASE 1			RETURN <<724.0555, 6993.4731, 2.1040>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Elysian_Island_2
					SWITCH iMissionEntity
						CASE 0			RETURN <<780.2577, -3592.0364, 2.5770>>
						CASE 1			RETURN <<781.4731, -3593.6563, 3.1630>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Palomino_Highlands
					SWITCH iMissionEntity
						CASE 0			RETURN <<3055.2825, -1116.1404, 2.5540>>
						CASE 1			RETURN <<3054.5811, -1113.9380, 1.9970>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Chumash
					SWITCH iMissionEntity
						CASE 0			RETURN <<-3742.8835, 1314.5928, 2.3641>>
						CASE 1			RETURN <<-3742.1890, 1312.7640, 1.7870>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_San_Chianksi_Mountain_Range
					SWITCH iMissionEntity
						CASE 0			RETURN <<3989.2007, 5105.6113, 2.0339>>
						CASE 1			RETURN <<3990.7429, 5104.0718, 1.4770>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_PaletoForest
					SWITCH iMissionEntity
						CASE 0			RETURN <<-1525.6653, 5777.9985, 2.0100>>
						CASE 1			RETURN <<-1524.3187, 5776.5620, 1.4312>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_North_Chumash
					SWITCH iMissionEntity
						CASE 0			RETURN <<-2961.3882, 4414.4834, 2.0328>>
						CASE 1			RETURN <<-2959.3950, 4413.9922, 1.4570>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Pacific_Bluffs
					SWITCH iMissionEntity
						CASE 0			RETURN <<-2234.5986, -934.6196, 2.0509>> 
						CASE 1			RETURN <<-2234.2102, -932.4620, 1.4766>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Vespucci_Beach
					SWITCH iMissionEntity
						CASE 0			RETURN <<-1785.2294, -1758.2344, 2.1128>>
						CASE 1			RETURN <<-1783.4854, -1756.9006, 1.5266>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_UNDER_CONTROL
			SWITCH eLocation
				CASE GangOpsLocation_UnderControl_ServerRoom1
					SWITCH iMissionEntity
						CASE 0			RETURN <<-1181.1366, -2542.5422, 12.9452>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ServerRoom2
					SWITCH iMissionEntity
						CASE 0			RETURN <<-1903.8060, -3009.3911, 20.4380>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ServerRoom3
					SWITCH iMissionEntity
						CASE 0			RETURN <<-857.6380, -2953.1011, 12.9540>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ServerRoom4
					SWITCH iMissionEntity
						CASE 0			RETURN <<-1000.3260, -2379.3459, 12.9450>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ServerRoom5
					SWITCH iMissionEntity
						CASE 0			RETURN <<350.1672, -2534.6780, 4.7106>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ControlRoom1
					SWITCH iMissionEntity
						CASE 0			RETURN <<809.4970, -3203.5950, 4.9010>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ControlRoom2
					SWITCH iMissionEntity
						CASE 0			RETURN <<1191.5420, -2121.8191, 42.6684>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ControlRoom3
					SWITCH iMissionEntity
						CASE 0			RETURN <<1136.7440, -3253.5759, 4.9010>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ControlRoom4
					SWITCH iMissionEntity
						CASE 0			RETURN <<-46.8560, -2217.1233, 6.8117>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ControlRoom5
					SWITCH iMissionEntity
						CASE 0			RETURN <<296.1756, -2204.7478, 8.0365>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_GONE_BALLISTIC
			SWITCH eLocation
				CASE GangOpsLocation_GoneBallistic_ElysianIsland1
					SWITCH iMissionEntity
						CASE 0			RETURN <<-140.2833, -2565.4924, 5.0115>>
					ENDSWITCH
				BREAK	
				CASE GangOpsLocation_GoneBallistic_GrandSenoraDesert1
					SWITCH iMissionEntity
						CASE 0			RETURN <<1437.5228, 1522.2816, 110.8769>>
					ENDSWITCH
				BREAK	
				CASE GangOpsLocation_GoneBallistic_Grapeseed1
					SWITCH iMissionEntity
						CASE 0			RETURN <<2188.9905, 5016.0317, 41.6069>>
					ENDSWITCH
				BREAK	
				CASE GangOpsLocation_GoneBallistic_ElBurroHeights
					SWITCH iMissionEntity
						CASE 0			RETURN <<1118.0040, -2454.8989, 29.9470>>
					ENDSWITCH
				BREAK	
				CASE GangOpsLocation_GoneBallistic_Terminal
					SWITCH iMissionEntity
						CASE 0			RETURN <<1009.9137, -3195.9414, 4.9013>>
					ENDSWITCH
				BREAK	
				CASE GangOpsLocation_GoneBallistic_LSIA
					SWITCH iMissionEntity
						CASE 0			RETURN <<-1208.4019, -2063.1572, 12.9248>>
					ENDSWITCH
				BREAK	
				CASE GangOpsLocation_GoneBallistic_MurrietaHeights
					SWITCH iMissionEntity
						CASE 0			RETURN <<1278.0662, -1946.2949, 42.2405>>
					ENDSWITCH
				BREAK	
				CASE GangOpsLocation_GoneBallistic_ElysianIsland2
					SWITCH iMissionEntity
						CASE 0			RETURN <<-421.0339, -2727.4124, 5.0002>>
					ENDSWITCH
				BREAK	
				CASE GangOpsLocation_GoneBallistic_PaletoBay
					SWITCH iMissionEntity
						CASE 0			RETURN <<-430.7970, 6420.8555, 2.2329>>
					ENDSWITCH
				BREAK	
				CASE GangOpsLocation_GoneBallistic_FortZancudo
					SWITCH iMissionEntity
						CASE 0			RETURN <<-2760.1826, 3464.9370, 10.6327>>
					ENDSWITCH
				BREAK	
			ENDSWITCH
		BREAK
		
		CASE GOV_BURGLARY_JOB			RETURN <<584.6035, 4753.2319, -60.1962>>
		
		CASE GOV_FLIGHT_RECORDER	
			SWITCH eLocation
				CASE GangOpsLocation_FlightRecorder_Dam					RETURN <<3093.8779, 351.2879, -16.7804>>
				CASE GangOpsLocation_FlightRecorder_Palomino			RETURN <<2971.1575, -708.0753, -31.0175>>//<<3009.3845, -9.9455, -22.0449>>
				CASE GangOpsLocation_FlightRecorder_Terminal			RETURN <<1738.6970, -3035.3989, -57.3750>>
				CASE GangOpsLocation_FlightRecorder_PacificBluffs		RETURN <<-346.3149, -3112.0896, -33.1680>>
				CASE GangOpsLocation_FlightRecorder_MountGordo			RETURN <<3612.4580, 5548.0649, -28.3430>>
				CASE GangOpsLocation_FlightRecorder_AlamoSea1			RETURN <<1138.9456, 3962.2124, -12.9843>>
				CASE GangOpsLocation_FlightRecorder_AlamoSea2			RETURN <<2100.9067, 4256.7627, -12.6673>>
				CASE GangOpsLocation_FlightRecorder_AlamoSea3			RETURN <<99.3927, 3958.5125, 1.4494>>
				CASE GangOpsLocation_FlightRecorder_PaletoBay			RETURN <<1217.2694, 6862.8237, -26.7036>>
				CASE GangOpsLocation_FlightRecorder_FortZancudo			RETURN <<-3379.5195, 3678.3296, -86.8678>>
			ENDSWITCH
		BREAK
	ENDSWITCH

	IF NOT bForLaunchingChecks
		SCRIPT_ASSERT("GET_GANGOPS_ENTITY_SPAWN_COORDS - Invalid location passed! Further details in logs.")
		PRINTLN("[FM_GANGOPS] GET_GANGOPS_ENTITY_SPAWN_COORDS - Invalid location passed - iMissionEntity: ", iMissionEntity, " - eLocation: ", GET_GANGOPS_LOCATION_NAME(eLocation))
	ENDIF
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC FLOAT GET_GANGOPS_ENTITY_SPAWN_HEADING(GANGOPS_VARIATION eVariation, GANGOPS_LOCATION eLocation, INT iMissionEntity, BOOL bForLaunchingChecks = FALSE)

	SWITCH eVariation
		CASE GOV_FLARE_UP						
			SWITCH eLocation
				CASE GangOpsLocation_FlareUp_Chamberlain_Hills			
					SWITCH iMissionEntity
						CASE 0			RETURN 84.5990
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Morningwood
					SWITCH iMissionEntity
						CASE 0			RETURN 305.9987
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_La_Puerta
					SWITCH iMissionEntity
						CASE 0			RETURN 155.1988
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_El_burro
					SWITCH iMissionEntity
						CASE 0			RETURN 80.3989
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Davis
					SWITCH iMissionEntity
						CASE 0			RETURN 222.5989
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Sandy
					SWITCH iMissionEntity
						CASE 0			RETURN 12.9989
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Paleto
					SWITCH iMissionEntity
						CASE 0			RETURN 272.7986
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Capital
					SWITCH iMissionEntity
						CASE 0			RETURN 312.1999
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Clinton
					SWITCH iMissionEntity
						CASE 0			RETURN 289.9997
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Richman
					SWITCH iMissionEntity
						CASE 0			RETURN 275.1980
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE GOV_DEAD_DROP
			SWITCH eLocation				
				CASE GangOpsLocation_DeadDrop_Location1
					SWITCH iMissionEntity
						CASE 0			RETURN 213.6472
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DeadDrop_Location2
					SWITCH iMissionEntity
						CASE 0			RETURN 346.8000
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DeadDrop_Location3
					SWITCH iMissionEntity
						CASE 0			RETURN 17.5999
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DeadDrop_Location4
					SWITCH iMissionEntity
						CASE 0			RETURN 15.1999
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DeadDrop_Location5
					SWITCH iMissionEntity
						CASE 0			RETURN 359.2250
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DeadDrop_Location6
					SWITCH iMissionEntity
						CASE 0			RETURN 330.8029
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DeadDrop_Location7
					SWITCH iMissionEntity
						CASE 0			RETURN 336.5998
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DeadDrop_Location8
					SWITCH iMissionEntity
						CASE 0			RETURN -23.4002
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DeadDrop_Location9
					SWITCH iMissionEntity
						CASE 0			RETURN -23.4000
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DeadDrop_Location10
					SWITCH iMissionEntity
						CASE 0			RETURN 271.5996
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DeadDrop_Location11
					SWITCH iMissionEntity
						CASE 0			RETURN -88.4004
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DeadDrop_Location12
					SWITCH iMissionEntity
						CASE 0			RETURN 178.5995
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DeadDrop_Location13
					SWITCH iMissionEntity
						CASE 0			RETURN 88.1992
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DeadDrop_Location14
					SWITCH iMissionEntity
						CASE 0			RETURN 339.3990
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DeadDrop_Location15
					SWITCH iMissionEntity
						CASE 0			RETURN 338.7990
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_FORCED_ENTRY
			SWITCH eLocation
				CASE GangOpsLocation_ForcedEntry_Location_Fudge
					SWITCH iMissionEntity
						CASE 0			RETURN -115.6010
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Magellan
					SWITCH iMissionEntity
						CASE 0			RETURN -120.8010
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Forum
					SWITCH iMissionEntity
						CASE 0			RETURN -72.8010
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Roy_Lowenstein
					SWITCH iMissionEntity
						CASE 0			RETURN -172.0010
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Eastbourne
					SWITCH iMissionEntity
						CASE 0			 RETURN 132.3990
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_V_Magellan
					SWITCH iMissionEntity
						CASE 0			RETURN 0.0000
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Alta
					SWITCH iMissionEntity
						CASE 0			RETURN 3.9990
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Forum_Drive
					SWITCH iMissionEntity
						CASE 0			RETURN -29.4010
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Spanish
					SWITCH iMissionEntity
						CASE 0			RETURN -88.8009
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Tahitian
					SWITCH iMissionEntity
						CASE 0			RETURN 357.3989
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_AUTO_SALVAGE
			SWITCH iMissionEntity
				CASE 0			RETURN 0.0
				CASE 1			RETURN 0.0
				CASE 2			RETURN 0.0
				CASE 3			RETURN 0.0
				CASE 4			RETURN 0.0
				CASE 5			RETURN 0.0
				CASE 6			RETURN 0.0
				CASE 7			RETURN 0.0
				CASE 8			RETURN 0.0
				CASE 9			RETURN 0.0
			ENDSWITCH
		BREAK
		
		CASE GOV_CAR_COLLECTOR
			SWITCH iMissionEntity
				CASE 0				RETURN 268.0811
				CASE 1				RETURN 181.1828
				CASE 2				RETURN 81.9227
				CASE 3				RETURN 13.8157
			ENDSWITCH
		BREAK
		
		CASE GOV_MOB_MENTALITY
			SWITCH eLocation
				CASE GangOpsLocation_MobMentality_Morningwood
					SWITCH iMissionEntity
						CASE 0			RETURN 31.3990
					ENDSWITCH
				BREAK	
				CASE GangOpsLocation_MobMentality_Grapeseed
					SWITCH iMissionEntity
						CASE 0			RETURN 145.5970
					ENDSWITCH
				BREAK	
				CASE GangOpsLocation_MobMentality_Alta
					SWITCH iMissionEntity
						CASE 0			RETURN 248.1978
					ENDSWITCH
				BREAK	
				CASE GangOpsLocation_MobMentality_MissionRow
					SWITCH iMissionEntity
						CASE 0			RETURN 325.1987
					ENDSWITCH
				BREAK	
				CASE GangOpsLocation_MobMentality_HippyCamp
					SWITCH iMissionEntity
						CASE 0			RETURN 65.7990
					ENDSWITCH
				BREAK	
				CASE GangOpsLocation_MobMentality_SanAndreas
					SWITCH iMissionEntity
						CASE 0			RETURN 199.7995
					ENDSWITCH
				BREAK	
				CASE GangOpsLocation_MobMentality_MirrorPark
					SWITCH iMissionEntity
						CASE 0			RETURN 345.3997
					ENDSWITCH
				BREAK	
				CASE GangOpsLocation_MobMentality_Strawberry
					SWITCH iMissionEntity
						CASE 0			RETURN 40.7999
					ENDSWITCH
				BREAK	
				CASE GangOpsLocation_MobMentality_MirrorParkTavern
					SWITCH iMissionEntity
						CASE 0			RETURN 48.6000
					ENDSWITCH
				BREAK	
				CASE GangOpsLocation_MobMentality_ULSA
					SWITCH iMissionEntity
						CASE 0			RETURN 154.3992
					ENDSWITCH
				BREAK	
			ENDSWITCH
		BREAK
			
		CASE GOV_PARAMEDIC
			SWITCH eLocation
				CASE GangOpsLocation_Paramedic_Central_LS_Medical_Center
					SWITCH iMissionEntity
						CASE 0			RETURN 210.7990
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Pillbox_Hill_Medical_Center
					SWITCH iMissionEntity
						CASE 0			RETURN 160.7980
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_ST_Fiacre_Hospital
					SWITCH iMissionEntity
						CASE 0			RETURN 91.5978
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Sandy_Shores_Medical_Center
					SWITCH iMissionEntity
						CASE 0			RETURN 172.9993
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Paleto_Bay_Care_Center
					SWITCH iMissionEntity
						CASE 0			RETURN 225.9959
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Del_Perro_Freeway
					SWITCH iMissionEntity
						CASE 0			RETURN 86.9960
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Mount_Gordo
					SWITCH iMissionEntity
						CASE 0			RETURN 215.9950
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Great_Chaparral
					SWITCH iMissionEntity
						CASE 0			RETURN 165.7950
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Cassidy_Creek
					SWITCH iMissionEntity
						CASE 0			RETURN 109.5937
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Power_Station
					SWITCH iMissionEntity
						CASE 0			RETURN 262.5907
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_AQUALUNGS
			SWITCH eLocation
				CASE GangOpsLocation_Aqualungs_Elysian_Island_1
					SWITCH iMissionEntity
						CASE 0			RETURN 337.7957
						CASE 1			RETURN 244.7956
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Paleto_Bay
					SWITCH iMissionEntity
						CASE 0			RETURN 0.0000
						CASE 1			RETURN 0.0000
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Elysian_Island_2
					SWITCH iMissionEntity
						CASE 0			RETURN 0.0000
						CASE 1			RETURN 0.0000
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Palomino_Highlands
					SWITCH iMissionEntity
						CASE 0			RETURN 159.3997
						CASE 1			RETURN 74.5970
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Chumash
					SWITCH iMissionEntity
						CASE 0			RETURN 360.0000
						CASE 1			RETURN 62.1970
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_San_Chianksi_Mountain_Range
					SWITCH iMissionEntity
						CASE 0			RETURN 86.3996
						CASE 1			RETURN 174.7960
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_PaletoForest
					SWITCH iMissionEntity
						CASE 0			RETURN 61.5001
						CASE 1			RETURN 335.0000
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_North_Chumash
					SWITCH iMissionEntity
						CASE 0			RETURN 118.5997
						CASE 1			RETURN 25.5960 
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Pacific_Bluffs
					SWITCH iMissionEntity
						CASE 0			RETURN 129.9996
						CASE 1			RETURN 223.5964
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Vespucci_Beach
					SWITCH iMissionEntity
						CASE 0			RETURN 167.5998 
						CASE 1			RETURN 257.5956
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_UNDER_CONTROL
			SWITCH eLocation
				CASE GangOpsLocation_UnderControl_ServerRoom1
					SWITCH iMissionEntity
						CASE 0			RETURN 168.3980
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ServerRoom2
					SWITCH iMissionEntity
						CASE 0			RETURN 183.6000
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ServerRoom3
					SWITCH iMissionEntity
						CASE 0			RETURN 181.4000
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ServerRoom4
					SWITCH iMissionEntity
						CASE 0			RETURN 186.8000
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ServerRoom5
					SWITCH iMissionEntity
						CASE 0			RETURN 273.7982
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ControlRoom1
					SWITCH iMissionEntity
						CASE 0			RETURN 159.7990
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ControlRoom2
					SWITCH iMissionEntity
						CASE 0			RETURN 43.7992
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ControlRoom3
					SWITCH iMissionEntity
						CASE 0			RETURN 325.2000
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ControlRoom4
					SWITCH iMissionEntity
						CASE 0			RETURN 181.1994
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ControlRoom5
					SWITCH iMissionEntity
						CASE 0			RETURN 63.6982
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_GONE_BALLISTIC
			SWITCH eLocation
				CASE GangOpsLocation_GoneBallistic_ElysianIsland1
					SWITCH iMissionEntity
						CASE 0			RETURN 53.3999
					ENDSWITCH
				BREAK	
				CASE GangOpsLocation_GoneBallistic_GrandSenoraDesert1
					SWITCH iMissionEntity
						CASE 0			RETURN 67.5999
					ENDSWITCH
				BREAK	
				CASE GangOpsLocation_GoneBallistic_Grapeseed1
					SWITCH iMissionEntity
						CASE 0			RETURN 75.2758
					ENDSWITCH
				BREAK	
				CASE GangOpsLocation_GoneBallistic_ElBurroHeights
					SWITCH iMissionEntity
						CASE 0			RETURN 115.0720
					ENDSWITCH
				BREAK	
				CASE GangOpsLocation_GoneBallistic_Terminal
					SWITCH iMissionEntity
						CASE 0			RETURN 240.9997
					ENDSWITCH
				BREAK	
				CASE GangOpsLocation_GoneBallistic_LSIA
					SWITCH iMissionEntity
						CASE 0			RETURN 354.1165
					ENDSWITCH
				BREAK	
				CASE GangOpsLocation_GoneBallistic_MurrietaHeights
					SWITCH iMissionEntity
						CASE 0			RETURN 294.1988
					ENDSWITCH
				BREAK	
				CASE GangOpsLocation_GoneBallistic_ElysianIsland2
					SWITCH iMissionEntity
						CASE 0			RETURN 282.5978
					ENDSWITCH
				BREAK	
				CASE GangOpsLocation_GoneBallistic_PaletoBay
					SWITCH iMissionEntity
						CASE 0			RETURN 353.2010
					ENDSWITCH
				BREAK	
				CASE GangOpsLocation_GoneBallistic_FortZancudo
					SWITCH iMissionEntity
						CASE 0			RETURN 358.6488
					ENDSWITCH
				BREAK	
			ENDSWITCH
		BREAK
		
		CASE GOV_BURGLARY_JOB			RETURN 359.5966
		CASE GOV_FLIGHT_RECORDER		RETURN 0.0
	ENDSWITCH
	
	
	
	IF NOT bForLaunchingChecks
		SCRIPT_ASSERT("GET_GANGOPS_ENTITY_SPAWN_HEADING - Invalid location passed! Further details in logs.")
		PRINTLN("[FM_GANGOPS] GET_GANGOPS_ENTITY_SPAWN_HEADING - Invalid location passed - iMissionEntity: ", iMissionEntity, " - eLocation: ", GET_GANGOPS_LOCATION_NAME(eLocation))
	ENDIF
	RETURN 0.0
	
ENDFUNC

FUNC VECTOR GET_GANGOPS_ENTITY_PICKUP_ROTATION(INT iMissionEntity, GANGOPS_LOCATION eLocation)

	SWITCH eLocation
	
		// Dead Drop					- Martin 				- Package
		CASE GangOpsLocation_DeadDrop_Location1
			SWITCH iMissionEntity
				CASE 0			RETURN <<0.0000, -0.0000, -146.3528>>
			ENDSWITCH
		BREAK
		CASE GangOpsLocation_DeadDrop_Location2
			SWITCH iMissionEntity
				CASE 0			RETURN <<0.0000, 0.0000, -13.2000>>
			ENDSWITCH
		BREAK
		CASE GangOpsLocation_DeadDrop_Location3
			SWITCH iMissionEntity
				CASE 0			RETURN <<0.0000, 0.0000, 17.5999>>
			ENDSWITCH
		BREAK
		CASE GangOpsLocation_DeadDrop_Location4
			SWITCH iMissionEntity
				CASE 0			RETURN <<0.0000, 0.0000, 15.1999>>
			ENDSWITCH
		BREAK
		CASE GangOpsLocation_DeadDrop_Location5
			SWITCH iMissionEntity
				CASE 0			RETURN <<0.0000, 0.0000, -0.7750>>
			ENDSWITCH
		BREAK
		CASE GangOpsLocation_DeadDrop_Location6
			SWITCH iMissionEntity
				CASE 0			RETURN <<0.0000, 0.0000, -29.1971>>
			ENDSWITCH
		BREAK
		CASE GangOpsLocation_DeadDrop_Location7
			SWITCH iMissionEntity
				CASE 0			RETURN <<0.0000, 0.0000, -23.4002>>
			ENDSWITCH
		BREAK
		CASE GangOpsLocation_DeadDrop_Location8
			SWITCH iMissionEntity
				CASE 0			RETURN <<0.0000, 0.0000, -23.4002>>
			ENDSWITCH
		BREAK
		CASE GangOpsLocation_DeadDrop_Location9
			SWITCH iMissionEntity
				CASE 0			RETURN <<0.0000, 0.0000, -23.4000>>
			ENDSWITCH
		BREAK
		CASE GangOpsLocation_DeadDrop_Location10
			SWITCH iMissionEntity
				CASE 0			RETURN <<0.0000, 0.0000, -88.4004>>
			ENDSWITCH
		BREAK
		CASE GangOpsLocation_DeadDrop_Location11
			SWITCH iMissionEntity
				CASE 0			RETURN <<0.0000, 0.0000, -88.4004>>
			ENDSWITCH
		BREAK
		CASE GangOpsLocation_DeadDrop_Location12
			SWITCH iMissionEntity
				CASE 0			RETURN <<0.0000, -0.0000, 178.5995>>
			ENDSWITCH
		BREAK
		CASE GangOpsLocation_DeadDrop_Location13
			SWITCH iMissionEntity
				CASE 0			RETURN <<0.0000, 0.0000, 88.1992>>
			ENDSWITCH
		BREAK
		CASE GangOpsLocation_DeadDrop_Location14
			SWITCH iMissionEntity
				CASE 0			RETURN <<0.0000, 0.0000, -20.6010>>
			ENDSWITCH
		BREAK
		CASE GangOpsLocation_DeadDrop_Location15
			SWITCH iMissionEntity
				CASE 0			RETURN <<0.0000, 0.0000, -21.2009>>
			ENDSWITCH
		BREAK
		
		CASE GangOpsLocation_ForcedEntry_Location_Fudge
			SWITCH iMissionEntity
				CASE 0			RETURN <<0.0000, 0.0000, -166.0010>>
			ENDSWITCH
		BREAK
		CASE GangOpsLocation_ForcedEntry_Location_Magellan
			SWITCH iMissionEntity
				CASE 0			RETURN <<-0.0000, -0.0000, -88.8009>>
			ENDSWITCH
		BREAK
		CASE GangOpsLocation_ForcedEntry_Location_Forum
			SWITCH iMissionEntity
				CASE 0			RETURN <<0.0000, 0.0000, 136.3990>>
			ENDSWITCH
		BREAK
		CASE GangOpsLocation_ForcedEntry_Location_Roy_Lowenstein
			SWITCH iMissionEntity
				CASE 0			RETURN <<0.0000, 0.0000, -174.0010>>
			ENDSWITCH
		BREAK
		CASE GangOpsLocation_ForcedEntry_Location_Eastbourne
			SWITCH iMissionEntity
				CASE 0			RETURN <<0.0000, 0.0000, 111.9990>>
			ENDSWITCH
		BREAK
		
		CASE GangOpsLocation_ForcedEntry_Location_V_Magellan
			SWITCH iMissionEntity
				CASE 0			RETURN <<0.0000, 0.0000, 3.9990>>
			ENDSWITCH
		BREAK
		CASE GangOpsLocation_ForcedEntry_Location_Alta
			SWITCH iMissionEntity
				CASE 0			RETURN <<-0.0000, -0.0000, -29.4010>>
			ENDSWITCH
		BREAK
		CASE GangOpsLocation_ForcedEntry_Location_Forum_Drive
			SWITCH iMissionEntity
				CASE 0			RETURN <<0.0000, -0.0000, -106.4010>>
			ENDSWITCH
		BREAK
		CASE GangOpsLocation_ForcedEntry_Location_Spanish
			SWITCH iMissionEntity
				CASE 0			RETURN <<0.0000, -0.0000, -170.0009>>
			ENDSWITCH
		BREAK
		CASE GangOpsLocation_ForcedEntry_Location_Tahitian
			SWITCH iMissionEntity
				CASE 0			RETURN <<0.0000, 0.0000, 5.1991>>
			ENDSWITCH
		BREAK
		
		// Aqualungs					- Steve					- Pickup
		CASE GangOpsLocation_Aqualungs_Elysian_Island_1
			SWITCH iMissionEntity
				CASE 0			RETURN <<0.0000, 0.0000, -22.2043>>
				CASE 1			RETURN <<0.0000, -0.0000, -115.2044>>
			ENDSWITCH
		BREAK
		CASE GangOpsLocation_Aqualungs_Paleto_Bay
			SWITCH iMissionEntity
				CASE 0			RETURN <<1.2752, 0.5785, -93.4148>>
				CASE 1			RETURN <<1.2752, 0.5785, -1.6148>>
			ENDSWITCH
		BREAK
		CASE GangOpsLocation_Aqualungs_Elysian_Island_2
			SWITCH iMissionEntity
				CASE 0			RETURN <<0.0000, 0.0000, 84.9990>>
				CASE 1			RETURN <<0.0000, -0.0000, 174.7990>>
			ENDSWITCH
		BREAK
		CASE GangOpsLocation_Aqualungs_Palomino_Highlands
			SWITCH iMissionEntity
				CASE 0			RETURN <<0.0000, -0.0000, 159.3997>>
				CASE 1			RETURN <<0.0000, 0.0000, 74.5970>>
			ENDSWITCH
		BREAK
		CASE GangOpsLocation_Aqualungs_Chumash
			SWITCH iMissionEntity
				CASE 0			RETURN <<0.0000, 0.0000, 0.0000>>
				CASE 1			RETURN <<0.0000, 0.0000, 62.1970>>
			ENDSWITCH
		BREAK
		CASE GangOpsLocation_Aqualungs_San_Chianksi_Mountain_Range
			SWITCH iMissionEntity
				CASE 0			RETURN <<0.0000, 0.0000, 86.3995>>
				CASE 1			RETURN <<0.0000, 0.0000, 174.7960>>
			ENDSWITCH
		BREAK
		CASE GangOpsLocation_Aqualungs_PaletoForest
			SWITCH iMissionEntity
				CASE 0			RETURN <<0.0000, 0.0000, 61.5001>>
				CASE 1			RETURN <<0.0000, 0.0000, -25.0000>>
			ENDSWITCH
		BREAK
		CASE GangOpsLocation_Aqualungs_North_Chumash
			SWITCH iMissionEntity
				CASE 0			RETURN <<0.0000, -0.0000, 118.5996>>
				CASE 1			RETURN <<0.0000, 0.0000, 25.5960>>
			ENDSWITCH
		BREAK
		CASE GangOpsLocation_Aqualungs_Pacific_Bluffs
			SWITCH iMissionEntity
				CASE 0			RETURN <<0.0000, -0.0000, 129.9996>>
				CASE 1			RETURN <<0.0000, -0.0000, -136.4036>>
			ENDSWITCH
		BREAK
		CASE GangOpsLocation_Aqualungs_Vespucci_Beach
			SWITCH iMissionEntity
				CASE 0			RETURN <<0.0000, -0.0000, 167.5998>>
				CASE 1			RETURN <<0.0000, -0.0000, -102.4044>>
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

/////////////////////////////////////////////////
/// 	VARIATION VEHICLE SPAWN COORDS 			///
/////////////////////////////////////////////////

FUNC VECTOR GET_GANGOPS_VEHICLE_SPAWN_COORDS(GANGOPS_VARIATION eVariation, GANGOPS_LOCATION eLocation, INT iVehicle, FREEMODE_DELIVERY_DROPOFFS eDropOff = FREEMODE_DELIVERY_DROPOFF_INVALID, INT iExtraParam = -1, INT iRespawnLocation = 0)

	UNUSED_PARAMETER(eDropOff)
	UNUSED_PARAMETER(iRespawnLocation)

	SWITCH eVariation
	
		CASE GOV_FLARE_UP						
			SWITCH eLocation
				CASE GangOpsLocation_FlareUp_Chamberlain_Hills			
					SWITCH iVehicle
						CASE 0			RETURN <<-323.3124, -1457.7744, 29.5933>>
						CASE 1			RETURN <<-331.6465, -1463.1041, 29.5064>>
						CASE 2			RETURN <<-310.1210, -1481.7850, 29.5010>>
						CASE 3			RETURN <<-306.9440, -1494.4041, 29.2870>>
						CASE 4			RETURN <<-316.5618, -1468.1219, 29.5469>>
						CASE 5			RETURN <<-322.9952, -1475.7435, 29.5487>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Morningwood
					SWITCH iVehicle
						CASE 0			RETURN <<-1433.8445, -280.4124, 45.2077>>
						CASE 1			RETURN <<-1438.7719, -273.1184, 45.2077>>
						CASE 2			RETURN <<-1424.5968, -282.2938, 45.2077>>
						CASE 3			RETURN <<-1429.4980, -288.4810, 45.1833>>
						CASE 4			RETURN <<-1451.7222, -268.5485, 45.6377>>
						CASE 5			RETURN <<-1445.4575, -269.1100, 45.2132>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_La_Puerta
					SWITCH iVehicle
						CASE 0			RETURN <<-530.2459, -1200.6713, 17.2376>>
						CASE 1			RETURN <<-535.8429, -1207.3319, 17.1950>>
						CASE 2			RETURN <<-520.4943, -1203.4058, 17.3761>>
						CASE 3			RETURN <<-513.6445, -1207.6282, 17.4641>>
						CASE 4			RETURN <<-527.1280, -1218.6699, 17.2545>>
						CASE 5			RETURN <<-527.7153, -1210.3025, 17.1848>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_El_burro
					SWITCH iVehicle
						CASE 0			RETURN <<1217.9573, -1399.7089, 34.2165>>
						CASE 1			RETURN <<1218.5020, -1392.9012, 34.2045>>
						CASE 2			RETURN <<1200.4280, -1404.7452, 34.2241>>
						CASE 3			RETURN <<1205.0981, -1410.8114, 34.2250>>
						CASE 4			RETURN <<1208.7640, -1402.2343, 34.2242>>
						CASE 5			RETURN <<1202.3488, -1397.1276, 34.2255>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Davis
					SWITCH iVehicle
						CASE 0			RETURN <<-57.3450, -1764.9810, 27.9750>>
						CASE 1			RETURN <<-73.7258, -1769.8944, 28.0963>>
						CASE 2			RETURN <<-65.8147, -1772.9546, 27.9652>>
						CASE 3			RETURN <<-70.5990, -1753.9993, 28.4759>>
						CASE 4			RETURN <<-66.1051, -1749.8290, 28.4022>>
						CASE 5			RETURN <<-66.2130, -1760.8318, 28.2730>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Sandy
					SWITCH iVehicle
						CASE 0			RETURN <<2009.2491, 3771.5732, 31.1808>>
						CASE 1			RETURN <<2003.4694, 3777.3091, 31.1808>>
						CASE 2			RETURN <<1997.1168, 3767.1155, 31.1808>>
						CASE 3			RETURN <<2003.6600, 3766.9534, 31.1808>>
						CASE 4			RETURN <<2015.3091, 3777.2300, 31.1808>>
						CASE 5			RETURN <<2010.6725, 3782.4724, 31.1808>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Paleto
					SWITCH iVehicle
						CASE 0			RETURN <<186.7755, 6596.1733, 30.8444>>
						CASE 1			RETURN <<180.1303, 6593.4810, 30.8486>>
						CASE 2			RETURN <<183.6112, 6615.1631, 30.8202>>
						CASE 3			RETURN <<170.0681, 6613.3105, 30.8791>>
						CASE 4			RETURN <<176.8117, 6600.1436, 30.8492>>
						CASE 5			RETURN <<183.5503, 6604.4702, 30.8486>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Capital
					SWITCH iVehicle
						CASE 0			RETURN <<275.5028, -1274.6195, 28.2109>>
						CASE 1			RETURN <<269.4347, -1270.8610, 28.1606>>
						CASE 2			RETURN <<276.7648, -1249.2980, 28.1762>>
						CASE 3			RETURN <<269.8455, -1249.8101, 28.1467>>
						CASE 4			RETURN <<279.4228, -1260.9625, 28.1989>>
						CASE 5			RETURN <<269.5532, -1257.7421, 28.1429>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Clinton
					SWITCH iVehicle
						CASE 0			RETURN <<625.4030, 270.1370, 102.0890>>
						CASE 1			RETURN <<633.8670, 265.1630, 102.0890>>
						CASE 2			RETURN <<622.9936, 258.9009, 102.0514>>
						CASE 3			RETURN <<626.0742, 253.9614, 102.0354>>
						CASE 4			RETURN <<634.6985, 276.8108, 102.0894>>
						CASE 5			RETURN <<627.2611, 278.9620, 102.0894>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Richman
					SWITCH iVehicle					
						CASE 0			RETURN <<814.4340, -1030.0530, 25.2820>>
						CASE 1			RETURN <<823.0990, -1029.1210, 25.2610>>
						CASE 2			RETURN <<825.6860, -1020.8700, 25.3490>>
						CASE 3			RETURN <<816.9790, -1021.6340, 25.2180>>
						CASE 4			RETURN <<825.5140, -1036.5010, 25.5740>>
						CASE 5			RETURN <<810.3090, -1037.1080, 25.5070>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	
		CASE GOV_FORCED_ENTRY						
			SWITCH eLocation
				CASE GangOpsLocation_ForcedEntry_Location_Fudge				
					SWITCH iVehicle
						CASE 0			RETURN <<1526.7410, -2470.0891, 60.7080>>
						CASE 1			RETURN <<1528.8459, -2474.2600, 60.2110>>
						CASE 2			RETURN <<1319.5270, -1560.3380, 49.8270>>
						CASE 3			RETURN <<1106.2150, -944.0190, 46.7420>>
						CASE 4			RETURN <<853.9520, -2094.2781, 29.2590>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Magellan
					SWITCH iVehicle
						CASE 0			RETURN <<-883.1570, -1046.6010, 2.5040>>
						CASE 1			RETURN <<-886.0740, -1048.3230, 1.9110>>
						CASE 2			RETURN <<-1135.5730, -1476.4860, 3.5320>>
						CASE 3			RETURN <<-1358.6401, -671.1949, 24.6083>>
						CASE 4			RETURN <<-572.1290, -980.6790, 21.1780>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Forum
					SWITCH iVehicle
						CASE 0			RETURN <<-646.0440, -1188.0410, 11.6480>>
						CASE 1			RETURN <<249.3860, -1245.0760, 28.1520>>
						CASE 2			RETURN <<-642.0730, -1185.0200, 12.3590>>
						CASE 3			RETURN <<411.7590, -1930.5760, 23.3210>>
						CASE 4			RETURN <<-165.1450, -1633.1219, 32.6500>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Roy_Lowenstein
					SWITCH iVehicle
						CASE 0			RETURN <<20.9640, -1800.7820, 25.3370>>
						CASE 1			RETURN <<467.2090, -1579.4220, 28.1220>>
						CASE 2			RETURN <<15.6230, -1795.8580, 26.0990>>
						CASE 3			RETURN <<590.1280, -2194.4409, 5.4340>>
						CASE 4			RETURN <<850.9870, -1830.7330, 28.0340>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Eastbourne
					SWITCH iVehicle
						CASE 0			RETURN <<-702.2840, -436.6880, 34.4330>>
						CASE 1			RETURN <<-356.2290, 37.8940, 47.0060>>
						CASE 2			RETURN <<355.0920, -193.1710, 56.7250>>
						CASE 3			RETURN <<-695.3930, -436.6970, 34.3580>>
						CASE 4			RETURN <<-908.5013, -163.1137, 40.8772>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_V_Magellan
					SWITCH iVehicle
						CASE 0			RETURN <<-1261.2703, -1302.3896, 2.9854>>
						CASE 1			RETURN <<-1285.3657, -1421.4346, 3.4346>>
						CASE 2			RETURN <<-1344.3103, -1266.8247, 3.8977>>
						CASE 3			RETURN <<-995.9701, -1113.7339, 1.1455>>
						CASE 4			RETURN <<-1430.9786, -969.9283, 6.2909>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Alta
					SWITCH iVehicle
						CASE 0			RETURN <<206.6620, -112.8520, 67.8801>>
						CASE 1			RETURN <<25.5250, -92.9070, 56.5680>>
						CASE 2			RETURN <<608.8180, 79.2240, 90.7360>>
						CASE 3			RETURN <<356.8860, -194.4450, 56.8950>>
						CASE 4			RETURN <<114.5330, 282.3240, 108.9740>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Forum_Drive
					SWITCH iVehicle
						CASE 0			RETURN <<-158.0714, -1702.3563, 30.0720>>
						CASE 1			RETURN <<0.8405, -1475.5609, 29.3879>>
						CASE 2			RETURN <<83.7173, -1852.7365, 23.6997>>
						CASE 3			RETURN <<-353.3724, -1603.0652, 20.1052>>
						CASE 4			RETURN <<148.6665, -1518.0341, 28.1416>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Spanish
					SWITCH iVehicle
						CASE 0			RETURN <<-362.6908, 107.8964, 65.2010>>
						CASE 1			RETURN <<-183.4608, 286.6500, 93.5902>>
						CASE 2			RETURN <<-686.9656, -106.0126, 36.7496>>
						CASE 3			RETURN <<126.9548, 90.0200, 80.8608>>
						CASE 4			RETURN <<-362.0011, 264.5454, 83.6357>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Tahitian
					SWITCH iVehicle
						CASE 0			RETURN <<-37.5017, -1577.4169, 28.3060>>
						CASE 1			RETURN <<310.6046, -1716.4688, 28.1842>>
						CASE 2			RETURN <<-91.6573, -1386.5753, 28.3207>>
						CASE 3			RETURN <<-430.5734, -1829.8203, 19.3762>>
						CASE 4			RETURN <<440.1679, -1482.5582, 28.3020>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	
		CASE GOV_AUTO_SALVAGE
			SWITCH eLocation
				CASE GangOpsLocation_AutoSalvage_Location1
					SWITCH iExtraParam
						CASE 0			RETURN <<774.6323, -2278.1572, 27.9730>>
						CASE 1			RETURN <<436.5167, 123.6772, 98.9766>>
						CASE 2			RETURN <<-1580.1648, -139.8663, 54.4126>>
						CASE 3			RETURN <<2143.4590, 4928.1982, 39.8427>>
						CASE 4			RETURN <<2709.8740, 1652.5731, 23.5583>>
						CASE 5			RETURN <<-820.8776, -2098.1223, 7.8112>> 
					ENDSWITCH
				BREAK
				
				CASE GangOpsLocation_AutoSalvage_location2
					SWITCH iExtraParam
						CASE 0			RETURN <<595.7864, -1812.4551, 22.6429>>
						CASE 1			RETURN <<892.1925, 525.5474, 122.3912>>
						CASE 2			RETURN <<-1277.6293, -1221.8312, 3.4285>>
						CASE 3			RETURN <<155.7936, 1662.2792, 228.0795>>
						CASE 4			RETURN <<2073.4849, 3201.3398, 43.8717>>
						CASE 5			RETURN <<-861.3165, -2353.5803, 12.9557>>
					ENDSWITCH
				BREAK
				
				CASE GangOpsLocation_AutoSalvage_location3
					SWITCH iExtraParam
						CASE 0			RETURN <<1293.7682, -1653.9296, 49.3323>>
						CASE 1			RETURN <<-456.8669, -11.9690, 44.7322>>
						CASE 2			RETURN <<921.9067, 3600.6187, 31.9971>>
						CASE 3			RETURN <<-1362.2649, 2271.7456, 43.2636>>
						CASE 4			RETURN <<-924.3216, 389.8085, 78.0992>>
						CASE 5			RETURN <<-576.3100, -2331.5317, 12.8282>>
					ENDSWITCH
				BREAK
				
				CASE GangOpsLocation_AutoSalvage_location4
					SWITCH iExtraParam
						CASE 0			RETURN <<180.0058, -1891.4814, 22.7175>>
						CASE 1			RETURN <<-962.2097, -684.6381, 23.1846>>
						CASE 2			RETURN <<1838.0530, 3746.3496, 32.1706>>
						CASE 3			RETURN <<737.5007, 2194.3965, 55.6944>>
						CASE 4			RETURN <<1068.0713, -775.3863, 57.2184>>
						CASE 5			RETURN <<-825.5036, -2643.3809, 12.8121>>
					ENDSWITCH
				BREAK
				
				CASE GangOpsLocation_AutoSalvage_location5
					SWITCH iExtraParam
						CASE 0			RETURN <<127.6591, -305.5567, 44.1932>>
						CASE 1			RETURN <<-2880.5896, 54.6222, 13.1105>>
						CASE 2			RETURN <<2967.2083, 4657.2358, 51.4383>>
						CASE 3			RETURN <<2058.4785, 1430.7939, 74.5879>>
						CASE 4			RETURN <<291.7050, -2484.0957, 5.4412>>
						CASE 5			RETURN <<-1054.0513, -2645.2285, 12.8307>>
					ENDSWITCH
				BREAK
				
				CASE GangOpsLocation_AutoSalvage_location6
					SWITCH iExtraParam
						CASE 0			RETURN <<1639.4646, -2536.2734, 72.5596>>
						CASE 1			RETURN <<57.4685, 346.3922, 111.7661>>
						CASE 2			RETURN <<-1864.0808, 794.5695, 139.0788>>
						CASE 3			RETURN <<1740.7625, 6399.4473, 34.3447>>
						CASE 4			RETURN <<-2848.3530, 2197.7756, 31.4316>>
						CASE 5			RETURN <<-265.4065, -2639.7886, 5.0037>>
					ENDSWITCH
				BREAK
				
				CASE GangOpsLocation_AutoSalvage_location7
					SWITCH iExtraParam
						CASE 0			RETURN <<1971.2783, -917.6273, 78.1678>>
						CASE 1			RETURN <<993.5378, -299.2350, 66.0545>>
						CASE 2			RETURN <<-1358.3403, -871.2792, 14.5499>>
						CASE 3			RETURN <<-2169.9177, 4452.4814, 61.7216>>
						CASE 4			RETURN <<-800.7912, 1297.0177, 257.1681>>
						CASE 5			RETURN <<-336.4576, -2159.8223, 9.3174>>
					ENDSWITCH
				BREAK
				
				CASE GangOpsLocation_AutoSalvage_location8
					SWITCH iExtraParam
						CASE 0			RETURN <<466.9972, -1252.2458, 28.4305>>
						CASE 1			RETURN <<-537.6263, 348.7901, 82.0424>>
						CASE 2			RETURN <<-777.2712, -1299.6853, 4.0004>>
						CASE 3			RETURN <<2748.4929, 3407.4026, 55.5552>>
						CASE 4			RETURN <<1308.1472, 1082.9937, 104.6304>>
						CASE 5			RETURN <<-190.3448, -1756.7906, 29.3405>>
					ENDSWITCH
				BREAK
				
				CASE GangOpsLocation_AutoSalvage_location9
					SWITCH iExtraParam
						CASE 0			RETURN <<1279.6530, -2019.2544, 43.6157>>
						CASE 1			RETURN <<-145.1326, -813.2120, 30.5257>>
						CASE 2			RETURN <<-621.3791, -585.7535, 33.3083>>
						CASE 3			RETURN <<369.8071, 4422.7432, 61.5373>>
						CASE 4			RETURN <<2555.1934, 300.4682, 107.4606>>
						CASE 5			RETURN <<-1009.6603, -1862.8379, 15.5522>>
					ENDSWITCH
				BREAK
				
				CASE GangOpsLocation_AutoSalvage_location10
					SWITCH iExtraParam
						CASE 0			RETURN <<784.4866, -3101.8892, 4.8007>>
						CASE 1			RETURN <<221.4211, -608.9487, 40.5724>>
						CASE 2			RETURN <<-1030.9718, -212.0784, 36.7746>>
						CASE 3			RETURN <<-336.7441, 6255.1421, 30.4988>>
						CASE 4			RETURN <<1792.1393, 2263.9124, 58.1057>>
						CASE 5			RETURN <<-1045.0759, -1675.4041, 3.5399>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_CAR_COLLECTOR
			SWITCH iVehicle
				CASE 0		RETURN <<623.8916, 4747.2705, -60.0000>>
			ENDSWITCH
		BREAK
		CASE GOV_MOB_MENTALITY
			SWITCH eLocation
				CASE GangOpsLocation_MobMentality_Morningwood
					SWITCH iVehicle
						CASE 0			RETURN <<-1390.7052, -150.8429, 46.5657>>
						CASE 1			RETURN <<-1381.3663, -147.9393, 46.9094>>
						CASE 2			RETURN <<-1392.6892, -159.7647, 46.5073>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_MobMentality_Grapeseed
					SWITCH iVehicle
						CASE 0			RETURN <<1801.1652, 4586.8560, 36.0928>>
						CASE 1			RETURN <<1789.6841, 4585.6636, 36.4149>>
						CASE 2			RETURN <<1785.3682, 4585.0923, 36.5216>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_MobMentality_Alta
					SWITCH iVehicle
						CASE 0			RETURN <<418.9228, -347.3040, 46.2620>>
						CASE 1			RETURN <<411.8482, -351.4157, 46.1114>>
						CASE 2			RETURN <<411.5239, -350.1000, 46.1304>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_MobMentality_MissionRow
					SWITCH iVehicle
						CASE 0			RETURN <<262.0585, -1150.1442, 28.2917>>
						CASE 1			RETURN <<259.3955, -1149.8529, 28.2917>>
						CASE 2			RETURN <<250.1504, -1161.6500, 28.1441>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_MobMentality_HippyCamp
					SWITCH iVehicle
						CASE 0			RETURN <<2326.3826, 2504.5085, 45.4107>>
						CASE 1			RETURN <<2327.6418, 2508.4902, 45.6677>>
						CASE 2			RETURN <<2314.6409, 2516.2397, 45.6677>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_MobMentality_SanAndreas
					SWITCH iVehicle
						CASE 0			RETURN <<-1198.1249, -1456.0236, 3.3791>>
						CASE 1			RETURN <<-1219.9482, -1448.4550, 3.3434>>
						CASE 2			RETURN <<-1221.3744, -1448.8250, 3.3348>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_MobMentality_MirrorPark
					SWITCH iVehicle
						CASE 0			RETURN <<1337.3380, -722.5860, 65.5480>>
						CASE 1			RETURN <<1341.0280, -688.4140, 65.6930>>
						CASE 2			RETURN <<1360.1073, -719.5023, 66.0396>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_MobMentality_Strawberry
					SWITCH iVehicle
						CASE 0			RETURN <<53.1270, -1554.4980, 28.4600>>
						CASE 1			RETURN <<61.8644, -1565.7544, 28.4601>>
						CASE 2			RETURN <<56.5981, -1573.2654, 28.4603>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_MobMentality_MirrorParkTavern
					SWITCH iVehicle
						CASE 0			RETURN <<1223.9156, -427.2592, 66.6334>>
						CASE 1			RETURN <<1227.4287, -441.2229, 66.5063>>
						CASE 2			RETURN <<1220.4982, -439.8712, 66.2664>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_MobMentality_ULSA
					SWITCH iVehicle
						CASE 0			RETURN <<-1603.6141, 178.9601, 58.3318>>
						CASE 1			RETURN <<-1607.5432, 196.7204, 58.7842>>
						CASE 2			RETURN <<-1616.2191, 181.0374, 58.9642>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_PARAMEDIC
			SWITCH eLocation
				CASE GangOpsLocation_Paramedic_Central_LS_Medical_Center
					SWITCH iVehicle
						CASE 0			RETURN <<401.1400, -1426.0439, 28.4510>>
						CASE 1			RETURN <<394.4040, -1445.4390, 28.3760>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Pillbox_Hill_Medical_Center
					SWITCH iVehicle
						CASE 0			RETURN <<378.4420, -575.8930, 27.8420>>
						CASE 1			RETURN <<367.3070, -598.4880, 27.8580>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_ST_Fiacre_Hospital
					SWITCH iVehicle
						CASE 0			RETURN <<1173.3400, -1535.7030, 38.4010>>
						CASE 1			RETURN <<1175.1040, -1523.1870, 33.6930>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Sandy_Shores_Medical_Center
					SWITCH iVehicle
						CASE 0			RETURN <<-485.3936, -328.5174, 33.5015>>
						CASE 1			RETURN <<-464.4834, -319.9254, 33.3637>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Paleto_Bay_Care_Center
					SWITCH iVehicle
						CASE 0			RETURN <<-254.7530, 6340.5752, 31.4260>>
						CASE 1			RETURN <<-258.5140, 6347.8472, 31.4260>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Del_Perro_Freeway
					SWITCH iVehicle
						CASE 0			RETURN <<-1477.8290, -766.2460, 10.1880>>
						CASE 1			RETURN <<-1485.7430, -768.5020, 9.9970>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Mount_Gordo
					SWITCH iVehicle
						CASE 0			RETURN <<1936.1407, 6288.7065, 40.0680>>
						CASE 1			RETURN <<1933.9744, 6280.3525, 41.7774>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Great_Chaparral
					SWITCH iVehicle
						CASE 0			RETURN <<-768.4230, 1690.9449, 199.6950>>
						CASE 1			RETURN <<-752.9310, 1688.5170, 200.3520>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Cassidy_Creek
					SWITCH iVehicle
						CASE 0			RETURN <<-883.0544, 4429.4595, 19.8535>>
						CASE 1			RETURN <<-892.4820, 4421.2905, 19.8866>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Power_Station
					SWITCH iVehicle
						CASE 0			RETURN <<-2418.7908, 1019.0956, 190.4588>>
						CASE 1			RETURN <<-2411.8855, 1026.1110, 194.5612>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_AQUALUNGS
			SWITCH eLocation
				CASE GangOpsLocation_Aqualungs_Elysian_Island_1
					SWITCH iVehicle
						CASE 0			RETURN <<-481.3853, -3212.3813, 0.000>>
						CASE 1			RETURN <<-491.7104, -3208.5386, 0.000>>
						CASE 2			RETURN <<-502.3377, -3227.6389, 0.8164>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Paleto_Bay
					SWITCH iVehicle
						CASE 0			RETURN <<687.1586, 6971.2358, 0.000>>
						CASE 1			RETURN <<695.8054, 6965.0313, 0.000>>
						CASE 2			RETURN <<706.3797, 6978.4673, 1.4542>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Elysian_Island_2
					SWITCH iVehicle
						CASE 0			RETURN <<765.5132, -3578.5398, 2.5543>>
						CASE 1			RETURN <<757.4534, -3561.7981, 0.000>>
						CASE 2			RETURN <<751.3599, -3569.3508, 0.000>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Palomino_Highlands
					SWITCH iVehicle
						CASE 0			RETURN <<3045.9705, -1089.8003, 1.9543>>
						CASE 1			RETURN <<3044.9819, -1071.8890, 0.000>>
						CASE 2			RETURN <<3035.3521, -1076.4139, 0.000>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Chumash
					SWITCH iVehicle
						CASE 0			RETURN <<-3703.3230, 1296.1520, 0.000>>
						CASE 1			RETURN <<-3699.9761, 1305.4290, 0.000>>
						CASE 2			RETURN <<-3720.8027, 1307.6138, 2.0164>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_San_Chianksi_Mountain_Range
					SWITCH iVehicle
						CASE 0			RETURN <<3981.5054, 5082.4053, 2.0164>>
						CASE 1			RETURN <<3982.0850, 5062.8398, 0.000>>
						CASE 2			RETURN <<3972.4861, 5066.6392, 0.000>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_PaletoForest
					SWITCH iVehicle
						CASE 0			RETURN <<-1501.1713, 5779.8774, 1.4247>>
						CASE 1			RETURN <<-1480.6299, 5777.9053, 0.1875>>
						CASE 2			RETURN <<-1483.0741, 5788.4111, -0.3750>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_North_Chumash
					SWITCH iVehicle
						CASE 0			RETURN <<-2917.0859, 4405.7002, 0.000>>
						CASE 1			RETURN <<-2915.2490, 4415.5269, 0.000>>
						CASE 2			RETURN <<-2935.9304, 4412.3755, 2.0164>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Pacific_Bluffs
					SWITCH iVehicle
						CASE 0			RETURN <<-2221.6704, -888.9195, 0.000>>
						CASE 1			RETURN <<-2232.3552, -890.9289, 0.000>>
						CASE 2			RETURN <<-2229.4050, -908.6776, 2.0164>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Vespucci_Beach
					SWITCH iVehicle
						CASE 0			RETURN <<-1738.6846, -1747.0631, 0.000>>
						CASE 1			RETURN <<-1743.7642, -1739.5004, 0.000>>
						CASE 2			RETURN <<-1761.0157, -1748.3948, 2.0164>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_UNDER_CONTROL
			SWITCH eLocation
				CASE GangOpsLocation_UnderControl_ServerRoom1
					SWITCH iVehicle
						CASE 0			RETURN <<-1192.4279, -2527.5439, 12.9452>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ServerRoom2
					SWITCH iVehicle
						CASE 0			RETURN <<-1928.4919, -3004.7610, 12.9440>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ServerRoom3
					SWITCH iVehicle
						CASE 0			RETURN <<-841.6110, -2936.9031, 12.9710>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ServerRoom4
					SWITCH iVehicle
						CASE 0			RETURN <<-1025.5900, -2374.6450, 12.9450>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ServerRoom5
					SWITCH iVehicle
						CASE 0			RETURN <<363.4324, -2543.2656, 4.7459>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ControlRoom1
					SWITCH iVehicle
						CASE 0			RETURN <<814.8370, -3184.8511, 4.9030>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ControlRoom2
					SWITCH iVehicle
						CASE 0			RETURN <<1177.0898, -2126.5793, 42.2460>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ControlRoom3
					SWITCH iVehicle
						CASE 0			RETURN <<1120.0270, -3239.6089, 4.8950>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ControlRoom4
					SWITCH iVehicle
						CASE 0			RETURN <<-35.2950, -2232.3989, 6.8120>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ControlRoom5
					SWITCH iVehicle
						CASE 0			RETURN <<272.1206, -2203.6011, 7.8954>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_BURGLARY_JOB
			SWITCH eLocation
				CASE GangOpsLocation_BurglaryJob_ProcopioDrive
					SWITCH iVehicle
						CASE 0			RETURN <<-396.5980, 6312.4341, 27.8988>>
						CASE 1			RETURN <<-392.1804, 6307.3804, 28.3690>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_BurglaryJob_SouthMoMilton
					SWITCH iVehicle
						CASE 0			RETURN <<-1097.4656, 548.5806, 101.7121>>
						CASE 1			RETURN <<-1107.1416, 549.8876, 101.5932>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_BurglaryJob_MirrorPark
					SWITCH iVehicle
						CASE 0			RETURN <<1184.5009, -573.2849, 63.1735>>
						CASE 1			RETURN <<1184.9597, -582.6427, 63.2098>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_BurglaryJob_ImaginationCourt
					SWITCH iVehicle
						CASE 0			RETURN <<-1125.7233, -1069.8094, 1.1084>>
						CASE 1			RETURN <<-1137.5984, -1063.9890, 1.1504>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_BurglaryJob_DidionDrive
					SWITCH iVehicle
						CASE 0			RETURN <<-365.5430, 353.6118, 108.4730>>
						CASE 1			RETURN <<-376.6736, 348.6796, 108.2257>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_BurglaryJob_KimbleHill
					SWITCH iVehicle
						CASE 0			RETURN <<-275.9112, 599.8813, 180.6837>>
						CASE 1			RETURN <<-272.7558, 600.2863, 180.6991>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_BurglaryJob_BridgeStreet
					SWITCH iVehicle
						CASE 0			RETURN <<1048.5222, -486.9284, 62.9246>>
						CASE 1			RETURN <<1055.4055, -488.4610, 62.7264>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_BurglaryJob_NikolaAvenue
					SWITCH iVehicle
						CASE 0			RETURN <<925.4179, -563.7004, 56.9670>>
						CASE 1			RETURN <<928.4501, -569.7017, 56.9306>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_BurglaryJob_NorthSheldonAvenue
					SWITCH iVehicle
						CASE 0			RETURN <<-964.3839, 765.3812, 174.4322>>
						CASE 1			RETURN <<-967.0838, 763.1378, 174.4007>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_BurglaryJob_InesenoRoad
					SWITCH iVehicle
						CASE 0			RETURN <<-3091.0293, 339.4230, 6.4349>>
						CASE 1			RETURN <<-3089.9165, 342.7941, 6.4293>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_FLIGHT_RECORDER
			SWITCH eLocation
				CASE GangOpsLocation_FlightRecorder_Dam
					SWITCH iVehicle
						CASE 0		RETURN <<870.5683, -530.9145, 490.6413>>
						CASE 1		RETURN <<3077.9812, 295.1917, 0.1875>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlightRecorder_Palomino
					SWITCH iVehicle
						CASE 0		RETURN <<759.0447, -1203.4915, 326.5067>>
						CASE 1		RETURN <<2947.5779, -736.8120, -0.0000>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlightRecorder_Terminal
					SWITCH iVehicle
						CASE 0		RETURN <<36.7369, -1597.6149, 187.0704>>
						CASE 1		RETURN <<1688.8042, -3038.5623, 0.9375>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlightRecorder_PacificBluffs
					SWITCH iVehicle
						CASE 0		RETURN <<-696.3125, -609.0925,  235.9581>>
						CASE 1		RETURN <<-326.3706, -3073.1926, 0.1875>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlightRecorder_MountGordo
					SWITCH iVehicle
						CASE 0		RETURN <<1536.2373, 6624.1475, 192.1024>>
						CASE 1		RETURN <<3558.4678, 5535.3735, -0.2935>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlightRecorder_AlamoSea1
					SWITCH iVehicle
						CASE 0		RETURN <<1235.5138, 3707.3193, 256.6749>>
						CASE 1		RETURN <<1184.6162, 3950.7446, 29.2500>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlightRecorder_AlamoSea2
					SWITCH iVehicle
						CASE 0		RETURN <<2049.2805, 4582.7554, 405.7150>>
						CASE 1		RETURN <<2057.4448, 4216.3560, 30.5625>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlightRecorder_AlamoSea3
					SWITCH iVehicle
						CASE 0		RETURN <<865.5893, 1721.0413, 351.8940>>
						CASE 1		RETURN <<45.4551, 3959.3230, 29.4375>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlightRecorder_PaletoBay
					SWITCH iVehicle
						CASE 0		RETURN <<60.0790, 7227.8721, 123.6636>>
						CASE 1		RETURN <<1264.1479, 6841.9570, 0.1874>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlightRecorder_FortZancudo
					SWITCH iVehicle
						CASE 0		RETURN <<-2041.8317, 5267.3296, 600.9536>>
						CASE 1		RETURN <<-3332.2498, 3659.6514, -0.7501>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_GONE_BALLISTIC
			SWITCH eLocation
				CASE GangOpsLocation_GoneBallistic_ElysianIsland1
					SWITCH iVehicle
						CASE 0			RETURN <<-151.4660, -2545.4751, 5.0420>>
						CASE 1			RETURN <<-125.1900, -2570.5371, 5.0010>>
						CASE 2			RETURN <<-127.4010, -2561.7229, 5.0170>>
						CASE 3			RETURN <<-166.4480, -2559.3799, 5.0140>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_GoneBallistic_GrandSenoraDesert1
					SWITCH iVehicle
						CASE 0			RETURN <<1449.8800, 1530.6770, 110.4450>>
						CASE 1			RETURN <<1417.4020, 1534.9990, 108.6550>>
						CASE 2			RETURN <<1433.3140, 1506.3140, 112.4470>>
						CASE 3			RETURN <<1411.5551, 1512.3149, 111.7770>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_GoneBallistic_Grapeseed1
					SWITCH iVehicle
						CASE 0			RETURN <<2185.4280, 4997.3672, 40.8780>>
						CASE 1			RETURN <<2188.3230, 4995.2441, 41.0190>>
						CASE 2			RETURN <<2206.1682, 5017.3491, 42.5655>>
						CASE 3			RETURN <<2173.8594, 5039.0659, 41.7936>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_GoneBallistic_ElBurroHeights
					SWITCH iVehicle
						CASE 0			RETURN <<1100.2220, -2450.0750, 29.4900>>
						CASE 1			RETURN <<1109.8790, -2468.0281, 29.3410>>
						CASE 2			RETURN <<1116.1240, -2439.3750, 30.7390>>
						CASE 3			RETURN <<1091.7137, -2471.5017, 28.8290>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_GoneBallistic_Terminal
					SWITCH iVehicle
						CASE 0			RETURN <<998.2521, -3195.2021, 4.9010>>
						CASE 1			RETURN <<998.1599, -3199.1155, 4.9012>>
						CASE 2			RETURN <<1009.7358, -3181.9858, 4.9008>>
						CASE 3			RETURN <<1032.9375, -3192.1201, 4.9014>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_GoneBallistic_LSIA
					SWITCH iVehicle
						CASE 0			RETURN <<-1217.4885, -2045.6161, 13.0816>>
						CASE 1			RETURN <<-1223.2111, -2062.8508, 13.3329>>
						CASE 2			RETURN <<-1192.8986, -2047.6458, 13.0665>>
						CASE 3			RETURN <<-1202.8495, -2031.6779, 13.2960>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_GoneBallistic_MurrietaHeights
					SWITCH iVehicle
						CASE 0			RETURN <<1278.3518, -1960.1853, 42.6730>>
						CASE 1			RETURN <<1268.4708, -1940.8927, 42.2639>>
						CASE 2			RETURN <<1293.9791, -1946.9562, 42.4994>>
						CASE 3			RETURN <<1300.5261, -1930.7705, 42.2639>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_GoneBallistic_ElysianIsland2
					SWITCH iVehicle
						CASE 0			RETURN <<-436.6960, -2733.4700, 5.0000>>
						CASE 1			RETURN <<-431.4430, -2738.3069, 5.0000>>
						CASE 2			RETURN <<-409.4460, -2740.5400, 5.0000>>
						CASE 3			RETURN <<-392.9009, -2710.8325, 5.0002>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_GoneBallistic_PaletoBay
					SWITCH iVehicle
						CASE 0			RETURN <<-427.9031, 6437.2905, 2.3078>>
						CASE 1			RETURN <<-417.2166, 6416.1436, 1.8207>>
						CASE 2			RETURN <<-449.1577, 6418.3042, 1.4016>>
						CASE 3			RETURN <<-449.4671, 6437.2378, 1.4366>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_GoneBallistic_FortZancudo
					SWITCH iVehicle
						CASE 0			RETURN <<-2752.8491, 3452.0410, 10.8628>>
						CASE 1			RETURN <<-2769.5869, 3457.1191, 10.6722>>
						CASE 2			RETURN <<-2765.5686, 3485.6975, 9.7447>>
						CASE 3			RETURN <<-2748.3743, 3483.8547, 10.4027>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH

	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC FLOAT GET_GANGOPS_VEHICLE_SPAWN_HEADING(GANGOPS_VARIATION eVariation, GANGOPS_LOCATION eLocation, INT iVehicle, FREEMODE_DELIVERY_DROPOFFS eDropOff = FREEMODE_DELIVERY_DROPOFF_INVALID, INT iExtraParam = -1, INT iRespawnLocation = 0)

	UNUSED_PARAMETER(eDropOff)
	UNUSED_PARAMETER(iRespawnLocation)

	SWITCH eVariation
		CASE GOV_FLARE_UP						
			SWITCH eLocation
				CASE GangOpsLocation_FlareUp_Chamberlain_Hills			
					SWITCH iVehicle
						CASE 0			RETURN 141.9979
						CASE 1			RETURN 107.5978
						CASE 2			RETURN 346.5980
						CASE 3			RETURN 139.3980
						CASE 4			RETURN 254.7988
						CASE 5			RETURN 65.5985
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Morningwood
					SWITCH iVehicle
						CASE 0			RETURN 258.9999
						CASE 1			RETURN 72.1999
						CASE 2			RETURN 321.8000
						CASE 3			RETURN 351.0000
						CASE 4			RETURN 342.9994
						CASE 5			RETURN 289.5991
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_La_Puerta
					SWITCH iVehicle
						CASE 0			RETURN 274.9995
						CASE 1			RETURN 310.5995
						CASE 2			RETURN 97.5992
						CASE 3			RETURN 178.7989
						CASE 4			RETURN 267.3996
						CASE 5			RETURN 95.9992
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_El_burro
					SWITCH iVehicle
						CASE 0			RETURN 184.5999
						CASE 1			RETURN 206.8001
						CASE 2			RETURN 16.3998
						CASE 3			RETURN 52.5998
						CASE 4			RETURN 348.0000
						CASE 5			RETURN 100.1998
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Davis
					SWITCH iVehicle
						CASE 0			RETURN 313.4000
						CASE 1			RETURN 22.7999
						CASE 2			RETURN 286.7997
						CASE 3			RETURN 295.7991
						CASE 4			RETURN 270.7987
						CASE 5			RETURN 212.3989
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Sandy
					SWITCH iVehicle
						CASE 0			RETURN 334.6000
						CASE 1			RETURN 98.9988
						CASE 2			RETURN 47.7989
						CASE 3			RETURN 65.5988
						CASE 4			RETURN 211.7987
						CASE 5			RETURN 180.3988
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Paleto
					SWITCH iVehicle
						CASE 0			RETURN 72.9996
						CASE 1			RETURN 102.5996
						CASE 2			RETURN 241.5990
						CASE 3			RETURN 301.5983
						CASE 4			RETURN 214.1977
						CASE 5			RETURN 344.997
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Capital
					SWITCH iVehicle
						CASE 0			RETURN 287.9995
						CASE 1			RETURN 39.1993
						CASE 2			RETURN 243.7991
						CASE 3			RETURN 267.7990
						CASE 4			RETURN 218.1980
						CASE 5			RETURN 323.1980
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Clinton
					SWITCH iVehicle
						CASE 0			RETURN 196.8000
						CASE 1			RETURN 339.0000
						CASE 2			RETURN 298.9993
						CASE 3			RETURN 256.7992
						CASE 4			RETURN 226.3987
						CASE 5			RETURN 264.7985
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Richman
					SWITCH iVehicle
						CASE 0			RETURN 346.0000
						CASE 1			RETURN 188.8000
						CASE 2			RETURN 245.0000
						CASE 3			RETURN 114.6000
						CASE 4			RETURN 299.3990
						CASE 5			RETURN 288.7990
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE GOV_FORCED_ENTRY						
			SWITCH eLocation
				CASE GangOpsLocation_ForcedEntry_Location_Fudge				
					SWITCH iVehicle
						CASE 0			RETURN 203.9990
						CASE 1			RETURN 198.5980
						CASE 2			RETURN 236.6000
						CASE 3			RETURN 200.3980
						CASE 4			RETURN 230.5990
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Magellan
					SWITCH iVehicle
						CASE 0			RETURN 306.3500
						CASE 1			RETURN 299.2000
						CASE 2			RETURN 113.5990
						CASE 3			RETURN 213.5990
						CASE 4			RETURN 8.5990
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Forum
					SWITCH iVehicle
						CASE 0			RETURN 304.3990
						CASE 1			RETURN 94.1970
						CASE 2			RETURN 304.3990
						CASE 3			RETURN 227.3300
						CASE 4			RETURN 301.1290
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Roy_Lowenstein
					SWITCH iVehicle
						CASE 0			RETURN 42.4000
						CASE 1			RETURN 49.8000
						CASE 2			RETURN 42.4000
						CASE 3			RETURN 355.0730
						CASE 4			RETURN 115.4000
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Eastbourne
					SWITCH iVehicle
						CASE 0			RETURN 272.2000
						CASE 1			RETURN 88.2000
						CASE 2			RETURN 248.0000
						CASE 3			RETURN 272.2000
						CASE 4			RETURN 207.6000
					ENDSWITCH
				BREAK
				
				CASE GangOpsLocation_ForcedEntry_Location_V_Magellan
					SWITCH iVehicle
						CASE 0			RETURN 197.0001
						CASE 1			RETURN 212.4003
						CASE 2			RETURN 16.4003
						CASE 3			RETURN 120.0002
						CASE 4			RETURN 239.1994
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Alta
					SWITCH iVehicle
						CASE 0			RETURN 249.7985
						CASE 1			RETURN 68.5990
						CASE 2			RETURN 147.1990
						CASE 3			RETURN 253.7990
						CASE 4			RETURN 12.5990
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Forum_Drive
					SWITCH iVehicle
						CASE 0			RETURN 44.9997
						CASE 1			RETURN 319.9997
						CASE 2			RETURN 230.9990
						CASE 3			RETURN 177.7990
						CASE 4			RETURN 320.9989
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Spanish
					SWITCH iVehicle
						CASE 0			RETURN 185.7999
						CASE 1			RETURN 181.1998
						CASE 2			RETURN 301.7995
						CASE 3			RETURN 252.3992
						CASE 4			RETURN 180.9994
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Tahitian
					SWITCH iVehicle
						CASE 0			RETURN 44.0000
						CASE 1			RETURN 315.7994
						CASE 2			RETURN 346.3989
						CASE 3			RETURN 229.7988
						CASE 4			RETURN 287.9987
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE GOV_AUTO_SALVAGE
			SWITCH eLocation
				CASE GangOpsLocation_AutoSalvage_Location1
					SWITCH iExtraParam
						CASE 0			RETURN 354.3447
						CASE 1			RETURN 70.1474
						CASE 2			RETURN 35.5467
						CASE 3			RETURN 235.9441
						CASE 4			RETURN -0.2595
						CASE 5			RETURN 225.1445
					ENDSWITCH
				BREAK
				
				CASE GangOpsLocation_AutoSalvage_location2
					SWITCH iExtraParam
						CASE 0			RETURN 354.9445
						CASE 1			RETURN 266.3470
						CASE 2			RETURN 19.9457
						CASE 3			RETURN 113.7420
						CASE 4			RETURN 59.7423
						CASE 5			RETURN 266.9443
					ENDSWITCH
				BREAK
				
				CASE GangOpsLocation_AutoSalvage_location3
					SWITCH iExtraParam
						CASE 0			RETURN 305.3441
						CASE 1			RETURN 265.9464
						CASE 2			RETURN 181.5442
						CASE 3			RETURN 180.3421
						CASE 4			RETURN 188.5440
						CASE 5			RETURN 319.7442
					ENDSWITCH
				BREAK
				
				CASE GangOpsLocation_AutoSalvage_location4
					SWITCH iExtraParam
						CASE 0			RETURN 65.3422
						CASE 1			RETURN 295.7450
						CASE 2			RETURN 209.1436
						CASE 3			RETURN 288.5420
						CASE 4			RETURN -0.2543
						CASE 5			RETURN 61.1437
					ENDSWITCH
				BREAK
				
				CASE GangOpsLocation_AutoSalvage_location5
					SWITCH iExtraParam
						CASE 0			RETURN 69.7461
						CASE 1			RETURN 253.9462
						CASE 2			RETURN 191.5437
						CASE 3			RETURN 215.1420
						CASE 4			RETURN 200.1417
						CASE 5			RETURN 239.7436
					ENDSWITCH
				BREAK
				
				CASE GangOpsLocation_AutoSalvage_location6
					SWITCH iExtraParam
						CASE 0			RETURN 291.3987
						CASE 1			RETURN 244.1975
						CASE 2			RETURN 131.3964
						CASE 3			RETURN 110.9947
						CASE 4			RETURN 115.5927
						CASE 5			RETURN 315.9916
					ENDSWITCH
				BREAK
				
				CASE GangOpsLocation_AutoSalvage_location7
					SWITCH iExtraParam
						CASE 0			RETURN 123.1985
						CASE 1			RETURN 58.3972
						CASE 2			RETURN 314.3958
						CASE 3			RETURN 128.1943
						CASE 4			RETURN 22.5925
						CASE 5			RETURN 269.1913
					ENDSWITCH
				BREAK
				
				CASE GangOpsLocation_AutoSalvage_location8
					SWITCH iExtraParam
						CASE 0			RETURN 91.5971
						CASE 1			RETURN 174.7957
						CASE 2			RETURN 319.9957
						CASE 3			RETURN 249.3940
						CASE 4			RETURN 184.9924
						CASE 5			RETURN 235.9911
					ENDSWITCH
				BREAK
				
				CASE GangOpsLocation_AutoSalvage_location9
					SWITCH iExtraParam
						CASE 0			RETURN 201.3977
						CASE 1			RETURN 340.1969
						CASE 2			RETURN -0.4043
						CASE 3			RETURN 236.3932
						CASE 4			RETURN 179.9926
						CASE 5			RETURN 313.7906
					ENDSWITCH
				BREAK
				
				CASE GangOpsLocation_AutoSalvage_location10
					SWITCH iExtraParam
						CASE 0			RETURN 360.1976
						CASE 1			RETURN 69.9968
						CASE 2			RETURN 254.3951
						CASE 3			RETURN 225.7930
						CASE 4			RETURN 166.3924
						CASE 5			RETURN 358.9898
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	
		CASE GOV_CAR_COLLECTOR
			SWITCH iVehicle
				CASE 0		RETURN 250.8627
			ENDSWITCH
		BREAK
		CASE GOV_MOB_MENTALITY
			SWITCH eLocation
				CASE GangOpsLocation_MobMentality_Morningwood
					SWITCH iVehicle
						CASE 0			RETURN 357.4000
						CASE 1			RETURN 271.5995
						CASE 2			RETURN 336.2000
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_MobMentality_Grapeseed
					SWITCH iVehicle
						CASE 0			RETURN 5.4000
						CASE 1			RETURN 185.6000
						CASE 2			RETURN 4.7999
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_MobMentality_Alta
					SWITCH iVehicle
						CASE 0			RETURN 339.1969
						CASE 1			RETURN 100.9967
						CASE 2			RETURN 73.1964
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_MobMentality_MissionRow
					SWITCH iVehicle
						CASE 0			RETURN 357.1996
						CASE 1			RETURN 191.3995
						CASE 2			RETURN 186.5981
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_MobMentality_HippyCamp
					SWITCH iVehicle
						CASE 0			RETURN 52.3988
						CASE 1			RETURN 24.1989
						CASE 2			RETURN 317.1978
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_MobMentality_SanAndreas
					SWITCH iVehicle
						CASE 0			RETURN 213.9977
						CASE 1			RETURN 152.1991
						CASE 2			RETURN 127.9993
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_MobMentality_MirrorPark
					SWITCH iVehicle
						CASE 0			RETURN 256.6400
						CASE 1			RETURN 102.0390
						CASE 2			RETURN 198.4395
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_MobMentality_Strawberry
					SWITCH iVehicle
						CASE 0			RETURN 50.1990
						CASE 1			RETURN 49.5996
						CASE 2			RETURN 229.5994
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_MobMentality_MirrorParkTavern
					SWITCH iVehicle
						CASE 0			RETURN 9.9980
						CASE 1			RETURN 10.5980
						CASE 2			RETURN 188.3999
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_MobMentality_ULSA
					SWITCH iVehicle
						CASE 0			RETURN 23.4000
						CASE 1			RETURN 236.3995
						CASE 2			RETURN 177.3993
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_PARAMEDIC
			SWITCH eLocation
				CASE GangOpsLocation_Paramedic_Central_LS_Medical_Center
					SWITCH iVehicle
						CASE 0			RETURN 230.3980
						CASE 1			RETURN 29.7980
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Pillbox_Hill_Medical_Center
					SWITCH iVehicle
						CASE 0			RETURN 93.1970
						CASE 1			RETURN 339.9960
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_ST_Fiacre_Hospital
					SWITCH iVehicle
						CASE 0			RETURN 179.3970
						CASE 1			RETURN 180.7990
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Sandy_Shores_Medical_Center
					SWITCH iVehicle
						CASE 0			RETURN 82.7999
						CASE 1			RETURN 23.5997
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Paleto_Bay_Care_Center
					SWITCH iVehicle
						CASE 0			RETURN 315.3960
						CASE 1			RETURN 86.7960
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Del_Perro_Freeway
					SWITCH iVehicle
						CASE 0			RETURN 295.3960
						CASE 1			RETURN 271.9940
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Mount_Gordo
					SWITCH iVehicle
						CASE 0			RETURN 259.3950
						CASE 1			RETURN 218.3939
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Great_Chaparral
					SWITCH iVehicle
						CASE 0			RETURN 129.9950
						CASE 1			RETURN 335.7950
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Cassidy_Creek
					SWITCH iVehicle
						CASE 0			RETURN 272.5940
						CASE 1			RETURN 264.1921
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Power_Station
					SWITCH iVehicle
						CASE 0			RETURN 250.3914
						CASE 1			RETURN 102.7904
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_AQUALUNGS
			SWITCH eLocation
				CASE GangOpsLocation_Aqualungs_Elysian_Island_1
					SWITCH iVehicle
						CASE 0			RETURN 4.7939
						CASE 1			RETURN 299.7939
						CASE 2			RETURN 120.1906
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Paleto_Bay
					SWITCH iVehicle
						CASE 0			RETURN 149.5988
						CASE 1			RETURN 112.3986
						CASE 2			RETURN 327.3980
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Elysian_Island_2
					SWITCH iVehicle
						CASE 0			RETURN 243.7979
						CASE 1			RETURN 63.9979
						CASE 2			RETURN 36.1978
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Palomino_Highlands
					SWITCH iVehicle
						CASE 0			RETURN 219.0000
						CASE 1			RETURN 37.0000
						CASE 2			RETURN 15.9990
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Chumash
					SWITCH iVehicle
						CASE 0			RETURN 267.2000
						CASE 1			RETURN 249.0000
						CASE 2			RETURN 88.5988
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_San_Chianksi_Mountain_Range
					SWITCH iVehicle
						CASE 0			RETURN 317.9989
						CASE 1			RETURN 151.9990
						CASE 2			RETURN 175.9980
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_PaletoForest
					SWITCH iVehicle
						CASE 0			RETURN 133.3998
						CASE 1			RETURN 305.5995
						CASE 2			RETURN 281.1992
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_North_Chumash
					SWITCH iVehicle
						CASE 0			RETURN 298.7990
						CASE 1			RETURN 268.1990
						CASE 2			RETURN 107.7978
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Pacific_Bluffs
					SWITCH iVehicle
						CASE 0			RETURN 11.3990
						CASE 1			RETURN 340.1990
						CASE 2			RETURN 157.7991
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Vespucci_Beach
					SWITCH iVehicle
						CASE 0			RETURN 304.7987
						CASE 1			RETURN 289.1985
						CASE 2			RETURN 118.5978
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_UNDER_CONTROL
			SWITCH eLocation
				CASE GangOpsLocation_UnderControl_ServerRoom1
					SWITCH iVehicle
						CASE 0			RETURN 151.5974
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ServerRoom2
					SWITCH iVehicle
						CASE 0			RETURN 150.9990
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ServerRoom3
					SWITCH iVehicle
						CASE 0			RETURN 328.8000
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ServerRoom4
					SWITCH iVehicle
						CASE 0			RETURN 60.9990
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ServerRoom5
					SWITCH iVehicle
						CASE 0			RETURN 316.5978
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ControlRoom1
					SWITCH iVehicle
						CASE 0			RETURN 326.9980
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ControlRoom2
					SWITCH iVehicle
						CASE 0			RETURN 181.4001
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ControlRoom3
					SWITCH iVehicle
						CASE 0			RETURN 148.9990
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ControlRoom4
					SWITCH iVehicle
						CASE 0			RETURN 144.9990
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ControlRoom5
					SWITCH iVehicle
						CASE 0			RETURN 174.6001
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_BURGLARY_JOB
			SWITCH eLocation
				CASE GangOpsLocation_BurglaryJob_ProcopioDrive
					SWITCH iVehicle
						CASE 0			RETURN 40.8000
						CASE 1			RETURN 40.2000
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_BurglaryJob_SouthMoMilton
					SWITCH iVehicle
						CASE 0			RETURN 30.8000
						CASE 1			RETURN 226.9997
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_BurglaryJob_MirrorPark
					SWITCH iVehicle
						CASE 0			RETURN 3.3993
						CASE 1			RETURN 4.3989
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_BurglaryJob_ImaginationCourt
					SWITCH iVehicle
						CASE 0			RETURN 298.3986
						CASE 1			RETURN 28.9984
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_BurglaryJob_DidionDrive
					SWITCH iVehicle
						CASE 0			RETURN 135.1995
						CASE 1			RETURN 97.1998
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_BurglaryJob_KimbleHill
					SWITCH iVehicle
						CASE 0			RETURN 177.6000
						CASE 1			RETURN 351.2000
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_BurglaryJob_BridgeStreet
					SWITCH iVehicle
						CASE 0			RETURN 76.9999
						CASE 1			RETURN 76.9999
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_BurglaryJob_NikolaAvenue
					SWITCH iVehicle
						CASE 0			RETURN 25.8000
						CASE 1			RETURN 27.0000
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_BurglaryJob_NorthSheldonAvenue
					SWITCH iVehicle
						CASE 0			RETURN 229.7992
						CASE 1			RETURN 44.3990
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_BurglaryJob_InesenoRoad
					SWITCH iVehicle
						CASE 0			RETURN 75.5998
						CASE 1			RETURN 254.9997
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_FLIGHT_RECORDER
			SWITCH eLocation
				CASE GangOpsLocation_FlightRecorder_Dam
					SWITCH iVehicle
						CASE 1		RETURN 30.6000
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlightRecorder_Palomino
					SWITCH iVehicle
						CASE 1		RETURN 15.0000
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlightRecorder_Terminal
					SWITCH iVehicle
						CASE 1		RETURN 205.3998
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlightRecorder_PacificBluffs
					SWITCH iVehicle
						CASE 1		RETURN 187.2000
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlightRecorder_MountGordo
					SWITCH iVehicle
						CASE 1		RETURN 69.3999
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlightRecorder_AlamoSea1
					SWITCH iVehicle
						CASE 1		RETURN 84.1995
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlightRecorder_AlamoSea2
					SWITCH iVehicle
						CASE 1		RETURN 129.3998
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlightRecorder_AlamoSea3
					SWITCH iVehicle
						CASE 1		RETURN 87.9994
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlightRecorder_PaletoBay
					SWITCH iVehicle
						CASE 1		RETURN 243.3998
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlightRecorder_FortZancudo
					SWITCH iVehicle
						CASE 1		RETURN 246.9997
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_GONE_BALLISTIC
			SWITCH eLocation
				CASE GangOpsLocation_GoneBallistic_ElysianIsland1
					SWITCH iVehicle
						CASE 0			RETURN 329.1970
						CASE 1			RETURN 264.1960
						CASE 2			RETURN 324.9960
						CASE 3			RETURN 334.3950
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_GoneBallistic_GrandSenoraDesert1
					SWITCH iVehicle
						CASE 0			RETURN 319.3480
						CASE 1			RETURN 128.9480
						CASE 2			RETURN 97.7480
						CASE 3			RETURN 336.0000
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_GoneBallistic_Grapeseed1
					SWITCH iVehicle
						CASE 0			RETURN 128.0760
						CASE 1			RETURN 316.2760
						CASE 2			RETURN 252.8758
						CASE 3			RETURN 261.2759
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_GoneBallistic_ElBurroHeights
					SWITCH iVehicle
						CASE 0			RETURN 70.8720
						CASE 1			RETURN 128.0720
						CASE 2			RETURN 347.4720
						CASE 3			RETURN 227.6719
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_GoneBallistic_Terminal
					SWITCH iVehicle
						CASE 0			RETURN 271.3987
						CASE 1			RETURN 87.7990
						CASE 2			RETURN 359.9999
						CASE 3			RETURN 197.0001
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_GoneBallistic_LSIA
					SWITCH iVehicle
						CASE 0			RETURN 3.3173
						CASE 1			RETURN 140.3171
						CASE 2			RETURN 289.1168
						CASE 3			RETURN 256.1166
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_GoneBallistic_MurrietaHeights
					SWITCH iVehicle
						CASE 0			RETURN 219.1949
						CASE 1			RETURN 82.3945
						CASE 2			RETURN 270.9942
						CASE 3			RETURN 213.1993
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_GoneBallistic_ElysianIsland2
					SWITCH iVehicle
						CASE 0			RETURN 44.9980
						CASE 1			RETURN 245.5970
						CASE 2			RETURN 108.5980
						CASE 3			RETURN 45.5979
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_GoneBallistic_PaletoBay
					SWITCH iVehicle
						CASE 0			RETURN 259.2015
						CASE 1			RETURN 230.8018
						CASE 2			RETURN 144.0008
						CASE 3			RETURN 160.8006
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_GoneBallistic_FortZancudo
					SWITCH iVehicle
						CASE 0			RETURN 216.2501
						CASE 1			RETURN 144.0501
						CASE 2			RETURN 134.4492
						CASE 3			RETURN 237.4490
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

////////////////////////////////////////
/// 		 VARIATION VEHICLE MODEL ///
////////////////////////////////////////

FUNC MODEL_NAMES GET_GANGOPS_AMBUSH_VEHICLE_MODEL(GANGOPS_VARIATION eVariation, GANGOPS_SUBVARIATION eSubvariation, INT iAmbushArrayIndex, BOOL bAlternateMissionCondition = FALSE)
	UNUSED_PARAMETER(eSubvariation)
	SWITCH eVariation
		CASE GOV_DEAD_DROP
			SWITCH iAmbushArrayIndex
				CASE 0		RETURN fbi2
				CASE 1		RETURN fbi2
			ENDSWITCH
		BREAK
		CASE GOV_AUTO_SALVAGE
			IF bAlternateMissionCondition
				SWITCH iAmbushArrayIndex
					CASE 0	RETURN BANSHEE2
					CASE 1	RETURN SULTANRS
				ENDSWITCH
			ENDIF
			RETURN SCHAFTER2
		CASE GOV_AQUALUNGS
			RETURN FROGGER
		CASE GOV_FORCED_ENTRY
			RETURN BUCCANEER2
		CASE GOV_BURGLARY_JOB
			RETURN Dubsta3
		CASE GOV_GONE_BALLISTIC
			RETURN MESA3
		CASE GOV_DAYLIGHT_ROBBERY
			RETURN XLS2
		CASE GOV_FLIGHT_RECORDER
			RETURN BUZZARD
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

//FUNC MODEL_NAMES GET_VEH_MODEL_FLARE_UP(BOOL bBikerGang)
//
//	IF bBikerGang 
//	
//		RETURN SLAMVAN2
//	ENDIF
//	
//	RETURN FUGITIVE
//ENDFUNC
  
FUNC MODEL_NAMES GET_GANGOPS_VEHICLE_MODEL(GANGOPS_VARIATION eVariation, GANGOPS_SUBVARIATION eSubvariation1, INT iVehicle, BOOL bAlternateMissionCondition = FALSE)
//	BOOL bBikerGang = GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
	UNUSED_PARAMETER(iVehicle)

	SWITCH eVariation
		CASE GOV_FLARE_UP
			SWITCH eSubvariation1
				CASE GOS_FU_CHAMBERLAIN_HILLS
					SWITCH iVehicle
						CASE 0		RETURN POLICE3
						CASE 1		RETURN POLICE3
						CASE 2		RETURN POLICE3
						CASE 3		RETURN POLICE3
						CASE 4		RETURN FUGITIVE
						CASE 5		RETURN FUGITIVE
					ENDSWITCH
				BREAK
				CASE GOS_FU_MORNINGWOOD
					SWITCH iVehicle
						CASE 0		RETURN FUGITIVE
						CASE 1		RETURN FUGITIVE
						CASE 2		RETURN police3
						CASE 3		RETURN police3
						CASE 4		RETURN police3
						CASE 5		RETURN police3
					ENDSWITCH
				BREAK
				CASE GOS_FU_LA_PUERTA
					SWITCH iVehicle
						CASE 0		RETURN POLICE3
						CASE 1		RETURN POLICE3
						CASE 2		RETURN POLICE3
						CASE 3		RETURN POLICE3
						CASE 4		RETURN FUGITIVE
						CASE 5		RETURN FUGITIVE
					ENDSWITCH
				BREAK
				CASE GOS_FU_EL_BURRO
					SWITCH iVehicle
						CASE 0		RETURN POLICE3
						CASE 1		RETURN POLICE3
						CASE 2		RETURN POLICE3
						CASE 3		RETURN POLICE3
						CASE 4		RETURN FUGITIVE
						CASE 5		RETURN FUGITIVE
					ENDSWITCH
				BREAK
				CASE GOS_FU_DAVIS
					SWITCH iVehicle
						CASE 0		RETURN FUGITIVE
						CASE 1		RETURN police3
						CASE 2		RETURN police3
						CASE 3		RETURN police3
						CASE 4		RETURN police3
						CASE 5		RETURN FUGITIVE
					ENDSWITCH
				BREAK
				CASE GOS_FU_SANDY
					SWITCH iVehicle
						CASE 0		RETURN FUGITIVE
						CASE 1		RETURN FUGITIVE
						CASE 2		RETURN sheriff
						CASE 3		RETURN sheriff
						CASE 4		RETURN sheriff
						CASE 5		RETURN sheriff
					ENDSWITCH
				BREAK
				CASE GOS_FU_PALETO
					SWITCH iVehicle
						CASE 0		RETURN sheriff
						CASE 1		RETURN sheriff
						CASE 2		RETURN sheriff
						CASE 3		RETURN sheriff
						CASE 4		RETURN FUGITIVE
						CASE 5		RETURN FUGITIVE
					ENDSWITCH
				BREAK
				CASE GOS_FU_CAPITAL
					SWITCH iVehicle
						CASE 0		RETURN POLICE3
						CASE 1		RETURN POLICE3
						CASE 2		RETURN POLICE3
						CASE 3		RETURN POLICE3
						CASE 4		RETURN FUGITIVE
						CASE 5		RETURN FUGITIVE
					ENDSWITCH
				BREAK
				CASE GOS_FU_CLINTON
					SWITCH iVehicle
						CASE 0		RETURN FUGITIVE
						CASE 1		RETURN FUGITIVE
						CASE 2		RETURN police3
						CASE 3		RETURN police3
						CASE 4		RETURN police3
						CASE 5		RETURN police3
					ENDSWITCH
				BREAK
				CASE GOS_FU_RICHMAN
					SWITCH iVehicle
						CASE 0		RETURN FUGITIVE
						CASE 1		RETURN FUGITIVE
						CASE 2		RETURN police3
						CASE 3		RETURN police3
						CASE 4		RETURN police3
						CASE 5		RETURN police3
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE GOV_FORCED_ENTRY
			SWITCH eSubvariation1
				CASE GOS_FE_LOCATION_FUDGE	
					SWITCH iVehicle
						CASE 0		RETURN BMX
						CASE 1		RETURN BMX
						CASE 2		RETURN BUCCANEER2
						CASE 3		RETURN BUCCANEER2
						CASE 4		RETURN BUCCANEER2
						CASE 5		RETURN OPPRESSOR
						CASE 6		RETURN OPPRESSOR
						CASE 7		RETURN OPPRESSOR
						CASE 8		RETURN OPPRESSOR
					ENDSWITCH
				BREAK
				CASE GOS_FE_LOCATION_MAGELLAN
					SWITCH iVehicle
						CASE 0		RETURN BMX
						CASE 1		RETURN BMX
						CASE 2		RETURN BUCCANEER2
						CASE 3		RETURN BUCCANEER2
						CASE 4		RETURN BUCCANEER2
						CASE 5		RETURN OPPRESSOR
						CASE 6		RETURN OPPRESSOR
						CASE 7		RETURN OPPRESSOR
						CASE 8		RETURN OPPRESSOR
					ENDSWITCH
				BREAK
				CASE GOS_FE_LOCATION_FORUM
					SWITCH iVehicle
						CASE 0		RETURN BMX
						CASE 1		RETURN BUCCANEER2
						CASE 2		RETURN BMX
						CASE 3		RETURN BUCCANEER2
						CASE 4		RETURN BUCCANEER2
						CASE 5		RETURN OPPRESSOR
						CASE 6		RETURN OPPRESSOR
						CASE 7		RETURN OPPRESSOR
						CASE 8		RETURN OPPRESSOR
					ENDSWITCH
				BREAK
				CASE GOS_FE_LOCATION_ROY_LOWENSTEIN
					SWITCH iVehicle
						CASE 0		RETURN BMX
						CASE 1		RETURN BUCCANEER2
						CASE 2		RETURN BMX
						CASE 3		RETURN BUCCANEER2
						CASE 4		RETURN BUCCANEER2
						CASE 5		RETURN OPPRESSOR
						CASE 6		RETURN OPPRESSOR
						CASE 7		RETURN OPPRESSOR
						CASE 8		RETURN OPPRESSOR
					ENDSWITCH
				BREAK
				CASE GOS_FE_LOCATION_EASTBOURNE
					SWITCH iVehicle
						CASE 0		RETURN BMX
						CASE 1		RETURN BUCCANEER2
						CASE 2		RETURN BUCCANEER2
						CASE 3		RETURN BMX
						CASE 4		RETURN BUCCANEER2
						CASE 5		RETURN OPPRESSOR
						CASE 6		RETURN OPPRESSOR
						CASE 7		RETURN OPPRESSOR
						CASE 8		RETURN OPPRESSOR
					ENDSWITCH
				BREAK
				CASE GOS_FE_LOCATION_V_MAGELLAN
				CASE GOS_FE_LOCATION_ALTA
				CASE GOS_FE_LOCATION_FORUM_DRIVE
				CASE GOS_FE_LOCATION_SPANISH
				CASE GOS_FE_LOCATION_TAHITIAN
					SWITCH iVehicle
						CASE 0		RETURN BUCCANEER2
						CASE 1		RETURN BMX
						CASE 2		RETURN BMX
						CASE 3		RETURN BUCCANEER2
						CASE 4		RETURN BUCCANEER2
						CASE 5		RETURN OPPRESSOR
						CASE 6		RETURN OPPRESSOR
						CASE 7		RETURN OPPRESSOR
						CASE 8		RETURN OPPRESSOR
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	
		// Steal
		CASE GOV_DEAD_DROP
			RETURN fbi2
			
		CASE GOV_AUTO_SALVAGE
			IF bAlternateMissionCondition
				RETURN WASTELANDER
			ENDIF
			RETURN FLATBED
		
		CASE GOV_CAR_COLLECTOR
			SWITCH iVehicle
				CASE 0		RETURN xls
			ENDSWITCH
		BREAK	
		
		CASE GOV_MOB_MENTALITY
			SWITCH eSubvariation1
				CASE GOS_MM_MORNINGWOOD
					SWITCH iVehicle
						CASE 0			RETURN STRETCH
						CASE 1			RETURN STRETCH
						CASE 2			RETURN STRETCH
					ENDSWITCH
				BREAK
				CASE GOS_MM_GRAPESEED
					SWITCH iVehicle
						CASE 0			RETURN SURFER
						CASE 1			RETURN SURFER
						CASE 2			RETURN REBEL
					ENDSWITCH
				BREAK
				CASE GOS_MM_ALTA
					SWITCH iVehicle
						CASE 0			RETURN dilettante2
						CASE 1			RETURN pcj
						CASE 2			RETURN pcj
					ENDSWITCH
				BREAK
				CASE GOS_MM_MISSION_ROW
					SWITCH iVehicle
						CASE 0			RETURN manchez
						CASE 1			RETURN manchez
						CASE 2			RETURN dilettante2
					ENDSWITCH
				BREAK
				CASE GOS_MM_HIPPY_CAMP
					SWITCH iVehicle
						CASE 0			RETURN ratloader
						CASE 1			RETURN ratloader
						CASE 2			RETURN SURFER
					ENDSWITCH
				BREAK
				CASE GOS_MM_SAN_ANDREAS
					SWITCH iVehicle
						CASE 0			RETURN dilettante2
						CASE 1			RETURN faggio2
						CASE 2			RETURN faggio2
					ENDSWITCH
				BREAK
				CASE GOS_MM_MIRROR_PARK
					SWITCH iVehicle
						CASE 0			RETURN dilettante2
						CASE 1			RETURN bulldozer
						CASE 2			RETURN MIXER2
					ENDSWITCH
				BREAK
				CASE GOS_MM_STRAWBERRY
					SWITCH iVehicle
						CASE 0			RETURN dilettante2
						CASE 1			RETURN EMPEROR
						CASE 2			RETURN EMPEROR
					ENDSWITCH
				BREAK
				CASE GOS_MM_MIRROR_PARK_TAVERN
					SWITCH iVehicle
						CASE 0			RETURN BfInjection
						CASE 1			RETURN Huntley
						CASE 2			RETURN JOURNEY
					ENDSWITCH
				BREAK
				CASE GOS_MM_ULSA
					SWITCH iVehicle
						CASE 0			RETURN caddy
						CASE 1			RETURN CRUISER
						CASE 2			RETURN CRUISER
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_PARAMEDIC
			SWITCH eSubvariation1
				CASE GOS_PM_CENTRAL_LS_MEDICAL_CENTER
				CASE GOS_PM_PILLBOX_HILL_MEDICAL_CENTER
					SWITCH iVehicle
						CASE 0			RETURN DILETTANTE2
						CASE 1			RETURN RUMPO2
					ENDSWITCH
				BREAK
				CASE GOS_PM_ST_FIACRE_HOSPITAL
				CASE GOS_PM_SANDY_SHORES_MEDICAL_CENTER
				CASE GOS_PM_PALETO_BAY_CARE_CENTER
					SWITCH iVehicle
						CASE 0			RETURN RUMPO2
						CASE 1			RETURN DILETTANTE2
					ENDSWITCH
				BREAK
				CASE GOS_PM_DEL_PERRO_FREEWAY
					SWITCH iVehicle
						CASE 0			RETURN JACKAL
						CASE 1			RETURN POLICE3
					ENDSWITCH
				BREAK
				CASE GOS_PM_MOUNT_GORDO
					SWITCH iVehicle
						CASE 0			RETURN RHAPSODY
						CASE 1			RETURN SHERIFF
					ENDSWITCH
				BREAK
				CASE GOS_PM_GREAT_CHAPARRAL
					SWITCH iVehicle
						CASE 0			RETURN FUGITIVE
						CASE 1			RETURN SHERIFF
					ENDSWITCH
				BREAK
				CASE GOS_PM_CASSIDY_CREEK
					SWITCH iVehicle
						CASE 0			RETURN BODHI2
						CASE 1			RETURN SHERIFF
					ENDSWITCH
				BREAK
				CASE GOS_PM_POWER_STATION
					SWITCH iVehicle
						CASE 0			RETURN BJXL
						CASE 1			RETURN SHERIFF
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_AQUALUNGS
			SWITCH eSubvariation1
				CASE GOS_AL_ELYSIAN_ISLAND_1
				CASE GOS_AL_PALETO_BAY
				CASE GOS_AL_CHUMASH
				CASE GOS_AL_NORTH_CHUMASH
				CASE GOS_AL_PACIFIC_BLUFFS
				CASE GOS_AL_VESPUCCI_BEACH
					SWITCH iVehicle
						CASE 0			RETURN SEASHARK
						CASE 1			RETURN SEASHARK
						CASE 2			RETURN FROGGER
						CASE 3			RETURN FROGGER			// Ambush
						CASE 4			RETURN FROGGER			// Ambush
					
						// Support
						CASE 5
							IF bAlternateMissionCondition
								RETURN BLAZER5
							ENDIF
							RETURN SEASHARK
							
						CASE 6			
							IF bAlternateMissionCondition
								RETURN BLAZER5
							ENDIF
							RETURN SEASHARK
							
						CASE 7			
							IF bAlternateMissionCondition
								RETURN BLAZER5
							ENDIF
							RETURN SEASHARK
							
						CASE 8			
							IF bAlternateMissionCondition
								RETURN BLAZER5
							ENDIF
							RETURN SEASHARK
					ENDSWITCH
				BREAK
				CASE GOS_AL_ELYSIAN_ISLAND_2
				CASE GOS_AL_PALOMINO_HIGHLANDS
				CASE GOS_AL_SAN_CHIANSKI_MOUNTAIN_RANGE
				CASE GOS_AL_PALETO_FOREST
					SWITCH iVehicle
						CASE 0			RETURN FROGGER
						CASE 1			RETURN SEASHARK
						CASE 2			RETURN SEASHARK
						CASE 3			RETURN FROGGER			// Ambush
						CASE 4			RETURN FROGGER			// Ambush
					
						// Support
						CASE 5
							IF bAlternateMissionCondition
								RETURN BLAZER5
							ENDIF
							RETURN SEASHARK
							
						CASE 6			
							IF bAlternateMissionCondition
								RETURN BLAZER5
							ENDIF
							RETURN SEASHARK
							
						CASE 7			
							IF bAlternateMissionCondition
								RETURN BLAZER5
							ENDIF
							RETURN SEASHARK
							
						CASE 8			
							IF bAlternateMissionCondition
								RETURN BLAZER5
							ENDIF
							RETURN SEASHARK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_UNDER_CONTROL
			SWITCH iVehicle
				CASE 0			RETURN INSURGENT2
				CASE 1			RETURN TULA
			ENDSWITCH
		BREAK
		
		CASE GOV_BURGLARY_JOB
			SWITCH iVehicle
				CASE 0			RETURN baller5
				CASE 1			RETURN baller5
			ENDSWITCH
		BREAK
		
		CASE GOV_FLIGHT_RECORDER		
			SWITCH iVehicle
				CASE 0			RETURN HYDRA
				CASE 1			RETURN TROPIC
			ENDSWITCH
		BREAK

		CASE GOV_GONE_BALLISTIC
			SWITCH iVehicle
				CASE 0			RETURN MESA3
				CASE 1			RETURN MESA3
				CASE 2			RETURN MESA3
				CASE 3			RETURN RHINO
			ENDSWITCH
		BREAK	
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

/////////////////////////////////
/// MISSION PED SPAWN COORDS ///
/////////////////////////////////
   

FUNC VECTOR FORCED_ENTRY_COVER_POS()
	RETURN <<602.1050, 4750.8330, -60.0086>>
ENDFUNC

FUNC VECTOR FORCED_ENTRY_LATE_PED_SPAWN_POS1()
	RETURN <<606.1780, 4739.9448, -62.0086>>
ENDFUNC

FUNC FLOAT FORCED_ENTRY_COVER_HEADING()
	RETURN 282.5447
ENDFUNC

FUNC VECTOR GET_GANGOPS_PED_SPAWN_COORDS(INT iPed, GANGOPS_LOCATION eLocation, GANGOPS_VARIATION eVariation, FREEMODE_DELIVERY_DROPOFFS eDropOff = FREEMODE_DELIVERY_DROPOFF_INVALID, INT iRespawnLocation = 0)

	UNUSED_PARAMETER(eDropOff)
	UNUSED_PARAMETER(iRespawnLocation)

	SWITCH eVariation
		CASE GOV_MAX			
			SWITCH eLocation
				CASE GangOpsLocation_Max
					SWITCH iPed
						CASE 0			RETURN <<1743.8560, 3296.6101, 40.1170>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK	
		CASE GOV_DAYLIGHT_ROBBERY
			SWITCH eLocation
				CASE GangOpsLocation_DaylightRobbery_MirrorPark
					SWITCH iPed
						CASE 0			RETURN <<1177.9672, -410.8847, 66.5979>>
						CASE 1			RETURN <<1180.1439, -413.8670, 66.5541>>
						CASE 2			RETURN <<1186.0314, -389.2472, 67.3549>>
						CASE 3			RETURN <<1182.9230, -394.7327, 67.0350>>
						CASE 4			RETURN <<1184.0015, -394.4294, 67.0496>>
						CASE 5			RETURN <<1170.0752, -400.2971, 70.5896>>
						CASE 6			RETURN <<1175.5010, -397.1472, 66.8495>>
						CASE 7			RETURN <<1175.2380, -398.1803, 66.8398>>
						CASE 8			RETURN <<1177.7870, -402.9529, 66.8418>>
						CASE 9			RETURN <<1181.0178, -400.1154, 66.9255>>
						CASE 10			RETURN <<1175.7223, -408.3553, 66.6858>>
						CASE 11			RETURN <<1182.2750, -404.3900, 66.8356>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DaylightRobbery_SanAndreas
					SWITCH iPed
						CASE 0			RETURN <<-1266.9680, -880.3270, 10.9300>>
						CASE 1			RETURN <<-1283.2230, -873.2780, 10.9300>>
						CASE 2			RETURN <<-1280.7321, -879.6770, 10.9290>>
						CASE 3			RETURN <<-1272.6500, -881.1280, 10.9300>>
						CASE 4			RETURN <<-1279.0990, -876.6960, 10.9300>>
						CASE 5			RETURN <<-1282.6331, -874.1270, 10.9300>>
						CASE 6			RETURN <<-1266.8361, -879.2510, 10.9300>>
						CASE 7			RETURN <<-1275.4570, -883.1960, 10.9300>>
						CASE 8			RETURN <<-1274.0480, -883.3680, 10.9300>>
						CASE 9			RETURN <<-1268.5300, -883.2460, 10.9300>>
						CASE 10			RETURN <<-1276.6169, -880.0230, 10.9300>>
						CASE 11			RETURN <<-1274.2590, -887.1280, 10.9290>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DaylightRobbery_Vespucci
					SWITCH iPed
						CASE 0			RETURN <<-1207.0920, -1123.2140, 6.8020>>
						CASE 1			RETURN <<-1198.9690, -1117.8970, 6.6720>>
						CASE 2			RETURN <<-1186.1700, -1129.9740, 6.6720>>
						CASE 3			RETURN <<-1191.2550, -1134.0540, 6.6720>>
						CASE 4			RETURN <<-1190.0680, -1133.6150, 6.6720>>
						CASE 5			RETURN <<-1193.5190, -1125.0120, 6.6750>>
						CASE 6			RETURN <<-1207.6110, -1124.3580, 6.8290>>
						CASE 7			RETURN <<-1199.8149, -1120.8530, 6.6930>>
						CASE 8			RETURN <<-1194.7030, -1125.5630, 6.6800>>
						CASE 9			RETURN <<-1198.4650, -1121.3459, 6.6910>>
						CASE 10			RETURN <<-1196.0270, -1130.3361, 6.6890>>
						CASE 11			RETURN <<-1203.1190, -1123.0551, 6.7150>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DaylightRobbery_LegionSq
					SWITCH iPed
						CASE 0			RETURN <<264.9310, -982.3940, 28.3580>>
						CASE 1			RETURN <<264.4840, -980.3570, 28.3620>>
						CASE 2			RETURN <<258.7790, -985.1230, 28.3210>>
						CASE 3			RETURN <<263.4690, -978.1010, 28.3530>>
						CASE 4			RETURN <<265.5820, -977.1800, 28.3460>>
						CASE 5			RETURN <<260.4180, -980.3350, 28.3590>>
						CASE 6			RETURN <<263.4530, -980.6580, 28.3650>>
						CASE 7			RETURN <<263.8850, -977.3160, 28.3490>>
						CASE 8			RETURN <<261.2010, -980.7390, 28.3590>>
						CASE 9			RETURN <<260.3930, -981.3460, 28.3520>>
						CASE 10			RETURN <<259.4610, -985.8660, 28.3170>>
						CASE 11			RETURN <<262.5980, -985.5600, 28.3290>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DaylightRobbery_DelPerro
					SWITCH iPed
						CASE 0			RETURN <<-860.4036, -1146.6510, 5.1285>>
						CASE 1			RETURN <<-854.4824, -1139.1771, 5.9933>>
						CASE 2			RETURN <<-855.7938, -1143.7957, 5.2700>>
						CASE 3			RETURN <<-863.0139, -1143.6294, 5.9913>>
						CASE 4			RETURN <<-864.9890, -1139.8080, 5.9974>>
						CASE 5			RETURN <<-867.0667, -1139.3479, 5.9944>>
						CASE 6			RETURN <<-855.7711, -1142.6377, 5.2615>>
						CASE 7			RETURN <<-858.7530, -1144.4255, 5.9978>>
						CASE 8			RETURN <<-861.9677, -1143.5472, 5.9936>>
						CASE 9			RETURN <<-862.5610, -1144.5374, 5.9898>>
						CASE 10			RETURN <<-864.1413, -1140.8390, 5.9963>>
						CASE 11			RETURN <<-860.2674, -1141.5282, 6.0024>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DaylightRobbery_Rockford
					SWITCH iPed
						CASE 0			RETURN <<-836.4240, -341.6600, 37.6800>>
						CASE 1			RETURN <<-834.0600, -352.8390, 37.6800>>
						CASE 2			RETURN <<-838.3720, -348.0770, 37.6800>>
						CASE 3			RETURN <<-841.4490, -355.8940, 37.6800>>
						CASE 4			RETURN <<-833.0240, -345.8050, 37.6800>>
						CASE 5			RETURN <<-843.4530, -348.9960, 37.6800>>
						CASE 6			RETURN <<-840.1240, -355.2330, 37.6800>>
						CASE 7			RETURN <<-837.9650, -348.8880, 37.6800>>
						CASE 8			RETURN <<-839.2080, -348.7560, 37.6800>>
						CASE 9			RETURN <<-833.5640, -346.7310, 37.6800>>
						CASE 10			RETURN <<-835.3790, -341.6850, 37.6800>>
						CASE 11			RETURN <<-840.6750, -345.0640, 37.6800>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DaylightRobbery_WestVinewood
					SWITCH iPed
						CASE 0			RETURN <<-630.2640, 241.0330, 80.8940>>
						CASE 1			RETURN <<-622.0310, 241.9750, 80.8950>>
						CASE 2			RETURN <<-635.3890, 245.3170, 80.8950>>
						CASE 3			RETURN <<-623.8770, 243.4710, 80.8950>>
						CASE 4			RETURN <<-627.6530, 243.7860, 80.8940>>
						CASE 5			RETURN <<-629.3880, 241.3020, 80.8920>>
						CASE 6			RETURN <<-630.1750, 241.9200, 80.8940>>
						CASE 7			RETURN <<-624.8740, 243.2620, 80.8950>>
						CASE 8			RETURN <<-635.4830, 244.4970, 80.8950>>
						CASE 9			RETURN <<-630.0840, 246.9450, 80.6060>>
						CASE 10			RETURN <<-632.9940, 240.1240, 80.8950>>
						CASE 11			RETURN <<-628.0930, 244.6500, 80.8650>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DaylightRobbery_Burton
					SWITCH iPed
						CASE 0			RETURN <<-519.5505, -26.2107, 44.6140>>
						CASE 1			RETURN <<-514.1493, -26.2024, 44.6140>>
						CASE 2			RETURN <<-522.4080, -28.3258, 44.6140>>
						CASE 3			RETURN <<-509.6910, -21.3245, 44.6140>>
						CASE 4			RETURN <<-521.9390, -22.3246, 43.6039>>
						CASE 5			RETURN <<-514.6408, -21.7301, 44.6140>>
						CASE 6			RETURN <<-521.9693, -21.2139, 43.6040>>
						CASE 7			RETURN <<-520.5085, -25.4976, 44.6140>>
						CASE 8			RETURN <<-514.2043, -22.8295, 44.6140>>
						CASE 9			RETURN <<-513.6570, -21.8488, 44.6140>>
						CASE 10			RETURN <<-510.6325, -20.8797, 44.6140>>
						CASE 11			RETURN <<-518.2557, -20.7976, 44.6140>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DaylightRobbery_DelPerroPlaza
					SWITCH iPed
						CASE 0			RETURN <<-1547.9041, -441.2025, 34.8870>>
						CASE 1			RETURN <<-1549.3060, -437.2910, 34.8870>>
						CASE 2			RETURN <<-1544.0765, -433.7490, 34.5970>>
						CASE 3			RETURN <<-1542.5881, -447.3071, 34.9294>>
						CASE 4			RETURN <<-1545.0690, -439.8398, 34.8867>>
						CASE 5			RETURN <<-1544.3540, -443.8939, 34.8870>>
						CASE 6			RETURN <<-1545.4078, -438.9643, 34.8867>>
						CASE 7			RETURN <<-1548.4867, -437.9734, 34.8870>>
						CASE 8			RETURN <<-1548.2749, -436.9392, 34.8867>>
						CASE 9			RETURN <<-1546.3345, -433.5431, 34.8867>>
						CASE 10			RETURN <<-1543.3896, -444.6070, 34.8870>>
						CASE 11			RETURN <<-1551.6620, -438.6929, 34.8870>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DaylightRobbery_Pillbox
					SWITCH iPed
						CASE 0			RETURN <<59.0130, -799.4840, 30.5710>>
						CASE 1			RETURN <<54.7840, -796.8530, 30.5850>>
						CASE 2			RETURN <<46.8250, -795.1850, 30.6030>>
						CASE 3			RETURN <<41.8170, -807.1380, 30.4280>>
						CASE 4			RETURN <<54.4630, -797.9600, 30.5870>>
						CASE 5			RETURN <<53.8910, -797.0440, 30.5890>>
						CASE 6			RETURN <<42.2280, -805.7070, 30.4540>>
						CASE 7			RETURN <<47.9780, -794.1680, 30.6140>>
						CASE 8			RETURN <<46.1490, -801.5840, 30.5600>>
						CASE 9			RETURN <<41.8550, -799.3800, 30.5480>>
						CASE 10			RETURN <<41.5300, -798.0840, 30.5660>>
						CASE 11			RETURN <<42.5870, -798.5170, 30.5670>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE GOV_FLARE_UP			
			SWITCH eLocation
				CASE GangOpsLocation_FlareUp_Chamberlain_Hills
					SWITCH iPed
						CASE 0			RETURN <<-314.0860, -1485.5240, 29.5490>>
						CASE 1			RETURN <<-321.2590, -1487.4760, 29.5720>>
						CASE 2			RETURN <<-308.1290, -1481.2240, 29.4730>>
						CASE 3			RETURN <<-309.3390, -1484.5530, 29.5120>>
						CASE 4			RETURN <<-323.4930, -1454.9940, 29.6990>>
						CASE 5			RETURN <<-325.8327, -1458.0834, 29.5581>>
						CASE 6			RETURN <<-330.2455, -1461.0216, 29.5026>>
						CASE 7			RETURN <<-333.8680, -1461.7650, 29.5060>>
						CASE 8			RETURN <<-316.2900, -1466.0730, 29.5460>>
						CASE 9			RETURN <<-314.0990, -1462.3409, 29.5440>>
						CASE 10			RETURN <<-323.8040, -1477.1980, 29.5490>>
						CASE 11			RETURN <<-326.1710, -1480.9850, 29.5490>>
						CASE 12			RETURN <<-319.7160, -1470.2700, 29.5480>>
						CASE 13			RETURN <<-319.3680, -1473.4189, 29.5490>>
						CASE 14			RETURN <<-317.5480, -1469.3929, 29.5480>>
						CASE 15			RETURN <<-322.4240, -1473.6429, 29.5490>>
						CASE 16			RETURN <<-304.2630, -1493.8361, 29.2310>>
						CASE 17			RETURN <<-310.0250, -1496.0031, 29.3350>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Morningwood
					SWITCH iPed
						CASE 0			RETURN <<-1434.8690, -278.2860, 45.2080>>
						CASE 1			RETURN <<-1435.9280, -271.0720, 45.4030>>
						CASE 2			RETURN <<-1439.1600, -274.4160, 45.2080>>
						CASE 3			RETURN <<-1431.3979, -276.9130, 45.3960>>
						CASE 4			RETURN <<-1442.5280, -276.2650, 45.3990>>
						CASE 5			RETURN <<-1437.5500, -282.0830, 45.3960>>
						CASE 6			RETURN <<-1436.8280, -284.0140, 45.3920>>
						CASE 7			RETURN <<-1438.0120, -271.6680, 45.2080>>
						CASE 8			RETURN <<-1421.8905, -281.7049, 45.2076>>
						CASE 9			RETURN <<-1424.6206, -285.0687, 45.2146>>
						CASE 10			RETURN <<-1427.5852, -287.7065, 45.2139>>
						CASE 11			RETURN <<-1430.4617, -291.9240, 45.1160>>
						CASE 12			RETURN <<-1452.1954, -271.8094, 45.6639>>
						CASE 13			RETURN <<-1447.9304, -268.1865, 45.2868>>
						CASE 14			RETURN <<-1439.3540, -258.7229, 45.2078>>
						CASE 15			RETURN <<-1437.3260, -256.9640, 45.2380>>
						CASE 16			RETURN <<-1445.0804, -262.6208, 45.2266>>
						CASE 17			RETURN <<-1444.6467, -266.8217, 45.2077>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_La_Puerta
					SWITCH iPed
						CASE 0			RETURN <<-517.3139, -1202.5015, 17.6792>>
						CASE 1			RETURN <<-528.9654, -1198.6671, 17.3267>>
						CASE 2			RETURN <<-521.4834, -1201.4409, 17.5334>>
						CASE 3			RETURN <<-511.5761, -1207.7518, 17.5832>>
						CASE 4			RETURN <<-532.6790, -1199.5352, 17.2305>>
						CASE 5			RETURN <<-515.2285, -1221.7614, 17.4377>>
						CASE 6			RETURN <<-512.1871, -1205.1803, 17.8353>>
						CASE 7			RETURN <<-511.7497, -1214.9580, 17.4773>>
						CASE 8			RETURN <<-526.0443, -1220.5914, 17.3000>>
						CASE 9			RETURN <<-530.8950, -1218.1793, 17.2949>>
						CASE 10			RETURN <<-527.0240, -1215.4680, 17.1850>>
						CASE 11			RETURN <<-527.1791, -1212.2500, 17.1848>>
						CASE 12			RETURN <<-528.4421, -1208.4965, 17.1848>>
						CASE 13			RETURN <<-522.5373, -1209.1161, 17.1848>>
						CASE 14			RETURN <<-526.5756, -1206.9852, 17.1848>>
						CASE 15			RETURN <<-528.0350, -1216.9404, 17.2152>>
						CASE 16			RETURN <<-535.7297, -1204.6786, 17.1919>>
						CASE 17			RETURN <<-538.9269, -1207.7250, 17.0593>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_El_burro
					SWITCH iPed
						CASE 0			RETURN <<1219.7953, -1400.7576, 34.1571>>
						CASE 1			RETURN <<1215.2369, -1392.1509, 34.2240>>
						CASE 2			RETURN <<1202.6024, -1411.0721, 34.2264>>
						CASE 3			RETURN <<1198.4265, -1404.2174, 34.2249>>
						CASE 4			RETURN <<1219.3195, -1397.5781, 34.1875>>
						CASE 5			RETURN <<1215.5020, -1388.5720, 34.3580>>
						CASE 6			RETURN <<1199.5667, -1407.4611, 34.2257>>
						CASE 7			RETURN <<1210.5660, -1402.4850, 34.2240>>
						CASE 8			RETURN <<1206.5000, -1402.2360, 34.2240>>
						CASE 9			RETURN <<1203.3060, -1399.2000, 34.2240>>
						CASE 10			RETURN <<1206.7111, -1396.8300, 34.2240>>
						CASE 11			RETURN <<1204.5940, -1388.9290, 34.2270>>
						CASE 12			RETURN <<1201.3865, -1395.5143, 34.2269>>
						CASE 13			RETURN <<1207.1256, -1392.1927, 34.2256>>
						CASE 14			RETURN <<1209.6287, -1404.4446, 34.2242>>
						CASE 15			RETURN <<1205.9730, -1413.3418, 34.2256>>
						CASE 16			RETURN <<1219.1948, -1390.3864, 34.1888>>
						CASE 17			RETURN <<1220.6000, -1393.3187, 34.1463>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Davis
					SWITCH iPed
						CASE 0			RETURN <<-64.3420, -1774.3881, 27.9355>>
						CASE 1			RETURN <<-68.8351, -1773.8978, 27.7854>>
						CASE 2			RETURN <<-74.1389, -1772.4005, 27.8453>>
						CASE 3			RETURN <<-75.4678, -1767.3945, 28.3330>>
						CASE 4			RETURN <<-70.0241, -1751.7306, 28.5005>>
						CASE 5			RETURN <<-56.2534, -1757.2014, 28.0515>>
						CASE 6			RETURN <<-73.4834, -1753.1974, 28.5688>>
						CASE 7			RETURN <<-63.2687, -1751.7366, 28.3531>>
						CASE 8			RETURN <<-55.1314, -1762.6517, 27.9670>>
						CASE 9			RETURN <<-57.7049, -1767.6801, 27.9778>>
						CASE 10			RETURN <<-56.0855, -1765.9775, 27.9041>>
						CASE 11			RETURN <<-60.0750, -1760.8350, 28.2659>>
						CASE 12			RETURN <<-67.3210, -1761.2660, 28.3190>>
						CASE 13			RETURN <<-64.5019, -1761.6426, 29.0403>>
						CASE 14			RETURN <<-62.8741, -1766.2229, 28.0731>>
						CASE 15			RETURN <<-64.4739, -1760.5204, 28.2067>>
						CASE 16			RETURN <<-64.7142, -1748.1449, 28.3616>>
						CASE 17			RETURN <<-68.4684, -1748.4047, 28.4686>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Sandy
					SWITCH iPed
						CASE 0			RETURN <<2001.5985, 3766.0520, 31.1809>>
						CASE 1			RETURN <<2004.8929, 3764.6931, 31.1808>>
						CASE 2			RETURN <<1995.8665, 3778.2397, 31.1808>>
						CASE 3			RETURN <<2017.4215, 3776.7280, 31.1808>>
						CASE 4			RETURN <<2010.2091, 3785.7319, 31.1809>>
						CASE 5			RETURN <<2012.3839, 3781.5430, 31.1808>>
						CASE 6			RETURN <<1997.7209, 3764.9939, 31.1810>>
						CASE 7			RETURN <<2015.3975, 3779.9504, 31.1808>>
						CASE 8			RETURN <<2009.8888, 3769.0276, 31.1808>>
						CASE 9			RETURN <<2011.1857, 3771.6387, 31.1808>>
						CASE 10			RETURN <<2002.1710, 3779.1956, 31.1808>>
						CASE 11			RETURN <<2004.3572, 3779.5120, 31.1808>>
						CASE 12			RETURN <<2003.7419, 3774.2859, 31.4040>>
						CASE 13			RETURN <<2007.9590, 3776.6550, 31.4041>>
						CASE 14			RETURN <<2007.2285, 3772.1660, 31.1808>>
						CASE 15			RETURN <<2005.2528, 3773.9937, 31.4041>>
						CASE 16			RETURN <<1994.5435, 3766.8774, 31.1808>>
						CASE 17			RETURN <<1997.9868, 3769.4463, 31.1808>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Paleto
					SWITCH iPed
						CASE 0			RETURN <<190.1796, 6596.2500, 30.8468>>
						CASE 1			RETURN <<178.7548, 6591.3789, 30.8538>>
						CASE 2			RETURN <<182.8476, 6592.6201, 30.8600>>
						CASE 3			RETURN <<167.1187, 6611.4604, 30.8760>>
						CASE 4			RETURN <<170.7361, 6615.7261, 30.8730>>
						CASE 5			RETURN <<184.6067, 6616.6470, 30.8041>>
						CASE 6			RETURN <<180.5370, 6618.4785, 30.7850>>
						CASE 7			RETURN <<184.8046, 6595.0215, 30.8494>>
						CASE 8			RETURN <<174.7931, 6600.4917, 30.8491>>
						CASE 9			RETURN <<176.3715, 6597.7480, 30.8478>>
						CASE 10			RETURN <<184.6399, 6602.4390, 30.8493>>
						CASE 11			RETURN <<186.7840, 6605.0220, 31.0470>>
						CASE 12			RETURN <<180.1070, 6601.3960, 31.0470>>
						CASE 13			RETURN <<181.6257, 6603.1753, 30.8489>>
						CASE 14			RETURN <<179.5620, 6606.1831, 31.0480>>
						CASE 15			RETURN <<177.9856, 6600.7949, 30.8491>>
						CASE 16			RETURN <<179.8871, 6613.9937, 30.8263>>
						CASE 17			RETURN <<171.9507, 6611.5234, 30.8657>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Capital
					SWITCH iPed
						CASE 0			RETURN <<267.1202, -1270.8954, 28.1599>>
						CASE 1			RETURN <<270.0890, -1273.5110, 28.1790>>
						CASE 2			RETURN <<285.4156, -1268.9526, 28.2752>>
						CASE 3			RETURN <<278.0208, -1272.2625, 28.2096>>
						CASE 4			RETURN <<273.9484, -1247.5551, 28.1583>>
						CASE 5			RETURN <<278.7066, -1248.2483, 28.1984>>
						CASE 6			RETURN <<271.4084, -1248.2032, 28.1504>>
						CASE 7			RETURN <<266.6418, -1249.5128, 28.1450>>
						CASE 8			RETURN <<281.0830, -1260.6790, 28.2200>>
						CASE 9			RETURN <<277.1450, -1259.6150, 28.1730>>
						CASE 10			RETURN <<274.2490, -1263.3900, 28.2930>>
						CASE 11			RETURN <<267.3438, -1261.0394, 28.1429>>
						CASE 12			RETURN <<271.4526, -1258.5472, 28.1429>>
						CASE 13			RETURN <<265.2010, -1264.0620, 28.2930>>
						CASE 14			RETURN <<273.7210, -1255.1021, 28.2930>>
						CASE 15			RETURN <<273.2886, -1258.7894, 28.1429>>
						CASE 16			RETURN <<272.2953, -1275.4725, 28.2098>>
						CASE 17			RETURN <<277.5349, -1275.6765, 28.2343>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Clinton
					SWITCH iPed
						CASE 0			RETURN <<637.1243, 255.4809, 102.1522>>
						CASE 1			RETURN <<623.9315, 257.3946, 102.0450>>
						CASE 2			RETURN <<629.7783, 253.7913, 102.0606>>
						CASE 3			RETURN <<620.8738, 260.2130, 102.0894>>
						CASE 4			RETURN <<634.2640, 279.3830, 102.0700>>
						CASE 5			RETURN <<636.7685, 277.0450, 102.0773>>
						CASE 6			RETURN <<625.1244, 280.7661, 102.0894>>
						CASE 7			RETURN <<629.2691, 280.3488, 102.0817>>
						CASE 8			RETURN <<631.2370, 263.6090, 102.0890>>
						CASE 9			RETURN <<632.8720, 267.1250, 102.0890>>
						CASE 10			RETURN <<628.9980, 268.4030, 102.0890>>
						CASE 11			RETURN <<623.8770, 268.5960, 102.0890>>
						CASE 12			RETURN <<628.3380, 273.6700, 102.0890>>
						CASE 13			RETURN <<623.1741, 271.5803, 102.0894>>
						CASE 14			RETURN <<627.4410, 270.3120, 102.0890>>
						CASE 15			RETURN <<634.7420, 262.8200, 102.0890>>
						CASE 16			RETURN <<627.3941, 251.7876, 102.0396>>
						CASE 17			RETURN <<622.8217, 254.4351, 102.0324>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Richman
					SWITCH iPed
						CASE 0			RETURN <<819.7630, -1020.2990, 25.1650>>
						CASE 1			RETURN <<814.4530, -1020.3770, 25.1590>>
						CASE 2			RETURN <<828.9530, -1021.7390, 25.5660>>
						CASE 3			RETURN <<822.7410, -1019.1620, 25.1140>>
						CASE 4			RETURN <<806.8630, -1037.1400, 25.4520>>
						CASE 5			RETURN <<828.4470, -1036.7040, 25.7290>>
						CASE 6			RETURN <<811.7550, -1038.5720, 25.5620>>
						CASE 7			RETURN <<824.3920, -1039.3340, 25.6530>>
						CASE 8			RETURN <<821.1880, -1037.4540, 25.5140>>
						CASE 9			RETURN <<814.1620, -1035.9410, 25.4110>>
						CASE 10			RETURN <<824.6360, -1030.1949, 25.3390>>
						CASE 11			RETURN <<821.3180, -1027.5750, 25.2700>>
						CASE 12			RETURN <<820.2090, -1030.2740, 25.2940>>
						CASE 13			RETURN <<828.3240, -1026.6620, 25.5810>>
						CASE 14			RETURN <<812.8690, -1029.5070, 25.2780>>
						CASE 15			RETURN <<817.5480, -1026.9570, 25.2730>>
						CASE 16			RETURN <<816.5480, -1030.6650, 25.2920>>
						CASE 17			RETURN <<809.3710, -1025.9640, 25.2480>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE GOV_FORCED_ENTRY						
			SWITCH eLocation
				CASE GangOpsLocation_ForcedEntry_Location_Fudge				
					SWITCH iPed
						CASE 0			RETURN <<1105.7340, -943.9050, 47.8080>>
						CASE 1			RETURN <<1106.1680, -944.0520, 47.8610>>
						CASE 2			RETURN <<1527.0930, -2470.7371, 61.0890>>
						CASE 3			RETURN <<1528.8390, -2473.9460, 60.6580>>
						CASE 4			RETURN <<851.2880, -2095.1831, 29.2690>>
						CASE 5			RETURN <<851.8410, -2096.3940, 29.2780>>
						CASE 6			RETURN <<1324.8058, -1554.3052, 53.0517>>
						CASE 7			RETURN <<604.7007, 4750.8960, -60.0086>>
						CASE 8			RETURN FORCED_ENTRY_COVER_POS()
						CASE 9			RETURN <<595.0571, 4746.5435, -60.0099>>
						CASE 10			RETURN <<600.3612, 4743.3418, -60.0086>>
						CASE 11			RETURN <<596.7017, 4749.6582, -60.0086>>
						CASE 12			RETURN FORCED_ENTRY_LATE_PED_SPAWN_POS1()
						CASE 13			RETURN <<606.0074, 4741.2969, -61.5356>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Magellan
					SWITCH iPed
						CASE 0			RETURN <<-883.1530, -1046.5920, 3.3300>>
						CASE 1			RETURN <<-886.7270, -1048.7410, 2.1320>>
						CASE 2			RETURN <<-1358.8652, -671.3949, 25.4186>>
						CASE 3			RETURN <<-1358.7130, -671.8732, 25.5378>>
						CASE 4			RETURN <<-572.4490, -985.3000, 21.1790>>
						CASE 5			RETURN <<-571.6660, -984.2970, 21.1790>>
						CASE 6			RETURN <<-1142.6444, -1463.0880, 6.6957>>
						CASE 7			RETURN <<604.7007, 4750.8960, -60.0086>>
						CASE 8			RETURN FORCED_ENTRY_COVER_POS()
						CASE 9			RETURN <<595.0571, 4746.5435, -60.0099>>
						CASE 10			RETURN <<600.3612, 4743.3418, -60.0086>>
						CASE 11			RETURN <<596.7017, 4749.6582, -60.0086>>
						CASE 12			RETURN FORCED_ENTRY_LATE_PED_SPAWN_POS1()
						CASE 13			RETURN <<606.0074, 4741.2969, -61.5356>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Forum
					SWITCH iPed
						CASE 0			RETURN <<-646.3070, -1188.1639, 12.2790>>
						CASE 1			RETURN <<249.0390, -1244.9590, 29.2070>>
						CASE 2			RETURN <<249.2900, -1245.4030, 29.3040>>
						CASE 3			RETURN <<-642.4950, -1185.2230, 12.8460>>
						CASE 4			RETURN <<408.6720, -1926.8820, 23.8530>>
						CASE 5			RETURN <<408.3170, -1928.1080, 23.7800>>
						CASE 6			RETURN <<-162.6320, -1636.0123, 36.2509>>
						CASE 7			RETURN <<604.7007, 4750.8960, -60.0086>>
						CASE 8			RETURN FORCED_ENTRY_COVER_POS()
						CASE 9			RETURN <<595.0571, 4746.5435, -60.0099>>
						CASE 10			RETURN <<600.3612, 4743.3418, -60.0086>>
						CASE 11			RETURN <<596.7017, 4749.6582, -60.0086>>
						CASE 12			RETURN FORCED_ENTRY_LATE_PED_SPAWN_POS1()
						CASE 13			RETURN <<606.0074, 4741.2969, -61.5356>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Roy_Lowenstein
					SWITCH iPed
						CASE 0			RETURN <<21.2270, -1801.0330, 25.9740>>
						CASE 1			RETURN <<590.2150, -2194.1011, 6.4810>>
						CASE 2			RETURN <<590.5230, -2194.7290, 6.1770>>
						CASE 3			RETURN <<15.7930, -1796.0430, 26.7040>>
						CASE 4			RETURN <<853.7760, -1831.8250, 28.1150>>
						CASE 5			RETURN <<852.6650, -1832.8590, 28.1120>>
						CASE 6			RETURN <<459.6240, -1578.6207, 31.7919>>
						CASE 7			RETURN <<604.7007, 4750.8960, -60.0086>>
						CASE 8			RETURN FORCED_ENTRY_COVER_POS()
						CASE 9			RETURN <<597.4970, 4746.9111, -60.0086>>
						CASE 10			RETURN <<600.3612, 4743.3418, -60.0086>>
						CASE 11			RETURN <<596.7017, 4749.6582, -60.0086>>
						CASE 12			RETURN FORCED_ENTRY_LATE_PED_SPAWN_POS1()
						CASE 13			RETURN <<606.0074, 4741.2969, -61.5356>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Eastbourne
					SWITCH iPed
						CASE 0			RETURN <<-701.5190, -436.7000, 34.6780>>
						CASE 1			RETURN <<355.0340, -193.0040, 57.9270>>
						CASE 2			RETURN <<354.9830, -193.4880, 57.8690>>
						CASE 3			RETURN <<-694.6660, -436.7110, 34.6550>>
						CASE 4			RETURN <<-910.1040, -159.1140, 40.8790>>
						CASE 5			RETURN <<-910.5880, -160.1530, 40.8790>>
						CASE 6			RETURN <<-364.7600, 55.7080, 53.4300>>
						CASE 7			RETURN <<604.7007, 4750.8960, -60.0086>>
						CASE 8			RETURN FORCED_ENTRY_COVER_POS()
						CASE 9			RETURN <<597.4970, 4746.9111, -60.0086>>
						CASE 10			RETURN <<600.3612, 4743.3418, -60.0086>>
						CASE 11			RETURN <<596.7017, 4749.6582, -60.0086>>
						CASE 12			RETURN FORCED_ENTRY_LATE_PED_SPAWN_POS1()
						CASE 13			RETURN <<606.0074, 4741.2969, -61.5356>>
					ENDSWITCH
				BREAK
				
				CASE GangOpsLocation_ForcedEntry_Location_V_Magellan				
					SWITCH iPed
						CASE 0			RETURN <<-1285.6591, -1420.7772, 3.5119>>
						CASE 1			RETURN <<-1344.0457, -1267.5690, 4.2455>>
						CASE 2			RETURN <<-995.0416, -1113.0502, 2.2696>>
						CASE 3			RETURN <<-995.0416, -1113.0502, 2.2696>>
						CASE 4			RETURN <<-1434.6056, -969.4302, 6.2947>>
						CASE 5			RETURN <<-1434.4264, -968.1811, 6.2889>>
						CASE 6			RETURN <<-1268.8391, -1295.8781, 7.2909>>
						CASE 7			RETURN <<604.7007, 4750.8960, -60.0086>>
						CASE 8			RETURN FORCED_ENTRY_COVER_POS()
						CASE 9			RETURN <<595.0571, 4746.5435, -60.0099>>
						CASE 10			RETURN <<600.3612, 4743.3418, -60.0086>>
						CASE 11			RETURN <<596.7017, 4749.6582, -60.0086>>
						CASE 12			RETURN FORCED_ENTRY_LATE_PED_SPAWN_POS1()
						CASE 13			RETURN <<606.0074, 4741.2969, -61.5356>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Alta
					SWITCH iPed
						CASE 0			RETURN <<25.8290, -92.9650, 57.0930>>
						CASE 1			RETURN <<608.5040, 78.6530, 91.3290>>
						CASE 2			RETURN <<357.4790, -194.5970, 57.7770>>
						CASE 3			RETURN <<357.4790, -194.5970, 57.7770>>
						CASE 4			RETURN <<116.1390, 278.4050, 108.9740>>
						CASE 5			RETURN <<114.7560, 278.0500, 108.9740>>
						CASE 6			RETURN <<202.2350, -99.3590, 72.2850>>
						CASE 7			RETURN <<604.7007, 4750.8960, -60.0086>>
						CASE 8			RETURN FORCED_ENTRY_COVER_POS()
						CASE 9			RETURN <<595.0571, 4746.5435, -60.0099>>
						CASE 10			RETURN <<600.3612, 4743.3418, -60.0086>>
						CASE 11			RETURN <<596.7017, 4749.6582, -60.0086>>
						CASE 12			RETURN FORCED_ENTRY_LATE_PED_SPAWN_POS1()
						CASE 13			RETURN <<606.0074, 4741.2969, -61.5356>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Forum_Drive
					SWITCH iPed
						CASE 0			RETURN <<0.5088, -1476.0061, 29.9264>>
						CASE 1			RETURN <<84.1004, -1852.9344, 23.9578>>
						CASE 2			RETURN <<-353.7215, -1602.7560, 20.9110>>
						CASE 3			RETURN <<-353.7215, -1602.7560, 20.9110>>
						CASE 4			RETURN <<146.3326, -1521.8635, 28.1416>>
						CASE 5			RETURN <<145.6308, -1520.8506, 28.1416>>
						CASE 6			RETURN <<-144.0155, -1694.8501, 35.1709>>
						CASE 7			RETURN <<604.7007, 4750.8960, -60.0086>>
						CASE 8			RETURN FORCED_ENTRY_COVER_POS()
						CASE 9			RETURN <<595.0571, 4746.5435, -60.0099>>
						CASE 10			RETURN <<600.3612, 4743.3418, -60.0086>>
						CASE 11			RETURN <<596.7017, 4749.6582, -60.0086>>
						CASE 12			RETURN FORCED_ENTRY_LATE_PED_SPAWN_POS1()
						CASE 13			RETURN <<606.0074, 4741.2969, -61.5356>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Spanish
					SWITCH iPed
						CASE 0			RETURN <<-183.4757, 286.9361, 94.2600>>
						CASE 1			RETURN <<-687.0374, -106.0729, 37.4153>>
						CASE 2			RETURN <<126.7504, 90.3278, 81.5919>>
						CASE 3			RETURN <<126.7504, 90.3278, 81.5919>>
						CASE 4			RETURN <<-362.8526, 268.6968, 83.6497>>
						CASE 5			RETURN <<-361.7039, 268.5218, 83.6840>>
						CASE 6			RETURN <<-355.5171, 84.9435, 69.5252>>
						CASE 7			RETURN <<604.7007, 4750.8960, -60.0086>>
						CASE 8			RETURN FORCED_ENTRY_COVER_POS()
						CASE 9			RETURN <<597.4970, 4746.9111, -60.0086>>
						CASE 10			RETURN <<600.3612, 4743.3418, -60.0086>>
						CASE 11			RETURN <<596.7017, 4749.6582, -60.0086>>
						CASE 12			RETURN FORCED_ENTRY_LATE_PED_SPAWN_POS1()
						CASE 13			RETURN <<606.0074, 4741.2969, -61.5356>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Tahitian
					SWITCH iPed
						CASE 0			RETURN <<310.1522, -1716.9768, 28.6563>>
						CASE 1			RETURN <<-91.5852, -1386.1493, 29.1907>>
						CASE 2			RETURN <<-431.1589, -1829.1895, 20.6682>>
						CASE 3			RETURN <<-432.0234, -1829.5726, 20.4655>>
						CASE 4			RETURN <<436.3523, -1483.7844, 28.2713>>
						CASE 5			RETURN <<437.2502, -1484.5437, 28.2843>>
						CASE 6			RETURN <<-28.5310, -1544.3412, 32.8264>>
						CASE 7			RETURN <<604.7007, 4750.8960, -60.0086>>
						CASE 8			RETURN FORCED_ENTRY_COVER_POS()
						CASE 9			RETURN <<597.4970, 4746.9111, -60.0086>>
						CASE 10			RETURN <<600.3612, 4743.3418, -60.0086>>
						CASE 11			RETURN <<596.7017, 4749.6582, -60.0086>>
						CASE 12			RETURN FORCED_ENTRY_LATE_PED_SPAWN_POS1()
						CASE 13			RETURN <<606.0074, 4741.2969, -61.5356>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_CAR_COLLECTOR
			// Garage
			SWITCH iPed
				CASE 20			RETURN <<630.6110, 4755.0332, -60.0000>>
				CASE 21			RETURN <<635.8645, 4754.4385, -60.0000>>
				CASE 22			RETURN <<621.8602, 4752.8071, -60.0000>>
			ENDSWITCH
			
			SWITCH eLocation
				CASE GangOpsLocation_CarCollector_SteeleWay
					SWITCH iPed
						CASE 0			RETURN <<-1050.4648, 224.9237, 62.7664>>
						CASE 1			RETURN <<-1051.3647, 224.4688, 62.7669>>
						CASE 2			RETURN <<-1038.2321, 222.0810, 63.3760>>
						CASE 3			RETURN <<-1055.2430, 225.9210, 63.1650>>
						CASE 4			RETURN <<-1049.3101, 211.1450, 62.6870>>
						CASE 5			RETURN <<-1053.5480, 237.8830, 63.1650>>
						CASE 6			RETURN <<-1004.2830, 239.4730, 65.4870>>
						CASE 7			RETURN <<-1027.0410, 214.8040, 63.5640>>
						CASE 8			RETURN <<-1042.9620, 237.5500, 63.1650>>
						CASE 9			RETURN <<-1006.1820, 227.3970, 64.2700>>
						CASE 10			RETURN <<-1006.4340, 228.3960, 64.2700>>
						CASE 11			RETURN <<-1023.6310, 237.7370, 63.5640>>
						CASE 12			RETURN <<-1044.0699, 211.8680, 62.6430>>
						CASE 13			RETURN <<-1052.9800, 215.3000, 62.7670>>
						CASE 14			RETURN <<-1024.2640, 226.7260, 67.2350>>
						CASE 15			RETURN <<-1025.4467, 222.0013, 63.6823>>
						CASE 16			RETURN <<-1024.4377, 222.9549, 63.6877>>
						CASE 17			RETURN <<-1011.3500, 213.8750, 64.2700>>
						CASE 18			RETURN <<-1012.2370, 210.6130, 64.4700>>
						CASE 19			RETURN <<-1011.1350, 211.1060, 64.4700>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_CarCollector_AmericanoWay
					SWITCH iPed
						CASE 0			RETURN <<-1570.4800, 23.2200, 58.5540>>
						CASE 1			RETURN <<-1552.9189, 15.0960, 61.1770>>
						CASE 2			RETURN <<-1579.1350, 13.1980, 60.0820>>
						CASE 3			RETURN <<-1587.7230, 15.6370, 60.2590>>
						CASE 4			RETURN <<-1597.5980, 21.6180, 60.0820>>
						CASE 5			RETURN <<-1585.2209, 35.5860, 58.9920>>
						CASE 6			RETURN <<-1593.6320, 4.8730, 59.8930>>
						CASE 7			RETURN <<-1594.7610, 5.6580, 59.8930>>
						CASE 8			RETURN <<-1563.3070, -4.7940, 58.3730>>
						CASE 9			RETURN <<-1565.9742, 24.9743, 58.5541>>
						CASE 10			RETURN <<-1579.0548, 6.8058, 59.8912>>
						CASE 11			RETURN <<-1579.6293, 5.9574, 59.8916>>
						CASE 12			RETURN <<-1557.1899, 23.1060, 57.6270>>
						CASE 13			RETURN <<-1556.3060, 22.5970, 57.6130>>
						CASE 14			RETURN <<-1553.2421, 35.2926, 57.1187>>
						CASE 15			RETURN <<-1546.7103, 34.1793, 57.0452>>
						CASE 16			RETURN <<-1589.1387, 42.7033, 59.1114>>
						CASE 17			RETURN <<-1582.9312, 41.4277, 59.0317>>
						CASE 18			RETURN <<-1573.5603, 3.1069, 59.6840>>
						CASE 19			RETURN <<-1604.6930, 0.4130, 60.0520>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_CarCollector_AceJonesDrive
					SWITCH iPed
						CASE 0			RETURN <<-1539.9630, 421.3320, 109.0190>>
						CASE 1			RETURN <<-1543.2067, 409.2770, 108.6832>>
						CASE 2			RETURN <<-1527.7344, 436.7926, 107.8508>>
						CASE 3			RETURN <<-1534.6610, 435.9252, 107.3511>>
						CASE 4			RETURN <<-1555.9337, 431.3166, 108.4466>>
						CASE 5			RETURN <<-1548.8864, 428.4229, 108.3338>>
						CASE 6			RETURN <<-1548.0980, 429.6672, 108.2545>>
						CASE 7			RETURN <<-1528.0297, 429.6363, 108.3366>>
						CASE 8			RETURN <<-1533.1067, 397.9857, 106.8830>>
						CASE 9			RETURN <<-1527.6052, 400.1403, 106.8830>>
						CASE 10			RETURN <<-1528.6958, 400.2828, 106.8829>>
						CASE 11			RETURN <<-1509.6084, 404.7508, 106.8829>>
						CASE 12			RETURN <<-1517.2726, 397.7086, 106.4831>>
						CASE 13			RETURN <<-1516.1790, 398.5998, 106.4831>>
						CASE 14			RETURN <<-1508.9714, 394.1364, 106.4831>>
						CASE 15			RETURN <<-1518.7400, 415.0620, 108.6820>>
						CASE 16			RETURN <<-1535.1799, 390.9119, 106.8784>>
						CASE 17			RETURN <<-1534.6458, 391.9051, 106.8827>>
						CASE 18			RETURN <<-1541.0188, 409.3816, 108.6832>>
						CASE 19			RETURN <<-1535.7981, 391.9260, 106.8812>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_CarCollector_Richman
					SWITCH iPed
						CASE 0			RETURN <<-1733.2640, 379.5890, 88.7250>>
						CASE 1			RETURN <<-1742.7250, 364.5430, 87.7280>>
						CASE 2			RETURN <<-1760.1219, 370.1037, 87.8853>>
						CASE 3			RETURN <<-1767.1466, 366.6364, 87.7777>>
						CASE 4			RETURN <<-1753.8801, 373.2370, 88.6276>>
						CASE 5			RETURN <<-1712.5323, 387.2217, 88.7251>>
						CASE 6			RETURN <<-1712.9136, 388.5241, 88.7251>>
						CASE 7			RETURN <<-1748.7277, 370.8982, 88.7252>>
						CASE 8			RETURN <<-1748.1799, 369.6838, 88.7252>>
						CASE 9			RETURN <<-1744.6165, 335.1134, 86.4116>>
						CASE 10			RETURN <<-1717.8352, 369.9077, 88.7773>>
						CASE 11			RETURN <<-1716.8302, 370.4547, 88.7773>>
						CASE 12			RETURN <<-1718.5651, 358.8365, 88.3252>>
						CASE 13			RETURN <<-1704.1052, 363.6238, 86.1311>>
						CASE 14			RETURN <<-1743.6978, 348.0800, 87.7282>>
						CASE 15			RETURN <<-1706.1622, 373.2253, 89.2253>>
						CASE 16			RETURN <<-1707.1348, 373.9999, 89.2253>>
						CASE 17			RETURN <<-1701.0513, 373.5834, 86.4545>>
						CASE 18			RETURN <<-1746.1819, 335.7900, 86.4564>>
						CASE 19			RETURN <<-1717.9154, 357.9315, 88.3252>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_CarCollector_NorthRockford_1
					SWITCH iPed
						CASE 0			RETURN <<-1873.5314, 201.7861, 83.2945>>
						CASE 1			RETURN <<-1847.1710, 212.9280, 87.3310>>
						CASE 2			RETURN <<-1877.0580, 194.6680, 83.2650>>
						CASE 3			RETURN <<-1871.7280, 186.9870, 83.2540>>
						CASE 4			RETURN <<-1864.0800, 209.2510, 87.3290>>
						CASE 5			RETURN <<-1858.6902, 207.4592, 83.2940>>
						CASE 6			RETURN <<-1858.2876, 221.7814, 83.2930>>
						CASE 7			RETURN <<-1856.9078, 221.5087, 83.2930>>
						CASE 8			RETURN <<-1873.4728, 223.7346, 83.2928>>
						CASE 9			RETURN <<-1872.5100, 224.5746, 83.2929>>
						CASE 10			RETURN <<-1873.1830, 209.3120, 83.4393>>
						CASE 11			RETURN <<-1877.6221, 205.9321, 83.2945>>
						CASE 12			RETURN <<-1877.6538, 215.5201, 83.4393>>
						CASE 13			RETURN <<-1861.0383, 244.2713, 83.4393>>
						CASE 14			RETURN <<-1854.7220, 232.5950, 83.4390>>
						CASE 15			RETURN <<-1863.9070, 234.3530, 83.2920>>
						CASE 16			RETURN <<-1853.0017, 216.9588, 83.2930>>
						CASE 17			RETURN <<-1839.2124, 204.3736, 83.4394>>
						CASE 18			RETURN <<-1850.9751, 199.1152, 83.4394>>
						CASE 19			RETURN <<-1849.8350, 199.4715, 83.4394>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_CarCollector_DunstableLane_1
					SWITCH iPed
						CASE 0			RETURN <<-1930.9150, 362.6136, 93.0931>>
						CASE 1			RETURN <<-1954.1406, 350.6864, 89.9038>>
						CASE 2			RETURN <<-1953.3740, 344.8604, 89.6495>>
						CASE 3			RETURN <<-1939.8922, 364.4707, 92.7200>>
						CASE 4			RETURN <<-1938.8026, 363.5762, 92.7059>>
						CASE 5			RETURN <<-1931.9283, 360.0111, 92.7895>>
						CASE 6			RETURN <<-1934.3981, 372.0598, 92.9388>>
						CASE 7			RETURN <<-1902.2130, 378.1749, 93.2204>>
						CASE 8			RETURN <<-1905.5189, 355.2595, 92.5831>>
						CASE 9			RETURN <<-1906.1324, 356.5919, 92.5836>>
						CASE 10			RETURN <<-1923.1478, 375.4446, 93.1988>>
						CASE 11			RETURN <<-1921.9679, 375.1646, 93.1988>>
						CASE 12			RETURN <<-1931.7557, 369.3018, 92.7853>>
						CASE 13			RETURN <<-1913.1989, 367.3925, 92.7814>>
						CASE 14			RETURN <<-1912.4407, 368.1230, 92.7814>>
						CASE 15			RETURN <<-1918.7303, 355.5656, 92.7814>>
						CASE 16			RETURN <<-1919.9321, 363.2147, 92.7814>>
						CASE 17			RETURN <<-1890.4374, 351.6378, 92.4034>>
						CASE 18			RETURN <<-1916.5486, 362.8606, 92.5812>>
						CASE 19			RETURN <<-1950.3525, 348.2962, 89.9856>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_CarCollector_DunstableLane_2
					SWITCH iPed
						CASE 0			RETURN <<-881.5604, 363.9382, 84.3677>>
						CASE 1			RETURN <<-868.7438, 384.0313, 86.5453>>
						CASE 2			RETURN <<-868.6186, 388.7484, 86.5500>>
						CASE 3			RETURN <<-886.1880, 367.7850, 84.0323>>
						CASE 4			RETURN <<-886.4376, 366.7598, 84.0314>>
						CASE 5			RETURN <<-892.8793, 359.0860, 84.2611>>
						CASE 6			RETURN <<-877.5408, 351.6896, 87.9259>>
						CASE 7			RETURN <<-871.4297, 382.9583, 86.4214>>
						CASE 8			RETURN <<-873.9764, 353.5718, 84.2853>>
						CASE 9			RETURN <<-886.7542, 348.2119, 84.2853>>
						CASE 10			RETURN <<-885.8595, 347.0579, 84.2853>>
						CASE 11			RETURN <<-870.6737, 338.6645, 84.2900>>
						CASE 12			RETURN <<-874.0017, 347.6775, 84.1060>>
						CASE 13			RETURN <<-873.3083, 346.8300, 84.1062>>
						CASE 14			RETURN <<-883.1603, 340.7129, 84.1061>>
						CASE 15			RETURN <<-882.8375, 341.7484, 84.1062>>
						CASE 16			RETURN <<-889.9663, 351.1320, 84.2846>>
						CASE 17			RETURN <<-871.8013, 386.3335, 86.4752>>
						CASE 18			RETURN <<-870.4891, 356.8366, 84.1025>>
						CASE 19			RETURN <<-887.6594, 383.1720, 84.9606>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_CarCollector_NorthRockford_2
					SWITCH iPed
						CASE 0			RETURN <<-1964.3490, 270.1420, 85.3730>>
						CASE 1			RETURN <<-1965.9790, 275.9880, 85.7840>>
						CASE 2			RETURN <<-1995.3380, 300.5160, 90.9650>>
						CASE 3			RETURN <<-1997.0150, 292.5730, 90.7650>>
						CASE 4			RETURN <<-1997.6600, 291.6330, 90.7650>>
						CASE 5			RETURN <<-2012.9611, 299.5340, 90.7650>>
						CASE 6			RETURN <<-1999.1040, 314.1670, 90.5660>>
						CASE 7			RETURN <<-2009.5880, 316.7630, 90.5660>>
						CASE 8			RETURN <<-2002.5649, 313.8300, 94.7210>>
						CASE 9			RETURN <<-2010.8170, 292.5830, 90.9830>>
						CASE 10			RETURN <<-2024.6460, 327.0560, 90.3640>>
						CASE 11			RETURN <<-2024.8781, 325.9350, 90.3640>>
						CASE 12			RETURN <<-2007.5220, 317.4560, 90.5660>>
						CASE 13			RETURN <<-2004.8938, 322.2877, 90.3656>>
						CASE 14			RETURN <<-2005.1398, 323.4706, 90.3656>>
						CASE 15			RETURN <<-1970.8380, 272.6560, 86.2160>>
						CASE 16			RETURN <<-1985.8170, 304.2820, 90.7650>>
						CASE 17			RETURN <<-2021.2710, 323.9690, 90.3650>>
						CASE 18			RETURN <<-1998.0006, 330.7766, 90.1655>>
						CASE 19			RETURN <<-1996.9238, 330.6139, 90.1666>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_CarCollector_CaesarsPlace
					SWITCH iPed
						CASE 0			RETURN <<-880.6300, 20.6030, 44.2260>>
						CASE 1			RETURN <<-876.4180, 18.2770, 44.2310>>
						CASE 2			RETURN <<-888.0120, 42.5820, 48.1520>>
						CASE 3			RETURN <<-888.2080, 58.7210, 49.6660>>
						CASE 4			RETURN <<-874.1670, 50.6310, 47.7690>>
						CASE 5			RETURN <<-896.2690, 50.5000, 49.0430>>
						CASE 6			RETURN <<-867.8310, 47.7920, 47.7810>>
						CASE 7			RETURN <<-896.2480, 32.9710, 47.8660>>
						CASE 8			RETURN <<-920.7590, 65.3290, 50.2410>>
						CASE 9			RETURN <<-906.6240, 46.1670, 48.6520>>
						CASE 10			RETURN <<-907.6870, 46.3620, 48.6570>>
						CASE 11			RETURN <<-901.6110, 61.4040, 48.8660>>
						CASE 12			RETURN <<-901.5430, 60.3250, 48.8660>>
						CASE 13			RETURN <<-881.9770, 22.7610, 44.4290>>
						CASE 14			RETURN <<-870.8170, 35.8180, 47.7590>>
						CASE 15			RETURN <<-919.8110, 56.2920, 48.6080>>
						CASE 16			RETURN <<-919.8870, 55.2320, 48.6080>>
						CASE 17			RETURN <<-931.7950, 67.5330, 49.9000>>
						CASE 18			RETURN <<-877.3940, 34.4500, 47.7590>>
						CASE 19			RETURN <<-878.6060, 33.8160, 47.7590>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_CarCollector_HillcrestAvenue
					SWITCH iPed
						CASE 0			RETURN <<-689.8950, 664.2470, 151.3430>>
						CASE 1			RETURN <<-690.3350, 671.9430, 152.1120>>
						CASE 2			RETURN <<-694.5070, 649.2380, 154.1750>>
						CASE 3			RETURN <<-723.3170, 659.8990, 154.5540>>
						CASE 4			RETURN <<-722.4480, 661.0670, 154.5540>>
						CASE 5			RETURN <<-700.7160, 647.5760, 154.1750>>
						CASE 6			RETURN <<-710.0780, 658.8610, 154.1800>>
						CASE 7			RETURN <<-710.5000, 659.8860, 154.1790>>
						CASE 8			RETURN <<-710.7320, 628.8170, 154.1650>>
						CASE 9			RETURN <<-706.3690, 627.8670, 154.1650>>
						CASE 10			RETURN <<-711.3690, 619.2920, 154.2210>>
						CASE 11			RETURN <<-710.2980, 620.0640, 154.2090>>
						CASE 12			RETURN <<-722.5950, 619.2820, 154.5180>>
						CASE 13			RETURN <<-721.7070, 629.1780, 154.1650>>
						CASE 14			RETURN <<-729.8370, 636.7300, 154.5180>>
						CASE 15			RETURN <<-729.0140, 636.0670, 154.5180>>
						CASE 16			RETURN <<-724.9130, 632.1230, 154.1650>>
						CASE 17			RETURN <<-709.0120, 659.8740, 154.1790>>
						CASE 18			RETURN <<-702.1220, 615.6890, 154.1650>>
						CASE 19			RETURN <<-722.2330, 620.8130, 154.5180>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE GOV_MOB_MENTALITY			
			SWITCH eLocation
				CASE GangOpsLocation_MobMentality_Morningwood
					SWITCH iPed
						CASE 0			RETURN <<-1383.1615, -156.1583, 46.5773>>
						CASE 1			RETURN <<-1381.0262, -186.8536, 45.9305>>
						CASE 2			RETURN <<-1371.9231, -166.5285, 46.4837>>
						CASE 3			RETURN <<-1373.4802, -167.3593, 46.4865>>
						CASE 4			RETURN <<-1373.3076, -165.3836, 46.4954>>
						CASE 5			RETURN <<-1374.0558, -164.3400, 46.5197>>
						CASE 6			RETURN <<-1374.2120, -162.5638, 46.5482>>
						CASE 7			RETURN <<-1386.7557, -170.5536, 46.4532>>
						CASE 8			RETURN <<-1385.6740, -170.9857, 46.4580>>
						CASE 9			RETURN <<-1383.2518, -177.1957, 46.3918>>
						CASE 10			RETURN <<-1386.4399, -171.4624, 46.4450>>
						CASE 11			RETURN <<-1389.1359, -175.1289, 46.3736>>
						CASE 12			RETURN <<-1388.1007, -175.9378, 46.3738>>
						CASE 13			RETURN <<-1370.9424, -175.9967, 46.4646>>
						CASE 14			RETURN <<-1374.1604, -175.8383, 46.4586>>
						CASE 15			RETURN <<-1383.2137, -172.8942, 46.4447>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_MobMentality_Grapeseed
					SWITCH iPed
						CASE 0			RETURN <<1787.3091, 4582.7100, 36.3061>>
						CASE 1			RETURN <<1787.9221, 4581.5513, 36.1633>>
						CASE 2			RETURN <<1792.7379, 4593.2505, 36.6829>>
						CASE 3			RETURN <<1792.6755, 4591.9907, 36.6829>>
						CASE 4			RETURN <<1796.4114, 4587.9424, 36.4186>>
						CASE 5			RETURN <<1784.0436, 4591.4126, 36.6830>>
						CASE 6			RETURN <<1801.2036, 4582.8623, 35.4984>>
						CASE 7			RETURN <<1797.8442, 4593.4487, 36.6829>>
						CASE 8			RETURN <<1795.0848, 4587.8594, 36.4230>>
						CASE 9			RETURN <<1795.9072, 4586.7007, 36.2652>>
						CASE 10			RETURN <<1799.3719, 4593.2261, 36.6828>>
						CASE 11			RETURN <<1785.3059, 4590.4600, 36.6831>>
						CASE 12			RETURN <<1802.1667, 4583.5195, 35.5247>>
						CASE 13			RETURN <<1788.2181, 4593.7456, 36.6830>>
						CASE 14			RETURN <<1802.7688, 4590.9307, 36.6828>>
						CASE 15			RETURN <<1785.1869, 4591.6724, 36.6830>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_MobMentality_Alta
					SWITCH iPed
						CASE 0			RETURN <<402.1553, -338.9266, 45.9728>>
						CASE 1			RETURN <<415.9414, -358.2336, 46.1467>>
						CASE 2			RETURN <<396.1925, -357.8391, 45.8152>>
						CASE 3			RETURN <<388.3925, -342.1580, 45.8111>>
						CASE 4			RETURN <<412.5110, -352.4416, 46.1177>>
						CASE 5			RETURN <<404.8435, -348.8843, 45.8302>>
						CASE 6			RETURN <<405.9246, -349.4503, 45.8239>>
						CASE 7			RETURN <<403.1505, -357.6790, 45.8306>>
						CASE 8			RETURN <<403.6065, -356.6118, 45.8215>>
						CASE 9			RETURN <<390.0892, -355.5867, 47.0249>>
						CASE 10			RETURN <<391.0550, -356.8315, 47.0260>>
						CASE 11			RETURN <<412.7168, -349.4939, 46.2033>>
						CASE 12			RETURN <<413.8307, -350.0274, 46.2621>>
						CASE 13			RETURN <<397.6365, -358.0837, 45.8152>>
						CASE 14			RETURN <<389.5204, -356.8549, 47.0240>>
						CASE 15			RETURN <<396.4565, -352.8649, 45.8152>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_MobMentality_MissionRow
					SWITCH iPed
						CASE 0			RETURN <<268.7935, -1153.9486, 28.2917>>
						CASE 1			RETURN <<268.7864, -1156.7810, 28.2872>>
						CASE 2			RETURN <<252.9365, -1161.5947, 28.1449>>
						CASE 3			RETURN <<252.4952, -1160.6238, 28.1464>>
						CASE 4			RETURN <<258.3830, -1150.2787, 28.2917>>
						CASE 5			RETURN <<260.0046, -1148.4182, 28.2917>>
						CASE 6			RETURN <<262.1448, -1152.1581, 28.2917>>
						CASE 7			RETURN <<262.9810, -1148.4821, 28.2917>>
						CASE 8			RETURN <<267.6763, -1153.9910, 28.2917>>
						CASE 9			RETURN <<264.4962, -1157.6439, 28.2508>>
						CASE 10			RETURN <<264.4373, -1156.5258, 28.2683>>
						CASE 11			RETURN <<266.6448, -1154.5807, 28.2917>>
						CASE 12			RETURN <<267.4411, -1156.5183, 28.2828>>
						CASE 13			RETURN <<259.6978, -1164.9962, 28.1572>>
						CASE 14			RETURN <<259.8337, -1163.2787, 28.1748>>
						CASE 15			RETURN <<258.7189, -1163.5212, 28.1723>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_MobMentality_HippyCamp
					SWITCH iPed
						CASE 0			RETURN <<2325.5850, 2507.7800, 45.5575>>
						CASE 1			RETURN <<2315.2356, 2513.7217, 45.6677>>
						CASE 2			RETURN <<2334.0173, 2523.3384, 45.5177>>
						CASE 3			RETURN <<2334.4387, 2521.8704, 45.6678>>
						CASE 4			RETURN <<2333.0876, 2524.1250, 45.5693>>
						CASE 5			RETURN <<2316.5288, 2507.6887, 45.3997>>
						CASE 6			RETURN <<2318.2461, 2507.6392, 45.3446>>
						CASE 7			RETURN <<2317.1887, 2506.0081, 45.2959>>
						CASE 8			RETURN <<2316.0154, 2514.7910, 45.6677>>
						CASE 9			RETURN <<2317.1499, 2525.5562, 45.6677>>
						CASE 10			RETURN <<2322.9766, 2521.3201, 45.6677>>
						CASE 11			RETURN <<2323.2271, 2520.2161, 45.6677>>
						CASE 12			RETURN <<2328.2439, 2514.3469, 45.6677>>
						CASE 13			RETURN <<2313.5122, 2522.1086, 45.6677>>
						CASE 14			RETURN <<2314.5244, 2521.1960, 45.6677>>
						CASE 15			RETURN <<2324.4233, 2508.0464, 45.5800>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_MobMentality_SanAndreas
					SWITCH iPed
						CASE 0			RETURN <<-1199.8569, -1452.7870, 3.3740>>
						CASE 1			RETURN <<-1201.1556, -1452.7416, 3.3743>>
						CASE 2			RETURN <<-1225.8661, -1444.7397, 3.2729>>
						CASE 3			RETURN <<-1225.3086, -1469.1721, 3.3219>>
						CASE 4			RETURN <<-1219.5975, -1449.6871, 3.3473>>
						CASE 5			RETURN <<-1231.1171, -1448.6233, 3.2529>>
						CASE 6			RETURN <<-1213.5006, -1452.6545, 3.3765>>
						CASE 7			RETURN <<-1221.8115, -1458.6700, 3.3330>>
						CASE 8			RETURN <<-1222.3013, -1461.3086, 3.3321>>
						CASE 9			RETURN <<-1235.5181, -1456.1737, 3.2439>>
						CASE 10			RETURN <<-1220.7451, -1449.6969, 3.3400>>
						CASE 11			RETURN <<-1219.1327, -1459.0269, 3.6087>>
						CASE 12			RETURN <<-1212.9321, -1451.6296, 3.3803>>
						CASE 13			RETURN <<-1228.7889, -1459.4882, 3.3180>>
						CASE 14			RETURN <<-1227.7090, -1458.7644, 3.3227>>
						CASE 15			RETURN <<-1228.8687, -1458.3154, 3.3153>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_MobMentality_MirrorPark
					SWITCH iPed
						CASE 0			RETURN <<1337.5630, -721.2020, 65.4940>>
						CASE 1			RETURN <<1349.3622, -717.2839, 65.6544>>
						CASE 2			RETURN <<1326.2371, -701.4190, 65.8770>>
						CASE 3			RETURN <<1349.2163, -692.8033, 67.4208>>
						CASE 4			RETURN <<1350.0662, -718.7891, 65.8892>>
						CASE 5			RETURN <<1326.5380, -708.7420, 64.9720>>
						CASE 6			RETURN <<1345.3450, -709.2020, 66.6930>>
						CASE 7			RETURN <<1333.5530, -696.7340, 65.8770>>
						CASE 8			RETURN <<1335.8450, -709.3430, 65.0310>>
						CASE 9			RETURN <<1353.4871, -702.8340, 66.1860>>
						CASE 10			RETURN <<1354.5010, -701.7190, 66.1860>>
						CASE 11			RETURN <<1343.4150, -702.0570, 66.0110>>
						CASE 12			RETURN <<1355.8459, -713.1990, 66.2360>>
						CASE 13			RETURN <<1333.7190, -703.3410, 65.8770>>
						CASE 14			RETURN <<1334.7360, -704.2550, 65.8770>>
						CASE 15			RETURN <<1354.0089, -708.0627, 66.1862>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_MobMentality_Strawberry
					SWITCH iPed
						CASE 0			RETURN <<69.7690, -1568.8800, 28.6030>>
						CASE 1			RETURN <<55.5610, -1586.0470, 28.6030>>
						CASE 2			RETURN <<56.2230, -1556.6331, 28.4600>>
						CASE 3			RETURN <<55.0410, -1556.9950, 28.4600>>
						CASE 4			RETURN <<53.6790, -1570.4980, 28.4550>>
						CASE 5			RETURN <<62.0630, -1576.1639, 28.6030>>
						CASE 6			RETURN <<59.9520, -1563.7030, 28.4600>>
						CASE 7			RETURN <<60.8050, -1569.8831, 28.4600>>
						CASE 8			RETURN <<59.8370, -1569.3800, 28.4600>>
						CASE 9			RETURN <<65.1480, -1573.8990, 29.0710>>
						CASE 10			RETURN <<65.0870, -1572.7550, 28.6030>>
						CASE 11			RETURN <<70.7710, -1564.1520, 28.6030>>
						CASE 12			RETURN <<61.7480, -1563.8580, 28.4600>>
						CASE 13			RETURN <<60.7807, -1568.9235, 28.4601>>
						CASE 14			RETURN <<69.6776, -1564.2632, 28.6028>>
						CASE 15			RETURN <<53.3507, -1571.4104, 28.4611>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_MobMentality_MirrorParkTavern
					SWITCH iPed
						CASE 0			RETURN <<1217.7280, -417.1720, 66.7390>>
						CASE 1			RETURN <<1217.1740, -415.6820, 66.7750>>
						CASE 2			RETURN <<1229.6080, -439.6889, 66.6537>>
						CASE 3			RETURN <<1229.2399, -440.7524, 66.5868>>
						CASE 4			RETURN <<1216.4821, -416.7760, 66.7410>>
						CASE 5			RETURN <<1216.5540, -417.8870, 66.7130>>
						CASE 6			RETURN <<1222.4572, -428.1315, 66.5171>>
						CASE 7			RETURN <<1222.1239, -429.1175, 66.4442>>
						CASE 8			RETURN <<1213.5500, -433.4450, 66.2820>>
						CASE 9			RETURN <<1212.4670, -432.9710, 66.2810>>
						CASE 10			RETURN <<1231.5024, -432.8948, 66.7362>>
						CASE 11			RETURN <<1230.7452, -429.5980, 66.9592>>
						CASE 12			RETURN <<1224.3750, -434.8835, 66.5785>>
						CASE 13			RETURN <<1223.5725, -434.1374, 66.5318>>
						CASE 14			RETURN <<1223.5443, -435.3868, 66.5672>>
						CASE 15			RETURN <<1229.4557, -431.8471, 66.6393>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_MobMentality_ULSA
					SWITCH iPed
						CASE 0			RETURN <<-1602.6586, 181.5433, 58.4716>>
						CASE 1			RETURN <<-1601.7765, 180.5233, 58.4409>>
						CASE 2			RETURN <<-1625.1063, 180.1366, 59.5649>>
						CASE 3			RETURN <<-1617.8556, 195.0943, 59.3157>>
						CASE 4			RETURN <<-1615.1777, 182.0239, 58.9147>>
						CASE 5			RETURN <<-1608.9446, 196.2908, 58.8825>>
						CASE 6			RETURN <<-1617.2565, 194.1164, 59.2710>>
						CASE 7			RETURN <<-1616.7225, 195.1414, 59.2638>>
						CASE 8			RETURN <<-1623.4083, 186.1350, 59.5245>>
						CASE 9			RETURN <<-1625.0173, 185.3892, 59.8383>>
						CASE 10			RETURN <<-1615.0348, 180.9938, 58.9020>>
						CASE 11			RETURN <<-1622.8491, 187.2285, 59.4963>>
						CASE 12			RETURN <<-1609.4003, 194.8463, 58.8894>>
						CASE 13			RETURN <<-1608.1340, 194.0759, 58.8085>>
						CASE 14			RETURN <<-1616.9106, 173.3527, 59.2413>>
						CASE 15			RETURN <<-1607.3405, 195.6541, 58.7651>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_PARAMEDIC
			SWITCH eLocation
				CASE GangOpsLocation_Paramedic_Central_LS_Medical_Center
					SWITCH iPed
						CASE 0			RETURN <<392.3540, -1432.5200, 28.4400>>
						CASE 1			RETURN <<399.3390, -1436.9840, 28.4300>>
						CASE 2			RETURN <<391.3990, -1431.9890, 28.4340>>
						CASE 3			RETURN <<389.1080, -1434.8940, 28.4340>>
						CASE 4			RETURN <<403.3920, -1417.6550, 28.4330>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Pillbox_Hill_Medical_Center
					SWITCH iPed
						CASE 0			RETURN <<362.2970, -580.5490, 27.8330>>
						CASE 1			RETURN <<364.0430, -585.3180, 27.6910>>
						CASE 2			RETURN <<355.0480, -597.8670, 27.7710>>
						CASE 3			RETURN <<371.9580, -574.4740, 27.8410>>
						CASE 4			RETURN <<363.7550, -580.4670, 27.8400>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_ST_Fiacre_Hospital
					SWITCH iPed
						CASE 0			RETURN <<1161.0459, -1514.9884, 33.6926>>
						CASE 1			RETURN <<1153.8970, -1524.6820, 33.8430>>
						CASE 2			RETURN <<1153.9160, -1525.9440, 33.8430>>
						CASE 3			RETURN <<1148.2690, -1528.5060, 34.3910>>
						CASE 4			RETURN <<1172.0410, -1527.7100, 34.0510>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Sandy_Shores_Medical_Center
					SWITCH iPed
						CASE 0			RETURN <<-492.5327, -330.9824, 33.3653>>
						CASE 1			RETURN <<-497.0471, -334.9928, 33.5017>>
						CASE 2			RETURN <<-497.6642, -335.8562, 33.5017>>
						CASE 3			RETURN <<-499.2599, -342.8306, 33.5019>>
						CASE 4			RETURN <<-487.7382, -337.2915, 33.5007>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Paleto_Bay_Care_Center
					SWITCH iPed
						CASE 0			RETURN <<-246.9050, 6330.6372, 31.4260>>
						CASE 1			RETURN <<-243.5017, 6337.7471, 31.4259>>
						CASE 2			RETURN <<-248.1470, 6330.8540, 31.4260>>
						CASE 3			RETURN <<-242.9450, 6324.2109, 31.4260>>
						CASE 4			RETURN <<-251.9660, 6339.0181, 31.4890>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Del_Perro_Freeway
					SWITCH iPed
						CASE 0			RETURN <<-1486.0950, -765.8050, 10.1950>>
						CASE 1			RETURN <<-1489.6312, -764.7600, 10.1977>>
						CASE 2			RETURN <<-1484.9730, -765.2070, 10.1940>>
						CASE 3			RETURN <<-1481.0420, -767.3980, 10.1910>>
						CASE 4			RETURN <<-1477.4949, -764.2672, 10.1876>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Mount_Gordo
					SWITCH iPed
						CASE 0			RETURN <<1933.1920, 6294.3701, 40.0070>>
						CASE 1			RETURN <<1932.9730, 6295.9048, 40.0560>>
						CASE 2			RETURN <<1921.5476, 6295.5845, 41.3918>>
						CASE 3			RETURN <<1932.0692, 6287.1362, 41.3995>>
						CASE 4			RETURN <<1938.5780, 6286.8096, 40.0531>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Great_Chaparral
					SWITCH iPed
						CASE 0			RETURN <<-765.3820, 1699.5790, 199.7550>>
						CASE 1			RETURN <<-761.8220, 1708.6030, 200.0640>>
						CASE 2			RETURN <<-764.5790, 1698.8140, 199.8270>>
						CASE 3			RETURN <<-766.0200, 1694.0570, 199.7660>>
						CASE 4			RETURN <<-770.4539, 1691.8444, 199.5712>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Cassidy_Creek
					SWITCH iPed
						CASE 0			RETURN <<-890.6874, 4431.4272, 20.1269>>
						CASE 1			RETURN <<-889.7377, 4432.1001, 20.1685>>
						CASE 2			RETURN <<-894.7537, 4432.4175, 20.1025>>
						CASE 3			RETURN <<-884.7969, 4431.7339, 19.9135>>
						CASE 4			RETURN <<-883.0228, 4427.1211, 19.8374>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Power_Station
					SWITCH iPed
						CASE 0			RETURN <<-2426.4775, 1028.6262, 194.0403>>
						CASE 1			RETURN <<-2423.3887, 1020.7234, 190.6064>>
						CASE 2			RETURN <<-2424.6826, 1020.6835, 190.6339>>
						CASE 3			RETURN <<-2415.3730, 1025.4438, 194.2555>>
						CASE 4			RETURN <<-2418.0662, 1020.5215, 190.0408>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_AQUALUNGS
			SWITCH eLocation
				CASE GangOpsLocation_Aqualungs_Elysian_Island_1
					SWITCH iPed
						CASE 0			RETURN <<-500.7897, -3221.9587, 1.8155>>
						CASE 1			RETURN <<-499.7346, -3222.4768, 1.8156>>
						CASE 2			RETURN <<-509.4364, -3238.1323, 4.6364>>
						CASE 3			RETURN <<-510.2546, -3244.9985, 4.6250>>
						CASE 4			RETURN <<-519.7932, -3243.8059, 1.8155>>
						CASE 5			RETURN <<-518.6583, -3244.4858, 1.8156>>
						CASE 6			RETURN <<-520.2446, -3247.5781, 4.6230>>
						CASE 7			RETURN <<-491.0046, -3207.8062, 0.3468>>
						CASE 8			RETURN <<-481.0725, -3211.8440, 0.3559>>
						CASE 9			RETURN <<-512.7815, -3231.6692, 1.8043>>
						CASE 10			RETURN <<-503.7830, -3230.3506, 1.8277>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Paleto_Bay
					SWITCH iPed
						CASE 0			RETURN <<724.1314, 6985.3022, 4.2812>>
						CASE 1			RETURN <<717.9803, 6986.8638, 4.2749>>
						CASE 2			RETURN <<710.9453, 6989.1260, 1.4541>>
						CASE 3			RETURN <<710.3897, 6981.3853, 1.4774>>
						CASE 4			RETURN <<721.8221, 6991.7427, 1.4542>>
						CASE 5			RETURN <<721.7635, 6993.2334, 1.6874>>
						CASE 6			RETURN <<703.1763, 6980.6738, 1.4776>>
						CASE 7			RETURN <<704.9584, 6979.9780, 1.4777>>
						CASE 8			RETURN <<696.4058, 6965.1646, 1.5889>>
						CASE 9			RETURN <<687.0583, 6971.2031, 2.1223>>
						CASE 10			RETURN <<727.1300, 6995.0796, 4.2730>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Elysian_Island_2
					SWITCH iPed
						CASE 0			RETURN <<784.8557, -3598.0972, 5.3730>>
						CASE 1			RETURN <<774.3211, -3595.3921, 5.3811>>
						CASE 2			RETURN <<771.8720, -3587.8479, 5.3710>>
						CASE 3			RETURN <<776.5485, -3581.9084, 2.5543>>
						CASE 4			RETURN <<768.2509, -3582.0442, 2.5774>>
						CASE 5			RETURN <<779.6629, -3591.3494, 2.5543>>
						CASE 6			RETURN <<779.4949, -3593.0251, 2.5543>>
						CASE 7			RETURN <<767.3260, -3577.2000, 2.5780>>
						CASE 8			RETURN <<766.0240, -3576.6650, 2.5780>>
						CASE 9			RETURN <<757.8132, -3562.2158, -0.4044>>
						CASE 10			RETURN <<750.6843, -3568.7576, 0.5474>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Palomino_Highlands
					SWITCH iPed
						CASE 0			RETURN <<3059.5901, -1108.9600, 4.7910>>
						CASE 1			RETURN <<3058.3464, -1118.8347, 4.7729>>
						CASE 2			RETURN <<3054.0640, -1112.7500, 1.9780>>
						CASE 3			RETURN <<3053.5811, -1114.4410, 1.9780>>
						CASE 4			RETURN <<3053.9863, -1104.5668, 4.7911>>
						CASE 5			RETURN <<3046.9067, -1104.9111, 1.9543>>
						CASE 6			RETURN <<3048.9546, -1092.3103, 1.9543>>
						CASE 7			RETURN <<3042.9480, -1090.9290, 1.9780>>
						CASE 8			RETURN <<3044.3730, -1091.1620, 1.9780>>
						CASE 9			RETURN <<3044.9480, -1072.1750, 0.2250>>
						CASE 10			RETURN <<3034.9771, -1075.6400, 0.6480>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Chumash
					SWITCH iPed
						CASE 0			RETURN <<-3747.3562, 1316.8762, 4.5730>>
						CASE 1			RETURN <<-3741.6421, 1311.7321, 1.7780>>
						CASE 2			RETURN <<-3741.1621, 1312.8459, 1.7780>>
						CASE 3			RETURN <<-3737.8740, 1319.0260, 4.5910>>
						CASE 4			RETURN <<-3735.1033, 1314.0256, 4.5911>>
						CASE 5			RETURN <<-3733.3003, 1306.6671, 2.2501>>
						CASE 6			RETURN <<-3723.9932, 1309.1332, 1.7775>>
						CASE 7			RETURN <<-3717.9050, 1303.9620, 1.7780>>
						CASE 8			RETURN <<-3716.4839, 1304.7410, 1.7780>>
						CASE 9			RETURN <<-3699.4231, 1305.4550, -0.1000>>
						CASE 10			RETURN <<-3703.5750, 1296.5060, -1.2150>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_San_Chianksi_Mountain_Range
					SWITCH iPed
						CASE 0			RETURN <<3989.9192, 5110.0215, 4.2731>>
						CASE 1			RETURN <<3982.7161, 5103.4980, 4.2910>>
						CASE 2			RETURN <<3985.2661, 5097.1333, 4.3010>>
						CASE 3			RETURN <<3981.8291, 5085.1357, 1.4774>>
						CASE 4			RETURN <<3989.7830, 5103.4922, 1.4780>>
						CASE 5			RETURN <<3991.1531, 5103.1548, 1.4780>>
						CASE 6			RETURN <<3990.4949, 5092.5669, 1.4541>>
						CASE 7			RETURN <<3982.1709, 5078.9258, 1.4780>>
						CASE 8			RETURN <<3983.3950, 5079.6021, 1.4780>>
						CASE 9			RETURN <<3981.7891, 5061.8950, 0.9480>>
						CASE 10			RETURN <<3972.4680, 5066.0889, 0.6040>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_PaletoForest
					SWITCH iPed
						CASE 0			RETURN <<-1518.4634, 5769.5659, 4.2454>>
						CASE 1			RETURN <<-1514.5582, 5776.1152, 4.2454>>
						CASE 2			RETURN <<-1529.0754, 5774.1333, 4.1439>>
						CASE 3			RETURN <<-1513.7258, 5782.1831, 1.4246>>
						CASE 4			RETURN <<-1524.2925, 5775.3696, 1.4247>>
						CASE 5			RETURN <<-1525.5084, 5775.8384, 1.4245>>
						CASE 6			RETURN <<-1505.1476, 5779.9629, 1.4246>>
						CASE 7			RETURN <<-1504.0072, 5780.9272, 1.4248>>
						CASE 8			RETURN <<-1501.5690, 5777.3896, 1.4245>>
						CASE 9			RETURN <<-1480.1210, 5777.9868, 1.7395>>
						CASE 10			RETURN <<-1482.2504, 5788.6377, 0.8847>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_North_Chumash
					SWITCH iPed
						CASE 0			RETURN <<-2965.7925, 4415.5840, 4.2629>>
						CASE 1			RETURN <<-2959.7209, 4412.8481, 1.4680>>
						CASE 2			RETURN <<-2958.3530, 4413.7539, 1.4680>>
						CASE 3			RETURN <<-2956.6079, 4420.0708, 4.2910>>
						CASE 4			RETURN <<-2951.7920, 4415.5278, 4.2711>>
						CASE 5			RETURN <<-2949.4124, 4408.5093, 1.4444>>
						CASE 6			RETURN <<-2938.6553, 4413.4292, 1.4674>>
						CASE 7			RETURN <<-2932.7759, 4410.4761, 1.4680>>
						CASE 8			RETURN <<-2933.6230, 4411.4360, 1.4680>>
						CASE 9			RETURN <<-2915.6111, 4415.4980, 1.6290>>
						CASE 10			RETURN <<-2916.3330, 4406.0469, 2.0960>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Pacific_Bluffs
					SWITCH iPed
						CASE 0			RETURN <<-2234.7292, -938.9608, 4.2730>>
						CASE 1			RETURN <<-2234.2068, -931.2715, 1.4777>>
						CASE 2			RETURN <<-2235.3066, -932.1537, 1.4777>>
						CASE 3			RETURN <<-2227.3274, -932.0264, 4.2807>>
						CASE 4			RETURN <<-2231.5215, -925.6750, 4.2811>>
						CASE 5			RETURN <<-2237.3125, -922.2652, 1.4543>>
						CASE 6			RETURN <<-2229.2625, -911.9962, 1.4774>>
						CASE 7			RETURN <<-2230.1980, -906.8578, 1.4777>>
						CASE 8			RETURN <<-2231.5886, -906.4961, 1.4777>>
						CASE 9			RETURN <<-2232.7163, -891.0059, 0.6758>>
						CASE 10			RETURN <<-2221.7239, -889.1395, 0.7971>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Vespucci_Beach
					SWITCH iPed
						CASE 0			RETURN <<-1790.6869, -1757.3029, 4.3430>>
						CASE 1			RETURN <<-1782.8890, -1757.7933, 1.5478>>
						CASE 2			RETURN <<-1782.6255, -1756.4471, 1.5477>>
						CASE 3			RETURN <<-1783.3320, -1748.9799, 4.3507>>
						CASE 4			RETURN <<-1777.9363, -1752.6101, 4.3511>>
						CASE 5			RETURN <<-1773.1794, -1758.0767, 1.5243>>
						CASE 6			RETURN <<-1764.6722, -1748.2860, 1.5474>>
						CASE 7			RETURN <<-1759.2628, -1751.0751, 1.5478>>
						CASE 8			RETURN <<-1759.9746, -1749.7955, 1.5478>>
						CASE 9			RETURN <<-1738.0330, -1746.8995, 1.3168>>
						CASE 10			RETURN <<-1743.4049, -1739.1177, 1.8113>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_UNDER_CONTROL
			SWITCH eLocation
				CASE GangOpsLocation_UnderControl_ServerRoom1
					SWITCH iPed
						CASE 0			RETURN <<-417.0477, 1153.1833, 511.7188>>
						CASE 1			RETURN <<-417.0477, 1153.1833, 511.7188>>
						CASE 2			RETURN <<-417.0477, 1153.1833, 511.7188>>
						CASE 3			RETURN <<-417.0477, 1153.1833, 511.7188>>
						CASE 4			RETURN <<-1190.6833, -2529.2786, 12.9452>>
						CASE 5			RETURN <<-2350.1458, 3266.7466, 31.8107>>
						CASE 6			RETURN <<-2359.7446, 3254.6287, 91.9037>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ServerRoom2
					SWITCH iPed
						CASE 0			RETURN <<-1533.8590, 855.7600, 491.3570>>
						CASE 1			RETURN <<-1533.8590, 855.7600, 491.3570>>
						CASE 2			RETURN <<-1533.8590, 855.7600, 491.3570>>
						CASE 3			RETURN <<-1533.8590, 855.7600, 491.3570>>
						CASE 4			RETURN <<-1923.6000, -3018.2290, 20.4380>>
						CASE 5			RETURN <<-2348.7549, 3266.7202, 31.8107>>
						CASE 6			RETURN <<-2357.8384, 3253.8352, 91.9037>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ServerRoom3
					SWITCH iPed
						CASE 0			RETURN <<-95.3700, 880.3560, 504.0490>>
						CASE 1			RETURN <<-95.3700, 880.3560, 504.0490>>
						CASE 2			RETURN <<-95.3700, 880.3560, 504.0490>>
						CASE 3			RETURN <<-95.3700, 880.3560, 504.0490>>
						CASE 4			RETURN <<-843.7370, -2940.0061, 12.9710>>
						CASE 5			RETURN <<-2348.7827, 3268.6201, 31.8107>>
						CASE 6			RETURN <<-2360.4216, 3252.0486, 91.9037>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ServerRoom4
					SWITCH iPed
						CASE 0			RETURN <<-1812.8719, 813.6180, 501.0170>>
						CASE 1			RETURN <<-1812.8719, 813.6180, 501.0170>>
						CASE 2			RETURN <<-1812.8719, 813.6180, 501.0170>>
						CASE 3			RETURN <<-1812.8719, 813.6180, 501.0170>>
						CASE 4			RETURN <<-1022.0860, -2371.4580, 12.9450>>
						CASE 5			RETURN <<-2346.8889, 3266.9714, 31.8107>>
						CASE 6			RETURN <<-2360.1829, 3253.4958, 91.9037>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ServerRoom5
					SWITCH iPed
						CASE 0			RETURN <<821.0580, 1270.4580, 507.3040>>
						CASE 1			RETURN <<821.0580, 1270.4580, 507.3040>>
						CASE 2			RETURN <<821.0580, 1270.4580, 507.3040>>
						CASE 3			RETURN <<821.0580, 1270.4580, 507.3040>>
						CASE 4			RETURN <<362.4916, -2541.4314, 4.7432>>
						CASE 5			RETURN <<-2345.4595, 3267.9648, 31.8107>>
						CASE 6			RETURN <<-2359.1289, 3252.4028, 91.9037>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ControlRoom1
					SWITCH iPed
						CASE 0			RETURN <<1329.2380, 1139.8571, 510.9130>>
						CASE 1			RETURN <<1329.2380, 1139.8571, 510.9130>>
						CASE 2			RETURN <<1329.2380, 1139.8571, 510.9130>>
						CASE 3			RETURN <<1329.2380, 1139.8571, 510.9130>>
						CASE 4			RETURN <<806.1410, -3184.3931, 4.9010>>
						CASE 5			RETURN <<-2345.1194, 3265.4080, 31.8107>>
						CASE 6			RETURN <<-2356.0847, 3252.7136, 93.4227>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ControlRoom2
					SWITCH iPed
						CASE 0			RETURN <<2465.0000, 1470.3420, 509.1520>>
						CASE 1			RETURN <<2465.0000, 1470.3420, 509.1520>>
						CASE 2			RETURN <<2465.0000, 1470.3420, 509.1520>>
						CASE 3			RETURN <<2465.0000, 1470.3420, 509.1520>>
						CASE 4			RETURN <<1178.7483, -2126.9548, 42.2456>>
						CASE 5			RETURN <<-2346.5588, 3263.1748, 31.8107>>
						CASE 6			RETURN <<-2358.0007, 3249.1848, 91.9037>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ControlRoom3
					SWITCH iPed
						CASE 0			RETURN <<1841.5500, 1731.5720, 513.8310>>
						CASE 1			RETURN <<1841.5500, 1731.5720, 513.8310>>
						CASE 2			RETURN <<1841.5500, 1731.5720, 513.8310>>
						CASE 3			RETURN <<1841.5500, 1731.5720, 513.8310>>
						CASE 4			RETURN <<1121.4060, -3241.1479, 4.8950>>
						CASE 5			RETURN <<-2347.6699, 3262.3118, 31.8107>>
						CASE 6			RETURN <<-2357.6147, 3250.5815, 91.9037>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ControlRoom4
					SWITCH iPed
						CASE 0			RETURN <<-2966.7629, 384.9720, 506.3940>>
						CASE 1			RETURN <<-2966.7629, 384.9720, 506.3940>>
						CASE 2			RETURN <<-2966.7629, 384.9720, 506.3940>>
						CASE 3			RETURN <<-2966.7629, 384.9720, 506.3940>>
						CASE 4			RETURN <<-37.1040, -2231.7690, 6.8120>>
						CASE 5			RETURN <<-2347.3765, 3261.2070, 31.8107>>
						CASE 6			RETURN <<-2358.7559, 3251.3215, 91.9037>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ControlRoom5
					SWITCH iPed
						CASE 0			RETURN <<2576.6750, 384.9670, 511.4830>>
						CASE 1			RETURN <<2576.6750, 384.9670, 511.4830>>
						CASE 2			RETURN <<2576.6750, 384.9670, 511.4830>>
						CASE 3			RETURN <<2576.6750, 384.9670, 511.4830>>
						CASE 4			RETURN <<272.8994, -2197.4846, 8.4920>>
						CASE 5			RETURN <<-2343.7710, 3267.8162, 31.8107>>
						CASE 6			RETURN <<-2353.8013, 3249.7668, 94.7347>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_BURGLARY_JOB
			SWITCH iPed
				CASE 0			RETURN <<578.9485, 4745.3628, -60.1962>>
				CASE 1			RETURN <<572.4837, 4751.0532, -60.1962>>
				CASE 2			RETURN <<577.2777, 4753.4976, -60.1962>>
				CASE 3			RETURN <<575.2202, 4744.4204, -60.1962>>
				CASE 4			RETURN <<579.4749, 4753.3223, -60.1962>>
			ENDSWITCH
			
			SWITCH eLocation
				CASE GangOpsLocation_BurglaryJob_ProcopioDrive
					SWITCH iPed
						CASE 5			RETURN <<-406.9331, 6313.8657, 27.9423>>
						CASE 6			RETURN <<-402.8049, 6324.3794, 27.9391>>
						CASE 7			RETURN <<-411.7240, 6307.0132, 27.7883>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_BurglaryJob_SouthMoMilton
					SWITCH iPed
						CASE 5			RETURN <<-1091.0540, 548.1360, 102.6330>>
						CASE 6			RETURN <<-1084.6638, 541.2791, 101.7258>>
						CASE 7			RETURN <<-1102.5081, 549.2690, 101.6870>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_BurglaryJob_MirrorPark
					SWITCH iPed
						CASE 5			RETURN <<1199.6399, -577.4921, 68.1391>>
						CASE 6			RETURN <<1215.3173, -569.4616, 67.8207>>
						CASE 7			RETURN <<1216.8582, -583.5948, 67.8206>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_BurglaryJob_ImaginationCourt
					SWITCH iPed
						CASE 5			RETURN <<-1124.3174, -1090.2998, 1.5502>>
						CASE 6			RETURN <<-1129.2162, -1073.3202, 1.1504>>
						CASE 7			RETURN <<-1120.9069, -1098.7544, 1.1504>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_BurglaryJob_DidionDrive
					SWITCH iPed
						CASE 5			RETURN <<-371.8799, 344.7749, 108.9476>>
						CASE 6			RETURN <<-360.0098, 322.6068, 108.3480>>
						CASE 7			RETURN <<-355.2810, 349.3585, 108.4077>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_BurglaryJob_KimbleHill
					SWITCH iPed
						CASE 5			RETURN <<-293.2824, 601.4164, 180.5807>>
						CASE 6			RETURN <<-274.7406, 574.0489, 180.6866>>
						CASE 7			RETURN <<-278.5895, 574.8804, 180.6866>>
					ENDSWITCH	
				BREAK
				CASE GangOpsLocation_BurglaryJob_BridgeStreet
					SWITCH iPed
						CASE 5			RETURN <<1046.3124, -497.6946, 63.0843>>
						CASE 6			RETURN <<1031.9846, -485.7884, 62.9245>>
						CASE 7			RETURN <<1028.8940, -499.8010, 62.9250>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_BurglaryJob_NikolaAvenue
					SWITCH iPed
						CASE 5			RETURN <<920.1403, -570.6132, 57.3714>>
						CASE 6			RETURN <<925.8187, -569.0217, 56.9671>>
						CASE 7			RETURN <<912.3590, -554.0137, 57.3714>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_BurglaryJob_NorthSheldonAvenue
					SWITCH iPed
						CASE 5			RETURN <<-972.8000, 752.9660, 175.3860>>
						CASE 6			RETURN <<-989.3513, 739.4559, 174.4701>>
						CASE 7			RETURN <<-965.2032, 770.0242, 174.5226>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_BurglaryJob_InesenoRoad
					SWITCH iPed
						CASE 5			RETURN <<-3093.5906, 349.1390, 6.5457>>
						CASE 6			RETURN <<-3087.9399, 338.6815, 6.3819>>
						CASE 7			RETURN <<-3100.3022, 360.5767, 6.5960>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_GONE_BALLISTIC		
			SWITCH eLocation
				CASE GangOpsLocation_GoneBallistic_ElysianIsland1
					SWITCH iPed
						CASE 0			RETURN <<-141.5962, -2553.7317, 5.0194>>
						CASE 1			RETURN <<-133.0097, -2560.8347, 5.0149>>
						CASE 2			RETURN <<-163.6466, -2573.2227, 5.0052>>
						CASE 3			RETURN <<-156.7782, -2545.1301, 5.0288>>
						CASE 4			RETURN <<-151.9980, -2576.5969, 5.0010>>
						CASE 5			RETURN <<-131.6390, -2572.3630, 5.0010>>
						CASE 6			RETURN <<-133.2360, -2573.1750, 5.0010>>
						CASE 7			RETURN <<-139.1450, -2560.8911, 5.0160>>
						CASE 8			RETURN <<-142.0890, -2571.5581, 5.0010>>
						CASE 9			RETURN <<-147.8500, -2553.4500, 5.0230>>
						CASE 10			RETURN <<-149.6610, -2553.2959, 5.0240>>
						CASE 11			RETURN <<-153.2960, -2573.1321, 5.0010>>
						CASE 12			RETURN <<-155.2780, -2572.5249, 5.0010>>
						CASE 13			RETURN <<-142.8848, -2557.9348, 5.0106>>
						CASE 14			RETURN <<-145.2009, -2568.1279, 5.0007>>
						CASE 15			RETURN <<-145.0731, -2563.4338, 7.7424>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_GoneBallistic_GrandSenoraDesert1
					SWITCH iPed
						CASE 0			RETURN <<1449.8929, 1516.6080, 111.2750>>
						CASE 1			RETURN <<1449.1500, 1515.2280, 111.3860>>
						CASE 2			RETURN <<1433.4640, 1539.0780, 109.6210>>
						CASE 3			RETURN <<1434.5950, 1539.9750, 109.6220>>
						CASE 4			RETURN <<1423.4941, 1540.1375, 108.9971>>
						CASE 5			RETURN <<1423.6639, 1506.9980, 112.4690>>
						CASE 6			RETURN <<1421.9640, 1506.2729, 112.4950>>
						CASE 7			RETURN <<1445.3110, 1535.2061, 110.1720>>
						CASE 8			RETURN <<1414.6116, 1531.7338, 109.2397>>
						CASE 9			RETURN <<1434.2281, 1518.1465, 111.1604>>
						CASE 10			RETURN <<1432.8140, 1528.0680, 110.1940>>
						CASE 11			RETURN <<1435.1281, 1503.5350, 112.5670>>
						CASE 12			RETURN <<1413.0662, 1530.7190, 109.4206>>
						CASE 13			RETURN <<1439.7655, 1516.7587, 111.4028>>
						CASE 14			RETURN <<1442.2244, 1525.1786, 110.7563>>
						CASE 15			RETURN <<1434.9292, 1523.0911, 114.0689>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_GoneBallistic_Grapeseed1
					SWITCH iPed
						CASE 0			RETURN <<2170.3079, 5031.4199, 41.2949>>
						CASE 1			RETURN <<2170.3054, 5029.8979, 41.2242>>
						CASE 2			RETURN <<2167.2205, 5013.7725, 40.5756>>
						CASE 3			RETURN <<2188.0869, 5033.6650, 42.3770>>
						CASE 4			RETURN <<2199.0774, 5024.1235, 42.5316>>
						CASE 5			RETURN <<2202.2307, 5012.9106, 42.2158>>
						CASE 6			RETURN <<2203.6233, 5011.8726, 42.2368>>
						CASE 7			RETURN <<2197.6406, 5001.4590, 41.5720>>
						CASE 8			RETURN <<2177.6150, 5009.3672, 40.9600>>
						CASE 9			RETURN <<2177.2900, 5010.7310, 40.9890>>
						CASE 10			RETURN <<2162.2458, 5025.0967, 41.3096>>
						CASE 11			RETURN <<2187.3560, 5023.1851, 41.7870>>
						CASE 12			RETURN <<2182.4619, 5014.8359, 41.4220>>
						CASE 13			RETURN <<2190.4165, 5011.6968, 41.4875>>
						CASE 14			RETURN <<2193.4846, 5019.3462, 42.1441>>
						CASE 15			RETURN <<2184.6421, 5017.5469, 44.2195>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_GoneBallistic_ElBurroHeights
					SWITCH iPed
						CASE 0			RETURN <<1095.5337, -2459.4270, 29.2530>>
						CASE 1			RETURN <<1094.8627, -2460.7246, 29.1998>>
						CASE 2			RETURN <<1105.6532, -2470.1970, 29.1586>>
						CASE 3			RETURN <<1104.4923, -2471.1584, 29.1091>>
						CASE 4			RETURN <<1114.7506, -2466.7268, 29.5699>>
						CASE 5			RETURN <<1126.0725, -2460.8755, 30.1690>>
						CASE 6			RETURN <<1124.8936, -2461.8545, 30.1320>>
						CASE 7			RETURN <<1110.8895, -2441.3108, 30.1803>>
						CASE 8			RETURN <<1121.2705, -2448.3872, 30.1762>>
						CASE 9			RETURN <<1119.7369, -2460.8235, 29.9595>>
						CASE 10			RETURN <<1111.1157, -2452.0835, 29.9753>>
						CASE 11			RETURN <<1104.0958, -2445.8948, 29.6733>>
						CASE 12			RETURN <<1104.3300, -2447.5317, 29.7263>>
						CASE 13			RETURN <<1116.5288, -2450.1274, 30.0100>>
						CASE 14			RETURN <<1123.4244, -2457.0112, 30.1597>>
						CASE 15			RETURN <<1114.7329, -2456.0825, 33.1737>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_GoneBallistic_Terminal
					SWITCH iPed
						CASE 0			RETURN <<1005.6957, -3184.5618, 4.9009>>
						CASE 1			RETURN <<1017.3247, -3209.6392, 4.8698>>
						CASE 2			RETURN <<1007.0870, -3206.5920, 4.9010>>
						CASE 3			RETURN <<1007.9750, -3205.3279, 4.9020>>
						CASE 4			RETURN <<1000.5927, -3204.5703, 4.9011>>
						CASE 5			RETURN <<1003.1410, -3190.3550, 4.9010>>
						CASE 6			RETURN <<1004.5640, -3189.2839, 4.9010>>
						CASE 7			RETURN <<1017.4940, -3184.2041, 4.9010>>
						CASE 8			RETURN <<1026.3695, -3203.8352, 4.9018>>
						CASE 9			RETURN <<1024.6918, -3204.2429, 4.9018>>
						CASE 10			RETURN <<1001.2950, -3186.4067, 4.9008>>
						CASE 11			RETURN <<1016.0613, -3191.5767, 4.9012>>
						CASE 12			RETURN <<1014.2804, -3203.8936, 4.9016>>
						CASE 13			RETURN <<1003.8205, -3197.3086, 4.9012>>
						CASE 14			RETURN <<1011.1102, -3190.5640, 4.9012>>
						CASE 15			RETURN <<1014.3403, -3197.6824, 7.6263>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_GoneBallistic_LSIA
					SWITCH iPed
						CASE 0			RETURN <<-1200.6871, -2060.2793, 12.9248>>
						CASE 1			RETURN <<-1221.4945, -2052.1003, 13.0912>>
						CASE 2			RETURN <<-1196.7434, -2047.1868, 12.9247>>
						CASE 3			RETURN <<-1196.8802, -2048.6663, 12.9247>>
						CASE 4			RETURN <<-1215.0326, -2045.3179, 13.2313>>
						CASE 5			RETURN <<-1214.3477, -2043.6783, 13.2213>>
						CASE 6			RETURN <<-1204.7457, -2073.3828, 12.9248>>
						CASE 7			RETURN <<-1205.3413, -2074.7410, 12.9247>>
						CASE 8			RETURN <<-1218.7457, -2064.0364, 13.4553>>
						CASE 9			RETURN <<-1220.2483, -2064.2012, 13.4337>>
						CASE 10			RETURN <<-1213.1528, -2053.6704, 13.4407>>
						CASE 11			RETURN <<-1213.6979, -2065.4500, 13.2733>>
						CASE 12			RETURN <<-1202.2809, -2056.0149, 12.9248>>
						CASE 13			RETURN <<-1209.5458, -2072.5168, 12.9246>>
						CASE 14			RETURN <<-1214.5422, -2059.5400, 13.4419>>
						CASE 15			RETURN <<-1207.9708, -2059.4358, 15.6479>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_GoneBallistic_MurrietaHeights
					SWITCH iPed
						CASE 0			RETURN <<1296.5342, -1943.5673, 42.3183>>
						CASE 1			RETURN <<1295.6066, -1944.5842, 42.3342>>
						CASE 2			RETURN <<1287.5542, -1934.1464, 42.2639>>
						CASE 3			RETURN <<1286.5157, -1935.0806, 42.2639>>
						CASE 4			RETURN <<1281.1903, -1954.1692, 42.5710>>
						CASE 5			RETURN <<1264.9829, -1942.0034, 42.2639>>
						CASE 6			RETURN <<1270.7076, -1957.7825, 42.2643>>
						CASE 7			RETURN <<1265.5623, -1952.8966, 42.2639>>
						CASE 8			RETURN <<1265.5885, -1951.4998, 42.2639>>
						CASE 9			RETURN <<1274.8234, -1958.2600, 42.2876>>
						CASE 10			RETURN <<1275.6368, -1956.6715, 42.3147>>
						CASE 11			RETURN <<1283.1862, -1949.1697, 42.2872>>
						CASE 12			RETURN <<1277.9402, -1940.4999, 42.2639>>
						CASE 13			RETURN <<1276.7777, -1952.1313, 42.2382>>
						CASE 14			RETURN <<1271.1180, -1945.8859, 42.2639>>
						CASE 15			RETURN <<1281.4061, -1944.5304, 44.9557>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_GoneBallistic_ElysianIsland2
					SWITCH iPed
						CASE 0			RETURN <<-431.9308, -2729.0435, 5.0002>>
						CASE 1			RETURN <<-420.3070, -2709.1455, 5.0002>>
						CASE 2			RETURN <<-413.1069, -2708.6990, 5.0002>>
						CASE 3			RETURN <<-414.2298, -2709.7598, 5.0002>>
						CASE 4			RETURN <<-412.6486, -2743.2529, 5.0002>>
						CASE 5			RETURN <<-395.2530, -2730.0750, 5.0110>>
						CASE 6			RETURN <<-396.2960, -2731.4089, 5.0100>>
						CASE 7			RETURN <<-405.2123, -2742.8591, 5.0002>>
						CASE 8			RETURN <<-422.2481, -2740.1201, 5.0002>>
						CASE 9			RETURN <<-420.4359, -2739.3435, 5.0002>>
						CASE 10			RETURN <<-432.8715, -2731.1270, 5.0002>>
						CASE 11			RETURN <<-421.3361, -2721.6719, 5.0002>>
						CASE 12			RETURN <<-415.1946, -2731.6809, 5.0002>>
						CASE 13			RETURN <<-426.3370, -2724.7610, 5.0002>>
						CASE 14			RETURN <<-423.4526, -2731.8691, 5.0002>>
						CASE 15			RETURN <<-418.0486, -2726.2561, 8.3466>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_GoneBallistic_PaletoBay
					SWITCH iPed
						CASE 0			RETURN <<-426.6687, 6425.0913, 1.8663>>
						CASE 1			RETURN <<-431.9579, 6443.7964, 2.2645>>
						CASE 2			RETURN <<-426.3965, 6434.4204, 2.3041>>
						CASE 3			RETURN <<-427.9860, 6434.7061, 2.2394>>
						CASE 4			RETURN <<-420.9776, 6431.7349, 2.4910>>
						CASE 5			RETURN <<-421.1429, 6416.9468, 1.8822>>
						CASE 6			RETURN <<-421.5555, 6415.2686, 1.6494>>
						CASE 7			RETURN <<-427.7790, 6410.8047, 1.7399>>
						CASE 8			RETURN <<-436.4338, 6410.4009, 1.8261>>
						CASE 9			RETURN <<-437.7088, 6411.7275, 1.8666>>
						CASE 10			RETURN <<-446.6407, 6421.4058, 1.4486>>
						CASE 11			RETURN <<-418.7654, 6422.6162, 2.0660>>
						CASE 12			RETURN <<-445.8712, 6422.6172, 1.4634>>
						CASE 13			RETURN <<-427.2618, 6419.0898, 2.1037>>
						CASE 14			RETURN <<-434.9174, 6422.3770, 1.9485>>
						CASE 15			RETURN <<-430.8662, 6425.1563, 4.4761>>
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_GoneBallistic_FortZancudo
					SWITCH iPed
						CASE 0			RETURN <<-2769.9790, 3461.7883, 10.9019>>
						CASE 1			RETURN <<-2764.4692, 3455.2703, 10.6259>>
						CASE 2			RETURN <<-2746.2927, 3471.2529, 10.7006>>
						CASE 3			RETURN <<-2767.5105, 3466.6055, 10.8599>>
						CASE 4			RETURN <<-2767.1606, 3467.9307, 10.7639>>
						CASE 5			RETURN <<-2749.4956, 3463.4946, 10.9637>>
						CASE 6			RETURN <<-2750.2444, 3464.7046, 10.8560>>
						CASE 7			RETURN <<-2755.6423, 3454.9365, 10.6892>>
						CASE 8			RETURN <<-2756.6992, 3453.9116, 10.6766>>
						CASE 9			RETURN <<-2763.5691, 3483.7261, 9.8435>>
						CASE 10			RETURN <<-2764.6492, 3482.8396, 9.8821>>
						CASE 11			RETURN <<-2755.6250, 3460.6145, 10.6689>>
						CASE 12			RETURN <<-2763.3970, 3473.0767, 10.3706>>
						CASE 13			RETURN <<-2754.8462, 3467.8679, 10.5160>>
						CASE 14			RETURN <<-2764.7866, 3461.6946, 10.7481>>
						CASE 15			RETURN <<-2759.4270, 3468.6589, 13.3811>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_FLIGHT_RECORDER
			SWITCH iPed
				CASE 1		RETURN <<0.0, -1.2, 0.0>>
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC FLOAT GET_GANGOPS_PED_SPAWN_HEADING(INT iPed, GANGOPS_VARIATION eVariation, GANGOPS_LOCATION eLocation, FREEMODE_DELIVERY_DROPOFFS eDropOff = FREEMODE_DELIVERY_DROPOFF_INVALID, INT iRespawnLocation = 0)

	UNUSED_PARAMETER(eDropOff)
	UNUSED_PARAMETER(iRespawnLocation)

	SWITCH eVariation
		CASE GOV_MAX			
			SWITCH eLocation
				CASE GangOpsLocation_Max
					SWITCH iPed
						CASE 0			RETURN 193.9980
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE GOV_DAYLIGHT_ROBBERY
			SWITCH eLocation
				CASE GangOpsLocation_DaylightRobbery_MirrorPark
					SWITCH iPed
						CASE 0			RETURN 161.5994
						CASE 1			RETURN 259.5989
						CASE 2			RETURN 74.7986
						CASE 3			RETURN 286.5983
						CASE 4			RETURN 101.3977
						CASE 5			RETURN 308.9967
						CASE 6			RETURN 161.5962
						CASE 7			RETURN 340.3959
						CASE 8			RETURN 270.1957
						CASE 9			RETURN 137.5953
						CASE 10			RETURN 281.5947
						CASE 11			RETURN 17.3988
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DaylightRobbery_SanAndreas
					SWITCH iPed
						CASE 0			RETURN 1.2000
						CASE 1			RETURN 232.8000
						CASE 2			RETURN 137.2000
						CASE 3			RETURN 228.3990
						CASE 4			RETURN 318.7990
						CASE 5			RETURN 35.7980
						CASE 6			RETURN 179.3980
						CASE 7			RETURN 260.9970
						CASE 8			RETURN 71.7970
						CASE 9			RETURN 34.3970
						CASE 10			RETURN 120.7970
						CASE 11			RETURN 168.3990
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DaylightRobbery_Vespucci
					SWITCH iPed
						CASE 0			RETURN 153.0000
						CASE 1			RETURN 33.9990
						CASE 2			RETURN 33.9990
						CASE 3			RETURN 294.5990
						CASE 4			RETURN 103.7990
						CASE 5			RETURN 107.9990
						CASE 6			RETURN 332.1980
						CASE 7			RETURN 240.9970
						CASE 8			RETURN 291.9970
						CASE 9			RETURN 79.7960
						CASE 10			RETURN 119.5960
						CASE 11			RETURN 13.9960
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DaylightRobbery_LegionSq
					SWITCH iPed
						CASE 0			RETURN 82.0000
						CASE 1			RETURN 120.0000
						CASE 2			RETURN 231.1990
						CASE 3			RETURN 342.3990
						CASE 4			RETURN 282.1990
						CASE 5			RETURN 231.5980
						CASE 6			RETURN 287.5980
						CASE 7			RETURN 156.1980
						CASE 8			RETURN 76.9970
						CASE 9			RETURN 332.7970
						CASE 10			RETURN 39.3960
						CASE 11			RETURN 36.7960
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DaylightRobbery_DelPerro
					SWITCH iPed
						CASE 0			RETURN 33.1999
						CASE 1			RETURN 211.9996
						CASE 2			RETURN 350.9991
						CASE 3			RETURN 265.1984
						CASE 4			RETURN 224.7983
						CASE 5			RETURN 125.5983
						CASE 6			RETURN 176.1981
						CASE 7			RETURN 212.9979
						CASE 8			RETURN 125.9978
						CASE 9			RETURN 47.7976
						CASE 10			RETURN 51.3976
						CASE 11			RETURN 174.5972
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DaylightRobbery_Rockford
					SWITCH iPed
						CASE 0			RETURN 278.8000
						CASE 1			RETURN 253.5990
						CASE 2			RETURN 181.5990
						CASE 3			RETURN 307.5990
						CASE 4			RETURN 157.9980
						CASE 5			RETURN 231.9980
						CASE 6			RETURN 110.7980
						CASE 7			RETURN 51.3980
						CASE 8			RETURN 285.3980
						CASE 9			RETURN 337.3970
						CASE 10			RETURN 83.9970
						CASE 11			RETURN 295.3970
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DaylightRobbery_WestVinewood
					SWITCH iPed
						CASE 0			RETURN 342.2000
						CASE 1			RETURN 187.7990
						CASE 2			RETURN 187.7990
						CASE 3			RETURN 119.7990
						CASE 4			RETURN 38.9980
						CASE 5			RETURN 100.7980
						CASE 6			RETURN 188.9980
						CASE 7			RETURN 282.7970
						CASE 8			RETURN 357.1970
						CASE 9			RETURN 349.9970
						CASE 10			RETURN 171.3970
						CASE 11			RETURN 216.5970
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DaylightRobbery_Burton
					SWITCH iPed
						CASE 0			RETURN 60.5999
						CASE 1			RETURN 185.3993
						CASE 2			RETURN 101.1993
						CASE 3			RETURN 72.1991
						CASE 4			RETURN 10.9990
						CASE 5			RETURN 209.3992
						CASE 6			RETURN 179.1992
						CASE 7			RETURN 229.5990
						CASE 8			RETURN 25.5988
						CASE 9			RETURN 139.1982
						CASE 10			RETURN 234.9981
						CASE 11			RETURN 11.3982
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DaylightRobbery_DelPerroPlaza
					SWITCH iPed
						CASE 0			RETURN 133.7997
						CASE 1			RETURN 270.1992
						CASE 2			RETURN 234.9988
						CASE 3			RETURN 319.9979
						CASE 4			RETURN 22.1988
						CASE 5			RETURN 227.7984
						CASE 6			RETURN 214.7984
						CASE 7			RETURN 53.1982
						CASE 8			RETURN 130.5980
						CASE 9			RETURN 210.1969
						CASE 10			RETURN 67.7971
						CASE 11			RETURN 58.9964
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DaylightRobbery_Pillbox
					SWITCH iPed
						CASE 0			RETURN 30.6000
						CASE 1			RETURN 132.7990
						CASE 2			RETURN 324.9990
						CASE 3			RETURN 360.7980
						CASE 4			RETURN 3.7980
						CASE 5			RETURN 243.3980
						CASE 6			RETURN 164.1970
						CASE 7			RETURN 131.9970
						CASE 8			RETURN 84.9980
						CASE 9			RETURN 333.7970
						CASE 10			RETURN 208.7960
						CASE 11			RETURN 121.7960
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE GOV_FLARE_UP			
			SWITCH eLocation
				CASE GangOpsLocation_FlareUp_Chamberlain_Hills
					SWITCH iPed
						CASE 0			RETURN 41.7980
						CASE 1			RETURN 56.1980
						CASE 2			RETURN 45.1980
						CASE 3			RETURN 65.1980
						CASE 4			RETURN 223.3990
						CASE 5			RETURN 215.3990
						CASE 6			RETURN 209.5990
						CASE 7			RETURN 209.5990
						CASE 8			RETURN 54.9980
						CASE 9			RETURN 70.5980
						CASE 10			RETURN 232.3980
						CASE 11			RETURN 246.9980
						CASE 12			RETURN 90.9980
						CASE 13			RETURN 274.1970
						CASE 14			RETURN 213.9970
						CASE 15			RETURN 40.7980
						CASE 16			RETURN 43.1970
						CASE 17			RETURN 44.3970
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Morningwood
					SWITCH iPed
						CASE 0			RETURN 251.7990
						CASE 1			RETURN 20.7990
						CASE 2			RETURN 36.7990
						CASE 3			RETURN 216.1990
						CASE 4			RETURN 6.7980
						CASE 5			RETURN 251.7990
						CASE 6			RETURN 211.3990
						CASE 7			RETURN 55.3990
						CASE 8			RETURN 60.9990
						CASE 9			RETURN 61.9989
						CASE 10			RETURN 40.9990
						CASE 11			RETURN 23.3990
						CASE 12			RETURN 266.1980
						CASE 13			RETURN 218.1980
						CASE 14			RETURN 218.1980
						CASE 15			RETURN 190.9980
						CASE 16			RETURN 218.1980
						CASE 17			RETURN 236.3979
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_La_Puerta
					SWITCH iPed
						CASE 0			RETURN 133.3989
						CASE 1			RETURN 180.7978
						CASE 2			RETURN 150.7978
						CASE 3			RETURN 94.1980
						CASE 4			RETURN 204.9978
						CASE 5			RETURN 58.5980
						CASE 6			RETURN 121.5979
						CASE 7			RETURN 78.3980
						CASE 8			RETURN 259.5978
						CASE 9			RETURN 249.3970
						CASE 10			RETURN 304.1970
						CASE 11			RETURN 74.3967
						CASE 12			RETURN 65.5968
						CASE 13			RETURN 236.5968
						CASE 14			RETURN 68.3957
						CASE 15			RETURN 254.9957
						CASE 16			RETURN 234.1976
						CASE 17			RETURN 234.1976
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_El_burro
					SWITCH iPed
						CASE 0			RETURN 104.3990
						CASE 1			RETURN 119.1988
						CASE 2			RETURN 316.9980
						CASE 3			RETURN 322.3980
						CASE 4			RETURN 93.3966
						CASE 5			RETURN 106.5970
						CASE 6			RETURN 314.9970
						CASE 7			RETURN 314.9970
						CASE 8			RETURN 171.3960
						CASE 9			RETURN 105.3960
						CASE 10			RETURN 274.1960
						CASE 11			RETURN 254.3950
						CASE 12			RETURN 156.5948
						CASE 13			RETURN 250.3948
						CASE 14			RETURN 144.3956
						CASE 15			RETURN 350.9990
						CASE 16			RETURN 118.5989
						CASE 17			RETURN 118.5989
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Davis
					SWITCH iPed
						CASE 0			RETURN 312.2000
						CASE 1			RETURN 336.1989
						CASE 2			RETURN 316.1988
						CASE 3			RETURN 303.7990
						CASE 4			RETURN 188.1978
						CASE 5			RETURN 162.5979
						CASE 6			RETURN 207.9981
						CASE 7			RETURN 151.5980
						CASE 8			RETURN 28.3980
						CASE 9			RETURN 91.3979
						CASE 10			RETURN 58.9980
						CASE 11			RETURN 24.7980
						CASE 12			RETURN 150.1970
						CASE 13			RETURN 352.1970
						CASE 14			RETURN 109.1969
						CASE 15			RETURN 344.9966
						CASE 16			RETURN 182.1978
						CASE 17			RETURN 182.1978
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Sandy
					SWITCH iPed
						CASE 0			RETURN 334.3990
						CASE 1			RETURN 333.9978
						CASE 2			RETURN 276.5979
						CASE 3			RETURN 131.5979
						CASE 4			RETURN 148.1980
						CASE 5			RETURN 116.5966
						CASE 6			RETURN 282.3970
						CASE 7			RETURN 148.5969
						CASE 8			RETURN 129.1969
						CASE 9			RETURN 331.1968
						CASE 10			RETURN 116.1960
						CASE 11			RETURN 303.9960
						CASE 12			RETURN 84.5960
						CASE 13			RETURN 337.1948
						CASE 14			RETURN 119.9946
						CASE 15			RETURN 167.3960
						CASE 16			RETURN 307.1977
						CASE 17			RETURN 291.5976
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Paleto
					SWITCH iPed
						CASE 0			RETURN 30.5990
						CASE 1			RETURN 19.5990
						CASE 2			RETURN 24.5990
						CASE 3			RETURN 236.1978
						CASE 4			RETURN 232.1980
						CASE 5			RETURN 186.3980
						CASE 6			RETURN 190.3980
						CASE 7			RETURN 34.5979
						CASE 8			RETURN 13.5980
						CASE 9			RETURN 247.3980
						CASE 10			RETURN 213.3468
						CASE 11			RETURN 36.3470
						CASE 12			RETURN 206.7460
						CASE 13			RETURN 30.3459
						CASE 14			RETURN 20.5460
						CASE 15			RETURN 217.5461
						CASE 16			RETURN 190.3980
						CASE 17			RETURN 190.3980
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Capital
					SWITCH iPed
						CASE 0			RETURN 1.3990
						CASE 1			RETURN 336.1988
						CASE 2			RETURN 12.7990
						CASE 3			RETURN 28.1990
						CASE 4			RETURN 192.1980
						CASE 5			RETURN 190.1980
						CASE 6			RETURN 198.1977
						CASE 7			RETURN 216.9980
						CASE 8			RETURN 200.7980
						CASE 9			RETURN 12.7980
						CASE 10			RETURN 199.3970
						CASE 11			RETURN 196.3970
						CASE 12			RETURN 3.3970
						CASE 13			RETURN 232.1970
						CASE 14			RETURN 59.1960
						CASE 15			RETURN 222.5960
						CASE 16			RETURN 350.3990
						CASE 17			RETURN 350.3990
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Clinton
					SWITCH iPed
						CASE 0			RETURN 346.9997
						CASE 1			RETURN 315.7990
						CASE 2			RETURN 352.3987
						CASE 3			RETURN 309.5989
						CASE 4			RETURN 126.1990
						CASE 5			RETURN 139.3980
						CASE 6			RETURN 166.3978
						CASE 7			RETURN 162.7977
						CASE 8			RETURN 169.5980
						CASE 9			RETURN 354.9970
						CASE 10			RETURN 293.1970
						CASE 11			RETURN 158.5970
						CASE 12			RETURN 345.5960
						CASE 13			RETURN 350.5954
						CASE 14			RETURN 177.1950
						CASE 15			RETURN 134.7950
						CASE 16			RETURN 348.1986
						CASE 17			RETURN 320.9984
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Richman
					SWITCH iPed
						CASE 0			RETURN 183.5980
						CASE 1			RETURN 162.5980
						CASE 2			RETURN 152.5980
						CASE 3			RETURN 170.7980
						CASE 4			RETURN 309.5980
						CASE 5			RETURN 15.1970
						CASE 6			RETURN 338.3970
						CASE 7			RETURN 8.9970
						CASE 8			RETURN 3.3970
						CASE 9			RETURN 339.3970
						CASE 10			RETURN 146.1960
						CASE 11			RETURN 335.7960
						CASE 12			RETURN 180.3960
						CASE 13			RETURN 0.3950
						CASE 14			RETURN 156.3950
						CASE 15			RETURN 8.1950
						CASE 16			RETURN 209.1940
						CASE 17			RETURN 315.9940
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE GOV_FORCED_ENTRY	
			SWITCH eLocation
				CASE GangOpsLocation_ForcedEntry_Location_Fudge		
					SWITCH iPed
						CASE 0			RETURN 194.9990
						CASE 1			RETURN 194.9990
						CASE 2			RETURN 203.9990
						CASE 3			RETURN 323.5980
						CASE 4			RETURN 219.5980
						CASE 5			RETURN 37.3980
						CASE 6			RETURN 87.3989
						CASE 7			RETURN 180.1994
						CASE 8			RETURN FORCED_ENTRY_COVER_HEADING()
						CASE 9			RETURN 271.7992
						CASE 10			RETURN 22.7283
						CASE 11			RETURN 266.9973
						CASE 12			RETURN 18.5432
						CASE 13			RETURN 10.8818
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Magellan
					SWITCH iPed
						CASE 0			RETURN 42.9990
						CASE 1			RETURN 299.2000
						CASE 2			RETURN 142.5990
						CASE 3			RETURN 142.5990
						CASE 4			RETURN 328.9990
						CASE 5			RETURN 139.7980
						CASE 6			RETURN 226.1984
						CASE 7			RETURN 180.1994
						CASE 8			RETURN FORCED_ENTRY_COVER_HEADING()
						CASE 9			RETURN 271.7992
						CASE 10			RETURN 22.7283
						CASE 11			RETURN 266.9973
						CASE 12			RETURN 18.5432
						CASE 13			RETURN 10.8818
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Forum
					SWITCH iPed
						CASE 0			RETURN 304.3300
						CASE 1			RETURN 94.9990
						CASE 2			RETURN 94.9990
						CASE 3			RETURN 304.3990
						CASE 4			RETURN 178.5290
						CASE 5			RETURN 348.9290
						CASE 6			RETURN 250.1970
						CASE 7			RETURN 180.1994
						CASE 8			RETURN FORCED_ENTRY_COVER_HEADING()
						CASE 9			RETURN 271.7992
						CASE 10			RETURN 22.7283
						CASE 11			RETURN 266.9973
						CASE 12			RETURN 18.5432
						CASE 13			RETURN 10.8818
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Roy_Lowenstein
					SWITCH iPed
						CASE 0			RETURN 42.4730
						CASE 1			RETURN 354.2000
						CASE 2			RETURN 354.2000
						CASE 3			RETURN 42.4730
						CASE 4			RETURN 139.4000
						CASE 5			RETURN 327.2000
						CASE 6			RETURN 250.7990
						CASE 7			RETURN 181.9983
						CASE 8			RETURN FORCED_ENTRY_COVER_HEADING()
						CASE 9			RETURN 272.3972
						CASE 10			RETURN 22.7283
						CASE 11			RETURN 266.9973
						CASE 12			RETURN 18.5432
						CASE 13			RETURN 10.8818
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Eastbourne
					SWITCH iPed
						CASE 0			RETURN 272.2000
						CASE 1			RETURN 251.9990
						CASE 2			RETURN 251.9990
						CASE 3			RETURN 272.2000
						CASE 4			RETURN 165.6000
						CASE 5			RETURN 344.9990
						CASE 6			RETURN 186.9990
						CASE 7			RETURN 181.9983
						CASE 8			RETURN FORCED_ENTRY_COVER_HEADING()
						CASE 9			RETURN 272.3972
						CASE 10			RETURN 22.7283
						CASE 11			RETURN 266.9973
						CASE 12			RETURN 18.5432
						CASE 13			RETURN 10.8818
					ENDSWITCH
				BREAK
				
				CASE GangOpsLocation_ForcedEntry_Location_V_Magellan		
					SWITCH iPed
						CASE 0			RETURN 212.4003
						CASE 1			RETURN 16.4003
						CASE 2			RETURN 120.0002
						CASE 3			RETURN 120.0002
						CASE 4			RETURN 2.9998
						CASE 5			RETURN 190.1989
						CASE 6			RETURN 292.7989
						CASE 7			RETURN 180.1994
						CASE 8			RETURN FORCED_ENTRY_COVER_HEADING()
						CASE 9			RETURN 271.7992
						CASE 10			RETURN 22.7283
						CASE 11			RETURN 266.9973
						CASE 12			RETURN 18.5432
						CASE 13			RETURN 10.8818
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Alta
					SWITCH iPed
						CASE 0			RETURN 68.5990
						CASE 1			RETURN 147.1990
						CASE 2			RETURN 253.7990
						CASE 3			RETURN 253.7990
						CASE 4			RETURN 109.1980
						CASE 5			RETURN 282.1980
						CASE 6			RETURN 164.5980
						CASE 7			RETURN 180.1994
						CASE 8			RETURN FORCED_ENTRY_COVER_HEADING()
						CASE 9			RETURN 271.7992
						CASE 10			RETURN 22.7283
						CASE 11			RETURN 266.9973
						CASE 12			RETURN 18.5432
						CASE 13			RETURN 10.8818
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Forum_Drive
					SWITCH iPed
						CASE 0			RETURN 319.9997
						CASE 1			RETURN 230.9990
						CASE 2			RETURN 177.7990
						CASE 3			RETURN 177.7990
						CASE 4			RETURN 31.1984
						CASE 5			RETURN 206.3982
						CASE 6			RETURN 126.1993
						CASE 7			RETURN 180.1994
						CASE 8			RETURN FORCED_ENTRY_COVER_HEADING()
						CASE 9			RETURN 271.7992
						CASE 10			RETURN 22.7283
						CASE 11			RETURN 266.9973
						CASE 12			RETURN 18.5432
						CASE 13			RETURN 10.8818
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Spanish
					SWITCH iPed
						CASE 0			RETURN 181.1998
						CASE 1			RETURN 301.7995
						CASE 2			RETURN 252.3992
						CASE 3			RETURN 252.3992
						CASE 4			RETURN 275.3992
						CASE 5			RETURN 86.1988
						CASE 6			RETURN 201.9982
						CASE 7			RETURN 181.9983
						CASE 8			RETURN FORCED_ENTRY_COVER_HEADING()
						CASE 9			RETURN 272.3972
						CASE 10			RETURN 22.7283
						CASE 11			RETURN 266.9973
						CASE 12			RETURN 18.5432
						CASE 13			RETURN 10.8818
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_ForcedEntry_Location_Tahitian
					SWITCH iPed
						CASE 0			RETURN 316.3323
						CASE 1			RETURN 14.1989
						CASE 2			RETURN 43.3990
						CASE 3			RETURN 43.3990
						CASE 4			RETURN 243.1989
						CASE 5			RETURN 57.5979
						CASE 6			RETURN 166.7980
						CASE 7			RETURN 181.9983
						CASE 8			RETURN FORCED_ENTRY_COVER_HEADING()
						CASE 9			RETURN 272.3972
						CASE 10			RETURN 22.7283
						CASE 11			RETURN 266.9973
						CASE 12			RETURN 18.5432
						CASE 13			RETURN 10.8818
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_CAR_COLLECTOR
			// Garage
			SWITCH iPed
				CASE 20			RETURN 4.3212
				CASE 21			RETURN 155.5531
				CASE 22			RETURN 268.3627
			ENDSWITCH
			
			SWITCH eLocation
				CASE GangOpsLocation_CarCollector_SteeleWay
					SWITCH iPed
						CASE 0			RETURN 129.7987
						CASE 1			RETURN 301.9990
						CASE 2			RETURN 102.5990
						CASE 3			RETURN 187.5990
						CASE 4			RETURN 202.7990
						CASE 5			RETURN 285.7980
						CASE 6			RETURN 147.7970
						CASE 7			RETURN 4.7980
						CASE 8			RETURN -0.0020
						CASE 9			RETURN 28.7980
						CASE 10			RETURN 203.7980
						CASE 11			RETURN 201.1980
						CASE 12			RETURN 195.5990
						CASE 13			RETURN 272.9990
						CASE 14			RETURN 265.9980
						CASE 15			RETURN 305.7980
						CASE 16			RETURN 134.7975
						CASE 17			RETURN 11.3970
						CASE 18			RETURN 301.5970
						CASE 19			RETURN 103.5970
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_CarCollector_AmericanoWay
					SWITCH iPed
						CASE 0			RETURN 353.8000
						CASE 1			RETURN 6.3990
						CASE 2			RETURN 182.5990
						CASE 3			RETURN 169.3990
						CASE 4			RETURN 89.9980
						CASE 5			RETURN 88.8000
						CASE 6			RETURN 54.6000
						CASE 7			RETURN 239.9990
						CASE 8			RETURN 307.1990
						CASE 9			RETURN 271.9998
						CASE 10			RETURN 134.9993
						CASE 11			RETURN 320.7991
						CASE 12			RETURN 232.5990
						CASE 13			RETURN 47.3990
						CASE 14			RETURN 344.3986
						CASE 15			RETURN 14.9984
						CASE 16			RETURN 342.9980
						CASE 17			RETURN 9.3993
						CASE 18			RETURN 99.3991
						CASE 19			RETURN 311.5988
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_CarCollector_AceJonesDrive
					SWITCH iPed
						CASE 0			RETURN 0.0000
						CASE 1			RETURN 214.1999
						CASE 2			RETURN 65.0000
						CASE 3			RETURN 312.7999
						CASE 4			RETURN 276.3996
						CASE 5			RETURN 331.5995
						CASE 6			RETURN 146.7991
						CASE 7			RETURN 72.3988
						CASE 8			RETURN 270.1982
						CASE 9			RETURN 76.7975
						CASE 10			RETURN 266.7973
						CASE 11			RETURN 193.7980
						CASE 12			RETURN 311.7969
						CASE 13			RETURN 130.1968
						CASE 14			RETURN 176.1975
						CASE 15			RETURN 33.3990
						CASE 16			RETURN 349.3973
						CASE 17			RETURN 156.9969
						CASE 18			RETURN 175.7999
						CASE 19			RETURN 241.9997
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_CarCollector_Richman
					SWITCH iPed
						CASE 0			RETURN 42.0000
						CASE 1			RETURN 124.0000
						CASE 2			RETURN 56.3998
						CASE 3			RETURN 7.5998
						CASE 4			RETURN 126.5996
						CASE 5			RETURN 13.5993
						CASE 6			RETURN 196.3988
						CASE 7			RETURN 196.3988
						CASE 8			RETURN 24.1987
						CASE 9			RETURN 61.7986
						CASE 10			RETURN 295.1985
						CASE 11			RETURN 111.1979
						CASE 12			RETURN 218.5970
						CASE 13			RETURN 335.3966
						CASE 14			RETURN 70.9950
						CASE 15			RETURN 55.3975
						CASE 16			RETURN 233.5972
						CASE 17			RETURN 201.7948
						CASE 18			RETURN 232.5984
						CASE 19			RETURN 31.7981
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_CarCollector_NorthRockford_1
					SWITCH iPed
						CASE 0			RETURN 128.9998
						CASE 1			RETURN 57.9990
						CASE 2			RETURN 171.0000
						CASE 3			RETURN 81.5990
						CASE 4			RETURN 318.3980
						CASE 5			RETURN 38.5988
						CASE 6			RETURN 255.9985
						CASE 7			RETURN 83.7983
						CASE 8			RETURN 313.5979
						CASE 9			RETURN 131.5970
						CASE 10			RETURN 40.3968
						CASE 11			RETURN 212.3966
						CASE 12			RETURN 224.9966
						CASE 13			RETURN 190.7966
						CASE 14			RETURN 225.5960
						CASE 15			RETURN 215.9960
						CASE 16			RETURN 98.9960
						CASE 17			RETURN 314.1956
						CASE 18			RETURN 293.1954
						CASE 19			RETURN 112.5950
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_CarCollector_DunstableLane_1
					SWITCH iPed
						CASE 0			RETURN 97.1998
						CASE 1			RETURN 135.1996
						CASE 2			RETURN 73.1995
						CASE 3			RETURN 216.3990
						CASE 4			RETURN 42.9989
						CASE 5			RETURN 8.1988
						CASE 6			RETURN 278.1983
						CASE 7			RETURN 105.1978
						CASE 8			RETURN 22.3976
						CASE 9			RETURN 208.9973
						CASE 10			RETURN 246.9972
						CASE 11			RETURN 59.7970
						CASE 12			RETURN 107.7968
						CASE 13			RETURN 316.5964
						CASE 14			RETURN 136.3959
						CASE 15			RETURN 10.3957
						CASE 16			RETURN 191.7955
						CASE 17			RETURN 282.5971
						CASE 18			RETURN 104.3974
						CASE 19			RETURN 101.3465
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_CarCollector_DunstableLane_2
					SWITCH iPed
						CASE 0			RETURN 46.9999
						CASE 1			RETURN 290.1996
						CASE 2			RETURN 239.5995
						CASE 3			RETURN 156.9994
						CASE 4			RETURN 347.7990
						CASE 5			RETURN 1.5988
						CASE 6			RETURN 162.3985
						CASE 7			RETURN 3.3984
						CASE 8			RETURN 192.1981
						CASE 9			RETURN 227.5982
						CASE 10			RETURN 46.7978
						CASE 11			RETURN 42.1968
						CASE 12			RETURN 218.3967
						CASE 13			RETURN 48.5966
						CASE 14			RETURN 347.7960
						CASE 15			RETURN 161.1954
						CASE 16			RETURN 102.1951
						CASE 17			RETURN 276.9941
						CASE 18			RETURN 274.5946
						CASE 19			RETURN 240.3949
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_CarCollector_NorthRockford_2
					SWITCH iPed
						CASE 0			RETURN 329.4000
						CASE 1			RETURN 259.1990
						CASE 2			RETURN 207.9990
						CASE 3			RETURN 161.5990
						CASE 4			RETURN 326.9990
						CASE 5			RETURN 103.5990
						CASE 6			RETURN 22.3990
						CASE 7			RETURN 290.5980
						CASE 8			RETURN 343.1980
						CASE 9			RETURN 24.9980
						CASE 10			RETURN 165.1970
						CASE 11			RETURN 344.5970
						CASE 12			RETURN 203.5970
						CASE 13			RETURN 355.3968
						CASE 14			RETURN 180.3958
						CASE 15			RETURN 273.9990
						CASE 16			RETURN 205.3980
						CASE 17			RETURN 13.5960
						CASE 18			RETURN 259.3950
						CASE 19			RETURN 81.7956
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_CarCollector_CaesarsPlace
					SWITCH iPed
						CASE 0			RETURN 179.5990
						CASE 1			RETURN 117.1990
						CASE 2			RETURN 244.9990
						CASE 3			RETURN 354.3980
						CASE 4			RETURN 192.9980
						CASE 5			RETURN 60.5980
						CASE 6			RETURN 189.9980
						CASE 7			RETURN 243.3980
						CASE 8			RETURN 146.5970
						CASE 9			RETURN 87.7970
						CASE 10			RETURN 266.1970
						CASE 11			RETURN 200.3960
						CASE 12			RETURN 10.7960
						CASE 13			RETURN 162.5960
						CASE 14			RETURN 84.7960
						CASE 15			RETURN 176.1950
						CASE 16			RETURN 4.9950
						CASE 17			RETURN 47.7940
						CASE 18			RETURN 96.9940
						CASE 19			RETURN 279.9940
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_CarCollector_HillcrestAvenue
					SWITCH iPed
						CASE 0			RETURN 310.2000
						CASE 1			RETURN 255.3990
						CASE 2			RETURN 167.5990
						CASE 3			RETURN 318.7980
						CASE 4			RETURN 139.7980
						CASE 5			RETURN 342.7980
						CASE 6			RETURN 29.7980
						CASE 7			RETURN 197.7980
						CASE 8			RETURN 178.3980
						CASE 9			RETURN 178.3980
						CASE 10			RETURN 302.9970
						CASE 11			RETURN 124.7970
						CASE 12			RETURN 349.5970
						CASE 13			RETURN 84.3960
						CASE 14			RETURN 232.5960
						CASE 15			RETURN 48.9960
						CASE 16			RETURN 176.1970
						CASE 17			RETURN 120.9970
						CASE 18			RETURN 208.3970
						CASE 19			RETURN 169.3970
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE GOV_MOB_MENTALITY			
			SWITCH eLocation
				CASE GangOpsLocation_MobMentality_Morningwood
					SWITCH iPed
						CASE 0			RETURN 140.1992
						CASE 1			RETURN 137.5988
						CASE 2			RETURN 68.3986
						CASE 3			RETURN -2.6015
						CASE 4			RETURN 194.9981
						CASE 5			RETURN 194.9981
						CASE 6			RETURN 202.7980
						CASE 7			RETURN 226.7979
						CASE 8			RETURN 78.5977
						CASE 9			RETURN 185.9968
						CASE 10			RETURN 341.9965
						CASE 11			RETURN 223.7957
						CASE 12			RETURN 44.1955
						CASE 13			RETURN 87.9947
						CASE 14			RETURN 264.1932
						CASE 15			RETURN 60.7934
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_MobMentality_Grapeseed
					SWITCH iPed
						CASE 0			RETURN 206.9997
						CASE 1			RETURN 19.1996
						CASE 2			RETURN 175.5991
						CASE 3			RETURN 350.9988
						CASE 4			RETURN 122.5984
						CASE 5			RETURN 231.1981
						CASE 6			RETURN 299.9978
						CASE 7			RETURN 272.9975
						CASE 8			RETURN 267.3974
						CASE 9			RETURN 360.3972
						CASE 10			RETURN 104.3970
						CASE 11			RETURN 53.9969
						CASE 12			RETURN 117.3966
						CASE 13			RETURN 10.1958
						CASE 14			RETURN 205.9996
						CASE 15			RETURN 159.7996
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_MobMentality_Alta
					SWITCH iPed
						CASE 0			RETURN 162.5994
						CASE 1			RETURN 47.1991
						CASE 2			RETURN 260.1989
						CASE 3			RETURN 187.5987
						CASE 4			RETURN 10.5961
						CASE 5			RETURN 249.9955
						CASE 6			RETURN 51.9954
						CASE 7			RETURN 331.1951
						CASE 8			RETURN 141.9948
						CASE 9			RETURN 261.5942
						CASE 10			RETURN 29.1940
						CASE 11			RETURN 131.9928
						CASE 12			RETURN 92.9960
						CASE 13			RETURN 92.9960
						CASE 14			RETURN 292.7957
						CASE 15			RETURN 287.1951
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_MobMentality_MissionRow
					SWITCH iPed
						CASE 0			RETURN 87.5989
						CASE 1			RETURN 87.5989
						CASE 2			RETURN 14.7981
						CASE 3			RETURN 197.5980
						CASE 4			RETURN 293.5977
						CASE 5			RETURN 140.5973
						CASE 6			RETURN 3.1971
						CASE 7			RETURN 132.9966
						CASE 8			RETURN 268.5962
						CASE 9			RETURN 14.1960
						CASE 10			RETURN 178.1957
						CASE 11			RETURN 277.5950
						CASE 12			RETURN 253.1945
						CASE 13			RETURN 9.1943
						CASE 14			RETURN 152.9939
						CASE 15			RETURN 203.5937
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_MobMentality_HippyCamp
					SWITCH iPed
						CASE 0			RETURN 79.3969
						CASE 1			RETURN 321.7986
						CASE 2			RETURN 39.8477
						CASE 3			RETURN 29.4478
						CASE 4			RETURN 236.3964
						CASE 5			RETURN 220.3455
						CASE 6			RETURN 115.7959
						CASE 7			RETURN 347.3946
						CASE 8			RETURN 135.5947
						CASE 9			RETURN 171.5948
						CASE 10			RETURN 195.9928
						CASE 11			RETURN 16.5939
						CASE 12			RETURN 158.5927
						CASE 13			RETURN 243.5927
						CASE 14			RETURN 46.3949
						CASE 15			RETURN 252.9949
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_MobMentality_SanAndreas
					SWITCH iPed
						CASE 0			RETURN 83.9974
						CASE 1			RETURN 271.5970
						CASE 2			RETURN 218.1967
						CASE 3			RETURN 33.9961
						CASE 4			RETURN 92.1989
						CASE 5			RETURN 36.9988
						CASE 6			RETURN 320.5985
						CASE 7			RETURN 212.1982
						CASE 8			RETURN 296.7977
						CASE 9			RETURN 258.7973
						CASE 10			RETURN 276.7968
						CASE 11			RETURN 223.7964
						CASE 12			RETURN 150.1963
						CASE 13			RETURN 345.5960
						CASE 14			RETURN 118.1972
						CASE 15			RETURN 196.9969
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_MobMentality_MirrorPark
					SWITCH iPed
						CASE 0			RETURN 351.0400
						CASE 1			RETURN 207.6390
						CASE 2			RETURN 84.4390
						CASE 3			RETURN 162.4387
						CASE 4			RETURN 28.8380
						CASE 5			RETURN 221.0380
						CASE 6			RETURN 242.0380
						CASE 7			RETURN 72.4380
						CASE 8			RETURN 342.4370
						CASE 9			RETURN 318.4370
						CASE 10			RETURN 141.0360
						CASE 11			RETURN 276.0360
						CASE 12			RETURN 190.2360
						CASE 13			RETURN 232.2350
						CASE 14			RETURN 43.0350
						CASE 15			RETURN 96.2350
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_MobMentality_Strawberry
					SWITCH iPed
						CASE 0			RETURN 72.1990
						CASE 1			RETURN 53.9990
						CASE 2			RETURN 107.9980
						CASE 3			RETURN 278.9980
						CASE 4			RETURN 149.1990
						CASE 5			RETURN 228.7980
						CASE 6			RETURN 215.8000
						CASE 7			RETURN 34.5990
						CASE 8			RETURN 254.7990
						CASE 9			RETURN 50.9980
						CASE 10			RETURN 188.7980
						CASE 11			RETURN 104.5950
						CASE 12			RETURN 141.9950
						CASE 13			RETURN 157.1995
						CASE 14			RETURN 277.1992
						CASE 15			RETURN 351.9985
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_MobMentality_MirrorParkTavern
					SWITCH iPed
						CASE 0			RETURN 87.5990
						CASE 1			RETURN 155.7990
						CASE 2			RETURN 155.7990
						CASE 3			RETURN 343.1990
						CASE 4			RETURN 308.9980
						CASE 5			RETURN 293.5980
						CASE 6			RETURN 146.3970
						CASE 7			RETURN 341.5970
						CASE 8			RETURN 68.5960
						CASE 9			RETURN 232.9960
						CASE 10			RETURN 7.9950
						CASE 11			RETURN 268.3945
						CASE 12			RETURN 85.5940
						CASE 13			RETURN 211.5940
						CASE 14			RETURN 323.7940
						CASE 15			RETURN 282.5930
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_MobMentality_ULSA
					SWITCH iPed
						CASE 0			RETURN 215.9989
						CASE 1			RETURN 43.7989
						CASE 2			RETURN 299.5986
						CASE 3			RETURN 238.5982
						CASE 4			RETURN 195.1980
						CASE 5			RETURN 219.3978
						CASE 6			RETURN 22.9977
						CASE 7			RETURN 127.5975
						CASE 8			RETURN 126.3974
						CASE 9			RETURN 302.3968
						CASE 10			RETURN 360.1964
						CASE 11			RETURN 132.5961
						CASE 12			RETURN 283.3953
						CASE 13			RETURN 356.9951
						CASE 14			RETURN 288.7945
						CASE 15			RETURN 98.1950
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_PARAMEDIC
			SWITCH eLocation
				CASE GangOpsLocation_Paramedic_Central_LS_Medical_Center
					SWITCH iPed
						CASE 0			RETURN 56.1990
						CASE 1			RETURN 208.5990
						CASE 2			RETURN 232.7990
						CASE 3			RETURN 228.9990
						CASE 4			RETURN 229.7990
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Pillbox_Hill_Medical_Center
					SWITCH iPed
						CASE 0			RETURN 260.7990
						CASE 1			RETURN 162.7980
						CASE 2			RETURN 254.1960
						CASE 3			RETURN 248.5960
						CASE 4			RETURN 93.5970
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_ST_Fiacre_Hospital
					SWITCH iPed
						CASE 0			RETURN 89.5977
						CASE 1			RETURN 186.5970
						CASE 2			RETURN -0.8020
						CASE 3			RETURN 303.9990
						CASE 4			RETURN 277.7970
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Sandy_Shores_Medical_Center
					SWITCH iPed
						CASE 0			RETURN 172.9993
						CASE 1			RETURN 149.1992
						CASE 2			RETURN 321.9984
						CASE 3			RETURN 263.1977
						CASE 4			RETURN 87.9973
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Paleto_Bay_Care_Center
					SWITCH iPed
						CASE 0			RETURN 84.1960
						CASE 1			RETURN 230.9958
						CASE 2			RETURN 254.9950
						CASE 3			RETURN 317.9950
						CASE 4			RETURN 314.5940
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Del_Perro_Freeway
					SWITCH iPed
						CASE 0			RETURN 298.7960
						CASE 1			RETURN 281.6913
						CASE 2			RETURN 130.9950
						CASE 3			RETURN 285.9940
						CASE 4			RETURN 217.7873
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Mount_Gordo
					SWITCH iPed
						CASE 0			RETURN 5.5950
						CASE 1			RETURN 183.7950
						CASE 2			RETURN 220.9938
						CASE 3			RETURN 288.5936
						CASE 4			RETURN 47.9932
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Great_Chaparral
					SWITCH iPed
						CASE 0			RETURN 223.7950
						CASE 1			RETURN 158.7950
						CASE 2			RETURN 51.3940
						CASE 3			RETURN 161.7940
						CASE 4			RETURN 206.6763
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Cassidy_Creek
					SWITCH iPed
						CASE 0			RETURN 298.1938
						CASE 1			RETURN 123.9929
						CASE 2			RETURN 100.1927
						CASE 3			RETURN 218.7918
						CASE 4			RETURN 326.9914
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Power_Station
					SWITCH iPed
						CASE 0			RETURN 262.5907
						CASE 1			RETURN 83.7903
						CASE 2			RETURN 272.1893
						CASE 3			RETURN 146.9899
						CASE 4			RETURN 177.5897
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_AQUALUNGS
			SWITCH eLocation
				CASE GangOpsLocation_Aqualungs_Elysian_Island_1
					SWITCH iPed
						CASE 0			RETURN 249.7970
						CASE 1			RETURN 65.7969
						CASE 2			RETURN 225.9960
						CASE 3			RETURN 337.1959
						CASE 4			RETURN 251.9951
						CASE 5			RETURN 55.1950
						CASE 6			RETURN 342.1954
						CASE 7			RETURN 299.7939
						CASE 8			RETURN 299.7939
						CASE 9			RETURN 122.9920
						CASE 10			RETURN 186.3918
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Paleto_Bay
					SWITCH iPed
						CASE 0			RETURN 123.7980
						CASE 1			RETURN 217.9980
						CASE 2			RETURN 326.5970
						CASE 3			RETURN 265.7979
						CASE 4			RETURN 12.7970
						CASE 5			RETURN 181.3970
						CASE 6			RETURN 241.3965
						CASE 7			RETURN 68.3963
						CASE 8			RETURN 68.3963
						CASE 9			RETURN 68.3963
						CASE 10			RETURN 139.1998
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Elysian_Island_2
					SWITCH iPed
						CASE 0			RETURN 54.2000
						CASE 1			RETURN 50.1980
						CASE 2			RETURN 122.7980
						CASE 3			RETURN 242.5970
						CASE 4			RETURN 160.3979
						CASE 5			RETURN 165.9970
						CASE 6			RETURN 352.3970
						CASE 7			RETURN 62.5960
						CASE 8			RETURN 250.1960
						CASE 9			RETURN 36.1978
						CASE 10			RETURN 36.1978
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Palomino_Highlands
					SWITCH iPed
						CASE 0			RETURN 24.9990
						CASE 1			RETURN 20.7986
						CASE 2			RETURN 156.9990
						CASE 3			RETURN 340.5990
						CASE 4			RETURN 299.5990
						CASE 5			RETURN 191.5980
						CASE 6			RETURN 277.7999
						CASE 7			RETURN 269.5980
						CASE 8			RETURN 81.7980
						CASE 9			RETURN 81.7980
						CASE 10			RETURN 81.7980
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Chumash
					SWITCH iPed
						CASE 0			RETURN 251.1978
						CASE 1			RETURN 335.9990
						CASE 2			RETURN 155.7980
						CASE 3			RETURN 252.5980
						CASE 4			RETURN 350.7979
						CASE 5			RETURN 81.5980
						CASE 6			RETURN 26.9989
						CASE 7			RETURN 299.7970
						CASE 8			RETURN 116.1970
						CASE 9			RETURN 116.1970
						CASE 10			RETURN 116.1970
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_San_Chianksi_Mountain_Range
					SWITCH iPed
						CASE 0			RETURN 175.7967
						CASE 1			RETURN 150.5980
						CASE 2			RETURN 70.7980
						CASE 3			RETURN 52.3988
						CASE 4			RETURN 261.9980
						CASE 5			RETURN 81.7970
						CASE 6			RETURN 331.7970
						CASE 7			RETURN 292.5970
						CASE 8			RETURN 115.9960
						CASE 9			RETURN 115.9960
						CASE 10			RETURN 115.9960
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_PaletoForest
					SWITCH iPed
						CASE 0			RETURN 281.1992
						CASE 1			RETURN 197.1990
						CASE 2			RETURN 279.9988
						CASE 3			RETURN 81.9986
						CASE 4			RETURN 72.9985
						CASE 5			RETURN 245.9983
						CASE 6			RETURN 305.9981
						CASE 7			RETURN 129.5974
						CASE 8			RETURN 223.7972
						CASE 9			RETURN 223.7972
						CASE 10			RETURN 223.7972
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_North_Chumash
					SWITCH iPed
						CASE 0			RETURN 279.7964
						CASE 1			RETURN 289.9980
						CASE 2			RETURN 118.3970
						CASE 3			RETURN 262.3970
						CASE 4			RETURN 359.5970
						CASE 5			RETURN 101.5970
						CASE 6			RETURN 20.9978
						CASE 7			RETURN 44.5960
						CASE 8			RETURN 219.7960
						CASE 9			RETURN 44.5960
						CASE 10			RETURN 44.5960
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Pacific_Bluffs
					SWITCH iPed
						CASE 0			RETURN 5.9966
						CASE 1			RETURN 117.5984
						CASE 2			RETURN 307.7980
						CASE 3			RETURN 339.1978
						CASE 4			RETURN 268.9970
						CASE 5			RETURN 151.5968
						CASE 6			RETURN 217.1988
						CASE 7			RETURN 81.3969
						CASE 8			RETURN 260.1966
						CASE 9			RETURN 260.1966
						CASE 10			RETURN 260.1966
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Vespucci_Beach
					SWITCH iPed
						CASE 0			RETURN 284.7957
						CASE 1			RETURN 352.5976
						CASE 2			RETURN 165.9969
						CASE 3			RETURN 300.9963
						CASE 4			RETURN 5.9960
						CASE 5			RETURN 116.9959
						CASE 6			RETURN 45.7978
						CASE 7			RETURN 30.9958
						CASE 8			RETURN 211.3955
						CASE 9			RETURN 211.3955
						CASE 10			RETURN 211.3955
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_UNDER_CONTROL
			SWITCH eLocation
				CASE GangOpsLocation_UnderControl_ServerRoom1
					SWITCH iPed
						CASE 0			RETURN 156.5958
						CASE 1			RETURN 156.5958
						CASE 2			RETURN 156.5958
						CASE 3			RETURN 156.5958
						CASE 4			RETURN 239.9954
						CASE 5			RETURN 265.5995
						CASE 6			RETURN 244.3977
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ServerRoom2
					SWITCH iPed
						CASE 0			RETURN 180.6000
						CASE 1			RETURN 180.6000
						CASE 2			RETURN 180.6000
						CASE 3			RETURN 180.6000
						CASE 4			RETURN 296.3980
						CASE 5			RETURN 258.3994
						CASE 6			RETURN 244.3977
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ServerRoom3
					SWITCH iPed
						CASE 0			RETURN 169.6000
						CASE 1			RETURN 169.6000
						CASE 2			RETURN 169.6000
						CASE 3			RETURN 169.6000
						CASE 4			RETURN 140.9990
						CASE 5			RETURN 218.1993
						CASE 6			RETURN 280.9974
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ServerRoom4
					SWITCH iPed
						CASE 0			RETURN 192.4000
						CASE 1			RETURN 192.4000
						CASE 2			RETURN 192.4000
						CASE 3			RETURN 192.4000
						CASE 4			RETURN 256.4000
						CASE 5			RETURN 214.1991
						CASE 6			RETURN 253.3972
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ServerRoom5
					SWITCH iPed
						CASE 0			RETURN 179.9990
						CASE 1			RETURN 179.9990
						CASE 2			RETURN 179.9990
						CASE 3			RETURN 179.9990
						CASE 4			RETURN 66.3987
						CASE 5			RETURN 198.5991
						CASE 6			RETURN 278.7971
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ControlRoom1
					SWITCH iPed
						CASE 0			RETURN 168.6000
						CASE 1			RETURN 168.6000
						CASE 2			RETURN 168.6000
						CASE 3			RETURN 168.6000
						CASE 4			RETURN 186.1980
						CASE 5			RETURN 150.3988
						CASE 6			RETURN 226.9960
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ControlRoom2
					SWITCH iPed
						CASE 0			RETURN 171.6000
						CASE 1			RETURN 171.6000
						CASE 2			RETURN 171.6000
						CASE 3			RETURN 171.6000
						CASE 4			RETURN 290.9988
						CASE 5			RETURN 147.3988
						CASE 6			RETURN 343.3964
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ControlRoom3
					SWITCH iPed
						CASE 0			RETURN 176.4000
						CASE 1			RETURN 176.4000
						CASE 2			RETURN 176.4000
						CASE 3			RETURN 176.4000
						CASE 4			RETURN 242.5980
						CASE 5			RETURN 158.3986
						CASE 6			RETURN 10.5962
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ControlRoom4
					SWITCH iPed
						CASE 0			RETURN 216.7990
						CASE 1			RETURN 216.7990
						CASE 2			RETURN 216.7990
						CASE 3			RETURN 216.7990
						CASE 4			RETURN 32.9990
						CASE 5			RETURN 131.3983
						CASE 6			RETURN 310.5961
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_UnderControl_ControlRoom5
					SWITCH iPed
						CASE 0			RETURN 165.1990
						CASE 1			RETURN 165.1990
						CASE 2			RETURN 165.1990
						CASE 3			RETURN 165.1990
						CASE 4			RETURN 254.9986
						CASE 5			RETURN 143.9982
						CASE 6			RETURN 62.7957
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_BURGLARY_JOB
			SWITCH iPed
				CASE 0			RETURN 89.7996
				CASE 1			RETURN 96.6159
				CASE 2			RETURN 284.3974
				CASE 3			RETURN 180.5971
				CASE 4			RETURN 98.5997
			ENDSWITCH
			
			SWITCH eLocation
				CASE GangOpsLocation_BurglaryJob_ProcopioDrive
					SWITCH iPed
						CASE 5			RETURN 229.3997
						CASE 6			RETURN 24.3988
						CASE 7			RETURN 230.9988
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_BurglaryJob_SouthMoMilton
					SWITCH iPed
						CASE 5			RETURN 131.7990
						CASE 6			RETURN 125.9999
						CASE 7			RETURN 50.7990
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_BurglaryJob_MirrorPark
					SWITCH iPed
						CASE 5			RETURN 142.1994
						CASE 6			RETURN 280.1989
						CASE 7			RETURN 280.1989
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_BurglaryJob_ImaginationCourt
					SWITCH iPed
						CASE 5			RETURN 127.1999
						CASE 6			RETURN 42.5997
						CASE 7			RETURN 217.5994
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_BurglaryJob_DidionDrive
					SWITCH iPed
						CASE 5			RETURN 0.0000
						CASE 6			RETURN 174.5996
						CASE 7			RETURN 7.5993
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_BurglaryJob_KimbleHill
					SWITCH iPed
						CASE 5			RETURN 357.1994
						CASE 6			RETURN 190.5991
						CASE 7			RETURN 175.9990
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_BurglaryJob_BridgeStreet
					SWITCH iPed
						CASE 5			RETURN 1.3998
						CASE 6			RETURN 84.5997
						CASE 7			RETURN 88.1980
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_BurglaryJob_NikolaAvenue
					SWITCH iPed
						CASE 5			RETURN 208.7997
						CASE 6			RETURN 208.7997
						CASE 7			RETURN 28.3995
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_BurglaryJob_NorthSheldonAvenue
					SWITCH iPed
						CASE 5			RETURN 44.5990
						CASE 6			RETURN 134.5982
						CASE 7			RETURN 338.3978
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_BurglaryJob_InesenoRoad
					SWITCH iPed
						CASE 5			RETURN 254.9997
						CASE 6			RETURN 74.0248
						CASE 7			RETURN 346.9992
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_GONE_BALLISTIC		
			SWITCH eLocation
				CASE GangOpsLocation_GoneBallistic_ElysianIsland1
					SWITCH iPed
						CASE 0			RETURN 135.9998
						CASE 1			RETURN 142.5990
						CASE 2			RETURN 360.7990
						CASE 3			RETURN 334.3990
						CASE 4			RETURN 212.6000
						CASE 5			RETURN 116.3990
						CASE 6			RETURN 282.7990
						CASE 7			RETURN 175.3990
						CASE 8			RETURN 355.9990
						CASE 9			RETURN 120.7980
						CASE 10			RETURN 126.1980
						CASE 11			RETURN 79.9980
						CASE 12			RETURN 262.3980
						CASE 13			RETURN 54.5978
						CASE 14			RETURN 113.7989
						CASE 15			RETURN 63.5737
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_GoneBallistic_GrandSenoraDesert1
					SWITCH iPed
						CASE 0			RETURN 148.7470
						CASE 1			RETURN 332.3470
						CASE 2			RETURN 307.1470
						CASE 3			RETURN 129.5460
						CASE 4			RETURN 205.7460
						CASE 5			RETURN 114.5459
						CASE 6			RETURN 288.5460
						CASE 7			RETURN 133.1450
						CASE 8			RETURN 175.5450
						CASE 9			RETURN 46.9440
						CASE 10			RETURN 141.1440
						CASE 11			RETURN 0.5440
						CASE 12			RETURN 178.9438
						CASE 13			RETURN 184.9436
						CASE 14			RETURN 298.7997
						CASE 15			RETURN 103.7999
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_GoneBallistic_Grapeseed1
					SWITCH iPed
						CASE 0			RETURN 321.0756
						CASE 1			RETURN 331.2757
						CASE 2			RETURN 124.4759
						CASE 3			RETURN 91.6760
						CASE 4			RETURN 126.8750
						CASE 5			RETURN 230.2750
						CASE 6			RETURN 50.0750
						CASE 7			RETURN 41.0750
						CASE 8			RETURN 17.0750
						CASE 9			RETURN 193.4740
						CASE 10			RETURN 312.2739
						CASE 11			RETURN 164.2740
						CASE 12			RETURN 318.2740
						CASE 13			RETURN 197.4757
						CASE 14			RETURN 278.8737
						CASE 15			RETURN 37.6759
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_GoneBallistic_ElBurroHeights
					SWITCH iPed
						CASE 0			RETURN 159.2717
						CASE 1			RETURN 336.8714
						CASE 2			RETURN 104.0715
						CASE 3			RETURN 87.8714
						CASE 4			RETURN 14.6713
						CASE 5			RETURN 133.6711
						CASE 6			RETURN 313.2708
						CASE 7			RETURN 226.0703
						CASE 8			RETURN 175.4704
						CASE 9			RETURN 13.0700
						CASE 10			RETURN 254.2697
						CASE 11			RETURN 190.0698
						CASE 12			RETURN 8.0697
						CASE 13			RETURN 29.0696
						CASE 14			RETURN 249.6694
						CASE 15			RETURN 104.0715
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_GoneBallistic_Terminal
					SWITCH iPed
						CASE 0			RETURN 357.9995
						CASE 1			RETURN 359.7990
						CASE 2			RETURN 313.5990
						CASE 3			RETURN 143.9990
						CASE 4			RETURN 271.1979
						CASE 5			RETURN 299.9980
						CASE 6			RETURN 126.9980
						CASE 7			RETURN 168.9980
						CASE 8			RETURN 312.9998
						CASE 9			RETURN 303.1984
						CASE 10			RETURN 270.2000
						CASE 11			RETURN 142.3988
						CASE 12			RETURN 21.1990
						CASE 13			RETURN 83.1996
						CASE 14			RETURN 351.3987
						CASE 15			RETURN 287.2000
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_GoneBallistic_LSIA
					SWITCH iPed
						CASE 0			RETURN 72.9165
						CASE 1			RETURN 241.9162
						CASE 2			RETURN 26.7159
						CASE 3			RETURN 11.1159
						CASE 4			RETURN 339.7158
						CASE 5			RETURN 159.5154
						CASE 6			RETURN 156.5153
						CASE 7			RETURN 332.9149
						CASE 8			RETURN 97.9145
						CASE 9			RETURN 277.7140
						CASE 10			RETURN 231.5138
						CASE 11			RETURN 295.7135
						CASE 12			RETURN 156.1131
						CASE 13			RETURN 165.1130
						CASE 14			RETURN 60.1126
						CASE 15			RETURN 156.1131
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_GoneBallistic_MurrietaHeights
					SWITCH iPed
						CASE 0			RETURN 143.3970
						CASE 1			RETURN 313.7966
						CASE 2			RETURN 279.5960
						CASE 3			RETURN 279.5960
						CASE 4			RETURN 25.7959
						CASE 5			RETURN 235.1957
						CASE 6			RETURN 323.1952
						CASE 7			RETURN 353.9948
						CASE 8			RETURN 170.3942
						CASE 9			RETURN 327.3939
						CASE 10			RETURN 146.7935
						CASE 11			RETURN 65.5933
						CASE 12			RETURN 164.7931
						CASE 13			RETURN 182.7928
						CASE 14			RETURN 56.3925
						CASE 15			RETURN 65.5933
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_GoneBallistic_ElysianIsland2
					SWITCH iPed
						CASE 0			RETURN 173.7970
						CASE 1			RETURN 319.7966
						CASE 2			RETURN 266.1968
						CASE 3			RETURN 270.1968
						CASE 4			RETURN 1.5968
						CASE 5			RETURN 143.3960
						CASE 6			RETURN 324.9960
						CASE 7			RETURN 362.9960
						CASE 8			RETURN 290.7954
						CASE 9			RETURN 116.3949
						CASE 10			RETURN 342.5978
						CASE 11			RETURN 225.3950
						CASE 12			RETURN 49.1950
						CASE 13			RETURN 41.5977
						CASE 14			RETURN 166.7945
						CASE 15			RETURN 305.5979
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_GoneBallistic_PaletoBay
					SWITCH iPed
						CASE 0			RETURN 112.2009
						CASE 1			RETURN 133.8000
						CASE 2			RETURN 76.3999
						CASE 3			RETURN 255.5998
						CASE 4			RETURN 276.7988
						CASE 5			RETURN 157.1988
						CASE 6			RETURN 338.3984
						CASE 7			RETURN 17.9980
						CASE 8			RETURN 47.3987
						CASE 9			RETURN 225.1977
						CASE 10			RETURN 9.2006
						CASE 11			RETURN 117.9979
						CASE 12			RETURN 19.9979
						CASE 13			RETURN 237.0007
						CASE 14			RETURN 91.1998
						CASE 15			RETURN 35.4009
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_GoneBallistic_FortZancudo
					SWITCH iPed
						CASE 0			RETURN 119.8485
						CASE 1			RETURN 348.8480
						CASE 2			RETURN 57.2478
						CASE 3			RETURN 347.0478
						CASE 4			RETURN 162.6471
						CASE 5			RETURN 33.0468
						CASE 6			RETURN 217.8466
						CASE 7			RETURN 130.8465
						CASE 8			RETURN 317.2459
						CASE 9			RETURN 290.0458
						CASE 10			RETURN 276.8457
						CASE 11			RETURN 21.8454
						CASE 12			RETURN 211.6453
						CASE 13			RETURN 306.6449
						CASE 14			RETURN 137.8443
						CASE 15			RETURN 211.6453
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK

		CASE GOV_FLIGHT_RECORDER
			SWITCH eLocation
				CASE GangOpsLocation_FlightRecorder_Dam
					SWITCH iPed
						CASE 1		RETURN 211.3641
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlightRecorder_Palomino
					SWITCH iPed
						CASE 1		RETURN 191.0104
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlightRecorder_Terminal
					SWITCH iPed
						CASE 1		RETURN 31.5906
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlightRecorder_PacificBluffs
					SWITCH iPed
						CASE 1		RETURN 2.1082
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlightRecorder_MountGordo
					SWITCH iPed
						CASE 1		RETURN 238.6116
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlightRecorder_AlamoSea1
					SWITCH iPed
						CASE 1		RETURN 274.1464
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlightRecorder_AlamoSea2
					SWITCH iPed
						CASE 1		RETURN 305.5119
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlightRecorder_AlamoSea3
					SWITCH iPed
						CASE 1		RETURN 265.2030
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlightRecorder_PaletoBay
					SWITCH iPed
						CASE 1		RETURN 58.6409
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlightRecorder_FortZancudo
					SWITCH iPed
						CASE 1		RETURN 62.4715
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC MODEL_NAMES GET_GANGOPS_AMBUSH_PED_MODEL(INT iAmbushPedBit, GANGOPS_VARIATION eVariation, GANGOPS_SUBVARIATION eSubvariation, BOOL bAlternateMissionCondition = FALSE)
	UNUSED_PARAMETER(eSubvariation)
	
	SWITCH eVariation
		CASE GOV_DEAD_DROP
			SWITCH iAmbushPedBit
				CASE 0				RETURN s_m_m_fibsec_01			// ePEDBITSET_I_AM_AMBUSH_CAR_0_DRIVER
				CASE 1				RETURN s_m_m_fibsec_01			// ePEDBITSET_I_AM_AMBUSH_CAR_0_PASSENGER
				CASE 2				RETURN s_m_m_fibsec_01			// ePEDBITSET_I_AM_AMBUSH_CAR_0_SECOND_PASSENGER
				CASE 3				RETURN s_m_m_fibsec_01			// ePEDBITSET_I_AM_AMBUSH_CAR_0_THIRD_PASSENGER
				CASE 4				RETURN s_m_m_fibsec_01			// ePEDBITSET_I_AM_AMBUSH_CAR_1_DRIVER
				CASE 5				RETURN s_m_m_fibsec_01			// ePEDBITSET_I_AM_AMBUSH_CAR_1_PASSENGER
				CASE 6				RETURN s_m_m_fibsec_01			// ePEDBITSET_I_AM_AMBUSH_CAR_1_SECOND_PASSENGER
				CASE 7				RETURN s_m_m_fibsec_01			// ePEDBITSET_I_AM_AMBUSH_CAR_1_THIRD_PASSENGER
			ENDSWITCH
		BREAK
		
		CASE GOV_AUTO_SALVAGE	
			IF bAlternateMissionCondition
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("g_m_importexport_01"))
			ENDIF
			RETURN G_M_M_ArmGoon_01
			
		CASE GOV_AQUALUNGS
			RETURN G_M_M_ChiGoon_02 
	
		CASE GOV_FORCED_ENTRY
			RETURN G_M_Y_BallaOrig_01
			
		CASE GOV_BURGLARY_JOB
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_avongoon"))
			
		CASE GOV_GONE_BALLISTIC
			RETURN S_M_Y_BLACKOPS_02	
						
		CASE GOV_DAYLIGHT_ROBBERY
			RETURN S_M_M_HighSec_01
		
		CASE GOV_FLIGHT_RECORDER
			RETURN S_M_M_Pilot_02
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

//FUNC MODEL_NAMES GET_ENEMY_PED_MODEL_FLARE_UP(BOOL bBikerGang)
//
//	IF bBikerGang 
//	
//		RETURN G_M_Y_Lost_02
//	ENDIF
//	
//	RETURN MP_G_M_Pros_01
//ENDFUNC

FUNC MODEL_NAMES GET_GANGOPS_PED_MODEL(INT iPed, GANGOPS_VARIATION eVariation, GANGOPS_SUBVARIATION eSubvariation, GANGOPS_LOCATION eLocation, BOOL bAlternateMissionCondition = FALSE)
//	BOOL bBikerGang = GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
	UNUSED_PARAMETER(eLocation)
	UNUSED_PARAMETER(iPed)
	
	SWITCH eVariation
		CASE GOV_DEAD_DROP
			RETURN s_m_m_fibsec_01
		CASE GOV_DAYLIGHT_ROBBERY
			SWITCH eLocation
				CASE GangOpsLocation_DaylightRobbery_MirrorPark
					SWITCH iPed
						CASE 0			RETURN A_M_Y_Business_02
						CASE 1			RETURN A_F_Y_Business_04
						CASE 2			RETURN A_M_Y_Business_02
						CASE 3			RETURN A_F_Y_Business_04
						CASE 4			RETURN A_M_Y_Business_02
						CASE 5			RETURN A_F_Y_Business_04
						CASE 6			RETURN A_M_Y_Business_02
						CASE 7			RETURN A_F_Y_Business_04
						CASE 8			RETURN A_M_Y_Business_02
						CASE 9			RETURN A_F_Y_Business_04
						CASE 10			RETURN A_M_Y_Business_02
						CASE 11			RETURN A_F_Y_Business_04
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DaylightRobbery_SanAndreas
					SWITCH iPed
						CASE 0			RETURN A_F_Y_Business_04
						CASE 1			RETURN A_F_Y_Business_04
						CASE 2			RETURN A_F_Y_Business_04
						CASE 3			RETURN A_F_Y_Business_04
						CASE 4			RETURN A_F_Y_Business_04
						CASE 5			RETURN A_M_Y_Business_02
						CASE 6			RETURN A_M_Y_Business_02
						CASE 7			RETURN A_M_Y_Business_02
						CASE 8			RETURN A_M_Y_Business_02
						CASE 9			RETURN A_M_Y_Business_02
						CASE 10			RETURN A_M_Y_Business_02
						CASE 11			RETURN A_F_Y_Business_04
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DaylightRobbery_Vespucci
					SWITCH iPed
						CASE 0			RETURN A_F_Y_Business_04
						CASE 1			RETURN A_F_Y_Business_04
						CASE 2			RETURN A_F_Y_Business_04
						CASE 3			RETURN A_F_Y_Business_04
						CASE 4			RETURN A_M_Y_Business_02
						CASE 5			RETURN A_M_Y_Business_02
						CASE 6			RETURN A_M_Y_Business_02
						CASE 7			RETURN A_M_Y_Business_02
						CASE 8			RETURN A_M_Y_Business_02
						CASE 9			RETURN A_F_Y_Business_04
						CASE 10			RETURN A_F_Y_Business_04
						CASE 11			RETURN A_M_Y_Business_02
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DaylightRobbery_LegionSq
					SWITCH iPed
						CASE 0			RETURN A_F_Y_Business_04
						CASE 1			RETURN A_F_Y_Business_04
						CASE 2			RETURN A_F_Y_Business_04
						CASE 3			RETURN A_F_Y_Business_04
						CASE 4			RETURN A_F_Y_Business_04
						CASE 5			RETURN A_F_Y_Business_04
						CASE 6			RETURN A_M_Y_Business_02
						CASE 7			RETURN A_M_Y_Business_02
						CASE 8			RETURN A_M_Y_Business_02
						CASE 9			RETURN A_M_Y_Business_02
						CASE 10			RETURN A_M_Y_Business_02
						CASE 11			RETURN A_M_Y_Business_02
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DaylightRobbery_DelPerro
					SWITCH iPed
						CASE 0			RETURN A_M_Y_Business_02
						CASE 1			RETURN A_M_Y_Business_02
						CASE 2			RETURN A_M_Y_Business_02
						CASE 3			RETURN A_M_Y_Business_02
						CASE 4			RETURN A_M_Y_Business_02
						CASE 5			RETURN A_M_Y_Business_02
						CASE 6			RETURN A_F_Y_Business_04
						CASE 7			RETURN A_F_Y_Business_04
						CASE 8			RETURN A_F_Y_Business_04
						CASE 9			RETURN A_F_Y_Business_04
						CASE 10			RETURN A_F_Y_Business_04
						CASE 11			RETURN A_F_Y_Business_04
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DaylightRobbery_Rockford
					SWITCH iPed
						CASE 0			RETURN A_F_Y_Business_04
						CASE 1			RETURN A_F_Y_Business_04
						CASE 2			RETURN A_F_Y_Business_04
						CASE 3			RETURN A_F_Y_Business_04
						CASE 4			RETURN A_F_Y_Business_04
						CASE 5			RETURN A_F_Y_Business_04
						CASE 6			RETURN A_M_Y_Business_02
						CASE 7			RETURN A_M_Y_Business_02
						CASE 8			RETURN A_M_Y_Business_02
						CASE 9			RETURN A_M_Y_Business_02
						CASE 10			RETURN A_M_Y_Business_02
						CASE 11			RETURN A_M_Y_Business_02
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DaylightRobbery_WestVinewood
					SWITCH iPed
						CASE 0			RETURN A_F_Y_Business_04
						CASE 1			RETURN A_F_Y_Business_04
						CASE 2			RETURN A_F_Y_Business_04
						CASE 3			RETURN A_F_Y_Business_04
						CASE 4			RETURN A_F_Y_Business_04
						CASE 5			RETURN A_M_Y_Business_02
						CASE 6			RETURN A_M_Y_Business_02
						CASE 7			RETURN A_M_Y_Business_02
						CASE 8			RETURN A_M_Y_Business_02
						CASE 9			RETURN A_M_Y_Business_02
						CASE 10			RETURN A_M_Y_Business_02
						CASE 11			RETURN A_F_Y_Business_04
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DaylightRobbery_Burton
					SWITCH iPed
						CASE 0			RETURN A_F_Y_Business_04
						CASE 1			RETURN A_F_Y_Business_04
						CASE 2			RETURN A_F_Y_Business_04
						CASE 3			RETURN A_F_Y_Business_04
						CASE 4			RETURN A_F_Y_Business_04
						CASE 5			RETURN A_F_Y_Business_04
						CASE 6			RETURN A_M_Y_Business_02
						CASE 7			RETURN A_M_Y_Business_02
						CASE 8			RETURN A_M_Y_Business_02
						CASE 9			RETURN A_M_Y_Business_02
						CASE 10			RETURN A_M_Y_Business_02
						CASE 11			RETURN A_M_Y_Business_02
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DaylightRobbery_DelPerroPlaza
					SWITCH iPed
						CASE 0			RETURN A_M_Y_Business_02
						CASE 1			RETURN A_M_Y_Business_02
						CASE 2			RETURN A_M_Y_Business_02
						CASE 3			RETURN A_M_Y_Business_02
						CASE 4			RETURN A_M_Y_Business_02
						CASE 5			RETURN A_M_Y_Business_02
						CASE 6			RETURN A_F_Y_Business_04
						CASE 7			RETURN A_F_Y_Business_04
						CASE 8			RETURN A_F_Y_Business_04
						CASE 9			RETURN A_F_Y_Business_04
						CASE 10			RETURN A_F_Y_Business_04
						CASE 11			RETURN A_F_Y_Business_04
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_DaylightRobbery_Pillbox
					SWITCH iPed
						CASE 0			RETURN A_F_Y_Business_04
						CASE 1			RETURN A_F_Y_Business_04
						CASE 2			RETURN A_F_Y_Business_04
						CASE 3			RETURN A_F_Y_Business_04
						CASE 4			RETURN A_M_Y_Business_02
						CASE 5			RETURN A_M_Y_Business_02
						CASE 6			RETURN A_M_Y_Business_02
						CASE 7			RETURN A_M_Y_Business_02
						CASE 8			RETURN A_F_Y_Business_04
						CASE 9			RETURN A_M_Y_Business_02
						CASE 10			RETURN A_M_Y_Business_02
						CASE 11			RETURN A_F_Y_Business_04
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE GOV_FLARE_UP			
			SWITCH eLocation
				CASE GangOpsLocation_FlareUp_Chamberlain_Hills
					SWITCH iPed
						CASE 0			RETURN S_M_Y_Swat_01
						CASE 1			RETURN S_M_Y_Swat_01
						CASE 2			RETURN S_M_Y_Swat_01
						CASE 3			RETURN S_M_Y_Swat_01
						CASE 4			RETURN S_M_Y_Cop_01
						CASE 5			RETURN S_M_Y_Cop_01
						CASE 6			RETURN S_M_Y_Cop_01
						CASE 7			RETURN S_M_Y_Cop_01
						CASE 8			RETURN MP_G_M_Pros_01
						CASE 9			RETURN MP_G_M_Pros_01
						CASE 10			RETURN MP_G_M_Pros_01
						CASE 11			RETURN MP_G_M_Pros_01
						CASE 12			RETURN MP_G_M_Pros_01
						CASE 13			RETURN MP_G_M_Pros_01
						CASE 14			RETURN MP_G_M_Pros_01
						CASE 15			RETURN MP_G_M_Pros_01
						CASE 16			RETURN S_M_Y_Cop_01
						CASE 17			RETURN S_M_Y_Cop_01
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Morningwood
					SWITCH iPed
						CASE 0			RETURN MP_G_M_Pros_01
						CASE 1			RETURN MP_G_M_Pros_01
						CASE 2			RETURN MP_G_M_Pros_01
						CASE 3			RETURN MP_G_M_Pros_01
						CASE 4			RETURN MP_G_M_Pros_01
						CASE 5			RETURN MP_G_M_Pros_01
						CASE 6			RETURN MP_G_M_Pros_01
						CASE 7			RETURN MP_G_M_Pros_01
						CASE 8			RETURN S_M_Y_Cop_01
						CASE 9			RETURN S_M_Y_Cop_01
						CASE 10			RETURN S_M_Y_Cop_01
						CASE 11			RETURN S_M_Y_Cop_01
						CASE 12			RETURN S_M_Y_Cop_01
						CASE 13			RETURN S_M_Y_Cop_01
						CASE 14			RETURN S_M_Y_Swat_01
						CASE 15			RETURN S_M_Y_Swat_01
						CASE 16			RETURN S_M_Y_Swat_01
						CASE 17			RETURN S_M_Y_Swat_01
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_La_Puerta
					SWITCH iPed
						CASE 0			RETURN S_M_Y_Cop_01
						CASE 1			RETURN S_M_Y_Cop_01
						CASE 2			RETURN S_M_Y_Cop_01
						CASE 3			RETURN S_M_Y_Swat_01
						CASE 4			RETURN S_M_Y_Cop_01
						CASE 5			RETURN S_M_Y_Swat_01
						CASE 6			RETURN S_M_Y_Swat_01
						CASE 7			RETURN S_M_Y_Swat_01
						CASE 8			RETURN MP_G_M_Pros_01
						CASE 9			RETURN MP_G_M_Pros_01
						CASE 10			RETURN MP_G_M_Pros_01
						CASE 11			RETURN MP_G_M_Pros_01
						CASE 12			RETURN MP_G_M_Pros_01
						CASE 13			RETURN MP_G_M_Pros_01
						CASE 14			RETURN MP_G_M_Pros_01
						CASE 15			RETURN MP_G_M_Pros_01
						CASE 16			RETURN S_M_Y_Cop_01
						CASE 17			RETURN S_M_Y_Cop_01
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_El_burro
					SWITCH iPed
						CASE 0			RETURN S_M_Y_Swat_01
						CASE 1			RETURN S_M_Y_Swat_01
						CASE 2			RETURN S_M_Y_Swat_01
						CASE 3			RETURN S_M_Y_Cop_01
						CASE 4			RETURN S_M_Y_Swat_01
						CASE 5			RETURN S_M_Y_Cop_01
						CASE 6			RETURN S_M_Y_Cop_01
						CASE 7			RETURN MP_G_M_Pros_01
						CASE 8			RETURN MP_G_M_Pros_01
						CASE 9			RETURN MP_G_M_Pros_01
						CASE 10			RETURN MP_G_M_Pros_01
						CASE 11			RETURN MP_G_M_Pros_01
						CASE 12			RETURN MP_G_M_Pros_01
						CASE 13			RETURN MP_G_M_Pros_01
						CASE 14			RETURN MP_G_M_Pros_01
						CASE 15			RETURN S_M_Y_Cop_01
						CASE 16			RETURN S_M_Y_Cop_01
						CASE 17			RETURN S_M_Y_Cop_01
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Davis
					SWITCH iPed
						CASE 0			RETURN S_M_Y_Cop_01
						CASE 1			RETURN S_M_Y_Cop_01
						CASE 2			RETURN S_M_Y_Cop_01
						CASE 3			RETURN S_M_Y_Cop_01
						CASE 4			RETURN S_M_Y_Swat_01
						CASE 5			RETURN S_M_Y_Swat_01
						CASE 6			RETURN S_M_Y_Swat_01
						CASE 7			RETURN S_M_Y_Swat_01
						CASE 8			RETURN MP_G_M_Pros_01
						CASE 9			RETURN MP_G_M_Pros_01
						CASE 10			RETURN MP_G_M_Pros_01
						CASE 11			RETURN MP_G_M_Pros_01
						CASE 12			RETURN MP_G_M_Pros_01
						CASE 13			RETURN MP_G_M_Pros_01
						CASE 14			RETURN MP_G_M_Pros_01
						CASE 15			RETURN MP_G_M_Pros_01
						CASE 16			RETURN S_M_Y_Cop_01
						CASE 17			RETURN S_M_Y_Cop_01
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Sandy
					SWITCH iPed
						CASE 0			RETURN S_M_Y_Swat_01
						CASE 1			RETURN S_M_Y_Swat_01
						CASE 2			RETURN S_M_Y_Swat_01
						CASE 3			RETURN S_M_Y_Swat_01
						CASE 4			RETURN S_M_Y_Cop_01
						CASE 5			RETURN S_M_Y_Cop_01
						CASE 6			RETURN S_M_Y_Cop_01
						CASE 7			RETURN S_M_Y_Cop_01
						CASE 8			RETURN MP_G_M_Pros_01
						CASE 9			RETURN MP_G_M_Pros_01
						CASE 10			RETURN MP_G_M_Pros_01
						CASE 11			RETURN MP_G_M_Pros_01
						CASE 12			RETURN MP_G_M_Pros_01
						CASE 13			RETURN MP_G_M_Pros_01
						CASE 14			RETURN MP_G_M_Pros_01
						CASE 15			RETURN MP_G_M_Pros_01
						CASE 16			RETURN S_M_Y_Cop_01
						CASE 17			RETURN S_M_Y_Cop_01
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Paleto
					SWITCH iPed
						CASE 0			RETURN S_M_Y_Cop_01
						CASE 1			RETURN S_M_Y_Cop_01
						CASE 2			RETURN S_M_Y_Cop_01
						CASE 3			RETURN S_M_Y_Cop_01
						CASE 4			RETURN S_M_Y_Swat_01
						CASE 5			RETURN S_M_Y_Swat_01
						CASE 6			RETURN S_M_Y_Swat_01
						CASE 7			RETURN S_M_Y_Swat_01
						CASE 8			RETURN MP_G_M_Pros_01
						CASE 9			RETURN MP_G_M_Pros_01
						CASE 10			RETURN MP_G_M_Pros_01
						CASE 11			RETURN MP_G_M_Pros_01
						CASE 12			RETURN MP_G_M_Pros_01
						CASE 13			RETURN MP_G_M_Pros_01
						CASE 14			RETURN MP_G_M_Pros_01
						CASE 15			RETURN MP_G_M_Pros_01
						CASE 16			RETURN S_M_Y_Cop_01
						CASE 17			RETURN S_M_Y_Cop_01
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Capital
					SWITCH iPed
						CASE 0			RETURN S_M_Y_Swat_01
						CASE 1			RETURN S_M_Y_Swat_01
						CASE 2			RETURN S_M_Y_Swat_01
						CASE 3			RETURN S_M_Y_Swat_01
						CASE 4			RETURN S_M_Y_Cop_01
						CASE 5			RETURN S_M_Y_Cop_01
						CASE 6			RETURN S_M_Y_Cop_01
						CASE 7			RETURN S_M_Y_Cop_01
						CASE 8			RETURN MP_G_M_Pros_01
						CASE 9			RETURN MP_G_M_Pros_01
						CASE 10			RETURN MP_G_M_Pros_01
						CASE 11			RETURN MP_G_M_Pros_01
						CASE 12			RETURN MP_G_M_Pros_01
						CASE 13			RETURN MP_G_M_Pros_01
						CASE 14			RETURN MP_G_M_Pros_01
						CASE 15			RETURN MP_G_M_Pros_01
						CASE 16			RETURN S_M_Y_Cop_01
						CASE 17			RETURN S_M_Y_Cop_01
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Clinton
					SWITCH iPed
						CASE 0			RETURN S_M_Y_Swat_01
						CASE 1			RETURN S_M_Y_Swat_01
						CASE 2			RETURN S_M_Y_Swat_01
						CASE 3			RETURN S_M_Y_Swat_01
						CASE 4			RETURN S_M_Y_Cop_01
						CASE 5			RETURN S_M_Y_Cop_01
						CASE 6			RETURN S_M_Y_Cop_01
						CASE 7			RETURN S_M_Y_Cop_01
						CASE 8			RETURN MP_G_M_Pros_01
						CASE 9			RETURN MP_G_M_Pros_01
						CASE 10			RETURN MP_G_M_Pros_01
						CASE 11			RETURN MP_G_M_Pros_01
						CASE 12			RETURN MP_G_M_Pros_01
						CASE 13			RETURN MP_G_M_Pros_01
						CASE 14			RETURN MP_G_M_Pros_01
						CASE 15			RETURN MP_G_M_Pros_01
						CASE 16			RETURN S_M_Y_Cop_01
						CASE 17			RETURN S_M_Y_Cop_01
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_FlareUp_Gas_Station_Richman
					SWITCH iPed
						CASE 0			RETURN S_M_Y_Cop_01
						CASE 1			RETURN S_M_Y_Cop_01
						CASE 2			RETURN S_M_Y_Cop_01
						CASE 3			RETURN S_M_Y_Cop_01
						CASE 4			RETURN S_M_Y_Cop_01
						CASE 5			RETURN S_M_Y_Cop_01
						CASE 6			RETURN S_M_Y_Swat_01
						CASE 7			RETURN S_M_Y_Swat_01
						CASE 8			RETURN S_M_Y_Swat_01
						CASE 9			RETURN S_M_Y_Swat_01
						CASE 10			RETURN mp_g_m_pros_01
						CASE 11			RETURN mp_g_m_pros_01
						CASE 12			RETURN mp_g_m_pros_01
						CASE 13			RETURN mp_g_m_pros_01
						CASE 14			RETURN mp_g_m_pros_01
						CASE 15			RETURN mp_g_m_pros_01
						CASE 16			RETURN mp_g_m_pros_01
						CASE 17			RETURN mp_g_m_pros_01
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE GOV_FORCED_ENTRY						
//			SWITCH eLocation
//				CASE GangOpsLocation_ForcedEntry_Location_Fudge						
//				CASE GangOpsLocation_ForcedEntry_Location_Magellan
//				CASE GangOpsLocation_ForcedEntry_Location_Forum
//				CASE GangOpsLocation_ForcedEntry_Location_Roy_Lowenstein
//				CASE GangOpsLocation_ForcedEntry_Location_Eastbourne
//				CASE GangOpsLocation_ForcedEntry_Location_V_Magellan
//				CASE GangOpsLocation_ForcedEntry_Location_Alta
//				CASE GangOpsLocation_ForcedEntry_Location_Forum_Drive
//				CASE GangOpsLocation_ForcedEntry_Location_Spanish
//				CASE GangOpsLocation_ForcedEntry_Location_Tahitian
					SWITCH iPed
						CASE 0			RETURN G_M_Y_BallaSout_01
						CASE 1			RETURN G_M_Y_BallaSout_01
						CASE 2			RETURN G_M_Y_BallaSout_01
						CASE 3			RETURN G_M_Y_BallaSout_01
						CASE 4			RETURN G_M_Y_BallaSout_01
						CASE 5			RETURN G_M_Y_BallaSout_01
						CASE 6			RETURN G_F_Y_ballas_01 
						CASE 7			RETURN G_M_Y_BallaOrig_01 
						CASE 8			RETURN G_M_Y_BallaOrig_01 
						CASE 9			RETURN G_M_Y_BallaOrig_01 
						CASE 10			RETURN G_M_Y_BallaOrig_01 
						CASE 11			RETURN G_M_Y_BallaOrig_01 
						CASE 12			RETURN G_M_Y_BallaOrig_01 
						CASE 13			RETURN G_M_Y_BallaOrig_01 
					ENDSWITCH
				BREAK
//			ENDSWITCH
		BREAK
			
		CASE GOV_AUTO_SALVAGE
			IF bAlternateMissionCondition
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("g_m_importexport_01")) 
			ENDIF
			RETURN g_m_m_armgoon_01
			
		CASE GOV_CAR_COLLECTOR
			SWITCH iPed			
				CASE 20			RETURN A_M_M_Genfat_01
				DEFAULT 		RETURN S_M_M_HighSec_01
			ENDSWITCH
		BREAK
		CASE GOV_MOB_MENTALITY			
			SWITCH eSubvariation
				CASE GOS_MM_MORNINGWOOD
					SWITCH iPed
						CASE 0			RETURN S_M_M_HighSec_01
						CASE 1			RETURN S_M_M_HighSec_01
						CASE 2			RETURN S_M_M_HighSec_01
						CASE 3			RETURN S_M_M_HighSec_01
						CASE 4			RETURN s_m_m_movprem_01
						CASE 5			RETURN s_f_y_movprem_01
						CASE 6			RETURN s_m_m_movprem_01
						CASE 7			RETURN s_m_m_movprem_01
						CASE 8			RETURN s_m_m_movprem_01
						CASE 9			RETURN s_m_m_movprem_01
						CASE 10			RETURN s_f_y_movprem_01
						CASE 11			RETURN s_f_y_movprem_01
						CASE 12			RETURN s_m_m_movprem_01
						CASE 13			RETURN s_m_m_movprem_01
						CASE 14			RETURN A_M_M_Paparazzi_01
						CASE 15			RETURN A_M_M_Paparazzi_01
						CASE 16			RETURN MP_S_M_Armoured_01
						CASE 17			RETURN MP_S_M_Armoured_01
						CASE 18			RETURN MP_S_M_Armoured_01
						CASE 19			RETURN MP_S_M_Armoured_01
					ENDSWITCH
				BREAK
				CASE GOS_MM_GRAPESEED
					SWITCH iPed
						CASE 0			RETURN A_M_M_Hillbilly_01
						CASE 1			RETURN A_M_M_Hillbilly_01
						CASE 2			RETURN A_M_M_Hillbilly_01
						CASE 3			RETURN A_M_M_Hillbilly_01
						CASE 4			RETURN A_F_Y_Hippie_01
						CASE 5			RETURN A_F_Y_Hippie_01
						CASE 6			RETURN A_F_Y_Hippie_01
						CASE 7			RETURN A_F_Y_Hippie_01
						CASE 8			RETURN A_M_Y_Hippy_01
						CASE 9			RETURN A_M_Y_Hippy_01
						CASE 10			RETURN A_M_Y_Hippy_01
						CASE 11			RETURN A_F_Y_Hippie_01
						CASE 12			RETURN A_M_Y_Hippy_01
						CASE 13			RETURN A_M_Y_Hippy_01
						CASE 14			RETURN A_F_Y_Hippie_01
						CASE 15			RETURN A_M_Y_Hippy_01
						CASE 16			RETURN MP_S_M_Armoured_01
						CASE 17			RETURN MP_S_M_Armoured_01
						CASE 18			RETURN MP_S_M_Armoured_01
						CASE 19			RETURN MP_S_M_Armoured_01
					ENDSWITCH
				BREAK
				CASE GOS_MM_ALTA
					SWITCH iPed
						CASE 0			RETURN S_M_M_Security_01
						CASE 1			RETURN S_M_M_Security_01
						CASE 2			RETURN S_M_M_Security_01
						CASE 3			RETURN S_M_M_Security_01
						CASE 4			RETURN A_M_Y_Hipster_01
						CASE 5			RETURN A_M_Y_Hipster_01
						CASE 6			RETURN A_M_Y_Hipster_01
						CASE 7			RETURN A_M_Y_Hipster_01
						CASE 8			RETURN A_F_Y_Hipster_02
						CASE 9			RETURN A_F_Y_Hipster_02
						CASE 10			RETURN A_F_Y_Hipster_02
						CASE 11			RETURN A_F_Y_Hipster_02
						CASE 12			RETURN A_M_Y_Hipster_01
						CASE 13			RETURN A_M_Y_Hipster_01
						CASE 14			RETURN A_M_Y_Hipster_01
						CASE 15			RETURN A_M_Y_Hipster_01
						CASE 16			RETURN MP_S_M_Armoured_01
						CASE 17			RETURN MP_S_M_Armoured_01
						CASE 18			RETURN MP_S_M_Armoured_01
						CASE 19			RETURN MP_S_M_Armoured_01
					ENDSWITCH
				BREAK
				CASE GOS_MM_MISSION_ROW
					SWITCH iPed
						CASE 0			RETURN S_M_M_Security_01
						CASE 1			RETURN S_M_M_Security_01
						CASE 2			RETURN S_M_M_Security_01
						CASE 3			RETURN S_M_M_Security_01
						CASE 4			RETURN A_M_Y_MotoX_01
						CASE 5			RETURN A_M_Y_MotoX_01
						CASE 6			RETURN A_M_Y_MotoX_01
						CASE 7			RETURN A_M_Y_MotoX_01
						CASE 8			RETURN A_M_Y_MotoX_01
						CASE 9			RETURN A_M_Y_MotoX_01
						CASE 10			RETURN A_M_Y_MotoX_01
						CASE 11			RETURN A_M_Y_MotoX_01
						CASE 12			RETURN A_M_Y_MotoX_01
						CASE 13			RETURN A_M_Y_MotoX_01
						CASE 14			RETURN A_M_Y_MotoX_01
						CASE 15			RETURN A_M_Y_MotoX_01
						CASE 16			RETURN MP_S_M_Armoured_01
						CASE 17			RETURN MP_S_M_Armoured_01
						CASE 18			RETURN MP_S_M_Armoured_01
						CASE 19			RETURN MP_S_M_Armoured_01
					ENDSWITCH
				BREAK
				CASE GOS_MM_HIPPY_CAMP
					SWITCH iPed
						CASE 0			RETURN A_M_Y_ACULT_02
						CASE 1			RETURN A_M_Y_ACULT_02
						CASE 2			RETURN A_M_Y_ACULT_02
						CASE 3			RETURN A_M_Y_ACULT_02
						CASE 4			RETURN A_M_Y_Hippy_01
						CASE 5			RETURN A_M_Y_Hippy_01
						CASE 6			RETURN A_F_Y_Hippie_01
						CASE 7			RETURN A_F_Y_Hippie_01
						CASE 8			RETURN A_M_Y_Hippy_01
						CASE 9			RETURN A_M_Y_Hippy_01
						CASE 10			RETURN A_F_Y_Hippie_01
						CASE 11			RETURN A_M_Y_Hippy_01
						CASE 12			RETURN A_F_Y_Hippie_01
						CASE 13			RETURN A_F_Y_Hippie_01
						CASE 14			RETURN A_M_Y_Hippy_01
						CASE 15			RETURN A_M_Y_Hippy_01
						CASE 16			RETURN MP_S_M_Armoured_01
						CASE 17			RETURN MP_S_M_Armoured_01
						CASE 18			RETURN MP_S_M_Armoured_01
						CASE 19			RETURN MP_S_M_Armoured_01
					ENDSWITCH
				BREAK
				CASE GOS_MM_SAN_ANDREAS
					SWITCH iPed
						CASE 0			RETURN S_M_M_Security_01
						CASE 1			RETURN S_M_M_Security_01
						CASE 2			RETURN S_M_M_Security_01
						CASE 3			RETURN S_M_M_Security_01
						CASE 4			RETURN A_M_Y_BEACH_03
						CASE 5			RETURN A_M_Y_BEACH_03
						CASE 6			RETURN A_M_Y_BEACH_03
						CASE 7			RETURN A_M_Y_BEACH_03
						CASE 8			RETURN A_M_Y_BEACH_03
						CASE 9			RETURN A_M_Y_BEACH_03
						CASE 10			RETURN A_F_Y_BEACH_01
						CASE 11			RETURN A_F_Y_BEACH_01
						CASE 12			RETURN A_F_Y_BEACH_01
						CASE 13			RETURN A_F_Y_BEACH_01
						CASE 14			RETURN A_M_Y_BEACH_03
						CASE 15			RETURN A_M_Y_BEACH_03
						CASE 16			RETURN MP_S_M_Armoured_01
						CASE 17			RETURN MP_S_M_Armoured_01
						CASE 18			RETURN MP_S_M_Armoured_01
						CASE 19			RETURN MP_S_M_Armoured_01
					ENDSWITCH
				BREAK
				CASE GOS_MM_MIRROR_PARK
					SWITCH iPed
						CASE 0			RETURN S_M_M_Security_01
						CASE 1			RETURN S_M_M_Security_01
						CASE 2			RETURN S_M_M_Security_01
						CASE 3			RETURN S_M_M_Security_01
						CASE 4			RETURN S_M_Y_Construct_02
						CASE 5			RETURN S_M_Y_Construct_02
						CASE 6			RETURN S_M_Y_Construct_02
						CASE 7			RETURN S_M_Y_Construct_02
						CASE 8			RETURN S_M_Y_Construct_02
						CASE 9			RETURN S_M_Y_Construct_02
						CASE 10			RETURN S_M_Y_Construct_02
						CASE 11			RETURN S_M_Y_Construct_02
						CASE 12			RETURN S_M_Y_Construct_02
						CASE 13			RETURN S_M_Y_Construct_02
						CASE 14			RETURN S_M_Y_Construct_02
						CASE 15			RETURN S_M_Y_Construct_02
						CASE 16			RETURN MP_S_M_Armoured_01
						CASE 17			RETURN MP_S_M_Armoured_01
						CASE 18			RETURN MP_S_M_Armoured_01
						CASE 19			RETURN MP_S_M_Armoured_01
					ENDSWITCH
				BREAK
				CASE GOS_MM_STRAWBERRY
					SWITCH iPed
						CASE 0			RETURN S_M_M_Security_01
						CASE 1			RETURN S_M_M_Security_01
						CASE 2			RETURN S_M_M_Security_01
						CASE 3			RETURN S_M_M_Security_01
						CASE 4			RETURN A_M_M_EastSA_02
						CASE 5			RETURN A_M_M_EastSA_02
						CASE 6			RETURN A_M_M_EastSA_02
						CASE 7			RETURN A_M_M_EastSA_02
						CASE 8			RETURN A_M_M_EastSA_02
						CASE 9			RETURN A_M_M_EastSA_02
						CASE 10			RETURN A_M_M_EastSA_02
						CASE 11			RETURN A_M_M_EastSA_02
						CASE 12			RETURN A_M_M_EastSA_02
						CASE 13			RETURN A_M_M_EastSA_01
						CASE 14			RETURN A_M_M_EastSA_01
						CASE 15			RETURN A_M_M_EastSA_01
						CASE 16			RETURN MP_S_M_Armoured_01
						CASE 17			RETURN MP_S_M_Armoured_01
						CASE 18			RETURN MP_S_M_Armoured_01
						CASE 19			RETURN MP_S_M_Armoured_01
					ENDSWITCH
				BREAK
				CASE GOS_MM_MIRROR_PARK_TAVERN
					SWITCH iPed
						CASE 0			RETURN S_M_M_Bouncer_01
						CASE 1			RETURN S_M_M_Bouncer_01
						CASE 2			RETURN S_M_M_Bouncer_01
						CASE 3			RETURN S_M_M_Bouncer_01
						CASE 4			RETURN A_M_M_Hillbilly_02
						CASE 5			RETURN A_M_M_Hillbilly_02
						CASE 6			RETURN A_M_M_Hillbilly_02
						CASE 7			RETURN A_M_M_Hillbilly_02
						CASE 8			RETURN A_M_M_Hillbilly_02
						CASE 9			RETURN A_M_M_Hillbilly_02
						CASE 10			RETURN A_M_M_Hillbilly_02
						CASE 11			RETURN A_M_M_Hillbilly_02
						CASE 12			RETURN A_M_M_Hillbilly_02
						CASE 13			RETURN A_M_M_Hillbilly_02
						CASE 14			RETURN A_M_M_Hillbilly_02
						CASE 15			RETURN A_M_M_Hillbilly_02
						CASE 16			RETURN MP_S_M_Armoured_01
						CASE 17			RETURN MP_S_M_Armoured_01
						CASE 18			RETURN MP_S_M_Armoured_01
						CASE 19			RETURN MP_S_M_Armoured_01
					ENDSWITCH
				BREAK
				CASE GOS_MM_ULSA
					SWITCH iPed
						CASE 0			RETURN S_M_M_Security_01
						CASE 1			RETURN S_M_M_Security_01
						CASE 2			RETURN S_M_M_Security_01
						CASE 3			RETURN S_M_M_Security_01
						CASE 4			RETURN A_F_Y_BevHills_02
						CASE 5			RETURN A_F_Y_BevHills_02
						CASE 6			RETURN A_F_Y_BevHills_02
						CASE 7			RETURN A_F_Y_BevHills_02
						CASE 8			RETURN A_F_Y_BevHills_02
						CASE 9			RETURN A_M_Y_BevHills_02
						CASE 10			RETURN A_M_Y_BevHills_02
						CASE 11			RETURN A_M_Y_BevHills_02
						CASE 12			RETURN A_M_Y_BevHills_02
						CASE 13			RETURN A_M_Y_BevHills_02
						CASE 14			RETURN A_M_Y_BevHills_02
						CASE 15			RETURN A_M_Y_BevHills_02
						CASE 16			RETURN MP_S_M_Armoured_01
						CASE 17			RETURN MP_S_M_Armoured_01
						CASE 18			RETURN MP_S_M_Armoured_01
						CASE 19			RETURN MP_S_M_Armoured_01
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_PARAMEDIC
			SWITCH eLocation
				CASE GangOpsLocation_Paramedic_Central_LS_Medical_Center
					SWITCH iPed
						CASE 0			RETURN S_M_M_Paramedic_01
						CASE 1			RETURN S_M_M_Paramedic_01
						CASE 2			RETURN S_M_M_Doctor_01
						CASE 3			RETURN S_M_M_Security_01
						CASE 4			RETURN S_M_M_Security_01
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Pillbox_Hill_Medical_Center
					SWITCH iPed
						CASE 0			RETURN S_M_M_Paramedic_01
						CASE 1			RETURN S_M_M_Paramedic_01
						CASE 2			RETURN S_M_M_Security_01
						CASE 3			RETURN S_M_M_Security_01
						CASE 4			RETURN S_M_M_Doctor_01
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_ST_Fiacre_Hospital
					SWITCH iPed
						CASE 0			RETURN S_M_M_Paramedic_01
						CASE 1			RETURN S_M_M_Paramedic_01
						CASE 2			RETURN S_M_M_Doctor_01
						CASE 3			RETURN S_M_M_Security_01
						CASE 4			RETURN S_M_M_Security_01
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Sandy_Shores_Medical_Center
					SWITCH iPed
						CASE 0			RETURN S_M_M_Paramedic_01
						CASE 1			RETURN S_M_M_Paramedic_01
						CASE 2			RETURN S_M_M_Doctor_01
						CASE 3			RETURN S_M_M_Security_01
						CASE 4			RETURN S_M_M_Security_01
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Paleto_Bay_Care_Center
					SWITCH iPed
						CASE 0			RETURN S_M_M_Paramedic_01
						CASE 1			RETURN S_M_M_Paramedic_01
						CASE 2			RETURN S_M_M_Doctor_01
						CASE 3			RETURN S_M_M_Security_01
						CASE 4			RETURN S_M_M_Security_01
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Del_Perro_Freeway
					SWITCH iPed
						CASE 0			RETURN S_M_M_Paramedic_01
						CASE 1			RETURN S_M_M_Paramedic_01
						CASE 2			RETURN A_F_Y_Hipster_02
						CASE 3			RETURN S_F_Y_Cop_01
						CASE 4			RETURN S_M_Y_COP_01
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Mount_Gordo
					SWITCH iPed
						CASE 0			RETURN A_M_Y_Hippy_01
						CASE 1			RETURN S_M_M_Paramedic_01
						CASE 2			RETURN S_M_M_Paramedic_01
						CASE 3			RETURN S_M_Y_Sheriff_01
						CASE 4			RETURN S_F_Y_Sheriff_01
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Great_Chaparral
					SWITCH iPed
						CASE 0			RETURN S_M_M_Paramedic_01
						CASE 1			RETURN S_M_M_Paramedic_01
						CASE 2			RETURN A_F_Y_Business_04
						CASE 3			RETURN S_F_Y_Sheriff_01
						CASE 4			RETURN S_M_Y_Sheriff_01
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Cassidy_Creek
					SWITCH iPed
						CASE 0			RETURN A_M_M_Hillbilly_02
						CASE 1			RETURN S_M_M_Paramedic_01
						CASE 2			RETURN S_M_M_Paramedic_01
						CASE 3			RETURN S_F_Y_Sheriff_01
						CASE 4			RETURN S_M_Y_Sheriff_01
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Paramedic_Power_Station
					SWITCH iPed
						CASE 0			RETURN S_M_M_Paramedic_01
						CASE 1			RETURN S_M_M_Paramedic_01
						CASE 2			RETURN A_M_Y_BusiCas_01
						CASE 3			RETURN S_M_Y_Sheriff_01
						CASE 4			RETURN S_F_Y_Sheriff_01
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_AQUALUNGS
			SWITCH eLocation
				CASE GangOpsLocation_Aqualungs_Elysian_Island_1
					SWITCH iPed
						CASE 4			RETURN G_M_M_ArmBoss_01
						CASE 5			RETURN G_M_M_ArmBoss_01
						CASE 11			RETURN g_m_m_armgoon_01
						CASE 12			RETURN g_m_m_armgoon_01
						CASE 13			RETURN g_m_m_armgoon_01
						CASE 14			RETURN g_m_m_armgoon_01
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Paleto_Bay
					SWITCH iPed
						CASE 4			RETURN G_M_M_ArmBoss_01
						CASE 5			RETURN G_M_M_ArmBoss_01
						CASE 11			RETURN g_m_m_armgoon_01
						CASE 12			RETURN g_m_m_armgoon_01
						CASE 13			RETURN g_m_m_armgoon_01
						CASE 14			RETURN g_m_m_armgoon_01
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Elysian_Island_2
					SWITCH iPed
						CASE 5			RETURN G_M_M_ArmBoss_01
						CASE 6			RETURN G_M_M_ArmBoss_01
						CASE 11			RETURN g_m_m_armgoon_01
						CASE 12			RETURN g_m_m_armgoon_01
						CASE 13			RETURN g_m_m_armgoon_01
						CASE 14			RETURN g_m_m_armgoon_01
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Palomino_Highlands
					SWITCH iPed
						CASE 2			RETURN G_M_M_ArmBoss_01
						CASE 3			RETURN G_M_M_ArmBoss_01
						CASE 11			RETURN g_m_m_armgoon_01
						CASE 12			RETURN g_m_m_armgoon_01
						CASE 13			RETURN g_m_m_armgoon_01
						CASE 14			RETURN g_m_m_armgoon_01
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Chumash
					SWITCH iPed
						CASE 1			RETURN G_M_M_ArmBoss_01
						CASE 2			RETURN G_M_M_ArmBoss_01
						CASE 11			RETURN g_m_m_armgoon_01
						CASE 12			RETURN g_m_m_armgoon_01
						CASE 13			RETURN g_m_m_armgoon_01
						CASE 14			RETURN g_m_m_armgoon_01
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_San_Chianksi_Mountain_Range
					SWITCH iPed
						CASE 4			RETURN G_M_M_ArmBoss_01
						CASE 5			RETURN G_M_M_ArmBoss_01
						CASE 11			RETURN g_m_m_armgoon_01
						CASE 12			RETURN g_m_m_armgoon_01
						CASE 13			RETURN g_m_m_armgoon_01
						CASE 14			RETURN g_m_m_armgoon_01
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_PaletoForest
					SWITCH iPed
						CASE 5			RETURN G_M_M_ArmBoss_01
						CASE 6			RETURN G_M_M_ArmBoss_01
						CASE 11			RETURN g_m_m_armgoon_01
						CASE 12			RETURN g_m_m_armgoon_01
						CASE 13			RETURN g_m_m_armgoon_01
						CASE 14			RETURN g_m_m_armgoon_01
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_North_Chumash
					SWITCH iPed
						CASE 1			RETURN G_M_M_ArmBoss_01
						CASE 2			RETURN G_M_M_ArmBoss_01
						CASE 11			RETURN g_m_m_armgoon_01
						CASE 12			RETURN g_m_m_armgoon_01
						CASE 13			RETURN g_m_m_armgoon_01
						CASE 14			RETURN g_m_m_armgoon_01
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Pacific_Bluffs
					SWITCH iPed
						CASE 1			RETURN G_M_M_ArmBoss_01
						CASE 2			RETURN G_M_M_ArmBoss_01
						CASE 11			RETURN g_m_m_armgoon_01
						CASE 12			RETURN g_m_m_armgoon_01
						CASE 13			RETURN g_m_m_armgoon_01
						CASE 14			RETURN g_m_m_armgoon_01
					ENDSWITCH
				BREAK
				CASE GangOpsLocation_Aqualungs_Vespucci_Beach
					SWITCH iPed
						CASE 1			RETURN G_M_M_ArmBoss_01
						CASE 2			RETURN G_M_M_ArmBoss_01
						CASE 11			RETURN g_m_m_armgoon_01
						CASE 12			RETURN g_m_m_armgoon_01
						CASE 13			RETURN g_m_m_armgoon_01
						CASE 14			RETURN g_m_m_armgoon_01
					ENDSWITCH
				BREAK
			ENDSWITCH
			RETURN S_M_Y_Dockwork_01
			
		CASE GOV_UNDER_CONTROL
			SWITCH iPed
				CASE 0			RETURN S_M_Y_Pilot_01
				CASE 1			RETURN S_M_Y_Pilot_01
				CASE 2			RETURN S_M_M_Marine_02
				CASE 3			RETURN S_M_M_Marine_02
				CASE 4			RETURN S_M_M_Marine_01
				CASE 5			RETURN S_M_M_Marine_02
				CASE 6			RETURN S_M_M_Marine_01
			ENDSWITCH
		BREAK
		
		CASE GOV_BURGLARY_JOB
			SWITCH iPed
				CASE 4			RETURN S_M_M_Doctor_01
			ENDSWITCH
			RETURN S_M_M_HighSec_01
			
		CASE GOV_FLIGHT_RECORDER
			SWITCH iPed
				CASE 1			RETURN A_M_Y_Sunbathe_01
			ENDSWITCH
			RETURN S_M_M_Pilot_02
		
		CASE GOV_GONE_BALLISTIC			
			SWITCH iPed
				CASE 13			
				CASE 14			
					RETURN hc_gunman
				CASE 15
					RETURN S_M_Y_ArmyMech_01
			ENDSWITCH
			RETURN S_M_Y_BLACKOPS_02
			
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC VECTOR GET_GANGOPS_PED_RESPAWN_COORDS(INT iPed, GANGOPS_LOCATION eLocation, GANGOPS_VARIATION eVariation, FREEMODE_DELIVERY_DROPOFFS eDropOff = FREEMODE_DELIVERY_DROPOFF_INVALID, INT iRespawnLocation = 0)
	
	UNUSED_PARAMETER(iPed)
	UNUSED_PARAMETER(eVariation)
	UNUSED_PARAMETER(eLocation)
	UNUSED_PARAMETER(eDropOff)
	UNUSED_PARAMETER(iRespawnLocation)

//	SWITCH eVariation
//		
//	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC FLOAT GET_GANGOPS_PED_RESPAWN_HEADING(INT iPed, GANGOPS_VARIATION eVariation, GANGOPS_LOCATION eLocation, FREEMODE_DELIVERY_DROPOFFS eDropOff = FREEMODE_DELIVERY_DROPOFF_INVALID, INT iRespawnLocation = 0)

	UNUSED_PARAMETER(iPed)
	UNUSED_PARAMETER(eVariation)
	UNUSED_PARAMETER(eLocation)
	UNUSED_PARAMETER(eDropOff)
	UNUSED_PARAMETER(iRespawnLocation)

//	SWITCH eVariation
//		
//	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC MODEL_NAMES GET_GANGOPS_PROP_MODEL(INT iProp, GANGOPS_VARIATION eVariation, GANGOPS_SUBVARIATION eSubvariation)
	
	SWITCH eVariation
	
		CASE GOV_CAR_COLLECTOR
			SWITCH iProp
				CASE 0			RETURN prop_rub_pile_03
			ENDSWITCH
			
			SWITCH eSubvariation
				CASE GOS_CC_AMERICANCO_WAY
					SWITCH iProp
						CASE 1			RETURN prop_elecbox_16
					ENDSWITCH
				BREAK
				CASE GOS_CC_HILLCREST_AVENUE
					SWITCH iProp
						CASE 1			RETURN prop_sec_gate_01d
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE GOV_MOB_MENTALITY
			SWITCH eSubvariation
				CASE GOS_MM_MORNINGWOOD
					SWITCH iProp
						CASE 0			RETURN prop_news_disp_02d
					ENDSWITCH
				BREAK
				CASE GOS_MM_GRAPESEED
					SWITCH iProp
						CASE 0			RETURN prop_vend_water_01
					ENDSWITCH
				BREAK
				CASE GOS_MM_ALTA
					SWITCH iProp
						CASE 0			RETURN prop_boombox_01
					ENDSWITCH
				BREAK
				CASE GOS_MM_MISSION_ROW
					SWITCH iProp
						CASE 0			RETURN prop_radio_01
					ENDSWITCH
				BREAK
				CASE GOS_MM_HIPPY_CAMP
					SWITCH iProp
						CASE 0			RETURN prop_rub_couch02
					ENDSWITCH
				BREAK
				CASE GOS_MM_SAN_ANDREAS
					SWITCH iProp
						CASE 0			RETURN prop_food_van_01
					ENDSWITCH
				BREAK
				CASE GOS_MM_MIRROR_PARK
					SWITCH iProp
						CASE 0			RETURN prop_shuttering01
					ENDSWITCH
				BREAK
				CASE GOS_MM_STRAWBERRY
					SWITCH iProp
						CASE 0			RETURN prop_bench_05
					ENDSWITCH
				BREAK
				CASE GOS_MM_MIRROR_PARK_TAVERN
					SWITCH iProp
						CASE 0			RETURN prop_food_van_01
					ENDSWITCH
				BREAK
				CASE GOS_MM_ULSA
					SWITCH iProp
						CASE 0			RETURN prop_food_van_02
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_AUTO_SALVAGE
			SWITCH iProp
			
				CASE 0		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_auto_salvage_elegy"))
				CASE 1		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_auto_salvage_elegy"))
				CASE 2		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_auto_salvage_elegy"))
			
				DEFAULT 
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_auto_salvage_elegy"))
			ENDSWITCH
		BREAK
			
			
		CASE GOV_AQUALUNGS
			SWITCH eSubvariation
				CASE GOS_AL_ELYSIAN_ISLAND_1
				CASE GOS_AL_PALETO_BAY
				CASE GOS_AL_ELYSIAN_ISLAND_2
				CASE GOS_AL_PALOMINO_HIGHLANDS
				CASE GOS_AL_CHUMASH
				CASE GOS_AL_SAN_CHIANSKI_MOUNTAIN_RANGE
				CASE GOS_AL_NORTH_CHUMASH
					SWITCH iProp
						CASE 0			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_barge_01"))
						CASE 1			RETURN Prop_Contr_03b_LD
						CASE 2			RETURN prop_container_03mb
						CASE 3			RETURN Prop_Contr_03b_LD
						CASE 4			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01b")) 
						CASE 5			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_Barge_Col_01")) 
						CASE 6			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_Barge_Col_02")) 
					ENDSWITCH
				BREAK
				CASE GOS_AL_PALETO_FOREST
					SWITCH iProp
						CASE 0			RETURN xm_prop_x17_barge_01
						CASE 1			RETURN Prop_Contr_03b_LD
						CASE 2			RETURN prop_container_03mb
						CASE 3			RETURN Prop_Contr_03b_LD
						CASE 4			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01b")) 
						CASE 5			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_Barge_Col_01")) 
						CASE 6			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_Barge_Col_02")) 
					ENDSWITCH
				BREAK
				CASE GOS_AL_PACIFIC_BLUFFS
				CASE GOS_AL_VESPUCCI_BEACH
					SWITCH iProp
						CASE 0			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_barge_01"))
						CASE 1			RETURN prop_container_03mb
						CASE 2			RETURN Prop_Contr_03b_LD
						CASE 3			RETURN Prop_Contr_03b_LD
						CASE 4			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01b"))  
						CASE 5			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_Barge_Col_01")) 
						CASE 6			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_Barge_Col_02")) 
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_BURGLARY_JOB
			SWITCH iProp
				CASE 0		RETURN ex_prop_adv_case
				CASE 1 		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xm_Prop_x17_LAP_TOP_01"))
			ENDSWITCH
			
			SWITCH eSubvariation
				CASE GOS_BJ_INESENO_ROAD
					SWITCH iProp
						CASE 2		RETURN prop_wall_light_06a
						CASE 3		RETURN prop_wall_light_06a
						CASE 4		RETURN prop_wall_light_06a
						CASE 5		RETURN prop_wall_light_06a
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_FLIGHT_RECORDER
			SWITCH iProp
				CASE 0		RETURN xm_prop_x17_Flight_Rec_01a
			ENDSWITCH
		BREAK
		
		CASE GOV_GONE_BALLISTIC
			SWITCH eSubvariation
				CASE GOS_GB_ELYSIAN_ISLAND_1
					SWITCH iProp
						CASE 0			RETURN xm_prop_x17_trail_02a
						CASE 1			RETURN xm_prop_x17_trail_02a
						CASE 2			RETURN xm_prop_x17_trail_01a
						CASE 3			RETURN xm_prop_x17_trail_01a
						CASE 4			RETURN prop_container_ld_pu
						CASE 5			RETURN prop_container_ld_pu
						CASE 6			RETURN prop_container_ld_pu
						CASE 7			RETURN prop_container_ld_pu
						CASE 8			RETURN prop_generator_03b
						CASE 9			RETURN prop_generator_03b
						CASE 10			RETURN prop_generator_03b
						CASE 11			RETURN prop_generator_03b
					ENDSWITCH
				BREAK
				CASE GOS_GB_GRAND_SENORA_DESERT_1
					SWITCH iProp
						CASE 0			RETURN xm_prop_x17_trail_01a
						CASE 1			RETURN xm_prop_x17_trail_02a
						CASE 2			RETURN xm_prop_x17_trail_02a
						CASE 3			RETURN xm_prop_x17_trail_01a
						CASE 4			RETURN prop_container_ld_pu
						CASE 5			RETURN prop_container_ld_pu
						CASE 6			RETURN prop_container_ld_pu
						CASE 7			RETURN prop_container_ld_pu
						CASE 8			RETURN prop_generator_03b
						CASE 9			RETURN prop_generator_03b
						CASE 10			RETURN prop_generator_03b
						CASE 11			RETURN prop_generator_03b
					ENDSWITCH
				BREAK
				CASE GOS_GB_GRAPESEED_1
					SWITCH iProp
						CASE 0			RETURN xm_prop_x17_trail_01a
						CASE 1			RETURN xm_prop_x17_trail_02a
						CASE 2			RETURN xm_prop_x17_trail_01a
						CASE 3			RETURN xm_prop_x17_trail_02a
						CASE 4			RETURN prop_container_ld_pu
						CASE 5			RETURN prop_container_ld_pu
						CASE 6			RETURN prop_container_ld_pu
						CASE 7			RETURN prop_container_ld_pu
						CASE 8			RETURN prop_generator_03b
						CASE 9			RETURN prop_generator_03b
						CASE 10			RETURN prop_generator_03b
						CASE 11			RETURN prop_generator_03b
					ENDSWITCH
				BREAK
				CASE GOS_GB_EL_BURRO_HEIGHTS
					SWITCH iProp
						CASE 0			RETURN xm_prop_x17_trail_02a
						CASE 1			RETURN xm_prop_x17_trail_01a
						CASE 2			RETURN xm_prop_x17_trail_02a
						CASE 3			RETURN xm_prop_x17_trail_01a
						CASE 4			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_cont_01a"))
						CASE 5			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_cont_01a"))
						CASE 6			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_cont_01a"))
						CASE 7			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_cont_01a"))
						CASE 8			RETURN prop_generator_03b
						CASE 9			RETURN prop_generator_03b
						CASE 10			RETURN prop_generator_03b
						CASE 11			RETURN prop_generator_03b
					ENDSWITCH
				BREAK
				CASE GOS_GB_TERMINAL
					SWITCH iProp
						CASE 0			RETURN xm_prop_x17_trail_01a
						CASE 1			RETURN xm_prop_x17_trail_02a
						CASE 2			RETURN xm_prop_x17_trail_02a
						CASE 3			RETURN xm_prop_x17_trail_01a
						CASE 4			RETURN prop_container_ld_pu
						CASE 5			RETURN prop_container_ld_pu
						CASE 6			RETURN prop_container_ld_pu
						CASE 7			RETURN prop_container_ld_pu
						CASE 8			RETURN prop_generator_03b
						CASE 9			RETURN prop_generator_03b
						CASE 10			RETURN prop_generator_03b
						CASE 11			RETURN prop_generator_03b
					ENDSWITCH
				BREAK
				CASE GOS_GB_LSIA
					SWITCH iProp
						CASE 0			RETURN xm_prop_x17_trail_01a
						CASE 1			RETURN xm_prop_x17_trail_02a
						CASE 2			RETURN xm_prop_x17_trail_01a
						CASE 3			RETURN xm_prop_x17_trail_02a
						CASE 4			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_cont_01a"))
						CASE 5			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_cont_01a"))
						CASE 6			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_cont_01a"))
						CASE 7			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_cont_01a"))
						CASE 8			RETURN prop_generator_03b
						CASE 9			RETURN prop_generator_03b
						CASE 10			RETURN prop_generator_03b
						CASE 11			RETURN prop_generator_03b
					ENDSWITCH
				BREAK
				CASE GOS_GB_MURRIETA_HEIGHTS
					SWITCH iProp
						CASE 0			RETURN xm_prop_x17_trail_02a
						CASE 1			RETURN xm_prop_x17_trail_02a
						CASE 2			RETURN xm_prop_x17_trail_01a
						CASE 3			RETURN xm_prop_x17_trail_01a
						CASE 4			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_cont_01a"))
						CASE 5			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_cont_01a"))
						CASE 6			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_cont_01a"))
						CASE 7			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_cont_01a"))
						CASE 8			RETURN prop_generator_03b
						CASE 9			RETURN prop_generator_03b
						CASE 10			RETURN prop_generator_03b
						CASE 11			RETURN prop_generator_03b
					ENDSWITCH
				BREAK
				CASE GOS_GB_ELYSIAN_ISLAND_2
					SWITCH iProp
						CASE 0			RETURN xm_prop_x17_trail_02a
						CASE 1			RETURN xm_prop_x17_trail_01a
						CASE 2			RETURN xm_prop_x17_trail_02a
						CASE 3			RETURN xm_prop_x17_trail_01a
						CASE 4			RETURN prop_container_ld_pu
						CASE 5			RETURN prop_container_ld_pu
						CASE 6			RETURN prop_container_ld_pu
						CASE 7			RETURN prop_container_ld_pu
						CASE 8			RETURN prop_generator_03b
						CASE 9			RETURN prop_generator_03b
						CASE 10			RETURN prop_generator_03b
						CASE 11			RETURN prop_generator_03b
					ENDSWITCH
				BREAK
				CASE GOS_GB_PALETO_BAY
					SWITCH iProp
						CASE 0			RETURN xm_prop_x17_trail_01a
						CASE 1			RETURN xm_prop_x17_trail_02a
						CASE 2			RETURN xm_prop_x17_trail_01a
						CASE 3			RETURN xm_prop_x17_trail_02a
						CASE 4			RETURN prop_container_ld_pu
						CASE 5			RETURN prop_container_ld_pu
						CASE 6			RETURN prop_container_ld_pu
						CASE 7			RETURN prop_container_ld_pu
						CASE 8			RETURN prop_generator_03b
						CASE 9			RETURN prop_generator_03b
						CASE 10			RETURN prop_generator_03b
						CASE 11			RETURN prop_generator_03b
					ENDSWITCH
				BREAK
				CASE GOS_GB_FORT_ZANCUDO
					SWITCH iProp
						CASE 0			RETURN xm_prop_x17_trail_01a
						CASE 1			RETURN xm_prop_x17_trail_01a
						CASE 2			RETURN xm_prop_x17_trail_02a
						CASE 3			RETURN xm_prop_x17_trail_02a
						CASE 4			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_cont_01a"))
						CASE 5			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_cont_01a"))
						CASE 6			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_cont_01a"))
						CASE 7			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_cont_01a"))
						CASE 8			RETURN prop_generator_03b
						CASE 9			RETURN prop_generator_03b
						CASE 10			RETURN prop_generator_03b
						CASE 11			RETURN prop_generator_03b
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC VECTOR GET_GANGOPS_PROP_COORDS(INT iProp, GANGOPS_VARIATION eVariation, GANGOPS_SUBVARIATION eSubvariation, INT iExtraParam = -1, HANGAR_ID eHangar = HANGAR_ID_INVALID, INT iRespawnLocation = 0)
	
	UNUSED_PARAMETER(iExtraParam)
	UNUSED_PARAMETER(eHangar)
	UNUSED_PARAMETER(iRespawnLocation)
	
	SWITCH eVariation
	
		CASE GOV_CAR_COLLECTOR
			SWITCH iProp
				CASE 0			RETURN <<637.3531, 4750.4722, -60.0000>>
			ENDSWITCH
			
			SWITCH eSubvariation
				CASE GOS_CC_AMERICANCO_WAY
					SWITCH iProp
						CASE 1			RETURN <<-1564.4750, 7.9360, 58.4450>>
					ENDSWITCH
				BREAK
				CASE GOS_CC_HILLCREST_AVENUE
					SWITCH iProp
						CASE 1			RETURN <<-711.7120, 647.5380, 156.0740>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE GOV_MOB_MENTALITY
			SWITCH eSubvariation
				CASE GOS_MM_MORNINGWOOD
					SWITCH iProp
						CASE 0			RETURN <<-1383.0950, -178.0631, 46.3700>>
					ENDSWITCH
				BREAK
				CASE GOS_MM_GRAPESEED
					SWITCH iProp
						CASE 0			RETURN <<1788.1426, 4594.5034, 36.6826>>
					ENDSWITCH
				BREAK
				CASE GOS_MM_ALTA
					SWITCH iProp
						CASE 0			RETURN <<393.0432, -356.6242, 47.2646>>
					ENDSWITCH
				BREAK
				CASE GOS_MM_MISSION_ROW
					SWITCH iProp
						CASE 0			RETURN <<261.0518, -1151.0374, 28.3732>>
					ENDSWITCH
				BREAK
				CASE GOS_MM_HIPPY_CAMP
					SWITCH iProp
						CASE 0			RETURN <<2326.0151, 2514.6035, 45.6380>>
					ENDSWITCH
				BREAK
				CASE GOS_MM_SAN_ANDREAS
					SWITCH iProp
						CASE 0			RETURN <<-1220.2712, -1459.6193, 4.5605>>
					ENDSWITCH
				BREAK
				CASE GOS_MM_MIRROR_PARK
					SWITCH iProp
						CASE 0			RETURN <<1348.9170, -711.1690, 65.1940>>
					ENDSWITCH
				BREAK
				CASE GOS_MM_STRAWBERRY
					SWITCH iProp
						CASE 0			RETURN <<64.7840, -1574.6121, 28.6690>>
					ENDSWITCH
				BREAK
				CASE GOS_MM_MIRROR_PARK_TAVERN
					SWITCH iProp
						CASE 0			RETURN <<1230.8131, -430.8905, 67.9109>>
					ENDSWITCH
				BREAK
				CASE GOS_MM_ULSA
					SWITCH iProp
						CASE 0			RETURN <<-1624.6580, 185.3620, 60.7872>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_AQUALUNGS
			SWITCH eSubvariation
				CASE GOS_AL_ELYSIAN_ISLAND_1
					SWITCH iProp
						CASE 5
						CASE 6
						CASE 0			
							RETURN <<-512.6076, -3240.4370, -0.1497>>
							
						CASE 1			RETURN <<-510.1722, -3246.1030, 1.8030>>
						CASE 2			RETURN <<-516.7360, -3235.9299, 1.8030>>
						CASE 3			RETURN <<-511.0520, -3236.9180, 1.8140>>
						CASE 4			RETURN <<-517.2375, -3243.3049, 1.8043>>
					ENDSWITCH
				BREAK
				CASE GOS_AL_PALETO_BAY
					SWITCH iProp
						CASE 5
						CASE 6
						CASE 0
							RETURN <<719.3928, 6989.2305, -0.4997>>
							
						CASE 1			RETURN <<725.1097, 6985.5142, 1.4590>>
						CASE 2			RETURN <<714.4261, 6992.9126, 1.4528>>
						CASE 3			RETURN <<716.8192, 6988.7148, 1.4527>>
						CASE 4			RETURN <<723.6021, 6991.6616, 1.4770>>
					ENDSWITCH
				BREAK
				CASE GOS_AL_ELYSIAN_ISLAND_2
					SWITCH iProp
						CASE 5
						CASE 6
						CASE 0
							RETURN <<777.8170, -3590.5801, 0.6000>>
							
						CASE 1			RETURN <<774.8008, -3596.5571, 2.5590>>
						CASE 2			RETURN <<780.3835, -3586.1265, 2.5528>>
						CASE 3			RETURN <<773.7910, -3586.4409, 2.5490>>
						CASE 4			RETURN <<781.2618, -3591.7708, 2.5770>>
					ENDSWITCH
				BREAK
				CASE GOS_AL_PALOMINO_HIGHLANDS
					SWITCH iProp
						CASE 5
						CASE 6
						CASE 0
							RETURN <<3054.3921, -1109.6013, 0.0000>>
						CASE 1			RETURN <<3060.8420, -1109.3788, 1.9690>>
						CASE 2			RETURN <<3048.3579, -1110.4839, 1.9529>>
						CASE 3			RETURN <<3051.8052, -1105.7367, 1.9690>>
						CASE 4			RETURN <<3055.6311, -1114.2578, 1.9674>>
					ENDSWITCH
				BREAK
				CASE GOS_AL_CHUMASH
					SWITCH iProp
						CASE 5
						CASE 6
						CASE 0
							RETURN <<-3738.3291, 1313.7350, -0.2000>>
						CASE 1			RETURN <<-3738.2458, 1320.4564, 1.7690>>
						CASE 2			RETURN <<-3739.3762, 1307.8030, 1.7529>>
						CASE 3			RETURN <<-3736.0696, 1311.8668, 1.7690>>
						CASE 4			RETURN <<-3743.3262, 1312.8463, 1.7774>>
					ENDSWITCH
				BREAK
				CASE GOS_AL_SAN_CHIANSKI_MOUNTAIN_RANGE
					SWITCH iProp
						CASE 5
						CASE 6
						CASE 0
							RETURN <<3987.4790, 5100.8999, -0.5000>>
						CASE 1			RETURN <<3983.7791, 5105.9810, 1.4690>>
						CASE 2			RETURN <<3992.7791, 5097.4336, 1.4528>>
						CASE 3			RETURN <<3987.4224, 5096.6880, 1.4790>>
						CASE 4			RETURN <<3991.0466, 5105.1582, 1.4475>>
					ENDSWITCH
				BREAK
				CASE GOS_AL_PALETO_FOREST
					SWITCH iProp
						CASE 5
						CASE 6
						CASE 0
							RETURN <<-1519.6511, 5775.9199, -0.5297>>
						CASE 1			RETURN <<-1520.6847, 5769.1851, 1.4232>>
						CASE 2			RETURN <<-1518.9584, 5782.2036, 1.4232>>
						CASE 3			RETURN <<-1514.8250, 5777.8296, 1.4232>>
						CASE 4			RETURN <<-1523.8406, 5777.4395, 1.4247>>
					ENDSWITCH
				BREAK
				CASE GOS_AL_NORTH_CHUMASH
					SWITCH iProp
						CASE 5
						CASE 6
						CASE 0
							RETURN <<-2956.1641, 4414.5649, -0.5100>>
						CASE 1			RETURN <<-2959.0259, 4420.4131, 1.4690>>
						CASE 2			RETURN <<-2957.0132, 4408.7759, 1.4428>>
						CASE 3			RETURN <<-2952.5356, 4413.0708, 1.4490>>
						CASE 4			RETURN <<-2959.6106, 4415.0801, 1.4474>>
					ENDSWITCH
				BREAK
				CASE GOS_AL_PACIFIC_BLUFFS
					SWITCH iProp
						CASE 5
						CASE 6
						CASE 0
							RETURN <<-2233.3040, -929.5290, -0.5000>>
						CASE 1			RETURN <<-2238.8135, -926.7743, 1.4529>>
						CASE 2			RETURN <<-2228.8286, -933.9307, 1.4590>>
						CASE 3			RETURN <<-2233.6785, -925.4976, 1.4590>>
						CASE 4			RETURN <<-2233.4006, -933.1432, 1.4674>>
					ENDSWITCH
				BREAK
				CASE GOS_AL_VESPUCCI_BEACH
					SWITCH iProp
						CASE 5
						CASE 6
						CASE 0
							RETURN <<-1781.5380, -1754.9690, -0.4300>>
						CASE 1			RETURN <<-1778.3102, -1760.2300, 1.5229>>
						CASE 2			RETURN <<-1785.7799, -1750.4393, 1.5290>>
						CASE 3			RETURN <<-1777.4003, -1755.0104, 1.5290>>
						CASE 4			RETURN <<-1784.5081, -1756.4857, 1.5275>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_BURGLARY_JOB
			SWITCH iExtraParam
				CASE 0 
					SWITCH iProp
						CASE 0		RETURN <<584.616, 4754.479, -59.900>>
						CASE 1		RETURN <<584.616, 4754.510, -59.750>>
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH iProp
						CASE 0		RETURN <<573.600, 4750.100, -59.900>>
						CASE 1		RETURN <<573.600, 4750.131, -59.750>>
					ENDSWITCH
				BREAK
			ENDSWITCH
			
			SWITCH eSubvariation
				CASE GOS_BJ_INESENO_ROAD
					SWITCH iProp
						CASE 2		RETURN <<-3093.710,340.330,9.450>>
						CASE 3		RETURN <<-3092.440,345.070,9.450>>
						CASE 4		RETURN <<-3093.100,350.000,9.450>>
						CASE 5		RETURN <<-3091.520,355.800,9.450>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_FLIGHT_RECORDER
			SWITCH iProp
				CASE 0		RETURN <<-50.0, -50.0, -50.0>>
			ENDSWITCH
		BREAK
		
		CASE GOV_GONE_BALLISTIC
			SWITCH eSubvariation
				CASE GOS_GB_ELYSIAN_ISLAND_1
					SWITCH iProp
						CASE 0			RETURN <<-163.7430, -2579.6440, 5.4964>>
						CASE 1			RETURN <<-129.3215, -2555.8677, 5.4944>>
						CASE 2			RETURN <<-136.3338, -2551.2258, 5.4978>>
						CASE 3			RETURN <<-140.5752, -2578.2385, 5.4960>>
						CASE 4			RETURN <<-150.6160, -2579.1960, 4.9990>>
						CASE 5			RETURN <<-156.1180, -2577.3750, 4.9990>>
						CASE 6			RETURN <<-129.3220, -2575.1470, 4.9990>>
						CASE 7			RETURN <<-155.3200, -2542.2820, 5.0470>>
						CASE 8			RETURN <<-133.1920, -2576.7400, 5.0600>>
						CASE 9			RETURN <<-136.1110, -2556.7209, 5.0660>>
						CASE 10			RETURN <<-158.5690, -2575.5549, 5.0610>>
						CASE 11			RETURN <<-159.7290, -2541.6699, 5.0910>>
					ENDSWITCH
				BREAK
				CASE GOS_GB_GRAND_SENORA_DESERT_1
					SWITCH iProp
						CASE 0			RETURN <<1454.7935, 1516.8613, 111.4405>>
						CASE 1			RETURN <<1432.7411, 1500.2556, 113.2811>>
						CASE 2			RETURN <<1446.5851, 1539.8411, 110.4020>>
						CASE 3			RETURN <<1420.0863, 1543.4922, 108.9584>>
						CASE 4			RETURN <<1445.3840, 1505.1130, 112.1400>>
						CASE 5			RETURN <<1411.9830, 1537.3970, 108.3420>>
						CASE 6			RETURN <<1436.9100, 1543.3640, 109.5220>>
						CASE 7			RETURN <<1432.1910, 1543.4760, 109.3350>>
						CASE 8			RETURN <<1423.3669, 1503.6610, 112.7460>>
						CASE 9			RETURN <<1451.4530, 1525.8669, 110.7620>>
						CASE 10			RETURN <<1427.1980, 1542.4540, 109.1820>>
						CASE 11			RETURN <<1409.0790, 1530.0210, 109.5740>>
					ENDSWITCH
				BREAK
				CASE GOS_GB_GRAPESEED_1
					SWITCH iProp
						CASE 0			RETURN <<2200.7610, 5028.6641, 43.3540>>
						CASE 1			RETURN <<2201.5840, 5000.5771, 42.2410>>
						CASE 2			RETURN <<2175.7019, 5005.3682, 41.3120>>
						CASE 3			RETURN <<2161.2910, 5021.4360, 41.6720>>
						CASE 4			RETURN <<2192.9460, 5034.2642, 42.5620>>
						CASE 5			RETURN <<2208.9280, 5009.3481, 42.5620>>
						CASE 6			RETURN <<2205.8821, 5005.2358, 42.3630>>
						CASE 7			RETURN <<2165.5630, 5011.3970, 40.5100>>
						CASE 8			RETURN <<2187.9590, 5035.9482, 42.5130>>
						CASE 9			RETURN <<2204.6970, 5022.7739, 42.7896>>
						CASE 10			RETURN <<2183.6421, 5001.9028, 40.9710>>
						CASE 11			RETURN <<2165.6655, 5016.2402, 40.6891>>
					ENDSWITCH
				BREAK
				CASE GOS_GB_EL_BURRO_HEIGHTS
					SWITCH iProp
						CASE 0			RETURN <<1094.0360, -2454.1211, 29.6190>>
						CASE 1			RETURN <<1106.3051, -2439.2561, 30.2210>>
						CASE 2			RETURN <<1128.3470, -2448.1919, 30.8100>>
						CASE 3			RETURN <<1118.1150, -2470.7000, 30.1660>>
						CASE 4			RETURN <<1107.3340, -2473.1289, 29.2860>>
						CASE 5			RETURN <<1126.4270, -2466.2380, 30.0460>>
						CASE 6			RETURN <<1130.9980, -2465.8679, 30.1320>>
						CASE 7			RETURN <<1100.2209, -2445.6589, 29.4420>>
						CASE 8			RETURN <<1127.6820, -2462.6489, 30.1810>>
						CASE 9			RETURN <<1120.6670, -2441.6509, 30.5280>>
						CASE 10			RETURN <<1110.9821, -2470.4070, 29.4580>>
						CASE 11			RETURN <<1092.2950, -2461.6299, 29.0820>>
					ENDSWITCH
				BREAK
				CASE GOS_GB_TERMINAL
					SWITCH iProp
						CASE 0			RETURN <<996.6782, -3188.0024, 5.3961>>
						CASE 1			RETURN <<1019.7880, -3180.3950, 5.3861>>
						CASE 2			RETURN <<997.0750, -3207.3037, 5.3964>>
						CASE 3			RETURN <<1019.2342, -3214.4888, 5.2876>>
						CASE 4			RETURN <<1005.5296, -3181.7517, 4.8993>>
						CASE 5			RETURN <<1005.6576, -3213.5449, 4.8976>>
						CASE 6			RETURN <<1009.5392, -3213.4497, 4.8937>>
						CASE 7			RETURN <<1029.7872, -3209.5471, 4.8692>>
						CASE 8			RETURN <<1029.3647, -3205.4290, 4.9613>>
						CASE 9			RETURN <<1002.2422, -3211.8877, 4.9616>>
						CASE 10			RETURN <<1001.6072, -3184.4529, 4.9602>>
						CASE 11			RETURN <<1025.5017, -3183.8274, 4.9603>>
					ENDSWITCH
				BREAK
				CASE GOS_GB_LSIA
					SWITCH iProp
						CASE 0			RETURN <<-1195.2975, -2059.9561, 13.7253>>
						CASE 1			RETURN <<-1219.7146, -2069.0654, 13.7858>>
						CASE 2			RETURN <<-1226.3219, -2052.3740, 13.5147>>
						CASE 3			RETURN <<-1215.4188, -2037.5431, 13.4740>>
						CASE 4			RETURN <<-1221.4550, -2043.2756, 12.9814>>
						CASE 5			RETURN <<-1228.7511, -2065.2876, 13.4581>>
						CASE 6			RETURN <<-1230.7584, -2061.5671, 13.3659>>
						CASE 7			RETURN <<-1202.9528, -2078.6213, 13.0971>>
						CASE 8			RETURN <<-1194.4313, -2052.1636, 13.1896>>
						CASE 9			RETURN <<-1225.8942, -2060.6956, 13.3195>>
						CASE 10			RETURN <<-1221.5776, -2047.7876, 13.0386>>
						CASE 11			RETURN <<-1199.8436, -2066.2212, 13.1388>>
					ENDSWITCH
				BREAK
				CASE GOS_GB_MURRIETA_HEIGHTS
					SWITCH iProp
						CASE 0			RETURN <<1302.8270, -1947.0304, 42.8446>>
						CASE 1			RETURN <<1267.2318, -1962.5267, 42.7892>>
						CASE 2			RETURN <<1260.7849, -1941.0095, 42.7892>>
						CASE 3			RETURN <<1285.0796, -1956.6442, 43.2430>>
						CASE 4			RETURN <<1256.9270, -1947.3783, 42.2624>>
						CASE 5			RETURN <<1261.5604, -1948.0508, 42.2624>>
						CASE 6			RETURN <<1281.8270, -1933.4846, 42.2624>>
						CASE 7			RETURN <<1271.5422, -1967.7047, 42.2678>>
						CASE 8			RETURN <<1263.5186, -1950.7781, 42.3232>>
						CASE 9			RETURN <<1275.0038, -1961.5460, 42.4237>>
						CASE 10			RETURN <<1289.4534, -1950.9946, 42.6266>>
						CASE 11			RETURN <<1284.8652, -1934.4056, 42.3232>>
					ENDSWITCH
				BREAK
				CASE GOS_GB_ELYSIAN_ISLAND_2
					SWITCH iProp
						CASE 0			RETURN <<-412.2687, -2749.4321, 5.4856>>
						CASE 1			RETURN <<-391.8336, -2733.5781, 5.4903>>
						CASE 2			RETURN <<-405.0316, -2748.9734, 5.4856>>
						CASE 3			RETURN <<-428.5004, -2713.3555, 5.4856>>
						CASE 4			RETURN <<-384.9870, -2725.3521, 5.0160>>
						CASE 5			RETURN <<-421.7420, -2703.2991, 4.9990>>
						CASE 6			RETURN <<-418.4800, -2706.8059, 4.9990>>
						CASE 7			RETURN <<-397.6740, -2740.7590, 5.0060>>
						CASE 8			RETURN <<-427.3760, -2720.3799, 5.0600>>
						CASE 9			RETURN <<-416.7360, -2744.5381, 5.0600>>
						CASE 10			RETURN <<-389.0090, -2727.3059, 5.0740>>
						CASE 11			RETURN <<-408.5170, -2701.8391, 5.0600>>
					ENDSWITCH
				BREAK
				CASE GOS_GB_PALETO_BAY
					SWITCH iProp
						CASE 0			RETURN <<-423.8440, 6407.3374, 1.7344>>
						CASE 1			RETURN <<-415.0120, 6422.4570, 2.8330>>
						CASE 2			RETURN <<-426.8056, 6444.4697, 2.9362>>
						CASE 3			RETURN <<-443.7218, 6408.4868, 1.8620>>
						CASE 4			RETURN <<-433.1564, 6405.6509, 1.4198>>
						CASE 5			RETURN <<-418.2893, 6431.9990, 2.7586>>
						CASE 6			RETURN <<-422.1100, 6436.2051, 2.5338>>
						CASE 7			RETURN <<-450.9187, 6422.7627, 1.4295>>
						CASE 8			RETURN <<-437.1135, 6407.1040, 1.5512>>
						CASE 9			RETURN <<-446.9612, 6415.4653, 1.5103>>
						CASE 10			RETURN <<-419.7205, 6428.6353, 2.5997>>
						CASE 11			RETURN <<-419.3654, 6414.2534, 1.6457>>
					ENDSWITCH
				BREAK
				CASE GOS_GB_FORT_ZANCUDO
					SWITCH iProp
						CASE 0			RETURN <<-2741.4341, 3470.8037, 11.3896>>
						CASE 1			RETURN <<-2763.1697, 3450.1868, 11.2088>>
						CASE 2			RETURN <<-2747.6365, 3456.6965, 11.5753>>
						CASE 3			RETURN <<-2769.4497, 3478.4500, 10.5607>>
						CASE 4			RETURN <<-2772.6670, 3459.9688, 10.8025>>
						CASE 5			RETURN <<-2770.8540, 3464.8857, 11.0674>>
						CASE 6			RETURN <<-2767.0686, 3489.6782, 9.5852>>
						CASE 7			RETURN <<-2744.8105, 3464.6052, 11.3014>>
						CASE 8			RETURN <<-2756.4358, 3451.4944, 10.8121>>
						CASE 9			RETURN <<-2766.9919, 3471.7578, 10.6064>>
						CASE 10			RETURN <<-2741.0923, 3478.2473, 10.7775>>
						CASE 11			RETURN <<-2767.9646, 3454.7539, 10.6716>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC FLOAT GET_GANGOPS_PROP_HEADING(INT iProp, GANGOPS_VARIATION eVariation, GANGOPS_SUBVARIATION eSubvariation, INT iExtraParam = -1, HANGAR_ID eHangar = HANGAR_ID_INVALID, INT iRespawnLocation = 0)
	
	UNUSED_PARAMETER(iExtraParam)
	UNUSED_PARAMETER(eHangar)
	UNUSED_PARAMETER(iRespawnLocation)
	
	SWITCH eVariation
	
		CASE GOV_CAR_COLLECTOR
			SWITCH iProp
				CASE 0			RETURN 91.0205
			ENDSWITCH
					
			SWITCH eSubvariation
				CASE GOS_CC_AMERICANCO_WAY
					SWITCH iProp
						CASE 1			RETURN 258.6090
					ENDSWITCH
				BREAK
				CASE GOS_CC_HILLCREST_AVENUE
					SWITCH iProp
						CASE 1			RETURN 0.0
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE GOV_MOB_MENTALITY
			SWITCH eSubvariation
				CASE GOS_MM_MORNINGWOOD
					SWITCH iProp
						CASE 0			RETURN 182.1997
					ENDSWITCH
				BREAK
				CASE GOS_MM_GRAPESEED
					SWITCH iProp
						CASE 0			RETURN 5.1989
					ENDSWITCH
				BREAK
				CASE GOS_MM_ALTA
					SWITCH iProp
						CASE 0			RETURN 258.3999
					ENDSWITCH
				BREAK
				CASE GOS_MM_MISSION_ROW
					SWITCH iProp
						CASE 0			RETURN 31.7985
					ENDSWITCH
				BREAK
				CASE GOS_MM_HIPPY_CAMP
					SWITCH iProp
						CASE 0			RETURN 344.597
					ENDSWITCH
				BREAK
				CASE GOS_MM_SAN_ANDREAS
					SWITCH iProp
						CASE 0			RETURN 305.1983
					ENDSWITCH
				BREAK
				CASE GOS_MM_MIRROR_PARK
					SWITCH iProp
						CASE 0			RETURN 85.8410
					ENDSWITCH
				BREAK
				CASE GOS_MM_STRAWBERRY
					SWITCH iProp
						CASE 0			RETURN 230.1990
					ENDSWITCH
				BREAK
				CASE GOS_MM_MIRROR_PARK_TAVERN
					SWITCH iProp
						CASE 0			RETURN 10.5960
					ENDSWITCH
				BREAK
				CASE GOS_MM_ULSA
					SWITCH iProp
						CASE 0			RETURN 115.3983
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_AQUALUNGS
			SWITCH eSubvariation
				CASE GOS_AL_ELYSIAN_ISLAND_1
					SWITCH iProp
						CASE 5
						CASE 6
						CASE 0
							RETURN 48.7992
							
						CASE 1			RETURN 0.0000
						CASE 2			RETURN 137.8990
						CASE 3			RETURN 49.0000
						CASE 4			RETURN 63.5958
					ENDSWITCH
				BREAK
				CASE GOS_AL_PALETO_BAY
					SWITCH iProp
						CASE 5
						CASE 6
						CASE 0
							RETURN 217.3993
						CASE 1			RETURN 0.0000
						CASE 2			RETURN 307.1994
						CASE 3			RETURN 0.0000
						CASE 4			RETURN 0.0000
					ENDSWITCH
				BREAK
				CASE GOS_AL_ELYSIAN_ISLAND_2
					SWITCH iProp
						CASE 5
						CASE 6
						CASE 0
							RETURN 133.0000
						CASE 1			RETURN 222.6997
						CASE 2			RETURN 224.7988
						CASE 3			RETURN 306.5980
						CASE 4			RETURN 0.0000
					ENDSWITCH
				BREAK
				CASE GOS_AL_PALOMINO_HIGHLANDS
					SWITCH iProp
						CASE 5
						CASE 6
						CASE 0
							RETURN 110.5990
						CASE 1			RETURN 205.1990
						CASE 2			RETURN 200.7000
						CASE 3			RETURN -75.6000
						CASE 4			RETURN 159.3997
					ENDSWITCH
				BREAK
				CASE GOS_AL_CHUMASH
					SWITCH iProp
						CASE 5
						CASE 6
						CASE 0
							RETURN 339.3000
						CASE 1			RETURN 69.0000
						CASE 2			RETURN 72.0999
						CASE 3			RETURN 0.0000
						CASE 4			RETURN 332.6000
					ENDSWITCH
				BREAK
				CASE GOS_AL_SAN_CHIANSKI_MOUNTAIN_RANGE
					SWITCH iProp
						CASE 5
						CASE 6
						CASE 0
							RETURN 254.2000
						CASE 1			RETURN 152.9997
						CASE 2			RETURN 0.0000
						CASE 3			RETURN 0.0000
						CASE 4			RETURN 86.3996
					ENDSWITCH
				BREAK
				CASE GOS_AL_PALETO_FOREST
					SWITCH iProp
						CASE 5
						CASE 6
						CASE 0
							RETURN 10.1995
						CASE 1			RETURN 280.6000
						CASE 2			RETURN 95.4000
						CASE 3			RETURN 189.5000
						CASE 4			RETURN 61.5001
					ENDSWITCH
				BREAK
				CASE GOS_AL_NORTH_CHUMASH
					SWITCH iProp
						CASE 5
						CASE 6
						CASE 0
							RETURN 352.4000
						CASE 1			RETURN 277.5001
						CASE 2			RETURN 0.0000
						CASE 3			RETURN 0.0000
						CASE 4			RETURN 118.5997
					ENDSWITCH
				BREAK
				CASE GOS_AL_PACIFIC_BLUFFS
					SWITCH iProp
						CASE 5
						CASE 6
						CASE 0
							RETURN 80.9000
						CASE 1			RETURN 0.0000
						CASE 2			RETURN 156.1000
						CASE 3			RETURN 0.0000
						CASE 4			RETURN 129.9997
					ENDSWITCH
				BREAK
				CASE GOS_AL_VESPUCCI_BEACH
					SWITCH iProp
						CASE 5
						CASE 6
						CASE 0
							RETURN 15.2990
						CASE 1			RETURN 0.0000
						CASE 2			RETURN 298.2981
						CASE 3			RETURN 0.0000
						CASE 4			RETURN 167.5998
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_BURGLARY_JOB
			SWITCH iProp
				CASE 0		RETURN -0.2030
			ENDSWITCH
		BREAK
		
		CASE GOV_GONE_BALLISTIC
			SWITCH eSubvariation
				CASE GOS_GB_ELYSIAN_ISLAND_1
					SWITCH iProp
						CASE 0			RETURN 88.9980
						CASE 1			RETURN 234.7980
						CASE 2			RETURN 235.1980
						CASE 3			RETURN 89.5970
						CASE 4			RETURN 39.1970
						CASE 5			RETURN 324.7970
						CASE 6			RETURN 294.1970
						CASE 7			RETURN 332.1970
						CASE 8			RETURN 34.1960
						CASE 9			RETURN 144.1960
						CASE 10			RETURN 334.1950
						CASE 11			RETURN 201.1950
					ENDSWITCH
				BREAK
				CASE GOS_GB_GRAND_SENORA_DESERT_1
					SWITCH iProp
						CASE 0			RETURN 255.8000
						CASE 1			RETURN 174.6000
						CASE 2			RETURN 307.2000
						CASE 3			RETURN 21.6570
						CASE 4			RETURN 111.8060
						CASE 5			RETURN 322.2000
						CASE 6			RETURN -38.1960
						CASE 7			RETURN 256.8040
						CASE 8			RETURN 330.6630
						CASE 9			RETURN 111.6060
						CASE 10			RETURN 183.6060
						CASE 11			RETURN 245.9200
					ENDSWITCH
				BREAK
				CASE GOS_GB_GRAPESEED_1
					SWITCH iProp
						CASE 0			RETURN 314.4000
						CASE 1			RETURN 224.5990
						CASE 2			RETURN 135.7990
						CASE 3			RETURN 134.9990
						CASE 4			RETURN 71.3370
						CASE 5			RETURN 86.9370
						CASE 6			RETURN 17.7370
						CASE 7			RETURN 330.7370
						CASE 8			RETURN 175.7370
						CASE 9			RETURN 97.1360
						CASE 10			RETURN 352.8760
						CASE 11			RETURN 231.6760
					ENDSWITCH
				BREAK
				CASE GOS_GB_EL_BURRO_HEIGHTS
					SWITCH iProp
						CASE 0			RETURN 233.5990
						CASE 1			RETURN 39.7990
						CASE 2			RETURN 130.1990
						CASE 3			RETURN 193.7990
						CASE 4			RETURN 70.5990
						CASE 5			RETURN 356.7990
						CASE 6			RETURN 136.3980
						CASE 7			RETURN 263.5980
						CASE 8			RETURN 60.0000
						CASE 9			RETURN 145.8000
						CASE 10			RETURN 349.2150
						CASE 11			RETURN 297.0150
					ENDSWITCH
				BREAK
				CASE GOS_GB_TERMINAL
					SWITCH iProp
						CASE 0			RETURN 0.0000
						CASE 1			RETURN 0.0000
						CASE 2			RETURN 0.0000
						CASE 3			RETURN 180.2000
						CASE 4			RETURN 61.8000
						CASE 5			RETURN 121.8000
						CASE 6			RETURN 355.1990
						CASE 7			RETURN 308.8000
						CASE 8			RETURN 67.8000
						CASE 9			RETURN 7.6000
						CASE 10			RETURN 269.5990
						CASE 11			RETURN 131.1990
					ENDSWITCH
				BREAK
				CASE GOS_GB_LSIA
					SWITCH iProp
						CASE 0			RETURN 250.9995
						CASE 1			RETURN 345.3991
						CASE 2			RETURN 65.7990
						CASE 3			RETURN 50.1990
						CASE 4			RETURN 12.7989
						CASE 5			RETURN 307.7989
						CASE 6			RETURN 358.3988
						CASE 7			RETURN 37.7290
						CASE 8			RETURN 100.5889
						CASE 9			RETURN 289.1330
						CASE 10			RETURN 243.9328
						CASE 11			RETURN 78.5323
					ENDSWITCH
				BREAK
				CASE GOS_GB_MURRIETA_HEIGHTS
					SWITCH iProp
						CASE 0			RETURN 154.3971
						CASE 1			RETURN 53.7992
						CASE 2			RETURN 69.5978
						CASE 3			RETURN 210.1976
						CASE 4			RETURN 28.3968
						CASE 5			RETURN 94.3969
						CASE 6			RETURN 349.1968
						CASE 7			RETURN 117.3958
						CASE 8			RETURN 279.1951
						CASE 9			RETURN 357.7953
						CASE 10			RETURN 52.3421
						CASE 11			RETURN 151.1967
					ENDSWITCH
				BREAK
				CASE GOS_GB_ELYSIAN_ISLAND_2
					SWITCH iProp
						CASE 0			RETURN 0.0000
						CASE 1			RETURN 0.0000
						CASE 2			RETURN 0.0000
						CASE 3			RETURN 0.0000
						CASE 4			RETURN 15.2000
						CASE 5			RETURN -27.4000
						CASE 6			RETURN 314.8000
						CASE 7			RETURN 332.6000
						CASE 8			RETURN 267.5990
						CASE 9			RETURN 332.7990
						CASE 10			RETURN 119.7990
						CASE 11			RETURN 203.3980
					ENDSWITCH
				BREAK
				CASE GOS_GB_PALETO_BAY
					SWITCH iProp
						CASE 0			RETURN 206.1490
						CASE 1			RETURN 297.0610
						CASE 2			RETURN 301.8040
						CASE 3			RETURN 121.7249
						CASE 4			RETURN 270.8039
						CASE 5			RETURN 273.4040
						CASE 6			RETURN 324.0040
						CASE 7			RETURN 39.2027
						CASE 8			RETURN 342.4027
						CASE 9			RETURN 303.0030
						CASE 10			RETURN 110.4017
						CASE 11			RETURN 57.0019
					ENDSWITCH
				BREAK
				CASE GOS_GB_FORT_ZANCUDO
					SWITCH iProp
						CASE 0			RETURN 237.9997
						CASE 1			RETURN 169.2718
						CASE 2			RETURN 84.5994
						CASE 3			RETURN 274.5987
						CASE 4			RETURN 127.9980
						CASE 5			RETURN 157.9975
						CASE 6			RETURN 72.3342
						CASE 7			RETURN 63.3338
						CASE 8			RETURN 9.7338
						CASE 9			RETURN 255.3332
						CASE 10			RETURN 108.8506
						CASE 11			RETURN 334.3329
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC VECTOR GET_GANGOPS_PROP_ROTATION(INT iProp, GANGOPS_VARIATION eVariation, GANGOPS_SUBVARIATION eSubvariation, INT iExtraParam = -1, HANGAR_ID eHangar = HANGAR_ID_INVALID, INT iRespawnLocation = 0)
	
	
	UNUSED_PARAMETER(iExtraParam)
	UNUSED_PARAMETER(eHangar)
	UNUSED_PARAMETER(iRespawnLocation)
	
	SWITCH eVariation
		
		CASE GOV_CAR_COLLECTOR
			SWITCH eSubvariation
				CASE GOS_CC_AMERICANCO_WAY
					SWITCH iProp
						CASE 1			RETURN <<0.0000, 0.0000, -101.3910>>
					ENDSWITCH
				BREAK
				CASE GOS_CC_HILLCREST_AVENUE
					SWITCH iProp
						CASE 1			RETURN <<0.0000, 0.0000, -11.8000>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE GOV_MOB_MENTALITY
			SWITCH eSubvariation
				CASE GOS_MM_MORNINGWOOD
					SWITCH iProp
						CASE 0			RETURN <<0.0000, 0.0000, -177.8003>>
					ENDSWITCH
				BREAK
				CASE GOS_MM_GRAPESEED
					SWITCH iProp
						CASE 0			RETURN <<0.0000, 0.0000, 5.1989>>
					ENDSWITCH
				BREAK
				CASE GOS_MM_ALTA
					SWITCH iProp
						CASE 0			RETURN <<0.0000, -0.0000, -101.6001>>
					ENDSWITCH
				BREAK
				CASE GOS_MM_MISSION_ROW
					SWITCH iProp
						CASE 0			RETURN <<0.0000, 0.0000, 31.7985>>
					ENDSWITCH
				BREAK
				CASE GOS_MM_HIPPY_CAMP
					SWITCH iProp
						CASE 0			RETURN <<0.0000, 0.0000, -15.4023>>
					ENDSWITCH
				BREAK
				CASE GOS_MM_SAN_ANDREAS
					SWITCH iProp
						CASE 0			RETURN <<0.0000, 0.0000, -54.8017>>
					ENDSWITCH
				BREAK
				CASE GOS_MM_MIRROR_PARK
					SWITCH iProp
						CASE 0			RETURN <<-29.5000, 89.9000, 85.8410>>
					ENDSWITCH
				BREAK
				CASE GOS_MM_STRAWBERRY
					SWITCH iProp
						CASE 0			RETURN <<0.0000, 0.0000, -129.8020>>
					ENDSWITCH
				BREAK
				CASE GOS_MM_MIRROR_PARK_TAVERN
					SWITCH iProp
						CASE 0			RETURN <<0.0000, 0.0000, 10.5960>>
					ENDSWITCH
				BREAK
				CASE GOS_MM_ULSA
					SWITCH iProp
						CASE 0			RETURN <<0.0000, -0.0000, 115.3983>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_AQUALUNGS
			SWITCH eSubvariation
				CASE GOS_AL_ELYSIAN_ISLAND_1
					SWITCH iProp
						CASE 5
						CASE 6
						CASE 0
							RETURN <<0.0000, 0.0000, 48.7992>>
						CASE 1			RETURN <<0.0000, -0.0000, 137.9989>>
						CASE 2			RETURN <<0.0000, 0.0000, 137.8990>>
						CASE 3			RETURN <<0.0000, 0.0000, 49.0000>>
						CASE 4			RETURN <<0.0000, 0.0000, 63.5958>>
					ENDSWITCH
				BREAK
				CASE GOS_AL_PALETO_BAY
					SWITCH iProp
						CASE 5
						CASE 6
						CASE 0
							RETURN <<0.0000, -0.0000, -142.6007>>
						CASE 1			RETURN <<0.0000, 0.0000, -52.8010>>
						CASE 2			RETURN <<0.0000, 0.0000, -52.8006>>
						CASE 3			RETURN <<0.0000, 0.0000, 37.3990>>
						CASE 4			RETURN <<1.2752, 0.5785, -1.6148>>
					ENDSWITCH
				BREAK
				CASE GOS_AL_ELYSIAN_ISLAND_2
					SWITCH iProp
						CASE 5
						CASE 6
						CASE 0
							RETURN <<0.0000, 0.0000, 133.0000>>
						CASE 1			RETURN <<0.0000, -0.0000, -137.3003>>
						CASE 2			RETURN <<0.0000, -0.0000, -135.2012>>
						CASE 3			RETURN <<0.0000, 0.0000, -53.4020>>
						CASE 4			RETURN <<0.0000, -0.0000, 174.7990>>
					ENDSWITCH
				BREAK
				CASE GOS_AL_PALOMINO_HIGHLANDS
					SWITCH iProp
						CASE 5
						CASE 6
						CASE 0
							RETURN <<0.0000, 0.0000, 110.5990>>
						CASE 1			RETURN <<0.0000, -0.0000, -154.8010>>
						CASE 2			RETURN <<0.0000, -0.0000, -159.3000>>
						CASE 3			RETURN <<0.0000, -0.0000, 114.8000>>
						CASE 4			RETURN <<0.0000, -0.0000, 159.3997>>
					ENDSWITCH
				BREAK
				CASE GOS_AL_CHUMASH
					SWITCH iProp
						CASE 5
						CASE 6
						CASE 0
							RETURN <<0.0000, 0.0000, -20.7001>>
						CASE 1			RETURN <<0.0000, 0.0000, 69.0000>>
						CASE 2			RETURN <<0.0000, 0.0000, 72.0999>>
						CASE 3			RETURN <<0.0000, -0.0000, 161.1000>>
						CASE 4			RETURN <<0.0000, 0.0000, -27.4000>>
					ENDSWITCH
				BREAK
				CASE GOS_AL_SAN_CHIANSKI_MOUNTAIN_RANGE
					SWITCH iProp
						CASE 5
						CASE 6
						CASE 0
							RETURN <<0.0000, -0.0000, -105.8000>>
						CASE 1			RETURN <<0.0000, -0.0000, 152.9997>>
						CASE 2			RETURN <<0.0000, 0.0000, -15.8010>>
						CASE 3			RETURN <<0.0000, -0.0000, -105.6010>>
						CASE 4			RETURN <<0.0000, 0.0000, 86.3996>>
					ENDSWITCH
				BREAK
				CASE GOS_AL_PALETO_FOREST
					SWITCH iProp
						CASE 5
						CASE 6
						CASE 0
							RETURN <<0.0000, 0.0000, 10.1995>>
						CASE 1			RETURN <<0.0000, 0.0000, -79.4000>>
						CASE 2			RETURN <<0.0000, -0.0000, 95.4000>>
						CASE 3			RETURN <<0.0000, -0.0000, -170.5000>>
						CASE 4			RETURN <<0.0000, 0.0000, 61.5001>>
					ENDSWITCH
				BREAK
				CASE GOS_AL_NORTH_CHUMASH
					SWITCH iProp
						CASE 5
						CASE 6
						CASE 0
							RETURN <<0.0000, 0.0000, -7.6000>>
						CASE 1			RETURN <<0.0000, 0.0000, -82.4999>>
						CASE 2			RETURN <<0.0000, -0.0000, -96.9010>>
						CASE 3			RETURN <<0.0000, -0.0000, 170.4991>>
						CASE 4			RETURN <<0.0000, -0.0000, 118.5997>>
					ENDSWITCH
				BREAK
				CASE GOS_AL_PACIFIC_BLUFFS
					SWITCH iProp
						CASE 5
						CASE 6
						CASE 0
							RETURN <<0.0000, 0.0000, 80.9000>>
						CASE 1			RETURN <<0.0000, -0.0000, 170.2000>>
						CASE 2			RETURN <<0.0000, -0.0000, 156.1000>>
						CASE 3			RETURN <<0.0000, 0.0000, 80.9599>>
						CASE 4			RETURN <<0.0000, -0.0000, 129.9996>>
					ENDSWITCH
				BREAK
				CASE GOS_AL_VESPUCCI_BEACH
					SWITCH iProp
						CASE 5
						CASE 6
						CASE 0
							RETURN <<0.0000, 0.0000, 15.2990>>
						CASE 1			RETURN <<0.0000, 0.0000, -74.8010>>
						CASE 2			RETURN <<0.0000, 0.0000, -61.7019>>
						CASE 3			RETURN <<0.0000, -0.0000, -166.0009>>
						CASE 4			RETURN <<0.0000, -0.0000, 167.5998>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_BURGLARY_JOB
			SWITCH iProp
				CASE 0		RETURN <<-0.0000, -0.0000, -0.2030>>
			ENDSWITCH
		BREAK
		
		CASE GOV_GONE_BALLISTIC
			SWITCH eSubvariation
				CASE GOS_GB_ELYSIAN_ISLAND_1
					SWITCH iProp
						CASE 0			RETURN <<0.0000, 0.0000, 88.9980>>
						CASE 1			RETURN <<0.0000, -0.0000, -125.2020>>
						CASE 2			RETURN <<0.0000, -0.0000, -124.8020>>
						CASE 3			RETURN <<0.0000, 0.0000, 89.5970>>
						CASE 4			RETURN <<0.0000, 0.0000, -152.0010>>
						CASE 5			RETURN <<0.0000, 0.0000, -35.2030>>
						CASE 6			RETURN <<0.0000, 0.0000, -65.8030>>
						CASE 7			RETURN <<0.0000, 0.0000, -27.8040>>
						CASE 8			RETURN <<0.0000, 0.0000, 34.1960>>
						CASE 9			RETURN <<0.0000, 0.0000, 144.1960>>
						CASE 10			RETURN <<0.0000, 0.0000, -25.8050>>
						CASE 11			RETURN <<0.0000, 0.0000, -158.8050>>
					ENDSWITCH
				BREAK
				CASE GOS_GB_GRAND_SENORA_DESERT_1
					SWITCH iProp
						CASE 0			RETURN <<-4.4003, 0.0000, -104.2000>>
						CASE 1			RETURN <<0.0000, -1.0001, 174.6000>>
						CASE 2			RETURN <<0.0000, -2.0001, -52.8000>>
						CASE 3			RETURN <<-4.7003, -3.4002, 21.6570>>
						CASE 4			RETURN <<0.0000, 0.0000, 111.8050>>
						CASE 5			RETURN <<-7.9530, -5.6880, -38.1960>>
						CASE 6			RETURN <<0.0000, 0.0000, -38.1960>>
						CASE 7			RETURN <<3.1160, -2.1690, -103.1370>>
						CASE 8			RETURN <<-3.1470, -2.0670, -29.3940>>
						CASE 9			RETURN <<0.0000, 0.0000, 111.6060>>
						CASE 10			RETURN <<2.9770, 3.2910, -176.4800>>
						CASE 11			RETURN <<4.1550, -6.2800, -113.8520>>
					ENDSWITCH
				BREAK
				CASE GOS_GB_GRAPESEED_1
					SWITCH iProp
						CASE 0			RETURN <<0.0000, 0.0000, -45.6000>>
						CASE 1			RETURN <<-0.5090, 4.0130, -135.3830>>
						CASE 2			RETURN <<0.0000, 0.0000, 135.7990>>
						CASE 3			RETURN <<-4.1100, -0.9710, 134.9640>>
						CASE 4			RETURN <<0.0000, 0.0000, 71.3370>>
						CASE 5			RETURN <<0.0000, 0.0000, 86.9370>>
						CASE 6			RETURN <<0.0000, 0.0000, 17.7370>>
						CASE 7			RETURN <<0.0000, 0.0000, 144.7990>>
						CASE 8			RETURN <<0.0000, 0.0000, 175.7370>>
						CASE 9			RETURN <<0.0000, -0.0000, 104.3760>>
						CASE 10			RETURN <<0.0000, 0.0000, -7.1230>>
						CASE 11			RETURN <<-0.0000, -0.0000, -77.9240>>
					ENDSWITCH
				BREAK
				CASE GOS_GB_EL_BURRO_HEIGHTS
					SWITCH iProp
						CASE 0			RETURN <<4.6000, 3.2000, -126.4010>>
						CASE 1			RETURN <<-1.6000, -4.3000, 39.7990>>
						CASE 2			RETURN <<-0.9000, 0.0000, 130.1990>>
						CASE 3			RETURN <<0.0000, 3.3000, -166.2010>>
						CASE 4			RETURN <<0.0000, 0.0000, 70.5990>>
						CASE 5			RETURN <<0.0000, 0.0000, -3.2010>>
						CASE 6			RETURN <<0.0000, 0.0000, 136.3980>>
						CASE 7			RETURN <<0.0000, 0.0000, -96.4020>>
						CASE 8			RETURN <<0.0000, 0.0000, 60.0000>>
						CASE 9			RETURN <<-3.6470, -5.7860, 145.6150>>
						CASE 10			RETURN <<0.0000, 0.0000, -10.7850>>
						CASE 11			RETURN <<3.4650, -1.9130, -62.9270>>
					ENDSWITCH
				BREAK
				CASE GOS_GB_TERMINAL
					SWITCH iProp
						CASE 0			RETURN <<0.0000, 0.0000, 89.8000>>
						CASE 1			RETURN <<0.0000, -0.0000, 179.8990>>
						CASE 2			RETURN <<0.0000, 0.0000, 89.8000>>
						CASE 3			RETURN <<-1.0001, -0.0000, -179.8000>>
						CASE 4			RETURN <<-0.0000, -0.0000, -0.0010>>
						CASE 5			RETURN <<0.0000, -0.0000, 179.8990>>
						CASE 6			RETURN <<0.0000, -0.0000, 179.8990>>
						CASE 7			RETURN <<0.0000, -0.0000, 179.1990>>
						CASE 8			RETURN <<0.0000, 0.0000, 48.5990>>
						CASE 9			RETURN <<-0.0000, -0.0000, -27.0010>>
						CASE 10			RETURN <<-0.0000, -0.0000, -139.0009>>
						CASE 11			RETURN <<0.0000, -0.0000, 143.3990>>
					ENDSWITCH
				BREAK
				CASE GOS_GB_LSIA
					SWITCH iProp
						CASE 0			RETURN <<4.5000, -0.0000, -109.0005>>
						CASE 1			RETURN <<0.9000, 0.9000, -14.6009>>
						CASE 2			RETURN <<-0.3000, -0.0000, 65.7990>>
						CASE 3			RETURN <<-1.5001, -0.8001, 50.1990>>
						CASE 4			RETURN <<0.0000, 0.0000, 12.7989>>
						CASE 5			RETURN <<0.0000, 0.0000, -52.2011>>
						CASE 6			RETURN <<0.0000, 0.0000, -1.6012>>
						CASE 7			RETURN <<-4.7648, -3.3629, 37.5890>>
						CASE 8			RETURN <<-4.6674, 3.5436, 100.7333>>
						CASE 9			RETURN <<0.0000, 0.0000, -70.8670>>
						CASE 10			RETURN <<0.0000, -0.0000, -116.0672>>
						CASE 11			RETURN <<-6.3492, -0.2678, 78.5174>>
					ENDSWITCH
				BREAK
				CASE GOS_GB_MURRIETA_HEIGHTS
					SWITCH iProp
						CASE 0			RETURN <<0.8000, 0.9000, 154.3971>>
						CASE 1			RETURN <<0.0000, 0.0000, 53.7992>>
						CASE 2			RETURN <<0.0000, 0.0000, 69.5978>>
						CASE 3			RETURN <<3.6000, 0.0000, -149.8024>>
						CASE 4			RETURN <<0.0000, 0.0000, 28.3968>>
						CASE 5			RETURN <<0.0000, -0.0000, 94.3969>>
						CASE 6			RETURN <<0.0000, 0.0000, -10.8032>>
						CASE 7			RETURN <<0.0000, -0.0000, 117.3958>>
						CASE 8			RETURN <<0.0000, 0.0000, -80.8049>>
						CASE 9			RETURN <<-1.3147, -4.6230, -2.2578>>
						CASE 10			RETURN <<-3.6767, 1.9743, 52.4055>>
						CASE 11			RETURN <<0.0000, -0.0000, 151.1967>>
					ENDSWITCH
				BREAK
				CASE GOS_GB_ELYSIAN_ISLAND_2
					SWITCH iProp
						CASE 0			RETURN <<0.0000, -0.0000, 91.6000>>
						CASE 1			RETURN <<0.0000, -0.0000, -133.9000>>
						CASE 2			RETURN <<0.0000, -0.0000, 92.2500>>
						CASE 3			RETURN <<0.0000, 0.0000, -45.6000>>
						CASE 4			RETURN <<0.0000, 0.0000, 15.2000>>
						CASE 5			RETURN <<0.0000, 0.0000, -179.3500>>
						CASE 6			RETURN <<0.0000, 0.0000, -45.2000>>
						CASE 7			RETURN <<0.0000, 0.0000, -27.4000>>
						CASE 8			RETURN <<0.0000, 0.0000, -118.3000>>
						CASE 9			RETURN <<0.0000, 0.0000, 0.0000>>
						CASE 10			RETURN <<0.0000, 0.0000, 50.8000>>
						CASE 11			RETURN <<0.0000, 0.0000, -142.5000>>
					ENDSWITCH
				BREAK
				CASE GOS_GB_PALETO_BAY
					SWITCH iProp
						CASE 0			RETURN <<-5.2520, -2.2121, -153.8510>>
						CASE 1			RETURN <<5.7780, 1.6990, -64.8250>>
						CASE 2			RETURN <<0.8000, -0.6000, -58.1960>>
						CASE 3			RETURN <<-2.6030, -2.0119, 121.7249>>
						CASE 4			RETURN <<0.4939, 6.8860, -89.2258>>
						CASE 5			RETURN <<0.0000, 0.0000, -86.5960>>
						CASE 6			RETURN <<-0.7548, 6.7688, -138.6517>>
						CASE 7			RETURN <<0.0000, 0.0000, 39.2027>>
						CASE 8			RETURN <<0.0000, 0.0000, -17.5973>>
						CASE 9			RETURN <<-0.7548, 6.7688, -64.0517>>
						CASE 10			RETURN <<-4.1559, -1.3016, 110.3544>>
						CASE 11			RETURN <<3.8530, -6.8263, 57.2318>>
					ENDSWITCH
				BREAK
				CASE GOS_GB_FORT_ZANCUDO
					SWITCH iProp
						CASE 0			RETURN <<3.0000, -0.7001, -122.0002>>
						CASE 1			RETURN <<0.9000, 0.0000, 169.2718>>
						CASE 2			RETURN <<-3.4002, -0.2000, 84.5994>>
						CASE 3			RETURN <<-1.6001, -2.6002, -85.4013>>
						CASE 4			RETURN <<0.0000, -0.0000, 127.9980>>
						CASE 5			RETURN <<0.0000, -0.0000, 157.9975>>
						CASE 6			RETURN <<0.0000, 0.0000, 72.3342>>
						CASE 7			RETURN <<-6.3589, -0.0000, 63.3338>>
						CASE 8			RETURN <<0.0000, 0.0000, 9.7338>>
						CASE 9			RETURN <<0.0000, -0.0000, -104.6668>>
						CASE 10			RETURN <<0.0000, -0.0000, 108.8506>>
						CASE 11			RETURN <<0.0000, 0.0000, -25.6670>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC VECTOR GET_SUPPORT_VEHICLE_SPAWN_COORDS(DEFUNCT_BASE_ID defunctBaseId, INT iVehicle, MODEL_NAMES eModel, BOOL bAlternateMissionCondition = FALSE, GANGOPS_LOCATION eLocation = GangOpsLocation_Max)

	// Specific locations have specific coords for spawning support vehicles
	SWITCH eLocation
		// Aqualungs
		CASE GangOpsLocation_Aqualungs_Elysian_Island_1
			IF bAlternateMissionCondition
				SWITCH iVehicle
					CASE 0			RETURN <<-423.8374, -2859.1975, 5.0004>>
					CASE 1			RETURN <<-421.2206, -2859.1670, 5.0004>>
					CASE 2			RETURN <<-418.5291, -2859.1877, 5.0004>>
					CASE 3			RETURN <<-415.7802, -2859.1985, 5.0004>>
				ENDSWITCH
			ELSE
				SWITCH iVehicle
					CASE 0			RETURN <<-423.8374, -2865.1975, 0.0004>>
					CASE 1			RETURN <<-421.2206, -2865.1670, 0.0004>>
					CASE 2			RETURN <<-418.5291, -2865.1877, 0.0004>>
					CASE 3			RETURN <<-415.7802, -2865.1985, 0.0004>>
				ENDSWITCH
			ENDIF
			RETURN <<0.0, 0.0, 0.0>>
		BREAK
		CASE GangOpsLocation_Aqualungs_Paleto_Bay
			IF bAlternateMissionCondition
				SWITCH iVehicle
					CASE 0			RETURN <<463.8748, 6757.4517, 0.4600>>
					CASE 1			RETURN <<466.7859, 6755.8711, 0.2015>>
					CASE 2			RETURN <<455.0936, 6762.2227, 1.1764>>
					CASE 3			RETURN <<451.7273, 6763.0513, 1.6609>>
				ENDSWITCH
			ELSE
				SWITCH iVehicle
					CASE 0			RETURN <<476.6718, 6786.1416, 0.3751>>
					CASE 1			RETURN <<474.1923, 6791.5342, 0.5625>>
					CASE 2			RETURN <<485.8094, 6775.4775, -0.1875>>
					CASE 3			RETURN <<490.3120, 6776.5957, -0.1877>>
				ENDSWITCH
			ENDIF
			RETURN <<0.0, 0.0, 0.0>>
		BREAK
		CASE GangOpsLocation_Aqualungs_Elysian_Island_2
			IF bAlternateMissionCondition
				SWITCH iVehicle
					CASE 0			RETURN <<619.8850, -3202.0339, 5.0690>>
					CASE 1			RETURN <<618.7970, -3204.4490, 5.0880>>
					CASE 2			RETURN <<606.8647, -3216.9983, 5.0695>>
					CASE 3			RETURN <<606.9007, -3214.6355, 5.0695>>
				ENDSWITCH
			ELSE
				SWITCH iVehicle
					CASE 0			RETURN <<625.7360, -3197.7849, 0.9380>>
					CASE 1			RETURN <<615.8480, -3209.5481, 0.0020>>
					CASE 2			RETURN <<624.8440, -3202.5139, -0.1880>>
					CASE 3			RETURN <<611.6400, -3209.4700, -0.9000>>
				ENDSWITCH
			ENDIF
			RETURN <<0.0, 0.0, 0.0>>
		BREAK
		CASE GangOpsLocation_Aqualungs_Palomino_Highlands
			IF bAlternateMissionCondition
				SWITCH iVehicle
					CASE 0			RETURN <<2815.8662, -812.1984, 4.1898>>
					CASE 1			RETURN <<2819.1538, -813.1436, 3.6371>>
					CASE 2			RETURN <<2797.6858, -817.0100, 6.6525>>
					CASE 3			RETURN <<2797.5042, -820.7687, 6.3327>>
				ENDSWITCH
			ELSE
				SWITCH iVehicle
					CASE 0			RETURN <<2838.7192, -828.9978, 0.9375>>
					CASE 1			RETURN <<2837.0049, -833.1832, 0.7500>>
					CASE 2			RETURN <<2822.0002, -840.8355, 0.7500>>
					CASE 3			RETURN <<2818.0149, -844.1680, 0.5625>>
				ENDSWITCH
			ENDIF
			RETURN <<0.0, 0.0, 0.0>>
		BREAK
		CASE GangOpsLocation_Aqualungs_Chumash
			IF bAlternateMissionCondition
				SWITCH iVehicle
					CASE 0			RETURN <<-3251.1777, 1216.0575, 1.4837>>
					CASE 1			RETURN <<-3249.1624, 1213.6649, 1.5022>>
					CASE 2			RETURN <<-3250.7424, 1202.8077, 1.6326>>
					CASE 3			RETURN <<-3255.0601, 1203.6149, 1.4873>>
				ENDSWITCH
			ELSE
				SWITCH iVehicle
					CASE 0			RETURN <<-3297.5010, 1203.3240, 0.0570>>
					CASE 1			RETURN <<-3299.7390, 1206.9463, 1.1249>>
					CASE 2			RETURN <<-3293.5212, 1221.6010, 1.0616>>
					CASE 3			RETURN <<-3291.9504, 1225.5510, 1.0136>>
				ENDSWITCH
			ENDIF
			RETURN <<0.0, 0.0, 0.0>>
		BREAK
		CASE GangOpsLocation_Aqualungs_San_Chianksi_Mountain_Range
			IF bAlternateMissionCondition
				SWITCH iVehicle
					CASE 0			RETURN <<3640.7844, 4677.4243, 1.7823>>
					CASE 1			RETURN <<3643.1821, 4680.2007, 1.6213>>
					CASE 2			RETURN <<3637.4658, 4691.1914, 2.0614>>
					CASE 3			RETURN <<3635.5813, 4693.9883, 1.9637>>
				ENDSWITCH
			ELSE
				SWITCH iVehicle
					CASE 0			RETURN <<3650.3049, 4720.4150, -0.0001>>
					CASE 1			RETURN <<3654.6963, 4718.9551, 0.0000>>
					CASE 2			RETURN <<3653.1509, 4698.6245, 0.1875>>
					CASE 3			RETURN <<3654.3962, 4695.0371, 0.1875>>
				ENDSWITCH
			ENDIF
			RETURN <<0.0, 0.0, 0.0>>
		BREAK
		CASE GangOpsLocation_Aqualungs_PaletoForest
			IF bAlternateMissionCondition
				SWITCH iVehicle
					CASE 0			RETURN <<-883.3495, 5777.8809, 3.2802>>
					CASE 1			RETURN <<-880.4175, 5779.7632, 3.1555>>
					CASE 2			RETURN <<-886.0445, 5765.7710, 3.6382>>
					CASE 3			RETURN <<-886.5935, 5768.4736, 3.6525>>
				ENDSWITCH
			ELSE
				SWITCH iVehicle
					CASE 0			RETURN <<-927.8899, 5760.8813, 0.3750>>
					CASE 1			RETURN <<-930.6428, 5764.5195, 0.3750>>
					CASE 2			RETURN <<-922.9142, 5792.4229, 0.3750>>
					CASE 3			RETURN <<-919.9430, 5795.8901, 0.3750>>
				ENDSWITCH
			ENDIF
			RETURN <<0.0, 0.0, 0.0>>
		BREAK
		CASE GangOpsLocation_Aqualungs_North_Chumash
			IF bAlternateMissionCondition
				SWITCH iVehicle
					CASE 0			RETURN <<-2478.4771, 4003.6394, 6.1776>>
					CASE 1			RETURN <<-2475.0972, 4003.9807, 6.6908>>
					CASE 2			RETURN <<-2477.5305, 4015.4702, 6.2698>>
					CASE 3			RETURN <<-2479.1113, 4018.8325, 6.0387>>
				ENDSWITCH
			ELSE
				SWITCH iVehicle
					CASE 0			RETURN <<-2543.8643, 4020.9487, 0.1874>>
					CASE 1			RETURN <<-2545.8369, 4026.1616, 0.1875>>
					CASE 2			RETURN <<-2547.9951, 4040.1460, 0.3750>>
					CASE 3			RETURN <<-2543.2793, 4042.6423, 0.3749>>
				ENDSWITCH
			ENDIF
			RETURN <<0.0, 0.0, 0.0>>
		BREAK
		CASE GangOpsLocation_Aqualungs_Pacific_Bluffs
			IF bAlternateMissionCondition
				SWITCH iVehicle
					CASE 0			RETURN <<-2009.5830, -559.2603, 10.0707>>
					CASE 1			RETURN <<-2009.9044, -556.5217, 9.9174>>
					CASE 2			RETURN <<-2003.6576, -560.7831, 10.1805>>
					CASE 3			RETURN <<-2000.3269, -559.8959, 10.3914>>
				ENDSWITCH
			ELSE
				SWITCH iVehicle
					CASE 0			RETURN <<-2074.9312, -635.8578, 0.5625>>
					CASE 1			RETURN <<-2070.4932, -635.0846, 0.3750>>
					CASE 2			RETURN <<-2087.1267, -620.2362, 0.3750>>
					CASE 3			RETURN <<-2087.1086, -616.1238, 0.3750>>
				ENDSWITCH
			ENDIF
			RETURN <<0.0, 0.0, 0.0>>
		BREAK
		CASE GangOpsLocation_Aqualungs_Vespucci_Beach
			IF bAlternateMissionCondition
				SWITCH iVehicle
					CASE 0			RETURN <<-1376.6036, -1626.0150, 1.1489>>
					CASE 1			RETURN <<-1376.5527, -1623.0808, 1.1503>>
					CASE 2			RETURN <<-1371.2552, -1628.4272, 1.1458>>
					CASE 3			RETURN <<-1367.7124, -1627.2681, 1.1368>>
				ENDSWITCH
			ELSE
				SWITCH iVehicle
					CASE 0			RETURN <<-1403.9919, -1648.0687, -0.1875>>
					CASE 1			RETURN <<-1399.9545, -1648.7898, -0.1875>>
					CASE 2			RETURN <<-1413.8413, -1637.5532, -0.1875>>
					CASE 3			RETURN <<-1412.1729, -1633.3295, -0.1875>>	
				ENDSWITCH
			ENDIF
			RETURN <<0.0, 0.0, 0.0>>
		BREAK
		
		CASE GangOpsLocation_UnderControl_ServerRoom1			
		CASE GangOpsLocation_UnderControl_ServerRoom2			
		CASE GangOpsLocation_UnderControl_ServerRoom3			
		CASE GangOpsLocation_UnderControl_ServerRoom4			
		CASE GangOpsLocation_UnderControl_ServerRoom5			
		CASE GangOpsLocation_UnderControl_ControlRoom1			
		CASE GangOpsLocation_UnderControl_ControlRoom2			
		CASE GangOpsLocation_UnderControl_ControlRoom3			
		CASE GangOpsLocation_UnderControl_ControlRoom4			
		CASE GangOpsLocation_UnderControl_ControlRoom5
			SWITCH iVehicle
				CASE 0			RETURN <<-2364.3950, 3351.1047, 31.8329>>
				CASE 1			RETURN <<-2316.0933, 3353.1084, 31.8368>>
				CASE 2			RETURN <<-2334.4307, 3363.3738, 31.8321>>
				CASE 3			RETURN <<-2353.2769, 3374.9424, 31.8329>>
				CASE 4			RETURN <<-2372.0039, 3385.2944, 31.8329>>
				CASE 5			RETURN <<-2224.3394, 3248.8999, 31.8102>>
				CASE 6			RETURN <<-2238.1506, 3223.9824, 31.8102>>
				CASE 7			RETURN <<-2251.9897, 3200.8274, 31.8100>>
				CASE 8			RETURN <<-2262.2456, 3183.0215, 31.8100>>
				CASE 9			RETURN <<-2383.7446, 3361.9934, 31.8329>>
			ENDSWITCH
			RETURN <<0.0, 0.0, 0.0>>
		BREAK 
	ENDSWITCH
	
	// Generic base spawns
	SWITCH defunctBaseId
		CASE DEFUNCT_BASE_1
			SWITCH eModel
				CASE TULA
					SWITCH iVehicle
						CASE 0		RETURN <<1225.9397, 2861.9041, 45.5514>>
						CASE 1		RETURN <<1232.9038, 2876.4336, 45.4660>>
						CASE 2		RETURN <<1236.1194, 2892.7422, 45.1626>>
						CASE 3		RETURN <<1262.8979, 2795.1050, 48.0661>>
						CASE 4		RETURN <<1282.3485, 2792.1497, 46.8078>>
					ENDSWITCH
				BREAK
				CASE OPPRESSOR
					SWITCH iVehicle
						CASE 0		RETURN <<1247.1633, 2842.9548, 44.7118>>
						CASE 1		RETURN <<1245.0547, 2845.8054, 45.2592>>
						CASE 2		RETURN <<1249.6215, 2846.4709, 44.8548>>
						CASE 3		RETURN <<1248.1866, 2849.1777, 45.3386>>
						CASE 4		RETURN <<1253.8491, 2847.4048, 44.0806>>
						CASE 5		RETURN <<1251.9252, 2851.6748, 45.2005>>
						CASE 6		RETURN <<1257.0953, 2851.1208, 44.0930>>
						CASE 7		RETURN <<1255.5898, 2854.8884, 45.0437>>
						CASE 8		RETURN <<1260.1263, 2854.6992, 44.4052>>
						CASE 9		RETURN <<1258.3550, 2859.4226, 45.3958>>
						CASE 10		RETURN <<1265.0251, 2857.1978, 44.2385>>
						CASE 11		RETURN <<1261.8894, 2859.1277, 45.1001>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE DEFUNCT_BASE_2
			SWITCH eModel
				CASE TULA
					SWITCH iVehicle
						CASE 0		RETURN <<-4.9504, 2620.8850, 82.5306>> 
						CASE 1		RETURN <<-15.8412, 2635.1802, 80.0948>>
						CASE 2		RETURN <<7.6901, 2629.6873, 82.5015>>
						CASE 3		RETURN <<-3.3683, 2644.2292, 80.4186>> 
						CASE 4		RETURN <<-26.9259, 2648.5017, 76.6610>>
					ENDSWITCH
				BREAK
				CASE OPPRESSOR
					SWITCH iVehicle
						CASE 0		RETURN <<20.0980, 2634.1633, 82.5833>>
						CASE 1		RETURN <<19.9318, 2630.0208, 83.5529>>
						CASE 2		RETURN <<25.0468, 2637.7363, 82.3885>>
						CASE 3		RETURN <<25.1131, 2634.2144, 82.9902>>
						CASE 4		RETURN <<30.5417, 2639.5981, 81.9888>>
						CASE 5		RETURN <<30.0813, 2644.2024, 81.4394>>
						CASE 6		RETURN <<35.0327, 2642.9604, 81.6115>>
						CASE 7		RETURN <<34.3168, 2647.3313, 81.1033>>
						CASE 8		RETURN <<24.5269, 2643.0581, 81.5380>>
						CASE 9		RETURN <<22.0871, 2647.2690, 80.8546>>
						CASE 10		RETURN <<26.8432, 2648.1311, 80.9469>>
						CASE 11		RETURN <<25.5972, 2652.4468, 80.3968>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE DEFUNCT_BASE_3
			SWITCH eModel
				CASE TULA
					SWITCH iVehicle
						CASE 0		RETURN <<2811.7729, 3876.9749, 48.6616>>
						CASE 1		RETURN <<2821.5181, 3862.3057, 47.9400>>
						CASE 2		RETURN <<2703.4556, 3884.1975, 42.4539>>
						CASE 3		RETURN <<2688.8149, 3894.3452, 41.4574>>
						CASE 4		RETURN <<2831.3638, 3875.3494, 48.0681>>
					ENDSWITCH
				BREAK
				CASE OPPRESSOR
					SWITCH iVehicle
						CASE 0		RETURN <<2745.5454, 3922.4839, 42.9769>>
						CASE 1		RETURN <<2749.0266, 3924.3103, 42.8873>>
						CASE 2		RETURN <<2745.0898, 3926.4060, 43.4593>>
						CASE 3		RETURN <<2749.2861, 3928.2615, 43.3985>>
						CASE 4		RETURN <<2741.3000, 3917.0210, 42.5636>>
						CASE 5		RETURN <<2742.0566, 3922.6265, 43.2151>>
						CASE 6		RETURN <<2736.9922, 3918.4868, 42.8330>>
						CASE 7		RETURN <<2737.1265, 3924.5144, 43.0257>>
						CASE 8		RETURN <<2740.9204, 3927.9111, 43.4918>>
						CASE 9		RETURN <<2741.2576, 3932.7273, 43.9494>>
						CASE 10		RETURN <<2745.7024, 3931.0767, 43.9394>>
						CASE 11		RETURN <<2736.2388, 3929.2795, 43.3168>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE DEFUNCT_BASE_4
			SWITCH eModel
				CASE TULA
					SWITCH iVehicle
						CASE 0		RETURN <<3380.5833, 5446.5215, 14.6018>>
						CASE 1		RETURN <<3368.5691, 5459.4707, 16.1566>>
						CASE 2		RETURN <<3328.9241, 5431.9902, 18.1067>>
						CASE 3		RETURN <<3316.8293, 5418.0708, 16.4870>>
						CASE 4		RETURN <<3297.3586, 5406.4751, 15.1589>>
					ENDSWITCH
				BREAK
				CASE OPPRESSOR
					SWITCH iVehicle
						CASE 0		RETURN <<3352.7456, 5520.7622, 17.2101>>
						CASE 1		RETURN <<3350.0044, 5523.9331, 17.7138>>
						CASE 2		RETURN <<3355.6440, 5523.1074, 16.3747>>
						CASE 3		RETURN <<3353.1028, 5526.1343, 16.6763>>
						CASE 4		RETURN <<3356.3057, 5528.6250, 15.8013>>
						CASE 5		RETURN <<3353.2207, 5531.7563, 16.4328>>
						CASE 6		RETURN <<3349.2334, 5533.8110, 17.2334>>
						CASE 7		RETURN <<3349.9482, 5528.7910, 17.3835>>
						CASE 8		RETURN <<3357.7388, 5533.6035, 15.2242>>
						CASE 9		RETURN <<3354.6296, 5536.6450, 15.9099>>
						CASE 10		RETURN <<3361.8655, 5535.7544, 14.7272>>
						CASE 11		RETURN <<3359.1543, 5540.3232, 15.1184>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
//		CASE DEFUNCT_BASE_5
//			SWITCH eModel
//				CASE TULA
//					SWITCH iVehicle
//						CASE 0		RETURN <<-808.6589, 5633.1553, 23.3701>>
//						CASE 1		RETURN <<-813.0442, 5615.6973, 23.9570>>
//						CASE 2		RETURN <<-817.1557, 5597.9497, 24.9756>>
//						CASE 3		RETURN <<-783.7012, 5671.3765, 22.6341>>
//						CASE 4		RETURN <<-786.6047, 5687.4736, 21.4707>>
//					ENDSWITCH
//				BREAK
//				CASE OPPRESSOR
//					SWITCH iVehicle
//						CASE 0		RETURN <<-818.4761, 5652.1484, 22.0148>>
//						CASE 1		RETURN <<-818.2706, 5647.4922, 22.0541>>
//						CASE 2		RETURN <<-822.9063, 5654.4302, 21.4334>>
//						CASE 3		RETURN <<-821.6710, 5649.3540, 21.5467>>
//						CASE 4		RETURN <<-813.5252, 5650.3633, 22.6384>>
//						CASE 5		RETURN <<-814.0875, 5645.7344, 22.5450>>
//						CASE 6		RETURN <<-809.3096, 5647.3687, 23.1340>>
//						CASE 7		RETURN <<-810.5007, 5642.4648, 22.9342>>
//						CASE 8		RETURN <<-817.0024, 5642.6401, 21.9875>>
//						CASE 9		RETURN <<-821.9719, 5644.8877, 21.3167>>
//						CASE 10		RETURN <<-821.4977, 5640.0117, 21.2685>>
//						CASE 11		RETURN <<-817.8392, 5638.0728, 21.7177>>
//					ENDSWITCH
//				BREAK
//			ENDSWITCH
//		BREAK
		
		CASE DEFUNCT_BASE_6
			SWITCH eModel
				CASE TULA
					SWITCH iVehicle
						CASE 0		RETURN <<26.3475, 6855.6157, 12.3238>> 
						CASE 1		RETURN <<7.9770, 6860.8120, 11.8330>>
						CASE 2		RETURN <<-7.4510, 6867.7651, 11.9590>> 
						CASE 3		RETURN <<-20.4730, 6853.7441, 12.5720>>
						CASE 4		RETURN <<-27.9550, 6837.2808, 12.4790>>
					ENDSWITCH
				BREAK
				CASE OPPRESSOR
					SWITCH iVehicle
						CASE 0		RETURN <<22.7110, 6812.0840, 14.5380>>
						CASE 1		RETURN <<24.4830, 6809.3740, 15.6800>>
						CASE 2		RETURN <<18.5460, 6809.9502, 15.1380>>
						CASE 3		RETURN <<20.4810, 6807.7939, 16.0330>>
						CASE 4		RETURN <<30.8330, 6843.3008, 12.7780>>
						CASE 5		RETURN <<26.0360, 6846.3652, 12.5370>>
						CASE 6		RETURN <<26.2440, 6841.2412, 12.7710>>
						CASE 7		RETURN <<21.0430, 6843.5439, 12.8790>>
						CASE 8		RETURN <<17.9030, 6848.5308, 12.3360>>
						CASE 9		RETURN <<15.2030, 6844.4448, 12.9440>>
						CASE 10		RETURN <<13.2180, 6850.3740, 12.3040>>
						CASE 11		RETURN <<10.4960, 6846.3169, 12.8560>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE DEFUNCT_BASE_7
			SWITCH eModel
				CASE TULA
					SWITCH iVehicle
						CASE 0		RETURN <<-2300.2390, 2427.7947, 1.9265>>
						CASE 1		RETURN <<-2297.9980, 2444.2808, 1.9977>>
						CASE 2		RETURN <<-2293.4648, 2460.6865, 2.0462>>
						CASE 3		RETURN <<-2292.6111, 2479.5242, 2.0465>>
						CASE 4		RETURN <<-2279.0149, 2491.7515, 2.0531>>
					ENDSWITCH
				BREAK
				CASE OPPRESSOR
					SWITCH iVehicle
						CASE 0		RETURN <<-2243.1479, 2403.9736, 10.4634>>
						CASE 1		RETURN <<-2245.6833, 2400.0459, 9.7451>> 
						CASE 2		RETURN <<-2246.2336, 2406.0500, 9.8220>> 
						CASE 3		RETURN <<-2249.1191, 2403.0232, 8.9188>> 
						CASE 4		RETURN <<-2246.4858, 2411.0684, 9.8766>>
						CASE 5		RETURN <<-2249.8689, 2409.0161, 8.7791>>
						CASE 6		RETURN <<-2248.2974, 2415.4976, 9.2995>>
						CASE 7		RETURN <<-2251.8145, 2413.4473, 7.9651>>
						CASE 8		RETURN <<-2252.0344, 2418.7803, 7.7839>>
						CASE 9		RETURN <<-2255.4011, 2415.4695, 6.6919>>
						CASE 10		RETURN <<-2254.6646, 2422.9321, 6.7931>>
						CASE 11		RETURN <<-2257.8164, 2419.6531, 5.8798>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE DEFUNCT_BASE_8
			SWITCH eModel
				CASE TULA
					SWITCH iVehicle
						CASE 0		RETURN <<66.8100, 3307.9500, 35.3698>>
						CASE 1		RETURN <<52.9644, 3296.6772, 36.5062>>
						CASE 2		RETURN <<38.9166, 3283.3323, 38.6251>>
						CASE 3		RETURN <<28.2837, 3270.4517, 39.9721>>
						CASE 4		RETURN <<85.3236, 3321.3071, 33.5763>>
					ENDSWITCH
				BREAK
				
				CASE OPPRESSOR
					SWITCH iVehicle
						CASE 0		RETURN <<24.1138, 3325.6384, 37.2005>>
						CASE 1		RETURN <<23.3991, 3321.8286, 37.4353>>
						CASE 2		RETURN <<20.5356, 3326.6670, 37.2042>>
						CASE 3		RETURN <<22.3561, 3330.0457, 36.9536>>
						CASE 4		RETURN	<<19.6223, 3320.7119, 37.6521>>
						CASE 5		RETURN	<<18.4946, 3316.9927, 37.8867>>
						CASE 6		RETURN	<<16.8322, 3324.0056, 37.6872>>
						CASE 7		RETURN	<<14.5595, 3320.3501, 38.2498>>
						CASE 8		RETURN	<<14.8082, 3328.7766, 37.7463>>
						CASE 9		RETURN	<<11.8810, 3323.8928, 38.6714>>
						CASE 10		RETURN	<<10.1371, 3329.2827, 38.8416>>
						CASE 11		RETURN	<<8.7064, 3325.4153, 39.3235>> 
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE DEFUNCT_BASE_9
			SWITCH eModel
				CASE TULA
					SWITCH iVehicle
						CASE 0		RETURN <<2125.9971, 1781.3530, 102.3210>>
						CASE 1		RETURN <<2141.3560, 1788.8459, 103.3470>>
						CASE 2		RETURN <<2157.1201, 1795.2419, 105.1060>>
						CASE 3		RETURN <<2125.7720, 1798.5110, 102.2590>>
						CASE 4		RETURN <<2172.4900, 1804.4580, 106.2240>>
					ENDSWITCH
				BREAK
				
				CASE OPPRESSOR
					SWITCH iVehicle
						CASE 0		RETURN <<2114.7480, 1767.3816, 101.9113>>
						CASE 1		RETURN <<2113.6970, 1773.0791, 101.5352>>
						CASE 2		RETURN <<2111.2070, 1770.4865, 101.4658>>
						CASE 3		RETURN <<2117.1687, 1769.7833, 101.9315>>
						CASE 4		RETURN <<2109.8386, 1776.6978, 101.1374>>
						CASE 5		RETURN <<2107.4497, 1773.7839, 100.9482>>
						CASE 6		RETURN <<2106.7781, 1779.3781, 100.6955>>
						CASE 7		RETURN <<2103.9204, 1776.3718, 100.8128>>
						CASE 8		RETURN <<2119.6433, 1772.3079, 101.9507>>
						CASE 9		RETURN <<2122.8889, 1775.3170, 102.0133>>
						CASE 10		RETURN <<2117.3452, 1776.7710, 101.8669>>
						CASE 11		RETURN <<2119.9651, 1779.3188, 102.0275>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE DEFUNCT_BASE_10
			SWITCH eModel
				CASE TULA
					SWITCH iVehicle
						CASE 0		RETURN <<1885.2330, 220.5869, 160.5265>>
						CASE 1		RETURN <<1904.2603, 224.3585, 160.3390>>
						CASE 2		RETURN <<1890.4218, 235.2156, 160.7140>>
						CASE 3		RETURN <<1909.3990, 242.4036, 160.7140>>
						CASE 4		RETURN <<1918.5881, 261.9843, 160.5265>>
					ENDSWITCH
				BREAK
				
				CASE OPPRESSOR
					SWITCH iVehicle
						CASE 0		RETURN <<1875.2690, 262.3857, 162.0291>>
						CASE 1		RETURN <<1876.1965, 265.5036, 162.4371>>
						CASE 2		RETURN <<1878.2213, 261.2158, 161.4793>>
						CASE 3		RETURN <<1879.3901, 264.2720, 161.6580>>
						CASE 4		RETURN <<1885.1935, 266.3538, 161.6360>>
						CASE 5		RETURN <<1881.7300, 268.4110, 162.0988>>
						CASE 6		RETURN <<1884.5066, 272.3345, 162.1315>>
						CASE 7		RETURN <<1887.5874, 270.5345, 161.6603>>
						CASE 8		RETURN <<1883.4980, 257.6874, 161.3071>>
						CASE 9		RETURN <<1882.7051, 262.0593, 161.5190>>
						CASE 10		RETURN <<1888.0092, 259.6203, 161.5064>>
						CASE 11		RETURN <<1888.3365, 264.1778, 161.6405>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC FLOAT GET_SUPPORT_VEHICLE_SPAWN_HEADING(DEFUNCT_BASE_ID defunctBaseId, INT iVehicle, MODEL_NAMES eModel, BOOL bAlternateMissionCondition = FALSE, GANGOPS_LOCATION eLocation = GangOpsLocation_Max)

	// Specific locations have specific coords for spawning support vehicles
	SWITCH eLocation
		// Aqualungs
		CASE GangOpsLocation_Aqualungs_Elysian_Island_1
			IF bAlternateMissionCondition
				SWITCH iVehicle
					CASE 0			RETURN 180.0
					CASE 1			RETURN 180.0
					CASE 2			RETURN 180.0
					CASE 3			RETURN 180.0
				ENDSWITCH
			ELSE
				SWITCH iVehicle
					CASE 0			RETURN 180.0
					CASE 1			RETURN 180.0
					CASE 2			RETURN 180.0
					CASE 3			RETURN 180.0
				ENDSWITCH
			ENDIF
		BREAK
		CASE GangOpsLocation_Aqualungs_Paleto_Bay
			IF bAlternateMissionCondition
				SWITCH iVehicle
					CASE 0			RETURN 324.2000
					CASE 1			RETURN 354.8000
					CASE 2			RETURN 324.2000
					CASE 3			RETURN 305.3998
				ENDSWITCH
			ELSE
				SWITCH iVehicle
					CASE 0			RETURN 305.3998
					CASE 1			RETURN 326.3996
					CASE 2			RETURN 347.3996
					CASE 3			RETURN 329.1996
				ENDSWITCH
			ENDIF
		BREAK
		CASE GangOpsLocation_Aqualungs_Elysian_Island_2
			IF bAlternateMissionCondition
				SWITCH iVehicle
					CASE 0			RETURN 206.2000
					CASE 1			RETURN 228.6000
					CASE 2			RETURN 246.9998
					CASE 3			RETURN 261.9998
				ENDSWITCH
			ELSE
				SWITCH iVehicle
					CASE 0			RETURN 210.2000
					CASE 1			RETURN 174.4000
					CASE 2			RETURN 245.4000
					CASE 3			RETURN 199.2000
				ENDSWITCH
			ENDIF
		BREAK
		CASE GangOpsLocation_Aqualungs_Palomino_Highlands
			IF bAlternateMissionCondition
				SWITCH iVehicle
					CASE 0			RETURN 200.8000
					CASE 1			RETURN 231.4000
					CASE 2			RETURN 231.4000
					CASE 3			RETURN 262.9998
				ENDSWITCH
			ELSE
				SWITCH iVehicle
					CASE 0			RETURN 234.3999
					CASE 1			RETURN 207.6000
					CASE 2			RETURN 200.4001
					CASE 3			RETURN 229.6000
				ENDSWITCH
			ENDIF
		BREAK
		CASE GangOpsLocation_Aqualungs_Chumash
			IF bAlternateMissionCondition
				SWITCH iVehicle
					CASE 0			RETURN 76.2000
					CASE 1			RETURN 97.1999
					CASE 2			RETURN 59.9999
					CASE 3			RETURN 41.9999
				ENDSWITCH
			ELSE
				SWITCH iVehicle
					CASE 0			RETURN 94.4000
					CASE 1			RETURN 67.1999
					CASE 2			RETURN 73.9999
					CASE 3			RETURN 104.1999
				ENDSWITCH
			ENDIF
		BREAK
		CASE GangOpsLocation_Aqualungs_San_Chianksi_Mountain_Range
			IF bAlternateMissionCondition
				SWITCH iVehicle
					CASE 0			RETURN 280.9998
					CASE 1			RETURN 319.0000
					CASE 2			RETURN 303.3999
					CASE 3			RETURN 342.8000
				ENDSWITCH
			ELSE
				SWITCH iVehicle
					CASE 0			RETURN 340.0000
					CASE 1			RETURN 305.7999
					CASE 2			RETURN 340.0000
					CASE 3			RETURN 319.0000
				ENDSWITCH
			ENDIF
		BREAK
		CASE GangOpsLocation_Aqualungs_PaletoForest
			IF bAlternateMissionCondition
				SWITCH iVehicle
					CASE 0			RETURN 92.1999
					CASE 1			RETURN 72.5999
					CASE 2			RETURN 78.7999
					CASE 3			RETURN 100.1997
				ENDSWITCH
			ELSE
				SWITCH iVehicle
					CASE 0			RETURN 102.3994
					CASE 1			RETURN 84.1993
					CASE 2			RETURN 102.3993
					CASE 3			RETURN 87.3990
				ENDSWITCH
			ENDIF
		BREAK
		CASE GangOpsLocation_Aqualungs_North_Chumash
			IF bAlternateMissionCondition
				SWITCH iVehicle
					CASE 0			RETURN 82.7999
					CASE 1			RETURN 49.2000
					CASE 2			RETURN 73.4000
					CASE 3			RETURN 59.4000
				ENDSWITCH
			ELSE
				SWITCH iVehicle
					CASE 0			RETURN 96.7999
					CASE 1			RETURN 55.0000
					CASE 2			RETURN 79.6000
					CASE 3			RETURN 68.2000
				ENDSWITCH
			ENDIF
		BREAK
		CASE GangOpsLocation_Aqualungs_Pacific_Bluffs
			IF bAlternateMissionCondition
				SWITCH iVehicle
					CASE 0			RETURN 112.1999
					CASE 1			RETURN 141.5997
					CASE 2			RETURN 139.3997
					CASE 3			RETURN 130.3997
				ENDSWITCH
			ELSE
				SWITCH iVehicle
					CASE 0			RETURN 148.5995
					CASE 1			RETURN 169.5994
					CASE 2			RETURN 151.3992
					CASE 3			RETURN 144.1991
				ENDSWITCH
			ENDIF
		BREAK
		CASE GangOpsLocation_Aqualungs_Vespucci_Beach
			IF bAlternateMissionCondition
				SWITCH iVehicle
					CASE 0			RETURN 96.9998
					CASE 1			RETURN 144.5996
					CASE 2			RETURN 138.9996
					CASE 3			RETURN 125.7995
				ENDSWITCH
			ELSE
				SWITCH iVehicle
					CASE 0			RETURN 125.7995
					CASE 1			RETURN 159.9994
					CASE 2			RETURN 138.9992
					CASE 3			RETURN 99.3989
				ENDSWITCH
			ENDIF
		BREAK
		
		CASE GangOpsLocation_UnderControl_ServerRoom1			
		CASE GangOpsLocation_UnderControl_ServerRoom2			
		CASE GangOpsLocation_UnderControl_ServerRoom3			
		CASE GangOpsLocation_UnderControl_ServerRoom4			
		CASE GangOpsLocation_UnderControl_ServerRoom5			
		CASE GangOpsLocation_UnderControl_ControlRoom1			
		CASE GangOpsLocation_UnderControl_ControlRoom2			
		CASE GangOpsLocation_UnderControl_ControlRoom3			
		CASE GangOpsLocation_UnderControl_ControlRoom4			
		CASE GangOpsLocation_UnderControl_ControlRoom5
			SWITCH iVehicle
				CASE 0			RETURN 329.9994
				CASE 1			RETURN 150.7997
				CASE 2			RETURN 150.7997
				CASE 3			RETURN 150.3997
				CASE 4			RETURN 150.3997
				CASE 5			RETURN 239.3995
				CASE 6			RETURN 238.5995
				CASE 7			RETURN 238.5995
				CASE 8			RETURN 238.5995
				CASE 9			RETURN 331.7995
			ENDSWITCH
		BREAK 
	ENDSWITCH
	
	SWITCH defunctBaseId
		CASE DEFUNCT_BASE_1
			SWITCH eModel
				CASE TULA
					SWITCH iVehicle
						CASE 0		RETURN 257.5995
						CASE 1		RETURN 257.5995
						CASE 2		RETURN 257.5995
						CASE 3		RETURN 338.7995
						CASE 4		RETURN 338.7995
					ENDSWITCH
				BREAK
				CASE OPPRESSOR
					SWITCH iVehicle
						CASE 0		RETURN 115.9997
						CASE 1		RETURN 115.9997
						CASE 2		RETURN 115.9997
						CASE 3		RETURN 115.9997
						CASE 4		RETURN 115.9997
						CASE 5		RETURN 115.9997
						CASE 6		RETURN 115.9997
						CASE 7		RETURN 115.9997
						CASE 8		RETURN 115.9997
						CASE 9		RETURN 115.9997
						CASE 10		RETURN 115.9997
						CASE 11		RETURN 115.9997
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE DEFUNCT_BASE_2
			SWITCH eModel
				CASE TULA
					SWITCH iVehicle
						CASE 0		RETURN 304.7998
						CASE 1		RETURN 304.7998
						CASE 2		RETURN 304.7998
						CASE 3		RETURN 308.7995
						CASE 4		RETURN 310.7994
					ENDSWITCH
				BREAK
				CASE OPPRESSOR
					SWITCH iVehicle
						CASE 0		RETURN 302.5999
						CASE 1		RETURN 302.5999
						CASE 2		RETURN 302.5999
						CASE 3		RETURN 302.5999
						CASE 4		RETURN 302.5999
						CASE 5		RETURN 302.5999
						CASE 6		RETURN 302.5999
						CASE 7		RETURN 302.5999
						CASE 8		RETURN 302.5999
						CASE 9		RETURN 302.5999
						CASE 10		RETURN 302.5999
						CASE 11		RETURN 302.5999
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE DEFUNCT_BASE_3
			SWITCH eModel
				CASE TULA
					SWITCH iVehicle
						CASE 0		RETURN 117.3987
						CASE 1		RETURN 140.5986
						CASE 2		RETURN 330.9980
						CASE 3		RETURN 311.7980
						CASE 4		RETURN 128.3985
					ENDSWITCH
				BREAK
				CASE OPPRESSOR
					SWITCH iVehicle
						CASE 0		RETURN 120.0000
						CASE 1		RETURN 120.0000
						CASE 2		RETURN 120.0000
						CASE 3		RETURN 120.0000
						CASE 4		RETURN 120.0000
						CASE 5		RETURN 120.0000
						CASE 6		RETURN 120.0000
						CASE 7		RETURN 120.0000
						CASE 8		RETURN 120.0000
						CASE 9		RETURN 120.0000
						CASE 10		RETURN 120.0000
						CASE 11		RETURN 120.0000
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE DEFUNCT_BASE_4
			SWITCH eModel
				CASE TULA
					SWITCH iVehicle
						CASE 0		RETURN 43.2000
						CASE 1		RETURN 42.9998
						CASE 2		RETURN 53.7997
						CASE 3		RETURN 46.5997
						CASE 4		RETURN 25.3997
					ENDSWITCH
				BREAK
				CASE OPPRESSOR
					SWITCH iVehicle
						CASE 0		RETURN 155.9997
						CASE 1		RETURN 161.9997
						CASE 2		RETURN 158.5997
						CASE 3		RETURN 151.7998
						CASE 4		RETURN 155.9997
						CASE 5		RETURN 155.9997
						CASE 6		RETURN 155.9997
						CASE 7		RETURN 155.9997
						CASE 8		RETURN 155.9997
						CASE 9		RETURN 155.9997
						CASE 10		RETURN 155.9997
						CASE 11		RETURN 155.9997
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
//		CASE DEFUNCT_BASE_5
//			SWITCH eModel
//				CASE TULA
//					SWITCH iFallback
//						CASE 0		RETURN 254.1998
//						CASE 1		RETURN 254.1998
//						CASE 2		RETURN 257.7997
//						CASE 3		RETURN 250.1994
//						CASE 4		RETURN 251.3994
//					ENDSWITCH
//				BREAK
//				CASE OPPRESSOR
//					SWITCH iFallback
//						CASE 0
//							SWITCH iVehicle
//								CASE 0		RETURN 250.4000
//								CASE 1		RETURN 250.4000
//								CASE 2		RETURN 250.4000
//								CASE 3		RETURN 250.4000
//							ENDSWITCH
//						BREAK
//						CASE 1
//							SWITCH iVehicle
//								CASE 0		RETURN 250.4000
//								CASE 1		RETURN 250.4000
//								CASE 2		RETURN 250.4000
//								CASE 3		RETURN 250.4000
//							ENDSWITCH
//						BREAK
//						CASE 2
//							SWITCH iVehicle
//								CASE 0		RETURN 250.4000
//								CASE 1		RETURN 250.4000
//								CASE 2		RETURN 250.4000
//								CASE 3		RETURN 250.4000
//							ENDSWITCH
//						BREAK
//					ENDSWITCH
//				BREAK
//			ENDSWITCH
//		BREAK
		
		CASE DEFUNCT_BASE_6
			SWITCH eModel
				CASE TULA
					SWITCH iVehicle
						CASE 0		RETURN 248.2000
						CASE 1		RETURN 248.2000
						CASE 2		RETURN 248.2000
						CASE 3		RETURN 248.2000
						CASE 4		RETURN 248.2000
					ENDSWITCH
				BREAK
				CASE OPPRESSOR
					SWITCH iVehicle
						CASE 0		RETURN 271.4000
						CASE 1		RETURN 271.4000
						CASE 2		RETURN 271.4000
						CASE 3		RETURN 271.4000
						CASE 4		RETURN 247.7651
						CASE 5		RETURN 247.7651
						CASE 6		RETURN 247.7651
						CASE 7		RETURN 247.7651
						CASE 8		RETURN 247.7651
						CASE 9		RETURN 247.7651
						CASE 10		RETURN 247.7651
						CASE 11		RETURN 247.7651
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE DEFUNCT_BASE_7
			SWITCH eModel
				CASE TULA
					SWITCH iVehicle
						CASE 0		RETURN 226.9977
						CASE 1		RETURN 226.9977
						CASE 2		RETURN 226.9977
						CASE 3		RETURN 226.9977
						CASE 4		RETURN 226.9977
					ENDSWITCH
				BREAK
				CASE OPPRESSOR
					SWITCH iVehicle
						CASE 0		RETURN 213.7998
						CASE 1		RETURN 213.7998
						CASE 2		RETURN 213.7998
						CASE 3		RETURN 213.7998
						CASE 4		RETURN 213.7998
						CASE 5		RETURN 213.7998
						CASE 6		RETURN 213.7998
						CASE 7		RETURN 213.7998
						CASE 8		RETURN 213.7998
						CASE 9		RETURN 213.7998
						CASE 10		RETURN 213.7998
						CASE 11		RETURN 213.7998
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE DEFUNCT_BASE_8
			SWITCH eModel
				CASE TULA
					SWITCH iVehicle
						CASE 0		RETURN 40.8000
						CASE 1		RETURN 40.8000
						CASE 2		RETURN 49.7999
						CASE 3		RETURN 59.7999
						CASE 4		RETURN 40.1998
					ENDSWITCH
				BREAK
				
				CASE OPPRESSOR
					SWITCH iVehicle
						CASE 0		RETURN 228.5980
						CASE 1		RETURN 228.5980
						CASE 2		RETURN 228.5980
						CASE 3		RETURN 228.5980
						CASE 4		RETURN 228.5980
						CASE 5		RETURN 228.5980
						CASE 6		RETURN 228.5980
						CASE 7		RETURN 228.5980
						CASE 8		RETURN 228.5980
						CASE 9		RETURN 228.5980
						CASE 10		RETURN 228.5980
						CASE 11		RETURN 228.5980
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE DEFUNCT_BASE_9
			SWITCH eModel
				CASE TULA
					SWITCH iVehicle
						CASE 0		RETURN 214.2000
						CASE 1		RETURN 206.4000
						CASE 2		RETURN 206.4000
						CASE 3		RETURN 217.6000
						CASE 4		RETURN 230.8000
					ENDSWITCH
				BREAK
				
				CASE OPPRESSOR
					SWITCH iVehicle
						CASE 0		RETURN 225.2000
						CASE 1		RETURN 225.2000
						CASE 2		RETURN 225.2000
						CASE 3		RETURN 225.2000
						CASE 4		RETURN 225.2000
						CASE 5		RETURN 225.2000
						CASE 6		RETURN 225.2000
						CASE 7		RETURN 225.2000
						CASE 8		RETURN 225.2000
						CASE 9		RETURN 225.2000
						CASE 10		RETURN 225.2000
						CASE 11		RETURN 225.2000
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE DEFUNCT_BASE_10
			SWITCH eModel
				CASE TULA
					SWITCH iVehicle
						CASE 0		RETURN 143.5943
						CASE 1		RETURN 143.5943
						CASE 2		RETURN 143.5943
						CASE 3		RETURN 143.5943
						CASE 4		RETURN 143.5943
					ENDSWITCH
				BREAK
				
				CASE OPPRESSOR
					SWITCH iVehicle
						CASE 0		RETURN 106.9982
						CASE 1		RETURN 106.9982
						CASE 2		RETURN 106.9982
						CASE 3		RETURN 106.9982
						CASE 4		RETURN 106.9982
						CASE 5		RETURN 106.9982
						CASE 6		RETURN 106.9982
						CASE 7		RETURN 106.9982
						CASE 8		RETURN 106.9982
						CASE 9		RETURN 106.9982
						CASE 10		RETURN 106.9982
						CASE 11		RETURN 106.9982
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

