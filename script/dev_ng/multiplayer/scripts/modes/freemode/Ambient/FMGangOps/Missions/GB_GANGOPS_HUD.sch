
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GB_FM_GANGOPS_HUD.sch																									//
// Handles the displaying and processing of frontend HUD elements such as bottom right HUD, help text and text messages	//
// Written by:  William Kennedy, Ryan Elliott & Martin McMillan.														//
// Date: 		08/08/2017																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//----------------------
//	INCLUDES
//----------------------

USING "rage_builtins.sch"
USING "globals.sch"

USING "GB_GANGOPS_HEADER_COMMON.sch"

///////////////////////////////////
////  		  COMMON  	       ////
///////////////////////////////////   

FUNC BOOL SAFE_TO_DISPLAY_CALL_OR_TEXT()
	IF IS_PLAYER_IN_OFFICE_PROPERTY(PLAYER_ID())
	OR NOT IS_PLAYER_PLAYING(PLAYER_ID())
	OR IS_PLAYER_IN_CUTSCENE(PLAYER_ID())
	OR IS_LOCAL_PLAYER_DOING_HELI_DOCK_CUTSCENE()
	OR IS_PAUSE_MENU_ACTIVE()
	OR IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC INT GET_NUMBER_OF_MISSION_ENTITIES_FOUND()
	INT iLoop, iCounter
	
	REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iLoop
		IF IS_MISSION_ENTITY_BIT_SET(iLoop, eMISSIONENTITYBITSET_FOUND)
			iCounter++
		ENDIF
	ENDREPEAT
	
	RETURN iCounter
ENDFUNC

///////////////////////////////////
////  	  OBJECTIVE TEXT  	   ////
/////////////////////////////////// 

FUNC BOOL SHOULD_DO_EXIT_OFFICE_OBJECTIVE()
	IF IS_LOCAL_BIT_SET(eLOCALBITSET_HANDLED_OFFICE_OBJECTIVE)
		RETURN FALSE
	ENDIF
	
	IF HAS_ANY_MISSION_ENTITY_BEEN_COLLECTED_FOR_THE_FIRST_TIME()
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_OFFICE_PROPERTY(PLAYER_ID())
	OR IS_PLAYER_IN_OFFICE_GARAGE_PROPERTY(PLAYER_ID())
	OR IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
	OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_SPAWNING_IN_A_PROPERTY)	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_FIND_ITEM_OBJECTIVE_TEXT_LABEL()
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_AUTO_SALVAGE
			IF GET_NUMBER_OF_MISSION_ENTITIES_FOUND() >= (GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION()-1)
				RETURN "FHOT_FINDWRCK2"
			ENDIF
			RETURN "FHOT_FINDWRECK"
		CASE GOV_CAR_COLLECTOR		
			IF IS_SERVER_BIT_SET(eSERVERBITSET_ITEM_HOLDER_IS_DEAD)
				RETURN "FHOT_COLKEY"
			ENDIF
			RETURN "FHOT_FINDKEY"
		CASE GOV_BURGLARY_JOB
			RETURN "FHOT_COLPHONE"
	ENDSWITCH

	RETURN ""
ENDFUNC

FUNC STRING GET_EXIT_OFFICE_OBJECTIVE_TEXT_LABEL()
	IF IS_PLAYER_IN_OFFICE_PROPERTY(PLAYER_ID())
		RETURN "FHOT_EXITOFF"
	ELIF IS_PLAYER_IN_OFFICE_GARAGE_PROPERTY(PLAYER_ID())
		RETURN "FHOT_EXITGAR"
	ENDIF
	RETURN ""
ENDFUNC

FUNC STRING GET_LOSE_COPS_MISSION_ENTITY_OBJECTIVE_TEXT_LABEL()
	RETURN "FHOT_LOSECOPS"
ENDFUNC

FUNC STRING GET_VARIATION_GOTO_LOCATION_NAME_FOR_OBJECTIVE_TEXT(BOOL &bNeedsThe)
	
	SWITCH GET_GANGOPS_LOCATION()
	
		CASE GangOpsLocation_DaylightRobbery_MirrorPark					RETURN "FH_LOC_MPB"
		CASE GangOpsLocation_DaylightRobbery_SanAndreas					RETURN "FH_LOC_SANAN" 
		CASE GangOpsLocation_DaylightRobbery_Vespucci					RETURN "FH_LOC_IMAGIN"
		CASE GangOpsLocation_DaylightRobbery_LegionSq					RETURN "FH_LOC_STRAWA"
		CASE GangOpsLocation_DaylightRobbery_DelPerro					RETURN "FH_LOC_PALH2"
		CASE GangOpsLocation_DaylightRobbery_Rockford					RETURN "FH_LOC_HW"
		CASE GangOpsLocation_DaylightRobbery_WestVinewood				RETURN "FH_LOC_ECB"
		CASE GangOpsLocation_DaylightRobbery_Burton						RETURN "FH_LOC_HAW"
		CASE GangOpsLocation_DaylightRobbery_DelPerroPlaza				RETURN "FH_LOC_DPPL"
		CASE GangOpsLocation_DaylightRobbery_Pillbox					RETURN "FH_LOC_SANAN"
	
		CASE GangOpsLocation_DeadDrop_Location1														RETURN "FH_LOC_LSIA"			// LSIA
		CASE GangOpsLocation_DeadDrop_Location2														RETURN "FH_LOC_LSIA"			// LSIA
		CASE GangOpsLocation_DeadDrop_Location3														RETURN "FH_LOC_LSIA"			// LSIA
		CASE GangOpsLocation_DeadDrop_Location4														RETURN "FH_LOC_LSIA"			// LSIA
		CASE GangOpsLocation_DeadDrop_Location5														RETURN "FH_LOC_LSIA"			// LSIA
		CASE GangOpsLocation_DeadDrop_Location6														RETURN "FH_LOC_LSIA"			// LSIA
		CASE GangOpsLocation_DeadDrop_Location7														RETURN "FH_LOC_PUERTOS"			// Puerto Del Sol Station
		CASE GangOpsLocation_DeadDrop_Location8														RETURN "FH_LOC_PUERTOS"			// Puerto Del Sol Station
		CASE GangOpsLocation_DeadDrop_Location9														RETURN "FH_LOC_PUERTOS"			// Puerto Del Sol Station
		CASE GangOpsLocation_DeadDrop_Location10													RETURN "FH_LOC_STRAWS"			// Strawberry Station
		CASE GangOpsLocation_DeadDrop_Location11													RETURN "FH_LOC_STRAWS"			// Strawberry Station
		CASE GangOpsLocation_DeadDrop_Location12													RETURN "FH_LOC_STRAWS"			// Strawberry Station
		CASE GangOpsLocation_DeadDrop_Location13													RETURN "FH_LOC_STRAWS"			// Strawberry Station
		CASE GangOpsLocation_DeadDrop_Location14													RETURN "FH_LOC_PILLS"			// Pillbox South Station
		CASE GangOpsLocation_DeadDrop_Location15													RETURN "FH_LOC_PILLS"			// Pillbox South Station
		
		CASE GangOpsLocation_FlareUp_Chamberlain_Hills												RETURN "FH_FU_GTL1"
		CASE GangOpsLocation_FlareUp_Gas_Station_Morningwood										RETURN "FH_FU_GTL2"
		CASE GangOpsLocation_FlareUp_Gas_Station_La_Puerta											RETURN "FH_FU_GTL3"
		CASE GangOpsLocation_FlareUp_Gas_Station_El_burro											RETURN "FH_FU_GTL4"
		CASE GangOpsLocation_FlareUp_Gas_Station_Davis												RETURN "FH_FU_GTL5"
		CASE GangOpsLocation_FlareUp_Gas_Station_Sandy												RETURN "FH_FU_GTL6"
		CASE GangOpsLocation_FlareUp_Gas_Station_Paleto												RETURN "FH_FU_GTL7"
		CASE GangOpsLocation_FlareUp_Gas_Station_Capital											RETURN "FH_FU_GTL8"
		CASE GangOpsLocation_FlareUp_Gas_Station_Clinton											RETURN "FH_FU_GTL9"
		CASE GangOpsLocation_FlareUp_Gas_Station_Richman											RETURN "FH_FU_GTL10"
	
		CASE GangOpsLocation_ForcedEntry_Location_Fudge												RETURN "PM_SPAWN_A4"
		CASE GangOpsLocation_ForcedEntry_Location_Magellan											RETURN "PM_SPAWN_A4"
		CASE GangOpsLocation_ForcedEntry_Location_Forum												RETURN "PM_SPAWN_A4"
		CASE GangOpsLocation_ForcedEntry_Location_Roy_Lowenstein									RETURN "PM_SPAWN_A4"
		CASE GangOpsLocation_ForcedEntry_Location_Eastbourne										RETURN "PM_SPAWN_A4"
		
		CASE GangOpsLocation_ForcedEntry_Location_V_Magellan										RETURN "PM_SPAWN_A4"
		CASE GangOpsLocation_ForcedEntry_Location_Alta												RETURN "PM_SPAWN_A4"
		CASE GangOpsLocation_ForcedEntry_Location_Forum_Drive										RETURN "PM_SPAWN_A4"
		CASE GangOpsLocation_ForcedEntry_Location_Spanish											RETURN "PM_SPAWN_A4"
		CASE GangOpsLocation_ForcedEntry_Location_Tahitian											RETURN "PM_SPAWN_A4"
		
		CASE GangOpsLocation_CarCollector_SteeleWay													RETURN "FH_LOC_STEELE"			// Steele Way
		CASE GangOpsLocation_CarCollector_AmericanoWay												RETURN "FH_LOC_AMERI"			// Americano Way
		CASE GangOpsLocation_CarCollector_AceJonesDrive												RETURN "FH_LOC_ACEJONE"			// Ace Jones Drive
		CASE GangOpsLocation_CarCollector_Richman													RETURN "FH_LOC_RICHM"			// Richman
		CASE GangOpsLocation_CarCollector_NorthRockford_1											RETURN "FH_LOC_NROCKF"			// North Rockford Drive
		CASE GangOpsLocation_CarCollector_DunstableLane_1											RETURN "FH_LOC_NROCKF"			// Dunstable Lane
		CASE GangOpsLocation_CarCollector_DunstableLane_2											RETURN "FH_LOC_DUNLA"			// Dunstable Lane
		CASE GangOpsLocation_CarCollector_NorthRockford_2											RETURN "FH_LOC_NROCKF"			// North Rockford Drive
		CASE GangOpsLocation_CarCollector_CaesarsPlace												RETURN "FH_LOC_CAES"			// Caesars Place
		CASE GangOpsLocation_CarCollector_HillcrestAvenue											RETURN "FH_LOC_HILLC"			// Hillcrest Avenue
	
		// Mob Mentality
		CASE GangOpsLocation_MobMentality_Morningwood					bNeedsThe = TRUE			RETURN "FH_LOC_MOVIE"			// movie theatre
		CASE GangOpsLocation_MobMentality_Grapeseed						bNeedsThe = TRUE			RETURN "FH_LOC_ALFM"			// Alamo Fruit Market
		CASE GangOpsLocation_MobMentality_Alta 							bNeedsThe = TRUE			RETURN "FH_LOC_MEXMARK"			// Mexican Market
		CASE GangOpsLocation_MobMentality_MissionRow												RETURN "FH_LOC_MOTO"			// Sanders Motorcycles
		CASE GangOpsLocation_MobMentality_HippyCamp						bNeedsThe = TRUE			RETURN "FH_LOC_HIPPY"			// hippy camp
		CASE GangOpsLocation_MobMentality_SanAndreas												RETURN "FH_LOC_AQUJA"			// Aquja Street
		CASE GangOpsLocation_MobMentality_MirrorPark					bNeedsThe = TRUE			RETURN "FH_LOC_CONST"			// construction site
		CASE GangOpsLocation_MobMentality_Strawberry												RETURN "FH_LOC_STRAWP"			// Strawberry Plaza
		CASE GangOpsLocation_MobMentality_MirrorParkTavern				bNeedsThe = TRUE			RETURN "FH_LOC_MPT"				// Mirror Park Tavern
		CASE GangOpsLocation_MobMentality_ULSA														RETURN "FH_LOC_ULSA"			// ULSA
		
		// Paramedic
		CASE GangOpsLocation_Paramedic_Central_LS_Medical_Center		bNeedsThe = TRUE			RETURN "FH_LOC_LSMC"	// Central Los Santos Medical Center
		CASE GangOpsLocation_Paramedic_Pillbox_Hill_Medical_Center		bNeedsThe = TRUE			RETURN "FH_LOC_PHMC"	// Pillbox Hill Medical Center
		CASE GangOpsLocation_Paramedic_ST_Fiacre_Hospital				bNeedsThe = TRUE			RETURN "FH_LOC_STFC"	// St Fiacre Hospital
		CASE GangOpsLocation_Paramedic_Sandy_Shores_Medical_Center		bNeedsThe = TRUE			RETURN "FH_LOC_ZONA"	// Mount Zonah Medical Center
		CASE GangOpsLocation_Paramedic_Paleto_Bay_Care_Center			bNeedsThe = TRUE			RETURN "FH_LOC_PBCC"	// Paleto Bay Care Center
		CASE GangOpsLocation_Paramedic_Del_Perro_Freeway				bNeedsThe = TRUE			RETURN "FH_LOC_DPF"		// Del Perro Freeway
		CASE GangOpsLocation_Paramedic_Mount_Gordo													RETURN "FH_LOC_MG"		// Mount Gordo
		CASE GangOpsLocation_Paramedic_Great_Chaparral												RETURN "FH_LOC_GC"		// Great Chaparral
		CASE GangOpsLocation_Paramedic_Cassidy_Creek												RETURN "FH_LOC_CC"		// Cassidy Creek
		CASE GangOpsLocation_Paramedic_Power_Station												RETURN "FH_LOC_BANH"	// Banham Canyon
				
		// Aqualungs
		CASE GangOpsLocation_Aqualungs_Elysian_Island_1												RETURN "FH_LOC_ELYS"	// Elysian Island
		CASE GangOpsLocation_Aqualungs_Paleto_Bay													RETURN "FH_LOC_PALB"	// Paleto Bay
		CASE GangOpsLocation_Aqualungs_Elysian_Island_2												RETURN "FH_LOC_ELYS"	// Elysian Island
		CASE GangOpsLocation_Aqualungs_Palomino_Highlands				bNeedsThe = TRUE			RETURN "FH_LOC_PALHI"	// Palomino Highlands
		CASE GangOpsLocation_Aqualungs_Chumash														RETURN "FH_LOC_CHUM"	// Chumash
		CASE GangOpsLocation_Aqualungs_San_Chianksi_Mountain_Range		bNeedsThe = TRUE			RETURN "FH_LOC_SCMR"	// San Chianski Mountain Range
		CASE GangOpsLocation_Aqualungs_PaletoForest													RETURN "FH_LOC_PALFOR"	// Paleto Forest
		CASE GangOpsLocation_Aqualungs_North_Chumash												RETURN "FH_LOC_NCHUM"	// North Chumash
		CASE GangOpsLocation_Aqualungs_Pacific_Bluffs												RETURN "FH_LOC_PACBLU"	// Pacific Bluffs
		CASE GangOpsLocation_Aqualungs_Vespucci_Beach												RETURN "FH_LOC_VESPU"	// Vespucci Beach
		
		// Under Control
		CASE GangOpsLocation_UnderControl_ServerRoom1												RETURN "FH_LOC_ZANC"	// Fort Zancudo
		CASE GangOpsLocation_UnderControl_ServerRoom2												RETURN "FH_LOC_ZANC"	// Fort Zancudo
		CASE GangOpsLocation_UnderControl_ServerRoom3												RETURN "FH_LOC_ZANC"	// Fort Zancudo
		CASE GangOpsLocation_UnderControl_ServerRoom4												RETURN "FH_LOC_ZANC"	// Fort Zancudo
		CASE GangOpsLocation_UnderControl_ServerRoom5												RETURN "FH_LOC_ZANC"	// Fort Zancudo
		CASE GangOpsLocation_UnderControl_ControlRoom1												RETURN "FH_LOC_ZANC"	// Fort Zancudo
		CASE GangOpsLocation_UnderControl_ControlRoom2												RETURN "FH_LOC_ZANC"	// Fort Zancudo
		CASE GangOpsLocation_UnderControl_ControlRoom3												RETURN "FH_LOC_ZANC"	// Fort Zancudo
		CASE GangOpsLocation_UnderControl_ControlRoom4												RETURN "FH_LOC_ZANC"	// Fort Zancudo
		CASE GangOpsLocation_UnderControl_ControlRoom5												RETURN "FH_LOC_ZANC"	// Fort Zancudo
		
		// Burglary Job
		CASE GangOpsLocation_BurglaryJob_ProcopioDrive												RETURN "FH_LOC_PROPOC"	// Procopio Drive
		CASE GangOpsLocation_BurglaryJob_SouthMoMilton												RETURN "FH_LOC_SMOMILT"	// South Mo Milton Drive
		CASE GangOpsLocation_BurglaryJob_MirrorPark													RETURN "FH_LOC_MPB"		// Mirror Park Boulevard
		CASE GangOpsLocation_BurglaryJob_ImaginationCourt											RETURN "FH_LOC_IMAGIN"	// Imagination Court
		CASE GangOpsLocation_BurglaryJob_DidionDrive												RETURN "FH_LOC_DIDION"	// Didion Drive
		CASE GangOpsLocation_BurglaryJob_KimbleHill													RETURN "FH_LOC_KIMBLE"	// Kimble Hill Drive
		CASE GangOpsLocation_BurglaryJob_BridgeStreet												RETURN "FH_LOC_BRISTRT"	// Bridge Street
		CASE GangOpsLocation_BurglaryJob_NikolaAvenue												RETURN "FH_LOC_NIKAVE"	// Nikola Avenue
		CASE GangOpsLocation_BurglaryJob_NorthSheldonAvenue											RETURN "FH_LOC_NSHEL"	// North Sheldon Avenue
		CASE GangOpsLocation_BurglaryJob_InesenoRoad												RETURN "FH_LOC_INES"	// Ineseno Road
		
		// Gone Ballistic
		CASE GangOpsLocation_GoneBallistic_ElysianIsland1				bNeedsThe = TRUE			RETURN "FH_LOC_MRRWC"	// Merryweather camp
		CASE GangOpsLocation_GoneBallistic_GrandSenoraDesert1			bNeedsThe = TRUE			RETURN "FH_LOC_MRRWC"	// Merryweather camp
		CASE GangOpsLocation_GoneBallistic_Grapeseed1        			bNeedsThe = TRUE			RETURN "FH_LOC_MRRWC"	// Merryweather camp
		CASE GangOpsLocation_GoneBallistic_ElBurroHeights        		bNeedsThe = TRUE			RETURN "FH_LOC_MRRWC"	// Merryweather camp
		CASE GangOpsLocation_GoneBallistic_Terminal          			bNeedsThe = TRUE			RETURN "FH_LOC_MRRWC"	// Merryweather camp
		CASE GangOpsLocation_GoneBallistic_LSIA							bNeedsThe = TRUE			RETURN "FH_LOC_MRRWC"	// Merryweather camp
		CASE GangOpsLocation_GoneBallistic_MurrietaHeights				bNeedsThe = TRUE			RETURN "FH_LOC_MRRWC"	// Merryweather camp
		CASE GangOpsLocation_GoneBallistic_ElysianIsland2    			bNeedsThe = TRUE			RETURN "FH_LOC_MRRWC"	// Merryweather camp
		CASE GangOpsLocation_GoneBallistic_PaletoBay         			bNeedsThe = TRUE			RETURN "FH_LOC_MRRWC"	// Merryweather camp
		CASE GangOpsLocation_GoneBallistic_FortZancudo       			bNeedsThe = TRUE			RETURN "FH_LOC_MRRWC"	// Merryweather camp
		
		// Amateur Photography
		CASE GangOpsLocation_AmateurPhotography_Location1				bNeedsThe = TRUE			RETURN "FH_LOC_SHPYRD"	// Pacific Allied Shipyard
			
	ENDSWITCH
	UNUSED_PARAMETER(bNeedsThe)
	RETURN ""
ENDFUNC

FUNC STRING GET_MISSION_GOTO_OBJECTIVE_TEXT_LABEL(BOOL bNeedsThe = FALSE)
	IF SHOULD_RUN_BIKER_RACE_FOR_VARIATION()
		IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_RACE_TO_POINT_FINISHED)
		AND NOT GB_BIKER_RACE_IS_RACE_OVER(serverBD.sRaceServerVars)
			IF bNeedsThe
				RETURN "SMOT_RACETO_THE"
			ENDIF
			RETURN "SMOT_RACETO"
		ENDIF
	ENDIF
	
	IF bNeedsThe
		RETURN "FHOT_GOTO_THE" // Go to the ~a~.
	ENDIF
	RETURN "FHOT_GOTO" // Go to ~a~.
ENDFUNC

FUNC STRING GET_MISSION_HELP_GOTO_OBJECTIVE_TEXT_LABEL(BOOL bNeedsThe = FALSE)
	IF bNeedsThe
		RETURN "SMOT_HELPTRANSP"
	ENDIF
	RETURN "SMOT_HELPTRANSP"
ENDFUNC

FUNC BOOL SHOULD_MISSION_VARIATION_SAY_COLLECT_NOT_STEAL()

	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_DEAD_DROP				RETURN TRUE
		CASE GOV_FLIGHT_RECORDER		RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC STRING GET_ITEM_LABEL_FOR_MISSION(BOOL bNeedsThe, BOOL bForBigMessage = FALSE)
	UNUSED_PARAMETER(bNeedsThe)
	
	INT iNumEntitiesLeft

	SWITCH GET_GANGOPS_ITEM_TYPE()
		CASE eFHIT_DUFFEL_BAG_AQ	
			IF bForBigMessage
				RETURN "FHBM_CL_SCUB"
			ELSE
				RETURN "FH_COL_SCUB"
			ENDIF
		BREAK
		
		CASE eFHIT_DUFFEL_BAG_DD		
			IF bForBigMessage
				RETURN "FHBM_CL_DUFF"
			ELSE
				RETURN "FH_COL_DUFF"
			ENDIF
		BREAK
		CASE eFHIT_DUFFEL_BAG_FE
			IF bForBigMessage
				RETURN "FHBM_CL_DUFF"
			ELSE
				RETURN "FH_COL_DUFF"
			ENDIF
		BREAK
			
		CASE eFHIT_LAPTOP	
			IF bForBigMessage
				RETURN "FHBM_C_LAPTOP"
			ELSE
				RETURN "FH_COL_LAPTOP"
			ENDIF
		BREAK
			
		CASE eFHIT_BRIEFCASE		
			IF bForBigMessage
				RETURN "FHBM_CL_BRIEF"
			ELSE
				RETURN "FH_COL_BRIEF"
			ENDIF
		BREAK
		CASE eFHIT_FLATBED	
		CASE eFHIT_WASTELANDER
			IF bForBigMessage
				RETURN "FHBM_CL_STROM"
			ELSE
				iNumEntitiesLeft = GET_NUM_MISSION_ENTITIES_NOT_DELIVERED_OR_DESTROYED()
				IF iNumEntitiesLeft = 1
					RETURN "FH_COL_TRUCK"
				ELIF iNumEntitiesLeft > 1
					RETURN "FH_COL_TRUCKS"
				ENDIF
			ENDIF
		BREAK
		CASE eFHIT_DELUXO	
			IF bForBigMessage
				RETURN "FHBM_CL_DELU"
			ELSE
				iNumEntitiesLeft = GET_NUM_MISSION_ENTITIES_NOT_DELIVERED_OR_DESTROYED()
				IF iNumEntitiesLeft = 1
					RETURN "FH_COL_VEH1"
				ELIF iNumEntitiesLeft > 1
					RETURN "FH_COL_VEH2"
				ENDIF
			ENDIF
		BREAK
		CASE eFHIT_RIOT_TRUCK
			IF bForBigMessage
				RETURN "FHBM_CL_RCV"
			ENDIF
			RETURN "FH_COL_RIOT"
		CASE eFHIT_STEALTH_GEAR
			IF bForBigMessage
				RETURN "FHBM_CL_RIOT"
			ENDIF
			RETURN "FH_RIOT"
		CASE eFHIT_AMBULANCE
			IF bForBigMessage
				RETURN "FHBM_CL_AMBU"
			ENDIF
			RETURN "FH_COL_AMBU"
		CASE eFHIT_STEALTH_HELI
			IF bForBigMessage
				RETURN "FHBM_CL_STEAHE"
			ENDIF
			RETURN "FH_COL_STEAHE"
		CASE eFHIT_FLIGHT_RECORDER
			IF bForBigMessage
				RETURN "FHBM_C_FLIREC"
			ELSE
				RETURN "FH_COL_FLIREC"
			ENDIF
		BREAK
		CASE eFHIT_CHERNOBOG
			IF bForBigMessage
				RETURN "FHBM_C_CHERNO"
			ENDIF
			RETURN "FH_COL_CHERNO"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC BOOL SHOULD_DO_RECOVER_OBJECTIVE_NOT_STEAL()
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_FLIGHT_RECORDER		RETURN (NOT HAS_ANY_MISSION_ENTITY_BEEN_COLLECTED_FOR_THE_FIRST_TIME())
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC DO_COLLECT_MISSION_ENTITY_OBJECTIVE_TEXT()
	IF IS_FINAL_MISSION_ENTITY_AT_DROP_OFF()
		EXIT
	ENDIF
	
	TEXT_LABEL_15 objectiveText
	TEXT_LABEL_15 targetEntity = GET_ITEM_LABEL_FOR_MISSION(FALSE)
	HUD_COLOURS colour
	
	IF SHOULD_DO_RECOVER_OBJECTIVE_NOT_STEAL()
		IF GET_GANGOPS_VARIATION() = GOV_FLIGHT_RECORDER
			objectiveText = "FHOT_FR_RCVR"
		ELSE
			objectiveText = "FHOT_RECOVER"
		ENDIF
	ELIF SHOULD_MISSION_VARIATION_SAY_COLLECT_NOT_STEAL()
		objectiveText = "FHOT_COLLECTa"
	ELSE
		objectiveText = "FHOT_STEALa"
	ENDIF
	
	IF serverBD.sMissionEntities.eType = eMISSIONENTITYTYPE_CAR
		colour = HUD_COLOUR_BLUE
	ELSE
		colour = HUD_COLOUR_GREEN
	ENDIF
	
	Print_Objective_Text_With_Coloured_Text_Label(objectiveText, targetEntity, colour)						
ENDPROC

FUNC BOOL OBJECTIVE_TEXT_NEEDS_THE()
	RETURN FALSE
ENDFUNC

PROC DO_GO_TO_OBJECTIVE_TEXT()
	BOOL bNeedsThe = OBJECTIVE_TEXT_NEEDS_THE()
	STRING strLocName = GET_VARIATION_GOTO_LOCATION_NAME_FOR_OBJECTIVE_TEXT(bNeedsThe)
	
	Print_Objective_Text_With_Coloured_Text_Label(GET_MISSION_GOTO_OBJECTIVE_TEXT_LABEL(bNeedsThe), strLocName, HUD_COLOUR_YELLOW)
ENDPROC

FUNC STRING GET_VARIATION_GOTO_ENTITY_NAME_FOR_OBJECTIVE_TEXT(BOOL &bNeedsThe)
	UNUSED_PARAMETER(bNeedsThe)
	RETURN ""
ENDFUNC

FUNC STRING GET_VARIATION_GOTO_ENTITY_OBJECTIVE_TEXT_LABEL(BOOL bNeedsThe)
	RETURN GET_MISSION_GOTO_OBJECTIVE_TEXT_LABEL(bNeedsThe)
ENDFUNC

FUNC HUD_COLOURS GET_GO_TO_ENTITY_OBJECTIVE_HUD_COLOUR()
	RETURN HUD_COLOUR_BLUE
ENDFUNC

PROC DO_GO_TO_ENTITY_OBJECTIVE_TEXT()
	BOOL bNeedsThe = FALSE
	STRING strEntityName = GET_VARIATION_GOTO_ENTITY_NAME_FOR_OBJECTIVE_TEXT(bNeedsThe)
	
	Print_Objective_Text_With_Coloured_Text_Label(GET_VARIATION_GOTO_ENTITY_OBJECTIVE_TEXT_LABEL(bNeedsThe), strEntityName, GET_GO_TO_ENTITY_OBJECTIVE_HUD_COLOUR())
ENDPROC

FUNC STRING GET_HACK_OBJECTIVE_TEXT_SUBSTRING()

	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_UNDER_CONTROL	
			SWITCH GET_GANGOPS_SUBVARIATION()
				CASE GOS_UC_SERVER_ROOM_1	
				CASE GOS_UC_SERVER_ROOM_2
				CASE GOS_UC_SERVER_ROOM_3	
				CASE GOS_UC_SERVER_ROOM_4	
				CASE GOS_UC_SERVER_ROOM_5	
					RETURN "FH_OBJ_HACKS"
				CASE GOS_UC_CONTROL_ROOM_1
				CASE GOS_UC_CONTROL_ROOM_2
				CASE GOS_UC_CONTROL_ROOM_3	
				CASE GOS_UC_CONTROL_ROOM_4	
				CASE GOS_UC_CONTROL_ROOM_5	
					RETURN "FH_OBJ_HACKC"
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN ""

ENDFUNC

PROC DO_HACK_OBJECTIVE_TEXT()
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_BURGLARY_JOB 		Print_Objective_Text("FHOT_CRACK")		BREAK
		CASE GOV_UNDER_CONTROL 		Print_Objective_Text_With_String("FHOT_HACK", GET_HACK_OBJECTIVE_TEXT_SUBSTRING())		BREAK
	ENDSWITCH
ENDPROC

PROC DO_INTERACT_OBJECTIVE_TEXT()
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_BURGLARY_JOB		Print_Objective_Text("FHOT_BJ_INTER")			BREAK
		DEFAULT 					Print_Objective_Text("FHOT_INTERACT")			BREAK
	ENDSWITCH
ENDPROC

PROC DO_FOLLOW_OBJECTIVE_TEXT()
	Print_Objective_Text("FHOT_FOLLOW")
ENDPROC

PROC DO_REACH_SCORE_OBJECTIVE_TEXT()
	Print_Objective_Text("FHOT_DOWNLOAD")
ENDPROC

PROC DO_RETURN_TO_AREA_OBJECTIVE_TEXT()
	Print_Objective_Text("FHOT_RETURNTO")
ENDPROC

PROC DO_THROW_PROJECTILE_OBJECTIVE_TEXT()
	Print_Objective_Text("FHOT_TEAR")
ENDPROC

PROC DO_RIOT_OBJECTIVE_TEXT()
	Print_Objective_Text("FHOT_FIGHT")
ENDPROC

FUNC STRING GET_CURRENT_PHOTOGRAPH_LOCATION_NAME(INT iPoint)

	IF GET_GANGOPS_VARIATION() = GOV_DAYLIGHT_ROBBERY
		RETURN "DAY_TARGT"
	ENDIF

	SWITCH iPoint
		CASE ciAMATEUR_PHOTOGRAPHY_MAIN_GATE 			RETURN "FHOT_AP_0"
		CASE ciAMATEUR_PHOTOGRAPHY_EAST_DOOR 			RETURN "FHOT_AP_1"
		CASE ciAMATEUR_PHOTOGRAPHY_NORTH_DOOR			RETURN "FHOT_AP_2"
		CASE ciAMATEUR_PHOTOGRAPHY_WEST_DOOR			RETURN "FHOT_AP_3"
		CASE ciAMATEUR_PHOTOGRAPHY_SIDE_DOOR			RETURN "FHOT_AP_4"
		CASE ciAMATEUR_PHOTOGRAPHY_JETTY				RETURN "FHOT_AP_5"
		CASE ciAMATEUR_PHOTOGRAPHY_WATER_GATE			RETURN "FHOT_AP_6"
		CASE ciAMATEUR_PHOTOGRAPHY_PIER					RETURN "FHOT_AP_7"
		CASE ciAMATEUR_PHOTOGRAPHY_INTERIOR				RETURN "FHOT_AP_8"
		CASE ciAMATEUR_PHOTOGRAPHY_OFFICE				RETURN "FHOT_AP_9"
		CASE 10											RETURN "FHOT_AP_9"			// This is here to prevent assert when the last photo is taken, and timing issue means text stays on for a few frames longer
	ENDSWITCH
	
	RETURN ""
ENDFUNC

PROC DO_TAKE_PHOTOGRAPHS_OBJECTIVE_TEXT()
	IF IS_LOCAL_BIT_SET(eLOCALBITSET_PHOTO_TAKEN)
		// Send the photo to Agent 14
		Print_Objective_Text("FHOT_AP_SEND")
	ELSE
		// Use your phone to take a photo of the ~a~.
		IF iClosestPhotoPoint != (-1)
			Print_Objective_Text_With_Coloured_Text_Label("FHOT_AP_PHOTO", GET_CURRENT_PHOTOGRAPH_LOCATION_NAME(iClosestPhotoPoint), HUD_COLOUR_BLUE)
		ENDIF
	ENDIF
ENDPROC

FUNC VECTOR GET_CAMERA_BLIP_COORDS(INT iPoint)

	IF GET_GANGOPS_VARIATION() = GOV_DAYLIGHT_ROBBERY
		RETURN vCHOSEN_PED_COORDS()
	ENDIF

	SWITCH iPoint
		CASE ciAMATEUR_PHOTOGRAPHY_MAIN_GATE 			RETURN <<64.6700, -2629.5525, 8.6678>>
		CASE ciAMATEUR_PHOTOGRAPHY_EAST_DOOR 			RETURN <<58.02, -2667.63, 6.1439>>
		CASE ciAMATEUR_PHOTOGRAPHY_NORTH_DOOR			RETURN <<34.4865, -2654.3831, 7.9585>>
		CASE ciAMATEUR_PHOTOGRAPHY_WEST_DOOR			RETURN <<12.20, -2654.41, 6.20>>
		CASE ciAMATEUR_PHOTOGRAPHY_SIDE_DOOR			RETURN <<11.49, -2700.49, 6.4220>>
		CASE ciAMATEUR_PHOTOGRAPHY_JETTY				RETURN <<13.88, -2800.52, 2.53>>
		CASE ciAMATEUR_PHOTOGRAPHY_WATER_GATE			RETURN <<34.57, -2758.88, 4.15>> 
		CASE ciAMATEUR_PHOTOGRAPHY_PIER					RETURN <<46.0776, -2790.3376, 6.2752>>
		CASE ciAMATEUR_PHOTOGRAPHY_INTERIOR				RETURN <<34.3316, -2718.3523, 4.3880>>
		CASE ciAMATEUR_PHOTOGRAPHY_OFFICE				RETURN <<33.98, -2693.71, 12.74>>
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC STRING GET_CURRENT_PHOTOGRAPH_BLIP_NAME(INT iPoint)
	SWITCH iPoint
		CASE ciAMATEUR_PHOTOGRAPHY_MAIN_GATE 			RETURN "FMBLIP_CAM_0"
		CASE ciAMATEUR_PHOTOGRAPHY_EAST_DOOR 			RETURN "FMBLIP_CAM_1"
		CASE ciAMATEUR_PHOTOGRAPHY_NORTH_DOOR			RETURN "FMBLIP_CAM_2"
		CASE ciAMATEUR_PHOTOGRAPHY_WEST_DOOR			RETURN "FMBLIP_CAM_3"
		CASE ciAMATEUR_PHOTOGRAPHY_SIDE_DOOR			RETURN "FMBLIP_CAM_4"
		CASE ciAMATEUR_PHOTOGRAPHY_JETTY				RETURN "FMBLIP_CAM_5"
		CASE ciAMATEUR_PHOTOGRAPHY_WATER_GATE			RETURN "FMBLIP_CAM_6"
		CASE ciAMATEUR_PHOTOGRAPHY_PIER					RETURN "FMBLIP_CAM_7"
		CASE ciAMATEUR_PHOTOGRAPHY_INTERIOR				RETURN "FMBLIP_CAM_8"
		CASE ciAMATEUR_PHOTOGRAPHY_OFFICE				RETURN "FMBLIP_CAM_9"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC HUD_COLOURS GET_CAMERA_HUD_COLOUR(INT iPoint)
	IF iClosestPhotoPoint = iPoint
		RETURN HUD_COLOUR_BLUE
	ENDIF
	RETURN HUD_COLOUR_GREY
ENDFUNC

PROC DRAW_CAMERA_BLIPS()
	INT iPoint 
	REPEAT ciAMATEUR_PHOTOGRAPHY_MAX_PHOTOS iPoint
		VECTOR vBlipCoord = GET_CAMERA_BLIP_COORDS(iPoint)
		IF NOT IS_VECTOR_ZERO(vBlipCoord)
		AND NOT IS_BIT_SET(serverBD.iPhotographyBitSetServer, iPoint)
			IF NOT DOES_BLIP_EXIST(blipCamera[iPoint])
				blipCamera[iPoint] = ADD_BLIP_FOR_COORD(vBlipCoord)
				SET_BLIP_SPRITE(blipCamera[iPoint], RADAR_TRACE_CAMERA)
				SET_BLIP_COLOUR_FROM_HUD_COLOUR(blipCamera[iPoint], GET_CAMERA_HUD_COLOUR(iPoint))
				SET_BLIP_NAME_FROM_TEXT_FILE(blipCamera[iPoint], GET_CURRENT_PHOTOGRAPH_BLIP_NAME(iPoint))
				PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - DRAW_CAMERA_BLIPS - Blip added at ", vBlipCoord, " for location ", GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_CURRENT_PHOTOGRAPH_BLIP_NAME(iPoint)))
			ELSE
				IF GET_BLIP_COLOUR(blipCamera[iPoint]) != GET_BLIP_COLOUR_FROM_HUD_COLOUR(GET_CAMERA_HUD_COLOUR(iPoint))
					SET_BLIP_COLOUR_FROM_HUD_COLOUR(blipCamera[iPoint], GET_CAMERA_HUD_COLOUR(iPoint))
					PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - DRAW_CAMERA_BLIPS - Updating camera blip colour photo location ", GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_CURRENT_PHOTOGRAPH_BLIP_NAME(iPoint)))
				ENDIF
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(blipCamera[iPoint])
				PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - DRAW_CAMERA_BLIPS - Removing camera blip for photo location ", GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_CURRENT_PHOTOGRAPH_BLIP_NAME(iPoint)))
				REMOVE_BLIP(blipCamera[iPoint])
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC REMOVE_CAMERA_BLIPS()
	INT iPoint 
	REPEAT ciAMATEUR_PHOTOGRAPHY_MAX_PHOTOS iPoint
		IF DOES_BLIP_EXIST(blipCamera[iPoint])
			PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - REMOVE_CAMERA_BLIPS - removing camera blip")
			REMOVE_BLIP(blipCamera[iPoint])
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL SHOULD_MISSION_ENTITY_BE_UNBLIPPED_UNTIL_FOUND()
	RETURN FALSE
ENDFUNC

FUNC STRING GET_VARIATION_SEARCH_AREA_NAME_FOR_OBJECTIVE_TEXT(BOOL &bNeedsThe)
	bNeedsThe = TRUE
	RETURN "SMOT_DROPZONE"
ENDFUNC

FUNC STRING GET_VARIATION_SEARCH_ENTITY_NAME_FOR_OBJECTIVE_TEXT()
	RETURN "SMBLIP_CARGO"
ENDFUNC

FUNC STRING GET_MISSION_SEARCH_OBJECTIVE_TEXT_LABEL(BOOL bNeedsThe = FALSE)
	IF bNeedsThe
		RETURN "SMOT_SEARCHa"
	ENDIF
	RETURN "SMOT_SEARCHb"
ENDFUNC

FUNC BOOL SHOULD_DO_SEARCH_AREA()
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_BURGLARY_JOB		
			IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_SEARCH_AREA_POINT_FOUND)
			AND IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_AM_INSIDE_MISSION_INTERIOR)
				RETURN TRUE
			ENDIF
		BREAK
		DEFAULT
			IF SHOULD_MISSION_ENTITY_BE_UNBLIPPED_UNTIL_FOUND()
				IF GET_CLIENT_MODE_STATE() < eMODESTATE_DELIVER_MISSION_ENTITY
				OR (NOT IS_PLAYER_IN_POSSESSION_OF_ANY_MISSION_ENTITY(PLAYER_ID())
				AND NOT HAVE_ALL_MISSION_ENTITIES_BEEN_FOUND())
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH	
	RETURN FALSE
ENDFUNC

FUNC HUD_COLOURS GET_HUD_COLOUR_FOR_SEARCH_AREA()
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_BURGLARY_JOB	RETURN HUD_COLOUR_WHITE
	ENDSWITCH
	RETURN HUD_COLOUR_YELLOW
ENDFUNC

FUNC VECTOR GET_SEARCH_AREA_FIND_POINT()
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_BURGLARY_JOB		RETURN GET_GANGOPS_PROP_COORDS(0, GET_GANGOPS_VARIATION(), GET_GANGOPS_SUBVARIATION(), GET_PROP_EXTRA_PARAM())
	ENDSWITCH
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC FLOAT GET_SEARCH_AREA_FIND_POINT_RANGE()
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_BURGLARY_JOB		RETURN 2.0
	ENDSWITCH
	RETURN 5.0
ENDFUNC

PROC DO_SEARCH_AREA_OBJECTIVE_TEXT()
	BOOL bNeedsThe
	STRING strEntityName
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_BURGLARY_JOB
			Print_Objective_Text("FHOT_BJ_FIND")
		BREAK
		DEFAULT
			bNeedsThe = FALSE
			strEntityName = GET_VARIATION_SEARCH_AREA_NAME_FOR_OBJECTIVE_TEXT(bNeedsThe)
			Print_Objective_Text_With_Two_Strings_Coloured(GET_MISSION_SEARCH_OBJECTIVE_TEXT_LABEL(bNeedsThe), strEntityName, GET_VARIATION_SEARCH_ENTITY_NAME_FOR_OBJECTIVE_TEXT(), GET_HUD_COLOUR_FOR_SEARCH_AREA())
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL PERFORM_SEARCH_AREA_OBJECTIVE_TEXT()
	IF SHOULD_DO_SEARCH_AREA()
		DO_SEARCH_AREA_OBJECTIVE_TEXT()
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL PERFORM_RECOVER_MISSION_ENTITY_OBJECTIVE_TEXT()
	
	IF IS_ANY_MISSION_ENTITY_HELD_BY_RIVALS()
		TRIGGER_HELP(eHELPTEXT_RIVAL_EXPLAIN)
		IF NOT IS_PLAYER_IN_POSSESSION_OF_ANY_MISSION_ENTITY(PLAYER_ID())
			Print_Objective_Text_With_Coloured_Text_Label("FHOT_RECOVER", GET_ITEM_LABEL_FOR_MISSION(FALSE), HUD_COLOUR_RED)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_VARIATION_PERFORM_TAKE_OUT_TARGETS_OBJECTIVE()

//	SWITCH GET_GANGOPS_VARIATION()
//		CASE GOV_FORCED_ENTRY
//				
//			RETURN TRUE
//	ENDSWITCH	

	RETURN FALSE
ENDFUNC

FUNC STRING GET_PREREQUISITE_VEHICLE_OBJECTIVE_TEXT_LABEL()
	RETURN "FHOT_COLLECTa"
ENDFUNC

PROC DO_PROTECT_ENTITY_OBJECTIVE()
	TEXT_LABEL_15 targetEntity
	TEXT_LABEL_15 label
	
	label = "SMOT_ESCORT"
	targetEntity = "SMOT_ESCTIT"
	
	Print_Objective_Text_With_Coloured_Text_Label(label, targetEntity, HUD_COLOUR_BLUE)
ENDPROC

PROC DO_ELIMINATE_OBJECTIVE()
	
	TEXT_LABEL_15 targetEntity
	TEXT_LABEL_15 label
	HUD_COLOURS eColour = HUD_COLOUR_RED
	
	label = "FHOT_ELIMINATE"
	targetEntity = "SMOT_ENEMIES"
	
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_FORCED_ENTRY					
			label = "FH_FE_GT1"			// Take out the
			targetEntity = "FH_FE_GTL1"	// gang members
		BREAK
		CASE GOV_BURGLARY_JOB
		CASE GOV_GONE_BALLISTIC
			label = "FH_FE_GT1"				// Take out the
			targetEntity = "FHOT_GUARDS"	// guards
		BREAK
		CASE GOV_DAYLIGHT_ROBBERY
			IF IS_SERVER_BIT_SET(eSERVERBITSET_MAKE_ALL_PEDS_REACT)
				label = "DAY_TARGT_4"		// Take out the ~a~.
			ELSE
				label = "DAY_TARGT_3"		// Knock out the ~a~.
			ENDIF
			targetEntity = "DAY_TARGT"		// target
			
			IF IS_SERVER_BIT_SET(eSERVERBITSET_DAYLIGHT_ROBBERY_PED_DEAD)
				EXIT
			ENDIF
		BREAK
		CASE GOV_FLIGHT_RECORDER
			IF IS_SERVER_BIT_SET(eSERVERBITSET_FLIGHT_RECORDER_VEHICLE_ON_ROUTE_TO_CRASH_SITE)
				label = "FHOT_FOLLOWCS"			// Follow the ~a~~s~ to the crash site.
				eColour = HUD_COLOUR_BLUE
			ELSE
				label = "FHOT_DAMAGE"			// Damage the ~a~.
			ENDIF
			targetEntity = "FHOT_JET"		// jet
		BREAK
	ENDSWITCH
	
	Print_Objective_Text_With_Coloured_Text_Label(label, targetEntity, eColour)
ENDPROC

FUNC BOOL PERFORM_TAKE_OUT_TARGETS_OBJECTIVE_TEXT()

	IF NOT SHOULD_VARIATION_PERFORM_TAKE_OUT_TARGETS_OBJECTIVE()
		RETURN FALSE
	ENDIF
	
	DO_ELIMINATE_OBJECTIVE()
	RETURN TRUE
	
ENDFUNC

PROC DO_GET_PREREQUISITE_VEHICLE_OBJECTIVE_TEXT()
	Print_Objective_Text(GET_PREREQUISITE_VEHICLE_OBJECTIVE_TEXT_LABEL())							
ENDPROC

PROC DO_WAIT_FOR_WANTED_OBJECTIVE_TEXT()
	Print_Objective_Text("GR_OBJ_WAITW")
ENDPROC

PROC DO_WAIT_FOR_ENTITY_OBJECTIVE_TEXT()
	TEXT_LABEL_15 objectiveText
	TEXT_LABEL_15 targetEntity = GET_ITEM_LABEL_FOR_MISSION(FALSE)
	
	objectiveText = "FHT_WAITFOR"

	Print_Objective_Text_With_Coloured_Text_Label(objectiveText, targetEntity, HUD_COLOUR_WHITE)
ENDPROC

SCRIPT_TIMER stTextDelay

PROC DO_DELIVER_MISSION_ENTITY_OBJECTIVE_TEXT()
	TEXT_LABEL_15 objectiveText
	TEXT_LABEL_15 targetEntity = GET_ITEM_LABEL_FOR_MISSION(FALSE)
	
	objectiveText = "FHT_DELDROP"

	Print_Objective_Text_With_Coloured_Text_Label(objectiveText, targetEntity, HUD_COLOUR_WHITE)	
ENDPROC

PROC DO_HELP_DELIVER_OBJECTIVE_TEXT_BIG_MESSAGE()
	TEXT_LABEL_15 objectiveText
	TEXT_LABEL_15 targetEntity = GET_ITEM_LABEL_FOR_MISSION(FALSE)
	
	objectiveText = "FHOT_HLPDROP"

	Print_Objective_Text_With_Coloured_Text_Label(objectiveText, targetEntity, HUD_COLOUR_WHITE)
ENDPROC

PROC DO_COLLECT_CONTAINER_OBJECTIVE_TEXT()
	IF NOT Has_This_MP_Objective_Text_Been_Received("SMOBJ_COLCON")
		Print_Objective_Text("SMOBJ_COLCON")
	ENDIF
ENDPROC

///////////////////////////////////
////         CORONAS  	       ////
///////////////////////////////////

FUNC BOOL SHOULD_PROCESS_DROP_OFF_CORONAS(INT iDropOff)

	UNUSED_PARAMETER(iDropoff)
//	IF MissionEntityHolderPlayerID[iDropOff] != PLAYER_ID()
//		RETURN TRUE
//	ENDIF

	RETURN TRUE
ENDFUNC

PROC DRAW_DROP_OFF_CORONA(INT iDropOff)

	INT iR, iG, iB, iA
	FLOAT iRadius = GET_DROP_RADIUS()
	
	GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)	
	
	VECTOR vCoords = GET_DROP_OFF_COORDS(iDropOff)
	vCoords.z -= 0.7
	
	DRAW_MARKER(	MARKER_CYLINDER,
					vCoords,
					<<0,0,0>>,
					<<0,0,0>>,
					<<iRadius*1.8, iRadius*1.8, 2.5 >>,
					iR,
					iG,
					iB,
					150)
ENDPROC

FUNC BOOL SHOULD_DRAW_GOTO_CORONA()
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DRAW_GOTO_MARKER()
	RETURN FALSE
ENDFUNC

FUNC VECTOR GOTO_CORONA_SCALE()

	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_FORCED_ENTRY
			RETURN <<0.5,0.5,1.0>>
		BREAK
		CASE GOV_DAYLIGHT_ROBBERY
			RETURN <<LOCATE_SIZE_MISSION_TRIGGER,LOCATE_SIZE_MISSION_TRIGGER,LOCATE_SIZE_HEIGHT>>
		BREAK
		CASE GOV_BURGLARY_JOB
		CASE GOV_CAR_COLLECTOR
			RETURN <<0.5,0.5,1.0>>
	ENDSWITCH

	RETURN <<1.0,1.0,1.0>>
ENDFUNC

PROC DRAW_GOTO_CORONA(VECTOR vCoords)
	
	INT iR, iG, iB, iA
	
	GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)	

	vCoords.z -= 0.2
	
	DRAW_MARKER(	MARKER_CYLINDER,
					vCoords,
					<<0,0,0>>,
					<<0,0,0>>,
					GOTO_CORONA_SCALE(),
					iR,
					iG,
					iB,
					150)
						
ENDPROC

PROC DRAW_GOTO_MARKER(VECTOR vCoords)
	
	INT iR, iG, iB, iA
	
	GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)	
					
	DRAW_MARKER(MARKER_RING, vCoords, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>, <<20.0, 20.0, 20.0>>, iR, iG, iB, 100, FALSE, TRUE)
	DRAW_MARKER(MARKER_PLANE, vCoords, <<0.0,0.0,0.0>>, <<270.0,0.0,0.0>>, <<10.0, 10.0, 10.0>>, iR, iG, iB, 100, FALSE, TRUE)
						
ENDPROC

///////////////////////////////////
////          BLIPS  	       ////
///////////////////////////////////

FUNC BOOL SHOULD_SET_BLIP_ROUTE_FOR_MISSION_ENTITY_BLIP(INT iMissionEntity)
	UNUSED_PARAMETER(iMissionEntity)
//	IF IS_THIS_VARIATION_A_SETUP_MISSION()
//		RETURN TRUE
//	ENDIF
	RETURN FALSE // We dont want gps routes to blue mission entity blips. Remove this and add conditions for TRUE if needed. 
ENDFUNC

FUNC BOOL SHOULD_MISSION_ENTITY_BLIP_FLASH_WHEN_FIRST_ADDED(INT iMissionEntity)
	UNUSED_PARAMETER(iMissionEntity)
	RETURN TRUE
ENDFUNC

FUNC BOOL DOES_ANY_MISSION_ENTITY_BLIP_EXIST()
	INT iMissionEntity
	REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
		IF DOES_BLIP_EXIST(blipMissionEntity[iMissionEntity])
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DISPLAY_MISSION_ENTITY_BLIP(INT iMissionEntity)

	IF HAS_MISSION_ENTITY_BEEN_DELIVERED_OR_DESTROYED(iMissionEntity)
		RETURN FALSE
	ENDIF

	IF SHOULD_MISSION_ENTITY_BE_UNBLIPPED_UNTIL_FOUND()
	AND NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity,eMISSIONENTITYBITSET_FOUND)
		RETURN FALSE
	ENDIF
	
	IF SHOULD_VARIATION_PERFORM_TAKE_OUT_TARGETS_OBJECTIVE()
	AND NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_CREATED)
		RETURN FALSE
	ENDIF
	
	IF MissionEntityHolderPlayerID[iMissionEntity] != INVALID_PLAYER_INDEX()
	AND IS_MISSION_ENTITY_DELIVERY_DISTANCE_FROM_DROP_OFF(iMissionEntity)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_WARP_IN_OPEN_WORLD_INTERIOR()
	AND IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_I_AM_IN_A_MISSION_INTERIOR) != IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_AM_INSIDE_MISSION_INTERIOR)
		RETURN FALSE
	ENDIF
	
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_CAR_COLLECTOR		
			IF GET_MODE_STATE() = eMODESTATE_FIND_ITEM
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH

	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_ALWAYS_SHOW_HEIGHT_INDICATOR_ON_EDGE_MISSION_BLIP()
	RETURN FALSE
ENDFUNC

PROC ACTIVATE_HEIGHT_INDICATOR_EDGE_ON_MISSION_ENTITY_BLIP(INT iMissionEntity)
	SET_BLIP_USE_HEIGHT_INDICATOR_ON_EDGE(blipMissionEntity[iMissionEntity], TRUE) 
ENDPROC

PROC DEACTIVATE_HEIGHT_INDICATOR_EDGE_ON_MISSION_ENTITY_BLIP(INT iMissionEntity)
	SET_BLIP_USE_HEIGHT_INDICATOR_ON_EDGE(blipMissionEntity[iMissionEntity], FALSE) 
ENDPROC

PROC MANAGE_MISSION_ENTITY_BLIP_HEIGHT_INDICATOR(INT iMissionEntity)
	
	IF NOT IS_MISSION_ENTITY_LOCAL_BIT_SET(iMissionEntity, eMISSIONENTITYLOCALBITSET_TURNED_ON_BLIP_HEIGHT_INDICATOR)
		IF SHOULD_ALWAYS_SHOW_HEIGHT_INDICATOR_ON_EDGE_MISSION_BLIP()
			ACTIVATE_HEIGHT_INDICATOR_EDGE_ON_MISSION_ENTITY_BLIP(iMissionEntity)
			SET_MISSION_ENTITY_LOCAL_BIT(iMissionEntity, eMISSIONENTITYLOCALBITSET_TURNED_ON_BLIP_HEIGHT_INDICATOR)
		ENDIF
	ELSE
		ACTIVATE_HEIGHT_INDICATOR_EDGE_ON_MISSION_ENTITY_BLIP(iMissionEntity)
		CLEAR_MISSION_ENTITY_LOCAL_BIT(iMissionEntity, eMISSIONENTITYLOCALBITSET_TURNED_ON_BLIP_HEIGHT_INDICATOR)
	ENDIF
	
ENDPROC

FUNC STRING GET_MISSION_ENTITY_BLIP_NAME(INT iMissionEntity = 0)
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_AUTO_SALVAGE
			IF NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_FOUND)
				RETURN "FH_TRUCK"
			ENDIF
		BREAK
			
//		CASE GOV_PARAMEDIC		RETURN "FH_AMBULANCE"
	ENDSWITCH

	RETURN "FH_HSE"
ENDFUNC

FUNC BOOL SHOULD_SHOW_HEIGHT_ON_MISSION_ENTITY_BLIP()

	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_DEAD_DROP
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

FUNC INT GET_MISSION_ENTITY_BLIP_FLASH_TIME()

	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_UNDER_CONTROL
			RETURN 14000
	ENDSWITCH

	RETURN FLASH_MISSION_ENTITY_BLIP_DEFAULT_TIME

ENDFUNC

PROC DRAW_MISSION_ENTITY_BLIP(INT iMissionEntity #IF IS_DEBUG_BUILD , INT iCall #ENDIF)
	
	#IF IS_DEBUG_BUILD
	IF MissionEntityHolderPlayerID[iMissionEntity] != INVALID_PLAYER_INDEX()
		SCRIPT_ASSERT("[FM_GANGOPS] calling DRAW_MISSION_ENTITY_BLIP when holder is > -1")
	ENDIF
	#ENDIF
	
	IF NOT DOES_BLIP_EXIST(blipMissionEntity[iMissionEntity])
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntities.netId[iMissionEntity])
			#IF IS_DEBUG_BUILD
			PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [CONTBLIP] - DRAW_MISSION_ENTITY_BLIP - adding blipMissionEntity[", iMissionEntity, "], iCall = ", iCall)
			DEBUG_PRINTCALLSTACK()
			#ENDIF
			blipMissionEntity[iMissionEntity] = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(serverBD.sMissionEntities.netId[iMissionEntity]))
			SET_BLIP_SPRITE(blipMissionEntity[iMissionEntity], GET_MISSION_ENTITY_BLIP_SPRITE(iMissionEntity))
			SET_BLIP_SCALE(blipMissionEntity[iMissionEntity], GET_GANGOPS_ENTITY_BLIP_SCALE())
			SET_BLIP_COLOUR(blipMissionEntity[iMissionEntity], GET_MISSION_ENTITY_BLIP_COLOUR(iMissionEntity))
			SET_BLIP_NAME_FROM_TEXT_FILE(blipMissionEntity[iMissionEntity], GET_MISSION_ENTITY_BLIP_NAME(iMissionEntity))
			SET_BLIP_PRIORITY(blipMissionEntity[iMissionEntity], BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
			IF SHOULD_SHOW_HEIGHT_ON_MISSION_ENTITY_BLIP()
				SHOW_HEIGHT_ON_BLIP(blipMissionEntity[iMissionEntity], TRUE)
			ENDIF
			IF NOT IS_MISSION_ENTITY_LOCAL_BIT_SET(iMissionEntity, eMISSIONENTITYLOCALBITSET_FLASHED_ON_FIRST_APPEARANCE)
				IF SHOULD_MISSION_ENTITY_BLIP_FLASH_WHEN_FIRST_ADDED(iMissionEntity)
					SET_BLIP_FLASHES(blipMissionEntity[iMissionEntity], TRUE)		
					SET_BLIP_FLASH_INTERVAL(blipMissionEntity[iMissionEntity], BLIP_FLASHING_TIME)
					SET_BLIP_FLASH_TIMER(blipMissionEntity[iMissionEntity], GET_MISSION_ENTITY_BLIP_FLASH_TIME())
					//FLASH_MINIMAP_DISPLAY()
					SET_MISSION_ENTITY_LOCAL_BIT(iMissionEntity, eMISSIONENTITYLOCALBITSET_FLASHED_ON_FIRST_APPEARANCE)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		SET_BLIP_COLOUR(blipMissionEntity[iMissionEntity], GET_MISSION_ENTITY_BLIP_COLOUR(iMissionEntity))
		IF NOT IS_ANY_MISSION_ENTITY_BLIPPED()
			IF SHOULD_SET_BLIP_ROUTE_FOR_MISSION_ENTITY_BLIP(iMissionEntity)
				IF NOT IS_MISSION_ENTITY_LOCAL_BIT_SET(iMissionEntity, eMISSIONENTITYLOCALBITSET_SET_BLIP_ROUTE)
					SET_BLIP_ROUTE(blipMissionEntity[iMissionEntity], TRUE)
					SET_MISSION_ENTITY_LOCAL_BIT(iMissionEntity, eMISSIONENTITYLOCALBITSET_SET_BLIP_ROUTE)
				ENDIF
			ENDIF
		ELSE
			IF NOT SHOULD_SET_BLIP_ROUTE_FOR_MISSION_ENTITY_BLIP(iMissionEntity)
				IF IS_MISSION_ENTITY_LOCAL_BIT_SET(iMissionEntity, eMISSIONENTITYLOCALBITSET_SET_BLIP_ROUTE)
					SET_BLIP_ROUTE(blipMissionEntity[iMissionEntity], FALSE)
					CLEAR_MISSION_ENTITY_LOCAL_BIT(iMissionEntity, eMISSIONENTITYLOCALBITSET_SET_BLIP_ROUTE)
				ENDIF
			ENDIF
		ENDIF
		MANAGE_MISSION_ENTITY_BLIP_HEIGHT_INDICATOR(iMissionEntity)
		IF GET_BLIP_SPRITE(blipMissionEntity[iMissionEntity]) != GET_MISSION_ENTITY_BLIP_SPRITE(iMissionEntity)
			SET_BLIP_SPRITE(blipMissionEntity[iMissionEntity], GET_MISSION_ENTITY_BLIP_SPRITE(iMissionEntity))
			SET_BLIP_NAME_FROM_TEXT_FILE(blipMissionEntity[iMissionEntity], GET_MISSION_ENTITY_BLIP_NAME(iMissionEntity))
		ENDIF
		IF GET_BLIP_COLOUR(blipMissionEntity[iMissionEntity]) != GET_MISSION_ENTITY_BLIP_COLOUR(iMissionEntity)
			SET_BLIP_COLOUR(blipMissionEntity[iMissionEntity], GET_MISSION_ENTITY_BLIP_COLOUR(iMissionEntity))
		ENDIF
	ENDIF
	
ENDPROC

PROC REMOVE_MISSION_ENTITY_BLIP(INT iMissionEntity #If IS_DEBUG_BUILD , INT iCall #ENDIF)
	
	IF DOES_BLIP_EXIST(blipMissionEntity[iMissionEntity])
		#IF IS_DEBUG_BUILD
		PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [CONTBLIP] - REMOVE_MISSION_ENTITY_BLIP - removing blipMissionEntity[", iMissionEntity, "], iCall = ", iCall)
		DEBUG_PRINTCALLSTACK()
		#ENDIF
		SET_BLIP_ROUTE(blipMissionEntity[iMissionEntity], FALSE)
		REMOVE_BLIP(blipMissionEntity[iMissionEntity])
	ENDIF
	IF IS_MISSION_ENTITY_LOCAL_BIT_SET(iMissionEntity, eMISSIONENTITYLOCALBITSET_SET_BLIP_ROUTE)
		IF DOES_BLIP_EXIST(blipMissionEntity[iMissionEntity])
			SET_BLIP_ROUTE(blipMissionEntity[iMissionEntity], FALSE)
		ENDIF
		CLEAR_MISSION_ENTITY_LOCAL_BIT(iMissionEntity, eMISSIONENTITYLOCALBITSET_SET_BLIP_ROUTE)
	ENDIF
			
ENDPROC

PROC SET_PLAYER_MISSION_ENTITY_BLIP(PLAYER_INDEX playerId)
	
	IF playerId != INVALID_PLAYER_INDEX()
		IF IS_NET_PLAYER_OK(playerId)
			SET_PLAYER_BLIP_AS_HIDDEN(playerId, FALSE)
//			IF DOES_THIS_THREAD_HAVE_CONTROL_OF_CUSTOM_BLIP_COLOUR_FOR_PLAYER(playerId)
//					SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(playerId, GET_MISSION_ENTITY_BLIP_COLOUR(), TRUE)
//					SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(playerId, GET_PLAYER_HUD_COLOUR(PLAYER_ID()), TRUE)
//			ENDIF
			SET_PLAYER_BLIP_AS_LONG_RANGE(playerId, TRUE)
			IF DOES_THIS_THREAD_HAVE_CONTROL_OF_FORCE_BLIP_PLAYER(playerId)
				IF NOT IS_PLAYER_FORCE_BLIPPED(playerId)
					FORCE_BLIP_PLAYER(playerId, TRUE, TRUE)
				ENDIF
			ENDIF
			SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(playerId, GET_GANGOPS_ENTITY_BLIP_SPRITE((NOT IS_THIS_VARIATION_A_VEHICLE_VARIATION())), TRUE)
			SET_FIXED_BLIP_SCALE_FOR_PLAYER(playerId, GET_GANGOPS_ENTITY_BLIP_SCALE(), TRUE)
			DISABLE_SECONDARY_OUTLINES_FOR_PLAYER_BLIP(playerId, FALSE)
			SET_BIT(iSetPlayerBlipBitSet, NATIVE_TO_INT(playerId))
		ENDIF
	ENDIF
	
ENDPROC

PROC UNSET_PLAYER_MISSION_ENTITY_BLIP(PLAYER_INDEX playerId, BOOL bForPlayerThatLeft = FALSE)

	IF playerId != INVALID_PLAYER_INDEX()
		IF IS_BIT_SET(iPlayerOkBitset, NATIVE_TO_INT(playerId))
			IF IS_BIT_SET(iSetPlayerBlipBitSet, NATIVE_TO_INT(playerId))
			OR bForPlayerThatLeft
//				IF DOES_THIS_THREAD_HAVE_CONTROL_OF_CUSTOM_BLIP_COLOUR_FOR_PLAYER(playerId)
//					SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(playerId, GET_BLIP_COLOUR_FROM_HUD_COLOUR(GET_PLAYER_HUD_COLOUR(PLAYER_ID())) /*GET_MISSION_ENTITY_BLIP_COLOUR()*/, FALSE)
//				ENDIF
				SET_PLAYER_BLIP_AS_LONG_RANGE(playerId, FALSE)
				IF DOES_THIS_THREAD_HAVE_CONTROL_OF_FORCE_BLIP_PLAYER(playerId)
					FORCE_BLIP_PLAYER(playerId, FALSE, TRUE)
				ENDIF
				SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(playerId, GET_GANGOPS_ENTITY_BLIP_SPRITE((NOT IS_THIS_VARIATION_A_VEHICLE_VARIATION())), FALSE)
				SET_FIXED_BLIP_SCALE_FOR_PLAYER(playerId, 1.0, FALSE)
				DISABLE_SECONDARY_OUTLINES_FOR_PLAYER_BLIP(playerId, FALSE)
				CLEAR_BIT(iSetPlayerBlipBitSet, NATIVE_TO_INT(playerId))
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC REMOVE_DROP_OFF_BLIP(INT iDropOff)

	IF DOES_BLIP_EXIST(blipDropOff[iDropOff])
		SET_BLIP_ROUTE(blipDropOff[iDropOff], FALSE)
		REMOVE_BLIP(blipDropOff[iDropOff])
		#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		#ENDIF
	ENDIF
	
	IF DOES_BLIP_EXIST(blipDropOffRadius[iDropOff])
		SET_BLIP_ROUTE(blipDropOffRadius[iDropOff], FALSE)
		REMOVE_BLIP(blipDropOffRadius[iDropOff])
		#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		#ENDIF
	ENDIF
	
	// remove radius blip
	
	IF IS_DROP_OFF_LOCAL_BIT_SET(iDropOff, eLOCALDROPOFFBITSET_SET_BLIP_ROUTE)
		CLEAR_DROP_OFF_LOCAL_BIT(iDropOff, eLOCALDROPOFFBITSET_SET_BLIP_ROUTE)
	ENDIF
	
ENDPROC

PROC REMOVE_ALL_DROP_OFF_BLIPS()
	INT i
	REPEAT FREEMODE_DELIVERY_MAX_ACTIVE_DROPOFFS i
		REMOVE_DROP_OFF_BLIP(i)
	ENDREPEAT
ENDPROC


PROC REMOVE_MISSION_MEET_UP_AREA_BLIP()
	IF DOES_BLIP_EXIST(blipMeetupArea)
		REMOVE_BLIP(blipMeetupArea)
	ENDIF
ENDPROC

PROC REMOVE_AREA_BLIP()
	IF DOES_BLIP_EXIST(blipRadius)
		REMOVE_BLIP(blipRadius)
	ENDIF
ENDPROC

PROC REMOVE_ALL_MISSION_ENTITY_BUY_BLIPS(BOOL bIncludePlayers = TRUE, BOOL bIncludeAreaBlip = TRUE)
	
	INT i
	REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() i
		REMOVE_MISSION_ENTITY_BLIP(i #IF IS_DEBUG_BUILD , 0 #ENDIF)
	ENDREPEAT
	
	REMOVE_ALL_DROP_OFF_BLIPS()
	
	IF bIncludePlayers
		REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() i
			IF MissionEntityPreviousHolderPartID[i] != INVALID_PARTICIPANT_INDEX()
				IF NETWORK_IS_GAME_IN_PROGRESS()
					IF NETWORK_IS_PARTICIPANT_ACTIVE(MissionEntityPreviousHolderPartID[i])
						UNSET_PLAYER_MISSION_ENTITY_BLIP(NETWORK_GET_PLAYER_INDEX(MissionEntityPreviousHolderPartID[i]))
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	IF bIncludeAreaBlip
		REMOVE_AREA_BLIP()
	ENDIF
	
ENDPROC

FUNC FLOAT GET_DROP_OFF_REMOVE_GPS_DISTANCE()
//	IF NOT IS_GANGOPS_VARIATION_A_SELL_MISSION(GET_GANGOPS_VARIATION())
//		RETURN 600.0
//	ENDIF
	
	RETURN 0.0
ENDFUNC

FUNC FLOAT GET_DROP_OFF_IGNORE_NO_GPS_DISTANCE()

	RETURN 500.0
ENDFUNC

FUNC BOOL SHOULD_IGNORE_GPS_WHEN_NEAR_FACILITY()

	IF DOES_PLAYER_OWN_DEFUNCT_BASE(GB_GET_LOCAL_PLAYER_GANG_BOSS(),DEFUNCT_BASE_10)
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

PROC UPDATE_BLIP_ROUTE_FOR_DROP_OFF_BLIP(INT iMissionEntity, BOOL bDrawBlipRoute, INT iDropOff)

	#IF IS_DEBUG_BUILD
	IF bDoFlashingGpsPrints
		FLOAT fTempDistance = GET_DROP_OFF_REMOVE_GPS_DISTANCE()
		PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [FLASHINGGPS] - UPDATE_BLIP_ROUTE_FOR_DROP_OFF_BLIP - iMissionEntity = ", iMissionEntity)
		PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [FLASHINGGPS] - UPDATE_BLIP_ROUTE_FOR_DROP_OFF_BLIP - fMyDistanceFromCurrentMissionEntityDropOff = ", fMyDistanceFromCurrentMissionEntityDropOff)
		PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [FLASHINGGPS] - UPDATE_BLIP_ROUTE_FOR_DROP_OFF_BLIP - fTempDistance = ", fTempDistance)
	ENDIF
	#ENDIF
	
	UNUSED_PARAMETER(iMissionEntity)
	
	IF SHOULD_IGNORE_GPS_WHEN_NEAR_FACILITY()
		IF fMyDistanceFromCurrentMissionEntityDropOff < GET_DROP_OFF_IGNORE_NO_GPS_DISTANCE()
			IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_IGNORED_NO_GPS)
				SET_IGNORE_NO_GPS_FLAG(TRUE)
				SET_BLIP_ROUTE(blipDropOff[iDropOff], TRUE)
				SET_LOCAL_BIT(eLOCALBITSET_IGNORED_NO_GPS)
				PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [FLASHINGGPS] - UPDATE_BLIP_ROUTE_FOR_DROP_OFF_BLIP - SET_IGNORE_NO_GPS_FLAG(TRUE))")
			ENDIF
		ELSE
			IF IS_LOCAL_BIT_SET(eLOCALBITSET_IGNORED_NO_GPS)
				SET_IGNORE_NO_GPS_FLAG(FALSE)
				SET_BLIP_ROUTE(blipDropOff[iDropOff], TRUE)
				CLEAR_LOCAL_BIT(eLOCALBITSET_IGNORED_NO_GPS)
				PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [FLASHINGGPS] - UPDATE_BLIP_ROUTE_FOR_DROP_OFF_BLIP - SET_IGNORE_NO_GPS_FLAG(FALSE)")
			ENDIF
		ENDIF
	ENDIF
		
	IF fMyDistanceFromCurrentMissionEntityDropOff > GET_DROP_OFF_REMOVE_GPS_DISTANCE()
	AND bDrawBlipRoute
		#IF IS_DEBUG_BUILD
		IF bDoFlashingGpsPrints
			PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [FLASHINGGPS] - UPDATE_BLIP_ROUTE_FOR_DROP_OFF_BLIP - fMyDistanceFromCurrentMissionEntityDropOff > GET_DROP_OFF_REMOVE_GPS_DISTANCE()")
		ENDIF
		#ENDIF
		IF NOT IS_DROP_OFF_LOCAL_BIT_SET(iDropOff, eLOCALDROPOFFBITSET_SET_BLIP_ROUTE)
			SET_BLIP_ROUTE(blipDropOff[iDropOff], TRUE)
			SET_DROP_OFF_LOCAL_BIT(iDropOff, eLOCALDROPOFFBITSET_SET_BLIP_ROUTE)
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		IF bDoFlashingGpsPrints
			PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [FLASHINGGPS] - UPDATE_BLIP_ROUTE_FOR_DROP_OFF_BLIP - fMyDistanceFromCurrentMissionEntityDropOff <= GET_DROP_OFF_REMOVE_GPS_DISTANCE()")
		ENDIF
		#ENDIF
		IF IS_DROP_OFF_LOCAL_BIT_SET(iDropOff, eLOCALDROPOFFBITSET_SET_BLIP_ROUTE)
			SET_BLIP_ROUTE(blipDropOff[iDropOff], FALSE)
			CLEAR_DROP_OFF_LOCAL_BIT(iDropOff, eLOCALDROPOFFBITSET_SET_BLIP_ROUTE)
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_PROCESS_DROP_OFF_BLIP_ROUTES()
	IF IS_PLAYER_IN_POSSESSION_OF_ANY_MISSION_ENTITY(PLAYER_ID())
		RETURN TRUE
	ENDIF

	IF IS_LOCAL_PLAYER_IN_VEHICLE_WITH_ANY_MISSION_ENTITY()
	AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DROP_OFF_HAVE_RADIUS_BLIP()
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_DROP_OFF_RADIUS_BLIP_SIZE()
	RETURN 15.0
ENDFUNC

FUNC INT GET_DROP_OFF_RADIUS_BLIP_ALPHA()
	RETURN 85
ENDFUNC

PROC DRAW_DROP_OFF_BLIP(INT iDropOff, BOOL bDrawCorona = TRUE)

	IF GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID()) = INVALID_PLAYER_INDEX()
		EXIT
	ENDIF
	
	IF NOT NETWORK_IS_PLAYER_ACTIVE(GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID()))
		EXIT
	ENDIF
	
	VECTOR vBlipcoords 
	BOOL bDrawBlipRoute = TRUE
	
	IF NOT DOES_BLIP_EXIST(blipDropOff[iDropOff])
		blipDropOff[iDropOff] = ADD_BLIP_FOR_COORD(GET_DROP_OFF_COORDS(iDropOff))
		SET_BLIP_MARKER_LONG_DISTANCE(blipDropOff[iDropOff], TRUE)
		SET_BLIP_PRIORITY(blipDropOff[iDropOff], BLIP_PRIORITY_HIGHES_SPECIAL_MED)
		
		vBlipcoords = GET_DROP_OFF_COORDS(iDropOff)
		
		PRINTLN("[DLETE] DRAW_DROP_OFF_BLIP, iDropOff = ", iDropOff, " vBlipcoords = ", vBlipcoords)
	ELSE
		vBlipcoords = GET_DROP_OFF_COORDS(iDropOff)
		IF FREEMODE_DELIVERY_IS_DROP_OFF_IN_AIRPORT(GET_CURRENT_DROP_OFF(iDropOff))
			IF NOT IS_PLAYER_IN_AIRPORT_GROUNDS(PLAYER_ID())
				vBlipcoords = GET_COORDS_OF_CLOSEST_AIRPORT_GATE_TO_DROP_OFF(vBlipCoords)
				bDrawCorona = FALSE
			ENDIF
		ENDIF
		SET_BLIP_COORDS(blipDropOff[iDropOff], vBlipcoords)
		SET_BLIP_SCALE(blipDropOff[iDropOff], 1.0)
		IF SHOULD_PROCESS_DROP_OFF_BLIP_ROUTES()
			UPDATE_BLIP_ROUTE_FOR_DROP_OFF_BLIP(GET_MISSION_ENTITY_PLAYER_IS_HOLDING_OR_IN_VEHICLE_WITH(),bDrawBlipRoute,iDropOff)
		ENDIF
		IF NOT IS_DROP_OFF_LOCAL_BIT_SET(iDropOff, eMISSIONENTITYLOCALBITSET_FLASH_BLIP_FOR_ROUGH_T) 	
			IF ON_FINAL_MEET_DROP()
				FLASH_MISSION_BLIP(blipDropOff[iDropOff])
				SET_DROP_OFF_LOCAL_BIT(iDropOff, eMISSIONENTITYLOCALBITSET_FLASH_BLIP_FOR_ROUGH_T)	
			ENDIF
		ENDIF
		IF SHOULD_PROCESS_DROP_OFF_CORONAS(iDropOff)
			PRINTLN("[DLETE] MAINTAIN_DRAWING_MULTIPLE_DROP_OFFS, SHOULD_PROCESS_DROP_OFF_CORONAS ", iDropOff)
			IF bDrawCorona
				PRINTLN("[DLETE] MAINTAIN_DRAWING_MULTIPLE_DROP_OFFS, bDrawCorona ", iDropOff)
				DRAW_DROP_OFF_CORONA(iDropOff)
			ENDIF
		ENDIF
	ENDIF
	
	IF SHOULD_DROP_OFF_HAVE_RADIUS_BLIP()
		IF NOT DOES_BLIP_EXIST(blipDropOffRadius[iDropOff])
			FLOAT fRadius = GET_DROP_OFF_RADIUS_BLIP_SIZE() //FREEMODE_DELIVERY_DROPOFF_GET_IN_CAR_RADIUS(iDropOff)
			blipDropOffRadius[iDropOff] = ADD_BLIP_FOR_RADIUS(vBlipcoords, fRadius)
			SET_BLIP_COLOUR(blipDropOffRadius[iDropOff], GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_YELLOW))
			SET_BLIP_ALPHA(blipDropOffRadius[iDropOff], GET_DROP_OFF_RADIUS_BLIP_ALPHA())
			PRINTLN("[FM_GANGOPS] [", GET_CURRENT_VARIATION_STRING(), "] - DRAW_DROP_OFF_BLIP - Added radius blip for MissionEntity.", fRadius)
		ENDIF
	ENDIF
ENDPROC

PROC DRAW_SUPPRESSED_DROP_OFF_BLIP(INT iDropOff)
	IF NOT DOES_BLIP_EXIST(blipDropOff[iDropOff])
		blipDropOff[iDropOff] = ADD_BLIP_FOR_COORD(GET_DROP_OFF_COORDS(iDropOff))
		SET_BLIP_MARKER_LONG_DISTANCE(blipDropOff[iDropOff], TRUE)
		SET_BLIP_PRIORITY(blipDropOff[iDropOff], BLIP_PRIORITY_HIGHES_SPECIAL_MED)
		SET_BLIP_SCALE(blipDropOff[iDropOff], 0.5)
		#IF IS_DEBUG_BUILD
		VECTOR vBlipCoords = GET_BLIP_COORDS(blipDropOff[iDropOff])
		PRINTLN("[FM_GANGOPS] [", GET_CURRENT_VARIATION_STRING(),"] DRAW_SUPPRESSED_DROP_OFF_BLIP, Added suppressed drop off blip for drop = ", iDropOff, " at coords = ", vBlipCoords)
		#ENDIF
	ENDIF
ENDPROC

FUNC HUD_COLOURS GET_MISSION_ENTITY_BLIP_COLOUR_AS_HUD_COLOUR(INT iBlipColour)
	IF iBlipColour = BLIP_COLOUR_BLUE
		RETURN HUD_COLOUR_BLUE
	ELIF iBlipColour = BLIP_COLOUR_GREEN
		RETURN HUD_COLOUR_GREEN
	ELIF iBlipColour = BLIP_COLOUR_RED
		RETURN HUD_COLOUR_RED
	ELIF iBlipColour = BLIP_COLOUR_WHITE
		RETURN HUD_COLOUR_WHITE
	ENDIF
	
	RETURN HUD_COLOUR_YELLOW
ENDFUNC

FUNC INT GET_MISSION_ENTITY_GOD_TEXT_LABEL_COLOUR(BOOL bForTextOnly = FALSE)
	UNUSED_PARAMETER(bForTextOnly)
	RETURN GET_MISSION_ENTITY_BLIP_COLOUR(MISSION_ENTITY_VEHICLE)
ENDFUNC

PROC DRAW_MISSION_MEET_UP_AREA_BLIP()
	IF NOT DOES_BLIP_EXIST(blipMeetupArea)
		blipMeetupArea = ADD_BLIP_FOR_RADIUS(GET_MISSION_ENTITY_COORDS_MEET_UP_LOCATION(), 50.0)
		SET_BLIP_COLOUR_FROM_HUD_COLOUR(blipMeetupArea, HUD_COLOUR_YELLOW)
		SET_BLIP_ALPHA(blipMeetupArea, 85)
	ENDIF
ENDPROC

PROC DRAW_MISSION_MEET_UP_BLIP()
	IF NOT DOES_BLIP_EXIST(blipGoToPoint)
		blipGoToPoint = ADD_BLIP_FOR_COORD(GET_MISSION_ENTITY_COORDS_MEET_UP_LOCATION())
		SET_BLIP_COLOUR_FROM_HUD_COLOUR(blipGoToPoint, HUD_COLOUR_YELLOW)
		SET_BLIP_ROUTE(blipGoToPoint, TRUE)
		SET_BLIP_PRIORITY(blipGoToPoint, BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
	ENDIF
ENDPROC

PROC DRAW_MISSION_GO_TO_BLIP(BOOL bDrawGotoCorona, BOOL bDrawGotoMarker)
	IF NOT DOES_BLIP_EXIST(blipGoToPoint)
		blipGoToPoint = ADD_BLIP_FOR_COORD(GET_GOTO_LOCATION_COORDS())
		SET_BLIP_COLOUR_FROM_HUD_COLOUR(blipGoToPoint, HUD_COLOUR_YELLOW)
		SET_BLIP_ROUTE(blipGoToPoint, TRUE)
		SET_BLIP_PRIORITY(blipGoToPoint, BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
	ELSE
		IF bDrawGotoCorona
			DRAW_GOTO_CORONA(GET_GOTO_LOCATION_COORDS())
		ENDIF
		IF bDrawGotoMarker
			DRAW_GOTO_MARKER(GET_GOTO_LOCATION_COORDS())
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_BLIP_FIND_ITEM_PORTABLE_PICKUP()
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_CAR_COLLECTOR
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niFindItemPortablePickup)
			AND NOT IS_ENTITY_ATTACHED(NET_TO_ENT(serverBD.niFindItemPortablePickup))
			AND IS_SERVER_BIT_SET(eSERVERBITSET_CREATED_FIND_ITEM_PORTABLE_PICKUP)
			AND IS_SERVER_BIT_SET(eSERVERBITSET_ITEM_HOLDER_IS_DEAD)
				RETURN TRUE
			ENDIF
		BREAK
		CASE GOV_BURGLARY_JOB
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niFindItemPortablePickup)
			AND NOT IS_ENTITY_ATTACHED(NET_TO_ENT(serverBD.niFindItemPortablePickup))
			AND IS_SERVER_BIT_SET(eSERVERBITSET_CREATED_FIND_ITEM_PORTABLE_PICKUP)
			AND IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_AM_INSIDE_MISSION_INTERIOR)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC STRING GET_FIND_ITEM_PORTABLE_PICKUP_BLIP_NAME()
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_CAR_COLLECTOR		RETURN "FHB_KEY"
		CASE GOV_BURGLARY_JOB		RETURN "FHB_PHONE"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC BLIP_SPRITE GET_FIND_ITEM_PORTABLE_PICKUP_BLIP_SPRITE()
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_CAR_COLLECTOR		RETURN RADAR_TRACE_CRIM_CUFF_KEYS
		//CASE GOV_BURGLARY_JOB		RETURN 
	ENDSWITCH
	RETURN RADAR_TRACE_INVALID
ENDFUNC

FUNC FLOAT GET_FIND_ITEM_PORTABLE_PICKUP_BLIP_SCALE()
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_CAR_COLLECTOR		RETURN 1.6
		CASE GOV_BURGLARY_JOB		RETURN 0.8
	ENDSWITCH
	RETURN 1.0
ENDFUNC

PROC DRAW_FIND_ITEM_PORTABLE_PICKUP_BLIP()
	IF SHOULD_BLIP_FIND_ITEM_PORTABLE_PICKUP()
		IF NOT DOES_BLIP_EXIST(blipFindItemPortablePickup)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niFindItemPortablePickup)
				blipFindItemPortablePickup = ADD_BLIP_FOR_ENTITY(NET_TO_ENT(serverBD.niFindItemPortablePickup))
				
				SET_BLIP_ROUTE(blipFindItemPortablePickup, FALSE)
				SET_BLIP_PRIORITY(blipFindItemPortablePickup, BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
				IF GET_FIND_ITEM_PORTABLE_PICKUP_BLIP_SPRITE() != RADAR_TRACE_INVALID
					SET_BLIP_SPRITE(blipFindItemPortablePickup, GET_FIND_ITEM_PORTABLE_PICKUP_BLIP_SPRITE())
				ENDIF
				SET_BLIP_SCALE(blipFindItemPortablePickup, GET_FIND_ITEM_PORTABLE_PICKUP_BLIP_SCALE())
				SET_BLIP_COLOUR_FROM_HUD_COLOUR(blipFindItemPortablePickup, HUD_COLOUR_GREEN)
				
				FLASH_MISSION_BLIP(blipFindItemPortablePickup)
				SET_BLIP_NAME_FROM_TEXT_FILE(blipFindItemPortablePickup, GET_FIND_ITEM_PORTABLE_PICKUP_BLIP_NAME())
			ENDIF
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(blipFindItemPortablePickup)
			REMOVE_BLIP(blipFindItemPortablePickup)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_DRAW_FIND_ITEM_PORTABLE_PICKUP_MARKER()
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_CAR_COLLECTOR			RETURN DOES_BLIP_EXIST(blipFindItemPortablePickup)
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC DRAW_FIND_ITEM_PORTABLE_PICKUP_MARKER()
	IF SHOULD_DRAW_FIND_ITEM_PORTABLE_PICKUP_MARKER()
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niFindItemPortablePickup)
			INT colR, colG, colB, colA
			GET_HUD_COLOUR(HUD_COLOUR_GREEN, colR, colG, colB, colA)
			FM_EVENTDRAW_MARKER_ABOVE_ENTITY(NET_TO_ENT(serverBD.niFindItemPortablePickup), colR, colG, colB, 0.3)
		ENDIF
	ENDIF	
ENDPROC

FUNC BOOL SHOULD_BLIP_MINIGAME_OBJECT()
	RETURN FALSE
ENDFUNC

FUNC STRING GET_MINIGAME_OBJECT_BLIP_NAME()
	RETURN ""
ENDFUNC

PROC DRAW_MINIGAME_OBJECT_BLIP()
	IF SHOULD_BLIP_MINIGAME_OBJECT()
		IF NOT DOES_BLIP_EXIST(blipMinigameObject)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GET_MINIGAME_OBJECT_NETWORK_INDEX())
				blipMinigameObject = ADD_BLIP_FOR_ENTITY(NET_TO_ENT(GET_MINIGAME_OBJECT_NETWORK_INDEX()))
				
				SET_BLIP_ROUTE(blipMinigameObject, FALSE)
				SET_BLIP_PRIORITY(blipMinigameObject, BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
				SET_BLIP_COLOUR_FROM_HUD_COLOUR(blipMinigameObject, HUD_COLOUR_GREEN)
				
				FLASH_MISSION_BLIP(blipMinigameObject)
				SET_BLIP_NAME_FROM_TEXT_FILE(blipMinigameObject, GET_MINIGAME_OBJECT_BLIP_NAME())
			ENDIF
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(blipMinigameObject)
			REMOVE_BLIP(blipMinigameObject)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_DRAW_MINIGAME_OBJECT_MARKER()
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_BURGLARY_JOB			RETURN DOES_BLIP_EXIST(blipMinigameObject)
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC DRAW_MINIGAME_OBJECT_MARKER()
	IF SHOULD_DRAW_MINIGAME_OBJECT_MARKER()
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GET_MINIGAME_OBJECT_NETWORK_INDEX())
			INT colR, colG, colB, colA
			GET_HUD_COLOUR(HUD_COLOUR_GREEN, colR, colG, colB, colA)
			FM_EVENTDRAW_MARKER_ABOVE_ENTITY(NET_TO_ENT(GET_MINIGAME_OBJECT_NETWORK_INDEX()), colR, colG, colB, 0.15)
		ENDIF
	ENDIF	
ENDPROC

PROC REMOVE_MINIGAME_OBJECT_BLIP()
	IF DOES_BLIP_EXIST(blipMinigameObject)
		REMOVE_BLIP(blipMinigameObject)
	ENDIF
ENDPROC

PROC REMOVE_GOTOPOINT_BLIP()
	IF DOES_BLIP_EXIST(blipGoToPoint)
		REMOVE_BLIP(blipGoToPoint)
	ENDIF
ENDPROC

PROC REMOVE_NON_MISSION_ENTITY_PICKUP_BLIP()
	IF DOES_BLIP_EXIST(blipFindItemPortablePickup)
		REMOVE_BLIP(blipFindItemPortablePickup)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_DRAW_MULTI_DROP_OFF_BLIP(INT iDropOff)
	
	IF SHOULD_DO_LOSE_WANTED_LEVEL_OBJECTIVE()
		RETURN FALSE
	ENDIF
	
//	SWITCH GET_GANGOPS_VARIATION()
//		
//	ENDSWITCH
	
	UNUSED_PARAMETER(iDropOff)
	
	RETURN FALSE
	
ENDFUNC

PROC DRAW_SINGLE_SEPARATE_DROP_OFF_BLIPS()
	INT iMissionEntity = GET_MISSION_ENTITY_VEHICLE_I_AM_IN()
	INT iDropOff
	IF iMissionEntity != -1
		REPEAT GET_TOTAL_DROP_OFFS() iDropOff
			IF IS_DROP_OFF_ASSIGNED_TO_MISSION_ENTITY(iMissionEntity, iDropOff)
			AND NOT HAS_MISSION_ENTITY_BEEN_TO_THIS_DROP_OFF(iMissionEntity, iDropOff)
				DRAW_DROP_OFF_BLIP(iDropOff)
			ELSE
				REMOVE_DROP_OFF_BLIP(iDropOff)
			ENDIF
		ENDREPEAT
	ELSE
		REPEAT GET_TOTAL_DROP_OFFS() iDropOff
			IF NOT HAS_ANY_MISSION_ENTITY_BEEN_TO_THIS_DROP_OFF(iDropOff)
				DRAW_DROP_OFF_BLIP(iDropOff)
			ELSE
				REMOVE_DROP_OFF_BLIP(iDropOff)
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

PROC DRAW_SEQUENTIAL_DROP_OFF_BLIPS(BOOL bHidden, BOOL bDrawCorona = TRUE)
	INT iCurrentDropOff = GET_CURRENT_DROP_OFF_INT()
	INT iLoop
	
	REPEAT GET_DELIVERABLE_QUANTITY() iLoop
		IF iLoop = iCurrentDropOff
			DRAW_DROP_OFF_BLIP(iLoop, bDrawCorona)
		ELIF iLoop = iCurrentDropOff+1
		AND NOT bHidden
			DRAW_SUPPRESSED_DROP_OFF_BLIP(iLoop)
		ELSE
			REMOVE_DROP_OFF_BLIP(iLoop)
		ENDIF
	ENDREPEAT
ENDPROC

PROC DRAW_SCATTERED_FREE_FOR_ALL_DROP_OFF_BLIPS()
	INT iDropOff
	BOOL bDrawCorona = TRUE
	REPEAT GET_TOTAL_DROP_OFFS() iDropOff
		IF NOT HAS_ANY_MISSION_ENTITY_BEEN_TO_THIS_DROP_OFF(iDropOff)
			DRAW_DROP_OFF_BLIP(iDropOff, bDrawCorona)
		ELSE
			REMOVE_DROP_OFF_BLIP(iDropOff)
		ENDIF
	ENDREPEAT
ENDPROC

PROC DRAW_SCATTERED_SEPARATE_DROP_OFF_BLIPS()
	INT iDropOff	
	INT iLocalVeh = GET_MISSION_ENTITY_VEHICLE_I_AM_IN()
	
	IF iLocalVeh != -1
		IF HAS_MISSION_ENTITY_BEEN_DELIVERED(iLocalVeh)
			iLocalVeh = -1
		ENDIF
	ENDIF
	
	REPEAT GET_TOTAL_DROP_OFFS() iDropOff
		IF iLocalVeh != -1
			IF IS_DROP_OFF_ASSIGNED_TO_MISSION_ENTITY(iLocalVeh, iDropOff)
			AND NOT HAS_MISSION_ENTITY_BEEN_TO_THIS_DROP_OFF(iLocalVeh, iDropOff)
				DRAW_DROP_OFF_BLIP(iDropOff, FALSE)
				PRINTLN("DRAW_SCATTERED_SEPARATE_DROP_OFF_BLIPS, A, iDropOff = ", iDropOff)
			ELSE
				REMOVE_DROP_OFF_BLIP(iDropOff)
			ENDIF
		ELSE
			IF iVehNearest != -1
				IF NOT IS_MISSION_ENTITY_BIT_SET(iVehNearest, eMISSIONENTITYBITSET_DELIVERED)
				AND NOT IS_MISSION_ENTITY_BIT_SET(iVehNearest, eMISSIONENTITYBITSET_DESTROYED)
					IF IS_DROP_OFF_ASSIGNED_TO_MISSION_ENTITY(iVehNearest, iDropOff)
					AND NOT HAS_MISSION_ENTITY_BEEN_TO_THIS_DROP_OFF(iVehNearest, iDropOff)
						DRAW_DROP_OFF_BLIP(iDropOff, FALSE)
						PRINTLN("DRAW_SCATTERED_SEPARATE_DROP_OFF_BLIPS, A, iDropOff = ", iDropOff, " iVehNearest = ", iVehNearest)
					ELSE
						REMOVE_DROP_OFF_BLIP(iDropOff)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC DRAW_SCATTERED_SHARED_DROP_OFF_BLIPS()
	INT iDropOff
	PRINTLN("DRAW_SCATTERED_SHARED_DROP_OFF_BLIPS, GET_MISSION_ENTITY_VEHICLE_I_AM_IN() = ", GET_MISSION_ENTITY_VEHICLE_I_AM_IN())
	REPEAT GET_DELIVERABLE_QUANTITY() iDropOff
		IF IS_PLAYER_IN_POSSESSION_OF_ANY_MISSION_ENTITY(PLAYER_ID())
			IF NOT HAS_MISSION_ENTITY_BEEN_TO_THIS_DROP_OFF(GET_MISSION_ENTITY_PLAYER_IS_HOLDING(PLAYER_ID()), iDropOff)
				DRAW_DROP_OFF_BLIP(iDropOff, FALSE)
				PRINTLN("DRAW_SCATTERED_SHARED_DROP_OFF_BLIPS, DRAW_DROP_OFF_BLIP, iVehNearest = ", iVehNearest, " iDropOff = ", iDropOff)
			ELSE
				REMOVE_DROP_OFF_BLIP(iDropOff)
				PRINTLN("DRAW_SCATTERED_SHARED_DROP_OFF_BLIPS, REMOVE_DROP_OFF_BLIP, iVehNearest = ", iVehNearest, " iDropOff = ", iDropOff)
			ENDIF
		ELSE
			IF iVehNearest != -1
				PRINTLN("DRAW_SCATTERED_SHARED_DROP_OFF_BLIPS A, iVehNearest = ", iVehNearest, " iDropOff = ", iDropOff)
				IF NOT IS_MISSION_ENTITY_BIT_SET(iVehNearest, eMISSIONENTITYBITSET_DELIVERED)
				AND NOT IS_MISSION_ENTITY_BIT_SET(iVehNearest, eMISSIONENTITYBITSET_DESTROYED)
					PRINTLN("DRAW_SCATTERED_SHARED_DROP_OFF_BLIPS B, iVehNearest = ", iVehNearest, " iDropOff = ", iDropOff)
					IF NOT HAS_MISSION_ENTITY_BEEN_TO_THIS_DROP_OFF(iVehNearest, iDropOff)
						DRAW_DROP_OFF_BLIP(iDropOff, FALSE)
						PRINTLN("DRAW_SCATTERED_SHARED_DROP_OFF_BLIPS C, iVehNearest = ", iVehNearest, " iDropOff = ", iDropOff)
					ELSE
						REMOVE_DROP_OFF_BLIP(iDropOff)
						PRINTLN("DRAW_SCATTERED_SHARED_DROP_OFF_BLIPS D, iVehNearest = ", iVehNearest, " iDropOff = ", iDropOff)
					ENDIF
				ELSE
					REMOVE_DROP_OFF_BLIP(iDropOff)
					PRINTLN("DRAW_SCATTERED_SHARED_DROP_OFF_BLIPS E, iVehNearest = ", iVehNearest, " iDropOff = ", iDropOff)
				ENDIF
			ELSE
				PRINTLN("DRAW_SCATTERED_SHARED_DROP_OFF_BLIPS iVehNearest = -1 ")
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL SHOULD_DRAW_DROP_OFF_BLIPS()
	IF SHOULD_PLAYER_BE_GIVEN_DELIVERY_OBJECTIVE(PLAYER_ID())
		IF NOT SHOULD_DO_LOSE_WANTED_LEVEL_OBJECTIVE()
		AND NOT SHOULD_DO_COLLECT_CONTAINER_OBJECTIVE()
			RETURN TRUE
		ENDIF
	ELSE
		IF OK_TO_DO_REMOTE_DELIVER_HUD()
		AND HAVE_ALL_MISSION_ENTITIES_BEEN_CREATED()
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
   
PROC MAINTAIN_DROP_OFF_BLIPS()
	
	BOOL bDrawDropOffBlip = SHOULD_DRAW_DROP_OFF_BLIPS()
	
	IF GET_MODE_STATE() != eMODESTATE_DELIVER_MISSION_ENTITY
	OR NETWORK_IS_IN_MP_CUTSCENE()
	OR bDrawDropOffBlip = FALSE
		REMOVE_ALL_DROP_OFF_BLIPS()
	ELSE
		SWITCH GET_DROP_OFF_TYPE()	
		
			// One drop off, sweet and simple
			CASE eDROP_OFF_TYPE_SINGLE
				DRAW_DROP_OFF_BLIP(GET_CURRENT_DROP_OFF_INT())
			BREAK
			
			// Each vehicle has its own drop off, but only one. If in possession, draw my vehicle blip. If not, draw them all
			CASE eDROP_OFF_TYPE_SINGLE_SEPARATE
				DRAW_SINGLE_SEPARATE_DROP_OFF_BLIPS()
			BREAK
			
			// Vehicles visit the same drop in a sequence. Blip the first drop in full, blip the next drop smaller
			CASE eDROP_OFF_TYPE_SEQUENTIAL
				DRAW_SEQUENTIAL_DROP_OFF_BLIPS(FALSE)
			BREAK
			
			CASE eDROP_OFF_TYPE_SEQUENTIAL_HIDDEN
				DRAW_SEQUENTIAL_DROP_OFF_BLIPS(TRUE, FALSE)
			BREAK
			
			// Draw all potential drop off locations, vehicles can go to any. Once a vehicle has been to a drop, remove the blip.
			CASE eDROP_OFF_TYPE_SCATTERED_FREE_FOR_ALL
				DRAW_SCATTERED_FREE_FOR_ALL_DROP_OFF_BLIPS()
			BREAK
			
			// Draw all potential drop off locations specific to current vehicle. Draw blips of closest vehicle if not in one.
			CASE eDROP_OFF_TYPE_SCATTERED_SEPARATE
				DRAW_SCATTERED_SEPARATE_DROP_OFF_BLIPS()
			BREAK
			
			// Draw the set drop offs that the vehicles need to go to. Remove the blips once closest/current vehicle has been to that drop.
			CASE eDROP_OFF_TYPE_SCATTERED_SHARED
				DRAW_SCATTERED_SHARED_DROP_OFF_BLIPS()
			BREAK
		
		ENDSWITCH
		
	ENDIF
	
ENDPROC

FUNC VECTOR GET_COORDS_FOR_PROJECTILE_AREA()

	SWITCH GET_GANGOPS_SUBVARIATION()
		CASE GOS_MM_MORNINGWOOD					RETURN GET_GOTO_LOCATION_COORDS()
		CASE GOS_MM_GRAPESEED					RETURN GET_GOTO_LOCATION_COORDS()
		CASE GOS_MM_ALTA              			RETURN GET_GOTO_LOCATION_COORDS()
		CASE GOS_MM_MISSION_ROW         		RETURN GET_GOTO_LOCATION_COORDS()
		CASE GOS_MM_HIPPY_CAMP          		RETURN GET_GOTO_LOCATION_COORDS()
		CASE GOS_MM_SAN_ANDREAS        		RETURN GET_GOTO_LOCATION_COORDS()
		CASE GOS_MM_MIRROR_PARK         		RETURN GET_GOTO_LOCATION_COORDS()
		CASE GOS_MM_STRAWBERRY          		RETURN GET_GOTO_LOCATION_COORDS()
		CASE GOS_MM_MIRROR_PARK_TAVERN  		RETURN GET_GOTO_LOCATION_COORDS()
		CASE GOS_MM_ULSA             		RETURN GET_GOTO_LOCATION_COORDS()
	ENDSWITCH
	
	RETURN <<0.0,0.0,0.0>>

ENDFUNC

FUNC FLOAT GET_RADIUS_FOR_PROJECTILE_AREA()

	SWITCH GET_GANGOPS_SUBVARIATION()
		CASE GOS_MM_MORNINGWOOD					RETURN 18.0
		CASE GOS_MM_GRAPESEED					RETURN 11.0
		CASE GOS_MM_ALTA              			RETURN 18.0
		CASE GOS_MM_MISSION_ROW         		RETURN 14.0
		CASE GOS_MM_HIPPY_CAMP          		RETURN 15.0
		CASE GOS_MM_SAN_ANDREAS        			RETURN 20.0
		CASE GOS_MM_MIRROR_PARK         		RETURN 18.0
		CASE GOS_MM_STRAWBERRY          		RETURN 16.5
		CASE GOS_MM_MIRROR_PARK_TAVERN  		RETURN 15.0
		CASE GOS_MM_ULSA             			RETURN 15.0
	ENDSWITCH
	
	RETURN 25.0

ENDFUNC

FUNC BOOL SHOULD_DISABLE_BOSS_RADIUS_BLIP_IN_AREA()
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_COORDS_FOR_RADIUS_BLIP()

	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_MOB_MENTALITY			RETURN GET_COORDS_FOR_PROJECTILE_AREA()
	ENDSWITCH

	RETURN <<0.0,0.0,0.0>>
ENDFUNC

FUNC BOOL GET_COORDS_FOR_AREA_BLIP(VECTOR &v1, VECTOR &v2, FLOAT &fWidth)
	UNUSED_PARAMETER(v1)
	UNUSED_PARAMETER(v2)
	UNUSED_PARAMETER(fWidth)
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_RADIUS_FOR_RADIUS_BLIP()

	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_MOB_MENTALITY		RETURN GET_RADIUS_FOR_PROJECTILE_AREA()
	ENDSWITCH

	RETURN -1.0
ENDFUNC

FUNC BOOL HAS_PLAYER_REACHED_ROOF()

	IF IS_LOCAL_BIT_SET(eLOCALBITSET_ON_ROOF)
		RETURN TRUE
	ENDIF
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		VECTOR vCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
		VECTOR vGoto = GET_GOTO_LOCATION_COORDS()

		IF vCoords.z <= vGoto.z + 2.0
		AND NOT IS_ENTITY_IN_AIR(PLAYER_PED_ID())
			SET_LOCAL_BIT(eLOCALBITSET_ON_ROOF)
		ENDIF
	ENDIF

	RETURN IS_LOCAL_BIT_SET(eLOCALBITSET_ON_ROOF)
ENDFUNC

FUNC BOOL SHOULD_SHOW_RADIUS_BLIP()
	IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE, TRUE) 
		RETURN FALSE
	ENDIF
	
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_MOB_MENTALITY			RETURN (GET_MODE_STATE() = eMODESTATE_THROW_PROJECTILE)
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC INT GET_RADIUS_BLIP_ALPHA()
	RETURN 85
ENDFUNC

FUNC BLIP_SPRITE GET_RADIUS_BLIP_CENTRE_SPRITE()

	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_MOB_MENTALITY			RETURN RADAR_TRACE_INVALID
	ENDSWITCH

	RETURN RADAR_TRACE_FINDERS_KEEPERS
ENDFUNC

FUNC STRING GET_RADIUS_BLIP_MAP_STRING()

	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_MOB_MENTALITY			RETURN "GOB_AREA"
	ENDSWITCH

	RETURN "VEX_SEARCH"
ENDFUNC

FUNC FLOAT GET_RADIUS_BLIP_SCALE()

	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_MOB_MENTALITY
			RETURN 1.0
	ENDSWITCH

	RETURN 1.4
ENDFUNC

FUNC BOOL SHOULD_SHOW_RADIUS_CENTRE_BLIP()

	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_MOB_MENTALITY
			RETURN TRUE
	ENDSWITCH

	RETURN TRUE

ENDFUNC

PROC MAINTAIN_MISSION_ENTITY_RADIUS_BLIP()

	BOOL bDrawBlip = FALSE
	BOOL bOutsideArea = FALSE
	BOOL bDrawAreaBlip = FALSE
	
	VECTOR vCoords1, vCoords2
	FLOAT fWidth
	VECTOR vCentrePoint
	
	VECTOR vCoords = GET_COORDS_FOR_RADIUS_BLIP()
	FLOAT fRadius = GET_RADIUS_FOR_RADIUS_BLIP()
	
	bDrawAreaBlip = GET_COORDS_FOR_AREA_BLIP(vCoords1, vCoords2, fWidth)
	
	IF IS_VECTOR_ZERO(vCoords)
	AND NOT bDrawAreaBlip
		EXIT
	ENDIF
	
	IF bDrawAreaBlip
		vCentrePoint = GET_MEAN_OF_TWO_VECTORS(vCoords1, vCoords2)
	ELSE
		vCentrePoint = vCoords
	ENDIF
	
	IF SHOULD_SHOW_RADIUS_BLIP()
		bDrawBlip = TRUE            
		
		IF bDrawAreaBlip
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vCoords1, vCoords2, fWidth)
				bOutsideArea = TRUE
			ENDIF
		ELSE
			IF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vCoords, fRadius+50)
				bOutsideArea = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF bOutsideArea
		IF SHOULD_SHOW_RADIUS_CENTRE_BLIP()
			IF NOT DOES_BLIP_EXIST(blipMisc)
				blipMisc = ADD_BLIP_FOR_COORD(vCentrePoint)
				IF GET_RADIUS_BLIP_CENTRE_SPRITE() != RADAR_TRACE_INVALID
					SET_BLIP_SPRITE(blipMisc, GET_RADIUS_BLIP_CENTRE_SPRITE())
				ENDIF
				SET_BLIP_COLOUR(blipMisc, GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_YELLOW))
				SET_BLIP_ROUTE(blipMisc, TRUE)

				SET_BLIP_NAME_FROM_TEXT_FILE(blipMisc, GET_RADIUS_BLIP_MAP_STRING())
				
				SET_BLIP_SCALE(blipMisc, GET_RADIUS_BLIP_SCALE())
				SET_BLIP_PRIORITY(blipMisc, BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
			ENDIF
		ENDIF
		
		IF SHOULD_DISABLE_BOSS_RADIUS_BLIP_IN_AREA()
			SET_BOSS_PROXIMITY_BLIP_AS_DISABLED(FALSE)
		ENDIF
	ELSE
		IF SHOULD_SHOW_RADIUS_CENTRE_BLIP()
			IF DOES_BLIP_EXIST(blipMisc)
				SET_BLIP_ROUTE(blipMisc, FALSE)
				REMOVE_BLIP(blipMisc)
				PRINTLN("[FM_GANGOPS] [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_MISSION_ENTITY_RADIUS_BLIP - Removed misc blip for MissionEntity as close enough to area.")	
			ENDIF
		ENDIF
		
		IF SHOULD_DISABLE_BOSS_RADIUS_BLIP_IN_AREA()
			SET_BOSS_PROXIMITY_BLIP_AS_DISABLED(TRUE)
		ENDIF
	ENDIF
	
	IF bDrawBlip
		IF NOT DOES_BLIP_EXIST(blipRadius)
		
			IF bDrawAreaBlip
				blipRadius = ADD_BLIP_FOR_ANGLED_AREA(vCoords1,vCoords2,fWidth)
			ELSE
				blipRadius = ADD_BLIP_FOR_RADIUS(vCoords, fRadius)
			ENDIF
			
			SET_BLIP_COLOUR(blipRadius, GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_YELLOW))
			SET_BLIP_ALPHA(blipRadius, GET_RADIUS_BLIP_ALPHA())
			SET_BLIP_AS_SHORT_RANGE(blipRadius,TRUE)
			PRINTLN("[FM_GANGOPS] [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_MISSION_ENTITY_RADIUS_BLIP - Added radius blip for MissionEntity.")
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(blipRadius)
			SET_BLIP_ROUTE(blipRadius, FALSE)
			REMOVE_BLIP(blipRadius)
			PRINTLN("[FM_GANGOPS] [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_MISSION_ENTITY_RADIUS_BLIP - Removed radius blip for MissionEntity.")	
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_FLASH_BLIPS_AT_START_OF_MODE()
	
	INT iBlip
	IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_FLASHED_BLIPS_FOR_START_OF_MODE)
		IF DOES_BLIP_EXIST(blipGoToPoint)
			FLASH_MISSION_BLIP(blipGoToPoint)
			PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - flashed blipGoToPoint")
			SET_LOCAL_BIT(eLOCALBITSET_FLASHED_BLIPS_FOR_START_OF_MODE)
		ENDIF
		
		IF DOES_BLIP_EXIST(blipFindItemPortablePickup)
			FLASH_MISSION_BLIP(blipFindItemPortablePickup)
			PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - flashed blipFindItemPortablePickup")
			SET_LOCAL_BIT(eLOCALBITSET_FLASHED_BLIPS_FOR_START_OF_MODE)
		ENDIF
			
		REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iBlip
			IF DOES_BLIP_EXIST(blipMissionEntity[iBlip])
				FLASH_MISSION_BLIP(blipMissionEntity[iBlip])
				PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - flashed blipMissionEntity[", iBlip, "]")
				SET_LOCAL_BIT(eLOCALBITSET_FLASHED_BLIPS_FOR_START_OF_MODE)
			ENDIF
		ENDREPEAT
		
		REPEAT FREEMODE_DELIVERY_MAX_ACTIVE_DROPOFFS iBlip
			IF DOES_BLIP_EXIST(blipDropOff[iBlip])
				FLASH_MISSION_BLIP(blipDropOff[iBlip])
				PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - flashed blipDropOff[", iBlip, "]")
				SET_LOCAL_BIT(eLOCALBITSET_FLASHED_BLIPS_FOR_START_OF_MODE)
			ENDIF
		ENDREPEAT
	ENDIF
	
ENDPROC

PROC MAINTAIN_FLASH_MISSION_ENTITY_BLIPS_FOR_GLOBAL_PING()
	INT iMissionEntity
	REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
		IF IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_A_PARTICIPANT_HAS_ENABLED_MY_GLOBAL_BLIP)
			IF NOT IS_MISSION_ENTITY_LOCAL_BIT_SET(iMissionEntity, eMISSIONENTITYLOCALBITSET_FLASHED_BLIP_FOR_GLOBAL_PING)
				IF GET_GANGOPS_VARIATION() != GOV_AUTO_SALVAGE
				OR (GET_GANGOPS_VARIATION() = GOV_AUTO_SALVAGE AND IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_FOUND))
					IF DOES_BLIP_EXIST(blipMissionEntity[iMissionEntity])
						FLASH_MISSION_BLIP(blipMissionEntity[iMissionEntity], TRUE)
						PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - flashed blipMissionEntity[", iMissionEntity, "]")
					ENDIF
				ENDIF
				FLASH_MINIMAP_DISPLAY()
				SET_MISSION_ENTITY_LOCAL_BIT(iMissionEntity, eMISSIONENTITYLOCALBITSET_FLASHED_BLIP_FOR_GLOBAL_PING)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC MAINTAIN_BLIPS()
	MAINTAIN_MISSION_ENTITY_RADIUS_BLIP()
	MAINTAIN_FLASH_BLIPS_AT_START_OF_MODE()
	MAINTAIN_FLASH_MISSION_ENTITY_BLIPS_FOR_GLOBAL_PING()
ENDPROC

PROC DRAW_RETURN_BLIP()
	IF NOT DOES_BLIP_EXIST(blipReturnRadius)
		blipReturnRadius = ADD_BLIP_FOR_RADIUS(GET_HANGAR_COORDS(serverBD.eLaunchHangarID), 200.0)
		SET_BLIP_COLOUR_FROM_HUD_COLOUR(blipReturnRadius, HUD_COLOUR_YELLOW)
		SET_BLIP_ALPHA(blipReturnRadius, GET_RADIUS_BLIP_ALPHA())
		PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - added return area blip")
	ENDIF
	
	IF NOT DOES_BLIP_EXIST(blipReturnCentre)
		blipReturnCentre = ADD_BLIP_FOR_COORD(GET_HANGAR_COORDS(serverBD.eLaunchHangarID))
		SET_BLIP_AS_SHORT_RANGE(blipReturnCentre, FALSE)
		SET_BLIP_COLOUR_FROM_HUD_COLOUR(blipReturnCentre, HUD_COLOUR_YELLOW)
		SET_BLIP_NAME_FROM_TEXT_FILE(blipReturnCentre, "SMBLIP_HANGAR")
		PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - added return centre blip")
	ENDIF
ENDPROC

PROC REMOVE_RETURN_BLIP()
	IF DOES_BLIP_EXIST(blipReturnRadius)
		REMOVE_BLIP(blipReturnRadius)
	ENDIF
	IF DOES_BLIP_EXIST(blipReturnCentre)
		REMOVE_BLIP(blipReturnCentre)
	ENDIF
ENDPROC

///////////////////////////////////
////    SECONDARY LOCAL GOTO   ////
///////////////////////////////////

FUNC BOOL SHOULD_DRAW_SECONDARY_LOCAL_GOTO_CORONA()
	IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_NOT_MOVED_SINCE_INTERIOR_WARP)
		SWITCH GET_GANGOPS_VARIATION()
			CASE GOV_FORCED_ENTRY			RETURN TRUE
			CASE GOV_CAR_COLLECTOR			RETURN SHOULD_ALLOW_ENTRY_TO_INTERIOR()
			CASE GOV_BURGLARY_JOB			RETURN TRUE
		ENDSWITCH
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_SECONDARY_LOCAL_GOTO_MAIN_LABEL(BOOL bNeedsThe)

	IF NOT IS_WARP_IN_OPEN_WORLD_INTERIOR()
		IF IS_LOCAL_PLAYER_IN_ANY_STORED_MISSION_INTERIOR()
			RETURN "FH_FE_EXT"		// Exit the ~a~.
		ENDIF
	ENDIF
	
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_DAYLIGHT_ROBBERY		
			IF DAYLIGHT_ROBBERY_PED_GOOD_TO_GO()
			AND serverBD.iChosenPed != -1
			AND !ARE_VECTORS_ALMOST_EQUAL( vCHOSEN_PED_COORDS(), 
											GET_GANGOPS_PED_SPAWN_COORDS(serverBD.iChosenPed, GET_GANGOPS_LOCATION(), GET_GANGOPS_VARIATION()),
											2.0)
			
				RETURN "DAY_TARGT_2" // Follow the ~a~~s~ to a quiet spot. 
			ELSE
				RETURN "DAY_TARGT_1" // Wait for the ~a~~s~ to leave the area.
			ENDIF
		BREAK
		CASE GOV_CAR_COLLECTOR			
			IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_INTERIOR_UNLOCKED)
			AND NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_HAVE_COLLECTED_FIND_ITEM_PORTABLE_PICKUP)
				RETURN "FH_SECGOTO_CC"		// Wait for ~a~ to open the garage.
			ENDIF
			RETURN "FH_SECGOTO"				// Enter the ~a~
		CASE GOV_BURGLARY_JOB			RETURN "FH_SECGOTO"
		CASE GOV_FORCED_ENTRY		
			IF HAVE_INITIAL_PEDS_BEEN_KILLED()
				RETURN "FH_SECGOTO"		// Enter the ~a~.
			ELSE
				RETURN "FH_FE_GT1" 		// Take out the ~a~.
			ENDIF
		BREAK
		CASE GOV_UNDER_CONTROL	
			IF NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(),eCLIENTBITSET_I_AM_INSIDE_MISSION_INTERIOR)
				RETURN "FH_SECGOTO"		// Enter the ~a~.
			ELSE
				RETURN "FH_SECGETTO" 		// Get to the ~a~.
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN "GR_GOTO_THE_OBJ"
	UNUSED_PARAMETER(bNeedsThe)
ENDFUNC

FUNC STRING GET_SECONDARY_LOCAL_GOTO_LOCATION_LABEL(BOOL &bNeedsThe)

	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_DAYLIGHT_ROBBERY		RETURN "DAY_TARGT" // target
		CASE GOV_FORCED_ENTRY		
			IF HAVE_INITIAL_PEDS_BEEN_KILLED()
				RETURN "FH_FE_GTL2"	// apartment
			ELSE
				RETURN "FH_FE_GTL1"	// gang members
			ENDIF
		BREAK
		CASE GOV_CAR_COLLECTOR			
			IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_INTERIOR_UNLOCKED)
			AND NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_HAVE_COLLECTED_FIND_ITEM_PORTABLE_PICKUP)
			AND IS_NET_PLAYER_OK(serverBD.playerFindItemHolder, FALSE)
				RETURN GET_PLAYER_NAME(serverBD.playerFindItemHolder)			// player name
			ENDIF
			RETURN "FH_SECLOC_GAR"			// garage
		
		CASE GOV_BURGLARY_JOB			RETURN "FH_FE_GTL3"		// house
		CASE GOV_UNDER_CONTROL	
			IF NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(),eCLIENTBITSET_I_AM_INSIDE_MISSION_INTERIOR)
				RETURN "FHOT_ENTCONT"		// control tower
			ELSE
				RETURN "FHOT_UPSTAIR" 		// 6th floor
			ENDIF
		BREAK
	ENDSWITCH

	RETURN ""
	UNUSED_PARAMETER(bNeedsThe)
ENDFUNC

FUNC BOOL SHOULD_DRAW_SECONDARY_LOCAL_GOTO_BLIP()
	IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_CAR_COLLECTOR
			IF IS_CLIENT_BIT_SET(PARTICIPANT_ID(),eCLIENTBITSET_I_AM_INSIDE_MISSION_INTERIOR)
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN (NOT IS_VECTOR_ZERO(GET_SECONDARY_LOCAL_GOTO_LOCATION()))
ENDFUNC

FUNC BOOL SHOULD_FLASH_SECONDARY_LOCAL_GOTO_LOCATION_BLIP()
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_FORCED_ENTRY		RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

BLIP_INDEX blipSecondaryLocalGotoLocation
PROC DRAW_SECONDARY_LOCAL_GOTO_LOCATION_BLIP(BOOL bDrawGotoCorona)
	IF SHOULD_DRAW_SECONDARY_LOCAL_GOTO_BLIP()
		IF NOT DOES_BLIP_EXIST(blipSecondaryLocalGotoLocation)
			blipSecondaryLocalGotoLocation = ADD_BLIP_FOR_COORD(GET_SECONDARY_LOCAL_GOTO_LOCATION())
			SET_BLIP_ROUTE(blipSecondaryLocalGotoLocation, TRUE)
			SET_BLIP_PRIORITY(blipSecondaryLocalGotoLocation, BLIP_PRIORITY_HIGHES_SPECIAL_MED)
			SET_BLIP_AS_SHORT_RANGE(blipSecondaryLocalGotoLocation, FALSE)
			IF SHOULD_FLASH_SECONDARY_LOCAL_GOTO_LOCATION_BLIP()
				FLASH_MISSION_BLIP(blipSecondaryLocalGotoLocation)
			ENDIF
		ELSE 
			IF bDrawGotoCorona
				DRAW_GOTO_CORONA(GET_SECONDARY_LOCAL_GOTO_LOCATION())
			ENDIF
			IF NOT ARE_VECTORS_EQUAL(GET_BLIP_COORDS(blipSecondaryLocalGotoLocation),GET_SECONDARY_LOCAL_GOTO_LOCATION())
				SET_BLIP_COORDS(blipSecondaryLocalGotoLocation, GET_SECONDARY_LOCAL_GOTO_LOCATION())
				PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - DRAW_SECONDARY_LOCAL_GOTO_LOCATION_BLIP - Updated blip")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_SECONDARY_LOCAL_GOTO_LOCATION_BLIP()
	IF DOES_BLIP_EXIST(blipSecondaryLocalGotoLocation)
		REMOVE_BLIP(blipSecondaryLocalGotoLocation)
	ENDIF
ENDPROC

FUNC HUD_COLOURS GET_SECONDARY_HUD_COLOUR()

	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_FORCED_ENTRY		
			IF NOT HAVE_INITIAL_PEDS_BEEN_KILLED()
				RETURN HUD_COLOUR_RED	
			ENDIF
		BREAK
		CASE GOV_CAR_COLLECTOR
			IF IS_CLIENT_BIT_SET(PARTICIPANT_ID(),eCLIENTBITSET_I_AM_INSIDE_MISSION_INTERIOR)
			OR (NOT IS_SERVER_BIT_SET(eSERVERBITSET_INTERIOR_UNLOCKED)
			AND NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_HAVE_COLLECTED_FIND_ITEM_PORTABLE_PICKUP))
				RETURN HUD_COLOUR_WHITE
			ENDIF
		BREAK
		CASE GOV_DAYLIGHT_ROBBERY
		
			IF IS_SERVER_BIT_SET(eSERVERBITSET_CAN_KILL_DAYLIGHT_ROBBERY_PED)
				RETURN HUD_COLOUR_RED	
			ENDIF
		
			RETURN HUD_COLOUR_BLUE
		BREAK
	ENDSWITCH
	
	RETURN HUD_COLOUR_YELLOW
ENDFUNC

PROC PRINT_SECONDARY_LOCAL_GOTO_OBJECTIVE_TEXT()
	IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
		Print_Objective_Text("FHOT_EXITFAC")
	ELSE
		BOOL bNeedsThe = FALSE
		IF GET_GANGOPS_VARIATION() = GOV_CAR_COLLECTOR
		AND NOT IS_SERVER_BIT_SET(eSERVERBITSET_INTERIOR_UNLOCKED)
		AND NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_HAVE_COLLECTED_FIND_ITEM_PORTABLE_PICKUP)
			Print_Objective_Text_With_Player_Name(GET_SECONDARY_LOCAL_GOTO_MAIN_LABEL(bNeedsThe), serverBD.playerFindItemHolder)
		ELSE
			STRING strLocName = GET_SECONDARY_LOCAL_GOTO_LOCATION_LABEL(bNeedsThe)
			Print_Objective_Text_With_Coloured_Text_Label(GET_SECONDARY_LOCAL_GOTO_MAIN_LABEL(bNeedsThe), strLocName, GET_SECONDARY_HUD_COLOUR())
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL PERFORM_SECONDARY_LOCAL_GOTO_OBJECTIVE_AND_BLIP()
	IF SHOULD_DO_SECONDARY_LOCAL_GOTO()
		REMOVE_MINIGAME_OBJECT_BLIP()
		REMOVE_GOTOPOINT_BLIP()
		REMOVE_ALL_MISSION_ENTITY_BUY_BLIPS(FALSE,FALSE)
		PRINT_SECONDARY_LOCAL_GOTO_OBJECTIVE_TEXT()
		DRAW_SECONDARY_LOCAL_GOTO_LOCATION_BLIP(SHOULD_DRAW_SECONDARY_LOCAL_GOTO_CORONA())
		RETURN TRUE
	ENDIF
	
	REMOVE_SECONDARY_LOCAL_GOTO_LOCATION_BLIP()
	RETURN FALSE
ENDFUNC


FUNC BOOL SHOULD_COORDS_BLIPS_BE_DISPLAYED()

	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_UNDER_CONTROL		
			IF (GET_MODE_STATE() = eMODESTATE_INTERACT AND NOT SHOULD_DO_SECONDARY_LOCAL_GOTO())
			OR (GET_MODE_STATE() = eMODESTATE_REACH_SCORE AND NOT IS_SERVER_BIT_SET(eSERVERBITSET_ANY_PLAYER_IN_MEET_UP_AREA))
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_COORDS_FOR_COORDS_BLIP(INT iBlip)
	
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_UNDER_CONTROL		
			SWITCH iBlip
				CASE 0		RETURN GET_INTERACT_COORDS()	
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN <<0.0,0.0,0.0>>
ENDFUNC

FUNC HUD_COLOURS GET_COLOUR_FOR_COORDS_BLIP()

	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_UNDER_CONTROL		RETURN HUD_COLOUR_GREEN
	ENDSWITCH
	
	RETURN HUD_COLOUR_WHITE

ENDFUNC

FUNC STRING GET_TEXT_LABEL_FOR_COORDS_BLIP()

	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_UNDER_CONTROL	
			SWITCH GET_GANGOPS_SUBVARIATION()
				CASE GOS_UC_SERVER_ROOM_1	
				CASE GOS_UC_SERVER_ROOM_2
				CASE GOS_UC_SERVER_ROOM_3	
				CASE GOS_UC_SERVER_ROOM_4	
				CASE GOS_UC_SERVER_ROOM_5	
				CASE GOS_UC_CONTROL_ROOM_1
				CASE GOS_UC_CONTROL_ROOM_2
				CASE GOS_UC_CONTROL_ROOM_3	
				CASE GOS_UC_CONTROL_ROOM_4	
				CASE GOS_UC_CONTROL_ROOM_5	
					RETURN "FH_BLIP_HACKC"
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN ""

ENDFUNC

PROC MAINTAIN_COORDS_BLIPS()

	INT iBlip

	IF SHOULD_COORDS_BLIPS_BE_DISPLAYED()
		REPEAT NUM_COORDS_BLIPS iBlip
			IF NOT DOES_BLIP_EXIST(blipAtCoords[iBlip])
				VECTOR vCoords = GET_COORDS_FOR_COORDS_BLIP(iBlip)
				IF NOT IS_VECTOR_ZERO(vCoords)
					blipAtCoords[iBlip] = ADD_BLIP_FOR_COORD(vCoords)
					SET_BLIP_SCALE(blipAtCoords[iBlip],1.0)
					SET_BLIP_COLOUR_FROM_HUD_COLOUR(blipAtCoords[iBlip], GET_COLOUR_FOR_COORDS_BLIP())
					SET_BLIP_NAME_FROM_TEXT_FILE(blipAtCoords[iBlip], GET_TEXT_LABEL_FOR_COORDS_BLIP())
				ENDIF
			ENDIF
		ENDREPEAT
	ELSE
		REPEAT NUM_COORDS_BLIPS iBlip
			IF DOES_BLIP_EXIST(blipAtCoords[iBlip])
				REMOVE_BLIP(blipAtCoords[iBlip])
			ENDIF
		ENDREPEAT
	ENDIF

ENDPROC

///////////////////////////////////
////    MOD MISSION VEHICLE    ////
///////////////////////////////////  

FUNC BOOL SHOULD_DO_MOD_MISSION_VEHICLE(BOOL bForDriver = TRUE)
	INT iMissionEntity
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_CAR_COLLECTOR	
			iMissionEntity = GET_MISSION_ENTITY_VEHICLE_I_AM_IN(bForDriver)
			IF iMissionEntity != (-1)
				IF NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_VEHICLE_HAS_BEEN_MODDED)
				OR IS_PLAYER_BROWSING_ITEMS_IN_SHOP_OF_TYPE(SHOP_TYPE_CARMOD)
					RETURN TRUE
				ENDIF
			ENDIF
			IF NOT bForDriver
			AND NOT HAVE_ALL_MISSION_VEHICLES_BEEN_MODDED()
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC STRING GET_MOD_MISSION_VEHICLE_MAIN_LABEL(BOOL bForDriver = TRUE)
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_CAR_COLLECTOR		
			IF bForDriver
				IF IS_PLAYER_BROWSING_ITEMS_IN_SHOP_OF_TYPE(SHOP_TYPE_CARMOD)
					RETURN "FHOT_MODRESPb"
				ENDIF
				RETURN "FHOT_MODRESPa"
			ENDIF
			RETURN "FHOT_MODRESPc"
	ENDSWITCH
	
	RETURN "FHOT_MODRESPa"
ENDFUNC

CONST_INT NUM_MOD_SHOPS 5
BLIP_INDEX blipModMissionVehicleShop[NUM_MOD_SHOPS]
FUNC BOOL SHOULD_DRAW_MOD_MISSION_BLIPS()
	IF IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_AM_INSIDE_MISSION_INTERIOR)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC REMOVE_MOD_MISSION_VEHICLE_BLIPS()
	INT iShop
	REPEAT NUM_MOD_SHOPS iShop
		IF DOES_BLIP_EXIST(blipModMissionVehicleShop[iShop])
			REMOVE_BLIP(blipModMissionVehicleShop[iShop])
		ENDIF
	ENDREPEAT
ENDPROC

PROC DRAW_MOD_MISSION_VEHICLE_BLIPS()
	IF NOT SHOULD_DRAW_MOD_MISSION_BLIPS()
		REMOVE_MOD_MISSION_VEHICLE_BLIPS()
		EXIT
	ENDIF
	
	FLOAT fClosestShopDistance = 99999.0
	INT iShop, iCount
	INT iGpsBlipIndex = -1
	REPEAT ENUM_TO_INT(GUN_SHOP_ARMORY_AVENGER) iShop
		SHOP_NAME_ENUM eShop = INT_TO_ENUM(SHOP_NAME_ENUM, iShop)
		IF GET_SHOP_TYPE_ENUM(eShop) = SHOP_TYPE_CARMOD
		AND eShop != CARMOD_SHOP_SUPERMOD
		AND eShop != CARMOD_SHOP_PERSONALMOD
			IF NOT DOES_BLIP_EXIST(blipModMissionVehicleShop[iCount])
				IF IS_SHOP_OPEN_FOR_BUSINESS(eShop)
				AND NOT IS_SHOP_TEMPORARILY_UNAVAILABLE(eShop)
					blipModMissionVehicleShop[iCount] = ADD_BLIP_FOR_COORD(GET_SHOP_COORDS(eShop))
					SET_BLIP_PRIORITY(blipModMissionVehicleShop[iCount], BLIP_PRIORITY_HIGHES_SPECIAL_MED)
					//SET_BLIP_AS_SHORT_RANGE(blipModMissionVehicleShop[iCount], TRUE)
					SET_BLIP_SPRITE(blipModMissionVehicleShop[iCount], RADAR_TRACE_CAR_MOD_SHOP)
					SET_BLIP_COLOUR_FROM_HUD_COLOUR(blipModMissionVehicleShop[iCount], HUD_COLOUR_YELLOW)
				ENDIF
			ELSE
				IF NOT IS_SHOP_OPEN_FOR_BUSINESS(eShop)
				OR IS_SHOP_TEMPORARILY_UNAVAILABLE(eShop)
					REMOVE_BLIP(blipModMissionVehicleShop[iCount])
				ELSE
					IF HAS_NET_TIMER_EXPIRED(missionVehicleModBlipTimer, 5000)
						FLOAT fDistance = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), GET_SHOP_COORDS(eShop))
						IF fDistance < fClosestShopDistance
							fClosestShopDistance = fDistance
							iGpsBlipIndex = iCount
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			iCount++
		ENDIF
	ENDREPEAT
	
	IF HAS_NET_TIMER_EXPIRED(missionVehicleModBlipTimer, 5000)
		REPEAT NUM_MOD_SHOPS iShop
			IF DOES_BLIP_EXIST(blipModMissionVehicleShop[iShop])
				IF iShop = iGpsBlipIndex
					IF NOT DOES_BLIP_HAVE_GPS_ROUTE(blipModMissionVehicleShop[iShop])
						SET_BLIP_ROUTE(blipModMissionVehicleShop[iShop], TRUE)
					ENDIF
					IF IS_BLIP_SHORT_RANGE(blipModMissionVehicleShop[iShop])
						SET_BLIP_AS_SHORT_RANGE(blipModMissionVehicleShop[iShop], FALSE)
					ENDIF
				ELSE
					IF DOES_BLIP_HAVE_GPS_ROUTE(blipModMissionVehicleShop[iShop])
						SET_BLIP_ROUTE(blipModMissionVehicleShop[iShop], FALSE)
					ENDIF
					IF NOT IS_BLIP_SHORT_RANGE(blipModMissionVehicleShop[iShop])
						SET_BLIP_AS_SHORT_RANGE(blipModMissionVehicleShop[iShop], TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		RESET_NET_TIMER(missionVehicleModBlipTimer)
	ENDIF
ENDPROC

FUNC BOOL PERFORM_MOD_MISSION_VEHICLE_OBJECTIVE_AND_BLIP(BOOL bForDriver = TRUE)
	IF SHOULD_DO_MOD_MISSION_VEHICLE(bForDriver)
		REMOVE_SECONDARY_LOCAL_GOTO_LOCATION_BLIP()
		REMOVE_ALL_MISSION_ENTITY_BUY_BLIPS(FALSE,FALSE)
		Print_Objective_Text(GET_MOD_MISSION_VEHICLE_MAIN_LABEL(bForDriver))
		DRAW_MOD_MISSION_VEHICLE_BLIPS()
		
		RETURN TRUE
	ENDIF
	
	REMOVE_MOD_MISSION_VEHICLE_BLIPS()
	RETURN FALSE
ENDFUNC

///////////////////////////////////
////  		PHONECALLS  	   ////
///////////////////////////////////  
   
 FUNC BOOL IS_SAFE_TO_PLAY_MISSION_DIALOGUE()

	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
		PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [GO_DIALOGUE] - IS_SAFE_TO_PLAY_MISSION_DIALOGUE - NOT IS_NET_PLAYER_OK - Return FALSE")
		RETURN FALSE
	ENDIF
	
	IF IS_SCREEN_FADED_OUT()
	OR IS_SCREEN_FADING_OUT()
	OR IS_SCREEN_FADING_IN()
		PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [GO_DIALOGUE] - IS_SAFE_TO_PLAY_MISSION_DIALOGUE - IS_SCREEN_FADED_OUT - Return FALSE")
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_PLAYER_IN_MP_CUTSCENE(PLAYER_ID())
		PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [GO_DIALOGUE] - IS_SAFE_TO_PLAY_MISSION_DIALOGUE - NETWORK_IS_PLAYER_IN_MP_CUTSCENE - Return FALSE")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_ANY_SIMPLE_INTERIOR_ENTRY_LOCATE()
		PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [GO_DIALOGUE] - IS_SAFE_TO_PLAY_MISSION_DIALOGUE - IS_PLAYER_IN_ANY_SIMPLE_INTERIOR_ENTRY_LOCATE - Return FALSE")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [GO_DIALOGUE] - IS_SAFE_TO_PLAY_MISSION_DIALOGUE - IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR - Return FALSE")
		RETURN FALSE
	ENDIF

	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_MP_SMPL_INTERIOR_INT")) > 0
	AND g_iSimpleInteriorState != SIMPLE_INT_STATE_IDLE
		PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [GO_DIALOGUE] - IS_SAFE_TO_PLAY_MISSION_DIALOGUE - NETWORK_IS_SCRIPT_ACTIVE - Return FALSE")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC
   
FUNC BOOL DOES_VARIATION_HAVE_UPDATE_CALL()
	RETURN FALSE
ENDFUNC   
   
FUNC BOOL SHOULD_DO_UPDATE_CALL()
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_MID_MISSION_CALL_DONE()

	IF DOES_VARIATION_HAVE_UPDATE_CALL()
		RETURN HAS_NET_TIMER_EXPIRED(UpdateMessageTimer, OPENING_MESSAGE_DELAY_TIME)
		AND IS_LOCAL_BIT_SET(eLOCALBITSET_DID_UPDATE_CALL)
		AND NOT IS_PHONE_ONSCREEN()
	ENDIF

	RETURN TRUE	

ENDFUNC

FUNC BOOL DOES_VARIATION_HAVE_RANDOM_OPENING_CALLS()
	RETURN FALSE
ENDFUNC

FUNC INT GET_MAX_RANGE_FOR_OPENING_CALL_INT(BOOL bUseGeneric)
	IF bUseGeneric
		RETURN 3
	ENDIF
	RETURN 1
ENDFUNC

FUNC TEXT_LABEL_15 GET_OPENING_CALL_LABEL(BOOL bMaleCharacter)
	BOOL bBikerGang = GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
	TEXT_LABEL_15 txtLblCall
	BOOL bUseGeneric
	//INT iNumMissionEntities = GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION()
	
	SWITCH GET_GANGOPS_VARIATION()
		// Setup
		CASE GOV_PARAMEDIC
			txtLblCall = "XMFM_PARAMOU"
		BREAK
		CASE GOV_CAR_COLLECTOR
			txtLblCall = "XMFM_CARCO1"
		BREAK
		CASE GOV_UNDER_CONTROL
			txtLblCall = "XMFM_UNDERO1"
		BREAK
		CASE GOV_FLARE_UP
			txtLblCall = "XMFM_FLAROPA"
		BREAK
		CASE GOV_FORCED_ENTRY
			txtLblCall = "XMFM_FORCEO1"
		BREAK
		CASE GOV_MOB_MENTALITY
			txtLblCall = "XMFM_MOBMOA"
		BREAK
		CASE GOV_AUTO_SALVAGE
			IF DOES_PLAYER_OWN_AN_IE_GARAGE(GB_GET_LOCAL_PLAYER_MISSION_HOST())
				txtLblCall = "XMFM_AUTOIA"
			ELSE
				txtLblCall = "XMFM_AUTOAA"
			ENDIF
		BREAK
		CASE GOV_AQUALUNGS
			txtLblCall = "XMFM_AQUAOA"
		BREAK
		CASE GOV_DEAD_DROP
			txtLblCall = "XMFM_DROPOA"
		BREAK
		CASE GOV_AMATEUR_PHOTOGRAPHY
			txtLblCall = "XMFM_AMATOA"
		BREAK
		CASE GOV_GONE_BALLISTIC
			txtLblCall = "XMFM_BALLIOA"
		BREAK
		CASE GOV_DAYLIGHT_ROBBERY
			txtLblCall = "XMFM_DAYOA"
		BREAK
		CASE GOV_BURGLARY_JOB
			txtLblCall = "XMFM_BURGOA"
		BREAK
		CASE GOV_FLIGHT_RECORDER
			txtLblCall = "XMFM_RECORO1"
		BREAK
		
		DEFAULT
			txtLblCall = "XMFM_GENO"
			bUseGeneric = TRUE
			PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - GET_OPENING_CALL_LABEL - No main text label. Mission call most likely not implemented yet. Using generic.")
		BREAK
		
	ENDSWITCH																									
	PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - GET_OPENING_CALL_LABEL - Main label: ", txtLblCall)
	
	INT iRandomCall = GET_RANDOM_INT_IN_RANGE(1, (GET_MAX_RANGE_FOR_OPENING_CALL_INT(bUseGeneric)+1))
	IF DOES_VARIATION_HAVE_RANDOM_OPENING_CALLS()
	OR bUseGeneric
		txtLblCall += iRandomCall
		PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - GET_OPENING_CALL_LABEL - Main label with random variation: ", txtLblCall)
	ENDIF
	
//	IF bMaleCharacter
//		txtLblCall += "M"
//	ELSE
//		txtLblCall += "F"
//	ENDIF
//	IF iNumMissionEntities = 1
//		txtLblCall += "S"
//	ELSE
//		txtLblCall += "M"
//	ENDIF
	
//	PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - GET_OPENING_CALL_LABEL - Main label with random variation & PA: ", txtLblCall)
	UNUSED_Parameter(bMaleCharacter)
	Unused_parameter(bBikerGang)
	
	RETURN txtLblCall
ENDFUNC

FUNC BOOL DOES_UPDATE_CALL_HAVE_RANDOM_VARIATIONS()
	RETURN FALSE
ENDFUNC

FUNC INT GET_MAX_RANGE_FOR_UPDATE_CALL_INT()
	RETURN 1
ENDFUNC

FUNC TEXT_LABEL_15 GET_UPDATE_CALL_LABEL(BOOL bMaleCharacter)
	BOOL bBikerGang = GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
	INT iRandomCall = GET_RANDOM_INT_IN_RANGE(1, GET_MAX_RANGE_FOR_UPDATE_CALL_INT()+1)
	TEXT_LABEL_15 txtLblCall
	
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_MAX
			txtLblCall = "SMGC_SPUC"
		BREAK
		
		DEFAULT
			PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - GET_UPDATE_CALL_LABEL - No main text label. Mission call most likely not implemented yet.")
			RETURN txtLblCall
	ENDSWITCH																									

	PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - GET_UPDATE_CALL_LABEL - Main label: ", txtLblCall)
	
	IF DOES_UPDATE_CALL_HAVE_RANDOM_VARIATIONS()
		txtLblCall += iRandomCall
		PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - GET_UPDATE_CALL_LABEL - Main label with random variation: ", txtLblCall)
	ENDIF
	
//	IF bMaleCharacter
//		txtLblCall += "M"
//	ELSE
//		txtLblCall += "F"
//	ENDIF
//	
//	PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - GET_OPENING_CALL_LABEL - Main label with random variation & PA: ", txtLblCall)
	
	RETURN txtLblCall
	
	UNUSED_Parameter(bMaleCharacter)
	UNUSED_Parameter(bBikerGang)
ENDFUNC

FUNC BOOL SHOULD_DO_OPENING_CALL()
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_USE_HEADSET_AUDIO()

	RETURN TRUE

ENDFUNC

FUNC BOOL SHOULD_OPENING_DIALOGUE_USE_ADDITIONAL_SPEAKER()

	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_CAR_COLLECTOR
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

PROC MAINTAIN_PHONECALLS()
	BOOL bSkipOpeningComms
	
	// Activating opening commuication
	IF GET_MODE_STATE() > eMODESTATE_INIT
		SET_LOCAL_BIT(eLOCALBITSET_DO_OPENING_COMMUNICATION)
	ENDIF

	// If there's no opening comms, set as done already without actually doing anything so we never do it. 
	IF bSkipOpeningComms 
		IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_DID_OPENING_COMMUNICATION)
			PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] [MAINTAIN_PHONECALLS] - bSkipOpeningComms = TRUE, not actually doing opening text, but setting flag as if have done, to silently bypass.")
			SET_LOCAL_BIT(eLOCALBITSET_DID_OPENING_COMMUNICATION)
		ENDIF
	ELSE
		IF IS_LOCAL_BIT_SET(eLOCALBITSET_DO_OPENING_COMMUNICATION)
		AND NOT IS_LOCAL_BIT_SET(eLOCALBITSET_DID_OPENING_COMMUNICATION)
		AND IS_SCREEN_FADED_IN()
		AND NOT IS_SCREEN_FADING_IN()
		AND NOT IS_SCREEN_FADED_OUT()
		AND NOT IS_SCREEN_FADING_OUT()
		AND IS_NET_PLAYER_OK(PLAYER_ID())
			IF NOT HAS_NET_TIMER_STARTED(InitialMessageTimer)
				PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] [MAINTAIN_PHONECALLS] - START_NET_TIMER(InitialMessageTimer)")
				START_NET_TIMER(InitialMessageTimer)
				ALLOW_DIALOGUE_IN_WATER(TRUE)
				SET_VEHICLE_CONVERSATIONS_PERSIST_NEW(TRUE, TRUE, TRUE)
			ELIF HAS_NET_TIMER_EXPIRED(InitialMessageTimer, OPENING_MESSAGE_DELAY_TIME)
				PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] [MAINTAIN_PHONECALLS] - HAS_NET_TIMER_EXPIRED(InitialMessageTimer, ",OPENING_MESSAGE_DELAY_TIME,") = TRUE.")
				BOOL bMaleCharacter = TRUE
				ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 2, NULL, GET_GANGOPS_VOICE_FOR_DIALOGUE())
				PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] [MAINTAIN_PHONECALLS] - ADD_PED_FOR_DIALOGUE")
				
				IF SHOULD_OPENING_DIALOGUE_USE_ADDITIONAL_SPEAKER()
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, GET_GANGOPS_ADDITIONAL_SPEAKER_FOR_DIALOGUE(), NULL, GET_GANGOPS_ADDITIONAL_VOICE_FOR_DIALOGUE())
					PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] [MAINTAIN_PHONECALLS] - ADD_PED_FOR_DIALOGUE - Additional.")
				ENDIF

				TEXT_LABEL_15 stCall = GET_OPENING_CALL_LABEL(bMaleCharacter)
				IF NOT SHOULD_DO_OPENING_CALL()
				OR IS_STRING_NULL_OR_EMPTY(stCall)
					SET_LOCAL_BIT(eLOCALBITSET_DID_OPENING_COMMUNICATION) 
					SET_LOCAL_BIT(eLOCALBITSET_TRIGGERED_OPENING_PHONECALL)
					SET_LOCAL_BIT(eLOCALBITSET_OPENING_PHONECALL_DONE_OR_NEVER_GOING_TO_HAPPEN)
					
					#IF IS_DEBUG_BUILD
					IF NOT SHOULD_DO_OPENING_CALL()
						PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] [MAINTAIN_PHONECALLS] - Not sending call.")
					ELIF IS_STRING_NULL_OR_EMPTY(stCall)
						PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] [MAINTAIN_PHONECALLS] - GET_OPENING_CALL_LABEL is empty. Variation call most likely unimplemented.")
					ENDIF
					#ENDIF
				ELSE

					IF SHOULD_USE_HEADSET_AUDIO()
						IF CREATE_CONVERSATION(MyLocalPedStruct, "XMFMAUD", stCall, CONV_PRIORITY_VERY_HIGH)
							SET_LOCAL_BIT(eLOCALBITSET_DID_OPENING_COMMUNICATION) 
							SET_LOCAL_BIT(eLOCALBITSET_TRIGGERED_OPENING_PHONECALL)
						ENDIF
					ELSE
						IF CHAR_CALL_PLAYER_CELLPHONE_FORCE_ANSWER(MyLocalPedStruct, GET_GANGOPS_CONTACT(), "XMFMAUD", stCall, CONV_PRIORITY_VERY_HIGH) 
							SET_LOCAL_BIT(eLOCALBITSET_DID_OPENING_COMMUNICATION) 
							SET_LOCAL_BIT(eLOCALBITSET_TRIGGERED_OPENING_PHONECALL)
						ENDIF
					ENDIF
					
					IF HAS_NET_TIMER_EXPIRED(timerOpeningFailsafe, 20000)
						SET_LOCAL_BIT(eLOCALBITSET_DID_OPENING_COMMUNICATION) 
						SET_LOCAL_BIT(eLOCALBITSET_TRIGGERED_OPENING_PHONECALL)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// Update calls
	IF SHOULD_DO_UPDATE_CALL()
		IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_DID_UPDATE_CALL)
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				BOOL bMaleCharacter = TRUE
				TEXT_LABEL_15 stCall = GET_UPDATE_CALL_LABEL(bMaleCharacter)
				ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 2, NULL, "NervousRon")
				IF CHAR_CALL_PLAYER_CELLPHONE_FORCE_ANSWER(MyLocalPedStruct, GET_GANGOPS_CONTACT(), "SMGCAUD", stCall, CONV_PRIORITY_VERY_HIGH) 
					SET_LOCAL_BIT(eLOCALBITSET_DID_UPDATE_CALL)
				ENDIF
			ENDIF
	 	ENDIF
	ENDIF
	
	IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_OPENING_PHONECALL_DONE_OR_NEVER_GOING_TO_HAPPEN)
		IF IS_LOCAL_BIT_SET(eLOCALBITSET_DID_OPENING_COMMUNICATION)
			IF IS_LOCAL_BIT_SET(eLOCALBITSET_TRIGGERED_OPENING_PHONECALL)
				SWITCH iOpeningCallFinishedTrackingStage
					CASE 0
						IF SHOULD_USE_HEADSET_AUDIO()
							IF NOT IS_CONVERSATION_STATUS_FREE()
								SET_LOCAL_BIT(eLOCALBITSET_OPENING_PHONECALL_DONE_OR_NEVER_GOING_TO_HAPPEN)
								PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_IS_OPENING_PHONECALL_FINISHED_OR_NEVER_GOING_TO_HAPPEN - Headset - Opening dialogue now playing.")
							ELIF HAS_NET_TIMER_EXPIRED(stCommunicationFailsafeTimer, ciCOMMUNICATION_FAILSAFE_TIME)
								PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_IS_OPENING_PHONECALL_FINISHED_OR_NEVER_GOING_TO_HAPPEN - Headset - Opening dialogue hasn't come through for ",ciCOMMUNICATION_FAILSAFE_TIME/1000," seconds. Moving on.")
								SET_LOCAL_BIT(eLOCALBITSET_OPENING_PHONECALL_DONE_OR_NEVER_GOING_TO_HAPPEN)
							ENDIF
						ELSE
							IF IS_PHONE_ONSCREEN()
								iOpeningCallFinishedTrackingStage++
								PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_IS_OPENING_PHONECALL_FINISHED_OR_NEVER_GOING_TO_HAPPEN - iOpeningCallFinishedTrackingStage = ", iOpeningCallFinishedTrackingStage)
							ELIF HAS_NET_TIMER_EXPIRED(stCommunicationFailsafeTimer, ciCOMMUNICATION_FAILSAFE_TIME)
								PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_IS_OPENING_PHONECALL_FINISHED_OR_NEVER_GOING_TO_HAPPEN - Phonecall hasn't come through for ",ciCOMMUNICATION_FAILSAFE_TIME/1000," seconds. Moving on.")
								SET_LOCAL_BIT(eLOCALBITSET_OPENING_PHONECALL_DONE_OR_NEVER_GOING_TO_HAPPEN)
							ENDIF
						ENDIF
					BREAK
					CASE 1
						IF NOT IS_PHONE_ONSCREEN()
							SET_LOCAL_BIT(eLOCALBITSET_OPENING_PHONECALL_DONE_OR_NEVER_GOING_TO_HAPPEN)
							iOpeningCallFinishedTrackingStage++
							PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_IS_OPENING_PHONECALL_FINISHED_OR_NEVER_GOING_TO_HAPPEN - iOpeningCallFinishedTrackingStage = ", iOpeningCallFinishedTrackingStage)
						ENDIF
					BREAK
				ENDSWITCH
			ELSE
				PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_IS_OPENING_PHONECALL_FINISHED_OR_NEVER_GOING_TO_HAPPEN - phonecall is never going to happen")
				SET_LOCAL_BIT(eLOCALBITSET_OPENING_PHONECALL_DONE_OR_NEVER_GOING_TO_HAPPEN)
			ENDIF
		ENDIF
	ENDIF
	
	// Deal with pausing the opening dialogue on transitions/fades etc
	IF SHOULD_USE_HEADSET_AUDIO()
		IF IS_LOCAL_BIT_SET(eLOCALBITSET_OPENING_PHONECALL_DONE_OR_NEVER_GOING_TO_HAPPEN)
			IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_OPENING_DIALOGUE_FINISHED)
				IF g_ConversationStatus = CONV_STATE_PLAYING
					TEXT_LABEL_23 tlCurrent = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
					TEXT_LABEL_23 sRoot = GET_OPENING_CALL_LABEL(TRUE)
					IF ARE_STRINGS_EQUAL(tlCurrent, sRoot) 
						IF NOT IS_SAFE_TO_PLAY_MISSION_DIALOGUE()
							IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
								PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
								PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_IS_OPENING_PHONECALL_FINISHED_OR_NEVER_GOING_TO_HAPPEN - Not safe for opening dialogue, pausing.")
							ENDIF
						ELSE
							IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
								PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
								PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_IS_OPENING_PHONECALL_FINISHED_OR_NEVER_GOING_TO_HAPPEN - Safe again for opening dialogue, unpausing.")
							ENDIF
						ENDIF
					ELSE
						SET_LOCAL_BIT(eLOCALBITSET_OPENING_DIALOGUE_FINISHED)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

///////////////////////////////////
////       TEXT MESSAGES  	   ////
/////////////////////////////////// 

FUNC BOOL SHOULD_SHOW_HURRY_UP_TEXT()
	IF IS_SERVER_BIT_SET(eSERVERBITSET_TIME_FOR_HURRY_UP_COLLECT_UI)
	OR IS_SERVER_BIT_SET(eSERVERBITSET_TIME_FOR_HURRY_UP_DELIVER_UI)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DO_AMBUSH_TEXT()
//	IF NOT DOES_GANGOPS_VARIATION_HAVE_AMBUSH(GET_GANGOPS_VARIATION())
//		RETURN FALSE
//	ENDIF
//	
//	SWITCH GET_GANGOPS_VARIATION()
//		CASE GOV_DEAD_DROP
//		CASE GOV_CAR_COLLECTOR
//		CASE GOV_AQUALUNGS
//		CASE GOV_FORCED_ENTRY
//		CASE GOV_AUTO_SALVAGE
//			RETURN FALSE
//	ENDSWITCH
//	
//	IF ARE_ANY_AMBUSH_PED_BLIPPED()
//		RETURN TRUE
//	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_SEND_DESTROYED_TEXT(INT iVehicle, BOOL bForCarrier)
	IF bForCarrier
	AND NOT IS_VEHICLE_A_CARRIER_VEHICLE(iVehicle)
		RETURN FALSE
	ENDIF
	IF NOT bForCarrier
	AND IS_VEHICLE_A_CARRIER_VEHICLE(iVehicle)
		RETURN FALSE
	ENDIF
	IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
	OR IS_VEHICLE_AN_AMBUSH_VEHICLE(iVehicle)
		RETURN FALSE
	ENDIF
	IF GET_VEHICLE_STATE(iVehicle) <= eVEHICLESTATE_DRIVEABLE
		RETURN FALSE
	ENDIF
	IF bForCarrier
	AND GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() > 1
	AND GET_NUM_MISSION_ENTITIES_INSIDE_CARRIER_VEHICLE() <= 1
		RETURN FALSE
	ENDIF
	
	RETURN (NOT IS_BIT_SET(iBombingRunVehicleTextBitset, iVehicle) AND IS_BIT_SET(iDestroyedVehicleBitset, iVehicle))
ENDFUNC

FUNC INT GET_NUM_TEXT_MESSAGE_VARIATIONS(INT iUpdate, SM_TEXT_MESSAGE_TYPE eType = eSMTXT_VARIATION)
	SWITCH eType
		CASE eSMTXT_AMBUSH
		CASE eSMTXT_LOSE_COPS
		CASE eSMTXT_HURRY_UP
		CASE eSMTXT_CARRIER_VEHICLE_DESTROYED
		CASE eSMTXT_SUPPORT_VEHICLE_DESTROYED
			RETURN 3
		CASE eSMTXT_VARIATION
			SWITCH GET_GANGOPS_VARIATION()
				CASE GOV_DEAD_DROP
					SWITCH iUpdate
						CASE 0		RETURN 1
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH

	RETURN 1
ENDFUNC

FUNC TEXT_LABEL_15 GET_VARIATION_TEXT_MESSAGE_LABEL(INT iUpdate)
	TEXT_LABEL_15 txtLblCall
	
	SWITCH GET_GANGOPS_VARIATION()		
		CASE GOV_DEAD_DROP						txtLblCall = "FHTXT_DDR"		BREAK
		CASE GOV_FORCED_ENTRY					txtLblCall = "FHTXT_FE"			BREAK	
		CASE GOV_MOB_MENTALITY					txtLblCall = "FHTXT_MM"			BREAK	
		CASE GOV_BURGLARY_JOB					txtLblCall = "FHTXT_BJ"			BREAK
		CASE GOV_AQUALUNGS						txtLblCall = "FHTXT_AL"			BREAK
		CASE GOV_AMATEUR_PHOTOGRAPHY			txtLblCall = "FHTXT_AP"			BREAK
		CASE GOV_FLARE_UP						txtLblCall = "FHTXT_FL"			BREAK
		CASE GOV_UNDER_CONTROL					txtLblCall = "FHTXT_UC"			BREAK
		CASE GOV_CAR_COLLECTOR					txtLblCall = "FHTXT_CC"			BREAK
		CASE GOV_PARAMEDIC						txtLblCall = "FHTXT_PM"			BREAK
		CASE GOV_DAYLIGHT_ROBBERY				txtLblCall = "FHTXT_DAY"		BREAK
		CASE GOV_FLIGHT_RECORDER				txtLblCall = "FHTXT_FR"			BREAK
		CASE GOV_GONE_BALLISTIC					txtLblCall = "FHTXT_GB"			BREAK
	ENDSWITCH	
	
	txtLblCall += iUpdate
	PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [SM_TXT] - GET_VARIATION_TEXT_MESSAGE_LABEL - Mission label with update: ", txtLblCall)
	
	RETURN txtLblCall
ENDFUNC

FUNC BOOL SHOULD_SEND_VARIATION_TEXT_MESSAGE(INT iText)
	IF IS_BIT_SET(iLocalVariationTextBitset0, iText)
		RETURN FALSE
	ENDIF

	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_DEAD_DROP
			SWITCH iText
				CASE 0 			RETURN GET_MODE_STATE() = eMODESTATE_GO_TO_POINT AND (fMyDistanceFromGoToLocation < 700.0)	
			ENDSWITCH
		BREAK
		CASE GOV_AMATEUR_PHOTOGRAPHY
			SWITCH iText
				CASE 0			
					IF iPhotoTakenForText = (-1) 
					AND GET_MODE_STATE() > eMODESTATE_TAKE_PHOTOGRAPHS
						IF HAS_NET_TIMER_EXPIRED(textDelayTimer, 3000)
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
				CASE 1	
					IF GET_MODE_STATE() = eMODESTATE_LEAVE_AREA
					AND IS_LOCAL_BIT_SET(eLOCALBITSET_INSIDE_LEAVE_AREA_WARNING_DISTANCE)
						INT iTimeLeft
						iTimeLeft =  (GET_MODE_TIME_LIMIT_INCLUDING_TUTORIAL_OFFSET() - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.ModeTimer))
						IF iTimeLeft < 60000 // 1 minute
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
				//CASE 2			RETURN (IS_LOCAL_BIT_SET(eLOCALBITSET_OPENING_DIALOGUE_FINISHED) AND IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_AM_IN_RESTRICTED_AREA_AIR))
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: For generic mission wide texts only. Check with Martin or Ryan before adding here.  
FUNC BOOL SHOULD_SEND_TEXT_TYPE_MESSAGE(SM_TEXT_MESSAGE_TYPE eType)
	IF IS_BIT_SET(iLocalTextTypeBitset0, ENUM_TO_INT(eType))
		RETURN FALSE
	ENDIF

	SWITCH eType
		CASE eSMTXT_AMBUSH							RETURN SHOULD_DO_AMBUSH_TEXT()
		CASE eSMTXT_HURRY_UP						RETURN FALSE
		CASE eSMTXT_LOSE_COPS						RETURN FALSE
		BREAK
		CASE eSMTXT_SELL_LEAVE_AREA					RETURN FALSE // LEAVE_AREA_OBJECTIVE
		CASE eSMTXT_CARRIER_VEHICLE_DESTROYED
			IF NOT DOES_MISSION_ENTITY_HAVE_CARRIER_VEHICLE(0)
				RETURN FALSE
			ENDIF
			RETURN IS_SERVER_BIT_SET(eSERVERBITSET_ANY_CARRIER_VEHICLE_DESTROYED)
		CASE eSMTXT_SUPPORT_VEHICLE_DESTROYED				RETURN FALSE
		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


FUNC TEXT_LABEL_15 GET_TEXT_MESSAGE_LABEL(INT iUpdate, SM_TEXT_MESSAGE_TYPE eType = eSMTXT_VARIATION)
	BOOL bMaleContact = TRUE
	BOOL bBespoke = FALSE
	
	INT iRandomCall = GET_RANDOM_INT_IN_RANGE(0, GET_NUM_TEXT_MESSAGE_VARIATIONS(iUpdate, eType))
	TEXT_LABEL_15 txtLblCall

	IF eType = eSMTXT_LOSE_COPS
		IF IS_THIS_VARIATION_A_SELL_MISSION()
			txtLblCall = "SMTXT_LCOPSS"
		ELIF IS_THIS_VARIATION_A_SETUP_MISSION()
			txtLblCall = "SMTXT_LCPST0M" // The cops are on to you!? I’m sorry friend I should have known you were clean. Lose them and get back here.
			bBespoke = TRUE
		ELSE
			txtLblCall = "SMTXT_LCOPS"
		ENDIF	
	ELIF eType = eSMTXT_AMBUSH
		txtLblCall = "SMTXT_AMB"
	ELIF eType = eSMTXT_HURRY_UP
		txtLblCall = "SMTXT_HURRY"
	ELIF eType = eSMTXT_SELL_LEAVE_AREA
		txtLblCall = "SMTXT_LEVARE"
	ELIF eType = eSMTXT_SUPPORT_VEHICLE_DESTROYED
		txtLblCall = "SMTXT_SUPVDES"
	ELIF eType = eSMTXT_CARRIER_VEHICLE_DESTROYED
		txtLblCall = "SMTXT_INTACT"
	ELSE
		txtLblCall = GET_VARIATION_TEXT_MESSAGE_LABEL(iUpdate)
	ENDIF
	PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [SM_TXT] - GET_TEXT_MESSAGE_LABEL - Main label: ", txtLblCall)
	
	//Return instead of going through random logic
	IF bBespoke
		RETURN txtLblCall
	ENDIF
	
	txtLblCall += iRandomCall
	PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [SM_TXT] - GET_TEXT_MESSAGE_LABEL - Main label with random variation: ", txtLblCall)
	
	IF bMaleContact
		txtLblCall += "M"
	ELSE
		txtLblCall += "F"
	ENDIF
	PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [SM_TXT] - GET_TEXT_MESSAGE_LABEL - Main label with random variation & contact: ", txtLblCall)
	
	RETURN txtLblCall
ENDFUNC

FUNC BOOL SHOULD_SET_DONE_BIT(INT iUpdate)
	UNUSED_PARAMETER(iUpdate)
	RETURN TRUE
ENDFUNC

FUNC enumCharacterList GET_CHARACTER_FOR_MISSION_TEXT(INT iUpdate)
	UNUSED_PARAMETER(iUpdate)
	RETURN GET_GANGOPS_CONTACT()
ENDFUNC

FUNC BOOL DO_TEXT_MESSAGE(INT iUpdate, SM_TEXT_MESSAGE_TYPE eType = eSMTXT_VARIATION)

	IF eType = eSMTXT_VARIATION
	AND IS_BIT_SET(iLocalVariationTextBitset0,iUpdate)
		RETURN FALSE
	ENDIF
	
	IF eType != eSMTXT_VARIATION
	AND IS_BIT_SET(iLocalTextTypeBitset0, ENUM_TO_INT(eType))
		RETURN FALSE
	ENDIF
	
	TEXT_LABEL_15 txtLblCall = GET_TEXT_MESSAGE_LABEL(iUpdate, eType)
	
	IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(GET_CHARACTER_FOR_MISSION_TEXT(iUpdate), txtLblCall, TXTMSG_LOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_DO_NOT_AUTO_UNLOCK, NO_REPLY_REQUIRED)
		IF eType = eSMTXT_VARIATION
			IF SHOULD_SET_DONE_BIT(iUpdate)
				SET_BIT(iLocalVariationTextBitset0,iUpdate)
				PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [SM_TXT] - DO_TEXT_MESSAGE - Set bit ", iUpdate)
			ENDIF
		ELSE
			SET_BIT(iLocalTextTypeBitset0, ENUM_TO_INT(eType))
			PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [SM_TXT] - DO_TEXT_MESSAGE - Set bit ", iUpdate)
		ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_TEXT_MESSAGES()

	//Controls the majority of text messages
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		INT iText
		
		// Variation texts
		REPEAT MAX_NUM_TEXT_MESSAGES iText
			IF SHOULD_SEND_VARIATION_TEXT_MESSAGE(iText)
				DO_TEXT_MESSAGE(iText)
			ENDIF
		ENDREPEAT
		
		// Special textx (Ambush, Lose Cops, Hurry)
		REPEAT ENUM_TO_INT(eSMTXT_END) iText
			IF SHOULD_SEND_TEXT_TYPE_MESSAGE(INT_TO_ENUM(SM_TEXT_MESSAGE_TYPE, iText))
				DO_TEXT_MESSAGE(0, INT_TO_ENUM(SM_TEXT_MESSAGE_TYPE, iText))
			ENDIF
		ENDREPEAT	
	ENDIF
	
ENDPROC

PROC DELETE_VARIATION_TEXT_MESSAGES()
	
	INT iText
	REPEAT MAX_NUM_TEXT_MESSAGES iText
		TEXT_LABEL_15 tlTemp = GET_TEXT_MESSAGE_LABEL(iText)
		DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER(tlTemp)
	ENDREPEAT
	
	REPEAT ENUM_TO_INT(eSMTXT_END) iText
		TEXT_LABEL_15 tlTemp = GET_TEXT_MESSAGE_LABEL(0,INT_TO_ENUM(SM_TEXT_MESSAGE_TYPE,iText))
		DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER(tlTemp)
	ENDREPEAT
	
ENDPROC

///////////////////////////////////
////       	DIALOGUE	  	   ////
/////////////////////////////////// 

FUNC BOOL HAS_DIALOGUE_BEEN_TRIGGERED(INT iDialogue)
	RETURN IS_BIT_SET(iLocalVariationDialogueBitset0, iDialogue)
ENDFUNC

FUNC BOOL HAS_DIALOGUE_FINISHED_PLAYING(INT iDialogue)
	RETURN IS_BIT_SET(iLocalVariationDialogueBitset0, iDialogue)
		AND iCurrentPlayingDialogue != iDialogue
ENDFUNC


FUNC STRING GET_VARIATION_DIALOGUE_ROOT(INT iDialogue)

	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_DAYLIGHT_ROBBERY
			SWITCH iDialogue
				CASE 0			RETURN "XMFM_DAYUA"
				CASE 1			RETURN "XMFM_DAYUC1" // There he is. Wait for him to leave.
				CASE 2			RETURN "XMFM_DAYUB" // He's on the move. Stay close but don't get detected.
				CASE 3			RETURN "XMFM_DAYUD" // No one's watching, now's your chance!
				CASE 4			RETURN "XMFM_DAYUE" // He saw you!! Get the documents and lose the heat.
				CASE 5			RETURN "XMFM_DAYUF" // Someone's alerted the cops. Lose them and bring the documents back.
				CASE 6			RETURN "XMFM_DAYUA2"
				CASE 7			RETURN "XMFM_DAYOC"
				CASE 8			RETURN "XMFM_DAYUC1"
				CASE 9			RETURN "XMFM_DAYUC2"
				CASE 10			RETURN "XMFM_DAYUC3"
			ENDSWITCH
		BREAK
		CASE GOV_DEAD_DROP
			SWITCH iDialogue
				CASE 0 			RETURN "XMFM_DROPUB"
				CASE 1 			RETURN "XMFM_DROPUA"
				CASE 2 			RETURN "XMFM_DROPUA2"
				CASE 3 			RETURN "XMFM_DROPUB2"
				CASE 4			RETURN "XMFM_STSI_A1"
				CASE 5			RETURN "XMFM_STSI_A2"
			ENDSWITCH
		BREAK
		CASE GOV_FORCED_ENTRY
			SWITCH iDialogue
				CASE 0 			RETURN "XMFM_FORCEUA"
				CASE 1 			RETURN "XMFM_FORCEUB"
				CASE 2 			RETURN "XMFM_FORCERA"
				CASE 3 			RETURN "XMFM_FORCERB"
				CASE 4 			RETURN "XMFM_FORCEBG"
				CASE 5			RETURN "XMFM_STSU_B2" //At least there are some seemingly competent people at the IAA. 
			ENDSWITCH
		BREAK
		CASE GOV_FLARE_UP
			SWITCH iDialogue
				CASE 0 			RETURN "XMFM_FLARUA"
				CASE 1 			RETURN "XMFM_FLARUB"  //My my. You all alright? 
//				CASE 2 			RETURN "XMFM_FLAROPB" //We need a NOOSE van, to get a key card, 
				CASE 3 			RETURN "XMFM_FLARUBN" //You got it? Okay. It’s back to base for you.   
				CASE 4 			RETURN "XMFM_FLARUBC" //I should probably say, we’d rather you didn’t bring the cops there
				CASE 5			RETURN "XMFM_STSU_B1" //I feel like Mrs. Rackman is not a lady we want to trifle with.
				CASE 6			RETURN "XMFM_STSU_A1" //There’s a sub off the coast.
			ENDSWITCH
		BREAK
		CASE GOV_MOB_MENTALITY
			SWITCH iDialogue
				CASE 0 			RETURN "XMFM_MOBMUA"
				CASE 1 			RETURN "XMFM_MOBMUB"
				CASE 2			RETURN "XMFM_MOBMOC"
				CASE 3			RETURN "XMFM_MOBMRI1"
				CASE 4			RETURN "XMFM_MOBMRI2"
				CASE 5			RETURN "XMFM_MOBMRI3"
				CASE 6			RETURN "XMFM_MOBMUB2"
				CASE 7 			RETURN "XMFM_STSU_A2"
				CASE 8 			RETURN "XMFM_STSU_A3"
			ENDSWITCH
		BREAK
		CASE GOV_BURGLARY_JOB
			SWITCH iDialogue
				CASE 0 			RETURN "XMFM_BURGUA"
				CASE 1			RETURN "XMFM_BURGUB"
				CASE 2 			RETURN "XMFM_BURGUC"
				CASE 3 			RETURN "XMFM_BURGUD"
				CASE 4 			RETURN "XMFM_BURGUE"
				CASE 5			RETURN "XMFM_STSI_A3"
				CASE 6			RETURN "XMFM_STSI_A4"
			ENDSWITCH
		BREAK
		CASE GOV_AQUALUNGS
			SWITCH iDialogue
				CASE 0			RETURN "XMFM_AQUAOC"
				CASE 1			RETURN "XMFM_AQUAUA"
				CASE 2			RETURN "XMFM_AQUADEL"
				CASE 3			RETURN "XMFM_AQUACHT"
				CASE 4			RETURN "XMFM_STSU_B3"
			ENDSWITCH
		BREAK	
		CASE GOV_AMATEUR_PHOTOGRAPHY
			SWITCH iDialogue
				CASE 0			RETURN "XMFM_AMATOC"
				CASE 1			RETURN "XMFM_AMATUA"
				CASE 2 			RETURN "XMFM_AMATHEL"
			ENDSWITCH
		BREAK
		
		CASE GOV_PARAMEDIC
			SWITCH iDialogue
				CASE 0		RETURN "XMFM_PARAMUA"
//				CASE 1		RETURN "XMFM_PARAMO1"
				CASE 2		RETURN "XMFM_PARAMUB"
				CASE 3		RETURN "XMFM_STIA_A2"
				CASE 4		RETURN "XMFM_STIA_A3"
			ENDSWITCH
		BREAK

		CASE GOV_UNDER_CONTROL
			SWITCH iDialogue
				CASE 0		RETURN "XMFM_UNDERUA"
				CASE 1		RETURN "XMFM_UNDERUC"
				CASE 2		RETURN "XMFM_UNDERUE"
				CASE 3		RETURN "XMFM_UNDERUB"
				CASE 4		RETURN "XMFM_UNDERUD"
				CASE 5		RETURN "XMFM_UNDEROP" 
				CASE 6		RETURN "XMFM_UNDERUF"
				CASE 7		RETURN "XMFM_UNDRUFB"
				CASE 8		RETURN "XMFM_UNDERRE"
				CASE 9		RETURN "XMFM_STIA_B2"
				CASE 10		RETURN "XMFM_STIA_A1"
				CASE 11		RETURN "XMFM_STIA_B1"
			ENDSWITCH
		BREAK
		
		CASE GOV_CAR_COLLECTOR
			SWITCH iDialogue
				CASE 0			RETURN "XMFM_CARCUA"
				CASE 1			RETURN "XMFM_CARCUB"
				CASE 2			RETURN "XMFM_CARCUC"
				CASE 3			RETURN "XMFM_CARCUD"
				CASE 4			RETURN "XMFM_CARCCRA"
				CASE 5			RETURN "XMFM_CARCCRB"
				CASE 6			RETURN "XMFM_CARCCRC"
				CASE 7			RETURN "XMFM_STIA_B3"
			ENDSWITCH
		BREAK
		
		CASE GOV_FLIGHT_RECORDER
			SWITCH iDialogue
				CASE 0			RETURN "XMFM_RECORUB"
				CASE 1			RETURN "XMFM_RECORUC"
				CASE 2			RETURN "XMFM_RECORUD"
				CASE 3			RETURN "XMFM_RECORUE"
				CASE 4			RETURN "XMFM_STSI_B1"
				CASE 5			RETURN "XMFM_STSI_B3"
			ENDSWITCH
		BREAK
		
		CASE GOV_GONE_BALLISTIC
			SWITCH iDialogue
				CASE 0			RETURN "XMFM_BALLUA"
				CASE 1			RETURN "XMFM_BALLUB"
				CASE 2			RETURN "XMFM_BALLUB2"
				CASE 3			RETURN "XMFM_BALLUB3"
				CASE 4			RETURN "XMFM_STSI_B2"
			ENDSWITCH
		BREAK
		
		CASE GOV_AUTO_SALVAGE
			SWITCH iDialogue
				CASE 0			RETURN "XMFM_AUTOUP1"
				CASE 1			RETURN "XMFM_AUTOUP2"
				CASE 2			RETURN "XMFM_AUTOUP3"
				CASE 3			RETURN "XMFM_AUTOUP4"
				CASE 4		
					SWITCH GET_RANDOM_INT_IN_RANGE(0, 3)
						CASE 0	RETURN "XMFM_AUTOUN1"
						CASE 1	RETURN "XMFM_AUTOUN2"
						CASE 2	RETURN "XMFM_AUTOUN3"
					ENDSWITCH
				BREAK
				CASE 5			
					SWITCH GET_RANDOM_INT_IN_RANGE(0, 2)
						CASE 0	RETURN "XMFM_AUTOUN4"
						CASE 1	RETURN "XMFM_AUTOUN5"
					ENDSWITCH
				BREAK
				CASE 6			RETURN "XMFM_AUTOULO"
				CASE 7			RETURN "XMFM_AUTOST1"
				CASE 8			RETURN "XMFM_AUTOST2"
				CASE 9			RETURN "XMFM_AUTOST3"
			ENDSWITCH
		BREAK
		
	ENDSWITCH
	
	RETURN ""
	
ENDFUNC

FUNC BOOL SHOULD_PLAY_VARIATION_DIALOGUE(INT iDialogue)
	IF IS_BIT_SET(iLocalVariationDialogueBitset0, iDialogue)
		RETURN FALSE
	ENDIF
	
	INT iMissionEntity

	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_DAYLIGHT_ROBBERY
			SWITCH iDialogue
				CASE 0			RETURN GET_MODE_STATE() = eMODESTATE_GO_TO_POINT AND fMyDistanceFromGoToLocation < 500.00 // This is the target! [IMAGE OF ARMS DEALER]
				CASE 1			RETURN IS_LOCAL_BIT_SET(eLOCALBITSET_NEAR_PED_ONCE) // There he is. Wait for him to leave.
				CASE 2			RETURN !IS_SERVER_BIT_SET(eSERVERBITSET_DAYLIGHT_ROBBERY_PED_DEAD) AND DAYLIGHT_ROBBERY_PED_GOOD_TO_GO() AND !IS_SERVER_BIT_SET(eSERVERBITSET_MAKE_ALL_PEDS_REACT) AND HAS_NET_TIMER_EXPIRED(dialogueTimer, 5000) // He's on the move. Stay close but don't get detected.
				CASE 3			RETURN IS_SERVER_BIT_SET(eSERVERBITSET_CAN_KILL_DAYLIGHT_ROBBERY_PED) AND NOT HAS_ALTERNATE_MISSION_CONDITION_BEEN_MET() // No one's watching, now's your chance!
				CASE 4			RETURN !IS_BIT_SET(iLocalVariationTextBitset0, 6) AND !IS_SERVER_BIT_SET(eSERVERBITSET_DAYLIGHT_ROBBERY_PED_DEAD) AND !IS_SERVER_BIT_SET(eSERVERBITSET_CAN_KILL_DAYLIGHT_ROBBERY_PED) AND IS_SERVER_BIT_SET(eSERVERBITSET_MAKE_ALL_PEDS_REACT) AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0 // He saw you!! Get the documents and lose the heat.
				CASE 5			RETURN IS_SERVER_BIT_SET(eSERVERBITSET_DAYLIGHT_ROBBERY_PED_DEAD) AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) >= GET_MAX_WANTED_LEVEL_FOR_VARIATION() AND GET_MODE_STATE() = eMODESTATE_COLLECT_MISSION_ENTITY // Someone's alerted the cops. Lose them and bring the documents back.
				CASE 6			RETURN IS_LOCAL_BIT_SET(eLOCALBITSET_PICTURE_MESSAGE_SENT)  // Trigger after players receive the image of the target on their phone
				CASE 7			RETURN HAS_DIALOGUE_FINISHED_PLAYING(7) AND HAS_NET_TIMER_EXPIRED(dialogueTimer, 1000) // Trigger seamlessly after the previous line
				CASE 8			RETURN HAS_NET_TIMER_STARTED(stNearPedTimeout) AND !IS_SERVER_BIT_SET(eSERVERBITSET_MAKE_ALL_PEDS_REACT) AND IS_INT_IN_RANGE(serverBD.iChosenPed, 0, 3) 	// Trigger one of these randomly if you enter the target’s sphere
				CASE 9			RETURN HAS_NET_TIMER_STARTED(stNearPedTimeout) AND !IS_SERVER_BIT_SET(eSERVERBITSET_MAKE_ALL_PEDS_REACT) AND IS_INT_IN_RANGE(serverBD.iChosenPed, 4, 7)		// Trigger one of these randomly if you enter the target’s sphere
				CASE 10			RETURN HAS_NET_TIMER_STARTED(stNearPedTimeout) AND !IS_SERVER_BIT_SET(eSERVERBITSET_MAKE_ALL_PEDS_REACT) AND serverBD.iChosenPed > 7						// Trigger one of these randomly if you enter the target’s sphere
			ENDSWITCH
		BREAK
		CASE GOV_DEAD_DROP
			SWITCH iDialogue
				CASE 0 			RETURN IS_SERVER_BIT_SET(eSERVERBITSET_GIVEN_GANG_WANTED_LEVEL_FOR_MISSION_ENTITY_ENTRY) AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
				CASE 1 			RETURN GET_MODE_STATE() = eMODESTATE_GO_TO_POINT AND (fMyDistanceFromGoToLocation < 700.0)
				CASE 2 			
								IF GET_MODE_STATE() = eMODESTATE_GO_TO_POINT AND HAS_DIALOGUE_FINISHED_PLAYING(1) AND HAS_NET_TIMER_EXPIRED(dialogueTimer,1000)
									RESET_NET_TIMER(dialogueTimer)
									RETURN TRUE
								ENDIF
				BREAK
				CASE 3 			RETURN GET_MODE_STATE() = eMODESTATE_DELIVER_MISSION_ENTITY AND IS_SERVER_BIT_SET(eSERVERBITSET_GIVEN_GANG_WANTED_LEVEL_FOR_MISSION_ENTITY_ENTRY) AND NOT IS_LOCAL_BIT_SET(eLOCALBITSET_REAPPLY_WANTED_ON_ACTION) AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0 AND HAS_NET_TIMER_EXPIRED(dialogueTimer,5000)
				CASE 4			
					IF IS_LOCAL_BIT_SET(eLOCALBITSET_DID_OPENING_COMMUNICATION) 
					AND HAS_NET_TIMER_EXPIRED(dialogueTimer, 20000)
						RESET_NET_TIMER(dialogueTimer)
						RETURN TRUE
					ENDIF
				BREAK
				CASE 5
					IF HAS_DIALOGUE_FINISHED_PLAYING(4)
					AND HAS_NET_TIMER_EXPIRED(dialogueTimer, 2500)
						RESET_NET_TIMER(dialogueTimer)
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE GOV_FORCED_ENTRY
			SWITCH iDialogue
				CASE 0 			RETURN HAVE_INITIAL_PEDS_BEEN_KILLED()
				CASE 1 			RETURN GET_MODE_STATE() >= eMODESTATE_DELIVER_MISSION_ENTITY AND NOT IS_LOCAL_PLAYER_IN_ANY_STORED_MISSION_INTERIOR()
				CASE 2 			RETURN serverBD.iForcedEntryDeadPedCount > 0 					//Trigger after the first dealer has been killed.
				CASE 3 			RETURN serverBD.iForcedEntryDeadPedCount > 2					//Trigger after 3 dealers have been killed.
				CASE 4 			RETURN GET_MODE_STATE() = eMODESTATE_COLLECT_MISSION_ENTITY		//Trigger once you’re on the objective to grab the duffel bag
				CASE 5			
				
					IF IS_LOCAL_BIT_SET(eLOCALBITSET_DID_OPENING_COMMUNICATION) 
					AND HAS_NET_TIMER_EXPIRED(dialogueTimer, 20000)
							RESET_NET_TIMER(dialogueTimer)
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE GOV_FLARE_UP
			SWITCH iDialogue
				CASE 0 			RETURN GET_MODE_STATE() = eMODESTATE_GO_TO_POINT AND (fMyDistanceFromGoToLocation < 200.0)
				CASE 1 			RETURN ( IS_SERVER_BIT_SET(eSERVERBITSET_DONE_FAKE_EXPLOSION) AND HAS_NET_TIMER_STARTED(serverBD.stExplode) AND HAS_NET_TIMER_EXPIRED(serverBD.stExplode, serverBD.iRandomExplodeInt + 5000) )
				CASE 2 			RETURN FALSE //IS_LOCAL_BIT_SET(eLOCALBITSET_OPENING_PHONECALL_DONE_OR_NEVER_GOING_TO_HAPPEN) AND HAS_NET_TIMER_EXPIRED(dialogueTimer,1000) 																	//Trigger this next line one second after the above line – if this seems odd we can make it seamless
				CASE 3 			RETURN GET_MODE_STATE() = eMODESTATE_DELIVER_MISSION_ENTITY AND NOT HAS_DIALOGUE_FINISHED_PLAYING(1)																						//Trigger when you get in the truck but you’ve killed all cops/crooks so the station does not blow up.
				CASE 4 			RETURN GET_MODE_STATE() = eMODESTATE_DELIVER_MISSION_ENTITY AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0 AND HAS_NET_TIMER_EXPIRED(dialogueTimer, 10000) AND HAS_NET_TIMER_STARTED(serverBD.stExplode)	//Trigger 10 seconds after getting in the van if you have the lose the cops objective
				CASE 5			
				
					IF IS_LOCAL_BIT_SET(eLOCALBITSET_DID_OPENING_COMMUNICATION) 
					AND HAS_NET_TIMER_EXPIRED(dialogueTimer, 20000)
							RESET_NET_TIMER(dialogueTimer)
						RETURN TRUE
					ENDIF
				BREAK
				
				CASE 6	
					IF GET_MODE_STATE() = eMODESTATE_DELIVER_MISSION_ENTITY
						REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntities.netId[iMissionEntity])
								IF IS_ENTITY_IN_RANGE_COORDS(NET_TO_ENT(serverBD.sMissionEntities.netId[iMissionEntity]), vMissionFacilityCoords, 500.0)
									RETURN TRUE
								ENDIF
							ENDIF
						ENDREPEAT
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE GOV_MOB_MENTALITY
			SWITCH iDialogue
				CASE 0 
					IF serverBD.iTotalScore > 0	
					AND GET_MODE_STATE() = eMODESTATE_RIOT
						RETURN TRUE
					ENDIF
				BREAK
				CASE 1 			RETURN GET_MODE_STATE() >= eMODESTATE_COLLECT_MISSION_ENTITY
				CASE 2			RETURN GET_MODE_STATE() = eMODESTATE_GO_TO_POINT AND (fMyDistanceFromGoToLocation < 1000.0)
				CASE 3			RETURN GET_MODE_STATE() = eMODESTATE_RIOT AND HAS_NET_TIMER_EXPIRED(dialogueTimer,3000)
				CASE 4			RETURN GET_MODE_STATE() = eMODESTATE_RIOT AND HAS_DIALOGUE_FINISHED_PLAYING(3) AND HAS_NET_TIMER_EXPIRED(dialogueTimer,18000)
				CASE 5			
					IF GET_MODE_STATE() = eMODESTATE_RIOT 
					AND HAS_DIALOGUE_FINISHED_PLAYING(4) 
					AND HAS_NET_TIMER_EXPIRED(dialogueTimer,28000)
						RESET_NET_TIMER(dialogueTimer)
						RETURN TRUE
					ENDIF
				BREAK
				CASE 6			RETURN GET_MODE_STATE() = eMODESTATE_DELIVER_MISSION_ENTITY
				CASE 7			RETURN (HAS_DIALOGUE_FINISHED_PLAYING(6) AND HAS_NET_TIMER_EXPIRED(dialogueTimer, 10000))
				CASE 8
					REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
						IF IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_I_HAVE_BEEN_COLLECTED_FOR_FIRST_TIME)
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntities.netId[iMissionEntity])
								IF IS_ENTITY_IN_RANGE_COORDS(NET_TO_ENT(serverBD.sMissionEntities.netId[iMissionEntity]), vMissionFacilityCoords, 500.0)
									RETURN TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
				BREAK
			ENDSWITCH
		BREAK
		CASE GOV_BURGLARY_JOB
			SWITCH iDialogue
				CASE 0 			RETURN GET_MODE_STATE() > eMODESTATE_GO_TO_POINT
				CASE 1			RETURN IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_AM_INSIDE_MISSION_INTERIOR)
				CASE 2 			RETURN IS_SERVER_BIT_SET(eSERVERBITSET_MAKE_ALL_PEDS_REACT)
				CASE 3 			RETURN HAS_ANY_MISSION_ENTITY_BEEN_COLLECTED_FOR_THE_FIRST_TIME()
				CASE 4 			RETURN (IS_SERVER_BIT_SET(eSERVERBITSET_MAKE_ALL_PEDS_REACT) AND HAS_ANY_MISSION_ENTITY_BEEN_COLLECTED_FOR_THE_FIRST_TIME() AND ARE_ANY_MISSION_ENTITIES_NOT_IN_MISSION_INTERIOR())
				CASE 5			RETURN HAS_DIALOGUE_FINISHED_PLAYING(4) AND HAS_NET_TIMER_EXPIRED(dialogueTimer, 10000)
				CASE 6
					REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
						IF NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_I_AM_IN_A_MISSION_INTERIOR)
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntities.netId[iMissionEntity])
								IF IS_ENTITY_IN_RANGE_COORDS(NET_TO_ENT(serverBD.sMissionEntities.netId[iMissionEntity]), vMissionFacilityCoords, 500.0)
									RETURN TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
				BREAK
			ENDSWITCH
		BREAK
		CASE GOV_AQUALUNGS
			SWITCH iDialogue
				CASE 0			RETURN GET_MODE_STATE() > eMODESTATE_GO_TO_POINT
				CASE 1			RETURN HAS_ANY_MISSION_ENTITY_BEEN_DESTROYED() OR HAS_ANY_MISSION_ENTITY_BEEN_DELIVERED_BY_RIVALS()
				CASE 2			RETURN HAS_ANY_MISSION_ENTITY_BEEN_COLLECTED_FOR_THE_FIRST_TIME()
				CASE 3
					REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntities.netId[iMissionEntity])
							IF VDIST2(GET_GANGOPS_ENTITY_SPAWN_COORDS(GET_GANGOPS_VARIATION(), GET_GANGOPS_LOCATION(), iMissionEntity, FALSE), GET_ENTITY_COORDS(NET_TO_ENT(serverBD.sMissionEntities.netId[iMissionEntity]), FALSE)) > (500*500)
								RETURN TRUE
							ENDIF
						ENDIF
					ENDREPEAT
				BREAK
				CASE 4
					IF IS_BIT_SET(iLocalVariationDialogueBitset0, 3)
					AND HAS_DIALOGUE_FINISHED_PLAYING(3)
						RETURN HAS_NET_TIMER_EXPIRED(dialogueTimer, 10000)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK	
		CASE GOV_AMATEUR_PHOTOGRAPHY
			SWITCH iDialogue
				CASE 0			RETURN (IS_LOCAL_BIT_SET(eLOCALBITSET_DID_OPENING_COMMUNICATION) AND HAS_NET_TIMER_EXPIRED(dialogueTimer,15000))
				CASE 1			RETURN (GET_MODE_STATE() = eMODESTATE_TAKE_PHOTOGRAPHS)
				CASE 2			RETURN IS_SERVER_BIT_SET(eSERVERBITSET_ANY_PLAYER_IN_RESTRICTED_AREA)
			ENDSWITCH
		BREAK
		
		CASE GOV_PARAMEDIC
			SWITCH iDialogue
				CASE 0			
					IF (IS_SERVER_BIT_SET(eSERVERBITSET_AMBULANCE_WARPED_AFTER_REQUEST)
					OR IS_SERVER_BIT_SET(eSERVERBITSET_AMBULANCE_WARP_TO_BOSS))
					AND GET_MODE_STATE() = eMODESTATE_COLLECT_MISSION_ENTITY
						RETURN TRUE
					ENDIF
				BREAK
				CASE 1		
					RETURN FALSE//GET_MODE_STATE() = eMODESTATE_GO_TO_POINT
				BREAK
				CASE 2		
					IF GET_MODE_STATE() = eMODESTATE_DELIVER_MISSION_ENTITY
					AND !HAS_DIALOGUE_FINISHED_PLAYING(0)
						RETURN TRUE
					ENDIF
				BREAK
				CASE 3
					IF GET_MODE_STATE() = eMODESTATE_DELIVER_MISSION_ENTITY
						REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntities.netId[iMissionEntity])
								IF IS_ENTITY_IN_RANGE_COORDS(NET_TO_ENT(serverBD.sMissionEntities.netId[iMissionEntity]), vMissionFacilityCoords, 500.0)
									RETURN TRUE
								ENDIF
							ENDIF
						ENDREPEAT
					ENDIF
				BREAK
				
				CASE 4	
					IF GET_MODE_STATE() = eMODESTATE_GO_TO_POINT
					OR GET_MODE_STATE() = eMODESTATE_COLLECT_MISSION_ENTITY
						IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), GET_GANGOPS_VEHICLE_SPAWN_COORDS(GOV_PARAMEDIC, GET_GANGOPS_LOCATION(), 0), 200.0)
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK

		CASE GOV_UNDER_CONTROL
			SWITCH iDialogue
				CASE 0
					IF IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_AM_INSIDE_MISSION_INTERIOR) 
						IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_TRIGGERED_INTERIOR_WARP_ONCE)
							VECTOR vPlayerCoords
							vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
							IF vPlayerCoords.z >= 60.3654 AND vPlayerCoords.z < 64.3654
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE 1		RETURN GET_MODE_STATE() = eMODESTATE_REACH_SCORE AND serverBD.iTotalScore >= 5 AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
				CASE 2		RETURN GET_MODE_STATE() >= eMODESTATE_COLLECT_MISSION_ENTITY
				CASE 3		RETURN GET_MODE_STATE() = eMODESTATE_INTERACT AND NOT SHOULD_DO_SECONDARY_LOCAL_GOTO()
				CASE 4		RETURN GET_MODE_STATE() = eMODESTATE_REACH_SCORE AND serverBD.iTotalScore > 0 AND NOT IS_SERVER_BIT_SET(eSERVERBITSET_ANY_PLAYER_IN_MEET_UP_AREA)
				CASE 5		RETURN GET_MODE_STATE() > eMODESTATE_GO_TO_POINT
				CASE 6		RETURN GET_MODE_STATE() = eMODESTATE_DELIVER_MISSION_ENTITY
				CASE 7		RETURN GET_MODE_STATE() = eMODESTATE_DELIVER_MISSION_ENTITY
				CASE 8		RETURN IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_AM_INSIDE_MISSION_INTERIOR)
				CASE 9		RETURN IS_LOCAL_BIT_SET(eLOCALBITSET_DID_OPENING_COMMUNICATION) AND HAS_NET_TIMER_EXPIRED(dialogueTimer, 15000)
				CASE 10		RETURN IS_SERVER_BIT_SET(eSERVERBITSET_ENTITY_REACHED_DESTINATION) AND IS_SERVER_BIT_SET(eSERVERBITSET_MAKE_ALL_PEDS_REACT)
				CASE 11	
					REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
						IF IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_I_HAVE_BEEN_COLLECTED_FOR_FIRST_TIME)
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntities.netId[iMissionEntity])
								IF IS_ENTITY_IN_RANGE_COORDS(NET_TO_ENT(serverBD.sMissionEntities.netId[iMissionEntity]), vMissionFacilityCoords, 500.0)
									RETURN TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_CAR_COLLECTOR
			SWITCH iDialogue
				CASE 0			RETURN GET_MODE_STATE() > eMODESTATE_GO_TO_POINT
				CASE 1			RETURN IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_HAVE_COLLECTED_FIND_ITEM_PORTABLE_PICKUP)
				CASE 2			RETURN HAS_ANY_MISSION_ENTITY_BEEN_COLLECTED_FOR_THE_FIRST_TIME() AND ARE_ANY_MISSION_ENTITIES_NOT_IN_MISSION_INTERIOR()
				CASE 3			RETURN (GET_END_REASON() = eENDREASON_NO_REASON_YET AND GET_NUM_MISSION_ENTITIES_NOT_DELIVERED_OR_DESTROYED() >= 1 AND HAS_ANY_MISSION_ENTITY_BEEN_DESTROYED())
				CASE 4	
					IF NOT g_FreemodeDeliveryData.bDeliveryScriptTriggeredCutScene
						IF serverBD.iNumEntitiesRequired = (-1)
							RETURN serverBD.iTotalDeliveredCount >= 1
						ENDIF
					ENDIF
				BREAK
				CASE 5	
					IF NOT g_FreemodeDeliveryData.bDeliveryScriptTriggeredCutScene
						IF serverBD.iNumEntitiesRequired = (-1)
							RETURN serverBD.iTotalDeliveredCount >= 2
						ELIF serverBD.iTotalDeliveredCount >= 1
							RETURN (serverBD.iNumEntitiesRequired - serverBD.iTotalDeliveredCount) = 2
						ENDIF
					ENDIF
				BREAK
				CASE 6		
					IF NOT g_FreemodeDeliveryData.bDeliveryScriptTriggeredCutScene
						IF serverBD.iNumEntitiesRequired = (-1)
							RETURN serverBD.iTotalDeliveredCount >= 3
						ELIF serverBD.iTotalDeliveredCount >= 1
							RETURN (serverBD.iNumEntitiesRequired - serverBD.iTotalDeliveredCount) = 1
						ENDIF
					ENDIF
				BREAK
				CASE 7
					REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
						IF IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_VEHICLE_HAS_BEEN_MODDED)
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntities.netId[iMissionEntity])
								IF IS_ENTITY_IN_RANGE_COORDS(NET_TO_ENT(serverBD.sMissionEntities.netId[iMissionEntity]), vMissionFacilityCoords, 500.0)
									RETURN TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_FLIGHT_RECORDER
			SWITCH iDialogue
				CASE 0			RETURN ((fDistanceFromClosestVehicle <= 400.0) AND NOT IS_SERVER_BIT_SET(eSERVERBITSET_FLIGHT_RECORDER_VEHICLE_ON_ROUTE_TO_CRASH_SITE))
				CASE 1			RETURN IS_SERVER_BIT_SET(eSERVERBITSET_FLIGHT_RECORDER_VEHICLE_ON_ROUTE_TO_CRASH_SITE)
				CASE 2			RETURN (IS_SERVER_BIT_SET(eSERVERBITSET_FLIGHT_RECORDER_VEHICLE_HAS_ENTERED_WATER) AND NOT HAS_ANY_MISSION_ENTITY_BEEN_COLLECTED_FOR_THE_FIRST_TIME())
				CASE 3			RETURN HAS_ANY_MISSION_ENTITY_BEEN_COLLECTED_FOR_THE_FIRST_TIME()
				CASE 4			
					IF IS_LOCAL_BIT_SET(eLOCALBITSET_DID_OPENING_COMMUNICATION)
						RETURN HAS_NET_TIMER_EXPIRED(dialogueTimer, 20000)
					ENDIF
				BREAK
				CASE 5
					REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntities.netId[iMissionEntity])
							IF IS_ENTITY_IN_RANGE_COORDS(NET_TO_ENT(serverBD.sMissionEntities.netId[iMissionEntity]), vMissionFacilityCoords, 500.0)
								RETURN TRUE
							ENDIF
						ENDIF
					ENDREPEAT
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_GONE_BALLISTIC
			SWITCH iDialogue
				CASE 0			RETURN GET_MODE_STATE() < eMODESTATE_COLLECT_MISSION_ENTITY AND fMyDistanceFromGoToLocation < 500.0
				CASE 1			RETURN GET_MODE_STATE() = eMODESTATE_COLLECT_MISSION_ENTITY
				CASE 2			RETURN GET_MODE_STATE() = eMODESTATE_DELIVER_MISSION_ENTITY
				CASE 3			RETURN GET_MODE_STATE() = eMODESTATE_DELIVER_MISSION_ENTITY AND HAS_DIALOGUE_FINISHED_PLAYING(2)
				CASE 4		
					REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
						IF IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_I_HAVE_BEEN_COLLECTED_FOR_FIRST_TIME)
						AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntities.netId[iMissionEntity])
							IF IS_ENTITY_IN_RANGE_COORDS(NET_TO_ENT(serverBD.sMissionEntities.netId[iMissionEntity]), vMissionFacilityCoords, 500.0)
								RETURN TRUE
							ENDIF
						ENDIF
					ENDREPEAT
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GOV_AUTO_SALVAGE
			SWITCH iDialogue
				CASE 0			RETURN IS_BIT_SET(iLocalTextMessageBitSet, 0)
				CASE 1			RETURN IS_BIT_SET(iLocalTextMessageBitSet, 1)
				CASE 2			RETURN IS_BIT_SET(iLocalTextMessageBitSet, 2)
				CASE 3			RETURN IS_BIT_SET(iLocalTextMessageBitSet, 3)
				CASE 4			RETURN IS_BIT_SET(iLocalTextMessageBitSet, 4)
				CASE 5			RETURN IS_BIT_SET(iLocalTextMessageBitSet, 5)
				CASE 6			RETURN HAS_ANY_MISSION_ENTITY_BEEN_DESTROYED() OR HAS_ANY_MISSION_ENTITY_BEEN_DELIVERED_BY_RIVALS()
				CASE 7			RETURN (serverBD.iTotalDeliveredCount = 1)
				CASE 8			RETURN (serverBD.iTotalDeliveredCount = 2)
				CASE 9			RETURN (serverBD.iTotalDeliveredCount = 3 AND NOT HAVE_ALL_MISSION_ENTITIES_BEEN_DELIVERED_OR_DESTROYED())
			ENDSWITCH
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_NUM_DIALOGUE_VARIATIONS(INT iDialogue)

	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_DEAD_DROP
			SWITCH iDialogue
				CASE 0		RETURN 1
			ENDSWITCH
		BREAK
	ENDSWITCH

	RETURN 1
ENDFUNC

FUNC TEXT_LABEL_23 GET_DIALOGUE_ROOT(INT iDialogue)
	
	TEXT_LABEL_23 txtLblCall

	txtLblCall = GET_VARIATION_DIALOGUE_ROOT(iDialogue)
	//PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [GO_DIALOGUE] - GET_DIALOGUE_ROOT - Main label: ", txtLblCall)
	
	// If dialogue has random variations, add number to the end
	INT iNumVariations = GET_NUM_DIALOGUE_VARIATIONS(iDialogue)
	IF iNumVariations > 1
		INT iRandomCall = GET_RANDOM_INT_IN_RANGE(0, iNumVariations)
		txtLblCall += iRandomCall
		//PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [GO_DIALOGUE] - GET_DIALOGUE_ROOT - Main label with random variation: ", txtLblCall)
	ENDIF
		
	RETURN txtLblCall
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_INTERRUPT_IMMEDIATELY(INT iDialogue)

	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_CAR_COLLECTOR
			SWITCH iDialogue
				CASE 2		RETURN TRUE
			ENDSWITCH
		BREAK
		CASE GOV_UNDER_CONTROL
			SWITCH iDialogue
				CASE 4		RETURN TRUE
			ENDSWITCH
		BREAK
		CASE GOV_AQUALUNGS
			SWITCH iDialogue
				CASE 0		RETURN TRUE
			ENDSWITCH
		BREAK
		CASE GOV_DAYLIGHT_ROBBERY
			SWITCH iDialogue
				CASE 4		RETURN TRUE
			ENDSWITCH
		BREAK
		CASE GOV_BURGLARY_JOB
			SWITCH iDialogue
				CASE 2		RETURN TRUE
			ENDSWITCH
		BREAK
		CASE GOV_AUTO_SALVAGE
			SWITCH iDialogue
				CASE 6		RETURN TRUE
			ENDSWITCH
		BREAK
		CASE GOV_MOB_MENTALITY
			SWITCH iDialogue
				CASE 0		RETURN TRUE
			ENDSWITCH
		BREAK
		CASE GOV_AMATEUR_PHOTOGRAPHY
			SWITCH iDialogue
				CASE 2		RETURN TRUE
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

FUNC INT GET_DIALOGUE_PRIORITY(INT iDialogue)

	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_UNDER_CONTROL
			SWITCH iDialogue
				CASE 0		RETURN DIALOGUE_PRIORITY_VERY_LOW
				CASE 7		RETURN DIALOGUE_PRIORITY_LOW
			ENDSWITCH
		BREAK
		CASE GOV_MOB_MENTALITY
			SWITCH iDialogue
				CASE 0		RETURN DIALOGUE_PRIORITY_HIGH
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN DIALOGUE_PRIORITY_MEDIUM

ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_USE_ADDITIONAL_SPEAKER(INT iDialogue)

	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_CAR_COLLECTOR
			SWITCH iDialogue
				CASE 0		
				CASE 2		
					RETURN TRUE
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

FUNC BOOL PLAY_DIALOGUE(INT iDialogue)

	IF IS_BIT_SET(iLocalVariationDialogueBitset0,iDialogue)
		RETURN FALSE
	ENDIF
	
	IF iCurrentPlayingDialogue != -1
		IF GET_DIALOGUE_PRIORITY(iDialogue) >= GET_DIALOGUE_PRIORITY(iCurrentPlayingDialogue)
			PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [GO_DIALOGUE] - PLAY_DIALOGUE - Interrupting dialogue ", iCurrentPlayingDialogue, " to play dialogue ",iDialogue)
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF SHOULD_DIALOGUE_INTERRUPT_IMMEDIATELY(iDialogue)
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [GO_DIALOGUE] - PLAY_DIALOGUE - KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE")
				ELSE
					KILL_FACE_TO_FACE_CONVERSATION()
					PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [GO_DIALOGUE] - PLAY_DIALOGUE - KILL_FACE_TO_FACE_CONVERSATION")
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		TEXT_LABEL_15 txtLblCall = GET_DIALOGUE_ROOT(iDialogue)
		
		IF NOT IS_STRING_NULL_OR_EMPTY(txtLblCall)
		
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 2, NULL, GET_GANGOPS_VOICE_FOR_DIALOGUE())
			PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [GO_DIALOGUE] - PLAY_DIALOGUE - ADD_PED_FOR_DIALOGUE - Speaker: ",2," Voice: ",GET_GANGOPS_VOICE_FOR_DIALOGUE())
			
			IF SHOULD_DIALOGUE_USE_ADDITIONAL_SPEAKER(iDialogue)
				ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, GET_GANGOPS_ADDITIONAL_SPEAKER_FOR_DIALOGUE(), NULL, GET_GANGOPS_ADDITIONAL_VOICE_FOR_DIALOGUE())
				PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [GO_DIALOGUE] - PLAY_DIALOGUE - ADD_PED_FOR_DIALOGUE - (Additional) Speaker: ",GET_GANGOPS_ADDITIONAL_SPEAKER_FOR_DIALOGUE()," Voice: ",GET_GANGOPS_ADDITIONAL_VOICE_FOR_DIALOGUE())
			ENDIF
			
			IF CREATE_CONVERSATION(MyLocalPedStruct, "XMFMAUD", txtLblCall, CONV_PRIORITY_VERY_HIGH)
				SET_BIT(iLocalVariationDialogueBitset0,iDialogue)
				PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [GO_DIALOGUE] - PLAY_DIALOGUE - Playing dialogue ", iDialogue, " ",txtLblCall)
				RETURN TRUE
			ENDIF

		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_DIALOGUE()

	IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_OPENING_PHONECALL_DONE_OR_NEVER_GOING_TO_HAPPEN)
		EXIT
	ENDIF

	//Controls the majority of dialogue
	IF IS_SAFE_TO_PLAY_MISSION_DIALOGUE()
		INT iDialogue
		INT iCurrent = -1
		
		IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
			PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
			PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [GO_DIALOGUE] - MAINTAIN_DIALOGUE - Safe again, unpausing ", iCurrentPlayingDialogue)
		ENDIF
		
		// Variation texts
		REPEAT MAX_NUM_DIALOGUE iDialogue
			IF SHOULD_PLAY_VARIATION_DIALOGUE(iDialogue)
				IF PLAY_DIALOGUE(iDialogue)
					iCurrent = iDialogue
					BREAKLOOP	// Break as we can only play one dialogue at once
				ENDIF
			ELSE
				// Keep track of which dialogue is playing
				IF IS_BIT_SET(iLocalVariationDialogueBitset0,iDialogue)
					TEXT_LABEL_23 tlCurrent = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
					TEXT_LABEL_23 sRoot = GET_DIALOGUE_ROOT(iDialogue)
					IF ARE_STRINGS_EQUAL(tlCurrent, sRoot) 
						iCurrent = iDialogue
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF iCurrentPlayingDialogue != iCurrent
			iCurrentPlayingDialogue = iCurrent
			PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [GO_DIALOGUE] - MAINTAIN_DIALOGUE - Now playing dialogue ", iCurrentPlayingDialogue)
		ENDIF
	ELSE
		IF iCurrentPlayingDialogue != -1
			IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
				PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
				PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [GO_DIALOGUE] - MAINTAIN_DIALOGUE - Not safe, pausing current conversation ", iCurrentPlayingDialogue)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC


///////////////////////////////////
////       	RADAR ZOOM  	   ////
/////////////////////////////////// 
  
FUNC FLOAT GET_ZOOM_PERCENTAGE()

	SWITCH GET_GANGOPS_VARIATION()		
		CASE GOV_UNDER_CONTROL		RETURN 20.0
	ENDSWITCH

	RETURN 60.0
ENDFUNC

FUNC BOOL IS_PLAYER_IN_RANGE_OF_ZOOM_AREA()
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_CAR_COLLECTOR			RETURN IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), GET_GANGOPS_VEHICLE_SPAWN_COORDS(GOV_CAR_COLLECTOR, GET_GANGOPS_LOCATION(), 0), 100.0)
		CASE GOV_UNDER_CONTROL			RETURN IS_CLIENT_BIT_SET(PARTICIPANT_ID(),eCLIENTBITSET_I_AM_INSIDE_MISSION_INTERIOR)
		CASE GOV_BURGLARY_JOB			RETURN IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), GET_SECONDARY_LOCAL_GOTO_LOCATION(), 50.0)
		CASE GOV_DAYLIGHT_ROBBERY		RETURN IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vCHOSEN_PED_COORDS(), 50.0)
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_ZOOM_RADAR_FOR_VARIATION()
	SWITCH GET_GANGOPS_VARIATION()			
		CASE GOV_UNDER_CONTROL			RETURN TRUE
		CASE GOV_CAR_COLLECTOR			RETURN (GET_MODE_STATE() = eMODESTATE_FIND_ITEM)
		CASE GOV_BURGLARY_JOB			RETURN (GET_MODE_STATE() = eMODESTATE_TAKE_OUT_TARGETS)
		CASE GOV_DAYLIGHT_ROBBERY		RETURN HAS_NET_TIMER_STARTED(stNearPedTimeout)	
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DISABLE_BOSS_RADIUS_BLIP_WHEN_ZOOMED()
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_RADAR_ZOOM()
	IF SHOULD_ZOOM_RADAR_FOR_VARIATION()
	AND IS_NET_PLAYER_OK(PLAYER_ID())
	AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
	AND NOT IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
		IF IS_PLAYER_IN_RANGE_OF_ZOOM_AREA()
			IF GET_DPADDOWN_ACTIVATION_STATE() <> DPADDOWN_SECOND
			AND GET_PROFILE_SETTING(PROFILE_DIAPLAY_BIG_RADAR) != 1
				SET_RADAR_ZOOM_PRECISE(GET_ZOOM_PERCENTAGE())
				SET_LOCAL_PLAYER_AS_USING_MISSION_SPECIFIC_ZOOM(TRUE)
				IF SHOULD_DISABLE_BOSS_RADIUS_BLIP_WHEN_ZOOMED()
					SET_BOSS_PROXIMITY_BLIP_AS_DISABLED(TRUE)
				ENDIF
				SET_LOCAL_BIT(eLOCALBITSET_USING_MISSION_ZOOM)
			ENDIF
		ELSE
			IF IS_LOCAL_BIT_SET(eLOCALBITSET_USING_MISSION_ZOOM)
				SET_RADAR_ZOOM_PRECISE(0)
				SET_LOCAL_PLAYER_AS_USING_MISSION_SPECIFIC_ZOOM(FALSE)
				IF SHOULD_DISABLE_BOSS_RADIUS_BLIP_WHEN_ZOOMED()
					SET_BOSS_PROXIMITY_BLIP_AS_DISABLED(FALSE)
				ENDIF
				CLEAR_LOCAL_BIT(eLOCALBITSET_USING_MISSION_ZOOM)
			ENDIF
		ENDIF
	ELSE
		IF IS_LOCAL_BIT_SET(eLOCALBITSET_USING_MISSION_ZOOM)
			SET_RADAR_ZOOM_PRECISE(0)
			SET_LOCAL_PLAYER_AS_USING_MISSION_SPECIFIC_ZOOM(FALSE)
			IF SHOULD_DISABLE_BOSS_RADIUS_BLIP_WHEN_ZOOMED()
				SET_BOSS_PROXIMITY_BLIP_AS_DISABLED(FALSE)
			ENDIF
			CLEAR_LOCAL_BIT(eLOCALBITSET_USING_MISSION_ZOOM)
		ENDIF
	ENDIF
ENDPROC

///////////////////////////////////
////        QUICK GPS   	   ////
/////////////////////////////////// 

FUNC BOOL SHOULD_QUICK_GPS_BE_ALLOWED()
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_ALLOW_QUICK_GPS()
	IF NOT SHOULD_QUICK_GPS_BE_ALLOWED()
	OR NOT HAVE_ANY_MISSION_ENTITIES_NOT_BEEN_COLLECTED_FOR_FIRST_TIME()
		MPGlobalsAmbience.sMagnateGangBossData.bAllowExportMissionQuickGPS = FALSE
	ENDIF
ENDPROC

///////////////////////////////////
////       BIG MESSAGES		   ////
/////////////////////////////////// 

FUNC BOOL IS_SAFE_FOR_BIG_MESSAGE()	

	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND NOT IS_PAUSE_MENU_ACTIVE()
	AND NOT IS_CUSTOM_MENU_ON_SCREEN()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC STRING GET_OPENING_BIG_MESSAGE_TITLE()
	IF IS_THIS_VARIATION_A_SELL_MISSION()
		RETURN "SMBM_SESTART_T"
	ELIF IS_THIS_VARIATION_A_SETUP_MISSION()
		RETURN "SMBM_STSTART_1"
	ENDIF
	RETURN "FHBM_STSTART_T"
ENDFUNC

FUNC STRING GET_START_OF_JOB_BIG_MESSAGE_STRAPLINE()
	
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_PARAMEDIC					RETURN "FHBM_PM_START"
		CASE GOV_CAR_COLLECTOR				RETURN "FHBM_CC_START"
		CASE GOV_UNDER_CONTROL				RETURN "FHBM_UC_START"
		CASE GOV_FLARE_UP					RETURN "FHBM_FU_START"
		CASE GOV_FORCED_ENTRY				RETURN "FHBM_FE_START"
		CASE GOV_MOB_MENTALITY				RETURN "FHBM_MM_START"
		CASE GOV_AUTO_SALVAGE				RETURN "FHBM_AS_START"
		CASE GOV_AQUALUNGS					RETURN "FHBM_AL_START"
		CASE GOV_DEAD_DROP					RETURN "FHBM_DD_START"
		CASE GOV_GONE_BALLISTIC				RETURN "FHBM_GB_START"
		CASE GOV_FLIGHT_RECORDER			RETURN "FHBM_FR_START"
		CASE GOV_AMATEUR_PHOTOGRAPHY		RETURN "FHBM_AP_START"
		CASE GOV_DAYLIGHT_ROBBERY			RETURN "FHBM_DR_START"
		CASE GOV_BURGLARY_JOB				RETURN "FHBM_BJ_START"
	ENDSWITCH
	
	RETURN GET_ITEM_LABEL_FOR_MISSION(TRUE)
	
ENDFUNC

SCRIPT_TIMER openingShardDelayTimer
FUNC BOOL DO_OPENING_BIG_MESSAGE()
	IF HAS_NET_TIMER_EXPIRED(openingShardDelayTimer, 1350)
	AND NOT SHOULD_SHARD_BE_HELD_UP_FOR_OFFICE_SEAT_ANIM()
		SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_START_OF_JOB, GET_OPENING_BIG_MESSAGE_TITLE(), GET_START_OF_JOB_BIG_MESSAGE_STRAPLINE(),HUD_COLOUR_H)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC STRING GET_END_BIG_MESSAGE_TITLE_LABEL(BOOL bPartialDelivery, BOOL bPassedJob)
	IF IS_THIS_VARIATION_A_SELL_MISSION()
		IF NOT bPassedJob
			RETURN "SMBM_SEOVER_T"
		ENDIF
		RETURN "SMBM_SESOLDa_T"
	ELIF IS_THIS_VARIATION_A_SETUP_MISSION()
		IF NOT bPassedJob
			RETURN "BBS_OVER"
		ENDIF
		RETURN "BBS_WIN_TTL"
	ENDIF
	
	IF NOT bPassedJob
		RETURN "FHBM_STOVER_T"
	ENDIF
	IF bPartialDelivery
		RETURN "FHBM_STPART_T"
	ENDIF
	RETURN "FHBM_STPASS_T"
ENDFUNC

FUNC BOOL SHOULD_SAY_WERE_RATHER_THAN_WAS()
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_CAR_COLLECTOR	
		CASE GOV_AQUALUNGS
		CASE GOV_AUTO_SALVAGE
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC STRING GET_END_BIG_MESSAGE_STRAPLINE_LABEL(BOOL bPartialDelivery, BOOL bPassedJob)
	IF IS_THIS_VARIATION_A_SELL_MISSION()
		IF NOT bPassedJob
			RETURN "SMBM_FAIL_S"
		ELIF bPartialDelivery
			RETURN "SMBM_SESOLDb_S"
		ENDIF
		RETURN "SMBM_SESOLDa_S"
	ELIF IS_THIS_VARIATION_A_SETUP_MISSION()
		IF bPassedJob
			RETURN "SMBM_SET2"
		ELSE
			RETURN "SMBM_SET3"
		ENDIF
	ENDIF
	
	IF NOT bPassedJob
		IF SHOULD_SAY_WERE_RATHER_THAN_WAS()
			RETURN "FHBM_FAILb_S"
		ENDIF
		RETURN "FHBM_FAIL_S"
	ENDIF
	IF bPartialDelivery
		IF GET_GANGOPS_VARIATION() = GOV_CAR_COLLECTOR
		OR GET_GANGOPS_VARIATION() = GOV_AUTO_SALVAGE
			RETURN "FHBM_STPART_Sb"
		ENDIF
		RETURN "FHBM_STPART_S"
	ENDIF
	RETURN "FHBM_STPASS_S"
ENDFUNC

FUNC BOOL SHOULD_SHOW_CRATE_COUNT_IN_END_BIG_MESSAGE()

	IF IS_THIS_VARIATION_A_SELL_MISSION()
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC INT GET_END_BIG_MESSAGE_AMOUNT_DELIVERED()
	IF SHOULD_SHOW_CRATE_COUNT_IN_END_BIG_MESSAGE()
		RETURN serverBD.iTotalDeliveredCount
	ENDIF

	RETURN GET_NUMBER_OF_DELIVERED_MISSION_ENTITIES()
ENDFUNC

FUNC INT GET_END_BIG_MESSAGE_MAX_DELIVERIES()

	IF SHOULD_SHOW_CRATE_COUNT_IN_END_BIG_MESSAGE()
		RETURN ( GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() * GET_DELIVERABLE_QUANTITY() )
	ENDIF

	RETURN GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION()
ENDFUNC

PROC MAINTAIN_OPENING_BIG_MESSAGE()
	
	IF GET_MODE_STATE() > eMODESTATE_INIT
		IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_SETUP_JOB_START_BIG_MESSAGE)
			IF IS_SAFE_FOR_BIG_MESSAGE()
				IF DO_OPENING_BIG_MESSAGE()
					SET_LOCAL_BIT(eLOCALBITSET_SETUP_JOB_START_BIG_MESSAGE)
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_SHOWN_JOB_START_BIG_MESSAGE)
				IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GB_START_OF_JOB)
					SET_LOCAL_BIT(eLOCALBITSET_SHOWN_JOB_START_BIG_MESSAGE)
				ENDIF
			ELSE
				IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_FINISHED_JOB_START_BIG_MESSAGE)
					IF NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GB_START_OF_JOB)
						SET_LOCAL_BIT(eLOCALBITSET_FINISHED_JOB_START_BIG_MESSAGE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_END_SUCCESS_BIG_MESSAGE()
	
	IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_DISPLAYED_END_OF_MODE_SUCCESS_BIG_MESSAGE)
		
		//IF IS_LOCAL_BIT_SET(eLOCALBITSET_DISPLAY_END_OF_MODE_SUCCESS_SHAR)D
		IF GET_END_REASON() = eENDREASON_MISSION_ENTITY_DELIVERED
//		OR HAVE_ALL_MISSION_ENTITIES_BEEN_DELIVERED_OR_DESTROYED()
			
			#IF IS_DEBUG_BUILD
			IF GET_END_REASON() = eENDREASON_MISSION_ENTITY_DELIVERED	
				PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_END_SUCCESS_SHARD - GET_END_REASON() = eENDREASON_MISSION_ENTITY_DELIVERED")
			ENDIF
			#ENDIF
			
			//g_bEndScreenSuppressFadeIn = TRUE
			
			IF SHOULD_SET_PASS_END_REASON()
				
				#IF IS_DEBUG_BUILD
				IF SHOULD_SET_PASS_END_REASON()
					PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_END_SUCCESS_SHARD - SHOULD_SET_PASS_END_REASON = TRUE")
				ENDIF
				PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_END_SUCCESS_SHARD - some MissionEntity has been delivered")
				#ENDIF
				
				IF NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
				AND NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE()
				AND NOT GB_SHOULD_HIDE_GANG_BOSS_EVENT(GET_THIS_MISSION_FMMC_TYPE())
					
					INT iNumDeliveredEntites
					
					
					iNumDeliveredEntites = GET_END_BIG_MESSAGE_AMOUNT_DELIVERED()
					PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_END_SUCCESS_SHARD - GET_END_SHARD_AMOUNT_DELIVERED()= ", iNumDeliveredEntites)
					
					IF iNumDeliveredEntites > GET_END_BIG_MESSAGE_MAX_DELIVERIES()
						#IF IS_DEBUG_BUILD
						SCRIPT_ASSERT("[FM_GANGOPS] - iNumDeliveredEntites > GET_END_SHARD_MAX_DELIVERIES()")
						#ENDIF
						iNumDeliveredEntites = GET_END_BIG_MESSAGE_MAX_DELIVERIES()
						PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_END_SUCCESS_SHARD - iNumDeliveredEntites > GET_END_SHARD_MAX_DELIVERIES(). Capping.")
					ENDIF		
					
					IF GET_GANGOPS_VARIATION() = GOV_AMATEUR_PHOTOGRAPHY
						PRINTLN("[3499026] amateur photography, photos taken = ", serverBD.iPhotosTaken)
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_BUY_END_OF_JOB_SUCCESS, GET_END_BIG_MESSAGE_TITLE_LABEL(FALSE, TRUE), "FHBM_AP_PASS",HUD_COLOUR_H)
					ELIF iNumDeliveredEntites = GET_END_BIG_MESSAGE_MAX_DELIVERIES()	// All entities delivered.
					OR IS_SERVER_BIT_SET(eSERVERBITSET_COMPLETED_A_PARTIAL_DELIVERY_MISSION)
						PRINTLN("[3499026] a iNumDeliveredEntites = ", iNumDeliveredEntites)
						SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_GB_BUY_END_OF_JOB_SUCCESS, GET_ITEM_LABEL_FOR_MISSION(FALSE, TRUE), GET_END_BIG_MESSAGE_STRAPLINE_LABEL(FALSE, TRUE), GET_END_BIG_MESSAGE_TITLE_LABEL(FALSE, TRUE), DEFAULT, DEFAULT, DEFAULT ,DEFAULT, HUD_COLOUR_H)
					ELIF iNumDeliveredEntites = 0	// No entities delivered.
						PRINTLN("[3499026] b iNumDeliveredEntites = ", iNumDeliveredEntites)
						SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_GB_BUY_END_OF_JOB_SUCCESS, GET_ITEM_LABEL_FOR_MISSION(FALSE, TRUE), GET_END_BIG_MESSAGE_STRAPLINE_LABEL(FALSE, TRUE), GET_END_BIG_MESSAGE_TITLE_LABEL(FALSE, FALSE), DEFAULT, DEFAULT, DEFAULT ,DEFAULT, HUD_COLOUR_H)
					ELSE	// Some entities delivered.
						PRINTLN("[3499026] c iNumDeliveredEntites = ", iNumDeliveredEntites)
						IF GET_GANGOPS_VARIATION() = GOV_CAR_COLLECTOR
						OR GET_GANGOPS_VARIATION() = GOV_AUTO_SALVAGE
							SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_GB_BUY_END_OF_JOB_SUCCESS, GET_ITEM_LABEL_FOR_MISSION(FALSE, TRUE), GET_END_BIG_MESSAGE_STRAPLINE_LABEL(TRUE, TRUE), GET_END_BIG_MESSAGE_TITLE_LABEL(TRUE, TRUE), DEFAULT, iNumDeliveredEntites, DEFAULT, DEFAULT, HUD_COLOUR_H)
						ELSE
							SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_GB_BUY_END_OF_JOB_SUCCESS, GET_ITEM_LABEL_FOR_MISSION(FALSE, TRUE), GET_END_BIG_MESSAGE_STRAPLINE_LABEL(TRUE, TRUE), GET_END_BIG_MESSAGE_TITLE_LABEL(TRUE, TRUE), DEFAULT, DEFAULT, DEFAULT ,DEFAULT, HUD_COLOUR_H)
						ENDIF
						GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_FM_GANGOPS_PARTIAL_DELIVERY)
					ENDIF
						
					SET_LOCAL_BIT(eLOCALBITSET_DISPLAYED_END_OF_MODE_SUCCESS_BIG_MESSAGE)
				
				ELSE
				
					#IF IS_DEBUG_BUILD
					IF GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
						PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_END_SUCCESS_SHARD - GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE = TRUE")
					ENDIF
					IF GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE()
						PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_END_SUCCESS_SHARD - GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE = TRUE")
					ENDIF
					IF GB_SHOULD_HIDE_GANG_BOSS_EVENT(GET_THIS_MISSION_FMMC_TYPE())
						PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_END_SUCCESS_SHARD - GB_SHOULD_HIDE_GANG_BOSS_EVENT(GET_THIS_MISSION_FMMC_TYPE()) = TRUE")
					ENDIF
					#ENDIF
				
				ENDIF
				
			#IF IS_DEBUG_BUILD	
			ELSE
				
				PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_END_SUCCESS_SHARD - SHOULD_SET_PASS_END_REASON = FALSE")
				PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_END_SUCCESS_SHARD - no MissionEntity has been or is being delivered")
			#ENDIF
			
			ENDIF
			
		ENDIF
		
	ENDIF
	
ENDPROC

PROC MAINTAIN_BIG_MESSAGES()
	MAINTAIN_OPENING_BIG_MESSAGE()
	MAINTAIN_END_SUCCESS_BIG_MESSAGE()
ENDPROC

///////////////////////////////////
////        HELP TEXT   	   ////
///////////////////////////////////     
FUNC BOOL IS_SAFE_FOR_HELP_TEXT(BOOL bShowDuringBigMessage = FALSE)
	IF NOT IS_CUSTOM_MENU_ON_SCREEN()
	AND NOT IS_PAUSE_MENU_ACTIVE()
	AND NOT IS_RADAR_HIDDEN()
	AND IS_NET_PLAYER_OK(PLAYER_ID())
	AND (NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED() OR bShowDuringBigMessage)
	AND IS_LOCAL_BIT_SET(eLOCALBITSET_SETUP_JOB_START_BIG_MESSAGE)
	AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
	AND NOT IS_SCREEN_FADED_OUT()
	AND NOT IS_SCREEN_FADING_OUT()
	AND NOT IS_SCREEN_FADING_IN()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Special cases for legacy help - Don't set up new maintains like this
PROC MAINTAIN_WANTED_HELP()
	IF NOT HAS_HELP_BEEN_DONE(eHELPTEXT_EXPLAIN_LOSE_WANTED_ABILITY)
		IF GB_IS_PLAYER_MEMBER_OF_GANG_TYPE(PLAYER_ID(),TRUE,GT_VIP)
		AND NOT IS_BIT_SET(MPGlobalsAmbience.iFmGbHelpBitSet, BI_FM_GANG_BOSS_HELP_BUY_MISSION_WANTED_DONE)
		AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		AND HAS_NET_TIMER_EXPIRED(stWantedLevelHelp, 3000)
			GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_BUY_MISSION_WANTED_HELP)
			SET_HELP_AS_DONE(eHELPTEXT_EXPLAIN_LOSE_WANTED_ABILITY)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Special cases for legacy help - Don't set up new maintains like this
PROC MAINTAIN_GHOST_ORG_HELP()
	IF NOT HAS_HELP_BEEN_DONE(eHELPTEXT_EXPLAIN_GHOST_ORG)
		IF IS_ANY_MISSION_ENTITY_GLOBALLY_BLIPPED()
			GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_BLIP_WARN)
			IF GB_TRIGGER_GHOST_ORG_HELP_TEXT_FOR_CONTRABAND_MISSIONS()
				SET_HELP_AS_DONE(eHELPTEXT_EXPLAIN_GHOST_ORG)
			ENDIF	
		ENDIF	
	ENDIF
ENDPROC

FUNC BOOL SHOULD_DO_AIRCRAFT_STASHED_TEXT()
	RETURN FALSE
ENDFUNC

FUNC STRING GET_STASHED_HELP_TEXT_LABEL()
	INT iNumVehicles = GET_GANGOPS_NUM_CARRIER_VEHICLES_FOR_ORG_SIZE(serverBD.iOrganisationSizeOnLaunch, GET_GANGOPS_VARIATION())
	
	IF SHOULD_DO_AIRCRAFT_STASHED_TEXT()
		IF iNumVehicles <= 1
			RETURN "SMH_STASHED"
		ENDIF
		RETURN "SMH_STASHED2"
	ENDIF
	
	IF iNumVehicles <= 1
		RETURN "SMH_STASHEDBG1"
	ENDIF
	RETURN "SMH_STASHEDBG2"
ENDFUNC

FUNC STRING GET_BOMBING_HELP_TEXT_LABEL()
	
	IF NOT GET_ARE_BOMB_BAY_DOORS_OPEN(currentVehicle)
		RETURN "SMH_BOMB2"
	ENDIF
	
	RETURN "SMH_BOMB"
	
ENDFUNC

/// PURPOSE: Returns the main text label for the help text enum. Substrings are entered directly in PRINT_HELP_FOR_ENUM.
FUNC STRING GET_MAIN_LABEL_FOR_HELP(HELP_TEXT_ENUM eHelp)
	SWITCH eHelp
		CASE eHELPTEXT_EXPLAIN_SUPPORT_VEHICLES		
			IF DOES_GANGOPS_VARIATION_USE_VEHICLES_AS_MISSION_ENTITIES(GET_GANGOPS_VARIATION())
				RETURN "FHH_SUPVEH"
			ELSE
				RETURN "FHH_SUPVEH1"
			ENDIF
		BREAK
		CASE eHELPTEXT_RIVAL_EXPLAIN								RETURN "FHH_RIVRECVR"
		CASE eHELPTEXT_STASHED										RETURN GET_STASHED_HELP_TEXT_LABEL()
		CASE eHELPTEXT_STEAL_GENERIC_EXPLAIN						RETURN "FHH_STGENHELP"
		CASE eHELPTEXT_CRATE_ALREADY_HELD							RETURN "FHH_HELD"
		CASE eHELPTEXT_CARGO_VISIBLE_WARNING						RETURN "FHH_VISWARN"
		CASE eHELPTEXT_NO_KILL										RETURN "FHH_NOKILL"
		CASE eHELPTEXT_AP_TAKE_PHOTOS								RETURN "FHH_APPHONE"
		CASE eHELPTEXT_AP_WRONG_CONTACT								RETURN "FHH_APWC"
		CASE eHELPTEXT_AP_SEND_HELP									RETURN "FHH_APSEND"
		CASE eHELPTEXT_DR_WARN										RETURN "DR_HLP_WRN"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC BOOL DOES_SUPPORT_HELP_NEED_THE()
	
	RETURN TRUE

ENDFUNC

/// PURPOSE: Calls the appropriate print function for each help, allowing for substrings, ints, etc.
PROC PRINT_HELP_FOR_ENUM(HELP_TEXT_ENUM eHelp)
	TEXT_LABEL_15 txtMainLabel = GET_MAIN_LABEL_FOR_HELP(eHelp)
	
	SWITCH eHelp
		CASE eHELPTEXT_EXPLAIN_SUPPORT_VEHICLES			PRINT_HELP_WITH_2_STRINGS(txtMainLabel, GET_SUPPORT_VEHICLE_BLIP_NAME(DOES_SUPPORT_HELP_NEED_THE()), GET_SUPPORT_BLIP_STRING()) BREAK
		DEFAULT											PRINT_HELP(txtMainLabel)																							BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE: Checks the help text is being displayed then clears it
PROC CLEAR_THIS_HELP_TEXT(HELP_TEXT_ENUM eHelp)
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_MAIN_LABEL_FOR_HELP(eHelp))
		CLEAR_HELP()
	ENDIF
ENDPROC

/// PURPOSE: Returns whether the help done bit should be set after displaying help. Should return false for help that is required all the time in certain conditions.
FUNC BOOL SHOULD_SET_HELP_AS_DONE_ON_DISPLAY(HELP_TEXT_ENUM eHelp)
	SWITCH eHelp
		CASe eHELPTEXT_CRATE_ALREADY_HELD				RETURN FALSE
	ENDSWITCH
	RETURN TRUE
ENDFUNC

/// PURPOSE: Returns whether the help trigger should be cleared on display. 
FUNC BOOL SHOULD_CLEAR_HELP_TRIGGER_ON_DISPLAY(HELP_TEXT_ENUM eHelp)
	UNUSED_PARAMETER(eHELP)
	RETURN FALSE
ENDFUNC

/// PURPOSE: Processes all help triggers that have been set throughout the script. Priority of help printed is in the order of their place in the enum.
PROC MAINTAIN_HELP()
	IF NOT IS_SAFE_FOR_HELP_TEXT()
		EXIT
	ENDIF
	
	INT iHelp
	REPEAT ENUM_TO_INT(eHELPTEXT_END) iHelp
		HELP_TEXT_ENUM eHelp = INT_TO_ENUM(HELP_TEXT_ENUM, iHelp)
		IF SHOULD_SHOW_HELP(eHelp)
			PRINT_HELP_FOR_ENUM(eHelp)
			
			IF SHOULD_CLEAR_HELP_TRIGGER_ON_DISPLAY(eHelp)
				CLEAR_HELP_TRIGGER(eHelp)
			ENDIF
			IF SHOULD_SET_HELP_AS_DONE_ON_DISPLAY(eHelp)
				SET_HELP_AS_DONE(eHelp)
			ENDIF
			
			EXIT
		ENDIF	
	ENDREPEAT
	
	// Special cases for legacy help - Don't set up new maintains like this, add your help to the appropriate point above
	//MAINTAIN_WANTED_HELP()
	//MAINTAIN_GHOST_ORG_HELP()
ENDPROC

PROC DRAW_AIR_RESPAWNS_HUD()
	IF SHOULD_RESPAWN_IN_AIR()
		HUD_COLOURS eHudColour = HUD_COLOUR_WHITE
		IF iNumAirRespawnsRemaining = 0
			eHudColour = HUD_COLOUR_RED
		ENDIF
		DRAW_GENERIC_BIG_NUMBER(iNumAirRespawnsRemaining, "SMHUD_AIRSPWN", DEFAULT, eHudColour, HUDORDER_TOP)
	ENDIF
ENDPROC

///////////////////////////////////
////      ELIMINATE HUD   	   ////
/////////////////////////////////// 

FUNC STRING GET_TARGET_NAME_FOR_HUD()
	RETURN "SMHUD_ENEMIES"
ENDFUNC

FUNC BOOL SHOULD_DRAW_ELIMINATE_HUD()
	RETURN FALSE
ENDFUNC

FUNC INT GET_ELIMINATE_HUD_NUM_PEDS()
	RETURN 0
ENDFUNC

FUNC INT GET_ELIMINATE_HUD_NUM_VEHICLES()
	RETURN 0
ENDFUNC

FUNC INT GET_ELIMINATE_HUD_NUM_PROPS()
	RETURN 0
ENDFUNC

FUNC BOOL SHOULD_PLAY_TARGET_TICK_SOUND()
	RETURN FALSE
ENDFUNC

PROC DRAW_ELIMINATE_HUD()
	IF SHOULD_DRAW_ELIMINATE_HUD()
		INT iTotalTargets, iTotalPeds, iTotalVehs, iTotalProps, iRemaining, iLoop
		
		iTotalPeds = GET_ELIMINATE_HUD_NUM_PEDS()
		iTotalVehs = GET_ELIMINATE_HUD_NUM_VEHICLES()
		iTotalProps = GET_ELIMINATE_HUD_NUM_PROPS()
		
		iTotalTargets = iTotalPeds + iTotalVehs + iTotalProps
		
		IF IS_SERVER_BIT_SET(eSERVERBITSET_ALL_TARGET_VEHICLES_CREATED)
//			iTotalEliminationTargets = iTotalTargets
//			PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - iTotalEliminationTargets = ",iTotalEliminationTargets)
		ENDIF

		IF iTotalPeds > 0
			REPEAT iTotalPeds iLoop
				IF GET_PED_STATE(iLoop) > ePEDSTATE_CREATE
					IF GET_PED_STATE(iLoop) != ePEDSTATE_DEAD
						iRemaining++
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
		IF iTotalVehs > 0
			REPEAT iTotalVehs iLoop
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iLoop].netId)
					IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.sVehicle[iLoop].netId))
						iRemaining++
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
		IF iTotalProps > 0
			REPEAT iTotalProps iLoop
				IF NOT IS_BIT_SET(serverBD.iServerPropDestroyedBitSet, iLoop)
					iRemaining++
				ENDIF
			ENDREPEAT
		ENDIF
		
		IF IS_SERVER_BIT_SET(eSERVERBITSET_ALL_TARGET_VEHICLES_CREATED)
			//iRemainingTargets = iRemaining
			//PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - iRemainingTargets = ",iRemainingTargets)
			
			//Tick sound
			IF iPreviousRemainingTargets != iRemaining
				IF iPreviousRemainingTargets != -1
					
				ENDIF
				iPreviousRemainingTargets = iRemaining
			ENDIF
		ENDIF
		
		DRAW_GENERIC_METER(iRemaining, iTotalTargets, GET_TARGET_NAME_FOR_HUD(), DEFAULT, DEFAULT, HUDORDER_THIRDBOTTOM)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_DRAW_HEALTH_HUD()
	RETURN FALSE
ENDFUNC

FUNC INT GET_HEALTH_ENTITY_ARRAY_INDEX(INT iEntity)

	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_GONE_BALLISTIC
			SWITCH iEntity
				CASE 0		RETURN 13
				CASE 1		RETURN 14
			ENDSWITCH
		BREAK
	ENDSWITCH

	RETURN -1

ENDFUNC

FUNC NETWORK_INDEX GET_HEALTH_ENTITY_NETWORK_INDEX(INT iEntity)
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_GONE_BALLISTIC			
			RETURN serverBD.sPed[GET_HEALTH_ENTITY_ARRAY_INDEX(iEntity)].netId
	ENDSWITCH
	
	RETURN INT_TO_NATIVE(NETWORK_INDEX, -1)
ENDFUNC

FUNC INT GET_HEALTH_OF_HEALTH_PED(INT iEntity)
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GET_HEALTH_ENTITY_NETWORK_INDEX(iEntity))
		PED_INDEX pedId = NET_TO_PED(GET_HEALTH_ENTITY_NETWORK_INDEX(iEntity))
		IF NOT IS_PED_INJURED(pedId)
			RETURN GET_ENTITY_HEALTH(pedId)
		ENDIF
	ENDIF
	
	RETURN 0
ENDFUNC

PROC DRAW_HEALTH_HUD()

	IF NOT SHOULD_DRAW_HEALTH_HUD()
		EXIT
	ENDIF
	
	INT iMaxHealth
	INT iCurrentHealth
	HUD_COLOURS eHudColour
	
	INT i
	REPEAT NUM_HEALTH_PEDS i
		iMaxHealth = BALLISTIC_ARMOR_PED_HEALTH
		iCurrentHealth = GET_HEALTH_OF_HEALTH_PED(i)
		eHudColour = HUD_COLOUR_WHITE
	//	IF iCurrentHealth <= 300
	//		eHudColour = HUD_COLOUR_RED
	//	ENDIF
		
		IF iCurrentHealth > 0
			DRAW_GENERIC_METER(iCurrentHealth, iMaxHealth, "GOHUD_NM_HEALTH",eHudColour,DEFAULT,HUDORDER_FIFTHBOTTOM,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT, eHudColour)
		ENDIF
	ENDREPEAt
		
ENDPROC

FUNC BOOL SHOULD_DRAW_SCORE_HUD()

	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_UNDER_CONTROL		RETURN GET_MODE_STATE() = eMODESTATE_REACH_SCORE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

PROC DRAW_SCORE_HUD()

	IF NOT SHOULD_DRAW_SCORE_HUD()
		EXIT
	ENDIF
	
	HUD_COLOURS colour = HUD_COLOUR_WHITE
	IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_ANY_PLAYER_IN_MEET_UP_AREA)
		colour = HUD_COLOUR_RED
	ENDIF

	DRAW_GENERIC_METER(serverBD.iTotalScore, GET_TOTAL_SCORE_TO_REACH(), "GOHUD_DLOAD", colour, DEFAULT, HUDORDER_THIRDBOTTOM, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, colour)

ENDPROC

///////////////////////////////////
////      DELIVERY HUD   	   ////
/////////////////////////////////// 

FUNC BOOL SHOULD_DRAW_CARRYING_HUD()
	IF IS_PLAYER_IN_POSSESSION_OF_ANY_MISSION_ENTITY(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	IF GET_MISSION_VEHICLE_I_AM_IN_WITH_MISSION_ENTITY_HOLDER() <> -1
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DRAW_DELIVERIES_MADE_HUD()
	
//	SWITCH GET_GANGOPS_VARIATION()
//		CASE 
//			IF GET_MODE_STATE() <  eMODESTATE_REWARDS
//				RETURN TRUE
//			ENDIF
//		BREAK
//	ENDSWITCH

	IF IS_GANGOPS_VARIATION_A_SELL_MISSION(GET_GANGOPS_VARIATION())
		IF GET_MODE_STATE() <  eMODESTATE_REWARDS
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PACKAGE_DISPLAY_SECTION_BE_ACTIVE(INT iMissionEntity)
	IF HAS_MISSION_ENTITY_BEEN_DELIVERED_OR_DESTROYED(iMissionEntity)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC STRING GET_BOTTOM_RIGHT_PACKAGE_DISPLAY_LABEL()
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_CAR_COLLECTOR			RETURN "FHHUD_DELUX"
		CASE GOV_AQUALUNGS				RETURN "FHHUD_TORP"
		CASE GOV_AUTO_SALVAGE			RETURN "FHHUD_STROM"
	ENDSWITCH
	RETURN "SMHUD_CARGO"
ENDFUNC

FUNC BOOL SHOULD_USE_CIRCLE_DISPLAY()
	IF GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() > 1
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC MAINTAIN_BOTTOM_RIGHT_DELIVERED_HUD_SERVER_DATA()
	IF SHOULD_USE_CIRCLE_DISPLAY()
		INT iMissionEntity
		serverbd.iBottomRightResolvedMissionEntities = 0 //(GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() - 1)

		REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
			serverbd.eBottomRightBlipState[iMissionEntity] = eBRBS_UNHELD
		ENDREPEAT
		
		INT iNumEntites = GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION()
		
		REPEAT iNumEntites iMissionEntity
			IF IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_DELIVERED)
				serverbd.eBottomRightBlipState[serverbd.iBottomRightResolvedMissionEntities] = eBRBS_DELIVERED
				serverbd.iBottomRightResolvedMissionEntities++
			ELIF IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_DESTROYED)
				serverbd.eBottomRightBlipState[serverbd.iBottomRightResolvedMissionEntities] = eBRBS_DESTROYED
				serverbd.iBottomRightResolvedMissionEntities++
			ELIF MissionEntityHolderPartID[iMissionEntity] != INVALID_PARTICIPANT_INDEX()
			OR IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_RIVAL_PLAYER_HOLDING_ENTITY)
				serverbd.eBottomRightBlipState[serverbd.iBottomRightResolvedMissionEntities] = eBRBS_BEING_HELD
				serverbd.iBottomRightResolvedMissionEntities++
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

PROC DRAW_BOTTOM_RIGHT_PACKAGE_DISPLAY()
	
	IF SHOULD_USE_CIRCLE_DISPLAY()
	
		IF NOT HAS_ANY_MISSION_ENTITY_BEEN_CREATED()
		OR NOT IS_LOCAL_BIT_SET(eLOCALBITSET_OPENING_PHONECALL_DONE_OR_NEVER_GOING_TO_HAPPEN)
		AND IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_HAVE_TRIGGERED_WARP_INTO_WAREHOUSE)
			EXIT
		ENDIF

		INT iMissionEntity
		HUD_COLOURS eBoxColour[MAX_NUM_MISSION_ENTITIES]
		HUD_COLOURS eCrossColour[MAX_NUM_MISSION_ENTITIES]
		
		REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
			eBoxColour[iMissionEntity] = HUD_COLOUR_GREYLIGHT
			eCrossColour[iMissionEntity] = HUD_COLOUR_GREYLIGHT
		ENDREPEAT
		
		REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
			IF serverbd.eBottomRightBlipState[iMissionEntity] = eBRBS_DELIVERED
				eBoxColour[iMissionEntity] = GET_PLAYER_HUD_COLOUR(PLAYER_ID())
				eCrossColour[iMissionEntity] = HUD_COLOUR_BLACK
			ELIF serverbd.eBottomRightBlipState[iMissionEntity] = eBRBS_BEING_HELD
				eBoxColour[iMissionEntity] = GET_PLAYER_HUD_COLOUR(PLAYER_ID())
			ELIF serverbd.eBottomRightBlipState[iMissionEntity] = eBRBS_DESTROYED
				eBoxColour[iMissionEntity] = HUD_COLOUR_BLACK
				eCrossColour[iMissionEntity] = HUD_COLOUR_GREYLIGHT
			ENDIF
			
			IF serverbd.eBottomRightBlipState[iMissionEntity] != eBRBS_UNHELD
				IF IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_RIVAL_PLAYER_HOLDING_ENTITY)
					eBoxColour[iMissionEntity] = HUD_COLOUR_RED
				ENDIF
			ENDIF
		ENDREPEAT
		
		DRAW_GENERIC_ELIMINATION(	GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION(), GET_BOTTOM_RIGHT_PACKAGE_DISPLAY_LABEL(), DEFAULT, 
									TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, DEFAULT, DEFAULT, DEFAULT, HUDORDER_SECONDBOTTOM , DEFAULT, DEFAULT, DEFAULT, eBoxColour[0], eBoxColour[1], eBoxColour[2], eBoxColour[3], eBoxColour[4], eBoxColour[5], eBoxColour[6], eBoxColour[7],
									DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 
									//serverbd.eBottomRightBlipState[0] = eBRBS_DESTROYED, serverbd.eBottomRightBlipState[1] = eBRBS_DESTROYED, serverbd.eBottomRightBlipState[2] = eBRBS_DESTROYED, serverbd.eBottomRightBlipState[3] = eBRBS_DESTROYED,
									//serverbd.eBottomRightBlipState[4] = eBRBS_DESTROYED, serverbd.eBottomRightBlipState[5] = eBRBS_DESTROYED, serverbd.eBottomRightBlipState[6] = eBRBS_DESTROYED, serverbd.eBottomRightBlipState[7] = eBRBS_DESTROYED,
									//HAS_MISSION_ENTITY_BEEN_DELIVERED_OR_DESTROYED(0), HAS_MISSION_ENTITY_BEEN_DELIVERED_OR_DESTROYED(1), HAS_MISSION_ENTITY_BEEN_DELIVERED_OR_DESTROYED(2), HAS_MISSION_ENTITY_BEEN_DELIVERED_OR_DESTROYED(3), 
									//HAS_MISSION_ENTITY_BEEN_DELIVERED_OR_DESTROYED(4), HAS_MISSION_ENTITY_BEEN_DELIVERED_OR_DESTROYED(5), HAS_MISSION_ENTITY_BEEN_DELIVERED_OR_DESTROYED(6), HAS_MISSION_ENTITY_BEEN_DELIVERED_OR_DESTROYED(7),
									((serverbd.eBottomRightBlipState[0] = eBRBS_DESTROYED) OR (serverbd.eBottomRightBlipState[0] = eBRBS_DELIVERED)),
									((serverbd.eBottomRightBlipState[1] = eBRBS_DESTROYED) OR (serverbd.eBottomRightBlipState[1] = eBRBS_DELIVERED)),
									((serverbd.eBottomRightBlipState[2] = eBRBS_DESTROYED) OR (serverbd.eBottomRightBlipState[2] = eBRBS_DELIVERED)),
									((serverbd.eBottomRightBlipState[3] = eBRBS_DESTROYED) OR (serverbd.eBottomRightBlipState[3] = eBRBS_DELIVERED)),
									((serverbd.eBottomRightBlipState[4] = eBRBS_DESTROYED) OR (serverbd.eBottomRightBlipState[4] = eBRBS_DELIVERED)),
									((serverbd.eBottomRightBlipState[5] = eBRBS_DESTROYED) OR (serverbd.eBottomRightBlipState[5] = eBRBS_DELIVERED)),
									((serverbd.eBottomRightBlipState[6] = eBRBS_DESTROYED) OR (serverbd.eBottomRightBlipState[6] = eBRBS_DELIVERED)),
									((serverbd.eBottomRightBlipState[7] = eBRBS_DESTROYED) OR (serverbd.eBottomRightBlipState[7] = eBRBS_DELIVERED)),
									DEFAULT, DEFAULT, eCrossColour[0], eCrossColour[1], eCrossColour[2], eCrossColour[3], eCrossColour[4], eCrossColour[5], eCrossColour[6], eCrossColour[7])
		
	ENDIF
ENDPROC

FUNC STRING GET_NUM_REQUIRED_HUD_STRING()
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_AQUALUNGS			RETURN "FHHUD_ECUREQ"
		CASE GOV_CAR_COLLECTOR		RETURN "FHHUD_DELREQ"
		CASE GOV_AUTO_SALVAGE		RETURN "FHHUD_STRMREQ"
	ENDSWITCH
	RETURN ""
ENDFUNC

PROC DRAW_NUM_REQUIRED_HUD()
	IF serverBD.iNumEntitiesRequired = (-1)
		EXIT
	ENDIF
	
	INT iCurrentNumEntitiesRequired = (serverBD.iNumEntitiesRequired - serverBD.iTotalDeliveredCount)
	IF iCurrentNumEntitiesRequired > 0
		DRAW_GENERIC_SCORE(iCurrentNumEntitiesRequired, GET_NUM_REQUIRED_HUD_STRING(), DEFAULT, DEFAULT, HUDORDER_THIRDBOTTOM)
	ENDIF
ENDPROC

INT iStoredCount
SCRIPT_TIMER stFlash

PROC DRAW_DELIVERIES_MADE_HUD()
	IF SHOULD_DRAW_DELIVERIES_MADE_HUD()
		INT iCarrying, iTotalDeliveries, iMyMissionEntity, iLoop
	
		IF SHOULD_DRAW_CARRYING_HUD()
		
			iMyMissionEntity = GET_MISSION_ENTITY_PLAYER_IS_HOLDING(PLAYER_ID())
			
			IF iMyMissionEntity = -1
			
				IF DOES_VARIATION_ALLOW_PASSENGER_DELIVERY()
					IF GET_MISSION_ENTITY_VEHICLE_I_AM_IN() != -1
						IF DOES_MISSION_ENTITY_HAVE_PASSENGER(GET_MISSION_ENTITY_VEHICLE_I_AM_IN())
						AND IS_PLAYER_IN_POSSESSION_OF_ANY_MISSION_ENTITY(GET_PASSENGER_IN_MISSION_ENTITY(GET_MISSION_ENTITY_VEHICLE_I_AM_IN()))
							iMyMissionEntity = GET_MISSION_ENTITY_PLAYER_IS_HOLDING(GET_PASSENGER_IN_MISSION_ENTITY(GET_MISSION_ENTITY_VEHICLE_I_AM_IN()))
						ENDIF
					ENDIF				
				ELSE
					iMyMissionEntity = GET_MISSION_VEHICLE_I_AM_IN_WITH_MISSION_ENTITY_HOLDER()
				ENDIF
				
//				PRINTLN("DRAW_DELIVERIES_MADE_HUD, iMyMissionEntity = ", iMyMissionEntity)
				
			ENDIF
			
			IF iMyMissionEntity <> -1
		
				iCarrying = GET_MISSION_ENTITY_QUANTITY_TO_DELIVER(iMyMissionEntity)
				
//				PRINTLN("DRAW_DELIVERIES_MADE_HUD, iMyMissionEntity = ", iMyMissionEntity, " iCarrying = ", iCarrying)
				
				IF iCarrying > 0
					// WEAPONS CARRIED
					DRAW_GENERIC_BIG_NUMBER(iCarrying, "SMHUD_CARGOHELD", DEFAULT, DEFAULT, HUDORDER_THIRDBOTTOM)
				ENDIF
			ENDIF
		ENDIF
	
		iTotalDeliveries = GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() * GET_DELIVERABLE_QUANTITY()
		
		// Take away from the total amount to deliver the number of drop-offs the vehicle had yet to go to
		REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iLoop
			IF IS_MISSION_ENTITY_BIT_SET(iLoop, eMISSIONENTITYBITSET_DESTROYED)
			AND NOT IS_MISSION_ENTITY_BIT_SET(iLoop, eMISSIONENTITYBITSET_DELIVERED)
				iTotalDeliveries -= GET_MISSION_ENTITY_QUANTITY_TO_DELIVER(iLoop)
			ENDIF
		ENDREPEAT
		
		INT iFlash
		IF iStoredCount <> serverBD.iTotalDeliveredCount
		AND serverBD.iTotalDeliveredCount > 0
			iStoredCount = serverBD.iTotalDeliveredCount
			
			RESET_NET_TIMER(stFlash)
			START_NET_TIMER(stFlash)
			
			PLAY_CRATE_DROPPED_AUDIO()
		ENDIF
		
		IF HAS_NET_TIMER_STARTED(stFlash)
			IF NOT HAS_NET_TIMER_EXPIRED(stFlash, 5000)
				iFlash = 5000
			ELSE
				RESET_NET_TIMER(stFlash)
			ENDIF
		ENDIF
		
		// TOTAL DELIVERED
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(serverBD.iTotalDeliveredCount, iTotalDeliveries, "SMHUD_TOTALDEL", iFlash, DEFAULT, HUDORDER_SECONDBOTTOM)
	ENDIF
	
	DRAW_NUM_REQUIRED_HUD()
	DRAW_BOTTOM_RIGHT_PACKAGE_DISPLAY()
ENDPROC

FUNC BOOL SHOULD_DRAW_PHOTOS_TAKEN_HUD()
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_AMATEUR_PHOTOGRAPHY
			IF GET_MODE_STATE() > eMODESTATE_GO_TO_POINT
			AND GET_MODE_STATE() < eMODESTATE_LEAVE_AREA
			AND serverBD.iPhotosTaken > 0
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC DRAW_PHOTOS_TAKEN_HUD()
	IF SHOULD_DRAW_PHOTOS_TAKEN_HUD()
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(serverBD.iPhotosTaken, ciAMATEUR_PHOTOGRAPHY_MAX_PHOTOS, "FHHUD_PHOTOS")
	ENDIF
ENDPROC

///////////////////////////////////
////        TIMER HUD   	   ////
/////////////////////////////////// 

FUNC BOOL HAVE_PROCESS_HURRY_UP_TIMER_CONDITIONS_BEEN_MET()

	RETURN TRUE

ENDFUNC

FUNC BOOL IS_TIMER_ASCENDING()
	RETURN FALSE
ENDFUNC

FUNC SCRIPT_TIMER GET_COUNTDOWN_TIMER()

	RETURN serverBD.countdownTimer

ENDFUNC

FUNC INT GET_TIME_FOR_COUNTDOWN_TIMER()
	RETURN 600000
ENDFUNC

FUNC STRING GET_LABEL_FOR_COUNTDOWN_TIMER()

	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_UNDER_CONTROL
			RETURN "GO_UC_BSTIM"
	ENDSWITCH	

	RETURN "TIMER_TIME"
ENDFUNC

FUNC TIMER_STYLE GET_STYLE_FOR_COUNTDOWN_TIMER()
	RETURN TIMER_STYLE_DONTUSEMILLISECONDS
ENDFUNC

PROC DRAW_LEAVE_AREA_TIMER()

	IF NOT HAS_NET_TIMER_STARTED(serverBD.timerLeaveAreaTimer)
		EXIT
	ENDIF

	INT iEventTime
	HUD_COLOURS eColour = HUD_COLOUR_WHITE	
	
	iEventTime = (GET_LEAVE_AREA_TIMER_DURATION() - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timerLeaveAreaTimer))
	
	IF iEventTime > NEARLY_FINISHED_TIME
		eColour = HUD_COLOUR_WHITE
	ELSE
		eColour = HUD_COLOUR_RED
	ENDIF

	IF iEventTime < 0
		iEventTime = 0
	ENDIF
	
	IF IS_LANGUAGE_NON_ROMANIC()
		SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
	ELSE
		SET_MIDDLE_TITLE_POSITION_HUD_THIS_FRAME()
	ENDIF
	
	DRAW_GENERIC_TIMER(iEventTime, "FHHUD_LEAVE", 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1, PODIUMPOS_NONE, HUDORDER_DONTCARE, FALSE, eColour, HUDFLASHING_NONE, 0, FALSE, eColour)
ENDPROC

PROC DRAW_COUNTDOWN_TIMER()

	IF NOT HAS_NET_TIMER_STARTED(serverBD.countdownTimer)
		EXIT
	ENDIF

	INT iEventTime
	HUD_COLOURS eColour = HUD_COLOUR_WHITE	
	
	IF IS_TIMER_ASCENDING()
		iEventTime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.countdownTimer)
		
		IF iEventTime < (GET_TIME_FOR_COUNTDOWN_TIMER() - NEARLY_FINISHED_TIME)
			eColour = HUD_COLOUR_WHITE
		ELSE
			eColour = HUD_COLOUR_RED
		ENDIF
	ELSE
		iEventTime = (GET_TIME_FOR_COUNTDOWN_TIMER() - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.countdownTimer))
		
		IF iEventTime > NEARLY_FINISHED_TIME
			eColour = HUD_COLOUR_WHITE
		ELSE
			eColour = HUD_COLOUR_RED
		ENDIF
	ENDIF
	
	IF iEventTime < 0
		iEventTime = 0
	ENDIF
	
	IF IS_LANGUAGE_NON_ROMANIC()
		SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
	ELSE
		SET_MIDDLE_TITLE_POSITION_HUD_THIS_FRAME()
	ENDIF
	
	DRAW_GENERIC_TIMER(iEventTime, GET_LABEL_FOR_COUNTDOWN_TIMER(), 0, GET_STYLE_FOR_COUNTDOWN_TIMER(), -1, PODIUMPOS_NONE, HUDORDER_DONTCARE, FALSE, eColour, HUDFLASHING_NONE, 0, FALSE, eColour)

ENDPROC

FUNC BOOL SHOULD_SHOW_BOTTOM_RIGHT_TIMER_BEFORE_END()
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_AMATEUR_PHOTOGRAPHY			RETURN GET_MODE_STATE() > eMODESTATE_GO_TO_POINT
	ENDSWITCH	
	RETURN FALSE
ENDFUNC

PROC DRAW_BOTTOM_RIGHT_HURRY_UP_TIMER()
	
	INT iEventTime
	HUD_COLOURS eColour
	
	IF NOT HAS_NET_TIMER_STARTED(serverBD.ModeTimer)
		EXIT
	ENDIF

	iEventTime =  (GET_MODE_TIME_LIMIT_INCLUDING_TUTORIAL_OFFSET() - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.ModeTimer))
	
	IF GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iMissionPosixEndForRivals = (-1)
		GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iMissionPosixEndForRivals = (GET_CLOUD_TIME_AS_INT() + (iEventTime/1000))
		PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - iMissionPosixEndForRivals = ", GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iMissionPosixEndForRivals)
	ENDIF
	
	// Only show in last 5 mins
	IF iEventTime > 1000 * 60 * 5
	AND NOT SHOULD_SHOW_BOTTOM_RIGHT_TIMER_BEFORE_END()
		EXIT
	ENDIF
	
	IF iEventTime < 0
		iEventTime = 0
	ENDIF
	
	IF iEventTime > NEARLY_FINISHED_TIME
		eColour = HUD_COLOUR_WHITE
	ELSE
		eColour = HUD_COLOUR_RED
	ENDIF
	
	STRING strTemp
	
	strTemp = "SMHUD_TIMEREM"
		
	DRAW_GENERIC_TIMER(iEventTime, strTemp, 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1, PODIUMPOS_NONE, HUDORDER_BOTTOM , FALSE, eColour, HUDFLASHING_NONE, 0, FALSE, eColour)
	
	IF HAS_NET_TIMER_EXPIRED_READ_ONLY(serverBd.modeTimer, GET_MODE_TIME_LIMIT_INCLUDING_TUTORIAL_OFFSET() - 5000)
	AND NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(serverBd.modeTimer, GET_MODE_TIME_LIMIT_INCLUDING_TUTORIAL_OFFSET() - 4000)
		IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_PLAYED_5_SECOND_AUDIO)
			PLAY_SOUND_FRONTEND(-1, "5s", "MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
			SET_LOCAL_BIT(eLOCALBITSET_PLAYED_5_SECOND_AUDIO)
		ENDIF
	ELSE
		IF IS_LOCAL_BIT_SET(eLOCALBITSET_PLAYED_5_SECOND_AUDIO)
			CLEAR_LOCAL_BIT(eLOCALBITSET_PLAYED_5_SECOND_AUDIO)
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_START_OF_MODE_GLOBAL_PING_DELAY_BOTTOM_RIGHT_TIMER()
	
	#IF IS_DEBUG_BUILD
	IF bDoBottomRightPrints
		PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [BOTTOMRIGHT] - MAINTAIN_START_OF_MODE_GLOBAL_PING_DELAY_BOTTOM_RIGHT_TIMER beng called")
	ENDIF
	#ENDIF
	
	IF iMissionEntityToUseForBottomRightTimer = (-1)
		
		#IF IS_DEBUG_BUILD
		IF bDoBottomRightPrints
			PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [BOTTOMRIGHT] - iMissionEntityToUseForBottomRightTimer = (-1)")
		ENDIF
		#ENDIF
	
		INT iMissionEntity
		
		REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
			#IF IS_DEBUG_BUILD
			IF bDoBottomRightPrints
				PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [BOTTOMRIGHT] - evaluating iMissionEntity")
			ENDIF
			#ENDIF
			IF HAS_NET_TIMER_STARTED(serverBD.timerDelayGlobalPing[iMissionEntity])
				iMissionEntityToUseForBottomRightTimer = iMissionEntity
				#IF IS_DEBUG_BUILD
				IF bDoBottomRightPrints
					PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [BOTTOMRIGHT] - serverBD.timerDelayGlobalPing[", iMissionEntity, "] net timer has started. iMissionEntityToUseForBottomRightTimer = ", iMissionEntityToUseForBottomRightTimer)
				ENDIF
				#ENDIF
			ENDIF
		ENDREPEAT
		
	ELSE
		
		#IF IS_DEBUG_BUILD
		IF bDoBottomRightPrints
			PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [BOTTOMRIGHT] - iMissionEntityToUseForBottomRightTimer = ", iMissionEntityToUseForBottomRightTimer)
		ENDIF
		#ENDIF
		
		IF HAS_NET_TIMER_STARTED(serverBD.timerDelayGlobalPing[iMissionEntityToUseForBottomRightTimer])
		
			IF NOT HAS_NET_TIMER_EXPIRED(serverBD.timerDelayGlobalPing[iMissionEntityToUseForBottomRightTimer], GET_DELAY_GLOBAL_PING_TIME_LIMIT())
			
				#IF IS_DEBUG_BUILD
				IF bDoBottomRightPrints
					PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [BOTTOMRIGHT] - timerDelayGlobalPing[", iMissionEntityToUseForBottomRightTimer, "] has not expired")
				ENDIF
				#ENDIF
				
				INT iEventTime
				HUD_COLOURS eColour
		
				iEventTime = (GET_DELAY_GLOBAL_PING_TIME_LIMIT() - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timerDelayGlobalPing[iMissionEntityToUseForBottomRightTimer]))
					
				#IF IS_DEBUG_BUILD
				IF bDoBottomRightPrints
					PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [BOTTOMRIGHT] - iEventTime = ", iEventTime)
				ENDIF
				#ENDIF
				
				IF iEventTime <= 0
					EXIT
				ENDIF
				
				IF iEventTime > MISSION_ENTITY_NEARLY_GLOBAL_TIME
					eColour = HUD_COLOUR_WHITE
				ELSE
					eColour = HUD_COLOUR_RED
				ENDIF
				
				#IF IS_DEBUG_BUILD
				IF bDoBottomRightPrints
					PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [BOTTOMRIGHT] - calling DRAW_GENERIC_TIMER(iEventTime, GR_GLBPNG, 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1, PODIUMPOS_NONE, HUDORDER_DONTCARE, FALSE, eColour, HUDFLASHING_NONE, 0, FALSE, eColour)")
				ENDIF
				#ENDIF
				
				DRAW_GENERIC_TIMER(iEventTime, "GR_GLBPNG", 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1, PODIUMPOS_NONE, HUDORDER_FOURTHBOTTOM, FALSE, eColour, HUDFLASHING_NONE, 0, FALSE, eColour)
			ENDIF
		ELSE
			iMissionEntityToUseForBottomRightTimer = -1
		ENDIF		
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_DRAW_COUNTDOWN_TIMER()

	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_UNDER_CONTROL
			RETURN IS_SERVER_BIT_SET(eSERVERBITSET_ANY_PLAYER_IN_RESTRICTED_AREA)
	ENDSWITCH

	IF GET_MODE_STATE() = eMODESTATE_MEET_UP
	OR GET_MODE_STATE() = eMODESTATE_WAIT_FOR_PASSENGERS
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

PROC MAINTAIN_BOTTOM_RIGHT_HUD()
	
	IF NOT GB_IS_IT_SAFE_TO_DISPLAY_BOSS_MISSION_UI(DEFAULT, DEFAULT, (NOT IS_LOCAL_BIT_SET(eLOCALBITSET_STARTED_TUTORIAL_SESSION)))
		EXIT
	ENDIF
	
	IF NETWORK_IS_IN_MP_CUTSCENE()
		EXIT
	ENDIF
	
	IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_OPENING_PHONECALL_DONE_OR_NEVER_GOING_TO_HAPPEN)
		EXIT
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(serverBD.modeTimer)
		EXIT
	ENDIF
	
	IF GET_MODE_STATE() != eMODESTATE_LEAVE_AREA
	
		IF SHOULD_DRAW_COUNTDOWN_TIMER()
			DRAW_COUNTDOWN_TIMER()
		ENDIF
		
		DRAW_AIR_RESPAWNS_HUD()
		DRAW_ELIMINATE_HUD()
		DRAW_BOTTOM_RIGHT_STATIONARY_AMBUSH_HUD()
		DRAW_DELIVERIES_MADE_HUD()
		DRAW_SCORE_HUD()
		DRAW_PHOTOS_TAKEN_HUD()
		DRAW_HEALTH_HUD()
		
		IF GET_END_REASON() = eENDREASON_NO_REASON_YET
			IF SHOULD_MONITOR_END_REASON()
				IF IS_THIS_VARIATION_A_SELL_MISSION()
					IF IS_SERVER_BIT_SET(eSERVERBITSET_TIME_FOR_HURRY_UP_COLLECT_UI)
					OR IS_SERVER_BIT_SET(eSERVERBITSET_TIME_FOR_HURRY_UP_DELIVER_UI)
						IF SHOULD_SHOW_HURRY_UP_TEXT()
							SET_LOCAL_BIT(eLOCALBITSET_DO_HURRY_UP_UPDATE_TEXT_MESSAGE)
						ENDIF
					ENDIF
				ELSE
					IF IS_SERVER_BIT_SET(eSERVERBITSET_TIME_FOR_HURRY_UP_COLLECT_UI)
					OR IS_SERVER_BIT_SET(eSERVERBITSET_TIME_FOR_HURRY_UP_DELIVER_UI)
						IF SHOULD_SHOW_HURRY_UP_TEXT()
							SET_LOCAL_BIT(eLOCALBITSET_DO_HURRY_UP_UPDATE_TEXT_MESSAGE)
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
			
		ENDIF
	ENDIF
	
	IF GET_END_REASON() = eENDREASON_NO_REASON_YET
	AND SHOULD_DRAW_TIME_REMAINING_HUD()
		DRAW_BOTTOM_RIGHT_HURRY_UP_TIMER()
	ENDIF
	
	MAINTAIN_START_OF_MODE_GLOBAL_PING_DELAY_BOTTOM_RIGHT_TIMER()
	
ENDPROC

///////////////////////////////////
////      GLOBAL SIGNAL  	   ////
///////////////////////////////////

FUNC BOOL SHOULD_DO_WARNING_HELP()
	IF IS_THIS_VARIATION_A_SETUP_MISSION()
	AND NOT IS_BIT_SET(MPGlobalsAmbience.iFmGbHelpBitSet4, BI_FM_GANG_BOSS_HELP_4_DO_CARGO_VISIBLE_WARNING_SETUP)
		RETURN TRUE
	ELIF IS_THIS_VARIATION_A_STEAL_MISSION()
	AND NOT IS_BIT_SET(MPGlobalsAmbience.iFmGbHelpBitSet4, BI_FM_GANG_BOSS_HELP_4_DO_CARGO_VISIBLE_WARNING_STEAL)
		RETURN TRUE
	ELIF IS_THIS_VARIATION_A_SELL_MISSION()
	AND NOT IS_BIT_SET(MPGlobalsAmbience.iFmGbHelpBitSet4, BI_FM_GANG_BOSS_HELP_4_DO_CARGO_VISIBLE_WARNING_SELL)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_BEACON_SIGNIFIER_BESIDE_RADAR()
	
	IF IS_SERVER_BIT_SET(eSERVERBITSET_MISSION_ENTITY_VISIBLE_TICKER_SENT)
		IF NOT IS_PLAYER_GANG_OFF_THE_RADAR()
			IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_ACTIVATED_GR_BEACON_SIGNIFIER_BESIDE_RADAR)
				DISPLAY_GR_BEACON_SIGNIFIER_BESIDE_RADAR()
				SET_LOCAL_BIT(eLOCALBITSET_ACTIVATED_GR_BEACON_SIGNIFIER_BESIDE_RADAR)
				
				IF SHOULD_DO_WARNING_HELP()
					TRIGGER_HELP(eHELPTEXT_CARGO_VISIBLE_WARNING)
				ENDIF
			ENDIF
		ELSE
			IF IS_LOCAL_BIT_SET(eLOCALBITSET_ACTIVATED_GR_BEACON_SIGNIFIER_BESIDE_RADAR)
				CLEAR_GR_BEACON_SIGNIFIER_FROM_RADAR()
				CLEAR_LOCAL_BIT(eLOCALBITSET_ACTIVATED_GR_BEACON_SIGNIFIER_BESIDE_RADAR)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

///////////////////////////////////
////      MINIMAP ARROW  	   ////
///////////////////////////////////

PROC MAINTAIN_FLASH_PLAYER_ARROW_RED()
	
//	SWITCH GET_GANGOPS_VARIATION()
//		DEFAULT
			IF IS_SERVER_BIT_SET(eSERVERBITSET_MISSION_ENTITY_VISIBLE_TICKER_SENT)
				FLASH_MY_PLAYER_ARROW_RED_FOR_REST_OF_MISSION()
			ENDIF
//		BREAK
//	ENDSWITCH
	
ENDPROC

///////////////////////////////////
////         TICKERS    	   ////
/////////////////////////////////// 
   
SCRIPT_TIMER shownGlobalEntitiesTickerTimer
PROC MAINTAIN_CLIENT_GLOBAL_PING_TICKER()

	IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_DID_AFTER_DEATH_GLOBAL_PING_TICKER)
		IF IS_SERVER_BIT_SET(eSERVERBITSET_MISSION_ENTITY_VISIBLE_TICKER_SENT)
			IF NOT HAS_NET_TIMER_EXPIRED(shownGlobalEntitiesTickerTimer, 5000)
				IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_DO_AFTER_DEATH_GLOBAL_PING_TICKER)
					IF IS_BIT_SET(iPlayerDeadBitset, NATIVE_TO_INT(PLAYER_ID()))
						SET_LOCAL_BIT(eLOCALBITSET_DO_AFTER_DEATH_GLOBAL_PING_TICKER)
						PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_CLIENT_GLOBAL_PING_TICKER - Local player died within 5 seconds after global ping. Sending death ticker.")
					ENDIF
				ELSE
					IF NOT IS_BIT_SET(iPlayerDeadBitset, NATIVE_TO_INT(PLAYER_ID()))
						SEND_BUSINESS_ENTITY_VISIBLE_TICKER(FMMC_TYPE_FM_GANGOPS, TRUE)
						SET_LOCAL_BIT(eLOCALBITSET_DID_AFTER_DEATH_GLOBAL_PING_TICKER)
						PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_CLIENT_GLOBAL_PING_TICKER - Sent death ticker.")
					ENDIF
				ENDIF
			ELSE
				SET_LOCAL_BIT(eLOCALBITSET_DID_AFTER_DEATH_GLOBAL_PING_TICKER)
				PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_CLIENT_GLOBAL_PING_TICKER - Local player still alive 5 seconds after global ping. Not sending death ticker.")
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

INT iHeldCargo = -1
PROC MAINTAIN_COLLECTED_DROPPED_TICKERS()
	IF GET_END_REASON() != eENDREASON_NO_REASON_YET
		EXIT
	ENDIF
	
	INT iMissionEntity
	REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
		IF iHeldCargo != (-1)
			IF iHeldCargo = iMissionEntity
			AND MissionEntityHolderPlayerID[iMissionEntity] != PLAYER_ID()
				iHeldCargo = GET_MISSION_ENTITY_PLAYER_IS_HOLDING(PLAYER_ID())

				IF NOT IS_MISSION_ENTITY_CLIENT_BIT_SET(iMissionEntity, PARTICIPANT_ID(), eMISSIONENTITYCLIENTBITSET_DELIVERED)
				AND NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_I_AM_IN_CARRIER_VEHICLE)
				AND serverBD.sMissionEntities.eType != eMISSIONENTITYTYPE_CAR
					SEND_BUSINESS_ENTITY_COLLECTED_DROPPED_TICKER(FMMC_TYPE_FM_GANGOPS, PLAYER_ID(), GB_GET_LOCAL_PLAYER_GANG_BOSS(), TRUE)
					PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [MAINTAIN_COLLECTED_DROPPED_TICKERS] - Local player dropped mission entity #", iMissionEntity, ".")
				ENDIF
			ENDIF
		ELSE
			IF MissionEntityHolderPlayerID[iMissionEntity] = PLAYER_ID()
				IF NOT IS_MISSION_ENTITY_CLIENT_BIT_SET(iMissionEntity, PARTICIPANT_ID(), eMISSIONENTITYCLIENTBITSET_DELIVERED)
				AND NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_DELIVERED)
					iHeldCargo = iMissionEntity
					SEND_BUSINESS_ENTITY_COLLECTED_DROPPED_TICKER(FMMC_TYPE_FM_GANGOPS, PLAYER_ID(), GB_GET_LOCAL_PLAYER_GANG_BOSS(), FALSE)
					PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - [MAINTAIN_COLLECTED_DROPPED_TICKERS] - Local player collected mission entity #", iMissionEntity, ".")
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC MAINTAIN_TICKERS()
	//MAINTAIN_COLLECTED_DROPPED_TICKERS()
	MAINTAIN_CLIENT_GLOBAL_PING_TICKER()
ENDPROC

///////////////////////////////////
////        MAINTAINS   	   ////
/////////////////////////////////// 

PROC MAINTAIN_HUD()
	IF NOT IS_CONTACT_IN_PHONEBOOK(GET_GANGOPS_CONTACT(), MULTIPLAYER_BOOK) 
		ADD_CONTACT_TO_PHONEBOOK(GET_GANGOPS_CONTACT(), MULTIPLAYER_BOOK, FALSE)
		SET_LOCAL_BIT(eLOCALBITSET_ADDED_PHONE_CONTACT_TEMPORARILY_FOR_MISSION)
	ENDIF
	
	MAINTAIN_HELP()
	MAINTAIN_BLIPS()
	MAINTAIN_TICKERS()
	MAINTAIN_RADAR_ZOOM()
	MAINTAIN_BIG_MESSAGES()
	MAINTAIN_ALLOW_QUICK_GPS()
	MAINTAIN_BOTTOM_RIGHT_HUD()
	MAINTAIN_FLASH_PLAYER_ARROW_RED()
	MAINTAIN_BEACON_SIGNIFIER_BESIDE_RADAR()
	
	IF SAFE_TO_DISPLAY_CALL_OR_TEXT()
		MAINTAIN_PHONECALLS()
		MAINTAIN_TEXT_MESSAGES()
		MAINTAIN_DIALOGUE()
	ELSE
		PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] [MAINTAIN_HUD] - Player is in office or transition out. SAFE_TO_DISPLAY_CALL_OR_TEXT = false.")
	ENDIF
	
ENDPROC


