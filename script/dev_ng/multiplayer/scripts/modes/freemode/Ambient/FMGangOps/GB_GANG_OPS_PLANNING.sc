//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        GB_GANG_OPS_PLANNING.sc																				//
// Description: Script for managing the gang ops planning board inside the IAA base.								//
// Written by:  Steve Tiley																							//
// Date:  		11/09/2017																							//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_ambience.sch"
USING "net_wait_zero.sch"
USING "fmmc_header.sch"
USING "net_simple_interior.sch"
USING "net_spawn_group.sch"

#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF

USING "net_gang_ops_idle.sch"
USING "net_gang_ops_planning.sch"
USING "net_gang_ops_finale.sch"

//╒════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ DATA ╞═════════════════════════════════╡
//╘════════════════════════════════════════════════════════════════════════╛

// ENUMS
ENUM SCRIPT_STAGE_ENUM
	STAGE_INIT,
	STAGE_PROCESS_IDLE,
	STAGE_PROCESS_PLANNING,
	STAGE_PROCESS_FINALE,
	STAGE_PROCESS_LAUNCH,
	STAGE_PROCESS_HEIST_SELECTED,
	STAGE_CLEANUP
ENDENUM
SCRIPT_STAGE_ENUM eStage = STAGE_INIT

CONST_INT ARBITRARY_DELAY 5000

// LOCAL VARIABLES
//INT iContentDataBS
BOOL bBecomeBossHelpDone = FALSE
SCRIPT_TIMER stArbitraryDelayTimer

CAMERA_INDEX ciCoronaCam

//GET_UGC_CONTENT_STRUCT 	sGetContentData

#IF IS_DEBUG_BUILD
// Debug
BOOL bResetGangOps = FALSE
BOOL bResetGangOpsPlanning = FALSE
BOOL bResetGangOpsFinale = FALSE
BOOL bEndScript = FALSE
BOOL bPrintToLogs = FALSE
INT iRootContentIDHashCache
BOOL bFinaleDebugCache

WIDGET_GROUP_ID planningWidgets
#ENDIF

BOOL bDisablePlanningScreenShortly
SCRIPT_TIMER stDisablePlanningScreenTimer

FUNC BOOL IS_PAUSE_MENU_BEING_ACTIONED()
	BOOL bPausePressed = (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE) OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE) OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE))
								
	IF bPausePressed
		bDisablePlanningScreenShortly = TRUE
		PRINTLN("IS_PAUSE_MENU_BEING_ACTIONED - bPausePressed - Disable planning screen")
		RETURN TRUE
	ELSE
		IF bDisablePlanningScreenShortly
			PRINTLN("IS_PAUSE_MENU_BEING_ACTIONED - bDisablePlanningScreenShortly - Disable planning screen")
			IF HAS_NET_TIMER_EXPIRED(stDisablePlanningScreenTimer, 1000)
				RESET_NET_TIMER(stDisablePlanningScreenTimer)
				bDisablePlanningScreenShortly = FALSE
				PRINTLN("IS_PAUSE_MENU_BEING_ACTIONED - bDisablePlanningScreenShortly - Timer expired, can be used again")
				RETURN FALSE
			ELSE
				PRINTLN("IS_PAUSE_MENU_BEING_ACTIONED - bDisablePlanningScreenShortly - Waiting for timer to expire")
			ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_PLAYER_INTERACT_WITH_SCREEN(INT iScreen)

	IF IS_GANG_OPS_PLANNING_BOARD_DISABLED_THIS_FRAME()
	OR IS_GANG_OPS_PLANNING_BOARD_DISABLED()
		PRINTLN("canuseplanningscreen -2")
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		PRINTLN("canuseplanningscreen -1")
		RETURN FALSE
	ENDIF
	
	IF NOT LOAD_INTERACT_ANIM()
		PRINTLN("canuseplanningscreen -3")
		RETURN FALSE
	ENDIF
	
	IF IS_CUTSCENE_PLAYING()
		PRINTLN("canuseplanningscreen -4")
		RETURN FALSE
	ENDIF
	
	IF NOT GB_IS_GLOBAL_CLIENT_BIT0_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_0_COMPLETED_INITIAL_GANG_BOSS_DATA_SETUP)
		PRINTLN("canuseplanningscreen -5")
		RETURN FALSE
	ENDIF
	
	IF IS_PAUSE_MENU_BEING_ACTIONED()
		PRINTLN("canuseplanningscreen -6")
		RETURN FALSE
	ENDIF
	
	IF GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_NPC_INVITE
	OR GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_INVITE_FROM_MP
		PRINTLN("canuseplanningscreen -99")
		RETURN FALSE
	ENDIF
	
	IF IS_PHONE_ONSCREEN()
		PRINTLN("canuseplanningscreen -98")
		RETURN FALSE
	ENDIF
	
//	IF GB_IS_LOCAL_GANGOPS_CALL_BIT_SET(eGB_LOCAL_GANGOPS_CALL_BITSET_NOTIFICATION_SKIPPED_PREP_MISSION) 
//	OR GB_IS_LOCAL_GANGOPS_CALL_BIT_SET(eGB_LOCAL_GANGOPS_CALL_BITSET_NOTIFICATION_SKIPPED_CASH_DROP_PREP_MISSION)
//		PRINTLN("canuseplanningscreen -6")
//		RETURN FALSE
//	ENDIF

	FLOAT fHeading

	SWITCH iScreen
		CASE GANG_OPS_IDLE_SCREEN
			IF IS_BROWSER_OPEN()
				PRINTLN("canuseplanningscreen 0")
				RETURN FALSE
			ENDIF
		
			IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())
			AND GB_IS_PLAYER_PERMANENT_PARTICIPANT(PLAYER_ID())
				PRINTLN("canuseplanningscreen 1")
				RETURN FALSE
			ENDIF
			
			IF g_GangOpsIdle.sIdleData.piLeader != PLAYER_ID()
				PRINTLN("canuseplanningscreen 2")
				RETURN FALSE
			ENDIF
			
			IF GET_PLAYER_CURRENT_GANG_OPS_MISSION_STRAND(PLAYER_ID()) != GANG_OPS_MISSION_STRAND_INVALID
				PRINTLN("canuseplanningscreen 3")
				RETURN FALSE
			ENDIF
			
			IF g_GangOpsIdle.sStateData.eState < GOBS_DISPLAYING
			OR g_GangOpsIdle.sStateData.eState >= GOBS_INUSE
				PRINTLN("canuseplanningscreen 4")
				RETURN FALSE
			ENDIF
			
			IF NOT IS_BIT_SET(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_IN_ROOM_LONG_ENOUGH_TO_INTERACT)
				PRINTLN("canuseplanningscreen 5")
				RETURN FALSE
			ENDIF
			
			IF NOT IS_BIT_SET(g_iFacilityHeistPlanningSeats, ciFACILITY_HEIST_PLANNING_SEATS_READY_FOR_BOARD)
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					fHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
					IF fHeading > 27 AND fHeading < 252
						PRINTLN("canuseplanningscreen 6")
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE GANG_OPS_PLANNING_SCREEN
			IF IS_BROWSER_OPEN()
				PRINTLN("canuseplanningscreen 19")
				RETURN FALSE
			ENDIF
		
			IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())
			AND GB_IS_PLAYER_PERMANENT_PARTICIPANT(PLAYER_ID())
				PRINTLN("canuseplanningscreen 7")
				RETURN FALSE
			ENDIF
		
			IF g_GangOpsPlanning.sStrandData.piLeader != PLAYER_ID()
				PRINTLN("canuseplanningscreen 8")
				RETURN FALSE
			ENDIF
			
			IF g_GangOpsPlanning.sStateData.eState < GOBS_DISPLAYING
			OR g_GangOpsPlanning.sStateData.eState >= GOBS_INUSE
				PRINTLN("canuseplanningscreen 9")
				RETURN FALSE
			ENDIF
			
			IF NOT IS_BIT_SET(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_IN_ROOM_LONG_ENOUGH_TO_INTERACT)
				PRINTLN("canuseplanningscreen 10")
				RETURN FALSE
			ENDIF
		
			IF IS_BIT_SET(g_GangOpsPlanning.iPlanningBitSet, GANG_OPS_BITSET_LAUNCH_MISSION)
			OR IS_BIT_SET(g_GangOpsPlanning.iPlanningBitSet, GANG_OPS_BITSET_MISSION_LAUNCHED)
				PRINTLN("canuseplanningscreen 11")
				RETURN FALSE
			ENDIF
			
			IF NOT IS_BIT_SET(g_iFacilityHeistPlanningSeats, ciFACILITY_HEIST_PLANNING_SEATS_READY_FOR_BOARD)
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					fHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
					IF fHeading > 27 AND fHeading < 252
						PRINTLN("canuseplanningscreen 12")
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE GANG_OPS_FINALE_SCREEN
			IF IS_BROWSER_OPEN()
				PRINTLN("canuseplanningscreen 20")
				RETURN FALSE
			ENDIF
		
			IF IS_PLAYER_IN_CORONA()
			OR IS_TRANSITION_SESSION_LAUNCHING()
			OR IS_TRANSITION_SESSION_RESTARTING()
				PRINTLN("canuseplanningscreen 13")
				RETURN FALSE
			ENDIF
			
			IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())
			AND GB_IS_PLAYER_PERMANENT_PARTICIPANT(PLAYER_ID())
				PRINTLN("canuseplanningscreen 13.5")
				RETURN FALSE
			ENDIF
			
			IF g_GangOpsFinale.sFinaleData.piLeader != PLAYER_ID()
				PRINTLN("canuseplanningscreen 14")
				RETURN FALSE
			ENDIF
			
			IF g_GangOpsFinale.sStateData.eState < GOBS_DISPLAYING
			OR g_GangOpsFinale.sStateData.eState >= GOBS_INUSE
				PRINTLN("canuseplanningscreen 15")
				RETURN FALSE
			ENDIF
			
			IF NOT IS_BIT_SET(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_IN_ROOM_LONG_ENOUGH_TO_INTERACT)
				PRINTLN("canuseplanningscreen 16")
				RETURN FALSE
			ENDIF
		
			IF IS_BIT_SET(g_GangOpsFinale.iFinaleBitSet, GANG_OPS_BITSET_LAUNCH_MISSION)
				PRINTLN("canuseplanningscreen 17")
				RETURN FALSE
			ENDIF
			
			IF NOT IS_BIT_SET(g_iFacilityHeistPlanningSeats, ciFACILITY_HEIST_PLANNING_SEATS_READY_FOR_BOARD)
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					fHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
					IF fHeading > 27 AND fHeading < 252
						//PRINTLN("canuseplanningscreen 18")
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH 

	RETURN TRUE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_POSITION_TO_USE_BOARD()
	IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<353.70377, 4877.57910, -61.60188>>, FALSE) <= 6.0
		RETURN TRUE
	ENDIF
	
	IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<350.806366,4872.391113,-61.794243>>, <<348.484436,4866.689941,-58.351421>>, 1.750000)
		RETURN TRUE
	ENDIF
	
	IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<350.806366,4872.391113,-61.794243>>, <<346.147400,4868.041016,-58.351421>>, 1.750000)
		RETURN TRUE
	ENDIF
	
	IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<350.806366,4872.391113,-61.794243>>, <<346.944092,4866.923828,-58.351421>>, 1.750000)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC TEXT_LABEL_63 GET_IDLE_SCREEN_CONTEXT_HELP()
	TEXT_LABEL_63 tlToReturn = "HPCONTEXT_STP"
	IF IS_BIT_SET(g_iFacilityHeistPlanningSeats, ciFACILITY_HEIST_PLANNING_SEATS_READY_FOR_BOARD)
		tlToReturn += "c"
	ENDIF
	RETURN tlToReturn
ENDFUNC

FUNC TEXT_LABEL_63 GET_REPLAY_SCREEN_CONTEXT_HELP()
	TEXT_LABEL_63 tlToReturn = "HPCONTEXT_RPLY"
	IF IS_BIT_SET(g_iFacilityHeistPlanningSeats, ciFACILITY_HEIST_PLANNING_SEATS_READY_FOR_BOARD)
		tlToReturn += "c"
	ELSE
		IF NOT AM_I_TRANSITION_SESSIONS_STARTING_GANG_OPS_QUICK_MATCH()
			tlToReturn += "b"
		ENDIF
	ENDIF
	RETURN tlToReturn
ENDFUNC

FUNC TEXT_LABEL_63 GET_PLANNING_SCREEN_CONTEXT_HELP()
	TEXT_LABEL_63 tlToReturn = "HPCONTEXT_PLAN"
	IF IS_BIT_SET(g_iFacilityHeistPlanningSeats, ciFACILITY_HEIST_PLANNING_SEATS_READY_FOR_BOARD)
		tlToReturn += "c"
	ELSE
		IF NOT AM_I_TRANSITION_SESSIONS_STARTING_GANG_OPS_QUICK_MATCH()
			tlToReturn += "b"
		ENDIF
	ENDIF
	RETURN tlToReturn
ENDFUNC

FUNC TEXT_LABEL_63 GET_FINALE_SCREEN_CONTEXT_HELP()
	TEXT_LABEL_63 tlToReturn = "HPCONTEXT_FIN"
	IF IS_BIT_SET(g_iFacilityHeistPlanningSeats, ciFACILITY_HEIST_PLANNING_SEATS_READY_FOR_BOARD)
		tlToReturn += "c"
	ELSE
		IF NOT AM_I_TRANSITION_SESSIONS_STARTING_GANG_OPS_QUICK_MATCH()
			tlToReturn += "b"
		ENDIF
	ENDIF
	RETURN tlToReturn
ENDFUNC

PROC CLEANUP_IDLE_BOARD_CONTEXT()
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HPCONTEXT_STP")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HPCONTEXT_STPb")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HPCONTEXT_STPc")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HPCONTEXT_RPLY")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HPCONTEXT_RPLYb")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HPCONTEXT_RPLYc")
		CLEAR_HELP()
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - CLEANUP_IDLE_BOARD_CONTEXT -  Help cleaned up")
	ENDIF
ENDPROC

PROC DO_IDLE_BECOME_BOSS_HELP()
	IF bBecomeBossHelpDone = FALSE
		IF g_GangOpsIdle.sIdleData.bShowReplayBoard
			IF IS_BIT_SET(g_iFacilityHeistPlanningSeats, ciFACILITY_HEIST_PLANNING_SEATS_READY_FOR_BOARD)
				PRINT_HELP_FOREVER("HRBOARD_REGb")
			ELSE
				PRINT_HELP_FOREVER("HRBOARD_REG")
			ENDIF
		ELSE
			IF IS_BIT_SET(g_iFacilityHeistPlanningSeats, ciFACILITY_HEIST_PLANNING_SEATS_READY_FOR_BOARD)
				PRINT_HELP_FOREVER("HIBOARD_REGb")
			ELSE
				PRINT_HELP_FOREVER("HIBOARD_REG")
			ENDIF
		ENDIF
		bBecomeBossHelpDone = TRUE
	ENDIF
ENDPROC

PROC CLEAR_IDLE_BECOME_BOSS_HELP()
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HRBOARD_REG")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HIBOARD_REG")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HRBOARD_REGb")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HIBOARD_REGb")
		CLEAR_HELP(TRUE)
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - CLEAR_IDLE_BECOME_BOSS_HELP -  Become boss help cleared 1")
	ENDIF
	
	IF bBecomeBossHelpDone
		bBecomeBossHelpDone = FALSE
	ENDIF
ENDPROC

PROC MAINTAIN_IDLE_BOARD_USEAGE()
	IF CAN_PLAYER_INTERACT_WITH_SCREEN(GANG_OPS_IDLE_SCREEN)
		IF IS_PLAYER_IN_POSITION_TO_USE_BOARD()
		OR IS_BIT_SET(g_iFacilityHeistPlanningSeats, ciFACILITY_HEIST_PLANNING_SEATS_READY_FOR_BOARD)
			IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
				 CLEAR_IDLE_BECOME_BOSS_HELP()
				
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HP_EXPLAIN")
					CLEAR_HELP()
				ENDIF
				
				IF g_GangOpsIdle.sIdleData.bShowReplayBoard
					// Should we show the prompt to access the replay board?
					TEXT_LABEL_63 tlTemp2 = GET_REPLAY_SCREEN_CONTEXT_HELP()
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(tlTemp2)
						IF IS_BIT_SET(g_GangOpsIdle.iIdleBitSet, GANG_OPS_BITSET_SUPPRESS_HELP_SOUND)
							PRINT_HELP_FOREVER_NO_SOUND(tlTemp2)
							CLEAR_BIT(g_GangOpsIdle.iIdleBitSet, GANG_OPS_BITSET_SUPPRESS_HELP_SOUND)
						ELSE
							PRINT_HELP_FOREVER(tlTemp2)
						ENDIF
						CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - MAINTAIN_IDLE_BOARD_USEAGE -  Player is within range of board, registering context for replay board")
					ELSE
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
							CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - MAINTAIN_IDLE_BOARD_USEAGE -  Player has selected to access the replay board")
							SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW_IF_NEEDED()
							PRIVATE_TOGGLE_GANG_OPS_IDLE_INTERACTION(g_GangOpsIdle, TRUE)
							DO_INTERACT_ANIM()
							PLAY_SOUND_FROM_COORD(-1,"Use", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
							g_GangOpsIdle.sIdleData.iWarningScreenBuffer = 0
							CLEANUP_IDLE_BOARD_CONTEXT()
						ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT_SECONDARY)
						AND NOT IS_BIT_SET(g_iFacilityHeistPlanningSeats, ciFACILITY_HEIST_PLANNING_SEATS_READY_FOR_BOARD)
							IF REGISTER_FOR_GANG_OPS_HEIST_QUICK_MATCH()
								CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - MAINTAIN_IDLE_BOARD_USEAGE -  Player has selected to go on call for heist")
								CLEANUP_IDLE_BOARD_CONTEXT()
							ENDIF
						ENDIF
					ENDIF
				ELSE
					// Should we show the prompt to access the replay board?
					TEXT_LABEL_63 tlTemp = GET_IDLE_SCREEN_CONTEXT_HELP()
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(tlTemp)
						IF IS_BIT_SET(g_GangOpsIdle.iIdleBitSet, GANG_OPS_BITSET_SUPPRESS_HELP_SOUND)
							PRINT_HELP_FOREVER_NO_SOUND(tlTemp)
							CLEAR_BIT(g_GangOpsIdle.iIdleBitSet, GANG_OPS_BITSET_SUPPRESS_HELP_SOUND)
						ELSE
							PRINT_HELP_FOREVER(tlTemp)
						ENDIF
						CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - MAINTAIN_IDLE_BOARD_USEAGE -  Player is within range of board, registering context for replay board")
					ELSE
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
							CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - MAINTAIN_IDLE_BOARD_USEAGE -  Player has selected to access the idle/setup board")
							SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW_IF_NEEDED()
							PRIVATE_TOGGLE_GANG_OPS_IDLE_INTERACTION(g_GangOpsIdle, TRUE)
							DO_INTERACT_ANIM()
							PLAY_SOUND_FROM_COORD(-1,"Use", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
							g_GangOpsIdle.sIdleData.iWarningScreenBuffer = 0
							CLEANUP_IDLE_BOARD_CONTEXT()
						ENDIF
					ENDIF
				ENDIF
			ELSE
				CLEANUP_IDLE_BOARD_CONTEXT()
				DO_IDLE_BECOME_BOSS_HELP()
				PRINTLN("[idleboarduseage] - 4")
			ENDIF
		ELSE
			CLEAR_IDLE_BECOME_BOSS_HELP()
			PRINTLN("[idleboarduseage] - 3")
			CLEANUP_IDLE_BOARD_CONTEXT()
		ENDIF
	ELSE
		CLEAR_IDLE_BECOME_BOSS_HELP()
		PRINTLN("[idleboarduseage] - 0")
		CLEANUP_IDLE_BOARD_CONTEXT()
	ENDIF
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ PLANNING BOARD INTERATION ╞═════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════════════════════╛

PROC CLEANUP_PLANNING_BOARD_CONTEXT()
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HPCONTEXT_PLAN")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HPCONTEXT_PLANb")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HPCONTEXT_PLANc")
		CLEAR_HELP()
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - CLEANUP_PLANNING_BOARD_CONTEXT -  Help cleaned up")
	ENDIF
ENDPROC

PROC DO_PLANNING_BECOME_BOSS_HELP()
	IF bBecomeBossHelpDone = FALSE
		IF IS_BIT_SET(g_iFacilityHeistPlanningSeats, ciFACILITY_HEIST_PLANNING_SEATS_READY_FOR_BOARD)
			PRINT_HELP_FOREVER("HPBOARD_REGb")
		ELSE
			PRINT_HELP_FOREVER("HPBOARD_REG")
		ENDIF
		bBecomeBossHelpDone = TRUE
	ENDIF
ENDPROC

PROC CLEAR_PLANNING_BECOME_BOSS_HELP()
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HPBOARD_REG")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HPBOARD_REGb")
		CLEAR_HELP(TRUE)
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - CLEAR_PLANNING_BECOME_BOSS_HELP -  Become boss help cleared 1")
	ENDIF
	
	IF bBecomeBossHelpDone
		bBecomeBossHelpDone = FALSE
	ENDIF
ENDPROC

PROC MAINTAIN_PLANNING_BOARD_USEAGE()
	IF CAN_PLAYER_INTERACT_WITH_SCREEN(GANG_OPS_PLANNING_SCREEN)
		IF IS_PLAYER_IN_POSITION_TO_USE_BOARD()
		OR IS_BIT_SET(g_iFacilityHeistPlanningSeats, ciFACILITY_HEIST_PLANNING_SEATS_READY_FOR_BOARD)
			IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
				CLEAR_PLANNING_BECOME_BOSS_HELP()
				
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HP_EXPLAIN")
					CLEAR_HELP()
				ENDIF
				
				TEXT_LABEL_63 tlTemp = GET_PLANNING_SCREEN_CONTEXT_HELP()
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(tlTemp)
					IF IS_BIT_SET(g_GangOpsPlanning.iPlanningBitSet, GANG_OPS_BITSET_SUPPRESS_HELP_SOUND)
						PRINT_HELP_FOREVER_NO_SOUND(tlTemp)
						CLEAR_BIT(g_GangOpsPlanning.iPlanningBitSet, GANG_OPS_BITSET_SUPPRESS_HELP_SOUND)
					ELSE
						PRINT_HELP_FOREVER(tlTemp)
					ENDIF
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - MAINTAIN_PLANNING_BOARD_USEAGE -  Player is within range of board, registering context")
				ELSE
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
						CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - MAINTAIN_PLANNING_BOARD_USEAGE -  Player has selected to access the planning board")
						SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW_IF_NEEDED()
						PRIVATE_TOGGLE_GANG_OPS_INTERACTION(g_GangOpsPlanning, TRUE)
						DO_INTERACT_ANIM()
						PLAY_SOUND_FROM_COORD(-1,"Use", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
						g_GangOpsPlanning.sStrandData.iWarningScreenBuffer = 0
						CLEANUP_PLANNING_BOARD_CONTEXT()
					ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT_SECONDARY)
					AND NOT IS_BIT_SET(g_iFacilityHeistPlanningSeats, ciFACILITY_HEIST_PLANNING_SEATS_READY_FOR_BOARD)
						IF REGISTER_FOR_GANG_OPS_HEIST_QUICK_MATCH()
							CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - MAINTAIN_PLANNING_BOARD_USEAGE -  Player has selected to go on call for heist")
							CLEANUP_PLANNING_BOARD_CONTEXT()
						ENDIF
					ENDIF
				ENDIF
			ELSE
				CLEANUP_PLANNING_BOARD_CONTEXT()
				DO_PLANNING_BECOME_BOSS_HELP()
				PRINTLN("[planningboarduseage] - 4")
			ENDIF
		ELSE
			CLEAR_PLANNING_BECOME_BOSS_HELP()
			PRINTLN("[planningboarduseage] - 3")
			CLEANUP_PLANNING_BOARD_CONTEXT()
		ENDIF
	ELSE
		CLEAR_PLANNING_BECOME_BOSS_HELP()
		PRINTLN("[planningboarduseage] - 0")
		CLEANUP_PLANNING_BOARD_CONTEXT()
	ENDIF
ENDPROC

//╒═══════════════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ FINALE BOARD INTERATION ╞═════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════════════╛

PROC CLEANUP_FINALE_BOARD_CONTEXT()
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HPCONTEXT_FIN")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HPCONTEXT_FINb")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HPCONTEXT_FINc")
		CLEAR_HELP()
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - CLEANUP_FINALE_BOARD_CONTEXT -  Context cleaned up")
	ENDIF
ENDPROC

PROC DO_FINALE_BECOME_BOSS_HELP()
	IF bBecomeBossHelpDone = FALSE
		IF IS_BIT_SET(g_iFacilityHeistPlanningSeats, ciFACILITY_HEIST_PLANNING_SEATS_READY_FOR_BOARD)
			PRINT_HELP_FOREVER("HFBOARD_REGb")
		ELSE
			PRINT_HELP_FOREVER("HFBOARD_REG")
		ENDIF
		bBecomeBossHelpDone = TRUE
	ENDIF
ENDPROC

PROC CLEAR_FINALE_BECOME_BOSS_HELP()
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HFBOARD_REG")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HFBOARD_REGb")
		CLEAR_HELP(TRUE)
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - MAINTAIN_FINALE_BOARD_USEAGE -  Become boss help cleared 1")
	ENDIF
	
	IF bBecomeBossHelpDone
		bBecomeBossHelpDone = FALSE
	ENDIF
ENDPROC

PROC MAINTAIN_FINALE_BOARD_USEAGE()
	IF CAN_PLAYER_INTERACT_WITH_SCREEN(GANG_OPS_FINALE_SCREEN)
		IF IS_PLAYER_IN_POSITION_TO_USE_BOARD()
		OR IS_BIT_SET(g_iFacilityHeistPlanningSeats, ciFACILITY_HEIST_PLANNING_SEATS_READY_FOR_BOARD)
			IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
				CLEAR_FINALE_BECOME_BOSS_HELP()
				
				TEXT_LABEL_63 tlTemp = GET_FINALE_SCREEN_CONTEXT_HELP()
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(tlTemp)
					IF IS_BIT_SET(g_GangOpsFinale.iFinaleBitSet, GANG_OPS_BITSET_SUPPRESS_HELP_SOUND)
						PRINT_HELP_FOREVER_NO_SOUND(tlTemp)
						CLEAR_BIT(g_GangOpsFinale.iFinaleBitSet, GANG_OPS_BITSET_SUPPRESS_HELP_SOUND)
					ELSE
						PRINT_HELP_FOREVER(tlTemp)
					ENDIF
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - MAINTAIN_FINALE_BOARD_USEAGE -  Player is within range of board, registering context")
				ELSE
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
					AND NOT IS_ANY_TRANSITION_SESSION_INVITE_BEING_ACTIONED()
						SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW_IF_NEEDED()
						SET_BIT(g_GangOpsFinale.iFinaleBitSet, GANG_OPS_BITSET_LAUNCH_MISSION)
						PLAY_SOUND_FROM_COORD(-1,"Use", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
						CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - MAINTAIN_FINALE_BOARD_USEAGE -  Player has triggered context to launch the finale! GANG_OPS_BITSET_LAUNCH_MISSION SET")				
						CLEANUP_FINALE_BOARD_CONTEXT()
					ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT_SECONDARY)
					AND NOT IS_BIT_SET(g_iFacilityHeistPlanningSeats, ciFACILITY_HEIST_PLANNING_SEATS_READY_FOR_BOARD)
						IF REGISTER_FOR_GANG_OPS_HEIST_QUICK_MATCH()
							CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - MAINTAIN_PLANNING_BOARD_USEAGE -  Player has selected to go on call for heist")
							CLEANUP_FINALE_BOARD_CONTEXT()
						ENDIF
					ENDIF
				ENDIF
				
			ELSE
				CLEANUP_FINALE_BOARD_CONTEXT()
				DO_FINALE_BECOME_BOSS_HELP()
				PRINTLN("[finaleboarduseage] - 6")
			ENDIF
		ELSE
			CLEAR_FINALE_BECOME_BOSS_HELP()
			PRINTLN("[finaleboarduseage] - 5")
			CLEANUP_FINALE_BOARD_CONTEXT()
		ENDIF
	ELSE
		CLEAR_FINALE_BECOME_BOSS_HELP()
		//PRINTLN("[finaleboarduseage] - 1")
		CLEANUP_FINALE_BOARD_CONTEXT()
	ENDIF
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ DATA CONFIGURATION ╞═════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════════════════╛

PROC SETUP_GANG_OPS_PLANNING_BOARD_MISSION(INT iIndex, GANG_OPS_MISSION_STRAND_ENUM eStrand)

	GANG_OPS_MISSION_ENUM eMission, ePrepMission1, ePrepMission2
	INT iMissionIndex
	TEXT_LABEL_63 tlTemp
	
	eMission = GET_GANG_OPS_INSTANCED_MISSION_IN_STRAND_AT_INDEX(iIndex, eStrand)
	IF IS_GANG_OPS_MISSION_VALID(eMission)
		iMissionIndex = g_sGangOpsRootArrayPos.iPos[GET_GANG_OPS_INSTANCED_MISSION_CONST_FROM_ENUM(eMission)]
		ePrepMission1 = GET_GANG_OPS_PREP_MISSION_FROM_INSTANCED_MISSION(eMission, GANG_OPS_PREP_MISSION_1)
		ePrepMission2 = GET_GANG_OPS_PREP_MISSION_FROM_INSTANCED_MISSION(eMission, GANG_OPS_PREP_MISSION_2)
			
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - SETUP_GANG_OPS_PLANNING_BOARD_MISSION -  setting up data for mission: ", GET_GANG_OPS_MISSION_NAME_FOR_DEBUG(eMission), "; iMissionIndex = ", iMissionIndex)
		
		IF iMissionIndex != -1
			tlTemp = GET_GANG_OPS_SETUP_MISSION_TITLE(eMission)
			g_GangOpsPlanning.sStrandData.sMissionData[iIndex].sMissionTitle 													= tlTemp
			g_GangOpsPlanning.sStrandData.sMissionData[iIndex].iMissionDescriptionHash 											= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iMissionIndex].iMissionDecHash
			g_GangOpsPlanning.sStrandData.sMissionData[iIndex].iMissionState													= GET_PLAYER_GANG_OPS_MISSION_STATE(g_GangOpsPlanning.sStrandData.piLeader, eMission)
			g_GangOpsPlanning.sStrandData.sMissionData[iIndex].tlPicture														= GET_GANG_OPS_MISSION_PICTURE(eMission)
			g_GangOpsPlanning.sStrandData.sMissionData[iIndex].sMissionDescription												= GET_GANG_OPS_MISSION_DESCRIPTION(eMission)
			
			tlTemp = GET_GANG_OPS_PREP_MISSION_TITLE(ePrepMission1)
			g_GangOpsPlanning.sStrandData.sMissionData[iIndex].sPrepMission[GANG_OPS_PREP_MISSION_1].sMissionTitle				= tlTemp
			g_GangOpsPlanning.sStrandData.sMissionData[iIndex].sPrepMission[GANG_OPS_PREP_MISSION_1].iMissionState				= GET_PLAYER_GANG_OPS_MISSION_STATE(g_GangOpsPlanning.sStrandData.piLeader, ePrepMission1)
			g_GangOpsPlanning.sStrandData.sMissionData[iIndex].sPrepMission[GANG_OPS_PREP_MISSION_1].tlPicture					= GET_GANG_OPS_MISSION_PICTURE(ePrepMission1)
			g_GangOpsPlanning.sStrandData.sMissionData[iIndex].sPrepMission[GANG_OPS_PREP_MISSION_1].sMissionDescription		= GET_GANG_OPS_MISSION_DESCRIPTION(ePrepMission1)
			g_GangOpsPlanning.sStrandData.sMissionData[iIndex].sPrepMission[GANG_OPS_PREP_MISSION_1].tlEquipmentAcquired		= GET_GANG_OPS_MISSION_EQUIPMENT_ACQUIRED(ePrepMission1)
			
			IF ePrepMission2 != GANG_OPS_MISSION_INVALID
				tlTemp = GET_GANG_OPS_PREP_MISSION_TITLE(ePrepMission2)
				g_GangOpsPlanning.sStrandData.sMissionData[iIndex].sPrepMission[GANG_OPS_PREP_MISSION_2].sMissionTitle			= tlTemp
				g_GangOpsPlanning.sStrandData.sMissionData[iIndex].sPrepMission[GANG_OPS_PREP_MISSION_2].iMissionState			= GET_PLAYER_GANG_OPS_MISSION_STATE(g_GangOpsPlanning.sStrandData.piLeader, ePrepMission2)
				g_GangOpsPlanning.sStrandData.sMissionData[iIndex].sPrepMission[GANG_OPS_PREP_MISSION_2].tlPicture				= GET_GANG_OPS_MISSION_PICTURE(ePrepMission2)
				g_GangOpsPlanning.sStrandData.sMissionData[iIndex].sPrepMission[GANG_OPS_PREP_MISSION_2].sMissionDescription	= GET_GANG_OPS_MISSION_DESCRIPTION(ePrepMission2)
				g_GangOpsPlanning.sStrandData.sMissionData[iIndex].sPrepMission[GANG_OPS_PREP_MISSION_2].tlEquipmentAcquired	= GET_GANG_OPS_MISSION_EQUIPMENT_ACQUIRED(ePrepMission2)
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - SETUP_GANG_OPS_PLANNING_BOARD_MISSION - prep 2 is valid, ", GET_GANG_OPS_MISSION_NAME_FOR_DEBUG(ePrepMission2), "; ", GET_FILENAME_FOR_AUDIO_CONVERSATION(g_GangOpsPlanning.sStrandData.sMissionData[iIndex].sPrepMission[GANG_OPS_PREP_MISSION_1].sMissionDescription))
			ELSE
				g_GangOpsPlanning.sStrandData.sMissionData[iIndex].sPrepMission[GANG_OPS_PREP_MISSION_2].iMissionState			= GANG_OPS_MISSION_STATE_DONT_SHOW
				g_GangOpsPlanning.sStrandData.sMissionData[iIndex].sPrepMission[GANG_OPS_PREP_MISSION_2].tlPicture				= GET_GANG_OPS_MISSION_PICTURE(ePrepMission2)
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - SETUP_GANG_OPS_PLANNING_BOARD_MISSION -  invalid gang ops mission at index ", iIndex, " in strand ", GET_GANG_OPS_MISSION_STRAND_NAME_FOR_DEBUG(eStrand))
			ENDIF
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - SETUP_GANG_OPS_PLANNING_BOARD_MISSION -  invalid gang ops mission at index ", iIndex, " in strand ", GET_GANG_OPS_MISSION_STRAND_NAME_FOR_DEBUG(eStrand))
	ENDIF
ENDPROC

// In here we need to place the data for the replay board. 3x Strand names, strand descriptions etc
PROC FILL_GANG_OPS_IDLE_DATA()
	
	INT index = 0
	
	IF g_GangOpsIdle.sIdleData.bShowReplayBoard
		
		g_GangOpsIdle.sIdleData.iNumStrandsToShow = 3
		g_GangOpsIdle.sIdleData.tlScreenName = "HPIDLE_REPLAY"
		
		FOR index = 0 TO (g_GangOpsIdle.sIdleData.iNumStrandsToShow-1)
			g_GangOpsIdle.sIdleData.iStrandId[index] = index
			g_GangOpsIdle.sIdleData.iStrandStatus[index] = GET_PLAYER_GANG_OPS_STRAND_STATE(g_GangOpsIdle.sIdleData.piLeader, GET_GANG_OPS_MISSION_STRAND_FROM_CONST(g_GangOpsIdle.sIdleData.iStrandId[index]))
			g_GangOpsIdle.sIdleData.tlStrandName[index] = GET_GANG_OPS_MISSION_STRAND_NAME(g_GangOpsIdle.sIdleData.iStrandId[index])
			g_GangOpsIdle.sIdleData.tlStrandDesc[index] = GET_GANG_OPS_MISSION_STRAND_DESCRIPTION(g_GangOpsIdle.sIdleData.iStrandId[index])
			g_GangOpsIdle.sIdleData.iStrandCost[index] = GET_GANG_OPS_MISSION_STRAND_SETUP_COST(GET_GANG_OPS_MISSION_STRAND_FROM_CONST(g_GangOpsIdle.sIdleData.iStrandId[index]))
			g_GangOpsIdle.sIdleData.tlStrandImage[index] = GET_GANG_OPS_MISSION_STRAND_PICTURE(g_GangOpsIdle.sIdleData.iStrandId[index])
			
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - FILL_GANG_OPS_IDLE_DATA -  g_GangOpsIdle.sIdleData.iStrandId[",index,"] = ", g_GangOpsIdle.sIdleData.iStrandId[index])
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - FILL_GANG_OPS_IDLE_DATA -  g_GangOpsIdle.sIdleData.iStrandStatus[",index,"] = ", g_GangOpsIdle.sIdleData.iStrandStatus[index])
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - FILL_GANG_OPS_IDLE_DATA -  g_GangOpsIdle.sIdleData.tlStrandName[",index,"] = ", g_GangOpsIdle.sIdleData.tlStrandName[index])
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - FILL_GANG_OPS_IDLE_DATA -  g_GangOpsIdle.sIdleData.tlStrandDesc[",index,"] = ", g_GangOpsIdle.sIdleData.tlStrandDesc[index])
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - FILL_GANG_OPS_IDLE_DATA -  g_GangOpsIdle.sIdleData.iStrandCost[",index,"] = ", g_GangOpsIdle.sIdleData.iStrandCost[index])
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - FILL_GANG_OPS_IDLE_DATA -  g_GangOpsIdle.sIdleData.tlStrandImage[",index,"] = ", g_GangOpsIdle.sIdleData.tlStrandImage[index])
		ENDFOR
	ELSE
		g_GangOpsIdle.sIdleData.iNumStrandsToShow = 1
		g_GangOpsIdle.sIdleData.tlScreenName = "HPIDLE_PLAY"
		
		g_GangOpsIdle.sIdleData.iStrandId[index] = GET_PLAYER_NEXT_AVAILABLE_GANG_OPS_MISSION_STRAND_AS_INT(g_GangOpsIdle.sIdleData.piLeader)
		g_GangOpsIdle.sIdleData.iStrandStatus[index] = GET_PLAYER_GANG_OPS_STRAND_STATE(g_GangOpsIdle.sIdleData.piLeader, GET_GANG_OPS_MISSION_STRAND_FROM_CONST(g_GangOpsIdle.sIdleData.iStrandId[index]))
		g_GangOpsIdle.sIdleData.tlStrandName[index] = GET_GANG_OPS_MISSION_STRAND_NAME(g_GangOpsIdle.sIdleData.iStrandId[index])
		g_GangOpsIdle.sIdleData.tlStrandDesc[index] = GET_GANG_OPS_MISSION_STRAND_DESCRIPTION(g_GangOpsIdle.sIdleData.iStrandId[index])
		g_GangOpsIdle.sIdleData.iStrandCost[index] = GET_GANG_OPS_MISSION_STRAND_SETUP_COST(GET_GANG_OPS_MISSION_STRAND_FROM_CONST(g_GangOpsIdle.sIdleData.iStrandId[index]))
		g_GangOpsIdle.sIdleData.tlStrandImage[index] = GET_GANG_OPS_MISSION_STRAND_PICTURE(g_GangOpsIdle.sIdleData.iStrandId[index], TRUE)
		
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - FILL_GANG_OPS_IDLE_DATA -  g_GangOpsIdle.sIdleData.iStrandId[",index,"] = ", g_GangOpsIdle.sIdleData.iStrandId[index])
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - FILL_GANG_OPS_IDLE_DATA -  g_GangOpsIdle.sIdleData.iStrandStatus[",index,"] = ", g_GangOpsIdle.sIdleData.iStrandStatus[index])
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - FILL_GANG_OPS_IDLE_DATA -  g_GangOpsIdle.sIdleData.tlStrandName[",index,"] = ", g_GangOpsIdle.sIdleData.tlStrandName[index])
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - FILL_GANG_OPS_IDLE_DATA -  g_GangOpsIdle.sIdleData.tlStrandDesc[",index,"] = ", g_GangOpsIdle.sIdleData.tlStrandDesc[index])
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - FILL_GANG_OPS_IDLE_DATA -  g_GangOpsIdle.sIdleData.iStrandCost[",index,"] = ", g_GangOpsIdle.sIdleData.iStrandCost[index])
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - FILL_GANG_OPS_IDLE_DATA -  g_GangOpsIdle.sIdleData.tlStrandImage[",index,"] = ", g_GangOpsIdle.sIdleData.tlStrandImage[index])
		
	ENDIF
	
ENDPROC

PROC FILL_GANG_OPS_PLANNING_DATA()

	INT iIndex
	
	GANG_OPS_MISSION_STRAND_ENUM eStrand = GET_GANG_OPS_MISSION_STRAND_FROM_CONST(g_GangOpsPlanning.sStrandData.iStrandId)

	SWITCH (eStrand)
		
		/////////////////////////////
		//
		// IAA STRAND
		//
		// Instanced missions:
		//		- Morgue
		//		- DeLorean
		//		- Server Farm
		//	
		// Freemode prep missions:
		//		- Paramedic
		//		- Car Collector
		//		- Under Control
		//
		/////////////////////////////
		
		CASE GANG_OPS_MISSION_STRAND_IAA
			
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - FILL_GANG_OPS_PLANNING_DATA -  setting up data for GANG_OPS_MISSION_STRAND_IAA strand")
			
			g_GangOpsPlanning.sStrandData.iStrandCost 		= GET_GANG_OPS_MISSION_STRAND_SETUP_COST(eStrand)
			g_GangOpsPlanning.sStrandData.iStrandDesc 		= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[0].iMissionDecHash
			g_GangOpsPlanning.sStrandData.tlStrandName 		= GET_GANG_OPS_MISSION_STRAND_NAME(g_GangOpsPlanning.sStrandData.iStrandId)
			g_GangOpsPlanning.sStrandData.tlStrandDesc		= GET_GANG_OPS_MISSION_STRAND_DESCRIPTION(g_GangOpsPlanning.sStrandData.iStrandId)
			g_GangOpsPlanning.sStrandData.iTotalMissions 	= 3			// The number of instanced missions, freemode prep have post-it
						
			FOR iIndex = 0 TO (g_GangOpsPlanning.sStrandData.iTotalMissions-1)
				SETUP_GANG_OPS_PLANNING_BOARD_MISSION(iIndex, eStrand)
			ENDFOR
			
		BREAK
		
		/////////////////////////////
		//
		// SUBMARINE STRAND
		//
		// Instanced missions:
		//		- Steal Osprey
		//		- Foundry
		//		- Riot Van
		//		- Sub Car
		//	
		// Freemode prep missions:
		//		- Flare Up
		//		- Forced Entry
		//		- Mob Mentality
		//		- Auto Salvage
		//		- Aqualungs
		//
		/////////////////////////////
		
		CASE GANG_OPS_MISSION_STRAND_SUBMARINE
		
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - FILL_GANG_OPS_PLANNING_DATA -  setting up data for GANG_OPS_MISSION_STRAND_SUBMARINE strand")
		
			g_GangOpsPlanning.sStrandData.iStrandCost 		= GET_GANG_OPS_MISSION_STRAND_SETUP_COST(eStrand)
			g_GangOpsPlanning.sStrandData.iStrandDesc 		= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[0].iMissionDecHash
			g_GangOpsPlanning.sStrandData.tlStrandName 		= GET_GANG_OPS_MISSION_STRAND_NAME(g_GangOpsPlanning.sStrandData.iStrandId)
			g_GangOpsPlanning.sStrandData.tlStrandDesc		= GET_GANG_OPS_MISSION_STRAND_DESCRIPTION(g_GangOpsPlanning.sStrandData.iStrandId)
			g_GangOpsPlanning.sStrandData.iTotalMissions 	= 4			// The number of instanced missions, freemode prep have post-it
			
			FOR iIndex = 0 TO (g_GangOpsPlanning.sStrandData.iTotalMissions-1)
				SETUP_GANG_OPS_PLANNING_BOARD_MISSION(iIndex, eStrand)
			ENDFOR
		
		BREAK
		
		/////////////////////////////
		//
		// MISSILE SILO STRAND
		//
		// Instanced missions:
		//		- Predator
		//		- BM Launcher
		//		- BC Custom
		//		- Stealth Tanks
		//		- Spy Plane
		//	
		// Freemode prep missions:
		//		- Dead Drop
		//		- Amateur Photography
		//		- Gone Ballistic
		//		- Daylight Robbery
		//		- Burglary Job
		//		- Flight Recorder
		//
		/////////////////////////////
		
		CASE GANG_OPS_MISSION_STRAND_MISSILE_SILO
		
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - FILL_GANG_OPS_PLANNING_DATA -  setting up data for GANG_OPS_MISSION_STRAND_MISSILE_SILO strand")
		
			g_GangOpsPlanning.sStrandData.iStrandCost 		= GET_GANG_OPS_MISSION_STRAND_SETUP_COST(eStrand)
			g_GangOpsPlanning.sStrandData.iStrandDesc 		= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[0].iMissionDecHash
			g_GangOpsPlanning.sStrandData.tlStrandName 		= GET_GANG_OPS_MISSION_STRAND_NAME(g_GangOpsPlanning.sStrandData.iStrandId)
			g_GangOpsPlanning.sStrandData.tlStrandDesc		= GET_GANG_OPS_MISSION_STRAND_DESCRIPTION(g_GangOpsPlanning.sStrandData.iStrandId)
			g_GangOpsPlanning.sStrandData.iTotalMissions 	= 5			// The number of instanced missions, freemode prep have post-it
			
			FOR iIndex = 0 TO (g_GangOpsPlanning.sStrandData.iTotalMissions-1)
				SETUP_GANG_OPS_PLANNING_BOARD_MISSION(iIndex, eStrand)
			ENDFOR
		
		BREAK
		
	ENDSWITCH
	
	IF GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.bInitialisedBoard = FALSE
		GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.bInitialisedBoard = TRUE
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] FILL_GANG_OPS_PLANNING_DATA - bInitialisedBoard = TRUE")
	ENDIF

ENDPROC

PROC FILL_GANG_OPS_FINALE_DATA()

	// Process mission details.
	// Get the coords for TEAM 0 start location, and place them on the map.
	INT index, iLoop
	TEXT_LABEL_63 tlStep
	
	FOR index = 0 TO (MAX_GANG_OPS_PLAYERS-1)
		g_GangOpsFinale.sFinaleData.iSelectedRole[index] = GANG_OPS_INVALID
		g_GangOpsFinale.sFinaleData.iRewardPercentages[index] = 0
	ENDFOR
	
	g_GangOpsFinale.sFinaleData.iTotalPay = GET_CASH_VALUE_FROM_CREATOR(g_FMMC_STRUCT.iCashReward, TRUE)
	
	// Make sure we include difficulty multipliers
	IF NETWORK_IS_ACTIVITY_SESSION()
		FLOAT fMultiplier = 1.0
		SWITCH g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_DIFFICULTY]
			CASE DIFF_EASY			fMultiplier = 1.0												BREAK
			CASE DIFF_NORMAL		fMultiplier = g_sMPTunables.fGangops_difficulty_normal			BREAK
			CASE DIFF_HARD			fMultiplier = g_sMPTunables.fGangops_difficulty_hard			BREAK
		ENDSWITCH
		
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] FILL_GANG_OPS_FINALE_DATA - difficulty = ", g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_DIFFICULTY])
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] FILL_GANG_OPS_FINALE_DATA - multiplier = ", fMultiplier)
		
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] FILL_GANG_OPS_FINALE_DATA - iTotalPay = ", g_GangOpsFinale.sFinaleData.iTotalPay," before multipliers")
		g_GangOpsFinale.sFinaleData.iTotalPay = ROUND(g_GangOpsFinale.sFinaleData.iTotalPay * fMultiplier)
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] FILL_GANG_OPS_FINALE_DATA - iTotalPay = ", g_GangOpsFinale.sFinaleData.iTotalPay," after multipliers")
	ENDIF
	
	IF g_GangOpsFinale.sFinaleData.iTotalPay > g_sMPTunables.IH2_PLANNING_SANITY_CEILING
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] FILL_GANG_OPS_FINALE_DATA - WARNING - iTotalPay (",g_GangOpsFinale.sFinaleData.iTotalPay,") > IH2_PLANNING_SANITY_CEILING (",g_sMPTunables.IH2_PLANNING_SANITY_CEILING,")")
		g_GangOpsFinale.sFinaleData.iTotalPay = g_sMPTunables.IH2_PLANNING_SANITY_CEILING
	ENDIF
	
	GANG_OPS_MISSION_STRAND_ENUM eStrand = GET_GANG_OPS_MISSION_STRAND_FROM_CONST(g_GangOpsFinale.sFinaleData.iStrandId)

	SWITCH (eStrand)
	
		/////////////////////////////
		//
		// IAA STRAND
		//
		// Instanced missions:
		//		- Morgue
		//		- DeLorean
		//		- Server Farm
		//	
		// Freemode prep missions:
		//		- Paramedic
		//		- Car Collector
		//		- Under Control
		//
		/////////////////////////////
		
		CASE GANG_OPS_MISSION_STRAND_IAA
			
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - FILL_GANG_OPS_FINALE_DATA -  setting up finale data for GANG_OPS_MISSION_STRAND_IAA strand")
			
			g_GangOpsFinale.sFinaleData.tlStrandName 		= GET_FILENAME_FOR_AUDIO_CONVERSATION("HPSTRAND_IAA")
			
			FOR iLoop = 0 TO (MAX_GANG_OPS_FINALE_STEPS-1)
				tlStep = "HP_IAA_STEP_"
				tlStep += iLoop
				IF DOES_TEXT_LABEL_EXIST(tlStep)
					g_GangOpsFinale.sFinaleData.tlFinaleSteps[iLoop] = tlStep
				ENDIF
			ENDFOR
		
		BREAK
		
		/////////////////////////////
		//
		// SUBMARINE STRAND
		//
		// Instanced missions:
		//		- Steal Osprey
		//		- Foundry
		//		- Riot Van
		//		- Sub Car
		//	
		// Freemode prep missions:
		//		- Flare Up
		//		- Forced Entry
		//		- Mob Mentality
		//		- Auto Salvage
		//		- Aqualungs
		//
		/////////////////////////////
		
		CASE GANG_OPS_MISSION_STRAND_SUBMARINE
		
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - FILL_GANG_OPS_FINALE_DATA -  setting up finale data for GANG_OPS_MISSION_STRAND_SUBMARINE strand")
		
			g_GangOpsFinale.sFinaleData.tlStrandName 		= GET_FILENAME_FOR_AUDIO_CONVERSATION("HPSTRAND_SUB")

			g_GangOpsFinale.sFinaleData.iTeamStepsBeingDisplayed = GANG_OPS_TEAM_0
			FOR iLoop = 0 TO (MAX_GANG_OPS_FINALE_STEPS-1)
				tlStep = "HP_SUB_STEP_A"
				tlStep += iLoop
				IF DOES_TEXT_LABEL_EXIST(tlStep)
					g_GangOpsFinale.sFinaleData.tlFinaleSteps[iLoop] = tlStep
				ENDIF
			ENDFOR

		BREAK
		
		/////////////////////////////
		//
		// MISSILE SILO STRAND
		//
		// Instanced missions:
		//		- Predator
		//		- BM Launcher
		//		- BC Custom
		//		- Stealth Tanks
		//		- Spy Plane
		//	
		// Freemode prep missions:
		//		- Dead Drop
		//		- Amateur Photography
		//		- Gone Ballistic
		//		- Daylight Robbery
		//		- Burglary Job
		//		- Flight Recorder
		//
		/////////////////////////////
		
		CASE GANG_OPS_MISSION_STRAND_MISSILE_SILO
		
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - FILL_GANG_OPS_FINALE_DATA -  setting up finale data for GANG_OPS_MISSION_STRAND_MISSILE_SILO strand")
		
			g_GangOpsFinale.sFinaleData.tlStrandName 		= GET_FILENAME_FOR_AUDIO_CONVERSATION("HPSTRAND_MSIL")

			FOR iLoop = 0 TO (MAX_GANG_OPS_FINALE_STEPS-1)
				tlStep = "HP_MSI_STEP_"
				tlStep += iLoop
				IF DOES_TEXT_LABEL_EXIST(tlStep)
					g_GangOpsFinale.sFinaleData.tlFinaleSteps[iLoop] = tlStep
				ENDIF
			ENDFOR

		BREAK
	
	ENDSWITCH
	
	IF GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.bInitialisedBoard = FALSE
		GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.bInitialisedBoard = TRUE
		START_NET_TIMER(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.stFinaleLaunchTimer)
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] FILL_GANG_OPS_FINALE_DATA - bInitialisedBoard = TRUE")
	ENDIF
	
ENDPROC

//╒════════════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡    MAIN FUNCTIONS    ╞═════════════════════════════════╡
//╘════════════════════════════════════════════════════════════════════════════════════════╛

PROC MAINTAIN_GANG_OPS_IDLE_BOARD()
	MAINTAIN_GANG_OPS_IDLE(g_GangOpsIdle)
	IF IS_BIT_SET(g_GangOpsIdle.iIdleBitSet, GANG_OPS_BITSET_SELECTED_HEIST)
		eStage = STAGE_PROCESS_HEIST_SELECTED
	ENDIF
ENDPROC

PROC MAINTAIN_GANG_OPS_PLANNING_BOARD()
	MAINTAIN_GANG_OPS_PLANNING(g_GangOpsPlanning)
	IF IS_BIT_SET(g_GangOpsPlanning.iPlanningBitSet, GANG_OPS_BITSET_LAUNCH_MISSION)
		eStage = STAGE_PROCESS_LAUNCH
	ENDIF
ENDPROC

PROC MAINTAIN_GANG_OPS_FINALE_BOARD()
	MAINTAIN_GANG_OPS_FINALE(g_GangOpsFinale)
ENDPROC

PROC MAINTAIN_MISSION_LAUNCHING()
	IF IS_BIT_SET(g_GangOpsPlanning.iPlanningBitSet, GANG_OPS_BITSET_LAUNCH_MISSION)
		IF NOT IS_BIT_SET(g_GangOpsPlanning.iPlanningBitSet, GANG_OPS_BITSET_MISSION_LAUNCHED)
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [MISSION_LAUNCH] PRIVATE_MAINTAIN_LAUNCHING_MISSION - iSelectionID = ", g_GangOpsPlanning.sCursorData.iHighlight)
			
			GANG_OPS_MISSION_ENUM eMissionToLaunch = GET_GANG_OPS_MISSION_FROM_SELECTION_ID(g_GangOpsPlanning.sCursorData.iHighlight, g_GangOpsPlanning.sStrandData.iStrandId)

			IF eMissionToLaunch != GANG_OPS_MISSION_INVALID

				IF NOT IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_OUT(500)
				ELSE
//					IF g_GangOpsPlanning.sStateData.eState < GOBS_CLEANUP 
//						PUBLIC_PROGRESS_GANG_OPS_FSM(g_GangOpsPlanning.sStateData)
//					ELSE
//						IF g_GangOpsPlanning.sStateData.eState = GOBS_FINISHED

					IF g_GangOpsPlanning.sStateData.eState = GOBS_INUSE
						PRIVATE_TOGGLE_GANG_OPS_INTERACTION(g_GangOpsPlanning, FALSE)
					ELSE
						IF g_GangOpsPlanning.sStateData.eState = GOBS_DISPLAYING
							IF IS_GANG_OPS_MISSION_AN_INSTANCED_MISSION(eMissionToLaunch)
								CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [MISSION_LAUNCH] PRIVATE_MAINTAIN_LAUNCHING_MISSION - mission is ", GET_GANG_OPS_MISSION_NAME_FOR_DEBUG(eMissionToLaunch),", launching with SVM_FLOW_LAUNCH_NEXT_AVAILABLE_MISSION(iMission, ciFLOW_MISSION_TYPE_GANGOPS)")
								SVM_FLOW_LAUNCH_NEXT_AVAILABLE_MISSION(ciCoronaCam, GET_GANG_OPS_INSTANCED_MISSION_CONST_FROM_ENUM(eMissionToLaunch), ciFLOW_MISSION_TYPE_GANGOPS)
								g_SimpleInteriorData.bAcceptedSimpleIntSameSessionHeistInv = TRUE
								SET_BIT(g_GangOpsPlanning.iPlanningBitSet, GANG_OPS_BITSET_DO_TOGGLE_RENDERPHASES)
							ELSE
								CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [MISSION_LAUNCH] PRIVATE_MAINTAIN_LAUNCHING_MISSION - mission is ", GET_GANG_OPS_MISSION_NAME_FOR_DEBUG(eMissionToLaunch),", launching with GB_BOSS_REQUEST_FM_GANGOPS_MISSION_LAUNCH_FROM_SERVER(FMMC_TYPE_FM_GANGOPS), variation: ", GET_GANGOPS_VARIATION_NAME_FOR_DEBUG(GET_GANG_OPS_VARIATION_FROM_GANG_OPS_FREEMODE_MISSION(eMissionToLaunch)))
								GB_SET_PLAYER_GANG_BOSS_MISSION_VARIATION(ENUM_TO_INT(GET_GANG_OPS_VARIATION_FROM_GANG_OPS_FREEMODE_MISSION(eMissionToLaunch)))
								GB_BOSS_REQUEST_FM_GANGOPS_MISSION_LAUNCH_FROM_SERVER(FMMC_TYPE_FM_GANGOPS)
								WARP_ALL_PLAYERS_IN_SIMPLE_INTERIOR_OUTSIDE()
								CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [MISSION_LAUNCH] PRIVATE_MAINTAIN_LAUNCHING_MISSION - mission is ", GET_GANG_OPS_MISSION_NAME_FOR_DEBUG(eMissionToLaunch),", calling WARP_ALL_PLAYERS_IN_SIMPLE_INTERIOR_OUTSIDE()")
							ENDIF
							
							SET_BIT(g_GangOpsPlanning.iPlanningBitSet, GANG_OPS_BITSET_MISSION_LAUNCHED)
						ENDIF
					ENDIF
				ENDIF
			
			ELSE
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [MISSION_LAUNCH] PRIVATE_MAINTAIN_LAUNCHING_MISSION - mission is GANG_OPS_MISSION_INVALID, clearing bit and not launching anything")
				CLEAR_BIT(g_GangOpsPlanning.iPlanningBitSet, GANG_OPS_BITSET_LAUNCH_MISSION)
			ENDIF
		ELIF IS_BIT_SET(g_GangOpsPlanning.iPlanningBitSet, GANG_OPS_BITSET_DO_TOGGLE_RENDERPHASES)
			IF NOT IS_BIT_SET(g_GangOpsPlanning.iPlanningBitSet, GANG_OPS_BITSET_TOGGLED_RENDERPHASES)
				SET_BIT(g_GangOpsPlanning.iPlanningBitSet, GANG_OPS_BITSET_TOGGLED_RENDERPHASES)
				#IF IS_DEBUG_BUILD 
				IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_toggleHeistRenderPhases")
				#ENDIF
					TOGGLE_RENDERPHASES(FALSE) 
					TOGGLE_RENDERPHASES(FALSE)
				#IF IS_DEBUG_BUILD 
				ENDIF
				#ENDIF
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [MISSION_LAUNCH] PRIVATE_MAINTAIN_LAUNCHING_MISSION -SET_BIT(g_GangOpsPlanning.iPlanningBitSet, GANG_OPS_BITSET_TOGGLED_RENDERPHASES)")
			ENDIF
			CLEAR_BIT(g_GangOpsPlanning.iPlanningBitSet, GANG_OPS_BITSET_DO_TOGGLE_RENDERPHASES)
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [MISSION_LAUNCH] PRIVATE_MAINTAIN_LAUNCHING_MISSION -CLEAR_BIT(g_GangOpsPlanning.iPlanningBitSet, GANG_OPS_BITSET_DO_TOGGLE_RENDERPHASES)")
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_LAUNCH_FINALE_MISSION()
	IF IS_BIT_SET(g_GangOpsFinale.iFinaleBitSet, GANG_OPS_BITSET_LAUNCH_MISSION)
	AND NOT IS_BIT_SET(g_GangOpsFinale.iFinaleBitSet, GANG_OPS_BITSET_MISSION_LAUNCHED)
		IF g_GangOpsFinale.sFinaleData.piLeader = PLAYER_ID()
//		AND g_GangOpsFinale.sStateData.eState = GOBS_FINISHED
			IF AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()
			OR HAS_ON_CALL_TRANSITION_SESSION_BEEN_SET_UP()
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [MISSION_LAUNCH] - MAINTAIN_LAUNCH_FINALE_MISSION - I am on call, cleaning up on call now... SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW")
				SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW()
			ELSE
				IF NOT IS_BIT_SET(g_GangOpsFinale.iFinaleBitSet, GANG_OPS_BITSET_MISSION_LAUNCHED)
					GANG_OPS_MISSION_ENUM eFinale = GET_GANG_OPS_STRAND_FINALE(GET_PLAYER_CURRENT_GANG_OPS_MISSION_STRAND(PLAYER_ID()))
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [MISSION_LAUNCH] - MAINTAIN_LAUNCH_FINALE_MISSION - Launching finale: ", GET_GANG_OPS_MISSION_NAME_FOR_DEBUG(eFinale))
					SVM_FLOW_LAUNCH_NEXT_AVAILABLE_MISSION(ciCoronaCam, GET_GANG_OPS_INSTANCED_MISSION_CONST_FROM_ENUM(eFinale), ciFLOW_MISSION_TYPE_GANGOPS)
					g_SimpleInteriorData.bAcceptedSimpleIntSameSessionHeistInv = TRUE
					SET_BIT(g_GangOpsFinale.iFinaleBitSet, GANG_OPS_BITSET_MISSION_LAUNCHED)
					SET_BIT(g_GangOpsFinale.iFinaleBitSet, GANG_OPS_BITSET_DO_TOGGLE_RENDERPHASES)
				ENDIF
			ENDIF
		ENDIF
	ELIF IS_BIT_SET(g_GangOpsFinale.iFinaleBitSet, GANG_OPS_BITSET_DO_TOGGLE_RENDERPHASES)
		IF NOT IS_BIT_SET(g_GangOpsFinale.iFinaleBitSet, GANG_OPS_BITSET_TOGGLED_RENDERPHASES)
			SET_BIT(g_GangOpsFinale.iFinaleBitSet, GANG_OPS_BITSET_TOGGLED_RENDERPHASES)
			#IF IS_DEBUG_BUILD 
			IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_toggleHeistRenderPhases")
			#ENDIF
			TOGGLE_RENDERPHASES(FALSE) 
			TOGGLE_RENDERPHASES(FALSE)		
			#IF IS_DEBUG_BUILD 
			ENDIF
			#ENDIF
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [MISSION_LAUNCH] PRIVATE_MAINTAIN_LAUNCHING_MISSION -SET_BIT(g_GangOpsPlanning.iPlanningBitSet, GANG_OPS_BITSET_TOGGLED_RENDERPHASES)")
		ENDIF
		CLEAR_BIT(g_GangOpsFinale.iFinaleBitSet, GANG_OPS_BITSET_DO_TOGGLE_RENDERPHASES)
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [MISSION_LAUNCH] PRIVATE_MAINTAIN_LAUNCHING_MISSION -CLEAR_BIT(g_GangOpsPlanning.iPlanningBitSet, GANG_OPS_BITSET_DO_TOGGLE_RENDERPHASES)")
	ENDIF
ENDPROC

PROC MAINTAIN_HEIST_SELECTED()
	IF IS_BIT_SET(g_GangOpsIdle.iIdleBitSet, GANG_OPS_BITSET_SELECTED_HEIST)
		IF g_GangOpsIdle.sStateData.eState < GOBS_CLEANUP 
			PUBLIC_PROGRESS_GANG_OPS_FSM(g_GangOpsIdle.sStateData)
		ELSE
			IF g_GangOpsIdle.sStateData.eState = GOBS_FINISHED
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] MAINTAIN_HEIST_SELECTED - Selected heist and cleaned up the board, time to launch the planning board")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC INITIALISE()

	PLAYER_INDEX piLeader = INVALID_PLAYER_INDEX()
	
	BOOL bPrepareFinale
	BOOL bPrepareIdle
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		piLeader = g_GangOpsFinale.sFinaleData.piLeader
		IF piLeader != INVALID_PLAYER_INDEX()
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [GB_GANG_OPS_PLANNING] - INITIALISE - piLeader = g_GangOpsFinale.sFinaleData.piLeader , ", GET_PLAYER_NAME(piLeader))
		ELSE
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [GB_GANG_OPS_PLANNING] - INITIALISE - piLeader = g_GangOpsFinale.sFinaleData.piLeader , INVALID_PLAYER_INDEX()")
		ENDIF
	ELSE
		piLeader = GET_OWNER_OF_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN()
		#IF IS_DEBUG_BUILD
		IF piLeader != INVALID_PLAYER_INDEX()
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [GB_GANG_OPS_PLANNING] - INITIALISE - piLeader = GET_OWNER_OF_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN() , ", GET_PLAYER_NAME(piLeader))
		ELSE
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [GB_GANG_OPS_PLANNING] - INITIALISE - piLeader = GET_OWNER_OF_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN() , INVALID_PLAYER_INDEX()")
		ENDIF
		#ENDIF
	ENDIF

	IF piLeader != INVALID_PLAYER_INDEX()
		#IF IS_DEBUG_BUILD
		iRootContentIDHashCache = g_FMMC_STRUCT.iRootContentIDHash
		bFinaleDebugCache = FALSE
		iGangOpsPlanningBoardActive = ciDEBUG_GANGOPS_PLANNINGBOARD_PREP
		#ENDIF
		
		IF piLeader = PLAYER_ID()
			INT iStat = GET_MP_INT_CHARACTER_STAT(MP_STAT_GANGOPS_HEIST_STATUS)
			IF NOT IS_BIT_SET(iStat, GANG_OPS_BD_HEIST_STATUS_BITSET_SHOWN_FIRST_TIME_IN_ROOM_HELP)
				IF GET_PLAYER_CURRENT_GANG_OPS_MISSION_STRAND(PLAYER_ID()) = GANG_OPS_MISSION_STRAND_INVALID
					IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmGbHelpBitSet4, BI_FM_GANG_BOSS_HELP_4_PLANNING_SCREEN_EXPLAIN_HELP_DONE_THIS_BOOT)
						PRINT_HELP("HP_EXPLAIN")
						SET_BIT(MPGlobalsAmbience.iFmGbHelpBitSet4, BI_FM_GANG_BOSS_HELP_4_PLANNING_SCREEN_EXPLAIN_HELP_DONE_THIS_BOOT)
						CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [GB_GANG_OPS_PLANNING] - INITIALISE - Explain planning screen help done this boot")
					ENDIF
				ELSE
					SET_BIT(iStat, GANG_OPS_BD_HEIST_STATUS_BITSET_SHOWN_FIRST_TIME_IN_ROOM_HELP)
					SET_MP_INT_CHARACTER_STAT(MP_STAT_GANGOPS_HEIST_STATUS, iStat)
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [GB_GANG_OPS_PLANNING] - INITIALISE - Player has an active strand, dont need to explain the planning screen any more")
				ENDIF
			ENDIF
		ENDIF
		
		
		IF GANGOPS_FLOW_IS_MISSION_A_GANGOPS_FINALE_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
		AND NETWORK_IS_ACTIVITY_SESSION()
			bPrepareFinale = TRUE
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [GB_GANG_OPS_PLANNING] - INITIALISE - In an activity session, we need to set up the finale board")
		ELSE
			IF GET_PLAYER_CURRENT_GANG_OPS_MISSION_STRAND(piLeader, TRUE) = GANG_OPS_MISSION_STRAND_INVALID
				bPrepareIdle = TRUE
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [GB_GANG_OPS_PLANNING] - INITIALISE - Leader doesnt have a current strand, we need to set up the idle board")
			ELSE
				IF IS_GANG_OPS_MISSION_AVAILABLE(piLeader, GANG_OPS_MISSION_INSTANCED_IAABASE_FINALE)
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [GB_GANG_OPS_PLANNING] - INITIALISE - IAA Base finale is available, we need to set up the finale board")
					bPrepareFinale = TRUE
				ENDIF
				IF IS_GANG_OPS_MISSION_AVAILABLE(piLeader, GANG_OPS_MISSION_INSTANCED_SUBMARINE_FINALE)
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [GB_GANG_OPS_PLANNING] - INITIALISE - Submarine finale is available, we need to set up the finale board")
					bPrepareFinale = TRUE
				ENDIF
				IF IS_GANG_OPS_MISSION_AVAILABLE(piLeader, GANG_OPS_MISSION_INSTANCED_MISSILE_SILO_FINALE)
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [GB_GANG_OPS_PLANNING] - INITIALISE - Missile Silo finale is available, we need to set up the finale board")
					bPrepareFinale = TRUE
				ENDIF
				IF IS_GANG_OPS_MISSION_AVAILABLE(piLeader, GANG_OPS_MISSION_INSTANCED_MISSILE_SILO_FINALE_P2)
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [GB_GANG_OPS_PLANNING] - INITIALISE - Missile Silo finale P2 is available, we need to set up the finale board")
					bPrepareFinale = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF bPrepareIdle
		
			#IF IS_DEBUG_BUILD
			bFinaleDebugCache = TRUE
			IF iGangOpsPlanningBoardActive != ciDEBUG_GANGOPS_PLANNINGBOARD_IDLE
				iGangOpsPlanningBoardActive = ciDEBUG_GANGOPS_PLANNINGBOARD_IDLE
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [GB_GANG_OPS_PLANNING] - iGangOpsPlanningBoardActive = ciDEBUG_GANGOPS_PLANNINGBOARD_IDLE")
			ENDIF
			#ENDIF
			IF NOT IS_BIT_SET(g_GangOpsIdle.iIdleBitSet, GANG_OPS_BITSET_BOARD_SETUP)
				STRING sRenderTarget = "Prop_x17DLC_Monitor_Wall_01a"
				IF NOT IS_NAMED_RENDERTARGET_REGISTERED(sRenderTarget)
					REGISTER_NAMED_RENDERTARGET(sRenderTarget)
					LINK_NAMED_RENDERTARGET(XM_PROP_X17DLC_MONITOR_WALL_01A)
					g_GangOpsIdle.sScaleformData.iRenderTargetID = GET_NAMED_RENDERTARGET_RENDER_ID(sRenderTarget)
				ELSE
					g_GangOpsIdle.sIdleData.piLeader = piLeader
					
					IF IS_ANY_PLAYER_USING_DEFUNCT_BASE_PLANNING_BOARD()
					OR IS_PLAYER_IN_CORONA()
					OR g_SimpleInteriorData.bAllowInstanceOverrideForCrossSession
						g_GangOpsIdle.sScaleformData.bSkipLoading = TRUE
					ENDIF
										
					g_GangOpsIdle.sPositionData.vBoardPosition = GET_SCREEN_COORDS()
					
					g_GangOpsIdle.sCameraData.ePositionId = GOCAM_MAIN
					g_GangOpsIdle.sCameraData.vCamInitialOffset = << 1.0, -2.5, -1.2 >>
					g_GangOpsIdle.sCameraData.vCamMapOffset = << -0.5, -2.5, -1.2 >>
					g_GangOpsIdle.sCameraData.vCamInitialOffsetRot = << 0.0, 0.0, 0.0 >>
					g_GangOpsIdle.sCameraData.vCamMapOffsetRot = << 0.0, 0.0, 0.0 >>
					
					g_GangOpsIdle.sCameraData.vBaseRotation.X = 0.0
					g_GangOpsIdle.sCameraData.vBaseRotation.Y = 0.0
					g_GangOpsIdle.sCameraData.vBaseRotation.Z = -180.0
					
					// If the leader doesnt have an active heist but has already completed them all, we need to show the replay board.
					IF HAS_PLAYER_COMPLETED_ALL_GANG_OPS_HEIST_STRANDS(g_GangOpsIdle.sIdleData.piLeader)
						g_GangOpsIdle.sIdleData.bShowReplayBoard = TRUE
					ENDIF
					
					FILL_GANG_OPS_IDLE_DATA()
					
					START_NET_TIMER(stArbitraryDelayTimer)
					
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [GB_GANG_OPS_PLANNING] - INITIALISE - Start Gang Ops Planning Board")
					SET_BIT(g_GangOpsIdle.iIdleBitSet, GANG_OPS_BITSET_BOARD_SETUP)
					eStage = STAGE_PROCESS_IDLE
				ENDIF
			ENDIF
		
		
		ELIF bPrepareFinale
		
			#IF IS_DEBUG_BUILD
			bFinaleDebugCache = TRUE
			IF iGangOpsPlanningBoardActive != ciDEBUG_GANGOPS_PLANNINGBOARD_FINALE
				iGangOpsPlanningBoardActive = ciDEBUG_GANGOPS_PLANNINGBOARD_FINALE
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [GB_GANG_OPS_PLANNING] - iGangOpsPlanningBoardActive = ciDEBUG_GANGOPS_PLANNINGBOARD_FINALE")
			ENDIF
			#ENDIF
			IF NOT IS_BIT_SET(g_GangOpsFinale.iFinaleBitSet, GANG_OPS_BITSET_BOARD_SETUP)
				STRING sRenderTarget = "Prop_x17DLC_Monitor_Wall_01a"
				IF NOT IS_NAMED_RENDERTARGET_REGISTERED(sRenderTarget)
					REGISTER_NAMED_RENDERTARGET(sRenderTarget)
					LINK_NAMED_RENDERTARGET(XM_PROP_X17DLC_MONITOR_WALL_01A)
					g_GangOpsFinale.sScaleformData.iRenderTargetID = GET_NAMED_RENDERTARGET_RENDER_ID(sRenderTarget)
				ELSE
					IF g_GangOpsFinale.sFinaleData.piLeader != piLeader
						g_GangOpsFinale.sFinaleData.piLeader = piLeader
						CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [GB_GANG_OPS_PLANNING] - g_GangOpsFinale.sFinaleData.piLeader = ", GET_PLAYER_NAME(g_GangOpsFinale.sFinaleData.piLeader))
					ENDIF
				
					g_GangOpsFinale.sFinaleData.iStrandId = GET_PLAYER_CURRENT_GANG_OPS_MISSION_STRAND_AS_INT(g_GangOpsFinale.sFinaleData.piLeader)
					
					IF IS_ANY_PLAYER_USING_DEFUNCT_BASE_PLANNING_BOARD()
					OR IS_PLAYER_IN_CORONA()
					OR g_SimpleInteriorData.bAllowInstanceOverrideForCrossSession
						g_GangOpsFinale.sScaleformData.bSkipLoading = TRUE
					ENDIF
					
					g_GangOpsFinale.sPositionData.vBoardPosition = GET_SCREEN_COORDS()
					
					g_GangOpsFinale.sCameraData.ePositionId = GOCAM_MAIN
					g_GangOpsFinale.sCameraData.vCamInitialOffset = << 1.0, -2.5, -1.2 >>
					g_GangOpsFinale.sCameraData.vCamMapOffset = << -0.5, -2.5, -1.2 >>
					g_GangOpsFinale.sCameraData.vCamInitialOffsetRot = << 0.0, 0.0, 0.0 >>
					g_GangOpsFinale.sCameraData.vCamMapOffsetRot = << 0.0, 0.0, 0.0 >>
					
					g_GangOpsFinale.sCameraData.vBaseRotation.X = 0.0
					g_GangOpsFinale.sCameraData.vBaseRotation.Y = 0.0
					g_GangOpsFinale.sCameraData.vBaseRotation.Z = -180.0
					
					FILL_GANG_OPS_FINALE_DATA()
					
					START_NET_TIMER(stArbitraryDelayTimer)
					
					IF g_bGangOpsPlanningBlockHeadshotUpdates = FALSE
						g_bGangOpsPlanningBlockHeadshotUpdates = TRUE
						SET_BIT(g_GangOpsFinale.iFinaleBitSet, GANG_OPS_BITSET_BLOCKED_HEADSHOT_UPDATES)
						CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [GB_GANG_OPS_PLANNING] - INITIALISE - g_bGangOpsPlanningBlockHeadshotUpdates = TRUE")
					ENDIF
					
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [GB_GANG_OPS_PLANNING] - INITIALISE - Start Gang Ops Finale Board")
					SET_BIT(g_GangOpsFinale.iFinaleBitSet, GANG_OPS_BITSET_BOARD_SETUP)
					eStage = STAGE_PROCESS_FINALE
				ENDIF
			ENDIF
			
			
		ELSE
			IF NOT IS_BIT_SET(g_GangOpsPlanning.iPlanningBitSet, GANG_OPS_BITSET_BOARD_SETUP)
				STRING sRenderTarget = "Prop_x17DLC_Monitor_Wall_01a"
				IF NOT IS_NAMED_RENDERTARGET_REGISTERED(sRenderTarget)
					REGISTER_NAMED_RENDERTARGET(sRenderTarget)
					LINK_NAMED_RENDERTARGET(XM_PROP_X17DLC_MONITOR_WALL_01A)
					g_GangOpsPlanning.sScaleformData.iRenderTargetID = GET_NAMED_RENDERTARGET_RENDER_ID(sRenderTarget)
				ELSE
					g_GangOpsPlanning.sStrandData.piLeader = piLeader
					
					IF IS_ANY_PLAYER_USING_DEFUNCT_BASE_PLANNING_BOARD()
					OR IS_PLAYER_IN_CORONA()
					OR g_SimpleInteriorData.bAllowInstanceOverrideForCrossSession	// Have we accepted a cross session invite?
						g_GangOpsPlanning.sScaleformData.bSkipLoading = TRUE
					ENDIF
					
					g_GangOpsPlanning.sPositionData.vBoardPosition = GET_SCREEN_COORDS()
					
					g_GangOpsPlanning.sCameraData.ePositionId = GOCAM_MAIN
					g_GangOpsPlanning.sCameraData.vCamInitialOffset = << 1.0, -2.5, -1.2 >>
					g_GangOpsPlanning.sCameraData.vCamMapOffset = << -0.5, -2.5, -1.2 >>
					g_GangOpsPlanning.sCameraData.vCamInitialOffsetRot = << 0.0, 0.0, 0.0 >>
					g_GangOpsPlanning.sCameraData.vCamMapOffsetRot = << 0.0, 0.0, 0.0 >>
					
					g_GangOpsPlanning.sCameraData.vBaseRotation.X = 0.0
					g_GangOpsPlanning.sCameraData.vBaseRotation.Y = 0.0
					g_GangOpsPlanning.sCameraData.vBaseRotation.Z = -180.0
					
					//g_GangOpsPlanning.sCameraData.vBaseRotation = GET_ENTITY_ROTATION(PLAYER_PED_ID())
					
					g_GangOpsPlanning.sStrandData.iStrandId = GET_PLAYER_CURRENT_GANG_OPS_MISSION_STRAND_AS_INT(g_GangOpsPlanning.sStrandData.piLeader)
								
					FILL_GANG_OPS_PLANNING_DATA()
					
					START_NET_TIMER(stArbitraryDelayTimer)
					
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [GB_GANG_OPS_PLANNING] - INITIALISE - Start Gang Ops Planning Board")
					SET_BIT(g_GangOpsPlanning.iPlanningBitSet, GANG_OPS_BITSET_BOARD_SETUP)
					eStage = STAGE_PROCESS_PLANNING
				ENDIF
			ENDIF
		ENDIF

	ELSE
	
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [GB_GANG_OPS_PLANNING] - INITIALISE - Leader is an invalid player index!!!")
	
	ENDIF

ENDPROC

PROC MAINTAIN_CORRECT_LEADER(BOOL bFinale = FALSE)
	IF NOT NETWORK_IS_ACTIVITY_SESSION()
		IF !bFinale
			IF g_GangOpsPlanning.sStrandData.piLeader != GET_OWNER_OF_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN()
			AND g_GangOpsPlanning.sStrandData.piLeader != INVALID_PLAYER_INDEX()
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - MAINTAIN_CORRECT_LEADER - g_GangOpsPlanning.sStrandData.piLeader != GET_OWNER_OF_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN() (",
														GET_PLAYER_NAME(g_GangOpsPlanning.sStrandData.piLeader), " != GET_OWNER_OF_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN")
				
				IF g_GangOpsPlanning.sStateData.eState < GOBS_CLEANUP
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - MAINTAIN_BOARD_CLEANUP_CHECKS -  Set planning state machine to cleanup")
					PUBLIC_SET_GANG_OPS_FSM_STATE(g_GangOpsPlanning.sStateData, GOBS_CLEANUP)
				ELIF g_GangOpsPlanning.sStateData.eState = GOBS_FINISHED
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - MAINTAIN_BOARD_CLEANUP_CHECKS -  Planning state machine has finished, move to cleanup script")
					eStage = STAGE_CLEANUP
				ENDIF
			ENDIF
		ELSE
			IF g_GangOpsFinale.sFinaleData.piLeader != GET_OWNER_OF_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN()
			AND g_GangOpsFinale.sFinaleData.piLeader != INVALID_PLAYER_INDEX()
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - MAINTAIN_CORRECT_LEADER - g_GangOpsFinale.sStrandData.piLeader != GET_OWNER_OF_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN() (",
														GET_PLAYER_NAME(g_GangOpsFinale.sFinaleData.piLeader), " != GET_OWNER_OF_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN")
			
				IF g_GangOpsFinale.sStateData.eState < GOBS_CLEANUP
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - MAINTAIN_BOARD_CLEANUP_CHECKS -  Set finale state machine to cleanup")
					PUBLIC_SET_GANG_OPS_FSM_STATE(g_GangOpsFinale.sStateData, GOBS_CLEANUP)
				ELIF g_GangOpsFinale.sStateData.eState = GOBS_FINISHED
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - MAINTAIN_BOARD_CLEANUP_CHECKS -  Finale state machine has finished, move to cleanup script")
					eStage = STAGE_CLEANUP
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//BOOL bCachedRadarState
PROC MAINTAIN_INTERACTING_WITH_PLANNING_SCREEN()
	IF g_GangOpsIdle.sStateData.eState = GOBS_INUSE
	OR g_GangOpsPlanning.sStateData.eState = GOBS_INUSE
	OR g_GangOpsFinale.sStateData.eState = GOBS_INUSE
		IF NOT IS_BIT_SET(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_INTERACTING_WITH_BOARD)
			SET_BIT(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_INTERACTING_WITH_BOARD)
			DISABLE_RADAR_MAP(TRUE)
			
			IF g_GangOpsFinale.sStateData.eState = GOBS_INUSE
				//If this a finale, track the time at which the player starts interacting with the planning board.
				g_tsHeistPlanningBoardTime = GET_NETWORK_TIME()
			ENDIF
			
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - MAINTAIN_INTERACTING_WITH_PLANNING_SCREEN -  Player is interacting with the gang ops planning screen")
		ENDIF
	ELSE
		IF IS_BIT_SET(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_INTERACTING_WITH_BOARD)
			CLEAR_BIT(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_INTERACTING_WITH_BOARD)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			DISABLE_RADAR_MAP(FALSE)
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - MAINTAIN_INTERACTING_WITH_PLANNING_SCREEN -  Player is not interacting with the gang ops planning screen")
		ENDIF
	ENDIF
ENDPROC

PROC SET_ALL_REMOTE_PLAYERS_INVISIBILE()
	INT iPlayer
	PLAYER_INDEX playerID
	
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		IF NETWORK_IS_PLAYER_ACTIVE(INT_TO_PLAYERINDEX(iPlayer))
			playerID = INT_TO_PLAYERINDEX(iPlayer)
			IF playerID != INVALID_PLAYER_INDEX()
				SET_PLAYER_INVISIBLE_LOCALLY(playerID)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC MAINTAIN_PLAYERS_INVISIBLE()
	IF IS_BIT_SET(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_INTERACTING_WITH_BOARD)
		SET_ALL_REMOTE_PLAYERS_INVISIBILE()
	ENDIF
	
	IF (IS_PLAYER_IN_CORONA() AND NOT NETWORK_IS_ACTIVITY_SESSION())
		SET_ALL_REMOTE_PLAYERS_INVISIBILE()
	ENDIF
	
	IF (IS_BIT_SET(g_GangOpsPlanning.iPlanningBitSet, GANG_OPS_BITSET_LAUNCH_MISSION)
	OR IS_BIT_SET(g_GangOpsPlanning.iPlanningBitSet, GANG_OPS_BITSET_MISSION_LAUNCHED))
	AND NOT IS_BIT_SET(g_GangOpsPlanning.iPlanningBitSet, GANG_OPS_BITSET_TOGGLED_RENDERPHASES)
		SET_ALL_REMOTE_PLAYERS_INVISIBILE()
	ENDIF
	
	IF (IS_BIT_SET(g_GangOpsFinale.iFinaleBitSet, GANG_OPS_BITSET_LAUNCH_MISSION)
	OR IS_BIT_SET(g_GangOpsFinale.iFinaleBitSet, GANG_OPS_BITSET_MISSION_LAUNCHED))
	AND NOT IS_BIT_SET(g_GangOpsFinale.iFinaleBitSet, GANG_OPS_BITSET_TOGGLED_RENDERPHASES)
		SET_ALL_REMOTE_PLAYERS_INVISIBILE()
	ENDIF
ENDPROC

FUNC INT GET_BOARD_DELAY_TIME()
	IF IS_BIT_SET(g_GangOpsIdle.iIdleBitSet, GANG_OPS_BITSET_BOARD_SETUP)
	OR IS_BIT_SET(g_GangOpsPlanning.iPlanningBitSet, GANG_OPS_BITSET_BOARD_SETUP)
		RETURN ARBITRARY_DELAY
	ENDIF
	
	// Default to finale time
	RETURN (ARBITRARY_DELAY+2000)
ENDFUNC

PROC MAINTAIN_PLAYER_IN_ROOM_LONG_ENOUGH_TO_INTERACT()
	IF HAS_NET_TIMER_STARTED(stArbitraryDelayTimer)
		IF HAS_NET_TIMER_EXPIRED(stArbitraryDelayTimer, GET_BOARD_DELAY_TIME())
			IF NOT IS_BIT_SET(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_IN_ROOM_LONG_ENOUGH_TO_INTERACT)
				SET_BIT(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_IN_ROOM_LONG_ENOUGH_TO_INTERACT)
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - MAINTAIN_PLAYER_IN_ROOM_LONG_ENOUGH_TO_INTERACT -  In room long enough to be able to interact")
			ENDIF
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - MAINTAIN_PLAYER_IN_ROOM_LONG_ENOUGH_TO_INTERACT -  Timer hasnt started yet, waiting for initialisation to complete")
	ENDIF
ENDPROC

PROC MAINTAIN_RETURN_SCREEN_TO_MAINTAIN_STATE()
	IF IS_BIT_SET(g_GangOpsFinale.iFinaleBitSet, GANG_OPS_BITSET_BOARD_SETUP)

		IF IS_BIT_SET(g_GangOpsFinale.iFinaleBitSet, GANG_OPS_BITSET_RETURN_TO_MAINTAIN)
			CLEAN_UP_INTRO_STAGE_CAM(ciCoronaCam, -1)
			eStage = STAGE_PROCESS_FINALE
			CLEAR_BIT(g_GangOpsFinale.iFinaleBitSet, GANG_OPS_BITSET_DO_TOGGLE_RENDERPHASES)
			CLEAR_BIT(g_GangOpsFinale.iFinaleBitSet, GANG_OPS_BITSET_TOGGLED_RENDERPHASES)
			CLEAR_BIT(g_GangOpsFinale.iFinaleBitSet, GANG_OPS_BITSET_LAUNCH_MISSION)
			CLEAR_BIT(g_GangOpsFinale.iFinaleBitSet, GANG_OPS_BITSET_MISSION_LAUNCHED)
			CLEAR_BIT(g_GangOpsFinale.iFinaleBitSet, GANG_OPS_BITSET_RETURN_TO_MAINTAIN)
			PRINTLN("[GANGOPS_FLOW] MAINTAIN_RETURN_SCREEN_TO_MAINTAIN_STATE - CLEAR_BIT(g_GangOpsFinale.iFinaleBitSet, GANG_OPS_BITSET_RETURN_TO_MAINTAIN), eStage = STAGE_PROCESS_FINALE")
		ENDIF
		
	ELIF IS_BIT_SET(g_GangOpsPlanning.iPlanningBitSet, GANG_OPS_BITSET_BOARD_SETUP)

		IF IS_BIT_SET(g_GangOpsPlanning.iPlanningBitSet, GANG_OPS_BITSET_RETURN_TO_MAINTAIN)
			CLEAN_UP_INTRO_STAGE_CAM(ciCoronaCam, -1)
			eStage = STAGE_PROCESS_PLANNING
			CLEAR_BIT(g_GangOpsPlanning.iPlanningBitSet, GANG_OPS_BITSET_DO_TOGGLE_RENDERPHASES)
			CLEAR_BIT(g_GangOpsPlanning.iPlanningBitSet, GANG_OPS_BITSET_TOGGLED_RENDERPHASES)
			CLEAR_BIT(g_GangOpsPlanning.iPlanningBitSet, GANG_OPS_BITSET_LAUNCH_MISSION)
			CLEAR_BIT(g_GangOpsPlanning.iPlanningBitSet, GANG_OPS_BITSET_MISSION_LAUNCHED)
			CLEAR_BIT(g_GangOpsPlanning.iPlanningBitSet, GANG_OPS_BITSET_RETURN_TO_MAINTAIN)
			PRINTLN("[GANGOPS_FLOW] MAINTAIN_RETURN_SCREEN_TO_MAINTAIN_STATE - CLEAR_BIT(g_GangOpsPlanning.iPlanningBitSet, GANG_OPS_BITSET_RETURN_TO_MAINTAIN), eStage = STAGE_PROCESS_PLANNING")
		ENDIF

	ENDIF
ENDPROC

PROC MAINTAIN_GENERAL_CHECKS()
	MAINTAIN_INTERACTING_WITH_PLANNING_SCREEN()
	MAINTAIN_PLAYERS_INVISIBLE()
	MAINTAIN_PLAYER_IN_ROOM_LONG_ENOUGH_TO_INTERACT()
	MAINTAIN_RETURN_SCREEN_TO_MAINTAIN_STATE()
ENDPROC

#IF IS_DEBUG_BUILD

//╒═══════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡    DEBUG    ╞═════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════╛

PROC CREATE_GANGOPS_PLANNING_WIDGETS()
	IF NOT DOES_WIDGET_GROUP_EXIST(planningWidgets)
		planningWidgets = START_WIDGET_GROUP("GB Gang Ops Planning")
	
		ADD_WIDGET_BOOL("Reset all board data", bResetGangOps)
		ADD_WIDGET_BOOL("Reset planning board data", bResetGangOpsPlanning)
		ADD_WIDGET_BOOL("Reset finale board data", bResetGangOpsFinale)
		
		ADD_WIDGET_BOOL("Print heist mission array positions to logs", bPrintToLogs)
	
		ADD_WIDGET_BOOL("Kill Script", bEndScript)
	
		STOP_WIDGET_GROUP()
	ENDIF
ENDPROC

PROC MAINTAIN_GANG_OPS_PLANNING_WIDGETS()

	IF bResetGangOps
		PUBLIC_CLEAN_GANG_OPS_ALL()
		eStage = STAGE_INIT
		bResetGangOps = FALSE
	ENDIF

	IF bResetGangOpsPlanning
		PUBLIC_CLEAN_GANG_OPS_PLANNING()
		eStage = STAGE_INIT
		bResetGangOps = FALSE
		bResetGangOpsPlanning = FALSE
	ENDIF
	
	IF bResetGangOpsFinale
		PUBLIC_CLEAN_GANG_OPS_FINALE()
		eStage = STAGE_INIT
		bResetGangOps = FALSE
		bResetGangOpsFinale = FALSE
	ENDIF
	
	IF bEndScript
		eStage = STAGE_CLEANUP
		bEndScript = FALSE
	ENDIF
	
	IF bPrintToLogs
		INT iMissionLoop, iMission
		REPEAT 17 iMission
			REPEAT FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED iMissionLoop
				IF g_sMPTunables.iGangOpsFlowRootContentID[iMission] = g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[iMissionLoop].iRootContentIdHash
					CDEBUG1LN(DEBUG_GANG_OPS, "MAINTAIN_GANG_OPS_PLANNING_WIDGETS - ", GET_GANG_OPS_MISSION_NAME_FOR_DEBUG(INT_TO_ENUM(GANG_OPS_MISSION_ENUM, iMission)), " array pos = ", iMissionLoop)
				ENDIF
			ENDREPEAT
		ENDREPEAT
		bPrintToLogs = FALSE
	ENDIF

ENDPROC

#ENDIF

//╒════════════════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡    SCRIPT TERMINATION    ╞═════════════════════════════════╡
//╘════════════════════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL SHOULD_PLANNING_SCRIPT_CLEANUP()
	// IF I'm far away from the board and not in CCTV, clean up
	IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<353.71417, 4877.06006, -59.14812>>) >= 21.0
	AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<345.187897,4864.414551,-60.026810>>, <<334.601746,4848.472168,-56.749474>>, 6.000000)
	AND NOT IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<336.949036,4848.106934,-60.249474>>, <<339.973877,4843.732422,-56.749470>>, 7.000000)
	AND NOT g_bUsingCCTV
		RETURN TRUE
	ENDIF
	
	// IF i'm CCTV and no one else is in the room, clean up
	IF g_bUsingCCTV
	AND NOT IS_ANY_PLAYER_USING_DEFUNCT_BASE_PLANNING_BOARD()
		RETURN TRUE
	ENDIF
	
	IF g_GangOpsIdle.sStateData.eState >= GOBS_DISPLAYING
		IF IS_BIT_SET(g_GangOpsIdle.iIdleBitSet, GANG_OPS_BITSET_BOARD_SETUP)
		AND GET_PLAYER_CURRENT_GANG_OPS_MISSION_STRAND(g_GangOpsIdle.sIdleData.piLeader) != GANG_OPS_MISSION_STRAND_INVALID
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - SHOULD_PLANNING_SCRIPT_CLEANUP -  I'm displaying the idle board but the leader has an active strand, clean up and restart!")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROGRESS_GANG_OPS_FSM_TO_CLEANUP()
	// Finale
	IF g_GangOpsFinale.sStateData.eState < GOBS_CLEANUP
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - PROGRESS_GANG_OPS_FSM_TO_CLEANUP -  Set finale state machine to cleanup")
		PUBLIC_SET_GANG_OPS_FSM_STATE(g_GangOpsFinale.sStateData, GOBS_CLEANUP)
	ELIF g_GangOpsFinale.sStateData.eState = GOBS_FINISHED
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - PROGRESS_GANG_OPS_FSM_TO_CLEANUP -  Finale state machine has finished, move to cleanup script")
		eStage = STAGE_CLEANUP
	ENDIF
	
	// Planning
	IF g_GangOpsPlanning.sStateData.eState < GOBS_CLEANUP
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - PROGRESS_GANG_OPS_FSM_TO_CLEANUP -  Set planning state machine to cleanup")
		PUBLIC_SET_GANG_OPS_FSM_STATE(g_GangOpsPlanning.sStateData, GOBS_CLEANUP)
	ELIF g_GangOpsPlanning.sStateData.eState = GOBS_FINISHED
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - PROGRESS_GANG_OPS_FSM_TO_CLEANUP -  Planning state machine has finished, move to cleanup script")
		eStage = STAGE_CLEANUP
	ENDIF
	
	// Idle
	IF g_GangOpsIdle.sStateData.eState < GOBS_CLEANUP
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - PROGRESS_GANG_OPS_FSM_TO_CLEANUP -  Set idle state machine to cleanup")
		PUBLIC_SET_GANG_OPS_FSM_STATE(g_GangOpsIdle.sStateData, GOBS_CLEANUP)
		eStage = STAGE_PROCESS_IDLE
	ELIF g_GangOpsIdle.sStateData.eState = GOBS_FINISHED
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - PROGRESS_GANG_OPS_FSM_TO_CLEANUP -  Idle state machine has finished, move to cleanup script")
		eStage = STAGE_CLEANUP
	ENDIF
ENDPROC

PROC MAINTAIN_BOARD_CLEANUP_CHECKS()
	#IF IS_DEBUG_BUILD
	IF iGangOpsPlanningBoardActive = ciDEBUG_GANGOPS_PLANNINGBOARD_RESTART
		PROGRESS_GANG_OPS_FSM_TO_CLEANUP()
	ENDIF
	#ENDIF
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
	AND IS_NET_PLAYER_OK(PLAYER_ID())
		IF NOT IS_PLAYER_IN_CORONA()
		AND NOT IS_TRANSITION_SESSION_LAUNCHING()
		AND NOT IS_TRANSITION_SESSION_RESTARTING()
			IF SHOULD_PLANNING_SCRIPT_CLEANUP()
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - MAINTAIN_BOARD_CLEANUP_CHECKS -  Player is >= 18.0 from board and isnt using CCTV, we need to clean up")
				PROGRESS_GANG_OPS_FSM_TO_CLEANUP()
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_REBOOT_PLANNING_SCREEN)
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - MAINTAIN_BOARD_CLEANUP_CHECKS -  GANG_OPS_BD_GENERAL_BITSET_REBOOT_PLANNING_SCREEN is set")
		PROGRESS_GANG_OPS_FSM_TO_CLEANUP()
	ENDIF
	
	IF GANG_OPS_SHOULD_BOARD_CLEAN_UP()
	OR TRANSITION_SESSIONS_FAILED_IN_LOAD_MISSION_FOR_TS()
		IF eStage > STAGE_INIT
		AND eStage < STAGE_CLEANUP
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - MAINTAIN_BOARD_CLEANUP_CHECKS -  NOT SHOULD_TRANSITION_SESSIONS_CLEANUP_AFTER_CORONA_WALK_OUT, eStage = STAGE_CLEANUP")
			eStage = STAGE_CLEANUP
		ENDIF
		PROGRESS_GANG_OPS_FSM_TO_CLEANUP()
	ENDIF
ENDPROC

PROC CLEANUP_SOUNDS()
	IF g_GangOpsIdle.sAudioData.bStartedLoopingBackgroundSound
		IF g_GangOpsIdle.sAudioData.iLoopingBackgroundSoundId != -1
			STOP_SOUND(g_GangOpsIdle.sAudioData.iLoopingBackgroundSoundId)
			RELEASE_SOUND_ID(g_GangOpsIdle.sAudioData.iLoopingBackgroundSoundId)
			g_GangOpsIdle.sAudioData.iLoopingBackgroundSoundId = -1
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] CLEANUP_SOUNDS - g_GangOpsIdle.sAudioData.iLoopingBackgroundSoundId Cleared")
			g_GangOpsIdle.sAudioData.bStartedLoopingBackgroundSound = FALSE
		ENDIF
	ENDIF


	IF g_GangOpsPlanning.sAudioData.bStartedLoopingBackgroundSound
		IF g_GangOpsPlanning.sAudioData.iLoopingBackgroundSoundId != -1
			STOP_SOUND(g_GangOpsPlanning.sAudioData.iLoopingBackgroundSoundId)
			RELEASE_SOUND_ID(g_GangOpsPlanning.sAudioData.iLoopingBackgroundSoundId)
			g_GangOpsPlanning.sAudioData.iLoopingBackgroundSoundId = -1
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] CLEANUP_SOUNDS - g_GangOpsPlanning.sAudioData.iLoopingBackgroundSoundId Cleared")
			g_GangOpsPlanning.sAudioData.bStartedLoopingBackgroundSound = FALSE
		ENDIF
	ENDIF

	IF g_GangOpsFinale.sAudioData.bStartedLoopingBackgroundSound
		IF g_GangOpsFinale.sAudioData.iLoopingBackgroundSoundId != -1
			STOP_SOUND(g_GangOpsFinale.sAudioData.iLoopingBackgroundSoundId)
			RELEASE_SOUND_ID(g_GangOpsFinale.sAudioData.iLoopingBackgroundSoundId)
			g_GangOpsFinale.sAudioData.iLoopingBackgroundSoundId = -1
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] CLEANUP_SOUNDS - g_GangOpsFinale.sAudioData.iLoopingBackgroundSoundId Cleared")
			g_GangOpsFinale.sAudioData.bStartedLoopingBackgroundSound = FALSE
		ENDIF
	ENDIF
	
	KILL_FINALE_READY_SOUND(g_GangOpsFinale)
ENDPROC

PROC CLEANUP_HELP()
	CLEANUP_PLANNING_BOARD_CONTEXT()
	CLEANUP_FINALE_BOARD_CONTEXT()
	CLEANUP_IDLE_BOARD_CONTEXT()
	CLEAR_PLANNING_BECOME_BOSS_HELP()
	CLEAR_FINALE_BECOME_BOSS_HELP()
	CLEAR_IDLE_BECOME_BOSS_HELP()
ENDPROC

PROC SCRIPT_CLEANUP()
	CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - GB_GANG_OPS_PLANNING.sc SCRIPT_CLEANUP")
	
	IF IS_BIT_SET(g_GangOpsFinale.iFinaleBitSet, GANG_OPS_BITSET_BLOCKED_HEADSHOT_UPDATES)
		IF g_bGangOpsPlanningBlockHeadshotUpdates
			g_bGangOpsPlanningBlockHeadshotUpdates = FALSE
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [GB_GANG_OPS_PLANNING] - SCRIPT_CLEANUP - g_bGangOpsPlanningBlockHeadshotUpdates = FALSE")
		ENDIF
	ENDIF
	
	// ALL DATA GETS WIPED HERE, MAKE SURE YOU DO ANYTHING CLEANUP THAT NEEDS THE DATA ABOVE HERE
	
	CLEANUP_HELP()
	CLEANUP_SOUNDS()
	PUBLIC_CLEAN_GANG_OPS_ALL()
	IF IS_NAMED_RENDERTARGET_REGISTERED("Prop_x17DLC_Monitor_Wall_01a")
		RELEASE_NAMED_RENDERTARGET("Prop_x17DLC_Monitor_Wall_01a")
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - SCRIPT_CLEANUP - RELEASE_NAMED_RENDERTARGET(Prop_x17DLC_Monitor_Wall_01a)")
	ELSE
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - SCRIPT_CLEANUP - named rendertarget Prop_x17DLC_Monitor_Wall_01a is not registered")
	ENDIF
	
	CLEAN_UP_INTRO_STAGE_CAM(ciCoronaCam, -1)
	
	#IF IS_DEBUG_BUILD
	IF DOES_WIDGET_GROUP_EXIST(planningWidgets)
		DELETE_WIDGET_GROUP(planningWidgets)
	ENDIF
	iGangOpsPlanningBoardActive = ciDEBUG_GANGOPS_PLANNINGBOARD_OFF
	#ENDIF
	
	IF g_bGangOpsPlanningScriptActive
		g_bGangOpsPlanningScriptActive = FALSE
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - g_bGangOpsPlanningScriptActive - FALSE")
		
		IF IS_BIT_SET(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_RUNNING_PLANNING_SCRIPT)
			CLEAR_BIT(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_RUNNING_PLANNING_SCRIPT)
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - CLEAR_BIT(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_RUNNING_PLANNING_SCRIPT)")
		ENDIF
		
		IF IS_BIT_SET(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_BOARD_IN_DISPLAYING_STATE)
			CLEAR_BIT(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_BOARD_IN_DISPLAYING_STATE)
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - CLEAR_BIT(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_BOARD_IN_DISPLAYING_STATE)")
		ENDIF
		
		IF IS_BIT_SET(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_INTERACTING_WITH_BOARD)
			CLEAR_BIT(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_INTERACTING_WITH_BOARD)
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - CLEAR_BIT(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_INTERACTING_WITH_BOARD)")
		ENDIF
		
		IF IS_BIT_SET(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_IN_ROOM_LONG_ENOUGH_TO_INTERACT)
			CLEAR_BIT(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_IN_ROOM_LONG_ENOUGH_TO_INTERACT)
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - CLEAR_BIT(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_IN_ROOM_LONG_ENOUGH_TO_INTERACT)")
		ENDIF
	ENDIF
	
	TERMINATE_THIS_THREAD()
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡    PRE GAME / SCRIPT LAUNCHING    ╞═════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════════════════════════════╛

PROC PROCESS_PRE_GAME()
	CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - PROCESS_PRE_GAME")
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	
	IF NOT AM_I_TRANSITION_SESSIONS_STARTING_GANG_OPS_QUICK_MATCH()
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - Clear on call")
    	SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW_IF_NEEDED()
	ELSE
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - Already on call to gang ops")
	ENDIF
	
	IF g_bGangOpsPlanningScriptActive = FALSE
		g_bGangOpsPlanningScriptActive = TRUE
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - g_bGangOpsPlanningScriptActive - TRUE")
		
		IF IS_BIT_SET(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_REBOOT_PLANNING_SCREEN)
			CLEAR_BIT(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_REBOOT_PLANNING_SCREEN)
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - We rebooted the screen, clear GANG_OPS_BD_GENERAL_BITSET_REBOOT_PLANNING_SCREEN")
		ENDIF
		
		IF NOT g_bUsingCCTV
			IF NOT IS_BIT_SET(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_RUNNING_PLANNING_SCRIPT)
				SET_BIT(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_RUNNING_PLANNING_SCRIPT)
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - SET_BIT(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_RUNNING_PLANNING_SCRIPT)")
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - using cctv, not setting in use GBD bit")
		ENDIF
	ENDIF
ENDPROC


//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡    MAIN LOOP    ╞═════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

SCRIPT

	IF NETWORK_IS_GAME_IN_PROGRESS() 
		PROCESS_PRE_GAME()
	ELSE
		SCRIPT_CLEANUP()
	ENDIF
	
	#IF IS_DEBUG_BUILD
	CREATE_GANGOPS_PLANNING_WIDGETS()
	#ENDIF

	WHILE TRUE
	
		WAIT(0)
		
		#IF IS_DEBUG_BUILD
		MAINTAIN_GANG_OPS_PLANNING_WIDGETS()
		//If we change root ID
		//And we ere not on a finale before, and we are now, or vice a versa then complain.
		IF iRootContentIDHashCache != g_FMMC_STRUCT.iRootContentIDHash
		AND eStage > STAGE_INIT
			IF bFinaleDebugCache != GANGOPS_FLOW_IS_MISSION_A_GANGOPS_FINALE_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [GB_GANG_OPS_PLANNING] - bFinaleDebugCache != GANGOPS_FLOW_IS_MISSION_A_GANGOPS_FINALE_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)")
				IF NETWORK_IS_ACTIVITY_SESSION()
					ASSERTLN("[GANGOPS_FLOW] [GB_GANG_OPS_PLANNING] - bFinaleDebugCache != GANGOPS_FLOW_IS_MISSION_A_GANGOPS_FINALE_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)")
				ENDIF
			ENDIF
		ENDIF 
		#ENDIF
			
		IF IS_BIT_SET(g_iFacilityHeistPlanningSeats, ciFACILITY_HEIST_PLANNING_SEATS_READY_FOR_BOARD)
			PRINTLN("[boardspam] - IS_BIT_SET(g_iFacilityHeistPlanningSeats, ciFACILITY_HEIST_PLANNING_SEATS_READY_FOR_BOARD)")
		ENDIF
				
		MAINTAIN_GENERAL_CHECKS()
		MAINTAIN_BOARD_CLEANUP_CHECKS()
		
		SWITCH eStage
			// Check whether we need to load/display the planning board or the finale board, prep the scaleforms and grab the data about the strand/finale
			CASE STAGE_INIT
				INITIALISE()
			BREAK
			
			// Maintain the heist select/replay board
			CASE STAGE_PROCESS_IDLE
				MAINTAIN_GANG_OPS_IDLE_BOARD()
				MAINTAIN_IDLE_BOARD_USEAGE()
			BREAK
			
			// Maintain setting the selected heist after using the heist select/replay board
			CASE STAGE_PROCESS_HEIST_SELECTED
				MAINTAIN_GANG_OPS_IDLE_BOARD()
				MAINTAIN_HEIST_SELECTED()
			BREAK
			
			// Maintain the planning board for setup missions
			CASE STAGE_PROCESS_PLANNING
				MAINTAIN_CORRECT_LEADER()
				MAINTAIN_GANG_OPS_PLANNING_BOARD()
				MAINTAIN_PLANNING_BOARD_USEAGE()
			BREAK
			
			// Maintin launching the missions from the planning board
			CASE STAGE_PROCESS_LAUNCH
				MAINTAIN_GANG_OPS_PLANNING_BOARD()
				MAINTAIN_MISSION_LAUNCHING()
			BREAK
			
			// Maintain the finale board, and launching the finale from the board
			CASE STAGE_PROCESS_FINALE
				MAINTAIN_CORRECT_LEADER(TRUE)
				MAINTAIN_GANG_OPS_FINALE_BOARD()
				MAINTAIN_FINALE_BOARD_USEAGE()
				MAINTAIN_LAUNCH_FINALE_MISSION()
			BREAK
			
			// Cleanup
			CASE STAGE_CLEANUP
				SCRIPT_CLEANUP()
			BREAK
		ENDSWITCH
		
		CLEAR_GANG_OPS_PLANNING_BOARD_DISABLED_THIS_FRAME()
	ENDWHILE

ENDSCRIPT
