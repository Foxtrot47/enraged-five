//----------------------
//	INCLUDES
//----------------------

USING "rage_builtins.sch"
USING "globals.sch"

//USING "GB_GANGOPS_HEADER_COMMON.sch"

CONST_INT ENTRY_CS_STAGE_INIT_DATA				0
CONST_INT ENTRY_CS_STAGE_WAIT_FOR_MODELS		1	
CONST_INT ENTRY_CS_STAGE_TRIGGER_CUT			2
CONST_INT ENTRY_CS_STAGE_RUN_CUT				3
CONST_INT ENTRY_CS_STAGE_RUN_SECOND_CUT			4
CONST_INT ENTRY_CS_STAGE_HANDLE_PLAYER_PED		5

CONST_INT ENTRY_CS_BS_GET_DATA					0
CONST_INT ENTRY_CS_BS_STARTED_MP_CUTSCENE		1
CONST_INT ENTRY_CS_BS_TOOK_AWAY_CONTROL			2
//CONST_INT ENTRY_CS_BS_SECOND_CUT				3
CONST_INT ENTRY_CS_BS_APT_DOOR_MODEL_HIDE		4
CONST_INT ENTRY_CS_BS_STARTED_FAKE_CUT			5
CONST_INT ENTRY_CS_BS_DONE_SHAKE				6

ENUM SYNC_SCENE_ELEMENTS
	SYNC_SCENE_PED_A,
	SYNC_SCENE_DOOR,
	SYNC_SCENE_CAM,
	SYNC_SCENE_MAX_ELEMENTS
ENDENUM

STRUCT ENTRY_CUTSCENE_DATA
	PED_INDEX playerClone
	
	OBJECT_INDEX objIDs[2]
	CAMERA_INDEX cameras[2]
	SCRIPT_TIMER timer
	INT iStage
	INT iSyncedSceneID = -1
	INT iBS
	
	STRING animDictionary
	STRING animName[SYNC_SCENE_MAX_ELEMENTS]
	
	VECTOR vCamLoc[2]
	VECTOR vCamRot[2]
	FLOAT fCamFOV[2]
	
	VECTOR vSyncSceneLoc
	VECTOR vSyncSceneRot
	
	FLOAT fSyncedSceneCompleteStage
	
//	INT iSoundID
ENDSTRUCT
ENTRY_CUTSCENE_DATA entryCutData

MP_CAM_OFFSET_STRUCT tempCamOffset
MP_CAM_OFFSET_STRUCT tempCamOffset2
MP_PROP_OFFSET_STRUCT tempPropOffset

FUNC BOOL SHOULD_DO_BARGE_ANIM_FOR_BURGLARY_JOB()
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_BURGLARY_JOB			RETURN IS_SERVER_BIT_SET(eSERVERBITSET_MAKE_ALL_PEDS_REACT)
	ENDSWITCH
	RETURN FALSE
ENDFUNC

// Anim SyncedScene Editor/Anim synced scene editor/mp_m_freemode_01/Clip Selector/Clip Context
PROC GET_ENTER_SCENE_DETAILS(MP_PROP_OFFSET_STRUCT &mpOffset) 
	SWITCH GET_GANGOPS_LOCATION()
		CASE GangOpsLocation_ForcedEntry_Location_Fudge
			mpOffset.vLoc 		= << 1326.484, -1552.217, 54.000 >>
			mpOffset.vRot		=  << 0.000, -0.000, -123.000 >>
		BREAK
		CASE GangOpsLocation_ForcedEntry_Location_Magellan
			mpOffset.vLoc 		= << -1141.092, -1462.227, 7.700 >>
			mpOffset.vRot		= << 0.000, 0.000, 51.000 >>
		BREAK
		CASE GangOpsLocation_ForcedEntry_Location_Forum
			mpOffset.vLoc 		= << -161.275, -1636.864, 37.250 >>
			mpOffset.vRot		= << 0.000, -0.000, 164.000 >>
		BREAK
		CASE GangOpsLocation_ForcedEntry_Location_Roy_Lowenstein
			mpOffset.vLoc 		= << 455.300, -1578.719, 32.800 >>
			mpOffset.vRot		= << 0.000, -0.000, 164.000 >>
		BREAK
		CASE GangOpsLocation_ForcedEntry_Location_Eastbourne//4
			mpOffset.vLoc 		= << -362.250, 56.213, 54.450 >>
			mpOffset.vRot		= << 0.000, 0.000, 10.000 >>
		BREAK
		
		CASE GangOpsLocation_ForcedEntry_Location_V_Magellan
			mpOffset.vLoc 		= << -1270.578, -1296.622, 8.300 >>
			mpOffset.vRot		= << 0.000, -0.000, 130.000 >>
		BREAK
		CASE GangOpsLocation_ForcedEntry_Location_Alta
			mpOffset.vLoc 		= << 204.168, -99.953, 73.298 >>
			mpOffset.vRot		= << 0.000, 0.000, 12.000 >>
		BREAK
		CASE GangOpsLocation_ForcedEntry_Location_Forum_Drive
			mpOffset.vLoc 		= << -148.900, -1688.784, 36.200 >>
			mpOffset.vRot		= << 0.000, 0.000, -27.000 >>
		BREAK
		CASE GangOpsLocation_ForcedEntry_Location_Spanish
			mpOffset.vLoc 		= << -356.946, 91.177, 70.532 >>
			mpOffset.vRot		= << 0.000, 0.000, 87.000 >>
		BREAK
		CASE GangOpsLocation_ForcedEntry_Location_Tahitian
			mpOffset.vLoc 		= << -27.826, -1545.253, 33.830 >>
			mpOffset.vRot		= << 0.000, 0.000, -44.000 >>
		BREAK
		
		
		CASE GangOpsLocation_BurglaryJob_ProcopioDrive
			IF SHOULD_DO_BARGE_ANIM_FOR_BURGLARY_JOB()
				mpOffset.vLoc 		= << -406.510, 6313.010, 28.951 >>
				mpOffset.vRot		= << 0.000, 0.000, 41.040 >>
			ELSE
				mpOffset.vLoc 		= << -406.460, 6312.610, 28.950 >>
				mpOffset.vRot		= << 0.000, 0.000, 38.040 >>
			ENDIF
		BREAK
		CASE GangOpsLocation_BurglaryJob_SouthMoMilton
			IF SHOULD_DO_BARGE_ANIM_FOR_BURGLARY_JOB()
				mpOffset.vLoc 		= << -1091.500, 547.850, 103.650 >>
				mpOffset.vRot		= << 0.000, 0.000, -60.120 >>
			ELSE
				mpOffset.vLoc 		= << -1091.750, 547.700, 103.650 >>
				mpOffset.vRot		= << 0.000, 0.000, -60.120 >>
			ENDIF
		BREAK
		CASE GangOpsLocation_BurglaryJob_MirrorPark
			IF SHOULD_DO_BARGE_ANIM_FOR_BURGLARY_JOB()
				mpOffset.vLoc 		= << 1199.733, -576.475, 69.124 >>
				mpOffset.vRot		= << 0.000, 0.000, -46.800 >>
			ELSE
				mpOffset.vLoc 		= << 1199.483, -576.575, 69.124 >>
				mpOffset.vRot		= << 0.000, 0.000, -51.840 >>
			ENDIF
		BREAK
		CASE GangOpsLocation_BurglaryJob_ImaginationCourt
			IF SHOULD_DO_BARGE_ANIM_FOR_BURGLARY_JOB()
				mpOffset.vLoc 		= << -1124.050, -1090.100, 2.600 >>
				mpOffset.vRot		= << 0.000, 0.000, -60.120 >>
			ELSE
				mpOffset.vLoc 		= << -1124.250, -1090.350, 2.600 >>
				mpOffset.vRot		= << 0.000, 0.000, -60.120 >>
			ENDIF
		BREAK
		CASE GangOpsLocation_BurglaryJob_DidionDrive
			IF SHOULD_DO_BARGE_ANIM_FOR_BURGLARY_JOB()
				mpOffset.vLoc 		= << -371.950, 344.900, 109.900 >>
				mpOffset.vRot		= << 0.000, -0.000, -173.880 >>
			ELSE
				mpOffset.vLoc 		= << -371.950, 345.250, 109.900 >>
				mpOffset.vRot		= << 0.000, -0.000, -173.880 >>
			ENDIF
		BREAK
		CASE GangOpsLocation_BurglaryJob_KimbleHill
			IF SHOULD_DO_BARGE_ANIM_FOR_BURGLARY_JOB()
				mpOffset.vLoc 		= << -293.450, 602.400, 181.500 >>
				mpOffset.vRot		= << 0.000, -0.000, 177.480 >>
			ELSE
				mpOffset.vLoc 		= << -293.450, 602.700, 181.500 >>
				mpOffset.vRot		= << 0.000, -0.000, 177.480 >>
			ENDIF
		BREAK
		CASE GangOpsLocation_BurglaryJob_BridgeStreet
			IF SHOULD_DO_BARGE_ANIM_FOR_BURGLARY_JOB()
				mpOffset.vLoc 		= << 1046.550, -496.600, 64.200 >>
				mpOffset.vRot		= << 0.000, -0.000, 168.120 >>
			ELSE
				mpOffset.vLoc 		= << 1046.650, -496.350, 64.200 >>
				mpOffset.vRot		= << 0.000, -0.000, 168.120 >>
			ENDIF
		BREAK
		CASE GangOpsLocation_BurglaryJob_NikolaAvenue
			IF SHOULD_DO_BARGE_ANIM_FOR_BURGLARY_JOB()
				mpOffset.vLoc 		= << 920.350, -571.050, 58.400 >>
				mpOffset.vRot		= << 0.000, 0.000, 25.560 >>
			ELSE
				mpOffset.vLoc 		= << 920.500, -571.300, 58.400 >>
				mpOffset.vRot		= << 0.000, 0.000, 25.560 >>
			ENDIF
		BREAK
		CASE GangOpsLocation_BurglaryJob_NorthSheldonAvenue
			IF SHOULD_DO_BARGE_ANIM_FOR_BURGLARY_JOB()
				mpOffset.vLoc 		= << -973.300, 753.400, 176.400 >>
				mpOffset.vRot		= << 0.000, -0.000, -135.000 >>
			ELSE
				mpOffset.vLoc 		= << -973.550, 753.650, 176.350 >>
				mpOffset.vRot		= << 0.000, -0.000, -135.000 >>
			ENDIF
		BREAK
		CASE GangOpsLocation_BurglaryJob_InesenoRoad
			IF SHOULD_DO_BARGE_ANIM_FOR_BURGLARY_JOB()
				mpOffset.vLoc 		= << -3092.300, 348.850, 7.500 >>
				mpOffset.vRot		= << 0.000, 0.000, 74.160 >>
			ELSE
				mpOffset.vLoc 		= << -3092.000, 348.800, 7.500 >>
				mpOffset.vRot		= << 0.000, 0.000, 74.160 >>
			ENDIF
		BREAK
		
		CASE GangOpsLocation_CarCollector_SteeleWay
			mpOffset.vLoc 		= << -1045.350, 225.350, 62.750 >>
			mpOffset.vRot		= << 0.000, -0.000, 90.720 >>
		BREAK
		CASE GangOpsLocation_CarCollector_AmericanoWay
			mpOffset.vLoc 		= << -1558.350, 17.450, 57.850 >>
			mpOffset.vRot		= << 0.000, -0.000, -99.360 >>
		BREAK
		CASE GangOpsLocation_CarCollector_AceJonesDrive
			mpOffset.vLoc 		= << -1556.350, 431.400, 108.500 >>
			mpOffset.vRot		= << 0.000, -0.000, 180.000 >>
		BREAK
		CASE GangOpsLocation_CarCollector_Richman
			mpOffset.vLoc 		= << -1745.350, 368.600, 88.750 >>
			mpOffset.vRot		= << 0.000, 0.000, 44.640 >>
		BREAK
		CASE GangOpsLocation_CarCollector_NorthRockford_1
			mpOffset.vLoc 		= << -1868.273, 192.074, 83.294 >>
			mpOffset.vRot		= << 0.000, 0.000, 36.360 >>
		BREAK
		CASE GangOpsLocation_CarCollector_DunstableLane_1
			mpOffset.vLoc 		= << -1935.775, 366.112, 92.823 >>
			mpOffset.vRot		= << 0.000, -0.000, 100.440 >>
		BREAK
		CASE GangOpsLocation_CarCollector_DunstableLane_2
			mpOffset.vLoc 		= << -889.336, 360.446, 84.031 >>
			mpOffset.vRot		= << 0.000, 0.000, -84.960 >>
		BREAK
		CASE GangOpsLocation_CarCollector_NorthRockford_2
			mpOffset.vLoc 		= << -2000.684, 296.406, 90.765 >>
			mpOffset.vRot		= << 0.000, -0.000, -156.240 >>
		BREAK
		CASE GangOpsLocation_CarCollector_CaesarsPlace
			mpOffset.vLoc 		= << -872.431, 51.410, 47.780 >>
			mpOffset.vRot		= << 0.000, -0.000, 125.280 >>
		BREAK
		CASE GangOpsLocation_CarCollector_HillcrestAvenue
			mpOffset.vLoc 		= << -714.273, 648.418, 154.175 >>
			mpOffset.vRot		= << 0.000, -0.000, -94.320 >>
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_CAM_DETAILS(MP_CAM_OFFSET_STRUCT &mpOffset, MP_CAM_OFFSET_STRUCT &mpOffset2)
	SWITCH GET_GANGOPS_LOCATION()
		CASE GangOpsLocation_ForcedEntry_Location_Fudge
			mpOffset.vLoc 		= <<1325.6871, -1554.9233, 54.4206>>
			mpOffset.vRot		= <<-4.8858, -0.0107, -44.2220>>
			mpOffset.iFov 		= 22
			
			mpOffset2.vLoc 		= <<1325.6871, -1554.9233, 54.4206>>
			mpOffset2.vRot		= <<-3.0093, -0.0107, -51.0018>>
			mpOffset2.iFov 		= 22
		BREAK
		CASE GangOpsLocation_ForcedEntry_Location_Magellan//1
			mpOffset.vLoc 		= <<-1143.8638, -1462.4069, 7.4066>>
			mpOffset.vRot		= <<11.8616, -0.0733, -64.5087>>
			mpOffset.iFov 		= 23
			
			mpOffset2.vLoc 		= <<-1143.8638, -1462.4069, 7.4066>>
			mpOffset2.vRot		= <<14.9876, -0.0733, -50.0625>>
			mpOffset2.iFov 		= 23
		BREAK
		CASE GangOpsLocation_ForcedEntry_Location_Forum//2
			mpOffset.vLoc 		= <<-157.4229, -1641.9858, 37.1793>>
			mpOffset.vRot		= <<2.6399, 0.0000, 43.6441>>
			mpOffset.iFov 		= 10
			
			mpOffset2.vLoc 		= <<-157.4158, -1642.0028, 37.1784>>
			mpOffset2.vRot		= <<2.9699, -0.0000, 48.4484>>
			mpOffset2.iFov 		= 10
		BREAK
		CASE GangOpsLocation_ForcedEntry_Location_Roy_Lowenstein//3
			mpOffset.vLoc 		= <<456.9140, -1582.1851, 32.8571>>
			mpOffset.vRot		= <<3.5541, -0.0000, 35.6120>>
			mpOffset.iFov 		= 16
			
			mpOffset2.vLoc 		= <<456.9140, -1582.1851, 32.8571>>
			mpOffset2.vRot		= <<5.3077, -0.0000, 50.3660>>
			mpOffset2.iFov 		= 16
		BREAK
		CASE GangOpsLocation_ForcedEntry_Location_Eastbourne//4
			mpOffset.vLoc 		= <<-366.1773, 57.9783, 54.6076>>
			mpOffset.vRot		= <<1.9691, 0.0000, -103.8855>>
			mpOffset.iFov 		= 13
			
			mpOffset2.vLoc 		= <<-366.1773, 57.9783, 54.6076>>
			mpOffset2.vRot		= <<1.9691, 0.0000, -92.9304>>
			mpOffset2.iFov 		= 13
		BREAK
		
		CASE GangOpsLocation_ForcedEntry_Location_V_Magellan//5
			mpOffset.vLoc 		= <<-1270.3506, -1301.9675, 8.3237>>
			mpOffset.vRot		= <<2.7386, -0.0000, 10.7706>>
			mpOffset.iFov 		= 11
			
			mpOffset2.vLoc 		= <<-1270.3506, -1301.9675, 8.3237>>
			mpOffset2.vRot		= <<3.6780, 0.0000, 17.9281>>
			mpOffset2.iFov 		= 11
		BREAK
		CASE GangOpsLocation_ForcedEntry_Location_Alta//6
			mpOffset.vLoc 		= <<201.5236, -97.4889, 73.3964>>
			mpOffset.vRot		= <<4.3111, 0.0000, -119.5115>>
			mpOffset.iFov 		= 17
			
			mpOffset2.vLoc 		= <<201.5236, -97.4889, 73.3964>>
			mpOffset2.vRot		= <<4.3111, -0.0000, -109.8280>>
			mpOffset2.iFov 		= 17
		BREAK
		CASE GangOpsLocation_ForcedEntry_Location_Forum_Drive//7
			mpOffset.vLoc 		= <<-151.1386, -1684.4850, 36.1779>>
			mpOffset.vRot		= <<4.1526, -0.0000, -138.5295>>
			mpOffset.iFov 		= 15
			
			mpOffset2.vLoc 		= <<-151.1386, -1684.4850, 36.1779>>
			mpOffset2.vRot		= <<4.1526, -0.0000, -133.5285>>
			mpOffset2.iFov 		= 15
		BREAK
		CASE GangOpsLocation_ForcedEntry_Location_Spanish//8
			mpOffset.vLoc 		= <<-358.5946, 93.8106, 70.1609>>
			mpOffset.vRot		= <<13.4041, 0.0128, -164.4424>>
			mpOffset.iFov 		= 22
			
			mpOffset2.vLoc 		= <<-358.5946, 93.8106, 70.1609>>
			mpOffset2.vRot		= <<13.4041, 0.0128, -179.0614>>
			mpOffset2.iFov 		= 22
		BREAK
		CASE GangOpsLocation_ForcedEntry_Location_Tahitian//9
			mpOffset.vLoc 		= <<-23.7178, -1546.1914, 33.5607>>
			mpOffset.vRot		= <<8.3464, 0.0000, 65.5155>>
			mpOffset.iFov 		= 13
			
			mpOffset2.vLoc 		= <<-23.7178, -1546.1914, 33.5607>>
			mpOffset2.vRot		= <<8.8395, 0.0000, 53.8180>>
			mpOffset2.iFov 		= 13
		BREAK
		
		CASE GangOpsLocation_BurglaryJob_ProcopioDrive
			IF SHOULD_DO_BARGE_ANIM_FOR_BURGLARY_JOB()
				mpOffset.vLoc 		= <<-409.7236, 6312.2725, 29.2002>>
				mpOffset.vRot		= <<0.4619, -0.0000, -63.3747>>
				mpOffset.iFov 		= 19
				
				mpOffset2.vLoc 		= <<-409.7236, 6312.2725, 29.2002>>
				mpOffset2.vRot		= <<0.4619, -0.0000, -48.7636>>
				mpOffset2.iFov 		= 19
			ELSE
				mpOffset.vLoc 		= <<-408.9158, 6312.9780, 29.4694>>
				mpOffset.vRot		= <<-9.8704, -0.0000, -53.0851>>
				mpOffset.iFov 		= 29
				
				mpOffset2.vLoc 		= <<-408.9158, 6312.9780, 29.4694>>
				mpOffset2.vRot		= <<-8.8228, -0.0000, -44.8101>>
				mpOffset2.iFov 		= 29
			ENDIF
		BREAK
		CASE GangOpsLocation_BurglaryJob_SouthMoMilton
			IF SHOULD_DO_BARGE_ANIM_FOR_BURGLARY_JOB()
				mpOffset.vLoc 		= <<-1091.4232, 549.9750, 104.0071>>
				mpOffset.vRot		= <<-1.5402, -0.0000, -162.9021>>
				mpOffset.iFov 		= 25
				
				mpOffset2.vLoc 		= <<-1091.4232, 549.9750, 104.0071>>
				mpOffset2.vRot		= <<-1.5402, -0.0000, -140.9852>>
				mpOffset2.iFov 		= 25
			ELSE
				mpOffset.vLoc 		= <<-1091.1614, 549.7019, 103.9176>>
				mpOffset.vRot		= <<-3.0083, -0.0000, -151.4711>>
				mpOffset.iFov 		= 40
				
				mpOffset2.vLoc 		= <<-1091.1614, 549.7019, 103.9176>>
				mpOffset2.vRot		= <<-3.0083, -0.0000, -139.3221>>
				mpOffset2.iFov 		= 40
			ENDIF
		BREAK
		CASE GangOpsLocation_BurglaryJob_MirrorPark
			IF SHOULD_DO_BARGE_ANIM_FOR_BURGLARY_JOB()
				mpOffset.vLoc 		= <<1200.5435, -574.8756, 69.3737>>
				mpOffset.vRot		= <<-0.1827, 0.0000, -169.4189>>
				mpOffset.iFov 		= 53
				
				mpOffset2.vLoc 		= <<1200.5435, -574.8756, 69.3737>>
				mpOffset2.vRot		= <<-0.1827, -0.0000, -161.8604>>
				mpOffset2.iFov 		= 53
			ELSE
				mpOffset.vLoc 		= <<1200.1962, -574.8662, 69.3967>>
				mpOffset.vRot		= <<-0.0448, -0.0000, -158.6262>>
				mpOffset.iFov 		= 49
				
				mpOffset2.vLoc 		= <<1200.1962, -574.8662, 69.3967>>
				mpOffset2.vRot		= <<-0.0448, -0.0000, -153.1391>>
				mpOffset2.iFov 		= 49
			ENDIF
		BREAK
		CASE GangOpsLocation_BurglaryJob_ImaginationCourt
			IF SHOULD_DO_BARGE_ANIM_FOR_BURGLARY_JOB()
				mpOffset.vLoc 		= <<-1123.0708, -1088.0138, 2.9606>>
				mpOffset.vRot		= <<-1.1454, -0.0000, 179.3426>>
				mpOffset.iFov 		= 31
				
				mpOffset2.vLoc 		= <<-1123.0708, -1088.0138, 2.9606>>
				mpOffset2.vRot		= <<-1.1454, 0.0000, -154.6447>>
				mpOffset2.iFov 		= 31
			ELSE
				mpOffset.vLoc 		= <<-1123.3798, -1087.9028, 2.8350>>
				mpOffset.vRot		= <<0.5048, -0.0000, -159.7505>>
				mpOffset.iFov 		= 24
				
				mpOffset2.vLoc 		= <<-1123.3798, -1087.9028, 2.8350>>
				mpOffset2.vRot		= <<0.5048, -0.0000, -151.1593>>
				mpOffset2.iFov 		= 24
			ENDIF
		BREAK
		CASE GangOpsLocation_BurglaryJob_DidionDrive
			IF SHOULD_DO_BARGE_ANIM_FOR_BURGLARY_JOB()
				mpOffset.vLoc 		= <<-369.3828, 343.5820, 110.2231>>
				mpOffset.vRot		= <<-0.0008, 0.0000, 79.6180>>
				mpOffset.iFov 		= 20
				
				mpOffset2.vLoc 		= <<-369.3828, 343.5820, 110.2231>>
				mpOffset2.vRot		= <<-0.0008, 0.0000, 97.8871>>
				mpOffset2.iFov 		= 20
			ELSE
				mpOffset.vLoc 		= <<-368.9140, 343.6969, 110.2432>>
				mpOffset.vRot		= <<-2.2853, 0.0000, 93.5779>>
				mpOffset.iFov 		= 16
				
				mpOffset2.vLoc 		= <<-368.9140, 343.6969, 110.2432>>
				mpOffset2.vRot		= <<-2.2853, -0.0000, 99.7526>>
				mpOffset2.iFov 		= 16
			ENDIF
		BREAK
		CASE GangOpsLocation_BurglaryJob_KimbleHill
			IF SHOULD_DO_BARGE_ANIM_FOR_BURGLARY_JOB()
				mpOffset.vLoc 		= <<-291.0020, 601.6849, 181.7908>>
				mpOffset.vRot		= <<1.1429, 0.0000, 86.1419>>
				mpOffset.iFov 		= 18
				
				mpOffset2.vLoc 		= <<-291.0020, 601.6849, 181.7908>>
				mpOffset2.vRot		= <<1.1429, 0.0000, 104.6526>>
				mpOffset2.iFov 		= 18
			ELSE
				mpOffset.vLoc 		= <<-290.9635, 601.6606, 181.8785>>
				mpOffset.vRot		= <<-1.9783, 0.0000, 97.3345>>
				mpOffset.iFov 		= 17
				
				mpOffset2.vLoc 		= <<-290.9635, 601.6606, 181.8785>>
				mpOffset2.vRot		= <<-3.0795, -0.0000, 104.0771>>
				mpOffset2.iFov 		= 17
			ENDIF
		BREAK
		CASE GangOpsLocation_BurglaryJob_BridgeStreet
			IF SHOULD_DO_BARGE_ANIM_FOR_BURGLARY_JOB()
				mpOffset.vLoc 		= <<1048.8633, -498.7066, 64.5468>>
				mpOffset.vRot		= <<-0.6052, -0.0000, 61.1503>>
				mpOffset.iFov 		= 18
				
				mpOffset2.vLoc 		= <<1048.8633, -498.7066, 64.5468>>
				mpOffset2.vRot		= <<-0.6052, 0.0000, 80.4423>>
				mpOffset2.iFov 		= 18
			ELSE
				mpOffset.vLoc 		= <<1049.0571, -498.8536, 64.4368>>
				mpOffset.vRot		= <<-0.3892, -0.0000, 73.8669>>
				mpOffset.iFov 		= 16
				
				mpOffset2.vLoc 		= <<1049.0571, -498.8536, 64.4368>>
				mpOffset2.vRot		= <<-0.3892, -0.0000, 78.2157>>
				mpOffset2.iFov 		= 16
			ENDIF
		BREAK
		CASE GangOpsLocation_BurglaryJob_NikolaAvenue
			IF SHOULD_DO_BARGE_ANIM_FOR_BURGLARY_JOB()
				mpOffset.vLoc 		= <<922.2167, -569.5790, 58.7916>>
				mpOffset.vRot		= <<-1.4521, 0.0000, 114.0588>>
				mpOffset.iFov 		= 23
				
				mpOffset2.vLoc 		= <<922.2167, -569.5790, 58.7916>>
				mpOffset2.vRot		= <<-1.4521, 0.0000, 94.1429>>
				mpOffset2.iFov 		= 23
			ELSE
				mpOffset.vLoc 		= <<922.1313, -569.5632, 58.7227>>
				mpOffset.vRot		= <<-1.8782, -0.0000, 100.7116>>
				mpOffset.iFov 		= 20
				
				mpOffset2.vLoc 		= <<922.1313, -569.5632, 58.7227>>
				mpOffset2.vRot		= <<-1.8782, 0.0000, 92.4014>>
				mpOffset2.iFov 		= 20
			ENDIF
		BREAK
		CASE GangOpsLocation_BurglaryJob_NorthSheldonAvenue
			IF SHOULD_DO_BARGE_ANIM_FOR_BURGLARY_JOB()
				mpOffset.vLoc 		= <<-970.8979, 754.8649, 176.7703>>
				mpOffset.vRot		= <<-1.6605, 0.0000, 134.0483>>
				mpOffset.iFov 		= 18
				
				mpOffset2.vLoc 		= <<-970.8979, 754.8649, 176.7703>>
				mpOffset2.vRot		= <<-1.6605, -0.0000, 151.5788>>
				mpOffset2.iFov 		= 18
			ELSE
				mpOffset.vLoc 		= <<-970.5284, 755.5592, 176.6534>>
				mpOffset.vRot		= <<-1.5352, 0.0000, 148.7186>>
				mpOffset.iFov 		= 14
				
				mpOffset2.vLoc 		= <<-970.5284, 755.5592, 176.6534>>
				mpOffset2.vRot		= <<-1.5352, -0.0000, 151.3079>>
				mpOffset2.iFov 		= 14
			ENDIF
		BREAK
		CASE GangOpsLocation_BurglaryJob_InesenoRoad
			IF SHOULD_DO_BARGE_ANIM_FOR_BURGLARY_JOB()
				mpOffset.vLoc 		= <<-3093.3887, 351.9139, 7.8587>>
				mpOffset.vRot		= <<0.1677, -0.0000, -173.0190>>
				mpOffset.iFov 		= 19
				
				mpOffset2.vLoc 		= <<-3093.3887, 351.9139, 7.8587>>
				mpOffset2.vRot		= <<0.1677, -0.0000, 167.0885>>
				mpOffset2.iFov 		= 19
			ELSE
				mpOffset.vLoc 		= <<-3093.2158, 352.2303, 7.7087>>
				mpOffset.vRot		= <<1.2273, -0.0000, 175.2593>>
				mpOffset.iFov 		= 16
				
				mpOffset2.vLoc 		= <<-3093.2158, 352.2303, 7.7087>>
				mpOffset2.vRot		= <<1.2273, -0.0000, 167.5378>>
				mpOffset2.iFov 		= 16
			ENDIF
		BREAK
		
		CASE GangOpsLocation_CarCollector_SteeleWay
			mpOffset.vLoc 		= <<-1046.1517, 224.3581, 64.2445>>
			mpOffset.vRot		= <<-29.8166, -0.0000, -27.3404>>
			mpOffset.iFov 		= 50
		BREAK
		CASE GangOpsLocation_CarCollector_AmericanoWay
			mpOffset.vLoc 		= <<-1557.2146, 18.3557, 58.9537>>
			mpOffset.vRot		= <<-21.2153, -0.0000, 133.5246>>
			mpOffset.iFov 		= 50
		BREAK
		CASE GangOpsLocation_CarCollector_AceJonesDrive
			mpOffset.vLoc 		= <<-1555.0699, 430.7594, 109.4188>>
			mpOffset.vRot		= <<-15.9143, 0.0000, 68.0333>>
			mpOffset.iFov 		= 50
		BREAK
		CASE GangOpsLocation_CarCollector_Richman
			mpOffset.vLoc 		= <<-1746.9810, 368.5312, 89.7454>>
			mpOffset.vRot		= <<-14.1393, -0.0000, -82.3350>>
			mpOffset.iFov 		= 50
		BREAK
		CASE GangOpsLocation_CarCollector_NorthRockford_1
			mpOffset.vLoc 		= <<-1869.6735, 192.1267, 84.3556>>
			mpOffset.vRot		= <<-20.0839, -0.0000, -87.0091>>
			mpOffset.iFov 		= 50
		BREAK
		CASE GangOpsLocation_CarCollector_DunstableLane_1
			mpOffset.vLoc 		= <<-1936.4955, 364.6855, 93.9809>>
			mpOffset.vRot		= <<-21.2573, -0.0000, -25.0865>>
			mpOffset.iFov 		= 50
		BREAK
		CASE GangOpsLocation_CarCollector_DunstableLane_2
			mpOffset.vLoc 		= <<-888.4464, 361.7714, 85.1534>>
			mpOffset.vRot		= <<-18.8621, 0.0000, 145.4278>>
			mpOffset.iFov 		= 50
		BREAK
		CASE GangOpsLocation_CarCollector_NorthRockford_2
			mpOffset.vLoc 		= <<-1999.1984, 295.7242, 91.7588>>
			mpOffset.vRot		= <<-14.7781, 0.0000, 66.1764>>
			mpOffset.iFov 		= 50
		BREAK
		CASE GangOpsLocation_CarCollector_CaesarsPlace
			mpOffset.vLoc 		= <<-872.8923, 49.8119, 48.7342>>
			mpOffset.vRot		= <<-12.5648, -0.0000, -15.2915>>
			mpOffset.iFov 		= 50
		BREAK
		CASE GangOpsLocation_CarCollector_HillcrestAvenue
			mpOffset.vLoc 		= <<-713.2217, 649.6705, 155.1804>>
			mpOffset.vRot		= <<-17.1832, -0.0000, 140.0646>>
			mpOffset.iFov 		= 50
		BREAK
	ENDSWITCH
ENDPROC

FUNC STRING GET_ENTRY_CUTSCENE_ANIM_DICT()
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_BURGLARY_JOB			
			IF SHOULD_DO_BARGE_ANIM_FOR_BURGLARY_JOB()
				RETURN "anim@apt_trans@hinge_l_action"
			ENDIF
			RETURN "anim@apt_trans@hinge_l_stealth"
		BREAK
		CASE GOV_FORCED_ENTRY			RETURN "anim@apt_trans@hinge_l_action"
		CASE GOV_CAR_COLLECTOR			RETURN "anim@apt_trans@garage"
	ENDSWITCH
	RETURN "anim@apt_trans@hinge_r"
ENDFUNC

FUNC STRING GET_ENTRY_CUTSCENE_ANIM_NAME()
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_BURGLARY_JOB			RETURN "player_exit"
		CASE GOV_FORCED_ENTRY			RETURN "player_exit"
		CASE GOV_CAR_COLLECTOR			RETURN "gar_open_1_left"
	ENDSWITCH
	RETURN "ext_player"
ENDFUNC

FUNC FLOAT GET_ANIM_START_PHASE()
	SWITCH GET_GANGOPS_LOCATION()
		CASE GangOpsLocation_ForcedEntry_Location_Fudge
			RETURN 0.025
		CASE GangOpsLocation_ForcedEntry_Location_Magellan
			RETURN 0.0
		CASE GangOpsLocation_ForcedEntry_Location_Forum
			RETURN 0.0
		CASE GangOpsLocation_ForcedEntry_Location_Roy_Lowenstein
			RETURN 0.0
		CASE GangOpsLocation_ForcedEntry_Location_Eastbourne
			RETURN 0.0
		CASE GangOpsLocation_ForcedEntry_Location_V_Magellan
			RETURN 0.0
		CASE GangOpsLocation_ForcedEntry_Location_Alta
			RETURN 0.025
		CASE GangOpsLocation_ForcedEntry_Location_Forum_Drive
			RETURN 0.025
		CASE GangOpsLocation_ForcedEntry_Location_Spanish
			RETURN 0.0
		CASE GangOpsLocation_ForcedEntry_Location_Tahitian
			RETURN 0.025
			
		CASE GangOpsLocation_BurglaryJob_ProcopioDrive	
		CASE GangOpsLocation_BurglaryJob_SouthMoMilton
		CASE GangOpsLocation_BurglaryJob_MirrorPark
		CASE GangOpsLocation_BurglaryJob_ImaginationCourt
		CASE GangOpsLocation_BurglaryJob_DidionDrive
		CASE GangOpsLocation_BurglaryJob_KimbleHill
		CASE GangOpsLocation_BurglaryJob_BridgeStreet
		CASE GangOpsLocation_BurglaryJob_NikolaAvenue
		CASE GangOpsLocation_BurglaryJob_NorthSheldonAvenue
		CASE GangOpsLocation_BurglaryJob_InesenoRoad
			IF SHOULD_DO_BARGE_ANIM_FOR_BURGLARY_JOB()
				RETURN 0.0
			ENDIF
			RETURN 0.139
	ENDSWITCH
	RETURN 0.0 
ENDFUNC

FUNC FLOAT GET_ANIM_SCENE_RATE()
	SWITCH GET_GANGOPS_LOCATION()
		CASE GangOpsLocation_ForcedEntry_Location_Fudge
			RETURN 0.6
		CASE GangOpsLocation_ForcedEntry_Location_Magellan
			RETURN 0.5
		CASE GangOpsLocation_ForcedEntry_Location_Forum
			RETURN 0.5
		CASE GangOpsLocation_ForcedEntry_Location_Roy_Lowenstein
			RETURN 0.5
		CASE GangOpsLocation_ForcedEntry_Location_Eastbourne
			RETURN 0.5
		CASE GangOpsLocation_ForcedEntry_Location_V_Magellan
			RETURN 0.7
		CASE GangOpsLocation_ForcedEntry_Location_Alta
			RETURN 0.7
		CASE GangOpsLocation_ForcedEntry_Location_Forum_Drive
			RETURN 0.7
		CASE GangOpsLocation_ForcedEntry_Location_Spanish
			RETURN 0.7
		CASE GangOpsLocation_ForcedEntry_Location_Tahitian
			RETURN 0.8
			
		CASE GangOpsLocation_BurglaryJob_ProcopioDrive
		CASE GangOpsLocation_BurglaryJob_SouthMoMilton
		CASE GangOpsLocation_BurglaryJob_MirrorPark
		CASE GangOpsLocation_BurglaryJob_ImaginationCourt
		CASE GangOpsLocation_BurglaryJob_DidionDrive
		CASE GangOpsLocation_BurglaryJob_KimbleHill
		CASE GangOpsLocation_BurglaryJob_BridgeStreet
		CASE GangOpsLocation_BurglaryJob_NikolaAvenue
		CASE GangOpsLocation_BurglaryJob_NorthSheldonAvenue
		CASE GangOpsLocation_BurglaryJob_InesenoRoad
			IF SHOULD_DO_BARGE_ANIM_FOR_BURGLARY_JOB()
				RETURN 0.8
			ENDIF
		BREAK
	ENDSWITCH
	RETURN 1.0 
ENDFUNC

FUNC BOOL GET_ENTRY_CUTSCENE_DATA()
	
	IF NOT IS_BIT_SET(entryCutData.iBS,ENTRY_CS_BS_GET_DATA)

		entryCutData.animDictionary = GET_ENTRY_CUTSCENE_ANIM_DICT()
//		entryCutData.animDictionary = "anim@apt_trans@hinge_l"

		entryCutData.animName[SYNC_SCENE_PED_A] = GET_ENTRY_CUTSCENE_ANIM_NAME()
		entryCutData.animName[SYNC_SCENE_DOOR] = "ext_door"
		
		tempCamOffset.vLoc = <<0,0,0>>
		tempCamOffset.vRot = <<0,0,0>>
		tempCamOffset.iFov = 0
		
		tempCamOffset2.vLoc = <<0,0,0>>
		tempCamOffset2.vRot = <<0,0,0>>
		tempCamOffset2.iFov = 0
		//CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: TRANS: mpProperties[iCurrentPropertyID].iBuildingID: ", mpProperties[iCurrentPropertyID].iBuildingID)
		GET_CAM_DETAILS(tempCamOffset, tempCamOffset2) //WARPING WORK CDM 176870
		entryCutData.vCamLoc[0] = tempCamOffset.vLoc
		entryCutData.vCamRot[0] = tempCamOffset.vRot
		entryCutData.fCamFOV[0] = TO_FLOAT(tempCamOffset.iFov)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: TRANS: vCameraPos", tempCamOffset.vLoc)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: TRANS: vCameraRot", tempCamOffset.vRot)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: TRANS: fCameraFov", tempCamOffset.iFov)
		
		entryCutData.vCamLoc[1] = tempCamOffset2.vLoc
		entryCutData.vCamRot[1] = tempCamOffset2.vRot
		entryCutData.fCamFOV[1] = TO_FLOAT(tempCamOffset2.iFov)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: TRANS: vCameraPos2", tempCamOffset2.vLoc)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: TRANS: vCameraRot2", tempCamOffset2.vRot)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: TRANS: fCameraFov2", tempCamOffset2.iFov)
		
		GET_ENTER_SCENE_DETAILS(tempPropOffset)
		entryCutData.vSyncSceneLoc = tempPropOffset.vLoc
		entryCutData.vSyncSceneRot = tempPropOffset.vRot
		CDEBUG1LN(DEBUG_SAFEHOUSE, "Entry enter scene location", entryCutData.vSyncSceneLoc)

		VECTOR vPlayer = GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<0.0, 0.0, -10.0>>
		
		IF NOT IS_PLAYER_FEMALE()
			entryCutData.playerClone = CREATE_PED(PEDTYPE_CIVMALE, GET_ENTITY_MODEL(PLAYER_PED_ID()), vPlayer, GET_ENTITY_HEADING(PLAYER_PED_ID()), FALSE, FALSE)
			PRINTLN("GET_ENTRY_CUTSCENE_DATA, PEDTYPE_CIVMALE at ", vPlayer)
		ELSE
			entryCutData.playerClone = CREATE_PED(PEDTYPE_CIVFEMALE, GET_ENTITY_MODEL(PLAYER_PED_ID()), vPlayer, GET_ENTITY_HEADING(PLAYER_PED_ID()), FALSE, FALSE)
			PRINTLN("GET_ENTRY_CUTSCENE_DATA, PEDTYPE_CIVFEMALE at ", vPlayer)
		ENDIF
		CLONE_PED_TO_TARGET(PLAYER_PED_ID(), entryCutData.playerClone)
		UPDATE_MC_EMBLEM(GET_PLAYER_INDEX(), entryCutData.playerClone)
		FREEZE_ENTITY_POSITION(entryCutData.playerClone,TRUE)
		SET_ENTITY_PROOFS(entryCutData.playerClone,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
		entryCutData.fSyncedSceneCompleteStage = 0.6

		SET_BIT(entryCutData.iBS,ENTRY_CS_BS_GET_DATA)
	ENDIF
	RETURN TRUE
ENDFUNC

PROC SET_ENTRY_CS_STAGE(INT iStage)
	#IF IS_DEBUG_BUILD
	SWITCH iStage
		CASE ENTRY_CS_STAGE_INIT_DATA
			CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_ENTRY_CS_STAGE - ENTRY_CS_STAGE_INIT_DATA")
		BREAK
		CASE ENTRY_CS_STAGE_WAIT_FOR_MODELS
			CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_ENTRY_CS_STAGE - ENTRY_CS_STAGE_WAIT_FOR_MODELS")
		BREAK
		CASE ENTRY_CS_STAGE_TRIGGER_CUT
			CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_ENTRY_CS_STAGE - ENTRY_CS_STAGE_TRIGGER_CUT")
		BREAK
		CASE ENTRY_CS_STAGE_RUN_CUT
			CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_ENTRY_CS_STAGE - ENTRY_CS_STAGE_RUN_CUT")
		BREAK
		CASE ENTRY_CS_STAGE_RUN_SECOND_CUT
			CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_ENTRY_CS_STAGE - ENTRY_CS_STAGE_RUN_SECOND_CUT")
		BREAK
		CASE ENTRY_CS_STAGE_HANDLE_PLAYER_PED
			CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_ENTRY_CS_STAGE - ENTRY_CS_STAGE_HANDLE_PLAYER_PED")
		BREAK
	ENDSWITCH
	#ENDIF
	entryCutData.iStage = iStage
ENDPROC

PROC KILL_CUSTCENE_CAMS()
	IF DOES_CAM_EXIST(entryCutData.cameras[0])
		SET_CAM_ACTIVE(entryCutData.cameras[0], FALSE)
		STOP_CAM_SHAKING(entryCutData.cameras[0], TRUE)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		DESTROY_CAM(entryCutData.cameras[0])
	ENDIF	
ENDPROC

PROC CLEANUP_INTERIOR_ENTRY_CUTSCENE()
	IF IS_LOCAL_BIT_SET(eLOCALBITSET_NEED_ENTRYCUTSCENE_CLEANUP)
		SET_ENTRY_CS_STAGE(ENTRY_CS_STAGE_INIT_DATA)
		RESET_NET_TIMER(entryCutData.timer)
		entryCutData.iBS = 0
		KILL_CUSTCENE_CAMS()
		CLEANUP_MP_CUTSCENE(TRUE, TRUE)
		CLEAR_LOCAL_BIT(eLOCALBITSET_NEED_ENTRYCUTSCENE_CLEANUP)
		IF DOES_ENTITY_EXIST(entryCutData.playerClone)
			DELETE_PED(entryCutData.playerClone)
			PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] [ENTRY_CUT] - CLEANUP_INTERIOR_ENTRY_CUTSCENE, DELETE_PED ")
		ENDIF
		PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] [ENTRY_CUT] - CLEANUP_INTERIOR_ENTRY_CUTSCENE ")
	ENDIF
ENDPROC

FUNC BOOL INTERP_ENTRY_CAM()

	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_FORCED_ENTRY			RETURN TRUE
		CASE GOV_BURGLARY_JOB			RETURN TRUE
	ENDSWITCH	
	
//	SWITCH GET_GANGOPS_LOCATION()
//		CASE GangOpsLocation_ForcedEntry_Location_Fudge 	RETURN TRUE
//	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_END_PHASE()
	SWITCH GET_GANGOPS_LOCATION()
		CASE GangOpsLocation_ForcedEntry_Location_Fudge 			
		CASE GangOpsLocation_ForcedEntry_Location_Magellan			
		CASE GangOpsLocation_ForcedEntry_Location_Forum				
		CASE GangOpsLocation_ForcedEntry_Location_Roy_Lowenstein	
		CASE GangOpsLocation_ForcedEntry_Location_Eastbourne		
		CASE GangOpsLocation_ForcedEntry_Location_V_Magellan		
		CASE GangOpsLocation_ForcedEntry_Location_Alta				
		CASE GangOpsLocation_ForcedEntry_Location_Forum_Drive		
		CASE GangOpsLocation_ForcedEntry_Location_Spanish			
		CASE GangOpsLocation_ForcedEntry_Location_Tahitian			
			RETURN 0.5
		
		CASE GangOpsLocation_CarCollector_SteeleWay					
		CASE GangOpsLocation_CarCollector_AmericanoWay				
		CASE GangOpsLocation_CarCollector_AceJonesDrive				
		CASE GangOpsLocation_CarCollector_Richman					
		CASE GangOpsLocation_CarCollector_NorthRockford_1			
		CASE GangOpsLocation_CarCollector_DunstableLane_1			
		CASE GangOpsLocation_CarCollector_DunstableLane_2			
		CASE GangOpsLocation_CarCollector_NorthRockford_2			
		CASE GangOpsLocation_CarCollector_CaesarsPlace				
		CASE GangOpsLocation_CarCollector_HillcrestAvenue		
			RETURN 0.4
			
		CASE GangOpsLocation_BurglaryJob_ProcopioDrive	
		CASE GangOpsLocation_BurglaryJob_SouthMoMilton
		CASE GangOpsLocation_BurglaryJob_MirrorPark
		CASE GangOpsLocation_BurglaryJob_ImaginationCourt
		CASE GangOpsLocation_BurglaryJob_DidionDrive
		CASE GangOpsLocation_BurglaryJob_KimbleHill
		CASE GangOpsLocation_BurglaryJob_BridgeStreet
		CASE GangOpsLocation_BurglaryJob_NikolaAvenue
		CASE GangOpsLocation_BurglaryJob_NorthSheldonAvenue
		CASE GangOpsLocation_BurglaryJob_InesenoRoad
			IF SHOULD_DO_BARGE_ANIM_FOR_BURGLARY_JOB()
				RETURN 0.5
			ENDIF
			RETURN 0.55
		
	ENDSWITCH

	RETURN entryCutData.fSyncedSceneCompleteStage
ENDFUNC

FUNC FLOAT GET_SHAKE_PHASE()
	SWITCH GET_GANGOPS_LOCATION()
		CASE GangOpsLocation_ForcedEntry_Location_Fudge 			RETURN 0.14
		CASE GangOpsLocation_ForcedEntry_Location_Magellan			RETURN 0.164
		CASE GangOpsLocation_ForcedEntry_Location_Forum				RETURN 0.18
		CASE GangOpsLocation_ForcedEntry_Location_Roy_Lowenstein	RETURN 0.18
		CASE GangOpsLocation_ForcedEntry_Location_Eastbourne		RETURN 0.16
		CASE GangOpsLocation_ForcedEntry_Location_V_Magellan		RETURN 0.17
		CASE GangOpsLocation_ForcedEntry_Location_Alta				RETURN 0.15
		CASE GangOpsLocation_ForcedEntry_Location_Forum_Drive		RETURN 0.18
		CASE GangOpsLocation_ForcedEntry_Location_Spanish			RETURN 0.16
		CASE GangOpsLocation_ForcedEntry_Location_Tahitian			RETURN 0.18
		
		CASE GangOpsLocation_BurglaryJob_ProcopioDrive				
		CASE GangOpsLocation_BurglaryJob_SouthMoMilton
		CASE GangOpsLocation_BurglaryJob_MirrorPark
		CASE GangOpsLocation_BurglaryJob_ImaginationCourt
		CASE GangOpsLocation_BurglaryJob_DidionDrive
		CASE GangOpsLocation_BurglaryJob_KimbleHill
		CASE GangOpsLocation_BurglaryJob_BridgeStreet
		CASE GangOpsLocation_BurglaryJob_NikolaAvenue
		CASE GangOpsLocation_BurglaryJob_NorthSheldonAvenue
		CASE GangOpsLocation_BurglaryJob_InesenoRoad
			IF SHOULD_DO_BARGE_ANIM_FOR_BURGLARY_JOB()
				RETURN 0.197
			ENDIF
			RETURN 0.254
	ENDSWITCH

	RETURN -1.0
ENDFUNC

FUNC STRING GET_SHAKE_TYPE_STRING()
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_BURGLARY_JOB
			IF SHOULD_DO_BARGE_ANIM_FOR_BURGLARY_JOB()
				RETURN "small_explosion_shake"
			ENDIF
			RETURN "HAND_SHAKE"
	ENDSWITCH
	RETURN "small_explosion_shake"
ENDFUNC

FUNC FLOAT GET_SHAKE_AMPLITUDE()
	SWITCH GET_GANGOPS_VARIATION()
		CASE GOV_BURGLARY_JOB
			IF SHOULD_DO_BARGE_ANIM_FOR_BURGLARY_JOB()
				RETURN 0.05
			ENDIF
			RETURN 0.5
	ENDSWITCH
	RETURN 0.05
ENDFUNC

FUNC BOOL RUN_INTERIOR_ENTRY_CUTSCENE()

	IF NOT IS_GAMEPLAY_CAM_RENDERING()
		HIDE_HUD_AND_RADAR_THIS_FRAME()
	ENDIF	
	
	BOOL bStartActive = TRUE

	SWITCH entryCutData.iStage
		
		CASE ENTRY_CS_STAGE_INIT_DATA
			IF GET_ENTRY_CUTSCENE_DATA()
				REQUEST_ANIM_DICT(entryCutData.animDictionary)
				IF HAS_ANIM_DICT_LOADED(entryCutData.animDictionary)
					SET_LOCAL_BIT(eLOCALBITSET_NEED_ENTRYCUTSCENE_CLEANUP)
					REINIT_NET_TIMER(entryCutData.timer, TRUE)
					SET_ENTRY_CS_STAGE(ENTRY_CS_STAGE_WAIT_FOR_MODELS)
				ENDIF
			ENDIF
		BREAK
		
		CASE ENTRY_CS_STAGE_WAIT_FOR_MODELS
			IF NOT HAS_NET_TIMER_EXPIRED(entryCutData.timer,5000,TRUE)
				IF DOES_ENTITY_EXIST(entryCutData.playerClone)
					IF NOT IS_PED_INJURED(entryCutData.playerClone)
						IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(entryCutData.playerClone)
							PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - HAVE_ALL_STREAMING_REQUESTS_COMPLETED ")
							RETURN FALSE
						ENDIF
						IF NOT HAS_PED_HEAD_BLEND_FINISHED(entryCutData.playerClone)
							PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - HAS_PED_HEAD_BLEND_FINISHED ")
							RETURN FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			SET_ENTRY_CS_STAGE(ENTRY_CS_STAGE_HANDLE_PLAYER_PED)
		BREAK
		
		CASE ENTRY_CS_STAGE_HANDLE_PLAYER_PED
			
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_NO_COLLISION | NSPC_FREEZE_POSITION | NSPC_PREVENT_VISIBILITY_CHANGES | NSPC_SET_INVISIBLE)
			
			NETWORK_FADE_OUT_ENTITY(PLAYER_PED_ID(), TRUE, TRUE)
			
			PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - ENTRY_CS_STAGE_HANDLE_PLAYER_PED ")
			
			SET_BIT(entryCutData.iBS, ENTRY_CS_BS_TOOK_AWAY_CONTROL)
			SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(FALSE, TRUE)
			
			START_MP_CUTSCENE(TRUE)
			SET_BIT(entryCutData.iBS, ENTRY_CS_BS_STARTED_MP_CUTSCENE)
			SET_ENTRY_CS_STAGE(ENTRY_CS_STAGE_TRIGGER_CUT)
		FALLTHRU // url:bugstar:2167563 - Character pops / disappears right before camera cut of player entering apartment building
		
		CASE ENTRY_CS_STAGE_TRIGGER_CUT
			
			//Create Camera
			IF DOES_CAM_EXIST(entryCutData.cameras[0])
				DESTROY_CAM(entryCutData.cameras[0])
			ENDIF
			
			IF DOES_CAM_EXIST(entryCutData.cameras[1])
				DESTROY_CAM(entryCutData.cameras[1])
			ENDIF

			SET_BIT(entryCutData.iBS,ENTRY_CS_BS_APT_DOOR_MODEL_HIDE)
			
			IF INTERP_ENTRY_CAM()
				bStartActive = FALSE
			ENDIF

			entryCutData.cameras[0] = CREATE_CAMERA(CAMTYPE_SCRIPTED, bStartActive)
				
			SET_CAM_PARAMS(entryCutData.cameras[0],
							entryCutData.vCamLoc[0],
							entryCutData.vCamRot[0],
							entryCutData.fCamFOV[0])
				
			SET_CAM_FAR_CLIP(entryCutData.cameras[0],1000)
			SHAKE_CAM(entryCutData.cameras[0], "HAND_SHAKE", 0.25)
			
			IF INTERP_ENTRY_CAM()
				entryCutData.cameras[1] = CREATE_CAMERA(CAMTYPE_SCRIPTED, bStartActive)
					
				SET_CAM_PARAMS(entryCutData.cameras[1],
								entryCutData.vCamLoc[1],
								entryCutData.vCamRot[1],
								entryCutData.fCamFOV[1])
					
				SET_CAM_FAR_CLIP(entryCutData.cameras[1],1000)
			ENDIF

			SET_BIT(entryCutData.iBS,ENTRY_CS_BS_STARTED_MP_CUTSCENE)

			REINIT_NET_TIMER(entryCutData.timer,TRUE)	
			
			IF INTERP_ENTRY_CAM()
				SET_CAM_ACTIVE_WITH_INTERP(entryCutData.cameras[1], entryCutData.cameras[0], 600, GRAPH_TYPE_QUADRATIC_EASE_OUT, GRAPH_TYPE_QUADRATIC_EASE_OUT)
			ENDIF
			
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			FREEZE_ENTITY_POSITION(entryCutData.playerClone,FALSE)
			entryCutData.iSyncedSceneID = CREATE_SYNCHRONIZED_SCENE(entryCutData.vSyncSceneLoc,entryCutData.vSyncSceneRot)
			IF NOT IS_PED_INJURED(entryCutData.playerClone)
				//VECTOR sceneRotation = << -1.725, -0.000, 2.215 >>
				
				TASK_SYNCHRONIZED_SCENE(entryCutData.playerClone, entryCutData.iSyncedSceneID, 
													entryCutData.animDictionary, entryCutData.animName[SYNC_SCENE_PED_A], 
													INSTANT_BLEND_IN,INSTANT_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT,RBF_NONE)
			ENDIF
		
			SET_SYNCHRONIZED_SCENE_PHASE(entryCutData.iSyncedSceneID, GET_ANIM_START_PHASE())
			SET_SYNCHRONIZED_SCENE_RATE(entryCutData.iSyncedSceneID, GET_ANIM_SCENE_RATE())
			
			SET_ENTRY_CS_STAGE(ENTRY_CS_STAGE_RUN_CUT)
		BREAK
		CASE ENTRY_CS_STAGE_RUN_CUT
		
			PRINTLN("GET_SYNCHRONIZED_SCENE_PHASE(entryCutData.iSyncedSceneID) = ", GET_SYNCHRONIZED_SCENE_PHASE(entryCutData.iSyncedSceneID))
		
			IF GET_SHAKE_PHASE() != -1.0
			
				IF NOT IS_BIT_SET(entryCutData.iBS,ENTRY_CS_BS_DONE_SHAKE)
					IF GET_SYNCHRONIZED_SCENE_PHASE(entryCutData.iSyncedSceneID) > GET_SHAKE_PHASE()
						SHAKE_CAM(entryCutData.cameras[1], GET_SHAKE_TYPE_STRING(), GET_SHAKE_AMPLITUDE())
						PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - ENTRY_CS_STAGE_RUN_CUT, SHAKE_CAM,GET_SHAKE_PHASE() = ", GET_SHAKE_PHASE(), " - GET_SHAKE_TYPE_STRING() = ", GET_SHAKE_TYPE_STRING())
						SET_BIT(entryCutData.iBS,ENTRY_CS_BS_DONE_SHAKE)
					ENDIF
				ENDIF
				
//				IF GET_SYNCHRONIZED_SCENE_PHASE(entryCutData.iSyncedSceneID) > 0.38
//					PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - ENTRY_CS_STAGE_RUN_CUT, FE DONE ")
//					RETURN TRUE
//				ENDIF
			ENDIF
		
//
//			IF HAS_NET_TIMER_EXPIRED(entryCutData.timer,5000,TRUE)
//
//				PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - ENTRY_CS_STAGE_RUN_CUT - HAS_NET_TIMER_EXPIRED")
//				RETURN TRUE
//			ENDIF
			IF entryCutData.iSyncedSceneID >= -1
				IF IS_SYNCHRONIZED_SCENE_RUNNING(entryCutData.iSyncedSceneID)
					IF GET_SYNCHRONIZED_SCENE_PHASE(entryCutData.iSyncedSceneID) > GET_END_PHASE()
					
						PRINTLN("[FM_GANGOPS] - [", GET_CURRENT_VARIATION_STRING(), "] - ENTRY_CS_STAGE_RUN_CUT, GET_END_PHASE() = ", GET_END_PHASE())
		
						RETURN TRUE
					ENDIF
				ENDIF
			ELSE

				RETURN TRUE
			ENDIF
		BREAK

	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

