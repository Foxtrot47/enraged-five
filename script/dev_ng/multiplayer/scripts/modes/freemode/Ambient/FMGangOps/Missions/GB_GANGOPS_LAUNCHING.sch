USING "globals.sch"
USING "net_prints.sch"
USING "net_include.sch"
USING "commands_misc.sch"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Written by:  William Kennedy, Ryan Elliott & Martin McMillan.														//
// Date: 		08/08/2017																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "GB_GANGOPS_COORDS.sch"
USING "GB_GANGOPS_DROPOFFS.sch"

//////////////////////////////////////
/// MISSION LOCATION SPAWN CHECKS ///
//////////////////////////////////////

FUNC INT GET_MINIMUM_DISTANCE_FROM_FACILITY_FOR_FM_GANGOPS_MISSION(GANGOPS_VARIATION eVariation)
	
	SWITCH eVariation
		CASE GOV_DEAD_DROP				RETURN g_sMPTunables.IH2_PREP_SILO_CASH_DROP_GENERAL_DISTANCE_CHECK
		CASE GOV_FORCED_ENTRY			RETURN g_sMPTunables.IH2_PREP_SUB_FORCED_ENTRY_GENERAL_DISTANCE_CHECK
		CASE GOV_AUTO_SALVAGE			RETURN g_sMPTunables.IH2_PREP_SUB_AUTO_SALVAGE_GENERAL_DISTANCE_CHECK
		CASE GOV_CAR_COLLECTOR			RETURN g_sMPTunables.IH2_PREP_IAA_CAR_COLLECTOR_GENERAL_DISTANCE_CHECK
		CASE GOV_MOB_MENTALITY			RETURN g_sMPTunables.IH2_PREP_SUB_MOB_MENTALITY_GENERAL_DISTANCE_CHECK
		CASE GOV_PARAMEDIC				RETURN g_sMPTunables.IH2_PREP_IAA_PARAMEDIC_GENERAL_DISTANCE_CHECK
		CASE GOV_UNDER_CONTROL			RETURN g_sMPTunables.IH2_PREP_IAA_UNDER_CONTROL_GENERAL_DISTANCE_CHECK
		CASE GOV_FLARE_UP				RETURN g_sMPTunables.IH2_PREP_SUB_FLARE_UP_GENERAL_DISTANCE_CHECK
		CASE GOV_AQUALUNGS				RETURN g_sMPTunables.IH2_PREP_SUB_AQUALUNGS_GENERAL_DISTANCE_CHECK
		CASE GOV_AMATEUR_PHOTOGRAPHY	RETURN g_sMPTunables.IH2_PREP_SILO_AMATEUR_PHOTOGRAPHY_GENERAL_DISTANCE_CHECK
		CASE GOV_GONE_BALLISTIC			RETURN g_sMPTunables.IH2_PREP_SILO_GONE_BALLISTIC_GENERAL_DISTANCE_CHECK
		CASE GOV_DAYLIGHT_ROBBERY		RETURN g_sMPTunables.IH2_PREP_SILO_DAYLIGHT_ROBBERY_GENERAL_DISTANCE_CHECK
		CASE GOV_BURGLARY_JOB			RETURN g_sMPTunables.IH2_PREP_SILO_BURGLARY_JOB_GENERAL_DISTANCE_CHECK
		CASE GOV_FLIGHT_RECORDER		RETURN g_sMPTunables.IH2_PREP_SILO_FLIGHT_RECORDER_GENERAL_DISTANCE_CHECK
	ENDSWITCH
	
	RETURN 1000
ENDFUNC

FUNC INT GET_MAXIMUM_DISTANCE_FROM_FACILITY_FOR_FM_GANGOPS_MISSION()

	RETURN 999999

ENDFUNC
 
FUNC VECTOR GET_SPAWN_COORD_FOR_GANGOPS_DISTANCE_CHECK(GANGOPS_VARIATION eVariation, GANGOPS_LOCATION eLocation)
	
	SWITCH eVariation
		CASE GOV_FORCED_ENTRY
			RETURN GET_GANGOPS_PED_SPAWN_COORDS(0,eLocation,eVariation)
		CASE GOV_BURGLARY_JOB
			RETURN GET_GANGOPS_PED_SPAWN_COORDS(5,eLocation,eVariation)
		CASE GOV_CAR_COLLECTOR
			RETURN GET_GANGOPS_PED_SPAWN_COORDS(0,eLocation,eVariation)
	ENDSWITCH
	
	RETURN GET_GANGOPS_ENTITY_SPAWN_COORDS(eVariation, eLocation, 0, TRUE)
ENDFUNC

FUNC BOOL IS_THIS_GANGOPS_VARIATION_BUSY(PLAYER_INDEX playerId, GANGOPS_VARIATION eVariation)

	INT iCount

	INT iPlayer
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		IF iPlayer != NATIVE_TO_INT(playerID)
			PLAYER_INDEX remotePlayer = INT_TO_PLAYERINDEX(iPlayer)
			IF NETWORK_IS_PLAYER_ACTIVE(remotePlayer)
				IF GB_IS_PLAYER_BOSS_OF_A_GANG(remotePlayer)
					IF GB_GET_GANGOPS_VARIATION_PLAYER_IS_ON(remotePlayer) = eVariation
						iCount++				
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

	IF (iCount >= g_sMPTunables.IH2_PREP_IGNORE_DISTANCE_CHECK_THRESHOLD)
		PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] IS_THIS_GANGOPS_VARIATION_BUSY - TRUE - Number of players playing this variation = ",iCount)
		RETURN TRUE
	ENDIF
		
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_GANGOPS_LOCATION_IN_SAFE_RANGE(GANGOPS_LOCATION eLocation, GANGOPS_VARIATION eVariation, PLAYER_INDEX playerId, BOOL bIgnoreDistanceCheck = FALSE)
	IF bIgnoreDistanceCheck
	#IF IS_DEBUG_BUILD
	OR GET_COMMANDLINE_PARAM_EXISTS("sc_FM_GangOpsFlowPlayVariations")
	#ENDIF
		PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] IS_GANGOPS_LOCATION_IN_SAFE_RANGE - RETURN TRUE - Ignoring distance check.")
		RETURN TRUE
	ENDIF
	
	VECTOR vDistanceCheckLocation = GET_SPAWN_COORD_FOR_GANGOPS_DISTANCE_CHECK(eVariation, eLocation)
	 
	IF NOT IS_VECTOR_ZERO(vDistanceCheckLocation)
		
		IF GET_SIMPLE_INTERIOR_ID_FROM_DEFUNCT_BASE_ID(GET_PLAYERS_OWNED_DEFUNCT_BASE(playerId)) != SIMPLE_INTERIOR_INVALID
			FLOAT fDistance = VDIST(g_SimpleInteriorData.vMidPoints[GET_SIMPLE_INTERIOR_ID_FROM_DEFUNCT_BASE_ID(GET_PLAYERS_OWNED_DEFUNCT_BASE(playerId))], vDistanceCheckLocation)
			IF fDistance < GET_MINIMUM_DISTANCE_FROM_FACILITY_FOR_FM_GANGOPS_MISSION(eVariation) OR fDistance > GET_MAXIMUM_DISTANCE_FROM_FACILITY_FOR_FM_GANGOPS_MISSION()
				PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] IS_GANGOPS_LOCATION_IN_SAFE_RANGE - Distance between facility and location is outside range (", GET_MINIMUM_DISTANCE_FROM_FACILITY_FOR_FM_GANGOPS_MISSION(eVariation) ," - ",GET_MAXIMUM_DISTANCE_FROM_FACILITY_FOR_FM_GANGOPS_MISSION() ,") - Location ", GET_GANGOPS_LOCATION_NAME(eLocation), " not valid.")
				RETURN FALSE
			ENDIF
		ELSE
			PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] IS_GANGOPS_LOCATION_IN_SAFE_RANGE - RETURN TRUE - No facility detected.")
		ENDIF
	ELSE
		PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] IS_GANGOPS_LOCATION_IN_SAFE_RANGE - RETURN TRUE - Mission entity spawn coords is <<0.0, 0.0, 0.0>>. Probably a moving vehicle.")
	ENDIF
	
	UNUSED_PARAMETER(playerId)
	UNUSED_PARAMETER(eVariation)
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_ALLOW_MULTIPLE_GANGS_ON_LOCATION(GANGOPS_LOCATION eLocation1)
	SWITCH eLocation1
		CASE GangOpsLocation_AmateurPhotography_Location1
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_GANGOPS_LOCATION_IN_USE(INT iLocation, PLAYER_INDEX playerID)
	
	IF SHOULD_ALLOW_MULTIPLE_GANGS_ON_LOCATION(INT_TO_ENUM(GANGOPS_LOCATION, iLocation))
		PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] IS_GANGOPS_LOCATION_IN_USE - SHOULD_ALLOW_MULTIPLE_GANGS_ON_LOCATION = TRUE")
		RETURN FALSE
	ENDIF
	
	INT iPlayer
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		IF iPlayer != NATIVE_TO_INT(playerID)
			IF GB_GET_LOCATION_RESERVATION_FOR_MISSION(FMMC_TYPE_FM_GANGOPS, iPlayer) = iLocation
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT

	RETURN FALSE

ENDFUNC

FUNC BOOL SHOULD_CHECK_FM_GANGOPS_VEHICLE_SPAWN_LOCATION(GANGOPS_VARIATION eVariation, INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	
	SWITCH eVariation
		CASE GOV_CAR_COLLECTOR			RETURN FALSE
		CASE GOV_AMATEUR_PHOTOGRAPHY	RETURN FALSE
	ENDSWITCH

	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_CHECK_FM_GANGOPS_PED_SPAWN_LOCATION(GANGOPS_VARIATION eVariation, INT iPed)

	SWITCH eVariation
		CASE GOV_CAR_COLLECTOR
			SWITCH iPed
				CASE 20
				CASE 21
				CASE 22
					RETURN FALSE
			ENDSWITCH
		BREAK
		CASE GOV_FORCED_ENTRY
			SWITCH iPed
				CASE 7
				CASE 8
				CASE 9
				CASE 10
				CASE 11
				CASE 12
				CASE 13
					RETURN FALSE
			ENDSWITCH
		BREAK
		CASE GOV_BURGLARY_JOB
			SWITCH iPed
				CASE 0
				CASE 1
				CASE 2
				CASE 3
				CASE 4
					RETURN FALSE
			ENDSWITCH
		BREAK
	ENDSWITCH

	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_CHECK_MISSION_ENTITY_SPAWN_LOCATION(GANGOPS_VARIATION eVariation)
	SWITCH eVariation
		CASE GOV_CAR_COLLECTOR
		CASE GOV_MOB_MENTALITY
		CASE GOV_FORCED_ENTRY
		CASE GOV_BURGLARY_JOB
		CASE GOV_AMATEUR_PHOTOGRAPHY
		CASE GOV_DAYLIGHT_ROBBERY
		CASE GOV_AUTO_SALVAGE
			RETURN FALSE
	ENDSWITCH 
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_GANGOPS_LOCATION_SAFE_TO_SPAWN_ENTITIES(GANGOPS_LOCATION eLocation, GANGOPS_VARIATION eVariation, GANGOPS_SUBVARIATION eSubvariation, PLAYER_INDEX playerID)

	MODEL_NAMES model
	FLOAT fRadius
	
	INT iNumEntities = GB_GET_NUM_FM_GANGOPS_ENTITIES_FOR_VARIATION(eVariation,GB_GET_NUM_GOONS_IN_PLAYER_GANG(playerID)+1,playerID)

	
	// Mission Entities
	INT iMissionEntity	
	REPEAT iNumEntities iMissionEntity
		IF SHOULD_CHECK_MISSION_ENTITY_SPAWN_LOCATION(eVariation)
			model = GET_GANGOPS_ENTITY_MODEL(GET_GANGOPS_ITEM_TYPE_FOR_VARIATION(eVariation))
			fRadius = GET_RADIUS_FROM_CONTRABAND_ENTITY(model)
			
			VECTOR vMissionEntityCoords = GET_GANGOPS_ENTITY_SPAWN_COORDS(eVariation, eLocation, iMissionEntity, TRUE) 

			PRINTLN("[FM_GANGOPS] - IS_GANGOPS_LOCATION_SAFE_TO_SPAWN_ENTITIES - Checking - Mission entity #",iMissionEntity," at ",vMissionEntityCoords," with radius ",fRadius, " with model ",GET_MODEL_NAME_FOR_DEBUG(model))
			IF NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION(vMissionEntityCoords,fRadius,fRadius,fRadius,0.0,FALSE,FALSE,FALSE,0.0,FALSE,DEFAULT,FALSE,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
				PRINTLN("[FM_GANGOPS] - IS_GANGOPS_LOCATION_SAFE_TO_SPAWN_ENTITIES - FALSE - Mission entity #",iMissionEntity," spawn point is not safe.")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT

	// Non-mission vehicles
	INT iNonMissionVehicle
	WHILE ( NOT IS_VECTOR_ZERO(GET_GANGOPS_VEHICLE_SPAWN_COORDS(eVariation, eLocation, iNonMissionVehicle)) )
	
		IF SHOULD_CHECK_FM_GANGOPS_VEHICLE_SPAWN_LOCATION(eVariation, iNonMissionVehicle)
			VECTOR vNonCoords = GET_GANGOPS_VEHICLE_SPAWN_COORDS(eVariation, eLocation, iNonMissionVehicle)
			model = GET_GANGOPS_VEHICLE_MODEL(eVariation, eSubvariation, iNonMissionVehicle)
			fRadius = GET_RADIUS_FROM_CONTRABAND_ENTITY(model)
			
			PRINTLN("[FM_GANGOPS] - IS_GANGOPS_LOCATION_SAFE_TO_SPAWN_ENTITIES - Checking - Non-mission vehicle #",iNonMissionVehicle," at ",vNonCoords," with radius ",fRadius, " with model ",GET_MODEL_NAME_FOR_DEBUG(model))
			IF NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION(vNonCoords,fRadius,fRadius,fRadius,0.0,FALSE,FALSE,FALSE,0.0,FALSE,DEFAULT,FALSE,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
				PRINTLN("[FM_GANGOPS] - IS_GANGOPS_LOCATION_SAFE_TO_SPAWN_ENTITIES - FALSE - Non-mission vehicle ",iNonMissionVehicle," not safe.")
				RETURN FALSE
			ENDIF
		ENDIF
		
		iNonMissionVehicle++
	ENDWHILE
	
	//Mission peds
	INT iPed
	WHILE (NOT IS_VECTOR_ZERO(GET_GANGOPS_PED_SPAWN_COORDS(iPed, eLocation, eVariation)))
		
		IF SHOULD_CHECK_FM_GANGOPS_PED_SPAWN_LOCATION(eVariation, iPed)
			VECTOR vPedCoords = GET_GANGOPS_PED_SPAWN_COORDS(iPed, eLocation, eVariation)
			model = mp_m_freemode_01
			fRadius = 2
			
			PRINTLN("[FM_GANGOPS] - IS_GANGOPS_LOCATION_SAFE_TO_SPAWN_ENTITIES - Checking - Ped #",iPed," at ",vPedCoords," with radius ",fRadius)
			IF NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION(vPedCoords,fRadius,fRadius,fRadius,0.0,FALSE,FALSE,FALSE,0.0,FALSE,DEFAULT,FALSE,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
				PRINTLN("[FM_GANGOPS] - IS_GANGOPS_LOCATION_SAFE_TO_SPAWN_ENTITIES - FALSE - Ped #",iPed," is not safe.")
				RETURN FALSE
			ENDIF
		ENDIF
		
		iPed++
	ENDWHILE
	
	RETURN TRUE
	
	UNUSED_PARAMETER(playerId)
	UNUSED_PARAMETER(eSubvariation)
ENDFUNC

/// Returns false if the variation does not require checks against entity spawn coords to launch.
FUNC BOOL DOES_GANGOPS_VARIATION_REQUIRE_ENTITY_COORD_LAUNCHING_CHECK(GANGOPS_VARIATION eVariation)
	
	UNUSED_PARAMETER(eVariation)
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_GANGOPS_LOCATION_SAFE(GANGOPS_LOCATION eLocation, GANGOPS_VARIATION eVariation, GANGOPS_SUBVARIATION eSubvariation, PLAYER_INDEX playerID)
	
	// Specific variations do not require spawn checks since they are not fixed entity locations
	IF NOT DOES_GANGOPS_VARIATION_REQUIRE_ENTITY_COORD_LAUNCHING_CHECK(eVariation)
		RETURN TRUE
	ENDIF
	
	IF SHOULD_CHECK_MISSION_ENTITY_SPAWN_LOCATION(eVariation)
		VECTOR vMissionEntityCoords
		vMissionEntityCoords = GET_GANGOPS_ENTITY_SPAWN_COORDS(eVariation, eLocation, 0, TRUE)

		//Check if in view of any players
		IF NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION(vMissionEntityCoords,0,0,0,DEFAULT,TRUE,TRUE,DEFAULT,DEFAULT,DEFAULT,DEFAULT,FALSE)
			PRINTLN("[FM_GANGOPS] - IS_GANGOPS_LOCATION_SAFE - location in view of player, location not valid.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	//Add entity coords checks here
	IF NOT IS_GANGOPS_LOCATION_SAFE_TO_SPAWN_ENTITIES(eLocation, eVariation, eSubvariation, playerID)
		PRINTLN("[FM_GANGOPS] - IS_GANGOPS_LOCATION_SAFE - location not valid to spawn entities")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

////////////////////////////////////////
/// MISSION LAUNCH LOCATION SELECTION ///
////////////////////////////////////////

FUNC BOOL IS_GANGOPS_LOCATION_DISABLED(GANGOPS_LOCATION eLocation)
	
	UNUSED_PARAMETER(eLocation)
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_GANGOPS_LOCATION_A_GLOBAL_RESUPPLY_LOCATION(GANGOPS_LOCATION eLocation)
//	SWITCH eLocation
//
//			RETURN TRUE
//	ENDSWITCH
	
	UNUSED_PARAMETER(eLocation)
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_GANGOPS_LOCATION_SUITABLE_FOR_SELL_VARIATION(GANGOPS_LOCATION eLocation, GANGOPS_VARIATION eVariation)
	UNUSED_PARAMETER(eLocation)
	UNUSED_PARAMETER(eVariation)
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_GANGOPS_LOCATION_SUITABLE_FOR_VARIATION_AND_SUBVARIATION(GANGOPS_LOCATION eLocation, GANGOPS_VARIATION eVariation, GANGOPS_SUBVARIATION eSubvariation)
	
	// SELL
	IF IS_GANGOPS_VARIATION_A_SELL_MISSION(eVariation)
		RETURN IS_GANGOPS_LOCATION_SUITABLE_FOR_SELL_VARIATION(eLocation, eVariation)
	ENDIF

	SWITCH eVariation
		
		// PREP MISSIONS
		CASE GOV_DEAD_DROP
			SWITCH eSubvariation
				CASE GOS_DD_LOCATION_1				RETURN (eLocation = GangOpsLocation_DeadDrop_Location1)
				CASE GOS_DD_LOCATION_2				RETURN (eLocation = GangOpsLocation_DeadDrop_Location2)
				CASE GOS_DD_LOCATION_3				RETURN (eLocation = GangOpsLocation_DeadDrop_Location3)
				CASE GOS_DD_LOCATION_4				RETURN (eLocation = GangOpsLocation_DeadDrop_Location4)
				CASE GOS_DD_LOCATION_5				RETURN (eLocation = GangOpsLocation_DeadDrop_Location5)
				CASE GOS_DD_LOCATION_6				RETURN (eLocation = GangOpsLocation_DeadDrop_Location6)
				CASE GOS_DD_LOCATION_7				RETURN (eLocation = GangOpsLocation_DeadDrop_Location7)
				CASE GOS_DD_LOCATION_8				RETURN (eLocation = GangOpsLocation_DeadDrop_Location8)
				CASE GOS_DD_LOCATION_9				RETURN (eLocation = GangOpsLocation_DeadDrop_Location9)
				CASE GOS_DD_LOCATION_10				RETURN (eLocation = GangOpsLocation_DeadDrop_Location10)
				CASE GOS_DD_LOCATION_11				RETURN (eLocation = GangOpsLocation_DeadDrop_Location11)
				CASE GOS_DD_LOCATION_12				RETURN (eLocation = GangOpsLocation_DeadDrop_Location12)
				CASE GOS_DD_LOCATION_13				RETURN (eLocation = GangOpsLocation_DeadDrop_Location13)
				CASE GOS_DD_LOCATION_14				RETURN (eLocation = GangOpsLocation_DeadDrop_Location14)
				CASE GOS_DD_LOCATION_15				RETURN (eLocation = GangOpsLocation_DeadDrop_Location15)
			ENDSWITCH
		BREAK
		CASE GOV_FORCED_ENTRY	
			SWITCH eSubvariation
				CASE GOS_FE_LOCATION_FUDGE			RETURN (eLocation = GangOpsLocation_ForcedEntry_Location_Fudge)
				CASE GOS_FE_LOCATION_MAGELLAN		RETURN (eLocation = GangOpsLocation_ForcedEntry_Location_Magellan)
				CASE GOS_FE_LOCATION_FORUM			RETURN (eLocation = GangOpsLocation_ForcedEntry_Location_Forum)
				CASE GOS_FE_LOCATION_ROY_LOWENSTEIN	RETURN (eLocation = GangOpsLocation_ForcedEntry_Location_Roy_Lowenstein)
				CASE GOS_FE_LOCATION_EASTBOURNE		RETURN (eLocation = GangOpsLocation_ForcedEntry_Location_Eastbourne)		
				CASE GOS_FE_LOCATION_V_MAGELLAN		RETURN (eLocation = GangOpsLocation_ForcedEntry_Location_V_Magellan)	
				CASE GOS_FE_LOCATION_ALTA			RETURN (eLocation = GangOpsLocation_ForcedEntry_Location_Alta)	
				CASE GOS_FE_LOCATION_FORUM_DRIVE	RETURN (eLocation = GangOpsLocation_ForcedEntry_Location_Forum_Drive)	
				CASE GOS_FE_LOCATION_SPANISH		RETURN (eLocation = GangOpsLocation_ForcedEntry_Location_Spanish)	
				CASE GOS_FE_LOCATION_TAHITIAN		RETURN (eLocation = GangOpsLocation_ForcedEntry_Location_Tahitian)	
			ENDSWITCH
		BREAK
		CASE GOV_CAR_COLLECTOR
			SWITCH eSubvariation
				CASE GOS_CC_STEELE_WAY				RETURN (eLocation = GangOpsLocation_CarCollector_SteeleWay)
				CASE GOS_CC_AMERICANCO_WAY			RETURN (eLocation = GangOpsLocation_CarCollector_AmericanoWay)
				CASE GOS_CC_ACE_JONES_DRIVE			RETURN (eLocation = GangOpsLocation_CarCollector_AceJonesDrive)
				CASE GOS_CC_RICHMAN					RETURN (eLocation = GangOpsLocation_CarCollector_Richman)
				CASE GOS_CC_NORTH_ROCKFORD_1		RETURN (eLocation = GangOpsLocation_CarCollector_NorthRockford_1)
				CASE GOS_CC_DUNSTABLE_LANE_1		RETURN (eLocation = GangOpsLocation_CarCollector_DunstableLane_1)
				CASE GOS_CC_DUNSTABLE_LANE_2		RETURN (eLocation = GangOpsLocation_CarCollector_DunstableLane_2)
				CASE GOS_CC_NORTH_ROCKFORD_2		RETURN (eLocation = GangOpsLocation_CarCollector_NorthRockford_2)
				CASE GOS_CC_CAESARS_PLACE			RETURN (eLocation = GangOpsLocation_CarCollector_CaesarsPlace)
				CASE GOS_CC_HILLCREST_AVENUE		RETURN (eLocation = GangOpsLocation_CarCollector_HillcrestAvenue)
			ENDSWITCH
		BREAK
		CASE GOV_AUTO_SALVAGE
			SWITCH eSubvariation
				CASE GOS_AS_LOCATION_1				RETURN (eLocation = GangOpsLocation_AutoSalvage_Location1)
				CASE GOS_AS_LOCATION_2				RETURN (eLocation = GangOpsLocation_AutoSalvage_Location2)
				CASE GOS_AS_LOCATION_3				RETURN (eLocation = GangOpsLocation_AutoSalvage_Location3)
				CASE GOS_AS_LOCATION_4				RETURN (eLocation = GangOpsLocation_AutoSalvage_Location4)
				CASE GOS_AS_LOCATION_5				RETURN (eLocation = GangOpsLocation_AutoSalvage_Location5)
				CASE GOS_AS_LOCATION_6				RETURN (eLocation = GangOpsLocation_AutoSalvage_Location6)
				CASE GOS_AS_LOCATION_7				RETURN (eLocation = GangOpsLocation_AutoSalvage_Location7)
				CASE GOS_AS_LOCATION_8				RETURN (eLocation = GangOpsLocation_AutoSalvage_Location8)
				CASE GOS_AS_LOCATION_9				RETURN (eLocation = GangOpsLocation_AutoSalvage_Location9)
				CASE GOS_AS_LOCATION_10				RETURN (eLocation = GangOpsLocation_AutoSalvage_Location10)
			ENDSWITCH
		BREAK
		CASE GOV_MOB_MENTALITY
			SWITCH eSubvariation
				CASE GOS_MM_MORNINGWOOD				RETURN (eLocation = GangOpsLocation_MobMentality_Morningwood)
				CASE GOS_MM_GRAPESEED				RETURN (eLocation = GangOpsLocation_MobMentality_Grapeseed)
				CASE GOS_MM_ALTA                    RETURN (eLocation = GangOpsLocation_MobMentality_Alta)
				CASE GOS_MM_MISSION_ROW             RETURN (eLocation = GangOpsLocation_MobMentality_MissionRow)
				CASE GOS_MM_HIPPY_CAMP              RETURN (eLocation = GangOpsLocation_MobMentality_HippyCamp)
				CASE GOS_MM_SAN_ANDREAS            	RETURN (eLocation = GangOpsLocation_MobMentality_SanAndreas)
				CASE GOS_MM_MIRROR_PARK             RETURN (eLocation = GangOpsLocation_MobMentality_MirrorPark)
				CASE GOS_MM_STRAWBERRY              RETURN (eLocation = GangOpsLocation_MobMentality_Strawberry)
				CASE GOS_MM_MIRROR_PARK_TAVERN      RETURN (eLocation = GangOpsLocation_MobMentality_MirrorParkTavern)
				CASE GOS_MM_ULSA                 	RETURN (eLocation = GangOpsLocation_MobMentality_ULSA)
			ENDSWITCH
		BREAK
		CASE GOV_PARAMEDIC
			SWITCH eSubvariation
				CASE GOS_PM_CENTRAL_LS_MEDICAL_CENTER		RETURN (eLocation = GangOpsLocation_Paramedic_Central_LS_Medical_Center)
				CASE GOS_PM_PILLBOX_HILL_MEDICAL_CENTER		RETURN (eLocation = GangOpsLocation_Paramedic_Pillbox_Hill_Medical_Center)
				CASE GOS_PM_ST_FIACRE_HOSPITAL				RETURN (eLocation = GangOpsLocation_Paramedic_ST_Fiacre_Hospital)
				CASE GOS_PM_SANDY_SHORES_MEDICAL_CENTER		RETURN (eLocation = GangOpsLocation_Paramedic_Sandy_Shores_Medical_Center)
				CASE GOS_PM_PALETO_BAY_CARE_CENTER			RETURN (eLocation = GangOpsLocation_Paramedic_Paleto_Bay_Care_Center)
				CASE GOS_PM_DEL_PERRO_FREEWAY				RETURN (eLocation = GangOpsLocation_Paramedic_Del_Perro_Freeway)
				CASE GOS_PM_MOUNT_GORDO						RETURN (eLocation = GangOpsLocation_Paramedic_Mount_Gordo)
				CASE GOS_PM_GREAT_CHAPARRAL					RETURN (eLocation = GangOpsLocation_Paramedic_Great_Chaparral)
				CASE GOS_PM_CASSIDY_CREEK					RETURN (eLocation = GangOpsLocation_Paramedic_Cassidy_Creek)
				CASE GOS_PM_POWER_STATION					RETURN (eLocation = GangOpsLocation_Paramedic_Power_Station)
			ENDSWITCH
		BREAK
		CASE GOV_UNDER_CONTROL
			SWITCH eSubvariation
				CASE GOS_UC_SERVER_ROOM_1				RETURN (eLocation = GangOpsLocation_UnderControl_ServerRoom1)
				CASE GOS_UC_SERVER_ROOM_2          	 	RETURN (eLocation = GangOpsLocation_UnderControl_ServerRoom2)
				CASE GOS_UC_SERVER_ROOM_3          	 	RETURN (eLocation = GangOpsLocation_UnderControl_ServerRoom3)
				CASE GOS_UC_SERVER_ROOM_4          	 	RETURN (eLocation = GangOpsLocation_UnderControl_ServerRoom4)
				CASE GOS_UC_SERVER_ROOM_5          	 	RETURN (eLocation = GangOpsLocation_UnderControl_ServerRoom5)
				CASE GOS_UC_CONTROL_ROOM_1         	 	RETURN (eLocation = GangOpsLocation_UnderControl_ControlRoom1)
				CASE GOS_UC_CONTROL_ROOM_2         	 	RETURN (eLocation = GangOpsLocation_UnderControl_ControlRoom2)
				CASE GOS_UC_CONTROL_ROOM_3         	 	RETURN (eLocation = GangOpsLocation_UnderControl_ControlRoom3)
				CASE GOS_UC_CONTROL_ROOM_4         	 	RETURN (eLocation = GangOpsLocation_UnderControl_ControlRoom4)
				CASE GOS_UC_CONTROL_ROOM_5         	 	RETURN (eLocation = GangOpsLocation_UnderControl_ControlRoom5)
			ENDSWITCH
		BREAK		 
		CASE GOV_FLARE_UP    
			SWITCH eSubvariation			
				CASE GOS_FU_CHAMBERLAIN_HILLS   		RETURN (eLocation = GangOpsLocation_FlareUp_Chamberlain_Hills)
				CASE GOS_FU_MORNINGWOOD				RETURN (eLocation = GangOpsLocation_FlareUp_Gas_Station_Morningwood)
				CASE GOS_FU_LA_PUERTA				RETURN (eLocation = GangOpsLocation_FlareUp_Gas_Station_La_Puerta)
				CASE GOS_FU_EL_BURRO				RETURN (eLocation = GangOpsLocation_FlareUp_Gas_Station_El_burro)
				CASE GOS_FU_DAVIS				RETURN (eLocation = GangOpsLocation_FlareUp_Gas_Station_Davis)
				CASE GOS_FU_SANDY				RETURN (eLocation = GangOpsLocation_FlareUp_Gas_Station_Sandy)
				CASE GOS_FU_PALETO				RETURN (eLocation = GangOpsLocation_FlareUp_Gas_Station_Paleto)
				CASE GOS_FU_CAPITAL				RETURN (eLocation = GangOpsLocation_FlareUp_Gas_Station_Capital)
				CASE GOS_FU_CLINTON				RETURN (eLocation = GangOpsLocation_FlareUp_Gas_Station_Clinton)
				CASE GOS_FU_RICHMAN				RETURN (eLocation = GangOpsLocation_FlareUp_Gas_Station_Richman)
			ENDSWITCH
		BREAK          
		CASE GOV_AQUALUNGS        
			SWITCH eSubvariation
				CASE GOS_AL_ELYSIAN_ISLAND_1				RETURN (eLocation = GangOpsLocation_Aqualungs_Elysian_Island_1)
				CASE GOS_AL_PALETO_BAY						RETURN (eLocation = GangOpsLocation_Aqualungs_Paleto_Bay)
				CASE GOS_AL_ELYSIAN_ISLAND_2				RETURN (eLocation = GangOpsLocation_Aqualungs_Elysian_Island_2)
				CASE GOS_AL_PALOMINO_HIGHLANDS				RETURN (eLocation = GangOpsLocation_Aqualungs_Palomino_Highlands)
				CASE GOS_AL_CHUMASH							RETURN (eLocation = GangOpsLocation_Aqualungs_Chumash)
				CASE GOS_AL_SAN_CHIANSKI_MOUNTAIN_RANGE		RETURN (eLocation = GangOpsLocation_Aqualungs_San_Chianksi_Mountain_Range)
				CASE GOS_AL_PALETO_FOREST				RETURN (eLocation = GangOpsLocation_Aqualungs_PaletoForest)
				CASE GOS_AL_NORTH_CHUMASH					RETURN (eLocation = GangOpsLocation_Aqualungs_North_Chumash)
				CASE GOS_AL_PACIFIC_BLUFFS					RETURN (eLocation = GangOpsLocation_Aqualungs_Pacific_Bluffs)
				CASE GOS_AL_VESPUCCI_BEACH					RETURN (eLocation = GangOpsLocation_Aqualungs_Vespucci_Beach)
			ENDSWITCH
		BREAK        
		CASE GOV_AMATEUR_PHOTOGRAPHY   
			SWITCH eSubvariation
				CASE GOS_AP_LOCATION_1				RETURN (eLocation = GangOpsLocation_AmateurPhotography_Location1)
			ENDSWITCH
		BREAK   
		CASE GOV_GONE_BALLISTIC        
			SWITCH eSubvariation
				CASE GOS_GB_ELYSIAN_ISLAND_1			RETURN (eLocation = GangOpsLocation_GoneBallistic_ElysianIsland1)
				CASE GOS_GB_GRAND_SENORA_DESERT_1       RETURN (eLocation = GangOpsLocation_GoneBallistic_GrandSenoraDesert1)
				CASE GOS_GB_GRAPESEED_1                 RETURN (eLocation = GangOpsLocation_GoneBallistic_Grapeseed1)
				CASE GOS_GB_EL_BURRO_HEIGHTS            RETURN (eLocation = GangOpsLocation_GoneBallistic_ElBurroHeights)
				CASE GOS_GB_TERMINAL                    RETURN (eLocation = GangOpsLocation_GoneBallistic_Terminal)
				CASE GOS_GB_LSIA       					RETURN (eLocation = GangOpsLocation_GoneBallistic_LSIA)
				CASE GOS_GB_MURRIETA_HEIGHTS       		RETURN (eLocation = GangOpsLocation_GoneBallistic_MurrietaHeights)
				CASE GOS_GB_ELYSIAN_ISLAND_2            RETURN (eLocation = GangOpsLocation_GoneBallistic_ElysianIsland2)
				CASE GOS_GB_PALETO_BAY                  RETURN (eLocation = GangOpsLocation_GoneBallistic_PaletoBay)
				CASE GOS_GB_FORT_ZANCUDO                RETURN (eLocation = GangOpsLocation_GoneBallistic_FortZancudo)
			ENDSWITCH
		BREAK   
		CASE GOV_DAYLIGHT_ROBBERY      
			SWITCH eSubvariation				
				CASE GOS_DR_LOCATION_MIRROR_PARK	RETURN (eLocation = GangOpsLocation_DaylightRobbery_MirrorPark)
				CASE GOS_DR_LOCATION_SAN_ANDREAS	RETURN (eLocation = GangOpsLocation_DaylightRobbery_SanAndreas)
				CASE GOS_DR_LOCATION_VESPUCCI		RETURN (eLocation = GangOpsLocation_DaylightRobbery_Vespucci)
				CASE GOS_DR_LOCATION_LEGION_SQ		RETURN (eLocation = GangOpsLocation_DaylightRobbery_LegionSq)
				CASE GOS_DR_LOCATION_DEL_PERRO		RETURN (eLocation = GangOpsLocation_DaylightRobbery_DelPerro)
				CASE GOS_DR_LOCATION_ROCKFORD		RETURN (eLocation = GangOpsLocation_DaylightRobbery_Rockford)
				CASE GOS_DR_LOCATION_WESTVINEWOOD	RETURN (eLocation = GangOpsLocation_DaylightRobbery_WestVinewood)
				CASE GOS_DR_LOCATION_BURTON			RETURN (eLocation = GangOpsLocation_DaylightRobbery_Burton)
				CASE GOS_DR_LOCATION_DELPERROPLAZA	RETURN (eLocation = GangOpsLocation_DaylightRobbery_DelPerroPlaza)
				CASE GOS_DR_LOCATION_PILLBOX		RETURN (eLocation = GangOpsLocation_DaylightRobbery_Pillbox)
			ENDSWITCH
		BREAK   
		CASE GOV_BURGLARY_JOB          
			SWITCH eSubvariation
				CASE GOS_BJ_PROCOPIO				RETURN (eLocation = GangOpsLocation_BurglaryJob_ProcopioDrive)
				CASE GOS_BJ_SOUTH_MO_MILTON  		RETURN (eLocation = GangOpsLocation_BurglaryJob_SouthMoMilton)
				CASE GOS_BJ_MIRROR_PARK  			RETURN (eLocation = GangOpsLocation_BurglaryJob_MirrorPark)
				CASE GOS_BJ_IMAGINATION_COURT  		RETURN (eLocation = GangOpsLocation_BurglaryJob_ImaginationCourt)
				CASE GOS_BJ_DIDION_DRIVE  			RETURN (eLocation = GangOpsLocation_BurglaryJob_DidionDrive)
				CASE GOS_BJ_KIMBLE_HILL  			RETURN (eLocation = GangOpsLocation_BurglaryJob_KimbleHill)
				CASE GOS_BJ_BRIDGE_STREET  			RETURN (eLocation = GangOpsLocation_BurglaryJob_BridgeStreet)
				CASE GOS_BJ_NIKOLA_AVENUE    		RETURN (eLocation = GangOpsLocation_BurglaryJob_NikolaAvenue)  
				CASE GOS_BJ_NORTH_SHELDON_AVENUE  	RETURN (eLocation = GangOpsLocation_BurglaryJob_NorthSheldonAvenue)
				CASE GOS_BJ_INESENO_ROAD  			RETURN (eLocation = GangOpsLocation_BurglaryJob_InesenoRoad)
			ENDSWITCH
		BREAK   
		CASE GOV_FLIGHT_RECORDER       
			SWITCH eSubvariation
				CASE GOS_FR_DAM						RETURN (eLocation = GangOpsLocation_FlightRecorder_Dam)		
				CASE GOS_FR_PALOMINO				RETURN (eLocation = GangOpsLocation_FlightRecorder_Palomino)	
				CASE GOS_FR_TERMINAL				RETURN (eLocation = GangOpsLocation_FlightRecorder_Terminal)
				CASE GOS_FR_PACIFIC_BLUFFS			RETURN (eLocation = GangOpsLocation_FlightRecorder_PacificBluffs)
				CASE GOS_FR_MOUNT_GORDO				RETURN (eLocation = GangOpsLocation_FlightRecorder_MountGordo)		
				CASE GOS_FR_ALAMO_SEA_1				RETURN (eLocation = GangOpsLocation_FlightRecorder_AlamoSea1)		
				CASE GOS_FR_ALAMO_SEA_2				RETURN (eLocation = GangOpsLocation_FlightRecorder_AlamoSea2)		
				CASE GOS_FR_ALAMO_SEA_3				RETURN (eLocation = GangOpsLocation_FlightRecorder_AlamoSea3)		
				CASE GOS_FR_PALETO_BAY				RETURN (eLocation = GangOpsLocation_FlightRecorder_PaletoBay)	
				CASE GOS_FR_FORT_ZANCUDO			RETURN (eLocation = GangOpsLocation_FlightRecorder_FortZancudo)	
			ENDSWITCH
		BREAK   
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_GANGOPS_LOCATION_VALID_FOR_THIS_VARIATION(GANGOPS_LOCATION eLocation, GANGOPS_VARIATION eVariation, GANGOPS_SUBVARIATION eSubvariation)

	IF eLocation = GangOpsLocation_Max
		RETURN FALSE
	ENDIF
	
	IF NOT IS_GANGOPS_LOCATION_SUITABLE_FOR_VARIATION_AND_SUBVARIATION(eLocation, eVariation, eSubvariation)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE	
ENDFUNC

FUNC GANGOPS_LOCATION GB_GET_RANDOM_GANGOPS_LOCATION(GANGOPS_VARIATION eVariation, GANGOPS_SUBVARIATION eSubvariation, PLAYER_INDEX playerID, BOOL bIgnoreDistanceChecks = FALSE)

	
	PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] -----------------------------------------------------------------------------------------------------------------")
	
	INT iLoop, iSuitableLocationCount, iNumMissionEntities
	GANGOPS_LOCATION eLocation[COUNT_OF(GANGOPS_LOCATION)]
	GANGOPS_LOCATION eTemp
	BOOL bShouldIgnoreDistanceChecks
	
	IF bIgnoreDistanceChecks 
	#IF IS_DEBUG_BUILD
	OR GET_COMMANDLINE_PARAM_EXISTS("sc_FM_GangOpsFlowPlaySubvariations")
	#ENDIF
		bShouldIgnoreDistanceChecks = TRUE
	ENDIF
	
	iNumMissionEntities = GB_GET_NUM_FM_GANGOPS_ENTITIES_FOR_VARIATION(eVariation, (GB_GET_NUM_GOONS_IN_PLAYER_GANG(playerID) + 1),playerID)
	
	REPEAT COUNT_OF(GANGOPS_LOCATION) iLoop
		eTemp = INT_TO_ENUM(GANGOPS_LOCATION, iLoop)
		
		IF eTemp = GangOpsLocation_Max
			BREAKLOOP
		ENDIF
		
		#IF IS_DEBUG_BUILD
		STRING sTemp = GET_GANGOPS_LOCATION_NAME(eTemp)
		PRINTLN("[FM_GANGOPS] GB_GET_RANDOM_GANGOPS_LOCATION - ", sTemp, " is being checked for suitability.")
		#ENDIF
		
		BOOL bRelaxRestrictions = IS_THIS_GANGOPS_VARIATION_BUSY(playerID,eVariation) OR bIgnoreDistanceChecks
		
		IF IS_GANGOPS_LOCATION_VALID_FOR_THIS_VARIATION(eTemp, eVariation, eSubvariation)
			PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] GB_GET_RANDOM_GANGOPS_LOCATION - ", sTemp, " is valid for this variation.")
			IF NOT IS_GANGOPS_LOCATION_DISABLED(eTemp)
				IF NOT IS_GANGOPS_LOCATION_IN_USE(iLoop, playerID)
					IF IS_GANGOPS_LOCATION_IN_SAFE_RANGE(eTemp, eVariation, playerID, bShouldIgnoreDistanceChecks)
					OR bRelaxRestrictions
						IF IS_GANGOPS_LOCATION_SAFE(eTemp, eVariation, eSubvariation, playerID)
						OR bShouldIgnoreDistanceChecks
							IF IS_GANGOPS_LOCATION_DIFFERENT_FROM_LAST_USED(playerID, eTemp)
							OR bRelaxRestrictions
								IF ARE_NETWORK_ENTITIES_AVAILABLE_FOR_SCRIPT_LAUNCH_IN_AREA(GB_GET_GANGOPS_NUM_PEDS_REQUIRED(eVariation, eSubvariation, iNumMissionEntities), GB_GET_GANGOPS_NUM_VEH_REQUIRED(eVariation, eSubvariation, iNumMissionEntities, playerID), GB_GET_GANGOPS_NUM_OBJ_REQUIRED(eVariation, eSubvariation, iNumMissionEntities,playerID),GET_SPAWN_COORD_FOR_GANGOPS_DISTANCE_CHECK(eVariation,eTemp))
									eLocation[iSuitableLocationCount] = eTemp
									iSuitableLocationCount++
									PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] GB_GET_RANDOM_GANGOPS_LOCATION - PASS - ", sTemp, " is suitable. Current number of suitable locations: ", iSuitableLocationCount)
								ENDIF
							ELSE
								PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] GB_GET_RANDOM_GANGOPS_LOCATION - FAIL - ", sTemp, " is same as last used location.")
							ENDIF
						ELSE
							PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] GB_GET_RANDOM_GANGOPS_LOCATION - FAIL - ", sTemp, " is not safe to spawn entities.")
						ENDIF
					ELSE
						PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] GB_GET_RANDOM_GANGOPS_LOCATION - FAIL - ", sTemp, " is not in safe range to launch.")
					ENDIF
				ELSE
					PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] GB_GET_RANDOM_GANGOPS_LOCATION - FAIL - ", sTemp, " is currently in use by another player.")
				ENDIF
			ELSE
				PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] - GB_GET_RANDOM_GANGOPS_LOCATION - FAIL - ", sTemp, " is currently disabled.")
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF iSuitableLocationCount < 1
		PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] GB_GET_RANDOM_GANGOPS_LOCATION - No suitable locations. Returning eVEHICLEEXPORTLOCATION_MAX to start search again.")
		RETURN GangOpsLocation_Max
	ENDIF
	
	eTemp = eLocation[GET_RANDOM_INT_IN_RANGE(0, iSuitableLocationCount)]
	PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] GB_GET_RANDOM_GANGOPS_LOCATION - Number of Possible Locations: ", iSuitableLocationCount, ". Selected location: ", GET_GANGOPS_LOCATION_NAME(eTemp))
	PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] -----------------------------------------------------------------------------------------------------------------")
	
	RETURN eTemp
ENDFUNC

FUNC FLOAT GET_GANGOPS_VARIATION_WEIGHTING(GANGOPS_VARIATION eVariation)

	SWITCH eVariation
		// Resupply
		CASE GOV_DEAD_DROP						RETURN 1.0
		CASE GOV_FORCED_ENTRY					RETURN 1.0
		CASE GOV_AUTO_SALVAGE					RETURN 1.0
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC FLOAT GET_GANGOPS_SUBVARIATION_WEIGHTING(GANGOPS_SUBVARIATION eSubvariation)              

	SWITCH eSubvariation
	
		CASE GOS_PM_CENTRAL_LS_MEDICAL_CENTER		RETURN g_sMPTunables.FH2_PREP_IAA_PARAMEDIC_CENTRAL_LS_MEDICAL_CENTER_WEIGHTING         
		CASE GOS_PM_PILLBOX_HILL_MEDICAL_CENTER		RETURN g_sMPTunables.FH2_PREP_IAA_PARAMEDIC_PILLBOX_HILL_MEDICAL_CENTER_WEIGHTING       
		CASE GOS_PM_ST_FIACRE_HOSPITAL				RETURN g_sMPTunables.FH2_PREP_IAA_PARAMEDIC_ST_FIACRE_HOSPITAL_WEIGHTING                
		CASE GOS_PM_SANDY_SHORES_MEDICAL_CENTER		RETURN g_sMPTunables.FH2_PREP_IAA_PARAMEDIC_SANDY_SHORES_MEDICAL_CENTER_WEIGHTING       
		CASE GOS_PM_PALETO_BAY_CARE_CENTER			RETURN g_sMPTunables.FH2_PREP_IAA_PARAMEDIC_PALETO_BAY_CARE_CENTER_WEIGHTING            
		CASE GOS_PM_DEL_PERRO_FREEWAY				RETURN g_sMPTunables.FH2_PREP_IAA_PARAMEDIC_DEL_PERRO_FREEWAY_WEIGHTING                 
		CASE GOS_PM_MOUNT_GORDO						RETURN g_sMPTunables.FH2_PREP_IAA_PARAMEDIC_MOUNT_GORDO_WEIGHTING                  
		CASE GOS_PM_GREAT_CHAPARRAL					RETURN g_sMPTunables.FH2_PREP_IAA_PARAMEDIC_GREAT_CHAPARRAL_WEIGHTING                   
		CASE GOS_PM_CASSIDY_CREEK					RETURN g_sMPTunables.FH2_PREP_IAA_PARAMEDIC_CASSIDY_CREEK_WEIGHTING                     
		CASE GOS_PM_POWER_STATION					RETURN g_sMPTunables.FH2_PREP_IAA_PARAMEDIC_POWER_STATION_WEIGHTING      

		CASE GOS_FU_CHAMBERLAIN_HILLS				RETURN g_sMPTunables.FH2_PREP_SUB_FLARE_UP_CHAMBERLAIN_HILLS_WEIGHTING  
		CASE GOS_FU_MORNINGWOOD						RETURN g_sMPTunables.FH2_PREP_SUB_FLARE_UP_MORNINGWOOD_WEIGHTING        
		CASE GOS_FU_LA_PUERTA						RETURN g_sMPTunables.FH2_PREP_SUB_FLARE_UP_LA_PUERTA_WEIGHTING          
		CASE GOS_FU_EL_BURRO						RETURN g_sMPTunables.FH2_PREP_SUB_FLARE_UP_EL_BURRO_HEIGHTS_WEIGHTING   
		CASE GOS_FU_DAVIS							RETURN g_sMPTunables.FH2_PREP_SUB_FLARE_UP_DAVIS_WEIGHTING              
		CASE GOS_FU_SANDY							RETURN g_sMPTunables.FH2_PREP_SUB_FLARE_UP_SANDY_SHORES_WEIGHTING       
		CASE GOS_FU_PALETO							RETURN g_sMPTunables.FH2_PREP_SUB_FLARE_UP_PALETO_BAY_WEIGHTING         
		CASE GOS_FU_CAPITAL							RETURN g_sMPTunables.FH2_PREP_SUB_FLARE_UP_STRAWBERRY_WEIGHTING         
		CASE GOS_FU_CLINTON							RETURN g_sMPTunables.FH2_PREP_SUB_FLARE_UP_DOWNTOWN_VINEWOOD_WEIGHTING  
		CASE GOS_FU_RICHMAN							RETURN g_sMPTunables.FH2_PREP_SUB_FLARE_UP_RICHMAN_GLEN_WEIGHTING    
				
		CASE GOS_FE_LOCATION_FUDGE					RETURN g_sMPTunables.FH2_PREP_SUB_FORCED_ENTRY_EL_BURRO_HEIGHTS_WEIGHTING   
		CASE GOS_FE_LOCATION_MAGELLAN				RETURN g_sMPTunables.FH2_PREP_SUB_FORCED_ENTRY_LA_PUERTA_WEIGHTING          
		CASE GOS_FE_LOCATION_FORUM					RETURN g_sMPTunables.FH2_PREP_SUB_FORCED_ENTRY_CHAMBERLAIN_HILLS_WEIGHTING  
		CASE GOS_FE_LOCATION_ROY_LOWENSTEIN			RETURN g_sMPTunables.FH2_PREP_SUB_FORCED_ENTRY_RANCHO_WEIGHTING	
		CASE GOS_FE_LOCATION_EASTBOURNE				RETURN g_sMPTunables.FH2_PREP_SUB_FORCED_ENTRY_WEST_VINEWOOD_WEIGHTING      
		CASE GOS_FE_LOCATION_V_MAGELLAN				RETURN g_sMPTunables.FH2_PREP_SUB_FORCED_ENTRY_VESPUCCI_WEIGHTING           
		CASE GOS_FE_LOCATION_ALTA					RETURN g_sMPTunables.FH2_PREP_SUB_FORCED_ENTRY_HAWICK_WEIGHTING             
		CASE GOS_FE_LOCATION_FORUM_DRIVE			RETURN g_sMPTunables.FH2_PREP_SUB_FORCED_ENTRY_CHAMBERLAIN_HILLS_2_WEIGHTING
		CASE GOS_FE_LOCATION_SPANISH				RETURN g_sMPTunables.FH2_PREP_SUB_FORCED_ENTRY_WEST_VINEWOOD_2_WEIGHTING    
		CASE GOS_FE_LOCATION_TAHITIAN				RETURN g_sMPTunables.FH2_PREP_SUB_FORCED_ENTRY_STRAWBERRY_WEIGHTING         
			 
		CASE GOS_DR_LOCATION_MIRROR_PARK			RETURN g_sMPTunables.FH2_PREP_SILO_DAYLIGHT_ROBBERY_MIRROR_PARK_WEIGHTING      
		CASE GOS_DR_LOCATION_SAN_ANDREAS			RETURN g_sMPTunables.FH2_PREP_SILO_DAYLIGHT_ROBBERY_SAN_ANDREAS_AVE_WEIGHTING  
		CASE GOS_DR_LOCATION_VESPUCCI				RETURN g_sMPTunables.FH2_PREP_SILO_DAYLIGHT_ROBBERY_VESPUCCI_CANALS_WEIGHTING  
		CASE GOS_DR_LOCATION_LEGION_SQ				RETURN g_sMPTunables.FH2_PREP_SILO_DAYLIGHT_ROBBERY_LEGION_SQUARE_WEIGHTING    
		CASE GOS_DR_LOCATION_DEL_PERRO				RETURN g_sMPTunables.FH2_PREP_SILO_DAYLIGHT_ROBBERY_DEL_PERRO_PIER_WEIGHTING   
		CASE GOS_DR_LOCATION_ROCKFORD				RETURN g_sMPTunables.FH2_PREP_SILO_DAYLIGHT_ROBBERY_ROCKFORD_HILLS_WEIGHTING   
		CASE GOS_DR_LOCATION_WESTVINEWOOD			RETURN g_sMPTunables.FH2_PREP_SILO_DAYLIGHT_ROBBERY_WEST_VINEWOOD_WEIGHTING    
		CASE GOS_DR_LOCATION_BURTON					RETURN g_sMPTunables.FH2_PREP_SILO_DAYLIGHT_ROBBERY_BURTON_WEIGHTING           
		CASE GOS_DR_LOCATION_DELPERROPLAZA			RETURN g_sMPTunables.FH2_PREP_SILO_DAYLIGHT_ROBBERY_DEL_PERRO_WEIGHTING        
		CASE GOS_DR_LOCATION_PILLBOX				RETURN g_sMPTunables.FH2_PREP_SILO_DAYLIGHT_ROBBERY_PILLBOX_HILL_WEIGHTING   

		// Car Collector
		CASE GOS_CC_STEELE_WAY						RETURN g_sMPTunables.FH2_PREP_IAA_CAR_COLLECTOR_ROCKFORD_HILLS_WEIGHTING   
		CASE GOS_CC_AMERICANCO_WAY					RETURN g_sMPTunables.FH2_PREP_IAA_CAR_COLLECTOR_RICHMAN_WEIGHTING          
		CASE GOS_CC_ACE_JONES_DRIVE					RETURN g_sMPTunables.FH2_PREP_IAA_CAR_COLLECTOR_VINEWOOD_HILLS_WEIGHTING   
		CASE GOS_CC_RICHMAN							RETURN g_sMPTunables.FH2_PREP_IAA_CAR_COLLECTOR_RICHMAN_2_WEIGHTING        
		CASE GOS_CC_NORTH_ROCKFORD_1				RETURN g_sMPTunables.FH2_PREP_IAA_CAR_COLLECTOR_NORTH_ROCKFORD_WEIGHTING   
		CASE GOS_CC_DUNSTABLE_LANE_1				RETURN g_sMPTunables.FH2_PREP_IAA_CAR_COLLECTOR_RICHMAN_3_WEIGHTING        
		CASE GOS_CC_DUNSTABLE_LANE_2				RETURN g_sMPTunables.FH2_PREP_IAA_CAR_COLLECTOR_ROCKFORD_HILLS_1_WEIGHTING 
		CASE GOS_CC_NORTH_ROCKFORD_2				RETURN g_sMPTunables.FH2_PREP_IAA_CAR_COLLECTOR_RICHMAN_4_WEIGHTING        
		CASE GOS_CC_CAESARS_PLACE					RETURN g_sMPTunables.FH2_PREP_IAA_CAR_COLLECTOR_ROCKFORD_HILLS_2_WEIGHTING 
		CASE GOS_CC_HILLCREST_AVENUE				RETURN g_sMPTunables.FH2_PREP_IAA_CAR_COLLECTOR_VINEWOOD_HILLS_2_WEIGHTING 
		
		// Auto Salvage
		CASE GOS_AS_LOCATION_1						RETURN g_sMPTunables.FH2_PREP_SUB_AUTO_SALVAGE_GROUP_1_WEIGHTING     
		CASE GOS_AS_LOCATION_2						RETURN g_sMPTunables.FH2_PREP_SUB_AUTO_SALVAGE_GROUP_2_WEIGHTING     
		CASE GOS_AS_LOCATION_3						RETURN g_sMPTunables.FH2_PREP_SUB_AUTO_SALVAGE_GROUP_3_WEIGHTING     
		CASE GOS_AS_LOCATION_4						RETURN g_sMPTunables.FH2_PREP_SUB_AUTO_SALVAGE_GROUP_4_WEIGHTING     
		CASE GOS_AS_LOCATION_5						RETURN g_sMPTunables.FH2_PREP_SUB_AUTO_SALVAGE_GROUP_5_WEIGHTING     
		CASE GOS_AS_LOCATION_6						RETURN g_sMPTunables.FH2_PREP_SUB_AUTO_SALVAGE_GROUP_6_WEIGHTING     
		CASE GOS_AS_LOCATION_7						RETURN g_sMPTunables.FH2_PREP_SUB_AUTO_SALVAGE_GROUP_7_WEIGHTING     
		CASE GOS_AS_LOCATION_8						RETURN g_sMPTunables.FH2_PREP_SUB_AUTO_SALVAGE_GROUP_8_WEIGHTING     
		CASE GOS_AS_LOCATION_9						RETURN g_sMPTunables.FH2_PREP_SUB_AUTO_SALVAGE_GROUP_9_WEIGHTING     
		CASE GOS_AS_LOCATION_10						RETURN g_sMPTunables.FH2_PREP_SUB_AUTO_SALVAGE_GROUP_10_WEIGHTING 
		
		// Mob Mentality
		CASE GOS_MM_MORNINGWOOD						RETURN g_sMPTunables.FH2_PREP_SUB_MOB_MENTALITY_MORNINGWOOD_WEIGHTING          
		CASE GOS_MM_GRAPESEED						RETURN g_sMPTunables.FH2_PREP_SUB_MOB_MENTALITY_GRAPESEED_WEIGHTING            
		CASE GOS_MM_ALTA							RETURN g_sMPTunables.FH2_PREP_SUB_MOB_MENTALITY_ALTA_WEIGHTING                 
		CASE GOS_MM_MISSION_ROW						RETURN g_sMPTunables.FH2_PREP_SUB_MOB_MENTALITY_MISSION_ROW_WEIGHTING          
		CASE GOS_MM_HIPPY_CAMP						RETURN g_sMPTunables.FH2_PREP_SUB_MOB_MENTALITY_HIPPY_CAMP_WEIGHTING           
		CASE GOS_MM_SAN_ANDREAS						RETURN g_sMPTunables.FH2_PREP_SUB_MOB_MENTALITY_SANDY_SHORES_WEIGHTING         
		CASE GOS_MM_MIRROR_PARK						RETURN g_sMPTunables.FH2_PREP_SUB_MOB_MENTALITY_MIRROR_PARK_WEIGHTING          
		CASE GOS_MM_STRAWBERRY						RETURN g_sMPTunables.FH2_PREP_SUB_MOB_MENTALITY_STRAWBERRY_WEIGHTING           
		CASE GOS_MM_MIRROR_PARK_TAVERN				RETURN g_sMPTunables.FH2_PREP_SUB_MOB_MENTALITY_MIRROR_PARK_2_WEIGHTING        
		CASE GOS_MM_ULSA							RETURN g_sMPTunables.FH2_PREP_SUB_MOB_MENTALITY_HARMONY_WEIGHTING   
		
		// Under Control
		CASE GOS_UC_SERVER_ROOM_1					RETURN g_sMPTunables.FH2_PREP_IAA_UNDER_CONTROL_SERVER_ROOM_1_WEIGHTING    
		CASE GOS_UC_SERVER_ROOM_2					RETURN g_sMPTunables.FH2_PREP_IAA_UNDER_CONTROL_SERVER_ROOM_2_WEIGHTING    
		CASE GOS_UC_SERVER_ROOM_3					RETURN g_sMPTunables.FH2_PREP_IAA_UNDER_CONTROL_SERVER_ROOM_3_WEIGHTING    
		CASE GOS_UC_SERVER_ROOM_4					RETURN g_sMPTunables.FH2_PREP_IAA_UNDER_CONTROL_SERVER_ROOM_4_WEIGHTING    
		CASE GOS_UC_SERVER_ROOM_5					RETURN g_sMPTunables.FH2_PREP_IAA_UNDER_CONTROL_SERVER_ROOM_5_WEIGHTING    
		CASE GOS_UC_CONTROL_ROOM_1					RETURN g_sMPTunables.FH2_PREP_IAA_UNDER_CONTROL_CONTROL_ROOM_6_WEIGHTING   
		CASE GOS_UC_CONTROL_ROOM_2					RETURN g_sMPTunables.FH2_PREP_IAA_UNDER_CONTROL_CONTROL_ROOM_7_WEIGHTING   
		CASE GOS_UC_CONTROL_ROOM_3					RETURN g_sMPTunables.FH2_PREP_IAA_UNDER_CONTROL_CONTROL_ROOM_8_WEIGHTING   
		CASE GOS_UC_CONTROL_ROOM_4					RETURN g_sMPTunables.FH2_PREP_IAA_UNDER_CONTROL_CONTROL_ROOM_9_WEIGHTING   
		CASE GOS_UC_CONTROL_ROOM_5					RETURN g_sMPTunables.FH2_PREP_IAA_UNDER_CONTROL_CONTROL_ROOM_10_WEIGHTING  
		
		// Aqualungs
		CASE GOS_AL_ELYSIAN_ISLAND_1				RETURN g_sMPTunables.FH2_PREP_SUB_AQUALUNGS_ELYSIAN_ISLAND_1_WEIGHTING             
		CASE GOS_AL_PALETO_BAY						RETURN g_sMPTunables.FH2_PREP_SUB_AQUALUNGS_PALETO_BAY_WEIGHTING                   
		CASE GOS_AL_ELYSIAN_ISLAND_2				RETURN g_sMPTunables.FH2_PREP_SUB_AQUALUNGS_ELYSIAN_ISLAND_2_WEIGHTING             
		CASE GOS_AL_PALOMINO_HIGHLANDS				RETURN g_sMPTunables.FH2_PREP_SUB_AQUALUNGS_PALOMINO_HIGHLANDS_WEIGHTING           
		CASE GOS_AL_CHUMASH							RETURN g_sMPTunables.FH2_PREP_SUB_AQUALUNGS_CHUMASH_WEIGHTING                      
		CASE GOS_AL_SAN_CHIANSKI_MOUNTAIN_RANGE		RETURN g_sMPTunables.FH2_PREP_SUB_AQUALUNGS_SAN_CHIANSKI_MOUNTAIN_RANGE_WEIGHTING  
		CASE GOS_AL_PALETO_FOREST				RETURN g_sMPTunables.FH2_PREP_SUB_AQUALUNGS_PROCOPIO_PROMENADE_WEIGHTING           
		CASE GOS_AL_NORTH_CHUMASH					RETURN g_sMPTunables.FH2_PREP_SUB_AQUALUNGS_NORTH_CHUMASH_WEIGHTING                
		CASE GOS_AL_PACIFIC_BLUFFS					RETURN g_sMPTunables.FH2_PREP_SUB_AQUALUNGS_PACIFIC_BLUFFS_WEIGHTING               
		CASE GOS_AL_VESPUCCI_BEACH					RETURN g_sMPTunables.FH2_PREP_SUB_AQUALUNGS_VESPUCCI_BEACH_WEIGHTING   
		
		// Gone Ballistic
		CASE GOS_GB_ELYSIAN_ISLAND_1				RETURN g_sMPTunables.FH2_PREP_SILO_GONE_BALLISTIC_ELYSIAN_ISLAND_WEIGHTING          
		CASE GOS_GB_GRAND_SENORA_DESERT_1			RETURN g_sMPTunables.FH2_PREP_SILO_GONE_BALLISTIC_GRAND_SENORA_DESERT_2_WEIGHTING   
		CASE GOS_GB_GRAPESEED_1						RETURN g_sMPTunables.FH2_PREP_SILO_GONE_BALLISTIC_GRAPESEED__WEIGHTING              
		CASE GOS_GB_EL_BURRO_HEIGHTS				RETURN g_sMPTunables.FH2_PREP_SILO_GONE_BALLISTIC_GRAPESEED_2_WEIGHTING             
		CASE GOS_GB_TERMINAL						RETURN g_sMPTunables.FH2_PREP_SILO_GONE_BALLISTIC_TERMINAL_WEIGHTING                
		CASE GOS_GB_LSIA			RETURN g_sMPTunables.FH2_PREP_SILO_GONE_BALLISTIC_GRAND_SENORA_DESERT_3_WEIGHTING   
		CASE GOS_GB_MURRIETA_HEIGHTS			RETURN g_sMPTunables.FH2_PREP_SILO_GONE_BALLISTIC_GRAND_SENORA_DESERT_4_WEIGHTING   
		CASE GOS_GB_ELYSIAN_ISLAND_2				RETURN g_sMPTunables.FH2_PREP_SILO_GONE_BALLISTIC_ELYSIAN_ISLAND_2_WEIGHTING        
		CASE GOS_GB_PALETO_BAY						RETURN g_sMPTunables.FH2_PREP_SILO_GONE_BALLISTIC_PALETO_BAY_WEIGHTING              
		CASE GOS_GB_FORT_ZANCUDO					RETURN g_sMPTunables.FH2_PREP_SILO_GONE_BALLISTIC_LAGO_ZANCUDO_WEIGHTING    
		
		// Burglary Job
		CASE GOS_BJ_PROCOPIO						RETURN g_sMPTunables.FH2_PREP_SILO_BURGLARY_JOB_PALETO_BAY_WEIGHTING       
		CASE GOS_BJ_SOUTH_MO_MILTON					RETURN g_sMPTunables.FH2_PREP_SILO_BURGLARY_JOB_VINEWOOD_HILLS_WEIGHTING   
		CASE GOS_BJ_MIRROR_PARK						RETURN g_sMPTunables.FH2_PREP_SILO_BURGLARY_JOB_MIRROR_PARK_WEIGHTING      
		CASE GOS_BJ_IMAGINATION_COURT				RETURN g_sMPTunables.FH2_PREP_SILO_BURGLARY_JOB_VESPUCCI_CANALS_WEIGHTING  
		CASE GOS_BJ_DIDION_DRIVE					RETURN g_sMPTunables.FH2_PREP_SILO_BURGLARY_JOB_WEST_VINEWOOD_WEIGHTING  
		CASE GOS_BJ_KIMBLE_HILL						RETURN g_sMPTunables.FH2_PREP_SILO_BURGLARY_JOB_VINEWOOD_HILLS_2_WEIGHTING  		
		CASE GOS_BJ_BRIDGE_STREET					RETURN g_sMPTunables.FH2_PREP_SILO_BURGLARY_JOB_MIRROR_PARK_2_WEIGHTING  
		CASE GOS_BJ_NIKOLA_AVENUE 					RETURN g_sMPTunables.FH2_PREP_SILO_BURGLARY_JOB_MIRROR_PARK_3_WEIGHTING  
		CASE GOS_BJ_NORTH_SHELDON_AVENUE			RETURN g_sMPTunables.FH2_PREP_SILO_BURGLARY_JOB_VINEWOOD_HILLS_3_WEIGHTING  
		CASE GOS_BJ_INESENO_ROAD					RETURN g_sMPTunables.FH2_PREP_SILO_BURGLARY_JOB_BANHAM_CANYON_WEIGHTING  
		
		// Flight Recorder
		CASE GOS_FR_DAM								RETURN g_sMPTunables.FH2_PREP_SILO_FLIGHT_RECORDER_LAND_ACT_DAM_WEIGHTING            
		CASE GOS_FR_PALOMINO						RETURN g_sMPTunables.FH2_PREP_SILO_FLIGHT_RECORDER_PALOMINO_HIGHLANDS_WEIGHTING      
		CASE GOS_FR_TERMINAL						RETURN g_sMPTunables.FH2_PREP_SILO_FLIGHT_RECORDER_TERMINAL_WEIGHTING                
		CASE GOS_FR_PACIFIC_BLUFFS					RETURN g_sMPTunables.FH2_PREP_SILO_FLIGHT_RECORDER_PACIFIC_BLUFFS_WEIGHTING          
		CASE GOS_FR_MOUNT_GORDO						RETURN g_sMPTunables.FH2_PREP_SILO_FLIGHT_RECORDER_MOUNT_GORDO_WEIGHTING             
		CASE GOS_FR_ALAMO_SEA_1						RETURN g_sMPTunables.FH2_PREP_SILO_FLIGHT_RECORDER_ALAMO_SEA_WEIGHTING               
		CASE GOS_FR_ALAMO_SEA_2						RETURN g_sMPTunables.FH2_PREP_SILO_FLIGHT_RECORDER_ALAMO_SEA_2_WEIGHTING             
		CASE GOS_FR_ALAMO_SEA_3						RETURN g_sMPTunables.FH2_PREP_SILO_FLIGHT_RECORDER_ALAMO_SEA_3_WEIGHTING             
		CASE GOS_FR_PALETO_BAY						RETURN g_sMPTunables.FH2_PREP_SILO_FLIGHT_RECORDER_PALETO_BAY_WEIGHTING              
		CASE GOS_FR_FORT_ZANCUDO					RETURN g_sMPTunables.FH2_PREP_SILO_FLIGHT_RECORDER_FORT_ZANCUDO_WEIGHTING  
		
		CASE GOS_DD_LOCATION_1						RETURN g_sMPTunables.FH2_PREP_SILO_CASH_DROP_LSIA_BAG_0_WEIGHTING                  
		CASE GOS_DD_LOCATION_2						RETURN g_sMPTunables.FH2_PREP_SILO_CASH_DROP_LSIA_BAG_1_WEIGHTING                  
		CASE GOS_DD_LOCATION_3						RETURN g_sMPTunables.FH2_PREP_SILO_CASH_DROP_LSIA_BAG_2_WEIGHTING                  
		CASE GOS_DD_LOCATION_4						RETURN g_sMPTunables.FH2_PREP_SILO_CASH_DROP_LSIA_BAG_3_WEIGHTING                  
		CASE GOS_DD_LOCATION_5						RETURN g_sMPTunables.FH2_PREP_SILO_CASH_DROP_LSIA_BAG_4_WEIGHTING                  
		CASE GOS_DD_LOCATION_6						RETURN g_sMPTunables.FH2_PREP_SILO_CASH_DROP_LSIA_BAG_5_WEIGHTING                  
		CASE GOS_DD_LOCATION_7						RETURN g_sMPTunables.FH2_PREP_SILO_CASH_DROP_PUERTO_DEL_SOL_STATION_BAG_0_WEIGHTING
		CASE GOS_DD_LOCATION_8						RETURN g_sMPTunables.FH2_PREP_SILO_CASH_DROP_PUERTO_DEL_SOL_STATION_BAG_1_WEIGHTING
		CASE GOS_DD_LOCATION_9						RETURN g_sMPTunables.FH2_PREP_SILO_CASH_DROP_PUERTO_DEL_SOL_STATION_BAG_2_WEIGHTING
		CASE GOS_DD_LOCATION_10						RETURN g_sMPTunables.FH2_PREP_SILO_CASH_DROP_STRAWBERRY_STATION_BAG_0_WEIGHTING    
		CASE GOS_DD_LOCATION_11						RETURN g_sMPTunables.FH2_PREP_SILO_CASH_DROP_STRAWBERRY_STATION_BAG_1_WEIGHTING    
		CASE GOS_DD_LOCATION_12						RETURN g_sMPTunables.FH2_PREP_SILO_CASH_DROP_STRAWBERRY_STATION_BAG_2_WEIGHTING    
		CASE GOS_DD_LOCATION_13						RETURN g_sMPTunables.FH2_PREP_SILO_CASH_DROP_STRAWBERRY_STATION_BAG_3_WEIGHTING    
		CASE GOS_DD_LOCATION_14						RETURN g_sMPTunables.FH2_PREP_SILO_CASH_DROP_PILLBOX_SOUTH_STATION_BAG_0_WEIGHTING 
		CASE GOS_DD_LOCATION_15						RETURN g_sMPTunables.FH2_PREP_SILO_CASH_DROP_PILLBOX_SOUTH_STATION_BAG_1_WEIGHTING 
		
	ENDSWITCH
	
	RETURN 1.0
ENDFUNC

/////////////////////////////////////////
/// MISSION LAUNCH VARIATION SELECTION ///
/////////////////////////////////////////
FUNC BOOL IS_GANGOPS_SUBVARIATION_DISABLED(GANGOPS_SUBVARIATION eSubvariation)

	UNUSED_PARAMETER(eSubvariation)
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_GANGOPS_SUBVARIATION_SUITABLE(GANGOPS_SUBVARIATION eSubvariation #IF IS_DEBUG_BUILD , GANGOPS_VARIATION eVariation #ENDIF )
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_FM_GangOpsFlowPlaySubvariations")
		IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_FM_GangOpsFlowPlaySubvariations_alt")
			SWITCH eVariation
				CASE GOV_DEAD_DROP		RETURN (eSubvariation = GOS_DD_LOCATION_1)
			ENDSWITCH
		ELSE
			SWITCH eVariation
				CASE GOV_DEAD_DROP		RETURN (eSubvariation = GOS_DD_LOCATION_1)
			ENDSWITCH
		ENDIF
	ENDIF
	#ENDIF

	UNUSED_PARAMETER(eSubvariation)

	RETURN TRUE
ENDFUNC

/// PURPOSE: Returns the number of subvariations a variation has.
/// PARAMS: iVariation - The variation that is being checked
/// RETURNS: The current number of subvariations the passed in variation has.
/// THIS HAS TO BE UPDATED WHEN ADDING NEW SUBVARIATIONS
FUNC INT GET_MAX_GANGOPS_SUBVARIATION_FOR_VARIATION(GANGOPS_VARIATION eVariation)

	SWITCH eVariation
		CASE GOV_DEAD_DROP				RETURN 15
		CASE GOV_FORCED_ENTRY			RETURN 10
		CASE GOV_CAR_COLLECTOR			RETURN 10
		CASE GOV_AUTO_SALVAGE			RETURN 10
		CASE GOV_MOB_MENTALITY			RETURN 10
		CASE GOV_PARAMEDIC				RETURN 10
		CASE GOV_UNDER_CONTROL			RETURN 10
		CASE GOV_FLARE_UP             	RETURN 10
		CASE GOV_AQUALUNGS            	RETURN 10
		CASE GOV_AMATEUR_PHOTOGRAPHY  	RETURN 1
		CASE GOV_GONE_BALLISTIC       	RETURN 10
		CASE GOV_DAYLIGHT_ROBBERY     	RETURN 10
		CASE GOV_BURGLARY_JOB         	RETURN 10
		CASE GOV_FLIGHT_RECORDER      	RETURN 10
	ENDSWITCH
	
	PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] GET_MAX_GANGOPS_SUBVARIATION_FOR_VARIATION - Variation is invalid. Variation = ", GET_GANGOPS_VARIATION_NAME_FOR_DEBUG(eVariation))
	RETURN 0
ENDFUNC

/// PURPOSE: Returns the start point of subvariations for a variation in the subvariation enum.
/// PARAMS: iVariation - The variation that is being checked
/// RETURNS: the start point of subvariations for a variation in the subvariation enum.
/// This shouldn't need updated as long as all new subvariations are added after the existing subvariations for that variation
FUNC INT GET_START_GANGOPS_SUBVARIATION_FOR_VARIATION(GANGOPS_VARIATION eVariation)

	SWITCH eVariation
		CASE GOV_DEAD_DROP			RETURN ENUM_TO_INT(GOS_DD_LOCATION_1)
		CASE GOV_FORCED_ENTRY			RETURN ENUM_TO_INT(GOS_FE_LOCATION_FUDGE)
		CASE GOV_CAR_COLLECTOR		RETURN ENUM_TO_INT(GOS_CC_STEELE_WAY)
		CASE GOV_AUTO_SALVAGE			RETURN ENUM_TO_INT(GOS_AS_LOCATION_1)
		CASE GOV_MOB_MENTALITY			RETURN ENUM_TO_INT(GOS_MM_MORNINGWOOD)
		CASE GOV_PARAMEDIC				RETURN ENUM_TO_INT(GOS_PM_CENTRAL_LS_MEDICAL_CENTER)
		CASE GOV_UNDER_CONTROL			RETURN ENUM_TO_INT(GOS_UC_SERVER_ROOM_1)
		CASE GOV_FLARE_UP             RETURN ENUM_TO_INT(GOS_FU_CHAMBERLAIN_HILLS)
		CASE GOV_AQUALUNGS            RETURN ENUM_TO_INT(GOS_AL_ELYSIAN_ISLAND_1)
		CASE GOV_AMATEUR_PHOTOGRAPHY  RETURN ENUM_TO_INT(GOS_AP_LOCATION_1)
		CASE GOV_GONE_BALLISTIC       RETURN ENUM_TO_INT(GOS_GB_ELYSIAN_ISLAND_1)
		CASE GOV_DAYLIGHT_ROBBERY     RETURN ENUM_TO_INT(GOS_DR_LOCATION_MIRROR_PARK)
		CASE GOV_BURGLARY_JOB         RETURN ENUM_TO_INT(GOS_BJ_PROCOPIO)
		CASE GOV_FLIGHT_RECORDER      RETURN ENUM_TO_INT(GOS_FR_DAM)
	ENDSWITCH
	
	PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] GET_START_GANGOPS_SUBVARIATION_FOR_VARIATION - Variation is invalid. Variation = ", GET_GANGOPS_VARIATION_NAME_FOR_DEBUG(eVariation))
	RETURN ENUM_TO_INT(GOV_INVALID)
ENDFUNC
    
/// Select a random subvariation for the supplied variation
FUNC INT GB_GET_RANDOM_GANGOPS_SUBVARIATION(GANGOPS_VARIATION eVariation)
	INT iSelectedSubvariation
	
	// Get the start and end point for the RNG, so as only to check subvariations for the supplied variation
	INT iStartSubvariation = GET_START_GANGOPS_SUBVARIATION_FOR_VARIATION(eVariation)
	INT iEndSubvariation = iStartSubvariation + GET_MAX_GANGOPS_SUBVARIATION_FOR_VARIATION(eVariation)
	
	PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] -----------------------------------------------------------------------------------------------------------------")
	
	PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] GB_GET_RANDOM_GANGOPS_SUBVARIATION - iStartSubvariation = ", iStartSubvariation, " - iEndSubvariation = ", iEndSubvariation)
	
	IF iStartSubvariation = iEndSubvariation
		PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] GB_GET_RANDOM_GANGOPS_SUBVARIATION - Variation has no subvariations. Returning -1.")
		RETURN -1
	ENDIF
	
	FLOAT fWeightings[GOS_MAX]
	FLOAT fTotalWeight, fCurrentRange
	
	//Assign weightings to each available subvariation
	INT iLoop
	FOR iLoop = iStartSubvariation TO (iEndSubvariation-1)
		GANGOPS_SUBVARIATION eTemp = INT_TO_ENUM(GANGOPS_SUBVARIATION, iLoop)
		IF IS_GANGOPS_SUBVARIATION_DISABLED(eTemp)
		OR NOT IS_GANGOPS_SUBVARIATION_SUITABLE(eTemp #IF IS_DEBUG_BUILD , eVariation #ENDIF )
			fWeightings[iLoop] = 0.0
		ELSE
			fWeightings[iLoop] = GET_GANGOPS_SUBVARIATION_WEIGHTING(eTemp)
		ENDIF
	ENDFOR
	
	//Calculate total of all weightings
	FOR iLoop = iStartSubvariation TO (iEndSubvariation-1)
		fTotalWeight += fWeightings[iLoop]
		PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] GET_RANDOM_GANGOPS_SUBVARIATION - ", GET_GANGOPS_SUBVARIATION_NAME_FOR_DEBUG(INT_TO_ENUM(GANGOPS_SUBVARIATION, iLoop)), " has a weighting of ", fWeightings[iLoop])
	ENDFOR
	
	//Pick a position between 0 and total of all weightings
	FLOAT fSelection = GET_RANDOM_FLOAT_IN_RANGE(0, fTotalWeight)
	PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] GB_GET_RANDOM_GANGOPS_SUBVARIATION - fSelection = ", fSelection)
	
	//Find the variation at that position
	FOR iLoop = iStartSubvariation TO (iEndSubvariation-1)
		fCurrentRange += fWeightings[iLoop]
		IF fSelection < fCurrentRange
			iSelectedSubvariation = iLoop
			BREAKLOOP
		ENDIF
	ENDFOR
	
	PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] GB_GET_RANDOM_GANGOPS_SUBVARIATION - Selected subvariation ", GET_GANGOPS_SUBVARIATION_NAME_FOR_DEBUG(INT_TO_ENUM(GANGOPS_SUBVARIATION, iSelectedSubvariation)), " for variation ", GET_GANGOPS_VARIATION_NAME_FOR_DEBUG(eVariation))
	PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] -----------------------------------------------------------------------------------------------------------------")

	RETURN iSelectedSubvariation

ENDFUNC

#IF IS_DEBUG_BUILD
CONST_INT MAX_FM_GANGOPS_RESUPPLY_FLOW_PLAY_VARIATIONS		7
CONST_INT MAX_FM_GANGOPS_RESUPPLY_FLOW_PLAY_VARIATIONS_ALT	7
FUNC GANGOPS_VARIATION GET_NEXT_FM_GANGOPS_RESUPPLY_FLOW_PLAY_VARIATION(INT iNextBuyStealFlowPlayVariation)
	
	IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_FM_GangOpsFlowPlayVariations_alt")
		SWITCH iNextBuyStealFlowPlayVariation
			CASE 0		RETURN GOV_DEAD_DROP
		ENDSWITCH
	ELSE
		SWITCH iNextBuyStealFlowPlayVariation
			CASE 0		RETURN GOV_DEAD_DROP
		ENDSWITCH
	ENDIF
	
	RETURN GOV_MAX
ENDFUNC

FUNC BOOL IS_FM_GANGOPS_RESUPPLY_VARIATION_IN_FLOW_PLAY(GANGOPS_VARIATION eVariation, INT iNextBuyStealFlowPlayVariation)
	RETURN GET_NEXT_FM_GANGOPS_RESUPPLY_FLOW_PLAY_VARIATION(iNextBuyStealFlowPlayVariation) = eVariation
ENDFUNC

CONST_INT MAX_FM_GANGOPS_SELL_FLOW_PLAY_VARIATIONS		5
CONST_INT MAX_FM_GANGOPS_SELL_FLOW_PLAY_VARIATIONS_ALT	5
FUNC GANGOPS_VARIATION GET_NEXT_FM_GANGOPS_SELL_FLOW_PLAY_VARIATION(INT iNextSellFlowPlayVariation)

	IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_FM_GangOpsFlowPlayVariations_alt")
		SWITCH iNextSellFlowPlayVariation
			CASE 0		RETURN GOV_DEAD_DROP
		ENDSWITCH
	ELSE
		SWITCH iNextSellFlowPlayVariation
			CASE 0		RETURN GOV_DEAD_DROP
		ENDSWITCH
	ENDIF
	
	RETURN GOV_MAX
ENDFUNC

FUNC BOOL IS_FM_GANGOPS_SELL_VARIATION_IN_FLOW_PLAY(GANGOPS_VARIATION eVariation, INT iNextSellFlowPlayVariation)
	RETURN GET_NEXT_FM_GANGOPS_SELL_FLOW_PLAY_VARIATION(iNextSellFlowPlayVariation) = eVariation
ENDFUNC
#ENDIF

PROC GB_COPY_FM_GANGOPS_HISTORY_FROM_TRANSITION()
	INT iSlot
	
	// Resupply
	REPEAT GET_GANGOPS_RESUPPLY_HISTORY_LIST_TUNABLE() iSlot
		GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iFM_GangOpsHistory[iSlot] = g_TransitionSessionNonResetVars.sMagnateGangBossData.iFM_GangOpsHistory[iSlot]
		PRINTLN("[GB] - GB_COPY_FM_GANGOPS_HISTORY_FROM_TRANSITION - Resupply History Slot #", iSlot, ": ", GET_GANGOPS_VARIATION_NAME_FOR_DEBUG(INT_TO_ENUM(GANGOPS_VARIATION,GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iFM_GangOpsHistory[iSlot])))
	ENDREPEAT
	
	PRINTLN("[GB] - GB_COPY_FM_GANGOPS_HISTORY_FROM_TRANSITION ")
ENDPROC

FUNC BOOL GB_IS_GANGOPS_VARIATION_IN_PLAYERS_HISTORY_LIST(GANGOPS_VARIATION eVariation, PLAYER_INDEX playerId)
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_FM_GangOpsFlowPlayVariations")
		RETURN FALSE
	ENDIF
	#ENDIF
	
	INT iPlayer = NATIVE_TO_INT(playerId)
	IF iPlayer = -1
		RETURN FALSE
	ENDIF
	
	INT iSlot
	
	REPEAT GET_GANGOPS_RESUPPLY_HISTORY_LIST_TUNABLE() iSlot
		IF GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.iFM_GangOpsHistory[iSlot] = ENUM_TO_INT(eVariation)
			PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] GB_IS_GANGOPS_VARIATION_IN_PLAYERS_HISTORY_LIST - TRUE - ", GET_GANGOPS_VARIATION_NAME_FOR_DEBUG(eVariation), " is in slot ", iSlot, " in the history list.")
			RETURN TRUE
		ENDIF
	ENDREPEAT

	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_TIME_OF_DAY_SUITABLE_FOR_GANGOPS_VARIATION(GANGOPS_VARIATION eVariation)

	UNUSED_PARAMETER(eVariation)

//	TIMEOFDAY tofTime = GET_CURRENT_TIMEOFDAY() 
//	INT iHour = GET_TIMEOFDAY_HOUR(tofTime)
	
//	SWITCH eVariation
//
//	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_GANGOPS_VARIATION_DISABLED(GANGOPS_VARIATION eVariation, PLAYER_INDEX playerID #IF IS_DEBUG_BUILD , INT iNextResupplyFlowPlayVariation#ENDIF )
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_FM_GangOpsFlowPlayVariations")

		IF NOT IS_FM_GANGOPS_RESUPPLY_VARIATION_IN_FLOW_PLAY(eVariation, iNextResupplyFlowPlayVariation)
			RETURN TRUE
		ELSE
			PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] [FPLAY_RESUP] IS_GANGOPS_VARIATION_DISABLED - ", GET_GANGOPS_VARIATION_NAME_FOR_DEBUG(eVariation), " is next Resupply in the flow play.")
			RETURN FALSE
		ENDIF

	ENDIF
	#ENDIF
	
	IF NOT IS_TIME_OF_DAY_SUITABLE_FOR_GANGOPS_VARIATION(eVariation)
		PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] IS_GANGOPS_VARIATION_DISABLED - Time of day is not suitable for ", GET_GANGOPS_VARIATION_NAME_FOR_DEBUG(eVariation), " to launch. Disabling variation.")
		RETURN TRUE
	ENDIF
	
	IF NOT FREEMODE_CONTENT_CAN_REGISTER_DELIVERABLES(GB_GET_NUM_FM_GANGOPS_ENTITIES_FOR_VARIATION(eVariation, GB_GET_NUM_GOONS_IN_PLAYER_GANG(playerID)+1, playerID))
		RETURN TRUE
	ENDIF
	
	// Launch Restrictions (Gang Size, Product %)
//	SWITCH(eVariation)
//
//			IF GB_GET_NUM_GOONS_IN_PLAYER_GANG(playerID) = 0
//				PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] IS_GANGOPS_VARIATION_DISABLED - ", GET_GANGOPS_VARIATION_NAME_FOR_DEBUG(eVariation), " requires at least 2 players in gang. Disabling variation.")
//				RETURN TRUE
//			ENDIF
//		BREAK
//		
//	ENDSWITCH
	
	RETURN FALSE
	UNUSED_PARAMETER(eVariation)
	UNUSED_PARAMETER(playerID)
ENDFUNC

PROC GB_GET_RANDOM_GANGOPS_VARIATION(INT& iVariation, INT& iSubvariation, PLAYER_INDEX playerId, INT iFMMCType)

	UNUSED_PARAMETER(iFMMCType)

	INT iTemp
	
	PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] -----------------------------------------------------------------------------------------------------------------")

	IF iVariation > -1
		PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] GB_GET_RANDOM_GANGOPS_VARIATION - Forced variation: ", GET_GANGOPS_VARIATION_NAME_FOR_DEBUG(INT_TO_ENUM(GANGOPS_VARIATION, iVariation)))
		IF DOES_GANGOPS_VARIATION_HAVE_SUBVARIATIONS(INT_TO_ENUM(GANGOPS_VARIATION, iVariation))
			IF iSubvariation > -1
				PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] GB_GET_RANDOM_GANGOPS_VARIATION - Forced subvariation: ", GET_GANGOPS_SUBVARIATION_NAME_FOR_DEBUG(INT_TO_ENUM(GANGOPS_SUBVARIATION, iSubvariation)))
			ELSE
				iSubvariation = GB_GET_RANDOM_GANGOPS_SUBVARIATION(INT_TO_ENUM(GANGOPS_VARIATION, iVariation))
				PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] GB_GET_RANDOM_GANGOPS_VARIATION - Forced variation without subvariation. Subvariation selected: ", GET_GANGOPS_SUBVARIATION_NAME_FOR_DEBUG(INT_TO_ENUM(GANGOPS_SUBVARIATION, iSubvariation)))
			ENDIF
			
		ENDIF
		EXIT
	ENDIF

	FLOAT fWeightings[GOV_MAX]
	FLOAT fTotalWeight, fCurrentRange
	
	//Assign weightings to each variation
	INT iVar
	REPEAT ENUM_TO_INT(GOV_MAX) iVar
		GANGOPS_VARIATION eTemp = INT_TO_ENUM(GANGOPS_VARIATION,iVar)
		IF IS_GANGOPS_VARIATION_DISABLED(eTemp, playerID #IF IS_DEBUG_BUILD , GlobalplayerBD_FM_3[NATIVE_TO_INT(playerID)].sMagnateGangBossData.iNextBuyStealFlowPlayVariation#ENDIF )
		OR GB_IS_GANGOPS_VARIATION_IN_PLAYERS_HISTORY_LIST(eTemp, playerID)
			fWeightings[iVar] = 0.0
		ELSE
			fWeightings[iVar] = GET_GANGOPS_VARIATION_WEIGHTING(eTemp)
		ENDIF
	ENDREPEAT
	
	//Calculate total of all weightings
	REPEAT GOV_MAX iTemp
		fTotalWeight += fWeightings[iTemp]
		PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] GB_GET_RANDOM_GANGOPS_VARIATION - ", GET_GANGOPS_VARIATION_NAME_FOR_DEBUG(INT_TO_ENUM(GANGOPS_VARIATION, iTemp)), " has a weighting of ", fWeightings[iTemp])
	ENDREPEAT
	
	//Pick a position between 0 and total of all weightings
	FLOAT fSelection = GET_RANDOM_FLOAT_IN_RANGE(0,fTotalWeight)
	PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] GB_GET_RANDOM_GANGOPS_VARIATION - fSelection = ", fSelection)
	
	//Find the variation at that position
	REPEAT GOV_MAX iTemp
		fCurrentRange += fWeightings[iTemp]
		PRINTLN("[MAGNATE_GANG_BOSS] [FM_GANGOPS] GB_GET_RANDOM_GANGOPS_VARIATION - fCurrentRange = ", fCurrentRange)
		IF fSelection < fCurrentRange
			IF NOT DOES_GANGOPS_VARIATION_HAVE_SUBVARIATIONS(INT_TO_ENUM(GANGOPS_VARIATION, iTemp))
				iVariation = iTemp
				EXIT
			ELSE
				iSubvariation = GB_GET_RANDOM_GANGOPS_SUBVARIATION(INT_TO_ENUM(GANGOPS_VARIATION, iTemp))
				iVariation = iTemp
				EXIT
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

/////////////////////////////
/// MISSION LAUNCH DATA  ///
/////////////////////////////

FUNC BOOL GB_GET_RANDOM_GANGOPS_VARIATION_DATA(INT& iVariation, INT& iSubvariation, INT& iLocation, PLAYER_INDEX playerID, INT iFMMCType, FREEMODE_DELIVERY_ACTIVE_DROPOFF_PROPERTIES &sDropOffProperties)
	INT iTempVar = iVariation
	INT iTempSub = iSubvariation
	INT iTempLoc = iLocation
	GB_GET_RANDOM_GANGOPS_VARIATION(iTempVar, iTempSub, playerID, iFMMCType)
	
	IF iTempVar != (-1)
	
		#IF IS_DEBUG_BUILD
		IF iTempLoc > -1
			PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] GB_GET_RANDOM_GANGOPS_VARIATION_DATA - Used debug location: ", GET_GANGOPS_LOCATION_NAME(INT_TO_ENUM(GANGOPS_LOCATION, iTempLoc)))
		ELSE
		#ENDIF
	
		IF NOT IS_GANGOPS_VARIATION_A_SELL_MISSION(INT_TO_ENUM(GANGOPS_VARIATION,iTempVar))
			BOOL bIgnoreDistanceChecks
			IF iSubvariation != -1
				bIgnoreDistanceChecks = TRUE
			ENDIF
			
			iTempLoc = ENUM_TO_INT(GB_GET_RANDOM_GANGOPS_LOCATION(INT_TO_ENUM(GANGOPS_VARIATION, iTempVar), INT_TO_ENUM(GANGOPS_SUBVARIATION, iTempSub),playerID, bIgnoreDistanceChecks))
			
		ENDIF
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		IF iTempLoc != ENUM_TO_INT(GangOpsLocation_Max)
			iVariation = iTempVar
			iSubvariation = iTempSub
			iLocation = iTempLoc
			PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] -----------------------------------------------------------------------------------------------------------------")
			PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] GB_GET_RANDOM_GANGOPS_VARIATION_DATA - Selected variation: ", GET_GANGOPS_VARIATION_NAME_FOR_DEBUG(INT_TO_ENUM(GANGOPS_VARIATION, iVariation)))
			PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] GB_GET_RANDOM_GANGOPS_VARIATION_DATA - Selected subvariation: ", GET_GANGOPS_SUBVARIATION_NAME_FOR_DEBUG(INT_TO_ENUM(GANGOPS_SUBVARIATION, iSubvariation)))
			PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] GB_GET_RANDOM_GANGOPS_VARIATION_DATA - Selected location: ", GET_GANGOPS_LOCATION_NAME(INT_TO_ENUM(GANGOPS_LOCATION, iLocation)))
			PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] -----------------------------------------------------------------------------------------------------------------")
			
			//Find dropoff
			IF GET_DROP_OFF_FOR_GANGOPS_VARIATION(playerID,INT_TO_ENUM(GANGOPS_VARIATION, iTempVar),sDropOffProperties, GET_PLAYERS_OWNED_DEFUNCT_BASE(playerID))
				RESERVE_DROPOFFS_ON_SERVER(sDropOffProperties.eDropoffList, playerID)
				PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] GB_GET_RANDOM_GANGOPS_VARIATION_DATA - Found dropoff starting at ",sDropOffProperties.eDropoffList.eDropoffID[0])
			ELSE
				PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] GB_GET_RANDOM_GANGOPS_VARIATION_DATA - Couldn't find a valid dropoff.")
				RETURN FALSE
			ENDIF
			
			RETURN TRUE
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] GB_GET_RANDOM_GANGOPS_VARIATION_DATA - Couldn't find a valid location.")
		#ENDIF
		ENDIF
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FM_GANGOPS] GB_GET_RANDOM_GANGOPS_VARIATION_DATA - Couldn't find a valid variation.")
	#ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

