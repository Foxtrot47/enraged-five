
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Written by:  Steve Tiley	& Christopher Speirs																		//
// Date: 		15/01/2018																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "net_mission.sch"
USING "net_common_functions.sch"


//----------------------
//	INCLUDES
//----------------------
USING "business_battles_sell_header.sch"

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////// MAIN SCRIPT FUNCTIONS				///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF IS_DEBUG_BUILD

PROC MAINTAIN_J_SKIPS()

	VECTOR vCoords
	FLOAT fHeading
	
	IF GET_SERVER_GAME_STATE() = GAME_STATE_RUNNING
		IF NOT IS_CLIENT_DEBUG_BIT_SET(PARTICIPANT_ID(), eCLIENTDEBUGBITSET_I_HAVE_S_PASSED)
			IF IS_DEBUG_KEY_JUST_RELEASED(KEY_S, KEYBOARD_MODIFIER_NONE, "S Pass")
				PRINTLN("[MB_MISSION] KEY_S, KEYBOARD_MODIFIER_NONE just released")
				SET_CLIENT_DEBUG_BIT(eCLIENTDEBUGBITSET_I_HAVE_S_PASSED)
			ENDIF
		ENDIF
		
		IF NOT IS_CLIENT_DEBUG_BIT_SET(PARTICIPANT_ID(), eCLIENTDEBUGBITSET_I_HAVE_S_PASSED)
			IF IS_DEBUG_KEY_JUST_RELEASED(KEY_F, KEYBOARD_MODIFIER_NONE, "F Fail")
				PRINTLN("[MB_MISSION] KEY_F, KEYBOARD_MODIFIER_NONE just released")
				SET_CLIENT_DEBUG_BIT(eCLIENTDEBUGBITSET_I_HAVE_F_FAILED)
			ENDIF
		ENDIF
		
		VEHICLE_INDEX vNearestMissionEnt
		IF iNearestMissionEntity != -1
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[iNearestMissionEntity].netId)
				vNearestMissionEnt = NET_TO_VEH(serverBD.sMissionEntity[iNearestMissionEntity].netId)
			ENDIF
		ENDIF
		
		VEHICLE_INDEX vMyMissionEntity
		IF GET_MISSION_ENTITY_PLAYER_IS_HOLDING(localPlayerId) != -1
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[GET_MISSION_ENTITY_PLAYER_IS_HOLDING(localPlayerId)].netId)
				vMyMissionEntity = NET_TO_VEH(serverBD.sMissionEntity[GET_MISSION_ENTITY_PLAYER_IS_HOLDING(localPlayerId)].netId)
			ENDIF
		ENDIF
		
		SWITCH GET_MODE_STATE()
			CASE eMODESTATE_COLLECT_ENTITY
				IF IS_DEBUG_KEY_JUST_RELEASED(KEY_J, KEYBOARD_MODIFIER_NONE, "J Skip (warp to vehicle)")
					IF iNearestMissionEntity != -1
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[iNearestMissionEntity].netId)
						AND IS_ENTITY_ALIVE(vNearestMissionEnt)
							vCoords = GET_ENTITY_COORDS(vNearestMissionEnt,FALSE)
			
							IF IS_ENTITY_AT_ENTITY(localPlayerPed, vNearestMissionEnt, <<5.0, 5.0, 5.0>>)

								IF IS_VEHICLE_SEAT_FREE(vNearestMissionEnt, VS_DRIVER)
									PRINTLN("[MB_MISSION] J_SKIP TASK_ENTER_VEHICLE")
									TASK_ENTER_VEHICLE(localPlayerPed, vNearestMissionEnt, -1, VS_DRIVER, PEDMOVEBLENDRATIO_RUN, ECF_WARP_PED)
								ELSE
									IF IS_VEHICLE_SEAT_FREE(vNearestMissionEnt, VS_FRONT_RIGHT)
										TASK_ENTER_VEHICLE(localPlayerPed, vNearestMissionEnt, -1, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_RUN, ECF_WARP_PED)
										PRINTLN("[MB_MISSION] J_SKIP TASK_ENTER_VEHICLE VS_FRONT_RIGHT")
									ELSE
										vCoords = vCoords + <<0.0, 3.0, -1.0>>
										SET_ENTITY_COORDS(localPlayerPed, vCoords)
										PRINTLN("[MB_MISSION] J_SKIP SET_ENTITY_COORDS VEH_POS 1 ")
									ENDIF
								ENDIF
							ELSE					
								IF HAS_MISSION_ENTITY_BEEN_CREATED(iNearestMissionEntity)
								AND NOT HAS_MISSION_ENTITY_BEEN_DELIVERED_OR_DESTROYED(iNearestMissionEntity)
									SET_ENTITY_COORDS(localPlayerPed, GET_ENTITY_COORDS(vNearestMissionEnt)+<<2.0, 2.0, 1.00>>)
									PRINTLN("[MB_MISSION] KEY_J, KEYBOARD_MODIFIER_NONE just released - eMODESTATE_COLLECT_ENTITY - Warping player to nearest mission entity")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELIF IS_DEBUG_KEY_JUST_RELEASED(KEY_J, KEYBOARD_MODIFIER_SHIFT, "J Skip (warp to vehicle)")
					IF GET_MEGA_BUSINESS_VARIATION() = MBV_SELL_PROTECT_BUYER
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[0].netId)
							SET_ENTITY_COORDS(localPlayerPed, GET_ENTITY_COORDS(NET_TO_PED(serverBD.sPed[0].netId), FALSE) + <<1.0, 1.0, 0.0>>)
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE eMODESTATE_COLLECT_VEHICLE
			
				IF IS_DEBUG_KEY_JUST_RELEASED(KEY_J, KEYBOARD_MODIFIER_NONE, "J Skip (warp to vehicle)")
				
					VEHICLE_INDEX vCargoBob
					
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[ciHOOKED_VEH_CARGOBOB].netId)
					
						vCargoBob = NET_TO_VEH(serverBD.sVehicle[ciHOOKED_VEH_CARGOBOB].netId)

						IF IS_ENTITY_ALIVE(vCargoBob)
						
							vCoords = GET_ENTITY_COORDS(vCargoBob, FALSE)
			
//							IF IS_ENTITY_AT_ENTITY(localPlayerPed, vCargoBob, <<5.0, 5.0, 5.0>>)

								IF IS_VEHICLE_SEAT_FREE(vCargoBob, VS_DRIVER)
									PRINTLN("[MB_MISSION] J_SKIP TASK_ENTER_VEHICLE")
									TASK_ENTER_VEHICLE(localPlayerPed, vCargoBob, -1, VS_DRIVER, PEDMOVEBLENDRATIO_RUN, ECF_WARP_PED)
								ELSE
									IF IS_VEHICLE_SEAT_FREE(vCargoBob, VS_FRONT_RIGHT)
										TASK_ENTER_VEHICLE(localPlayerPed, vCargoBob, -1, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_RUN, ECF_WARP_PED)
										PRINTLN("[MB_MISSION] J_SKIP TASK_ENTER_VEHICLE VS_FRONT_RIGHT")
									ELSE
										vCoords = vCoords + <<0.0, 3.0, -1.0>>
										SET_ENTITY_COORDS(localPlayerPed, vCoords)
										PRINTLN("[MB_MISSION] J_SKIP SET_ENTITY_COORDS VEH_POS 1 ")
									ENDIF
								ENDIF
//							ELSE					
//								SET_ENTITY_COORDS(localPlayerPed, GET_ENTITY_COORDS(vCargoBob)+<<2.0, 2.0, 1.00>>)
//								PRINTLN("[MB_MISSION] KEY_J, KEYBOARD_MODIFIER_NONE just released - eMODESTATE_COLLECT_ENTITY - Warping player to nearest mission entity")
//							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE eMODESTATE_COLLECT_PED
				IF IS_DEBUG_KEY_JUST_RELEASED(KEY_J, KEYBOARD_MODIFIER_NONE, "J Skip (warp to vehicle)")
					IF GET_MISSION_ENTITY_PLAYER_IS_HOLDING(localPlayerId) != -1
						IF HAS_MISSION_ENTITY_BEEN_CREATED(GET_MISSION_ENTITY_PLAYER_IS_HOLDING(localPlayerId))
						AND NOT HAS_MISSION_ENTITY_BEEN_DELIVERED_OR_DESTROYED(GET_MISSION_ENTITY_PLAYER_IS_HOLDING(localPlayerId))
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[GET_MISSION_ENTITY_PLAYER_IS_HOLDING(localPlayerId)].netId)
							AND IS_ENTITY_ALIVE(vMyMissionEntity)
								IF TAKE_CONTROL_OF_NET_ID(serverBD.sMissionEntity[GET_MISSION_ENTITY_PLAYER_IS_HOLDING(localPlayerId)].netId)
									INT iPed
									IF GET_MEGA_BUSINESS_VARIATION() = MBV_SNAPMATIC
										iPed = 1
									ENDIF
									vCoords = GET_MEGA_BUSINESS_PED_SPAWN_COORDS(iPed, GET_MEGA_BUSINESS_VARIATION(), GET_MEGA_BUSINESS_SUBVARIATION(), GET_RANDOM_MODE_VARIANT_FOR_ENTITY_LOOKUP()) + <<1.0,1.0,0.0>>
									SET_ENTITY_COORDS(vMyMissionEntity, vCoords)
									PRINTLN("[MB_MISSION] KEY_J, KEYBOARD_MODIFIER_NONE just released - eMODESTATE_COLLECT_PED - Warping player and mission entity to nearest drop off")
								ENDIF
							ENDIF
						ENDIF
					ELIF iNearestMissionEntity != -1
						IF HAS_MISSION_ENTITY_BEEN_CREATED(iNearestMissionEntity)
						AND NOT HAS_MISSION_ENTITY_BEEN_DELIVERED_OR_DESTROYED(iNearestMissionEntity)
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[iNearestMissionEntity].netId)
							AND IS_ENTITY_ALIVE(vNearestMissionEnt)
								SET_ENTITY_COORDS(localPlayerPed, GET_ENTITY_COORDS(vNearestMissionEnt)+<<0.0, 0.0, 1.0>>)
								PRINTLN("[MB_MISSION] KEY_J, KEYBOARD_MODIFIER_NONE just released - eMODESTATE_COLLECT_PED - Warping player to nearest mission entity")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE eMODESTATE_CAR_WASH
				IF IS_DEBUG_KEY_JUST_RELEASED(KEY_J, KEYBOARD_MODIFIER_NONE, "J Skip (warp to vehicle)")
					IF GET_MISSION_ENTITY_PLAYER_IS_HOLDING(localPlayerId) != -1
						IF HAS_MISSION_ENTITY_BEEN_CREATED(GET_MISSION_ENTITY_PLAYER_IS_HOLDING(localPlayerId))
						AND NOT HAS_MISSION_ENTITY_BEEN_DELIVERED_OR_DESTROYED(GET_MISSION_ENTITY_PLAYER_IS_HOLDING(localPlayerId))
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[GET_MISSION_ENTITY_PLAYER_IS_HOLDING(localPlayerId)].netId)
							AND IS_ENTITY_ALIVE(vMyMissionEntity)
								IF TAKE_CONTROL_OF_NET_ID(serverBD.sMissionEntity[GET_MISSION_ENTITY_PLAYER_IS_HOLDING(localPlayerId)].netId)
									SET_ENTITY_COORDS(vMyMissionEntity, <<-700.0872, -921.2989, 18.0139>>)
									SET_ENTITY_HEADING(vMyMissionEntity, 181.3260)
									PRINTLN("[MB_MISSION] KEY_J, KEYBOARD_MODIFIER_NONE just released - eMODESTATE_GO_TO_POINT - Warping player and mission entity to nearest drop off")
								ENDIF
							ENDIF
						ENDIF
					ELIF iNearestMissionEntity != -1
						IF HAS_MISSION_ENTITY_BEEN_CREATED(iNearestMissionEntity)
						AND NOT HAS_MISSION_ENTITY_BEEN_DELIVERED_OR_DESTROYED(iNearestMissionEntity)
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[iNearestMissionEntity].netId)
							AND IS_ENTITY_ALIVE(vNearestMissionEnt)
								SET_ENTITY_COORDS(localPlayerPed, GET_ENTITY_COORDS(vNearestMissionEnt)+<<0.0, 0.0, 1.0>>)
								PRINTLN("[MB_MISSION] KEY_J, KEYBOARD_MODIFIER_NONE just released - eMODESTATE_GO_TO_POINT - Warping player to nearest mission entity")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE eMODESTATE_REACH_SCORE
				IF IS_DEBUG_KEY_JUST_RELEASED(KEY_J, KEYBOARD_MODIFIER_NONE, "J Skip (eMODESTATE_REACH_SCORE)")
					playerBD[PARTICIPANT_ID_TO_INT()].fScore += 250.00 //= GET_TOTAL_SCORE_TO_REACH()
					sScore.fLocalScore = playerBD[PARTICIPANT_ID_TO_INT()].fScore
				ENDIF
			BREAK
			CASE eMODESTATE_POSTER
				IF IS_DEBUG_KEY_JUST_RELEASED(KEY_J, KEYBOARD_MODIFIER_NONE, "J Skip (warp to vehicle)")
					SET_ENTITY_COORDS(localPlayerPed, GET_POSTER_POINT_COORDS(GET_MEGA_BUSINESS_VARIATION(), GET_MEGA_BUSINESS_SUBVARIATION(), iMyNearestPoster)+<<1.0, 1.0, 0.25>>)
					SET_ENTITY_HEADING(localPlayerPed, GET_POSTER_POINT_HEADING(GET_MEGA_BUSINESS_VARIATION(), GET_MEGA_BUSINESS_SUBVARIATION(), iMyNearestPoster))
				ENDIF			
			BREAK
			CASE eMODESTATE_GO_TO_POINT
				IF IS_DEBUG_KEY_JUST_RELEASED(KEY_J, KEYBOARD_MODIFIER_NONE, "J Skip (warp to vehicle)")
				
					IF GET_MEGA_BUSINESS_VARIATION() = MBV_COLLECT_DJ_HOOKED
					AND serverBD.iCurrentGotoLocation = FIRST_GOTO_LOCATION
					AND HOOKED_IS_LOCAL_PLAYER_IN_CARGOBOB(TRUE)
					
						SWITCH GET_MEGA_BUSINESS_SUBVARIATION()
							CASE MBSV_COLLECT_HOOKED_0	vCoords = <<1258.1460, -3233.5940, 4.8014>> 	fHeading = 359.8082	BREAK
							CASE MBSV_COLLECT_HOOKED_1	vCoords = <<1040.5558, -210.9701, 69.2031>> 	fHeading = 224.0642 BREAK
							CASE MBSV_COLLECT_HOOKED_2	vCoords = <<842.8911, -2451.9163, 26.1527>> 	fHeading = 263.3397 BREAK
							CASE MBSV_COLLECT_HOOKED_3	vCoords = <<-1373.2894, -973.4222, 7.8169>> 	fHeading = 37.9947  BREAK
							CASE MBSV_COLLECT_HOOKED_4	vCoords = <<400.7294, -1019.0615, 28.4202>> 	fHeading = 2.2080  	BREAK
							CASE MBSV_COLLECT_HOOKED_5	vCoords = <<-591.0543, 134.4914, 59.6691>>		fHeading = 267.0058 BREAK
							CASE MBSV_COLLECT_HOOKED_6	vCoords = <<806.1404, -1797.8191, 28.3511>> 	fHeading = 352.8963 BREAK
							CASE MBSV_COLLECT_HOOKED_7	vCoords = <<-1310.6425, -490.5291, 32.1979>> 	fHeading = 35.2051 	BREAK
							CASE MBSV_COLLECT_HOOKED_8	vCoords = <<103.4492, -176.2581, 53.9960>> 		fHeading = 72.9146  BREAK
							CASE MBSV_COLLECT_HOOKED_9	vCoords =  <<162.0612, -872.0740, 29.7123>> 	fHeading = 156.6252 BREAK					
						ENDSWITCH
						
						SET_ENTITY_COORDS(GET_VEHICLE_PED_IS_USING(localPlayerPed), vCoords)
						SET_ENTITY_HEADING(GET_VEHICLE_PED_IS_USING(localPlayerPed), fHeading)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				
					ELIF GET_MISSION_ENTITY_PLAYER_IS_HOLDING(localPlayerId) != -1
						IF HAS_MISSION_ENTITY_BEEN_CREATED(GET_MISSION_ENTITY_PLAYER_IS_HOLDING(localPlayerId))
						AND NOT HAS_MISSION_ENTITY_BEEN_DELIVERED_OR_DESTROYED(GET_MISSION_ENTITY_PLAYER_IS_HOLDING(localPlayerId))
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[GET_MISSION_ENTITY_PLAYER_IS_HOLDING(localPlayerId)].netId)
							AND IS_ENTITY_ALIVE(vMyMissionEntity)
								IF TAKE_CONTROL_OF_NET_ID(serverBD.sMissionEntity[GET_MISSION_ENTITY_PLAYER_IS_HOLDING(localPlayerId)].netId)
									SET_ENTITY_COORDS(vMyMissionEntity, GET_MEGA_BUSINESS_GOTO_COORDS(GET_MEGA_BUSINESS_VARIATION(), GET_MEGA_BUSINESS_SUBVARIATION(), serverBD.nightclubID, GET_EXTRA_INT_PARAM_FOR_GOTO_COORDS_LOOKUP(), serverBD.iRandomVariant))
									PRINTLN("[MB_MISSION] KEY_J, KEYBOARD_MODIFIER_NONE just released - eMODESTATE_GO_TO_POINT - Warping player and mission entity to nearest drop off")
								ENDIF
							ENDIF
						ENDIF
					ELIF iNearestMissionEntity != -1
						IF HAS_MISSION_ENTITY_BEEN_CREATED(iNearestMissionEntity)
						AND NOT HAS_MISSION_ENTITY_BEEN_DELIVERED_OR_DESTROYED(iNearestMissionEntity)
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[iNearestMissionEntity].netId)
							AND IS_ENTITY_ALIVE(vNearestMissionEnt)
							
								SWITCH GET_MEGA_BUSINESS_VARIATION()
									CASE MBV_COLLECT_EQUIPMENT
										SET_ENTITY_COORDS(localPlayerPed, GET_ENTITY_COORDS(vNearestMissionEnt)+<<5.0, 5.0, 0.5>>)
										PRINTLN("[MB_MISSION] KEY_J, KEYBOARD_MODIFIER_NONE just released - eMODESTATE_GO_TO_POINT - Warping player to nearest mission entity")
									BREAK
									DEFAULT
										SET_ENTITY_COORDS(localPlayerPed, GET_ENTITY_COORDS(vNearestMissionEnt)+<<0.0, 0.0, 1.0>>)
										PRINTLN("[MB_MISSION] KEY_J, KEYBOARD_MODIFIER_NONE just released - eMODESTATE_GO_TO_POINT - Warping player to nearest mission entity")
									BREAK
								ENDSWITCH

							ENDIF
						ELSE
							SET_ENTITY_COORDS(localPlayerPed, GET_MEGA_BUSINESS_GOTO_COORDS(GET_MEGA_BUSINESS_VARIATION(), GET_MEGA_BUSINESS_SUBVARIATION(), serverBD.nightclubID, GET_EXTRA_INT_PARAM_FOR_GOTO_COORDS_LOOKUP(), serverBD.iRandomVariant))
						ENDIF
					ELSE
						SET_ENTITY_COORDS(localPlayerPed, GET_MEGA_BUSINESS_GOTO_COORDS(GET_MEGA_BUSINESS_VARIATION(), GET_MEGA_BUSINESS_SUBVARIATION(), serverBD.nightclubID, GET_EXTRA_INT_PARAM_FOR_GOTO_COORDS_LOOKUP(), serverBD.iRandomVariant))
					ENDIF
				ENDIF
			BREAK
			CASE eMODESTATE_DELIVER_ENTITY
				IF IS_DEBUG_KEY_JUST_RELEASED(KEY_J, KEYBOARD_MODIFIER_NONE, "J Skip (warp to vehicle)")
					IF GET_MISSION_ENTITY_PLAYER_IS_HOLDING(localPlayerId) != -1
						IF HAS_MISSION_ENTITY_BEEN_CREATED(GET_MISSION_ENTITY_PLAYER_IS_HOLDING(localPlayerId))
						AND NOT HAS_MISSION_ENTITY_BEEN_DELIVERED_OR_DESTROYED(GET_MISSION_ENTITY_PLAYER_IS_HOLDING(localPlayerId))
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[GET_MISSION_ENTITY_PLAYER_IS_HOLDING(localPlayerId)].netId)
							AND IS_ENTITY_ALIVE(vMyMissionEntity)
								IF TAKE_CONTROL_OF_NET_ID(serverBD.sMissionEntity[GET_MISSION_ENTITY_PLAYER_IS_HOLDING(localPlayerId)].netId)
									SET_ENTITY_COORDS(vMyMissionEntity, 
										GET_MEGA_BUSINESS_DROP_OFF_COORDS(serverBD.sMissionEntity[GET_MISSION_ENTITY_PLAYER_IS_HOLDING(localPlayerId)].eDropOffs[iMyNearestDropOff], serverBD.nightclubID, GET_MEGA_BUSINESS_SUBVARIATION(), GET_RANDOM_MODE_VARIANT_FOR_DROP_OFF_LOOKUP()))
									PRINTLN("[MB_MISSION] KEY_J, KEYBOARD_MODIFIER_NONE just released - eMODESTATE_DELIVER_ENTITY - Warping player and mission entity to nearest drop off")
								ENDIF
							ENDIF
						ENDIF
					ELIF iNearestMissionEntity != -1
						IF HAS_MISSION_ENTITY_BEEN_CREATED(iNearestMissionEntity)
						AND NOT HAS_MISSION_ENTITY_BEEN_DELIVERED_OR_DESTROYED(iNearestMissionEntity)
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[iNearestMissionEntity].netId)
							AND IS_ENTITY_ALIVE(vNearestMissionEnt)
								IF IS_ENTITY_AT_ENTITY(localPlayerPed, vNearestMissionEnt, <<5.0, 5.0, 5.0>>)
								AND DOES_VEHICLE_HAVE_FREE_SEAT(vNearestMissionEnt)
									TASK_ENTER_VEHICLE(localPlayerPed, vNearestMissionEnt, -1, GET_FIRST_FREE_VEHICLE_SEAT(vNearestMissionEnt), PEDMOVEBLENDRATIO_RUN, ECF_WARP_PED)
									PRINTLN("[MB_MISSION] KEY_J, KEYBOARD_MODIFIER_NONE just released - eMODESTATE_DELIVER_ENTITY - Warping player into first free seat")
								ELSE
									SET_ENTITY_COORDS(localPlayerPed, GET_ENTITY_COORDS(vNearestMissionEnt)+<<0.0, 0.0, 1.0>>)
									PRINTLN("[MB_MISSION] KEY_J, KEYBOARD_MODIFIER_NONE just released - eMODESTATE_DELIVER_ENTITY - Warping player to nearest mission entity")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE eMODESTATE_TAKE_OUT_TARGETS
				IF IS_DEBUG_KEY_JUST_RELEASED(KEY_J, KEYBOARD_MODIFIER_NONE, "J Skip eMODESTATE_TAKE_OUT_TARGETS")
					SWITCH GET_MEGA_BUSINESS_VARIATION()
						CASE MBV_SNAPMATIC
							IF iDeadPedCounter = 0
								iDeadPedCounter = 1
							ENDIF
							
							IF GET_COMMANDLINE_PARAM_EXISTS("scBBSellsDebug")
								IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[iDeadPedCounter].netId)
								AND NOT IS_ENTITY_DEAD(NET_TO_PED(serverBD.sPed[iDeadPedCounter].netId))
									SET_ENTITY_HEALTH(NET_TO_PED(serverBD.sPed[iDeadPedCounter].netId), 0)
									iDeadPedCounter++
								ENDIF
							ENDIF
							IF iDeadPedCounter >= NUM_MISSION_PEDS
								iDeadPedCounter = 0
							ENDIF
							
						BREAK
						CASE MBV_MUSCLE_OUT
							IF iNearestMissionEntity != -1
								IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[iNearestMissionEntity].netId)
								AND IS_ENTITY_ALIVE(vNearestMissionEnt)
									vCoords = GET_ENTITY_COORDS(vNearestMissionEnt,FALSE)
					
									IF IS_ENTITY_AT_ENTITY(localPlayerPed, vNearestMissionEnt, <<5.0, 5.0, 5.0>>)
									
										IF GET_COMMANDLINE_PARAM_EXISTS("scBBSellsDebug")
											IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[iDeadPedCounter].netId)
											AND NOT IS_ENTITY_DEAD(NET_TO_PED(serverBD.sPed[iDeadPedCounter].netId))
												SET_ENTITY_HEALTH(NET_TO_PED(serverBD.sPed[iDeadPedCounter].netId), 0)
												iDeadPedCounter++
											ENDIF
										ENDIF
										IF iDeadPedCounter >= NUM_MISSION_PEDS
											iDeadPedCounter = 0
										ENDIF
									ELSE					
										IF HAS_MISSION_ENTITY_BEEN_CREATED(iNearestMissionEntity)
										AND NOT HAS_MISSION_ENTITY_BEEN_DELIVERED_OR_DESTROYED(iNearestMissionEntity)
											SET_ENTITY_COORDS(localPlayerPed, GET_ENTITY_COORDS(vNearestMissionEnt)+<<2.0, 2.0, 1.00>>)
											PRINTLN("[MB_MISSION] KEY_J, KEYBOARD_MODIFIER_NONE just released - eMODESTATE_COLLECT_ENTITY - Warping player to nearest mission entity")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						
						BREAK
						CASE MBV_COLLECT_DJ_STOLEN_BAG
							IF GET_COMMANDLINE_PARAM_EXISTS("scBBSellsDebug")
								IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[iDeadPedCounter].netId)
								AND NOT IS_ENTITY_DEAD(NET_TO_PED(serverBD.sPed[iDeadPedCounter].netId))
									SET_ENTITY_HEALTH(NET_TO_PED(serverBD.sPed[iDeadPedCounter].netId), 0)
								ENDIF
								iDeadPedCounter++
							ENDIF
							IF iDeadPedCounter >= NUM_MISSION_PEDS
								iDeadPedCounter = 0
							ENDIF
						BREAK
						CASE MBV_COLLECT_DJ_COLLECTOR
							INT iProp
							REPEAT NUM_MISSION_PROPS iProp
								IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niProps[iProp])
								AND NOT IS_ENTITY_DEAD(NET_TO_ENT(serverBD.niProps[iProp]))
									SET_ENTITY_HEALTH(NET_TO_ENT(serverBD.niProps[iProp]), 0)
									PRINTLN("[MB_MISSION] KEY_J, KEYBOARD_MODIFIER_NONE just released - eMODESTATE_TAKE_OUT_TARGETS - Killing prop #", iProp)
									BREAKLOOP
								ENDIF
							ENDREPEAT
						BREAK
					ENDSWITCH			
				

				ENDIF
			BREAK
			CASE eMODESTATE_COLLECT_ITEM
				IF IS_DEBUG_KEY_JUST_RELEASED(KEY_J, KEYBOARD_MODIFIER_NONE, "J Skip eMODESTATE_COLLECT_ITEM")
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niFindItemPortablePickup[0])
					AND IS_ENTITY_ALIVE(NET_TO_VEH(serverBD.niFindItemPortablePickup[0]))
						SET_ENTITY_COORDS(localPlayerPed, GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niFindItemPortablePickup[0]))+<<1.0, 2.0, 0.0>>)
						PRINTLN("[MB_MISSION] KEY_J, KEYBOARD_MODIFIER_NONE just released - eMODESTATE_COLLECT_ITEM - Warping player to nearest niFindItemPortablePickup ")
					ENDIF
				ENDIF			
			BREAK
			CASE eMODESTATE_FOLLOW_ENTITY
				IF GET_MEGA_BUSINESS_VARIATION() = MBV_COLLECT_DJ_CRASH
					IF IS_DEBUG_KEY_JUST_RELEASED(KEY_J, KEYBOARD_MODIFIER_NONE, "J Skip eMODESTATE_FOLLOW_ENTITY")
						IF NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_WARPED_SOLOMUN_PLANE)
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[0].netId)
							AND NOT IS_ENTITY_DEAD(NET_TO_ENT(serverBD.sVehicle[0].netId))
								IF TAKE_CONTROL_OF_NET_ID(serverBD.sVehicle[0].netId)
									SET_ENTITY_COORDS(NET_TO_ENT(serverBD.sVehicle[0].netId), <<1334.8727, 3119.3840, 39.9832>>)
									SET_ENTITY_HEADING(NET_TO_ENT(serverBD.sVehicle[0].netId), 88.0974)
									CONTROL_LANDING_GEAR(NET_TO_VEH(serverBD.sVehicle[0].netId), LGC_DEPLOY_INSTANT)
									
									SET_CLIENT_BIT(eCLIENTBITSET_WARPED_SOLOMUN_PLANE)
									PRINTLN("[MB_MISSION] KEY_J, KEYBOARD_MODIFIER_NONE just released - eMODESTATE_FOLLOW_ENTITY - Warping plane to end point ")
								ENDIF
							ENDIF
						ENDIF	
						
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[0].netId)
						AND NOT IS_ENTITY_DEAD(NET_TO_ENT(serverBD.sPed[0].netId))
							IF TAKE_CONTROL_OF_NET_ID(serverBD.sPed[0].netId)
								SET_ENTITY_HEALTH(NET_TO_ENT(serverBD.sPed[0].netId), 0)
								PRINTLN("[MB_MISSION] KEY_J, KEYBOARD_MODIFIER_NONE just released - eMODESTATE_FOLLOW_ENTITY - Killed pilot ")
							ENDIF
						ENDIF
					ENDIF	
				ENDIF
			BREAK
		ENDSWITCH
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			IF serverBD.iSPassPart > (-1)
				PRINTLN("[MB_MISSION] serverBD.iSPassPart > (-1)")
				IF GET_MODE_STATE() < eMODESTATE_REWARDS
					SET_END_REASON(eENDREASON_MISSION_ENTITY_DELIVERED)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ENDIF
			ELIF serverBD.iFFailPart > (-1)
				PRINTLN("[MB_MISSION] serverBD.iFFailPart > (-1)")
				IF GET_MODE_STATE() < eMODESTATE_REWARDS
					SET_END_REASON(eENDREASON_MISSION_ENTITY_DESTROYED)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC
#ENDIF

FUNC INT GET_END_REASON_AS_TELEMTRY_TYPE(eEND_REASON eEndReason, BOOL bForcedEndOfMode)
	
	IF bForcedEndOfMode
		PRINTLN("[MB_MISSION] - GET_END_REASON_AS_TELEMTRY_TYPE - bExternallyForcedEndOfMode = TRUE, returning GB_TELEMETRY_END_FORCED")
		RETURN GB_TELEMETRY_END_FORCED
	ENDIF
	
	SWITCH eEndReason
		
		CASE eENDREASON_NO_BOSS_LEFT
			PRINTLN("[MB_MISSION] - [TELEMETRY] - GET_END_REASON_AS_TELEMTRY_TYPE - eEndReason = eENDREASON_NO_BOSS_LEFT, returning GB_TELEMETRY_END_BOSS_LEFT")
			RETURN GB_TELEMETRY_END_BOSS_LEFT	
		
		CASE eENDREASON_ALL_GOONS_LEFT	
			PRINTLN("[MB_MISSION] - [TELEMETRY] - GET_END_REASON_AS_TELEMTRY_TYPE - eEndReason = eENDREASON_ALL_GOONS_LEFT, returning GB_TELEMETRY_END_LOW_NUMBERS")
			RETURN GB_TELEMETRY_END_LOW_NUMBERS
		
		CASE eENDREASON_TIME_UP
			PRINTLN("[MB_MISSION] - [TELEMETRY] - GET_END_REASON_AS_TELEMTRY_TYPE - eEndReason = eENDREASON_TIME_UP, returning GB_TELEMETRY_END_TIME_OUT")
			RETURN GB_TELEMETRY_END_TIME_OUT
		
		CASE eENDREASON_MISSION_ENTITY_DELIVERED
		CASE eENDREASON_ALL_TARGETS_ELIMINATED
			PRINTLN("[MB_MISSION] - [TELEMETRY] - GET_END_REASON_AS_TELEMTRY_TYPE - eEndReason = eENDREASON_MISSION_ENTITY_DELIVERED or eENDREASON_ALL_TARGETS_ELIMINATED, returning GB_TELEMETRY_END_WON")
			RETURN GB_TELEMETRY_END_WON	
			
		CASE eENDREASON_MISSION_ENTITY_DESTROYED
			PRINTLN("[MB_MISSION] - [TELEMETRY] - GET_END_REASON_AS_TELEMTRY_TYPE - eEndReason = eENDREASON_MISSION_ENTITY_DESTROYED, returning GB_TELEMETRY_END_LOST")
			RETURN GB_TELEMETRY_END_LOST
		
	ENDSWITCH
	
	PRINTLN("[MB_MISSION] - [TELEMETRY] - GET_END_REASON_AS_TELEMTRY_TYPE - all other telemetry type checks failed, must be leaving of own choice, returning GB_TELEMETRY_END_LEFT")
	RETURN GB_TELEMETRY_END_LEFT
	
ENDFUNC

FUNC INT GET_TOTAL_TO_DELIVER()
	INT iLoop, iTotalToDeliver
	REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iLoop
		iTotalToDeliver += serverBD.sMissionEntity[iLoop].iDeliveriesToMake
		PRINTLN("[MB_MISSION] GET_TOTAL_TO_DELIVER - mission entity ", iLoop, " had to deliver ", serverBD.sMissionEntity[iLoop].iDeliveriesToMake, "; new overall total to be delivered: ", iTotalToDeliver)
	ENDREPEAT
	
	RETURN iTotalToDeliver
ENDFUNC

FUNC INT GET_TOTAL_DELIVERED()
	INT iLoop, iTotalDelivered
	REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iLoop
		iTotalDelivered += serverBD.sMissionEntity[iLoop].iDeliveriesMade
		PRINTLN("[MB_MISSION] GET_TOTAL_DELIVERED - mission entity ", iLoop, " delivered ", serverBD.sMissionEntity[iLoop].iDeliveriesMade, "; new overall total delivered: ", iTotalDelivered)
	ENDREPEAT
	
	RETURN iTotalDelivered
ENDFUNC

FUNC INT GET_UNITS_SOLD_FOR_TELEM()
	INT iTotal = serverBD.sHubSellMissionData.iGoodsTotal
	INT iPercent = (GET_TOTAL_DELIVERED() / GET_TOTAL_TO_DELIVER()) * 100
	INT iSold = (iTotal/100) * iPercent
	RETURN iSold
ENDFUNC

CONST_INT ciATTACK_TYPE_NOBODY 			-1
CONST_INT ciATTACK_TYPE_NON_AFFILIATED 	0
CONST_INT ciATTACK_TYPE_BIKER 			1
CONST_INT ciATTACK_TYPE_SECUROSERV	 	2

PROC SERVER_RECORD_ATTACK_TYPE_FOR_TELEMETRY(ENTITY_INDEX entityVictim, ENTITY_INDEX entityDamager)

	IF serverBD.iAttackType = -1
	
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
			BOOL bPlayerVictim = TRUE
		
			PED_INDEX victimPed 
			PED_INDEX damagerPed
			
			PLAYER_INDEX victimPlayer
			PLAYER_INDEX damagerPlayer
			
			IF DOES_ENTITY_EXIST(entityVictim)
			AND IS_ENTITY_A_PED(entityVictim)
				victimPed = GET_PED_INDEX_FROM_ENTITY_INDEX(entityVictim) 
				IF IS_PED_A_PLAYER(victimPed)
					victimPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(victimPed)
				ELSE
					bPlayerVictim = FALSE
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(entityDamager)
			AND IS_ENTITY_A_PED(entityDamager)
				damagerPed = GET_PED_INDEX_FROM_ENTITY_INDEX(entityDamager) 
				IF IS_PED_A_PLAYER(damagerPed)
					damagerPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(damagerPed)
				ENDIF
			ENDIF
		
			IF bPlayerVictim = FALSE
			OR ( IS_NET_PLAYER_OK(victimPlayer, FALSE) AND GB_IS_PLAYER_MEMBER_OF_THIS_GANG(victimPlayer, GB_GET_LOCAL_PLAYER_MISSION_HOST()) )
			
				IF IS_NET_PLAYER_OK(damagerPlayer, FALSE)
				AND NOT GB_IS_PLAYER_MEMBER_OF_THIS_GANG(damagerPlayer, GB_GET_LOCAL_PLAYER_MISSION_HOST())
				
					IF GB_IS_PLAYER_MEMBER_OF_GANG_TYPE(damagerPlayer, TRUE, GT_VIP)
						serverBD.iAttackType = ciATTACK_TYPE_SECUROSERV
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] SERVER_RECORD_ATTACK_TYPE_FOR_TELEMETRY = ciATTACK_TYPE_SECUROSERV ")
					ELIF GB_IS_PLAYER_MEMBER_OF_GANG_TYPE(damagerPlayer, TRUE, GT_BIKER)
						serverBD.iAttackType = ciATTACK_TYPE_BIKER
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] SERVER_RECORD_ATTACK_TYPE_FOR_TELEMETRY = ciATTACK_TYPE_BIKER ")
					ELSE
						serverBD.iAttackType = ciATTACK_TYPE_NON_AFFILIATED
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] SERVER_RECORD_ATTACK_TYPE_FOR_TELEMETRY = ciATTACK_TYPE_NON_AFFILIATED ")
					ENDIF
					
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] SERVER_RECORD_ATTACK_TYPE_FOR_TELEMETRY, damagerPlayer = ", GET_PLAYER_NAME(damagerPlayer))
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] SERVER_RECORD_ATTACK_TYPE_FOR_TELEMETRY, victimPlayer  = ", GET_PLAYER_NAME(victimPlayer))
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SERVER_TRACK_INNOCENTS_FOR_TELEMETRY()
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		serverBD.iInnocentsKilled++
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] SERVER_TRACK_INNOCENTS_FOR_TELEMETRY - serverBD.iInnocentsKilled = ", serverBD.iInnocentsKilled)
	ENDIF
ENDPROC

PROC SERVER_TRACK_PEDS_KILLED_FOR_TELEMETRY()
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		serverBD.iPedsKilledTelemetry++
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] SERVER_TRACK_PEDS_KILLED_FOR_TELEMETRY - serverBD.iPedsKilledTelemetry = ", serverBD.iPedsKilledTelemetry)
	ENDIF
ENDPROC

FUNC INT GET_ENEMY_GROUP_FOR_TELEMETRY()
	
	IF DOES_VARIATION_HAVE_GANG_CHASE()
		SWITCH GET_VARIATION_GANG_CHASE_PED_MODEL()
			
			CASE G_M_Y_MEXGOON_02
				RETURN ciENEMY_GROUP_VAGOS
			CASE G_M_Y_BALLAORIG_01
				RETURN ciENEMY_GROUP_BALLAS
			CASE G_M_M_CHIGOON_02
				RETURN ciENEMY_GROUP_TRIADS
			CASE G_M_Y_KOREAN_01
				RETURN ciENEMY_GROUP_KKANGPAE
			CASE G_M_Y_LOST_01
				RETURN ciENEMY_GROUP_LOST_MC
			CASE A_M_M_Hillbilly_01
				RETURN ciENEMY_GROUP_HILLBILLYS
			CASE mp_g_m_pros_01
				RETURN ciENEMY_GROUP_PROFESSIONALS
			CASE G_M_Y_MEXGANG_01
				RETURN ciENEMY_GROUP_MADRAZZO_CARTEL
			CASE S_M_Y_BLACKOPS_01
				RETURN ciENEMY_GROUP_MERRYWEATHER
			
		ENDSWITCH
	ENDIF
	
	RETURN ciENEMY_GROUP_NONE
ENDFUNC

CONST_INT PRODUCT_SELL_CHOICE_INVALID				-1
CONST_INT PRODUCT_SELL_CHOICE_ALL					0
CONST_INT PRODUCT_SELL_CHOICE_SPECIAL_BUYER			1
CONST_INT PRODUCT_SELL_CHOICE_SPECIFIC_PRODUCT		2

FUNC INT GET_PRODUCT_SELL_CHOICE()
	SWITCH serverBD.sHubSellMissionData.eSellType
		CASE MPT_SELL_ALL
			RETURN PRODUCT_SELL_CHOICE_ALL
		
		CASE MPT_SELL_TO_SPECIAL_BUYER_1
		CASE MPT_SELL_TO_SPECIAL_BUYER_2
		CASE MPT_SELL_TO_SPECIAL_BUYER_3
			RETURN PRODUCT_SELL_CHOICE_SPECIAL_BUYER
			
		CASE MPT_SELL_CARGO
		CASE MPT_SELL_WEAPONS
		CASE MPT_SELL_COKE
		CASE MPT_SELL_METH
		CASE MPT_SELL_WEED
		CASE MPT_SELL_FORGED_DOCS
		CASE MPT_SELL_COUNTERFEIT_CASH
			RETURN PRODUCT_SELL_CHOICE_SPECIFIC_PRODUCT
	ENDSWITCH
	
	RETURN PRODUCT_SELL_CHOICE_INVALID
ENDFUNC

PROC EXECUTE_COMMON_END_TELEMTRY(BOOL bIwon, BOOL bForcedEndOfMode)
	
	IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_CALLED_COMMON_SETUP)
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - Not processing telemetry, mission was launched and killed before GB_COMMON_BOSS_MISSION_SETUP was called.")
		EXIT
	ENDIF
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - NETWORK_IS_GAME_IN_PROGRESS = FALSE, not setting telemetry")
		EXIT
	ENDIF
	
	IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_EXECUTED_END_TELEMTRY)
	
		// MBS Telemetry
		g_sGb_Telemetry_data.sdata.m_match1						= serverBD.iMatchId1
		g_sGb_Telemetry_data.sdata.m_match2 					= serverBD.iMatchId2
	
		IF IS_MEGA_BUSINESS_VARIATION_A_SETUP_MISSION(GET_MEGA_BUSINESS_VARIATION())
		OR IS_MEGA_BUSINESS_VARIATION_A_CLUB_MANAGEMENT_MISSION(GET_MEGA_BUSINESS_VARIATION())
	
			g_sFM_NIGHTCLUB_Telemetry_data.m_attacktype								= serverBD.iAttackType
//			g_sFM_NIGHTCLUB_Telemetry_data.m_Deaths									= iLocalDeathsForTelem
			g_sFM_NIGHTCLUB_Telemetry_data.m_TargetsKilled							= serverBD.iPedsKilledTelemetry
			g_sFM_NIGHTCLUB_Telemetry_data.m_InnocentsKilled						= serverBD.iInnocentsKilled

			g_sFM_NIGHTCLUB_Telemetry_data.m_PlayersLeftInProgress					= MPGlobalsAmbience.iNumPlayersLeftDuringBusinessBattle
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - g_sFM_NIGHTCLUB_Telemetry_data.m_PlayersLeftInProgress	= ", g_sFM_NIGHTCLUB_Telemetry_data.m_PlayersLeftInProgress)

			g_sFM_NIGHTCLUB_Telemetry_data.m_ambushvehicle 							= ENUM_TO_INT(GET_VARIATION_GANG_CHASE_VEHICLE_MODEL())
//			g_sFM_NIGHTCLUB_Telemetry_data.m_PlayerRole								= GET_PLAYER_ROLE_FOR_TELEMETRY()
			g_sFM_NIGHTCLUB_Telemetry_data.m_LauncherRank							= ENUM_TO_INT(GB_GET_PLAYER_GANG_ROLE(GB_GET_LOCAL_PLAYER_MISSION_HOST()))
			g_sFM_NIGHTCLUB_Telemetry_data.m_Location								= ENUM_TO_INT(GET_MEGA_BUSINESS_SUBVARIATION())
			g_sFM_NIGHTCLUB_Telemetry_data.m_enemygroup								= GET_ENEMY_GROUP_FOR_TELEMETRY()
			g_sFM_NIGHTCLUB_Telemetry_data.m_Won									= BOOL_TO_INT(bIwon)
	
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - g_sFM_NIGHTCLUB_Telemetry_data.m_attacktype 				= ", g_sFM_NIGHTCLUB_Telemetry_data.m_attacktype)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - g_sFM_NIGHTCLUB_Telemetry_data.m_Deaths 					= ", g_sFM_NIGHTCLUB_Telemetry_data.m_Deaths)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - g_sFM_NIGHTCLUB_Telemetry_data.m_TargetsKilled 			= ", g_sFM_NIGHTCLUB_Telemetry_data.m_TargetsKilled)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - g_sFM_NIGHTCLUB_Telemetry_data.m_InnocentsKilled 			= ", g_sFM_NIGHTCLUB_Telemetry_data.m_InnocentsKilled)
			
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - g_sFM_NIGHTCLUB_Telemetry_data.m_ambushvehicle 			= ", g_sFM_NIGHTCLUB_Telemetry_data.m_ambushvehicle)
//			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - g_sFM_NIGHTCLUB_Telemetry_data.m_PlayerRole 				= ", g_sFM_NIGHTCLUB_Telemetry_data.m_PlayerRole)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - g_sFM_NIGHTCLUB_Telemetry_data.m_LauncherRank 				= ", g_sFM_NIGHTCLUB_Telemetry_data.m_LauncherRank)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - g_sFM_NIGHTCLUB_Telemetry_data.m_Location 					= ", g_sFM_NIGHTCLUB_Telemetry_data.m_Location)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - g_sFM_NIGHTCLUB_Telemetry_data.m_enemygroup 				= ", g_sFM_NIGHTCLUB_Telemetry_data.m_enemygroup)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - g_sFM_NIGHTCLUB_Telemetry_data.m_Won 						= ", g_sFM_NIGHTCLUB_Telemetry_data.m_Won)
		
		ELIF IS_MEGA_BUSINESS_VARIATION_A_SELL_MISSION(GET_MEGA_BUSINESS_VARIATION())
		
			g_sFM_WAREHOUSE_SELL_Telemetry_data.m_attacktype						= serverBD.iAttackType
		//	g_sFM_WAREHOUSE_SELL_Telemetry_data.m_Deaths							= iLocalDeathsForTelem
			g_sFM_WAREHOUSE_SELL_Telemetry_data.m_TargetsKilled						= serverBD.iPedsKilledTelemetry
			g_sFM_WAREHOUSE_SELL_Telemetry_data.m_InnocentsKilled					= serverBD.iInnocentsKilled
			g_sFM_WAREHOUSE_SELL_Telemetry_data.m_PlayersLeftInProgress				= serverBD.iOrganisationSizeOnLaunch - (GB_GET_NUM_GOONS_IN_LOCAL_GANG()+1)
			g_sFM_WAREHOUSE_SELL_Telemetry_data.m_ambushvehicle 					= ENUM_TO_INT(GET_VARIATION_GANG_CHASE_VEHICLE_MODEL())
//			g_sFM_WAREHOUSE_SELL_Telemetry_data.m_PlayerRole						= GET_PLAYER_ROLE_FOR_TELEMETRY()
			g_sFM_WAREHOUSE_SELL_Telemetry_data.m_LauncherRank						= ENUM_TO_INT(GB_GET_PLAYER_GANG_ROLE(GB_GET_LOCAL_PLAYER_MISSION_HOST()))
			g_sFM_WAREHOUSE_SELL_Telemetry_data.m_Location							= ENUM_TO_INT(GET_MEGA_BUSINESS_SUBVARIATION())
			g_sFM_WAREHOUSE_SELL_Telemetry_data.m_enemygroup						= GET_ENEMY_GROUP_FOR_TELEMETRY()
			g_sFM_WAREHOUSE_SELL_Telemetry_data.m_deliveryproduct					= ENUM_TO_INT(serverBD.sHubSellMissionData.eSellType)
			g_sFM_WAREHOUSE_SELL_Telemetry_data.m_shipmentsize						= ENUM_TO_INT(serverBD.eShipmentSize)
			g_sFM_WAREHOUSE_SELL_Telemetry_data.m_deliveryvehicle					= ENUM_TO_INT(GET_MB_ENTITY_MODEL_FROM_SHIPMENT_SIZE(serverBD.eShipmentSize))
			g_sFM_WAREHOUSE_SELL_Telemetry_data.m_ProductsOwned						= serverBD.sHubSellMissionData.iGoodsTotal
			g_sFM_WAREHOUSE_SELL_Telemetry_data.m_SuppliesOwned						= serverBD.sHubSellMissionData.iGoodsTotal
//			g_sFM_WAREHOUSE_SELL_Telemetry_data.m_totalunitssold					= GET_UNITS_SOLD_FOR_TELEM()
			g_sFM_WAREHOUSE_SELL_Telemetry_data.m_productssellchoice				= GET_PRODUCT_SELL_CHOICE()
			g_sFM_WAREHOUSE_SELL_Telemetry_data.m_Won								= BOOL_TO_INT(bIwon)
		
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - g_sFM_WAREHOUSE_SELL_Telemetry_data.m_attacktype 				= ", g_sFM_WAREHOUSE_SELL_Telemetry_data.m_attacktype)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - g_sFM_WAREHOUSE_SELL_Telemetry_data.m_Deaths 					= ", g_sFM_WAREHOUSE_SELL_Telemetry_data.m_Deaths)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - g_sFM_WAREHOUSE_SELL_Telemetry_data.m_TargetsKilled 			= ", g_sFM_WAREHOUSE_SELL_Telemetry_data.m_TargetsKilled)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - g_sFM_WAREHOUSE_SELL_Telemetry_data.m_InnocentsKilled 			= ", g_sFM_WAREHOUSE_SELL_Telemetry_data.m_InnocentsKilled)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - g_sFM_WAREHOUSE_SELL_Telemetry_data.m_PlayersLeftInProgress 	= ", g_sFM_WAREHOUSE_SELL_Telemetry_data.m_PlayersLeftInProgress)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - g_sFM_WAREHOUSE_SELL_Telemetry_data.m_ambushvehicle 			= ", g_sFM_WAREHOUSE_SELL_Telemetry_data.m_ambushvehicle)
//			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - g_sFM_WAREHOUSE_SELL_Telemetry_data.m_PlayerRole 				= ", g_sFM_WAREHOUSE_SELL_Telemetry_data.m_PlayerRole)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - g_sFM_WAREHOUSE_SELL_Telemetry_data.m_LauncherRank 			= ", g_sFM_WAREHOUSE_SELL_Telemetry_data.m_LauncherRank)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - g_sFM_WAREHOUSE_SELL_Telemetry_data.m_Location 				= ", g_sFM_WAREHOUSE_SELL_Telemetry_data.m_Location)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - g_sFM_WAREHOUSE_SELL_Telemetry_data.m_enemygroup 				= ", g_sFM_WAREHOUSE_SELL_Telemetry_data.m_enemygroup)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - g_sFM_WAREHOUSE_SELL_Telemetry_data.m_deliveryproduct 			= ", g_sFM_WAREHOUSE_SELL_Telemetry_data.m_deliveryproduct)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - g_sFM_WAREHOUSE_SELL_Telemetry_data.m_shipmentsize 			= ", g_sFM_WAREHOUSE_SELL_Telemetry_data.m_shipmentsize)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - g_sFM_WAREHOUSE_SELL_Telemetry_data.m_deliveryvehicle 			= ", g_sFM_WAREHOUSE_SELL_Telemetry_data.m_deliveryvehicle)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - g_sFM_WAREHOUSE_SELL_Telemetry_data.m_ProductsOwned 			= ", g_sFM_WAREHOUSE_SELL_Telemetry_data.m_ProductsOwned)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - g_sFM_WAREHOUSE_SELL_Telemetry_data.m_SuppliesOwned 			= ", g_sFM_WAREHOUSE_SELL_Telemetry_data.m_SuppliesOwned)
//			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - g_sFM_WAREHOUSE_SELL_Telemetry_data.m_totalunitssold 			= ", g_sFM_WAREHOUSE_SELL_Telemetry_data.m_totalunitssold)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - g_sFM_WAREHOUSE_SELL_Telemetry_data.m_productssellchoice 		= ", g_sFM_WAREHOUSE_SELL_Telemetry_data.m_productssellchoice)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - g_sFM_WAREHOUSE_SELL_Telemetry_data.m_Won 						= ", g_sFM_WAREHOUSE_SELL_Telemetry_data.m_Won)
		ELSE
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - Not setting telemetry")
		ENDIF
		
		// Not needed
		
//		g_sFM_WAREHOUSE_SELL_Telemetry_data.m_SuppliesStolen
//		g_sFM_WAREHOUSE_SELL_Telemetry_data.m_quickrestart
//		g_sFM_WAREHOUSE_SELL_Telemetry_data.m_timeusingdrone
//		g_sFM_WAREHOUSE_SELL_Telemetry_data.m_dronedestroyed
//		g_sFM_WAREHOUSE_SELL_Telemetry_data.m_totaldronetases
		
		GB_SET_COMMON_TELEMETRY_DATA_ON_END(bIWon, GET_END_REASON_AS_TELEMTRY_TYPE(GET_END_REASON(), bForcedEndOfMode))
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - called GB_SET_COMMON_TELEMETRY_DATA_ON_END(", bIWon,", ", GET_END_REASON_AS_TELEMTRY_TYPE(GET_END_REASON(), bForcedEndOfMode), ")")
		
		SET_LOCAL_BIT(eLOCALBITSET_EXECUTED_END_TELEMTRY)
		
	ENDIF
ENDPROC

FUNC BOOL SHOULD_DELAY_SCRIPT_END()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_CRASH			RETURN TRUE
		CASE MBV_COLLECT_DJ_HOOKED			RETURN TRUE
		CASE MBV_COLLECT_DJ_COLLECTOR		RETURN TRUE
		CASE MBV_COLLECT_DJ_STOLEN_BAG		RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC MP_DAILY_OBJECTIVES GET_MP_DAILY_OBJEVTIVE_FROM_GOODS_TYPE(HUB_SELL_MISSION_PRODUCT_TYPE eGoodsType)
	SWITCH eGoodsType
		CASE MPT_SELL_WEAPONS					RETURN MP_DAILY_AM_SELL_WEAPONS
		CASE MPT_SELL_COKE						RETURN MP_DAILY_AM_SELL_COCAINE
		CASE MPT_SELL_METH						RETURN MP_DAILY_AM_SELL_METH
		CASE MPT_SELL_WEED						RETURN MP_DAILY_AM_SELL_WEED
		CASE MPT_SELL_FORGED_DOCS				RETURN MP_DAILY_AM_SELL_DOCUMENTS
		CASE MPT_SELL_COUNTERFEIT_CASH			RETURN MP_DAILY_AM_SELL_CASH
	ENDSWITCH
	
	RETURN MP_DAILY_MAX
ENDFUNC

SCRIPT_TIMER stEndDelay
PROC HANDLE_REWARDS()
	IF SHOULD_DELAY_SCRIPT_END()
		IF NOT HAS_NET_TIMER_EXPIRED(stEndDelay, 1000)
			EXIT
		ENDIF
	ENDIF
	
	BOOL bIWon
	
	IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_CALCULATED_REWARDS)
		
		PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - HANDLE_REWARDS - Not calculated rewards")
		
		IF GET_END_REASON() != eENDREASON_NO_REASON_YET
		
			PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - HANDLE_REWARDS - GET_END_REASON() != eENDREASON_NO_REASON_YET")
		
			IF bLocalPlayerOk
		
				PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - HANDLE_REWARDS - bLocalPlayerOk")
		
				IF (IS_SCREEN_FADED_IN()
				AND NOT IS_SCREEN_FADING_IN())
				OR GET_MEGA_BUSINESS_VARIATION() = MBV_SUPPLY_RUN
				OR GET_MEGA_BUSINESS_VARIATION() = MBV_STOLEN_SUPPLIES
				OR GET_MEGA_BUSINESS_VARIATION() = MBV_COLLECT_DJ_COLLECTOR
				OR GET_MEGA_BUSINESS_VARIATION() = MBV_COLLECT_DJ_CRASH
				OR GET_MEGA_BUSINESS_VARIATION() = MBV_COLLECT_DJ_HOOKED
				OR GET_MEGA_BUSINESS_VARIATION() = MBV_COLLECT_DJ_STOLEN_BAG
				
					PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - HANDLE_REWARDS - IS_SCREEN_FADED_IN() AND NOT IS_SCREEN_FADING_IN()")
		
					IF GB_IS_IT_SAFE_TO_DISPLAY_BOSS_MISSION_UI(DEFAULT, DEFAULT, bUICheckTutorialSession)
					
						PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - HANDLE_REWARDS - MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI")
							
						IF NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
						AND NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE()
						
							PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - HANDLE_REWARDS - NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE() AND NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE()")
						
							IF IS_SAFE_FOR_BIG_MESSAGE()
							
								PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - HANDLE_REWARDS - IS_SAFE_FOR_BIG_MESSAGE()")
								
								IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_DONE_END_SHARD)
								
									SWITCH GET_END_REASON()
										CASE eENDREASON_NO_BOSS_LEFT
											PRINTLN("[MB_MISSION] - no boss left on script.")
											CLEAR_HELP()
											// Display end shard here.
										BREAK
										CASE eENDREASON_ALL_GOONS_LEFT
											PRINTLN("[MB_MISSION] - no goons left on script")
											CLEAR_HELP()
											// Display end shard here.
										BREAK
										CASE eENDREASON_TIME_UP
											PRINTLN("[MB_MISSION] - mode time expired")
											CLEAR_HELP()
											DO_BIG_MESSAGE(eBIGMESSAGE_END_TIME_EXPIRED)
										BREAK
										CASE eENDREASON_MISSION_ENTITY_DELIVERED
											PRINTLN("[MB_MISSION] - mission entity delivered")
											CLEAR_HELP()
											IF GET_TOTAL_DELIVERIES_MADE() = GET_TOTAL_DELIVERIES_TO_MAKE()
												IF SHOULD_SHOW_PASS_SHARD()
													DO_BIG_MESSAGE(eBIGMESSAGE_END_DELIVERED)
												ENDIF
											ELSE	
												DO_BIG_MESSAGE(eBIGMESSAGE_END_PARTIAL_DELIVERED)
											ENDIF
										BREAK
										CASE eENDREASON_MISSION_ENTITY_DESTROYED
											PRINTLN("[MB_MISSION] - mission entity destroyed")
											CLEAR_HELP()
											DO_BIG_MESSAGE(eBIGMESSAGE_END_DESTROYED)
										BREAK
										CASE eENDREASON_ALL_TARGETS_ELIMINATED
											PRINTLN("[MB_MISSION] - mission entity destroyed")
											CLEAR_HELP()
											DO_BIG_MESSAGE(eBIGMESSAGE_END_TARGETS_ELIMINATED)
										BREAK
									ENDSWITCH
									
									SET_LOCAL_BIT(eLOCALBITSET_DONE_END_SHARD)
								ENDIF
							ELSE
								PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - HANDLE_REWARDS - Not safe for big message, setting eLOCALBITSET_DONE_END_SHARD regardless to ensure we're warped into the club if needed")
								SET_LOCAL_BIT(eLOCALBITSET_DONE_END_SHARD)
							ENDIF
							
						ELSE
							PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - HANDLE_REWARDS - Player is restricted for hide or passive, not showing end shard")
						ENDIF
						
					ELSE
						PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - HANDLE_REWARDS - Not safe for boss mission UI, setting eLOCALBITSET_DONE_END_SHARD regardless to ensure we're warped into the club if needed")
						SET_LOCAL_BIT(eLOCALBITSET_DONE_END_SHARD)
					ENDIF
					
					PLAYER_INDEX playerId = localPlayerId // Set the winning player here before passing into wanted cleanup function. Using localPlayerId as placeholder right now.
					IF NOT MODE_TIME_EXPIRED_BEFORE_LEAVE_AREA()
						GANG_BOSS_MANAGE_WANTED_CLEAN_UP(TRUE, playerId)
					ENDIF
					
					IF GET_END_REASON() = eENDREASON_MISSION_ENTITY_DELIVERED
					OR GET_END_REASON() = eENDREASON_ALL_TARGETS_ELIMINATED
						PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - HANDLE_REWARDS - bIWon = TRUE")
						bIWon = TRUE
					ELSE
						bIWon = FALSE
						PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - HANDLE_REWARDS - bIWon = FALSE")
					ENDIF
					
					IF bIWon
						// Do any extra processing here for if the locla player won, before passing into GANG_BOSS_MANAGE_REWARDS
						// sGangBossManageRewardsData etc.
						
						SWITCH GET_MEGA_BUSINESS_VARIATION()
							CASE MBV_SELL_NOT_A_SCRATCH
								IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
									INT iMissionEntity, iCashBonus
									REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
										IF HAS_MISSION_ENTITY_BEEN_CREATED(iMissionEntity)
										AND HAS_MISSION_ENTITY_BEEN_DELIVERED(iMissionEntity)
										AND NOT HAS_MISSION_ENTITY_BEEN_DESTROYED(iMissionEntity)
											iCashBonus += serverBD.sMissionEntity[iMissionEntity].iBonusCash
										ENDIF
									ENDREPEAT
									
									IF iCashBonus > 0
										sGangBossManageRewardsData.iExtraCash = iCashBonus
										PRINT_TICKER_WITH_INT("MBS_TK_BON", sGangBossManageRewardsData.iExtraCash)
										PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - HANDLE_REWARDS - Receiving ", sGangBossManageRewardsData.iExtraCash, " as bonus cash")
									ENDIF
								ENDIF
							BREAK
							CASE MBV_SELL_PROTECT_BUYER
								IF IS_SERVER_BIT_SET(eSERVERBITSET_FAILED_TO_PROTECT_BUYER)
									sGangBossManageRewardsData.bCashDeductConditionMet = TRUE
								ENDIF
							BREAK
						ENDSWITCH
						
						IF IS_MEGA_BUSINESS_VARIATION_A_CLUB_MANAGEMENT_MISSION(GET_MEGA_BUSINESS_VARIATION())
							IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
								INCREMENT_TOTAL_NIGHTCLUB_MANAGEMENT_JOBS_COMPLETED(1)
								PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - HANDLE_REWARDS - INCREMENT_TOTAL_NIGHTCLUB_MANAGEMENT_JOBS_COMPLETED(1)")
							ENDIF
						ELIF IS_MEGA_BUSINESS_VARIATION_A_SELL_MISSION(GET_MEGA_BUSINESS_VARIATION())
							IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
								INCREMENT_TOTAL_HUB_SELL_MISSIONS_COMPLETED(1)
								PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - HANDLE_REWARDS - INCREMENT_TOTAL_HUB_SELL_MISSIONS_COMPLETED(1)")
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_MEGA_BUSINESS_VARIATION_A_SELL_MISSION(GET_MEGA_BUSINESS_VARIATION())
						sGangBossManageRewardsData.iNumDropsMade = GET_TOTAL_DELIVERED()
						sGangBossManageRewardsData.iTotalDrops = GET_TOTAL_TO_DELIVER()
						PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - HANDLE_REWARDS - sGangBossManageRewardsData.iNumDropsMade = ", sGangBossManageRewardsData.iNumDropsMade)
						PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - HANDLE_REWARDS - sGangBossManageRewardsData.iTotalDrops = ", sGangBossManageRewardsData.iTotalDrops)
					ELIF IS_MEGA_BUSINESS_VARIATION_A_SETUP_MISSION(GET_MEGA_BUSINESS_VARIATION())
						sGangBossManageRewardsData.bIsSetupMission = TRUE
						PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - HANDLE_REWARDS - sGangBossManageRewardsData.bIsSetupMission = TRUE")
					ENDIF
					
					sGangBossManageRewardsData.iNightclubID = ENUM_TO_INT(serverBD.nightclubID)
					
					GANG_BOSS_MANAGE_REWARDS(serverBD.iFMMCType, bIWon, sGangBossManageRewardsData)
					
					EXECUTE_COMMON_END_TELEMTRY(bIWon, FALSE)
					
					SET_LOCAL_BIT(eLOCALBITSET_CALCULATED_REWARDS)
					
					SET_CLIENT_BIT(eCLIENTBITSET_COMPLETED_REWARDS)
					
				ENDIF
				
			ENDIF
			
		ENDIF
	ELSE
		IF IS_MEGA_BUSINESS_VARIATION_A_SELL_MISSION(GET_MEGA_BUSINESS_VARIATION())
			IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
				#IF IS_DEBUG_BUILD
				IF HAS_MISSION_BEEN_DEBUG_PASSED_OR_FAILED()
					PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - HANDLE_REWARDS - HAS_MISSION_BEEN_DEBUG_PASSED_OR_FAILED, eCLIENTBITSET_I_HAVE_REMOVED_GOODS_FROM_BUSINESS_HUB SET")
					SET_CLIENT_BIT(eCLIENTBITSET_I_HAVE_REMOVED_GOODS_FROM_BUSINESS_HUB)
				ELSE
				#ENDIF
					IF NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_HAVE_REMOVED_GOODS_FROM_BUSINESS_HUB)
						//Set this for sell missions - Value ignored for buy missions
						REMOVE_CONTRABAND_REASON_ENUM eReason = REMOVE_CONTRA_MISSION_FAILED
						FLOAT fSuccessRate = TO_FLOAT(GET_TOTAL_DELIVERED()) / TO_FLOAT(GET_TOTAL_TO_DELIVER())
						
						IF GET_END_REASON() = eENDREASON_MISSION_ENTITY_DELIVERED
							eReason = REMOVE_CONTRA_MISSION_PASSED
						ENDIF
						
						IF REMOVE_PRODUCT_FROM_BUSINESS_HUB_USING_SELL_MISSION_DATA(GlobalplayerBD_FM[NETWORK_PLAYER_ID_TO_INT()].sHubSellMissionData, eReason, eResult, FALSE, MPGlobalsAmbience.sMagnateGangBossData.iContrabandSellEarnings, fSuccessRate)
							
							IF eResult = CONTRABAND_TRANSACTION_STATE_SUCCESS
							OR eResult = CONTRABAND_TRANSACTION_STATE_FAILED
								PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - HANDLE_REWARDS - Contraband transaction completed. Result: ", ENUM_TO_INT(eResult))
								SET_CLIENT_BIT(eCLIENTBITSET_I_HAVE_REMOVED_GOODS_FROM_BUSINESS_HUB)
								IF eReason = REMOVE_CONTRA_MISSION_PASSED
									SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_SELL_NIGHTCLUB_GOODS)
									
									IF g_sMPTunables.bENABLE_FIRST_PAYOUT_GOODS_BOOST_BUSINESS_BATTLES
										SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SUM22_FIRST_PAYOUT_BOOST_DONE_BUSINESS_BATTLES, TRUE)
									ENDIF
									IF g_sMPTunables.bENABLE_FIRST_PAYOUT_GOODS_BOOST_NIGHTCLUB_SOURCE
										SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SUM22_FIRST_PAYOUT_BOOST_DONE_NIGHTCLUB_SOURCE, TRUE)
									ENDIF
									
									PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - HANDLE_REWARDS - SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_SELL_NIGHTCLUB_GOODS)")
									IF GlobalplayerBD_FM[NETWORK_PLAYER_ID_TO_INT()].sHubSellMissionData.eSellType = MPT_SELL_CARGO
										SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_SELL_AIR_FREIGHT_CARGO)
										SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_SELL_SPECIAL_CARGO)
										PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - HANDLE_REWARDS - SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_SELL_AIR_FREIGHT_CARGO)")
										PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - HANDLE_REWARDS - SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_SELL_SPECIAL_CARGO)")
									ELIF GET_MP_DAILY_OBJEVTIVE_FROM_GOODS_TYPE(GlobalplayerBD_FM[NETWORK_PLAYER_ID_TO_INT()].sHubSellMissionData.eSellType) != MP_DAILY_MAX
										SET_MP_DAILY_OBJECTIVE_COMPLETE(GET_MP_DAILY_OBJEVTIVE_FROM_GOODS_TYPE(GlobalplayerBD_FM[NETWORK_PLAYER_ID_TO_INT()].sHubSellMissionData.eSellType))
										PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - HANDLE_REWARDS - SET_MP_DAILY_OBJECTIVE_COMPLETE(GET_MP_DAILY_OBJEVTIVE_FROM_GOODS_TYPE(GlobalplayerBD_FM[NETWORK_PLAYER_ID_TO_INT()].sHubSellMissionData.eSellType))")
									ENDIF
								ENDIF
							ELSE
								PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - HANDLE_REWARDS - Waiting for Contraband transaction to completed. Result: ", ENUM_TO_INT(eResult))
							ENDIF
						ENDIF
					ENDIF
				#IF IS_DEBUG_BUILD
				ENDIF
				#ENDIF
			ENDIF
		
		ENDIF
	ENDIF
	
//	IF GB_MAINTAIN_BOSS_END_UI(sEndUI)
//		SET_CLIENT_BIT(eCLIENTBITSET_COMPLETED_REWARDS)
//	ENDIF
	
ENDPROC

PROC DEAL_WITH_VEHICLE_GENERATORS(BOOL bActive)
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_SUPPLY_RUN
			SWITCH GET_MEGA_BUSINESS_SUBVARIATION()
				CASE MBSV_SUPPLY_RUN_1
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1832.95129, 775.47760, 134.98502>>, <<-1812.77759, 788.70941, 140.75195>>, bActive) 

			
					PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
				BREAK
				CASE MBSV_SUPPLY_RUN_2
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1721.91956, 4942.15088, 41.02951>>, <<1711.23889, 4934.28369, 44.68797>>, bActive)
	
					PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
				BREAK
				CASE MBSV_SUPPLY_RUN_9
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-2200.83960, 4276.91455, 46.48012>>, <<-2188.23584, 4263.83008, 50.78886>>, bActive)
		
					PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
				BREAK
			ENDSWITCH
		BREAK
		CASE MBV_MUSCLE_OUT
			SWITCH GET_MEGA_BUSINESS_SUBVARIATION()
				CASE MBSV_MUSCLE_OUT_0		
//					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<454.86731, -1693.70776, 27.28749>>, <<469.95657, -1669.58911, 32.53831>>, bActive)
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<810.19519, -766.37341, 24.72731>>, <<828.44019, -746.22357, 30.41972>>, bActive) 	
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1154.73376, -1310.89124, 32.70103>>, <<1167.63782, -1325.63049, 37.74274>>, bActive) 					

					PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
				BREAK
//				CASE MBSV_MUSCLE_OUT_1	
//					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<461.25598, -572.83582, 26.65421>>, <<471.45712, -590.78424, 30.49958>>, bActive) 		
//					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<428.01178, -1533.77393, 27.26931>>, <<449.18823, -1512.08740, 32.18638>>, bActive) 	
//					PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
//				BREAK
				CASE MBSV_MUSCLE_OUT_2
//					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-591.66968, -883.21252, 23.94213>>, <<-599.99231, -896.92731, 27.35652>>, bActive) 		
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-320.64703, -1533.95752, 25.63080>>, <<-334.75928, -1524.68799, 29.61280>>, bActive) 		
//					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<167.22275, -1632.56116, 27.29168>>, <<146.30447, -1657.58069, 32.33144>>, bActive) 		
					PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
				BREAK
				CASE MBSV_MUSCLE_OUT_3			
//					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<288.25739, 314.00122, 103.54633>>, <<278.61874, 306.18317, 107.54575>>, bActive) 	
//					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-500.12250, 268.54568, 81.29829>>, <<-488.76556, 274.55170, 85.25215>>, bActive) 	
//					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-58.98646, -224.79253, 43.37305>>, <<-40.38953, -212.73450, 48.14582>>, bActive) 
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<889.27759, -194.77477, 69.59876>>, <<915.44360, -171.24982, 77.36123>>, bActive) 				

					PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
				BREAK
				CASE MBSV_MUSCLE_OUT_4	
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<193.85963, -2022.14758, 16.81498>>, <<209.15273, -2034.13086, 20.30676>>, bActive) 
//					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<314.75735, -1267.59094, 29.48466>>, <<339.18375, -1297.90649, 36.51250>>, bActive) 					

					PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
				BREAK
				CASE MBSV_MUSCLE_OUT_5
//					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-2977.88477, 81.13478, 9.50174>>, <<-2949.21216, 52.58125, 15.19422>>, bActive) 	
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-699.08600, -866.68921, 21.69361>>, <<-716.46851, -885.15747, 25.82048>>, bActive) 	
					PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
				BREAK
				CASE MBSV_MUSCLE_OUT_6	
//					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1765.21008, -2788.12158, 11.94470>>, <<-1781.48938, -2805.82642, 16.94470>>, bActive) 		
//					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-915.74817, -2059.23389, 7.29899>>, <<-892.41693, -2030.44434, 10.92404>>, bActive) 			
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<210.80180, -2008.09058, 16.86148>>, <<234.42493, -1988.25427, 23.60730>>, bActive) 			
					PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
				BREAK
				CASE MBSV_MUSCLE_OUT_7	
//					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<585.24811, 130.40788, 96.04148>>, <<576.76605, 138.40955, 100.47482>>, bActive) 		
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-260.42773, 280.52087, 89.08582>>, <<-246.98340, 288.99884, 95.17360>>, bActive) 			
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<95.10229, -151.17815, 52.03491>>, <<110.95064, -138.58833, 56.76627>>, bActive) 			
					PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
				BREAK
				CASE MBSV_MUSCLE_OUT_8
//					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<664.47406, -2730.75391, 4.11862>>, <<682.25494, -2740.95215, 10.00000>>, bActive) 
//					PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
				BREAK
				CASE MBSV_MUSCLE_OUT_9
//					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1035.81287, -1508.61133, 3.23306>>, <<-1009.89453, -1532.03198, 10.37018>>, bActive) 
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-328.72070, -1291.85132, 29.36329>>, <<-341.04373, -1300.11963, 33.35562>>, bActive) 					
//					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-448.78943, -463.05902, 31.01653>>, <<-419.42621, -447.69061, 37.42461>>, bActive) 					

					PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
				BREAK
			ENDSWITCH
		BREAK
		CASE MBV_SELL_ROADBLOCK
			SWITCH GET_MEGA_BUSINESS_SUBVARIATION()
				CASE MBSV_SELL_ROADBLOCK_0
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<18.67957, 372.17603, 110.30373>>, <<12.21459, 379.71405, 115.40380>>, bActive) 
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<400.66479, 2964.46924, 38.06107>>, <<410.80280, 2975.36426, 44.93449>>, bActive) 
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-175.34738, 6567.06885, 2.36428>>, <<-605.92737, 6095.73145, 14.93398>>, bActive) 							

					PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
				BREAK
				CASE MBSV_SELL_ROADBLOCK_1
//					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1654.76672, 3726.93945, 32.31158>>, <<1629.57593, 3743.57617, 37.69759>>, bActive) 
//					PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
				BREAK
				CASE MBSV_SELL_ROADBLOCK_2
//					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<390.77747, -1448.64209, 27.35701>>, <<424.45184, -1407.95142, 36.33485>>, bActive) 
//					PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
				BREAK
			ENDSWITCH
		BREAK
		CASE MBV_SELL_HACK_DROP
			SWITCH GET_MEGA_BUSINESS_SUBVARIATION()
				CASE MBSV_SELL_HACK_DROP_0
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<829.39142, -1946.10681, 26.93806>>, <<843.32574, -1925.50574, 34.31966>>, bActive) 
					PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
				BREAK
				CASE MBSV_SELL_HACK_DROP_1
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1654.76672, 3726.93945, 32.31158>>, <<1629.57593, 3743.57617, 37.69759>>, bActive) 
					PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
				BREAK
				CASE MBSV_SELL_HACK_DROP_2
//					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<390.77747, -1448.64209, 27.35701>>, <<424.45184, -1407.95142, 36.33485>>, bActive) 
//					PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
				BREAK
			ENDSWITCH
		BREAK
		CASE MBV_SELL_FIND_BUYER
			SWITCH GET_MEGA_BUSINESS_SUBVARIATION()
				CASE MBSV_SELL_FIND_BUYER_0
					SWITCH serverBD.iRandomVariant 
						CASE 0
							SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA( <<1266.74097, -366.76852, 66.04028>>, <<1275.74512, -374.40741, 75.05965>>, bActive)

							PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
						BREAK
						CASE 5
							SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1249.89319, -582.45319, 66.28429>>, <<1240.75989, -588.60077, 75.43742>>, bActive)

							PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
						BREAK
						CASE 6
							SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1153.33948, -373.98315, 64.24604>>, <<1138.92700, -384.08496, 70.04910>>, bActive)

							PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
						BREAK
						CASE 7
							SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1014.02899, -586.51477, 56.82239>>, <<1002.63397, -591.67712, 62.21430>>, bActive)

							PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
						BREAK
						CASE 8
							SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1015.87689, -457.46088, 62.22504>>, <<1024.45923, -465.82944, 66.90347>>, bActive)

							PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
						BREAK
					ENDSWITCH
				BREAK
				CASE MBSV_SELL_FIND_BUYER_1
					SWITCH serverBD.iRandomVariant 
						CASE 0
							SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1828.07446, 3919.49146, 31.36893>>, <<1840.06323, 3903.55737, 38.31018>>, bActive)


							PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
						BREAK
						CASE 2
							SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1938.80481, 3897.10303, 30.37070>>, <<1926.87842, 3908.94482, 35.51570>>, bActive)


							PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
						BREAK
						CASE 3
							SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1839.37854, 3775.93115, 31.32429>>, <<1850.52173, 3768.36816, 36.04179>>, bActive)


							PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
						BREAK
						CASE 9
							SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1951.19495, 3793.05054, 30.22110>>, <<1943.19995, 3799.40088, 35.03714>>, bActive)
							PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
						BREAK
					ENDSWITCH
				BREAK
				CASE MBSV_SELL_FIND_BUYER_2
					SWITCH serverBD.iRandomVariant 
						CASE 0
							SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-352.81372, 6154.91846, 29.48814>>, <<-344.96667, 6147.08643, 34.48684>>, bActive)


							PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
						BREAK
						CASE 1
							SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-150.51125, 6332.14990, 29.57475>>, <<-134.37471, 6348.32910, 35.49070>>, bActive)


							PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
						BREAK
						CASE 3
							SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-51.83322, 6354.88770, 28.36140>>, <<-34.42327, 6369.76123, 36.33176>>, bActive)


							PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
						BREAK
						CASE 5
							SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-106.43063, 6563.61914, 27.49081>>, <<-121.14346, 6575.82764, 34.62831>>, bActive)


							PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
						BREAK
						CASE 6
							SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-78.53560, 6455.13086, 29.44666>>, <<-70.66024, 6447.04688, 34.46980>>, bActive)


							PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
						BREAK
						CASE 7
							SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-370.14731, 6280.35840, 27.73278>>, <<-355.21988, 6268.30029, 36.39355>>, bActive)
							PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
						BREAK
						CASE 8
							SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-275.79413, 6364.46533, 29.27545>>, <<-262.61377, 6352.79590, 36.46300>>, bActive)
							PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE MBV_COLLECT_STAFF
			SWITCH GET_MEGA_BUSINESS_SUBVARIATION()
				CASE MBSV_COLLECT_STAFF_0
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<494.65436, -1548.29895, 27.08541>>, <<503.07712, -1539.86975, 32.11694>>, bActive)

					PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
				BREAK
				CASE MBSV_COLLECT_STAFF_1
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<131.95212, -1573.18799, 27.34709>>, <<120.28302, -1565.09448, 31.19129>>, bActive)
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<350.76901, -190.09035, 55.76540>>, <<342.58121, -184.09782, 60.24255>>, bActive)
					
					PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
				BREAK
				CASE MBSV_COLLECT_STAFF_4
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<251.53937, -9.46092, 71.32910>>, <<258.36441, -15.53884, 75.64911>>, bActive)
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-589.18188, -1739.94958, 20.43498>>, <<-574.11633, -1724.13647, 25.66422>>, bActive)
					
					PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
				BREAK
				CASE MBSV_COLLECT_STAFF_5
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<73.81705, -1041.62146, 27.44369>>, <<92.42756, -1016.16827, 34.40726>>, bActive)
					
					PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
				BREAK
				CASE MBSV_COLLECT_STAFF_7
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<191.74135, -149.93692, 54.10835>>, <<183.23833, -155.68028, 58.29953>>, bActive)
					
					PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
				BREAK
				CASE MBSV_COLLECT_STAFF_8
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<503.30368, -1613.29773, 27.19882>>, <<482.66302, -1633.51709, 32.38373>>, bActive)
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-781.73761, -203.86790, 35.47221>>, <<-796.79736, -192.99144, 40.28367>>, bActive)
					
					PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
				BREAK
				CASE MBSV_COLLECT_STAFF_9
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<250.69624, -1004.30609, 27.26891>>, <<239.11823, -1021.43768, 32.28334>>, bActive)
					
					PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
				BREAK
			ENDSWITCH
		BREAK
		CASE MBV_SELL_PROTECT_BUYER
			SWITCH GET_MEGA_BUSINESS_SUBVARIATION()
				CASE MBSV_SELL_PROTECT_BUYER_0
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<120.67857, 97.65006, 79.21075>>, <<113.60785, 103.86375, 84.21388>>, bActive)
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-682.51837, -968.25507, 17.32519>>, <<-708.94208, -991.60895, 24.89081>>, bActive)
					
					PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
				BREAK
				CASE MBSV_SELL_PROTECT_BUYER_1
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-776.34247, 5553.30371, 31.49125>>, <<-743.22229, 5517.49316, 37.93015>>, bActive)
						
					PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
				BREAK
				CASE MBSV_SELL_PROTECT_BUYER_2
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<532.83612, -1810.74878, 26.56597>>, <<538.94550, -1803.11108, 31.72484>>, bActive)
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<203.01009, 2692.23926, 40.43395>>, <<173.02565, 2719.72241, 46.14589>>, bActive)
						
					PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
				BREAK
			ENDSWITCH
		BREAK
		CASE MBV_COLLECT_DJ_CRASH
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1035.90613, -2728.29443, 25.10497>>, <<-1025.92419, -2735.73218, 18.03105>>, bActive)
			PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
		BREAK
		CASE MBV_SELL_SINGLE_DROP
			SWITCH GET_MEGA_BUSINESS_SUBVARIATION()
				CASE MBSV_SELL_SINGLE_DROP_2
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-216.32195, 6338.24707, 29.54625>>, <<-235.95876, 6356.75977, 35.53409>>, bActive)
						
					PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
				BREAK
				CASE MBSV_SELL_SINGLE_DROP_3
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<2615.68091, 3264.32764, 53.21385>>, <<2606.71313, 3270.97876, 57.12621>>, bActive)
						
					PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
				BREAK
			ENDSWITCH
		BREAK
		CASE MBV_COLLECT_DJ_HOOKED		
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-706.64343, -1440.86377, 4.00052>>, <<-692.93854, -1456.26501, 4.00052>>, bActive)
			PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
			
			SWITCH GET_MEGA_BUSINESS_SUBVARIATION()
				CASE MBSV_COLLECT_HOOKED_2	
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<991.49255, -2468.81372, 26.57829>>, <<1018.04980, -2451.81177, 31.40102>>, bActive)					
				BREAK
				CASE MBSV_COLLECT_HOOKED_3
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1460.23230, -877.48370, 8.61478>>, <<-1441.78943, -889.70062, 12.76572>>, bActive)					
				BREAK
				CASE MBSV_COLLECT_HOOKED_4
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<344.35059, -838.02667, 27.29159>>, <<377.24887, -845.33575, 32.31634>>, bActive)				
				BREAK
				CASE MBSV_COLLECT_HOOKED_6			
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1099.82837, -1805.88855, 34.62763>>, <<1084.75330, -1840.69897, 40.30690>>, bActive)	
				BREAK
				CASE MBSV_COLLECT_HOOKED_7			
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1223.00317, -421.92465, 31.47926>>, <<-1193.72668, -405.94089, 36.23858>>, bActive)					
				BREAK
				CASE MBSV_COLLECT_HOOKED_9			
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1554.93164, -468.96646, 33.73997>>, <<-1531.44946, -484.31332, 38.48026>>, bActive)				
				BREAK
			ENDSWITCH
		BREAK
		CASE MBV_PRIVATE_TAXI
			SWITCH GET_MEGA_BUSINESS_SUBVARIATION()
				CASE MBSV_PRIVATE_TAXI_1
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-430.40591, -37.64626, 44.24245>>, <<-409.22159, -24.45505, 50.67489>>, bActive)
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<226.94069, -1090.46191, 27.32173>>, <<221.29970, -1110.45776, 31.29582>>, bActive)
					PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
				BREAK
				CASE MBSV_PRIVATE_TAXI_2
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-2984.20288, 609.06567, 17.83863>>, <<-2974.52856, 598.98718, 22.42480>>, bActive)
					PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
				BREAK
				CASE MBSV_PRIVATE_TAXI_3
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-883.61298, -2194.03418, 6.48832>>, <<-891.10339, -2186.59668, 10.54049>>, bActive)
					PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
				BREAK
				CASE MBSV_PRIVATE_TAXI_4
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1016.19397, 363.13239, 69.79909>>, <<-1007.48743, 354.40991, 70.02805>>, bActive)
					PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
				BREAK
				CASE MBSV_PRIVATE_TAXI_6
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<243.10533, -828.37756, 27.94320>>, <<264.40756, -842.47595, 33.39191>>, bActive)
					PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
				BREAK
				CASE MBSV_PRIVATE_TAXI_7
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-741.41803, -129.38757, 35.48682>>, <<-725.97058, -146.82841, 40.31238>>, bActive)
					PRINTLN("DEAL_WITH_VEHICLE_GENERATORS, bActive = ", bActive)
				BREAK
			ENDSWITCH
		BREAK
		
		CASE MBV_SNAPMATIC
			SWITCH GET_MEGA_BUSINESS_SUBVARIATION()
				CASE MBSV_SNAPMATIC_0
					SWITCH GET_RANDOM_MODE_VARIANT_FOR_ENTITY_LOOKUP()
						CASE 0			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1593.18945, -1045.08337, 11.01828>>, <<-1601.06262, -1053.87207, 17.01748>>, bActive)		BREAK	
						CASE 1			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<295.49048, 173.23239, 102.07510>>, <<286.89954, 180.59743, 107.38293>>, bActive)			BREAK		
						CASE 5			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1295.74609, 265.69641, 61.77942>>, <<-1288.37988, 273.01892, 68.70142>>, bActive)			BREAK
					ENDSWITCH
				BREAK
				CASE MBSV_SNAPMATIC_1
					SWITCH GET_RANDOM_MODE_VARIANT_FOR_ENTITY_LOOKUP()	
						CASE 3			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<233.19789, -854.82245, 27.80155>>, <<242.83966, -863.34265, 31.67048>>, bActive)			BREAK	
						CASE 7			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1175.84705, -748.48779, 55.84187>>, <<1168.26526, -740.56281, 59.25374>>, bActive)			BREAK
						CASE 9			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-584.46759, -381.17374, 32.85201>>, <<-574.32361, -387.05438, 36.99887>>, bActive)			BREAK
					ENDSWITCH
				BREAK
				CASE MBSV_SNAPMATIC_2
					SWITCH GET_RANDOM_MODE_VARIANT_FOR_ENTITY_LOOKUP()	
						CASE 5			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-751.40326, -38.39894, 35.83663>>, <<-760.35718, -32.04122, 40.83189>>, bActive)			BREAK		
						CASE 8			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<388.76657, -207.51884, 55.94208>>, <<393.70593, -201.38525, 61.08971>>, bActive)			BREAK
					ENDSWITCH																																						
				BREAK
				CASE MBSV_SNAPMATIC_3
					SWITCH GET_RANDOM_MODE_VARIANT_FOR_ENTITY_LOOKUP()
						CASE 1			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-492.99374, -193.33214, 35.33711>>, <<-478.71512, -210.77295, 38.61180>>, bActive)			BREAK	
						CASE 3			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<691.76691, 1211.94983, 322.87717>>, <<682.75116, 1205.36438, 326.04602>>, bActive)			BREAK
						CASE 5			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<216.52640, -847.13672, 28.29555>>, <<234.29256, -860.28998, 32.88537>>, bActive)			BREAK
						CASE 7			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-381.95935, 284.00391, 82.85438>>, <<-389.50632, 271.29605, 87.61776>>, bActive)			BREAK
						CASE 9			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-890.78064, -888.90656, 13.80397>>, <<-901.30127, -875.42554, 17.39189>>, bActive)			BREAK
					ENDSWITCH																																							
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

BOOL bDoStartingWeaponTicker = TRUE

PROC GIVE_STARTING_WEAPON()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_MUSCLE_OUT
			IF NOT IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(localPlayerId)
			AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(localPlayerId)
			AND IS_SCREEN_FADED_IN()
				IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_GIVEN_STARTING_WEAPON)
					IF bDoStartingWeaponTicker
						IF NOT DOES_PLAYER_HAVE_WEAPON(WEAPONTYPE_BAT)
							GIVE_WEAPON_TO_PED(localPlayerPed, WEAPONTYPE_BAT, 0, TRUE)
							PRINTLN("[MB_MISSION] [START_WEAPON] - [", GET_CURRENT_VARIATION_STRING(),"] - GIVE_STARTING_WEAPON - local player doesn't have a WEAPONTYPE_BAT, giving them WEAPONTYPE_BAT")
							SET_LOCAL_BIT(eLOCALBITSET_GIVEN_STARTING_WEAPON)
						ELSE
							SET_CURRENT_PED_WEAPON(localPlayerPed, WEAPONTYPE_BAT, TRUE)
						ENDIF
					
						PRINT_TICKER("MBS_TK_BAT")
						bDoStartingWeaponTicker = FALSE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE MBV_RIVAL_SUPPLY
			IF NOT IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(localPlayerId)
			AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(localPlayerId)
			AND IS_SCREEN_FADED_IN()
				IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_GIVEN_STARTING_WEAPON)
					IF NOT DOES_PLAYER_HAVE_WEAPON(WEAPONTYPE_STICKYBOMB)
						GIVE_WEAPON_TO_PED(localPlayerPed, WEAPONTYPE_STICKYBOMB, 4, TRUE)
						PRINTLN("[MB_MISSION] [START_WEAPON] - [", GET_CURRENT_VARIATION_STRING(),"] - GIVE_STARTING_WEAPON - local player doesn't have a WEAPONTYPE_STICKYBOMB, giving them 4x WEAPONTYPE_STICKYBOMB")
					ELSE
						IF GET_PED_AMMO_BY_TYPE(localPlayerPed, GET_PED_AMMO_TYPE_FROM_WEAPON(localPlayerPed, WEAPONTYPE_STICKYBOMB)) < 4
							SET_PED_AMMO(localPlayerPed, WEAPONTYPE_STICKYBOMB, 4)
							PRINTLN("[MB_MISSION] [START_WEAPON] - [", GET_CURRENT_VARIATION_STRING(),"] - GIVE_STARTING_WEAPON - local player had less than 3x WEAPONTYPE_STICKYBOMB, giving them 4x WEAPONTYPE_STICKYBOMB")
						ELSE
							PRINTLN("[MB_MISSION] [START_WEAPON] - [", GET_CURRENT_VARIATION_STRING(),"] - GIVE_STARTING_WEAPON - local player has more than 3x WEAPONTYPE_STICKYBOMB, not giving them any")
						ENDIF
					ENDIF
					
					SET_LOCAL_BIT(eLOCALBITSET_GIVEN_STARTING_WEAPON)
				ENDIF
			ENDIF
		BREAK
		CASE MBV_COLLECT_DJ_COLLECTOR
		CASE MBV_COLLECT_DJ_STOLEN_BAG
			IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_GIVEN_STARTING_WEAPON)
				GIVE_WEAPON_TO_PED(localPlayerPed, WEAPONTYPE_MICROSMG, 250, FALSE, FALSE)
				PRINTLN("[MB_MISSION] [START_WEAPON] - [", GET_CURRENT_VARIATION_STRING(),"] - GIVE_STARTING_WEAPON - giving local player WEAPONTYPE_MICROSMG")
				SET_LOCAL_BIT(eLOCALBITSET_GIVEN_STARTING_WEAPON)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC REMOVE_STARTING_WEAPON()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_MUSCLE_OUT
			IF IS_LOCAL_BIT_SET(eLOCALBITSET_GIVEN_STARTING_WEAPON)
				IF DOES_PLAYER_HAVE_WEAPON(WEAPONTYPE_BAT)
					REMOVE_WEAPON_FROM_PED(localPlayerPed, WEAPONTYPE_BAT)
					SET_CURRENT_PED_WEAPON(localPlayerPed, WEAPONTYPE_UNARMED, TRUE)
					PRINTLN("[MB_MISSION] [START_WEAPON] - [", GET_CURRENT_VARIATION_STRING(),"] - REMOVE_STARTING_WEAPON - local player didn't have a WEAPONTYPE_BAT, remove WEAPONTYPE_BAT")
					CLEAR_LOCAL_BIT(eLOCALBITSET_GIVEN_STARTING_WEAPON)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC CLIENT_INITIALISE_VARS()
	DEAL_WITH_VEHICLE_GENERATORS(FALSE)
	CLEAR_HOOKED_CARGBOBOB_AREA()
	TOGGLE_CHECK_LOCKED_WARP_FLAG(TRUE)
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_EQUIPMENT
			START_AUDIO_SCENE("DLC_BTL_PBUS2_Music_Boost_Scene")
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - CLIENT_INITIALISE_VARS - [4861122] START_AUDIO_SCENE, (DLC_BTL_PBUS2_Music_Boost_Scene)")
		BREAK
		CASE MBV_STOLEN_SUPPLIES
		CASE MBV_SUPPLY_RUN
			REQUEST_SCRIPT_AUDIO_BANK("SCRIPT/DELIVERIES")
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - CLIENT_INITIALISE_VARS - REQUEST_SCRIPT_AUDIO_BANK(PLANES)")
		BREAK
		CASE MBV_SELL_HACK_DROP
			INT i
			REPEAT MAX_SOUNDS i
				IF sHacking[i] != (-1)
					sHacking[i] = -1 
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - CLIENT_INITIALISE_VARS - sHacking[i] = -1 ", i)
				ENDIF
			ENDREPEAT
			
			FOR i = 0 TO ( 7 )				
				IF iNewMiniGameSoundID[i] != (-1)
					iNewMiniGameSoundID[i] = -1 
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - CLIENT_INITIALISE_VARS - iNewMiniGameSoundID[i] = -1 ", i)
				ENDIF
			ENDFOR
		BREAK
		CASE MBV_BLIMP_PROMOTION
			START_AUDIO_SCENE("DLC_BTL_Blimp_Promotion_General_Scene")
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - CLIENT_INITIALISE_VARS - START_AUDIO_SCENE, (DLC_BTL_Blimp_Promotion_General_Scene)")
		BREAK
	ENDSWITCH
	PRINTLN("CLIENT_INITIALISE_VARS, DONE ")
ENDPROC

FUNC STRING DPAD_TITLE()

	STRING sReturn = "DPAD_BB_BAT"
	
	IF IS_MEGA_BUSINESS_VARIATION_A_CLUB_MANAGEMENT_MISSION(GET_MEGA_BUSINESS_VARIATION()) 
		sReturn = "DPAD_BB_CLB"
	ELIF IS_MEGA_BUSINESS_VARIATION_A_SETUP_MISSION(GET_MEGA_BUSINESS_VARIATION()) 
		sReturn = "DPAD_BB_SETUP"
	ELIF IS_MEGA_BUSINESS_VARIATION_A_SELL_MISSION(GET_MEGA_BUSINESS_VARIATION()) 
		sReturn = "DPAD_BB_SELL"
	ENDIF
	
	RETURN sReturn
ENDFUNC

PROC MAINTAIN_DPAD_LEADERBOARD()

	IF IS_SERVER_BIT_SET(eSERVERBITSET_SETUP_VARIATION)
//	AND IS_LOCAL_BIT_SET(eLOCALBITSET_SHOWN_JOB_START_BIG_MESSAGE)
		
		DRAW_CHALLENGE_DPAD_LBD(g_GBLeaderboardStruct.challengeLbdStruct, g_GBLeaderboardStruct.siDpadMovie, SUB_BOSS, g_GBLeaderboardStruct.dpadVariables, g_GBLeaderboardStruct.fmDpadStruct, DEFAULT, DEFAULT, DPAD_TITLE())
	ENDIF
ENDPROC

PROC MAINTAIN_EXECUTE_COMMON_START_TELEMETRY()
	IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_CALLED_START_COMMON_TELEMETRY)
		GB_SET_COMMON_TELEMETRY_DATA_ON_START()
		GB_CLEAR_BUSINESSBATTLES_GOON_LEFT_TELEMETRY()
		SET_LOCAL_BIT(eLOCALBITSET_CALLED_START_COMMON_TELEMETRY)
		PRINTLN("[MB_MISSION] - [TELEMETRY] - called GB_SET_COMMON_TELEMETRY_DATA_ON_START()")
	ENDIF
ENDPROC

FUNC BOOL SHOULD_UNLOCK_AIRPORT_GATES()

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_BLIMP_PROMOTION
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DISABLE_SPAWN_EXCLUSION_ZONES()

	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DISABLE_ARMY_BASE()

	RETURN FALSE

ENDFUNC

PROC DISABLE_ARMY_BASE(BOOL bDisable)

	g_bDisableArmyBase = bDisable
	IF bDisable
		 UNREGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MIL_BASE_GATE_IN, localPlayerPed)
        UNREGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MIL_BASE_GATE_OUT, localPlayerPed)
        UNREGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MIL_BASE_BARRIER_IN, localPlayerPed)
        UNREGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MIL_BASE_BARRIER_OUT, localPlayerPed)
	ELSE
	
	ENDIF
	PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - DISABLE_ARMY_BASE - ", bDisable)

ENDPROC

FUNC BOOL SHOULD_DISABLE_WANTED_HELIS()
	RETURN TRUE
ENDFUNC

PROC BLOCK_WANTED_LEVEL_FOR_VARIATION_IF_REQUIRED()
		
	BOOL bSupressWanted = TRUE
	//SWITCH GET_GANGOPS_VARIATION()
		//ADD IN EXCEPTIONS HERE
	//ENDSWITCH
	
	IF bSupressWanted
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - BLOCK_WANTED_LEVEL_FOR_VARIATION_IF_REQUIRED - SET_MAX_WANTED_LEVEL(0)")
		SET_MAX_WANTED_LEVEL(0)
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - BLOCK_WANTED_LEVEL_FOR_VARIATION_IF_REQUIRED - SET_PLAYER_WANTED_LEVEL(localPlayerId, 0)")
		SET_PLAYER_WANTED_LEVEL(localPlayerId, 0)
		SET_PLAYER_WANTED_LEVEL_NOW(localPlayerId)
		SET_BIT(g_GB_WORK_VARS.iEventBitSet, ciGB_WORK_WANTED_SUPRESSED_BY_EVENT)
	ENDIF
	
ENDPROC

PROC MAINTAIN_SETTING_SUBVARIATION_BD()
	IF IS_SERVER_BIT_SET(eSERVERBITSET_SETUP_VARIATION)
		GB_SET_CONTRABAND_SUBVARIATION_LOCAL_PLAYER_IS_ON(ENUM_TO_INT(GET_MEGA_BUSINESS_SUBVARIATION()))
		GB_SET_CONTRABAND_VARIATION_LOCAL_PLAYER_IS_ON(ENUM_TO_INT(GET_MEGA_BUSINESS_VARIATION()))
	ENDIF
ENDPROC

PROC MAINTAIN_CALL_COMMON_START_FUNCTIONS()
	IF GET_MODE_STATE() > eMODESTATE_INIT
		
		// Common start.
		IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_CALLED_COMMON_SETUP)
			
			serverBD.iFMMCType = GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION_TO_LAUNCH(localPlayerId)
			
			GB_COMMON_BOSS_MISSION_SETUP(serverBD.iFMMCType, DEFAULT, ENUM_TO_INT(GET_MEGA_BUSINESS_VARIATION()))
			
			GB_SET_MEGA_BUSINESS_VARIATION_LOCAL_PLAYER_IS_ON(ENUM_TO_INT(GET_MEGA_BUSINESS_VARIATION()))
			
			GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
			BLOCK_WANTED_LEVEL_FOR_VARIATION_IF_REQUIRED()
			
			INT iMissionEntity
			REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
				SET_VEHICLE_MODEL_IS_SUPPRESSED(serverBD.sMissionEntity[iMissionEntity].modelName, TRUE)
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_VEHICLE_MODEL_IS_SUPPRESSED(",GET_MODEL_NAME_FOR_DEBUG(serverBD.sMissionEntity[iMissionEntity].modelName),", TRUE) - GET_MISSION_ENTITY_MODEL")
			ENDREPEAT
			
			
			IF SHOULD_UNLOCK_AIRPORT_GATES()
				UNLOCK_AIRPORT_GATES()
				SET_LOCAL_BIT(eLOCALBITSET_UNLOCKED_AIRPORT_GATES)
			ENDIF
			
			IF SHOULD_DISABLE_SPAWN_EXCLUSION_ZONES()
				DISABLE_GLOBAL_EXCLUSION_ZONES_FOR_SPAWNING(TRUE)
			ENDIF
			
			IF SHOULD_DISABLE_ARMY_BASE()
				DISABLE_ARMY_BASE(TRUE)
			ENDIF
			
			IF SHOULD_DISABLE_WANTED_HELIS()
				SET_PED_CONFIG_FLAG(localPlayerPed,PCF_DisableWantedHelicopterSpawning, TRUE)
			ENDIF
			
			IF GET_MEGA_BUSINESS_VARIATION() != MBV_INVALID
			AND GET_MEGA_BUSINESS_VARIATION() != MBV_MAX
				ADD_MEGA_BUSINESS_VARIATION_TO_PLAYERS_HISTORY_LIST(GET_MEGA_BUSINESS_VARIATION(), localPlayerId)
			ENDIF
			
//			INITIALISE_GANGOPS_FLOW_AWARDS(ENUM_TO_INT(GET_GANGOPS_VARIATION())) 
			
//			SET_LAST_GANGOPS_LOCATION(GET_GANGOPS_LOCATION())
			
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				serverBD.iLaunchPosix = GET_CLOUD_TIME_AS_INT()
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - Launch Posix = ", serverBD.iLaunchPosix)
			ENDIF
			
//			IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
//			AND SHOULD_RUN_BIKER_RACE_FOR_VARIATION()
//				GB_BIKER_RACE_SET_UP(sBikerRace, serverBD.sRaceServerVars, GET_GOTO_LOCATION_COORDS())
//			ELSE
//				SET_CLIENT_BIT(eCLIENTBITSET_RACE_TO_POINT_FINISHED)
//				IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
//					SET_SERVER_BIT(eSERVERBITSET_RACE_TO_POINT_FINISHED)
//				ENDIF
//			ENDIF
			
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_CALL_COMMON_START_FUNCTIONS - Variation: ", GET_CURRENT_VARIATION_STRING())
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_CALL_COMMON_START_FUNCTIONS - Subvariation: ", GET_MEGA_BUSINESS_SUBVARIATION_NAME_FOR_DEBUG(GET_MEGA_BUSINESS_SUBVARIATION()))
				
			SET_LOCAL_BIT(eLOCALBITSET_CALLED_COMMON_SETUP)
		ELSE
			IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_CALLED_SAFE_FOR_SPEC_CAM_SETUP)
				IF HAS_NET_TIMER_STARTED(serverBD.modeTimer)
					IF HAS_NET_TIMER_EXPIRED(serverBD.modeTimer, TRACK_GOON_DELAY)
						GB_SET_ITS_SAFE_FOR_SPEC_CAM()
						SET_LOCAL_BIT(eLOCALBITSET_CALLED_SAFE_FOR_SPEC_CAM_SETUP)
					ENDIF
				ENDIF
			ENDIF
		ENDIF						
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_SEND_COLLECTED_TICKER()
	IF IS_MEGA_BUSINESS_VARIATION_A_CLUB_MANAGEMENT_MISSION(GET_MEGA_BUSINESS_VARIATION())
	OR IS_MEGA_BUSINESS_VARIATION_A_SETUP_MISSION(GET_MEGA_BUSINESS_VARIATION())
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SET_MISSION_ENTITY_POSSESSION(INT iMissionEntity, BOOL bInPossession)
	IF bInPossession
		IF NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_I_AM_OCCUPIED)
			IF NOT IS_MISSION_ENTITY_CLIENT_BIT_SET(PARTICIPANT_ID(), iMissionEntity, eMISSIONENTITYCLIENTBITSET_I_AM_OCCUPIED)
				PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - SET_MISSION_ENTITY_POSSESSION - I have taken possession of mission entity #", iMissionEntity)
				SET_MISSION_ENTITY_CLIENT_BIT(iMissionEntity, eMISSIONENTITYCLIENTBITSET_I_AM_OCCUPIED)
			ENDIF
			
			IF NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_AM_IN_POSSESSION_OF_A_MISSION_ENTITY)
				PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - SET_MISSION_ENTITY_POSSESSION - I am in possession of mission entity. #", iMissionEntity)
				SET_CLIENT_BIT(eCLIENTBITSET_I_AM_IN_POSSESSION_OF_A_MISSION_ENTITY)
				playerBD[PARTICIPANT_ID_TO_INT()].iMyMissionEntity = iMissionEntity
				iMyLastMissionEntity = iMissionEntity
//				IF SHOULD_SEND_COLLECTED_TICKER()
//					SEND_BUSINESS_ENTITY_COLLECTED_DROPPED_TICKER(serverBD.iFMMCType, localPlayerId, GB_GET_LOCAL_PLAYER_GANG_BOSS(), FALSE)
//				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_MISSION_ENTITY_CLIENT_BIT_SET(PARTICIPANT_ID(), iMissionEntity, eMISSIONENTITYCLIENTBITSET_I_AM_OCCUPIED)
			PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - SET_MISSION_ENTITY_POSSESSION - No one is in possession of mission entity #", iMissionEntity)
			CLEAR_MISSION_ENTITY_CLIENT_BIT(iMissionEntity, eMISSIONENTITYCLIENTBITSET_I_AM_OCCUPIED)
		ENDIF
		
		IF IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_AM_IN_POSSESSION_OF_A_MISSION_ENTITY)
			PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - SET_MISSION_ENTITY_POSSESSION - I am no longer in possession of mission entity. #", iMissionEntity)
			CLEAR_CLIENT_BIT(eCLIENTBITSET_I_AM_IN_POSSESSION_OF_A_MISSION_ENTITY)
			playerBD[PARTICIPANT_ID_TO_INT()].iMyMissionEntity = -1
		ENDIF
	ENDIF
ENDPROC

// url:bugstar:4785325
PROC MAINTAIN_INTERIOR_LOCATES()
	IF DOES_MEGA_BUSINESS_VARIATION_USE_MISSION_ENTITIES(GET_MEGA_BUSINESS_VARIATION())
		IF NETWORK_IS_GAME_IN_PROGRESS()
			IF IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_AM_IN_POSSESSION_OF_A_MISSION_ENTITY)
				IF NOT GB_IS_GLOBAL_NON_BD_BIT0_SET(eGB_GLOBAL_NON_BD_BITSET_0_PREVENT_DRAWING_SIMPLE_INTERIOR_CORONA)
					GB_SET_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_PREVENT_DRAWING_SIMPLE_INTERIOR_CORONA)
				ENDIF
			ELSE
				IF GB_IS_GLOBAL_NON_BD_BIT0_SET(eGB_GLOBAL_NON_BD_BITSET_0_PREVENT_DRAWING_SIMPLE_INTERIOR_CORONA)
					GB_CLEAR_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_PREVENT_DRAWING_SIMPLE_INTERIOR_CORONA)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_MISSION_ENTITY_POSSESSION()
	INT iMissionEntity
	IF DOES_MEGA_BUSINESS_VARIATION_USE_MISSION_ENTITIES(GET_MEGA_BUSINESS_VARIATION())
		IF IS_LOCAL_PLAYER_PILOT_OF_A_CARGOBOB()
			ENTITY_INDEX entAttached = GET_ENTITY_ATTACHED_TO_CARGOBOB(GET_VEHICLE_PED_IS_USING(localPlayerPed))
			IF DOES_ENTITY_EXIST(entAttached)
				IF IS_ENTITY_A_MEGA_BUSINESS_MISSION_ENTITY(entAttached, iMissionEntity)
					SET_MISSION_ENTITY_POSSESSION(iMissionEntity, TRUE)
					SET_LOCAL_BIT(eLOCALBITSET_CARRYING_MISSION_ENTITY_WITH_CARGOBOB)
				ENDIF
			ELSE
				IF playerBD[PARTICIPANT_ID_TO_INT()].iMyMissionEntity != -1
					SET_MISSION_ENTITY_POSSESSION(playerBD[PARTICIPANT_ID_TO_INT()].iMyMissionEntity, FALSE)
					CLEAR_LOCAL_BIT(eLOCALBITSET_CARRYING_MISSION_ENTITY_WITH_CARGOBOB)
				ENDIF
			ENDIF
		ELSE
			CLEAR_LOCAL_BIT(eLOCALBITSET_CARRYING_MISSION_ENTITY_WITH_CARGOBOB)
			
			BOOL bSetInSellHeli = FALSE
			
			REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
				IF IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_CREATED)
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
						VEHICLE_INDEX veh = NET_TO_VEH(serverBD.sMissionEntity[iMissionEntity].netId)
						IF IS_PED_SITTING_IN_VEHICLE(localPlayerPed, veh)
						AND NOT IS_CARGOBOB_COLLECTION_NEEDED()
							bSetInSellHeli = TRUE
							IF GET_PED_IN_VEHICLE_SEAT(veh, VS_DRIVER) = localPlayerPed
							AND NOT IS_PED_INJURED(localPlayerPed)
								SET_MISSION_ENTITY_POSSESSION(iMissionEntity, TRUE)
							ELSE
								SET_MISSION_ENTITY_POSSESSION(iMissionEntity, FALSE)
							ENDIF
						ELSE
							IF playerBD[PARTICIPANT_ID_TO_INT()].iMyMissionEntity = iMissionEntity
								SET_MISSION_ENTITY_POSSESSION(iMissionEntity, FALSE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF bSetInSellHeli
				g_bBikerInSellHeli = TRUE
			ELSE
				g_bBikerInSellHeli = FALSE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_MISSION_ENTITY_DESTROYED(INT iMissionEntity)

	SWITCH GET_MEGA_BUSINESS_VARIATION() 
		CASE MBV_MUSCLE_OUT
			IF GET_HEALTH_OF_MISSION_ENTITY(iMissionEntity) <= 0 
			
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	IF IS_ENTITY_DEAD(NET_TO_VEH(serverBD.sMissionEntity[iMissionEntity].netId))
		PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - IS_MISSION_ENTITY_DESTROYED - Mission entity #", iMissionEntity, " - IS_ENTITY_DEAD")
		RETURN TRUE
	ENDIF
	
//	IF NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.sMissionEntity[iMissionEntity].netId))
//		PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - IS_MISSION_ENTITY_DESTROYED - Mission entity #", iMissionEntity, " - NOT IS_VEHICLE_DRIVEABLE")
//		RETURN TRUE
//	ENDIF

	RETURN FALSE
ENDFUNC

PROC MAINTAIN_MISSION_ENTITY_DELIVERED(INT iMissionEntity)
	IF IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_CREATED)
	AND NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_DESTROYED)
	AND IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_DELIVERED)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
			VEHICLE_INDEX vehID = NET_TO_VEH(serverBD.sMissionEntity[iMissionEntity].netId)
			IF IS_ENTITY_ALIVE(vehID)
				IF GET_ENTITY_CAN_BE_DAMAGED(vehID)
					IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
						SET_ENTITY_CAN_BE_DAMAGED(vehID, FALSE)
						SET_ENTITY_INVINCIBLE(vehID, TRUE)
						PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - MAINTAIN_MISSION_ENTITY_DELIVERED - Mission entity #", iMissionEntity, " has been made invincible")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_MISSION_ENTITY_DESTROYED(INT iMissionEntity)
	IF IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_CREATED)
	AND NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_DESTROYED)
	AND NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_DELIVERED)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
			IF IS_MISSION_ENTITY_DESTROYED(iMissionEntity)
				IF NOT IS_MISSION_ENTITY_CLIENT_BIT_SET(PARTICIPANT_ID(), iMissionEntity, eMISSIONENTITYCLIENTBITSET_DESTROYED)
					PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - MAINTAIN_MISSION_ENTITY_DESTROYED - I see mission entity #", iMissionEntity, " as destroyed! Setting eMISSIONENTITYCLIENTBITSET_DESTROYED")
					SET_MISSION_ENTITY_CLIENT_BIT(iMissionEntity, eMISSIONENTITYCLIENTBITSET_DESTROYED)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_MISSION_ENTITY_UNDRIVEABLE(INT iMissionEntity)
	IF IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_CREATED)
	AND NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_DESTROYED)
	AND NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_DELIVERED)
	AND NOT IS_MISSION_ENTITY_CLIENT_BIT_SET(PARTICIPANT_ID(), iMissionEntity, eMISSIONENTITYCLIENTBITSET_DESTROYED)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
			
			INT iTimer = 5000
			BOOL bRunTimer = FALSE
			BOOL bResetTimer = TRUE
			FLOAT fSubmergeLevel
			PED_INDEX pedId
			
			pedId = GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(serverBD.sMissionEntity[iMissionEntity].netId))
			IF DOES_ENTITY_EXIST(pedId)
			AND NOT IS_ENTITY_DEAD(pedId)
			AND IS_PED_A_PLAYER(pedId)
				IF piLastPlayerInVehicle[iMissionEntity] != NETWORK_GET_PLAYER_INDEX_FROM_PED(pedId)
					piLastPlayerInVehicle[iMissionEntity] = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedId)
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_MISSION_ENTITY_UNDRIVEABLE - Last Player in mission entity ", iMissionEntity,": ", GET_PLAYER_NAME(piLastPlayerInVehicle[iMissionEntity]))
				ENDIF
			ENDIF
			
			IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.sMissionEntity[iMissionEntity].netId)
				bRunTimer = TRUE
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_MISSION_ENTITY_UNDRIVEABLE - bRunTimer = TRUE - Mission entity ", iMissionEntity," is undriveable.")
			ENDIF
			IF IS_ENTITY_IN_WATER(NET_TO_ENT(serverBD.sMissionEntity[iMissionEntity].netId))
				fSubmergeLevel = GET_ENTITY_SUBMERGED_LEVEL(NET_TO_ENT(serverBD.sMissionEntity[iMissionEntity].netId))
				IF fSubmergeLevel >= 0.9
					bRunTimer = TRUE
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_MISSION_ENTITY_UNDRIVEABLE - bRunTimer = TRUE - Mission entity ", iMissionEntity," is submerged. Submerged level: ", fSubmergeLevel)
				ELSE
					PRINTLN("Mission entity ", iMissionEntity," is in water but not submerged enough. Submerged level: ", fSubmergeLevel)
				ENDIF
			ENDIF
			
			IF bRunTimer
				bResetTimer = FALSE
				
				IF HAS_NET_TIMER_EXPIRED(UndriveableTimer[iMissionEntity], iTimer)
				
					SET_MISSION_ENTITY_CLIENT_BIT(iMissionEntity, eMISSIONENTITYCLIENTBITSET_DESTROYED)
					PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - MAINTAIN_MISSION_ENTITY_UNDRIVEABLE - I see mission entity ", iMissionEntity, " as undriveable! Setting eMISSIONENTITYCLIENTBITSET_DESTROYED")
					IF piLastPlayerInVehicle[iMissionEntity] != INVALID_PLAYER_INDEX()
					AND DOES_MEGA_BUSINESS_VARIATION_NEED_GLOBAL_SIGNAL()
						SEND_BUSINESS_ENTITY_DESTROYED_TICKER(serverBD.iFMMCType, piLastPlayerInVehicle[iMissionEntity])
						SEND_CONTRABAND_DESTROYED_EVENT(piLastPlayerInVehicle[iMissionEntity])
					ENDIF
				ENDIF
			ENDIF
			
			IF bResetTimer
				IF HAS_NET_TIMER_STARTED(UndriveableTimer[iMissionEntity])
					RESET_NET_TIMER(UndriveableTimer[iMissionEntity])
					PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - MAINTAIN_MISSION_ENTITY_UNDRIVEABLE - Mission entity ", iMissionEntity, " timer had started, reset it")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_LEAVE_VEHICLE()
	
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_STOLEN_SUPPLIES
		CASE MBV_SUPPLY_RUN
		CASE MBV_COLLECT_DJ_COLLECTOR
		CASE MBV_COLLECT_DJ_CRASH
		CASE MBV_COLLECT_DJ_HOOKED
		CASE MBV_COLLECT_DJ_STOLEN_BAG
		CASE MBV_FLYER_PROMOTION
		CASE MBV_BLIMP_PROMOTION
			RETURN FALSE
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_FULLY_LOCK_DOORS()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_SELL_PROTECT_BUYER
			RETURN FALSE
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC FLOAT STOPPING_DISTANCE_FOR_DROP_OFFS()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_SELL_FIND_BUYER
			RETURN 0.0
	ENDSWITCH
	RETURN 3.0
ENDFUNC

PROC MAINTAIN_EXIT_MISSION_ENTITY_AFTER_DELIVERY(INT iMissionEntity)
	IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_MISSION_EOM_DELIVERY_CUT)
		IF IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_DELIVERED)
		AND NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_DESTROYED)
		AND NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_LOCKED_FOR_ALL_PLAYERS)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
				VEHICLE_INDEX veh = NET_TO_VEH(serverBD.sMissionEntity[iMissionEntity].netId)
				IF NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(veh))
				AND NOT IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(veh))
					IF bLocalPlayerOk
						IF IS_PED_IN_VEHICLE(localPlayerPed, veh)
							IF TAKE_CONTROL_OF_NET_ID(serverBD.sMissionEntity[iMissionEntity].netId)
								IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(veh, STOPPING_DISTANCE_FOR_DROP_OFFS())
									IF SHOULD_LEAVE_VEHICLE()
										TASK_LEAVE_VEHICLE(localPlayerPed, veh)
										PRINTLN("MAINTAIN_EXIT_MISSION_ENTITY_AFTER_DELIVERY, DONE ")
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF IS_VEHICLE_EMPTY(veh)
								IF NOT IS_MISSION_ENTITY_CLIENT_BIT_SET(PARTICIPANT_ID(), iMissionEntity, eMISSIONENTITYCLIENTBITSET_LOCKED_FOR_ALL_PLAYERS)
									IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
									OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sMissionEntity[iMissionEntity].netId) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
										IF TAKE_CONTROL_OF_NET_ID(serverBD.sMissionEntity[iMissionEntity].netId)
											IF SHOULD_FULLY_LOCK_DOORS()
												SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(veh, TRUE)
											ELSE
												INT iDoor
												REPEAT SC_MAX_DOORS iDoor
													IF DOES_VEHICLE_HAVE_DOOR(veh, INT_TO_ENUM(SC_DOOR_LIST, iDoor))
														SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(veh, iDoor, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
													ENDIF
												ENDREPEAT
											ENDIF
											SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(veh, TRUE)
											SET_VEHICLE_AUTOMATICALLY_ATTACHES(veh, FALSE)
											SET_MISSION_ENTITY_CLIENT_BIT(iMissionEntity, eMISSIONENTITYCLIENTBITSET_LOCKED_FOR_ALL_PLAYERS)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_HALT_AT_GOTO()

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_SNAPMATIC				RETURN GET_MODE_STATE() = eMODESTATE_GO_TO_POINT
		CASE MBV_COLLECT_DJ_COLLECTOR
		CASE MBV_COLLECT_DJ_STOLEN_BAG
			RETURN GET_MODE_STATE() = eMODESTATE_GO_TO_POINT
		CASE MBV_COLLECT_DJ_HOOKED
			RETURN serverBD.iCurrentGotoLocation = FIRST_GOTO_LOCATION
		CASE MBV_COLLECT_DJ_CRASH
			RETURN FALSE //GET_MODE_STATE() = eMODESTATE_GO_TO_POINT
		CASE MBV_PRIVATE_TAXI
			RETURN GET_MODE_STATE() = eMODESTATE_GO_TO_POINT
		CASE MBV_SELL_UNDERCOVER_COPS
			RETURN GET_MODE_STATE() = eMODESTATE_GO_TO_POINT
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_LEAVE_VEHICLE_AT_GOTO()

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_HOOKED
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL MAKE_SURE_HALT_COMPLETE()

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_HOOKED
			RETURN IS_PLAYER_IN_POSSESSION_OF_A_MISSION_ENTITY(localPlayerId)
	ENDSWITCH

	RETURN FALSE
ENDFUNC

TIME_DATATYPE timeLeaveEvent

PROC MAINTAIN_VEHICLE_HALT_AT_GOTO(INT iMissionEntity)
	IF SHOULD_HALT_AT_GOTO()
	AND IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_CREATED)
	AND NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_DESTROYED)
	AND ( IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_HAVE_REACHED_GOTO_LOCATION) OR MAKE_SURE_HALT_COMPLETE() )
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
			
				VEHICLE_INDEX veh = NET_TO_VEH(serverBD.sMissionEntity[iMissionEntity].netId)
				
				IF NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(veh))
				AND NOT IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(veh))
					IF NOT IS_VEHICLE_EMPTY_AND_STATIONARY(veh)
						IF IS_PED_IN_VEHICLE(localPlayerPed, veh)
							IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(veh, STOPPING_DIST())
								PRINTLN("MAINTAIN_VEHICLE_HALT_AT_GOTO")
								
								IF SHOULD_LEAVE_VEHICLE_AT_GOTO()
									EMPTY_AND_LOCK_VEHICLE(veh, timeLeaveEvent, DEFAULT, FALSE)
									PRINTLN("MAINTAIN_VEHICLE_HALT_AT_GOTO, EMPTY_AND_LOCK_VEHICLE")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HOOKED_LIMO_IN_PROTECT_STAGE()

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_HOOKED
			IF serverBD.iCurrentGotoLocation != INVALID_GOTO_LOCATION
			AND NOT IS_SERVER_BIT_SET(eSERVERBITSET_MID_MISSION_MOCAP_STARTED_FOR_ALL)
			
				RETURN TRUE
			ENDIF
		BREAK		
	ENDSWITCH

	RETURN FALSE
ENDFUNC

CONST_FLOAT cfLIMO_CARGO_DIST 150.00

PROC CALCULATE_DIST_BETWEEN_CARGOBOB_AND_MADONNA()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[ciHOOKED_VEH_CARGOBOB].netId)
	
		VEHICLE_INDEX vBob

		vBob = NET_TO_VEH(serverBD.sVehicle[ciHOOKED_VEH_CARGOBOB].netId)
		
		IF NOT IS_ENTITY_DEAD(vBob)
		AND DOES_ENTITY_EXIST(HOOKED_PED(HOOKED_PED_MADONNA))
		AND NOT IS_ENTITY_DEAD(HOOKED_PED(HOOKED_PED_MADONNA))
		
			fDistBetweenCargoboAndCopCar = GET_DISTANCE_BETWEEN_ENTITIES(HOOKED_PED(HOOKED_PED_MADONNA), vBob) 
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_MISSION_ENTITY_MISSION_SPECIFIC_CLIENT(INT iMissionEntity)

	VEHICLE_INDEX veh, vBob 
	ENTITY_INDEX ent

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_SNAPMATIC	
		
			IF IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_CREATED)
			AND NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_DESTROYED)

				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
					OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sMissionEntity[iMissionEntity].netId) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
					
						veh = NET_TO_VEH(serverBD.sMissionEntity[iMissionEntity].netId)
								
						INT iDoor
						REPEAT SC_MAX_DOORS iDoor
							IF SHOULD_LOCK_DOOR_FOR_PLAYERS(INT_TO_ENUM(SC_DOOR_LIST, iDoor))
								SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(veh, iDoor, GET_VEHICLE_DOOR_LOCKOUT_TYPE())
//								PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_MISSION_ENTITY_MISSION_SPECIFIC_CLIENT - locked door ", GET_VEHICLE_DOOR_NAME(INT_TO_ENUM(SC_DOOR_LIST, iDoor)))
							ENDIF
						ENDREPEAT				
					ENDIF
				ENDIF
			ENDIF
		
		BREAK
		CASE MBV_COLLECT_DJ_HOOKED

			IF IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_CREATED)
			AND NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_DESTROYED)

				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
					
					veh = NET_TO_VEH(serverBD.sMissionEntity[iMissionEntity].netId)
					
					IF HOOKED_LIMO_IN_PROTECT_STAGE()
						IF IS_VEHICLE_EMPTY(veh)
							
							// Lock and freeze the limo
							IF NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_LOCKED_FOR_ALL_PLAYERS)
								IF NOT IS_MISSION_ENTITY_CLIENT_BIT_SET(PARTICIPANT_ID(), iMissionEntity, eMISSIONENTITYCLIENTBITSET_LOCKED_FOR_ALL_PLAYERS)
									IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
									OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sMissionEntity[iMissionEntity].netId) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
										IF TAKE_CONTROL_OF_NET_ID(serverBD.sMissionEntity[iMissionEntity].netId)
											SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(veh, TRUE)
											SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(veh, TRUE)
											FREEZE_ENTITY_POSITION(veh, TRUE)
											PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - MAINTAIN_MISSION_ENTITY_MISSION_SPECIFIC_CLIENT - SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS ")
											PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - MAINTAIN_MISSION_ENTITY_MISSION_SPECIFIC_CLIENT - FREEZE_ENTITY_POSITION ")
											SET_MISSION_ENTITY_CLIENT_BIT(iMissionEntity, eMISSIONENTITYCLIENTBITSET_LOCKED_FOR_ALL_PLAYERS)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							FLOAT fDist
							
							// Grad dist between cargobob and limo
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[ciHOOKED_VEH_CARGOBOB].netId)
		
								vBob = NET_TO_VEH(serverBD.sVehicle[ciHOOKED_VEH_CARGOBOB].netId)
								
								fDist = GET_DISTANCE_BETWEEN_ENTITIES(veh, vBob) 
							ENDIF

							SWITCH GET_MODE_STATE() 
							
								CASE eMODESTATE_GO_TO_POINT
								
									SWITCH serverBD.iCurrentGotoLocation 
									
										CASE FIRST_GOTO_LOCATION
								
											// Fade out and set limo local
											IF NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_LIMO_HIDDEN)
												IF NOT IS_MISSION_ENTITY_CLIENT_BIT_SET(PARTICIPANT_ID(), iMissionEntity, eMISSIONENTITYCLIENTBITSET_HOOKED_LIMO_HIDDEN)

													IF fDist > cfLIMO_CARGO_DIST
								
														IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
														OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sMissionEntity[iMissionEntity].netId) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
															IF TAKE_CONTROL_OF_NET_ID(serverBD.sMissionEntity[iMissionEntity].netId)
													
																NETWORK_FADE_OUT_ENTITY(veh, TRUE, TRUE)
																NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(veh, TRUE)
																
																PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - MAINTAIN_MISSION_ENTITY_MISSION_SPECIFIC_CLIENT - NETWORK_FADE_OUT_ENTITY ")
																PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - MAINTAIN_MISSION_ENTITY_MISSION_SPECIFIC_CLIENT - NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS ")
													
																SET_MISSION_ENTITY_CLIENT_BIT(iMissionEntity, eMISSIONENTITYCLIENTBITSET_HOOKED_LIMO_HIDDEN)
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										BREAK
										
										CASE SECOND_GOTO_LOCATION
											IF NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_LIMO_FADED_IN)
												IF NOT IS_MISSION_ENTITY_CLIENT_BIT_SET(PARTICIPANT_ID(), iMissionEntity, eMISSIONENTITYCLIENTBITSET_HOOKED_LIMO_FADED_IN)
										
													IF fDist < cfLIMO_CARGO_DIST*3
														IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
															SET_CLIENT_BIT(eCLIENTBITSET_NEED_WARP_COORD)
														ENDIF
													ENDIF
													
													IF NOT IS_VECTOR_ZERO(serverBD.vMocapWarpCoord)
														IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
//														OR TAKE_CONTROL_OF_NET_ID(serverBD.sMissionEntity[iMissionEntity].netId)
															SET_ENTITY_COORDS(veh, serverBD.vMocapWarpCoord)
															SET_ENTITY_HEADING(veh, serverBD.fMocapWarpHeading)
															NETWORK_FADE_IN_ENTITY(veh, TRUE, TRUE)
															PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - MAINTAIN_MISSION_ENTITY_MISSION_SPECIFIC_CLIENT - WARP & NETWORK_FADE_IN_ENTITY ", serverBD.vMocapWarpCoord)
															SET_MISSION_ENTITY_CLIENT_BIT(iMissionEntity, eMISSIONENTITYCLIENTBITSET_HOOKED_LIMO_FADED_IN)
														ENDIF
													ENDIF
												ENDIF
											ELSE
												CLEAR_CLIENT_BIT(eCLIENTBITSET_NEED_WARP_COORD)
											ENDIF
										BREAK
									ENDSWITCH
								BREAK
								
							ENDSWITCH
						ENDIF
					ELSE
						IF IS_SERVER_BIT_SET(eSERVERBITSET_SCRIPTED_CUTSCENE_DONE)
							// Set limo exists for all, unlock and unfreeze
							IF NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_LIMO_RESET)
							AND NOT IS_MISSION_ENTITY_CLIENT_BIT_SET(PARTICIPANT_ID(), iMissionEntity, eMISSIONENTITYCLIENTBITSET_LIMO_RESET) 
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
								OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sMissionEntity[iMissionEntity].netId) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
									IF TAKE_CONTROL_OF_NET_ID(serverBD.sMissionEntity[iMissionEntity].netId)
										IF IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_LIMO_HIDDEN)
											NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(veh, FALSE)
											PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - MAINTAIN_MISSION_ENTITY_MISSION_SPECIFIC_CLIENT - NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS (CLEARED) ")
										ENDIF
										
										SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(veh, FALSE)
										SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(veh, FALSE)
										
										FREEZE_ENTITY_POSITION(veh, FALSE)
										SET_VEHICLE_FIXED(veh)																	
										
										PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - MAINTAIN_MISSION_ENTITY_MISSION_SPECIFIC_CLIENT - UNFROZEN/UNLOCKED ")
							
										SET_MISSION_ENTITY_CLIENT_BIT(iMissionEntity, eMISSIONENTITYCLIENTBITSET_LIMO_RESET)
									ENDIF
								ENDIF
							ELSE
							
								IF NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_LIMO_EXCLUSIVE)
								AND NOT IS_MISSION_ENTITY_CLIENT_BIT_SET(PARTICIPANT_ID(), iMissionEntity, eMISSIONENTITYCLIENTBITSET_LIMO_EXCLUSIVE) 
								
									IF GET_MODE_STATE() = eMODESTATE_DELIVER_ENTITY
										IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
											SET_VEHICLE_EXCLUSIVE_DRIVER(veh, localPlayerPed)
											PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_MISSION_ENTITY_MISSION_SPECIFIC_CLIENT - SET_VEHICLE_EXCLUSIVE_DRIVER to boss.")
										ELSE
											IF IS_NET_PLAYER_OK(GB_GET_LOCAL_PLAYER_GANG_BOSS(), FALSE)
												SET_VEHICLE_EXCLUSIVE_DRIVER(veh, GET_PLAYER_PED(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
												PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_MISSION_ENTITY_MISSION_SPECIFIC_CLIENT - SET_VEHICLE_EXCLUSIVE_DRIVER to boss.")
											ENDIF
										ENDIF	
										
										SET_MISSION_ENTITY_CLIENT_BIT(iMissionEntity, eMISSIONENTITYCLIENTBITSET_LIMO_EXCLUSIVE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE MBV_SELL_OFFSHORE_TRANSFER
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
				veh = NET_TO_VEH(serverBD.sMissionEntity[iMissionEntity].netId)
				IF IS_ENTITY_ALIVE(veh)
				AND NOT HAS_MISSION_ENTITY_BEEN_DELIVERED_OR_DESTROYED(iMissionEntity)
					IF IS_ENTITY_ATTACHED(veh)
						ent = GET_ENTITY_ATTACHED_TO(veh)
						IF IS_ENTITY_A_VEHICLE(ent)
							vBob = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(ent)
							IF IS_VEHICLE_A_CARGOBOB(vBob)
								SWITCH GET_MEGA_BUSINESS_SUBVARIATION()
									CASE MBSV_SELL_OFFSHORE_TRANSFER_0
										IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
											IF IS_ENTITY_IN_ANGLED_AREA( veh, <<3211.942383,1456.958374,0.454320>>, <<3213.609131,1449.184937,5.454319>>, 12.500000)
												DETACH_ENTITY_FROM_CARGOBOB(vBob, veh)
												SET_VEHICLE_AUTOMATICALLY_ATTACHES(veh, FALSE)
												SET_VEHICLE_DISABLE_TOWING(veh, TRUE)
												PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - MAINTAIN_MISSION_ENTITY_MISSION_SPECIFIC_CLIENT - DETACH_ENTITY(veh) ")
											ENDIF
										ENDIF
									BREAK
									CASE MBSV_SELL_OFFSHORE_TRANSFER_1
										IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
											IF IS_ENTITY_IN_ANGLED_AREA( veh, <<-164.387115,7382.008301,0.454318>>, <<-156.708984,7383.602051,5.454319>>, 12.500000)
												DETACH_ENTITY_FROM_CARGOBOB(vBob, veh)
												SET_VEHICLE_AUTOMATICALLY_ATTACHES(veh, FALSE)
												SET_VEHICLE_DISABLE_TOWING(veh, TRUE)
												PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - MAINTAIN_MISSION_ENTITY_MISSION_SPECIFIC_CLIENT - DETACH_ENTITY(veh) ")
											ENDIF
										ENDIF
									BREAK
									CASE MBSV_SELL_OFFSHORE_TRANSFER_2
										IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
											IF IS_ENTITY_IN_ANGLED_AREA( veh, <<-2500.997314,5049.311523,0.454318>>, <<-2494.145264,5045.412109,5.454318>>, 12.500000)
												DETACH_ENTITY_FROM_CARGOBOB(vBob, veh)
												SET_VEHICLE_AUTOMATICALLY_ATTACHES(veh, FALSE)
												SET_VEHICLE_DISABLE_TOWING(veh, TRUE)
												PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - MAINTAIN_MISSION_ENTITY_MISSION_SPECIFIC_CLIENT - DETACH_ENTITY(veh) ")
											ENDIF
										ENDIF
									BREAK
								ENDSWITCH
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC MAINTAIN_UPDATING_MISSION_ENTITY_GLOBAL_VEHICLE_COORDS(INT iMissionEntity)

	IF IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_A_PARTICIPANT_HAS_ENABLED_MY_GLOBAL_BLIP)

		VEHICLE_INDEX viVehicle
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sMissionEntity[iMissionEntity].netId)
			viVehicle = NET_TO_VEH(serverBD.sMissionEntity[iMissionEntity].netId)
		ENDIF
		BOOL bDraw
		bDraw = TRUE
		
		IF NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_I_HAVE_BEEN_ENTERED_AT_LEAST_ONCE) 
		OR IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_DELIVERED)
		OR IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_FIND_BUYER_IN_CLUE_ZONE) 
			bDraw = FALSE
		ENDIF
		
		STORE_CONTRABAND_VEHICLE_COORDS(viVehicle, missionEntityVehicleCoordTimer[iMissionEntity], iMissionEntity , (NOT bDraw))
	
	ENDIF

ENDPROC

PROC MAINTAIN_NEAREST_MISSION_ENTITY()
	IF DOES_MEGA_BUSINESS_VARIATION_USE_MISSION_ENTITIES(GET_MEGA_BUSINESS_VARIATION())
		INT iMissionEntity
		INT iTempNearest = -1
		FLOAT fNearest = 99999.99
		FLOAT fDistBetween
		REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
			IF HAS_MISSION_ENTITY_BEEN_CREATED(iMissionEntity)
				IF NOT HAS_MISSION_ENTITY_BEEN_DELIVERED_OR_DESTROYED(iMissionEntity)
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
						VEHICLE_INDEX vehID = NET_TO_VEH(serverBD.sMissionEntity[iMissionEntity].netId)
						IF IS_ENTITY_ALIVE(vehID)
							fDistBetween = GET_DISTANCE_BETWEEN_ENTITIES(localPlayerPed, vehID, FALSE)
							IF fDistBetween < fNearest
								iTempNearest = iMissionEntity
								fNearest = fDistBetween
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF iNearestMissionEntity != iTempNearest
			PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - MAINTAIN_NEAREST_MISSION_ENTITY - New nearest mission entity is ", iTempNearest)
			iNearestMissionEntity = iTempNearest
		ELSE
			fMyDistanceFromNearestMissionEntity = fNearest
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DO_I_WANT_MISSION_ENTITY_TO_SPAWN()
	
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_I_WANT_MISSION_ENTITY_TO_SPAWN()
	
	INT iMissionEntity
	
	IF DOES_MEGA_BUSINESS_VARIATION_USE_MISSION_ENTITIES(GET_MEGA_BUSINESS_VARIATION())
		REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
			IF GET_MODE_STATE() > eMODESTATE_INIT
			AND DO_I_WANT_MISSION_ENTITY_TO_SPAWN()
				IF NOT IS_MISSION_ENTITY_CLIENT_BIT_SET(PARTICIPANT_ID(), iMissionEntity, eMISSIONENTITYCLIENTBITSET_I_WANT_TO_SPAWN)
					IF NOT IS_MISSION_ENTITY_CLIENT_BIT_SET(PARTICIPANT_ID(), iMissionEntity, eMISSIONENTITYCLIENTBITSET_I_WANT_TO_SPAWN)
						SET_MISSION_ENTITY_CLIENT_BIT(iMissionEntity, eMISSIONENTITYCLIENTBITSET_I_WANT_TO_SPAWN)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

PROC MAINTAIN_RESERVE_NETWORK_ENTITIES()
	
	IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_RESERVED_NETWORK_ENTITIES)	
		IF GET_MEGA_BUSINESS_VARIATION() != MBV_INVALID // Server has picked a variation.
		AND GET_MEGA_BUSINESS_VARIATION() !=  MBV_MAX
			INT iType = serverBD.iFMMCType
			
			INT iVariation = ENUM_TO_INT(GET_MEGA_BUSINESS_VARIATION())
			INT iSubvariation = ENUM_TO_INT(GET_MEGA_BUSINESS_SUBVARIATION())
			INT iNumMissionEntities = GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION()
			
			INT iNumObjects = GB_GET_BOSS_MISSION_NUM_OBJ_REQUIRED(iType, iVariation, iSubvariation, iNumMissionEntities)
			INT iNumPeds = GB_GET_BOSS_MISSION_NUM_PEDS_REQUIRED(iType, iVariation, iSubvariation, iNumMissionEntities)
			INT iNumVehicles = GB_GET_BOSS_MISSION_NUM_VEH_REQUIRED(iType, iVariation, serverBD.iOrganisationSizeOnLaunch, iSubvariation, iNumMissionEntities)
			
			iNumPeds += GET_VARIATION_GANG_CHASE_NUM_PEDS_PER_VEHICLE() * GET_VARIATION_GANG_CHASE_NUM_VEHICLES_PER_WAVE(TRUE)
			iNumVehicles += GET_VARIATION_GANG_CHASE_NUM_VEHICLES_PER_WAVE(TRUE)
			
			RESERVE_NETWORK_MISSION_OBJECTS(iNumObjects)
			RESERVE_NETWORK_MISSION_PEDS(iNumPeds)
			RESERVE_NETWORK_MISSION_VEHICLES(iNumVehicles)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - reserved ", iNumObjects, " network objects")
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - reserved ", iNumPeds, " network peds")
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - reserved ", iNumVehicles, " network vehicles")
			SET_LOCAL_BIT(eLOCALBITSET_RESERVED_NETWORK_ENTITIES)
		ENDIF
	ENDIF
	
ENDPROC	

FUNC BOOL MODE_NEEDS_CARWASH()
//	SWITCH GET_MEGA_BUSINESS_VARIATION()
//		CASE MBV_COLLECT_DJ
//			RETURN TRUE
//		BREAK
//	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

INT iCarwashProgress
PROC MAINTAIN_CARWASH_FLAGS()
	IF MODE_NEEDS_CARWASH()
		IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_CAR_WASH_COMPLETE)
			SWITCH iCarwashProgress
				CASE 0
					IF IS_PLAYER_IN_POSSESSION_OF_A_MISSION_ENTITY(localPlayerId)
						IF IS_PLAYER_USING_CARWASH(NETWORK_PLAYER_ID_TO_INT())
							iCarwashProgress++
							PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_CARWASH_FLAGS ", iCarwashProgress)
						ENDIF
					ENDIF
				BREAK
				
				CASE 1
					IF NOT IS_PLAYER_USING_CARWASH(NETWORK_PLAYER_ID_TO_INT())
						SET_CLIENT_BIT(eCLIENTBITSET_I_HAVE_COMPLETED_CAR_WASH)
						iCarwashProgress++
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_CARWASH_FLAGS ", iCarwashProgress)
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

PROC SERVER_MAINTAIN_CLOSEST_CARWASH()
	IF MODE_NEEDS_CARWASH()
		VECTOR vInCoords = GET_MISSION_ENTITY_COORDS(0, TRUE)
		
		INT iClosest = ciCARWASH_ONE
		// get the closest carwash
		IF VDIST2(vInCoords, GET_STATIC_BLIP_COORDS(STATIC_BLIP_AMBIENT_CARWASH_SHORT)) < VDIST2(vInCoords, GET_STATIC_BLIP_COORDS(STATIC_BLIP_AMBIENT_CARWASH_LONG))
			iClosest = ciCARWASH_TWO
		ENDIF
		
		IF serverBD.iClosestCarWash != iClosest
			serverBD.iClosestCarWash = iClosest
		ENDIF
	ENDIF
ENDPROC

FUNC VECTOR GET_CARWASH_COORDS()
	IF serverBD.iClosestCarWash = ciCARWASH_TWO
		RETURN GET_STATIC_BLIP_COORDS(STATIC_BLIP_AMBIENT_CARWASH_SHORT)
	ENDIF

	RETURN GET_STATIC_BLIP_COORDS(STATIC_BLIP_AMBIENT_CARWASH_LONG)
ENDFUNC	

CONST_INT ciNUM_CARWASHES 2
BLIP_INDEX blipCarwash[ciNUM_CARWASHES]
INT iStoredClosestCarwash = -1

PROC REMOVE_CARWASH_BLIPS()
	INT i
	REPEAT ciNUM_CARWASHES i
		IF DOES_BLIP_EXIST(blipCarwash[i])
		
			IF DOES_BLIP_HAVE_GPS_ROUTE(blipCarwash[i])
				SET_BLIP_ROUTE(blipCarwash[i], FALSE)
			ENDIF
			IF NOT IS_BLIP_SHORT_RANGE(blipCarwash[i])
				SET_BLIP_AS_SHORT_RANGE(blipCarwash[i], TRUE)
			ENDIF
		
			REMOVE_BLIP(blipCarwash[i])
		ENDIF
	ENDREPEAT
ENDPROC

PROC MAINTAIN_CARWASH_BLIPS()

	IF MODE_NEEDS_CARWASH()
		IF GET_MODE_STATE() = eMODESTATE_CAR_WASH
			IF serverBD.iClosestCarWash != -1
			
				IF iStoredClosestCarwash != -1
				AND iStoredClosestCarwash != serverBD.iClosestCarWash
					REMOVE_CARWASH_BLIPS()
					iStoredClosestCarwash = -1
				ELSE
					IF NOT DOES_BLIP_EXIST(blipCarwash[serverBD.iClosestCarWash])

						blipCarwash[serverBD.iClosestCarWash] = ADD_BLIP_FOR_COORD(GET_CARWASH_COORDS())
						SET_BLIP_PRIORITY(blipCarwash[serverBD.iClosestCarWash], BLIP_PRIORITY_HIGHES_SPECIAL_MED)
						SET_BLIP_SPRITE(blipCarwash[serverBD.iClosestCarWash], RADAR_TRACE_CAR_WASH)
						SET_BLIP_COLOUR_FROM_HUD_COLOUR(blipCarwash[serverBD.iClosestCarWash], HUD_COLOUR_YELLOW)

					ELSE
						iStoredClosestCarwash = serverBD.iClosestCarWash
						
						IF NOT DOES_BLIP_HAVE_GPS_ROUTE(blipCarwash[serverBD.iClosestCarWash])
							SET_BLIP_ROUTE(blipCarwash[serverBD.iClosestCarWash], TRUE)
						ENDIF
						IF IS_BLIP_SHORT_RANGE(blipCarwash[serverBD.iClosestCarWash])
							SET_BLIP_AS_SHORT_RANGE(blipCarwash[serverBD.iClosestCarWash], FALSE)
						ENDIF
			
					ENDIF
				ENDIF
			ENDIF
		ELSE
			REMOVE_CARWASH_BLIPS()
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_CLEAR_COLLECT_ITEM_AFTER_COLLECTING_ITEM()
//	SWITCH GET_MEGA_BUSINESS_VARIATION()
//		CASE MBV_COLLECT_DJ
//			RETURN GET_MODE_STATE() != eMODESTATE_COLLECT_ITEM
//		BREAK
//	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_CLEAR_ITEM_COLLECTION()
	IF SHOULD_CLEAR_COLLECT_ITEM_AFTER_COLLECTING_ITEM()
		IF IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_HAVE_COLLECTED_ITEM)
			CLEAR_CLIENT_BIT(eCLIENTBITSET_I_HAVE_COLLECTED_ITEM)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - CLIENT_PROCESSING - Cleared that I have arrived at goto location")
		ENDIF
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			IF IS_SERVER_BIT_SET(eSERVERBITSET_SOMEONE_COLLECTED_ITEM)
				CLEAR_SERVER_BIT(eSERVERBITSET_SOMEONE_COLLECTED_ITEM)
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - CLIENT_PROCESSING - Cleared server bit for participant arriving at goto location")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC MODEL_NAMES GET_FIND_ITEM_PORTABLE_PICKUP_MODEL(INT iPickup)
	UNUSED_PARAMETER(iPickup)
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_SNAPMATIC					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_Pap_Camera_01"))
		CASE MBV_COLLECT_DJ_STOLEN_BAG		RETURN prop_michael_backpack
	ENDSWITCH
	RETURN PROP_DRUG_PACKAGE
ENDFUNC

FUNC BOOL SHOULD_PLAYER_BE_ALLOWED_TO_COLLECT_ITEM()

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_STOLEN_BAG
			IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE)
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN TRUE

ENDFUNC

PROC MAINTAIN_I_HAVE_COLLECTED_ITEM()

	BOOL bAllowCollection = TRUE
	IF GET_MODE_STATE() = eMODESTATE_COLLECT_ITEM
		IF IS_SERVER_BIT_SET(eSERVERBITSET_CREATED_FIND_ITEM_PORTABLE_PICKUP)
			IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_SOMEONE_COLLECTED_ITEM)
				IF NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_HAVE_COLLECTED_ITEM)
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niFindItemPortablePickup[PICKUP_TO_CHECK_FOR()])
						bAllowCollection = SHOULD_PLAYER_BE_ALLOWED_TO_COLLECT_ITEM()
						IF IS_ENTITY_ATTACHED_TO_ENTITY(localPlayerPed, NET_TO_ENT(serverBD.niFindItemPortablePickup[PICKUP_TO_CHECK_FOR()]))
							IF TAKE_CONTROL_OF_NET_ID(serverBD.niFindItemPortablePickup[PICKUP_TO_CHECK_FOR()])
								DELETE_NET_ID(serverBD.niFindItemPortablePickup[PICKUP_TO_CHECK_FOR()])
								SET_CLIENT_BIT(eCLIENTBITSET_I_HAVE_COLLECTED_ITEM)
								PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_I_HAVE_COLLECTED_ITEM - I have reached the goto location. Setting eCLIENTBITSET_I_HAVE_COLLECTED_ITEM")
							ENDIF
						ELSE
							PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.niFindItemPortablePickup[PICKUP_TO_CHECK_FOR()]), (NOT bAllowCollection), TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_SETUP_GANG_CHASE_OPTIONS()
	IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_SETUP_GANG_CHASE_OPTIONS)
		IF DOES_VARIATION_HAVE_GANG_CHASE()
		AND HAS_ANY_MISSION_ENTITY_BEEN_CREATED()
			sGangChaseOptions.iTotalNumberOfWaves = GET_VARIATION_GANG_CHASE_NUM_WAVES()
			sGangChaseOptions.iTimeBetweenWaves = GET_VARIATION_GANG_CHASE_TIME_BETWEEN_WAVES()
			sGangChaseOptions.iNumPedsInEachVeh = GET_VARIATION_GANG_CHASE_NUM_PEDS_PER_VEHICLE()
			sGangChaseOptions.iNumVehInEachWave = GET_VARIATION_GANG_CHASE_NUM_VEHICLES_PER_WAVE()
			sGangChaseOptions.vDropffCoord = GET_VARIATION_GANG_CHASE_FINAL_DROP_OFF_COORD()
			sGangChaseOptions.mGangChasePed = GET_VARIATION_GANG_CHASE_PED_MODEL()
			sGangChaseOptions.mGangChaseVeh = GET_VARIATION_GANG_CHASE_VEHICLE_MODEL()
			sGangChaseOptions.iPedAccuracy = GET_VARIATION_GANG_CHASE_PED_ACCURACY()
			sGangChaseOptions.weaponGangChasePed = GET_VARIATION_GANG_CHASE_WEAPON()
			sGangChaseOptions.bDriversUsePistols = TRUE
			
			SWITCH GET_MEGA_BUSINESS_VARIATION()
				CASE MBV_SELL_SINGLE_DROP
					SWITCH GET_MEGA_BUSINESS_SUBVARIATION()
						CASE MBSV_SELL_SINGLE_DROP_0
							sGangChaseOptions.bSetVehicleColours[0] = TRUE
							sGangChaseOptions.iVehicleColour1[0] = 88
							sGangChaseOptions.iVehicleColour2[0] = 88
							sGangChaseOptions.bSetVehicleColours[1] = TRUE
							sGangChaseOptions.iVehicleColour1[1] = 88
							sGangChaseOptions.iVehicleColour2[1] = 88
						BREAK
						
						CASE MBSV_SELL_SINGLE_DROP_1
							sGangChaseOptions.bSetVehicleColours[0] = TRUE
							sGangChaseOptions.iVehicleColour1[0] = 145
							sGangChaseOptions.iVehicleColour2[0] = 0
							sGangChaseOptions.bSetVehicleColours[1] = TRUE
							sGangChaseOptions.iVehicleColour1[1] = 145
							sGangChaseOptions.iVehicleColour2[1] = 0
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH

//			sGangChaseOptions.relGangChase = GET_VARIATION_GANG_CHASE_RELGROUP()
	
			IF SHOULD_PREVENT_GANG_CHASE_LEAVING_VEHS()
				SET_GANG_CHASE_DONT_LEAVE_VEHICLE(sGangChaseOptions)
			ENDIF
			
			SET_LOCAL_BIT(eLOCALBITSET_SETUP_GANG_CHASE_OPTIONS)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_GLOBAL_REVEAL_SOUNDS()

	INT iMissionEntity
	
	REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
	
		IF HAS_NET_TIMER_STARTED(serverBD.sMissionEntity[iMissionEntity].eGlobalPingTimer)
		
			IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_PLAYED_FIVE_SECOND_GLOBAL_TIMER)
				IF HAS_NET_TIMER_EXPIRED(serverBD.sMissionEntity[iMissionEntity].eGlobalPingTimer, (GET_DELAY_GLOBAL_PING_TIME_LIMIT()-5000 ))
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_GLOBAL_REVEAL_SOUNDS - net timer serverBD.sMissionEntity[", iMissionEntity, "].eGlobalPingTimer has five seconds left. BLEEP BLEEP BLEEP")
					PLAY_SOUND_FRONTEND(-1, "5s", "MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
					SET_LOCAL_BIT(eLOCALBITSET_PLAYED_FIVE_SECOND_GLOBAL_TIMER)
				ENDIF
			ENDIF
			
			IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_PLAYED_GLOBAL_BLIP)
				IF HAS_NET_TIMER_EXPIRED(serverBD.sMissionEntity[iMissionEntity].eGlobalPingTimer, GET_DELAY_GLOBAL_PING_TIME_LIMIT())
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_GLOBAL_REVEAL_SOUNDS - net timer serverBD.sMissionEntity[", iMissionEntity, "].eGlobalPingTimer is done. BEEEEEP")
					PLAY_SOUND_FRONTEND(-1, "Crates_Blipped", "GTAO_Magnate_Boss_Modes_Soundset", FALSE)
					SET_LOCAL_BIT(eLOCALBITSET_PLAYED_GLOBAL_BLIP)
				ENDIF
			ENDIF
			
		ENDIF
		
	ENDREPEAT
	
ENDPROC

PROC INITIALISE_CLIENT_VARS()
	IF NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_INITIALISED_VARS)
		IF SHOULD_TRACK_CASH_DEDUCTION_DAMAGE()
			INT iLoop
			REPEAT MAX_SELL_ENTITIES iLoop
				playerBD[PARTICIPANT_ID_TO_INT()].iMissionEntityBonusCash[iLoop] = GET_MODE_TUNABLE_BONUS_CASH_VALUE()
			ENDREPEAT
		ENDIF
		
		IF LOAD_ASSETS()
			SET_CLIENT_BIT(eCLIENTBITSET_INITIALISED_VARS)
		ENDIF
	ENDIF
ENDPROC

PROC SETUP_DAMAGE_TRACKING(BOOL bON)
	IF DOES_MEGA_BUSINESS_VARIATION_USE_MISSION_ENTITIES(GET_MEGA_BUSINESS_VARIATION())
		INT i
		REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() i
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[i].netId)
				IF bON
					IF NOT IS_DAMAGE_TRACKER_ACTIVE_ON_NETWORK_ID(serverBD.sMissionEntity[i].netId)
						ACTIVATE_DAMAGE_TRACKER_ON_NETWORK_ID(serverBD.sMissionEntity[i].netId, TRUE)
						PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - SETUP_DAMAGE_TRACKING - Activated for mission entity #", i)
					ENDIF
				ELSE
					IF IS_DAMAGE_TRACKER_ACTIVE_ON_NETWORK_ID(serverBD.sMissionEntity[i].netId)
						ACTIVATE_DAMAGE_TRACKER_ON_NETWORK_ID(serverBD.sMissionEntity[i].netId, FALSE)
						PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - SETUP_DAMAGE_TRACKING - Deactivated for mission entity #", i)
					ENDIF
				ENDIF
			ENDIF				
		ENDREPEAT
	ENDIF
ENDPROC

PROC MAINTAIN_DAMAGE_TRACKING_ACTIVATION()
	SETUP_DAMAGE_TRACKING(TRUE)
ENDPROC

FUNC INT GET_NEAREST_PED(FLOAT &fDistance)
	INT iPedLoop
	INT iNearestPed = -1
	FLOAT fDistBetween
	FLOAT fNearestDist = 99999.99
	VECTOR vPlayerCoords = GET_ENTITY_COORDS(localPlayerPed, FALSE)
	VECTOR vPedCoords
	REPEAT NUM_STAFF_PEDS iPedLoop
		IF NOT IS_PED_BIT_SET(iPedLoop, ePEDBITSET_COLLECTED)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[iPedLoop].netId)
			AND IS_ENTITY_ALIVE(NET_TO_PED(serverBD.sPed[iPedLoop].netId))
			AND NOT SHOULD_CREATE_PED_IN_MISSION_ENTITY(iPedLoop)
				vPedCoords = GET_ENTITY_COORDS(NET_TO_PED(serverBD.sPed[iPedLoop].netId))
				fDistBetween = GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, vPedCoords)
				IF fDistBetween < fNearestDist
					iNearestPed = iPedLoop
					fNearestDist = fDistBetween
					fDistance = fNearestDist
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN iNearestPed
ENDFUNC

PROC DO_PED_COLLECT_HELP()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_SNAPMATIC
			SWITCH GET_MEGA_BUSINESS_SUBVARIATION()
				CASE MBSV_SNAPMATIC_0
				CASE MBSV_SNAPMATIC_2
					PRINT_HELP_FOREVER("MBS_HLP_SIG11")
				BREAK
				CASE MBSV_SNAPMATIC_1
				CASE MBSV_SNAPMATIC_3
					PRINT_HELP_FOREVER("MBS_HLP_SIG12")
				BREAK
			ENDSWITCH
		BREAK
		DEFAULT
			SWITCH serverBD.iPedStaffBeingCollected
				CASE 0	
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MBS_HLP_SIG1")
						PRINT_HELP_FOREVER("MBS_HLP_SIG1")
					ENDIF
				BREAK
				CASE 1	
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MBS_HLP_SIG2")
						PRINT_HELP_FOREVER("MBS_HLP_SIG2")
					ENDIF
				BREAK
				CASE 2	
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MBS_HLP_SIG3")
						PRINT_HELP_FOREVER("MBS_HLP_SIG3")
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

FUNC STRING GET_HORN_HELP()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_PRIVATE_TAXI
			SWITCH GET_MEGA_BUSINESS_SUBVARIATION()
				CASE MBSV_PRIVATE_TAXI_0
				CASE MBSV_PRIVATE_TAXI_1
					IF serverBD.iCurrentGotoLocation != -1
						IF serverBD.sPed[serverBD.iCurrentGotoLocation].bIsMale
							RETURN "MBS_HLP_SIG5"
						ELSE
							RETURN "MBS_HLP_SIG5b"
						ENDIF
					ENDIF
				BREAK	
				CASE MBSV_PRIVATE_TAXI_2
				CASE MBSV_PRIVATE_TAXI_3
					IF serverBD.iCurrentGotoLocation != -1
						IF serverBD.sPed[serverBD.iCurrentGotoLocation].bIsMale
							RETURN "MBS_HLP_SIG6"
						ELSE
							RETURN "MBS_HLP_SIG6b"
						ENDIF
					ENDIF
				BREAK	
				CASE MBSV_PRIVATE_TAXI_4
				CASE MBSV_PRIVATE_TAXI_5
					SWITCH serverBD.iCurrentGotoLocation
						CASE FIRST_GOTO_LOCATION
							RETURN "MBS_HLP_SIG7"
						CASE SECOND_GOTO_LOCATION
							RETURN "MBS_HLP_SIG8"
					ENDSWITCH
				BREAK
				CASE MBSV_PRIVATE_TAXI_6
				CASE MBSV_PRIVATE_TAXI_7
					SWITCH serverBD.iCurrentGotoLocation
						CASE FIRST_GOTO_LOCATION
							RETURN "MBS_HLP_SIG9"
						CASE SECOND_GOTO_LOCATION
							RETURN "MBS_HLP_SIG10"
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

PROC CLEAR_PED_COLLECT_HELP()
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MBS_HLP_SIG1")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MBS_HLP_SIG2")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MBS_HLP_SIG3")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MBS_HLP_SIG6")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MBS_HLP_SIG11")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MBS_HLP_SIG12")
		CLEAR_HELP()
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(GET_HORN_HELP())
	AND IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HORN_HELP())
		CLEAR_HELP()
	ENDIF
ENDPROC

PROC DO_PED_COLLECT_SEAT_FULL_HELP()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_SNAPMATIC
			PRINT_HELP("MBS_HLP_NFS5")
		BREAK
		DEFAULT
			SWITCH serverBD.iPedStaffBeingCollected
				CASE 0	PRINT_HELP("MBS_HLP_NFS1")	BREAK
				CASE 1	PRINT_HELP("MBS_HLP_NFS2")	BREAK
				CASE 2	PRINT_HELP("MBS_HLP_NFS3")	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL IS_PED_IN_CORRECT_STATE_FOR_COLLECTION(INT iPed)
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_STAFF
			RETURN GET_PED_STATE(iPed) = ePEDSTATE_DO_SCENARIOS
		CASE MBV_SNAPMATIC
			IF SHOULD_PED_EXIST_FOR_ALL(iPed)
				IF GET_PED_STATE(iPed) = ePEDSTATE_DO_SCENARIOS
					RETURN TRUE
				ENDIF
			ENDIF
			
			RETURN FALSE
		BREAK		
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

PROC STOP_VEHICLE_NEAR_PEDS(INT iMyMissionEntity)
	IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_STOPPED_VEHICLE_NEAR_PEDS)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[iMyMissionEntity].netId)
			IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sMissionEntity[iMyMissionEntity].netId)
				IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(NET_TO_VEH(serverBD.sMissionEntity[iMyMissionEntity].netId), STOPPING_DIST())
					PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - STOP_VEHICLE_NEAR_PEDS - Stopped Mission entity ", iMyMissionEntity, " near peds!")
					SET_LOCAL_BIT(eLOCALBITSET_STOPPED_VEHICLE_NEAR_PEDS)
				ENDIF
			ENDIF
		ENDIF 
	ENDIF
ENDPROC

PROC CLEAR_STOP_VEHICLE_NEAR_PEDS()
	IF IS_LOCAL_BIT_SET(eLOCALBITSET_STOPPED_VEHICLE_NEAR_PEDS)
		PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - CLEAR_STOP_VEHICLE_NEAR_PEDS - Cleared")
		CLEAR_LOCAL_BIT(eLOCALBITSET_STOPPED_VEHICLE_NEAR_PEDS)
	ENDIF
ENDPROC

FUNC BOOL PED_IN_COLLECTABLE_STAGE()

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_SNAPMATIC
			IF serverBD.iTotalDeliveredCount < (GET_NUM_DROP_OFFS_FOR_MEGA_BUSINESS_VARIATION(GET_MEGA_BUSINESS_VARIATION()) -1)
			
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH

	RETURN TRUE
ENDFUNC

PROC MAINTAIN_PED_COLLECTION()
	INT iMissionEntity = GET_MISSION_ENTITY_PLAYER_IS_HOLDING(localPlayerId)
	INT iNearestPed
	FLOAT fDistanceFromPed
	
	IF iMissionEntity != -1
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
		AND IS_ENTITY_ALIVE(NET_TO_VEH(serverBD.sMissionEntity[iMissionEntity].netId))
			iNearestPed = GET_NEAREST_PED(fDistanceFromPed)
			IF iNearestPed != -1
				IF NOT IS_PED_BIT_SET(iNearestPed, ePEDBITSET_COLLECTED)
				AND NOT IS_PED_CLIENT_BIT_SET(PARTICIPANT_ID(), iNearestPed, ePEDCLIENTBITSET_COLLECTED)
					IF NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_NEAR_PED_TO_COLLECT)
						IF fDistanceFromPed < ciMIN_PED_PICKUP_DISTANCE
						AND PED_IN_COLLECTABLE_STAGE()
							IF NOT IS_MISSION_ENTITY_CLIENT_BIT_SET(PARTICIPANT_ID(), iMissionEntity, eMISSIONENTITYCLIENTBITSET_NEAR_PED_TO_COLLECT)
								SET_MISSION_ENTITY_CLIENT_BIT(iMissionEntity, eMISSIONENTITYCLIENTBITSET_NEAR_PED_TO_COLLECT)
								playerBD[PARTICIPANT_ID_TO_INT()].iPedStaffBeingCollected = iNearestPed
								SET_LOCAL_BIT(eLOCALBITSET_RADAR_SHOULD_ZOOM)
								STOP_VEHICLE_NEAR_PEDS(iMissionEntity)
								PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_PED_COLLECTION - I am close enough to ped ", iNearestPed, " to signal them to enter")
							ENDIF
						ENDIF
					ELSE
						IF serverBD.iPedStaffBeingCollected != -1
							IF NOT IS_PED_CLIENT_BIT_SET(PARTICIPANT_ID(), serverBD.iPedStaffBeingCollected, ePEDCLIENTBITSET_SIGNALED_TO_ENTER_VEHICLE)
							AND IS_PED_IN_CORRECT_STATE_FOR_COLLECTION(serverBD.iPedStaffBeingCollected)
								IF IS_PLAYER_PRESSING_HORN(localPlayerId)
									CLEAR_HELP()
									SET_PED_CLIENT_BIT(serverBD.iPedStaffBeingCollected, ePEDCLIENTBITSET_SIGNALED_TO_ENTER_VEHICLE)
									PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_PED_COLLECTION - I have tooted horn and signaled ped ", iNearestPed, " to enter")
								ELSE
									DO_PED_COLLECT_HELP()
								ENDIF
							ENDIF
							IF fDistanceFromPed > ciMAX_PED_PICKUP_DISTANCE
								IF IS_MISSION_ENTITY_CLIENT_BIT_SET(PARTICIPANT_ID(), iMissionEntity, eMISSIONENTITYCLIENTBITSET_NEAR_PED_TO_COLLECT)
									CLEAR_MISSION_ENTITY_CLIENT_BIT(iMissionEntity, eMISSIONENTITYCLIENTBITSET_NEAR_PED_TO_COLLECT)
									playerBD[PARTICIPANT_ID_TO_INT()].iPedStaffBeingCollected = -1
									CLEAR_PED_COLLECT_HELP()
									CLEAR_LOCAL_BIT(eLOCALBITSET_RADAR_SHOULD_ZOOM)
									CLEAR_STOP_VEHICLE_NEAR_PEDS()
									PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_PED_COLLECTION - I am no longer close enough to ped ", iNearestPed, " to signal them to enter")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			CLEAR_PED_COLLECT_HELP()
			IF IS_MISSION_ENTITY_CLIENT_BIT_SET(PARTICIPANT_ID(), iMissionEntity, eMISSIONENTITYCLIENTBITSET_NEAR_PED_TO_COLLECT)
				CLEAR_MISSION_ENTITY_CLIENT_BIT(iMissionEntity, eMISSIONENTITYCLIENTBITSET_NEAR_PED_TO_COLLECT)
				playerBD[PARTICIPANT_ID_TO_INT()].iPedStaffBeingCollected = -1
				CLEAR_LOCAL_BIT(eLOCALBITSET_RADAR_SHOULD_ZOOM)
				CLEAR_STOP_VEHICLE_NEAR_PEDS()
				PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_STAFF_COLLECTION - I am no longer close enough to ped ", iNearestPed, " to signal them to enter")
			ENDIF
		ENDIF
	ELSE
		CLEAR_PED_COLLECT_HELP()
		IF IS_MISSION_ENTITY_CLIENT_BIT_SET(PARTICIPANT_ID(), iMyLastMissionEntity, eMISSIONENTITYCLIENTBITSET_NEAR_PED_TO_COLLECT)
			CLEAR_MISSION_ENTITY_CLIENT_BIT(iMyLastMissionEntity, eMISSIONENTITYCLIENTBITSET_NEAR_PED_TO_COLLECT)
			playerBD[PARTICIPANT_ID_TO_INT()].iPedStaffBeingCollected = -1
			CLEAR_LOCAL_BIT(eLOCALBITSET_RADAR_SHOULD_ZOOM)
			CLEAR_STOP_VEHICLE_NEAR_PEDS()
			PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_STAFF_COLLECTION - I am no longer in a mission entity")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAVE_ALL_LOCATION_PEDS_BEEN_COLLECTED()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_PRIVATE_TAXI
			SWITCH serverBD.iCurrentGotoLocation
				CASE FIRST_GOTO_LOCATION
					IF IS_PED_BIT_SET(0, ePEDBITSET_COLLECTED)
						RETURN TRUE
					ENDIF
				BREAK
				CASE SECOND_GOTO_LOCATION
					IF IS_PED_BIT_SET(1, ePEDBITSET_COLLECTED)
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
//		CASE MBV_SNAPMATIC
//			SWITCH serverBD.iCurrentGotoLocation
//				CASE FIRST_GOTO_LOCATION
//					IF IS_PED_BIT_SET(0, ePEDBITSET_COLLECTED)
//						RETURN TRUE
//					ENDIF
//				BREAK
//				CASE SECOND_GOTO_LOCATION
//					IF IS_PED_BIT_SET(1, ePEDBITSET_COLLECTED)
//						RETURN TRUE
//					ENDIF
//				BREAK
//			ENDSWITCH
//		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_VARIATION_USE_SEQUENTIAL_PED_COLLECTION()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_PRIVATE_TAXI
//		CASE MBV_SPECIAL_REQUEST
//		CASE MBV_SNAPMATIC
//		CASE MBV_COLLECT_DJ
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_VARIATION_USE_SIGNAL_OBJECTIVE()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_STAFF
			RETURN IS_MISSION_ENTITY_CLIENT_BIT_SET(PARTICIPANT_ID(), 0, eMISSIONENTITYCLIENTBITSET_NEAR_PED_TO_COLLECT)
		CASE MBV_PRIVATE_TAXI
		CASE MBV_SNAPMATIC
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_COORD_FOR_PED_PICKUP_CHECK()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_PRIVATE_TAXI
			SWITCH serverBD.iCurrentGotoLocation
				CASE FIRST_GOTO_LOCATION
					RETURN GET_MEGA_BUSINESS_PED_SPAWN_COORDS(0, GET_MEGA_BUSINESS_VARIATION(), GET_MEGA_BUSINESS_SUBVARIATION())
				
				CASE SECOND_GOTO_LOCATION
					RETURN GET_MEGA_BUSINESS_PED_SPAWN_COORDS(1, GET_MEGA_BUSINESS_VARIATION(), GET_MEGA_BUSINESS_SUBVARIATION())
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC BOOL IS_MISSION_ENTITY_IN_RANGE_OF_LOCATION_PEDS()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
		VEHICLE_INDEX vehID = NET_TO_VEH(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
		IF IS_ENTITY_ALIVE(vehID)
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehID, GET_COORD_FOR_PED_PICKUP_CHECK()) < ciMIN_PED_PICKUP_DISTANCE
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_LOCATION_PEDS_ENTER()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_PRIVATE_TAXI
			SWITCH serverBD.iCurrentGotoLocation
				CASE FIRST_GOTO_LOCATION
					IF GET_PED_STATE(0) != ePEDSTATE_DO_SCENARIOS
						RETURN FALSE
					ENDIF
				BREAK
				CASE SECOND_GOTO_LOCATION
					IF GET_PED_STATE(1) != ePEDSTATE_DO_SCENARIOS
						RETURN FALSE
					ENDIF
				BREAK
			ENDSWITCH
			
			IF NOT IS_MISSION_ENTITY_IN_RANGE_OF_LOCATION_PEDS()
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

PROC BRING_VEHICLE_TO_HALT_NEAR_PEDS()
	IF IS_MISSION_ENTITY_IN_RANGE_OF_LOCATION_PEDS()
		STOP_VEHICLE_NEAR_PEDS(GET_MISSION_ENTITY_PLAYER_IS_HOLDING(localPlayerId))
	ELSE
		CLEAR_STOP_VEHICLE_NEAR_PEDS()
	ENDIF
ENDPROC

PROC MAINTAIN_SEQUENTIAL_PED_COLLECTION()
	IF IS_PLAYER_IN_POSSESSION_OF_A_MISSION_ENTITY(localPlayerId)
	AND NOT IS_SERVER_BIT_SET(GET_SERVER_LOCATION_PEDS_SHOULD_ENTER_BIT())
	AND NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(), GET_CLIENT_LOCATION_PEDS_SHOULD_ENTER_BIT())
		BRING_VEHICLE_TO_HALT_NEAR_PEDS()
		IF CAN_LOCATION_PEDS_ENTER()
			IF IS_PLAYER_PRESSING_HORN(localPlayerId)
				IF NOT IS_STRING_NULL_OR_EMPTY(GET_HORN_HELP())
				AND IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HORN_HELP())
					CLEAR_HELP()
				ENDIF
				SET_CLIENT_BIT(GET_CLIENT_LOCATION_PEDS_SHOULD_ENTER_BIT())
				PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_PED_COLLECTION_PRIVATE_TAXI - I have tooted horn and signaled peds at location ", serverBD.iCurrentGotoLocation, " to enter")
			ELSE
				IF NOT IS_STRING_NULL_OR_EMPTY(GET_HORN_HELP())
				AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HORN_HELP())
					PRINT_HELP_FOREVER(GET_HORN_HELP())
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_STRING_NULL_OR_EMPTY(GET_HORN_HELP())
			AND IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HORN_HELP())
				CLEAR_HELP()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// WANTED LEVEL EVENT HANDLING
FUNC BOOL SHOULD_APPLY_WANTED_LEVEL(INT iMaxWantedLevel, BOOL bDamagedCop, BOOL bApplyAfterMissionEnd = FALSE)
	
	IF iMaxWantedLevel <= 0
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] [SM_WANTED] - SHOULD_APPLY_WANTED_LEVEL - RETURN FALSE - Max wanted level is 0.")
		RETURN FALSE
	ENDIF
	
	IF NOT bApplyAfterMissionEnd
		IF GET_END_REASON() != eENDREASON_NO_REASON_YET
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] [SM_WANTED] - SHOULD_APPLY_WANTED_LEVEL - RETURN FALSE - Mission ending, forget about wanted level.")
			RETURN FALSE
		ENDIF
	ENDIF

	IF IS_PLAYER_IN_MP_PROPERTY(localPlayerId, FALSE)
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] [SM_WANTED] - SHOULD_APPLY_WANTED_LEVEL - RETURN FALSE - Player is inside a property.")
		RETURN FALSE
	ENDIF
	
	IF bDamagedCop
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] [SM_WANTED] - SHOULD_APPLY_WANTED_LEVEL - RETURN TRUE - Damaged cop, always give wanted level.")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_GANG_BRIBING_POLICE()
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] [SM_WANTED] - SHOULD_APPLY_WANTED_LEVEL - RETURN FALSE - Player gang is bribing police.")
		RETURN FALSE
	ENDIF
	
	IF GET_PLAYER_WANTED_LEVEL(localPlayerId) = iMaxWantedLevel
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] [SM_WANTED] - SHOULD_APPLY_WANTED_LEVEL - RETURN FALSE - Player already has a wanted level.")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC RESET_WANTED_LEVEL_EVENT_STRUCT_DATA()
	sWantedStruct.iWantedLevel = 0
	sWantedStruct.iDelay = 0
	sWantedStruct.bDamagedCop = FALSE
	RESET_NET_TIMER(sWantedStruct.stDelayTimer)
	
	PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] [SM_WANTED] - RESET_WANTED_LEVEL_EVENT_STRUCT_DATA - Reset local wanted level event data. Callstack follows.")
	DEBUG_PRINTCALLSTACK()
ENDPROC

FUNC BOOL SHOULD_APPLY_WANTED_LEVEL_AFTER_MISSION_END()

//	SWITCH GET_MEGA_BUSINESS_VARIATION()
//		CASE MBV_COLLECT_STAFF
//			RETURN FALSE
//	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

PROC MAINTAIN_WANTED_LEVEL_EVENTS()
	IF IS_LOCAL_BIT_SET(eLOCALBITSET_NEED_DELAYED_WANTED_LEVEL)
//		IF NOT SHOULD_DELAY_WANTED_LEVEL_BY_CONDITION(sWantedStruct.iDelay)
			IF HAS_NET_TIMER_EXPIRED(sWantedStruct.stDelayTimer, sWantedStruct.iDelay)
			OR sWantedStruct.iDelay = 0
				INT iMaxWantedLevel = GET_MAX_WANTED_LEVEL_FOR_VARIATION()
				INT iWantedGiven
				IF SHOULD_APPLY_WANTED_LEVEL(iMaxWantedLevel, sWantedStruct.bDamagedCop, SHOULD_APPLY_WANTED_LEVEL_AFTER_MISSION_END())
					IF GET_MAX_WANTED_LEVEL() != iMaxWantedLevel
						SET_MAX_WANTED_LEVEL(iMaxWantedLevel)
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] [SM_WANTED] - MAINTAIN_WANTED_LEVEL_EVENTS - Max wanted level set to ", iMaxWantedLevel, " as previous maximum was incorrect.")
					ENDIF
					
					IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayerID)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bNoCopsActive)
						RESET_LESTER_NO_COPS()
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] [SM_WANTED] - MAINTAIN_WANTED_LEVEL_EVENTS - g_bNoCopsActive = TRUE - Calling RESET_LESTER_NO_COPS in order to give wanted level.")
					ENDIF
					
					iWantedGiven = sWantedStruct.iWantedLevel
					IF sWantedStruct.iWantedLevel > iMaxWantedLevel
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] [SM_WANTED] - MAINTAIN_WANTED_LEVEL_EVENTS - not giving sWantedStruct.iWantedLevel as it is more than the cap: sWantedStruct.iWantedLevel = ", sWantedStruct.iWantedLevel, "; cap = ", iMaxWantedLevel)
						iWantedGiven = iMaxWantedLevel
					ENDIF
					
					
					SET_PLAYER_WANTED_LEVEL(localPlayerId, iWantedGiven)
					SET_PLAYER_WANTED_LEVEL_NOW(localPlayerId)
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] [SM_WANTED] - MAINTAIN_WANTED_LEVEL_EVENTS - Local player given ", iWantedGiven, " star wanted level after delay of ", sWantedStruct.iDelay,"ms.")
					
//					IF SHOULD_CREATE_SHOCKING_EVENT_ON_WANTED()
//						ADD_SHOCKING_EVENT_FOR_ENTITY(EVENT_SHOCKING_PED_SHOT,localPlayerPed)
//						TELL_GROUP_PEDS_IN_AREA_TO_ATTACK(localPlayerPed,GET_ENTITY_COORDS(localPlayerPed,FALSE),20.0,RELGROUPHASH_AMBIENT_ARMY)
//						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] [SM_WANTED] - MAINTAIN_WANTED_LEVEL_EVENTS - ADD_SHOCKING_EVENT_FOR_ENTITY")
//					ENDIF
					
					RESET_WANTED_LEVEL_EVENT_STRUCT_DATA()
					CLEAR_LOCAL_BIT(eLOCALBITSET_NEED_DELAYED_WANTED_LEVEL)
				ELSE
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] [SM_WANTED] - MAINTAIN_WANTED_LEVEL_EVENTS - Local player not given ", sWantedStruct.iWantedLevel, " star wanted level after delay of ", sWantedStruct.iDelay,"ms as SHOULD_APPLY_WANTED_LEVEL returned FALSE.")
					
					RESET_WANTED_LEVEL_EVENT_STRUCT_DATA()
					CLEAR_LOCAL_BIT(eLOCALBITSET_NEED_DELAYED_WANTED_LEVEL)
				ENDIF
			ENDIF
//		ELSE
//			#IF IS_DEBUG_BUILD
//			IF (GET_FRAME_COUNT() % 30) = 0
//				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] [SM_WANTED] - MAINTAIN_WANTED_LEVEL_EVENTS - SHOULD_DELAY_WANTED_LEVEL_BY_CONDITION = TRUE.")
//			ENDIF
//			#ENDIF
//		ENDIF
	ENDIF
ENDPROC

PROC TOGGLE_PAID_WANTED_LEVEL_REMOVAL_SERVICES(BOOL bEnable)
	IF bEnable
		IF IS_LOCAL_BIT_SET(eLOCALBITSET_BLOCKED_PAID_WANTED_LEVEL_REMOVAL)
			GB_CLEAR_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_LOSE_COPS_ABILITY)
			GB_CLEAR_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_BRIBE_AUTHORITIES)
			GB_CLEAR_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_BLIND_EYE)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] [MB_WANTED] - TOGGLE_PAID_WANTED_LEVEL_REMOVAL_SERVICES - Enabling paid wanted level removal services")
			CLEAR_LOCAL_BIT(eLOCALBITSET_BLOCKED_PAID_WANTED_LEVEL_REMOVAL)
		ENDIF
	ELSE
		IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_BLOCKED_PAID_WANTED_LEVEL_REMOVAL)
			GB_SET_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_LOSE_COPS_ABILITY)
			GB_SET_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_BRIBE_AUTHORITIES)
			GB_SET_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_BLIND_EYE)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] [MB_WANTED] - TOGGLE_PAID_WANTED_LEVEL_REMOVAL_SERVICES - Disabling paid wanted level removal services")
			SET_LOCAL_BIT(eLOCALBITSET_BLOCKED_PAID_WANTED_LEVEL_REMOVAL)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_BLOCK_LOSE_COPS_ABILITY()

	SWITCH GET_MEGA_BUSINESS_VARIATION()
//		CASE MBV_SELL_UNDERCOVER_COPS
		CASE MBV_COLLECT_DJ_HOOKED
		CASE MBV_SELL_SINGLE_DROP
		
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_BLOCK_BRIBE_AUTHORITIES()

	SWITCH GET_MEGA_BUSINESS_VARIATION()
	
		CASE MBV_SELL_UNDERCOVER_COPS
		CASE MBV_COLLECT_DJ_HOOKED
		CASE MBV_SELL_SINGLE_DROP
		
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_BLOCK_BLIND_EYE()

	SWITCH GET_MEGA_BUSINESS_VARIATION()
	
		CASE MBV_SELL_UNDERCOVER_COPS
		CASE MBV_COLLECT_DJ_HOOKED
		CASE MBV_SELL_SINGLE_DROP
		
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

PROC MAINTAIN_BLOCKING_LOSE_COPS()

	IF SHOULD_BLOCK_LOSE_COPS_ABILITY()
		GB_SET_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_LOSE_COPS_ABILITY)
	ELSE
		GB_CLEAR_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_LOSE_COPS_ABILITY)
	ENDIF
	
	IF SHOULD_BLOCK_BRIBE_AUTHORITIES()
		GB_SET_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_BRIBE_AUTHORITIES)
	ELSE
		GB_CLEAR_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_BRIBE_AUTHORITIES)
	ENDIF
	
	IF SHOULD_BLOCK_BLIND_EYE()
		GB_SET_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_BLIND_EYE)
	ELSE
		GB_CLEAR_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_BLIND_EYE)
	ENDIF

ENDPROC

FUNC BOOL SHOULD_PEDS_REACT_FROM_SHOOTING_IN_AREA()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_VIP_RESCUE
		CASE MBV_RIVAL_SUPPLY
		CASE MBV_MUSCLE_OUT
		CASE MBV_STOLEN_SUPPLIES
		CASE MBV_PRIVATE_TAXI
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PEDS_BE_CENTRE_OF_ACTION()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_PRIVATE_TAXI
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_SHOOTING_IN_AREA_RANGE()

	IF IS_PED_IN_ANY_HELI(localPlayerPed)
	OR IS_PED_IN_ANY_PLANE(localPlayerPed)
		RETURN 150.0
	ENDIF

	RETURN 50.0
ENDFUNC

PROC MAINTAIN_CLIENT_SHOULD_ALL_PEDS_REACT_CHECKS()

	IF IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_PED_HAS_BEEN_ALERTED_BY_SIGHT)
	OR IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_PED_HAS_BEEN_ALERTED_BY_GUNFIRE)
	OR IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_PED_HAS_BEEN_ALERTED_BY_NOISE)
		IF NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_WANT_PEDS_TO_REACT)
			SET_CLIENT_BIT(eCLIENTBITSET_I_WANT_PEDS_TO_REACT)
		ENDIF
	ENDIF
	
	// Variation specific reactions
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_VIP_RESCUE
			IF NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_WANT_PEDS_TO_REACT)
			AND NOT IS_SERVER_BIT_SET(eSERVERBITSET_MAKE_ALL_PEDS_REACT)
			AND NOT HAS_ANY_MISSION_ENTITY_BEEN_ENTERED_AT_LEAST_ONCE()
				IF GET_PLAYER_WANTED_LEVEL(localPlayerId) > 0
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(localPlayerPed, GET_MEGA_BUSINESS_MISSION_ENTITY_SPAWN_COORDS(MISSION_ENTITY_ONE, GET_MEGA_BUSINESS_VARIATION(), GET_MEGA_BUSINESS_SUBVARIATION(), serverBD.nightclubID)) < 50
						SET_CLIENT_BIT(eCLIENTBITSET_I_WANT_PEDS_TO_REACT)
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_CLIENT_SHOULD_ALL_PEDS_REACT_CHECKS - I am wanted within 50m of mission entity. Peds should react.")
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
		
	IF SHOULD_PEDS_REACT_FROM_SHOOTING_IN_AREA()
		INT iVeh
		VECTOR vCentreOfAction
		FLOAT fShootingReactDist = GET_SHOOTING_IN_AREA_RANGE()
		BOOL bReact = FALSE
		INT iPed
		
		IF DOES_MEGA_BUSINESS_VARIATION_USE_MISSION_ENTITIES(GET_MEGA_BUSINESS_VARIATION())
			IF SHOULD_PEDS_BE_CENTRE_OF_ACTION()
				REPEAT GET_NUM_VARIATION_PEDS() iPed
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[iPed].netId)
					AND IS_ENTITY_ALIVE(NET_TO_PED(serverBD.sPed[iPed].netId))
						vCentreOfAction = GET_ENTITY_COORDS(NET_TO_PED(serverBD.sPed[iPed].netId))
						bReact = FALSE
					
						IF (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(localPlayerPed, vCentreOfAction) <= fShootingReactDist
						AND IS_PED_SHOOTING(localPlayerPed))
							bReact = TRUE
							PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_CLIENT_SHOULD_ALL_PEDS_REACT_CHECKS - I am shooting within ", fShootingReactDist, "m of ped ", iPed, ". Peds should react.")
						ENDIF
						
						IF IS_BULLET_IN_AREA(vCentreOfAction, 30.0)
							bReact = TRUE
							PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_CLIENT_SHOULD_ALL_PEDS_REACT_CHECKS - There is a bullet within 30m of ped ", iPed, ". Peds should react.")
						ENDIF
						
						IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, vCentreOfAction, 50.0)
							bReact = TRUE
							PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_CLIENT_SHOULD_ALL_PEDS_REACT_CHECKS - There is an explosion within 50m of ped ", iPed, ". Peds should react.")
						ENDIF
						
						IF bReact
							IF SHOULD_ONLY_GROUPED_PEDS_REACT()
								IF GET_PED_REACT_GROUP(iPed) != -1
									IF NOT GET_GROUP_SHOULD_REACT(GET_PED_REACT_GROUP(iPed))
									AND NOT GET_I_WANT_GROUP_TO_REACT(GET_PED_REACT_GROUP(iPed))
										SET_I_WANT_GROUP_TO_REACT(GET_PED_REACT_GROUP(iPed))
										PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_CLIENT_SHOULD_ALL_PEDS_REACT_CHECKS - SHOULD_PEDS_REACT_ON_SCRIPT_ENTITY_DAMAGE - SET_I_WANT_GROUP_TO_REACT - Ped: ", iPed, ", Group: ", GET_PED_REACT_GROUP(iPed))
									ENDIF
								ENDIF
							ELSE
								SET_I_WANT_PEDS_TO_REACT(iPed)
								PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_CLIENT_SHOULD_ALL_PEDS_REACT_CHECKS - SHOULD_PEDS_REACT_ON_SCRIPT_ENTITY_DAMAGE - SET_I_WANT_PEDS_TO_REACT - Ped: ", iPed)
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
			ELSE
				REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iVeh
					IF IS_MISSION_ENTITY_OK(iVeh)
						vCentreOfAction = GET_MISSION_ENTITY_COORDS(iVeh)
						bReact = FALSE
					
						IF (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(localPlayerPed, vCentreOfAction) <= fShootingReactDist
						AND IS_PED_SHOOTING(localPlayerPed))
							bReact = TRUE
							PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_CLIENT_SHOULD_ALL_PEDS_REACT_CHECKS - I am shooting within ", fShootingReactDist, "m of mission entity ", iVeh, ". Peds should react.")
						ENDIF
						
						IF IS_BULLET_IN_AREA(vCentreOfAction, 30.0)
							bReact = TRUE
							PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_CLIENT_SHOULD_ALL_PEDS_REACT_CHECKS - There is a bullet within 30m of mission entity ", iVeh, ". Peds should react.")
						ENDIF
						
						IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, vCentreOfAction, 50.0)
							bReact = TRUE
							PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_CLIENT_SHOULD_ALL_PEDS_REACT_CHECKS - There is an explosion within 50m of mission entity ", iVeh, ". Peds should react.")
						ENDIF
						
						IF bReact
							IF NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_WANT_PEDS_TO_REACT)
								
								SET_CLIENT_BIT(eCLIENTBITSET_I_WANT_PEDS_TO_REACT)
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
		ELSE
			REPEAT GET_NUM_VARIATION_VEHS() iVeh
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVeh].netId)
				AND IS_ENTITY_ALIVE(NET_TO_VEH(serverBD.sVehicle[iVeh].netId))
					vCentreOfAction = GET_ENTITY_COORDS(NET_TO_VEH(serverBD.sVehicle[iVeh].netId))
					bReact = FALSE
				
					IF (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(localPlayerPed, vCentreOfAction) <= fShootingReactDist
					AND IS_PED_SHOOTING(localPlayerPed))
						bReact = TRUE
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_CLIENT_SHOULD_ALL_PEDS_REACT_CHECKS - I am shooting within ", fShootingReactDist, "m of non mission entity veh ", iVeh, ". Peds should react.")
					ENDIF
					
					IF IS_BULLET_IN_AREA(vCentreOfAction, 30.0)
						bReact = TRUE
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_CLIENT_SHOULD_ALL_PEDS_REACT_CHECKS - There is a bullet within 30m of non mission entity vehicle ", iVeh, ". Peds should react.")
					ENDIF
					
					IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, vCentreOfAction, 50.0)
						bReact = TRUE
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_CLIENT_SHOULD_ALL_PEDS_REACT_CHECKS - There is an explosion within 50m of non mission entity vehicle ", iVeh, ". Peds should react.")
					ENDIF
					
					IF bReact
						IF NOT GET_I_WANT_GROUP_TO_REACT(iVeh)
							
							SET_I_WANT_GROUP_TO_REACT(iVeh)
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
		
ENDPROC

FUNC INT GET_TOTAL_NUM_GROUND_FLARES_REQUIRED()
	
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_SELL_OFFSHORE_TRANSFER
		CASE MBV_SELL_FOLLOW_HELI
			RETURN 1
	ENDSWITCH
	
	RETURN 0
	
ENDFUNC

FUNC BOOL LOAD_INTERACT_PARTICLE_FX()
	REQUEST_PTFX_ASSET()

	IF HAS_PTFX_ASSET_LOADED()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEANUP_FLARE(INT iFlare)
	IF DOES_PARTICLE_FX_LOOPED_EXIST(sFlare[iFlare].ptfxID1)
		STOP_PARTICLE_FX_LOOPED(sFlare[iFlare].ptfxID1)
	ENDIF 
	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(sFlare[iFlare].ptfxID2)
		STOP_PARTICLE_FX_LOOPED(sFlare[iFlare].ptfxID2)
	ENDIF
	
	IF DOES_ENTITY_EXIST(sFlare[iFlare].flareObject)
		DELETE_OBJECT(sFlare[iFlare].flareObject)
	ENDIF
ENDPROC

PROC CLEANUP_FLARES()
	INT iFlare
	REPEAT GET_TOTAL_NUM_GROUND_FLARES_REQUIRED() iFlare
		CLEANUP_FLARE(iFlare)
	ENDREPEAT
ENDPROC

FUNC VECTOR GET_GROUND_FLARE_COORDS(INT iFlare)
	
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_SELL_OFFSHORE_TRANSFER 
			RETURN GET_MEGA_BUSINESS_DROP_OFF_COORDS(serverBD.sMissionEntity[MISSION_ENTITY_ONE].eDropOffs[iFlare], serverBD.nightclubID, GET_MEGA_BUSINESS_SUBVARIATION(), DEFAULT)
		CASE MBV_SELL_FOLLOW_HELI
			SWITCH GET_MEGA_BUSINESS_SUBVARIATION()
				CASE MBSV_SELL_FOLLOW_HELI_0
					RETURN <<659.5710, 3507.7419, 37.3490>>

				CASE MBSV_SELL_FOLLOW_HELI_1
					RETURN <<2285.8870, 4965.2349, 44.8490>>

				CASE MBSV_SELL_FOLLOW_HELI_2
					RETURN <<-19.6530, -2694.3379, 5.0940>>

			ENDSWITCH
		BREAK
			
	ENDSWITCH
	
	RETURN << 0.0, 0.0, 0.0 >>
	
ENDFUNC

FUNC BOOL SHOULD_PROCESS_GROUND_FLARES()
	
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_SELL_OFFSHORE_TRANSFER
		CASE MBV_SELL_FOLLOW_HELI
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_CREATE_FLARE(INT iFlare)
	UNUSED_PARAMETER(iFlare)

	IF IS_LOCAL_PLAYER_IN_SIMPLE_INTERIOR(GET_SIMPLE_INTERIOR_ID_FROM_NIGHTCLUB_ID(serverBD.nightclubID))
	OR IS_PLAYER_DOING_SIMPLE_INTERIOR_AUTOWARP(localPlayerId)
		PRINTLN("[FLARES] - SHOULD_CREATE_FLARE - flare ", iFlare," - flase - warping in or out of interior")
		RETURN FALSE
	ENDIF

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_SELL_OFFSHORE_TRANSFER
			IF GET_MODE_STATE() = eMODESTATE_COLLECT_ENTITY
			OR GET_MODE_STATE() = eMODESTATE_LEAVE_AREA
				RETURN FALSE
			ENDIF
		BREAK
		CASE MBV_SELL_FOLLOW_HELI
			IF IS_SERVER_BIT_SET(eSERVERBITSET_FOLLOW_HELI_FALL_BACK_TO_TRACKIFY)
				PRINTLN("[FLARES] - SHOULD_CREATE_FLARE - flare ", iFlare," - flase - trackify")
				RETURN FALSE
			ELSE
				IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_FINAL_CHECKPOINT_HAS_BEEN_REACHED)
					PRINTLN("[FLARES] - SHOULD_CREATE_FLARE - flare ", iFlare," - flase - final checkpoint not reached")
					RETURN FALSE
				ENDIF
			ENDIF
			IF HAS_MISSION_ENTITY_BEEN_DELIVERED_OR_DESTROYED(MISSION_ENTITY_ONE, TRUE)
				PRINTLN("[FLARES] - SHOULD_CREATE_FLARE - flare ", iFlare," - flase - mission entity delivered or destroyed")
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC FLOAT GET_GROUND_FLARE_SCALE(BOOL bSmokeFX)
	
	UNUSED_PARAMETER(bSmokeFX)
	
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_SELL_OFFSHORE_TRANSFER	RETURN 3.0
		CASE MBV_SELL_FOLLOW_HELI		RETURN 1.5
	ENDSWITCH
	
	RETURN 1.0
ENDFUNC

FUNC HUD_COLOURS GET_GROUND_FLARE_HUD_COLOUR()
	
	RETURN HUD_COLOUR_YELLOW
ENDFUNC

PROC MAINTAIN_GROUND_FLARE(INT iFlare, FLARE_STRUCT &sFlareData)
	
	IF IS_VECTOR_ZERO(GET_GROUND_FLARE_COORDS(iFlare))
		PRINTLN("[FLARES] - MAINTAIN_GROUND_FLARE - flare ", iFlare," - vector is zero")
		EXIT
	ENDIF
	
	IF SHOULD_CREATE_FLARE(iFlare)
	
		PRINTLN("[FLARES] - MAINTAIN_GROUND_FLARE - flare ", iFlare," - should be created")
	
		IF LOAD_INTERACT_PARTICLE_FX()
			PRINTLN("[FLARES] - MAINTAIN_GROUND_FLARE - flare ", iFlare," - loaded ptfx")
		
			INT iR, iG, iB, iA
			IF NOT DOES_ENTITY_EXIST(sFlareData.flareObject)
			OR NOT DOES_PARTICLE_FX_LOOPED_EXIST(sFlareData.ptfxID1)
			OR NOT DOES_PARTICLE_FX_LOOPED_EXIST(sFlareData.ptfxID2)
				VECTOR vFlareLocation = GET_GROUND_FLARE_COORDS(iFlare)
			
				IF NOT DOES_ENTITY_EXIST(sFlareData.flareObject)
					sFlareData.flareObject = CREATE_OBJECT_NO_OFFSET(PROP_FLARE_01, vFlareLocation, FALSE, FALSE)
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_GROUND_FLARES - Flare object created at ", vFlareLocation)
				ENDIF
				
				IF DOES_ENTITY_EXIST(sFlareData.flareObject)
			
					PRINTLN("[FLARES] - MAINTAIN_GROUND_FLARE - flare ", iFlare," - prop exists")
			
					IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(sFlareData.ptfxID1)
						sFlareData.ptfxID1 = START_PARTICLE_FX_LOOPED_AT_COORD("scr_ba_bb_flare", vFlareLocation, <<0.0, 0.0, 0.0>>,GET_GROUND_FLARE_SCALE(FALSE),DEFAULT,DEFAULT,DEFAULT,TRUE) 
						IF sFlareData.ptfxID1 != null
							PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_GROUND_FLARE - Flare fizzle effect started.")
						ENDIF
					ENDIF
					IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(sFlareData.ptfxID2)
						sFlareData.ptfxID2 = START_PARTICLE_FX_LOOPED_AT_COORD("scr_ba_bb_package_flare", vFlareLocation, <<0.0, 0.0, 0.0>>,GET_GROUND_FLARE_SCALE(TRUE),DEFAULT,DEFAULT,DEFAULT,TRUE)  
						IF sFlareData.ptfxID2 != null
							PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_GROUND_FLARE - Flare smoke effect started.")
						ENDIF
					ENDIF
					
					IF sFlareData.ptfxID2 != null
						IF DOES_PARTICLE_FX_LOOPED_EXIST(sFlareData.ptfxID2)
//							GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(GET_GROUND_FLARE_HUD_COLOUR()), iR, iG, iB, iA)
							GET_HUD_COLOUR(GET_GROUND_FLARE_HUD_COLOUR(), iR, iG, iB, iA)
							SET_PARTICLE_FX_LOOPED_COLOUR(sFlareData.ptfxID2, TO_FLOAT(iR)/255, TO_FLOAT(iG)/255, TO_FLOAT(iB)/255, TRUE)
						ENDIF
					ENDIF
				ELSE
					PRINTLN("[FLARES] - MAINTAIN_GROUND_FLARE - flare ", iFlare," - prop doesnt exist")

				ENDIF
			ELSE
				PRINTLN("[FLARES] - MAINTAIN_GROUND_FLARE - flare ", iFlare," - already exists")
			ENDIF
		ELSE
			PRINTLN("[FLARES] - MAINTAIN_GROUND_FLARE - flare ", iFlare," - not loaded ptfx")
			
		ENDIF

	ELSE
		PRINTLN("[FLARES] - MAINTAIN_GROUND_FLARE - flare ", iFlare," - should not be created")
		CLEANUP_FLARE(iFlare)
	ENDIF

ENDPROC

PROC MAINTAIN_GROUND_FLARES()
	
	IF NOT SHOULD_PROCESS_GROUND_FLARES()
		PRINTLN("[FLARES] - MAINTAIN_GROUND_FLARES - should not process")
		EXIT
	ENDIF
	
	INT iFlare
	
	REPEAT GET_TOTAL_NUM_GROUND_FLARES_REQUIRED() iFlare
		MAINTAIN_GROUND_FLARE(iFlare, sFlare[iFlare])
	ENDREPEAT
	
ENDPROC

FUNC BOOL REQUEST_LOAD_PTFX_ASSET()
	REQUEST_PTFX_ASSET()

	IF HAS_PTFX_ASSET_LOADED()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_PTFX()

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_FLYER_PROMOTION
		
		
			// Manage Leaflet dropping mechanic
			//---------------------------------------------------------------------------------
			
			IF REQUEST_LOAD_PTFX_ASSET()
				
				VEHICLE_INDEX vehPlane
				IF iLocalMissionEntityForPTFX != -1
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[iLocalMissionEntityForPTFX].netId)
						vehPlane = NET_TO_VEH(serverBD.sMissionEntity[iLocalMissionEntityForPTFX].netId)
					ENDIF
				
					// Must be in the plane and the plane must be in the air to start the drop
					IF DOES_ENTITY_EXIST(vehPlane)
					AND IS_ENTITY_IN_AIR(vehPlane)
					AND REQUEST_SCRIPT_AUDIO_BANK("PLANES")

						IF NOT bLeafletsDropping
						
							IF IS_LOCAL_BIT_SET(eLOCALBITSET_TRIGGER_LEAFLET_PTFX)
								// Cool down before you can use it again
								IF NOT HAS_NET_TIMER_STARTED(stLeafletDropTimer)
					
									IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxLeafletDrop)
										ptfxLeafletDrop		= START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ba_bb_leaflet_drop", vehPlane, <<0.0, -2.25, -0.1>>, <<0,0,0>>, 2.0)
										fLeafletCurrentEvo = 0.0
									ENDIF
									
									PLAY_SOUND_FROM_ENTITY(-1, "Flyer_Drop", vehPlane, "DLC_BTL_Flyer_Promotion_Sounds")
									PRINTLN("MAINTAIN_PTFX, Flyer_Drop, DLC_BTL_Flyer_Promotion_Sounds")
									
									START_NET_TIMER(stLeafletDropTimer)
									bLeafletsDropping = TRUE
									
									CLEAR_LOCAL_BIT(eLOCALBITSET_TRIGGER_LEAFLET_PTFX)
								ENDIF
							ENDIF
						
						// stop drop after 3 seoncs
						ELSE
							IF HAS_NET_TIMER_EXPIRED(stLeafletDropTimer, 3000)
								RESET_NET_TIMER(stLeafletDropTimer)
								bLeafletsDropping = FALSE
								iLocalMissionEntityForPTFX = -1
							ENDIF
						ENDIF
					
					ELSE
						IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxLeafletDrop)
							STOP_PARTICLE_FX_LOOPED(ptfxLeafletDrop)
							fLeafletCurrentEvo = 0.0
						ENDIF
					ENDIF
					
					// Manage leaflet ptfx evolution
					IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxLeafletDrop)
					
						FLOAT fIncrement
						IF bLeafletsDropping
							fIncrement = 1
						ELSE
							fIncrement = -1
						ENDIF
					
						fLeafletCurrentEvo += (fIncrement * TIMESTEP() * I_LEAFLET_DROP_RATE)
						fLeafletCurrentEvo = CLAMP(fLeafletCurrentEvo, 0.0, F_LEAFLET_DROP_RATE_MAX)
						SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxLeafletDrop, "spawn", fLeafletCurrentEvo)
					
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

// ********************************************************************

FUNC BOOL SHOULD_TRIGGER_GOTO_IN_RESTRICTED_AREA()

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_HOOKED		
			RETURN FALSE
		BREAK
	ENDSWITCH

	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_GIVE_WANTED_LEVEL_ON_PED_REACTION(INT iPed = -1)
	UNUSED_PARAMETER(iPed)
	
	IF IS_SERVER_BIT_SET(eSERVERBITSET_MAKE_ALL_PEDS_REACT)
		RETURN FALSE
	ENDIF
	
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_HOOKED
			IF iPed != -1
				RETURN IS_PED_A_COP(NET_TO_PED(serverBD.sPed[iPed].netId))
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_WANTED_LEVEL_DELAY_FOR_VARIATION()
	RETURN 0
ENDFUNC

FUNC INT GET_TIME_ALLOWED_IN_RESTRICTED_AREA()
	IF IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_AM_IN_RESTRICTED_AREA_AIR)
		RETURN 6000
	ENDIF

	RETURN 10000
ENDFUNC 

FUNC BOOL SHOULD_RESTRICTED_AREA_HAVE_TIMER()
	RETURN TRUE
ENDFUNC

FUNC INT GET_PED_TO_REACT_FOR_RESTRICTED_AREA_TIMER()
	RETURN -1
ENDFUNC

PROC MAINTAIN_RESTRICTED_AREA_TIMER()

	IF NOT SHOULD_RESTRICTED_AREA_HAVE_TIMER()
		EXIT
	ENDIF
	
	IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_MAKE_ALL_PEDS_REACT)
		IF IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_AM_IN_RESTRICTED_AREA)
		
//			// Trigger audio if neccessary
//			IF SHOULD_DO_RESTRICTED_AREA_PED_AUDIO()
//				PLAY_AMBIENT_SPEECH_FROM_CLOSEST_PED()
//			ENDIF

			// Trigger peds after time in area
			IF NOT HAS_NET_TIMER_STARTED(stTimeInRestrictedArea)
				START_NET_TIMER(stTimeInRestrictedArea)	
			ELIF HAS_NET_TIMER_EXPIRED(stTimeInRestrictedArea, GET_TIME_ALLOWED_IN_RESTRICTED_AREA())
				SET_I_WANT_PEDS_TO_REACT(GET_PED_TO_REACT_FOR_RESTRICTED_AREA_TIMER())
				IF SHOULD_GIVE_WANTED_LEVEL_ON_PED_REACTION()
					GIVE_WANTED_LEVEL_TO_GANG(GET_MAX_WANTED_LEVEL_FOR_VARIATION(), FALSE, GET_WANTED_LEVEL_DELAY_FOR_VARIATION())
				ENDIF
			ENDIF
		ELSE
			RESET_NET_TIMER(stTimeInRestrictedArea)
		ENDIF
	ENDIF
	
ENDPROC

CONST_INT ciNUM_WEAPON_GROUPS 4

SCRIPT_TIMER stSeenGunTimeout[ciNUM_WEAPON_GROUPS]

PROC RESET_WEAPON_OUT_TIMERS(INT iGroup)
	IF HAS_NET_TIMER_STARTED(stSeenGunTimeout[iGroup])
		RESET_NET_TIMER(stSeenGunTimeout[iGroup])
		PRINTLN("[MB_MISSION] HAS_WEAPON_OUT_FOR_TIME, RESET_NET_TIMER, iGroup = ", iGroup)
	ENDIF
ENDPROC

FUNC BOOL HAS_WEAPON_OUT_FOR_TIME(BOOL bInArea, INT iTime, INT iGroup)

	IF bInArea
	
		IF DOES_ENTITY_EXIST(localPlayerPed)
		AND NOT IS_ENTITY_DEAD(localPlayerPed)
//			IF NOT IS_PED_IN_ANY_VEHICLE(localPlayerPed)
//			OR GET_HEALTH_OF_MISSION_ENTITY(iGroup) < GET_MISSION_ENTITY_START_HEALTH(iGroup) // url:bugstar:4976041
		
				WEAPON_TYPE wtWeapon 

				GET_CURRENT_PED_WEAPON(localPlayerPed, wtWeapon)
				
				IF wtWeapon != WEAPONTYPE_UNARMED 
					
					IF NOT HAS_NET_TIMER_STARTED(stSeenGunTimeout[iGroup])
						PRINTLN("[MB_MISSION] HAS_WEAPON_OUT_FOR_TIME, START_NET_TIMER ", iGroup, " for ", GET_MEGA_BUSINESS_SUBVARIATION_NAME_FOR_DEBUG(serverBD.eMissionSubvariation))
						START_NET_TIMER(stSeenGunTimeout[iGroup])
					ELSE	
						IF HAS_NET_TIMER_EXPIRED(stSeenGunTimeout[iGroup], iTime)
						OR GET_HEALTH_OF_MISSION_ENTITY(iGroup) < GET_MISSION_ENTITY_START_HEALTH(iGroup) // url:bugstar:4976041
							
							PRINTLN("[MB_MISSION] HAS_WEAPON_OUT_FOR_TIME, RETURN TRUE ", iGroup, " for ", GET_MEGA_BUSINESS_SUBVARIATION_NAME_FOR_DEBUG(serverBD.eMissionSubvariation))
							
							RETURN TRUE
						ENDIF
					ENDIF
				ELSE
					RESET_WEAPON_OUT_TIMERS(iGroup)
				ENDIF
//			ENDIF
		ELSE
			RESET_WEAPON_OUT_TIMERS(iGroup)
		ENDIF
	ELSE
		RESET_WEAPON_OUT_TIMERS(iGroup)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT REACT_AREA_TIME()

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_HOOKED
		
			RETURN 500
	ENDSWITCH
	
	RETURN 0
ENDFUNC

SCRIPT_TIMER stReactTimer

PROC MAINTAIN_TRIGGER_AREAS()
	BOOL bPlayerInAirVeh = IS_PED_IN_ANY_HELI(localPlayerPed) OR IS_PED_IN_ANY_PLANE(localPlayerPed)
	
	//BOOL bPlayerInVehicle = IS_PED_IN_ANY_VEHICLE(localPlayerPed)
	BOOL bInReactArea, bInRestrictedArea, bInAirArea, bAmbushTrigger, bTriggerGoto, bInReactAreaTimed //, bDo3dCheck
//	VECTOR vTriggerSphereCentre
//	FLOAT fTriggerRange
	BOOL bGroupReact[4]
	
	IF NOT HAVE_ALL_MISSION_ENTITIES_BEEN_CREATED()
//	AND NOT ARE_MISSION_ENTITIES_MADE_DURING_MISSION()
		EXIT
	ENDIF
	
	PED_INDEX PedIndex = localPlayerPed
	WEAPON_TYPE ePedWeapon = WEAPONTYPE_UNARMED
	
//	IF SHOULD_VARIATION_USE_TRIGGER_SPHERE()
//		vTriggerSphereCentre = GET_TRIGGER_SPHERE_COORDS()
//		fTriggerRange = GET_TRIGGER_SPHERE_RANGE(bPlayerInAirVeh)
//		bDo3dCheck = SHOULD_DO_3D_CHECK_FOR_TRIGGER_SPHERE()
//		
//		bInReactArea = (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PedIndex, vTriggerSphereCentre, bDo3dCheck) <= fTriggerRange)
//	ELSE
	
		SWITCH GET_MEGA_BUSINESS_VARIATION()
		
			CASE MBV_MUSCLE_OUT
			
				SWITCH GET_MEGA_BUSINESS_SUBVARIATION()
				
					CASE MBSV_MUSCLE_OUT_0
						bGroupReact[0] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<456.320679,-1671.911011,27.134470>>, <<470.393372,-1683.715576,33.252129>>, 15.000000), 10000, 0)											
						bGroupReact[1] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<822.200562,-756.436523,24.727308>>, <<800.513184,-756.321167,29.284166>>, 20.000000), 10000, 1)	 												
						bGroupReact[2] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<726.160645,-2108.884033,27.255627>>, <<727.907959,-2090.735107,31.291618>>, 18.000000), 10000, 2)	
						bGroupReact[3] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<1161.572632,-1310.465576,33.648190>>, <<1160.097168,-1326.816406,38.742741>>, 15.000000), 10000, 3)
					BREAK
					CASE MBSV_MUSCLE_OUT_1
						bGroupReact[0] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<451.223938,-575.158325,26.499805>>, <<473.570770,-577.564392,30.499741>>, 14.000000), 10000, 0)										
						bGroupReact[1] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<436.677979,-1530.177612,27.285658>>, <<424.635498,-1498.012695,33.291603>>, 18.000000), 10000, 1)																		
						bGroupReact[2] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<715.112976,-899.454590,21.850060>>, <<715.599548,-922.772339,25.935968>>, 14.000000), 10000, 2)	
						bGroupReact[3] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<43.691990,-904.590942,28.058910>>, <<26.074541,-898.198364,34.043743>>, 18.000000), 10000, 3) 											
					BREAK
					CASE MBSV_MUSCLE_OUT_2
						bGroupReact[0] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<-313.022888,-1526.735352,21.593517>>, <<-322.680359,-1546.215210,32.769829>>, 14.000000), 10000, 0)											
						bGroupReact[1] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<153.966522,-1637.080322,27.291677>>, <<163.448029,-1653.196045,33.291676>>, 15.000000), 10000, 1)																		
						bGroupReact[2] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<-591.410522,-865.359619,19.762524>>, <<-591.043213,-887.427979,27.274740>>, 18.000000), 10000, 2)	
						bGroupReact[3] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<375.418274,-904.133789,27.483959>>, <<398.071411,-904.745544,33.417236>>, 14.000000), 10000, 3) 											
					BREAK
					CASE MBSV_MUSCLE_OUT_3
						bGroupReact[0] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<-42.660477,-214.096878,42.827015>>, <<-48.668785,-230.393173,48.601978>>, 13.000000), 10000, 0)												
						bGroupReact[1] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<-480.627014,271.899536,80.223366>>, <<-500.053741,274.163086,86.223495>>, 13.000000), 10000, 1)													
						bGroupReact[2] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<267.490326,316.755859,99.763557>>, <<287.213165,310.244629,110.078735>>, 15.000000), 10000, 2)		
						bGroupReact[3] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<908.246521,-187.240570,72.048660>>, <<887.125183,-174.536255,78.705299>>, 17.000000)	, 10000, 3)											
					BREAK
					CASE MBSV_MUSCLE_OUT_4
						bGroupReact[0] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<1098.131958,-2396.479004,27.748854>>, <<1073.169556,-2392.010010,33.636192>>, 25.000000), 10000, 0)									
						bGroupReact[1] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<202.609695,-2032.676392,15.286942>>, <<179.057098,-2028.259521,21.290798>>, 15.000000), 10000, 1)													
						bGroupReact[2] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<1147.233521,-1401.918335,31.812546>>, <<1147.332886,-1415.366333,37.623669>>, 20.000000), 10000, 2)	
						bGroupReact[3] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<332.240173,-1279.506592,29.779259>>, <<319.542267,-1295.707886,35.428993>>, 17.000000), 10000, 3)												
					BREAK
					CASE MBSV_MUSCLE_OUT_5
						bGroupReact[0] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<-1654.619873,-980.293396,5.447124>>, <<-1667.170044,-969.422913,10.577587>>, 25.000000), 10000, 0)													
						bGroupReact[1] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<-705.181213,-886.868530,21.052082>>, <<-705.024963,-866.646851,26.437601>>, 20.000000), 10000, 1)													
						bGroupReact[2] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<-517.260986,-47.433105,38.249798>>, <<-493.985321,-57.607639,42.992233>>, 15.000000)	, 10000, 2)	 											
						bGroupReact[3] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<-2974.460205,70.893616,9.608498>>, <<-2954.218506,61.149582,14.608500>>, 13.000000), 10000, 3)												
					BREAK
					CASE MBSV_MUSCLE_OUT_6
						bGroupReact[0] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<-915.871582,-2037.638062,7.002050>>, <<-903.305237,-2049.824951,12.299147>>, 18.000000), 10000, 0)																					
						bGroupReact[1] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<218.626389,-1988.005493,16.866890>>, <<232.490097,-1999.984619,22.484451>>, 21.000000), 10000, 1)																								
						bGroupReact[2] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<-1784.005127,-2773.224854,11.010372>>, <<-1763.953125,-2784.692383,16.944700>>, 35.000000), 10000, 2)												
						bGroupReact[3] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<-238.212830,-2575.599365,4.071543>>, <<-238.268188,-2561.911133,9.001398>>, 17.000000), 10000, 3)												
					BREAK
					CASE MBSV_MUSCLE_OUT_7					
						bGroupReact[0] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<1104.299805,-322.252319,65.163811>>, <<1112.933838,-335.643341,70.094872>>, 10.000000),  10000, 0)																														
						bGroupReact[1] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<-248.354523,293.382751,86.762489>>, <<-248.261871,273.844543,95.807014>>, 27.000000), 10000, 1)																								
						bGroupReact[2] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<108.046539,-142.435837,52.749031>>, <<102.806839,-156.714706,58.881775>>, 21.000000), 10000, 2)	
						bGroupReact[3] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<450.786438,-929.827454,26.512974>>, <<450.582184,-948.548828,31.515972>>, 14.000000)	, 10000, 3)											
					BREAK
					CASE MBSV_MUSCLE_OUT_8
						bGroupReact[0] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<-49.166470,-2532.580811,3.009999>>, <<-35.895866,-2513.142822,9.010000>>, 20.000000), 10000, 0)																											
						bGroupReact[1] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<736.255798,-1920.512817,27.292046>>, <<738.646973,-1895.364746,33.800232>>, 15.000000), 10000, 0)																																					
						bGroupReact[2] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<814.299866,-2398.620361,20.821712>>, <<812.947510,-2413.493164,26.698397>>, 18.000000), 10000, 2)												
						bGroupReact[3] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<1207.139160,-3206.176270,2.951578>>, <<1207.222656,-3177.923096,8.590716>>, 18.000000), 10000, 3)												
					BREAK
					CASE MBSV_MUSCLE_OUT_9
						bGroupReact[0] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<-1449.650513,-692.674500,23.312798>>, <<-1466.480957,-678.625916,29.664948>>, 10.000000), 10000, 0)																							
						bGroupReact[1] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<-326.236450,-1297.573608,29.521027>>, <<-340.184937,-1297.505981,34.399094>>, 15.000000), 10000, 1)																											
						bGroupReact[2] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<-1027.642456,-1523.695557,2.584244>>, <<-1014.219666,-1514.143921,9.932402>>, 15.000000), 10000, 2)	
						bGroupReact[3] = HAS_WEAPON_OUT_FOR_TIME(IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<-429.571808,-464.860168,31.362915>>, <<-427.104340,-451.351807,35.283356>>, 22.000000), 10000, 3)												
					BREAK
				ENDSWITCH
			BREAK
			CASE MBV_STOLEN_SUPPLIES
				SWITCH GET_MEGA_BUSINESS_SUBVARIATION()
					CASE MBSV_STOLEN_SUPPLIES_0
						bInReactArea = IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<41.554001,3670.532715,36.640919>>, <<31.512444,3652.667480,43.755001>>, 15.000000)
										OR IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<38.160076,3655.385986,36.741539>>, <<48.673866,3661.177734,43.775627>>, 8.000000)		
										OR IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<53.366749,3685.720459,37.755001>>, <<35.829166,3673.976807,48.543457>>, 15.000000)									
										OR IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<56.103500,3672.153564,37.727226>>, <<55.804169,3658.387207,48.702187>>, 9.000000)									

						bInAirArea = (bPlayerInAirVeh AND (IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<27.099648,3642.638428,37.808746>>, <<57.661274,3686.680664,78.754997>>, 50.000000)))							

					BREAK
					CASE MBSV_STOLEN_SUPPLIES_1
						bInReactArea = IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<-612.849548,-1066.370850,20.178352>>, <<-612.187988,-1047.068848,28.422077>>, 25.000000)				
										OR IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<-621.466248,-1064.839600,19.957561>>, <<-614.894958,-1049.781250,30.787540>>, 8.000000)	
										OR IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<-613.196350,-1059.857544,19.787540>>, <<-612.974365,-1073.818237,26.378517>>, 18.000000)												


						bInAirArea = (bPlayerInAirVeh AND (IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<-612.370972,-1087.563110,20.329254>>, <<-612.665649,-1028.534912,70.787537>>, 50.000000)))
					BREAK
					CASE MBSV_STOLEN_SUPPLIES_2
						bInReactArea = IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<215.664520,-2018.431763,16.740129>>, <<200.320724,-2004.613647,20.661566>>, 17.000000)		
										OR IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<224.423645,-2015.735718,16.859116>>, <<210.274399,-2026.490845,27.267525>>, 7.000000)												


						bInAirArea = (bPlayerInAirVeh AND (IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<225.852310,-2027.037598,16.075081>>, <<189.051315,-1995.476929,57.482018>>, 50.000000)))
					BREAK
				ENDSWITCH
			BREAK
			
			CASE MBV_VIP_RESCUE
				SWITCH GET_MEGA_BUSINESS_SUBVARIATION()
					CASE MBSV_VIP_RESCUE_0
						IF (bPlayerInAirVeh AND (IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<-3023.942139,322.028687,7.803196>>, <<-3005.468994,406.947235,53.925304>>, 80.000000)))
							bInReactArea = TRUE
						ENDIF
						IF (!bPlayerInAirVeh AND IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<-3014.043457,362.741058,12.746187>>, <<-3016.931641,348.967346,18.629059>>, 10.000000))
							GET_CURRENT_PED_WEAPON(PedIndex, ePedWeapon)
							IF ePedWeapon != WEAPONTYPE_UNARMED
								bInReactArea = TRUE
							ENDIF
						ENDIF
						IF IS_ENTITY_AT_COORD(PedIndex, <<-3015.237061,359.497284,14.582359>>,<<1.000000,1.000000,1.000000>>)
							bInReactArea = TRUE
						ENDIF
						IF IS_ENTITY_AT_COORD(PedIndex, <<-3013.626953,356.666565,14.193412>>,<<1.000000,1.000000,1.500000>>)
							bInReactArea = TRUE
						ENDIF
					BREAK
					CASE MBSV_VIP_RESCUE_1
						IF (bPlayerInAirVeh AND (IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<-1236.051025,444.672852,88.701981>>, <<-1243.129883,524.904663,134.491760>>, 60.000000)))
							bInReactArea = TRUE
						ENDIF
						IF (!bPlayerInAirVeh AND IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<-1238.758911,492.946289,90.556900>>, <<-1236.380981,475.240204,96.721703>>, 12.000000))
							GET_CURRENT_PED_WEAPON(PedIndex, ePedWeapon)
							IF ePedWeapon != WEAPONTYPE_UNARMED
								bInReactArea = TRUE
							ENDIF
						ENDIF
						IF IS_ENTITY_AT_COORD(PedIndex, <<-1238.071167,477.583344,92.637360>>,<<1.000000,1.000000,1.000000>>)
							bInReactArea = TRUE
						ENDIF
						IF IS_ENTITY_AT_COORD(PedIndex, <<-1237.776611,489.481232,92.854805>>,<<1.000000,1.000000,1.500000>>)
							bInReactArea = TRUE
						ENDIF
					BREAK
					CASE MBSV_VIP_RESCUE_2
						IF (bPlayerInAirVeh AND (IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<332.853424,-1940.534424,21.633427>>, <<408.053833,-1982.791504,62.338612>>, 60.000000)))
							bInReactArea = TRUE
						ENDIF
						IF (!bPlayerInAirVeh AND IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<369.162964,-1970.530884,21.298038>>, <<358.026276,-1960.955078,28.475283>>, 12.000000))
							GET_CURRENT_PED_WEAPON(PedIndex, ePedWeapon)
							IF ePedWeapon != WEAPONTYPE_UNARMED
								bInReactArea = TRUE
							ENDIF
						ENDIF
						IF IS_ENTITY_AT_COORD(PedIndex, <<365.956543,-1968.150635,23.883610>>,<<1.000000,1.000000,1.500000>>)
							bInReactArea = TRUE
						ENDIF
						IF IS_ENTITY_AT_COORD(PedIndex, <<361.235504,-1965.025879,24.001471>>,<<1.000000,1.000000,1.500000>>)
							bInReactArea = TRUE
						ENDIF
					BREAK
					CASE MBSV_VIP_RESCUE_3
						IF (bPlayerInAirVeh AND (IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<705.464478,-314.357788,53.248341>>, <<695.032776,-263.556885,97.670929>>, 60.000000)))
							bInReactArea = TRUE
						ENDIF
						IF (!bPlayerInAirVeh AND IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<696.565247,-296.168854,57.268436>>, <<712.258057,-293.131775,62.978470>>, 12.000000))
							GET_CURRENT_PED_WEAPON(PedIndex, ePedWeapon)
							IF ePedWeapon != WEAPONTYPE_UNARMED
								bInReactArea = TRUE
							ENDIF
						ENDIF
						IF IS_ENTITY_AT_COORD(PedIndex, <<708.197510,-290.358276,58.687798>>,<<1.000000,1.000000,1.500000>>)
							bInReactArea = TRUE
						ENDIF
						IF IS_ENTITY_AT_COORD(PedIndex, <<699.492554,-299.395508,58.682205>>,<<1.000000,1.000000,1.500000>>)
							bInReactArea = TRUE
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			
			CASE MBV_SELL_ROADBLOCK
				SWITCH GET_MEGA_BUSINESS_SUBVARIATION()
					CASE MBSV_SELL_ROADBLOCK_0
						bInReactArea = IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<-195.328537,6543.193359,0.097816>>, <<-345.483459,6413.529785,52.451607>>, 100.000)	
					BREAK
					CASE MBSV_SELL_ROADBLOCK_1
						bInReactArea = IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<-366.188690,-2515.246094,3.000793>>, <<-248.481339,-2611.145264,46.722317>>, 100.000)	
					BREAK
					CASE MBSV_SELL_ROADBLOCK_2
						bInReactArea = IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<2711.945313,4126.461914,40.881683>>, <<2676.390625,4314.881836,84.836349>>, 100.000)	
					BREAK
				ENDSWITCH
			BREAK
			CASE MBV_SELL_UNDERCOVER_COPS
				IF IS_SERVER_BIT_SET(eSERVERBITSET_GIVEN_WANTED_RATING)
					bInReactArea = TRUE
				ENDIF
			BREAK
			CASE MBV_COLLECT_DJ_HOOKED
				// For initial helipad meet
				IF serverBD.iCurrentGotoLocation = INVALID_GOTO_LOCATION
					bTriggerGoto = IS_PLAYER_IN_POSSESSION_OF_A_MISSION_ENTITY(localPlayerId) AND IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<-703.832214,-1421.705688,3.000523>>, <<-758.570801,-1487.371460,9.000523>>, 80.000000)	
				ENDIF
				
				// Cops get spooked when cargobob flies about in these areas for too long
				SWITCH GET_MEGA_BUSINESS_SUBVARIATION()
					CASE MBSV_COLLECT_HOOKED_0
						bInReactAreaTimed = 						bPlayerInAirVeh AND IS_ENTITY_AT_COORD(PedIndex, <<1246.170288,-3104.710693,19.028435>>,<<25.000000,25.000000,15.000000>>)	
					BREAK									
					CASE MBSV_COLLECT_HOOKED_1				
						bInReactAreaTimed = 						bPlayerInAirVeh AND IS_ENTITY_AT_COORD(PedIndex, <<1174.919312,-368.304169,80.286041>>,<<25.000000,25.000000,15.000000>>)
					BREAK									
					CASE MBSV_COLLECT_HOOKED_2				
						bInReactAreaTimed = 						bPlayerInAirVeh AND IS_ENTITY_AT_COORD(PedIndex, <<1015.391479,-2460.727295,40.731480>>,<<25.000000,25.000000,15.000000>>)
					BREAK									
					CASE MBSV_COLLECT_HOOKED_3				
						bInReactAreaTimed = 						bPlayerInAirVeh AND IS_ENTITY_AT_COORD(PedIndex, <<-1449.441162,-884.429993,22.592133>>,<<25.000000,25.000000,15.000000>>)	
					BREAK									
					CASE MBSV_COLLECT_HOOKED_4				
						bInReactAreaTimed = 						bPlayerInAirVeh AND IS_ENTITY_AT_COORD(PedIndex, <<364.843231,-841.253845,41.136738>>,<<25.000000,25.000000,15.000000>>)	
					BREAK									
					CASE MBSV_COLLECT_HOOKED_5				
						bInReactAreaTimed = 						bPlayerInAirVeh AND IS_ENTITY_AT_COORD(PedIndex, <<-439.275970,121.836563,76.639725>>,<<25.000000,25.000000,15.000000>>)
					BREAK									
					CASE MBSV_COLLECT_HOOKED_6				
						bInReactAreaTimed = 						bPlayerInAirVeh AND IS_ENTITY_AT_COORD(PedIndex, <<1080.882813,-1816.057495,49.806896>>,<<25.000000,25.000000,15.000000>>)								
					BREAK									
					CASE MBSV_COLLECT_HOOKED_7				
						bInReactAreaTimed = 						bPlayerInAirVeh AND IS_ENTITY_AT_COORD(PedIndex,<<-1199.201782,-409.200897,46.881691>>,<<25.000000,25.000000,15.000000>>)		 
					BREAK									
					CASE MBSV_COLLECT_HOOKED_8				
						bInReactAreaTimed = 						bPlayerInAirVeh AND IS_ENTITY_AT_COORD(PedIndex, <<1184.064331,-3331.299561,18.528767>>,<<25.000000,25.000000,15.000000>>)									
					BREAK								
					CASE MBSV_COLLECT_HOOKED_9				
						bInReactAreaTimed = 						bPlayerInAirVeh AND IS_ENTITY_AT_COORD(PedIndex, <<-1553.348877,-475.691101,48.449165>>,<<25.000000,25.000000,15.000000>>)								
					BREAK					
				ENDSWITCH
				
				IF bInReactAreaTimed
				AND IS_SERVER_BIT_SET(eSERVERBITSET_MADONNA_SCENE_COMPLETE)
					IF NOT HAS_NET_TIMER_STARTED(stReactTimer)
						START_NET_TIMER(stReactTimer)
					ELSE
						bInReactArea = HAS_NET_TIMER_EXPIRED(stReactTimer, REACT_AREA_TIME())
					ENDIF
				ELSE
					IF HAS_NET_TIMER_STARTED(stReactTimer)
						RESET_NET_TIMER(stReactTimer)
					ENDIF
				ENDIF
				
			BREAK
		ENDSWITCH
		
//	ENDIF
	
	IF NOT bInRestrictedArea
	AND bInAirArea
		bInRestrictedArea = bInAirArea
	ENDIF
	
	IF bTriggerGoto
		SET_CLIENT_BIT(eCLIENTBITSET_I_HAVE_REACHED_GOTO_LOCATION)	
	ENDIF
	
	IF bGroupReact[0]
		SET_I_WANT_GROUP_TO_REACT(0)
		SET_CLIENT_BIT(eCLIENTBITSET_I_AM_IN_RESTRICTED_AREA)
		
		IF SHOULD_TRIGGER_GOTO_IN_RESTRICTED_AREA()
			SET_CLIENT_BIT(eCLIENTBITSET_I_HAVE_REACHED_GOTO_LOCATION)		
		ENDIF
		IF SHOULD_GIVE_WANTED_LEVEL_ON_PED_REACTION()
			GIVE_WANTED_LEVEL_TO_GANG(GET_MAX_WANTED_LEVEL_FOR_VARIATION(), FALSE, GET_WANTED_LEVEL_DELAY_FOR_VARIATION())
		ENDIF
	ENDIF
	
	IF bGroupReact[1]
		SET_I_WANT_GROUP_TO_REACT(1)
		SET_CLIENT_BIT(eCLIENTBITSET_I_AM_IN_RESTRICTED_AREA)
		
		IF SHOULD_TRIGGER_GOTO_IN_RESTRICTED_AREA()
			SET_CLIENT_BIT(eCLIENTBITSET_I_HAVE_REACHED_GOTO_LOCATION)		
		ENDIF
		IF SHOULD_GIVE_WANTED_LEVEL_ON_PED_REACTION()
			GIVE_WANTED_LEVEL_TO_GANG(GET_MAX_WANTED_LEVEL_FOR_VARIATION(), FALSE, GET_WANTED_LEVEL_DELAY_FOR_VARIATION())
		ENDIF
	ENDIF
	
	IF bGroupReact[2]
		SET_I_WANT_GROUP_TO_REACT(2)
		SET_CLIENT_BIT(eCLIENTBITSET_I_AM_IN_RESTRICTED_AREA)
		
		IF SHOULD_TRIGGER_GOTO_IN_RESTRICTED_AREA()
			SET_CLIENT_BIT(eCLIENTBITSET_I_HAVE_REACHED_GOTO_LOCATION)		
		ENDIF
		IF SHOULD_GIVE_WANTED_LEVEL_ON_PED_REACTION()
			GIVE_WANTED_LEVEL_TO_GANG(GET_MAX_WANTED_LEVEL_FOR_VARIATION(), FALSE, GET_WANTED_LEVEL_DELAY_FOR_VARIATION())
		ENDIF
	ENDIF
	
	IF bAmbushTrigger
		SET_CLIENT_BIT(eCLIENTBITSET_TRIGGER_AMBUSH)
	ENDIF

	IF bInReactArea
	
		SET_I_WANT_PEDS_TO_REACT(-1)
		SET_CLIENT_BIT(eCLIENTBITSET_I_AM_IN_RESTRICTED_AREA)
		
		IF SHOULD_TRIGGER_GOTO_IN_RESTRICTED_AREA()
			SET_CLIENT_BIT(eCLIENTBITSET_I_HAVE_REACHED_GOTO_LOCATION)		
		ENDIF
		IF SHOULD_GIVE_WANTED_LEVEL_ON_PED_REACTION()
			GIVE_WANTED_LEVEL_TO_GANG(GET_MAX_WANTED_LEVEL_FOR_VARIATION(), FALSE, GET_WANTED_LEVEL_DELAY_FOR_VARIATION())
		ENDIF
	
	ELIF bInRestrictedArea
		SET_CLIENT_BIT(eCLIENTBITSET_I_AM_IN_RESTRICTED_AREA)
		IF SHOULD_TRIGGER_GOTO_IN_RESTRICTED_AREA()
			SET_CLIENT_BIT(eCLIENTBITSET_I_HAVE_REACHED_GOTO_LOCATION)	
		ENDIF
		
		IF bInAirArea
			SET_CLIENT_BIT(eCLIENTBITSET_I_AM_IN_RESTRICTED_AREA_AIR)
		ELSE
			CLEAR_CLIENT_BIT(eCLIENTBITSET_I_AM_IN_RESTRICTED_AREA_AIR)
		ENDIF
	ELSE
		CLEAR_CLIENT_BIT(eCLIENTBITSET_I_AM_IN_RESTRICTED_AREA)
		CLEAR_CLIENT_BIT(eCLIENTBITSET_I_AM_IN_RESTRICTED_AREA_AIR)
	ENDIF
	
ENDPROC

FUNC eLOCAL_BITSET GET_LOCAL_NEAR_CAR_BIT()

	SWITCH iNearestMissionEntity
		CASE 0		RETURN eLOCALBITSET_MUSCLE_OUT_EQUIP_BAT_NEAR_CAR_0
		CASE 1		RETURN eLOCALBITSET_MUSCLE_OUT_EQUIP_BAT_NEAR_CAR_1
		CASE 2		RETURN eLOCALBITSET_MUSCLE_OUT_EQUIP_BAT_NEAR_CAR_2
	ENDSWITCH

	RETURN eLOCALBITSET_END
ENDFUNC

SCRIPT_TIMER stInCargobobCallDelay

PROC MAINTAIN_MISSION_SPECIFIC_LOGIC_CLIENT()
	VEHICLE_INDEX vehID

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_EQUIPMENT
			IF NETWORK_IS_GAME_IN_PROGRESS()
				IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_WITHIN_RANGE_OF_MISSION_ENTITY) 
					IF NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_WITHIN_RANGE_OF_MISSION_ENTITY)
						IF IS_MISSION_ENTITY_BIT_SET(MISSION_ENTITY_ONE, eMISSIONENTITYBITSET_CREATED)
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
								IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(localPlayerId), GET_MISSION_ENTITY_COORDS(MISSION_ENTITY_ONE), FALSE) <= 150.0
									PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_MISSION_SPECIFIC_LOGIC_CLIENT - client close to MISSION_ENTITY_ONE ")
									SET_CLIENT_BIT(eCLIENTBITSET_WITHIN_RANGE_OF_MISSION_ENTITY)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					// Lightbar
					IF IS_MISSION_ENTITY_BIT_SET(MISSION_ENTITY_ONE, eMISSIONENTITYBITSET_CREATED)
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
							IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(localPlayerId), GET_MISSION_ENTITY_COORDS(MISSION_ENTITY_ONE), FALSE) <= 150.0
								PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_MISSION_SPECIFIC_LOGIC_CLIENT - client close to MISSION_ENTITY_ONE ")
								MAINTAIN_PARTY_BUS2_SCALEFORM()
							ELSE
								CLEANUP_PARTY_BUS2_SCALEFORM()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE MBV_SELL_ROADBLOCK
			IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_ROADBLOCK_FLAG)
				IF NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_ROADBLOCK_FLAG)
					
					IF fMyDistanceFromNearestDropOff > 0
					AND fMyDistanceFromNearestDropOff <= (150.00)
					
						PRINTLN("[MB_MISSION]  MAINTAIN_MISSION_SPECIFIC_LOGIC_CLIENT,eCLIENTBITSET_ROADBLOCK_FLAG ")
						SET_CLIENT_BIT(eCLIENTBITSET_ROADBLOCK_FLAG)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE MBV_COLLECT_DJ_CRASH
			IF GET_MODE_STATE() = eMODESTATE_FOLLOW_ENTITY
			OR GET_MODE_STATE() = eMODESTATE_DELIVER_ENTITY
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[1].netId)
				AND NOT IS_ENTITY_DEAD(NET_TO_ENT(serverBD.sPed[1].netId))
				AND NOT IS_ENTITY_VISIBLE(NET_TO_ENT(serverBD.sPed[1].netId))
					IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[1].netId)
						SET_ENTITY_VISIBLE(NET_TO_ENT(serverBD.sPed[1].netId), TRUE)
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_MISSION_SPECIFIC_LOGIC_CLIENT - Dave was invisible while following. Set to visible.")
					ENDIF
				ENDIF
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[2].netId)
				AND NOT IS_ENTITY_DEAD(NET_TO_ENT(serverBD.sPed[2].netId))
				AND NOT IS_ENTITY_VISIBLE(NET_TO_ENT(serverBD.sPed[2].netId))
					IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[2].netId)
						SET_ENTITY_VISIBLE(NET_TO_ENT(serverBD.sPed[2].netId), TRUE)
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_MISSION_SPECIFIC_LOGIC_CLIENT - Solomun was invisible while following. Set to visible.")
					ENDIF
				ENDIF
			ENDIF
					
			IF GET_MODE_STATE() = eMODESTATE_FOLLOW_ENTITY
				//IF NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_NEAR_LANDED_AIRCRAFT)

					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[0].netId)
					AND NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.sVehicle[0].netId))
						vehId = NET_TO_VEH(serverBD.sVehicle[0].netId)
						
						IF GET_PED_STATE(0) = ePEDSTATE_LAND_AIRCRAFT
							IF IS_ENTITY_IN_ANGLED_AREA(vehId, <<1165.743530,3158.519775,35.679779>>, <<1319.363403,3121.822754,153.907806>>, 197.750000)
							OR HAS_NET_TIMER_EXPIRED(serverBD.stLandAircraftFailsafe, 90000)
								IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[0].netId)
									CONTROL_LANDING_GEAR(vehId, LGC_DEPLOY_INSTANT)
									SET_VEHICLE_ENGINE_ON(vehId, FALSE, TRUE)	
									
									IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[0].netId)
									AND NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.sPed[0].netId))
										CLEAR_PED_TASKS(NET_TO_PED(serverBD.sPed[0].netId))
									ENDIF
									
									#IF IS_DEBUG_BUILD
									IF HAS_NET_TIMER_EXPIRED(serverBD.stLandAircraftFailsafe, 90000)
										PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_MISSION_SPECIFIC_LOGIC_CLIENT - Killed plane engine and pilot tasks as failsafe timer has expired.")
									ELIF IS_ENTITY_IN_ANGLED_AREA(vehId, <<1165.743530,3158.519775,35.679779>>, <<1319.363403,3121.822754,153.907806>>, 197.750000)
										PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_MISSION_SPECIFIC_LOGIC_CLIENT - Killed plane engine and pilot tasks as plane has overshot.")
									ENDIF
									#ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF IS_SECOND_MID_MISSION_MOCAP_ABOUT_TO_BEGIN()
							
							IF NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_FROZEN_LANDED_AIRCRAFT)

								IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[0].netId)
									SET_VEHICLE_ON_GROUND_PROPERLY(vehId)
									FREEZE_ENTITY_POSITION(vehId, TRUE)
									SET_VEHICLE_ENGINE_ON(vehId, FALSE, TRUE)
									
									SET_CLIENT_BIT(eCLIENTBITSET_FROZEN_LANDED_AIRCRAFT)
								ENDIF
	
								IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[0].netId)
								AND NOT IS_ENTITY_DEAD(NET_TO_ENT(serverBD.sPed[0].netId))
									IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[0].netId)
										SET_ENTITY_HEALTH(NET_TO_ENT(serverBD.sPed[0].netId), 0)
									ENDIF
								ENDIF
							ENDIF
							
							IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_REMOVE_LAND_AIRCRAFT_BLIP)
								REMOVE_VEHICLE_BLIP(0)
								SET_LOCAL_BIT(eLOCALBITSET_REMOVE_LAND_AIRCRAFT_BLIP)
							ENDIF
							
							IF IS_ENTITY_IN_RANGE_ENTITY(localPlayerPed, vehId, 50.0, FALSE)
								SET_CLIENT_BIT(eCLIENTBITSET_NEAR_LANDED_AIRCRAFT)
							ELSE
								CLEAR_CLIENT_BIT(eCLIENTBITSET_NEAR_LANDED_AIRCRAFT)
							ENDIF
						ENDIF
					ENDIF
				//ENDIF
			ELIF GET_MODE_STATE() = eMODESTATE_MID_MISSION_MOCAP
			AND IS_SECOND_MID_MISSION_MOCAP_ABOUT_TO_BEGIN()
				
				IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
					SET_SERVER_BIT(eSERVERBITSET_SAFE_TO_DELIVER)
				ENDIF
				
				IF NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_WARPED_SOLOMUN_PLANE)
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[0].netId)
						vehID = NET_TO_VEH(serverBD.sVehicle[0].netId)
						IF NOT IS_ENTITY_DEAD(vehID)
							IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[0].netId)
								IF sMocap.iStage = 3
									IF IS_SCREEN_FADED_OUT()
									AND NOT IS_ENTITY_IN_RANGE_COORDS(vehID, <<1334.8727, 3119.3840, 39.9832>>, 5.0)
										NETWORK_FADE_IN_ENTITY(vehID, TRUE)
										SET_ENTITY_COORDS(vehID, <<1334.8727, 3119.3840, 39.9832>>)
										SET_ENTITY_HEADING(vehID, 88.0974)
										SET_CLIENT_BIT(eCLIENTBITSET_WARPED_SOLOMUN_PLANE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF	
			ENDIF
		BREAK
		
		CASE MBV_COLLECT_DJ_HOOKED
		
			IF GET_MODE_STATE() = eMODESTATE_DELIVER_ENTITY
			
				INT iMadonna
				iMadonna = HOOKED_PED_ARRAY_INDEX(HOOKED_PED_MADONNA)
			
				IF iMadonna != -1
				AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[iMadonna].netId)
				AND NOT IS_ENTITY_DEAD(NET_TO_ENT(serverBD.sPed[iMadonna].netId))
				AND NOT IS_ENTITY_VISIBLE(NET_TO_ENT(serverBD.sPed[iMadonna].netId))
					IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iMadonna].netId)
						SET_ENTITY_VISIBLE(NET_TO_ENT(serverBD.sPed[iMadonna].netId), TRUE)
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_MISSION_SPECIFIC_LOGIC_CLIENT - iMadonna was invisible while following. Set to visible.", iMadonna)
					ENDIF
				ENDIF
				
				INT iDave
				iDave = HOOKED_PED_ARRAY_INDEX(HOOKED_PED_DAVE)
				
				IF iDave != -1				
				AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[iDave].netId)
				AND NOT IS_ENTITY_DEAD(NET_TO_ENT(serverBD.sPed[iDave].netId))
				AND NOT IS_ENTITY_VISIBLE(NET_TO_ENT(serverBD.sPed[iDave].netId))
					IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iDave].netId)
						SET_ENTITY_VISIBLE(NET_TO_ENT(serverBD.sPed[iDave].netId), TRUE)
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_MISSION_SPECIFIC_LOGIC_CLIENT - iDave was invisible while following. Set to visible.", iDave)
					ENDIF
				ENDIF
				
			ENDIF
		
//			FLOAT fDist
//			fDist =	GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(localPlayerPed, GET_MEGA_BUSINESS_DROP_OFF_COORDS(serverBD.sMissionEntity[MISSION_ENTITY_ONE].eDropOffs[0], 
//																		serverBD.nightclubID, GET_MEGA_BUSINESS_SUBVARIATION(), GET_RANDOM_MODE_VARIANT_FOR_DROP_OFF_LOOKUP()))
																		
//			PRINTLN("MBV_COLLECT_DJ_HOOKED, fDist = ", fDist)
		
			CALCULATE_DIST_BETWEEN_CARGOBOB_AND_MADONNA()
			
			// url:bugstar:4823554 deal with scene being messed with early so that TBM is in the car at least
			IF NOT HOOKED_ARREST_HAS_STARTED()
			AND NOT HOOKED_MADONNA_SCENE_FINISHED() 
				IF NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_ARREST_TRIGGERED_EARLY)
					VECTOR vPos 
					vPos = GET_MEGA_BUSINESS_VEHICLE_SPAWN_COORDS(ciHOOKED_VEH_POLICE3, GET_MEGA_BUSINESS_VARIATION(), GET_MEGA_BUSINESS_SUBVARIATION(), GET_RANDOM_MODE_VARIANT_FOR_ENTITY_LOOKUP())
					IF IS_ENTITY_AT_COORD(localPlayerPed, vPos, <<45.00, 45.00, 10.00>>, TRUE)
					OR IS_SERVER_BIT_SET(eSERVERBITSET_MAKE_ALL_PEDS_REACT)
						SET_CLIENT_BIT(eCLIENTBITSET_ARREST_TRIGGERED_EARLY)
					ENDIF
				ENDIF
			ENDIF
			
			IF HOOKED_CARGOBOB_ALLOWED_TO_UNHOOK_COP_CAR()		
			
				// Radius blip for cargo drop
				IF NOT DOES_BLIP_EXIST(localCargoDropRadiusBlip)
					localCargoDropRadiusBlip = ADD_BLIP_FOR_RADIUS(GET_MEGA_BUSINESS_GOTO_COORDS(GET_MEGA_BUSINESS_VARIATION(), GET_MEGA_BUSINESS_SUBVARIATION(), serverBD.nightclubID, GET_EXTRA_INT_PARAM_FOR_GOTO_COORDS_LOOKUP(), serverBD.iRandomVariant), 
																	cfDROP_ZONE_SIZE)
					SET_BLIP_COLOUR_FROM_HUD_COLOUR(localCargoDropRadiusBlip, HUD_COLOUR_YELLOW)
					SET_BLIP_AS_SHORT_RANGE(localCargoDropRadiusBlip,TRUE)
					SET_BLIP_ALPHA(localCargoDropRadiusBlip, 80)
					PRINTLN("[MB_MISSION] ADD_SEARCH_AREA_BLIP added. ")
				ENDIF
				
				// Auto detach cop car if flying too low
				VEHICLE_INDEX vCopCar, vBob
				FLOAT fHeight
				
				IF IS_PLAYER_PILOT_OF_A_CARGOBOB(localPlayerId)
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[ciHOOKED_VEH_POLICE3].netId)
					AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[ciHOOKED_VEH_CARGOBOB].netId)
					
//						// Press ~INPUT_VEH_GRAPPLING_HOOK~ to detach the police car from the Cargobob while hovering over the helipad.
//						PRINT_THIS_HELP(eHELPTEXT_MAD_BOB_2)
					
						vCopCar = NET_TO_VEH(serverBD.sVehicle[ciHOOKED_VEH_POLICE3].netId)
						vBob 	= NET_TO_VEH(serverBD.sVehicle[ciHOOKED_VEH_CARGOBOB].netId)
						
						IF NOT IS_ENTITY_DEAD(vCopCar)
						AND NOT IS_ENTITY_DEAD(vBob)
						
							fHeight = GET_ENTITY_HEIGHT_ABOVE_GROUND(vCopCar)
							PRINTLN(" [MB_MISSION] fHeight = ", fHeight)
							
							// Auto detahc if too low
							IF fHeight < 3.0
//							OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_GRAPPLING_HOOK)
//							OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_GRAPPLING_HOOK)
								DETACH_ENTITY_FROM_CARGOBOB(vBob, vCopCar)
//								REMOVE_PICK_UP_ROPE_FOR_CARGOBOB(vBob)
								PRINTLN(" [MB_MISSION] DETACH_ENTITY_FROM_CARGOBOB, fHeight = ", fHeight)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BHH_MAD2")
						CLEAR_HELP()
					ENDIF
				ENDIF
			ELSE
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BHH_MAD2")
					CLEAR_HELP()
				ENDIF
			
				IF DOES_BLIP_EXIST(localCargoDropRadiusBlip)
					REMOVE_BLIP(localCargoDropRadiusBlip)
					PRINTLN(" [MB_MISSION] REMOVE_SEARCH_AREA_BLIP kill sound ")
				ENDIF
				
			ENDIF
		BREAK
		
		CASE MBV_COLLECT_DJ_COLLECTOR
			
			IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_LOADED_COLLECTOR_SOUNDS)
				IF REQUEST_SCRIPT_AUDIO_BANK("SCRIPT/CAR_STEAL_4")
					SET_LOCAL_BIT(eLOCALBITSET_LOADED_COLLECTOR_SOUNDS)	
				ENDIF
			ENDIF
			
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
				vehId = NET_TO_VEH(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
				IF NOT IS_ENTITY_DEAD(vehId)
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
						IF GET_MODE_STATE() = eMODESTATE_TAKE_OUT_TARGETS
							REMOVE_DECALS_FROM_VEHICLE(vehId)
						ENDIF
						
						FLOAT fDirtLevel
						fDirtLevel = GET_VEHICLE_DIRT_LEVEL(vehId)
						IF fDirtLevel > 0.0
							PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_MISSION_SPECIFIC_LOGIC_CLIENT - Cleaning mission entity. Dirt level was", fDirtLevel)
							SET_VEHICLE_DIRT_LEVEL(vehId, 0.0)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_REMOVE_LAND_AIRCRAFT_BLIP)
			AND IS_SERVER_BIT_SET(eSERVERBITSET_COLLECTOR_TRAILER_DISCONNECTED)
				REMOVE_VEHICLE_BLIP(COLLECTOR_TRAILER)
				SET_LOCAL_BIT(eLOCALBITSET_REMOVE_LAND_AIRCRAFT_BLIP)
			ENDIF
			
			ENTITY_INDEX propId
			
			INT iProp
			REPEAT GB_GET_NUM_PROPS_FOR_MEGA_BUSINESS_VARIATION(GET_MEGA_BUSINESS_VARIATION(), GET_MEGA_BUSINESS_SUBVARIATION()) iProp
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niProps[iProp])
					propId = NET_TO_ENT(serverBD.niProps[iProp])
					IF IS_ENTITY_DEAD(propId)
						IF IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(propId)
							IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.niProps[iProp])
								DETACH_ENTITY(propId)
								SET_ENTITY_DYNAMIC(propId, TRUE)
								ACTIVATE_PHYSICS(propId)
								FREEZE_ENTITY_POSITION(propId, FALSE)
								APPLY_FORCE_TO_ENTITY(propId, APPLY_TYPE_FORCE, << 0.0, 0.0, 1.0>>, <<0.0, 0.0, 0.0>>, 0, FALSE, FALSE, FALSE)
								APPLY_FORCE_TO_ENTITY(propId, APPLY_TYPE_TORQUE, <<5000.0, 0.0, 0.0>>, <<0.0,0.0,0.0>>, 0, FALSE, TRUE, TRUE)
								PLAY_SOUND_FROM_ENTITY(-1, "lock_break", propId, "DLC_BTL_Collector_Sounds", TRUE)
								CLEANUP_NET_ID(serverBD.niProps[iProp])
								PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_MISSION_SPECIFIC_LOGIC_CLIENT - Cleaning up prop #", iProp)
							ENDIF
						ENDIF
					ELSE
						iLastHingeRemaining = iProp
						IF IS_SERVER_BIT_SET(eSERVERBITSET_REAR_RAMP_PROPS_DESTROYED)
							IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_MIDDLE_RAMP_PROPS_DESTROYED)
								IF iProp = COLLECTOR_MID_LEFT_HINGE
								OR iProp = COLLECTOR_MID_RIGHT_HINGE
									IF NOT GET_ENTITY_CAN_BE_DAMAGED(propId)
										IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.niProps[iProp])
											SET_ENTITY_INVINCIBLE(propId, FALSE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELIF GET_MODE_STATE() = eMODESTATE_TAKE_OUT_TARGETS
							IF iProp = COLLECTOR_BACK_LEFT_HINGE
							OR iProp = COLLECTOR_BACK_RIGHT_HINGE
								IF NOT GET_ENTITY_CAN_BE_DAMAGED(propId)
									IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.niProps[iProp])
										SET_ENTITY_INVINCIBLE(propId, FALSE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF IS_MISSION_ENTITY_BIT_SET(0, eMISSIONENTITYBITSET_DETACHED_FROM_VEHICLE)
			AND NOT IS_MISSION_ENTITY_BIT_SET(0, eMISSIONENTITYBITSET_PLAYED_COLLECTOR_LAND_SOUND)
			AND NOT IS_MISSION_ENTITY_CLIENT_BIT_SET(PARTICIPANT_ID(), 0, eMISSIONENTITYCLIENTBITSET_PLAYED_COLLECTOR_LAND_SOUND)
				vehId = NET_TO_VEH(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
				IF NOT IS_ENTITY_DEAD(vehId)
					IF NOT IS_ENTITY_IN_AIR(vehId)
						IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
							PLAY_SOUND_FROM_ENTITY(-1, "stafford_fall", vehId, "DLC_BTL_Collector_Sounds", TRUE)
							SET_MISSION_ENTITY_CLIENT_BIT(0, eMISSIONENTITYCLIENTBITSET_PLAYED_COLLECTOR_LAND_SOUND)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_SERVER_BIT_SET(eSERVERBITSET_REAR_RAMP_PROPS_DESTROYED)
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[COLLECTOR_TRAILER].netId)
					vehId = NET_TO_VEH(serverBD.sVehicle[COLLECTOR_TRAILER].netId)
					IF NOT IS_ENTITY_DEAD(vehId)
						IF NOT IS_VEHICLE_BIT_SET(COLLECTOR_TRAILER, eVEHICLEBITSET_OPENED_BOOT)
							IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[COLLECTOR_TRAILER].netId)
								SET_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_BOOT, FALSE)
								
								IF NOT IS_VEHICLE_BIT_SET(COLLECTOR_TRAILER, eVEHICLEBITSET_PLAYED_COLLECTOR_LAND_SOUND)
								AND NOT IS_VEHICLE_CLIENT_BIT_SET(PARTICIPANT_ID(), COLLECTOR_TRAILER, eVEHICLECLIENTBITSET_PLAYED_COLLECTOR_LAND_SOUND)
									PLAY_SOUND_FROM_ENTITY(-1, "ramp_fall", vehId, "DLC_BTL_Collector_Sounds", TRUE)
									SET_VEHICLE_CLIENT_BIT(COLLECTOR_TRAILER, eVEHICLECLIENTBITSET_PLAYED_COLLECTOR_LAND_SOUND)
								ENDIF
								
								IF GET_VEHICLE_DOOR_ANGLE_RATIO(vehId, SC_DOOR_BOOT) > 0.4
								OR IS_SERVER_BIT_SET(eSERVERBITSET_COLLECTOR_TRAILER_DISCONNECTED)
									SET_VEHICLE_CLIENT_BIT(COLLECTOR_TRAILER, eVEHICLECLIENTBITSET_OPENED_BOOT)
								ENDIF
							ENDIF
						ELSE
							IF NOT IS_VEHICLE_DOOR_DAMAGED(vehId, SC_DOOR_BOOT)
								IF HAS_NET_TIMER_EXPIRED(serverBD.stCollectorDetachDelay, 500)
								OR IS_SERVER_BIT_SET(eSERVERBITSET_COLLECTOR_TRAILER_DISCONNECTED)
									IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[COLLECTOR_TRAILER].netId)
										SET_VEHICLE_DOOR_BROKEN(vehId, SC_DOOR_BOOT, FALSE)			// DETACH FIRST
									ENDIF
								ENDIF
							ENDIF
						ENDIF

						IF IS_SERVER_BIT_SET(eSERVERBITSET_MIDDLE_RAMP_PROPS_DESTROYED)
						AND NOT IS_SERVER_BIT_SET(eSERVERBITSET_COLLECTOR_TRAILER_DISCONNECTED)
							IF NOT IS_VEHICLE_BIT_SET(COLLECTOR_TRAILER, eVEHICLEBITSET_OPENED_BONNET)
								IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[COLLECTOR_TRAILER].netId)
									SET_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_BONNET, FALSE)
									SET_VEHICLE_CLIENT_BIT(COLLECTOR_TRAILER, eVEHICLECLIENTBITSET_OPENED_BONNET)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
				
		CASE MBV_SELL_NOT_A_SCRATCH
			IF IS_SERVER_BIT_SET(eSERVERBITSET_SERVER_INITIALISED)
				INT iMissionEntity
				REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
					IF serverBD.sMissionEntity[iMissionEntity].iBonusCash < playerBD[PARTICIPANT_ID_TO_INT()].iMissionEntityBonusCash[iMissionEntity]
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_MISSION_SPECIFIC_LOGIC_CLIENT - Server bonus cash value for mission entity ", iMissionEntity," is less than mine. Server: ", serverBD.sMissionEntity[iMissionEntity].iBonusCash, "; mine: ", playerBD[PARTICIPANT_ID_TO_INT()].iMissionEntityBonusCash[iMissionEntity], ". Updating mine to match.")
						playerBD[PARTICIPANT_ID_TO_INT()].iMissionEntityBonusCash[iMissionEntity] = serverBD.sMissionEntity[iMissionEntity].iBonusCash
						IF playerBD[PARTICIPANT_ID_TO_INT()].iMissionEntityBonusCash[iMissionEntity] < 0
							playerBD[PARTICIPANT_ID_TO_INT()].iMissionEntityBonusCash[iMissionEntity] = 0
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
		BREAK
		
		CASE MBV_SUPPLY_RUN
			IF IS_SERVER_BIT_SET(eSERVERBITSET_SERVER_INITIALISED)
				INT iMissionEntity
				REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
					IF serverBD.sMissionEntity[iMissionEntity].iBonusCash < playerBD[PARTICIPANT_ID_TO_INT()].iMissionEntityBonusCash[iMissionEntity]
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_MISSION_SPECIFIC_LOGIC_CLIENT - Server bonus cash value for mission entity ", iMissionEntity," is less than mine. Server: ", serverBD.sMissionEntity[iMissionEntity].iBonusCash, "; mine: ", playerBD[PARTICIPANT_ID_TO_INT()].iMissionEntityBonusCash[iMissionEntity], ". Updating mine to match.")
						playerBD[PARTICIPANT_ID_TO_INT()].iMissionEntityBonusCash[iMissionEntity] = serverBD.sMissionEntity[iMissionEntity].iBonusCash
					ENDIF
				ENDREPEAT
				
				IF serverBD.sMissionEntity[MISSION_ENTITY_ONE].iBonusCash <= 0
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
						vehID = NET_TO_VEH(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
						IF IS_ENTITY_ALIVE(vehID)
							IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
							
								KNACKER_MISSION_ENTITY(vehID)
								SET_MISSION_ENTITY_CLIENT_BIT(MISSION_ENTITY_ONE, eMISSIONENTITYCLIENTBITSET_KNACKERED)
								PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_MISSION_SPECIFIC_LOGIC_CLIENT - KNACKER_MISSION_ENTITY  ")
								
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE MBV_SELL_PROTECT_BUYER
			IF IS_LOCAL_BIT_SET(eLOCALBITSET_DO_PROTECT_BUYER_DELAYED_AMBUSH)
			AND NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_TRIGGER_AMBUSH)
				IF HAS_NET_TIMER_EXPIRED(stLocalProtectBuyerAmbushDelayTimer, 10000)
					SET_CLIENT_BIT(eCLIENTBITSET_TRIGGER_AMBUSH)
				ENDIF
			ENDIF
		
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(localPlayerPed, GET_MEGA_BUSINESS_DROP_OFF_COORDS(serverBD.sMissionEntity[MISSION_ENTITY_ONE].eDropOffs[2], serverBD.nightclubID, GET_MEGA_BUSINESS_SUBVARIATION())) < 150
				IF NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_AM_DEFENDING_BUYER)
					SET_CLIENT_BIT(eCLIENTBITSET_I_AM_DEFENDING_BUYER)
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_MISSION_SPECIFIC_LOGIC_CLIENT - I'm within 50m of buyer, defending them")
				ENDIF
			ELSE
				IF IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_AM_DEFENDING_BUYER)
					CLEAR_CLIENT_BIT(eCLIENTBITSET_I_AM_DEFENDING_BUYER)
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_MISSION_SPECIFIC_LOGIC_CLIENT - I'm no longer within 50m of buyer, not defending them")
				ENDIF
			ENDIF
		BREAK
		
		CASE MBV_SELL_OFFSHORE_TRANSFER
			IF GET_MODE_STATE() >= eMODESTATE_LEAVE_AREA
				IF NOT IS_MISSION_ENTITY_BIT_SET(MISSION_ENTITY_ONE, eMISSIONENTITYBITSET_CANNOT_BE_ATTACHED)
				AND NOT IS_MISSION_ENTITY_CLIENT_BIT_SET(PARTICIPANT_ID(), MISSION_ENTITY_ONE, eMISSIONENTITYCLIENTBITSET_CANNOT_BE_ATTACHED)
					IF HAS_MISSION_ENTITY_BEEN_DELIVERED(MISSION_ENTITY_ONE)
					#IF IS_DEBUG_BUILD
					AND NOT HAS_MISSION_BEEN_DEBUG_PASSED_OR_FAILED()
					#ENDIF
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
							vehID = NET_TO_VEH(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
							IF IS_ENTITY_ALIVE(vehID)
								IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
									SET_VEHICLE_AUTOMATICALLY_ATTACHES(vehID, FALSE)
									SET_VEHICLE_DISABLE_TOWING(vehID, TRUE)
									PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_MISSION_SPECIFIC_LOGIC_CLIENT - Prevented mission entity from being collected by cargobob")
									SET_MISSION_ENTITY_CLIENT_BIT(MISSION_ENTITY_ONE, eMISSIONENTITYCLIENTBITSET_CANNOT_BE_ATTACHED)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_MISSION_ENTITY_ATTACHED(MISSION_ENTITY_ONE)
					IF IS_MISSION_ENTITY_IN_POSSESSION_OF_A_PLAYER(MISSION_ENTITY_ONE)
						IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_OFFSHORE_TRANSFER_DO_CALL_NEAR_CARGOBOB)
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
								vehID = NET_TO_VEH(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
								IF IS_ENTITY_ALIVE(vehID)
									IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehID, GET_MEGA_BUSINESS_GOTO_COORDS(GET_MEGA_BUSINESS_VARIATION(), GET_MEGA_BUSINESS_SUBVARIATION(), serverBD.nightclubID)) < 800.0
										SET_LOCAL_BIT(eLOCALBITSET_OFFSHORE_TRANSFER_DO_CALL_NEAR_CARGOBOB)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_OFFSHORE_TRANSFER_DO_CARGOBOB_PILOT_CALL)
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[0].netId)
							vehID = NET_TO_VEH(serverBD.sVehicle[0].netId)
							IF IS_ENTITY_ALIVE(vehID)
								IF IS_PED_IN_THIS_VEHICLE(localPlayerPed, vehID)
								AND GET_PED_IN_VEHICLE_SEAT(vehID, VS_DRIVER) = localPlayerPed
									IF NOT HAS_NET_TIMER_STARTED(stInCargobobCallDelay)
										START_NET_TIMER(stInCargobobCallDelay)
									ELSE
										IF HAS_NET_TIMER_EXPIRED(stInCargobobCallDelay, 3000)
											SET_LOCAL_BIT(eLOCALBITSET_OFFSHORE_TRANSFER_DO_CARGOBOB_PILOT_CALL)
										ENDIF
									ENDIF
								ELSE
									IF HAS_NET_TIMER_STARTED(stInCargobobCallDelay)
										RESET_NET_TIMER(stInCargobobCallDelay)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE MBV_POSTER_PROMOTION
			MAINTAIN_POSTER_DROP_CLIENT()
		BREAK
		CASE MBV_VIP_RESCUE
			// Make sure door is open
			IF NOT IS_MISSION_ENTITY_BIT_SET(MISSION_ENTITY_ONE, eMISSIONENTITYBITSET_DOOR_OPENED)
			AND NOT IS_MISSION_ENTITY_CLIENT_BIT_SET(PARTICIPANT_ID(), MISSION_ENTITY_ONE, eMISSIONENTITYCLIENTBITSET_DOOR_OPENED)
				SWITCH GET_MEGA_BUSINESS_SUBVARIATION()
					CASE MBSV_VIP_RESCUE_1
					CASE MBSV_VIP_RESCUE_3
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
							vehID = NET_TO_VEH(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
							IF IS_ENTITY_ALIVE(vehID)
								IF GET_DISTANCE_BETWEEN_ENTITIES(localPlayerPed, vehID, TRUE) < 100
									IF TAKE_CONTROL_OF_NET_ID(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
										SET_VEHICLE_DOOR_OPEN(vehID, SC_DOOR_REAR_RIGHT, TRUE, TRUE)
										PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_MISSION_SPECIFIC_LOGIC_CLIENT - Made sure the van door is open")
										SET_MISSION_ENTITY_CLIENT_BIT(MISSION_ENTITY_ONE, eMISSIONENTITYCLIENTBITSET_DOOR_OPENED)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
		
		CASE MBV_MUSCLE_OUT
			IF fMyDistanceFromNearestMissionEntity != -1.0
			AND fMyDistanceFromNearestMissionEntity < 30.0
				IF GET_LOCAL_NEAR_CAR_BIT() != eLOCALBITSET_END
					IF NOT IS_LOCAL_BIT_SET(GET_LOCAL_NEAR_CAR_BIT())
						IF NOT IS_PED_IN_ANY_VEHICLE(localPlayerPed)
							IF HAS_NET_TIMER_EXPIRED(stGenericDelayTimer, 500)
								WEAPON_TYPE eWeapon
								GET_CURRENT_PED_WEAPON(localPlayerPed, eWeapon)
								
								IF eWeapon = WEAPONTYPE_UNARMED
									PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_MISSION_SPECIFIC_LOGIC_CLIENT - Within 50m of developer vehicle and unarmed, equipping bat")
									SET_CURRENT_PED_WEAPON(localPlayerPed, WEAPONTYPE_BAT, TRUE)
								ELSE
									PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_MISSION_SPECIFIC_LOGIC_CLIENT - Within 50m of developer vehicle and armed, not equipping bat")
								ENDIF
								
								SET_LOCAL_BIT(GET_LOCAL_NEAR_CAR_BIT())
								RESET_NET_TIMER(stGenericDelayTimer)
							ELSE
								PRINTLN("timer not expired")
							ENDIF
						ELSE
							PRINTLN("in a vehicle")
						ENDIF
					ELSE
						PRINTLN("already equipeed for car")
					ENDIF
				ELSE
					PRINTLN("bitset end")
				ENDIF
				
				IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_MUSCLE_OUT_DO_NEAR_VEH_HELP)
					SET_LOCAL_BIT(eLOCALBITSET_MUSCLE_OUT_DO_NEAR_VEH_HELP)
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_MISSION_SPECIFIC_LOGIC_CLIENT - Within 50m of developer vehicle, doing help about bat")
				ENDIF
			ENDIF
			
			IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_MUSCLE_OUT_DONE_VEH_0_DESTROYED_TICKER)
				IF HAS_MISSION_ENTITY_BEEN_DESTROYED(0)
				AND MissionEntityLastDamagerPlayerID[0] != INVALID_PLAYER_INDEX()
					IF MissionEntityLastDamagerPlayerID[0] = localPlayerId
						PRINT_TICKER("MBS_TK_MSC1")
					ELSE
						PRINT_TICKER_WITH_PLAYER_NAME("MBS_TK_MSC2", MissionEntityLastDamagerPlayerID[0])
					ENDIF
					
					SET_LOCAL_BIT(eLOCALBITSET_MUSCLE_OUT_DONE_VEH_0_DESTROYED_TICKER)
				ENDIF
			ENDIF
			IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_MUSCLE_OUT_DONE_VEH_1_DESTROYED_TICKER)
				IF HAS_MISSION_ENTITY_BEEN_DESTROYED(1)
				AND MissionEntityLastDamagerPlayerID[1] != INVALID_PLAYER_INDEX()
					IF MissionEntityLastDamagerPlayerID[1] = localPlayerId
						PRINT_TICKER("MBS_TK_MSC1")
					ELSE
						PRINT_TICKER_WITH_PLAYER_NAME("MBS_TK_MSC2", MissionEntityLastDamagerPlayerID[1])
					ENDIF
					
					SET_LOCAL_BIT(eLOCALBITSET_MUSCLE_OUT_DONE_VEH_1_DESTROYED_TICKER)
				ENDIF
			ENDIF
			IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_MUSCLE_OUT_DONE_VEH_2_DESTROYED_TICKER)
				IF HAS_MISSION_ENTITY_BEEN_DESTROYED(2)
				AND MissionEntityLastDamagerPlayerID[2] != INVALID_PLAYER_INDEX()
					IF MissionEntityLastDamagerPlayerID[2] = localPlayerId
						PRINT_TICKER("MBS_TK_MSC1")
					ELSE
						PRINT_TICKER_WITH_PLAYER_NAME("MBS_TK_MSC2", MissionEntityLastDamagerPlayerID[2])
					ENDIF
					
					SET_LOCAL_BIT(eLOCALBITSET_MUSCLE_OUT_DONE_VEH_2_DESTROYED_TICKER)
				ENDIF
			ENDIF
			IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_MUSCLE_OUT_DONE_VEH_3_DESTROYED_TICKER)
				IF HAS_MISSION_ENTITY_BEEN_DESTROYED(3)
				AND MissionEntityLastDamagerPlayerID[3] != INVALID_PLAYER_INDEX()
					IF MissionEntityLastDamagerPlayerID[3] = localPlayerId
						PRINT_TICKER("MBS_TK_MSC1")
					ELSE
						PRINT_TICKER_WITH_PLAYER_NAME("MBS_TK_MSC2", MissionEntityLastDamagerPlayerID[3])
					ENDIF
					
					SET_LOCAL_BIT(eLOCALBITSET_MUSCLE_OUT_DONE_VEH_3_DESTROYED_TICKER)
				ENDIF
			ENDIF
		BREAK
		
		CASE MBV_SELL_HACK_DROP
			IF serverBD.iHacksComplete != 0
				IF NOT IS_BIT_SET(iHackDropTickerBitset, serverBD.iHacksComplete)

					PARTICIPANT_INDEX participantId
					PLAYER_INDEX playerId
					
					participantId = INT_TO_PARTICIPANTINDEX(serverBD.iHackParticipant)
										
					IF NETWORK_IS_PARTICIPANT_ACTIVE(participantId)

						playerId = NETWORK_GET_PLAYER_INDEX(participantId)
						IF IS_NET_PLAYER_OK(playerId, FALSE)
						
							IF playerId = localPlayerId
								PRINT_TICKER("MBS_TK_HK1")
							ELSE
								PRINT_TICKER_WITH_PLAYER_NAME("MBS_TK_HK2", playerId)
							ENDIF
							
							SET_BIT(iHackDropTickerBitset, serverBD.iHacksComplete)
						
						ENDIF
					ENDIF	
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

//PROC MAINTAIN_JIMMY_BREAK_IN_FLAG()
//
//	SWITCH GET_MEGA_BUSINESS_VARIATION()
//		CASE MBV_COLLECT_DJ
//			
//			INT iMissionEntity
//			BOOL bSetFlag
//			
//			REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
//				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
//					IF GET_DISTANCE_BETWEEN_COORDS(serverBD.sMissionEntity[iMissionEntity].vSpawnCoords, GET_ENTITY_COORDS(localPlayerPed)) < 12.0
//						IF IS_NET_VEHICLE_DRIVEABLE(serverBD.sMissionEntity[iMissionEntity].netId)
//							bSetFlag = TRUE
//							BREAKLOOP
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDREPEAT
//			
//			IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_SET_JIMMY_BREAK_IN_FLAG)
//				IF bSetFlag
//					SET_PED_CONFIG_FLAG(localPlayerPed, PCF_UseLockpickVehicleEntryAnimations, TRUE)
//					SET_LOCAL_BIT(eLOCALBITSET_SET_JIMMY_BREAK_IN_FLAG)
//				ENDIF
//			ELSE
//				IF NOT bSetFlag
//					SET_PED_CONFIG_FLAG(localPlayerPed, PCF_UseLockpickVehicleEntryAnimations, FALSE)
//					CLEAR_LOCAL_BIT(eLOCALBITSET_SET_JIMMY_BREAK_IN_FLAG)
//				ENDIF
//			ENDIF
//	
//		BREAK
//	ENDSWITCH
//	
//ENDPROC

PROC CLEAR_GLOBAL_BLIP_DATA_FOR_MISSION_ENTITIES()
	CLEAR_RIVAL_ENTITY_VISIBILITY()
ENDPROC

PROC MAINTAIN_SET_GLOBAL_BLIP_DATA_FOR_MISSION_ENTITIES()
	INT iMissionEntity = GET_MISSION_ENTITY_PLAYER_IS_HOLDING(localPlayerId)
				
	IF iMissionEntity != -1
	AND NOT NETWORK_IS_IN_MP_CUTSCENE()
	AND NOT HAS_MISSION_ENTITY_BEEN_DELIVERED_OR_DESTROYED(iMissionEntity)
	AND DOES_MEGA_BUSINESS_VARIATION_NEED_GLOBAL_SIGNAL()
		SET_PLAYER_RIVAL_ENTITY_TYPE(eRIVALENTITYTYPE_FMBB)
		SET_PLAYER_RIVAL_ENTITY_VISIBLE_TO_GANG(TRUE)
		IF IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_A_PARTICIPANT_HAS_ENABLED_MY_GLOBAL_BLIP)
			SET_PLAYER_RIVAL_ENTITY_VISIBLE_TO_RIVALS(TRUE)
		ENDIF
	ELSE
		CLEAR_GLOBAL_BLIP_DATA_FOR_MISSION_ENTITIES()
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_FOCUS_CAM_BE_ACTIVE()
	IF NOT IS_PED_IN_ANY_VEHICLE(localPlayerPed)
		RETURN FALSE
	ENDIF
	
	INT iNearestPed 
	FLOAT fDistanceFromPed

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_SNAPMATIC
			IF GET_MODE_STATE() != eMODESTATE_COLLECT_PED
				RETURN FALSE
			ENDIF
			
			IF NOT IS_PLAYER_IN_POSSESSION_OF_A_MISSION_ENTITY(localPlayerId)
				RETURN FALSE
			ENDIF
			
//			iNearestPed = GET_NEAREST_PED(fDistanceFromPed)
//			IF iNearestPed != -1
				RETURN TRUE
//			ENDIF
		BREAK
		CASE MBV_SELL_FOLLOW_HELI
			IF GET_MODE_STATE() = eMODESTATE_FOLLOW_ENTITY
			AND ARE_ALL_MISSION_ENTITIES_IN_POSSESSION_OF_A_PLAYER()
				RETURN TRUE
			ENDIF
		BREAK
		CASE MBV_COLLECT_STAFF
			IF NOT IS_PLAYER_IN_POSSESSION_OF_A_MISSION_ENTITY(localPlayerId)
				RETURN FALSE
			ENDIF
		
			IF IS_SERVER_BIT_SET(eSERVERBITSET_SETUP_ALL_STAFF_COLLECTED)
				RETURN FALSE
			ENDIF
			
			iNearestPed = GET_NEAREST_PED(fDistanceFromPed)
			IF iNearestPed != -1
				RETURN TRUE
			ENDIF
		BREAK
		CASE MBV_COLLECT_DJ_CRASH
			IF GET_MODE_STATE() = eMODESTATE_FOLLOW_ENTITY
			OR (IS_SERVER_BIT_SET(eSERVERBITSET_SOMEONE_HAS_REACHED_GOTO_LOCATION)
			AND IS_LOCAL_BIT_SET(eLOCALBITSET_STARTED_PRE_MOCAP_STAGE))
				RETURN TRUE
			ENDIF
		BREAK
		CASE MBV_COLLECT_DJ_COLLECTOR
			IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_MIDDLE_RAMP_PROPS_DESTROYED)
			AND GET_MODE_STATE() != eMODESTATE_GO_TO_POINT
				RETURN TRUE
			ENDIF
		BREAK
		CASE MBV_COLLECT_DJ_STOLEN_BAG
			IF GET_MODE_STATE() = eMODESTATE_TAKE_OUT_TARGETS
				RETURN TRUE
			ENDIF
		BREAK
		CASE MBV_COLLECT_DJ_HOOKED

			IF fDistBetweenCargoboAndCopCar > 0
			AND fDistBetweenCargoboAndCopCar <= cfMADONNA_HINT_DIST		// sync with SHOULD_SHOW_HELP
			AND NOT IS_VEHICLE_BIT_SET(ciHOOKED_VEH_POLICE3, eVEHICLEBITSET_VEH_CARGOBOBBED)
			AND NOT IS_SERVER_BIT_SET(eSERVERBITSET_MID_MISSION_MOCAP_DONE)
			AND GET_MODE_STATE() != eMODESTATE_DELIVER_ENTITY
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE MBV_SELL_FIND_BUYER
			IF GET_MODE_STATE() != eMODESTATE_LEAVE_AREA
				RETURN IS_SERVER_BIT_SET(eSERVERBITSET_BUYER_FOUND)
			ENDIF
		BREAK
		
		CASE MBV_SELL_PROTECT_BUYER
			IF serverBD.iTotalDeliveredCount = 2
				IF GET_MISSION_ENTITY_I_AM_IN_WITH_MISSION_ENTITY_HOLDER() != -1
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC ENTITY_INDEX GET_FOCUS_CAM_ENTITY()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_SELL_FOLLOW_HELI
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[0].netId)
				RETURN NET_TO_ENT(serverBD.sVehicle[0].netId)
			ENDIF
		BREAK
		
		CASE MBV_COLLECT_STAFF
			INT iNearestPed 
			FLOAT fDistanceFromPed
			iNearestPed = GET_NEAREST_PED(fDistanceFromPed)
			IF iNearestPed != -1
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[iNearestPed].netId)
					RETURN NET_TO_ENT(serverBD.sPed[iNearestPed].netId)
				ENDIF
			ENDIF
		BREAK
		
		CASE MBV_COLLECT_DJ_CRASH
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[0].netId)
				RETURN NET_TO_ENT(serverBD.sVehicle[0].netId)
			ENDIF
		BREAK
		
		CASE MBV_COLLECT_DJ_COLLECTOR
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
				RETURN NET_TO_ENT(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
			ENDIF
		BREAK
		
		CASE MBV_COLLECT_DJ_STOLEN_BAG
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[3].netId)
				RETURN NET_TO_ENT(serverBD.sPed[3].netId)
			ENDIF
		BREAK
		CASE MBV_COLLECT_DJ_HOOKED		
			INT iMadonna
			iMadonna = HOOKED_PED_ARRAY_INDEX(HOOKED_PED_MADONNA)
			IF iMadonna != -1
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[iMadonna].netId)
					RETURN NET_TO_ENT(serverBD.sPed[iMadonna].netId)
				ENDIF
			ENDIF
		BREAK
		CASE MBV_SNAPMATIC
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[0].netId)
				RETURN NET_TO_ENT(serverBD.sPed[0].netId)
			ENDIF
		BREAK
		CASE MBV_SELL_PROTECT_BUYER
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[0].netId)
				RETURN NET_TO_ENT(serverBD.sPed[0].netId)
			ENDIF
		BREAK
		CASE MBV_SELL_FIND_BUYER
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[0].netId)
				RETURN NET_TO_ENT(serverBD.sPed[0].netId)
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN NULL
ENDFUNC 

FUNC FLOAT GET_FOCUS_CAM_DISTANCE()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_SELL_FOLLOW_HELI
			RETURN 400.0
			
		CASE MBV_COLLECT_STAFF
			RETURN 50.0
			
		CASE MBV_SELL_FIND_BUYER
			RETURN cfCLUE_VIS_DISTANCE
			
		CASE MBV_SNAPMATIC
			
			SWITCH GET_MEGA_BUSINESS_SUBVARIATION()
				CASE MBSV_SNAPMATIC_1
					SWITCH GET_RANDOM_MODE_VARIANT_FOR_ENTITY_LOOKUP()
						CASE 6
							RETURN 100.00
					ENDSWITCH
				BREAK
			ENDSWITCH				
		
			RETURN 60.0
			
		CASE MBV_COLLECT_DJ_CRASH
			RETURN 1000.0
		
		CASE MBV_COLLECT_DJ_COLLECTOR
			RETURN 100.0
			
		CASE MBV_COLLECT_DJ_STOLEN_BAG
			RETURN 70.0

		CASE MBV_COLLECT_DJ_HOOKED
			RETURN cfMADONNA_HINT_DIST
			
		CASE MBV_SELL_PROTECT_BUYER
			RETURN 100.0
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC FLOAT GET_FOCUS_CAM_MIN_DISTANCE()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_SELL_FOLLOW_HELI
			RETURN 150.0
	ENDSWITCH

	RETURN -1.0
ENDFUNC

FUNC BOOL SHOULD_CHECK_FOR_PLAYER_CONTROL_FOR_FOCUS_CAM()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_CRASH			RETURN FALSE
	ENDSWITCH
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_SHOW_FOCUS_CAM_HINT()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_CRASH	
		CASE MBV_COLLECT_DJ_HOOKED
		CASE MBV_SELL_PROTECT_BUYER
			RETURN FALSE
	ENDSWITCH
	RETURN TRUE
ENDFUNC

FUNC STRING GET_FOCUS_CAM_HINT_STRING()
	RETURN "GR_TOGFOCUS"
ENDFUNC

FUNC HINT_TYPE GET_FOCUS_CAM_HINT_TYPE()

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_HOOKED
			RETURN HINTTYPE_VEHICLE_HIGH_ZOOM
	ENDSWITCH

	RETURN HINTTYPE_DEFAULT
ENDFUNC

PROC CLEANUP_FOCUS_CAM()
	IF IS_LOCAL_BIT_SET(eLOCALBITSET_FOCUS_CAM_ACTIVE)
		KILL_CHASE_HINT_CAM(chcs_FocusCam)
	    CLEAR_LOCAL_BIT(eLOCALBITSET_FOCUS_CAM_ACTIVE)
	    SET_USING_MISSION_SPECIFIC_HINT_CAM_IN_FM(FALSE)
	ENDIF
ENDPROC

PROC MAINTAIN_FOCUS_CAM()

	BOOL bFocusActive = FALSE

	IF SHOULD_FOCUS_CAM_BE_ACTIVE()	

		ENTITY_INDEX focusEntity = GET_FOCUS_CAM_ENTITY()

		IF bLocalPlayerOk
			IF DOES_ENTITY_EXIST(focusEntity)
			AND IS_ENTITY_ALIVE(focusEntity)
				IF NOT IS_SCREEN_FADED_OUT()
				    IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(focusEntity), GET_ENTITY_COORDS(localPlayerPed)) <= GET_FOCUS_CAM_DISTANCE()
//					AND (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(focusEntity), GET_ENTITY_COORDS(localPlayerPed), FALSE) > GET_FOCUS_CAM_MIN_DISTANCE()
//					OR GET_FOCUS_CAM_MIN_DISTANCE() = -1)

						#IF IS_DEBUG_BUILD
						IF GET_COMMANDLINE_PARAM_EXISTS("sc_FocusControlDebug")
							PRINTLN("[FOCUS_NIGHTMARE] MAINTAIN_FOCUS_CAM - SHOULD_CHECK_FOR_PLAYER_CONTROL_FOR_FOCUS_CAM = ", GET_STRING_FROM_BOOL(SHOULD_CHECK_FOR_PLAYER_CONTROL_FOR_FOCUS_CAM()))
						ENDIF
						#ENDIF
						
						CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE(chcs_FocusCam, focusEntity, GET_FOCUS_CAM_HINT_STRING(), GET_FOCUS_CAM_HINT_TYPE(), SHOULD_CHECK_FOR_PLAYER_CONTROL_FOR_FOCUS_CAM(), SHOULD_SHOW_FOCUS_CAM_HINT())
			            SET_USING_MISSION_SPECIFIC_HINT_CAM_IN_FM(TRUE)
			            SET_LOCAL_BIT(eLOCALBITSET_FOCUS_CAM_ACTIVE)
						bFocusActive = TRUE
				    ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	IF NOT bFocusActive
		CLEANUP_FOCUS_CAM()
	ENDIF

ENDPROC

FUNC BOOL ACTIVATE_FOCUS_CAM()
	IF NOT IS_GAMEPLAY_HINT_ACTIVE()	
		ENTITY_INDEX focusEntity = GET_FOCUS_CAM_ENTITY()
		IF DOES_ENTITY_EXIST(focusEntity)
		AND IS_ENTITY_ALIVE(focusEntity)
			IF SET_CONTROL_VALUE_NEXT_FRAME(PLAYER_CONTROL, INPUT_VEH_CIN_CAM, 1.0)
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - ACTIVATE_FOCUS_CAM - Activated focus cam.")
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_VARIATION_ACTION(INT iAreaNumber)

	SWITCH GET_MEGA_BUSINESS_SUBVARIATION()
		CASE MBSV_COLLECT_STAFF_0
			SWITCH iAreaNumber
				CASE 0
					RETURN <<498.524292,-1539.962036,28.259773>> 
				CASE 1
					RETURN <<-1572.005981,-1131.250488,2.118061>> 
			ENDSWITCH
		BREAK
		CASE MBSV_COLLECT_STAFF_2
			SWITCH iAreaNumber
				CASE 0
					RETURN <<-740.4633, 243.8979, 75.5489>>
				CASE 1
					RETURN <<-1954.3846, -659.0789, 5.6608>>
			ENDSWITCH
		BREAK
		CASE MBSV_COLLECT_STAFF_3
			SWITCH iAreaNumber
				CASE 0
					RETURN <<-429.0680, 259.9240, 82.0160>>
				CASE 1
					RETURN <<-1787.8680, -866.6340, 6.5480>>
			ENDSWITCH
		BREAK
		CASE MBSV_COLLECT_STAFF_4
			SWITCH iAreaNumber
				CASE 0
					RETURN <<-1897.7830, -706.8260, 6.5720>>
			ENDSWITCH
		BREAK
		CASE MBSV_COLLECT_STAFF_5
			SWITCH iAreaNumber
				CASE 0
					RETURN <<73.6210, -1026.9080, 28.4760>>
				CASE 1
					RETURN <<-1292.8948, -1710.5352, 1.7151>>
			ENDSWITCH
		BREAK
		CASE MBSV_COLLECT_STAFF_7
			SWITCH iAreaNumber
				CASE 0
					RETURN <<99.3430, -1316.0140, 28.2900>>
				CASE 1
					RETURN <<-1442.0576, -1446.0238, 1.6371>>
			ENDSWITCH
		BREAK
		CASE MBSV_COLLECT_STAFF_9
			SWITCH iAreaNumber
				CASE 0
					RETURN <<254.4998, -1010.5359, 28.2722>>
				CASE 1
					RETURN <<-388.1436, -148.5444, 37.5323>>
			ENDSWITCH
		BREAK
		
		CASE MBSV_VIP_RESCUE_2
			SWITCH iAreaNumber
				CASE 0
					RETURN <<357.397308,-1974.021362,22.852692>> 
			ENDSWITCH
		BREAK

		CASE MBSV_MUSCLE_OUT_1			
			SWITCH iAreaNumber
				CASE 0
					RETURN <<326.1970, -1287.0861, 30.7080>>
			ENDSWITCH
		BREAK
		
		CASE MBSV_MUSCLE_OUT_2
			SWITCH iAreaNumber
				CASE 0
					RETURN <<376.01129, -909.26715, 27.55390>>				
				CASE 1
					RETURN <<396.72525, -898.06219, 30.41868>>		
			ENDSWITCH
		BREAK				

		CASE MBSV_MUSCLE_OUT_3
			SWITCH iAreaNumber
				CASE 0
					RETURN <<-487.8270, 273.4700, 82.2360>>				
				CASE 1
					RETURN <<-47.8660, -217.3830, 44.4450>>			
			ENDSWITCH
		BREAK
		
		CASE MBSV_MUSCLE_OUT_4
			SWITCH iAreaNumber
				CASE 0
					RETURN <<314.75735, -1267.59094, 29.48466>> 				
			
				CASE 1
					RETURN <<339.18375, -1297.90649, 36.51250>>		
			ENDSWITCH
		BREAK

		CASE MBSV_MUSCLE_OUT_5
			SWITCH iAreaNumber
				CASE 0
					RETURN <<-704.0390, -874.8890, 22.5440>>
			ENDSWITCH
		BREAK
		
		CASE MBSV_MUSCLE_OUT_6
			SWITCH iAreaNumber
				CASE 0
					RETURN <<1077.27405, -1810.17651, 35.30690>> 
				CASE 1
					RETURN <<1073.72656, -1807.01514, 37.30690>>
			ENDSWITCH
		BREAK
		
		CASE MBSV_MUSCLE_OUT_7		
			SWITCH iAreaNumber
				CASE 0
					RETURN <<820.5696, -2401.1482, 22.6579>>
				CASE 1
					RETURN <<105.2480, -148.0750, 53.7490>>
			ENDSWITCH
		BREAK

		CASE MBSV_MUSCLE_OUT_8			
			SWITCH iAreaNumber
				CASE 0
					RETURN <<1195.08325, -3328.52124, 4.02877>> 				

				CASE 1
					RETURN <<1199.15051, -3331.36279, 6.02877>>	
			ENDSWITCH
		BREAK

		CASE MBSV_MUSCLE_OUT_9
			SWITCH iAreaNumber
				CASE 0
					RETURN <<-1570.06799, -486.76248, 33.41711>> 					
				
				CASE 1
					RETURN <<-1531.45020, -464.90082, 36.41632>>	
			ENDSWITCH
		BREAK
		
		CASE MBSV_SELL_UNDERCOVER_COPS_0
			SWITCH iAreaNumber
				CASE 0
					RETURN <<-727.2366, -1500.1879, 4.0005>>
			ENDSWITCH
		BREAK
		CASE MBSV_SELL_UNDERCOVER_COPS_1
			SWITCH iAreaNumber
				CASE 0
					RETURN <<1050.6659, 2671.3445, 38.5512>>
			ENDSWITCH
		BREAK
		CASE MBSV_SELL_UNDERCOVER_COPS_2
			SWITCH iAreaNumber
				CASE 0
					RETURN <<2514.2642, 4115.8379, 37.5919>>			
				CASE 1
					RETURN <<2500.4788, 4084.6531, 37.3716>>				
			ENDSWITCH
		BREAK
		
		CASE MBSV_POSTER_PROMOTION_0	
			SWITCH iAreaNumber
				CASE 0
					RETURN <<-1539.8850, -448.5810, 34.8870>>
				CASE 1
					RETURN <<-425.1638, 1109.6737, 326.6821>>
			ENDSWITCH
		BREAK
		CASE MBSV_POSTER_PROMOTION_1	
			SWITCH iAreaNumber
				CASE 0
					RETURN <<-705.118286,-885.602905,22.802042>>
			ENDSWITCH
		BREAK	
		
		CASE MBSV_COLLECT_HOOKED_0	
			SWITCH iAreaNumber
				CASE 0
					RETURN <<1211.250122,-3106.554688,5.765787>> 
			ENDSWITCH
		BREAK

		CASE MBSV_COLLECT_HOOKED_3
			SWITCH iAreaNumber
				CASE 0
					RETURN <<-1443.6689, -881.5500, 9.7900>>
			ENDSWITCH
		BREAK
		CASE MBSV_COLLECT_HOOKED_4
			SWITCH iAreaNumber
				CASE 0
					RETURN <<394.5623, -904.0329, 28.4181>>
			ENDSWITCH
		BREAK
		CASE MBSV_COLLECT_HOOKED_5
			SWITCH iAreaNumber
				CASE 0
					RETURN <<-604.7650, -1798.4332, 22.4385>>
			ENDSWITCH
		BREAK
		CASE MBSV_COLLECT_HOOKED_6
			SWITCH iAreaNumber
				CASE 0
					RETURN <<857.6037, -1645.4447, 29.0996>>
			ENDSWITCH
		BREAK
		CASE MBSV_COLLECT_HOOKED_7
			SWITCH iAreaNumber
				CASE 0
					RETURN <<-1400.5989, -342.9808, 39.0997>>
			ENDSWITCH
		BREAK
		CASE MBSV_COLLECT_HOOKED_8
			SWITCH iAreaNumber
				CASE 0
					RETURN <<-39.8261, -103.5784, 56.6225>>
			ENDSWITCH
		BREAK
		CASE MBSV_COLLECT_HOOKED_9
			SWITCH iAreaNumber
				CASE 0
					RETURN <<68.4922, -1049.9224, 28.4272>>
			ENDSWITCH
		BREAK
		
		CASE MBSV_VIP_RESCUE_3
			SWITCH iAreaNumber
				CASE 0
					RETURN <<705.2330, -293.4020, 58.1830>>
			ENDSWITCH
		BREAK
		
		CASE MBSV_PRIVATE_TAXI_4
			SWITCH iAreaNumber
				CASE 0
					RETURN <<-1019.02881, 358.69800, 68.63680>>
			ENDSWITCH
		BREAK
		
		CASE MBSV_PRIVATE_TAXI_5
			SWITCH iAreaNumber
				CASE 0
					RETURN <<-120.93193, 382.06717, 110.62803>>
			ENDSWITCH
		BREAK
		
		CASE MBSV_PRIVATE_TAXI_6
			SWITCH iAreaNumber
				CASE 0
					RETURN <<-166.7080, 268.6090, 92.3320>>
			ENDSWITCH
		BREAK
		
		CASE MBSV_PRIVATE_TAXI_7
			SWITCH iAreaNumber
				CASE 0
					RETURN <<-733.3334, -132.6494, 36.4412>>
				CASE 1
					RETURN <<952.1340, -652.4770, 56.8290>>
			ENDSWITCH
		BREAK

	ENDSWITCH
	
	RETURN <<0.0,0.0,0.0>>
ENDFUNC

FUNC VECTOR GET_SCENARIO_BLOCKING_SIZE(INT iAreaNumber)

	SWITCH GET_MEGA_BUSINESS_SUBVARIATION()
		CASE MBSV_COLLECT_STAFF_0
			SWITCH iAreaNumber
				CASE 0
					RETURN <<8, 8, 8>>
				CASE 1
					RETURN <<5, 5, 5>>
			ENDSWITCH
		BREAK
		CASE MBSV_COLLECT_STAFF_2
			SWITCH iAreaNumber
				CASE 0
					RETURN <<10, 10, 10>>
				CASE 1
					RETURN <<3, 3, 3>>
			ENDSWITCH
		BREAK
		CASE MBSV_COLLECT_STAFF_3
			SWITCH iAreaNumber
				CASE 0
					RETURN <<5, 5, 5>>
				CASE 1
					RETURN <<5, 5, 5>>
			ENDSWITCH
		BREAK
		CASE MBSV_COLLECT_STAFF_4
			SWITCH iAreaNumber
				CASE 0
					RETURN <<5, 5, 5>>
			ENDSWITCH
		BREAK
		CASE MBSV_COLLECT_STAFF_5
			SWITCH iAreaNumber
				CASE 0
					RETURN <<5, 5, 5>>
				CASE 1
					RETURN <<5, 5, 5>>
			ENDSWITCH
		BREAK
		CASE MBSV_COLLECT_STAFF_7
			SWITCH iAreaNumber
				CASE 0
					RETURN <<10, 10, 10>>
				CASE 1
					RETURN <<5, 5, 5>>
			ENDSWITCH
		BREAK
		CASE MBSV_COLLECT_STAFF_9
			SWITCH iAreaNumber
				CASE 0
					RETURN <<5.5, 5.5, 5.5>>
				CASE 1
					RETURN <<5, 5, 5>>
			ENDSWITCH
		BREAK
		
		CASE MBSV_VIP_RESCUE_2
			SWITCH iAreaNumber
				CASE 0
					RETURN <<20.0, 20.0, 20.0>>
			ENDSWITCH
		BREAK
		
		CASE MBSV_MUSCLE_OUT_0
		CASE MBSV_MUSCLE_OUT_1
		CASE MBSV_MUSCLE_OUT_2
		CASE MBSV_MUSCLE_OUT_3
		CASE MBSV_MUSCLE_OUT_4
		CASE MBSV_MUSCLE_OUT_5
		CASE MBSV_MUSCLE_OUT_6
		CASE MBSV_MUSCLE_OUT_7
		CASE MBSV_MUSCLE_OUT_8
		CASE MBSV_MUSCLE_OUT_9
		
			RETURN <<15.0, 15.0, 15.0>>		

		CASE MBSV_SELL_UNDERCOVER_COPS_0
			RETURN <<5.0, 5.0, 5.0>>			
		BREAK
		CASE MBSV_SELL_UNDERCOVER_COPS_1
			RETURN <<15.0, 15.0, 15.0>>				
		BREAK
		CASE MBSV_SELL_UNDERCOVER_COPS_2
			SWITCH iAreaNumber
				CASE 0
					RETURN <<6.0, 6.0, 6.0>>					
				CASE 1
					RETURN <<6.0, 6.0, 6.0>>					
			ENDSWITCH
		BREAK
			
		CASE MBSV_COLLECT_HOOKED_0
			RETURN <<50.0, 50.0, 50.0>> 
		
		CASE MBSV_COLLECT_HOOKED_3
			RETURN <<20.0, 20.0, 20.0>> 
			
		CASE MBSV_COLLECT_HOOKED_4
			RETURN <<15.0, 15.0, 15.0>>	
			
		CASE MBSV_COLLECT_HOOKED_5
			RETURN <<20.0, 20.0, 20.0>> 

		CASE MBSV_COLLECT_HOOKED_6
			RETURN <<20.0, 20.0, 20.0>> 

		CASE MBSV_COLLECT_HOOKED_7
			RETURN <<20.0, 20.0, 20.0>> 

		CASE MBSV_COLLECT_HOOKED_8
			RETURN <<20.0, 20.0, 20.0>> 

		CASE MBSV_COLLECT_HOOKED_9
			RETURN <<20.0, 20.0, 20.0>> 

			
		CASE MBSV_POSTER_PROMOTION_0
			SWITCH iAreaNumber
				CASE 0
				CASE 1
					RETURN <<5.0, 5.0, 5.0>>
			ENDSWITCH
		BREAK
		CASE MBSV_POSTER_PROMOTION_1	
			SWITCH iAreaNumber
				CASE 0
					RETURN <<5.0, 5.0, 5.0>>
			ENDSWITCH
		BREAK
		
		CASE MBSV_VIP_RESCUE_3
			SWITCH iAreaNumber
				CASE 0
					RETURN <<10.0, 10.0, 10.0>>
			ENDSWITCH
		BREAK
		
		CASE MBSV_PRIVATE_TAXI_4
			SWITCH iAreaNumber
				CASE 0
					RETURN <<15.0, 15.0, 15.0>>
			ENDSWITCH
		BREAK
		
		CASE MBSV_PRIVATE_TAXI_5
			SWITCH iAreaNumber
				CASE 0
					RETURN <<10.0, 10.0, 10.0>>
			ENDSWITCH
		BREAK
		
		CASE MBSV_PRIVATE_TAXI_6
			SWITCH iAreaNumber
				CASE 0
					RETURN <<5.0, 5.0, 5.0>>
			ENDSWITCH
		BREAK
		
		CASE MBSV_PRIVATE_TAXI_7
			SWITCH iAreaNumber
				CASE 0
					RETURN <<5.0, 5.0, 5.0>>
				CASE 1
					RETURN <<10.0, 10.0, 10.0>>
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN <<0.0,0.0,0.0>>
ENDFUNC

CONST_INT ciMAX_SCENARIO_BLOCKS 3

SCENARIO_BLOCKING_INDEX sbiBlockingArea[ciMAX_SCENARIO_BLOCKS]
INT iScenarioBitset

PROC CLEANUP_SCENARIO_BLOCKS()
	INT i
	REPEAT ciMAX_SCENARIO_BLOCKS i
		IF IS_BIT_SET(iScenarioBitset, i)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SCRIPT_CLEANUP - REMOVE_SCENARIO_BLOCKING_AREA(sbiBlockingArea)")
			REMOVE_SCENARIO_BLOCKING_AREA(sbiBlockingArea[i])
			CLEAR_BIT(iScenarioBitset, i)
		ENDIF
	ENDREPEAT
ENDPROC

//Maintain adding blocking areas
PROC MAINTAIN_SCENARIO_BLOCKING_AREAS()	
	IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_SCENARIO_BLOCKING_AREA_ADDED)
	
		INT i
		VECTOR vActionPos
		VECTOR vBlockingSize
		
		REPEAT ciMAX_SCENARIO_BLOCKS i

			vActionPos = GET_VARIATION_ACTION(i)
			vBlockingSize = GET_SCENARIO_BLOCKING_SIZE(i)
			
			IF NOT IS_VECTOR_ZERO(vActionPos)
				IF NOT DOES_SCENARIO_BLOCKING_AREA_EXISTS(vActionPos - vBlockingSize, vActionPos + vBlockingSize)
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_SCENARIO_BLOCKING_AREAS - ADD_SCENARIO_BLOCKING_AREA(", vActionPos - vBlockingSize, ", ", vActionPos + vBlockingSize, ")")
					sbiBlockingArea[i] = ADD_SCENARIO_BLOCKING_AREA(vActionPos - vBlockingSize, vActionPos + vBlockingSize)
					SET_BIT(iScenarioBitset, i)
				ELSE
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_SCENARIO_BLOCKING_AREAS - scenario blocking area already exists for (", vActionPos - vBlockingSize, ", ", vActionPos + vBlockingSize, ")")
				ENDIF
			ENDIF
		
		ENDREPEAT
		
		SET_LOCAL_BIT(eLOCALBITSET_SCENARIO_BLOCKING_AREA_ADDED)
	ENDIF
ENDPROC

FUNC FLOAT GET_ZOOM_PERCENTAGE()
	RETURN 40.0
ENDFUNC

FUNC BOOL SHOULD_ZOOM_RADAR_FOR_VARIATION()
	IF IS_LOCAL_BIT_SET(eLOCALBITSET_RADAR_SHOULD_ZOOM)
	OR IS_LOCAL_BIT_SET(eLOCALBITSET_RADAR_SHOULD_ZOOM_FOR_DROP_OFF)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_RADAR_ZOOM()
	IF SHOULD_ZOOM_RADAR_FOR_VARIATION()
	AND bLocalPlayerOk
	AND NOT IS_ENTITY_DEAD(localPlayerPed)
	AND NOT IS_PED_IN_ANY_PLANE(localPlayerPed)
		IF GET_DPADDOWN_ACTIVATION_STATE() <> DPADDOWN_SECOND
		AND GET_PROFILE_SETTING(PROFILE_DIAPLAY_BIG_RADAR) != 1
			SET_RADAR_ZOOM_PRECISE(GET_ZOOM_PERCENTAGE())
			SET_LOCAL_PLAYER_AS_USING_MISSION_SPECIFIC_ZOOM(TRUE)
			SET_LOCAL_BIT(eLOCALBITSET_USING_MISSION_ZOOM)
		ENDIF
	ELSE
		IF IS_LOCAL_BIT_SET(eLOCALBITSET_USING_MISSION_ZOOM)
			SET_RADAR_ZOOM_PRECISE(0)
			SET_LOCAL_PLAYER_AS_USING_MISSION_SPECIFIC_ZOOM(FALSE)
			CLEAR_LOCAL_BIT(eLOCALBITSET_USING_MISSION_ZOOM)
		ENDIF
	ENDIF
ENDPROC

FUNC VECTOR GET_EOM_LEAVE_AREA_CENTRE()
	
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_SELL_OFFSHORE_TRANSFER
		CASE MBV_SELL_FIND_BUYER
			RETURN GET_MEGA_BUSINESS_DROP_OFF_COORDS(serverBD.sMissionEntity[MISSION_ENTITY_ONE].eDropOffs[0], serverBD.nightclubID, GET_MEGA_BUSINESS_SUBVARIATION(), GET_RANDOM_MODE_VARIANT_FOR_DROP_OFF_LOOKUP())
		CASE MBV_SELL_ROADBLOCK
		CASE MBV_SELL_PROTECT_BUYER
			RETURN GET_MEGA_BUSINESS_DROP_OFF_COORDS(serverBD.sMissionEntity[MISSION_ENTITY_ONE].eDropOffs[serverBD.sMissionEntity[MISSION_ENTITY_ONE].iDeliveriesToMake-1], serverBD.nightclubID, GET_MEGA_BUSINESS_SUBVARIATION(), GET_RANDOM_MODE_VARIANT_FOR_DROP_OFF_LOOKUP())
	ENDSWITCH
	
	SCRIPT_ASSERT("[MB_MISSION] - GET_EOM_LEAVE_AREA_CENTRE needs variation setup")
	RETURN << 0.0, 0.0, 0.0 >>
	
ENDFUNC

FUNC FLOAT GET_LEAVE_AREA_DISTANCE()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_SELL_OFFSHORE_TRANSFER		
//		CASE MBV_SELL_ROADBLOCK
//		CASE MBV_SELL_FIND_BUYER
//		CASE MBV_SELL_PROTECT_BUYER
			RETURN 500.0
	ENDSWITCH
	RETURN 200.0
ENDFUNC

FUNC FLOAT GET_LEAVE_AREA_WARNING_DISTANCE()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_SELL_OFFSHORE_TRANSFER		
		CASE MBV_SELL_ROADBLOCK
		CASE MBV_SELL_FIND_BUYER
			RETURN 150.0
	ENDSWITCH
	RETURN 0.0
ENDFUNC

FUNC BOOL SHOULD_CHECK_LEAVE_AREA_WARNING_DISTANCE()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_SELL_OFFSHORE_TRANSFER		
		CASE MBV_SELL_ROADBLOCK
		CASE MBV_SELL_FIND_BUYER
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_LEAVE_AREA_WARNING_DISTANCE_CHECK(FLOAT fDistance)
	IF NOT SHOULD_CHECK_LEAVE_AREA_WARNING_DISTANCE()
		EXIT
	ENDIF
	
	IF fDistance <= GET_LEAVE_AREA_WARNING_DISTANCE()
		SET_LOCAL_BIT(eLOCALBITSET_INSIDE_LEAVE_AREA_WARNING_DISTANCE)
	ELSE
		CLEAR_LOCAL_BIT(eLOCALBITSET_INSIDE_LEAVE_AREA_WARNING_DISTANCE)
	ENDIF
ENDPROC

PROC MAINTAIN_LEFT_EOM_AREA()
	
	IF GET_MODE_STATE() != eMODESTATE_LEAVE_AREA
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iPlayerOkBitset, NATIVE_TO_INT(localPlayerId))
		IF NOT IS_BIT_SET(iPlayerDeadBitset, NATIVE_TO_INT(localPlayerId))
			
			VECTOR v1 = GET_ENTITY_COORDS(localPlayerPed)
			VECTOR v2 = GET_EOM_LEAVE_AREA_CENTRE()
			FLOAT fDistance = GET_DISTANCE_BETWEEN_COORDS(v1, v2)
			
			MAINTAIN_LEAVE_AREA_WARNING_DISTANCE_CHECK(fDistance)
			
			IF IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_LEFT_EOM_AREA)
				IF fDistance <= GET_LEAVE_AREA_DISTANCE()
					CLEAR_CLIENT_BIT(eCLIENTBITSET_LEFT_EOM_AREA)
				ENDIF
			ELSE
				IF fDistance > GET_LEAVE_AREA_DISTANCE()
					PRINTLN("MAINTAIN_LEFT_EOM_AREA, fDistance = ", fDistance, " v2 = ", v2)
					SET_CLIENT_BIT(eCLIENTBITSET_LEFT_EOM_AREA)
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_COUNT_PLAYER_SCORE()

//	SWITCH GET_MEGA_BUSINESS_VARIATION()
//		CASE MBV_COLLECT_DJ
//			SWITCH GET_MEGA_BUSINESS_SUBVARIATION()
//				CASE MBSV_COLLECT_DJ_LSIA
//				
//					RETURN GET_MODE_STATE() = eMODESTATE_REACH_SCORE
//				BREAK
//				CASE MBSV_COLLECT_DJ_HELI
//				
//					RETURN GET_MODE_STATE() = eMODESTATE_DELIVER_ENTITY 
//				BREAK
//			ENDSWITCH
//		BREAK
//	ENDSWITCH

	RETURN FALSE
ENDFUNC

//PROC START_RECORDING_MODE_STAT()
//
//	SWITCH GET_MEGA_BUSINESS_VARIATION()
//		CASE MBV_COLLECT_DJ
//			SWITCH GET_MEGA_BUSINESS_SUBVARIATION()
//				CASE MBSV_COLLECT_DJ_LSIA
//					STAT_START_RECORD_STAT(REC_STAT_TOP_SPEED_CAR, RSP_GREATEST)
//				BREAK
//				CASE MBSV_COLLECT_DJ_HELI
////					STAT_START_RECORD_STAT(REC_STAT_LONGEST_DRIVE_NOCRASH, RSP_GREATEST)
//				BREAK
//			ENDSWITCH
//		BREAK
//	ENDSWITCH
//
//ENDPROC

FUNC INT GET_SCORE_FOR_MODE()

//	FLOAT fStat
//	INT iStat
//	INT iDmgSinceLastUpdate

//	//Low flying inverted
//	IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_STAT_RECORDING)
//		IF NOT STAT_IS_RECORDING_STAT()
//			START_RECORDING_MODE_STAT()
//			SET_LOCAL_BIT(eLOCALBITSET_STAT_RECORDING)
//		ENDIF
//	ELSE		
//		
//		SWITCH GET_MEGA_BUSINESS_VARIATION()
//			CASE MBV_COLLECT_DJ
//				SWITCH GET_MEGA_BUSINESS_SUBVARIATION()
//					CASE MBSV_COLLECT_DJ_LSIA
//						IF STAT_GET_CURRENT_SPEED() > 100
//							RETURN 1
//						ENDIF
//					BREAK
//					CASE MBSV_COLLECT_DJ_HELI // See PROCESS_DAMAGE_EVENT
//					
////						PRINTLN("[SMUGGLER] [", GET_CURRENT_VARIATION_STRING(), "] - STAT_GET_LAST_PLAYERS_VEHICLES_DAMAGES = ", STAT_GET_CURRENT_DRIVE_NOCRASH_DISTANCE())
////					
////						IF STAT_GET_CURRENT_DRIVE_NOCRASH_DISTANCE() != 0.0
////						AND STAT_GET_CURRENT_DRIVE_NOCRASH_DISTANCE() < 1.0
////							RETURN 10
////						ENDIF
////						
////						STAT_GET_RECORDED_VALUE(fStat)
////						iStat = ROUND(fStat)
////
////						IF sScore.iPreviousScoreStatValue != iStat
////							//Calculate distance since last update
////							iDmgSinceLastUpdate = iStat - sScore.iPreviousScoreStatValue
////							
////							//Print values
////							PRINTLN("[SMUGGLER] [", GET_CURRENT_VARIATION_STRING(), "] - GET_SCORE_FOR_MODE - iStat = ",iStat)
////							PRINTLN("[SMUGGLER] [", GET_CURRENT_VARIATION_STRING(), "] - GET_SCORE_FOR_MODE - sScore.iPreviousScoreStatValue = ",sScore.iPreviousScoreStatValue)
////							PRINTLN("[SMUGGLER] [", GET_CURRENT_VARIATION_STRING(), "] - GET_SCORE_FOR_MODE - iDmgSinceLastUpdate = ",iDmgSinceLastUpdate)
////							
////							//Store distances for next frame
////							sScore.iPreviousScoreStatValue = iStat
////
////							//Update score
////							RETURN iDmgSinceLastUpdate
////						ENDIF
////						
////						PRINTLN("[SMUGGLER] [", GET_CURRENT_VARIATION_STRING(), "] - GET_SCORE_FOR_MODE - iStat = ",iStat)
//					BREAK
//				ENDSWITCH
//			BREAK
//		ENDSWITCH
//		
////		PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - GET_SCORE_FOR_MODE, STAT_GET_CURRENT_SPEED = ", STAT_GET_CURRENT_SPEED())
//	ENDIF

	RETURN 0
ENDFUNC

CONST_INT ciBD_SCORE_UPDATE_DELAY 500

PROC MAINTAIN_COUNT_PLAYER_SCORE()

	IF NOT SHOULD_COUNT_PLAYER_SCORE()
		EXIT
	ENDIF
	
	sScore.fLocalScore += GET_SCORE_FOR_MODE()
	// For MBSV_COLLECT_DJ_HELI see PROCESS_DAMAGE_EVENT
	
	IF playerBD[PARTICIPANT_ID_TO_INT()].fScore != sScore.fLocalScore
		IF HAS_NET_TIMER_EXPIRED(sScore.stScoreBroadcastTimer, ciBD_SCORE_UPDATE_DELAY)
			playerBD[PARTICIPANT_ID_TO_INT()].fScore = sScore.fLocalScore
			PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_COUNT_PLAYER_SCORE - Updated client score to ",playerBD[PARTICIPANT_ID_TO_INT()].fScore)
			RESET_NET_TIMER(sScore.stScoreBroadcastTimer)
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL IS_MISSION_ENTITY_NEAR_DROP_OFF()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
		VEHICLE_INDEX vehID = NET_TO_VEH(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
		IF IS_ENTITY_ALIVE(vehID)
			FLOAT fDistFromDropOff = GET_ENTITY_DISTANCE_FROM_LOCATION(vehID, GET_MEGA_BUSINESS_DROP_OFF_COORDS(serverBD.sMissionEntity[MISSION_ENTITY_ONE].eDropOffs[0], serverBD.nightclubID, GET_MEGA_BUSINESS_SUBVARIATION()), FALSE)
			
			IF fDistFromDropOff < 60.0
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_TRACKIFY_LOGIC()
	IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_ENABLED_TRACKIFY_APP)
		ENABLE_MULTIPLAYER_TRACKIFY_APP(TRUE)
		SET_NUMBER_OF_MULTIPLE_TRACKIFY_TARGETS(1)
		VECTOR vDropOffCoords = GET_MEGA_BUSINESS_DROP_OFF_COORDS(serverBD.sMissionEntity[MISSION_ENTITY_ONE].eDropOffs[0], serverBD.nightclubID, GET_MEGA_BUSINESS_SUBVARIATION())
		SET_TRACKIFY_MULTIPLE_TARGET_VECTOR(0, vDropOffCoords)
		
		PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_TRACKIFY_LOGIC - Enabled the trackify app")
		SET_LOCAL_BIT(eLOCALBITSET_ENABLED_TRACKIFY_APP)
	ELSE
		IF NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_DELIVERY_VEHICLE_NEAR_DROP_OFF)
			IF IS_MISSION_ENTITY_NEAR_DROP_OFF()
				PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_TRACKIFY_LOGIC - Delivery vehicle is near the drop off, setting eCLIENTBITSET_DELIVERY_VEHICLE_NEAR_DROP_OFF")
				SET_CLIENT_BIT(eCLIENTBITSET_DELIVERY_VEHICLE_NEAR_DROP_OFF)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CLOSE_TRACKIFY()
	IF IS_LOCAL_BIT_SET(eLOCALBITSET_ENABLED_TRACKIFY_APP)
		IF IS_PHONE_ONSCREEN()
			HANG_UP_AND_PUT_AWAY_PHONE()
		ELSE
			PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - CLOSE_TRACKIFY - TRACKIFY HAS BEEN DISABLED")
			ENABLE_MULTIPLAYER_TRACKIFY_APP(FALSE)
			CLEAR_LOCAL_BIT(eLOCALBITSET_ENABLED_TRACKIFY_APP)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_FADE_OUT_PLAYER_AND_VEHICLE_DURING_MOCAP()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_HOOKED		RETURN FALSE
	ENDSWITCH
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_FREEZE_PLAYER_VEH_DURING_MOCAP()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_HOOKED		RETURN IS_PLAYER_PILOT_OF_A_CARGOBOB(localPlayerId)
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_SET_VEH_INVINCIBLE_DURING_MOCAP()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_HOOKED		RETURN IS_PLAYER_PILOT_OF_A_CARGOBOB(localPlayerId)
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_WARP_PLAYER_DURING_MOCAP()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_CRASH		RETURN IS_PLAYER_IN_POSSESSION_OF_A_MISSION_ENTITY(localPlayerId)
		CASE MBV_COLLECT_DJ_STOLEN_BAG	RETURN IS_PLAYER_IN_POSSESSION_OF_A_MISSION_ENTITY(localPlayerId)
		CASE MBV_COLLECT_DJ_COLLECTOR	RETURN IS_PLAYER_IN_POSSESSION_OF_A_MISSION_ENTITY(localPlayerId)
		CASE MBV_COLLECT_DJ_HOOKED		RETURN FALSE//IS_PLAYER_IN_POSSESSION_OF_A_MISSION_ENTITY(localPlayerId)
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_WARP_PLAYER_INTO_MISSION_ENTITY_DURING_MOCAP()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_HOOKED		RETURN GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_WARP_PLAYER_DURING_SCRIPTED_CUTSCENE()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_STOLEN_BAG	RETURN IS_PLAYER_IN_POSSESSION_OF_A_MISSION_ENTITY(localPlayerId)
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_FADE_PLAYER_DURING_SCRIPTED_CUTSCENE()
//	SWITCH GET_MEGA_BUSINESS_VARIATION()
//		CASE MBV_COLLECT_DJ_STOLEN_BAG	RETURN IS_PLAYER_IN_POSSESSION_OF_A_MISSION_ENTITY(localPlayerId)
//	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC FADE_PLAYER_DURING_SCENE(BOOL bFade)
	
	IF bLocalPlayerOk
		VEHICLE_INDEX vehID = GET_VEHICLE_PED_IS_IN(localPlayerPed)
		IF IS_VEHICLE_DRIVEABLE(vehID)
			IF bFade
				NETWORK_FADE_OUT_ENTITY(vehID,FALSE,TRUE)
			ELSE
				NETWORK_FADE_IN_ENTITY(vehID,TRUE,FALSE)
			ENDIF
			PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] FADE_PLAYER_DURING_SCENE - bFade = ",bFade)
		ENDIF
	ENDIF

ENDPROC

PROC DISABLE_COLLISION_AND_FREEZE_PLAYER_DURING_SCENE(BOOL bFade)
	
	IF bLocalPlayerOk
		VEHICLE_INDEX vehID = GET_VEHICLE_PED_IS_IN(localPlayerPed)
		IF IS_VEHICLE_DRIVEABLE(vehID)
			SET_ENTITY_COLLISION(vehId,!bFade)
			FREEZE_ENTITY_POSITION(vehID,bFade)
			PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] DISABLE_COLLISION_AND_FREEZE_PLAYER_DURING_SCENE - bFade = ",bFade)
		ENDIF
	ENDIF

ENDPROC

PROC PRINT_PRE_MOCAP_STAGE_HELP()
	IF GET_MEGA_BUSINESS_VARIATION() = MBV_COLLECT_DJ_CRASH
	AND NOT IS_SECOND_MID_MISSION_MOCAP_ABOUT_TO_BEGIN()
	AND IS_PED_IN_ANY_VEHICLE(localPlayerPed)
		IF NOT IS_PLAYER_IN_FIRST_PERSON_CAMERA()
		AND NOT IS_GAMEPLAY_HINT_ACTIVE()
			PRINT_HELP("BBH_CRFOCUS")
		ELSE
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BBH_CRFOCUS")
				CLEAR_HELP()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC NET_SET_PLAYER_CONTROL_FLAGS GET_NSPC_FLAGS_FOR_MID_MISSION_MOCAP(BOOL bInvisible)
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_CRASH			
			IF bInvisible
				RETURN NSPC_FREEZE_POSITION | NSPC_SET_INVISIBLE | NSPC_NO_COLLISION | NSPC_LEAVE_CAMERA_CONTROL_ON
			ENDIF
			RETURN NSPC_FREEZE_POSITION | NSPC_NO_COLLISION | NSPC_LEAVE_CAMERA_CONTROL_ON
	ENDSWITCH
	
	IF bInvisible
		RETURN NSPC_FREEZE_POSITION | NSPC_SET_INVISIBLE | NSPC_NO_COLLISION
	ENDIF
	RETURN NSPC_FREEZE_POSITION | NSPC_NO_COLLISION
ENDFUNC

FUNC BOOL STOP_VEHICLE_AND_REMOVE_CONTROL_FOR_MOCAP(BOOL bInvisible, BOOL bRemoveControl = TRUE)
	BOOL bStoppedVehicle
	
	IF IS_PED_IN_ANY_LAND_VEHICLE(localPlayerPed)
		VEHICLE_INDEX vehID
		vehId = GET_VEHICLE_PED_IS_IN(localPlayerPed)
		
		IF NETWORK_HAS_CONTROL_OF_ENTITY(vehId)
			IF GET_MEGA_BUSINESS_VARIATION() = MBV_COLLECT_DJ_CRASH
				bStoppedVehicle = BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehId, 20.0, 3, 0.2)
			ELSE
				bStoppedVehicle = BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehId, DEFAULT, 2)
			ENDIF
		ELSE
			IF iMocapStage = MOCAP_STAGE_SCENE_FINISHED
				bStoppedVehicle = TRUE
			ENDIF
			IF NOT IS_ENTITY_DEAD(vehId)
			AND ABSF(GET_ENTITY_SPEED(vehId)) <= 0.2
				bStoppedVehicle = TRUE
			ENDIF
		ENDIF
	ELSE
		bStoppedVehicle = TRUE
	ENDIF
	
	IF bStoppedVehicle
		IF bRemoveControl
			IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_REMOVED_PLAYER_CONTROL)
				NET_SET_PLAYER_CONTROL(localPlayerId, FALSE, GET_NSPC_FLAGS_FOR_MID_MISSION_MOCAP(bInvisible))
				SET_LOCAL_BIT(eLOCALBITSET_REMOVED_PLAYER_CONTROL)
			ENDIF
		ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC RESET_SCENE_WARP_DATA()
	CLEAR_CLIENT_BIT(eCLIENTBITSET_NEED_WARP_COORD)
	iMocapWarpStage = 0
	PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - RESET_SCENE_WARP_DATA")
ENDPROC

PROC RESET_MID_MISSION_MOCAP_DATA()
	IF IS_LOCAL_BIT_SET(eLOCALBITSET_OUT_OF_RANGE_FOR_MID_MISSION_MOCAP)
		CLEAR_LOCAL_BIT(eLOCALBITSET_OUT_OF_RANGE_FOR_MID_MISSION_MOCAP)
	ENDIF
	
	IF IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_WATCHED_MID_MISSION_MOCAP)
		sMocap.iStage = 0
		RESET_NET_TIMER(sMocap.stTimer)
		RESET_SCENE_WARP_DATA()
		iMocapStage = 0
		CLEAR_CLIENT_BIT(eCLIENTBITSET_I_WATCHED_MID_MISSION_MOCAP)
	ENDIF
	
	IF IS_LOCAL_BIT_SET(eLOCALBITSET_STARTED_PRE_MOCAP_STAGE)
		CLEAR_LOCAL_BIT(eLOCALBITSET_STARTED_PRE_MOCAP_STAGE)
	ENDIF
	
	IF IS_SERVER_BIT_SET(eSERVERBITSET_MID_MISSION_MOCAP_DONE)
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			CLEAR_SERVER_BIT(eSERVERBITSET_MID_MISSION_MOCAP_DONE)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DOES_MID_MISSION_MOCAP_HAVE_PRE_MOCAP_STAGE()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_CRASH			
			IF NOT IS_SECOND_MID_MISSION_MOCAP_ABOUT_TO_BEGIN()
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_START_MID_MISSION_MOCAP()
	IF NOT bLocalPlayerOk
		RETURN FALSE
	ENDIF
	
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_CRASH			
			IF IS_SECOND_MID_MISSION_MOCAP_ABOUT_TO_BEGIN()
				IF HAS_DIALOGUE_FINISHED_PLAYING(15)
				OR NOT HAS_DIALOGUE_BEEN_TRIGGERED(15)
					IF IS_LOCAL_PLAYER_IN_VEHICLE_WITH_MISSION_ENTITY_HOLDER()
						RETURN TRUE
					ENDIF
				ENDIF
				
				IF GB_GET_LOCAL_PLAYER_GANG_BOSS() != INVALID_PLAYER_INDEX()
				AND IS_CLIENT_BIT_SET(NETWORK_GET_PARTICIPANT_INDEX(GB_GET_LOCAL_PLAYER_GANG_BOSS()), eCLIENTBITSET_STARTED_MID_MISSION_MOCAP)
					RETURN TRUE
				ENDIF
				RETURN FALSE
			ELSE
				IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
					IF IS_PLAYER_IN_POSSESSION_OF_A_MISSION_ENTITY(localPlayerId)
						IF HAS_DIALOGUE_FINISHED_PLAYING(34)
						AND HAS_NET_TIMER_EXPIRED(serverBD.stMocapStartDelay, 1000)
							RETURN TRUE
						ENDIF
					ELIF IS_LOCAL_BIT_SET(eLOCALBITSET_STARTED_PRE_MOCAP_STAGE)
						IF HAS_NET_TIMER_EXPIRED(serverBD.stMocapStartDelay, 2000)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF GB_GET_LOCAL_PLAYER_GANG_BOSS() != INVALID_PLAYER_INDEX()
				AND IS_CLIENT_BIT_SET(NETWORK_GET_PARTICIPANT_INDEX(GB_GET_LOCAL_PLAYER_GANG_BOSS()), eCLIENTBITSET_STARTED_MID_MISSION_MOCAP)
					RETURN TRUE
				ENDIF
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_START_SCRIPTED_CUTSCENE()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_STOLEN_BAG		
			IF IS_LOCAL_PLAYER_IN_VEHICLE_WITH_MISSION_ENTITY_HOLDER()
			AND HAS_NET_TIMER_EXPIRED(serverBD.stScriptCutStartDelay, 1000)
				RETURN TRUE
			ENDIF
			
			IF GB_GET_LOCAL_PLAYER_GANG_BOSS() != INVALID_PLAYER_INDEX()
			AND IS_CLIENT_BIT_SET(NETWORK_GET_PARTICIPANT_INDEX(GB_GET_LOCAL_PLAYER_GANG_BOSS()), eCLIENTBITSET_STARTED_SCRIPTED_CUTSCENE)
				RETURN TRUE
			ENDIF
		RETURN FALSE
	ENDSWITCH
	RETURN TRUE
ENDFUNC

PROC LATCH_VEHICLE_DOORS()
	IF GET_MEGA_BUSINESS_VARIATION() = MBV_STOLEN_SUPPLIES
		IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_LATCHED_DOORS)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
				VEHICLE_INDEX vehID = NET_TO_VEH(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)// MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
					IF IS_MISSION_ENTITY_BIT_SET(MISSION_ENTITY_ONE, eMISSIONENTITYBITSET_I_HAVE_BEEN_ENTERED_AT_LEAST_ONCE)
					AND GET_ENTITY_SPEED(vehID) > 5.00
						FLOAT fDoorOpenRatio = 0.0
						FLOAT fAngleRatioDelta = 0.0
						FLOAT fDoorCloseTime = 1.0 //The time we want it to take for a door to go from fully open to fully closed.
						BOOL bBothDoorsShut = TRUE
						
						//Handle closing the left door.
						IF GET_VEHICLE_DOOR_ANGLE_RATIO(vehID, SC_DOOR_REAR_LEFT) > 0.01
							bBothDoorsShut = FALSE
							fDoorOpenRatio = GET_VEHICLE_DOOR_ANGLE_RATIO(vehID, SC_DOOR_REAR_LEFT)
							fAngleRatioDelta = GET_FRAME_TIME() / (fDoorCloseTime * (1.0 - fDoorOpenRatio)) //Get the amount we should close the door this frame by dividing the last frame time by the amount of time we want to take to finish closing the door.
							SET_VEHICLE_DOOR_CONTROL(vehID, SC_DOOR_REAR_LEFT, DT_DOOR_NO_RESET, FMAX(0.0, fDoorOpenRatio - fAngleRatioDelta))
						ELSE
							SET_VEHICLE_DOOR_SHUT(vehID, SC_DOOR_REAR_LEFT)
							SET_VEHICLE_DOOR_LATCHED(vehID, SC_DOOR_REAR_LEFT, TRUE, TRUE)
						ENDIF
						
						//Handle closing the right door.
						IF GET_VEHICLE_DOOR_ANGLE_RATIO(vehID, SC_DOOR_REAR_RIGHT) > 0.01
							bBothDoorsShut = FALSE
							fDoorOpenRatio = GET_VEHICLE_DOOR_ANGLE_RATIO(vehID, SC_DOOR_REAR_RIGHT)
							fAngleRatioDelta = GET_FRAME_TIME() / (fDoorCloseTime * (1.0 - fDoorOpenRatio)) //Get the amount we should close the door this frame by dividing the last frame time by the amount of time we want to take to finish closing the door.
							SET_VEHICLE_DOOR_CONTROL(vehID, SC_DOOR_REAR_RIGHT, DT_DOOR_NO_RESET, FMAX(0.0, fDoorOpenRatio - fAngleRatioDelta))
						ELSE
							SET_VEHICLE_DOOR_SHUT(vehID, SC_DOOR_REAR_RIGHT)
							SET_VEHICLE_DOOR_LATCHED(vehID, SC_DOOR_REAR_RIGHT, TRUE, TRUE)
						ENDIF
						
						IF bBothDoorsShut
							PRINTLN("[MB_MISSION] - LATCH_VEHICLE_DOORS DONE ")
							SET_LOCAL_BIT(eLOCALBITSET_LATCHED_DOORS)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_CLUB_POPULARITY_CHANGES()
	IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_HANDLED_CLUB_POPULARITY_CHANGES)
	
		IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
			BOOL bPassed = FALSE
			IF GET_END_REASON() = eENDREASON_MISSION_ENTITY_DELIVERED
			OR GET_END_REASON() = eENDREASON_ALL_TARGETS_ELIMINATED
				bPassed = TRUE
			ENDIF
			IF IS_MEGA_BUSINESS_VARIATION_A_CLUB_MANAGEMENT_MISSION(GET_MEGA_BUSINESS_VARIATION())
				IF bPassed
					CLUB_MODIFY_POPULARITY_BY_TYPE(NPM_COMPLETE_MANAGEMENT_MISSION_PASS)
					SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_CLUB_MANAGEMENT)
					PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - HANDLE_CLUB_POPULARITY_CHANGES - Passed a club management mission, increasing popularity by ", GET_NIGHTCLUB_POPULARITY_MODIFIER_VALUE(NPM_COMPLETE_MANAGEMENT_MISSION_PASS),"%")
				ELSE
					PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - HANDLE_CLUB_POPULARITY_CHANGES - Failed a club management mission, not adjusting popularity")
				ENDIF
			
			ELIF IS_MB_VARIATION_A_DJ_COLLECTION_MISSION(GET_MEGA_BUSINESS_VARIATION())
				IF NOT HAS_LOCAL_PLAYER_COMPLETED_NIGHTCLUB_SETUP_MISSION(GET_MEGA_BUSINESS_VARIATION())
				AND bPassed
					CLUB_MODIFY_POPULARITY_BY_TYPE(NPM_COMPLETE_DJ_COLLECT_MISSION_PASS)
					PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - HANDLE_CLUB_POPULARITY_CHANGES - Completed DJ mission, setting club popularity to 100%")
				ELSE
					PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - HANDLE_CLUB_POPULARITY_CHANGES - Failed DJ mission or have already completed it, not adjusting popularity")
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - HANDLE_CLUB_POPULARITY_CHANGES - I'm not a boss, not modifying the popularity of my club")
		ENDIF
		
		SET_LOCAL_BIT(eLOCALBITSET_HANDLED_CLUB_POPULARITY_CHANGES)
	ENDIF
ENDPROC

SCRIPT_TIMER missionEntityDestroyableDelayTimer
FUNC BOOL HAS_SELL_DESTROYABLE_DELAY_TIMER_EXPIRED()
	RETURN HAS_NET_TIMER_EXPIRED(missionEntityDestroyableDelayTimer, 15000)
ENDFUNC

FUNC BOOL SHOULD_MISSION_ENTITIES_SPAWN_INVINCIBLE()

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_SUPPLY_RUN
		CASE MBV_BLIMP_PROMOTION
			RETURN TRUE
		
	ENDSWITCH

	RETURN IS_MEGA_BUSINESS_VARIATION_A_SELL_MISSION(GET_MEGA_BUSINESS_VARIATION())
ENDFUNC

FUNC BOOL SHOULD_MISSION_ENTITIES_BE_INVINCIBLE()

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_HOOKED
			IF HOOKED_LIMO_IN_PROTECT_STAGE()
			
				RETURN TRUE
			ENDIF
		BREAK		
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL DO_I_WANT_MISSION_ENTITY_TO_BE_DESTROYABLE(INT iMissionEntity)//, BOOL& bWaitingForRespawnDelay)
	BOOL bSetDestroyable
	
	IF SHOULD_MISSION_ENTITIES_BE_INVINCIBLE()
	
		RETURN FALSE
	ENDIF
	
	IF NOT SHOULD_MISSION_ENTITIES_SPAWN_INVINCIBLE()
		RETURN TRUE
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
	AND NOT IS_ENTITY_DEAD(NET_TO_ENT(serverBD.sMissionEntity[iMissionEntity].netId))
		
//		#IF IS_DEBUG_BUILD
//		IF bDoDestroyablePrints
//			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] [GR_DESTROYABLE] - DO_I_WANT_MISSION_ENTITY_TO_BE_DESTROYABLE(", iMissionEntity, ") - Mission entity exists and is not dead.")
//		ENDIF
//		#ENDIF
		
		IF HAS_SELL_DESTROYABLE_DELAY_TIMER_EXPIRED()
//			#IF IS_DEBUG_BUILD
//			IF bDoDestroyablePrints
//				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] [GR_DESTROYABLE] - DO_I_WANT_MISSION_ENTITY_TO_BE_DESTROYABLE(", iMissionEntity, ") - Sell mission destroyable delay timer has expired.")
//			ENDIF
//			#ENDIF
			
			IF NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_DELIVERED) 
//				IF IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_SOMEONE_IS_NEAR_ME)
				IF IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_I_HAVE_BEEN_ENTERED_AT_LEAST_ONCE)
//					#IF IS_DEBUG_BUILD
//					IF bDoDestroyablePrints
//						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] [GR_DESTROYABLE] - DO_I_WANT_MISSION_ENTITY_TO_BE_DESTROYABLE(", iMissionEntity, ") - Someone has been near Sell Mission entity or collected it. Set as destroyable.")
//					ENDIF
//					#ENDIF
					
					bSetDestroyable = TRUE
				ENDIF
			ENDIF
		ENDIF

	ENDIF
	
//	#IF IS_DEBUG_BUILD
//	IF bDoDestroyablePrints
//		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] [GR_DESTROYABLE] - DO_I_WANT_MISSION_ENTITY_TO_BE_DESTROYABLE(", iMissionEntity, ") = ", GET_STRING_FROM_BOOL(bSetDestroyable))
//	ENDIF
//	#ENDIF
	
	RETURN bSetDestroyable
ENDFUNC

PROC MAINTAIN_SHOULD_MISSION_ENTITY_BE_DESTROYABLE_SERVER(INT iMissionEntity)
//	BOOL bWaitingForRespawnDelay
	IF NOT DO_I_WANT_MISSION_ENTITY_TO_BE_DESTROYABLE(iMissionEntity)//, bWaitingForRespawnDelay)
//		IF NOT bWaitingForRespawnDelay
//			#IF IS_DEBUG_BUILD
//			IF bDoDestroyablePrints
//				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] [GR_DESTROYABLE] - MAINTAIN_SHOULD_MISSION_ENTITY_BE_DESTROYABLE_SERVER(", iMissionEntity, ") = FALSE.")
//			ENDIF
//			#ENDIF
		
//			serverBD.iSpawnedAtPosix[iMissionEntity] = GET_CLOUD_TIME_AS_INT()
			CLEAR_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_SET_AS_DESTROYABLE)
//		ELSE
////			#IF IS_DEBUG_BUILD
////			IF bDoDestroyablePrints
////				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] [GR_DESTROYABLE] - MAINTAIN_SHOULD_MISSION_ENTITY_BE_DESTROYABLE_SERVER(", iMissionEntity, ") - bWaitingForRespawnDelay = ", GET_STRING_FROM_BOOL(bWaitingForRespawnDelay))
////			ENDIF
////			#ENDIF
//		ENDIF
	ELSE
//		#IF IS_DEBUG_BUILD
//		IF bDoDestroyablePrints
//			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] [GR_DESTROYABLE] - MAINTAIN_SHOULD_MISSION_ENTITY_BE_DESTROYABLE_SERVER(", iMissionEntity, ") = TRUE.")
//		ENDIF
//		#ENDIF
			
		SET_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_SET_AS_DESTROYABLE)
	ENDIF
ENDPROC

PROC MAINTAIN_SHOULD_MISSION_ENTITY_BE_DESTROYABLE_CLIENT(INT iMissionEntity)
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
	AND NOT IS_ENTITY_DEAD(NET_TO_ENT(serverBD.sMissionEntity[iMissionEntity].netId))
		IF IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_SET_AS_DESTROYABLE)
			IF NOT GET_ENTITY_CAN_BE_DAMAGED(NET_TO_ENT(serverBD.sMissionEntity[iMissionEntity].netId))
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
				OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sMissionEntity[iMissionEntity].netId) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
					IF TAKE_CONTROL_OF_NET_ID(serverBD.sMissionEntity[iMissionEntity].netId)

						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] [GR_DESTROYABLE] - MAINTAIN_SHOULD_MISSION_ENTITY_BE_DESTROYABLE_CLIENT(", iMissionEntity, ") - SET_ENTITY_INVINCIBLE to FALSE.")

						SET_ENTITY_INVINCIBLE(NET_TO_ENT(serverBD.sMissionEntity[iMissionEntity].netId), FALSE)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF GET_ENTITY_CAN_BE_DAMAGED(NET_TO_ENT(serverBD.sMissionEntity[iMissionEntity].netId))
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
				OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sMissionEntity[iMissionEntity].netId) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
					IF TAKE_CONTROL_OF_NET_ID(serverBD.sMissionEntity[iMissionEntity].netId)

						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] [GR_DESTROYABLE] - MAINTAIN_SHOULD_MISSION_ENTITY_BE_DESTROYABLE_CLIENT(", iMissionEntity, ") - SET_ENTITY_INVINCIBLE to TRUE.")

						SET_ENTITY_INVINCIBLE(NET_TO_ENT(serverBD.sMissionEntity[iMissionEntity].netId), TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_ATTACHING_MISSION_ENTITY_VEHICLE_LOGIC(INT iMissionEntity)
	IF NOT DOES_VEHICLE_NEED_ATTACHED(iMissionEntity, TRUE)
		EXIT
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
		VEHICLE_INDEX vehID = NET_TO_VEH(serverBD.sMissionEntity[iMissionEntity].netId)
		NETWORK_INDEX parentNetId = GET_PARENT_VEHICLE_NEEDS_ATTACHED_TO(iMissionEntity, TRUE)
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(parentNetId)
		AND IS_NET_VEHICLE_DRIVEABLE(parentNetId)
		AND NOT IS_ENTITY_DEAD(vehID)
			VEHICLE_INDEX parentVehId = NET_TO_VEH(parentNetId)
			IF IS_THIS_MODEL_A_TRUCK_CAB(GET_ENTITY_MODEL(vehID))
				IF NOT IS_VEHICLE_ATTACHED_TO_TRAILER(vehID)
					ATTACH_VEHICLE_TO_TRAILER(vehID, parentVehId, 0.1)
					PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_ATTACHING_MISSION_ENTITY_VEHICLE_LOGIC - Called ATTACH_VEHICLE_TO_TRAILER on mission entity #", iMissionEntity)
				ELSE
					SET_MISSION_ENTITY_CLIENT_BIT(iMissionEntity, eMISSIONENTITYCLIENTBITSET_ATTACHED_TO_VEHICLE)
					PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_ATTACHING_MISSION_ENTITY_VEHICLE_LOGIC - Attached mission entity #", iMissionEntity, " to trailer.")
				ENDIF
			ELSE
				IF NOT IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(vehID)
					NETWORK_ALLOW_REMOTE_ATTACHMENT_MODIFICATION(parentVehId, TRUE)
					ATTACH_ENTITY_TO_ENTITY(vehID, parentVehId, GET_ENTITY_BONE_INDEX_BY_NAME(parentVehId, "chassis_dummy"), GET_CREATE_VEHICLE_FROM_OFFSET_OFFSET(iMissionEntity, TRUE), GET_VEHICLE_ATTACH_ROTATION(iMissionEntity, TRUE), FALSE, FALSE, FALSE)
					PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_ATTACHING_MISSION_ENTITY_VEHICLE_LOGIC - Called ATTACH_ENTITY_TO_ENTITY on mission entity #", iMissionEntity)
				ELSE
					SET_MISSION_ENTITY_CLIENT_BIT(iMissionEntity, eMISSIONENTITYCLIENTBITSET_ATTACHED_TO_VEHICLE)
					PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_ATTACHING_MISSION_ENTITY_VEHICLE_LOGIC - Attached mission entity #", iMissionEntity, " to vehicle.")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_MISSION_PED_INDEX_FOR_CUTSCENE(INT iCutscenePed)

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_STOLEN_BAG
			SWITCH iCutscenePed
				CASE 0		RETURN 0
				CASE 1		RETURN 1
			ENDSWITCH
		BREAK
		CASE MBV_COLLECT_DJ_HOOKED
			RETURN 0
		
	ENDSWITCH

	RETURN -1

ENDFUNC

FUNC PED_INDEX GET_MISSION_PED_FOR_CUTSCENE(INT iCutscenePed)

	PED_INDEX ped
	INT iPed = GET_MISSION_PED_INDEX_FOR_CUTSCENE(iCutscenePed)

	IF iPed != -1
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[iPed].netId)
			IF NOT IS_NET_PED_INJURED(serverBD.sPed[iPed].netId)
				ped = NET_TO_PED(serverBD.sPed[iPed].netId)
			ENDIF
		ENDIF
	ENDIF
	
	RETURN ped

ENDFUNC

FUNC VECTOR GET_MISSION_PED_COORDS_FOR_CUTSCENE(INT iCutscenePed)

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_STOLEN_BAG
			SWITCH cutsceneStruct.iCurrentScene
				CASE 0
					SWITCH iCutscenePed
						CASE 0		RETURN <<-1275.5499, 315.6674, 64.5118>>
						CASE 1		RETURN <<-1273.9683, 315.2585, 64.5118>>
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH iCutscenePed
						CASE 0		RETURN <<-1285.0988, 292.7097, 63.8299>>
						CASE 1		RETURN <<-1283.9771, 295.0145, 63.8656>>
					ENDSWITCH
				BREAK 
			ENDSWITCH
		BREAK
	ENDSWITCH

	RETURN <<0.0,0.0,0.0>>

ENDFUNC

FUNC FLOAT GET_MISSION_PED_HEADING_FOR_CUTSCENE(INT iCutscenePed)

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_STOLEN_BAG
			SWITCH cutsceneStruct.iCurrentScene
				CASE 0
					SWITCH iCutscenePed
						CASE 0		RETURN 153.1936
						CASE 1		RETURN 145.7609
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH iCutscenePed
						CASE 0		RETURN 334.0773
						CASE 1		RETURN 146.3893
					ENDSWITCH
				BREAK 
			ENDSWITCH
		BREAK
	ENDSWITCH

	RETURN 0.0

ENDFUNC

FUNC VEHICLE_SEAT GET_MISSION_PED_VEHICLE_SEAT_FOR_CUTSCENE(INT iCutscenePed)

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_STOLEN_BAG
			SWITCH iCutscenePed
				CASE 0		RETURN VS_BACK_RIGHT
				CASE 1		RETURN VS_BACK_LEFT 
			ENDSWITCH
		BREAK
	ENDSWITCH

	RETURN VS_DRIVER

ENDFUNC

FUNC ENTER_EXIT_VEHICLE_FLAGS GET_MISSION_PED_VEHICLE_ENTRY_POINT_FOR_CUTSCENE(INT iCutscenePed)

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_STOLEN_BAG
			SWITCH iCutscenePed
				CASE 0		RETURN ECF_WARP_ENTRY_POINT | ECF_USE_RIGHT_ENTRY
				CASE 1		RETURN ECF_WARP_ENTRY_POINT | ECF_USE_LEFT_ENTRY
			ENDSWITCH
		BREAK
	ENDSWITCH

	RETURN ECF_WARP_ENTRY_POINT

ENDFUNC

FUNC VECTOR GET_GOTO_COORDS_FOR_CUTSCENE_PEDS(INT iCutscenePed)

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_STOLEN_BAG
			SWITCH iCutscenePed
				CASE 0		RETURN <<-1282.4362, 303.1669, 63.9592>>
				CASE 1		RETURN <<-1280.5026, 303.1587, 63.9743>>
			ENDSWITCH
		BREAK
	ENDSWITCH

	RETURN <<0.0,0.0,0.0>>

ENDFUNC

FUNC BOOL SHOULD_CUTSCENE_PEDS_BE_VISIBLE_AT_START()

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_STOLEN_BAG
			RETURN FALSE
	ENDSWITCH

	RETURN TRUE
	
ENDFUNC

FUNC BOOL CLONE_PEDS_FOR_CUTSCENE()

	INT iPed
	REPEAT NUM_CUTSCENE_PEDS iPed
		IF NOT DOES_ENTITY_EXIST(cutscenePed[iPed])
			IF CUTSCENE_HELP_CLONE_PED(cutscenePed[iPed],GET_MISSION_PED_FOR_CUTSCENE(iPed))
				SET_ENTITY_COORDS(cutscenePed[iPed],GET_MISSION_PED_COORDS_FOR_CUTSCENE(iPed))
				SET_ENTITY_HEADING(cutscenePed[iPed],GET_MISSION_PED_HEADING_FOR_CUTSCENE(iPed))
				IF NOT SHOULD_CUTSCENE_PEDS_BE_VISIBLE_AT_START()
					SET_ENTITY_VISIBLE(cutscenePed[iPed],FALSE)
				ENDIF
//				SET_ENTITY_VISIBLE_IN_CUTSCENE(cutscenePed[iPed],TRUE)
				TASK_GO_STRAIGHT_TO_COORD(cutscenePed[iPed],GET_GOTO_COORDS_FOR_CUTSCENE_PEDS(iPed),PEDMOVE_WALK)
				PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] CLONE_PEDS_FOR_CUTSCENE - Cloned and tasked ped, ",iPed)
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT

	RETURN TRUE

ENDFUNC

FUNC VEHICLE_INDEX GET_VEHICLE_FOR_SCRIPTED_CUTSCENE()

	VEHICLE_INDEX vehId
	
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_HOOKED		
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[ciHOOKED_VEH_CARGOBOB].netId)
				vehId = NET_TO_VEH(serverBD.sVehicle[ciHOOKED_VEH_CARGOBOB].netId)
			ENDIF
		BREAK
		CASE MBV_COLLECT_DJ_STOLEN_BAG
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
				vehId = NET_TO_VEH(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
			ENDIF
		BREAK
	ENDSWITCH

	RETURN vehId
ENDFUNC

FUNC VECTOR GET_VEH_SCRIPTED_CUTSCENE_COORDS()

	VECTOR vReturn 

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_HOOKED
			vReturn = CARGBOB_POS_AFTER_MOCAP()
			vReturn.z += 7.00
		BREAK
		CASE MBV_COLLECT_DJ_STOLEN_BAG
			vReturn = <<-1285.4291, 294.4042, 63.8617>>
		BREAK
	ENDSWITCH

	RETURN vReturn
ENDFUNC

FUNC FLOAT GET_VEH_SCRIPTED_CUTSCENE_HEADING()

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_HOOKED 				RETURN CARGBOB_HEADING_AFTER_MOCAP()
		CASE MBV_COLLECT_DJ_STOLEN_BAG			RETURN 59.6599
	ENDSWITCH

	RETURN 0.0
ENDFUNC

PROC TASK_CUTSCENE_CLONE_VEHICLE_ON_SPAWN()

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_HOOKED
			IF NOT IS_ENTITY_DEAD(cutsceneVehHelpStruct.vehicle)
			AND NOT IS_ENTITY_DEAD(cutsceneVehHelpStruct.vehPassengers[0])
			
				IF (GET_SCRIPT_TASK_STATUS(cutsceneVehHelpStruct.vehPassengers[0], SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(cutsceneVehHelpStruct.vehPassengers[0], SCRIPT_TASK_VEHICLE_MISSION) != WAITING_TO_START_TASK)
				
					VECTOR VecCoors 
					VecCoors = CARGBOB_POS_AFTER_MOCAP()
//					VecCoors.Z = VecCoors.Z + 1.5
					
					SET_HELI_BLADES_FULL_SPEED(cutsceneVehHelpStruct.vehicle)
			
					TASK_HELI_MISSION(cutsceneVehHelpStruct.vehPassengers[0], 	// 	PedIndex
										cutsceneVehHelpStruct.vehicle, 			// 	HeliIndex
										NULL, 									// 	TargetVehicleIndex
										NULL, 									// 	TargetPedIndex
										VecCoors, 								// 	VecCoors
										MISSION_LAND, 							// 	VEHICLE_MISSION
										25.00, 									//	CruiseSpeed
										-1, 									//	TargetReachedDist
										CARGBOB_HEADING_AFTER_MOCAP(), 			//	HeliOrientation
										0, 										//	FlightHeight
										0, 										//	MinHeightAboveTerrain
										DEFAULT, 								//	fSlowDownDistance
										HF_StartEngineImmediately | HF_AttainRequestedOrientation | HF_LandOnArrival | HF_IgnoreHiddenEntitiesDuringLand )//| HF_ForceHeightMapAvoidance | HF_MaintainHeightAboveTerrain)				//	HELIMODE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

FUNC CUTSCENE_HELP_VEHICLE_CLONE_FLAGS GET_VEHICLE_CLONE_FLAGS() 

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_HOOKED				RETURN chVEHICLE_CLONE_DEAD_PED_PASSENGERS
		CASE MBV_COLLECT_DJ_STOLEN_BAG			RETURN chVEHICLE_CLONE_ONLY_PLAYER_PASSENGERS
	ENDSWITCH

	RETURN chVEHICLE_CLONE_TRAILER

ENDFUNC

FUNC BOOL SHOULD_FREEZE_CLONE_VEHICLE_ON_SPAWN()

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_STOLEN_BAG
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE

ENDFUNC

FUNC BOOL CLONE_VEHICLES_AND_PASSENGERS_FOR_CUTSCENE()

	VEHICLE_INDEX vehToClone
	vehToClone = GET_VEHICLE_FOR_SCRIPTED_CUTSCENE()
	
	VECTOR vCoord = GET_VEH_SCRIPTED_CUTSCENE_COORDS()
	FLOAT fHeading = GET_VEH_SCRIPTED_CUTSCENE_HEADING()

	IF DOES_ENTITY_EXIST(vehToClone)
	AND NOT IS_ENTITY_DEAD(vehToClone)
	
		IF CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS(vehToClone, cutsceneVehHelpStruct, GET_VEHICLE_CLONE_FLAGS())
		AND DOES_ENTITY_EXIST(cutsceneVehHelpStruct.vehPassengers[0]) // url:bugstar:5138927, CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS does not appear to wait for peds to clone
			IF SHOULD_FREEZE_CLONE_VEHICLE_ON_SPAWN()
				FREEZE_ENTITY_POSITION(cutsceneVehHelpStruct.vehicle, TRUE)
				SET_ENTITY_COLLISION(cutsceneVehHelpStruct.vehicle, FALSE)
			ELSE
				SET_ENTITY_COLLISION(cutsceneVehHelpStruct.vehicle, TRUE)
			ENDIF
			SET_ENTITY_VISIBLE(cutsceneVehHelpStruct.vehicle, FALSE)
			SET_ENTITY_INVINCIBLE(cutsceneVehHelpStruct.vehicle, TRUE)
			SET_ENTITY_COORDS(cutsceneVehHelpStruct.vehicle, vCoord)
			SET_ENTITY_HEADING(cutsceneVehHelpStruct.vehicle, fHeading)
			SET_VEHICLE_ENGINE_ON(cutsceneVehHelpStruct.vehicle, TRUE, TRUE)
			
			TASK_CUTSCENE_CLONE_VEHICLE_ON_SPAWN()
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CLONING_COMPLETE()

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_HOOKED		
			RETURN CLONE_VEHICLES_AND_PASSENGERS_FOR_CUTSCENE()
		CASE MBV_COLLECT_DJ_STOLEN_BAG
			RETURN CLONE_PEDS_FOR_CUTSCENE()
			AND CLONE_VEHICLES_AND_PASSENGERS_FOR_CUTSCENE()
			AND HAS_NET_TIMER_EXPIRED(cutsceneStartTimer,500)
	ENDSWITCH

	RETURN CLONE_PEDS_FOR_CUTSCENE()
ENDFUNC

PROC MAKE_PED_ENTER_VEHICLE_FOR_CUTSCENE(INT iPed)

	IF DOES_ENTITY_EXIST(cutscenePed[iPed])
		IF NOT IS_PED_INJURED(cutscenePed[iPed])
			IF DOES_ENTITY_EXIST(cutsceneVehHelpStruct.vehicle)
			AND IS_VEHICLE_DRIVEABLE(cutsceneVehHelpStruct.vehicle)
				IF NOT IS_PED_IN_VEHICLE(cutscenePed[iPed],cutsceneVehHelpStruct.vehicle)
					IF (GET_SCRIPT_TASK_STATUS(cutscenePed[iPed], SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(cutscenePed[iPed], SCRIPT_TASK_ENTER_VEHICLE) != WAITING_TO_START_TASK)
						TASK_ENTER_VEHICLE(cutscenePed[iPed],cutsceneVehHelpStruct.vehicle,DEFAULT,GET_MISSION_PED_VEHICLE_SEAT_FOR_CUTSCENE(iPed),PEDMOVE_WALK,GET_MISSION_PED_VEHICLE_ENTRY_POINT_FOR_CUTSCENE(iPed))
						PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] MAKE_PEDS_ENTER_VEHICLE_FOR_CUTSCENE - Tasked ped, ",iPed)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL MAKE_PEDS_ENTER_VEHICLE_FOR_CUTSCENE()

	MAKE_PED_ENTER_VEHICLE_FOR_CUTSCENE(0)
	IF HAS_NET_TIMER_EXPIRED(cutsceneEnterVehTimer,1000)
		MAKE_PED_ENTER_VEHICLE_FOR_CUTSCENE(1)
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

PROC DELETE_PEDS_FOR_CUTSCENE()

	INT iPed
	REPEAT NUM_CUTSCENE_PEDS iPed
		IF DOES_ENTITY_EXIST(cutscenePed[iPed])
			DELETE_PED(cutscenePed[iPed])
			PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] DELETE_PEDS_FOR_CUTSCENE - Deleted ped, ",iPed)
		ENDIF
	ENDREPEAT

ENDPROC

FUNC VEHICLE_INDEX GET_MISSION_VEHICLE_FOR_CUTSCENE()

	VEHICLE_INDEX veh
	INT iVeh = 0

	IF iVeh != -1
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[iVeh].netId)
			IF IS_VEHICLE_DRIVEABLE(serverBD.sMissionEntity[iVeh].netId)
				veh = NET_TO_VEH(serverBD.sMissionEntity[iVeh].netId)
			ENDIF
		ENDIF
	ENDIF
	
	RETURN veh

ENDFUNC

FUNC STRING GET_AUDIO_SCENE_FOR_SCRIPTED_CUTSCENE()

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_STOLEN_BAG			RETURN "DLC_BTL_Collect_DJ_Exterior_Shot_Scene"
	ENDSWITCH

	RETURN ""

ENDFUNC

PROC START_AUDIO_SCENE_FOR_SCRIPTED_CUTSCENE()

	IF NOT IS_STRING_NULL_OR_EMPTY(GET_AUDIO_SCENE_FOR_SCRIPTED_CUTSCENE())
		START_AUDIO_SCENE(GET_AUDIO_SCENE_FOR_SCRIPTED_CUTSCENE())
		PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] START_AUDIO_SCENE_FOR_SCRIPTED_CUTSCENE - Started.")
	ENDIF

ENDPROC

PROC STOP_AUDIO_SCENE_FOR_SCRIPTED_CUTSCENE()

	IF NOT IS_STRING_NULL_OR_EMPTY(GET_AUDIO_SCENE_FOR_SCRIPTED_CUTSCENE())
		STOP_AUDIO_SCENE(GET_AUDIO_SCENE_FOR_SCRIPTED_CUTSCENE())
		PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] STOP_AUDIO_SCENE_FOR_SCRIPTED_CUTSCENE - Stopped.")
	ENDIF

ENDPROC

FUNC STRING GET_AUDIO_SCENE_FOR_MOCAP_CUTSCENE()

	RETURN "MUTES_RADIO_SCENE"

ENDFUNC

PROC START_AUDIO_SCENE_FOR_MOCAP_CUTSCENE()

	IF NOT IS_STRING_NULL_OR_EMPTY(GET_AUDIO_SCENE_FOR_MOCAP_CUTSCENE())
		START_AUDIO_SCENE(GET_AUDIO_SCENE_FOR_MOCAP_CUTSCENE())
		PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] START_AUDIO_SCENE_FOR_MOCAP_CUTSCENE - Started.")
	ENDIF

ENDPROC

PROC STOP_AUDIO_SCENE_FOR_MOCAP_CUTSCENE()

	IF NOT IS_STRING_NULL_OR_EMPTY(GET_AUDIO_SCENE_FOR_MOCAP_CUTSCENE())
		STOP_AUDIO_SCENE(GET_AUDIO_SCENE_FOR_MOCAP_CUTSCENE())
		PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] STOP_AUDIO_SCENE_FOR_MOCAP_CUTSCENE - Stopped.")
	ENDIF

ENDPROC

FUNC INT SCRIPTED_CUT_DURATION(INT iScene)

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_HOOKED			RETURN 6000
		CASE MBV_COLLECT_DJ_STOLEN_BAG
			SWITCH iScene
				CASE 0		RETURN 6000
				CASE 1		RETURN 3000
			ENDSWITCH
		BREAK
	ENDSWITCH

	RETURN 6000
ENDFUNC

FUNC STRING SCRIPTED_CUT_NAME(INT iScene)

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_HOOKED			RETURN "Landing Cargobob"
		CASE MBV_COLLECT_DJ_STOLEN_BAG
			SWITCH iScene
				CASE 0						RETURN "Leave Hotel"
				CASE 1						RETURN "Bonnet Cam"
			ENDSWITCH
		BREAK
	ENDSWITCH

	RETURN ""
ENDFUNC

FUNC VECTOR SC_CAM_START(INT iScene)

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_HOOKED			RETURN <<-712.0115, -1447.7743, 4.8319>>
		CASE MBV_COLLECT_DJ_STOLEN_BAG
			SWITCH iScene
				CASE 0						RETURN <<-1277.0133, 306.4725, 64.6837>>
				CASE 1						RETURN <<-1286.0997, 294.7434, 65.0228>>
			ENDSWITCH
		BREAK
	ENDSWITCH

	RETURN <<0.0,0.0,0.0>>
ENDFUNC

FUNC VECTOR SC_ROT_START(INT iScene)

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_HOOKED			RETURN <<27.7119, 0.0237, -87.5427>>
		CASE MBV_COLLECT_DJ_STOLEN_BAG
			SWITCH iScene
				CASE 0						RETURN <<8.1260, -0.0130, -14.8351>>
				CASE 1						RETURN <<-5.5723, 0.1498, -123.4552>>
			ENDSWITCH
		BREAK
	ENDSWITCH

	RETURN <<0.0,0.0,0.0>>
ENDFUNC

FUNC FLOAT SC_FOV_START(INT iScene)

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_HOOKED			RETURN 41.7028
		CASE MBV_COLLECT_DJ_STOLEN_BAG
			SWITCH iScene
				CASE 0						RETURN 34.2876
				CASE 1						RETURN 34.2876
			ENDSWITCH
		BREAK
	ENDSWITCH


	RETURN 50.0
ENDFUNC

FUNC VECTOR SC_CAM_FINISH(INT iScene)

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_HOOKED			RETURN <<-712.0348, -1445.1368, 4.7980>>
		CASE MBV_COLLECT_DJ_STOLEN_BAG
			SWITCH iScene
				CASE 0						RETURN <<-1277.9519, 306.8006, 64.6835>>
				CASE 1						RETURN <<-1286.0997, 294.7434, 65.0228>>
			ENDSWITCH
		BREAK
	ENDSWITCH


	RETURN <<0.0,0.0,0.0>>
ENDFUNC

FUNC VECTOR SC_ROT_FINISH(INT iScene)

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_HOOKED			RETURN <<8.8482, 0.0237, -96.5737>>
		CASE MBV_COLLECT_DJ_STOLEN_BAG
			SWITCH iScene
				CASE 0						RETURN <<7.5997, -0.1255, -17.3697>>
				CASE 1						RETURN <<-5.5723, 0.1498, -123.4552>>
			ENDSWITCH
		BREAK
	ENDSWITCH

	RETURN <<0.0,0.0,0.0>>
ENDFUNC

FUNC FLOAT SC_FOV_FINISH(INT iScene)

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_HOOKED			RETURN 41.7028
		CASE MBV_COLLECT_DJ_STOLEN_BAG
			SWITCH iScene
				CASE 0						RETURN 34.2876
				CASE 1						RETURN 34.2876
			ENDSWITCH
		BREAK
	ENDSWITCH

	RETURN 50.0
ENDFUNC

FUNC FLOAT SCRIPTED_CAM_SHAKE(INT iScene)

	UNUSED_PARAMETER(iScene)

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_HOOKED			RETURN 0.2
		CASE MBV_COLLECT_DJ_STOLEN_BAG		RETURN 1.0
	ENDSWITCH

	RETURN 1.0
ENDFUNC

FUNC BOOL SHOULD_CLEAN_UP_SCRIPTED_CUTSCENE_CLONES_DURING_MOCAP()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_HOOKED			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_HOLD_SCRIPTED_CUTSCENE_ON_END()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_HOOKED			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC INT GET_NUM_SCRIPTED_CUTSCENE_SCENES()

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_STOLEN_BAG			RETURN 2
	ENDSWITCH
	
	RETURN 1

ENDFUNC

PROC MAINTAIN_SCRIPTED_CUTSCENE_SCENES()

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_STOLEN_BAG
			SWITCH cutsceneStruct.iCurrentScene
				CASE 0
					SET_ENTITY_VISIBLE(cutscenePed[0],TRUE)
					SET_ENTITY_VISIBLE(cutscenePed[1],TRUE)
				BREAK
				CASE 1
					IF IS_VEHICLE_DRIVEABLE(cutsceneVehHelpStruct.vehicle)
						SET_ENTITY_VISIBLE(cutsceneVehHelpStruct.vehicle, TRUE)
						FREEZE_ENTITY_POSITION(cutsceneVehHelpStruct.vehicle, FALSE)
						SET_ENTITY_COLLISION(cutsceneVehHelpStruct.vehicle, TRUE)
						SET_ENTITY_COORDS(cutsceneVehHelpStruct.vehicle,GET_VEH_SCRIPTED_CUTSCENE_COORDS())
						SET_ENTITY_HEADING(cutsceneVehHelpStruct.vehicle,GET_VEH_SCRIPTED_CUTSCENE_HEADING())
						MAKE_PEDS_ENTER_VEHICLE_FOR_CUTSCENE()
					ENDIF
					//FADE_PLAYER_DURING_SCENE(FALSE)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH

ENDPROC

FUNC CAMERA_GRAPH_TYPE SCRIPTED_CUT_GRAPH_TYPE_POS()

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_HOOKED			RETURN GRAPH_TYPE_QUADRATIC_EASE_OUT
	ENDSWITCH

	RETURN GRAPH_TYPE_LINEAR
ENDFUNC

FUNC CAMERA_GRAPH_TYPE SCRIPTED_CUT_GRAPH_TYPE_ROT()

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_HOOKED			RETURN GRAPH_TYPE_QUADRATIC_EASE_OUT
	ENDSWITCH

	RETURN GRAPH_TYPE_LINEAR
ENDFUNC

FUNC BOOL RUN_SCRIPTED_CUTSCENE()

	IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_SETUP_SCRIPTED_CUTSCENE)
		IF CLONING_COMPLETE()
		
			INT iScene
			REPEAT GET_NUM_SCRIPTED_CUTSCENE_SCENES() iScene
				SIMPLE_CUTSCENE_ADD_SCENE(cutsceneStruct, SCRIPTED_CUT_DURATION(iScene), SCRIPTED_CUT_NAME(iScene), 
										SC_CAM_START(iScene), SC_ROT_START(iScene), SC_FOV_START(iScene), 
										SC_CAM_FINISH(iScene), SC_ROT_FINISH(iScene), SC_FOV_FINISH(iScene), 
										SCRIPTED_CAM_SHAKE(iScene), DEFAULT, DEFAULT, SCRIPTED_CUT_GRAPH_TYPE_POS(), SCRIPTED_CUT_GRAPH_TYPE_ROT())
			ENDREPEAT
									
			SIMPLE_CUTSCENE_START(cutsceneStruct)
			START_AUDIO_SCENE_FOR_SCRIPTED_CUTSCENE()
			CLEAR_AREA_OF_PROJECTILES(SC_CAM_START(0),25.0)
			SET_LOCAL_BIT(eLOCALBITSET_SETUP_SCRIPTED_CUTSCENE)
		ENDIF
	ELSE
		IF cutsceneStruct.bPlaying
		
			IF GET_MEGA_BUSINESS_VARIATION() = MBV_COLLECT_DJ_HOOKED
				SET_ENTITY_VISIBLE(cutsceneVehHelpStruct.vehicle, TRUE)
			ENDIF
			
			IF SHOULD_HOLD_SCRIPTED_CUTSCENE_ON_END()
				SET_BITMASK_ENUM_AS_ENUM(cutsceneStruct.iSetupBM, SCS_DO_NOT_RETURN_PLAYER_CONTROL)
			ENDIF
			
			SIMPLE_CUTSCENE_MAINTAIN(cutsceneStruct, SHOULD_HOLD_SCRIPTED_CUTSCENE_ON_END(), (NOT SHOULD_HOLD_SCRIPTED_CUTSCENE_ON_END()))

			MAINTAIN_SCRIPTED_CUTSCENE_SCENES()
		ELSE
			IF NOT SHOULD_CLEAN_UP_SCRIPTED_CUTSCENE_CLONES_DURING_MOCAP()
				DELETE_PEDS_FOR_CUTSCENE()
				CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS_CLEANUP(cutsceneVehHelpStruct)
			ENDIF
			STOP_AUDIO_SCENE_FOR_SCRIPTED_CUTSCENE()
			
			IF NOT SHOULD_HOLD_SCRIPTED_CUTSCENE_ON_END()
				//FORCE_CAMERA_RELATIVE_HEADING_AND_PITCH()
			ENDIF
			
			IF SHOULD_FADE_PLAYER_DURING_SCRIPTED_CUTSCENE()
				DISABLE_COLLISION_AND_FREEZE_PLAYER_DURING_SCENE(FALSE)
			ENDIF
			
			PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] RUN_SCRIPTED_CUTSCENE - Cutscene no longer playing, cleaning up.")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL SHOULD_KNACKER_MISSION_ENTITY(INT iMissionEntity)
	IF serverBD.sMissionEntity[iMissionEntity].iDeliveriesMade < serverBD.sMissionEntity[iMissionEntity].iDeliveriesToMake
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SHOULD_KNACKER_MISSION_ENTITY - yes - serverBD.sMissionEntity[iMissionEntity].iDeliveriesMade < serverBD.sMissionEntity[iMissionEntity].iDeliveriesToMake")
		RETURN TRUE
	ENDIF
	
	IF GET_END_REASON() = eENDREASON_TIME_UP
	OR GET_END_REASON() = eENDREASON_NO_BOSS_LEFT
	OR GET_END_REASON() = eENDREASON_ALL_GOONS_LEFT
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SHOULD_KNACKER_MISSION_ENTITY - yes - eENDREASON_TIME_UP OR eENDREASON_NO_BOSS_LEFT OR eENDREASON_ALL_GOONS_LEFT")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_KNACKERING_MISSION_ENTITY(INT iMissionEntity)
	IF GET_END_REASON() = eENDREASON_NO_REASON_YET
		EXIT
	ENDIF

	IF NOT IS_MISSION_ENTITY_CLIENT_BIT_SET(PARTICIPANT_ID(), iMissionEntity, eMISSIONENTITYCLIENTBITSET_KNACKERED)
	AND NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_KNACKERED)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
		AND IS_ENTITY_ALIVE(NET_TO_VEH(serverBD.sMissionEntity[iMissionEntity].netId))
			VEHICLE_INDEX vehID = NET_TO_VEH(serverBD.sMissionEntity[iMissionEntity].netId)
			IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
				IF IS_MEGA_BUSINESS_VARIATION_A_SELL_MISSION(GET_MEGA_BUSINESS_VARIATION())
					IF SHOULD_KNACKER_MISSION_ENTITY(iMissionEntity)
						KNACKER_MISSION_ENTITY(vehID)
						SET_MISSION_ENTITY_CLIENT_BIT(iMissionEntity, eMISSIONENTITYCLIENTBITSET_KNACKERED)
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_KNACKERING_MISSION_ENTITY - knackered mission entity ", iMissionEntity, " - sell mission")
					ENDIF
				ELIF DOES_MEGA_BUSINESS_VARIATION_HAVE_WARP_INTO_NIGHTCLUB(GET_MEGA_BUSINESS_VARIATION())
					IF GET_END_REASON() != eENDREASON_MISSION_ENTITY_DELIVERED
						KNACKER_MISSION_ENTITY(vehID)
						SET_MISSION_ENTITY_CLIENT_BIT(iMissionEntity, eMISSIONENTITYCLIENTBITSET_KNACKERED)
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_KNACKERING_MISSION_ENTITY - knackered mission entity ", iMissionEntity, " - should warp in but failed")
					ENDIF
				ELIF GET_MEGA_BUSINESS_VARIATION() = MBV_FLYER_PROMOTION
				OR GET_MEGA_BUSINESS_VARIATION() = MBV_BLIMP_PROMOTION
					SET_VEHICLE_STRONG(vehID, FALSE)
					SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(vehID, TRUE)
					SET_VEHICLE_DAMAGE_SCALE(vehID, 1.0)
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_KNACKERING_MISSION_ENTITY - mission entity ", iMissionEntity, " will now take normal damage")
					SET_MISSION_ENTITY_CLIENT_BIT(iMissionEntity, eMISSIONENTITYCLIENTBITSET_KNACKERED)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CLEAR_DELIVERY_CUTSCENE()
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_CLIENT_BIT_SET(PARTICIPANT_ID(),eCLIENTBITSET_I_AM_IN_THE_CUTSCENE_VEHICLE)
		AND NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(),eCLIENTBITSET_MISSION_EOM_DELIVERY_CUTSCENE_COMPLETE)
			CLEANUP_DELIVERY_CUTSCENE_CLIENT(playerBD[PARTICIPANT_ID_TO_INT()].sDeliveryCutscene, bbdcCutsceneData,GET_SERVER_GAME_STATE()!=GAME_STATE_END)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_SEATS_RESERVED_HELP()

	IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_DO_SEATS_RESERVED_HELP)
		VEHICLE_INDEX vehID
		INT iMissionEntity = MISSION_ENTITY_ONE // mission entity lookup?
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
			IF IS_ENTITY_ALIVE(NET_TO_VEH(serverBD.sMissionEntity[iMissionEntity].netId))
				vehID = NET_TO_VEH(serverBD.sMissionEntity[iMissionEntity].netId)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehID)
		AND NOT IS_PLAYER_IN_POSSESSION_OF_A_MISSION_ENTITY(localPlayerId)
			SWITCH GET_MEGA_BUSINESS_VARIATION()
				CASE MBV_COLLECT_STAFF
				CASE MBV_COLLECT_DJ_COLLECTOR
					IF IS_MISSION_ENTITY_IN_POSSESSION_OF_A_PLAYER(iMissionEntity)
						IF IS_VEHICLE_SEAT_FREE(vehID, VS_FRONT_RIGHT)
						OR IS_VEHICLE_SEAT_FREE(vehID, VS_BACK_LEFT)
						OR IS_VEHICLE_SEAT_FREE(vehID, VS_BACK_RIGHT)
							IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
								IF GET_DISTANCE_BETWEEN_ENTITIES(localPlayerPed, vehID) < 10
									SET_LOCAL_BIT(eLOCALBITSET_DO_SEATS_RESERVED_HELP)
									PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_SEATS_RESERVED_HELP - Player tried to enter the limo when no unreserved seats are free")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE MBV_PRIVATE_TAXI
				CASE MBV_SNAPMATIC
					IF IS_MISSION_ENTITY_IN_POSSESSION_OF_A_PLAYER(iMissionEntity)
						IF NOT IS_VEHICLE_SEAT_FREE(vehID, VS_FRONT_RIGHT)
						AND (IS_VEHICLE_SEAT_FREE(vehID, VS_BACK_LEFT)
						OR IS_VEHICLE_SEAT_FREE(vehID, VS_BACK_RIGHT))
							IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
								IF GET_DISTANCE_BETWEEN_ENTITIES(localPlayerPed, vehID) < 10
									SET_LOCAL_BIT(eLOCALBITSET_DO_SEATS_RESERVED_HELP)
									PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_SEATS_RESERVED_HELP - Player tried to enter the limo when no unreserved seats are free")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE MBV_COLLECT_DJ_CRASH
				CASE MBV_COLLECT_DJ_STOLEN_BAG
					IF IS_PED_TRYING_TO_ENTER_A_LOCKED_VEHICLE(localPlayerPed)
					AND GET_VEHICLE_PED_IS_USING(localPlayerPed) = vehID
						SET_LOCAL_BIT(eLOCALBITSET_DO_SEATS_RESERVED_HELP)
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_SEATS_RESERVED_HELP - Player tried to enter the the vehicle when no unreserved seats are free")
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_PLAYER_VEHICLE_AND_WARP_DURING_MOCAP()
	IF sMocap.iStage > 1
	OR (iMocapStage >= MOCAP_STAGE_SCENE_PLAYING
	AND iMocapStage < MOCAP_STAGE_SCENE_FINISHED)
		IF SHOULD_FADE_OUT_PLAYER_AND_VEHICLE_DURING_MOCAP()
			IF GET_SEAT_PED_IS_IN(localPlayerPed) = VS_DRIVER
				NETWORK_FADE_OUT_ENTITY(GET_VEHICLE_PED_IS_IN(localPlayerPed), FALSE, TRUE)
				PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] eMODESTATE_MID_MISSION_MOCAP - Faded out local player vehicle during mocap.")
			ENDIF
		ENDIF
		
		IF SHOULD_FREEZE_PLAYER_VEH_DURING_MOCAP()
			IF GET_SEAT_PED_IS_IN(localPlayerPed) = VS_DRIVER
				FREEZE_ENTITY_POSITION(GET_VEHICLE_PED_IS_IN(localPlayerPed), TRUE)
				PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] eMODESTATE_MID_MISSION_MOCAP - SHOULD_FREEZE_PLAYER_VEH_DURING_MOCAP during mocap.")
			ENDIF
		ENDIF						
		
		IF SHOULD_SET_VEH_INVINCIBLE_DURING_MOCAP()
			IF GET_SEAT_PED_IS_IN(localPlayerPed) = VS_DRIVER
				SET_ENTITY_INVINCIBLE(GET_VEHICLE_PED_IS_IN(localPlayerPed), TRUE)
				PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] eMODESTATE_MID_MISSION_MOCAP - SHOULD_SET_VEH_INVINCIBLE_DURING_MOCAP during mocap.")
			ENDIF
		ENDIF
		
		IF SHOULD_WARP_PLAYER_INTO_MISSION_ENTITY_DURING_MOCAP()
			IF GET_SCRIPT_TASK_STATUS(localPlayerPed, SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK
			AND GET_SCRIPT_TASK_STATUS(localPlayerPed, SCRIPT_TASK_ENTER_VEHICLE) != WAITING_TO_START_TASK
			AND IS_VEHICLE_SEAT_FREE(NET_TO_VEH(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId))
				TASK_ENTER_VEHICLE(localPlayerPed, NET_TO_VEH(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId), -1, VS_DRIVER, DEFAULT, ECF_WARP_PED)
				PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] eMODESTATE_MID_MISSION_MOCAP - TASK_ENTER_VEHICLE during mocap.")
			ENDIF								
		ENDIF
	ENDIF
	
	IF sMocap.iStage > 1
	OR iMocapStage = MOCAP_STAGE_SCENE_PLAYING
		IF SHOULD_WARP_PLAYER_DURING_MOCAP()
			WARP_PLAYER_DURING_SCENE()
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_STARTING_WEAPONS()
	GIVE_STARTING_WEAPON()
ENDPROC

PROC SET_PLAYER_POST_MISSION_CLUB_CUTSCENE_DATA()
	#IF IS_DEBUG_BUILD
		IF NOT HAS_MISSION_BEEN_DEBUG_PASSED_OR_FAILED()
	#ENDIF
			IF GET_MEGA_BUSINESS_VARIATION() = MBV_COLLECT_EQUIPMENT
			OR GET_MEGA_BUSINESS_VARIATION() = MBV_COLLECT_STAFF
				IF SHOULD_PLAYER_SET_WATCH_NIGHTCLUB_SETUP_CUTSCENE(localPlayerId)
					SET_LOCAL_PLAYER_WATCHING_NIGHTCLUB_SETUP_CUTSCENE(TRUE)
					PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - SET_PLAYER_POST_MISSION_CLUB_CUTSCENE_DATA - Calling SET_LOCAL_PLAYER_WATCHING_NIGHTCLUB_SETUP_CUTSCENE")
				ELSE
					PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - SET_PLAYER_POST_MISSION_CLUB_CUTSCENE_DATA - SHOULD_PLAYER_SET_WATCH_NIGHTCLUB_SETUP_CUTSCENE = FALSE - Already viewed: ", HAS_LOCAL_PLAYER_VIEWED_NIGHTCLUB_SETUP_CUTSCENE())
				ENDIF
			ELIF GET_MEGA_BUSINESS_VARIATION() = MBV_COLLECT_DJ_CRASH
				IF SHOULD_PLAYER_SET_WATCH_NIGHTCLUB_SOLOMUN_CUTSCENE(localPlayerId)
					SET_LOCAL_PLAYER_WATCHING_NIGHTCLUB_SOLOMUN_CUTSCENE(TRUE)
					PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - SET_PLAYER_POST_MISSION_CLUB_CUTSCENE_DATA - Calling SET_LOCAL_PLAYER_WATCHING_NIGHTCLUB_SOLOMUN_CUTSCENE")
				ELSE
					PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - SET_PLAYER_POST_MISSION_CLUB_CUTSCENE_DATA - SHOULD_PLAYER_SET_WATCH_NIGHTCLUB_SOLOMUN_CUTSCENE = FALSE. Already viewed: ", HAS_LOCAL_PLAYER_VIEWED_NIGHTCLUB_SOLOMUN_CUTSCENE())
				ENDIF
			ELSE
				PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - SET_PLAYER_POST_MISSION_CLUB_CUTSCENE_DATA - No scene to setup")
			ENDIF
	#IF IS_DEBUG_BUILD
		ENDIF
	#ENDIF
ENDPROC

BOOL bTrackDeaths
PROC MAINTAIN_TELEM_DEATH_TRACKING()
	IF IS_LOCAL_BIT_SET(eLOCALBITSET_JUST_BEEN_KILLED)
		IF bTrackDeaths
			//PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_TELEM_DEATH_TRACKING - I've died! New death count is ", iLocalDeathsForTelem, ", blocking death tracking until i've respawned")
			//iLocalDeathsForTelem++
			bTrackDeaths = FALSE
		ELSE
			IF bLocalPlayerOk
			AND	GET_LOCAL_PLAYER_RESPAWN_STATE() = RESPAWN_STATE_PLAYING
				CLEAR_LOCAL_BIT(eLOCALBITSET_JUST_BEEN_KILLED)
				PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_TELEM_DEATH_TRACKING - Started death tracking")
				bTrackDeaths = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC INT SNAPMATIC_DELAY()

	IF serverBD.iTotalDeliveredCount = 0

		RETURN 18000
	ENDIF
	
	RETURN 500
ENDFUNC

PROC MAINTAIN_END_SHARD_DURING_CUTSCENE()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_SELL_SINGLE_DROP
		CASE MBV_SELL_MULTI_DROP
		CASE MBV_SELL_HACK_DROP
		CASE MBV_SELL_UNDERCOVER_COPS
		CASE MBV_SELL_FOLLOW_HELI
		CASE MBV_SELL_NOT_A_SCRATCH
		CASE MBV_SUPPLY_RUN
		CASE MBV_STOLEN_SUPPLIES
			IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_DONE_END_SHARD)
				IF GET_MODE_STATE() = eMODESTATE_DELIVER_CUTSCENE
					IF IS_CUTSCENE_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sDeliveryCutscene.iBitSet, eDELIVERYCUTSCENEBITSET_OK_TO_SHOW_SHARD)
						DO_BIG_MESSAGE(eBIGMESSAGE_END_DELIVERED)
						SET_LOCAL_BIT(eLOCALBITSET_DONE_END_SHARD)
						SET_LOCAL_BIT(eLOCALBITSET_DID_END_SHARD_DURING_CUTSCENE)
						PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_END_SHARD_DURING_CUTSCENE - Drawing end shard!")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE MBV_COLLECT_DJ_COLLECTOR
		CASE MBV_COLLECT_DJ_CRASH
		CASE MBV_COLLECT_DJ_HOOKED
		CASE MBV_COLLECT_DJ_STOLEN_BAG
			IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_DONE_END_SHARD)
				IF GET_MODE_STATE() = eMODESTATE_DELIVER_CUTSCENE
					IF IS_CUTSCENE_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sDeliveryCutscene.iBitSet, eDELIVERYCUTSCENEBITSET_OK_TO_SHOW_SHARD)
						SET_LOCAL_BIT(eLOCALBITSET_DONE_END_SHARD)
						SET_LOCAL_BIT(eLOCALBITSET_DID_END_SHARD_DURING_CUTSCENE)
						PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_END_SHARD_DURING_CUTSCENE - Drawing end shard!")
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

PROC MAINTAIN_LOSE_PURSUERS()
	BOOL bLostAll = TRUE
	INT iPed
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_PRIVATE_TAXI
			SWITCH GET_MEGA_BUSINESS_SUBVARIATION()
				CASE MBSV_PRIVATE_TAXI_4
				CASE MBSV_PRIVATE_TAXI_5
				CASE MBSV_PRIVATE_TAXI_6
				CASE MBSV_PRIVATE_TAXI_7
					IF NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_LOST_ALL_PURSUERS)
					AND NOT IS_SERVER_BIT_SET(eSERVERBITSET_LOST_ALL_PURSUERS)
						REPEAT GET_NUM_VARIATION_PEDS() iPed
							IF IS_PED_BIT_SET(iPed, ePEDBITSET_I_AM_PAPARAZZI_PED)
								IF NOT IS_PED_BIT_SET(iPed, ePEDBITSET_I_HAVE_BEEN_LOST)
									bLostAll = FALSE
								ENDIF
							ENDIF
						ENDREPEAT
						IF bLostAll
							SET_CLIENT_BIT(eCLIENTBITSET_LOST_ALL_PURSUERS)
							PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_END_SHARD_DURING_CUTSCENE - Drawing end shard!")
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL SHOULD_PREVENT_SHUFFLING()
	VEHICLE_SEAT eSeat
	
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_COLLECTOR
		CASE MBV_COLLECT_DJ_CRASH
		CASE MBV_COLLECT_DJ_HOOKED
		CASE MBV_COLLECT_DJ_STOLEN_BAG
		CASE MBV_PRIVATE_TAXI
		CASE MBV_SNAPMATIC
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
				eSeat = GET_SEAT_PED_IS_IN(localPlayerPed, TRUE) 
				IF eSeat = VS_BACK_LEFT
				OR eSeat = VS_BACK_RIGHT
					IF GET_VEHICLE_PED_IS_IN(localPlayerPed, TRUE) = NET_TO_VEH(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_DISABLE_SHUFFLING_BEHAVIOUR()
	IF SHOULD_PREVENT_SHUFFLING()
		IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_DISABLE_SHUFFLE)
			SET_PED_CONFIG_FLAG(localPlayerPed, PCF_DontShuffleInVehicleToMakeRoom, TRUE)
			SET_LOCAL_BIT(eLOCALBITSET_DISABLE_SHUFFLE)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_DISABLE_SHUFFLING_BEHAVIOUR - Set PCF_DontShuffleInVehicleToMakeRoom flag.")
		ENDIF
	ELSE
		IF IS_LOCAL_BIT_SET(eLOCALBITSET_DISABLE_SHUFFLE)
			SET_PED_CONFIG_FLAG(localPlayerPed, PCF_DontShuffleInVehicleToMakeRoom, FALSE)
			CLEAR_LOCAL_BIT(eLOCALBITSET_DISABLE_SHUFFLE)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_DISABLE_SHUFFLING_BEHAVIOUR - Cleaned up PCF_DontShuffleInVehicleToMakeRoom flag.")
		ENDIF
	ENDIF
ENDPROC

FUNC VECTOR GET_TRAFFIC_REDUCTION_ZONE_COORDS()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_CRASH			RETURN GET_MEGA_BUSINESS_GOTO_COORDS(GET_MEGA_BUSINESS_VARIATION(), GET_MEGA_BUSINESS_SUBVARIATION(), serverBD.nightclubID, GET_EXTRA_INT_PARAM_FOR_GOTO_COORDS_LOOKUP(), serverBD.iRandomVariant)
//		CASE MBV_COLLECT_DJ_HOOKED          RETURN GET_MEGA_BUSINESS_VEHICLE_SPAWN_COORDS(ciHOOKED_VEH_POLICE3, GET_MEGA_BUSINESS_VARIATION(), GET_MEGA_BUSINESS_SUBVARIATION(), GET_RANDOM_MODE_VARIANT_FOR_ENTITY_LOOKUP())
	ENDSWITCH
	RETURN <<0.0,0.0,0.0>>
ENDFUNC

FUNC FLOAT GET_TRAFFIC_REDUCTION_RANGE()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_HOOKED          RETURN 250.00
		CASE MBV_COLLECT_DJ_CRASH          RETURN 250.00
	ENDSWITCH
	RETURN 50.00
ENDFUNC

INT iTrafficReductionIndex
PROC MAINTAIN_TRAFFIC_REDUCTION_ZONES()
	IF IS_VECTOR_ZERO(GET_TRAFFIC_REDUCTION_ZONE_COORDS())
	OR NOT GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
		EXIT
	ENDIF

	IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_ADDED_TRAFFIC_REDUCTION_ZONE)
		ADD_TRAFFIC_REDUCTION_ZONE(iTrafficReductionIndex, GET_TRAFFIC_REDUCTION_ZONE_COORDS(), FALSE, GET_TRAFFIC_REDUCTION_RANGE())
		SET_LOCAL_BIT(eLOCALBITSET_ADDED_TRAFFIC_REDUCTION_ZONE)
	ENDIF
ENDPROC

FUNC BOOL IN_RANGE_AND_OK_FOR_CUTSCENE()
	IF IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(localPlayerId)
	OR IS_HACKER_TRUCK_PILOT_ENTERING()
		RETURN FALSE
	ENDIF
	
	RETURN IS_ENTITY_IN_RANGE_COORDS(localPlayerPed, GET_MID_MISSION_MOCAP_RANGE_COORDS(), GET_MID_MISSION_MOCAP_RANGE())
ENDFUNC

PROC CLIENT_PROCESSING()
		
	#IF IS_DEBUG_BUILD
	MAINTAIN_J_SKIPS()																				#IF IS_DEBUG_BUILD #IF SCRIPT_PROFILER_ACTIVE	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_J_SKIPS")										#ENDIF	#ENDIF 
	#ENDIF
	
	
	INT iMissionEntity

	IF GET_MODE_STATE() > eMODESTATE_INIT
		INITIALISE_CLIENT_VARS()
		MAINTAIN_CALL_COMMON_START_FUNCTIONS()
		MAINTAIN_SETUP_GANG_CHASE_OPTIONS()
		MAINTAIN_RESERVE_NETWORK_ENTITIES()
		MAINTAIN_SETTING_SUBVARIATION_BD()
		MAINTAIN_EXECUTE_COMMON_START_TELEMETRY()
		MAINTAIN_I_WANT_MISSION_ENTITY_TO_SPAWN()
		MAINTAIN_MISSION_SPECIFIC_LOGIC_CLIENT()
		MAINTAIN_DISABLE_SHUFFLING_BEHAVIOUR()
		MAINTAIN_TRAFFIC_REDUCTION_ZONES()
//		MAINTAIN_JIMMY_BREAK_IN_FLAG()
		
		IF DOES_MEGA_BUSINESS_VARIATION_USE_MISSION_ENTITIES(GET_MEGA_BUSINESS_VARIATION())
			REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
				MAINTAIN_MISSION_ENTITY_DELIVERED(iMissionEntity)
				MAINTAIN_MISSION_ENTITY_DESTROYED(iMissionEntity)
				MAINTAIN_MISSION_ENTITY_UNDRIVEABLE(iMissionEntity)
				MAINTAIN_SHOULD_MISSION_ENTITY_BE_DESTROYABLE_CLIENT(iMissionEntity)
				MAINTAIN_EXIT_MISSION_ENTITY_AFTER_DELIVERY(iMissionEntity)
				MAINTAIN_VEHICLE_HALT_AT_GOTO(iMissionEntity)
				MAINTAIN_UPDATING_MISSION_ENTITY_GLOBAL_VEHICLE_COORDS(iMissionEntity)
				MAINTAIN_ATTACHING_MISSION_ENTITY_VEHICLE_LOGIC(iMissionEntity)
				MAINTAIN_DETACHING_VEHICLE_LOGIC(iMissionEntity, TRUE)
				MAINTAIN_KNACKERING_MISSION_ENTITY(iMissionEntity)
				MAINTAIN_MISSION_ENTITY_VEHICLE_RADIO(iMissionEntity)
				MAINTAIN_MISSION_ENTITY_MUSIC_STREAM(iMissionEntity)
				MAINTAIN_MISSION_ENTITY_MISSION_SPECIFIC_CLIENT(iMissionEntity)
			ENDREPEAT
		ENDIF
		
		MAINTAIN_AUDIO()
		MAINTAIN_PROP_MARKERS()
		MAINTAIN_SET_GLOBAL_BLIP_DATA_FOR_MISSION_ENTITIES()
		MAINTAIN_MISSION_ENTITY_POSSESSION()
		MAINTAIN_INTERIOR_LOCATES()
		MAINTAIN_NEAREST_MISSION_ENTITY()
		MAINTAIN_DROP_OFFS()
		MAINTAIN_NEAREST_DROP_OFF()
		MAINTAIN_NEAREST_POSTER()
		MAINTAIN_HUD()
		MAINTAIN_I_HAVE_REACHED_GOTO_LOCATION()
		MAINTAIN_I_HAVE_COLLECTED_ITEM()
		MAINTAIN_PED_BODIES()
		MAINTAIN_VEHICLE_BODIES()
		MAINTAIN_HOOKED_CARGOBOB()
		MAINTAIN_HOOKED_HELIPAD_DELIVERY()
		MAINTAIN_GANG_CHASE_START_CHECKS()
		MAINTAIN_GANG_CHASE_CLIENT(sGangChaseLocal, serverBD.sGangChaseServer)
		MAINTAIN_GLOBAL_REVEAL_SOUNDS()
		MAINTAIN_DAMAGE_TRACKING_ACTIVATION()
		MAINTAIN_WANTED_LEVEL_EVENTS()																
		MAINTAIN_CLIENT_SHOULD_ALL_PEDS_REACT_CHECKS()
		MAINTAIN_CLUE_FOUND()
		MAINTAIN_GROUND_FLARES()
		MAINTAIN_PTFX()
		MAINTAIN_TRIGGER_AREAS()
		MAINTAIN_SCENARIO_BLOCKING_AREAS()
		MAINTAIN_RESTRICTED_AREA_TIMER()
		MAINTAIN_CLEAR_ITEM_COLLECTION()
		MAINTAIN_CLEAR_GOTO()
		MAINTAIN_CARWASH_FLAGS()
		MAINTAIN_CARWASH_BLIPS()
		MAINTAIN_FOCUS_CAM()
		MAINTAIN_RADAR_ZOOM()
		MAINTAIN_COUNT_PLAYER_SCORE()
		MAINTAIN_DRAW_PICKUP_ARROW()
		MAINTAIN_BLOCKING_LOSE_COPS()
		LATCH_VEHICLE_DOORS()
		MAINTAIN_GUIDANCE_BLIP()
		MAINTAIN_DIALOGUE()
		MAINTAIN_MODEL_HIDES()
		MAINTAIN_DUFFEL_BAG()
		MAINTAIN_BLOCKING_WANTED_LEVEL()
		MAINTAIN_SEATS_RESERVED_HELP()
		MAINTAIN_STARTING_WEAPONS()
		MAINTAIN_TELEM_DEATH_TRACKING()
		MAINTAIN_END_SHARD_DURING_CUTSCENE()
		MAINTAIN_LOSE_PURSUERS()
	ENDIF
		
	iMissionEntity = 0
	
	SWITCH GET_MODE_STATE()
	
		CASE eMODESTATE_INIT
			// ********************************************************************
			// Server says when omode has intialised and moves us onto run state.
			// ********************************************************************
		BREAK
		
		CASE eMODESTATE_BACKTRACK
		
			SWITCH GET_MEGA_BUSINESS_VARIATION()
				CASE MBV_SNAPMATIC
					// We need to make sure we're tracking the ped we're collecting before we get into wait stage
					MAINTAIN_PED_COLLECTION()
				
					IF GET_MISSION_ENTITY_PLAYER_IS_HOLDING(localPlayerId) = MISSION_ENTITY_ONE
					OR GET_MISSION_ENTITY_I_AM_IN_WITH_MISSION_ENTITY_HOLDER() = MISSION_ENTITY_ONE
						IF IS_LOCAL_BIT_SET(eLOCALBITSET_OPENING_PHONECALL_DONE_OR_NEVER_GOING_TO_HAPPEN)
						AND serverBD.iTotalDeliveredCount > 0
							DISABLE_CELLPHONE_THIS_FRAME_ONLY()
						ENDIF
					ENDIF
				
					REMOVE_ALL_MISSION_ENTITY_BLIPS()
					REMOVE_ALL_DROP_OFF_BLIPS()
//					IF serverBD.iTotalDeliveredCount < GET_NUM_DROP_OFFS_FOR_MEGA_BUSINESS_VARIATION(GET_MEGA_BUSINESS_VARIATION()) -1
					IF serverBD.iTotalDeliveredCount = 0
						DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_SNAPMATIC_WAIT)
					ELSE
						CLEAR_ALL_OBJECTIVE_TEXT()
						Clear_Any_Objective_Text_From_This_Script()
					ENDIF			
				BREAK
			ENDSWITCH
		
		BREAK
		
		CASE eMODESTATE_GO_TO_POINT
			IF NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
			AND NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE()
			AND GB_IS_IT_SAFE_TO_DISPLAY_BOSS_MISSION_UI(DEFAULT, DEFAULT, bUICheckTutorialSession)
			AND IS_LOCAL_BIT_SET(eLOCALBITSET_OPENING_PHONECALL_DONE_OR_NEVER_GOING_TO_HAPPEN) // No god text, hud etc, until we have seen the opening phone call/text msg - for narrative purposes
			AND NOT IS_LOCAL_BIT_SET(eLOCALBITSET_DISPLAYED_END_OF_MODE_SUCCESS_BIG_MESSAGE)
				
				IF IS_LOCAL_BIT_SET(eLOCALBITSET_CALLED_START_COMMON_TELEMETRY)
					
					IF GET_END_REASON() = eENDREASON_NO_REASON_YET
						
						// ********************************************
						// Run main mode logic here.
						// Remember to set the player as 
						// GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT() 
						// when appropriate.
						// ********************************************
												
						IF SHOULD_DO_LOSE_WANTED_LEVEL_OBJECTIVE()
									
							REMOVE_ALL_MISSION_ENTITY_BLIPS()
							REMOVE_ALL_DROP_OFF_BLIPS()
							REMOVE_GO_TO_BLIP()
							
							DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_LOSE_COPS)
						ELSE	
							
							IF GET_MEGA_BUSINESS_VARIATION() = MBV_PRIVATE_TAXI
								IF IS_LOCAL_BIT_SET(eLOCALBITSET_RADAR_SHOULD_ZOOM)
									CLEAR_LOCAL_BIT(eLOCALBITSET_RADAR_SHOULD_ZOOM)
								ENDIF
							ENDIF
							
							IF SHOULD_PLAYER_BE_GIVEN_ALTERNATIVE_GOTO_OBJECTIVE()
								DRAW_GO_TO_BLIP()
								DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_GO_TO_POINT_ALT)
							ELIF SHOULD_PLAYER_BE_GIVEN_GOTO_OBJECTIVE()
								DRAW_GO_TO_BLIP()
								DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_GO_TO_POINT)
							ELIF SHOULD_PLAYER_BE_GIVEN_HELP_GOTO_OBJECTIVE()
								IF GET_MEGA_BUSINESS_VARIATION() != MBV_COLLECT_DJ_HOOKED
								OR serverBD.iCurrentGotoLocation != INVALID_GOTO_LOCATION
									DRAW_GO_TO_BLIP()
								ELSE
									REMOVE_GO_TO_BLIP()
								ENDIF
								DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_ESCORT)
							ELSE
								REMOVE_GO_TO_BLIP()
								DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_COLLECT_ENTITY)
							ENDIF
							
							MAINTAIN_MISSION_ENTITY_BLIPS()
						ENDIF
					ENDIF
				ENDIF
			ELSE
				CLEAR_ALL_OBJECTIVE_TEXT()
				REMOVE_ALL_MISSION_ENTITY_BLIPS()
			ENDIF
		BREAK
		
		CASE eMODESTATE_GO_TO_ENTITY
		
		BREAK
		
		CASE eMODESTATE_CAR_WASH
			REMOVE_GO_TO_BLIP()
			
			IF NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
			AND NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE()
			AND GB_IS_IT_SAFE_TO_DISPLAY_BOSS_MISSION_UI(DEFAULT, DEFAULT, bUICheckTutorialSession)
			AND IS_LOCAL_BIT_SET(eLOCALBITSET_OPENING_PHONECALL_DONE_OR_NEVER_GOING_TO_HAPPEN) // No god text, hud etc, until we have seen the opening phone call/text msg - for narrative purposes
			AND NOT IS_LOCAL_BIT_SET(eLOCALBITSET_DISPLAYED_END_OF_MODE_SUCCESS_BIG_MESSAGE)
				
				IF IS_LOCAL_BIT_SET(eLOCALBITSET_CALLED_START_COMMON_TELEMETRY)
					
					IF GET_END_REASON() = eENDREASON_NO_REASON_YET
						
						IF SHOULD_DO_LOSE_WANTED_LEVEL_OBJECTIVE()
										
							REMOVE_ALL_MISSION_ENTITY_BLIPS()
							REMOVE_ALL_DROP_OFF_BLIPS()
							REMOVE_GO_TO_BLIP()
							
							DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_LOSE_COPS)
						ELSE	
						
							IF IS_PLAYER_USING_CARWASH(NETWORK_PLAYER_ID_TO_INT())
								Clear_Any_Objective_Text_From_This_Script()
							ELSE
								DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_CAR_WASH)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
			
				Clear_Any_Objective_Text_From_This_Script() // Clear objective text if player is retricted for hide or passive.
					
			ENDIF
		BREAK
		
		CASE eMODESTATE_TAKE_OUT_TARGETS
		
			REMOVE_GO_TO_BLIP()
			
			IF NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
			AND NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE()
			AND GB_IS_IT_SAFE_TO_DISPLAY_BOSS_MISSION_UI(DEFAULT, DEFAULT, bUICheckTutorialSession)
			AND IS_LOCAL_BIT_SET(eLOCALBITSET_OPENING_PHONECALL_DONE_OR_NEVER_GOING_TO_HAPPEN) // No god text, hud etc, until we have seen the opening phone call/text msg - for narrative purposes
			AND NOT IS_LOCAL_BIT_SET(eLOCALBITSET_DISPLAYED_END_OF_MODE_SUCCESS_BIG_MESSAGE)
				
				IF IS_LOCAL_BIT_SET(eLOCALBITSET_CALLED_START_COMMON_TELEMETRY)
					
					IF GET_END_REASON() = eENDREASON_NO_REASON_YET
						
						IF SHOULD_DO_LOSE_WANTED_LEVEL_OBJECTIVE()
										
							REMOVE_ALL_MISSION_ENTITY_BLIPS()
							REMOVE_ALL_DROP_OFF_BLIPS()
							REMOVE_GO_TO_BLIP()
							
							DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_LOSE_COPS)
						ELSE	
						
							DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_ELIMINATE_TARGETS)
						
							MAINTAIN_MISSION_ENTITY_BLIPS()
						ENDIF
					
					ENDIF
				ENDIF
			ELSE
			
				Clear_Any_Objective_Text_From_This_Script() // Clear objective text if player is retricted for hide or passive.
					
			ENDIF
		BREAK
		
		CASE eMODESTATE_INCAPACITATE_TARGETS
		
			REMOVE_GO_TO_BLIP()
			
			IF NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
			AND NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE()
			AND GB_IS_IT_SAFE_TO_DISPLAY_BOSS_MISSION_UI(DEFAULT, DEFAULT, bUICheckTutorialSession)
			AND IS_LOCAL_BIT_SET(eLOCALBITSET_OPENING_PHONECALL_DONE_OR_NEVER_GOING_TO_HAPPEN) // No god text, hud etc, until we have seen the opening phone call/text msg - for narrative purposes
			AND NOT IS_LOCAL_BIT_SET(eLOCALBITSET_DISPLAYED_END_OF_MODE_SUCCESS_BIG_MESSAGE)
				
				IF IS_LOCAL_BIT_SET(eLOCALBITSET_CALLED_START_COMMON_TELEMETRY)
					
					IF GET_END_REASON() = eENDREASON_NO_REASON_YET
						
						IF SHOULD_DO_LOSE_WANTED_LEVEL_OBJECTIVE()
										
							REMOVE_ALL_MISSION_ENTITY_BLIPS()
							REMOVE_ALL_DROP_OFF_BLIPS()
							REMOVE_GO_TO_BLIP()
							
							DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_LOSE_COPS)
						ELSE	
						
							DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_INCAPACITATE_TARGETS)
							
						ENDIF
					ENDIF
				ENDIF
			ELSE
			
				Clear_Any_Objective_Text_From_This_Script() // Clear objective text if player is retricted for hide or passive.
					
			ENDIF
		BREAK
		
		CASE eMODESTATE_FIND_ITEM

		BREAK
		
		CASE eMODESTATE_COLLECT_ITEM
			REMOVE_GO_TO_BLIP()
			
			IF NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
			AND NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE()
			AND GB_IS_IT_SAFE_TO_DISPLAY_BOSS_MISSION_UI(DEFAULT, DEFAULT, bUICheckTutorialSession)
			AND IS_LOCAL_BIT_SET(eLOCALBITSET_OPENING_PHONECALL_DONE_OR_NEVER_GOING_TO_HAPPEN) // No god text, hud etc, until we have seen the opening phone call/text msg - for narrative purposes
			AND NOT IS_LOCAL_BIT_SET(eLOCALBITSET_DISPLAYED_END_OF_MODE_SUCCESS_BIG_MESSAGE)
				
				IF IS_LOCAL_BIT_SET(eLOCALBITSET_CALLED_START_COMMON_TELEMETRY)
					
					IF GET_END_REASON() = eENDREASON_NO_REASON_YET
						IF SHOULD_DO_LOSE_WANTED_LEVEL_OBJECTIVE()
										
							REMOVE_ALL_MISSION_ENTITY_BLIPS()
							REMOVE_ALL_DROP_OFF_BLIPS()
							REMOVE_GO_TO_BLIP()
							
							DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_LOSE_COPS)
						ELSE	
							DRAW_FIND_ITEM_PORTABLE_PICKUP_BLIP()
							
							IF SHOULD_DO_COLLECT_ITEM_OBJECTIVE()
								DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_COLLECT_ITEM)
							ELSE
								IF HOOKED_CARGOBOB_OCCUPIED()
									DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_ESCORT)
								ELSE
									DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_COLLECT_VEHICLE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
			
				Clear_Any_Objective_Text_From_This_Script() // Clear objective text if player is retricted for hide or passive.
			ENDIF
		BREAK
		
		CASE eMODESTATE_MEET_UP
			
		BREAK
		
		CASE eMODESTATE_HACK
			MAINTAIN_MISSION_ENTITY_BLIPS()
			MAINTAIN_PLAYER_HACKING()
			DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_SIGHTSEER)
		BREAK
		
		CASE eMODESTATE_MID_MISSION_MOCAP
			
			IF NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_WATCHED_MID_MISSION_MOCAP)
				IF SHOULD_START_MID_MISSION_MOCAP()
				OR IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_STARTED_MID_MISSION_MOCAP)
					IF IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_STARTED_MID_MISSION_MOCAP)
					OR GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
					OR (NOT IS_LOCAL_BIT_SET(eLOCALBITSET_OUT_OF_RANGE_FOR_MID_MISSION_MOCAP)
					AND IN_RANGE_AND_OK_FOR_CUTSCENE())
						VEHICLE_INDEX vehId
						CLEAR_ALL_OBJECTIVE_TEXT()
						REMOVE_GO_TO_BLIP()
						SET_CLIENT_BIT(eCLIENTBITSET_STARTED_MID_MISSION_MOCAP)

						MAINTAIN_PLAYER_VEHICLE_AND_WARP_DURING_MOCAP()
						CONTACT_REQUEST_MENU_DISABLE_THIS_FRAME()
						KILL_INTERACTION_MENU()
						DISABLE_INTERACTION_MENU()
						IF IS_PHONE_ONSCREEN()
							HANG_UP_AND_PUT_AWAY_PHONE()
						ENDIF
						
						IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_RS)
						ELSE
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CONTEXT)
						ENDIF
						
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
		
						SWITCH iMocapStage
							CASE MOCAP_STAGE_INIT
								IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
									vehId = GET_VEHICLE_PED_IS_IN(localPlayerPed)
									IF DOES_ENTITY_EXIST(vehId)
									AND IS_VEHICLE_RADIO_ON(vehId)
									AND NETWORK_HAS_CONTROL_OF_ENTITY(vehId)
										SET_VEHICLE_RADIO_ENABLED(vehId, FALSE)
										SET_LOCAL_BIT(eLOCALBITSET_TURNED_OFF_RADIO_FOR_MOCAP)
									ENDIF
								ENDIF
		
								START_AUDIO_SCENE_FOR_MOCAP_CUTSCENE()
								
								NETWORK_SET_VOICE_ACTIVE(FALSE)
								iMocapStage++
								PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] eMODESTATE_MID_MISSION_MOCAP - MOCAP_STAGE_INIT - Moving to MOCAP_STAGE_REQUEST_SCENE.")
							BREAK
							
							CASE MOCAP_STAGE_REQUEST_SCENE
								BOOL bMoveOn
								bMoveOn = TRUE
								IF DOES_MOCAP_SCENE_NEED_PED_CLONE()
									IF NOT CREATE_PLAYER_PED_COPY_FOR_MOCAP_SCENE()
										bMoveOn = FALSE
									ENDIF
								ENDIF
								
								IF DOES_MOCAP_SCENE_NEED_MISSION_ENTITY_VEH_CLONE()
									IF NOT CREATE_MOCAP_MISSION_ENTITY_VEH_COPY_FOR_MOCAP_SCENE()
										bMoveOn = FALSE
									ENDIF
								ENDIF
								
								IF DOES_MOCAP_SCENE_NEED_MOCAP_VEHICLE_CLONE()
									IF NOT CREATE_MOCAP_VEHICLE_COPY_FOR_MOCAP_SCENE()
										bMoveOn = FALSE
									ENDIF
								ENDIF
								
								IF DOES_MOCAP_SCENE_NEED_DJ_CLONE()
									IF NOT CREATE_DIXON_PED_FOR_MOCAP_SCENE()
										bMoveOn = FALSE
									ENDIF
								ENDIF
								
								
								IF bMoveOn
									REQUEST_CUTSCENE(GET_MOCAP_SCENE_NAME())
									iMocapStage++
									PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] eMODESTATE_MID_MISSION_MOCAP - MOCAP_STAGE_REQUEST_SCENE - Moving to MOCAP_STAGE_REQUEST_ASSETS.")
								ENDIF
							BREAK
							
							CASE MOCAP_STAGE_REQUEST_ASSETS
								IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
									IF DOES_MOCAP_SCENE_NEED_PED_CLONE()
										SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_1", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
									ENDIF
									IF DOES_MOCAP_SCENE_NEED_MISSION_ENTITY_VEH_CLONE()
										SET_CUTSCENE_ENTITY_STREAMING_FLAGS(GET_MOCAP_SCENE_MISSION_ENTITY_VEH_CLONE_HANDLE(), DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
									ENDIF
									IF DOES_MOCAP_SCENE_NEED_MOCAP_VEHICLE_CLONE()
										SET_CUTSCENE_ENTITY_STREAMING_FLAGS(GET_MOCAP_SCENE_VEHICLE_CLONE_HANDLE(), DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
									ENDIF
									IF DOES_MOCAP_SCENE_NEED_DJ_CLONE()
										SET_CUTSCENE_ENTITY_STREAMING_FLAGS(GET_MOCAP_SCENE_DJ_CLONE_HANDLE(), DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
									ENDIF
									iMocapStage++
									PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] eMODESTATE_MID_MISSION_MOCAP - MOCAP_STAGE_REQUEST_ASSETS - Moving to MOCAP_STAGE_START_SCENE.")
								ELSE
									PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] eMODESTATE_MID_MISSION_MOCAP - MOCAP_STAGE_REQUEST_ASSETS - CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY = FALSE")
								ENDIF
							BREAK
						ENDSWITCH
						
						IF STOP_VEHICLE_AND_REMOVE_CONTROL_FOR_MOCAP(FALSE)
						OR IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_WATCHED_MID_MISSION_MOCAP)
							
							IF iMocapStage != MOCAP_STAGE_PLACEHOLDER_SCENE
							AND IS_STRING_NULL_OR_EMPTY(GET_MOCAP_SCENE_NAME())
								iMocapStage = MOCAP_STAGE_PLACEHOLDER_SCENE
								PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] eMODESTATE_MID_MISSION_MOCAP - No mocap scene name. Falling back to placeholder.")
							ENDIF 
							
							SWITCH iMocapStage
								CASE MOCAP_STAGE_START_SCENE
									IF bLocalPlayerOk
									AND IN_RANGE_AND_OK_FOR_CUTSCENE()
										IF HAS_CUTSCENE_LOADED() 
											IF DOES_ENTITY_EXIST(bossPlayerPed)
											AND NOT IS_PED_INJURED(bossPlayerPed)
												PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] eMODESTATE_MID_MISSION_MOCAP - MOCAP_STAGE_START_SCENE - Added boss player ped to scene.")
												REGISTER_ENTITY_FOR_CUTSCENE(bossPlayerPed, "MP_1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
											ENDIF
											IF DOES_ENTITY_EXIST(missionEntityMocapVeh)
											AND NOT IS_ENTITY_DEAD(missionEntityMocapVeh)
												PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] eMODESTATE_MID_MISSION_MOCAP - MOCAP_STAGE_START_SCENE - Added mission entity mocap vehicle to scene.")
												REGISTER_ENTITY_FOR_CUTSCENE(missionEntityMocapVeh, GET_MOCAP_SCENE_MISSION_ENTITY_VEH_CLONE_HANDLE(), CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME | CEO_CLONE_DAMAGE_TO_CS_MODEL | CEO_PRESERVE_FACE_BLOOD_DAMAGE | CEO_PRESERVE_BODY_BLOOD_DAMAGE)
											ENDIF
											IF DOES_ENTITY_EXIST(mocapVehicle)
											AND NOT IS_ENTITY_DEAD(mocapVehicle)
												PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] eMODESTATE_MID_MISSION_MOCAP - MOCAP_STAGE_START_SCENE - Added mocap vehicle to scene.")
												REGISTER_ENTITY_FOR_CUTSCENE(mocapVehicle, GET_MOCAP_SCENE_VEHICLE_CLONE_HANDLE(), CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
											ENDIF
											IF DOES_ENTITY_EXIST(djMissionPed)
											AND NOT IS_ENTITY_DEAD(djMissionPed)
												PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] eMODESTATE_MID_MISSION_MOCAP - MOCAP_STAGE_START_SCENE - Added mocap DJ to scene.")
												REGISTER_ENTITY_FOR_CUTSCENE(djMissionPed, GET_MOCAP_SCENE_DJ_CLONE_HANDLE(), CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
											ENDIF
											
											START_CUTSCENE()
											
											iMocapStage++
											PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] eMODESTATE_MID_MISSION_MOCAP - MOCAP_STAGE_START_SCENE - Started cutscene. Moving to MOCAP_STAGE_FADE_IN_FOR_SCENE.")
										ELSE
											PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] eMODESTATE_MID_MISSION_MOCAP - MOCAP_STAGE_START_SCENE - HAS_CUTSCENE_LOADED = FALSE")
										ENDIF
									ELSE
										PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] eMODESTATE_MID_MISSION_MOCAP - MOCAP_STAGE_START_SCENE - bLocalPlayerOk = FALSE")
										IF NOT GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()	
											SET_LOCAL_BIT(eLOCALBITSET_OUT_OF_RANGE_FOR_MID_MISSION_MOCAP)
										ENDIF
									ENDIF
								BREAK
								
								CASE MOCAP_STAGE_FADE_IN_FOR_SCENE
									IF IS_CUTSCENE_PLAYING()
										IF IS_SCREEN_FADED_OUT()					
											DO_SCREEN_FADE_IN(500)
										ENDIF
										
										IF SHOULD_CLEAN_UP_SCRIPTED_CUTSCENE_CLONES_DURING_MOCAP()
											DELETE_PEDS_FOR_CUTSCENE()
											CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS_CLEANUP(cutsceneVehHelpStruct)
										ENDIF
										
										IF SHOULD_HOLD_SCRIPTED_CUTSCENE_ON_END()
											SIMPLE_CUTSCENE_KILL_CAMERA(cutsceneStruct)
										ENDIF
										
										START_MP_CUTSCENE()
										iMocapStage++
										PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] eMODESTATE_MID_MISSION_MOCAP - MOCAP_STAGE_FADE_IN_FOR_SCENE - Cutscene playing. Moving to MOCAP_STAGE_SCENE_PLAYING.")
									ELSE
										PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] eMODESTATE_MID_MISSION_MOCAP - MOCAP_STAGE_FADE_IN_FOR_SCENE - Cutscene not playing.")
									ENDIF
								BREAK
								
								CASE MOCAP_STAGE_SCENE_PLAYING
									IF NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_WATCHING_MID_MISSION_MOCAP)	
										SET_CLIENT_BIT(eCLIENTBITSET_WATCHING_MID_MISSION_MOCAP)
									ENDIF
									IF HAS_CUTSCENE_FINISHED() 
									OR NOT IS_CUTSCENE_PLAYING()
										CLEANUP_MP_CUTSCENE(DEFAULT, FALSE)
										SET_GAMEPLAY_CAM_RELATIVE_HEADING()
										
										IF SHOULD_FADE_OUT_PLAYER_AND_VEHICLE_DURING_MOCAP()
											IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
											AND NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(localPlayerPed))
												SET_ENTITY_VISIBLE(GET_VEHICLE_PED_IS_IN(localPlayerPed), TRUE)
											ENDIF
											
											IF GET_MEGA_BUSINESS_VARIATION() = MBV_COLLECT_DJ_CRASH
												IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[1].netId)
												AND NOT IS_ENTITY_DEAD(NET_TO_ENT(serverBD.sPed[1].netId))
													SET_ENTITY_VISIBLE(NET_TO_ENT(serverBD.sPed[1].netId), TRUE)
												ENDIF
											ENDIF
										ENDIF
										
										IF DOES_ENTITY_EXIST(bossPlayerPed)
											DELETE_PED(bossPlayerPed)
											PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] eMODESTATE_MID_MISSION_MOCAP - MOCAP_STAGE_SCENE_PLAYING - Deleting ped boss clone.")
										ENDIF
										
										IF DOES_ENTITY_EXIST(missionEntityMocapVeh)
											IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
											AND NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId))
											AND NOT IS_ENTITY_DEAD(missionEntityMocapVeh)
												IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
													MOVE_VEHICLE_DECALS(missionEntityMocapVeh, NET_TO_VEH(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId))
												ENDIF
											ENDIF
											DELETE_VEHICLE(missionEntityMocapVeh)
											PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] eMODESTATE_MID_MISSION_MOCAP - MOCAP_STAGE_SCENE_PLAYING - Deleting mission entity mocap vehicle clone.")
										ENDIF
										
										IF DOES_ENTITY_EXIST(mocapVehicle)
											DELETE_VEHICLE(mocapVehicle)
											PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] eMODESTATE_MID_MISSION_MOCAP - MOCAP_STAGE_SCENE_PLAYING - Deleting mocap vehicle clone.")
										ENDIF
										
										IF DOES_ENTITY_EXIST(djMissionPed)
											DELETE_PED(djMissionPed)
											PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] eMODESTATE_MID_MISSION_MOCAP - MOCAP_STAGE_SCENE_PLAYING - Deleting mocap vehicle clone.")
										ENDIF

										iMocapStage++
										PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] eMODESTATE_MID_MISSION_MOCAP - MOCAP_STAGE_SCENE_PLAYING - Cutscene finished or not playing. Cleaning up cutscene. Moving to MOCAP_STAGE_SCENE_FINISHED.")
									ELSE
										IF IS_HELP_MESSAGE_BEING_DISPLAYED()
											CLEAR_HELP()
										ENDIF
									ENDIF
								BREAK
								
								CASE MOCAP_STAGE_SCENE_FINISHED
									IF IS_CUTSCENE_PLAYING()
										STOP_CUTSCENE_IMMEDIATELY()
										REMOVE_CUTSCENE()
									ENDIF
									
									IF IS_LOCAL_BIT_SET(eLOCALBITSET_TURNED_OFF_RADIO_FOR_MOCAP)
										IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
											vehId = GET_VEHICLE_PED_IS_IN(localPlayerPed)
											IF DOES_ENTITY_EXIST(vehId)
											AND NETWORK_HAS_CONTROL_OF_ENTITY(vehId)
												SET_VEHICLE_RADIO_ENABLED(vehId, TRUE)
												CLEAR_LOCAL_BIT(eLOCALBITSET_TURNED_OFF_RADIO_FOR_MOCAP)
											ENDIF
										ENDIF
									ENDIF
									
									IF DOES_ENTITY_EXIST(bossPlayerPed)
										DELETE_PED(bossPlayerPed)
										PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] eMODESTATE_MID_MISSION_MOCAP - MOCAP_STAGE_SCENE_FINISHED - Deleting ped boss clone.")
									ENDIF
									
									IF DOES_ENTITY_EXIST(missionEntityMocapVeh)
										DELETE_VEHICLE(missionEntityMocapVeh)
										PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] eMODESTATE_MID_MISSION_MOCAP - MOCAP_STAGE_SCENE_FINISHED - Deleting mission entity mocap vehicle clone.")
									ENDIF
									
									IF DOES_ENTITY_EXIST(mocapVehicle)
										DELETE_VEHICLE(mocapVehicle)
										PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] eMODESTATE_MID_MISSION_MOCAP - MOCAP_STAGE_SCENE_FINISHED - Deleting mocap vehicle clone.")
									ENDIF

									IF GET_MEGA_BUSINESS_VARIATION() = MBV_COLLECT_DJ_CRASH
										IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BBH_CRFOCUS")
											CLEAR_HELP()
										ENDIF
									ENDIF

									BOOL bFadeIn
									IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
										vehId = GET_VEHICLE_PED_IS_IN(localPlayerPed)
										
										IF DOES_ENTITY_EXIST(vehId)
										AND NOT IS_ENTITY_DEAD(vehId)
										
											// 4898672
											SWITCH GET_MEGA_BUSINESS_VARIATION()
												CASE MBV_COLLECT_DJ_HOOKED
													IF NOT SHOULD_WARP_PLAYER_INTO_MISSION_ENTITY_DURING_MOCAP()
													AND HOOKED_IS_LOCAL_PLAYER_IN_CARGOBOB(FALSE, TRUE)
														IF GET_SCRIPT_TASK_STATUS(localPlayerPed, SCRIPT_TASK_LEAVE_VEHICLE) != PERFORMING_TASK
														AND GET_SCRIPT_TASK_STATUS(localPlayerPed, SCRIPT_TASK_LEAVE_VEHICLE) != WAITING_TO_START_TASK
															TASK_LEAVE_VEHICLE(localPlayerPed, vehId)
															PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] eMODESTATE_MID_MISSION_MOCAP - TASK_LEAVE_VEHICLE during mocap.")
														ENDIF								
													ENDIF
												BREAK
											ENDSWITCH										
										
											IF IS_ENTITY_VISIBLE(vehId)
											//AND NOT NETWORK_IS_ENTITY_FADING(vehId)
											AND GET_ENTITY_ALPHA(vehId) = 255
												bFadeIn = TRUE
											ENDIF	
										ELSE
											bFadeIn = TRUE
										ENDIF
									ELSE
										bFadeIn = TRUE
									ENDIF
									
									IF bFadeIn
										NETWORK_SET_VOICE_ACTIVE(TRUE)
										
										STOP_AUDIO_SCENE_FOR_MOCAP_CUTSCENE()

										IF IS_SKYSWOOP_AT_GROUND()
											NET_SET_PLAYER_CONTROL(localPlayerId, TRUE)
											ENABLE_INTERACTION_MENU()
											SET_GAMEPLAY_CAM_RELATIVE_HEADING()
										ENDIF
										
										CLEAR_LOCAL_BIT(eLOCALBITSET_REMOVED_PLAYER_CONTROL)
										
										CLEAR_CLIENT_BIT(eCLIENTBITSET_WATCHING_MID_MISSION_MOCAP)
										CLEAR_CLIENT_BIT(eCLIENTBITSET_STARTED_MID_MISSION_MOCAP)
										SET_CLIENT_BIT(eCLIENTBITSET_I_WATCHED_MID_MISSION_MOCAP)
										CLEAR_HOOKED_CARGBOBOB_AREA()
										
										iMocapStage++
										PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] eMODESTATE_MID_MISSION_MOCAP - MOCAP_STAGE_FADE_IN_FOR_END - Faded in screen, all stages finished.")
									ENDIF
								BREAK
								
								CASE MOCAP_STAGE_PLACEHOLDER_SCENE
									#IF IS_DEBUG_BUILD
									IF RUN_PLACEHOLDER_MO_CAP(sMocap.iStage, sMocap.stTimer, ciPLACEHOLDER_CUTSCENE_MID_STRAND_CUT)
									#ENDIF
										
										IF GET_MEGA_BUSINESS_VARIATION() = MBV_COLLECT_DJ_CRASH
											IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BBH_CRFOCUS")
												CLEAR_HELP()
											ENDIF
										ENDIF
										
										DO_SCREEN_FADE_IN(3500)
										
										IF SHOULD_FADE_OUT_PLAYER_AND_VEHICLE_DURING_MOCAP()
											IF GET_SEAT_PED_IS_IN(localPlayerPed) = VS_DRIVER
												NETWORK_FADE_IN_ENTITY(GET_VEHICLE_PED_IS_IN(localPlayerPed), TRUE, FALSE)
												PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - eMODESTATE_MID_MISSION_MOCAP, - NETWORK_FADE_IN_ENTITY ")
											ENDIF
										ENDIF
										
										IF IS_SKYSWOOP_AT_GROUND()
											ENABLE_INTERACTION_MENU()
											NET_SET_PLAYER_CONTROL(localPlayerId, TRUE)
										ENDIF
										CLEAR_LOCAL_BIT(eLOCALBITSET_REMOVED_PLAYER_CONTROL)

										CLEAR_CLIENT_BIT(eCLIENTBITSET_STARTED_MID_MISSION_MOCAP)
										SET_CLIENT_BIT(eCLIENTBITSET_I_WATCHED_MID_MISSION_MOCAP)
										
										PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] eMODESTATE_MID_MISSION_MOCAP - MOCAP_STAGE_PLACEHOLDER_SCENE - Finished placeholder scene.")
									#IF IS_DEBUG_BUILD
									ENDIF
									#ENDIF
								BREAK
							ENDSWITCH
						ENDIF
					ELSE
						REMOVE_ALL_MISSION_ENTITY_BLIPS()
						DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_MID_MISSION_MOCAP)
						SET_LOCAL_BIT(eLOCALBITSET_OUT_OF_RANGE_FOR_MID_MISSION_MOCAP)
						SET_CLIENT_BIT(eCLIENTBITSET_I_WATCHED_MID_MISSION_MOCAP)
					ENDIF
				ELSE
					IF DOES_MID_MISSION_MOCAP_HAVE_PRE_MOCAP_STAGE()
						IF NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_STARTED_MID_MISSION_MOCAP)
							IF IS_LOCAL_BIT_SET(eLOCALBITSET_STARTED_PRE_MOCAP_STAGE)
							OR SHOULD_DO_PRE_MOCAP_STAGE()
								
								SET_LOCAL_BIT(eLOCALBITSET_STARTED_PRE_MOCAP_STAGE)
								CLEAR_ALL_OBJECTIVE_TEXT()
								REMOVE_GO_TO_BLIP()
								
								KILL_INTERACTION_MENU()
								DISABLE_INTERACTION_MENU()
								CONTACT_REQUEST_MENU_DISABLE_THIS_FRAME()
								IF IS_PHONE_ONSCREEN()
									HANG_UP_AND_PUT_AWAY_PHONE()
								ENDIF
			
								IF STOP_VEHICLE_AND_REMOVE_CONTROL_FOR_MOCAP(FALSE)
									//IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_DONE_PRE_MOCAP_FOCUS_CAM)
									//	IF ACTIVATE_FOCUS_CAM()
									//		SET_LOCAL_BIT(eLOCALBITSET_DONE_PRE_MOCAP_FOCUS_CAM)
									//	ENDIF
									//ELSE
										PRINT_PRE_MOCAP_STAGE_HELP()
									//ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE eMODESTATE_SCRIPTED_CUTSCENE
			
			DISABLE_VEHICLE_EXIT_THIS_FRAME()
			
			IF NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_WATCHED_SCRIPTED_CUTSCENE)
				IF SHOULD_START_SCRIPTED_CUTSCENE()
				OR IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_STARTED_SCRIPTED_CUTSCENE)
					IF IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_STARTED_SCRIPTED_CUTSCENE)
					OR (NOT IS_LOCAL_BIT_SET(eLOCALBITSET_OUT_OF_RANGE_FOR_SCRIPTED_CUTSCENE)
					AND IN_RANGE_AND_OK_FOR_CUTSCENE())
						CLEAR_ALL_OBJECTIVE_TEXT()
						REMOVE_GO_TO_BLIP()
						
						CONTACT_REQUEST_MENU_DISABLE_THIS_FRAME()
						IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_RS)
						ELSE
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CONTEXT)
						ENDIF
						
						IF NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(),eCLIENTBITSET_STARTED_SCRIPTED_CUTSCENE)
							RESET_SCENE_WARP_DATA()
							CLEAR_HOOKED_CARGBOBOB_AREA()
						ENDIF
						SET_CLIENT_BIT(eCLIENTBITSET_STARTED_SCRIPTED_CUTSCENE)
										
						IF cutsceneStruct.bPlaying
							IF SHOULD_WARP_PLAYER_DURING_SCRIPTED_CUTSCENE()
								WARP_PLAYER_DURING_SCENE()
							ENDIF
							IF SHOULD_FADE_PLAYER_DURING_SCRIPTED_CUTSCENE()
								FADE_PLAYER_DURING_SCENE(TRUE)
								DISABLE_COLLISION_AND_FREEZE_PLAYER_DURING_SCENE(TRUE)
							ENDIF
						ENDIF

						IF RUN_SCRIPTED_CUTSCENE()
							CLEAR_CLIENT_BIT(eCLIENTBITSET_STARTED_SCRIPTED_CUTSCENE)
							SET_CLIENT_BIT(eCLIENTBITSET_I_WATCHED_SCRIPTED_CUTSCENE)
						ENDIF
					ELSE
						DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_MID_MISSION_MOCAP)
						SET_LOCAL_BIT(eLOCALBITSET_OUT_OF_RANGE_FOR_SCRIPTED_CUTSCENE)
						SET_CLIENT_BIT(eCLIENTBITSET_I_WATCHED_SCRIPTED_CUTSCENE)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE eMODESTATE_REACH_SCORE
			REMOVE_GO_TO_BLIP()
			DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_REACH_SCORE)
		BREAK
		
		CASE eMODESTATE_COLLECT_PREREQUISITE_VEHICLE
		
		BREAK
		
		CASE eMODESTATE_COLLECT_VEHICLE
			REMOVE_GO_TO_BLIP()
			REMOVE_ALL_MISSION_ENTITY_BLIPS()
			
			IF NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
			AND NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE()
			AND GB_IS_IT_SAFE_TO_DISPLAY_BOSS_MISSION_UI(DEFAULT, DEFAULT, bUICheckTutorialSession)
			AND IS_LOCAL_BIT_SET(eLOCALBITSET_OPENING_PHONECALL_DONE_OR_NEVER_GOING_TO_HAPPEN) // No god text, hud etc, until we have seen the opening phone call/text msg - for narrative purposes
			AND NOT IS_LOCAL_BIT_SET(eLOCALBITSET_DISPLAYED_END_OF_MODE_SUCCESS_BIG_MESSAGE)
				
				IF IS_LOCAL_BIT_SET(eLOCALBITSET_CALLED_START_COMMON_TELEMETRY)
					
					IF GET_END_REASON() = eENDREASON_NO_REASON_YET
						IF SHOULD_DO_LOSE_WANTED_LEVEL_OBJECTIVE()
										
							REMOVE_ALL_MISSION_ENTITY_BLIPS()
							REMOVE_ALL_DROP_OFF_BLIPS()
							REMOVE_GO_TO_BLIP()
							
							DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_LOSE_COPS)
						ELSE	
						
							IF HOOKED_CARGOBOB_OCCUPIED()
								DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_HOOK_VEHICLE)
							ELSE
								DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_COLLECT_VEHICLE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
			
				Clear_Any_Objective_Text_From_This_Script() // Clear objective text if player is retricted for hide or passive.
			ENDIF
		BREAK
		
		CASE eMODESTATE_GAIN_WANTED
		
		BREAK
		
		CASE eMODESTATE_WAIT_FOR_PASSENGERS
			REMOVE_GO_TO_BLIP()
		
			IF NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
			AND NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE()
			AND GB_IS_IT_SAFE_TO_DISPLAY_BOSS_MISSION_UI(DEFAULT, DEFAULT, bUICheckTutorialSession)
			AND IS_LOCAL_BIT_SET(eLOCALBITSET_OPENING_PHONECALL_DONE_OR_NEVER_GOING_TO_HAPPEN)  // No god text, hud etc, until we have seen the opening phone call/text msg - for narrative purposes
			AND NOT IS_LOCAL_BIT_SET(eLOCALBITSET_DISPLAYED_END_OF_MODE_SUCCESS_BIG_MESSAGE)
				
				IF IS_LOCAL_BIT_SET(eLOCALBITSET_CALLED_START_COMMON_TELEMETRY)
					
					IF GET_END_REASON() = eENDREASON_NO_REASON_YET
						
						// ********************************************
						// Run main mode logic here.
						// Remember to set the player as 
						// GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT() 
						// when appropriate.
						// ********************************************
						
						IF HAS_ANY_MISSION_ENTITY_BEEN_CREATED()
							IF SHOULD_DO_LOSE_WANTED_LEVEL_OBJECTIVE()
										
								REMOVE_ALL_MISSION_ENTITY_BLIPS()
								REMOVE_ALL_DROP_OFF_BLIPS()
								REMOVE_GO_TO_BLIP()
								
								DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_LOSE_COPS)
							ELSE	
								IF ARE_ALL_MISSION_ENTITIES_IN_POSSESSION_OF_A_PLAYER()
								
									IF SHOULD_VARIATION_USE_SEQUENTIAL_PED_COLLECTION()
										MAINTAIN_SEQUENTIAL_PED_COLLECTION()
									ELSE
										MAINTAIN_PED_COLLECTION()
									ENDIF
								
									IF SHOULD_VARIATION_USE_SIGNAL_OBJECTIVE()
										IF SHOULD_VARIATION_USE_SEQUENTIAL_PED_COLLECTION()
											IF IS_MISSION_ENTITY_IN_RANGE_OF_LOCATION_PEDS()
												IF NOT IS_SERVER_BIT_SET(GET_SERVER_LOCATION_PEDS_SHOULD_ENTER_BIT())
												AND IS_PLAYER_IN_POSSESSION_OF_A_MISSION_ENTITY(localPlayerId)
													DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_SIGNAL_PASSENGERS)
												ELSE
													DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_WAIT_FOR_PASSENGERS)
												ENDIF
												
												IF GET_MEGA_BUSINESS_VARIATION() = MBV_PRIVATE_TAXI
													IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_RADAR_SHOULD_ZOOM)
														SET_LOCAL_BIT(eLOCALBITSET_RADAR_SHOULD_ZOOM)
													ENDIF
												ENDIF
											ELSE
												IF IS_PLAYER_IN_POSSESSION_OF_A_MISSION_ENTITY(localPlayerId)
													DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_COLLECT_VIPS)
												ELSE
													DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_HELP_COLLECT_VIPS)
												ENDIF
												
												IF GET_MEGA_BUSINESS_VARIATION() = MBV_PRIVATE_TAXI
													IF IS_LOCAL_BIT_SET(eLOCALBITSET_RADAR_SHOULD_ZOOM)
														CLEAR_LOCAL_BIT(eLOCALBITSET_RADAR_SHOULD_ZOOM)
													ENDIF
												ENDIF
											ENDIF
										ELSE
											IF serverBD.iPedStaffBeingCollected != -1
											AND NOT IS_PED_BIT_SET(serverBD.iPedStaffBeingCollected, ePEDBITSET_SIGNALED_TO_ENTER_VEHICLE)
											AND IS_PLAYER_IN_POSSESSION_OF_A_MISSION_ENTITY(localPlayerId)
												DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_SIGNAL_PASSENGERS)
											ELSE
												IF IS_PLAYER_IN_POSSESSION_OF_A_MISSION_ENTITY(localPlayerId)
													DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_WAIT_FOR_PASSENGERS)
												ELSE
													DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_HELP_COLLECT_VIPS)
												ENDIF
											ENDIF
										ENDIF
									ELSE
										DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_WAIT_FOR_PASSENGERS)
									ENDIF
								ELSE
									CLEAR_PED_COLLECT_HELP()
									DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_COLLECT_ENTITY)
								ENDIF
								
								MAINTAIN_MISSION_ENTITY_BLIPS()
								
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
				
			ELSE
				CLEAR_ALL_OBJECTIVE_TEXT()
				REMOVE_ALL_MISSION_ENTITY_BLIPS()
			ENDIF
		BREAK
		
		CASE eMODESTATE_FOLLOW_ENTITY
			RESET_MID_MISSION_MOCAP_DATA()
		
			IF NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
			AND NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE()
			AND GB_IS_IT_SAFE_TO_DISPLAY_BOSS_MISSION_UI(DEFAULT, DEFAULT, bUICheckTutorialSession)
			AND IS_LOCAL_BIT_SET(eLOCALBITSET_OPENING_PHONECALL_DONE_OR_NEVER_GOING_TO_HAPPEN)  // No god text, hud etc, until we have seen the opening phone call/text msg - for narrative purposes
			AND NOT IS_LOCAL_BIT_SET(eLOCALBITSET_DISPLAYED_END_OF_MODE_SUCCESS_BIG_MESSAGE)
				
				IF IS_LOCAL_BIT_SET(eLOCALBITSET_CALLED_START_COMMON_TELEMETRY)
					
					IF GET_END_REASON() = eENDREASON_NO_REASON_YET
						
						// ********************************************
						// Run main mode logic here.
						// Remember to set the player as 
						// GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT() 
						// when appropriate.
						// ********************************************
						
						MAINTAIN_ENTITY_TO_FOLLOW_SIGNALED()
						
						IF HAS_ANY_MISSION_ENTITY_BEEN_CREATED()
							IF SHOULD_DO_LOSE_WANTED_LEVEL_OBJECTIVE()
										
								REMOVE_ALL_MISSION_ENTITY_BLIPS()
								REMOVE_ALL_DROP_OFF_BLIPS()
								REMOVE_GO_TO_BLIP()
								
								DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_LOSE_COPS)
							ELSE	
								IF ARE_ALL_MISSION_ENTITIES_IN_POSSESSION_OF_A_PLAYER()
									IF DOES_ENTITY_TO_FOLLOW_NEED_SIGNALED()
									AND NOT HAS_ENTITY_TO_FOLLOW_BEEN_SIGNALED()
										IF IS_PLAYER_WITHIN_RANGE_TO_SIGNAL_ENTITY()
											REMOVE_GO_TO_BLIP()
											DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_SIGNAL_ENTITY)
										ELSE
											DRAW_GO_TO_BLIP()
											DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_GO_TO_POINT)
										ENDIF
									ELSE
										REMOVE_GO_TO_BLIP()
										DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_FOLLOW_ENTITY)
									ENDIF
								ELSE
									REMOVE_GO_TO_BLIP()
									DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_COLLECT_ENTITY)
								ENDIF
								
								MAINTAIN_MISSION_ENTITY_BLIPS()
								
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
				
			ELSE
				CLEAR_ALL_OBJECTIVE_TEXT()
				REMOVE_ALL_MISSION_ENTITY_BLIPS()
			ENDIF
		BREAK
		
		CASE eMODESTATE_TRACKIFY
			REMOVE_GO_TO_BLIP()
		
			IF NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
			AND NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE()
			AND GB_IS_IT_SAFE_TO_DISPLAY_BOSS_MISSION_UI(DEFAULT, DEFAULT, bUICheckTutorialSession)
			AND IS_LOCAL_BIT_SET(eLOCALBITSET_OPENING_PHONECALL_DONE_OR_NEVER_GOING_TO_HAPPEN)  // No god text, hud etc, until we have seen the opening phone call/text msg - for narrative purposes
			AND NOT IS_LOCAL_BIT_SET(eLOCALBITSET_DISPLAYED_END_OF_MODE_SUCCESS_BIG_MESSAGE)
				
				IF IS_LOCAL_BIT_SET(eLOCALBITSET_CALLED_START_COMMON_TELEMETRY)
					
					IF GET_END_REASON() = eENDREASON_NO_REASON_YET
						
						// ********************************************
						// Run main mode logic here.
						// Remember to set the player as 
						// GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT() 
						// when appropriate.
						// ********************************************
						
						MAINTAIN_TRACKIFY_LOGIC()
						
						IF HAS_ANY_MISSION_ENTITY_BEEN_CREATED()
						
							IF SHOULD_DO_LOSE_WANTED_LEVEL_OBJECTIVE()
									
								REMOVE_ALL_MISSION_ENTITY_BLIPS()
								REMOVE_ALL_DROP_OFF_BLIPS()
								REMOVE_GO_TO_BLIP()
								
								DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_LOSE_COPS)
							ELSE	
							
								IF SHOULD_PLAYER_BE_GIVEN_DELIVER_OBJECTIVE()
									DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_USE_TRACKIFY)
								ELIF SHOULD_PLAYER_BE_GIVEN_HELP_DELIVER_OBJECTIVE()
									DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_USE_TRACKIFY)
								ELSE
									DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_COLLECT_ENTITY)
								ENDIF
							
								MAINTAIN_MISSION_ENTITY_BLIPS()
							
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
				
			ELSE
				CLEAR_ALL_OBJECTIVE_TEXT()
				REMOVE_ALL_MISSION_ENTITY_BLIPS()
			ENDIF
		BREAK
		
		CASE eMODESTATE_LOSE_PURSUERS
			REMOVE_GO_TO_BLIP()
		
			IF NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
			AND NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE()
			AND GB_IS_IT_SAFE_TO_DISPLAY_BOSS_MISSION_UI(DEFAULT, DEFAULT, bUICheckTutorialSession)
			AND IS_LOCAL_BIT_SET(eLOCALBITSET_OPENING_PHONECALL_DONE_OR_NEVER_GOING_TO_HAPPEN)  // No god text, hud etc, until we have seen the opening phone call/text msg - for narrative purposes
			AND NOT IS_LOCAL_BIT_SET(eLOCALBITSET_DISPLAYED_END_OF_MODE_SUCCESS_BIG_MESSAGE)
				
				IF IS_LOCAL_BIT_SET(eLOCALBITSET_CALLED_START_COMMON_TELEMETRY)
					
					IF GET_END_REASON() = eENDREASON_NO_REASON_YET
						
						// ********************************************
						// Run main mode logic here.
						// Remember to set the player as 
						// GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT() 
						// when appropriate.
						// ********************************************
						
						IF GET_MEGA_BUSINESS_VARIATION() = MBV_PRIVATE_TAXI
							IF IS_LOCAL_BIT_SET(eLOCALBITSET_RADAR_SHOULD_ZOOM)
								CLEAR_LOCAL_BIT(eLOCALBITSET_RADAR_SHOULD_ZOOM)
							ENDIF
						ENDIF
						
						IF HAS_ANY_MISSION_ENTITY_BEEN_CREATED()
							
							IF SHOULD_PLAYER_BE_GIVEN_DELIVER_OBJECTIVE()
								DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_LOSE_PURSUERS)
							ELIF SHOULD_PLAYER_BE_GIVEN_HELP_DELIVER_OBJECTIVE()
								DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_HELP_LOSE_PURSUERS)
							ELSE
								DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_COLLECT_ENTITY)
							ENDIF
						
							MAINTAIN_MISSION_ENTITY_BLIPS()
						ENDIF
					ENDIF
					
				ENDIF
				
			ELSE
				CLEAR_ALL_OBJECTIVE_TEXT()
				REMOVE_ALL_MISSION_ENTITY_BLIPS()
			ENDIF
		BREAK
		
		CASE eMODESTATE_COLLECT_PED
			REMOVE_GO_TO_BLIP()
		
			IF NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
			AND NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE()
			AND GB_IS_IT_SAFE_TO_DISPLAY_BOSS_MISSION_UI(DEFAULT, DEFAULT, bUICheckTutorialSession)
			AND IS_LOCAL_BIT_SET(eLOCALBITSET_OPENING_PHONECALL_DONE_OR_NEVER_GOING_TO_HAPPEN)  // No god text, hud etc, until we have seen the opening phone call/text msg - for narrative purposes
			AND NOT IS_LOCAL_BIT_SET(eLOCALBITSET_DISPLAYED_END_OF_MODE_SUCCESS_BIG_MESSAGE)
				
				IF IS_LOCAL_BIT_SET(eLOCALBITSET_CALLED_START_COMMON_TELEMETRY)
					
					IF GET_END_REASON() = eENDREASON_NO_REASON_YET
						
						// ********************************************
						// Run main mode logic here.
						// Remember to set the player as 
						// GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT() 
						// when appropriate.
						// ********************************************
						
						IF HAS_ANY_MISSION_ENTITY_BEEN_CREATED()
						
							IF SHOULD_DO_LOSE_WANTED_LEVEL_OBJECTIVE()
										
								REMOVE_ALL_MISSION_ENTITY_BLIPS()
								REMOVE_ALL_DROP_OFF_BLIPS()
								REMOVE_GO_TO_BLIP()
								
								DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_LOSE_COPS)
							ELSE	
							
								MAINTAIN_PED_COLLECTION()
							
								IF IS_PLAYER_IN_POSSESSION_OF_A_MISSION_ENTITY(localPlayerId)
									DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_COLLECT_STAFF)
								ELIF iNearestMissionEntity != -1
								AND IS_MISSION_ENTITY_IN_POSSESSION_OF_A_PLAYER(iNearestMissionEntity)
									DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_HELP_COLLECT_STAFF)
								ELSE
									DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_COLLECT_ENTITY)
								ENDIF
								
								MAINTAIN_MISSION_ENTITY_BLIPS()
								
							ENDIF
							
						ENDIF
					ENDIF
					
				ENDIF
				
			ELSE
				CLEAR_ALL_OBJECTIVE_TEXT()
				REMOVE_ALL_MISSION_ENTITY_BLIPS()
			ENDIF
		BREAK
		
		CASE eMODESTATE_POSTER
		
			REMOVE_GO_TO_BLIP()
			CLEAR_LOCAL_BIT(eLOCALBITSET_RADAR_SHOULD_ZOOM)
			
			IF NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
			AND NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE()
			AND GB_IS_IT_SAFE_TO_DISPLAY_BOSS_MISSION_UI(DEFAULT, DEFAULT, bUICheckTutorialSession)
			AND IS_LOCAL_BIT_SET(eLOCALBITSET_OPENING_PHONECALL_DONE_OR_NEVER_GOING_TO_HAPPEN)  // No god text, hud etc, until we have seen the opening phone call/text msg - for narrative purposes
			AND NOT IS_LOCAL_BIT_SET(eLOCALBITSET_DISPLAYED_END_OF_MODE_SUCCESS_BIG_MESSAGE)
				IF IS_LOCAL_BIT_SET(eLOCALBITSET_CALLED_START_COMMON_TELEMETRY)
					
					IF GET_END_REASON() = eENDREASON_NO_REASON_YET
						
						IF SHOULD_DO_LOSE_WANTED_LEVEL_OBJECTIVE()
										
							REMOVE_ALL_MISSION_ENTITY_BLIPS()
							REMOVE_ALL_DROP_OFF_BLIPS()
							
							DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_LOSE_COPS)
						ELSE	
							IF GET_TOTAL_DELIVERIES_MADE() = GET_TOTAL_DELIVERIES_TO_MAKE() 
								CLEAR_ALL_OBJECTIVE_TEXT()
							ELIF GET_TOTAL_DELIVERIES_MADE() = (GET_TOTAL_DELIVERIES_TO_MAKE() -1) 
								DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_POSTER2)
							ELSE
								DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_POSTER1)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				CLEAR_ALL_OBJECTIVE_TEXT()
			ENDIF
		BREAK
		
		CASE eMODESTATE_COLLECT_ENTITY
			REMOVE_GO_TO_BLIP()
			CLEANUP_SIGHTSEER_HACK()
		
			IF NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
			AND NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE()
			AND GB_IS_IT_SAFE_TO_DISPLAY_BOSS_MISSION_UI(DEFAULT, DEFAULT, bUICheckTutorialSession)
			AND IS_LOCAL_BIT_SET(eLOCALBITSET_OPENING_PHONECALL_DONE_OR_NEVER_GOING_TO_HAPPEN)  // No god text, hud etc, until we have seen the opening phone call/text msg - for narrative purposes
			AND NOT IS_LOCAL_BIT_SET(eLOCALBITSET_DISPLAYED_END_OF_MODE_SUCCESS_BIG_MESSAGE)
				
				IF IS_LOCAL_BIT_SET(eLOCALBITSET_CALLED_START_COMMON_TELEMETRY)
					
					IF GET_END_REASON() = eENDREASON_NO_REASON_YET
						
						// ********************************************
						// Run main mode logic here.
						// Remember to set the player as 
						// GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT() 
						// when appropriate.
						// ********************************************
						
						IF HAS_ANY_MISSION_ENTITY_BEEN_CREATED()
						AND NOT HAS_ANY_MISSION_ENTITY_BEEN_ENTERED_AT_LEAST_ONCE()
						
							IF SHOULD_DO_LOSE_WANTED_LEVEL_OBJECTIVE()
										
								REMOVE_ALL_MISSION_ENTITY_BLIPS()
								REMOVE_ALL_DROP_OFF_BLIPS()
								REMOVE_GO_TO_BLIP()
								
								DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_LOSE_COPS)
							ELSE
								DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_COLLECT_ENTITY)
							
								MAINTAIN_MISSION_ENTITY_BLIPS()
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
				
			ELSE
				CLEAR_ALL_OBJECTIVE_TEXT()
				REMOVE_ALL_MISSION_ENTITY_BLIPS()
			ENDIF
		BREAK
		
		CASE eMODESTATE_DELIVER_ENTITY
			REMOVE_GO_TO_BLIP()
			CLEANUP_SIGHTSEER_HACK()
			CLEAR_LOCAL_BIT(eLOCALBITSET_RADAR_SHOULD_ZOOM)
			
			IF NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
			AND NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE()
			AND GB_IS_IT_SAFE_TO_DISPLAY_BOSS_MISSION_UI(DEFAULT, DEFAULT, bUICheckTutorialSession)
			AND IS_LOCAL_BIT_SET(eLOCALBITSET_OPENING_PHONECALL_DONE_OR_NEVER_GOING_TO_HAPPEN)  // No god text, hud etc, until we have seen the opening phone call/text msg - for narrative purposes
			AND NOT IS_LOCAL_BIT_SET(eLOCALBITSET_DISPLAYED_END_OF_MODE_SUCCESS_BIG_MESSAGE)
				
				IF IS_LOCAL_BIT_SET(eLOCALBITSET_CALLED_START_COMMON_TELEMETRY)
					
					IF GET_END_REASON() = eENDREASON_NO_REASON_YET
					
						// We need to make sure we're tracking the ped we're collecting before we get into wait stage
						IF GET_MEGA_BUSINESS_VARIATION() = MBV_SNAPMATIC
							MAINTAIN_PED_COLLECTION()
						ENDIF
						
						IF SHOULD_DO_LOSE_WANTED_LEVEL_OBJECTIVE()
										
							REMOVE_ALL_MISSION_ENTITY_BLIPS()
							REMOVE_ALL_DROP_OFF_BLIPS()
							
							DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_LOSE_COPS)
						ELSE		
							IF SHOULD_PLAYER_BE_GIVEN_DELIVER_OBJECTIVE()
								IF SHOULD_PLAYER_DO_ALTERNATIVE_DELIVER_OBJECTIVE()
									IF GET_MEGA_BUSINESS_VARIATION() = MBV_SNAPMATIC
										DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_GO_TO_POINT)
									ELSE
										DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_DELIVER_ALT)
									ENDIF
								ELSE
									IF SHOULD_PLAYER_DO_ALTERNATIVE2_DELIVER_OBJECTIVE()
										DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_DELIVER_ALT2)
									ELSE
										DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_DELIVER_ENITTY)
									ENDIF
								ENDIF
							ELIF SHOULD_PLAYER_BE_GIVEN_HELP_DELIVER_OBJECTIVE()
								IF SHOULD_PLAYER_DO_ALTERNATIVE_DELIVER_OBJECTIVE()
								AND GET_MEGA_BUSINESS_VARIATION() = MBV_SNAPMATIC
									DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_GO_TO_POINT)
								ELSE
									DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_HELP_DELIVER_ENTITY)
								ENDIF
							ELSE
								DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_COLLECT_ENTITY)
							ENDIF
							
							MAINTAIN_MISSION_ENTITY_BLIPS()
						ENDIF
					ENDIF
					
				ENDIF
				
			ELSE
				CLEAR_ALL_OBJECTIVE_TEXT()
				REMOVE_ALL_MISSION_ENTITY_BLIPS()
			ENDIF
		BREAK
		
		CASE eMODESTATE_DELIVER_CUTSCENE
			CLEAR_ALL_OBJECTIVE_TEXT()
			REMOVE_ALL_MISSION_ENTITY_BLIPS()
			REMOVE_GO_TO_BLIP()
			REMOVE_ALL_DROP_OFF_BLIPS()
		
			IF NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(),eCLIENTBITSET_I_AM_IN_THE_CUTSCENE_VEHICLE)
			AND NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(),eCLIENTBITSET_I_AM_READY_FOR_CUTSCENE)
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
					IF bLocalPlayerOk
					AND IS_ENTITY_ALIVE(localPlayerPed)
						IF IS_PED_SITTING_IN_VEHICLE(localPlayerPed,NET_TO_VEH(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId))
							IF ABSF(GET_ENTITY_SPEED(NET_TO_VEH(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId))) <= 1.0
								SET_CLIENT_BIT(eCLIENTBITSET_I_AM_IN_THE_CUTSCENE_VEHICLE)
								bbdcCutsceneData.vsSeat = GET_PED_VEHICLE_SEAT(localPlayerPed, NET_TO_VEH(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId))
								PRINTLN("[BB_DELIVERY_CUTSCENES][MAINTAIN_DELIVERY_CUTSCENE_CLIENT] sClient.vsSeat ",bbdcCutsceneData.vsSeat)
								SET_CLIENT_BIT(eCLIENTBITSET_I_AM_READY_FOR_CUTSCENE)
							ENDIF
						ELSE
							SET_CLIENT_BIT(eCLIENTBITSET_I_AM_READY_FOR_CUTSCENE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF IS_CLIENT_BIT_SET(PARTICIPANT_ID(),eCLIENTBITSET_I_AM_IN_THE_CUTSCENE_VEHICLE)
			AND IS_SERVER_BIT_SET(eSERVERBITSET_READY_FOR_CUTSCENE)
				PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] eMODESTATE_DELIVER_CUTSCENE - MAINTAIN_DELIVERY_CUTSCENE_CLIENT")
				IF NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(),eCLIENTBITSET_CUTSCENE_FINISHED)
					VEHICLE_INDEX vehID
					OBJECT_INDEX objID
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
						vehID = NET_TO_VEH(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
					ENDIF
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[MISSION_ENTITY_ONE].vehCrate)
						objID = NET_TO_OBJ(serverBD.sMissionEntity[MISSION_ENTITY_ONE].vehCrate)
					ENDIF
					IF MAINTAIN_DELIVERY_CUTSCENE_CLIENT(playerBD[PARTICIPANT_ID_TO_INT()].sDeliveryCutscene,
														 serverBD.sDeliveryCutscene,
														 bbdcCutsceneData,
														 vehID,
														 objID,
														 GET_CUTSCENE_TYPE_FOR_MEGA_BUSINESS_VARIATION(),
														 WARP_VEHICLE_AFTER_CUTSCENE(),
														 WARP_PEDS_AFTER_CUTSCENE(),
														 FADE_IN_VEHICLE_AFTER_CUTSCENE(),
														 FADE_IN_PEDS_AFTER_CUTSCENE(),
														 FADE_IN_SCREEN_AFTER_CUTSCENE(),
														 SHOW_NEARBY_PEDS_IN_CUTSCENE())
						SET_CLIENT_BIT(eCLIENTBITSET_CUTSCENE_FINISHED)
					ENDIF
				ENDIF
				IF IS_SERVER_BIT_SET(eSERVERBITSET_CUTSCENE_FINISHED)
				AND NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(),eCLIENTBITSET_MISSION_EOM_DELIVERY_CUTSCENE_COMPLETE)
					PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] eMODESTATE_DELIVER_CUTSCENE - MAINTAIN_DELIVERY_CUTSCENE_CLIENT COMPLETE")
					IF SHOULD_DELETE_MISSION_ENTITY_BY_CUTSCENE()
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
						AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
							DELETE_NET_ID(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
						ENDIF
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[MISSION_ENTITY_ONE].vehCrate)
						AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sMissionEntity[MISSION_ENTITY_ONE].vehCrate)
							DELETE_NET_ID(serverBD.sMissionEntity[MISSION_ENTITY_ONE].vehCrate)
						ENDIF
					ENDIF
					SET_CLIENT_BIT(eCLIENTBITSET_MISSION_EOM_DELIVERY_CUTSCENE_COMPLETE)
				ENDIF
			ELIF IS_SERVER_BIT_SET(eSERVERBITSET_READY_FOR_CUTSCENE)
				IF NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_MISSION_EOM_DELIVERY_CUTSCENE_COMPLETE)
					SET_CLIENT_BIT(eCLIENTBITSET_CUTSCENE_FINISHED)
					SET_CLIENT_BIT(eCLIENTBITSET_MISSION_EOM_DELIVERY_CUTSCENE_COMPLETE)
				ENDIF
			ENDIF
		BREAK
		
		CASE eMODESTATE_DEFEND_AREA
			REMOVE_GO_TO_BLIP()
			REMOVE_ALL_DROP_OFF_BLIPS()
			REMOVE_ALL_MISSION_ENTITY_BLIPS()
			
			DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_DEFEND_AREA)
		
		BREAK
		
		CASE eMODESTATE_LEAVE_AREA
			REMOVE_GO_TO_BLIP()
			REMOVE_ALL_DROP_OFF_BLIPS()
			REMOVE_ALL_MISSION_ENTITY_BLIPS()
			MAINTAIN_LEFT_EOM_AREA()
			SET_LOCAL_BIT(eLOCALBITSET_REACHED_LEAVE_STAGE)
			
			DO_OBJECTIVE_TEXT(eOBJECTIVE_TEXT_LEAVE_AREA)
		
		BREAK
		
		CASE eMODESTATE_RETURN_ENTITY
		
		BREAK
		
		CASE eMODESTATE_REWARDS
		
			IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_DONE_PASS_FAIL_CALL)
			AND GET_END_REASON() != eENDREASON_NO_BOSS_LEFT
				// Add pass/fail call here
				GB_BUSINESS_BATTLES_SET_PASS_FAIL_CALL_DATA(serverBD.iFMMCType,
															ENUM_TO_INT(GET_MEGA_BUSINESS_VARIATION()),
															MISSION_PASSED(),
															PARTIAL_PASS(),
															IS_LOCAL_BIT_SET(eLOCALBITSET_DO_UPGRADE_CALL_IF_FAIL),
															ENUM_TO_INT(GET_MEGA_BUSINESS_SUBVARIATION()))
													 
				SET_LOCAL_BIT(eLOCALBITSET_DONE_PASS_FAIL_CALL)
			ENDIF
		
			HANDLE_CLUB_POPULARITY_CHANGES()
			CLOSE_TRACKIFY()
			REMOVE_GO_TO_BLIP()
			REMOVE_ALL_DROP_OFF_BLIPS()
			CLEAR_ALL_OBJECTIVE_TEXT()
			HANDLE_REWARDS() // Handle end of mode rewards.
			REMOVE_ALL_MISSION_ENTITY_BLIPS()
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_IHELP_CBO")
				CLEAR_HELP()
			ENDIF

			IF GET_END_REASON() = eENDREASON_MISSION_ENTITY_DELIVERED
				IF IS_MEGA_BUSINESS_VARIATION_A_SETUP_MISSION(GET_MEGA_BUSINESS_VARIATION())
					IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
						IF NOT HAS_LOCAL_PLAYER_COMPLETED_NIGHTCLUB_SETUP_MISSION(GET_MEGA_BUSINESS_VARIATION())
							SET_LOCAL_PLAYER_HAS_COMPLETED_NIGHTCLUB_SETUP_MISSION(GET_MEGA_BUSINESS_VARIATION())
							SET_PLAYER_POST_MISSION_CLUB_CUTSCENE_DATA()
							#IF FEATURE_GEN9_EXCLUSIVE
							SET_MP_INTRO_SETUP_MISSION_COMPLETE()
							#ENDIF
							PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - eMODESTATE_REWARDS - Calling SET_LOCAL_PLAYER_HAS_COMPLETED_NIGHTCLUB_SETUP_MISSION with ", GET_CURRENT_VARIATION_STRING())
						ELSE
							PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - eMODESTATE_REWARDS - Passed the ", GET_CURRENT_VARIATION_STRING(), " setup mission but already have the stat set for completion.")
						ENDIF
						
//						#IF IS_DEBUG_BUILD
//						IF NOT HAS_MISSION_BEEN_DEBUG_PASSED_OR_FAILED()
//						#ENDIF
							SWITCH GET_MEGA_BUSINESS_VARIATION()
								CASE MBV_COLLECT_DJ_CRASH
									MPGlobalsAmbience.bNightclubSetupComplete = TRUE
									PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - eMODESTATE_REWARDS - MPGlobalsAmbience.bNightclubSetupComplete = TRUE")
									SET_NIGHTCLUB_SOLO_ACTIVE_DJ(DJ_SOLOMUN, FALSE)
								BREAK
								CASE MBV_COLLECT_DJ_STOLEN_BAG
									#IF IS_DEBUG_BUILD
									IF HAS_MISSION_BEEN_DEBUG_PASSED_OR_FAILED()
										SET_NIGHTCLUB_SOLO_ACTIVE_DJ(DJ_DIXON,FALSE)
									ELSE
									#ENDIF
										SET_NIGHTCLUB_SOLO_ACTIVE_DJ(DJ_DIXON, !GET_PLAYER_NIGHTCLUB_PRIVATE_MODE(localPlayerId), TRUE)
									#IF IS_DEBUG_BUILD
									ENDIF
									#ENDIF
								BREAK
								CASE MBV_COLLECT_DJ_COLLECTOR
									#IF IS_DEBUG_BUILD
									IF HAS_MISSION_BEEN_DEBUG_PASSED_OR_FAILED()
										SET_NIGHTCLUB_SOLO_ACTIVE_DJ(DJ_TALE_OF_US,FALSE)
									ELSE
									#ENDIF
										SET_NIGHTCLUB_SOLO_ACTIVE_DJ(DJ_TALE_OF_US, !GET_PLAYER_NIGHTCLUB_PRIVATE_MODE(localPlayerId), TRUE)
									#IF IS_DEBUG_BUILD
									ENDIF
									#ENDIF
								BREAK
								CASE MBV_COLLECT_DJ_HOOKED
									#IF IS_DEBUG_BUILD
									IF HAS_MISSION_BEEN_DEBUG_PASSED_OR_FAILED()
										SET_NIGHTCLUB_SOLO_ACTIVE_DJ(DJ_BLACK_MADONNA, FALSE)
									ELSE
									#ENDIF
										SET_NIGHTCLUB_SOLO_ACTIVE_DJ(DJ_BLACK_MADONNA, !GET_PLAYER_NIGHTCLUB_PRIVATE_MODE(localPlayerId), TRUE)
									#IF IS_DEBUG_BUILD
									ENDIF
									#ENDIF
								BREAK
							ENDSWITCH
//						#IF IS_DEBUG_BUILD
//						ELSE
//							PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - eMODESTATE_REWARDS - Mission was passed or failed with debug, not setting any nightclub DJs")
//						ENDIF
//						#ENDIF
					ENDIF
				ELIF IS_MB_VARIATION_PROMOTION_VARIATION(GET_MEGA_BUSINESS_VARIATION())
					IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
						INT iStat
						iStat = GET_MP_INT_CHARACTER_STAT(MP_STAT_FMBB_FLOW_NOTIFICATIONS)
						SET_BIT(iStat,biFM_BUSINESS_BATTLES_FIRST_PROMOTION_MISSION_DONE)
						SET_MP_INT_CHARACTER_STAT(MP_STAT_FMBB_FLOW_NOTIFICATIONS,iStat)
						PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - eMODESTATE_REWARDS - biFM_BUSINESS_BATTLES_FIRST_PROMOTION_MISSION_DONE = TRUE - iStat = ",iStat)
					ENDIF
				ELIF IS_MB_VARIATION_A_CELEBRITY_VARIATION(GET_MEGA_BUSINESS_VARIATION(), GET_MEGA_BUSINESS_SUBVARIATION())
					IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
						ADD_CELEBRITY_TO_NIGHTCLUB_CELEB_LIST(localPlayerId, GET_NIGHTCLUB_CELEBRITY_FROM_MB_VARIATION(GET_MEGA_BUSINESS_VARIATION(), GET_MEGA_BUSINESS_SUBVARIATION()))
					ENDIF
				ENDIF
				
				IF GET_MEGA_BUSINESS_VARIATION() = MBV_SUPPLY_RUN
				OR GET_MEGA_BUSINESS_VARIATION() = MBV_STOLEN_SUPPLIES
					IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
						IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_NIGHTCLUB_ES_DELIVERY_TRUCK)
							SET_BIT(GlobalPlayerBD_FM[NATIVE_TO_INT(localPlayerId)].propertyDetails.bdNightclubData.iBSNightclub2, PROPERTY_BROADCAST_BS_NIGHTCLUB_ES_DELIVERY_TRUCK)
							SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_NIGHTCLUB_ES_DELIVERY_TRUCK, TRUE)
							PRINTLN("[SUPPLY_RUN] Supply mission completed - Setting PACKED_MP_BOOL_NIGHTCLUB_ES_DELIVERY_TRUCK to TRUE")
							REQUEST_SAVE(SSR_REASON_NIGHTCLUB_POPULARITY, STAT_SAVETYPE_AMBIENT)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			// Server will move us onto end state once all end of mode logic is complete.
			
		BREAK
		
		CASE eMODESTATE_WARP_INTO_NIGHTCLUB
			REMOVE_GO_TO_BLIP()
			REMOVE_ALL_MISSION_ENTITY_BLIPS()
			
			MAINTAIN_WARP_INTO_NIGHTCLUB()
		BREAK
		
		CASE eMODESTATE_END
			
		BREAK
		
	ENDSWITCH

ENDPROC

/// PURPOSE:
///    Performs client initialisation - this will wait for the server to be initialised
/// RETURNS:
///    
FUNC BOOL INIT_CLIENT()
	
	IF NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_CLIENT_INITIALISED)
		IF IS_SERVER_BIT_SET(eSERVERBITSET_SERVER_INITIALISED)
			
			// Do any client initialisation here
			
			SET_CLIENT_BIT(eCLIENTBITSET_CLIENT_INITIALISED)
		ENDIF
	ENDIF


	RETURN IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_CLIENT_INITIALISED)
ENDFUNC

PROC MONITOR_TIME_UP_END_REASON()
	#IF FEATURE_GEN9_EXCLUSIVE
	IF IS_PLAYER_ON_MP_INTRO()
		EXIT
	ENDIF
	#ENDIF
		
	IF GET_MODE_STATE() < eMODESTATE_REWARDS
	AND GET_MODE_STATE() != eMODESTATE_DELIVER_CUTSCENE
	AND GET_MODE_STATE() != eMODESTATE_MID_MISSION_MOCAP
	AND GET_MODE_STATE() != eMODESTATE_SCRIPTED_CUTSCENE
	
		// Widget for forcing time up at end of mode.
		#IF IS_DEBUG_BUILD
		IF serverBd.bFakeEndOfModeTimer
			PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - MONITOR_TIME_UP_END_REASON - serverBd.bFakeEndOfModeTimer = TRUE.")
			SET_END_REASON(eENDREASON_TIME_UP)
			SET_MODE_STATE(eMODESTATE_REWARDS)
		ENDIF
		#ENDIF
	
		// If the mode timer expires, set end reason to time up.
		IF NOT HAS_NET_TIMER_STARTED(serverBd.modeTimer)
			START_NET_TIMER(serverBd.modeTimer)
			PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - MONITOR_TIME_UP_END_REASON - Time started at ", GET_CLOUD_TIME_AS_INT())
		ELSE
			IF GET_MODE_STATE() != eMODESTATE_MID_MISSION_MOCAP
			AND GET_MODE_STATE() != eMODESTATE_SCRIPTED_CUTSCENE
				IF HAS_NET_TIMER_EXPIRED(serverBd.modeTimer, GET_MODE_TUNEABLE_TIME_LIMIT())
					IF SHOULD_SET_PASS_END_REASON()
						SET_END_REASON(eENDREASON_MISSION_ENTITY_DELIVERED)
						PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - MONITOR_TIME_UP_END_REASON - Time expired at ", GET_CLOUD_TIME_AS_INT(),", eENDREASON_MISSION_ENTITY_DELIVERED set")
					ELSE
						SET_END_REASON(eENDREASON_TIME_UP)
						PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - MONITOR_TIME_UP_END_REASON - Time expired at ", GET_CLOUD_TIME_AS_INT(),", eENDREASON_TIME_UP set")
					ENDIF
					SET_MODE_STATE(eMODESTATE_REWARDS)
					PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - MONITOR_TIME_UP_END_REASON - Time expired at ", GET_CLOUD_TIME_AS_INT(),", moving to rewards")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC FLOAT GET_MISSION_ENTITY_DAMAGE_SCALE(MODEL_NAMES model)

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_MUSCLE_OUT
			RETURN 0.25
	ENDSWITCH
	
	SWITCH model
		CASE BLIMP3
			RETURN 0.13
		CASE COGNOSCENTI
			RETURN 0.1		// 5 RPG hits
	ENDSWITCH
	
	RETURN 0.2
ENDFUNC

FUNC BOOL SHOULD_LOCK_REAR_DOORS()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_STAFF
		CASE MBV_FLYER_PROMOTION
		CASE MBV_PRIVATE_TAXI
		CASE MBV_SNAPMATIC
		CASE MBV_VIP_RESCUE
		CASE MBV_COLLECT_EQUIPMENT
		CASE MBV_STOLEN_SUPPLIES // oddly caused url:bugstar:4830742 - Stolen Supplies - Local player cannot re-enter the Supply Truck if they exit the vehicle if TRUE
		CASE MBV_SUPPLY_RUN
		CASE MBV_BLIMP_PROMOTION
		CASE MBV_COLLECT_DJ_HOOKED
			RETURN FALSE
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL DOES_MISSION_ENTITY_VEHICLE_NEED_HOTWIRED(INT iMissionEntity)
	
	UNUSED_PARAMETER(iMissionEntity)
	
//	SWITCH GET_MEGA_BUSINESS_VARIATION()
//		CASE MBV_COLLECT_DJ
//			RETURN FALSE
//	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL DOES_MISSION_ENTITY_VEHICLE_NEED_BROKEN_INTO(INT iMissionEntity)
	
	UNUSED_PARAMETER(iMissionEntity)
	
//	SWITCH GET_MEGA_BUSINESS_VARIATION()
//		CASE MBV_COLLECT_DJ
//			RETURN FALSE
//	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_BE_LOCKED_ON_SPAWN(INT iMissionEntity)
	
	UNUSED_PARAMETER(iMissionEntity)
	
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_MUSCLE_OUT
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_SET_VEHICLE_STRONG(INT iMissionEntity)
	
	UNUSED_PARAMETER(iMissionEntity)
	
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_SUPPLY_RUN
		CASE MBV_MUSCLE_OUT
			RETURN FALSE
		
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_SET_VEHICLE_BULLETPROOF_TYRES(INT iMissionEntity)
	
	UNUSED_PARAMETER(iMissionEntity)
	
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_MUSCLE_OUT
			RETURN FALSE
		
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_SET_VEHICLE_DMG_MP(INT iMissionEntity)
	
	UNUSED_PARAMETER(iMissionEntity)
	
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_SUPPLY_RUN
			RETURN FALSE
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_SET_VEHICLE_DMG_SCALE(INT iMissionEntity)
	
	UNUSED_PARAMETER(iMissionEntity)
	
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_SUPPLY_RUN
//		CASE MBV_MUSCLE_OUT
			RETURN FALSE
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_SET_RAMMING(INT iMissionEntity)
	
	UNUSED_PARAMETER(iMissionEntity)
	
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_SUPPLY_RUN
		CASE MBV_MUSCLE_OUT
			RETURN FALSE
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DISABLE_STAND_ON_ROOF(INT iMissionEntity)
	
	UNUSED_PARAMETER(iMissionEntity)
	
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_SUPPLY_RUN
		CASE MBV_STOLEN_SUPPLIES
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC GB_CONTRABAND_TYPE GET_CONTRABAND_TYPE()

	SWITCH serverBD.iFMMCType
		CASE FMMC_TYPE_FMBB_SELL				RETURN GBCT_MBS
		CASE FMMC_TYPE_FMBB_CLUB_MANAGEMENT		RETURN GBCT_CLUB
	ENDSWITCH
	
	RETURN GBCT_MBS

ENDFUNC

PROC SETUP_STRETCHED_PATRIOT_MODS(VEHICLE_INDEX vehID)

	VEHICLE_SETUP_STRUCT_MP sData
	sData.VehicleSetup.eModel = PATRIOT2
	sData.VehicleSetup.tlPlateText = "PR2NCE"
	sData.VehicleSetup.iPlateIndex = 3
	sData.VehicleSetup.iColour1 = 112
	sData.VehicleSetup.iColour2 = 120
	sData.VehicleSetup.iColourExtra1 = 145
	sData.VehicleSetup.iColourExtra2 = 156
	sData.iColour5 = 1
	sData.iColour6 = 132
	sData.iLivery2 = 0
	sData.VehicleSetup.iWindowTintColour = 1
	sData.VehicleSetup.iWheelType = 7
	sData.VehicleSetup.iTyreR = 255
	sData.VehicleSetup.iTyreG = 255
	sData.VehicleSetup.iTyreB = 255
	sData.VehicleSetup.iNeonR = 35
	sData.VehicleSetup.iNeonG = 1
	sData.VehicleSetup.iNeonB = 255
	SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
	SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
	SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
	SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
	sData.VehicleSetup.iModIndex[MOD_WING_R] = 2
	sData.VehicleSetup.iModIndex[MOD_HORN] = 45
	sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
	sData.VehicleSetup.iModIndex[MOD_WHEELS] = 21
	SET_VEHICLE_SETUP_MP(vehID, sData)

	PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - SETUP_STRETCHED_PATRIOT_MODS - Mission entity patriot2 has been set up like Tony's PR2NCE stretched patriot")
ENDPROC

FUNC INT GET_LIVERY_FOR_DJ()

	NIGHTCLUB_DJ_ID eDJID = GET_PLAYERS_NIGHTCLUB_ACTIVE_DJ(GB_GET_LOCAL_PLAYER_GANG_BOSS())

	SWITCH eDJID
		CASE DJ_SOLOMUN				RETURN 18
		CASE DJ_TALE_OF_US 			RETURN 16
		CASE DJ_DIXON				RETURN 19
		CASE DJ_BLACK_MADONNA		RETURN 17
	ENDSWITCH
	
	RETURN -1

ENDFUNC

PROC APPLY_VARIATION_SPECIFIC_MODS_TO_MISSION_ENTITY(INT iMissionEntity, VEHICLE_INDEX vehID)

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_STAFF
			SETUP_TONY_LIMO_MODS(vehID)
		BREAK
		CASE MBV_COLLECT_DJ_CRASH
		CASE MBV_COLLECT_DJ_STOLEN_BAG
		CASE MBV_COLLECT_DJ_HOOKED
			SETUP_TONY_LIMO_MODS(vehID)
			SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(vehId, FALSE)
		BREAK
		CASE MBV_PRIVATE_TAXI
			SWITCH GET_MEGA_BUSINESS_SUBVARIATION()
				CASE MBSV_PRIVATE_TAXI_0
				CASE MBSV_PRIVATE_TAXI_1
					SETUP_TONY_LIMO_MODS(vehID)
				BREAK
				CASE MBSV_PRIVATE_TAXI_2
				CASE MBSV_PRIVATE_TAXI_3
				CASE MBSV_PRIVATE_TAXI_4
				CASE MBSV_PRIVATE_TAXI_5
				CASE MBSV_PRIVATE_TAXI_6
				CASE MBSV_PRIVATE_TAXI_7
					SETUP_STRETCHED_PATRIOT_MODS(vehID)
				BREAK
			ENDSWITCH
		BREAK
		CASE MBV_SNAPMATIC
			SETUP_STRETCHED_PATRIOT_MODS(vehID)
		BREAK
		CASE MBV_SUPPLY_RUN
			IF DOES_EXTRA_EXIST(vehId, 1)
				SET_VEHICLE_EXTRA(vehId, 1, TRUE)
			ENDIF
			IF DOES_EXTRA_EXIST(vehId, 2)
				SET_VEHICLE_EXTRA(vehId, 2, TRUE)
			ENDIF
			IF DOES_EXTRA_EXIST(vehId, 3)
				SET_VEHICLE_EXTRA(vehId, 3, TRUE)
			ENDIF
			IF DOES_EXTRA_EXIST(vehId, 4)
				SET_VEHICLE_EXTRA(vehId, 4, TRUE)
			ENDIF
			IF DOES_EXTRA_EXIST(vehId, 5)
				SET_VEHICLE_EXTRA(vehId, 5, TRUE)
			ENDIF
			IF DOES_EXTRA_EXIST(vehId, 6)
				SET_VEHICLE_EXTRA(vehId, 6, TRUE)
			ENDIF
			IF DOES_EXTRA_EXIST(vehId, 7)
				SET_VEHICLE_EXTRA(vehId, 7, FALSE)
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - SET_VEHICLE_EXTRA(vehId, 7, TRUE)")
			ENDIF
			IF DOES_EXTRA_EXIST(vehId, 8)
				SET_VEHICLE_EXTRA(vehId, 8, TRUE)
			ENDIF
			IF DOES_EXTRA_EXIST(vehId, 9)
				SET_VEHICLE_EXTRA(vehId, 9, TRUE)
			ENDIF
		BREAK
		CASE MBV_STOLEN_SUPPLIES
			IF DOES_EXTRA_EXIST(vehId, 1)
				SET_VEHICLE_EXTRA(vehId, 1, TRUE)
			ENDIF
			IF DOES_EXTRA_EXIST(vehId, 2)
				SET_VEHICLE_EXTRA(vehId, 2, TRUE)
			ENDIF
			IF DOES_EXTRA_EXIST(vehId, 3)
				SET_VEHICLE_EXTRA(vehId, 3, TRUE)
			ENDIF
			IF DOES_EXTRA_EXIST(vehId, 4)
				SET_VEHICLE_EXTRA(vehId, 4, TRUE)
			ENDIF
			IF DOES_EXTRA_EXIST(vehId, 5)
				SET_VEHICLE_EXTRA(vehId, 5, TRUE)
			ENDIF
			IF DOES_EXTRA_EXIST(vehId, 6)
				SET_VEHICLE_EXTRA(vehId, 6, TRUE)
			ENDIF
			IF DOES_EXTRA_EXIST(vehId, 7)
				SET_VEHICLE_EXTRA(vehId, 7, FALSE)
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - SET_VEHICLE_EXTRA(vehId, 7, TRUE)")
			ENDIF
			IF DOES_EXTRA_EXIST(vehId, 8)
				SET_VEHICLE_EXTRA(vehId, 8, TRUE)
			ENDIF
			IF DOES_EXTRA_EXIST(vehId, 9)
				SET_VEHICLE_EXTRA(vehId, 9, TRUE)
			ENDIF
			
			SET_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_REAR_LEFT, FALSE, TRUE)
			SET_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_REAR_RIGHT, FALSE, TRUE)
			SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehId, SC_DOOR_REAR_LEFT, FALSE)
			SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehId, SC_DOOR_REAR_RIGHT, FALSE)

		BREAK
		CASE MBV_VIP_RESCUE
			SWITCH GET_MEGA_BUSINESS_SUBVARIATION()
				CASE MBSV_VIP_RESCUE_0
					SET_VEHICLE_SIREN(vehID, TRUE)
//					SET_VEHICLE_HAS_MUTED_SIRENS(vehID, TRUE)
				BREAK
				CASE MBSV_VIP_RESCUE_1
					SET_VEHICLE_SIREN(vehID, TRUE)
//					SET_VEHICLE_HAS_MUTED_SIRENS(vehID, TRUE)
					SET_VEHICLE_DOOR_OPEN(vehID, SC_DOOR_REAR_RIGHT, TRUE, TRUE)
				BREAK
				CASE MBSV_VIP_RESCUE_2
					SET_VEHICLE_SIREN(vehID, TRUE)
//					SET_VEHICLE_HAS_MUTED_SIRENS(vehID, TRUE)
				BREAK
				CASE MBSV_VIP_RESCUE_3
					SET_VEHICLE_SIREN(vehID, TRUE)
//					SET_VEHICLE_HAS_MUTED_SIRENS(vehID, TRUE)
					SET_VEHICLE_DOOR_OPEN(vehID, SC_DOOR_REAR_RIGHT, TRUE, TRUE)
				BREAK
			ENDSWITCH
		BREAK
		CASE MBV_FLYER_PROMOTION
			SET_VEHICLE_COLOURS(vehID, 120, 120)
		BREAK
		CASE MBV_MUSCLE_OUT
			SET_VEHICLE_LIGHTS(vehID, FORCE_VEHICLE_LIGHTS_ON)
			SETUP_VEHICLE_COSMETICS(serverBD.sMissionEntity[iMissionEntity].modelName, vehId) 
		BREAK
		CASE MBV_COLLECT_EQUIPMENT
//			IF DOES_EXTRA_EXIST(vehId, 1)
//				SET_VEHICLE_EXTRA(vehId, 1, TRUE)
//			ENDIF

			SET_VEHICLE_SIREN(vehID, TRUE)
			SET_VEHICLE_MOD_KIT(vehid, 0)
			SET_VEHICLE_MOD(vehid, MOD_LIVERY, 0)
			
			SET_VEHICLE_ENGINE_ON(vehID, TRUE, TRUE)
			SET_VEHICLE_LIGHTS(vehID, FORCE_VEHICLE_LIGHTS_ON)
		BREAK
		CASE MBV_COLLECT_DJ_COLLECTOR
			SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(vehId, FALSE)
			SETUP_VEHICLE_COSMETICS(serverBD.sMissionEntity[iMissionEntity].modelName, vehId) 
		BREAK
		CASE MBV_BLIMP_PROMOTION
			SET_VEHICLE_MOD_KIT(vehid, 0)
			SET_VEHICLE_MOD(vehid, MOD_LIVERY, GET_LIVERY_FOR_DJ())
			SET_VEHICLE_RADIO_ENABLED(vehId, FALSE)
			SET_AIRCRAFT_COUNTERMEASURE_TYPE(vehid,1)
			ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehId,"DLC_BTL_Blimp_Promotion_Mixgroup")
		BREAK
	ENDSWITCH
ENDPROC

FUNC INT GET_DISPLAY_SLOT_FROM_MISSION_ENTITY_MODEL(MODEL_NAMES eModel)

	SWITCH eModel
		CASE SPEEDO4		RETURN DISPLAY_SLOT_CLUBSPEEDO
		CASE MULE4			RETURN DISPLAY_SLOT_CLUBMULE
		CASE POUNDER2		RETURN DISPLAY_SLOT_CLUBPOUNDER
	ENDSWITCH

	RETURN -1
ENDFUNC

PROC CHECK_FOR_AVAILABLE_UPGRADES(VEHICLE_INDEX vehID, MODEL_NAMES model)
	BOOL bUpgraded = FALSE

	IF IS_LOCAL_BIT_SET(eLOCALBITSET_DO_UPGRADE_CALL_IF_FAIL)
		EXIT
	ENDIF

	SWITCH model
		CASE SPEEDO4
			IF GET_VEHICLE_MOD(vehID, MOD_WING_R) != -1
				bUpgraded = TRUE
				SET_SERVER_BIT(eSERVERBITSET_SELL_VEHICLE_HAS_PROXY_MINES)
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - CHECK_FOR_AVAILABLE_UPGRADES - SPEEDO4 - Vehicle has MOD_WING_R upgrade, no upgrade phonecall needed")
			ENDIF
			IF GET_VEHICLE_MOD(vehID, MOD_ROOF) != -1
				bUpgraded = TRUE
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - CHECK_FOR_AVAILABLE_UPGRADES - SPEEDO4 - Vehicle has MOD_ROOF upgrade, no upgrade phonecall needed")
			ENDIF
			IF GET_VEHICLE_MOD(vehID, MOD_CHASSIS) != -1
				bUpgraded = TRUE
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - CHECK_FOR_AVAILABLE_UPGRADES - SPEEDO4 - Vehicle has MOD_CHASSIS upgrade, no upgrade phonecall needed")
			ENDIF
		BREAK
		CASE MULE4
			IF GET_VEHICLE_MOD(vehID, MOD_WING_R) != -1
				bUpgraded = TRUE
				SET_SERVER_BIT(eSERVERBITSET_SELL_VEHICLE_HAS_PROXY_MINES)
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - CHECK_FOR_AVAILABLE_UPGRADES - MULE4 - Vehicle has MOD_WING_R upgrade, no upgrade phonecall needed")
			ENDIF
			IF GET_VEHICLE_MOD(vehID, MOD_ROOF) != -1
				bUpgraded = TRUE
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - CHECK_FOR_AVAILABLE_UPGRADES - MULE4 - Vehicle has MOD_ROOF upgrade, no upgrade phonecall needed")
			ENDIF
			IF GET_VEHICLE_MOD(vehID, MOD_BUMPER_F) != -1
				bUpgraded = TRUE
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - CHECK_FOR_AVAILABLE_UPGRADES - MULE4 - Vehicle has MOD_BUMPER_F upgrade, no upgrade phonecall needed")
			ENDIF
			IF GET_VEHICLE_MOD(vehID, MOD_CHASSIS) != -1
				bUpgraded = TRUE
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - CHECK_FOR_AVAILABLE_UPGRADES - MULE4 - Vehicle has MOD_CHASSIS upgrade, no upgrade phonecall needed")
			ENDIF
		BREAK
		CASE POUNDER2
			IF GET_VEHICLE_MOD(vehID, MOD_BUMPER_R) != -1
				bUpgraded = TRUE
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - CHECK_FOR_AVAILABLE_UPGRADES - POUNDER2 - Vehicle has MOD_BUMPER_R upgrade, no upgrade phonecall needed")
			ENDIF
			IF GET_VEHICLE_MOD(vehID, MOD_WING_R) != -1
				bUpgraded = TRUE
				SET_SERVER_BIT(eSERVERBITSET_SELL_VEHICLE_HAS_PROXY_MINES)
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - CHECK_FOR_AVAILABLE_UPGRADES - POUNDER2 - Vehicle has MOD_WING_R upgrade, no upgrade phonecall needed")
			ENDIF
			IF GET_VEHICLE_MOD(vehID, MOD_ROOF) != -1
				bUpgraded = TRUE
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - CHECK_FOR_AVAILABLE_UPGRADES - POUNDER2 - Vehicle has MOD_ROOF upgrade, no upgrade phonecall needed")
			ENDIF
			IF GET_VEHICLE_MOD(vehID, MOD_CHASSIS) != -1
				bUpgraded = TRUE
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - CHECK_FOR_AVAILABLE_UPGRADES - POUNDER2 - Vehicle has MOD_CHASSIS upgrade, no upgrade phonecall needed")
			ENDIF
		BREAK
	ENDSWITCH
	
	IF bUpgraded = FALSE
		SET_LOCAL_BIT(eLOCALBITSET_DO_UPGRADE_CALL_IF_FAIL)
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - CHECK_FOR_AVAILABLE_UPGRADES - Vehicle has no upgrades, do upgrade phonecall if we fail")
	ENDIF
ENDPROC

PROC SET_MISSION_ENTITY_SPAWN_ATTRIBUTES(INT iMissionEntity, MODEL_NAMES model)
	VEHICLE_INDEX vehId = NET_TO_VEH(serverBD.sMissionEntity[iMissionEntity].netId)
	
	PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - Setting up spawn attributes for mission entity ", iMissionEntity)
	
	INT iSaveSlot
	IF IS_MEGA_BUSINESS_VARIATION_A_SELL_MISSION(GET_MEGA_BUSINESS_VARIATION())
	
		MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(GET_DISPLAY_SLOT_FROM_MISSION_ENTITY_MODEL(model), iSaveSlot)
		IF iSaveSlot != -1
			SET_VEHICLE_SETUP_MP(vehId, g_MpSavedVehicles[iSaveSlot].vehicleSetupMP)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - Mission entity vehicle has been set up with SET_VEHICLE_SETUP_MP")
			
			// Set up armour levels based on the vehicles' armour upgrade level - url:bugstar:4737663
			
			INT iArmourUpgrade = GET_VEHICLE_MOD(vehID, MOD_CHASSIS)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - iArmourUpgrade = ", iArmourUpgrade)
			
			SWITCH model
				CASE SPEEDO4
					SWITCH iArmourUpgrade
						// No armour upgrade = vehicle is destroyed in 1 RPG hit (normal behaviour)
						CASE -1
							PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - SPEEDO armour level -1, not setting any damage scaling")
						BREAK
						
						// Armour upgrade level 1 = vehicle is destroyed in 2 RPG hits
						CASE 0
							SET_VEHICLE_DAMAGE_SCALE(vehId, g_sMPTunables.fBB_SELL_MISSIONS_DELIVERY_VEHICLE_ARMOR_LEVEL_1_DAMAGE_MODIFIER)
							PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - SPEEDO armour level 0 - SET_VEHICLE_DAMAGE_SCALE = ", g_sMPTunables.fBB_SELL_MISSIONS_DELIVERY_VEHICLE_ARMOR_LEVEL_1_DAMAGE_MODIFIER)
						BREAK
						
						// Armour upgrade level 2 = vehicle is destroyed in 3 RPG hits
						CASE 1
							SET_VEHICLE_DAMAGE_SCALE(vehId, g_sMPTunables.fBB_SELL_MISSIONS_DELIVERY_VEHICLE_ARMOR_LEVEL_2_DAMAGE_MODIFIER)
							PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - SPEEDO armour level 1 - SET_VEHICLE_DAMAGE_SCALE = ", g_sMPTunables.fBB_SELL_MISSIONS_DELIVERY_VEHICLE_ARMOR_LEVEL_2_DAMAGE_MODIFIER)
						BREAK
						
						// Armour upgrade level 3 = vehicle is destroyed in 4 RPG hits
						CASE 2
							SET_VEHICLE_DAMAGE_SCALE(vehId, g_sMPTunables.fBB_SELL_MISSIONS_DELIVERY_VEHICLE_ARMOR_LEVEL_3_DAMAGE_MODIFIER)
							PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - SPEEDO armour level 2 - SET_VEHICLE_DAMAGE_SCALE = ", g_sMPTunables.fBB_SELL_MISSIONS_DELIVERY_VEHICLE_ARMOR_LEVEL_3_DAMAGE_MODIFIER)
						BREAK
					ENDSWITCH
				BREAK
				CASE MULE4
					SWITCH iArmourUpgrade
						// No armour upgrade = vehicle is destroyed in 1 RPG hit (normal behaviour)
						CASE -1
							PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - MULE armour level -1, not setting any damage scaling")
						BREAK
						
						// Armour upgrade level 1 = vehicle is destroyed in 2 RPG hits
						CASE 0
							SET_VEHICLE_DAMAGE_SCALE(vehId, g_sMPTunables.fBB_SELL_MISSIONS_DELIVERY_VEHICLE_ARMOR_LEVEL_1_DAMAGE_MODIFIER)
							PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - MULE armour level 0 - SET_VEHICLE_DAMAGE_SCALE = ", g_sMPTunables.fBB_SELL_MISSIONS_DELIVERY_VEHICLE_ARMOR_LEVEL_1_DAMAGE_MODIFIER)
						BREAK
						
						// Armour upgrade level 2 = vehicle is destroyed in 3 RPG hits
						CASE 1
							SET_VEHICLE_DAMAGE_SCALE(vehId, g_sMPTunables.fBB_SELL_MISSIONS_DELIVERY_VEHICLE_ARMOR_LEVEL_2_DAMAGE_MODIFIER)
							PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - MULE armour level 1 - SET_VEHICLE_DAMAGE_SCALE = ", g_sMPTunables.fBB_SELL_MISSIONS_DELIVERY_VEHICLE_ARMOR_LEVEL_2_DAMAGE_MODIFIER)
						BREAK
						
						// Armour upgrade level 3 = vehicle is destroyed in 4 RPG hits
						CASE 2
							SET_VEHICLE_DAMAGE_SCALE(vehId, g_sMPTunables.fBB_SELL_MISSIONS_DELIVERY_VEHICLE_ARMOR_LEVEL_3_DAMAGE_MODIFIER)
							PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - MULE armour level 2 - SET_VEHICLE_DAMAGE_SCALE = ", g_sMPTunables.fBB_SELL_MISSIONS_DELIVERY_VEHICLE_ARMOR_LEVEL_3_DAMAGE_MODIFIER)
						BREAK
					ENDSWITCH
				BREAK
				CASE POUNDER2
					SWITCH iArmourUpgrade
						// No armour upgrade = vehicle is destroyed in 1 RPG hit (normal behaviour)
						CASE -1
							PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - POUNDER armour level -1, not setting any damage scaling")
						BREAK
						
						// Armour upgrade level 1 = vehicle is destroyed in 2 RPG hits
						CASE 0
							SET_VEHICLE_DAMAGE_SCALE(vehId, g_sMPTunables.fBB_SELL_MISSIONS_DELIVERY_VEHICLE_ARMOR_LEVEL_1_DAMAGE_MODIFIER)
							PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - POUNDER armour level 0 - SET_VEHICLE_DAMAGE_SCALE = ", g_sMPTunables.fBB_SELL_MISSIONS_DELIVERY_VEHICLE_ARMOR_LEVEL_1_DAMAGE_MODIFIER)
						BREAK
						
						// Armour upgrade level 2 = vehicle is destroyed in 3 RPG hits
						CASE 1
							SET_VEHICLE_DAMAGE_SCALE(vehId, g_sMPTunables.fBB_SELL_MISSIONS_DELIVERY_VEHICLE_ARMOR_LEVEL_2_DAMAGE_MODIFIER)
							PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - POUNDER armour level 1 - SET_VEHICLE_DAMAGE_SCALE = ", g_sMPTunables.fBB_SELL_MISSIONS_DELIVERY_VEHICLE_ARMOR_LEVEL_2_DAMAGE_MODIFIER)
						BREAK
						
						// Armour upgrade level 3 = vehicle is destroyed in 4 RPG hits
						CASE 2
							SET_VEHICLE_DAMAGE_SCALE(vehId, g_sMPTunables.fBB_SELL_MISSIONS_DELIVERY_VEHICLE_ARMOR_LEVEL_3_DAMAGE_MODIFIER)
							PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - POUNDER armour level 2 - SET_VEHICLE_DAMAGE_SCALE = ", g_sMPTunables.fBB_SELL_MISSIONS_DELIVERY_VEHICLE_ARMOR_LEVEL_3_DAMAGE_MODIFIER)
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
			
			CHECK_FOR_AVAILABLE_UPGRADES(vehId, model)
		ELSE
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - SAVE SLOT FOR VEHICLE NOT FOUND - iSaveSlot = ", iSaveSlot, "; vehicle model = ", GET_MODEL_NAME_FOR_DEBUG(model))
			SCRIPT_ASSERT("[MB_MISSION] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - SAVE SLOT FOR VEHICLE NOT FOUND - See logs for more details.")
		ENDIF
		
	ELSE
	
		APPLY_VARIATION_SPECIFIC_MODS_TO_MISSION_ENTITY(iMissionEntity, vehID)
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - Mission entity vehicle has been set up with APPLY_VARIATION_SPECIFIC_MODS_TO_MISSION_ENTITY")
		
		IF SHOULD_SET_VEHICLE_BULLETPROOF_TYRES(iMissionEntity)
			SET_VEHICLE_TYRES_CAN_BURST(vehId, FALSE)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - SET_VEHICLE_TYRES_CAN_BURST(vehId, FALSE)")
		ENDIF
		
		IF SHOULD_SET_VEHICLE_STRONG(iMissionEntity)
			SET_VEHICLE_STRONG(vehId, TRUE)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - Setting vehicle strong.")
		ENDIF

		IF SHOULD_SET_VEHICLE_DMG_SCALE(iMissionEntity)
			SET_VEHICLE_DAMAGE_SCALE(vehId, GET_MISSION_ENTITY_DAMAGE_SCALE(model))
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - SET_VEHICLE_DAMAGE_SCALE = ", GET_MISSION_ENTITY_DAMAGE_SCALE(model))
		ENDIF
		
	ENDIF
	
	IF GET_MEGA_BUSINESS_VARIATION() = MBV_MUSCLE_OUT
		SET_VEHICLE_ENGINE_CAN_DEGRADE(vehId, FALSE)
		SET_VEHICLE_ENGINE_HEALTH(vehId, 2000)
	ENDIF
	
	IF SHOULD_SET_VEHICLE_DMG_MP(iMissionEntity)
		SET_VEHICLE_USES_MP_PLAYER_DAMAGE_MULTIPLIER(vehId, FALSE)
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - SET_VEHICLE_USES_MP_PLAYER_DAMAGE_MULTIPLIER = FALSE")
	ENDIF
	
	SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(vehId, FALSE)
	PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE = FALSE")
	
	SET_ENTITY_HEALTH(vehId, GET_MISSION_ENTITY_START_HEALTH(iMissionEntity))
	PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - SET_ENTITY_HEALTH = ", GET_MISSION_ENTITY_START_HEALTH(iMissionEntity))
	
	IF SHOULD_DISABLE_STAND_ON_ROOF(iMissionEntity)
		SET_DISABLE_PED_STAND_ON_TOP(vehId, TRUE)
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - SET_DISABLE_PED_STAND_ON_TOP ")
	ENDIF
	
	IF SHOULD_SET_RAMMING(iMissionEntity)
		CALL_SET_ALLOW_RAMMING_SOOP_OR_RAMP(vehId)
	ENDIF
	
	SET_MENTAL_STATE_WILL_IGNORE_THIS_VEHICLE(vehId, TRUE)
	
	// Lock stuff and entering
	IF GET_MEGA_BUSINESS_VARIATION() != MBV_COLLECT_STAFF
	AND GET_MEGA_BUSINESS_VARIATION() != MBV_COLLECT_DJ_COLLECTOR
	AND GET_MEGA_BUSINESS_VARIATION() != MBV_COLLECT_DJ_CRASH
	AND GET_MEGA_BUSINESS_VARIATION() != MBV_COLLECT_DJ_STOLEN_BAG
	AND GET_MEGA_BUSINESS_VARIATION() != MBV_PRIVATE_TAXI
		SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(vehId, TRUE)
	ENDIF
	SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(vehId, TRUE)
	
	IF DOES_MISSION_ENTITY_VEHICLE_NEED_HOTWIRED(iMissionEntity)
		SET_VEHICLE_NEEDS_TO_BE_HOTWIRED(vehId, TRUE)
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - SET_VEHICLE_NEEDS_TO_BE_HOTWIRED(vehId, TRUE) ")
	ENDIF
	
	IF DOES_MISSION_ENTITY_VEHICLE_NEED_BROKEN_INTO(iMissionEntity)
		SET_VEHICLE_DOORS_LOCKED(vehId, VEHICLELOCK_LOCKED_BUT_CAN_BE_DAMAGED)
//		SET_VEHICLE_DOORS_LOCKED(vehId, VEHICLELOCK_LOCKED)
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - VEHICLELOCK_LOCKED_BUT_CAN_BE_DAMAGED ")
	ELSE
		BOOL bLock = SHOULD_VEHICLE_BE_LOCKED_ON_SPAWN(iMissionEntity)
		SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehId, bLock)
	ENDIF

	SET_VEHICLE_DOORS_LOCKED_FOR_NON_SCRIPT_PLAYERS(vehId, TRUE)
	
	IF SHOULD_LOCK_REAR_DOORS()
		IF GET_MEGA_BUSINESS_VARIATION() != MBV_COLLECT_DJ_CRASH
			SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(vehId, ENUM_TO_INT(SC_DOOR_REAR_LEFT), VEHICLELOCK_LOCKED)
		ENDIF
		SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(vehId, ENUM_TO_INT(SC_DOOR_REAR_RIGHT), VEHICLELOCK_LOCKED)
		IF GET_MEGA_BUSINESS_VARIATION() != MBV_COLLECT_DJ_CRASH
			SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehId, SC_DOOR_REAR_LEFT, FALSE)
		ENDIF
		SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehId, SC_DOOR_REAR_RIGHT, FALSE)
		SET_OPEN_REAR_DOORS_ON_EXPLOSION(vehId, FALSE)
	ENDIF
	
	INT iDoor
	REPEAT SC_MAX_DOORS iDoor
		IF SHOULD_LOCK_DOOR_FOR_PLAYERS(INT_TO_ENUM(SC_DOOR_LIST, iDoor))
			SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(vehId, iDoor, GET_VEHICLE_DOOR_LOCKOUT_TYPE())
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - locked door ", GET_VEHICLE_DOOR_NAME(INT_TO_ENUM(SC_DOOR_LIST, iDoor)))
		ENDIF
	ENDREPEAT
	
	IF NOT DOES_MEGA_BUSINESS_VARIATION_ALLOW_CARGOBOB_DELIVERY()
		SET_VEHICLE_AUTOMATICALLY_ATTACHES(vehId, FALSE)				
		SET_VEHICLE_DISABLE_TOWING(vehId, TRUE)
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - SET_VEHICLE_AUTOMATICALLY_ATTACHES(vehId, FALSE)")
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - SET_VEHICLE_DISABLE_TOWING(vehId, TRUE)")
	ENDIF
	
	IF GET_MEGA_BUSINESS_VARIATION() = MBV_COLLECT_DJ_CRASH
	OR GET_MEGA_BUSINESS_VARIATION() = MBV_COLLECT_DJ_STOLEN_BAG
	OR GET_MEGA_BUSINESS_VARIATION() = MBV_COLLECT_DJ_COLLECTOR
	OR GET_MEGA_BUSINESS_VARIATION() = MBV_COLLECT_EQUIPMENT
	OR GET_MEGA_BUSINESS_VARIATION() = MBV_COLLECT_STAFF
//		SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(vehId, ENUM_TO_INT(SC_DOOR_FRONT_RIGHT), VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
		IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
			SET_VEHICLE_EXCLUSIVE_DRIVER(vehId, localPlayerPed)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - SET_VEHICLE_EXCLUSIVE_DRIVER to boss.")
		ELSE
			IF IS_NET_PLAYER_OK(GB_GET_LOCAL_PLAYER_GANG_BOSS(), FALSE)
				SET_VEHICLE_EXCLUSIVE_DRIVER(vehId, GET_PLAYER_PED(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - SET_VEHICLE_EXCLUSIVE_DRIVER to boss.")
			ENDIF
		ENDIF
	ENDIF
	
	IF GET_MEGA_BUSINESS_VARIATION() = MBV_COLLECT_EQUIPMENT
		ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehId, "DLC_BTL_PBUS2_Music_Boost_Mixgroup")
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - [4861122] ADD_ENTITY_TO_AUDIO_MIX_GROUP, (DLC_BTL_PBUS2_Music_Boost_Mixgroup)")
	ENDIF
	
	// Lock on stuff
	SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(vehId, FALSE)
	SET_VEHICLE_ALLOW_NO_PASSENGERS_LOCKON(vehId, FALSE)
	
	SET_ENTITY_LOD_DIST(vehId, 1200)
	
	SET_VEHICLE_ON_GROUND_PROPERLY(vehId)
	
	GB_SET_VEHICLE_AS_CONTRABAND(vehId, iMissionEntity, GET_CONTRABAND_TYPE())
	
	// Decorator stuff
	IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh", DECOR_TYPE_INT)
    	DECOR_SET_INT(vehId, "Not_Allow_As_Saved_Veh", MP_NOT_ALLOWED_AS_SAVED_VEHICLE_DEC_SIMEON)
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] Contraband vehicle set as 'Not_Allow_As_Saved_Veh'.")
	ENDIF
	
	IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
		INT iDecoratorValue
		IF DECOR_EXIST_ON(vehId, "MPBitset")
			iDecoratorValue = DECOR_GET_INT(vehId, "MPBitset")
		ENDIF
		SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_PASSIVE_MODE_VEHICLE)
		SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_MODDABLE_VEHICLE)
		DECOR_SET_INT(vehId, "MPBitset", iDecoratorValue)
		
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - Mission entity vehicle set as 'MP_DECORATOR_BS_NON_PASSIVE_MODE_VEHICLE'.")
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - Mission entity vehicle set as 'MP_DECORATOR_BS_NON_MODDABLE_VEHICLE'.")
	ENDIF

	IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh",DECOR_TYPE_INT)
		DECOR_SET_INT(vehId,"Not_Allow_As_Saved_Veh",MP_NOT_ALLOWED_AS_SAVED_VEHICLE_DEC_SIMEON)
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - Mission entity vehicle set as 'MP_NOT_ALLOWED_AS_SAVED_VEHICLE_DEC_SIMEON'.")
	ENDIF
	
ENDPROC

FUNC BOOL IS_ANY_GANG_USING_THIS_SPAWN_LOCATION(INT iLocation)

	INT iLoop
	REPEAT NUM_NETWORK_PLAYERS iLoop
		IF INT_TO_NATIVE(PLAYER_INDEX, iLoop) != INVALID_PLAYER_INDEX()
			PLAYER_INDEX playerID = INT_TO_NATIVE(PLAYER_INDEX, iLoop)
		
			// Is player a boss
			IF GB_IS_PLAYER_BOSS_OF_A_GANG(playerID)
			
				// Does player own the same nightclub as the one we're launching from
				IF GET_PLAYERS_OWNED_NIGHTCLUB(playerID) = serverBD.nightclubID
				
					// Is player running the same FMMC type as the one we're running
					IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(playerID) = serverBD.iFMMCType
				
						// Then we need to check if this player is using this spawn location
						IF GlobalplayerBD_FM_3[NATIVE_TO_INT(playerID)].iMegaBusinessSpawnLocationUsed = iLocation
						
							PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - IS_ANY_GANG_USING_THIS_SPAWN_LOCATION - YES - ", GET_PLAYER_NAME(playerID), " is using spawn location ", iLocation, " at nightclub ", GET_NIGHTCLUB_NAME_FROM_ID(GET_PLAYERS_OWNED_NIGHTCLUB(playerID)))
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_ALWAYS_SPAWN_MISSION_ENTITY()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_COLLECTOR
		CASE MBV_COLLECT_EQUIPMENT
		CASE MBV_FLYER_PROMOTION
		CASE MBV_SUPPLY_RUN
		CASE MBV_VIP_RESCUE
		CASE MBV_STOLEN_SUPPLIES
		CASE MBV_MUSCLE_OUT
		CASE MBV_BLIMP_PROMOTION
			PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - SHOULD_ALWAYS_SPAWN_MISSION_ENTITY - YES")
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

PROC MAINTAIN_CREATE_MISSION_ENTITY(INT iMissionEntity)
	
	IF IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_A_PARTICIPANT_WANTS_ME_TO_SPAWN)
	AND NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_CREATED)
		MODEL_NAMES eMissionEntityModel = serverBD.sMissionEntity[iMissionEntity].modelName
		IF REQUEST_LOAD_MODEL(eMissionEntityModel)
			IF IS_VECTOR_ZERO(serverBD.sMissionEntity[iMissionEntity].vSpawnCoords)
				IF SHOULD_ALWAYS_SPAWN_MISSION_ENTITY()
					IF SHOULD_CREATE_VEHICLE_AT_OFFSET(iMissionEntity, TRUE)
						VECTOR vOffsetCoord = GET_CREATE_VEHICLE_FROM_OFFSET_VEHICLE_COORD(iMissionEntity, TRUE)
						FLOAT fOffsetHeading = GET_CREATE_VEHICLE_FROM_OFFSET_VEHICLE_HEADING(iMissionEntity, TRUE)
						
						serverBD.sMissionEntity[iMissionEntity].vSpawnCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOffsetCoord, fOffsetHeading, GET_CREATE_VEHICLE_FROM_OFFSET_OFFSET(iMissionEntity, TRUE)) 
						serverBD.sMissionEntity[iMissionEntity].fSpawnHeading = fOffsetHeading
					ELSE
						serverBD.sMissionEntity[iMissionEntity].vSpawnCoords 	= GET_MEGA_BUSINESS_MISSION_ENTITY_SPAWN_COORDS(iMissionEntity, GET_MEGA_BUSINESS_VARIATION(), GET_MEGA_BUSINESS_SUBVARIATION(), serverBD.nightclubID, DEFAULT)
						serverBD.sMissionEntity[iMissionEntity].fSpawnHeading 	= GET_MEGA_BUSINESS_MISSION_ENTITY_SPAWN_HEADING(iMissionEntity, GET_MEGA_BUSINESS_VARIATION(), GET_MEGA_BUSINESS_SUBVARIATION(), serverBD.nightclubID, DEFAULT)
					ENDIF
					
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_CREATE_MISSION_ENTITY - Coords: ", serverBD.sMissionEntity[iMissionEntity].vSpawnCoords, 
																													"; Heading ", serverBD.sMissionEntity[iMissionEntity].fSpawnHeading)
					
					DELETE_VEHICLE_GEN_VEHICLES_IN_AREA(serverBD.sMissionEntity[iMissionEntity].vSpawnCoords, 30.0)
					CLEAR_AREA_OF_VEHICLES(serverBD.sMissionEntity[iMissionEntity].vSpawnCoords, 30.0, FALSE, FALSE, FALSE, FALSE, TRUE)
				
				ELSE
					VECTOR vCoords, vSpawnHeading
					FLOAT fHeading
					SWITCH serverBd.iGetSpawnMissionEntityVehicleCoordsStage
						CASE 0
							CLEAR_CUSTOM_VEHICLE_NODES()
							
							INT iSpawnLocation
							WHILE NOT IS_VECTOR_ZERO(GET_MEGA_BUSINESS_MISSION_ENTITY_SPAWN_COORDS(iMissionEntity, GET_MEGA_BUSINESS_VARIATION(), GET_MEGA_BUSINESS_SUBVARIATION(), serverBD.nightclubID, iSpawnLocation))
								vCoords = GET_MEGA_BUSINESS_MISSION_ENTITY_SPAWN_COORDS(iMissionEntity, GET_MEGA_BUSINESS_VARIATION(), GET_MEGA_BUSINESS_SUBVARIATION(), serverBD.nightclubID, iSpawnLocation)
								fHeading = GET_MEGA_BUSINESS_MISSION_ENTITY_SPAWN_HEADING(iMissionEntity, GET_MEGA_BUSINESS_VARIATION(), GET_MEGA_BUSINESS_SUBVARIATION(), serverBD.nightclubID, iSpawnLocation)
								IF NOT IS_VECTOR_ZERO(vCoords)
									ADD_CUSTOM_VEHICLE_NODE(vCoords, fHeading)		
									PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_CREATE_MISSION_ENTITY - Adding custom vehicle node ", iSpawnLocation, " at ", vCoords, " for mission entity #", iMissionEntity)
								ENDIF
								iSpawnLocation++
							ENDWHILE	
							serverBd.iGetSpawnMissionEntityVehicleCoordsStage++
							PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_CREATE_MISSION_ENTITY - Added custom vehicle nodes. Moving on to find spawn coord.")
						BREAK
						
						CASE 1
							VEHICLE_SPAWN_LOCATION_PARAMS Params
							Params.bEnforceMinDistForCustomNodes 		= FALSE
							Params.bStartOfMissionPVSpawn 				= FALSE
							Params.bIgnoreCustomNodesForArea 			= TRUE
							Params.bAvoidSpawningInExclusionZones  		= FALSE
							Params.bIgnoreCustomNodesForMissionLaunch 	= TRUE
							Params.bCheckEntityArea 					= TRUE
							
							vCoords = GET_MEGA_BUSINESS_MISSION_ENTITY_SPAWN_COORDS(iMissionEntity, GET_MEGA_BUSINESS_VARIATION(), GET_MEGA_BUSINESS_SUBVARIATION(), serverBD.nightclubID, 0)
							vSpawnHeading = <<0.0, 0.0, 0.0>>
							
							IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(vCoords, vSpawnHeading, eMissionEntityModel, FALSE, serverBD.sMissionEntity[iMissionEntity].vSpawnCoords, serverBD.sMissionEntity[iMissionEntity].fSpawnHeading, Params)
								PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_CREATE_MISSION_ENTITY - Mission entity #", iMissionEntity, " has got safe spawn at ", serverBD.sMissionEntity[iMissionEntity].vSpawnCoords)
								serverBd.iGetSpawnMissionEntityVehicleCoordsStage++
								DELETE_VEHICLE_GEN_VEHICLES_IN_AREA(serverBD.sMissionEntity[iMissionEntity].vSpawnCoords, 30.0)
								CLEAR_AREA_OF_VEHICLES(serverBD.sMissionEntity[iMissionEntity].vSpawnCoords, 30.0, FALSE, FALSE, FALSE, FALSE, TRUE)
							ELSE
								PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_CREATE_MISSION_ENTITY - Failed to find spawn for mission entity #", iMissionEntity)
							ENDIF
						BREAK
						
						CASE 2
							
						BREAK
					ENDSWITCH
							
				ENDIF
			ELSE
				IF CREATE_NET_VEHICLE(	serverBD.sMissionEntity[iMissionEntity].netId, eMissionEntityModel, 
										serverBD.sMissionEntity[iMissionEntity].vSpawnCoords, 
										serverBD.sMissionEntity[iMissionEntity].fSpawnHeading)
					
					NETWORK_FADE_IN_ENTITY(NET_TO_VEH(serverBD.sMissionEntity[iMissionEntity].netId), TRUE)
					SET_MISSION_ENTITY_SPAWN_ATTRIBUTES(iMissionEntity, eMissionEntityModel)
					SET_MODEL_AS_NO_LONGER_NEEDED(eMissionEntityModel)	
					#IF IS_DEBUG_BUILD
					VECTOR vCoords = GET_ENTITY_COORDS(NET_TO_VEH(serverBD.sMissionEntity[iMissionEntity].netId))
					PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - MAINTAIN_CREATE_MISSION_ENTITY - Created mission entity #", iMissionEntity, " at vCoords: ", vCoords)
					#ENDIF
					serverBd.iGetSpawnMissionEntityVehicleCoordsStage = 0
					SET_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_CREATED)
					serverBD.iNumMissionEntitiesCreated++
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DOES_VEHICLE_NEED_CRATE(MODEL_NAMES vehModel)

	SWITCH vehModel
		CASE SPEEDO4	
		CASE MULE4	
		CASE MULE
		CASE POUNDER2
		CASE POLICET
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC MODEL_NAMES GET_CRATE_MODEL_FOR_VEHICLE(MODEL_NAMES vehModel)

	SWITCH vehModel
		CASE SPEEDO4
			RETURN gr_prop_gr_rsply_crate04b // Prop_box_wood04a
			
		CASE MULE4
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ba_Prop_Batle_Crates_Mule"))
			
		CASE POUNDER2
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ba_Prop_Batle_Crates_Pounder"))
			
		CASE MULE
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ba_prop_battle_crate_beer_double"))
			
		CASE POLICET
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ba_prop_battle_policet_seats"))
		
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC VECTOR GET_VEHICLE_CRATE_OFFSET(MODEL_NAMES vehModel)
	SWITCH vehModel
		CASE SPEEDO4
			RETURN <<0.0, -1.25, -0.1>>
			
		CASE MULE
			RETURN <<0.0, -1.25, -0.1>>
			
		CASE POLICET
		CASE MULE4
		CASE POUNDER2
			RETURN <<0.0, 0.0, 0.0>>
	ENDSWITCH
	
	RETURN <<0.0, -1.0, 0.0>>
ENDFUNC

FUNC VECTOR GET_VEHICLE_CRATE_ROTATION(MODEL_NAMES vehModel)
	SWITCH vehModel
		CASE SPEEDO4
			RETURN <<0.0, 0.0, 90.0>>
			
		CASE MULE
			RETURN <<0.0, 0.0, 90.0>>
			
		CASE POLICET
			RETURN <<0.0, 0.0, 0.0>>
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC BOOL SHOULD_CRATE_DETACH_WHEN_DEAD()
	
	RETURN FALSE

ENDFUNC

FUNC BOOL SHOULD_CRATE_HAVE_COLLISION()
	
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_STOLEN_SUPPLIES
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ATTACH_CRATE_TO_VEHICLE(OBJECT_INDEX objCrate, VEHICLE_INDEX vehID)

	PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - ATTACH_CRATE_TO_VEHICLE - Called")

	STRING sBone = "chassis_dummy"
	VECTOR vOffset = GET_VEHICLE_CRATE_OFFSET(GET_ENTITY_MODEL(vehID))
	VECTOR vRotation = GET_VEHICLE_CRATE_ROTATION(GET_ENTITY_MODEL(vehID))
	BOOL bDetachWhenDead = SHOULD_CRATE_DETACH_WHEN_DEAD()
	BOOL bDetachWhenRagdoll = FALSE
	BOOL bCollisions = SHOULD_CRATE_HAVE_COLLISION()
	
	IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(objCrate, vehID)
		ATTACH_ENTITY_TO_ENTITY(objCrate, vehID, GET_ENTITY_BONE_INDEX_BY_NAME(vehID, sBone), vOffset, vRotation, bDetachWhenDead, bDetachWhenRagdoll, bCollisions)
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - ATTACH_CRATE_TO_VEHICLE - Attaching crate to vehicle using ATTACH_ENTITY_TO_ENTITY. Bone = ", sBone)
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_DISABLE_COLLISIONS_FOR_VEHICLE_CRATE(INT iMissionEntity, VEHICLE_INDEX vehID)

	UNUSED_PARAMETER(iMissionEntity)

	SWITCH GET_ENTITY_MODEL(vehID)
		CASE SPEEDO4	
		CASE MULE4
		CASE POUNDER2
		CASE POLICET
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_CRATE_FOR_VEHICLE(INT iMissionEntity, VEHICLE_INDEX vehID)
	MODEL_NAMES crateModel = GET_CRATE_MODEL_FOR_VEHICLE(GET_ENTITY_MODEL(vehID))

	IF crateModel != DUMMY_MODEL_FOR_SCRIPT
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sMissionEntity[iMissionEntity].vehCrate)
			IF CAN_REGISTER_MISSION_OBJECTS( 1 )
				IF REQUEST_LOAD_MODEL(crateModel)
					IF CREATE_NET_OBJ(serverBD.sMissionEntity[iMissionEntity].vehCrate, crateModel, GET_ENTITY_COORDS(vehID)+<<0.0,0.0,150.0>>)
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_CRATE_FOR_VEHICLE - Crate created, now we'll attach it...")
						IF SHOULD_DISABLE_COLLISIONS_FOR_VEHICLE_CRATE(iMissionEntity, vehID)
							PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_CRATE_FOR_VEHICLE - SHOULD_DISABLE_COLLISIONS_FOR_VEHICLE_CRATE is TRUE, disabling collisions for crate ", iMissionEntity)
							SET_ENTITY_COMPLETELY_DISABLE_COLLISION(NET_TO_OBJ(serverBD.sMissionEntity[iMissionEntity].vehCrate), FALSE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF TAKE_CONTROL_OF_NET_ID(serverBD.sMissionEntity[iMissionEntity].vehCrate)
				IF ATTACH_CRATE_TO_VEHICLE(NET_TO_OBJ(serverBD.sMissionEntity[iMissionEntity].vehCrate), vehID)
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_CRATE_FOR_VEHICLE - Crate attached!")
					SET_MODEL_AS_NO_LONGER_NEEDED(crateModel)
					SET_ENTITY_LOD_DIST(NET_TO_OBJ(serverBD.sMissionEntity[iMissionEntity].vehCrate), 1200)
					SET_ENTITY_VISIBLE(NET_TO_OBJ(serverBD.sMissionEntity[iMissionEntity].vehCrate),TRUE)
					SET_ENTITY_INVINCIBLE(NET_TO_OBJ(serverBD.sMissionEntity[iMissionEntity].vehCrate),TRUE)
					IF DOES_ENTITY_HAVE_PHYSICS(NET_TO_OBJ(serverBD.sMissionEntity[iMissionEntity].vehCrate)) 
					AND GET_IS_ENTITY_A_FRAG(NET_TO_OBJ(serverBD.sMissionEntity[iMissionEntity].vehCrate))
						SET_DISABLE_BREAKING(NET_TO_OBJ(serverBD.sMissionEntity[iMissionEntity].vehCrate), TRUE)
						SET_DISABLE_FRAG_DAMAGE(NET_TO_OBJ(serverBD.sMissionEntity[iMissionEntity].vehCrate), TRUE)
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_CRATE_FOR_VEHICLE - SET_DISABLE_BREAKING(NET_TO_OBJ(serverBD.sMissionEntity[iMissionEntity].vehCrate), TRUE)")
					ENDIF
					SET_ENTITY_PROOFS(NET_TO_OBJ(serverBD.sMissionEntity[iMissionEntity].vehCrate), TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_CRATE_FOR_VEHICLE - crateModel = DUMMY_MODEL_FOR_SCRIPT")
		RETURN FALSE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_CREATE_VEHICLE_CRATE()

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_STAFF
			RETURN FALSE
	ENDSWITCH

	RETURN TRUE
ENDFUNC

PROC MAINTAIN_CREATE_MISSION_ENTITY_CRATE(INT iMissionEntity)

	IF NOT SHOULD_CREATE_VEHICLE_CRATE()
		EXIT
	ENDIF

	IF IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_CREATED)
	AND NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity,  eMISSIONENTITYBITSET_CRATE_CREATED_IN_VEHICLE)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
			VEHICLE_INDEX vehID = NET_TO_VEH(serverBD.sMissionEntity[iMissionEntity].netId)
			IF IS_VEHICLE_DRIVEABLE(vehID)
				IF DOES_VEHICLE_NEED_CRATE(GET_ENTITY_MODEL(vehID))
					IF CREATE_CRATE_FOR_VEHICLE(iMissionEntity, vehID)
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_CREATE_VEHICLE_CRATE - Created a crate for vehicle ", iMissionEntity)
						SET_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_CRATE_CREATED_IN_VEHICLE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC MAINTAIN_MISSION_ENTITY_DISTANCE_CHECKS(INT iMissionEntity)
	IF DOES_VARIATION_HAVE_GANG_CHASE()
	AND NOT DOES_VARIATION_HAVE_UNIQUE_GANG_CHASE_TRIGGER()
		IF IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_CREATED)
		AND NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_I_AM_FAR_ENOUGH_FOR_GANG_CHASE)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
				IF GET_DISTANCE_BETWEEN_COORDS(serverBD.sMissionEntity[iMissionEntity].vSpawnCoords, GET_MISSION_ENTITY_COORDS(iMissionEntity), FALSE) > 300.0
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_MISSION_ENTITY_DISTANCE_CHECKS - Mission entity ", iMissionEntity, " is far enough from spawn for gang chase")
					SET_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_I_AM_FAR_ENOUGH_FOR_GANG_CHASE)
					
					IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_GANG_CHASE_CAN_SPAWN)
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_MISSION_ENTITY_DISTANCE_CHECKS - SET_SERVER_BIT - eSERVERBITSET_GANG_CHASE_CAN_SPAWN")
						SET_SERVER_BIT(eSERVERBITSET_GANG_CHASE_CAN_SPAWN)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_START_GLOBAL_PING(INT iMissionEntity)
	IF NOT DOES_MEGA_BUSINESS_VARIATION_NEED_GLOBAL_SIGNAL()
		RETURN FALSE
	ENDIF

	SWITCH GET_MEGA_BUSINESS_VARIATION()
//		CASE MBV_PRIVATE_TAXI
//			IF GET_MODE_STATE() != eMODESTATE_DELIVER_ENTITY
//				RETURN FALSE
//			ENDIF
//		BREAK
		CASE MBV_SELL_FIND_BUYER
			IF IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_FIND_BUYER_IN_CLUE_ZONE) 
				RETURN FALSE
			ENDIF
		BREAK
		CASE MBV_BLIMP_PROMOTION
			IF serverBD.sMissionEntity[MISSION_ENTITY_ONE].iDeliveriesMade < 1
				RETURN FALSE
			ENDIF
		BREAK
		CASE MBV_SELL_OFFSHORE_TRANSFER
			IF IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_I_AM_ATTACHED_TO_CARGOBOB)
				BOOL bMissionEntOk, bBobOk
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
					IF IS_ENTITY_ALIVE(NET_TO_VEH(serverBD.sMissionEntity[iMissionEntity].netId))
						bMissionEntOk = TRUE
					ENDIF
				ENDIF
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[0].netId)
					IF IS_ENTITY_ALIVE(NET_TO_VEH(serverBD.sVehicle[0].netId))
						bBobOk = TRUE
					ENDIF
				ENDIF
				
				IF bMissionEntOk
				AND bBobOk
					IF GET_ENTITY_ATTACHED_TO(NET_TO_VEH(serverBD.sMissionEntity[iMissionEntity].netId)) = NET_TO_ENT(serverBD.sVehicle[0].netId)
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE MBV_SELL_PROTECT_BUYER
			IF GET_MODE_STATE() >= eMODESTATE_DEFEND_AREA
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH

	IF IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_I_HAVE_BEEN_ENTERED_AT_LEAST_ONCE)
	AND NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_I_AM_IN_A_MISSION_INTERIOR)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_MISSION_ENTITY_START_GLOBAL_PING_TIMER(INT iMissionEntity)
	IF NOT HAS_NET_TIMER_STARTED(serverBD.sMissionEntity[iMissionEntity].eGlobalPingTimer)
		IF SHOULD_START_GLOBAL_PING(iMissionEntity)
			START_NET_TIMER(serverBD.sMissionEntity[iMissionEntity].eGlobalPingTimer)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] [GLOBAL_PING] Started global ping for mission entity #", iMissionEntity)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_SERVER_GLOBAL_PING_PROCESSING()
	
	INT iMissionEntity
	
	IF DOES_MEGA_BUSINESS_VARIATION_USE_MISSION_ENTITIES(GET_MEGA_BUSINESS_VARIATION())
	AND DOES_MEGA_BUSINESS_VARIATION_NEED_GLOBAL_SIGNAL()
		REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
			BOOL bEnableGlobalPing = FALSE
			
			// If the global ping timer expires, blip the entities.
			IF HAS_NET_TIMER_STARTED(serverBD.sMissionEntity[iMissionEntity].eGlobalPingTimer)
				IF HAS_NET_TIMER_EXPIRED(serverBD.sMissionEntity[iMissionEntity].eGlobalPingTimer, GET_DELAY_GLOBAL_PING_TIME_LIMIT())
					#IF IS_DEBUG_BUILD
					IF NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_A_PARTICIPANT_HAS_ENABLED_MY_GLOBAL_BLIP)
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] [GLOBAL_PING] Timer expired for mission entity #", iMissionEntity)
					ENDIF
					#ENDIF
					bEnableGlobalPing = TRUE
				ENDIF
			ENDIF
			
			SWITCH GET_MEGA_BUSINESS_VARIATION()
				CASE MBV_SELL_FIND_BUYER
				CASE MBV_SELL_OFFSHORE_TRANSFER
				CASE MBV_SELL_PROTECT_BUYER
					IF NOT SHOULD_START_GLOBAL_PING(iMissionEntity)
						CLEAR_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_A_PARTICIPANT_HAS_ENABLED_MY_GLOBAL_BLIP)
						CLEAR_SERVER_BIT(eSERVERBITSET_MISSION_ENTITY_VISIBLE_TICKER_SENT)
						bEnableGlobalPing = FALSE
					ENDIF
				BREAK
			ENDSWITCH
					
			IF NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_A_PARTICIPANT_HAS_ENABLED_MY_GLOBAL_BLIP)
				IF bEnableGlobalPing
					SET_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_A_PARTICIPANT_HAS_ENABLED_MY_GLOBAL_BLIP)
					IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_MISSION_ENTITY_VISIBLE_TICKER_SENT)
						SEND_BUSINESS_ENTITY_VISIBLE_TICKER(serverBD.iFMMCType)
						SET_SERVER_BIT(eSERVERBITSET_MISSION_ENTITY_VISIBLE_TICKER_SENT)
					ENDIF
				ENDIF
			ENDIF
			
		ENDREPEAT
	ENDIF
ENDPROC

FUNC BOOL DOES_VARIATION_HAVE_TARGET_PEDS()

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_SELL_UNDERCOVER_COPS
		CASE MBV_MUSCLE_OUT
		CASE MBV_SELL_ROADBLOCK
		CASE MBV_SNAPMATIC
		CASE MBV_COLLECT_DJ_STOLEN_BAG
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL HAVE_ALL_TARGET_PEDS_BEEN_ELIMINATED()

	IF DOES_VARIATION_HAVE_TARGET_PEDS()
		RETURN HAVE_ALL_PEDS_BEEN_ELIMINATED()
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC BOOL DOES_VARIATION_HAVE_TARGET_PROPS()

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_COLLECTOR
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PROP_A_TARGET(INT iProp)
	UNUSED_PARAMETER(iProp)
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_COLLECTOR
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL HAVE_ALL_PROPS_BEEN_ELIMINATED()
	IF DOES_VARIATION_HAVE_TARGET_PROPS()
		INT iProp
			REPEAT GB_GET_NUM_PROPS_FOR_MEGA_BUSINESS_VARIATION(GET_MEGA_BUSINESS_VARIATION(), GET_MEGA_BUSINESS_SUBVARIATION()) iProp
			IF IS_PROP_A_TARGET(iProp)
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niProps[iProp])
					IF NOT IS_ENTITY_DEAD(NET_TO_ENT(serverBD.niProps[iProp]))
						PRINTLN("HAVE_ALL_PROPS_BEEN_ELIMINATED - FALSE - Prop ", iProp, " is still alive")
						RETURN FALSE
					ENDIF
				ENDIF
			ELSE
				RETURN FALSE
			ENDIF
		ENDREPEAT
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC BOOL HAVE_ALL_TARGET_PROPS_BEEN_ELIMINATED()

	IF DOES_VARIATION_HAVE_TARGET_PROPS()
		RETURN HAVE_ALL_PROPS_BEEN_ELIMINATED()
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC BOOL DOES_VARIATION_HAVE_TARGET_VEHICLES()

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_RIVAL_SUPPLY
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL HAVE_ALL_TARGET_VEHICLES_BEEN_ELIMINATED()

	IF DOES_VARIATION_HAVE_TARGET_VEHICLES()
		RETURN HAVE_ALL_VEHICLES_BEEN_DESTROYED()
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC BOOL HAVE_TARGETS_BEEN_ELIMINATED()

//	IF NOT HAVE_ALL_TARGETS_BEEN_CREATED()
//		PRINTLN("HAVE_TARGETS_BEEN_ELIMINATED - False - Not all created yet")
//		RETURN FALSE
//	ENDIF

	IF NOT HAVE_ALL_TARGET_PEDS_BEEN_ELIMINATED()
		PRINTLN("HAVE_TARGETS_BEEN_ELIMINATED - False - Peds")
		RETURN FALSE
	ENDIF
	
	IF NOT HAVE_ALL_TARGET_PROPS_BEEN_ELIMINATED()
		PRINTLN("HAVE_TARGETS_BEEN_ELIMINATED - False - Props")
		RETURN FALSE
	ENDIF
	
	IF NOT HAVE_ALL_TARGET_VEHICLES_BEEN_ELIMINATED()
		PRINTLN("HAVE_TARGETS_BEEN_ELIMINATED - False - Vehs")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL HAS_ANY_TARGET_BEEN_CREATED()
	RETURN IS_SERVER_BIT_SET(eSERVERBITSET_TARGETS_CREATED)
ENDFUNC

FUNC BOOL SHOULD_MISSION_END_WHEN_TARGETS_ELIMINATED()

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_RIVAL_SUPPLY
		CASE MBV_MUSCLE_OUT
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL HAVE_TARGETS_BEEN_INCAPACITATED()
	INT iPed
	REPEAT GET_NUM_VARIATION_PEDS() iPed
		IF IS_PED_BIT_SET(iPed, ePEDBITSET_PED_CREATED)
			IF NOT IS_PED_BIT_SET(iPed, ePEDBITSET_INCAPACITATED)
			AND IS_PED_BIT_SET(iPed, ePEDBITSET_I_AM_STREET_PUNK_PED)
				PRINTLN("HAVE_TARGETS_BEEN_INCAPACITATED - False - ped ", iPed, " has not been incapacitated")
				RETURN FALSE
			ENDIF
		ELSE
			PRINTLN("HAVE_TARGETS_BEEN_INCAPACITATED - False - ped ", iPed, " has not been created")
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC BOOL ARE_ALL_AMBUSH_WAVE_PEDS_ELIMINATED(INT iWave)
	SWITCH iWave
		CASE 0
			IF  GET_PED_STATE(3) = ePEDSTATE_DEAD
			AND GET_PED_STATE(4) = ePEDSTATE_DEAD
			AND GET_PED_STATE(5) = ePEDSTATE_DEAD
			AND GET_PED_STATE(6) = ePEDSTATE_DEAD
				RETURN TRUE
			ENDIF
		BREAK
		CASE 1
			IF  GET_PED_STATE(7) = ePEDSTATE_DEAD
			AND GET_PED_STATE(8) = ePEDSTATE_DEAD
			AND GET_PED_STATE(9) = ePEDSTATE_DEAD
			AND GET_PED_STATE(10) = ePEDSTATE_DEAD
				RETURN TRUE
			ENDIF
		BREAK
		CASE 2
			IF  GET_PED_STATE(11) = ePEDSTATE_DEAD
			AND GET_PED_STATE(12) = ePEDSTATE_DEAD
			AND GET_PED_STATE(13) = ePEDSTATE_DEAD
			AND GET_PED_STATE(14) = ePEDSTATE_DEAD
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC DO_ALL_STAFF_COLLECTED_CHECK()
	IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_SETUP_ALL_STAFF_COLLECTED)
		INT iLoop
		REPEAT GET_NUM_PEDS_TO_COLLECT() iLoop
			IF NOT IS_PED_BIT_SET(iLoop, ePEDBITSET_COLLECTED)
				EXIT
			ENDIF
		ENDREPEAT
		
		PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - DO_ALL_STAFF_COLLECTED_CHECK - all collected")
		SET_SERVER_BIT(eSERVERBITSET_SETUP_ALL_STAFF_COLLECTED)
	ENDIF
ENDPROC

PROC MAINTAIN_MISSION_SPECIFIC_LOGIC_SERVER()
	//VEHICLE_INDEX vehID
	
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_SELL_FOLLOW_HELI
			IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_FOLLOW_HELI_FALL_BACK_TO_TRACKIFY)
			AND NOT IS_SERVER_BIT_SET(eSERVERBITSET_FINAL_CHECKPOINT_HAS_BEEN_REACHED)
				IF IS_VEHICLE_BIT_SET(0, eVEHICLEBITSET_DESTROYED)
					SET_SERVER_BIT(eSERVERBITSET_FOLLOW_HELI_FALL_BACK_TO_TRACKIFY)
				ENDIF
			ENDIF
		BREAK
//		CASE MBV_SELL_NOT_A_SCRATCH
//			REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
//				IF HAS_NET_TIMER_EXPIRED(serverBD.sMissionEntity[iMissionEntity].BonusCashCalculationTimer, 500)
//					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
//						vehID = NET_TO_VEH(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
//						
//						IF IS_ENTITY_ALIVE(vehID)
//							
//							fPercentage = GET_VEHICLE_HEALTH_PERCENTAGE(vehID, DEFAULT, DEFAULT, TO_FLOAT(GET_ENTITY_MAX_HEALTH(vehID)))
//							iCurrentValue = CEIL(GET_MODE_TUNABLE_BONUS_CASH_VALUE() * (fPercentage/ 100))
//							
//							IF serverBD.sMissionEntity[iMissionEntity].iBonusCash != iCurrentValue
//								serverBD.sMissionEntity[iMissionEntity].iBonusCash = iCurrentValue
//							ENDIF
//						ELSE
//							serverBD.sMissionEntity[iMissionEntity].iBonusCash = 0
//						ENDIF
//					ENDIF
//					RESET_NET_TIMER(serverBD.sMissionEntity[iMissionEntity].BonusCashCalculationTimer)
//				ENDIF
//			ENDREPEAT
//		BREAK
		CASE MBV_SELL_PROTECT_BUYER
			IF GET_MODE_STATE() = eMODESTATE_DEFEND_AREA
				SWITCH serverBD.iDefendAreaWavesEliminated
					CASE 0
						IF HAS_NET_TIMER_EXPIRED(serverBD.AmbushWaveSpawnTimer, 3000)
							IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_AMBUSH_WAVE_0_CAN_SPAWN)
								SET_SERVER_BIT(eSERVERBITSET_AMBUSH_WAVE_0_CAN_SPAWN)
							ENDIF
							IF ARE_ALL_AMBUSH_WAVE_PEDS_ELIMINATED(0)
								serverBD.iDefendAreaWavesEliminated++
							ENDIF
						ENDIF
					BREAK
					CASE 1
						IF HAS_NET_TIMER_EXPIRED(serverBD.AmbushWaveSpawnTimer, 8000)
							IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_AMBUSH_WAVE_1_CAN_SPAWN)
								SET_SERVER_BIT(eSERVERBITSET_AMBUSH_WAVE_1_CAN_SPAWN)
							ENDIF
							IF ARE_ALL_AMBUSH_WAVE_PEDS_ELIMINATED(1)
								serverBD.iDefendAreaWavesEliminated++
								RESET_NET_TIMER(serverBD.AmbushWaveSpawnTimer)
							ENDIF
						ENDIF
					BREAK
					CASE 2
						IF HAS_NET_TIMER_EXPIRED(serverBD.AmbushWaveSpawnTimer, 8000)
							IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_AMBUSH_WAVE_2_CAN_SPAWN)
								SET_SERVER_BIT(eSERVERBITSET_AMBUSH_WAVE_2_CAN_SPAWN)
							ENDIF
							IF ARE_ALL_AMBUSH_WAVE_PEDS_ELIMINATED(2)
								serverBD.iDefendAreaWavesEliminated++
								RESET_NET_TIMER(serverBD.AmbushWaveSpawnTimer)
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
		
		CASE MBV_COLLECT_DJ_COLLECTOR
			IF GET_MODE_STATE() = eMODESTATE_TAKE_OUT_TARGETS
				IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_REAR_RAMP_PROPS_DESTROYED)
					IF HAVE_REAR_RAMP_PROPS_BEEN_BROKEN()
						SET_SERVER_BIT(eSERVERBITSET_REAR_RAMP_PROPS_DESTROYED)
					ENDIF
				ELIF NOT IS_SERVER_BIT_SET(eSERVERBITSET_MIDDLE_RAMP_PROPS_DESTROYED)
					IF HAVE_MIDDLE_RAMP_PROPS_BEEN_BROKEN()
						GIVE_WANTED_LEVEL_TO_GANG(GET_MAX_WANTED_LEVEL_FOR_VARIATION(), FALSE)
						SET_SERVER_BIT(eSERVERBITSET_MIDDLE_RAMP_PROPS_DESTROYED)
					ENDIF
				ENDIF
			ELIF GET_MODE_STATE() = eMODESTATE_MID_MISSION_MOCAP
				IF serverBD.iCollectorVehicleState = COLLECTOR_VS_INVALID
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
						//vehId = NET_TO_VEH(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
						//FLOAT fHealthPercentage 
						//fHealthPercentage = GET_VEHICLE_HEALTH_PERCENTAGE(vehId)
						
						//IF fHealthPercentage < 25
						//	serverBD.iCollectorVehicleState = COLLECTOR_VS_WRECKED
						IF serverBD.iCollectorVehicleBulletHits >= 30 
							serverBD.iCollectorVehicleState = COLLECTOR_VS_SHOTUP
						//ELIF fHealthPercentage < 60
						//	serverBD.iCollectorVehicleState = COLLECTOR_VS_BEATENUP
						//ELIF GET_VEHICLE_DIRT_LEVEL(vehId) > 14.0
						//	serverBD.iCollectorVehicleState = COLLECTOR_VS_DIRTY
						ELSE
							serverBD.iCollectorVehicleState = COLLECTOR_VS_FINE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE MBV_SELL_OFFSHORE_TRANSFER
			IF GET_MODE_STATE() >= eMODESTATE_LEAVE_AREA
				IF HAS_MISSION_ENTITY_BEEN_DELIVERED(MISSION_ENTITY_ONE)
				#IF IS_DEBUG_BUILD
				AND NOT HAS_MISSION_BEEN_DEBUG_PASSED_OR_FAILED()
				#ENDIF
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
						IF IS_ENTITY_ALIVE(NET_TO_ENT(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId))
							IF NOT IS_MISSION_ENTITY_IN_MEGA_BUSINESS_DROP_OFF_ANGLED_AREA(NET_TO_ENT(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId), serverBD.sMissionEntity[MISSION_ENTITY_ONE].eDropOffs[0])
								IF TAKE_CONTROL_OF_NET_ID(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
									VECTOR vMoveToCoords
									vMoveToCoords = GET_MEGA_BUSINESS_DROP_OFF_COORDS(serverBD.sMissionEntity[MISSION_ENTITY_ONE].eDropOffs[0], serverBD.nightclubID, GET_MEGA_BUSINESS_SUBVARIATION())
									IF NOT IS_VECTOR_ZERO(vMoveToCoords)
										IF IS_ENTITY_ATTACHED(NET_TO_ENT(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId))
											DETACH_ENTITY(NET_TO_ENT(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId))
											PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_MISSION_SPECIFIC_LOGIC_SERVER - Detached mission entity")
										ELSE
											SET_ENTITY_COORDS(NET_TO_ENT(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId), vMoveToCoords)
											NETWORK_FADE_IN_ENTITY(NET_TO_ENT(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId), TRUE, TRUE)
											PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_MISSION_SPECIFIC_LOGIC_SERVER - Warped mission entity to ", vMoveToCoords, " as it was off the barge")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE MBV_COLLECT_STAFF
			DO_ALL_STAFF_COLLECTED_CHECK()
			
			IF IS_SERVER_BIT_SET(eSERVERBITSET_SETUP_ALL_STAFF_COLLECTED)
				IF NOT HAS_MISSION_ENTITY_BEEN_DELIVERED_OR_DESTROYED(MISSION_ENTITY_ONE)
				AND NOT IS_MISSION_ENTITY_BIT_SET(MISSION_ENTITY_ONE, eMISSIONENTITYBITSET_KNACKERED)
				AND GET_MODE_STATE() <= eMODESTATE_REWARDS
				AND GET_MODE_STATE() != eMODESTATE_DELIVER_ENTITY
					SET_MODE_STATE(eMODESTATE_DELIVER_ENTITY)
				ENDIF
			ENDIF
		BREAK
		
		CASE MBV_PRIVATE_TAXI
		CASE MBV_SNAPMATIC
			DO_ALL_STAFF_COLLECTED_CHECK()
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL SHOULD_ATTACH_FIND_ITEM_TO_ENTITY()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_SNAPMATIC
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_FIND_ITEM_BE_ATTACHED_TO_PED()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_SNAPMATIC
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC INT NUM_FIND_ITEMS()

//	SWITCH GET_MEGA_BUSINESS_VARIATION()
//		CASE MBV_COLLECT_DJ
//		
//			RETURN ciNUM_FIND_ITEMS
//		BREAK
//	ENDSWITCH

	RETURN 1
ENDFUNC

FUNC VECTOR GET_FIND_ITEM_PORTABLE_PICKUP_SPAWN_COORD(INT iPickup)
	UNUSED_PARAMETER(iPickup)
	IF SHOULD_ATTACH_FIND_ITEM_TO_ENTITY()
		IF SHOULD_FIND_ITEM_BE_ATTACHED_TO_PED()
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[serverBD.iItemHolder].netId)
				RETURN GET_ENTITY_COORDS(NET_TO_ENT(serverBD.sPed[serverBD.iItemHolder].netId), FALSE)
			ENDIF
		ENDIF
	ELSE
		SWITCH GET_MEGA_BUSINESS_VARIATION()
			CASE MBV_COLLECT_DJ_STOLEN_BAG
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[3].netId)
					RETURN GET_ENTITY_COORDS(NET_TO_ENT(serverBD.sPed[3].netId), FALSE)
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	SCRIPT_ASSERT("[MB_MISSION] - GET_FIND_ITEM_PORTABLE_PICKUP_SPAWN_COORD returning zero")
	RETURN << 0.0, 0.0, 0.0 >>
ENDFUNC

FUNC VECTOR GET_FIND_ITEM_PORTABLE_PICKUP_SPAWN_ROTATION(INT iPickup)
	UNUSED_PARAMETER(iPickup)
//	SWITCH GET_MEGA_BUSINESS_VARIATION()
//		CASE MBV_COLLECT_DJ
//			SWITCH iPickup
//				CASE 0	RETURN << 0.0, 0.0, 0.0 >>
//				CASE 1	RETURN <<0.0000, 0.0000, 68.1998>>
//			ENDSWITCH
//		BREAK
//	ENDSWITCH

	RETURN << 0.0, 0.0, 0.0 >>
ENDFUNC

FUNC PICKUP_TYPE GET_PICKUP_TYPE_FOR_FIND_ITEM_PORTABLE_PICKUP()
//	// RETURN PICKUP_PORTABLE_CRATE_UNFIXED_INCAR // So crate falls according physics. 
//	SWITCH GET_GANGOPS_VARIATION()
//		CASE GOV_CAR_COLLECTOR		RETURN PICKUP_PORTABLE_CRATE_UNFIXED_INCAR
//	ENDSWITCH
//	RETURN PICKUP_PORTABLE_CRATE_FIXED_INCAR // Crate stays at creation coords.

	RETURN PICKUP_PORTABLE_CRATE_UNFIXED_INCAR
ENDFUNC

FUNC BOOL CREATE_FIND_ITEM_PORTABLE_PICKUP()

	IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_CREATED_FIND_ITEM_PORTABLE_PICKUP)
	
		INT i
		VECTOR vSpawnLocation
		INT iCount
		
		REPEAT NUM_FIND_ITEMS() i
	
			vSpawnLocation = GET_FIND_ITEM_PORTABLE_PICKUP_SPAWN_COORD(i)		
			
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niFindItemPortablePickup[i]) 
				REQUEST_MODEL(GET_FIND_ITEM_PORTABLE_PICKUP_MODEL(i))
				IF HAS_MODEL_LOADED(GET_FIND_ITEM_PORTABLE_PICKUP_MODEL(i))
					IF CAN_REGISTER_MISSION_PICKUPS(1)
						// PICKUP_PORTABLE_CRATE_UNFIXED_INCAR prevents pickup falling through ground.
						serverBD.niFindItemPortablePickup[i] = OBJ_TO_NET(CREATE_PORTABLE_PICKUP(GET_PICKUP_TYPE_FOR_FIND_ITEM_PORTABLE_PICKUP(), vSpawnLocation, FALSE, GET_FIND_ITEM_PORTABLE_PICKUP_MODEL(i)))
				        SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(serverBD.niFindItemPortablePickup[i], TRUE)
						NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_ENT(serverBD.niFindItemPortablePickup[i]), TRUE)
						SET_ENTITY_LOAD_COLLISION_FLAG(NET_TO_ENT(serverBD.niFindItemPortablePickup[i]), TRUE)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_ENT(serverBD.niFindItemPortablePickup[i]), TRUE)
				        SET_ENTITY_INVINCIBLE(NET_TO_OBJ(serverBD.niFindItemPortablePickup[i]), TRUE)
						IF NOT IS_VECTOR_ZERO(GET_FIND_ITEM_PORTABLE_PICKUP_SPAWN_ROTATION(i))
							SET_ENTITY_ROTATION(NET_TO_OBJ(serverBD.niFindItemPortablePickup[i]), GET_FIND_ITEM_PORTABLE_PICKUP_SPAWN_ROTATION(i), EULER_XYZ)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
					
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niFindItemPortablePickup[i])  
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_FIND_ITEM_PORTABLE_PICKUP - Created portable pickup at ", vSpawnLocation)
				iCount++
			ENDIF

		ENDREPEAT
	
		IF (iCount >= NUM_FIND_ITEMS())
			SET_SERVER_BIT(eSERVERBITSET_CREATED_FIND_ITEM_PORTABLE_PICKUP)
		ENDIF
		
	ENDIF
	
	RETURN IS_SERVER_BIT_SET(eSERVERBITSET_CREATED_FIND_ITEM_PORTABLE_PICKUP)
ENDFUNC

PROC MAINTAIN_NON_MISSION_ENTITY_PICKUP_CREATION()
	IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_CREATED_FIND_ITEM_PORTABLE_PICKUP)
		IF SHOULD_CREATE_NON_MISSION_ENTITY_PICKUP()
			CREATE_FIND_ITEM_PORTABLE_PICKUP()
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_DEFEND_AREA_OVER_TIME()

	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_MOVE_ON_FROM_FOLLOW_ENTITY()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_CRASH			
			IF GET_PED_STATE(0) = ePEDSTATE_IDLE
			OR GET_PED_STATE(0) = ePEDSTATE_DEAD
				IF HAS_NET_TIMER_EXPIRED(serverBD.stFollowEntityMoveOnDelay, 3000)
					IF IS_SERVER_BIT_SET(eSERVERBITSET_BOSS_NEAR_LANDED_AIRCRAFT)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
			RETURN FALSE
	ENDSWITCH
	
	RETURN IS_SERVER_BIT_SET(eSERVERBITSET_FINAL_CHECKPOINT_HAS_BEEN_REACHED)
ENDFUNC

FUNC BOOL HAS_AREA_BEEN_DEFENDED()
	IF serverBD.iDefendAreaWavesEliminated >= 3
		RETURN TRUE
	ENDIF
	
	IF IS_SERVER_BIT_SET(eSERVERBITSET_FAILED_TO_PROTECT_BUYER)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_SHOCKING_EVENT_COORDS()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_COLLECTOR
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[0].netId)
				RETURN GET_ENTITY_COORDS(NET_TO_ENT(serverBD.sVehicle[0].netId), FALSE)
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC EVENT_NAMES GET_SHOCKING_EVENT_TYPE()

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_COLLECTOR
			RETURN EVENT_SHOCKING_EXPLOSION
	ENDSWITCH

	RETURN EVENT_INVALID
ENDFUNC

FUNC FLOAT GET_SHOCKING_EVENT_DURATION()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_COLLECTOR
			RETURN 0.0
	ENDSWITCH

	RETURN -1.0
ENDFUNC

FUNC BOOL SHOULD_ADD_SHOCKING_EVENT()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_COLLECTOR	
			RETURN GET_MODE_STATE() = eMODESTATE_TAKE_OUT_TARGETS
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC INT GET_SHOCKING_EVENT_TRIGGER_INTERVAL()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_COLLECT_DJ_COLLECTOR	
			RETURN 500
	ENDSWITCH

	RETURN 0
ENDFUNC

PROC CLEANUP_SHOCKING_EVENTS()
	IF serverBD.iShockingEvent != -1
		REMOVE_SHOCKING_EVENT(serverBD.iShockingEvent)
		serverBD.iShockingEvent = -1
		PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - CLEANUP_SHOCKING_EVENTS - Removed shocking event")
	ENDIF
ENDPROC

PROC MAINTAIN_SHOCKING_EVENTS()
	IF SHOULD_ADD_SHOCKING_EVENT()
		IF GET_SHOCKING_EVENT_TYPE() != EVENT_INVALID
			IF HAS_NET_TIMER_STARTED(serverBD.stShockingEvent)
				IF HAS_NET_TIMER_EXPIRED(serverBD.stShockingEvent, GET_SHOCKING_EVENT_TRIGGER_INTERVAL())
					IF serverBD.iShockingEvent > 0
						REMOVE_SHOCKING_EVENT(serverBD.iShockingEvent)
					ENDIF
					RESET_NET_TIMER(serverBD.stShockingEvent)
				ENDIF
			ELSE
				VECTOR vCoords
				vCoords = GET_SHOCKING_EVENT_COORDS()
				serverBD.iShockingEvent = ADD_SHOCKING_EVENT_AT_POSITION(GET_SHOCKING_EVENT_TYPE(), vCoords, GET_SHOCKING_EVENT_DURATION())
				START_NET_TIMER(serverBD.stShockingEvent)
			ENDIF
		ENDIF
	ELSE
		CLEANUP_SHOCKING_EVENTS()
	ENDIF
ENDPROC

PROC RETURN_MISSION_ENTITY_PV_TO_STORAGE()
	IF IS_MEGA_BUSINESS_VARIATION_A_SELL_MISSION(GET_MEGA_BUSINESS_VARIATION())
		IF DOES_ENTITY_EXIST(PERSONAL_VEHICLE_ID())
			BOOL bModelsMatch
			INT iMissionEntity
			MODEL_NAMES pvModel = GET_ENTITY_MODEL(PERSONAL_VEHICLE_ID())
			REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
				IF pvModel = serverBD.sMissionEntity[iMissionEntity].modelName
					bModelsMatch = TRUE
				ENDIF
			ENDREPEAT
			
			IF bModelsMatch
				CLEANUP_MP_SAVED_VEHICLE(DEFAULT, DEFAULT, TRUE, DEFAULT, TRUE)
				PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - RETURN_MISSION_ENTITY_PV_TO_STORAGE - PV model matches one we need for mission, calling CLEANUP_MP_SAVED_VEHICLE(DEFAULT, DEFAULT, TRUE, DEFAULT, TRUE)")
			ELSE
				PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - RETURN_MISSION_ENTITY_PV_TO_STORAGE - PV model doesnt match the one being used for the mission, don't need to return PV to storage")
			ENDIF
		ELSE
			PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - RETURN_MISSION_ENTITY_PV_TO_STORAGE - PV doesnt exist, don't need to return PV to storage")
		ENDIF
	ELSE
		PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - RETURN_MISSION_ENTITY_PV_TO_STORAGE - Not on a sell mission, don't need to return PV to storage")
	ENDIF
ENDPROC

FUNC BOOL VARIATION_HAS_DOORS()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_PRIVATE_TAXI
			SWITCH GET_MEGA_BUSINESS_SUBVARIATION()
				CASE MBSV_PRIVATE_TAXI_4
					RETURN TRUE
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC INT NUM_VARIATION_DOORS()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_PRIVATE_TAXI
			SWITCH GET_MEGA_BUSINESS_SUBVARIATION()
				CASE MBSV_PRIVATE_TAXI_4
					RETURN 4
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC VECTOR GET_DOOR_COORDS(INT iDoor)
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_PRIVATE_TAXI
			SWITCH GET_MEGA_BUSINESS_SUBVARIATION()
				CASE MBSV_PRIVATE_TAXI_4
					SWITCH iDoor
						CASE 0		RETURN <<-1017.809,351.321,70.664>>
						CASE 1		RETURN <<-1006.890,364.487,72.238>>
						CASE 2		RETURN <<-1012.492,352.580,70.664>>
						CASE 3		RETURN <<-1011.628,367.209,72.238>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH

	RETURN <<0,0,0>>
ENDFUNC

FUNC MODEL_NAMES GET_DOOR_MODEL(INT iDoor)
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_PRIVATE_TAXI
			SWITCH GET_MEGA_BUSINESS_SUBVARIATION()
				CASE MBSV_PRIVATE_TAXI_4
					SWITCH iDoor
						CASE 0		RETURN prop_lrggate_01_l
						CASE 1		RETURN prop_lrggate_01_r
						CASE 2		RETURN prop_lrggate_01_r
						CASE 3		RETURN prop_lrggate_01_l
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH

	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC FLOAT GET_DOOR_OPEN_RATIO(INT iDoor)
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_PRIVATE_TAXI
			SWITCH GET_MEGA_BUSINESS_SUBVARIATION()
				CASE MBSV_PRIVATE_TAXI_4
					SWITCH iDoor
						CASE 0		RETURN -1.0
						CASE 1		RETURN -1.0
						CASE 2		RETURN 1.0
						CASE 3		RETURN 1.0
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH

	RETURN 0.0
ENDFUNC

FUNC eSERVER_BITSET GET_DOOR_SERVER_BIT(INT iDoor)
	SWITCH iDoor
		CASE 0		RETURN eSERVERBITSET_DOOR_0_OPENED
		CASE 1		RETURN eSERVERBITSET_DOOR_1_OPENED
		CASE 2		RETURN eSERVERBITSET_DOOR_2_OPENED
		CASE 3		RETURN eSERVERBITSET_DOOR_3_OPENED
	ENDSWITCH
	
	RETURN eSERVERBITSET_END
ENDFUNC

PROC MAINTAIN_DOORS_SERVER()
	IF VARIATION_HAS_DOORS()
		INT iDoor
		INT iDoorHash
		FLOAT fOpenRatio
		VECTOR vDoorCoords
		MODEL_NAMES eDoorModel
		
		REPEAT NUM_VARIATION_DOORS() iDoor
			IF GET_DOOR_SERVER_BIT(iDoor) != eSERVERBITSET_END
				IF NOT IS_SERVER_BIT_SET(GET_DOOR_SERVER_BIT(iDoor))	
					vDoorCoords = GET_DOOR_COORDS(iDoor)
					eDoorModel = GET_DOOR_MODEL(iDoor)
					fOpenRatio = GET_DOOR_OPEN_RATIO(iDoor)
					iDoorHash = GET_UNIQUE_DOOR_HASH(eDoorModel, vDoorCoords)
				
					IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iDoorHash)
						ADD_DOOR_TO_SYSTEM(iDoorHash, eDoorModel, vDoorCoords)
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_DOORS - Door ", iDoor," - ADD_DOOR_TO_SYSTEM(iDoorHash, ", GET_MODEL_NAME_FOR_DEBUG(eDoorModel),", ", vDoorCoords,")")
					ELSE
						IF NETWORK_IS_DOOR_NETWORKED(iDoorHash)
						AND NETWORK_HAS_CONTROL_OF_DOOR(iDoorHash)
							IF DOOR_SYSTEM_GET_OPEN_RATIO(iDoorHash) != fOpenRatio
								DOOR_SYSTEM_SET_OPEN_RATIO(iDoorHash, fOpenRatio)
								DOOR_SYSTEM_SET_DOOR_STATE(iDoorHash, DOORSTATE_FORCE_OPEN_THIS_FRAME)
								PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_DOORS - Door ", iDoor," - DOOR_SYSTEM_SET_OPEN_RATIO(iDoorHash, ",fOpenRatio,")")
								PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_DOORS - Door ", iDoor," - DOOR_SYSTEM_SET_DOOR_STATE(iDoorHash,DOORSTATE_FORCE_OPEN_THIS_FRAME)")
								SET_SERVER_BIT(GET_DOOR_SERVER_BIT(iDoor))
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

FUNC BOOL SHOULD_DELAY_REWARDS()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_FLYER_PROMOTION
			IF GET_END_REASON() = eENDREASON_MISSION_ENTITY_DELIVERED
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC SERVER_PROCESSING()
	
	#IF IS_DEBUG_BUILD
    #IF SCRIPT_PROFILER_ACTIVE 
    OPEN_SCRIPT_PROFILE_MARKER_GROUP("SERVER_PROCESSING") // MUST BE AT START OF FUNCTION
    #ENDIF
    #ENDIF 
	
	INT iMissionEntity
	
	IF GET_MODE_STATE() > eMODESTATE_INIT
		MONITOR_TIME_UP_END_REASON()
		
		IF DOES_MEGA_BUSINESS_VARIATION_USE_MISSION_ENTITIES(GET_MEGA_BUSINESS_VARIATION())
			REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
				MAINTAIN_CREATE_MISSION_ENTITY(iMissionEntity)
				MAINTAIN_CREATE_MISSION_ENTITY_CRATE(iMissionEntity)
				MAINTAIN_MISSION_ENTITY_DISTANCE_CHECKS(iMissionEntity)
				MAINTAIN_MISSION_ENTITY_START_GLOBAL_PING_TIMER(iMissionEntity)
				MAINTAIN_SHOULD_MISSION_ENTITY_BE_DESTROYABLE_SERVER(iMissionEntity)
			ENDREPEAT
		ENDIF
		
		MAINTAIN_MISSION_SPECIFIC_LOGIC_SERVER()
		MAINTAIN_ALL_MISSION_ENTITIES_CREATED()
		MAINTAIN_CREATE_PROPS()
		MAINTAIN_VEHICLE_BRAINS()
		MAINTAIN_PED_BRAINS()
		MAINTAIN_GANG_CHASE_SERVER(serverBD.sGangChaseServer, sGangChaseOptions)
		MAINTAIN_SERVER_GLOBAL_PING_PROCESSING()
		MAINTAIN_NON_MISSION_ENTITY_PICKUP_CREATION()
		MAINTAIN_SHOCKING_EVENTS()
		MAINTAIN_DOORS_SERVER()
		MAINTAIN_SERVER_FIND_SPAWN_FOR_SCENE_WARP()
//		MAINTAIN_PED_ITEM_HOLDER()
	ENDIF
	
	SWITCH GET_MODE_STATE()
	
		CASE eMODESTATE_INIT
			
			// ********************************************************************
			// Catch if we should end the mode because there is no boss present.
			// ********************************************************************
			
			IF NOT GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_INIT - Local player is not the launching Boss and is trying to setup launch data. End reason set to eENDREASON_NO_BOSS_LEFT.")
				SET_END_REASON(eENDREASON_NO_BOSS_LEFT)
				bKillScriptLocal = TRUE
				EXIT
			ENDIF
			
			SWITCH serverBD.iServerInitStage
				CASE 0
					
					serverBD.iOrganisationSizeOnLaunch = (GB_GET_NUM_GOONS_IN_LOCAL_GANG() + 1) // Add one on to account for Boss.
					GlobalplayerBD_FM_3[NATIVE_TO_INT(localPlayerId)].sMagnateGangBossData.iOrganizationSizeOnLaunch = serverBD.iOrganisationSizeOnLaunch
					#IF IS_DEBUG_BUILD
					PRINTLN("[MB_MISSION] - SERVER_PROCESSING - eMODESTATE_INIT - serverBD.iOrganisationSizeOnLaunch = (GB_GET_NUM_GOONS_IN_LOCAL_GANG() + 1) = ", serverBD.iOrganisationSizeOnLaunch)
					#ENDIF
					
					IF NETWORK_GET_TOTAL_NUM_PLAYERS() <= 1
						SET_SERVER_BIT(eSERVERBITSET_1_PLAYER_SESSION_ON_MISSION_LAUNCH) // Save if we launched in a 1 player session. Used for end reason checks. 
					ENDIF
					
					// Variation data setup. Variation, subvariation, location and flow play ordering.
					
					serverBD.nightclubID = GET_PLAYERS_OWNED_NIGHTCLUB(localPlayerId)
					PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - SERVER_PROCESSING - eMODESTATE_INIT - serverBD.nightclubID = ", GET_NIGHTCLUB_NAME_FROM_ID(serverBD.nightclubID))
					
					serverBD.iGangID = GB_GET_GANG_ID_FROM_BOSS(localPlayerId)
					PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - SERVER_PROCESSING - eMODESTATE_INIT - serverBD.iGangID = ", serverBD.iGangID)
					
					SETUP_VARIATION()
					
					IF IS_MEGA_BUSINESS_VARIATION_A_SELL_MISSION(GET_MEGA_BUSINESS_VARIATION())
						serverBD.sHubSellMissionData = GlobalplayerBD_FM[NATIVE_TO_INT(localPlayerId)].sHubSellMissionData
						
						PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - SERVER_PROCESSING - eMODESTATE_INIT - serverBD.sHubSellMissionData.iBuyerID = ", serverBD.sHubSellMissionData.iBuyerID)
						PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - SERVER_PROCESSING - eMODESTATE_INIT - serverBD.sHubSellMissionData.iGoodsTotal = ", serverBD.sHubSellMissionData.iGoodsTotal)
						PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - SERVER_PROCESSING - eMODESTATE_INIT - serverBD.sHubSellMissionData.iSaleValue = ", serverBD.sHubSellMissionData.iSaleValue)
						PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - SERVER_PROCESSING - eMODESTATE_INIT - serverBD.sHubSellMissionData.eSellType = ", serverBD.sHubSellMissionData.eSellType)
					ENDIF
					
					#IF IS_DEBUG_BUILD
					INCREMENT_FLOW_PLAY_NEXT_VARIATION_DATA()
					#ENDIF
					
					INITIALISE_SERVER_VARS()
					
					// Smuggler entities. Type of entity, number that need to be delivered and the entity models.
					SETUP_NUM_MISSION_ENTITIES_IN_THIS_VARIATION()
					
					// Set the type of contraband rival players will see
					GB_SET_PLAYER_CONTRABAND_TYPE(GET_CONTRABAND_TYPE())
					
					// How difficult should this mission be?
					SETUP_DIFFICULTY()
					
					// Init entity bitsets as required for mode.
//					INITIALISE_PED_REACTION_TIMES()
//					INITIALISE_PED_BITSETS_FOR_MODE_START()
//					INITIALISE_VEHICLE_BITSETS_FOR_MODE_START()
//					INITIALISE_STATIONARY_AMBUSH()
					
					INITIALISE_MISSION_ENTITIES()
					INITIALISE_VEHICLES()
					INITIALISE_PEDS()
					INITIALISE_PED_REACTION_TIMES()
					
					// Saves the 4 product slots that can be filled during a buy mission. Helps safeguard against players quitting mid buy mission etc.
//					IF NOT IS_THIS_SMUGGLER_VARIATION_A_SETUP_MISSION()
//						SET_HANGAR_MISSION_PRODUCT_SLOTS(TRUE, GET_SMUGGLED_GOODS_TYPE())
//					ENDIF
					
					IF GB_IS_GLOBAL_NON_BD_BIT0_SET(eGB_GLOBAL_NON_BD_BITSET_0_LAUNCHED_SMUG_MISSION_WHIT_ANY_TYPE)
						SET_SERVER_BIT(eSERVERBITSET_LAUNCHED_MISSION_WITH_ANY_TYPE)
						GB_CLEAR_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_LAUNCHED_SMUG_MISSION_WHIT_ANY_TYPE)
					ENDIF
					
					serverBD.iUniqueMissionID = GET_RANDOM_INT_IN_RANGE()
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_INIT - INIT - serverBD.iUniqueMissionID = ", serverBD.iUniqueMissionID)
					
//					serverBD.eLaunchHangarID = GET_PLAYERS_OWNED_HANGAR(localPlayerId)					
					
					// Done. Move onto init stage 2.
					serverBD.iServerInitStage++
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_INIT - INIT - Moving on to stage #", serverBD.iServerInitStage)
				BREAK
				
				CASE 1
				
					IF SETUP_DROP_OFFS()
//					AND SETUP_PROP_LOCATIONS()
						
						SWITCH GET_MEGA_BUSINESS_VARIATION()
							CASE MBV_SELL_OFFSHORE_TRANSFER
							CASE MBV_SELL_FIND_BUYER
							CASE MBV_SELL_ROADBLOCK
							CASE MBV_SELL_PROTECT_BUYER
								SET_SERVER_BIT(eSERVERBITSET_MISSION_EOM_LEAVE_AREA)
							BREAK
						ENDSWITCH
						
						SWITCH GET_MEGA_BUSINESS_VARIATION()
							CASE MBV_SELL_SINGLE_DROP
							CASE MBV_SELL_MULTI_DROP
							CASE MBV_SELL_HACK_DROP
							CASE MBV_SELL_UNDERCOVER_COPS
							CASE MBV_SELL_FOLLOW_HELI
							CASE MBV_SELL_NOT_A_SCRATCH
							CASE MBV_SUPPLY_RUN
							CASE MBV_STOLEN_SUPPLIES
							CASE MBV_COLLECT_DJ_COLLECTOR
							CASE MBV_COLLECT_DJ_CRASH
							CASE MBV_COLLECT_DJ_HOOKED
							CASE MBV_COLLECT_DJ_STOLEN_BAG
								SET_SERVER_BIT(eSERVERBITSET_MISSION_EOM_DELIVERY_CUT)
							BREAK
						ENDSWITCH
						
						RETURN_MISSION_ENTITY_PV_TO_STORAGE()
						
						serverBD.iServerInitStage++
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_INIT - INIT - Moving on to stage #", serverBD.iServerInitStage)	
					ENDIF
				BREAK
			
				CASE 2
				
					SET_SERVER_BIT(eSERVERBITSET_SERVER_INITIALISED)
				
					SET_MODE_STATE(GET_MODE_STATE_FOR_START())
					
					// Get MatchIds for Telemetry.
					PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBD.iMatchId1, serverBD.iMatchId2)
					GlobalplayerBD_FM_3[NATIVE_TO_INT(localPlayerId)].iMatchID1 = serverBD.iMatchId1
					GlobalplayerBD_FM_3[NATIVE_TO_INT(localPlayerId)].iMatchID2 = serverBD.iMatchId2
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_INIT - INIT - iMatchId1 = ", serverBD.iMatchId1, " - iMatchId2 = ", serverBD.iMatchId2)
					
				BREAK
			ENDSWITCH
			
		BREAK
		
		CASE eMODESTATE_BACKTRACK
			SWITCH GET_MEGA_BUSINESS_VARIATION()
				CASE MBV_SNAPMATIC

					IF ( IS_LOCAL_BIT_SET(eLOCALBITSET_OPENING_PHONECALL_DONE_OR_NEVER_GOING_TO_HAPPEN) AND HAS_DIALOGUE_FINISHED_PLAYING(0) AND HAS_ANY_MISSION_ENTITY_BEEN_ENTERED_AT_LEAST_ONCE() )
					OR serverBD.iTotalDeliveredCount > 0
						IF serverBD.iTotalDeliveredCount > 0
						OR HAS_NET_TIMER_EXPIRED(serverBD.stSnapmatic, 5000)
							IF serverBD.iCachedDeliveries = serverBD.iTotalDeliveredCount
								PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [eMODESTATE_BACKTRACK] MBV_SNAPMATIC, bMoveOn = TRUE .")
								RESET_NET_TIMER(serverBD.stSnapmatic)
								SET_MODE_STATE(GET_MODE_STATE_AFTER_BACKTRACK())
							ENDIF
						ELSE
							IF NOT HAS_NET_TIMER_STARTED(serverBD.stSnapmatic)
								START_NET_TIMER(serverBD.stSnapmatic)
							ENDIF
						ENDIF
					ENDIF
					
				BREAK
			ENDSWITCH
			
			IF HAS_END_CONDITION_BEEN_MET()
				IF SHOULD_SET_PASS_END_REASON()
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_BACKTRACK - HAS_END_CONDITION_BEEN_MET 1.")
					SET_END_REASON(eENDREASON_MISSION_ENTITY_DELIVERED)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ELSE
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_BACKTRACK - HAS_END_CONDITION_BEEN_MET 2")
					SET_END_REASON(eENDREASON_MISSION_ENTITY_DESTROYED)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ENDIF
			ENDIF
		BREAK
		
		CASE eMODESTATE_GO_TO_POINT
		
			IF IS_SERVER_BIT_SET(eSERVERBITSET_FOLLOW_HELI_FALL_BACK_TO_TRACKIFY)
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_GO_TO_POINT - Moving to use trackify as this is now required")
				SET_MODE_STATE(eMODESTATE_TRACKIFY)
			ENDIF
		
			BOOL bMoveOn
			
			IF IS_SERVER_BIT_SET(eSERVERBITSET_SOMEONE_HAS_REACHED_GOTO_LOCATION)
				bMoveOn = TRUE
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [eMODESTATE_GO_TO_POINT] SERVER_PROCESSING - eMODESTATE_GO_TO_POINT - Someone has reached goto location, set bMoveOn = TRUE")
			ENDIF
			
			IF HAS_ANY_MISSION_ENTITY_BEEN_ENTERED_AT_LEAST_ONCE()
			AND SHOULD_MISSION_PROGRESS_FROM_GO_TO_POINT_UPON_MISSION_ENTITY_ENTRY()
				bMoveOn = TRUE
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [eMODESTATE_GO_TO_POINT] HAS_ANY_MISSION_ENTITY_BEEN_ENTERED_AT_LEAST_ONCE, set bMoveOn = TRUE")
			ENDIF
			
			SWITCH GET_MEGA_BUSINESS_VARIATION()
				CASE MBV_COLLECT_DJ_HOOKED
					IF HOOKED_CARGOBOB_TARGET_DELIVERED()
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [eMODESTATE_GO_TO_POINT] MBV_COLLECT_DJ_HOOKED, bMoveOn = TRUE, goto point no longer needed, HOOKED_CARGOBOB_TARGET_DELIVERED.")
						bMoveOn = TRUE
					ENDIF
					
					IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_MID_MISSION_MOCAP_DONE)
					
						IF serverBD.iCurrentGotoLocation = INVALID_GOTO_LOCATION
//							IF NOT ARE_ALL_MISSION_ENTITIES_IN_POSSESSION_OF_A_PLAYER()		
//								PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [eMODESTATE_GO_TO_POINT] MBV_COLLECT_DJ_HOOKED, eMODESTATE_COLLECT_ENTITY, as not in limo ")
//								SET_MODE_STATE(eMODESTATE_COLLECT_ENTITY)
//							ENDIF
						ELSE
							IF NOT HOOKED_CARGOBOB_OCCUPIED()
								PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [eMODESTATE_GO_TO_POINT] MBV_COLLECT_DJ_HOOKED, eMODESTATE_COLLECT_VEHICLE, as not HOOKED_CARGOBOB_OCCUPIED ")
								SET_MODE_STATE(eMODESTATE_COLLECT_VEHICLE)
							ENDIF
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
			
			IF bMoveOn
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [eMODESTATE_GO_TO_POINT] bMoveOn = TRUE, goto point no longer needed, moving on.")
				SET_MODE_STATE(GET_MODE_STATE_AFTER_GOTO())
			ENDIF
			
			IF HAS_END_CONDITION_BEEN_MET()
				IF SHOULD_SET_PASS_END_REASON()
					IF IS_SERVER_BIT_SET(eSERVERBITSET_MISSION_EOM_LEAVE_AREA)
					AND NOT HAVE_ALL_MISSION_ENTITIES_BEEN_DESTROYED()
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_GO_TO_POINT - SHOULD_GO_TO_LEAVE_AREA = TRUE")
						SET_MODE_STATE(eMODESTATE_LEAVE_AREA)
					ELSE
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_GO_TO_POINT - HAS_END_CONDITION_BEEN_MET 1.")
						SET_END_REASON(eENDREASON_MISSION_ENTITY_DELIVERED)
						SET_MODE_STATE(eMODESTATE_REWARDS)
					ENDIF
				ELSE
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_GO_TO_POINT - HAS_END_CONDITION_BEEN_MET 2")
					SET_END_REASON(eENDREASON_MISSION_ENTITY_DESTROYED)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ENDIF
			ENDIF
		BREAK
		
		CASE eMODESTATE_CAR_WASH
		
			SERVER_MAINTAIN_CLOSEST_CARWASH()
		
			IF IS_SERVER_BIT_SET(eSERVERBITSET_CAR_WASH_COMPLETE)
			
				SET_MODE_STATE(GET_MODE_STATE_AFTER_CARWASH())
			ENDIF
		
		
		BREAK
		
		CASE eMODESTATE_GO_TO_ENTITY
		
		BREAK
		
		CASE eMODESTATE_TAKE_OUT_TARGETS
		
			SWITCH GET_MEGA_BUSINESS_VARIATION()
				CASE MBV_SNAPMATIC
					IF IS_SERVER_BIT_SET(eSERVERBITSET_CREATED_FIND_ITEM_PORTABLE_PICKUP)	
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [eMODESTATE_TAKE_OUT_TARGETS], moving on as eSERVERBITSET_CREATED_FIND_ITEM_PORTABLE_PICKUP ")
						SET_MODE_STATE(GET_MODE_STATE_AFTER_TAKE_OUT_TARGETS())
					ENDIF
				BREAK
				CASE MBV_COLLECT_DJ_COLLECTOR
					IF HAS_ANY_MISSION_ENTITY_BEEN_ENTERED_AT_LEAST_ONCE()
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [eMODESTATE_TAKE_OUT_TARGETS] - a participant has collected a MissionEntity.")
						SET_MODE_STATE(GET_MODE_STATE_AFTER_COLLECT())
					ENDIF
				BREAK
			ENDSWITCH

			IF HAS_ANY_TARGET_BEEN_CREATED()
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [eMODESTATE_TAKE_OUT_TARGETS], HAS_ANY_TARGET_BEEN_CREATED")
				IF HAVE_TARGETS_BEEN_ELIMINATED()
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [eMODESTATE_TAKE_OUT_TARGETS], HAVE_TARGETS_BEEN_ELIMINATED")
					IF SHOULD_MISSION_END_WHEN_TARGETS_ELIMINATED()
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [eMODESTATE_TAKE_OUT_TARGETS], all targets have been killed, mission ended.")
						SET_END_REASON(eENDREASON_ALL_TARGETS_ELIMINATED)
						SET_MODE_STATE(eMODESTATE_REWARDS)
					ELSE
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [eMODESTATE_TAKE_OUT_TARGETS], all targets have been killed.")
						SET_MODE_STATE(GET_MODE_STATE_AFTER_TAKE_OUT_TARGETS())
					ENDIF
				ENDIF
			ENDIF
			
			IF HAS_END_CONDITION_BEEN_MET()
				IF SHOULD_SET_PASS_END_REASON()
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_TAKE_OUT_TARGETS - some MissionEntity has been delivered.")
					SET_END_REASON(eENDREASON_MISSION_ENTITY_DELIVERED)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ELSE
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_TAKE_OUT_TARGETS - no MissionEntity has been delivered, all MissionEntity has been destroyed.")
					SET_END_REASON(eENDREASON_MISSION_ENTITY_DESTROYED)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ENDIF
			ENDIF
		
		BREAK
		
		CASE eMODESTATE_INCAPACITATE_TARGETS
		
//			SWITCH GET_MEGA_BUSINESS_VARIATION()
//				CASE MBV_COLLECT_DJ
//					SWITCH GET_MEGA_BUSINESS_SUBVARIATION()
//						CASE MBSV_COLLECT_DJ_YACHT
//							IF IS_SERVER_BIT_SET(eSERVERBITSET_CREATED_FIND_ITEM_PORTABLE_PICKUP)	
//								PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [eMODESTATE_INCAPACITATE_TARGETS], moving on as eSERVERBITSET_CREATED_FIND_ITEM_PORTABLE_PICKUP ")
//								SET_MODE_STATE(GET_MODE_STATE_AFTER_TAKE_OUT_TARGETS())
//							ENDIF
//						BREAK
//					ENDSWITCH
//				BREAK
//			ENDSWITCH
		
			IF HAVE_TARGETS_BEEN_INCAPACITATED()
				IF SHOULD_MISSION_END_WHEN_TARGETS_ELIMINATED()
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [eMODESTATE_INCAPACITATE_TARGETS], all targets have been killed, mission ended.")
					SET_END_REASON(eENDREASON_ALL_TARGETS_ELIMINATED)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ELSE
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [eMODESTATE_INCAPACITATE_TARGETS], all targets have been killed.")
					SET_MODE_STATE(GET_MODE_STATE_AFTER_TAKE_OUT_TARGETS())
				ENDIF
			ENDIF
			
			IF HAS_END_CONDITION_BEEN_MET()
				IF SHOULD_SET_PASS_END_REASON()
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_INCAPACITATE_TARGETS - some MissionEntity has been delivered.")
					SET_END_REASON(eENDREASON_MISSION_ENTITY_DELIVERED)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ELSE
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_INCAPACITATE_TARGETS - no MissionEntity has been delivered, all MissionEntity has been destroyed.")
					SET_END_REASON(eENDREASON_MISSION_ENTITY_DESTROYED)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ENDIF
			ENDIF
		BREAK
		
		CASE eMODESTATE_FIND_ITEM
		
		BREAK
		
		CASE eMODESTATE_COLLECT_ITEM
		
			IF IS_SERVER_BIT_SET(eSERVERBITSET_SOMEONE_COLLECTED_ITEM)
			OR GET_MEGA_BUSINESS_VARIATION() = MBV_COLLECT_DJ_HOOKED AND HOOKED_CARGOBOB_TARGET_IS_ATTACHED()
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_COLLECT_ITEM - a participant has collected a MissionEntity.")
				SET_MODE_STATE(GET_MODE_STATE_AFTER_ITEM_COLLECT())
			ENDIF
		
			IF HAS_END_CONDITION_BEEN_MET()
				IF SHOULD_SET_PASS_END_REASON()
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_COLLECT_ITEM - some MissionEntity has been delivered.")
					SET_END_REASON(eENDREASON_MISSION_ENTITY_DELIVERED)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ELSE
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_COLLECT_ITEM - no MissionEntity has been delivered, all MissionEntity has been destroyed.")
					SET_END_REASON(eENDREASON_MISSION_ENTITY_DESTROYED)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ENDIF
			ENDIF
		BREAK
		
		CASE eMODESTATE_MEET_UP
		
		BREAK
		
		CASE eMODESTATE_HACK
			IF serverBD.iHacksComplete > serverBD.iTotalDeliveredCount
				SET_MODE_STATE(GET_MODE_STATE_AFTER_HACK())
			ENDIF
			
			IF HAS_END_CONDITION_BEEN_MET()
				IF SHOULD_SET_PASS_END_REASON()
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_HACK - SHOULD_SET_PASS_END_REASON ")
					SET_END_REASON(eENDREASON_MISSION_ENTITY_DELIVERED)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ELSE
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_HACK - FAIL ")
					SET_END_REASON(eENDREASON_MISSION_ENTITY_DESTROYED)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ENDIF
			ENDIF
		BREAK
		
		CASE eMODESTATE_MID_MISSION_MOCAP
			IF IS_SERVER_BIT_SET(eSERVERBITSET_MID_MISSION_MOCAP_DONE)
				SET_MODE_STATE(GET_MODE_STATE_AFTER_MID_MOCAP())
			ENDIF
		BREAK
		
		CASE eMODESTATE_SCRIPTED_CUTSCENE
			IF IS_SERVER_BIT_SET(eSERVERBITSET_SCRIPTED_CUTSCENE_DONE)
				SET_MODE_STATE(GET_MODE_STATE_AFTER_SCRIPTED_CUTSCENE())
			ENDIF
			
			IF HAS_END_CONDITION_BEEN_MET()
				IF SHOULD_SET_PASS_END_REASON()
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_SCRIPTED_CUTSCENE - some MissionEntity has been delivered.")
					SET_END_REASON(eENDREASON_MISSION_ENTITY_DELIVERED)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ELSE
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_SCRIPTED_CUTSCENE - no MissionEntity has been delivered, all MissionEntity has been destroyed.")
					SET_END_REASON(eENDREASON_MISSION_ENTITY_DESTROYED)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ENDIF
			ENDIF
		BREAK
		
		CASE eMODESTATE_REACH_SCORE
		
			IF serverBD.fTotalScore > 0.0
			AND serverBD.fTotalScore >= GET_TOTAL_SCORE_TO_REACH()
				SET_MODE_STATE(GET_MODE_STATE_AFTER_SCORE())
			ENDIF
		
		BREAK
		
		CASE eMODESTATE_COLLECT_PREREQUISITE_VEHICLE
		
		BREAK
		
		CASE eMODESTATE_COLLECT_VEHICLE
		
			IF HOOKED_CARGOBOB_OCCUPIED()
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_COLLECT_VEHICLE - a participant HOOKED_CARGOBOB_OCCUPIED.")
				SET_MODE_STATE(GET_MODE_STATE_AFTER_COLLECT_VEHICLE())
			ENDIF
		
			IF HAS_END_CONDITION_BEEN_MET()
				IF SHOULD_SET_PASS_END_REASON()
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_COLLECT_VEHICLE - some MissionEntity has been delivered.")
					SET_END_REASON(eENDREASON_MISSION_ENTITY_DELIVERED)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ELSE
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_COLLECT_VEHICLE - no MissionEntity has been delivered, all MissionEntity has been destroyed.")
					SET_END_REASON(eENDREASON_MISSION_ENTITY_DESTROYED)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ENDIF
			ENDIF
		BREAK
		
		CASE eMODESTATE_GAIN_WANTED
		
		BREAK
		
		CASE eMODESTATE_WAIT_FOR_PASSENGERS
			IF SHOULD_VARIATION_USE_SEQUENTIAL_PED_COLLECTION()
				IF HAVE_ALL_LOCATION_PEDS_BEEN_COLLECTED()
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_WAIT_FOR_PASSENGERS - All location peds have been collected, moving on")
					SET_MODE_STATE(GET_MODE_STATE_AFTER_WAIT_FOR_PASSENGERS())
				ENDIF
			ELSE
				IF serverBD.iPedStaffBeingCollected != -1
					IF IS_PED_BIT_SET(serverBD.iPedStaffBeingCollected, ePEDBITSET_COLLECTED)
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_WAIT_FOR_PASSENGERS - ped ", serverBD.iPedStaffBeingCollected," has been picked up.")
						SET_MODE_STATE(GET_MODE_STATE_AFTER_WAIT_FOR_PASSENGERS())
						serverBD.iPedStaffJustCollected = serverBD.iPedStaffBeingCollected
						serverBD.iPedStaffBeingCollected = -1
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - serverBD.iPedStaffJustCollected = ", serverBD.iPedStaffJustCollected)
					ENDIF
					
					IF NOT IS_MISSION_ENTITY_BIT_SET(MISSION_ENTITY_ONE, eMISSIONENTITYBITSET_NEAR_PED_TO_COLLECT)
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_WAIT_FOR_PASSENGERS - Mission entity is no longer near a ped to pick them up")
						SET_MODE_STATE(GET_MODE_STATE_AFTER_WAIT_FOR_PASSENGERS())
						serverBD.iPedStaffBeingCollected = -1
					ENDIF
				ENDIF
			ENDIF
			
			IF HAS_END_CONDITION_BEEN_MET()
				IF SHOULD_SET_PASS_END_REASON()
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_WAIT_FOR_PASSENGERS - some MissionEntity has been delivered.")
					SET_END_REASON(eENDREASON_MISSION_ENTITY_DELIVERED)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ELSE
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_WAIT_FOR_PASSENGERS - no MissionEntity has been delivered, all MissionEntity has been destroyed.")
					SET_END_REASON(eENDREASON_MISSION_ENTITY_DESTROYED)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ENDIF
			ENDIF
		BREAK
		
		CASE eMODESTATE_FOLLOW_ENTITY
			IF NOT DOES_ENTITY_TO_FOLLOW_NEED_SIGNALED()
			OR (DOES_ENTITY_TO_FOLLOW_NEED_SIGNALED() AND HAS_ENTITY_TO_FOLLOW_BEEN_SIGNALED())
				IF SHOULD_MOVE_ON_FROM_FOLLOW_ENTITY()
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_FOLLOW_ENTITY - condition met, moving on")
					SET_MODE_STATE(GET_MODE_STATE_AFTER_FOLLOW_ENTITY())
				ENDIF
			ENDIF
			
			IF IS_SERVER_BIT_SET(eSERVERBITSET_FOLLOW_HELI_FALL_BACK_TO_TRACKIFY)
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_FOLLOW_ENTITY - Moving to use trackify as this is now required")
				SET_MODE_STATE(eMODESTATE_TRACKIFY)
			ENDIF
			
			IF HAS_END_CONDITION_BEEN_MET()
				IF SHOULD_SET_PASS_END_REASON()
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_FOLLOW_ENTITY - some MissionEntity has been delivered.")
					SET_END_REASON(eENDREASON_MISSION_ENTITY_DELIVERED)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ELSE
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_FOLLOW_ENTITY - no MissionEntity has been delivered, all MissionEntity has been destroyed.")
					SET_END_REASON(eENDREASON_MISSION_ENTITY_DESTROYED)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ENDIF
			ENDIF
		BREAK
		
		CASE eMODESTATE_TRACKIFY
			IF IS_SERVER_BIT_SET(eSERVERBITSET_FOLLOW_HELI_REVEAL_DROP_OFF_FOUND)
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_TRACKIFY - condition met, moving on to eMODESTATE_DELIVER_ENTITY")
				SET_MODE_STATE(eMODESTATE_DELIVER_ENTITY)
			ENDIF
			
			IF HAS_END_CONDITION_BEEN_MET()
				IF SHOULD_SET_PASS_END_REASON()
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_TRACKIFY - some MissionEntity has been delivered.")
					SET_END_REASON(eENDREASON_MISSION_ENTITY_DELIVERED)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ELSE
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_TRACKIFY - no MissionEntity has been delivered, all MissionEntity has been destroyed.")
					SET_END_REASON(eENDREASON_MISSION_ENTITY_DESTROYED)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ENDIF
			ENDIF
		BREAK
		
		CASE eMODESTATE_LOSE_PURSUERS
			IF IS_SERVER_BIT_SET(eSERVERBITSET_LOST_ALL_PURSUERS)
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_LOSE_PURSUERS - condition met, moving on to eMODESTATE_GO_TO_POINT")
				SET_MODE_STATE(eMODESTATE_GO_TO_POINT)
			ENDIF
			
			IF HAS_END_CONDITION_BEEN_MET()
				IF SHOULD_SET_PASS_END_REASON()
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_TRACKIFY - some MissionEntity has been delivered.")
					SET_END_REASON(eENDREASON_MISSION_ENTITY_DELIVERED)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ELSE
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_TRACKIFY - no MissionEntity has been delivered, all MissionEntity has been destroyed.")
					SET_END_REASON(eENDREASON_MISSION_ENTITY_DESTROYED)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ENDIF
			ENDIF
		BREAK
		
		CASE eMODESTATE_POSTER
			IF HAS_END_CONDITION_BEEN_MET()
				IF SHOULD_SET_PASS_END_REASON()
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_POSTER - SHOULD_SET_PASS_END_REASON, TRUE ")
					SET_END_REASON(eENDREASON_MISSION_ENTITY_DELIVERED)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ELSE
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_POSTER - SHOULD_SET_PASS_END_REASON, FALSE ")
					SET_END_REASON(eENDREASON_MISSION_ENTITY_DESTROYED)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ENDIF
			ELSE
		
		
			ENDIF
		BREAK
		
		CASE eMODESTATE_COLLECT_PED
			IF IS_MISSION_ENTITY_BIT_SET(MISSION_ENTITY_ONE, eMISSIONENTITYBITSET_NEAR_PED_TO_COLLECT)
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_COLLECT_PED - a participant has collected a MissionEntity.")
				SET_MODE_STATE(GET_MODE_STATE_AFTER_COLLECT_PED())
			ENDIF
			
			IF HAS_END_CONDITION_BEEN_MET()
				IF SHOULD_SET_PASS_END_REASON()
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_COLLECT_PED - some MissionEntity has been delivered.")
					SET_END_REASON(eENDREASON_MISSION_ENTITY_DELIVERED)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ELSE
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_COLLECT_PED - no MissionEntity has been delivered, all MissionEntity has been destroyed.")
					SET_END_REASON(eENDREASON_MISSION_ENTITY_DESTROYED)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ENDIF
			ENDIF
		BREAK
		
		CASE eMODESTATE_COLLECT_ENTITY
		
			IF HAS_ANY_MISSION_ENTITY_BEEN_ENTERED_AT_LEAST_ONCE()
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_COLLECT_ENTITY - a participant has collected a MissionEntity.")
				SET_MODE_STATE(GET_MODE_STATE_AFTER_COLLECT())
			ENDIF
			
			
			IF HAS_END_CONDITION_BEEN_MET()
				IF SHOULD_SET_PASS_END_REASON()
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_COLLECT_ENTITY - some MissionEntity has been delivered.")
					SET_END_REASON(eENDREASON_MISSION_ENTITY_DELIVERED)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ELSE
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_COLLECT_ENTITY - no MissionEntity has been delivered, all MissionEntity has been destroyed.")
					SET_END_REASON(eENDREASON_MISSION_ENTITY_DESTROYED)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ENDIF
			ENDIF
		
		BREAK
		
		CASE eMODESTATE_DELIVER_ENTITY
			IF HAS_END_CONDITION_BEEN_MET()
				IF GET_MEGA_BUSINESS_VARIATION() = MBV_SELL_PROTECT_BUYER
				AND NOT HAVE_ALL_MISSION_ENTITIES_BEEN_DESTROYED()
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_DELIVER_ENTITY - SHOULD_GO_TO_LEAVE_AREA = TRUE")
					SET_MODE_STATE(eMODESTATE_DEFEND_AREA)
				ELIF IS_SERVER_BIT_SET(eSERVERBITSET_MISSION_EOM_LEAVE_AREA)
				AND NOT HAVE_ALL_MISSION_ENTITIES_BEEN_DESTROYED()
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_DELIVER_ENTITY - SHOULD_GO_TO_LEAVE_AREA = TRUE")
					SET_MODE_STATE(eMODESTATE_LEAVE_AREA)
				ELIF SHOULD_RETURN_MISSION_ENTITIES()
				AND NOT HAVE_ALL_MISSION_ENTITIES_BEEN_DESTROYED()
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_DELIVER_ENTITY - SHOULD_RETURN_MISSION_ENTITIES = TRUE")
					SET_MODE_STATE(eMODESTATE_RETURN_ENTITY)
				ELIF IS_SERVER_BIT_SET(eSERVERBITSET_MISSION_EOM_DELIVERY_CUT)
				AND NOT HAVE_ALL_MISSION_ENTITIES_BEEN_DESTROYED()
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_DELIVER_ENTITY - eMODESTATE_DELIVER_CUTSCENE = TRUE")
					SET_MODE_STATE(eMODESTATE_DELIVER_CUTSCENE)
				ELSE
					IF SHOULD_SET_PASS_END_REASON()
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_DELIVER_ENTITY - some MissionEntity has been delivered.")
						SET_END_REASON(eENDREASON_MISSION_ENTITY_DELIVERED)
						SET_MODE_STATE(eMODESTATE_REWARDS)
					ELSE
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_DELIVER_ENTITY - no MissionEntity has been delivered, all MissionEntity has been destroyed.")
						SET_END_REASON(eENDREASON_MISSION_ENTITY_DESTROYED)
						SET_MODE_STATE(eMODESTATE_REWARDS)
					ENDIF
				ENDIF
			ELSE
				SWITCH GET_MEGA_BUSINESS_VARIATION()
					CASE MBV_SELL_HACK_DROP
						IF serverBD.iHacksComplete = serverBD.iTotalDeliveredCount
						AND serverBD.iHacksComplete < serverBD.iTotalDeliveriesToMake
							SET_MODE_STATE(eMODESTATE_HACK)
						ENDIF
					BREAK
					CASE MBV_SELL_ROADBLOCK		
						IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_ROADBLOCK_FLAG_DONE) 
							IF (serverBD.iTotalDeliveriesToMake - serverBD.iTotalDeliveredCount) <= 1
							AND IS_SERVER_BIT_SET(eSERVERBITSET_ROADBLOCK_FLAG)
								DISABLE_DROP_OFFS((serverBD.iTotalDeliveriesToMake-1))
								SET_MODE_STATE(eMODESTATE_GO_TO_POINT)
								SET_SERVER_BIT(eSERVERBITSET_ROADBLOCK_FLAG_DONE)
							ENDIF
						ELSE
							ENABLE_DROP_OFFS_AFTER_DELIVER((serverBD.iTotalDeliveriesToMake-1))
						ENDIF
					BREAK
					CASE MBV_SELL_UNDERCOVER_COPS					
						IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_GIVEN_WANTED_RATING)
							GIVE_WANTED_LEVEL_TO_GANG(2, FALSE, 2000)
							SET_SERVER_BIT(eSERVERBITSET_GIVEN_WANTED_RATING)
						ENDIF
					BREAK
					CASE MBV_SELL_OFFSHORE_TRANSFER
						IF NOT IS_MISSION_ENTITY_IN_POSSESSION_OF_A_PLAYER(MISSION_ENTITY_ONE)
						AND NOT IS_SERVER_BIT_SET(eSERVERBITSET_SOMEONE_HAS_REACHED_GOTO_LOCATION)
							SET_MODE_STATE(eMODESTATE_GO_TO_POINT)
							PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_DELIVER_ENTITY - mission entity is no longer occupied and no one has reached goto location, moving to go to")
						ENDIF
					BREAK
					CASE MBV_SNAPMATIC
						IF serverBD.iCachedDeliveries != serverBD.iTotalDeliveredCount
							SET_MODE_STATE(eMODESTATE_BACKTRACK)
							serverBD.iCachedDeliveries = serverBD.iTotalDeliveredCount
							ENABLE_DROP_OFFS_AFTER_HACK(serverBD.iCachedDeliveries)
						ENDIF
					BREAK
					CASE MBV_COLLECT_DJ_HOOKED
						IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_GAMEPLAY_CAM)
							IF IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_WATCHED_MID_MISSION_MOCAP)
							AND IS_PLAYER_IN_POSSESSION_OF_A_MISSION_ENTITY(localPlayerId)
								SET_GAMEPLAY_CAM_RELATIVE_HEADING()
							ENDIF
							SET_LOCAL_BIT(eLOCALBITSET_GAMEPLAY_CAM)
						ENDIF
					BREAK
					CASE MBV_COLLECT_STAFF
						IF serverBD.iPedStaffBeingCollected != -1
							PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_DELIVER_ENTITY - ped ", serverBD.iPedStaffBeingCollected," has been picked up.")
							serverBD.iPedStaffJustCollected = serverBD.iPedStaffBeingCollected
							serverBD.iPedStaffBeingCollected = -1
							PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - serverBD.iPedStaffJustCollected [2] = ", serverBD.iPedStaffJustCollected)
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
		
		CASE eMODESTATE_DELIVER_CUTSCENE
			IF HAS_END_CONDITION_BEEN_MET()
				IF SHOULD_SET_PASS_END_REASON()
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_DELIVER_CUTSCENE - some MissionEntity has been delivered.")
					SET_END_REASON(eENDREASON_MISSION_ENTITY_DELIVERED)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ELSE
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_DELIVER_CUTSCENE - no MissionEntity has been delivered, all MissionEntity has been destroyed.")
					SET_END_REASON(eENDREASON_MISSION_ENTITY_DESTROYED)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ENDIF
			ELSE
				IF IS_SERVER_BIT_SET(eSERVERBITSET_READY_FOR_CUTSCENE)
					MAINTAIN_DELIVERY_CUTSCENE_SERVER(serverBD.sDeliveryCutscene)
				ENDIF
			ENDIF
		BREAK
		
		CASE eMODESTATE_DEFEND_AREA
			IF HAS_AREA_BEEN_DEFENDED()
				IF IS_SERVER_BIT_SET(eSERVERBITSET_MISSION_EOM_LEAVE_AREA)
				AND NOT HAVE_ALL_MISSION_ENTITIES_BEEN_DESTROYED()
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_DEFEND_AREA - SHOULD_GO_TO_LEAVE_AREA = TRUE")
					SET_MODE_STATE(eMODESTATE_LEAVE_AREA)
				ELIF SHOULD_RETURN_MISSION_ENTITIES()
				AND NOT HAVE_ALL_MISSION_ENTITIES_BEEN_DESTROYED()
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_DEFEND_AREA - SHOULD_RETURN_MISSION_ENTITIES = TRUE")
					SET_MODE_STATE(eMODESTATE_RETURN_ENTITY)
				ELSE
					IF SHOULD_SET_PASS_END_REASON()
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_DEFEND_AREA - some MissionEntity has been delivered.")
						SET_END_REASON(eENDREASON_MISSION_ENTITY_DELIVERED)
						SET_MODE_STATE(eMODESTATE_REWARDS)
					ELSE
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_DEFEND_AREA - no MissionEntity has been delivered, all MissionEntity has been destroyed.")
						SET_END_REASON(eENDREASON_MISSION_ENTITY_DESTROYED)
						SET_MODE_STATE(eMODESTATE_REWARDS)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE eMODESTATE_LEAVE_AREA
			IF HAS_END_CONDITION_BEEN_MET()
				IF SHOULD_SET_PASS_END_REASON()
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_LEAVE_AREA - some MissionEntity has been delivered.")
					SET_END_REASON(eENDREASON_MISSION_ENTITY_DELIVERED)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ELSE
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_LEAVE_AREA - no MissionEntity has been delivered, all MissionEntity has been destroyed.")
					SET_END_REASON(eENDREASON_MISSION_ENTITY_DESTROYED)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ENDIF
			ENDIF
		BREAK
		
		CASE eMODESTATE_RETURN_ENTITY
			IF HAS_END_CONDITION_BEEN_MET()
				IF SHOULD_SET_PASS_END_REASON()
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_RETURN_ENTITY - some MissionEntity has been delivered.")
					SET_END_REASON(eENDREASON_MISSION_ENTITY_DELIVERED)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ELSE
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_RETURN_ENTITY - no MissionEntity has been delivered, all MissionEntity has been destroyed.")
					SET_END_REASON(eENDREASON_MISSION_ENTITY_DESTROYED)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ENDIF
			ENDIF
		BREAK
				
		CASE eMODESTATE_REWARDS
			
			BOOL bContinue
			bContinue = TRUE
			
			// Once all clients have handled the end of the mode, allow the server to move to the end. 
			IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_ALL_PARTICIPANTS_COMPLETED_REWARDS)
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_REWARDS - not all participants have completed rewards.")
				bContinue = FALSE
			ENDIF
			
			IF IS_MEGA_BUSINESS_VARIATION_A_SELL_MISSION(GET_MEGA_BUSINESS_VARIATION())
				IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_GOODS_REMOVED_FROM_BUSINESS_HUB)
				AND GET_END_REASON() != eENDREASON_NO_BOSS_LEFT
				AND GB_IS_PLAYER_MEMBER_OF_A_GANG(localPlayerId)
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_REWARDS - Boss hasnt removed goods from their business hub yet")
					bContinue = FALSE
				ENDIF
			ENDIF
			
			IF SHOULD_DELAY_REWARDS()
				IF NOT HAS_NET_TIMER_STARTED(serverBD.RewardsDelayTimer)
					START_NET_TIMER(serverBD.RewardsDelayTimer)
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_REWARDS - Started rewards delay timer")
					bContinue = FALSE
				ELSE
					IF NOT HAS_NET_TIMER_EXPIRED(serverBD.RewardsDelayTimer, 5000)
						bContinue = FALSE
						PRINTLN("Rewards delay timer - time remaining: ", 5000 - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.RewardsDelayTimer))
					ENDIF
				ENDIF
			ENDIF
			
			IF bContinue
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_REWARDS - bContinue = true, moving to next stage")
				SET_MODE_STATE(GET_MODE_STATE_AFTER_REWARDS())
			ENDIF
			
		BREAK
		
		CASE eMODESTATE_WARP_INTO_NIGHTCLUB
			IF IS_SERVER_BIT_SET(eSERVERBITSET_ALL_WARPED_INTO_NIGHTCLUB)
				IF NOT HAS_NET_TIMER_STARTED(serverBD.WarpIntoClubTimer)
					START_NET_TIMER(serverBD.WarpIntoClubTimer)
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_WARP_INTO_NIGHTCLUB - Everyone has finished their warp, starting timer ", GET_CLOUD_TIME_AS_INT())
				ELSE
					IF HAS_NET_TIMER_EXPIRED(serverBD.WarpIntoClubTimer, GET_MODE_TUNABLE_WARP_HOLD_UP_TIME())
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_WARP_INTO_NIGHTCLUB - Warp timer has expired, ending mode ", GET_CLOUD_TIME_AS_INT())
						SET_MODE_STATE(eMODESTATE_END)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE eMODESTATE_END
			// Do anything required at very end of mode once rewards and gameplay are opver, then end.
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SERVER_PROCESSING - eMODESTATE_END - SET_SERVER_GAME_STATE(GAME_STATE_END)")
			SET_SERVER_GAME_STATE(GAME_STATE_END)
		BREAK
		
	ENDSWITCH

ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------

PROC CLEANUP_MISSION_ENTITIES()
	INT iMissionEntity
	REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
	
		CLEANUP_MISSION_CRATES(iMissionEntity)
	
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
			IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
				GB_CLEAR_VEHICLE_AS_CONTRABAND(NET_TO_VEH(serverBD.sMissionEntity[iMissionEntity].netId))
					
				IF DOES_MEGA_BUSINESS_VARIATION_HAVE_WARP_INTO_NIGHTCLUB(GET_MEGA_BUSINESS_VARIATION())
//				OR GET_MEGA_BUSINESS_VARIATION() = MBV_SELL_ROADBLOCK
				OR GET_MEGA_BUSINESS_VARIATION() = MBV_SELL_UNDERCOVER_COPS
					IF GET_END_REASON() = eENDREASON_MISSION_ENTITY_DELIVERED
					AND IS_ENTITY_ALIVE(NET_TO_VEH(serverBD.sMissionEntity[iMissionEntity].netId))
					#IF IS_DEBUG_BUILD
					AND NOT HAS_MISSION_BEEN_DEBUG_PASSED_OR_FAILED()
					#ENDIF
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - CLEANUP_MISSION_ENTITIES - deleted mission entity ", iMissionEntity)
						DELETE_NET_ID(serverBD.sMissionEntity[iMissionEntity].netId)
					ELSE
						CLEANUP_NET_ID(serverBD.sMissionEntity[iMissionEntity].netId)
					ENDIF
				ELSE
					CLEANUP_NET_ID(serverBD.sMissionEntity[iMissionEntity].netId)
				ENDIF
				
			
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - CLEANUP_MISSION_ENTITIES - cleaned up mission entity ", iMissionEntity)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC CLEANUP_PEDS()
	INT iPed
	REPEAT NUM_MISSION_PEDS iPed
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[iPed].netId)
			CLEANUP_NET_ID(serverBD.sPed[iPed].netId)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - CLEANUP_PEDS - cleaned up ped ", iPed)
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL SHOULD_DELETE_VEHICLE_ON_CLEANUP()
	IF GET_MEGA_BUSINESS_VARIATION() = MBV_COLLECT_DJ_CRASH
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC CLEANUP_VEHICLES()
	INT iVeh
	REPEAT NUM_MISSION_VEHICLES iVeh
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVeh].netId)
			IF SHOULD_DELETE_VEHICLE_ON_CLEANUP()
				IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVeh].netId)
					DELETE_NET_ID(serverBD.sVehicle[iVeh].netId)
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - CLEANUP_VEHICLES - Deleted vehicle ", iVeh)
				ENDIF
			ELSE
				CLEANUP_NET_ID(serverBD.sVehicle[iVeh].netId)
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - CLEANUP_VEHICLES - Cleaned up vehicle ", iVeh)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC CLEANUP_TRAFFIC_REDUCTION_SPHERE_AROUND_VEHICLE()
	IF iCurrentPopZone != -1
		IF DOES_POP_MULTIPLIER_SPHERE_EXIST(iCurrentPopZone)
			REMOVE_POP_MULTIPLIER_SPHERE(iCurrentPopZone,FALSE)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] [SCRIPT_CLEANUP] CLEANUP_TRAFFIC_REDUCTION_SPHERE_AROUND_VEHICLE - Removed pop multiplier zone. ")
			iCurrentPopZone = -1
		ENDIF
	ENDIF
ENDPROC

FUNC CONTACT_REQUEST_TYPE GET_CONTACT_REQUEST_TYPE_FROM_MISSION_ENTITY_MODEL()
	INT iMissionEntity = MISSION_ENTITY_ONE

	SWITCH serverBD.sMissionEntity[iMissionEntity].modelName
		CASE SPEEDO4		RETURN REQUEST_HUB_VEHICLE_SPEEDO
		CASE MULE4			RETURN REQUEST_HUB_VEHICLE_MULE
		CASE POUNDER2		RETURN REQUEST_HUB_VEHICLE_POUNDER
	ENDSWITCH
	
	RETURN REQUEST_INVALID
ENDFUNC

PROC HANDLE_HUB_VEHICLE_REQUEST_RESET()
	IF IS_MEGA_BUSINESS_VARIATION_A_SELL_MISSION(GET_MEGA_BUSINESS_VARIATION())
		IF GET_END_REASON() = eENDREASON_MISSION_ENTITY_DESTROYED
		OR GET_END_REASON() = eENDREASON_TIME_UP
			SET_BIT(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_DELIVERY_VEHICLE_DEST)
		ENDIF
		
		REINIT_HUB_VEHICLE_REQUEST_TIMER(REQUEST_ANY_HUB_VEHICLE)
		REINIT_HUB_VEHICLE_REQUEST_TIMER(GET_CONTACT_REQUEST_TYPE_FROM_MISSION_ENTITY_MODEL())
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] [SCRIPT_CLEANUP] HANDLE_HUB_VEHICLE_REQUEST_RESET - Reset request timers for all hub vehicles, and specifically for the mission entity model")
	ENDIF
ENDPROC

PROC CLEANUP_BOTTLE_CLINK_AUDIO()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_STOLEN_SUPPLIES
		CASE MBV_SUPPLY_RUN
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("SCRIPT/DELIVERIES")
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - CLIENT_INITIALISE_VARS - RELEASE_NAMED_SCRIPT_AUDIO_BANK(SCRIPT/DELIVERIES)")
		BREAK
	ENDSWITCH
ENDPROC

PROC SCRIPT_CLEANUP()
	CLEANUP_FOCUS_CAM()
	CLEANUP_HOOKED_ANIMS()
	REMOVE_CONTACT_FROM_PHONEBOOK_IF_NEEDED()
	
	DELETE_DR_PHONE()
	CLEANUP_PHONE_MODEL()
	
	CLEANUP_POSTER_ANIM_PREP()
	CLEANUP_PARTY_BUS2_SCALEFORM()

	CLEAR_DELIVERY_CUTSCENE()
	CLEANUP_AUDIO_MIX_GROUPS()
	CLEANUP_BOTTLE_CLINK_AUDIO()

	TOGGLE_CHECK_LOCKED_WARP_FLAG(FALSE)
	
	IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
		PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] SCRIPT_CLEANUP - PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)")
	ENDIF
	
	IF IS_LOCAL_BIT_SET(eLOCALBITSET_DISABLE_SHUFFLE)
		SET_PED_CONFIG_FLAG(localPlayerPed, PCF_DontShuffleInVehicleToMakeRoom, FALSE)
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SCRIPT_CLEANUP - Cleaned up PCF_DontShuffleInVehicleToMakeRoom flag.")
	ENDIF

	PRINTLN("[MB_MISSION] SCRIPT_CLEANUP")
	
	#IF IS_DEBUG_BUILD
		DEBUG_STOP_PEDS_ATTACKING_ON_DEBUG_PASS()
	#ENDIF
	
	IF IS_LOCAL_BIT_SET(eLOCALBITSET_LOADED_COLLECTOR_SOUNDS)
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("SCRIPT/CAR_STEAL_4")
	ENDIF
	
	vCustomQuickGPSCoords = <<0.0,0.0,0.0>>
	
	IF IS_LOCAL_BIT_SET(eLOCALBITSET_STARTED_PRE_MOCAP_STAGE)
		IF IS_INTERACTION_MENU_DISABLED()
			ENABLE_INTERACTION_MENU()
		ENDIF
	ENDIF
	
	IF GB_IS_GLOBAL_NON_BD_BIT0_SET(eGB_GLOBAL_NON_BD_BITSET_0_PREVENT_DRAWING_SIMPLE_INTERIOR_CORONA)
		GB_CLEAR_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_PREVENT_DRAWING_SIMPLE_INTERIOR_CORONA)
	ENDIF
	
	CLEANUP_POSTERS()
	CLEANUP_AUDIO_BOTTLE_CLINKING()
	
	STAT_STOP_RECORD_STAT()
	
	CLEANUP_TRAFFIC_REDUCTION_SPHERE_AROUND_VEHICLE()
	
	CLEAR_GLOBAL_BLIP_DATA_FOR_MISSION_ENTITIES()
	
	GB_SET_MEGA_BUSINESS_VARIATION_LOCAL_PLAYER_IS_ON(-1)
	
	REMOVE_MODEL_HIDES()
	
	DELETE_PEDS_FOR_CUTSCENE()
	
	CLEANUP_DUFFEL_BAG()
	
	STOP_AUDIO_SCENE_FOR_SCRIPTED_CUTSCENE()
	STOP_AUDIO_SCENE_FOR_MOCAP_CUTSCENE()
	
	CLEAR_AUDIO_FLAGS_ON_MODE_END()
	
	CLEAR_PED_COLLECT_HELP()
	
	IF IS_AUDIO_SCENE_ACTIVE("DLC_BTL_Blimp_Promotion_General_Scene")
		STOP_AUDIO_SCENE("DLC_BTL_Blimp_Promotion_General_Scene")
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - SCRIPT_CLEANUP - STOP_AUDIO_SCENE, (DLC_BTL_Blimp_Promotion_General_Scene)")
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_CLIENT_BIT_SET(PARTICIPANT_ID(),eCLIENTBITSET_STARTED_SCRIPTED_CUTSCENE)
			SIMPLE_CUTSCENE_STOP(cutsceneStruct)
		ENDIF
		IF IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_WATCHING_MID_MISSION_MOCAP)
			CLEANUP_MP_CUTSCENE(DEFAULT, TRUE)
			IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
			AND NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(localPlayerPed))
				NETWORK_FADE_IN_ENTITY(GET_VEHICLE_PED_IS_IN(localPlayerPed), TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_POSTER_PROMOTION
			REMOVE_CLIP_SET("move_m@bag")
			RESET_PED_MOVEMENT_CLIPSET(localPlayerPed)
			CLEAR_GPS_PLAYER_WAYPOINT() 
		BREAK
	ENDSWITCH

	IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
		GlobalplayerBD_FM_3[NATIVE_TO_INT(localPlayerId)].iMegaBusinessSpawnLocationUsed = -1
		PRINTLN("SCRIPT_CLEANUP - GlobalplayerBD_FM_3[NATIVE_TO_INT(localPlayerId)].iMegaBusinessSpawnLocationUsed = ", GlobalplayerBD_FM_3[NATIVE_TO_INT(localPlayerId)].iMegaBusinessSpawnLocationUsed)
		
		IF IS_MEGA_BUSINESS_VARIATION_A_CLUB_MANAGEMENT_MISSION(GET_MEGA_BUSINESS_VARIATION())
			START_NIGHTCLUB_PROMOTION_MISSION_COOLDOWN()
			PRINTLN("[MB_MISSION] - SCRIPT_CLEANUP - called START_NIGHTCLUB_PROMOTION_MISSION_COOLDOWN()")
		ELIF IS_MEGA_BUSINESS_VARIATION_A_SELL_MISSION(GET_MEGA_BUSINESS_VARIATION())
			START_BUSINESS_HUB_SELL_MISSION_COOLDOWN()
			SELECT_BUSINESS_HUB_SPECIAL_BUYERS()
			RESET_NET_TIMER(g_stHubBuyerSelectionTimer)
			PRINTLN("[MB_MISSION] - SCRIPT_CLEANUP - called START_BUSINESS_HUB_SELL_MISSION_COOLDOWN()")
		ENDIF
	ENDIF
	
	g_sGb_Telemetry_data.sdata.m_match1 = serverBD.iMatchId1
	g_sGb_Telemetry_data.sdata.m_match2 = serverBD.iMatchId2
	PROCESS_CURRENT_BOSS_CHALLENGE_PLAYSTATS(g_sGb_Telemetry_data.sdata)
	PRINTLN("[MB_MISSION] - [TELEMETRY] - called PROCESS_CURRENT_BOSS_CHALLENGE_PLAYSTATS")
	
	IF IS_LOCAL_BIT_SET(eLOCALBITSET_UNLOCKED_AIRPORT_GATES)
		RESET_UNLOCK_AIRPORT_GATES()
	ENDIF
	
	REMOVE_STARTING_WEAPON()
	
	CLEANUP_SCENARIO_BLOCKS()
	
	CLEAR_GR_BEACON_SIGNIFIER_FROM_RADAR()
	
	SETUP_DAMAGE_TRACKING(FALSE)
	
	DEAL_WITH_VEHICLE_GENERATORS(TRUE)
	
	GB_SET_CONTRABAND_SUBVARIATION_LOCAL_PLAYER_IS_ON(ENUM_TO_INT(MBV_INVALID))
	
	CLEANUP_SIGHTSEER_HACK()
	
	CLOSE_TRACKIFY()
	
	IF IS_STREAM_PLAYING()
		STOP_STREAM()
		PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] SCRIPT_CLEANUP - Stopping stream.")
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_LOCAL_PLAYER_VALID_GANG_CHASE_TARGET()
			PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - SCRIPT_CLEANUP - I am no longer a valid gang chase target")
			CLEAR_LOCAL_PLAYER_IS_VALID_GANG_CHASE_TARGET()
		ENDIF
		
		CLEANUP_GANG_CHASE_CLIENT()
		
		IF IS_LOCAL_PLAYER_VALID_GANG_CHASE_TARGET()
			PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - SCRIPT_CLEANUP - I am no longer a valid gang chase target")
			CLEAR_LOCAL_PLAYER_IS_VALID_GANG_CHASE_TARGET()
		ENDIF
	ENDIF
	
	SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY(FALSE)
	GB_COMMON_BOSS_MISSION_CLEANUP()	
	
	CLEANUP_FLARES()
	CLEANUP_CRATES()
	
	IF IS_LOCAL_BIT_SET(eLOCALBITSET_USING_MISSION_ZOOM)
		SET_RADAR_ZOOM_PRECISE(0)
		SET_LOCAL_PLAYER_AS_USING_MISSION_SPECIFIC_ZOOM(FALSE)
		CLEAR_LOCAL_BIT(eLOCALBITSET_USING_MISSION_ZOOM)
	ENDIF
	
	IF DOES_ENTITY_EXIST(localPlayerPed)
	AND NOT IS_ENTITY_DEAD(localPlayerPed)
	
		IF SHOULD_DISABLE_WANTED_HELIS()
			SET_PED_CONFIG_FLAG(localPlayerPed,PCF_DisableWantedHelicopterSpawning, FALSE)
		ENDIF
		
		SET_PED_CONFIG_FLAG(localPlayerPed, PCF_UseLockpickVehicleEntryAnimations, FALSE)
	ENDIF
	
	CLEAR_DROP_OFF_DPAD_PROMPT()
	Clear_Any_Objective_Text_From_This_Script()
	
	g_bBikerInSellHeli = FALSE
	
	IF GET_END_REASON() != eENDREASON_NO_REASON_YET
		CLEANUP_MISSION_ENTITIES()
		CLEANUP_PEDS()
		CLEANUP_VEHICLES()
		CLEANUP_SHOCKING_EVENTS()
		CLEANUP_VARIATION_PROPS()
		
		IF IS_LOCAL_BIT_SET(eLOCALBITSET_ADDED_TRAFFIC_REDUCTION_ZONE)
			REMOVE_TRAFFIC_REDUCTION_ZONE(iTrafficReductionIndex,FALSE)
		ENDIF
		
		IF IS_MEGA_BUSINESS_VARIATION_A_SELL_MISSION(GET_MEGA_BUSINESS_VARIATION())
		AND GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
			CLEAR_HUB_GLOBAL_SELL_DATA()
			HANDLE_HUB_VEHICLE_REQUEST_RESET()
		ENDIF
	ENDIF
	
	REINIT_NET_TIMER(MPGlobalsAmbience.PostClubManagementTimer)
	
	GB_TIDYUP_SPECTATOR_CAM()

	IF IS_LOCAL_BIT_SET(eLOCALBITSET_REMOVED_PLAYER_CONTROL)
		IF IS_SKYSWOOP_AT_GROUND()
			NET_SET_PLAYER_CONTROL(localPlayerId, TRUE)
		ENDIF
	ENDIF
	
	IF MODE_TIME_EXPIRED_BEFORE_LEAVE_AREA()
		SET_PLAYER_WANTED_LEVEL(localPlayerId, 3)
		SET_PLAYER_WANTED_LEVEL_NOW(localPlayerId)
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] MODE_TIME_EXPIRED_BEFORE_LEAVE_AREA, GIVING WANTED RATING  ")
	ENDIF
	
	CLEAR_ALL_POSTER_HELP()
	
	TERMINATE_THIS_THREAD()
	
ENDPROC

FUNC BOOL PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)

	PRINTLN("[MB_MISSION] PROCESS_PRE_GAME")
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GB_MAX_GANG_SIZE_INCLUDING_BOSS, missionScriptArgs)
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD,SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD,SIZE_OF(playerBD))
	
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	GB_COMMON_BOSS_MISSION_PREGAME()
	
	SET_AUDIO_FLAGS_ON_MUSIC_START()
	
	INT iLoop
	REPEAT MAX_SELL_ENTITIES iLoop
		piLastPlayerInVehicle[iLoop] = INVALID_PLAYER_INDEX()
	ENDREPEAT
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL SHOULD_GIVE_WANTED_LEVEL_FOR_KILLING_COP_PED(PED_INDEX pedID)
	UNUSED_PARAMETER(pedID)
	RETURN TRUE
ENDFUNC

PROC GIVE_WANTED_LEVEL_TO_PLAYER(PLAYER_INDEX playerId, INT iWantedLevel, BOOL bDamagedCop, INT iDelay = 0)
	IF playerId != INVALID_PLAYER_INDEX()
	AND IS_NET_PLAYER_OK(playerId)
		#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		IF playerId = localPlayerId
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] GIVE_WANTED_LEVEL_TO_PLAYER - Giving ", iWantedLevel, " star wanted level to local player. Damaged cop: ", bDamagedCop, " - Delay: ", iDelay)
		ELSE
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] GIVE_WANTED_LEVEL_TO_PLAYER - Giving ", iWantedLevel, " star wanted level to ", GET_PLAYER_NAME(playerId), ". Damaged cop: ", bDamagedCop, " - Delay: ", iDelay)
		ENDIF
		#ENDIF
		
		GB_BROADCAST_BUSINESS_GIVE_WANTED_LEVEL(SPECIFIC_PLAYER(playerId), iWantedLevel, bDamagedCop, iDelay)
	ENDIF
ENDPROC

FUNC BOOL IS_MODEL_A_COP_VEHICLE(MODEL_NAMES model)
	IF model = POLICE
	OR model = POLICE2
	OR model = POLICE3
	OR model = POLICE4
	OR model = POLICET
	OR model = POLMAV
	OR model = RIOT
	OR model = FBI
	OR model = FBI2
	OR model = SHERIFF
	OR model = SHERIFF2
	OR model = BARRACKS
	OR model = BARRACKS2
	OR model = BARRACKS3
	OR model = CRUSADER
	OR model = TANK
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_DAMAGED_A_VALID_COP_VEHICLE(ENTITY_INDEX vehID, PED_INDEX driverId, PED_INDEX passengerId)

	IF DOES_ENTITY_EXIST(driverId)
	AND IS_PED_A_PLAYER(driverId)
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - HAS_PLAYER_DAMAGED_A_VALID_COP_VEHICLE - RETURN FALSE - Driver is a player.")
		RETURN FALSE
	ENDIF
	
	IF DOES_ENTITY_EXIST(passengerId)
	AND IS_PED_A_PLAYER(passengerId)
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - HAS_PLAYER_DAMAGED_A_VALID_COP_VEHICLE - RETURN FALSE - Passenger is a player.")
		RETURN FALSE
	ENDIF
	
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_SELL_UNDERCOVER_COPS
			IF DOES_ENTITY_EXIST(driverId)
			AND IS_ENTITY_DEAD(driverId)
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	IF IS_MODEL_A_COP_VEHICLE(GET_ENTITY_MODEL(vehID))
	AND GET_ENTITY_POPULATION_TYPE(vehID) != PT_MISSION
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - HAS_PLAYER_DAMAGED_A_VALID_COP_VEHICLE - RETURN TRUE - Vehicle is a cop model. Model hash: ", ENUM_TO_INT(GET_ENTITY_MODEL(vehID)))
		RETURN TRUE
	ENDIF
	
	IF IS_PED_A_COP(driverId)
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - HAS_PLAYER_DAMAGED_A_VALID_COP_VEHICLE - RETURN TRUE - Driver is a cop.")
		RETURN TRUE
	ENDIF
	
	IF IS_PED_A_COP(passengerId)
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - HAS_PLAYER_DAMAGED_A_VALID_COP_VEHICLE - RETURN TRUE - Passenger is a cop.")
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PEDS_REACT_ON_SCRIPT_ENTITY_DAMAGE()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_MUSCLE_OUT
		CASE MBV_RIVAL_SUPPLY
		CASE MBV_COLLECT_EQUIPMENT
			RETURN TRUE

	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PLAYER_DAMAGED_LETHALLY(WEAPON_TYPE WeaponUsed, DAMAGE_TYPE DamageType)
	
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_MUSCLE_OUT
		
			SWITCH DamageType
				
				CASE DAMAGE_TYPE_BULLET
				CASE DAMAGE_TYPE_EXPLOSIVE
				CASE DAMAGE_TYPE_FIRE
				
					RETURN TRUE
					
				CASE DAMAGE_TYPE_MELEE
					SWITCH WeaponUsed
						CASE WEAPONTYPE_KNIFE 
						CASE WEAPONTYPE_HAMMER 
						CASE WEAPONTYPE_CROWBAR 
						CASE WEAPONTYPE_DLC_BOTTLE
						CASE WEAPONTYPE_DLC_DAGGER
						CASE WEAPONTYPE_DLC_HATCHET
						CASE WEAPONTYPE_DLC_MACHETE
						CASE WEAPONTYPE_DLC_SWITCHBLADE
						CASE WEAPONTYPE_DLC_BATTLEAXE
						CASE WEAPONTYPE_DLC_STONE_HATCHET
						
							RETURN TRUE
					ENDSWITCH
					
					RETURN FALSE
			ENDSWITCH
			
			IF WeaponUsed = WEAPONTYPE_RUNOVERBYVEHICLE
		
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_DAMAGE_TYPE_ALLOWED(DAMAGE_TYPE damageType)
	IF damageType = DAMAGE_TYPE_FIRE
	OR damageType = DAMAGE_TYPE_BULLET
	OR damageType = DAMAGE_TYPE_MELEE
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_PROCESS_DAMAGE_FOR_NOT_A_SCRATCH(STRUCT_ENTITY_DAMAGE_EVENT sDamageEvent, INT& iMissionEntity)
	BOOL bProcess = FALSE
	INT iMissionEntityInvolved
	DAMAGE_TYPE damageType = GET_WEAPON_DAMAGE_TYPE(INT_TO_ENUM(WEAPON_TYPE, sDamageEvent.WeaponUsed))
	PED_INDEX damagerPed
	PED_INDEX victimPed
	PLAYER_INDEX damagerPlayer
	PLAYER_INDEX victimPlayer
	
	IF IS_DAMAGE_TYPE_ALLOWED(damageType)
	
		// Check if the mission entity (or holder of the mission entity) was a damager
		IF DOES_ENTITY_EXIST(sDamageEvent.DamagerIndex)
			IF IS_ENTITY_A_MEGA_BUSINESS_MISSION_ENTITY(sDamageEvent.DamagerIndex, iMissionEntityInvolved)
				
				iMissionEntity = iMissionEntityInvolved
				bProcess = TRUE
				PRINTLN("DEAL_WITH_NOT_A_SCRATCH_DAMAGE_EVENT - Mission entity ", iMissionEntity, " was the damager. bProcess = TRUE")
				
			ELIF IS_ENTITY_A_PED(sDamageEvent.DamagerIndex)
				damagerPed = GET_PED_INDEX_FROM_ENTITY_INDEX(sDamageEvent.DamagerIndex)
				IF IS_PED_A_PLAYER(damagerPed)
					damagerPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(damagerPed)
					IF IS_PLAYER_IN_POSSESSION_OF_A_MISSION_ENTITY(damagerPlayer)
					
						iMissionEntityInvolved = GET_MISSION_ENTITY_PLAYER_IS_HOLDING(damagerPlayer)
						iMissionEntity = iMissionEntityInvolved
						bProcess = TRUE
						PRINTLN("DEAL_WITH_NOT_A_SCRATCH_DAMAGE_EVENT - Damager was ", GET_PLAYER_NAME(damagerPlayer), " who is in possession of Mission entity ", iMissionEntity, ". bProcess = TRUE")
					ELSE
						PRINTLN("DEAL_WITH_NOT_A_SCRATCH_DAMAGE_EVENT - Damager was a player not in possession of a mission entity")
					ENDIF
				ELSE
					PRINTLN("DEAL_WITH_NOT_A_SCRATCH_DAMAGE_EVENT - Damager was an NPC, not processing")
				ENDIF
			ELSE
				PRINTLN("DEAL_WITH_NOT_A_SCRATCH_DAMAGE_EVENT - Damager was neither a mission entity or ped")
			ENDIF
		ELSE
			PRINTLN("DEAL_WITH_NOT_A_SCRATCH_DAMAGE_EVENT - Damager does not exist")
		ENDIF
		
		IF DOES_ENTITY_EXIST(sDamageEvent.VictimIndex)
			IF IS_ENTITY_A_MEGA_BUSINESS_MISSION_ENTITY(sDamageEvent.VictimIndex, iMissionEntityInvolved)
		
				iMissionEntity = iMissionEntityInvolved
				bProcess = TRUE
				PRINTLN("DEAL_WITH_NOT_A_SCRATCH_DAMAGE_EVENT - Mission entity ", iMissionEntity, " was the victim. bProcess = TRUE")
		
			ELIF IS_ENTITY_A_PED(sDamageEvent.VictimIndex)
				victimPed = GET_PED_INDEX_FROM_ENTITY_INDEX(sDamageEvent.VictimIndex)
				IF IS_PED_A_PLAYER(victimPed)
					victimPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(victimPed)
					IF IS_PLAYER_IN_POSSESSION_OF_A_MISSION_ENTITY(victimPlayer)
					
						iMissionEntityInvolved = GET_MISSION_ENTITY_PLAYER_IS_HOLDING(victimPlayer)
						iMissionEntity = iMissionEntityInvolved
						bProcess = TRUE
						PRINTLN("DEAL_WITH_NOT_A_SCRATCH_DAMAGE_EVENT - Victim was ", GET_PLAYER_NAME(victimPlayer), " who is in possession of Mission entity ", iMissionEntity, ". bProcess = TRUE")
					ELSE
						PRINTLN("DEAL_WITH_NOT_A_SCRATCH_DAMAGE_EVENT - Victim was a player not in possession of a mission entity")
					ENDIF
				ELSE
					PRINTLN("DEAL_WITH_NOT_A_SCRATCH_DAMAGE_EVENT - Victim was an NPC, not processing")
					bProcess = FALSE
				ENDIF
			ELSE
				PRINTLN("DEAL_WITH_NOT_A_SCRATCH_DAMAGE_EVENT - Victim was neither a mission entity or ped")
			ENDIF
		ELSE
			PRINTLN("DEAL_WITH_NOT_A_SCRATCH_DAMAGE_EVENT - Victim does not exist")
		ENDIF
	
	ELSE
		PRINTLN("DEAL_WITH_NOT_A_SCRATCH_DAMAGE_EVENT - Damage type not allowed. Damage type was: ", ENUM_TO_INT(damageType), " from weapon type ", ENUM_TO_INT(sDamageEvent.WeaponUsed))
	ENDIF
	
	RETURN bProcess
ENDFUNC

CONST_INT ciNOT_A_SCRATCH_MIN_DAMAGE	5
CONST_INT ciNOT_A_SCRATCH_MAX_DAMAGE	500

FUNC INT GET_DAMAGE_VALUE(INT iDamage)

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_SELL_NOT_A_SCRATCH
			RETURN (iDamage * 10)
		CASE MBV_SUPPLY_RUN
			RETURN (iDamage * 5)
	ENDSWITCH

	RETURN iDamage
ENDFUNC

FUNC INT GET_MAX_DAMAGE_VALUE()
	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_SELL_NOT_A_SCRATCH
			RETURN 1000
	ENDSWITCH

	RETURN ciNOT_A_SCRATCH_MAX_DAMAGE
ENDFUNC
PROC DEAL_WITH_NOT_A_SCRATCH_DAMAGE_EVENT(STRUCT_ENTITY_DAMAGE_EVENT sDamageEvent)
	IF GET_MEGA_BUSINESS_VARIATION() = MBV_SELL_NOT_A_SCRATCH
	OR GET_MEGA_BUSINESS_VARIATION() = MBV_SUPPLY_RUN
		BOOL bShouldProcessDamage = FALSE
		INT iMissionEntity = -1
		
		IF SHOULD_PROCESS_DAMAGE_FOR_NOT_A_SCRATCH(sDamageEvent, iMissionEntity)
			bShouldProcessDamage = TRUE
			PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] DEAL_WITH_NOT_A_SCRATCH_DAMAGE_EVENT - bShouldProcessDamage = TRUE, mission entity ", iMissionEntity, " was the victim of a damage event")
		ENDIF
			
		IF bShouldProcessDamage
			INT iDamage = ROUND(sDamageEvent.Damage)

			IF iDamage > ciNOT_A_SCRATCH_MIN_DAMAGE
				INT iValueOfDamage = GET_DAMAGE_VALUE(iDamage)
				
				// url:bugstar:4907020
				IF GET_MEGA_BUSINESS_VARIATION() = MBV_SUPPLY_RUN
				AND sDamageEvent.WeaponUsed = ENUM_TO_INT(WEAPONTYPE_RAMMEDBYVEHICLE)
					IF iValueOfDamage > 75
						iValueOfDamage = 75
					ENDIF
				ENDIF
				
				IF iValueOfDamage > GET_MAX_DAMAGE_VALUE()
					iValueOfDamage = GET_MAX_DAMAGE_VALUE()
				ENDIF
						
				playerBD[PARTICIPANT_ID_TO_INT()].iMissionEntityBonusCash[iMissionEntity] -= iValueOfDamage
				PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] DEAL_WITH_NOT_A_SCRATCH_DAMAGE_EVENT - Mission Entity #", iMissionEntity, " has been damaged. New health: ", playerBD[PARTICIPANT_ID_TO_INT()].iMissionEntityBonusCash[iMissionEntity], " - Cash lost from damage: ", iValueOfDamage)
			ENDIF
		ELSE
			PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] DEAL_WITH_NOT_A_SCRATCH_DAMAGE_EVENT - Not processing")
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_DAMAGE_EVENT(INT iCount)
	PED_INDEX VictimPedID	
	PED_INDEX DamagerPedID
	PLAYER_INDEX DamagerPlayerID
	PLAYER_INDEX VictimPlayerID
	VEHICLE_INDEX VictimVehicleID
	VEHICLE_INDEX DamagerVehicleID
	INT iPed
	INT iMissionEntity
	INT iVehicle

	STRUCT_ENTITY_DAMAGE_EVENT sEntityID
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sEntityID, SIZE_OF(sEntityID))
		
		DEAL_WITH_NOT_A_SCRATCH_DAMAGE_EVENT(sEntityID)
		
		// IF the victim exists
		IF DOES_ENTITY_EXIST(sEntityID.VictimIndex)
		
			BOOL bAnyScriptCreatedEntityDamaged = FALSE
			INT iEntityDamaged = -1
			INT iGroupToReact = -1
		
			// If the victom is a ped
			IF IS_ENTITY_A_PED(sEntityID.VictimIndex)
			
				SERVER_RECORD_ATTACK_TYPE_FOR_TELEMETRY(sEntityID.VictimIndex, sEntityID.DamagerIndex)
			
				VictimPedID = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)
				
				/////////////////////////////////////////////////
				/////////// START - DAMAGED A COP PED ///////////
				/////////////////////////////////////////////////
				INT iMaxWantedLevel = GET_MAX_WANTED_LEVEL_FOR_VARIATION()
				IF GET_MAX_WANTED_LEVEL() < iMaxWantedLevel
					IF IS_PED_A_COP(VictimPedID)
						IF SHOULD_GIVE_WANTED_LEVEL_FOR_KILLING_COP_PED(VictimPedID)
							IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
							AND IS_ENTITY_A_PED(sEntityID.DamagerIndex)
							AND localPlayerPed = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex) 
								PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - PROCESS_DAMAGE_EVENT - Victim is a cop. Giving local player a ", iMaxWantedLevel, " star wanted level via GIVE_WANTED_LEVEL_TO_PLAYER event.")
								
								// Give wanted level regardless of mission for hurting cops
								GIVE_WANTED_LEVEL_TO_PLAYER(localPlayerId, iMaxWantedLevel, TRUE)
								
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				// END - DAMAGED A COP PED
				
				
				IF IS_PED_A_MISSION_PED(VictimPedID, iPed)
					
					// -----------------------------------
					//
					// DAMAGED PED IS A MISSION PED
					//
					// -----------------------------------
					
					bAnyScriptCreatedEntityDamaged = TRUE
					iEntityDamaged = iPed
						
					// CHeck if the local player damaged them
					IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
						IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
							// damaged by a ped
							DamagerPedID = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
							IF DamagerPedID = localPlayerPed
								SWITCH GET_MEGA_BUSINESS_VARIATION()
									CASE MBV_COLLECT_STAFF
										SET_I_WANT_PEDS_TO_REACT(iPed)
										IF NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_DAMAGED_A_COLLECT_STAFF_PED)
											PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - PROCESS_DAMAGE_EVENT - Ped ", iPed, " has been damaged by me")
											SET_CLIENT_BIT(eCLIENTBITSET_I_DAMAGED_A_COLLECT_STAFF_PED)
										ENDIF
									BREAK
																	
									CASE MBV_MUSCLE_OUT
									
										SET_I_WANT_PEDS_TO_REACT(iPed)
									
										IF sEntityID.VictimDestroyed
										AND NOT IS_PED_CLIENT_BIT_SET(PARTICIPANT_ID(), iPed, ePEDCLIENTBITSET_TAKEN_LETHAL_DAMAGE)
										AND NOT IS_PED_BIT_SET(iPed, ePEDBITSET_TAKEN_LETHAL_DAMAGE)
											IF PLAYER_DAMAGED_LETHALLY(INT_TO_ENUM(WEAPON_TYPE, sEntityID.WeaponUsed), GET_WEAPON_DAMAGE_TYPE(INT_TO_ENUM(WEAPON_TYPE, sEntityID.WeaponUsed)))
												PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - PROCESS_DAMAGE_EVENT - Ped ", iPed, " has sustained lethal damage from ped damager")
												SET_PED_CLIENT_BIT(iPed, ePEDCLIENTBITSET_TAKEN_LETHAL_DAMAGE)
											ENDIF
										ENDIF
									BREAK
								ENDSWITCH
							ENDIF
						ELIF IS_ENTITY_A_VEHICLE(sEntityID.DamagerIndex)
							DamagerVehicleID = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
							IF IS_PED_IN_THIS_VEHICLE(localPlayerPed, DamagerVehicleID)
								SWITCH GET_MEGA_BUSINESS_VARIATION()
									CASE MBV_COLLECT_STAFF
										SET_I_WANT_PEDS_TO_REACT(iPed)
										IF NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_DAMAGED_A_COLLECT_STAFF_PED)
											PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - PROCESS_DAMAGE_EVENT - Ped ", iPed, " has been damaged by me")
											SET_CLIENT_BIT(eCLIENTBITSET_I_DAMAGED_A_COLLECT_STAFF_PED)
										ENDIF
									BREAK
																
									CASE MBV_MUSCLE_OUT
										
										SET_I_WANT_PEDS_TO_REACT(iPed)
									
										IF sEntityID.VictimDestroyed
										AND NOT IS_PED_CLIENT_BIT_SET(PARTICIPANT_ID(), iPed, ePEDCLIENTBITSET_TAKEN_LETHAL_DAMAGE)
										AND NOT IS_PED_BIT_SET(iPed, ePEDBITSET_TAKEN_LETHAL_DAMAGE)
											// Has ped been damaged by a lethal weapon, Riggs?
											IF INT_TO_ENUM(WEAPON_TYPE, sEntityID.WeaponUsed) = WEAPONTYPE_RAMMEDBYVEHICLE
											OR INT_TO_ENUM(WEAPON_TYPE, sEntityID.WeaponUsed) = WEAPONTYPE_RUNOVERBYVEHICLE
												PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - PROCESS_DAMAGE_EVENT - Ped ", iPed, " has sustained lethal damage from vehicle damager")
												SET_PED_CLIENT_BIT(iPed, ePEDCLIENTBITSET_TAKEN_LETHAL_DAMAGE)
											ENDIF
										ENDIF
									BREAK
								ENDSWITCH
							ENDIF
						ENDIF						
					ENDIF
				ENDIF
				
				// Telemetry
				IF NOT IS_PED_A_PLAYER(VictimPedID)
					IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
					AND IS_ENTITY_A_PED(sEntityID.DamagerIndex)
						DamagerPedID = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
						DamagerPlayerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(DamagerPedID)
						IF GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG(DamagerPlayerID, GB_GET_LOCAL_PLAYER_GANG_BOSS())
							
							IF sEntityID.VictimDestroyed
								IF NOT bAnyScriptCreatedEntityDamaged
								AND NOT GANG_CHASE_IS_THIS_PED_A_GANG_CHASE_PED(VictimPedID, serverBD.sGangChaseServer)
									SERVER_TRACK_INNOCENTS_FOR_TELEMETRY()
								ELSE	
									SERVER_TRACK_PEDS_KILLED_FOR_TELEMETRY()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					VictimPlayerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(VictimPedID)
					IF VictimPlayerID = localPlayerId
						IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
						AND IS_ENTITY_A_PED(sEntityID.DamagerIndex)
							DamagerPedID = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
							
							IF DamagerPedID != localPlayerPed
								IF sEntityID.VictimDestroyed
									IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_JUST_BEEN_KILLED)
										SET_LOCAL_BIT(eLOCALBITSET_JUST_BEEN_KILLED)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			// IF the victim is a vehicle
			ELIF IS_ENTITY_A_VEHICLE(sEntityID.VictimIndex)
				VictimVehicleID = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)
				
				
				/////////////////////////////////////////////////
				// START - DAMAGED A NON-MISSION ENTITY VEHICLE //
				/////////////////////////////////////////////////
				REPEAT NUM_MISSION_VEHICLES iVehicle
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
						IF sEntityID.VictimIndex = NET_TO_ENT(serverBD.sVehicle[iVehicle].netId)

							PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - PROCESS_MISSION_ENTITY_DAMAGE_EVENT - Victim is vehicle #", iVehicle,".")

							bAnyScriptCreatedEntityDamaged = TRUE
							iEntityDamaged = iVehicle
							
							IF GET_MEGA_BUSINESS_VARIATION() = MBV_RIVAL_SUPPLY
								IF sEntityID.VictimDestroyed
									IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
										IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
											DamagerPedID = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
											IF IS_PED_A_PLAYER(DamagerPedID)
												DamagerPlayerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(DamagerPedID)
												IF GB_ARE_PLAYERS_IN_SAME_GANG(DamagerPlayerID, GB_GET_LOCAL_PLAYER_GANG_BOSS())
													IF DamagerPlayerID = localPlayerId
														PRINT_TICKER("MBS_TK_DEST")
													ELSE
														PRINT_TICKER_WITH_PLAYER_NAME_WITH_HUD_COLOUR("MBS_TK_DEST2", DamagerPlayerID, GB_GET_PLAYER_GANG_HUD_COLOUR(DamagerPlayerID))
													ENDIF
												ENDIF
												
												IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_SEND_RIVAL_SUPPLIES_DESTROYED_TEXT)
													SET_LOCAL_BIT(eLOCALBITSET_SEND_RIVAL_SUPPLIES_DESTROYED_TEXT)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT				
				
				/////////////////////////////////////////////////
				//// START - DAMAGED A MISSION ENTITY VEHICLE ////
				/////////////////////////////////////////////////
				REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
					IF NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_DELIVERED)
					AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
					AND NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_DESTROYED)
						IF sEntityID.VictimIndex = NET_TO_ENT(serverBD.sMissionEntity[iMissionEntity].netId)
							
							IF GET_MEGA_BUSINESS_VARIATION() = MBV_COLLECT_DJ_COLLECTOR
								IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
									IF GET_WEAPON_DAMAGE_TYPE(INT_TO_ENUM(WEAPON_TYPE, sEntityID.WeaponUsed)) = DAMAGE_TYPE_BULLET
										serverBD.iCollectorVehicleBulletHits++
										PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - PROCESS_MISSION_ENTITY_DAMAGE_EVENT - Mission vehicle #", iVehicle," has been shot by ", serverBD.iCollectorVehicleBulletHits, " bullets.")
									ENDIF
								ENDIF
							ENDIF
							
							IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
								IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
									// damaged by a ped
									DamagerPedID = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
									IF IS_PED_A_PLAYER(DamagerPedID)
										DamagerPlayerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(DamagerPedID)
											
										PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - PROCESS_MISSION_ENTITY_DAMAGE_EVENT - Victim is mission entity #", iMissionEntity,". playerId = ", GET_PLAYER_NAME(DamagerPlayerID))
										
										bAnyScriptCreatedEntityDamaged = TRUE
										iEntityDamaged = iMissionEntity
										
										MissionEntityLastDamagerPlayerID[iMissionEntity] = DamagerPlayerID
										
										IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
											
											IF sEntityID.VictimDestroyed
												PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - PROCESS_MISSION_ENTITY_DAMAGE_EVENT - mission entity #", iMissionEntity," VictimDestroyed")
											ENDIF
											
											IF NOT GB_ARE_PLAYERS_IN_SAME_GANG(DamagerPlayerID,localPlayerId)
												SET_SERVER_BIT(eSERVERBITSET_MISSION_ENTITY_DAMAGED_BY_RIVALS)
											ENDIF
										ENDIF
									ENDIF
								ELIF IS_ENTITY_A_VEHICLE(sEntityID.DamagerIndex)
								
									DamagerVehicleID = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
									
									IF DOES_ENTITY_EXIST(GET_PED_IN_VEHICLE_SEAT(DamagerVehicleID))
									
										DamagerPlayerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(DamagerVehicleID))
									
										PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - PROCESS_MISSION_ENTITY_DAMAGE_EVENT - (IS_ENTITY_A_VEHICLE(sEntityID.DamagerIndex)) Victim is mission entity #", iMissionEntity,". playerId = ", GET_PLAYER_NAME(DamagerPlayerID))
										
										MissionEntityLastDamagerPlayerID[iMissionEntity] = DamagerPlayerID
										
										IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
											
											IF NOT GB_ARE_PLAYERS_IN_SAME_GANG(DamagerPlayerID,localPlayerId)
												SET_SERVER_BIT(eSERVERBITSET_MISSION_ENTITY_DAMAGED_BY_RIVALS)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
				
				/////////////////////////////////////////////////
				// START - DAMAGED A VEHICLE WITH A COP INSIDE //
				/////////////////////////////////////////////////
				INT iMaxWantedLevel = GET_MAX_WANTED_LEVEL_FOR_VARIATION()
				IF GET_MAX_WANTED_LEVEL() < iMaxWantedLevel
					PED_INDEX pedDriverId = GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex), VS_DRIVER)
					PED_INDEX pedPassId = GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex), VS_FRONT_RIGHT)
					
					IF HAS_PLAYER_DAMAGED_A_VALID_COP_VEHICLE(sEntityID.VictimIndex, pedDriverId, pedPassId)
						
						IF SHOULD_GIVE_WANTED_LEVEL_FOR_KILLING_COP_PED(pedDriverId)
							IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
							AND IS_ENTITY_A_PED(sEntityID.DamagerIndex)
							AND localPlayerPed = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex) 
								PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - PROCESS_MISSION_ENTITY_DAMAGE_EVENT - Victim is vehicle with cop inside. Giving local player a ", iMaxWantedLevel, " star wanted level via GIVE_WANTED_LEVEL_TO_PLAYER event.")
															
								// Give wanted level regardless of mission for hurting cops
								GIVE_WANTED_LEVEL_TO_PLAYER(localPlayerId, iMaxWantedLevel, TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				// END - DAMAGED A VEHICLE WITH A COP INSIDE
				
				
				// If I'm driving the vehicle thats been hit
				IF IS_PED_IN_THIS_VEHICLE(localPlayerPed, VictimVehicleID)
					IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
						IF SHOULD_COUNT_PLAYER_SCORE()
						AND IS_ENTITY_A_MEGA_BUSINESS_MISSION_ENTITY(VictimVehicleID, iMissionEntity)
							sScore.fLocalScore += (sEntityID.Damage * 100)
							PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - PROCESS_DAMAGE_EVENT - sEntityID.Damage = ", sEntityID.Damage)
							PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - PROCESS_DAMAGE_EVENT - sScore.fLocalScore = ", sScore.fLocalScore)
						ENDIF
					ENDIF
				ENDIF
				
				// Rival logic
				IF DOES_MEGA_BUSINESS_VARIATION_USE_MISSION_ENTITIES(GET_MEGA_BUSINESS_VARIATION())
				AND GET_MEGA_BUSINESS_VARIATION() != MBV_MUSCLE_OUT // url:bugstar:4804581
				AND DOES_MEGA_BUSINESS_VARIATION_NEED_GLOBAL_SIGNAL()
					IF sEntityID.VictimDestroyed
						IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
							IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
								DamagerPedID = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
								IF IS_PED_A_PLAYER(DamagerPedID)
									DamagerPlayerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(DamagerPedID)
									IF IS_ENTITY_A_MEGA_BUSINESS_MISSION_ENTITY(VictimVehicleID, iMissionEntity)
										IF NOT HAS_MISSION_ENTITY_BEEN_DELIVERED(iMissionEntity, TRUE)
											IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
												SEND_CONTRABAND_DESTROYED_EVENT(DamagerPlayerID)
												SEND_BUSINESS_ENTITY_DESTROYED_TICKER(serverBD.iFMMCType, DamagerPlayerID)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			ELIF IS_ENTITY_AN_OBJECT(sEntityId.VictimIndex)
			
				/////////////////////////////////////////////////
				///////// START - DAMAGED A SCRIPT PROP /////////
				/////////////////////////////////////////////////
				
//				IF GET_MEGA_BUSINESS_VARIATION() = MBV_COLLECT_DJ_COLLECTOR
//				AND IS_SERVER_BIT_SET(eSERVERBITSET_REAR_RAMP_PROPS_DESTROYED)
//					INT iProp
//					REPEAT GB_GET_NUM_PROPS_FOR_MEGA_BUSINESS_VARIATION(GET_MEGA_BUSINESS_VARIATION(), GET_MEGA_BUSINESS_SUBVARIATION()) iProp
//						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niProps[iProp])
//							IF sEntityId.VictimIndex = NET_TO_ENT(serverBD.niProps[iProp])
//								GIVE_WANTED_LEVEL_TO_GANG(GET_MAX_WANTED_LEVEL_FOR_VARIATION(), FALSE)
//							ENDIF
//						ENDIF
//					ENDREPEAT
//				ENDIF
			ENDIF
			
			////////////////////////////////////////////////////////
			// START - ANY SCRIPT CREATED ENTITY HAS BEEN DAMAGED //
			////////////////////////////////////////////////////////
			IF bAnyScriptCreatedEntityDamaged
				IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
				
					iGroupToReact = GET_PED_REACT_GROUP(iEntityDamaged)
					IF IS_ENTITY_A_VEHICLE(sEntityID.VictimIndex)
						iGroupToReact = iEntityDamaged
					ENDIF
					
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - PROCESS_MISSION_ENTITY_DAMAGE_EVENT - bAnyScriptCreatedEntityDamaged is true, iGroupToReact = ", iGroupToReact, " iEntityDamaged = ", iEntityDamaged)
				
					IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
						DamagerPedID = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
						IF IS_PED_A_PLAYER(DamagerPedID)
							DamagerPlayerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(DamagerPedID)
							
							SERVER_RECORD_ATTACK_TYPE_FOR_TELEMETRY(sEntityID.VictimIndex, sEntityID.DamagerIndex)
							
							PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - PROCESS_MISSION_ENTITY_DAMAGE_EVENT - bAnyScriptCreatedEntityDamaged is true.")

							// Variation specific reactions to damaging a script created entity
							IF SHOULD_PEDS_REACT_ON_SCRIPT_ENTITY_DAMAGE()
								IF SHOULD_ONLY_GROUPED_PEDS_REACT()
									IF NOT GET_GROUP_SHOULD_REACT(iGroupToReact)
									AND NOT GET_I_WANT_GROUP_TO_REACT(iGroupToReact)
										SET_I_WANT_GROUP_TO_REACT(iGroupToReact)
									ENDIF
								ELSE
									SET_I_WANT_PEDS_TO_REACT(iEntityDamaged)
									PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - PROCESS_MISSION_ENTITY_DAMAGE_EVENT - SHOULD_PEDS_REACT_ON_SCRIPT_ENTITY_DAMAGE because bAnyScriptCreatedEntityDamaged. Damager is a ped ")
									IF DamagerPlayerID = localPlayerId
										IF SHOULD_GIVE_WANTED_LEVEL_ON_PED_REACTION()
											GIVE_WANTED_LEVEL_TO_GANG(GET_MAX_WANTED_LEVEL_FOR_VARIATION(), FALSE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELIF IS_ENTITY_A_VEHICLE(sEntityID.DamagerIndex)
						DamagerVehicleID = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
						DamagerPedID = GET_PED_IN_VEHICLE_SEAT(DamagerVehicleID, VS_DRIVER)
						IF DOES_ENTITY_EXIST(DamagerPedID)
							IF IS_PED_A_PLAYER(DamagerPedID)
								DamagerPlayerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(DamagerPedID)
								
								IF SHOULD_PEDS_REACT_ON_SCRIPT_ENTITY_DAMAGE()
									IF SHOULD_ONLY_GROUPED_PEDS_REACT()
										IF NOT GET_GROUP_SHOULD_REACT(iGroupToReact)
										AND NOT GET_I_WANT_GROUP_TO_REACT(iGroupToReact)
											SET_I_WANT_GROUP_TO_REACT(iGroupToReact)
										ENDIF
									ELSE
										SET_I_WANT_PEDS_TO_REACT(iEntityDamaged)
										PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - PROCESS_MISSION_ENTITY_DAMAGE_EVENT - SHOULD_PEDS_REACT_ON_SCRIPT_ENTITY_DAMAGE because bAnyScriptCreatedEntityDamaged. Damager is a vehicle being driven by a player ")
										IF DamagerPlayerID = localPlayerId
											IF SHOULD_GIVE_WANTED_LEVEL_ON_PED_REACTION()
												GIVE_WANTED_LEVEL_TO_GANG(GET_MAX_WANTED_LEVEL_FOR_VARIATION(), FALSE)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_BUSINESS_GIVE_WANTED_LEVEL(INT iEventId)
	SCRIPT_EVENT_DATA_BUSINESS_GIVE_WANTED_LEVEL sEventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, sEventData, SIZE_OF(sEventData))
	
		#IF IS_DEBUG_BUILD
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] [SM_WANTED] - PROCESS_SCRIPT_EVENT_BUSINESS_GIVE_WANTED_LEVEL - [", sEventData.iSentAtPosix,"] - Received SCRIPT_EVENT_BUSINESS_GIVE_WANTED_LEVEL. Wanted level sent: ", sEventData.iWantedLevel)
		
		IF IS_NET_PLAYER_OK(sEventData.Details.FromPlayerIndex, FALSE)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] [SM_WANTED] - PROCESS_SCRIPT_EVENT_BUSINESS_GIVE_WANTED_LEVEL - [", sEventData.iSentAtPosix,"] - Event sent by ", GET_PLAYER_NAME(sEventData.Details.FromPlayerIndex), " at posix ", sEventData.iSentAtPosix) 
		ELSE
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] [SM_WANTED] - PROCESS_SCRIPT_EVENT_BUSINESS_GIVE_WANTED_LEVEL - [", sEventData.iSentAtPosix,"] - Sending player is no longer valid. Native to int: ", NATIVE_TO_INT(sEventData.Details.FromPlayerIndex))
		ENDIF
		#ENDIF
		
		sWantedStruct.iWantedLevel = sEventData.iWantedLevel
		sWantedStruct.iDelay = sEventData.iDelay
		sWantedStruct.bDamagedCop = sEventData.bDamagedCop
		
		SET_LOCAL_BIT(eLOCALBITSET_NEED_DELAYED_WANTED_LEVEL)
		
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] [SM_WANTED] - PROCESS_SCRIPT_EVENT_BUSINESS_GIVE_WANTED_LEVEL - [", sEventData.iSentAtPosix,"] - Wanted level: ", sWantedStruct.iWantedLevel, " - Delay: ", sWantedStruct.iDelay, " - Damaged Cop: ", sWantedStruct.bDamagedCop)
	ENDIF
ENDPROC

PROC PROCESS_EVENTS()

    INT iCount
    EVENT_NAMES ThisScriptEvent
	STRUCT_EVENT_COMMON_DETAILS Details
   	SCRIPT_EVENT_DATA_MBS_TRIGGER_PTFX sPTFXEvent
	SCRIPT_EVENT_DATA_EXPORT_VEHICLE_UPDATE_CURRENT_CHECKPOINT sCheckpointDetails
	SCRIPT_EVENT_DATA_MBS_PRINT_DELIVERY_TICKER sDeliveryTickerEvent
  	SCRIPT_EVENT_DATA_EXEC_DROP sEvent
   
    REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount

        ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)
       	
        SWITCH ThisScriptEvent
            
			CASE EVENT_NETWORK_SCRIPT_EVENT
				
				GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Details, SIZE_OF(Details))
				
				SWITCH Details.Type
					CASE SCRIPT_EVENT_EXEC_DROP
						IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sEvent, SIZE_OF(sEvent))
							IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
								IF sEvent.iPosix = serverBD.iLaunchPosix					
									IF NOT IS_BIT_SET(serverBD.iPosterPostedBitset, sEvent.iDropOff)
										SET_BIT(serverBD.iPosterPostedBitset, sEvent.iDropOff)
										PRINTLN("[MB_MISSION] [POSTER] SCRIPT_EVENT_EXEC_DROP, serverBD.iPosterPostedBitset = ", sEvent.iDropOff, " FROM ", GET_PLAYER_NAME(sEvent.Details.FromPlayerIndex))
										SET_SERVER_BIT(eSERVERBITSET_SOMEONE_DOING_POSTER_ANIMATION)
									ENDIF
								ELSE
									PRINTLN("[MB_MISSION] [POSTER] SCRIPT_EVENT_EXEC_DROP - Posix does not match. sEvent.iPosix = ", sEvent.iPosix, " - serverBD.iLaunchPosix = ", serverBD.iLaunchPosix)
								ENDIF
							ENDIF
							
							IF sEvent.Details.FromPlayerIndex = localPlayerId
//								PRINT_TICKER("MBS_TK_PST1")
							ELSE
								PRINT_TICKER_WITH_PLAYER_NAME("MBS_TK_PST2", sEvent.Details.FromPlayerIndex)
							ENDIF
						ENDIF
					BREAK
					CASE SCRIPT_EVENT_BUSINESS_GIVE_WANTED_LEVEL
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] [SM_WANTED] - PROCESS_EVENTS - Received SCRIPT_EVENT_BUSINESS_GIVE_WANTED_LEVEL.")
						PROCESS_SCRIPT_EVENT_BUSINESS_GIVE_WANTED_LEVEL(iCount)
					BREAK
					CASE SCRIPT_EVENT_MBS_TRIGGER_PTFX
						IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sPTFXEvent, SIZE_OF(sPTFXEvent))
							
							IF g_sBlockedEvents.bSCRIPT_EVENT_MBS_TRIGGER_PTFX
								PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - PROCESS_EVENTS - Received SCRIPT_EVENT_MBS_TRIGGER_PTFX, but g_sBlockedEvents.bSCRIPT_EVENT_MBS_TRIGGER_PTFX = TRUE")
								EXIT
							ENDIF
							
							IF sPTFXEvent.iUniqueMissionID != serverBD.iUniqueMissionID
								PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - PROCESS_EVENTS - Received SCRIPT_EVENT_MBS_TRIGGER_PTFX, but sPTFXEvent.iUniqueMissionID != serverBD.iUniqueMissionID")
								EXIT
							ENDIF
							
							IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_TRIGGER_LEAFLET_PTFX)
								SET_LOCAL_BIT(eLOCALBITSET_TRIGGER_LEAFLET_PTFX)
								iLocalMissionEntityForPTFX = sPTFXEvent.iMissionEntity
								PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - PROCESS_EVENTS - Received SCRIPT_EVENT_MBS_TRIGGER_PTFX, iLocalMissionEntityForPTFX = ", iLocalMissionEntityForPTFX)
							ELSE
								PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - PROCESS_EVENTS - Received SCRIPT_EVENT_MBS_TRIGGER_PTFX, but eLOCALBITSET_TRIGGER_LEAFLET_PTFX is already set")
							ENDIF
							
						ELSE
							PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - PROCESS_EVENTS - Received SCRIPT_EVENT_MBS_TRIGGER_PTFX, but failed to get event data")
						ENDIF
					BREAK
					CASE SCRIPT_EVENT_FMEVENT_VEHICLE_UPDATE_CURRENT_CHECKPOINT
//						IF SHOULD_PROCESS_CHECKPOINT_UPDATE_EVENT()
							IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
								IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sCheckpointDetails, SIZE_OF(sCheckpointDetails))
									serverBD.sPed[sCheckpointDetails.iPed].iCheckpointOnRouteTo = sCheckpointDetails.iCheckpointOnRouteTo
	//								serverBD.iPatrolReachedPosix[GET_ARRAY_POSITION_FOR_PATROL_PED(sCheckpointDetails.iPed)] = -1
									PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - [DRIVE_TO_POINT] - Received SCRIPT_EVENT_BUSINESS_VEHICLE_UPDATE_CURRENT_CHECKPOINT. Ped #", sCheckpointDetails.iPed," - Checkpoint On Route To: ", sCheckpointDetails.iCheckpointOnRouteTo)
								ENDIF
							ENDIF
//						ENDIF
					BREAK
					CASE SCRIPT_EVENT_MBS_PRINT_DELIVERY_TICKER
						IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sDeliveryTickerEvent, SIZE_OF(sDeliveryTickerEvent))
							
							IF g_sBlockedEvents.bSCRIPT_EVENT_MBS_PRINT_DELIVERY_TICKER
								EXIT
							ENDIF
							
							IF sDeliveryTickerEvent.iMissionID = serverBD.iUniqueMissionID
								IF sDeliveryTickerEvent.piDeliverer != INVALID_PLAYER_INDEX()
									IF serverBD.iTotalDeliveredCount != serverBD.iTotalDeliveriesToMake
										PRINT_DELIVER_TICKER(sDeliveryTickerEvent.piDeliverer, sDeliveryTickerEvent.eVariation1)
									ENDIF
									PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - PROCESS_EVENTS - Received SCRIPT_EVENT_MBS_PRINT_DELIVERY_TICKER, deliverer = ", GET_PLAYER_NAME(sDeliveryTickerEvent.piDeliverer))
								ELSE
									PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - PROCESS_EVENTS - Received SCRIPT_EVENT_MBS_PRINT_DELIVERY_TICKER, deliverer = INVALID_PLAYER_INDEX(), not printing ticker")
								ENDIF
							ELSE
								PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - PROCESS_EVENTS - Received SCRIPT_EVENT_MBS_PRINT_DELIVERY_TICKER but mission ID does not match mine, this came from another organisation delivery in session!")
								PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - PROCESS_EVENTS - sDeliveryTickerEvent.iMissionID = ", sDeliveryTickerEvent.iMissionID)
								PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - PROCESS_EVENTS - serverBD.iUniqueMissionID = ", serverBD.iUniqueMissionID)
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
				
			BREAK
			
			CASE EVENT_NETWORK_DAMAGE_ENTITY
			
				PROCESS_DAMAGE_EVENT(iCount)
				
			BREAK
			
        ENDSWITCH
            
    ENDREPEAT
    
ENDPROC

FUNC FLOAT GET_TOTAL_SCORE_STARTING_POINT()

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_BLIMP_PROMOTION		RETURN serverBD.fTotalScore
	ENDSWITCH

	RETURN 0.0

ENDFUNC

FUNC BOOL SHOULD_USE_SERVER_TOTAL_SCORE()

	SWITCH GET_MEGA_BUSINESS_VARIATION()
		CASE MBV_BLIMP_PROMOTION		
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE

ENDFUNC

SCRIPT_TIMER stWarpIntoClubDelayTimer
PROC PROCESS_PARTICIPANT_LOOP()
	
	INT iParticipant
	INT iPlayer
	INT iMissionEntity
	INT iDropOff
	PLAYER_INDEX playerTemp
	PED_INDEX pedTemp
	TEXT_LABEL_63 tl63Temp
	//VEHICLE_INDEX vehTemp
	BOOL bNoBossOnScript = TRUE
	BOOL bAllCompletedRewards = TRUE
	BOOL bAllLeftArea = FALSE
	BOOL bAllDeliveryComplete = FALSE
	BOOL bAllCutsceneComplete = FALSE
	BOOL bReadyForCutscene = FALSE
	BOOL bMidMissionMocapComplete = TRUE
	BOOL bScriptedCutsceneComplete = TRUE
	BOOL bMidMoCapStarted = TRUE
	BOOL bAllWarpedIntoNightclub = FALSE
	BOOL bNoneDefendingBuyer = FALSE
	BOOL bSomeoneDoingPosterAnims = FALSE
	
	INT iHacksComplete
	
	FLOAT fTotalScore = GET_TOTAL_SCORE_STARTING_POINT()
	BOOL bIncreasingScore
	
	// Reset data.
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		sPlayer[iPlayer].playerId = INVALID_PLAYER_INDEX()
		sPlayer[iPlayer].iParticipantId = (-1)
		sPlayer[iPlayer].playerPedId = pedTemp
		CLEAR_BIT(iPlayerOkBitset, iPlayer)
		CLEAR_BIT(iPlayerDeadBitset, iPlayer)
	ENDREPEAT
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		sParticipant[iParticipant].participantId = INVALID_PARTICIPANT_INDEX()
		sParticipant[iParticipant].iPlayerId = (-1)
		CLEAR_BIT(iParticipantActiveBitset, iParticipant)
		sParticipant[iParticipant].tl63Name = tl63Temp
		sParticipant[iParticipant].bIsBoss = FALSE
		sParticipant[iParticipant].bIsGoon = FALSE
	ENDREPEAT
	
	REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
	
		MissionEntityHolderPartID[iMissionEntity] = INVALID_PARTICIPANT_INDEX()
		MissionEntityHolderPlayerID[iMissionEntity] = INVALID_PLAYER_INDEX()
	
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			CLEAR_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_I_AM_OCCUPIED #IF IS_DEBUG_BUILD , TRUE #ENDIF)
			CLEAR_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_I_AM_ATTACHED_TO_CARGOBOB #IF IS_DEBUG_BUILD , TRUE #ENDIF)
			CLEAR_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_NEAR_PED_TO_COLLECT #IF IS_DEBUG_BUILD , TRUE #ENDIF)
			CLEAR_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_LIMO_HIDDEN #IF IS_DEBUG_BUILD , TRUE #ENDIF)
		ENDIF
	ENDREPEAT
	
	INT iVehicle
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		REPEAT NUM_MISSION_VEHICLES iVehicle
			CLEAR_VEHICLE_BIT(iVehicle, eVEHICLEBITSET_VEH_OCCUPIED #IF IS_DEBUG_BUILD , TRUE #ENDIF)
			CLEAR_VEHICLE_BIT(iVehicle, eVEHICLEBITSET_VEH_CARGOBOBBED #IF IS_DEBUG_BUILD , TRUE #ENDIF)
			CLEAR_VEHICLE_BIT(iVehicle, eVEHICLEBITSET_VEH_DELIVERED #IF IS_DEBUG_BUILD , TRUE #ENDIF)
		ENDREPEAT
	ENDIF
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		CLEAR_SERVER_BIT(eSERVERBITSET_SOMEONE_TRIGGERING_DROPOFF #IF IS_DEBUG_BUILD , TRUE #ENDIF)
	ENDIF
		
	//currentVehicle = vehTemp
	//eCurrentVehicleModel = DUMMY_MODEL_FOR_SCRIPT
//	vMyBossCoords = << 0.0, 0.0, 0.0 >>
	//vFocusParticipantCoords = << 0.0, 0.0, 0.0 >>
	iNumGoonsOnScript = 0
//	fMyDistanceFromBoss = 999999.0
	
	IF GET_MODE_STATE() = eMODESTATE_LEAVE_AREA
		bAllLeftArea = TRUE
	ENDIF
	IF GET_MODE_STATE() = eMODESTATE_DELIVER_CUTSCENE
		bAllCutsceneComplete = TRUE
		bAllDeliveryComplete = TRUE
		bReadyForCutscene = TRUE
	ENDIF
	
	IF DOES_MEGA_BUSINESS_VARIATION_HAVE_WARP_INTO_NIGHTCLUB(GET_MEGA_BUSINESS_VARIATION())
		IF GET_MODE_STATE() = eMODESTATE_WARP_INTO_NIGHTCLUB
			IF HAS_NET_TIMER_EXPIRED(stWarpIntoClubDelayTimer, 1500)
				bAllWarpedIntoNightclub = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF GET_MEGA_BUSINESS_VARIATION() = MBV_SELL_PROTECT_BUYER
		IF GET_MODE_STATE() = eMODESTATE_DEFEND_AREA
			bNoneDefendingBuyer = TRUE
		ENDIF
	ENDIF
	
	// Refill data.
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant))
			
			sParticipant[iParticipant].participantId = INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant)
			SET_BIT(iParticipantActiveBitset, iParticipant)
			
			playerTemp = NETWORK_GET_PLAYER_INDEX(sParticipant[iParticipant].participantId)
			iPlayer = NATIVE_TO_INT(playerTemp)
			
			IF IS_NET_PLAYER_OK(playerTemp, FALSE)
				
				sPlayer[iPlayer].playerId = playerTemp
				sParticipant[iParticipant].iPlayerId = iPlayer
				sPlayer[iPlayer].iParticipantId = iParticipant
				sPlayer[iPlayer].playerPedId = GET_PLAYER_PED(playerTemp)
				sParticipant[iParticipant].tl63Name = GET_PLAYER_NAME(playerTemp)
				
				SET_BIT(iPlayerOkBitset, iPlayer)
				
				IF IS_ENTITY_DEAD(sPlayer[iPlayer].playerPedId)
					SET_BIT(iPlayerDeadBitset, iPlayer)
				ENDIF
				
				// Track if boss or a goon.
				IF GB_IS_PLAYER_BOSS_OF_A_GANG(sPlayer[iPlayer].playerId)
					sParticipant[iParticipant].bIsBoss = TRUE
				ELIF GB_IS_PLAYER_MEMBER_OF_A_GANG(sPlayer[iPlayer].playerId, FALSE)
					sParticipant[iParticipant].bIsGoon = TRUE
				ENDIF
					
				// Track if in a target vehicle.
//				IF sPlayer[iPlayer].playerPedId = localPlayerPed
				IF iFocusParticipant = iParticipant
					//vFocusParticipantCoords = GET_ENTITY_COORDS(sPlayer[iPlayer].playerPedId)
					IF IS_PED_IN_ANY_VEHICLE(sPlayer[iPlayer].playerPedId)
						//currentVehicle = GET_VEHICLE_PED_IS_IN(sPlayer[iPlayer].playerPedId)
						//eCurrentVehicleModel = GET_ENTITY_MODEL(currentVehicle)
					ENDIF
				ENDIF
				
				IF sParticipant[iParticipant].bIsBoss
					bNoBossOnScript = FALSE
//					vMyBossCoords = GET_ENTITY_COORDS(sPlayer[iPlayer].playerPedId, FALSE) // Track my boss coords.
					IF NOT IS_BIT_SET(iPlayerDeadBitset, iPlayer)
					AND NOT IS_BIT_SET(iPlayerDeadBitset, NATIVE_TO_INT(localPlayerId))
//						fMyDistanceFromBoss = GET_DISTANCE_BETWEEN_COORDS(vFocusParticipantCoords, vMyBossCoords)
					ENDIF
				ELIF sParticipant[iParticipant].bIsGoon
					iNumGoonsOnScript++ // Track number of goons from my gang on this script.
				ENDIF
				
				IF NOT IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_COMPLETED_REWARDS)
//					PRINTLN(GET_PLAYER_NAME(sPlayer[iPlayer].playerId), " has not completed their rewards")
					bAllCompletedRewards = FALSE
				ENDIF
				
				IF NOT IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_I_WATCHED_MID_MISSION_MOCAP)
//					PRINTLN(GET_PLAYER_NAME(sPlayer[iPlayer].playerId), " has not watched the mid mission mocap")
					bMidMissionMocapComplete = FALSE
				ENDIF
				
				IF NOT IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_I_WATCHED_SCRIPTED_CUTSCENE)
//					PRINTLN(GET_PLAYER_NAME(sPlayer[iPlayer].playerId), " hasnt watched scripted cutscene")
					bScriptedCutsceneComplete = FALSE
				ENDIF
				
				IF NOT IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_WATCHING_MID_MISSION_MOCAP)
				AND NOT IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_I_WATCHED_MID_MISSION_MOCAP)
//					PRINTLN(GET_PLAYER_NAME(sPlayer[iPlayer].playerId), " is not watching the cutscene and hasnt watched it")
					bMidMoCapStarted = FALSE
				ENDIF
				
				IF NOT IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_LEFT_EOM_AREA)
//					PRINTLN(GET_PLAYER_NAME(sPlayer[iPlayer].playerId), " has not left EOM leave area")
					bAllLeftArea = FALSE
				ENDIF

				IF NOT IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_MISSION_EOM_DELIVERY_CUTSCENE_COMPLETE)
//					PRINTLN(GET_PLAYER_NAME(sPlayer[iPlayer].playerId), " is has not completed EOM delivery cutscene")
					bAllDeliveryComplete = FALSE
				ENDIF

				IF NOT IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_CUTSCENE_FINISHED)
//					PRINTLN(GET_PLAYER_NAME(sPlayer[iPlayer].playerId), " is has not finished cutscene")
					bAllCutsceneComplete = FALSE
				ENDIF

				IF NOT IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_I_AM_READY_FOR_CUTSCENE)
//					PRINTLN(GET_PLAYER_NAME(sPlayer[iPlayer].playerId), " is not ready for cutscene")
					bReadyForCutscene = FALSE
				ENDIF
				
				IF IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_I_AM_DEFENDING_BUYER)
//					PRINTLN(GET_PLAYER_NAME(sPlayer[iPlayer].playerId), " is defending buyer")
					bNoneDefendingBuyer = FALSE
				ENDIF
				
				IF IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_I_AM_DOING_POSTER_ANIMATION)
//					PRINTLN(GET_PLAYER_NAME(sPlayer[iPlayer].playerId), " is doing poster animation")
					bSomeoneDoingPosterAnims = TRUE
				ENDIF
				 
				IF IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_I_AM_WARPING_INTO_NIGHTCLUB)
					IF NOT IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_I_HAVE_WARPED_INTO_NIGHTCLUB)
						bAllWarpedIntoNightclub = FALSE
					ELSE
						PRINTLN("[MB_MISSION] waiting for ", GET_PLAYER_NAME(sPlayer[iPlayer].playerId), " to finish warping into the club")
					ENDIF
				ENDIF
				
				REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
					IF IS_MISSION_ENTITY_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iMissionEntity, eMISSIONENTITYCLIENTBITSET_I_AM_OCCUPIED)
						MissionEntityHolderPartID[iMissionEntity] = sParticipant[iParticipant].participantId
						MissionEntityHolderPlayerID[iMissionEntity] = sPlayer[iPlayer].playerId
					ENDIF
				ENDREPEAT

				IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
					
					IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_SOMEONE_HAS_REACHED_GOTO_LOCATION)
						IF NOT HAS_NET_TIMER_STARTED(serverBD.GotoTriggerTimer)
						OR (HAS_NET_TIMER_STARTED(serverBD.GotoTriggerTimer) AND HAS_NET_TIMER_EXPIRED(serverBD.GotoTriggerTimer, 3000))
							IF IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_I_HAVE_REACHED_GOTO_LOCATION)
								PRINTLN("[MB_MISSION] participant ", iParticipant, " has reached goto location.")
								SET_SERVER_BIT(eSERVERBITSET_SOMEONE_HAS_REACHED_GOTO_LOCATION)	
								RESET_NET_TIMER(serverBD.GotoTriggerTimer)
								
								serverBD.iCurrentGotoLocation++
								PRINTLN("[MB_MISSION] serverBD.iCurrentGotoLocation = ", serverBD.iCurrentGotoLocation)
							ENDIF
						ENDIF
					ENDIF
					
					SWITCH GET_MEGA_BUSINESS_VARIATION()
						CASE MBV_SELL_FIND_BUYER
							INT iNonExportVehicle 
							IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_SET_VEH_RADIO_EVERY_FRAME)
								REPEAT NUM_MISSION_VEHICLES iNonExportVehicle
									IF IS_VEHICLE_BIT_SET(iNonExportVehicle, eVEHICLEBITSET_RADIO_CAR)
										IF IS_VEHICLE_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iNonExportVehicle, eVEHICLECLIENTBITSET_SET_RADIO)
											SET_SERVER_BIT(eSERVERBITSET_SET_VEH_RADIO_EVERY_FRAME)
										ENDIF
									ENDIF
								ENDREPEAT
							ENDIF
						BREAK
						CASE MBV_COLLECT_DJ_HOOKED
							IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_MADONNA_SCENE_COMPLETE)
								IF IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_MADONNA_SCENE_DONE)
									SET_SERVER_BIT(eSERVERBITSET_MADONNA_SCENE_COMPLETE)
								ENDIF
							ENDIF
							IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_HOOKED_ARREST_TRIGGERED_EARLY)
								IF IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_ARREST_TRIGGERED_EARLY)
									SET_SERVER_BIT(eSERVERBITSET_HOOKED_ARREST_TRIGGERED_EARLY)
								ENDIF
							ENDIF
							IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_SOMEONE_STARTED_HOOKED_SYNC_SCENE)
								IF IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_SOMEONE_STARTED_HOOKED_SYNC_SCENE)
									SET_SERVER_BIT(eSERVERBITSET_SOMEONE_STARTED_HOOKED_SYNC_SCENE)
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
					
					IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_SOMEONE_COLLECTED_ITEM)
						IF IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_I_HAVE_COLLECTED_ITEM)
							PRINTLN("[MB_MISSION] participant ", iParticipant, " has eCLIENTBITSET_I_HAVE_COLLECTED_ITEM.")
							SET_SERVER_BIT(eSERVERBITSET_SOMEONE_COLLECTED_ITEM)	
							serverBD.iCurrentItemsCollected++
						ENDIF
					ENDIF
					
					IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_STARTED_CRASH_LANDING_SMOKE_PTFX)
						IF IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_STARTED_CRASH_LANDING_SMOKE_PTFX)
							PRINTLN("[MB_MISSION] participant ", iParticipant, " has eCLIENTBITSET_STARTED_CRASH_LANDING_SMOKE_PTFX.")
							SET_SERVER_BIT(eSERVERBITSET_STARTED_CRASH_LANDING_SMOKE_PTFX)	
						ENDIF
					ENDIF
					
					
					IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_MAKE_ALL_PEDS_REACT)
						IF IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_I_WANT_PEDS_TO_REACT)
							PRINTLN("[MB_MISSION] participant ", iParticipant, " wants all the peds to attack. PlayerId = ", iPlayer)
							SET_SERVER_BIT(eSERVERBITSET_MAKE_ALL_PEDS_REACT)
							
							IF SHOULD_GIVE_WANTED_LEVEL_FOR_ALERTING_PEDS()
								IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_GIVEN_WANTED_RATING)
									GIVE_WANTED_LEVEL_TO_GANG(GET_MAX_WANTED_LEVEL_FOR_VARIATION(), FALSE, 0)
									SET_SERVER_BIT(eSERVERBITSET_GIVEN_WANTED_RATING)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					// Find buyer logic
					IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_BUYER_FOUND)
						IF IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_BUYER_FOUND)
							PRINTLN("[MB_MISSION][CLUE] participant ", iParticipant, " eCLIENTBITSET_BUYER_FOUND ", iPlayer)
							SET_SERVER_BIT(eSERVERBITSET_BUYER_FOUND)
						ENDIF
						
						IF NOT HAS_NET_TIMER_STARTED(serverBD.stTimeInArea)
							IF IS_MISSION_ENTITY_BIT_SET(MISSION_ENTITY_ONE, eMISSIONENTITYBITSET_FIND_BUYER_IN_CLUE_ZONE) 
								PRINTLN("[MB_MISSION][CLUE] participant ", iParticipant, " START_NET_TIMER(serverBD.stTimeInArea) ", iPlayer)
								START_NET_TIMER(serverBD.stTimeInArea)
							ENDIF
						ELSE
							IF HAS_NET_TIMER_EXPIRED(serverBD.stTimeInArea, ciCLUE_TIME)
							
								SET_SERVER_BIT(eSERVERBITSET_CLUE_TIME_EXPIRED)
	
								PRINTLN("[MB_MISSION][CLUE] ciCLUE_TIME expired, eSERVERBITSET_BUYER_FOUND ")
								SET_SERVER_BIT(eSERVERBITSET_BUYER_FOUND)
							ENDIF
						ENDIF
						
					ENDIF
					
					IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_WITHIN_RANGE_OF_MISSION_ENTITY)
						IF IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_WITHIN_RANGE_OF_MISSION_ENTITY)
							PRINTLN("[MB_MISSION] participant ", iParticipant, " [4678169] eCLIENTBITSET_WITHIN_RANGE_OF_MISSION_ENTITY ", iPlayer)
							SET_SERVER_BIT(eSERVERBITSET_WITHIN_RANGE_OF_MISSION_ENTITY)
						ENDIF
					ENDIF
					
					IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_ROADBLOCK_FLAG)
						IF IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_ROADBLOCK_FLAG)
							PRINTLN("[MB_MISSION] participant ", iParticipant, " eCLIENTBITSET_ROADBLOCK_FLAG ", iPlayer)
							SET_SERVER_BIT(eSERVERBITSET_ROADBLOCK_FLAG)
						ENDIF
					ENDIF
					
					IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_RESTRICTED_AREA_HAS_BEEN_ENTERED)
						IF IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_I_AM_IN_RESTRICTED_AREA)
							PRINTLN("[MB_MISSION] participant ", iParticipant, " has entered a restricted area. PlayerId = ", iPlayer)
							SET_SERVER_BIT(eSERVERBITSET_RESTRICTED_AREA_HAS_BEEN_ENTERED)
						ENDIF
					ENDIF
					
					IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_GANG_CHASE_CAN_SPAWN)
						IF IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_TRIGGER_AMBUSH)
							PRINTLN("[MB_MISSION] participant ", iParticipant, " has set eCLIENTBITSET_TRIGGER_AMBUSH. PlayerId = ", iPlayer)
							SET_SERVER_BIT(eSERVERBITSET_GANG_CHASE_CAN_SPAWN)
						ENDIF
					ENDIF
					
					IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_PEDS_AT_LOCATION_0_SHOULD_ENTER)
						IF IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_PEDS_AT_LOCATION_0_SHOULD_ENTER)
							PRINTLN("[MB_MISSION] participant ", iParticipant, " says that peds at location 0 should enter. PlayerId = ", iPlayer)
							SET_SERVER_BIT(eSERVERBITSET_PEDS_AT_LOCATION_0_SHOULD_ENTER)
						ENDIF
					ENDIF
					
					IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_PEDS_AT_LOCATION_1_SHOULD_ENTER)
						IF IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_PEDS_AT_LOCATION_1_SHOULD_ENTER)
							PRINTLN("[MB_MISSION] participant ", iParticipant, " says that peds at location 1 should enter. PlayerId = ", iPlayer)
							SET_SERVER_BIT(eSERVERBITSET_PEDS_AT_LOCATION_1_SHOULD_ENTER)
						ENDIF
					ENDIF
					
					IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_PEDS_AT_LOCATION_2_SHOULD_ENTER)
						IF IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_PEDS_AT_LOCATION_2_SHOULD_ENTER)
							PRINTLN("[MB_MISSION] participant ", iParticipant, " says that peds at location 2 should enter. PlayerId = ", iPlayer)
							SET_SERVER_BIT(eSERVERBITSET_PEDS_AT_LOCATION_2_SHOULD_ENTER)
						ENDIF
					ENDIF
					
					IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_CAR_WASH_COMPLETE)
						IF IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_I_HAVE_COMPLETED_CAR_WASH)
							PRINTLN("[MB_MISSION] participant ", iParticipant, " has eCLIENTBITSET_I_HAVE_COMPLETED_CAR_WASH ", iPlayer)
							SET_SERVER_BIT(eSERVERBITSET_CAR_WASH_COMPLETE)
						ENDIF
					ENDIF
					
					IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_ENTITY_TO_FOLLOW_HAS_BEEN_SIGNALED)
						IF IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_I_HAVE_SIGNALED_ENTITY_TO_FOLLOW)
							PRINTLN("[MB_MISSION] participant ", iParticipant, " has eCLIENTBITSET_I_HAVE_SIGNALED_ENTITY_TO_FOLLOW ", iPlayer)
							SET_SERVER_BIT(eSERVERBITSET_ENTITY_TO_FOLLOW_HAS_BEEN_SIGNALED)
						ENDIF
					ENDIF
					
					IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_FOLLOW_HELI_REVEAL_DROP_OFF_FOUND)
						IF IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_DELIVERY_VEHICLE_NEAR_DROP_OFF)
							PRINTLN("[MB_MISSION] participant ", iParticipant, " has eCLIENTBITSET_DELIVERY_VEHICLE_NEAR_DROP_OFF ", iPlayer)
							SET_SERVER_BIT(eSERVERBITSET_FOLLOW_HELI_REVEAL_DROP_OFF_FOUND)
						ENDIF
					ENDIF
					
					IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_COLLECT_STAFF_PED_DAMAGED)
						IF IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_I_DAMAGED_A_COLLECT_STAFF_PED)
							PRINTLN("[MB_MISSION] participant ", iParticipant, " has eCLIENTBITSET_I_DAMAGED_A_COLLECT_STAFF_PED ", iPlayer)
							SET_SERVER_BIT(eSERVERBITSET_COLLECT_STAFF_PED_DAMAGED)
						ENDIF
					ENDIF
					
					IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_LOST_ALL_PURSUERS)
						IF IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_LOST_ALL_PURSUERS)
							PRINTLN("[MB_MISSION] participant ", iParticipant, " has eCLIENTBITSET_LOST_ALL_PURSUERS ", iPlayer)
							SET_SERVER_BIT(eSERVERBITSET_LOST_ALL_PURSUERS)
							IF NOT IS_PED_BIT_SET(0, ePEDBITSET_COLLECTED)
								SET_SERVER_BIT(eSERVERBITSET_LOST_ALL_PURSUERS_BEFORE_COLLECTION)
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_DROPOFF_TRIGGERED)
						IF serverBD.fTotalScore > 0
							IF IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_TRIGGERING_DROPOFF)
								PRINTLN("[MB_MISSION] participant ", iParticipant, " has eCLIENTBITSET_TRIGGERING_DROPOFF ", iPlayer)
								SET_SERVER_BIT(eSERVERBITSET_DROPOFF_TRIGGERED)
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_BOSS_NEAR_LANDED_AIRCRAFT)
						IF IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_NEAR_LANDED_AIRCRAFT)
						AND sParticipant[iParticipant].bIsBoss
						AND IS_PLAYER_IN_POSSESSION_OF_A_MISSION_ENTITY(playerTemp)
							PRINTLN("[MB_MISSION] participant ", iParticipant, " has eCLIENTBITSET_NEAR_LANDED_AIRCRAFT ", iPlayer)
							SET_SERVER_BIT(eSERVERBITSET_BOSS_NEAR_LANDED_AIRCRAFT)
						ENDIF
					ENDIF
					
					IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_SAFE_TO_FADE_OUT_PEDS)
						IF IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_SAFE_TO_FADE_OUT_PEDS)
							PRINTLN("[MB_MISSION] participant ", iParticipant, " has eCLIENTBITSET_SAFE_TO_FADE_OUT_PEDS ", iPlayer)
							SET_SERVER_BIT(eSERVERBITSET_SAFE_TO_FADE_OUT_PEDS)
						ENDIF
					ENDIF	
					
					IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_PEDS_SHOULD_EXIT_VEHICLE_FOR_WARP_INTO_NIGHTCLUB)
						IF IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_I_WANT_PEDS_TO_EXIT_VEHICLE_FOR_WARP_INTO_NIGHTCLUB)
							PRINTLN("[MB_MISSION] participant ", iParticipant, " has eCLIENTBITSET_I_WANT_PEDS_TO_EXIT_VEHICLE_FOR_WARP_INTO_NIGHTCLUB ", iPlayer)
							SET_SERVER_BIT(eSERVERBITSET_PEDS_SHOULD_EXIT_VEHICLE_FOR_WARP_INTO_NIGHTCLUB)
						ENDIF
					ENDIF	
					
					IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_GOODS_REMOVED_FROM_BUSINESS_HUB)
						IF sParticipant[iParticipant].bIsBoss
							IF IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_I_HAVE_REMOVED_GOODS_FROM_BUSINESS_HUB)
								PRINTLN("[MB_MISSION] boss participant ", iParticipant, " has removed goods from business hub. PlayerId = ", iPlayer)
								SET_SERVER_BIT(eSERVERBITSET_GOODS_REMOVED_FROM_BUSINESS_HUB)
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_SOMEONE_STARTED_SCRIPTED_CUTSCENE)
						IF IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_STARTED_SCRIPTED_CUTSCENE)
							PRINTLN("[MB_MISSION] participant ", iParticipant, " has eCLIENTBITSET_STARTED_SCRIPTED_CUTSCENE ", iPlayer)
							SET_SERVER_BIT(eSERVERBITSET_SOMEONE_STARTED_SCRIPTED_CUTSCENE)
						ENDIF
					ENDIF
										
//					IF IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_I_AM_IN_RESTRICTED_AREA)
//						IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_ANY_MISSION_ENTITY_IN_RESTRICTED_AREA)
//							IF IS_PLAYER_IN_POSSESSION_OF_ANY_MISSION_ENTITY(NETWORK_GET_PLAYER_INDEX(sParticipant[iParticipant].participantId))
//								SET_SERVER_BIT(eSERVERBITSET_ANY_MISSION_ENTITY_IN_RESTRICTED_AREA #IF IS_DEBUG_BUILD , TRUE #ENDIF)
//							ENDIF
//						ENDIF
//						IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_ANY_PLAYER_IN_RESTRICTED_AREA)
//							SET_SERVER_BIT(eSERVERBITSET_ANY_PLAYER_IN_RESTRICTED_AREA #IF IS_DEBUG_BUILD , TRUE #ENDIF)
//						ENDIF
//					ENDIF
				
					iHacksComplete += playerBD[iParticipant].iNumCompletedHackingGames
					
					IF SHOULD_USE_SERVER_TOTAL_SCORE()
						IF IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_TRIGGERING_DROPOFF)
							SET_SERVER_BIT(eSERVERBITSET_SOMEONE_TRIGGERING_DROPOFF #IF IS_DEBUG_BUILD , TRUE #ENDIF)
							bIncreasingScore = TRUE
						ENDIF
					ELSE
						fTotalScore += playerBD[iParticipant].fScore 
					ENDIF
					
					IF GET_MEGA_BUSINESS_VARIATION() = MBV_SELL_NOT_A_SCRATCH
					OR GET_MEGA_BUSINESS_VARIATION() = MBV_SUPPLY_RUN
						// Make sure we only update mission entity values once we know the client has set the cash value
						IF IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET_INITIALISED_VARS)
							IF serverBD.sMissionEntity[MISSION_ENTITY_ONE].iBonusCash > playerBD[iParticipant].iMissionEntityBonusCash[MISSION_ENTITY_ONE]
							AND serverBD.sMissionEntity[MISSION_ENTITY_ONE].iBonusCash > 0
								PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(), "] - Player #", iPlayer, " has lower bonus cash value for mission entity. Old bonus cash: ", serverBD.sMissionEntity[MISSION_ENTITY_ONE].iBonusCash, " - New bonus cash: ", playerBD[iParticipant].iMissionEntityBonusCash[MISSION_ENTITY_ONE])
								serverBD.sMissionEntity[MISSION_ENTITY_ONE].iBonusCash = playerBD[iParticipant].iMissionEntityBonusCash[MISSION_ENTITY_ONE]
								IF serverBD.sMissionEntity[MISSION_ENTITY_ONE].iBonusCash < 0
									serverBD.sMissionEntity[MISSION_ENTITY_ONE].iBonusCash = 0
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					// Client/Server Mission Entity Loop
					
					REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
						IF NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_I_AM_OCCUPIED)
							IF IS_MISSION_ENTITY_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iMissionEntity, eMISSIONENTITYCLIENTBITSET_I_AM_OCCUPIED)
								
								SET_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_I_AM_OCCUPIED #IF IS_DEBUG_BUILD , TRUE #ENDIF)
								
								IF IS_PLAYER_PILOT_OF_A_CARGOBOB(playerTemp)
									SET_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_I_AM_ATTACHED_TO_CARGOBOB #IF IS_DEBUG_BUILD , TRUE #ENDIF)
								ENDIF
								
								IF NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_I_HAVE_BEEN_ENTERED_AT_LEAST_ONCE)
									SET_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_I_HAVE_BEEN_ENTERED_AT_LEAST_ONCE)
								ENDIF
								
								IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_SOMEONE_USING_CAR_WASH)
									IF IS_PLAYER_USING_CARWASH(iPlayer)
										PRINTLN("[MB_MISSION] participant ", iParticipant, " is bDrivingThroughCarwash.")
										SET_SERVER_BIT(eSERVERBITSET_SOMEONE_USING_CAR_WASH)	
									ENDIF
								ENDIF
								
								IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[iMissionEntity].netId)
								AND IS_ENTITY_ALIVE(NET_TO_VEH(serverBD.sMissionEntity[iMissionEntity].netId))
									IF IS_ENTITY_IN_FIND_BUYER_CLUE_ZONE(NET_TO_ENT(serverBD.sMissionEntity[iMissionEntity].netId))
												
										SET_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_FIND_BUYER_IN_CLUE_ZONE) 
									ELSE
										CLEAR_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_FIND_BUYER_IN_CLUE_ZONE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
												
						IF NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_ATTACHED_TO_VEHICLE)
							IF IS_MISSION_ENTITY_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iMissionEntity, eMISSIONENTITYCLIENTBITSET_ATTACHED_TO_VEHICLE)
								PRINTLN("[MB_MISSION] participant ", iParticipant, " has attached MissionEntity #", iMissionEntity, " to vehicle. PlayerId = ", iPlayer)
								SET_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_ATTACHED_TO_VEHICLE)
							ENDIF
						ENDIF
						
						IF NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_DETACHED_FROM_VEHICLE)
							IF IS_MISSION_ENTITY_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iMissionEntity, eMISSIONENTITYCLIENTBITSET_DETACHED_FROM_VEHICLE)
								PRINTLN("[MB_MISSION] participant ", iParticipant, " has detached MissionEntity #", iMissionEntity, " from vehicle. PlayerId = ", iPlayer)
								SET_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_DETACHED_FROM_VEHICLE)
							ENDIF
						ENDIF
						
						IF NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_PLAYED_COLLECTOR_LAND_SOUND)
							IF IS_MISSION_ENTITY_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iMissionEntity, eMISSIONENTITYCLIENTBITSET_PLAYED_COLLECTOR_LAND_SOUND)
								PRINTLN("[MB_MISSION] participant ", iParticipant, " has played MissionEntity #", iMissionEntity, " detached sound. PlayerId = ", iPlayer)
								SET_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_PLAYED_COLLECTOR_LAND_SOUND)
							ENDIF
						ENDIF
						
						IF NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_A_PARTICIPANT_WANTS_ME_TO_SPAWN)
							IF IS_MISSION_ENTITY_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iMissionEntity, eMISSIONENTITYCLIENTBITSET_I_WANT_TO_SPAWN)
								PRINTLN("[MB_MISSION] participant ", iParticipant, " wants the MissionEntity #", iMissionEntity, " to spawn. PlayerId = ", iPlayer)
								SET_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_A_PARTICIPANT_WANTS_ME_TO_SPAWN)
							ENDIF
						ENDIF
						
						IF NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_DESTROYED)
							IF IS_MISSION_ENTITY_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iMissionEntity, eMISSIONENTITYCLIENTBITSET_DESTROYED)
								PRINTLN("[MB_MISSION] participant ", iParticipant, " sees the MissionEntity #", iMissionEntity, " as destroyed. PlayerId = ", iPlayer)
								SET_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_DESTROYED)
							ENDIF
						ENDIF
						
						IF NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_LOCKED_FOR_ALL_PLAYERS)
							IF IS_MISSION_ENTITY_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iMissionEntity, eMISSIONENTITYCLIENTBITSET_LOCKED_FOR_ALL_PLAYERS)
								PRINTLN("[MB_MISSION] participant ", iParticipant, " has locked doors for all players on MissionEntity #", iMissionEntity, ". PlayerId = ", iPlayer)
								SET_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_LOCKED_FOR_ALL_PLAYERS)
							ENDIF
						ENDIF
						
						IF NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_LIMO_HIDDEN)
							IF IS_MISSION_ENTITY_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iMissionEntity, eMISSIONENTITYCLIENTBITSET_HOOKED_LIMO_HIDDEN)
								SET_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_LIMO_HIDDEN #IF IS_DEBUG_BUILD , TRUE #ENDIF)
							ENDIF
						ENDIF
						
						IF NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_LIMO_RESET)
							IF IS_MISSION_ENTITY_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iMissionEntity, eMISSIONENTITYCLIENTBITSET_LIMO_RESET)
								PRINTLN("[MB_MISSION] participant ", iParticipant, " has eMISSIONENTITYBITSET_LIMO_RESET for all players on MissionEntity #", iMissionEntity, ". PlayerId = ", iPlayer)
								SET_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_LIMO_RESET)
							ENDIF
						ENDIF
						
						IF NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_LIMO_EXCLUSIVE)
							IF IS_MISSION_ENTITY_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iMissionEntity, eMISSIONENTITYCLIENTBITSET_LIMO_EXCLUSIVE)
								PRINTLN("[MB_MISSION] participant ", iParticipant, " has eMISSIONENTITYBITSET_LIMO_EXCLUSIVE for all players on MissionEntity #", iMissionEntity, ". PlayerId = ", iPlayer)
								SET_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_LIMO_EXCLUSIVE)
							ENDIF
						ENDIF
						
						IF NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_LIMO_FADED_IN)
							IF IS_MISSION_ENTITY_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iMissionEntity, eMISSIONENTITYCLIENTBITSET_HOOKED_LIMO_FADED_IN)
								PRINTLN("[MB_MISSION] participant ", iParticipant, " has eMISSIONENTITYBITSET_LIMO_FADED_IN for all players on MissionEntity #", iMissionEntity, ". PlayerId = ", iPlayer)
								SET_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_LIMO_FADED_IN)
							ENDIF
						ENDIF
						
						IF NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_NEAR_PED_TO_COLLECT)
							IF IS_MISSION_ENTITY_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iMissionEntity, eMISSIONENTITYCLIENTBITSET_NEAR_PED_TO_COLLECT)
								SET_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_NEAR_PED_TO_COLLECT #IF IS_DEBUG_BUILD , TRUE #ENDIF)
								IF serverBD.iPedStaffBeingCollected = -1
								AND NOT IS_SERVER_BIT_SET(eSERVERBITSET_SETUP_ALL_STAFF_COLLECTED)
									serverBD.iPedStaffBeingCollected = playerBD[NATIVE_TO_INT(sParticipant[iParticipant].participantId)].iPedStaffBeingCollected
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_KNACKERED)
							IF IS_MISSION_ENTITY_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iMissionEntity, eMISSIONENTITYCLIENTBITSET_KNACKERED)
								PRINTLN("[MB_MISSION] participant ", iParticipant, " has knackered MissionEntity #", iMissionEntity, ". PlayerId = ", iPlayer)
								SET_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_KNACKERED)
							ENDIF
						ENDIF
						
						IF NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_CANNOT_BE_ATTACHED)
							IF IS_MISSION_ENTITY_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iMissionEntity, eMISSIONENTITYCLIENTBITSET_CANNOT_BE_ATTACHED)
								PRINTLN("[MB_MISSION] participant ", iParticipant, " has prevented towing/attaching on MissionEntity #", iMissionEntity, ". PlayerId = ", iPlayer)
								SET_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_CANNOT_BE_ATTACHED)
							ENDIF
						ENDIF
						
						IF NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_DOOR_OPENED)
							IF IS_MISSION_ENTITY_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iMissionEntity, eMISSIONENTITYCLIENTBITSET_DOOR_OPENED)
								PRINTLN("[MB_MISSION] participant ", iParticipant, " has opened doof of MissionEntity #", iMissionEntity, ". PlayerId = ", iPlayer)
								SET_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_DOOR_OPENED)
							ENDIF
						ENDIF
						
						// Drop offs
						
						IF HAS_MISSION_ENTITY_BEEN_CREATED(iMissionEntity)
						AND NOT HAS_MISSION_ENTITY_BEEN_DELIVERED_OR_DESTROYED(iMissionEntity)
							REPEAT GET_NUM_DROP_OFFS_FOR_MEGA_BUSINESS_VARIATION(GET_MEGA_BUSINESS_VARIATION()) iDropOff
								IF NOT IS_MISSION_ENTITY_DROP_OFF_BIT_SET(iMissionEntity, iDropOff, ciDROP_OFF_DONE)
								AND NOT IS_MISSION_ENTITY_DROP_OFF_BIT_SET(iMissionEntity, iDropOff, ciDROP_OFF_DISABLED)
									IF IS_MISSION_ENTITY_CLIENT_DROP_OFF_BIT_SET(sParticipant[iParticipant].participantId, iMissionEntity, iDropOff, ciDROP_OFF_DONE)
										SET_MISSION_ENTITY_DROP_OFF_BIT(iMissionEntity, iDropOff, ciDROP_OFF_DONE)
										serverBD.sMissionEntity[iMissionEntity].iDeliveriesMade++
										serverBD.iTotalDeliveredCount++
										ENABLE_DROP_OFFS_AFTER_DELIVER(serverBD.iTotalDeliveredCount)
										CLEAR_SERVER_DATA_ON_DROPOFF()
										PRINTLN("[MB_MISSION] participant ", iParticipant, " says that mission entity ", iMissionEntity, " has been to drop off ", iDropOff, ". Total deliveries made by this mission entity: ", serverBD.sMissionEntity[iMissionEntity].iDeliveriesMade)

										IF serverBD.sMissionEntity[iMissionEntity].iDeliveriesMade = serverBD.sMissionEntity[iMissionEntity].iDeliveriesToMake
											PRINTLN("[+++++ DELIVERED +++++]")
											PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - PROCESS_PARTICIPANT_LOOP - Mission entity ", iMissionEntity, " has completed its deliveries! Setting as delivered!")
											PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - PROCESS_PARTICIPANT_LOOP - 		serverBD.sMissionEntity[iMissionEntity].iDeliveriesToMake 	= ", serverBD.sMissionEntity[iMissionEntity].iDeliveriesToMake)
											PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - PROCESS_PARTICIPANT_LOOP - 		serverBD.sMissionEntity[iMissionEntity].iDeliveriesMade 	= ", serverBD.sMissionEntity[iMissionEntity].iDeliveriesMade)
											SET_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_DELIVERED)
										ENDIF
									ENDIF
								ENDIF
							ENDREPEAT
						ENDIF
					ENDREPEAT					
					
					// Client/Server Vehicle Loop
					REPEAT NUM_MISSION_VEHICLES iVehicle
						IF NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_SET_AS_NO_LONGER_NEEDED)
							IF IS_VEHICLE_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iVehicle, eVEHICLECLIENTBITSET_SET_AS_NO_LONGER_NEEDED)
								PRINTLN("[MB_MISSION] participant ", iParticipant, " has set vehicle #", iVehicle, " as no longer needed. PlayerId = ", iPlayer)
								SET_VEHICLE_BIT(iVehicle, eVEHICLEBITSET_SET_AS_NO_LONGER_NEEDED)
							ENDIF
						ENDIF
						
						IF NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_VEH_OCCUPIED)
							IF IS_VEHICLE_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iVehicle, eVEHICLECLIENTBITSET_VEH_OCCUPIED)
								PRINTLN("[MB_MISSION] participant ", iParticipant, " has set vehicle #", iVehicle, " as occupied. PlayerId = ", iPlayer)
								SET_VEHICLE_BIT(iVehicle, eVEHICLEBITSET_VEH_OCCUPIED)
							ENDIF
						ENDIF
						
						IF NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_VEH_CARGOBOBBED)
							IF IS_VEHICLE_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iVehicle, eVEHICLECLIENTBITSET_VEH_CARGOBOBBED)
								PRINTLN("[MB_MISSION] participant ", iParticipant, " has set vehicle #", iVehicle, " as eVEHICLEBITSET_VEH_CARGOBOBBED. PlayerId = ", iPlayer)
								SET_VEHICLE_BIT(iVehicle, eVEHICLEBITSET_VEH_CARGOBOBBED)
							ENDIF
						ENDIF
						
						IF NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_VEH_WARPED)
							IF IS_VEHICLE_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iVehicle, eVEHICLECLIENTBITSET_VEH_WARPED)
								PRINTLN("[MB_MISSION] participant ", iParticipant, " has set vehicle #", iVehicle, " as eVEHICLEBITSET_VEH_WARPED. PlayerId = ", iPlayer)
								SET_VEHICLE_BIT(iVehicle, eVEHICLEBITSET_VEH_WARPED)
							ENDIF
						ENDIF
						
						IF NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_VEH_DELIVERED)
							IF IS_VEHICLE_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iVehicle, eVEHICLECLIENTBITSET_VEH_DELIVERED)
								PRINTLN("[MB_MISSION] participant ", iParticipant, " has set vehicle #", iVehicle, " as eVEHICLEBITSET_VEH_DELIVERED. PlayerId = ", iPlayer)
								SET_VEHICLE_BIT(iVehicle, eVEHICLEBITSET_VEH_DELIVERED)
							ENDIF
						ENDIF
						
						IF NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_ATTACHED_TO_VEHICLE)
							IF IS_VEHICLE_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iVehicle, eVEHICLECLIENTBITSET_ATTACHED_TO_VEHICLE)
								PRINTLN("[MB_MISSION] participant ", iParticipant, " has set vehicle #", iVehicle, " as attached. PlayerId = ", iPlayer)
								SET_VEHICLE_BIT(iVehicle, eVEHICLEBITSET_ATTACHED_TO_VEHICLE)
							ENDIF
						ENDIF
						
						IF NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_OPENED_BOOT)
							IF IS_VEHICLE_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iVehicle, eVEHICLECLIENTBITSET_OPENED_BOOT)
								PRINTLN("[MB_MISSION] participant ", iParticipant, " has opened vehicle #", iVehicle, " boot. PlayerId = ", iPlayer)
								SET_VEHICLE_BIT(iVehicle, eVEHICLEBITSET_OPENED_BOOT)
							ENDIF
						ENDIF
						
						IF NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_OPENED_BONNET)
							IF IS_VEHICLE_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iVehicle, eVEHICLECLIENTBITSET_OPENED_BONNET)
								PRINTLN("[MB_MISSION] participant ", iParticipant, " has opened vehicle #", iVehicle, " bonnet. PlayerId = ", iPlayer)
								SET_VEHICLE_BIT(iVehicle, eVEHICLEBITSET_OPENED_BONNET)
							ENDIF
						ENDIF
						
						IF NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_DETACHED_FROM_VEHICLE)
							IF IS_VEHICLE_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iVehicle, eVEHICLECLIENTBITSET_DETACHED_FROM_VEHICLE)
								PRINTLN("[MB_MISSION] participant ", iParticipant, " has deatched vehicle #", iVehicle, ". PlayerId = ", iPlayer)
								SET_VEHICLE_BIT(iVehicle, eVEHICLEBITSET_DETACHED_FROM_VEHICLE)
							ENDIF
						ENDIF
						
						IF NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_PLAYED_COLLECTOR_LAND_SOUND)
							IF IS_VEHICLE_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iVehicle, eVEHICLECLIENTBITSET_PLAYED_COLLECTOR_LAND_SOUND)
								PRINTLN("[MB_MISSION] participant ", iParticipant, " has played deatched vehicle #", iVehicle, " sound. PlayerId = ", iPlayer)
								SET_VEHICLE_BIT(iVehicle, eVEHICLEBITSET_PLAYED_COLLECTOR_LAND_SOUND)
							ENDIF
						ENDIF
						
						IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_FROZEN_WAITING_FOR_PILOT)
							IF IS_VEHICLE_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iVehicle, eVEHICLECLIENTBITSET_UNFROZEN_AFTER_WAITING_FOR_PILOT)
								PRINTLN("[MB_MISSION] participant ", iParticipant, " has unfrozen vehicle #", iVehicle, " after pilot was created. PlayerId = ", iPlayer)
								CLEAR_VEHICLE_BIT(iVehicle, eVEHICLEBITSET_FROZEN_WAITING_FOR_PILOT)
							ENDIF
						ENDIF
						
						
						IF NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_INCREASED_ROPE_LENGTH)
							IF IS_VEHICLE_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iVehicle, eVEHICLECLIENTBITSET_INCREASED_ROPE_LENGTH)
								PRINTLN("[MB_MISSION] participant ", iParticipant, " has increased the rope length of cargobob vehicle #", iVehicle, ". PlayerId = ", iPlayer)
								SET_VEHICLE_BIT(iVehicle, eVEHICLEBITSET_INCREASED_ROPE_LENGTH)
							ENDIF
						ENDIF
						
						IF NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_DESTROYED)
							IF IS_VEHICLE_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iVehicle, eVEHICLECLIENTBITSET_DESTROYED)
								PRINTLN("[MB_MISSION] participant ", iParticipant, " says vehicle #", iVehicle, " is dead. PlayerId = ", iPlayer)
								SET_VEHICLE_BIT(iVehicle, eVEHICLEBITSET_DESTROYED)
							ENDIF
						ENDIF
						
						IF NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_STUCK)
							IF IS_VEHICLE_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iVehicle, eVEHICLECLIENTBITSET_STUCK)
								PRINTLN("[MB_MISSION] participant ", iParticipant, " says vehicle #", iVehicle, " is stuck. PlayerId = ", iPlayer)
								SET_VEHICLE_BIT(iVehicle, eVEHICLEBITSET_STUCK)
							ENDIF
						ENDIF
					ENDREPEAT
					
					// Client/Server ped loop
					INT iPed
					REPEAT NUM_MISSION_PEDS iPed
						IF GET_PED_STATE(iPed) != ePEDSTATE_INACTIVE
						
							IF NOT IS_PED_BIT_SET(iPed, ePEDBITSET_SIGNALED_TO_ENTER_VEHICLE)
								IF IS_PED_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iPed, ePEDCLIENTBITSET_SIGNALED_TO_ENTER_VEHICLE)
									PRINTLN("[MB_MISSION] participant ", iParticipant, " has signaled ped #", iPed, " to enter. PlayerId = ", iPlayer)
									SET_PED_BIT(iPed, ePEDBITSET_SIGNALED_TO_ENTER_VEHICLE)
								ENDIF
							ENDIF
							
							IF NOT IS_PED_BIT_SET(iPed, ePEDBITSET_COLLECTED)
								IF IS_PED_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iPed, ePEDCLIENTBITSET_COLLECTED)
									PRINTLN("[MB_MISSION] participant ", iParticipant, " says that ped #", iPed, " has been collected. PlayerId = ", iPlayer)
									SET_PED_BIT(iPed, ePEDBITSET_COLLECTED)
									IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_AT_LEAST_ONE_BAR_STAFF_COLLECTED)
										SET_SERVER_BIT(eSERVERBITSET_AT_LEAST_ONE_BAR_STAFF_COLLECTED)
									ENDIF
									serverBD.iPedStaffJustCollected = iPed
									PRINTLN("[MB_MISSION] iPedStaffJustCollected = ", serverBD.iPedStaffJustCollected)
									serverBD.iNumPedsCollected++
								ENDIF
							ENDIF
							
							IF NOT IS_PED_BIT_SET(iPed, ePEDBITSET_INCAPACITATED)
								IF IS_PED_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iPed, ePEDCLIENTBITSET_INCAPACITATED)
									PRINTLN("[MB_MISSION] participant ", iParticipant, " says that ped #", iPed, " has been incapacitated. PlayerId = ", iPlayer)
									SET_PED_BIT(iPed, ePEDBITSET_INCAPACITATED)
									serverBD.iNumPedsIncapacitated++
								ENDIF
							ENDIF
							
							IF NOT IS_PED_BIT_SET(iPed, ePEDBITSET_FADED_OUT)
								IF IS_PED_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iPed, ePEDCLIENTBITSET_FADED_OUT)
									PRINTLN("[MB_MISSION] participant ", iParticipant, " says that ped #", iPed, " has been faded out. PlayerId = ", iPlayer)
									SET_PED_BIT(iPed, ePEDBITSET_FADED_OUT)
								ENDIF
							ENDIF
							
							IF NOT IS_PED_BIT_SET(iPed, ePEDBITSET_I_HAVE_WALKED_INTO_NIGHTCLUB)
								IF IS_PED_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iPed, ePEDCLIENTBITSET_I_HAVE_WALKED_INTO_NIGHTCLUB)
									PRINTLN("[MB_MISSION] participant ", iParticipant, " says that ped #", iPed, " has been faded out. PlayerId = ", iPlayer)
									SET_PED_BIT(iPed, ePEDBITSET_I_HAVE_WALKED_INTO_NIGHTCLUB)
								ENDIF
							ENDIF
							
							IF NOT IS_PED_BIT_SET(iPed, ePEDBITSET_TAKEN_LETHAL_DAMAGE)
								IF IS_PED_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iPed, ePEDCLIENTBITSET_TAKEN_LETHAL_DAMAGE)
									PRINTLN("[MB_MISSION] participant ", iParticipant, " says that ped #", iPed, " has taken lethal damage. PlayerId = ", iPlayer)
									SET_PED_BIT(iPed, ePEDBITSET_TAKEN_LETHAL_DAMAGE)
									serverBD.iNumPedsTakenLethalDamage++
									IF serverBD.iNumPedsTakenLethalDamage >= NUM_PEDS_KILLED_TO_FAIL()
										SET_SERVER_BIT(eSERVERBITSET_TOO_MANY_PEDS_KILLED)
									ENDIF
								ENDIF
							ENDIF
							
							IF NOT IS_PED_BIT_SET(iPed, ePEDBITSET_HEALTH_BOOSTED_FOR_COMBAT)
								IF IS_PED_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iPed, ePEDCLIENTBITSET_HEALTH_BOOSTED_FOR_COMBAT)
									PRINTLN("[MB_MISSION] participant ", iParticipant, " says that ped #", iPed, " has had their health boosted for combat. PlayerId = ", iPlayer)
									SET_PED_BIT(iPed, ePEDBITSET_HEALTH_BOOSTED_FOR_COMBAT)
								ENDIF
							ENDIF
							
							IF NOT IS_PED_BIT_SET(iPed, ePEDBITSET_UNFROZEN)
								IF IS_PED_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iPed, ePEDCLIENTBITSET_UNFROZEN)
									PRINTLN("[MB_MISSION] participant ", iParticipant, " says that ped #", iPed, " has been unfrozen. PlayerId = ", iPlayer)
									SET_PED_BIT(iPed, ePEDBITSET_UNFROZEN)
								ENDIF
							ENDIF
							
							IF NOT IS_PED_BIT_SET(iPed, ePEDBITSET_KILLED_WITH_MAGIC_BULLET)
								IF IS_PED_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iPed, ePEDCLIENTBITSET_KILLED_WITH_MAGIC_BULLET)
									PRINTLN("[MB_MISSION] participant ", iParticipant, " says that ped #", iPed, " has been killed with a magic bullet. PlayerId = ", iPlayer)
									SET_PED_BIT(iPed, ePEDBITSET_KILLED_WITH_MAGIC_BULLET)
								ENDIF
							ENDIF
							
							IF NOT IS_PED_BIT_SET(iPed, ePEDBITSET_DAVE_TASKED_WITH_ENTERING)
								IF IS_PED_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iPed, ePEDCLIENTBITSET_DAVE_TASKED_WITH_ENTERING)
									PRINTLN("[MB_MISSION] participant ", iParticipant, " says that ped #", iPed, " has tasked dave with entering the bob. PlayerId = ", iPlayer)
									SET_PED_BIT(iPed, ePEDBITSET_DAVE_TASKED_WITH_ENTERING)
								ENDIF
							ENDIF
							
							IF NOT IS_PED_BIT_SET(iPed, ePEDBITSET_I_HAVE_BEEN_LOST)
								IF IS_PED_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, iPed, ePEDCLIENTBITSET_I_HAVE_BEEN_LOST)
									PRINTLN("[MB_MISSION] participant ", iParticipant, " says that ped #", iPed, " has been lost as a pursuer. PlayerId = ", iPlayer)
									SET_PED_BIT(iPed, ePEDBITSET_I_HAVE_BEEN_LOST)
								ENDIF
							ENDIF
							
							INT iSpeech
							REPEAT ENUM_TO_INT(eSPEECH_NOT_USED) iSpeech 
								IF NOT HAS_SPEECH_BEEN_DONE(iPed, INT_TO_ENUM(SPEECH_ENUM, iSpeech))
									IF HAS_SPEECH_BEEN_DONE_CLIENT(sParticipant[iParticipant].participantId, iPed, INT_TO_ENUM(SPEECH_ENUM, iSpeech))
										PRINTLN("[MB_MISSION] participant ", iParticipant, " says that ped #", iPed, " has done speech ", SPEECH_ENUM_DEBUG(INT_TO_ENUM(SPEECH_ENUM, iSpeech)),". PlayerId = ", iPlayer)
										SET_SPEECH_AS_DONE(iPed, INT_TO_ENUM(SPEECH_ENUM, iSpeech))
									ENDIF
								ENDIF
							ENDREPEAT
							
						ENDIF
					ENDREPEAT		
					
					INT iGroup
					REPEAT MAX_NUM_PED_GROUPS iGroup
						IF NOT GET_GROUP_SHOULD_REACT(iGroup)
							IF IS_BIT_SET(playerBD[NATIVE_TO_INT(sParticipant[iParticipant].participantId)].iGroupReactBS, iGroup)
								PRINTLN("[MB_MISSION] participant ", iParticipant, " wants all the group ",iGroup," peds to react. PlayerId = ", iPlayer)
								SET_GROUP_SHOULD_REACT(iGroup)
							ENDIF
						ENDIF
					ENDREPEAT
					
					IF serverBD.iCurrentCheckpoint < 32
						IF NOT IS_CHECKPOINT_BIT_SET(serverBD.iCurrentCheckpoint)
							IF IS_CHECKPOINT_CLIENT_BIT_SET(sParticipant[iParticipant].participantId, serverBD.iCurrentCheckpoint)
								SET_CHECKPOINT_BIT(serverBD.iCurrentCheckpoint)
								serverBD.iCurrentCheckpoint++
								
								IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_FINAL_CHECKPOINT_HAS_BEEN_REACHED)
								AND IS_CHECKPOINT_BIT_SET(GET_FINAL_CHECKPOINT())
									SET_SERVER_BIT(eSERVERBITSET_FINAL_CHECKPOINT_HAS_BEEN_REACHED)
								ENDIF
								PRINTLN("[MB_MISSION] participant ", iParticipant, " says that checkpoint ", (serverBD.iCurrentCheckpoint - 1)," has been cleared, current checkpoint is now ", serverBD.iCurrentCheckpoint,". PlayerId = ", iPlayer)
							ENDIF
						ENDIF
					ENDIF
					
					#IF IS_DEBUG_BUILD
					IF serverBD.iSPassPart = (-1)
					AND serverBD.iFFailPart = (-1)
						IF IS_CLIENT_DEBUG_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTDEBUGBITSET_I_HAVE_S_PASSED)
							serverBD.iSPassPart = iParticipant
							PRINTLN("[MB_MISSION] participant ", iParticipant, " has S passed. Setting serverBD.iSPassPart = ", iParticipant)
						ELIF IS_CLIENT_DEBUG_BIT_SET(sParticipant[iParticipant].participantId, eCLIENTDEBUGBITSET_I_HAVE_F_FAILED)
							serverBD.iFFailPart = iParticipant
							PRINTLN("[MB_MISSION] participant ", iParticipant, " has F failed. Setting serverBD.iFFailPart = ", iParticipant)
						ENDIF
					ENDIF
					#ENDIF
					IF IS_CLIENT_BIT_SET(sParticipant[iParticipant].participantId,eCLIENTBITSET_I_AM_IN_THE_CUTSCENE_VEHICLE)
					AND IS_SERVER_BIT_SET(eSERVERBITSET_READY_FOR_CUTSCENE)
					AND NOT IS_THIS_PLAYER_IN_CORONA(NETWORK_GET_PLAYER_INDEX(sParticipant[iParticipant].participantId))
						PRINTLN("[MB_MISSION] participant ", iParticipant, " eCLIENTBITSET_I_AM_IN_THE_CUTSCENE_VEHICLE")
						MAINTAIN_DELIVERY_CUTSCENE_PARTICIPANT_LOOP(playerBD[iParticipant].sDeliveryCutscene,serverBD.sDeliveryCutscene)
					ENDIF	
				ENDIF
				
			ENDIF
			
		ENDIF
		
	ENDREPEAT
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
	
		IF serverBD.iHacksComplete != iHacksComplete
		
			ENABLE_DROP_OFFS_AFTER_HACK(serverBD.iHacksComplete)
			serverBD.iHacksComplete = iHacksComplete
			
			INT iLatestHackTime = -1
			INT iHackPart
			REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant))
					
					IF playerBD[iParticipant].iTimeHacked > iLatestHackTime
						iLatestHackTime = playerBD[iParticipant].iTimeHacked
						iHackPart = iParticipant
					ENDIF
				ENDIF
			ENDREPEAT
						
			serverBD.iHackParticipant = iHackPart
			PRINTLN(" 4969319 serverBD.iHackParticipant = ", serverBD.iHackParticipant)

			#IF IS_DEBUG_BUILD
			PRINTLN("[MB_MISSION] [", GET_CURRENT_VARIATION_STRING(),"] - PROCESS_PARTICIPANT_LOOP - 		serverBD.iHacksComplete 	= ", serverBD.iHacksComplete)
			IF serverBD.iHacksComplete > serverBD.iTotalDeliveriesToMake
				SCRIPT_ASSERT("PROCESS_PARTICIPANT_LOOP (serverBD.iHacksComplete > serverBD.iTotalDeliveriesToMake), bug for Chris Speirs please ")
			ENDIF
			#ENDIF
		ENDIF
		
		IF SHOULD_USE_SERVER_TOTAL_SCORE()
			IF bIncreasingScore
				fTotalScore = FMIN(fTotalScore+1,GET_TOTAL_SCORE_TO_REACH())
			ELSE
				fTotalScore = FMAX(fTotalScore-1,0.0)
			ENDIF
		ENDIF
		
		IF serverBD.fTotalScore != fTotalScore
			IF HAS_NET_TIMER_EXPIRED(serverBD.scoreUpdateTimer, ciBD_SCORE_UPDATE_DELAY)
				serverBD.fTotalScore = fTotalScore
				RESET_NET_TIMER(serverBD.scoreUpdateTimer)
				PRINTLN("[MB_MISSION] PROCESS_PARTICIPANT_LOOP - serverBD.iTotalScore = ",serverBD.fTotalScore)
				
				IF serverBD.fTotalScore >= GET_TOTAL_SCORE_TO_REACH()
					SET_SERVER_BIT(eSERVERBITSET_TOTAL_SCORE_REACHED)
				ENDIF
			ENDIF
		ENDIF
		
		IF bAllCompletedRewards
			IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_ALL_PARTICIPANTS_COMPLETED_REWARDS)
				SET_SERVER_BIT(eSERVERBITSET_ALL_PARTICIPANTS_COMPLETED_REWARDS)
			ENDIF
		ELSE
			IF IS_SERVER_BIT_SET(eSERVERBITSET_ALL_PARTICIPANTS_COMPLETED_REWARDS)
				CLEAR_SERVER_BIT(eSERVERBITSET_ALL_PARTICIPANTS_COMPLETED_REWARDS)
			ENDIF
		ENDIF
		
		IF bMidMissionMocapComplete
			IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_MID_MISSION_MOCAP_DONE)
				SET_SERVER_BIT(eSERVERBITSET_MID_MISSION_MOCAP_DONE)
			ENDIF
		ELSE
			IF IS_SERVER_BIT_SET(eSERVERBITSET_MID_MISSION_MOCAP_DONE)
				CLEAR_SERVER_BIT(eSERVERBITSET_MID_MISSION_MOCAP_DONE)
			ENDIF
		ENDIF
		
		IF bMidMoCapStarted
			IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_MID_MISSION_MOCAP_STARTED_FOR_ALL)
				SET_SERVER_BIT(eSERVERBITSET_MID_MISSION_MOCAP_STARTED_FOR_ALL)
			ENDIF
		ENDIF
		
		
		IF bScriptedCutsceneComplete
			IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_SCRIPTED_CUTSCENE_DONE)
				SET_SERVER_BIT(eSERVERBITSET_SCRIPTED_CUTSCENE_DONE)
			ENDIF
		ELSE
			IF IS_SERVER_BIT_SET(eSERVERBITSET_SCRIPTED_CUTSCENE_DONE)
				CLEAR_SERVER_BIT(eSERVERBITSET_SCRIPTED_CUTSCENE_DONE)
			ENDIF
		ENDIF
		
		IF bAllLeftArea
			IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_ALL_PLAYERS_LEFT_DELIVERY_AREA_EOM)
				SET_SERVER_BIT(eSERVERBITSET_ALL_PLAYERS_LEFT_DELIVERY_AREA_EOM)
			ENDIF
		ENDIF

		IF bAllDeliveryComplete
			IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_MISSION_EOM_DELIVERY_CUTSCENE_COMPLETE)
				SET_SERVER_BIT(eSERVERBITSET_MISSION_EOM_DELIVERY_CUTSCENE_COMPLETE)
			ENDIF
		ENDIF	

		IF bAllCutsceneComplete
			IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_CUTSCENE_FINISHED)
				SET_SERVER_BIT(eSERVERBITSET_CUTSCENE_FINISHED)
			ENDIF
		ENDIF
		
		IF bReadyForCutscene
			IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_READY_FOR_CUTSCENE)
				SET_SERVER_BIT(eSERVERBITSET_READY_FOR_CUTSCENE)
			ENDIF
		ENDIF
		
		IF bAllWarpedIntoNightclub
			IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_ALL_WARPED_INTO_NIGHTCLUB)
				SET_SERVER_BIT(eSERVERBITSET_ALL_WARPED_INTO_NIGHTCLUB)
			ENDIF
		ENDIF
		
		IF bNoneDefendingBuyer
			IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_BUYER_UNDEFENDED)
				SET_SERVER_BIT(eSERVERBITSET_BUYER_UNDEFENDED)
			ENDIF
		ELSE
			IF IS_SERVER_BIT_SET(eSERVERBITSET_BUYER_UNDEFENDED)
				CLEAR_SERVER_BIT(eSERVERBITSET_BUYER_UNDEFENDED)
			ENDIF
		ENDIF
		
		IF bSomeoneDoingPosterAnims
			SET_SERVER_BIT(eSERVERBITSET_SOMEONE_DOING_POSTER_ANIMATION)
		ELSE
			CLEAR_SERVER_BIT(eSERVERBITSET_SOMEONE_DOING_POSTER_ANIMATION)
		ENDIF
		
		IF GET_END_REASON() = eENDREASON_NO_REASON_YET
		
			IF bNoBossOnScript
				SET_END_REASON(eENDREASON_NO_BOSS_LEFT)
				SET_MODE_STATE(eMODESTATE_REWARDS)
			ELIF iNumGoonsOnScript = 0
//				IF HAS_NET_TIMER_STARTED(serverBD.modeTimer)
//					IF HAS_NET_TIMER_EXPIRED(serverBD.modeTimer, TRACK_GOON_DELAY) // Wait for little bit before ending script for lack of goons, to give them a chance to join the script before it terminates for them not being on it. 
//						SET_END_REASON(eENDREASON_ALL_GOONS_LEFT)
//					ENDIF
//				ENDIF
			ENDIF
			
		ENDIF
		
	ENDIF
	
ENDPROC

#IF IS_DEBUG_BUILD

TEXT_WIDGET_ID twIdServerGameState
TEXT_WIDGET_ID twIdClientGameState[GB_MAX_GANG_SIZE_INCLUDING_BOSS]
TEXT_WIDGET_ID twIdClientName[GB_MAX_GANG_SIZE_INCLUDING_BOSS]
TEXT_WIDGET_ID twIdModeState
BOOL bTerminateScriptNow

PROC UPDATE_SERVER_GAME_STATE_WIDGET()
	SET_CONTENTS_OF_TEXT_WIDGET(twIdServerGameState, GET_GAME_STATE_NAME(GET_SERVER_GAME_STATE()))
	SET_CONTENTS_OF_TEXT_WIDGET(twIdModeState, GET_MODE_STATE_NAME(GET_MODE_STATE()))
ENDPROC

PROC UPDATE_CLIENT_GAME_STATE_WIDGET(INT iParticipant)
	
	STRING strName
	
	SET_CONTENTS_OF_TEXT_WIDGET(twIdClientGameState[iParticipant], "NOT ACTIVE")
	SET_CONTENTS_OF_TEXT_WIDGET(twIdClientName[iParticipant], "NOT ACTIVE")
	
	IF IS_BIT_SET(iParticipantActiveBitset, iParticipant)
		IF IS_BIT_SET(iPlayerOkBitset, sParticipant[iParticipant].iPlayerId)
			SET_CONTENTS_OF_TEXT_WIDGET(twIdClientGameState[iParticipant], GET_GAME_STATE_NAME(GET_CLIENT_GAME_STATE(iParticipant)))
			strName = GET_PLAYER_NAME(INT_TO_NATIVE(PLAYER_INDEX, sParticipant[iParticipant].iPlayerId))
			SET_CONTENTS_OF_TEXT_WIDGET(twIdClientName[iParticipant], strName)
		ENDIF
	ENDIF
	
ENDPROC

BOOL bRequestToBeHost
BOOL bShowEntityArrayIndexes
SCRIPT_TIMER entityIndexDebugTimer

BOOL bDebugFireFlareFromLocalHeli
FLOAT fDebugFireToSpeed = 10.0

BOOL bDebugDamageScale
FLOAT fDebugDamageScale = 1.0

CONST_INT DEBUG_NUM_PED_SPEECH	15
INT iDebugPedForSpeech = 0
BOOL bDebugPedSpeechToPlay[DEBUG_NUM_PED_SPEECH]

PROC CREATE_WIDGETS()
	
	INT iParticipant
	TEXT_LABEL_63 tl63Temp
	
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_ShowMissionEntityArrayIndexes")
		bShowEntityArrayIndexes = TRUE
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	ENDIF
	
	START_WIDGET_GROUP("BattlesSells")
		START_WIDGET_GROUP("Amanda")
			ADD_WIDGET_INT_READ_ONLY("amandaData.iArrestScene", amandaData.iArrestScene)
			ADD_WIDGET_FLOAT_READ_ONLY("fDistBetweenCargoboAndCopCar", fDistBetweenCargoboAndCopCar)
		STOP_WIDGET_GROUP()		
		START_WIDGET_GROUP("Poster")
			ADD_WIDGET_INT_READ_ONLY("sLocalPosterData.iPosterProgress", sLocalPosterData.iPosterProgress)
		STOP_WIDGET_GROUP()	
		START_WIDGET_GROUP("Collector")
			ADD_WIDGET_BOOL("Disconnect Trailer", bDisconnectCollector)
		STOP_WIDGET_GROUP()	
		
		START_WIDGET_GROUP("GPS")
			ADD_WIDGET_INT_SLIDER("iIgnoreGpsFlag", iIgnoreGpsFlag, -1, 1, 1)
		STOP_WIDGET_GROUP()

		ADD_WIDGET_BOOL("Entity Spawn debug", g_SpawnData.bShowAdvancedSpew)
		ADD_WIDGET_BOOL("bRevealDrop", bRevealDrop)
		ADD_WIDGET_BOOL("Locally terminate Script Now", bTerminateScriptNow)
		ADD_WIDGET_BOOL("Fake End Of Mode Timer", serverBd.bFakeEndOfModeTimer)
		ADD_WIDGET_INT_READ_ONLY("iMyNearestPoster", iMyNearestPoster)
		ADD_WIDGET_INT_READ_ONLY("iHacksComplete", serverBD.iHacksComplete)
		ADD_WIDGET_INT_READ_ONLY("serverBD.iCurrentGotoLocation", serverBD.iCurrentGotoLocation)
		ADD_WIDGET_INT_READ_ONLY("serverBD.iCurrentItemsCollected", serverBD.iCurrentItemsCollected)
		ADD_WIDGET_INT_READ_ONLY("serverBD.iClosestCarWash", serverBD.iClosestCarWash)
		ADD_WIDGET_INT_READ_ONLY("serverBD.iPedStaffBeingCollected", serverBD.iPedStaffBeingCollected)
		ADD_WIDGET_FLOAT_READ_ONLY("fMyDistanceFromNearestDropOff", fMyDistanceFromNearestDropOff)
		ADD_WIDGET_FLOAT_SLIDER("fDebugGoto", fDebugGoto, 0.0, 10000, 1.0)
		ADD_WIDGET_BOOL("Request to be Host", bRequestToBeHost)
		START_WIDGET_GROUP("Hack")
			ADD_WIDGET_INT_READ_ONLY("iHackingProg", iHackingProg)
		STOP_WIDGET_GROUP()		
		
		START_WIDGET_GROUP("Peds")
			ADD_WIDGET_INT_READ_ONLY("serverBD.iNumDeadPeds", serverBD.iNumDeadPeds)
			ADD_WIDGET_INT_READ_ONLY("serverBD.iNumPedsIncapacitated", serverBD.iNumPedsIncapacitated)
			ADD_WIDGET_INT_READ_ONLY("serverBD.iNumPedsTakenLethalDamage", serverBD.iNumPedsTakenLethalDamage)
			ADD_WIDGET_INT_READ_ONLY("serverBD.iItemHolder", serverBD.iItemHolder)
			ADD_WIDGET_INT_READ_ONLY("serverBD.iPedStripperModel", serverBD.iPedStripperModel)
		STOP_WIDGET_GROUP()	
		
		ADD_WIDGET_INT_READ_ONLY("serverBD.iTotalDeliveredCount", serverBD.iTotalDeliveredCount)
		ADD_WIDGET_INT_READ_ONLY("serverBD.iCachedDeliveries", serverBD.iCachedDeliveries)
		ADD_WIDGET_INT_READ_ONLY("iMyCurrentDropOff", iMyCurrentDropOff)
		
		START_WIDGET_GROUP("Score")
		ADD_WIDGET_FLOAT_READ_ONLY("serverBD.fTotalScore", serverBD.fTotalScore)
		ADD_WIDGET_FLOAT_READ_ONLY("sScore.fLocalScore", sScore.fLocalScore)
		STOP_WIDGET_GROUP()	
		START_WIDGET_GROUP("Entity Array Indexes")
			ADD_WIDGET_STRING("'Script/Draw Debug Lines And Spheres' also needs set to display entity indexes.")
			ADD_WIDGET_BOOL("Draw Entity Array Indexes", bShowEntityArrayIndexes)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Flare Testing")
			ADD_WIDGET_BOOL("Fire", bDebugFireFlareFromLocalHeli)
			ADD_WIDGET_FLOAT_SLIDER("Speed", fDebugFireToSpeed, -1.0, 100.0, 1.0)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Damage Scale Testing")
			ADD_WIDGET_BOOL("Set", bDebugDamageScale)
			ADD_WIDGET_FLOAT_SLIDER("Scale", fDebugDamageScale, 0.0, 1.0, 0.01)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Ped Speech")
			ADD_WIDGET_INT_SLIDER("Ped for speech", iDebugPedForSpeech, 0, NUM_MISSION_PEDS, 1)
			ADD_WIDGET_BOOL("GENERIC_HI", 						bDebugPedSpeechToPlay[0])
			ADD_WIDGET_BOOL("GENERIC_HOWS_IT_GOING", 			bDebugPedSpeechToPlay[1])
			ADD_WIDGET_BOOL("OVER_THERE", 						bDebugPedSpeechToPlay[2])
			ADD_WIDGET_BOOL("JACK_VEHICLE_BACK", 				bDebugPedSpeechToPlay[3])
			ADD_WIDGET_BOOL("FIGHT_RUN", 						bDebugPedSpeechToPlay[4])
			ADD_WIDGET_BOOL("FIGHT", 							bDebugPedSpeechToPlay[5])
			ADD_WIDGET_BOOL("CHALLENGE_THREATEN", 				bDebugPedSpeechToPlay[6])
			ADD_WIDGET_BOOL("GENERIC_SHOCKED_HIGH", 			bDebugPedSpeechToPlay[7])
			ADD_WIDGET_BOOL("GENERIC_SHOCKED_MED", 				bDebugPedSpeechToPlay[8])
			ADD_WIDGET_BOOL("GENERIC_CURSE_HIGH", 				bDebugPedSpeechToPlay[9])
			ADD_WIDGET_BOOL("GENERIC_CURSE_MED", 				bDebugPedSpeechToPlay[10])
			ADD_WIDGET_BOOL("WON_DISPUTE", 						bDebugPedSpeechToPlay[11])
			ADD_WIDGET_BOOL("GENERIC_BUY", 						bDebugPedSpeechToPlay[12])
			ADD_WIDGET_BOOL("GENERIC_THANKS", 					bDebugPedSpeechToPlay[13])
			ADD_WIDGET_BOOL("GENERIC_BYE", 						bDebugPedSpeechToPlay[14])
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Game State")
			START_WIDGET_GROUP("Server")
				twIdServerGameState = ADD_TEXT_WIDGET("Game State")
				twIdModeState = ADD_TEXT_WIDGET("Mode State")
				UPDATE_SERVER_GAME_STATE_WIDGET()
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Client")
				REPEAT GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS() iParticipant
					tl63Temp = ""
					tl63Temp +="Part "
					tl63Temp += iParticipant
					tl63Temp += " Name"
					twIdClientName[iParticipant] = ADD_TEXT_WIDGET(tl63Temp)
					tl63Temp = ""
					tl63Temp += "Part "
					tl63Temp += iParticipant
					tl63Temp += " Game State"
					twIdClientGameState[iParticipant] = ADD_TEXT_WIDGET(tl63Temp)
					tl63Temp = ""
					tl63Temp +="Part "
					tl63Temp += iParticipant
					tl63Temp += " Player ID"
					ADD_WIDGET_INT_READ_ONLY(tl63Temp, sParticipant[iParticipant].iPlayerId)
					tl63Temp = ""
					tl63Temp +="Part "
					tl63Temp += iParticipant
					tl63Temp += " Player stored Part ID"
					ADD_WIDGET_INT_READ_ONLY(tl63Temp, sPlayer[sParticipant[iParticipant].iPlayerId].iParticipantId)
					UPDATE_CLIENT_GAME_STATE_WIDGET(iParticipant)
				ENDREPEAT
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	
	PRINTLN("[MB_MISSION] - created widgets.")
	
ENDPROC	

PROC DRAW_DEBUG_TEXT_ABOVE_NETID(NETWORK_INDEX netID, TEXT_LABEL_63& txtLbl, INT iEntity, BOOL bForPed = FALSE, FLOAT fOffset = 0.0)
	ENTITY_INDEX entityId = NET_TO_ENT(netID)
	VECTOR vEntityCoords
	
	IF bForPed
		vEntityCoords = GET_PED_BONE_COORDS(NET_TO_PED(netID), BONETAG_ROOT, <<0.0, 0.0, 0.0>>)
		vEntityCoords += <<0.0, 0.0, 1.0>>
	ELSE
		vEntityCoords = GET_ENTITY_COORDS(entityId, FALSE)
		vEntityCoords += <<0.0, 0.0, 1.0>>
	ENDIF
	
	IF fOffset != 0.0
		vEntityCoords += <<0.0, 0.0, fOffset>>
	ENDIF
	
	txtLbl += iEntity
	
	DRAW_DEBUG_TEXT(txtLbl, vEntityCoords, 255, 0, 0)
ENDPROC

FUNC STRING GET_SPEECH(INT iSpeech)
	SWITCH iSpeech
		CASE 0		RETURN "GENERIC_HI"
		CASE 1		RETURN "GENERIC_HOWS_IT_GOING"
		CASE 2		RETURN "OVER_THERE"
		CASE 3		RETURN "JACK_VEHICLE_BACK"
		CASE 4		RETURN "FIGHT_RUN"
		CASE 5		RETURN "FIGHT"
		CASE 6		RETURN "CHALLENGE_THREATEN"
		CASE 7		RETURN "GENERIC_SHOCKED_HIGH"
		CASE 8		RETURN "GENERIC_SHOCKED_MED"
		CASE 9		RETURN "GENERIC_CURSE_HIGH"
		CASE 10		RETURN "GENERIC_CURSE_MED"
		CASE 11		RETURN "WON_DISPUTE"
		CASE 12		RETURN "GENERIC_BUY"
		CASE 13		RETURN "GENERIC_THANKS"
		CASE 14		RETURN "GENERIC_BYE"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

PROC UPDATE_WIDGETS()
	
	INT iPartcount
	
	UPDATE_SERVER_GAME_STATE_WIDGET()
	
	REPEAT GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS() iPartcount
		UPDATE_CLIENT_GAME_STATE_WIDGET(iPartcount)
	ENDREPEAT
	
	IF IS_DEBUG_KEY_JUST_RELEASED(KEY_R, KEYBOARD_MODIFIER_CTRL, "Draw mission entity array indexes on screen")
		IF bShowEntityArrayIndexes
			bShowEntityArrayIndexes = FALSE
		ELSE
			bShowEntityArrayIndexes = TRUE
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE) 
		ENDIF
	ENDIF
	
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_SPACE, KEYBOARD_MODIFIER_NONE, "Draw mission entity array indexes on screen for F9")
	AND NOT bShowEntityArrayIndexes
		bShowEntityArrayIndexes = TRUE
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
		START_NET_TIMER(entityIndexDebugTimer)
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(entityIndexDebugTimer)
	AND HAS_NET_TIMER_EXPIRED(entityIndexDebugTimer, 2000)
		bShowEntityArrayIndexes = FALSE
		RESET_NET_TIMER(entityIndexDebugTimer)
	ENDIF
	
	IF bDisconnectCollector
	OR IS_DEBUG_KEY_JUST_RELEASED(KEY_D, KEYBOARD_MODIFIER_CTRL_SHIFT, "Disconnect Collector")
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[COLLECTOR_TRAILER].netId)
			DISCONNECT_TRAILER_FROM_ANY_VEHICLE(NET_TO_VEH(serverBD.sVehicle[COLLECTOR_TRAILER].netId))
			PRINTLN("[MB_MISSION] Disconnected collector trailer.")
		ENDIF
		bDisconnectCollector = FALSE
	ENDIF
	
	IF NOT IS_LOCAL_DEBUG_BIT_SET(eLOCALDEBUGBITSET_TERMINATE_SCRIPT_NOW)
		IF bTerminateScriptNow
			PRINTLN("[MB_MISSION] widget bTerminateScriptNow = TRUE.")
			SET_LOCAL_DEBUG_BIT(eLOCALDEBUGBITSET_TERMINATE_SCRIPT_NOW)
		ENDIF
	ENDIF
	
	INT iEntity
	TEXT_LABEL_63 tl63Temp
	
	IF bRequestToBeHost
		NETWORK_REQUEST_TO_BE_HOST_OF_THIS_SCRIPT()
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			bRequestToBeHost = FALSE
		ENDIF
	ENDIF
	
	IF bShowEntityArrayIndexes

		REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iEntity
//			IF MissionEntityHolderPlayerID[iEntity] != INVALID_PLAYER_INDEX()
//				IF IS_MISSION_ENTITY_BIT_SET(iEntity, eMISSIONENTITYBITSET_I_AM_OCCUPIED)
//					tl63Temp = "(Held, In Carrier) mission entity #"
//				ELSE
//					tl63Temp = "(Held) mission entity #"
//				ENDIF
			IF IS_MISSION_ENTITY_BIT_SET(iEntity, eMISSIONENTITYBITSET_I_AM_OCCUPIED)
				tl63Temp = "(In Carrier) mission entity #"
			ELSE
				tl63Temp = "mission entity #"
			ENDIF
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[iEntity].netId)
				DRAW_DEBUG_TEXT_ABOVE_NETID(serverBD.sMissionEntity[iEntity].netId, tl63Temp, iEntity, FALSE, 0.2)
			ENDIF
		ENDREPEAT
		
		REPEAT NUM_MISSION_PEDS iEntity
//			IF IS_PED_AN_AMBUSH_PED(iEntity)
//				tl63Temp = "(Ambush) Ped #"
//			ELIF IS_PED_A_STATIONARY_AMBUSH_PED(iEntity)
//				tl63Temp = "(StatAmb) Ped #"
//			ELSE
				tl63Temp = "Ped #"
//			ENDIF
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[iEntity].netId)
				DRAW_DEBUG_TEXT_ABOVE_NETID(serverBD.sPed[iEntity].netId, tl63Temp, iEntity, TRUE)
			ENDIF
		ENDREPEAT
		
		REPEAT NUM_MISSION_VEHICLES iEntity
//			IF IS_VEHICLE_AN_AMBUSH_VEHICLE(iEntity)
//				tl63Temp = "(Ambush) Vehicle #"
//			ELIF IS_VEHICLE_A_SUPPORT_VEHICLE(iEntity)
//				tl63Temp = "(Support) Vehicle #"
//			ELIF IS_VEHICLE_A_STATIONARY_AMBUSH_VEHICLE(iEntity)
//				tl63Temp = "(StatAmb) Vehicle #"
//			ELIF IS_VEHICLE_A_CARRIER_VEHICLE(iEntity)
//				tl63Temp = "(Carrier) Vehicle #"
//			ELSE
				tl63Temp = "Vehicle #"
//			ENDIF
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iEntity].netId)
				DRAW_DEBUG_TEXT_ABOVE_NETID(serverBD.sVehicle[iEntity].netId, tl63Temp, iEntity, FALSE, -0.2)
			ENDIF
		ENDREPEAT
		
		REPEAT NUM_MISSION_PROPS iEntity
			tl63Temp = "Prop #"
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niProps[iEntity])
				DRAW_DEBUG_TEXT_ABOVE_NETID(serverBD.niProps[iEntity], tl63Temp, iEntity)
			ENDIF
		ENDREPEAT
	ENDIF
	
	IF bDebugFireFlareFromLocalHeli
	
//		SHOOT_SINGLE_BULLET_BETWEEN_COORDS(<<2610.5713, 4586.0698, 63.3994>>, <<2610.5713, 4586.0698, 33.3994>>, 0, FALSE, WEAPONTYPE_DLC_FLAREGUN, DEFAULT, DEFAULT, DEFAULT, fDebugFireToSpeed)
	
		SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_NCLUBT, "FMBB_TXT_1", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
	
		bDebugFireFlareFromLocalHeli = FALSE
	ENDIF
		
	IF bDebugDamageScale
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
			IF IS_ENTITY_ALIVE(NET_TO_VEH(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId))
				IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId)
					SET_VEHICLE_DAMAGE_SCALE(NET_TO_VEH(serverBD.sMissionEntity[MISSION_ENTITY_ONE].netId), fDebugDamageScale)
					PRINTLN("[MB_MISSION] - DAMAGE_SCALE - new value set on vehicle: ", fDebugDamageScale)
				ENDIF
			ENDIF
		ENDIF
	
		bDebugDamageScale = FALSE
	ENDIF
	
	
	INT iSpeech
	REPEAT DEBUG_NUM_PED_SPEECH iSpeech
		IF bDebugPedSpeechToPlay[iSpeech]
			IF iDebugPedForSpeech != -1
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[iDebugPedForSpeech].netId)
				AND IS_ENTITY_ALIVE(NET_TO_PED(serverBD.sPed[iDebugPedForSpeech].netId))
					PLAY_PED_AMBIENT_SPEECH(NET_TO_PED(serverBD.sPed[iDebugPedForSpeech].netId), GET_SPEECH(iSpeech), SPEECH_PARAMS_INTERRUPT | SPEECH_PARAMS_FORCE)
					bDebugPedSpeechToPlay[iSpeech] = FALSE
					PRINTLN("[MB_MISSION] - DEBUG - Ped ", iDebugPedForSpeech, " has triggered ambient speech ", iSpeech, " (", GET_SPEECH(iSpeech), ")")
				
				ELSE
					bDebugPedSpeechToPlay[iSpeech] = FALSE
					PRINTLN("[MB_MISSION] - DEBUG - Ambient speech ", iSpeech, " (", GET_SPEECH(iSpeech), ") requested but ped ", iDebugPedForSpeech, " doesnt exist")
				ENDIF
			ELSE
				bDebugPedSpeechToPlay[iSpeech] = FALSE
				PRINTLN("[MB_MISSION] - DEBUG - Ambient speech ", iSpeech, " (", GET_SPEECH(iSpeech), ") requested but ped = ", iDebugPedForSpeech)
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

#ENDIF



PROC UPDATE_FOCUS_PARTICIPANT()

	// Save out the current focus participant. Stays at -1 if not the local player or not spectating a script participant.
	iFocusParticipant = (-1)
	
	IF IS_BIT_SET(iPlayerOkBitset, NATIVE_TO_INT(localPlayerId))
		IF IS_BIT_SET(iParticipantActiveBitset, PARTICIPANT_ID_TO_INT())
			IF NOT IS_PLAYER_SPECTATING(localPlayerId)
				iFocusParticipant = PARTICIPANT_ID_TO_INT()
			ELSE
				IF IS_A_SPECTATOR_CAM_RUNNING()
				AND NOT IS_SPECTATOR_HUD_HIDDEN()
					PED_INDEX specTargetPed = GET_SPECTATOR_SELECTED_PED()
					IF IS_PED_A_PLAYER(specTargetPed)
						PLAYER_INDEX specPlayerTemp = NETWORK_GET_PLAYER_INDEX_FROM_PED(specTargetPed)
						IF NETWORK_IS_PLAYER_A_PARTICIPANT(specPlayerTemp)
							PARTICIPANT_INDEX specTargetParticipant = NETWORK_GET_PARTICIPANT_INDEX(specPlayerTemp)
							iFocusParticipant = NATIVE_TO_INT(specTargetParticipant)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL DOES_BUSINESS_HUB_HAVE_ENOUGH_GOODS_FOR_INITIAL_REMOVAL()

	IF GlobalplayerBD_FM[NETWORK_PLAYER_ID_TO_INT()].sHubSellMissionData.iGoodsTotal > g_sMPTunables.iBB_SELL_MISSIONS_SMALL_SELL_SIZE_INITIAL_GOODS_DEDUCTION
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HANDLE_INITIAL_PRODUCT_REMOVAL()

	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_SkipInitialProductRemoval")
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - HANDLE_INITIAL_PRODUCT_REMOVAL - sc_SkipInitialProductRemoval - Return TRUE")
		RETURN TRUE
	ENDIF
	#ENDIF

	BOOL bOkayToContinue = FALSE

	IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
		IF DOES_BUSINESS_HUB_HAVE_ENOUGH_GOODS_FOR_INITIAL_REMOVAL()

			IF REMOVE_PRODUCT_FROM_BUSINESS_HUB_USING_SELL_MISSION_DATA(GlobalplayerBD_FM[NETWORK_PLAYER_ID_TO_INT()].sHubSellMissionData, REMOVE_CONTRA_MISSION_STARTED, eResult, TRUE, 0)
				
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - HANDLE_INITIAL_PRODUCT_REMOVAL - REMOVE_PRODUCT_FROM_BUSINESS_HUB_USING_SELL_MISSION_DATA")
				
				IF eResult = CONTRABAND_TRANSACTION_STATE_SUCCESS
				OR eResult = CONTRABAND_TRANSACTION_STATE_FAILED

					IF eResult = CONTRABAND_TRANSACTION_STATE_SUCCESS
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - HANDLE_INITIAL_PRODUCT_REMOVAL - Transaction success! Removed product intially from business hub")
					ELIF eResult = CONTRABAND_TRANSACTION_STATE_FAILED
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - HANDLE_INITIAL_PRODUCT_REMOVAL - Transaction fail! Failed to remove product intially from business hub")
					ENDIF

					bOkayToContinue = TRUE
					eResult = CONTRABAND_TRANSACTION_STATE_DEFAULT
				ELSE
					PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - HANDLE_INITIAL_PRODUCT_REMOVAL - Waiting for transaction result...")
				ENDIF
			ENDIF
		ELSE
			bOkayToContinue = TRUE
			
			IF NOT DOES_BUSINESS_HUB_HAVE_ENOUGH_GOODS_FOR_INITIAL_REMOVAL()
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - HANDLE_INITIAL_PRODUCT_REMOVAL - I am the boss - We dont have enough goods to remove - bOkayToContinue = TRUE")
			ENDIF
		ENDIF
	ELSE
		bOkayToContinue = TRUE
		PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - HANDLE_INITIAL_PRODUCT_REMOVAL - I am a goon - bOkayToContinue = TRUE")
	ENDIF

	IF bOkayToContinue
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


//----------------------
//	MAIN SCRIPT
//----------------------

SCRIPT(MP_MISSION_DATA missionScriptArgs)


	PRINTLN("[MB_MISSION] - START")
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
			
			IF NOT PROCESS_PRE_GAME(missionScriptArgs)
				EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
				SCRIPT_CLEANUP()
			ENDIF
			// Setup widgets. Make sure this is done after PROCESS_PRE_GAME()
			#IF IS_DEBUG_BUILD
				CREATE_WIDGETS()
			#ENDIF
		ELSE
			
			PRINTLN("[MB_MISSION] calling SCRIPT_CLEANUP() - IS_NET_PLAYER_OK(localPlayerId, FALSE, TRUE) = FALSE")
			SCRIPT_CLEANUP()
			
		ENDIF
	ELSE
		
		PRINTLN("[MB_MISSION] calling SCRIPT_CLEANUP() - NETWORK_IS_GAME_IN_PROGRESS() = FALSE")
		EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
		SCRIPT_CLEANUP()
		
	ENDIF
	
//	FREEMODE_DELIVERY_MISSION_INIT()
	
	WHILE (TRUE)
	
        #IF IS_DEBUG_BUILD
        #IF SCRIPT_PROFILER_ACTIVE
        SCRIPT_PROFILER_END_OF_FRAME()
        #ENDIF
        #ENDIF                 
	
		MP_LOOP_WAIT_ZERO()
		
		#IF IS_DEBUG_BUILD
        #IF SCRIPT_PROFILER_ACTIVE
        SCRIPT_PROFILER_START_OF_FRAME()
        #ENDIF
        #ENDIF 
		
		#IF IS_DEBUG_BUILD
		//RUN_DEBUG_BOAT_OFFSET_TOOL()
		#ENDIF
				
		// Stop compiler whinging.
		IF iNumParticipantsOnThisScript = iNumParticipantsOnThisScript
			iNumParticipantsOnThisScript = iNumParticipantsOnThisScript
		ENDIF
		
		localPlayerId = PLAYER_ID()
		localPlayerPed = PLAYER_PED_ID()
		bLocalPlayerOk = IS_NET_PLAYER_OK(localPlayerId)
		
		#IF IS_DEBUG_BUILD
		IF IS_LOCAL_DEBUG_BIT_SET(eLOCALDEBUGBITSET_TERMINATE_SCRIPT_NOW)
			PRINTLN("[MB_MISSION] eLOCALDEBUGBITSET_TERMINATE_SCRIPT_NOW is set")
			EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
			SCRIPT_CLEANUP()
		ENDIF
		#ENDIF
		
		IF GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION(FALSE)
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION = TRUE")
			EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("[MB_MISSION] SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")
			
			IF NOT NETWORK_IS_GAME_IN_PROGRESS()	
				SET_LOCAL_BIT(eLOCALBITSET_SIGNED_OUT)
			ENDIF
			
			EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
			SCRIPT_CLEANUP()
		ENDIF
		
		IF bKillScriptLocal
			PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - bKillScriptLocal = TRUE")
			EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
			SCRIPT_CLEANUP()
		ENDIF
		
		// Only process once mode has been initialised.
		IF GET_MODE_STATE() > eMODESTATE_INIT
			//MAINTAIN_IS_SMUGGLER_VEHICLE_IN_POSSESSION_OF_RIVAL_PLAYER() 	// Needs to be done before participant loop because we check client data (this data is cleared every frame and then evaluated if it should be set - if we do this after the part loop then when the loop checks it it will be clear every time).
			PROCESS_PARTICIPANT_LOOP()
			PROCESS_EVENTS()
			MAINTAIN_POSTER_ANIMS()
		ENDIF
		
		IF IS_BIT_SET(MPGlobalsAmbience.iLesterDisableCopsBitset, biNoCops_Activated)
			SET_CLIENT_BIT(eCLIENTBITSET_I_HAVE_BRIBE_AUTHORITIES_RUNNING)
		ELSE
			CLEAR_CLIENT_BIT(eCLIENTBITSET_I_HAVE_BRIBE_AUTHORITIES_RUNNING)
		ENDIF
		
		#IF FEATURE_GEN9_EXCLUSIVE
		IF IS_PLAYER_ON_MP_INTRO()
			bUICheckTutorialSession = FALSE
		ELSE
		#ENDIF
			bUICheckTutorialSession = (NOT IS_LOCAL_BIT_SET(eLOCALBITSET_STARTED_TUTORIAL_SESSION))
		#IF FEATURE_GEN9_EXCLUSIVE
		ENDIF
		#ENDIF
		
		//GB_MAINTAIN_SPECTATE(sSpecVars)
		
		MAINTAIN_DPAD_LEADERBOARD()
		
		// Deal with the debug
		#IF IS_DEBUG_BUILD		
			UPDATE_WIDGETS()
			PRINT_SCRIPT_HOST_PLAYER(phStruct)
		#ENDIF
		
		SWITCH(GET_CLIENT_GAME_STATE(PARTICIPANT_ID_TO_INT()))
		
			CASE GAME_STATE_INIT
				IF GET_SERVER_GAME_STATE() = GAME_STATE_RUNNING
					IF GET_MEGA_BUSINESS_VARIATION() != MBV_INVALID
					AND GET_MEGA_BUSINESS_VARIATION() != MBV_MAX
					AND IS_SERVER_BIT_SET(eSERVERBITSET_SETUP_VARIATION)
						IF HANDLE_INITIAL_PRODUCT_REMOVAL()
							CLIENT_INITIALISE_VARS()
							SET_CLIENT_GAME_STATE(PARTICIPANT_ID_TO_INT(),GAME_STATE_RUNNING)
						ENDIF
					ELSE
						PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] [INITIAL_PRODUCT_REMOVAL] - GET_MEGA_BUSINESS_VARIATION() = MBV_MAX or MBV_INVALID")
					ENDIF
				ENDIF
			BREAK
			
			CASE GAME_STATE_RUNNING
				IF GET_SERVER_GAME_STATE() = GAME_STATE_RUNNING
				
					CLIENT_PROCESSING()

				ELIF GET_SERVER_GAME_STATE() = GAME_STATE_END
					SET_CLIENT_GAME_STATE(PARTICIPANT_ID_TO_INT(), GAME_STATE_END)
				ENDIF
			BREAK
			
			CASE GAME_STATE_END
				SCRIPT_CLEANUP()
			BREAK
			
		ENDSWITCH
	
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
			IF GB_SHOULD_KILL_ACTIVE_BOSS_MISSION()
				PRINTLN("[MB_MISSION] - [", GET_CURRENT_VARIATION_STRING(), "] - GB_SHOULD_KILL_ACTIVE_BOSS_MISSION = TRUE")
				SET_SERVER_GAME_STATE(GAME_STATE_END)
			ENDIF
		
			SWITCH(GET_SERVER_GAME_STATE())
			
				CASE GAME_STATE_INIT
					SET_SERVER_GAME_STATE(GAME_STATE_RUNNING)
				BREAK
				
				CASE GAME_STATE_RUNNING
					
					SERVER_PROCESSING()
					
				BREAK
				
				CASE GAME_STATE_END
					SCRIPT_CLEANUP()
				BREAK
				
			ENDSWITCH
		ENDIF
		
		#IF IS_DEBUG_BUILD	#IF SCRIPT_PROFILER_ACTIVE	ADD_SCRIPT_PROFILE_MARKER("THE REST")	#ENDIF	#ENDIF 
		
	ENDWHILE

	
	
// Script should never reach here. Always terminate with cleanup function.


ENDSCRIPT

// [MB_MISSION] - START
// mode state going to

//		CASE MBV_COLLECT_DJ_HOOKED
//			SWITCH iVehicle
//				CASE 0		RETURN POLICE3
//				CASE 1		RETURN CARGOBOB2
//				CASE 2		RETURN POLICET
//				CASE 3		RETURN POLICET
//			ENDSWITCH
//		BREAK

// GET_GOTO_LOCATION_DISTANCE
// fDistBetweenCargoboAndCopCar
// HOOKED_ARREST_HAS_STARTED()
// HOOKED_PED_ARRAY_INDEX
// HOOKED_PED
// MAINTAIN_HOOKED_CARGOBOB
// HOOKED_CARGOBOB_OCCUPIED
// HOOKED_IS_LOCAL_PLAYER_IN_CARGOBOB()
// MAINTAIN_MISSION_ENTITY_MISSION_SPECIFIC_CLIENT
// HOOKED_CARGOBOB_TARGET_IS_ATTACHED()
// HOOKED_MADONNA_SCENE_FINISHED()

//CASE MBV_COLLECT_DJ_HOOKED mission flow
	
	//eMODESTATE_COLLECT_ENTITY		"Collect the limo"
	//eMODESTATE_GO_TO_POINT		"Go to the helipad"							serverBD.iCurrentGotoLocation = INVALID_GOTO_LOCATION to FIRST_GOTO_LOCATION
	//eMODESTATE_COLLECT_VEHICLE	"Collect Cargobob"	
	//eMODESTATE_GO_TO_POINT		"Collect TBM from the rave"					serverBD.iCurrentGotoLocation = INVALID_GOTO_LOCATION to SECOND_GOTO_LOCATION
	//eMODESTATE_COLLECT_ITEM		"Pick up the cop car using the Cargobob"
	//eMODESTATE_GO_TO_POINT		"Fly to helipad"							serverBD.iCurrentGotoLocation = INVALID_GOTO_LOCATION to THIRD_GOTO_LOCATION
	//eMODESTATE_MID_MISSION_MOCAP 	
	//eMODESTATE_DELIVER_ENTITY		"Deliver TBM to the Nightclub"




