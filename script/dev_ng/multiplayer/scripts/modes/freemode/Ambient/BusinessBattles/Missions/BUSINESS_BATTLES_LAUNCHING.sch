USING "globals.sch"
USING "net_prints.sch"
USING "net_include.sch"
USING "commands_misc.sch"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Written by:  Martin McMillan & Ryan Elliott.																			//
// Date: 		08/12/2017																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "BUSINESS_BATTLES_COORDS.sch"
USING "BUSINESS_BATTLES_DROPOFFS.sch"

////////////////////////////////////////
/// MISSION LAUNCH LOCATION SELECTION ///
////////////////////////////////////////

FUNC FLOAT GET_FMBB_VARIATION_WEIGHTING(FMBB_VARIATION eVariation)
	SWITCH eVariation
		CASE BBV_ASSASSINATE_TARGETS	RETURN g_sMPTunables.fBBV_ASSASSINATE_TARGETS	
		CASE BBV_BREAK_IN				RETURN g_sMPTunables.fBBV_BREAK_IN             
		CASE BBV_CAR_MEET      			RETURN g_sMPTunables.fBBV_CAR_MEET             
		CASE BBV_CARPARK_SEARCH			RETURN g_sMPTunables.fBBV_CARPARK_SEARCH       
		CASE BBV_DROP_SITE     			RETURN g_sMPTunables.fBBV_DROP_SITE            
		CASE BBV_GANG_SHOOTOUT 			RETURN g_sMPTunables.fBBV_GANG_SHOOTOUT        
		CASE BBV_HAULAGE       			RETURN g_sMPTunables.fBBV_HAULAGE              
		CASE BBV_JOYRIDERS     			RETURN g_sMPTunables.fBBV_JOYRIDERS            
		CASE BBV_SHOWROOM      			RETURN g_sMPTunables.fBBV_SHOWROOM             
		CASE BBV_UNDER_GUARD   			RETURN g_sMPTunables.fBBV_UNDER_GUARD          
	ENDSWITCH
	
	RETURN 1.0
ENDFUNC

FUNC FLOAT GET_FMBB_SUBVARIATION_WEIGHTING(FMBB_SUBVARIATION eSubvariation)              
	SWITCH eSubvariation
		CASE BBS_MAX        
	ENDSWITCH
	
	RETURN 1.0
ENDFUNC

/////////////////////////////////////////
/// MISSION LAUNCH VARIATION SELECTION ///
/////////////////////////////////////////
FUNC BOOL IS_FMBB_SUBVARIATION_DISABLED(FMBB_SUBVARIATION eSubvariation)

	UNUSED_PARAMETER(eSubvariation)
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_FMBB_SUBVARIATION_SUITABLE(FMBB_SUBVARIATION eSubvariation #IF IS_DEBUG_BUILD , FMBB_VARIATION eVariation #ENDIF )
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_FM_BusinessBattlesFlowPlaySubvariations")
		IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_FM_BusinessBattlesFlowPlaySubvariations_alt")
			SWITCH eVariation
				CASE BBV_HAULAGE			RETURN (eSubvariation = BBS_HA_LOCATION_4)
				CASE BBV_DROP_SITE			RETURN (eSubvariation = BBS_DS_GRAND_SENORA_2)
				CASE BBV_JOYRIDERS			RETURN (eSubvariation = BBS_JO_ROUTE_6)
				CASE BBV_UNDER_GUARD		RETURN (eSubvariation = BBS_UG_FORT_ZANCUDO)
				CASE BBV_CARPARK_SEARCH		RETURN (eSubvariation = BBS_CPS_LSIA)
				CASE BBV_CAR_MEET			RETURN (eSubvariation = BBS_CM_SAN_ANDREAS)
				CASE BBV_GANG_SHOOTOUT		RETURN (eSubvariation = BBS_GS_KORTZ)
			ENDSWITCH
		ELSE
			SWITCH eVariation
				CASE BBV_MAX		RETURN (eSubvariation = BBS_MAX)
			ENDSWITCH
		ENDIF
	ENDIF
	#ENDIF

	UNUSED_PARAMETER(eSubvariation)

	RETURN TRUE
ENDFUNC

FUNC INT GET_FMBB_TUNABLE_DISTANCE_FROM_PROPERTY()
	RETURN g_sMPTunables.iBB_BUSINESS_BATTLES_GENERAL_DISTANCE_RESTRICTION_FROM_NIGHTCLUB                            
ENDFUNC

FUNC BOOL IS_FMBB_SUBVARIATION_SUITABLE_DISTANCE_FROM_PROPERTY(FMBB_VARIATION eVariation, FMBB_SUBVARIATION eSubvariation)
	
	SWITCH eVariation
		CASE BBV_ASSASSINATE_TARGETS
		CASE BBV_JOYRIDERS
		CASE BBV_BREAK_IN
		CASE BBV_SHOWROOM
			RETURN TRUE
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_FM_BusinessBattlesFlowPlaySubvariations")
		RETURN TRUE
	ENDIF
	#ENDIF
	
	INT iPlayer, iBitset
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		PLAYER_INDEX playerID = INT_TO_PLAYERINDEX(iPlayer)
		IF IS_NET_PLAYER_OK(playerID, FALSE)
			NIGHTCLUB_ID nightclub = GET_PLAYERS_OWNED_NIGHTCLUB(playerID)
			IF nightclub != NIGHTCLUB_ID_INVALID
			AND nightclub != NIGHTCLUB_ID_COUNT
				SET_BIT(iBitset, ENUM_TO_INT(nightclub))
			ENDIF
		ENDIF
	ENDREPEAT
	
	INT iNightclub
	REPEAT NIGHTCLUB_ID_COUNT iNightclub
		IF IS_BIT_SET(iBitset,iNightclub)
			VECTOR vCoords = g_SimpleInteriorData.vMidPoints[GET_SIMPLE_INTERIOR_ID_FROM_NIGHTCLUB_ID(INT_TO_ENUM(NIGHTCLUB_ID,iNightclub))]
			IF VDIST2(vCoords, GET_FMBB_ENTITY_SPAWN_COORDS(eVariation, eSubvariation, 0, TRUE, 0)) < (GET_FMBB_TUNABLE_DISTANCE_FROM_PROPERTY() * GET_FMBB_TUNABLE_DISTANCE_FROM_PROPERTY())
				PRINTLN("[FMBB] IS_FMBB_SUBVARIATION_SUITABLE_DISTANCE_FROM_PROPERTY(", GET_FMBB_SUBVARIATION_NAME_FOR_DEBUG(eSubvariation) ,") RETURNING FALSE")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT

	RETURN TRUE

ENDFUNC

FUNC BOOL SHOULD_CHECK_FMBB_ENTITY_SPAWN_LOCATION(FMBB_VARIATION eVariation, FMBB_SUBVARIATION eSubvariation)

	//carrier
	IF GET_FMBB_CARRIER_VEHICLE_INDEX(0, eSubvariation) != -1
		RETURN FALSE
	ENDIF

	SWITCH eVariation
		CASE BBV_ASSASSINATE_TARGETS
			RETURN FALSE
	ENDSWITCH 
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_CHECK_FMBB_VEHICLE_SPAWN_LOCATION(FMBB_VARIATION eVariation)
	SWITCH eVariation
		CASE BBV_MAX
			RETURN FALSE
	ENDSWITCH 
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_CHECK_FMBB_PED_SPAWN_LOCATION(FMBB_VARIATION eVariation)
	SWITCH eVariation
		CASE BBV_MAX
			RETURN FALSE
	ENDSWITCH 
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_FMBB_SUBVARIATION_SAFE_TO_SPAWN_ENTITIES(FMBB_VARIATION eVariation, FMBB_SUBVARIATION eSubvariation)
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_FM_BusinessBattlesFlowPlaySubvariations")
		RETURN TRUE
	ENDIF
	#ENDIF
	
	// Special case for Showroom
	IF eVariation = BBV_SHOWROOM
		IF IS_ANY_PLAYER_NEAR_POINT(<<-43.4203, -1096.9979, 26.8572>>, 100)
			PRINTLN("[FMBB] - IS_FMBB_SUBVARIATION_SAFE_TO_SPAWN_ENTITIES - FALSE - Players are within 100m of BBV_SHOWROOM spawn.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	MODEL_NAMES model
	FLOAT fRadius
	
	INT iNumEntities = GB_GET_NUM_FMBB_ENTITIES_FOR_VARIATION(eVariation)

	// Mission Entities
	INT iMissionEntity	
	REPEAT iNumEntities iMissionEntity
		IF SHOULD_CHECK_FMBB_ENTITY_SPAWN_LOCATION(eVariation, eSubvariation)
			model = GET_FMBB_ENTITY_MODEL(eVariation, BBT_WAREHOUSE)
			fRadius = GET_RADIUS_FROM_CONTRABAND_ENTITY(model)
			
			VECTOR vMissionEntityCoords = GET_FMBB_ENTITY_SPAWN_COORDS(eVariation, eSubvariation, iMissionEntity, TRUE,0) 

			PRINTLN("[FMBB] - IS_FMBB_SUBVARIATION_SAFE_TO_SPAWN_ENTITIES - Checking - Mission entity #",iMissionEntity," at ",vMissionEntityCoords," with radius ",fRadius, " with model ",GET_MODEL_NAME_FOR_DEBUG(model))
			IF NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION(vMissionEntityCoords,fRadius,fRadius,fRadius,0.0,FALSE,FALSE,FALSE,0.0,FALSE,DEFAULT,FALSE,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
				PRINTLN("[FMBB] - IS_FMBB_SUBVARIATION_SAFE_TO_SPAWN_ENTITIES - FALSE - Mission entity #",iMissionEntity," spawn point is not safe.")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT

	// Non-mission vehicles
	INT iNonMissionVehicle
	WHILE ( NOT IS_VECTOR_ZERO(GET_FMBB_VEHICLE_SPAWN_COORDS(eVariation, eSubvariation, iNonMissionVehicle, DEFAULT, 0)) )
	
		IF SHOULD_CHECK_FMBB_VEHICLE_SPAWN_LOCATION(eVariation)
			VECTOR vNonCoords = GET_FMBB_VEHICLE_SPAWN_COORDS(eVariation, eSubvariation, iNonMissionVehicle, DEFAULT, 0)
			model = GET_FMBB_VEHICLE_MODEL(eVariation, eSubvariation, iNonMissionVehicle,0)
			fRadius = GET_RADIUS_FROM_CONTRABAND_ENTITY(model)
			
			PRINTLN("[FMBB] - IS_FMBB_SUBVARIATION_SAFE_TO_SPAWN_ENTITIES - Checking - Non-mission vehicle #",iNonMissionVehicle," at ",vNonCoords," with radius ",fRadius, " with model ",GET_MODEL_NAME_FOR_DEBUG(model))
			IF NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION(vNonCoords,fRadius,fRadius,fRadius,0.0,FALSE,FALSE,FALSE,0.0,FALSE,DEFAULT,FALSE,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
				PRINTLN("[FMBB] - IS_FMBB_SUBVARIATION_SAFE_TO_SPAWN_ENTITIES - FALSE - Non-mission vehicle ",iNonMissionVehicle," not safe.")
				RETURN FALSE
			ENDIF
		ENDIF
		
		iNonMissionVehicle++
	ENDWHILE
	
	//Mission peds
	INT iPed
	WHILE (NOT IS_VECTOR_ZERO(GET_FMBB_PED_SPAWN_COORDS(iPed, eVariation, eSubvariation)))
		
		IF SHOULD_CHECK_FMBB_PED_SPAWN_LOCATION(eVariation)
			VECTOR vPedCoords = GET_FMBB_PED_SPAWN_COORDS(iPed, eVariation, eSubvariation)
			model = mp_m_freemode_01
			fRadius = 2
			
			PRINTLN("[FMBB] - IS_FMBB_SUBVARIATION_SAFE_TO_SPAWN_ENTITIES - Checking - Ped #",iPed," at ",vPedCoords," with radius ",fRadius)
			IF NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION(vPedCoords,fRadius,fRadius,fRadius,0.0,FALSE,FALSE,FALSE,0.0,FALSE,DEFAULT,FALSE,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
				PRINTLN("[FMBB] - IS_FMBB_SUBVARIATION_SAFE_TO_SPAWN_ENTITIES - FALSE - Ped #",iPed," is not safe.")
				RETURN FALSE
			ENDIF
		ENDIF
		
		iPed++
	ENDWHILE
	
	
	
	RETURN TRUE

ENDFUNC

/// PURPOSE: Returns the number of subvariations a variation has.
/// PARAMS: iVariation - The variation that is being checked
/// RETURNS: The current number of subvariations the passed in variation has.
/// THIS HAS TO BE UPDATED WHEN ADDING NEW SUBVARIATIONS
FUNC INT GET_MAX_FMBB_SUBVARIATION_FOR_VARIATION(FMBB_VARIATION eVariation)

	SWITCH eVariation
		CASE BBV_ASSASSINATE_TARGETS		RETURN 3
		CASE BBV_BREAK_IN                   RETURN 10
		CASE BBV_CAR_MEET                   RETURN 3
		CASE BBV_CARPARK_SEARCH             RETURN 3
		CASE BBV_DROP_SITE                  RETURN 3
		CASE BBV_GANG_SHOOTOUT              RETURN 3
		CASE BBV_HAULAGE                    RETURN 4
		CASE BBV_JOYRIDERS                  RETURN 10
		CASE BBV_SHOWROOM                   RETURN 1
		CASE BBV_UNDER_GUARD                RETURN 3
	ENDSWITCH
	
	PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FMBB] GET_MAX_FMBB_SUBVARIATION_FOR_VARIATION - Variation is invalid. Variation = ", GET_FMBB_VARIATION_NAME_FOR_DEBUG(eVariation))
	RETURN 0
ENDFUNC

/// PURPOSE: Returns the start point of subvariations for a variation in the subvariation enum.
/// PARAMS: iVariation - The variation that is being checked
/// RETURNS: the start point of subvariations for a variation in the subvariation enum.
/// This shouldn't need updated as long as all new subvariations are added after the existing subvariations for that variation
FUNC INT GET_START_FMBB_SUBVARIATION_FOR_VARIATION(FMBB_VARIATION eVariation)

	SWITCH eVariation
		CASE BBV_ASSASSINATE_TARGETS		RETURN ENUM_TO_INT(BBS_AT_LOCATION_1)
		CASE BBV_BREAK_IN                   RETURN ENUM_TO_INT(BBS_BI_LOCATION_1)
		CASE BBV_CAR_MEET                   RETURN ENUM_TO_INT(BBS_CM_SAN_ANDREAS)
		CASE BBV_CARPARK_SEARCH             RETURN ENUM_TO_INT(BBS_CPS_LSIA)
		CASE BBV_DROP_SITE                  RETURN ENUM_TO_INT(BBS_DS_GRAND_SENORA)
		CASE BBV_GANG_SHOOTOUT              RETURN ENUM_TO_INT(BBS_GS_SLAUGHTERHOUSE)
		CASE BBV_HAULAGE                    RETURN ENUM_TO_INT(BBS_HA_LOCATION_1)
		CASE BBV_JOYRIDERS                  RETURN ENUM_TO_INT(BBS_JO_ROUTE_1)
		CASE BBV_SHOWROOM                   RETURN ENUM_TO_INT(BBS_SH_SHOWROOM)
		CASE BBV_UNDER_GUARD                RETURN ENUM_TO_INT(BBS_UG_FORT_ZANCUDO)
	ENDSWITCH
	
	PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FMBB] GET_START_FMBB_SUBVARIATION_FOR_VARIATION - Variation is invalid. Variation = ", GET_FMBB_VARIATION_NAME_FOR_DEBUG(eVariation))
	RETURN ENUM_TO_INT(BBV_INVALID)
ENDFUNC
    
/// Select a random subvariation for the supplied variation
FUNC INT GB_GET_RANDOM_FMBB_SUBVARIATION(FMBB_VARIATION eVariation)
	INT iSelectedSubvariation = -1
	
	// Get the start and end point for the RNG, so as only to check subvariations for the supplied variation
	INT iStartSubvariation = GET_START_FMBB_SUBVARIATION_FOR_VARIATION(eVariation)
	INT iEndSubvariation = (iStartSubvariation + GET_MAX_FMBB_SUBVARIATION_FOR_VARIATION(eVariation)) - 1
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[FMBB] -----------------------------------------------------------------------------------------------------------------")
	PRINTLN("[FMBB] GB_GET_RANDOM_FMBB_SUBVARIATION - Start Variation: ", GET_FMBB_SUBVARIATION_NAME_FOR_DEBUG(INT_TO_ENUM(FMBB_SUBVARIATION, iStartSubvariation))) 
	PRINTLN("[FMBB] GB_GET_RANDOM_FMBB_SUBVARIATION - End Variation: ", GET_FMBB_SUBVARIATION_NAME_FOR_DEBUG(INT_TO_ENUM(FMBB_SUBVARIATION, iEndSubvariation)))
	#ENDIF
	
	IF iStartSubvariation = iEndSubvariation
		PRINTLN("[FMBB] GB_GET_RANDOM_FMBB_SUBVARIATION - ", GET_FMBB_VARIATION_NAME_FOR_DEBUG(eVariation), " has only 1 subvariation - ", GET_FMBB_SUBVARIATION_NAME_FOR_DEBUG(INT_TO_ENUM(FMBB_SUBVARIATION, iStartSubvariation)))
	ENDIF
	
	FLOAT fWeightings[BBS_MAX]
	FLOAT fTotalWeight, fCurrentRange
	
	//Assign weightings to each available subvariation
	INT iLoop
	FOR iLoop = iStartSubvariation TO (iEndSubvariation)
		FMBB_SUBVARIATION eTemp = INT_TO_ENUM(FMBB_SUBVARIATION, iLoop)
		IF IS_FMBB_SUBVARIATION_DISABLED(eTemp)
			PRINTLN("[FMBB] GB_GET_RANDOM_FMBB_SUBVARIATION - IS_FMBB_SUBVARIATION_DISABLED(", GET_FMBB_SUBVARIATION_NAME_FOR_DEBUG(eTemp) ,") = TRUE")
			RELOOP
		ENDIF
		IF NOT IS_FMBB_SUBVARIATION_SUITABLE(eTemp #IF IS_DEBUG_BUILD , eVariation #ENDIF )
			PRINTLN("[FMBB] GB_GET_RANDOM_FMBB_SUBVARIATION - IS_FMBB_SUBVARIATION_SUITABLE(", GET_FMBB_SUBVARIATION_NAME_FOR_DEBUG(eTemp) ,") = FALSE")
			RELOOP
		ENDIF
		IF NOT IS_FMBB_SUBVARIATION_SUITABLE_DISTANCE_FROM_PROPERTY(eVariation, eTemp)
			PRINTLN("[FMBB] GB_GET_RANDOM_FMBB_SUBVARIATION - IS_FMBB_SUBVARIATION_SUITABLE_DISTANCE_FROM_PROPERTY(", GET_FMBB_SUBVARIATION_NAME_FOR_DEBUG(eTemp) ,") = FALSE")
			RELOOP
		ENDIF
		IF NOT IS_FMBB_SUBVARIATION_SAFE_TO_SPAWN_ENTITIES(eVariation, eTemp)
			PRINTLN("[FMBB] GB_GET_RANDOM_FMBB_SUBVARIATION - IS_FMBB_SUBVARIATION_SAFE_TO_SPAWN_ENTITIES(", GET_FMBB_SUBVARIATION_NAME_FOR_DEBUG(eTemp) ,") = FALSE")
			RELOOP
		ENDIF
		fWeightings[iLoop] = GET_FMBB_SUBVARIATION_WEIGHTING(eTemp)
		PRINTLN("[FMBB] GB_GET_RANDOM_FMBB_VARIATION - GET_FMBB_SUBVARIATION_WEIGHTING(", GET_FMBB_SUBVARIATION_NAME_FOR_DEBUG(eTemp) ,") = ", fWeightings[iLoop])
	ENDFOR
	
	//Calculate total of all weightings
	FOR iLoop = iStartSubvariation TO (iEndSubvariation)
		fTotalWeight += fWeightings[iLoop]
		PRINTLN("[FMBB] GB_GET_RANDOM_FMBB_SUBVARIATION - ", GET_FMBB_SUBVARIATION_NAME_FOR_DEBUG(INT_TO_ENUM(FMBB_SUBVARIATION, iLoop)), " has a weighting of ", fWeightings[iLoop])
	ENDFOR
	
	//Pick a position between 0 and total of all weightings
	FLOAT fSelection = GET_RANDOM_FLOAT_IN_RANGE(0, fTotalWeight)
	
	//Find the variation at that position
	FOR iLoop = iStartSubvariation TO (iEndSubvariation)
		fCurrentRange += fWeightings[iLoop]
		IF fSelection < fCurrentRange
			iSelectedSubvariation = iLoop
			BREAKLOOP
		ENDIF
	ENDFOR
	
	PRINTLN("[FMBB] GB_GET_RANDOM_FMBB_SUBVARIATION - Selected subvariation ", GET_FMBB_SUBVARIATION_NAME_FOR_DEBUG(INT_TO_ENUM(FMBB_SUBVARIATION, iSelectedSubvariation)), " for variation ", GET_FMBB_VARIATION_NAME_FOR_DEBUG(eVariation))
	PRINTLN("[FMBB] -----------------------------------------------------------------------------------------------------------------")
	RETURN iSelectedSubvariation
ENDFUNC

#IF IS_DEBUG_BUILD
CONST_INT MAX_FMBB_RESUPPLY_FLOW_PLAY_VARIATIONS		7
CONST_INT MAX_FMBB_RESUPPLY_FLOW_PLAY_VARIATIONS_ALT	7
FUNC FMBB_VARIATION GET_NEXT_FMBB_RESUPPLY_FLOW_PLAY_VARIATION(INT iNextBuyStealFlowPlayVariation)
	
	IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_FM_BusinessBattlesFlowPlayVariations_alt")
		SWITCH iNextBuyStealFlowPlayVariation
			CASE 0		RETURN BBV_MAX
		ENDSWITCH
	ELSE
		SWITCH iNextBuyStealFlowPlayVariation
			CASE 0		RETURN BBV_MAX
		ENDSWITCH
	ENDIF
	
	RETURN BBV_MAX
ENDFUNC

FUNC BOOL IS_FMBB_RESUPPLY_VARIATION_IN_FLOW_PLAY(FMBB_VARIATION eVariation, INT iNextBuyStealFlowPlayVariation)
	RETURN GET_NEXT_FMBB_RESUPPLY_FLOW_PLAY_VARIATION(iNextBuyStealFlowPlayVariation) = eVariation
ENDFUNC

CONST_INT MAX_FMBB_SELL_FLOW_PLAY_VARIATIONS		5
CONST_INT MAX_FMBB_SELL_FLOW_PLAY_VARIATIONS_ALT	5
FUNC FMBB_VARIATION GET_NEXT_FMBB_SELL_FLOW_PLAY_VARIATION(INT iNextSellFlowPlayVariation)

	IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_FM_BusinessBattlesFlowPlayVariations_alt")
		SWITCH iNextSellFlowPlayVariation
			CASE 0		RETURN BBV_MAX
		ENDSWITCH
	ELSE
		SWITCH iNextSellFlowPlayVariation
			CASE 0		RETURN BBV_MAX
		ENDSWITCH
	ENDIF
	
	RETURN BBV_MAX
ENDFUNC

FUNC BOOL IS_FMBB_SELL_VARIATION_IN_FLOW_PLAY(FMBB_VARIATION eVariation, INT iNextSellFlowPlayVariation)
	RETURN GET_NEXT_FMBB_SELL_FLOW_PLAY_VARIATION(iNextSellFlowPlayVariation) = eVariation
ENDFUNC
#ENDIF

FUNC INT GET_NUM_MIN_PARTICIPANTS_FOR_FMBB_VARIATION(FMBB_VARIATION eVariation)

	UNUSED_PARAMETER(eVariation)

	RETURN 0

ENDFUNC

FUNC BOOL GB_DOES_FMBB_VARIATION_HAVE_ENOUGH_PARTICIPANTS(FMBB_VARIATION eVariation, INT iNumParticipants)

	RETURN iNumParticipants >= GET_NUM_MIN_PARTICIPANTS_FOR_FMBB_VARIATION(eVariation)

ENDFUNC

FUNC BOOL IS_TIME_OF_DAY_SUITABLE_FOR_FMBB_VARIATION(FMBB_VARIATION eVariation)

	UNUSED_PARAMETER(eVariation)

//	TIMEOFDAY tofTime = GET_CURRENT_TIMEOFDAY() 
//	INT iHour = GET_TIMEOFDAY_HOUR(tofTime)
	
//	SWITCH eVariation
//
//	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_FMBB_VARIATION_DISABLED(FMBB_VARIATION eVariation #IF IS_DEBUG_BUILD , INT iNextResupplyFlowPlayVariation#ENDIF )
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_FM_BusinessBattlesFlowPlayVariations")

		IF NOT IS_FMBB_RESUPPLY_VARIATION_IN_FLOW_PLAY(eVariation, iNextResupplyFlowPlayVariation)
			RETURN TRUE
		ELSE
			PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FMBB] [FPLAY_RESUP] IS_FMBB_VARIATION_DISABLED - ", GET_FMBB_VARIATION_NAME_FOR_DEBUG(eVariation), " is next Resupply in the flow play.")
			RETURN FALSE
		ENDIF

	ENDIF
	#ENDIF
	
	IF NOT IS_TIME_OF_DAY_SUITABLE_FOR_FMBB_VARIATION(eVariation)
		PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FMBB] IS_FMBB_VARIATION_DISABLED - Time of day is not suitable for ", GET_FMBB_VARIATION_NAME_FOR_DEBUG(eVariation), " to launch. Disabling variation.")
		RETURN TRUE
	ENDIF
	
	// Launch Restrictions (Gang Size, Product %)
	SWITCH(eVariation)

		CASE BBV_UNDER_GUARD			RETURN g_sMPTunables.bBB_BUSINESS_BATTLES_MISSIONS_DISABLE_UNDER_GUARD                                           
		CASE BBV_CARPARK_SEARCH			RETURN g_sMPTunables.bBB_BUSINESS_BATTLES_MISSIONS_DISABLE_CARPARK_SEARCH                                        
		CASE BBV_JOYRIDERS				RETURN g_sMPTunables.bBB_BUSINESS_BATTLES_MISSIONS_DISABLE_JOYRIDERS                                             
		CASE BBV_HAULAGE				RETURN g_sMPTunables.bBB_BUSINESS_BATTLES_MISSIONS_DISABLE_HAULAGE_II                                            
		CASE BBV_BREAK_IN				RETURN g_sMPTunables.bBB_BUSINESS_BATTLES_MISSIONS_DISABLE_BREAK_IN                                              
		CASE BBV_CAR_MEET				RETURN g_sMPTunables.bBB_BUSINESS_BATTLES_MISSIONS_DISABLE_CAR_MEET_II                                           
		CASE BBV_SHOWROOM				RETURN g_sMPTunables.bBB_BUSINESS_BATTLES_MISSIONS_DISABLE_SHOWROOM                                              
		CASE BBV_GANG_SHOOTOUT			RETURN g_sMPTunables.bBB_BUSINESS_BATTLES_MISSIONS_DISABLE_GANG_SHOOTOUT                                         
		CASE BBV_ASSASSINATE_TARGETS	RETURN g_sMPTunables.bBB_BUSINESS_BATTLES_MISSIONS_DISABLE_ASSASSINATE_TARGETS                                   
		CASE BBV_DROP_SITE				RETURN g_sMPTunables.bBB_BUSINESS_BATTLES_MISSIONS_DISABLE_DROP_SITE                                             
		
	ENDSWITCH
	
	RETURN FALSE
	UNUSED_PARAMETER(eVariation)
ENDFUNC

FUNC BOOL IS_FMBB_VARIATION_IN_HISTORY(FMBB_VARIATION eVariation)

	INT iLength = GET_FMBB_HISTORY_LIST_LENGTH()

	INT iTemp	
	IF iLength > 0 AND iLength <= ENUM_TO_INT(BBV_MAX)
		REPEAT iLength iTemp
			IF GlobalServerBD_FM_events.sGBWorkData.FMBBHistory[iTemp] = eVariation
				RETURN TRUE
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_VARIATION_SUITABLE_FOR_CARGO_TYPE(FMBB_VARIATION eVariation1, FMBB_TYPE eType)

	UNUSED_PARAMETER(eVariation1)

	IF eType = BBT_EXPORT_GARAGE
		RETURN FALSE
	ENDIF
		
	RETURN TRUE
ENDFUNC

FUNC BOOL GB_DOES_PLAYER_OWN_BUSINESS_FOR_CARGO_TYPE(PLAYER_INDEX playerId, FMBB_TYPE type)

	SWITCH type
		CASE BBT_WAREHOUSE		RETURN DOES_PLAYER_OWN_A_WAREHOUSE(playerId)
		CASE BBT_BIKER_FACTORY	RETURN DOES_PLAYER_OWN_A_FACTORY(playerId)
		CASE BBT_EXPORT_GARAGE	RETURN DOES_PLAYER_OWN_AN_IE_GARAGE(playerId)
		CASE BBT_BUNKER			RETURN DOES_PLAYER_OWN_A_BUNKER(playerId)
		CASE BBT_HANGAR			RETURN DOES_PLAYER_OWN_A_HANGER(playerId)
		CASE BBT_EVENT			RETURN TRUE
		CASE BBT_UFO_PARTS		RETURN TRUE
	ENDSWITCH

	RETURN FALSE

ENDFUNC

FUNC FLOAT GET_EVENT_ITEM_TUNABLE_WEIGHTING()

	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_ForceBBEventItems")
		RETURN 1.0
	ENDIF
	#ENDIF

	RETURN g_sMPTunables.fBB_BUSINESS_BATTLE_EVENT_CARGO_WEIGHTING
ENDFUNC

FUNC BOOL SHOULD_BUSINESS_TYPE_BE_EVENT_ITEM()

	// Check if we have an event item to give
	IF NOT IS_BBT_EVENT_ALLOWED()
		RETURN FALSE
	ENDIF

	IF GET_EVENT_ITEM_TUNABLE_WEIGHTING() = 0.0
		RETURN FALSE
	ENDIF

	IF GET_RANDOM_FLOAT_IN_RANGE() <= GET_EVENT_ITEM_TUNABLE_WEIGHTING()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC GB_GET_BUSINESS_TYPE_FOR_FMBB_VARIATION(FMBB_VARIATION eVariation, FMBB_TYPE& eBusinessType)
	IF eBusinessType != BBT_INVALID
		PRINTLN("[FMBB] [FMBB_LAUNCH] GB_GET_BUSINESS_TYPE_FOR_FMBB_VARIATION - Forced business type - ", GET_FMBB_TYPE_NAME_FOR_DEBUG(eBusinessType))
		EXIT
	ENDIF
	
	IF SHOULD_BUSINESS_TYPE_BE_EVENT_ITEM()
		eBusinessType = BBT_EVENT
		PRINTLN("[FMBB] [FMBB_LAUNCH] GB_GET_BUSINESS_TYPE_FOR_FMBB_VARIATION - SHOULD_BUSINESS_TYPE_BE_EVENT_ITEM - ", GET_FMBB_TYPE_NAME_FOR_DEBUG(eBusinessType))
		EXIT
	ENDIF
	
	FMBB_TYPE eSuitableTypes[BBT_NORMAL_MAX]
	INT iType, iSuitableTypes
	REPEAT ENUM_TO_INT(BBT_NORMAL_MAX) iType
		FMBB_TYPE eType = INT_TO_ENUM(FMBB_TYPE, iType)
		IF IS_VARIATION_SUITABLE_FOR_CARGO_TYPE(eVariation, eType)
			eSuitableTypes[iSuitableTypes] = eType
			iSuitableTypes++
			PRINTLN("[FMBB] GB_GET_BUSINESS_TYPE_FOR_FMBB_VARIATION - ", GET_FMBB_TYPE_NAME_FOR_DEBUG(eType), " is suitable for ", GET_FMBB_VARIATION_NAME_FOR_DEBUG(eVariation))
		ENDIF
	ENDREPEAT
	
	IF iSuitableTypes > 0
		eBusinessType = eSuitableTypes[GET_RANDOM_INT_IN_RANGE(0, iSuitableTypes)]
	ELSE
		eBusinessType = BBT_INVALID
		PRINTLN("[FMBB] [FMBB_LAUNCH] GB_GET_BUSINESS_TYPE_FOR_FMBB_VARIATION - Unable to find suitable Business Type for ", GET_FMBB_VARIATION_NAME_FOR_DEBUG(eVariation))
	ENDIF
ENDPROC

PROC GB_GET_RANDOM_FMBB_VARIATION(INT& iVariation, INT& iSubvariation, FMBB_TYPE& eBusinessType, INT iNumParticipants)
	PRINTLN("[FMBB] [FMBB_LAUNCH] GB_GET_RANDOM_FMBB_VARIATION - START")
	
	IF iVariation > -1
		PRINTLN("[FMBB] [FMBB_LAUNCH] GB_GET_RANDOM_FMBB_VARIATION - Forced variation: ", GET_FMBB_VARIATION_NAME_FOR_DEBUG(INT_TO_ENUM(FMBB_VARIATION, iVariation)))
		IF DOES_FMBB_VARIATION_HAVE_SUBVARIATIONS(INT_TO_ENUM(FMBB_VARIATION, iVariation))
			IF iSubvariation > -1
				PRINTLN("[FMBB] [FMBB_LAUNCH] GB_GET_RANDOM_FMBB_VARIATION - Forced subvariation: ", GET_FMBB_SUBVARIATION_NAME_FOR_DEBUG(INT_TO_ENUM(FMBB_SUBVARIATION, iSubvariation)))
			ELSE
				iSubvariation = GB_GET_RANDOM_FMBB_SUBVARIATION(INT_TO_ENUM(FMBB_VARIATION, iVariation))
				PRINTLN("[FMBB] [FMBB_LAUNCH] GB_GET_RANDOM_FMBB_VARIATION - Forced variation without subvariation. Subvariation selected: ", GET_FMBB_SUBVARIATION_NAME_FOR_DEBUG(INT_TO_ENUM(FMBB_SUBVARIATION, iSubvariation)))
			ENDIF
		ENDIF
		GB_GET_BUSINESS_TYPE_FOR_FMBB_VARIATION(INT_TO_ENUM(FMBB_VARIATION, iVariation), eBusinessType)
		PRINTLN("[FMBB] [FMBB_LAUNCH] GB_GET_RANDOM_FMBB_VARIATION - END")
		EXIT
	ENDIF

	FLOAT fWeightings[BBV_MAX]
	FLOAT fTotalWeight, fCurrentRange
	
	//Assign weightings to each variation
	INT iVar
	REPEAT ENUM_TO_INT(BBV_MAX) iVar
		FMBB_VARIATION eTemp = INT_TO_ENUM(FMBB_VARIATION,iVar)
		IF IS_FMBB_VARIATION_DISABLED(eTemp #IF IS_DEBUG_BUILD , GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iNextBuyStealFlowPlayVariation #ENDIF )
			PRINTLN("[FMBB] [FMBB_LAUNCH] GB_GET_RANDOM_FMBB_VARIATION - IS_FMBB_VARIATION_DISABLED(", GET_FMBB_VARIATION_NAME_FOR_DEBUG(eTemp) ,") = TRUE")
			RELOOP
		ENDIF
		IF IS_FMBB_VARIATION_IN_HISTORY(eTemp)
			PRINTLN("[FMBB] [FMBB_LAUNCH] GB_GET_RANDOM_FMBB_VARIATION - IS_FMBB_VARIATION_IN_HISTORY(", GET_FMBB_VARIATION_NAME_FOR_DEBUG(eTemp) ,") = TRUE")
			RELOOP
		ENDIF
		IF NOT GB_DOES_FMBB_VARIATION_HAVE_ENOUGH_PARTICIPANTS(eTemp,iNumParticipants)
			PRINTLN("[FMBB] [FMBB_LAUNCH] GB_GET_RANDOM_FMBB_VARIATION - NOT GB_DOES_FMBB_VARIATION_HAVE_ENOUGH_PARTICIPANTS(", GET_FMBB_VARIATION_NAME_FOR_DEBUG(eTemp) ,") = TRUE")
			RELOOP
		ENDIF
		IF NOT FREEMODE_CONTENT_CAN_REGISTER_DELIVERABLES(GB_GET_NUM_FMBB_ENTITIES_FOR_VARIATION(eTemp))
			RELOOP
		ENDIF		
		
		fWeightings[iVar] = GET_FMBB_VARIATION_WEIGHTING(eTemp)
	ENDREPEAT
	
	//Calculate total of all weightings
	INT iTemp
	REPEAT BBV_MAX iTemp
		fTotalWeight += fWeightings[iTemp]
		PRINTLN("[FMBB] GB_GET_RANDOM_FMBB_VARIATION - ", GET_FMBB_VARIATION_NAME_FOR_DEBUG(INT_TO_ENUM(FMBB_VARIATION, iTemp)), " has a weighting of ", fWeightings[iTemp])
	ENDREPEAT
	
	//Pick a position between 0 and total of all weightings
	FLOAT fSelection = GET_RANDOM_FLOAT_IN_RANGE(0,fTotalWeight)
	PRINTLN("[FMBB] GB_GET_RANDOM_FMBB_VARIATION - fSelection = ", fSelection)

	//Find the variation at that position
	REPEAT BBV_MAX iTemp
		fCurrentRange += fWeightings[iTemp]
		IF fSelection < fCurrentRange
			IF NOT DOES_FMBB_VARIATION_HAVE_SUBVARIATIONS(INT_TO_ENUM(FMBB_VARIATION, iTemp))
				iVariation = iTemp
				GB_GET_BUSINESS_TYPE_FOR_FMBB_VARIATION(INT_TO_ENUM(FMBB_VARIATION, iVariation), eBusinessType)
				PRINTLN("[FMBB] [FMBB_LAUNCH] GB_GET_RANDOM_FMBB_VARIATION - END")
				EXIT
			ELSE
				iSubvariation = GB_GET_RANDOM_FMBB_SUBVARIATION(INT_TO_ENUM(FMBB_VARIATION, iTemp))
				iVariation = iTemp
				GB_GET_BUSINESS_TYPE_FOR_FMBB_VARIATION(INT_TO_ENUM(FMBB_VARIATION, iVariation), eBusinessType)
				PRINTLN("[FMBB] [FMBB_LAUNCH] GB_GET_RANDOM_FMBB_VARIATION - END")
				EXIT
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

/////////////////////////////
/// MISSION LAUNCH DATA  ///
/////////////////////////////

FUNC BOOL GB_GET_RANDOM_FMBB_VARIATION_DATA(INT& iVariation, INT& iSubvariation, FMBB_TYPE& eBusinessType, INT iNumParticipants)//, FREEMODE_DELIVERY_ACTIVE_DROPOFF_PROPERTIES &sDropOffProperties)
	INT iTempVar = iVariation
	INT iTempSub = iSubvariation
	FMBB_TYPE eTempBusinessType = eBusinessType
	GB_GET_RANDOM_FMBB_VARIATION(iTempVar, iTempSub, eTempBusinessType, iNumParticipants)
	
	IF iTempVar != (-1)
	
		IF iTempSub != ENUM_TO_INT(BBS_INVALID)
			iVariation = iTempVar
			iSubvariation = iTempSub
			eBusinessType = eTempBusinessType
			PRINTLN("[FMBB] [FMBB_LAUNCH] -----------------------------------------------------------------------------------------------------------------")
			PRINTLN("[FMBB] [FMBB_LAUNCH] GB_GET_RANDOM_FMBB_VARIATION_DATA - Selected variation: ", GET_FMBB_VARIATION_NAME_FOR_DEBUG(INT_TO_ENUM(FMBB_VARIATION, iVariation)))
			PRINTLN("[FMBB] [FMBB_LAUNCH] GB_GET_RANDOM_FMBB_VARIATION_DATA - Selected subvariation: ", GET_FMBB_SUBVARIATION_NAME_FOR_DEBUG(INT_TO_ENUM(FMBB_SUBVARIATION, iSubvariation)))
			PRINTLN("[FMBB] [FMBB_LAUNCH] GB_GET_RANDOM_FMBB_VARIATION_DATA - Selected business type: ", GET_FMBB_TYPE_NAME_FOR_DEBUG(eBusinessType))
			PRINTLN("[FMBB] [FMBB_LAUNCH] -----------------------------------------------------------------------------------------------------------------")
			
			//Find dropoff
//			IF GET_DROP_OFF_FOR_FMBB_VARIATION(playerID,INT_TO_ENUM(FMBB_VARIATION, iTempVar),sDropOffProperties, GET_PLAYERS_OWNED_DEFUNCT_BASE(playerID))
//				RESERVE_DROPOFFS_ON_SERVER(sDropOffProperties.eDropoffList, playerID)
//				PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] [FMBB] GB_GET_RANDOM_FMBB_VARIATION_DATA - Found dropoff starting at ",sDropOffProperties.eDropoffList.eDropoffID[0])
//			ELSE
//				PRINTLN("[FMBB] GB_GET_RANDOM_FMBB_VARIATION_DATA - Couldn't find a valid dropoff.")
//				RETURN FALSE
//			ENDIF
			
			RETURN TRUE
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[FMBB] [FMBB_LAUNCH] GB_GET_RANDOM_FMBB_VARIATION_DATA - Couldn't find a valid subvariation.")
		#ENDIF
		ENDIF
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[FMBB] [FMBB_LAUNCH] GB_GET_RANDOM_FMBB_VARIATION_DATA - Couldn't find a valid variation.")
	#ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

