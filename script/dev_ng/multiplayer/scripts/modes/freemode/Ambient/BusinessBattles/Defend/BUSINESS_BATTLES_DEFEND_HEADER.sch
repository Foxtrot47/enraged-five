
//////////////////////////////////////////////////////////////////////////////////////////
// Name:        BUSINESS_BATTLES_DEFEND.sch												//
// Description: Defend Contraband business header										//
// Written by:  David Trenholme															//
// Date: 		17/01/2017																//
//////////////////////////////////////////////////////////////////////////////////////////

//----------------------
//	INCLUDES
//----------------------
USING "globals.sch"
USING "business_battles_dropoffs.sch"
USING "net_gang_boss_common.sch"
USING "net_include.sch"
USING "net_realty_nightclub.sch"
USING "net_prints.sch"
USING "net_gang_boss_gang_chase.sch"
USING "net_simple_interior.sch"

//----------------------
//	CONSTANTS
//----------------------
CONST_INT 	DIFFICULTY_NONE					0				//Difficulty settings.
CONST_INT 	DIFFICULTY_EASY					1
CONST_INT 	DIFFICULTY_MEDIUM				2
CONST_INT 	DIFFICULTY_HARD					3
CONST_INT	DISPLAY_TIMER_TIME				1000 * 60 * 5	//How much time should be remaining before displaying the timer on the HUD (5 minutes).
CONST_INT	DISPLAY_TIMER_CRITICAL_TIME		1000 * 30		//How much time should be remaining before displaying the timer as critical on the HUD (30 seconds).
CONST_INT	BUSTED_SHARD_TIME				1000 * 4		//How much time to spend on the busted shard before transitioning to the spectator camera (4 seconds).
CONST_INT	KILL_FAILSAFE_TIME				1000 * 10		//The length of time before a killed target is set as killed when no-one has claimed the kill (10 seconds).
CONST_INT	MAX_PANIC_TIME					1000 * 15		//How much time can be spent above PANIC_HEIGHT_ABOVE_GROUND before the ped panics (15 seconds).
CONST_INT	WARP_DELAY						1500			//How much time to wait before considering the warp finished (1.5 seconds).
CONST_INT	WARP_TIMEOUT					1000 * 3		//How much time to wait after the warp before moving on (3 seconds).
CONST_INT	IPL_DELAY						1000 * 2		//How much time to wait before considering an IPL to have been completely loaded in (2 seconds).
CONST_INT	PROP_SPAWN_TIME_LIMIT			1000 * 15		//How much time to attempt to spawn the props.
CONST_INT	MISSION_END_TIMEOUT				1000 * 60		//The maximum time to wait before the script cleanup runs automatically. Used in Hold Up in case the escorted peds cannot reach the Nightclub door (60 seconds).
CONST_INT	NIGHTCLUB_INVINCIBILITY_TIME	1000 * 5		//How much time the player is invincible for when forced to leave the Nightclub due to a mission starting (5 seconds).
CONST_INT	TIME_BEFORE_WARP				1000 * 60		//How much time to wait for a ped to reach their destination before warping them straight there (60 seconds).
CONST_INT	OPENING_CALL_TIMEOUT			1000 * 20		//How much time to attempt to make the opening phonecall for a mission (20 seconds).
CONST_INT	STOLEN_VINYL_ALERT_DELAY		1000 * 10		//How long it takes for the peds on the yacht to be alerted in Stolen Vinyl (10 seconds).
CONST_FLOAT MAX_COMBAT_RANGE				299.9			//The maximum range within which to combat hated targets.
CONST_INT	MAX_SPAWN_ALTERNATIVES			2				//The maximum number of alternatives to check before spawning a vehicle.
CONST_FLOAT SPAWN_SEARCH_MIN_RADIUS			20.0			//The minimum radius from a player to search for suitable enemy spawn points.
CONST_FLOAT	SPAWN_SEARCH_MAX_RADIUS			50.0			//The maximum radius from a specified location to search for suitable enemy spawn points.
CONST_FLOAT SAFE_SPAWN_AREA_RADIUS			30.0			//The radius from a point that must not be visible to a player for it to be considered safe for spawning entities.
CONST_INT	MAX_SPAWN_CHECKS				10				//The maximum number of spawn area safety checks to make before simply using the last selected spawn point.
CONST_FLOAT	PANIC_HEIGHT_ABOVE_GROUND		50.0			//The height above ground at which the DJ's friend will start to panic.
CONST_FLOAT	IN_PROXIMITY_VEHICLE			4.0				//The distance within which a ped is considered to be in close proximity to a vehicle.
CONST_FLOAT HOLD_UP_UPDATE_CALL_DISTANCE	300.0			//The distance from the location at which to trigger the update phone call in Hold Up.
CONST_INT	MAX_PED_GROUPS					4				//The maximum number of groups that will be used in a variation.
CONST_FLOAT	IN_PROXIMITY_DJ_YACHT			100.0			//The distance within which a ped is considered to be in close proximity to the DJ yacht.
CONST_FLOAT	IN_PROXIMITY_BMX				4.0				//The distance within which a ped is considered to be in close proximity to a BMX.
CONST_INT	BLIP_FLASH_TIME_LIMIT			1000 * 3		//How much time to flash a blip (3 seconds).

CONST_INT	MAX_TARGETS						27				//The maximum number of peds that can be spawned in a variation.
CONST_INT	MAX_ENEMY_VEH					6				//The maximum number of vehicles that can be spawned in a variation.
CONST_INT	MAX_OBJECTS						4				//The maximum number of objects that can be spawned in a variation.
CONST_INT	MAX_STATIC_EMITTERS				1				//The maximum number of static emitters that can be spawned in a variation.
CONST_INT	MAX_MISSION_ENTITIES			3				//The maximum number of mission entities that can be spawned in a variation.
CONST_INT	MAX_SCENARIO_BLOCKERS			2				//The maximum number of scenario blockers used in a variation.
CONST_INT 	MAX_TRAFFIC_REDUCTION_ZONES		1				//The maximum number of traffic reduction zones in a variation.

ENUM HELP_TEXT_ENUM
	eHT_NONE,
	eHT_POLICE_RAID_APPROACH_0,
	eHT_POLICE_RAID_ATTACK_0,
	eHT_POLICE_RAID_ATTACK_1,
	eHT_RECOVER_PROPERTY_0,
	eHT_STOLEN_VINYLS_COLLECT_0,
	eHT_STOLEN_VINYLS_COLLECT_1,
	eHT_COLLECT_FRIENDS_CHAUFFEUR_0,
	eHT_COLLECT_FRIENDS_CHAUFFEUR_1,
	eHT_COLLECT_FRIENDS_CHAUFFEUR_2,
	eHT_COLLECT_FRIENDS_CHAUFFEUR_3,
	eHT_MUSIC_EQUIPMENT_STEAL_0,
	eHT_HOLD_UP_ATTACK_0
ENDENUM

ENUM TEXT_MESSAGE_ENUM
	eTM_POLICE_RAID_OPENING,
	eTM_RECOVER_PROPERTY_OPENING,
	eTM_STOLEN_VINYLS_OPENING,
	eTM_COLLECT_FRIENDS_OPENING,
	eTM_MUSIC_EQUIPMENT_OPENING,
	eTM_HOLD_UP_OPENING,
	eTM_POLICE_RAID_UPDATE,
	eTM_RECOVER_PROPERTY_UPDATE,
	eTM_MUSIC_EQUIPMENT_UPDATE,
	eTM_COLLECT_FRIENDS_UPDATE_0,
	eTM_COLLECT_FRIENDS_UPDATE_1,
	eTM_COLLECT_FRIENDS_UPDATE_2,
	
	eTM_END
ENDENUM

ENUM MUSIC_CUE_ENUM
	eMC_DID_MUSIC_CUE_0,
	eMC_DID_MUSIC_CUE_1,
	eMC_DID_MUSIC_CUE_2,
	eMC_DID_MUSIC_CUE_3,
	eMC_DID_MUSIC_CUE_4,
	eMC_DID_MUSIC_CUE_5,
	
	eMC_END
ENDENUM

ENUM MUSIC_TRIGGER_ENUM
	eMT_SET_UP_DONE,
	
	eMT_BTL_DELIVERING_START,
	eMT_BTL_SILENT,
	eMT_BTL_MED_INTENSITY_START,
	eMT_BTL_MED_INTENSITY,
	eMT_BTL_GUNFIGHT,
	eMT_BTL_DELIVERING,
	eMT_BTL_MUSIC_STOP,
	eMT_BTL_FAIL,
	
	eMT_END
ENDENUM

//Ped speech states. A ped might have several possible speech contexts for each state.
ENUM PED_SPEECH_STATE_ENUM
	ePSS_START,
	ePSS_HOSTAGE,
	ePSS_GREETING,
	ePSS_TOUR,
	ePSS_TENSION,
	ePSS_DANGER,
	ePSS_FAIL,
	ePSS_SUCCEED,
	
	ePSS_END
ENDENUM

FUNC STRING GET_PED_SPEECH_STATE_ENUM_STRING(PED_SPEECH_STATE_ENUM eState)
	SWITCH eState
		CASE ePSS_START		RETURN "ePSS_START"
		CASE ePSS_HOSTAGE	RETURN "ePSS_HOSTAGE"
		CASE ePSS_GREETING	RETURN "ePSS_GREETING"
		CASE ePSS_TOUR		RETURN "ePSS_TOUR"
		CASE ePSS_TENSION	RETURN "ePSS_TENSION"
		CASE ePSS_DANGER	RETURN "ePSS_DANGER"
		CASE ePSS_FAIL		RETURN "ePSS_FAIL"
		CASE ePSS_SUCCEED	RETURN "ePSS_SUCCEED"
		
		CASE ePSS_END		RETURN "ePSS_END"
	ENDSWITCH
	
	RETURN "INVALID"
ENDFUNC

//This enumeration represents the speech contexts that will be passed to PLAY_PED_AMBIENT_SPEECH.
ENUM AMBIENT_SPEECH_ENUM
	eAS_INVALID = -1,
	
	eAS_CHAT_STATE,
	eAS_GENERIC_BYE,
	eAS_GENERIC_THANKS,
	eAS_GENERIC_FRIGHTENED_HIGH,
	eAS_GENERIC_FRIGHTENED_MED,
	eAS_GENERIC_HI,
	eAS_GENERIC_HOWS_IT_GOING,
	eAS_GENERIC_SHOCKED_HIGH,
	eAS_GENERIC_SHOCKED_MED,
	eAS_GUN_BEG,
	eAS_PROVOKE_GENERIC,
	
	eAS_END
ENDENUM

//Returns the actual context string for a given AMBIENT_SPEECH_ENUM.
FUNC STRING GET_AMBIENT_SPEECH_CONTEXT_STRING(AMBIENT_SPEECH_ENUM eDialogue)
	SWITCH eDialogue
		CASE eAS_CHAT_STATE					RETURN "CHAT_STATE"
		CASE eAS_GENERIC_BYE				RETURN "GENERIC_BYE"
		CASE eAS_GENERIC_THANKS				RETURN "GENERIC_THANKS"
		CASE eAS_GENERIC_FRIGHTENED_HIGH	RETURN "GENERIC_FRIGHTENED_HIGH"
		CASE eAS_GENERIC_FRIGHTENED_MED		RETURN "GENERIC_FRIGHTENED_MED"
		CASE eAS_GENERIC_HI					RETURN "GENERIC_HI"
		CASE eAS_GENERIC_HOWS_IT_GOING		RETURN "GENERIC_HOWS_IT_GOING"
		CASE eAS_GENERIC_SHOCKED_MED		RETURN "GENERIC_SHOCKED_MED"
		CASE eAS_GENERIC_SHOCKED_HIGH		RETURN "GENERIC_SHOCKED_HIGH"
		CASE eAS_GUN_BEG					RETURN "GUN_BEG"
		CASE eAS_PROVOKE_GENERIC			RETURN "PROVOKE_GENERIC"
	ENDSWITCH
	
	RETURN "INVALID"
ENDFUNC

//Ped speech event struct. This is broadcast when ped = iPed has just played speech = eSpeech.
STRUCT SCRIPT_EVENT_DATA_UPDATE_AMBIENT_SPEECH
	STRUCT_EVENT_COMMON_DETAILS Details
	INT iPed
	PED_SPEECH_STATE_ENUM eState
	AMBIENT_SPEECH_ENUM eSpeech
	BOOL bAcknowledgement
	INT iFrameCount
ENDSTRUCT

//Ped brain states.
ENUM PED_AI_STATE
	PED_AI_STATE_CREATED,
	PED_AI_STATE_AIM_AT_COORD,
	PED_AI_STATE_IN_COVER,
	PED_AI_STATE_COWER,
	PED_AI_STATE_PLAY_SCENARIO,
	PED_AI_STATE_PLAY_ANIM,
	PED_AI_STATE_DEFEND_AREA,
	PED_AI_STATE_DEFEND_IN_CAR,
	PED_AI_STATE_FLEE_PED,
	PED_AI_STATE_FLEE_PED_IN_VEHICLE,
	PED_AI_STATE_FLEE_POINT,
	PED_AI_STATE_ENTER_CAR,
	PED_AI_STATE_LEAVE_CAR,
	PED_AI_STATE_TOUR_ON_FOOT,
	PED_AI_STATE_TOUR_IN_CAR,
	PED_AI_STATE_WANDER_IN_VEHICLE,
	PED_AI_STATE_HOVER_IN_HELI,
	PED_AI_STATE_HELI_ATTACK,
	PED_AI_STATE_GOTO_COORD_ON_FOOT,
	PED_AI_STATE_GOTO_COORD_IN_CAR,
	PED_AI_STATE_GOTO_COORD_IN_HELI,
	PED_AI_STATE_PASSENGER_IN_VEHICLE,
	PED_AI_STATE_FOLLOW_CLOSEST_PLAYER,
	PED_AI_STATE_FADE_OUT,
	PED_AI_STATE_WALK_TO_POINT,
	PED_AI_STATE_RETURN_TO_START,
	PED_AI_STATE_PANIC,
	PED_AI_STATE_CLEANUP,
	PED_AI_STATE_FINISHED
ENDENUM

//Enemy behaviour bits.
CONST_INT ENEMY_PED_BIT_CREATE_ON_FOOT			0
CONST_INT ENEMY_PED_BIT_CREATE_AS_DRIVER		1
CONST_INT ENEMY_PED_BIT_CREATE_AS_FRPASS		2
CONST_INT ENEMY_PED_BIT_CREATE_AS_BLPASS		3
CONST_INT ENEMY_PED_BIT_CREATE_AS_BRPASS		4
CONST_INT ENEMY_PED_BIT_CREATE_AS_XL1PASS		5
CONST_INT ENEMY_PED_BIT_CREATE_AS_XR1PASS		6
CONST_INT ENEMY_PED_BIT_CREATE_AS_XL2PASS		7
CONST_INT ENEMY_PED_BIT_CREATE_AS_XR2PASS		8
CONST_INT ENEMY_PED_BIT_CREATE_AS_XL3PASS		9
CONST_INT ENEMY_PED_BIT_CREATE_AS_XR3PASS		10
CONST_INT ENEMY_PED_BIT_HELI_PILOT				11
CONST_INT ENEMY_PED_BIT_HELI_PASS				12
CONST_INT ENEMY_PED_BIT_DRIVE_TO_COORD			13
CONST_INT ENEMY_PED_BIT_FLY_TO_COORD			14
CONST_INT ENEMY_PED_BIT_REACHED_ON_FOOT_COORD	15
CONST_INT ENEMY_PED_BIT_REACHED_DRIVE_TO_COORD	16
CONST_INT ENEMY_PED_BIT_HAS_MARKER				17
CONST_INT ENEMY_PED_BIT_VISION_CONE				18
CONST_INT ENEMY_PED_BIT_IS_CORRUPT_COP			19
CONST_INT ENEMY_PED_BIT_IN_COVER				20
CONST_INT ENEMY_PED_BIT_AIM_AT_COORD			21
CONST_INT ENEMY_PED_BIT_DEFEND_START_LOC		22
CONST_INT ENEMY_PED_BIT_USE_DEFENSIVE_AREA		23
CONST_INT ENEMY_PED_BIT_USE_DEFENSIVE_SPHERE	24
CONST_INT ENEMY_PED_BIT_HAS_SCENARIO			25
CONST_INT ENEMY_PED_BIT_IS_COWERING				26
CONST_INT ENEMY_PED_BIT_IS_FRIENDLY				27
CONST_INT ENEMY_PED_BIT_FIGHT_UNARMED			28
CONST_INT ENEMY_PED_BIT_FLEE_IN_COMBAT			29
CONST_INT ENEMY_PED_BIT_WANDER_IN_VEHICLE		30
CONST_INT ENEMY_PED_BIT_HAS_ANIM				31

//Enemy vehicle status bits.
CONST_INT ENEMY_VEH_BIT_ATTACKED_BY_PLAYER		0
CONST_INT ENEMY_VEH_BIT_DO_NEARBY_PLAYER_CHECK	1
CONST_INT ENEMY_VEH_BIT_PLAYER_NEARBY			2
CONST_INT ENEMY_VEH_BIT_USE_EXACT_COORDS		3

//Mission entity bits.
CONST_INT MISSION_ENTITY_BIT_CREATED					0
CONST_INT MISSION_ENTITY_BIT_HELD						1
CONST_INT MISSION_ENTITY_BIT_HELD_BY_OTHER				2
CONST_INT MISSION_ENTITY_BIT_DELIVERED					3
CONST_INT MISSION_ENTITY_BIT_DESTROYED					4
CONST_INT MISSION_ENTITY_BIT_TICKER_DROPPED_PRINTED		5
CONST_INT MISSION_ENTITY_BIT_TICKER_COLLECTED_PRINTED	6
CONST_INT MISSION_ENTITY_BIT_TICKER_DELIVERED_PRINTED	7

//Game states.
CONST_INT GAME_STATE_INIT 		0
CONST_INT GAME_STATE_RUNNING	1
CONST_INT GAME_STATE_END		2

//Mission stage enumeration.
ENUM FMBB_DEFEND_STAGE
	eFDS_INIT,
	eFDS_APPROACH,
	eFDS_ATTACK,
	eFDS_FIND,
	eFDS_STEAL,
	eFDS_COLLECT,
	eFDS_ESCORT,
	eFDS_CHAUFFEUR,
	eFDS_OFF_MISSION,
	eFDS_WARP,
	eFDS_END
ENDENUM

//Mission end reasons.
ENUM FMBB_DEFEND_END_REASON
	eFDER_NO_REASON_YET,
	eFDER_BOSS_LAUNCHED_QUIT,
	eFDER_TIMEOUT_EXPIRED,
	eFDER_RAID_STOPPED,
	eFDER_ALL_BUSTED,
	eFDER_MISSION_ENTITIES_DELIVERED,
	eFDER_MISSION_ENTITY_DESTROYED,
	eFDER_HOSTAGE_KILLED,
	eFDER_PANIC_TIME_EXPIRED,
	eFDER_ALL_ESCORTED
ENDENUM

//Mission entity types.
ENUM FMBB_DEFEND_MISSION_ENTITY_TYPE
	eFDMET_NONE,
	eFDMET_OBJ,
	eFDMET_VEH
ENDENUM

//Collection type (for telemetry).
ENUM FMBB_DEFEND_COLLECTION_TYPE
	eFDCT_NA = -1,	//Not applicable.
	eFDCT_OBJECT,	//Object.
	eFDCT_VEHICLE,	//Vehicle.
	eFDCT_PED		//Ped.
ENDENUM

//----------------------
//	DATA
//----------------------
STRUCT structTarget
	NETWORK_INDEX niPed
	NETWORK_INDEX niVeh
	
	VECTOR vSpawnCoord
	FLOAT fSpawnHead
	INT iServerTargetBitset
	INT iCheckpointStage
	INT iCheckpointOnRouteTo = -1
	INT iGroup = -1
	PED_AI_STATE ePedState
	PED_SPEECH_STATE_ENUM eSpeechState
	INT iPedDoneSpeech //A bitset indexed using AMBIENT_SPEECH_ENUM to determine whether a context has already been played.
	SCRIPT_TIMER timeSpeechTimer //A timer for the ped's ambient speech.
ENDSTRUCT

STRUCT structDefendSpawnCoords
	VECTOR vLoc
	FLOAT fHead
	BOOL bUsed
ENDSTRUCT

//Server bitset data.
CONST_INT biS_ServerDataInitialised		0
CONST_INT biS_AllPlayersFinished		1
CONST_INT biS_ApproachedDestination		2
CONST_INT biS_AllTargetsKilled			3
CONST_INT biS_SecondWaveCreated			4
CONST_INT biS_PlayerInArea				5
CONST_INT biS_WarningAreaTriggered		6
CONST_INT biS_PlayerAtCollectPoint		7
CONST_INT biS_CollectedFriends			8
CONST_INT biS_FinishedWarp				9
CONST_INT biS_AllPedsShouldReact		10
CONST_INT biS_HelicoptersUnfrozen		11
CONST_INT biS_SafeToFadePeds			12

//Server broadcast data.
STRUCT ServerBroadcastData
	#IF IS_DEBUG_BUILD
	BOOL bSomeoneSPassed
	#ENDIF
	
	INT iMatchId1
	INT iMatchId2
	INT iMissionType
	
	INT iVariation = -1
	INT iSubVariation = -1
	
	INT iBossPartWhoLaunched = -1
	INT iBossPlayerWhoLaunched = -1
	
	FMBB_DEFEND_STAGE eStage = eFDS_INIT
	FMBB_DEFEND_END_REASON eEndReason = eFDER_NO_REASON_YET
	INT iServerGameState = GAME_STATE_INIT
	
	structTarget Targets[MAX_TARGETS]
	structDefendSpawnCoords pedSpawnCoords[MAX_TARGETS]
	NETWORK_INDEX niVeh[MAX_ENEMY_VEH]
	structDefendSpawnCoords enemyVehSpawnCoords[MAX_ENEMY_VEH]
	NETWORK_INDEX niProp[MAX_OBJECTS]
	structDefendSpawnCoords propSpawnCoords[MAX_OBJECTS]
	
	INT iServerBitSet
	INT iEnemyVehBitset[MAX_ENEMY_VEH]
	
	INT iPedBeingCreated = -1
	
	INT iTargetAimedAtBitSet
	INT iTargetNearbyShotsBitSet
	INT iTargetSpookedBitSet
	INT iTargetSpookedByAreaCheckBitSet
	
	INT iTargetKilledBitSet
	INT iLockDoorsBitSet
	INT iVehClearAreaBitSet
	INT iPropClearAreaBitSet
	INT iPedClearAreaBitSet
	
	INT iNumTargetsKilled
	
	FMBB_DEFEND_MISSION_ENTITY_TYPE eMissionEntityType = eFDMET_NONE
	NETWORK_INDEX niMissionEntities[MAX_MISSION_ENTITIES]
	INT iMissionEntityBitset[MAX_MISSION_ENTITIES]
	PLAYER_INDEX piMissionEntityPlayerHolder[MAX_MISSION_ENTITIES]
	INT iMissionEntityPedHolder[MAX_MISSION_ENTITIES]
	INT iRandomMissionEntity
	
	STRUCT_GANG_CHASE_SERVER sGangChaseServer
	
	SCRIPT_TIMER timeTimeout
	SCRIPT_TIMER timeKillFailsafe[MAX_TARGETS]
	SCRIPT_TIMER timeSpawnProps
	SCRIPT_TIMER timePanic
	SCRIPT_TIMER timeCooldown
	SCRIPT_TIMER timeWarp
	SCRIPT_TIMER timeIplDelay
	SCRIPT_TIMER timeAmbientSpeechTimer
	
	INT iPanicLevel
	INT iLastPanicLevelReached
	
	INT iGroupReactBitSet
	
	INT iOrganisationSizeOnLaunch //Number of members in the organisation, including the boss (for telemetry).
	
	INT iCurrentGoto = 0
ENDSTRUCT
ServerBroadcastData serverBD

CONST_INT biP_HudOkForSCTV			0
CONST_INT biP_Finished				1
CONST_INT biP_ApproachedDestination	2
CONST_INT biP_Busted				3
CONST_INT biP_WarningAreaTriggered	4
CONST_INT biP_PlayerAtCollectPoint	5
CONST_INT biP_CollectedFriends		6
CONST_INT biP_Warping				7
CONST_INT biP_WarpingDone			8
CONST_INT biP_AllPedsShouldReact	9
CONST_INT biP_HelicoptersUnfrozen	10
CONST_INT biP_SafeToFadePeds		11

//Player broadcast data.
STRUCT PlayerBroadcastData
	#IF IS_DEBUG_BUILD
	INT iPDebugBitSet
	#ENDIF
	
	INT iMyBossPart = -1
	
	FMBB_DEFEND_STAGE eStage = eFDS_INIT
	INT iClientGameState = GAME_STATE_INIT
	
	INT iPlayerBitSet
	
	INT iPlayerAimAtTargetBitSet
	INT iPlayerShotsNearTargetBitSet
	INT iPlayerTargetSpookedBitSet
	INT iPlayerTargetSpookedByAreaCheckBitSet
	
	INT iPlayerKilledTargetBitSet
	INT iNonPlayerKilledTargetBitSet
	INT iNonPartPlayerKilledTargetBitSet
	
	INT iPlayerTargetVehDamagedBitSet
	INT iPlayerTargetVehNearbyBitSet
	
	INT iEnemyPedReachedInCarGoto
	INT iEnemyPedReachedOnFootGoto
	
	INT iMissionEntityBitSet[MAX_MISSION_ENTITIES]
	
	INT iGroupReactBitSet
	
	INT iCurrentGoto = 0
ENDSTRUCT
PlayerBroadcastData playerBD[GB_MAX_GANG_SIZE_INCLUDING_BOSS]

//Local mission variables.
//Bit sets.
INT iLocalBitSet
CONST_INT biL_DoneIntroShard		0
CONST_INT biL_WaitForShard			1
CONST_INT biL_BlippedRival			2
CONST_INT biL_DoneEndTelemetry		3
CONST_INT biL_ShouldResetWanted		4
CONST_INT biL_ResetWanted			5
CONST_INT biL_DoneStartTelemetry	6
CONST_INT biL_RewardsProcessed		7
CONST_INT biL_NoNightclubWarp		8
CONST_INT biL_IsEscort				9
CONST_INT biL_PedsHaveEscort		10
CONST_INT biL_UsingCustomSpawns		11
CONST_INT biL_UpdatedCustomSpawns	12
CONST_INT biL_DonePassFailCall		13
CONST_INT biL_DoneInvincibility		14
CONST_INT biL_ReservedEntities		15
CONST_INT biL_CreatedInitialTargets	16
CONST_INT biL_ShowIplMap			17
CONST_INT biL_StaticEmittersCreated	18
CONST_INT biL_HideBlipRoute			19
CONST_INT biL_TriggeredOpeningCall	20
CONST_INT biL_FinishedOpeningCall	21
CONST_INT biL_FinishedUpdateCall	22
CONST_INT biL_DoneCommonSetup		23
CONST_INT biL_TempBlockTextMessage	24
CONST_INT biL_DoneSeatingPrefs		25

INT iMusicTriggerBitSet
INT iMusicCueBitSet
INT iMusicTrigger
INT iStaticEmitterEnabledBitSet

INT iLocalHelpBitSet

INT iTextMessageBitSet

INT iLocalSpeechStateBitSet[MAX_TARGETS] //A local bitset indicating which peds/states this client has played ambient speech for.

//Relationship groups.
REL_GROUP_HASH relDefendPlayer
REL_GROUP_HASH relMyFmGroup

//Contraband transaction.
CONTRABAND_TRANSACTION_STATE eResult

//Rewards data.
GANG_BOSS_MANAGE_REWARDS_DATA gbRewards

GB_MAINTAIN_SPECTATE_VARS sSpecVars
GB_COUNTDOWN_MUSIC_STRUCT cmStruct

//Blips.
AI_BLIP_STRUCT pedBlipData[MAX_TARGETS]
BLIP_INDEX blipTarget[MAX_TARGETS]
BLIP_INDEX blipEnemyVeh[MAX_ENEMY_VEH]
BLIP_INDEX blipMissionEntities[MAX_MISSION_ENTITIES]
BLIP_INDEX blipDropOff
BLIP_INDEX blipDestination

SCENARIO_BLOCKING_INDEX sbiScenarioBlockingIndices[MAX_SCENARIO_BLOCKERS]
INT iTrafficReductionIndices[MAX_TRAFFIC_REDUCTION_ZONES]

STRUCT_GANG_CHASE_LOCAL sGangChaseLocal
STRUCT_GANG_CHASE_OPTIONS sGangChaseOptions

GROUP_INDEX giPlayerGroup

SCRIPT_TIMER stKillStripTimer
SCRIPT_TIMER stWarningArea
SCRIPT_TIMER stMissionEnd
SCRIPT_TIMER stInvincibility
SCRIPT_TIMER stWarpDelay
SCRIPT_TIMER stTextMessageDelay
SCRIPT_TIMER stAlertDelay
SCRIPT_TIMER stPhonecallDelay
SCRIPT_TIMER stBlipFlash

INT iHeliSpawnChecks
INT iSpawnAlternatives[MAX_ENEMY_VEH]
INT iWarpIntoNightclubStage = 0
INT iPedsAvailableForEscort = 0
INT iPedsBeingEscorted = 0
INT iOpeningCallFinishedTrackingStage = 0

PICKUP_MAP_EXIT_WARPING_DATA sPickupMapExitWarpData

OBJECT_INDEX obStaticEmitter[MAX_STATIC_EMITTERS]

SCRIPT_TIMER stOpeningCallTimeout
structPedsForConversation MyLocalPedStruct

INT iTargetsKilled = 0

//----------------------
//	MAIN PROCEDURES
//----------------------
//Returns the number of variations in this script.
FUNC INT GET_NUMBER_OF_VARIATIONS()
	RETURN 6
ENDFUNC

//Given the variation, returns its number of subvariations. If the variation is not specified, returns the number of subvariations for the current variation.
FUNC INT GET_NUMBER_OF_SUBVARIATIONS(INT iVariation = -1)
	IF iVariation = -1
		iVariation = serverBD.iVariation
	ENDIF
	
	SWITCH iVariation
		CASE	FMBB_DEFEND_POLICE_RAID			RETURN 10
		CASE	FMBB_DEFEND_RECOVER_PROPERTY	RETURN 10
		CASE	FMBB_DEFEND_STOLEN_VINYLS		RETURN 1
		CASE	FMBB_DEFEND_COLLECT_FRIENDS		RETURN 3
		CASE	FMBB_DEFEND_MUSIC_EQUIPMENT		RETURN 3
		CASE	FMBB_DEFEND_HOLD_UP				RETURN 3
	ENDSWITCH
	
	RETURN 0
ENDFUNC

//Returns TRUE if the current variation is one of the phone call missions. Returns FALSE if it is one of the defends.
FUNC BOOL IS_THIS_VARIATION_A_PHONECALL_MISSION()
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_STOLEN_VINYLS
		CASE FMBB_DEFEND_COLLECT_FRIENDS
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT
		CASE FMBB_DEFEND_HOLD_UP	
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

//For the defends, returns the subvariation associated with gang boss' currently owned Nightclub.
FUNC INT GET_SUBVARIATION_FOR_NIGHTCLUB()
	NIGHTCLUB_ID niNightclub = GET_PLAYERS_OWNED_NIGHTCLUB(GB_GET_LOCAL_PLAYER_GANG_BOSS())
	
	SWITCH niNightclub
		CASE NIGHTCLUB_LA_MESA				RETURN 0
		CASE NIGHTCLUB_MISSION_ROW			RETURN 1
		CASE NIGHTCLUB_STRAWBERRY_WAREHOUSE	RETURN 2
		CASE NIGHTCLUB_WEST_VINEWOOD		RETURN 3
		CASE NIGHTCLUB_CYPRESS_FLATS		RETURN 4
		CASE NIGHTCLUB_LSIA_WAREHOUSE		RETURN 5
		CASE NIGHTCLUB_ELYSIAN_ISLAND		RETURN 6
		CASE NIGHTCLUB_DOWNTOWN_VINEWOOD	RETURN 7
		CASE NIGHTCLUB_DEL_PERRO_BUILDING	RETURN 8
		CASE NIGHTCLUB_VESPUCCI_CANALS		RETURN 9
	ENDSWITCH
	
	RETURN -1
ENDFUNC

//With iWave = -1, the number of peds in total is returned. Otherwise, the number of peds in the wave is returned.
FUNC INT GET_NUMBER_OF_PEDS(INT iWave = -1)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_POLICE_RAID
			SWITCH iWave
				CASE	-1	RETURN	14
				CASE	0	RETURN	7
				CASE	1	RETURN	7
			ENDSWITCH
		BREAK
		
		CASE	FMBB_DEFEND_RECOVER_PROPERTY	RETURN	7
		CASE	FMBB_DEFEND_STOLEN_VINYLS		RETURN	27
		CASE	FMBB_DEFEND_COLLECT_FRIENDS		RETURN	3
		CASE	FMBB_DEFEND_MUSIC_EQUIPMENT		RETURN	4
		CASE	FMBB_DEFEND_HOLD_UP				RETURN	7
	ENDSWITCH
	
	RETURN 0
ENDFUNC

//Returns the number of waves of enemy peds in the current variation.
FUNC INT GET_NUMBER_OF_WAVES()
	SWITCH serverBD.iVariation
		CASE	FMBB_DEFEND_POLICE_RAID		RETURN	2
	ENDSWITCH
	
	RETURN 1
ENDFUNC

//With iWave = -1, the number of vehicles in total is returned. Otherwise, the number of vehicles in the wave is returned.
FUNC INT GET_NUMBER_OF_ENEMY_VEHS(INT iWave = -1)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_POLICE_RAID
			SWITCH iWave
				CASE	-1	RETURN	5
				CASE	0	RETURN	3
				CASE	1	RETURN	2
			ENDSWITCH
		BREAK
		
		CASE	FMBB_DEFEND_RECOVER_PROPERTY	RETURN	4
		CASE	FMBB_DEFEND_STOLEN_VINYLS		RETURN	5
		CASE	FMBB_DEFEND_HOLD_UP				RETURN	2
	ENDSWITCH
	
	RETURN 0
ENDFUNC

//Returns the number of prop objects in the current variation.
FUNC INT GET_NUMBER_OF_PROPS()
	SWITCH serverBD.iVariation
		CASE	FMBB_DEFEND_POLICE_RAID		RETURN	3
		CASE	FMBB_DEFEND_STOLEN_VINYLS	RETURN	3
		CASE	FMBB_DEFEND_MUSIC_EQUIPMENT	RETURN	3
	ENDSWITCH
	
	RETURN 0
ENDFUNC

//Returns the number of mission entities in the current variation.
FUNC INT GET_NUMBER_OF_MISSION_ENTITIES()
	SWITCH serverBD.iVariation
		CASE	FMBB_DEFEND_RECOVER_PROPERTY	RETURN	3
		CASE	FMBB_DEFEND_STOLEN_VINYLS		RETURN	1
		CASE	FMBB_DEFEND_COLLECT_FRIENDS		RETURN	1
		CASE	FMBB_DEFEND_MUSIC_EQUIPMENT		RETURN	1
	ENDSWITCH
	
	RETURN 0
ENDFUNC

//Returns the total number of help messages used in the current variation.
FUNC INT GET_NUMBER_OF_HELP_MESSAGES()
	SWITCH serverBD.iVariation
		CASE	FMBB_DEFEND_POLICE_RAID			RETURN	2
		CASE	FMBB_DEFEND_RECOVER_PROPERTY	RETURN	1
		CASE	FMBB_DEFEND_STOLEN_VINYLS		RETURN	2
		CASE	FMBB_DEFEND_COLLECT_FRIENDS		RETURN	2
		CASE	FMBB_DEFEND_HOLD_UP				RETURN	1
	ENDSWITCH
	
	RETURN 0
ENDFUNC

//Returns the length of time (in milliseconds) before the variation expires.
FUNC INT GET_TIMEOUT()
	INT iTimeout = 30
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_DefendPhoneCallTimeout")
		iTimeout = GET_COMMANDLINE_PARAM_INT("sc_DefendPhoneCallTimeout")
		PRINTLN("[FMBB_DEFEND] - [GET_TIMEOUT] Timeout has been set by the commandline, sc_DefendPhoneCallTimeout = ", iTimeout, " seconds.")
		RETURN iTimeout * 1000 //The commandline parameter is in seconds.
	ENDIF
	#ENDIF
	
	SWITCH serverBD.iVariation
		CASE	FMBB_DEFEND_POLICE_RAID			iTimeout = g_sMPTunables.iBB_DEFEND_MISSIONS_DURATION_POLICE_RAID			BREAK
		CASE	FMBB_DEFEND_RECOVER_PROPERTY	iTimeout = g_sMPTunables.iBB_DEFEND_MISSIONS_DURATION_RECOVER_PROPERTY		BREAK
		
		CASE	FMBB_DEFEND_STOLEN_VINYLS		iTimeout = g_sMPTunables.iBB_PHONECALL_MISSIONS_DURATION_STOLEN_VINYLS		BREAK
		CASE	FMBB_DEFEND_COLLECT_FRIENDS		iTimeout = g_sMPTunables.iBB_PHONECALL_MISSIONS_DURATION_COLLECT_FRIENDS	BREAK
		CASE	FMBB_DEFEND_MUSIC_EQUIPMENT		iTimeout = g_sMPTunables.iBB_PHONECALL_MISSIONS_DURATION_MUSIC_EQUIPMENT	BREAK
		CASE	FMBB_DEFEND_HOLD_UP				iTimeout = g_sMPTunables.iBB_PHONECALL_MISSIONS_DURATION_HOLD_UP			BREAK
	ENDSWITCH
	
	RETURN iTimeout * 60 * 1000 //The tunables are in minutes.
ENDFUNC

//Get the difficulty (max wanted level, AI ped accuracy etc.) for the variation. Based on the values used for the Biker DLC pack.
FUNC INT GET_DIFFICULTY()
	PRINTLN("[FMBB_DEFEND] - [GET_DIFFICULTY]")
	
	SWITCH serverBD.iVariation
		CASE	FMBB_DEFEND_POLICE_RAID
		DEFAULT							RETURN	DIFFICULTY_HARD
	ENDSWITCH
ENDFUNC

//Returns the wanted level cap for the current variation.
FUNC INT GET_MAX_WANTED_LEVEL_FOR_VARIATION()
	PRINTLN("[FMBB_DEFEND] - [GET_MAX_WANTED_LEVEL]")
	
	SWITCH serverBD.iVariation
		CASE	FMBB_DEFEND_POLICE_RAID		RETURN 3 //3 star wanted level cap.
		DEFAULT								RETURN 2 //2 star wanted level cap.
	ENDSWITCH
ENDFUNC

//Returns the radius in metres of the area to be cleared around the mission location at the start of the mission.
FUNC FLOAT GET_CLEAR_AREA_RADIUS()
	PRINTLN("[FMBB_DEFEND] - [GET_CLEAR_AREA_RADIUS]")
	
	SWITCH serverBD.iVariation
		CASE	FMBB_DEFEND_POLICE_RAID			RETURN 70.0
		CASE	FMBB_DEFEND_RECOVER_PROPERTY	RETURN 0.0
		CASE	FMBB_DEFEND_COLLECT_FRIENDS		RETURN 0.0
		CASE	FMBB_DEFEND_HOLD_UP				RETURN 50.0
		DEFAULT									RETURN 30.0
	ENDSWITCH
ENDFUNC

FUNC INT GET_NUMBER_OF_GOTOS()
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_STOLEN_VINYLS	RETURN	2
	ENDSWITCH
	
	RETURN 1
ENDFUNC

//Returns the location of the action area.
FUNC VECTOR GET_LOCATION(INT iVariation = -1, INT iSubVariation = -1, INT iGoto = -1)
	IF iVariation = -1
		iVariation = serverBD.iVariation
	ENDIF
	
	IF iSubVariation = -1
		iSubVariation = serverBD.iSubVariation
	ENDIF
	
	SWITCH iVariation
		CASE FMBB_DEFEND_POLICE_RAID
			SWITCH iSubVariation
				CASE 0	RETURN	<<718.5640, -1289.8290, 25.1160>>
				CASE 1	RETURN	<<330.1026, -1001.0793, 28.2801>>
				CASE 2	RETURN	<<-163.8086, -1300.1824, 30.2642>>
				CASE 3	RETURN	<<-22.8591, 214.9007, 105.5648>>
				CASE 4	RETURN	<<909.0583, -2094.7737, 29.5154>>
				CASE 5	RETURN	<<-681.9150, -2379.3401, 12.9460>>
				CASE 6	RETURN	<<210.4602, -3115.2461, 4.7903>>
				CASE 7	RETURN	<<377.8100, 228.4226, 102.0406>>
				CASE 8	RETURN	<<-1227.9430, -692.0988, 22.4340>>
				CASE 9	RETURN	<<-1165.4460, -1179.5712, 4.6235>>
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_STOLEN_VINYLS
			//Stolen Vinyl uses a waypoint on the way to the yacht, so use that as the first goto and return the yacht co-ordinate when non is specified.
			SWITCH iGoto
				CASE 0	RETURN <<-1590.2136, -878.8138, 9.0391>>
				
				CASE -1
				CASE 1	RETURN <<-2017.8246, -1039.9008, 1.4467>>
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_COLLECT_FRIENDS
			SWITCH iGoto
				CASE 0	RETURN <<-1145.0938, -2719.9539, 12.9570>>
				
				CASE -1
				CASE 1
					SWITCH iSubVariation
						CASE 0	RETURN	<<747.0380, 2520.4009, 72.1710>>
						CASE 1	RETURN	<<1405.1570, 3592.5488, 33.9241>>
						CASE 2	RETURN	<<2630.5552, 3272.4561, 54.2218>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT
			SWITCH iSubVariation
				CASE 0	RETURN	<<-952.2560, -303.1360, 37.7200>>
				CASE 1	RETURN	<<480.7710, -1541.7469, 28.0210>>
				CASE 2	RETURN	<<1964.1980, 3047.4150, 45.8830>>
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_HOLD_UP
			SWITCH iSubVariation
				CASE 0	RETURN	<<-1118.3199, -1606.0060, 3.3560>>
				CASE 1	RETURN	<<2013.7310, 3803.9861, 31.2810>>
				CASE 2	RETURN	<<52.1110, -1907.5500, 20.5930>>
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN <<0, 0, 0>>
ENDFUNC

//Get the model name for the specified ped.
FUNC MODEL_NAMES GET_PED_MODEL(INT iPed)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_POLICE_RAID
			SWITCH serverBD.iSubVariation
				CASE 0
				DEFAULT
					SWITCH iPed
						CASE 0
						DEFAULT	RETURN S_M_M_CIASEC_01
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_RECOVER_PROPERTY
			SWITCH serverBD.iSubVariation
				CASE 0
				CASE 6
				RETURN G_M_Y_AZTECA_01
				
				CASE 1
				CASE 4
				CASE 8
				CASE 9
				RETURN G_M_Y_MEXGOON_02
				
				CASE 2
				RETURN G_M_Y_BALLAORIG_01
				
				CASE 3
				RETURN G_M_Y_SALVAGOON_01
				
				CASE 5
				RETURN G_M_Y_FAMCA_01
				
				CASE 7
				RETURN G_M_Y_BALLAEAST_01
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_STOLEN_VINYLS
			SWITCH serverBD.iSubVariation
				CASE 0
				DEFAULT
					SWITCH iPed
						CASE 3
						CASE 4
						CASE 10
						CASE 11
						CASE 15
						CASE 20
						CASE 21	RETURN A_M_Y_BEACH_03
						
						CASE 6
						CASE 13
						CASE 16
						CASE 17
						CASE 24	RETURN A_M_Y_BEACH_01
						
						CASE 26	RETURN A_F_Y_JUGGALO_01
						
						DEFAULT RETURN A_F_Y_BEACH_01
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_COLLECT_FRIENDS
			SWITCH GET_PLAYERS_NIGHTCLUB_ACTIVE_DJ(GB_GET_LOCAL_PLAYER_GANG_BOSS())
				CASE DJ_SOLOMUN
					SWITCH iPed
						CASE 0	RETURN A_M_M_EASTSA_02
						CASE 1	RETURN A_M_M_MALIBU_01
						CASE 2	RETURN S_M_Y_ROBBER_01
					ENDSWITCH
				BREAK
				
				CASE DJ_DIXON
					SWITCH iPed
						CASE 0	RETURN A_M_Y_HIPSTER_03
						CASE 1	RETURN A_F_Y_HIPSTER_01
						CASE 2	RETURN A_M_Y_VINEWOOD_02
					ENDSWITCH
				BREAK
				
				CASE DJ_TALE_OF_US
					SWITCH iPed
						CASE 0	RETURN A_M_Y_VINEWOOD_01
						CASE 1	RETURN A_F_Y_SOUCENT_03
						CASE 2	RETURN A_M_Y_HIPSTER_01
					ENDSWITCH
				BREAK
				
				CASE DJ_BLACK_MADONNA
					SWITCH iPed
						CASE 0	RETURN A_F_Y_BEVHILLS_03
						CASE 1	RETURN A_F_Y_HIPSTER_04
						CASE 2	RETURN A_M_Y_EASTSA_01
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT
			SWITCH serverBD.iSubVariation
				CASE 0
					SWITCH iPed
						CASE 0	RETURN A_M_Y_VINEWOOD_03
						CASE 1	RETURN S_M_M_SECURITY_01
						CASE 2	RETURN S_M_M_SECURITY_01
						CASE 3	RETURN S_M_M_SECURITY_01
					ENDSWITCH
				BREAK
				
				CASE 1
					SWITCH iPed
						CASE 0	RETURN A_M_Y_VINEWOOD_03
						CASE 1	RETURN S_M_M_BOUNCER_01
						CASE 2	RETURN S_M_M_BOUNCER_01
						CASE 3	RETURN S_M_M_SECURITY_01
					ENDSWITCH
				BREAK
				
				CASE 2
					SWITCH iPed
						CASE 0	RETURN A_M_Y_VINEWOOD_03
						CASE 1	RETURN S_M_M_SECURITY_01
						CASE 2	RETURN S_M_M_BOUNCER_01
						CASE 3	RETURN S_M_M_BOUNCER_01
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_HOLD_UP
			SWITCH iPed
				CASE 0
					SWITCH GET_PLAYERS_NIGHTCLUB_ACTIVE_DJ(GB_GET_LOCAL_PLAYER_GANG_BOSS())
						CASE DJ_SOLOMUN			RETURN	A_M_M_EASTSA_02
						CASE DJ_DIXON			RETURN	A_M_Y_HIPSTER_03
						CASE DJ_TALE_OF_US		RETURN	A_M_Y_VINEWOOD_01
						CASE DJ_BLACK_MADONNA	RETURN	A_F_Y_BEVHILLS_03
					ENDSWITCH
				BREAK
				
				CASE 1
					SWITCH GET_PLAYERS_NIGHTCLUB_ACTIVE_DJ(GB_GET_LOCAL_PLAYER_GANG_BOSS())
						CASE DJ_SOLOMUN			RETURN	A_M_M_MALIBU_01
						CASE DJ_DIXON			RETURN	A_M_Y_VINEWOOD_02
						CASE DJ_TALE_OF_US		RETURN	A_M_Y_HIPSTER_01
						CASE DJ_BLACK_MADONNA	RETURN	A_M_Y_EASTSA_01
					ENDSWITCH
				BREAK
				
				DEFAULT
					SWITCH serverBD.iSubVariation
						CASE 0	RETURN G_M_Y_MEXGOON_02
						CASE 1	RETURN A_M_M_HILLBILLY_02
						CASE 2	RETURN G_M_Y_BALLAORIG_01
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

//Get the model name for the specified vehicle.
FUNC MODEL_NAMES GET_ENEMY_VEH_MODEL(INT iVeh)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_POLICE_RAID
			SWITCH serverBD.iSubVariation
				CASE 0
				DEFAULT
					SWITCH iVeh
						CASE 0	RETURN POLICE4
						CASE 1	RETURN POLICE4
						CASE 2	RETURN POLMAV
						CASE 3	RETURN GRANGER
						CASE 4	RETURN GRANGER
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_RECOVER_PROPERTY
			SWITCH serverBD.iSubVariation
				CASE 0
				CASE 5
					SWITCH iVeh
						CASE 0
						CASE 1
						CASE 2	RETURN BMX
						CASE 3	RETURN EMPEROR
					ENDSWITCH
				BREAK
				
				CASE 1
					SWITCH iVeh
						CASE 0
						CASE 1
						CASE 2	RETURN BMX
						CASE 3	RETURN MANANA
					ENDSWITCH
				BREAK
				
				CASE 2
				CASE 7
					SWITCH iVeh
						CASE 0
						CASE 1
						CASE 2	RETURN BMX
						CASE 3	RETURN BUCCANEER
					ENDSWITCH
				BREAK
				
				CASE 3
				CASE 4
					SWITCH iVeh
						CASE 0
						CASE 1
						CASE 2	RETURN BMX
						CASE 3	RETURN TORNADO2
					ENDSWITCH
				BREAK
				
				CASE 6
					SWITCH iVeh
						CASE 0
						CASE 1
						CASE 2	RETURN BMX
						CASE 3	RETURN PHOENIX
					ENDSWITCH
				BREAK
				
				CASE 8
				CASE 9
					SWITCH iVeh
						CASE 0
						CASE 1
						CASE 2	RETURN BMX
						CASE 3	RETURN PEYOTE
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_STOLEN_VINYLS
			SWITCH iVeh
				CASE 0	RETURN SEASHARK
				CASE 1	RETURN SEASHARK
				CASE 2	RETURN SEASPARROW
				CASE 3	RETURN SEASHARK
				CASE 4	RETURN SEASHARK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_HOLD_UP
			SWITCH serverBD.iSubVariation
				CASE 0
					SWITCH iVeh
						CASE 0	RETURN EMPEROR
						CASE 1	RETURN MANANA
					ENDSWITCH
				BREAK
				
				CASE 1
					SWITCH iVeh
						CASE 0	RETURN SANCHEZ
						CASE 1	RETURN BODHI2
					ENDSWITCH
				BREAK
				
				CASE 2	RETURN EMPEROR
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

//Get the model name for the specified prop. If no prop is specified, this will return the either the only model for the variation, if available.
FUNC MODEL_NAMES GET_PROP_MODEL(INT iProp = -1)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_POLICE_RAID		RETURN	PROP_BARRIER_WORK05
		
		CASE FMBB_DEFEND_STOLEN_VINYLS
			SWITCH iProp
				CASE 0
				CASE 1	RETURN	PROP_SPEAKER_07
				CASE 2	RETURN	INT_TO_ENUM(MODEL_NAMES, HASH("BA_PROP_BATTLE_DJ_STAND"))
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT	RETURN	EX_PROP_ADV_CASE_SM_02
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

//Spawn locations for peds.
PROC INIT_PED_SPAWN_COORDS(INT iPed, VECTOR &vLoc, FLOAT &fHeading)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_POLICE_RAID
			SWITCH serverBD.iSubVariation
				CASE 0
					SWITCH iPed
						//First wave.
						CASE 0	vLoc = <<727.8980, -1289.8340, 55.3110>>	fHeading = 147.3970	BREAK //Heli pilot.
						CASE 1	vLoc = <<728.3730, -1290.3030, 55.1140>>	fHeading = 147.3970	BREAK //Heli passenger.
						CASE 2	vLoc = <<717.7190, -1289.8409, 25.0710>>	fHeading = 254.3970	BREAK
						CASE 3	vLoc = <<721.7910, -1281.2010, 25.2780>>	fHeading = 235.7970	BREAK
						CASE 4	vLoc = <<719.5780, -1298.5601, 25.1270>>	fHeading = 308.7970	BREAK
						CASE 5	vLoc = <<728.0100, -1290.3240, 55.1490>>	fHeading = 342.1960	BREAK //Heli passenger.
						CASE 6	vLoc = <<716.9250, -1284.4440, 25.0710>>	fHeading = 251.5960	BREAK
						
						//Second wave.
						CASE 7	vLoc = <<779.7355, -1260.4210, 27.0752>>	fHeading = 174.1942	BREAK //Granger driver.
						CASE 8	vLoc = <<779.3478, -1261.1489, 27.1985>>	fHeading = 174.1942	BREAK //Hanging off right side of Granger.
						CASE 9	vLoc = <<778.7377, -1260.7026, 27.2142>>	fHeading = 174.1942	BREAK //Hanging off left side of Granger.
						CASE 10	vLoc = <<827.6603, -1426.4628, 27.7710>>	fHeading = 174.1942	BREAK //Granger driver.
						CASE 11	vLoc = <<827.5900, -1425.1310, 27.8647>>	fHeading = 174.1942	BREAK //Hanging off right side of Granger.
						CASE 12	vLoc = <<828.9839, -1426.0372, 28.1268>>	fHeading = 174.1942	BREAK //Hanging off left side of Granger.
						CASE 13 vLoc = <<778.3478, -1261.1489, 27.1985>>	fHeading = 2.1964	BREAK //Front passenger seat of Granger.
					ENDSWITCH
				BREAK
				
				CASE 1
					SWITCH iPed
						//First wave.
						CASE 0	vLoc = <<316.2020, -997.8390, 28.1220>>		fHeading = 278.3980	BREAK
						CASE 1	vLoc = <<316.0670, -1004.1480, 28.3070>>	fHeading = 302.3980	BREAK
						CASE 2	vLoc = <<323.7970, -1005.3460, 28.2970>>	fHeading = 309.1990	BREAK
						CASE 3	vLoc = <<346.1420, -998.4220, 28.2830>>		fHeading = 81.7980	BREAK
						CASE 4	vLoc = <<332.8980, -1041.1331, 57.4630>>	fHeading = 357.1970	BREAK //Heli pilot.
						CASE 5	vLoc = <<332.8980, -1041.1331, 57.4630>>	fHeading = 357.1970	BREAK //Heli passenger.
						CASE 6	vLoc = <<332.8980, -1041.1331, 57.4630>>	fHeading = 357.1970	BREAK //Heli passenger.
						
						//Second wave.
						CASE 7	vLoc = <<394.2660, -1015.6960, 28.3060>>	fHeading = 179.1980	BREAK //Granger driver.
						CASE 8	vLoc = <<394.2660, -1015.6960, 28.3060>>	fHeading = 179.1980	BREAK //Hanging off right side of Granger.
						CASE 9	vLoc = <<394.2660, -1015.6960, 28.3060>>	fHeading = 179.1980	BREAK //Hanging off left side of Granger.
						CASE 10	vLoc = <<301.0670, -1088.1071, 28.3530>>	fHeading = 0.3970	BREAK //Granger driver.
						CASE 11	vLoc = <<301.0670, -1088.1071, 28.3530>>	fHeading = 0.3970	BREAK //Front passenger seat of Granger.
						CASE 12	vLoc = <<301.0670, -1088.1071, 28.3530>>	fHeading = 0.3970	BREAK //Hanging off right side of Granger.
						CASE 13 vLoc = <<301.0670, -1088.1071, 28.3530>>	fHeading = 0.3970	BREAK //Hanging off left side of Granger.
					ENDSWITCH
				BREAK
				
				CASE 2
					SWITCH iPed
						//First wave.
						CASE 0	vLoc = <<-160.3620, -1310.2440, 30.3870>>	fHeading = 14.8140	BREAK //Heli pilot.
						CASE 1	vLoc = <<-156.3280, -1309.0590, 30.3280>>	fHeading = 22.0140	BREAK //Heli passenger.
						CASE 2	vLoc = <<-177.0820, -1299.4150, 30.2140>>	fHeading = 291.8140	BREAK
						CASE 3	vLoc = <<-178.5950, -1295.2150, 30.2080>>	fHeading = 271.8130	BREAK
						CASE 4	vLoc = <<-165.5700, -1300.4160, 65.6980>>	fHeading = 271.8130	BREAK
						CASE 5	vLoc = <<-165.5700, -1300.4160, 65.6980>>	fHeading = 271.8130	BREAK
						CASE 6	vLoc = <<-165.5700, -1300.4160, 65.6980>>	fHeading = 271.8130	BREAK
						
						//Second wave.
						CASE 7	vLoc = <<-230.0320, -1274.2419, 30.2960>>	fHeading = 201.8130	BREAK //Granger driver.
						CASE 8	vLoc = <<-230.0320, -1274.2419, 30.2960>>	fHeading = 201.8130	BREAK //Hanging off right side of Granger.
						CASE 9	vLoc = <<-230.0320, -1274.2419, 30.2960>>	fHeading = 201.8130	BREAK //Hanging off left side of Granger.
						CASE 10	vLoc = <<-290.3700, -1244.5300, 27.0350>>	fHeading = 180.6120	BREAK //Granger driver.
						CASE 11	vLoc = <<-290.3700, -1244.5300, 27.0350>>	fHeading = 180.6120	BREAK //Hanging off right side of Granger.
						CASE 12	vLoc = <<-290.3700, -1244.5300, 27.0350>>	fHeading = 180.6120	BREAK //Hanging off left side of Granger.
						CASE 13 vLoc = <<-290.3700, -1244.5300, 27.0350>>	fHeading = 180.6120	BREAK //Front passenger seat of Granger.
					ENDSWITCH
				BREAK
				
				CASE 3
					SWITCH iPed
						//First wave.
						CASE 0	vLoc = <<-33.7181, 216.1364, 105.5534>>	fHeading = 280.5989	BREAK //Heli pilot.
						CASE 1	vLoc = <<-36.7250, 213.5060, 105.5530>>	fHeading = 281.3990	BREAK //Heli passenger.
						CASE 2	vLoc = <<-26.1480, 206.5320, 105.5530>>	fHeading = 336.7990	BREAK
						CASE 3	vLoc = <<-20.7950, 204.0480, 105.5530>>	fHeading = 353.3980	BREAK
						CASE 4	vLoc = <<-30.3060, 210.1670, 127.9000>>	fHeading = 298.1980	BREAK
						CASE 5	vLoc = <<-30.3060, 210.1670, 127.9000>>	fHeading = 298.1980	BREAK
						CASE 6	vLoc = <<-30.3060, 210.1670, 127.9000>>	fHeading = 298.1980	BREAK
						
						//Second wave.
						CASE 7	vLoc = <<15.1610, 231.2040, 108.2120>>	fHeading = 160.1980	BREAK //Granger driver.
						CASE 8	vLoc = <<15.1610, 231.2040, 108.2120>>	fHeading = 160.1980	BREAK //Hanging off right side of Granger.
						CASE 9	vLoc = <<15.1610, 231.2040, 108.2120>>	fHeading = 160.1980	BREAK //Hanging off left side of Granger.
						CASE 10	vLoc = <<-67.3090, 247.3450, 101.5030>>	fHeading = 277.9970	BREAK //Granger driver.
						CASE 11	vLoc = <<-67.3090, 247.3450, 101.5030>>	fHeading = 277.9970	BREAK //Hanging off right side of Granger.
						CASE 12	vLoc = <<-67.3090, 247.3450, 101.5030>>	fHeading = 277.9970	BREAK //Hanging off left side of Granger.
						CASE 13 vLoc = <<-67.3090, 247.3450, 101.5030>>	fHeading = 277.9970	BREAK //Front passenger seat of Granger.
					ENDSWITCH
				BREAK
				
				CASE 4
					SWITCH iPed
						//First wave.
						CASE 0	vLoc = <<920.5810, -2097.3140, 29.4940>>	fHeading = 82.5990	BREAK //Heli pilot.
						CASE 1	vLoc = <<918.5460, -2093.5300, 29.4290>>	fHeading = 108.7990	BREAK //Heli passenger.
						CASE 2	vLoc = <<894.4766, -2091.8967, 29.7588>>	fHeading = 245.9990	BREAK
						CASE 3	vLoc = <<890.7863, -2094.0532, 29.7316>>	fHeading = 263.3988	BREAK
						CASE 4	vLoc = <<909.8660, -2089.9861, 56.5780>>	fHeading = 271.1990	BREAK
						CASE 5	vLoc = <<909.8660, -2089.9861, 56.5780>>	fHeading = 271.1990	BREAK
						CASE 6	vLoc = <<909.8660, -2089.9861, 56.5780>>	fHeading = 271.1990	BREAK
						
						//Second wave.
						CASE 7	vLoc = <<788.4015, -2110.6318, 28.1614>>	fHeading = 354.3979	BREAK //Granger driver.
						CASE 8	vLoc = <<788.4015, -2110.6318, 28.1614>>	fHeading = 354.3979	BREAK //Hanging off right side of Granger.
						CASE 9	vLoc = <<788.4015, -2110.6318, 28.1614>>	fHeading = 354.3979	BREAK //Hanging off left side of Granger.
						CASE 10	vLoc = <<873.6440, -2052.8391, 29.4370>>	fHeading = 173.5980	BREAK //Granger driver.
						CASE 11	vLoc = <<873.6440, -2052.8391, 29.4370>>	fHeading = 173.5980	BREAK //Hanging off right side of Granger.
						CASE 12	vLoc = <<873.6440, -2052.8391, 29.4370>>	fHeading = 173.5980	BREAK //Hanging off left side of Granger.
						CASE 13 vLoc = <<873.6440, -2052.8391, 29.4370>>	fHeading = 173.5980	BREAK //Front passenger seat of Granger.
					ENDSWITCH
				BREAK
				
				CASE 5
					SWITCH iPed
						//First wave.
						CASE 0	vLoc = <<-669.7520, -2374.3960, 12.8090>>	fHeading = 167.7980	BREAK 
						CASE 1	vLoc = <<-673.0850, -2374.4370, 12.7970>>	fHeading = 184.1980	BREAK
						CASE 2	vLoc = <<-681.3120, -2376.8889, 12.9700>>	fHeading = 220.7980	BREAK
						CASE 3	vLoc = <<-680.9410, -2380.8940, 12.8820>>	fHeading = 238.7980	BREAK
						CASE 4	vLoc = <<-697.4010, -2370.1021, 38.3650>>	fHeading = 215.5980	BREAK //Heli pilot.
						CASE 5	vLoc = <<-697.1760, -2369.8081, 38.5540>>	fHeading = 215.5980	BREAK //Heli passenger (rear right).
						CASE 6	vLoc = <<-695.4740, -2370.9409, 38.6170>>	fHeading = 215.5610	BREAK //Heli passenger (rear left).
						
						//Second wave.
						CASE 7	vLoc = <<-713.1200, -2459.0349, 12.8570>>	fHeading = 59.9980	BREAK //Granger driver.
						CASE 8	vLoc = <<-713.1200, -2459.0349, 12.8570>>	fHeading = 59.9980	BREAK //Hanging off right side of Granger.
						CASE 9	vLoc = <<-713.1200, -2459.0349, 12.8570>>	fHeading = 59.9980	BREAK //Hanging off left side of Granger.
						CASE 10	vLoc = <<-739.5080, -2582.4370, 12.8280>>	fHeading = 328.9980	BREAK //Granger driver.
						CASE 11	vLoc = <<-739.5080, -2582.4370, 12.8280>>	fHeading = 328.9980	BREAK //Front passenger seat of Granger.
						CASE 12	vLoc = <<-739.5080, -2582.4370, 12.8280>>	fHeading = 328.9980	BREAK //Hanging off right side of Granger.
						CASE 13 vLoc = <<-739.5080, -2582.4370, 12.8280>>	fHeading = 328.9980	BREAK //Hanging off left side of Granger.
					ENDSWITCH
				BREAK
				
				CASE 6
					SWITCH iPed
						//First wave.
						CASE 0	vLoc = <<214.0207, -3115.7524, 4.7903>>		fHeading = 212.5990 BREAK
						CASE 1	vLoc = <<211.8460, -3117.0107, 4.7903>>		fHeading = 212.5990 BREAK
						CASE 2	vLoc = <<201.7230, -3129.8982, 4.7903>>		fHeading = 254.5990 BREAK
						CASE 3	vLoc = <<198.7784, -3126.4646, 4.7903>>		fHeading = 254.5990 BREAK
						CASE 4	vLoc = <<204.0846, -3102.2737, 36.2484>>	fHeading = 239.9990	BREAK //Heli pilot.
						CASE 5	vLoc = <<204.2693, -3103.0981, 35.8973>>	fHeading = 239.9990	BREAK //Heli passenger (rear right).
						CASE 6	vLoc = <<204.0738, -3102.7695, 35.8330>>	fHeading = 239.9649	BREAK //Heli passenger (rear left).
						
						//Second wave.
						CASE 7	vLoc = <<207.8370, -3327.7639, 4.8160>>		fHeading = 87.5980	BREAK //Granger driver.
						CASE 8	vLoc = <<207.8370, -3327.7639, 4.8160>>		fHeading = 87.5980	BREAK //Hanging off right side of Granger.
						CASE 9	vLoc = <<207.8370, -3327.7639, 4.8160>>		fHeading = 270.9980	BREAK //Hanging off left side of Granger.
						CASE 10	vLoc = <<140.6630, -3088.1160, 4.8960>>		fHeading = 270.9980	BREAK //Granger driver.
						CASE 11	vLoc = <<140.6630, -3088.1160, 4.8960>>		fHeading = 270.9980	BREAK //Front passenger seat of Granger.
						CASE 12	vLoc = <<140.6630, -3088.1160, 4.8960>>		fHeading = 270.9980	BREAK //Hanging off right side of Granger.
						CASE 13 vLoc = <<140.6630, -3088.1160, 4.8960>>		fHeading = 270.9980	BREAK //Hanging off left side of Granger.
					ENDSWITCH
				BREAK
				
				CASE 7
					SWITCH iPed
						//First wave.
						CASE 0	vLoc = <<374.0240, 212.0120, 102.0470>>	fHeading = -5.3510	BREAK 
						CASE 1	vLoc = <<378.1060, 212.8270, 102.0450>>	fHeading = 1.8490	BREAK
						CASE 2	vLoc = <<388.1610, 239.5350, 102.0300>>	fHeading = 142.4490	BREAK
						CASE 3	vLoc = <<385.9780, 241.7810, 102.0300>>	fHeading = 149.6490	BREAK
						CASE 4	vLoc = <<392.4480, 218.2950, 130.7530>>	fHeading = 149.6490	BREAK //Heli pilot.
						CASE 5	vLoc = <<392.4480, 218.2950, 130.7530>>	fHeading = 149.6490	BREAK //Heli passenger (rear right).
						CASE 6	vLoc = <<392.4480, 218.2950, 130.7530>>	fHeading = 149.6490	BREAK //Heli passenger (rear left).
						
						//Second wave.
						CASE 7	vLoc = <<348.4790, 312.9990, 103.0730>>	fHeading = 256.4480	BREAK //Granger driver.
						CASE 8	vLoc = <<348.4790, 312.9990, 103.0730>>	fHeading = 256.4480	BREAK //Hanging off right side of Granger.
						CASE 9	vLoc = <<348.4790, 312.9990, 103.0730>>	fHeading = 256.4480	BREAK //Hanging off left side of Granger.
						CASE 10	vLoc = <<401.7050, 133.6000, 100.9750>>	fHeading = 69.4480	BREAK //Granger driver.
						CASE 11	vLoc = <<401.7050, 133.6000, 100.9750>>	fHeading = 69.4480	BREAK //Hanging off right side of Granger.
						CASE 12	vLoc = <<401.7050, 133.6000, 100.9750>>	fHeading = 69.4480	BREAK //Hanging off left side of Granger.
						CASE 13 vLoc = <<401.7050, 133.6000, 100.9750>>	fHeading = 69.4480	BREAK //Front passenger seat of Granger.
					ENDSWITCH
				BREAK
				
				CASE 8
					SWITCH iPed
						//First wave.
						CASE 0	vLoc = <<-1221.5020, -701.4280, 21.6710>>	fHeading = 53.1290	BREAK 
						CASE 1	vLoc = <<-1223.9340, -705.5050, 21.5770>>	fHeading = 51.9290	BREAK
						CASE 2	vLoc = <<-1230.2170, -681.0810, 23.1820>>	fHeading = 166.9290	BREAK
						CASE 3	vLoc = <<-1234.4550, -677.2550, 23.5960>>	fHeading = 187.9290	BREAK
						CASE 4	vLoc = <<-1231.2710, -691.8820, 56.1690>>	fHeading = 220.3270	BREAK //Heli pilot.
						CASE 5	vLoc = <<-1231.2710, -691.8820, 56.1690>>	fHeading = 220.3270	BREAK //Heli passenger (rear right).
						CASE 6	vLoc = <<-1231.2710, -691.8820, 56.1690>>	fHeading = 220.3270	BREAK //Heli passenger (rear left).
						
						//Second wave.
						CASE 7	vLoc = <<-1296.9750, -619.4940, 26.0950>>	fHeading = 215.7290	BREAK //Granger driver.
						CASE 8	vLoc = <<-1296.9750, -619.4940, 26.0950>>	fHeading = 215.7290	BREAK //Hanging off right side of Granger.
						CASE 9	vLoc = <<-1296.9750, -619.4940, 26.0950>>	fHeading = 215.7290	BREAK //Hanging off left side of Granger.
						CASE 10	vLoc = <<-1159.1660, -712.2960, 20.2660>>	fHeading = 131.9280	BREAK //Granger driver.
						CASE 11	vLoc = <<-1159.1660, -712.2960, 20.2660>>	fHeading = 131.9280	BREAK //Hanging off right side of Granger.
						CASE 12	vLoc = <<-1159.1660, -712.2960, 20.2660>>	fHeading = 131.9280	BREAK //Hanging off left side of Granger.
						CASE 13 vLoc = <<-1159.1660, -712.2960, 20.2660>>	fHeading = 131.9280	BREAK //Front passenger seat of Granger.
					ENDSWITCH
				BREAK
				
				CASE 9
					SWITCH iPed
						//First wave.
						CASE 0	vLoc = <<-1161.0570, -1189.2550, 4.6230>>	fHeading = 39.2000	BREAK 
						CASE 1	vLoc = <<-1160.7340, -1183.4230, 4.6230>>	fHeading = 60.1990	BREAK
						CASE 2	vLoc = <<-1161.5490, -1175.3890, 4.6240>>	fHeading = 88.5990	BREAK
						CASE 3	vLoc = <<-1160.7581, -1170.7740, 4.6240>>	fHeading = 112.5990	BREAK
						CASE 4	vLoc = <<-1166.6066, -1197.4434, 32.4721>>	fHeading = 150.2000	BREAK //Heli pilot.
						CASE 5	vLoc = <<-1166.2457, -1197.5015, 32.3916>>	fHeading = 150.2000	BREAK //Heli passenger (rear right).
						CASE 6	vLoc = <<-1166.1514, -1198.6212, 32.3750>>	fHeading = 150.2339	BREAK //Heli passenger (rear left).
						
						//Second wave.
						CASE 7	vLoc = <<-1110.6890, -1178.9060, 1.1520>>	fHeading = 118.1990	BREAK //Granger driver.
						CASE 8	vLoc = <<-1110.6890, -1178.9060, 1.1520>>	fHeading = 118.1990	BREAK //Hanging off right side of Granger.
						CASE 9	vLoc = <<-1110.6890, -1178.9060, 1.1520>>	fHeading = 118.1990	BREAK //Hanging off left side of Granger.
						CASE 10	vLoc = <<-1184.8690, -1227.7180, 5.7430>>	fHeading = 111.1990	BREAK //Granger driver.
						CASE 11	vLoc = <<-1184.8690, -1227.7180, 5.7430>>	fHeading = 111.1990	BREAK //Hanging off right side of Granger.
						CASE 12	vLoc = <<-1184.8690, -1227.7180, 5.7430>>	fHeading = 111.1990	BREAK //Hanging off left side of Granger.
						CASE 13 vLoc = <<-1184.8690, -1227.7180, 5.7430>>	fHeading = 111.1990	BREAK //Front passenger seat of Granger.
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_RECOVER_PROPERTY
			SWITCH serverBD.iSubVariation
				CASE 0
					SWITCH iPed
						CASE 0	vLoc = <<502.2741, -1315.5908, 29.0229>>	fHeading = 17.5999	BREAK //Cycling CASE 0 BMX
						CASE 1	vLoc = <<837.0940, -1448.6100, 26.9400>>	fHeading = 265.4000	BREAK //Cycling CASE 1 BMX
						CASE 2	vLoc = <<776.7632, -1161.9338, 27.5442>>	fHeading = 345.5999	BREAK //Cycling CASE 2 BMX
						CASE 3	vLoc = <<805.9010, -1074.2610, 27.6860>>	fHeading = 189.7990	BREAK //WORLD_HUMAN_HANG_OUT_STREET
						CASE 4	vLoc = <<806.2600, -1075.2450, 27.7110>>	fHeading = 21.7990	BREAK //WORLD_HUMAN_DRINKING
						CASE 5	vLoc = <<982.4930, -1400.0300, 30.5230>>	fHeading = 267.7980	BREAK //WORLD_HUMAN_HANG_OUT_STREET
						CASE 6	vLoc = <<983.8150, -1400.3210, 30.5140>>	fHeading = 82.9980	BREAK //WORLD_HUMAN_SMOKING
					ENDSWITCH
				BREAK
				
				CASE 1
					SWITCH iPed
						CASE 0	vLoc = <<512.5560, -1044.2350, 35.8965>>	fHeading = 1.8000	BREAK //Cycling CASE 0 BMX
						CASE 1	vLoc = <<364.9430, -826.6480, 28.8970>>		fHeading = 176.0000	BREAK //Cycling CASE 1 BMX
						CASE 2	vLoc = <<101.1370, -1068.4880, 28.7770>>	fHeading = 172.8000	BREAK //Cycling CASE 2 BMX
						CASE 3	vLoc = <<461.3250, -717.9320, 26.3580>>		fHeading = 138.6000	BREAK //WORLD_HUMAN_HANG_OUT_STREET
						CASE 4	vLoc = <<460.2460, -719.0170, 26.3590>>		fHeading = 318.1990	BREAK //WORLD_HUMAN_DRINKING
						CASE 5	vLoc = <<378.9870, -1115.1940, 28.4060>>	fHeading = 155.7980	BREAK //WORLD_HUMAN_HANG_OUT_STREET
						CASE 6	vLoc = <<378.2260, -1116.6300, 28.4060>>	fHeading = 336.5980	BREAK //WORLD_HUMAN_SMOKING
					ENDSWITCH
				BREAK
				
				CASE 2
					SWITCH iPed
						CASE 0	vLoc = <<-229.7190, -1346.1830, 30.0900>>	fHeading = 90.8000	BREAK //Cycling CASE 0 BMX
						CASE 1	vLoc = <<-13.8670, -1387.9120, 29.0360>>	fHeading = 353.6000	BREAK //Cycling CASE 1 BMX
						CASE 2	vLoc = <<-216.8290, -1118.2130, 22.2680>>	fHeading = 146.0000	BREAK //Cycling CASE 2 BMX
						CASE 3	vLoc = <<-214.6850, -1500.4480, 30.4200>>	fHeading = 99.7990	BREAK //WORLD_HUMAN_HANG_OUT_STREET
						CASE 4	vLoc = <<-215.8320, -1500.5630, 30.4240>>	fHeading = 277.1990	BREAK //WORLD_HUMAN_DRINKING
						CASE 5	vLoc = <<51.0150, -1000.9560, 28.3570>>		fHeading = 34.1990	BREAK //WORLD_HUMAN_STAND_MOBILE
						CASE 6	vLoc = <<50.3490, -1000.1190, 28.3570>>		fHeading = 214.7990	BREAK //WORLD_HUMAN_SMOKING
					ENDSWITCH
				BREAK
				
				CASE 3
					SWITCH iPed
						CASE 0	vLoc = <<35.2810, 34.3430, 70.7200>>	fHeading = 3.7990	BREAK //Cycling CASE 0 BMX
						CASE 1	vLoc = <<-226.6880, 280.5000, 91.6710>>	fHeading = 161.3990	BREAK //Cycling CASE 1 BMX
						CASE 2	vLoc = <<268.9210, 146.1230, 103.8730>>	fHeading = 330.5990	BREAK //Cycling CASE 2 BMX
						CASE 3	vLoc = <<80.9490, 273.9890, 109.2100>>	fHeading = 275.3990	BREAK //WORLD_HUMAN_HANG_OUT_STREET
						CASE 4	vLoc = <<82.0820, 274.1110, 109.2100>>	fHeading = 94.7980	BREAK //WORLD_HUMAN_SMOKING
						CASE 5	vLoc = <<-93.3020, -68.9700, 55.9060>>	fHeading = 136.9980	BREAK //WORLD_HUMAN_HANG_OUT_STREET
						CASE 6	vLoc = <<-94.1310, -70.0530, 55.8280>>	fHeading = 324.7980	BREAK //WORLD_HUMAN_DRINKING
					ENDSWITCH
				BREAK
				
				CASE 4
					SWITCH iPed
						CASE 0	vLoc = <<857.2380, -2220.0859, 29.9990>>	fHeading = 171.6000	BREAK //Cycling CASE 0 BMX
						CASE 1	vLoc = <<959.5090, -1746.8230, 30.7980>>	fHeading = 173.7990	BREAK //Cycling CASE 1 BMX
						CASE 2	vLoc = <<743.0340, -2017.9600, 28.7760>>	fHeading = 266.7990	BREAK //Cycling CASE 2 BMX
						CASE 3	vLoc = <<965.4580, -1868.8400, 30.2510>>	fHeading = 280.7990	BREAK //WORLD_HUMAN_HANG_OUT_STREET
						CASE 4	vLoc = <<966.6850, -1868.8010, 30.2740>>	fHeading = 91.5990	BREAK //WORLD_HUMAN_DRINKING
						CASE 5	vLoc = <<782.1900, -2379.8120, 21.4760>>	fHeading = 208.7980	BREAK //WORLD_HUMAN_SMOKING_POT
						CASE 6	vLoc = <<782.6650, -2380.9680, 21.4150>>	fHeading = 33.7980	BREAK //WORLD_HUMAN_DRINKING
					ENDSWITCH
				BREAK
				
				CASE 5
					SWITCH iPed
						CASE 0	vLoc = <<-850.9370, -2691.1990, 13.2680>>	fHeading = 64.2000	BREAK //Cycling CASE 0 BMX
						CASE 1	vLoc = <<-909.8100, -2120.2520, 8.3460>>	fHeading = 57.0000	BREAK //Cycling CASE 1 BMX
						CASE 2	vLoc = <<-163.0400, -2151.2351, 15.8470>>	fHeading = 110.3990	BREAK //Cycling CASE 2 BMX
						CASE 3	vLoc = <<-231.3810, -1994.0150, 28.9460>>	fHeading = 337.1990	BREAK //WORLD_HUMAN_HANG_OUT_STREET
						CASE 4	vLoc = <<-230.7330, -1993.0439, 28.9460>>	fHeading = 147.9990	BREAK //WORLD_HUMAN_DRINKING
						CASE 5	vLoc = <<-1064.2170, -2082.8711, 12.2910>>	fHeading = 67.3990	BREAK //WORLD_HUMAN_SMOKING_POT
						CASE 6	vLoc = <<-1065.5320, -2082.3269, 12.2910>>	fHeading = 247.1980	BREAK //WORLD_HUMAN_HANG_OUT_STREET
					ENDSWITCH
				BREAK
				
				CASE 6
					SWITCH iPed
						CASE 0	vLoc = <<295.7940, -3114.0681, 5.3460>>		fHeading = 0.6000	BREAK //Cycling CASE 0 BMX
						CASE 1	vLoc = <<111.3970, -3184.3979, 5.5300>>		fHeading = 0.6000	BREAK //Cycling CASE 1 BMX
						CASE 2	vLoc = <<-33.0380, -2524.8040, 5.4700>>		fHeading = 330.6000	BREAK //Cycling CASE 2 BMX
						CASE 3	vLoc = <<-288.5610, -2496.6689, 6.3000>>	fHeading = 205.9990	BREAK //WORLD_HUMAN_SMOKING_POT
						CASE 4	vLoc = <<-287.6070, -2498.1550, 6.3000>>	fHeading = 26.7990	BREAK //WORLD_HUMAN_HANG_OUT_STREET
						CASE 5	vLoc = <<370.1080, -2451.9431, 5.4950>>		fHeading = 268.5980	BREAK //WORLD_HUMAN_WELDING
						CASE 6	vLoc = <<368.8660, -2453.2161, 5.1340>>		fHeading = 90.3980	BREAK //WORLD_HUMAN_GUARD_STAND
					ENDSWITCH
				BREAK
				
				CASE 7
					SWITCH iPed
						CASE 0	vLoc = <<269.6230, 147.8330, 103.8900>>		fHeading = 342.8000	BREAK //Cycling CASE 0 BMX
						CASE 1	vLoc = <<645.0280, 204.5450, 96.2580>>		fHeading = 339.8000	BREAK //Cycling CASE 1 BMX
						CASE 2	vLoc = <<136.2880, 583.6060, 183.5970>>		fHeading = 129.7990	BREAK //Cycling CASE 2 BMX
						CASE 3	vLoc = <<-69.7700, 384.5450, 111.4310>>		fHeading = 42.5980	BREAK //WORLD_HUMAN_SMOKING_POT
						CASE 4	vLoc = <<-70.9090, 385.7670, 111.4310>>		fHeading = 226.1980	BREAK //WORLD_HUMAN_HANG_OUT_STREET
						CASE 5	vLoc = <<113.1830, -6.9550, 66.8320>>		fHeading = 293.3980	BREAK //WORLD_HUMAN_HANG_OUT_STREET
						CASE 6	vLoc = <<114.6440, -6.5620, 66.8100>>		fHeading = 114.9970	BREAK //WORLD_HUMAN_DRINKING
					ENDSWITCH
				BREAK
				
				CASE 8
					SWITCH iPed
						CASE 0	vLoc = <<-1367.3979, -575.3310, 29.6830>>	fHeading = 35.2000	BREAK //Cycling CASE 0 BMX
						CASE 1	vLoc = <<-1218.1940, -799.3730, 16.4020>>	fHeading = 211.6000	BREAK //Cycling CASE 1 BMX
						CASE 2	vLoc = <<-1045.7310, -388.2820, 37.0130>>	fHeading = 292.8000	BREAK //Cycling CASE 2 BMX
						CASE 3	vLoc = <<-1360.6110, -964.9200, 8.6980>>	fHeading = 262.2000	BREAK //WORLD_HUMAN_SMOKING_POT
						CASE 4	vLoc = <<-1359.4800, -965.3130, 8.6970>>	fHeading = 85.1990	BREAK //WORLD_HUMAN_DRINKING
						CASE 5	vLoc = <<-1337.1340, -392.5670, 35.6470>>	fHeading = 143.9990	BREAK //WORLD_HUMAN_DRINKING
						CASE 6	vLoc = <<-1338.2970, -393.8310, 35.6250>>	fHeading = 321.9990	BREAK //WORLD_HUMAN_HANG_OUT_STREET
					ENDSWITCH
				BREAK
				
				CASE 9
					SWITCH iPed
						CASE 0	vLoc = <<-1056.0400, -1150.2080, 1.6310>>	fHeading = 297.0000	BREAK //Cycling CASE 0 BMX
						CASE 1	vLoc = <<-1242.2040, -1484.5190, 4.1670>>	fHeading = 202.7990	BREAK //Cycling CASE 1 BMX
						CASE 2	vLoc = <<-1359.7440, -670.4140, 25.1860>>	fHeading = 212.0000	BREAK //Cycling CASE 2 BMX
						CASE 3	vLoc = <<-892.7380, -1162.3110, 3.8980>>	fHeading = 295.1990	BREAK //WORLD_HUMAN_SMOKING_POT
						CASE 4	vLoc = <<-891.1800, -1161.8440, 3.9130>>	fHeading = 112.7990	BREAK //WORLD_HUMAN_DRINKING
						CASE 5	vLoc = <<-1563.9270, -977.9110, 12.0170>>	fHeading = 15.7990	BREAK //WORLD_HUMAN_SMOKING
						CASE 6	vLoc = <<-1564.0780, -976.5770, 12.0170>>	fHeading = 184.9990	BREAK //WORLD_HUMAN_HANG_OUT_STREET
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_STOLEN_VINYLS
			SWITCH iPed
				CASE 0	vLoc = <<-2029.0104, -1033.2864, 4.8834>>	fHeading = 100.3967	BREAK	//Cowers on alerted / mini@strip_club@lap_dance@ld_girl_a_song_a_p1
				CASE 1	vLoc = <<-2029.8900, -1033.4701, 4.8831>>	fHeading = 319.7965	BREAK	//Cowers on alerted / AMB@WORLD_HUMAN_PARTYING@FEMALE@PARTYING_BEER@BASE
				CASE 2	vLoc = <<-2028.2628, -1037.3096, 5.7225>>	fHeading = 337.3958	BREAK	//Cowers on alerted / AMB@WORLD_HUMAN_PARTYING@FEMALE@PARTYING_BEER@BASE
				CASE 3	vLoc = <<-2027.3441, -1036.2589, 5.7970>>	fHeading = 139.3956	BREAK	//WORLD_HUMAN_DRINKING
				CASE 4	vLoc = <<-2030.0228, -1032.4790, 4.8831>>	fHeading = 201.7952	BREAK	//WORLD_HUMAN_MUSCLE_FLEX
				CASE 5	vLoc = <<-2018.4818, -1040.1005, 1.4460>>	fHeading = 345.7960	BREAK	//Cowers on alerted / mini@strip_club@lap_dance@ld_girl_a_song_a_p1
				CASE 6	vLoc = <<-2018.1379, -1038.9982, 1.4463>>	fHeading = 175.7955	BREAK	//Cowers on alerted / WORLD_HUMAN_DRINKING
				CASE 7	vLoc = <<-2023.7990, -1031.3556, 4.8820>>	fHeading = 276.5955	BREAK	//Cowers on alerted / AMB@WORLD_HUMAN_PARTYING@FEMALE@PARTYING_BEER@BASE
				CASE 8	vLoc = <<-2022.7633, -1031.5226, 4.8820>>	fHeading = 112.5954	BREAK	//Cowers on alerted / AMB@WORLD_HUMAN_PARTYING@FEMALE@PARTYING_BEER@BASE
				CASE 9	vLoc = <<-2029.5647, -1042.3636, 4.8821>>	fHeading = 29.3952	BREAK	//Cowers on alerted / mini@strip_club@lap_dance@ld_girl_a_song_a_p1
				CASE 10	vLoc = <<-2029.8666, -1041.4225, 4.8821>>	fHeading = 183.5949	BREAK	//WORLD_HUMAN_HANG_OUT_STREET
				CASE 11	vLoc = <<-2036.9813, -1032.3804, 7.9715>>	fHeading = 139.5948	BREAK	//WORLD_HUMAN_DRINKING
				CASE 12	vLoc = <<-2037.2048, -1033.6052, 7.9715>>	fHeading = 19.3944	BREAK	//Cowers on alerted / WORLD_HUMAN_HANG_OUT_STREET
				CASE 13	vLoc = <<-2037.9761, -1032.9272, 7.9715>>	fHeading = 262.3940	BREAK	//Cowers on alerted / WORLD_HUMAN_DRINKING
				CASE 14	vLoc = <<-2036.7286, -1038.9658, 4.8824>>	fHeading = 119.9940	BREAK	//Cowers on alerted / AMB@WORLD_HUMAN_PARTYING@FEMALE@PARTYING_BEER@BASE
				CASE 15	vLoc = <<-2037.3136, -1039.9683, 4.8825>>	fHeading = 338.9937	BREAK	//WORLD_HUMAN_DRINKING
				CASE 16	vLoc = <<-2037.8628, -1039.1359, 4.8824>>	fHeading = 263.1934	BREAK	//Cowers on alerted / WORLD_HUMAN_HANG_OUT_STREET
				CASE 17	vLoc = <<-2033.2137, -1036.0881, 4.8827>>	fHeading = 55.9928	BREAK	//Cowers on alerted / WORLD_HUMAN_MUSCLE_FLEX
				CASE 18	vLoc = <<-2034.1824, -1035.4418, 4.8826>>	fHeading = 257.5924	BREAK	//Cowers on alerted / mini@strip_club@lap_dance@ld_girl_a_song_a_p1
				CASE 19	vLoc = <<-2040.3917, -1025.6243, 4.8820>>	fHeading = 278.5923	BREAK	//Cowers on alerted / AMB@WORLD_HUMAN_PARTYING@FEMALE@PARTYING_BEER@BASE
				CASE 20	vLoc = <<-2039.0696, -1025.8740, 4.8820>>	fHeading = 71.3920	BREAK	//WORLD_HUMAN_DRINKING
				CASE 21	vLoc = <<-2051.5322, -1027.8907, 10.9076>>	fHeading = 272.1916	BREAK	//WORLD_HUMAN_DRINKING
				CASE 22	vLoc = <<-2050.2246, -1028.5063, 10.9076>>	fHeading = 65.5913	BREAK	//Cowers on alerted / AMB@WORLD_HUMAN_PARTYING@FEMALE@PARTYING_BEER@BASE
				CASE 23	vLoc = <<-2050.4058, -1027.4242, 10.9076>>	fHeading = 130.5912	BREAK	//Cowers on alerted / mini@strip_club@lap_dance@ld_girl_a_song_a_p1
				CASE 24	vLoc = <<-2046.1691, -1024.8229, 10.9076>>	fHeading = 267.9904	BREAK	//Cowers on alerted / WORLD_HUMAN_DRINKING
				CASE 25	vLoc = <<-2045.1696, -1024.7551, 10.9076>>	fHeading = 106.7901	BREAK	//Cowers on alerted / WORLD_HUMAN_HANG_OUT_STREET
				CASE 26	vLoc = <<-2036.1713, -1033.8597, 4.8823>>	fHeading = 254.6309	BREAK	//Cowers on alerted / mini@strip_club@idles@dj@idle_01/idle_01
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_COLLECT_FRIENDS
			SWITCH serverBD.iSubVariation
				CASE 0
					SWITCH iPed
						CASE 0	vLoc = <<734.3821, 2522.4001, 72.2236>>	fHeading = 267.3990	BREAK	//WORLD_HUMAN_STAND_MOBILE
						CASE 1	vLoc = <<737.3206, 2519.9504, 72.2072>>	fHeading = 334.5990	BREAK	//WORLD_HUMAN_HANG_OUT_STREET
						CASE 2	vLoc = <<737.9312, 2520.9216, 72.2038>>	fHeading = 150.7990	BREAK	//WORLD_HUMAN_DRINKING
					ENDSWITCH
				BREAK
				
				CASE 1
					SWITCH iPed
						CASE 0	vLoc = <<1401.4940, 3601.2900, 34.0220>>	fHeading = 18.3990	BREAK	//WORLD_HUMAN_WINDOW_SHOP_BROWSE
						CASE 1	vLoc = <<1397.6600, 3600.0669, 34.0140>>	fHeading = 252.0000	BREAK	//WORLD_HUMAN_DRINKING
						CASE 2	vLoc = <<1398.6060, 3599.8669, 34.0150>>	fHeading = 72.3990	BREAK	//WORLD_HUMAN_SMOKING
					ENDSWITCH
				BREAK
				
				CASE 2
					SWITCH iPed
						CASE 0	vLoc = <<2619.2405, 3274.6438, 54.7487>>	fHeading = 240.9987	BREAK	//WORLD_HUMAN_SMOKING
						CASE 1	vLoc = <<2623.4265, 3275.5071, 54.2510>>	fHeading = 4.3990	BREAK	//WORLD_HUMAN_HANG_OUT_STREET
						CASE 2	vLoc = <<2623.5291, 3276.9448, 54.2510>>	fHeading = 172.7980	BREAK	//WORLD_HUMAN_DRINKING
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT
			SWITCH serverBD.iSubVariation
				CASE 0
					SWITCH iPed
						CASE 0	vLoc = <<-975.0960, -266.8340, 37.3280>>	fHeading = 189.6000	BREAK	//WORLD_HUMAN_CLIPBOARD
						CASE 1	vLoc = <<-969.7850, -266.5430, 37.5470>>	fHeading = 120.6000	BREAK	//WORLD_HUMAN_SMOKING
						CASE 2	vLoc = <<-964.3770, -284.2470, 34.6900>>	fHeading = 27.6000	BREAK	//WORLD_HUMAN_COP_IDLES
						CASE 3	vLoc = <<-979.7450, -266.3390, 37.4080>>	fHeading = 206.7990	BREAK	//WORLD_HUMAN_GUARD_STAND
					ENDSWITCH
				BREAK
				
				CASE 1
					SWITCH iPed
						CASE 0	vLoc = <<481.1740, -1530.4810, 28.3050>>	fHeading = 351.3990	BREAK	//WORLD_HUMAN_WINDOW_SHOP_BROWSE
						CASE 1	vLoc = <<486.6500, -1524.1910, 28.2950>>	fHeading = 98.3990	BREAK	//WORLD_HUMAN_HANG_OUT_STREET
						CASE 2	vLoc = <<485.8450, -1529.4020, 28.2820>>	fHeading = 49.7990	BREAK	//WORLD_HUMAN_GUARD_STAND
						CASE 3	vLoc = <<485.5660, -1524.4440, 28.2960>>	fHeading = 288.1990	BREAK	//WORLD_HUMAN_CLIPBOARD
					ENDSWITCH
				BREAK
				
				CASE 2
					SWITCH iPed
						CASE 0	vLoc = <<1990.3051, 3039.6331, 46.0390>>	fHeading = 57.9990	BREAK	//WORLD_HUMAN_WINDOW_SHOP_BROWSE
						CASE 1	vLoc = <<1983.7910, 3041.7361, 46.0570>>	fHeading = 201.7990	BREAK	//WORLD_HUMAN_CLIPBOARD
						CASE 2	vLoc = <<1984.3650, 3040.6841, 46.0560>>	fHeading = 28.9990	BREAK	//WORLD_HUMAN_STAND_IMPATIENT
						CASE 3	vLoc = <<1980.4290, 3049.8201, 49.4370>>	fHeading = 149.5980	BREAK	//WORLD_HUMAN_GUARD_STAND
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_HOLD_UP
			SWITCH serverBD.iSubVariation
				CASE 0
					SWITCH iPed
						CASE 0	vLoc = <<-1079.2061, -1678.1100, 3.5750>>	fHeading = 286.6000	BREAK	//TASK_COWER
						CASE 1	vLoc = <<-1076.7020, -1679.0580, 3.5750>>	fHeading = 348.1990	BREAK	//TASK_COWER
						CASE 2	vLoc = <<-1077.9310, -1677.2390, 3.5750>>	fHeading = 136.3990	BREAK	//Aiming weapon at <<-1079.2061, -1678.1100, 3.5750>>
						CASE 3	vLoc = <<-1075.8571, -1677.6121, 3.5750>>	fHeading = 160.7990	BREAK	//Aiming weapon at <<-1076.7020, -1679.0580, 3.5750>>
						CASE 4	vLoc = <<-1076.1154, -1675.3678, 3.5752>>	fHeading = 317.7990	BREAK	//WORLD_HUMAN_STAND_MOBILE
						CASE 5	vLoc = <<-1072.9150, -1671.0470, 3.4690>>	fHeading = 98.3980	BREAK	//WORLD_HUMAN_SMOKING
						CASE 6	vLoc = <<-1074.3790, -1671.0980, 3.4780>>	fHeading = 270.3980	BREAK	//WORLD_HUMAN_DRINKING
					ENDSWITCH
				BREAK
				
				CASE 1
					SWITCH iPed
						CASE 0	vLoc = <<1987.2310, 3792.2671, 31.1810>>	fHeading = 278.3990	BREAK	//TASK_COWER
						CASE 1	vLoc = <<1988.9630, 3789.8169, 31.1810>>	fHeading = 339.1990	BREAK	//TASK_COWER
						CASE 2	vLoc = <<1990.2360, 3791.3889, 31.1810>>	fHeading = 138.1980	BREAK	//Aiming weapon at <<1988.9630, 3789.8169, 31.1810>>
						CASE 3	vLoc = <<1988.9960, 3792.5630, 31.1810>>	fHeading = 101.9980	BREAK	//Aiming weapon at <<1987.2310, 3792.2671, 31.1810>>
						CASE 4	vLoc = <<1994.2910, 3797.4570, 31.1810>>	fHeading = 301.1980	BREAK	//WORLD_HUMAN_GUARD_STAND
						CASE 5	vLoc = <<1999.1720, 3798.9280, 31.1810>>	fHeading = 335.3970	BREAK	//WORLD_HUMAN_SMOKING
						CASE 6	vLoc = <<1999.6880, 3799.9751, 31.1810>>	fHeading = 149.9970	BREAK	//WORLD_HUMAN_DRINKING
					ENDSWITCH
				BREAK
				
				CASE 2
					SWITCH iPed
						CASE 0	vLoc = <<32.1460, -1926.1510, 20.7430>>	fHeading = 76.1990	BREAK	//TASK_COWER
						CASE 1	vLoc = <<29.7360, -1929.2581, 20.8740>>	fHeading = 43.3980	BREAK	//TASK_COWER
						CASE 2	vLoc = <<30.0920, -1925.6281, 20.6750>>	fHeading = 260.7980	BREAK	//Aiming weapon at <<32.1460, -1926.1510, 20.7430>>
						CASE 3	vLoc = <<28.5780, -1927.9520, 20.8490>>	fHeading = 218.7980	BREAK	//Aiming weapon at <<29.7360, -1929.2581, 20.8740>>
						CASE 4	vLoc = <<27.5187, -1915.1176, 20.9487>>	fHeading = 182.5980	BREAK	//WORLD_HUMAN_GUARD_STAND
						CASE 5	vLoc = <<27.5974, -1916.4188, 20.8458>>	fHeading = 2.3980	BREAK	//WORLD_HUMAN_SMOKING
						CASE 6	vLoc = <<37.2363, -1907.2974, 20.6329>>	fHeading = 320.7964	BREAK	//WORLD_HUMAN_DRINKING
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

//Returns the position of the specified custom spawn point.
FUNC VECTOR GET_CUSTOM_PED_SPAWN_LOCATION(INT iSpawnPoint)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_STOLEN_VINYLS
			SWITCH iSpawnPoint
				CASE 0	RETURN <<-1912.6383, -976.7737, 0.1499>>
				CASE 1	RETURN <<-1943.5847, -948.0579, 1.2663>>
				CASE 2	RETURN <<-1916.4996, -952.5147, 0.4021>>
				CASE 3	RETURN <<-1884.2371, -1002.0722, 0.8078>>
				CASE 4	RETURN <<-1860.1165, -1044.1671, 1.0228>>
				CASE 5	RETURN <<-1873.4486, -1096.8768, 1.3641>>
				CASE 6	RETURN <<-1849.9624, -1075.1914, 0.0000>>
				CASE 7	RETURN <<-1890.9192, -1126.0608, 1.4918>>
				CASE 8	RETURN <<-1923.1611, -1156.1876, 0.3765>>
				CASE 9	RETURN <<-1897.6337, -1152.0675, 0.5535>>
				CASE 10	RETURN <<-1957.9856, -1166.2285, 0.3741>>
				CASE 11	RETURN <<-1992.7480, -1186.3361, 0.6489>>
				CASE 12	RETURN <<-2032.0869, -1179.0664, -0.0000>>
				CASE 13	RETURN <<-1934.3500, -1174.5101, 0.6552>>
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

//Returns the heading of the specified custom spawn point.
FUNC FLOAT GET_CUSTOM_PED_SPAWN_HEADING(INT iSpawnPoint)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_STOLEN_VINYLS
			SWITCH iSpawnPoint
				CASE 0	RETURN 117.7997
				CASE 1	RETURN 140.7995
				CASE 2	RETURN 125.9993
				CASE 3	RETURN 110.3992
				CASE 4	RETURN 83.3990
				CASE 5	RETURN 69.3988
				CASE 6	RETURN 72.3988
				CASE 7	RETURN 57.3987
				CASE 8	RETURN 36.1986
				CASE 9	RETURN 41.5986
				CASE 10	RETURN 25.7985
				CASE 11	RETURN 8.5985
				CASE 12	RETURN 357.1985
				CASE 13	RETURN 18.3984
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

//Spawn locations for vehicles.
PROC INIT_ENEMY_VEH_SPAWN_COORDS(INT iVeh, VECTOR &vLoc, FLOAT &fHeading, INT iAlternative = 0)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_POLICE_RAID
			SWITCH serverBD.iSubVariation
				CASE 0
					SWITCH iVeh
						CASE 0																					//police4
							SWITCH iAlternative
								CASE 0
								DEFAULT	vLoc =  <<719.0160, -1283.8690, 25.1580>>	fHeading = 327.0000 BREAK
							ENDSWITCH
						BREAK
						
						CASE 1																					//police4
							SWITCH iAlternative
								CASE 0
								DEFAULT	vLoc =  <<722.2130, -1298.8900, 25.2500>>	fHeading = 221.9980	BREAK
							ENDSWITCH
						BREAK
						
						CASE 2																					//polmav
							SWITCH iAlternative
								CASE 0
								DEFAULT	vLoc =  <<727.9240, -1288.8590, 52.7700>>	fHeading = 178.7970	BREAK
							ENDSWITCH
						BREAK
						
						CASE 3																					//granger
							SWITCH iAlternative
								CASE 0	vLoc =  <<828.6694, -1425.6813, 26.2314>>	fHeading = 92.1956	BREAK
								CASE 1	vLoc =  <<767.1661, -1401.1687, 25.5010>>	fHeading = 271.1998	BREAK
								CASE 2	vLoc =  <<823.7745, -1491.3459, 26.8426>>	fHeading = 22.7995	BREAK
							ENDSWITCH
						BREAK
						
						CASE 4																					//granger
							SWITCH iAlternative
								CASE 0	vLoc =  <<778.8632, -1260.8005, 25.3953>>	fHeading = 87.2918	BREAK
								CASE 1	vLoc =  <<772.2142, -1223.6353, 24.7547>>	fHeading = 271.1998	BREAK
								CASE 2	vLoc =  <<763.6183, -1181.6180, 25.6364>>	fHeading = 271.1998	BREAK
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 1
					SWITCH iVeh
						CASE 0																					//police4
							SWITCH iAlternative
								CASE 0
								DEFAULT	vLoc =	<<314.8390, -1000.5750, 28.2610>>	fHeading = 50.5990	BREAK
							ENDSWITCH
						BREAK
						
						CASE 1																					//police4
							SWITCH iAlternative
								CASE 0
								DEFAULT	vLoc =	<<322.3070, -1003.2620, 28.2990>>	fHeading = 78.1990	BREAK
							ENDSWITCH
						BREAK
						
						CASE 2																					//polmav
							SWITCH iAlternative
								CASE 0
								DEFAULT	vLoc =	<<333.6720, -1041.3450, 55.4190>>	fHeading = 359.5990	BREAK
							ENDSWITCH
						BREAK
						
						CASE 3																					//granger
							SWITCH iAlternative
								CASE 0	vLoc =  <<394.2660, -1015.6960, 28.3060>>	fHeading = 179.1980	BREAK
								CASE 1	vLoc =  <<463.2940, -1032.2150, 32.1050>>	fHeading = 97.3970	BREAK
								CASE 2	vLoc =  <<402.9200, -1096.0590, 28.2930>>	fHeading = -0.4030	BREAK
							ENDSWITCH
						BREAK
						
						CASE 4																					//granger
							SWITCH iAlternative
								CASE 0	vLoc =  <<301.0670, -1088.1071, 28.3530>>	fHeading = 87.2918	BREAK
								CASE 1	vLoc =  <<265.1920, -1060.8130, 28.2040>>	fHeading = 271.1998	BREAK
								CASE 2	vLoc =  <<223.9600, -1080.3270, 28.2090>>	fHeading = 357.1970	BREAK
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 2
					SWITCH iVeh
						CASE 0																					//police4
							SWITCH iAlternative
								CASE 0	vLoc =  <<-177.8630, -1296.9010, 30.0900>>	fHeading = 328.2150	BREAK
								DEFAULT	vLoc =	<<-177.8630, -1296.9010, 30.0900>>	fHeading = 328.2150 BREAK
							ENDSWITCH
						BREAK
						
						CASE 1																					//police4
							SWITCH iAlternative
								CASE 0	vLoc =  <<-159.1340, -1308.2610, 30.3060>>	fHeading = 108.4140	BREAK
								DEFAULT	vLoc =	<<-159.1340, -1308.2610, 30.3060>>	fHeading = 108.4140	BREAK
							ENDSWITCH
						BREAK
						
						CASE 2																					//polmav
							SWITCH iAlternative
								CASE 0	vLoc =  <<-164.5670, -1300.4620, 63.0550>>	fHeading = 271.8130	BREAK
								DEFAULT	vLoc =	<<-164.5670, -1300.4620, 63.0550>>	fHeading = 271.8130	BREAK
							ENDSWITCH
						BREAK
						
						CASE 3																					//granger
							SWITCH iAlternative
								CASE 0	vLoc =  <<-230.0320, -1274.2419, 30.2960>>	fHeading = 201.8130	BREAK
								CASE 1	vLoc =  <<-247.1150, -1315.5930, 30.2710>>	fHeading = 359.0120	BREAK
								CASE 2	vLoc =  <<-264.1710, -1340.2930, 30.1450>>	fHeading = -1.5880	BREAK
							ENDSWITCH
						BREAK
						
						CASE 4																					//granger
							SWITCH iAlternative
								CASE 0	vLoc =  <<-290.3700, -1244.5300, 27.0350>>	fHeading = 180.6120	BREAK
								CASE 1	vLoc =  <<-265.6287, -1372.8632, 30.1869>>	fHeading = -1.5880	BREAK
								CASE 2	vLoc =  <<-240.7343, -1346.5472, 30.0181>>	fHeading = 90.4119	BREAK
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 3
					SWITCH iVeh
						CASE 0																					//police4
							SWITCH iAlternative
								CASE 0	vLoc =  <<-22.8370, 205.7970, 105.5530>>	fHeading = -96.2000	BREAK
								DEFAULT	vLoc =	<<-22.8370, 205.7970, 105.5530>>	fHeading = -96.2000 BREAK
							ENDSWITCH
						BREAK
						
						CASE 1																					//police4
							SWITCH iAlternative
								CASE 0	vLoc =  <<-36.6890, 215.7150, 105.5530>>	fHeading = 45.3990	BREAK
								DEFAULT	vLoc =	<<-36.6890, 215.7150, 105.5530>>	fHeading = 45.3990	BREAK
							ENDSWITCH
						BREAK
						
						CASE 2																					//polmav
							SWITCH iAlternative
								CASE 0	vLoc =  <<-30.1010, 209.8930, 125.4210>>	fHeading = 298.1980	BREAK
								DEFAULT	vLoc =	<<-30.1010, 209.8930, 125.4210>>	fHeading = 298.1980	BREAK
							ENDSWITCH
						BREAK
						
						CASE 3																					//granger
							SWITCH iAlternative
								CASE 0	vLoc =  <<15.1610, 231.2040, 108.2120>>	fHeading = 160.1980	BREAK
								CASE 1	vLoc =  <<-4.7740, 251.8940, 107.8010>>	fHeading = 265.7980	BREAK
								CASE 2	vLoc =  <<8.9370, 130.9970, 87.2100>>	fHeading = 71.7970	BREAK
							ENDSWITCH
						BREAK
						
						CASE 4																					//granger
							SWITCH iAlternative
								CASE 0	vLoc =  <<-67.3090, 247.3450, 101.5030>>	fHeading = 277.9970	BREAK
								CASE 1	vLoc =  <<-101.7790, 226.7580, 96.2820>>	fHeading = 357.1970	BREAK
								CASE 2	vLoc =  <<-141.7280, 242.2190, 94.1240>>	fHeading = 268.5970	BREAK
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 4
					SWITCH iVeh
						CASE 0																					//police4
							SWITCH iAlternative
								CASE 0	vLoc =  <<891.4522, -2092.4768, 29.7139>>	fHeading = 234.8000	BREAK
								DEFAULT	vLoc =	<<891.4522, -2092.4768, 29.7139>>	fHeading = 234.8000 BREAK
							ENDSWITCH
						BREAK
						
						CASE 1																					//police4
							SWITCH iAlternative
								CASE 0	vLoc =  <<921.2090, -2094.6489, 29.4270>>	fHeading = 204.7990	BREAK
								DEFAULT	vLoc =	<<921.2090, -2094.6489, 29.4270>>	fHeading = 204.7990	BREAK
							ENDSWITCH
						BREAK
						
						CASE 2																					//polmav
							SWITCH iAlternative
								CASE 0	vLoc =  <<910.5860, -2090.5000, 54.4720>>	fHeading = 271.1990	BREAK
								DEFAULT	vLoc =	<<910.5860, -2090.5000, 54.4720>>	fHeading = 271.1990	BREAK
							ENDSWITCH
						BREAK
						
						CASE 3																					//granger
							SWITCH iAlternative
								CASE 0	vLoc =  <<788.4015, -2110.6318, 28.1614>>	fHeading = 354.3979	BREAK
								CASE 1	vLoc =  <<797.4940, -2123.3010, 28.4560>>	fHeading = 85.9980	BREAK
								CASE 2	vLoc =  <<727.3900, -2074.9041, 28.2910>>	fHeading = 262.1980	BREAK
							ENDSWITCH
						BREAK
						
						CASE 4																					//granger
							SWITCH iAlternative
								CASE 0	vLoc =  <<873.6440, -2052.8391, 29.4370>>	fHeading = 173.5980	BREAK
								CASE 1	vLoc =  <<745.3600, -2018.0630, 28.2920>>	fHeading = 262.7970	BREAK
								CASE 2	vLoc =  <<768.0020, -1978.4240, 28.1580>>	fHeading = 171.1970	BREAK
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 5
					SWITCH iVeh
						CASE 0	vLoc =	<<-669.2390, -2373.1060, 12.8060>>	fHeading = 266.4000	BREAK			//police4
						
						CASE 1	vLoc =	<<-681.9150, -2379.3401, 12.9460>>	fHeading = 307.7990	BREAK			//police4
						
						CASE 2	vLoc =	<<-696.9600, -2369.3230, 36.3630>>	fHeading = 234.7980	BREAK			//polmav
						
						CASE 3																					//granger
							SWITCH iAlternative
								CASE 0	vLoc =  <<-713.1200, -2459.0349, 12.8570>>	fHeading = 59.9980	BREAK
								CASE 1	vLoc =  <<-759.5130, -2529.3569, 12.8690>>	fHeading = 59.9980	BREAK
								CASE 2	vLoc =  <<-823.0020, -2609.8601, 12.9320>>	fHeading = 328.9980	BREAK
							ENDSWITCH
						BREAK
						
						CASE 4																					//granger
							SWITCH iAlternative
								CASE 0	vLoc =  <<-739.5080, -2582.4370, 12.8280>>	fHeading = 328.9980	BREAK
								CASE 1	vLoc =  <<-792.6080, -2457.0029, 13.5710>>	fHeading = 237.9980	BREAK
								CASE 2	vLoc =  <<-797.4790, -2526.0549, 12.7570>>	fHeading = 332.1980	BREAK
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 6
					SWITCH iVeh
						CASE 0	vLoc =	<<199.6197, -3129.7869, 4.7903>>	fHeading = 194.5990	BREAK			//police4
						
						CASE 1	vLoc =	<<210.4602, -3115.2461, 4.7903>>	fHeading = 286.1989	BREAK			//police4
						
						CASE 2	vLoc =	<<204.2783, -3102.1553, 33.5515>>	fHeading = 205.5990	BREAK			//polmav
						
						CASE 3																					//granger
							SWITCH iAlternative
								CASE 0	vLoc =  <<207.8370, -3327.7639, 4.8160>>	fHeading = 87.5980	BREAK
								CASE 1	vLoc =  <<285.6150, -3286.6589, 4.6370>>	fHeading = 177.5980	BREAK
								CASE 2	vLoc =  <<281.5720, -3124.4871, 4.6410>>	fHeading = 38.5980	BREAK
							ENDSWITCH
						BREAK
						
						CASE 4																					//granger
							SWITCH iAlternative
								CASE 0	vLoc =  <<140.6630, -3088.1160, 4.8960>>	fHeading = 270.9980	BREAK
								CASE 1	vLoc =  <<144.6470, -2981.1060, 5.3040>>	fHeading = 269.7980	BREAK
								CASE 2	vLoc =  <<192.7630, -2933.5120, 5.6190>>	fHeading = 181.3980	BREAK
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 7
					SWITCH iVeh
						CASE 0																					//police4
							SWITCH iAlternative
								CASE 0
								DEFAULT	vLoc =	<<387.4280, 243.2330, 102.0280>>	fHeading = 38.0490	BREAK
							ENDSWITCH
						BREAK
						
						CASE 1																					//police4
							SWITCH iAlternative
								CASE 0
								DEFAULT	vLoc =	<<375.4110, 210.6180, 102.0480>>	fHeading = 119.2490	BREAK
							ENDSWITCH
						BREAK
						
						CASE 2																					//polmav
							SWITCH iAlternative
								CASE 0
								DEFAULT	vLoc =	<<393.2110, 217.6120, 128.2670>>	fHeading = 70.2490	BREAK
							ENDSWITCH
						BREAK
						
						CASE 3																					//granger
							SWITCH iAlternative
								CASE 0	vLoc =  <<348.4790, 312.9990, 103.0730>>	fHeading = 256.4480	BREAK
								CASE 1	vLoc =  <<425.0080, 340.1060, 104.1070>>	fHeading = 195.6480	BREAK
								CASE 2	vLoc =  <<464.8370, 284.2100, 101.9670>>	fHeading = 71.4480	BREAK
							ENDSWITCH
						BREAK
						
						CASE 4																					//granger
							SWITCH iAlternative
								CASE 0	vLoc =  <<401.7050, 133.6000, 100.9750>>	fHeading = 69.4480	BREAK
								CASE 1	vLoc =  <<348.2540, 86.7420, 98.3700>>		fHeading = 340.6480	BREAK
								CASE 2	vLoc =  <<313.4590, 154.5540, 102.7860>>	fHeading = 250.0470	BREAK
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 8
					SWITCH iVeh
						CASE 0	vLoc =	<<-1222.4950, -703.4690, 21.6160>>	fHeading = 96.9290	BREAK			//police4
						
						CASE 1	vLoc =	<<-1232.2190, -680.6480, 23.2960>>	fHeading = 206.9290	BREAK			//police4
						
						CASE 2	vLoc =	<<-1230.8640, -691.5670, 53.9560>>	fHeading = 54.3290	BREAK			//polmav
						
						CASE 3																					//granger
							SWITCH iAlternative
								CASE 0	vLoc =  <<-1296.9750, -619.4940, 26.0950>>	fHeading = 215.7290	BREAK
								CASE 1	vLoc =  <<-1332.4230, -676.5220, 25.3080>>	fHeading = 303.9280	BREAK
								CASE 2	vLoc =  <<-1288.5670, -687.0980, 23.7700>>	fHeading = 36.9280	BREAK
							ENDSWITCH
						BREAK
						
						CASE 4																					//granger
							SWITCH iAlternative
								CASE 0	vLoc =  <<-1159.1660, -712.2960, 20.2660>>	fHeading = 131.9280	BREAK
								CASE 1	vLoc =  <<-1139.3870, -761.8640, 17.7140>>	fHeading = 37.3280	BREAK
								CASE 2	vLoc =  <<-1165.9260, -680.2820, 21.3180>>	fHeading = 220.3270	BREAK
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 9
					SWITCH iVeh
						CASE 0	vLoc =	<<-1160.5920, -1186.5370, 4.6230>>	fHeading = 138.2000	BREAK			//police4
						
						CASE 1	vLoc =	<<-1161.0850, -1173.1360, 4.6240>>	fHeading = 210.4000	BREAK			//police4
						
						CASE 2	vLoc =	<<-1166.5294, -1199.1533, 30.1265>>	fHeading = 12.7998	BREAK			//polmav
						
						CASE 3																					//granger
							SWITCH iAlternative
								CASE 0	vLoc =  <<-1110.6890, -1178.9060, 1.1520>>	fHeading = 118.1990	BREAK
								CASE 1	vLoc =  <<-1111.5150, -1239.3409, 1.3550>>	fHeading = 31.1990	BREAK
								CASE 2	vLoc =  <<-1153.3190, -1148.8800, 1.7110>>	fHeading = 206.7990	BREAK
							ENDSWITCH
						BREAK
						
						CASE 4																					//granger
							SWITCH iAlternative
								CASE 0	vLoc =  <<-1184.8690, -1227.7180, 5.7430>>	fHeading = 111.1990	BREAK
								CASE 1	vLoc =  <<-1216.9830, -1180.6720, 6.7320>>	fHeading = 188.1980	BREAK
								CASE 2	vLoc =  <<-1184.1630, -1266.4460, 5.4070>>	fHeading = 19.1980	BREAK
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_RECOVER_PROPERTY
			SWITCH serverBD.iSubVariation
				CASE 0
					SWITCH iVeh
						CASE 0	vLoc =	<<501.8685, -1315.8176, 28.2021>>	fHeading = 294.9994	BREAK			//bmx
						CASE 1	vLoc =	<<837.4830, -1448.7360, 26.5760>>	fHeading = 265.4000	BREAK			//bmx
						CASE 2	vLoc =	<<776.8274, -1161.8593, 27.5582>>	fHeading = 275.3998	BREAK			//bmx
						CASE 3	vLoc =	<<983.9900, -1405.7950, 30.3510>>	fHeading = 29.3980	BREAK			//emperor
					ENDSWITCH
				BREAK
				
				CASE 1
					SWITCH iVeh
						CASE 0	vLoc =	<<513.3302, -1044.2371, 35.7153>>	fHeading = 275.9998	BREAK			//bmx
						CASE 1	vLoc =	<<364.9800, -826.0170, 28.2930>>	fHeading = 176.0000	BREAK			//bmx
						CASE 2	vLoc =	<<101.7450, -1068.8290, 28.2480>>	fHeading = 58.0000	BREAK			//bmx
						CASE 3	vLoc =	<<375.6670, -1116.2750, 28.4060>>	fHeading = 178.1980	BREAK			//manana
					ENDSWITCH
				BREAK
				
				CASE 2
					SWITCH iVeh
						CASE 0	vLoc =	<<-230.1220, -1346.0790, 30.0260>>	fHeading = 90.8000	BREAK			//bmx
						CASE 1	vLoc =	<<-13.9290, -1388.5020, 28.3880>>	fHeading = 353.6000	BREAK			//bmx
						CASE 2	vLoc =	<<-217.1360, -1118.8051, 21.9710>>	fHeading = 146.0000	BREAK			//bmx
						CASE 3	vLoc =	<<-214.0940, -1497.0780, 30.2650>>	fHeading = 277.1990	BREAK			//buccaneer
					ENDSWITCH
				BREAK
				
				CASE 3
					SWITCH iVeh
						CASE 0	vLoc =	<<35.0080, 33.6470, 70.2200>>		fHeading = 159.8000	BREAK			//bmx
						CASE 1	vLoc =	<<-226.4550, 281.0450, 91.0480>>	fHeading = 161.3990	BREAK			//bmx
						CASE 2	vLoc =	<<268.7240, 145.6610, 103.3610>>	fHeading = 330.5990	BREAK			//bmx
						CASE 3	vLoc =	<<-95.4680, -67.7540, 55.5310>>		fHeading = 136.9980	BREAK			//tornado2
					ENDSWITCH
				BREAK
				
				CASE 4
					SWITCH iVeh
						CASE 0	vLoc =	<<857.1130, -2220.3799, 29.5200>>	fHeading = 171.6000	BREAK			//bmx
						CASE 1	vLoc =	<<959.5930, -1746.1780, 30.2350>>	fHeading = 173.7990	BREAK			//bmx
						CASE 2	vLoc =	<<743.6510, -2018.0530, 28.2920>>	fHeading = 266.7990	BREAK			//bmx
						CASE 3	vLoc =	<<966.9060, -1873.2120, 30.1340>>	fHeading = 39.7990	BREAK			//tornado2
					ENDSWITCH
				BREAK
				
				CASE 5
					SWITCH iVeh
						CASE 0	vLoc =	<<-851.5120, -2690.8621, 12.8120>>	fHeading = 64.2000	BREAK			//bmx
						CASE 1	vLoc =	<<-909.2120, -2120.6550, 7.8580>>	fHeading = 57.0000	BREAK			//bmx
						CASE 2	vLoc =	<<-163.5010, -2151.2981, 15.7050>>	fHeading = 110.3990	BREAK			//bmx
						CASE 3	vLoc =	<<-1062.2750, -2081.2429, 12.2920>>	fHeading = 187.1980	BREAK			//emperor
					ENDSWITCH
				BREAK
				
				CASE 6
					SWITCH iVeh
						CASE 0	vLoc =	<<295.7830, -3113.3501, 4.8810>>	fHeading = 0.6000	BREAK			//bmx
						CASE 1	vLoc =	<<111.3830, -3183.7410, 5.0010>>	fHeading = 0.6000	BREAK			//bmx
						CASE 2	vLoc =	<<-32.6690, -2524.2119, 5.0100>>	fHeading = 330.6000	BREAK			//bmx
						CASE 3	vLoc =	<<-290.2390, -2500.1980, 5.0010>>	fHeading = 59.7990	BREAK			//phoenix
					ENDSWITCH
				BREAK
				
				CASE 7
					SWITCH iVeh
						CASE 0	vLoc =	<<269.3470, 147.1590, 103.4080>>	fHeading = 342.8000	BREAK			//bmx
						CASE 1	vLoc =	<<645.0110, 204.7910, 95.8040>>		fHeading = 339.8000	BREAK			//bmx
						CASE 2	vLoc =	<<135.8970, 583.4250, 183.3020>>	fHeading = 129.7990	BREAK			//bmx
						CASE 3	vLoc =	<<-67.4950, 386.2280, 111.4310>>	fHeading = 224.3990	BREAK			//buccaneer
					ENDSWITCH
				BREAK
				
				CASE 8
					SWITCH iVeh
						CASE 0	vLoc =	<<-1367.5750, -575.0340, 29.0900>>	fHeading = 35.2000	BREAK			//bmx
						CASE 1	vLoc =	<<-1218.1560, -799.4380, 15.7330>>	fHeading = 211.6000	BREAK			//bmx
						CASE 2	vLoc =	<<-1045.0090, -387.9670, 36.6520>>	fHeading = 292.8000	BREAK			//bmx
						CASE 3	vLoc =	<<-1339.6460, -391.7370, 35.6610>>	fHeading = 305.1980	BREAK			//peyote
					ENDSWITCH
				BREAK
				
				CASE 9
					SWITCH iVeh
						CASE 0	vLoc =	<<-1055.4990, -1149.8879, 1.0950>>	fHeading = 297.0000	BREAK			//bmx
						CASE 1	vLoc =	<<-1242.4290, -1484.1820, 3.2850>>	fHeading = 202.7990	BREAK			//bmx
						CASE 2	vLoc =	<<-1359.3810, -670.8010, 24.6550>>	fHeading = 212.0000	BREAK			//bmx
						CASE 3	vLoc =	<<-1557.5380, -977.1780, 12.0170>>	fHeading = 75.1990	BREAK			//peyote
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_STOLEN_VINYLS
			SWITCH iVeh
				CASE 0	vLoc =	<<-2016.6946, -1044.3771, 0.3781>>	fHeading = 151.3991 BREAK //seashark
				CASE 1	vLoc =	<<-2014.3228, -1038.3533, 1.4481>>	fHeading = 348.9989 BREAK //seashark
				CASE 2	vLoc =	<<-2017.7046, -1026.3899, 0.0792>>	fHeading = 225.3985 BREAK //seasparrow
				CASE 3	vLoc =	<<-1840.8966, -926.8209, 0.1287>>	fHeading = 111.1998 BREAK //seashark
				CASE 4	vLoc =	<<-1835.3519, -938.4202, 0.5941>>	fHeading = 111.1998 BREAK //seashark
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_HOLD_UP
			SWITCH serverBD.iSubVariation
				CASE 0
					SWITCH iVeh
						CASE 0	vLoc =  <<-1072.2743, -1668.5948, 3.4427>>	fHeading = 57.7998	BREAK //manana
						CASE 1	vLoc =  <<-1071.1010, -1674.8800, 3.5180>>	fHeading = 207.4000	BREAK //manana
					ENDSWITCH
				BREAK
				
				CASE 1
					SWITCH iVeh
						CASE 0	vLoc =  <<1999.1071, 3796.9031, 31.1810>>	fHeading = 150.0000	BREAK //sanchez
						CASE 1	vLoc =  <<2007.4070, 3793.3799, 31.1808>>	fHeading = 270.7997	BREAK //sanchez
					ENDSWITCH
				BREAK
				
				CASE 2
					SWITCH iVeh
						CASE 0	vLoc =  <<40.9870, -1921.5010, 20.6610>>	fHeading = 130.6000	BREAK //emperor
						CASE 1	vLoc =  <<46.5690, -1914.8790, 20.6570>>	fHeading = 137.8000	BREAK //emperor
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

//Spawn locations for props.
PROC INIT_PROP_SPAWN_COORDS(INT iProp, VECTOR &vLoc, FLOAT &fHeading)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_POLICE_RAID
			SWITCH serverBD.iSubVariation
				CASE 0
					SWITCH iProp
						CASE 0	vLoc =  <<722.8020, -1282.0970, 25.2890>>	fHeading = 63.1980	BREAK
						CASE 1	vLoc = 	<<718.5640, -1289.8290, 25.1160>>	fHeading = 89.3980	BREAK
						CASE 2	vLoc = 	<<726.4450, -1297.7870, 25.2710>>	fHeading = 180.5980	BREAK
					ENDSWITCH
				BREAK
				
				CASE 1
					SWITCH iProp
						CASE 0	vLoc =  <<317.4830, -997.6870, 28.1250>>	fHeading = 271.8000	BREAK
						CASE 1	vLoc = 	<<318.2120, -1003.3670, 28.3090>>	fHeading = 303.2000	BREAK
						CASE 2	vLoc = 	<<344.6120, -997.7850, 28.2020>>	fHeading = 92.3990	BREAK
					ENDSWITCH
				BREAK
				
				CASE 2
					SWITCH iProp
						CASE 0	vLoc =  <<-159.3600, -1305.5050, 30.3440>>	fHeading = 36.4000	BREAK
						CASE 1	vLoc = 	<<-163.8780, -1306.2340, 30.3280>>	fHeading = 1.0000	BREAK
						CASE 2	vLoc = 	<<-175.7810, -1298.9430, 30.1800>>	fHeading = 264.8030	BREAK
					ENDSWITCH
				BREAK
				
				CASE 3
					SWITCH iProp
						CASE 0	vLoc =  <<-22.0400, 208.2030, 105.5580>>	fHeading = 351.0000	BREAK
						CASE 1	vLoc = 	<<-25.9590, 208.8240, 105.5580>>	fHeading = 344.0000	BREAK
						CASE 2	vLoc = 	<<-32.6470, 216.0150, 105.5580>>	fHeading = 263.8000	BREAK
					ENDSWITCH
				BREAK
				
				CASE 4
					SWITCH iProp
						CASE 0	vLoc =  <<895.6293, -2093.1296, 29.8046>>	fHeading = 266.2000	BREAK
						CASE 1	vLoc = 	<<918.0450, -2093.9250, 29.4950>>	fHeading = 305.8000	BREAK
						CASE 2	vLoc = 	<<919.5600, -2097.2849, 29.4980>>	fHeading = 82.5990	BREAK
					ENDSWITCH
				BREAK
				
				CASE 5
					SWITCH iProp
						CASE 0	vLoc =  <<-669.8920, -2375.3040, 12.8950>>	fHeading = 354.4000	BREAK
						CASE 1	vLoc = 	<<-672.9260, -2375.3650, 12.8300>>	fHeading = 17.2000	BREAK
						CASE 2	vLoc = 	<<-680.2330, -2381.4260, 12.8620>>	fHeading = 48.6000	BREAK
					ENDSWITCH
				BREAK
				
				CASE 6
					SWITCH iProp
						CASE 0	vLoc =  <<202.7403, -3130.0356, 4.7949>>	fHeading = 83.8000	BREAK
						CASE 1	vLoc = 	<<212.5484, -3118.1116, 4.7949>>	fHeading = 38.8000	BREAK
						CASE 2	vLoc = 	<<214.7690, -3117.1643, 4.7949>>	fHeading = 2.0000	BREAK
					ENDSWITCH
				BREAK
				
				CASE 7
					SWITCH iProp
						CASE 0	vLoc =  <<385.1900, 240.6210, 102.0370>>	fHeading = 326.6000	BREAK
						CASE 1	vLoc = 	<<386.9700, 238.7060, 102.0370>>	fHeading = 292.4000	BREAK
						CASE 2	vLoc = 	<<376.8040, 214.4510, 102.0500>>	fHeading = 177.2000	BREAK
					ENDSWITCH
				BREAK
				
				CASE 8
					SWITCH iProp
						CASE 0	vLoc =  <<-1222.2996, -700.8438, 21.7300>>	fHeading = 53.6000	BREAK
						CASE 1	vLoc = 	<<-1226.7310, -704.5225, 21.7257>>	fHeading = 29.6000	BREAK
						CASE 2	vLoc = 	<<-1235.3414, -679.0843, 23.5336>>	fHeading = 328.3999	BREAK
					ENDSWITCH
				BREAK
				
				CASE 9
					SWITCH iProp
						CASE 0	vLoc =  <<-1163.2960, -1185.5010, 4.6280>>	fHeading = 35.0000	BREAK
						CASE 1	vLoc = 	<<-1161.7300, -1183.1610, 4.6280>>	fHeading = 80.0000	BREAK
						CASE 2	vLoc = 	<<-1162.4470, -1175.5150, 4.6280>>	fHeading = 104.0000	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_STOLEN_VINYLS
			SWITCH iProp
				CASE 0	vLoc =	<<-2037.1740, -1035.4529, 5.3141>>	fHeading = 76.1990	BREAK
				CASE 1	vLoc =	<<-2035.9908, -1032.2062, 5.3144>>	fHeading = 60.5990	BREAK
				CASE 2	vLoc =	<<-2035.7511, -1034.1659, 4.8954>>	fHeading = 251.5996	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

//Returns whether exact co-ordinates should be used for a prop.
FUNC BOOL GET_SHOULD_USE_EXACT_COORDS_FOR_PROPS()
	RETURN TRUE
ENDFUNC

//Returns whether CREATE_NET_OBJ should ignore the model offset.
FUNC BOOL GET_SHOULD_CREATE_PROP_WITH_NO_OFFSET(INT iProp)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_STOLEN_VINYLS
			SWITCH iProp
				CASE 0
				CASE 1	RETURN TRUE
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

//Returns the weapon to be used by the specified ped.
FUNC WEAPON_TYPE GET_PED_WEAPON(INT iPed)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_POLICE_RAID
			SWITCH serverBD.iSubVariation
				CASE 0
					SWITCH iPed
						CASE 0	RETURN WEAPONTYPE_PISTOL
						CASE 1	RETURN WEAPONTYPE_SMG
						CASE 2	RETURN WEAPONTYPE_PUMPSHOTGUN
						CASE 3	RETURN WEAPONTYPE_PUMPSHOTGUN
						CASE 4	RETURN WEAPONTYPE_PISTOL
						CASE 5	RETURN WEAPONTYPE_PISTOL
						CASE 6	RETURN WEAPONTYPE_PISTOL
						CASE 7	RETURN WEAPONTYPE_PUMPSHOTGUN
						CASE 8	RETURN WEAPONTYPE_PISTOL
						CASE 9	RETURN WEAPONTYPE_PISTOL
						CASE 10	RETURN WEAPONTYPE_PUMPSHOTGUN
						CASE 11	RETURN WEAPONTYPE_PISTOL
						CASE 12	RETURN WEAPONTYPE_PISTOL
						CASE 13	RETURN WEAPONTYPE_SMG
					ENDSWITCH
				BREAK
				
				DEFAULT
					SWITCH iPed
						CASE 0	RETURN WEAPONTYPE_PUMPSHOTGUN
						CASE 1	RETURN WEAPONTYPE_PISTOL
						CASE 2	RETURN WEAPONTYPE_PISTOL
						CASE 3	RETURN WEAPONTYPE_PUMPSHOTGUN
						CASE 4	RETURN WEAPONTYPE_PISTOL
						CASE 5	RETURN WEAPONTYPE_SMG
						CASE 6	RETURN WEAPONTYPE_PISTOL
						CASE 7	RETURN WEAPONTYPE_PUMPSHOTGUN
						CASE 8	RETURN WEAPONTYPE_PISTOL
						CASE 9	RETURN WEAPONTYPE_PISTOL
						CASE 10	RETURN WEAPONTYPE_PUMPSHOTGUN
						CASE 11	RETURN WEAPONTYPE_SMG
						CASE 12	RETURN WEAPONTYPE_PISTOL
						CASE 13	RETURN WEAPONTYPE_PISTOL
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_RECOVER_PROPERTY
			SWITCH serverBD.iSubVariation
				CASE 0
				CASE 1
				CASE 3
				CASE 4
				CASE 6
				CASE 7
				CASE 8
				CASE 9
					SWITCH iPed
						CASE 0	RETURN WEAPONTYPE_PISTOL
						CASE 1	RETURN WEAPONTYPE_PISTOL
						CASE 2	RETURN WEAPONTYPE_PISTOL
						CASE 3	RETURN WEAPONTYPE_ASSAULTRIFLE
						CASE 4	RETURN WEAPONTYPE_PISTOL
						CASE 5	RETURN WEAPONTYPE_ASSAULTRIFLE
						CASE 6	RETURN WEAPONTYPE_PISTOL
					ENDSWITCH
				BREAK
				
				CASE 2
				CASE 5
					SWITCH iPed
						CASE 0	RETURN WEAPONTYPE_PISTOL
						CASE 1	RETURN WEAPONTYPE_PISTOL
						CASE 2	RETURN WEAPONTYPE_PISTOL
						CASE 3	RETURN WEAPONTYPE_SMG
						CASE 4	RETURN WEAPONTYPE_PISTOL
						CASE 5	RETURN WEAPONTYPE_SMG
						CASE 6	RETURN WEAPONTYPE_PISTOL
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_STOLEN_VINYLS
			SWITCH iPed
				CASE 3
				CASE 15	RETURN WEAPONTYPE_KNIFE
				
				CASE 4
				CASE 11
				CASE 21	RETURN WEAPONTYPE_PISTOL
				
				CASE 10
				CASE 20	RETURN WEAPONTYPE_UNARMED
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT
			SWITCH serverBD.iSubVariation
				CASE 0
					SWITCH iPed
						CASE 0	RETURN WEAPONTYPE_UNARMED
						CASE 1	RETURN WEAPONTYPE_PISTOL
						CASE 2	RETURN WEAPONTYPE_PISTOL
						CASE 3	RETURN WEAPONTYPE_PISTOL
					ENDSWITCH
				BREAK
				
				CASE 1
					SWITCH iPed
						CASE 0	RETURN WEAPONTYPE_UNARMED
						CASE 1	RETURN WEAPONTYPE_UNARMED
						CASE 2	RETURN WEAPONTYPE_UNARMED
						CASE 3	RETURN WEAPONTYPE_PISTOL
					ENDSWITCH
				BREAK
				
				CASE 2
					SWITCH iPed
						CASE 0	RETURN WEAPONTYPE_UNARMED
						CASE 1	RETURN WEAPONTYPE_PISTOL
						CASE 2	RETURN WEAPONTYPE_UNARMED
						CASE 3	RETURN WEAPONTYPE_UNARMED
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_HOLD_UP
			SWITCH serverBD.iSubVariation
				CASE 0
					SWITCH iPed
						CASE 0	RETURN WEAPONTYPE_UNARMED
						CASE 1	RETURN WEAPONTYPE_UNARMED
						CASE 2	RETURN WEAPONTYPE_ASSAULTRIFLE
						CASE 3	RETURN WEAPONTYPE_ASSAULTRIFLE
						CASE 4	RETURN WEAPONTYPE_SAWNOFFSHOTGUN
						CASE 5	RETURN WEAPONTYPE_PISTOL
						CASE 6	RETURN WEAPONTYPE_PISTOL
					ENDSWITCH
				BREAK
				
				CASE 1
					SWITCH iPed
						CASE 0	RETURN WEAPONTYPE_UNARMED
						CASE 1	RETURN WEAPONTYPE_UNARMED
						CASE 2	RETURN WEAPONTYPE_ASSAULTRIFLE
						CASE 3	RETURN WEAPONTYPE_ASSAULTRIFLE
						CASE 4	RETURN WEAPONTYPE_PISTOL
						CASE 5	RETURN WEAPONTYPE_SAWNOFFSHOTGUN
						CASE 6	RETURN WEAPONTYPE_PISTOL
					ENDSWITCH
				BREAK
				
				CASE 2
					SWITCH iPed
						CASE 0	RETURN WEAPONTYPE_UNARMED
						CASE 1	RETURN WEAPONTYPE_UNARMED
						CASE 2	RETURN WEAPONTYPE_SMG
						CASE 3	RETURN WEAPONTYPE_SMG
						CASE 4	RETURN WEAPONTYPE_PISTOL
						CASE 5	RETURN WEAPONTYPE_PISTOL
						CASE 6	RETURN WEAPONTYPE_MICROSMG
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN WEAPONTYPE_UNARMED
ENDFUNC

//Returns the corresponding vehicle array index for the specified ped, i.e. the vehicle that the ped should be in.
FUNC INT GET_VEH_PED_SHOULD_USE(INT iPed)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_POLICE_RAID
			SWITCH serverBD.iSubVariation
				CASE 0
					SWITCH iPed
						CASE 0	RETURN 2	//Pilot of police heli (hover in place).
						CASE 1	RETURN 2	//Rear right seat of the police heli (hanging out the side).
						CASE 2	RETURN 0
						CASE 3	RETURN 0
						CASE 4	RETURN 1
						CASE 5	RETURN 2	//Rear left seat of the police heli (hanging out the side).
						CASE 6	RETURN 1
						CASE 7	RETURN 4	//Driver of second Granger.
						CASE 8	RETURN 4	//Passenger, hanging off the right side of the second Granger.
						CASE 9	RETURN 4	//Passenger, hanging off the left side of the second Granger.
						CASE 10	RETURN 3	//Driver of first Granger.
						CASE 11	RETURN 3	//Passenger, hanging off the right side of the first Granger.
						CASE 12	RETURN 3	//Passenger, hanging off the left side of the first Granger.
						CASE 13	RETURN 4	//Passenger, front passenger seat of the second Granger.
					ENDSWITCH
				BREAK
				
				DEFAULT
					SWITCH iPed
						CASE 0	RETURN 0	
						CASE 1	RETURN 0	
						CASE 2	RETURN 0
						CASE 3	RETURN 0
						CASE 4	RETURN 2	//Pilot of police heli (hover in place).
						CASE 5	RETURN 2	//Rear right seat of the police heli (hanging out the side).
						CASE 6	RETURN 2	//Rear left seat of the police heli (hanging out the side).
						CASE 7	RETURN 3	//Driver of first Granger.
						CASE 8	RETURN 3	//Passenger, hanging off the right side of the first Granger.
						CASE 9	RETURN 3	//Passenger, hanging off the left side of the first Granger.
						CASE 10	RETURN 4	//Driver of second Granger.
						CASE 11	RETURN 4	//Passenger, front passenger seat of the second Granger.
						CASE 12	RETURN 4	//Passenger, hanging off the right side of the second Granger.
						CASE 13	RETURN 4	//Passenger, hanging off the left side of the second Granger.
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_RECOVER_PROPERTY
			SWITCH iPed
				CASE 0	RETURN 0	//bmx
				CASE 1	RETURN 1	//bmx
				CASE 2	RETURN 2	//bmx
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_COLLECT_FRIENDS	RETURN 0	//Supervolito for escorting the DJ's friends.
	ENDSWITCH
	
	RETURN -1
ENDFUNC

//Returns the vehicle seat that should be used by the specified ped.
FUNC VEHICLE_SEAT GET_SEAT_PED_SHOULD_USE(INT iPed)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_COLLECT_FRIENDS
			SWITCH iPed
				CASE 0	RETURN VS_FRONT_RIGHT
				CASE 1	RETURN VS_BACK_LEFT
				DEFAULT	RETURN VS_BACK_RIGHT
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN VS_ANY_PASSENGER
ENDFUNC

//Set the ped component variation for the specified ped.
PROC SET_PED_VARIATION(INT iPed, PED_INDEX piPed)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_POLICE_RAID
			SWITCH serverBD.iSubVariation
				CASE 0
				DEFAULT
					SWITCH iPed
						CASE 0
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HEAD, 1, 2)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HAIR, 3, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_TORSO, 1, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_LEG, 0, 2)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL, 0, 0)
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL2, 2, 0)
						BREAK
						
						CASE 1
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HEAD, 0, 2)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HAIR, 3, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_TORSO, 2, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_LEG, 0, 2)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL, 0, 0)
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL2, 2, 0)
						BREAK
						
						CASE 2
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HEAD, 0, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HAIR, 2, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_TORSO, 1, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_LEG, 0, 2)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL, 0, 0)
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL2, 2, 0)
							SET_PED_PROP_INDEX(piPed, ANCHOR_EYES, 0, 0)
						BREAK
						
						CASE 3
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HEAD, 1, 2)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HAIR, 3, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_TORSO, 1, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_LEG, 0, 2)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL, 0, 0)
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL2, 2, 0)
						BREAK
						
						CASE 4
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HEAD, 2, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HAIR, 3, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_TORSO, 2, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_LEG, 0, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL, 0, 0)
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL2, 2, 0)
						BREAK
						
						CASE 5
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HEAD, 0, 1)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HAIR, 3, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_TORSO, 1, 2)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_LEG, 0, 1)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL, 0, 0)
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL2, 2, 0)
						BREAK
						
						CASE 6
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HEAD, 2, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HAIR, 3, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_TORSO, 1, 1)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_LEG, 0, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL, 0, 0) 
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL2, 2, 0)
							SET_PED_PROP_INDEX(piPed, ANCHOR_EYES, 0, 0)
						BREAK
						
						CASE 7
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HEAD, 0, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HAIR, 3, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_TORSO, 2, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_LEG, 0, 2)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL, 0, 0) 
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL2, 2, 0)
							SET_PED_PROP_INDEX(piPed, ANCHOR_EYES, 0, 0)
						BREAK
						
						CASE 8
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HEAD, 2, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HAIR, 3, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_TORSO, 1, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_LEG, 0, 1)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL, 0, 0) 
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL2, 2, 0)
						BREAK
						
						CASE 9
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HEAD, 1, 1)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HAIR, 3, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_TORSO, 1, 2)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_LEG, 0, 1)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL, 0, 0) 
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL2, 2, 0)
						BREAK
						
						CASE 10
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HEAD, 1, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HAIR, 3, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_TORSO, 0, 2)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_LEG, 0, 2)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL, 0, 0) 
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL2, 2, 0)
							SET_PED_PROP_INDEX(piPed, ANCHOR_EYES, 0, 0)
						BREAK
						
						CASE 11
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HEAD, 0, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HAIR, 3, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_TORSO, 0, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_LEG, 0, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL, 0, 0) 
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL2, 2, 0)
						BREAK
						
						CASE 12
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HEAD, 2, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HAIR, 1, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_TORSO, 2, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_LEG, 0, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL, 0, 0) 
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL2, 2, 0)
						BREAK
						
						CASE 13
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HEAD, 1, 2)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HAIR, 2, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_TORSO, 1, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_LEG, 0, 2)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL, 0, 0) 
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL2, 2, 0)
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_STOLEN_VINYLS
			SWITCH iPed
				CASE 26
					SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HEAD, 0, 1)
                    SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HAIR, 0, 0)
                    SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_TORSO, 1, 0)
                    SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_LEG, 0, 2)
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_COLLECT_FRIENDS
			SWITCH GET_PLAYERS_NIGHTCLUB_ACTIVE_DJ(GB_GET_LOCAL_PLAYER_GANG_BOSS())
				CASE DJ_SOLOMUN
					SWITCH iPed
						CASE 0
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HEAD, 0, 1)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HAIR, 0, 1)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_TORSO, 1, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_LEG, 0, 2)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_DECL, 1, 0)
							SET_PED_PROP_INDEX(piPed, ANCHOR_EYES, 0, 2)
						BREAK
						
						CASE 1
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HEAD, 1, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_TORSO, 0, 3)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_LEG, 1, 0)
							SET_PED_PROP_INDEX(piPed, ANCHOR_HEAD, 2, 1)
						BREAK
						
						CASE 2
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HEAD, 0, 2)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_TORSO, 1, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_LEG, 0, 2)
							SET_PED_PROP_INDEX(piPed, ANCHOR_EYES, 0, 2)
							SET_PED_PROP_INDEX(piPed, ANCHOR_HEAD, 0, 1)
						BREAK
					ENDSWITCH
				BREAK
				
				CASE DJ_DIXON
					SWITCH iPed
						CASE 0
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HEAD, 0, 1)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HAIR, 0, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_TORSO, 1, 2)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_LEG, 0, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL, 1, 1)
							SET_PED_PROP_INDEX(piPed, ANCHOR_EYES, 1, 2)
						BREAK
						
						CASE 1
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HEAD, 0, 1)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HAIR, 0, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_TORSO, 1, 3)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_LEG, 1, 2)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL, 0, 1)
							SET_PED_PROP_INDEX(piPed, ANCHOR_EYES, 1, 0)
						BREAK
						
						CASE 2
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HEAD, 0, 2)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HAIR, 1, 1)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_TORSO, 1, 2)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_LEG, 0, 2)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL, 1, 0)
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL2, 2, 0)
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_DECL, 0, 0)
						BREAK
					ENDSWITCH
				BREAK
				
				CASE DJ_TALE_OF_US
					SWITCH iPed
						CASE 0
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HEAD, 0, 2)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_TORSO, 1, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_LEG, 0, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL, 0, 0)
							SET_PED_PROP_INDEX(piPed, ANCHOR_HEAD, 0, 1)
						BREAK
						
						CASE 1
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HEAD, 0, 2)
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HAIR, 1, 2)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_TORSO, 0, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_LEG, 2, 0)
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL, 0, 0)
							SET_PED_PROP_INDEX(piPed, ANCHOR_EYES, 0, 1)
						BREAK
						
						CASE 2
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HEAD, 0, 2)
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HAIR, 2, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_TORSO, 0, 2)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_LEG, 0, 0)
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL, 1, 0)
							SET_PED_PROP_INDEX(piPed, ANCHOR_HEAD, 1, 1)
						BREAK
					ENDSWITCH
				BREAK
				
				CASE DJ_BLACK_MADONNA
					SWITCH iPed
						CASE 0
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HEAD, 0, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HAIR, 1, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_TORSO, 1, 2)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_LEG, 1, 0)
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_TEETH, 0, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL, 0, 0)
							SET_PED_PROP_INDEX(piPed, ANCHOR_EYES, 0, 0)
						BREAK
						
						CASE 1
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HEAD, 1, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HAIR, 1, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_TORSO, 0, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_LEG, 0, 0)
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HAND, 0, 0)
						BREAK
						
						CASE 2
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HEAD, 1, 2)
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HAIR, 0, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_TORSO, 0, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_LEG, 0, 0)
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_DECL, 1, 0)
							SET_PED_PROP_INDEX(piPed, ANCHOR_EYES, 0, 0)
							SET_PED_PROP_INDEX(piPed, ANCHOR_HEAD, 0, 2)
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_HOLD_UP
			SWITCH GET_PLAYERS_NIGHTCLUB_ACTIVE_DJ(GB_GET_LOCAL_PLAYER_GANG_BOSS())
				CASE DJ_SOLOMUN
					SWITCH iPed
						CASE 0
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HEAD, 0, 1)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HAIR, 0, 1)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_TORSO, 1, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_LEG, 0, 2)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_DECL, 1, 0)
							SET_PED_PROP_INDEX(piPed, ANCHOR_EYES, 0, 2)
						BREAK
						
						CASE 1
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HEAD, 1, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_TORSO, 0, 3)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_LEG, 1, 0)
							SET_PED_PROP_INDEX(piPed, ANCHOR_HEAD, 2, 1)
						BREAK
					ENDSWITCH
				BREAK
				
				CASE DJ_DIXON
					SWITCH iPed
						CASE 0
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HEAD, 0, 1)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HAIR, 0, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_TORSO, 1, 2)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_LEG, 0, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL, 1, 1)
							SET_PED_PROP_INDEX(piPed, ANCHOR_EYES, 1, 2)
						BREAK
						
						CASE 1
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HEAD, 0, 2)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HAIR, 1, 1)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_TORSO, 1, 2)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_LEG, 0, 2)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL, 1, 0)
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL2, 2, 0)
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_DECL, 0, 0)
						BREAK
					ENDSWITCH
				BREAK
				
				CASE DJ_TALE_OF_US
					SWITCH iPed
						CASE 0
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HEAD, 0, 2)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_TORSO, 1, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_LEG, 0, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL, 0, 0)
							SET_PED_PROP_INDEX(piPed, ANCHOR_HEAD, 0, 1)
						BREAK
						
						CASE 1
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HEAD, 0, 2)
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HAIR, 2, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_TORSO, 0, 2)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_LEG, 0, 0)
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL, 1, 0)
							SET_PED_PROP_INDEX(piPed, ANCHOR_HEAD, 1, 1)
						BREAK
					ENDSWITCH
				BREAK
				
				CASE DJ_BLACK_MADONNA
					SWITCH iPed
						CASE 0
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HEAD, 0, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HAIR, 1, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_TORSO, 1, 2)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_LEG, 1, 0)
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_TEETH, 0, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL, 0, 0)
							SET_PED_PROP_INDEX(piPed, ANCHOR_EYES, 0, 0)
						BREAK
						
						CASE 1
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HEAD, 1, 2)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HAIR, 0, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_TORSO, 0, 0)
	                        SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_LEG, 1, 2)
							SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_DECL, 1, 0)
							SET_PED_PROP_INDEX(piPed, ANCHOR_EYES, 0, 0)
							SET_PED_PROP_INDEX(piPed, ANCHOR_HEAD, 0, 2)
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

//Get the scenario for the specified ped.
FUNC TEXT_LABEL_63 GET_PED_SCENARIO(INT iPed)
	TEXT_LABEL_63 tl63Scen
	
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_RECOVER_PROPERTY
			SWITCH serverBD.iSubVariation
				CASE 0
				CASE 1
					SWITCH iPed
						CASE 3	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"	BREAK
						CASE 4	tl63Scen = "WORLD_HUMAN_DRINKING"			BREAK
						CASE 5	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"	BREAK
						CASE 6	tl63Scen = "WORLD_HUMAN_SMOKING"			BREAK
					ENDSWITCH
				BREAK
				
				CASE 2
					SWITCH iPed
						CASE 3	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"	BREAK
						CASE 4	tl63Scen = "WORLD_HUMAN_DRINKING"			BREAK
						CASE 5	tl63Scen = "WORLD_HUMAN_STAND_MOBILE"		BREAK
						CASE 6	tl63Scen = "WORLD_HUMAN_SMOKING"			BREAK
					ENDSWITCH
				BREAK
				
				CASE 3
					SWITCH iPed
						CASE 3	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"	BREAK
						CASE 4	tl63Scen = "WORLD_HUMAN_SMOKING"			BREAK
						CASE 5	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"	BREAK
						CASE 6	tl63Scen = "WORLD_HUMAN_SMOKING"			BREAK
					ENDSWITCH
				BREAK
				
				CASE 4
					SWITCH iPed
						CASE 3	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"	BREAK
						CASE 4	tl63Scen = "WORLD_HUMAN_DRINKING"			BREAK
						CASE 5	tl63Scen = "WORLD_HUMAN_SMOKING_POT"		BREAK
						CASE 6	tl63Scen = "WORLD_HUMAN_DRINKING"			BREAK
					ENDSWITCH
				BREAK
				
				CASE 5
					SWITCH iPed
						CASE 3	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"	BREAK
						CASE 4	tl63Scen = "WORLD_HUMAN_DRINKING"			BREAK
						CASE 5	tl63Scen = "WORLD_HUMAN_SMOKING_POT"		BREAK
						CASE 6	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"	BREAK
					ENDSWITCH
				BREAK
				
				CASE 6
					SWITCH iPed
						CASE 3	tl63Scen = "WORLD_HUMAN_SMOKING_POT"		BREAK
						CASE 4	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"	BREAK
						CASE 5	tl63Scen = "WORLD_HUMAN_WELDING"			BREAK
						CASE 6	tl63Scen = "WORLD_HUMAN_GUARD_STAND"		BREAK
					ENDSWITCH
				BREAK
				
				CASE 7
					SWITCH iPed
						CASE 3	tl63Scen = "WORLD_HUMAN_SMOKING_POT"		BREAK
						CASE 4	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"	BREAK
						CASE 5	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"	BREAK
						CASE 6	tl63Scen = "WORLD_HUMAN_DRINKING"			BREAK
					ENDSWITCH
				BREAK
				
				CASE 8
				CASE 9
					SWITCH iPed
						CASE 3	tl63Scen = "WORLD_HUMAN_SMOKING_POT"		BREAK
						CASE 4	tl63Scen = "WORLD_HUMAN_DRINKING"			BREAK
						CASE 5	tl63Scen = "WORLD_HUMAN_DRINKING"			BREAK
						CASE 6	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_STOLEN_VINYLS
			SWITCH iPed
				CASE 1	tl63Scen = "WORLD_HUMAN_PARTYING"			BREAK
				CASE 2	tl63Scen = "WORLD_HUMAN_PARTYING"			BREAK
				CASE 3	tl63Scen = "WORLD_HUMAN_DRINKING"			BREAK
				CASE 4 	tl63Scen = "WORLD_HUMAN_MUSCLE_FLEX"		BREAK
				CASE 6 	tl63Scen = "WORLD_HUMAN_DRINKING"			BREAK
				CASE 7	tl63Scen = "WORLD_HUMAN_PARTYING" 			BREAK
				CASE 8	tl63Scen = "WORLD_HUMAN_PARTYING"			BREAK
				CASE 10	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"	BREAK
				CASE 11	tl63Scen = "WORLD_HUMAN_DRINKING"			BREAK
				CASE 12	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"	BREAK
				CASE 13	tl63Scen = "WORLD_HUMAN_DRINKING"			BREAK
				CASE 14	tl63Scen = "WORLD_HUMAN_PARTYING"			BREAK
				CASE 15	tl63Scen = "WORLD_HUMAN_DRINKING"			BREAK
				CASE 16	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"	BREAK
				CASE 17	tl63Scen = "WORLD_HUMAN_MUSCLE_FLEX"		BREAK
				CASE 19	tl63Scen = "WORLD_HUMAN_PARTYING"			BREAK
				CASE 20	tl63Scen = "WORLD_HUMAN_DRINKING"			BREAK
				CASE 21	tl63Scen = "WORLD_HUMAN_DRINKING"			BREAK
				CASE 22	tl63Scen = "WORLD_HUMAN_PARTYING"			BREAK
				CASE 24	tl63Scen = "WORLD_HUMAN_DRINKING"			BREAK
				CASE 25	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"	BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_COLLECT_FRIENDS
			SWITCH serverBD.iSubVariation
				CASE 0 
					SWITCH iPed
						CASE 0	tl63Scen = "WORLD_HUMAN_STAND_MOBILE"		BREAK
						CASE 1 	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"	BREAK
						CASE 2 	tl63Scen = "WORLD_HUMAN_DRINKING"			BREAK
					ENDSWITCH
				BREAK
				
				CASE 1 
					SWITCH iPed
						CASE 0	tl63Scen = "WORLD_HUMAN_WINDOW_SHOP_BROWSE"	BREAK
						CASE 1 	tl63Scen = "WORLD_HUMAN_DRINKING"			BREAK
						CASE 2 	tl63Scen = "WORLD_HUMAN_SMOKING"			BREAK
					ENDSWITCH
				BREAK
				
				CASE 2
					SWITCH iPed
						CASE 0	tl63Scen = "WORLD_HUMAN_SMOKING"			BREAK
						CASE 1 	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"	BREAK
						CASE 2 	tl63Scen = "WORLD_HUMAN_DRINKING"			BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT
			SWITCH serverBD.iSubVariation
				CASE 0
					SWITCH iPed
						CASE 0	tl63Scen = "WORLD_HUMAN_CLIPBOARD"		BREAK
						CASE 1	tl63Scen = "WORLD_HUMAN_SMOKING"		BREAK
						CASE 2 	tl63Scen = "WORLD_HUMAN_COP_IDLES"		BREAK
						CASE 3 	tl63Scen = "WORLD_HUMAN_GUARD_STAND"	BREAK
					ENDSWITCH
				BREAK
				
				CASE 1
					SWITCH iPed
						CASE 0	tl63Scen = "WORLD_HUMAN_WINDOW_SHOP_BROWSE"	BREAK
						CASE 1	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"	BREAK
						CASE 2 	tl63Scen = "WORLD_HUMAN_GUARD_STAND"		BREAK
						CASE 3 	tl63Scen = "WORLD_HUMAN_CLIPBOARD"			BREAK
					ENDSWITCH
				BREAK
				
				CASE 2
					SWITCH iPed
						CASE 0	tl63Scen = "WORLD_HUMAN_WINDOW_SHOP_BROWSE"	BREAK
						CASE 1	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"	BREAK
						CASE 2 	tl63Scen = "WORLD_HUMAN_CLIPBOARD"			BREAK
						CASE 3 	tl63Scen = "WORLD_HUMAN_GUARD_STAND"		BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_HOLD_UP
			SWITCH serverBD.iSubVariation
				CASE 0 
					SWITCH iPed
						CASE 4 	tl63Scen = "WORLD_HUMAN_STAND_MOBILE"	BREAK
						CASE 5 	tl63Scen = "WORLD_HUMAN_SMOKING"		BREAK
						CASE 6 	tl63Scen = "WORLD_HUMAN_DRINKING"		BREAK
					ENDSWITCH
				BREAK
				
				CASE 1 
					SWITCH iPed
						CASE 4 	tl63Scen = "WORLD_HUMAN_GUARD_STAND"	BREAK
						CASE 5 	tl63Scen = "WORLD_HUMAN_SMOKING"		BREAK
						CASE 6 	tl63Scen = "WORLD_HUMAN_DRINKING"		BREAK
					ENDSWITCH
				BREAK
				
				CASE 2 
					SWITCH iPed
						CASE 4 	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"	BREAK
						CASE 5 	tl63Scen = "WORLD_HUMAN_DRINKING"			BREAK
						CASE 6 	tl63Scen = "WORLD_HUMAN_SMOKING"			BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN tl63Scen
ENDFUNC

//Get the animation dictionary for the specified ped.
FUNC STRING GET_PED_ANIM_DICT(INT iPed)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_STOLEN_VINYLS
			SWITCH iPed
				CASE 0
				CASE 5
				CASE 9
				CASE 18
				CASE 23	RETURN "mini@strip_club@lap_dance@ld_girl_a_song_a_p1"
				
				CASE 26	RETURN "mini@strip_club@idles@dj@idle_01"
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_COLLECT_FRIENDS
			SWITCH iPed
				CASE 2 RETURN "veh@helicopter@ps@idle_panic"
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

//Get the animation string for the specified ped.
FUNC STRING GET_PED_ANIM_STRING(INT iPed)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_STOLEN_VINYLS
			SWITCH iPed
				CASE 0
				CASE 5
				CASE 9
				CASE 18
				CASE 23	RETURN "ld_girl_a_song_a_p1_f"
				
				CASE 26	RETURN "idle_01"
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_COLLECT_FRIENDS
			SWITCH iPed
				CASE 2 RETURN "sit"
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC FLOAT GET_PED_ANIM_BLEND_IN(INT iPed)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_COLLECT_FRIENDS
			SWITCH iPed
				CASE 2	RETURN REALLY_SLOW_BLEND_IN
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN NORMAL_BLEND_IN
ENDFUNC

FUNC FLOAT GET_PED_ANIM_BLEND_OUT(INT iPed)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_COLLECT_FRIENDS
			SWITCH iPed
				CASE 2	RETURN REALLY_SLOW_BLEND_OUT
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN NORMAL_BLEND_OUT
ENDFUNC

//Get the animation flags for the specified ped.
FUNC ANIMATION_FLAGS GET_PED_ANIM_FLAGS(INT iPed)
	ANIMATION_FLAGS afAnimFlags
	
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_STOLEN_VINYLS
			SWITCH iPed
				CASE 0
				DEFAULT	afAnimFlags = AF_LOOPING	BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_COLLECT_FRIENDS
			SWITCH iPed
				CASE 2	afAnimFlags = AF_LOOPING	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN afAnimFlags
ENDFUNC

//Returns the co-ordinate to aim at.
FUNC VECTOR GET_AIM_POSITION(INT iPed)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_POLICE_RAID
			SWITCH serverBD.iSubVariation
				CASE 0	RETURN	<<733.7202, -1291.1652, 25.2848>>
				CASE 1	RETURN	<<333.2541, -994.5491, 28.3229>>
				CASE 2	RETURN	<<-163.8887, -1292.6212, 30.3185>>
				CASE 3	RETURN	<<-21.7931, 219.5366, 105.7258>>
				CASE 4	RETURN	<<908.8712, -2097.1448, 29.5537>>
				CASE 5	RETURN <<-670.8386, -2392.5562, 12.9445>>
				CASE 6	RETURN <<227.7781, -3136.8979, 4.7903>>
				CASE 7	RETURN <<377.8100, 228.4226, 102.0406>>
				CASE 8	RETURN <<-1237.5542, -693.9150, 22.7756>>
				CASE 9	RETURN <<-1173.5220, -1173.8550, 4.6236>>
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_HOLD_UP
			SWITCH serverBD.iSubVariation
				CASE 0
					SWITCH iPed
						CASE 2	RETURN	<<-1079.2061, -1678.1100, 3.5750>>
						CASE 3	RETURN	<<-1076.7020, -1679.0580, 3.5750>>
					ENDSWITCH
				BREAK
				
				CASE 1
					SWITCH iPed
						CASE 2	RETURN	<<1988.9630, 3789.8169, 31.1810>>
						CASE 3	RETURN	<<1987.2310, 3792.2671, 31.1810>>
					ENDSWITCH
				BREAK
				
				CASE 2
					SWITCH iPed
						CASE 2	RETURN	<<32.1460, -1926.1510, 20.7430>>
						CASE 3	RETURN	<<29.7360, -1929.2581, 20.8740>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN <<0, 0, 0>>
ENDFUNC

//Get the distance within which peds will join the player's group.
FUNC FLOAT GET_GROUP_JOIN_DISTANCE()
	RETURN 10.0
ENDFUNC

//Get the distance outside which peds will leave the player's group.
FUNC FLOAT GET_GROUP_LEAVE_DISTANCE()
	RETURN 35.0
ENDFUNC

//Get the distance to use as spacing in the current group formation.
FUNC FLOAT GET_GROUP_FORMATION_SPACING()
	RETURN 2.0
ENDFUNC

//Determines whether the given ped is a civilian (i.e. should they be targettable and blipped).
FUNC BOOL IS_PED_CIVILIAN(INT iPed)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_STOLEN_VINYLS
			SWITCH iPed
				CASE 0
				CASE 1
				CASE 2
				CASE 5
				CASE 6
				CASE 7
				CASE 8
				CASE 9
				CASE 12
				CASE 13
				CASE 14
				CASE 16
				CASE 17
				CASE 18
				CASE 19
				CASE 22
				CASE 23
				CASE 24
				CASE 25
				CASE 26	RETURN TRUE
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_COLLECT_FRIENDS	RETURN TRUE
		
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT
			SWITCH iPed
				CASE 0	RETURN TRUE
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_HOLD_UP
			SWITCH iPed
				CASE 0
				CASE 1	RETURN TRUE
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

//Determines whether the given ped is a friendly (i.e. should it be possible to shoot at them directly).
FUNC BOOL IS_PED_FRIENDLY(INT iPed)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_COLLECT_FRIENDS	RETURN TRUE
		
		CASE FMBB_DEFEND_HOLD_UP
			SWITCH iPed
				CASE 0
				CASE 1	RETURN TRUE
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_PED_USE_HANGING_SEAT(INT iPed)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_POLICE_RAID
			SWITCH iPed
				CASE 8
				CASE 9
				CASE 12
				CASE 13	RETURN TRUE
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_PED_SPEAK(INT iPed)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_STOLEN_VINYLS
			SWITCH iPed
				CASE 0
				CASE 5
				CASE 13		RETURN TRUE
				DEFAULT		RETURN FALSE
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

//Set some general and specific attributes for the given vehicle.
PROC SET_ENEMY_VEHICLE_ATTRIBUTES(INT iVeh, BOOL bLockRearDoors)
	VEHICLE_INDEX viEntity = NET_TO_VEH(serverBD.niVeh[iVeh])
	
	//General attributes.
	SET_VEHICLE_DOORS_LOCKED_FOR_NON_SCRIPT_PLAYERS(viEntity, TRUE)
	SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(viEntity, TRUE)
	SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(viEntity, TRUE) 
	
	IF bLockRearDoors
		SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(viEntity, ENUM_TO_INT(SC_DOOR_REAR_LEFT), VEHICLELOCK_LOCKED)
		SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(viEntity, ENUM_TO_INT(SC_DOOR_REAR_RIGHT), VEHICLELOCK_LOCKED)
	ENDIF
	
	//Prevent the vehicle from being saved in the player's garage.
	IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh", DECOR_TYPE_INT)
		DECOR_SET_INT(viEntity, "Not_Allow_As_Saved_Veh", MP_NOT_ALLOWED_AS_SAVED_VEHICLE_DEC_SIMEON)
		PRINTLN("[FMBB_DEFEND] - [SET_ENEMY_VEHICLE_ATTRIBUTES] - iVeh = ", iVeh, " set as 'Not_Allow_As_Saved_Veh'.")
	ENDIF
	
	//Prevent the vehicle from being used for activities that use passive mode or being modded.
	IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
		INT iDecoratorValue
		
		IF DECOR_EXIST_ON(NET_TO_VEH(serverBD.niVeh[iVeh]), "MPBitset")
		      iDecoratorValue = DECOR_GET_INT(viEntity, "MPBitset")
		ENDIF
		
		SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_PASSIVE_MODE_VEHICLE)
		SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_MODDABLE_VEHICLE)
		DECOR_SET_INT(viEntity, "MPBitset", iDecoratorValue)

		PRINTLN("[FMBB_DEFEND] - [SET_ENEMY_VEHICLE_ATTRIBUTES] - iVeh = ", iVeh, " set as 'MP_DECORATOR_BS_NON_PASSIVE_MODE_VEHICLE'.")
		PRINTLN("[FMBB_DEFEND] - [SET_ENEMY_VEHICLE_ATTRIBUTES] - iVeh = ", iVeh, " set as 'MP_DECORATOR_BS_NON_MODDABLE_VEHICLE'.")
	ENDIF
	
	//Variation specific attributes.
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_POLICE_RAID
			SWITCH iVeh
				CASE 0
				CASE 1
					SET_VEHICLE_ENGINE_ON(viEntity, TRUE, TRUE)
					SET_VEHICLE_SIREN(viEntity, TRUE)
					SET_VEHICLE_HAS_MUTED_SIRENS(viEntity, FALSE)
					SET_VEHICLE_COLOURS(viEntity, 7, 0)
					SET_VEHICLE_EXTRA_COLOURS(viEntity, 134, 0)
				BREAK
				
				CASE 2
					SET_VEHICLE_ENGINE_ON(viEntity, TRUE, TRUE)
					SET_VEHICLE_LIVERY(viEntity, 0)
					SET_HELI_BLADES_FULL_SPEED(viEntity)
					SET_ENTITY_DYNAMIC(viEntity, TRUE)
					ACTIVATE_PHYSICS(viEntity)
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_RECOVER_PROPERTY
			SWITCH serverBD.iSubVariation
				CASE 0
				CASE 3
					SWITCH iVeh
						CASE 3
							SET_VEHICLE_COLOURS(viEntity, 67, 67)
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 1
				CASE 4
				CASE 6
				CASE 8
				CASE 9
					SWITCH iVeh
						CASE 3
							SET_VEHICLE_COLOURS(viEntity, 89, 89)
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 2
				CASE 7
					SWITCH iVeh
						CASE 3
							SET_VEHICLE_COLOURS(viEntity, 145, 145)
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 5
					SWITCH iVeh
						CASE 3
							SET_VEHICLE_COLOURS(viEntity, 53, 53)
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_STOLEN_VINYLS
			SWITCH iVeh
				CASE 0
				CASE 1
				CASE 2
				CASE 3
				CASE 4
					SET_BOAT_ANCHOR(viEntity, TRUE)
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_HOLD_UP
			SWITCH serverBD.iSubVariation
				CASE 0
					SWITCH iVeh
						CASE 0
						CASE 1
							SET_VEHICLE_COLOURS(viEntity, 88, 88)
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 2
					SWITCH iVeh
						CASE 0
						CASE 1
							SET_VEHICLE_COLOURS(viEntity, 145, 145)
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

//Returns the number of scenario blocking areas.
FUNC INT GET_NUMBER_OF_SCENARIO_BLOCKING_AREAS()
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_RECOVER_PROPERTY
			SWITCH serverBD.iSubVariation
				CASE 0
				CASE 3
				CASE 4
				CASE 5
				CASE 6
				CASE 7
				CASE 8
				CASE 9	RETURN 1
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_STOLEN_VINYLS
			SWITCH serverBD.iSubVariation
				CASE 0	RETURN 1
				CASE 1	RETURN 1
				CASE 2	RETURN 0
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_COLLECT_FRIENDS	RETURN 1
		
		CASE FMBB_DEFEND_HOLD_UP
			SWITCH serverBD.iSubVariation
				CASE 1
				CASE 2	RETURN 1
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

//Retrieves the position and radius of the specified scenario blocking area.
PROC GET_SCENARIO_BLOCKING_AREA_RADIUS_DETAILS(INT iScenBlock, VECTOR &vLoc, FLOAT &fRadius)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_RECOVER_PROPERTY
			SWITCH serverBD.iSubVariation
				CASE 0
					SWITCH iScenBlock
						CASE 0
							vLoc = <<982.4930, -1400.0300, 30.5230>>	fRadius = 5
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 3
					SWITCH iScenBlock
						CASE 0
							vLoc = <<80.9490, 273.9890, 109.2100>>		fRadius = 5
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 4
					SWITCH iScenBlock
						CASE 0
							vLoc = <<782.1900, -2379.8120, 21.4760>>	fRadius = 5
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 5
					SWITCH iScenBlock
						CASE 0
							vLoc = <<-1065.5320, -2082.3269, 12.2910>>	fRadius = 8
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 6
					SWITCH iScenBlock
						CASE 0
							vLoc = <<370.1080, -2451.9431, 5.4950>>		fRadius = 5
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 7
					SWITCH iScenBlock
						CASE 0
							vLoc = <<114.6440, -6.5620, 66.8100>>		fRadius = 5
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 9
					SWITCH iScenBlock
						CASE 0
							vLoc = <<-892.7380, -1162.3110, 3.8980>>	fRadius = 5
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_COLLECT_FRIENDS
			SWITCH serverBD.iSubVariation
				CASE 0
					SWITCH iScenBlock
						CASE 0
							vLoc = <<734.3821, 2522.4001, 72.2236>>		fRadius = 5
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 1
					SWITCH iScenBlock
						CASE 0
							vLoc = <<1397.6600, 3600.0669, 34.0140>>	fRadius = 5
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 2
					SWITCH iScenBlock
						CASE 0
							vLoc = <<2619.2405, 3274.6438, 54.7487>>	fRadius = 5
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_HOLD_UP
			SWITCH serverBD.iSubVariation
				CASE 1
					SWITCH iScenBlock
						CASE 0
							vLoc = <<1999.1071, 3796.9031, 31.1810>>	fRadius = 10
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 2
					SWITCH iScenBlock
						CASE 0
							vLoc = <<46.5690, -1914.8790, 20.6570>>		fRadius = 12
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

//Returns the number of traffic reduction zones.
FUNC INT GET_NUMBER_OF_TRAFFIC_REDUCTION_ZONES()
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_POLICE_RAID
			SWITCH serverBD.iSubVariation
				CASE 7	RETURN 1
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

//Retrieves the position and radius of the specified traffic reduction zone.
PROC GET_TRAFFIC_REDUCTION_ZONE_DETAILS(INT iTrafficZone, VECTOR &vLoc, FLOAT &fRadius)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_POLICE_RAID
			SWITCH serverBD.iSubVariation
				CASE 7
					SWITCH iTrafficZone
						CASE 0
							vLoc = <<377.8100, 228.4226, 102.0406>> fRadius = 55.0
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

//Enables or disables vehicle generators in a specific area for the current variation/subvariation.
PROC SET_VEHICLE_GENERATORS(BOOL bActive)
	VECTOR vMin = <<0, 0, 0>>
	VECTOR vMax = <<0, 0, 0>>
	
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_RECOVER_PROPERTY
			SWITCH serverBD.iSubVariation
				CASE 0	vMin = <<977.28070, -1409.77307, 29.33489>>		vMax = <<987.47815, -1399.01453, 35.51195>>		BREAK
				CASE 1	vMin = <<372.00452, -1110.42139, 27.40642>> 	vMax = <<378.40652, -1120.15479, 31.40565>> 	BREAK
				CASE 4	vMin = <<958.75946, -1884.68677, 29.25038>>		vMax = <<978.47284, -1867.15820, 34.34451>>		BREAK
				CASE 9	vMin = <<-1571.92676, -986.60254, 11.01754>>	vMax = <<-1546.93054, -969.73132, 16.01739>>	BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_COLLECT_FRIENDS
			SWITCH GET_PLAYERS_OWNED_NIGHTCLUB(GB_GET_LOCAL_PLAYER_GANG_BOSS())
				CASE NIGHTCLUB_MISSION_ROW	vMin = <<334.23630, -954.69531, 26.44322>>	vMax = <<364.22540, -945.82220, 32.43410>>	BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT
			SWITCH serverBD.iSubVariation
				CASE 0	vMin = <<-980.27509, -274.67896, 36.15507>>	vMax = <<-970.99536, -263.46292, 41.59630>>	BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_HOLD_UP
			SWITCH serverBD.iSubVariation
				CASE 2	vMin = <<35.95770, -1927.15479, 19.33969>>	vMax = <<51.56091, -1909.79358, 25.62746>>	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	IF NOT IS_VECTOR_ZERO(vMin) AND NOT IS_VECTOR_ZERO(vMax)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vMin, vMax, bActive, FALSE)
		PRINTLN("[FMBB_DEFEND] - [SET_VEHICLE_GENERATORS] bActive = ", bActive)
	ENDIF
ENDPROC

//Get the angled defensive area for enemy ped, iPed, to use (if there is one). iPed = -1 will return the area to be used by all peds (if there is one).
PROC GET_ANGLED_DEFENSIVE_AREA(VECTOR &vVec1, VECTOR &vVec2, FLOAT &fRectangleWidth, INT iPed = -1)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_RECOVER_PROPERTY
			SWITCH serverBD.iSubVariation
				CASE 0
					SWITCH iPed
						CASE 3
						CASE 4	vVec1 = <<806.283447,-1068.936401,22.301254>>	vVec2 = <<806.392822,-1077.942383,32.589230>>	fRectangleWidth = 5.0	BREAK

						CASE 5
						CASE 6	vVec1 = <<981.024170,-1397.256836,25.613104>>	vVec2 = <<983.510681,-1401.708740,34.485661>>	fRectangleWidth = 5.0	BREAK
					ENDSWITCH
				BREAK
				
				CASE 1
					SWITCH iPed
						CASE 3
						CASE 4	vVec1 = <<463.111023,-718.330505,21.579481>>	vVec2 = <<458.043030,-718.310181,31.359077>>	fRectangleWidth = 5.0	BREAK

						CASE 5
						CASE 6	vVec1 = <<378.168488,-1118.338379,23.406424>>	vVec2 = <<378.249939,-1113.088379,33.406425>>	fRectangleWidth = 5.0	BREAK
					ENDSWITCH
				BREAK
				
				CASE 2
					SWITCH iPed
						CASE 3
						CASE 4	vVec1 = <<-217.075562,-1502.478638,25.449959>>	vVec2 = <<-212.292328,-1497.162476,36.099190>>	fRectangleWidth = 5.0	BREAK

						CASE 5
						CASE 6	vVec1 = <<50.352421,-1001.876892,23.531376>>	vVec2 = <<51.537514,-998.528015,33.357395>>		fRectangleWidth = 5.0	BREAK
					ENDSWITCH
				BREAK
				
				CASE 3
					SWITCH iPed
						CASE 3
						CASE 4	vVec1 = <<81.959015,275.275665,104.337624>>		vVec2 = <<80.799355,271.855743,114.210236>>		fRectangleWidth = 5.0	BREAK

						CASE 5
						CASE 6	vVec1 = <<-93.059067,-66.029793,50.817951>>		vVec2 = <<-94.844734,-71.648376,60.788963>>		fRectangleWidth = 4.0	BREAK
					ENDSWITCH
				BREAK
				
				CASE 4
					SWITCH iPed
						CASE 3
						CASE 4	vVec1 = <<966.101685,-1866.861938,25.452579>>	vVec2 = <<965.755249,-1870.433594,35.264580>>	fRectangleWidth = 5.0	BREAK

						CASE 5
						CASE 6	vVec1 = <<785.711060,-2380.750732,16.500237>>	vVec2 = <<779.900574,-2380.255615,26.391762>>	fRectangleWidth = 5.0	BREAK
					ENDSWITCH
				BREAK
				
				CASE 5
					SWITCH iPed
						CASE 3
						CASE 4	vVec1 = <<-234.839142,-1993.377197,23.946037>>	vVec2 = <<-228.446991,-1993.890381,33.946442>>	fRectangleWidth = 10.0	BREAK

						CASE 5
						CASE 6	vVec1 = <<-1066.749756,-2084.673584,7.541454>>	vVec2 = <<-1061.740845,-2079.792236,18.466562>>	fRectangleWidth = 10.0	BREAK
					ENDSWITCH
				BREAK
				
				CASE 6
					SWITCH iPed
						CASE 3
						CASE 4	vVec1 = <<-286.913818,-2496.053711,4.414141>>	vVec2 = <<-290.827209,-2500.351074,10.988848>>	fRectangleWidth = 10.0	BREAK

						CASE 5
						CASE 6	vVec1 = <<370.768799,-2452.897217,3.742084>>	vVec2 = <<365.575348,-2452.825684,10.210866>>	fRectangleWidth = 10.0	BREAK
					ENDSWITCH
				BREAK
				
				CASE 7
					SWITCH iPed
						CASE 3
						CASE 4	vVec1 = <<-57.941463,381.776428,110.430771>>	vVec2 = <<-81.183205,392.552917,116.430771>>	fRectangleWidth = 11.0	BREAK

						CASE 5
						CASE 6	vVec1 = <<111.573997,-12.427261,65.809669>>		vVec2 = <<116.793449,1.791802,71.782349>>		fRectangleWidth = 15.0	BREAK
					ENDSWITCH
				BREAK
				
				CASE 8
					SWITCH iPed
						CASE 3
						CASE 4	vVec1 = <<-1358.711914,-963.777893,6.831060>>	vVec2 = <<-1363.002808,-966.661865,13.108795>>	fRectangleWidth = 10.0	BREAK

						CASE 5
						CASE 6	vVec1 = <<-1333.938232,-390.768829,33.665890>>	vVec2 = <<-1341.213623,-395.005768,40.629047>>	fRectangleWidth = 5.0	BREAK
					ENDSWITCH
				BREAK
				
				CASE 9
					SWITCH iPed
						CASE 3
						CASE 4	vVec1 = <<-892.592407,-1160.951904,2.759018>>	vVec2 = <<-889.585388,-1165.892700,8.896853>>	fRectangleWidth = 10.0	BREAK

						CASE 5
						CASE 6	vVec1 = <<-1565.303467,-976.074585,10.587510>>	vVec2 = <<-1560.278809,-980.256592,17.017391>>	fRectangleWidth = 10.0	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT
			SWITCH serverBD.iSubVariation
				CASE 0
					vVec1 = <<-970.215088,-281.663788,36.212509>>	vVec2 = <<-978.944946,-264.929199,42.556309>>	fRectangleWidth = 16.0
				BREAK
				
				CASE 1
					vVec1 = <<479.083221,-1533.315674,26.297249>>	vVec2 = <<489.413971,-1520.813965,36.046005>>	fRectangleWidth = 6.0
				BREAK
				
				CASE 2
					vVec1 = <<1986.072266,3046.620117,42.739033>>	vVec2 = <<1981.817017,3039.377197,52.056297>>	fRectangleWidth = 15.0
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_HOLD_UP
			SWITCH serverBD.iSubVariation
				CASE 0
					vVec1 = <<-1080.372681,-1679.561279,6.575233>>	vVec2 = <<-1068.221191,-1667.975098,2.451617>>	fRectangleWidth = 13.0
				BREAK
				
				CASE 1
					vVec1 = <<1985.815186,3790.089600,30.185287>>	vVec2 = <<2003.501831,3800.742188,35.180801>>	fRectangleWidth = 7.0
				BREAK
				
				CASE 2
					vVec1 = <<25.582731,-1926.841797,20.008661>>	vVec2 = <<39.735558,-1909.692383,23.627853>>	fRectangleWidth = 15.0
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

//Get the defensive sphere for enemies to use.
PROC GET_DEFENSIVE_SPHERE(VECTOR &vCentre, FLOAT &fRadius)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_POLICE_RAID
			vCentre = GET_LOCATION()	fRadius = 50.0
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_SPAWN_SEARCH_PARAMS_AVOID_ANGLED_AREA(VECTOR &vAvoidAngledAreaPos1, VECTOR &vAvoidAngledAreaPos2, FLOAT &fAvoidAngledAreaWidth)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_POLICE_RAID
			SWITCH serverBD.iSubVariation
				CASE 8
					vAvoidAngledAreaPos1 = <<-1250.162109,-626.513489,20.201584>>	vAvoidAngledAreaPos2 = <<-1179.627319,-707.430603,45.693459>>	fAvoidAngledAreaWidth = 42.000000
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

//Get the time (in seconds) that a player can stay in a warning area before provoking enemies.
FUNC INT GET_WARNING_AREA_TIME_LIMIT()
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT
			SWITCH serverBD.iSubVariation
				CASE 0
				CASE 1
				CASE 2	RETURN 1000 * 10 //10 seconds.
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

//Check whether the local player is in a warning area.
FUNC BOOL IS_PLAYER_IN_WARNING_AREA()
	BOOL bWarning = FALSE
	
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT
			SWITCH serverBD.iSubVariation
				CASE 0
					bWarning = 	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-970.215088,-281.663788,36.212509>>, <<-978.944946,-264.929199,42.556309>>, 16.0)
								OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-964.059692,-284.812408,27.735825>>, <<-966.982788,-279.308472,42.460201>>, 2.0)
				BREAK
				
				CASE 1
					bWarning = 	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<479.083221,-1533.315674,26.297249>>, <<489.413971,-1520.813965,36.046005>>, 6.0)
								OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<475.078461,-1526.016113,26.303955>>, <<487.542664,-1521.841064,33.292191>>, 6.0)
				BREAK
				
				CASE 2
					bWarning = 	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1987.809937,3045.384766,41.320530>>, <<1976.759521,3026.671387,56.763927>>, 35.0)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN bWarning
ENDFUNC

FUNC BOOL IS_PLAYER_IN_AIRPORT()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		VECTOR vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
		
		IF IS_COORD_IN_SPECIFIED_AREA(vPlayerCoords, AC_AIRPORT_AIRSIDE, 1000)
		OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-1256.4573, -2150.7021, 12.9248>>, <<100.00, 100.00, 100.00>>)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC 

//Check whether the local player has approached the goto (or action area).
FUNC BOOL HAS_PLAYER_APPROACHED_LOCATION(VECTOR vLoc)
	BOOL bApproached = FALSE
	
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_POLICE_RAID
			SWITCH serverBD.iSubVariation
				CASE 0
				CASE 1
				CASE 2
				CASE 6
					bApproached = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vLoc) < 40.0 * 40.0
				BREAK
				
				CASE 7
				CASE 8
					bApproached = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vLoc) < 35.0 * 35.0
				BREAK
				
				DEFAULT
					bApproached = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vLoc) < 30.0 * 30.0
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_COLLECT_FRIENDS
			SWITCH serverBD.iCurrentGoto
				CASE 0
					bApproached = (VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vLoc) < 50.0 * 50.0)
									OR IS_PLAYER_IN_AIRPORT()
				BREAK
				
				CASE 1
					bApproached = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vLoc) < 50.0 * 50.0
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT
			SWITCH serverBD.iSubVariation
				CASE 0
				DEFAULT
					bApproached = (VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vLoc) < 20.0 * 20.0)
									OR IS_PLAYER_IN_WARNING_AREA()
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_HOLD_UP
			SWITCH serverBD.iSubVariation
				CASE 0
				DEFAULT
					bApproached = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vLoc) < 40.0 * 40.0
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_STOLEN_VINYLS
			SWITCH serverBD.iCurrentGoto
				CASE 0
					bApproached = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vLoc) < 40.0 * 40.0
				BREAK
				
				CASE 1
					bApproached = IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2016.599731, -1039.867432, -3.801932>>, <<-2129.955566, -1003.351379, 22.182341>>, 15.750000)
				BREAK
			ENDSWITCH
		BREAK
		
		DEFAULT
			bApproached = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vLoc) < 20.0 * 20.0
		BREAK
	ENDSWITCH
	
	RETURN bApproached
ENDFUNC

//Check whether the local player has spooked the given enemy ped. If no ped is specified, the check applies to all peds.
FUNC BOOL HAS_PLAYER_SPOOKED_TARGET(INT iPed = -1)
	BOOL bSpooked = FALSE
	
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_POLICE_RAID
			bSpooked = HAS_PLAYER_APPROACHED_LOCATION(GET_LOCATION())
		BREAK
		
		CASE FMBB_DEFEND_RECOVER_PROPERTY
			SWITCH serverBD.iSubVariation
				CASE 0
					SWITCH iPed
						CASE 3
						CASE 4
							bSpooked =	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<806.260986,-1085.468750,22.685556>>, <<805.401306,-1066.397461,32.637119>>, 20.0)
										OR (IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
											AND (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<805.414795,-1059.714111,22.866123>>, <<806.799500,-1093.976440,67.669342>>, 40.0)))
						BREAK
						
						CASE 5
						CASE 6
							bSpooked =	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<977.703735,-1394.640869,25.954826>>, <<986.419189,-1410.022583,34.247169>>, 20.0)
										OR (IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
											AND (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<981.024170,-1397.256836,25.613104>>, <<992.665283,-1418.036621,70.137329>>, 40.0)))
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 1
					SWITCH iPed
						CASE 3
						CASE 4
							bSpooked =	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<463.111023,-718.330505,21.579481>>, <<453.246185,-718.461243,31.359077>>, 20.0)
										OR (IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
											AND (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<463.111023,-718.330505,21.579481>>, <<449.317474,-718.483582,42.764561>>, 50.0)))
						BREAK
						
						CASE 5
						CASE 6
							bSpooked =	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<377.870941,-1125.571899,23.405655>>, <<377.935638,-1107.226196,33.502815>>, 20.0)
										OR (IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
											AND (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<378.244995,-1107.226196,23.500740>>, <<378.241425,-1133.067505,68.438324>>, 40.0)))
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 2
					SWITCH iPed
						CASE 3
						CASE 4
							bSpooked =	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-221.572205,-1507.904419,25.437309>>, <<-208.250504,-1492.899048,35.267773>>, 20.0)
										OR (IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
											AND (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-230.458618,-1517.612793,25.462114>>, <<-202.652054,-1485.121948,70.288910>>, 40.0)))
						BREAK
						
						CASE 5
						CASE 6
							bSpooked =	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<50.352421,-1001.876892,23.531376>>, <<53.740318,-992.235840,33.332314>>, 20.0)
										OR (IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
											AND (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<50.352421,-1001.876892,23.531376>>, <<59.241642,-979.558044,68.407639>>, 40.0)))
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 3
					SWITCH iPed
						CASE 3
						CASE 4
							bSpooked =	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<81.959015,275.275665,104.337624>>, <<79.289810,267.334167,113.479439>>, 15.0)
										OR (IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
											AND (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<81.959015,275.275665,104.337624>>, <<73.477745,250.579102,148.319748>>, 40.0)))
						BREAK
						
						CASE 5
						CASE 6
							bSpooked =	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-90.216179,-67.875984,52.915955>>, <<-106.002441,-62.800514,60.365650>>, 21.0)
										OR (IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
											AND (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-90.097435,-66.946159,53.253529>>, <<-118.344376,-58.344299,99.940598>>, 20.0)))
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 4
					SWITCH iPed
						CASE 3
						CASE 4
							bSpooked =	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<959.244507,-1870.026489,25.018913>>, <<975.170349,-1871.775635,37.537914>>, 14.0)
										OR (IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
											AND (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<966.101685,-1866.861938,25.452579>>, <<964.445251,-1891.437256,70.149658>>, 40.0)))
						BREAK
						
						CASE 5
						CASE 6
							bSpooked =	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<785.711060,-2380.750732,16.500237>>, <<770.226746,-2379.024658,26.278362>>, 20.0)
										OR (IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
											AND (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<785.711060,-2380.750732,16.500237>>, <<761.181763,-2377.828613,61.233833>>, 40.0)))
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 5
					SWITCH iPed
						CASE 3
						CASE 4
							bSpooked =	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-235.258560,-1998.163574,27.946037>>, <<-225.649139,-1985.572388,33.529686>>, 15.0)
										OR (IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
											AND (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-236.214508,-1993.084961,23.946039>>, <<-209.923050,-1994.343506,66.620407>>, 40.0)))
						BREAK
						
						CASE 5
						CASE 6
							bSpooked =	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1066.235962,-2087.427490,8.297785>>, <<-1054.611816,-2076.308105,17.236498>>, 16.0)
										OR (IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
											AND (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1066.235962,-2087.427490,8.297785>>, <<-1045.518433,-2067.482178,52.281181>>, 40.0)))
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 6
					SWITCH iPed
						CASE 3
						CASE 4
							bSpooked =	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-286.913818,-2496.053711,4.414141>>, <<-293.452026,-2504.222900,10.400028>>, 30.0)
										OR (IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
											AND (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-286.913818,-2496.053711,4.414141>>, <<-299.098206,-2510.658203,36.117802>>, 50.0)))
						BREAK
						
						CASE 5
						CASE 6
							bSpooked =	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<370.450867,-2453.333740,3.514954>>, <<352.812347,-2453.916748,10.402504>>, 20.0)
										OR (IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
											AND (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<370.768799,-2452.897217,3.742084>>, <<349.552704,-2452.755371,45.676151>>, 40.0)))
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 7
					SWITCH iPed
						CASE 3
						CASE 4
							bSpooked =	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-57.941463,381.776428,110.430771>>, <<-81.183205,392.552917,116.430771>>, 11.0)
										OR (IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
											AND (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-39.198505,373.092255,110.452271>>, <<-96.565483,398.989594,151.424469>>, 40.0)))
						BREAK
						
						CASE 5
						CASE 6
							bSpooked =	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<111.573997,-12.427261,65.809669>>, <<116.793449,1.791802,71.782349>>, 15.0)
										OR (IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
											AND (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<107.042900,-25.120262,65.817719>>, <<119.310890,8.339875,106.924110>>, 60.0)))
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 8
					SWITCH iPed
						CASE 3
						CASE 4
							bSpooked =	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1368.192627,-969.540955,5.804543>>, <<-1356.295166,-961.366943,22.666363>>, 15.0)
										OR (IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
											AND (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1379.079224,-976.273987,5.660301>>, <<-1356.295166,-961.366943,57.666363>>, 40.0)))
						BREAK
						
						CASE 5
						CASE 6
							bSpooked =	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1329.400757,-387.539948,33.616646>>, <<-1347.525513,-398.099182,40.608246>>, 15.0)
										OR (IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
											AND (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1349.793213,-374.738403,33.768654>>, <<-1329.833374,-404.854004,82.304504>>, 50.0)))
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 9
					SWITCH iPed
						CASE 3
						CASE 4
							bSpooked =	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-892.592407,-1160.951904,2.759018>>, <<-887.462219,-1169.447144,8.901423>>, 19.0)
										OR (IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
											AND (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-892.592407,-1160.951904,2.759018>>, <<-878.967834,-1184.462524,43.928303>>, 40.0)))
						BREAK
						
						CASE 5
						CASE 6
							bSpooked =	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1564.246338,-974.401245,10.072140>>, <<-1562.512451,-988.125122,17.017389>>, 10.0)
										OR (IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
											AND (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1565.303467,-976.074585,10.587510>>, <<-1553.182007,-986.600891,52.017601>>, 40.0)))
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
			
			SWITCH iPed
				CASE 0
				CASE 1
				CASE 2
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					AND NOT IS_PED_ON_ANY_BIKE(PLAYER_PED_ID())
						bSpooked = VDIST2(GET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())), GET_ENTITY_COORDS(NET_TO_PED(serverBD.Targets[iPed].niPed))) < (IN_PROXIMITY_BMX * 2.0 * IN_PROXIMITY_BMX * 2.0) //Double the distance when in vehicles.
					ELSE
						bSpooked = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(NET_TO_PED(serverBD.Targets[iPed].niPed))) < IN_PROXIMITY_BMX * IN_PROXIMITY_BMX
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_STOLEN_VINYLS
			WEAPON_TYPE wtWeapon
			
			IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtWeapon, FALSE)
			AND wtWeapon != WEAPONTYPE_UNARMED
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2016.669189,-1040.249878,-1.301931>>, <<-2111.201660,-1009.497070,20.967033>>, 14.750000)
					IF NOT HAS_NET_TIMER_STARTED(stAlertDelay)
						PRINTLN("[FMBB_DEFEND] - [HAS_PLAYER_SPOOKED_TARGET] Player has entered yacht area. Starting timer.")
						REINIT_NET_TIMER(stAlertDelay)
					ENDIF
				ELSE
					IF HAS_NET_TIMER_STARTED(stAlertDelay)
						PRINTLN("[FMBB_DEFEND] - [HAS_PLAYER_SPOOKED_TARGET] Player has exited yacht area. Resetting timer.")
						RESET_NET_TIMER(stAlertDelay)
					ENDIF
				ENDIF
				
				IF HAS_NET_TIMER_EXPIRED(stAlertDelay, STOLEN_VINYL_ALERT_DELAY)
					bSpooked = TRUE
				ENDIF
			ELSE
				IF HAS_NET_TIMER_STARTED(stAlertDelay)
					PRINTLN("[FMBB_DEFEND] - [HAS_PLAYER_SPOOKED_TARGET] Player has unequipped their weapon. Resetting timer.")
					RESET_NET_TIMER(stAlertDelay)
				ENDIF
			ENDIF
			
			INT i
			REPEAT GET_NUMBER_OF_MISSION_ENTITIES() i
				IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iMissionEntityBitSet[i], MISSION_ENTITY_BIT_HELD)
					PRINTLN("[FMBB_DEFEND] - [HAS_PLAYER_SPOOKED_TARGET] player = ", GET_PLAYER_NAME(PLAYER_ID()), " has taken the vinyls.")
					bSpooked = TRUE
				ENDIF
			ENDREPEAT
		BREAK
		
		CASE FMBB_DEFEND_HOLD_UP
			SWITCH serverBD.iSubVariation
				CASE 0
					bSpooked =  IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1066.983643,-1676.057007,6.531895>>, <<-1079.003296,-1661.866089,2.398428>>, 13.0)
								OR (IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
									AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1079.975464,-1679.347778,43.575233>>, <<-1060.742798,-1662.842651,1.614227>>, 40.0))
				BREAK
				
				CASE 1
					bSpooked =	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1985.843506,3790.040527,30.214453>>, <<2006.800171,3802.055176,35.180801>>, 7.0)
								OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<2003.729614,3798.552002,30.180801>>, <<1998.758545,3806.903809,35.268627>>, 10.0)
								OR (IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
									AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1979.720581,3786.819580,30.180801>>, <<2017.584839,3808.854248,71.251137>>, 40.0))
				BREAK
				
				CASE 2
					bSpooked =  IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<24.500536,-1927.544678,19.930521>>, <<37.542366,-1911.980225,27.273886>>, 12.0)
								OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<21.700226,-1921.845093,20.095978>>, <<34.668312,-1906.177979,23.687462>>, 4.5)
								OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<38.130848,-1908.519775,19.627853>>, <<43.611988,-1902.012451,23.709538>>, 13.0)
								OR (IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
									AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<21.623909,-1931.890015,18.429167>>, <<51.361694,-1895.723633,60.668476>>, 40.0))
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN bSpooked
ENDFUNC

//For vehicles using the checkpoint system, get the speed to use for the current checkpoint.
FUNC FLOAT GET_SPEED_FOR_CHECKPOINT(INT iVeh, INT iCheckpoint)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_POLICE_RAID
			SWITCH serverBD.iSubVariation
				CASE 0
					SWITCH iVeh
						CASE 3
							SWITCH iCheckpoint
								CASE 0
								DEFAULT		RETURN	15.0
							ENDSWITCH
						BREAK
						
						CASE 4
							SWITCH iCheckpoint
								CASE 0
								DEFAULT		RETURN	15.0
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 1
					SWITCH iVeh
						CASE 3
						CASE 4
							SWITCH iCheckpoint
								CASE 0		RETURN	20.0
								DEFAULT		RETURN	15.0
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 2
					SWITCH iVeh
						CASE 3
						CASE 4
							SWITCH iCheckpoint
								CASE 1
								DEFAULT		RETURN	10.0
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 3
					SWITCH iVeh
						CASE 3
						CASE 4
							SWITCH iCheckpoint
								CASE 0
								DEFAULT		RETURN	20.0
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 4
					SWITCH iVeh
						CASE 3
						CASE 4
							SWITCH iCheckpoint
								CASE 0		RETURN	20.0
								DEFAULT		RETURN	10.0
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 5
					SWITCH iVeh
						CASE 3
						CASE 4
							SWITCH iCheckpoint
								CASE 0		RETURN	25.0
								DEFAULT		RETURN	10.0
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 7
					SWITCH iVeh
						CASE 3
						CASE 4
							SWITCH iCheckpoint
								CASE 0		RETURN	20.0
								DEFAULT		RETURN	10.0
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 8
					SWITCH iVeh
						CASE 3
						CASE 4
							SWITCH iCheckpoint
								CASE 0		RETURN	20.0
								DEFAULT		RETURN	10.0
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 9
					SWITCH iVeh
						CASE 3
							SWITCH iCheckpoint
								CASE 0		RETURN 20.0
								DEFAULT		RETURN 7.5
							ENDSWITCH
						BREAK
						
						CASE 4
							SWITCH iCheckpoint
								CASE 0		RETURN 20.0
								CASE 1		RETURN 7.5
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN 25.0
ENDFUNC

//For vehicles using the checkpoint system, get the driving mode to use for the current checkpoint.
//Usually, DF_ForceStraightLine is needed for the penultimate checkpoint to force the vehicle to leave the road (i.e. ignore road nodes).
//The final checkpoint can usually use DF_PreferNavmeshRoute without DF_ForceStraightLine to navigate off-road.
FUNC DRIVINGMODE GET_DRIVING_MODE_FOR_CHECKPOINT(INT iVeh, INT iCheckpoint)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_POLICE_RAID
			SWITCH serverBD.iSubVariation
				CASE 0
					SWITCH iVeh
						CASE 3
							SWITCH iCheckpoint
								CASE	0	RETURN	DRIVINGMODE_AVOIDCARS
								CASE	8	RETURN	DF_UseShortCutLinks | DF_ChangeLanesAroundObstructions | DF_UseSwitchedOffNodes | DF_DriveIntoOncomingTraffic | DF_SwerveAroundAllCars | DF_SteerAroundPeds | DF_SteerAroundObjects | DF_PreferNavmeshRoute | DF_ForceStraightLine
								DEFAULT		RETURN	DF_UseShortCutLinks | DF_ChangeLanesAroundObstructions | DF_UseSwitchedOffNodes | DF_DriveIntoOncomingTraffic | DF_SwerveAroundAllCars | DF_SteerAroundPeds | DF_SteerAroundObjects | DF_PreferNavmeshRoute
							ENDSWITCH
						BREAK
						
						CASE 4
							SWITCH iCheckpoint
								CASE	0	RETURN	DRIVINGMODE_AVOIDCARS | DF_PreferNavmeshRoute
								CASE	1	RETURN	DF_UseShortCutLinks | DF_ChangeLanesAroundObstructions | DF_UseSwitchedOffNodes | DF_DriveIntoOncomingTraffic | DF_SwerveAroundAllCars | DF_SteerAroundPeds | DF_SteerAroundObjects | DF_PreferNavmeshRoute | DF_ForceStraightLine
								DEFAULT		RETURN	DF_UseShortCutLinks | DF_ChangeLanesAroundObstructions | DF_UseSwitchedOffNodes | DF_DriveIntoOncomingTraffic | DF_SwerveAroundAllCars | DF_SteerAroundPeds | DF_SteerAroundObjects | DF_PreferNavmeshRoute
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 1
					SWITCH iVeh
						CASE 3
						CASE 4
							SWITCH iCheckpoint
								CASE 0		RETURN	DRIVINGMODE_AVOIDCARS | DF_UseSwitchedOffNodes
								DEFAULT		RETURN	DF_UseShortCutLinks | DF_ChangeLanesAroundObstructions | DF_UseSwitchedOffNodes | DF_DriveIntoOncomingTraffic | DF_PreferNavmeshRoute
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 2
					SWITCH iVeh
						CASE 3
						CASE 4
							SWITCH iCheckpoint
								CASE	0	RETURN	DRIVINGMODE_AVOIDCARS | DF_UseSwitchedOffNodes
								DEFAULT		RETURN	DF_UseShortCutLinks | DF_ChangeLanesAroundObstructions | DF_UseSwitchedOffNodes | DF_DriveIntoOncomingTraffic | DF_SwerveAroundAllCars | DF_SteerAroundPeds | DF_SteerAroundObjects | DF_PreferNavmeshRoute
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 4
					SWITCH iVeh
						CASE 3
						CASE 4
							SWITCH iCheckpoint
								CASE	0	RETURN	DRIVINGMODE_AVOIDCARS
								DEFAULT		RETURN	DF_UseShortCutLinks | DF_ChangeLanesAroundObstructions | DF_UseSwitchedOffNodes | DF_DriveIntoOncomingTraffic | DF_SwerveAroundAllCars | DF_SteerAroundPeds | DF_SteerAroundObjects | DF_PreferNavmeshRoute
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 5
					SWITCH iVeh
						CASE 3
						CASE 4
							SWITCH iCheckpoint
								CASE	0	RETURN	DRIVINGMODE_AVOIDCARS
								CASE	1	RETURN  DF_UseShortCutLinks | DF_ChangeLanesAroundObstructions | DF_UseSwitchedOffNodes | DF_DriveIntoOncomingTraffic | DF_SwerveAroundAllCars | DF_SteerAroundPeds | DF_SteerAroundObjects | DF_PreferNavmeshRoute | DF_ForceStraightLine
								DEFAULT		RETURN	DF_UseShortCutLinks | DF_ChangeLanesAroundObstructions | DF_UseSwitchedOffNodes | DF_DriveIntoOncomingTraffic | DF_SwerveAroundAllCars | DF_SteerAroundPeds | DF_SteerAroundObjects | DF_PreferNavmeshRoute
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 7
					SWITCH iVeh
						CASE 3
						CASE 4
							SWITCH iCheckpoint
								CASE 0		RETURN	DRIVINGMODE_AVOIDCARS
								DEFAULT		RETURN	DF_UseShortCutLinks | DF_ChangeLanesAroundObstructions | DF_UseSwitchedOffNodes | DF_DriveIntoOncomingTraffic | DF_SwerveAroundAllCars | DF_SteerAroundPeds | DF_SteerAroundObjects | DF_ForceStraightLine
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 8
					SWITCH iVeh
						CASE 3
						CASE 4
							SWITCH iCheckpoint
								CASE	0	RETURN	DRIVINGMODE_AVOIDCARS
								DEFAULT		RETURN	DF_UseShortCutLinks | DF_ChangeLanesAroundObstructions | DF_UseSwitchedOffNodes | DF_DriveIntoOncomingTraffic | DF_SwerveAroundAllCars | DF_SteerAroundPeds | DF_SteerAroundObjects | DF_ForceStraightLine
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 9
					SWITCH iVeh
						CASE 3
							SWITCH iCheckpoint
								CASE	0	RETURN	DRIVINGMODE_AVOIDCARS
								DEFAULT		RETURN	DF_UseShortCutLinks | DF_ChangeLanesAroundObstructions | DF_UseSwitchedOffNodes | DF_DriveIntoOncomingTraffic | DF_SwerveAroundAllCars | DF_SteerAroundPeds | DF_SteerAroundObjects | DF_ForceStraightLine
							ENDSWITCH
						BREAK
						
						CASE 4
							SWITCH iCheckpoint
								CASE	0	RETURN	DRIVINGMODE_AVOIDCARS
								DEFAULT		RETURN	DF_UseShortCutLinks | DF_ChangeLanesAroundObstructions | DF_UseSwitchedOffNodes | DF_DriveIntoOncomingTraffic | DF_SwerveAroundAllCars | DF_SteerAroundPeds | DF_SteerAroundObjects | DF_ForceStraightLine
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN	DRIVINGMODE_AVOIDCARS
ENDFUNC

//Returns the number of checkpoints used by a vehicle.
FUNC INT GET_NUMBER_OF_VEH_CHECKPOINTS(INT iVeh)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_POLICE_RAID
			SWITCH serverBD.iSubVariation
				CASE 0
					SWITCH iVeh
						CASE	3	RETURN	11
						CASE	4	RETURN	4
					ENDSWITCH
				BREAK
				
				CASE 1
					SWITCH iVeh
						CASE	3	RETURN 2
						CASE	4	RETURN 2
					ENDSWITCH
				BREAK
				
				CASE 2
					SWITCH iVeh
						CASE	3	RETURN	3
						CASE	4	RETURN	3
					ENDSWITCH
				BREAK
				
				CASE 4
					SWITCH iVeh
						CASE	3	RETURN	2
						CASE	4	RETURN	2
					ENDSWITCH
				BREAK
				
				CASE 5
					SWITCH iVeh
						CASE	3	RETURN	2
						CASE	4	RETURN	2
					ENDSWITCH
				BREAK
				
				CASE 7
					SWITCH iVeh
						CASE	3	RETURN 2
						CASE	4	RETURN 2
					ENDSWITCH
				BREAK
				
				CASE 8
					SWITCH iVeh
						CASE	3	RETURN	2
						CASE	4	RETURN	2
					ENDSWITCH
				BREAK
				
				CASE 9
					SWITCH iVeh
						CASE	3	RETURN	2
						CASE	4	RETURN	2
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN 1
ENDFUNC

//Returns the position and heading for the specified checkpoint and vehicle combination.
PROC GET_NTH_VEH_GOTO_COORD(INT iVeh, INT iCheckpointStage, VECTOR &vLoc, FLOAT &fHeading)
	//Set the location and heading to zero here (should be replaced by the SWITCH, otherwise it can be used as an error value).
	vLoc = <<0, 0, 0>>
	fHeading = 0
	
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_POLICE_RAID
			SWITCH serverBD.iSubVariation
				CASE 0
					SWITCH iVeh
						CASE 0
							SWITCH iCheckpointStage
								CASE 0	vLoc = <<719.0160, -1283.8690, 25.1580>>	fHeading = 327.0000	BREAK
							ENDSWITCH
						BREAK
						
						CASE 1
							SWITCH iCheckpointStage
								CASE 0	vLoc = <<722.2130, -1298.8900, 25.2500>>	fHeading = 221.9980	BREAK
							ENDSWITCH
						BREAK
						
						CASE 2
							SWITCH iCheckpointStage
								CASE 0	vLoc = <<727.9240, -1288.8590, 52.7700>>	fHeading = 178.7970	BREAK
							ENDSWITCH
						BREAK
						
						CASE 3
							SWITCH iCheckpointStage
								CASE 0	vLoc = <<802.68414, -1362.79456, 25.54409>>	fHeading = 290.7940	BREAK
								CASE 1	vLoc = <<796.74103, -1358.05188, 25.62327>>	fHeading = 290.7940	BREAK
								CASE 2	vLoc = <<789.37964, -1352.79639, 25.22138>>	fHeading = 290.7940	BREAK
								CASE 3	vLoc = <<782.47705, -1348.76892, 25.31189>>	fHeading = 290.7940	BREAK
								CASE 4	vLoc = <<773.81079, -1346.66870, 25.31705>>	fHeading = 290.7940	BREAK
								CASE 5	vLoc = <<713.32538, -1348.97668, 24.67474>>	fHeading = 290.7940	BREAK
								CASE 6	vLoc = <<705.47864, -1344.09363, 24.59710>>	fHeading = 290.7940	BREAK
								CASE 7	vLoc = <<703.80029, -1337.10974, 24.70927>>	fHeading = 290.7940	BREAK
								CASE 8	vLoc = <<706.96484, -1330.47131, 24.80638>>	fHeading = 290.7940	BREAK
								CASE 9	vLoc = <<711.53827, -1326.86609, 24.90065>> fHeading = 290.7940 BREAK
								CASE 10	vLoc = <<715.43799, -1319.18030, 25.01778>>	fHeading = 290.7940	BREAK
							ENDSWITCH
						BREAK
						
						CASE 4
							SWITCH iCheckpointStage
								CASE 0	vLoc = <<776.96844, -1258.43481, 25.43561>>	fHeading = 359.5950	BREAK
								CASE 1	vLoc = <<734.19720, -1255.82007, 25.25932>>	fHeading = 359.5950	BREAK
								CASE 2	vLoc = <<720.30927, -1265.07837, 25.18436>>	fHeading = 359.5950	BREAK
								CASE 3	vLoc = <<715.63477, -1275.65820, 25.09004>>	fHeading = 359.5950	BREAK
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 1
					SWITCH iVeh
						CASE 3
							SWITCH iCheckpointStage
								CASE 0	vLoc = <<333.22540, -1032.94568, 28.19891>>	fHeading = 349.9980	BREAK
								CASE 1	vLoc = <<335.3760, -1006.4190, 28.3160>>	fHeading = 349.9980	BREAK
							ENDSWITCH
						BREAK
								
						CASE 4
							SWITCH iCheckpointStage
								CASE 0	vLoc = <<333.22540, -1032.94568, 28.19891>>	fHeading = 18.5980	BREAK
								CASE 1	vLoc = <<331.1650, -1014.3760, 28.2900>>	fHeading = 18.5980	BREAK
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 2
					SWITCH iVeh
						CASE 3
							SWITCH iCheckpointStage
								CASE 0	vLoc = <<-206.24573, -1302.00635, 30.29597>>	fHeading = 251.0130	BREAK
								CASE 1	vLoc = <<-185.65732, -1301.84534, 30.29597>>	fHeading = 251.0130	BREAK
								CASE 2	vLoc = <<-167.4560, -1308.3199, 30.3070>>		fHeading = 251.0130	BREAK
							ENDSWITCH
						BREAK
							
						CASE 4
							SWITCH iCheckpointStage
								CASE 0	vLoc = <<-206.24573, -1302.00635, 30.29597>>	fHeading = 286.6120 BREAK
								CASE 1	vLoc = <<-185.65732, -1301.84534, 30.29597>>	fHeading = 286.6120	BREAK
								CASE 2	vLoc = <<-176.8350, -1302.2180, 30.3170>>		fHeading = 286.6120	BREAK
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 3
					SWITCH iVeh
						CASE 3	vLoc = <<-15.1090, 211.5800, 105.5430>>		fHeading = 77.1980	BREAK
						CASE 4	vLoc = <<-35.6180, 210.0440, 105.5530>>		fHeading = 243.1980	BREAK
					ENDSWITCH
				BREAK
				
				CASE 4
					SWITCH iVeh
						CASE 3
							SWITCH iCheckpointStage
								CASE 0	vLoc = <<873.71796, -2082.57788, 29.49873>>	fHeading = 257.3980	BREAK
								CASE 1	vLoc = <<915.1670, -2090.6169, 29.3890>>	fHeading = 257.3980	BREAK
							ENDSWITCH
						BREAK
								
						CASE 4
							SWITCH iCheckpointStage
								CASE 0	vLoc = <<873.71796, -2082.57788, 29.49873>>	fHeading = 254.7980	BREAK
								CASE 1	vLoc = <<900.4360, -2089.7180, 29.5930>>	fHeading = 254.7980	BREAK
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 5
					SWITCH iVeh
						CASE 3
							SWITCH iCheckpointStage
								CASE 0	vLoc = <<-695.04120, -2395.17383, 12.93637>>	fHeading = 294.3990 BREAK
								CASE 1	vLoc = <<-683.8880, -2388.0139, 12.8000>>		fHeading = 294.3990	BREAK
							ENDSWITCH
						BREAK
						
						CASE 4
							SWITCH iCheckpointStage
								CASE 0	vLoc = <<-683.19879, -2381.68701, 12.93461>>	fHeading = 294.9990	BREAK
								CASE 1	vLoc = <<-677.3210, -2372.7900, 12.9640>>		fHeading = 294.9990	BREAK
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 6
					SWITCH iVeh
						CASE 3
							SWITCH iCheckpointStage
								CASE 0	vLoc =  <<203.3236, -3114.1284, 4.7903>>	fHeading = 303.9989	BREAK
							ENDSWITCH
						BREAK
						
						CASE 4
							SWITCH iCheckpointStage
								CASE 0	vLoc = <<202.9956, -3125.4363, 4.7903>>		fHeading = 233.5990	BREAK
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 7
					SWITCH iVeh
						CASE 3
							SWITCH iCheckpointStage
								CASE 0	vLoc =	<<400.41577, 255.58910, 102.05139>>	fHeading = 149.0480	BREAK
								CASE 1	vLoc =  <<391.1622, 239.4263, 101.9495>>	fHeading = 149.0480	BREAK
							ENDSWITCH
						BREAK
						
						CASE 4
							SWITCH iCheckpointStage
								CASE 0	vLoc = <<380.95444, 180.61906, 102.07892>>	fHeading = 355.6480	BREAK
								CASE 1	vLoc = <<380.3603, 210.9482, 101.9394>>		fHeading = 355.6480	BREAK
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 8
					SWITCH iVeh
						CASE 3
							SWITCH iCheckpointStage
								CASE 0	vLoc =	<<-1249.80286, -660.10901, 25.15817>>	fHeading = 210.7290	BREAK
								CASE 1	vLoc =  <<-1238.6890, -674.3770, 23.9570>>		fHeading = 210.7290	BREAK
							ENDSWITCH
						BREAK
						
						CASE 4
							SWITCH iCheckpointStage
								CASE 0	vLoc =	<<-1214.00964, -705.31421, 21.22859>>	fHeading = 48.1290	BREAK
								CASE 1	vLoc =	<<-1220.9561, -695.2140, 21.9330>>		fHeading = 48.1290	BREAK
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 9
					SWITCH iVeh
						CASE 3
							SWITCH iCheckpointStage
								CASE 0	vLoc =	<<-1149.45142, -1198.90527, 1.73157>>	fHeading = 99.2490	BREAK
								CASE 1	vLoc =  <<-1162.3260, -1197.6300, 2.5970>>		fHeading = 99.2490	BREAK
							ENDSWITCH
						BREAK
						
						CASE 4
							SWITCH iCheckpointStage
								CASE 0	vLoc =	<<-1184.85400, -1205.46082, 6.09606>>	fHeading = 44.7990	BREAK
								CASE 1	vLoc =	<<-1173.29919, -1199.84253, 3.81858>>	fHeading = 44.7990	BREAK
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

//Get the model names for the specified mission entity. If no mission entity is specified, this will return the either the only model for the variation, if available.
FUNC MODEL_NAMES GET_MISSION_ENTITY_MODEL(INT iEntity = -1)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_RECOVER_PROPERTY
			SWITCH iEntity
				CASE 0	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ba_Prop_Battle_AMB_Phone"))
				CASE 1	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ba_Prop_Battle_Wallet_Pickup"))
				CASE 2	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ba_Prop_Battle_Handbag"))
			ENDSWITCH
		BREAK
		
		CASE	FMBB_DEFEND_STOLEN_VINYLS	RETURN	INT_TO_ENUM(MODEL_NAMES, HASH("BA_PROP_BATTLE_VINYL_CASE"))
		CASE	FMBB_DEFEND_COLLECT_FRIENDS	RETURN	SUPERVOLITO
		CASE	FMBB_DEFEND_MUSIC_EQUIPMENT	RETURN	SPEEDO
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

//Spawn locations for mission entities.
PROC GET_MISSION_ENTITY_SPAWN_COORDS(VECTOR& vLoc, FLOAT& fHeading, INT iEntity = -1)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_COLLECT_FRIENDS
			SWITCH iEntity
				CASE 0	vLoc =  <<-1175.2637, -2522.7161, 12.9451>>	fHeading = 123.1986	BREAK //supervolito
				CASE 1	vLoc =  <<-1181.4904, -2550.6482, 12.9452>>	fHeading = 128.7985	BREAK //supervolito
				CASE 2	vLoc =  <<-1147.5507, -2571.6604, 12.9452>>	fHeading = 179.3983	BREAK //supervolito
				CASE 3	vLoc =  <<-1206.2177, -2507.0107, 12.9452>>	fHeading = 185.5983	BREAK //supervolito
				CASE 4	vLoc =  <<-1223.9563, -2532.3623, 12.9451>>	fHeading = 185.5983	BREAK //supervolito
				CASE 5	vLoc =  <<-1123.4937, -2560.9717, 12.9445>>	fHeading = 151.3983	BREAK //supervolito
				CASE 6	vLoc =  <<-1239.5184, -2629.4905, 12.9449>>	fHeading = 354.5980	BREAK //supervolito
				CASE 7	vLoc =  <<-1266.2723, -2612.8738, 12.9450>>	fHeading = 305.3979	BREAK //supervolito
				CASE 8	vLoc =  <<-1203.7996, -2661.7883, 12.9450>>	fHeading = 320.9977	BREAK //supervolito
				CASE 9	vLoc =  <<-1177.6196, -2656.1077, 12.9444>>	fHeading = 16.1977	BREAK //supervolito
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT
			SWITCH serverBD.iSubVariation
				CASE 0	vLoc =  <<-974.3800, -270.0470, 37.2940>>	fHeading = 192.6	BREAK //speedo
				CASE 1	vLoc =  <<481.1000, -1526.8700, 28.3010>>	fHeading = 1.6		BREAK //speedo
				CASE 2	vLoc =  <<1987.2240, 3041.1890, 46.0590>>	fHeading = 59.0		BREAK //speedo
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_STOLEN_VINYLS
			vLoc =	<<-2036.7036, -1034.7758, 4.8810>>	fHeading = 123.2993 //ba_prop_battle_vinyl_case_01
		BREAK
	ENDSWITCH
ENDPROC

//Set some attributes for the given mission entity object.
PROC SET_MISSION_ENTITY_OBJ_ATTRIBUTES(NETWORK_INDEX niEntity)
	OBJECT_INDEX oiEntity = NET_TO_OBJ(niEntity)
	
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_RECOVER_PROPERTY
		CASE FMBB_DEFEND_STOLEN_VINYLS
			SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(niEntity, TRUE)
			PLACE_OBJECT_ON_GROUND_PROPERLY(oiEntity)
			SET_ENTITY_LOAD_COLLISION_FLAG(oiEntity, TRUE)
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(oiEntity, TRUE)
			FREEZE_ENTITY_POSITION(oiEntity, FALSE)
			PLACE_OBJECT_ON_GROUND_PROPERLY(oiEntity)
			
			SET_ENTITY_LOD_DIST(oiEntity, 1200)
			SET_ENTITY_HEALTH(oiEntity, 50)
        	SET_ENTITY_INVINCIBLE(oiEntity, TRUE)
			SET_ENTITY_PROOFS(oiEntity, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE)
			SET_PICKUP_COLLIDES_WITH_PROJECTILES(oiEntity, TRUE)
			ALLOW_PORTABLE_PICKUP_TO_MIGRATE_TO_NON_PARTICIPANTS(oiEntity, FALSE)
			SET_OBJECT_FORCE_VEHICLES_TO_AVOID(oiEntity, FALSE)
			NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(oiEntity, TRUE)
			SET_ENTITY_VISIBLE(oiEntity, TRUE)
			HIDE_PORTABLE_PICKUP_WHEN_DETACHED(oiEntity, FALSE)
		BREAK
	ENDSWITCH
ENDPROC

//Set some attributes for the given mission entity vehicle.
PROC SET_MISSION_ENTITY_VEH_ATTRIBUTES(INT iMissionEntity, BOOL bLockRearDoors = FALSE)
	VEHICLE_INDEX viEntity = NET_TO_VEH(serverBD.niMissionEntities[iMissionEntity])
	
	//General attributes.
	SET_VEHICLE_DOORS_LOCKED_FOR_NON_SCRIPT_PLAYERS(viEntity, TRUE)
	SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(viEntity, TRUE)
	SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(viEntity, TRUE)
	SET_PICK_UP_BY_CARGOBOB_DISABLED(viEntity, TRUE)
	
	IF bLockRearDoors
		SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(viEntity, ENUM_TO_INT(SC_DOOR_REAR_LEFT), VEHICLELOCK_LOCKED)
		SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(viEntity, ENUM_TO_INT(SC_DOOR_REAR_RIGHT), VEHICLELOCK_LOCKED)
	ENDIF
	
	//Prevent the vehicle from being saved in the player's garage.
	IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh", DECOR_TYPE_INT)
		DECOR_SET_INT(viEntity, "Not_Allow_As_Saved_Veh", MP_NOT_ALLOWED_AS_SAVED_VEHICLE_DEC_SIMEON)
		PRINTLN("[FMBB_DEFEND] - [SET_MISSION_ENTITY_VEH_ATTRIBUTES] - iMissionEntity = ", iMissionEntity, " set as 'Not_Allow_As_Saved_Veh'.")
	ENDIF
	
	//Prevent the vehicle from being used for activities that use passive mode or being modded.
	IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
		INT iDecoratorValue
		
		IF DECOR_EXIST_ON(viEntity, "MPBitset")
		      iDecoratorValue = DECOR_GET_INT(viEntity, "MPBitset")
		ENDIF
		
		SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_PASSIVE_MODE_VEHICLE)
		SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_MODDABLE_VEHICLE)
		DECOR_SET_INT(viEntity, "MPBitset", iDecoratorValue)
		
		PRINTLN("[FMBB_DEFEND] - [SET_MISSION_ENTITY_VEH_ATTRIBUTES] - iMissionEntity = ", iMissionEntity, " set as 'MP_DECORATOR_BS_NON_PASSIVE_MODE_VEHICLE'.")
		PRINTLN("[FMBB_DEFEND] - [SET_MISSION_ENTITY_VEH_ATTRIBUTES] - iMissionEntity = ", iMissionEntity, " set as 'MP_DECORATOR_BS_NON_MODDABLE_VEHICLE'.")
	ENDIF
	
	//Variation specific attributes.
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_COLLECT_FRIENDS
			SET_VEHICLE_COLOURS(viEntity, 111, 111)
			
			//In Collect Friends, we want to remove the rear doors and only allow access to the pilot seat for one player.
			//Since the rear doors are removed, we need to set seating preferences for each player on the mission to force using the front seats.
			SET_VEHICLE_DOOR_BROKEN(viEntity, SC_DOOR_REAR_LEFT, TRUE)
			SET_VEHICLE_DOOR_BROKEN(viEntity, SC_DOOR_REAR_RIGHT, TRUE)
			SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(viEntity, ENUM_TO_INT(SC_DOOR_FRONT_RIGHT), VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
		BREAK
		
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT
			SET_VEHICLE_COLOURS(viEntity, 111, 111)
			SET_VEHICLE_DOOR_OPEN(viEntity, SC_DOOR_REAR_LEFT, TRUE)
			SET_VEHICLE_DOOR_OPEN(viEntity, SC_DOOR_REAR_RIGHT, TRUE)
			SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(viEntity, FALSE)
			SET_VEHICLE_TYRES_CAN_BURST(viEntity, FALSE)
		BREAK
	ENDSWITCH
ENDPROC

//Returns the position of the on-foot dropoff location for the given Nightclub.
FUNC VECTOR GET_PED_DROPOFF_LOCATION(FREEMODE_DELIVERY_DROPOFFS fddDropoff)
	SWITCH fddDropoff
		CASE BB_DROPOFF_NIGHTCLUB_LA_MESA				RETURN	<<771.9007, -1336.0919, 25.2463>>	//La Mesa
		CASE BB_DROPOFF_NIGHTCLUB_MISSION_ROW			RETURN	<<353.3344, -956.5939, 28.4041>>	//Mission Row Building
		CASE BB_DROPOFF_NIGHTCLUB_STRAWBERRY_WAREHOUSE	RETURN	<<-114.1347, -1254.0649, 28.2187>>	//Strawberry Warehouse
		CASE BB_DROPOFF_NIGHTCLUB_WEST_VINEWOOD			RETURN	<<11.5602, 214.4467, 106.1773>>		//West Vinewood Bilding
		CASE BB_DROPOFF_NIGHTCLUB_CYPRESS_FLATS			RETURN	<<868.5779, -2088.1831, 29.2070>>	//Cypress Flats Warehouse
		CASE BB_DROPOFF_NIGHTCLUB_LSIA_WAREHOUSE		RETURN	<<-685.2452, -2458.6565, 12.8790>>	//LSIA warehouse
		CASE BB_DROPOFF_NIGHTCLUB_ELYSIAN_ISLAND		RETURN	<<188.7281, -3161.0688, 4.7871>>	//Elysian Island Warehouse
		CASE BB_DROPOFF_NIGHTCLUB_DOWNTOWN_VINEWOOD		RETURN	<<386.0720, 253.1823, 101.9968>>	//Downtown Vinewood
		CASE BB_DROPOFF_NIGHTCLUB_DEL_PERRO_BUILDING	RETURN	<<-1286.1625, -641.5381, 25.5953>>	//Del Perro Building
		CASE BB_DROPOFF_NIGHTCLUB_VESPUCCI_CANALS		RETURN	<<-1167.3861, -1164.9691, 4.6318>>	//Cypress Flats Warehouse
	ENDSWITCH
	
	PRINTLN("[FMBB_DEFEND] - [GET_PED_DROPOFF_LOCATION] Invalid dropoff location.")
	RETURN <<0, 0, 0>>
ENDFUNC

FUNC VECTOR GET_ALLEY_DROPOFF_LOCATION(FREEMODE_DELIVERY_DROPOFFS fddDropoff)
	SWITCH fddDropoff
		CASE BB_DROPOFF_NIGHTCLUB_LA_MESA				RETURN <<726.9350, -1300.6260, 25.2720>>	//La Mesa
		CASE BB_DROPOFF_NIGHTCLUB_MISSION_ROW			RETURN <<329.9173, -1022.9581, 28.2801>>	//Mission Row Building
		CASE BB_DROPOFF_NIGHTCLUB_STRAWBERRY_WAREHOUSE	RETURN <<-143.6460, -1296.5291, 29.7570>>	//Strawberry Warehouse
		CASE BB_DROPOFF_NIGHTCLUB_WEST_VINEWOOD			RETURN <<-37.7120, 216.8300, 105.5530>>		//West Vinewood Bilding
		CASE BB_DROPOFF_NIGHTCLUB_CYPRESS_FLATS			RETURN <<854.1410, -2089.9150, 29.2110>>	//Cypress Flats Warehouse
		CASE BB_DROPOFF_NIGHTCLUB_LSIA_WAREHOUSE		RETURN <<-679.7210, -2379.2290, 12.8900>>	//LSIA warehouse
		CASE BB_DROPOFF_NIGHTCLUB_ELYSIAN_ISLAND		RETURN <<188.9420, -3148.1841, 4.7870>>		//Elysian Island Warehouse
		CASE BB_DROPOFF_NIGHTCLUB_DOWNTOWN_VINEWOOD		RETURN <<362.4747, 279.7013, 102.2875>>		//Downtown Vinewood
		CASE BB_DROPOFF_NIGHTCLUB_DEL_PERRO_BUILDING	RETURN <<-1260.5729, -654.7999, 25.6667>>	//Del Perro Building
		CASE BB_DROPOFF_NIGHTCLUB_VESPUCCI_CANALS		RETURN <<-1164.6680, -1171.6000, 4.6240>>	//Cypress Flats Warehouse
	ENDSWITCH
	
	PRINTLN("[FMBB_DEFEND] - [GET_ALLEY_DROPOFF_LOCATION] Invalid dropoff location.")
	RETURN <<0, 0, 0>>
ENDFUNC

//Returns the position of the helicopter dropoff location for the given Nightclub.
FUNC VECTOR GET_HELI_DROPOFF_LOCATION(FREEMODE_DELIVERY_DROPOFFS fddDropoff)
	SWITCH fddDropoff
		CASE BB_DROPOFF_NIGHTCLUB_LA_MESA				RETURN	<<724.6539, -1300.7332, 25.2464>>	//La Mesa
		CASE BB_DROPOFF_NIGHTCLUB_MISSION_ROW			RETURN	<<346.8737, -950.6725, 28.4103>>	//Mission Row Building
		CASE BB_DROPOFF_NIGHTCLUB_STRAWBERRY_WAREHOUSE	RETURN	<<-143.0640, -1280.0220, 46.8980>>	//Strawberry Warehouse
		CASE BB_DROPOFF_NIGHTCLUB_WEST_VINEWOOD			RETURN	<<-33.7454, 213.7827, 105.5534>>	//West Vinewood Bilding
		CASE BB_DROPOFF_NIGHTCLUB_CYPRESS_FLATS			RETURN	<<868.4749, -2087.9453, 29.1828>>	//Cypress Flats Warehouse
		CASE BB_DROPOFF_NIGHTCLUB_LSIA_WAREHOUSE		RETURN	<<-712.1770, -2442.1331, 19.6210>>	//LSIA warehouse
		CASE BB_DROPOFF_NIGHTCLUB_ELYSIAN_ISLAND		RETURN	<<181.1299, -3175.7383, 4.6188>>	//Elysian Island Warehouse
		CASE BB_DROPOFF_NIGHTCLUB_DOWNTOWN_VINEWOOD		RETURN	<<376.7823, 288.2303, 102.1889>>	//Downtown Vinewood
		CASE BB_DROPOFF_NIGHTCLUB_DEL_PERRO_BUILDING	RETURN	<<-1281.4277, -657.0988, 41.0053>>	//Del Perro Building
		CASE BB_DROPOFF_NIGHTCLUB_VESPUCCI_CANALS		RETURN	<<-1186.0439, -1160.6479, 12.1230>>	//Cypress Flats Warehouse
	ENDSWITCH
	
	PRINTLN("[FMBB_DEFEND] - [GET_HELI_DROPOFF_LOCATION] Invalid dropoff location.")
	RETURN <<0, 0, 0>>
ENDFUNC

FUNC FREEMODE_DELIVERY_DROPOFFS GET_FREEMODE_DELIVERY_DROP_OFF()
	NIGHTCLUB_ID niNightclub = GET_PLAYERS_OWNED_NIGHTCLUB(GB_GET_LOCAL_PLAYER_GANG_BOSS())
	SIMPLE_INTERIORS siSimpleInterior = NIGHTCLUB_TO_SIMPLE_INTERIORS(niNightclub)
	
	RETURN GET_FMBB_DROPOFF_SIMPLE_INTERIOR(siSimpleInterior)
ENDFUNC

//Returns the position of the dropoff location for the local player's (or their boss') owned Nightclub.
FUNC VECTOR GET_DROP_OFF_COORDS()
	FREEMODE_DELIVERY_DROPOFFS fddDropoff = GET_FREEMODE_DELIVERY_DROP_OFF()
	
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_COLLECT_FRIENDS	RETURN GET_HELI_DROPOFF_LOCATION(fddDropoff)
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT	RETURN GET_ALLEY_DROPOFF_LOCATION(fddDropoff)
		CASE FMBB_DEFEND_HOLD_UP			RETURN GET_PED_DROPOFF_LOCATION(fddDropoff)
		DEFAULT								RETURN FMBB_GET_IN_VEHICLE_DROPOFF_LOCATION(fddDropoff)
	ENDSWITCH
ENDFUNC

//Returns the radius to use for the dropoff corona.
FUNC FLOAT GET_DROP_OFF_RADIUS()
	RETURN 3.0
ENDFUNC

//Returns the height to use for the dropoff corona.
FUNC FLOAT GET_DROP_OFF_HEIGHT()	
	RETURN 2.5
ENDFUNC

//Returns the position of the on-foot entrance to the Nightclub (i.e. the front door).
FUNC VECTOR GET_NIGHTCLUB_ON_FOOT_ENTRANCE(NIGHTCLUB_ID niNightclubID)
	SWITCH niNightclubID
		CASE NIGHTCLUB_LA_MESA				RETURN	<<758.1191, -1332.2731, 26.2802>>	//La Mesa
		CASE NIGHTCLUB_MISSION_ROW			RETURN	<<345.5124, -977.7867, 28.3872>>	//Mission Row Building
		CASE NIGHTCLUB_STRAWBERRY_WAREHOUSE	RETURN	<<-121.5581, -1258.5781, 28.3088>>	//Strawberry Warehouse
		CASE NIGHTCLUB_WEST_VINEWOOD		RETURN	<<4.0870, 220.4594, 106.7566>>		//West Vinewood Bilding
		CASE NIGHTCLUB_CYPRESS_FLATS		RETURN	<<871.4524, -2100.6008, 29.4768>>	//Cypress Flats Warehouse
		CASE NIGHTCLUB_LSIA_WAREHOUSE		RETURN	<<-676.6141, -2458.2104, 12.9444>>	//LSIA warehouse
		CASE NIGHTCLUB_ELYSIAN_ISLAND		RETURN	<<195.4160, -3167.3811, 4.7903>>	//Elysian Island Warehouse
		CASE NIGHTCLUB_DOWNTOWN_VINEWOOD	RETURN	<<371.0099, 252.2451, 103.0081>>	//Downtown Vinewood
		CASE NIGHTCLUB_DEL_PERRO_BUILDING	RETURN	<<-1285.0198, -652.3701, 25.6332>>	//Del Perro Building
		CASE NIGHTCLUB_VESPUCCI_CANALS		RETURN	<<-1174.5742, -1153.4714, 4.6582>>	//Cypress Flats Warehouse
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

//Returns the position of the destination for peds in the PED_AI_STATE_WALK_TO_POINT state.
FUNC VECTOR GET_WALK_TO_POINT_COORD()
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_HOLD_UP	RETURN GET_NIGHTCLUB_ON_FOOT_ENTRANCE(GET_PLAYERS_OWNED_NIGHTCLUB(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

//Returns the Nightclub entrance to be used.
FUNC INT GET_NIGHTCLUB_ENTRANCE()
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_STOLEN_VINYLS
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT	RETURN ciBUSINESS_HUB_ENTRANCE_WAREHOUSE_FOOT
	ENDSWITCH
	
	RETURN ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_BUZZER
ENDFUNC

//Determines whether the current variation has any props that need to be created.
FUNC BOOL DOES_ENTITY_HAVE_PROPS()
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_STOLEN_VINYLS		RETURN TRUE
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT	RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

//Returns the entity that the specified prop should be attached to, if any (-1 otherwise).
FUNC INT GET_ENTITY_FOR_PROP(INT iProp)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT
			SWITCH iProp
				CASE 0
				DEFAULT	RETURN 0
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN -1
ENDFUNC

//Returns the offset to use for a prop, if any. This is used for exact positioning when attaching props to entities.
FUNC VECTOR GET_PROP_OFFSET(INT iProp)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT
			SWITCH iProp
				CASE 0	RETURN <<-0.430, -0.15, 0.1>>
				CASE 1	RETURN <<0.225, -0.2, 0.1>>
				CASE 2	RETURN <<0.0, -0.15, 0.55>>
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN <<0, 0, 0>>
ENDFUNC

//Returns the rotation to use for a prop, if any. This is used for exact positioning when attaching props to entities.
FUNC VECTOR GET_PROP_ROTATION(INT iProp)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT
			SWITCH iProp
				CASE 0	RETURN <<0.0, 0.0, -90.0>>
				CASE 1	RETURN <<0.0, 0.0, 90.0>>
				CASE 2	RETURN <<0.0, 0.0, 0.0>>
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 90.0>>
ENDFUNC

//Set some attributes for the given prop.
PROC SET_PROP_ATTRIBUTES(INT iProp)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT
			SET_ENTITY_COMPLETELY_DISABLE_COLLISION(NET_TO_OBJ(serverBD.niProp[iProp]), FALSE)
			SET_ENTITY_VISIBLE(NET_TO_OBJ(serverBD.niProp[iProp]),TRUE)
			SET_ENTITY_INVINCIBLE(NET_TO_OBJ(serverBD.niProp[iProp]),TRUE)
			
			IF DOES_ENTITY_HAVE_PHYSICS(NET_TO_OBJ(serverBD.niProp[iProp])) 
			AND GET_IS_ENTITY_A_FRAG(NET_TO_OBJ(serverBD.niProp[iProp]))
				SET_DISABLE_BREAKING(NET_TO_OBJ(serverBD.niProp[iProp]), TRUE)
				SET_DISABLE_FRAG_DAMAGE(NET_TO_OBJ(serverBD.niProp[iProp]), TRUE)
				PRINTLN("[FMBB_DEFEND] - [MAINTAIN_CREATE_VEHICLE_PROPS] - SET_DISABLE_BREAKING TRUE")
			ENDIF
			
			SET_ENTITY_PROOFS(NET_TO_OBJ(serverBD.niProp[iProp]), TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE)
		BREAK
	ENDSWITCH
ENDPROC

//Initialises the gang chase options for the current variation/subvariation.
PROC INIT_GANG_CHASE_OPTIONS(STRUCT_GANG_CHASE_OPTIONS& sgcoOptions)
	sgcoOptions.iTotalNumberOfWaves = 1
	sgcoOptions.iTimeBetweenWaves = 0
	sgcoOptions.iNumVehInEachWave = 2
	sgcoOptions.vDropffCoord = GET_DROP_OFF_COORDS()
	sgcoOptions.fFleeDistance = 100
	
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT
			sgcoOptions.iNumPedsInEachVeh = 2
			sgcoOptions.iPedAccuracy = 5
			sgcoOptions.mGangChaseVeh = DILETTANTE2
			sgcoOptions.mGangChasePed = S_M_M_SECURITY_01
			sgcoOptions.weaponGangChasePed = WEAPONTYPE_PISTOL
		BREAK
		
		CASE FMBB_DEFEND_HOLD_UP
			SWITCH serverBD.iSubVariation
				CASE 0
					sgcoOptions.iNumPedsInEachVeh = 2
					sgcoOptions.mGangChaseVeh = MANANA
					sgcoOptions.mGangChasePed = G_M_Y_MEXGOON_02
					
					sgcoOptions.bSetVehicleColours[0] = TRUE
					sgcoOptions.iVehicleColour1[0] = 88
					sgcoOptions.iVehicleColour2[0] = 88
					
					sgcoOptions.bSetVehicleColours[1] = TRUE
					sgcoOptions.iVehicleColour1[1] = 88
					sgcoOptions.iVehicleColour2[1] = 88
				BREAK
				
				CASE 1
					sgcoOptions.iNumPedsInEachVeh = 1
					sgcoOptions.mGangChaseVeh = SANCHEZ
					sgcoOptions.mGangChasePed = A_M_M_HILLBILLY_02
				BREAK
				
				CASE 2
					sgcoOptions.iNumPedsInEachVeh = 2
					sgcoOptions.mGangChaseVeh = EMPEROR
					sgcoOptions.mGangChasePed = G_M_Y_BALLAORIG_01
					
					sgcoOptions.bSetVehicleColours[0] = TRUE
					sgcoOptions.iVehicleColour1[0] = 145
					sgcoOptions.iVehicleColour2[0] = 145
					
					sgcoOptions.bSetVehicleColours[1] = TRUE
					sgcoOptions.iVehicleColour1[1] = 145
					sgcoOptions.iVehicleColour2[1] = 145
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

//Returns the restricted interior to use.
FUNC MP_RESTRICTED_INTERIORS GET_RESTRICTED_INTERIOR()
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_STOLEN_VINYLS	RETURN RESTRICTED_INTERIOR_DJ_YACHT
		
		CASE FMBB_DEFEND_HOLD_UP
			SWITCH serverBD.iSubVariation
				CASE 0	RETURN RESTRICTED_INTERIOR_MELANOMA_GARAGE
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN RESTRICTED_INTERIOR_INVALID
ENDFUNC

FUNC BOOL SHOULD_USE_IPL_DELAY()
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_STOLEN_VINYLS	RETURN TRUE
		
		CASE FMBB_DEFEND_HOLD_UP
			SWITCH serverBD.iSubVariation
				CASE 0	RETURN TRUE
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_IPL_CENTRE()
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_STOLEN_VINYLS	RETURN <<-2073.54102, -1021.10364, 14.99213>>
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC FLOAT GET_IPL_MAP_LOAD_DISTANCE()
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_STOLEN_VINYLS	RETURN 300.0
	ENDSWITCH
	
	RETURN -1.0
ENDFUNC

FUNC FLOAT GET_IPL_MAP_INTERNAL_DISTANCE()
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_STOLEN_VINYLS	RETURN 200.0
	ENDSWITCH
	
	RETURN -1.0
ENDFUNC

FUNC FLOAT GET_IPL_MAP_UNLOAD_DISTANCE()
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_STOLEN_VINYLS	RETURN 320.0
	ENDSWITCH
	
	RETURN -1.0
ENDFUNC

FUNC INT GET_IPL_MAP_CURRENT_FLOOR(BOOL bInternal, FLOAT fZHeight)
	INT iFloor = -2
	
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_STOLEN_VINYLS
			iFloor = 2
			
			IF bInternal
				IF fZHeight <= 4.00
					iFloor = 0
				ELIF fZHeight <= 6.3
					iFloor = 1
				ELIF fZHeight <= 9.5
					iFloor = 2
				ELIF fZHeight <= 13.1
					iFloor = 3
				ELIF fZHeight <= 26.3
					iFloor = 4
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN iFloor
ENDFUNC

FUNC INT GET_IPL_MAP_HASH()
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_STOLEN_VINYLS	RETURN 1906615853
	ENDSWITCH
	
	RETURN -1
ENDFUNC

FUNC VECTOR GET_IPL_MAP_MILO_OFFSET()
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_STOLEN_VINYLS	RETURN <<-2066.023, -1024.236, 0.0>>
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC INT GET_PANIC_PED()
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_COLLECT_FRIENDS	RETURN 2
	ENDSWITCH
	
	RETURN -1
ENDFUNC

FUNC INT GET_NUMBER_OF_STATIC_EMITTERS()
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_STOLEN_VINYLS	RETURN 1
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC VECTOR GET_STATIC_EMITTER_LOCATION(INT iEmitter)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_STOLEN_VINYLS
			SWITCH iEmitter
				CASE 0	RETURN <<-2035.81458, -1034.16370, 5.38237>>
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN <<0, 0, 0>>
ENDFUNC

FUNC TEXT_LABEL_63 GET_STATIC_EMITTER_NAME(INT iEmitter)
	TEXT_LABEL_63 tl63Emitter
	
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_STOLEN_VINYLS
			SWITCH iEmitter
				CASE 0
					tl63Emitter = "SE_DLC_BTL_Yacht_Exterior_01"
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN tl63Emitter
ENDFUNC

FUNC MODEL_NAMES GET_STATIC_EMITTER_LINKED_ENTITY_MODEL(INT iEmitter)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_STOLEN_VINYLS
			SWITCH iEmitter
				CASE 0	RETURN PROP_BOOMBOX_01
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC TEXT_LABEL_63 GET_STATIC_EMITTER_RADIO_STATION_NAME(INT iEmitter)
	TEXT_LABEL_63 tl63Station
	
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_STOLEN_VINYLS
			SWITCH iEmitter
				CASE 0
					tl63Station = "RADIO_07_DANCE_01"
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN tl63Station
ENDFUNC

FUNC BOOL SHOULD_DISABLE_STATIC_EMITTER(INT iEmitter)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_STOLEN_VINYLS
			SWITCH iEmitter
				CASE 0
					IF serverBD.iTargetAimedAtBitSet != 0
					OR serverBD.iTargetNearbyShotsBitSet != 0
					OR serverBD.iTargetSpookedBitSet != 0
					OR serverBD.iTargetSpookedByAreaCheckBitSet != 0
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_OPENING_CALL_SPEAKER_ID()
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_POLICE_RAID		RETURN 6
		CASE FMBB_DEFEND_RECOVER_PROPERTY	RETURN 2
	ENDSWITCH
	
	RETURN -1
ENDFUNC

FUNC TEXT_LABEL_15 GET_OPENING_CALL_VOICE_ID()
	TEXT_LABEL_15 tl15Voice
	
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_POLICE_RAID
			tl15Voice = "BTL_TONY"
		BREAK
		
		CASE FMBB_DEFEND_RECOVER_PROPERTY
			tl15Voice = "LAZLOW"
		BREAK
	ENDSWITCH
	
	RETURN tl15Voice
ENDFUNC

FUNC TEXT_LABEL_15 GET_OPENING_CALL_TEXT_BLOCK()
	TEXT_LABEL_15 tl15TextBlock
	
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_POLICE_RAID
			tl15TextBlock = "BATFMAU"
		BREAK
		
		CASE FMBB_DEFEND_RECOVER_PROPERTY
			tl15TextBlock = "LAZFMAU"
		BREAK
	ENDSWITCH
	
	RETURN tl15TextBlock
ENDFUNC

FUNC TEXT_LABEL_15 GET_OPENING_CALL_ROOT_LABEL()
	TEXT_LABEL_15 tl15RootLabel
	
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_POLICE_RAID
			IF GET_RANDOM_INT_IN_RANGE(0, 2) < 1
				tl15RootLabel = "BATFM_DPRO1"
			ELSE
				tl15RootLabel = "BATFM_DPRO2"
			ENDIF
		BREAK
		
		CASE FMBB_DEFEND_RECOVER_PROPERTY
			IF GET_RANDOM_INT_IN_RANGE(0, 2) < 1
				tl15RootLabel = "LAZFM_DRPO1"
			ELSE
				tl15RootLabel = "LAZFM_DRPO2"
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN tl15RootLabel
ENDFUNC

FUNC enumConversationPriority GET_OPENING_CALL_PRIORITY()
	RETURN CONV_PRIORITY_VERY_HIGH
ENDFUNC

FUNC INT GET_UPDATE_CALL_DELAY()
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_STOLEN_VINYLS
		CASE FMBB_DEFEND_COLLECT_FRIENDS	RETURN 3000
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC INT GET_UPDATE_CALL_SPEAKER_ID()
	RETURN 3
ENDFUNC

FUNC TEXT_LABEL_15 GET_UPDATE_CALL_VOICE_ID()
	TEXT_LABEL_15 tl15Voice = "BTL_DAVE"
	
	RETURN tl15Voice
ENDFUNC

FUNC TEXT_LABEL_15 GET_UPDATE_CALL_TEXT_BLOCK()
	TEXT_LABEL_15 tl15TextBlock = "BATFMAU"
	
	RETURN tl15TextBlock
ENDFUNC

FUNC TEXT_LABEL_15 GET_UPDATE_CALL_ROOT_LABEL()
	TEXT_LABEL_15 tl15RootLabel
	
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_STOLEN_VINYLS
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 4)
				CASE 0
					tl15RootLabel = "BATFM_SMUP1"
				BREAK
				
				CASE 1
					tl15RootLabel = "BATFM_SMUP2"
				BREAK
				
				CASE 2
					tl15RootLabel = "BATFM_SMUP3"
				BREAK
				
				CASE 3
					tl15RootLabel = "BATFM_SMUP4"
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_COLLECT_FRIENDS
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 2)
				CASE 0
					tl15RootLabel = "BATFM_CFUP1"
				BREAK
				
				CASE 1
					tl15RootLabel = "BATFM_CFUP2"
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_HOLD_UP
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 4)
				CASE 0
					tl15RootLabel = "BATFM_HUUP1"
				BREAK
				
				CASE 1
					tl15RootLabel = "BATFM_HUUP2"
				BREAK
				
				CASE 2
					tl15RootLabel = "BATFM_HUUP3"
				BREAK
				
				CASE 3
					tl15RootLabel = "BATFM_HUUP4"
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN tl15RootLabel
ENDFUNC

FUNC enumConversationPriority GET_UPDATE_CALL_PRIORITY()
	RETURN CONV_PRIORITY_VERY_HIGH
ENDFUNC

//Retrieves the text label for the intro shard title.
FUNC TEXT_LABEL_15 GET_INTRO_SHARD_TITLE()
	TEXT_LABEL_15 tl15Shard
	
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_POLICE_RAID
		CASE FMBB_DEFEND_RECOVER_PROPERTY
			tl15Shard = "BB_BM_DF_STRT_T"
		BREAK
		
		CASE FMBB_DEFEND_STOLEN_VINYLS
		CASE FMBB_DEFEND_COLLECT_FRIENDS
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT
		CASE FMBB_DEFEND_HOLD_UP
			SWITCH GET_PLAYERS_NIGHTCLUB_ACTIVE_DJ(GB_GET_LOCAL_PLAYER_GANG_BOSS())
				CASE DJ_SOLOMUN			tl15Shard = "BB_BM_PC_ST_T_S"	BREAK
				CASE DJ_DIXON			tl15Shard = "BB_BM_PC_ST_T_D"	BREAK
				CASE DJ_TALE_OF_US		tl15Shard = "BB_BM_PC_ST_T_T"	BREAK
				CASE DJ_BLACK_MADONNA	tl15Shard = "BB_BM_PC_ST_T_B"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN tl15Shard
ENDFUNC

//Retrieves the text label for the intro shard subtitle/strapline.
FUNC TEXT_LABEL_15 GET_INTRO_SHARD_SUBTITLE()
	TEXT_LABEL_15 tl15Shard
	
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_POLICE_RAID		tl15Shard = "BB_BM_PR_STRT_S"	BREAK
		CASE FMBB_DEFEND_RECOVER_PROPERTY	tl15Shard = "BB_BM_RP_STRT_S"	BREAK
		CASE FMBB_DEFEND_STOLEN_VINYLS		tl15Shard = "BB_BM_SV_STRT_S"	BREAK
		CASE FMBB_DEFEND_COLLECT_FRIENDS	tl15Shard = "BB_BM_CF_STRT_S"	BREAK
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT	tl15Shard = "BB_BM_ME_STRT_S"	BREAK
		CASE FMBB_DEFEND_HOLD_UP			tl15Shard = "BB_BM_HU_STRT_S"	BREAK
	ENDSWITCH
	
	RETURN tl15Shard
ENDFUNC

//Retrieves the text label for the successful end shard title.
FUNC TEXT_LABEL_15 GET_END_SHARD_SUCCESS_TITLE()
	TEXT_LABEL_15 tl15Shard
	
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_POLICE_RAID
		CASE FMBB_DEFEND_RECOVER_PROPERTY
			tl15Shard = "BB_BM_DF_SUCC_T"
		BREAK
			
		DEFAULT
			tl15SHard = "BB_BM_PC_SUCC_T"
		BREAK
	ENDSWITCH
	
	RETURN tl15Shard
ENDFUNC

//Retrieves the text label for the failure end shard title.
FUNC TEXT_LABEL_15 GET_END_SHARD_FAILURE_TITLE()
	TEXT_LABEL_15 tl15Shard
	
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_POLICE_RAID
		CASE FMBB_DEFEND_RECOVER_PROPERTY
			tl15Shard = "BB_BM_DF_FAIL_T"
		BREAK
			
		DEFAULT
			tl15SHard = "BB_BM_PC_FAIL_T"
		BREAK
	ENDSWITCH
	
	RETURN tl15Shard
ENDFUNC

//Retrieves the text label for the success end shard subtitle/strapline.
FUNC TEXT_LABEL_15 GET_END_SHARD_SUCCESS_SUBTITLE()
	TEXT_LABEL_15 tl15Shard
	
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_POLICE_RAID		tl15Shard = "BB_BM_PR_SUCC_S"	BREAK
		CASE FMBB_DEFEND_RECOVER_PROPERTY	tl15Shard = "BB_BM_RP_SUCC_S"	BREAK
		
		CASE FMBB_DEFEND_STOLEN_VINYLS
		CASE FMBB_DEFEND_COLLECT_FRIENDS
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT
		CASE FMBB_DEFEND_HOLD_UP			tl15Shard = "BB_BM_PC_SUCC_S"	BREAK
	ENDSWITCH
	
	RETURN tl15Shard
ENDFUNC

//Retrieves the text label for the failure end shard subtitle/strapline.
FUNC TEXT_LABEL_15 GET_END_SHARD_FAILURE_SUBTITLE()
	TEXT_LABEL_15 tl15Shard
	
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_POLICE_RAID		tl15Shard = "BB_BM_PR_FAIL_S"	BREAK
		CASE FMBB_DEFEND_RECOVER_PROPERTY	tl15Shard = "BB_BM_RP_FAIL_S"	BREAK
		CASE FMBB_DEFEND_COLLECT_FRIENDS	tl15Shard = "BB_BM_CF_FAIL_S"	BREAK
		CASE FMBB_DEFEND_STOLEN_VINYLS		tl15Shard = "BB_BM_SV_FAIL_S"	BREAK
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT	tl15Shard = "BB_BM_ME_FAIL_S"	BREAK
		CASE FMBB_DEFEND_HOLD_UP			tl15Shard = "BB_BM_HU_FAIL_S"	BREAK
	ENDSWITCH
	
	RETURN tl15Shard
ENDFUNC

//Get the objective text to display when the mission launches.
FUNC TEXT_LABEL_15 GET_OBJECTIVE_TEXT_FOR_APPROACH_ACTION()
	TEXT_LABEL_15 tl15Obj
	
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_POLICE_RAID
			SWITCH serverBD.iSubVariation
				CASE 0
				DEFAULT
					tl15Obj = "BB_OT_PR_0" //Go to the Nightclub.
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_STOLEN_VINYLS
			tl15Obj = "BB_OT_SV_0" //Go to the Yacht.
		BREAK
		
		CASE FMBB_DEFEND_COLLECT_FRIENDS
			SWITCH serverBD.iCurrentGoto
				CASE 0
					tl15Obj = "BB_OT_CF_3" //Go to LSIA.
				BREAK
				
				CASE 1
					SWITCH serverBD.iSubVariation
						CASE 0
							tl15Obj = "BB_OT_CF_0A" //Go to Rebel Radio.
						BREAK
						
						CASE 1
							tl15Obj = "BB_OT_CF_0B" //Go to Liquor Ace.
						BREAK
						
						CASE 2
							tl15Obj = "BB_OT_CF_0C" //Go to the Grand Senora Desert.
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT
			SWITCH serverBD.iSubVariation
				CASE 0
					tl15Obj = "BB_OT_ME_0A" //Go to Akan Records.
				BREAK
				
				CASE 1
					tl15Obj = "BB_OT_ME_0B" //Go to Hi-Men Cocktail Bar.
				BREAK
				
				CASE 2
					tl15Obj = "BB_OT_ME_0C" //Go to Yellow Jack Inn.
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_HOLD_UP
			SWITCH serverBD.iSubVariation
				CASE 0
					tl15Obj = "BB_OT_HU_0A" //Go to Del Perro Pier.
				BREAK
				
				CASE 1
					tl15Obj = "BB_OT_HU_0B" //Go to Joshua Road.
				BREAK
				
				CASE 2
					tl15Obj = "BB_OT_HU_0C" //Go to Grove Street.
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN tl15Obj
ENDFUNC

//Get the objective text to display when the player reaches the action area.
FUNC TEXT_LABEL_15 GET_OBJECTIVE_TEXT_FOR_ACTION_AREA()
	TEXT_LABEL_15 tl15Obj
	
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_POLICE_RAID
			SWITCH serverBD.iSubVariation
				CASE 0
				DEFAULT
					tl15Obj = "BB_OT_PR_1" //Take out the enemies.
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_RECOVER_PROPERTY
			SWITCH serverBD.iSubVariation
				CASE 0
				DEFAULT
					tl15Obj = "BB_OT_RP_0" //Recover the items from the street punks.
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_STOLEN_VINYLS
			SWITCH serverBD.iSubVariation
				CASE 0
				DEFAULT
					tl15Obj = "BB_OT_SV_1" //Recover the Vinyls.
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_HOLD_UP
			SWITCH serverBD.iSubVariation
				CASE 0
				DEFAULT
					tl15Obj = "BB_OT_HU_1" //Take out the gang members.
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN tl15Obj
ENDFUNC

//Get the objective text to display when the player has to find some items held by enemies. Varies depending on whether the player is currently holding an item.
FUNC TEXT_LABEL_15 GET_OBJECTIVE_TEXT_FOR_FIND(BOOL bHolder)
	TEXT_LABEL_15 tl15Obj
	
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_RECOVER_PROPERTY
			IF bHolder
				tl15Obj = "BB_OT_RP_2A" //Return the items to the Nightclub.
			ELSE
				tl15Obj = "BB_OT_RP_0" //Recover the property from the street punks.
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN tl15Obj
ENDFUNC

//Get the objective text to display when the player has to steal a mission entity.
FUNC TEXT_LABEL_15 GET_OBJECTIVE_TEXT_FOR_STEAL()
	TEXT_LABEL_15 tl15Obj
	
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_COLLECT_FRIENDS
			SWITCH serverBD.iSubVariation
				CASE 0
				DEFAULT
					tl15Obj = "BB_OT_CF_1" //Steal the helicopter.
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT
			SWITCH serverBD.iSubVariation
				CASE 0
				DEFAULT
					tl15Obj = "BB_OT_ME_1" //Steal the music equipment.
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN tl15Obj
ENDFUNC

//Get the objective text to display when the player has to pick up some items.
FUNC TEXT_LABEL_15 GET_OBJECTIVE_TEXT_FOR_PICKUP()
	TEXT_LABEL_15 tl15Obj
	
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_RECOVER_PROPERTY
			tl15Obj = "BB_OT_RP_1" //Pick up the items.
		BREAK
		
		CASE FMBB_DEFEND_STOLEN_VINYLS
			tl15Obj = "BB_OT_SV_1" //Recover the Vinyls.
		BREAK
		
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT
			tl15Obj = "BB_OT_ME_1" //Steal the music equipment.
		BREAK
	ENDSWITCH
	
	RETURN tl15Obj
ENDFUNC

//Get the objective text to display when a player has to deliver some items. Varies depending on whether the player is currently holding an item.
FUNC TEXT_LABEL_15 GET_OBJECTIVE_TEXT_FOR_DELIVER(BOOL bHolder)
	TEXT_LABEL_15 tl15Obj
	
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_RECOVER_PROPERTY
			IF bHolder
				tl15Obj = "BB_OT_RP_2A" //Return the property to the ~HUD_COLOUR_YELLOW~Nightclub.~s~
			ELSE
				tl15Obj = "BB_OT_RP_2B" //Help to return the property to the ~HUD_COLOUR_YELLOW~Nightclub.~s~
			ENDIF
		BREAK
		
		CASE FMBB_DEFEND_STOLEN_VINYLS
			IF bHolder
				tl15Obj = "BB_OT_SV_2A" //Deliver the Vinyls to the ~HUD_COLOUR_YELLOW~Nightclub.~s~
			ELSE
				tl15Obj = "BB_OT_SV_2B" //Help deliver the Vinyls to the ~HUD_COLOUR_YELLOW~Nightclub.~s~
			ENDIF
		BREAK
		
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT
			IF bHolder
				tl15Obj = "BB_OT_ME_2A" //Deliver the Music Equipment to the ~HUD_COLOUR_YELLOW~Nightclub.~s~
			ELSE
				tl15Obj = "BB_OT_ME_2B" //Help deliver the Music Equipment to the ~HUD_COLOUR_YELLOW~Nightclub.~s~
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN tl15Obj
ENDFUNC

//Get the objective text to display when a player is escorting some peds. Varies depending on whether the player is currently escorting any peds.
FUNC TEXT_LABEL_15 GET_OBJECTIVE_TEXT_FOR_ESCORT(BOOL bPlayerIsEscort, BOOL bFemaleFollower, BOOL bFemaleWaiting)
	TEXT_LABEL_15 tl15Obj
	
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_HOLD_UP
			IF bPlayerIsEscort
				IF iPedsBeingEscorted > 1
					tl15Obj = "BB_OT_HU_2B" //Take the friends to the Nightclub.
				ELSE
					IF bFemaleFollower
						tl15Obj = "BB_OT_HU_2BSF" //Take the friend to the Nightclub.
					ELSE
						tl15Obj = "BB_OT_HU_2BS" //Take the friend to the Nightclub.
					ENDIF
				ENDIF
			ELSE
				IF (iPedsAvailableForEscort - iPedsBeingEscorted) > 1
					tl15Obj = "BB_OT_HU_2A" //Collect the friends.
				ELIF (iPedsAvailableForEscort - iPedsBeingEscorted) = 1
					IF bFemaleWaiting
						tl15Obj = "BB_OT_HU_2ASF" //Collect the friend.
					ELSE
						tl15Obj = "BB_OT_HU_2AS" //Collect the friend.
					ENDIF
				ELSE
					IF iPedsBeingEscorted > 1
						tl15Obj = "BB_OT_HU_2C" //Help take the friends to the Nightclub.
					ELSE
						IF bFemaleFollower
							tl15Obj = "BB_OT_HU_2CSF" //Help take the friend to the Nightclub.
						ELSE
							tl15Obj = "BB_OT_HU_2CS" //Help take the friend to the Nightclub.
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN tl15Obj
ENDFUNC

//Get the objective text to display when a player has to drive some peds in a vehicle.
//Varies depending on whether the player is in the vehicle, at the collection point, the passengers are in the vehicle and whether they are the driver.
FUNC TEXT_LABEL_15 GET_OBJECTIVE_TEXT_FOR_CHAUFFEUR(BOOL bAnyPlayerInVehicle, BOOL bInProximity, BOOL bAtCollectPoint, BOOL bPassengersInVehicle, BOOL bIsDriver)
	TEXT_LABEL_15 tl15Obj
	
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_COLLECT_FRIENDS
			IF bAnyPlayerInVehicle
				SWITCH serverBD.iSubVariation
					CASE 0
						tl15Obj = "BB_OT_CF_0A" //Go to Rebel Radio.
					BREAK
					
					CASE 1
						tl15Obj = "BB_OT_CF_0B" //Go to Liquor Ace.
					BREAK
					
					CASE 2
						tl15Obj = "BB_OT_CF_0C" //Go to the Grand Senora Desert.
					BREAK
				ENDSWITCH
				
				IF bAtCollectPoint
					tl15Obj = "BB_OT_CF_2B" //Wait for the DJ's friends.
				ELSE
					IF NOT bIsDriver
					AND bInProximity
						tl15Obj = "BB_OT_CF_0D" //Wait for ~a~ to arrive in the helicopter.
					ENDIF
				ENDIF
				
				IF bPassengersInVehicle
					IF bIsDriver
						tl15Obj = "BB_OT_CF_2C" //Take the DJ's friends back to the Nightclub.
					ELSE
						tl15Obj = "BB_OT_CF_2D" //Help to take the DJ's friends back to the Nightclub.
					ENDIF
				ENDIF
			ELSE
				tl15Obj = "BB_OT_CF_2A" //Get in the helicopter.
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN tl15Obj
ENDFUNC

FUNC TEXT_LABEL_15 GET_OBJECTIVE_TEXT_FOR_LOSE_COPS()
	TEXT_LABEL_15 tl15Obj = "BB_OT_LOSECOPS"
	
	RETURN tl15Obj
ENDFUNC

//Returns the phone contact for the current mission.
FUNC enumCharacterList GET_PHONE_CONTACT_FOR_MISSION()
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_POLICE_RAID		RETURN GET_DEFEND_MISSION_POLICE_RAID_CONTACT()
		CASE FMBB_DEFEND_RECOVER_PROPERTY	RETURN GET_DEFEND_MISSION_RECOVER_PROPERTY_CONTACT()
		
		DEFAULT								RETURN GET_PHONECALL_MISSION_CONTACT()
	ENDSWITCH
ENDFUNC

FUNC INT GET_UPDATE_TEXT_MESSAGE_DELAY()
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_COLLECT_FRIENDS
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT	RETURN 3000
	ENDSWITCH
	
	RETURN 0
ENDFUNC

//Get the text message to send the player when a mission update has been triggered.
FUNC TEXT_LABEL_15 GET_UPDATE_TEXT_MESSAGE()
	TEXT_LABEL_15 tl15Txt
	
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_POLICE_RAID
			tl15Txt = "BB_TXT_PR_1"
		BREAK
		
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT
			tl15Txt = "BB_TXT_ME_1"
		BREAK
	ENDSWITCH
	
	RETURN tl15Txt
ENDFUNC

//Get the actual text label for the specified text message.
FUNC TEXT_LABEL_15 GET_TEXT_MESSAGE(TEXT_MESSAGE_ENUM eTextMessage)
	TEXT_LABEL_15 tl15Txt
	
	SWITCH eTextMessage
		CASE eTM_POLICE_RAID_OPENING
			IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
				IF GET_RANDOM_INT_IN_RANGE(0, 1) > 0
					tl15Txt = "BB_TXT_PR_0A"
				ELSE
					tl15Txt = "BB_TXT_PR_0B"
				ENDIF
			ELSE
				tl15Txt = "BB_TXT_PR_0C"
			ENDIF
		BREAK
		
		CASE eTM_RECOVER_PROPERTY_OPENING
			tl15Txt = "BB_TXT_RP_0"
		BREAK
		
		CASE eTM_STOLEN_VINYLS_OPENING
			SWITCH GET_PLAYERS_NIGHTCLUB_ACTIVE_DJ(GB_GET_LOCAL_PLAYER_GANG_BOSS())
				CASE DJ_SOLOMUN
					tl15Txt = "BB_TXT_SV_0_S"
				BREAK
				
				CASE DJ_DIXON
					tl15Txt = "BB_TXT_SV_0_D"
				BREAK
				
				CASE DJ_TALE_OF_US
					tl15Txt = "BB_TXT_SV_0_T"
				BREAK
				
				CASE DJ_BLACK_MADONNA
					tl15Txt = "BB_TXT_SV_0_B"
				BREAK
			ENDSWITCH
		BREAK
		
		CASE eTM_COLLECT_FRIENDS_OPENING
			SWITCH GET_PLAYERS_NIGHTCLUB_ACTIVE_DJ(GB_GET_LOCAL_PLAYER_GANG_BOSS())
				CASE DJ_SOLOMUN
					tl15Txt = "BB_TXT_CF_0_S"
				BREAK
				
				CASE DJ_DIXON
					tl15Txt = "BB_TXT_CF_0_D"
				BREAK
				
				CASE DJ_TALE_OF_US
					tl15Txt = "BB_TXT_CF_0_T"
				BREAK
				
				CASE DJ_BLACK_MADONNA
					tl15Txt = "BB_TXT_CF_0_B"
				BREAK
			ENDSWITCH
		BREAK
		
		CASE eTM_MUSIC_EQUIPMENT_OPENING
			SWITCH GET_PLAYERS_NIGHTCLUB_ACTIVE_DJ(GB_GET_LOCAL_PLAYER_GANG_BOSS())
				CASE DJ_SOLOMUN
					tl15Txt = "BB_TXT_ME_0_S"
				BREAK
				
				CASE DJ_DIXON
					tl15Txt = "BB_TXT_ME_0_D"
				BREAK
				
				CASE DJ_TALE_OF_US
					tl15Txt = "BB_TXT_ME_0_T"
				BREAK
				
				CASE DJ_BLACK_MADONNA
					tl15Txt = "BB_TXT_ME_0_B"
				BREAK
			ENDSWITCH
		BREAK
		
		CASE eTM_HOLD_UP_OPENING
			SWITCH GET_PLAYERS_NIGHTCLUB_ACTIVE_DJ(GB_GET_LOCAL_PLAYER_GANG_BOSS())
				CASE DJ_SOLOMUN
					tl15Txt = "BB_TXT_HU_0_S"
				BREAK
				
				CASE DJ_DIXON
					tl15Txt = "BB_TXT_HU_0_D"
				BREAK
				
				CASE DJ_TALE_OF_US
					tl15Txt = "BB_TXT_HU_0_T"
				BREAK
				
				CASE DJ_BLACK_MADONNA
					tl15Txt = "BB_TXT_HU_0_B"
				BREAK
			ENDSWITCH
		BREAK
		
		CASE eTM_POLICE_RAID_UPDATE
			tl15Txt = "BB_TXT_PR_1"
		BREAK
		
		CASE eTM_RECOVER_PROPERTY_UPDATE
			tl15Txt = "BB_TXT_RP_1"
		BREAK
		
		CASE eTM_MUSIC_EQUIPMENT_UPDATE	
			tl15Txt = "BB_TXT_ME_1"
		BREAK
		
		CASE eTM_COLLECT_FRIENDS_UPDATE_0
			tl15Txt = "BB_TXT_CF_1"
		BREAK
		
		CASE eTM_COLLECT_FRIENDS_UPDATE_1
			tl15Txt = "BB_TXT_CF_2"
		BREAK
		
		CASE eTM_COLLECT_FRIENDS_UPDATE_2
			SWITCH GET_PLAYERS_NIGHTCLUB_ACTIVE_DJ(GB_GET_LOCAL_PLAYER_GANG_BOSS())
				CASE DJ_SOLOMUN
					tl15Txt = "BB_TXT_CF_3_S"
				BREAK
				
				CASE DJ_DIXON
					tl15Txt = "BB_TXT_CF_3_D"
				BREAK
				
				CASE DJ_TALE_OF_US
					tl15Txt = "BB_TXT_CF_3_T"
				BREAK
				
				CASE DJ_BLACK_MADONNA
					tl15Txt = "BB_TXT_CF_3_B"
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN tl15Txt
ENDFUNC

//Prints the specified help text.
PROC PRINT_HELP_TEXT(HELP_TEXT_ENUM eHelpText)
	SWITCH eHelpText
		CASE eHT_POLICE_RAID_APPROACH_0
			IF NOT GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
				SWITCH GB_GET_PLAYER_GANG_TYPE(PLAYER_ID())
					CASE GT_VIP
						IF DOES_PLAYER_OWN_OFFICE(GB_GET_LOCAL_PLAYER_GANG_BOSS())
							PRINT_HELP_WITH_STRING_NO_SOUND("BB_HT_PR_HLP0", "BB_HT_CEO")
						ELSE
							PRINT_HELP_WITH_STRING_NO_SOUND("BB_HT_PR_HLP0", "BB_HT_VIP")
						ENDIF
					BREAK
					
					CASE GT_BIKER
						PRINT_HELP_WITH_STRING_NO_SOUND("BB_HT_PR_HLP0", "BB_HT_MCPRES")
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
		
		CASE eHT_POLICE_RAID_ATTACK_0
			PRINT_HELP_NO_SOUND("BB_HT_PR_HLP1")
		BREAK
		
		CASE eHT_POLICE_RAID_ATTACK_1
			IF NOT GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
				PRINT_HELP_NO_SOUND("BB_HT_PR_HLP2S")
			ELSE
				SWITCH GB_GET_PLAYER_GANG_TYPE(PLAYER_ID())
					CASE GT_VIP
						PRINT_HELP_NO_SOUND("BB_HT_PR_HLP2O")
					BREAK
					
					CASE GT_BIKER
						PRINT_HELP_NO_SOUND("BB_HT_PR_HLP2M")
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
		
		CASE eHT_RECOVER_PROPERTY_0
			PRINT_HELP_NO_SOUND("BB_HT_RP_HLP0")
		BREAK
		
		CASE eHT_STOLEN_VINYLS_COLLECT_0
			PRINT_HELP_NO_SOUND("BB_HT_SV_HLP0")
		BREAK
		
		CASE eHT_STOLEN_VINYLS_COLLECT_1
			PRINT_HELP_NO_SOUND("BB_HT_SV_HLP1")
		BREAK
		
		CASE eHT_COLLECT_FRIENDS_CHAUFFEUR_0
			PRINT_HELP_NO_SOUND("BB_HT_CF_HLP0")
		BREAK
		
		CASE eHT_COLLECT_FRIENDS_CHAUFFEUR_1
			PRINT_HELP_NO_SOUND("BB_HT_CF_HLP1")
		BREAK
		
		CASE eHT_COLLECT_FRIENDS_CHAUFFEUR_2
			PRINT_HELP_NO_SOUND("BB_HT_CF_HLP2")
		BREAK
		
		CASE eHT_MUSIC_EQUIPMENT_STEAL_0
			PRINT_HELP_NO_SOUND("BB_HT_ME_HLP0")
		BREAK
		
		CASE eHT_HOLD_UP_ATTACK_0
			PRINT_HELP_NO_SOUND("BB_HT_HU_HLP0")
		BREAK
	ENDSWITCH
ENDPROC

//Get the HUD label to display for the enemies remaining count.
FUNC TEXT_LABEL_15 GET_ENEMIES_REMAINING_HUD_TEXT()
	TEXT_LABEL_15 tl15Obj = "BB_HUD_ENMREM" //ENEMIES REMAINING
	
	RETURN tl15Obj
ENDFUNC

//Get the HUD label to display for the time remaining.
FUNC TEXT_LABEL_15 GET_TIME_REMAINING_HUD_TEXT()
	TEXT_LABEL_15 tl15Obj = "BB_HUD_TIMREM" //TIME REMAINING
	
	RETURN tl15Obj
ENDFUNC

//Get the HUD label to display for the panic meter.
FUNC TEXT_LABEL_15 GET_PANIC_METER_HUD_TEXT()
	TEXT_LABEL_15 tl15Obj = "BB_HUD_PANMET" //PANIC
	
	RETURN tl15Obj
ENDFUNC

//Get the HUD label to display for the property recovered count.
FUNC TEXT_LABEL_15 GET_PROPERTY_RECOVERED_HUD_TEXT()
	TEXT_LABEL_15 tl15Obj = "BB_HUD_PRPREC" //PROPERTY RECOVERED
	
	RETURN tl15Obj
ENDFUNC

//Get the destination blip priority.
FUNC BLIP_PRIORITY GET_DESTINATION_BLIP_PRIORITY()
	RETURN BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH
ENDFUNC

//Get whether the GPS route should be shown for a destination blip.
FUNC BOOL GET_SHOULD_SHOW_DESTINATION_BLIP_ROUTE()
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_STOLEN_VINYLS
			SWITCH serverBD.iCurrentGoto
				CASE 1	RETURN FALSE
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

//Get the map label to display for the destination blip.
FUNC TEXT_LABEL_15 GET_DESTINATION_BLIP_NAME()
	TEXT_LABEL_15 tl15Obj = "BB_BLP_DEST" //Destination
	
	RETURN tl15Obj
ENDFUNC

//Get the ped blip priority.
FUNC BLIP_PRIORITY GET_PED_BLIP_PRIORITY()
	RETURN BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH
ENDFUNC

//Get whether the GPS route should be shown for a ped blip.
FUNC BOOL GET_SHOULD_SHOW_PED_BLIP_ROUTE()
	RETURN FALSE
ENDFUNC

//Get the scale to use for the specified ped blip.
FUNC FLOAT GET_PED_BLIP_SCALE(INT iPed)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_COLLECT_FRIENDS	RETURN 1.0
		
		CASE FMBB_DEFEND_HOLD_UP
			SWITCH iPed
				CASE 0
				CASE 1	RETURN 1.0
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN 0.7
ENDFUNC

//Get the sprite to use for the specified ped blip.
FUNC BLIP_SPRITE GET_PED_BLIP_SPRITE(INT iPed)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_COLLECT_FRIENDS	RETURN RADAR_TRACE_VIP
		
		CASE FMBB_DEFEND_HOLD_UP
			SWITCH iPed
				CASE 0
				CASE 1	RETURN RADAR_TRACE_VIP
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN RADAR_TRACE_TEMP_4
ENDFUNC

//Get the colour to use for the specified ped blip.
FUNC INT GET_PED_BLIP_COLOUR(INT iPed)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_COLLECT_FRIENDS	RETURN BLIP_COLOUR_BLUE
		
		CASE FMBB_DEFEND_HOLD_UP
			SWITCH iPed
				CASE 0
				CASE 1	RETURN BLIP_COLOUR_BLUE
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN BLIP_COLOUR_RED
ENDFUNC

//Get the map label to display for the specified ped blip.
FUNC TEXT_LABEL_15 GET_PED_BLIP_NAME(INT iPed)	
	TEXT_LABEL_15 tl15Blip = "BB_BLP_ENM"
	
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_COLLECT_FRIENDS
			tl15Blip = "BB_BLP_FRN"
		BREAK
		
		CASE FMBB_DEFEND_HOLD_UP
			SWITCH iPed
				CASE 0
				CASE 1
					tl15Blip = "BB_BLP_FRN"
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN tl15Blip
ENDFUNC

//Get the vehicle blip priority.
FUNC BLIP_PRIORITY GET_VEH_BLIP_PRIORITY()
	RETURN BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH
ENDFUNC

//Get whether the GPS route should be shown for a vehicle blip.
FUNC BOOL GET_SHOULD_SHOW_VEH_BLIP_ROUTE()
	RETURN FALSE
ENDFUNC

//Get the scale to use for the specified vehicle blip.
FUNC FLOAT GET_VEH_BLIP_SCALE(INT iVeh)
	IF IS_THIS_MODEL_A_HELI(GET_ENEMY_VEH_MODEL(iVeh))
	OR IS_THIS_MODEL_A_BOAT(GET_ENEMY_VEH_MODEL(iVeh))
		RETURN 1.0
	ENDIF
	
	RETURN 0.7
ENDFUNC

//Get the sprite to use for the specified vehicle blip.
FUNC BLIP_SPRITE GET_VEH_BLIP_SPRITE(INT iVeh)
	IF IS_THIS_MODEL_A_HELI(GET_ENEMY_VEH_MODEL(iVeh))
		RETURN RADAR_TRACE_ENEMY_HELI_SPIN
	ENDIF
	
	IF IS_THIS_MODEL_A_BOAT(GET_ENEMY_VEH_MODEL(iVeh))
		RETURN RADAR_TRACE_SEASHARK
	ENDIF
	
	RETURN RADAR_TRACE_TEMP_4
ENDFUNC

//Get the colour to use for the specified vehicle blip.
FUNC INT GET_VEH_BLIP_COLOUR()
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_STOLEN_VINYLS	RETURN BLIP_COLOUR_WHITE
	ENDSWITCH
	
	RETURN BLIP_COLOUR_RED
ENDFUNC

//Get the map label to display for the specified vehicle blip.
FUNC TEXT_LABEL_15 GET_VEH_BLIP_NAME()	
	TEXT_LABEL_15 tl15Blip = "BB_BLP_ENM"
	
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_STOLEN_VINYLS
			tl15Blip = ""
		BREAK
		
		DEFAULT
			tl15Blip = "BB_BLP_ENM"
		BREAK
	ENDSWITCH
	
	RETURN tl15Blip
ENDFUNC

//Get the mission entity blip priority.
FUNC BLIP_PRIORITY GET_MISSION_ENTITY_BLIP_PRIORITY()
	RETURN BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH
ENDFUNC

//Get whether the GPS route should be shown for a mission entity blip.
FUNC BOOL GET_SHOULD_SHOW_MISSION_ENTITY_BLIP_ROUTE()
	RETURN FALSE
ENDFUNC

//Get the colour to use for the specified mission entity blip.
FUNC INT GET_MISSION_ENTITY_BLIP_COLOUR()
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_COLLECT_FRIENDS
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT	RETURN BLIP_COLOUR_BLUE
	ENDSWITCH
	
	RETURN BLIP_COLOUR_GREEN
ENDFUNC

//Get the map label to display for the specified mission entity blip.
FUNC TEXT_LABEL_15 GET_MISSION_ENTITY_BLIP_NAME()	
	TEXT_LABEL_15 tl15Blip
	
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_RECOVER_PROPERTY
			tl15Blip = "BB_BLP_CPRP"
		BREAK
		
		CASE FMBB_DEFEND_STOLEN_VINYLS
			tl15Blip = "BB_BLP_MUS"
		BREAK
		
		CASE FMBB_DEFEND_COLLECT_FRIENDS
			tl15Blip = "BB_BLP_HEL"
		BREAK
		
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT
			tl15Blip = "BB_BLP_MEQP"
		BREAK
	ENDSWITCH
	
	RETURN tl15Blip
ENDFUNC

//Set the attributes for the specified mission entity blip.
PROC SET_MISSION_ENTITY_BLIP_ATTRIBUTES(INT iEntity)
	//Get the sprite to use for the specified mission entity blip.
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_COLLECT_FRIENDS
			SET_BLIP_SPRITE(blipMissionEntities[iEntity], RADAR_TRACE_HELICOPTER)
		BREAK
		
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT
			SET_BLIP_SPRITE(blipMissionEntities[iEntity], RADAR_TRACE_GANG_VEHICLE)
		BREAK
	ENDSWITCH
	
	SET_BLIP_PRIORITY(blipMissionEntities[iEntity], GET_MISSION_ENTITY_BLIP_PRIORITY())
	SET_BLIP_ROUTE(blipMissionEntities[iEntity], GET_SHOULD_SHOW_MISSION_ENTITY_BLIP_ROUTE())
	SET_BLIP_COLOUR(blipMissionEntities[iEntity], GET_MISSION_ENTITY_BLIP_COLOUR())
	SET_BLIP_AS_SHORT_RANGE(blipMissionEntities[iEntity], FALSE)
	
	TEXT_LABEL_15 tl15Blip = GET_MISSION_ENTITY_BLIP_NAME()
	SET_BLIP_NAME_FROM_TEXT_FILE(blipMissionEntities[iEntity], tl15Blip)
ENDPROC

//For variations where the drop off blip GPS route is hidden within a certain distance, return that distance.
FUNC FLOAT GET_DROP_OFF_BLIP_ROUTE_HIDE_DISTANCE()
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_RECOVER_PROPERTY
		CASE FMBB_DEFEND_STOLEN_VINYLS
			SWITCH GET_PLAYERS_OWNED_NIGHTCLUB(GB_GET_LOCAL_PLAYER_GANG_BOSS())
				CASE NIGHTCLUB_LA_MESA
				CASE NIGHTCLUB_STRAWBERRY_WAREHOUSE
				CASE NIGHTCLUB_LSIA_WAREHOUSE		RETURN 125.0
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT
			SWITCH GET_PLAYERS_OWNED_NIGHTCLUB(GB_GET_LOCAL_PLAYER_GANG_BOSS())
				CASE NIGHTCLUB_LA_MESA
				CASE NIGHTCLUB_STRAWBERRY_WAREHOUSE
				CASE NIGHTCLUB_LSIA_WAREHOUSE		RETURN 125.0
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_HOLD_UP
			SWITCH GET_PLAYERS_OWNED_NIGHTCLUB(GB_GET_LOCAL_PLAYER_GANG_BOSS())
				CASE NIGHTCLUB_LA_MESA
				CASE NIGHTCLUB_LSIA_WAREHOUSE		RETURN 125.0
				CASE NIGHTCLUB_MISSION_ROW			RETURN 100.0
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN -1.0
ENDFUNC

//Get whether SET_IGNORE_NO_GPS_FLAG should be set for a blip. This is used to modify the GPS route for some of the Nightclub drop-offs.
FUNC BOOL GET_SHOULD_IGNORE_GPS_FLAG()
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_RECOVER_PROPERTY
		CASE FMBB_DEFEND_STOLEN_VINYLS
			SWITCH GET_PLAYERS_OWNED_NIGHTCLUB(GB_GET_LOCAL_PLAYER_GANG_BOSS())
				CASE NIGHTCLUB_WEST_VINEWOOD
				CASE NIGHTCLUB_ELYSIAN_ISLAND
				CASE NIGHTCLUB_DEL_PERRO_BUILDING	RETURN TRUE
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT
			SWITCH GET_PLAYERS_OWNED_NIGHTCLUB(GB_GET_LOCAL_PLAYER_GANG_BOSS())
				CASE NIGHTCLUB_WEST_VINEWOOD
				CASE NIGHTCLUB_ELYSIAN_ISLAND
				CASE NIGHTCLUB_DEL_PERRO_BUILDING	RETURN TRUE
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_HOLD_UP
			SWITCH GET_PLAYERS_OWNED_NIGHTCLUB(GB_GET_LOCAL_PLAYER_GANG_BOSS())
				CASE NIGHTCLUB_ELYSIAN_ISLAND
				CASE NIGHTCLUB_DEL_PERRO_BUILDING
				CASE NIGHTCLUB_VESPUCCI_CANALS		RETURN TRUE
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

//Get the value to modify the Nightclub popularlity by.
FUNC FLOAT GET_NIGHTCLUB_POPULARITY_MODIFIER(BOOL bWin)
	IF bWin
		SWITCH serverBD.iVariation
			CASE FMBB_DEFEND_STOLEN_VINYLS
			CASE FMBB_DEFEND_COLLECT_FRIENDS
			CASE FMBB_DEFEND_MUSIC_EQUIPMENT
			CASE FMBB_DEFEND_HOLD_UP			RETURN g_sMPTunables.fBB_PHONECALL_MISSIONS_PASSED_PHONE_CALL_MISSION_POPULARITY_INCREASE * 100
		ENDSWITCH
	ELSE
		SWITCH serverBD.iVariation
			CASE FMBB_DEFEND_RECOVER_PROPERTY	RETURN -(g_sMPTunables.fBB_DEFEND_MISSIONS_FAILED_RECOVER_PROPERTY_POPULARITY_LOSS * 100)
			
			CASE FMBB_DEFEND_STOLEN_VINYLS
			CASE FMBB_DEFEND_COLLECT_FRIENDS
			CASE FMBB_DEFEND_MUSIC_EQUIPMENT
			CASE FMBB_DEFEND_HOLD_UP			RETURN -(g_sMPTunables.fBB_PHONECALL_MISSIONS_FAILED_PHONE_CALL_MISSION_POPULARITY_LOSS * 100)
		ENDSWITCH
	ENDIF
	
	RETURN 0.0
ENDFUNC

//Check whether the current variation has grouped peds.
FUNC BOOL SHOULD_ONLY_GROUPED_PEDS_REACT()
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_RECOVER_PROPERTY	RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

//Get the react group of the specified ped.
FUNC INT GET_PED_REACT_GROUP(INT iPed)
	RETURN serverBD.Targets[iPed].iGroup
ENDFUNC

//Check whether the specified ped is in a group.
FUNC BOOL IS_PED_IN_REACT_GROUP(INT iPed)
	RETURN GET_PED_REACT_GROUP(iPed) != -1
ENDFUNC

//Get whether the specified group should be reacting.
FUNC BOOL GET_GROUP_SHOULD_REACT(INT iGroup)
	RETURN IS_BIT_SET(serverBD.iGroupReactBitSet, iGroup)
ENDFUNC

//Get whether the specified ped should be reacting.
FUNC BOOL SHOULD_PED_GROUP_REACT(INT iPed)
	IF IS_PED_IN_REACT_GROUP(iPed)
		RETURN GET_GROUP_SHOULD_REACT(GET_PED_REACT_GROUP(iPed))
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Set the specifed group to react.
PROC SET_I_WANT_GROUP_TO_REACT(INT iGroup)
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iGroupReactBitSet, iGroup)
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iGroupReactBitSet, iGroup)
		PRINTLN("[FMBB_DEFEND] - [SET_I_WANT_GROUP_TO_REACT] iGroup = ", iGroup)
	ENDIF
ENDPROC

//Set the specified ped to react. Pass in -1 to make all peds react.
PROC SET_I_WANT_PED_TO_REACT(INT iPed = -1)
	IF iPed = -1
		PRINTLN("[FMBB_DEFEND] - [SET_I_WANT_PED_TO_REACT] iPed = -1. Setting all peds to react.")
		
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_AllPedsShouldReact)
			PRINTLN("[FMBB_DEFEND] - [SET_I_WANT_PED_TO_REACT] iPed = ", iPed, " but either this ped is not in a react group, or this variation does not have react groups. Setting all peds to react.")
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_AllPedsShouldReact)
		ENDIF
	ELSE
		IF IS_PED_IN_REACT_GROUP(iPed)
		AND SHOULD_ONLY_GROUPED_PEDS_REACT()
			SET_I_WANT_GROUP_TO_REACT(GET_PED_REACT_GROUP(iPed))
		ELSE
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_AllPedsShouldReact)
				PRINTLN("[FMBB_DEFEND] - [SET_I_WANT_PED_TO_REACT] iPed = ", iPed, " but either this ped is not in a react group, or this variation does not have react groups. Setting all peds to react.")
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_AllPedsShouldReact)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SET_PED_REACT_GROUP(INT iPed, INT iGroup)
	IF serverBD.Targets[iPed].iGroup != iGroup
		serverBD.Targets[iPed].iGroup = iGroup
		PRINTLN("[FMBB_DEFEND] - [SET_PED_REACT_GROUP] Setting iPed = ", iPed, " into iGroup = ", iGroup)
	ENDIF
ENDPROC

PROC SET_GROUP_SHOULD_REACT(INT iGroup)
	IF NOT IS_BIT_SET(serverBD.iGroupReactBitSet, iGroup)
		SET_BIT(serverBD.iGroupReactBitSet, iGroup)
		PRINTLN("[FMBB_DEFEND] - [SET_GROUP_SHOULD_REACT] Setting iGroup = ", iGroup, " to react.")
	ENDIF
ENDPROC

//Get the ticker label to display for taking the specified mission entity.
FUNC TEXT_LABEL_15 GET_MISSION_ENTITY_COLLECTED_TICKER_TEXT(INT iEntity, BOOL bLocalPlayer = FALSE)
	TEXT_LABEL_15 tl15Tick
	
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_RECOVER_PROPERTY
			SWITCH iEntity
				CASE 0
					IF bLocalPlayer
						tl15Tick = "BB_TT_RP_0C"
					ELSE
						tl15Tick = "BB_TT_RP_0B"
					ENDIF
				BREAK
				
				CASE 1
					IF bLocalPlayer
						tl15Tick = "BB_TT_RP_1C"
					ELSE
						tl15Tick = "BB_TT_RP_1B"
					ENDIF
				BREAK
				
				CASE 2
					IF bLocalPlayer
						tl15Tick = "BB_TT_RP_2C"
					ELSE
						tl15Tick = "BB_TT_RP_2B"
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_STOLEN_VINYLS
			IF bLocalPlayer
				tl15Tick = "BB_TT_SV_0B"
			ELSE
				tl15Tick = "BB_TT_SV_0A"
			ENDIF
		BREAK
		
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT
			IF bLocalPlayer
				tl15Tick = "BB_TT_ME_0B"
			ELSE
				tl15Tick = "BB_TT_ME_0A"
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN tl15Tick
ENDFUNC

//Get the ticker label to display for dropping the specified mission entity.
FUNC TEXT_LABEL_15 GET_MISSION_ENTITY_DROPPED_TICKER_TEXT(INT iEntity)
	TEXT_LABEL_15 tl15Tick
	
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_RECOVER_PROPERTY
			SWITCH iEntity
				CASE 0
					tl15Tick = "BB_TT_RP_0A"
				BREAK
				
				CASE 1
					tl15Tick = "BB_TT_RP_1A"
				BREAK
				
				CASE 2
					tl15Tick = "BB_TT_RP_2A"
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN tl15Tick
ENDFUNC

//Get the ticker label to display for delivering the specified mission entity.
FUNC TEXT_LABEL_15 GET_MISSION_ENTITY_DELIVERED_TICKER_TEXT(INT iEntity, BOOL bLocalPlayer = FALSE)
	TEXT_LABEL_15 tl15Tick
	
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_RECOVER_PROPERTY
			SWITCH iEntity
				CASE 0
					IF bLocalPlayer
						tl15Tick = "BB_TT_RP_0E"
					ELSE
						tl15Tick = "BB_TT_RP_0D"
					ENDIF
				BREAK
				
				CASE 1
					IF bLocalPlayer
						tl15Tick = "BB_TT_RP_1E"
					ELSE
						tl15Tick = "BB_TT_RP_1D"
					ENDIF
				BREAK
				
				CASE 2
					IF bLocalPlayer
						tl15Tick = "BB_TT_RP_2E"
					ELSE
						tl15Tick = "BB_TT_RP_2D"
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN tl15Tick
ENDFUNC

FUNC FMBB_DEFEND_COLLECTION_TYPE GET_VARIATION_COLLECTION_TYPE_FOR_TELEMETRY()
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_RECOVER_PROPERTY
		CASE FMBB_DEFEND_STOLEN_VINYLS		RETURN eFDCT_OBJECT
		
		
		CASE FMBB_DEFEND_MUSIC_EQUIPMENT	RETURN eFDCT_VEHICLE
		
		CASE FMBB_DEFEND_COLLECT_FRIENDS
		CASE FMBB_DEFEND_HOLD_UP			RETURN eFDCT_PED
	ENDSWITCH
	
	RETURN eFDCT_NA
ENDFUNC

FUNC FLOAT GET_MARKER_SCALE(BOOL bVehicle = FALSE)
	IF bVehicle
		RETURN 0.5
	ENDIF
	
	RETURN 0.2
ENDFUNC

FLOAT fVariationLaunchWeightings[2]
FUNC INT GET_DEFEND_VARIATION_FROM_WEIGHTINGS()
	fVariationLaunchWeightings[0] = g_sMPTunables.fBB_DEFEND_MISSIONS_WEIGHTING_POLICE_RAID
	fVariationLaunchWeightings[1] = g_sMPTunables.fBB_DEFEND_MISSIONS_WEIGHTING_RECOVER_PROPERTY
	
	FLOAT fTotalWeighting = 0.0
	fTotalWeighting += g_sMPTunables.fBB_DEFEND_MISSIONS_WEIGHTING_POLICE_RAID
	fTotalWeighting += g_sMPTunables.fBB_DEFEND_MISSIONS_WEIGHTING_RECOVER_PROPERTY
	
	FLOAT fSelection = GET_RANDOM_FLOAT_IN_RANGE(0, fTotalWeighting)
	FLOAT fCurrentWeighting = 0.0
	
	INT i = 0
	REPEAT 2 i
		fCurrentWeighting += fVariationLaunchWeightings[i]
		
		IF fSelection < fCurrentWeighting
			RETURN i
		ENDIF
	ENDREPEAT
	
	RETURN -1
ENDFUNC

//Returns true if the specified ped has any ambient speech.
FUNC BOOL DOES_PED_HAVE_AMBIENT_SPEECH(INT iPed)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_COLLECT_FRIENDS
			SWITCH iPed
				CASE 0
				CASE 1
				CASE 2	RETURN TRUE
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_HOLD_UP
			SWITCH iPed
				CASE 0
				CASE 1	RETURN TRUE
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

//Returns the PVG hash for the given ped. For use with SET_PED_VOICE_GROUP.
//This is sometimes needed to enable playing all speech contexts for a particular ped model.
FUNC INT GET_PED_VOICE_GROUP_HASH(INT iPed)
	SWITCH serverBD.iVariation
		CASE FMBB_DEFEND_COLLECT_FRIENDS
			SWITCH GET_PLAYERS_NIGHTCLUB_ACTIVE_DJ(GB_GET_LOCAL_PLAYER_GANG_BOSS())
				CASE DJ_SOLOMUN
					SWITCH iPed
						CASE 0	RETURN GET_HASH_KEY("A_M_M_EastSA_02_Latino_PVG")
						CASE 1	RETURN GET_HASH_KEY("A_M_M_Malibu_01_White_PVG")
					ENDSWITCH
				BREAK
				
				CASE DJ_DIXON
					SWITCH iPed
						CASE 0	RETURN GET_HASH_KEY("A_M_Y_Hipster_03_White_PVG")
						CASE 1	RETURN GET_HASH_KEY("A_F_Y_Hipster_01_White_PVG")
					ENDSWITCH
				BREAK
				
				CASE DJ_TALE_OF_US
					SWITCH iPed
						CASE 0	RETURN GET_HASH_KEY("A_M_Y_Vinewood_01_Black_PVG")
						CASE 1	RETURN GET_HASH_KEY("A_F_Y_SouCent_03_Latino_PVG")
					ENDSWITCH
				BREAK
				
				CASE DJ_BLACK_MADONNA
					SWITCH iPed
						CASE 0	RETURN GET_HASH_KEY("A_F_Y_BevHills_04_White_PVG")
						CASE 1	RETURN GET_HASH_KEY("A_F_Y_Hipster_04_White_PVG")
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMBB_DEFEND_HOLD_UP
			SWITCH GET_PLAYERS_NIGHTCLUB_ACTIVE_DJ(GB_GET_LOCAL_PLAYER_GANG_BOSS())
				CASE DJ_SOLOMUN
					SWITCH iPed
						CASE 0	RETURN GET_HASH_KEY("A_M_M_EastSA_02_Latino_PVG")
						CASE 1	RETURN GET_HASH_KEY("A_M_M_Malibu_01_White_PVG")
					ENDSWITCH
				BREAK
				
				CASE DJ_DIXON
					SWITCH iPed
						CASE 0	RETURN GET_HASH_KEY("A_M_Y_Hipster_03_White_PVG")
						CASE 1	RETURN GET_HASH_KEY("A_M_Y_Vinewood_02_White_PVG")
					ENDSWITCH
				BREAK
				
				CASE DJ_TALE_OF_US
					SWITCH iPed
						CASE 0	RETURN GET_HASH_KEY("A_M_Y_Vinewood_01_Black_PVG")
						CASE 1	RETURN GET_HASH_KEY("A_M_Y_Hipster_01_White_PVG")
					ENDSWITCH
				BREAK
				
				CASE DJ_BLACK_MADONNA
					SWITCH iPed
						CASE 0	RETURN GET_HASH_KEY("A_F_Y_BevHills_03_White_PVG")
						CASE 1	RETURN GET_HASH_KEY("A_M_Y_EastSA_01_Latino_PVG")
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

