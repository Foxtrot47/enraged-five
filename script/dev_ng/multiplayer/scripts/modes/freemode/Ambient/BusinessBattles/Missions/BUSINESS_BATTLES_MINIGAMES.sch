
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// BUSINESS_BATTLES_MINIGAMES.sch																						//
// Handles the playing of SFX and mission music																			//
// Written by:  Martin McMillan & Ryan Elliott																			//
// Date: 		08/12/2017																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//----------------------
//	INCLUDES
//----------------------

USING "rage_builtins.sch"
USING "globals.sch"

USING "BUSINESS_BATTLES_COMMON.sch"

//----------------------
//	SAFECRACKING
//----------------------

FUNC BOOL SHOULD_RUN_SAFE_CRACK_MINIGAME()
	RETURN FALSE
ENDFUNC

PROC SETUP_SAFE_DATA(OBJECT_INDEX tempObj)

	sSafeCrackData.vSafeCoord 				= (GET_ENTITY_COORDS(tempObj)-<<0, 0, 0.04>>) 
	sSafeCrackData.fSafeHeading				= GET_ENTITY_HEADING(tempObj)  
	sSafeCrackData.vSafeCoord 				= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(sSafeCrackData.vSafeCoord, sSafeCrackData.fSafeHeading, <<0.40, -0.46, -0.47>>)
	
	//serverBD.SafeData.vSafeDoor						= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(sSafeCrackData.vSafeCoord, sSafeCrackData.fSafeHeading, <<0, 0, 0.1>>)
	//serverBD.SafeData.fSafeDoorHeading				= sSafeCrackData.fSafeHeading
	
	sSafeCrackData.vPlayerSafeCrackCoord 		= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(sSafeCrackData.vSafeCoord, sSafeCrackData.fSafeHeading, <<-0.55, -0.55, 0>>)
	sSafeCrackData.fPlayerSafeCrackHeading 		= sSafeCrackData.fSafeHeading	
	PRINTLN("[RCC MISSION]   ---->  MISSION CONTROLLER - sSafeCrackData.vPlayerSafeCrackCoord ",sSafeCrackData.vPlayerSafeCrackCoord)
	
	sSafeCrackData.vFloatingHelpMin				= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(sSafeCrackData.vSafeCoord, sSafeCrackData.fSafeHeading, <<-5, -5, -1.0>>)	
	sSafeCrackData.vFloatingHelpMax				= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(sSafeCrackData.vSafeCoord, sSafeCrackData.fSafeHeading, <<5, 5, 2.0>>)
	sSafeCrackData.fFloatingHelpWidth			= 8.5
	PRINTLN("[RCC MISSION]   ---->  MISSION CONTROLLER  - sSafeCrackData.vFloatingHelpMin ",sSafeCrackData.vFloatingHelpMin) 
	PRINTLN("[RCC MISSION]   ---->  MISSION CONTROLLER  - sSafeCrackData.vFloatingHelpMax ",sSafeCrackData.vFloatingHelpMax) 
	
	sSafeCrackData.vCameraPos					= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(sSafeCrackData.vSafeCoord, sSafeCrackData.fSafeHeading, <<-0.8, -1.8, 2.0>>) // <<0.4, -1.2, 0.75+0.5>>
	sSafeCrackData.fCameraFOV					= 60.0
	
	sSafeCrackData.vSafeDoorProp				= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(sSafeCrackData.vSafeCoord, sSafeCrackData.fSafeHeading, <<0.32, -0.34, -0.68>>)
	
	sSafeCrackData.iLockRange					= g_sMPTunables.fBIKER_CRACKED_UNLOCK_WIDTH                                           
	sSafeCrackData.iSafeCrackUnlockTime			= g_sMPTunables.iBIKER_CRACKED_UNLOCK_TIME                                            
	
ENDPROC

FUNC BOOL SHOULD_RESET_MINIGAME_DATA(OBJECT_INDEX objectId)
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
		PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] SHOULD_RESET_MINIGAME_DATA - Local player is not okay. Reset minigame data.")
		RETURN TRUE
	ELIF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),objectId) >= 3.0
		PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] SHOULD_RESET_MINIGAME_DATA - Local player is too far from minigame object. Reset minigame data.")
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC RESET_SAFE_CRACKING_DATA()
	RESET_SAFE_CRACK(sSafeCrackData, TRUE)
	CLEAR_SAFE_CRACK_SEQUENCES(sSafeCrackData)
	CLEAR_CLIENT_BIT(eCLIENTBITSET_I_AM_DOING_MINIGAME)
	PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] RESET_SAFE_CRACKING_DATA - Safe cracking data reset for local player.")
ENDPROC

PROC PROCESS_SAFE_CRACKING_MINIGAME()
	IF IS_SERVER_BIT_SET(eSERVERBITSET_COMPLETED_MINIGAME)
		EXIT
	ENDIF
	
	IF GET_CLIENT_MODE_STATE() > eMODESTATE_MINIGAME
	OR GET_CLIENT_GAME_STATE(PARTICIPANT_ID_TO_INT()) > GAME_STATE_RUNNING
	OR IS_PED_RUNNING_MELEE_TASK(PLAYER_PED_ID())
		sSafeCrackData.bForceExitSafe = TRUE
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GET_MINIGAME_OBJECT_NETWORK_INDEX())
		OBJECT_INDEX safeObjectId = NET_TO_OBJ(GET_MINIGAME_OBJECT_NETWORK_INDEX())		
		IF NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_HAVE_COMPLETED_MINIGAME)
			IF NOT IS_ANY_PLAYER_CURRENTLY_DOING_MINIGAME(playerBD[PARTICIPANT_ID_TO_INT()].iCurrentInteractLocation)
				IF NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_AM_DOING_MINIGAME)
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),safeObjectId) < 3.0
						IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					
							VECTOR vAACoordOb = GET_ENTITY_COORDS(safeObjectId)
							VECTOR vAACoordPro = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(safeObjectId, <<0.0,-1.8,0>>)
							vAACoordOb.z = vAACoordOb.z + 2.0
							vAACoordPro.z = vAACoordPro.z - 1.5

							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vAACoordOb, vAACoordPro, 1.2)
								IF CREATE_SAFE_CRACK_SEQUENCES(sSafeCrackData, safeObjectId)
									SETUP_SAFE_DATA(safeObjectId)
									SET_CLIENT_BIT(eCLIENTBITSET_I_AM_DOING_MINIGAME)
									PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] PROCESS_SAFE_CRACKING_MINI_GAME - Local player has started cracking safe.")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					PROCESS_PLAYER_SAFE_CRACKING(sSafeCrackData, safeObjectId, playerBD[PARTICIPANT_ID_TO_INT()].iMinigameSyncSceneID)
					IF IS_PLAYER_SAFE_CRACKING(sSafeCrackData)
						SET_CLIENT_BIT(eCLIENTBITSET_I_AM_DOING_MINIGAME)
					ELSE
						CLEAR_CLIENT_BIT(eCLIENTBITSET_I_AM_DOING_MINIGAME)
					ENDIF
					
					IF SHOULD_RESET_MINIGAME_DATA(safeObjectId)
						RESET_SAFE_CRACKING_DATA()
					ENDIF
				ENDIF
			ELIF IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_AM_DOING_MINIGAME)	
				IF SHOULD_RESET_MINIGAME_DATA(safeObjectId)
					RESET_SAFE_CRACKING_DATA()
				ELSE
					PROCESS_PLAYER_SAFE_CRACKING(sSafeCrackData, safeObjectId, playerBD[PARTICIPANT_ID_TO_INT()].iMinigameSyncSceneID)
					IF HAS_PLAYER_CRACKED_SAFE(sSafeCrackData)
						RESET_SAFE_CRACKING_DATA()
						SET_CLIENT_BIT(eCLIENTBITSET_I_HAVE_COMPLETED_MINIGAME)
						PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] PROCESS_SAFE_CRACKING_MINI_GAME - Local player has finished cracking safe.")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC PROCESS_MANAGE_SAFE_CRACK_ANIM_CLIENT()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GET_MINIGAME_OBJECT_NETWORK_INDEX())
		OBJECT_INDEX safeObjectId = NET_TO_OBJ(GET_MINIGAME_OBJECT_NETWORK_INDEX())	
		IF MAINTAIN_CONTROL_OF_NETWORK_ID(GET_MINIGAME_OBJECT_NETWORK_INDEX())
			REMOVE_DECALS_FROM_OBJECT(safeObjectId)
		ENDIF
		IF playerBD[PARTICIPANT_ID_TO_INT()].iMinigameSyncSceneID = -1
			IF IS_ANY_PLAYER_CURRENTLY_DOING_MINIGAME(playerBD[PARTICIPANT_ID_TO_INT()].iCurrentInteractLocation)
				IF playerBD[serverBD.iCurrentMinigameParticipant[playerBD[PARTICIPANT_ID_TO_INT()].iCurrentInteractLocation]].iMinigameSyncSceneID != -1 
					playerBD[PARTICIPANT_ID_TO_INT()].iMinigameSyncSceneID = playerBD[serverBD.iCurrentMinigameParticipant[playerBD[PARTICIPANT_ID_TO_INT()].iCurrentInteractLocation]].iMinigameSyncSceneID
					PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] CLIENT_MANAGE_SAFE_CRACK_ANIM - iMinigameSyncSceneID = ", playerBD[PARTICIPANT_ID_TO_INT()].iMinigameSyncSceneID)
				ENDIF
			ENDIF
		ELSE
			MAINTAIN_PASSED_SAFE_CRACK_ANIM(safeObjectId, playerBD[PARTICIPANT_ID_TO_INT()].iMinigameSyncSceneID)
			
			INT iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(playerBD[PARTICIPANT_ID_TO_INT()].iMinigameSyncSceneID)
			IF iLocalSceneID != -1
				IF IS_PED_INJURED(PLAYER_PED_ID())
					IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_SYNCHRONIZED_SCENE) = PERFORMING_TASK
						IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "mini@safe_cracking", "DOOR_OPEN_SUCCEED_STAND", ANIM_SYNCED_SCENE)
							IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
								IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) >= 0.49
									IF GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_UD) != 0
									OR GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_LR) != 0
									OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM)
									OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_ATTACK)
									OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_ATTACK2)
										CLEAR_PED_TASKS(PLAYER_PED_ID())
										PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] CLIENT_MANAGE_SAFE_CRACK_ANIM - Player interupted during sync scene.")
									ENDIF
								ENDIF
							ENDIF
						ENDIF	
					ENDIF
				ELSE
					IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_SYNCHRONIZED_SCENE) = PERFORMING_TASK
						IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "mini@safe_cracking", "DOOR_OPEN_SUCCEED_STAND", ANIM_SYNCED_SCENE)
							IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
								IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_PLAYED_MINIGAME_SOUND_0)
								AND GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) >= 0.096
									VECTOR vSafe = GET_ENTITY_COORDS(safeObjectId, FALSE)
									PLAY_SOUND_FROM_COORD(-1, "Safe_Handle_Spin", vSafe, "DLC_Biker_Cracked_Sounds")
									SET_LOCAL_BIT(eLOCALBITSET_PLAYED_MINIGAME_SOUND_0)
									PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] CLIENT_MANAGE_SAFE_CRACK_ANIM - Played 'Safe_Handle_Spin' sound.")
								ENDIF
								IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_PLAYED_MINIGAME_SOUND_1)
								AND GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) >= 0.362
									VECTOR vSafe = GET_ENTITY_COORDS(safeObjectId, FALSE)
									PLAY_SOUND_FROM_COORD(-1, "Safe_Door_Open", vSafe, "DLC_Biker_Cracked_Sounds")
									SET_LOCAL_BIT(eLOCALBITSET_PLAYED_MINIGAME_SOUND_1)
									PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] CLIENT_MANAGE_SAFE_CRACK_ANIM - Played 'Safe_Door_Open' sound.")
								ENDIF
							ENDIF
						ENDIF	
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_PROCESS_MINIGAME_PLAYER_LOOP()
	IF SHOULD_PROCESS_INTERACT()
		RETURN TRUE
	ENDIF
	
	SWITCH GET_FMBB_VARIATION()
		CASE BBV_SHOWROOM		RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_CHECK_MINIGAME_OBJECT()
	RETURN FALSE
ENDFUNC

PROC PROCESS_MINIGAME_PLAYER_LOOP(INT iParticipant)
	IF NOT SHOULD_PROCESS_MINIGAME_PLAYER_LOOP()
		EXIT
	ENDIF

	// Participant checks
	IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
		IF IS_CLIENT_BIT_SET(INT_TO_PARTICIPANTINDEX(iParticipant), eCLIENTBITSET_I_AM_DOING_MINIGAME)
			IF NOT IS_ANY_PLAYER_CURRENTLY_DOING_MINIGAME(playerBD[iParticipant].iCurrentInteractLocation)
				serverBD.iCurrentMinigameParticipant[playerBD[iParticipant].iCurrentInteractLocation] = iParticipant
				PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] PROCESS_MINIGAME_PLAYER_LOOP - Participant #", iParticipant, " is cracking the safe at location ",playerBD[iParticipant].iCurrentInteractLocation)
			ENDIF	
		ENDIF
	ELSE
		IF IS_CLIENT_BIT_SET(INT_TO_PARTICIPANTINDEX(iParticipant), eCLIENTBITSET_I_AM_DOING_MINIGAME)
			IF serverBD.iCurrentMinigameParticipant[playerBD[iParticipant].iCurrentInteractLocation] = iParticipant
				serverBD.iCurrentMinigameParticipant[playerBD[iParticipant].iCurrentInteractLocation] = -1
				PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] PROCESS_MINIGAME_PLAYER_LOOP - Participant #", iParticipant, " left session while cracking the safe at location ",playerBD[iParticipant].iCurrentInteractLocation)
			ENDIF
		ENDIF
	ENDIF
	
	// End of loop checks
	IF iParticipant = (NETWORK_GET_MAX_NUM_PARTICIPANTS()-1)
	
		INT iInteract
		REPEAT NUM_INTERACT_LOCATIONS iInteract
			IF IS_ANY_PLAYER_CURRENTLY_DOING_MINIGAME(iInteract)
				IF NOT IS_CLIENT_BIT_SET(INT_TO_PARTICIPANTINDEX(serverBD.iCurrentMinigameParticipant[iInteract]), eCLIENTBITSET_I_AM_DOING_MINIGAME)
					PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] PROCESS_MINIGAME_PLAYER_LOOP - Server thinks Participant #", serverBD.iCurrentMinigameParticipant[iInteract], " is cracking safe when participant is not anymore.")
					serverBD.iCurrentMinigameParticipant[iInteract] = -1
				ENDIF
				
				IF SHOULD_CHECK_MINIGAME_OBJECT()
					IF NETWORK_DOES_NETWORK_ID_EXIST(GET_MINIGAME_OBJECT_NETWORK_INDEX())
						IF IS_ENTITY_DEAD(NET_TO_ENT(GET_MINIGAME_OBJECT_NETWORK_INDEX()))
							serverBD.iCurrentMinigameParticipant[iInteract] = -1
							PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] PROCESS_MINIGAME_PLAYER_LOOP - Safe is somehow dead while being cracked.")
						ENDIF
					ELSE
						serverBD.iCurrentMinigameParticipant[iInteract] = -1
						PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] PROCESS_MINIGAME_PLAYER_LOOP - Safe no longer exists while being cracked.")
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC
