
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// BUSINESS_BATTLES_VEHICLES.sch																						//
// Handles the creation and processing of non mission critical vehicles													//
// Written by:  Martin McMillan & Ryan Elliott.																			//
// Date: 		08/12/2017																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//----------------------
//	INCLUDES
//----------------------

USING "rage_builtins.sch"
USING "globals.sch"

USING "BUSINESS_BATTLES_COMMON.sch"



//////////////////////////////////////////
////  	CRITICAL ENTITY FUNCTIONS 	  ////
//////////////////////////////////////////  
FUNC BOOL SHOULD_VARIATION_HAVE_PASSENGER_SEAT_BOMBS()
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_MISSION_ALLOW_PASSENGER_CHAFF()
	
	IF SHOULD_VARIATION_HAVE_PASSENGER_SEAT_BOMBS()
		RETURN FALSE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC SETUP_VEHICLE_MODS(VEHICLE_INDEX& vehId)
	SWITCH GET_FMBB_VARIATION()
		CASE BBV_MAX
			IF GET_NUM_MOD_KITS(vehId) > 0
				SET_VEHICLE_MOD_KIT(vehId, 0)
				
				IF GET_NUM_VEHICLE_MODS(vehId, MOD_CHASSIS) > 0
					SET_VEHICLE_MOD(vehId, MOD_CHASSIS, 0)
				ENDIF
				IF GET_NUM_VEHICLE_MODS(vehId, MOD_ROOF) > 0	// Machines Guns
					SET_VEHICLE_MOD(vehId, MOD_ROOF, 0)
				ENDIF
				IF GET_NUM_VEHICLE_MODS(vehId, MOD_EXHAUST) > 0	// JTOL
					SET_VEHICLE_MOD(vehId, MOD_EXHAUST, 0)
				ENDIF
			ENDIF
		BREAK	
	ENDSWITCH
ENDPROC

FUNC BOOL DOES_MISSION_ENTITY_VEHICLE_NEED_HOTWIRED(INT iMissionEntity)
	
	UNUSED_PARAMETER(iMissionEntity)
	
	IF IS_THIS_VARIATION_A_SELL_MISSION()
	OR IS_THIS_VARIATION_A_SETUP_MISSION()
		RETURN FALSE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL DOES_MISSION_ENTITY_VEHICLE_NEED_BROKEN_INTO(INT iMissionEntity)
	
	UNUSED_PARAMETER(iMissionEntity)
	
	RETURN FALSE
	
ENDFUNC

FUNC FLOAT GET_RANDOM_DIRT_LEVEL()

	FLOAT fReturn
	
	INT iRand = GET_RANDOM_INT_IN_RANGE(0, 10000)
	
	IF iRand > 8000
		fReturn = 15.00
	ELIF iRand > 4000
		fReturn = 10.00
	ELIF iRand > 2000
		fReturn = 7.50
	ELSE
		fReturn = 5.00
	ENDIF

	RETURN fReturn
ENDFUNC

/// PURPOSE: USed to set the mission vehicle visual attributes, like engine on, door open, lights on, etc.  
PROC SET_MISSION_VEHICLE_COSMETIC_ATTRIBUTES_ON_SPAWN(INT iVehicle, VEHICLE_INDEX vehId)
	
	UNUSED_PARAMETER(iVehicle)
	
	MODEL_NAMES eModel = GET_ENTITY_MODEL(vehId)
	
	SWITCH eModel
		CASE DUMMY_MODEL_FOR_SCRIPT
		
		BREAK
	ENDSWITCH
	
	IF IS_THIS_VARIATION_A_SETUP_MISSION()
//		SET_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_FRONT_LEFT)
		SET_VEHICLE_DIRT_LEVEL(vehId, 15.00)	
		PRINTLN("SET_MISSION_VEHICLE_COSMETIC_ATTRIBUTES_ON_SPAWN, SET_VEHICLE_DIRT_LEVEL ")
	ENDIF

ENDPROC

FUNC BOOL SHOULD_VEHICLE_BE_LOCKED_ON_SPAWN(INT iMissionEntity)
	
	UNUSED_PARAMETER(iMissionEntity)
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_LOCK_VEHICLE_REAR_DOORS(INT iMissionEntity)

	UNUSED_PARAMETER(iMissionEntity)

	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_MISSION_ENTITY_DAMAGE_SCALE(MODEL_NAMES eModel)
	UNUSED_PARAMETER(eModel)
	
	SWITCH eModel
		CASE INSURGENT3
			RETURN 1.0
	ENDSWITCH
	
	RETURN 0.2
ENDFUNC

FUNC BOOL SHOULD_ADJUST_DAMAGE_SCALE(MODEL_NAMES eModel)
	IF GET_MISSION_ENTITY_DAMAGE_SCALE(eModel) != 1.0
		RETURN TRUE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_RESIST_EXPLOSION(MODEL_NAMES model)

	SWITCH model
		CASE BOMBUSHKA
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

FUNC FLOAT GET_PLANE_ENGINE_HEALTH(MODEL_NAMES model)

	SWITCH model
		CASE BOMBUSHKA
			RETURN 5000.0
	ENDSWITCH
	
	RETURN -1.0

ENDFUNC

FUNC BOOL SHOULD_WARP_MISSION_ENTITY_ON_STUCK()
	
	RETURN FALSE

ENDFUNC

FUNC BOOL SHOULD_OVERRIDE_NUMBER_PLATE(INT iMissionEntity)
	UNUSED_PARAMETER(iMissionEntity)
	RETURN FALSE
ENDFUNC

FUNC STRING GET_NUMBER_PLATE_OVERRIDE_STRING(INT iMissionEntity)
	UNUSED_PARAMETER(iMissionEntity)
	RETURN ""
ENDFUNC

FUNC BOOL SHOULD_MISSION_ENTITY_VEHICLE_START_MOVING_ON_SPAWN()
	
	RETURN FALSE

ENDFUNC

PROC SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES(INT iMissionEntity, MODEL_NAMES eModel, NETWORK_INDEX netID, BOOL bForFakeVehicle, BOOL bForMissionTrailerCab = FALSE)
	
	VEHICLE_INDEX vehId = NET_TO_VEH(netID)
	
	IF SHOULD_SPAWN_MISSION_ENTITY_INSIDE_INTERIOR()
		NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_ENT(netID), TRUE)
		SET_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_ONLY_EXISTS_FOR_PARTICIPANTS)
		SET_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_I_AM_IN_A_MISSION_INTERIOR)
	ENDIF
	
	// Health Stuff
	IF NOT bForFakeVehicle
	OR bForMissionTrailerCab
		SET_ENTITY_INVINCIBLE(vehId, TRUE)
	ENDIF
	SET_VEHICLE_TYRES_CAN_BURST(vehId, FALSE)
	PRINTLN("SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - SET_VEHICLE_TYRES_CAN_BURST(vehId, FALSE) - vehId = ", NATIVE_TO_INT(vehId))
	
	SET_VEHICLE_STRONG(vehId, TRUE)
	PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] [STRONG] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - Setting vehicle strong.")
	
	SET_VEHICLE_USES_MP_PLAYER_DAMAGE_MULTIPLIER(vehId, FALSE)
	PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - SET_VEHICLE_USES_MP_PLAYER_DAMAGE_MULTIPLIER = FALSE")
	
	IF SHOULD_ADJUST_DAMAGE_SCALE(GET_ENTITY_MODEL(vehId))
		SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(vehId, FALSE)
		PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE = FALSE")
	
		SET_VEHICLE_DAMAGE_SCALE(vehId, GET_MISSION_ENTITY_DAMAGE_SCALE(GET_ENTITY_MODEL(vehId)))
		PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - SET_VEHICLE_DAMAGE_SCALE = ",GET_MISSION_ENTITY_DAMAGE_SCALE(GET_ENTITY_MODEL(vehId)))
		
		IF IS_THIS_MODEL_A_PLANE(eModel)
			IF SHOULD_RESIST_EXPLOSION(eModel)
				SET_PLANE_RESIST_TO_EXPLOSION(vehId, TRUE)
			ENDIF
			SET_VEHICLE_CAN_BREAK(vehId, FALSE)
		ENDIF
	
		SET_ENTITY_HEALTH(vehId, 1000)
		PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - SET_ENTITY_HEALTH = 1000")
	ENDIF
	
	IF IS_THIS_MODEL_A_PLANE(eModel)
		SET_PLANE_TURBULENCE_MULTIPLIER(vehId, 0.0)
		PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - SET_PLANE_TURBULENCE_MULTIPLIER to 0 for mission vehicle ",iMissionEntity)
		
		IF GET_PLANE_ENGINE_HEALTH(eModel) != -1.0
			SET_PLANE_ENGINE_HEALTH(vehId, GET_PLANE_ENGINE_HEALTH(eModel))
		ENDIF
	ENDIF

	CALL_SET_ALLOW_RAMMING_SOOP_OR_RAMP(vehId)
	
	IF GET_ENTITY_MODEL(vehId) = GET_PHANTOM_TRAILER_MODEL()
		SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(vehId, FALSE)
		SET_ENTITY_PROOFS(vehId, FALSE, FALSE, FALSE, TRUE, FALSE, FALSE, FALSE, FALSE)
		PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - set phantom tailer does not explode on high explosion damage - SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(vehId, FALSE)")
		PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - set phantom tailer does not have collision damage - SET_ENTITY_PROOFS(vehId, FALSE, FALSE, FALSE, TRUE, FALSE, FALSE, FALSE, FALSE)")
	ENDIF
	
//	IF NOT bForFakeVehicle
//	AND NOT bForMissionTrailerCab
//		GB_SET_VEHICLE_AS_CONTRABAND(NET_TO_VEH(serverBD.sMissionEntities.netId[iMissionEntity]), iMissionEntity, GBCT_FMBB)
//	ENDIF
	
	SET_MENTAL_STATE_WILL_IGNORE_THIS_VEHICLE(vehId, TRUE)
	
	// Lock stuff and entering
	SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(vehId, TRUE)
	SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(vehId, TRUE)
	IF DOES_MISSION_ENTITY_VEHICLE_NEED_HOTWIRED(iMissionEntity)
		SET_VEHICLE_NEEDS_TO_BE_HOTWIRED(vehId, TRUE)
	ENDIF
	
	IF DOES_MISSION_ENTITY_VEHICLE_NEED_BROKEN_INTO(iMissionEntity)
		IF GET_FMBB_ENTITY_MODEL_FOR_THIS_VARIATION() != BTYPE3 // Add vehicles to this list if we want it unlocked and enterable with no break in anims.
		AND GET_FMBB_ENTITY_MODEL_FOR_THIS_VARIATION() != ZTYPE
		AND GET_FMBB_ENTITY_MODEL_FOR_THIS_VARIATION() != FELTZER3
			SET_VEHICLE_DOORS_LOCKED(vehId, VEHICLELOCK_LOCKED_BUT_CAN_BE_DAMAGED)
		ENDIF 
	ENDIF
	
	BOOL bLock = SHOULD_VEHICLE_BE_LOCKED_ON_SPAWN(iMissionEntity)
	SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehId, bLock)
	SET_VEHICLE_DISABLE_TOWING(vehId, bLock)

	SET_VEHICLE_DOORS_LOCKED_FOR_NON_SCRIPT_PLAYERS(vehId, TRUE)
	
	IF SHOULD_LOCK_VEHICLE_REAR_DOORS(iMissionEntity)
		SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(vehId, ENUM_TO_INT(SC_DOOR_REAR_LEFT), VEHICLELOCK_LOCKED)
		SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(vehId, ENUM_TO_INT(SC_DOOR_REAR_RIGHT), VEHICLELOCK_LOCKED)
		SET_OPEN_REAR_DOORS_ON_EXPLOSION(vehId, FALSE)
	ENDIF
	
	// Lock on stuff
	SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(vehId, FALSE)
	SET_VEHICLE_ALLOW_NO_PASSENGERS_LOCKON(vehId, FALSE)
	
	// Cosmetic stuff
	IF NOT bForMissionTrailerCab
		SETUP_VEHICLE_MODS(vehId)
		SET_MISSION_VEHICLE_COSMETIC_ATTRIBUTES_ON_SPAWN(iMissionEntity, vehId)
	ENDIF
	
	SWITCH GET_FMBB_VARIATION()
		CASE BBV_MAX
		
		BREAK
	ENDSWITCH

	SET_ENTITY_LOD_DIST(vehId, 1200)
	
	// KW 23/3/17 For bug 3437744 Sell - Phantom - Should the rear middle seat of the Phantom be blocked for MC / org members for this mission?
	IF bForMissionTrailerCab
		IF DOES_VARIATION_VEHICLE_HAVE_DOOR(vehId, SC_DOOR_REAR_LEFT)
			SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(vehId, ENUM_TO_INT(SC_DOOR_REAR_LEFT), VEHICLELOCK_LOCKED)
		ENDIF
		
		IF DOES_VARIATION_VEHICLE_HAVE_DOOR(vehId, SC_DOOR_REAR_RIGHT)
			SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(vehId, ENUM_TO_INT(SC_DOOR_REAR_RIGHT), VEHICLELOCK_LOCKED)
		ENDIF
	ENDIF 
	
	GB_SET_FREEMODE_DELIVERABLE_ID_DECOR(NET_TO_ENT(serverBD.sMissionEntities.netId[iMissionEntity]), serverBD.deliverableMissionIds[iMissionEntity].iIndex)
	SET_VEHICLE_ON_GROUND_PROPERLY(vehId)
	
	IF SHOULD_MISSION_ENTITY_VEHICLE_START_MOVING_ON_SPAWN()
		SET_VEHICLE_ENGINE_ON(vehID, TRUE, TRUE)
		//SET_VEHICLE_FORWARD_SPEED(vehId, 30.0)
		PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - SHOULD_MISSION_ENTITY_VEHICLE_START_MOVING_ON_SPAWN for mission vehicle ",iMissionEntity)
	ENDIF
	
	// Decorator stuff
	IF NOT bForFakeVehicle
	AND NOT bForMissionTrailerCab
		IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh", DECOR_TYPE_INT)
	    	DECOR_SET_INT(vehId, "Not_Allow_As_Saved_Veh", MP_NOT_ALLOWED_AS_SAVED_VEHICLE_DEC_SIMEON)
			PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] Contraband vehicle set as 'Not_Allow_As_Saved_Veh'.")
		ENDIF
		
		IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
			INT iDecoratorValue
			IF DECOR_EXIST_ON(vehId, "MPBitset")
				iDecoratorValue = DECOR_GET_INT(vehId, "MPBitset")
			ENDIF
			SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_PASSIVE_MODE_VEHICLE)
			SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_MODDABLE_VEHICLE)
			DECOR_SET_INT(vehId, "MPBitset", iDecoratorValue)
			
			PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] Contraband vehicle set as 'MP_DECORATOR_BS_NON_PASSIVE_MODE_VEHICLE'.")
			PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] Contraband vehicle set as 'MP_DECORATOR_BS_NON_MODDABLE_VEHICLE'.")
		ENDIF
	ENDIF
	
	UNUSED_PARAMETER(eModel)
ENDPROC

/////////////////////////////////////////////////
////  	NON CRITICAL VEHICLE ATTRIBUTES 	 ////
/////////////////////////////////////////////////  

FUNC FLOAT GET_SUPPORT_DAMAGE_SCALE(MODEL_NAMES eModel)
	UNUSED_PARAMETER(eModel)
	
	SWITCH eModel
		CASE MOGUL		RETURN 0.5
	ENDSWITCH
	
	RETURN 0.3
ENDFUNC

PROC SET_SUPPORT_VEHICLE_COSMETIC_ATTRIBUTES(VEHICLE_INDEX vehId)
	UNUSED_PARAMETER(vehID)
ENDPROC

PROC SET_SUPPORT_VEHICLE_SPAWN_ATTRIBUTES(INT iVehicle, VEHICLE_INDEX vehId)
	MODEL_NAMES eModel = GET_ENTITY_MODEL(vehId)
	PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_SUPPORT_VEHICLE_SPAWN_ATTRIBUTES - Setting up support vehicle ", GET_SUPPORT_VEHICLE_INDEX(iVehicle), " with model ", GET_MODEL_NAME_FOR_DEBUG(eModel))
	
	// Damage Modifiers
	SET_VEHICLE_TYRES_CAN_BURST(vehId, FALSE)
	IF GET_SUPPORT_DAMAGE_SCALE(eModel) != 1.0
		SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(vehId, FALSE)
		SET_VEHICLE_STRONG(vehId, TRUE)
		SET_VEHICLE_DAMAGE_SCALE(vehId, GET_SUPPORT_DAMAGE_SCALE(eModel))
		PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] SET_SUPPORT_VEHICLE_SPAWN_ATTRIBUTES - Set vehicle to take additional explosive damage. Damage scale = ", GET_SUPPORT_DAMAGE_SCALE(eModel))
	ENDIF
	IF IS_THIS_MODEL_A_PLANE(eModel)
		SET_VEHICLE_CAN_BREAK(vehId, FALSE)
	ENDIF
		
	// Cosmetic
	SET_SUPPORT_VEHICLE_COSMETIC_ATTRIBUTES(vehID)
	
	// Lock state
	SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(vehId, TRUE)
	SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(vehId, TRUE)
	SET_VEHICLE_DOORS_LOCKED_FOR_NON_SCRIPT_PLAYERS(vehID, TRUE)
	
	// Mission specific support vehicle attributes
	SWITCH GET_FMBB_VARIATION()
		CASE BBV_MAX	
		
		BREAK
	ENDSWITCH

	UNUSED_PARAMETER(iVehicle)
ENDPROC

FUNC BOOL SHOULD_LOCK_CARRIER_VEHICLE_ON_SPAWN()

	SWITCH GET_FMBB_VARIATION()
		CASE BBV_UNDER_GUARD
		CASE BBV_BREAK_IN
		CASE BBV_SHOWROOM
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE

ENDFUNC

FUNC BOOL SHOULD_CARRIER_VEHICLE_NEED_HOTWIRED()

	SWITCH GET_FMBB_VARIATION()
		CASE BBV_HAULAGE
		CASE BBV_JOYRIDERS
			RETURN FALSE
	ENDSWITCH

	RETURN TRUE

ENDFUNC

FUNC BOOL SHOULD_CARRIER_VEHICLE_NEED_BROKEN_INTO(MODEL_NAMES eModel)

	SWITCH GET_FMBB_VARIATION()
		CASE BBV_SHOWROOM
		CASE BBV_UNDER_GUARD
		CASE BBV_HAULAGE
		CASE BBV_JOYRIDERS
		CASE BBV_CAR_MEET
		CASE BBV_CARPARK_SEARCH
			RETURN FALSE
	ENDSWITCH
	
	SWITCH eModel
		CASE HALFTRACK
		CASE INSURGENT3
			RETURN FALSE
	ENDSWITCH

	RETURN TRUE

ENDFUNC

PROC SET_BUSINESS_BATTLES_DECORATOR_ON_VEHICLE(VEHICLE_INDEX vehId)

	IF DECOR_IS_REGISTERED_AS_TYPE("BBCarrier", DECOR_TYPE_BOOL)
		DECOR_SET_BOOL(vehID, "BBCarrier", TRUE)
		PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_BUSINESS_BATTLES_DECORATOR_ON_VEHICLE")
	ENDIF
		
ENDPROC

PROC SET_CARRIER_VEHICLE_SPAWN_ATTRIBUTES(INT iVehicle, VEHICLE_INDEX vehId)

	UNUSED_PARAMETER(iVehicle)

	PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_CARRIER_VEHICLE_SPAWN_ATTRIBUTES")

	MODEL_NAMES eModel = GET_ENTITY_MODEL(vehId)
	
	VEHICLE_SETUP_STRUCT_MP sData // Use for any mods
	
	SWITCH GET_FMBB_VARIATION()
		CASE BBV_SHOWROOM
			IF GET_ENTITY_MODEL(vehId) = ellie
				sData.VehicleSetup.tlPlateText = "RA1NE"
				sData.VehicleSetup.iColour1 = 9
				sData.VehicleSetup.iColour2 = 111
				sData.VehicleSetup.iColourExtra1 = 7
				sData.VehicleSetup.iColourExtra2 = 156
				sData.iColour5 = 1
				sData.iColour6 = 132
				sData.iLivery2 = 0
				sData.VehicleSetup.iWindowTintColour = 3
				sData.VehicleSetup.iWheelType = 1
				sData.VehicleSetup.iTyreR = 255
				sData.VehicleSetup.iTyreG = 255
				sData.VehicleSetup.iTyreB = 255
				sData.VehicleSetup.iNeonR = 255
				sData.VehicleSetup.iNeonB = 255
				sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
				sData.VehicleSetup.iModIndex[MOD_WHEELS] = 8
				sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
				SET_VEHICLE_SETUP_MP(vehId, sData, FALSE, TRUE, TRUE)
			ENDIF
			SET_VEHICLE_DOORS_LOCKED(vehId, VEHICLELOCK_CANNOT_ENTER) 
		BREAK
		CASE BBV_UNDER_GUARD
			SWITCH GET_ENTITY_MODEL(vehId)
				CASE INSURGENT3
					sData.VehicleSetup.eModel = insurgent3 // INSURGENT3
					sData.VehicleSetup.iColour1 = 152
					sData.VehicleSetup.iColour2 = 152
					sData.VehicleSetup.iColourExtra2 = 152
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 3
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
					sData.VehicleSetup.iModIndex[MOD_WING_R] = 1
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
					sData.VehicleSetup.iModIndex[MOD_ARMOUR] = 5
					SET_VEHICLE_SETUP_MP(vehId, sData, FALSE, TRUE, TRUE)
				BREAK
				CASE TECHNICAL3
					sData.VehicleSetup.eModel = technical3 // TECHNICAL3
					sData.VehicleSetup.tlPlateText = "20ROF370"
					sData.VehicleSetup.iColour1 = 154
					sData.VehicleSetup.iColour2 = 12
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 4
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 3
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
					SET_VEHICLE_SETUP_MP(vehId, sData, FALSE, TRUE, TRUE)
				BREAK
				CASE BARRAGE
					sData.VehicleSetup.eModel = barrage // BARRAGE
					sData.VehicleSetup.tlPlateText = "07EMN393"
					sData.VehicleSetup.iColour2 = 1
					sData.VehicleSetup.iColourExtra1 = 10
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 4
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 10
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 10
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 19
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 27
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 8
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 20
					sData.VehicleSetup.iModIndex[MOD_WING_L] = 6
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 8
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 6
					SET_VEHICLE_SETUP_MP(vehId, sData, FALSE, TRUE, TRUE)
				BREAK
			ENDSWITCH
		BREAK
		CASE BBV_CARPARK_SEARCH
			SWITCH GET_ENTITY_MODEL(vehId)
				CASE ISSI3
					sData.VehicleSetup.eModel = issi3 // ISSI3
					sData.VehicleSetup.tlPlateText = "C607287R"
					sData.VehicleSetup.iColour1 = 91
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 2
					SET_VEHICLE_SETUP_MP(vehId, sData, FALSE, TRUE, TRUE)
				BREAK
				CASE RAPTOR
					sData.VehicleSetup.eModel = raptor // RAPTOR
					sData.VehicleSetup.iColour1 = 146
					sData.VehicleSetup.iColour2 = 141
					sData.VehicleSetup.iColourExtra1 = 145
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 7
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 19
					SET_VEHICLE_SETUP_MP(vehId, sData, FALSE, TRUE, TRUE)
				BREAK
				CASE PANTO
					sData.VehicleSetup.eModel = panto // PANTO
					sData.VehicleSetup.iColour1 = 137
					sData.VehicleSetup.iColour2 = 5
					sData.VehicleSetup.iColourExtra1 = 3
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_2)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_3)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_4)
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 5
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 4
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
					SET_VEHICLE_SETUP_MP(vehId, sData, FALSE, TRUE, TRUE)
				BREAK
			ENDSWITCH
		BREAK
		CASE BBV_CAR_MEET
			SET_VEHICLE_LIGHTS_ON_AT_NIGHT(vehID)
			
			SWITCH GET_ENTITY_MODEL(vehId)
				CASE HERMES
					sData.VehicleSetup.eModel = hermes // HERMES
					sData.VehicleSetup.iColourExtra1 = 10
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWindowTintColour = 3
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 4
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
					sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 1
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 3
					SET_VEHICLE_SETUP_MP(vehId, sData, FALSE, TRUE, TRUE)
				BREAK
				CASE sabregt2
					sData.VehicleSetup.eModel = sabregt2 // SABREGT2
					sData.VehicleSetup.iColour1 = 53
					sData.VehicleSetup.iColour2 = 120
					sData.VehicleSetup.iColourExtra1 = 59
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 94
					sData.VehicleSetup.iNeonG = 255
					sData.VehicleSetup.iNeonB = 1
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 4
					sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
					sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 1
					sData.VehicleSetup.iModIndex[MOD_PLTHOLDER] = 2
					sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 2
					sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 4
					sData.VehicleSetup.iModIndex[MOD_STEERING] = 8
					sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 11
					sData.VehicleSetup.iModIndex[MOD_ICE] = 1
					sData.VehicleSetup.iModIndex[MOD_HYDRO] = 1
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 6
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 4
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 5
					SET_VEHICLE_SETUP_MP(vehId, sData, FALSE, TRUE, TRUE)
				BREAK
				CASE dominator3
					sData.VehicleSetup.eModel = dominator3 // DOMINATOR3
					sData.VehicleSetup.iColour1 = 73
					sData.VehicleSetup.iColourExtra1 = 73
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 5
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 3
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 5
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 4
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 4
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 9
					sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
					sData.VehicleSetup.iModIndex[MOD_WING_R] = 1
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 12
					sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
					SET_VEHICLE_SETUP_MP(vehId, sData, FALSE, TRUE, TRUE)
				BREAK
			ENDSWITCH
		BREAK
		CASE BBV_HAULAGE
			SWITCH GET_ENTITY_MODEL(vehId)
				CASE CHEETAH2
					sData.VehicleSetup.iColour1 = 28
					sData.VehicleSetup.iColour2 = 7
					sData.VehicleSetup.iColourExtra1 = 28
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWindowTintColour = 3
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 17
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 7
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 4
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 3
					sData.VehicleSetup.iModIndex[MOD_WING_L] = 2
					sData.VehicleSetup.iModIndex[MOD_WING_R] = 4
					sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
					SET_VEHICLE_SETUP_MP(vehId, sData, FALSE, TRUE, TRUE)
				BREAK
				CASE GP1
					sData.VehicleSetup.iColour1 = 38
					sData.VehicleSetup.iColourExtra1 = 37
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 7
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 8
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 7
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 10
					sData.VehicleSetup.iModIndex[MOD_WING_L] = 3
					sData.VehicleSetup.iModIndex[MOD_WING_R] = 1
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 4
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 19
					sData.VehicleSetup.iModVariation[0] = 1
					SET_VEHICLE_SETUP_MP(vehId, sData, FALSE, TRUE, TRUE)
				BREAK
				CASE MAMBA
					sData.VehicleSetup.iColour1 = 50
					sData.VehicleSetup.iColour2 = 112
					sData.VehicleSetup.iColourExtra1 = 53
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
					sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 6
					SET_VEHICLE_SETUP_MP(vehId, sData, FALSE, TRUE, TRUE)
				BREAK
			ENDSWITCH
		BREAK
		DEFAULT
			SWITCH GET_ENTITY_MODEL(vehId)
				CASE RUINER2
					sData.VehicleSetup.eModel = ruiner2 // RUINER2
					sData.VehicleSetup.iColour1 = 118
					sData.VehicleSetup.iColour2 = 112
					sData.VehicleSetup.iColourExtra1 = 3
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWindowTintColour = 3
					sData.VehicleSetup.iWheelType = 7
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_6)
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 10
					SET_VEHICLE_SETUP_MP(vehId, sData, FALSE, TRUE, TRUE)
				BREAK
				CASE DUNE5
					sData.VehicleSetup.eModel = dune5 // DUNE5
					sData.VehicleSetup.iColour1 = 12
					sData.VehicleSetup.iColour2 = 151
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
					SET_VEHICLE_SETUP_MP(vehId, sData, FALSE, TRUE, TRUE)
				BREAK
				CASE TECHNICAL3
					sData.VehicleSetup.eModel = technical3 // TECHNICAL3
					sData.VehicleSetup.iColour1 = 131
					sData.VehicleSetup.iColour2 = 12
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 4
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 5
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 9
					SET_VEHICLE_SETUP_MP(vehId, sData, FALSE, TRUE, TRUE)
				BREAK
				CASE INSURGENT3
					sData.VehicleSetup.eModel = insurgent3 // INSURGENT3
					sData.VehicleSetup.iColour1 = 153
					sData.VehicleSetup.iColour2 = 12
					sData.VehicleSetup.iColourExtra2 = 152
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 3
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
					sData.VehicleSetup.iModIndex[MOD_ARMOUR] = 5
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 18
					SET_VEHICLE_SETUP_MP(vehId, sData, FALSE, TRUE, TRUE)
				BREAK
				CASE HALFTRACK
					sData.VehicleSetup.eModel = halftrack // HALFTRACK
					sData.VehicleSetup.iColour1 = 151
					sData.VehicleSetup.iColour2 = 151
					sData.VehicleSetup.iColourExtra2 = 154
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
					
					IF GET_FMBB_VARIATION() = BBV_JOYRIDERS
						sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 0
					ELSE
						sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
					ENDIF
					
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
					SET_VEHICLE_SETUP_MP(vehId, sData, FALSE, TRUE, TRUE)
				BREAK
				CASE APC
					sData.VehicleSetup.eModel = apc // APC
					sData.VehicleSetup.iColour1 = 154
					sData.VehicleSetup.iColour2 = 154
					sData.VehicleSetup.iColourExtra1 = 154
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
					sData.VehicleSetup.iModIndex[MOD_WING_R] = 1
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 5
					SET_VEHICLE_SETUP_MP(vehId, sData, FALSE, TRUE, TRUE)
				BREAK
				CASE BARRAGE
					sData.VehicleSetup.eModel = barrage // BARRAGE
					sData.VehicleSetup.iColour1 = 128
					sData.VehicleSetup.iColour2 = 154
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 9
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 9
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 20
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 22
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 3
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 21
					sData.VehicleSetup.iModIndex[MOD_WING_L] = 7
					sData.VehicleSetup.iModIndex[MOD_WING_R] = 5
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 9
					SET_VEHICLE_SETUP_MP(vehId, sData, FALSE, TRUE, TRUE)
				BREAK
				CASE DUNE3
					sData.VehicleSetup.eModel = dune3 // DUNE3
					sData.VehicleSetup.iColour1 = 154
					sData.VehicleSetup.iColour2 = 154
					sData.VehicleSetup.iColourExtra1 = 154
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
					sData.VehicleSetup.iModIndex[MOD_WING_R] = 1
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
					SET_VEHICLE_SETUP_MP(vehId, sData, FALSE, TRUE, TRUE)
				BREAK
				CASE TAMPA3
					sData.VehicleSetup.eModel = tampa3 // TAMPA3
					sData.VehicleSetup.iColour1 = 117
					sData.VehicleSetup.iColour2 = 3
					sData.VehicleSetup.iColourExtra1 = 18
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 3
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
					sData.VehicleSetup.iModIndex[MOD_WING_R] = 1
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
					SET_VEHICLE_SETUP_MP(vehId, sData, FALSE, TRUE, TRUE)
				BREAK
				CASE CARACARA
					sData.VehicleSetup.eModel = caracara // CARACARA
					sData.VehicleSetup.iColour1 = 14
					sData.VehicleSetup.iColour2 = 131
					sData.VehicleSetup.iColourExtra2 = 2
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWindowTintColour = 1
					sData.VehicleSetup.iWheelType = 4
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 1
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 19
					SET_VEHICLE_SETUP_MP(vehId, sData, FALSE, TRUE, TRUE)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH

	SET_VEHICLE_TYRES_CAN_BURST(vehId, FALSE)
	SET_VEHICLE_STRONG(vehId, TRUE)
	SET_VEHICLE_USES_MP_PLAYER_DAMAGE_MULTIPLIER(vehId, FALSE)
	
	IF SHOULD_ADJUST_DAMAGE_SCALE(GET_ENTITY_MODEL(vehId))
		SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(vehId, FALSE)
	
		SET_VEHICLE_DAMAGE_SCALE(vehId, GET_MISSION_ENTITY_DAMAGE_SCALE(GET_ENTITY_MODEL(vehId)))
		
		IF IS_THIS_MODEL_A_PLANE(eModel)
			//SET_PLANE_RESIST_TO_EXPLOSION(vehId, TRUE)
			SET_VEHICLE_CAN_BREAK(vehId, FALSE)
		ENDIF
	
		SET_ENTITY_HEALTH(vehId, 1000)
		PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_CARRIER_VEHICLE_SPAWN_ATTRIBUTES - SHOULD_ADJUST_DAMAGE_SCALE")
	ENDIF
	
	CALL_SET_ALLOW_RAMMING_SOOP_OR_RAMP(vehId)
	
	SET_MENTAL_STATE_WILL_IGNORE_THIS_VEHICLE(vehId, TRUE)
	
	IF GET_FMBB_BUSINESS_TYPE() = BBT_EXPORT_GARAGE
		GB_SET_VEHICLE_AS_CONTRABAND(vehId, 0, GBCT_FMBB)
		PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_CARRIER_VEHICLE_SPAWN_ATTRIBUTES - GB_SET_VEHICLE_AS_CONTRABAND")
	ENDIF
	
	SET_BUSINESS_BATTLES_DECORATOR_ON_VEHICLE(vehId)
	
	SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(vehId, FALSE)
	SET_VEHICLE_ALLOW_NO_PASSENGERS_LOCKON(vehId, FALSE)
	
	IF SHOULD_LOCK_CARRIER_VEHICLE_ON_SPAWN()
		SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehId, TRUE)
		SET_VEHICLE_DISABLE_TOWING(vehId, TRUE)
	ENDIF
	
	SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(vehId, TRUE)
	SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(vehId, TRUE)
	SET_VEHICLE_DOORS_LOCKED_FOR_NON_SCRIPT_PLAYERS(vehID, TRUE)
	
	IF SHOULD_CARRIER_VEHICLE_NEED_HOTWIRED()
		SET_VEHICLE_NEEDS_TO_BE_HOTWIRED(vehId, TRUE)
	ENDIF
	
	IF SHOULD_CARRIER_VEHICLE_NEED_BROKEN_INTO(eModel)
		SET_VEHICLE_DOORS_LOCKED(vehId, VEHICLELOCK_LOCKED_BUT_CAN_BE_DAMAGED)
	ENDIF
	
	IF eModel = PHANTOM2
	OR eModel = DUNE5
		VEHICLE_SET_RAMP_AND_RAMMING_CARS_TAKE_DAMAGE(vehId, FALSE)
		PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_CARRIER_VEHICLE_SPAWN_ATTRIBUTES - VEHICLE_SET_RAMP_AND_RAMMING_CARS_TAKE_DAMAGE(vehId, FALSE)")
	ENDIF
ENDPROC

FUNC BOOL SHOULD_SET_UP_CARRIER_VEHICLE_SPAWN_ATTRIBUTES(INT iVehicle)
	IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_FORCED_ENTRY_VEH_COSMETICS(INT iModel, VEHICLE_INDEX vehId)

	VEHICLE_SETUP_STRUCT_MP sData
	
	IF iModel = 1

		sData.VehicleSetup.eModel = BUCCANEER2 // BUCCANEE2						
		sData.VehicleSetup.iColour1 = 145						
		sData.VehicleSetup.iColourExtra1 = 74						
		sData.VehicleSetup.iColourExtra2 = 90						
		sData.iColour5 = 1						
		sData.iColour6 = 132						
		sData.iLivery2 = 0						
		sData.VehicleSetup.iWheelType = 1						
		sData.VehicleSetup.iTyreR = 255						
		sData.VehicleSetup.iTyreG = 255						
		sData.VehicleSetup.iTyreB = 255						
		sData.VehicleSetup.iNeonR = 255						
		sData.VehicleSetup.iNeonB = 255						
		SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_2)						
		sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1						
		sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1						
		sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1						
		sData.VehicleSetup.iModIndex[MOD_GRILL] = 1						
		sData.VehicleSetup.iModIndex[MOD_BONNET] = 3						
		sData.VehicleSetup.iModIndex[MOD_WING_L] = 1						
		sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 1						
		sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 3						
		sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 19						
		sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 1						
		sData.VehicleSetup.iModIndex[MOD_ICE] = 4						
		sData.VehicleSetup.iModIndex[MOD_TRUNK] = 7						
		sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 5						
		sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 3						
		sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 1						
		sData.VehicleSetup.iModIndex[MOD_LIVERY] = 5		
		
		SET_VEHICLE_SETUP_MP(vehId, sData, FALSE, TRUE, TRUE)
		
	ELIF iModel = 2
		
		sData.VehicleSetup.eModel = BUCCANEER2 // BUCCANEE2						
		sData.VehicleSetup.iColour1 = 145						
		sData.VehicleSetup.iColour2 = 89						
		sData.VehicleSetup.iColourExtra1 = 74						
		sData.VehicleSetup.iColourExtra2 = 90						
		sData.iColour5 = 1						
		sData.iColour6 = 132						
		sData.iLivery2 = 0						
		sData.VehicleSetup.iWheelType = 9						
		sData.VehicleSetup.iTyreR = 255						
		sData.VehicleSetup.iTyreG = 255						
		sData.VehicleSetup.iTyreB = 255						
		sData.VehicleSetup.iNeonR = 255						
		sData.VehicleSetup.iNeonB = 255						
		sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1						
		sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1						
		sData.VehicleSetup.iModIndex[MOD_BONNET] = 1						
		sData.VehicleSetup.iModIndex[MOD_WHEELS] = 93						
		sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 1						
		sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 1						
		sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 1						
		sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 3						
		sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 22						
		sData.VehicleSetup.iModIndex[MOD_ICE] = 4						
		sData.VehicleSetup.iModIndex[MOD_TRUNK] = 7						
		sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 5						
		sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 4						
		sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 2						
		sData.VehicleSetup.iModIndex[MOD_LIVERY] = 9			
			
		SET_VEHICLE_SETUP_MP(vehId, sData, FALSE, TRUE, TRUE)
	ENDIF

ENDPROC

PROC SET_CAR_COLLECTOR_COSMETIC_ATTRIBUTES(VEHICLE_INDEX vehId)	
	MODEL_NAMES eModel = GET_ENTITY_MODEL(vehId)
	
	SWITCH eModel
		CASE XLS
			SET_VEHICLE_COLOURS(vehId, 12, 12)
			SET_VEHICLE_EXTRA_COLOURS(vehId, 12, 12)
		BREAK
	ENDSWITCH
ENDPROC

PROC SET_UNDER_GUARD_VEHICLE_ATTRIBUTES(INT iVeh, VEHICLE_INDEX vehID)

	SWITCH GET_FMBB_SUBVARIATION()
		CASE BBS_UG_FORT_ZANCUDO
			SWITCH iVeh
				CASE 3
				CASE 4
					SET_VEHICLE_LIGHTS_ON_AT_NIGHT(vehID)
				BREAK
			ENDSWITCH
		BREAK
		CASE BBS_UG_UNION_DEPOSITORY
			SWITCH iVeh
				CASE 0 
				CASE 1
				CASE 4
				CASE 5
					SET_VEHICLE_DOOR_OPEN(vehID,SC_DOOR_REAR_LEFT)
					SET_VEHICLE_DOOR_OPEN(vehID,SC_DOOR_REAR_RIGHT)
				BREAK
				CASE 2
				CASE 3
					SET_VEHICLE_DOOR_OPEN(vehID,SC_DOOR_BOOT)
					SET_VEHICLE_DOOR_OPEN(vehID,SC_DOOR_BOOT)
				BREAK
			ENDSWITCH
		BREAK
		CASE BBS_UG_ELYSIAN_ISLAND
			SWITCH iVeh
				CASE 3
				CASE 4
					SET_VEHICLE_COLOURS(vehID, 0,0)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH

ENDPROC

FUNC INT GET_CAR_MEET_VEHICLE_MOD_VARIATION(INT iVeh)

	SWITCH GET_FMBB_SUBVARIATION()
		CASE BBS_CM_SAN_ANDREAS
			SWITCH iVeh
				CASE 0		RETURN 2 
				CASE 1		RETURN 0
				CASE 2		RETURN 1
				CASE 3		RETURN 2
				CASE 4		RETURN 0
				CASE 5		RETURN 1
				CASE 6		RETURN 2
				CASE 7		RETURN 0
				CASE 8		RETURN 1
				CASE 9		RETURN 0
				CASE 10		RETURN 1
				CASE 11		RETURN 2
				CASE 12		RETURN 0
				CASE 13		RETURN 1
				CASE 14		RETURN 2
			ENDSWITCH
		BREAK
		CASE BBS_CM_WEST_VINEWOOD
			SWITCH iVeh
				CASE 0		RETURN 2 
				CASE 1		RETURN 0
				CASE 2		RETURN 1
				CASE 3		RETURN 2
				CASE 4		RETURN 0
				CASE 5		RETURN 1
				CASE 6		RETURN 2
				CASE 7		RETURN 0
				CASE 8		RETURN 1
				CASE 9		RETURN 2
				CASE 10		RETURN 0
				CASE 11		RETURN 1
				CASE 12		RETURN 0
				CASE 13		RETURN 1
				CASE 14		RETURN 2
			ENDSWITCH
		BREAK
		CASE BBS_CM_PACIFIC_BLUFFS
			SWITCH iVeh
				CASE 0		RETURN 2
				CASE 1		RETURN 0
				CASE 2		RETURN 1
				CASE 3		RETURN 2
				CASE 4		RETURN 0
				CASE 5		RETURN 1
				CASE 6		RETURN 2
				CASE 7		RETURN 0
				CASE 8		RETURN 1
				CASE 9		RETURN 2
				CASE 10		RETURN 0
				CASE 11		RETURN 1
				CASE 12		RETURN 2
				CASE 13		RETURN 0
				CASE 14		RETURN 1
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN -1

ENDFUNC

PROC SET_SHOWROOM_VEHICLE_ATTRIBUTES(INT iVeh, VEHICLE_INDEX vehID)
	UNUSED_PARAMETER(iVeh)
	
	VEHICLE_SETUP_STRUCT_MP sData
	SWITCH GET_ENTITY_MODEL(vehID)
		CASE WINDSOR2
			sData.VehicleSetup.iColour1 = 112
			sData.VehicleSetup.iColour2 = 111
			sData.VehicleSetup.iColourExtra2 = 11
			sData.iColour5 = 98
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 12
			SET_VEHICLE_SETUP_MP(vehId, sData, FALSE, TRUE, TRUE)
		BREAK
		
		CASE STINGER
			sData.VehicleSetup.iColour1 = 9
			sData.VehicleSetup.iColour2 = 36
			sData.VehicleSetup.iColourExtra1 = 111
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			CLEAR_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_2)
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			SET_VEHICLE_SETUP_MP(vehId, sData, FALSE, TRUE, TRUE)
		BREAK
		
		CASE INFERNUS2
			sData.VehicleSetup.iColour1 = 28
			sData.VehicleSetup.iColour2 = 28
			sData.VehicleSetup.iColourExtra1 = 28
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 4
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			SET_VEHICLE_SETUP_MP(vehId, sData, FALSE, TRUE, TRUE)
		BREAK
	ENDSWITCH
	
	SET_VEHICLE_NEEDS_TO_BE_HOTWIRED(vehId, TRUE)
	SET_VEHICLE_DOORS_LOCKED(vehId, VEHICLELOCK_CANNOT_ENTER)
ENDPROC

PROC SET_DROP_SITE_VEHICLE_ATTRIBUTES(INT iVeh, VEHICLE_INDEX vehID)

	UNUSED_PARAMETER(iVeh)

	SET_VEHICLE_COLOURS(vehID,0,0)
	SET_VEHICLE_LIGHTS_ON_AT_NIGHT(vehID)

ENDPROC

PROC SET_GANG_SHOOTOUT_VEHICLE_ATTRIBUTES(INT iVeh, VEHICLE_INDEX vehID)
	UNUSED_PARAMETER(iVeh)

	IF GET_ENTITY_MODEL(vehID) != stockade
		SET_VEHICLE_COLOURS(vehID,0,0)
	ENDIF

	SWITCH GET_FMBB_SUBVARIATION()
		CASE BBS_GS_SLAUGHTERHOUSE
			SWITCH iVeh
				CASE 0
					SET_VEHICLE_COLOURS(vehId, 18, 0)
					SET_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_FRONT_LEFT)
					SET_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_FRONT_RIGHT)
					SET_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_REAR_RIGHT)
				BREAK
				CASE 7
					SET_VEHICLE_COLOURS(vehId, 40, 0)
					SET_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_FRONT_LEFT)
					SET_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_FRONT_RIGHT)
				BREAK
				CASE 8
					SET_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_FRONT_LEFT)
					SET_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_FRONT_RIGHT)
				BREAK
			ENDSWITCH
		BREAK
		CASE BBS_GS_LSIA
			SWITCH iVeh 
				CASE 0
					SET_VEHICLE_COLOURS(vehId, 39, 0)
					SET_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_FRONT_LEFT)
					SET_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_REAR_LEFT)
				BREAK
				CASE 1
					SET_VEHICLE_COLOURS(vehId, 6, 0)
					SET_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_BOOT)
				BREAK	
				CASE 2
					SET_VEHICLE_COLOURS(vehId, 6, 0)
					SET_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_BOOT)
					SET_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_REAR_LEFT)
					SET_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_REAR_RIGHT)
				BREAK
				CASE 3
					SET_VEHICLE_COLOURS(vehId, 27, 27)
				BREAK
				CASE 4
					SET_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_FRONT_LEFT)
					SET_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_REAR_LEFT)
					SET_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_REAR_RIGHT)
				BREAK
				CASE 5
					SET_VEHICLE_COLOURS(vehId, 27, 27)
				BREAK
				CASE 6
					SET_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_FRONT_RIGHT)
					SET_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_REAR_RIGHT)
				BREAK
			ENDSWITCH
		BREAK
		CASE BBS_GS_KORTZ
			SWITCH iVeh 
				CASE 2
					SET_VEHICLE_COLOURS(vehId, 2, 0)
					SET_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_FRONT_LEFT)
					SET_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_FRONT_RIGHT)
					SET_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_REAR_RIGHT)
				BREAK
				CASE 3
					SET_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_FRONT_LEFT)
					SET_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_FRONT_RIGHT)
				BREAK
				CASE 4
					SET_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_FRONT_LEFT)
					SET_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_FRONT_RIGHT)
					SET_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_REAR_RIGHT)
				BREAK
				CASE 5
					SET_VEHICLE_COLOURS(vehId, 112, 0)
					SET_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_FRONT_LEFT)
					SET_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_REAR_LEFT)
					SET_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_REAR_RIGHT)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
ENDPROC

PROC SET_HAULAGE_VEHICLE_ATTRIBUTES(INT iVeh, VEHICLE_INDEX vehID)
	UNUSED_PARAMETER(iVeh)
	
	SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(vehId, TRUE)
	SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(vehId, TRUE)
	SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehId, TRUE)
	
	SWITCH GET_ENTITY_MODEL(vehID)
		CASE POUNDER2
			SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(vehId, FALSE)
			SET_VEHICLE_DAMAGE_SCALE(vehId, 0.125)
			SET_VEHICLE_COLOURS(vehId, 112, 0)
		BREAK
	ENDSWITCH
	
	//SET_DISABLE_SUPERDUMMY(vehId, TRUE)
	SET_VEHICLE_TYRES_CAN_BURST(vehId, FALSE)
	SET_PICK_UP_BY_CARGOBOB_DISABLED(vehId, TRUE)
ENDPROC

PROC SET_ASSASSINATE_TARGETS_VEHICLE_ATTRIBUTES(INT iVeh, VEHICLE_INDEX vehID)
	UNUSED_PARAMETER(iVeh)
	UNUSED_PARAMETER(vehID)
	
	SET_VEHICLE_COLOURS(vehId, 0, 0)
	
	IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehId))
		SET_ENTITY_HEALTH(vehId, 500)
	ENDIF
ENDPROC

PROC TURN_ON_VEHICLE_RADIO(VEHICLE_INDEX vehID)

	SET_VEHICLE_ENGINE_ON(vehId, TRUE, TRUE)
	SET_VEHICLE_RADIO_ENABLED(vehId, TRUE)
	//SET_VEHICLE_RADIO_LOUD(vehId, TRUE)
	SET_VEH_FORCED_RADIO_THIS_FRAME(vehId)
	SET_VEH_RADIO_STATION(vehId, "RADIO_03_HIPHOP_NEW")
	
ENDPROC

PROC SET_CAR_MEET_VEHICLE_ATTRIBUTES(INT iVeh, VEHICLE_INDEX vehID)

	VEHICLE_SETUP_STRUCT_MP sData

	INT iModVariation = GET_CAR_MEET_VEHICLE_MOD_VARIATION(iVeh)
	BOOL bUseHighBeam

	SWITCH GET_ENTITY_MODEL(vehID)
		CASE SLAMVAN3
			SWITCH iModVariation
				CASE 0
					sData.VehicleSetup.eModel = slamvan3 // SLAMVAN3
					sData.VehicleSetup.tlPlateText = "85VVX241"
					sData.VehicleSetup.iColour1 = 12
					sData.VehicleSetup.iColour2 = 13
					sData.VehicleSetup.iColourExtra1 = 20
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 1
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 2
					sData.VehicleSetup.iNeonG = 21
					sData.VehicleSetup.iNeonB = 255
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 5
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 5
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 4
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 30
					sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
					sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 3
					sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 1
					sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 2
					sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 14
					sData.VehicleSetup.iModIndex[MOD_SEATS] = 4
					sData.VehicleSetup.iModIndex[MOD_STEERING] = 1
					sData.VehicleSetup.iModIndex[MOD_KNOB] = 1
					sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 2
					sData.VehicleSetup.iModIndex[MOD_HYDRO] = 4
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 4
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 5
					sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 1
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 9
				BREAK
				CASE 1
					sData.VehicleSetup.eModel = slamvan3 // SLAMVAN3
					sData.VehicleSetup.tlPlateText = "05SBF291"
					sData.VehicleSetup.iColour1 = 27
					sData.VehicleSetup.iColour2 = 112
					sData.VehicleSetup.iColourExtra1 = 36
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 1
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonG = 5
					sData.VehicleSetup.iNeonB = 190
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 3
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 2
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 26
					sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 3
					sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 2
					sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 12
					sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 2
					sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 14
					sData.VehicleSetup.iModIndex[MOD_SEATS] = 11
					sData.VehicleSetup.iModIndex[MOD_STEERING] = 10
					sData.VehicleSetup.iModIndex[MOD_KNOB] = 3
					sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 8
					sData.VehicleSetup.iModIndex[MOD_HYDRO] = 3
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 3
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 3
					sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 2
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 6
					sData.VehicleSetup.iModVariation[0] = 1
				BREAK
				CASE 2
					sData.VehicleSetup.eModel = slamvan3 // SLAMVAN3
					sData.VehicleSetup.tlPlateText = "75GTR201"
					sData.VehicleSetup.iColour1 = 6
					sData.VehicleSetup.iColour2 = 5
					sData.VehicleSetup.iColourExtra1 = 4
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 9
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonG = 255
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 65
					sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 2
					sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 1
					sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 12
					sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 1
					sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 12
					sData.VehicleSetup.iModIndex[MOD_SEATS] = 9
					sData.VehicleSetup.iModIndex[MOD_STEERING] = 14
					sData.VehicleSetup.iModIndex[MOD_KNOB] = 6
					sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 11
					sData.VehicleSetup.iModIndex[MOD_HYDRO] = 2
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 3
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 4
					sData.VehicleSetup.iModIndex[MOD_DOOR_L] = 1
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 8
				BREAK
			ENDSWITCH
		BREAK
		CASE FACTION3
			SWITCH iModVariation
				CASE 0
					sData.VehicleSetup.eModel = faction3 // FACTION3
					sData.VehicleSetup.tlPlateText = "81UKN529"
					sData.VehicleSetup.iColour1 = 38
					sData.VehicleSetup.iColour2 = 73
					sData.VehicleSetup.iColourExtra1 = 37
					sData.VehicleSetup.iColourExtra2 = 88
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 9
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 94
					sData.VehicleSetup.iNeonG = 255
					sData.VehicleSetup.iNeonB = 1
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 9
					sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
					sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 2
					sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 5
					sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 5
					sData.VehicleSetup.iModIndex[MOD_STEERING] = 13
					sData.VehicleSetup.iModIndex[MOD_KNOB] = 15
					sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 22
					sData.VehicleSetup.iModIndex[MOD_ICE] = 2
					sData.VehicleSetup.iModIndex[MOD_TRUNK] = 3
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 4
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 3
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 3
				BREAK
				CASE 1
					sData.VehicleSetup.eModel = faction3 // FACTION3
					sData.VehicleSetup.tlPlateText = "21MAY016"
					sData.VehicleSetup.iColour1 = 111
					sData.VehicleSetup.iColour2 = 88
					sData.VehicleSetup.iColourExtra2 = 88
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 8
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonG = 255
					sData.VehicleSetup.iNeonB = 140
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_2)
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 53
					sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
					sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 4
					sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 9
					sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 6
					sData.VehicleSetup.iModIndex[MOD_STEERING] = 12
					sData.VehicleSetup.iModIndex[MOD_KNOB] = 5
					sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 19
					sData.VehicleSetup.iModIndex[MOD_ICE] = 1
					sData.VehicleSetup.iModIndex[MOD_TRUNK] = 6
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 3
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 4
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 9
				BREAK
				CASE 2
					sData.VehicleSetup.eModel = faction3 // FACTION3
					sData.VehicleSetup.tlPlateText = "81UKN529"
					sData.VehicleSetup.iColour1 = 112
					sData.VehicleSetup.iColour2 = 53
					sData.VehicleSetup.iColourExtra2 = 125
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 8
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonG = 62
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 89
					sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
					sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 1
					sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 8
					sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 3
					sData.VehicleSetup.iModIndex[MOD_STEERING] = 9
					sData.VehicleSetup.iModIndex[MOD_KNOB] = 9
					sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 17
					sData.VehicleSetup.iModIndex[MOD_ICE] = 2
					sData.VehicleSetup.iModIndex[MOD_TRUNK] = 7
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 4
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 5
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 6
				BREAK
			ENDSWITCH
		BREAK
		CASE ELEGY
			SWITCH iModVariation
				CASE 0
					sData.VehicleSetup.eModel = elegy // ELEGY
					sData.VehicleSetup.tlPlateText = "27GLZ820"
					sData.VehicleSetup.iColour1 = 88
					sData.VehicleSetup.iColour2 = 88
					sData.VehicleSetup.iColourExtra1 = 88
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 20
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 6
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 3
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 3
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 9
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 5
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 3
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 6
					sData.VehicleSetup.iModIndex[MOD_WING_L] = 2
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 28
					sData.VehicleSetup.iModIndex[MOD_PLTVANITY] = 1
					sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 1
					sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 5
					sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 7
					sData.VehicleSetup.iModIndex[MOD_SEATS] = 14
					sData.VehicleSetup.iModIndex[MOD_STEERING] = 7
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 4
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 6
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY3] = 12
					sData.VehicleSetup.iModIndex[MOD_CHASSIS2] = 6
					sData.VehicleSetup.iModIndex[MOD_CHASSIS3] = 11
					sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 5
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 2
					sData.VehicleSetup.iModVariation[0] = 1
				BREAK
				CASE 1
					sData.VehicleSetup.eModel = elegy // ELEGY
					sData.VehicleSetup.tlPlateText = "20DWF158"
					sData.VehicleSetup.iColour1 = 137
					sData.VehicleSetup.iColour2 = 112
					sData.VehicleSetup.iColourExtra1 = 3
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 15
					sData.VehicleSetup.iNeonG = 3
					sData.VehicleSetup.iNeonB = 255
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 17
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 5
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 7
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 5
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
					sData.VehicleSetup.iModIndex[MOD_WING_L] = 3
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 7
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 37
					sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 1
					sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 5
					sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 7
					sData.VehicleSetup.iModIndex[MOD_SEATS] = 14
					sData.VehicleSetup.iModIndex[MOD_STEERING] = 7
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 2
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 8
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY3] = 9
					sData.VehicleSetup.iModIndex[MOD_CHASSIS2] = 5
					sData.VehicleSetup.iModIndex[MOD_CHASSIS3] = 10
					sData.VehicleSetup.iModIndex[MOD_CHASSIS4] = 1
					sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 2
					sData.VehicleSetup.iModIndex[MOD_DOOR_L] = 1
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 7
					sData.VehicleSetup.iModVariation[0] = 1
				BREAK
				CASE 2
					sData.VehicleSetup.eModel = elegy // ELEGY
					sData.VehicleSetup.tlPlateText = "25EAF559"
					sData.VehicleSetup.iColour1 = 63
					sData.VehicleSetup.iColour2 = 112
					sData.VehicleSetup.iColourExtra1 = 87
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonG = 1
					sData.VehicleSetup.iNeonB = 1
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 7
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 5
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 3
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 4
					sData.VehicleSetup.iModIndex[MOD_WING_L] = 4
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
					sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 31
					sData.VehicleSetup.iModIndex[MOD_PLTVANITY] = 3
					sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 1
					sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 1
					sData.VehicleSetup.iModIndex[MOD_SEATS] = 13
					sData.VehicleSetup.iModIndex[MOD_STEERING] = 16
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 3
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 2
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY3] = 6
					sData.VehicleSetup.iModIndex[MOD_CHASSIS2] = 3
					sData.VehicleSetup.iModIndex[MOD_CHASSIS3] = 6
					sData.VehicleSetup.iModIndex[MOD_CHASSIS4] = 1
					sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 5
					sData.VehicleSetup.iModIndex[MOD_DOOR_L] = 2
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 8
					sData.VehicleSetup.iModVariation[0] = 1
				BREAK
			ENDSWITCH
		BREAK
		CASE BANSHEE2
			SWITCH iModVariation
				CASE 0
					sData.VehicleSetup.eModel = banshee2 // BANSHEE2
					sData.VehicleSetup.tlPlateText = "40XHW363"
					sData.VehicleSetup.iColour1 = 88
					sData.VehicleSetup.iColour2 = 88
					sData.VehicleSetup.iColourExtra1 = 88
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 7
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonG = 62
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 6
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 4
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 5
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 6
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 39
					sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 21
					sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 3
					sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 4
					sData.VehicleSetup.iModIndex[MOD_SEATS] = 13
					sData.VehicleSetup.iModIndex[MOD_STEERING] = 16
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 4
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY3] = 10
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 8
				BREAK
				CASE 1
					sData.VehicleSetup.eModel = banshee2 // BANSHEE2
					sData.VehicleSetup.tlPlateText = "62UYW893"
					sData.VehicleSetup.iColour1 = 33
					sData.VehicleSetup.iColour2 = 88
					sData.VehicleSetup.iColourExtra1 = 137
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 8
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonG = 62
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 3
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 4
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 3
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 3
					sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 93
					sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 16
					sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 2
					sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 3
					sData.VehicleSetup.iModIndex[MOD_SEATS] = 13
					sData.VehicleSetup.iModIndex[MOD_STEERING] = 13
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 2
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY3] = 11
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 5
				BREAK
				CASE 2
					sData.VehicleSetup.eModel = banshee2 // BANSHEE2
					sData.VehicleSetup.tlPlateText = "81TTR412"
					sData.VehicleSetup.iColour1 = 158
					sData.VehicleSetup.iColour2 = 159
					sData.VehicleSetup.iColourExtra1 = 160
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 222
					sData.VehicleSetup.iNeonG = 222
					sData.VehicleSetup.iNeonB = 255
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 4
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 4
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 9
					sData.VehicleSetup.iModIndex[MOD_WING_L] = 2
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 47
					sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 12
					sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 1
					sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 1
					sData.VehicleSetup.iModIndex[MOD_SEATS] = 11
					sData.VehicleSetup.iModIndex[MOD_STEERING] = 7
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 2
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY3] = 6
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 3
				BREAK
			ENDSWITCH
		BREAK
		CASE SULTANRS
			bUseHighBeam = TRUE
		
			SWITCH iModVariation
				CASE 0
					sData.VehicleSetup.eModel = sultanrs // SULTANRS
					sData.VehicleSetup.tlPlateText = "41CMF473"
					sData.VehicleSetup.iColour1 = 62
					sData.VehicleSetup.iColour2 = 62
					sData.VehicleSetup.iColourExtra1 = 68
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonG = 255
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 10
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 14
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 8
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 5
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 5
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 4
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 9
					sData.VehicleSetup.iModIndex[MOD_WING_L] = 6
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 50
					sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 1
					sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 3
					sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 2
					sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 3
					sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 3
					sData.VehicleSetup.iModIndex[MOD_SEATS] = 14
					sData.VehicleSetup.iModIndex[MOD_STEERING] = 13
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 4
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 6
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY3] = 12
					sData.VehicleSetup.iModIndex[MOD_CHASSIS2] = 4
					sData.VehicleSetup.iModIndex[MOD_CHASSIS3] = 2
					sData.VehicleSetup.iModIndex[MOD_CHASSIS4] = 1
					sData.VehicleSetup.iModIndex[MOD_DOOR_L] = 1
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 7
				BREAK
				CASE 1
					sData.VehicleSetup.eModel = sultanrs // SULTANRS
					sData.VehicleSetup.tlPlateText = "31WGC903"
					sData.VehicleSetup.iColour1 = 71
					sData.VehicleSetup.iColour2 = 137
					sData.VehicleSetup.iColourExtra1 = 145
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonG = 50
					sData.VehicleSetup.iNeonB = 100
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 15
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 15
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 7
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 3
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 3
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 7
					sData.VehicleSetup.iModIndex[MOD_WING_L] = 4
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 43
					sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 10
					sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 3
					sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 5
					sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 5
					sData.VehicleSetup.iModIndex[MOD_SEATS] = 12
					sData.VehicleSetup.iModIndex[MOD_STEERING] = 15
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 2
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 8
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY3] = 9
					sData.VehicleSetup.iModIndex[MOD_CHASSIS2] = 3
					sData.VehicleSetup.iModIndex[MOD_CHASSIS4] = 2
					sData.VehicleSetup.iModIndex[MOD_DOOR_L] = 3
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 8
				BREAK
				CASE 2
					sData.VehicleSetup.eModel = sultanrs // SULTANRS
					sData.VehicleSetup.tlPlateText = "01CEV285"
					sData.VehicleSetup.iColour1 = 111
					sData.VehicleSetup.iColour2 = 31
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonG = 1
					sData.VehicleSetup.iNeonB = 1
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 16
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 12
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 5
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 4
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 4
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 10
					sData.VehicleSetup.iModIndex[MOD_WING_L] = 3
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 16
					sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 1
					sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 15
					sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 5
					sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 6
					sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 6
					sData.VehicleSetup.iModIndex[MOD_SEATS] = 13
					sData.VehicleSetup.iModIndex[MOD_STEERING] = 12
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 3
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 7
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY3] = 10
					sData.VehicleSetup.iModIndex[MOD_CHASSIS2] = 5
					sData.VehicleSetup.iModIndex[MOD_CHASSIS3] = 1
					sData.VehicleSetup.iModIndex[MOD_DOOR_L] = 2
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 6
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH

	IF iModVariation != -1
		SET_VEHICLE_SETUP_MP(vehId, sData, FALSE, TRUE, TRUE)
	ENDIF

	SWITCH GET_FMBB_SUBVARIATION()
		CASE BBS_CM_SAN_ANDREAS
			SWITCH iVeh	
				CASE 2
				CASE 3
				CASE 4
				CASE 7
				CASE 9
				CASE 11
				CASE 14
					SET_VEHICLE_DOOR_CONTROL(vehId, SC_DOOR_BONNET, DT_DOOR_NO_RESET, 1.0)
				BREAK
				CASE 5
					SET_VEHICLE_DOOR_OPEN(vehID,SC_DOOR_BOOT)
					SET_VEHICLE_DOOR_CONTROL(vehId, SC_DOOR_BOOT, DT_DOOR_NO_RESET, 1.0)
				BREAK
			ENDSWITCH
		BREAK
		CASE BBS_CM_WEST_VINEWOOD
			SWITCH iVeh
				CASE 0
				CASE 2
				CASE 7
				CASE 8
				CASE 9
				CASE 10
				CASE 13
					SET_VEHICLE_DOOR_CONTROL(vehId, SC_DOOR_BONNET, DT_DOOR_NO_RESET, 1.0)
				BREAK
				CASE 4
					SET_VEHICLE_DOOR_OPEN(vehID,SC_DOOR_BOOT)
					SET_VEHICLE_DOOR_CONTROL(vehId, SC_DOOR_BOOT, DT_DOOR_NO_RESET, 1.0)
				BREAK
			ENDSWITCH
		BREAK
		CASE BBS_CM_PACIFIC_BLUFFS
			SWITCH iVeh
				CASE 2
				CASE 3
				CASE 6
				CASE 7
				CASE 11
				CASE 13
					SET_VEHICLE_DOOR_CONTROL(vehId, SC_DOOR_BONNET, DT_DOOR_NO_RESET, 1.0)
				BREAK
				CASE 4
					SET_VEHICLE_DOOR_OPEN(vehID,SC_DOOR_BOOT)
					SET_VEHICLE_DOOR_CONTROL(vehId, SC_DOOR_BOOT, DT_DOOR_NO_RESET, 1.0)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
		
	SET_VEHICLE_LIGHTS_ON_AT_NIGHT(vehID, FALSE, bUseHighBeam)
	SET_VEHICLE_ENGINE_ON(vehId,TRUE,TRUE)

ENDPROC

PROC SET_NON_MISSION_ENTITY_VEHICLE_ATTRIBUTES_FOR_SPAWN(INT iVehicle)
	
	VEHICLE_INDEX vehId = NET_TO_VEH(serverBD.sVehicle[iVehicle].netId)
	
	IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_INSIDE_MISSION_INTERIOR)
		NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_ENT(serverBD.sVehicle[iVehicle].netId), TRUE)
	ENDIF
	
	// Reduce turbulence for mission planes
	IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
	OR IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
		IF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehId))
			SET_PLANE_TURBULENCE_MULTIPLIER(vehId, 0.0)
			PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_NON_MISSION_ENTITY_VEHICLE_ATTRIBUTES_FOR_SPAWN - SET_PLANE_TURBULENCE_MULTIPLIER to 0 for vehicle ",iVehicle)
		ENDIF
	ENDIF
	
	IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh", DECOR_TYPE_INT)
    	DECOR_SET_INT(vehId, "Not_Allow_As_Saved_Veh", MP_NOT_ALLOWED_AS_SAVED_VEHICLE_DEC_SIMEON)
		PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] SET_NON_MISSION_ENTITY_VEHICLE_ATTRIBUTES_FOR_SPAWN - Set decorator 'Not_Allow_As_Saved_Veh' on vehicle #", iVehicle)
	ENDIF
	
	IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
		SET_SUPPORT_VEHICLE_SPAWN_ATTRIBUTES(iVehicle, vehId)
		EXIT
	ENDIF

	IF SHOULD_SET_UP_CARRIER_VEHICLE_SPAWN_ATTRIBUTES(iVehicle)
		SET_CARRIER_VEHICLE_SPAWN_ATTRIBUTES(iVehicle, vehId)
		EXIT
	ENDIF
	
	SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(vehId, TRUE)
	SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(vehId, TRUE)
	SET_VEHICLE_DOORS_LOCKED_FOR_NON_SCRIPT_PLAYERS(vehID, TRUE)
	
	// Add in mission specific vehicle attributes here
	SWITCH GET_FMBB_VARIATION()
		CASE BBV_UNDER_GUARD		SET_UNDER_GUARD_VEHICLE_ATTRIBUTES(iVehicle, vehID)		BREAK
		CASE BBV_CAR_MEET			SET_CAR_MEET_VEHICLE_ATTRIBUTES(iVehicle, vehID)		BREAK
		CASE BBV_SHOWROOM			SET_SHOWROOM_VEHICLE_ATTRIBUTES(iVehicle, vehID)		BREAK
		CASE BBV_DROP_SITE			SET_DROP_SITE_VEHICLE_ATTRIBUTES(iVehicle, vehID)		BREAK
		CASE BBV_GANG_SHOOTOUT		SET_GANG_SHOOTOUT_VEHICLE_ATTRIBUTES(iVehicle, vehID)	BREAK
		CASE BBV_ASSASSINATE_TARGETS	SET_ASSASSINATE_TARGETS_VEHICLE_ATTRIBUTES(iVehicle, vehID)		BREAK
		CASE BBV_HAULAGE			SET_HAULAGE_VEHICLE_ATTRIBUTES(iVehicle, vehID)			BREAK
	ENDSWITCH
ENDPROC

PROC INITIALISE_AMBUSH_VEHICLE_BITSETS()
	IF GET_AMBUSH_VEHICLE_ARRAY_INDEX(eVEHICLEBITSET_I_AM_AMBUSH_VEHICLE_0) != (-1)
		SET_VEHICLE_BIT(GET_AMBUSH_VEHICLE_ARRAY_INDEX(eVEHICLEBITSET_I_AM_AMBUSH_VEHICLE_0), eVEHICLEBITSET_I_AM_AMBUSH_VEHICLE_0)
	ENDIF
	
	IF GET_AMBUSH_VEHICLE_ARRAY_INDEX(eVEHICLEBITSET_I_AM_AMBUSH_VEHICLE_1) != (-1)
		SET_VEHICLE_BIT(GET_AMBUSH_VEHICLE_ARRAY_INDEX(eVEHICLEBITSET_I_AM_AMBUSH_VEHICLE_1), eVEHICLEBITSET_I_AM_AMBUSH_VEHICLE_1)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_SET_CARRIER_VEHICLE_BIT_FROM_START()
	RETURN TRUE
ENDFUNC

PROC INITIALISE_VEHICLE_BITSETS_FOR_MODE_START()
	INT iSupportCount = GB_GET_FMBB_NUM_SUPPORT_VEHICLES_REQUIRED(GET_FMBB_VARIATION(), serverBD.iOrganisationSizeOnLaunch)
	
	IF DOES_FMBB_VARIATION_HAVE_AMBUSH(GET_FMBB_VARIATION())
		INITIALISE_AMBUSH_VEHICLE_BITSETS()
	ENDIF
	
	// set carrier bits
	INT iMissionEntity
	REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
		IF DOES_MISSION_ENTITY_HAVE_CARRIER_VEHICLE(iMissionEntity)
		AND SHOULD_SET_CARRIER_VEHICLE_BIT_FROM_START()
			SET_VEHICLE_BIT(serverBD.iCarrierVehicle[iMissionEntity], eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
		ENDIF
	ENDREPEAT
	
	SWITCH GET_FMBB_VARIATION()
		CASE BBV_HAULAGE
			SET_VEHICLE_BIT(0, eVEHICLEBITSET_I_AM_HAULAGE_POUNDER)
			SET_VEHICLE_BIT(0, eVEHICLEBITSET_WARP_VEHICLE_ON_STUCK)
			SET_VEHICLE_BIT(1, eVEHICLEBITSET_NEEDS_ATTACHED_TO_TRAILER)
		BREAK
		CASE BBV_MAX
			IF iSupportCount >= 1
				SET_VEHICLE_BIT(1, eVEHICLEBITSET_I_AM_SUPPORT_VEHICLE_0)
			ENDIF
		BREAK
		CASE BBV_CAR_MEET
			SWITCH GET_FMBB_SUBVARIATION()
				CASE BBS_CM_SAN_ANDREAS
					SET_VEHICLE_BIT(5, eVEHICLEBITSET_I_AM_RADIO_VEHICLE)
				BREAK
				CASE BBS_CM_WEST_VINEWOOD
					SET_VEHICLE_BIT(4, eVEHICLEBITSET_I_AM_RADIO_VEHICLE)
				BREAK
				CASE BBS_CM_PACIFIC_BLUFFS
					SET_VEHICLE_BIT(4, eVEHICLEBITSET_I_AM_RADIO_VEHICLE)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH

ENDPROC

PROC RESET_SERVER_NON_MISSION_ENTITY_VEHICLE_SPAWN_DATA()
	serverBd.iCurrentNonMissionEntityVehicleSearchingForSpawnCoords = (-1)
	serverBd.vSpawnNonMissionEntityVehicleCoords = << 0.0, 0.0, 0.0 >>
	serverBd.fSpawnNonMissionEntityVehicleHeading = 0.0
	serverBd.iGetSpawnNonMissionEntityVehicleCoordsStage = 0
	serverBd.iCircCount = 0
	serverBD.vVehicleSpawnCoord = << 0.0, 0.0, 0.0 >>
	PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - RESET_SERVER_NON_MISSION_ENTITY_VEHICLE_SPAWN_DATA - Vehicle data cleared.")
ENDPROC

PROC CLEANUP_VEHICLES()
	
	INT i
	
	REPEAT NUM_VARIATION_VEHICLES i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[i].netId)
			//IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[i].netId)
				CLEANUP_NET_ID(serverBD.sVehicle[i].netId)
				PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - set non-MissionEntity vehicle ", i, " as no longer needed")
			//ENDIF
		ENDIF
	ENDREPEAT	
	
ENDPROC

FUNC BOOL SHOULD_CREATE_VEHICLE_FOR_VARIATION(INT iVehicle)
	IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
		RETURN FALSE
	ENDIF
	
	SWITCH GET_FMBB_VARIATION()
		CASE BBV_ASSASSINATE_TARGETS
			SWITCH GET_FMBB_SUBVARIATION()
				CASE BBS_AT_LOCATION_1
					SWITCH iVehicle
						CASE 1
						CASE 3
							RETURN FALSE
					ENDSWITCH
				BREAK
				
				CASE BBS_AT_LOCATION_2
					SWITCH iVehicle
						CASE 2
						CASE 3
							RETURN FALSE
					ENDSWITCH
				BREAK
				
				CASE BBS_AT_LOCATION_3
					SWITCH iVehicle
						CASE 1
						CASE 3
							RETURN FALSE
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_CREATE_CARRIER_VEHICLES()
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_VEHICLES_BE_CREATED_ON_MISSION_START()

	SWITCH GET_FMBB_VARIATION()
		CASE BBV_MAX
			RETURN FALSE
	ENDSWITCH

	RETURN TRUE

ENDFUNC

PROC MAINTAIN_ACTIVATING_NON_MISSION_ENTITY_VEHICLES()
	
	INT iVehicle
	
	// Ambush vehicle activation
	// Shouldn't need to touch this, just add your mission to the wrappers
	IF DOES_FMBB_VARIATION_HAVE_AMBUSH(GET_FMBB_VARIATION())
		IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_ACTIVATED_VEHICLES_FOR_AMBUSH)
		AND SHOULD_AMBUSH_ACTIVATE()
			IF GET_AMBUSH_VEHICLE_ARRAY_INDEX(eVEHICLEBITSET_I_AM_AMBUSH_VEHICLE_0) != (-1)
				SET_VEHICLE_STATE(GET_AMBUSH_VEHICLE_ARRAY_INDEX(eVEHICLEBITSET_I_AM_AMBUSH_VEHICLE_0), eVEHICLESTATE_CREATE)
			ENDIF
			IF GET_AMBUSH_VEHICLE_ARRAY_INDEX(eVEHICLEBITSET_I_AM_AMBUSH_VEHICLE_1) != (-1)
				SET_VEHICLE_STATE(GET_AMBUSH_VEHICLE_ARRAY_INDEX(eVEHICLEBITSET_I_AM_AMBUSH_VEHICLE_1), eVEHICLESTATE_CREATE)
			ENDIF
			SET_SERVER_BIT(eSERVERBITSET_ACTIVATED_VEHICLES_FOR_AMBUSH)
		ENDIF
	ENDIF
	
	// Bunker Support Vehicles
	IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_ACTIVATED_SUPPORT_VEHICLES)
		IF GB_GET_FMBB_NUM_SUPPORT_VEHICLES_REQUIRED(GET_FMBB_VARIATION(), serverBD.iOrganisationSizeOnLaunch) > 0
			REPEAT NUM_VARIATION_VEHICLES iVehicle
				IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
					SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_CREATE)
				ENDIF
			ENDREPEAT
			SET_SERVER_BIT(eSERVERBITSET_ACTIVATED_SUPPORT_VEHICLES)
		ENDIF
	ENDIF
	
	// Carrier vehicles
	IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_ACTIVATED_CARRIER_VEHICLES)
		IF SHOULD_CREATE_CARRIER_VEHICLES()
			IF DOES_MISSION_ENTITY_HAVE_CARRIER_VEHICLE(0)	// Carrier vehicles are used on this mission
				REPEAT NUM_VARIATION_VEHICLES iVehicle
					IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
						SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_CREATE)
					ENDIF
				ENDREPEAT
				SET_SERVER_BIT(eSERVERBITSET_ACTIVATED_CARRIER_VEHICLES)
			ENDIF
		ENDIF
	ENDIF
	
	IF SHOULD_VEHICLES_BE_CREATED_ON_MISSION_START()
		IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_ACTIVATED_VEHICLES_FOR_MISSION_START)
		AND (HAVE_ALL_MISSION_ENTITIES_BEEN_CREATED()
		OR ARE_MISSION_ENTITIES_MADE_DURING_MISSION())

			REPEAT GB_GET_FMBB_NUM_VEH_REQUIRED(GET_FMBB_VARIATION(), GET_FMBB_SUBVARIATION(), TRUE, TRUE, TRUE) iVehicle
				IF SHOULD_CREATE_VEHICLE_FOR_VARIATION(iVehicle)
					SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_CREATE)
				ENDIF
			ENDREPEAT
			SET_SERVER_BIT(eSERVERBITSET_ACTIVATED_VEHICLES_FOR_MISSION_START)
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_SPAWN_VARIATION_VEHICLES()
	RETURN TRUE
ENDFUNC

PROC RESET_VEHICLE_DATA_FOR_RESPAWN(INT iVehicle)

	IF DOES_FMBB_VARIATION_HAVE_AMBUSH(GET_FMBB_VARIATION())
		serverBd.iCurrentNonMissionEntityVehicleSearchingForSpawnCoords = (-1)
		serverBd.vSpawnNonMissionEntityVehicleCoords = << 0.0, 0.0, 0.0 >>
		serverBd.fSpawnNonMissionEntityVehicleHeading = 0.0
		ServerBd.iAmbushGetNonMissionEntityCoordsAttempts = 0
		serverBd.iGetSpawnNonMissionEntityVehicleCoordsStage = 0
		RESET_MISSION_ENTITY_TARGET_FOR_VEHICLE(iVehicle)
		
		PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - RESET_VEHICLE_DATA_FOR_RESPAWN - Vehicle = ", iVehicle)
	ENDIF
	
ENDPROC

FUNC BOOL DOES_VARIATION_HAVE_VARIATION_VEHICLES()
	IF IS_THIS_VARIATION_A_SELL_MISSION()
		RETURN TRUE
	ENDIF
	
	IF GB_GET_FMBB_NUM_SUPPORT_VEHICLES_REQUIRED(GET_FMBB_VARIATION(), serverBD.iOrganisationSizeOnLaunch) > 0
		RETURN TRUE
	ENDIF
	
	IF DOES_FMBB_VARIATION_HAVE_AMBUSH(GET_FMBB_VARIATION())
		RETURN TRUE
	ENDIF
	
	IF GET_FMBB_CARRIER_VEHICLE_INDEX(0, GET_FMBB_SUBVARIATION()) != -1
		RETURN TRUE
	ENDIF
	
	// Add your mission here if it has vehicles
	SWITCH GET_FMBB_VARIATION()
		CASE BBV_UNDER_GUARD
		CASE BBV_BREAK_IN
		CASE BBV_CAR_MEET
		CASE BBV_SHOWROOM
		CASE BBV_CARPARK_SEARCH
		CASE BBV_DROP_SITE
		CASE BBV_GANG_SHOOTOUT
		CASE BBV_JOYRIDERS
		CASE BBV_ASSASSINATE_TARGETS
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_USE_GROUND_Z_FOR_AIR_SPAWNING()
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_CONSIDER_HIGHWAYS_FOR_VARIATION(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_ALLOW_OFF_ROAD_NODES(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_IGNORE_CUSTOM_NODES()
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_SPREAD_VEHICLES_APART()
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_DROP_OFF_NEED_LARGER_SPAWN_SEARCH_AREA()
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_DROP_OFF_NEED_CUSTOM_SPAWN_SEARCH_AREA()
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_DROP_OFF_CUSTOM_MAX_SPAWN_SEARCH_AREA_SIZE()
	RETURN 0.0
ENDFUNC

FUNC FLOAT GET_DROP_OFF_CUSTOM_MIN_SPAWN_SEARCH_AREA_SIZE()
	RETURN 0.0
ENDFUNC

FUNC VECTOR GET_FAVOURED_FACING(VECTOR vSpawnCoords)
	UNUSED_PARAMETER(vSpawnCoords)
	RETURN vSpawnCoords
ENDFUNC

FUNC FLOAT GET_VEHICLE_SPAWN_LOCATION_MAX_DISTANCE(BOOL bLargerSize = FALSE)
	UNUSED_PARAMETER(bLargerSize)
	RETURN 150.0
ENDFUNC

FUNC FLOAT GET_VEHICLE_SPAWN_LOCATION_MIN_DISTANCE(BOOL bLargerSize = FALSE)
	UNUSED_PARAMETER(bLargerSize)
	RETURN 0.0
ENDFUNC

FUNC VECTOR GET_RANDOM_SPAWN_COORD_FOR_VEHICLE(INT iVehicle, VECTOR vCentralCoord)
	INT iXCoord, iYCoord
	VECTOR vNewVector
	INT iMaxDist = 600
	INT iMinDist = 250
	INT iMinDistBetween = 200
	INT iLoop
	INT iNumVehs = GB_GET_FMBB_NUM_VEH_REQUIRED(GET_FMBB_VARIATION(), GET_FMBB_SUBVARIATION(), TRUE, TRUE, TRUE)
	FLOAT fDistBetween
	VECTOR vTempCoord
	
	// Get a random number between -iMaxDist and iMaxDist to get the x coord
	iXCoord = GET_RANDOM_INT_IN_RANGE((iMaxDist - (iMaxDist*2)), iMaxDist)
	
	// Get a random number between the rest of the value between xCoord and iMaxDist for the y coord
	iYCoord = GET_RANDOM_INT_IN_RANGE((iMaxDist - iXCoord), iMaxDist)
	
	// Should this go positive or negative on y from the x coord?
	iYCoord = PICK_INT(GET_RANDOM_BOOL(), iYCoord, (iYCoord - (iYCoord*2)))
	
	// Now get the new vector based on the grabbed coord offsets from the central coord
	vNewVector = <<vCentralCoord.x+iXCoord, vCentralCoord.y+iYCoord, 0.0>>
	vNewVector.z = GET_APPROX_HEIGHT_FOR_POINT(vNewVector.x, vNewVector.y)
	
	fDistBetween = GET_DISTANCE_BETWEEN_COORDS(vNewVector, vCentralCoord, FALSE)
	IF fDistBetween < iMinDist
		PRINTLN("GET_RANDOM_SPAWN_COORD_FOR_VEHICLE - vNewVector (", vNewVector, ") is too close to the centre of the search area (",vCentralCoord,"), fDistBetween = ", fDistBetween)
		vNewVector = <<0.0, 0.0, 0.0>>
	ENDIF
	
	// Dont bother looping through other vehicles, the coord we tried is too close to the centre of the search area
	IF NOT IS_VECTOR_ZERO(vNewVector)
	
		// Check to make sure its not near any of the other vehicles that have been created so far
		REPEAT iNumVehs iLoop
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iLoop].netId)
				vTempCoord = GET_ENTITY_COORDS(NET_TO_VEH(serverBD.sVehicle[iLoop].netId), FALSE)
				fDistBetween = GET_DISTANCE_BETWEEN_COORDS(vTempCoord, vNewVector, FALSE)
				PRINTLN("GET_RANDOM_SPAWN_COORD_FOR_VEHICLE - fDistBetween vehicle ", iVehicle, " and ", iLoop, " = ", fDistBetween)
				IF fDistBetween < iMinDistBetween
					PRINTLN("GET_RANDOM_SPAWN_COORD_FOR_VEHICLE - new vector at ", vNewVector," is too close to vehicle ", iLoop, " located at ", vTempCoord)
					vNewVector = <<0.0, 0.0, 0.0>>
					BREAKLOOP
				ENDIF
			ELSE
				IF iLoop > 0
				AND iLoop < iVehicle
					PRINTLN("GET_RANDOM_SPAWN_COORD_FOR_VEHICLE - vehicle ", iLoop, " doesnt exist, cannot compare them")
					vNewVector = <<0.0, 0.0, 0.0>>
					BREAKLOOP
				ENDIF
			ENDIF
		ENDREPEAT
	
	ENDIF
	
	IF IS_VECTOR_ZERO(vNewVector)
		PRINTLN("GET_RANDOM_SPAWN_COORD_FOR_VEHICLE - returning zero vector")
	ENDIF
	
	RETURN vNewVector
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_BE_INVINCIBLE_AT_START(INT iVeh)
	
	SWITCH GET_FMBB_VARIATION()
		CASE BBV_JOYRIDERS			RETURN FALSE
	ENDSWITCH
	
	IF SHOULD_SET_UP_CARRIER_VEHICLE_SPAWN_ATTRIBUTES(iVeh)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_NET_NON_MISSION_ENTITY_VEHICLE(MODEL_NAMES vehModel, INT iVehicle)

	IF IS_VEHICLE_A_CARRIER_VEHICLE(iVehicle)
	AND GET_FMBB_BUSINESS_TYPE() = BBT_EXPORT_GARAGE
		serverBD.sVehicle[iVehicle].netId = IE_NET_CREATE_VEHICLE(	serverBD.IEVehicle, serverBd.vSpawnNonMissionEntityVehicleCoords, serverBd.fSpawnNonMissionEntityVehicleHeading)
		RETURN NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
	ELSE
		RETURN CREATE_NET_VEHICLE(serverBD.sVehicle[iVehicle].netId, vehModel, serverBd.vSpawnNonMissionEntityVehicleCoords, serverBd.fSpawnNonMissionEntityVehicleHeading)	
	ENDIF

ENDFUNC

FUNC VECTOR GET_CREATE_FROM_OFFSET_VEHICLE_COORD(INT iVehicle)
	SWITCH GET_FMBB_VARIATION()
		CASE BBV_HAULAGE		
			SWITCH iVehicle
				CASE 1		RETURN GET_FMBB_VEHICLE_SPAWN_COORDS(GET_FMBB_VARIATION(), GET_FMBB_SUBVARIATION(), 0, DEFAULT, serverBD.iSelectedSpawn0)
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC FLOAT GET_CREATE_FROM_OFFSET_VEHICLE_HEADING(INT iVehicle)
	SWITCH GET_FMBB_VARIATION()
		CASE BBV_HAULAGE
			SWITCH iVehicle	
				CASE 1			RETURN GET_FMBB_VEHICLE_SPAWN_HEADING(GET_FMBB_VARIATION(), GET_FMBB_SUBVARIATION(), 0, DEFAULT, serverBD.iSelectedSpawn0)
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN 0.0
ENDFUNC

FUNC VECTOR GET_CREATE_FROM_OFFSET_OFFSET(INT iVehicle)
	SWITCH GET_FMBB_VARIATION()
		CASE BBV_HAULAGE
			SWITCH iVehicle	
				CASE 1			RETURN <<0.0, 0.2, 4.0>>
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC BOOL CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION(INT iVehicle, MODEL_NAMES vehModel, BOOL bShouldFindSpawnLocation, BOOL bShouldSpawnInAir, BOOL bUseIeCreation, BOOL bDoOkCheck, BOOL bCreateFromOffset)
	
	UNUSED_PARAMETER(bUseIeCreation)
	
	IF IS_VEHICLE_AN_AMBUSH_VEHICLE(iVehicle)
		CREATE_AMBUSH_VEHICLE(iVehicle)
	ELSE
		IF serverBD.iCurrentPedSearchingForSpawnCoords = -1
			IF serverBd.iCurrentNonMissionEntityVehicleSearchingForSpawnCoords = (-1)
			OR serverBd.iCurrentNonMissionEntityVehicleSearchingForSpawnCoords = iVehicle
			
				IF serverBd.iCurrentNonMissionEntityVehicleSearchingForSpawnCoords != iVehicle
					serverBd.iCurrentNonMissionEntityVehicleSearchingForSpawnCoords = iVehicle
					PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION - Vehicle #", iVehicle, " now searching for spawn coordinates.")
				ENDIF
				
				// Support Vehicle Coordinate Search
				IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
					VECTOR vCoords, vSpawnHeading
					FLOAT fHeading
					SWITCH serverBd.iGetSpawnNonMissionEntityVehicleCoordsStage
						CASE 0
							CLEAR_CUSTOM_VEHICLE_NODES()
							
							INT iSupportVehicleSpawn
							WHILE NOT IS_VECTOR_ZERO(GET_FMBB_SUPPORT_VEHICLE_SPAWN_COORDS(GET_PLAYERS_OWNED_DEFUNCT_BASE(GB_GET_LOCAL_PLAYER_MISSION_HOST()), iSupportVehicleSpawn, vehModel, GET_FMBB_SUBVARIATION()))
								vCoords = GET_FMBB_SUPPORT_VEHICLE_SPAWN_COORDS(GET_PLAYERS_OWNED_DEFUNCT_BASE(GB_GET_LOCAL_PLAYER_MISSION_HOST()), iSupportVehicleSpawn, vehModel, GET_FMBB_SUBVARIATION())
								fHeading = GET_FMBB_SUPPORT_VEHICLE_SPAWN_HEADING(GET_PLAYERS_OWNED_DEFUNCT_BASE(GB_GET_LOCAL_PLAYER_MISSION_HOST()), iSupportVehicleSpawn, vehModel, GET_FMBB_SUBVARIATION())
								IF NOT IS_VECTOR_ZERO(vCoords)
									ADD_CUSTOM_VEHICLE_NODE(vCoords, fHeading)		
									PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] [SUPPORT] - CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION - Adding custom vehicle node ", iSupportVehicleSpawn, " at ", vCoords, " for vehicle #", iVehicle, " (Support #", GET_SUPPORT_VEHICLE_INDEX(iVehicle), ")")
								ENDIF
								iSupportVehicleSpawn++
							ENDWHILE	
							serverBd.iGetSpawnNonMissionEntityVehicleCoordsStage++
							PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] [SUPPORT] - CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION - Added custom vehicle nodes. Moving on to find spawn coord.")
						BREAK
						
						CASE 1
							VEHICLE_SPAWN_LOCATION_PARAMS Params
							Params.bEnforceMinDistForCustomNodes 		= FALSE
							Params.bStartOfMissionPVSpawn 				= TRUE
							Params.bIgnoreCustomNodesForArea 			= TRUE
							Params.bAvoidSpawningInExclusionZones  		= FALSE
							Params.bIgnoreCustomNodesForMissionLaunch 	= TRUE
							//Params.bCheckEntityArea = TRUE
							
							vCoords = GET_FMBB_SUPPORT_VEHICLE_SPAWN_COORDS(GET_PLAYERS_OWNED_DEFUNCT_BASE(GB_GET_LOCAL_PLAYER_MISSION_HOST()), GET_SUPPORT_VEHICLE_INDEX(iVehicle), vehModel, GET_FMBB_SUBVARIATION())
							vSpawnHeading = <<0.0, 0.0, 0.0>>
							
							IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(vCoords, vSpawnHeading, vehModel, FALSE, serverBd.vSpawnNonMissionEntityVehicleCoords, serverBd.fSpawnNonMissionEntityVehicleHeading, Params)
								PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] [SUPPORT] - CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION - Vehicle #", iVehicle, " (Support #", GET_SUPPORT_VEHICLE_INDEX(iVehicle), ") has got safe spawn at ", serverBd.vSpawnNonMissionEntityVehicleCoords)
								serverBd.iGetSpawnNonMissionEntityVehicleCoordsStage++
							ELSE
								PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] [SUPPORT] - CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION - Failed to find spawn for vehicle #", iVehicle, " (Support #", GET_SUPPORT_VEHICLE_INDEX(iVehicle), ")")
							ENDIF
						BREAK
						
						CASE 2
							
						BREAK
					ENDSWITCH
				ENDIF
				
				IF bDoOkCheck
					VECTOR vCoords = GET_FMBB_VEHICLE_SPAWN_COORDS(GET_FMBB_VARIATION(), GET_FMBB_SUBVARIATION(), iVehicle, DEFAULT, serverBD.iSelectedSpawn0,GET_RESPAWN_LOCATION())
					FLOAT fRadius = GET_RADIUS_FROM_CONTRABAND_ENTITY(vehModel)
					IF NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION(vCoords,fRadius,fRadius,fRadius,0.0,FALSE,FALSE,FALSE,0.0,FALSE,DEFAULT,FALSE,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
						bShouldFindSpawnLocation = TRUE
						
						#IF IS_DEBUG_BUILD
						IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
							PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] [SUPPORT] - CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION - Vehicle #", iVehicle, " is support vehicle #", GET_SUPPORT_VEHICLE_INDEX(iVehicle), ". bShouldFindSpawnLocation set to true as location not safe.")
						ELSE
							PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION - Vehicle #", iVehicle, " - bShouldFindSpawnLocation set to true as location not safe.")
						ENDIF
						#ENDIF
					ENDIF
				ENDIF
				
				IF IS_VECTOR_ZERO(serverBd.vSpawnNonMissionEntityVehicleCoords)
					
					IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
						RETURN FALSE
					ENDIF
					
					IF bCreateFromOffset
						VECTOR vOffsetCoord = GET_CREATE_FROM_OFFSET_VEHICLE_COORD(iVehicle)
						FLOAT fOffsetHeading = GET_CREATE_FROM_OFFSET_VEHICLE_HEADING(iVehicle)
						serverBD.vSpawnNonMissionEntityVehicleCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOffsetCoord, fOffsetHeading, GET_CREATE_FROM_OFFSET_OFFSET(iVehicle)) 
						serverBd.fSpawnNonMissionEntityVehicleHeading = fOffsetHeading
						PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION - Vehicle #", iVehicle, " being created from offset. Offset coord: ", vOffsetCoord)
					// If we're spawning a vehicle up in the area somewhere
					ELIF bShouldSpawnInAir
						VECTOR vCoordInAir = GET_NON_MISSION_ENTITY_VEHICLE_FIND_SPAWN_CENTRE(iVehicle)
						
						IF bShouldFindSpawnLocation
							PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION - Vehicle #", iVehicle, " finding spawn location in air using centre point ", vCoordInAir)
							vCoordInAir = GET_OFFSET_FROM_COORD_IN_WORLD_COORDS(vCoordInAir, 0.0, <<(-1*200 * SIN(TO_FLOAT(serverBD.iCircCount)*30)), (200 * COS(TO_FLOAT(serverBD.iCircCount)*30)), 0.0>>)
						ENDIF
						
						// Handle special spawn cases e.g. above water
						IF SHOULD_USE_GROUND_Z_FOR_AIR_SPAWNING()
							BOOL bGotExactHeight
							IF GET_GROUND_Z_FOR_3D_COORD(vCoordInAir, vCoordInAir.z, TRUE)
								bGotExactHeight = TRUE
								PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION - Vehicle #", iVehicle, " got ", vCoordInAir.z, " as exact ground z.")
							ENDIF
							IF NOT bGotExactHeight
	        					vCoordInAir.z = GET_APPROX_HEIGHT_FOR_POINT(vCoordInAir.x, vCoordInAir.y)
								PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION - Vehicle #", iVehicle, " couldn't get exact ground z. Approx height is ", vCoordInAir.z)
							ENDIF
						ENDIF
	        			vCoordInAir.z += GET_SPAWN_IN_AIR_HEIGHT(iVehicle)
	        
	        			IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(vCoordInAir, 20.0, 1, 1, 15, TRUE, TRUE, TRUE, 180)
	            			serverBD.vSpawnNonMissionEntityVehicleCoords = vCoordInAir
	        			ELSE
	        				serverBD.iCircCount++
				            IF serverBD.iCircCount >= 12
				                serverBD.iCircCount = 0
				            ENDIF
						ENDIF
						
					// If we're spawning a vehicle up on the ground somewhere
					ELIF bShouldFindSpawnLocation
						
						VEHICLE_SPAWN_LOCATION_PARAMS Params
						Params.bConsiderHighways = SHOULD_CONSIDER_HIGHWAYS_FOR_VARIATION(iVehicle)
						Params.bConsiderOnlyActiveNodes = !SHOULD_ALLOW_OFF_ROAD_NODES(iVehicle)
						Params.bAvoidSpawningInExclusionZones = TRUE
						Params.bAllowFallbackToInactiveNodes = FALSE
						Params.bCheckEntityArea = TRUE
						Params.fMaxDistance = GET_VEHICLE_SPAWN_LOCATION_MAX_DISTANCE()
						Params.fMinDistFromCoords = GET_VEHICLE_SPAWN_LOCATION_MIN_DISTANCE()
						Params.bIgnoreCustomNodesForArea = SHOULD_IGNORE_CUSTOM_NODES()
						VECTOR vSpawnCoords = GET_NON_MISSION_ENTITY_VEHICLE_FIND_SPAWN_CENTRE(iVehicle)
						IF NOT IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
							IF IS_VECTOR_ZERO(serverBD.vVehicleSpawnCoord)
								serverBD.vVehicleSpawnCoord = GET_RANDOM_SPAWN_COORD_FOR_VEHICLE(iVehicle, vSpawnCoords)
							ENDIF
							vSpawnCoords = serverBD.vVehicleSpawnCoord
						ENDIF
						IF NOT IS_VECTOR_ZERO(vSpawnCoords)
							#IF IS_DEBUG_BUILD
							IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
								PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] [SUPPORT] - CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION - Vehicle #", iVehicle, " is support vehicle #", GET_SUPPORT_VEHICLE_INDEX(iVehicle), ". Should be created near ", vSpawnCoords)
							ELSE
								PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION - Vehicle #", iVehicle, " should be created near ", vSpawnCoords)
							ENDIF
							#ENDIF
							
							HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(vSpawnCoords, vSpawnCoords, vehModel, FALSE, serverBD.vSpawnNonMissionEntityVehicleCoords, serverBD.fSpawnNonMissionEntityVehicleHeading, Params)
						ELSE
							PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION - vSpawnCoords is zero vector ", vSpawnCoords)
						ENDIF
					// If we're spawning a vehicle at exact coordinates
					ELSE
					
						serverBd.vSpawnNonMissionEntityVehicleCoords = GET_FMBB_VEHICLE_SPAWN_COORDS(GET_FMBB_VARIATION(), GET_FMBB_SUBVARIATION(), iVehicle, GET_CURRENT_DROP_OFF(GET_DROP_OFF_INT_FOR_SPAWN_COORDS(iVehicle)), GET_EXTRA_PARAM_FOR_VEHICLE_SPAWN(iVehicle),GET_RESPAWN_LOCATION())
						serverBd.fSpawnNonMissionEntityVehicleHeading = GET_FMBB_VEHICLE_SPAWN_HEADING(GET_FMBB_VARIATION(), GET_FMBB_SUBVARIATION(), iVehicle, GET_CURRENT_DROP_OFF(GET_DROP_OFF_INT_FOR_SPAWN_COORDS(iVehicle)), GET_EXTRA_PARAM_FOR_VEHICLE_SPAWN(iVehicle),GET_RESPAWN_LOCATION())
		
						
						PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION - Vehicle #", iVehicle, " should be created at server coords ", serverBd.vSpawnNonMissionEntityVehicleCoords)
						PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION - Vehicle #", iVehicle, " should be created at server heading ", serverBd.fSpawnNonMissionEntityVehicleHeading)
					ENDIF
					
				ELSE
					DELETE_VEHICLE_GEN_VEHICLES_IN_AREA(serverBD.vSpawnNonMissionEntityVehicleCoords, 30.0)
					CLEAR_AREA(serverBD.vSpawnNonMissionEntityVehicleCoords, 30.0, FALSE, FALSE, FALSE, TRUE)
					PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION - Calling CREATE_NET_VEHICLE on vehicle #", iVehicle, " with coords ", serverBd.vSpawnNonMissionEntityVehicleCoords)
					IF CREATE_NET_NON_MISSION_ENTITY_VEHICLE(vehModel, iVehicle)
						PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION - Created vehicle #", iVehicle, " with model ", GET_MODEL_NAME_FOR_DEBUG(vehModel)," at ", serverBd.vSpawnNonMissionEntityVehicleCoords)
						IF bCreateFromOffset 
							SET_ENTITY_COORDS_NO_OFFSET(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId), serverBD.vSpawnNonMissionEntityVehicleCoords)
						ENDIF
						SET_VEHICLE_BITSET_DECORATOR(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId), MP_DECORATOR_BS_NON_MODDABLE_VEHICLE)
						SET_VEHICLE_CANNOT_BE_STORED_IN_PROPERTIES(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId))
						IF SHOULD_VEHICLE_BE_INVINCIBLE_AT_START(iVehicle)
							SET_ENTITY_INVINCIBLE(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId), TRUE)
						ENDIF
						ACTIVATE_DAMAGE_TRACKER_ON_NETWORK_ID(serverBD.sVehicle[iVehicle].netId, TRUE)
						serverBD.iCurrentNonMissionEntityVehicleSearchingForSpawnCoords = (-1)
						SET_MODEL_AS_NO_LONGER_NEEDED(vehModel)
						CLEAR_VEHICLE_BIT(iVehicle, eVEHICLEBITSET_I_HAVE_PERFORMED_MODEL_OVERRIDE_CHECK)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_CREATE_VEHICLE_FROM_OFFSET(INT iVehicle)
	SWITCH GET_FMBB_VARIATION()
		CASE BBV_HAULAGE
			SWITCH iVehicle
				CASE 1		RETURN TRUE
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC CREATE_NON_MISSION_ENTITY_VEHICLE(INT iVehicle, MODEL_NAMES vehModel)

	IF DOES_VARIATION_HAVE_VARIATION_VEHICLES()
		IF SHOULD_SPAWN_VARIATION_VEHICLES()
			BOOL bUseIeFunction 
			bUseIeFunction = FALSE
			IF CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION(iVehicle, vehModel, SHOULD_FIND_SPAWN_LOCATION_FOR_VEHICLE(iVehicle), SHOULD_SPAWN_VEHICLE_IN_THE_AIR(iVehicle), bUseIeFunction, SHOULD_DO_OK_TO_SPAWN_CHECK(iVehicle), SHOULD_CREATE_VEHICLE_FROM_OFFSET(iVehicle))
				SET_NON_MISSION_ENTITY_VEHICLE_ATTRIBUTES_FOR_SPAWN(iVehicle)
				SET_VEHICLE_STATE(iVehicle, GET_AFTER_CREATION_VEHICLE_STATE(iVehicle))
				PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_NON_MISSION_ENTITY_VEHICLE - Created vehicle #", iVehicle, " at ", serverBd.vSpawnNonMissionEntityVehicleCoords)
				RESET_SERVER_NON_MISSION_ENTITY_VEHICLE_SPAWN_DATA()
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_FAIL_MODE_FOR_DESTROYED_PREREQ_VEHICLE()
	RETURN FALSE
ENDFUNC

FUNC BOOL OK_TO_CLEAN_UP_HAULAGE_VEHICLE(INT iVehicle)
	IF GET_FMBB_VARIATION() = BBV_HAULAGE
		IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_HAULAGE_POUNDER)
			IF HAS_ANY_MISSION_ENTITY_BEEN_COLLECTED_FOR_THE_FIRST_TIME()
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_IT_SAFE_TO_CLEANUP_VEHICLE(INT iVeh)
	IF IS_VEHICLE_AN_AMBUSH_VEHICLE(iVeh)
	OR IS_VEHICLE_A_SUPPORT_VEHICLE(iVeh)
		RETURN TRUE
	ENDIF
	
	IF IS_VEHICLE_A_CARRIER_VEHICLE(iVeh)
		RETURN FALSE
	ENDIF
	
	SWITCH GET_FMBB_VARIATION()
		CASE BBV_GANG_SHOOTOUT			RETURN FALSE
		CASE BBV_CAR_MEET				RETURN FALSE
		CASE BBV_SHOWROOM				RETURN FALSE
		CASE BBV_HAULAGE				RETURN OK_TO_CLEAN_UP_HAULAGE_VEHICLE(iVeh)
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_VEHICLES_RESPAWN_ON_VARIATION()
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_RESPAWN(INT iVehicle)
	
	IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
	OR IS_VEHICLE_AN_AMBUSH_VEHICLE(iVehicle)
	OR IS_VEHICLE_A_CARRIER_VEHICLE(iVehicle)
		RETURN FALSE
	ENDIF
	
	IF GET_VEHICLE_STATE(iVehicle) < eVEHICLESTATE_NOT_DRIVEABLE
		RETURN FALSE
	ENDIF

	VECTOR vVehCoords = GET_FMBB_VEHICLE_SPAWN_COORDS(GET_FMBB_VARIATION(), GET_FMBB_SUBVARIATION(), iVehicle, GET_CURRENT_DROP_OFF(GET_CURRENT_DROP_OFF_INT(TRUE)),DEFAULT,GET_RESPAWN_LOCATION())

	IF IS_VECTOR_ZERO(vVehCoords)
		RETURN FALSE
	ENDIF
		
	IF NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION(vVehCoords,2,2,2,5.0,TRUE,TRUE,TRUE,15.0,FALSE,DEFAULT,FALSE,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC

FUNC BOOL SHOULD_DELETE_MISSION_ENTITY_PICKUP_ON_CARRIER_DESTROYED()

	IF GET_FMBB_BUSINESS_TYPE() = BBT_EXPORT_GARAGE
		PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] SHOULD_DELETE_MISSION_ENTITY_PICKUP_ON_CARRIER_DESTROYED - GET_FMBB_BUSINESS_TYPE = BBT_EXPORT_GARAGE - return true.")
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

PROC MAINTAIN_NON_MISSION_ENTITY_VEHICLE_BRAINS()

	INT iVehicle
	INT iSupportVehCount, iCarrierVehCount, iActiveTotalVehCount
	
//	VEHICLE_INDEX vehId
//	MODEL_NAMES eModel
	
	REPEAT NUM_VARIATION_VEHICLES iVehicle
		
		SWITCH GET_VEHICLE_STATE(iVehicle)
			
			CASE eVEHICLESTATE_INACTIVE
				
			BREAK
			
			CASE eVEHICLESTATE_WAITING_FOR_RESPAWN
				IF DOES_FMBB_VARIATION_HAVE_AMBUSH(GET_FMBB_VARIATION())
				AND IS_VEHICLE_AN_AMBUSH_VEHICLE(iVehicle)
					IF HAS_AMBUSH_RESPAWN_DELAY_PASSED(TRUE)
						SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_CREATE)
						PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] [AMBUSH] - eVEHICLESTATE_WAITING_FOR_RESPAWN - Vehicle #", iVehicle, " moved to eVEHICLESTATE_CREATE after respawn delay.")
					ENDIF
				ENDIF
			BREAK
			
			CASE eVEHICLESTATE_CREATE
				
				IF OK_TO_CREATE_NON_MISSION_ENTITY_VEHICLE(iVehicle)
					MAINTAIN_AMBUSH_VEHICLE_MODEL_SETUP(iVehicle)
					
					MODEL_NAMES eVehicleModel
					eVehicleModel = GET_FMBB_VEHICLE_MODEL(GET_FMBB_VARIATION(),GET_FMBB_SUBVARIATION(), iVehicle, serverBD.iChosenVehicleModel)
					IF IS_VEHICLE_AN_AMBUSH_VEHICLE(iVehicle)
						eVehicleModel = GET_FMBB_AMBUSH_VEHICLE_MODEL(GET_FMBB_VARIATION(), GET_FMBB_SUBVARIATION(), GET_AMBUSH_VEHICLE_NUMBER(iVehicle), serverBD.iRandomModeInt0)
						IF serverBD.eAmbushVehicleModel[GET_AMBUSH_VEHICLE_NUMBER(iVehicle)] != DUMMY_MODEL_FOR_SCRIPT 
							eVehicleModel = serverBD.eAmbushVehicleModel[GET_AMBUSH_VEHICLE_NUMBER(iVehicle)]
						ENDIF
					ENDIF
					
					IF REQUEST_LOAD_MODEL(eVehicleModel)
						PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] eVEHICLESTATE_CREATE - Loaded Vehicle #", iVehicle)
						CREATE_NON_MISSION_ENTITY_VEHICLE(iVehicle, eVehicleModel)
					ENDIF
				ENDIF
				
			BREAK
			
			CASE eVEHICLESTATE_DRIVEABLE
				
				// Need to do this once nearby.
				IF IS_NET_VEHICLE_DRIVEABLE(serverBD.sVehicle[iVehicle].netId)
				
//					vehId = NET_TO_VEH(serverBD.sVehicle[iVehicle].netId)
//					
//					eModel = GET_ENTITY_MODEL(vehId)
					
					iActiveTotalVehCount++
					IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
						iSupportVehCount++
					ELIF IS_VEHICLE_A_CARRIER_VEHICLE(iVehicle)
						iCarrierVehCount++
					ENDIF
					
					IF OK_TO_CLEAN_UP_HAULAGE_VEHICLE(iVehicle)
						SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_NOT_DRIVEABLE)
					ENDIF
					
					IF GET_FMBB_VARIATION() = BBV_JOYRIDERS
					AND NOT IS_SERVER_BIT_SET(eSERVERBITSET_MAKE_JOYRIDERS_DRIVE_TO_POINT)
					AND IS_VEHICLE_A_CARRIER_VEHICLE(iVehicle)
						IF GET_ENTITY_HEALTH(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId)) < 700
							SET_SERVER_BIT(eSERVERBITSET_MAKE_JOYRIDERS_DRIVE_TO_POINT)
						ENDIF
					ENDIF
					
					IF HAS_NET_TIMER_EXPIRED(stDogfightAmbushActionTimer, 5000)
						IF NOT IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
						AND NOT IS_VEHICLE_AN_AMBUSH_VEHICLE(iVehicle)
							RESET_NET_TIMER(stDogfightAmbushActionTimer)
						ENDIF
					ENDIF
					
					IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_RADIO_VEHICLE)
						IF NOT IS_VEHICLE_EMPTY_AND_STATIONARY(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId))	
							CLEAR_VEHICLE_BIT(iVehicle, eVEHICLEBITSET_I_AM_RADIO_VEHICLE)
						ENDIF
					ENDIF
					
				ELSE
					IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_PREREQUISITE_VEHICLE)
						IF SHOULD_FAIL_MODE_FOR_DESTROYED_PREREQ_VEHICLE()
							SET_END_REASON(eENDREASON_PREREQUISITE_VEHICLE_DESTROYED)
							SET_MODE_STATE(eMODESTATE_REWARDS)
						ENDIF
					ENDIF
					IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
						IF NOT HAVE_ALL_MISSION_ENTITIES_IN_CARRIER_VEHICLE_BEEN_DELIVERED(iVehicle,TRUE)
							IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sVehicle[iVehicle].netId)
								SET_SERVER_BIT(eSERVERBITSET_ANY_CARRIER_VEHICLE_DESTROYED)
							ENDIF
						ENDIF
					ENDIF
					SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_NOT_DRIVEABLE)
				ENDIF	
			BREAK
			
			CASE eVEHICLESTATE_NOT_DRIVEABLE
				
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
					
					IF SHOULD_DELETE_MISSION_ENTITY_PICKUP_ON_CARRIER_DESTROYED()
						INT iMissionEntity
						REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
							IF DOES_MISSION_ENTITY_HAVE_CARRIER_VEHICLE(iMissionEntity)
								IF iVehicle = GET_MISSION_ENTITY_CARRIER_VEHICLE(iMissionEntity)
									SET_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_DESTROYED)
									SET_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_RECEIVED_DESTROYED_DAMAGE_EVENT)
								ENDIF
							ENDIF
						ENDREPEAT
					ENDIF

					IF IS_IT_SAFE_TO_CLEANUP_VEHICLE(iVehicle)
						CLEANUP_NET_ID(serverBD.sVehicle[iVehicle].netId)
						PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - eVEHICLESTATE_NOT_DRIVEABLE - Vehicle #", iVehicle, " cleaned up as no longer driveable.")
					ENDIF
				ENDIF
				
				IF DOES_FMBB_VARIATION_HAVE_AMBUSH(GET_FMBB_VARIATION())
				AND IS_VEHICLE_AN_AMBUSH_VEHICLE(iVehicle)
					IF SHOULD_RESPAWN_AMBUSH_VEHICLE(iVehicle)
						RESET_VEHICLE_DATA_FOR_RESPAWN(iVehicle)
						SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_WAITING_FOR_RESPAWN)
						PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] [AMBUSH] - eVEHICLESTATE_NOT_DRIVEABLE - Vehicle #", iVehicle, " set to eVEHICLESTATE_WAITING_FOR_RESPAWN.")
					ENDIF
				ENDIF
				
			BREAK
			
			CASE eVEHICLESTATE_WARP
				
			BREAK
			
			CASE eVEHICLESTATE_NO_LONGER_NEEDED
			
			BREAK
			
			CASE eVEHICLESTATE_RESET
				
			BREAK
			
		ENDSWITCH
		
		//If vehicles should respawn
		IF SHOULD_VEHICLES_RESPAWN_ON_VARIATION()
			IF SHOULD_VEHICLE_RESPAWN(iVehicle)
				SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_CREATE)
			ENDIF
		ENDIF
		
		// Detect if the ped is dead and set state accordingly.
		IF GET_VEHICLE_STATE(iVehicle) > eVEHICLESTATE_CREATE
			IF GET_VEHICLE_STATE(iVehicle) != eVEHICLESTATE_NOT_DRIVEABLE
			AND GET_VEHICLE_STATE(iVehicle) != eVEHICLESTATE_WARP
				IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
					PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - Vehicle #", iVehicle, " does not exist.")
					SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_NOT_DRIVEABLE)
				ELSE
					IF IS_ENTITY_DEAD(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId))
					OR NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.sVehicle[iVehicle].netId)
						PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - Vehicle #", iVehicle, " is dead.")
						SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_NOT_DRIVEABLE)
					ELIF IS_VEHICLE_AN_AMBUSH_VEHICLE(iVehicle) 
					AND ARE_ALL_AMBUSH_PEDS_IN_VEHICLE_DEAD(iVehicle)
						PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] [AMBUSH] - Vehicle #", iVehicle, " cleaned up since all it's peds are dead.")
						SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_NOT_DRIVEABLE)
					ENDIF
				ENDIF

			ENDIF
		ENDIF	
			
	ENDREPEAT
	
	IF serverBD.iActiveCarrierVehCount <> iCarrierVehCount
		serverBD.iActiveCarrierVehCount = iCarrierVehCount
	ENDIF
	
	IF serverBD.iActiveSuppportVehCount <> iSupportVehCount
		serverBD.iActiveSuppportVehCount = iSupportVehCount
	ENDIF
	
	IF serverBd.iActiveTotalVehCount <> iActiveTotalVehCount
		serverBd.iActiveTotalVehCount = iActiveTotalVehCount
	ENDIF
ENDPROC

FUNC BOOL SHOULD_DESTROY_UNDRIVEABLE_NON_MISSION_ENTITIES_IN_VARIATION()
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_DESTROY_UNDRIVEABLE_NON_MISSION_ENTITY_VEHICLE(INT iVehicle)
	IF SHOULD_DESTROY_UNDRIVEABLE_NON_MISSION_ENTITIES_IN_VARIATION()
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
			IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
				IF IS_ENTITY_ALIVE(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId))
					IF NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId))
					OR GET_ENTITY_HEALTH(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId)) < (GET_ENTITY_MAX_HEALTH(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId)) / 5)
					OR GET_VEHICLE_ENGINE_HEALTH(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId)) < 150
						PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_DESTROY_UNDRIVEABLE_NON_MISSION_ENTITY_VEHICLE - Vehicle #", iVehicle, " is no longer driveable, destroying it")
						NETWORK_EXPLODE_VEHICLE(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId))
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_DO_DESTROY_HELICOPTER_CHECKS(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_HELICOPTER_TOO_LOW(VEHICLE_INDEX vehId #IF IS_DEBUG_BUILD , INT iVehicle #ENDIF )
	VECTOR vHelicoords = GET_ENTITY_COORDS(vehId, FALSE)
	FLOAT fZPosition
	
	IF GET_GROUND_Z_FOR_3D_COORD(vHelicoords, fZPosition, TRUE)
		IF vHelicoords.z < fZPosition + 30
			PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_SHOULD_DESTROY_HELICOPTER - IS_HELICOPTER_TOO_LOW(", iVehicle, ") - Heli Z: ", vHelicoords.z, " - Ground Z: ", fZPosition)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_SHOULD_DESTROY_HELICOPTER(INT iVehicle)
	IF NOT SHOULD_DO_DESTROY_HELICOPTER_CHECKS(iVehicle)
		EXIT
	ENDIF
	
	IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
		VEHICLE_INDEX vehId = NET_TO_VEH(serverBD.sVehicle[iVehicle].netId)
		IF NOT IS_ENTITY_DEAD(vehId)
			IF /*IS_HELICOPTER_DAMAGED(vehId #IF IS_DEBUG_BUILD , iVehicle #ENDIF )
			OR*/ IS_HELICOPTER_TOO_LOW(vehId #IF IS_DEBUG_BUILD , iVehicle #ENDIF )
				NETWORK_EXPLODE_HELI(vehId, TRUE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

INT iLastScriptVehicleInside = -1
PROC MAINTAIN_WARPING_VEHICLES_ON_DELIVERY(INT iVehicle)
	
	IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), GET_DROP_OFF_COORDS(), 10.0)
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX vehID = NET_TO_VEH(serverBD.sVehicle[iVehicle].netId)
			IF vehID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			AND iLastScriptVehicleInside != iVehicle
				iLastScriptVehicleInside = iVehicle
				PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] eVEHICLESTATE_WARP - Player is inside vehicle #", iVehicle, " near bunker.")
			ENDIF
		ENDIF
	ELIF iLastScriptVehicleInside != (-1)
	AND NOT g_FreemodeDeliveryData.bDeliveryScriptTriggeredCutScene
	AND NOT NETWORK_IS_IN_MP_CUTSCENE()
		iLastScriptVehicleInside = -1
		PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] eVEHICLESTATE_WARP - Clearing last vehicle index. Player is no longer in range of bunker.")
	ENDIF
	
	IF iVehicle = iLastScriptVehicleInside
		IF g_FreemodeDeliveryData.bDeliveryScriptTriggeredCutScene
		AND NETWORK_IS_IN_MP_CUTSCENE()
			BROADCAST_EVENT_WARP_BUYSELL_SCRIPT_VEHICLE(iVehicle, FALSE, serverBD.iLaunchPosix)
			iLastScriptVehicleInside = -1
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_CARRIER_VEHICLES_BE_MADE_DESTROYABLE_FOR_VARIATION()
	SWITCH GET_FMBB_VARIATION()
		CASE BBV_SHOWROOM		
			IF GET_MODE_STATE() < eMODESTATE_COLLECT_MISSION_ENTITY
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN HAS_ANY_MISSION_ENTITY_BEEN_COLLECTED_FOR_THE_FIRST_TIME()
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_BE_MADE_DESTROYABLE(INT iVehicle)
	IF IS_PED_SITTING_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.sVehicle[iVehicle].netId))
		IF IS_VEHICLE_BIT_SET(iVehicle,eVEHICLEBITSET_I_AM_PREREQUISITE_VEHICLE)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_BIT_SET(iVehicle,eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
		IF SHOULD_CARRIER_VEHICLES_BE_MADE_DESTROYABLE_FOR_VARIATION()
			INT iMissionEntity
			REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
				IF DOES_MISSION_ENTITY_HAVE_CARRIER_VEHICLE(iMissionEntity)
					IF iVehicle = GET_MISSION_ENTITY_CARRIER_VEHICLE(iMissionEntity)
						IF IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_SOMEONE_IS_NEAR_ME)
						OR IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_I_HAVE_BEEN_COLLECTED_FOR_FIRST_TIME)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
	SWITCH GET_FMBB_VARIATION()
		CASE BBV_MAX
			
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_TIME_STUCK_TIL_RESET()
	RETURN 10000
ENDFUNC

FUNC INT GET_TIME_JAMMED_TIL_RESET()
	RETURN 15000
ENDFUNC

PROC MAINTAIN_WARPING_STUCK_VEHICLES(NETWORK_INDEX netVeh, BOOL bDoHelpOnWarp = FALSE)

	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(netVeh)

		VEHICLE_INDEX vehID = NET_TO_VEH(netVeh)
		MODEL_NAMES model = GET_ENTITY_MODEL(vehID)
		
		IF DOES_ENTITY_EXIST(vehID)
		AND NOT IS_ENTITY_DEAD(vehID,TRUE)
			
			IF IS_VEHICLE_STUCK_TIMER_UP(vehID, VEH_STUCK_ON_ROOF, GET_TIME_STUCK_TIL_RESET())
			OR IS_VEHICLE_STUCK_TIMER_UP(vehID, VEH_STUCK_ON_SIDE, GET_TIME_STUCK_TIL_RESET())
			OR IS_VEHICLE_STUCK_TIMER_UP(vehID, VEH_STUCK_JAMMED, GET_TIME_JAMMED_TIL_RESET())
			OR IS_VEHICLE_STUCK_TIMER_UP(vehID, VEH_STUCK_HUNG_UP, GET_TIME_STUCK_TIL_RESET())
				
				IF bDoHelpOnWarp
					IF IS_LOCAL_PLAYER_IN_PARTICIPATION_RANGE(150.0)
						TRIGGER_HELP(eHELPTEXT_STUCK_VEHICLE_WARPED)
					ENDIF
				ENDIF
				
				IF MAINTAIN_CONTROL_OF_NETWORK_ID(netVeh)
					PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_WARPING_STUCK_VEHICLES - Vehicle is stuck. Finding warp location.")
					
					VECTOR vSpawnLocation
					FLOAT fSpawnHeading
					VEHICLE_SPAWN_LOCATION_PARAMS vehicleSpawnLocationParams
					
					vehicleSpawnLocationParams.fMinDistFromCoords = 20
					vehicleSpawnLocationParams.fMaxDistance = 150
					vehicleSpawnLocationParams.bConsiderHighways = TRUE
					vehicleSpawnLocationParams.bCheckEntityArea = TRUE
					vehicleSpawnLocationParams.bCheckOwnVisibility = FALSE
					vehicleSpawnLocationParams.bConsiderOnlyActiveNodes = TRUE
					
					IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(GET_ENTITY_COORDS(vehID), <<0.0, 0.0, 0.0>>, model, TRUE, vSpawnLocation, fSpawnHeading, vehicleSpawnLocationParams)									  
						PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_WARPING_STUCK_VEHICLES - Warping vehicle to ", vSpawnLocation)
						
						NETWORK_FADE_IN_ENTITY(vehID, TRUE)
						SET_ENTITY_COORDS_NO_OFFSET(vehID, vSpawnLocation)
						SET_ENTITY_HEADING(vehID, fSpawnHeading)
						SET_VEHICLE_ON_GROUND_PROPERLY(vehID)
						
						// Fade in the carrier vehicle attached to the trailer too for Haulage
						IF GET_FMBB_VARIATION() = BBV_HAULAGE
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[2].netId)
							AND IS_NET_VEHICLE_DRIVEABLE(serverBD.sVehicle[2].netId)
								NETWORK_FADE_IN_ENTITY(NET_TO_ENT(serverBD.sVehicle[2].netId), TRUE)
							ENDIF
						ENDIF
						
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF

	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_VEHICLE_BE_UNLOCKED_ON_CONDITION(INT iVehicle)
	
	SWITCH GET_FMBB_VARIATION()
		CASE BBV_UNDER_GUARD
			IF IS_VEHICLE_A_CARRIER_VEHICLE(iVehicle)
				RETURN IS_SERVER_BIT_SET(eSERVERBITSET_TARGETS_ELIMINATED)
			ENDIF
		BREAK
		CASE BBV_BREAK_IN				RETURN GET_MODE_STATE() >= eMODESTATE_COLLECT_MISSION_ENTITY
		CASE BBV_SHOWROOM				RETURN GET_CLIENT_MODE_STATE() > eMODESTATE_MINIGAME
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_SPEED_BE_ALTERED(INT iVehicle, VEHICLE_INDEX vehID)
	UNUSED_PARAMETER(iVehicle)
	UNUSED_PARAMETER(vehId)
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_ALTERED_VEHICLE_SPEED()
	RETURN 30.0
ENDFUNC

FUNC BOOL SHOULD_EMPTY_AND_LOCK_VEHICLE_ON_END(MODEL_NAMES eModel, INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	UNUSED_PARAMETER(eModel)
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_RUN_VEHICLE_DISTANCE_CHECKS()

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VEHICLE_VALID_FOR_CLOSEST_CHECK(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_ANY_PED_IN_NON_MISSION_ENTITY_VEHICLE(INT iVeh)
	PED_INDEX tempPed
	VEHICLE_INDEX tempVeh = NET_TO_VEH(serverBD.sVehicle[iVeh].netId)
	INT i
	
	IF DOES_ENTITY_EXIST(tempVeh)
		FOR i= -1 TO ENUM_TO_INT(VS_EXTRA_RIGHT_3)
			tempPed = GET_PED_IN_VEHICLE_SEAT(tempVeh, INT_TO_ENUM(VEHICLE_SEAT, i))
			IF DOES_ENTITY_EXIST(tempPed)
				IF NOT IS_ENTITY_DEAD(tempPed)
				AND NOT IS_PED_INJURED(tempPed)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SUPPORT_VEHICLE_BLIP_CHECK(INT iVehicle)

	SWITCH GET_FMBB_VARIATION()
		CASE BBV_MAX
			IF GET_CLIENT_MODE_STATE() < eMODESTATE_COLLECT_MISSION_ENTITY
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH

	RETURN IS_VEHICLE_EMPTY(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId))
ENDFUNC

FUNC BOOL SHOULD_NON_MISSION_ENTITY_VEHICLE_DISPLAY_UNIQUE_BLIP(INT iVehicle)

	IF NOT bSafeToDisplay
		RETURN FALSE
	ENDIF

	IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
		AND IS_NET_VEHICLE_DRIVEABLE(serverBD.sVehicle[iVehicle].netId)
		AND SUPPORT_VEHICLE_BLIP_CHECK(iVehicle)
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_BLIP_FLASH(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_FADE_BLIP_BASED_ON_DIST(INT iVehicle)
	IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC HUD_COLOURS GET_SUPPORT_BLIP_COLOUR()
	RETURN HUD_COLOUR_WHITE
ENDFUNC

FUNC BOOL SHOULD_ROTATE_BLIP(INT iVehicle, VEHICLE_INDEX vehId)
	
	IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
	AND NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehId))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC HUD_COLOURS GET_VEHICLE_BLIP_COLOUR(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)

	RETURN HUD_COLOUR_BLUE
ENDFUNC

FUNC BOOL SHOULD_UPDATE_VEHICLE_BLIP_COLOUR(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	SWITCH GET_FMBB_VARIATION()
		CASE BBV_MAX		
			IF GET_BLIP_COLOUR(blipVariationVehBlip[iVehicle]) != GET_BLIP_COLOUR_FROM_HUD_COLOUR(GET_VEHICLE_BLIP_COLOUR(iVehicle))
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_NON_MISSION_ENTITY_VEHICLE_UNIQUE_BLIP(INT iVehicle)

	BOOL bExists = FALSE
	BOOL bAlive = FALSE

	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
		bExists = TRUE
		IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId))
			bAlive = TRUE
		ENDIF
	ENDIF

	IF ((bExists AND bAlive) OR (IS_BLIP_FLASHING_ON_DESTRUCTION(blipVariationVehBlip[iVehicle],!bAlive,iVehFlashingBS,iVehicle)))
	AND SHOULD_NON_MISSION_ENTITY_VEHICLE_DISPLAY_UNIQUE_BLIP(iVehicle)
		IF NOT DOES_BLIP_EXIST(blipVariationVehBlip[iVehicle])
			IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
				ADD_MISSION_BLIP_FOR_ENTITY(blipVariationVehBlip[iVehicle], NET_TO_ENT(serverBD.sVehicle[iVehicle].netId), GET_SUPPORT_VEHICLE_BLIP_SPRITE(), GET_SUPPORT_BLIP_COLOUR(), GET_SUPPORT_VEHICLE_BLIP_NAME(FALSE), FALSE, FALSE, (NOT HAS_HELP_BEEN_DONE(eHELPTEXT_EXPLAIN_SUPPORT_VEHICLES)))
			ELSE
				SWITCH GET_FMBB_VARIATION()
					CASE BBV_MAX
						ADD_MISSION_BLIP_FOR_ENTITY(blipVariationVehBlip[iVehicle], NET_TO_ENT(serverBD.sVehicle[iVehicle].netId), RADAR_TRACE_TRUCK, HUD_COLOUR_GREEN, "FH_TRUCK")
					BREAK
				ENDSWITCH
			ENDIF
		ELSE
			IF SHOULD_ROTATE_BLIP(iVehicle, NET_TO_VEH(serverBD.sVehicle[iVehicle].netId))
				SET_BLIP_ROTATION(blipVariationVehBlip[iVehicle], ROUND(GET_ENTITY_HEADING_FROM_EULERS(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId))))
			ENDIF
			
			IF SHOULD_UPDATE_VEHICLE_BLIP_COLOUR(iVehicle)
				SET_BLIP_COLOUR_FROM_HUD_COLOUR(blipVariationVehBlip[iVehicle], GET_VEHICLE_BLIP_COLOUR(iVehicle))
			ENDIF
			
			IF SHOULD_FADE_BLIP_BASED_ON_DIST(iVehicle)
				IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
					SET_BLIP_ALPHA_BASED_ON_DIST_FROM_PLAYER(blipVariationVehBlip[iVehicle], 50)
				ELSE
					SET_BLIP_ALPHA_BASED_ON_DIST_FROM_PLAYER(blipVariationVehBlip[iVehicle], 100, 5000)
				ENDIF
			ENDIF
			
			IF SHOULD_BLIP_FLASH(iVehicle)
				IF NOT IS_BLIP_FLASHING(blipVariationVehBlip[iVehicle])
					FLASH_MISSION_BLIP(blipVariationVehBlip[iVehicle])
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(blipVariationVehBlip[iVehicle])
			REMOVE_BLIP(blipVariationVehBlip[iVehicle])
			PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_NON_MISSION_ENTITY_VEHICLE_UNIQUE_BLIP - removed blip from sVehicle[", iVehicle, "].netId")
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL SHOULD_ALLOW_USE_AFTER_MISSION(MODEL_NAMES vehModel, INT iVehicle)
	UNUSED_PARAMETER(vehModel)
	IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
		
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_USE_PMSD_FOR_VEHICLE(MODEL_NAMES eModel)
	SWITCH eModel
		CASE TULA	RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC SET_VEHICLE_UNLOCK_STATE_FOR_VARIATION(NETWORK_INDEX netId, VEHICLE_INDEX vehId)
	
	IF MAINTAIN_CONTROL_OF_NETWORK_ID(netId)
		IF SHOULD_CARRIER_VEHICLE_NEED_BROKEN_INTO(GET_ENTITY_MODEL(vehId))
			SET_VEHICLE_DOORS_LOCKED(vehId, VEHICLELOCK_LOCKED_BUT_CAN_BE_DAMAGED)
		ELSE
			SET_VEHICLE_DOORS_LOCKED(vehID, VEHICLELOCK_UNLOCKED)	
		ENDIF
		SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehId, FALSE)
		SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(vehID, PLAYER_ID(), FALSE)
		SET_VEHICLE_DOORS_LOCKED_FOR_NON_SCRIPT_PLAYERS(vehId, TRUE)
		SET_VEHICLE_DISABLE_TOWING(vehId, FALSE)
		PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] SET_VEHICLE_UNLOCK_STATE_FOR_VARIATION - Set.")
	ENDIF
ENDPROC

FUNC NETWORK_INDEX GET_TRAILER_VEHICLE_NEEDS_ATTACHED_TO(INT iVehicle)
	SWITCH GET_FMBB_VARIATION()
		CASE BBV_HAULAGE
			SWITCH iVehicle	
				CASE 1	
					RETURN serverBD.sVehicle[0].netId
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN INT_TO_NATIVE(NETWORK_INDEX, -1)
ENDFUNC

PROC MAINTAIN_ATTACHING_VEHICLE_LOGIC(INT iVehicle, VEHICLE_INDEX vehID)
	IF NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_NEEDS_ATTACHED_TO_TRAILER)
	OR IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_ATTACHED_TO_TRAILER)
	OR IS_VEHICLE_CLIENT_BIT_SET(iVehicle, PARTICIPANT_ID(), eVEHICLECLIENTBITSET_ATTACHED_TO_TRAILER)
		EXIT
	ENDIF
	
	NETWORK_INDEX trailerNetID = GET_TRAILER_VEHICLE_NEEDS_ATTACHED_TO(iVehicle)
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(trailerNetID)
	AND IS_NET_VEHICLE_DRIVEABLE(trailerNetID)
	AND NOT IS_ENTITY_DEAD(vehID)
		VEHICLE_INDEX trailerVehId = NET_TO_VEH(trailerNetID)
		IF IS_THIS_MODEL_A_TRUCK_CAB(GET_ENTITY_MODEL(vehID))
			IF NOT IS_VEHICLE_ATTACHED_TO_TRAILER(vehID)
				ATTACH_VEHICLE_TO_TRAILER(vehID, trailerVehId, 1.0)
				PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_ATTACHING_VEHICLE_LOGIC - Called ATTACH_VEHICLE_TO_TRAILER on vehicle #", iVehicle)
			ELSE
				SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_ATTACHED_TO_TRAILER)
				PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_ATTACHING_VEHICLE_LOGIC - Attached vehicle #", iVehicle, " to trailer.")
			ENDIF
		ELSE
			IF NOT IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(vehID)
				//VECTOR vTrailerRot = GET_ENTITY_ROTATION(trailerVehId)
				ATTACH_ENTITY_TO_ENTITY(vehID, trailerVehId, GET_ENTITY_BONE_INDEX_BY_NAME(trailerVehId, "chassis_dummy"), <<0.0, -5.5, 0.7>>, <<0.0, 0.0, 0.0>>, FALSE, FALSE, FALSE)
				PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_ATTACHING_VEHICLE_LOGIC - Called ATTACH_ENTITY_TO_ENTITY on vehicle #", iVehicle)
			ELSE
				SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_ATTACHED_TO_TRAILER)
				PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_ATTACHING_VEHICLE_LOGIC - Attached vehicle #", iVehicle, " to vehicle.")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_HAULAGE_TRAILER_DOOR_LOGIC(INT iVehicle, VEHICLE_INDEX vehID)
	IF GET_FMBB_VARIATION() != BBV_HAULAGE
		EXIT
	ENDIF
	
	IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_HAULAGE_POUNDER)
		
		BOOL bVehicleDead = IS_ENTITY_DEAD(vehId)

		IF NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_OPENED_BOOT)
			IF GET_VEHICLE_STATE(iVehicle) > eVEHICLESTATE_DRIVEABLE
				IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
					SET_VEHICLE_DOOR_BROKEN(vehId, SC_DOOR_REAR_LEFT, FALSE)
					SET_VEHICLE_DOOR_BROKEN(vehId, SC_DOOR_REAR_RIGHT, FALSE)
					SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_BLOWN_DOORS_OPEN)
					SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_OPENED_BOOT)
					PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_HAULAGE_TRAILER_DOOR_LOGIC - Trailer door of vehicle #", iVehicle, " has been broken off after dying or carrier detachings.")
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_OPENED_BOOT)
			
			IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), vehID, 100.0, FALSE)
				TRIGGER_HELP(eHELPTEXT_HAULAGE_BLOW_UP)
			ENDIF
			
			IF NOT bVehicleDead
				IF NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_BLOWN_DOORS_OPEN)
					BOOL bOpenDoors
					VECTOR vBackdoor = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehId, <<0.0, -8.0, 1.6>>)
					FLOAT fExplosionRadius = 1.5
					
					#IF IS_DEBUG_BUILD
					IF fHaulageExplosionRadius != 0.0
						fExplosionRadius = fHaulageExplosionRadius
					ENDIF
					IF bDrawHaulageExplosionSphere
						INT r, g, b, a
						GET_HUD_COLOUR(HUD_COLOUR_REDLIGHT, r, g, b, a)
						DRAW_MARKER_SPHERE(vBackdoor, fExplosionRadius, r, g, b, 0.5)
					ENDIF
					#ENDIF
					
					IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, vBackdoor, fExplosionRadius)
						bOpenDoors = TRUE
						PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_HAULAGE_TRAILER_DOOR_LOGIC - Trailer doors of vehicle #", iVehicle, " have been blown open. Breaking them off.")
					ENDIF
					
					IF bOpenDoors
						IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
							SET_VEHICLE_DOOR_CONTROL(vehId, SC_DOOR_REAR_LEFT, DT_DOOR_NO_RESET, 0.3)
							SET_VEHICLE_DOOR_CONTROL(vehId, SC_DOOR_REAR_RIGHT, DT_DOOR_NO_RESET, 0.3)
							SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_BLOWN_DOORS_OPEN)
						ENDIF
					ENDIF
				ELSE
					IF HAS_NET_TIMER_EXPIRED(serverBD.stGenericMissionTimer, 400)
					AND IS_VEHICLE_DOOR_DAMAGED(vehId, SC_DOOR_REAR_LEFT)
					AND IS_VEHICLE_DOOR_DAMAGED(vehId, SC_DOOR_REAR_RIGHT)
						SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_OPENED_BOOT)
						PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_HAULAGE_TRAILER_DOOR_LOGIC - 400ms expired since doors of vehicle #", iVehicle, " blown open. Moving on for detach.")
					ELIF HAS_NET_TIMER_EXPIRED(serverBD.stGenericMissionTimer, 200)
					AND IS_VEHICLE_DOOR_DAMAGED(vehId, SC_DOOR_REAR_RIGHT)
						IF NOT IS_VEHICLE_DOOR_DAMAGED(vehId, SC_DOOR_REAR_LEFT)
							IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
								SET_VEHICLE_DOOR_BROKEN(vehId, SC_DOOR_REAR_LEFT, FALSE)
								PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_HAULAGE_TRAILER_DOOR_LOGIC - 200ms expired since doors of vehicle #", iVehicle, " blown open. Breaking off rear left.")
							ENDIF
						ENDIF
					ELIF HAS_NET_TIMER_EXPIRED(serverBD.stGenericMissionTimer, 100)
						IF NOT IS_VEHICLE_DOOR_DAMAGED(vehId, SC_DOOR_REAR_RIGHT)
							IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
								SET_VEHICLE_DOOR_BROKEN(vehId, SC_DOOR_REAR_RIGHT, FALSE)
								PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_HAULAGE_TRAILER_DOOR_LOGIC - 100ms expired since doors of vehicle #", iVehicle, " blown open. Breaking off rear right.")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELIF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
		IF IS_VEHICLE_BIT_SET(0, eVEHICLEBITSET_OPENED_BOOT)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[0].netId)
				IF NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_DETACHED_FROM_VEHICLE)
					IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
						IF IS_ENTITY_ATTACHED_TO_ENTITY(vehId, NET_TO_ENT(serverBD.sVehicle[0].netId))
							DETACH_ENTITY(vehId)
							
							IF GET_VEHICLE_STATE(0) > eVEHICLESTATE_DRIVEABLE
								NETWORK_EXPLODE_VEHICLE(vehId)
								PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_HAULAGE_TRAILER_DOOR_LOGIC - Destroyed carrier vehicle #", iVehicle, " as trailer is dead.")
							ENDIF
							
							SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_DETACHED_FROM_VEHICLE)
							PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_HAULAGE_TRAILER_DOOR_LOGIC - Detached carrier vehicle #", iVehicle, " from trailer.")
						ENDIF
					ENDIF
				ELIF NOT IS_VEHICLE_CLIENT_BIT_SET(iVehicle, PARTICIPANT_ID(), eVEHICLECLIENTBITSET_APPLIED_FORCE)
				AND NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_APPLIED_FORCE)
					IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
						VECTOR vPushDirection = GET_ENTITY_COORDS(vehId, FALSE) - GET_ENTITY_COORDS(NET_TO_ENT(serverBD.sVehicle[0].netId), FALSE)
			            vPushDirection = NORMALISE_VECTOR(vPushDirection)
			            vPushDirection.x *= 18.0
			            vPushDirection.y *= 18.0
			            vPushDirection.z *= 0.0
						APPLY_FORCE_TO_ENTITY(vehId, APPLY_TYPE_IMPULSE, vPushDirection, <<0.0,0.0,0.0>>, 0, FALSE, TRUE, TRUE)
						SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_APPLIED_FORCE)
						PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_HAULAGE_TRAILER_DOOR_LOGIC - Applied force to carrier vehicle #", iVehicle)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_PMSD_FOR_CARRIER_VEHICLES(INT iVehicle, VEHICLE_INDEX vehId)
	IF NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
	OR GET_END_REASON() = eENDREASON_NO_REASON_YET
		EXIT
	ENDIF

	IF IS_VEHICLE_DRIVEABLE(vehId)
		INT iMissionEntity
		REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
			IF iVehicle = GET_MISSION_ENTITY_CARRIER_VEHICLE(iMissionEntity)
				IF NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_DELIVERED)
				AND NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_DESTROYED)
					IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
						SET_ENTITY_INVINCIBLE(vehId, FALSE)
						SET_VEHICLE_ENGINE_HEALTH(vehId, -1000.0)
						PRINTLN("[FMBB] [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_PMSD_FOR_CARRIER_VEHICLES - Killing engine for vehicle #", iVehicle, " as it is carrying mission entity #", iVehicle)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

FUNC BOOL SHOULD_PEDS_REACT_ON_VEHICLE_ENTRY(INT iVehicle)

	SWITCH GET_FMBB_VARIATION()
		CASE BBV_CAR_MEET
			RETURN TRUE
		CASE BBV_BREAK_IN
			SWITCH iVehicle
				CASE 0
				CASE 1
				CASE 2
				CASE 3
					RETURN TRUE
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

FUNC INT GET_PED_TO_REACT_ON_VEHICLE_ENTRY(INT iVehicle)

	SWITCH GET_FMBB_VARIATION()
		CASE BBV_BREAK_IN
			SWITCH iVehicle
				CASE 0
				CASE 1
					RETURN 0
				CASE 2
				CASE 3
					RETURN 3
			ENDSWITCH
		BREAK
	ENDSWITCH

	RETURN -1

ENDFUNC

PROC MAINTAIN_NON_MISSION_ENTITY_VEHICLE_BODIES()

	INT iVehicle
	VECTOR vSpawnLocation
	FLOAT fSpawnHeading
	VEHICLE_SPAWN_LOCATION_PARAMS vehicleSpawnLocationParams
	VEHICLE_INDEX vehID
	BOOL bEmptySeatAvailable
	
	fDistanceFromClosestVehicle = 99999.0
	CLEAR_CLIENT_BIT(eCLIENTBITSET_I_AM_IN_PREREQUISITE_VEHICLE #IF IS_DEBUG_BUILD , TRUE #ENDIF)
	CLEAR_CLIENT_BIT(eCLIENTBITSET_I_AM_IN_FRONT_RIGHT_OF_PREREQUISITE_VEHICLE #IF IS_DEBUG_BUILD , TRUE #ENDIF)
	CLEAR_CLIENT_BIT(eCLIENTBITSET_I_AM_DRIVER_OF_PREREQUISITE_VEHICLE #IF IS_DEBUG_BUILD , TRUE #ENDIF)
	CLEAR_CLIENT_BIT(eCLIENTBITSET_I_AM_ANY_PASSENGER_OF_PREREQUISITE_VEHICLE #IF IS_DEBUG_BUILD , TRUE #ENDIF)
	
	REPEAT GB_GET_FMBB_NUM_VEH_REQUIRED(GET_FMBB_VARIATION(),GET_FMBB_SUBVARIATION(),FALSE,TRUE,FALSE) iVehicle
	
		MAINTAIN_NON_MISSION_ENTITY_VEHICLE_UNIQUE_BLIP(iVehicle)
		MAINTAIN_DESTROY_UNDRIVEABLE_NON_MISSION_ENTITY_VEHICLE(iVehicle)

		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
			vehID = NET_TO_VEH(serverBD.sVehicle[iVehicle].netId)
			
			MAINTAIN_HAULAGE_TRAILER_DOOR_LOGIC(iVehicle, vehID)
			MAINTAIN_ATTACHING_VEHICLE_LOGIC(iVehicle, vehID)
			MAINTAIN_SHOULD_DESTROY_HELICOPTER(iVehicle)
			MAINTAIN_WARPING_VEHICLES_ON_DELIVERY(iVehicle)
			MAINTAIN_PMSD_FOR_CARRIER_VEHICLES(iVehicle, vehID)
	
			IF NOT IS_VEHICLE_CLIENT_BIT_SET(iVehicle, PARTICIPANT_ID(), eVEHICLECLIENTBITSET_DESTROYED)
				IF GET_VEHICLE_STATE(iVehicle) > eVEHICLESTATE_CREATE
					IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.sVehicle[iVehicle].netId)
						SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_DESTROYED)
					ENDIF
				ENDIF
			ENDIF
			
			IF GET_FMBB_VARIATION() = BBV_SHOWROOM
			AND GET_END_REASON() != eENDREASON_NO_REASON_YET
				IF NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_VEHICLE_HAS_BEEN_ENTERED)
					IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
						NETWORK_FADE_OUT_ENTITY(vehId, TRUE, TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			SWITCH GET_VEHICLE_STATE(iVehicle)
				
				CASE eVEHICLESTATE_INACTIVE

				BREAK
				
				CASE eVEHICLESTATE_CREATE
					
				BREAK
				
				CASE eVEHICLESTATE_DRIVEABLE
				
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
						IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
						AND NOT IS_VEHICLE_DRIVEABLE(vehID)
						AND GET_CLIENT_MODE_STATE() >= eMODESTATE_COLLECT_MISSION_ENTITY
							TRIGGER_HELP(eHELPTEXT_CARRIER_DESTROYED)
						ENDIF
						
						IF GET_END_REASON() != eENDREASON_NO_REASON_YET
						AND IS_ENTITY_ALIVE(vehID)

							MODEL_NAMES eVehModel
							eVehModel = GET_ENTITY_MODEL(vehID)
							IF NOT SHOULD_ALLOW_USE_AFTER_MISSION(eVehModel, iVehicle)
								IF NOT SHOULD_EMPTY_AND_LOCK_VEHICLE_ON_END(eVehModel, iVehicle)
									IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									AND GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) = vehId
										IF SHOULD_USE_PMSD_FOR_VEHICLE(eVehModel)
											GB_SET_GLOBAL_CLIENT_BIT1(eGB_GLOBAL_CLIENT_BITSET_1_INSIDE_POST_MISSION_SELF_DESTRUCT_VEHICLE)
											MPGlobalsAmbience.sPostMissionSelfDestruct.bIgnoreMessages = TRUE
										ELSE
											IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
												SET_VEHICLE_ENGINE_HEALTH(vehId, -1000.0)	// Kill engine if completely empty
											ENDIF
										ENDIF
									ELIF IS_VEHICLE_EMPTY(vehId, TRUE, TRUE)		// Check empty of players
										IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
											IF IS_VEHICLE_EMPTY(vehId, TRUE)		// Check empty of all peds
												SET_VEHICLE_ENGINE_HEALTH(vehId, -1000.0)	// Kill engine if completely empty
											ELIF IS_VEHICLE_AN_AMBUSH_VEHICLE(iVehicle)
												SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehID, TRUE)	// Lock the doors if it's an ambush vehicle and peds are still inside it
											ENDIF
										ENDIF
									ENDIF
								ELSE
									IF EMPTY_AND_LOCK_VEHICLE(vehID, timeLeaveEventVeh[iVehicle], FALSE)
										PRINTLN("[FMBB] [", GET_CURRENT_VARIATION_STRING(), "] - Vehicle #", iVehicle, " has been emptied and locked.")
									ENDIF
									SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehID, TRUE)
								ENDIF
							ELSE
								PRINTLN("[FMBB] [", GET_CURRENT_VARIATION_STRING(), "] - Vehicle #", iVehicle, " is allowed for use after mission. Not locking or starting kill switch.")
							ENDIF
						ENDIF

						IF IS_ENTITY_ALIVE(vehID)
							IF SHOULD_VEHICLE_BE_MADE_DESTROYABLE(iVehicle)	
							AND NOT IS_VEHICLE_CLIENT_BIT_SET(iVehicle, PARTICIPANT_ID(), eVEHICLECLIENTBITSET_MADE_VEHICLE_DESTROYABLE)
								IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
									SET_ENTITY_INVINCIBLE(vehID,FALSE)							
									SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_MADE_VEHICLE_DESTROYABLE)
								ENDIF
							ENDIF
							
							IF IS_VEHICLE_BIT_SET(iVehicle,eVEHICLEBITSET_WARP_VEHICLE_ON_STUCK)
								MAINTAIN_WARPING_STUCK_VEHICLES(serverBD.sVehicle[iVehicle].netId, TRUE) 
							ENDIF

							IF SHOULD_VEHICLE_BE_UNLOCKED_ON_CONDITION(iVehicle)	
							AND (GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(vehId, PLAYER_ID())
							OR GET_VEHICLE_DOOR_LOCK_STATUS(vehId) = VEHICLELOCK_LOCKED)
								SET_VEHICLE_UNLOCK_STATE_FOR_VARIATION(serverBD.sVehicle[iVehicle].netId, vehID)
								SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_I_AM_UNLOCKED)
							ENDIF
							
							IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_RADIO_VEHICLE)
								IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
									TURN_ON_VEHICLE_RADIO(vehID)
									PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_CAR_MEET_VEHICLE_ATTRIBUTES - Turned on vehicle radio for vehicle ",iVehicle)
								ENDIF
							ENDIF
							
							IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehID)
								SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_I_HAVE_ENTERED_VEHICLE)
								IF SHOULD_PEDS_REACT_ON_VEHICLE_ENTRY(iVehicle)
									SET_I_WANT_PEDS_TO_REACT(GET_PED_TO_REACT_ON_VEHICLE_ENTRY(iVehicle),TRUE)
								ENDIF
							ENDIF
							
//							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
//								IF SHOULD_VEHICLE_SPEED_BE_ALTERED(iVehicle,vehID)
//									SET_VEHICLE_MAX_SPEED(vehID,GET_ALTERED_VEHICLE_SPEED())			
//								ELSE
//									SET_VEHICLE_MAX_SPEED(vehID,-1)
//								ENDIF
//							ENDIF
						ENDIF
					
//						IF IS_ENTITY_ALIVE(vehID)
//							IF SHOULD_VARIATION_HAVE_PASSENGER_SEAT_BOMBS()
//								IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehID)
//										IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
//											IF NOT MPGlobalsAmbience.bEnablePassengerBombing
//												MPGlobalsAmbience.bEnablePassengerBombing = TRUE
//												PRINTLN("[FMBB] [", GET_CURRENT_VARIATION_STRING(), "] - Vehicle #", iVehicle, " has enabled passenger bombs")
//											ENDIF
//										ENDIF
//									ENDIF
//								ELSE
//									IF MPGlobalsAmbience.bEnablePassengerBombing
//										MPGlobalsAmbience.bEnablePassengerBombing = FALSE
//										PRINTLN("[FMBB] [", GET_CURRENT_VARIATION_STRING(), "] - Vehicle #", iVehicle, " has disabled passenger bombs")
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
					
						IF IS_VEHICLE_BIT_SET(iVehicle,eVEHICLEBITSET_I_AM_PREREQUISITE_VEHICLE)
							IF IS_NET_VEHICLE_DRIVEABLE(serverBD.sVehicle[iVehicle].netId)
								IF IS_NET_PLAYER_OK(PLAYER_ID(),FALSE)
									fDistanceFromPrerequisiteVehicle = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),vehID)
									IF fDistanceFromPrerequisiteVehicle = fDistanceFromPrerequisiteVehicle // Stop compiler moaning. May be useful later. 
										fDistanceFromPrerequisiteVehicle = fDistanceFromPrerequisiteVehicle
									ENDIF

									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehID)
										SET_CLIENT_BIT(eCLIENTBITSET_I_AM_IN_PREREQUISITE_VEHICLE #IF IS_DEBUG_BUILD , TRUE #ENDIF)
										
										VEHICLE_SEAT seat
										seat = GET_SEAT_PED_IS_IN(PLAYER_PED_ID())
										
										IF seat = VS_DRIVER
											SET_CLIENT_BIT(eCLIENTBITSET_I_AM_DRIVER_OF_PREREQUISITE_VEHICLE #IF IS_DEBUG_BUILD , TRUE #ENDIF)
										ELIF seat = VS_FRONT_RIGHT
											SET_CLIENT_BIT(eCLIENTBITSET_I_AM_IN_FRONT_RIGHT_OF_PREREQUISITE_VEHICLE #IF IS_DEBUG_BUILD , TRUE #ENDIF)
											SET_CLIENT_BIT(eCLIENTBITSET_I_AM_ANY_PASSENGER_OF_PREREQUISITE_VEHICLE #IF IS_DEBUG_BUILD , TRUE #ENDIF)
										ELIF seat = VS_BACK_LEFT
											SET_CLIENT_BIT(eCLIENTBITSET_I_AM_ANY_PASSENGER_OF_PREREQUISITE_VEHICLE #IF IS_DEBUG_BUILD , TRUE #ENDIF)
										ELIF seat = VS_BACK_RIGHT
											SET_CLIENT_BIT(eCLIENTBITSET_I_AM_ANY_PASSENGER_OF_PREREQUISITE_VEHICLE #IF IS_DEBUG_BUILD , TRUE #ENDIF)
										ELIF seat = VS_ANY_PASSENGER
											SET_CLIENT_BIT(eCLIENTBITSET_I_AM_ANY_PASSENGER_OF_PREREQUISITE_VEHICLE #IF IS_DEBUG_BUILD , TRUE #ENDIF)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF	
					
						IF SHOULD_RUN_VEHICLE_DISTANCE_CHECKS()
							IF IS_VEHICLE_VALID_FOR_CLOSEST_CHECK(iVehicle)
								IF IS_NET_VEHICLE_DRIVEABLE(serverBD.sVehicle[iVehicle].netId)
									FLOAT fDistance
									fDistance = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),vehID)
									IF fDistance < fDistanceFromClosestVehicle
										fDistanceFromClosestVehicle = fDistance
										iClosestVehicle = iClosestVehicle // To please release compiler
										iClosestVehicle = iVehicle
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				BREAK
				
				CASE eVEHICLESTATE_NOT_DRIVEABLE
				
					// Lock ambush vehicle when they're cleaned up 
					IF IS_VEHICLE_AN_AMBUSH_VEHICLE(iVehicle)
					AND IS_NET_VEHICLE_DRIVEABLE(serverBD.sVehicle[iVehicle].netId)
						IF GET_VEHICLE_DOOR_LOCK_STATUS(vehID) != VEHICLELOCK_LOCKED
						AND GET_ENTITY_MODEL(vehID) = TECHNICAL
							IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
								SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId), TRUE)
							ENDIF
						ENDIF
					ENDIF
					
				BREAK
				
				CASE eVEHICLESTATE_WARP
					IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
						vehicleSpawnLocationParams.fMinDistFromCoords = 20
						vehicleSpawnLocationParams.fMaxDistance = 100
						vehicleSpawnLocationParams.bConsiderHighways = TRUE
						vehicleSpawnLocationParams.bCheckEntityArea = TRUE
						vehicleSpawnLocationParams.bCheckOwnVisibility = FALSE
						vehicleSpawnLocationParams.bConsiderOnlyActiveNodes = FALSE
						vehID = NET_TO_VEH(serverBD.sVehicle[iVehicle].netId)
						IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(GET_ENTITY_COORDS(vehID), <<0.0, 0.0, 0.0>>, GET_ENTITY_MODEL(vehID), TRUE, vSpawnLocation, fSpawnHeading, vehicleSpawnLocationParams)									  
							NETWORK_FADE_IN_ENTITY(vehID, TRUE)
							SET_ENTITY_COORDS_NO_OFFSET(vehID, vSpawnLocation)
							SET_ENTITY_HEADING(vehID, fSpawnHeading)
							SET_VEHICLE_ON_GROUND_PROPERLY(vehID)
							BROADCAST_EVENT_WARP_BUYSELL_SCRIPT_VEHICLE(iVehicle, TRUE, serverBD.iLaunchPosix)
							PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] eVEHICLESTATE_WARP - Warping vehicle #", iVehicle, " to ", vSpawnLocation, " as local player is delivering in it.")
						ENDIF
					ENDIF
				BREAK
				
				CASE eVEHICLESTATE_NO_LONGER_NEEDED
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
						IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
							CLEANUP_NET_ID(serverBD.sVehicle[iVehicle].netId)
						ENDIF
					ENDIF
				BREAK
				
				CASE eVEHICLESTATE_RESET
					
				BREAK
				
			ENDSWITCH
		ENDIF
	ENDREPEAT
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF bEmptySeatAvailable
			IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_PREREQ_SEATS_AVAILABLE)
				SET_SERVER_BIT(eSERVERBITSET_PREREQ_SEATS_AVAILABLE)
			ENDIF
		ELSE
			IF IS_SERVER_BIT_SET(eSERVERBITSET_PREREQ_SEATS_AVAILABLE)
				CLEAR_SERVER_BIT(eSERVERBITSET_PREREQ_SEATS_AVAILABLE)
			ENDIF
		ENDIF
		ENDIF
	
	fLastDistanceFromClosestVehicle = fDistanceFromClosestVehicle
	fDistanceFromClosestVehicle = fLastDistanceFromClosestVehicle		//Stupid compiler
	
ENDPROC

FUNC BOOL KICK_OUT_VEH()

	RETURN FALSE
ENDFUNC

PROC MAINTAIN_HALTING_PLAYER_VEHICLE()
	IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_PLAYER_HALTED)
		IF SHOULD_HALT_PLAYER_VEHICLE()
			IF BRING_PLAYER_VEHICLE_TO_HALT()
			
				SET_LOCAL_BIT(eLOCALBITSET_PLAYER_HALTED)
				
			ENDIF
			
			IF KICK_OUT_VEH()
				SET_LOCAL_BIT(eLOCALBITSET_KICK_PLAYER_OUT_VEH)
			ENDIF
		ENDIF
	ELSE
		IF NOT SHOULD_HALT_PLAYER_VEHICLE()
			CLEAR_LOCAL_BIT(eLOCALBITSET_PLAYER_HALTED)
		ENDIF
	ENDIF
	
	IF IS_LOCAL_BIT_SET(eLOCALBITSET_KICK_PLAYER_OUT_VEH)
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			CLEAR_LOCAL_BIT(eLOCALBITSET_KICK_PLAYER_OUT_VEH)
		ELSE
			CLEAR_LOCAL_BIT(eLOCALBITSET_KICK_PLAYER_OUT_VEH)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL GET_ANY_OTHER_VEHICLE_AT_COORDS(VECTOR vTruck, NETWORK_INDEX niVeh )
	VEHICLE_INDEX viVeh, viVeh2
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niVeh)
		viVeh = NET_TO_VEH(niVeh)
	ENDIF
	viVeh2 = GET_CLOSEST_VEHICLE(vTruck, 2.0, DUMMY_MODEL_FOR_SCRIPT,
		VEHICLE_SEARCH_FLAG_RETURN_LAW_ENFORCER_VEHICLES 
		| VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES 
		| VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES 
		| VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_GROUP_MEMBERS 
		| VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_PLAYER                                                           
		| VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_DEAD_OR_DYING_PED                                   
		| VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_WITH_PEDS_ENTERING_OR_EXITING                                         
		| VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_SCRIPTED_TASK      
		| VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_NON_DEFAULT_TASK)
	IF DOES_ENTITY_EXIST(viVeh2)
	AND viVeh2 != viVeh
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PREREQUISITE_VEHICE_MOVE_TO_DESTINATION()
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_PREREQUISITE_VEHICE_GO_TO_COORDS()
	RETURN <<0.0,0.0,0.0>>
ENDFUNC

FUNC BOOL IS_HELICOPTER_DAMAGED(VEHICLE_INDEX vehId #IF IS_DEBUG_BUILD , INT iVehicle #ENDIF )
	
	IF NOT IS_VEHICLE_DRIVEABLE(vehId)
		PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_SHOULD_DESTROY_HELICOPTER - IS_HELICOPTER_DAMAGED(", iVehicle, ") - NOT IS_VEHICLE_DRIVEABLE")
		RETURN TRUE
	ENDIF
	
	IF GET_ENTITY_HEALTH(vehId) < 500
		PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_SHOULD_DESTROY_HELICOPTER - IS_HELICOPTER_DAMAGED(", iVehicle, ") - GET_ENTITY_HEALTH <= 500")
		RETURN TRUE
	ENDIF
	
	IF IS_HELI_PART_BROKEN(vehId, TRUE, TRUE, TRUE)
		PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_SHOULD_DESTROY_HELICOPTER - IS_HELICOPTER_DAMAGED(", iVehicle, ") - IS_HELI_PART_BROKEN")
		RETURN TRUE
	ENDIF
	
	IF GET_HELI_TAIL_ROTOR_HEALTH(vehId) <= 30
		PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_SHOULD_DESTROY_HELICOPTER - IS_HELICOPTER_DAMAGED(", iVehicle, ") - GET_HELI_TAIL_ROTOR_HEALTH <= 0")
		RETURN TRUE
	ENDIF
	
	IF GET_HELI_MAIN_ROTOR_HEALTH(vehId) <= 30
		PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_SHOULD_DESTROY_HELICOPTER - IS_HELICOPTER_DAMAGED(", iVehicle, ") - GET_HELI_MAIN_ROTOR_HEALTH <= 0")
		RETURN TRUE
	ENDIF
	
	IF GET_VEHICLE_ENGINE_HEALTH(vehId) <= 30
		PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_SHOULD_DESTROY_HELICOPTER - IS_HELICOPTER_DAMAGED(", iVehicle, ") - GET_VEHICLE_ENGINE_HEALTH <= 0")
		RETURN TRUE
	ENDIF
	
	IF GET_VEHICLE_PETROL_TANK_HEALTH(vehId) <= 30
		PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_SHOULD_DESTROY_HELICOPTER - IS_HELICOPTER_DAMAGED(", iVehicle, ") - GET_VEHICLE_PETROL_TANK_HEALTH <= 0")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_WARPING_STUCK_MISSION_ENTITIES(INT iMissionEntity)
	
	IF (IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_WARP_VEHICLE_ON_STUCK)
	OR SHOULD_WARP_MISSION_ENTITY_ON_STUCK())
	AND NOT HAS_MISSION_ENTITY_BEEN_DELIVERED_OR_DESTROYED(iMissionEntity)
		MAINTAIN_WARPING_STUCK_VEHICLES(serverBD.sMissionEntities.netId[iMissionEntity], TRUE)
	ENDIF
	
ENDPROC

FUNC BOOL HAVE_ALL_TARGET_VEHICLES_BEEN_ELIMINATED()

	SWITCH GET_FMBB_VARIATION()
		CASE BBV_MAX
			INT iVeh
			REPEAT GB_GET_FMBB_NUM_VEH_REQUIRED(GET_FMBB_VARIATION(), GET_FMBB_SUBVARIATION(), TRUE, TRUE, TRUE) iVeh
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVeh].netId)
					IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.sVehicle[iVeh].netId))
						RETURN FALSE
					ENDIF
				ENDIF
			ENDREPEAT
		BREAK
	ENDSWITCH

	RETURN TRUE
ENDFUNC

/*
PROC MAINTAIN_CLEAR_DROP_OFF_AREA_OF_UNDRIVEABLE_VEHICLES()
	
	BOOL bReset
	
	IF NOT HAS_NET_TIMER_STARTED(dropOffBlockedTimerA)
	
		PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - started dropOffBlockedTimerA")
		START_NET_TIMER(dropOffBlockedTimerA)
			
	ELIF HAS_NET_TIMER_EXPIRED(dropOffBlockedTimerA, CHECK_BLOCKED_DROP_OFF_TIME_LIMIT)
		
		IF NOT HAS_NET_TIMER_STARTED(dropOffBlockedTimerB)
		
			PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - dropOffBlockedTimerA expired = ", CHECK_BLOCKED_DROP_OFF_TIME_LIMIT)
			PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - started dropOffBlockedTimerB")
			START_NET_TIMER(dropOffBlockedTimerB)
			vehicleInDropOffA = GET_VEHICLE_BLOCKING_VEHICLE_DROP_OFF()
	
			#IF IS_DEBUG_BUILD
			PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - vehicleInDropOffA = GET_VEHICLE_BLOCKING_VEHICLE_DROP_OFF = ", NATIVE_TO_INT(vehicleInDropOffA))
			IF DOES_ENTITY_EXIST(vehicleInDropOffA)
				PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - vehicleInDropOffA = DOES_ENTITY_EXIST(vehicleInDropOffA) = TRUE")
				IF IS_VEHICLE_DRIVEABLE(vehicleInDropOffA)
					PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - vehicleInDropOffA = IS_VEHICLE_DRIVEABLE(vehicleInDropOffA) = TRUE")
				ELSE
					PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - vehicleInDropOffA = IS_VEHICLE_DRIVEABLE(vehicleInDropOffA) = FALSE")
				ENDIF
			ELSE
				PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - vehicleInDropOffA = DOES_ENTITY_EXIST(vehicleInDropOffA) = FALSE")
			ENDIF
			#ENDIF

		ELIF HAS_NET_TIMER_EXPIRED(dropOffBlockedTimerB, RECHECK_BLOCKED_DROP_OFF_TIME_LIMIT)
			
			PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - dropOffBlockedTimerB expired = ", RECHECK_BLOCKED_DROP_OFF_TIME_LIMIT)
			
			IF DOES_ENTITY_EXIST(vehicleInDropOffA)
				IF NOT IS_VEHICLE_DRIVEABLE(vehicleInDropOffA)
			
					vehicleInDropOffB = GET_VEHICLE_BLOCKING_VEHICLE_DROP_OFF()
					PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - vehicleInDropOffB = GET_VEHICLE_BLOCKING_VEHICLE_DROP_OFF = ", NATIVE_TO_INT(vehicleInDropOffB))
					
					#IF IS_DEBUG_BUILD
					IF DOES_ENTITY_EXIST(vehicleInDropOffB)
						PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - vehicleInDropOffB = DOES_ENTITY_EXIST(vehicleInDropOffB) = TRUE")
						IF IS_VEHICLE_DRIVEABLE(vehicleInDropOffB)
							PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - vehicleInDropOffB = IS_VEHICLE_DRIVEABLE(vehicleInDropOffB) = TRUE")
						ELSE
							PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - vehicleInDropOffB = IS_VEHICLE_DRIVEABLE(vehicleInDropOffB) = FALSE")
						ENDIF
					ELSE
						PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - vehicleInDropOffB = DOES_ENTITY_EXIST(vehicleInDropOffB) = FALSE")
					ENDIF
					#ENDIF
					
					IF DOES_ENTITY_EXIST(vehicleInDropOffB)
						IF (vehicleInDropOffA = vehicleInDropOffB)
							PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - vehicleInDropOffA = vehicleInDropOffB. Calling CLEAR_AREA_LEAVE_VEHICLE_HEALTH(GET_DROP_OFF_COORDS(), GET_DROP_RADIUS(), FALSE, FALSE, FALSE, TRUE)")
							CLEAR_AREA_LEAVE_VEHICLE_HEALTH(GET_DROP_OFF_COORDS(), GET_DROP_RADIUS(), FALSE, FALSE, FALSE, TRUE)
						ENDIF
					ENDIF
					
					PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - done. bReset = TRUE")
					bReset = TRUE
					
				ELSE
		
					PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - IS_VEHICLE_DRIVEABLE(vehicleInDropOffA) = TRUE, cannot be cleared. bReset = TRUE")
					bReset = TRUE
				
				ENDIF
				
			ELSE

				PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] -DOES_ENTITY_EXIST(vehicleInDropOffA) = FALSE, cannot be cleared. bReset = TRUE")
				bReset = TRUE
				
			ENDIF
			
		ENDIF
		
	ENDIF
	
	IF bReset
		PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - bReset = TRUE, doing reset.")
		VEHICLE_INDEX tempVeh
		vehicleInDropOffA = tempVeh
		vehicleInDropOffB = tempVeh
		RESET_NET_TIMER(dropOffBlockedTimerA)
		RESET_NET_TIMER(dropOffBlockedTimerB)
	ENDIF
	
ENDPROC
*/

TIME_DATATYPE timeLeaveEvent
PROC MAINTAIN_PREVENT_VEHICLE_IN_AREA()
	IF SHOULD_PREVENT_VEHICLE_IN_AREA()
		SET_VEHICLE_ENGINE_ON(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), FALSE, FALSE)
		IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 20.0, DEFAULT, DEFAULT, DEFAULT, FALSE, TRUE)
			IF EMPTY_AND_LOCK_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), timeLeaveEvent, FALSE, FALSE)
				PRINTLN("[FMBB] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_PREVENT_VEHICLE_IN_AREA - Slow vehicle for local player and kicked them off.")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

