/// Freemode Lester cutscene
///    Dave W



USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_camera.sch"
USING "commands_hud.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"

// Headers

USING "net_mission.sch"
USING "net_scoring_common.sch"

USING "commands_path.sch"
USING "net_blips.sch"
USING "commands_zone.sch"


USING "hud_drawing.sch"
USING "net_ambience.sch"


USING "shop_public.sch"
USING "net_garages.sch"



USING "net_mission_details_overlay.sch"

USING "net_hud_displays.sch"

USING "fm_hold_up_tut.sch"

USING "net_wait_zero.sch"

#IF IS_DEBUG_BUILD
	USING "profiler.sch"
#ENDIF

// Game States
CONST_INT GAME_STATE_INI 		0
CONST_INT GAME_STATE_INI_SPAWN	1
CONST_INT GAME_STATE_RUNNING	2
CONST_INT GAME_STATE_LEAVE		3
CONST_INT GAME_STATE_FAILED		4
CONST_INT GAME_STATE_TERMINATE_DELAY 5
CONST_INT GAME_STATE_END		6

//MP_MISSION thisMission = eFM_HOLD_UP_TUT

CONST_INT biS_AllPlayersFinished		0

// Everyone can read this data, only the server can update it. 
STRUCT ServerBroadcastData
	INT iServerGameState
	INT iServerBitSet
	INT iPartWithTrevor
	SCRIPT_TIMER timeTerminate
	HOLD_UP_TUT_SERVER_STRUCT holdUpTutServer
ENDSTRUCT
ServerBroadcastData serverBD


INT iServerStaggeredLoopCount

CONST_INT iCarModProgGetCarModded			0


CONST_INT biP_DoneCutscene					0
CONST_INT biP_WantToSeeTrevor				1
CONST_INT biP_WithTrevor					2

STRUCT PlayerBroadcastData
	INT iGameState
	INT iTrevorCutProg
	INT iPlayerBitSet
ENDSTRUCT
PlayerBroadcastData PlayerBD[NUM_NETWORK_PLAYERS]


BOOL bDoMocap = TRUE
INT iBoolsBitSet

CONST_INT biL_ServerLoopedThroughEveryone		0
CONST_INT biL_StaggeredAllPlayersFinished		1
CONST_INT biL_ToldToGoToLester					2
CONST_INT biL_RequestedCut						3
CONST_INT biL_SkippedMocap						4
CONST_INT biL_TextCleared						5
CONST_INT biL_OnMission							6
CONST_INT biL_SkippedCut						7
CONST_INT biL_ToldToLoseCops					8
CONST_INT biL_DoneBlipHelp						9
CONST_INT biL_ReqCutAssets						10
CONST_INT biL_SetEndCutPos						11
CONST_INT biL_ServerStagSomeoneWithTrevor		12
CONST_INT biL_TrevorBusy						13
CONST_INT biL_TrevorBlipInactive				14
CONST_INT biL_ITurnedOffInvites					15
CONST_INT biL_SetAlpha							16


PED_INDEX pedPlayer

//VECTOR g_vTrevorTrailer  = <<1973.6960, 3814.8611, 32.4259>>

//BLIP_INDEX g_blipTrevorTrailer

SCRIPT_TIMER timeSinceOnMission
SCRIPT_TIMER timeMocap

INTERIOR_INSTANCE_INDEX interiorTrailer



#IF IS_DEBUG_BUILD
	BOOL bDisableAllDebug = FALSE
	PROC NET_DW_PRINT(STRING sText)
		IF NOT bDisableAllDebug
			NET_NL() NET_PRINT_TIME() NET_PRINT("[am_RonTrevor_cut] [DSW] ") NET_PRINT_STRINGS(" ", sText) NET_NL()
		ENDIF
	ENDPROC 

	PROC NET_DW_PRINT_STRING_INT(STRING sText1, INT i)
		IF NOT bDisableAllDebug
			NET_NL() NET_PRINT_TIME() NET_PRINT("[am_RonTrevor_cut] [DSW] ") NET_PRINT_STRING_INT(sText1, i) NET_NL()
		ENDIF
	ENDPROC

	PROC NET_DW_PRINT_STRING_FLOAT(STRING sText1, FLOAT f)
		IF NOT bDisableAllDebug
			NET_NL() NET_PRINT_TIME() NET_PRINT("[am_RonTrevor_cut] [DSW] ") NET_PRINT_STRING_FLOAT(sText1, f) NET_NL()
		ENDIF
	ENDPROC

	PROC NET_DW_PRINT_STRING_VECTOR(STRING sText1, VECTOR v)
		IF NOT bDisableAllDebug
			NET_NL() NET_PRINT_TIME() NET_PRINT("[am_RonTrevor_cut] [DSW] ") NET_PRINT_STRING_VECTOR(sText1, v) NET_NL()
		ENDIF
	ENDPROC
	
	PROC NET_DW_PRINT_STRINGS(STRING sText1, STRING sText2)
		IF NOT bDisableAllDebug
			NET_NL() NET_PRINT_TIME() NET_PRINT("[am_RonTrevor_cut] [DSW] ") NET_PRINT_STRINGS(sText1, sText2) NET_NL()
		ENDIF
	ENDPROC
	
	PROC NET_DW_PRINT_STRING_WITH_PLAYER_NAME(STRING sText, PLAYER_INDEX player)
		IF NOT bDisableAllDebug
			NET_NL() NET_PRINT_TIME() NET_PRINT("[am_RonTrevor_cut] [DSW] ") NET_PRINT_STRINGS(" ", sText) NET_PRINT_STRINGS(" ", GET_PLAYER_NAME(player)) NET_NL()
		ENDIF
	ENDPROC
	
	BOOL bWdDoStaggeredDebug
	BOOL bWdCreateRon
	BOOL bWdCreateTrevor

	PROC CREATE_WIDGETS()
		START_WIDGET_GROUP("Ron & Trevor Cut")
			//Profiling widgets
			#IF SCRIPT_PROFILER_ACTIVE
			CREATE_SCRIPT_PROFILER_WIDGET()
			#ENDIF		
			ADD_WIDGET_BOOL("Create Ron", bWdCreateRon)
			ADD_WIDGET_BOOL("Create Trevor", bWdCreateTrevor)
			START_WIDGET_GROUP("Player")
				ADD_BIT_FIELD_WIDGET("Local", iBoolsBitSet)
				ADD_BIT_FIELD_WIDGET("Player", PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet)
				
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Server")
				
				ADD_WIDGET_BOOL("Staggered debug ", bWdDoStaggeredDebug)
				ADD_WIDGET_INT_READ_ONLY("serverbd.iPartWithTrevor", serverbd.iPartWithTrevor)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP() 
	ENDPROC
	
	
	

	
#ENDIF



#IF IS_DEBUG_BUILD
	PROC DO_LESTER_CUTSCENE_J_SKIPS()
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
					NET_DW_PRINT("Player ok for j-skip")
					SWITCH PlayerBD[PARTICIPANT_ID_TO_INT()].iTrevorCutProg	
						CASE 0
							NET_DW_PRINT("Running Case 0 j-skip")
							
							J_SKIP_MP(<<1980.5710, 3816.9031, 31.2725>>, <<1.0, 1.0, 1.0>>)
						BREAK
					ENDSWITCH
				ENDIF
			ENDIF
		ENDIF
		
	ENDPROC
#ENDIF

FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iGameState
ENDFUNC

FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC

FUNC BOOL SHOULD_UI_BE_HIDDEN()
	BOOL bSHouldHide
	RETURN FALSE
		
		IF SHOULD_HIDE_JOB_BLIP(CI_TYPE_M3_HIDE_JOB_MISSION)
			bSHouldHide = TRUE
		ENDIF

	
//	IF bSHouldHide 
//		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HidingEvent)
//			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HidingEvent)
//			PRINTLN("     ---------->     URBAN WARFARE [SHOULD_UI_BE_HIDDEN] SET biP_HidingEvent  <---------- ")
//		ENDIF
//	ELSE
//		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HidingEvent)
//			CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HidingEvent)
//			PRINTLN("     ---------->     URBAN WARFARE [SHOULD_UI_BE_HIDDEN] CLEAR biP_HidingEvent  <---------- ")
//		ENDIF
//	ENDIF
	
	RETURN bSHouldHide
ENDFUNC

/// PURPOSE:
///    Handles the cleanup of the script
PROC CLEANUP_SCRIPT()
	
	
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_DW_PRINT("Running cleanup")
	#ENDIF
	Clear_Any_Objective_Text_From_This_Script()
	SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_TREVOR_CUTSCENE, FALSE)
	
	IF IS_BIT_SET(iBoolsBitSet, biL_ITurnedOffInvites)
		CLEAR_TUTORIAL_INVITES_ARE_BLOCKED() 
		CLEAR_BIT(iBoolsBitSet, biL_ITurnedOffInvites)
	ENDIF

	//SET_FM_UNLOCKS_BIT_SET()
	
	IF DOES_BLIP_EXIST(g_blipTrevorTrailer)
		SET_BLIP_ROUTE(g_blipTrevorTrailer, FALSE)
		REMOVE_BLIP(g_blipTrevorTrailer)
	ENDIF
	
	IF interiorTrailer <> NULL
		UNPIN_INTERIOR(interiorTrailer)
	ENDIF
	
	REMOVE_CUTSCENE()
	
	//TERMINATE_THIS_THREAD()
	//RESET_FMMC_MISSION_VARIABLES(FALSE, FALSE, ciFMMC_END_OF_MISSION_STATUS_PASSED)
	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

/// PURPOSE:
///    Do necessary pre game start ini.
PROC PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission) , missionScriptArgs)
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	// KGM: Wait for the first network broadcast before moving on - call this as the last instruction in pre-game
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		#IF IS_DEBUG_BUILD
			NET_DW_PRINT("Failed to receive an initial network broadcast. Cleaning up.")
		#ENDIF
		CLEANUP_SCRIPT()
	ENDIF
	
	//SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_TREVOR_CUTSCENE, TRUE)
	//SET_TUTORIAL_INVITES_ARE_BLOCKED()

	//SET_FM_UNLOCKS_BIT_SET()

	#IF IS_DEBUG_BUILD
		NET_DW_PRINT("Launching....V2")
		
		IF SHOULD_UI_BE_HIDDEN()
			NET_DW_PRINT("Launching hidden!")
		ENDIF
	#ENDIF
ENDPROC

FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET()
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllPlayersFinished)
		#IF IS_DEBUG_BUILD NET_DW_PRINT("[HAVE_MISSION_END_CONDITIONS_BEEN_MET] True because biS_AllPlayersFinished") #ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


PROC MAINTAIN_SERVER_STAGGERED_LOOP()
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF iServerStaggeredLoopCount = 0
			CLEAR_BIT(iBoolsBitSet, biL_ServerLoopedThroughEveryone)
			SET_BIT(iBoolsBitSet, biL_StaggeredAllPlayersFinished)
			CLEAR_BIT(iBoolsBitSet, biL_ServerStagSomeoneWithTrevor)
			#IF IS_DEBUG_BUILD
				IF bWdDoStaggeredDebug
					NET_DW_PRINT("[ServStag] At Start of server staggered loop")
				ENDIF
			#ENDIF
		ENDIF
		
		
		
		
	
		PARTICIPANT_INDEX part = INT_TO_NATIVE(PARTICIPANT_INDEX, iServerStaggeredLoopCount )
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(part)
			PLAYER_INDEX player = NETWORK_GET_PLAYER_INDEX(part)
			
			IF NOT IS_BIT_SET(PlayerBD[iServerStaggeredLoopCount].iPlayerBitSet, biP_DoneCutscene)
				CLEAR_BIT(iBoolsBitSet, biL_StaggeredAllPlayersFinished)
			ENDIF
			
			IF IS_NET_PLAYER_OK(player, FALSE)
				
				
				IF IS_BIT_SET(PlayerBD[iServerStaggeredLoopCount].iPlayerBitSet, biP_WantToSeeTrevor)	
					SET_BIT(iBoolsBitSet, biL_ServerStagSomeoneWithTrevor)
					IF serverbd.iPartWithTrevor = -1
						IF serverbd.iPartWithTrevor <> iServerStaggeredLoopCount
							serverbd.iPartWithTrevor = iServerStaggeredLoopCount
							#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_WITH_PLAYER_NAME("[ServStag] Setting this player can see Trevor... ", player) #ENDIF
						ENDIF	
					ENDIF
				ELSE
					IF serverbd.iPartWithTrevor = iServerStaggeredLoopCount
						serverbd.iPartWithTrevor = -1
						#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("Setting serverbd.iPartWithTrevor as this part no longer with Trevor ", iServerStaggeredLoopCount) #ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		iServerStaggeredLoopCount++
		IF iServerStaggeredLoopCount = NUM_NETWORK_PLAYERS
			#IF IS_DEBUG_BUILD
				IF bWdDoStaggeredDebug
					NET_DW_PRINT("[ServStag] At End of server staggered loop")
				ENDIF
			#ENDIF
			IF IS_BIT_SET(iBoolsBitSet, biL_StaggeredAllPlayersFinished)
				SET_BIT(serverBD.iServerBitSet, biS_AllPlayersFinished)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[ServStag] Setting biS_AllPlayersFinished because all players finished") #ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(iBoolsBitSet, biL_ServerStagSomeoneWithTrevor)
				IF serverbd.iPartWithTrevor <> -1
					serverbd.iPartWithTrevor = -1
					#IF IS_DEBUG_BUILD NET_DW_PRINT("[ServStag] serverbd.iPartWithTrevor = -1 as no one with TREVOR") #ENDIF
				ENDIF
			ENDIF
			SET_BIT(iBoolsBitSet, biL_ServerLoopedThroughEveryone)
			iServerStaggeredLoopCount = 0
		ENDIF
	ENDIF
ENDPROC	



PROC MAINTAIN_LESTER_CUTSCENE_SERVER()
	
	
	
//	BOOL bFoundAPairing
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
		
	ENDIF
ENDPROC


FUNC BOOL REQUESTED_TREVOR_CUTSCENE()
	STRING sTrevorCut 
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF NOT IS_BIT_SET(iBoolsBitSet, biL_RequestedCut)
		
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), g_vTrevorTrailer ) < 625 // 25m
				IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01 //IS_PLAYER_MALE(PLAYER_ID())
					sTrevorCut = "MP_INTRO_MCS_16_A1"
					#IF IS_DEBUG_BUILD 
						NET_DW_PRINT("Requested male cutscene - model check...")
						IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bAmIMale
							NET_DW_PRINT("[dsw]  bAmIMale is set ")
						ELSE
							NET_DW_PRINT("[dsw]  bAmIMale is NOT set ")
						ENDIF
					#ENDIF
				ELSE
					sTrevorCut = "MP_INTRO_MCS_16_A2"
					#IF IS_DEBUG_BUILD 
						NET_DW_PRINT("Requested female cutscene - model check...")
						IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bAmIMale
							NET_DW_PRINT("[dsw]  bAmIMale is set ")
						ELSE
							NET_DW_PRINT("[dsw]  bAmIMale is NOT set ")
						ENDIF
					#ENDIF
				ENDIF
				REQUEST_CUTSCENE(sTrevorCut)
				interiorTrailer = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<1975.4589, 3819.8835, 32.4501>>, "v_trailer") 
				IF interiorTrailer <> NULL
					PIN_INTERIOR_IN_MEMORY(interiorTrailer)
					#IF IS_DEBUG_BUILD NET_DW_PRINT("Pinned Trevor's interior")#ENDIF
				ENDIF
				SET_BIT(iBoolsBitSet, biL_RequestedCut)
				CLEAR_BIT(iBoolsBitSet, biL_ReqCutAssets)
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRINGS("I have requested cutscene ", sTrevorCut) #ENDIF
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(iBoolsBitSet, biL_ReqCutAssets)
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()

					
					IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01 
						SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_1", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
					ELSE
						SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_Female_Character", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
					ENDIF
						
					SET_BIT(iBoolsBitSet, biL_ReqCutAssets)
					#IF IS_DEBUG_BUILD NET_DW_PRINT("Set biL_ReqCutAssets") #ENDIF
				ENDIF
			ENDIF

			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), g_vTrevorTrailer ) > 1225 // 35m
				REMOVE_CUTSCENE()
				IF interiorTrailer <> NULL
					UNPIN_INTERIOR(interiorTrailer)
				ENDIF
				CLEAR_BIT(iBoolsBitSet, biL_RequestedCut)
				CLEAR_BIT(iBoolsBitSet, biL_ReqCutAssets)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("I have removed Trevor cutscene as too far away") #ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN (IS_BIT_SET(iBoolsBitSet, biL_RequestedCut))
ENDFUNC	

FUNC BOOL SHOULD_TREVOR_CUTSCENE_COMPLETE()
	IF IS_BIT_SET(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneCutscene)
		#IF IS_DEBUG_BUILD NET_DW_PRINT("[SHOULD_TREVOR_CUTSCENE_COMPLETE] True because biP_DoneCutscene") #ENDIF
		RETURN TRUE
	ENDIF
	#IF IS_DEBUG_BUILD 
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S) 
			IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
			AND NOT IS_FM_MISSION_LAUNCH_IN_PROGRESS()
				IF PlayerBD[PARTICIPANT_ID_TO_INT()].iTrevorCutProg < 1
					NET_DW_PRINT("[SHOULD_TREVOR_CUTSCENE_COMPLETE] True because S-Passed")
				 	RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_OK_TO_PRINT_HELP()
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
	OR IS_PED_INJURED(PLAYER_PED_ID())
	OR NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
	OR  IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
	OR IS_SCRIPT_HUD_DISPLAYING(HUDPART_MISSIONBRIEF)
	OR IS_MP_MISSION_DETAILS_OVERLAY_ON_DISPLAY()
	OR IS_SCRIPT_HUD_DISPLAYING(HUDPART_MISSIONBOX)
	OR IS_HELP_MESSAGE_BEING_DISPLAYED()
	OR IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
	OR IS_FM_MISSION_LAUNCH_IN_PROGRESS()
	OR IS_CUSTOM_MENU_ON_SCREEN() 
	OR IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
	OR IS_MISSION_SUMMARY_SCREEN_DISPLAYED()
	OR IS_PAUSE_MENU_ACTIVE()
		RETURN(FALSE)
	ENDIF
	
	
	RETURN(TRUE)
ENDFUNC

FUNC BOOL CREATE_PLAYER_PED_COPY_FOR_TREVOR_CUT()
	IF NOT DOES_ENTITY_EXIST(pedPlayer)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			MODEL_NAMES mPlayer = GET_ENTITY_MODEL(PLAYER_PED_ID())
			pedPlayer = CREATE_PED(PEDTYPE_CIVMALE, mPlayer, <<1972.7494, 3817.5137, 32.4287>>, 26.5312, FALSE, FALSE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedPlayer, TRUE)
			CLONE_PED_TO_TARGET(PLAYER_PED_ID(), pedPlayer)
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(pedPlayer)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PED_INJURED(pedPlayer)
		IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedPlayer)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC CLEANUP_CLONE_PED()
	ENTITY_INDEX ent
	IF DOES_ENTITY_EXIST(pedPlayer)
		ent = pedPlayer
		DELETE_ENTITY(ent)
	ENDIF
ENDPROC


FUNC BOOL HAS_PLAYER_REACHED_TREVORS_TRAILER()
//	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//	//	IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<1972.883301,3813.399414,32.050156>>, <<1975.586670,3814.986816,34.612591>>, 3.250000)
//		IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<1972.407104,3814.872559,31.667412>>, <<1974.340210,3816.052002,35.364933>>, 0.5)
//			RETURN TRUE
//		ENDIF
//	ENDIF
//	
//	RETURN FALSE
	
	IF IS_PED_INJURED(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<1973.7563, 3815.4678, 32.4250>>) > 500
		CLEANUP_CLONE_PED()
		RETURN FALSE
	ENDIF
	
	CREATE_PLAYER_PED_COPY_FOR_TREVOR_CUT()
	
	
	IF NOT IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<1972.407104,3814.872559,31.667412>>, <<1974.340210,3816.052002,35.364933>>, 0.5)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_PLAYER_APPROACHING_TREVORS_TRAILER(FLOAT fDist = 2.0)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), g_vTrevorTrailer ) < fDist * fDist
			RETURN TRUE
		ENDIF
	ENDIF
		
	RETURN FALSE
ENDFUNC




INT iTrevorCutProg
PED_INDEX pedRon

PED_INDEX pedTrevor

CAMERA_INDEX camTrevor
SCRIPT_TIMER timeLester, timeSkipLesterCut
structPedsForConversation speechFmLesterCut

FUNC BOOL CREATE_RON_PED()
	IF NOT DOES_ENTITY_EXIST(pedRon)
		IF HAS_MODEL_LOADED(GET_NPC_PED_MODEL(CHAR_RON))
			IF CREATE_NPC_PED_ON_FOOT(pedRon, CHAR_RON, <<1975.1451, 3820.5938, 32.4422>>, 207.2145   )
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedRon, TRUE)
				ADD_PED_FOR_DIALOGUE(speechFmLesterCut, 1, pedRon, "NervousRon")
			ENDIF
		ENDIF
	ENDIF
	IF NOT DOES_ENTITY_EXIST(pedRon)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PED_INJURED(pedRon)
		IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedRon)
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL CREATE_TREVOR_PED()
	IF NOT DOES_ENTITY_EXIST(pedTrevor)
		IF HAS_MODEL_LOADED(PLAYER_TWO)
			//IF CREATE_NPC_PED_ON_FOOT(pedRon, CHAR_TREVOR, <<1976.8899, 3820.0571, 32.4501>>, 114.8015)
			pedTrevor =  CREATE_PED(PEDTYPE_CIVMALE, PLAYER_TWO, <<1976.8899, 3820.0571, 32.4501>>, 114.8015, FALSE, FALSE)
			SETUP_PLAYER_PED_DEFAULTS_FOR_MP( pedTrevor )
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedTrevor, TRUE)
			ADD_PED_FOR_DIALOGUE(speechFmLesterCut, 2, pedTrevor, "Trevor")
			
		ENDIF
	ENDIF
	IF NOT DOES_ENTITY_EXIST(pedTrevor)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PED_INJURED(pedTrevor)
		IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedTrevor)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC VECTOR CONVERT_ROTATION_TO_DIRECTION_VECTOR(VECTOR vRot)
	RETURN <<-SIN(vRot.z) * COS(vRot.x), COS(vRot.z) * COS(vRot.x), SIN(vRot.x)>>
ENDFUNC

FUNC BOOL CREATE_PLAYER_PED_COPY_FOR_LESTER_CUT()
	IF NOT DOES_ENTITY_EXIST(pedPlayer)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			MODEL_NAMES mPlayer = GET_ENTITY_MODEL(PLAYER_PED_ID())
			pedPlayer = CREATE_PED(PEDTYPE_CIVMALE, mPlayer, <<1974.6202, 3818.4038, 32.4363>>, 317.5041, FALSE, FALSE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedPlayer, TRUE)
			CLONE_PED_TO_TARGET(PLAYER_PED_ID(), pedPlayer)
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(pedPlayer)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PED_INJURED(pedPlayer)
		IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedPlayer)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL DO_RON_TREVOR_SCRIPTED_CUT()
	//PLAYER_SPAWN_LOCATION eSpawnLoc = SPAWN_LOCATION_NEAR_SPECIFIC_COORDS
	PLAYER_SPAWN_LOCATION eSpawnLoc = SPAWN_LOCATION_CUSTOM_SPAWN_POINTS
	SWITCH iTrevorCutProg
		CASE 0
			IF IS_PLAYER_OK_TO_START_MP_CUTSCENE()	
				
				IF NOT IS_SCREEN_FADED_OUT()
				AND NOT IS_SCREEN_FADING_OUT()
					
					DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
					
				ENDIF
				interiorTrailer = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<1975.4589, 3819.8835, 32.4501>>, "v_trailer") 
				REQUEST_MODEL(PLAYER_TWO)
				REQUEST_MODEL(GET_NPC_PED_MODEL(CHAR_RON))
				#IF IS_DEBUG_BUILD
					IF interiorTrailer = NULL
						NET_DW_PRINT("[DO_RON_TREVOR_SCRIPTED_CUT] interiorTrailer is NULL!")
					ENDIF
				#ENDIF
			ENDIF
			iTrevorCutProg = 1
			#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_RON_TREVOR_SCRIPTED_CUT] iTrevorCutProg = ", iTrevorCutProg) #ENDIF
		BREAK
		
		CASE 1
			IF IS_SCREEN_FADED_OUT()
				MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(FALSE)
				START_MP_CUTSCENE()		
				IF interiorTrailer <> NULL
					PIN_INTERIOR_IN_MEMORY(interiorTrailer)
					#IF IS_DEBUG_BUILD NET_DW_PRINT("Pinned Lester's interior")#ENDIF
				ENDIF
				 
				NEW_LOAD_SCENE_START(<<1972.0815, 3818.2559, 34.6237>>,  CONVERT_ROTATION_TO_DIRECTION_VECTOR( <<-16.1168, 0.0000, -71.1871>>),29.9136)
				iTrevorCutProg = 2
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_RON_TREVOR_SCRIPTED_CUT] iTrevorCutProg = ", iTrevorCutProg) #ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF IS_NEW_LOAD_SCENE_LOADED()
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(),  <<1971.8622, 3816.4172, 32.4287>>)
				ENDIF
				iTrevorCutProg = 3
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_RON_TREVOR_SCRIPTED_CUT] iTrevorCutProg = ", iTrevorCutProg) #ENDIF
			ELSE	
				#IF IS_DEBUG_BUILD NET_DW_PRINT("Waiting for Lester Load scene") #ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF CREATE_RON_PED()
			AND CREATE_TREVOR_PED()
			AND CREATE_PLAYER_PED_COPY_FOR_LESTER_CUT()
				START_NET_TIMER(timeSkipLesterCut)
				camTrevor = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE) 
				SET_CAM_ACTIVE(camTrevor, TRUE)								
				SET_CAM_PARAMS(camTrevor,   <<1972.2659, 3817.8455, 34.6237>>, <<-12.7971, -0.0000, -64.4780>>,  29.9136)
				SET_CAM_PARAMS(camTrevor,  <<1971.9852, 3818.5693, 34.6237>>, <<-12.7971, -0.0000, -77.6239>>, 29.9136, 30000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)										
				
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				NEW_LOAD_SCENE_STOP()
				
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				
				RESET_NET_TIMER(timeLester)
				
				iTrevorCutProg = 4
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_RON_TREVOR_SCRIPTED_CUT] iTrevorCutProg = ", iTrevorCutProg) #ENDIF
			ENDIF
		BREAK
		
		CASE 4
			IF CREATE_CONVERSATION (speechFmLesterCut, "FM_1AU", "FM_RT1", CONV_PRIORITY_MEDIUM)
				iTrevorCutProg = 5
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_RON_TREVOR_SCRIPTED_CUT] iTrevorCutProg = ", iTrevorCutProg) #ENDIF
			ENDIF
			
		BREAK
		
		CASE 5 
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			#IF IS_DEBUG_BUILD OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT) #ENDIF
				KILL_FACE_TO_FACE_CONVERSATION()
				iTrevorCutProg = 6
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_RON_TREVOR_SCRIPTED_CUT] iTrevorCutProg = ", iTrevorCutProg) #ENDIF
			ENDIF
		BREAK
		
		CASE 6
			IF CREATE_CONVERSATION (speechFmLesterCut, "FM_1AU", "FM_RT2", CONV_PRIORITY_MEDIUM)
				iTrevorCutProg = 7
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_RON_TREVOR_SCRIPTED_CUT] iTrevorCutProg = ", iTrevorCutProg) #ENDIF
			ENDIF
		BREAK
		
		CASE 7
			IF NOT HAS_NET_TIMER_STARTED(timeLester)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				#IF IS_DEBUG_BUILD OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT)#ENDIF
					KILL_FACE_TO_FACE_CONVERSATION()
					START_NET_TIMER(timeLester)
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(timeLester, 2000)
					//SETUP_SPECIFIC_SPAWN_LOCATION(<<1980.7352, 3816.7178, 31.2861>>, 282.4282, 50.0, TRUE)
					//-- Check skip as well!
					USE_CUSTOM_SPAWN_POINTS(TRUE) 
					ADD_CUSTOM_SPAWN_POINT(<<1976.4690, 3814.7078, 32.4271>>,291.9446)
					ADD_CUSTOM_SPAWN_POINT(<<1979.6758, 3816.6128, 31.3143>>,305.2479)
					ADD_CUSTOM_SPAWN_POINT(<<1984.6074, 3815.3240, 31.2693>>,209.0251)
					ADD_CUSTOM_SPAWN_POINT(<<1984.7838, 3820.3938, 31.3561>>,214.7469)
					ADD_CUSTOM_SPAWN_POINT(<<1983.7036, 3822.4080, 31.3691>>,184.6451)
					//	ADD_CUSTOM_SPAWN_POINT(<<>>, )
					//	ADD_CUSTOM_SPAWN_POINT(<<>>, )
					//	ADD_CUSTOM_SPAWN_POINT(<<>>, )
					DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
					iTrevorCutProg = 8
					#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_RON_TREVOR_SCRIPTED_CUT] iTrevorCutProg = ", iTrevorCutProg) #ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 8
			IF IS_SCREEN_FADED_OUT()
			//	IF NET_WARP_TO_COORD(<<1980.7352, 3816.7178, 31.2861>>, 282.4282)
				IF WARP_TO_SPAWN_LOCATION(eSpawnLoc, FALSE)
					iTrevorCutProg = 99
					#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_RON_TREVOR_SCRIPTED_CUT] iTrevorCutProg = ", iTrevorCutProg) #ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 99
			IF IS_SCREEN_FADED_OUT()
				KILL_FACE_TO_FACE_CONVERSATION()
				IF interiorTrailer <> NULL
					UNPIN_INTERIOR(interiorTrailer)
				ENDIF
				
				CLEANUP_MP_CUTSCENE()
				IF DOES_ENTITY_EXIST(pedRon)
					ENTITY_INDEX ent 
					ent = pedRon
					DELETE_ENTITY(ent)
				ENDIF
				
				IF DOES_ENTITY_EXIST(pedTrevor)
					ENTITY_INDEX ent 
					ent = pedTrevor
					DELETE_ENTITY(ent)
				ENDIF
				IF DOES_ENTITY_EXIST(pedPlayer)
					ENTITY_INDEX ent 
					ent = pedPlayer
					DELETE_ENTITY(ent)
				ENDIF
			//	CLEAR_SPECIFIC_SPAWN_LOCATION()
				USE_CUSTOM_SPAWN_POINTS(FALSE) 
				CLEAR_CUSTOM_SPAWN_POINTS()
				
				DESTROY_ALL_CAMS()
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				iTrevorCutProg = 100
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_RON_TREVOR_SCRIPTED_CUT] iTrevorCutProg = ", iTrevorCutProg) #ENDIF
			ENDIF
		BREAK
		
		CASE 100
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	IF NOT IS_BIT_SET(iBoolsBitSet, biL_SkippedCut)
		IF iTrevorCutProg > 4
		AND iTrevorCutProg < 8
			IF IS_SCREEN_FADED_IN()
				IF HAS_NET_TIMER_STARTED(timeSkipLesterCut)
					IF HAS_NET_TIMER_EXPIRED(timeSkipLesterCut, 500)
						IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
							DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
							KILL_FACE_TO_FACE_CONVERSATION()
							SET_BIT(iBoolsBitSet, biL_SkippedCut)
						//	SETUP_SPECIFIC_SPAWN_LOCATION(<<1980.7352, 3816.7178, 31.2861>>, 282.4282, 50.0, TRUE)
							USE_CUSTOM_SPAWN_POINTS(TRUE) 
							ADD_CUSTOM_SPAWN_POINT(<<1976.4690, 3814.7078, 32.4271>>,291.9446)
							ADD_CUSTOM_SPAWN_POINT(<<1979.6758, 3816.6128, 31.3143>>,305.2479)
							ADD_CUSTOM_SPAWN_POINT(<<1984.6074, 3815.3240, 31.2693>>,209.0251)
							ADD_CUSTOM_SPAWN_POINT(<<1984.7838, 3820.3938, 31.3561>>,214.7469)
							ADD_CUSTOM_SPAWN_POINT(<<1983.7036, 3822.4080, 31.3691>>,184.6451)
							iTrevorCutProg = 8
							#IF IS_DEBUG_BUILD NET_DW_PRINT("Cutscene skipped!") #ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MANAGE_PLAYER_TALKING()
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableVoiceDrivenMouthMovement , TRUE)
		
		IF NETWORK_PLAYER_HAS_HEADSET(PLAYER_ID())
			IF NETWORK_IS_PLAYER_TALKING(PLAYER_ID()) 
			//	SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_EnableVoiceDrivenMouthMovement, TRUE)
			//	#IF IS_DEBUG_BUILD NET_DW_PRINT("Talking!") #ENDIF
			ENDIF 
		ENDIF 
	ENDIF
	
ENDPROC



BOOL bEndCutsceneSkipped
FUNC BOOL DO_TREVOR_MOCAP()
	
	//STRING sTrevorCut 
	SWITCH iTrevorCutProg

		
		CASE 0
			IF HAS_PLAYER_REACHED_TREVORS_TRAILER()
				IF IS_BIT_SET(iBoolsBitSet, biL_ReqCutAssets)
					IF HAS_CUTSCENE_LOADED()
					AND CREATE_PLAYER_PED_COPY_FOR_TREVOR_CUT()
					//	SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
						SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW_IF_NEEDED() // 1854634
						
						IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01
							REGISTER_ENTITY_FOR_CUTSCENE(pedPlayer, "MP_1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ELSE
							REGISTER_ENTITY_FOR_CUTSCENE(pedPlayer, "MP_Female_Character", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						
						
						
						
						START_CUTSCENE()
						// (BOOL bLeavePedCopyBehind=TRUE, BOOL bPlayerInvisible = TRUE, BOOL bPlayerHasCollision = TRUE, BOOL bPlayerFrozen = FALSE
						
						NETWORK_SET_VOICE_ACTIVE(FALSE)
						iTrevorCutProg++
						#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_TREVOR_MOCAP] iTrevorCutProg = ", iTrevorCutProg) #ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD NET_DW_PRINT("Want to start Trevor cut, but biL_ReqCutAssets not set!") #ENDIF
				ENDIF
			ELSE
				IF NOT IS_PLAYER_APPROACHING_TREVORS_TRAILER()
					PlayerBD[PARTICIPANT_ID_TO_INT()].iTrevorCutProg = 0
					#IF IS_DEBUG_BUILD NET_DW_PRINT("iTrevorCutProg = 0 as I've reached trevor's trailer, but walked away") #ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF IS_CUTSCENE_PLAYING()
				START_NET_TIMER(timeMocap)
				// (BOOL bLeavePedCopyBehind=TRUE, BOOL bPlayerInvisible = TRUE, BOOL bPlayerHasCollision = TRUE, BOOL bPlayerFrozen = FALSE
				MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(FALSE, FALSE, TRUE, FALSE )
		
				//  bool bHasControl, BOOL bVisible = TRUE, BOOL bClearTasks = TRUE, BOOL bHasCollision = FALSE
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE | NSPC_CLEAR_TASKS )

				SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(FALSE, TRUE)
				
				NETWORK_FADE_OUT_ENTITY(PLAYER_PED_ID(), TRUE, TRUE)
				START_MP_CUTSCENE(TRUE)
						
				SET_BIT(iBoolsBitSet, biL_SetAlpha)
				
				IF DOES_BLIP_EXIST(g_blipTrevorTrailer)
					SET_BLIP_ROUTE(g_blipTrevorTrailer, FALSE)
					REMOVE_BLIP(g_blipTrevorTrailer)
				ENDIF
				iTrevorCutProg++
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_TREVOR_MOCAP] iTrevorCutProg = ", iTrevorCutProg) #ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF IS_CUTSCENE_PLAYING()
				MANAGE_PLAYER_TALKING()

			ELSE
				iTrevorCutProg++
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_TREVOR_MOCAP] iTrevorCutProg = ", iTrevorCutProg) #ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF bEndCutsceneSkipped
				IF NOT IS_CUTSCENE_ACTIVE()
					IF IS_SCREEN_FADED_OUT()
					OR IS_SCREEN_FADING_OUT()
						DO_SCREEN_FADE_IN(500)
					ENDIF
					bEndCutsceneSkipped = FALSE
				ENDIF
			ELSE
				IF HAS_CUTSCENE_FINISHED()
//					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1974.2528, 3813.3198, 32.4271>>)
//					SET_ENTITY_HEADING(PLAYER_PED_ID(), 293.6957)
//										
//					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
//					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					
					CLEANUP_CLONE_PED()
					CLEANUP_MP_CUTSCENE()
					IF interiorTrailer <> NULL
						UNPIN_INTERIOR(interiorTrailer)
					ENDIF
					NETWORK_SET_VOICE_ACTIVE(TRUE)
					#IF IS_DEBUG_BUILD NET_DW_PRINT("NETWORK_SET_VOICE_ACTIVE - TRUE") #ENDIF
					//SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					CLEAR_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WantToSeeTrevor)
					
					REQUEST_SYSTEM_ACTIVITY_TYPE_MET_TREVOR()
					
					iTrevorCutProg++
					#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_TREVOR_MOCAP] iTrevorCutProg = ", iTrevorCutProg) #ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 4
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	
	
	IF NOT IS_BIT_SET(iBoolsBitSet, biL_SetEndCutPos)
	//	IF IS_PLAYER_MALE(PLAYER_ID())
		IF iTrevorCutProg > 1
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MP_1")
//				OR CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Female_MP_Character")
//		//		OR HAS_NET_TIMER_EXPIRED(timeMocap, 5000)
//					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1974.2528, 3813.3198, 32.4271>>)
//					SET_ENTITY_HEADING(PLAYER_PED_ID(), 293.6957)
//					SET_BIT(iBoolsBitSet, biL_SetEndCutPos)
//					RESET_ENTITY_ALPHA(PLAYER_PED_ID())
//					CLEAR_BIT(iBoolsBitSet, biL_SetAlpha)
//					
//					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
//					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
//					#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_TREVOR_MOCAP] Set player pos at cutscene end") #ENDIF
//				ELSE
//					#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_TREVOR_MOCAP] Else 4") #ENDIF
//				ENDIF
				IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01 
					 IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MP_1")
			             //Do stuff.
			            IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							CLEANUP_CLONE_PED()
							NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
								NETWORK_FADE_IN_ENTITY(PLAYER_PED_ID(), TRUE)
								FORCE_PLAYER_LOCALLY_VISIBLE_FOR_FADE()
							SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1974.2528, 3813.3198, 32.4271>>)
							SET_ENTITY_HEADING(PLAYER_PED_ID(), 293.6957)
							SET_BIT(iBoolsBitSet, biL_SetEndCutPos)
							RESET_ENTITY_ALPHA(PLAYER_PED_ID())
							CLEAR_BIT(iBoolsBitSet, biL_SetAlpha)
							
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
							SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
							#IF IS_DEBUG_BUILD NET_DW_PRINT("Set player pos at cutscene end") #ENDIF
						ENDIF
					ENDIF
				ELSE
					 IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MP_Female_Character")
			             //Do stuff.
			            IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							CLEANUP_CLONE_PED()
							NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
								NETWORK_FADE_IN_ENTITY(PLAYER_PED_ID(), TRUE)
								FORCE_PLAYER_LOCALLY_VISIBLE_FOR_FADE()
							SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1974.2528, 3813.3198, 32.4271>>)
							SET_ENTITY_HEADING(PLAYER_PED_ID(), 293.6957 )
							SET_BIT(iBoolsBitSet, biL_SetEndCutPos)
							RESET_ENTITY_ALPHA(PLAYER_PED_ID())
							CLEAR_BIT(iBoolsBitSet, biL_SetAlpha)
							
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
							SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
							#IF IS_DEBUG_BUILD NET_DW_PRINT("Set player pos at cutscene end") #ENDIF
						ELSE
							#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_TREVOR_MOCAP] Waiting for CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY") #ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_TREVOR_MOCAP] Else 3") #ENDIF
			ENDIF
		ELSE	
			#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_TREVOR_MOCAP] waiting for iTrevorCutProg") #ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_TREVOR_MOCAP] Else 1") #ENDIF
	ENDIF
	
//	IF IS_BIT_SET(iBoolsBitSet, biL_SetAlpha)
//		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//			SET_ENTITY_ALPHA(PLAYER_PED_ID(), 255, FALSE)
//		//	#IF IS_DEBUG_BUILD NET_DW_PRINT("SET_ENTITY_ALPHA...") #ENDIF
//		ENDIF
//	ENDIF
	
	
	RETURN FALSE
ENDFUNC
PROC PROCESS_RON_TREVOR_CUTSCENE_CLIENT()	
	#IF IS_DEBUG_BUILD
		DO_LESTER_CUTSCENE_J_SKIPS()
	#ENDIF
	SWITCH PlayerBD[PARTICIPANT_ID_TO_INT()].iTrevorCutProg
		CASE 0
			IF NOT IS_BIT_SET(iBoolsBitSet, biL_ToldToGoToLester)
				IF NOT IS_BIT_SET(iBoolsBitSet, biL_TextCleared)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_CELLPHONE_CONVERSATION_PLAYING() //IS_MOBILE_PHONE_CALL_ONGOING()
							IF NOT Is_MP_Comms_Still_Playing()
								IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
									IF NOT IS_PHONE_ONSCREEN()
										IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
											IF NOT IS_FM_MISSION_LAUNCH_IN_PROGRESS()
												IF GET_LOCAL_PLAYER_CORONA_POS_STATE() = 0
												//	Print_Objective_Text("FM_RCUT_GOT") // Go to Lester's ~y~house.
//													g_blipTrevorTrailer = ADD_BLIP_FOR_COORD(g_vTrevorTrailer )
//												//	SET_BLIP_ROUTE(g_blipTrevorTrailer, TRUE)
//													SET_BLIP_SPRITE(g_blipTrevorTrailer, RADAR_TRACE_TREVOR_FAMILY)
//													SET_BLIP_FLASHES(g_blipTrevorTrailer, TRUE)
//													SET_BLIP_FLASH_TIMER(g_blipTrevorTrailer, 7000)
													SET_BIT(iBoolsBitSet, biL_ToldToGoToLester)
											//		#IF IS_DEBUG_BUILD NET_DW_PRINT("Added Lester house blip") #ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(iBoolsBitSet, biL_TextCleared)
				
				IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
				OR IS_FM_MISSION_LAUNCH_IN_PROGRESS()
				OR GET_LOCAL_PLAYER_CORONA_POS_STATE() > 0
				OR SHOULD_UI_BE_HIDDEN() 
					SET_BIT(iBoolsBitSet, biL_TextCleared)
					Clear_Any_Objective_Text_From_This_Script()
					IF DOES_BLIP_EXIST(g_blipTrevorTrailer)
						SET_BLIP_ROUTE(g_blipTrevorTrailer, FALSE)
						REMOVE_BLIP(g_blipTrevorTrailer)
						
					ENDIF
					
					IF SHOULD_UI_BE_HIDDEN()
						IF IS_BIT_SET(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WantToSeeTrevor)
							CLEAR_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WantToSeeTrevor)
							SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_TREVOR_CUTSCENE, FALSE)
							CLEAR_TUTORIAL_INVITES_ARE_BLOCKED()
							#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing biP_WantToSeeTrevor as SHOULD_UI_BE_HIDDEN") #ENDIF
						ENDIF
					ENDIF		
					#IF IS_DEBUG_BUILD NET_DW_PRINT("Setting biL_TextCleared as player on mission / launch in progress / SHOULD_UI_BE_HIDDEN") #ENDIF
					
				ELSE	
				//	IF REQUESTED_LESTER_CUTSCENE()
				//	ENDIF
					
				//	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
						IF IS_BIT_SET(iBoolsBitSet, biL_ToldToLoseCops)	
							CLEAR_BIT(iBoolsBitSet, biL_ToldToLoseCops)
						ENDIF
						IF DOES_BLIP_EXIST(g_blipTrevorTrailer)
							IF NOT IS_BIT_SET(iBoolsBitSet, biL_TrevorBlipInactive)					
								IF serverbd.iPartWithTrevor <> -1
									IF serverbd.iPartWithTrevor <> PARTICIPANT_ID_TO_INT()
										SET_BLIP_COLOUR(g_blipTrevorTrailer, BLIP_COLOUR_INACTIVE_MISSION)
										SET_BLIP_SCALE(g_blipTrevorTrailer, BLIP_SIZE_NETWORK_PED)
										SET_BIT(iBoolsBitSet, biL_TrevorBlipInactive)
										#IF IS_DEBUG_BUILD NET_DW_PRINT("Setting biL_TrevorBlipInactive as Trevor busy") #ENDIF
									ENDIF
								ENDIF
							ELSE
								IF serverbd.iPartWithTrevor = -1
								OR serverbd.iPartWithTrevor = PARTICIPANT_ID_TO_INT()
									SET_BLIP_COLOUR(g_blipTrevorTrailer ,BLIP_COLOUR_DEFAULT)
									SET_BLIP_SCALE(g_blipTrevorTrailer, BLIP_SIZE_NETWORK_COORD)
									CLEAR_BIT(iBoolsBitSet, biL_TrevorBlipInactive)
									#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing biL_TrevorBlipInactive as Trevor no longer busy") #ENDIF
								ENDIF
							ENDIF
						ENDIF
												
					//	IF HAS_PLAYER_REACHED_TREVORS_TRAILER()
						IF IS_PLAYER_APPROACHING_TREVORS_TRAILER()
							IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0

								IF NOT IS_BIT_SET(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WantToSeeTrevor)
									SET_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WantToSeeTrevor)
									#IF IS_DEBUG_BUILD NET_DW_PRINT("Setting I want to see Trevor") #ENDIF
								ELSE
									IF serverbd.iPartWithTrevor = PARTICIPANT_ID_TO_INT()
									
										Clear_Any_Objective_Text_From_This_Script()

										SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_TREVOR_CUTSCENE, TRUE)
										SET_TUTORIAL_INVITES_ARE_BLOCKED()
										SET_BIT(iBoolsBitSet, biL_ITurnedOffInvites)
										CLEAR_HELP()

										
										PlayerBD[PARTICIPANT_ID_TO_INT()].iTrevorCutProg = 1
										#IF IS_DEBUG_BUILD NET_DW_PRINT("iTrevorCutProg = 1 as I've reached trevor's trailer") #ENDIF
									ELSE
										
										IF serverbd.iPartWithTrevor <> -1
											IF NOT IS_BIT_SET(iBoolsBitSet, biL_TrevorBusy)
												IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
													PRINT_HELP("FM_RCUT_TRB")
													#IF IS_DEBUG_BUILD NET_DW_PRINT("Doing Trevor busy help") #ENDIF
													SET_BIT(iBoolsBitSet, biL_TrevorBusy)
												ELSE
													#IF IS_DEBUG_BUILD NET_DW_PRINT("Else 1") #ENDIF
												ENDIF
											ELSE
												#IF IS_DEBUG_BUILD NET_DW_PRINT("Else 2") #ENDIF
											ENDIF
										ELSE
											#IF IS_DEBUG_BUILD NET_DW_PRINT("Else 1") #ENDIF
										ENDIF
									ENDIF
								ENDIF
								
							ELSE
								IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
									IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_RCUT_LCP")
										PRINT_HELP("FM_RCUT_LCP") // Lose the cops
									ENDIF
								ENDIF
							ENDIF
						ELSE
							//-- Blip help
							IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneBlipHelp)
								IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
									IF NOT IS_TRANSITION_ACTIVE()
										PRINT_HELP("FM_RCUT_TBLP") // TP Industries ~BLIP_TREVOR_FAMILY~ is marked on the Map. 
										g_blipTrevorTrailer = ADD_BLIP_FOR_COORD(g_vTrevorTrailer )
										SET_BLIP_SPRITE(g_blipTrevorTrailer, RADAR_TRACE_TREVOR_FAMILY)
										SET_BLIP_FLASHES(g_blipTrevorTrailer, TRUE)
										SET_BLIP_FLASH_TIMER(g_blipTrevorTrailer, 7000)
										SET_BIT(iBoolsBitSet, biL_DoneBlipHelp)
										#IF IS_DEBUG_BUILD NET_DW_PRINT("Set biL_DoneBlipHelp") #ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							IF IS_BIT_SET(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WantToSeeTrevor)
								CLEAR_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WantToSeeTrevor)
								SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_TREVOR_CUTSCENE, FALSE)
								CLEAR_TUTORIAL_INVITES_ARE_BLOCKED()
								#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing biP_WantToSeeTrevor as I'm no longer at Trevor's") #ENDIF
							ENDIF
							
							IF IS_BIT_SET(iBoolsBitSet, biL_TrevorBusy)

								#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing biL_TrevorBusy as I'm not at Trevor's") #ENDIF
								CLEAR_BIT(iBoolsBitSet, biL_TrevorBusy)
								
							ENDIF
						ENDIF
//					ELSE
//						IF NOT IS_BIT_SET(iBoolsBitSet, biL_ToldToLoseCops)						
//							SET_BIT(iBoolsBitSet, biL_ToldToLoseCops)
//							CLEAR_BIT(iBoolsBitSet, biL_ToldToGoToLester)
//						//	Print_Objective_Text("FM_IHELP_LCP")
//							IF DOES_BLIP_EXIST(g_blipTrevorTrailer)
//								SET_BLIP_ROUTE(g_blipTrevorTrailer, FALSE)
//								REMOVE_BLIP(g_blipTrevorTrailer)
//							ENDIF
//							#IF IS_DEBUG_BUILD NET_DW_PRINT("doing lose cops objective") #ENDIF
//						ENDIF
//					ENDIF
				ENDIF
				
			ELSE
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
						IF IS_SCREEN_FADED_IN()
							IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
							AND NOT IS_FM_MISSION_LAUNCH_IN_PROGRESS()
							AND GET_LOCAL_PLAYER_CORONA_POS_STATE() = 0
							AND NOT SHOULD_UI_BE_HIDDEN()
								IF NOT HAS_NET_TIMER_STARTED(timeSinceOnMission)
									START_NET_TIMER(timeSinceOnMission)
									#IF IS_DEBUG_BUILD NET_DW_PRINT("Started timeSinceOnMission ") #ENDIF
								ELSE
									IF HAS_NET_TIMER_EXPIRED(timeSinceOnMission, 10)
										#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing biL_TextCleared as player control is on") #ENDIF
										
										CLEAR_BIT(iBoolsBitSet, biL_TextCleared)
										CLEAR_BIT(iBoolsBitSet, biL_ToldToGoToLester)
										CLEAR_BIT(iBoolsBitSet, biL_RequestedCut)
										CLEAR_BIT(iBoolsBitSet, biL_ToldToLoseCops)
										IF NOT DOES_BLIP_EXIST(g_blipTrevorTrailer)
											g_blipTrevorTrailer = ADD_BLIP_FOR_COORD(g_vTrevorTrailer )
											SET_BLIP_SPRITE(g_blipTrevorTrailer, RADAR_TRACE_TREVOR_FAMILY)
											#IF IS_DEBUG_BUILD NET_DW_PRINT("Re-adding Trevor Blip") #ENDIF
										ENDIF
										RESET_NET_TIMER(timeSinceOnMission)
										
										
										
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF bDoMocap
				IF NOT IS_BIT_SET(iBoolsBitSet, biL_TextCleared)
					REQUESTED_TREVOR_CUTSCENE()
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF bDoMocap
				IF DO_TREVOR_MOCAP()
					PlayerBD[PARTICIPANT_ID_TO_INT()].iTrevorCutProg = 2
					#IF IS_DEBUG_BUILD NET_DW_PRINT("iTrevorCutProg = 2 as Mocap finished") #ENDIF
				ENDIF
			ELSE
				IF DO_RON_TREVOR_SCRIPTED_CUT()
					IF IS_SCREEN_FADED_IN()
						PlayerBD[PARTICIPANT_ID_TO_INT()].iTrevorCutProg = 2
						#IF IS_DEBUG_BUILD NET_DW_PRINT("iTrevorCutProg = 2 as cutscene finished") #ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
	
			PlayerBD[PARTICIPANT_ID_TO_INT()].iTrevorCutProg = 3
			#IF IS_DEBUG_BUILD NET_DW_PRINT("iTrevorCutProg = 3 as cutscene is finished") #ENDIF
		BREAK
		
		CASE 3
			IF NOT IS_BIT_SET(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneCutscene)
				SET_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneCutscene)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("Set biP_DoneCutscene as cutscene finished") #ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	IF SHOULD_TREVOR_CUTSCENE_COMPLETE()
		#IF IS_DEBUG_BUILD
			g_bDebugLaunchTrevorCut = FALSE
		#ENDIF
		IF DOES_BLIP_EXIST(g_blipTrevorTrailer)
			REMOVE_BLIP(g_blipTrevorTrailer)
		ENDIF
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_RON))
		SET_MODEL_AS_NO_LONGER_NEEDED(PLAYER_TWO)
		Clear_Any_Objective_Text_From_This_Script()
		IF bDoMocap
			REMOVE_CUTSCENE()
		ENDIF
		//INT iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_CUT_DONE)
		INT iStatInt = g_MP_STAT_FM_CUT_DONE[GET_SLOT_NUMBER(-1)]
		SET_BIT(iStatInt, biFmCut_TrevorCut)
		SET_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneCutscene)
		SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_FM_CUT_DONE, iStatInt)
//		SET_FREEMODE_PROLOGUE_PROFILE_SETTINGS(PSFP_FM_TREVOR_CUTSCENE)
		SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_TREVOR_CUTSCENE, FALSE)
		CLEAR_TUTORIAL_INVITES_ARE_BLOCKED() 
		SET_FM_UNLOCKS_BIT_SET()
//		REQUEST_SAVE(STAT_SAVETYPE_PROLOGUE)

	ENDIF
ENDPROC





#IF IS_DEBUG_BUILD
	PROC UPDATE_WIDGETS()
		
		IF bWdCreateRon
			REQUEST_MODEL(GET_NPC_PED_MODEL(CHAR_RON))
			IF CREATE_RON_PED()
				bWdCreateRon = FALSE
			ENDIF
		ENDIF
		
		IF bWdCreateTrevor
			REQUEST_MODEL(PLAYER_TWO)
			IF CREATE_TREVOR_PED()
				bWdCreateTrevor= FALSE
			ENDIF
		ENDIF
	ENDPROC
	
	PROC UPDATE_DEBUG_KEYS()
		
	ENDPROC
#ENDIF
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                          ///////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

SCRIPT(MP_MISSION_DATA missionScriptArgs)
	
	
	PROCESS_PRE_GAME(missionScriptArgs)	
	
	#IF IS_DEBUG_BUILD
		CREATE_WIDGETS()	
	#ENDIF
	
	
	
	WHILE TRUE
	
		MP_LOOP_WAIT_ZERO()
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			SCRIPT_PROFILER_START_OF_FRAME()
		#ENDIF
		#ENDIF		
		
		

		// If we have a match end event, bail.
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
	//	OR NOT IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
		#IF IS_DEBUG_BUILD NET_DW_PRINT("Running cleanup (1)") #ENDIF
			CLEANUP_SCRIPT()
		ENDIF	
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER( "Cleanup checks")
		#ENDIF
		#ENDIF 
		
		#IF IS_DEBUG_BUILD
			UPDATE_WIDGETS()
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER( "UPDATE_WIDGETS")
			#ENDIF
			
			UPDATE_DEBUG_KEYS()
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER( "UPDATE_DEBUG_KEYS")
			#ENDIF
		#ENDIF
		
		
// *****************************************************************************************************************************************************************	
// *****************************************************************************************************************************************************************	
//												LOCAL PROCESSING
// *****************************************************************************************************************************************************************	
// *****************************************************************************************************************************************************************
		
		
		// -----------------------------------
		// Process your game logic.....
		SWITCH GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT())
			
			// Wait untill the server gives the all go before moving on
			CASE GAME_STATE_INI
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING
					#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_INI >  GAME_STATE_RUNNING") #ENDIF
					
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					
					#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_INI >  GAME_STATE_TERMINATE_DELAY") #ENDIF
					
				ENDIF
			BREAK
			
			// Main gameplay state.
			CASE GAME_STATE_RUNNING
				
				
				
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING	
					IF NOT HAS_PLAYER_COMPLETED_RON_AND_TREVOR_CUTSCENE(PLAYER_ID())
					#IF IS_DEBUG_BUILD OR g_bDebugLaunchTrevorCut #ENDIF
						PROCESS_RON_TREVOR_CUTSCENE_CLIENT()
					ELSE	
						IF DOES_BLIP_EXIST(g_blipTrevorTrailer)
							Clear_Any_Objective_Text_From_This_Script()
						
							SET_BLIP_ROUTE(g_blipTrevorTrailer, FALSE)
							REMOVE_BLIP(g_blipTrevorTrailer)
							
							SET_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneCutscene)
						ENDIF
						
					ENDIF
					
					//-- Cleanup if we're critical on new Ambeinet events
					IF IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
						MPGlobalsAmbience.bInitTrevorCutLaunch = FALSE
						playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_LEAVE
						#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_RUNNING >  GAME_STATE_LEAVE as IS_PLAYER_CRITICAL_TO_ANY_EVENT") #ENDIF
						
					ENDIF
		
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_RUNNING >  GAME_STATE_TERMINATE_DELAY") #ENDIF
				ENDIF
				
				
				//-- Leave if we start a mission
		/*		IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_RUNNING > GAME_STATE_END as on mission ") #ENDIF
					
				ENDIF
		*/						
			BREAK
			
			CASE GAME_STATE_TERMINATE_DELAY 
				SET_MISSION_FINISHED(serverBD.timeTerminate)
		      	IF IS_MISSION_READY_TO_CLEANUP(serverBD.timeTerminate)
		            playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_TERMINATE_DELAY >  GAME_STATE_END") #ENDIF
		      	ENDIF
			BREAK
			
			// The game stage the local player is placed when we want him to leave a mission
			CASE GAME_STATE_LEAVE

				playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
				#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_LEAVE >  GAME_STATE_END") #ENDIF

			FALLTHRU
			
			//Cleans up then terminates the mission
			CASE GAME_STATE_END
				#IF IS_DEBUG_BUILD NET_DW_PRINT("About to run cleanup from GAME_STATE_END") #ENDIF
				CLEANUP_SCRIPT()
			BREAK

		ENDSWITCH
		
	 	

		
// *****************************************************************************************************************************************************************	
// *****************************************************************************************************************************************************************	
//												SERVER PROCESSING
// *****************************************************************************************************************************************************************	
// *****************************************************************************************************************************************************************	
		
		
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			
			SWITCH GET_SERVER_MISSION_STATE()
				
				
				CASE GAME_STATE_INI
					
					serverBD.iServerGameState = GAME_STATE_RUNNING
					serverBD.iPartWithTrevor = -1
					#IF IS_DEBUG_BUILD NET_DW_PRINT("serverBD.iServerGameState = GAME_STATE_RUNNING") #ENDIF
						
				BREAK
				
				// Look for game end conditions
				CASE GAME_STATE_RUNNING
					MAINTAIN_SERVER_STAGGERED_LOOP()
					MAINTAIN_LESTER_CUTSCENE_SERVER()
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
						ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_LESTER_CUTSCENE_SERVER")
					#ENDIF
					#ENDIF		

					IF HAVE_MISSION_END_CONDITIONS_BEEN_MET()
						serverBD.iServerGameState = GAME_STATE_END
						#IF IS_DEBUG_BUILD NET_DW_PRINT ("serverBD.iServerGameState GAME_STATE_RUNNING > GAME_STATE_END ")#ENDIF
						
					ENDIF
					
				BREAK
				
				CASE GAME_STATE_END
				BREAK
				
			ENDSWITCH
			
		ENDIF
		
		
		
		#IF IS_DEBUG_BUILD 
			#IF SCRIPT_PROFILER_ACTIVE
				SCRIPT_PROFILER_END_OF_FRAME()
			#ENDIF
		#ENDIF			
	
	ENDWHILE

ENDSCRIPT

