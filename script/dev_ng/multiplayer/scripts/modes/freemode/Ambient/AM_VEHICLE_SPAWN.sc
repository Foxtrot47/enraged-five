//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_VEHICLE_SPAWN.sc																			//
// Description: Controls the vehicle spawning for rank vehicles and bought vehicles							//
// Written by:  David Gentles																				//
// Date: 16/04/2013																							//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "net_blips.sch"

USING "freemode_header.sch"

USING "net_vehicle_spawn.sch"

USING "net_contact_requests.sch"

USING "net_wait_zero.sch"

USING "website_public.sch"

USING "net_luxe_veh_activities.sch"

USING "net_gang_boss_common.sch"

#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
USING "profiler.sch"
#ENDIF

USING "net_personal_vehicle_menu.sch"
USING "net_office.sch"

USING "hacker_truck_scanner.sch"
USING "net_drone.sch"

///////////////////////////////////
///    		CONSTANTS    		///
///////////////////////////////////
VECTOR VECTOR_ZERO = <<0,0,0>>

// Game States. 
CONST_INT GAME_STATE_INI 											0
CONST_INT GAME_STATE_RUNNING										1
CONST_INT GAME_STATE_LEAVE											2
CONST_INT GAME_STATE_TERMINATE_DELAY								3
CONST_INT GAME_STATE_END											4

CONST_INT NUMBER_OF_SPAWN_REQUESTS									5
CONST_INT NUMBER_OF_SPAWN_ATTEMPTS									2

CONST_FLOAT APPROACH_DISTANCE_FOR_SPAWN_POINT_SQR					22500.0//150sqr
CONST_FLOAT LEAVE_DISTANCE_FOR_SPAWN_POINT_SQR						40000.0//200sqr
CONST_FLOAT SPAWN_DISTANCE_CHECK_SQR								22500.0//150sqr


CONST_FLOAT APPROACH_DISTANCE_FOR_NEAR_VEHICLE_SQR					40000.0//200sqr
CONST_FLOAT LEAVE_DISTANCE_FOR_NEAR_VEHICLE_SQR						62500.0//250sqr
CONST_FLOAT DISTANCE_FOR_EXPENSIVE_CHECK_SQR						36.0//6sqr
CONST_FLOAT DISTANCE_FOR_AT_CREATED_LOCATION_SQR					2500.0//50sqr
CONST_FLOAT DISTANCE_FOR_VEHICLE_TO_BE_STOLEN_SQR					40000.0//200sqr
 
CONST_FLOAT SPAWN_DISTANCE_FOR_LARGE_VEHICLE_SQR					90000.0//250sqr

CONST_INT TIME_VEHICLE_DELIVERY_USED								2880000*5//48minutes*5, 5 days ingames
CONST_INT TIME_VEHICLE_DELIVERY_ABANDONED							120000//2min
CONST_INT TIME_TO_COLLECT_BOOKING									300000//5min
CONST_INT TIME_WANTED_LEVEL											1440000//24minutes, 12hours ingame
CONST_INT TIME_UNTIL_SPAWN_ABANDON									10000//10sec
CONST_INT TIME_TO_FORCE_SPAWN_FAIL									10000//10sec
CONST_INT TIME_TO_DELAY_TEXT_MESSAGE								10000//10sec

CONST_INT TIME_FOR_BLIP_BLINK										10000
CONST_INT TIME_FOR_VEHICLE_SPAWN_COOLDOWN							500
BOOL bTempLuxeBool

LUXE_ACT_GEN_STATE luxActGenerationState
TWEAK_INT ciLUXE_CLEANUP_TIME_OUT									(3 * 1000)


///////////////////////////////////
///    		STRUCT 		   		///
///////////////////////////////////


///////////////////////////////////
///    		SERVER BD    		///
///////////////////////////////////
//iBitSet

/// PURPOSE: The server broadcast data. Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData
	INT iServerGameState
	INT iBitset = 0
		
	INT iPlayerThatOwnsSpawnPoint[NUMBER_OF_VEHICLE_SPAWN_POINTS]
	
	TIME_DATATYPE timePlayerSpawnAbandon[NUM_NETWORK_PLAYERS]
	
	SCRIPT_TIMER MissionTerminateDelayTimer
	
	INT luxeVehInstanceID
	PLAYER_INDEX LuxeVehIDPlayerRequest
	INT iLuxeInstanceBS
	INT objIDStoredVehicles[NUM_NETWORK_PLAYERS]
ENDSTRUCT
ServerBroadcastData serverBD

///////////////////////////////////
///    		PLAYER BD    		///
///////////////////////////////////


CONST_INT REQUEST_BS_ACTIVE											0
CONST_INT REQUEST_BS_IS_BOOKING										1
CONST_INT REQUEST_BS_RESERVED										2
CONST_INT REQUEST_BS_IGNORE_SIGHT_CHECKS							3
CONST_INT BS_BUNKER_FADE_OUT_FOR_TRUCK_CAB_RENOVATE					4
CONST_INT BS_OWNER_RENOVATED_TRUCK_CAB								5

/// PURPOSE: Data required for requesting a vehicle to be spawned
STRUCT VEHICLE_SPAWN_REQUEST
	INT iSpawnPointID = -1
	INT iBitset = 0
	INT iFailCounter = 0
	MODEL_NAMES mnSpawnModel = DUMMY_MODEL_FOR_SCRIPT
ENDSTRUCT

//iBitSet

/// PURPOSE: The client broadcast data. Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	INT iGameState
	INT iBitset = 0
	INT iAtSpawnPointBitset[NUMBER_OF_BITSETS_REQUIRED_FOR_VEHICLE_SPAWN_POINTS]
	
	VEHICLE_SPAWN_REQUEST spawnRequest[NUMBER_OF_SPAWN_REQUESTS]
	INT iNumberOfSpawnRequestsActive = 0
	INT objIDRequestingVehicle = -1
	BOOL playerWantsLuxeVehID
	BOOL playerHasTakenLuxeVehID
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]


///////////////////////////////////
///    		LOCAL BITSET   		///
///////////////////////////////////
CONST_INT PHONE_BS_CALL_SETUP														0
CONST_INT PHONE_BS_CALL_RECEIVED													1
CONST_INT PHONE_BS_CALL_BOSS_LIMO													2
   
INT iLocalPhoneBitset = 0 


CONST_INT PHONECALL_BS_REQUEST_DELIVERY_FAIL										0
CONST_INT PHONECALL_BS_REQUEST_DELIVERY_SUCCESS										1
INT iLocalPhonecallBitset = 0


CONST_INT TEXTMESSAGE_BS_REQUEST_DELIVERY_COLLECTED									0
CONST_INT TEXTMESSAGE_BS_REQUEST_DELIVERY_STOLEN									1
CONST_INT TEXTMESSAGE_BS_REQUEST_DELIVERY_DESTROYED									2
CONST_INT TEXTMESSAGE_BS_REQUEST_DELIVERY_UNUSED									3
CONST_INT TEXTMESSAGE_BS_REQUEST_BOOKING_RETURN_WARNING								4
CONST_INT TEXTMESSAGE_BS_REQUEST_BOOKING_WANTED										5
CONST_INT TEXTMESSAGE_BS_CLEANUP_MESSAGE_BYPASSED									6

INT iLocalTextMessageBitset = 0


CONST_INT LOCAL_BS_QUICK_GPS_ON														0
CONST_INT LOCAL_BS_SPAWNING_VEHICLE													1
CONST_INT LOCAL_BS_LESTER_VEHICLE_DEAD												2
CONST_INT LOCAL_BS_BOSS_LIMO														3

INT iLocalBitset = 0

INT iGivenWarningBS
INT iInRemoteVehBS


VECTOR vLastQuickGPS

//bookedVehicle.iBitset
CONST_INT BOOK_BS_BOOKED											0
CONST_INT BOOK_BS_VEHICLE_EXISTS									1
CONST_INT BOOK_BS_BLIP_CREATED_FOR_VEHICLE							2
CONST_INT BOOK_BS_BLIP_CREATED_FOR_SPAWN_POINT						3
CONST_INT BOOK_BS_NEAR_TO_VEHICLE									4
CONST_INT BOOK_BS_REQUEST_MADE										5
CONST_INT BOOK_BS_DELIVERY											6
CONST_INT BOOK_BS_NEED_TO_RESET_TIMER								7
CONST_INT BOOK_BS_HAS_BEEN_BLIPPED									8
CONST_INT BOOK_BS_DELIVERY_COUNTDOWN_SET							9
CONST_INT BOOK_BS_WARNED_TO_RETURN									10
CONST_INT BOOK_BS_EXPECTING_WANTED_LEVEL							11
CONST_INT BOOK_BS_RECEIVED_WANTED_LEVEL								12
CONST_INT BOOK_BS_THEFT_REPORTED									13
CONST_INT BOOK_BS_BOSS_LIMO											14
CONST_INT BOOK_BS_NOT_SPAWNED_BY_SCRIPT								15
CONST_INT BOOK_BS_PEGASUS_VEH_FADED_OUT								16
CONST_INT BOOK_BS_PEGASUS_VEH_WARPED								17

INT iLocalBS
CONST_INT LOCAL_BS_FORCE_TRUCK_TO_ROOM								0
CONST_INT LOCAL_BS_I_AM_OWNER_OF_TRUCK_RENOVATING_CAB				1
CONST_INT LOCAL_BS_SLOW_DOWN_TRUCK_IN_BUNKER						2
CONST_INT LOCAL_BS_CHANGE_TRAILER_TURRET_AFTER_RENOVATE				3
CONST_INT LOCAL_BS_CLEAN_UP_BEFORE_EXITING_BUNKER					4
CONST_INT LOCAL_BS_RESET_ARMORY_TRUCK_VISIBILITY					5
CONST_INT LOCAL_BS_CLEAN_UP_TRAILER_AFTER_DESTROYED					6
CONST_INT LOCAL_BS_OSPREY_NEAREST_RESERVED							7
CONST_INT LOCAL_BS_OSPREY_NEAREST_RETAINED							8
CONST_INT LOCAL_BS_CLEAN_UP_OSPREY_AFTER_DESTROYED					9
CONST_INT LOCAL_BS_RESET_ARMORY_AIRCRAFT_VISIBILITY					10
CONST_INT LOCAL_BS_AUTO_PILOT_HELP_TEXT								11
CONST_INT LOCAL_BS_DEACTIVATE_AUTO_PILOT_HELP_TEXT					12
CONST_INT LOCAL_BS_CLEAR_SEAT_SHUFFLE								13
CONST_INT LOCAL_BS_PRINT_HACKER_TRUCK_ENTRY_HELP					14
CONST_INT LOCAL_BS_CLEAN_UP_HACKERTRUCK_AFTER_DESTROYED				15
CONST_INT LOCAL_BS_DISPLAY_VEHICLE_NAME_WARNING						16
CONST_INT LOCAL_BS_VEHICLE_REWARD_SCRIPT_READY						17
CONST_INT LOCAL_BS_SET_ISLAND_HEIST_VEH_RADIO						19
CONST_INT LOCAL_BS_SHOULD_ALLOW_EXPENSIVE_PROCESSING				20
CONST_INT LOCAL_BS_HAS_PLAYER_STARTED_AVENGER_SEAT_SHUFFLE			21
CONST_INT LOCAL_BS_SUBMARINE_FIRST_REVEAL							22
#IF FEATURE_DLC_2_2022
CONST_INT LOCAL_BS_CLEAN_UP_ACIDLAB_AFTER_DESTROYED					23
#ENDIF

OBJECT_INDEX droneTargetObjectDamaged
CAMERA_INDEX avengerCockPitCam

CONST_INT FORCE_BS_RESERVED										0

/// PURPOSE: Data required to book a vehicle
STRUCT BOOKED_VEHICLE
	INT iBitset = 0
	INT iForceSpawnVehicleBitset = 0
	INT iRequestID = -1
	INT iSpawnPointID = -1
	BLIP_INDEX blipID
	TIME_DATATYPE timeCountdown
	TIME_DATATYPE timeWanted
	SCRIPT_TIMER timeForceSpawnFail
	SCRIPT_TIMER timeDelayTextMessage
	VEHICLE_REQUEST_TYPE vrType
	MODEL_NAMES mnBookModel = DUMMY_MODEL_FOR_SCRIPT
	//NETWORK_INDEX netIndex  // NeilF: moved the net id to GlobalplayerBD[iStaggerPlayer].netID_PegV, this is so we can cleanup/lock the vehicle when player leaves sesssion, to prevent car duping. see 1742386
	VECTOR vCreatedCoords
	SITE_BUYABLE_VEHICLE ePegasusBuyableVehicle = UNSET_BUYABLE_VEHICLE
ENDSTRUCT

BOOKED_VEHICLE bookedVehicle

//VEHICLE_INDEX vehCreate
NETWORK_INDEX	  vehCreate

CONST_INT LOCAL_EXPENSIVE_BS_CREATED												0

INT iLocalExpensiveBitset = 0

INT iLocalServerPlayerRequest[NUM_NETWORK_PLAYERS][NUMBER_OF_SPAWN_REQUESTS]

INT iRemoteVehicleStagger
INT iPersonalVehicleRemoteControlConceal
///////////////////////////////////
///    		VARIABLES    		///
///////////////////////////////////

TIME_DATATYPE timeNet

structPedsForConversation sSpeech

//Local only
//client
INT iLocalClientSpawnPointStagger = 0
INT iLocalClientRequestSpawnPointBitset[NUMBER_OF_BITSETS_REQUIRED_FOR_VEHICLE_SPAWN_POINTS]

BOOL bLocalClientCurrentlySpawningRequests[NUMBER_OF_SPAWN_REQUESTS]

SCRIPT_TIMER iLocalClientSpawningTimer[NUMBER_OF_SPAWN_REQUESTS]
CONST_INT CLIENT_SPAWNING_TIME	5000

SCRIPT_TIMER iLocalKickOutTimer
CONST_INT LOCAL_KICK_OUT_TIME	10000

//server
INT iLocalServerPlayerStagger = 0
INT iLocalServerSpawnPointStagger = 0
INT iLocalServerSpawnPointCooldown[NUMBER_OF_VEHICLE_SPAWN_POINTS]

// Widgets & Debug.
#IF IS_DEBUG_BUILD
	BOOL bDebugServerDeclaration = FALSE
	PLAYER_INDEX playerLastRecordedHost
	BOOL bDebugClientHostDeclaration = FALSE
	BOOL bDebugDrawSpawnPoints = FALSE
	BOOL bDebugSpawnLastVehicle = FALSE
	BOOL bDebugDrawMapSpawnPoints = FALSE
	BOOL bAdditionalSpew = FALSE
	BOOL bForceFailSpawn = FALSE
	BLIP_INDEX bDebugMapSpawnPoints[NUMBER_OF_VEHICLE_SPAWN_POINTS]
	
	BOOL bGrabAvengerCockPitCameraCoords = FALSE
	BOOL bStartDebugCam = FALSE
	VECTOR vAvengerCamCoord, vAvengerCamRotation
	FLOAT fAvengerFOV
	CAMERA_INDEX cockpitDebugCam
	FLOAT db_CentreX = 0.4
	FLOAT db_CentreY = 0.045
	FLOAT db_Width = 0.8
	FLOAT db_Height = 0.90
	BOOL db_bTestScaleform
//	BOOL bSpawnLuxor2
//INT iLuxeSeatWidget = -1

	#IF FEATURE_HEIST_ISLAND
	BOOL bTestCamCoords
	BOOL bTestCamRunning
	BOOL bGrabCamCoordsAsOffset
	VECTOR vCamPosAsOffset
	VECTOR vCamRotAsOffset
	FLOAT fCamFov
	FLOAT fCamShake	
	BOOL bFMSubmarineDebug_ResetFirstReveal
	BOOL bFMSub_Destroy
	
	BOOL bFMSub_TestNewCollisionDetection
	VEHICLE_INDEX DummySub
	VECTOR vDummySubPos
	FLOAT fDummySubHeading
	
	BOOL bCallRequestSubWarp
	VECTOR vRequestSubWarpCoords
	FLOAT fRequestSubWarpHeading
	
	BOOL bCallForceDive
	FLOAT fCallForceDiveDepth	
	#ENDIF

#ENDIF

INT iRevealHintType = ENUM_TO_INT(HINTTYPE_ARM2_MCS6_VEHICLE)
VECTOR vRevealOffset = <<0,0,4.0>>
FLOAT fRevealDist = 250.0
FLOAT fRevealDepth = -15.0

BOOL bStartedDMInAnotherPlayersPegasus
BOOL bHasDoneDMCheck
BOOL bLocalPlayerInsideAnotherPlayersPegasus
VEHICLE_INDEX StoredPegVehicleID
TIME_DATATYPE timeEmptyPegVehicle

LUXE_ACT_STRUCT luxeActState
//INT myLuxeVehInstanceID = -1
INT iLuxeVehDoorStateStaggeredLoop = 0			// Staggered counter for checking vehicle door states
SCRIPT_TIMER timeLuxeInstanceMonitor
INT iLuxeInstanceCheckBS[NUM_NETWORK_PLAYERS]

INT iCreationOfOfficeHelicopterStage
MODEL_NAMES modelNameOfficeHelicopter

INT iArmoryVehicleCamBS
INT iHackerTruckVehicleCamBS
CONST_INT BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_STARTED							1
CONST_INT BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_START_DONE						2
CONST_INT BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_LOOP								3
CONST_INT BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_STOP								4
CONST_INT BS_ARMORY_AIRCRAFT_CAM_SHAKE_ENGINE_STARTED						5
CONST_INT BS_ARMORY_AIRCRAFT_CAM_SHAKE_ENGINE_START_DONE					6
CONST_INT BS_ARMORY_AIRCRAFT_CAM_SHAKE_ENGINE_LOOP							7
CONST_INT BS_ARMORY_AIRCRAFT_CAM_SHAKE_ENGINE_STOP							8

FLOAT fTurretTimeModifier = 2.4

CONST_INT GUNRUN_TOTAL_MOC_TURRETS 3
VECTOR OLDoffsetVelocity

INT iVehicleNetTracker = 0

INT outsidesoundid = -1
INT hackerTruckSoundid = -1
#IF IS_DEBUG_BUILD
BOOL bForceMobileIsStuck
BOOL bPrintAvengerAutoPilot
BOOL bUpdateMOCDamageScale, bUpdateSubmarineDamageScale
#ENDIF

STRUCT PARTY_BUS_SCALEFORM
	OBJECT_INDEX theProp[NUM_NETWORK_PLAYERS]
	SCALEFORM_INDEX theMovie
	INT iLinkBS
	INT iLoadBS
	INT iRenderTargeID = -1
ENDSTRUCT
PARTY_BUS_SCALEFORM partyBusScaleform

CONST_INT PARTY_BUS_SCALEFORM_BS_REQUESTED_MOVIE				0
CONST_INT PARTY_BUS_SCALEFORM_BS_REQUESTED_MODEL				1
CONST_INT PARTY_BUS_SCALEFORM_BS_REGISTERED_RENDER_TARGET		2

SCRIPT_CONTROL_HOLD_TIMER sControlHold
SCRIPT_CONTROL_HOLD_TIMER sHackerTruckControlHold

#IF FEATURE_HEIST_ISLAND
INT iSubmarineCalmID[NUM_NETWORK_PLAYERS]
VECTOR vSubmarineCalmWaterCoord[NUM_NETWORK_PLAYERS]
INT iTotalNumCalmZone
#ENDIF

#IF FEATURE_CASINO
SCRIPT_TIMER sVehRewardScriptRunTimer

STRING sVehRewardScriptName
THREADID vehRewardThread
#ENDIF

//Hacker truck scanner.
//HACKER_TRUCK_SCANNER_STRUCT m_hackerTruckScanner

/// PURPOSE:
///    Returns if a specified coordinate is within an event blocking area from the current mission struct
/// PARAMS:
///    vCoords - coordinates to check
/// RETURNS:
///    true/false
FUNC BOOL CHECK_VECTOR_FOR_EVENT_BLOCKING(VECTOR vCoords)
	INT i
	IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MISSION)
		FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfZones-1)
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0])
			
				IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iType = ciFMMC_ZONE_TYPE__CLEAR_VEHICLES
				OR g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iType = ciFMMC_ZONE_TYPE__CLEAR_ALL
					
					IF vCoords.x >= g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0].x
					AND vCoords.x <= g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1].x
						IF vCoords.y >= g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0].y
						AND vCoords.y <= g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1].y
							RETURN FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Returns if the specified player can use the specified vehicle spawn point
/// PARAMS:
///    thisPlayer - player ID to check
///    iVSID - vehicle spawn point ID to check
/// RETURNS:
///    true/false
FUNC BOOL CAN_PLAYER_USE_VEHICLE_SPAWN_POINT(PLAYER_INDEX thisPlayer, INT iVSID)
	IF g_fmmc_struct.iRootContentIDHash = g_sMPTUNABLES.iroot_id_HASH_The_Pacific_Standard_Job
	OR g_fmmc_struct.iRootContentIDHash = g_sMPTUNABLES.iroot_id_HASH_The_Pacific_Standard_Job2
	OR g_fmmc_struct.iRootContentIDHash = g_sMPTUNABLES.iroot_id_HASH_The_Pacific_Standard_Job2a 
	OR GB_GET_MEGA_BUSINESS_VARIATION_PLAYER_IS_ON(PLAYER_ID()) = MBV_COLLECT_DJ_HOOKED
		IF iVSID = 4		//Puerto Del Sol Heliport
		OR iVSID = 5		//Airport Helipad
		OR iVSID = 6		//Paleto Police Station Heli
		OR iVSID = 7		//Alamo Sea Helipad
		OR iVSID = 16		//Grapeseed Helipad
		OR iVSID = 24		//Port of LS Helipad
		OR iVSID = 44		//Central Hospital Helipad
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF GB_GET_GB_CASINO_VARIATION_PLAYER_IS_ON(thisPlayer) = CSV_STAFF_PROBLEMS
		IF iVSID = 14		//Airport Upper
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF GB_GET_GB_CASINO_VARIATION_PLAYER_IS_ON(thisPlayer) = CSV_HIGH_ROLLER
		IF iVSID = 8		//Alamo Sea Airstrip
			RETURN FALSE
		ENDIF
	ENDIF
	
	#IF FEATURE_CASINO_HEIST
	IF GB_GET_CASINO_HEIST_PREP_PLAYER_IS_ON(thisPlayer) = CHV_MERRYWEATHER_TEST_SITE
		IF iVSID = 12		//Grand Senora Desert
			RETURN FALSE
		ENDIF
	ENDIF
	#ENDIF
	
	IF GET_PLAYER_FM_RANK(thisPlayer) >= GET_RANK_REQUIRED_FOR_VEHICLE(GET_VEHICLE_SPAWN_POINT_MODEL(iVSID, 0))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gets the number of vehicle models that are unlocked at a specified vehicle spawn point at a specified rank
/// PARAMS:
///    iRank - rank to check
///    iVSID - vehicle spawn point ID to check
/// RETURNS:
///    number of vehicles as INT
FUNC INT GET_NUMBER_OF_VEHICLES_UNLOCKED_AT_SPAWN_POINT_AT_RANK(INT iRank, INT iVSID)
	INT iVehicleCount = 0
	INT i
	
	REPEAT GET_NUMBER_OF_VEHICLES_AVAILABLE_AT_SPAWN_POINT(iVSID) i
		IF iRank >= GET_RANK_REQUIRED_FOR_VEHICLE(GET_VEHICLE_SPAWN_POINT_MODEL(iVSID, i))
			iVehicleCount++
		ENDIF
	ENDREPEAT
	
	RETURN iVehicleCount
ENDFUNC

/// PURPOSE:
///    Gets the vehicle model that will spawn at a specified vehicle spawn point for a specified player based on their rank
/// PARAMS:
///    thisPlayer - player to check
///    iVSID - vehicle spawn point ID to check
/// RETURNS:
///    vehicle model to spawn with as a MODEL_NAMES
FUNC MODEL_NAMES GET_VEHICLE_SPAWN_POINT_MODEL_TO_SPAWN_FOR_PLAYER(PLAYER_INDEX thisPlayer, INT iVSID)
	
	SET_RANDOM_SEED(GET_GAME_TIMER())
	
	RETURN GET_VEHICLE_SPAWN_POINT_MODEL(iVSID, GET_RANDOM_INT_IN_RANGE(0, GET_NUMBER_OF_VEHICLES_UNLOCKED_AT_SPAWN_POINT_AT_RANK(GET_PLAYER_FM_RANK(thisPlayer), iVSID)))
	
ENDFUNC

/// PURPOSE:
///    Gets a ticker text label based on the type of vehicle requested, and whether or not the request was successful
/// PARAMS:
///    thisVRT - vehicle request type to get label for
///    bSuccess - whether or not the request was successful
/// RETURNS:
///    ticker text label as a string
FUNC STRING GET_TICKER_LABEL_FOR_VEHICLE_REQUEST_TYPE(VEHICLE_REQUEST_TYPE thisVRT, BOOL bSuccess)
	IF bSuccess
		SWITCH thisVRT
			CASE VRT_BOAT
				RETURN "VS_TCK_LB" //"VS_TCK_10"//~s~Located a ~b~boat.
			BREAK
			CASE VRT_HELI
				RETURN "VS_TCK_LH" //"VS_TCK_11"//~s~Located a ~b~heli.
			BREAK
			CASE VRT_CAR
				RETURN "VS_TCK_LC" //"VS_TCK_12"//~s~Located a ~b~car.
			BREAK
			CASE VRT_PLANE
				RETURN "VS_TCK_LP" //"VS_TCK_13"//~s~Located a ~b~plane.
			BREAK
			CASE VRT_BLIMP
				RETURN "VS_TCK_LZ" //"VS_TCK_11"//~s~Located a ~b~blimp.
			BREAK
		ENDSWITCH
	ELSE
		SWITCH thisVRT
			CASE VRT_BOAT
				RETURN "VS_TCK_00"//~s~Unable to locate a boat.
			BREAK
			CASE VRT_HELI
				RETURN "VS_TCK_01"//~s~Unable to locate a heli.
			BREAK
			CASE VRT_CAR
				RETURN "VS_TCK_02"//~s~Unable to locate a car.
			BREAK
			CASE VRT_PLANE
				RETURN "VS_TCK_03"//~s~Unable to locate a plane.
			BREAK
			CASE VRT_BLIMP
				RETURN "VS_TCK_04"//~s~Unable to locate a blimp.
			BREAK
		ENDSWITCH
	ENDIF
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Gets the cost for a vehicle request type
/// PARAMS:
///    thisVRT - vehicle request type to check
/// RETURNS:
///    price as an INT
FUNC INT GET_PRICE_FOR_VEHICLE_REQUEST_TYPE(VEHICLE_REQUEST_TYPE thisVRT)
	SWITCH thisVRT
		CASE VRT_BOAT
			RETURN GET_CONTACT_REQUEST_COST(REQUEST_LESTER_LOCATE_BOAT)
		BREAK
		CASE VRT_HELI
			RETURN GET_CONTACT_REQUEST_COST(REQUEST_LESTER_LOCATE_HELI)
		BREAK
		CASE VRT_CAR
			RETURN GET_CONTACT_REQUEST_COST(REQUEST_LESTER_LOCATE_CAR)
		BREAK
		CASE VRT_PLANE
			RETURN GET_CONTACT_REQUEST_COST(REQUEST_LESTER_LOCATE_PLANE)
		BREAK
		CASE VRT_BLIMP
			RETURN GET_CONTACT_REQUEST_COST(REQUEST_LESTER_LOCATE_BLIMP)
		BREAK
	ENDSWITCH
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Returns if a specified vehicle is of a specified vehicle request type
/// PARAMS:
///    thisVehicle - vehicle ID to check
///    thisVRT - vehicle request type to check
/// RETURNS:
///    true/false
FUNC BOOL IS_VEHICLE_OF_VEHICLE_REQUEST_TYPE(VEHICLE_INDEX thisVehicle, VEHICLE_REQUEST_TYPE thisVRT)
	
	IF DOES_ENTITY_EXIST(thisVehicle)
	
		MODEL_NAMES thisModel = GET_ENTITY_MODEL(thisVehicle)
		
		SWITCH thisVRT
			CASE VRT_BOAT
				IF IS_THIS_MODEL_A_BOAT(thisModel)
				OR thisModel = SUBMERSIBLE
				OR thisModel = SUBMERSIBLE2
					RETURN TRUE
				ENDIF
			BREAK
			CASE VRT_HELI
				IF IS_THIS_MODEL_A_HELI(thisModel)
					RETURN TRUE
				ENDIF
			BREAK
			CASE VRT_CAR
				IF IS_THIS_MODEL_A_CAR(thisModel)
				OR IS_THIS_MODEL_A_QUADBIKE(thisModel)
					RETURN TRUE
				ENDIF
			BREAK
			CASE VRT_PLANE
				IF IS_THIS_MODEL_A_PLANE(thisModel)
					RETURN TRUE
				ENDIF
			BREAK
			CASE VRT_BLIMP
				IF thisModel = BLIMP
				OR thisModel = BLIMP2
				OR thisModel = BLIMP3
					RETURN TRUE
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gets the vehicle request type for the specified vehicle model
/// PARAMS:
///    thisModel - vehicle model to check
/// RETURNS:
///    vehicle request type as a VEHICLE_REQUEST_TYPE
FUNC VEHICLE_REQUEST_TYPE GET_MODEL_VEHICLE_REQUEST_TYPE(MODEL_NAMES thisModel)
	
	IF thisModel = BLIMP
	OR thisModel = BLIMP2
	OR thisModel = BLIMP3
		RETURN VRT_BLIMP
	ENDIF
	
	IF IS_THIS_MODEL_A_BOAT(thisModel)
	OR thisModel = SUBMERSIBLE
	OR thisModel = SUBMERSIBLE2
		RETURN VRT_BOAT
	ENDIF
	
	IF IS_THIS_MODEL_A_HELI(thisModel)
		RETURN VRT_HELI
	ENDIF
	
	IF IS_THIS_MODEL_A_CAR(thisModel)
	OR IS_THIS_MODEL_A_QUADBIKE(thisModel)
		RETURN VRT_CAR
	ENDIF
	
	IF IS_THIS_MODEL_A_PLANE(thisModel)
		RETURN VRT_PLANE
	ENDIF
	
	RETURN VRT_CAR
ENDFUNC

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MAIN PROCS                  //////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE:
///    Returns if two points are within a squared distance value of each other
/// PARAMS:
///    vCoord1 - first point to compare
///    vCoord2 - second point to compare
///    fDistanceSquared - squared distance value to compare with
/// RETURNS:
///    true/false
FUNC BOOL ARE_TWO_POINTS_WITHIN_SQUARE_DISTANCE(VECTOR vCoord1, VECTOR vCoord2, FLOAT fDistanceSquared)
	
	IF VDIST2(vCoord1, vCoord2) < fDistanceSquared									//Is within distance
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check whether the Lester vehicle has been destroyed
PROC MAINTAIN_IS_LESTER_VEHICLE_DEAD()
	IF (NETWORK_IS_SCRIPT_ACTIVE(GET_MP_MISSION_NAME(eAM_CRIMINAL_DAMAGE))
		OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_BIKER_RIPPIN_IT_UP
		 )
	
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === [MAINTAIN_IS_LESTER_VEHICLE_DEAD] - SCRIPT ACTIVE")
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === [MAINTAIN_IS_LESTER_VEHICLE_DEAD] - ENTITY ALIVE")
			IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV)
				CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === [MAINTAIN_IS_LESTER_VEHICLE_DEAD] - ID EXISTS")
				IF IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_BOOKED)
				AND NOT IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_DELIVERY)
					VEHICLE_INDEX thisVehicle = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV)
					CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === [MAINTAIN_IS_LESTER_VEHICLE_DEAD] - BITSET")
					
					IF NOT IS_VEHICLE_DRIVEABLE(thisVehicle)
						REINIT_NET_TIMER(g_TransitionSessionNonResetVars.contactRequests.stCDTimer[REQUEST_LESTER_LOCATE_CAR],TRUE)
						CONTACT_REQUEST_SET_CD_TIMER(REQUEST_LESTER_LOCATE_CAR)
						CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === [MAINTAIN_IS_LESTER_VEHICLE_DEAD] - START TIMER")
						SET_BIT(iLocalBitset, LOCAL_BS_LESTER_VEHICLE_DEAD)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Performs the player getting a wanted level related to doing something wrong with a lester spawned vehicle
PROC DO_VEHICLE_SPAWN_WANTED_LEVEL()
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF NOT IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_RECEIVED_WANTED_LEVEL)
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_BIT(iLocalTextMessageBitset, TEXTMESSAGE_BS_REQUEST_BOOKING_WANTED)")
			#ENDIF
			SET_BIT(iLocalTextMessageBitset, TEXTMESSAGE_BS_REQUEST_BOOKING_WANTED)
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_BIT(bookedVehicle.iBitset, BOOK_BS_RECEIVED_WANTED_LEVEL)")
			#ENDIF
			SET_BIT(bookedVehicle.iBitset, BOOK_BS_RECEIVED_WANTED_LEVEL)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Clears the booked vehicle struct, ready for the next booking. Applies wanted level if the flag is set.
/// PARAMS:
///    bSetCollectFlag - incremenets pegasus vehicles collected stat if booked vehicle is pegasus
///    bTestForWantedLevel - test if the player should receieve a wanted level from this booking, and give wanted level if so
PROC CLEAR_BOOKED_VEHICLE(BOOL bSetCollectFlag = FALSE, BOOL bTestForWantedLevel = TRUE, BOOL bCleanupNetID = TRUE, BOOL bCleanupTruck = FALSE, BOOL bForceDeleteNetID = FALSE, BOOL bCleanupPlane = FALSE, BOOL bCleanupHackertruck = FALSE, BOOL bClearVehicleSpawnSearch = FALSE #IF FEATURE_DLC_2_2022 , BOOL bCleanupSupportBike = FALSE, BOOL bCleanupAcidLab = FALSE #ENDIF  )
	CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === CLEAR_BOOKED_VEHICLE ",
			PICK_STRING(bSetCollectFlag,			"bSetCollectFlag, ",			""),
			PICK_STRING(bTestForWantedLevel,		"bTestForWantedLevel, ",		""),
			PICK_STRING(bCleanupNetID,				"bCleanupNetID, ",				""),
			PICK_STRING(bCleanupTruck,				"bCleanupTruck, ",				""),
			PICK_STRING(bForceDeleteNetID,			"bForceDeleteNetID, ",			""),
			PICK_STRING(bCleanupPlane,				"bCleanupPlane, ",				""),
			PICK_STRING(bCleanupHackertruck,		"bCleanupHackertruck, ",		""),
			PICK_STRING(bClearVehicleSpawnSearch,	"bClearVehicleSpawnSearch, ",	""),
			"- Calling this...")
			
	#IF FEATURE_DLC_2_2022
		CPRINTLN(DEBUG_NET_AMBIENT, PICK_STRING(bCleanupSupportBike,		"bCleanupSupportBike, ",	""), PICK_STRING(bCleanupAcidLab,		"bCleanupAcidLab, ",	""))
	#ENDIF
	DEBUG_PRINTCALLSTACK()
	
	IF bTestForWantedLevel
		IF IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_EXPECTING_WANTED_LEVEL)
			DO_VEHICLE_SPAWN_WANTED_LEVEL()
		ENDIF
	ENDIF
	
	IF bSetCollectFlag
		IF IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_BOOKED)
			IF IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_DELIVERY)
			AND NOT IS_BIT_SET(bookedVehicle.iBitset,BOOK_BS_BOSS_LIMO)
			AND NOT IS_REQUEST_TO_CREATE_ARMORY_TRUCK_IN_BUNKER(PLAYER_ID())
			AND NOT IS_REQUEST_TO_CREATE_ARMORY_AIRCRAFT_IN_DEFUNCT_BASE(PLAYER_ID())
			AND NOT IS_REQUEST_TO_CREATE_HACKER_TRUCK_IN_BUSINESS_HUB(PLAYER_ID())
			#IF FEATURE_DLC_2_2022
			AND NOT IS_REQUEST_TO_CREATE_ACID_LAB_IN_JUGGALO_HIDEOUT(PLAYER_ID())
			#ENDIF
				IF NOT GET_MP_BOOL_CHARACTER_STAT(MP_STAT_PEGASUS_VEHICLE_COLLECTED)
					SET_MP_BOOL_CHARACTER_STAT(MP_STAT_PEGASUS_VEHICLE_COLLECTED, TRUE)
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_MP_BOOL_CHARACTER_STAT(MP_STAT_PEGASUS_VEHICLE_COLLECTED, TRUE)")
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBitset, LOCAL_BS_LESTER_VEHICLE_DEAD)
		CLEAR_BIT(iLocalBitset, LOCAL_BS_LESTER_VEHICLE_DEAD)
	ENDIF
		
	g_bHasPegasusVehicleInAirport = FALSE
	
	IF DOES_BLIP_EXIST(bookedVehicle.blipID)
		REMOVE_BLIP(bookedVehicle.blipID)
	ENDIF
	bookedVehicle.iBitset = 0
	bookedVehicle.iForceSpawnVehicleBitset = 0
	bookedVehicle.iRequestID = -1
	bookedVehicle.iSpawnPointID = -1
	
	bookedVehicle.ePegasusBuyableVehicle = UNSET_BUYABLE_VEHICLE
	
	IF bClearVehicleSpawnSearch
		CLEAR_VEHICLE_SPAWN_SEARCH()
	ENDIF
	
	IF bCleanupNetID
		IF bForceDeleteNetID
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === CLEAR_BOOKED_VEHICLE - Doing bForceDeleteNetID")
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV)
				DELETE_NET_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV)
				CLEAR_CONCEAL_CURRENT_PEGASUS_VEHICLE()
				IF IS_PEGASUS_VEHICLE_IN_AVENGER(PLAYER_ID())
					SET_PEGASUS_VEHICLE_IN_AVENGER(FALSE)
				ENDIF
			ELSE
				NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV)
			ENDIF
		ELSE
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === CLEAR_BOOKED_VEHICLE - Doing CLEANUP_NET_ID")
			CLEANUP_NET_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV)
			CLEAR_CONCEAL_CURRENT_PEGASUS_VEHICLE()
		ENDIF
		
		IF bCleanupTruck
			CLEANUP_NET_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])
			CLEANUP_NET_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
		ENDIF
		IF bCleanupPlane
			CLEANUP_NET_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV)
		ENDIF
		IF bCleanupHackertruck
			CLEANUP_NET_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV)
		ENDIF
		
		#IF FEATURE_DLC_2_2022
		IF bCleanupSupportBike
			CLEANUP_NET_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SupportBikeV)
		ENDIF
		IF bCleanupAcidLab
			CLEANUP_NET_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV)
		ENDIF
		#ENDIF
		
		IF bookedVehicle.mnBookModel <> DUMMY_MODEL_FOR_SCRIPT
			SET_MODEL_AS_NO_LONGER_NEEDED(bookedVehicle.mnBookModel)
		ENDIF
		bookedVehicle.mnBookModel = DUMMY_MODEL_FOR_SCRIPT
		bookedVehicle.vCreatedCoords = VECTOR_ZERO
		
	ENDIF

	MPGlobalsAmbience.bCleanupPegasusVehicle = FALSE	
	MPGlobalsAmbience.bCleanupTruckVehicle = FALSE
	MPGlobalsAmbience.bCleanupHackertruckVehicle = FALSE
	#IF FEATURE_DLC_2_2022
	MPGlobalsAmbience.bCleanupSupportBike = FALSE
	MPGlobalsAmbience.bCleanupAcidLabVehicle = FALSE
	#ENDIF
ENDPROC

/// PURPOSE:
///    Cleanup entities involved in this script
PROC CLEANUP_ENTITIES()
	IF MPGlobalsAmbience.bForceDeletePegasusVehicle
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(vehCreate)
			ENTITY_INDEX eiVehicle = NET_TO_ENT(vehCreate)
			DELETE_ENTITY(eiVehicle)
		ENDIF
	ELSE
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(vehCreate)
			ENTITY_INDEX eiVehicle = NET_TO_ENT(vehCreate)
			SET_ENTITY_AS_NO_LONGER_NEEDED(eiVehicle)
			//SET_VEHICLE_AS_NO_LONGER_NEEDED(NET_TO_VEH(vehCreate))
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(eiVehicle,TRUE)
		ENDIF
	ENDIF
	
ENDPROC

#IF FEATURE_CASINO
PROC CLEAN_UP_VEHICLE_REWARD_SCRIPT()
	PRINTLN("CLEAN_UP_VEHICLE_REWARD_SCRIPT called")
	g_bCleanUpVehRewardScript = TRUE
	CLEAR_BIT(iLocalBS, LOCAL_BS_VEHICLE_REWARD_SCRIPT_READY)
	RESET_NET_TIMER(sVehRewardScriptRunTimer)
	IF NOT IS_STRING_NULL_OR_EMPTY(sVehRewardScriptName)
		SET_SCRIPT_AS_NO_LONGER_NEEDED(sVehRewardScriptName)
	ENDIF	
ENDPROC
#ENDIF

#IF FEATURE_HEIST_ISLAND
PROC CLEANUP_ALL_SUBMARINE_WAVE_CALMING()
	INT i
	FOR i = 0 TO NUM_NETWORK_PLAYERS - 1
		IF iSubmarineCalmID[i] != -1
			PRINTLN("CLEANUP_ALL_SUBMARINE_WAVE_CALMING - REMOVE_EXTRA_CALMING_QUAD iCalmID = ", iSubmarineCalmID[i])
			REMOVE_EXTRA_CALMING_QUAD(iSubmarineCalmID[i])
			iTotalNumCalmZone--
			iSubmarineCalmID[i] = -1
			vSubmarineCalmWaterCoord[i] = <<0, 0, 0>>
		ENDIF
	ENDFOR
ENDPROC
#ENDIF

/// PURPOSE:
///    Everything required to clean up, apart from terminating the thread. Used to reset the mission.
PROC SCRIPT_TIDY_UP()
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SCRIPT_TIDY_UP CALLED")
	#ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			serverBD.iBitset = 0
		ENDIF
		playerBD[NATIVE_TO_INT(PLAYER_ID())].iBitset = 0
		CLEANUP_ENTITIES()
		//CLEAR_BOOKED_VEHICLE(TRUE, FALSE)
		CLEAR_BOOKED_VEHICLE(TRUE, FALSE,DEFAULT, DEFAULT, MPGlobalsAmbience.bForceDeletePegasusVehicle, DEFAULT, DEFAULT, TRUE)
		
		IF DOES_ENTITY_EXIST(droneTargetObjectDamaged)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(droneTargetObjectDamaged)
				DELETE_OBJECT(droneTargetObjectDamaged)
				PRINTLN("[AM_MP_DRONE] - SCRIPT_TIDY_UP - DELETE_OBJECT swaped model")
			ELSE
				NETWORK_REQUEST_CONTROL_OF_ENTITY(droneTargetObjectDamaged)
			ENDIF
		ENDIF
	ENDIF
	
	CLEAR_BIT(MPGlobals.g_iVehicleSpawnRequestBitset, VEH_SPAWN_REQUEST_BS_HAS_PEGASUS_VEHICLE)
	CLEAR_BIT(MPGlobals.g_iVehicleSpawnRequestBitset, VEH_SPAWN_REQUEST_BS_HAS_BOOKED_VEHICLE)
	MPGlobalsAmbience.bCleanupPegasusVehicle = FALSE
	MPGlobalsAmbience.bForceDeletePegasusVehicle = FALSE
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bBlipMyPegasusVehicleForGangMembers = FALSE
	MPGlobalsAmbience.bBlipPegasusVehicleForGangMembers = FALSE
	MPGlobalsAmbience.bCleanupTruckVehicle = FALSE
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bBlipMyTruckVehicleForGangMembers = FALSE
	MPGlobalsAmbience.bBlipTruckVehicleForGangMembers = FALSE
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bBlipMyPlaneVehicleForGangMembers = FALSE
	MPGlobalsAmbience.bBlipPlaneVehicleForGangMembers = FALSE
	MPGlobalsAmbience.bCleanupHackertruckVehicle = FALSE
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bBlipMyHackertruckVehicleForGangMembers = FALSE
	MPGlobalsAmbience.bBlipHackertruckVehicleForGangMembers = FALSE
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bBlipMySubmarineVehicleForGangMembers = FALSE
	MPGlobalsAmbience.bBlipSubmarineVehicleForGangMembers = FALSE
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bBlipMySubmarineDinghyVehicleForGangMembers = FALSE
	MPGlobalsAmbience.bBlipSubmarineDinghyVehicleForGangMembers = FALSE
	
	#IF FEATURE_DLC_2_2022
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bBlipMySupportBikeVehicleForGangMembers = FALSE
	MPGlobalsAmbience.bBlipSupportBikeVehicleForGangMembers = FALSE
	MPGlobalsAmbience.bCleanupSupportBike = FALSE
	MPGlobalsAmbience.bCleanupAcidLabVehicle = FALSE
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bBlipMyAcidLabVehicleForGangMembers = FALSE
	MPGlobalsAmbience.bBlipAcidLabVehicleForGangMembers = FALSE
	#ENDIF
	
	CLEANUP_HACKER_TRUCK_SCANNER(g_hackerTruckScanner)
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPegasusBS = 0
	
	#IF FEATURE_CASINO
	CLEAN_UP_VEHICLE_REWARD_SCRIPT()
	#ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	CLEANUP_ALL_SUBMARINE_WAVE_CALMING()
	#ENDIF
ENDPROC

/// PURPOSE:
///    Cleanup Mission Data
PROC SCRIPT_CLEANUP()
	
	SCRIPT_TIDY_UP()
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SCRIPT_CLEANUP DONE")
	#ENDIF
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

/// PURPOSE:
///    Spawns a vehicle using the model, coords and heading supplied. Vehicle is networked, but not force cloned across all machines and set as no longer needed immediately
/// PARAMS:
///    thisModel - model to spawn with
///    vCoords - location to spawn at
///    fHeading - heading to spawn with
/// RETURNS:
///    true when complete
FUNC BOOL SPAWN_VEHICLE(MODEL_NAMES thisModel, VECTOR vCoords, FLOAT fHeading,BOOL bFadeIn=FALSE)
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(vehCreate)
		IF CAN_REGISTER_MISSION_VEHICLES(1)
			IF REQUEST_LOAD_MODEL(thisModel)
				IF CREATE_NET_VEHICLE(vehCreate,thisModel, vCoords, fHeading,FALSE, TRUE)
					IF bFadeIn
						NETWORK_FADE_IN_ENTITY(NET_TO_ENT(vehCreate),TRUE)
					ENDIF
					SET_VEHICLE_ON_GROUND_PROPERLY(NET_TO_VEH(vehCreate))
					IF IS_THIS_MODEL_A_BOAT(thisModel)
					OR thisModel = SUBMERSIBLE
					OR thisModel = SUBMERSIBLE2
						IF CAN_ANCHOR_BOAT_HERE(NET_TO_VEH(vehCreate))
							SET_BOAT_ANCHOR(NET_TO_VEH(vehCreate), TRUE)
						ENDIF
					ENDIF
					
					IF thisModel = RIOT	//lock up the riot
						SET_VEHICLE_IS_STOLEN(NET_TO_VEH(vehCreate), TRUE)
					ENDIF
					
					SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(NET_TO_VEH(vehCreate), FALSE)
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(vehCreate, FALSE)")
					#ENDIF
					
					SET_MODEL_AS_NO_LONGER_NEEDED(thisModel)
					
					ENTITY_INDEX eiVehicle = NET_TO_ENT(vehCreate)
					SET_ENTITY_AS_NO_LONGER_NEEDED(eiVehicle)
					
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_VEHICLE( Model ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(thisModel), 
												", vCoords ", vCoords, ", fHeading ", fHeading, ")")
					#ENDIF
					RETURN TRUE
				ELSE
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_VEHICLE fail as CREATE_VEHICLE = FALSE")
					#ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_VEHICLE fail as REQUEST_LOAD_MODEL(", 
											GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(thisModel), ") = FALSE")
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_VEHICLE fail as CAN_REGISTER_MISSION_VEHICLES(1) = FALSE")
			#ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_VEHICLE fail as DOES_ENTITY_EXIST = TRUE")
		#ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SET_COMMON_PROPERTIES_FOR_PEGASUS_VEHICLE(SITE_BUYABLE_VEHICLE sbvModel = UNSET_BUYABLE_VEHICLE)

	// store my pegasus vehicle as a global so i can access it from other scripts
	MPGlobalsAmbience.vehPegasusVehicle = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV)
	
	MODEL_NAMES thisModel = GET_ENTITY_MODEL(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV))
	VECTOR vCoords = GET_ENTITY_COORDS(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV))
	FLOAT fHeading = GET_ENTITY_HEADING(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV))
							
	IF IS_PLAYER_IN_LUX_JET_VEHICLE(thisModel)
		luxeActState.niVehNetID = GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV
	ENDIF
							
	INT iDecoratorValue
	IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === DECOR_IS_REGISTERED_AS_TYPE")
		IF DECOR_EXIST_ON(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV), "MPBitset")	
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === DECOR_EXIST_ON")
			iDecoratorValue = DECOR_GET_INT(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV), "MPBitset")
		ENDIF
			
		IF IS_PLAYER_IN_LUX_JET_VEHICLE(thisModel)
		OR IS_PLAYER_IN_LUX_HELI_VEHICLE(thisModel)
			//Prevents the vehicle from being used for activities that use passive mode - MJM 
			SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_PASSIVE_MODE_VEHICLE)			
		ENDIF
		
		IF IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_BOSS_LIMO)
			SET_BIT(iDecoratorValue,MP_DECORATOR_BS_BOSS_LIMO)
			SET_BIT(iLocalBitset,LOCAL_BS_BOSS_LIMO) 
			SET_BIT(iLocalPhoneBitset,PHONE_BS_CALL_BOSS_LIMO)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_BIT(iDecoratorValue,MP_DECORATOR_BS_BOSS_LIMO)")
		ENDIF
		
		DECOR_SET_INT(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV), "MPBitset", iDecoratorValue)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MPBitset = ",iDecoratorValue)
	ENDIF
					
	SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV, PLAYER_ID(), TRUE)
	
	//SET_VEHICLE_ON_GROUND_PROPERLY(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV))
	
	IF IS_THIS_MODEL_A_BOAT(thisModel)
	OR thisModel = SUBMERSIBLE
	OR thisModel = SUBMERSIBLE2
		//CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === Vehicle is boat, trying to anchor")
		//IF CAN_ANCHOR_BOAT_HERE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV))
			SET_BOAT_ANCHOR(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV), TRUE)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === Vehicle is boat, anchoring")
		//ENDIF
	ENDIF
	
	// Make sure monster spawns with patriot tyre smoke!
	IF thisModel = MONSTER
		SET_VEHICLE_TYRE_SMOKE_COLOR(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV), 0, 0, 0)
	ENDIF
	
	// Make sure insurgent spawns with armour!
	IF thisModel = INSURGENT OR thisModel = INSURGENT2
		SET_VEHICLE_MOD_KIT(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV),0)
		SET_VEHICLE_MOD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV), MOD_ARMOUR, GET_NUM_VEHICLE_MODS(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV), MOD_ARMOUR)-1)
		SET_VEHICLE_TYRES_CAN_BURST(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV), FALSE)
	ENDIF
	
	IF thisModel = BLIMP3
		SET_VEHICLE_DAMAGE_SCALE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV), g_sMPTunables.fBB_PEGASUS_BLIMP3_DAMAGE_SCALE)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === Set ", GET_MODEL_NAME_FOR_DEBUG(thisModel), " damage scale ", g_sMPTunables.fBB_PEGASUS_BLIMP3_DAMAGE_SCALE)
	ENDIF
	
	//Make sure technical spawns with bulletproof tyres
	IF thisModel = TECHNICAL
	OR thisModel = LIMO2
	OR thisModel = BRICKADE
	OR thisModel = RALLYTRUCK
	OR thisModel = TAXI
	OR thisModel = BULLDOZER
	OR thisModel = SPEEDO2
	OR thisModel = TRASH2
	OR thisModel = BARRACKS2
	OR thisModel = MIXER
	OR thisModel = DUNE2
	OR thisModel = TRACTOR
		SET_VEHICLE_TYRES_CAN_BURST(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV), FALSE)
	ENDIF	
					
	//Make sure vehicles spawn with correct livery
	IF (sbvModel = UNSET_BUYABLE_VEHICLE)
		sbvModel = GET_WEBSITE_BUYABLE_VEHICLE_FROM_MODEL(thisModel) 
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === set sbvModel to ", ENUM_TO_INT(sbvModel))
	ENDIF
	CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === sbvModel is ", ENUM_TO_INT(sbvModel))
	INT iColour1, iColour2
	
	IF IS_SBV_A_VEHICLE_WITH_LIVERIES(sbvModel) 
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === Checking Livery...")
	
		IF GET_NUM_MOD_KITS(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV)) > 0
			SET_VEHICLE_MOD_KIT(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV),0)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_VEHICLE_MOD_KIT - 0")
		ENDIF
		
		// Vehicles that use liveries either use the mod system or the old livery system
		// We can just query the mod count to determine which one should be used.
		IF GET_NUM_VEHICLE_MODS(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV), MOD_LIVERY) > 0
			SET_VEHICLE_MOD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV), MOD_LIVERY, GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].g_iVehicleSpawnLiveryIndex)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_VEHICLE_MOD(MOD_LIVERY) = ",GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].g_iVehicleSpawnLiveryIndex)
		ELSE
			SET_VEHICLE_LIVERY(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV),GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].g_iVehicleSpawnLiveryIndex)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_VEHICLE_LIVERY = ",GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].g_iVehicleSpawnLiveryIndex)
		ENDIF
		
		IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].g_iVehicleSpawnColour1 != -1
			SET_VEHICLE_COLOURS(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV),GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].g_iVehicleSpawnColour1,GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].g_iVehicleSpawnColour2)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_VEHICLE_COLOURS = ",GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].g_iVehicleSpawnColour1,", ",GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].g_iVehicleSpawnColour2)
		ENDIF
	ELIF GET_VEHICLE_COLOURS_FROM_BIGASS_VEHICLE_INDICE_BIT(GET_BIGASS_VEHICLE_INDICE_BIT_FROM_ENUM(sbvModel), iColour1, iColour2)
		SET_VEHICLE_COLOURS(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV), iColour1, iColour2)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_VEHICLE_COLOURS = ", iColour1, ", ", iColour2)
	ENDIF
	
	INT iExtraCol1, iExtraCol2
	IF IS_SBV_A_VEHICLE_WITH_COLOUR_EXTRAS(sbvModel, iExtraCol1, iExtraCol2)
		SET_VEHICLE_EXTRA_COLOURS(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV), iExtraCol1, iExtraCol2)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_VEHICLE_EXTRA_COLOURS = ",iExtraCol1,", ",iExtraCol2)
	ENDIF
	
	IF IS_SBV_A_VEHICLE_WITH_CUSTOM_SETUP(sbvModel)
		APPLY_CUSTOM_SETUP_TO_SBV(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV), sbvModel)	
	ENDIF
	
	IF thisModel = BLIMP3
		SET_VEHICLE_MOD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV), MOD_BUMPER_F, 1)
	ENDIF
	
	IF thisModel = TAXI
		SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV), 1)
		
		SET_VEHICLE_WINDOW_TINT(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV), 0)
		SET_VEHICLE_WINDOW_TINT(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV), 1)
		SET_VEHICLE_WHEEL_TYPE_PATCHED(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV), INT_TO_ENUM(MOD_WHEEL_TYPE, 2))
		SET_VEHICLE_TYRE_SMOKE_COLOR(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV), 255, 255, 255)
		
		SET_VEHICLE_MOD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV),MOD_ENGINE				, 4-1)
		SET_VEHICLE_MOD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV),MOD_BRAKES				, 3-1)
		SET_VEHICLE_MOD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV),MOD_GEARBOX				, 3-1)
		SET_VEHICLE_MOD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV),MOD_SUSPENSION			, 4-1)
		SET_VEHICLE_MOD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV),MOD_ARMOUR				, 5-1)
		TOGGLE_VEHICLE_MOD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV),MOD_TOGGLE_TURBO		, TRUE)	// 1
		TOGGLE_VEHICLE_MOD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV),MOD_TOGGLE_XENON_LIGHTS, TRUE)	// 8
		SET_VEHICLE_MOD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV),MOD_WHEELS				, 11-1, TRUE)
		
		SET_VEHICLE_MOD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV), INT_TO_ENUM(MOD_TYPE, 0), 0)
		
		SET_VEHICLE_EXTRA(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV), 4+1, !TRUE)
		SET_VEHICLE_EXTRA(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV), 7+1, !FALSE)
		SET_VEHICLE_EXTRA(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV), 8+1, !FALSE)
		SET_VEHICLE_EXTRA(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV), 9+1, !FALSE)
	ELIF thisModel = DINGHY5
		SET_VEHICLE_EXTRA(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV), 3, FALSE)
	ENDIF
	
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV),FALSE)
	
	bookedVehicle.vCreatedCoords = vCoords
	
	IF IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_DELIVERY)
		DECOR_SET_BOOL(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV), "CreatedByPegasus", TRUE)
		SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV), TRUE)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(vehCreate, TRUE)")
		#ENDIF
	ELSE
		SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV), FALSE)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(vehCreate, FALSE)")
		#ENDIF
	ENDIF
	
	SET_MODEL_AS_NO_LONGER_NEEDED(thisModel)
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_PEGASUS_VEHICLE( Model ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(thisModel), 
								", vCoords ", vCoords, ", fHeading ", fHeading, ")")
	#ENDIF

	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bBlipMyPegasusVehicleForGangMembers = MPGlobalsAmbience.bBlipPegasusVehicleForGangMembers

	UNUSED_PARAMETER(fHeading)

ENDPROC

FUNC BOOL SET_VEHICLE_AS_PEGASUS_VEHICLE(VEHICLE_INDEX vehID, SITE_BUYABLE_VEHICLE sbvModel = UNSET_BUYABLE_VEHICLE)
	
	IF DOES_ENTITY_EXIST(vehID)
		
		INT iInstance
		STRING scriptName 
		IF IS_VEHICLE_DRIVEABLE(vehID)
			IF NETWORK_GET_ENTITY_IS_NETWORKED(vehID)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(vehID)
					IF CAN_REGISTER_MISSION_VEHICLES(1)
						scriptName = GET_ENTITY_SCRIPT(vehID,iInstance)
						PRINTLN("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_PEGASUS_VEHICLE: script name = ",scriptName)
						IF NOT IS_STRING_NULL_OR_EMPTY(scriptName)
							IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(scriptName)) <= 0
								IF IS_ENTITY_A_MISSION_ENTITY(vehID)
									NET_PRINT("=== VEH SPAWN CLIENT == CDM_MP - waiting for entity to not be a mission entity as script that created it is cleaning up") NET_NL()
									RETURN FALSE
								ELSE
									PRINTLN("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_PEGASUS_VEHICLE: NOT a mission Entity")
								ENDIF
							ELSE
								PRINTLN("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_PEGASUS_VEHICLE: num scripts running = ",GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(scriptName)))
							ENDIF
						ENDIF
						
						IF NOT IS_ENTITY_A_MISSION_ENTITY(vehID)
							SET_ENTITY_AS_MISSION_ENTITY(vehID,FALSE,TRUE)
							NET_PRINT("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_PEGASUS_VEHICLE - not a mission entity, setting as one.") NET_NL()
						ENDIF
					
						IF IS_ENTITY_A_MISSION_ENTITY(vehID)
							IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehID)						
								SET_ENTITY_AS_MISSION_ENTITY(vehID,FALSE,TRUE)
								NET_PRINT("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_PEGASUS_VEHICLE - waiting for vehicle to belong to this script.") NET_NL()							
							ELSE
								GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV = VEH_TO_NET(vehID)
								SET_BIT(bookedVehicle.iBitset, BOOK_BS_BOOKED)
								SET_BIT(bookedVehicle.iBitset, BOOK_BS_DELIVERY)
								SET_BIT(bookedVehicle.iBitset, BOOK_BS_NOT_SPAWNED_BY_SCRIPT)
								SET_COMMON_PROPERTIES_FOR_PEGASUS_VEHICLE(sbvModel)
								bookedVehicle.mnBookModel = GET_ENTITY_MODEL(vehID)
								NET_PRINT("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_PEGASUS_VEHICLE - Returning TRUE") NET_NL()
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					NETWORK_REQUEST_CONTROL_OF_ENTITY(vehID)
				ENDIF		
			ENDIF		
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_VEHICLE_AS_PEGASUS_VEHICLE fail as NOT DOES_ENTITY_EXIST")
		#ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL SET_VEHICLE_AS_TRUCK_VEHICLE(VEHICLE_INDEX vehID, VEHICLE_INDEX trailerID)
	
//	IF HAS_TRUCK_ASSIGN_TO_MAIN_THREAD()
		IF DOES_ENTITY_EXIST(vehID)
		AND DOES_ENTITY_EXIST(trailerID)
		
			INT iInstance
			STRING scriptName 
			IF IS_VEHICLE_DRIVEABLE(vehID)
			AND IS_VEHICLE_DRIVEABLE(trailerID)
				IF IS_VEHICLE_DRIVEABLE(trailerID)
					IF NETWORK_GET_ENTITY_IS_NETWORKED(vehID)
					AND NETWORK_GET_ENTITY_IS_NETWORKED(trailerID)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(vehID)
						AND NETWORK_HAS_CONTROL_OF_ENTITY(trailerID)
							IF CAN_REGISTER_MISSION_VEHICLES(2)
								scriptName = GET_ENTITY_SCRIPT(vehID,iInstance)
								PRINTLN("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_TRUCK_VEHICLE: script name = ",scriptName)
								IF NOT IS_STRING_NULL_OR_EMPTY(scriptName)
									IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(scriptName)) <= 0
										IF IS_ENTITY_A_MISSION_ENTITY(vehID)
											NET_PRINT("=== VEH SPAWN CLIENT == CDM_MP - waiting for entity to not be a mission entity as script that created it is cleaning up") NET_NL()
											RETURN FALSE
										ELSE
											PRINTLN("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_TRUCK_VEHICLE: NOT a mission Entity")
										ENDIF
									ELSE
										PRINTLN("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_TRUCK_VEHICLE: num scripts running = ",GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(scriptName)))
									ENDIF
								ENDIF
								
								scriptName = GET_ENTITY_SCRIPT(trailerID,iInstance)
								PRINTLN("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_TRUCK_VEHICLE: script name = ",scriptName)
								IF NOT IS_STRING_NULL_OR_EMPTY(scriptName)
									IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(scriptName)) <= 0
										IF IS_ENTITY_A_MISSION_ENTITY(trailerID)
											NET_PRINT("=== VEH SPAWN CLIENT == CDM_MP - waiting for entity to not be a mission entity as script that created it is cleaning up") NET_NL()
											RETURN FALSE
										ELSE
											PRINTLN("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_TRUCK_VEHICLE: NOT a mission Entity")
										ENDIF
									ELSE
										PRINTLN("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_TRUCK_VEHICLE: num scripts running = ",GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(scriptName)))
									ENDIF
								ENDIF
								
								IF NOT IS_ENTITY_A_MISSION_ENTITY(vehID)
									SET_ENTITY_AS_MISSION_ENTITY(vehID,FALSE,TRUE)
									NET_PRINT("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_TRUCK_VEHICLE - not a mission entity, setting as one.") NET_NL()
								ENDIF
							
								IF NOT IS_ENTITY_A_MISSION_ENTITY(trailerID)
									SET_ENTITY_AS_MISSION_ENTITY(trailerID,FALSE,TRUE)
									NET_PRINT("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_TRUCK_VEHICLE - not a mission entity, setting as one.") NET_NL()
								ENDIF
							
								IF IS_ENTITY_A_MISSION_ENTITY(vehID)
								AND IS_ENTITY_A_MISSION_ENTITY(trailerID)
									IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehID)						
										SET_ENTITY_AS_MISSION_ENTITY(vehID,FALSE,TRUE)
										NET_PRINT("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_TRUCK_VEHICLE - waiting for vehicle to belong to this script.") NET_NL()							
									ENDIF
									IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(trailerID)						
										SET_ENTITY_AS_MISSION_ENTITY(trailerID,FALSE,TRUE)
										NET_PRINT("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_TRUCK_VEHICLE - waiting for vehicle to belong to this script.") NET_NL()							
									ENDIF
										
									IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehID)						
									AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(trailerID)						
										GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0] = VEH_TO_NET(vehID)
										GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1] = VEH_TO_NET(trailerID)
									//	SET_BIT(bookedVehicle.iBitset, BOOK_BS_BOOKED)
									//	SET_BIT(bookedVehicle.iBitset, BOOK_BS_DELIVERY)
									//	SET_BIT(bookedVehicle.iBitset, BOOK_BS_NOT_SPAWNED_BY_SCRIPT)
										SET_COMMON_PROPERTIES_FOR_TRUCK_VEHICLE(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0], GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
												
										
//										IF NOT DOES_VEHICLE_HAVE_STUCK_VEHICLE_CHECK(TrailerID)								
//											ADD_VEHICLE_STUCK_CHECK_WITH_WARP(trailerID, 2.0, 500, FALSE, FALSE, FALSE, -1) 
//										ENDIF
										
	//									bookedVehicle.mnBookModel = GET_ENTITY_MODEL(vehID)
										NET_PRINT("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_TRUCK_VEHICLE - Returning TRUE") NET_NL()
//										ASSIGNED_TRUCK_TO_MAIN_SCRIPT(TRUE)
//										ASSIGN_TRUCK_TO_MAIN_THREAD(FALSE)
										RETURN TRUE
									ENDIF
								ENDIF
							ENDIF
						ELSE
							NETWORK_REQUEST_CONTROL_OF_ENTITY(vehID)
							NETWORK_REQUEST_CONTROL_OF_ENTITY(trailerID)
						ENDIF		
					ENDIF
				ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			IF GET_FRAME_COUNT() % 60 = 0
				CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_VEHICLE_AS_TRUCK_VEHICLE fail as NOT DOES_ENTITY_EXIST")
			ENDIF
			#ENDIF
		ENDIF
//	ENDIF	
	
	RETURN FALSE

ENDFUNC

FUNC BOOL SET_VEHICLE_AS_AVENGER_VEHICLE(VEHICLE_INDEX vehID)
	
//	IF HAS_TRUCK_ASSIGN_TO_MAIN_THREAD()
		IF DOES_ENTITY_EXIST(vehID)
		
			INT iInstance
			STRING scriptName 
			IF IS_VEHICLE_DRIVEABLE(vehID)
				IF NETWORK_GET_ENTITY_IS_NETWORKED(vehID)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(vehID)
						IF CAN_REGISTER_MISSION_VEHICLES(1)
							scriptName = GET_ENTITY_SCRIPT(vehID,iInstance)
							PRINTLN("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_AVENGER_VEHICLE: script name = ",scriptName)
							IF NOT IS_STRING_NULL_OR_EMPTY(scriptName)
								IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(scriptName)) <= 0
									IF IS_ENTITY_A_MISSION_ENTITY(vehID)
										NET_PRINT("=== VEH SPAWN CLIENT == CDM_MP - waiting for entity to not be a mission entity as script that created it is cleaning up") NET_NL()
										RETURN FALSE
									ELSE
										PRINTLN("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_AVENGER_VEHICLE: NOT a mission Entity")
									ENDIF
								ELSE
									PRINTLN("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_AVENGER_VEHICLE: num scripts running = ",GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(scriptName)))
								ENDIF
							ENDIF
							
							IF NOT IS_ENTITY_A_MISSION_ENTITY(vehID)
								SET_ENTITY_AS_MISSION_ENTITY(vehID,FALSE,TRUE)
								NET_PRINT("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_AVENGER_VEHICLE - not a mission entity, setting as one.") NET_NL()
							ENDIF
						
							IF IS_ENTITY_A_MISSION_ENTITY(vehID)
								IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehID)						
									SET_ENTITY_AS_MISSION_ENTITY(vehID,FALSE,TRUE)
									NET_PRINT("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_AVENGER_VEHICLE - waiting for vehicle to belong to this script.") NET_NL()							
								ENDIF
									
								IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehID)						
									GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV = VEH_TO_NET(vehID)
								//	SET_BIT(bookedVehicle.iBitset, BOOK_BS_BOOKED)
								//	SET_BIT(bookedVehicle.iBitset, BOOK_BS_DELIVERY)
								//	SET_BIT(bookedVehicle.iBitset, BOOK_BS_NOT_SPAWNED_BY_SCRIPT)
									SET_COMMON_PROPERTIES_FOR_AVENGER_VEHICLE(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV)
									
//									bookedVehicle.mnBookModel = GET_ENTITY_MODEL(vehID)
									NET_PRINT("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_AVENGER_VEHICLE - Returning TRUE") NET_NL()
//										ASSIGNED_TRUCK_TO_MAIN_SCRIPT(TRUE)
//										ASSIGN_TRUCK_TO_MAIN_THREAD(FALSE)
									RETURN TRUE
								ENDIF
							ENDIF
						ENDIF
					ELSE
						NETWORK_REQUEST_CONTROL_OF_ENTITY(vehID)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			IF GET_FRAME_COUNT() % 60 = 0
				CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_VEHICLE_AS_AVENGER_VEHICLE fail as NOT DOES_ENTITY_EXIST")
			ENDIF
			#ENDIF
		ENDIF
//	ENDIF	
	
	RETURN FALSE

ENDFUNC

FUNC BOOL SET_VEHICLE_AS_HACKER_TRUCK_VEHICLE(VEHICLE_INDEX vehID)

	IF DOES_ENTITY_EXIST(vehID)
	
		INT iInstance
		STRING scriptName 
		IF IS_VEHICLE_DRIVEABLE(vehID)
			IF NETWORK_GET_ENTITY_IS_NETWORKED(vehID)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(vehID)
					IF CAN_REGISTER_MISSION_VEHICLES(1)
						scriptName = GET_ENTITY_SCRIPT(vehID,iInstance)
						PRINTLN("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_HACKER_TRUCK_VEHICLE: script name = ",scriptName)
						IF NOT IS_STRING_NULL_OR_EMPTY(scriptName)
							IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(scriptName)) <= 0
								IF IS_ENTITY_A_MISSION_ENTITY(vehID)
									NET_PRINT("=== VEH SPAWN CLIENT == CDM_MP - waiting for entity to not be a mission entity as script that created it is cleaning up") NET_NL()
									RETURN FALSE
								ELSE
									PRINTLN("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_HACKER_TRUCK_VEHICLE: NOT a mission Entity")
								ENDIF
							ELSE
								PRINTLN("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_HACKER_TRUCK_VEHICLE: num scripts running = ",GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(scriptName)))
							ENDIF
						ENDIF
						
						IF NOT IS_ENTITY_A_MISSION_ENTITY(vehID)
							SET_ENTITY_AS_MISSION_ENTITY(vehID,FALSE,TRUE)
							NET_PRINT("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_HACKER_TRUCK_VEHICLE - not a mission entity, setting as one.") NET_NL()
						ENDIF
					
						IF IS_ENTITY_A_MISSION_ENTITY(vehID)
							IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehID)						
								SET_ENTITY_AS_MISSION_ENTITY(vehID,FALSE,TRUE)
								NET_PRINT("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_HACKER_TRUCK_VEHICLE - waiting for vehicle to belong to this script.") NET_NL()							
							ENDIF
								
							IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehID)						
								GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV = VEH_TO_NET(vehID)
								SET_COMMON_PROPERTIES_FOR_HACKER_TRUCK_VEHICLE(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV)
								NET_PRINT("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_HACKER_TRUCK_VEHICLE - Returning TRUE") NET_NL()
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					NETWORK_REQUEST_CONTROL_OF_ENTITY(vehID)
				ENDIF		
			ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		IF GET_FRAME_COUNT() % 60 = 0
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_VEHICLE_AS_HACKER_TRUCK_VEHICLE fail as NOT DOES_ENTITY_EXIST")
		ENDIF
		#ENDIF
	ENDIF

	RETURN FALSE

ENDFUNC

#IF FEATURE_DLC_2_2022
FUNC BOOL SET_VEHICLE_AS_ACID_LAB_VEHICLE(VEHICLE_INDEX vehID)

	IF DOES_ENTITY_EXIST(vehID)
	
		INT iInstance
		STRING scriptName 
		IF IS_VEHICLE_DRIVEABLE(vehID)
			IF NETWORK_GET_ENTITY_IS_NETWORKED(vehID)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(vehID)
					IF CAN_REGISTER_MISSION_VEHICLES(1)
						scriptName = GET_ENTITY_SCRIPT(vehID,iInstance)
						PRINTLN("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_ACID_LAB_VEHICLE: script name = ",scriptName)
						IF NOT IS_STRING_NULL_OR_EMPTY(scriptName)
							IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(scriptName)) <= 0
								IF IS_ENTITY_A_MISSION_ENTITY(vehID)
									NET_PRINT("=== VEH SPAWN CLIENT == CDM_MP - waiting for entity to not be a mission entity as script that created it is cleaning up") NET_NL()
									RETURN FALSE
								ELSE
									PRINTLN("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_ACID_LAB_VEHICLE: NOT a mission Entity")
								ENDIF
							ELSE
								PRINTLN("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_ACID_LAB_VEHICLE: num scripts running = ",GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(scriptName)))
							ENDIF
						ENDIF
						
						IF NOT IS_ENTITY_A_MISSION_ENTITY(vehID)
							SET_ENTITY_AS_MISSION_ENTITY(vehID,FALSE,TRUE)
							NET_PRINT("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_ACID_LAB_VEHICLE - not a mission entity, setting as one.") NET_NL()
						ENDIF
					
						IF IS_ENTITY_A_MISSION_ENTITY(vehID)
							IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehID)						
								SET_ENTITY_AS_MISSION_ENTITY(vehID,FALSE,TRUE)
								NET_PRINT("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_ACID_LAB_VEHICLE - waiting for vehicle to belong to this script.") NET_NL()							
							ENDIF
								
							IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehID)						
								GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV = VEH_TO_NET(vehID)
								SET_COMMON_PROPERTIES_FOR_ACID_LAB_VEHICLE(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV)
								NET_PRINT("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_ACID_LAB_VEHICLE - Returning TRUE") NET_NL()
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					NETWORK_REQUEST_CONTROL_OF_ENTITY(vehID)
				ENDIF		
			ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		IF GET_FRAME_COUNT() % 60 = 0
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_VEHICLE_AS_ACID_LAB_VEHICLE fail as NOT DOES_ENTITY_EXIST")
		ENDIF
		#ENDIF
	ENDIF

	RETURN FALSE

ENDFUNC
#ENDIF

#IF FEATURE_HEIST_ISLAND
FUNC BOOL SET_VEHICLE_AS_SUBMARINE_VEHICLE(VEHICLE_INDEX vehID)

	IF DOES_ENTITY_EXIST(vehID)
	
		INT iInstance
		STRING scriptName 
		IF IS_VEHICLE_DRIVEABLE(vehID)
			IF NETWORK_GET_ENTITY_IS_NETWORKED(vehID)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(vehID)
					IF CAN_REGISTER_MISSION_VEHICLES(1)
						scriptName = GET_ENTITY_SCRIPT(vehID,iInstance)
						PRINTLN("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_SUBMARINE_VEHICLE: script name = ",scriptName)
						IF NOT IS_STRING_NULL_OR_EMPTY(scriptName)
							IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(scriptName)) <= 0
								IF IS_ENTITY_A_MISSION_ENTITY(vehID)
									NET_PRINT("=== VEH SPAWN CLIENT == CDM_MP - waiting for entity to not be a mission entity as script that created it is cleaning up") NET_NL()
									RETURN FALSE
								ELSE
									PRINTLN("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_SUBMARINE_VEHICLE: NOT a mission Entity")
								ENDIF
							ELSE
								PRINTLN("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_SUBMARINE_VEHICLE: num scripts running = ",GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(scriptName)))
							ENDIF
						ENDIF
						
						IF NOT IS_ENTITY_A_MISSION_ENTITY(vehID)
							SET_ENTITY_AS_MISSION_ENTITY(vehID,FALSE,TRUE)
							NET_PRINT("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_SUBMARINE_VEHICLE - not a mission entity, setting as one.") NET_NL()
						ENDIF
					
						IF IS_ENTITY_A_MISSION_ENTITY(vehID)
							IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehID)						
								SET_ENTITY_AS_MISSION_ENTITY(vehID,FALSE,TRUE)
								NET_PRINT("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_SUBMARINE_VEHICLE - waiting for vehicle to belong to this script.") NET_NL()							
							ENDIF
								
							IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehID)						
								GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineV = VEH_TO_NET(vehID)
								SET_COMMON_PROPERTIES_FOR_SUBMARINE_VEHICLE(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineV)
								NET_PRINT("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_SUBMARINE_VEHICLE - Returning TRUE") NET_NL()
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					NETWORK_REQUEST_CONTROL_OF_ENTITY(vehID)
				ENDIF		
			ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		IF GET_FRAME_COUNT() % 60 = 0
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_VEHICLE_AS_SUBMARINE_VEHICLE fail as NOT DOES_ENTITY_EXIST")
		ENDIF
		#ENDIF
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC BOOL SET_VEHICLE_AS_SUBMARINE_DINGHY_VEHICLE(VEHICLE_INDEX vehID)

	IF DOES_ENTITY_EXIST(vehID)
	
		INT iInstance
		STRING scriptName 
		IF IS_VEHICLE_DRIVEABLE(vehID)
			IF NETWORK_GET_ENTITY_IS_NETWORKED(vehID)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(vehID)
					IF CAN_REGISTER_MISSION_VEHICLES(1)
						scriptName = GET_ENTITY_SCRIPT(vehID,iInstance)
						PRINTLN("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_SUBMARINE_DINGHY_VEHICLE: script name = ",scriptName)
						IF NOT IS_STRING_NULL_OR_EMPTY(scriptName)
							IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(scriptName)) <= 0
								IF IS_ENTITY_A_MISSION_ENTITY(vehID)
									NET_PRINT("=== VEH SPAWN CLIENT == CDM_MP - waiting for entity to not be a mission entity as script that created it is cleaning up") NET_NL()
									RETURN FALSE
								ELSE
									PRINTLN("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_SUBMARINE_DINGHY_VEHICLE: NOT a mission Entity")
								ENDIF
							ELSE
								PRINTLN("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_SUBMARINE_DINGHY_VEHICLE: num scripts running = ",GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(scriptName)))
							ENDIF
						ENDIF
						
						IF NOT IS_ENTITY_A_MISSION_ENTITY(vehID)
							SET_ENTITY_AS_MISSION_ENTITY(vehID,FALSE,TRUE)
							NET_PRINT("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_SUBMARINE_DINGHY_VEHICLE - not a mission entity, setting as one.") NET_NL()
						ENDIF
					
						IF IS_ENTITY_A_MISSION_ENTITY(vehID)
							IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehID)						
								SET_ENTITY_AS_MISSION_ENTITY(vehID,FALSE,TRUE)
								NET_PRINT("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_SUBMARINE_DINGHY_VEHICLE - waiting for vehicle to belong to this script.") NET_NL()							
							ENDIF
								
							IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehID)						
								GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineDinghyV = VEH_TO_NET(vehID)
								SET_COMMON_PROPERTIES_FOR_SUBMARINE_DINGHY_VEHICLE(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineDinghyV)
								NET_PRINT("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_SUBMARINE_DINGHY_VEHICLE - Returning TRUE") NET_NL()
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					NETWORK_REQUEST_CONTROL_OF_ENTITY(vehID)
				ENDIF		
			ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		IF GET_FRAME_COUNT() % 60 = 0
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_VEHICLE_AS_SUBMARINE_DINGHY_VEHICLE fail as NOT DOES_ENTITY_EXIST")
		ENDIF
		#ENDIF
	ENDIF

	RETURN FALSE

ENDFUNC
#ENDIF

#IF FEATURE_DLC_2_2022
FUNC BOOL SET_VEHICLE_AS_SUPPORT_BIKE_VEHICLE(VEHICLE_INDEX vehID)

	IF DOES_ENTITY_EXIST(vehID)
	
		INT iInstance
		STRING scriptName 
		IF IS_VEHICLE_DRIVEABLE(vehID)
			IF NETWORK_GET_ENTITY_IS_NETWORKED(vehID)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(vehID)
					IF CAN_REGISTER_MISSION_VEHICLES(1)
						scriptName = GET_ENTITY_SCRIPT(vehID,iInstance)
						PRINTLN("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_SUPPORT_BIKE_VEHICLE: script name = ",scriptName)
						IF NOT IS_STRING_NULL_OR_EMPTY(scriptName)
							IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(scriptName)) <= 0
								IF IS_ENTITY_A_MISSION_ENTITY(vehID)
									NET_PRINT("=== VEH SPAWN CLIENT == CDM_MP - waiting for entity to not be a mission entity as script that created it is cleaning up") NET_NL()
									RETURN FALSE
								ELSE
									PRINTLN("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_SUPPORT_BIKE_VEHICLE: NOT a mission Entity")
								ENDIF
							ELSE
								PRINTLN("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_SUPPORT_BIKE_VEHICLE: num scripts running = ",GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(scriptName)))
							ENDIF
						ENDIF
						
						IF NOT IS_ENTITY_A_MISSION_ENTITY(vehID)
							SET_ENTITY_AS_MISSION_ENTITY(vehID,FALSE,TRUE)
							NET_PRINT("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_SUPPORT_BIKE_VEHICLE - not a mission entity, setting as one.") NET_NL()
						ENDIF
					
						IF IS_ENTITY_A_MISSION_ENTITY(vehID)
							IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehID)						
								SET_ENTITY_AS_MISSION_ENTITY(vehID,FALSE,TRUE)
								NET_PRINT("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_SUPPORT_BIKE_VEHICLE - waiting for vehicle to belong to this script.") NET_NL()							
							ENDIF
								
							IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehID)						
								GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SupportBikeV = VEH_TO_NET(vehID)
								SET_COMMON_PROPERTIES_FOR_SUPPORT_BIKE_VEHICLE(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SupportBikeV)
								NET_PRINT("=== VEH SPAWN CLIENT == SET_VEHICLE_AS_SUPPORT_BIKE_VEHICLE - Returning TRUE") NET_NL()
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					NETWORK_REQUEST_CONTROL_OF_ENTITY(vehID)
				ENDIF		
			ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		IF GET_FRAME_COUNT() % 60 = 0
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_VEHICLE_AS_SUPPORT_BIKE_VEHICLE fail as NOT DOES_ENTITY_EXIST")
		ENDIF
		#ENDIF
	ENDIF

	RETURN FALSE

ENDFUNC
#ENDIF

/// PURPOSE:
///    Spawns a booked vehicle with the model, coordinates and heading supplied. Vehicle is networked. Stores the location the booked vehicle was created at
/// PARAMS:
///    thisModel - model to spawn with
///    vCoords - location to spawn at
///    fHeading - haeding to spawn with
/// RETURNS:
///    true when complete
FUNC BOOL SPAWN_BOOKED_PEGASUS_VEHICLE(MODEL_NAMES thisModel, VECTOR vCoords, FLOAT fHeading,BOOL bFadeIn=FALSE)
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV)
	AND NOT DOES_ENTITY_EXIST(MPGlobals.RemotePegV[NATIVE_TO_INT(PLAYER_ID())])
		
		IF REQUEST_LOAD_MODEL(thisModel)
			IF CREATE_NET_VEHICLE(	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV, thisModel, vCoords, fHeading, FALSE, TRUE, FALSE)
				
				SET_COMMON_PROPERTIES_FOR_PEGASUS_VEHICLE()
				
				// store my pegasus vehicle as a global so i can access it from other scripts
				MPGlobalsAmbience.vehPegasusVehicle = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV)
										
				SET_VEHICLE_ON_GROUND_PROPERLY(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV))
				
				IF bFadeIn
					NETWORK_FADE_IN_ENTITY(NET_TO_ENT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV),TRUE)
				ENDIF				
				
				MPGlobalsAmbience.bCleanupPegasusVehicle = FALSE
				MPGlobalsAmbience.bForceDeletePegasusVehicle = FALSE
				RETURN TRUE
			ELSE
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_PEGASUS_VEHICLE fail as CREATE_NET_VEHICLE = FALSE")
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_PEGASUS_VEHICLE fail as REQUEST_LOAD_MODEL = FALSE")
			#ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_PEGASUS_VEHICLE fail as DOES_ENTITY_EXIST = TRUE")
		#ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Cycles through all spawn points and their spawn coordinates to decide what spawn coordinates are suitable to spawn at. Expensive, do sparingly.
/// PARAMS:
///    iVSID - vehicle spawn point ID to test
/// RETURNS:
///    bitset of suitable spawn coords as an INT
FUNC INT CREATE_EXPENSIVE_BITSET(INT iVSID)
	INT iSpawnCoordLoop, iEntityLoop
	INT iReturnBitset = 0
	
	BOOL bPointBlocked = FALSE
	
	REPEAT GET_NUMBER_OF_VEHICLE_SPAWN_POINT_COORDS(iVSID, TRUE) iSpawnCoordLoop
		
		bPointBlocked = FALSE
		
		REPEAT NUM_NETWORK_REAL_PLAYERS() iEntityLoop
			IF NOT bPointBlocked
				IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(iEntityLoop), FALSE)
					IF VDIST2(	GET_ENTITY_COORDS(GET_PLAYER_PED(INT_TO_PLAYERINDEX(iEntityLoop)), FALSE), 
								GET_VEHICLE_SPAWN_POINT_COORD(iVSID, iSpawnCoordLoop)) < DISTANCE_FOR_EXPENSIVE_CHECK_SQR
						bPointBlocked = TRUE
					ENDIF
				ENDIF
			ENDIf
		ENDREPEAT
				
		IF NOT bPointBlocked
			SET_BIT(iReturnBitset, iSpawnCoordLoop)
		ENDIF
		
	ENDREPEAT
	
	RETURN iReturnBitset
ENDFUNC

/// PURPOSE:
///    Sets up the cooldown timer for vehicle spawning at a specified vehicle spawn point
/// PARAMS:
///    iVSID - vehicle spawn point to start cooldown at
PROC SERVER_START_VEHICLE_SPAWN_POINT_COOLDOWN(INT iVSID)
	iLocalServerSpawnPointCooldown[iVSID] = GET_GAME_TIMER() + TIME_FOR_VEHICLE_SPAWN_COOLDOWN
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN SERVER === SERVER_START_VEHICLE_SPAWN_POINT_COOLDOWN(", iVSID, ") = ", iLocalServerSpawnPointCooldown[iVSID])
	#ENDIF
ENDPROC

/// PURPOSE:
///    Returns if the specified vehicle spawn point is still on cooldown for spawning a vehicle there
/// PARAMS:
///    iVSID - vehicle spawn point to check
/// RETURNS:
///    true/false
FUNC BOOL SERVER_IS_VEHICLE_SPAWN_POINT_ON_COOLDOWN(INT iVSID)
	
	IF GET_GAME_TIMER() < iLocalServerSpawnPointCooldown[iVSID]
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Clears array of server-stored player vehicle spawn requests
PROC SERVER_CLEAR_ALL_PLAYER_VEHICLE_SPAWN_REQUESTS()
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN SERVER === SERVER_CLEAR_ALL_PLAYER_VEHICLE_SPAWN_REQUESTS")
	#ENDIF
	
	INT iPlayer, iRequest
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		
		REPEAT NUMBER_OF_SPAWN_REQUESTS iRequest
		
			iLocalServerPlayerRequest[iPlayer][iRequest] = -1
			
		ENDREPEAT
		
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Clears array of sever-stored vehicle spawn requests for a specified player
/// PARAMS:
///    iPlayerID - player ID to clear vehicle requests of
PROC SERVER_CLEAR_PLAYER_VEHICLE_SPAWN_REQUESTS(INT iPlayerID)

	INT iRequest
		
	REPEAT NUMBER_OF_SPAWN_REQUESTS iRequest
	
		iLocalServerPlayerRequest[iPlayerID][iRequest] = -1
		
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Server stores that a player is requesting a vehicle spawn at a specified vehicle spawn point
/// PARAMS:
///    iPlayerID - player ID to store request for
///    iSpawnReq - spawn request ID to store the request in
///    iVSID - vehicle spawn point ID that request is for
PROC SERVER_SET_PLAYER_VEHICLE_SPAWN_REQUEST(INT iPlayerID, INT iSpawnReq, INT iVSID)
	IF iLocalServerPlayerRequest[iPlayerID][iSpawnReq] <> iVSID
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN SERVER === SERVER_SET_PLAYER_VEHICLE_SPAWN_REQUEST(", iPlayerID, ", ", iSpawnReq, ", ", iVSID, ")")
		#ENDIF
		iLocalServerPlayerRequest[iPlayerID][iSpawnReq] = iVSID
	ENDIF
ENDPROC

/// PURPOSE:
///    Server clears a vehicle spawn request for a specified player and spawn request ID
/// PARAMS:
///    iPlayerID - player ID to clear the spawn request for
///    iSpawnReq - spawn request ID to clear
PROC SERVER_CLEAR_PLAYER_VEHICLE_SPAWN_REQUEST(INT iPlayerID, INT iSpawnReq)
	IF iLocalServerPlayerRequest[iPlayerID][iSpawnReq] <> -1
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN SERVER === SERVER_CLEAR_PLAYER_VEHICLE_SPAWN_REQUEST(", iPlayerID, ",", iSpawnReq, ")")
		#ENDIF
		iLocalServerPlayerRequest[iPlayerID][iSpawnReq] = -1
	ENDIF
ENDPROC

/// PURPOSE:
///    Server returns the vehicle spawn point ID for a vehicle spawn request for a specified player ID and spawn request ID
/// PARAMS:
///    iPlayerID - player ID to get the spawn request for
///    iSpawnReq - spawn request ID to get vehicle spawn point ID for
/// RETURNS:
///    vehicle spawn point ID as an INT
FUNC INT SERVER_GET_PLAYER_VEHICLE_SPAWN_REQUEST(INT iPlayerID, INT iSpawnReq)
	RETURN iLocalServerPlayerRequest[iPlayerID][iSpawnReq]
ENDFUNC

/// PURPOSE:
///    Server sets the specified player as having permission to spawn a vehicle at the specified vehicle spawn point ID
/// PARAMS:
///    iPlayerID - player ID to set the spawn as belonging to
///    iVSID - vehicle spawn point ID to set belonging to the player
PROC SERVER_SET_PLAYER_OWNS_THIS_SPAWN_POINT(INT iPlayerID, INT iVSID)
	IF iPlayerID > -1
	AND IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(iPlayerID), FALSE)
		serverBD.iPlayerThatOwnsSpawnPoint[iVSID] = iPlayerID
		
		serverBD.timePlayerSpawnAbandon[iPlayerID] = timeNet
		
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN SERVER === SERVER_SET_PLAYER_OWNS_THIS_SPAWN_POINT(", 
									GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iPlayerID)), ", ", iVSID, ")")
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN SERVER === Called with invalid player SERVER_SET_PLAYER_OWNS_THIS_SPAWN_POINT(", iPlayerID, ", ", iVSID, ")")
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Server clears the specified vehicle spawn point spawn permissions belonging to anyone
/// PARAMS:
///    iVSID - vehicle spawn point ID to clear permissions from
PROC SERVER_CLEAR_SPAWN_POINT_OWNED(INT iVSID)
	IF serverBD.iPlayerThatOwnsSpawnPoint[iVSID] <> -1
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN SERVER === SERVER_CLEAR_SPAWN_POINT_OWNED(", iVSID, ")")
		#ENDIF
		serverBD.iPlayerThatOwnsSpawnPoint[iVSID] = -1
		SERVER_IS_VEHICLE_SPAWN_POINT_ON_COOLDOWN(iVSID)
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns if the specified player has spawning permissions from the server to spawn at the specified vehicle spawn point
/// PARAMS:
///    iPlayerID - player ID to check
///    iVSID - vehicle spawn point ID to check
/// RETURNS:
///    true/false
FUNC BOOL DOES_PLAYER_OWN_THIS_SPAWN_POINT(INT iPlayerID, INT iVSID)
	IF serverBD.iPlayerThatOwnsSpawnPoint[iVSID] = iPlayerID
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns if anyone has spawning permissions from the server to spawn at the specified vehicle spawn point
/// PARAMS:
///    iVSID - vehicle spawn point ID to check
/// RETURNS:
///    true/false
FUNC BOOL IS_THIS_SPAWN_POINT_OWNED(INT iVSID)
	IF serverBD.iPlayerThatOwnsSpawnPoint[iVSID] > -1
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Server instantly clears if the vehicle spawning permissions for a specified vehicle spawn point belong to anyone, with no regard for if they are actually owned or not
/// PARAMS:
///    iVSID - vehicle spawn point ID to force clear
///    bCooldown - Seems to perform an arbitrary check if the spawn point is on cooldown, but does nothing with it?
PROC SERVER_FORCE_CLEAR_THIS_SPAWN_POINT_OWNED(INT iVSID, BOOL bCooldown = TRUE)
	serverBD.iPlayerThatOwnsSpawnPoint[iVSID] = -1
	IF bCooldown
		SERVER_IS_VEHICLE_SPAWN_POINT_ON_COOLDOWN(iVSID)
	ENDIF
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN SERVER === SERVER_FORCE_CLEAR_THIS_SPAWN_POINT_OWNED(", iVSID, ")")
	#ENDIF
ENDPROC

/// PURPOSE:
///    Server loops through all the vehicle spawn points and instantly clears any vehicle spawn permissions with no regard for if any are actually belonging to any players
PROC SERVER_FORCE_CLEAR_ALL_SPAWN_POINTS()
	INT iVSID
	
	REPEAT NUMBER_OF_VEHICLE_SPAWN_POINTS iVSID
		SERVER_FORCE_CLEAR_THIS_SPAWN_POINT_OWNED(iVSID, FALSE)
		SERVER_IS_VEHICLE_SPAWN_POINT_ON_COOLDOWN(iVSID)
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN SERVER === SERVER_FORCE_CLEAR_ALL_SPAWN_POINTS")
	#ENDIF
ENDPROC

/// PURPOSE:
///    Server copies the vehicle spawn requests for the specified player to use to decide who has vehicle spawn permissions
/// PARAMS:
///    iPlayerID - player ID to copy requests from
PROC SERVER_COPY_PLAYER_VEHICLE_SPAWN_REQUESTS(INT iPlayerID)
	IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(iPlayerID), FALSE)
	AND NOT IS_PLAYER_SCTV(INT_TO_PLAYERINDEX(iPlayerID))
		INT iRequest
		
		REPEAT NUMBER_OF_SPAWN_REQUESTS iRequest
			IF IS_BIT_SET(playerBD[iPlayerID].spawnRequest[iRequest].iBitset, REQUEST_BS_ACTIVE)
				SERVER_SET_PLAYER_VEHICLE_SPAWN_REQUEST(iPlayerID, iRequest, playerBD[iPlayerID].spawnRequest[iRequest].iSpawnPointID)
			ELSE
				SERVER_CLEAR_PLAYER_VEHICLE_SPAWN_REQUEST(iPlayerID, iRequest)
			ENDIF
		ENDREPEAT
		
	ELSE
		SERVER_CLEAR_PLAYER_VEHICLE_SPAWN_REQUESTS(iPlayerID)
	ENDIF
ENDPROC

/// PURPOSE:
///    Server checks the specified player's spawn requests and grants permission to spawn at the vehicle spawn point if suitable
/// PARAMS:
///    iPlayerID - player ID to proess permissions for
PROC SERVER_CHECK_VEHICLE_SPAWN_PLAYER(INT iPlayerID)
	INT iRequestID
	REPEAT NUMBER_OF_SPAWN_REQUESTS iRequestID
		IF SERVER_GET_PLAYER_VEHICLE_SPAWN_REQUEST(iPlayerID, iRequestID) <> -1
			IF NOT IS_THIS_SPAWN_POINT_OWNED(SERVER_GET_PLAYER_VEHICLE_SPAWN_REQUEST(iPlayerID, iRequestID))
				IF NOT SERVER_IS_VEHICLE_SPAWN_POINT_ON_COOLDOWN(SERVER_GET_PLAYER_VEHICLE_SPAWN_REQUEST(iPlayerID, iRequestID))
				
					SERVER_SET_PLAYER_OWNS_THIS_SPAWN_POINT(iPlayerID, SERVER_GET_PLAYER_VEHICLE_SPAWN_REQUEST(iPlayerID, iRequestID))
				
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Server reacts to a spawn request event by storing the request if suitable
PROC SERVER_REACT_TO_SPAWN_REQUEST_EVENT()
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN SERVER === SERVER_REACT_TO_SPAWN_REQUEST_EVENT")
	CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN SERVER === g_iVehicleSpawnRequestPlayer = ", g_iVehicleSpawnRequestPlayer)
	CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN SERVER === g_iVehicleSpawnRequestRequest = ", g_iVehicleSpawnRequestRequest)
	CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN SERVER === g_iVehicleSpawnRequestVSID = ", g_iVehicleSpawnRequestVSID)
	#ENDIF

	IF g_iVehicleSpawnRequestPlayer > -1
	AND g_iVehicleSpawnRequestPlayer < NUM_NETWORK_PLAYERS
		IF g_iVehicleSpawnRequestVSID > -1
		AND g_iVehicleSpawnRequestVSID < NUMBER_OF_VEHICLE_SPAWN_POINTS
			IF g_iVehicleSpawnRequestRequest > -1
			AND g_iVehicleSpawnRequestPlayer < NUMBER_OF_SPAWN_REQUESTS
				SERVER_SET_PLAYER_VEHICLE_SPAWN_REQUEST(g_iVehicleSpawnRequestPlayer, g_iVehicleSpawnRequestRequest, g_iVehicleSpawnRequestVSID)
				iLocalServerPlayerStagger = g_iVehicleSpawnRequestPlayer
			ENDIF
		ENDIF
	ENDIF
	
	g_iVehicleSpawnRequestPlayer = -1
	g_iVehicleSpawnRequestRequest = -1
	g_iVehicleSpawnRequestVSID = -1
ENDPROC

/// PURPOSE:
///    Server staggers through the player list, storing player spawn requests and granting spawning permissions where suitable
PROC MAINTAIN_SERVER_PLAYER_STAGGER()
	
	IF g_iVehicleSpawnRequestPlayer <> -1	//react to request event
	OR g_iVehicleSpawnRequestVSID <> -1
	OR g_iVehicleSpawnRequestRequest <> -1
		SERVER_REACT_TO_SPAWN_REQUEST_EVENT()
	ELSE
		SERVER_COPY_PLAYER_VEHICLE_SPAWN_REQUESTS(iLocalServerPlayerStagger)
	ENDIF
	
	SERVER_CHECK_VEHICLE_SPAWN_PLAYER(iLocalServerPlayerStagger)
	
	iLocalServerPlayerStagger++
	IF iLocalServerPlayerStagger >= NUM_NETWORK_PLAYERS
		
		iLocalServerPlayerStagger = 0
	ENDIF
ENDPROC

/// PURPOSE:
///    Server processes a specified vehicle spawn point, checking if the current spawn point owner is still suitable and clearing if not
/// PARAMS:
///    iVSID - vehicle spawn point ID to process
PROC SERVER_CHECK_VEHICLE_SPAWN_POINT(INT iVSID)
	IF serverBD.iPlayerThatOwnsSpawnPoint[iVSID] > -1
		IF NOT IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(serverBD.iPlayerThatOwnsSpawnPoint[iVSID]), FALSE)
		
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN SERVER === player ID has become invalid, SERVER_CLEAR_SPAWN_POINT_OWNED(", 
										iVSID, ")")
			#ENDIF
			
			SERVER_CLEAR_SPAWN_POINT_OWNED(iVSID)
			
		ELSE
			//check if player has an active request at point
			BOOL bHasReservation = FALSE
			INT i
			REPEAT NUMBER_OF_SPAWN_REQUESTS i
			
				IF SERVER_GET_PLAYER_VEHICLE_SPAWN_REQUEST(serverBD.iPlayerThatOwnsSpawnPoint[iVSID], i) = iVSID
					bHasReservation = TRUE
				ENDIF
				
			ENDREPEAT
			
			IF NOT bHasReservation
				SERVER_CLEAR_SPAWN_POINT_OWNED(iVSID)
			ENDIF
			
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Server reacts to a player sending an event that they have succesfully completed a spawn request. Clears spawn point ownership and local copy of the spawn request.
PROC SERVER_REACT_TO_SPAWN_COMPLETE_EVENT()
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN SERVER === SERVER_REACT_TO_SPAWN_COMPLETE_EVENT")
	CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN SERVER === g_iVehicleSpawnCompletePlayer = ", g_iVehicleSpawnCompletePlayer)
	CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN SERVER === g_iVehicleSpawnCompleteRequest = ", g_iVehicleSpawnCompleteRequest)
	CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN SERVER === g_iVehicleSpawnCompleteVSID = ", g_iVehicleSpawnCompleteVSID)
	#ENDIF

	IF g_iVehicleSpawnCompletePlayer > -1
	AND g_iVehicleSpawnCompletePlayer < NUM_NETWORK_PLAYERS
		IF g_iVehicleSpawnCompleteVSID > -1
		AND g_iVehicleSpawnCompleteVSID < NUMBER_OF_VEHICLE_SPAWN_POINTS
			IF g_iVehicleSpawnCompleteRequest > -1
			AND g_iVehicleSpawnCompleteRequest < NUMBER_OF_SPAWN_REQUESTS
				SERVER_CLEAR_PLAYER_VEHICLE_SPAWN_REQUEST(g_iVehicleSpawnCompletePlayer, g_iVehicleSpawnCompleteRequest)
				SERVER_CLEAR_SPAWN_POINT_OWNED(g_iVehicleSpawnCompleteVSID)
			ENDIF
		ENDIF
	ENDIF
	
	g_iVehicleSpawnCompletePlayer = -1
	g_iVehicleSpawnCompleteVSID = -1
	g_iVehicleSpawnCompleteRequest = -1
ENDPROC

/// PURPOSE:
///    Server staggers through the list of spawn points, making sure that spawning permissions are still owned by suitable players
PROC MAINTAIN_SERVER_SPAWN_POINT_STAGGER()
	
	IF g_iVehicleSpawnCompletePlayer <> -1	//react to request event
	OR g_iVehicleSpawnCompleteVSID <> -1
	OR g_iVehicleSpawnCompleteRequest <> -1
		SERVER_REACT_TO_SPAWN_COMPLETE_EVENT()
	ENDIF
	
	SERVER_CHECK_VEHICLE_SPAWN_POINT(iLocalServerSpawnPointStagger)
	
	iLocalServerSpawnPointStagger++
	IF iLocalServerSpawnPointStagger >= NUMBER_OF_VEHICLE_SPAWN_POINTS
		
		iLocalServerSpawnPointStagger = 0
	ENDIF
ENDPROC

/// PURPOSE:
///    Server every-frame processing of players and spawn points
PROC SERVER_PROCESSING()
	MAINTAIN_SERVER_PLAYER_STAGGER()
	MAINTAIN_SERVER_SPAWN_POINT_STAGGER()
ENDPROC

/// PURPOSE:
///    Returns the total number of current spawn requests of the local player
/// RETURNS:
///    number of spawn requests as an INT
FUNC INT GET_NUMBER_OF_SPAWN_REQUESTS()
	INT iPlayerID = NATIVE_TO_INT(PLAYER_ID())
	
	INT iReturnValue = 0
	iReturnValue += playerBD[iPlayerID].iNumberOfSpawnRequestsActive
	
	IF IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_BOOKED)
		iReturnValue += 1
	ENDIF
	
	RETURN iReturnValue
ENDFUNC

/// PURPOSE:
///    Gets what bitset to use for a mutliple-bitset flag system entry
/// PARAMS:
///    i - entry across all bitsets to get bitset for
/// RETURNS:
///    bitset for flag i
FUNC INT GET_BITSET_ID_FOR_INT(INT i)
	RETURN FLOOR(TO_FLOAT(i)/32)
ENDFUNC

/// PURPOSE:
///    Gets what bit within the bitset selected by GET_BITSET_ID_FOR_INT to use
/// PARAMS:
///    i - entry across all bitsets to get bit for
/// RETURNS:
///    bit for flag i
FUNC INT GET_BIT_FOR_INT(INT i)
	RETURN i%32
ENDFUNC

/// PURPOSE:
///    Flags in the player broadcast data that the local player is at a specified spawn point
/// PARAMS:
///    iVSID - Spawn point ID that the local player is now at
PROC SET_PLAYER_BD_AT_SPAWN_POINT(INT iVSID)
	INT iPlayerID = NATIVE_TO_INT(PLAYER_ID())
	IF NOT IS_BIT_SET(playerBD[iPlayerID].iAtSpawnPointBitset[GET_BITSET_ID_FOR_INT(iVSID)], GET_BIT_FOR_INT(iVSID))
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_PLAYER_BD_AT_SPAWN_POINT(", iVSID, ")")
		#ENDIF
		SET_BIT(playerBD[iPlayerID].iAtSpawnPointBitset[GET_BITSET_ID_FOR_INT(iVSID)], GET_BIT_FOR_INT(iVSID))
	ENDIF
ENDPROC

/// PURPOSE:
///    Clears flag in player broadcast data that the local player is at a specified spawn point
/// PARAMS:
///    iVSID - Spawn point ID that the local player is no longer at
PROC CLEAR_PLAYER_BD_AT_SPAWN_POINT(INT iVSID)
	INT iPlayerID = NATIVE_TO_INT(PLAYER_ID())
	IF IS_BIT_SET(playerBD[iPlayerID].iAtSpawnPointBitset[GET_BITSET_ID_FOR_INT(iVSID)], GET_BIT_FOR_INT(iVSID))
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === CLEAR_PLAYER_BD_AT_SPAWN_POINT(", iVSID, ")")
		#ENDIF
		CLEAR_BIT(playerBD[iPlayerID].iAtSpawnPointBitset[GET_BITSET_ID_FOR_INT(iVSID)], GET_BIT_FOR_INT(iVSID))
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns if the local player is at a specified vehicle spawn point
/// PARAMS:
///    iVSID - spawn point ID to check
/// RETURNS:
///    true/false
FUNC BOOL IS_PLAYER_BD_AT_SPAWN_POINT(INT iVSID)
	INT iPlayerID = NATIVE_TO_INT(PLAYER_ID())
	RETURN IS_BIT_SET(playerBD[iPlayerID].iAtSpawnPointBitset[GET_BITSET_ID_FOR_INT(iVSID)], GET_BIT_FOR_INT(iVSID))
ENDFUNC

/// PURPOSE:
///    Stores locally that the local player has a spawn request at the specified spawn point
/// PARAMS:
///    iVSID - spawn point ID that the local player has a spawn request for
PROC SET_LOCAL_HAS_REQUEST_AT_SPAWN_POINT(INT iVSID)
	IF NOT IS_BIT_SET(iLocalClientRequestSpawnPointBitset[GET_BITSET_ID_FOR_INT(iVSID)], GET_BIT_FOR_INT(iVSID))
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_LOCAL_HAS_REQUEST_AT_SPAWN_POINT(", iVSID, ")")
		#ENDIF
		SET_BIT(iLocalClientRequestSpawnPointBitset[GET_BITSET_ID_FOR_INT(iVSID)], GET_BIT_FOR_INT(iVSID))
	ENDIF
ENDPROC

/// PURPOSE:
///    Clears locally that the local player has a spawn request at the specified spawn point
/// PARAMS:
///    iVSID - spawn point ID that should be cleared
PROC CLEAR_LOCAL_HAS_REQUEST_AT_SPAWN_POINT(INT iVSID)
	IF iVSID >= 0
		IF IS_BIT_SET(iLocalClientRequestSpawnPointBitset[GET_BITSET_ID_FOR_INT(iVSID)], GET_BIT_FOR_INT(iVSID))
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === CLEAR_LOCAL_HAS_REQUEST_AT_SPAWN_POINT(", iVSID, ")")
			#ENDIF
			CLEAR_BIT(iLocalClientRequestSpawnPointBitset[GET_BITSET_ID_FOR_INT(iVSID)], GET_BIT_FOR_INT(iVSID))
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns if the local player has a vehicle spawn request at the specified spawn point
/// PARAMS:
///    iVSID - spawn point ID to check
/// RETURNS:
///    true/false
FUNC BOOL IS_LOCAL_HAS_REQUEST_AT_SPAWN_POINT(INT iVSID)
	RETURN IS_BIT_SET(iLocalClientRequestSpawnPointBitset[GET_BITSET_ID_FOR_INT(iVSID)], GET_BIT_FOR_INT(iVSID))
ENDFUNC

/// PURPOSE:
///    Clears the player broadcast data for the local player's specified request. Sends an event that the spawn request has been completed
/// PARAMS:
///    iRequestID - request ID to clear
PROC RESET_CLIENT_SPAWN_REQUEST(INT iRequestID)
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === RESET_CLIENT_SPAWN_REQUEST(", iRequestID, ")")
	#ENDIF
	
	INT iPlayer = NATIVE_TO_INT(PLAYER_ID())
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === 	BROADCAST_SERVER_VEHICLE_SPAWN_COMPLETE(ALL_PLAYERS(), ", iPlayer, ", ", iRequestID, 
															", ", playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, ")")
	#ENDIF
	BROADCAST_SERVER_VEHICLE_SPAWN_COMPLETE(ALL_PLAYERS(), iPlayer, iRequestID, playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID)
	
	IF IS_BIT_SET(playerBD[iPlayer].spawnRequest[iRequestID].iBitset, REQUEST_BS_RESERVED)
		RESERVE_NETWORK_MISSION_VEHICLES(GET_NUM_RESERVED_MISSION_VEHICLES()-1)
	ENDIF
	
	CLEAR_LOCAL_HAS_REQUEST_AT_SPAWN_POINT(playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID)
	
	playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID = -1
	playerBD[iPlayer].spawnRequest[iRequestID].iBitset = 0
	playerBD[iPlayer].spawnRequest[iRequestID].iFailCounter = 0
	IF playerBD[iPlayer].spawnRequest[iRequestID].mnSpawnModel <> DUMMY_MODEL_FOR_SCRIPT
		SET_MODEL_AS_NO_LONGER_NEEDED(playerBD[iPlayer].spawnRequest[iRequestID].mnSpawnModel)
	ENDIF
	playerBD[iPlayer].spawnRequest[iRequestID].mnSpawnModel = DUMMY_MODEL_FOR_SCRIPT
	
	IF playerBD[iPlayer].iNumberOfSpawnRequestsActive > 0
		playerBD[iPlayer].iNumberOfSpawnRequestsActive--
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === playerBD[", iPlayer, "].iNumberOfSpawnRequestsActive = ", 
									playerBD[iPlayer].iNumberOfSpawnRequestsActive)
		#ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Local player staggers through all spawn points, processing if they are in proximity or not and creating spawn requests when suitable for ambient spawning
PROC MAINTAIN_CLIENT_SPAWN_POINT_STAGGER()
	FLOAT fSpawnDistance
	
	BOOL bPreventForCrimDam

	IF IS_ENTITY_DEAD(PLAYER_PED_ID())
	ENDIF
	
	INT iPlayer = NATIVE_TO_INT(PLAYER_ID())
	
	IF IS_POINT_NEAR_POINT(GET_VEHICLE_SPAWN_POINT_CENTRE(iLocalClientSpawnPointStagger),<<1752.6299, 3241.0481, 40.8582>>,50.0)
		fSpawnDistance = SPAWN_DISTANCE_FOR_LARGE_VEHICLE_SQR
	ELSE
		fSpawnDistance = APPROACH_DISTANCE_FOR_SPAWN_POINT_SQR
	ENDIF
	
	//if at spawn point, or booked this spawn and not made request yet
	IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_VEHICLE_SPAWN_POINT_CENTRE(iLocalClientSpawnPointStagger)) < fSpawnDistance
	
		BOOL bShouldSpawn = FALSE
		BOOL bIsBooking = FALSE
	
		IF NOT IS_PLAYER_BD_AT_SPAWN_POINT(iLocalClientSpawnPointStagger)
			SET_PLAYER_BD_AT_SPAWN_POINT(iLocalClientSpawnPointStagger)		//i'm at this point but wasnt before, test to spawn ambient
			
			IF NOT bShouldSpawn
				//If this is a suitable ambient spawn, spawn
				IF NOT IS_PLAYER_IN_BLOCKING_ACTIVITY(PLAYER_ID())	
				AND CAN_PLAYER_USE_VEHICLE_SPAWN_POINT(PLAYER_ID(), iLocalClientSpawnPointStagger)
				AND (NOT IS_LOCAL_HAS_REQUEST_AT_SPAWN_POINT(iLocalClientSpawnPointStagger))	//if not already got a request for this point
				AND GET_NUMBER_OF_SPAWN_REQUESTS() < NUMBER_OF_SPAWN_REQUESTS	//and not already requested or completed as may spawns as can be
				AND NOT(GET_VEHICLE_SPAWN_POINT_MODEL_TO_SPAWN_FOR_PLAYER(PLAYER_ID(), iLocalClientSpawnPointStagger) = DUMMY_MODEL_FOR_SCRIPT)
					bShouldSpawn = TRUE
				ENDIF
			ENDIF
			
		ENDIF
		
		//If this is the booking location, spawn
		IF (IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_BOOKED)		
		AND (NOT IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_REQUEST_MADE))
		AND (NOT IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_VEHICLE_EXISTS))
		AND bookedVehicle.iSpawnPointID = iLocalClientSpawnPointStagger)
			bShouldSpawn = TRUE
			bIsBooking = TRUE
		ENDIf
					
		IF bShouldSpawn
			
			INT i
			INT iRequestID = -1
			IF bIsBooking
				iRequestID = bookedVehicle.iRequestID
			ELSE
				REPEAT NUMBER_OF_SPAWN_REQUESTS i
					IF iRequestID = -1
						IF NOT IS_BIT_SET(playerBD[iPlayer].spawnRequest[i].iBitset, REQUEST_BS_ACTIVE)
						AND i <> bookedVehicle.iRequestID
							iRequestID = i
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
			
			IF iRequestID <> -1
						
				VEHICLE_SPAWN_REQUEST thisRequest
				
				thisRequest.iSpawnPointID = iLocalClientSpawnPointStagger
				thisRequest.iBitset = 0
				SET_BIT(thisRequest.iBitset, REQUEST_BS_ACTIVE)
				
				//If this is the booking location
				IF bIsBooking
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_BIT(playerBD[", iPlayer, "].spawnRequest[", iRequestID, "], REQUEST_BS_IS_BOOKING)")
					CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_BIT(bookedVehicle.iBitset, BOOK_BS_REQUEST_MADE)")
					#ENDIF
					SET_BIT(thisRequest.iBitset, REQUEST_BS_IS_BOOKING)
					SET_BIT(bookedVehicle.iBitset, BOOK_BS_REQUEST_MADE)
					
					IF IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_BLIP_CREATED_FOR_SPAWN_POINT)	//clear old blip marking spawn point, to be replaced with request info
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === CLEAR_BIT(bookedVehicle.iBitset, BOOK_BS_BLIP_CREATED_FOR_SPAWN_POINT)")
						#ENDIF
						CLEAR_BIT(bookedVehicle.iBitset, BOOK_BS_BLIP_CREATED_FOR_SPAWN_POINT)
					ENDIF
					
					thisRequest.mnSpawnModel = bookedVehicle.mnBookModel
					
				ELSE
					
					thisRequest.mnSpawnModel = GET_VEHICLE_SPAWN_POINT_MODEL_TO_SPAWN_FOR_PLAYER(PLAYER_ID(), iLocalClientSpawnPointStagger)
					
					IF NETWORK_IS_SCRIPT_ACTIVE(GET_MP_MISSION_NAME(eAM_CRIMINAL_DAMAGE))
					AND NOT IS_VEHICLE_SUITABLE_FOR_CRIMINAL_DAMAGE(thisRequest.mnSpawnModel)
						iRequestID = -1
						thisRequest.iSpawnPointID = -1
						thisRequest.iBitset = 0
						bPreventForCrimDam = TRUE
						CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAIN_CLIENT_SPAWN_POINT_STAGGER - [RE] - Criminal damage is active. Model '", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(thisRequest.mnSpawnModel), "' is not suitable. Reset request.")
					ENDIF
					
				ENDIF
				
				IF NOT bPreventForCrimDam
					playerBD[iPlayer].spawnRequest[iRequestID] = thisRequest
					
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === BROADCAST_SERVER_VEHICLE_SPAWN_REQUEST(ALL_PLAYERS(), ", iPlayer, ", ", iRequestID, 
																			", ", iLocalClientSpawnPointStagger, ")")
					#ENDIF
					BROADCAST_SERVER_VEHICLE_SPAWN_REQUEST(ALL_PLAYERS(), iPlayer, iRequestID, iLocalClientSpawnPointStagger)
					
					playerBD[iPlayer].iNumberOfSpawnRequestsActive++								//increase number of active requests
					
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === Request ", iRequestID, " added by ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iPlayer)))
					CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === iSpawnPointID = ", thisRequest.iSpawnPointID)
					CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === playerBD[", iPlayer, "].iNumberOfSpawnRequestsActive = ", 
												playerBD[iPlayer].iNumberOfSpawnRequestsActive)
					CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === playerBD[", iPlayer, "].spawnRequest[", iRequestID, "].mnSpawnModel = ", 
												GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(playerBD[iPlayer].spawnRequest[iRequestID].mnSpawnModel))
					#ENDIF
					
					
					SET_LOCAL_HAS_REQUEST_AT_SPAWN_POINT(iLocalClientSpawnPointStagger)		//mark in bitset that i have request here
				ENDIF
			ENDIF
		ENDIF
		
	//if we've left the area (slightly further to avoid snapping changes)	
	ELIF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_VEHICLE_SPAWN_POINT_CENTRE(iLocalClientSpawnPointStagger)) > LEAVE_DISTANCE_FOR_SPAWN_POINT_SQR
		IF IS_PLAYER_BD_AT_SPAWN_POINT(iLocalClientSpawnPointStagger)
			CLEAR_LOCAL_HAS_REQUEST_AT_SPAWN_POINT(iLocalClientSpawnPointStagger)
			CLEAR_PLAYER_BD_AT_SPAWN_POINT(iLocalClientSpawnPointStagger)		//i'm not at this point
		ENDIF
	ENDIF
	
	iLocalClientSpawnPointStagger++
	IF iLocalClientSpawnPointStagger >= NUMBER_OF_VEHICLE_SPAWN_POINTS
	
		//copy over process
		//none for this!
	
		iLocalClientSpawnPointStagger = 0
	ENDIF
ENDPROC

/// PURPOSE:
///    Processes local player's spawn requests and creates the vehicles when the local player has spawning permission for the request's spawn point (includes registering vehicle and loading models etc)
PROC MAINTAINT_CLIENT_REQUEST_SPAWNING()
	INT iPlayer = NETWORK_PLAYER_ID_TO_INT()
	INT iRequestID
	INT iSpawnCoordLoop
	BOOL bCompletedSpawn = FALSE
	FLOAT fBestDistance
	FLOAT fCurrentDistance
	FLOAT fSpawnDistance
	
	VECTOR vThisSpawnCoord
	
	INT iBestSpawnCoord
	
	BOOL bDoSightChecks = FALSE
	BOOL bDoDistanceChecks = FALSE
	BOOL bDoVehicleBlockingCheck = FALSE
	
	IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(PARTICIPANT_ID())
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAINT_CLIENT_REQUEST_SPAWNING - Local participant not active, exiting...")
		EXIT
	ENDIF
	
	REPEAT NUMBER_OF_SPAWN_REQUESTS iRequestID
		IF IS_BIT_SET(playerBD[iPlayer].spawnRequest[iRequestID].iBitset, REQUEST_BS_ACTIVE)
			IF NOT bLocalClientCurrentlySpawningRequests[iRequestID] 
				MODEL_NAMES mnVehicleModel = DUMMY_MODEL_FOR_SCRIPT
				
				#IF IS_DEBUG_BUILD
					IF bAdditionalSpew
						CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAINT_CLIENT_REQUEST_SPAWNING - [RE] - playerBD[",iPlayer,"].spawnRequest[",iRequestID,"].iBitset = ", IS_BIT_SET(playerBD[iPlayer].spawnRequest[iRequestID].iBitset, REQUEST_BS_IS_BOOKING))
					ENDIF
				#ENDIF
				
				IF IS_BIT_SET(playerBD[iPlayer].spawnRequest[iRequestID].iBitset, REQUEST_BS_IS_BOOKING)
					#IF IS_DEBUG_BUILD
					IF bAdditionalSpew
						CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAINT_CLIENT_REQUEST_SPAWNING - mnVehicleModel = bookedVehicle.mnBookModel")
					ENDIF
					#ENDIF
					mnVehicleModel = bookedVehicle.mnBookModel
				ELSE
					#IF IS_DEBUG_BUILD
					IF bAdditionalSpew
						CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAINT_CLIENT_REQUEST_SPAWNING - mnVehicleModel = playerBD[iPlayer].spawnRequest[iRequestID].mnSpawnModel")
					ENDIF
					#ENDIF
					mnVehicleModel = playerBD[iPlayer].spawnRequest[iRequestID].mnSpawnModel
				ENDIF	
				
				IF SHOULD_BLOCK_HELI_VEHICLE_SPAWN()
					IF IS_THIS_MODEL_A_HELI(mnVehicleModel)
						CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAINT_CLIENT_REQUEST_SPAWNING - SHOULD_BLOCK_HELI_VEHICLE_SPAWN = TRUE ")								
						SET_BIT(MPGlobals.g_iVehicleSpawnRequestFailed, iRequestID)
						mnVehicleModel = DUMMY_MODEL_FOR_SCRIPT
					ENDIF
				ENDIF
				
				IF mnVehicleModel <> DUMMY_MODEL_FOR_SCRIPT
					#IF IS_DEBUG_BUILD
					IF bAdditionalSpew
						CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAINT_CLIENT_REQUEST_SPAWNING - [RE] - iRequestID = ", iRequestID, " - Model = ", GET_MODEL_NAME_STRING(mnVehicleModel))
					ENDIF
					#ENDIF
					
					REQUEST_MODEL(mnVehicleModel)
					IF DOES_PLAYER_OWN_THIS_SPAWN_POINT(iPlayer, playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID)
						IF HAS_MODEL_LOADED(mnVehicleModel)
							IF NOT IS_BIT_SET(playerBD[iPlayer].spawnRequest[iRequestID].iBitset, REQUEST_BS_RESERVED)
							
								IF CAN_RESERVE_NETWORK_VEHICLES_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_VEHICLES()+1, FALSE, TRUE)
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === RESERVE_NETWORK_MISSION_VEHICLES(", GET_NUM_RESERVED_MISSION_VEHICLES()+1, ")", 
																", GET_NUM_CREATED_MISSION_VEHICLES() = ", GET_NUM_CREATED_MISSION_VEHICLES(FALSE))
									#ENDIF
									RESERVE_NETWORK_MISSION_VEHICLES(GET_NUM_RESERVED_MISSION_VEHICLES()+1)
									SET_BIT(playerBD[iPlayer].spawnRequest[iRequestID].iBitset, REQUEST_BS_RESERVED)
								ELSE
									CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAINT_CLIENT_REQUEST_SPAWNING - [RE] - CAN_RESERVE_NETWORK_VEHICLES_FOR_THIS_SCRIPT = FALSE")
								ENDIF
							ELSE
						
								IF CAN_REGISTER_MISSION_VEHICLES(1)
						
									IF NOT IS_BIT_SET(iLocalExpensiveBitset, LOCAL_EXPENSIVE_BS_CREATED)
										iLocalExpensiveBitset = CREATE_EXPENSIVE_BITSET(playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID)
										SET_BIT(iLocalExpensiveBitset, LOCAL_EXPENSIVE_BS_CREATED)
									ENDIF
									
									IF IS_BIT_SET(playerBD[iPlayer].spawnRequest[iRequestID].iBitset, REQUEST_BS_IGNORE_SIGHT_CHECKS)
										bDoSightChecks = FALSE	//try to spawn booking out of sight, then do it in wherever
									ELSE
										bDoSightChecks = TRUE
									ENDIF
									
									IF IS_BIT_SET(playerBD[iPlayer].spawnRequest[iRequestID].iBitset, REQUEST_BS_IS_BOOKING)
										bDoDistanceChecks = FALSE	
										bDoVehicleBlockingCheck = FALSE
									ELSE
										bDoDistanceChecks = TRUE	//spawn ambient vehicles within distance (so not instantly cleaned up)
										bDoVehicleBlockingCheck = TRUE
									ENDIF
									
									iBestSpawnCoord = -1
									
									REPEAT GET_NUMBER_OF_VEHICLE_SPAWN_POINT_COORDS(playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, (IS_BIT_SET(playerBD[iPlayer].spawnRequest[iRequestID].iBitset, REQUEST_BS_IS_BOOKING))) iSpawnCoordLoop
										#IF IS_DEBUG_BUILD
										IF bAdditionalSpew
											CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAINT_CLIENT_REQUEST_SPAWNING - [RE] - GET_NUMBER_OF_VEHICLE_SPAWN_POINT_COORDS = ", GET_NUMBER_OF_VEHICLE_SPAWN_POINT_COORDS(playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, (IS_BIT_SET(playerBD[iPlayer].spawnRequest[iRequestID].iBitset, REQUEST_BS_IS_BOOKING))), " - iSpawnPointID = ", playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, " - iSpawnCoordLoop = ", iSpawnCoordLoop)
										ENDIF
										#ENDIF
										
										IF IS_BIT_SET(iLocalExpensiveBitset, iSpawnCoordLoop)
											IF CHECK_SPECIAL_CASES_OF_VEHICLE_SPAWN_COORDS(playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, iSpawnCoordLoop, mnVehicleModel)
												
												IF NOT DOES_VEHICLE_NEED_BIG_SPAWN_COORD(mnVehicleModel)
												OR IS_VEHICLE_SPAWN_COORD_BIG(playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, iSpawnCoordLoop)
												
													IF NOT DOES_VEHICLE_NEED_REALLY_BIG_SPAWN_COORD(mnVehicleModel)
													OR IS_VEHICLE_SPAWN_COORD_REALLY_BIG(playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, iSpawnCoordLoop)
												
														vThisSpawnCoord = GET_VEHICLE_SPAWN_POINT_COORD(playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, iSpawnCoordLoop)
														#IF IS_DEBUG_BUILD
														IF bAdditionalSpew
															CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAINT_CLIENT_REQUEST_SPAWNING - [RE] - vThisSpawnCoord = ", vThisSpawnCoord)
														ENDIF
														#ENDIF
														
														IF IS_POINT_NEAR_POINT(vThisSpawnCoord,<<1752.6299, 3241.0481, 40.8582>>,50.0)
															fSpawnDistance = SPAWN_DISTANCE_FOR_LARGE_VEHICLE_SQR
		//													IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		//														SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1752.6299 -10, 3241.0481 -10, 40.8582 -10>>,<<1752.6299 +10, 3241.0481 +10, 40.8582 +10>>,FALSE,FALSE)
		//													ENDIF
														ELSE
															fSpawnDistance = SPAWN_DISTANCE_CHECK_SQR
														ENDIF
														
														IF NOT bDoDistanceChecks
														OR VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vThisSpawnCoord) < fSpawnDistance
														
															IF NOT bDoVehicleBlockingCheck
															OR CHECK_VECTOR_FOR_EVENT_BLOCKING(GET_VEHICLE_SPAWN_POINT_COORD(playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, iSpawnCoordLoop))
															
																FLOAT fVehicleRadius = 8.5
																IF IS_PLAYER_IN_LUX_JET_VEHICLE(mnVehicleModel)
																	fVehicleRadius = 13.0
																ENDIF
															
																IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(vThisSpawnCoord, fVehicleRadius, 5, 1, 5, bDoSightChecks, bDoSightChecks, bDoSightChecks)
																	IF NOT IsAnotherPlayerOrPlayerVehicleSpawningAtPosition(vThisSpawnCoord, 5.0, PLAYER_ID(), TRUE, TRUE)
																		IF iBestSpawnCoord = -1
																			iBestSpawnCoord = iSpawnCoordLoop
																			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAINT_CLIENT_REQUEST_SPAWNING - -1 iBestSpawnCoord = ",iBestSpawnCoord)
																		ELSE
																			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
																			ENDIF
																			fCurrentDistance = VDIST2(	GET_VEHICLE_SPAWN_POINT_COORD(playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, iSpawnCoordLoop),
																										GET_ENTITY_COORDS(PLAYER_PED_ID()))
																			fBestDistance = VDIST2(	GET_VEHICLE_SPAWN_POINT_COORD(playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, iBestSpawnCoord),
																									GET_ENTITY_COORDS(PLAYER_PED_ID()))
																			IF fCurrentDistance < fBestDistance
																				iBestSpawnCoord = iSpawnCoordLoop
																				CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAINT_CLIENT_REQUEST_SPAWNING - Using closer iBestSpawnCoord = ",iBestSpawnCoord)
																			ENDIF
																		ENDIF
																	ELSE
																		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAINT_CLIENT_REQUEST_SPAWNING - IsAnotherPlayerOrPlayerVehicleSpawningAtPosition")
																	ENDIF
																ELSE
																	CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAINT_CLIENT_REQUEST_SPAWNING - NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION")
																ENDIF
															ELSE
																CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAINT_CLIENT_REQUEST_SPAWNING - Point doesn't pass vehicle blocking checks.")
															ENDIF
														ELSE
															CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAINT_CLIENT_REQUEST_SPAWNING - Point doesn't pass distance checks.")
														ENDIF
													ELSE
														CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAINT_CLIENT_REQUEST_SPAWNING - Not really big spawn coord.")
														#IF IS_DEBUG_BUILD
														IF bAdditionalSpew
															CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAINT_CLIENT_REQUEST_SPAWNING - [RE] - IS_VEHICLE_SPAWN_COORD_REALLY_BIG = ", IS_VEHICLE_SPAWN_COORD_REALLY_BIG(playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, iSpawnCoordLoop))
															CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAINT_CLIENT_REQUEST_SPAWNING - [RE] - DOES_VEHICLE_NEED_REALLY_BIG_SPAWN_COORD = ", DOES_VEHICLE_NEED_REALLY_BIG_SPAWN_COORD(mnVehicleModel))
														ENDIF
														#ENDIF
													ENDIF
												ELSE
													CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAINT_CLIENT_REQUEST_SPAWNING - Not big spawn coord.")
													#IF IS_DEBUG_BUILD
													IF bAdditionalSpew
														CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAINT_CLIENT_REQUEST_SPAWNING - [RE] - IS_VEHICLE_SPAWN_COORD_BIG = ", IS_VEHICLE_SPAWN_COORD_BIG(playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, iSpawnCoordLoop))
														CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAINT_CLIENT_REQUEST_SPAWNING - [RE] - DOES_VEHICLE_NEED_BIG_SPAWN_COORD = ", DOES_VEHICLE_NEED_BIG_SPAWN_COORD(mnVehicleModel))
													ENDIF
													#ENDIF
												ENDIF
											ELSE
												CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAINT_CLIENT_REQUEST_SPAWNING - [RE] - CHECK_SPECIAL_CASES_OF_VEHICLE_SPAWN_COORDS = FALSE")
											ENDIF
										ELSE
											CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAINT_CLIENT_REQUEST_SPAWNING - [RE] - IS_BIT_SET(iLocalExpensiveBitset, iSpawnCoordLoop) = FALSE")
										ENDIF
									ENDREPEAT
									
									
									bCompletedSpawn = FALSE
									
									IF iBestSpawnCoord <> -1
									#IF IS_DEBUG_BUILD
									AND NOT bForceFailSpawn
									#ENDIF
										#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === Attempting to spawn request for ", 
																	GET_PLAYER_NAME(PLAYER_ID()), ", ReqID = ", iRequestID, ", SpawnID = ", 
																	playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, ", CoordID = ", 
																	iBestSpawnCoord, ", Model = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mnVehicleModel))
										#ENDIF
										
										IF IS_BIT_SET(playerBD[iPlayer].spawnRequest[iRequestID].iBitset, REQUEST_BS_IS_BOOKING)	
											IF (mnVehicleModel = GET_PLAYERS_GUNRUNNING_TRUCK_MODEL(INT_TO_PLAYERINDEX(iPlayer)) OR mnVehicleModel = TRAILERLARGE)
												IF CAN_SPAWN_BOOKED_VEHICLE(eVeh_spawn_3_TRUCK)
													SET_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_3_TRUCK)
													IF SPAWN_BOOKED_TRUCK_VEHICLE(
																			GET_VEHICLE_SPAWN_POINT_COORD(playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, iBestSpawnCoord),
																			GET_VEHICLE_SPAWN_POINT_HEADING(playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, iBestSpawnCoord),!bDoSightChecks)
														bookedVehicle.vCreatedCoords = GET_ENTITY_COORDS(MPGlobalsAmbience.vehTruckVehicle[0])
														
														IF IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_DELIVERY)
															DECOR_SET_BOOL(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]), "CreatedByPegasus", TRUE)
															SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]), TRUE)
															#IF IS_DEBUG_BUILD
															CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(vehCreate, TRUE)")
															#ENDIF
														ELSE
															SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]), FALSE)
															#IF IS_DEBUG_BUILD
															CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(vehCreate, FALSE)")
															#ENDIF
														ENDIF
														
														#IF IS_DEBUG_BUILD
														CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_TRUCK_VEHICLE( Model ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mnVehicleModel), 
																					", Spawn Point ", playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, ", Coord ", iBestSpawnCoord, ")")
														#ENDIF
														CLEAR_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_3_TRUCK)
														bCompletedSpawn = TRUE
													ENDIF
												ENDIF
											ELIF (mnVehicleModel = GET_ARMORY_AIRCRAFT_MODEL())
												IF CAN_SPAWN_BOOKED_VEHICLE(eVeh_spawn_4_AIRCRAFT)
													SET_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_4_AIRCRAFT)
													IF SPAWN_BOOKED_AIRCRAFT_VEHICLE(
																			GET_VEHICLE_SPAWN_POINT_COORD(playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, iBestSpawnCoord),
																			GET_VEHICLE_SPAWN_POINT_HEADING(playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, iBestSpawnCoord),!bDoSightChecks)
														bookedVehicle.vCreatedCoords = GET_ENTITY_COORDS(MPGlobalsAmbience.vehAvengerVehicle)
														
														IF IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_DELIVERY)
															DECOR_SET_BOOL(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV), "CreatedByPegasus", TRUE)
															SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV), TRUE)
															#IF IS_DEBUG_BUILD
															CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(vehCreate, TRUE)")
															#ENDIF
														ELSE
															SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV), FALSE)
															#IF IS_DEBUG_BUILD
															CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(vehCreate, FALSE)")
															#ENDIF
														ENDIF
														
														#IF IS_DEBUG_BUILD
														CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_AIRCRAFT_VEHICLE( Model ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mnVehicleModel), 
																					", Spawn Point ", playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, ", Coord ", iBestSpawnCoord, ")")
														#ENDIF
														CLEAR_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_4_AIRCRAFT)
														bCompletedSpawn = TRUE
													ENDIF
												ENDIF
											ELIF (mnVehicleModel = GET_HACKER_TRUCK_MODEL())
												IF CAN_SPAWN_BOOKED_VEHICLE(eVeh_spawn_5_HACKERTRUCK)
													SET_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_5_HACKERTRUCK)
													IF SPAWN_BOOKED_HACKER_TRUCK_VEHICLE(
																			GET_VEHICLE_SPAWN_POINT_COORD(playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, iBestSpawnCoord),
																			GET_VEHICLE_SPAWN_POINT_HEADING(playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, iBestSpawnCoord),!bDoSightChecks)
														bookedVehicle.vCreatedCoords = GET_ENTITY_COORDS(MPGlobalsAmbience.vehHackerTruck)
														
														IF IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_DELIVERY)
															DECOR_SET_BOOL(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV), "CreatedByPegasus", TRUE)
															SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV), TRUE)
															#IF IS_DEBUG_BUILD
															CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(vehCreate, TRUE)")
															#ENDIF
														ELSE
															SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV), FALSE)
															#IF IS_DEBUG_BUILD
															CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(vehCreate, FALSE)")
															#ENDIF
														ENDIF
														
														#IF IS_DEBUG_BUILD
														CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_HACKER_TRUCK_VEHICLE( Model ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mnVehicleModel), 
																					", Spawn Point ", playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, ", Coord ", iBestSpawnCoord, ")")
														#ENDIF
														CLEAR_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_5_HACKERTRUCK)
														bCompletedSpawn = TRUE
													ENDIF
												ENDIF
											ELIF (mnVehicleModel = GET_SUBMARINE_MODEL())
												IF CAN_SPAWN_BOOKED_VEHICLE(eVeh_spawn_6_SUBMARINE)
													SET_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_6_SUBMARINE)
													IF SPAWN_BOOKED_SUBMARINE_VEHICLE(
																			GET_VEHICLE_SPAWN_POINT_COORD(playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, iBestSpawnCoord),
																			GET_VEHICLE_SPAWN_POINT_HEADING(playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, iBestSpawnCoord),!bDoSightChecks)
														bookedVehicle.vCreatedCoords = GET_ENTITY_COORDS(MPGlobalsAmbience.vehSubmarine)
														
														IF IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_DELIVERY)
															DECOR_SET_BOOL(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineV), "CreatedByPegasus", TRUE)
															SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineV), TRUE)
															#IF IS_DEBUG_BUILD
															CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(vehCreate, TRUE)")
															#ENDIF
														ELSE
															SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineV), FALSE)
															#IF IS_DEBUG_BUILD
															CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(vehCreate, FALSE)")
															#ENDIF
														ENDIF
														
														#IF IS_DEBUG_BUILD
														CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_HACKER_TRUCK_VEHICLE( Model ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mnVehicleModel), 
																					", Spawn Point ", playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, ", Coord ", iBestSpawnCoord, ")")
														#ENDIF
														CLEAR_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_6_SUBMARINE)
														bCompletedSpawn = TRUE
													ENDIF
												ENDIF
											ELIF (mnVehicleModel = GET_SUBMARINE_DINGHY_MODEL())
												IF CAN_SPAWN_BOOKED_VEHICLE(eVeh_spawn_7_SUBMARINE_DINGHY)
													SET_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_7_SUBMARINE_DINGHY)
													IF SPAWN_BOOKED_SUBMARINE_DINGHY_VEHICLE(
																			GET_VEHICLE_SPAWN_POINT_COORD(playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, iBestSpawnCoord),
																			GET_VEHICLE_SPAWN_POINT_HEADING(playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, iBestSpawnCoord),!bDoSightChecks)
														bookedVehicle.vCreatedCoords = GET_ENTITY_COORDS(MPGlobalsAmbience.vehSubmarineDinghy)
														
														IF IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_DELIVERY)
															DECOR_SET_BOOL(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineDinghyV), "CreatedByPegasus", TRUE)
															SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineDinghyV), TRUE)
															#IF IS_DEBUG_BUILD
															CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(vehCreate, TRUE)")
															#ENDIF
														ELSE
															SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineDinghyV), FALSE)
															#IF IS_DEBUG_BUILD
															CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(vehCreate, FALSE)")
															#ENDIF
														ENDIF
														
														#IF IS_DEBUG_BUILD
														CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_SUBMARINE_DINGHY_VEHICLE( Model ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mnVehicleModel), 
																					", Spawn Point ", playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, ", Coord ", iBestSpawnCoord, ")")
														#ENDIF
														CLEAR_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_7_SUBMARINE_DINGHY)
														bCompletedSpawn = TRUE
													ENDIF
												ENDIF
											#ENDIF
											#IF FEATURE_DLC_2_2022
											ELIF (mnVehicleModel = GET_SUPPORT_BIKE_MODEL())
												IF CAN_SPAWN_BOOKED_VEHICLE(eVeh_spawn_9_SUPPORT_BIKE)
													SET_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_9_SUPPORT_BIKE)
													IF SPAWN_BOOKED_SUPPORT_BIKE_VEHICLE(
																			GET_VEHICLE_SPAWN_POINT_COORD(playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, iBestSpawnCoord),
																			GET_VEHICLE_SPAWN_POINT_HEADING(playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, iBestSpawnCoord),!bDoSightChecks)
														bookedVehicle.vCreatedCoords = GET_ENTITY_COORDS(MPGlobalsAmbience.vehSupportBike)
														
														IF IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_DELIVERY)
															DECOR_SET_BOOL(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SupportBikeV), "CreatedByPegasus", TRUE)
															SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SupportBikeV), TRUE)
															#IF IS_DEBUG_BUILD
															CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(vehCreate, TRUE)")
															#ENDIF
														ELSE
															SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SupportBikeV), FALSE)
															#IF IS_DEBUG_BUILD
															CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(vehCreate, FALSE)")
															#ENDIF
														ENDIF
														
														#IF IS_DEBUG_BUILD
														CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_SUPPORT_BIKE_VEHICLE( Model ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mnVehicleModel), 
																					", Spawn Point ", playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, ", Coord ", iBestSpawnCoord, ")")
														#ENDIF
														CLEAR_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_9_SUPPORT_BIKE)
														bCompletedSpawn = TRUE
													ENDIF
												ENDIF
											ELIF (mnVehicleModel = GET_ACID_LAB_MODEL())
												IF CAN_SPAWN_BOOKED_VEHICLE(eVeh_spawn_8_ACIDLAB)
													SET_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_8_ACIDLAB)
													IF SPAWN_BOOKED_ACID_LAB_VEHICLE(
																			GET_VEHICLE_SPAWN_POINT_COORD(playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, iBestSpawnCoord),
																			GET_VEHICLE_SPAWN_POINT_HEADING(playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, iBestSpawnCoord),!bDoSightChecks)
														bookedVehicle.vCreatedCoords = GET_ENTITY_COORDS(MPGlobalsAmbience.vehAcidLab)
														
														IF IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_DELIVERY)
															DECOR_SET_BOOL(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV), "CreatedByPegasus", TRUE)
															SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV), TRUE)
															#IF IS_DEBUG_BUILD
															CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(vehCreate, TRUE)")
															#ENDIF
														ELSE
															SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV), FALSE)
															#IF IS_DEBUG_BUILD
															CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(vehCreate, FALSE)")
															#ENDIF
														ENDIF
														
														#IF IS_DEBUG_BUILD
														CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_ACID_LAB_VEHICLE( Model ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mnVehicleModel), 
																					", Spawn Point ", playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, ", Coord ", iBestSpawnCoord, ")")
														#ENDIF
														CLEAR_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_8_ACIDLAB)
														bCompletedSpawn = TRUE
													ENDIF
												ENDIF
											#ENDIF	
											ELSE
												IF CAN_SPAWN_BOOKED_VEHICLE(eVeh_spawn_2_PEGASUS)
													SET_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_2_PEGASUS)
													IF SPAWN_BOOKED_PEGASUS_VEHICLE(mnVehicleModel, 
																			GET_VEHICLE_SPAWN_POINT_COORD(playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, iBestSpawnCoord),
																			GET_VEHICLE_SPAWN_POINT_HEADING(playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, iBestSpawnCoord),!bDoSightChecks)
														#IF IS_DEBUG_BUILD
														CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_PEGASUS_VEHICLE( Model ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mnVehicleModel), 
																					", Spawn Point ", playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, ", Coord ", iBestSpawnCoord, ")")
														#ENDIF
														CLEAR_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_2_PEGASUS)
														bCompletedSpawn = TRUE
													ENDIF
												ENDIF
											ENDIF
										ELSE
											IF CAN_SPAWN_BOOKED_VEHICLE(eVeh_spawn_1_PERSONAL)
												SET_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_1_PERSONAL)
												IF SPAWN_VEHICLE(	mnVehicleModel, 
																	GET_VEHICLE_SPAWN_POINT_COORD(playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, iBestSpawnCoord),
																	GET_VEHICLE_SPAWN_POINT_HEADING(playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, iBestSpawnCoord),!bDoSightChecks)
													#IF IS_DEBUG_BUILD
													CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_VEHICLE( Model ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mnVehicleModel), 
																				", Spawn Point ", playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID, ", Coord ", iBestSpawnCoord, ")")
													#ENDIF
													CLEAR_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_1_PERSONAL)
													bCompletedSpawn = TRUE
												ENDIF
											ENDIF
										ENDIF
											
										IF bCompletedSpawn
											//Start timer
											bLocalClientCurrentlySpawningRequests[iRequestID] = TRUE
											CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === bLocalClientCurrentlySpawningRequests[",iRequestID,"] = TRUE)")
											RESET_NET_TIMER(iLocalClientSpawningTimer[iRequestID])
										ENDIF
									ELSE
										CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAINT_CLIENT_REQUEST_SPAWNING - iBestSpawnCoord = -1")
									ENDIF
									
									IF NOT bCompletedSpawn
										SET_BIT(MPGlobals.g_iVehicleSpawnRequestFailed, iRequestID)
									ENDIF
									
									iLocalExpensiveBitset = 0
								ELSE
									#IF IS_DEBUG_BUILD
									IF bAdditionalSpew
										CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAINT_CLIENT_REQUEST_SPAWNING - [RE] - CAN_REGISTER_MISSION_VEHICLES = FALSE")
									ENDIF
									#ENDIF
								ENDIF
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
							IF bAdditionalSpew
								CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAINT_CLIENT_REQUEST_SPAWNING - [RE] - Failed to load ", GET_MODEL_NAME_STRING(mnVehicleModel), " model.")
							ENDIF
							#ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						IF bAdditionalSpew
							CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAINT_CLIENT_REQUEST_SPAWNING - [RE] - Player ", iPlayer, " does not own spawn point ", playerBD[iPlayer].spawnRequest[iRequestID].iSpawnPointID)
						ENDIF
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC MAINTAIN_CLIENT_SPAWNING_TIMER()
	INT iRequestID
	REPEAT NUMBER_OF_SPAWN_REQUESTS iRequestID
		IF bLocalClientCurrentlySpawningRequests[iRequestID]
			IF HAS_NET_TIMER_EXPIRED(iLocalClientSpawningTimer[iRequestID],CLIENT_SPAWNING_TIME)
				RESET_CLIENT_SPAWN_REQUEST(iRequestID)
				bLocalClientCurrentlySpawningRequests[iRequestID] = FALSE
				CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === bLocalClientCurrentlySpawningRequests[",iRequestID,"] = FALSE)")
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Manages the local player monitoring failed spawn atempts. Will react appropriately once the number of failed attempts reaches a limit, clearing the request and sending an event that the request is no longer required
PROC MAINTAIN_CLIENT_REQUEST_FAIL()
	IF MPGlobals.g_iVehicleSpawnRequestFailed <> 0
		INT iRequestID
		INT iPlayerID = NATIVE_TO_INT(PLAYER_ID())
		REPEAT NUMBER_OF_SPAWN_REQUESTS iRequestID
			IF IS_BIT_SET(MPGlobals.g_iVehicleSpawnRequestFailed, iRequestID)
				IF IS_BIT_SET(playerBD[iPlayerID].spawnRequest[iRequestID].iBitset, REQUEST_BS_ACTIVE)
					
					IF NOT IS_BIT_SET(playerBD[iPlayerID].spawnRequest[iRequestID].iBitset, REQUEST_BS_IGNORE_SIGHT_CHECKS)
						SET_BIT(playerBD[iPlayerID].spawnRequest[iRequestID].iBitset, REQUEST_BS_IGNORE_SIGHT_CHECKS)
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_BIT(playerBD[", iPlayerID, "].spawnRequest[", iRequestID, "].iBitset, REQUEST_BS_IGNORE_SIGHT_CHECKS)")
						#ENDIF
					ENDIF					
					
					playerBD[iPlayerID].spawnRequest[iRequestID].iFailCounter++
					
					//Actually bail out of the booking if we have failed too many times
					IF playerBD[iPlayerID].spawnRequest[iRequestID].iFailCounter >= NUMBER_OF_SPAWN_ATTEMPTS
					
						IF IS_BIT_SET(playerBD[iPlayerID].spawnRequest[iRequestID].iBitset, REQUEST_BS_IS_BOOKING)
							IF IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_DELIVERY)
								SET_BIT(iLocalPhonecallBitset, PHONECALL_BS_REQUEST_DELIVERY_FAIL)
								IF GET_CONTACT_REQUEST_COST(REQUEST_PEGASUS) > 0
									GIVE_LOCAL_PLAYER_FM_CASH(GET_CONTACT_REQUEST_COST(REQUEST_PEGASUS),1,FALSE,0)
									NETWORK_REFUND_CASH_TYPE(GET_CONTACT_REQUEST_COST(REQUEST_PEGASUS), MP_REFUND_TYPE_BUY_CARDROPOFF, MP_REFUND_REASON_DELIVERY_FAIL, TRUE)
								ENDIF
							ELSE
								PRINT_TICKER(GET_TICKER_LABEL_FOR_VEHICLE_REQUEST_TYPE(bookedVehicle.vrType, FALSE))
								IF GET_PRICE_FOR_VEHICLE_REQUEST_TYPE(bookedVehicle.vrType) > 0
									NETWORK_REFUND_CASH_TYPE(GET_PRICE_FOR_VEHICLE_REQUEST_TYPE(bookedVehicle.vrType), MP_REFUND_TYPE_BUY_CARDROPOFF, MP_REFUND_REASON_DELIVERY_FAIL, TRUE)
								ENDIF
							ENDIF
						
							CLEAR_BOOKED_VEHICLE(DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
							
						ENDIF
						
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === Request ", iRequestID, " removed as it failed ")
						#ENDIF
						
						RESET_CLIENT_SPAWN_REQUEST(iRequestID)
						
					ENDIF
				ENDIF
				
				CLEAR_BIT(MPGlobals.g_iVehicleSpawnRequestFailed, iRequestID)
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

FUNC HUD_COLOURS GET_PEGASUS_BLIP_COLOUR()
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
		RETURN GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID())
	ENDIF
	RETURN HUD_COLOUR_YELLOWLIGHT
ENDFUNC

/// PURPOSE:
///    Reacts to orders for vehicles coming in from phonecalls to pegasus and lester. Sets up the vehicle requests and flags for finishing the phonecalls
PROC MAINTAIN_CLIENT_PHONECALL_REACTIONS()
	
	VEHICLE_REQUEST_TYPE thisVRT
	BOOL bSearch = FALSE
	BOOL bDelivery = FALSE
	VECTOR vPlayerPosition
	
	IF IS_BIT_SET(MPGlobals.g_iVehicleSpawnRequestBitset, VEH_SPAWN_REQUEST_BS_DELIVERY)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === IS_BIT_SET(MPGlobals.g_iVehicleSpawnRequestBitset, VEH_SPAWN_REQUEST_BS_DELIVERY) = TRUE")
		#ENDIF
		thisVRT = GET_MODEL_VEHICLE_REQUEST_TYPE(MPGlobals.g_mnDeliveryModel)
		bDelivery = TRUE
		bSearch = TRUE
	ELIF IS_BIT_SET(MPGlobals.g_iVehicleSpawnRequestBitset, VEH_SPAWN_REQUEST_BS_BOAT)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === IS_BIT_SET(MPGlobals.g_iVehicleSpawnRequestBitset, VEH_SPAWN_REQUEST_BS_BOAT) = TRUE")
		#ENDIF
		thisVRT = VRT_BOAT
		bSearch = TRUE
	ELIF IS_BIT_SET(MPGlobals.g_iVehicleSpawnRequestBitset, VEH_SPAWN_REQUEST_BS_HELI)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === IS_BIT_SET(MPGlobals.g_iVehicleSpawnRequestBitset, VEH_SPAWN_REQUEST_BS_HELI) = TRUE")
		#ENDIF
		thisVRT = VRT_HELI
		bSearch = TRUE
	ELIF IS_BIT_SET(MPGlobals.g_iVehicleSpawnRequestBitset, VEH_SPAWN_REQUEST_BS_CAR)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === IS_BIT_SET(MPGlobals.g_iVehicleSpawnRequestBitset, VEH_SPAWN_REQUEST_BS_CAR) = TRUE")
		#ENDIF
		thisVRT = VRT_CAR
		bSearch = TRUE
	ELIF IS_BIT_SET(MPGlobals.g_iVehicleSpawnRequestBitset, VEH_SPAWN_REQUEST_BS_PLANE)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === IS_BIT_SET(MPGlobals.g_iVehicleSpawnRequestBitset, VEH_SPAWN_REQUEST_BS_PLANE) = TRUE")
		#ENDIF
		thisVRT = VRT_PLANE
		bSearch = TRUE
	ELIF IS_BIT_SET(MPGlobals.g_iVehicleSpawnRequestBitset, VEH_SPAWN_REQUEST_BS_BLIMP)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === IS_BIT_SET(MPGlobals.g_iVehicleSpawnRequestBitset, VEH_SPAWN_REQUEST_BS_BLIMP) = TRUE")
		#ENDIF
		thisVRT = VRT_BLIMP
		bSearch = TRUE
	ENDIF
	
	
	IF bSearch
		INT iRequestLoop
		INT iSpawnLoop
		INT iPlayerID = NATIVE_TO_INT(PLAYER_ID())
		
		//search for spawn request we can clear and replace with this
		IF bSearch
			REPEAT NUMBER_OF_SPAWN_REQUESTS iRequestLoop
				IF bSearch
					IF NOT IS_BIT_SET(playerBD[iPlayerID].spawnRequest[iRequestLoop].iBitset, REQUEST_BS_IS_BOOKING)
						
						//use this request
						IF IS_BIT_SET(playerBD[iPlayerID].spawnRequest[iRequestLoop].iBitset, REQUEST_BS_ACTIVE)
				
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === Request ", iRequestLoop, " removed as it was needed for booking!")
							#ENDIF
							
							RESET_CLIENT_SPAWN_REQUEST(iRequestLoop)
						ENDIF
						
						IF IS_PEGASUS_VEHICLE_IN_AVENGER(PLAYER_ID())
						OR IS_PEGASUS_VEHICLE_EXIST_IN_TRUCK_CAR_MOD_SECTION(PLAYER_ID())
							CLEAR_BOOKED_VEHICLE(DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)	//clear old booking
						ELSE
							CLEAR_BOOKED_VEHICLE()
						ENDIF
						bookedVehicle.iRequestID = iRequestLoop
						
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === Spawn request found for booking ", bookedVehicle.iRequestID)
						#ENDIF
						
						bSearch = FALSE
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
		MODEL_NAMES mnBest = DUMMY_MODEL_FOR_SCRIPT
		//search for a spawn point that is suitable
		IF bookedVehicle.iRequestID <> -1
			#IF IS_DEBUG_BUILD
			IF bDelivery
				CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === Start search for spawn point... ")
			ENDIF
			#ENDIF
			
			BOOL bCanRequestVehicleAtPoint = FALSE
			
			INT iCurrentBestSpawn = -1
			//INT iCurrentBestVehicle = -1
			MODEL_NAMES mnTest  = DUMMY_MODEL_FOR_SCRIPT
			
			REPEAT NUMBER_OF_VEHICLE_SPAWN_POINTS iSpawnLoop
				 
				// We want to be able to spawn at our current position so allow requests at this point.
				IF IS_LOCAL_PLAYER_IN_FORT_ZANCUDO() OR IS_LOCAL_PLAYER_IN_LSIA()
					bCanRequestVehicleAtPoint = TRUE
				ELSE
					IF NOT IS_PLAYER_BD_AT_SPAWN_POINT(iSpawnLoop)
						bCanRequestVehicleAtPoint = TRUE
					ENDIF
				ENDIF

				IF bCanRequestVehicleAtPoint
					BOOL bVerticalTakeoff = ((MPGlobals.g_mnDeliveryModel = HYDRA) OR (MPGlobals.g_mnDeliveryModel = BLIMP3))
					IF CAN_VEHICLE_REQUEST_TYPE_SPAWN_AT_VEHICLE_SPAWN_POINT(iSpawnLoop, thisVRT)	//can spawn
					OR (bVerticalTakeOff AND CAN_VEHICLE_REQUEST_TYPE_SPAWN_AT_VEHICLE_SPAWN_POINT(iSpawnLoop, VRT_HELI)) //Try heli if vertical takeoff
						IF CAN_PLAYER_USE_VEHICLE_SPAWN_POINT(PLAYER_ID(), iSpawnLoop)				//if I have unlocked something that can spawn
						OR bDelivery																//or it's a delivery
							IF IS_SPAWN_POINT_SUITABLE_FOR_DELIVERY(iSpawnLoop)						//is fine for delivery
							OR NOT bDelivery														//or not a delivery
								mnTest = DUMMY_MODEL_FOR_SCRIPT
								IF bDelivery
									mnTest = MPGlobals.g_mnDeliveryModel
								ELSE
									mnTest = GET_VEHICLE_SPAWN_POINT_MODEL_TO_SPAWN_FOR_PLAYER(PLAYER_ID(), iSpawnLoop)
								ENDIF
								
								IF mnTest <> DUMMY_MODEL_FOR_SCRIPT
								
									IF NOT IS_BIT_SET(MPGlobals.g_iVehicleSpawnRequestBitset, VEH_SPAWN_REQUEST_BS_CAR)
									OR IS_VEHICLE_SUITABLE_FOR_LESTER_REQUEST(mnTest)
									
										IF NOT IS_VEHICLE_SUBMERSIBLE(mnTest)
										OR IS_SPAWN_POINT_SUITABLE_FOR_SUBMERSIBLE(iSpawnLoop)
									
											IF NOT DOES_VEHICLE_NEED_BIG_SPAWN_COORD(mnTest)
											OR DOES_VEHICLE_SPAWN_POINT_HAVE_BIG_SPAWN_COORD(iSpawnLoop)
										
												IF NOT DOES_VEHICLE_NEED_REALLY_BIG_SPAWN_COORD(mnTest)
												OR DOES_VEHICLE_SPAWN_POINT_HAVE_REALLY_BIG_SPAWN_COORD(iSpawnLoop)
										
													IF CHECK_SPECIAL_CASES_OF_VEHICLE_SPAWN_POINT(iSpawnLoop, mnTest)					//1641695
														IF CHECK_SPECIAL_CONDITIONS_OF_VEHICLE_SPAWN_POINT(iSpawnLoop, mnTest)					//1641695
															//IF CHECK_VECTOR_FOR_EVENT_BLOCKING(GET_VEHICLE_SPAWN_POINT_CENTRE(iSpawnLoop))//allow delivery to blocking area
																//if nearer than current best
																IF iCurrentBestSpawn = -1
																	iCurrentBestSpawn = iSpawnLoop
																	mnBest = mnTest
																	
																	#IF IS_DEBUG_BUILD
																	IF bDelivery
																		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === Success! Found at least one spawn point iSpawnLoop = ", iSpawnLoop)
																	ENDIF
																	#ENDIF
																ELSE
																	
																	vPlayerPosition = GET_PLAYER_PERCEIVED_COORDS(PLAYER_ID())
																	
																	IF VDIST2(vPlayerPosition, GET_VEHICLE_SPAWN_POINT_CENTRE(iSpawnLoop)) < VDIST2(vPlayerPosition, GET_VEHICLE_SPAWN_POINT_CENTRE(iCurrentBestSpawn))
																		iCurrentBestSpawn = iSpawnLoop
																		mnBest = mnTest
																	ENDIF
																ENDIF
															//ENDIF
														ELSE
															#IF IS_DEBUG_BUILD
															IF bDelivery
																CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === Rejecting spawn point iSpawnLoop = ", iSpawnLoop, " because CHECK_SPECIAL_CONDITIONS_OF_VEHICLE_SPAWN_POINT")
															ENDIF
															#ENDIF
														ENDIF
													ELSE
														#IF IS_DEBUG_BUILD
														IF bDelivery
															CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === Rejecting spawn point iSpawnLoop = ", iSpawnLoop, " because CHECK_SPECIAL_CASES_OF_VEHICLE_SPAWN_POINT")
														ENDIF
														#ENDIF
													ENDIF
												ELSE
													#IF IS_DEBUG_BUILD
													IF bDelivery
														IF DOES_VEHICLE_NEED_REALLY_BIG_SPAWN_COORD(mnTest)
															CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === Rejecting spawn point iSpawnLoop = ", iSpawnLoop, " because DOES_VEHICLE_NEED_REALLY_BIG_SPAWN_COORD")
														ENDIF
														
														IF NOT DOES_VEHICLE_SPAWN_POINT_HAVE_REALLY_BIG_SPAWN_COORD(iSpawnLoop)
															CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === Rejecting spawn point iSpawnLoop = ", iSpawnLoop, " because NOT DOES_VEHICLE_SPAWN_POINT_HAVE_REALLY_BIG_SPAWN_COORD")
														ENDIF
													ENDIF
													#ENDIF
												ENDIF
											ELSE
												#IF IS_DEBUG_BUILD
												IF bDelivery
													IF DOES_VEHICLE_NEED_BIG_SPAWN_COORD(mnTest)
														CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === Rejecting spawn point iSpawnLoop = ", iSpawnLoop, " because DOES_VEHICLE_NEED_BIG_SPAWN_COORD")
													ENDIF
													
													IF NOT DOES_VEHICLE_SPAWN_POINT_HAVE_BIG_SPAWN_COORD(iSpawnLoop)
														CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === Rejecting spawn point iSpawnLoop = ", iSpawnLoop, " because NOT DOES_VEHICLE_SPAWN_POINT_HAVE_BIG_SPAWN_COORD")
													ENDIF
												ENDIF
												#ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
								IF bDelivery
									CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === Rejecting spawn point iSpawnLoop = ", iSpawnLoop, " because IS_SPAWN_POINT_SUITABLE_FOR_DELIVERY")
								ENDIF
								#ENDIF
							ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						IF bDelivery
							CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === Rejecting spawn point iSpawnLoop = ", iSpawnLoop, " because CAN_VEHICLE_REQUEST_TYPE_SPAWN_AT_VEHICLE_SPAWN_POINT ", PICK_STRING(thisVRT = VRT_BOAT, "BOAT ", PICK_STRING(thisVRT = VRT_HELI, "HELI ", PICK_STRING(thisVRT = VRT_CAR, "CAR ", PICK_STRING(thisVRT = VRT_PLANE, "PLANE ", PICK_STRING(thisVRT = VRT_BLIMP, "BLIMP ", "unknown thisVRT "))))))
						ENDIF
						#ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					IF bDelivery
						CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === Rejecting spawn point iSpawnLoop = ", iSpawnLoop, " because IS_PLAYER_BD_AT_SPAWN_POINT")
					ENDIF
					#ENDIF
				ENDIF
			ENDREPEAT
			
			#IF IS_DEBUG_BUILD
			IF bDelivery
				CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === Done search for spawn point, iCurrentBestSpawn = ", iCurrentBestSpawn)
			ENDIF
			#ENDIF
			
			IF iCurrentBestSpawn <> -1	//found
				bookedVehicle.iSpawnPointID = iCurrentBestSpawn
				bookedVehicle.timeCountdown = timeNet
				SET_BIT(bookedVehicle.iBitset, BOOK_BS_BOOKED)
				bookedVehicle.vrType = thisVRT
				bookedVehicle.mnBookModel = mnBest
				
				IF bDelivery
					SET_BIT(bookedVehicle.iBitset, BOOK_BS_DELIVERY)
				ENDIF
				
				IF IS_BIT_SET(MPGlobals.g_iVehicleSpawnRequestBitset, VEH_SPAWN_REQUEST_BS_BOSS_LIMO)
					SET_BIT(bookedVehicle.iBitset, BOOK_BS_BOSS_LIMO)
					CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_BIT(bookedVehicle.iBitset, BOOK_BS_BOSS_LIMO)")
				ENDIF
				
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === bookedVehicle.iBitset, BOOK_BS_BOOKED)")
				CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === Booked with Spawn Point ", bookedVehicle.iSpawnPointID)
				#ENDIF
				
				bSearch = FALSE
			ELSE
				bSearch = TRUE	//still searching (aka fail)
			ENDIF
			
		ENDIF
		
			
		//ticker
		IF IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_DELIVERY)
			SET_BIT(MPGlobals.g_iVehicleSpawnRequestBitset, VEH_SPAWN_REQUEST_BS_SEARCHED)
			SET_BIT(MPGlobals.g_iVehicleSpawnRequestBitset, VEH_SPAWN_REQUEST_BS_SUCCESS)
		ELSE
			SET_BIT(MPGlobals.g_iVehicleSpawnRequestBitset, VEH_SPAWN_REQUEST_BS_SEARCHED)
			
			IF NOT bSearch
				IF mnBest <> DUMMY_MODEL_FOR_SCRIPT
					MPGlobals.g_mnDeliveryModel = mnBest
					SET_BIT(MPGlobals.g_iVehicleSpawnRequestBitset, VEH_SPAWN_REQUEST_BS_SUCCESS)
					//PRINT_TICKER(GET_TICKER_LABEL_FOR_VEHICLE_REQUEST_TYPE(thisVRT, TRUE))
					
					PRINT_TICKER_WITH_STRING("VS_TCK_L", GET_TICKER_LABEL_FOR_VEHICLE_REQUEST_TYPE(thisVRT, TRUE), GET_PEGASUS_BLIP_COLOUR())
					
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC



/// PURPOSE:
///    Sets the blip sprite for the booked vehicle based on the booked vehicle's model
PROC SET_BOOKED_VEHICLE_TYPE_SPRITE()
	SET_BLIP_SPRITE(bookedVehicle.blipID, GET_PEGASUS_BLIP_SPRITE_FOR_MODEL(bookedVehicle.mnBookModel))
	SET_CUSTOM_BLIP_NAME_FROM_MODEL(bookedVehicle.blipID, bookedVehicle.mnBookModel)
	SET_BLIP_COLOUR_FROM_HUD_COLOUR(bookedVehicle.blipID, GET_PEGASUS_BLIP_COLOUR())
ENDPROC


FUNC BOOL SHOULD_HIDE_PEGASUS_BLIP(VEHICLE_INDEX VehicleID)

	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty > 0
		IF NOT IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())
			IF GET_PROPERTY_SIZE_TYPE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty ) != PROP_SIZE_TYPE_LARGE_APT
				RETURN TRUE
			ELSE
				IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_A_GARAGE)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF DOES_ENTITY_EXIST(VehicleID)
	AND NOT IS_ENTITY_DEAD(VehicleID)
		VECTOR vPosition = GET_ENTITY_COORDS(VehicleID)
		IF (vPosition.z < -80.0) // we're in a displaced interior.
			IF NOT IS_PLAYER_IN_BUNKER_THEY_OWN(PLAYER_ID())
			OR IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF (VDIST2(vPosition, <<1103.0, -2991.0, -39.0>>) < 2500.0)
			RETURN TRUE
		ENDIF
	ENDIF

	IF IS_PEGASUS_VEHICLE_IN_AVENGER(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Cleans up a remotely owned pegasus vehicle
///    (25/04/2016 NB this is now sometimes used to cleanup LOCAL player Pegasus)
/// PARAMS:
///    tempVeh - vehicle to cleanup
PROC CLEANUP_REMOTE_MP_PEG_SAVED_VEHICLE(VEHICLE_INDEX &tempVeh  ,BOOL bDontDoKickOutTimer = FALSE, INT iOverriddenPlayerStagger = -1  , BOOL bForceDelete = FALSE)

	INT iTempPlayerIndex = iRemoteVehicleStagger
	
	// What's going on here:
	// This function used to be used in a stagger loop to cleanup REMOTE players vehicles
	// However, at some point during Executive development it started to be used to cleanup LOCAL
	// players Pegasus but it still used the stagger loop so some parts of it didnt work correctly.
	// In order to keep the original stagger loop functionality of this I added option to override the player
	// stagger which has to be set to local player ID when used on local player helicopters.
	IF iOverriddenPlayerStagger > -1
		iTempPlayerIndex = iOverriddenPlayerStagger
	ENDIF

	BOOL bSetAsMissionEntity
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND IS_ENTITY_A_VEHICLE(tempVeh)
		INT i
		PLAYER_INDEX tempPlayer	

		IF DOES_ENTITY_EXIST(tempVeh)
		AND IS_VEHICLE_DRIVEABLE(tempVeh)
			IF IS_VEHICLE_IN_PLAYER_GARAGE(tempVeh)
				EXIT
			ENDIF	
		ENDIF
		
		IF NOT IS_VEHICLE_EMPTY(tempVeh, TRUE, TRUE)
		
		
			#IF IS_DEBUG_BUILD
			INT iMaxSeats = (GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(tempVeh) + 1) //Add 1 for the driver
			NET_NL() NET_PRINT("CLEANUP_REMOTE_MP_PEG_SAVED_VEHICLE: iMaxSeats = ") NET_PRINT_INT(iMaxSeats) NET_NL()
			#ENDIF
			
			NET_NL() NET_PRINT("CLEANUP_REMOTE_MP_PEG_SAVED_VEHICLE: Kicking remote players out of old player vehicle so we can cleanup") NET_NL()
			REPEAT NUM_NETWORK_PLAYERS i
				tempPlayer = INT_TO_PLAYERINDEX(i)
				IF IS_NET_PLAYER_OK(tempPlayer, FALSE)
					PRINTLN("CLEANUP_REMOTE_MP_PEG_SAVED_VEHICLE: Player is ok ",GET_PLAYER_NAME(tempPlayer))
					IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(tempPlayer), tempVeh)
						PRINTLN("CLEANUP_REMOTE_MP_PEG_SAVED_VEHICLE: Player in vehicle")
						IF HAS_NET_TIMER_EXPIRED(iLocalKickOutTimer,LOCAL_KICK_OUT_TIME)
						OR bDontDoKickOutTimer
							BROADCAST_LEAVE_VEHICLE(SPECIFIC_PLAYER(tempPlayer), FALSE, 0, 0, FALSE, FALSE)
							
							IF HAS_NET_TIMER_EXPIRED(iLocalKickOutTimer,LOCAL_KICK_OUT_TIME)
							PRINTLN("CLEANUP_REMOTE_MP_PEG_SAVED_VEHICLE: HAS_NET_TIMER_EXPIRED - BROADCAST_LEAVE_VEHICLE")
							ENDIF
							
							IF bDontDoKickOutTimer
								PRINTLN("CLEANUP_REMOTE_MP_PEG_SAVED_VEHICLE: bDontDoKickOutTimer = TRUE - BROADCAST_LEAVE_VEHICLE")
							ENDIF
						ELSE
							PRINTLN("CLEANUP_REMOTE_MP_PEG_SAVED_VEHICLE: NOT HAS_NET_TIMER_EXPIRED - ",LOCAL_KICK_OUT_TIME - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(iLocalKickOutTimer))
							IF NETWORK_PLAYER_ID_TO_INT() = i
								IF NOT IS_BIT_SET(iGivenWarningBS, iTempPlayerIndex)
									STRING sReclaim = "PEG_RECLAIME"
									IF GB_IS_THIS_VEHICLE_A_GANG_BOSS_LIMO(tempVeh)
										sReclaim = "GB_RECLAIME"
									ENDIF
									PRINT_HELP(sReclaim)
									PRINTLN("CLEANUP_REMOTE_MP_PEG_SAVED_VEHICLE: Printing PEG_RECLAIME or GB_RECLAIME, stagger: ", iTempPlayerIndex)
									SET_BIT(iGivenWarningBS, iTempPlayerIndex)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
		ELIF NOT NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
			NET_NL() NET_PRINT("CLEANUP_REMOTE_MP_PEG_SAVED_VEHICLE: Requesting control of saved vehicle") NET_NL()
			NETWORK_REQUEST_CONTROL_OF_ENTITY(tempVeh)
			RESET_NET_TIMER(iLocalKickOutTimer)
			CLEAR_BIT(iGivenWarningBS,iTempPlayerIndex)
		ELIF NOT CAN_EDIT_THIS_ENTITY(tempVeh, bSetAsMissionEntity)
			NET_NL() NET_PRINT("CLEANUP_REMOTE_MP_PEG_SAVED_VEHICLE: Requesting ability to edit saved vehicle, bSetAsMissionEntity:") NET_PRINT_BOOL(bSetAsMissionEntity) NET_NL()
		ELSE
			NET_NL() NET_PRINT("CLEANUP_REMOTE_MP_PEG_SAVED_VEHICLE: locking doors and invalidating vehicle decorator") NET_NL()
			SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(tempVeh, TRUE)
			//SET_ENTITY_AS_MISSION_ENTITY(tempVeh, FALSE, TRUE)
			LOCK_DOORS_WHEN_NO_LONGER_NEEDED(tempVeh)
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(tempVeh,TRUE)
			
			// this was causing too many problems so taking out. 2835125 
//			IF IS_VEHICLE_DRIVEABLE(tempVeh)
//				NETWORK_FADE_OUT_ENTITY(tempVeh, TRUE, TRUE)
//			ENDIF
			
			// delete if in the impound yard
			//IF IS_ENTITY_IN_IMPOUND_YARD(tempVeh)
			IF SHOULD_VEHICLE_BE_DELETED_ON_CLEANUP(tempVeh)
				NET_NL() NET_PRINT("CLEANUP_REMOTE_MP_PEG_SAVED_VEHICLE: Deleting as veh is in the impound yard") NET_NL()
				DELETE_VEHICLE(tempVeh)
			ELIF IS_VEHICLE_A_PERSONAL_TRUCK(tempVeh)
							
				IF IS_VEHICLE_ATTACHED_TO_TRAILER(tempVeh)
					NET_NL() NET_PRINT("CLEANUP_REMOTE_MP_PEG_SAVED_VEHICLE: detatching personal truck trailerVeh which was attached to ")
					IF NOT IS_ENTITY_DEAD(tempVeh)
						NET_PRINT(GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(tempVeh))
					ELSE
						NET_PRINT("tempVeh")
					ENDIF
					NET_NL()
					
					DETACH_VEHICLE_FROM_TRAILER(tempVeh)
				ENDIF
				
				NET_NL() NET_PRINT("CLEANUP_REMOTE_MP_PEG_SAVED_VEHICLE: Deleting personal truck or trailer ")
				IF NOT IS_ENTITY_DEAD(tempVeh)
					NET_PRINT(GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(tempVeh))
				ELSE
					NET_PRINT("tempVeh")
				ENDIF
				NET_NL()
				DELETE_VEHICLE(tempVeh)
			ELIF IS_ENTITY_IN_AREA(tempVeh, <<1097.5347, -3016.0115, -40.7658>>, <<1109.2977, -2983.6902, -34.1882>>, FALSE, FALSE)
				DELETE_VEHICLE(tempVeh)
				NET_NL() NET_PRINT("CLEANUP_REMOTE_MP_PEG_SAVED_VEHICLE: this vehicle is inside truck interior delete !") NET_NL()
			ELIF bForceDelete	
				DELETE_VEHICLE(tempVeh)
				NET_NL() NET_PRINT("CLEANUP_REMOTE_MP_PEG_SAVED_VEHICLE: force delete this vehicle!") NET_NL()
			ELSE
				NET_NL() NET_PRINT("CLEANUP_REMOTE_MP_PEG_SAVED_VEHICLE: Setting as no longer needed") NET_NL()
				SET_VEHICLE_AS_NO_LONGER_NEEDED(tempVeh)
			ENDIF
			
			StoredPegVehicleID = NULL
		
			RESET_NET_TIMER(iLocalKickOutTimer)
			CLEAR_BIT(iGivenWarningBS,iTempPlayerIndex)
			CLEAR_BIT(iInRemoteVehBS, iTempPlayerIndex)
		
		ENDIF
	ENDIF

ENDPROC

FUNC STRING GET_PEGASUS_VEHICLE_BLIP_NAME(MODEL_NAMES ModelName, SITE_BUYABLE_VEHICLE sbv)
	
	IF (sbv != UNSET_BUYABLE_VEHICLE)
		
		SWITCH sbv
			#IF FEATURE_FIXER
			CASE BV_AGENCY_HELI
				RETURN "BV_AGENCY_HELI"
			BREAK
			#ENDIF
			CASE UNSET_BUYABLE_VEHICLE
				// do nothing but trick the compiler.
			BREAK
		ENDSWITCH

	ENDIF

	
	RETURN GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(ModelName)

ENDFUNC

/// PURPOSE:
///    Handles monitoring the local player's booked vehicles (lester and pegasus). Reacts to the vehicle being created and destroyed, flagging for phone calls, refunds, wanted levels etc. as appropriate.
PROC MAINTAIN_CLIENT_BOOKING()

	BOOL bClearBookingNow = FALSE
	
	BOOL bDoTextNow = FALSE
	
	VEHICLE_INDEX thisVehicle
	
	IF SHOULD_CLEARNUP_PEGASUS_BEFORE_SUPERMOD(PLAYER_ID())
		g_bForceDeleteBookedVehicle = TRUE
		SET_CLEARNUP_PEGASUS_BEFORE_SUPERMOD(FALSE)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAIN_CLIENT_BOOKING - g_bForceDeleteBookedVehicle TRUE")
	ENDIF
	
	// should clear helis for url:bugstar:4976709 -  VEH SPAWN CLIENT: Ambient Frogger spawned during Hooked mission
	IF GB_GET_MEGA_BUSINESS_VARIATION_PLAYER_IS_ON(PLAYER_ID()) = MBV_COLLECT_DJ_HOOKED
		IF IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_BOOKED)
		OR IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_VEHICLE_EXISTS)
			IF IS_THIS_MODEL_A_HELI(bookedVehicle.mnBookModel)
				CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAIN_CLIENT_BOOKING - MBV_COLLECT_DJ_HOOKED is active, cleanup.")								
				g_bForceDeleteBookedVehicle = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(GlobalServerBD.iPegasusCleanupBS, NATIVE_TO_INT(PLAYER_ID()))
	AND NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPegasusBS, PEGASUS_BS_VEHICLE_IS_BEING_CLEANED_UP)
		PRINTLN("[FORCE_PEGASUS] Server is forcing me to clean up pegasus")
		
		CLEAR_BOOKED_VEHICLE(TRUE, DEFAULT, FALSE)
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV = NULL
		
		IF DOES_ENTITY_EXIST(MPGlobals.RemotePegV[NATIVE_TO_INT(PLAYER_ID())])
			MPGlobals.OldRemotePegV[NATIVE_TO_INT(PLAYER_ID())] = MPGlobals.RemotePegV[NATIVE_TO_INT(PLAYER_ID())]
			CLEANUP_REMOTE_MP_PEG_SAVED_VEHICLE(MPGlobals.RemotePegV[NATIVE_TO_INT(PLAYER_ID())], FALSE, NATIVE_TO_INT(PLAYER_ID()))
		ENDIF
		
		SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPegasusBS, PEGASUS_BS_VEHICLE_IS_BEING_CLEANED_UP)
	ENDIF
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])
	AND NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
		// skip
	ELSE
		IF IS_SAVED_TRUCK_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
			CLEAR_SAVED_TRUCK_FLAG(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
			
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAIN_CLIENT_BOOKING - MP_SAVED_VEH_FLAG_NEAR_PLAYER CLEARED")
		ENDIF
		
		IF IS_SAVED_TRUCK_FLAG_SET(MP_SAVED_VEH_FLAG_NOT_EMPTY)
			CLEAR_SAVED_TRUCK_FLAG(MP_SAVED_VEH_FLAG_NOT_EMPTY)
			
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAIN_CLIENT_BOOKING - MP_SAVED_VEH_FLAG_NOT_EMPTY CLEARED")
		ENDIF
	ENDIF
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV)
		// skip
	ELSE
		IF IS_SAVED_AVENGER_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
			CLEAR_SAVED_AVENGER_FLAG(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
			
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAIN_CLIENT_BOOKING - MP_SAVED_VEH_FLAG_NEAR_PLAYER CLEARED")
		ENDIF
		
		IF IS_SAVED_AVENGER_FLAG_SET(MP_SAVED_VEH_FLAG_NOT_EMPTY)
			CLEAR_SAVED_AVENGER_FLAG(MP_SAVED_VEH_FLAG_NOT_EMPTY)
			
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAIN_CLIENT_BOOKING - MP_SAVED_VEH_FLAG_NOT_EMPTY CLEARED")
		ENDIF
	ENDIF
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV)
		// skip
	ELSE
		IF IS_SAVED_HACKER_TRUCK_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
			CLEAR_SAVED_HACKER_TRUCK_FLAG(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
			
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAIN_CLIENT_BOOKING - MP_SAVED_VEH_FLAG_NEAR_PLAYER CLEARED")
		ENDIF
		
		IF IS_SAVED_HACKER_TRUCK_FLAG_SET(MP_SAVED_VEH_FLAG_NOT_EMPTY)
			CLEAR_SAVED_HACKER_TRUCK_FLAG(MP_SAVED_VEH_FLAG_NOT_EMPTY)
			
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAIN_CLIENT_BOOKING - MP_SAVED_VEH_FLAG_NOT_EMPTY CLEARED")
		ENDIF
	ENDIF
	
	#IF FEATURE_DLC_2_2022
	IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV)
		// skip
	ELSE
		IF IS_SAVED_ACID_LAB_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
			CLEAR_SAVED_ACID_LAB_FLAG(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
			
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAIN_CLIENT_BOOKING - MP_SAVED_VEH_FLAG_NEAR_PLAYER CLEARED")
		ENDIF
		
		IF IS_SAVED_ACID_LAB_FLAG_SET(MP_SAVED_VEH_FLAG_NOT_EMPTY)
			CLEAR_SAVED_ACID_LAB_FLAG(MP_SAVED_VEH_FLAG_NOT_EMPTY)
			
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAIN_CLIENT_BOOKING - MP_SAVED_VEH_FLAG_NOT_EMPTY CLEARED")
		ENDIF
	ENDIF
	#ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineV)
		// skip
	ELSE
		IF IS_SAVED_SUBMARINE_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
			CLEAR_SAVED_SUBMARINE_FLAG(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
			
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAIN_CLIENT_BOOKING - MP_SAVED_VEH_FLAG_NEAR_PLAYER CLEARED")
		ENDIF
		
		IF IS_SAVED_SUBMARINE_FLAG_SET(MP_SAVED_VEH_FLAG_NOT_EMPTY)
			CLEAR_SAVED_SUBMARINE_FLAG(MP_SAVED_VEH_FLAG_NOT_EMPTY)
			
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAIN_CLIENT_BOOKING - MP_SAVED_VEH_FLAG_NOT_EMPTY CLEARED")
		ENDIF
	ENDIF
	IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineDinghyV)
		// skip
	ELSE
		IF IS_SAVED_SUBMARINE_DINGHY_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
			CLEAR_SAVED_SUBMARINE_DINGHY_FLAG(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
			
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAIN_CLIENT_BOOKING - MP_SAVED_VEH_FLAG_NEAR_PLAYER CLEARED")
		ENDIF
		
		IF IS_SAVED_SUBMARINE_DINGHY_FLAG_SET(MP_SAVED_VEH_FLAG_NOT_EMPTY)
			CLEAR_SAVED_SUBMARINE_DINGHY_FLAG(MP_SAVED_VEH_FLAG_NOT_EMPTY)
			
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAIN_CLIENT_BOOKING - MP_SAVED_VEH_FLAG_NOT_EMPTY CLEARED")
		ENDIF
	ENDIF
	#ENDIF
	
	#IF FEATURE_DLC_2_2022
	IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SupportBikeV)
		// skip
	ELSE
		IF IS_SAVED_SUPPORT_BIKE_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
			CLEAR_SAVED_SUPPORT_BIKE_FLAG(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
			
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAIN_CLIENT_BOOKING - MP_SAVED_VEH_FLAG_NEAR_PLAYER CLEARED")
		ENDIF
		
		IF IS_SAVED_SUPPORT_BIKE_FLAG_SET(MP_SAVED_VEH_FLAG_NOT_EMPTY)
			CLEAR_SAVED_SUPPORT_BIKE_FLAG(MP_SAVED_VEH_FLAG_NOT_EMPTY)
			
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAIN_CLIENT_BOOKING - MP_SAVED_VEH_FLAG_NOT_EMPTY CLEARED")
		ENDIF
	ENDIF
	#ENDIF
	
	IF IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_BOOKED)
	OR IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_VEHICLE_EXISTS)
		IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPegasusBS, PEGASUS_BS_I_HAVE_ACTIVE_VEH)
			PRINTLN("[FORCE_PEGASUS] SET_BIT PEGASUS_BS_I_HAVE_ACTIVE_VEH")
			SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPegasusBS, PEGASUS_BS_I_HAVE_ACTIVE_VEH)
		ENDIF
		
		IF NOT IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_VEHICLE_EXISTS)
			IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPegasusBS, PEGASUS_BS_VEHICLE_IS_BEING_DELIVERED)
				PRINTLN("[FORCE_PEGASUS] SET_BIT PEGASUS_BS_VEHICLE_IS_BEING_DELIVERED")
				SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPegasusBS, PEGASUS_BS_VEHICLE_IS_BEING_DELIVERED)
			ENDIF
		ELSE
			IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPegasusBS, PEGASUS_BS_VEHICLE_IS_BEING_DELIVERED)
				PRINTLN("[FORCE_PEGASUS] CLEAR_BIT PEGASUS_BS_VEHICLE_IS_BEING_DELIVERED")
				CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPegasusBS, PEGASUS_BS_VEHICLE_IS_BEING_DELIVERED)
			ENDIF
		ENDIF
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV)
		AND IS_VEHICLE_EMPTY(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV))
			IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPegasusBS, PEGASUS_BS_VEHICLE_IS_EMPTY)
				PRINTLN("[FORCE_PEGASUS] SET_BIT PEGASUS_BS_VEHICLE_IS_EMPTY")
				SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPegasusBS, PEGASUS_BS_VEHICLE_IS_EMPTY)
			ENDIF
		ELSE
			IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPegasusBS, PEGASUS_BS_VEHICLE_IS_EMPTY)
				PRINTLN("[FORCE_PEGASUS] CLEAR_BIT PEGASUS_BS_VEHICLE_IS_EMPTY")
				CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPegasusBS, PEGASUS_BS_VEHICLE_IS_EMPTY)
			ENDIF
		ENDIF
	ELSE
		IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPegasusBS != 0
			PRINTLN("[FORCE_PEGASUS] Resetting Pegasus Bitset - 1")
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPegasusBS = 0
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_BOOKED)
		IF NOT IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_VEHICLE_EXISTS)
			IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV)
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_BIT(bookedVehicle.iBitset, BOOK_BS_VEHICLE_EXISTS)")
				#ENDIF
				bookedVehicle.timeCountdown = timeNet
				SET_BIT(bookedVehicle.iBitset, BOOK_BS_VEHICLE_EXISTS)
				CLEAR_BIT(bookedVehicle.iBitset, BOOK_BS_DELIVERY_COUNTDOWN_SET)
			ELSE
			
				IF NOT IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_BLIP_CREATED_FOR_VEHICLE)
					IF NOT IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_BLIP_CREATED_FOR_SPAWN_POINT)
					AND NOT IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_NOT_SPAWNED_BY_SCRIPT)
						
						IF DOES_BLIP_EXIST(bookedVehicle.blipID)
							REMOVE_BLIP(bookedVehicle.blipID)
						ENDIF
						
						IF NOT DOES_BLIP_EXIST(bookedVehicle.blipID)	//failsafe, stick the blip in the centre of the point
							bookedVehicle.blipID = CREATE_BLIP_FOR_COORD(GET_VEHICLE_SPAWN_POINT_CENTRE(bookedVehicle.iSpawnPointID))
						ENDIF
						
						// set the correct blip sprite (it also sets colour)
						SET_BOOKED_VEHICLE_TYPE_SPRITE()
						
						IF IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_DELIVERY)			
							SET_BLIP_NAME_FROM_TEXT_FILE(bookedVehicle.blipID, GET_PEGASUS_VEHICLE_BLIP_NAME(bookedVehicle.mnBookModel, bookedVehicle.ePegasusBuyableVehicle))
						ELSE												
							IF IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_REQUEST_MADE)
							AND bookedVehicle.iRequestID <> -1
								SET_BLIP_NAME_FROM_TEXT_FILE(bookedVehicle.blipID, GET_PEGASUS_VEHICLE_BLIP_NAME(bookedVehicle.mnBookModel, bookedVehicle.ePegasusBuyableVehicle))
							ELSE
								SET_BLIP_NAME_FROM_TEXT_FILE(bookedVehicle.blipID, "VED_BLIPN")
							ENDIF
						ENDIF
						
						IF NOT IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_HAS_BEEN_BLIPPED)
							SET_BLIP_FLASHES(bookedVehicle.blipID, TRUE)
							SET_BLIP_FLASH_TIMER(bookedVehicle.blipID, TIME_FOR_BLIP_BLINK)
							SET_BIT(bookedVehicle.iBitset, BOOK_BS_HAS_BEEN_BLIPPED)
						ENDIF					
						
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_BIT(bookedVehicle.iBitset, BOOK_BS_BLIP_CREATED_FOR_SPAWN_POINT)")
						#ENDIF
						
						//set blip as fake vehicle
						SET_BIT(bookedVehicle.iBitset, BOOK_BS_BLIP_CREATED_FOR_SPAWN_POINT)
					ENDIF
				ENDIF
				
				IF NOT IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_DELIVERY_COUNTDOWN_SET)
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_BIT(bookedVehicle.iBitset, BOOK_BS_DELIVERY_COUNTDOWN_SET)")
					#ENDIF
					SET_BIT(bookedVehicle.iBitset, BOOK_BS_DELIVERY_COUNTDOWN_SET)
					bookedVehicle.timeCountdown = timeNet
				ELSE
					//Fail if delivery goes unused
					IF IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_DELIVERY)
						IF GET_TIME_DIFFERENCE(timeNet, bookedVehicle.timeCountdown) > TIME_VEHICLE_DELIVERY_USED						
							SET_BIT(iLocalTextMessageBitset, TEXTMESSAGE_BS_REQUEST_DELIVERY_UNUSED)
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_BIT(iLocalTextMessageBitset, TEXTMESSAGE_BS_REQUEST_DELIVERY_UNUSED)")
							#ENDIF
							bClearBookingNow = TRUE
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
		ELSE
			IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV)
				thisVehicle = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV)
				
				IF NOT IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_BLIP_CREATED_FOR_VEHICLE)
					
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), thisVehicle, TRUE)
					AND NOT IS_VEHICLE_BEING_USED_FOR_TIME_TRIAL(thisVehicle)
					AND NOT IS_SHIPMENT_PLAYER_IN_VEHICLE(thisVehicle)
					AND NOT SHOULD_HIDE_PEGASUS_BLIP(thisVehicle)
					
						IF DOES_BLIP_EXIST(bookedVehicle.blipID)
							REMOVE_BLIP(bookedVehicle.blipID)
						ENDIF
						
						IF IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_BLIP_CREATED_FOR_SPAWN_POINT)
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === CLEAR_BIT(bookedVehicle.iBitset, BOOK_BS_BLIP_CREATED_FOR_SPAWN_POINT)")
							#ENDIF
							CLEAR_BIT(bookedVehicle.iBitset, BOOK_BS_BLIP_CREATED_FOR_SPAWN_POINT)
						ENDIF
						
						IF IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_DELIVERY)
							bookedVehicle.blipID = ADD_BLIP_FOR_ENTITY(thisVehicle)
							
							// set the correct sprite
							SET_BOOKED_VEHICLE_TYPE_SPRITE()
							
							SET_BLIP_NAME_FROM_TEXT_FILE(bookedVehicle.blipID, GET_PEGASUS_VEHICLE_BLIP_NAME(bookedVehicle.mnBookModel, bookedVehicle.ePegasusBuyableVehicle))
						ELSE
							bookedVehicle.blipID = CREATE_BLIP_FOR_VEHICLE(thisVehicle, FALSE, FALSE)
							
							SET_BLIP_NAME_FROM_TEXT_FILE(bookedVehicle.blipID, GET_PEGASUS_VEHICLE_BLIP_NAME(bookedVehicle.mnBookModel, bookedVehicle.ePegasusBuyableVehicle))
						ENDIF
						
						IF SHOULD_BLIP_BE_MANUALLY_ROTATED(bookedVehicle.blipID)
							SET_BLIP_ROTATION(bookedVehicle.blipID, ROUND(GET_ENTITY_HEADING_FROM_EULERS(thisVehicle)))							
						ENDIF						
						
						IF NOT IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_HAS_BEEN_BLIPPED)
							SET_BLIP_FLASHES(bookedVehicle.blipID, TRUE)
							SET_BLIP_FLASH_TIMER(bookedVehicle.blipID, TIME_FOR_BLIP_BLINK)
							SET_BIT(bookedVehicle.iBitset, BOOK_BS_HAS_BEEN_BLIPPED)
						ENDIF											
						
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_BIT(bookedVehicle.iBitset, BOOK_BS_BLIP_CREATED_FOR_VEHICLE)")
						#ENDIF
						SET_BIT(bookedVehicle.iBitset, BOOK_BS_BLIP_CREATED_FOR_VEHICLE)
					ENDIF
					
				ELSE
				
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), thisVehicle, TRUE)
					OR IS_SHIPMENT_PLAYER_IN_VEHICLE(thisVehicle)
					OR SHOULD_HIDE_PEGASUS_BLIP(thisVehicle)
						IF DOES_BLIP_EXIST(bookedVehicle.blipID)
							REMOVE_BLIP(bookedVehicle.blipID)
							PRINTLN("MAINTAIN_CLIENT_BOOKING() removing bookedvehicle blip")
						ENDIF
						
						CLEAR_BIT(bookedVehicle.iBitset, BOOK_BS_BLIP_CREATED_FOR_VEHICLE)
					ENDIF
					
				ENDIF
				
				IF bookedVehicle.mnBookModel = LIMO2
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(thisVehicle, FALSE)) < 6
						GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_ENTER_TURRET)
					ENDIF
				ENDIF
				
				//Cleanup distance checks
				IF NOT IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_NEAR_TO_VEHICLE)
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(thisVehicle, FALSE)) < APPROACH_DISTANCE_FOR_NEAR_VEHICLE_SQR
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_BIT(bookedVehicle.iBitset, BOOK_BS_NEAR_TO_VEHICLE)")
						#ENDIF
						SET_BIT(bookedVehicle.iBitset, BOOK_BS_NEAR_TO_VEHICLE)
					ENDIF
				ELSE
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(thisVehicle, FALSE)) > LEAVE_DISTANCE_FOR_NEAR_VEHICLE_SQR
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === CLEAR_BIT(bookedVehicle.iBitset, BOOK_BS_NEAR_TO_VEHICLE)")
						#ENDIF
						CLEAR_BIT(bookedVehicle.iBitset, BOOK_BS_NEAR_TO_VEHICLE)
						IF NOT IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_DELIVERY)
						//fail if go near vehicle, then not near vehicle
						
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === bClearBookingNow = TRUE, been near, then far from vehicle")
							#ENDIF
						
							bClearBookingNow = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				//Deal with wanted levels
				IF NOT IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_DELIVERY)
					IF NOT IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_WARNED_TO_RETURN)
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), thisVehicle, TRUE)
				
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_BIT(iLocalTextMessageBitset, TEXTMESSAGE_BS_REQUEST_BOOKING_RETURN_WARNING)")
							#ENDIF
							SET_BIT(iLocalTextMessageBitset, TEXTMESSAGE_BS_REQUEST_BOOKING_RETURN_WARNING)
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_BIT(bookedVehicle.iBitset, BOOK_BS_WARNED_TO_RETURN)")
							#ENDIF
							bookedVehicle.timeWanted = timeNet
							SET_BIT(bookedVehicle.iBitset, BOOK_BS_WARNED_TO_RETURN)
						ENDIF
					ELSE
						IF VDIST2(GET_ENTITY_COORDS(thisVehicle, FALSE), bookedVehicle.vCreatedCoords) > DISTANCE_FOR_AT_CREATED_LOCATION_SQR
							IF NOT IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_EXPECTING_WANTED_LEVEL)
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_BIT(bookedVehicle.iBitset, BOOK_BS_EXPECTING_WANTED_LEVEL)")
								#ENDIF
								SET_BIT(bookedVehicle.iBitset, BOOK_BS_EXPECTING_WANTED_LEVEL)
							ENDIF
							
							IF GET_TIME_DIFFERENCE(timeNet, bookedVehicle.timeWanted) > TIME_WANTED_LEVEL
								DO_VEHICLE_SPAWN_WANTED_LEVEL()
							ENDIF
							
						ELSE
							IF IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_EXPECTING_WANTED_LEVEL)
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === CLEAR_BIT(bookedVehicle.iBitset, BOOK_BS_EXPECTING_WANTED_LEVEL)")
								#ENDIF
								CLEAR_BIT(bookedVehicle.iBitset, BOOK_BS_EXPECTING_WANTED_LEVEL)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_BIT_SET(iLocalBitset, LOCAL_BS_LESTER_VEHICLE_DEAD)
					MAINTAIN_IS_LESTER_VEHICLE_DEAD()
				ELSE
					CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === - bClearBookingNow = TRUE")
					bClearBookingNow = TRUE
				ENDIF
				
				//fail if someone else is in vehicle
				IF NOT IS_ENTITY_DEAD(thisVehicle)
					IF NOT IS_VEHICLE_SEAT_FREE(thisVehicle)
						PED_INDEX pedID = GET_PED_IN_VEHICLE_SEAT(thisVehicle)
						IF (pedID <> PLAYER_PED_ID())
						AND (GET_ENTITY_MODEL(pedID) <> MP_F_HeliStaff_01) // if it is being delivered by PA
							
							bDoTextNow = FALSE
							
							IF IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_DELIVERY)
								IF VDIST2(GET_ENTITY_COORDS(thisVehicle, FALSE), GET_ENTITY_COORDS(PLAYER_PED_ID())) > DISTANCE_FOR_VEHICLE_TO_BE_STOLEN_SQR
								AND NOT GB_IS_PED_MEMBER_OF_THIS_GANG(pedID,GB_GET_LOCAL_PLAYER_GANG_BOSS())
									bDoTextNow = TRUE
								ENDIF
							ELSE
								bDoTextNow = TRUE
							ENDIF
								
							IF bDoTextNow
								IF IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_DELIVERY)
									IF NOT IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_THEFT_REPORTED)
										SET_BIT(iLocalTextMessageBitset, TEXTMESSAGE_BS_REQUEST_DELIVERY_STOLEN)
										#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_BIT(iLocalTextMessageBitset, TEXTMESSAGE_BS_REQUEST_DELIVERY_STOLEN)")
										#ENDIF
										SET_BIT(bookedVehicle.iBitset, BOOK_BS_THEFT_REPORTED)
									ENDIF
								ELSE
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === bClearBookingNow = TRUE, vehicle has been stolen")
									#ENDIF
									bClearBookingNow = TRUE
								ENDIF
							ENDIF
							
						ENDIF
					ENDIF
				ENDIF
				
				//fail if time up (and far from the booking)
				IF NOT IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_DELIVERY)
					IF NOT IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_NEAR_TO_VEHICLE)
						IF IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_NEED_TO_RESET_TIMER)
							bookedVehicle.timeCountdown = timeNet
							CLEAR_BIT(bookedVehicle.iBitset, BOOK_BS_NEED_TO_RESET_TIMER)
						ENDIF
						IF GET_TIME_DIFFERENCE(timeNet, bookedVehicle.timeCountdown) > TIME_TO_COLLECT_BOOKING	//time up to collect
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === bClearBookingNow = TRUE, time up")
							#ENDIF
							bClearBookingNow = TRUE
						ENDIF
					ELSE
						IF NOT IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_NEED_TO_RESET_TIMER)
							SET_BIT(bookedVehicle.iBitset, BOOK_BS_NEED_TO_RESET_TIMER)
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_VEHICLE_DRIVEABLE(thisVehicle)
					IF IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_DELIVERY)
						// 2445084: Start the cooldown when the vehicle is destroyed to prevent players ordering it before the event starts to get a maximum of 2
						REINIT_NET_TIMER(g_TransitionSessionNonResetVars.contactRequests.stCDTimer[REQUEST_PEGASUS],TRUE)
						CONTACT_REQUEST_SET_CD_TIMER(REQUEST_PEGASUS)
						
						IF g_OfficeHeliDockData.bRecreatePegasusAfterLeavingOffice
							PRINTLN("[HELI_DOCK] bRecreatePegasusAfterLeavingOffice = FALSE pegasus veh IS_VEHICLE_DRIVEABLE = FALSE")
							g_OfficeHeliDockData.bRecreatePegasusAfterLeavingOffice = FALSE
						ENDIF
						
						SET_BIT(iLocalTextMessageBitset, TEXTMESSAGE_BS_REQUEST_DELIVERY_DESTROYED)
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_BIT(iLocalTextMessageBitset, TEXTMESSAGE_BS_REQUEST_DELIVERY_DESTROYED)")
						#ENDIF
					ENDIF
					bClearBookingNow = TRUE
				ENDIF
				
			ELSE	//fail if vehicle has been seen existing, but no longer does
			
				IF MPGlobalsAmbience.bDestroyedOwnPegasus
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === clearning booking as used heli dock office landing with pegasus vehicle")
					#ENDIF
					CLEAR_BIT(bookedVehicle.iBitset, BOOK_BS_DELIVERY)
					MPGlobalsAmbience.bDestroyedOwnPegasus = FALSE
				ENDIF
			
				IF IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_DELIVERY)
					CLEAR_BIT(bookedVehicle.iBitset, BOOK_BS_VEHICLE_EXISTS)
					CLEAR_BIT(bookedVehicle.iBitset, BOOK_BS_BLIP_CREATED_FOR_VEHICLE)
					CLEAR_BIT(bookedVehicle.iBitset, BOOK_BS_BLIP_CREATED_FOR_SPAWN_POINT)
					CLEAR_BIT(bookedVehicle.iBitset, BOOK_BS_NEAR_TO_VEHICLE)
					CLEAR_BIT(bookedVehicle.iBitset, BOOK_BS_REQUEST_MADE)
					
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === Delivery reset")
					#ENDIF
					
				ELSE
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === bClearBookingNow = TRUE, no longer exists")
					#ENDIF
					bClearBookingNow = TRUE
					
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === Booking no longer exists")
					#ENDIF
					//fail ticker
					//PRINT_TICKER(GET_TICKER_LABEL_FOR_VEHICLE_REQUEST_TYPE(bookedVehicle.vrType, FALSE))
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT g_bHasPegasusVehicleInAirport
			IF IS_VEHICLE_SPAWN_AREA_IN_AIRPORT(bookedVehicle.iSpawnPointID)
				g_bHasPegasusVehicleInAirport = TRUE
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_DELIVERY)
			IF NOT IS_BIT_SET(MPGlobals.g_iVehicleSpawnRequestBitset, VEH_SPAWN_REQUEST_BS_HAS_PEGASUS_VEHICLE)
				SET_BIT(MPGlobals.g_iVehicleSpawnRequestBitset, VEH_SPAWN_REQUEST_BS_HAS_PEGASUS_VEHICLE)
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_BIT(MPGlobals.g_iVehicleSpawnRequestBitset, VEH_SPAWN_REQUEST_BS_HAS_PEGASUS_VEHICLE)")
				#ENDIF
			ENDIF
		ENDIF
		IF NOT IS_BIT_SET(MPGlobals.g_iVehicleSpawnRequestBitset, VEH_SPAWN_REQUEST_BS_HAS_BOOKED_VEHICLE)
			SET_BIT(MPGlobals.g_iVehicleSpawnRequestBitset, VEH_SPAWN_REQUEST_BS_HAS_BOOKED_VEHICLE)
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_BIT(MPGlobals.g_iVehicleSpawnRequestBitset, VEH_SPAWN_REQUEST_BS_HAS_BOOKED_VEHICLE)")
			#ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(MPGlobals.g_iVehicleSpawnRequestBitset, VEH_SPAWN_REQUEST_BS_HAS_PEGASUS_VEHICLE)
			CLEAR_BIT(MPGlobals.g_iVehicleSpawnRequestBitset, VEH_SPAWN_REQUEST_BS_HAS_PEGASUS_VEHICLE)
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === CLEAR_BIT(MPGlobals.g_iVehicleSpawnRequestBitset, VEH_SPAWN_REQUEST_BS_HAS_PEGASUS_VEHICLE)")
			#ENDIF
		ENDIF
		IF IS_BIT_SET(MPGlobals.g_iVehicleSpawnRequestBitset, VEH_SPAWN_REQUEST_BS_HAS_BOOKED_VEHICLE)
			CLEAR_BIT(MPGlobals.g_iVehicleSpawnRequestBitset, VEH_SPAWN_REQUEST_BS_HAS_BOOKED_VEHICLE)
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === CLEAR_BIT(MPGlobals.g_iVehicleSpawnRequestBitset, VEH_SPAWN_REQUEST_BS_HAS_BOOKED_VEHICLE)")
			#ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(MPGlobals.g_iVehicleSpawnRequestBitset, VEH_SPAWN_REQUEST_BS_HAS_PEGASUS_VEHICLE)
	AND IS_BIT_SET(MPGlobals.g_iVehicleSpawnRequestBitset, VEH_SPAWN_REQUEST_BS_SET_QUICK_GPS)
		IF DOES_BLIP_EXIST(bookedVehicle.blipID)
			VECTOR vBlipCoords = GET_BLIP_COORDS(bookedVehicle.blipID)
			IF NOT IS_BIT_SET(iLocalBitset, LOCAL_BS_QUICK_GPS_ON)
				SET_NEW_WAYPOINT(vBlipCoords.x, vBlipCoords.y)
				vLastQuickGPS = vBlipCoords
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_BIT(iLocalBitset, LOCAL_BS_QUICK_GPS_ON) x=", vBlipCoords.x, " y=", vBlipCoords.y)
				#ENDIF
				SET_BIT(iLocalBitset, LOCAL_BS_QUICK_GPS_ON)
			ELSE
				CONST_INT iDistanceMoved 20
				IF vLastQuickGPS.x < vBlipCoords.x - iDistanceMoved 
				OR vLastQuickGPS.x > vBlipCoords.x + iDistanceMoved
				OR vLastQuickGPS.y < vBlipCoords.y - iDistanceMoved 
				OR vLastQuickGPS.y > vBlipCoords.y + iDistanceMoved
					SET_NEW_WAYPOINT(vBlipCoords.x, vBlipCoords.y)
					vLastQuickGPS = vBlipCoords
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === UPDATE GPS x=", vBlipCoords.x, " y=", vBlipCoords.y)
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBitset, LOCAL_BS_QUICK_GPS_ON)
			vLastQuickGPS = VECTOR_ZERO
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === CLEAR_BIT(iLocalBitset, LOCAL_BS_QUICK_GPS_ON)")
			#ENDIF
			CLEAR_BIT(iLocalBitset, LOCAL_BS_QUICK_GPS_ON)
		ENDIF
	ENDIF
	
	IF g_bForceDeleteBookedVehicle
		IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV)
				IF IS_VEHICLE_EMPTY(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV), TRUE, TRUE)
					CLEAR_BOOKED_VEHICLE(DEFAULT,DEFAULT,TRUE, DEFAULT, TRUE, DEFAULT, DEFAULT, TRUE)
					g_bForceDeleteBookedVehicle = FALSE
					SET_PEGASUS_VEHICLE_EXIST_IN_TRUCK_CAR_MOD_SECTION(FALSE)
					SET_BIT(iLocalTextMessageBitset, TEXTMESSAGE_BS_CLEANUP_MESSAGE_BYPASSED)
					
					CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_BIT(iLocalTextMessageBitset, TEXTMESSAGE_BS_CLEANUP_MESSAGE_BYPASSED)")
				ELSE
				
					INT i
					PLAYER_INDEX tempPlayer
					REPEAT NUM_NETWORK_PLAYERS i
						tempPlayer = INT_TO_PLAYERINDEX(i)
						IF IS_NET_PLAYER_OK(tempPlayer, FALSE)
							IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(tempPlayer), NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV))
								IF HAS_NET_TIMER_EXPIRED(iLocalKickOutTimer,LOCAL_KICK_OUT_TIME)
									BROADCAST_LEAVE_VEHICLE(SPECIFIC_PLAYER(tempPlayer), FALSE, 0, 0, FALSE, FALSE)
								ELSE
									CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === NOT HAS_NET_TIMER_EXPIRED - ",LOCAL_KICK_OUT_TIME - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(iLocalKickOutTimer))
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
					
				ENDIF
			ELSE
				NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV)
			ENDIF
		ELSE
			CLEAR_BOOKED_VEHICLE(DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
			g_bForceDeleteBookedVehicle = FALSE
			SET_PEGASUS_VEHICLE_EXIST_IN_TRUCK_CAR_MOD_SECTION(FALSE)
			SET_PEGASUS_VEHICLE_IN_AVENGER(FALSE)
			SET_BIT(iLocalTextMessageBitset, TEXTMESSAGE_BS_CLEANUP_MESSAGE_BYPASSED)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_BIT(iLocalTextMessageBitset, TEXTMESSAGE_BS_CLEANUP_MESSAGE_BYPASSED)")
		ENDIF
	ELSE
	
		IF bClearBookingNow
			CLEAR_BOOKED_VEHICLE()
		ENDIF
	ENDIF
	
	// update blip so that the heli is spinning when not landed.
	IF DOES_BLIP_EXIST(bookedVehicle.blipID)

		// Fix for 2904852 - Blip for Pegasus land vehicle is white as opposed to yellow.		
		IF NOT (GET_BLIP_HUD_COLOUR(bookedVehicle.blipID) = GET_PEGASUS_BLIP_COLOUR())
			SET_BLIP_COLOUR_FROM_HUD_COLOUR(bookedVehicle.blipID, GET_PEGASUS_BLIP_COLOUR())
			SET_BLIP_NAME_FROM_TEXT_FILE(bookedVehicle.blipID, GET_PEGASUS_VEHICLE_BLIP_NAME(bookedVehicle.mnBookModel, bookedVehicle.ePegasusBuyableVehicle))
		ENDIF
		
//			IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
//				// Check that the players vehicle blip colour is the same as the players gang colour.
//				IF GET_BLIP_HUD_COLOUR(bookedVehicle.blipID) != GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID())
//					// Set the players vehicle HUD colour to match that of their gang.
//					SET_BLIP_COLOUR_FROM_HUD_COLOUR(bookedVehicle.blipID, GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID()))
//				ENDIF
//			ELSE
//				SET_BLIP_COLOUR_FROM_HUD_COLOUR(bookedVehicle.blipID, GET_PEGASUS_BLIP_COLOUR())
//			ENDIF
	
		IF DOES_ENTITY_EXIST(thisVehicle)
			// has sprite changed?
			IF UPDATE_VEHILCE_BLIP_BASED_ON_OCCUPANCY(thisVehicle, bookedVehicle.blipID, TRUE)
				SET_BLIP_NAME_FROM_TEXT_FILE(bookedVehicle.blipID, GET_PEGASUS_VEHICLE_BLIP_NAME(bookedVehicle.mnBookModel, bookedVehicle.ePegasusBuyableVehicle))	
				SET_BLIP_COLOUR_FROM_HUD_COLOUR(bookedVehicle.blipID, GET_PEGASUS_BLIP_COLOUR())
				
				IF SHOULD_BLIP_BE_MANUALLY_ROTATED(bookedVehicle.blipID)
					SET_BLIP_ROTATION(bookedVehicle.blipID, ROUND(GET_ENTITY_HEADING_FROM_EULERS(thisVehicle)))							
				ENDIF				
			ENDIF
			
			// if player is inside office and the heli is on the roof hide it
			HIDE_VEHICLE_BLIP_ON_CERTAIN_ROOFS(thisVehicle, bookedVehicle.blipID)
			
			
		ENDIF	
	ENDIF	
	
ENDPROC


/// PURPOSE:
///    Returns a text label for the currently flagged phonecall, based on what vehicle it is for and if the local player is male or female
/// RETURNS:
///    text label for phonecall as a STRING
FUNC STRING GET_CURRENT_PHONE_CALL_LABEL()
	
	IF IS_BIT_SET(iLocalPhonecallBitset, PHONECALL_BS_REQUEST_DELIVERY_FAIL)
		IF IS_BIT_SET(bookedVehicle.iBitset,BOOK_BS_BOSS_LIMO)
			RETURN "BACAL_UNABLE"
		ENDIF
		
		IF IS_PLAYER_FEMALE()
			RETURN "MPCT_19" //Our most sincere apologies, madam, we have been unable to move your vehicle to a suitable location.
		ELSE
			RETURN "MPCT_20" //Our most sincere apologies, sir, we have been unable to move your vehicle to a suitable location.
		ENDIF

	ELIF IS_BIT_SET(iLocalPhonecallBitset, PHONECALL_BS_REQUEST_DELIVERY_SUCCESS)
	
		IF IS_BIT_SET(bookedVehicle.iBitset,BOOK_BS_BOSS_LIMO)
			RETURN "BACAL_ACCEPT"
		ENDIF
	
		MODEL_NAMES thisModel = bookedVehicle.mnBookModel
		IF thisModel <> DUMMY_MODEL_FOR_SCRIPT
		
			IF IS_THIS_MODEL_A_BOAT(thisModel)
			OR thisModel = SUBMERSIBLE
			OR thisModel = SUBMERSIBLE2
				IF IS_PLAYER_FEMALE()
					RETURN "MPCT_13" //Thank you, madam! Your beautiful watercraft is now ready for you at our nearest harbour.
				ELSE
					RETURN "MPCT_14" //Thank you, sir! Your beautiful watercraft is now ready for you at our nearest harbour.
				ENDIF
			ELIF IS_THIS_MODEL_A_PLANE(thisModel)
			OR IS_THIS_MODEL_A_HELI(thisModel)
				IF IS_PLAYER_FEMALE()
					RETURN "MPCT_15" //Thank you, madam! Your spectacular aircraft is now ready for you at our nearest airfield.
				ELSE
					RETURN "MPCT_16" //Thank you, sir! Your spectacular aircraft is now ready for you at our nearest airfield.
				ENDIF
			ELSE
				IF IS_PLAYER_FEMALE()
					RETURN "MPCT_17" //Thank you, madam! Your specialist vehicle is now ready for you at our nearest pick-up station.
				ELSE
					RETURN "MPCT_18" //Thank you, sir! Your specialist vehicle is now ready for you at our nearest pick-up station.
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN "NULL"
ENDFUNC

/// PURPOSE:
///    Maintains ringing, playing conversation and hanging up for vehicle spawn-related phonecalls
PROC MAINTAIN_CLIENT_PHONE_CALLS()
	IF iLocalPhonecallBitset <> 0
		STRING thisCall
		thisCall = GET_CURRENT_PHONE_CALL_LABEL()
		
		IF NOT ARE_STRINGS_EQUAL(thisCall, "NULL")
			//Do the phonecall
			IF NOT IS_BIT_SET(iLocalPhoneBitset, PHONE_BS_CALL_SETUP)
			
				STRING sVoice = "PEGASUS"
				STRING sGroup = "CT_AUD"
				enumCharacterList eChar = CHAR_PEGASUS_DELIVERY
				IF IS_BIT_SET(iLocalPhoneBitset,PHONE_BS_CALL_BOSS_LIMO)
					sVoice = "BOSSAGENCY"
					sGroup = "BACALAU"
					eChar = CHAR_MP_RAY_LAVOY
				ENDIF
			
				ADD_PED_FOR_DIALOGUE(sSpeech, 3, NULL, sVoice)
				IF Request_MP_Comms_Message(sSpeech, eChar, sGroup, thisCall)	
					SET_BIT(iLocalPhoneBitset, PHONE_BS_CALL_SETUP)
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_BIT(iLocalPhoneBitset, PHONE_BS_CALL_SETUP) ", thisCall)
					#ENDIF
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(iLocalPhoneBitset, PHONE_BS_CALL_RECEIVED)
					IF IS_PHONE_ONSCREEN()
						SET_BIT(iLocalPhoneBitset, PHONE_BS_CALL_RECEIVED)
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_BIT(iLocalPhoneBitset, PHONE_BS_CALL_RECEIVED) ", thisCall)
						#ENDIF
					ENDIF
				ELSE
					IF NOT IS_PHONE_ONSCREEN()
						iLocalPhonecallBitset = 0
						iLocalPhoneBitset = 0
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === PHONECALL CLEANUP")
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns the text label for the currently flagged text message
/// PARAMS:
///    returnLabel - location to return the text label in
///    returnCharacter - location to return the character enum in
///    iTextBit - location to return the text message bit in
/// RETURNS:
///    true if successful
FUNC BOOL GET_CURRENT_TEXT_MESSAGE_INFO(TEXT_LABEL_15 &returnLabel, enumCharacterList &returnCharacter, INT &iTextBit)
	
	IF IS_BIT_SET(iLocalTextMessageBitset, TEXTMESSAGE_BS_CLEANUP_MESSAGE_BYPASSED)
		returnLabel = ""
		returnCharacter = NO_CHARACTER
		iTextBit = TEXTMESSAGE_BS_CLEANUP_MESSAGE_BYPASSED
		
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === GET_CURRENT_TEXT_MESSAGE_INFO - returnCharacter = NO_CHARACTER")
		RETURN FALSE
	ENDIF
	
		BOOL bBossLimo = FALSE
	
		IF IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_BOSS_LIMO)
		OR IS_BIT_SET(iLocalBitset,LOCAL_BS_BOSS_LIMO) 
			returnCharacter = CHAR_MP_RAY_LAVOY
			bBossLimo = TRUE
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === GET_CURRENT_TEXT_MESSAGE_INFO - returnCharacter = CHAR_MP_RAY_LAVOY")
		ELSE
			returnCharacter = CHAR_PEGASUS_DELIVERY
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === GET_CURRENT_TEXT_MESSAGE_INFO - returnCharacter = CHAR_PEGASUS_DELIVERY")
		ENDIF
	
	IF IS_BIT_SET(iLocalTextMessageBitset, TEXTMESSAGE_BS_REQUEST_DELIVERY_COLLECTED)
		IF bBossLimo
			returnLabel = "TXT_CAR_GBCOLL" //We have moved your vehicle back into storage. Please contact us again to arrange a delivery.
		ELSE
		    returnLabel = "TXT_CAR_PEGCOLL" //We have moved your vehicle back into storage. Please contact us again to arrange a delivery.
		ENDIF
		
		iTextBit = TEXTMESSAGE_BS_REQUEST_DELIVERY_COLLECTED
		RETURN TRUE
	ELIF IS_BIT_SET(iLocalTextMessageBitset, TEXTMESSAGE_BS_REQUEST_DELIVERY_STOLEN)
		IF bBossLimo
			returnLabel = "GB_TXT_1" //We have moved your vehicle back into storage. Please contact us again to arrange a delivery.
		ELSE
			returnLabel = "PEG_TXT_1" //Our most sincere apologies, we've received reports that your vehicle has been stolen. Please contact us again to arrange a replacement.
		ENDIF
		
		iTextBit = TEXTMESSAGE_BS_REQUEST_DELIVERY_STOLEN
		RETURN TRUE
	ELIF IS_BIT_SET(iLocalTextMessageBitset, TEXTMESSAGE_BS_REQUEST_DELIVERY_DESTROYED)
		IF bBossLimo
			returnLabel = "GB_TXT_2" //We're terrible sorry, but it would appear that your vehicle has been destroyed. Please contact us at any time to arrange a replacement.
		ELSE
			returnLabel = "PEG_TXT_2" //We're terrible sorry, but it would appear that your vehicle has been destroyed. Please contact us at any time to arrange a replacement.
		ENDIF
		
		iTextBit = TEXTMESSAGE_BS_REQUEST_DELIVERY_DESTROYED
		RETURN TRUE
	ELIF IS_BIT_SET(iLocalTextMessageBitset, TEXTMESSAGE_BS_REQUEST_DELIVERY_UNUSED)
		IF bBossLimo
			returnLabel = "GB_TXT_3" //We have moved your vehicle back into storage, as it appeared you were not intending to use it at this time. Please contact us again to arrange a delivery.
		ELSE
			returnLabel = "PEG_TXT_3" //We have moved your vehicle back into storage, as it appeared you were not intending to use it at this time. Please contact us again to arrange a delivery.
		ENDIF
	
		iTextBit = TEXTMESSAGE_BS_REQUEST_DELIVERY_UNUSED
		RETURN TRUE
	ELIF IS_BIT_SET(iLocalTextMessageBitset, TEXTMESSAGE_BS_REQUEST_BOOKING_RETURN_WARNING)
		returnLabel = "PEG_TXT_4" //Remember, you can have it for 12 hours but you'll need to return it after that or the cops will come and get you.
		returnCharacter = CHAR_LESTER
		iTextBit = TEXTMESSAGE_BS_REQUEST_BOOKING_RETURN_WARNING
		RETURN TRUE
	ELIF IS_BIT_SET(iLocalTextMessageBitset, TEXTMESSAGE_BS_REQUEST_BOOKING_WANTED)
		returnLabel = "PEG_TXT_5" //You were warned about that vehicle, you'll have to deal with the cops now.
		returnCharacter = CHAR_LESTER
		iTextBit = TEXTMESSAGE_BS_REQUEST_BOOKING_WANTED
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Processes the local player's text messages for vehicle spawn events
PROC MAINTAIN_CLIENT_TEXT_MESSAGES()

	IF NOT IS_BIT_SET(iLocalTextMessageBitset, TEXTMESSAGE_BS_REQUEST_DELIVERY_COLLECTED)
		IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_PEGASUS_VEHICLE_COLLECTED)
			IF IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_VEHICLE_EXISTS)
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_MP_BOOL_CHARACTER_STAT(MP_STAT_PEGASUS_VEHICLE_COLLECTED, FALSE) as pegasus exists")
				#ENDIF
				SET_MP_BOOL_CHARACTER_STAT(MP_STAT_PEGASUS_VEHICLE_COLLECTED, FALSE)
			ELSE
				IF HAS_NET_TIMER_EXPIRED(bookedVehicle.timeDelayTextMessage, TIME_TO_DELAY_TEXT_MESSAGE)
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_BIT(iLocalTextMessageBitset, TEXTMESSAGE_BS_REQUEST_DELIVERY_COLLECTED)")
					#ENDIF
					SET_BIT(iLocalTextMessageBitset, TEXTMESSAGE_BS_REQUEST_DELIVERY_COLLECTED)
				ELSE
					
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === Time until vehicle collected text message = ",
												TIME_TO_DELAY_TEXT_MESSAGE - ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), bookedVehicle.timeDelayTextMessage.Timer)))
					#ENDIF
				ENDIF
			ENDIF
		ELSE
			RESET_NET_TIMER(bookedVehicle.timeDelayTextMessage)
		ENDIF
	ENDIF

	IF iLocalTextMessageBitset <> 0
		TEXT_LABEL_15 thisMessage
		enumCharacterList thisCharacter
		INT iTextBit
		
		IF GET_CURRENT_TEXT_MESSAGE_INFO(thisMessage, thisCharacter, iTextBit)
			IF IS_OKAY_TO_SEND_CONTACT_TEXT_MESSAGE()
				IF NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)			//Don't bother if player is on transition hud
				AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()							//Don't bother if Brenda's player switch transition thing is in progress
					IF Request_MP_Comms_Txtmsg(thisCharacter, thisMessage)
						IF iTextBit = TEXTMESSAGE_BS_REQUEST_DELIVERY_COLLECTED
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_MP_BOOL_CHARACTER_STAT(MP_STAT_PEGASUS_VEHICLE_COLLECTED, FALSE) as text sent")
							#ENDIF
							SET_MP_BOOL_CHARACTER_STAT(MP_STAT_PEGASUS_VEHICLE_COLLECTED, FALSE)
						ENDIF
						CLEAR_BIT(iLocalTextMessageBitset, iTextBit)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			iLocalTextMessageBitset = 0
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === iLocalTextMessageBitset = 0")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Clears the globals used for storing event information if the local player is not the script host
PROC MAINTAIN_CLIENT_EVENT_GLOBALS()
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
			IF g_iVehicleSpawnRequestPlayer <> -1
				g_iVehicleSpawnRequestPlayer = -1
			ENDIF
			IF g_iVehicleSpawnRequestRequest <> -1
				g_iVehicleSpawnRequestRequest = -1
			ENDIF
			IF g_iVehicleSpawnRequestVSID <> -1
				g_iVehicleSpawnRequestVSID = -1
			ENDIF
			IF g_iVehicleSpawnCompletePlayer <> -1
				g_iVehicleSpawnCompletePlayer = -1
			ENDIF
			IF g_iVehicleSpawnCompleteRequest <> -1
				g_iVehicleSpawnCompleteRequest = -1
			ENDIF
			IF g_iVehicleSpawnCompleteVSID <> -1
				g_iVehicleSpawnCompleteVSID = -1
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Processes the local player re-creating their last pegasus vehicle if it was destroyed because they entered a corona
PROC MAINTAIN_CLIENT_RESTORE_LAST_VEHICLE()

	IF IS_BIT_SET(g_TransitionSessionNonResetVars.iRestoreLastVehicleBitset, TRAN_SESS_NON_RESET_LAST_VEHICLE_CANT_SPAWN_PEGASUS)
		SET_BIT(iLocalTextMessageBitset, TEXTMESSAGE_BS_REQUEST_DELIVERY_COLLECTED)		
		CLEAR_BIT(g_TransitionSessionNonResetVars.iRestoreLastVehicleBitset, TRAN_SESS_NON_RESET_LAST_VEHICLE_CANT_SPAWN_PEGASUS)
	ENDIF
	
	IF IS_BIT_SET(g_TransitionSessionNonResetVars.iRestoreLastVehicleBitset, TRAN_SESS_NON_RESET_LAST_VEHICLE_NEED_PEGASUS_SPAWN)
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAIN_CLIENT_RESTORE_LAST_VEHICLE - TRAN_SESS_NON_RESET_LAST_VEHICLE_NEED_PEGASUS_SPAWN is set")
		#ENDIF
		IF NOT IS_BIT_SET(g_TransitionSessionNonResetVars.iRestoreLastVehicleBitset, TRAN_SESS_NON_RESET_LAST_VEHICLE_PEGASUS_SPAWN_SUCCESS)
		AND NOT IS_BIT_SET(g_TransitionSessionNonResetVars.iRestoreLastVehicleBitset, TRAN_SESS_NON_RESET_LAST_VEHICLE_PEGASUS_SPAWN_FAIL)
			IF HAS_NET_TIMER_EXPIRED(bookedVehicle.timeForceSpawnFail, TIME_TO_FORCE_SPAWN_FAIL)
				SET_BIT(g_TransitionSessionNonResetVars.iRestoreLastVehicleBitset, TRAN_SESS_NON_RESET_LAST_VEHICLE_PEGASUS_SPAWN_FAIL)
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAIN_CLIENT_RESTORE_LAST_VEHICLE - Fail, timeout")
				#ENDIF
				
					IF IS_BIT_SET(g_TransitionSessionNonResetVars.iRestoreLastVehicleBitset, TRAN_SESS_NON_RESET_LAST_VEHICLE_CLEAN_WHEN_DONE)
						CLEAR_BIT(g_TransitionSessionNonResetVars.iRestoreLastVehicleBitset, TRAN_SESS_NON_RESET_LAST_VEHICLE_NEED_PEGASUS_SPAWN)
					ENDIF
			ELSE
				IF NOT IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_VEHICLE_EXISTS)
					IF NOT IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_BOOKED)
						MODEL_NAMES mnVehicleModel = g_TransitionSessionNonResetVars.sLastVehAfterJobData.VehicleSetup.eModel
						
						IF mnVehicleModel <> DUMMY_MODEL_FOR_SCRIPT
							REQUEST_MODEL(mnVehicleModel)
							IF HAS_MODEL_LOADED(mnVehicleModel)
								IF NOT IS_BIT_SET(bookedVehicle.iForceSpawnVehicleBitset, FORCE_BS_RESERVED)
								
									IF CAN_RESERVE_NETWORK_VEHICLES_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_VEHICLES()+1, FALSE, TRUE)
										#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === RESERVE_NETWORK_MISSION_VEHICLES(", GET_NUM_RESERVED_MISSION_VEHICLES()+1, ")", 
																	", GET_NUM_CREATED_MISSION_VEHICLES() = ", GET_NUM_CREATED_MISSION_VEHICLES(FALSE))
										#ENDIF
										RESERVE_NETWORK_MISSION_VEHICLES(GET_NUM_RESERVED_MISSION_VEHICLES()+1)
										SET_BIT(bookedVehicle.iForceSpawnVehicleBitset, FORCE_BS_RESERVED)
									ENDIF
								ELSE
							
									IF CAN_REGISTER_MISSION_VEHICLES(1)
										IF CAN_SPAWN_BOOKED_VEHICLE(eVeh_spawn_2_PEGASUS)
											SET_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_2_PEGASUS)
											
											SET_BIT(bookedVehicle.iBitset, BOOK_BS_BOOKED)
											SET_BIT(bookedVehicle.iBitset, BOOK_BS_DELIVERY)
											
											IF SPAWN_BOOKED_PEGASUS_VEHICLE(mnVehicleModel, 
																	g_TransitionSessionNonResetVars.vLastVehicleBeforeCoronaSavePosition,
																	g_TransitionSessionNonResetVars.fLastVehicleBeforeCoronaSaveHeading)
												
												bookedVehicle.timeCountdown = timeNet
												bookedVehicle.vrType = GET_MODEL_VEHICLE_REQUEST_TYPE(mnVehicleModel)
												bookedVehicle.mnBookModel = mnVehicleModel
												bookedVehicle.iForceSpawnVehicleBitset = 0
												
												RESERVE_NETWORK_MISSION_VEHICLES(GET_NUM_RESERVED_MISSION_VEHICLES()-1)
												SET_MODEL_AS_NO_LONGER_NEEDED(mnVehicleModel)
												
												#IF IS_DEBUG_BUILD
												CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_BIT(g_TransitionSessionNonResetVars.iRestoreLastVehicleBitset, TRAN_SESS_NON_RESET_LAST_VEHICLE_PEGASUS_SPAWN_SUCCESS)")
												#ENDIF
												
												SET_BIT(g_TransitionSessionNonResetVars.iRestoreLastVehicleBitset, TRAN_SESS_NON_RESET_LAST_VEHICLE_PEGASUS_SPAWN_SUCCESS)
												
												CLEAR_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_2_PEGASUS)
												
													IF IS_BIT_SET(g_TransitionSessionNonResetVars.iRestoreLastVehicleBitset, TRAN_SESS_NON_RESET_LAST_VEHICLE_CLEAN_WHEN_DONE)
														CLEAR_BIT(g_TransitionSessionNonResetVars.iRestoreLastVehicleBitset, TRAN_SESS_NON_RESET_LAST_VEHICLE_NEED_PEGASUS_SPAWN)
													ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAIN_CLIENT_RESTORE_LAST_VEHICLE - Vehicle is already booked")
						#ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAIN_CLIENT_RESTORE_LAST_VEHICLE - Booked vehicle already exists!")
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		RESET_NET_TIMER(bookedVehicle.timeForceSpawnFail)
	ENDIF
	
ENDPROC


/// PURPOSE:
///    Return TRUE if the vehicle passed is one we should be maintaining doors on
FUNC BOOL SHOULD_DOORS_BE_MAINTAINED_FOR_VEHICLE(MODEL_NAMES mnVeh)

	// Currently only the SWIFT2 we need to do this for
	IF IS_PLAYER_IN_LUX_HELI_VEHICLE(mnVeh)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if any ped / player is entering the luxe vehicles. Sets the bool that are passed by reference for use elsewhere.
FUNC BOOL ARE_ANY_PLAYERS_ENTERING_LUXE_VEHICLE(VEHICLE_INDEX vehIndex, BOOL &bLeftDoorInUse, BOOL &bRightDoorInUse)
	
	// Reset the values
	bLeftDoorInUse = FALSE
	bRightDoorInUse = FALSE
	
	// Grab the peds that are using the doors on the vehicle (If we are concerned about other doors we may need to create a lookup)
	PED_INDEX aPedLeft = GET_PED_USING_VEHICLE_DOOR(vehIndex, SC_DOOR_REAR_LEFT)
	PED_INDEX aPedRight = GET_PED_USING_VEHICLE_DOOR(vehIndex, SC_DOOR_REAR_RIGHT)
	
	// If the peds are valid, update bool to show this
	IF DOES_ENTITY_EXIST(aPedLeft)
	AND NOT IS_ENTITY_DEAD(aPedLeft)
		bLeftDoorInUse = TRUE
	ENDIF
	
	IF DOES_ENTITY_EXIST(aPedRight)
	AND NOT IS_ENTITY_DEAD(aPedRight)
		bRightDoorInUse = TRUE
	ENDIF
	
	// If both the doors are in use, pointless trying to close it. Return TRUE
	IF (bLeftDoorInUse AND bRightDoorInUse)
		RETURN TRUE	
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

/// PURPOSE:
///    Monitor all Pegasus vehciles and their door states to make sure it is closed when not in use
PROC MAINTAIN_LUXE_VEH_DOOR_STATE()
	
	// Does the 'players' Pegasus vehicle exist?
	IF DOES_ENTITY_EXIST(MPGlobals.RemotePegV[iLuxeVehDoorStateStaggeredLoop])
	
		//...yes, grab the vehicle and confirm it is a vehicle we are concerned about
		MODEL_NAMES mnVeh = GET_ENTITY_MODEL(MPGlobals.RemotePegV[iLuxeVehDoorStateStaggeredLoop])
		
		// Check if the vehicle is one we are concerned about		
		IF SHOULD_DOORS_BE_MAINTAINED_FOR_VEHICLE(mnVeh)

			// If we are in control of the entity and either door is open, we should attempt to close.
			IF NETWORK_HAS_CONTROL_OF_ENTITY(MPGlobals.RemotePegV[iLuxeVehDoorStateStaggeredLoop])
				IF DOES_ENTITY_EXIST(MPGlobals.RemotePegV[iLuxeVehDoorStateStaggeredLoop])
				AND NOT IS_ENTITY_DEAD(MPGlobals.RemotePegV[iLuxeVehDoorStateStaggeredLoop])
					IF IS_VEHICLE_DOOR_FULLY_OPEN(MPGlobals.RemotePegV[iLuxeVehDoorStateStaggeredLoop], SC_DOOR_REAR_LEFT)
					OR IS_VEHICLE_DOOR_FULLY_OPEN(MPGlobals.RemotePegV[iLuxeVehDoorStateStaggeredLoop], SC_DOOR_REAR_RIGHT)
						
						BOOL bLeftDoorInUse
						BOOL bRightDoorInUse
						
						// Confirm that there are not players trying to enter both of the doors.
						IF NOT ARE_ANY_PLAYERS_ENTERING_LUXE_VEHICLE(MPGlobals.RemotePegV[iLuxeVehDoorStateStaggeredLoop], bLeftDoorInUse, bRightDoorInUse)
							
							PRINTLN("MAINTAIN_LUXE_VEH_DOOR_STATE: Doors are not in full use. Attempt to close. Player Int: ", iLuxeVehDoorStateStaggeredLoop)
							
							// If the left or right doors are not in use then close them (not instantly as this causes a pop).
							IF NOT bLeftDoorInUse
								PRINTLN("MAINTAIN_LUXE_VEH_DOOR_STATE: SHUT LEFT")
								SET_VEHICLE_DOOR_SHUT(MPGlobals.RemotePegV[iLuxeVehDoorStateStaggeredLoop], SC_DOOR_REAR_LEFT, FALSE)
							ENDIF
							
							IF NOT bRightDoorInUse
								PRINTLN("MAINTAIN_LUXE_VEH_DOOR_STATE: SHUT RIGHT")
								SET_VEHICLE_DOOR_SHUT(MPGlobals.RemotePegV[iLuxeVehDoorStateStaggeredLoop], SC_DOOR_REAR_RIGHT, FALSE)
							ENDIF						
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF		
	ENDIF
		
	// Increment our loop for the next frame
	iLuxeVehDoorStateStaggeredLoop++
	
	// Loop round once we have checked every potential vehicle
	IF iLuxeVehDoorStateStaggeredLoop >= NUM_NETWORK_PLAYERS
		iLuxeVehDoorStateStaggeredLoop = 0
	ENDIF
ENDPROC


PROC DELETE_REMOTE_MP_SAVED_VEHICLE_AFTER_FADE_OUT(VEHICLE_INDEX &tempVeh)
	BOOL bSetAsMissionEntity
	IF DOES_ENTITY_EXIST(tempVeh)
		IF NETWORK_GET_ENTITY_IS_NETWORKED(tempVeh)
			IF (NOT IS_ENTITY_DEAD(tempVeh) AND NOT NETWORK_IS_ENTITY_FADING(tempVeh))
			OR IS_ENTITY_DEAD(tempVeh)
				IF IS_VEHICLE_EMPTY(tempVeh, TRUE, TRUE,FALSE, TRUE)
					IF NOT IS_ENTITY_VISIBLE(tempVeh)
						IF CAN_EDIT_THIS_ENTITY(tempVeh, bSetAsMissionEntity)
							DELETE_VEHICLE(tempVeh)
							PRINTLN("DELETE_REMOTE_MP_SAVED_VEHICLE_AFTER_FADE_OUT - deleted faded out vehicle")
						ELSE
							PRINTLN("DELETE_REMOTE_MP_SAVED_VEHICLE_AFTER_FADE_OUT - CAN_EDIT_THIS_ENTITY = FALSE")	
						ENDIF
					ELSE
						PRINTLN("DELETE_REMOTE_MP_SAVED_VEHICLE_AFTER_FADE_OUT - IS_ENTITY_VISIBLE = true ")	
					ENDIF
				ELSE
					PRINTLN("DELETE_REMOTE_MP_SAVED_VEHICLE_AFTER_FADE_OUT - vehilce not empty.")	
				ENDIF
			ELSE
				PRINTLN("DELETE_REMOTE_MP_SAVED_VEHICLE_AFTER_FADE_OUT - entity is fading.")
			ENDIF
		ELSE
			PRINTLN("DELETE_REMOTE_MP_SAVED_VEHICLE_AFTER_FADE_OUT - entity is not networked.")
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_CLIENT_CONVERT_TO_PEGASUS_VEHICLE()

	IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehToMakePegasus)
	
		// if there is a booked vehicle that is not yet created then clear it.
		IF IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_BLIP_CREATED_FOR_SPAWN_POINT)
		OR IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_DELIVERY_COUNTDOWN_SET)
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAIN_CLIENT_CONVERT_TO_PEGASUS_VEHICLE - clearing booked vehicle, bits set.")
			#ENDIF	
			CLEAR_BOOKED_VEHICLE()
		ENDIF	
	
		 // cleanup existing pegasus vehicle first.
		IF DOES_ENTITY_EXIST(MPGlobals.RemotePegV[NATIVE_TO_INT(PLAYER_ID())])
			IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(MPGlobals.RemotePegV[NATIVE_TO_INT(PLAYER_ID())], FALSE)
				IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV)
					IF NOT MPGlobalsAmbience.bConvertToPegasusDontClearBookedVehicle
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAIN_CLIENT_CONVERT_TO_PEGASUS_VEHICLE - clearing booked vehicle.")
						#ENDIF			
						CLEAR_BOOKED_VEHICLE()
					ELSE
						// So that other players will get a prompt to leave it
						CLEAR_BOOKED_VEHICLE(DEFAULT, DEFAULT, FALSE)
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV = NULL
					ENDIF
				ENDIF				
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAIN_CLIENT_CONVERT_TO_PEGASUS_VEHICLE - existing RemotePev - belongs to this script.")
				#ENDIF

					MPGlobals.OldRemotePegV[NATIVE_TO_INT(PLAYER_ID())] = MPGlobals.RemotePegV[NATIVE_TO_INT(PLAYER_ID())]
				CLEANUP_REMOTE_MP_PEG_SAVED_VEHICLE(MPGlobals.RemotePegV[NATIVE_TO_INT(PLAYER_ID())], FALSE, NATIVE_TO_INT(PLAYER_ID()))
				EXIT		
			ENDIF
		ENDIF
		
			IF MPGlobalsAmbience.bConvertToPegasusDontClearBookedVehicle
				MPGlobalsAmbience.bConvertToPegasusDontClearBookedVehicle = FALSE
				
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAIN_CLIENT_CONVERT_TO_PEGASUS_VEHICLE - bConvertToPegasusDontClearBookedVehicle was TRUE")
				#ENDIF
			ENDIF

		// remove existing blip
		IF DOES_BLIP_EXIST(bookedVehicle.blipID)
			REMOVE_BLIP(bookedVehicle.blipID)
		ENDIF
		
		// set new vehicle as pegasus vehicle
		IF SET_VEHICLE_AS_PEGASUS_VEHICLE(MPGlobalsAmbience.vehToMakePegasus)	
			MPGlobalsAmbience.vehToMakePegasus = NULL
		ENDIF

	ENDIF
		

ENDPROC


PROC MAINTAIN_CLIENT_CONVERT_TO_TRUCK_VEHICLE()

	IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehToMakeTruck[0])
	AND DOES_ENTITY_EXIST(MPGlobalsAmbience.vehToMakeTruck[1])
	
		// set new vehicle as truck
		IF SET_VEHICLE_AS_TRUCK_VEHICLE(MPGlobalsAmbience.vehToMakeTruck[0], MPGlobalsAmbience.vehToMakeTruck[1])	
			MPGlobalsAmbience.vehToMakeTruck[0] = NULL
			MPGlobalsAmbience.vehToMakeTruck[1] = NULL
		ENDIF
	ENDIF
		

ENDPROC

PROC MAINTAIN_CLIENT_CONVERT_TO_AVENGER_VEHICLE()

	IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehToMakeAvenger)
	
		// set new vehicle as truck
		IF SET_VEHICLE_AS_AVENGER_VEHICLE(MPGlobalsAmbience.vehToMakeAvenger)
			MPGlobalsAmbience.vehToMakeAvenger = NULL
		ENDIF
	ENDIF

ENDPROC

PROC MAINTAIN_CLIENT_CONVERT_TO_HACKER_TRUCK_VEHICLE()

	IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehToMakeHackerTruck)
	
		// set new vehicle as hacker truck
		IF SET_VEHICLE_AS_HACKER_TRUCK_VEHICLE(MPGlobalsAmbience.vehToMakeHackerTruck)
			MPGlobalsAmbience.vehToMakeHackerTruck = NULL
		ENDIF
	ENDIF

ENDPROC

#IF FEATURE_DLC_2_2022
PROC MAINTAIN_CLIENT_CONVERT_TO_ACID_LAB_VEHICLE()

	IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehToMakeAcidLab)
	
		// set new vehicle as hacker truck
		IF SET_VEHICLE_AS_ACID_LAB_VEHICLE(MPGlobalsAmbience.vehToMakeAcidLab)
			MPGlobalsAmbience.vehToMakeAcidLab = NULL
		ENDIF
	ENDIF

ENDPROC
#ENDIF

#IF FEATURE_HEIST_ISLAND
PROC MAINTAIN_CLIENT_CONVERT_TO_SUBMARINE_VEHICLE()

	IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehToMakeSubmarine)
	
		// set new vehicle as hacker truck
		IF SET_VEHICLE_AS_SUBMARINE_VEHICLE(MPGlobalsAmbience.vehToMakeSubmarine)
			MPGlobalsAmbience.vehToMakeSubmarine = NULL
		ENDIF
	ENDIF

ENDPROC

PROC MAINTAIN_CLIENT_CONVERT_TO_SUBMARINE_DINGHY_VEHICLE()

	IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehToMakeSubmarineDinghy)
	
		// set new vehicle as dinghy
		IF SET_VEHICLE_AS_SUBMARINE_DINGHY_VEHICLE(MPGlobalsAmbience.vehToMakeSubmarineDinghy)
			MPGlobalsAmbience.vehToMakeSubmarineDinghy = NULL
		ENDIF
	ENDIF

ENDPROC
#ENDIF

#IF FEATURE_DLC_2_2022
PROC MAINTAIN_CLIENT_CONVERT_TO_SUPPORT_BIKE_VEHICLE()

	IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehToMakeSupportBike)
	
		// set new vehicle as dinghy
		IF SET_VEHICLE_AS_SUPPORT_BIKE_VEHICLE(MPGlobalsAmbience.vehToMakeSupportBike)
			MPGlobalsAmbience.vehToMakeSupportBike = NULL
		ENDIF
	ENDIF

ENDPROC
#ENDIF

SCRIPT_TIMER timerBeforeForceCleanupForOfficeHeliCreation // timer before we force cleanup of old pegasus vehicloe, in case someone has trouble leaving it
PROC MAINTAIN_CREATION_OF_OFFICE_HELICOPTER()
	
	IF g_OfficeHeliDockData.bCreateHeliForTakeoff
	//AND g_OfficeHeliDockData.iBossRequestedPegasusHeliType > -1
	AND (g_OfficeHeliDockData.ePegasusBuyableVehicle != UNSET_BUYABLE_VEHICLE)
	//AND g_OfficeHeliDockData.iBossRequestedPegasusHeliType < NUMBER_OF_PEGASUS_HELI_LIST_OPTIONS
		
		IF iCreationOfOfficeHelicopterStage = 0
			// if there is a booked vehicle that is not yet created then clear it.
			IF IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_BLIP_CREATED_FOR_SPAWN_POINT)
			OR IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_DELIVERY_COUNTDOWN_SET)
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAIN_CREATION_OF_OFFICE_HELICOPTER - clearing booked vehicle, bits set.")
				#ENDIF	
				CLEAR_BOOKED_VEHICLE()
			ENDIF
			
			 // cleanup existing pegasus vehicle first.
			IF DOES_ENTITY_EXIST(MPGlobals.RemotePegV[NATIVE_TO_INT(PLAYER_ID())])
				IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(MPGlobals.RemotePegV[NATIVE_TO_INT(PLAYER_ID())], FALSE)
					IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV) 				
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAIN_CREATION_OF_OFFICE_HELICOPTER - clearing booked vehicle.")
						#ENDIF
						
						IF HAS_NET_TIMER_EXPIRED(timerBeforeForceCleanupForOfficeHeliCreation, 10000)
							CLEAR_BOOKED_VEHICLE()
						ENDIF
					ENDIF				
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAIN_CREATION_OF_OFFICE_HELICOPTER - existing RemotePev - belongs to this script.")
					#ENDIF
					MPGlobals.OldRemotePegV[NATIVE_TO_INT(PLAYER_ID())] = MPGlobals.RemotePegV[NATIVE_TO_INT(PLAYER_ID())]
					CLEANUP_REMOTE_MP_PEG_SAVED_VEHICLE(MPGlobals.RemotePegV[NATIVE_TO_INT(PLAYER_ID())]  , TRUE )
					EXIT
				ENDIF
			ENDIF
			
			CLEAR_BOOKED_VEHICLE()
			
			// remove existing blip
			IF DOES_BLIP_EXIST(bookedVehicle.blipID)
				REMOVE_BLIP(bookedVehicle.blipID)
			ENDIF
			
			modelNameOfficeHelicopter = GET_MODEL_FOR_BUYABLE_VEHICLE(g_OfficeHeliDockData.ePegasusBuyableVehicle) //GET_MODEL_FOR_PEGASUS_HELI(g_OfficeHeliDockData.iBossRequestedPegasusHeliType)
			REQUEST_MODEL(modelNameOfficeHelicopter)
			
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK_TAKEOFF] MAINTAIN_CREATION_OF_OFFICE_HELICOPTER - model requested")
			#ENDIF
			
			iCreationOfOfficeHelicopterStage = 1
		ENDIF

		IF iCreationOfOfficeHelicopterStage = 1
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK_TAKEOFF] MAINTAIN_CREATION_OF_OFFICE_HELICOPTER - waiting for model to load")
			#ENDIF
		
			IF HAS_MODEL_LOADED(modelNameOfficeHelicopter)
				
				VECTOR vSpawnPoint
				FLOAT fSpawnHeading = 0.0
					
				vSpawnPoint = GET_PLAYER_PERCEIVED_COORDS(PLAYER_ID())
				vSpawnPoint.Z = -20.0
				
				IF IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), FALSE)
					
					IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
						SIMPLE_INTERIORS eSimpleInteriorID = GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID())
					
						IF eSimpleInteriorID != SIMPLE_INTERIOR_INVALID
							
							INT iRandom = GET_RANDOM_INT_IN_RANGE(0, 4)
							
							GET_DEFUNCT_BASE_OUTSIDE_HELI_SPAWN_POINT(eSimpleInteriorID, iRandom, vSpawnPoint, fSpawnHeading)
							#IF IS_DEBUG_BUILD							
							CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK_TAKEOFF] MAINTAIN_CREATION_OF_OFFICE_HELICOPTER - Player is in a defunct base, spawning heli under base at Position: ", vSpawnPoint, ", Heading: ", fSpawnHeading)
							#ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
							CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK_TAKEOFF] MAINTAIN_CREATION_OF_OFFICE_HELICOPTER - Invalid simple interior, unable to retrieve heli spawn point.")
							#ENDIF
						ENDIF
					ELIF IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(PLAYER_ID())
						SIMPLE_INTERIORS eSimpleInteriorID = GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID())
						
						IF eSimpleInteriorID != SIMPLE_INTERIOR_INVALID
							IF DOES_THIS_SIMPLE_INTERIOR_ALLOW_EXIT_VIA_HELIPAD(eSimpleInteriorID)
								vSpawnPoint = GET_SIMPLE_INTERIOR_HELIPAD_POSITION(eSimpleInteriorID)
								CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK_TAKEOFF] MAINTAIN_CREATION_OF_OFFICE_HELICOPTER - Player is in a simple interior, spawning heli on the helipad at ", vSpawnPoint)
							ENDIF
						ELSE
							CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK_TAKEOFF] MAINTAIN_CREATION_OF_OFFICE_HELICOPTER - Invalid simple interior, unable to retrieve heli spawn point.")
						ENDIF
					ELSE
					
						INT iBuildingID = GET_PROPERTY_BUILDING(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
						
						IF DOES_BUILDING_HAVE_A_HELIPAD(iBuildingID)
							vSpawnPoint = GET_BUILDING_HELIPAD_POSITION(iBuildingID) // - <<0.0, 0.0, 30>>
							#IF IS_DEBUG_BUILD
								CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK_TAKEOFF] MAINTAIN_CREATION_OF_OFFICE_HELICOPTER - Player is in an office, spawning heli under the helipad at ", vSpawnPoint)
							#ENDIF
						ENDIF
					
					ENDIF
				ENDIF
				
				IF CREATE_NET_VEHICLE(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV, modelNameOfficeHelicopter, vSpawnPoint, 0.0, FALSE)
					#IF IS_DEBUG_BUILD
						CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK_TAKEOFF] MAINTAIN_CREATION_OF_OFFICE_HELICOPTER - vehicle created")
						CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAIN_CREATION_OF_OFFICE_HELICOPTER - vehicle created")
					#ENDIF
					
					VEHICLE_INDEX vehHeli = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV)
					
					IF NOT IS_ENTITY_DEAD(vehHeli)
					
						SET_ENTITY_HEADING(vehHeli, fSpawnHeading)
					
						FREEZE_ENTITY_POSITION(vehHeli, TRUE)
						
						SET_ENTITY_COLLISION(vehHeli, FALSE)
						SET_ENTITY_VISIBLE(vehHeli, FALSE)
						SET_ENTITY_PROOFS(vehHeli, TRUE, TRUE, TRUE, TRUE, TRUE)
					
						SET_VEHICLE_ENGINE_ON(vehHeli, TRUE, TRUE)
						SET_HELI_BLADES_FULL_SPEED(vehHeli)
						#IF IS_DEBUG_BUILD
							CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK_TAKEOFF][SVM] MAINTAIN_CREATION_OF_OFFICE_HELICOPTER - Setting engine on.")
						#ENDIF
					ENDIF
					
					SET_MODEL_AS_NO_LONGER_NEEDED(modelNameOfficeHelicopter)
					
					bookedVehicle.ePegasusBuyableVehicle = g_OfficeHeliDockData.ePegasusBuyableVehicle
					
					iCreationOfOfficeHelicopterStage = 2
				ENDIF
			ENDIF
		ENDIF
		
		IF iCreationOfOfficeHelicopterStage = 2
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK_TAKEOFF] MAINTAIN_CREATION_OF_OFFICE_HELICOPTER - waiting for setting veh as pegasus")
			#ENDIF
			
			IF SET_VEHICLE_AS_PEGASUS_VEHICLE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV), g_OfficeHeliDockData.ePegasusBuyableVehicle)
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK_TAKEOFF] MAINTAIN_CREATION_OF_OFFICE_HELICOPTER - vehicle set as pegasus, all done")
					CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MAINTAIN_CREATION_OF_OFFICE_HELICOPTER - vehicle set as pegasus, all done")
				#ENDIF
					
				iCreationOfOfficeHelicopterStage = 0
				//g_OfficeHeliDockData.iBossRequestedPegasusHeliType = -1
				g_OfficeHeliDockData.ePegasusBuyableVehicle = UNSET_BUYABLE_VEHICLE
				g_OfficeHeliDockData.bCreateHeliForTakeoff = FALSE
				g_OfficeHeliDockData.bHeliForTakeoffCreated = TRUE	

				RESET_NET_TIMER(timerBeforeForceCleanupForOfficeHeliCreation)
			ENDIF
		ENDIF
		
	ENDIF
ENDPROC

PROC MAINTAIN_WARP_INTO_VEHICLE_FROM_DEFUNCT_BASE()
	
	IF g_OfficeHeliDockData.bHeliForTakeoffCreated
	
		IF IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(PLAYER_ID())
						
			SIMPLE_INTERIORS eSimpleInteriorID = GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID())
		
			IF eSimpleInteriorID != SIMPLE_INTERIOR_INVALID
				
				IF GET_SIMPLE_INTERIOR_TYPE(eSimpleInteriorID) = SIMPLE_INTERIOR_TYPE_DEFUNCT_BASE

					IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV)
					AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV)

						VEHICLE_INDEX vehIndex = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV)
						
						IF IS_ENTITY_ALIVE(vehIndex)
							
							IF IS_SCREEN_FADED_IN() OR IS_SCREEN_FADING_IN()
								DO_SCREEN_FADE_OUT(500)
								PRINTLN("[HELI_DOCK_TAKEOFF] MAINTAIN_CREATION_OF_OFFICE_HELICOPTER - Fading screen out.")
							ENDIF
							
							INT fallbackPoint
							VECTOR newSpawnPoint
							FLOAT newSpawnHeading
							REPEAT 31 fallbackPoint
								bool goodPoint = TRUE
								IF fallbackPoint > 0
									GET_DEFUNCT_BASE_OUTSIDE_HELI_SPAWN_POINT(eSimpleInteriorID, fallbackPoint - 1, newSpawnPoint, newSpawnHeading)
								ELSE
									newSpawnPoint = GET_ENTITY_COORDS(vehIndex)
								ENDIF
								
								INT i
								PLAYER_INDEX PlayerID
								REPEAT NUM_NETWORK_PLAYERS i
									PlayerID = INT_TO_NATIVE(PLAYER_INDEX, i)		
									IF NOT (PlayerID = PLAYER_ID())
									AND IS_NET_PLAYER_OK(PlayerID)
										PRINTLN("[HELI_DOCK_TAKEOFF] MAINTAIN_CREATION_OF_OFFICE_HELICOPTER - Checking position with player ", i, " heli.")		
										IF VDIST2(GET_ENTITY_COORDS(GET_PLAYER_PED(PlayerID)), newSpawnPoint) < 300					
											goodPoint = FALSE
											BREAKLOOP
										ENDIF
									ENDIF
								ENDREPEAT
								
								IF goodPoint
									IF fallbackPoint > 0
										SET_ENTITY_COORDS(vehIndex, newSpawnPoint)
										SET_ENTITY_HEADING(vehIndex, newSpawnHeading)
										PRINTLN("[HELI_DOCK_TAKEOFF] MAINTAIN_CREATION_OF_OFFICE_HELICOPTER - Another heli in the same spot, spawning in another location (", fallbackPoint, ").")
									ELSE
										PRINTLN("[HELI_DOCK_TAKEOFF] MAINTAIN_CREATION_OF_OFFICE_HELICOPTER - No position conflict, not moving heli.")
									ENDIF
									BREAKLOOP
								ENDIF
							ENDREPEAT
							
							IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
								VECTOR vCamDirection = <<0.0, 0.0, GET_ENTITY_HEADING(vehIndex)>>
								NEW_LOAD_SCENE_START(GET_ENTITY_COORDS(vehIndex), vCamDirection, 500.0)
							ENDIF
							
							IF IS_NEW_LOAD_SCENE_ACTIVE()

								IF IS_SCREEN_FADED_IN() OR IS_SCREEN_FADING_IN()
									DO_SCREEN_FADE_OUT(500)
								ENDIF
							
								IF IS_NEW_LOAD_SCENE_LOADED()
									NEW_LOAD_SCENE_STOP()
									
									IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
										SET_PED_CAN_RAGDOLL(PLAYER_PED_ID(), FALSE)
										PRINTLN("[HELI_DOCK_TAKEOFF] MAINTAIN_CREATION_OF_OFFICE_HELICOPTER - Calling SET_PED_CAN_RAGDOLL(PLAYER_PED_ID(), FALSE)")
									ENDIF
									
									WarpPlayerIntoCar(PLAYER_PED_ID(), vehIndex, VS_DRIVER)
									PRINTLN("[HELI_DOCK_TAKEOFF] MAINTAIN_CREATION_OF_OFFICE_HELICOPTER - Warping player into pegasus vehicle.")
								ENDIF
			
							ENDIF
						ENDIF
					ELSE
						PRINTLN("[HELI_DOCK_TAKEOFF] MAINTAIN_CREATION_OF_OFFICE_HELICOPTER - Invalid network ID for pegasus concierge vehicle.")
					ENDIF
				ENDIF
			ENDIF
		ELSE
			VEHICLE_INDEX vehIndex = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV)
		
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehIndex)
				PRINTLN("[HELI_DOCK_TAKEOFF] MAINTAIN_CREATION_OF_OFFICE_HELICOPTER - Player is in the vehicle!")
					
				SET_ENTITY_COLLISION(vehIndex, TRUE)
				SET_ENTITY_VISIBLE(vehIndex, TRUE)
				SET_ENTITY_PROOFS(vehIndex, FALSE, FALSE, FALSE, FALSE, FALSE)
				SET_VEHICLE_DOORS_LOCKED(vehIndex, VEHICLELOCK_UNLOCKED)
				SET_VEHICLE_ENGINE_ON(vehIndex, TRUE, TRUE)
				SET_HELI_BLADES_FULL_SPEED(vehIndex)
				
				IF GET_VEHICLE_HAS_LANDING_GEAR(vehIndex)
					CONTROL_LANDING_GEAR(vehIndex, LGC_RETRACT_INSTANT)
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					SET_PED_CAN_RAGDOLL(PLAYER_PED_ID(), TRUE)
					PRINTLN("[HELI_DOCK_TAKEOFF] MAINTAIN_CREATION_OF_OFFICE_HELICOPTER - SET_PED_CAN_RAGDOLL(PLAYER_PED_ID(), TRUE)")
				ENDIF
				
				FREEZE_ENTITY_POSITION(vehIndex, FALSE)

				// Place camera behind player. 
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(2500)
					PRINTLN("[HELI_DOCK_TAKEOFF] MAINTAIN_CREATION_OF_OFFICE_HELICOPTER - Fading screen in.")
					
					SET_LOCAL_PLAYER_IS_DOING_HELI_DOCK_CUTSCENE(FALSE)
					
					g_OfficeHeliDockData.bHeliForTakeoffCreated = FALSE
					PRINTLN("[HELI_DOCK_TAKEOFF] MAINTAIN_CREATION_OF_OFFICE_HELICOPTER - Setting g_OfficeHeliDockData.bHeliForTakeoffCreated = FALSE")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

#IF FEATURE_HEIST_ISLAND
PROC INITIALIZE_SUBMARINE_CALM_ID()
	INT i 
	FOR i = 0 TO NUM_NETWORK_PLAYERS - 1
		iSubmarineCalmID[i] = -1
	ENDFOR	
ENDPROC
#ENDIF

PROC SCRIPT_INITIALISE()
	luxeActState.luxeActivityState = CL_LUXE_ACT_INIT
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NOT NETWORK_IS_ACTIVITY_SESSION()
			IF IS_GUNRUNNING_TRUCK_PURCHASED()
				BROADCAST_PLAYER_PURCHASED_ARMORY_TRUCK(TRUE)
				BROADCAST_PLAYER_TRUCK_PURCHASED_SECTIONS()
				BROADCAST_PLAYER_TRUCK_PURCHASED_SECTION_TINT()
				SET_REQUEST_TO_CREATE_ARMORY_TRUCK_IN_BUNKER(TRUE)
//				ASSIGN_TRUCK_TO_MAIN_THREAD(TRUE)
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.eTruckModel = GET_GUNRUNNING_TRUCK_MODEL()
				CPRINTLN(DEBUG_INTERNET, "AM_VEHICLE_SPAWN - SCRIPT_INITIALISE - ...eTruckModel = ", GET_MODEL_NAME_FOR_DEBUG(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.eTruckModel))
			ELSE
				BROADCAST_PLAYER_PURCHASED_ARMORY_TRUCK(FALSE)
			ENDIF
			IF IS_ARMORY_AIRCRAFT_PURCHASED()
				BROADCAST_PLAYER_PURCHASED_ARMORY_AIRCRAFT(TRUE)
			//	BROADCAST_PLAYER_AIRCRAFT_PURCHASED_SECTIONS()
			//	BROADCAST_PLAYER_AIRCRAFT_PURCHASED_SECTION_TINT()
				SET_REQUEST_TO_CREATE_ARMORY_AIRCRAFT_IN_DEFUNCT_BASE(TRUE)
			ELSE
				BROADCAST_PLAYER_PURCHASED_ARMORY_AIRCRAFT(FALSE)
			ENDIF
			INT iVehAccess
			iVehAccess = GET_MP_INT_CHARACTER_STAT(MP_STAT_AVENGER_ACCESS)
			IF iVehAccess = 4
				SET_AVNEGER_COCKPIT_ACCESS_IS_PASSENGERS_NO_ONE(TRUE)
			ENDIF
			IF iVehAccess = 5
				SET_AVNEGER_COCKPIT_ACCESS_IS_PASSENGERS_ONLY(TRUE)
			ENDIF	
			IF IS_HACKER_TRUCK_PURCHASED()
				BROADCAST_PLAYER_PURCHASED_HACKER_TRUCK(TRUE)
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.eHackerTruckModel = GET_HACKER_TRUCK_MODEL()
				CPRINTLN(DEBUG_INTERNET, "AM_VEHICLE_SPAWN - SCRIPT_INITIALISE - ...eHackerTruckModel = ", GET_MODEL_NAME_FOR_DEBUG(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.eHackerTruckModel))
			ELSE
				BROADCAST_PLAYER_PURCHASED_HACKER_TRUCK(FALSE)
			ENDIF
			
			iVehAccess = GET_MP_INT_CHARACTER_STAT(MP_STAT_HACKERTRUCK_ACCESS)
			IF iVehAccess = 4
				SET_HACKER_TRUCK_COCKPIT_ACCESS_IS_PASSENGERS_NO_ONE(TRUE)
			ENDIF
			IF iVehAccess = 5
				SET_HACKER_TRUCK_COCKPIT_ACCESS_IS_PASSENGERS_ONLY(TRUE)
			ENDIF
			
			#IF FEATURE_DLC_2_2022
			iVehAccess = GET_PACKED_STAT_INT(PACKED_MP_INT_ACIDLAB_ACCESS)
			IF iVehAccess = 4
				SET_ACID_LAB_ACCESS_IS_NO_ONE(TRUE)
			ENDIF
			IF iVehAccess = 5
				SET_ACID_LAB_ACCESS_IS_PASSENGERS_ONLY(TRUE)
			ENDIF
			#ENDIF
			
			IF IS_RC_BANDITO_PURCHASED()
				BROADCAST_PLAYER_RC_BANDITO_PURCHASED(TRUE)
			ENDIF
			
			#IF FEATURE_CASINO_HEIST
			IF IS_RC_TANK_PURCHASED()
				SET_PLAYER_PURCHASED_RC_TANK(TRUE)
			ENDIF
			#ENDIF
		ELSE
			IF PLAYER_ID() = GB_GET_LOCAL_PLAYER_GANG_BOSS() 
				SET_PLAYER_TRUCK_SECTIONS_FOR_CREATOR_TRAILER(GET_TRUCK_SECTION_FROM_CREATOR_SELECTION(0), GET_TRUCK_SECTION_FROM_CREATOR_SELECTION(1), GET_TRUCK_SECTION_FROM_CREATOR_SELECTION(2))
				SET_PLAYER_TRUCK_TINT_FOR_CREATOR_TRAILER(GET_TRUCK_TINT_FROM_CREATOR_SELECTION())
			ENDIF	
		ENDIF
	ENDIF	
	IF !IS_BIT_SET(iArmoryVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_STARTED)
		SET_BIT(iArmoryVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_STARTED)
		PRINTLN("AM_VEHICLE_SPAWN - INITIALISE BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_STARTED")
	ENDIF
	
	MPGlobalsAmbience.iRequestFadeOutPegasusVeh = -1
	
	#IF FEATURE_HEIST_ISLAND
	INITIALIZE_SUBMARINE_CALM_ID()	
	#ENDIF
ENDPROC


CONST_INT ciMOC_CAB 	0
CONST_INT ciMOC_TRAILER 1

PROC GRAB_MOC_ID_FOR_WANTED_LEVELS()

	// Grabbing trailer
	IF g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
	AND IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
		IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].netID_TruckV[ciMOC_TRAILER]) 
			g_viGunRunTruckTrailerRemote = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].netID_TruckV[ciMOC_TRAILER])
//			PRINTLN("GRAB_MOC_ID_FOR_WANTED_LEVELS, 1 g_viGunRunTruckTrailerRemote = ", NATIVE_TO_INT(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].netID_TruckV[ciMOC_TRAILER]))
		ENDIF
	ELSE

		VEHICLE_INDEX vehIn
		PLAYER_INDEX playerOwner
		
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			
				vehIn = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
				IF IS_VEHICLE_A_PERSONAL_TRUCK(vehIn) 
				
					playerOwner = GET_OWNER_OF_PERSONAL_TRUCK(vehIn)
					
					IF IS_NET_PLAYER_OK(playerOwner)
					
						IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(playerOwner)].netID_TruckV[ciMOC_TRAILER]) 
							g_viGunRunTruckTrailerRemote = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(playerOwner)].netID_TruckV[ciMOC_TRAILER])
//							PRINTLN("GRAB_MOC_ID_FOR_WANTED_LEVELS 2 g_viGunRunTruckTrailerRemote = ", NATIVE_TO_INT(GlobalplayerBD[NATIVE_TO_INT(playerOwner)].netID_TruckV[ciMOC_TRAILER]))
						ENDIF
					ENDIF
				ENDIF
			
			ENDIF
		ENDIF
	ENDIF

	// Grabbing cab
	IF g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
	AND IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
		IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].netID_TruckV[ciMOC_CAB]) 
			g_viGunRunTruckCabRemote = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].netID_TruckV[ciMOC_CAB])
//			PRINTLN("GRAB_MOC_ID_FOR_WANTED_LEVELS = ", NATIVE_TO_INT(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].netID_TruckV[ciMOC_CAB]))
		ENDIF
	ELSE
		VEHICLE_INDEX vehIn
		PLAYER_INDEX playerOwner
		
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			
				vehIn = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
				IF IS_VEHICLE_A_PERSONAL_TRUCK(vehIn) 
				
					playerOwner = GET_OWNER_OF_PERSONAL_TRUCK(vehIn)
					
					IF IS_NET_PLAYER_OK(playerOwner)
					
						IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(playerOwner)].netID_TruckV[ciMOC_CAB]) 
							g_viGunRunTruckCabRemote = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(playerOwner)].netID_TruckV[ciMOC_CAB])
//							PRINTLN("GRAB_MOC_ID_FOR_WANTED_LEVELS = ", NATIVE_TO_INT(GlobalplayerBD[NATIVE_TO_INT(playerOwner)].netID_TruckV[ciMOC_CAB]))
						ENDIF
					ENDIF
				ENDIF
			
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC UPDATE_AVENGER_REMOTE_VEHICLE_INDEX()
	IF g_ownerOfArmoryAircraftPropertyIAmIn != INVALID_PLAYER_INDEX()
	AND IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
		IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryAircraftPropertyIAmIn)].netID_AvengerV) 
			g_viAvengerRemoteVehIndex = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryAircraftPropertyIAmIn)].netID_AvengerV)
		ENDIF
	ELSE
		VEHICLE_INDEX vehIn
		PLAYER_INDEX playerOwner
		
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			
				vehIn = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
				IF IS_VEHICLE_A_PERSONAL_ARMORY_AIRCRAFT(vehIn) 
				
					playerOwner = GET_OWNER_OF_PERSONAL_ARMORY_AIRCRAFT(vehIn)
					
					IF IS_NET_PLAYER_OK(playerOwner)
					
						IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(playerOwner)].netID_AvengerV) 
							g_viAvengerRemoteVehIndex = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(playerOwner)].netID_AvengerV)
						ENDIF
					ENDIF
				ENDIF
			
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC UPDATE_HACKER_TRUCK_REMOTE_VEHICLE_INDEX()
	IF g_OwnerOfHackerTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
	AND IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
		IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(g_OwnerOfHackerTruckPropertyIAmIn)].netID_HackertruckV) 
			g_viHackerTruckRemoteVehIndex = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(g_OwnerOfHackerTruckPropertyIAmIn)].netID_HackertruckV)
		ENDIF
	ELSE
		VEHICLE_INDEX vehIn
		PLAYER_INDEX playerOwner
		
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			
				vehIn = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
				IF IS_VEHICLE_A_PERSONAL_HACKER_TRUCK(vehIn) 
				
					playerOwner = GET_OWNER_OF_PERSONAL_HACKER_TRUCK(vehIn)
					
					IF IS_NET_PLAYER_OK(playerOwner)
					
						IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(playerOwner)].netID_HackertruckV) 
							g_viHackerTruckRemoteVehIndex = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(playerOwner)].netID_HackertruckV)
						ENDIF
					ENDIF
				ENDIF
			
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

#IF FEATURE_DLC_2_2022
PROC UPDATE_ACID_LAB_REMOTE_VEHICLE_INDEX()
	IF g_OwnerOfAcidLabPropertyIAmIn != INVALID_PLAYER_INDEX()
	AND IS_PLAYER_IN_ACID_LAB(PLAYER_ID())
		IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(g_OwnerOfAcidLabPropertyIAmIn)].netID_AcidLabV) 
			g_viAcidLabRemoteVehIndex = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(g_OwnerOfAcidLabPropertyIAmIn)].netID_AcidLabV)
		ENDIF
	ELSE
		VEHICLE_INDEX vehIn
		PLAYER_INDEX playerOwner
		
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			
				vehIn = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
				IF IS_VEHICLE_A_PERSONAL_ACID_LAB(vehIn) 
				
					playerOwner = GET_OWNER_OF_PERSONAL_ACID_LAB(vehIn)
					
					IF IS_NET_PLAYER_OK(playerOwner)
					
						IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(playerOwner)].netID_AcidLabV) 
							g_viAcidLabRemoteVehIndex = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(playerOwner)].netID_AcidLabV)
						ENDIF
					ENDIF
				ENDIF
			
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC
#ENDIF

FUNC FLOAT GET_VECTOR_MAGNITUDE(VECTOR vVector)
	RETURN SQRT((vVector.x * vVector.x) + (vVector.y * vVector.y) + (vVector.z * vVector.z))
ENDFUNC 

PROC HANDLE_TRUCK_SOUNDS()

	IF IS_BIT_SET(iArmoryVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_LOOP)
		
		IF g_ownerOfArmoryTruckPropertyIAmIn = INVALID_PLAYER_INDEX()
			EXIT
		ENDIF
		
		IF !NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].netID_TruckV[0])
			EXIT
		ENDIF
		
		Vector offsetVelocitySound = NETWORK_GET_LAST_VEL_RECEIVED_OVER_NETWORK(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].netID_TruckV[0]))
		
		IF GET_VECTOR_MAGNITUDE(offsetVelocitySound) > 0.5
			IF outsidesoundid = -1
				outsidesoundid = GET_SOUND_ID()
				PLAY_SOUND_FROM_ENTITY(outsidesoundid, "Drive_Loop", PLAYER_PED_ID(), "DLC_GR_MOC_Sounds", TRUE, 0)
				SET_VARIABLE_ON_SOUND(outsidesoundid,"Drive_Loop", 30 )
				PRINTLN("PLAY Drive_Loop")
				PRINTLN("HANDLE_TRUCK_SOUNDS UPDATED START")
			//serverBD.iBleepSoundID = GET_NETWORK_ID_FROM_SOUND_ID(outsidesoundid)
			ELSE
				IF GET_VECTOR_MAGNITUDE(offsetVelocitySound) != GET_VECTOR_MAGNITUDE(OLDoffsetVelocity)
					OLDoffsetVelocity = offsetVelocitySound
					FLOAT soundvolume = GET_VECTOR_MAGNITUDE(offsetVelocitySound)
					soundvolume = GET_VECTOR_MAGNITUDE(offsetVelocitySound) * 5.0
					SET_VARIABLE_ON_SOUND(outsidesoundid,"Drive_Loop",soundvolume )
					PRINTLN("HANDLE_TRUCK_SOUNDS UPDATED TO ", soundvolume)
				ENDIF
			ENDIF
		ELSE
			IF outsidesoundid != (-1)
				PRINTLN("HANDLE_TRUCK_SOUNDS UPDATED STOP 2")
				STOP_SOUND(outsidesoundid)
				RELEASE_SOUND_ID(outsidesoundid)
				PRINTLN("STOP Drive_Loop")
				outsidesoundid = -1
			ENDIF
		ENDIF
		
	ELSE
		IF outsidesoundid != (-1)
			PRINTLN("HANDLE_TRUCK_SOUNDS UPDATED STOP")
			STOP_SOUND(outsidesoundid)
			RELEASE_SOUND_ID(outsidesoundid)
			PRINTLN("STOP Drive_Loop")
			outsidesoundid = -1
		ENDIF
	ENDIF
ENDPROC


PROC MAINTAIN_ARMORY_TRUCK_CAMERA_SHAKE()
	IF IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
		IF g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
			INT iRan
			IF NOT IS_GAMEPLAY_CAM_SHAKING()
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].netID_TruckV[0])
				AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].netID_TruckV[1])
				AND NOT IS_ENTITY_DEAD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].netID_TruckV[0]))
				AND NOT IS_ENTITY_DEAD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].netID_TruckV[1]))
					IF IS_VEHICLE_ATTACHED_TO_TRAILER(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].netID_TruckV[0]))
						VEHICLE_INDEX trailerVeh
						GET_VEHICLE_TRAILER_VEHICLE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].netID_TruckV[0]), trailerVeh)
						IF trailerVeh = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].netID_TruckV[1])
							IF NOT IS_ENTITY_DEAD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].netID_TruckV[0]))
								IF !GET_IS_VEHICLE_ENGINE_RUNNING(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].netID_TruckV[0]))
									IF IS_BIT_SET(iArmoryVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_STARTED)
										CLEAR_BIT(iArmoryVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_STARTED)
									ENDIF	
									IF (IS_BIT_SET(iArmoryVehicleCamBS,BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_START_DONE)
									OR IS_BIT_SET(iArmoryVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_LOOP))	
									AND (NOT IS_BIT_SET(iArmoryVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_STOP))
										SET_BIT(iArmoryVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_STOP)
										CLEAR_BIT(iArmoryVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_LOOP)
										CLEAR_BIT(iArmoryVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_STARTED)
										SHAKE_GAMEPLAY_CAM("GUNRUNNING_ENGINE_STOP_SHAKE")
										PRINTLN("MAINTAIN_ARMORY_TRUCK_CAMERA_SHAKE - start shake : GUNRUNNING_ENGINE_STOP_SHAKE")
									ENDIF
								ELSE
									IF !IS_BIT_SET(iArmoryVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_STARTED)
										SET_BIT(iArmoryVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_STARTED)
										SET_BIT(iArmoryVehicleCamBS,BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_START_DONE)
										SHAKE_GAMEPLAY_CAM("GUNRUNNING_ENGINE_START_SHAKE")
										PRINTLN("MAINTAIN_ARMORY_TRUCK_CAMERA_SHAKE - start shake : GUNRUNNING_ENGINE_START_SHAKE")
									ENDIF
									IF NOT IS_ENTITY_DEAD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].netID_TruckV[0]))
										IF !ARE_VECTORS_ALMOST_EQUAL(NETWORK_GET_LAST_VEL_RECEIVED_OVER_NETWORK(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].netID_TruckV[0])),  <<0,0,0>>)
											IF NOT IS_GAMEPLAY_CAM_SHAKING()
												iRan = GET_RANDOM_INT_IN_RANGE(0,4)
												SWITCH iRan
													CASE 0
													CASE 2
														SHAKE_GAMEPLAY_CAM("GUNRUNNING_LOOP_SHAKE")
														PRINTLN("MAINTAIN_ARMORY_TRUCK_CAMERA_SHAKE - start shake : GUNRUNNING_LOOP_SHAKE")
													BREAK
													CASE 1
														SHAKE_GAMEPLAY_CAM("GUNRUNNING_BUMP_SHAKE")
														PRINTLN("MAINTAIN_ARMORY_TRUCK_CAMERA_SHAKE - start shake : GUNRUNNING_BUMP_SHAKE")
													BREAK
												ENDSWITCH
												
												IF !IS_BIT_SET(iArmoryVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_LOOP)
													STOP_CONTROL_SHAKE(PLAYER_CONTROL)
													SET_CONTROL_SHAKE_SUPPRESSED_ID(PLAYER_CONTROL,1)
													PRINTLN("[3588562] A SET_CONTROL_SHAKE_SUPPRESSED_ID(PLAYER_CONTROL,-1) ")
													SET_BIT(iArmoryVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_LOOP)
												ENDIF
											ENDIF	
										ENDIF
									ELSE	
										PRINTLN("MAINTAIN_ARMORY_TRUCK_CAMERA_SHAKE - driverPed deosn't exist")
									ENDIF
								ENDIF
							ENDIF
						ELSE
							PRINTLN("MAINTAIN_ARMORY_TRUCK_CAMERA_SHAKE - truck is attached to diffrent trailer")
						ENDIF
					ELSE
						IF IS_BIT_SET(iArmoryVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_LOOP)
						OR IS_BIT_SET(iArmoryVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_STARTED)
						OR IS_BIT_SET(iArmoryVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_STOP)
						OR IS_BIT_SET(iArmoryVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_START_DONE)
							CLEAR_BIT(iArmoryVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_LOOP)
							CLEAR_BIT(iArmoryVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_STARTED)
							CLEAR_BIT(iArmoryVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_START_DONE)
							CLEAR_BIT(iArmoryVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_STOP)
						ENDIF	
					ENDIF
				ELSE
					PRINTLN("MAINTAIN_ARMORY_TRUCK_CAMERA_SHAKE - INVALID VEHICLE INDEX 0: ", NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].netID_TruckV[0]))
					PRINTLN("MAINTAIN_ARMORY_TRUCK_CAMERA_SHAKE - INVALID VEHICLE INDEX 1: ", NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].netID_TruckV[1]))
				ENDIF
			ELSE
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].netID_TruckV[0])
				AND NOT IS_ENTITY_DEAD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].netID_TruckV[0]))
					
					IF !GET_IS_VEHICLE_ENGINE_RUNNING(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].netID_TruckV[0]))
					OR !IS_VEHICLE_ATTACHED_TO_TRAILER(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].netID_TruckV[0]))
						#IF IS_DEBUG_BUILD
							IF (GET_FRAME_COUNT() % 120) = 0
								PRINTLN("MAINTAIN_ARMORY_TRUCK_CAMERA_SHAKE - Listening for Turn off - GET_IS_VEHICLE_ENGINE_RUNNING = FALSE ")
							ENDIF
						#ENDIF
						
						IF IS_BIT_SET(iArmoryVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_STARTED)
						OR IS_BIT_SET(iArmoryVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_LOOP)
							STOP_GAMEPLAY_CAM_SHAKING(TRUE)
							PRINTLN("MAINTAIN_ARMORY_TRUCK_CAMERA_SHAKE - BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_LOOP OR BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_STARTED active stop camera shake now")
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							IF (GET_FRAME_COUNT() % 120) = 0
								PRINTLN("MAINTAIN_ARMORY_TRUCK_CAMERA_SHAKE - Listening for Turn off - GET_IS_VEHICLE_ENGINE_RUNNING = TRUE ")
							ENDIF
						#ENDIF
						
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].netID_TruckV[0])
						AND NOT IS_ENTITY_DEAD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].netID_TruckV[0]))

							IF ARE_VECTORS_ALMOST_EQUAL(NETWORK_GET_LAST_VEL_RECEIVED_OVER_NETWORK(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].netID_TruckV[0])),  <<0,0,0>>)
								PRINTLN("MAINTAIN_ARMORY_TRUCK_CAMERA_SHAKE - truck stoped - stop camera shake now")
								STOP_GAMEPLAY_CAM_SHAKING(TRUE)
							ELSE
								#IF IS_DEBUG_BUILD
								IF (GET_FRAME_COUNT() % 120) = 0
									Vector offsetVelocity = NETWORK_GET_LAST_VEL_RECEIVED_OVER_NETWORK(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].netID_TruckV[0]))
									PRINTLN("MAINTAIN_ARMORY_TRUCK_CAMERA_SHAKE - Listening for Turn off - waiting for truck to stop - Velocity ", offsetVelocity)
								ENDIF
								#ENDIF
							ENDIF
						ENDIF	
					
					ENDIF
				ENDIF	
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(iArmoryVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_LOOP)
			CLEAR_BIT(iArmoryVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_LOOP)
			SET_CONTROL_SHAKE_SUPPRESSED_ID(PLAYER_CONTROL,-1)
			PRINTLN("[3588562] SET_CONTROL_SHAKE_SUPPRESSED_ID(PLAYER_CONTROL,-1) ")
		ENDIF
	ENDIF	
	
	// 3561576 When local player is in Mobile Operations Center trailer while a remote player is driving it around, there's very little sound indeed
	HANDLE_TRUCK_SOUNDS()
	
	
	IF IS_BIT_SET(iArmoryVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_LOOP)
		STOP_CONTROL_SHAKE(PLAYER_CONTROL)
		SET_CONTROL_SHAKE_SUPPRESSED_ID(PLAYER_CONTROL,1)
		PRINTLN("[3588562] B SET_CONTROL_SHAKE_SUPPRESSED_ID(PLAYER_CONTROL,1) ")
	ENDIF
ENDPROC

PROC HANDLE_HACKER_TRUCK_AND_SUBMARINE_SOUNDS()

	IF IS_BIT_SET(iHackerTruckVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_LOOP)
		
		PLAYER_INDEX ownerPlayerIndex
		VEHICLE_INDEX vehIndex 
		STRING sSoundName, sSoundSet, sSoundVariable
		
		IF IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
			ownerPlayerIndex = g_OwnerOfHackerTruckPropertyIAmIn
		ELIF (IS_PLAYER_IN_SUBMARINE(PLAYER_ID()) AND NOT IS_PLAYER_IN_SUBMARINE_DRIVER_SEAT())
			ownerPlayerIndex = g_OwnerOfSubmarinePropertyIAmIn
		ENDIF
		
		IF ownerPlayerIndex = INVALID_PLAYER_INDEX()
			EXIT
		ENDIF
		
		IF IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
			vehIndex = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(ownerPlayerIndex)].netID_HackertruckV)
			sSoundName = "Drive_Loop"
			sSoundSet = "DLC_GR_MOC_Sounds"
			sSoundVariable = "Drive_Loop"
		ELIF (IS_PLAYER_IN_SUBMARINE(PLAYER_ID()) AND NOT IS_PLAYER_IN_SUBMARINE_DRIVER_SEAT())
			vehIndex = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(ownerPlayerIndex)].netID_SubmarineV)
			sSoundName = "Drive_Loop"
			sSoundSet = "DLC_H4_Submarine_Motion_Sounds"
			sSoundVariable = "SubSpeed"
		ENDIF
		
		IF !DOES_ENTITY_EXIST(vehIndex)
			EXIT
		ENDIF
		
		VECTOR offsetVelocitySound = NETWORK_GET_LAST_VEL_RECEIVED_OVER_NETWORK(vehIndex)
		
		IF GET_VECTOR_MAGNITUDE(offsetVelocitySound) > 0.5
			IF hackerTruckSoundid = -1
				hackerTruckSoundid = GET_SOUND_ID()
				PLAY_SOUND_FROM_ENTITY(hackerTruckSoundid, sSoundName, PLAYER_PED_ID(), sSoundSet, TRUE, 0)
				SET_VARIABLE_ON_SOUND(hackerTruckSoundid, sSoundVariable, 30 )
				PRINTLN("PLAY Drive_Loop")
				PRINTLN("HANDLE_TRUCK_SOUNDS UPDATED START")
			ELSE
				IF GET_VECTOR_MAGNITUDE(offsetVelocitySound) != GET_VECTOR_MAGNITUDE(OLDoffsetVelocity)
					OLDoffsetVelocity = offsetVelocitySound
					FLOAT soundvolume = GET_VECTOR_MAGNITUDE(offsetVelocitySound)
					soundvolume = GET_VECTOR_MAGNITUDE(offsetVelocitySound) * 5.0
					SET_VARIABLE_ON_SOUND(hackerTruckSoundid, sSoundVariable, soundvolume)
					PRINTLN("HANDLE_HACKER_TRUCK_AND_SUBMARINE_SOUNDS UPDATED TO ", soundvolume)
				ENDIF
			ENDIF
		ELSE
			IF hackerTruckSoundid != (-1)
				PRINTLN("HANDLE_HACKER_TRUCK_AND_SUBMARINE_SOUNDS UPDATED STOP 2")
				STOP_SOUND(hackerTruckSoundid)
				RELEASE_SOUND_ID(hackerTruckSoundid)
				PRINTLN("STOP Drive_Loop")
				hackerTruckSoundid = -1
			ENDIF
		ENDIF
		
	ELSE
		IF hackerTruckSoundid != (-1)
			PRINTLN("HANDLE_HACKER_TRUCK_AND_SUBMARINE_SOUNDS UPDATED STOP")
			STOP_SOUND(hackerTruckSoundid)
			RELEASE_SOUND_ID(hackerTruckSoundid)
			PRINTLN("STOP Drive_Loop")
			hackerTruckSoundid = -1
		ENDIF
	ENDIF
ENDPROC


PROC MAINTAIN_HACKER_TRUCK_AND_SUBMARINE_CAMERA_SHAKE()
	IF IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
	OR (IS_PLAYER_IN_SUBMARINE(PLAYER_ID()) AND NOT IS_PLAYER_IN_SUBMARINE_DRIVER_SEAT())
	
		PLAYER_INDEX ownerPlayerIndex
		BOOL bOnlySetFlags = FALSE
		IF IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
			ownerPlayerIndex = g_OwnerOfHackerTruckPropertyIAmIn
		ELIF IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
			ownerPlayerIndex = g_OwnerOfSubmarinePropertyIAmIn
			bOnlySetFlags = TRUE
		ENDIF
		
		IF ownerPlayerIndex != INVALID_PLAYER_INDEX()
			INT iRan
			VEHICLE_INDEX vehIndex 
			IF IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(ownerPlayerIndex)].netID_HackertruckV)
					vehIndex = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(ownerPlayerIndex)].netID_HackertruckV)
				ENDIF	
			ELIF IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(ownerPlayerIndex)].netID_SubmarineV)
					vehIndex = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(ownerPlayerIndex)].netID_SubmarineV)
				ENDIF	
			ENDIF
			
			IF NOT IS_GAMEPLAY_CAM_SHAKING()
				IF IS_ENTITY_ALIVE(vehIndex)
					IF !GET_IS_VEHICLE_ENGINE_RUNNING(vehIndex)
						IF IS_BIT_SET(iHackerTruckVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_STARTED)
							CLEAR_BIT(iHackerTruckVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_STARTED)
						ENDIF	
						IF (IS_BIT_SET(iHackerTruckVehicleCamBS,BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_START_DONE)
						OR IS_BIT_SET(iHackerTruckVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_LOOP))	
						AND (NOT IS_BIT_SET(iHackerTruckVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_STOP))
							SET_BIT(iHackerTruckVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_STOP)
							CLEAR_BIT(iHackerTruckVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_LOOP)
							CLEAR_BIT(iHackerTruckVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_STARTED)
							IF NOT bOnlySetFlags
								SHAKE_GAMEPLAY_CAM("GUNRUNNING_ENGINE_STOP_SHAKE")	
							ENDIF	
							PRINTLN("MAINTAIN_HACKER_TRUCK_AND_SUBMARINE_CAMERA_SHAKE - start shake : GUNRUNNING_ENGINE_STOP_SHAKE")
						ENDIF
					ELSE
						IF !IS_BIT_SET(iHackerTruckVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_STARTED)
							SET_BIT(iHackerTruckVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_STARTED)
							SET_BIT(iHackerTruckVehicleCamBS,BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_START_DONE)
							IF NOT bOnlySetFlags
								SHAKE_GAMEPLAY_CAM("GUNRUNNING_ENGINE_START_SHAKE")
							ENDIF	
							PRINTLN("MAINTAIN_HACKER_TRUCK_AND_SUBMARINE_CAMERA_SHAKE - start shake : GUNRUNNING_ENGINE_START_SHAKE")
						ENDIF
						IF NOT IS_ENTITY_DEAD(vehIndex)
							IF !ARE_VECTORS_ALMOST_EQUAL(NETWORK_GET_LAST_VEL_RECEIVED_OVER_NETWORK(vehIndex),  <<0,0,0>>)
								IF NOT IS_GAMEPLAY_CAM_SHAKING()
									iRan = GET_RANDOM_INT_IN_RANGE(0,4)
									IF NOT bOnlySetFlags
										SWITCH iRan
											CASE 0
											CASE 2
												SHAKE_GAMEPLAY_CAM("GUNRUNNING_LOOP_SHAKE")
												PRINTLN("MAINTAIN_HACKER_TRUCK_AND_SUBMARINE_CAMERA_SHAKE - start shake : GUNRUNNING_LOOP_SHAKE")
											BREAK
											CASE 1
												SHAKE_GAMEPLAY_CAM("GUNRUNNING_BUMP_SHAKE")
												PRINTLN("MAINTAIN_HACKER_TRUCK_AND_SUBMARINE_CAMERA_SHAKE - start shake : GUNRUNNING_BUMP_SHAKE")
											BREAK
										ENDSWITCH
									ENDIF
									IF !IS_BIT_SET(iHackerTruckVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_LOOP)
										IF NOT bOnlySetFlags
											STOP_CONTROL_SHAKE(PLAYER_CONTROL)
											SET_CONTROL_SHAKE_SUPPRESSED_ID(PLAYER_CONTROL,1)
										ENDIF	
										PRINTLN("MAINTAIN_HACKER_TRUCK_AND_SUBMARINE_CAMERA_SHAKE SET_CONTROL_SHAKE_SUPPRESSED_ID(PLAYER_CONTROL,-1) ")
										SET_BIT(iHackerTruckVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_LOOP)
									ENDIF
								ENDIF	
							ENDIF
						ELSE	
							PRINTLN("MAINTAIN_HACKER_TRUCK_AND_SUBMARINE_CAMERA_SHAKE - driverPed deosn't exist")
						ENDIF
					ENDIF
				ELSE
					IF IS_BIT_SET(iHackerTruckVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_LOOP)
					OR IS_BIT_SET(iHackerTruckVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_STARTED)
					OR IS_BIT_SET(iHackerTruckVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_STOP)
					OR IS_BIT_SET(iHackerTruckVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_START_DONE)
						CLEAR_BIT(iHackerTruckVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_LOOP)
						CLEAR_BIT(iHackerTruckVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_STARTED)
						CLEAR_BIT(iHackerTruckVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_START_DONE)
						CLEAR_BIT(iHackerTruckVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_STOP)
					ENDIF	
				ENDIF
			ELSE
				IF IS_ENTITY_ALIVE(vehIndex)
					
					IF !GET_IS_VEHICLE_ENGINE_RUNNING(vehIndex)
						#IF IS_DEBUG_BUILD
							IF (GET_FRAME_COUNT() % 120) = 0
								PRINTLN("MAINTAIN_HACKER_TRUCK_AND_SUBMARINE_CAMERA_SHAKE - Listening for Turn off - GET_IS_VEHICLE_ENGINE_RUNNING = FALSE ")
							ENDIF
						#ENDIF
						
						IF IS_BIT_SET(iHackerTruckVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_STARTED)
						OR IS_BIT_SET(iHackerTruckVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_LOOP)
							IF NOT bOnlySetFlags
								STOP_GAMEPLAY_CAM_SHAKING(TRUE)
							ENDIF	
							PRINTLN("MAINTAIN_HACKER_TRUCK_AND_SUBMARINE_CAMERA_SHAKE - BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_LOOP OR BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_STARTED active stop camera shake now")
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							IF (GET_FRAME_COUNT() % 120) = 0
								PRINTLN("MAINTAIN_HACKER_TRUCK_AND_SUBMARINE_CAMERA_SHAKE - Listening for Turn off - GET_IS_VEHICLE_ENGINE_RUNNING = TRUE ")
							ENDIF
						#ENDIF
						
						IF IS_ENTITY_ALIVE(vehIndex)
							IF ARE_VECTORS_ALMOST_EQUAL(NETWORK_GET_LAST_VEL_RECEIVED_OVER_NETWORK(vehIndex),  <<0,0,0>>)
								PRINTLN("MAINTAIN_ARMORY_TRUCK_CAMERA_SHAKE - truck stoped - stop camera shake now")
								IF NOT bOnlySetFlags
									STOP_GAMEPLAY_CAM_SHAKING(TRUE)
								ENDIF	
							ELSE
								#IF IS_DEBUG_BUILD
								IF (GET_FRAME_COUNT() % 120) = 0
									Vector offsetVelocity = NETWORK_GET_LAST_VEL_RECEIVED_OVER_NETWORK(vehIndex)
									PRINTLN("MAINTAIN_HACKER_TRUCK_AND_SUBMARINE_CAMERA_SHAKE - Listening for Turn off - waiting for truck to stop - Velocity ", offsetVelocity)
								ENDIF
								#ENDIF
							ENDIF
						ENDIF	
					
					ENDIF
				ENDIF	
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(iHackerTruckVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_LOOP)
			CLEAR_BIT(iHackerTruckVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_LOOP)
			STOP_GAMEPLAY_CAM_SHAKING(TRUE)
			SET_CONTROL_SHAKE_SUPPRESSED_ID(PLAYER_CONTROL,-1)	
			PRINTLN("MAINTAIN_HACKER_TRUCK_AND_SUBMARINE_CAMERA_SHAKE(PLAYER_CONTROL,-1) ")				
		ENDIF
	ENDIF	
	
	HANDLE_HACKER_TRUCK_AND_SUBMARINE_SOUNDS()
	
	IF IS_BIT_SET(iHackerTruckVehicleCamBS, BS_ARMORY_TRUCK_CAM_SHAKE_ENGINE_LOOP)
		IF NOT IS_LOCAL_PLAYER_USING_DRONE()
			IF IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
				STOP_CONTROL_SHAKE(PLAYER_CONTROL)
				SET_CONTROL_SHAKE_SUPPRESSED_ID(PLAYER_CONTROL,1)
			ENDIF	
			PRINTLN("MAINTAIN_HACKER_TRUCK_AND_SUBMARINE_CAMERA_SHAKE SET_CONTROL_SHAKE_SUPPRESSED_ID(PLAYER_CONTROL,1) ")
		ELSE
			IF IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
				SET_CONTROL_SHAKE_SUPPRESSED_ID(PLAYER_CONTROL,-1)
			ENDIF	
		ENDIF
	ENDIF
ENDPROC

SCRIPT_TIMER sAvengerCamShakeTimer
PROC MAINTAIN_AVENGER_CAMERA_SHAKE()
	IF IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
	OR IS_PLAYER_IN_CREATOR_AIRCRAFT(PLAYER_ID())
		IF g_ownerOfArmoryAircraftPropertyIAmIn != INVALID_PLAYER_INDEX()
			INT iRan
			VEHICLE_INDEX avengerVeh
			IF IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryAircraftPropertyIAmIn)].netID_AvengerV)
					avengerVeh = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryAircraftPropertyIAmIn)].netID_AvengerV)
				ENDIF
			ELIF IS_PLAYER_IN_CREATOR_AIRCRAFT(PLAYER_ID())
				avengerVeh = MPGlobalsAmbience.vehCreatorAircraft
			ENDIF
			IF NOT IS_GAMEPLAY_CAM_SHAKING()
				IF DOES_ENTITY_EXIST(avengerVeh)
				AND NOT IS_ENTITY_DEAD(avengerVeh)

					IF NOT IS_ENTITY_DEAD(avengerVeh)
						IF !GET_IS_VEHICLE_ENGINE_RUNNING(avengerVeh)
							IF IS_BIT_SET(iArmoryVehicleCamBS, BS_ARMORY_AIRCRAFT_CAM_SHAKE_ENGINE_STARTED)
								CLEAR_BIT(iArmoryVehicleCamBS, BS_ARMORY_AIRCRAFT_CAM_SHAKE_ENGINE_STARTED)
							ENDIF	
							IF (IS_BIT_SET(iArmoryVehicleCamBS,BS_ARMORY_AIRCRAFT_CAM_SHAKE_ENGINE_START_DONE)
							OR IS_BIT_SET(iArmoryVehicleCamBS, BS_ARMORY_AIRCRAFT_CAM_SHAKE_ENGINE_LOOP))	
							AND (NOT IS_BIT_SET(iArmoryVehicleCamBS, BS_ARMORY_AIRCRAFT_CAM_SHAKE_ENGINE_STOP))
								SET_BIT(iArmoryVehicleCamBS, BS_ARMORY_AIRCRAFT_CAM_SHAKE_ENGINE_STOP)
								CLEAR_BIT(iArmoryVehicleCamBS, BS_ARMORY_AIRCRAFT_CAM_SHAKE_ENGINE_LOOP)
								CLEAR_BIT(iArmoryVehicleCamBS, BS_ARMORY_AIRCRAFT_CAM_SHAKE_ENGINE_STARTED)
								//SHAKE_GAMEPLAY_CAM("GUNRUNNING_ENGINE_STOP_SHAKE", 0.5)
								PRINTLN("MAINTAIN_AVENGER_CAMERA_SHAKE - start shake : GUNRUNNING_ENGINE_STOP_SHAKE")
							ENDIF
						ELSE
							IF !IS_BIT_SET(iArmoryVehicleCamBS, BS_ARMORY_AIRCRAFT_CAM_SHAKE_ENGINE_STARTED)
								SET_BIT(iArmoryVehicleCamBS, BS_ARMORY_AIRCRAFT_CAM_SHAKE_ENGINE_STARTED)
								SET_BIT(iArmoryVehicleCamBS,BS_ARMORY_AIRCRAFT_CAM_SHAKE_ENGINE_START_DONE)
								//SHAKE_GAMEPLAY_CAM("GUNRUNNING_ENGINE_START_SHAKE", 0.5)
								PRINTLN("MAINTAIN_AVENGER_CAMERA_SHAKE - start shake : GUNRUNNING_ENGINE_START_SHAKE")
							ENDIF
							IF NOT IS_ENTITY_DEAD(avengerVeh)
								IF !ARE_VECTORS_ALMOST_EQUAL(NETWORK_GET_LAST_VEL_RECEIVED_OVER_NETWORK(avengerVeh),  <<0,0,0>>)
								OR NOT IS_VEHICLE_ON_ALL_WHEELS(avengerVeh)
									IF NOT IS_GAMEPLAY_CAM_SHAKING()
										iRan = GET_RANDOM_INT_IN_RANGE(0,3)
										SWITCH iRan
											CASE 0
												SHAKE_GAMEPLAY_CAM("PLANE_PART_SPEED_SHAKE", 0.2)
												PRINTLN("MAINTAIN_AVENGER_CAMERA_SHAKE - start shake : PLANE_PART_SPEED_SHAKE 0.2")
											BREAK	
											CASE 1
												SHAKE_GAMEPLAY_CAM("HIGH_FALL_SHAKE", 0.2)
												PRINTLN("MAINTAIN_AVENGER_CAMERA_SHAKE - start shake : HIGH_FALL_SHAKE 0.2")
											BREAK		
											CASE 2
												SHAKE_GAMEPLAY_CAM("HIGH_FALL_SHAKE", 0.15)
												PRINTLN("MAINTAIN_AVENGER_CAMERA_SHAKE - start shake : HIGH_FALL_SHAKE  0.15")
											BREAK	
										ENDSWITCH
										IF !IS_BIT_SET(iArmoryVehicleCamBS, BS_ARMORY_AIRCRAFT_CAM_SHAKE_ENGINE_LOOP)
											STOP_CONTROL_SHAKE(PLAYER_CONTROL)
											SET_CONTROL_SHAKE_SUPPRESSED_ID(PLAYER_CONTROL,1)
											
											IF NOT HAS_NET_TIMER_STARTED(sAvengerCamShakeTimer)
												START_NET_TIMER(sAvengerCamShakeTimer)
											ENDIF
											SET_BIT(iArmoryVehicleCamBS, BS_ARMORY_AIRCRAFT_CAM_SHAKE_ENGINE_LOOP)
											PRINTLN("MAINTAIN_AVENGER_CAMERA_SHAKE BS_ARMORY_AIRCRAFT_CAM_SHAKE_ENGINE_LOOP true")
										ENDIF
									ENDIF	
								ENDIF
							ELSE	
								PRINTLN("MAINTAIN_AVENGER_CAMERA_SHAKE - driverPed deosn't exist")
							ENDIF
						ENDIF
					ENDIF
				ELSE
					PRINTLN("MAINTAIN_AVENGER_CAMERA_SHAKE - INVALID VEHICLE INDEX 0: ", DOES_ENTITY_EXIST(avengerVeh))
				ENDIF
			ELSE
				IF DOES_ENTITY_EXIST(avengerVeh)
				AND NOT IS_ENTITY_DEAD(avengerVeh)
					
					IF !GET_IS_VEHICLE_ENGINE_RUNNING(avengerVeh)
						#IF IS_DEBUG_BUILD
							IF (GET_FRAME_COUNT() % 120) = 0
								PRINTLN("MAINTAIN_AVENGER_CAMERA_SHAKE - Listening for Turn off - GET_IS_VEHICLE_ENGINE_RUNNING = FALSE ")
							ENDIF
						#ENDIF
						
						IF IS_BIT_SET(iArmoryVehicleCamBS, BS_ARMORY_AIRCRAFT_CAM_SHAKE_ENGINE_STARTED)
						OR IS_BIT_SET(iArmoryVehicleCamBS, BS_ARMORY_AIRCRAFT_CAM_SHAKE_ENGINE_LOOP)
							STOP_GAMEPLAY_CAM_SHAKING(TRUE)
							PRINTLN("MAINTAIN_AVENGER_CAMERA_SHAKE - BS_ARMORY_AIRCRAFT_CAM_SHAKE_ENGINE_LOOP OR BS_ARMORY_AIRCRAFT_CAM_SHAKE_ENGINE_STARTED active stop camera shake now")
						ENDIF
					ELSE
//						#IF IS_DEBUG_BUILD
//							IF (GET_FRAME_COUNT() % 120) = 0
//								PRINTLN("MAINTAIN_AVENGER_CAMERA_SHAKE - Listening for Turn off - GET_IS_VEHICLE_ENGINE_RUNNING = TRUE ")
//							ENDIF
//						#ENDIF
						
						IF HAS_NET_TIMER_STARTED(sAvengerCamShakeTimer)
							IF HAS_NET_TIMER_EXPIRED(sAvengerCamShakeTimer, 35000)
								IF IS_BIT_SET(iArmoryVehicleCamBS, BS_ARMORY_AIRCRAFT_CAM_SHAKE_ENGINE_LOOP)
									RESET_NET_TIMER(sAvengerCamShakeTimer)
									STOP_GAMEPLAY_CAM_SHAKING()
									CLEAR_BIT(iArmoryVehicleCamBS, BS_ARMORY_AIRCRAFT_CAM_SHAKE_ENGINE_LOOP)
									PRINTLN("MAINTAIN_AVENGER_CAMERA_SHAKE - sAvengerCamShakeTimer expired stop cam shake ")
								ENDIF
							ENDIF
						ENDIF
						
						IF DOES_ENTITY_EXIST(avengerVeh)
						AND NOT IS_ENTITY_DEAD(avengerVeh)

							IF ARE_VECTORS_ALMOST_EQUAL(NETWORK_GET_LAST_VEL_RECEIVED_OVER_NETWORK(avengerVeh),  <<0,0,0>>)
							AND IS_VEHICLE_ON_ALL_WHEELS(avengerVeh)
								PRINTLN("MAINTAIN_AVENGER_CAMERA_SHAKE - avenger stoped - stop camera shake now")
								STOP_GAMEPLAY_CAM_SHAKING(TRUE)
							ELSE
//								#IF IS_DEBUG_BUILD
//								IF (GET_FRAME_COUNT() % 120) = 0
//									Vector offsetVelocity = NETWORK_GET_LAST_VEL_RECEIVED_OVER_NETWORK(NET_TO_VEH(avengerVeh))
//									PRINTLN("MAINTAIN_AVENGER_CAMERA_SHAKE - Listening for Turn off - waiting for avenger to stop - Velocity ", offsetVelocity)
//								ENDIF
//								#ENDIF
							ENDIF
						ENDIF	
					
					ENDIF
				ENDIF	
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(iArmoryVehicleCamBS, BS_ARMORY_AIRCRAFT_CAM_SHAKE_ENGINE_LOOP)
		OR IS_BIT_SET(iArmoryVehicleCamBS, BS_ARMORY_AIRCRAFT_CAM_SHAKE_ENGINE_STARTED)
		OR IS_BIT_SET(iArmoryVehicleCamBS, BS_ARMORY_AIRCRAFT_CAM_SHAKE_ENGINE_STOP)
		OR IS_BIT_SET(iArmoryVehicleCamBS, BS_ARMORY_AIRCRAFT_CAM_SHAKE_ENGINE_START_DONE)
			CLEAR_BIT(iArmoryVehicleCamBS, BS_ARMORY_AIRCRAFT_CAM_SHAKE_ENGINE_LOOP)
			CLEAR_BIT(iArmoryVehicleCamBS, BS_ARMORY_AIRCRAFT_CAM_SHAKE_ENGINE_STARTED)
			CLEAR_BIT(iArmoryVehicleCamBS, BS_ARMORY_AIRCRAFT_CAM_SHAKE_ENGINE_START_DONE)
			CLEAR_BIT(iArmoryVehicleCamBS, BS_ARMORY_AIRCRAFT_CAM_SHAKE_ENGINE_STOP)
			CLEAR_BIT(iArmoryVehicleCamBS, BS_ARMORY_AIRCRAFT_CAM_SHAKE_ENGINE_LOOP)
			STOP_GAMEPLAY_CAM_SHAKING(TRUE)
			IF HAS_NET_TIMER_STARTED(sAvengerCamShakeTimer)
				RESET_NET_TIMER(sAvengerCamShakeTimer)
			ENDIF
			SET_CONTROL_SHAKE_SUPPRESSED_ID(PLAYER_CONTROL,-1)
			PRINTLN("MAINTAIN_AVENGER_CAMERA_SHAKE SET_CONTROL_SHAKE_SUPPRESSED_ID(PLAYER_CONTROL,-1) ")
		ENDIF
	ENDIF	

	IF IS_BIT_SET(iArmoryVehicleCamBS, BS_ARMORY_AIRCRAFT_CAM_SHAKE_ENGINE_LOOP)
		STOP_CONTROL_SHAKE(PLAYER_CONTROL)
		SET_CONTROL_SHAKE_SUPPRESSED_ID(PLAYER_CONTROL,1)
	ENDIF
ENDPROC


INT iAvengerSoundId = -1
BOOL bAvengerEngineRunning = FALSE
BOOL bAvengerPlayEngineTurningOffSound = FALSE
SCRIPT_TIMER scStopEngineTimer
PROC HANDLE_AVENGER_SOUNDS()
	IF IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
	OR IS_PLAYER_IN_CREATOR_AIRCRAFT(PLAYER_ID())
		VEHICLE_INDEX avengerVeh 
		IF IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
			IF (g_ownerOfArmoryAircraftPropertyIAmIn) != INVALID_PLAYER_INDEX()
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryAircraftPropertyIAmIn)].netID_AvengerV)
					avengerVeh = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryAircraftPropertyIAmIn)].netID_AvengerV)
				ENDIF	
			ELSE
				PRINTLN("HANDLE_AVENGER_SOUNDS - g_ownerOfArmoryAircraftPropertyIAmIn = INVALID_PLAYER_INDEX ")
			ENDIF
		ELSE	
			avengerVeh = MPGlobalsAmbience.vehCreatorAircraft
		ENDIF
		IF g_ownerOfArmoryAircraftPropertyIAmIn != INVALID_PLAYER_INDEX()
			//IF NOT IS_OWNERS_ARMORY_AIRCRAFT_INSIDE_DEFUNCT_BASE(g_ownerOfArmoryAircraftPropertyIAmIn)
				IF DOES_ENTITY_EXIST(avengerVeh)
				AND NOT IS_ENTITY_DEAD(avengerVeh)
					IF NOT IS_ENTITY_DEAD(avengerVeh)
						IF GET_IS_VEHICLE_ENGINE_RUNNING(avengerVeh)
						OR IS_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
							VECTOR offsetVelocitySound = NETWORK_GET_LAST_VEL_RECEIVED_OVER_NETWORK(avengerVeh)
							
							bAvengerEngineRunning = TRUE
							RESET_NET_TIMER(scStopEngineTimer)
							
							IF bAvengerPlayEngineTurningOffSound
								IF iAvengerSoundId != -1
									STOP_SOUND(iAvengerSoundId)
									RELEASE_SOUND_ID(iAvengerSoundId)
									iAvengerSoundId = -1
									PRINTLN("HANDLE_AVENGER_SOUNDS UPDATED STOP 2")
								ENDIF
								bAvengerPlayEngineTurningOffSound = FALSE
								RESET_NET_TIMER(scStopEngineTimer)
							ENDIF
							
							IF iAvengerSoundId = -1
								iAvengerSoundId = GET_SOUND_ID()
								PLAY_SOUND_FRONTEND(iAvengerSoundId, "Fly_Loop", "dlc_xm_avngr_sounds", TRUE)
								SET_VARIABLE_ON_SOUND(iAvengerSoundId,"Fly_Loop", 30)
								SET_VARIABLE_ON_SOUND(iAvengerSoundId,"avengerRotorSpeed", 100)
								PRINTLN("PLAY Drive_Loop")
								PRINTLN("HANDLE_AVENGER_SOUNDS UPDATED START")
							ELSE
								IF GET_VECTOR_MAGNITUDE(offsetVelocitySound) != GET_VECTOR_MAGNITUDE(OLDoffsetVelocity)
									OLDoffsetVelocity = offsetVelocitySound
									FLOAT soundvolume = GET_VECTOR_MAGNITUDE(offsetVelocitySound)
									soundvolume = GET_VECTOR_MAGNITUDE(offsetVelocitySound) * 5.0
									SET_VARIABLE_ON_SOUND(iAvengerSoundId,"Fly_Loop", soundvolume)
									//PRINTLN("HANDLE_AVENGER_SOUNDS UPDATED TO ", soundvolume)
								ENDIF
							ENDIF
						ELSE
							IF bAvengerEngineRunning
								SET_VARIABLE_ON_SOUND(iAvengerSoundId, "avengerRotorSpeed", 0)
								REINIT_NET_TIMER(scStopEngineTimer)
								bAvengerEngineRunning = FALSE
								bAvengerPlayEngineTurningOffSound = TRUE
							ENDIF
							
							IF HAS_NET_TIMER_EXPIRED(scStopEngineTimer, 10000)
								IF iAvengerSoundId != -1
									STOP_SOUND(iAvengerSoundId)
									RELEASE_SOUND_ID(iAvengerSoundId)
									iAvengerSoundId = -1
									PRINTLN("HANDLE_AVENGER_SOUNDS UPDATED STOP 2")
								ENDIF
								bAvengerPlayEngineTurningOffSound = FALSE
								RESET_NET_TIMER(scStopEngineTimer)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			//ENDIF	
		ENDIF
	ELSE
		IF iAvengerSoundId != -1
			STOP_SOUND(iAvengerSoundId)
			RELEASE_SOUND_ID(iAvengerSoundId)
			iAvengerSoundId = -1
			PRINTLN("HANDLE_AVENGER_SOUNDS UPDATED STOP")
		ENDIF
	ENDIF	
ENDPROC

PROC TAKE_CONTROL_OF_TRUCK()
	IF IS_PLAYER_IN_BUNKER(PLAYER_ID())	
	AND IS_ARMORY_TRUCK_UNDER_MAP(PLAYER_ID())
		IF g_ownerOfBunkerPropertyIAmIn != INVALID_PLAYER_INDEX()
			IF g_ownerOfBunkerPropertyIAmIn = PLAYER_ID()
				IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])
				AND DOES_ENTITY_EXIST(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]))
				AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]))
					IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])
						NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])
					ENDIF	
				ENDIF
				IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
				AND DOES_ENTITY_EXIST(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]))
				AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]))
					IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
						NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
					ENDIF	
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC	

PROC MAINTAIN_TRUCK_TRAILER_LOCATION()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
	
		// Cache the coords of the truck I have created
		VEHICLE_INDEX vehID = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
		IF DOES_ENTITY_EXIST(vehID)
		AND IS_VEHICLE_DRIVEABLE(vehID)
			IF GET_FRAME_COUNT() % (60*2) = 0
				VECTOR vTempCoords = GET_ENTITY_COORDS(vehID)//NETWORK_GET_LAST_ENTITY_POS_RECEIVED_OVER_NETWORK(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].netID_TruckV[1]))
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].vArmoryTruckLocation.x = vTempCoords.x
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].vArmoryTruckLocation.y = vTempCoords.y
			ENDIF
		ENDIF
		
		// Cache the owner of the truck I am currently driving so they can use my coords.
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayersTruckImIn = -1
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF DOES_ENTITY_EXIST(tempVeh)
			AND IS_VEHICLE_DRIVEABLE(tempVeh)
			AND IS_VEHICLE_ATTACHED_TO_TRAILER(tempVeh)
				VEHICLE_INDEX tempTrailer
				GET_VEHICLE_TRAILER_VEHICLE(tempVeh, tempTrailer)
				IF DOES_ENTITY_EXIST(tempTrailer)
				AND IS_VEHICLE_DRIVEABLE(tempTrailer)
				AND DECOR_EXIST_ON(tempTrailer, "Player_Truck")
					GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayersTruckImIn = DECOR_GET_INT(tempTrailer, "Player_Truck")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_HACKER_TRUCK_LOCATION()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV)
	
		// Cache the coords of the truck I have created
		VEHICLE_INDEX vehID = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV)
		IF DOES_ENTITY_EXIST(vehID)
		AND IS_VEHICLE_DRIVEABLE(vehID)
			IF GET_FRAME_COUNT() % (60*2) = 0
				VECTOR vTempCoords = GET_ENTITY_COORDS(vehID)
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].vArmoryTruckLocation.x = vTempCoords.x
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].vArmoryTruckLocation.y = vTempCoords.y
			ENDIF
		ENDIF
		
		// Cache the owner of the truck I am currently driving so they can use my coords.
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayersTruckImIn = -1
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF DOES_ENTITY_EXIST(tempVeh)
			AND IS_VEHICLE_DRIVEABLE(tempVeh)
				
				IF DECOR_EXIST_ON(tempVeh, "Player_Hacker_Truck")
					GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayersTruckImIn = DECOR_GET_INT(tempVeh, "Player_Hacker_Truck")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

#IF FEATURE_DLC_2_2022
PROC MAINTAIN_ACID_LAB_LOCATION()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV)
	
		// Cache the coords of the truck I have created
		VEHICLE_INDEX vehID = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV)
		IF DOES_ENTITY_EXIST(vehID)
		AND IS_VEHICLE_DRIVEABLE(vehID)
			IF GET_FRAME_COUNT() % (60*2) = 0
				VECTOR vTempCoords = GET_ENTITY_COORDS(vehID)
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].vAcidLabLocation.x = vTempCoords.x
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].vAcidLabLocation.y = vTempCoords.y
			ENDIF
		ENDIF
		
		// Cache the owner of the truck I am currently driving so they can use my coords.
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayersLabImIn = -1
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF DOES_ENTITY_EXIST(tempVeh)
			AND IS_VEHICLE_DRIVEABLE(tempVeh)
				
				IF DECOR_EXIST_ON(tempVeh, "Player_Acid_Lab")
					GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayersLabImIn = DECOR_GET_INT(tempVeh, "Player_Acid_Lab")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC
#ENDIF

FUNC BOOL SHOULD_ALLOW_EXPENSIVE_PROCESSING()
	IF IS_PLAYER_ON_HEIST_ISLAND(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_NIGHTCLUB(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_BUSINESS_HUB(PLAYER_ID())
		RETURN FALSE
	ENDIF	
	
	IF IS_PLAYER_IN_ARENA_GARAGE(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_CASINO(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	#IF FEATURE_CASINO_HEIST
	IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
		RETURN FALSE
	ENDIF
	#ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	IF IS_PLAYER_IN_CASINO_NIGHTCLUB(PLAYER_ID())
		RETURN FALSE
	ENDIF
	#ENDIF
	
	RETURN TRUE
ENDFUNC 
		
PROC MAINTAIN_SPAWN_TRUCK_IN_BUNKER()

	IF IS_PLAYER_GUNRUNNING_TRUCK_PURCHASED(PLAYER_ID())
	AND IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(PLAYER_ID())
	AND NOT IS_MP_SAVED_TRUCK_BEING_CLEANED_UP()
	AND IS_BIT_SET(iLocalBS, LOCAL_BS_SHOULD_ALLOW_EXPENSIVE_PROCESSING)
		//VECTOR vTruckCoord = <<834.2265, -3234.7949, -98.4865>>
		VECTOR vInitalTruckCoord 
		FLOAT fTruckHeading = 62.2800						
		
		IF IS_REQUEST_TO_CREATE_ARMORY_TRUCK_IN_BUNKER(PLAYER_ID())
			vInitalTruckCoord = <<848.867, -3236.171, -150.0>> + <<(NATIVE_TO_INT(PLAYER_ID()) * 32),0,0>>
			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])
			AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
				CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_SPAWN_TRUCK_IN_BUNKER - SPAWN_BOOKED_TRUCK_VEHICLE in  vInitalTruckCoord: ", vInitalTruckCoord)
				IF CAN_SPAWN_BOOKED_VEHICLE(eVeh_spawn_3_TRUCK)
					SET_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_3_TRUCK)
					IF SPAWN_BOOKED_TRUCK_VEHICLE(vInitalTruckCoord, fTruckHeading, FALSE, TRUE)

						SET_ARMORY_TRUCK_UNDER_MAP(TRUE)
						
						CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_SPAWN_TRUCK_IN_BUNKER - setting  truck under the map")
						SET_REQUEST_TO_CREATE_ARMORY_TRUCK_IN_BUNKER(FALSE)
						CLEAR_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_3_TRUCK)
					ENDIF
				ENDIF
			ENDIF	
		ENDIF	
	ELSE
		IF (GET_FRAME_COUNT() % 120) = 0
			CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_SPAWN_TRUCK_IN_BUNKER - ",
				PICK_STRING(IS_MP_SAVED_TRUCK_BEING_CLEANED_UP(),				"truck being cleaned up, ",			"NOT truck being cleaned up, "),
				PICK_STRING(IS_PLAYER_GUNRUNNING_TRUCK_PURCHASED(PLAYER_ID()),	"players truck purchased, ",		"NOT players truck purchased, "),
				PICK_STRING(IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(PLAYER_ID()),	"players truck is inside bunker, ",	"NOT players truck is inside bunker, "))
		ENDIF
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])
		AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
			IF NOT IS_MP_SAVED_TRUCK_BEING_CLEANED_UP()
				IF (NOT IS_BIT_SET(iLocalBS, LOCAL_BS_SHOULD_ALLOW_EXPENSIVE_PROCESSING)
				AND IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(PLAYER_ID()))
				OR !IS_SKYSWOOP_AT_GROUND()
					IF !IS_SKYSWOOP_AT_GROUND()
						CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_SPAWN_TRUCK_IN_BUNKER - sky camera is not at ground remove truck")
					ENDIF
					CLEANUP_MP_SAVED_TRUCK(FALSE, FALSE, TRUE, FALSE, TRUE)
					CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_SPAWN_TRUCK_IN_BUNKER - player is in nightclub remove truck")
				ENDIF
			ENDIF	
		ENDIF	
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS,BS_SIMPLE_INTERIOR_GLOBAL_REMOTE_STORED_TRUCK_RETURNED_TO_GARAGE)
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		AND NOT NETWORK_IS_PLAYER_IN_MP_CUTSCENE(PLAYER_ID())
		AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
		AND NOT IS_CUSTOM_MENU_ON_SCREEN()
		AND SAFE_TO_PRINT_PV_HELP()
			IF IS_PLAYER_IN_BUNKER(PLAYER_ID())
			AND (g_ownerOfBunkerPropertyIAmIn = PLAYER_ID())
				CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_SPAWN_TRUCK_IN_BUNKER - player is in his bunker, bypass MP_TRUCK_RET2")
			ELSE
				IF NOT IS_BIT_SET(iLocalBS , LOCAL_BS_CLEAN_UP_TRAILER_AFTER_DESTROYED)
					CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_SPAWN_TRUCK_IN_BUNKER - player not in his bunker, print MP_TRUCK_RET2")
					PRINT_HELP("MP_TRUCK_RET2")		//Your Mobile Operations Center has been returned to your bunker.
				ELSE
					CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_SPAWN_TRUCK_IN_BUNKER - player not in his bunker, print MP_TRUCK_DEST")
					PRINT_HELP("MP_TRUCK_DEST")		// The Mobile Operations Center has been destroyed and returned to the Bunker.
					CLEAR_BIT(iLocalBS , LOCAL_BS_CLEAN_UP_TRAILER_AFTER_DESTROYED)
				ENDIF
			ENDIF
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS,BS_SIMPLE_INTERIOR_GLOBAL_REMOTE_STORED_TRUCK_RETURNED_TO_GARAGE)	
		ENDIF
		
		// Skip this message for the renovation text
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MP_TRUCK_RETU")
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS,BS_SIMPLE_INTERIOR_GLOBAL_REMOTE_STORED_TRUCK_RETURNED_TO_GARAGE)
		ENDIF
	ENDIF
	
ENDPROC

		
PROC MAINTAIN_SPAWN_AVENGER_IN_DEFUNCT_BASE()

	IF IS_PLAYER_ARMORY_AIRCRAFT_PURCHASED(PLAYER_ID())
	AND IS_OWNERS_ARMORY_AIRCRAFT_INSIDE_DEFUNCT_BASE(PLAYER_ID())
	AND NOT IS_MP_SAVED_AVENGER_BEING_CLEANED_UP()
	AND IS_BIT_SET(iLocalBS, LOCAL_BS_SHOULD_ALLOW_EXPENSIVE_PROCESSING)
		//VECTOR vplaneCoord = <<834.2265, -3234.7949, -98.4865>>
		VECTOR vInitalAvengerCoord 
		FLOAT fplaneHeading = 62.2800						
		//CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_SPAWN_AVENGER_IN_DEFUNCT_BASE -trying to create vehicle ", vInitalAvengerCoord)
		IF IS_REQUEST_TO_CREATE_ARMORY_AIRCRAFT_IN_DEFUNCT_BASE(PLAYER_ID())
			//CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_SPAWN_AVENGER_IN_DEFUNCT_BASE -requested ", vInitalAvengerCoord)
			vInitalAvengerCoord = <<489.5733, 4785.6792, -100.0>> + <<(NATIVE_TO_INT(PLAYER_ID()) * 32),0,0>>
			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV)
				CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_SPAWN_AVENGER_IN_DEFUNCT_BASE - SPAWN_BOOKED_AIRCRAFT_VEHICLE in vInitalAvengerCoord: ", vInitalAvengerCoord)
				IF CAN_SPAWN_BOOKED_VEHICLE(eVeh_spawn_4_AIRCRAFT)
					SET_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_4_AIRCRAFT)
					IF SPAWN_BOOKED_AIRCRAFT_VEHICLE(vInitalAvengerCoord, fplaneHeading, FALSE, TRUE)
						SET_PLAYER_RENOVATED_AVENGER_TURRET(FALSE)
						SET_ARMORY_AIRCRAFT_UNDER_MAP(TRUE)
						
						CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_SPAWN_AVENGER_IN_DEFUNCT_BASE - setting  plane under the map")
						SET_REQUEST_TO_CREATE_ARMORY_AIRCRAFT_IN_DEFUNCT_BASE(FALSE)
						CLEAR_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_4_AIRCRAFT)
					ENDIF
				ENDIF
		//	ELSE
			//	CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_SPAWN_AVENGER_IN_DEFUNCT_BASE - exists ", vInitalAvengerCoord)
			ENDIF
		ENDIF	
	ELSE
		IF (GET_FRAME_COUNT() % 120) = 0
			CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_SPAWN_AVENGER_IN_DEFUNCT_BASE - ",
				PICK_STRING(IS_MP_SAVED_AVENGER_BEING_CLEANED_UP(),						"plane being cleaned up, ",			"NOT plane being cleaned up, "),
				PICK_STRING(IS_PLAYER_ARMORY_AIRCRAFT_PURCHASED(PLAYER_ID()),			"players plane purchased, ",		"NOT players plane purchased, "),
				PICK_STRING(IS_OWNERS_ARMORY_AIRCRAFT_INSIDE_DEFUNCT_BASE(PLAYER_ID()),	"players plane is inside base, ",	"NOT players plane is inside base, "))
		ENDIF
		
		IF !IS_BIT_SET(iLocalBS, LOCAL_BS_SHOULD_ALLOW_EXPENSIVE_PROCESSING)
			IF NOT IS_MP_SAVED_AVENGER_BEING_CLEANED_UP()
			AND IS_OWNERS_ARMORY_AIRCRAFT_INSIDE_DEFUNCT_BASE(PLAYER_ID())
				IF  NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV)
					CLEANUP_MP_SAVED_AVENGER(FALSE, FALSE, TRUE, FALSE, TRUE)	
					CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_SPAWN_AVENGER_IN_DEFUNCT_BASE - player is in nightclub remove avenger")
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSThree,BS_SIMPLE_INTERIOR_GLOBAL_BS_THREE_REMOTE_STORED_PLANE_RETURNED_TO_GARAGE)
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		AND NOT NETWORK_IS_PLAYER_IN_MP_CUTSCENE(PLAYER_ID())
		AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
		AND NOT IS_CUSTOM_MENU_ON_SCREEN()
		AND SAFE_TO_PRINT_PV_HELP()
			IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
			AND (g_OwnerOfBasePropertyIAmIn = PLAYER_ID())
				CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_SPAWN_AVENGER_IN_DEFUNCT_BASE - player is in his base, bypass MP_PLANE_RET2")
			ELSE
				IF NOT IS_BIT_SET(iLocalBS , LOCAL_BS_CLEAN_UP_OSPREY_AFTER_DESTROYED)
					CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_SPAWN_AVENGER_IN_DEFUNCT_BASE - player not in his base, print MP_PLANE_RET2")
					PRINT_HELP("MP_PLANE_RET2")		//Your Mobile Operations Center has been returned to your base.
				ELSE
					CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_SPAWN_AVENGER_IN_DEFUNCT_BASE - player not in his base, print MP_PLANE_DEST")
					PRINT_HELP("MP_PLANE_DEST")		// The Mobile Operations Center has been destroyed and returned to the base.
					CLEAR_BIT(iLocalBS , LOCAL_BS_CLEAN_UP_OSPREY_AFTER_DESTROYED)
				ENDIF
			ENDIF
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSThree,BS_SIMPLE_INTERIOR_GLOBAL_BS_THREE_REMOTE_STORED_PLANE_RETURNED_TO_GARAGE)	
		ENDIF
		
		// Skip this message for the renovation text
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MP_PLANE_RETU")
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSThree,BS_SIMPLE_INTERIOR_GLOBAL_BS_THREE_REMOTE_STORED_PLANE_RETURNED_TO_GARAGE)
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_SPAWN_HACKERTRUCK_IN_BUSINESS_HUB()

	IF IS_PLAYER_HACKER_TRUCK_PURCHASED(PLAYER_ID())
	AND IS_OWNERS_HACKER_TRUCK_INSIDE_BUSINESS_HUB(PLAYER_ID())
	//AND ( NOT NETWORK_IS_IN_MP_CUTSCENE() OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBitset, BS_OWNER_RENOVATED_TRUCK_1_CAB))
	AND NOT IS_MP_SAVED_HACKERTRUCK_BEING_CLEANED_UP()
		//VECTOR vTRUCK_1Coord = <<834.2265, -3234.7949, -98.4865>>
		VECTOR vInitalHackertruckCoord 
		FLOAT fHackertruckHeading = 62.2800						
		
		IF IS_REQUEST_TO_CREATE_HACKER_TRUCK_IN_BUSINESS_HUB(PLAYER_ID())
			vInitalHackertruckCoord =  <<-1508.7611, -2992.4343, -100>> + <<(NATIVE_TO_INT(PLAYER_ID()) * 32),0,0>>
			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV)
				CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_SPAWN_HACKERTRUCK_IN_BUSINESS_HUB - SPAWN_BOOKED_HACKER_TRUCK_VEHICLE in vInitalHackertruckCoord: ", vInitalHackertruckCoord)
				IF CAN_SPAWN_BOOKED_VEHICLE(eVeh_spawn_5_HACKERTRUCK)
					SET_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_5_HACKERTRUCK)
					IF SPAWN_BOOKED_HACKER_TRUCK_VEHICLE(vInitalHackertruckCoord, fHackertruckHeading, FALSE, TRUE)

						SET_HACKER_TRUCK_UNDER_MAP(TRUE)

						CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_SPAWN_HACKERTRUCK_IN_BUSINESS_HUB - setting hackertruck under the map")
						SET_REQUEST_TO_CREATE_HACKER_TRUCK_IN_BUSINESS_HUB(FALSE)
						CLEAR_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_5_HACKERTRUCK)
					ENDIF
				ENDIF
			ENDIF	
		ENDIF	
	ELSE
		IF (GET_FRAME_COUNT() % 120) = 0
			CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_SPAWN_HACKERTRUCK_IN_BUSINESS_HUB - ",
				PICK_STRING(IS_MP_SAVED_HACKERTRUCK_BEING_CLEANED_UP(),					"hackertruck being cleaned up, ",				"NOT hackertruck being cleaned up, "),
				PICK_STRING(IS_PLAYER_HACKER_TRUCK_PURCHASED(PLAYER_ID()),				"players hackertruck purchased, ",				"NOT players hackertruck purchased, "),
				PICK_STRING(IS_OWNERS_HACKER_TRUCK_INSIDE_BUSINESS_HUB(PLAYER_ID()),	"players hackertruck is inside business hub, ",	"NOT players hackertruck is inside business hub, "))
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS4, BS_SIMPLE_INTERIOR_GLOBAL_BS_FOUR_REMOTE_STORED_HACKER_TRUCK_RETURNED_TO_GARAGE)
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		AND NOT NETWORK_IS_PLAYER_IN_MP_CUTSCENE(PLAYER_ID())
		AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
		AND NOT IS_CUSTOM_MENU_ON_SCREEN()
		AND SAFE_TO_PRINT_PV_HELP()
			IF IS_PLAYER_IN_BUSINESS_HUB(PLAYER_ID())
			AND (GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = PLAYER_ID())
				CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_SPAWN_HACKERTRUCK_IN_BUSINESS_HUB - player is in his business hub, bypass MP_HTRUCK_RET2")
			ELSE
				IF NOT IS_BIT_SET(iLocalBS , LOCAL_BS_CLEAN_UP_HACKERTRUCK_AFTER_DESTROYED)
					CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_SPAWN_HACKERTRUCK_IN_BUSINESS_HUB - player not in his business hub, print MP_HTRUCK_RET2")
					PRINT_HELP("MP_HTRUCK_RET2")		//Your hackertruck has been returned to your business hub.
				ELSE
					CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_SPAWN_HACKERTRUCK_IN_BUSINESS_HUB - player not in his business hub, print MP_HTRUCK_DEST")
					PRINT_HELP("MP_HTRUCK_DEST")		// The hackertruck has been destroyed and returned to the business hub.
					CLEAR_BIT(iLocalBS , LOCAL_BS_CLEAN_UP_HACKERTRUCK_AFTER_DESTROYED)
				ENDIF
			ENDIF	
			
			MPGlobalsAmbience.bHackerTruckMovedHelp = FALSE
			MPGlobalsAmbience.bHackerTruckMovingHelp = FALSE
			
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS4, BS_SIMPLE_INTERIOR_GLOBAL_BS_FOUR_REMOTE_STORED_HACKER_TRUCK_RETURNED_TO_GARAGE)	
		ELSE
			IF IS_PLAYER_IN_BUSINESS_HUB(PLAYER_ID())
			AND (GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = PLAYER_ID())
			AND NOT IS_BIT_SET(iLocalBS , LOCAL_BS_CLEAN_UP_HACKERTRUCK_AFTER_DESTROYED)
				CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_SPAWN_HACKERTRUCK_IN_BUSINESS_HUB - player is in his business hub, bypass help MP_HTRUCK_RET2")
				CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS4, BS_SIMPLE_INTERIOR_GLOBAL_BS_FOUR_REMOTE_STORED_HACKER_TRUCK_RETURNED_TO_GARAGE)	
			ENDIF
		ENDIF
		
		// Skip this message for the renovation text
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MP_HTRUCK_RETU")
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS4, BS_SIMPLE_INTERIOR_GLOBAL_BS_FOUR_REMOTE_STORED_HACKER_TRUCK_RETURNED_TO_GARAGE)
		ENDIF
	ENDIF
	
ENDPROC

#IF FEATURE_DLC_2_2022
PROC MAINTAIN_SPAWN_ACIDLAB_IN_JUGGALO_HIDEOUT()

	IF IS_PLAYER_ACID_LAB_PURCHASED(PLAYER_ID())
	AND IS_OWNERS_ACID_LAB_INSIDE_JUGGALO_HIDEOUT(PLAYER_ID())
	//AND ( NOT NETWORK_IS_IN_MP_CUTSCENE() OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBitset, BS_OWNER_RENOVATED_TRUCK_1_CAB))
	AND NOT IS_MP_SAVED_ACIDLAB_BEING_CLEANED_UP()
		//VECTOR vTRUCK_1Coord = <<834.2265, -3234.7949, -98.4865>>
		VECTOR vInitalAcidLabCoord
		FLOAT fAcidLabHeading = GET_ACID_LAB_HIDEOUT_HEADING()
		
		IF IS_REQUEST_TO_CREATE_ACID_LAB_IN_JUGGALO_HIDEOUT(PLAYER_ID())
			vInitalAcidLabCoord = GET_ACID_LAB_HIDEOUT_SPAWN_POINT() + <<(NATIVE_TO_INT(PLAYER_ID()) * 32),0,0>>
			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV)
				CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_SPAWN_ACIDLAB_IN_JUGGALO_HIDEOUT - SPAWN_BOOKED_ACID_LAB_VEHICLE in vInitalAcidLabCoord: ", vInitalAcidLabCoord)
				IF CAN_SPAWN_BOOKED_VEHICLE(eVeh_spawn_8_ACIDLAB)
					SET_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_8_ACIDLAB)
					IF SPAWN_BOOKED_ACID_LAB_VEHICLE(vInitalAcidLabCoord, fAcidLabHeading, FALSE, TRUE)

						SET_ACID_LAB_UNDER_MAP(TRUE)

						CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_SPAWN_ACIDLAB_IN_JUGGALO_HIDEOUT - setting ACIDLAB under the map")
						SET_REQUEST_TO_CREATE_ACID_LAB_IN_JUGGALO_HIDEOUT(FALSE)
						CLEAR_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_8_ACIDLAB)
					ENDIF
				ENDIF
			ENDIF	
		ENDIF	
	ELSE
		IF (GET_FRAME_COUNT() % 120) = 0
			CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_SPAWN_ACIDLAB_IN_JUGGALO_HIDEOUT - ",
				PICK_STRING(IS_MP_SAVED_ACIDLAB_BEING_CLEANED_UP(),					"acidlab being cleaned up, ",				"NOT acidlab being cleaned up, "),
				PICK_STRING(IS_PLAYER_ACID_LAB_PURCHASED(PLAYER_ID()),				"players acidlab purchased, ",				"NOT players acidlab purchased, "),
				PICK_STRING(IS_OWNERS_ACID_LAB_INSIDE_JUGGALO_HIDEOUT(PLAYER_ID()),	"players acidlab is inside business hub, ",	"NOT players acidlab is inside hideout, "))
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSSeven, BS_SIMPLE_INTERIOR_GLOBAL_BS_SEVEN_REMOTE_STORED_ACID_LAB_RETURNED_TO_GARAGE)
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		AND NOT NETWORK_IS_PLAYER_IN_MP_CUTSCENE(PLAYER_ID())
		AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
		AND NOT IS_CUSTOM_MENU_ON_SCREEN()
		AND SAFE_TO_PRINT_PV_HELP()
			IF IS_PLAYER_IN_JUGGALO_HIDEOUT_PROPERTY(PLAYER_ID())
			AND (GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = PLAYER_ID())
				CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_SPAWN_ACIDLAB_IN_JUGGALO_HIDEOUT - player is in his hideout, bypass MP_ACDLB_RET2")
			ELSE
				IF NOT IS_BIT_SET(iLocalBS , LOCAL_BS_CLEAN_UP_ACIDLAB_AFTER_DESTROYED)
					CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_SPAWN_ACIDLAB_IN_JUGGALO_HIDEOUT - player not in his hideout, print MP_ACDLB_RET2")
					PRINT_HELP("MP_ACDLB_RET2")		// Your Acid Lab has been returned to your Juggalo Hideout. 
				ELSE
					CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_SPAWN_ACIDLAB_IN_JUGGALO_HIDEOUT - player not in his hideout, print MP_ACDLB_DEST")
					PRINT_HELP("MP_ACDLB_DEST")		// The Acid Lab has been destroyed and returned to the Juggalo Hideout.
					CLEAR_BIT(iLocalBS , LOCAL_BS_CLEAN_UP_ACIDLAB_AFTER_DESTROYED)
				ENDIF
			ENDIF	
			
			MPGlobalsAmbience.bAcidLabMovedHelp = FALSE
			MPGlobalsAmbience.bAcidLabMovingHelp = FALSE
			
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSSeven, BS_SIMPLE_INTERIOR_GLOBAL_BS_SEVEN_REMOTE_STORED_ACID_LAB_RETURNED_TO_GARAGE)	
		ELSE
			IF IS_PLAYER_IN_JUGGALO_HIDEOUT_PROPERTY(PLAYER_ID())
			AND (GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = PLAYER_ID())
			AND NOT IS_BIT_SET(iLocalBS , LOCAL_BS_CLEAN_UP_ACIDLAB_AFTER_DESTROYED)
				CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_SPAWN_ACIDLAB_IN_JUGGALO_HIDEOUT - player is in his hideput, bypass help MP_HTRUCK_RET2")
				CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSSeven, BS_SIMPLE_INTERIOR_GLOBAL_BS_SEVEN_REMOTE_STORED_ACID_LAB_RETURNED_TO_GARAGE)	
			ENDIF
		ENDIF
		
		// Skip this message for the renovation text
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MP_HTRUCK_RETU")
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSSeven, BS_SIMPLE_INTERIOR_GLOBAL_BS_SEVEN_REMOTE_STORED_ACID_LAB_RETURNED_TO_GARAGE)
		ENDIF
	ENDIF
	
ENDPROC
#ENDIF

PROC UPDATE_MAIN_TRUCK_TRAILER_MODS()
	IF IS_PLAYER_IN_BUNKER(PLAYER_ID())
	AND g_ownerOfBunkerPropertyIAmIn != INVALID_PLAYER_INDEX()
	AND PLAYER_ID() = g_ownerOfBunkerPropertyIAmIn
	AND SHOULD_UPDATE_MAIN_TRUCK_TRAILER_MODS(PLAYER_ID())
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])
		AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
			IF NOT IS_ENTITY_DEAD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]))
			AND NOT IS_ENTITY_DEAD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]))
				IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]))
				AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]))
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])		
					AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
						INT iSaveSlot
						MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(DISPLAY_SLOT_MODDED_ARMOURY_TRUCK  , iSaveSlot)
						
						IF iSaveSlot > 0 
							VEHICLE_INDEX truckIndex = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])
							MP_SAVE_VEHICLE_APPLY_SAVED_DETAILS_TO_VEHICLE(truckIndex,iSaveSlot)
						ENDIF
						
						
						
						VEHICLE_SETUP_STRUCT_MP sData
						GET_VEHICLE_SETUP_MP(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]), sData)
						SET_GR_TRAILER_SETUP_MP(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]), sData)
						SET_VEHICLE_SETUP_MP(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]), sData)
						IF !HAS_PLAYER_OWN_TRUCK_COMMAND_CENTER()
							sData.VehicleSetup.iModIndex[MOD_ROOF] = -1
						ELSE
							IF GET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_MOD_ROOF) != 2
								SET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_MOD_ROOF, 1)
								sData.VehicleSetup.iModIndex[MOD_ROOF] = 0
							ELSE
								SET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_MOD_ROOF, 2)
								sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
							ENDIF
						ENDIF
						CPRINTLN(DEBUG_NET_AMBIENT, "UPDATE_MAIN_TRUCK_TRAILER_MODS roof:  ",sData.VehicleSetup.iModIndex[MOD_ROOF])
						SET_VEHICLE_MOD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]), MOD_ROOF, sData.VehicleSetup.iModIndex[MOD_ROOF])
						
						VEHICLE_SETUP_STRUCT_MP sDataCab
						GET_VEHICLE_SETUP_MP(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]), sDataCab)
						CPRINTLN(DEBUG_NET_AMBIENT, "UPDATE_MAIN_TRUCK_TRAILER_MODS plate:  ",sData.VehicleSetup.tlPlateText)
						SET_VEHICLE_NUMBER_PLATE_TEXT(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]), sDataCab.VehicleSetup.tlPlateText)
						
						SET_UPDATE_MAIN_TRUCK_TRAILER_MODS(FALSE)
					ELSE
						NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])
						NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDIF	
ENDPROC

#IF FEATURE_DLC_2_2022
PROC UPDATE_MAIN_HIDEOUT_VEHICLES_MODS()
	IF IS_PLAYER_IN_JUGGALO_HIDEOUT_PROPERTY(PLAYER_ID())
	AND g_ownerOfPropertyIAmIn != INVALID_PLAYER_INDEX()
	AND PLAYER_ID() = g_ownerOfPropertyIAmIn
	AND SHOULD_UPDATE_MAIN_HIDEOUT_VEHICLES_MODS(PLAYER_ID())
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV)
		AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SupportBikeV)
			IF NOT IS_ENTITY_DEAD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV))
			AND NOT IS_ENTITY_DEAD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SupportBikeV))
				IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV))
				AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SupportBikeV))
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV)		
					AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SupportBikeV)
						//INT iSaveSlot
						//MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(DISPLAY_SLOT_MODDED_ARMOURY_TRUCK  , iSaveSlot)
						
						//IF iSaveSlot > 0 
						//	VEHICLE_INDEX truckIndex = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV)
						//	MP_SAVE_VEHICLE_APPLY_SAVED_DETAILS_TO_VEHICLE(truckIndex,iSaveSlot)
						//ENDIF
						
						SET_UPDATE_MAIN_TRUCK_TRAILER_MODS(FALSE)
					ELSE
						NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV)
						NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SupportBikeV)
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDIF	
ENDPROC
#ENDIF

PROC UPDATE_MAIN_AVENGER_MODS()
	IF g_ownerOfBasePropertyIAmIn != INVALID_PLAYER_INDEX()
	AND PLAYER_ID() = g_ownerOfBasePropertyIAmIn
	AND SHOULD_UPDATE_MAIN_AVENGER_MODS(PLAYER_ID())
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV)
			IF NOT IS_ENTITY_DEAD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV))
				IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV))
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV)		
						INT iSaveSlot
						MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(DISPLAY_SLOT_AVENGER_HELI  , iSaveSlot)
						
						IF iSaveSlot > 0 
							VEHICLE_INDEX avengerIndex = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV)
							MP_SAVE_VEHICLE_APPLY_SAVED_DETAILS_TO_VEHICLE(avengerIndex,iSaveSlot)
						ENDIF
						
						SET_UPDATE_MAIN_AVENGER_MODS(FALSE)
					ELSE
						NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV)
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDIF	
ENDPROC

FUNC BOOL SHOULD_PLAYER_FREEZE_TRUCK(VEHICLE_INDEX truckIndex)
	IF NOT IS_ENTITY_DEAD(truckIndex)
		IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(truckIndex)
			IF	HAS_START_TO_MOVE_PEDS_IN_MAIN_TRUCK(PLAYER_ID())
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),(truckIndex))
					IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_TRUCK_UNDER_MAP()
	IF NOT IS_ARMORY_TRUCK_UNDER_MAP(PLAYER_ID())
		EXIT
	ENDIF

	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])
		EXIT
	ENDIF
	
	VEHICLE_INDEX truckIndex = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])

	IF SHOULD_PLAYER_FREEZE_TRUCK(truckIndex)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
			VEHICLE_INDEX truckTrailerIndex = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
			IF NOT IS_ENTITY_DEAD(truckTrailerIndex)
				IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(truckIndex)
				AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(truckTrailerIndex)
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])		
					AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
						FREEZE_ENTITY_POSITION(truckIndex, TRUE)
						FREEZE_ENTITY_POSITION(truckTrailerIndex, TRUE)
					ELSE
						NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])
						NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

SCRIPT_TIMER sSafeTimerForTruckExist
PROC MAINTAIN_MOVING_OUT_OF_BUNKER()
	IF NOT NETWORK_IS_ACTIVITY_SESSION()
		IF g_ownerOfBunkerPropertyIAmIn != INVALID_PLAYER_INDEX()
		AND PLAYER_ID() = g_ownerOfBunkerPropertyIAmIn
			IF HAS_START_TO_MOVE_PEDS_IN_MAIN_TRUCK(PLAYER_ID())
				IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_RESET_ARMORY_TRUCK_VISIBILITY)
					SET_BIT(iLocalBS, LOCAL_BS_RESET_ARMORY_TRUCK_VISIBILITY)
					PRINTLN("MAINTAIN_MOVING_OUT_OF_BUNKER[ATVD] - LOCAL_BS_RESET_ARMORY_TRUCK_VISIBILITY")
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				IF GET_FRAME_COUNT() % 60 = 0
					PRINTLN("MAINTAIN_MOVING_OUT_OF_BUNKER[ATVD] - HAS_START_TO_MOVE_PEDS_IN_MAIN_TRUCK - false.")
				ENDIF
				#ENDIF
			ENDIF
			
			IF IS_BIT_SET(iLocalBS, LOCAL_BS_RESET_ARMORY_TRUCK_VISIBILITY)
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])
				AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
					IF NOT IS_ENTITY_DEAD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]))
					AND NOT IS_ENTITY_DEAD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]))
						IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]))
						AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]))
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])		
							AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
								PRINTLN("MAINTAIN_MOVING_OUT_OF_BUNKER[ATVD] - Visibiliy was reset for cab and trailer")
								SET_ENTITY_VISIBLE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]), TRUE)
								SET_ENTITY_VISIBLE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]), TRUE)
								CLEAR_BIT(iLocalBS, LOCAL_BS_RESET_ARMORY_TRUCK_VISIBILITY)
							ELSE
								PRINTLN("MAINTAIN_MOVING_OUT_OF_BUNKER[ATVD] - Doesn't have control, Cab: ", NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]),
								" Trailer: ", NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]))
								NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])
								NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
							ENDIF
						ELSE
							PRINTLN("MAINTAIN_MOVING_OUT_OF_BUNKER[ATVD] - Doesn't belong to this script, Cab: ",  DOES_ENTITY_BELONG_TO_THIS_SCRIPT(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])),
						" Trailer: ", DOES_ENTITY_BELONG_TO_THIS_SCRIPT(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])))
						ENDIF
					ELSE
						PRINTLN("MAINTAIN_MOVING_OUT_OF_BUNKER[ATVD] - Dead, Cab: ", IS_ENTITY_DEAD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])),
						" Trailer: ", IS_ENTITY_DEAD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])))
					ENDIF	
				ELSE
					PRINTLN("MAINTAIN_MOVING_OUT_OF_BUNKER[ATVD] - No Control, Cab: ", NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]),
					" Trailer: ", NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]))
					
					IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_CLEAN_UP_BEFORE_EXITING_BUNKER)
						CLEANUP_MP_SAVED_TRUCK(FALSE, FALSE, TRUE, FALSE, TRUE)
						SET_BIT(iLocalBS, LOCAL_BS_CLEAN_UP_BEFORE_EXITING_BUNKER)
						PRINTLN("MAINTAIN_MOVING_OUT_OF_BUNKER[ATVD] - driving out of bunker but truck doesn't exist cleanup!")
						RESET_NET_TIMER(sSafeTimerForTruckExist)
						START_NET_TIMER(sSafeTimerForTruckExist)
					ELSE
						IF HAS_NET_TIMER_EXPIRED(sSafeTimerForTruckExist, 20000)
							CLEAR_BIT(iLocalBS, LOCAL_BS_CLEAN_UP_BEFORE_EXITING_BUNKER)
							RESET_NET_TIMER(sSafeTimerForTruckExist)
							PRINTLN("MAINTAIN_MOVING_OUT_OF_BUNKER[ATVD] - driving out of bunker but truck doesn't exist cleanup!")
						ENDIF
					ENDIF	
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				IF GET_FRAME_COUNT() % 60 = 0
					PRINTLN("MAINTAIN_MOVING_OUT_OF_BUNKER[ATVD] - LOCAL_BS_RESET_ARMORY_TRUCK_VISIBILITY - false.")
				ENDIF
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			IF GET_FRAME_COUNT() % 60 = 0
				PRINTLN("MAINTAIN_MOVING_OUT_OF_BUNKER[ATVD] - g_ownerOfBunkerPropertyIAmIn is invalid.", NATIVE_TO_INT(g_ownerOfBunkerPropertyIAmIn))
				
				IF g_ownerOfBunkerPropertyIAmIn <> INVALID_PLAYER_INDEX()
					PRINTLN("MAINTAIN_MOVING_OUT_OF_BUNKER[ATVD] - g_ownerOfBunkerPropertyIAmIn name: ", NATIVE_TO_INT(g_ownerOfBunkerPropertyIAmIn))
				ENDIF
			ENDIF
			#ENDIF
		ENDIF	
	ENDIF	
ENDPROC 

PROC MAINTAIN_MOVING_INTO_BUNKER()
	IF NOT NETWORK_IS_ACTIVITY_SESSION()
		IF g_ownerOfBunkerPropertyIAmIn != INVALID_PLAYER_INDEX()
		AND PLAYER_ID() = g_ownerOfBunkerPropertyIAmIn
			IF NOT IS_ARMORY_TRUCK_UNDER_MAP(PLAYER_ID())
				IF HAS_START_TO_MOVE_PEDS_IN_CLONE_TRUCK(PLAYER_ID())
				AND NOT IS_SAFE_TO_START_WARP_INTO_CLONE_TRUCK(PLAYER_ID())
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])
					AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
						IF NOT IS_ENTITY_DEAD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]))
						AND NOT IS_ENTITY_DEAD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]))
							IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]))
							AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]))
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])		
								AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
									IF IS_VEHICLE_ATTACHED_TO_TRAILER(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]))
										FREEZE_ENTITY_POSITION(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]), TRUE)
										FREEZE_ENTITY_POSITION(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]), TRUE)
										SET_ENTITY_COLLISION(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]), TRUE)
										SET_ENTITY_COLLISION(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]), TRUE)
										SET_ENTITY_COORDS_NO_OFFSET(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]),<<848.867, -3236.171, -150.0>> + <<(NATIVE_TO_INT(PLAYER_ID()) * 32),0,0>>,TRUE, TRUE)
										SET_ENTITY_HEADING(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]), 62.2800)
										SET_ENTITY_VISIBLE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]), TRUE)
										SET_ARMORY_TRUCK_UNDER_MAP(TRUE)
										SET_ARMORY_TRUCK_IS_IN_BUNKER(TRUE)
										SET_PLAYER_TRUCK_IS_IN_FREEMODE(FALSE)
										SET_REQUEST_TO_CREATE_ARMORY_TRUCK_IN_BUNKER(FALSE)
										SET_SAFE_TO_START_WARP_INTO_CLONE_TRUCK(TRUE)
										CLEAR_BIT(iLocalBS, LOCAL_BS_RESET_ARMORY_TRUCK_VISIBILITY)
										CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_MOVING_INTO_BUNKER - setting  truck under the map")
									ELSE
										ATTACH_VEHICLE_TO_TRAILER(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]) , NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]))
										CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_MOVING_INTO_BUNKER - attaching truck and trailer before moving under the map")
									ENDIF
								ELSE
									NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])
									NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
								ENDIF
							ENDIF
						ENDIF	
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_PLAYER_SITTING_IN_CLONE_TRUCK(PLAYER_ID())
//				IF HAS_OWNER_ASSIGNED_TRUCK_TO_MAIN_THREAD(PLAYER_ID())
				IF IS_ARMORY_TRUCK_UNDER_MAP(PLAYER_ID())
				
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])
					AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
						IF NOT IS_ENTITY_DEAD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]))
						AND NOT IS_ENTITY_DEAD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]))
							IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]))
							AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]))
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])		
								AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
									IF NOT IS_MAIN_TRUCK_EMPTY_AFTER_WARP_INTO_BUNKER(PLAYER_ID())
										IF IS_VEHICLE_EMPTY(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]),TRUE, TRUE)
											IF IS_ENTITY_VISIBLE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]))
												SET_ENTITY_VISIBLE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]) ,FALSE)
											ENDIF
											IF IS_ENTITY_VISIBLE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]))
												SET_ENTITY_VISIBLE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]) ,FALSE)
											ENDIF
											IF NOT IS_ENTITY_VISIBLE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]))
											AND NOT IS_ENTITY_VISIBLE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]))
												SET_MAIN_TRUCK_EMPTY_AFTER_WARP_INTO_BUNKER(TRUE)
												CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_MOVING_INTO_BUNKER - setting  truck under the map, sitting in clone")
											ENDIF	
										ENDIF	
									ELSE
										IF IS_VEHICLE_EMPTY(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]),TRUE, TRUE)
											IF IS_ENTITY_VISIBLE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]))
											OR IS_ENTITY_VISIBLE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]))
												SET_MAIN_TRUCK_EMPTY_AFTER_WARP_INTO_BUNKER(FALSE)
												CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_MOVING_INTO_BUNKER - setting  truck under the map, but it is visible")
											ENDIF
										ENDIF	
									ENDIF
								ELSE
									NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])
									NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
								ENDIF
							
							ENDIF
						ENDIF	
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDIF	
	
ENDPROC

PROC MAINTAIN_MOVING_INTO_DEFUNCT_BASE()
	IF NOT NETWORK_IS_ACTIVITY_SESSION()
		IF g_ownerOfBasePropertyIAmIn != INVALID_PLAYER_INDEX()
		AND PLAYER_ID() = g_ownerOfBasePropertyIAmIn
			IF HAS_START_TO_MOVE_PEDS_IN_CLONE_AVENGER(PLAYER_ID())
				IF NOT IS_ARMORY_AIRCRAFT_UNDER_MAP(PLAYER_ID())
					IF NOT IS_SAFE_TO_START_WARP_INTO_CLONE_AVENGER(PLAYER_ID())
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV)
							IF NOT IS_ENTITY_DEAD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV))
								IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV))
									IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV)		
										//IF IS_VEHICLE_ATTACHED_TO_TRAILER(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV))
											FREEZE_ENTITY_POSITION(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV), TRUE)
											SET_ENTITY_COLLISION(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV), TRUE)
											SET_ENTITY_COORDS_NO_OFFSET(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV),<<336.9514, 4841.9663, -150>> + <<(NATIVE_TO_INT(PLAYER_ID()) * 32),0,0>>,TRUE, TRUE)
											SET_ENTITY_HEADING(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV), 62.2800)
											SET_ARMORY_AIRCRAFT_UNDER_MAP(TRUE)
											SET_ARMORY_AIRCRAFT_IS_IN_DEFUNCT_BASE(TRUE)
											SET_PLAYER_PLANE_IS_IN_FREEMODE(FALSE)
											SET_REQUEST_TO_CREATE_ARMORY_AIRCRAFT_IN_DEFUNCT_BASE(FALSE)
											SET_SAFE_TO_START_WARP_INTO_CLONE_AVENGER(TRUE)
											CLEAR_BIT(iLocalBS, LOCAL_BS_RESET_ARMORY_AIRCRAFT_VISIBILITY)
											CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_MOVING_INTO_DEFUNCT_BASE - setting avenger under the map")
										//ENDIF
									ELSE
										NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV)
										PRINTLN("MAINTAIN_MOVING_INTO_DEFUNCT_BASE failed waiting for control")
									ENDIF
								ELSE
									PRINTLN("MAINTAIN_MOVING_INTO_DEFUNCT_BASE failed entity doesn't belong to am_vehicle_spawn")
								ENDIF
							ELSE
								PRINTLN("MAINTAIN_MOVING_INTO_DEFUNCT_BASE failed entity dead")
							ENDIF	
						ELSE
							PRINTLN("MAINTAIN_MOVING_INTO_DEFUNCT_BASE failed no net ID")
						ENDIF
					ELSE
						PRINTLN("MAINTAIN_MOVING_INTO_DEFUNCT_BASE failed IS_SAFE_TO_START_WARP_INTO_CLONE_AVENGER")
					ENDIF
				ELSE
					PRINTLN("MAINTAIN_MOVING_INTO_DEFUNCT_BASE failed IS_ARMORY_AIRCRAFT_UNDER_MAP")
				ENDIF
			ENDIF
			
			IF IS_PLAYER_SITTING_IN_CLONE_AVENGER(PLAYER_ID())
//				IF HAS_OWNER_ASSIGNED_TRUCK_TO_MAIN_THREAD(PLAYER_ID())
				IF IS_ARMORY_AIRCRAFT_UNDER_MAP(PLAYER_ID())
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV)
						IF NOT IS_ENTITY_DEAD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV))
							IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV))
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV)		
									IF NOT IS_MAIN_AVENGER_EMPTY_AFTER_WARP_INTO_DEFUNCT_BASE(PLAYER_ID())
										
										IF IS_VEHICLE_EMPTY(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV),TRUE, TRUE)
											IF !IS_ENTITY_VISIBLE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV))
												SET_AVENGER_EMPTY_AFTER_WARP_INTO_DEFUNCT_BASE(TRUE)
												CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_MOVING_INTO_DEFUNCT_BASE - setting avenger under the map, sitting in clone")
											ELSE
												SET_ENTITY_VISIBLE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV) ,FALSE)
											ENDIF	
										ENDIF	
													
									ELSE
										IF IS_VEHICLE_EMPTY(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV),TRUE, TRUE)
											IF IS_ENTITY_VISIBLE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV))
												SET_AVENGER_EMPTY_AFTER_WARP_INTO_DEFUNCT_BASE(FALSE)
												CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_MOVING_INTO_DEFUNCT_BASE - setting avenger under the map but it is visible")
											ENDIF	
										ENDIF	
									ENDIF
								ELSE
									NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV)
								ENDIF
							ENDIF
						ENDIF
					ENDIF	
										
				ENDIF
			ENDIF
		ENDIF	
	ENDIF	
	
ENDPROC

PROC UPDATE_HIDE_PERSONAL_TRUCK()
	IF (g_SpawnData.MissionSpawnPersonalVehicleData.bHidePersonalVehicles)
	OR (g_b_JobIntroOpeningCut)
	OR IS_PROPERTY_SKYCAM_DOING_EXTERNAL_SHOT()
	OR IS_PROPERTY_SKYCAM()
	OR IS_BIT_SET(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_SPAWNING_IN_SIMPLE_INTERIOR)
	
		#IF IS_DEBUG_BUILD
		IF (g_SpawnData.MissionSpawnPersonalVehicleData.bHidePersonalVehicles)
			PRINTLN("[personal_vehicle] UPDATE_HIDE_PERSONAL_TRUCK - g_SpawnData.MissionSpawnPersonalVehicleData.bHidePersonalVehicles = TRUE")
		ENDIF
		IF (g_b_JobIntroOpeningCut)
			PRINTLN("[personal_vehicle] UPDATE_HIDE_PERSONAL_TRUCK - g_b_JobIntroOpeningCut = TRUE")
		ENDIF
		IF IS_PROPERTY_SKYCAM_DOING_EXTERNAL_SHOT()
			PRINTLN("[personal_vehicle] UPDATE_HIDE_PERSONAL_TRUCK - IS_PROPERTY_SKYCAM = TRUE")
		ENDIF
		#ENDIF
		
		INT i
		PLAYER_INDEX PlayerID
		VEHICLE_INDEX VehicleID
		
		REPEAT NUM_NETWORK_PLAYERS i
			IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[i].netID_TruckV[1])
			AND DOES_ENTITY_EXIST(NET_TO_VEH(GlobalplayerBD[i].netID_TruckV[1]))
				SET_ENTITY_LOCALLY_INVISIBLE(NET_TO_VEH(GlobalplayerBD[i].netID_TruckV[1]))
				PRINTLN("[personal_vehicle] UPDATE_HIDE_PERSONAL_TRUCK - hiding trailer locally for player ", i)		
			ENDIF
			IF DOES_ENTITY_EXIST(MPGlobals.RemoteTruckV[i][1])
				SET_ENTITY_LOCALLY_INVISIBLE(MPGlobals.RemoteTruckV[i][1])
				PRINTLN("[personal_vehicle] UPDATE_HIDE_PERSONAL_TRUCK - hiding trailer RemotePV locally for player ", i)
			ENDIF
			
			IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[i].netID_TruckV[0])
			AND DOES_ENTITY_EXIST(NET_TO_VEH(GlobalplayerBD[i].netID_TruckV[0]))
				SET_ENTITY_LOCALLY_INVISIBLE(NET_TO_VEH(GlobalplayerBD[i].netID_TruckV[0]))
				PRINTLN("[personal_vehicle] UPDATE_HIDE_PERSONAL_TRUCK - hiding locally truck for player ", i)		
			ENDIF
			IF DOES_ENTITY_EXIST(MPGlobals.RemoteTruckV[i][0])
				SET_ENTITY_LOCALLY_INVISIBLE(MPGlobals.RemoteTruckV[i][0])
				PRINTLN("[personal_vehicle] UPDATE_HIDE_PERSONAL_TRUCK - hiding RemotePV truck locally for player ", i)
			ENDIF
			
			// if the player is invisible then make sure his vehicle is too.
			IF IS_PROPERTY_SKYCAM_DOING_EXTERNAL_SHOT()
			OR IS_PROPERTY_SKYCAM()
				PlayerID = INT_TO_NATIVE(PLAYER_INDEX, i)		
				IF NOT (PlayerID = PLAYER_ID())
					IF IS_NET_PLAYER_OK(PlayerID, FALSE)
						IF DOES_ENTITY_EXIST(GET_PLAYER_PED(PlayerID))
							IF NOT IS_ENTITY_DEAD(GET_PLAYER_PED(PlayerID))
								IF NOT IS_ENTITY_VISIBLE(GET_PLAYER_PED(PlayerID))
									IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(PlayerID))
										VehicleID = GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(PlayerID))
										IF DOES_ENTITY_EXIST(VehicleID)
											SET_ENTITY_LOCALLY_INVISIBLE(VehicleID)	
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF			
			
		ENDREPEAT	
	ENDIF
	
ENDPROC

PROC SLOW_DOWN_TRUCK_TO_BUNKER_ROOM()
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_SLOW_DOWN_TRUCK_IN_BUNKER)
			IF IS_PLAYER_IN_BUNKER(PLAYER_ID())
			AND g_ownerOfBunkerPropertyIAmIn = PLAYER_ID()
				IF IS_PLAYER_GUNRUNNING_TRUCK_PURCHASED(PLAYER_ID())
				AND IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(PLAYER_ID())
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])
					AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
						IF NOT IS_ENTITY_DEAD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]))
						AND NOT IS_ENTITY_DEAD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]))
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])
							AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
								MODIFY_VEHICLE_TOP_SPEED(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]), -85.0)
								
								VEHICLE_SET_OVERRIDE_EXTENABLE_SIDE_RATIO(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]), TRUE)
								VEHICLE_SET_EXTENABLE_SIDE_TARGET_RATIO(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]), 1.0)
								
								SET_BIT(iLocalBS, LOCAL_BS_SLOW_DOWN_TRUCK_IN_BUNKER)
								PRINTLN("SLOW_DOWN_TRUCK_TO_BUNKER_ROOM LOCAL_BS_SLOW_DOWN_TRUCK_IN_BUNKER TRUE")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_PLAYER_IN_BUNKER(PLAYER_ID())
			OR g_ownerOfBunkerPropertyIAmIn != PLAYER_ID()
				IF IS_PLAYER_GUNRUNNING_TRUCK_PURCHASED(PLAYER_ID())
				AND NOT IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(PLAYER_ID())
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])
					AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
						IF NOT IS_ENTITY_DEAD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]))
						AND NOT IS_ENTITY_DEAD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]))
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])
							AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
								MODIFY_VEHICLE_TOP_SPEED(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]), 0)
								
								VEHICLE_SET_EXTENABLE_SIDE_TARGET_RATIO(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]), 0.0)
								VEHICLE_SET_OVERRIDE_EXTENABLE_SIDE_RATIO(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]), FALSE)
								
								CLEAR_BIT(iLocalBS, LOCAL_BS_SLOW_DOWN_TRUCK_IN_BUNKER)
								PRINTLN("SLOW_DOWN_TRUCK_TO_BUNKER_ROOM LOCAL_BS_SLOW_DOWN_TRUCK_IN_BUNKER FALSE")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC DONT_ALLOW_TRUCK_TO_MOVE_WHEN_OWNER_IS_ENTERING_WITH_VEHICLE()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX truckIndex = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			PLAYER_INDEX ownerPlayer
			IF IS_VEHICLE_A_PERSONAL_TRUCK(truckIndex)
				ownerPlayer = GET_OWNER_OF_PERSONAL_TRUCK(truckIndex)
				IF IS_NET_PLAYER_OK(ownerPlayer)
				AND ownerPlayer != PLAYER_ID()
					IF !IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(ownerPlayer)
						IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(ownerPlayer)].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_GLOBAL_PLAYER_BD_DRIVER_ENTERING)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
							PRINTLN("DONT_ALLOW_TRUCK_TO_MOVE_WHEN_OWNER_IS_ENTERING_WITH_VEHICLE - called")
							IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
							OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
							OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
							OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
							OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
							OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
							OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
								IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
								AND NOT IS_HELP_MESSAGE_ON_SCREEN()
									PRINT_HELP("GR_TRUCK_CAN_E")
								ENDIF
							ENDIF
						ELSE
							IF IS_HELP_MESSAGE_BEING_DISPLAYED()
								IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GR_TRUCK_CAN_E")
									CLEAR_HELP()
								ENDIF
							ENDIF	
						ENDIF
					ELSE
						IF IS_HELP_MESSAGE_BEING_DISPLAYED()
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GR_TRUCK_CAN_E")
								CLEAR_HELP()
							ENDIF
						ENDIF	
					ENDIF
				ENDIF	
			ENDIF
		ELSE
			IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GR_TRUCK_CAN_E")
					CLEAR_HELP()
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
ENDPROC

PROC KICK_PLAYERS_OUT_TRUCK_CAB()
	IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehTruckVehicle[0])
		IF IS_VEHICLE_DRIVEABLE(MPGlobalsAmbience.vehTruckVehicle[0])
			INT i
			REPEAT (GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(MPGlobalsAmbience.vehTruckVehicle[0]) + 1) i
				PED_INDEX thisPassenger
				thisPassenger = GET_PED_IN_VEHICLE_SEAT(MPGlobalsAmbience.vehTruckVehicle[0], INT_TO_ENUM(VEHICLE_SEAT, i - 1))
				
				IF thisPassenger != NULL
					IF NOT IS_PED_INJURED(thisPassenger)
						IF IS_PED_A_PLAYER(thisPassenger)
							BROADCAST_LEAVE_VEHICLE(SPECIFIC_PLAYER(NETWORK_GET_PLAYER_INDEX_FROM_PED(thisPassenger)), FALSE, 0, 0, FALSE, TRUE)
							CPRINTLN(DEBUG_AMBIENT, "KICK_PLAYERS_OUT_TRUCK_CAB - BROADCAST_LEAVE_VEHICLE to ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(thisPassenger)))
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ELSE
		CWARNINGLN(DEBUG_AMBIENT, "KICK_PLAYERS_OUT_TRUCK_CAB: DOES_ENTITY_EXIST = FALSE")
	ENDIF
ENDPROC


SCRIPT_TIMER sTruckCabFadeInTimer

/// PURPOSE:
///    If anyone got the event then leave any vehicle and fade out
PROC MAINTAIN_TRUCK_CAB_RENOVATE()
	IF NOT DO_I_NEED_TO_RENOVATE_TRUCK_CAB(PLAYER_ID())
		EXIT
	ENDIF
	
	IF (g_ownerOfBunkerPropertyIAmIn != INVALID_PLAYER_INDEX()
	AND NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfBunkerPropertyIAmIn)].SimpleInteriorBD.iBSTwo,  BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_EMPTY_ARMORY_TRUCK_TRAILER))
	AND NOT IS_LOCAL_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR()
		IF IS_PLAYER_IN_BUNKER(PLAYER_ID())
		AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
			IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBitset, BS_BUNKER_FADE_OUT_FOR_TRUCK_CAB_RENOVATE)
			AND NOT IS_WARNING_MESSAGE_ACTIVE()
				IF NOT IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_OUT(500)
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					PRINTLN("MAINTAIN_TRUCK_CAB_RENOVATE - fade out screen")						
				ENDIF
				
				IF g_SimpleInteriorData.bShouldExitMenuBeVisible 
					g_SimpleInteriorData.bShouldExitMenuBeVisible = FALSE
					PRINTLN("MAINTAIN_TRUCK_CAB_RENOVATE - set g_SimpleInteriorData.bShouldExitMenuBeVisible = FALSE")	
				ENDIF
				
				RESET_NET_TIMER(sTruckCabFadeInTimer)
				
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
				ELSE
					IF IS_BROWSER_OPEN()
						CLOSE_WEB_BROWSER()
					ENDIF
					
					IF IS_SCREEN_FADED_OUT()
						SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBitset, BS_BUNKER_FADE_OUT_FOR_TRUCK_CAB_RENOVATE)
						PRINTLN("MAINTAIN_TRUCK_CAB_RENOVATE - BS_BUNKER_FADE_OUT_FOR_TRUCK_CAB_RENOVATE true")
					ENDIF
				ENDIF
			ELSE
				IF g_ownerOfBunkerPropertyIAmIn != INVALID_PLAYER_INDEX()
					IF  NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfBunkerPropertyIAmIn)].netID_TruckV[0])
					AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfBunkerPropertyIAmIn)].netID_TruckV[1])
						IF  NOT IS_ENTITY_DEAD(NET_TO_ENT(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfBunkerPropertyIAmIn)].netID_TruckV[0]))
						AND NOT IS_ENTITY_DEAD(NET_TO_ENT(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfBunkerPropertyIAmIn)].netID_TruckV[1]))
							IF  NOT NETWORK_IS_ENTITY_FADING(NET_TO_ENT(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfBunkerPropertyIAmIn)].netID_TruckV[0]))	
							AND NOT NETWORK_IS_ENTITY_FADING(NET_TO_ENT(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfBunkerPropertyIAmIn)].netID_TruckV[1]))	
								IF NOT HAS_NET_TIMER_STARTED(sTruckCabFadeInTimer)
									START_NET_TIMER(sTruckCabFadeInTimer)
								ELSE
									IF HAS_NET_TIMER_EXPIRED(sTruckCabFadeInTimer,2500)
										DO_SCREEN_FADE_IN(500)
										NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
										CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBitset, BS_OWNER_RENOVATED_TRUCK_CAB)
										CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBitset, BS_BUNKER_FADE_OUT_FOR_TRUCK_CAB_RENOVATE)
										CLEAR_BIT(iLocalBS, LOCAL_BS_I_AM_OWNER_OF_TRUCK_RENOVATING_CAB)
										CLEAR_BIT(iLocalBS, LOCAL_BS_FORCE_TRUCK_TO_ROOM)
										SET_I_NEED_TO_RENOVATE_TRUCK_CAB(FALSE)
										RESET_NET_TIMER(sTruckCabFadeInTimer)
										PRINTLN("MAINTAIN_TRUCK_CAB_RENOVATE - fade in screen")
									ENDIF
								ENDIF	
							ELSE
								PRINTLN("MAINTAIN_TRUCK_CAB_RENOVATE - TRUCK FADING")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(iLocalBS, LOCAL_BS_I_AM_OWNER_OF_TRUCK_RENOVATING_CAB)
			AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
				IF  NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])
				AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
					IF  NOT IS_ENTITY_DEAD(NET_TO_ENT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]))
					AND NOT IS_ENTITY_DEAD(NET_TO_ENT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]))
						IF  NOT NETWORK_IS_ENTITY_FADING(NET_TO_ENT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]))	
						AND NOT NETWORK_IS_ENTITY_FADING(NET_TO_ENT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]))	
							IF NOT HAS_NET_TIMER_STARTED(sTruckCabFadeInTimer)
								START_NET_TIMER(sTruckCabFadeInTimer)
							ELSE
								IF HAS_NET_TIMER_EXPIRED(sTruckCabFadeInTimer,5000)
									CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBitset, BS_OWNER_RENOVATED_TRUCK_CAB)
									CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBitset, BS_BUNKER_FADE_OUT_FOR_TRUCK_CAB_RENOVATE)
									SET_I_NEED_TO_RENOVATE_TRUCK_CAB(FALSE)
									CLEAR_BIT(iLocalBS, LOCAL_BS_I_AM_OWNER_OF_TRUCK_RENOVATING_CAB)
									CLEAR_BIT(iLocalBS, LOCAL_BS_FORCE_TRUCK_TO_ROOM)
									RESET_NET_TIMER(sTruckCabFadeInTimer)
									PRINTLN("MAINTAIN_TRUCK_CAB_RENOVATE - owner is not in bunker create truck ")
								ENDIF
							ENDIF	
						ELSE
							PRINTLN("MAINTAIN_TRUCK_CAB_RENOVATE - owner is not TRUCK FADING")
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
					SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBitset, BS_BUNKER_FADE_OUT_FOR_TRUCK_CAB_RENOVATE)
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
ENDPROC 

SCRIPT_TIMER sTruckCabFadeOutTimer

/// PURPOSE:
///    Owner checks if everyone inside the bunker faded out and cleans up the truck 
PROC OWNER_RENOVATE_CAB_INSIDE_THE_BUNKER()
	IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_I_AM_OWNER_OF_TRUCK_RENOVATING_CAB)
		EXIT
	ENDIF
	
	IF IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(PLAYER_ID())
		IF DO_I_NEED_TO_RENOVATE_TRUCK_CAB(PLAYER_ID())
			IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehTruckVehicle[0])
			AND IS_VEHICLE_DRIVEABLE(MPGlobalsAmbience.vehTruckVehicle[0])
				IF GET_VEHICLE_NUMBER_OF_PASSENGERS(MPGlobalsAmbience.vehTruckVehicle[0]) = 0
				AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), MPGlobalsAmbience.vehTruckVehicle[0], TRUE)
					IF IS_PLAYER_IN_BUNKER(PLAYER_ID())
					AND IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBitset, BS_BUNKER_FADE_OUT_FOR_TRUCK_CAB_RENOVATE)
					AND NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBitset, BS_OWNER_RENOVATED_TRUCK_CAB)
					AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
					AND NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSTwo,  BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_EMPTY_ARMORY_TRUCK_TRAILER)
						SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBitset, BS_OWNER_RENOVATED_TRUCK_CAB)
						CLEANUP_MP_SAVED_TRUCK(DEFAULT , DEFAULT, TRUE, DEFAULT, TRUE)
						
						SET_REQUEST_TO_CREATE_ARMORY_TRUCK_IN_BUNKER(TRUE)
						PRINTLN("OWNER_RENOVATE_CAB_INSIDE_THE_BUNKER - player in bunker and faded out cleaning up truck")
					ELIF NOT IS_PLAYER_IN_BUNKER(PLAYER_ID())
					AND NOT IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
					AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
					AND NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSTwo,  BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_EMPTY_ARMORY_TRUCK_TRAILER)
						IF NOT HAS_NET_TIMER_STARTED(sTruckCabFadeOutTimer)
							START_NET_TIMER(sTruckCabFadeOutTimer)
						ELSE
							IF HAS_NET_TIMER_EXPIRED(sTruckCabFadeOutTimer,1500)		// wait for players who are inside the bunker to fade out
							AND NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBitset, BS_OWNER_RENOVATED_TRUCK_CAB)
								SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBitset, BS_OWNER_RENOVATED_TRUCK_CAB)
								CLEANUP_MP_SAVED_TRUCK(DEFAULT , DEFAULT, TRUE, DEFAULT, TRUE)
								RESET_NET_TIMER(sTruckCabFadeOutTimer)
								
								IF g_SimpleInteriorData.bShouldExitMenuBeVisible 
									g_SimpleInteriorData.bShouldExitMenuBeVisible = FALSE
									PRINTLN("OWNER_RENOVATE_CAB_INSIDE_THE_BUNKER - set g_SimpleInteriorData.bShouldExitMenuBeVisible = FALSE")	
								ENDIF
								
								SET_I_NEED_TO_RENOVATE_TRUCK_CAB(FALSE)
								SET_REQUEST_TO_CREATE_ARMORY_TRUCK_IN_BUNKER(TRUE)
								PRINTLN("OWNER_RENOVATE_CAB_INSIDE_THE_BUNKER - player not in bunker cleaning up truck")
							ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						IF NOT IS_PLAYER_IN_BUNKER(PLAYER_ID())
							PRINTLN("OWNER_RENOVATE_CAB_INSIDE_THE_BUNKER - IS_PLAYER_IN_BUNKER(PLAYER_ID()) - FALSE")
						ENDIF
						
						IF IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
							PRINTLN("OWNER_RENOVATE_CAB_INSIDE_THE_BUNKER - IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID()) - TRUE")
						ENDIF
						
						IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBitset, BS_BUNKER_FADE_OUT_FOR_TRUCK_CAB_RENOVATE)
							PRINTLN("OWNER_RENOVATE_CAB_INSIDE_THE_BUNKER - BS_BUNKER_FADE_OUT_FOR_TRUCK_CAB_RENOVATE - NOT SET")
						ENDIF
						
						IF IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBitset, BS_OWNER_RENOVATED_TRUCK_CAB)
							PRINTLN("OWNER_RENOVATE_CAB_INSIDE_THE_BUNKER - BS_OWNER_RENOVATED_TRUCK_CAB - SET")
						ENDIF
						
						IF IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
							PRINTLN("OWNER_RENOVATE_CAB_INSIDE_THE_BUNKER - IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR - TRUE")
						ENDIF
						#ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					IF GET_VEHICLE_NUMBER_OF_PASSENGERS(MPGlobalsAmbience.vehTruckVehicle[0]) != 0
						PRINTLN("OWNER_RENOVATE_CAB_INSIDE_THE_BUNKER - GET_VEHICLE_NUMBER_OF_PASSENGERS - NOT 0")
					ENDIF
					
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), MPGlobalsAmbience.vehTruckVehicle[0], TRUE)
						PRINTLN("OWNER_RENOVATE_CAB_INSIDE_THE_BUNKER - IS_PED_IN_VEHICLE - TRUE")
					ENDIF
					#ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				IF NOT DOES_ENTITY_EXIST(MPGlobalsAmbience.vehTruckVehicle[0])
					PRINTLN("OWNER_RENOVATE_CAB_INSIDE_THE_BUNKER - MPGlobalsAmbience.vehTruckVehicle[0] - DOES NOT EXIST")
				ENDIF
				
				IF NOT IS_VEHICLE_DRIVEABLE(MPGlobalsAmbience.vehTruckVehicle[0])
					PRINTLN("OWNER_RENOVATE_CAB_INSIDE_THE_BUNKER - IS_VEHICLE_DRIVEABLE - FALSE")
				ENDIF
				#ENDIF
			ENDIF
		ENDIF
	ENDIF	
ENDPROC

PROC MAINTAIN_TRUCK_TRAILER_RENOVATE()
	IF NOT DO_I_NEED_TO_RENOVATE_TRUCK_TRAILER(PLAYER_ID())
		EXIT
	ENDIF
	
	IF NOT IS_VEHICLE_EXIST_IN_TRUCK_CAR_MOD_SECTION(PLAYER_ID())
		IF NOT IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
		AND NOT IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
			IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_CHANGE_TRAILER_TURRET_AFTER_RENOVATE)
				SET_BIT(iLocalBS, LOCAL_BS_CHANGE_TRAILER_TURRET_AFTER_RENOVATE)
				SET_UPDATE_CLONE_TRUCK_TURRETS(TRUE)
				PRINTLN("MAINTAIN_TRAILER_TURRETS_RENOVATE LOCAL_BS_CHANGE_TRAILER_TURRET_AFTER_RENOVATE TRUE")
			ENDIF
			SET_OWNER_RENOVATED_TRUCK_TRAILER(FALSE)	
			SET_I_NEED_TO_RENOVATE_TRUCK_TRAILER(FALSE)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_TRAILER_TURRETS_RENOVATE()
	IF IS_BIT_SET(iLocalBS, LOCAL_BS_CHANGE_TRAILER_TURRET_AFTER_RENOVATE)
		IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehTruckVehicle[1])
		AND IS_VEHICLE_DRIVEABLE(MPGlobalsAmbience.vehTruckVehicle[1])
			IF NETWORK_HAS_CONTROL_OF_ENTITY(MPGlobalsAmbience.vehTruckVehicle[1])
				INT iModSlot
				
				IF !HAS_PLAYER_OWN_TRUCK_COMMAND_CENTER()
					iModSlot = -1
				ELSE
					IF GET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_MOD_ROOF) != 2
						SET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_MOD_ROOF, 1)
						iModSlot = 0
					ELSE
						SET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_MOD_ROOF, 2)
						iModSlot = 1
					ENDIF
				ENDIF
				
				SET_VEHICLE_MOD(MPGlobalsAmbience.vehTruckVehicle[1], MOD_ROOF,iModSlot)
				CLEAR_BIT(iLocalBS, LOCAL_BS_CHANGE_TRAILER_TURRET_AFTER_RENOVATE)
				PRINTLN("MAINTAIN_TRAILER_TURRETS_RENOVATE LOCAL_BS_CHANGE_TRAILER_TURRET_AFTER_RENOVATE FALSE iModSlot : ", iModSlot)
			ELSE
				NETWORK_REQUEST_CONTROL_OF_ENTITY(MPGlobalsAmbience.vehTruckVehicle[1])
			ENDIF
		ENDIF
	ENDIF
ENDPROC

SCRIPT_TIMER sTruckCabRenovateKickPlayers
PROC MAINTAIN_TRUCK_RENOVATION()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(PLAYER_ID())
			IF IS_OWNER_RENOVATED_TRUCK_CAB(PLAYER_ID())
			AND IS_OWNER_RENOVATED_TRUCK_TRAILER(PLAYER_ID())
				BROADCAST_OWNER_RENOVATED_TRUCK(TRUE)
				SET_OWNER_RENOVATED_TRUCK_CAB(FALSE)
				SET_OWNER_RENOVATED_TRUCK_TRAILER(FALSE)
				IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_I_AM_OWNER_OF_TRUCK_RENOVATING_CAB)
					SET_BIT(iLocalBS, LOCAL_BS_I_AM_OWNER_OF_TRUCK_RENOVATING_CAB)
				ENDIF
				IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSTwo,  BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_EMPTY_ARMORY_TRUCK_TRAILER)
					SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSTwo,  BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_RENOVATED_MOC)
					SET_EMPTY_ARMORY_TRUCK_TRAILER(TRUE)
					PRINTLN("MAINTAIN_TRUCK_RENOVATION 1 BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_EMPTY_ARMORY_TRUCK_TRAILER TRUE")
				ENDIF
			ELIF IS_OWNER_RENOVATED_TRUCK_TRAILER(PLAYER_ID())
			AND NOT IS_OWNER_RENOVATED_TRUCK_CAB(PLAYER_ID())
				BROADCAST_OWNER_RENOVATED_TRUCK()
				SET_OWNER_RENOVATED_TRUCK_TRAILER(FALSE)
				IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSTwo,  BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_EMPTY_ARMORY_TRUCK_TRAILER)
					SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSTwo,  BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_RENOVATED_MOC)
					SET_EMPTY_ARMORY_TRUCK_TRAILER(TRUE)
					PRINTLN("MAINTAIN_TRUCK_RENOVATION 2 BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_EMPTY_ARMORY_TRUCK_TRAILER TRUE")
				ENDIF
			ENDIF
		ELSE
			IF IS_OWNER_RENOVATED_TRUCK_CAB(PLAYER_ID())
				CLOSE_WEB_BROWSER()
				IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSTwo,  BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_EMPTY_ARMORY_TRUCK_TRAILER)
					SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSTwo,  BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_RENOVATED_MOC)
					SET_EMPTY_ARMORY_TRUCK_TRAILER(TRUE)
					PRINTLN("MAINTAIN_TRUCK_RENOVATION 3 BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_EMPTY_ARMORY_TRUCK_TRAILER TRUE")
				ENDIF
				KICK_PLAYERS_OUT_TRUCK_CAB()
				IF NOT IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
				AND GET_VEHICLE_NUMBER_OF_PASSENGERS(MPGlobalsAmbience.vehTruckVehicle[0]) = 0
				AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), MPGlobalsAmbience.vehTruckVehicle[0], TRUE)
				AND NOT IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
					SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(MPGlobalsAmbience.vehTruckVehicle[0], TRUE)
					IF NOT HAS_NET_TIMER_STARTED(sTruckCabRenovateKickPlayers)
						START_NET_TIMER(sTruckCabRenovateKickPlayers)
					ELSE
						IF HAS_NET_TIMER_EXPIRED(sTruckCabRenovateKickPlayers, 5000)
						AND NOT IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
							IF IS_HELP_MESSAGE_BEING_DISPLAYED()
							OR IS_HELP_MESSAGE_ON_SCREEN()	
								CLEAR_HELP()
							ENDIF	
							PRINT_HELP("MP_TRUCK_RETU")
							CLEANUP_MP_SAVED_TRUCK(DEFAULT , DEFAULT, TRUE, DEFAULT, TRUE)
							SET_REQUEST_TO_CREATE_ARMORY_TRUCK_IN_BUNKER(TRUE)
							SET_OWNER_RENOVATED_TRUCK_CAB(FALSE)
							RESET_NET_TIMER(sTruckCabRenovateKickPlayers)
						ENDIF
					ENDIF	
					IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_I_AM_OWNER_OF_TRUCK_RENOVATING_CAB)
						SET_BIT(iLocalBS, LOCAL_BS_I_AM_OWNER_OF_TRUCK_RENOVATING_CAB)
					ENDIF
				ENDIF	
			ELIF IS_OWNER_RENOVATED_TRUCK_TRAILER(PLAYER_ID())
			AND NOT IS_OWNER_RENOVATED_TRUCK_CAB(PLAYER_ID())
				BROADCAST_OWNER_RENOVATED_TRUCK()
				SET_OWNER_RENOVATED_TRUCK_TRAILER(FALSE)
				IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSTwo,  BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_EMPTY_ARMORY_TRUCK_TRAILER)
					SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSTwo,  BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_RENOVATED_MOC)
					SET_EMPTY_ARMORY_TRUCK_TRAILER(TRUE)
					PRINTLN("MAINTAIN_TRUCK_RENOVATION 4 BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_EMPTY_ARMORY_TRUCK_TRAILER TRUE")
				ENDIF
			ENDIF
		ENDIF
		MAINTAIN_TRUCK_CAB_RENOVATE()
		MAINTAIN_TRUCK_TRAILER_RENOVATE()
		OWNER_RENOVATE_CAB_INSIDE_THE_BUNKER()
		MAINTAIN_TRAILER_TURRETS_RENOVATE()
	ENDIF
ENDPROC

PROC MAINTAIN_KICK_PLAYERS_OUT_OF_CREATOR_TRAILER()
	IF NETWORK_IS_ACTIVITY_SESSION()
		IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehCreatorTrailer)
		AND GET_OWNER_OF_CREATOR_TRAILER(MPGlobalsAmbience.vehCreatorTrailer) = PLAYER_ID()
			IF IS_ENTITY_ON_FIRE(MPGlobalsAmbience.vehCreatorTrailer)
				SET_EMPTY_ARMORY_TRUCK_TRAILER(TRUE)
				CDEBUG1LN(DEBUG_SPAWNING, "MAINTAIN_KICK_PLAYERS_OUT_OF_CREATOR_TRAILER - BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_EMPTY_ARMORY_TRUCK_TRAILER true")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_ARMORY_TRUCK_TURRET_ACCESS()
	
	IF g_ownerOfarmorytruckPropertyIAmIn = INVALID_PLAYER_INDEX()
		EXIT
	ENDIF

	IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfarmorytruckPropertyIAmIn)].netID_TruckV[1])
		VEHICLE_INDEX vehIndex = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfarmorytruckPropertyIAmIn)].netID_TruckV[1])
		
		IF NOT IS_ENTITY_DEAD(vehIndex)
		AND GET_NUM_VEHICLE_MODS(vehIndex, MOD_ROOF) > 0	

			// Access to turret station A by default.
			IF GET_VEHICLE_MOD(vehIndex, MOD_ROOF) >= 0
				IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_GLOBAL_TRUCK_TURRET_STATION_A_AVAILABLE)
					SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_GLOBAL_TRUCK_TURRET_STATION_A_AVAILABLE)
					CDEBUG1LN(DEBUG_SPAWNING, "MAINTAIN_ARMORY_TRUCK_TURRET_ACCESS - Setting truck turret station A as available.")
				ENDIF
			ENDIF
			
			// Access to turret station B only once second turret is purchased.
			IF GET_VEHICLE_MOD(vehIndex, MOD_ROOF) >= 1
				IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_GLOBAL_TRUCK_TURRET_STATION_B_AVAILABLE)
					SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_GLOBAL_TRUCK_TURRET_STATION_B_AVAILABLE)
					CDEBUG1LN(DEBUG_SPAWNING, "MAINTAIN_ARMORY_TRUCK_TURRET_ACCESS - Setting truck turret station B as available.")
				ENDIF
			ELSE
				IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_GLOBAL_TRUCK_TURRET_STATION_B_AVAILABLE)
					CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_GLOBAL_TRUCK_TURRET_STATION_B_AVAILABLE)
					CDEBUG1LN(DEBUG_SPAWNING, "MAINTAIN_ARMORY_TRUCK_TURRET_ACCESS - Setting truck turret station B as unavailable.")
				ENDIF
			ENDIF
			
			// Access to turret station C only once third turret is purchased.
			IF GET_VEHICLE_MOD(vehIndex, MOD_ROOF) >= 1
				IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_GLOBAL_TRUCK_TURRET_STATION_C_AVAILABLE)
					SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_GLOBAL_TRUCK_TURRET_STATION_C_AVAILABLE)
					CDEBUG1LN(DEBUG_SPAWNING, "MAINTAIN_ARMORY_TRUCK_TURRET_ACCESS - Setting truck turret station C as available.")
				ENDIF
			ELSE
				IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_GLOBAL_TRUCK_TURRET_STATION_C_AVAILABLE)
					CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_GLOBAL_TRUCK_TURRET_STATION_C_AVAILABLE)
					CDEBUG1LN(DEBUG_SPAWNING, "MAINTAIN_ARMORY_TRUCK_TURRET_ACCESS - Setting truck turret station C as unavailable.")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_TRUCK_AFTER_JOIN_CEO()
	IF DO_I_NEED_TO_CLEARNUP_TRUCK_AFTER_JOIN_CEO()
		IF (NOT IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID()))
		OR (IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID()) AND PLAYER_ID() != g_ownerOfArmoryTruckPropertyIAmIn)
			CLEANUP_MP_SAVED_TRUCK(FALSE, FALSE, TRUE, FALSE, TRUE, FALSE, FALSE)
			SET_CLEANUP_TRUCK_AFTER_JOIN_CEO(FALSE)
		ENDIF
	ENDIF	
ENDPROC

PROC CLEANUP_AVENGER_AFTER_JOIN_CEO()
	IF DO_I_NEED_TO_CLEANUP_AVENGER_AFTER_JOIN_CEO()
		IF (NOT IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID()))
		OR (IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID()) AND PLAYER_ID() != g_OwnerOfArmoryAircraftPropertyIAmIn)
			CLEANUP_MP_SAVED_AVENGER(FALSE, FALSE, TRUE, FALSE, TRUE, FALSE, FALSE)
			SET_CLEANUP_AVENGER_AFTER_JOIN_CEO(FALSE)
		ENDIF
	ENDIF	
ENDPROC

PROC CLEANUP_HACKERTRUCK_AFTER_JOIN_CEO()
	IF DO_I_NEED_TO_CLEARNUP_HACKERTRUCK_AFTER_JOIN_CEO()
		IF (NOT IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID()))
		OR (IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID()) AND PLAYER_ID() != GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner)
			CLEANUP_MP_SAVED_HACKERTRUCK(FALSE, FALSE, TRUE, FALSE, TRUE, FALSE, FALSE)
			SET_CLEANUP_HACKERTRUCK_AFTER_JOIN_CEO(FALSE)
		ENDIF
	ENDIF	
ENDPROC

#IF FEATURE_DLC_2_2022
PROC CLEANUP_ACIDLAB_AFTER_JOIN_CEO()
	IF DO_I_NEED_TO_CLEARNUP_ACIDLAB_AFTER_JOIN_CEO()
		IF (NOT IS_PLAYER_IN_ACID_LAB(PLAYER_ID()))
		OR (IS_PLAYER_IN_ACID_LAB(PLAYER_ID()) AND PLAYER_ID() != GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner)
			CLEANUP_MP_SAVED_ACIDLAB(FALSE, FALSE, TRUE, FALSE, TRUE, FALSE, FALSE)
			SET_CLEANUP_ACIDLAB_AFTER_JOIN_CEO(FALSE)
		ENDIF
	ENDIF	
ENDPROC
#ENDIF

PROC MAINTAIN_PARTY_BUS_SIREN()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER)
			VEHICLE_INDEX pedVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF DOES_ENTITY_EXIST(pedVeh)
			AND NOT IS_ENTITY_DEAD(pedVeh)
				IF IS_VEHICLE_MODEL(pedVeh, PBUS2)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN)
					IF GET_IS_VEHICLE_ENGINE_RUNNING(pedVeh)
						IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_HORN)
							IF !IS_VEHICLE_SIREN_ON(pedVeh)
								SET_VEHICLE_SIREN(pedVeh, TRUE)
								SET_VEHICLE_KEEP_ENGINE_ON_WHEN_ABANDONED(pedVeh, TRUE)
							ELSE
								SET_VEHICLE_SIREN(pedVeh, FALSE)
								SET_VEHICLE_KEEP_ENGINE_ON_WHEN_ABANDONED(pedVeh, FALSE)
							ENDIF
						ENDIF	
					ELSE
						IF IS_VEHICLE_SIREN_ON(pedVeh)
							SET_VEHICLE_SIREN(pedVeh, FALSE)
							SET_VEHICLE_KEEP_ENGINE_ON_WHEN_ABANDONED(pedVeh, FALSE)
						ENDIF
					ENDIF
				ENDIF	
			ENDIF	
		ENDIF
	ENDIF
ENDPROC

SCRIPT_CONTROL_HOLD_TIMER trashMasterButton
PROC MAINTAIN_TRASH_MASTER_CRUSHER()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER)
			VEHICLE_INDEX pedVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF DOES_ENTITY_EXIST(pedVeh)
			AND NOT IS_ENTITY_DEAD(pedVeh)
				IF IS_VEHICLE_MODEL(pedVeh, TRASH2)
					trashMasterButton.action = INPUT_CONTEXT
					trashMasterButton.control = PLAYER_CONTROL
					IF IS_CONTROL_HELD(trashMasterButton, FALSE)
						IF GET_VEHICLE_DOOR_ANGLE_RATIO(pedVeh, SC_DOOR_BOOT) = 0.0
							SET_VEHICLE_DOOR_OPEN(pedVeh, SC_DOOR_BOOT)
						ELSE
							SET_VEHICLE_DOOR_SHUT(pedVeh, SC_DOOR_BOOT, FALSE)
						ENDIF
					ENDIF
				ENDIF	
			ENDIF	
		ENDIF
	ENDIF
ENDPROC


FUNC BOOL HAS_ANY_REMOTE_CAB_PASSANGER_REQUESTED_TO_CLEANUP()
	IF IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
	AND PLAYER_ID() = g_ownerOfArmoryTruckPropertyIAmIn
		IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehTruckVehicle[0])
			IF NOT IS_VEHICLE_SEAT_FREE(MPGlobalsAmbience.vehTruckVehicle[0])
				IF NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(MPGlobalsAmbience.vehTruckVehicle[0])) != INVALID_PLAYER_INDEX()
					IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(MPGlobalsAmbience.vehTruckVehicle[0])))].SimpleInteriorBD.iBSThree, BS_SIMPLE_INTERIOR_GLOBAL_BS_THREE_TELL_ONWER_TO_CLEANUP_MOC)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
			IF NOT IS_VEHICLE_SEAT_FREE(MPGlobalsAmbience.vehTruckVehicle[0], VS_FRONT_RIGHT)
				IF NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(MPGlobalsAmbience.vehTruckVehicle[0], VS_FRONT_RIGHT)) != INVALID_PLAYER_INDEX()
					IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(MPGlobalsAmbience.vehTruckVehicle[0], VS_FRONT_RIGHT)))].SimpleInteriorBD.iBSThree, BS_SIMPLE_INTERIOR_GLOBAL_BS_THREE_TELL_ONWER_TO_CLEANUP_MOC)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDIF

	RETURN FALSE
ENDFUNC


SCRIPT_TIMER sTimer_clearTruckDestroyedFlag
PROC CLEANUP_TRUCK_AFTER_TRAILER_IS_DESTROYED()
	IF IS_GUNRUNNING_TRUCK_PURCHASED()
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
			IF NOT IS_ARMORY_TRUCK_UNDER_MAP(PLAYER_ID())
				VEHICLE_INDEX truckCab, truckTrailer
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])
					truckCab = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])
				ENDIF	
				
				truckTrailer = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
				
				IF IS_ENTITY_DEAD(truckTrailer)
				OR NOT IS_VEHICLE_DRIVEABLE(truckTrailer)
				OR GET_ENTITY_HEALTH(truckTrailer) <= 0
				OR (DOES_ENTITY_EXIST(truckCab)
				AND IS_ENTITY_ATTACHED_TO_ENTITY(truckCab, truckTrailer)
				AND IS_ENTITY_IN_DEEP_WATER(truckCab))
				OR HAS_ANY_REMOTE_CAB_PASSANGER_REQUESTED_TO_CLEANUP()
					IF NOT IS_BIT_SET(iLocalBS , LOCAL_BS_CLEAN_UP_TRAILER_AFTER_DESTROYED ) 
						SET_EMPTY_ARMORY_TRUCK_TRAILER(TRUE)
						CLEANUP_MP_SAVED_TRUCK(FALSE, FALSE, TRUE, FALSE, TRUE)
						IF IS_PEGASUS_VEHICLE_EXIST_IN_TRUCK_CAR_MOD_SECTION(PLAYER_ID())
							SET_CLEARNUP_PEGASUS_BEFORE_SUPERMOD(TRUE)
							SET_PEGASUS_VEHICLE_EXIST_IN_TRUCK_CAR_MOD_SECTION(FALSE)
						ELIF IS_VEHICLE_EXIST_IN_TRUCK_CAR_MOD_SECTION(PLAYER_ID())
							CLEANUP_MP_SAVED_VEHICLE(FALSE, TRUE, DEFAULT, DEFAULT, TRUE, TRUE)
						ENDIF	
						SET_BIT(iLocalBS , LOCAL_BS_CLEAN_UP_TRAILER_AFTER_DESTROYED)
						PRINTLN("CLEANUP_TRUCK_AFTER_TRAILER_IS_DESTROYED done")
					ELSE
						PRINTLN("CLEANUP_TRUCK_AFTER_TRAILER_IS_DESTROYED LOCAL_BS_CLEAN_UP_TRAILER_AFTER_DESTROYED TRUE")
					ENDIF
				ENDIF	
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				IF GET_FRAME_COUNT() % 60 = 0 
					PRINTLN("CLEANUP_TRUCK_AFTER_TRAILER_IS_DESTROYED - truck doesn't exist did I delete it !?")
				ENDIF
			#ENDIF
			
			IF IS_BIT_SET(iLocalBS , LOCAL_BS_CLEAN_UP_TRAILER_AFTER_DESTROYED) 
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				AND NOT NETWORK_IS_PLAYER_IN_MP_CUTSCENE(PLAYER_ID())
				AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
					IF NOT HAS_NET_TIMER_STARTED(sTimer_clearTruckDestroyedFlag)
						START_NET_TIMER(sTimer_clearTruckDestroyedFlag)
					ENDIF
					IF HAS_NET_TIMER_STARTED(sTimer_clearTruckDestroyedFlag)
						IF HAS_NET_TIMER_EXPIRED(sTimer_clearTruckDestroyedFlag, 5000)
							PRINTLN("CLEANUP_TRUCK_AFTER_TRAILER_IS_DESTROYED cleared LOCAL_BS_CLEAN_UP_TRAILER_AFTER_DESTROYED")
							CLEAR_BIT(iLocalBS , LOCAL_BS_CLEAN_UP_TRAILER_AFTER_DESTROYED)
						ENDIF
					ENDIF
				ELSE
					IF HAS_NET_TIMER_STARTED(sTimer_clearTruckDestroyedFlag)
						RESET_NET_TIMER(sTimer_clearTruckDestroyedFlag)
					ENDIF
				ENDIF
			ELSE
				IF HAS_NET_TIMER_STARTED(sTimer_clearTruckDestroyedFlag)
					RESET_NET_TIMER(sTimer_clearTruckDestroyedFlag)
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		PLAYER_INDEX ownerOfArmoryTruckCabIAmIn = GET_OWNER_OF_ARMORY_TRUCK(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
		IF ownerOfArmoryTruckCabIAmIn != PLAYER_ID()
		AND ownerOfArmoryTruckCabIAmIn !=  INVALID_PLAYER_INDEX()
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(ownerOfArmoryTruckCabIAmIn)].netID_TruckV[1])
			
				VEHICLE_INDEX truckCab, truckTrailer
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(ownerOfArmoryTruckCabIAmIn)].netID_TruckV[0])
					truckCab = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(ownerOfArmoryTruckCabIAmIn)].netID_TruckV[0])
				ENDIF	
				
				truckTrailer = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(ownerOfArmoryTruckCabIAmIn)].netID_TruckV[1])
				
				IF IS_ENTITY_DEAD(truckTrailer)
				OR NOT IS_VEHICLE_DRIVEABLE(truckTrailer)
				OR GET_ENTITY_HEALTH(truckTrailer) <= 0
				OR (DOES_ENTITY_EXIST(truckCab)
				AND IS_ENTITY_ATTACHED_TO_ENTITY(truckCab, truckTrailer)
				AND IS_ENTITY_IN_DEEP_WATER(truckCab))
					IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSThree, BS_SIMPLE_INTERIOR_GLOBAL_BS_THREE_TELL_ONWER_TO_CLEANUP_MOC)
						SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSThree, BS_SIMPLE_INTERIOR_GLOBAL_BS_THREE_TELL_ONWER_TO_CLEANUP_MOC)
						PRINTLN("CLEANUP_TRUCK_AFTER_TRAILER_IS_DESTROYED BS_SIMPLE_INTERIOR_GLOBAL_BS_THREE_TELL_ONWER_TO_CLEANUP_MOC = TRUE")
					ENDIF
				ELSE
					IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSThree, BS_SIMPLE_INTERIOR_GLOBAL_BS_THREE_TELL_ONWER_TO_CLEANUP_MOC)
						CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSThree, BS_SIMPLE_INTERIOR_GLOBAL_BS_THREE_TELL_ONWER_TO_CLEANUP_MOC)
						PRINTLN("CLEANUP_TRUCK_AFTER_TRAILER_IS_DESTROYED BS_SIMPLE_INTERIOR_GLOBAL_BS_THREE_TELL_ONWER_TO_CLEANUP_MOC = FALSE no longer in water")
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSThree, BS_SIMPLE_INTERIOR_GLOBAL_BS_THREE_TELL_ONWER_TO_CLEANUP_MOC)
					CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSThree, BS_SIMPLE_INTERIOR_GLOBAL_BS_THREE_TELL_ONWER_TO_CLEANUP_MOC)
					PRINTLN("CLEANUP_TRUCK_AFTER_TRAILER_IS_DESTROYED BS_SIMPLE_INTERIOR_GLOBAL_BS_THREE_TELL_ONWER_TO_CLEANUP_MOC = FALSE 1")
				ENDIF
			ENDIF	
		ELSE
			IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSThree, BS_SIMPLE_INTERIOR_GLOBAL_BS_THREE_TELL_ONWER_TO_CLEANUP_MOC)
				CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSThree, BS_SIMPLE_INTERIOR_GLOBAL_BS_THREE_TELL_ONWER_TO_CLEANUP_MOC)
				PRINTLN("CLEANUP_TRUCK_AFTER_TRAILER_IS_DESTROYED BS_SIMPLE_INTERIOR_GLOBAL_BS_THREE_TELL_ONWER_TO_CLEANUP_MOC = FALSE 2")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_ANY_REMOTE_AVENGER_PASSANGER_REQUESTED_TO_CLEANUP()
	IF IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
	AND PLAYER_ID() = g_OwnerOfArmoryAircraftPropertyIAmIn
		IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehAvengerVehicle)
			IF NOT IS_VEHICLE_SEAT_FREE(MPGlobalsAmbience.vehAvengerVehicle)
				IF NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(MPGlobalsAmbience.vehAvengerVehicle)) != INVALID_PLAYER_INDEX()
					IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(MPGlobalsAmbience.vehAvengerVehicle)))].SimpleInteriorBD.iBSThree, BS_SIMPLE_INTERIOR_GLOBAL_BS_THREE_TELL_ONWER_TO_CLEANUP_AOC)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
			IF NOT IS_VEHICLE_SEAT_FREE(MPGlobalsAmbience.vehAvengerVehicle, VS_FRONT_RIGHT)
				IF NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(MPGlobalsAmbience.vehAvengerVehicle, VS_FRONT_RIGHT)) != INVALID_PLAYER_INDEX()
					IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(MPGlobalsAmbience.vehAvengerVehicle, VS_FRONT_RIGHT)))].SimpleInteriorBD.iBSThree, BS_SIMPLE_INTERIOR_GLOBAL_BS_THREE_TELL_ONWER_TO_CLEANUP_AOC)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDIF

	RETURN FALSE
ENDFUNC

SCRIPT_TIMER sTimer_clearOsprayDestroyedFlag
PROC CLEANUP_OSPREY_AFTER_IT_IS_DESTROYED()
	IF IS_ARMORY_AIRCRAFT_PURCHASED()
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV)
			IF NOT IS_ARMORY_AIRCRAFT_UNDER_MAP(PLAYER_ID())
				VEHICLE_INDEX avengerIndex = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV)
				IF IS_ENTITY_DEAD(avengerIndex)
				OR NOT IS_VEHICLE_DRIVEABLE(avengerIndex)
				OR GET_ENTITY_HEALTH(avengerIndex) <= 0
				OR HAS_ANY_REMOTE_AVENGER_PASSANGER_REQUESTED_TO_CLEANUP()
				OR IS_ENTITY_SUBMERGED_IN_WATER(avengerIndex, FALSE, 0.9)
					IF NOT IS_BIT_SET(iLocalBS , LOCAL_BS_CLEAN_UP_OSPREY_AFTER_DESTROYED ) 
						SET_PLAYER_AVENGER_DESTROYED(TRUE)
						SET_EMPTY_AVENGER_HOLD(TRUE)
						CLEANUP_MP_SAVED_AVENGER(FALSE, FALSE, TRUE, FALSE, TRUE)	
						SET_BIT(iLocalBS , LOCAL_BS_CLEAN_UP_OSPREY_AFTER_DESTROYED)
						PRINTLN("CLEANUP_OSPREY_AFTER_IT_IS_DESTROYED done")
					ELSE
						PRINTLN("CLEANUP_OSPREY_AFTER_IT_IS_DESTROYED LOCAL_BS_CLEAN_UP_OSPREY_AFTER_DESTROYED TRUE")
					ENDIF	
				ENDIF
			ENDIF	
		ELSE
			#IF IS_DEBUG_BUILD
				IF GET_FRAME_COUNT() % 60 = 0 
					PRINTLN("CLEANUP_OSPREY_AFTER_IT_IS_DESTROYED - avenger doesn't exist did I delete it !?")
				ENDIF
			#ENDIF
			
			IF IS_BIT_SET(iLocalBS , LOCAL_BS_CLEAN_UP_OSPREY_AFTER_DESTROYED) 
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				AND NOT NETWORK_IS_PLAYER_IN_MP_CUTSCENE(PLAYER_ID())
				AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
					IF NOT HAS_NET_TIMER_STARTED(sTimer_clearOsprayDestroyedFlag)
						START_NET_TIMER(sTimer_clearOsprayDestroyedFlag)
					ENDIF
					IF HAS_NET_TIMER_STARTED(sTimer_clearOsprayDestroyedFlag)
						IF HAS_NET_TIMER_EXPIRED(sTimer_clearOsprayDestroyedFlag, 5000)
							PRINTLN("CLEANUP_OSPREY_AFTER_IT_IS_DESTROYED cleared LOCAL_BS_CLEAN_UP_OSPREY_AFTER_DESTROYED")
							CLEAR_BIT(iLocalBS , LOCAL_BS_CLEAN_UP_OSPREY_AFTER_DESTROYED)
						ENDIF
					ENDIF
				ELSE
					IF HAS_NET_TIMER_STARTED(sTimer_clearOsprayDestroyedFlag)
						RESET_NET_TIMER(sTimer_clearOsprayDestroyedFlag)
					ENDIF
				ENDIF
			ELSE
				IF HAS_NET_TIMER_STARTED(sTimer_clearOsprayDestroyedFlag)
					RESET_NET_TIMER(sTimer_clearOsprayDestroyedFlag)
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		PLAYER_INDEX ownerOfPersonalArmoryAircraftIAmIn = GET_OWNER_OF_PERSONAL_ARMORY_AIRCRAFT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
		IF ownerOfPersonalArmoryAircraftIAmIn != PLAYER_ID()
		AND ownerOfPersonalArmoryAircraftIAmIn !=  INVALID_PLAYER_INDEX()
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(ownerOfPersonalArmoryAircraftIAmIn)].netID_AvengerV)
				VEHICLE_INDEX avengerIndex = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(ownerOfPersonalArmoryAircraftIAmIn)].netID_AvengerV)
				IF IS_ENTITY_DEAD(avengerIndex)
				OR NOT IS_VEHICLE_DRIVEABLE(avengerIndex)
				OR GET_ENTITY_HEALTH(avengerIndex) <= 0
					IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSThree, BS_SIMPLE_INTERIOR_GLOBAL_BS_THREE_TELL_ONWER_TO_CLEANUP_AOC)
						SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSThree, BS_SIMPLE_INTERIOR_GLOBAL_BS_THREE_TELL_ONWER_TO_CLEANUP_AOC)
						PRINTLN("CLEANUP_OSPREY_AFTER_IT_IS_DESTROYED BS_SIMPLE_INTERIOR_GLOBAL_BS_THREE_TELL_ONWER_TO_CLEANUP_AOC = TRUE")
					ENDIF
				ELSE
					IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSThree, BS_SIMPLE_INTERIOR_GLOBAL_BS_THREE_TELL_ONWER_TO_CLEANUP_AOC)
						CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSThree, BS_SIMPLE_INTERIOR_GLOBAL_BS_THREE_TELL_ONWER_TO_CLEANUP_AOC)
						PRINTLN("CLEANUP_OSPREY_AFTER_IT_IS_DESTROYED BS_SIMPLE_INTERIOR_GLOBAL_BS_THREE_TELL_ONWER_TO_CLEANUP_AOC = FALSE no longer in water")
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSThree, BS_SIMPLE_INTERIOR_GLOBAL_BS_THREE_TELL_ONWER_TO_CLEANUP_AOC)
					CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSThree, BS_SIMPLE_INTERIOR_GLOBAL_BS_THREE_TELL_ONWER_TO_CLEANUP_AOC)
					PRINTLN("CLEANUP_OSPREY_AFTER_IT_IS_DESTROYED BS_SIMPLE_INTERIOR_GLOBAL_BS_THREE_TELL_ONWER_TO_CLEANUP_AOC = FALSE 1")
				ENDIF
			ENDIF	
		ELSE
			IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSThree, BS_SIMPLE_INTERIOR_GLOBAL_BS_THREE_TELL_ONWER_TO_CLEANUP_AOC)
				CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSThree, BS_SIMPLE_INTERIOR_GLOBAL_BS_THREE_TELL_ONWER_TO_CLEANUP_AOC)
				PRINTLN("CLEANUP_OSPREY_AFTER_IT_IS_DESTROYED BS_SIMPLE_INTERIOR_GLOBAL_BS_THREE_TELL_ONWER_TO_CLEANUP_AOC = FALSE 2")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_ANY_REMOTE_HACKER_TRUCK_PASSANGER_REQUESTED_TO_CLEANUP()
	IF IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
	AND PLAYER_ID() = g_OwnerOfHackerTruckPropertyIAmIn
		IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehHackerTruck)
			IF NOT IS_VEHICLE_SEAT_FREE(MPGlobalsAmbience.vehHackerTruck)
				IF NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(MPGlobalsAmbience.vehHackerTruck)) != INVALID_PLAYER_INDEX()
					IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(MPGlobalsAmbience.vehHackerTruck)))].SimpleInteriorBD.iBS4, BS_SIMPLE_INTERIOR_GLOBAL_BS_FOUR_TELL_ONWER_TO_CLEANUP_HACKER_TRUCK)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
			IF NOT IS_VEHICLE_SEAT_FREE(MPGlobalsAmbience.vehHackerTruck, VS_FRONT_RIGHT)
				IF NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(MPGlobalsAmbience.vehHackerTruck, VS_FRONT_RIGHT)) != INVALID_PLAYER_INDEX()
					IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(MPGlobalsAmbience.vehHackerTruck, VS_FRONT_RIGHT)))].SimpleInteriorBD.iBS4, BS_SIMPLE_INTERIOR_GLOBAL_BS_FOUR_TELL_ONWER_TO_CLEANUP_HACKER_TRUCK)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDIF

	RETURN FALSE
ENDFUNC

SCRIPT_TIMER sTimer_clearHackerTruckDestroyedFlag
PROC CLEANUP_HACKER_TRUCK_AFTER_IT_IS_DESTROYED()
	IF IS_HACKER_TRUCK_PURCHASED()
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV)
			IF IS_ENTITY_DEAD(NET_TO_ENT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV))
			OR NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV))
			OR GET_ENTITY_HEALTH(NET_TO_ENT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV)) <= 0
			OR HAS_ANY_REMOTE_HACKER_TRUCK_PASSANGER_REQUESTED_TO_CLEANUP()
			OR IS_ENTITY_SUBMERGED_IN_WATER(NET_TO_ENT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV), FALSE, 0.9)
				IF NOT IS_BIT_SET(iLocalBS , LOCAL_BS_CLEAN_UP_HACKERTRUCK_AFTER_DESTROYED ) 
					#IF IS_DEBUG_BUILD
					IF IS_ENTITY_DEAD(NET_TO_ENT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV))
						PRINTLN("CLEANUP_HACKER_TRUCK_AFTER_IT_IS_DESTROYED truck is dead")
					ENDIF
					IF NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV))
						PRINTLN("CLEANUP_HACKER_TRUCK_AFTER_IT_IS_DESTROYED truck is not drivable")
					ENDIF
					IF GET_ENTITY_HEALTH(NET_TO_ENT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV)) <= 0
						PRINTLN("CLEANUP_HACKER_TRUCK_AFTER_IT_IS_DESTROYED truck health is 0")
					ENDIF
					IF HAS_ANY_REMOTE_HACKER_TRUCK_PASSANGER_REQUESTED_TO_CLEANUP()
						PRINTLN("CLEANUP_HACKER_TRUCK_AFTER_IT_IS_DESTROYED HAS_ANY_REMOTE_HACKER_TRUCK_PASSANGER_REQUESTED_TO_CLEANUP true")
					ENDIF
					IF IS_ENTITY_SUBMERGED_IN_WATER(NET_TO_ENT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV), FALSE, 0.9)
						PRINTLN("CLEANUP_HACKER_TRUCK_AFTER_IT_IS_DESTROYED truck is in water")
					ENDIF
					#ENDIF
					
					SET_EMPTY_HACKER_TRUCK_TRAILER(TRUE)
					CLEANUP_MP_SAVED_HACKERTRUCK(FALSE, FALSE, TRUE, FALSE, TRUE)	
					SET_BIT(iLocalBS , LOCAL_BS_CLEAN_UP_HACKERTRUCK_AFTER_DESTROYED)
					PRINTLN("CLEANUP_HACKER_TRUCK_AFTER_IT_IS_DESTROYED done")
				ELSE
					PRINTLN("CLEANUP_HACKER_TRUCK_AFTER_IT_IS_DESTROYED LOCAL_BS_CLEAN_UP_HACKERTRUCK_AFTER_DESTROYED TRUE")
				ENDIF	
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				IF GET_FRAME_COUNT() % 60 = 0 
					PRINTLN("CLEANUP_HACKER_TRUCK_AFTER_IT_IS_DESTROYED - hacker truck doesn't exist did I delete it !?")
				ENDIF
			#ENDIF
			
			IF IS_BIT_SET(iLocalBS , LOCAL_BS_CLEAN_UP_HACKERTRUCK_AFTER_DESTROYED) 
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				AND NOT NETWORK_IS_PLAYER_IN_MP_CUTSCENE(PLAYER_ID())
				AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
					IF NOT HAS_NET_TIMER_STARTED(sTimer_clearHackerTruckDestroyedFlag)
						START_NET_TIMER(sTimer_clearHackerTruckDestroyedFlag)
					ENDIF
					IF HAS_NET_TIMER_STARTED(sTimer_clearHackerTruckDestroyedFlag)
						IF HAS_NET_TIMER_EXPIRED(sTimer_clearHackerTruckDestroyedFlag, 5000)
							PRINTLN("CLEANUP_HACKER_TRUCK_AFTER_IT_IS_DESTROYED cleared LOCAL_BS_CLEAN_UP_HACKERTRUCK_AFTER_DESTROYED")
							CLEAR_BIT(iLocalBS , LOCAL_BS_CLEAN_UP_HACKERTRUCK_AFTER_DESTROYED)
						ENDIF
					ENDIF
				ELSE
					IF HAS_NET_TIMER_STARTED(sTimer_clearHackerTruckDestroyedFlag)
						RESET_NET_TIMER(sTimer_clearHackerTruckDestroyedFlag)
					ENDIF
				ENDIF
			ELSE
				IF HAS_NET_TIMER_STARTED(sTimer_clearHackerTruckDestroyedFlag)
					RESET_NET_TIMER(sTimer_clearHackerTruckDestroyedFlag)
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		PLAYER_INDEX ownerOfPersonalHackerTruckIAmIn = GET_OWNER_OF_PERSONAL_HACKER_TRUCK(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
		IF ownerOfPersonalHackerTruckIAmIn != PLAYER_ID()
		AND ownerOfPersonalHackerTruckIAmIn !=  INVALID_PLAYER_INDEX()
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(ownerOfPersonalHackerTruckIAmIn)].netID_HackertruckV)
				IF IS_ENTITY_DEAD(NET_TO_ENT(GlobalplayerBD[NATIVE_TO_INT(ownerOfPersonalHackerTruckIAmIn)].netID_HackertruckV))
				OR NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(ownerOfPersonalHackerTruckIAmIn)].netID_HackertruckV))
				OR GET_ENTITY_HEALTH(NET_TO_ENT(GlobalplayerBD[NATIVE_TO_INT(ownerOfPersonalHackerTruckIAmIn)].netID_HackertruckV)) <= 0
					IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS4, BS_SIMPLE_INTERIOR_GLOBAL_BS_FOUR_TELL_ONWER_TO_CLEANUP_HACKER_TRUCK)
						SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS4, BS_SIMPLE_INTERIOR_GLOBAL_BS_FOUR_TELL_ONWER_TO_CLEANUP_HACKER_TRUCK)
						PRINTLN("CLEANUP_HACKER_TRUCK_AFTER_IT_IS_DESTROYED BS_SIMPLE_INTERIOR_GLOBAL_BS_FOUR_TELL_ONWER_TO_CLEANUP_HACKER_TRUCK = TRUE")
					ENDIF
				ELSE
					IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS4, BS_SIMPLE_INTERIOR_GLOBAL_BS_FOUR_TELL_ONWER_TO_CLEANUP_HACKER_TRUCK)
						CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS4, BS_SIMPLE_INTERIOR_GLOBAL_BS_FOUR_TELL_ONWER_TO_CLEANUP_HACKER_TRUCK)
						PRINTLN("CLEANUP_HACKER_TRUCK_AFTER_IT_IS_DESTROYED BS_SIMPLE_INTERIOR_GLOBAL_BS_FOUR_TELL_ONWER_TO_CLEANUP_HACKER_TRUCK = FALSE no longer in water")
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS4, BS_SIMPLE_INTERIOR_GLOBAL_BS_FOUR_TELL_ONWER_TO_CLEANUP_HACKER_TRUCK)
					CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS4, BS_SIMPLE_INTERIOR_GLOBAL_BS_FOUR_TELL_ONWER_TO_CLEANUP_HACKER_TRUCK)
					PRINTLN("CLEANUP_HACKER_TRUCK_AFTER_IT_IS_DESTROYED BS_SIMPLE_INTERIOR_GLOBAL_BS_FOUR_TELL_ONWER_TO_CLEANUP_HACKER_TRUCK = FALSE 1")
				ENDIF
			ENDIF	
		ELSE
			IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS4, BS_SIMPLE_INTERIOR_GLOBAL_BS_FOUR_TELL_ONWER_TO_CLEANUP_HACKER_TRUCK)
				CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS4, BS_SIMPLE_INTERIOR_GLOBAL_BS_FOUR_TELL_ONWER_TO_CLEANUP_HACKER_TRUCK)
				PRINTLN("CLEANUP_HACKER_TRUCK_AFTER_IT_IS_DESTROYED BS_SIMPLE_INTERIOR_GLOBAL_BS_FOUR_TELL_ONWER_TO_CLEANUP_HACKER_TRUCK = FALSE 2")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

SCRIPT_TIMER sEmptyHackerTruck
PROC CLEAR_EMPTY_HACKER_TRUCK_FLAG()
	IF HAS_OWNER_EMPTY_HACKER_TRUCK(PLAYER_ID())
		IF NOT HAS_NET_TIMER_STARTED(sEmptyHackerTruck)
			START_NET_TIMER(sEmptyHackerTruck)
			PRINTLN("CLEAR_EMPTY_HACKER_TRUCK_FLAG - START sEmptyAvenger")
		ELSE
			IF HAS_NET_TIMER_EXPIRED(sEmptyHackerTruck, 15000)
				SET_EMPTY_HACKER_TRUCK_TRAILER(FALSE)
				PRINTLN("CLEAR_EMPTY_HACKER_TRUCK_FLAG - FALSE")
				RESET_NET_TIMER(sEmptyHackerTruck)
			ENDIF
		ENDIF
	ELSE
		IF HAS_NET_TIMER_STARTED(sEmptyHackerTruck)
			RESET_NET_TIMER(sEmptyHackerTruck)
		ENDIF
	ENDIF
ENDPROC

SCRIPT_TIMER sKickHackerTruck
PROC CLEAR_KICK_PLAYER_HACKER_TRUCK_FLAG()
	IF HAS_OWNER_KICKED_OUT_PLAYER_FROM_HACKER_TRUCK(PLAYER_ID())
		IF NOT HAS_NET_TIMER_STARTED(sKickHackerTruck)
			START_NET_TIMER(sKickHackerTruck)
			PRINTLN("CLEAR_KICK_PLAYER_HACKER_TRUCK_FLAG - START sEmptyAvenger")
		ELSE
			IF HAS_NET_TIMER_EXPIRED(sKickHackerTruck, 15000)
				SET_KICK_OUT_PLAYER_FROM_HACKER_TRUCK(FALSE)
				PRINTLN("CLEAR_KICK_PLAYER_HACKER_TRUCK_FLAG - FALSE")
				RESET_NET_TIMER(sKickHackerTruck)
			ENDIF
		ENDIF
	ELSE
		IF HAS_NET_TIMER_STARTED(sKickHackerTruck)
			RESET_NET_TIMER(sKickHackerTruck)
		ENDIF
	ENDIF
ENDPROC

#IF FEATURE_DLC_2_2022
FUNC BOOL HAS_ANY_REMOTE_ACID_LAB_PASSANGER_REQUESTED_TO_CLEANUP()
	IF IS_PLAYER_IN_ACID_LAB(PLAYER_ID())
	AND PLAYER_ID() = g_OwnerOfAcidLabPropertyIAmIn
		IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehAcidLab)
			IF NOT IS_VEHICLE_SEAT_FREE(MPGlobalsAmbience.vehAcidLab)
				IF NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(MPGlobalsAmbience.vehAcidLab)) != INVALID_PLAYER_INDEX()
					IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(MPGlobalsAmbience.vehAcidLab)))].SimpleInteriorBD.iBSSeven, BS_SIMPLE_INTERIOR_GLOBAL_BS_SEVEN_TELL_ONWER_TO_CLEANUP_ACID_LAB)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
			IF NOT IS_VEHICLE_SEAT_FREE(MPGlobalsAmbience.vehAcidLab, VS_FRONT_RIGHT)
				IF NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(MPGlobalsAmbience.vehAcidLab, VS_FRONT_RIGHT)) != INVALID_PLAYER_INDEX()
					IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(MPGlobalsAmbience.vehAcidLab, VS_FRONT_RIGHT)))].SimpleInteriorBD.iBSSeven, BS_SIMPLE_INTERIOR_GLOBAL_BS_SEVEN_TELL_ONWER_TO_CLEANUP_ACID_LAB)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDIF

	RETURN FALSE
ENDFUNC

SCRIPT_TIMER sEmptyAcidLab
PROC CLEAR_EMPTY_ACID_LAB_FLAG()
	IF HAS_OWNER_EMPTY_ACID_LAB(PLAYER_ID())
		IF NOT HAS_NET_TIMER_STARTED(sEmptyAcidLab)
			START_NET_TIMER(sEmptyAcidLab)
			PRINTLN("CLEAR_EMPTY_ACID_LAB_FLAG - START sEmptyAvenger")
		ELSE
			IF HAS_NET_TIMER_EXPIRED(sEmptyAcidLab, 15000)
				SET_EMPTY_ACID_LAB(FALSE)
				PRINTLN("CLEAR_EMPTY_ACID_LAB_FLAG - FALSE")
				RESET_NET_TIMER(sEmptyAcidLab)
			ENDIF
		ENDIF
	ELSE
		IF HAS_NET_TIMER_STARTED(sEmptyAcidLab)
			RESET_NET_TIMER(sEmptyAcidLab)
		ENDIF
	ENDIF
ENDPROC

SCRIPT_TIMER sTimer_clearAcidLabDestroyedFlag
PROC CLEANUP_ACID_LAB_AFTER_IT_IS_DESTROYED()
	IF IS_PLAYER_ACID_LAB_PURCHASED(PLAYER_ID())
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV)
			IF IS_ENTITY_DEAD(NET_TO_ENT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV))
			OR NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV))
			OR GET_ENTITY_HEALTH(NET_TO_ENT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV)) <= 0
			OR HAS_ANY_REMOTE_ACID_LAB_PASSANGER_REQUESTED_TO_CLEANUP()
			OR IS_ENTITY_SUBMERGED_IN_WATER(NET_TO_ENT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV), FALSE, 0.9)
				IF NOT IS_BIT_SET(iLocalBS , LOCAL_BS_CLEAN_UP_ACIDLAB_AFTER_DESTROYED ) 
					#IF IS_DEBUG_BUILD
					IF IS_ENTITY_DEAD(NET_TO_ENT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV))
						PRINTLN("CLEANUP_ACID_LAB_AFTER_IT_IS_DESTROYED truck is dead")
					ENDIF
					IF NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV))
						PRINTLN("CLEANUP_ACID_LAB_AFTER_IT_IS_DESTROYED truck is not drivable")
					ENDIF
					IF GET_ENTITY_HEALTH(NET_TO_ENT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV)) <= 0
						PRINTLN("CLEANUP_ACID_LAB_AFTER_IT_IS_DESTROYED truck health is 0")
					ENDIF
					IF HAS_ANY_REMOTE_ACID_LAB_PASSANGER_REQUESTED_TO_CLEANUP()
						PRINTLN("CLEANUP_ACID_LAB_AFTER_IT_IS_DESTROYED HAS_ANY_REMOTE_ACID_LAB_PASSANGER_REQUESTED_TO_CLEANUP true")
					ENDIF
					IF IS_ENTITY_SUBMERGED_IN_WATER(NET_TO_ENT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV), FALSE, 0.9)
						PRINTLN("CLEANUP_ACID_LAB_AFTER_IT_IS_DESTROYED truck is in water")
					ENDIF
					#ENDIF
					
					SET_EMPTY_ACID_LAB(TRUE)
					CLEANUP_MP_SAVED_ACIDLAB(FALSE, FALSE, TRUE, FALSE, TRUE)	
					SET_BIT(iLocalBS , LOCAL_BS_CLEAN_UP_ACIDLAB_AFTER_DESTROYED)
					PRINTLN("CLEANUP_ACID_LAB_AFTER_IT_IS_DESTROYED done")
				ELSE
					PRINTLN("CLEANUP_ACID_LAB_AFTER_IT_IS_DESTROYED LOCAL_BS_CLEAN_UP_ACIDLAB_AFTER_DESTROYED TRUE")
				ENDIF	
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				IF GET_FRAME_COUNT() % 60 = 0 
					PRINTLN("CLEANUP_ACID_LAB_AFTER_IT_IS_DESTROYED - acid lab doesn't exist did I delete it !?")
				ENDIF
			#ENDIF
			
			IF IS_BIT_SET(iLocalBS , LOCAL_BS_CLEAN_UP_ACIDLAB_AFTER_DESTROYED) 
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				AND NOT NETWORK_IS_PLAYER_IN_MP_CUTSCENE(PLAYER_ID())
				AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
					IF NOT HAS_NET_TIMER_STARTED(sTimer_clearAcidLabDestroyedFlag)
						START_NET_TIMER(sTimer_clearAcidLabDestroyedFlag)
					ENDIF
					IF HAS_NET_TIMER_STARTED(sTimer_clearAcidLabDestroyedFlag)
						IF HAS_NET_TIMER_EXPIRED(sTimer_clearAcidLabDestroyedFlag, 5000)
							PRINTLN("CLEANUP_ACID_LAB_AFTER_IT_IS_DESTROYED cleared LOCAL_BS_CLEAN_UP_ACIDLAB_AFTER_DESTROYED")
							CLEAR_BIT(iLocalBS , LOCAL_BS_CLEAN_UP_ACIDLAB_AFTER_DESTROYED)
						ENDIF
					ENDIF
				ELSE
					IF HAS_NET_TIMER_STARTED(sTimer_clearAcidLabDestroyedFlag)
						RESET_NET_TIMER(sTimer_clearAcidLabDestroyedFlag)
					ENDIF
				ENDIF
			ELSE
				IF HAS_NET_TIMER_STARTED(sTimer_clearAcidLabDestroyedFlag)
					RESET_NET_TIMER(sTimer_clearAcidLabDestroyedFlag)
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		PLAYER_INDEX ownerOfPersonalAcidLabIAmIn = GET_OWNER_OF_PERSONAL_ACID_LAB(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
		IF ownerOfPersonalAcidLabIAmIn != PLAYER_ID()
		AND ownerOfPersonalAcidLabIAmIn !=  INVALID_PLAYER_INDEX()
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(ownerOfPersonalAcidLabIAmIn)].netID_AcidLabV)
				IF IS_ENTITY_DEAD(NET_TO_ENT(GlobalplayerBD[NATIVE_TO_INT(ownerOfPersonalAcidLabIAmIn)].netID_AcidLabV))
				OR NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(ownerOfPersonalAcidLabIAmIn)].netID_AcidLabV))
				OR GET_ENTITY_HEALTH(NET_TO_ENT(GlobalplayerBD[NATIVE_TO_INT(ownerOfPersonalAcidLabIAmIn)].netID_AcidLabV)) <= 0
					IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSSeven, BS_SIMPLE_INTERIOR_GLOBAL_BS_SEVEN_TELL_ONWER_TO_CLEANUP_ACID_LAB)
						SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSSeven, BS_SIMPLE_INTERIOR_GLOBAL_BS_SEVEN_TELL_ONWER_TO_CLEANUP_ACID_LAB)
						PRINTLN("CLEANUP_ACID_LAB_AFTER_IT_IS_DESTROYED BS_SIMPLE_INTERIOR_GLOBAL_BS_SEVEN_TELL_ONWER_TO_CLEANUP_ACID_LAB = TRUE")
					ENDIF
				ELSE
					IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSSeven, BS_SIMPLE_INTERIOR_GLOBAL_BS_SEVEN_TELL_ONWER_TO_CLEANUP_ACID_LAB)
						CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSSeven, BS_SIMPLE_INTERIOR_GLOBAL_BS_SEVEN_TELL_ONWER_TO_CLEANUP_ACID_LAB)
						PRINTLN("CLEANUP_ACID_LAB_AFTER_IT_IS_DESTROYED BS_SIMPLE_INTERIOR_GLOBAL_BS_SEVEN_TELL_ONWER_TO_CLEANUP_ACID_LAB = FALSE no longer in water")
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSSeven, BS_SIMPLE_INTERIOR_GLOBAL_BS_SEVEN_TELL_ONWER_TO_CLEANUP_ACID_LAB)
					CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSSeven, BS_SIMPLE_INTERIOR_GLOBAL_BS_SEVEN_TELL_ONWER_TO_CLEANUP_ACID_LAB)
					PRINTLN("CLEANUP_ACID_LAB_AFTER_IT_IS_DESTROYED BS_SIMPLE_INTERIOR_GLOBAL_BS_SEVEN_TELL_ONWER_TO_CLEANUP_ACID_LAB = FALSE 1")
				ENDIF
			ENDIF	
		ELSE
			IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSSeven, BS_SIMPLE_INTERIOR_GLOBAL_BS_SEVEN_TELL_ONWER_TO_CLEANUP_ACID_LAB)
				CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSSeven, BS_SIMPLE_INTERIOR_GLOBAL_BS_SEVEN_TELL_ONWER_TO_CLEANUP_ACID_LAB)
				PRINTLN("CLEANUP_ACID_LAB_AFTER_IT_IS_DESTROYED BS_SIMPLE_INTERIOR_GLOBAL_BS_SEVEN_TELL_ONWER_TO_CLEANUP_ACID_LAB = FALSE 2")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

SCRIPT_TIMER sKickAcidLab
PROC CLEAR_KICK_PLAYER_ACID_LAB_FLAG()
	IF HAS_OWNER_KICKED_OUT_PLAYER_FROM_ACID_LAB(PLAYER_ID())
		IF NOT HAS_NET_TIMER_STARTED(sKickAcidLab)
			START_NET_TIMER(sKickAcidLab)
			PRINTLN("CLEAR_KICK_PLAYER_ACID_LAB_FLAG - START sKickAcidLab")
		ELSE
			IF HAS_NET_TIMER_EXPIRED(sKickAcidLab, 15000)
				SET_KICK_OUT_PLAYER_FROM_ACID_LAB(FALSE)
				PRINTLN("CLEAR_KICK_PLAYER_ACID_LAB_FLAG - FALSE")
				RESET_NET_TIMER(sKickAcidLab)
			ENDIF
		ENDIF
	ELSE
		IF HAS_NET_TIMER_STARTED(sKickAcidLab)
			RESET_NET_TIMER(sKickAcidLab)
		ENDIF
	ENDIF
ENDPROC
#ENDIF

#IF FEATURE_HEIST_ISLAND
SCRIPT_TIMER sKickSubmarine
PROC CLEAR_KICK_PLAYER_SUBMARINE_FLAG()
	IF HAS_OWNER_KICKED_OUT_PLAYER_FROM_SUBMARINE(PLAYER_ID())
		IF NOT HAS_NET_TIMER_STARTED(sKickSubmarine)
			START_NET_TIMER(sKickSubmarine)
			PRINTLN("CLEAR_KICK_PLAYER_SUBMARINE_FLAG - START sKickSubmarine")
		ELSE
			IF HAS_NET_TIMER_EXPIRED(sKickSubmarine, 15000)
				SET_KICK_OUT_PLAYER_FROM_SUBMARINE(FALSE)
				PRINTLN("CLEAR_KICK_PLAYER_SUBMARINE_FLAG - FALSE")
				RESET_NET_TIMER(sKickSubmarine)
			ENDIF
		ENDIF
	ELSE
		IF HAS_NET_TIMER_STARTED(sKickSubmarine)
			RESET_NET_TIMER(sKickSubmarine)
		ENDIF
	ENDIF
ENDPROC

PROC GET_SUBMARINE_WATER_CALMING_ZONE(VEHICLE_INDEX subVeh, VECTOR &vCoords, FLOAT &minX, FLOAT &minY, FLOAT &maxX, FLOAT &maxY)
	vCoords = GET_ENTITY_COORDS(subVeh)
	minX = vCoords.x - MPGlobals.fSubmarineWaveRadius
	minY = vCoords.y - MPGlobals.fSubmarineWaveRadius
	maxX = vCoords.x + MPGlobals.fSubmarineWaveRadius
	maxY = vCoords.y + MPGlobals.fSubmarineWaveRadius
ENDPROC

FUNC BOOL SHOULD_ALLOW_CALM_WATER_AROUND_SUBMARINE(VEHICLE_INDEX subVeh)
	IF NOT IS_ENTITY_ALIVE(subVeh)
		RETURN FALSE
	ENDIF

	IF IS_ENTITY_SUBMERGED_IN_WATER(subVeh, FALSE, 0.9)
		RETURN FALSE
	ENDIF
	
	IF IS_DOING_SUBMARINE_FAST_TRAVEL()
		RETURN FALSE
	ENDIF	
	
	IF IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
		RETURN FALSE
	ENDIF	
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_WAVE_CALMING_ZONE_ALREADY_EXISTS_IN_AREA(VECTOR vNearArea)
	INT i
	FOR i = 0 TO NUM_NETWORK_PLAYERS - 1
		IF iSubmarineCalmID[i] != -1
			IF VDIST2(vNearArea, vSubmarineCalmWaterCoord[i]) < MPGlobals.fSubmarineWaveRadius * MPGlobals.fSubmarineWaveRadius
				RETURN TRUE
			ENDIF
		ENDIF	
	ENDFOR
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_WATER_CALMING_AROUND_SUBMARINE()
	BOOL bMaintainCalmArea = FALSE

	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
		EXIT
	ENDIF
	
	IF DOES_ENTITY_EXIST(MPGlobals.RemoteSubmarineV[iRemoteVehicleStagger]) 

		VECTOR vTemp 
		FLOAT minX, minY, maxX, maxY
		VEHICLE_INDEX submarineVeh = MPGlobals.RemoteSubmarineV[iRemoteVehicleStagger]
		
		IF SHOULD_ALLOW_CALM_WATER_AROUND_SUBMARINE(submarineVeh)
			GET_SUBMARINE_WATER_CALMING_ZONE(submarineVeh, vTemp, minX, minY, maxX, maxY)
			IF NOT IS_VECTOR_ZERO(vTemp)
				IF iSubmarineCalmID[iRemoteVehicleStagger] = -1
					IF iTotalNumCalmZone < 8	// Max number of calming zone we can have
					AND NOT IS_WAVE_CALMING_ZONE_ALREADY_EXISTS_IN_AREA(vTemp)
						iSubmarineCalmID[iRemoteVehicleStagger] = ADD_EXTRA_CALMING_QUAD(minX, minY, maxX, maxY, 0.0)
						iTotalNumCalmZone++
						IF iSubmarineCalmID[iRemoteVehicleStagger] != -1
							SET_CALMED_WAVE_HEIGHT_SCALER(MPGlobals.fSubmarineWaveScaler)
							vSubmarineCalmWaterCoord[iRemoteVehicleStagger] = vTemp
							PRINTLN("MAINTAIN_WATER_CALMING_AROUND_SUBMARINE - Added calming quad ", iSubmarineCalmID[iRemoteVehicleStagger]," at ", vTemp)
						ELSE
							PRINTLN("MAINTAIN_WATER_CALMING_AROUND_SUBMARINE - ADD_EXTRA_CALMING_QUAD returned -1")
						ENDIF
					ENDIF
					
					bMaintainCalmArea = TRUE
				ELSE
					IF VDIST2(vTemp, vSubmarineCalmWaterCoord[iRemoteVehicleStagger]) <= MPGlobals.fSubmarineWaveCleanupRadius * MPGlobals.fSubmarineWaveCleanupRadius
						bMaintainCalmArea = TRUE
					ELSE	
						PRINTLN("MAINTAIN_WATER_CALMING_AROUND_SUBMARINE - moved away from previous water, current coord ", vTemp, " previous coord ", vSubmarineCalmWaterCoord[iRemoteVehicleStagger])
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF	
	ENDIF
	
	IF NOT bMaintainCalmArea
		IF iSubmarineCalmID[iRemoteVehicleStagger] != -1
			PRINTLN("MAINTAIN_WATER_CALMING_AROUND_SUBMARINE - REMOVE_EXTRA_CALMING_QUAD iCalmID = ", iSubmarineCalmID[iRemoteVehicleStagger])
			REMOVE_EXTRA_CALMING_QUAD(iSubmarineCalmID[iRemoteVehicleStagger])
			iTotalNumCalmZone--
			iSubmarineCalmID[iRemoteVehicleStagger] = -1
			vSubmarineCalmWaterCoord[iRemoteVehicleStagger] = <<0, 0, 0>>
		ENDIF
	ENDIF

ENDPROC
#ENDIF

SCRIPT_TIMER sKickEveryOneOutAvengerForRenovate
PROC CLEAR_KICK_OUT_FOR_RENOVATE_HACKER_TRUCK_FLAG()
	IF IS_OWNER_RENOVATED_HACKER_TRUCK(PLAYER_ID())
		IF NOT HAS_NET_TIMER_STARTED(sKickEveryOneOutAvengerForRenovate)
			START_NET_TIMER(sKickEveryOneOutAvengerForRenovate)
			PRINTLN("CLEAR_KICK_OUT_FOR_RENOVATE_HACKER_TRUCK_FLAG - START sKickEveryOneOutAvengerForRenovate")
		ELSE
			IF HAS_NET_TIMER_EXPIRED(sKickEveryOneOutAvengerForRenovate, 15000)
				SET_OWNER_RENOVATED_HACKER_TRUCK(FALSE)
				PRINTLN("CLEAR_KICK_OUT_FOR_RENOVATE_HACKER_TRUCK_FLAG - FALSE")
				RESET_NET_TIMER(sKickEveryOneOutAvengerForRenovate)
			ENDIF
		ENDIF
	ELSE
		IF HAS_NET_TIMER_STARTED(sKickEveryOneOutAvengerForRenovate)
			RESET_NET_TIMER(sKickEveryOneOutAvengerForRenovate)
		ENDIF
	ENDIF
ENDPROC

#IF FEATURE_HEIST_ISLAND
SCRIPT_TIMER sEmptySub
PROC CLEAR_EMPTY_SUB_FLAG()
	IF HAS_OWNER_EMPTIED_SUBMARINE(PLAYER_ID())
		IF NOT HAS_NET_TIMER_STARTED(sEmptySub)
			START_NET_TIMER(sEmptySub)
			
			PRINTLN("CLEAR_EMPTY_SUB_FLAG - START sEmptySub")
		ELSE
			IF HAS_NET_TIMER_EXPIRED(sEmptySub, 15000)
				SET_OWNER_EMPTIED_SUBMARINE(FALSE)
				
				PRINTLN("CLEAR_EMPTY_SUB_FLAG - FALSE")
				
				RESET_NET_TIMER(sEmptySub)
			ENDIF
		ENDIF
	ELSE
		IF HAS_NET_TIMER_STARTED(sEmptySub)
			RESET_NET_TIMER(sEmptySub)
		ENDIF
	ENDIF
ENDPROC

SCRIPT_TIMER sEmptySubHelm
PROC CLEAR_EMPTY_SUB_HELM_FLAG()
	IF HAS_OWNER_EMPTIED_SUBMARINE_HELM(PLAYER_ID())
		IF NOT HAS_NET_TIMER_STARTED(sEmptySubHelm)
			START_NET_TIMER(sEmptySubHelm)
			
			PRINTLN("CLEAR_EMPTY_SUB_HELM_FLAG - START sEmptySubHelm")
		ELSE
			IF HAS_NET_TIMER_EXPIRED(sEmptySubHelm, 15000)
				SET_OWNER_EMPTIED_SUBMARINE_HELM(FALSE)
				
				PRINTLN("CLEAR_EMPTY_SUB_HELM_FLAG - FALSE")
				
				RESET_NET_TIMER(sEmptySubHelm)
			ENDIF
		ENDIF
	ELSE
		IF HAS_NET_TIMER_STARTED(sEmptySubHelm)
			RESET_NET_TIMER(sEmptySubHelm)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_ANY_REMOTE_SUBMARINE_PASSANGER_REQUESTED_TO_CLEANUP()
	IF IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
	AND PLAYER_ID() = g_OwnerOfSubmarinePropertyIAmIn
		IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehSubmarine)
			IF NOT IS_VEHICLE_SEAT_FREE(MPGlobalsAmbience.vehSubmarine)
				IF NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(MPGlobalsAmbience.vehSubmarine)) != INVALID_PLAYER_INDEX()
					IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(MPGlobalsAmbience.vehSubmarine)))].SimpleInteriorBD.iBSSix, BS_SIMPLE_INTERIOR_GLOBAL_BS_SIX_TELL_ONWER_TO_CLEANUP_SUBMARINE)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDIF

	RETURN FALSE
ENDFUNC

SCRIPT_TIMER sTimer_clearSubmarineDestroyedFlag
PROC CLEANUP_SUBMARINE_AFTER_IT_IS_DESTROYED()

	PLAYER_INDEX LocalPlayer = PLAYER_ID()
	INT LocalPlayerInt = NATIVE_TO_INT(LocalPlayer)

	IF IS_PLAYER_SUBMARINE_PURCHASED(PLAYER_ID())
		
		NETWORK_INDEX LocalSubNetId = GlobalplayerBD[LocalPlayerInt].netID_SubmarineV
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(LocalSubNetId)
		AND NOT IS_MP_SAVED_SUBMARINE_BEING_CLEANED_UP()
			
			VEHICLE_INDEX LocalSubId = NET_TO_VEH(LocalSubNetId)
			
			IF IS_ENTITY_DEAD(LocalSubId)
			OR GET_ENTITY_HEALTH(LocalSubId) <= 0
			OR (NETWORK_HAS_CONTROL_OF_NETWORK_ID(LocalSubNetId) AND GET_SUBMARINE_NUMBER_OF_AIR_LEAKS(LocalSubId) >= SUBMARINE_AIR_LEAK_LIMIT)
			OR HAS_ANY_REMOTE_SUBMARINE_PASSANGER_REQUESTED_TO_CLEANUP()
			
				#IF IS_DEBUG_BUILD
				IF IS_ENTITY_DEAD(LocalSubId)
					PRINTLN("CLEANUP_SUBMARINE_AFTER_IT_IS_DESTROYED submarine is dead")
				ENDIF
				IF GET_ENTITY_HEALTH(LocalSubId) <= 0
					PRINTLN("CLEANUP_SUBMARINE_AFTER_IT_IS_DESTROYED submarine health is 0")
				ENDIF
				IF (NETWORK_HAS_CONTROL_OF_NETWORK_ID(LocalSubNetId) AND GET_SUBMARINE_NUMBER_OF_AIR_LEAKS(LocalSubId) >= SUBMARINE_AIR_LEAK_LIMIT)
					PRINTLN("CLEANUP_SUBMARINE_AFTER_IT_IS_DESTROYED submarine leak limit reached")
				ENDIF
				IF HAS_ANY_REMOTE_SUBMARINE_PASSANGER_REQUESTED_TO_CLEANUP()
					PRINTLN("CLEANUP_SUBMARINE_AFTER_IT_IS_DESTROYED submarine HAS_ANY_REMOTE_SUBMARINE_PASSANGER_REQUESTED_TO_CLEANUP")
				ENDIF
				#ENDIF
				
				IF NOT HAS_NET_TIMER_STARTED(sTimer_clearSubmarineDestroyedFlag)
					START_NET_TIMER(sTimer_clearSubmarineDestroyedFlag)
					
					PRINTLN("CLEANUP_SUBMARINE_AFTER_IT_IS_DESTROYED starting clean up delay")
					
				ELIF HAS_NET_TIMER_EXPIRED(sTimer_clearSubmarineDestroyedFlag, 3000)
					RESET_NET_TIMER(sTimer_clearSubmarineDestroyedFlag)
					
					SET_EMPTY_DESTROYED_SMPL_INTERIOR_SUBMARINE(TRUE)
					CLEANUP_MP_SAVED_SUBMARINE(FALSE, FALSE, FALSE, FALSE, TRUE, TRUE)	
					
					PRINTLN("CLEANUP_SUBMARINE_AFTER_IT_IS_DESTROYED done")
				ENDIF	
			ENDIF
		ELSE
			RESET_NET_TIMER(sTimer_clearSubmarineDestroyedFlag)
		ENDIF
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		PLAYER_INDEX ownerOfPersonalSubmarineIAmIn = GET_OWNER_OF_SMPL_INTERIOR_SUBMARINE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
		IF ownerOfPersonalSubmarineIAmIn != PLAYER_ID()
		AND ownerOfPersonalSubmarineIAmIn !=  INVALID_PLAYER_INDEX()
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(ownerOfPersonalSubmarineIAmIn)].netID_SubmarineV)
				IF IS_ENTITY_DEAD(NET_TO_ENT(GlobalplayerBD[NATIVE_TO_INT(ownerOfPersonalSubmarineIAmIn)].netID_SubmarineV))
				OR GET_ENTITY_HEALTH(NET_TO_ENT(GlobalplayerBD[NATIVE_TO_INT(ownerOfPersonalSubmarineIAmIn)].netID_SubmarineV)) <= 0
				OR (NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(ownerOfPersonalSubmarineIAmIn)].netID_SubmarineV) 
					AND GET_SUBMARINE_NUMBER_OF_AIR_LEAKS(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(ownerOfPersonalSubmarineIAmIn)].netID_SubmarineV)) >= SUBMARINE_AIR_LEAK_LIMIT)
					IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSSix, BS_SIMPLE_INTERIOR_GLOBAL_BS_SIX_TELL_ONWER_TO_CLEANUP_SUBMARINE)
						SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSSix, BS_SIMPLE_INTERIOR_GLOBAL_BS_SIX_TELL_ONWER_TO_CLEANUP_SUBMARINE)
						PRINTLN("CLEANUP_SUBMARINE_AFTER_IT_IS_DESTROYED BS_SIMPLE_INTERIOR_GLOBAL_BS_SIX_TELL_ONWER_TO_CLEANUP_SUBMARINE = TRUE")
					ENDIF
				ELSE
					IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSSix, BS_SIMPLE_INTERIOR_GLOBAL_BS_SIX_TELL_ONWER_TO_CLEANUP_SUBMARINE)
						CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSSix, BS_SIMPLE_INTERIOR_GLOBAL_BS_SIX_TELL_ONWER_TO_CLEANUP_SUBMARINE)
						PRINTLN("CLEANUP_SUBMARINE_AFTER_IT_IS_DESTROYED BS_SIMPLE_INTERIOR_GLOBAL_BS_SIX_TELL_ONWER_TO_CLEANUP_SUBMARINE = FALSE no longer in water")
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSSix, BS_SIMPLE_INTERIOR_GLOBAL_BS_SIX_TELL_ONWER_TO_CLEANUP_SUBMARINE)
					CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSSix, BS_SIMPLE_INTERIOR_GLOBAL_BS_SIX_TELL_ONWER_TO_CLEANUP_SUBMARINE)
					PRINTLN("CLEANUP_SUBMARINE_AFTER_IT_IS_DESTROYED BS_SIMPLE_INTERIOR_GLOBAL_BS_SIX_TELL_ONWER_TO_CLEANUP_SUBMARINE = FALSE 1")
				ENDIF
			ENDIF	
		ELSE
			IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSSix, BS_SIMPLE_INTERIOR_GLOBAL_BS_SIX_TELL_ONWER_TO_CLEANUP_SUBMARINE)
				CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSSix, BS_SIMPLE_INTERIOR_GLOBAL_BS_SIX_TELL_ONWER_TO_CLEANUP_SUBMARINE)
				PRINTLN("CLEANUP_SUBMARINE_AFTER_IT_IS_DESTROYED BS_SIMPLE_INTERIOR_GLOBAL_BS_SIX_TELL_ONWER_TO_CLEANUP_SUBMARINE = FALSE 2")
			ENDIF
		ENDIF
	ENDIF
ENDPROC
#ENDIF

FUNC BOOL SHOULD_PLAYERS_PEGASUS_VEHICLE_BE_CONCEALED(INT iPlayerToCheck,VEHICLE_INDEX theVeh, BOOL bPrintReason = FALSE)
	//force conceal
	IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerToCheck].pegasusConceal.iBS,CONCEAL_PEGASUS_VEHICLE_BS_FORCE_CONCEAL_CURRENT)
		IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),theVeh,TRUE)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),theVeh,TRUE)
		RETURN FALSE
	ENDIF
	
	IF GlobalplayerBD_FM[iPlayerToCheck].pegasusConceal.iConcealInType = PV_CONCEAL_TYPE_BUNKER
		IF PLAYER_ID() = INT_TO_PLAYERINDEX(iPlayerToCheck)
		AND DONT_CONCEAL_MY_PEGASUS_THIS_FRAME()
			PRINTLN("D NOT - bunker SHOULD_PLAYERS_PEGASUS_VEHICLE_BE_CONCEALED Don't Conceal vehicle for player # ",iPlayerToCheck)
			RETURN FALSE
		ENDIF
		
		
		//conceal if in different property 
		IF ENUM_TO_INT(GET_FACTORY_ID_FROM_SIMPLE_INTERIOR_ID(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior)) != GlobalplayerBD_FM[iPlayerToCheck].pegasusConceal.iConcealInID
		AND NOT (IS_PLAYER_MOVING_FROM_BUNKER_TO_TRUCK() AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND PLAYER_ID() = INT_TO_PLAYERINDEX(iPlayerToCheck)) 	// Need this for truck cutscene from bunker to truck
			IF bPrintReason
				PRINTLN("A - bunker SHOULD_PLAYERS_PEGASUS_VEHICLE_BE_CONCEALED Concealing vehicle for player # ",iPlayerToCheck)
			ENDIF
			RETURN TRUE
		//conceal if same property different instances
		ELIF (IS_PLAYER_MOVING_FROM_BUNKER_TO_TRUCK() AND iPlayerToCheck != NATIVE_TO_INT(PLAYER_ID()) AND IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER))
			IF bPrintReason
				PRINTLN("B - bunker SHOULD_PLAYERS_PEGASUS_VEHICLE_BE_CONCEALED Concealing vehicle for player # ",iPlayerToCheck)
			ENDIF
			RETURN TRUE
		//conceal if same property/instance but different owners i.e. instance re-used
		ELIF GlobalplayerBD_FM[iPlayerToCheck].pegasusConceal.iConcealInPlayerOwnedLocation != -1
		AND g_ownerOfBunkerPropertyIAmIn != INT_TO_PLAYERINDEX(GlobalplayerBD_FM[iPlayerToCheck].pegasusConceal.iConcealInPlayerOwnedLocation)
			IF bPrintReason
				PRINTLN("C - bunker SHOULD_PLAYERS_PEGASUS_VEHICLE_BE_CONCEALED Concealing vehicle for player # ",iPlayerToCheck)
			ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	IF GlobalplayerBD_FM[iPlayerToCheck].pegasusConceal.iConcealInType = PV_CONCEAL_TYPE_ARMORY_TRUCK
		IF PLAYER_ID() = INT_TO_PLAYERINDEX(iPlayerToCheck)
		AND DONT_CONCEAL_MY_PEGASUS_THIS_FRAME()
			PRINTLN("D NOT - truck SHOULD_PLAYERS_PEGASUS_VEHICLE_BE_CONCEALED Don't Conceal vehicle for player # ",iPlayerToCheck)
			RETURN FALSE
		ENDIF
		
		IF ENUM_TO_INT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior) != GlobalplayerBD_FM[iPlayerToCheck].pegasusConceal.iConcealInID
		AND NOT (IS_PLAYER_MOVING_FROM_BUNKER_TO_TRUCK() AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND PLAYER_ID() = INT_TO_PLAYERINDEX(iPlayerToCheck))				// Need this for truck cutscene from bunker to truck
			IF bPrintReason
				PRINTLN("A - truck SHOULD_PLAYERS_PEGASUS_VEHICLE_BE_CONCEALED Concealing vehicle for player # ",iPlayerToCheck)
			ENDIF	
			RETURN TRUE
		ELIF GlobalplayerBD_FM[iPlayerToCheck].pegasusConceal.iConcealInPlayerOwnedLocation != -1
		AND g_ownerOfArmoryTruckPropertyIAmIn != INT_TO_PLAYERINDEX(GlobalplayerBD_FM[iPlayerToCheck].pegasusConceal.iConcealInPlayerOwnedLocation)
			IF bPrintReason
				PRINTLN("C - truck SHOULD_PLAYERS_PEGASUS_VEHICLE_BE_CONCEALED Concealing vehicle for player # ",iPlayerToCheck)
			ENDIF
			RETURN TRUE
		ELIF globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iInstance != globalPlayerBD[iPlayerToCheck].SimpleInteriorBD.iInstance
		AND globalPlayerBD[iPlayerToCheck].SimpleInteriorBD.iInstance != -1
		AND IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
		AND IS_PLAYER_IN_ARMORY_TRUCK(INT_TO_PLAYERINDEX(iPlayerToCheck))
			IF bPrintReason
				PRINTLN("B - truck SHOULD_PLAYERS_PEGASUS_VEHICLE_BE_CONCEALED Concealing vehicle for player # ",iPlayerToCheck)
			ENDIF	
			RETURN TRUE
		ENDIF
	ENDIF
	IF GlobalplayerBD_FM[iPlayerToCheck].pegasusConceal.iConcealInType = PV_CONCEAL_TYPE_ARMORY_AIRCRAFT
		IF PLAYER_ID() = INT_TO_PLAYERINDEX(iPlayerToCheck)
		AND DONT_CONCEAL_MY_PEGASUS_THIS_FRAME()
			PRINTLN("D NOT - aircraft SHOULD_PLAYERS_PEGASUS_VEHICLE_BE_CONCEALED Don't Conceal vehicle for player # ",iPlayerToCheck)
			RETURN FALSE
		ENDIF
		
		IF ENUM_TO_INT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior) != GlobalplayerBD_FM[iPlayerToCheck].pegasusConceal.iConcealInID
		AND NOT (IS_PLAYER_MOVING_FROM_BASE_TO_AIRCRAFT() AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND PLAYER_ID() = INT_TO_PLAYERINDEX(iPlayerToCheck))				// Need this for truck cutscene from base to aircraft
			IF bPrintReason
				PRINTLN("A - aircraft SHOULD_PLAYERS_PEGASUS_VEHICLE_BE_CONCEALED Concealing vehicle for player # ",iPlayerToCheck)
			ENDIF	
			RETURN TRUE
		ELIF GlobalplayerBD_FM[iPlayerToCheck].pegasusConceal.iConcealInPlayerOwnedLocation != -1
		AND g_ownerOfArmoryAircraftPropertyIAmIn != INT_TO_PLAYERINDEX(GlobalplayerBD_FM[iPlayerToCheck].pegasusConceal.iConcealInPlayerOwnedLocation)
			IF bPrintReason
				PRINTLN("C - aircraft SHOULD_PLAYERS_PEGASUS_VEHICLE_BE_CONCEALED Concealing vehicle for player # ",iPlayerToCheck)
			ENDIF
			RETURN TRUE
		ELIF globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iInstance != globalPlayerBD[iPlayerToCheck].SimpleInteriorBD.iInstance
		AND globalPlayerBD[iPlayerToCheck].SimpleInteriorBD.iInstance != -1
		AND IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
		AND IS_PLAYER_IN_ARMORY_AIRCRAFT(INT_TO_PLAYERINDEX(iPlayerToCheck))
			IF bPrintReason
				PRINTLN("B - aircraft SHOULD_PLAYERS_PEGASUS_VEHICLE_BE_CONCEALED Concealing vehicle for player # ",iPlayerToCheck)
			ENDIF	
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


PROC HANDLE_CONCEALING_PEGASUS_VEHICLE()
	IF IS_PLAYER_ON_HEIST_ISLAND(PLAYER_ID())
		EXIT
	ENDIF
	
	BOOL bKeepConcealed
	BOOL bNeedsConcealCheck
	INT i

	IF IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
	OR IS_PLAYER_IN_PROPERTY(PLAYER_ID(),TRUE)
	OR (g_SimpleInteriorData.eCurrentInteriorID != SIMPLE_INTERIOR_INVALID
	AND GET_SIMPLE_INTERIOR_TYPE(g_SimpleInteriorData.eCurrentInteriorID ) = SIMPLE_INTERIOR_TYPE_BUNKER)
	OR (g_SimpleInteriorData.eCurrentInteriorID != SIMPLE_INTERIOR_INVALID
	AND GET_SIMPLE_INTERIOR_TYPE(g_SimpleInteriorData.eCurrentInteriorID ) = SIMPLE_INTERIOR_TYPE_ARMORY_TRUCK)
	OR IS_PLAYER_MOVING_FROM_BUNKER_TO_TRUCK()
	OR IS_PLAYER_MOVING_FROM_BASE_TO_AIRCRAFT()
	OR (g_SimpleInteriorData.eCurrentInteriorID != SIMPLE_INTERIOR_INVALID
	AND GET_SIMPLE_INTERIOR_TYPE(g_SimpleInteriorData.eCurrentInteriorID ) = SIMPLE_INTERIOR_TYPE_ARMORY_AIRCRAFT)
		bNeedsConcealCheck = TRUE
	ENDIF
	
	
	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].pegasusConceal.iConcealInPlayerOwnedLocation  != -1
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].pegasusConceal.iConcealInID != -1
			IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].pegasusConceal.iConcealInType = PV_CONCEAL_TYPE_BUNKER
				IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].pegasusConceal.iConcealInID != ENUM_TO_INT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdFactoryData[BUNKER_SAVE_SLOT ].eFactoryID)
					CLEANUP_CURRENT_PEGASUS_VEHICLE(TRUE)
					CLEAR_CONCEAL_CURRENT_PEGASUS_VEHICLE()
					PRINTLN("HANDLE_CONCEALING_PEGASUS_VEHICLE: Cleaning up requested conceal player no longer owns bunker")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//PRINTLN("[personal_vehicle] HANDLE_CONCEALING_PEGASUS_VEHICLE run this frame bNeedsConcealCheck = ",bNeedsConcealCheck)
	//Handle concealing your's and other players pegasus vehicles 
	REPEAT NUM_NETWORK_PLAYERS i
		bKeepConcealed = FALSE
		IF (IS_BIT_SET(GlobalplayerBD_FM[i].pegasusConceal.iBS,CONCEAL_PEGASUS_VEHICLE_BS_CONCEAL_CURRENT)
		OR IS_BIT_SET(GlobalplayerBD_FM[i].pegasusConceal.iBS,CONCEAL_PEGASUS_VEHICLE_BS_FORCE_CONCEAL_CURRENT))
		AND bNeedsConcealCheck
			//PRINTLN("[personal_vehicle] HANDLE_CONCEALING_PEGASUS_VEHICLE checking vehicle for player # ",i)
			IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[i].netID_PegV)
			AND DOES_ENTITY_EXIST(NET_TO_VEH(GlobalplayerBD[i].netID_PegV))
			AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(GlobalplayerBD[i].netID_PegV))
				//PRINTLN("[personal_vehicle] HANDLE_CONCEALING_PEGASUS_VEHICLE vehicle exists ")
				IF SHOULD_PLAYERS_PEGASUS_VEHICLE_BE_CONCEALED(I,NET_TO_VEH(GlobalplayerBD[i].netID_PegV))
					//PRINTLN("[personal_vehicle] HANDLE_CONCEALING_PEGASUS_VEHICLE should be concealed ")
					bKeepConcealed = TRUE
				ENDIF
				IF bKeepConcealed = TRUE
					IF NOT NETWORK_IS_ENTITY_CONCEALED(NET_TO_VEH(GlobalplayerBD[i].netID_PegV))
						SHOULD_PLAYERS_PEGASUS_VEHICLE_BE_CONCEALED(i,NET_TO_VEH(GlobalplayerBD[i].netID_PegV), TRUE)						
						NETWORK_CONCEAL_ENTITY(NET_TO_VEH(GlobalplayerBD[i].netID_PegV),TRUE)
						IF NOT IS_BIT_SET(g_iConcealedPegasusVehicleBS,i)
							SET_BIT(g_iConcealedPegasusVehicleBS,i)
							#IF IS_DEBUG_BUILD
							IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(i),FALSE)
								PRINTLN("[personal_vehicle] HANDLE_CONCEALING_PEGASUS_VEHICLE - concealing player ID ", i," name: ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(i)), " pegasus vehicle")
							ELSE
								PRINTLN("[personal_vehicle] HANDLE_CONCEALING_PEGASUS_VEHICLE - concealing player ", i, " pegasus vehicle")
							ENDIF
							#ENDIF
						ENDIF
					ENDIF	
					
					
				ENDIF
			ENDIF
		ENDIF
		IF NOT bKeepConcealed
			IF IS_BIT_SET(g_iConcealedPegasusVehicleBS,i)
				IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[i].netID_PegV)
				AND DOES_ENTITY_EXIST(NET_TO_VEH(GlobalplayerBD[i].netID_PegV))
				AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(GlobalplayerBD[i].netID_PegV))
				AND NOT IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(INT_TO_PLAYERINDEX(i))
				AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(INT_TO_PLAYERINDEX(i)) 
				AND NOT HAS_PLAYER_ACCEPTED_SIMPLE_INTERIOR_INVITE()
				OR (DONT_CONCEAL_MY_PEGASUS_THIS_FRAME() AND INT_TO_PLAYERINDEX(i) = PLAYER_ID())
					IF NETWORK_IS_ENTITY_CONCEALED(NET_TO_VEH(GlobalplayerBD[i].netID_PegV))
						IF DONT_CONCEAL_MY_PEGASUS_THIS_FRAME()
							PRINTLN("HANDLE_CONCEALING_PEGASUS_VEHICLE - DONT_CONCEAL_MY_PV_THIS_FRAME true un-concealing player ID ", i," name: ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(i)), " pegasus vehicle")
						ENDIF
						NETWORK_CONCEAL_ENTITY(NET_TO_VEH(GlobalplayerBD[i].netID_PegV),FALSE)
						CLEAR_BIT(g_iConcealedPegasusVehicleBS,i)
						IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(i),FALSE)
							PRINTLN("HANDLE_CONCEALING_PEGASUS_VEHICLE - un-concealing player ID ", i," name: ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(i)), " pegasus vehicle")
						ELSE
							PRINTLN("HANDLE_CONCEALING_PEGASUS_VEHICLE - un-concealing player ", i, " pegasus vehicle")
						ENDIF
					ELSE
						CLEAR_BIT(g_iConcealedPegasusVehicleBS,i)
						PRINTLN("HANDLE_CONCEALING_PEGASUS_VEHICLE - player ", i, " pegasus vehicle already un-concealed")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT	
ENDPROC

PROC MAINTAIN_MOVE_PEGASUS_VEH_NEARBY_REQUESTS()
	IF MPGlobalsAmbience.iRequestFadeOutPegasusVeh != -1		
		IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[MPGlobalsAmbience.iRequestFadeOutPegasusVeh].netID_PegV)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[MPGlobalsAmbience.iRequestFadeOutPegasusVeh].netID_PegV)
			 	VEHICLE_INDEX PegasusVeh = NET_TO_VEH(GlobalplayerBD[MPGlobalsAmbience.iRequestFadeOutPegasusVeh].netID_PegV)
				
				IF IS_ENTITY_ALIVE(PegasusVeh)
					IF NOT IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_PEGASUS_VEH_FADED_OUT)
					
						NETWORK_FADE_OUT_ENTITY(PegasusVeh, TRUE, TRUE)
						SET_BIT(bookedVehicle.iBitset, BOOK_BS_PEGASUS_VEH_FADED_OUT)
						PRINTLN("MAINTAIN_MOVE_PEGASUS_VEH_NEARBY_REQUESTS - Fading out the vehicle")
						
					ELIF NOT IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_PEGASUS_VEH_WARPED)
						
						IF NOT NETWORK_IS_ENTITY_FADING(PegasusVeh)
						AND NOT NETWORK_IS_IN_MP_CUTSCENE()
											
							VECTOR vSpawnLocation
							FLOAT fSpawnHeading
							VEHICLE_SPAWN_LOCATION_PARAMS vehicleSpawnLocationParams
							MODEL_NAMES ePegasusVehModel = GET_ENTITY_MODEL(PegasusVeh)
							
							vehicleSpawnLocationParams.fMinDistFromCoords 		= 20
							vehicleSpawnLocationParams.fMaxDistance 			= 100
							vehicleSpawnLocationParams.bConsiderHighways 		= FALSE
							vehicleSpawnLocationParams.bCheckEntityArea 		= TRUE
							vehicleSpawnLocationParams.bCheckOwnVisibility 		= FALSE
							vehicleSpawnLocationParams.bConsiderOnlyActiveNodes = TRUE
							
							vehicleSpawnLocationParams.vAvoidCoords[0] = <<1612.1893, 2225.1592, 75.4580>>
							vehicleSpawnLocationParams.fAvoidRadius[0] = 6.0
							
							PRINTLN("MAINTAIN_MOVE_PEGASUS_VEH_NEARBY_REQUESTS - Attempting vehicle warp...")
							
							IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(GET_ENTITY_COORDS(PegasusVeh), <<0.0, 0.0, 0.0>>, ePegasusVehModel, TRUE, vSpawnLocation, fSpawnHeading, vehicleSpawnLocationParams)									  
								
								SET_ENTITY_COORDS_NO_OFFSET(PegasusVeh, vSpawnLocation)
								SET_ENTITY_HEADING(PegasusVeh, fSpawnHeading)
								SET_VEHICLE_ON_GROUND_PROPERLY(PegasusVeh)
								SET_VEHICLE_ENGINE_ON(PegasusVeh, FALSE, TRUE)
								SET_BIT(bookedVehicle.iBitset, BOOK_BS_PEGASUS_VEH_WARPED)
								PRINTLN("MAINTAIN_MOVE_PEGASUS_VEH_NEARBY_REQUESTS - Fading the vehicle back in. New location = ", vSpawnLocation)
							ENDIF
							
						ENDIF
						
					ELSE
						PRINTLN("MAINTAIN_MOVE_PEGASUS_VEH_NEARBY_REQUESTS - complete")
						NETWORK_FADE_IN_ENTITY(PegasusVeh, TRUE, TRUE)
						CLEAR_BIT(bookedVehicle.iBitset, BOOK_BS_PEGASUS_VEH_FADED_OUT)
						CLEAR_BIT(bookedVehicle.iBitset, BOOK_BS_PEGASUS_VEH_WARPED)
						MPGlobalsAmbience.iRequestFadeOutPegasusVeh = -1
					ENDIF
				ELSE
					PRINTLN("MAINTAIN_MOVE_PEGASUS_VEH_NEARBY_REQUESTS - Attempt failed as the entity is not alive")
					CLEAR_BIT(bookedVehicle.iBitset, BOOK_BS_PEGASUS_VEH_FADED_OUT)
					CLEAR_BIT(bookedVehicle.iBitset, BOOK_BS_PEGASUS_VEH_WARPED)
					MPGlobalsAmbience.iRequestFadeOutPegasusVeh = -1
				ENDIF
			ELSE
				NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(GlobalplayerBD[MPGlobalsAmbience.iRequestFadeOutPegasusVeh].netID_PegV)
				PRINTLN("MAINTAIN_MOVE_PEGASUS_VEH_NEARBY_REQUESTS - Attempt failed as we don't have control of the network ID - Requesting")
			ENDIF
		ELSE
			PRINTLN("MAINTAIN_MOVE_PEGASUS_VEH_NEARBY_REQUESTS - Attempt failed as the entity does not exist")
			CLEAR_BIT(bookedVehicle.iBitset, BOOK_BS_PEGASUS_VEH_FADED_OUT)
			CLEAR_BIT(bookedVehicle.iBitset, BOOK_BS_PEGASUS_VEH_WARPED)
			MPGlobalsAmbience.iRequestFadeOutPegasusVeh = -1
		ENDIF
	ENDIF
ENDPROC


PROC PROCESS_TURRET_POSITION(PLAYER_INDEX PlayerID, PLAYER_INDEX OwnerID, VEHICLE_INDEX TruckID)
	INT iOwnerIndex  = NATIVE_TO_INT(OwnerID)
	INT iPlayerIndex = NATIVE_TO_INT(PlayerID)
	
	INT turretIndex
	REPEAT GUNRUN_TOTAL_MOC_TURRETS turretIndex
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PlayerID)].bTurretInUse, turretIndex)
			IF NOT ARE_VECTORS_ALMOST_EQUAL(g_vCachedTurretRotation[NATIVE_TO_INT(OwnerID)][turretIndex], GlobalplayerBD[iPlayerIndex].vTurretRayTrace[turretIndex])
				g_vCachedTurretRotation[iOwnerIndex][turretIndex] = GlobalplayerBD[iPlayerIndex].vTurretRayTrace[turretIndex]
				
				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[MAINTAIN_TURRETS] - PROCESSING PROCESS_TURRET_POSITION UPDATE ",turretIndex," - (TURRET ID ",turretIndex+1,")")
//				PRINTLN("  ... Local Player     : ", GET_PLAYER_NAME(PLAYER_ID()), " - ID: ", NATIVE_TO_INT(PLAYER_ID()))
//				PRINTLN("  ... Turret Player    : ", GET_PLAYER_NAME(PlayerID), " - ID: ", NATIVE_TO_INT(PlayerID))
//				PRINTLN("  ... Truck ID         : ", NATIVE_TO_INT(GlobalplayerBD[iOwnerIndex].netID_TruckV[1]))
//				PRINTLN("  ... Target Position  : ", g_vCachedTurretRotation[iOwnerIndex][turretIndex])
//				PRINTLN("  ... Current Position : ", g_vCurrentTurretRotation[iOwnerIndex][turretIndex])
				
				// Don't set cannon target to 0,0,0 at the start.
				IF IS_VECTOR_ZERO(g_vCurrentTurretRotation[iOwnerIndex][turretIndex])
					g_vCurrentTurretRotation[iOwnerIndex][turretIndex] = g_vCachedTurretRotation[iOwnerIndex][turretIndex]
					PRINTLN("  ... Default Position : ", g_vCurrentTurretRotation[iOwnerIndex][turretIndex])
				ENDIF
				
				g_fInterpDistance[iOwnerIndex][turretIndex] =  GET_DISTANCE_BETWEEN_COORDS(g_vCurrentTurretRotation[iOwnerIndex][turretIndex], g_vCachedTurretRotation[iOwnerIndex][turretIndex])
				
//				PRINTLN("  ... Interp Dist     : ", g_fInterpDistance[iOwnerIndex][turretIndex])
			ENDIF
			
			IF NOT IS_VECTOR_ZERO(g_vCurrentTurretRotation[iOwnerIndex][turretIndex])
				SET_VEHICLE_TURRET_TARGET(TruckID, turretIndex, g_vCurrentTurretRotation[iOwnerIndex][turretIndex])
//				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[MAINTAIN_TURRETS_SPAM] - PROCESSING TURRET UPDATE ",turretIndex," - NEW POS: ", g_vCurrentTurretRotation[iOwnerIndex][turretIndex], ", at index: ", turretIndex)
//				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[MAINTAIN_TURRETS_SPAM] - NETWORK_HAS_CONTROL_OF_ENTITY(TruckID) = ", NETWORK_HAS_CONTROL_OF_ENTITY(TruckID) )
			ENDIF
			
			IF NOT ARE_VECTORS_EQUAL(g_vCachedTurretRotation[iOwnerIndex][turretIndex], g_vCurrentTurretRotation[iOwnerIndex][turretIndex])
			AND g_fInterpDistance[iOwnerIndex][turretIndex] >= 0.1
				FLOAT fDist = GET_DISTANCE_BETWEEN_COORDS(g_vCurrentTurretRotation[iOwnerIndex][turretIndex], g_vCachedTurretRotation[iOwnerIndex][turretIndex])
				FLOAT fAlpha = (g_fInterpDistance[iOwnerIndex][turretIndex] - fDist) / g_fInterpDistance[iOwnerIndex][turretIndex]
				
				IF fAlpha = 0
					fAlpha = 0.01
				ENDIF
				
				fAlpha = fAlpha / fTurretTimeModifier
				
				g_vCurrentTurretRotation[iOwnerIndex][turretIndex] = INTERP_VECTOR(g_vCurrentTurretRotation[iOwnerIndex][turretIndex], g_vCachedTurretRotation[iOwnerIndex][turretIndex], fAlpha)
			ENDIF
			
			EXIT
		ENDIF
	ENDREPEAT
ENDPROC

PROC MAINTAIN_VEHICLE_TURRET_POSITIONS()
	PLAYER_INDEX PlayerID
	PLAYER_INDEX OwnerID
	VEHICLE_INDEX TruckID
	
	IF IS_BIT_SET(iLocalBS, LOCAL_BS_SHOULD_ALLOW_EXPENSIVE_PROCESSING)
	OR IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE, TRUE)
		EXIT
	ENDIF
	
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		PlayerID = INT_TO_PLAYERINDEX(i)
		
		// MOC
		IF IS_PLAYER_IN_ARMORY_TRUCK(PlayerID)
		AND GlobalplayerBD[NATIVE_TO_INT(PlayerID)].bTurretInUse != 0
			OwnerID = GET_OWNER_OF_ARMORY_TRUCK_THAT_PLAYER_IS_INSIDE(PlayerID)
			
			IF OwnerID != INVALID_PLAYER_INDEX()
			AND NETWORK_IS_PLAYER_ACTIVE(OwnerID)
			AND NOT IS_ARMORY_TRUCK_UNDER_MAP(OwnerID)
				IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(OwnerID)].netID_TruckV[1])
					TruckID = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(OwnerID)].netID_TruckV[1])
					
					IF DOES_ENTITY_EXIST(TruckID)
					AND IS_ENTITY_ALIVE(TruckID)
						PROCESS_TURRET_POSITION(PlayerID, OwnerID, TruckID)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		// Avenger
		IF IS_PLAYER_IN_ARMORY_AIRCRAFT(PlayerID)
		AND GlobalplayerBD[NATIVE_TO_INT(PlayerID)].bTurretInUse != 0
			OwnerID = GET_OWNER_OF_ARMORY_AIRCRAFT_THAT_PLAYER_IS_INSIDE(PlayerID)
			
			IF OwnerID != INVALID_PLAYER_INDEX()
			AND NETWORK_IS_PLAYER_ACTIVE(OwnerID)
			AND NOT IS_ARMORY_AIRCRAFT_UNDER_MAP(OwnerID)
				
				IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(OwnerID)].netID_AvengerV)
					
					TruckID = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(OwnerID)].netID_AvengerV)
					
					IF DOES_ENTITY_EXIST(TruckID)
					AND IS_ENTITY_ALIVE(TruckID)
						PROCESS_TURRET_POSITION(PlayerID, OwnerID, TruckID)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		// Hacker Truck
		IF IS_PLAYER_IN_HACKER_TRUCK(PlayerID)
		AND GlobalplayerBD[NATIVE_TO_INT(PlayerID)].bTurretInUse != 0
			OwnerID = GET_OWNER_OF_HACKER_TRUCK_THAT_PLAYER_IS_INSIDE(PlayerID)
			IF OwnerID != INVALID_PLAYER_INDEX()
			AND NETWORK_IS_PLAYER_ACTIVE(OwnerID)
			AND NOT IS_HACKER_TRUCK_UNDER_MAP(OwnerID)
			AND NOT IS_LOCAL_PLAYER_USING_VEHICLE_WEAPON()
				IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(OwnerID)].netID_HackertruckV)
					TruckID = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(OwnerID)].netID_HackertruckV)
					IF GlobalplayerBD_FM[i].propertyDetails.iSimpleInteriorHost != -1
						PED_INDEX pedToCheck = MPGlobals.RemoteFakeHackerTruckPed[GlobalplayerBD_FM[i].propertyDetails.iSimpleInteriorHost]
						IF  DOES_ENTITY_EXIST(pedToCheck)
						AND NOT IS_ENTITY_DEAD(pedToCheck)
						AND IS_ENTITY_ALIVE(TruckID)
							INT boneIndex = GET_ENTITY_BONE_INDEX_BY_NAME(TruckID, "misc_c")
							VECTOR vTurretCoords
							IF boneIndex <> -1
								VECTOR vRocketOffset = GET_WORLD_POSITION_OF_ENTITY_BONE(TruckID, boneIndex) + <<0.0, 0, 0.80>>
								VECTOR vRot = GET_ENTITY_ROTATION(pedToCheck)
								VECTOR vDir = <<-SIN(vRot.z) * COS(vRot.x), COS(vRot.z) * COS(vRot.x), SIN(vRot.x)>>
								VECTOR vOffset = <<10.0, 10.0, 10.0>>
								vTurretCoords = vRocketOffset + (vDir*vOffset)
							ENDIF
							IF IS_ENTITY_ALIVE(TruckID)
								SET_VEHICLE_TURRET_TARGET(TruckID, 0, vTurretCoords)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		#IF FEATURE_DLC_2_2022
		IF IS_PLAYER_IN_ACID_LAB(PlayerID)
		AND GlobalplayerBD[NATIVE_TO_INT(PlayerID)].bTurretInUse != 0
			OwnerID = GET_OWNER_OF_ACID_LAB_THAT_PLAYER_IS_INSIDE(PlayerID)
			
			IF OwnerID != INVALID_PLAYER_INDEX()
			AND NETWORK_IS_PLAYER_ACTIVE(OwnerID)
			AND NOT IS_ACID_LAB_UNDER_MAP(OwnerID)
				
				IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(OwnerID)].netID_AcidLabV)
					
					TruckID = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(OwnerID)].netID_AcidLabV)
					
					IF DOES_ENTITY_EXIST(TruckID)
					AND IS_ENTITY_ALIVE(TruckID)
						PROCESS_TURRET_POSITION(PlayerID, OwnerID, TruckID)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		#ENDIF
		
		IF PlayerID != INVALID_PLAYER_INDEX()
			IF DOES_ENTITY_EXIST(MPGlobals.RemoteFakeHackerTruckPed[NATIVE_TO_INT(PlayerID)])
				IF NOT IS_ENTITY_DEAD(MPGlobals.RemoteFakeHackerTruckPed[NATIVE_TO_INT(PlayerID)])
					IF NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(MPGlobals.RemoteFakeHackerTruckPed[NATIVE_TO_INT(PlayerID)])
						SET_ENTITY_LOCALLY_INVISIBLE(MPGlobals.RemoteFakeHackerTruckPed[NATIVE_TO_INT(PlayerID)])
						PRINTLN("MAINTAIN_FAKE_HACKER_TRUCK_PED, Hacker truck SET_ENTITY_LOCALLY_INVISIBLE .. ", GET_PLAYER_NAME(PlayerID) )
					ENDIF
				ENDIF	
			ENDIF
			
			IF DOES_ENTITY_EXIST(MPGlobals.RemoteFakeAvengerPed[NATIVE_TO_INT(PlayerID)])
				IF NOT IS_ENTITY_DEAD(MPGlobals.RemoteFakeAvengerPed[NATIVE_TO_INT(PlayerID)])
					IF NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(MPGlobals.RemoteFakeAvengerPed[NATIVE_TO_INT(PlayerID)])
						SET_ENTITY_LOCALLY_INVISIBLE(MPGlobals.RemoteFakeAvengerPed[NATIVE_TO_INT(PlayerID)])
						PRINTLN("MAINTAIN_FAKE_HACKER_TRUCK_PED, Avenger SET_ENTITY_LOCALLY_INVISIBLE .. ")
					ENDIF
				ENDIF	
			ENDIF
			
			IF DOES_ENTITY_EXIST(MPGlobals.RemoteFakeMocPed[NATIVE_TO_INT(PlayerID)])
				IF NOT IS_ENTITY_DEAD(MPGlobals.RemoteFakeMocPed[NATIVE_TO_INT(PlayerID)])
					IF NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(MPGlobals.RemoteFakeMocPed[NATIVE_TO_INT(PlayerID)])
						SET_ENTITY_LOCALLY_INVISIBLE(MPGlobals.RemoteFakeMocPed[NATIVE_TO_INT(PlayerID)])
						PRINTLN("MAINTAIN_FAKE_HACKER_TRUCK_PED, MOC SET_ENTITY_LOCALLY_INVISIBLE .. ")
					ENDIF
				ENDIF	
			ENDIF
		ENDIF	
	ENDREPEAT
ENDPROC

PROC MAINTAIN_ENTITIES_FOR_ORBITAL_CANNON()
	
	IF IS_PLAYER_ON_HEIST_ISLAND(PLAYER_ID())
		EXIT
	ENDIF
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		EXIT
	ENDIF
	
	IF NETWORK_GET_NUM_PARTICIPANTS() <= 1
		EXIT
	ENDIF	
	
	INT index = 0, subIndex = 0
	PLAYER_INDEX PlayerID
	
	REPEAT NUM_NETWORK_PLAYERS index
		PlayerID = INT_TO_PLAYERINDEX(index)
		
		IF PlayerID = INVALID_PLAYER_INDEX()
		OR NOT NETWORK_IS_PLAYER_ACTIVE(PlayerID)
		OR PlayerID = PLAYER_ID()
			RELOOP
		ENDIF
		
		IF PLAYER_USING_ORBITAL(PlayerID)
			REPEAT NUM_NETWORK_PLAYERS subIndex
				IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[subIndex].netID_TruckV[0])
				AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[subIndex].netID_TruckV[0])
					SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(GlobalplayerBD[subIndex].netID_TruckV[0], PlayerID, TRUE)
				ENDIF
				
				IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[subIndex].netID_TruckV[1])
				AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[subIndex].netID_TruckV[1])
					SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(GlobalplayerBD[subIndex].netID_TruckV[1], PlayerID, TRUE)
				ENDIF
				
				IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[subIndex].netID_AvengerV)
				AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[subIndex].netID_AvengerV)
					SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(GlobalplayerBD[subIndex].netID_AvengerV, PlayerID, TRUE)
				ENDIF
				
				IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[subIndex].netID_HackertruckV)
				AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[subIndex].netID_HackertruckV)
					SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(GlobalplayerBD[subIndex].netID_HackertruckV, PlayerID, TRUE)
				ENDIF
				
				#IF FEATURE_HEIST_ISLAND
				IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[subIndex].netID_SubmarineV)
				AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[subIndex].netID_SubmarineV)
					SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(GlobalplayerBD[subIndex].netID_SubmarineV, PlayerID, TRUE)
				ENDIF
				#ENDIF
				
				#IF FEATURE_DLC_2_2022
				IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[subIndex].netID_AcidLabV)
				AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[subIndex].netID_AcidLabV)
					SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(GlobalplayerBD[subIndex].netID_AcidLabV, PlayerID, TRUE)
				ENDIF
				#ENDIF
			ENDREPEAT
			
			SET_BIT(iVehicleNetTracker, index)
		ELIF IS_BIT_SET(iVehicleNetTracker, index)
			REPEAT NUM_NETWORK_PLAYERS subIndex
				IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[subIndex].netID_TruckV[0])
				AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[subIndex].netID_TruckV[0])
					SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(GlobalplayerBD[subIndex].netID_TruckV[0], PlayerID, FALSE)
				ENDIF
				
				IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[subIndex].netID_TruckV[1])
				AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[subIndex].netID_TruckV[1])
					SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(GlobalplayerBD[subIndex].netID_TruckV[1], PlayerID, FALSE)
				ENDIF
				
				IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[subIndex].netID_AvengerV)
				AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[subIndex].netID_AvengerV)
					SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(GlobalplayerBD[subIndex].netID_AvengerV, PlayerID, FALSE)
				ENDIF
				
				IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[subIndex].netID_HackertruckV)
				AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[subIndex].netID_HackertruckV)
					SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(GlobalplayerBD[subIndex].netID_HackertruckV, PlayerID, FALSE)
				ENDIF
				
				#IF FEATURE_HEIST_ISLAND
				IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[subIndex].netID_SubmarineV)
				AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[subIndex].netID_SubmarineV)
					SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(GlobalplayerBD[subIndex].netID_SubmarineV, PlayerID, FALSE)
				ENDIF
				#ENDIF
				
				#IF FEATURE_DLC_2_2022
				IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[subIndex].netID_AcidLabV)
				AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[subIndex].netID_AcidLabV)
					SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(GlobalplayerBD[subIndex].netID_AcidLabV, PlayerID, FALSE)
				ENDIF
				#ENDIF
			ENDREPEAT
			
			CLEAR_BIT(iVehicleNetTracker, index)
		ENDIF
	ENDREPEAT
ENDPROC

PROC MAINTAIN_HACKER_TRUCK_NETWORK_ENTITIES()
	IF iRemoteVehicleStagger != -1
	AND iRemoteVehicleStagger < NUM_NETWORK_PLAYERS
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iRemoteVehicleStagger].netID_HackertruckV)
			g_vehHackerTruck[iRemoteVehicleStagger] = NET_TO_VEH(GlobalplayerBD[iRemoteVehicleStagger].netID_HackertruckV)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_SUBMARINE_NETWORK_ENTITIES()
	IF iRemoteVehicleStagger != -1
	AND iRemoteVehicleStagger < NUM_NETWORK_PLAYERS
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iRemoteVehicleStagger].netID_SubmarineV)
			g_vehSubmarine[iRemoteVehicleStagger] = NET_TO_VEH(GlobalplayerBD[iRemoteVehicleStagger].netID_SubmarineV)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_SUBMARINE_DINGHY_NETWORK_ENTITIES()
	IF iRemoteVehicleStagger != -1
	AND iRemoteVehicleStagger < NUM_NETWORK_PLAYERS
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iRemoteVehicleStagger].netID_SubmarineDinghyV)
			g_vehSubmarineDinghy[iRemoteVehicleStagger] = NET_TO_VEH(GlobalplayerBD[iRemoteVehicleStagger].netID_SubmarineDinghyV)
		ENDIF
	ENDIF
ENDPROC

#IF FEATURE_DLC_2_2022
PROC MAINTAIN_ACID_LAB_NETWORK_ENTITIES()
	IF iRemoteVehicleStagger != -1
	AND iRemoteVehicleStagger < NUM_NETWORK_PLAYERS
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iRemoteVehicleStagger].netID_AcidLabV)
			g_vehAcidLab[iRemoteVehicleStagger] = NET_TO_VEH(GlobalplayerBD[iRemoteVehicleStagger].netID_AcidLabV)
		ENDIF
	ENDIF
ENDPROC
#ENDIF

#IF FEATURE_DLC_2_2022
PROC MAINTAIN_SUPPORT_BIKE_NETWORK_ENTITIES()
	IF iRemoteVehicleStagger != -1
	AND iRemoteVehicleStagger < NUM_NETWORK_PLAYERS
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iRemoteVehicleStagger].netID_SupportBikeV)
			g_vehSupportBike[iRemoteVehicleStagger] = NET_TO_VEH(GlobalplayerBD[iRemoteVehicleStagger].netID_SupportBikeV)
		ENDIF
	ENDIF
ENDPROC
#ENDIF

PROC PROCESS_VEHICLE_EXISTENCE_EVENT(INT iEventID)

	SCRIPT_EVENT_VEHICLE_EXISTENCE_STRUCT sEventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, sEventData, SIZE_OF(sEventData))
		
		PRINTLN("PROCESS_VEHICLE_EXISTENCE_EVENT - Recieved event from : ", GET_PLAYER_NAME(sEventData.Details.FromPlayerIndex), " sEventData.bExist: ", sEventData.bExist)
		
		IF sEventData.pPropertyOwner != INVALID_PLAYER_INDEX()
		AND sEventData.Details.FromPlayerIndex != INVALID_PLAYER_INDEX() 
			IF sEventData.bExist
				IF sEventData.bMOC
					IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(sEventData.pPropertyOwner)].netID_TruckV[0])
					AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(sEventData.pPropertyOwner)].netID_TruckV[0])
						SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(GlobalplayerBD[NATIVE_TO_INT(sEventData.pPropertyOwner)].netID_TruckV[0], sEventData.Details.FromPlayerIndex, TRUE)
						PRINTLN("PROCESS_VEHICLE_EXISTENCE_EVENT - Setting MOC cab ", GET_PLAYER_NAME(sEventData.pPropertyOwner), " to exist for: ", GET_PLAYER_NAME(sEventData.Details.FromPlayerIndex))
					ENDIF
					IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(sEventData.pPropertyOwner)].netID_TruckV[1])
					AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(sEventData.pPropertyOwner)].netID_TruckV[1])
						SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(GlobalplayerBD[NATIVE_TO_INT(sEventData.pPropertyOwner)].netID_TruckV[1], sEventData.Details.FromPlayerIndex, TRUE)
						PRINTLN("PROCESS_VEHICLE_EXISTENCE_EVENT - Setting MOC trailer ", GET_PLAYER_NAME(sEventData.pPropertyOwner), " to exist for: ", GET_PLAYER_NAME(sEventData.Details.FromPlayerIndex))
					ENDIF
				ELIF sEventData.bAvenger
					IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(sEventData.pPropertyOwner)].netID_AvengerV)
					AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(sEventData.pPropertyOwner)].netID_AvengerV)
						SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(GlobalplayerBD[NATIVE_TO_INT(sEventData.pPropertyOwner)].netID_AvengerV, sEventData.Details.FromPlayerIndex, TRUE)
						PRINTLN("PROCESS_VEHICLE_EXISTENCE_EVENT - Setting avenger ", GET_PLAYER_NAME(sEventData.pPropertyOwner), " to exist for: ", GET_PLAYER_NAME(sEventData.Details.FromPlayerIndex))
					ENDIF
				ELIF sEventData.bHackerTruck
					IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(sEventData.pPropertyOwner)].netID_HackertruckV)
					AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(sEventData.pPropertyOwner)].netID_HackertruckV)
						SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(GlobalplayerBD[NATIVE_TO_INT(sEventData.pPropertyOwner)].netID_HackertruckV, sEventData.Details.FromPlayerIndex, TRUE)
						PRINTLN("PROCESS_VEHICLE_EXISTENCE_EVENT - Setting hacker truck ", GET_PLAYER_NAME(sEventData.pPropertyOwner), " to exist for: ", GET_PLAYER_NAME(sEventData.Details.FromPlayerIndex))
					ENDIF	
				ELIF sEventData.bSubmarine
					IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(sEventData.pPropertyOwner)].netID_SubmarineV)
					AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(sEventData.pPropertyOwner)].netID_SubmarineV)
						SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(GlobalplayerBD[NATIVE_TO_INT(sEventData.pPropertyOwner)].netID_SubmarineV, sEventData.Details.FromPlayerIndex, TRUE)
						PRINTLN("PROCESS_VEHICLE_EXISTENCE_EVENT - Setting submarine ", GET_PLAYER_NAME(sEventData.pPropertyOwner), " to exist for: ", GET_PLAYER_NAME(sEventData.Details.FromPlayerIndex))
					ENDIF
				#IF FEATURE_DLC_2_2022
				ELIF sEventData.bAcidLab
					IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(sEventData.pPropertyOwner)].netID_AcidLabV)
					AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(sEventData.pPropertyOwner)].netID_AcidLabV)
						SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(GlobalplayerBD[NATIVE_TO_INT(sEventData.pPropertyOwner)].netID_AcidLabV, sEventData.Details.FromPlayerIndex, TRUE)
						PRINTLN("PROCESS_VEHICLE_EXISTENCE_EVENT - Setting acid lab ", GET_PLAYER_NAME(sEventData.pPropertyOwner), " to exist for: ", GET_PLAYER_NAME(sEventData.Details.FromPlayerIndex))
					ENDIF
				#ENDIF
				ELSE
					PRINTLN("PROCESS_VEHICLE_EXISTENCE_EVENT - Failed exist sEventData.bMOC: ", sEventData.bMOC, " sEventData.bAvenger: ", sEventData.bAvenger, " sEventData.bHackerTruck: ", sEventData.bHackerTruck)
				ENDIF
			ELSE
				IF sEventData.pPropertyOwner = sEventData.Details.FromPlayerIndex
					PRINTLN("PROCESS_VEHICLE_EXISTENCE_EVENT - Failed owner is sender vehicle should always exit for owner skip. Owner is: ", GET_PLAYER_NAME(sEventData.Details.FromPlayerIndex))
					EXIT
				ENDIF
				
				IF sEventData.bMOC
					IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(sEventData.pPropertyOwner)].netID_TruckV[0])
					AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(sEventData.pPropertyOwner)].netID_TruckV[0])
						SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(GlobalplayerBD[NATIVE_TO_INT(sEventData.pPropertyOwner)].netID_TruckV[0], sEventData.Details.FromPlayerIndex, FALSE)
						PRINTLN("PROCESS_VEHICLE_EXISTENCE_EVENT - Setting MOC cab ", GET_PLAYER_NAME(sEventData.pPropertyOwner), " to not exist for: ", GET_PLAYER_NAME(sEventData.Details.FromPlayerIndex))
					ENDIF
					IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(sEventData.pPropertyOwner)].netID_TruckV[1])
					AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(sEventData.pPropertyOwner)].netID_TruckV[1])
						SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(GlobalplayerBD[NATIVE_TO_INT(sEventData.pPropertyOwner)].netID_TruckV[1], sEventData.Details.FromPlayerIndex, FALSE)
						PRINTLN("PROCESS_VEHICLE_EXISTENCE_EVENT - Setting MOC trailer ", GET_PLAYER_NAME(sEventData.pPropertyOwner), " to not exist for: ", GET_PLAYER_NAME(sEventData.Details.FromPlayerIndex))
					ENDIF
				ELIF sEventData.bAvenger
					IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(sEventData.pPropertyOwner)].netID_AvengerV)
					AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(sEventData.pPropertyOwner)].netID_AvengerV)
						SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(GlobalplayerBD[NATIVE_TO_INT(sEventData.pPropertyOwner)].netID_AvengerV, sEventData.Details.FromPlayerIndex, FALSE)
						PRINTLN("PROCESS_VEHICLE_EXISTENCE_EVENT - Setting avenger ", GET_PLAYER_NAME(sEventData.pPropertyOwner), " to not exist for: ", GET_PLAYER_NAME(sEventData.Details.FromPlayerIndex))
					ENDIF
				ELIF sEventData.bHackerTruck
					IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(sEventData.pPropertyOwner)].netID_HackertruckV)
					AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(sEventData.pPropertyOwner)].netID_HackertruckV)
						SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(GlobalplayerBD[NATIVE_TO_INT(sEventData.pPropertyOwner)].netID_HackertruckV, sEventData.Details.FromPlayerIndex, FALSE)
						PRINTLN("PROCESS_VEHICLE_EXISTENCE_EVENT - Setting hacker truck ", GET_PLAYER_NAME(sEventData.pPropertyOwner), " to not exist for: ", GET_PLAYER_NAME(sEventData.Details.FromPlayerIndex))
					ENDIF		
				ELIF sEventData.bSubmarine
					IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(sEventData.pPropertyOwner)].netID_SubmarineV)
					AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(sEventData.pPropertyOwner)].netID_SubmarineV)
						SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(GlobalplayerBD[NATIVE_TO_INT(sEventData.pPropertyOwner)].netID_SubmarineV, sEventData.Details.FromPlayerIndex, FALSE)
						PRINTLN("PROCESS_VEHICLE_EXISTENCE_EVENT - Setting submarine ", GET_PLAYER_NAME(sEventData.pPropertyOwner), " to not exist for: ", GET_PLAYER_NAME(sEventData.Details.FromPlayerIndex))
					ENDIF		
				#IF FEATURE_DLC_2_2022
				ELIF sEventData.bAcidLab
					IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(sEventData.pPropertyOwner)].netID_AcidLabV)
					AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(sEventData.pPropertyOwner)].netID_AcidLabV)
						SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(GlobalplayerBD[NATIVE_TO_INT(sEventData.pPropertyOwner)].netID_AcidLabV, sEventData.Details.FromPlayerIndex, FALSE)
						PRINTLN("PROCESS_VEHICLE_EXISTENCE_EVENT - Setting acid lab ", GET_PLAYER_NAME(sEventData.pPropertyOwner), " to not exist for: ", GET_PLAYER_NAME(sEventData.Details.FromPlayerIndex))
					ENDIF	
				#ENDIF	
				ELSE
					PRINTLN("PROCESS_VEHICLE_EXISTENCE_EVENT - Failed not exist sEventData.bMOC: ", sEventData.bMOC, " sEventData.bAvenger: ", sEventData.bAvenger, " sEventData.bHackerTruck: ", sEventData.bHackerTruck)
				ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			IF sEventData.pPropertyOwner = INVALID_PLAYER_INDEX()
				PRINTLN("PROCESS_VEHICLE_EXISTENCE_EVENT - Failed sEventData.pPropertyOwner is invalid")
			ENDIF
			IF sEventData.Details.FromPlayerIndex = INVALID_PLAYER_INDEX()
				PRINTLN("PROCESS_VEHICLE_EXISTENCE_EVENT - Failed sEventData.Details.FromPlayerIndex is invalid")
			ENDIF
			#ENDIF
		ENDIF
	ELSE
		SCRIPT_ASSERT("PROCESS_VEHICLE_EXISTENCE_EVENT - could not retrieve data.")
	ENDIF
	
ENDPROC

PROC VEHICLE_EXISTENCE_EVENT_EVENTS()
	
	INT iCount
	EVENT_NAMES ThisScriptEvent
	STRUCT_EVENT_COMMON_DETAILS Details
	
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount

	    ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)
	   	
	    SWITCH ThisScriptEvent
	        
			CASE EVENT_NETWORK_SCRIPT_EVENT
				
				GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Details, SIZE_OF(Details))
				
				IF Details.Type	= SCRIPT_EVENT_VEHICLE_EXISTENCE
					IF g_sBlockedEvents.bSCRIPT_EVENT_VEHICLE_EXISTENCE
						EXIT
					ENDIF
					PROCESS_VEHICLE_EXISTENCE_EVENT(iCount)
				ENDIF
				
			BREAK
			
	    ENDSWITCH
        
    ENDREPEAT
		
ENDPROC

PROC BROADCAST_LOCAL_ARMORY_VEHICLES_EXIT_SPAWN_POINT(BOOL bBroadCastNow = FALSE)
	IF GET_FRAME_COUNT() % 30 = 0
	OR bBroadCastNow
		IF PLAYER_ID() != INVALID_PLAYER_INDEX()
			VEHICLE_INDEX truckIndex 
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
				truckIndex = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
			ENDIF
			VEHICLE_INDEX vehAvenger 
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV)
				vehAvenger = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV)
			ENDIF
			VEHICLE_INDEX vehHackerTruck 
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV)
				vehHackerTruck = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV)
			ENDIF
			VEHICLE_INDEX vehSubmarine 
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineV)
				vehSubmarine = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineV)
			ENDIF
			VEHICLE_INDEX vehSubmarineDinghy
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineDinghyV)
				vehSubmarineDinghy = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineDinghyV)
			ENDIF
			#IF FEATURE_DLC_2_2022
			VEHICLE_INDEX vehAcidLab 
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV)
				vehAcidLab = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV)
			ENDIF
			#ENDIF
			#IF FEATURE_DLC_2_2022
			VEHICLE_INDEX vehSupportBike
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SupportBikeV)
				vehSupportBike = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SupportBikeV)
			ENDIF
			#ENDIF
			
			IF DOES_ENTITY_EXIST(truckIndex)
			AND NOT IS_ENTITY_DEAD(truckIndex)
			AND NOT IS_PLAYER_TRUCK_DESTROYED(PLAYER_ID())
			AND NOT DO_I_NEED_TO_RENOVATE_TRUCK_CAB(PLAYER_ID())
			AND NOT DO_I_NEED_TO_RENOVATE_TRUCK_TRAILER(PLAYER_ID())
			AND NOT IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(PLAYER_ID())
				IF GET_OWNER_OF_PERSONAL_TRUCK(truckIndex, FALSE, PLAYER_ID()) = PLAYER_ID()
				AND NOT IS_ARMORY_TRUCK_UNDER_MAP(PLAYER_ID())
					GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.vMobileInteriorExitSpawnPoint = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(truckIndex), GET_ENTITY_HEADING(truckIndex), <<2,-17, 0>>)
					GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.fMobileInteriorExitHeading = GET_ENTITY_HEADING(truckIndex)
					//PRINTLN("BROADCAST_LOCAL_ARMORY_VEHICLES_EXIT_SPAWN_POINT MOC: ", GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.vMobileInteriorExitSpawnPoint)
				ENDIF
			ELIF DOES_ENTITY_EXIST(vehAvenger)
			AND NOT IS_ENTITY_DEAD(vehAvenger)
			AND NOT IS_OWNERS_ARMORY_AIRCRAFT_INSIDE_DEFUNCT_BASE(PLAYER_ID())
				IF GET_OWNER_OF_PERSONAL_ARMORY_AIRCRAFT(vehAvenger, FALSE, PLAYER_ID()) = PLAYER_ID()
				AND NOT IS_OWNERS_ARMORY_AIRCRAFT_INSIDE_DEFUNCT_BASE(PLAYER_ID())
				AND NOT IS_MP_SAVED_AVENGER_BEING_CLEANED_UP()
					GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.vArmoryAircraftExitSpawnPoint = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(vehAvenger), GET_ENTITY_HEADING(vehAvenger), <<2,-17, 0>>)
					GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.fArmoryAircraftExitHeading = GET_ENTITY_HEADING(vehAvenger)
					GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].vArmoryAircraftLocation = GET_ENTITY_COORDS(vehAvenger)
					//PRINTLN("BROADCAST_LOCAL_ARMORY_VEHICLES_EXIT_SPAWN_POINT Avenger: ", GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.vArmoryAircraftExitSpawnPoint)
				ENDIF
			ELIF DOES_ENTITY_EXIST(vehHackerTruck)
			AND NOT IS_ENTITY_DEAD(vehHackerTruck)
			AND NOT IS_OWNER_RENOVATED_HACKER_TRUCK(PLAYER_ID())
			AND NOT IS_OWNERS_HACKER_TRUCK_INSIDE_BUSINESS_HUB(PLAYER_ID())
				IF GET_OWNER_OF_PERSONAL_HACKER_TRUCK(vehHackerTruck, FALSE, PLAYER_ID()) = PLAYER_ID()
					GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.vMobileInteriorExitSpawnPoint = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(vehHackerTruck), GET_ENTITY_HEADING(vehHackerTruck), <<3.0,-1, 0>>)
					GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.fMobileInteriorExitHeading = GET_ENTITY_HEADING(vehHackerTruck) + 90
					GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].vHackerTruckLocation = GET_ENTITY_COORDS(vehHackerTruck)
					//PRINTLN("BROADCAST_LOCAL_ARMORY_VEHICLES_EXIT_SPAWN_POINT Hacker Truck: ", GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.vMobileInteriorExitSpawnPoint)
				ENDIF
			ELIF DOES_ENTITY_EXIST(vehSubmarine)
			AND NOT IS_ENTITY_DEAD(vehSubmarine)
				IF GET_OWNER_OF_SMPL_INTERIOR_SUBMARINE(vehSubmarine, FALSE, PLAYER_ID()) = PLAYER_ID()
					GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.vMobileInteriorExitSpawnPoint = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(vehSubmarine), GET_ENTITY_HEADING(vehSubmarine), <<0.0,30.0, 0>>)
					GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.fMobileInteriorExitHeading = GET_ENTITY_HEADING(vehSubmarine)
					GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].vSubmarineLocation = GET_ENTITY_COORDS(vehSubmarine)
					//PRINTLN("BROADCAST_LOCAL_ARMORY_VEHICLES_EXIT_SPAWN_POINT Hacker Truck: ", GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.vMobileInteriorExitSpawnPoint)
				ENDIF
			ELIF DOES_ENTITY_EXIST(vehSubmarineDinghy)
			AND NOT IS_ENTITY_DEAD(vehSubmarineDinghy)
				GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].vSubmarineDinghyLocation = GET_ENTITY_COORDS(vehSubmarineDinghy)
			#IF FEATURE_DLC_2_2022
			ELIF DOES_ENTITY_EXIST(vehAcidLab)
			AND NOT IS_ENTITY_DEAD(vehAcidLab)
			// AND NOT IS_OWNER_RENOVATED_ACID_LAB(PLAYER_ID()) // @TODO_JD add renovation check.
			AND NOT IS_OWNERS_ACID_LAB_INSIDE_JUGGALO_HIDEOUT(PLAYER_ID())
				IF GET_OWNER_OF_PERSONAL_ACID_LAB(vehAcidLab, FALSE, PLAYER_ID()) = PLAYER_ID()
					GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.vMobileInteriorExitSpawnPoint = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(vehAcidLab), GET_ENTITY_HEADING(vehAcidLab), <<3.0,-1, 0>>)
					GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.fMobileInteriorExitHeading = GET_ENTITY_HEADING(vehAcidLab) + 90
					GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].vAcidLablocation = GET_ENTITY_COORDS(vehAcidLab)
					//PRINTLN("BROADCAST_LOCAL_ARMORY_VEHICLES_EXIT_SPAWN_POINT Hacker Truck: ", GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.vMobileInteriorExitSpawnPoint)
				ENDIF
			#ENDIF
			
			#IF FEATURE_DLC_2_2022
			ELIF DOES_ENTITY_EXIST(vehSupportBike)
			AND NOT IS_ENTITY_DEAD(vehSupportBike)
				GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].vSupportBikeLocation = GET_ENTITY_COORDS(vehSupportBike)
			#ENDIF
			ENDIF	
		ENDIF	
	ENDIF	
ENDPROC

/// PURPOSE:
///    Updates g_viGunRunTruckTailerIamIn to the vehicle index of the trailer that the player is currently in
PROC UPDATE_TRUCK_TRAILER_I_AM_IN_VAR()
	IF IS_BIT_SET(iLocalBS, LOCAL_BS_SHOULD_ALLOW_EXPENSIVE_PROCESSING)
		BOOl bFoundGunRunTruckTailerIamIn = FALSE
		IF NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn) != -1
			IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].netID_TruckV[1])
				IF (g_viGunRunTruckTailerIamIn = NULL)
					PRINTLN("UPDATE_TRUCK_TRAILER_I_AM_IN_VAR - found truck trailer I am in, owned by, ", GET_PLAYER_NAME(g_ownerOfArmoryTruckPropertyIAmIn))
				ENDIF
				
				g_viGunRunTruckTailerIamIn = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].netID_TruckV[1])
				bFoundGunRunTruckTailerIamIn = TRUE
			ENDIF
		ENDIF
		
		IF NOT bFoundGunRunTruckTailerIamIn
			IF !(g_viGunRunTruckTailerIamIn = NULL)
				PRINTLN("UPDATE_TRUCK_TRAILER_I_AM_IN_VAR - lost truck trailer I am in")
			ENDIF
			g_viGunRunTruckTailerIamIn = NULL
		ENDIF
		
		BOOl GunRunTruckCabIamIn = FALSE
		PLAYER_INDEX ownerOfArmoryTruckCabIAmIn = INVALID_PLAYER_INDEX()
		
		INT i
		IF ((IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND IS_VEHICLE_ARMORY_TRUCK(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))) OR IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID()))
			REPEAT NUM_NETWORK_PLAYERS i
				IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(i), FALSE)
				AND GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayersTruckImIn = NETWORK_HASH_FROM_PLAYER_HANDLE(INT_TO_PLAYERINDEX(i))
					ownerOfArmoryTruckCabIAmIn = INT_TO_PLAYERINDEX(i)
				ENDIF
			ENDREPEAT
		ENDIF
		
		IF NATIVE_TO_INT(ownerOfArmoryTruckCabIAmIn) != -1
			IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(ownerOfArmoryTruckCabIAmIn)].netID_TruckV[0])
				IF (g_viGunRunTruckCabIamIn = NULL)
					PRINTLN("UPDATE_TRUCK_TRAILER_I_AM_IN_VAR - found truck cab I am in, owned by, ", GET_PLAYER_NAME(ownerOfArmoryTruckCabIAmIn))
				ENDIF
				
				g_viGunRunTruckCabIamIn = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(ownerOfArmoryTruckCabIAmIn)].netID_TruckV[0])
				GunRunTruckCabIamIn = TRUE
			ENDIF			
		ENDIF
		
		IF NOT GunRunTruckCabIamIn
			IF !(g_viGunRunTruckCabIamIn = NULL)
				PRINTLN("UPDATE_TRUCK_TRAILER_I_AM_IN_VAR - lost truck cab I am in")
			ENDIF
			g_viGunRunTruckCabIamIn = NULL
		ENDIF
	ENDIF	
ENDPROC

PROC UPDATE_AVENGER_I_AM_IN_VAR()
	IF IS_BIT_SET(iLocalBS, LOCAL_BS_SHOULD_ALLOW_EXPENSIVE_PROCESSING)
		BOOL bFoundAvengerIamIn = FALSE
		
		IF NATIVE_TO_INT(g_OwnerOfArmoryAircraftPropertyIAmIn) != -1
			IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(g_OwnerOfArmoryAircraftPropertyIAmIn)].netID_AvengerV)
				IF (g_viAvengerIamIn = NULL)
					PRINTLN("UPDATE_AVENGER_I_AM_IN_VAR - found avenger I am in, owned by, ", GET_PLAYER_NAME(g_OwnerOfArmoryAircraftPropertyIAmIn))
				ENDIF
				
				g_viAvengerIamIn = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(g_OwnerOfArmoryAircraftPropertyIAmIn)].netID_AvengerV)
				bFoundAvengerIamIn = TRUE
			ENDIF
		ENDIF
		IF NOT bFoundAvengerIamIn
			IF !(g_viAvengerIamIn = NULL)
				PRINTLN("UPDATE_AVENGER_I_AM_IN_VAR - lost avenger I am in")
			ENDIF
			g_viAvengerIamIn = NULL
		ENDIF
	ENDIF	
ENDPROC

FUNC BOOL IS_PLAYER_IN_OSPREY_DOOR_AREA(PLAYER_INDEX piPlayer, VEHICLE_INDEX vehOsprey, BOOL bEscArea = FALSE)
		
	FLOAT fReversePoints = 1.0
	VECTOR vFrontPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehOsprey, <<0.0,0.0,0.0>>)
	VECTOR vRearPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehOsprey, <<0.0,-15.0,0.0>>)
	
	IF vFrontPos.Z > vRearPos.Z
		fReversePoints = -1.0
	ENDIF
	
	
	VECTOR vArea1
	VECTOR vArea2
	FLOAT fArea3
	
	IF bEscArea
		vArea1 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehOsprey, <<0.0, 0.0, -2.0 * fReversePoints>>)
		IF GET_VEHICLE_DOOR_ANGLE_RATIO(vehOsprey, SC_DOOR_BOOT) = 0.0
			vArea2 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehOsprey, <<0.0, -9.0, 2.0 * fReversePoints>>)
		ELSE
			vArea2 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehOsprey, <<0.0, -12.0, 2.0 * fReversePoints>>)
		ENDIF
		fArea3 = 3.0
	ELSE
		vArea1 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehOsprey, <<0.0, 0.0, -3.0 * fReversePoints>>)
		vArea2 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehOsprey, <<0.0, -15.0, 3.0 * fReversePoints>>)
		fArea3 = 6.0
	ENDIF
	
	RETURN IS_ENTITY_IN_ANGLED_AREA(GET_PLAYER_PED(piPlayer), vArea1, vArea2, fArea3)
ENDFUNC

INT iOspreyOwnerStagger

PROC MAINTAIN_AVENGER_DOOR()
	
	VEHICLE_INDEX vehOsprey
	PLAYER_INDEX piOspreyOwner
	
	// Staggered loop through ospreys
	
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
	
		iOspreyOwnerStagger++
		IF iOspreyOwnerStagger >= NUM_NETWORK_PLAYERS
			iOspreyOwnerStagger = 0
		ENDIF
		
		vehOsprey = GET_LOCAL_ARMORY_AIRCRAFT_VEHICLE(INT_TO_PLAYERINDEX(iOspreyOwnerStagger))
		
		IF DOES_ENTITY_EXIST(vehOsprey)
		AND NOT IS_ENTITY_DEAD(vehOsprey)
			piOspreyOwner = INT_TO_PLAYERINDEX(iOspreyOwnerStagger)
			PRINTLN("[osprey] MAINTAIN_AVENGER_DOOR: Checking player ", NATIVE_TO_INT(piOspreyOwner), "'s osprey.")
			BREAKLOOP
		ELIF i = NUM_NETWORK_PLAYERS - 1
			EXIT
		ENDIF
		
	ENDREPEAT
	
	
	//check door for opening
	 
	IF NETWORK_HAS_CONTROL_OF_ENTITY(vehOsprey)
	
		PRINTLN("[osprey] MAINTAIN_AVENGER_DOOR: Have control of player ", NATIVE_TO_INT(piOspreyOwner), "'s osprey.")
		
		BOOL bOpenDoor = FALSE
		
		REPEAT NUM_NETWORK_PLAYERS i
			IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(i), TRUE)	
			AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(INT_TO_PLAYERINDEX(i))
			AND NOT IS_PED_IN_VEHICLE(GET_PLAYER_PED(piOspreyOwner), vehOsprey, TRUE)
				PRINTLN("[osprey] MAINTAIN_AVENGER_DOOR: Checking player ", i, " for player ", NATIVE_TO_INT(piOspreyOwner), "'s osprey.")
				IF INT_TO_PLAYERINDEX(i) = piOspreyOwner
				OR (GB_IS_PLAYER_MEMBER_OF_A_GANG(piOspreyOwner) AND GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG(INT_TO_PLAYERINDEX(i), piOspreyOwner))
					IF IS_PLAYER_IN_OSPREY_DOOR_AREA(INT_TO_PLAYERINDEX(i), vehOsprey)
						PRINTLN("[osprey] MAINTAIN_AVENGER_DOOR: Player ", i, " has access to this osprey and is in range.")
						bOpenDoor = TRUE
						BREAKLOOP
					ENDIF
					PRINTLN("[osprey] MAINTAIN_AVENGER_DOOR: Player ", i, " has access to this osprey but is not in range.")
				ELIF IS_PLAYER_IN_OSPREY_DOOR_AREA(INT_TO_PLAYERINDEX(i), vehOsprey, TRUE)
					PRINTLN("[osprey] MAINTAIN_AVENGER_DOOR: Player ", i, " doesn't have access to this osprey but is inside.")
					//keep door open so they can escape
					bOpenDoor = TRUE
					BREAKLOOP
				ENDIF
				PRINTLN("[osprey] MAINTAIN_AVENGER_DOOR: Player ", i, " doesn't have access to this osprey.")
			ENDIF
		ENDREPEAT
		IF bOpenDoor
		AND GET_VEHICLE_DOOR_ANGLE_RATIO(vehOsprey, SC_DOOR_BOOT) != 1.0
			SET_VEHICLE_DOOR_OPEN(vehOsprey, SC_DOOR_BOOT)
		ELIF !bOpenDoor
		AND GET_VEHICLE_DOOR_ANGLE_RATIO(vehOsprey, SC_DOOR_BOOT) != 0.0
			SET_VEHICLE_DOOR_SHUT(vehOsprey, SC_DOOR_BOOT, FALSE)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_AVENGER_AUTOPILOT()
	
	IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE)
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),AVENGER)
			VEHICLE_INDEX avengerVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
			IF DOES_ENTITY_EXIST(avengerVeh)
			AND NOT IS_ENTITY_DEAD(avengerVeh)
			AND (IS_VEHICLE_A_PERSONAL_ARMORY_AIRCRAFT(avengerVeh) OR IS_VEHICLE_A_CREATOR_AIRCRAFT(avengerVeh))
			AND NOT IS_ARMORY_AIRCRAFT_PILOT_ENTERING()
				IF DOES_PLAYER_NEED_TO_ACTIVATE_AVENGER_AUTOPILOT(PLAYER_ID())
					
					PRINTLN("MAINTAIN_AVENGER_AUTOPILOT: ready to autopilot")
					
					IF NOT IS_PAUSE_MENU_ACTIVE_EX()
					AND NOT IS_CUSTOM_MENU_ON_SCREEN()
					AND GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
						IF NETWORK_HAS_CONTROL_OF_ENTITY(avengerVeh)
							SET_VEHICLE_FLIGHT_NOZZLE_POSITION(avengerVeh,1)
							VECTOR vAvengerCoords = GET_ENTITY_COORDS(avengerVeh)
							VECTOR vForwardCoord = vAvengerCoords + (GET_ENTITY_FORWARD_VECTOR(avengerVeh) * 10.0)
							IF NOT IS_ENTITY_DEAD(avengerVeh)
								IF GET_VEHICLE_FLIGHT_NOZZLE_POSITION(avengerVeh) = 1.0
									TASK_PLANE_GOTO_PRECISE_VTOL(PLAYER_PED_ID(), avengerVeh, vForwardCoord, ROUND(vAvengerCoords.z) , 30, TRUE, GET_ENTITY_HEADING(avengerVeh), TRUE)
									SET_DISABLE_SUPERDUMMY(avengerVeh, TRUE)
									SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(avengerVeh, TRUE)
									SET_PLAYER_NEED_TO_ACTIVATE_AVENGER_AUTOPILOT(FALSE)
									SET_DISABLE_VERTICAL_FLIGHT_MODE_TRANSITION(avengerVeh, TRUE)
									IF !NETWORK_IS_ACTIVITY_SESSION()
										BROADCAST_EVENT_AVENGER_AUTOPILOT_ACTIVATED(GET_OWNER_OF_PERSONAL_ARMORY_AIRCRAFT(avengerVeh), TRUE)
									ELSE
										BROADCAST_EVENT_AVENGER_AUTOPILOT_ACTIVATED(GET_OWNER_OF_CREATOR_AIRCRAFT(avengerVeh), TRUE)
									ENDIF
									IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AOC_DEAUTO_ACT")
										CLEAR_HELP()
									ENDIF
									IF IS_BIT_SET(iLocalBs, LOCAL_BS_DEACTIVATE_AUTO_PILOT_HELP_TEXT)
										CLEAR_BIT(iLocalBs, LOCAL_BS_DEACTIVATE_AUTO_PILOT_HELP_TEXT)
									ENDIF
									PRINTLN("MAINTAIN_AVENGER_AUTOPILOT: Player activated autopilot")
								ENDIF
							ENDIF	
						ELSE
							NETWORK_REQUEST_CONTROL_OF_ENTITY(avengerVeh)
						ENDIF
					ENDIF
			
				ENDIF
				
				IF !NETWORK_IS_ACTIVITY_SESSION()
					IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
						IF IS_BIT_SET(iLocalBs, LOCAL_BS_DEACTIVATE_AUTO_PILOT_HELP_TEXT)
						AND GET_OWNER_OF_PERSONAL_ARMORY_AIRCRAFT(avengerVeh) != PLAYER_ID()
							
							IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
								PRINT_HELP("AOC_DEAUTO_ACT")
								CLEAR_BIT(iLocalBs, LOCAL_BS_DEACTIVATE_AUTO_PILOT_HELP_TEXT)
								PRINTLN("MAINTAIN_AVENGER_AUTOPILOT: LOCAL_BS_DEACTIVATE_AUTO_PILOT_HELP_TEXT print help")
							ENDIF
						ENDIF
					ELSE
						IF IS_BIT_SET(iLocalBs, LOCAL_BS_DEACTIVATE_AUTO_PILOT_HELP_TEXT)
							CLEAR_BIT(iLocalBs, LOCAL_BS_DEACTIVATE_AUTO_PILOT_HELP_TEXT)
						ENDIF	
					ENDIF
				ENDIF	
				
				IF DOES_PLAYER_NEED_TO_DEACTIVATE_AVENGER_AUTOPILOT(PLAYER_ID())
				AND GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
					PRINTLN("MAINTAIN_AVENGER_AUTOPILOT: ready to de-activate")
					IF !IS_BIT_SET(iLocalBs, LOCAL_BS_DEACTIVATE_AUTO_PILOT_HELP_TEXT)
						SET_BIT(iLocalBs, LOCAL_BS_DEACTIVATE_AUTO_PILOT_HELP_TEXT)
						PRINTLN("MAINTAIN_AVENGER_AUTOPILOT: LOCAL_BS_DEACTIVATE_AUTO_PILOT_HELP_TEXT TRUE")
					ENDIF
					IF NETWORK_HAS_CONTROL_OF_ENTITY(avengerVeh)
						SET_VEHICLE_FLIGHT_NOZZLE_POSITION(avengerVeh,1)
						SET_DISABLE_SUPERDUMMY(avengerVeh, FALSE)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(avengerVeh, FALSE)
						CLEAR_PRIMARY_VEHICLE_TASK(avengerVeh)
						SET_PLAYER_NEED_TO_DEACTIVATE_AVENGER_AUTOPILOT(FALSE)
						SET_DISABLE_VERTICAL_FLIGHT_MODE_TRANSITION(avengerVeh, FALSE)
						IF !NETWORK_IS_ACTIVITY_SESSION()
							BROADCAST_EVENT_AVENGER_AUTOPILOT_ACTIVATED(GET_OWNER_OF_PERSONAL_ARMORY_AIRCRAFT(avengerVeh), FALSE)
						ELSE
							BROADCAST_EVENT_AVENGER_AUTOPILOT_ACTIVATED(GET_OWNER_OF_CREATOR_AIRCRAFT(avengerVeh), FALSE)
						ENDIF	
						IF IS_BIT_SET(iLocalBs, LOCAL_BS_AUTO_PILOT_HELP_TEXT)
							CLEAR_BIT(iLocalBs, LOCAL_BS_AUTO_PILOT_HELP_TEXT)
						ENDIF	
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AOC_AUTO_ACT")
							CLEAR_HELP()
						ENDIF
						PRINTLN("MAINTAIN_AVENGER_AUTOPILOT: Player de-activated autopilot")	
					ELSE
						NETWORK_REQUEST_CONTROL_OF_ENTITY(avengerVeh)
					ENDIF
						
				ENDIF
				
				IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
				AND NOT DOES_PLAYER_NEED_TO_DEACTIVATE_AVENGER_AUTOPILOT(PLAYER_ID())
					IF NETWORK_IS_ACTIVITY_SESSION()
						IF IS_ARMORY_AIRCRAFT_AUTOPILOT_ACTIVE(PLAYER_ID())
							IF NETWORK_HAS_CONTROL_OF_ENTITY(avengerVeh)
								SET_DISABLE_VERTICAL_FLIGHT_MODE_TRANSITION(avengerVeh, TRUE)
							ELSE
								NETWORK_REQUEST_CONTROL_OF_ENTITY(avengerVeh)
							ENDIF
						ELSE
							IF NETWORK_HAS_CONTROL_OF_ENTITY(avengerVeh)
								SET_DISABLE_VERTICAL_FLIGHT_MODE_TRANSITION(avengerVeh, FALSE)
							ELSE
								NETWORK_REQUEST_CONTROL_OF_ENTITY(avengerVeh)
							ENDIF
						ENDIF
					ELSE
						IF IS_ARMORY_AIRCRAFT_AUTOPILOT_ACTIVE(GET_OWNER_OF_PERSONAL_ARMORY_AIRCRAFT(avengerVeh))
							IF NETWORK_HAS_CONTROL_OF_ENTITY(avengerVeh)
								SET_DISABLE_VERTICAL_FLIGHT_MODE_TRANSITION(avengerVeh, TRUE)
								//PRINTLN("MAINTAIN_AVENGER_AUTOPILOT: SET_DISABLE_VERTICAL_FLIGHT_MODE_TRANSITION true")
							ELSE
								NETWORK_REQUEST_CONTROL_OF_ENTITY(avengerVeh)
							ENDIF
							IF PLAYER_ID() != GET_OWNER_OF_PERSONAL_ARMORY_AIRCRAFT(avengerVeh)
								IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
									IF NOT IS_BIT_SET(iLocalBs, LOCAL_BS_AUTO_PILOT_HELP_TEXT)
										PRINT_HELP("AOC_AUTO_ACT")
										SET_BIT(iLocalBs, LOCAL_BS_AUTO_PILOT_HELP_TEXT)
									ENDIF	
								ENDIF
							ENDIF
						ELSE
							IF NETWORK_HAS_CONTROL_OF_ENTITY(avengerVeh)
								SET_DISABLE_VERTICAL_FLIGHT_MODE_TRANSITION(avengerVeh, FALSE)
							ELSE
								NETWORK_REQUEST_CONTROL_OF_ENTITY(avengerVeh)
							ENDIF
							IF IS_BIT_SET(iLocalBs, LOCAL_BS_AUTO_PILOT_HELP_TEXT)
								CLEAR_BIT(iLocalBs, LOCAL_BS_AUTO_PILOT_HELP_TEXT)
							ENDIF
						ENDIF
					ENDIF
				ENDIF	
			ENDIF
		ELSE
			IF IS_BIT_SET(iLocalBs, LOCAL_BS_AUTO_PILOT_HELP_TEXT)
				CLEAR_BIT(iLocalBs, LOCAL_BS_AUTO_PILOT_HELP_TEXT)
			ENDIF
			IF IS_BIT_SET(iLocalBs, LOCAL_BS_DEACTIVATE_AUTO_PILOT_HELP_TEXT)
				CLEAR_BIT(iLocalBs, LOCAL_BS_DEACTIVATE_AUTO_PILOT_HELP_TEXT)
			ENDIF
		ENDIF	
	ELSE
		IF IS_BIT_SET(iLocalBs, LOCAL_BS_AUTO_PILOT_HELP_TEXT)
			CLEAR_BIT(iLocalBs, LOCAL_BS_AUTO_PILOT_HELP_TEXT)
		ENDIF
		IF IS_BIT_SET(iLocalBs, LOCAL_BS_DEACTIVATE_AUTO_PILOT_HELP_TEXT)
			CLEAR_BIT(iLocalBs, LOCAL_BS_DEACTIVATE_AUTO_PILOT_HELP_TEXT)
		ENDIF
	ENDIF
ENDPROC

SCRIPT_TIMER sAllowEnter
FUNC BOOL SHOULD_ALLOW_ENTER_AVENGER_HOLD_FROM_PILOT_SEAT_WHEN_LANDED(VEHICLE_INDEX avengerVeh)
	
	IF NOT DOES_ENTITY_EXIST(avengerVeh)
		IF HAS_NET_TIMER_STARTED(sAllowEnter)
			RESET_NET_TIMER(sAllowEnter)
			#IF IS_DEBUG_BUILD
			IF bPrintAvengerAutoPilot
				PRINTLN("MAINTAIN_ENTER_AVENGER_INTERIOR_FROM_PILOT_SEAT SHOULD_ALLOW_ENTER_AVENGER_HOLD_FROM_PILOT_SEAT_WHEN_LANDED RESET_NET_TIMER 4")
			ENDIF
			#ENDIF
		ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR()
	OR IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
	OR IS_PLAYER_IN_CREATOR_AIRCRAFT(PLAYER_ID())
		RESET_NET_TIMER(sAllowEnter)
		#IF IS_DEBUG_BUILD
		IF bPrintAvengerAutoPilot
			PRINTLN("MAINTAIN_ENTER_AVENGER_INTERIOR_FROM_PILOT_SEAT SHOULD_ALLOW_ENTER_AVENGER_HOLD_FROM_PILOT_SEAT_WHEN_LANDED RESET_NET_TIMER 3")
		ENDIF
		#ENDIF
	ENDIF
	
	IF IS_VEHICLE_ON_ALL_WHEELS(avengerVeh)
	AND GET_ENTITY_SPEED(avengerVeh) <= 0.1
	AND (HAS_NET_TIMER_EXPIRED(sAllowEnter, 2500) OR NOT HAS_NET_TIMER_STARTED(sAllowEnter))
		RETURN TRUE
	ELSE
		IF NOT HAS_NET_TIMER_STARTED(sAllowEnter)
			START_NET_TIMER(sAllowEnter)
		ELSE
			IF HAS_NET_TIMER_EXPIRED(sAllowEnter, 2500)
				RESET_NET_TIMER(sAllowEnter)
				#IF IS_DEBUG_BUILD
				IF bPrintAvengerAutoPilot
					PRINTLN("MAINTAIN_ENTER_AVENGER_INTERIOR_FROM_PILOT_SEAT SHOULD_ALLOW_ENTER_AVENGER_HOLD_FROM_PILOT_SEAT_WHEN_LANDED RESET_NET_TIMER 2")
				ENDIF
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(sAllowEnter)
		IF HAS_NET_TIMER_EXPIRED(sAllowEnter, 2500)
			RESET_NET_TIMER(sAllowEnter)
			#IF IS_DEBUG_BUILD
			IF bPrintAvengerAutoPilot
				PRINTLN("MAINTAIN_ENTER_AVENGER_INTERIOR_FROM_PILOT_SEAT SHOULD_ALLOW_ENTER_AVENGER_HOLD_FROM_PILOT_SEAT_WHEN_LANDED RESET_NET_TIMER 1")
			ENDIF
			#ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEAR_SEAT_SHUFFLE()
	IF IS_ARMORY_AIRCRAFT_PILOT_ENTERING()
	OR IS_PLAYER_ENTERING_AIRCRAFT_FROM_BASE_COPILOT_SEAT()
		IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_CLEAR_SEAT_SHUFFLE)
			IF IS_SCREEN_FADED_OUT()
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), 0, ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP | ECF_WARP_PED)
				ELSE
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					SET_BIT(iLocalBS, LOCAL_BS_CLEAR_SEAT_SHUFFLE)
					PRINTLN("CLEAR_SEAT_SHUFFLE ...")
				ENDIF
			ENDIF
		ENDIF	
	ELSE
		IF IS_BIT_SET(iLocalBS, LOCAL_BS_CLEAR_SEAT_SHUFFLE)
			CLEAR_BIT(iLocalBS, LOCAL_BS_CLEAR_SEAT_SHUFFLE)
		ENDIF	
	ENDIF
ENDPROC

VEHICLE_INDEX cockPitVehIndex
INT iCockpitSoundID = -1
PROC UPDATE_COCKPIT_CAMERA(VEHICLE_INDEX pedVeh)

	cockPitVehIndex = pedVeh
	
	IF !DOES_CAM_EXIST(avengerCockPitCam)
		IF iCockpitSoundID != -1
			IF IS_AUDIO_SCENE_ACTIVE("DLC_XM_Vehicle_Interior_Security_Camera_Sounds")
				STOP_SOUND(iCockpitSoundID)
				STOP_AUDIO_SCENE("DLC_XM_Vehicle_Interior_Security_Camera_Sounds")
				PRINTLN("UPDATE_COCKPIT_CAMERA STOP_SOUND(DLC_XM_Vehicle_Interior_Security_Camera_Sounds) - as camera doesn't exist and should only run for the cutscene ")
				iCockpitSoundID = -1
			ENDIF
		ENDIF
		EXIT
	ENDIF

	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF HAS_PLAYER_STARTED_AVENGER_SEAT_SHUFFLE(PLAYER_ID())
		OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT) = WAITING_TO_START_TASK
		OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT) = PERFORMING_TASK
		OR IS_LOCAL_PLAYER_WALKING_INTO_THIS_SIMPLE_INTERIOR(SIMPLE_INTERIOR_ARMORY_AIRCRAFT_1)
			IF NOT IS_AUDIO_SCENE_ACTIVE("dlc_xm_avenger_interior_scene")
				START_AUDIO_SCENE("dlc_xm_avenger_interior_scene")
			ELSE
				IF iCockpitSoundID = -1
					iCockpitSoundID = GET_SOUND_ID()
					PLAY_SOUND_FRONTEND(iCockpitSoundID, "Background_Hum", "DLC_XM_Vehicle_Interior_Security_Camera_Sounds", TRUE)
					PRINTLN("UPDATE_COCKPIT_CAMERA - PLAY_SOUND_FRONTEND(Background_Hum, DLC_XM_Vehicle_Interior_Security_Camera_Sounds) ")
				ENDIF	
			ENDIF	
			ATTACH_CAM_TO_VEHICLE_BONE(avengerCockPitCam, cockPitVehIndex, GET_ENTITY_BONE_INDEX_BY_NAME(cockPitVehIndex, "siren4"), TRUE, << -14.617 , -0.562 , 358.630 >>, <<-0.100, 0.205, 2.970>>, TRUE)
			SET_CAM_FOV(avengerCockPitCam, 61.600)
			RENDER_SCRIPT_CAMS(TRUE, FALSE, 0 , TRUE, TRUE)
			SET_TIMECYCLE_MODIFIER("CAMERA_secuirity")
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			SET_PLAYER_VISIBLE_LOCALLY(PLAYER_ID(), TRUE)
		ENDIF
	ELSE
		IF iCockpitSoundID != -1
			IF IS_AUDIO_SCENE_ACTIVE("DLC_XM_Vehicle_Interior_Security_Camera_Sounds")
				STOP_SOUND(iCockpitSoundID)
				STOP_AUDIO_SCENE("DLC_XM_Vehicle_Interior_Security_Camera_Sounds")
				PRINTLN("UPDATE_COCKPIT_CAMERA STOP_SOUND(DLC_XM_Vehicle_Interior_Security_Camera_Sounds) - as player is not ok ")
				iCockpitSoundID = -1
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_ANY_PASSENGER_ENTERING_AVENGER_HOLD(VEHICLE_INDEX avengerVeh)
		
	PED_INDEX driverPed = GET_PED_IN_VEHICLE_SEAT(avengerVeh)
	PED_INDEX passengerPed = GET_PED_IN_VEHICLE_SEAT(avengerVeh, VS_FRONT_RIGHT)
	IF DOES_ENTITY_EXIST(driverPed)
	AND NOT IS_ENTITY_DEAD(driverPed)
		IF driverPed != PLAYER_PED_ID()
			IF GET_SCRIPT_TASK_STATUS(driverPed, SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT) = WAITING_TO_START_TASK
			OR GET_SCRIPT_TASK_STATUS(driverPed, SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT) = PERFORMING_TASK
				PRINTLN("MAINTAIN_ENTER_AVENGER_INTERIOR_FROM_PILOT_SEAT IS_ANY_PASSENGER_ENTERING_AVENGER_HOLD driver moving")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(passengerPed)
	AND NOT IS_ENTITY_DEAD(passengerPed)
		IF passengerPed != PLAYER_PED_ID()
			IF GET_SCRIPT_TASK_STATUS(passengerPed, SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT) = WAITING_TO_START_TASK
			OR GET_SCRIPT_TASK_STATUS(passengerPed, SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT) = PERFORMING_TASK
				PRINTLN("MAINTAIN_ENTER_AVENGER_INTERIOR_FROM_PILOT_SEAT IS_ANY_PASSENGER_ENTERING_AVENGER_HOLD passenger moving")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEANUP_SCRIPTED_CAMERA_ENTERING_BACK()

	IF DOES_CAM_EXIST(avengerCockPitCam)
	AND IS_CAM_RENDERING(avengerCockPitCam)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		DESTROY_CAM(avengerCockPitCam)
		CLEAR_TIMECYCLE_MODIFIER()
		SET_PLAYER_ENTERING_ARMORY_AIRCRAFT_INTERIOR_FROM_PILOT_SEAT(FALSE)
		SET_PLAYER_ENTER_AIRCRAFT_FROM_BASE_COPILOT_SEAT(FALSE)
		cockPitVehIndex = NULL
		PRINTLN("CLEANUP_SCRIPTED_CAMERA_ENTERING_BACK - turn off camera ")
	ENDIF

ENDPROC

INT iTimer
FUNC BOOL IS_ANY_PED_ENTERING_AVENGER(VEHICLE_INDEX avengerIndex)
	iTimer = 2000
	
	IF IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(PLAYER_ID())
	AND NOT IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE)
		RETURN FALSE
	ENDIF 
	
	PLAYER_INDEX staggerPed = INT_TO_PLAYERINDEX(iRemoteVehicleStagger)
	
	IF IS_NET_PLAYER_OK(staggerPed)
	AND DOES_ENTITY_EXIST(avengerIndex)
	AND NOT IS_ENTITY_DEAD(avengerIndex)
	AND staggerPed  != PLAYER_ID()
		IF VDIST2(GET_ENTITY_COORDS(avengerIndex, FALSE), GET_ENTITY_COORDS(GET_PLAYER_PED(staggerPed))) < 100 //GET_DISTANCE_BETWEEN_ENTITIES(avengerIndex, ) < 10
		AND NOT IS_REMOTE_PLAYER_IN_NON_CLONED_VEHICLE(staggerPed)
			IF GET_VEHICLE_PED_IS_TRYING_TO_ENTER(GET_PLAYER_PED(staggerPed)) = NATIVE_TO_INT(avengerIndex)
			OR GET_VEHICLE_PED_IS_ENTERING(GET_PLAYER_PED(staggerPed)) = avengerIndex
				iTimer = 7000
				RETURN TRUE
			ENDIF	
		ENDIF	
	ENDIF

	RETURN FALSE
ENDFUNC

PLAYER_INDEX pedEnteringHackerTruck
FUNC BOOL PROCESS_ENTERING_HACKER_TRUCK_CAB_CHECK()

	IF IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
	AND NOT IS_OWNERS_HACKER_TRUCK_INSIDE_BUSINESS_HUB(g_OwnerOfHackerTruckPropertyIAmIn)
		PLAYER_INDEX staggerPed = INT_TO_PLAYERINDEX(iRemoteVehicleStagger)
		
		IF IS_NET_PLAYER_OK(pedEnteringHackerTruck)
			IF IS_ENTITY_ALIVE(g_viHackerTruckIamIn) 
			AND GET_DISTANCE_BETWEEN_ENTITIES(g_viHackerTruckIamIn, GET_PLAYER_PED(pedEnteringHackerTruck)) < 5
				PRINTLN("PROCESS_ENTERING_HACKER_TRUCK_CAB_CHECK pedEnteringHackerTruck")
				IF (GET_VEHICLE_PED_IS_TRYING_TO_ENTER(GET_PLAYER_PED(pedEnteringHackerTruck)) = NATIVE_TO_INT(g_viHackerTruckIamIn)
				OR GET_VEHICLE_PED_IS_ENTERING(GET_PLAYER_PED(pedEnteringHackerTruck)) = g_viHackerTruckIamIn)
					SET_SOMEONE_TRYING_TO_ENTER_HACKER_TRUCK_CAB(TRUE)
					RETURN TRUE
				ENDIF	
			ELSE
				pedEnteringHackerTruck = INT_TO_NATIVE(PLAYER_INDEX, -1)
			ENDIF
		ENDIF
		
		IF IS_NET_PLAYER_OK(staggerPed)
		AND DOES_ENTITY_EXIST(g_viHackerTruckIamIn)
		AND NOT IS_ENTITY_DEAD(g_viHackerTruckIamIn)
		AND staggerPed  != PLAYER_ID()
			IF GET_DISTANCE_BETWEEN_ENTITIES(g_viHackerTruckIamIn, GET_PLAYER_PED(staggerPed)) < 5
				PRINTLN("PROCESS_ENTERING_HACKER_TRUCK_CAB_CHECK staggerPed")
				IF GET_VEHICLE_PED_IS_TRYING_TO_ENTER(GET_PLAYER_PED(staggerPed)) = NATIVE_TO_INT(g_viHackerTruckIamIn)
				OR GET_VEHICLE_PED_IS_ENTERING(GET_PLAYER_PED(staggerPed)) = g_viHackerTruckIamIn
					pedEnteringHackerTruck = staggerPed
					SET_SOMEONE_TRYING_TO_ENTER_HACKER_TRUCK_CAB(TRUE)
					RETURN TRUE
				ENDIF	
			ENDIF	
		ENDIF
	ENDIF
	
	SET_SOMEONE_TRYING_TO_ENTER_HACKER_TRUCK_CAB(FALSE)
	RETURN FALSE
ENDFUNC

#IF FEATURE_DLC_2_2022
PLAYER_INDEX pedEnteringAcidLab
FUNC BOOL PROCESS_ENTERING_ACID_LAB_CAB_CHECK()

	IF IS_PLAYER_IN_ACID_LAB(PLAYER_ID())
	AND NOT IS_OWNERS_ACID_LAB_INSIDE_JUGGALO_HIDEOUT(g_OwnerOfAcidLabPropertyIAmIn)
		PLAYER_INDEX staggerPed = INT_TO_PLAYERINDEX(iRemoteVehicleStagger)
		
		IF IS_NET_PLAYER_OK(pedEnteringAcidLab)
			IF IS_ENTITY_ALIVE(g_viAcidLabIamIn) 
			AND GET_DISTANCE_BETWEEN_ENTITIES(g_viAcidLabIamIn, GET_PLAYER_PED(pedEnteringAcidLab)) < 5
				PRINTLN("PROCESS_ENTERING_ACID_LAB_CAB_CHECK pedEnteringAcidLab")
				IF (GET_VEHICLE_PED_IS_TRYING_TO_ENTER(GET_PLAYER_PED(pedEnteringAcidLab)) = NATIVE_TO_INT(g_viAcidLabIamIn)
				OR GET_VEHICLE_PED_IS_ENTERING(GET_PLAYER_PED(pedEnteringAcidLab)) = g_viAcidLabIamIn)
					SET_SOMEONE_TRYING_TO_ENTER_ACID_LAB_CAB(TRUE)
					RETURN TRUE
				ENDIF	
			ELSE
				pedEnteringAcidLab = INT_TO_NATIVE(PLAYER_INDEX, -1)
			ENDIF
		ENDIF
		
		IF IS_NET_PLAYER_OK(staggerPed)
		AND DOES_ENTITY_EXIST(g_viAcidLabIamIn)
		AND NOT IS_ENTITY_DEAD(g_viAcidLabIamIn)
		AND staggerPed  != PLAYER_ID()
			IF GET_DISTANCE_BETWEEN_ENTITIES(g_viAcidLabIamIn, GET_PLAYER_PED(staggerPed)) < 5
				PRINTLN("PROCESS_ENTERING_ACID_LAB_CAB_CHECK staggerPed")
				IF GET_VEHICLE_PED_IS_TRYING_TO_ENTER(GET_PLAYER_PED(staggerPed)) = NATIVE_TO_INT(g_viAcidLabIamIn)
				OR GET_VEHICLE_PED_IS_ENTERING(GET_PLAYER_PED(staggerPed)) = g_viAcidLabIamIn
					pedEnteringAcidLab = staggerPed
					SET_SOMEONE_TRYING_TO_ENTER_ACID_LAB_CAB(TRUE)
					RETURN TRUE
				ENDIF	
			ENDIF	
		ENDIF
	ENDIF
	
	SET_SOMEONE_TRYING_TO_ENTER_ACID_LAB_CAB(FALSE)
	RETURN FALSE
ENDFUNC
#ENDIF

SCRIPT_TIMER sBlockEnterAvengerFromPilotSeat
SCRIPT_TIMER sSeatShuffleTaskTimer
PROC MAINTAIN_ENTER_AVENGER_INTERIOR_FROM_PILOT_SEAT()
	
	IF !IS_BIT_SET(iLocalBS, LOCAL_BS_SHOULD_ALLOW_EXPENSIVE_PROCESSING)
		EXIT
	ENDIF
	
	IF IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_CONCEAL_PLAYER_FOR_PILOT_SEAT_TRANS(FALSE)
		SET_PLAYER_ENTER_AIRCRAFT_FROM_BASE_COPILOT_SEAT(FALSE)
		SET_PLAYER_ENTERING_ARMORY_AIRCRAFT_INTERIOR_FROM_PILOT_SEAT(FALSE)
		
		IF(iCockpitSoundID!= -1)
			IF IS_AUDIO_SCENE_ACTIVE("dlc_xm_avenger_interior_scene")
				STOP_SOUND(iCockpitSoundID)
				STOP_AUDIO_SCENE("dlc_xm_avenger_interior_scene")
				iCockpitSoundID = -1
			ENDIF
		ENDIF
		
		CLEANUP_SCRIPTED_CAMERA_ENTERING_BACK()
		PRINTLN("MAINTAIN_ENTER_AVENGER_INTERIOR_FROM_PILOT_SEAT - Player is dead ")
	ENDIF	
	
	IF !IS_NET_PLAYER_OK(PLAYER_ID())
		SET_CONCEAL_PLAYER_FOR_PILOT_SEAT_TRANS(FALSE)
		SET_PLAYER_ENTER_AIRCRAFT_FROM_BASE_COPILOT_SEAT(FALSE)
		SET_PLAYER_ENTERING_ARMORY_AIRCRAFT_INTERIOR_FROM_PILOT_SEAT(FALSE)
		SET_PLAYER_STARTED_AVENGER_SEAT_SHUFFLE(FALSE)
		IF IS_BIT_SET(iLocalBS, LOCAL_BS_CLEAR_SEAT_SHUFFLE)
			CLEAR_BIT(iLocalBS, LOCAL_BS_CLEAR_SEAT_SHUFFLE)
		ENDIF
		EXIT
	ENDIF
	
	IF HAS_PLAYER_STARTED_AVENGER_SEAT_SHUFFLE(PLAYER_ID())
		SET_BIT(iLocalBS, LOCAL_BS_HAS_PLAYER_STARTED_AVENGER_SEAT_SHUFFLE)
	ELSE
		CLEAR_BIT(iLocalBS, LOCAL_BS_HAS_PLAYER_STARTED_AVENGER_SEAT_SHUFFLE)
	ENDIF
	
	BOOL bShowHelpCheckForEntry = FALSE
	VEHICLE_INDEX pedVeh 
	MODEL_NAMES ePedVehicleModel
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	
		pedVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF DOES_ENTITY_EXIST(pedVeh)
		AND IS_VEHICLE_DRIVEABLE(pedVeh)
			ePedVehicleModel = GET_ENTITY_MODEL(pedVeh)
		ENDIF
		
		IF ePedVehicleModel = AVENGER
			IF NOT IS_PED_TRYING_TO_ENTER_OR_EXIT_VEHICLE(PLAYER_PED_ID(), FALSE)
				IF (IS_VEHICLE_A_PERSONAL_ARMORY_AIRCRAFT(pedVeh) OR IS_VEHICLE_A_CREATOR_AIRCRAFT(pedVeh))
				AND NOT HAS_OWNER_EMPTY_AVENGER_HOLD(GET_OWNER_OF_PERSONAL_ARMORY_AIRCRAFT(pedVeh))
				AND NOT GET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_IsSeatShuffling)
				AND NOT IS_ARMORY_AIRCRAFT_PILOT_ENTERING()
				AND NOT IS_ENTITY_ON_FIRE(pedVeh)
					IF GET_OWNER_OF_PERSONAL_ARMORY_AIRCRAFT(pedVeh) = PLAYER_ID()	
					AND	NOT NETWORK_IS_ACTIVITY_SESSION()
						IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
							IF IS_ARMORY_AIRCRAFT_AUTOPILOT_ACTIVE(PLAYER_ID())
							OR SHOULD_ALLOW_ENTER_AVENGER_HOLD_FROM_PILOT_SEAT_WHEN_LANDED(pedVeh)
								bShowHelpCheckForEntry = TRUE
								#IF IS_DEBUG_BUILD
								IF bPrintAvengerAutoPilot
									PRINTLN("MAINTAIN_ENTER_AVENGER_INTERIOR_FROM_PILOT_SEAT bShowHelpCheckForEntry 1")
								ENDIF
								#ENDIF
							ENDIF
						ELSE
							bShowHelpCheckForEntry = TRUE	
							#IF IS_DEBUG_BUILD
							IF bPrintAvengerAutoPilot
								PRINTLN("MAINTAIN_ENTER_AVENGER_INTERIOR_FROM_PILOT_SEAT bShowHelpCheckForEntry 2")
							ENDIF
							#ENDIF
						ENDIF
					ELSE	
						IF (GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
						AND GB_GET_LOCAL_PLAYER_GANG_BOSS() = GET_OWNER_OF_PERSONAL_ARMORY_AIRCRAFT(pedVeh))
						OR NETWORK_IS_ACTIVITY_SESSION()
							IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
								IF NOT NETWORK_IS_ACTIVITY_SESSION()
									IF IS_ARMORY_AIRCRAFT_AUTOPILOT_ACTIVE(GB_GET_LOCAL_PLAYER_GANG_BOSS())
									OR SHOULD_ALLOW_ENTER_AVENGER_HOLD_FROM_PILOT_SEAT_WHEN_LANDED(pedVeh)
										bShowHelpCheckForEntry = TRUE
										#IF IS_DEBUG_BUILD
										IF bPrintAvengerAutoPilot
											PRINTLN("MAINTAIN_ENTER_AVENGER_INTERIOR_FROM_PILOT_SEAT bShowHelpCheckForEntry 3") 
										ENDIF
										#ENDIF	
									ENDIF
								ELSE
									IF IS_ARMORY_AIRCRAFT_AUTOPILOT_ACTIVE(PLAYER_ID())
									OR SHOULD_ALLOW_ENTER_AVENGER_HOLD_FROM_PILOT_SEAT_WHEN_LANDED(pedVeh)
										bShowHelpCheckForEntry = TRUE
										#IF IS_DEBUG_BUILD
										IF bPrintAvengerAutoPilot
											PRINTLN("MAINTAIN_ENTER_AVENGER_INTERIOR_FROM_PILOT_SEAT bShowHelpCheckForEntry 4") 
										ENDIF
										#ENDIF
									ENDIF
								ENDIF	
							ELSE
								bShowHelpCheckForEntry = TRUE	
								#IF IS_DEBUG_BUILD
								IF bPrintAvengerAutoPilot
									PRINTLN("MAINTAIN_ENTER_AVENGER_INTERIOR_FROM_PILOT_SEAT bShowHelpCheckForEntry 5")
								ENDIF
								#ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF NOT NETWORK_IS_ACTIVITY_SESSION()
						IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
							IF NOT IS_ARMORY_AIRCRAFT_AUTOPILOT_ACTIVE(GET_OWNER_OF_PERSONAL_ARMORY_AIRCRAFT(pedVeh))
							AND NOT SHOULD_ALLOW_ENTER_AVENGER_HOLD_FROM_PILOT_SEAT_WHEN_LANDED(pedVeh)
								bShowHelpCheckForEntry = FALSE
								#IF IS_DEBUG_BUILD
								IF bPrintAvengerAutoPilot
									PRINTLN("MAINTAIN_ENTER_AVENGER_INTERIOR_FROM_PILOT_SEAT bShowHelpCheckForEntry FALSE 2")
								ENDIF
								#ENDIF
							ENDIF
						ENDIF
					ELSE
						IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
							IF NOT IS_ARMORY_AIRCRAFT_AUTOPILOT_ACTIVE(PLAYER_ID())
							AND NOT SHOULD_ALLOW_ENTER_AVENGER_HOLD_FROM_PILOT_SEAT_WHEN_LANDED(pedVeh)
								bShowHelpCheckForEntry = FALSE
								#IF IS_DEBUG_BUILD
								IF bPrintAvengerAutoPilot
									PRINTLN("MAINTAIN_ENTER_AVENGER_INTERIOR_FROM_PILOT_SEAT bShowHelpCheckForEntry FALSE 3")
								ENDIF
								#ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF NOT NETWORK_IS_ACTIVITY_SESSION()			
						IF IS_OWNERS_ARMORY_AIRCRAFT_INSIDE_DEFUNCT_BASE(GET_OWNER_OF_PERSONAL_ARMORY_AIRCRAFT(pedVeh))
							IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
								bShowHelpCheckForEntry = FALSE
								#IF IS_DEBUG_BUILD
								IF bPrintAvengerAutoPilot
									PRINTLN("MAINTAIN_ENTER_AVENGER_INTERIOR_FROM_PILOT_SEAT bShowHelpCheckForEntry FALSE 4")
								ENDIF
								#ENDIF
							ELSE
								bShowHelpCheckForEntry = TRUE
								#IF IS_DEBUG_BUILD
								IF bPrintAvengerAutoPilot
									PRINTLN("MAINTAIN_ENTER_AVENGER_INTERIOR_FROM_PILOT_SEAT bShowHelpCheckForEntry 5")
								ENDIF
								#ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_ANY_PASSENGER_ENTERING_AVENGER_HOLD(pedVeh)
					#IF IS_DEBUG_BUILD
					IF bPrintAvengerAutoPilot
						PRINTLN("MAINTAIN_ENTER_AVENGER_INTERIOR_FROM_PILOT_SEAT IS_ANY_PASSENGER_ENTERING_AVENGER_HOLD TRUE")
					ENDIF
					#ENDIF
					bShowHelpCheckForEntry = FALSE
				ENDIF
			ENDIF	
		ENDIF	
	ENDIF

	IF IS_BIT_SET(iLocalBS, LOCAL_BS_HAS_PLAYER_STARTED_AVENGER_SEAT_SHUFFLE)
		IF (GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) = PERFORMING_TASK
		OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) = WAITING_TO_START_TASK)
		AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT) != WAITING_TO_START_TASK
		AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT) != PERFORMING_TASK
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			SET_CONCEAL_PLAYER_FOR_PILOT_SEAT_TRANS(TRUE)
			SET_PLAYER_STARTED_AVENGER_SEAT_SHUFFLE(FALSE)
			CLEANUP_SCRIPTED_CAMERA_ENTERING_BACK()
			PRINTLN("MAINTAIN_ENTER_AVENGER_INTERIOR_FROM_PILOT_SEAT - player is leaving vehicle SET_PLAYER_STARTED_AVENGER_SEAT_SHUFFLE false")
		ENDIF
	ENDIF
	
	IF IS_ENTITY_DEAD(pedVeh)
		IF IS_BIT_SET(iLocalBS, LOCAL_BS_HAS_PLAYER_STARTED_AVENGER_SEAT_SHUFFLE)
			SET_PLAYER_STARTED_AVENGER_SEAT_SHUFFLE(FALSE)
			SET_CONCEAL_PLAYER_FOR_PILOT_SEAT_TRANS(TRUE)
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			PRINTLN("MAINTAIN_ENTER_AVENGER_INTERIOR_FROM_PILOT_SEAT - pedVeh vehicle dead SET_PLAYER_STARTED_AVENGER_SEAT_SHUFFLE = false")

			IF(iCockpitSoundID!= -1)
				IF IS_AUDIO_SCENE_ACTIVE("dlc_xm_avenger_interior_scene")
					STOP_SOUND(iCockpitSoundID)
					STOP_AUDIO_SCENE("dlc_xm_avenger_interior_scene")
					iCockpitSoundID = -1
				ENDIF
			ENDIF

			CLEANUP_SCRIPTED_CAMERA_ENTERING_BACK()
		ENDIF	
	ENDIF
	
	IF bShowHelpCheckForEntry
		IF NOT NETWORK_IS_ACTIVITY_SESSION()
			IF SHOULD_THIS_ARMORY_AIRCRAFT_LOCATE_BE_HIDDEN(SIMPLE_INTERIOR_ARMORY_AIRCRAFT_1)
			OR !CAN_PLAYER_ENTER_ARMORY_AIRCRAFT(PLAYER_ID(), SIMPLE_INTERIOR_ARMORY_AIRCRAFT_1, 0)
			OR IS_DRIVER_ENTERING_SIMPLE_INTERIOR(PLAYER_ID())
				bShowHelpCheckForEntry = FALSE
				#IF IS_DEBUG_BUILD
				IF bPrintAvengerAutoPilot
					PRINTLN("MAINTAIN_ENTER_AVENGER_INTERIOR_FROM_PILOT_SEAT bShowHelpCheckForEntry FALSE 5")
				ENDIF
				#ENDIF
			ENDIF
			IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
			AND GET_OWNER_OF_PERSONAL_ARMORY_AIRCRAFT(pedVeh) = PLAYER_ID()
				bShowHelpCheckForEntry = FALSE
				#IF IS_DEBUG_BUILD
				PRINTLN("MAINTAIN_ENTER_AVENGER_INTERIOR_FROM_PILOT_SEAT I'm in a gang and this is my avenger")
				#ENDIF
			ENDIF
		ELSE
			IF SHOULD_THIS_CREATOR_AIRCRAFT_LOCATE_BE_HIDDEN(SIMPLE_INTERIOR_CREATOR_AIRCRAFT_1)
			OR g_bMissionOver
			OR (HAS_THIS_ADDITIONAL_TEXT_LOADED("MC_PLAY", MISSION_TEXT_SLOT) AND IS_HELP_MESSAGE_BEING_DISPLAYED() AND IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SUBJOB_HELP_7"))
				bShowHelpCheckForEntry = FALSE
			ENDIF
		ENDIF
		
		IF IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
		OR IS_BIT_SET(iLocalBS, LOCAL_BS_HAS_PLAYER_STARTED_AVENGER_SEAT_SHUFFLE)
		OR !IS_SKYSWOOP_AT_GROUND()
			bShowHelpCheckForEntry = FALSE
			#IF IS_DEBUG_BUILD
			PRINTLN("MAINTAIN_ENTER_AVENGER_INTERIOR_FROM_PILOT_SEAT player is walking in or out of simple interior bShowHelpCheckForEntry = FALSE")
			#ENDIF
		ENDIF
	ENDIF
	
	IF (IS_CELLPHONE_CAMERA_IN_USE()
	OR IS_BROWSER_OPEN()
	OR IS_CUSTOM_MENU_ON_SCREEN()	
	OR IS_PHONE_ONSCREEN()
	OR (ePedVehicleModel = AVENGER AND IS_ANY_PED_ENTERING_AVENGER(pedVeh)))
	AND NOT IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
	AND NOT IS_PLAYER_ENTERING_AIRCRAFT_FROM_BASE_COPILOT_SEAT()
	AND NOT IS_ARMORY_AIRCRAFT_PILOT_ENTERING()
	AND NOT IS_LOCAL_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR()
		IF NOT HAS_NET_TIMER_STARTED(sBlockEnterAvengerFromPilotSeat)
			START_NET_TIMER(sBlockEnterAvengerFromPilotSeat)
		ENDIF	
		bShowHelpCheckForEntry = FALSE
		#IF IS_DEBUG_BUILD
		IF bPrintAvengerAutoPilot
			PRINTLN("MAINTAIN_ENTER_AVENGER_INTERIOR_FROM_PILOT_SEAT bShowHelpCheckForEntry FALSE 6")
		ENDIF
		#ENDIF
	ENDIF

	IF HAS_NET_TIMER_STARTED(sBlockEnterAvengerFromPilotSeat)
		IF !HAS_NET_TIMER_EXPIRED(sBlockEnterAvengerFromPilotSeat, iTimer)
			bShowHelpCheckForEntry = FALSE
			#IF IS_DEBUG_BUILD
			IF bPrintAvengerAutoPilot
				PRINTLN("MAINTAIN_ENTER_AVENGER_INTERIOR_FROM_PILOT_SEAT bShowHelpCheckForEntry FALSE 77")
			ENDIF
			#ENDIF
		ELSE	
			RESET_NET_TIMER(sBlockEnterAvengerFromPilotSeat)
		ENDIF
	ENDIF
	
	IF bShowHelpCheckForEntry 
		sControlHold.control = PLAYER_CONTROL
		sControlHold.action = INPUT_CONTEXT
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		AND NOT IS_HELP_MESSAGE_ON_SCREEN()					  
			PRINT_HELP_FOREVER("CHR17_HOVER")
		ELSE	
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CHR17_HOVER") 
				IF IS_CONTROL_HELD(sControlHold)
				AND NOT IS_PAUSE_MENU_ACTIVE_EX()
					IF NOT DOES_CAM_EXIST(avengerCockPitCam)
						avengerCockPitCam = CREATE_CAMERA(DEFAULT, TRUE)
					ENDIF	
					
					SET_CONCEAL_PLAYER_FOR_PILOT_SEAT_TRANS(TRUE)
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(PLAYER_PED_ID(), GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), TRUE)
					SET_PLAYER_STARTED_AVENGER_SEAT_SHUFFLE(TRUE)
					RESET_NET_TIMER(sSeatShuffleTaskTimer)
					START_NET_TIMER(sSeatShuffleTaskTimer)
					PRINTLN("MAINTAIN_ENTER_AVENGER_INTERIOR_FROM_PILOT_SEAT SET_PLAYER_STARTED_AVENGER_SEAT_SHUFFLE TRUE")
				ENDIF
			ENDIF		
		ENDIF
	ELSE
		RESET_NET_TIMER(sControlHold.sTimer)
		IF IS_HELP_MESSAGE_BEING_DISPLAYED()
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CHR17_HOVER")
				CLEAR_HELP()
				#IF IS_DEBUG_BUILD
				IF bPrintAvengerAutoPilot
					PRINTLN("MAINTAIN_ENTER_AVENGER_INTERIOR_FROM_PILOT_SEAT bShowHelpCheckForEntry clear help")
				ENDIF
				#ENDIF
			ENDIF
		ENDIF	
	ENDIF
	
	IF IS_BIT_SET(iLocalBS, LOCAL_BS_HAS_PLAYER_STARTED_AVENGER_SEAT_SHUFFLE)
		IF (GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT) != WAITING_TO_START_TASK
		AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT) != PERFORMING_TASK)
		OR HAS_NET_TIMER_EXPIRED(sSeatShuffleTaskTimer, 5000)
			IF NOT IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_OUT(500)
			ELSE	
				IF NOT IS_SCREEN_FADING_OUT()
					IF NOT NETWORK_IS_ACTIVITY_SESSION()			
						IF IS_OWNERS_ARMORY_AIRCRAFT_INSIDE_DEFUNCT_BASE(GET_OWNER_OF_PERSONAL_ARMORY_AIRCRAFT(pedVeh))
							SET_PLAYER_ENTER_AIRCRAFT_FROM_BASE_COPILOT_SEAT(TRUE)
							SET_PLAYER_STARTED_AVENGER_SEAT_SHUFFLE(FALSE)
						ELSE
							SET_PLAYER_ENTERING_ARMORY_AIRCRAFT_INTERIOR_FROM_PILOT_SEAT(TRUE)
							SET_PLAYER_STARTED_AVENGER_SEAT_SHUFFLE(FALSE)
						ENDIF
					ELSE	
						SET_PLAYER_ENTERING_ARMORY_AIRCRAFT_INTERIOR_FROM_PILOT_SEAT(TRUE)
						SET_PLAYER_STARTED_AVENGER_SEAT_SHUFFLE(FALSE)
					ENDIF
					RESET_NET_TIMER(sSeatShuffleTaskTimer)
					IF IS_AUDIO_SCENE_ACTIVE("dlc_xm_avenger_interior_scene")
						STOP_SOUND(iCockpitSoundID)
						STOP_AUDIO_SCENE("dlc_xm_avenger_interior_scene")
						iCockpitSoundID = -1
					ENDIF
				ENDIF	
			ENDIF	
		ELSE
			IF NOT IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
				IF !DOES_ENTITY_EXIST(pedVeh)
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(500)
					ENDIF	
					SET_CONCEAL_PLAYER_FOR_PILOT_SEAT_TRANS(FALSE)
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					SET_PLAYER_STARTED_AVENGER_SEAT_SHUFFLE(FALSE)
					CLEANUP_SCRIPTED_CAMERA_ENTERING_BACK()
					PRINTLN("MAINTAIN_ENTER_AVENGER_INTERIOR_FROM_PILOT_SEAT avenger doesn't exist cleanup!")
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
	
	IF ePedVehicleModel = AVENGER
		UPDATE_COCKPIT_CAMERA(pedVeh)
	ENDIF
	CLEAR_SEAT_SHUFFLE()
	
	IF DOES_CAM_EXIST(avengerCockPitCam)
		IF !IS_BIT_SET(iLocalBS, LOCAL_BS_HAS_PLAYER_STARTED_AVENGER_SEAT_SHUFFLE)
		AND IS_SCREEN_FADED_OUT()
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			DESTROY_CAM(avengerCockPitCam)
			cockPitVehIndex = NULL
			
			IF iCockpitSoundID != -1
		
				IF IS_AUDIO_SCENE_ACTIVE("dlc_xm_avenger_interior_scene")
					STOP_SOUND(iCockpitSoundID)
					STOP_AUDIO_SCENE("dlc_xm_avenger_interior_scene")
					iCockpitSoundID = -1
					PRINTLN("MAINTAIN_ENTER_AVENGER_INTERIOR_FROM_PILOT_SEAT STOP_AUDIO_SCENE(dlc_xm_avenger_interior_scene) left freemode avenger")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

SCRIPT_TIMER sBlockEnterHackerTruckFromPilotSeat
INT iHackerTruckTimer
FUNC BOOL IS_ANY_PED_ENTERING_HACKER_TRUCK(VEHICLE_INDEX hackerTruckIndex)

	IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE, TRUE)
	AND NOT IS_PLAYER_IN_BUSINESS_HUB(PLAYER_ID())
		RETURN FALSE
	ENDIF 
	
	PLAYER_INDEX staggerPed = INT_TO_PLAYERINDEX(iRemoteVehicleStagger)
	
	IF IS_NET_PLAYER_OK(staggerPed)
	AND DOES_ENTITY_EXIST(hackerTruckIndex)
	AND NOT IS_ENTITY_DEAD(hackerTruckIndex)
	AND staggerPed  != PLAYER_ID()
		IF GET_DISTANCE_BETWEEN_ENTITIES(hackerTruckIndex, GET_PLAYER_PED(staggerPed)) < 10
			IF GET_VEHICLE_PED_IS_TRYING_TO_ENTER(GET_PLAYER_PED(staggerPed)) = NATIVE_TO_INT(hackerTruckIndex)
			OR GET_VEHICLE_PED_IS_ENTERING(GET_PLAYER_PED(staggerPed)) = hackerTruckIndex
				RETURN TRUE
			ENDIF	
		ENDIF	
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_ENTER_HACKER_TRUCK_INTERIOR_FROM_CAB_SEAT()
	
	IF (IS_PLAYER_IN_NIGHTCLUB(PLAYER_ID())
	AND GET_INTERIOR_FLOOR_INDEX() != ciBUSINESS_HUB_FLOOR_B5)
	OR IS_PLAYER_IN_CASINO(PLAYER_ID())
		EXIT
	ENDIF
	
	IF !IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	OR (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND NOT IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), TERBYTE))
		EXIT
	ENDIF
	
	BOOL bShowHelpCheckForEntry = FALSE
	VEHICLE_INDEX pedVeh 
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND NOT IS_PED_TRYING_TO_ENTER_OR_EXIT_VEHICLE(PLAYER_PED_ID())
			pedVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF IS_VEHICLE_A_PERSONAL_HACKER_TRUCK(pedVeh)
			AND NOT HAS_OWNER_EMPTY_HACKER_TRUCK(GET_OWNER_OF_PERSONAL_HACKER_TRUCK(pedVeh))
			AND NOT GET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_IsSeatShuffling)
			AND NOT IS_HACKER_TRUCK_PILOT_ENTERING()
			AND NOT IS_ENTITY_ON_FIRE(pedVeh)
				IF GET_OWNER_OF_PERSONAL_HACKER_TRUCK(pedVeh) = PLAYER_ID()	
					bShowHelpCheckForEntry = TRUE	
				ELSE	
					IF (GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
					AND GB_GET_LOCAL_PLAYER_GANG_BOSS() = GET_OWNER_OF_PERSONAL_HACKER_TRUCK(pedVeh))
						bShowHelpCheckForEntry = TRUE	
					ENDIF
				ENDIF		
				IF IS_OWNERS_HACKER_TRUCK_INSIDE_BUSINESS_HUB(GET_OWNER_OF_PERSONAL_HACKER_TRUCK(pedVeh))
					IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
						bShowHelpCheckForEntry = FALSE
					ELSE
						bShowHelpCheckForEntry = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_BIT_SET(iLocalBS, LOCAL_BS_PRINT_HACKER_TRUCK_ENTRY_HELP)
				CLEAR_BIT(iLocalBS, LOCAL_BS_PRINT_HACKER_TRUCK_ENTRY_HELP)
			ENDIF
		ENDIF
	ELSE
		SET_CONCEAL_PLAYER_FOR_PILOT_SEAT_TRANS(FALSE)
		SET_PLAYER_ENTER_HACKER_TRUCK_FROM_HUB_COPILOT_SEAT(FALSE)
		SET_PLAYER_ENTERING_HACKER_TRUCK_INTERIOR_FROM_PILOT_SEAT(FALSE)
	ENDIF
	
	IF IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_CONCEAL_PLAYER_FOR_PILOT_SEAT_TRANS(FALSE)
		SET_PLAYER_ENTER_HACKER_TRUCK_FROM_HUB_COPILOT_SEAT(FALSE)
		SET_PLAYER_ENTERING_HACKER_TRUCK_INTERIOR_FROM_PILOT_SEAT(FALSE)

		PRINTLN("MAINTAIN_ENTER_HACKER_TRUCK_INTERIOR_FROM_CAB_SEAT - Player is dead ")
	ELSE
		IF (GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) = PERFORMING_TASK
		OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) = WAITING_TO_START_TASK)
		AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT) != WAITING_TO_START_TASK
		AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT) != PERFORMING_TASK
			IF IS_PLAYER_STARTED_MOVE_TO_HACKER_TRUCK_FROM_CAB(PLAYER_ID())
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				SET_CONCEAL_PLAYER_FOR_PILOT_SEAT_TRANS(TRUE)
				SET_PLAYER_STARTED_MOVE_TO_HACKER_TRUCK_FROM_CAB(FALSE)
				DO_SCREEN_FADE_IN(500)	
				PRINTLN("MAINTAIN_ENTER_HACKER_TRUCK_INTERIOR_FROM_CAB_SEAT - player is leaving vehicle SET_PLAYER_STARTED_MOVE_TO_HACKER_TRUCK_FROM_CAB false")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_ENTITY_DEAD(pedVeh)
		IF IS_PLAYER_STARTED_MOVE_TO_HACKER_TRUCK_FROM_CAB(PLAYER_ID())
			SET_PLAYER_STARTED_MOVE_TO_HACKER_TRUCK_FROM_CAB(FALSE)
			SET_CONCEAL_PLAYER_FOR_PILOT_SEAT_TRANS(TRUE)
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			PRINTLN("MAINTAIN_ENTER_HACKER_TRUCK_INTERIOR_FROM_CAB_SEAT - pedVeh vehicle dead SET_PLAYER_STARTED_MOVE_TO_HACKER_TRUCK_FROM_CAB = false")
		ENDIF	
	ENDIF
	
	IF SHOULD_HACKER_TRUCK_LOCATE_BE_HIDDEN()
	OR !CAN_PLAYER_ENTER_HACKER_TRUCK(PLAYER_ID(), SIMPLE_INTERIOR_HACKER_TRUCK, 0)
	OR IS_DRIVER_ENTERING_SIMPLE_INTERIOR(PLAYER_ID())
		bShowHelpCheckForEntry = FALSE
		#IF IS_DEBUG_BUILD
		IF bPrintAvengerAutoPilot
			PRINTLN("MAINTAIN_ENTER_HACKER_TRUCK_INTERIOR_FROM_CAB_SEAT bShowHelpCheckForEntry FALSE 5")
		ENDIF
		#ENDIF
	ENDIF
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
	AND GET_OWNER_OF_PERSONAL_HACKER_TRUCK(pedVeh) = PLAYER_ID()
		bShowHelpCheckForEntry = FALSE
		#IF IS_DEBUG_BUILD
		PRINTLN("MAINTAIN_ENTER_HACKER_TRUCK_INTERIOR_FROM_CAB_SEAT I'm in a gang and this is my avenger")
		#ENDIF
	ENDIF

	IF IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
	OR IS_PLAYER_STARTED_MOVE_TO_HACKER_TRUCK_FROM_CAB(PLAYER_ID())
	OR !IS_SKYSWOOP_AT_GROUND()
		bShowHelpCheckForEntry = FALSE
		#IF IS_DEBUG_BUILD
		PRINTLN("MAINTAIN_ENTER_HACKER_TRUCK_INTERIOR_FROM_CAB_SEAT player is walking in or out of simple interior bShowHelpCheckForEntry = FALSE")
		#ENDIF
	ENDIF

	IF (IS_CELLPHONE_CAMERA_IN_USE()
	OR IS_BROWSER_OPEN()
	OR IS_CUSTOM_MENU_ON_SCREEN()	
	OR IS_PHONE_ONSCREEN()
	OR IS_ANY_PED_ENTERING_HACKER_TRUCK(pedVeh))
	AND NOT IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
	AND NOT IS_PLAYER_ENTERING_HACKER_TRUCK_FROM_HUB_COPILOT_SEAT(PLAYER_ID())
	AND NOT IS_HACKER_TRUCK_PILOT_ENTERING()
	AND NOT IS_LOCAL_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR()
		IF IS_ANY_PED_ENTERING_HACKER_TRUCK(pedVeh)
			iHackerTruckTimer = 7000
		ELSE
			iHackerTruckTimer = 2000
		ENDIF
		IF NOT HAS_NET_TIMER_STARTED(sBlockEnterHackerTruckFromPilotSeat)
			START_NET_TIMER(sBlockEnterHackerTruckFromPilotSeat)
		ENDIF	
		bShowHelpCheckForEntry = FALSE
		#IF IS_DEBUG_BUILD
		IF bPrintAvengerAutoPilot
			PRINTLN("MAINTAIN_ENTER_HACKER_TRUCK_INTERIOR_FROM_CAB_SEAT bShowHelpCheckForEntry FALSE 6")
		ENDIF
		#ENDIF
	ENDIF

	IF HAS_NET_TIMER_STARTED(sBlockEnterHackerTruckFromPilotSeat)
		IF !HAS_NET_TIMER_EXPIRED(sBlockEnterHackerTruckFromPilotSeat, iHackerTruckTimer)
			bShowHelpCheckForEntry = FALSE
			#IF IS_DEBUG_BUILD
			IF bPrintAvengerAutoPilot
				PRINTLN("MAINTAIN_ENTER_HACKER_TRUCK_INTERIOR_FROM_CAB_SEAT bShowHelpCheckForEntry FALSE 77")
			ENDIF
			#ENDIF
		ELSE	
			RESET_NET_TIMER(sBlockEnterHackerTruckFromPilotSeat)
		ENDIF
	ENDIF
	
	IF g_hackerTruckScanner.eState = HTSS_IN_CAM 
		bShowHelpCheckForEntry = FALSE
		#IF IS_DEBUG_BUILD
		IF bPrintAvengerAutoPilot
			PRINTLN("MAINTAIN_ENTER_HACKER_TRUCK_INTERIOR_FROM_CAB_SEAT clear help in scanner")
		ENDIF
		#ENDIF
	ENDIF
	
	
	IF bShowHelpCheckForEntry 
		sHackerTruckControlHold.control = PLAYER_CONTROL
		IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
			sHackerTruckControlHold.action = INPUT_FRONTEND_RS
		ELSE
			sHackerTruckControlHold.action = INPUT_CONTEXT
		ENDIF
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		AND NOT IS_HELP_MESSAGE_ON_SCREEN()
		AND NOT IS_BIT_SET(iLocalBS, LOCAL_BS_PRINT_HACKER_TRUCK_ENTRY_HELP)
			IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
				PRINT_HELP("CAB_TRUCK_PC")
			ELSE
				PRINT_HELP("CAB_TRUCK_ENT")
			ENDIF	
			SET_BIT(iLocalBS, LOCAL_BS_PRINT_HACKER_TRUCK_ENTRY_HELP)
		ENDIF
		IF IS_CONTROL_HELD(sHackerTruckControlHold, FALSE)
		AND NOT IS_PAUSE_MENU_ACTIVE_EX()

			SET_CONCEAL_PLAYER_FOR_PILOT_SEAT_TRANS(TRUE)
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			SET_PLAYER_STARTED_MOVE_TO_HACKER_TRUCK_FROM_CAB(TRUE)
			PRINTLN("MAINTAIN_ENTER_HACKER_TRUCK_INTERIOR_FROM_CAB_SEAT SET_PLAYER_STARTED_MOVE_TO_HACKER_TRUCK_FROM_CAB TRUE")
		ENDIF
	ELSE
		RESET_NET_TIMER(sHackerTruckControlHold.sTimer)
		IF IS_HELP_MESSAGE_BEING_DISPLAYED()
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CAB_TRUCK_ENT")
				CLEAR_HELP()
				#IF IS_DEBUG_BUILD
				IF bPrintAvengerAutoPilot
					PRINTLN("MAINTAIN_ENTER_HACKER_TRUCK_INTERIOR_FROM_CAB_SEAT bShowHelpCheckForEntry clear help")
				ENDIF
				#ENDIF
			ENDIF
		ENDIF	
	ENDIF

	IF IS_PLAYER_STARTED_MOVE_TO_HACKER_TRUCK_FROM_CAB(PLAYER_ID())
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			IF (GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT) != WAITING_TO_START_TASK
			AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT) != PERFORMING_TASK)
				IF NOT IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_OUT(500)
				ELSE	
					IF NOT IS_SCREEN_FADING_OUT()
						IF NOT NETWORK_IS_ACTIVITY_SESSION()	
							HOLSTER_WEAPON_IN_SIMPLE_INTERIOR()
							IF IS_OWNERS_HACKER_TRUCK_INSIDE_BUSINESS_HUB(GET_OWNER_OF_PERSONAL_HACKER_TRUCK(pedVeh))
								SET_PLAYER_ENTER_HACKER_TRUCK_FROM_HUB_COPILOT_SEAT(TRUE)
								SET_PLAYER_STARTED_MOVE_TO_HACKER_TRUCK_FROM_CAB(FALSE)
							ELSE
								SET_PLAYER_ENTERING_HACKER_TRUCK_INTERIOR_FROM_PILOT_SEAT(TRUE)
								SET_PLAYER_STARTED_MOVE_TO_HACKER_TRUCK_FROM_CAB(FALSE)
							ENDIF
						ENDIF
					ENDIF	
				ENDIF	
			ELSE
				IF NOT IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
					IF !DOES_ENTITY_EXIST(pedVeh)
						DO_SCREEN_FADE_IN(500)
						SET_PLAYER_STARTED_MOVE_TO_HACKER_TRUCK_FROM_CAB(FALSE)
						SET_CONCEAL_PLAYER_FOR_PILOT_SEAT_TRANS(FALSE)
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						PRINTLN("MAINTAIN_ENTER_HACKER_TRUCK_INTERIOR_FROM_CAB_SEAT avenger doesn't exist cleanup!")
					ENDIF
				ENDIF
			ENDIF	
		ELSE	
			SET_PLAYER_STARTED_MOVE_TO_HACKER_TRUCK_FROM_CAB(FALSE)
		ENDIF	
	ENDIF
ENDPROC

PROC MAINTAIN_AVENGER()
	
	IF IS_BIT_SET(iLocalBS, LOCAL_BS_SHOULD_ALLOW_EXPENSIVE_PROCESSING)
		UPDATE_LOCAL_ARMORY_AIRCRAFT_VEHICLE(iRemoteVehicleStagger )
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("UPDATE_LOCAL_ARMORY_AIRCRAFT_VEHICLE")
		#ENDIF	
		#ENDIF	
		MAINTAIN_AVENGER_AUTOPILOT()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_AVENGER_AUTOPILOT")
		#ENDIF	
		#ENDIF
	ENDIF
	CLEANUP_OSPREY_AFTER_IT_IS_DESTROYED()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("CLEANUP_OSPREY_AFTER_IT_IS_DESTROYED")
	#ENDIF	
	#ENDIF

ENDPROC

/// PURPOSE:
///    Do necessary pre game start ini. Returns FALSE if the script fails to receive its initial network broadcast.
/// PARAMS:
///    missionScriptArgs - 
/// RETURNS:
///    true when complete
FUNC BOOL PROCESS_PRE_GAME( MP_MISSION_DATA missionScriptArgs )
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(NUM_NETWORK_PLAYERS, missionScriptArgs)
	
	// This makes sure the net script is active, waits untill it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	// KGM: Wait for the first network broadcast before moving on - call this as the last instruction in pre-game
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		RETURN FALSE
	ENDIF
	
	g_iVehicleSpawnRequestPlayer = -1
	g_iVehicleSpawnRequestRequest = -1
	g_iVehicleSpawnRequestVSID = -1

	g_iVehicleSpawnCompletePlayer = -1
	g_iVehicleSpawnCompleteRequest = -1
	g_iVehicleSpawnCompleteVSID = -1
	
	g_bHasPegasusVehicleInAirport = FALSE
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_AdditionalSpewFor2469530")
		bAdditionalSpew = TRUE
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === sc_AdditionalSpewFor2469530 exists. bAdditionalSpew = ", bAdditionalSpew)
	ENDIF
	#ENDIF
	
	playerBD[NETWORK_PLAYER_ID_TO_INT()].iGameState = GAME_STATE_INI
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === PRE_GAME DONE")
	#ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Server only function that checks to see if the game mode should now end. For this test script, it if time has passed.
/// RETURNS:
///    false, always, apparently
FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET()
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Helper function to get a clients game/mission state
/// PARAMS:
///    iPlayer - player ID to get gamestate of
/// RETURNS:
///    player's game state as an INT
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iGameState
ENDFUNC

/// PURPOSE:
///    Helper function to get the servers game/mission state
/// RETURNS:
///    servers's game state as an INT
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC

#IF IS_DEBUG_BUILD
/// PURPOSE:
///    Creates the widgets for the vehicle spawn script
PROC CREATE_WIDGETS()
	START_WIDGET_GROUP("AM_VEHICLE_SPAWN")
		ADD_WIDGET_BOOL("Draw Spawn Points", bDebugDrawSpawnPoints)
		ADD_WIDGET_BOOL("Draw Spawn Points on map", bDebugDrawMapSpawnPoints)
		ADD_WIDGET_BOOL("Spawn Last Vehicle", bDebugSpawnLastVehicle)
		ADD_WIDGET_BOOL("bForceFailSpawn",bForceFailSpawn)
		ADD_WIDGET_BOOL("Additional Spew", bAdditionalSpew)
		ADD_WIDGET_BOOL("bForceMobileIsStuck", bForceMobileIsStuck)
//		CREATE_LUXE_ACT_WIDGETS(luxeActState)
		START_WIDGET_GROUP("FEATURE_LUXE_VEH_ACTIVITIES")
			ADD_WIDGET_INT_SLIDER("ciLUXE_CLEANUP_TIME_OUT", ciLUXE_CLEANUP_TIME_OUT, 0, 60000, 1000)
		STOP_WIDGET_GROUP()
		
		ADD_WIDGET_FLOAT_SLIDER("Turret Slowdown Modifier", fTurretTimeModifier, 0.1, 20, 0.1)
		
		ADD_WIDGET_BOOL("Print avenger autopilot information", bPrintAvengerAutoPilot)
		
//	START_WIDGET_GROUP("LUXE_VEH_ACTIVITY")
//		ADD_WIDGET_INT_SLIDER("Player Seat: ",iLuxeSeatWidget, -1, 8, 1)
//	STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("MOC DAMAGE SCALE")
			ADD_WIDGET_FLOAT_SLIDER("PHANTOM3 damage scale",		g_sMPTunables.fGR_MOC_PHANTOM3_DAMAGE_SCALE		, 0.0, 1.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("HAULER2 damage scale",			g_sMPTunables.fGR_MOC_HAULER2_DAMAGE_SCALE		, 0.0, 1.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("TRAILERLARGE damage scale",	g_sMPTunables.fGR_MOC_TRAILERLARGE_DAMAGE_SCALE	, 0.0, 1.0, 0.01)
			
			ADD_WIDGET_BOOL("bUpdateMOCDamageScale", 				bUpdateMOCDamageScale)
		STOP_WIDGET_GROUP()

		START_WIDGET_GROUP("Avenger Cockpit Camera")
	
			ADD_WIDGET_BOOL("Grab CockPit camera Coords", bGrabAvengerCockPitCameraCoords)
			
			ADD_WIDGET_BOOL("Start CockPit debug camera", bStartDebugCam)
			ADD_WIDGET_FLOAT_SLIDER("Camera coord x offset", vAvengerCamCoord.x, -1000, 1000, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Camera coord y offset", vAvengerCamCoord.y, -1000, 1000, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Camera coord z offset", vAvengerCamCoord.z, -1000, 1000, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Camera rot x offset", vAvengerCamRotation.x, -1000, 1000, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Camera rot y offset", vAvengerCamRotation.y, -1000, 1000, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Camera rot z offset", vAvengerCamRotation.z, -1000, 1000, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Camera FOV", fAvengerFOV, -1000, 1000, 0.1)

		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Party bus Movie")
			ADD_WIDGET_BOOL("activate text widgets",db_bTestScaleform)
			ADD_WIDGET_FLOAT_SLIDER("CentreX", db_CentreX, -2, 10, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("CentreY", db_CentreY, -2, 10, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Width", db_Width, -1, 10, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Height", db_Height, -1, 10, 0.01)
		STOP_WIDGET_GROUP()
		
		#IF FEATURE_HEIST_ISLAND
		START_WIDGET_GROUP("Submarine Establishing shot tool")
			ADD_WIDGET_BOOL("Grab Debug Cam as Offset", bGrabCamCoordsAsOffset)
			
			ADD_WIDGET_VECTOR_SLIDER("vCamPosAsOffset", vCamPosAsOffset, -9999.9, 9999.9, 0.001)
			ADD_WIDGET_VECTOR_SLIDER("vCamRotAsOffset", vCamRotAsOffset, -9999.9, 9999.9, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("fCamFov", fCamFov, -9999.9, 9999.9, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("fCamShake", fCamShake, -9999.9, 9999.9, 0.001)
			
			ADD_WIDGET_BOOL("Run Test Camera", bTestCamCoords)
		STOP_WIDGET_GROUP()			

		INT i
		TEXT_LABEL_63 str

		START_WIDGET_GROUP("Freemode Submarine")
			ADD_WIDGET_BOOL("Reset first reveal", bFMSubmarineDebug_ResetFirstReveal)
			ADD_WIDGET_INT_SLIDER("iRevealHintType", iRevealHintType, LOWEST_INT, HIGHEST_INT, 1)
			ADD_WIDGET_VECTOR_SLIDER("vRevealOffset", vRevealOffset, -9999.9, 9999.9, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("fRevealDist", fRevealDist, -9999.9, 9999.9, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("fRevealDepth", fRevealDepth, -9999.9, 9999.9, 0.001)
			ADD_WIDGET_BOOL("Destroy Sub", bFMSub_Destroy)			
			
			ADD_WIDGET_BOOL("MPGlobalsAmbience.bDisableKillChaseHintCam", MPGlobalsAmbience.bDisableKillChaseHintCam)
			ADD_WIDGET_INT_SLIDER("SUB_WARP_WAIT_TIME", SUB_WARP_WAIT_TIME, 0, 999999, 1)
			ADD_WIDGET_FLOAT_SLIDER("SUB_OUT_TO_SEA_DIST", SUB_OUT_TO_SEA_DIST, -99999.9, 99999.9, 0.01)
			
			START_WIDGET_GROUP("REQUEST_WARP_SUBMARINE")
				ADD_WIDGET_BOOL("Call", bCallRequestSubWarp)
				ADD_WIDGET_VECTOR_SLIDER("Warp Coords", vRequestSubWarpCoords, -99999.9, 99999.9, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Warp Heading", fRequestSubWarpHeading, -99999.9, 99999.9, 0.01)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("FORCE_DIVE")
				ADD_WIDGET_BOOL("Call", bCallForceDive)
				ADD_WIDGET_FLOAT_SLIDER("Depth", fCallForceDiveDepth, -500, 500.0, 0.01)
			STOP_WIDGET_GROUP()				
			
			START_WIDGET_GROUP("Submarine DAMAGE SCALE")
				ADD_WIDGET_FLOAT_SLIDER("Submarine damage scale",		g_sMPTunables.fSUBMARINE_DAMAGE_SCALE		, 0.0, 1.0, 0.01)
				ADD_WIDGET_BOOL("Update Submarine Damage Scale", 		bUpdateSubmarineDamageScale)
			STOP_WIDGET_GROUP()

			START_WIDGET_GROUP("Location")
				REPEAT NUM_NETWORK_PLAYERS i
					str = "["
					str += i
					str += "].vSubmarineLocation"
					ADD_WIDGET_VECTOR_SLIDER(str, GlobalplayerBD_FM_4[i].vSubmarineLocation, -99999.9, 99999.9, 0.01)
				ENDREPEAT				
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Test new sub collision detection")
				ADD_WIDGET_BOOL("Active", bFMSub_TestNewCollisionDetection)
				ADD_WIDGET_BOOL("g_bNewSpawningBoundingBoxChecksEnabled", g_bNewSpawningBoundingBoxChecksEnabled)
				ADD_WIDGET_VECTOR_SLIDER("vDummySubPos", vDummySubPos, -9999.9, 9999.9, 0.05)
				ADD_WIDGET_FLOAT_SLIDER("fDummySubHeading", fDummySubHeading, 0.0, 360.0, 0.1)
			STOP_WIDGET_GROUP()
			
		STOP_WIDGET_GROUP()	

		START_WIDGET_GROUP("Imani PV armor plating DAMAGE SCALE")
			ADD_WIDGET_FLOAT_SLIDER("Damage scale",		g_sMPTunables.fIMANI_ARMOR_PLATING_DAMAGE_SCALE		, 0.0, 1.0, 0.01)
			ADD_WIDGET_BOOL("Update armor playing Damage Scale", 		bUpdateSubmarineDamageScale)
		STOP_WIDGET_GROUP()

		#ENDIF		
		
	STOP_WIDGET_GROUP()


	#IF SCRIPT_PROFILER_ACTIVE
	CREATE_SCRIPT_PROFILER_WIDGET() 
	#ENDIF

	
ENDPROC

#IF FEATURE_DLC_2_2022
PROC DEBUG_CREATE_ACID_LAB()
	IF NOT DOES_ENTITY_EXIST(MPGlobalsAmbience.vehAcidLab)
	AND g_bDebugCreateAcidLab
		PRINTLN("[ACID_LAB] IF REQUEST_LOAD_MODEL(GET_ACID_LAB_MODEL())")
		VECTOR vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
		NETWORK_INDEX truckIndex
		IF REQUEST_LOAD_MODEL(GET_ACID_LAB_MODEL())
			IF CREATE_NET_VEHICLE(truckIndex,GET_ACID_LAB_MODEL(),  <<vPlayerCoords.x + 10, vPlayerCoords.y + 10, vPlayerCoords.z>>, 0,FALSE, TRUE)
				PRINTLN("[ACID_LAB] CREATE_NET_VEHICLE")
				MPGlobalsAmbience.vehAcidLab = NET_TO_VEH(truckIndex)
				SET_VEHICLE_ON_GROUND_PROPERLY(MPGlobalsAmbience.vehAcidLab)
				IF DECOR_IS_REGISTERED_AS_TYPE("Player_Acid_Lab", DECOR_TYPE_INT)
					IF NOT DECOR_EXIST_ON(MPGlobalsAmbience.vehAcidLab, "Player_Acid_Lab")
						DECOR_SET_INT(MPGlobalsAmbience.vehAcidLab, "Player_Acid_Lab", NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
					ENDIF
				ENDIF
			ENDIF		
		ENDIF	
	ELSE
		g_bDebugCreateAcidLab = FALSE
	ENDIF
ENDPROC
#ENDIF


#IF FEATURE_HEIST_ISLAND
PROC OUTPUT_DEBUG_CAM_COORDS_AS_OFFSET()

	IF NOT IS_ENTITY_DEAD(MPGlobalsAmbience.vehSubmarine)
		
		CAMERA_INDEX CamID = GET_DEBUG_CAM() 
		VECTOR vCamPos = GET_CAM_COORD(CamID)
		VECTOR vCamRot = GET_CAM_ROT(CamID)
		fCamFov = GET_CAM_FOV(CamID)		
		vCamPosAsOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(MPGlobalsAmbience.vehSubmarine, vCamPos)
		vCamRotAsOffset = <<vCamRot.x, vCamRot.y, vCamRot.z - GET_ENTITY_HEADING(MPGlobalsAmbience.vehSubmarine)>>
		
		
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("camera as offset from sub:")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		SAVE_STRING_TO_DEBUG_FILE("vCamPosAsOffset:")
		SAVE_VECTOR_TO_DEBUG_FILE(vCamPosAsOffset)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		SAVE_STRING_TO_DEBUG_FILE("vCamRotAsOffset:")
		SAVE_VECTOR_TO_DEBUG_FILE(vCamRotAsOffset)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		SAVE_STRING_TO_DEBUG_FILE("fCamFov:")
		SAVE_FLOAT_TO_DEBUG_FILE(fCamFov)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		SAVE_STRING_TO_DEBUG_FILE("fCamShake:")
		SAVE_FLOAT_TO_DEBUG_FILE(fCamShake)
		SAVE_NEWLINE_TO_DEBUG_FILE()
	ENDIF
	
ENDPROC

CAMERA_INDEX TestCamID
PROC TEST_CAM_COORDS()

	IF NOT IS_ENTITY_DEAD(MPGlobalsAmbience.vehSubmarine)
		VECTOR vCam = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(MPGlobalsAmbience.vehSubmarine, vCamPosAsOffset)
		VECTOR vRot = <<vCamRotAsOffset.x, vCamRotAsOffset.y, vCamRotAsOffset.z + GET_ENTITY_HEADING(MPGlobalsAmbience.vehSubmarine)>>

		IF NOT DOES_CAM_EXIST(TestCamID)
			TestCamID = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vCam, vRot, fCamFov)
			SET_CAM_ACTIVE(TestCamID, TRUE)			
		ENDIF		
		
		SET_CAM_COORD(TestCamID, vCam)
		SET_CAM_ROT(TestCamID, vRot)
		SET_CAM_FOV(TestCamID, fCamFov)
		SHAKE_CAM(TestCamID, "HAND_SHAKE", fCamShake)

	ENDIF

	RENDER_SCRIPT_CAMS(TRUE, FALSE)


ENDPROC
#ENDIF

/// PURPOSE:
///    Updates the widgets for the vehicle spawn script
PROC UPDATE_WIDGETS()
	INT iSpawnAreaCount
	INT iSpawnCoordCount
	IF bDebugDrawSpawnPoints
		
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
		
		FLOAT fAreaRange = SQRT(APPROACH_DISTANCE_FOR_SPAWN_POINT_SQR)
		FLOAT fCoordRange = 8.5
		
		REPEAT NUMBER_OF_VEHICLE_SPAWN_POINTS iSpawnAreaCount
			DRAW_DEBUG_SPHERE(GET_VEHICLE_SPAWN_POINT_CENTRE(iSpawnAreaCount), fAreaRange, 0, 255, 0, 125)
			REPEAT GET_NUMBER_OF_VEHICLE_SPAWN_POINT_COORDS(iSpawnAreaCount) iSpawnCoordCount
				DRAW_DEBUG_SPHERE(GET_VEHICLE_SPAWN_POINT_COORD(iSpawnAreaCount, iSpawnCoordCount), fCoordRange, 255, 0, 0, 125)
			ENDREPEAT
		ENDREPEAT
	ENDIF

	IF bDebugDrawMapSpawnPoints
		FLOAT fAreaRange = SQRT(APPROACH_DISTANCE_FOR_SPAWN_POINT_SQR)
	
		REPEAT NUMBER_OF_VEHICLE_SPAWN_POINTS iSpawnAreaCount
			IF NOT DOES_BLIP_EXIST(bDebugMapSpawnPoints[iSpawnAreaCount])
				bDebugMapSpawnPoints[iSpawnAreaCount] = ADD_BLIP_FOR_AREA(GET_VEHICLE_SPAWN_POINT_CENTRE(iSpawnAreaCount),fAreaRange,fAreaRange)
			ENDIF
		ENDREPEAT
	ELSE
		IF DOES_BLIP_EXIST(bDebugMapSpawnPoints[0])
			REPEAT NUMBER_OF_VEHICLE_SPAWN_POINTS iSpawnAreaCount
				IF DOES_BLIP_EXIST(bDebugMapSpawnPoints[iSpawnAreaCount])
					REMOVE_BLIP(bDebugMapSpawnPoints[iSpawnAreaCount])
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF	
	
	IF bDebugSpawnLastVehicle
		SET_BIT(g_TransitionSessionNonResetVars.iRestoreLastVehicleBitset, TRAN_SESS_NON_RESET_LAST_VEHICLE_NEED_PEGASUS_SPAWN)
		g_TransitionSessionNonResetVars.sLastVehAfterJobData.VehicleSetup.eModel = STUNT
		g_TransitionSessionNonResetVars.vLastVehicleBeforeCoronaSavePosition = <<-760.0814, 289.5827, 84.6198>>
		g_TransitionSessionNonResetVars.fLastVehicleBeforeCoronaSaveHeading = 92.5419
		bDebugSpawnLastVehicle = FALSE
	ENDIF
	
	IF bUpdateMOCDamageScale
		bUpdateMOCDamageScale = FALSE
		
		IF NOT IS_ENTITY_DEAD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]))
			SET_ENTITY_HEALTH(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]), GET_ENTITY_MAX_HEALTH(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])))
			IF (GET_PLAYERS_GUNRUNNING_TRUCK_MODEL(PLAYER_ID()) = PHANTOM3)
				SET_VEHICLE_DAMAGE_SCALE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]), g_sMPTunables.fGR_MOC_PHANTOM3_DAMAGE_SCALE)
			ELSE
				SET_VEHICLE_DAMAGE_SCALE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]), g_sMPTunables.fGR_MOC_HAULER2_DAMAGE_SCALE)
			ENDIF
		ENDIF
		IF NOT IS_ENTITY_DEAD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]))
			SET_ENTITY_HEALTH(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]), GET_ENTITY_MAX_HEALTH(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])))
			SET_VEHICLE_DAMAGE_SCALE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]), g_sMPTunables.fGR_MOC_TRAILERLARGE_DAMAGE_SCALE)
		ENDIF
	ENDIF
	
	IF bUpdateSubmarineDamageScale
		IF NOT IS_ENTITY_DEAD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineV))
			SET_ENTITY_HEALTH(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineV), GET_ENTITY_MAX_HEALTH(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineV)))
			SET_VEHICLE_DAMAGE_SCALE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineV), g_sMPTunables.fSUBMARINE_DAMAGE_SCALE)
			SET_VEHICLE_EXPLODES_ON_EXPLOSION_DAMAGE_AT_ZERO_BODY_HEALTH(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineV), TRUE)
			bUpdateSubmarineDamageScale = FALSE
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(GET_FOCUS_ENTITY_INDEX())
			SET_ENTITY_HEALTH(GET_FOCUS_ENTITY_INDEX(), GET_ENTITY_MAX_HEALTH(GET_FOCUS_ENTITY_INDEX()))
			SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_FOCUS_ENTITY_INDEX()), FALSE)
			SET_VEHICLE_STRONG(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_FOCUS_ENTITY_INDEX()), TRUE)
			SET_VEHICLE_DAMAGE_SCALE(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_FOCUS_ENTITY_INDEX()), g_sMPTunables.fIMANI_ARMOR_PLATING_DAMAGE_SCALE)
			SET_VEHICLE_EXPLODES_ON_EXPLOSION_DAMAGE_AT_ZERO_BODY_HEALTH(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_FOCUS_ENTITY_INDEX()), TRUE)
			bUpdateSubmarineDamageScale = FALSE
		ENDIF
	ENDIF
	
	IF bGrabAvengerCockPitCameraCoords
		VECTOR vCamHeading
		vCamHeading = GET_FINAL_RENDERED_CAM_ROT()
		
		OPEN_DEBUG_FILE()	
		SAVE_STRING_TO_DEBUG_FILE("//════════════════════════════════════════════════════════════════════") SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("Camera offset coords = ") SAVE_VECTOR_TO_DEBUG_FILE(GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), GET_FINAL_RENDERED_CAM_COORD())) SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("Camera offset rotation = << ") SAVE_FLOAT_TO_DEBUG_FILE(vCamHeading.x) 
		SAVE_STRING_TO_DEBUG_FILE(" , ") SAVE_FLOAT_TO_DEBUG_FILE(vCamHeading.y) 
		SAVE_STRING_TO_DEBUG_FILE(" , ") SAVE_FLOAT_TO_DEBUG_FILE(GET_ENTITY_HEADING(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) - vCamHeading.z)
		SAVE_STRING_TO_DEBUG_FILE(" >> ") SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("Camera FOV = ") SAVE_FLOAT_TO_DEBUG_FILE(GET_FINAL_RENDERED_CAM_FOV()) SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("Avenger heading = ") SAVE_FLOAT_TO_DEBUG_FILE(GET_ENTITY_HEADING(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))) SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("//════════════════════════════════════════════════════════════════════") SAVE_NEWLINE_TO_DEBUG_FILE()
		CLOSE_DEBUG_FILE()
		bGrabAvengerCockPitCameraCoords = FALSE
	ENDIF
	
	
		IF bStartDebugCam
			IF NOT DOES_CAM_EXIST(cockpitDebugCam)
				cockpitDebugCam = CREATE_CAMERA(DEFAULT, TRUE)
			ELSE
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				VEHICLE_INDEX avengerVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				ATTACH_CAM_TO_VEHICLE_BONE(cockpitDebugCam, avengerVeh, GET_ENTITY_BONE_INDEX_BY_NAME(avengerVeh, "siren4"), TRUE, vAvengerCamRotation, vAvengerCamCoord, TRUE)
				SET_CAM_FOV(cockpitDebugCam, fAvengerFOV)
				RENDER_SCRIPT_CAMS(TRUE, FALSE, 0 , TRUE, TRUE)
			ENDIF
		ELSE
			IF DOES_CAM_EXIST(cockpitDebugCam)
				DESTROY_CAM(cockpitDebugCam)
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
			ENDIF
		ENDIF	
		
	#IF FEATURE_HEIST_ISLAND	
		IF bGrabCamCoordsAsOffset
			OUTPUT_DEBUG_CAM_COORDS_AS_OFFSET()
			bGrabCamCoordsAsOffset = FALSE
		ENDIF
		
		IF bTestCamCoords
			IF NOT (bTestCamRunning)
				TEST_CAM_COORDS()
				bTestCamRunning = TRUE
			ENDIF
		ELSE
			IF (bTestCamRunning)
				DESTROY_ALL_CAMS()
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				bTestCamRunning = FALSE
			ENDIF
		ENDIF	
		
	
		IF (bCallRequestSubWarp)
			REQUEST_WARP_SUBMARINE(vRequestSubWarpCoords, fRequestSubWarpHeading)
			bCallRequestSubWarp = FALSE
		ENDIF

		IF (bCallForceDive)
			FORCE_DIVE_SUBMARINE(fCallForceDiveDepth)
		ENDIF		
	#ENDIF

	#IF FEATURE_DLC_2_2022
	DEBUG_CREATE_ACID_LAB()
	#ENDIF
ENDPROC

#ENDIF


/// PURPOSE:
///    Clears a remote player's pegasus vehicle
/// PARAMS:
///    iStaggerPlayer - player ID of owner of pegasus vehicle to clear
PROC CLEAR_REMOTE_PEGASUS_VEHICLE(INT iStaggerPlayer)
	
	// cleanup pegasus pv
	IF NOT IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, iStaggerPlayer), FALSE)	
	OR NOT NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_PegV)
	OR (NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_PegV) AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_PegV))
	OR IS_PLAYER_NEEDS_TO_REMOVE_PEGASUS_INSIDE_TRUCK_PROPERTY(INT_TO_NATIVE(PLAYER_INDEX, iStaggerPlayer))
	OR IS_PLAYER_ON_HEIST_ISLAND(INT_TO_NATIVE(PLAYER_INDEX, iStaggerPlayer))
		IF IS_PLAYER_NEEDS_TO_REMOVE_PEGASUS_INSIDE_TRUCK_PROPERTY(INT_TO_NATIVE(PLAYER_INDEX, iStaggerPlayer))
			NET_PRINT("CLEAR_REMOTE_PEGASUS_VEHICLE - remove peg v exists IS_PLAYER_NEEDS_TO_REMOVE_PEGASUS_INSIDE_TRUCK_PROPERTY is true for player: ") NET_PRINT_INT(iStaggerPlayer) NET_NL()
		ENDIF
		
		
		IF DOES_ENTITY_EXIST(MPGlobals.RemotePegV[iStaggerPlayer])
		AND IS_THIS_VEHICLE_A_PEGASUS_VEHICLE(MPGlobals.RemotePegV[iStaggerPlayer])
			NET_PRINT("CLEAR_REMOTE_PEGASUS_VEHICLE - remove peg v exists for old player ") NET_PRINT_INT(iStaggerPlayer) NET_NL()
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), MPGlobals.RemotePegV[iStaggerPlayer])
				SET_BIT(iInRemoteVehBS, iStaggerPlayer)
				NET_PRINT("CLEAR_REMOTE_PEGASUS_VEHICLE - SET_BIT(bookedVehicle.iBitset, BOOK_BS_PLAYER_WAS_IN_REMOTE_VEH)") NET_PRINT_INT(iStaggerPlayer) NET_NL()
			ENDIF
			IF NETWORK_HAS_CONTROL_OF_ENTITY(MPGlobals.RemotePegV[iStaggerPlayer])
					MPGlobals.OldRemotePegV[iStaggerPlayer] = MPGlobals.RemotePegV[iStaggerPlayer]
				CLEANUP_REMOTE_MP_PEG_SAVED_VEHICLE(MPGlobals.RemotePegV[iStaggerPlayer]  ,DEFAULT , DEFAULT  , IS_PLAYER_NEEDS_TO_REMOVE_PEGASUS_INSIDE_TRUCK_PROPERTY(INT_TO_NATIVE(PLAYER_INDEX, iStaggerPlayer)))
			ENDIF
		ENDIF		
	ELSE	
		//NET_PRINT("CLEAR_REMOTE_PEGASUS_VEHICLE - GlobalplayerBD[iStaggerPlayer].netID_PegV = ") NET_PRINT_INT(NATIVE_TO_INT(GlobalplayerBD[iStaggerPlayer].netID_PegV)) NET_NL()
		IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_PegV)			
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_PegV)
				IF IS_THIS_VEHICLE_A_PEGASUS_VEHICLE(NET_TO_VEH(GlobalplayerBD[iStaggerPlayer].netID_PegV))
				//NET_PRINT("CLEAR_REMOTE_PEGASUS_VEHICLE - netID_PegV exists for player ") NET_PRINT_INT(iStaggerPlayer) NET_NL()
					IF NOT (MPGlobals.RemotePegV[iStaggerPlayer] = NET_TO_VEH(GlobalplayerBD[iStaggerPlayer].netID_PegV))						
						MPGlobals.RemotePegV[iStaggerPlayer] = NET_TO_VEH(GlobalplayerBD[iStaggerPlayer].netID_PegV)
						PRINTLN("CLEAR_REMOTE_PEGASUS_VEHICLE - updating RemotePegV for player ", iStaggerPlayer, " RemotePegV = ", NATIVE_TO_INT(MPGlobals.RemotePegV[iStaggerPlayer] ))
					ENDIF
				ENDIF
			ELSE
				//NET_PRINT("CLEAR_REMOTE_PEGASUS_VEHICLE - NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID=FALSE for player ") NET_PRINT_INT(iStaggerPlayer) NET_NL()
			ENDIF
		ELSE
			//NET_PRINT("CLEAR_REMOTE_PEGASUS_VEHICLE - NETWORK_DOES_NETWORK_ID_EXIST=FALSE for player ") NET_PRINT_INT(iStaggerPlayer) NET_NL()
		ENDIF							
	ENDIF	
	
	// if the local player has just been kicked out of a pegasus vehicle then print a ticker message
	IF (MPGlobalsAmbience.bPrintPegasusReclaimTicker)
		STRING sReclaim = "PEG_RECLAIM"
		IF IS_BIT_SET(bookedVehicle.iBitset,BOOK_BS_BOSS_LIMO)
			sReclaim = "GB_RECLAIM"
		ENDIF
		PRINT_TICKER(sReclaim) // pegasus are reclaiming this vehicle
		MPGlobalsAmbience.bPrintPegasusReclaimTicker = FALSE
	ENDIF
	
	DELETE_REMOTE_MP_SAVED_VEHICLE_AFTER_FADE_OUT(MPGlobals.OldRemotePegV[iStaggerPlayer])
ENDPROC


/// PURPOSE:
///    Clears a remote player's pegasus vehicle
/// PARAMS:
///    iStaggerPlayer - player ID of owner of pegasus vehicle to clear
PROC CLEAR_REMOTE_TRUCK_VEHICLE(INT iStaggerPlayer)
	// cleanup truck cab
	IF NOT IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, iStaggerPlayer), FALSE)	
	OR NOT NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_TruckV[0])
	OR (NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_TruckV[0]) AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_TruckV[0]))
		IF DOES_ENTITY_EXIST(MPGlobals.RemoteTruckV[iStaggerPlayer][0])
		AND IS_ENTITY_A_VEHICLE(MPGlobals.RemoteTruckV[iStaggerPlayer][0])
		AND IS_VEHICLE_A_PERSONAL_TRUCK(MPGlobals.RemoteTruckV[iStaggerPlayer][0])

			NET_PRINT("CLEAR_REMOTE_TRUCK_VEHICLE - remove truck cab exists for old player ") NET_PRINT_INT(iStaggerPlayer) NET_NL()
			
			NET_PRINT("CLEAR_REMOTE_TRUCK_VEHICLE - IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, iStaggerPlayer), FALSE) = ")
			NET_PRINT_BOOL(IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, iStaggerPlayer), FALSE)) NET_NL()
			NET_PRINT("CLEAR_REMOTE_TRUCK_VEHICLE - NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_TruckV[0]) = ")
			NET_PRINT_BOOL(NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_TruckV[0])) NET_NL()
			NET_PRINT("CLEAR_REMOTE_TRUCK_VEHICLE - (NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_TruckV[0]) AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_TruckV[0])) = ")
			NET_PRINT_BOOL((NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_TruckV[0]) AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_TruckV[0]))) NET_NL()
			
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), MPGlobals.RemoteTruckV[iStaggerPlayer][0])
				SET_BIT(iInRemoteVehBS, iStaggerPlayer)
				NET_PRINT("CLEAR_REMOTE_TRUCK_VEHICLE - SET_BIT(bookedVehicle.iBitset, BOOK_BS_PLAYER_WAS_IN_REMOTE_VEH)") NET_PRINT_INT(iStaggerPlayer) NET_NL()
			ENDIF
			IF NETWORK_HAS_CONTROL_OF_ENTITY(MPGlobals.RemoteTruckV[iStaggerPlayer][0])
				IF IS_VEHICLE_ATTACHED_TO_TRAILER(MPGlobals.RemoteTruckV[iStaggerPlayer][0])
					DETACH_VEHICLE_FROM_TRAILER(MPGlobals.RemoteTruckV[iStaggerPlayer][0])
					NET_PRINT("CLEAR_REMOTE_TRUCK_VEHICLE - detach truck cab for old player ") NET_PRINT_INT(iStaggerPlayer) NET_NL()
					
					EXIT
				ENDIF
				
					MPGlobals.OldRemoteTruckV[iStaggerPlayer][0] = MPGlobals.RemoteTruckV[iStaggerPlayer][0]
				CLEANUP_REMOTE_MP_PEG_SAVED_VEHICLE(MPGlobals.RemoteTruckV[iStaggerPlayer][0])
			ENDIF
		ENDIF
	ELSE	
		IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_TruckV[0])			
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_TruckV[0])
				IF IS_ENTITY_A_VEHICLE(NET_TO_VEH(GlobalplayerBD[iStaggerPlayer].netID_TruckV[0]))
					IF IS_VEHICLE_A_PERSONAL_TRUCK(NET_TO_VEH(GlobalplayerBD[iStaggerPlayer].netID_TruckV[0]))
						IF NOT (MPGlobals.RemoteTruckV[iStaggerPlayer][0] = NET_TO_VEH(GlobalplayerBD[iStaggerPlayer].netID_TruckV[0]))
							MPGlobals.RemoteTruckV[iStaggerPlayer][0] = NET_TO_VEH(GlobalplayerBD[iStaggerPlayer].netID_TruckV[0])
							PRINTLN("CLEAR_REMOTE_TRUCK_VEHICLE - updating RemoteTruckV for player ", iStaggerPlayer, " RemoteTruckV[0] = ", NATIVE_TO_INT(MPGlobals.RemoteTruckV[iStaggerPlayer][0]))
						ENDIF
					ENDIF
				ELSE
					PRINTLN("CLEAR_REMOTE_TRUCK_VEHICLE - RemoteTruckV not a vehicle for player ", iStaggerPlayer, " RemoteTruckV[0] = ", NATIVE_TO_INT(MPGlobals.RemoteTruckV[iStaggerPlayer][0]))
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	
	// cleanup truck trailer
	IF NOT IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, iStaggerPlayer), FALSE)	
	OR NOT NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_TruckV[1])
	OR (NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_TruckV[1]) AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_TruckV[1]))
		IF DOES_ENTITY_EXIST(MPGlobals.RemoteTruckV[iStaggerPlayer][1])
		AND IS_ENTITY_A_VEHICLE(MPGlobals.RemoteTruckV[iStaggerPlayer][1])
		AND IS_VEHICLE_A_PERSONAL_TRUCK(MPGlobals.RemoteTruckV[iStaggerPlayer][1])
			NET_PRINT("CLEAR_REMOTE_TRUCK_VEHICLE - remove truck trailer exists for old player ") NET_PRINT_INT(iStaggerPlayer) NET_NL()
			
			NET_PRINT("CLEAR_REMOTE_TRUCK_VEHICLE - IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, iStaggerPlayer), FALSE) = ")
			NET_PRINT_BOOL(IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, iStaggerPlayer), FALSE)) NET_NL()
			NET_PRINT("CLEAR_REMOTE_TRUCK_VEHICLE - NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_TruckV[1]) = ")
			NET_PRINT_BOOL(NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_TruckV[1])) NET_NL()
			NET_PRINT("CLEAR_REMOTE_TRUCK_VEHICLE - (NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_TruckV[1]) AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_TruckV[1])) = ")
			NET_PRINT_BOOL((NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_TruckV[1]) AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_TruckV[1]))) NET_NL()
			
			IF NETWORK_HAS_CONTROL_OF_ENTITY(MPGlobals.RemoteTruckV[iStaggerPlayer][1])
					MPGlobals.OldRemoteTruckV[iStaggerPlayer][1] = MPGlobals.RemoteTruckV[iStaggerPlayer][1]
				CLEANUP_REMOTE_MP_PEG_SAVED_VEHICLE(MPGlobals.RemoteTruckV[iStaggerPlayer][1])
			ENDIF
		ENDIF
	ELSE	
		IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_TruckV[1])			
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_TruckV[1])
				IF IS_ENTITY_A_VEHICLE(NET_TO_VEH(GlobalplayerBD[iStaggerPlayer].netID_TruckV[1]))
					IF IS_VEHICLE_A_PERSONAL_TRUCK(NET_TO_VEH(GlobalplayerBD[iStaggerPlayer].netID_TruckV[1]))
						IF NOT (MPGlobals.RemoteTruckV[iStaggerPlayer][1] = NET_TO_VEH(GlobalplayerBD[iStaggerPlayer].netID_TruckV[1]))
							MPGlobals.RemoteTruckV[iStaggerPlayer][1] = NET_TO_VEH(GlobalplayerBD[iStaggerPlayer].netID_TruckV[1])
							PRINTLN("CLEAR_REMOTE_TRUCK_VEHICLE - updating RemoteTruckV for player ", iStaggerPlayer, " RemoteTruckV[1] = ", NATIVE_TO_INT(MPGlobals.RemoteTruckV[iStaggerPlayer][1]))
						ENDIF
					ENDIF
				ELSE
					PRINTLN("CLEAR_REMOTE_TRUCK_VEHICLE - RemoteTruckV not a vehicle for player ", iStaggerPlayer, " RemoteTruckV[1] = ", NATIVE_TO_INT(MPGlobals.RemoteTruckV[iStaggerPlayer][1]))
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	
	// if the local player has just been kicked out of a pegasus vehicle then print a ticker message
	IF (MPGlobalsAmbience.bPrintPegasusReclaimTicker)
		STRING sReclaim = "PEG_RECLAIM"
		IF IS_BIT_SET(bookedVehicle.iBitset,BOOK_BS_BOSS_LIMO)
			sReclaim = "GB_RECLAIM"
		ENDIF
		PRINT_TICKER(sReclaim) // pegasus are reclaiming this vehicle
		MPGlobalsAmbience.bPrintPegasusReclaimTicker = FALSE
	ENDIF
	
//	DELETE_REMOTE_MP_SAVED_VEHICLE_AFTER_FADE_OUT(MPGlobals.OldRemoteTruckV[iStaggerPlayer][0])
//	DELETE_REMOTE_MP_SAVED_VEHICLE_AFTER_FADE_OUT(MPGlobals.OldRemoteTruckV[iStaggerPlayer][1])
ENDPROC


/// PURPOSE:
///    Clears a remote player's osprey vehicle
/// PARAMS:
///    iStaggerPlayer - player ID of owner of pegasus vehicle to clear
PROC CLEAR_REMOTE_AVENGER_VEHICLE(INT iStaggerPlayer)
	// cleanup avenger
	IF NOT IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, iStaggerPlayer), FALSE)	
	OR NOT NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_AvengerV)
	OR (NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_AvengerV) AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_AvengerV))
		IF DOES_ENTITY_EXIST(MPGlobals.RemoteAvengerV[iStaggerPlayer])
		AND IS_ENTITY_A_VEHICLE(MPGlobals.RemoteAvengerV[iStaggerPlayer])
		AND IS_VEHICLE_A_PERSONAL_ARMORY_AIRCRAFT(MPGlobals.RemoteAvengerV[iStaggerPlayer])
			
			NET_PRINT("CLEAR_REMOTE_AVENGER_VEHICLE - remove osprey exists for old player ") NET_PRINT_INT(iStaggerPlayer) NET_NL()
			
			NET_PRINT("CLEAR_REMOTE_AVENGER_VEHICLE - IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, iStaggerPlayer), FALSE) = ")
			NET_PRINT_BOOL(IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, iStaggerPlayer), FALSE)) NET_NL()
			NET_PRINT("CLEAR_REMOTE_AVENGER_VEHICLE - NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_AvengerV) = ")
			NET_PRINT_BOOL(NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_AvengerV)) NET_NL()
			NET_PRINT("CLEAR_REMOTE_AVENGER_VEHICLE - (NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_AvengerV) AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_AvengerV)) = ")
			NET_PRINT_BOOL((NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_AvengerV) AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_AvengerV))) NET_NL()
			
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), MPGlobals.RemoteAvengerV[iStaggerPlayer])
				SET_BIT(iInRemoteVehBS, iStaggerPlayer)
				NET_PRINT("CLEAR_REMOTE_AVENGER_VEHICLE - SET_BIT(bookedVehicle.iBitset, BOOK_BS_PLAYER_WAS_IN_REMOTE_VEH)") NET_PRINT_INT(iStaggerPlayer) NET_NL()
			ENDIF
			IF NETWORK_HAS_CONTROL_OF_ENTITY(MPGlobals.RemoteAvengerV[iStaggerPlayer])
					MPGlobals.OldRemoteAvengerV[iStaggerPlayer] = MPGlobals.RemoteAvengerV[iStaggerPlayer]
				VECTOR vCoords = GET_ENTITY_COORDS(MPGlobals.RemoteAvengerV[iStaggerPlayer], FALSE)
				CLEANUP_REMOTE_MP_PEG_SAVED_VEHICLE(MPGlobals.RemoteAvengerV[iStaggerPlayer], DEFAULT, DEFAULT, (vCoords.z < -90))
			ENDIF
		ENDIF
	ELSE	
		IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_AvengerV)			
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_AvengerV)
				IF IS_ENTITY_A_VEHICLE(NET_TO_VEH(GlobalplayerBD[iStaggerPlayer].netID_AvengerV))
					IF IS_VEHICLE_A_PERSONAL_ARMORY_AIRCRAFT(NET_TO_VEH(GlobalplayerBD[iStaggerPlayer].netID_AvengerV))
						IF NOT (MPGlobals.RemoteAvengerV[iStaggerPlayer] = NET_TO_VEH(GlobalplayerBD[iStaggerPlayer].netID_AvengerV))
							MPGlobals.RemoteAvengerV[iStaggerPlayer] = NET_TO_VEH(GlobalplayerBD[iStaggerPlayer].netID_AvengerV)
							PRINTLN("CLEAR_REMOTE_AVENGER_VEHICLE - updating RemoteAvengerV for player ", iStaggerPlayer, " RemoteAvengerV[0] = ", NATIVE_TO_INT(MPGlobals.RemoteAvengerV[iStaggerPlayer]))
						ENDIF
					ENDIF
				ELSE
					PRINTLN("CLEAR_REMOTE_AVENGER_VEHICLE - RemoteAvengerV not a vehicle for player ", iStaggerPlayer, " RemoteAvengerV[0] = ", NATIVE_TO_INT(MPGlobals.RemoteAvengerV[iStaggerPlayer]))
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// if the local player has just been kicked out of a pegasus vehicle then print a ticker message
	IF (MPGlobalsAmbience.bPrintPegasusReclaimTicker)
		STRING sReclaim = "PEG_RECLAIM"
		IF IS_BIT_SET(bookedVehicle.iBitset,BOOK_BS_BOSS_LIMO)
			sReclaim = "GB_RECLAIM"
		ENDIF
		PRINT_TICKER(sReclaim) // pegasus are reclaiming this vehicle
		MPGlobalsAmbience.bPrintPegasusReclaimTicker = FALSE
	ENDIF
	
//	DELETE_REMOTE_MP_SAVED_VEHICLE_AFTER_FADE_OUT(MPGlobals.OldRemoteAvengerV[iStaggerPlayer])
ENDPROC

/// PURPOSE:
///    Clears a remote player's pegasus vehicle
/// PARAMS:
///    iStaggerPlayer - player ID of owner of pegasus vehicle to clear
PROC CLEAR_REMOTE_HACKER_TRUCK_VEHICLE(INT iStaggerPlayer)
	// cleanup hacker truck
	IF NOT IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, iStaggerPlayer), FALSE)	
	OR NOT NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_HackertruckV)
	OR (NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_HackertruckV) AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_HackertruckV))
		IF DOES_ENTITY_EXIST(MPGlobals.RemoteHackertruckV[iStaggerPlayer])
		AND IS_ENTITY_A_VEHICLE(MPGlobals.RemoteHackertruckV[iStaggerPlayer])
		AND IS_VEHICLE_A_PERSONAL_HACKER_TRUCK(MPGlobals.RemoteHackertruckV[iStaggerPlayer])
			
			NET_PRINT("CLEAR_REMOTE_HACKER_TRUCK_VEHICLE - remove Truck_1 cab exists for old player ") NET_PRINT_INT(iStaggerPlayer) NET_NL()
			
			NET_PRINT("CLEAR_REMOTE_HACKER_TRUCK_VEHICLE - IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, iStaggerPlayer), FALSE) = ")
			NET_PRINT_BOOL(IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, iStaggerPlayer), FALSE)) NET_NL()
			NET_PRINT("CLEAR_REMOTE_HACKER_TRUCK_VEHICLE - NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_HackertruckV) = ")
			NET_PRINT_BOOL(NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_HackertruckV)) NET_NL()
			NET_PRINT("CLEAR_REMOTE_HACKER_TRUCK_VEHICLE - (NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_HackertruckV) AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_HackertruckV)) = ")
			NET_PRINT_BOOL((NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_HackertruckV) AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_HackertruckV))) NET_NL()
			
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), MPGlobals.RemoteHackertruckV[iStaggerPlayer])
				SET_BIT(iInRemoteVehBS, iStaggerPlayer)
				NET_PRINT("CLEAR_REMOTE_HACKER_TRUCK_VEHICLE - SET_BIT(bookedVehicle.iBitset, BOOK_BS_PLAYER_WAS_IN_REMOTE_VEH)") NET_PRINT_INT(iStaggerPlayer) NET_NL()
			ENDIF
			IF NETWORK_HAS_CONTROL_OF_ENTITY(MPGlobals.RemoteHackertruckV[iStaggerPlayer])
				IF IS_VEHICLE_ATTACHED_TO_TRAILER(MPGlobals.RemoteHackertruckV[iStaggerPlayer])
					DETACH_VEHICLE_FROM_TRAILER(MPGlobals.RemoteHackertruckV[iStaggerPlayer])
					NET_PRINT("CLEAR_REMOTE_HACKER_TRUCK_VEHICLE - detach Truck_1 cab for old player ") NET_PRINT_INT(iStaggerPlayer) NET_NL()
					
					EXIT
				ENDIF
				
					MPGlobals.OldRemoteHackertruckV[iStaggerPlayer] = MPGlobals.RemoteHackertruckV[iStaggerPlayer]
				CLEANUP_REMOTE_MP_PEG_SAVED_VEHICLE(MPGlobals.RemoteHackertruckV[iStaggerPlayer]  , FALSE, -1  , TRUE )
			ENDIF
		ENDIF
	ELSE	
		IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_HackertruckV)			
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_HackertruckV)
				IF IS_ENTITY_A_VEHICLE(NET_TO_VEH(GlobalplayerBD[iStaggerPlayer].netID_HackertruckV))
					IF IS_VEHICLE_A_PERSONAL_HACKER_TRUCK(NET_TO_VEH(GlobalplayerBD[iStaggerPlayer].netID_HackertruckV))
						IF NOT (MPGlobals.RemoteHackertruckV[iStaggerPlayer] = NET_TO_VEH(GlobalplayerBD[iStaggerPlayer].netID_HackertruckV))
							MPGlobals.RemoteHackertruckV[iStaggerPlayer] = NET_TO_VEH(GlobalplayerBD[iStaggerPlayer].netID_HackertruckV)
							PRINTLN("CLEAR_REMOTE_HACKER_TRUCK_VEHICLE - updating RemoteHackertruckV for player ", iStaggerPlayer, " RemoteHackertruckV[0] = ", NATIVE_TO_INT(MPGlobals.RemoteHackertruckV[iStaggerPlayer]))
						ENDIF
					ENDIF
				ELSE
					PRINTLN("CLEAR_REMOTE_HACKER_TRUCK_VEHICLE - RemoteHackertruckV not a vehicle for player ", iStaggerPlayer, " RemoteHackertruckV[0] = ", NATIVE_TO_INT(MPGlobals.RemoteHackertruckV[iStaggerPlayer]))
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	
	// if the local player has just been kicked out of a pegasus vehicle then print a ticker message
	IF (MPGlobalsAmbience.bPrintPegasusReclaimTicker)
		MPGlobalsAmbience.bPrintPegasusReclaimTicker = FALSE
	ENDIF

ENDPROC

#IF FEATURE_DLC_2_2022
/// PURPOSE:
///    Clears a remote player's pegasus vehicle
/// PARAMS:
///    iStaggerPlayer - player ID of owner of pegasus vehicle to clear
PROC CLEAR_REMOTE_ACID_LAB_VEHICLE(INT iStaggerPlayer)
	// cleanup acid lab
	IF NOT IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, iStaggerPlayer), FALSE)	
	OR NOT NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_AcidLabV)
	OR (NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_AcidLabV) AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_AcidLabV))
		IF DOES_ENTITY_EXIST(MPGlobals.RemoteAcidLabV[iStaggerPlayer])
		AND IS_ENTITY_A_VEHICLE(MPGlobals.RemoteAcidLabV[iStaggerPlayer])
		AND IS_VEHICLE_A_PERSONAL_ACID_LAB(MPGlobals.RemoteAcidLabV[iStaggerPlayer])
			
			NET_PRINT("CLEAR_REMOTE_ACID_LAB_VEHICLE - remove AcidLab cab exists for old player ") NET_PRINT_INT(iStaggerPlayer) NET_NL()
			
			NET_PRINT("CLEAR_REMOTE_ACID_LAB_VEHICLE - IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, iStaggerPlayer), FALSE) = ")
			NET_PRINT_BOOL(IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, iStaggerPlayer), FALSE)) NET_NL()
			NET_PRINT("CLEAR_REMOTE_ACID_LAB_VEHICLE - NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_AcidLabV) = ")
			NET_PRINT_BOOL(NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_AcidLabV)) NET_NL()
			NET_PRINT("CLEAR_REMOTE_ACID_LAB_VEHICLE - (NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_AcidLabV) AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_AcidLabV)) = ")
			NET_PRINT_BOOL((NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_AcidLabV) AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_AcidLabV))) NET_NL()
			
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), MPGlobals.RemoteAcidLabV[iStaggerPlayer])
				SET_BIT(iInRemoteVehBS, iStaggerPlayer)
				NET_PRINT("CLEAR_REMOTE_ACID_LAB_VEHICLE - SET_BIT(bookedVehicle.iBitset, BOOK_BS_PLAYER_WAS_IN_REMOTE_VEH)") NET_PRINT_INT(iStaggerPlayer) NET_NL()
			ENDIF
			IF NETWORK_HAS_CONTROL_OF_ENTITY(MPGlobals.RemoteAcidLabV[iStaggerPlayer])
				IF IS_VEHICLE_ATTACHED_TO_TRAILER(MPGlobals.RemoteAcidLabV[iStaggerPlayer])
					DETACH_VEHICLE_FROM_TRAILER(MPGlobals.RemoteAcidLabV[iStaggerPlayer])
					NET_PRINT("CLEAR_REMOTE_ACID_LAB_VEHICLE - detach AcidLab cab for old player ") NET_PRINT_INT(iStaggerPlayer) NET_NL()
					
					EXIT
				ENDIF
				
				MPGlobals.OldRemoteAcidLabV[iStaggerPlayer] = MPGlobals.RemoteAcidLabV[iStaggerPlayer]
				CLEANUP_REMOTE_MP_PEG_SAVED_VEHICLE(MPGlobals.RemoteAcidLabV[iStaggerPlayer]  , FALSE, -1  , TRUE )
			ENDIF
		ENDIF
	ELSE	
		IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_AcidLabV)			
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_AcidLabV)
				IF IS_ENTITY_A_VEHICLE(NET_TO_VEH(GlobalplayerBD[iStaggerPlayer].netID_AcidLabV))
					IF IS_VEHICLE_A_PERSONAL_ACID_LAB(NET_TO_VEH(GlobalplayerBD[iStaggerPlayer].netID_AcidLabV))
						IF NOT (MPGlobals.RemoteAcidLabV[iStaggerPlayer] = NET_TO_VEH(GlobalplayerBD[iStaggerPlayer].netID_AcidLabV))
							MPGlobals.RemoteAcidLabV[iStaggerPlayer] = NET_TO_VEH(GlobalplayerBD[iStaggerPlayer].netID_AcidLabV)
							PRINTLN("CLEAR_REMOTE_ACID_LAB_VEHICLE - updating RemoteAcidLabV for player ", iStaggerPlayer, " RemoteAcidLabV[0] = ", NATIVE_TO_INT(MPGlobals.RemoteAcidLabV[iStaggerPlayer]))
						ENDIF
					ENDIF
				ELSE
					PRINTLN("CLEAR_REMOTE_ACID_LAB_VEHICLE - RemoteAcidLabV not a vehicle for player ", iStaggerPlayer, " RemoteAcidLabV[0] = ", NATIVE_TO_INT(MPGlobals.RemoteAcidLabV[iStaggerPlayer]))
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	
	// if the local player has just been kicked out of a pegasus vehicle then print a ticker message
	IF (MPGlobalsAmbience.bPrintPegasusReclaimTicker)
		MPGlobalsAmbience.bPrintPegasusReclaimTicker = FALSE
	ENDIF

ENDPROC
#ENDIF

PROC CLEANUP_FAKE_MOC_PED(INT iStaggerPlayer)

	IF NOT IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, iStaggerPlayer), FALSE)	
	OR NOT NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_FakeMocPed)
	OR (NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_FakeMocPed) AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_FakeMocPed))
		IF DOES_ENTITY_EXIST(MPGlobals.RemoteFakeMocPed[iStaggerPlayer])
			NET_PRINT("CLEANUP_FAKE_MOC_PED - remove ped exists for old player ") NET_PRINT_INT(iStaggerPlayer) NET_NL()
			
			NET_PRINT("CLEANUP_FAKE_MOC_PED - IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, iStaggerPlayer), FALSE) = ")
			NET_PRINT_BOOL(IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, iStaggerPlayer), FALSE)) NET_NL()
			NET_PRINT("CLEANUP_FAKE_MOC_PED - NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_FakeMocPed) = ")
			NET_PRINT_BOOL(NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_FakeMocPed)) NET_NL()
			NET_PRINT("CLEANUP_FAKE_MOC_PED - (NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_FakeMocPed) AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_FakeMocPed)) = ")
			NET_PRINT_BOOL((NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_FakeMocPed) AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_FakeMocPed))) NET_NL()
			
			IF NETWORK_HAS_CONTROL_OF_ENTITY(MPGlobals.RemoteFakeMocPed[iStaggerPlayer])
						
				IF NOT IS_ENTITY_DEAD(MPGlobals.RemoteFakeMocPed[iStaggerPlayer])
					DETACH_ENTITY(MPGlobals.RemoteFakeMocPed[iStaggerPlayer])
					VECTOR vPedPos = GET_ENTITY_COORDS(MPGlobals.RemoteFakeMocPed[iStaggerPlayer])
					vPedPos.z = ( vPedPos.z - 100 )
					SET_ENTITY_COORDS(MPGlobals.RemoteFakeMocPed[iStaggerPlayer], vPedPos)
					PRINTLN("CLEANUP_FAKE_MOC_PED, vPedPos = ", vPedPos)
				ENDIF
				
				IF NOT IS_ENTITY_A_MISSION_ENTITY(MPGlobals.RemoteFakeMocPed[iStaggerPlayer])
					SET_ENTITY_AS_NET_MISSION_ENTITY(GlobalplayerBD[iStaggerPlayer].netID_FakeMocPed, MPGlobals.RemoteFakeMocPed[iStaggerPlayer], FALSE, TRUE)
					DELETE_PED(MPGlobals.RemoteFakeMocPed[iStaggerPlayer])
					PRINTLN("CLEANUP_FAKE_MOC_PED, DELETE_PED 1 ")
				ELSE
					DELETE_PED(MPGlobals.RemoteFakeMocPed[iStaggerPlayer])
					PRINTLN("CLEANUP_FAKE_MOC_PED, DELETE_PED 2 ")
				ENDIF
			ENDIF
		ENDIF
	ELSE	
		IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_FakeMocPed)	
			PRINTLN("CLEANUP_FAKE_MOC_PED, 3 ")
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_FakeMocPed)
				PRINTLN("CLEANUP_FAKE_MOC_PED, 4 ")
				IF NOT (MPGlobals.RemoteFakeMocPed[iStaggerPlayer] = NET_TO_PED(GlobalplayerBD[iStaggerPlayer].netID_FakeMocPed))
					MPGlobals.RemoteFakeMocPed[iStaggerPlayer] = NET_TO_PED(GlobalplayerBD[iStaggerPlayer].netID_FakeMocPed)
					PRINTLN("CLEANUP_FAKE_MOC_PED - updating RemoteFakeMocPed for player ", iStaggerPlayer, " RemoteFakeMocPed[1] = ", NATIVE_TO_INT(MPGlobals.RemoteFakeMocPed[iStaggerPlayer]))
				ENDIF
			ENDIF
		ENDIF
	ENDIF	

ENDPROC

#IF FEATURE_HEIST_ISLAND

PROC MAINTAIN_REMOTE_VEHICLE(PLAYER_INDEX RemotePlayerId, NETWORK_INDEX VehNetId, VEHICLE_INDEX& VehId)
	
	PLAYER_INDEX LocalPlayerId = PLAYER_ID()
	
	IF IS_NET_PLAYER_OK(RemotePlayerId, FALSE, FALSE) AND NETWORK_DOES_NETWORK_ID_EXIST(VehNetId)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(VehNetId)
			VehId = NET_TO_VEH(VehNetId)
		ELSE
			VehId = INT_TO_NATIVE(VEHICLE_INDEX, 0)
		ENDIF
		
		IF IS_ENTITY_ALIVE(VehId)
			BOOL bIsFriendly = RemotePlayerId = LocalPlayerId OR GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG(RemotePlayerId, LocalPlayerId)
			BOOL bCanLockon = NOT bIsFriendly
			SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON(VehId, bCanLockon, TRUE)
		ENDIF
	ELSE
		IF DOES_ENTITY_EXIST(VehId) AND NETWORK_HAS_CONTROL_OF_ENTITY(VehId)
			CLEANUP_REMOTE_MP_PEG_SAVED_VEHICLE(VehId, FALSE, -1, TRUE)
		ENDIF
	ENDIF

ENDPROC



/// PURPOSE:
///    Maintains a remote player's submarine
/// PARAMS:
///    iStaggerPlayer - player ID of owner of vehicle to maintain
PROC MAINTAIN_REMOTE_SUBMARINE_VEHICLE(INT iStaggerPlayer)
	
	PLAYER_INDEX StaggerPlayerId = INT_TO_NATIVE(PLAYER_INDEX, iStaggerPlayer)
	NETWORK_INDEX SubVehNetId = GlobalplayerBD[iStaggerPlayer].netID_SubmarineV		
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(SubVehNetId)
		VEHICLE_INDEX SubVeh = NET_TO_VEH(SubVehNetId)
		IF IS_VEHICLE_A_PERSONAL_SUBMARINE(SubVeh)
			IF MPGlobals.RemoteSubmarineV[iStaggerPlayer] != SubVeh
				PRINTLN("MAINTAIN_REMOTE_SUBMARINE_VEHICLE - updating RemoteSubmarineV for iStaggerPlayer = ", iStaggerPlayer, ", SubVeh = ", NATIVE_TO_INT(SubVeh))
				MPGlobals.RemoteSubmarineV[iStaggerPlayer] = SubVeh
			ENDIF
		ELSE
			PRINTLN("MAINTAIN_REMOTE_SUBMARINE_VEHICLE - Vehicle not a submarine iStaggerPlayer = ", iStaggerPlayer, ", SubVeh = ", NATIVE_TO_INT(SubVeh))
		ENDIF
	ENDIF
	
	MAINTAIN_REMOTE_VEHICLE(StaggerPlayerId, GlobalplayerBD[iStaggerPlayer].netID_SubmarineV, MPGlobals.RemoteSubmarineV[iStaggerPlayer])

ENDPROC

/// PURPOSE:
///    Maintains a remote player's submarine dinghy
/// PARAMS:
///    iStaggerPlayer - player ID of owner of vehicle to maintain
PROC MAINTAIN_REMOTE_SUBMARINE_DINGHY_VEHICLE(INT iStaggerPlayer)
	
	PLAYER_INDEX StaggerPlayerId = INT_TO_NATIVE(PLAYER_INDEX, iStaggerPlayer)
	MAINTAIN_REMOTE_VEHICLE(StaggerPlayerId, GlobalplayerBD[iStaggerPlayer].netID_SubmarineDinghyV, MPGlobals.RemoteSubmarineDinghyV[iStaggerPlayer])

ENDPROC

#ENDIF

#IF FEATURE_DLC_2_2022
PROC MAINTAIN_REMOTE_SUPPORT_BIKE_VEHICLE(INT iStaggerPlayer)
	
	PLAYER_INDEX StaggerPlayerId = INT_TO_NATIVE(PLAYER_INDEX, iStaggerPlayer)
	
	NETWORK_INDEX SupBikeVehNetID = GlobalplayerBD[iStaggerPlayer].netID_SupportBikeV	
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(SupBikeVehNetID)
		VEHICLE_INDEX SupBikeVeh = NET_TO_VEH(SupBikeVehNetID)
		IF IS_VEHICLE_A_PERSONAL_SUPPORT_BIKE(SupBikeVeh)
			IF MPGlobals.RemoteSupportBikeV[iStaggerPlayer] != SupBikeVeh
				PRINTLN("MAINTAIN_REMOTE_SUPPORT_BIKE_VEHICLE - updating RemoteSupportBikeV for iStaggerPlayer = ", iStaggerPlayer, ", SupBikeVeh = ", NATIVE_TO_INT(SupBikeVeh))
				MPGlobals.RemoteSupportBikeV[iStaggerPlayer] = SupBikeVeh
			ENDIF
		ELSE
			PRINTLN("MAINTAIN_REMOTE_SUPPORT_BIKE_VEHICLE - Vehicle not a support bike for iStaggerPlayer = ", iStaggerPlayer, ", SupBikeVeh = ", NATIVE_TO_INT(SupBikeVeh))
		ENDIF
	ENDIF
	
	MAINTAIN_REMOTE_VEHICLE(StaggerPlayerId, GlobalplayerBD[iStaggerPlayer].netID_SupportBikeV, MPGlobals.RemoteSupportBikeV[iStaggerPlayer])
	
ENDPROC
#ENDIF
FUNC VEHICLE_INDEX GET_TRUCK_ID()
	
	RETURN g_viGunRunTruckTrailerRemote
ENDFUNC


FUNC BOOL SHOULD_MOC_PED_EXIST()

	IF DOES_ENTITY_EXIST(GET_TRUCK_ID())
	AND NOT IS_ENTITY_DEAD(GET_TRUCK_ID())
	AND IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
	AND IS_NET_PLAYER_OK(PLAYER_ID()) 
	AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0 
	
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

BOOL bRegisteredPed
VECTOR vCloseTruck[NUM_NETWORK_PLAYERS]
INT iCloseTruck

PROC MAINTAIN_FAKE_MOC_PED()
	
	IF !IS_BIT_SET(iLocalBS, LOCAL_BS_SHOULD_ALLOW_EXPENSIVE_PROCESSING)
		EXIT
	ENDIF
	
	IF IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
		EXIT
	ENDIF
	
	IF g_bOkToMakeMocPed
	
		CLEAR_BIT(iCloseTruck, iRemoteVehicleStagger)
		
		VEHICLE_INDEX mocVeh = MPGlobals.RemoteTruckV[iRemoteVehicleStagger][1]  
		
		IF DOES_ENTITY_EXIST(mocVeh)
		AND NOT IS_ENTITY_DEAD(mocVeh)
		
//			PRINTLN("    ->     [MOC_WANTED], [3704938], DOES_ENTITY_EXIST( iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])")
		
			VECTOR vPos = GET_ENTITY_COORDS(mocVeh)
			
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			
//				PRINTLN("    ->     [MOC_WANTED], [3704938], DOES_ENTITY_EXIST2( iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])")
			
				VECTOR vplayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		
				IF VDIST2(vPos, vplayerPos) < 10000.00
					SET_BIT(iCloseTruck, iRemoteVehicleStagger)
					vCloseTruck[iRemoteVehicleStagger] = vPos
					
					PRINTLN("    ->     [MOC_WANTED], [3704938], vCloseTruck( iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])", " vCloseTruck[iRemoteVehicleStagger] = ", vCloseTruck[iRemoteVehicleStagger])
//				ELSE
//					PRINTLN("    ->     [MOC_WANTED], [3704938], VDIST2 too far( iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])", " VDIST2(vPos, vplayerPos)  = ", VDIST2(vPos, vplayerPos) )
				ENDIF
			ENDIF
		
		ENDIF
		
		CONST_INT ciNEARBY_PEDS 10
		PED_INDEX pedArray[ciNEARBY_PEDS]
		
		INT iLoop2, iLoopCount
		PED_INDEX pedToCheck

		IF DOES_ENTITY_EXIST(MPGlobals.RemoteFakeMocPed[iRemoteVehicleStagger])
			SET_ENTITY_LOCALLY_INVISIBLE(MPGlobals.RemoteFakeMocPed[iRemoteVehicleStagger])
			PRINTLN("    ->     [MOC_WANTED], SET_ENTITY_LOCALLY_INVISIBLE  ")
		ENDIF
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iRemoteVehicleStagger].netID_FakeMocPed)
			SET_ENTITY_LOCALLY_INVISIBLE(NET_TO_ENT(GlobalplayerBD[iRemoteVehicleStagger].netID_FakeMocPed))
			PRINTLN("    ->     [MOC_WANTED], net id SET_ENTITY_LOCALLY_INVISIBLE  ")
		ENDIF
		
		IF IS_BIT_SET(iCloseTruck, iRemoteVehicleStagger)
			PRINTLN("    ->     [MOC_WANTED], FIX_FOR_3704938, IS_BIT_SET(iCloseTruck - iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])")
			IF GET_CLOSEST_PED(vCloseTruck[iRemoteVehicleStagger], 15.00, TRUE, TRUE, pedToCheck, FALSE, TRUE) 
				PRINTLN("    ->     [MOC_WANTED], FIX_FOR_3704938, GET_CLOSEST_PED - iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])")
				IF DOES_ENTITY_EXIST(pedToCheck)
					PRINTLN("    ->     [MOC_WANTED], FIX_FOR_3704938, DOES_ENTITY_EXIST - iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])")
					IF GET_ENTITY_MODEL(pedToCheck) = G_M_M_ChiGoon_02
						SET_ENTITY_LOCALLY_INVISIBLE(pedToCheck)
						PRINTLN("    ->     [MOC_WANTED], FIX_FOR_3704938, SET_ENTITY_LOCALLY_INVISIBLE(pedToCheck) - iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])")
						iLoopCount = GET_PED_NEARBY_PEDS(pedToCheck, pedArray)
						PRINTLN("    ->     [MOC_WANTED], FIX_FOR_3704938, iLoopCount = ", iLoopCount, "])")
						REPEAT iLoopCount iLoop2
							IF pedArray[iLoop2] != pedToCheck
								IF DOES_ENTITY_EXIST(pedArray[iLoop2])
									IF GET_ENTITY_MODEL(pedArray[iLoop2]) = G_M_M_ChiGoon_02
										SET_ENTITY_LOCALLY_INVISIBLE(pedArray[iLoop2])
										PRINTLN("    ->     [MOC_WANTED], FIX_FOR_3704938, SET_ENTITY_LOCALLY_INVISIBLE(pedArray[", iLoop2, "]) - iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])")
										IF GET_PED_RELATIONSHIP_GROUP_HASH(pedArray[iLoop2])  = rgFM_AiHatedByCopsAndMercs
										
											IF (IS_ENTITY_ALIVE(pedArray[iLoop2]) AND NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(pedArray[iLoop2]))
												IF NETWORK_HAS_CONTROL_OF_ENTITY(pedArray[iLoop2])
													SET_ENTITY_AS_MISSION_ENTITY(pedArray[iLoop2],FALSE,TRUE)
													PRINTLN("    ->     [MOC_WANTED], FIX_FOR_3704938, DELETE_PED1(pedArray[", iLoop2, "]) - iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])")
													CLEAR_AREA_OF_PEDS(GET_ENTITY_COORDS(pedArray[iLoop2]), 1.0)
													DELETE_PED(pedArray[iLoop2])
												ELSE
//													NETWORK_REQUEST_CONTROL_OF_ENTITY(pedArray[iLoop2])
													PRINTLN("[MOC_WANTED], FIX_FOR_3704938, NETWORK_HAS_CONTROL_OF_ENTITY, FALSE[", iLoop2, "]) - iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])")
												ENDIF
											ELSE
												IF (IS_ENTITY_ALIVE(pedArray[iLoop2]) AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(pedArray[iLoop2]))
													IF NETWORK_HAS_CONTROL_OF_ENTITY(pedArray[iLoop2])
														PRINTLN("    ->     [MOC_WANTED], FIX_FOR_3704938, DELETE_PED2(pedArray[", iLoop2, "]) - iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])")
														CLEAR_AREA_OF_PEDS(GET_ENTITY_COORDS(pedArray[iLoop2]), 1.0)
														DELETE_PED(pedArray[iLoop2])
													ELSE
														NETWORK_REQUEST_CONTROL_OF_ENTITY(pedArray[iLoop2])
													ENDIF
												ENDIF	
											ENDIF
										ENDIF
									ELSE
										PRINTLN("    ->     [MOC_WANTED], pedArray GET_ENTITY_MODEL = ", iLoop2, "])")
									ENDIF
								ELSE
									PRINTLN("    ->     [MOC_WANTED], pedArray DOES_ENTITY_EXIST = ", iLoop2, "])")
								ENDIF
							ELSE
								PRINTLN("    ->     [MOC_WANTED], pedArray SAME_PED = ", iLoop2, "])")
							ENDIF
						ENDREPEAT
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
		IF g_iHostOfam_mp_armory_truck = NETWORK_PLAYER_ID_TO_INT()

			// Create the ped
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[g_iHostOfam_mp_armory_truck].netID_FakeMocPed)
				IF SHOULD_MOC_PED_EXIST()
					REQUEST_MODEL(GET_MOC_PED_MODEL())
					
					IF HAS_MODEL_LOADED(GET_MOC_PED_MODEL())
						IF NOT bRegisteredPed
							IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_OBJECTS()+ 1,false,true)
								RESERVE_NETWORK_MISSION_PEDS(GET_NUM_CREATED_MISSION_PEDS() + 1) // For fake MOC wanted ped 
								bRegisteredPed = TRUE
							ENDIF
						ELSE
							IF CAN_REGISTER_MISSION_PEDS(1)
								CREATE_NET_PED(GlobalplayerBD[NETWORK_PLAYER_ID_TO_INT()].netID_FakeMocPed, PEDTYPE_MISSION, GET_MOC_PED_MODEL(), GET_ENTITY_COORDS(GET_TRUCK_ID()), GET_ENTITY_HEADING(GET_TRUCK_ID()), FALSE)
								SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(GlobalplayerBD[NETWORK_PLAYER_ID_TO_INT()].netID_FakeMocPed), rgFM_AiHatedByCopsAndMercs)
								SET_ENTITY_INVINCIBLE(NET_TO_PED(GlobalplayerBD[NETWORK_PLAYER_ID_TO_INT()].netID_FakeMocPed), TRUE)
								SET_ENTITY_VISIBLE(NET_TO_PED(GlobalplayerBD[NETWORK_PLAYER_ID_TO_INT()].netID_FakeMocPed), FALSE)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(GlobalplayerBD[NETWORK_PLAYER_ID_TO_INT()].netID_FakeMocPed), TRUE)
								SET_MODEL_AS_NO_LONGER_NEEDED(GET_MOC_PED_MODEL())
								
								SET_PED_CONFIG_FLAG(NET_TO_PED(GlobalplayerBD[NETWORK_PLAYER_ID_TO_INT()].netID_FakeMocPed), PCF_RunFromFiresAndExplosions, FALSE)
								SET_PED_CONFIG_FLAG(NET_TO_PED(GlobalplayerBD[NETWORK_PLAYER_ID_TO_INT()].netID_FakeMocPed), PCF_DontActivateRagdollFromExplosions, TRUE)
								SET_PED_CONFIG_FLAG(NET_TO_PED(GlobalplayerBD[NETWORK_PLAYER_ID_TO_INT()].netID_FakeMocPed), PCF_DisableExplosionReactions, TRUE)
								SET_ENTITY_PROOFS(NET_TO_PED(GlobalplayerBD[NETWORK_PLAYER_ID_TO_INT()].netID_FakeMocPed), TRUE, TRUE, TRUE, TRUE, TRUE)
								
								PRINTLN("    ->     [MOC_WANTED] MAINTAIN_FAKE_MOC_PED, DONE ")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				// Maintain the ped				
				//IF TAKE_CONTROL_OF_NET_ID(GlobalplayerBD[g_iHostOfam_mp_armory_truck].netID_FakeMocPed)
				
					IF SHOULD_MOC_PED_EXIST()
					
						PED_INDEX pedMoc
						pedMoc = NET_TO_PED(GlobalplayerBD[g_iHostOfam_mp_armory_truck].netID_FakeMocPed)
			
						IF DOES_ENTITY_EXIST(pedMoc)
						AND NOT IS_ENTITY_DEAD(pedMoc)
							IF !IS_ENTITY_ATTACHED(NET_TO_PED(GlobalplayerBD[g_iHostOfam_mp_armory_truck].netID_FakeMocPed))
								IF TAKE_CONTROL_OF_NET_ID(GlobalplayerBD[g_iHostOfam_mp_armory_truck].netID_FakeMocPed)
									SET_ENTITY_VISIBLE(NET_TO_PED(GlobalplayerBD[g_iHostOfam_mp_armory_truck].netID_FakeMocPed), FALSE)
								
									INT iBone = GET_ENTITY_BONE_INDEX_BY_NAME(pedMoc, "chassis_dummy")
									
									IF NOT IS_ENTITY_ATTACHED(pedMoc)
										ATTACH_ENTITY_TO_ENTITY(pedMoc, GET_TRUCK_ID(), iBone, <<0,1,2.5>>, <<0,0,0>>)
										SET_ENTITY_COLLISION(NET_TO_PED(GlobalplayerBD[g_iHostOfam_mp_armory_truck].netID_FakeMocPed), FALSE)
										PRINTLN("    ->     [MOC_WANTED] MAINTAIN_FAKE_MOC_PED, ATTACH_ENTITY_TO_ENTITY ")
									ENDIF
								ENDIF	
							ENDIF	
						ELSE
							PRINTLN("    ->     [MOC_WANTED] MAINTAIN_FAKE_MOC_PED, DOES_ENTITY_EXIST, FALSE ")
						ENDIF
					ELSE
						IF DOES_ENTITY_EXIST(MPGlobals.RemoteFakeMocPed[g_iHostOfam_mp_armory_truck])
							IF TAKE_CONTROL_OF_NET_ID(GlobalplayerBD[g_iHostOfam_mp_armory_truck].netID_FakeMocPed)
								PRINTLN("    ->     [MOC_WANTED] MAINTAIN_FAKE_MOC_PED, DELETE_NET_ID ")
								SET_ENTITY_AS_NET_MISSION_ENTITY(GlobalplayerBD[g_iHostOfam_mp_armory_truck].netID_FakeMocPed, MPGlobals.RemoteFakeMocPed[g_iHostOfam_mp_armory_truck], FALSE, TRUE)
								DELETE_PED(MPGlobals.RemoteFakeMocPed[g_iHostOfam_mp_armory_truck])
								DELETE_NET_ID(GlobalplayerBD[g_iHostOfam_mp_armory_truck].netID_FakeMocPed)
								IF bRegisteredPed
									IF GET_NUM_CREATED_MISSION_PEDS() > 0
										RESERVE_NETWORK_MISSION_PEDS(GET_NUM_CREATED_MISSION_PEDS() - 1) // For fake MOC wanted ped 
									ENDIF
									bRegisteredPed = FALSE
								ENDIF
							ENDIF	
						ENDIF	
					ENDIF
//				ELSE
//					PRINTLN("    ->     [MOC_WANTED] MAINTAIN_FAKE_MOC_PED, MAINTAIN_CONTROL_OF_NETWORK_ID ")
//				ENDIF
			ENDIF
		// If host changes, delete the ped
		ELSE
			IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NETWORK_PLAYER_ID_TO_INT()].netID_FakeMocPed)
				IF TAKE_CONTROL_OF_NET_ID(GlobalplayerBD[NETWORK_PLAYER_ID_TO_INT()].netID_FakeMocPed)
					SET_ENTITY_AS_NET_MISSION_ENTITY(GlobalplayerBD[NETWORK_PLAYER_ID_TO_INT()].netID_FakeMocPed, MPGlobals.RemoteFakeMocPed[NETWORK_PLAYER_ID_TO_INT()], FALSE, TRUE)
					PRINTLN("    ->     [MOC_WANTED] MAINTAIN_FAKE_MOC_PED, HOST CHANGE, DELETE_NET_ID  ")
					DELETE_PED(MPGlobals.RemoteFakeMocPed[NETWORK_PLAYER_ID_TO_INT()])
					DELETE_NET_ID(GlobalplayerBD[NETWORK_PLAYER_ID_TO_INT()].netID_FakeMocPed)
					IF bRegisteredPed
						IF GET_NUM_CREATED_MISSION_PEDS() > 0
							RESERVE_NETWORK_MISSION_PEDS(GET_NUM_CREATED_MISSION_PEDS() - 1) // For fake MOC wanted ped 
						ENDIF
						bRegisteredPed = FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		PRINTLN("    ->     [MOC_WANTED] MAINTAIN_FAKE_MOC_PED, g_bOkToMakeMocPed = FALSE ")
	ENDIF
ENDPROC

FUNC VEHICLE_INDEX GET_AVENGER_VEHICLE_INDEX()
	VEHICLE_INDEX avengerVeh
	IF !NETWORK_IS_ACTIVITY_SESSION()

		IF g_ownerOfArmoryAircraftPropertyIAmIn != INVALID_PLAYER_INDEX()
		AND IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
			IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryAircraftPropertyIAmIn)].netID_AvengerV) 
				avengerVeh = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryAircraftPropertyIAmIn)].netID_AvengerV)
			ENDIF
		ELSE

			VEHICLE_INDEX vehIn
			PLAYER_INDEX playerOwner
			
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				
					vehIn = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				
					IF IS_VEHICLE_A_PERSONAL_ARMORY_AIRCRAFT(vehIn) 
					
						playerOwner = GET_OWNER_OF_PERSONAL_ARMORY_AIRCRAFT(vehIn)
						
						IF IS_NET_PLAYER_OK(playerOwner)
						
							IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(playerOwner)].netID_AvengerV) 
								avengerVeh = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(playerOwner)].netID_AvengerV)
							ENDIF
						ENDIF
					ENDIF
				
				ENDIF
			ENDIF
		ENDIF
	ELSE	
		IF g_OwnerOfArmoryAircraftPropertyIAmIn != INVALID_PLAYER_INDEX()
		AND IS_PLAYER_IN_CREATOR_AIRCRAFT(PLAYER_ID())
			avengerVeh = MPGlobalsAmbience.vehCreatorAircraft
		ELSE

			PLAYER_INDEX playerOwner
			
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						
					IF IS_VEHICLE_A_CREATOR_AIRCRAFT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) 
						IF IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), AVENGER)
							playerOwner = GET_OWNER_OF_PERSONAL_ARMORY_AIRCRAFT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
							
							IF IS_NET_PLAYER_OK(playerOwner)
								IF playerOwner = GB_GET_LOCAL_PLAYER_GANG_BOSS() 
									avengerVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
								ENDIF
							ENDIF	
						ENDIF	
					ENDIF
				
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN avengerVeh
ENDFUNC

PROC CLEANUP_AVENGER_MOC_PED(INT iStaggerPlayer)

	IF NOT IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, iStaggerPlayer), FALSE)	
	OR NOT NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_FakeAvengerPed)
	OR (NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_FakeAvengerPed) AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_FakeAvengerPed))
		IF DOES_ENTITY_EXIST(MPGlobals.RemoteFakeAvengerPed[iStaggerPlayer])
			NET_PRINT("CLEANUP_AVENGER_MOC_PED - remove ped exists for old player ") NET_PRINT_INT(iStaggerPlayer) NET_NL()
			
			NET_PRINT("CLEANUP_AVENGER_MOC_PED - IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, iStaggerPlayer), FALSE) = ")
			NET_PRINT_BOOL(IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, iStaggerPlayer), FALSE)) NET_NL()
			NET_PRINT("CLEANUP_AVENGER_MOC_PED - NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_FakeAvengerPed) = ")
			NET_PRINT_BOOL(NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_FakeAvengerPed)) NET_NL()
			NET_PRINT("CLEANUP_AVENGER_MOC_PED - (NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_FakeAvengerPed) AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_FakeAvengerPed)) = ")
			NET_PRINT_BOOL((NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_FakeAvengerPed) AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_FakeAvengerPed))) NET_NL()
			
			IF NETWORK_HAS_CONTROL_OF_ENTITY(MPGlobals.RemoteFakeAvengerPed[iStaggerPlayer])
						
				IF NOT IS_ENTITY_DEAD(MPGlobals.RemoteFakeAvengerPed[iStaggerPlayer])
					DETACH_ENTITY(MPGlobals.RemoteFakeAvengerPed[iStaggerPlayer])
					VECTOR vPedPos = GET_ENTITY_COORDS(MPGlobals.RemoteFakeAvengerPed[iStaggerPlayer])
					vPedPos.z = ( vPedPos.z - 100 )
					SET_ENTITY_COORDS(MPGlobals.RemoteFakeAvengerPed[iStaggerPlayer], vPedPos)
					PRINTLN("CLEANUP_AVENGER_MOC_PED, vPedPos = ", vPedPos)
				ENDIF
				
				IF NOT IS_ENTITY_A_MISSION_ENTITY(MPGlobals.RemoteFakeAvengerPed[iStaggerPlayer])
					SET_ENTITY_AS_NET_MISSION_ENTITY(GlobalplayerBD[iStaggerPlayer].netID_FakeAvengerPed, MPGlobals.RemoteFakeAvengerPed[iStaggerPlayer], FALSE, TRUE)
					DELETE_PED(MPGlobals.RemoteFakeAvengerPed[iStaggerPlayer])
					PRINTLN("CLEANUP_AVENGER_MOC_PED, DELETE_PED 1 ")
				ELSE
					DELETE_PED(MPGlobals.RemoteFakeAvengerPed[iStaggerPlayer])
					PRINTLN("CLEANUP_AVENGER_MOC_PED, DELETE_PED 2 ")
				ENDIF
			ENDIF
		ENDIF
	ELSE	
		IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_FakeAvengerPed)	
			PRINTLN("CLEANUP_AVENGER_MOC_PED, 3 ")
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_FakeAvengerPed)
				PRINTLN("CLEANUP_AVENGER_MOC_PED, 4 ")
				IF NOT (MPGlobals.RemoteFakeAvengerPed[iStaggerPlayer] = NET_TO_PED(GlobalplayerBD[iStaggerPlayer].netID_FakeAvengerPed))
					MPGlobals.RemoteFakeAvengerPed[iStaggerPlayer] = NET_TO_PED(GlobalplayerBD[iStaggerPlayer].netID_FakeAvengerPed)
					PRINTLN("CLEANUP_AVENGER_MOC_PED - updating RemoteFakeAvengerPed for player ", iStaggerPlayer, " RemoteFakeAvengerPed[1] = ", NATIVE_TO_INT(MPGlobals.RemoteFakeAvengerPed[iStaggerPlayer]))
				ENDIF
			ENDIF
		ENDIF
	ENDIF	

ENDPROC

FUNC BOOL SHOULD_AVENGER_PED_EXIST()
	IF !NETWORK_IS_ACTIVITY_SESSION()
		IF DOES_ENTITY_EXIST(GET_AVENGER_VEHICLE_INDEX())
		AND NOT IS_ENTITY_DEAD(GET_AVENGER_VEHICLE_INDEX())
		AND IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
		AND IS_NET_PLAYER_OK(PLAYER_ID()) 
		AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0 
		
			RETURN TRUE
		ENDIF
	ELSE
		IF DOES_ENTITY_EXIST(GET_AVENGER_VEHICLE_INDEX())
		AND NOT IS_ENTITY_DEAD(GET_AVENGER_VEHICLE_INDEX())
		AND IS_PLAYER_IN_CREATOR_AIRCRAFT(PLAYER_ID())
		AND IS_NET_PLAYER_OK(PLAYER_ID()) 
		AND NOT SHOULD_CREATOR_AIRCRAFT_SPAWN_PLAYER_IN_HANGAR_INTERIOR()
			RETURN TRUE
		ENDIF
	
	ENDIF

	RETURN FALSE
ENDFUNC

BOOL bRegisteredAvengerPed
VECTOR vCloseAvenger[NUM_NETWORK_PLAYERS]
INT iCloseAvenger

PROC MAINTAIN_FAKE_AVENGER_PED()
	
	IF !IS_BIT_SET(iLocalBS, LOCAL_BS_SHOULD_ALLOW_EXPENSIVE_PROCESSING)
		EXIT
	ENDIF
	
	CLEAR_BIT(iCloseAvenger, iRemoteVehicleStagger)
	
	VEHICLE_INDEX avengerVeh 
	
	IF !NETWORK_IS_ACTIVITY_SESSION()
	
		avengerVeh = MPGlobals.RemoteAvengerV[iRemoteVehicleStagger]
	ELSE
		avengerVeh = GET_AVENGER_VEHICLE_INDEX()
	ENDIF
	
	IF DOES_ENTITY_EXIST(avengerVeh)
	AND NOT IS_ENTITY_DEAD(avengerVeh)
	
		VECTOR vPos = GET_ENTITY_COORDS(avengerVeh)
		
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		
			VECTOR vplayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	
			IF VDIST2(vPos, vplayerPos) < 10000.00
				SET_BIT(iCloseAvenger, iRemoteVehicleStagger)
				vCloseAvenger[iRemoteVehicleStagger] = vPos
				
				PRINTLN("    ->     [AVENGER_WANTED], [3704938], vCloseAvenger( iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])", " vCloseAvenger[iRemoteVehicleStagger] = ", vCloseAvenger[iRemoteVehicleStagger])
			ENDIF
		ENDIF
	
	ENDIF
	
	CONST_INT ciNEARBY_PEDS 10
	PED_INDEX pedArray[ciNEARBY_PEDS]
	
	INT iLoop2, iLoopCount
	PED_INDEX pedToCheck

	IF DOES_ENTITY_EXIST(MPGlobals.RemoteFakeAvengerPed[iRemoteVehicleStagger])
		IF NOT IS_ENTITY_DEAD(MPGlobals.RemoteFakeAvengerPed[iRemoteVehicleStagger])
			SET_ENTITY_LOCALLY_INVISIBLE(MPGlobals.RemoteFakeAvengerPed[iRemoteVehicleStagger])
			PRINTLN("    ->     [AVENGER_WANTED], SET_ENTITY_LOCALLY_INVISIBLE  ")
		ENDIF	
	ELSE
		IF IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
		OR IS_PLAYER_IN_CREATOR_AIRCRAFT(PLAYER_ID())
			IF IS_ENTITY_ALIVE(GET_AVENGER_VEHICLE_INDEX())
				VECTOR vPedCooordtoCheck = GET_ENTITY_COORDS(GET_AVENGER_VEHICLE_INDEX()) - <<0,0,-10>>
				IF GET_CLOSEST_PED(vPedCooordtoCheck, 30.00, TRUE, TRUE, pedToCheck, FALSE, TRUE) 
					IF DOES_ENTITY_EXIST(pedToCheck)
						IF GET_ENTITY_MODEL(pedToCheck) = GET_MOC_PED_MODEL()
							SET_ENTITY_LOCALLY_INVISIBLE(pedToCheck)
							PRINTLN("    ->     [AVENGER_WANTED], MPGlobals.RemoteFakeAvengerPed doesn't exist found nearest ped SET_ENTITY_LOCALLY_INVISIBLE  ")
						ENDIF
					ENDIF	
				ENDIF
			ENDIF	
		ENDIF	
	ENDIF

	IF IS_BIT_SET(iCloseAvenger, iRemoteVehicleStagger)
		//PRINTLN("    ->     [AVENGER_WANTED], FIX_FOR_3704938, IS_BIT_SET(iCloseAvenger - iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])")
		IF GET_CLOSEST_PED(vCloseAvenger[iRemoteVehicleStagger], 15.00, TRUE, TRUE, pedToCheck, FALSE, TRUE) 
			//PRINTLN("    ->     [AVENGER_WANTED], FIX_FOR_3704938, GET_CLOSEST_PED - iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])")
			IF DOES_ENTITY_EXIST(pedToCheck)
				//PRINTLN("    ->     [AVENGER_WANTED], FIX_FOR_3704938, DOES_ENTITY_EXIST - iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])")
				IF GET_ENTITY_MODEL(pedToCheck) = GET_MOC_PED_MODEL()
					SET_ENTITY_LOCALLY_INVISIBLE(pedToCheck)
					PRINTLN("    ->     [AVENGER_WANTED],  SET_ENTITY_LOCALLY_INVISIBLE(pedToCheck) - iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])")
					iLoopCount = GET_PED_NEARBY_PEDS(pedToCheck, pedArray)
					//PRINTLN("    ->     [AVENGER_WANTED], FIX_FOR_3704938, iLoopCount = ", iLoopCount, "])")
					REPEAT iLoopCount iLoop2
						IF pedArray[iLoop2] != pedToCheck
							IF DOES_ENTITY_EXIST(pedArray[iLoop2])
								IF GET_ENTITY_MODEL(pedArray[iLoop2]) = GET_MOC_PED_MODEL()
									SET_ENTITY_LOCALLY_INVISIBLE(pedArray[iLoop2])
									//PRINTLN("    ->     [AVENGER_WANTED], FIX_FOR_3704938, SET_ENTITY_LOCALLY_INVISIBLE(pedArray[", iLoop2, "]) - iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])")
									IF GET_PED_RELATIONSHIP_GROUP_HASH(pedArray[iLoop2])  = rgFM_AiHatedByCopsAndMercs
										IF NOT IS_ENTITY_DEAD(pedArray[iLoop2])
											IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(pedArray[iLoop2])
												IF NETWORK_HAS_CONTROL_OF_ENTITY(pedArray[iLoop2])
													SET_ENTITY_AS_MISSION_ENTITY(pedArray[iLoop2],FALSE,TRUE)
													PRINTLN("    ->     [AVENGER_WANTED], FIX_FOR_3704938, DELETE_PED1(pedArray[", iLoop2, "]) - iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])")
													CLEAR_AREA_OF_PEDS(GET_ENTITY_COORDS(pedArray[iLoop2]), 1.0)
													IF NETWORK_HAS_CONTROL_OF_ENTITY(pedArray[iLoop2])
														DELETE_PED(pedArray[iLoop2])
													ELSE
														NETWORK_REQUEST_CONTROL_OF_ENTITY(pedArray[iLoop2])
													ENDIF
												ELSE
	//													NETWORK_REQUEST_CONTROL_OF_ENTITY(pedArray[iLoop2])
													PRINTLN("[AVENGER_WANTED],  NETWORK_HAS_CONTROL_OF_ENTITY, FALSE[", iLoop2, "]) - iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])")
												ENDIF
											ELSE
												PRINTLN("    ->     [AVENGER_WANTED],  DELETE_PED2(pedArray[", iLoop2, "]) - iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])")
												IF NETWORK_HAS_CONTROL_OF_ENTITY(pedArray[iLoop2])
													CLEAR_AREA_OF_PEDS(GET_ENTITY_COORDS(pedArray[iLoop2]), 1.0)
													DELETE_PED(pedArray[iLoop2])
												ELSE
													NETWORK_REQUEST_CONTROL_OF_ENTITY(pedArray[iLoop2])
												ENDIF	
											ENDIF
										ENDIF	
									ENDIF
								ELSE
									PRINTLN("    ->     [AVENGER_WANTED], pedArray GET_ENTITY_MODEL = ", iLoop2, "])")
								ENDIF
							ELSE
								PRINTLN("    ->     [AVENGER_WANTED], pedArray DOES_ENTITY_EXIST = ", iLoop2, "])")
							ENDIF
						ELSE
							PRINTLN("    ->     [AVENGER_WANTED], pedArray SAME_PED = ", iLoop2, "])")
						ENDIF
					ENDREPEAT
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF g_iHostOfam_mp_armory_aircraft = NATIVE_TO_INT(PLAYER_ID())

		// Create the ped
		IF g_iHostOfam_mp_armory_aircraft  != -1
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[g_iHostOfam_mp_armory_aircraft].netID_FakeAvengerPed)
				IF SHOULD_AVENGER_PED_EXIST()
					REQUEST_MODEL(GET_MOC_PED_MODEL())
					
					IF HAS_MODEL_LOADED(GET_MOC_PED_MODEL())
						IF NOT bRegisteredAvengerPed
							IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_OBJECTS()+ 1, FALSE, TRUE)
								RESERVE_NETWORK_MISSION_PEDS(GET_NUM_CREATED_MISSION_PEDS() + 1) // For fake Avenger wanted ped 
								bRegisteredAvengerPed = TRUE
							ENDIF
						ELSE
							IF CAN_REGISTER_MISSION_PEDS(1)
								VECTOR vPedCooord = GET_ENTITY_COORDS(GET_AVENGER_VEHICLE_INDEX()) - <<0,0,-3>>
								CREATE_NET_PED(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeAvengerPed, PEDTYPE_MISSION, GET_MOC_PED_MODEL(), vPedCooord, GET_ENTITY_HEADING(GET_AVENGER_VEHICLE_INDEX()), FALSE)
								SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeAvengerPed, PLAYER_ID(), TRUE)
								//SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeAvengerPed, TRUE)
								SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeAvengerPed), rgFM_AiHatedByCopsAndMercs)
								SET_ENTITY_INVINCIBLE(NET_TO_PED(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeAvengerPed), TRUE)
								SET_ENTITY_VISIBLE(NET_TO_PED(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeAvengerPed), FALSE)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeAvengerPed), TRUE)
								SET_MODEL_AS_NO_LONGER_NEEDED(GET_MOC_PED_MODEL())
								
								SET_PED_CONFIG_FLAG(NET_TO_PED(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeAvengerPed), PCF_RunFromFiresAndExplosions, FALSE)
								SET_PED_CONFIG_FLAG(NET_TO_PED(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeAvengerPed), PCF_DontActivateRagdollFromExplosions, TRUE)
								SET_PED_CONFIG_FLAG(NET_TO_PED(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeAvengerPed), PCF_DisableExplosionReactions, TRUE)
								SET_ENTITY_PROOFS(NET_TO_PED(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeAvengerPed), TRUE, TRUE, TRUE, TRUE, TRUE)
								
								PRINTLN("    ->     [AVENGER_WANTED] MAINTAIN_FAKE_AVENGER_PED, DONE ")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				// Maintain the ped				
				IF TAKE_CONTROL_OF_NET_ID(GlobalplayerBD[g_iHostOfam_mp_armory_aircraft].netID_FakeAvengerPed)
				
					IF SHOULD_AVENGER_PED_EXIST()
					
						PED_INDEX pedMoc
						pedMoc = NET_TO_PED(GlobalplayerBD[g_iHostOfam_mp_armory_aircraft].netID_FakeAvengerPed)
			
						IF DOES_ENTITY_EXIST(pedMoc)
						AND NOT IS_ENTITY_DEAD(pedMoc)
						
							SET_ENTITY_VISIBLE(NET_TO_PED(GlobalplayerBD[g_iHostOfam_mp_armory_aircraft].netID_FakeAvengerPed), FALSE)
						
							INT iBone = GET_ENTITY_BONE_INDEX_BY_NAME(pedMoc, "chassis_dummy")
							
							IF NOT IS_ENTITY_ATTACHED(pedMoc)
								ATTACH_ENTITY_TO_ENTITY(pedMoc, GET_AVENGER_VEHICLE_INDEX(), iBone, <<0,1,1.8>>, <<0,0,0>>)
								SET_ENTITY_COLLISION(NET_TO_PED(GlobalplayerBD[g_iHostOfam_mp_armory_aircraft].netID_FakeAvengerPed), FALSE)
								SET_ENTITY_COMPLETELY_DISABLE_COLLISION(NET_TO_PED(GlobalplayerBD[g_iHostOfam_mp_armory_aircraft].netID_FakeAvengerPed), FALSE)
								PRINTLN("    ->     [AVENGER_WANTED] MAINTAIN_FAKE_AVENGER_PED, ATTACH_ENTITY_TO_ENTITY ")
							ENDIF
						ELSE
							PRINTLN("    ->     [AVENGER_WANTED] MAINTAIN_FAKE_AVENGER_PED, DOES_ENTITY_EXIST, FALSE ")
						ENDIF
					ELSE
						IF DOES_ENTITY_EXIST(MPGlobals.RemoteFakeAvengerPed[g_iHostOfam_mp_armory_aircraft])
							PRINTLN("    ->     [AVENGER_WANTED] MAINTAIN_FAKE_AVENGER_PED, DELETE_NET_ID ")
							SET_ENTITY_AS_NET_MISSION_ENTITY(GlobalplayerBD[g_iHostOfam_mp_armory_aircraft].netID_FakeAvengerPed, MPGlobals.RemoteFakeAvengerPed[g_iHostOfam_mp_armory_aircraft], FALSE, TRUE)
							DELETE_PED(MPGlobals.RemoteFakeAvengerPed[g_iHostOfam_mp_armory_aircraft])
							DELETE_NET_ID(GlobalplayerBD[g_iHostOfam_mp_armory_aircraft].netID_FakeAvengerPed)
							IF bRegisteredAvengerPed
								IF GET_NUM_CREATED_MISSION_PEDS() > 0
									RESERVE_NETWORK_MISSION_PEDS(GET_NUM_CREATED_MISSION_PEDS() - 1) // For fake Avenger wanted ped 
								ENDIF
								bRegisteredAvengerPed = FALSE
							ENDIF
						ENDIF	
					ENDIF
				ELSE
					PRINTLN("    ->     [AVENGER_WANTED] MAINTAIN_FAKE_AVENGER_PED, MAINTAIN_CONTROL_OF_NETWORK_ID ")
				ENDIF
			ENDIF
		ENDIF	
	// If host changes, delete the ped
	ELSE
		IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NETWORK_PLAYER_ID_TO_INT()].netID_FakeAvengerPed)
			IF TAKE_CONTROL_OF_NET_ID(GlobalplayerBD[NETWORK_PLAYER_ID_TO_INT()].netID_FakeAvengerPed)
				IF DOES_ENTITY_EXIST(MPGlobals.RemoteFakeAvengerPed[NETWORK_PLAYER_ID_TO_INT()])
					SET_ENTITY_AS_NET_MISSION_ENTITY(GlobalplayerBD[NETWORK_PLAYER_ID_TO_INT()].netID_FakeAvengerPed, MPGlobals.RemoteFakeAvengerPed[NETWORK_PLAYER_ID_TO_INT()], FALSE, TRUE)
					PRINTLN("    ->     [AVENGER_WANTED] MAINTAIN_FAKE_AVENGER_PED, HOST CHANGE, DELETE_NET_ID  ")
					DELETE_PED(MPGlobals.RemoteFakeAvengerPed[NETWORK_PLAYER_ID_TO_INT()])
					DELETE_NET_ID(GlobalplayerBD[NETWORK_PLAYER_ID_TO_INT()].netID_FakeAvengerPed)
					IF bRegisteredAvengerPed
						IF GET_NUM_CREATED_MISSION_PEDS() > 0
							RESERVE_NETWORK_MISSION_PEDS(GET_NUM_CREATED_MISSION_PEDS() - 1) // For fake Avenger wanted ped 
						ENDIF
						bRegisteredAvengerPed = FALSE
					ENDIF
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC VEHICLE_INDEX GET_HACKER_TRUCK_VEHICLE_INDEX()
	VEHICLE_INDEX hackerTruckVeh
	IF !NETWORK_IS_ACTIVITY_SESSION()

		IF IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
		AND globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner != INVALID_PLAYER_INDEX()
			IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner)].netID_HackertruckV) 
				hackerTruckVeh = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner)].netID_HackertruckV)
			ENDIF
		ELSE

			VEHICLE_INDEX vehIn
			PLAYER_INDEX playerOwner
			
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				
					vehIn = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				
					IF IS_VEHICLE_A_PERSONAL_HACKER_TRUCK(vehIn) 
					
						playerOwner = GET_OWNER_OF_PERSONAL_HACKER_TRUCK(vehIn)
						
						IF IS_NET_PLAYER_OK(playerOwner)
						
							IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(playerOwner)].netID_HackertruckV) 
								hackerTruckVeh = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(playerOwner)].netID_HackertruckV)
							ENDIF
						ENDIF
					ENDIF
				
				ENDIF
			ENDIF
		ENDIF
	ELSE
	
	ENDIF
	RETURN hackerTruckVeh
ENDFUNC

FUNC BOOL SHOULD_HACKER_TRUCK_PED_EXIST()
	IF !NETWORK_IS_ACTIVITY_SESSION()
		IF DOES_ENTITY_EXIST(GET_HACKER_TRUCK_VEHICLE_INDEX())
		AND NOT IS_ENTITY_DEAD(GET_HACKER_TRUCK_VEHICLE_INDEX())
		AND IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
		AND IS_NET_PLAYER_OK(PLAYER_ID()) 
		AND (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0   OR IS_ANY_HACEKR_TRUCK_PARTICIPANT_USING_MISSILES())
			RETURN TRUE
		ENDIF
	ELSE

	ENDIF

	RETURN FALSE
ENDFUNC

PROC CLEANUP_FAKE_HCAKER_TRUCK_PED(INT iStaggerPlayer)

	IF NOT IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, iStaggerPlayer), FALSE)	
	OR NOT NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_FakeHackerTruckPed)
	OR (NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_FakeHackerTruckPed) AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_FakeHackerTruckPed))

		IF DOES_ENTITY_EXIST(MPGlobals.RemoteFakeHackerTruckPed[iStaggerPlayer])
			IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(MPGlobals.RemoteFakeHackerTruckPed[iStaggerPlayer], FALSE)
				NET_PRINT("CLEANUP_FAKE_HCAKER_TRUCK_PED - remove ped exists for old player ") NET_PRINT_INT(iStaggerPlayer) NET_NL()
				
				NET_PRINT("CLEANUP_FAKE_HCAKER_TRUCK_PED - IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, iStaggerPlayer), FALSE) = ")
				NET_PRINT_BOOL(IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, iStaggerPlayer), FALSE)) NET_NL()
				NET_PRINT("CLEANUP_FAKE_HCAKER_TRUCK_PED - NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_FakeHackerTruckPed) = ")
				NET_PRINT_BOOL(NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_FakeHackerTruckPed)) NET_NL()
				NET_PRINT("CLEANUP_FAKE_HCAKER_TRUCK_PED - (NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_FakeHackerTruckPed) AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_FakeHackerTruckPed)) = ")
				NET_PRINT_BOOL((NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_FakeHackerTruckPed) AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_FakeHackerTruckPed))) NET_NL()
				
				IF NETWORK_HAS_CONTROL_OF_ENTITY(MPGlobals.RemoteFakeHackerTruckPed[iStaggerPlayer])
							
					IF NOT IS_ENTITY_DEAD(MPGlobals.RemoteFakeHackerTruckPed[iStaggerPlayer])
						DETACH_ENTITY(MPGlobals.RemoteFakeHackerTruckPed[iStaggerPlayer])
						VECTOR vPedPos = GET_ENTITY_COORDS(MPGlobals.RemoteFakeHackerTruckPed[iStaggerPlayer])
						vPedPos.z = ( vPedPos.z - 100 )
						SET_ENTITY_COORDS(MPGlobals.RemoteFakeHackerTruckPed[iStaggerPlayer], vPedPos)
						PRINTLN("CLEANUP_FAKE_HCAKER_TRUCK_PED, vPedPos = ", vPedPos)
					ENDIF
					
					IF NOT IS_ENTITY_A_MISSION_ENTITY(MPGlobals.RemoteFakeHackerTruckPed[iStaggerPlayer])
						SET_ENTITY_AS_NET_MISSION_ENTITY(GlobalplayerBD[iStaggerPlayer].netID_FakeHackerTruckPed, MPGlobals.RemoteFakeHackerTruckPed[iStaggerPlayer], FALSE, TRUE)
						DELETE_PED(MPGlobals.RemoteFakeHackerTruckPed[iStaggerPlayer])
						PRINTLN("CLEANUP_FAKE_HCAKER_TRUCK_PED, DELETE_PED 1 ")
					ELSE
						DELETE_PED(MPGlobals.RemoteFakeHackerTruckPed[iStaggerPlayer])
						PRINTLN("CLEANUP_FAKE_HCAKER_TRUCK_PED, DELETE_PED 2 ")
					ENDIF
				ENDIF
			ELSE
				NET_PRINT("CLEANUP_FAKE_HCAKER_TRUCK_PED - remove ped exists entity not belong to this script ") NET_PRINT_INT(iStaggerPlayer) NET_NL()
				IF NOT IS_ENTITY_A_MISSION_ENTITY(MPGlobals.RemoteFakeHackerTruckPed[iStaggerPlayer])
					SET_ENTITY_AS_NET_MISSION_ENTITY(GlobalplayerBD[iStaggerPlayer].netID_FakeHackerTruckPed, MPGlobals.RemoteFakeHackerTruckPed[iStaggerPlayer], FALSE, TRUE)
					NET_PRINT("CLEANUP_FAKE_HCAKER_TRUCK_PED - remove ped exists entity grab this script ") NET_PRINT_INT(iStaggerPlayer) NET_NL()
				ENDIF
			ENDIF		
		ENDIF
	ELSE	
		IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_FakeHackerTruckPed)	
			PRINTLN("CLEANUP_FAKE_HCAKER_TRUCK_PED, 3 ")
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_FakeHackerTruckPed)
				PRINTLN("CLEANUP_FAKE_HCAKER_TRUCK_PED, 4 ")
				IF NOT (MPGlobals.RemoteFakeHackerTruckPed[iStaggerPlayer] = NET_TO_PED(GlobalplayerBD[iStaggerPlayer].netID_FakeHackerTruckPed))
					MPGlobals.RemoteFakeHackerTruckPed[iStaggerPlayer] = NET_TO_PED(GlobalplayerBD[iStaggerPlayer].netID_FakeHackerTruckPed)
					PRINTLN("CLEANUP_FAKE_HCAKER_TRUCK_PED - updating RemoteFakeHackerTruckPed for player ", iStaggerPlayer, " RemoteFakeHackerTruckPed[1] = ", NATIVE_TO_INT(MPGlobals.RemoteFakeHackerTruckPed[iStaggerPlayer]))
				ENDIF
			ENDIF
		ENDIF
	ENDIF	

ENDPROC

PROC MAINTAIN_FAKE_HACKER_TRUCK_PED()
	
	IF !IS_BIT_SET(iLocalBS, LOCAL_BS_SHOULD_ALLOW_EXPENSIVE_PROCESSING)
		EXIT
	ENDIF
	
	IF g_OwnerOfHackerTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
	AND GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bTurretInUse != 0
	AND IS_PLAYER_USING_VEHICLE_WEAPON(PLAYER_ID())
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost != -1
			PED_INDEX pedToSet = MPGlobals.RemoteFakeHackerTruckPed[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost]
			IF DOES_ENTITY_EXIST(pedToSet)
			AND NOT IS_ENTITY_DEAD(pedToSet)
				
				IF NETWORK_HAS_CONTROL_OF_ENTITY(pedToSet)
					IF IS_ENTITY_ALIVE(GET_HACKER_TRUCK_VEHICLE_INDEX())

						SET_ENTITY_LOCALLY_INVISIBLE(pedToSet)

						INT iBone = GET_ENTITY_BONE_INDEX_BY_NAME(pedToSet, "weapon_1d")
								
						ATTACH_ENTITY_TO_ENTITY(pedToSet, GET_HACKER_TRUCK_VEHICLE_INDEX(), iBone, <<-0.25, -1, 2.5>>, GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].vTurretRayTrace[0], DEFAULT, DEFAULT, DEFAULT, TRUE, DEFAULT, TRUE)
						SET_ENTITY_COMPLETELY_DISABLE_COLLISION(pedToSet, FALSE)
					ENDIF	
				ELSE
					NETWORK_REQUEST_CONTROL_OF_ENTITY(pedToSet)
				ENDIF
			ENDIF
		ENDIF	
	ENDIF	
	
	CLEAR_BIT(iCloseTruck, iRemoteVehicleStagger)
	
	VEHICLE_INDEX avengerVeh 
	
	IF !NETWORK_IS_ACTIVITY_SESSION()
	
		avengerVeh = MPGlobals.RemoteHackertruckV[iRemoteVehicleStagger]
	ELSE
		avengerVeh = GET_HACKER_TRUCK_VEHICLE_INDEX()
	ENDIF
	
	IF DOES_ENTITY_EXIST(avengerVeh)
	AND NOT IS_ENTITY_DEAD(avengerVeh)
	
		VECTOR vPos = GET_ENTITY_COORDS(avengerVeh)
		
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		
			VECTOR vplayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	
			IF VDIST2(vPos, vplayerPos) < 10000.00
				SET_BIT(iCloseTruck, iRemoteVehicleStagger)
				vCloseTruck[iRemoteVehicleStagger] = vPos
				
				PRINTLN("MAINTAIN_FAKE_HACKER_TRUCK_PED, vCloseHcakerTruck( iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])", " vCloseHcakerTruck[iRemoteVehicleStagger] = ", vCloseTruck[iRemoteVehicleStagger])
			ENDIF
		ENDIF
	
	ENDIF
	
	CONST_INT ciNEARBY_PEDS 10
	PED_INDEX pedArray[ciNEARBY_PEDS]
	
	INT iLoop2, iLoopCount
	PED_INDEX pedToCheck

	IF DOES_ENTITY_EXIST(MPGlobals.RemoteFakeHackerTruckPed[iRemoteVehicleStagger])
		IF NOT IS_ENTITY_DEAD(MPGlobals.RemoteFakeHackerTruckPed[iRemoteVehicleStagger])
			IF NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(MPGlobals.RemoteFakeHackerTruckPed[iRemoteVehicleStagger])
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(PED_TO_NET(MPGlobals.RemoteFakeHackerTruckPed[iRemoteVehicleStagger]))
					SET_ENTITY_LOCALLY_INVISIBLE(MPGlobals.RemoteFakeHackerTruckPed[iRemoteVehicleStagger])
					PRINTLN("MAINTAIN_FAKE_HACKER_TRUCK_PED, SET_ENTITY_LOCALLY_INVISIBLE  ")
				ENDIF	
			ELSE
			
			ENDIF
		ENDIF	
	ELSE
		//IF IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
			IF IS_ENTITY_ALIVE(GET_HACKER_TRUCK_VEHICLE_INDEX())
				VECTOR vPedCooordtoCheck = GET_ENTITY_COORDS(GET_HACKER_TRUCK_VEHICLE_INDEX())
				IF GET_CLOSEST_PED(vPedCooordtoCheck, 30.00, TRUE, TRUE, pedToCheck, FALSE, TRUE) 
					IF DOES_ENTITY_EXIST(pedToCheck)
						IF GET_ENTITY_MODEL(pedToCheck) = GET_MOC_PED_MODEL()
							SET_ENTITY_LOCALLY_INVISIBLE(pedToCheck)
							PRINTLN("    ->     [HCAKER_TRUCK_WANTED], MPGlobals.RemoteFakeHackerTruckPed doesn't exist found nearest ped SET_ENTITY_LOCALLY_INVISIBLE  ")
						ENDIF
					ENDIF	
				ENDIF
			ENDIF	
		//ENDIF	
	ENDIF

	IF IS_BIT_SET(iCloseTruck, iRemoteVehicleStagger)
		//PRINTLN("    ->     [HCAKER_TRUCK_WANTED], FIX_FOR_3704938, IS_BIT_SET(iCloseTruck - iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])")
		IF GET_CLOSEST_PED(vCloseTruck[iRemoteVehicleStagger], 15.00, TRUE, TRUE, pedToCheck, FALSE, TRUE) 
			//PRINTLN("    ->     [HCAKER_TRUCK_WANTED], FIX_FOR_3704938, GET_CLOSEST_PED - iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])")
			IF DOES_ENTITY_EXIST(pedToCheck)
				//PRINTLN("    ->     [HCAKER_TRUCK_WANTED], FIX_FOR_3704938, DOES_ENTITY_EXIST - iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])")
				IF GET_ENTITY_MODEL(pedToCheck) = GET_MOC_PED_MODEL()
					SET_ENTITY_LOCALLY_INVISIBLE(pedToCheck)
					//PRINTLN("    ->     [HCAKER_TRUCK_WANTED],  SET_ENTITY_LOCALLY_INVISIBLE(pedToCheck) - iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])")
					iLoopCount = GET_PED_NEARBY_PEDS(pedToCheck, pedArray)
					//PRINTLN("    ->     [HCAKER_TRUCK_WANTED], FIX_FOR_3704938, iLoopCount = ", iLoopCount, "])")
					REPEAT iLoopCount iLoop2
						IF pedArray[iLoop2] != pedToCheck
							IF DOES_ENTITY_EXIST(pedArray[iLoop2])
								IF GET_ENTITY_MODEL(pedArray[iLoop2]) = GET_MOC_PED_MODEL()
									SET_ENTITY_LOCALLY_INVISIBLE(pedArray[iLoop2])
									//PRINTLN("    ->     [HCAKER_TRUCK_WANTED], FIX_FOR_3704938, SET_ENTITY_LOCALLY_INVISIBLE(pedArray[", iLoop2, "]) - iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])")
									IF GET_PED_RELATIONSHIP_GROUP_HASH(pedArray[iLoop2])  = rgFM_AiHatedByCopsAndMercs
										IF NOT IS_ENTITY_DEAD(pedArray[iLoop2])
											IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(pedArray[iLoop2])
												IF NETWORK_HAS_CONTROL_OF_ENTITY(pedArray[iLoop2])
													SET_ENTITY_AS_MISSION_ENTITY(pedArray[iLoop2],FALSE,TRUE)
													PRINTLN("MAINTAIN_FAKE_HACKER_TRUCK_PED, DELETE_PED1(pedArray[", iLoop2, "]) - iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])")
													CLEAR_AREA_OF_PEDS(GET_ENTITY_COORDS(pedArray[iLoop2]), 1.0)
													IF NETWORK_HAS_CONTROL_OF_ENTITY(pedArray[iLoop2])
														DELETE_PED(pedArray[iLoop2])
													ELSE
														NETWORK_REQUEST_CONTROL_OF_ENTITY(pedArray[iLoop2])
													ENDIF
												ELSE
	//													NETWORK_REQUEST_CONTROL_OF_ENTITY(pedArray[iLoop2])
													PRINTLN("MAINTAIN_FAKE_HACKER_TRUCK_PED  NETWORK_HAS_CONTROL_OF_ENTITY, FALSE[", iLoop2, "]) - iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])")
												ENDIF
											ELSE
												PRINTLN("MAINTAIN_FAKE_HACKER_TRUCK_PED  DELETE_PED2(pedArray[", iLoop2, "]) - iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])")
												IF NETWORK_HAS_CONTROL_OF_ENTITY(pedArray[iLoop2])
													CLEAR_AREA_OF_PEDS(GET_ENTITY_COORDS(pedArray[iLoop2]), 1.0)
													DELETE_PED(pedArray[iLoop2])
												ELSE
													NETWORK_REQUEST_CONTROL_OF_ENTITY(pedArray[iLoop2])
												ENDIF	
											ENDIF
										ENDIF	
									ENDIF
								ELSE
									PRINTLN("MAINTAIN_FAKE_HACKER_TRUCK_PED, pedArray GET_ENTITY_MODEL = ", iLoop2, "])")
								ENDIF
							ELSE
								PRINTLN("MAINTAIN_FAKE_HACKER_TRUCK_PED, pedArray DOES_ENTITY_EXIST = ", iLoop2, "])")
							ENDIF
						ELSE
							PRINTLN("MAINTAIN_FAKE_HACKER_TRUCK_PED, pedArray SAME_PED = ", iLoop2, "])")
						ENDIF
					ENDREPEAT
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	
	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost = NATIVE_TO_INT(PLAYER_ID())
	OR IS_PLAYER_USING_VEHICLE_WEAPON(PLAYER_ID())
	
		// Create the ped
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost  != -1
			
			IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost = NATIVE_TO_INT(PLAYER_ID())
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost].netID_FakeHackerTruckPed)
					IF SHOULD_HACKER_TRUCK_PED_EXIST()
						REQUEST_MODEL(GET_MOC_PED_MODEL())
						
						IF HAS_MODEL_LOADED(GET_MOC_PED_MODEL())
							IF NOT bRegisteredPed
								IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_OBJECTS()+ 1, FALSE, TRUE)
									RESERVE_NETWORK_MISSION_PEDS(GET_NUM_CREATED_MISSION_PEDS() + 1) // For fake Avenger wanted ped 
									bRegisteredPed = TRUE
								ENDIF
							ELSE
								IF CAN_REGISTER_MISSION_PEDS(1)

									CREATE_NET_PED(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeHackerTruckPed, PEDTYPE_MISSION, GET_MOC_PED_MODEL(), GET_ENTITY_COORDS(GET_HACKER_TRUCK_VEHICLE_INDEX()), GET_ENTITY_HEADING(GET_HACKER_TRUCK_VEHICLE_INDEX()), FALSE)
									SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeHackerTruckPed, PLAYER_ID(), TRUE)
									//SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeHackerTruckPed, TRUE)
									
									SET_ENTITY_INVINCIBLE(NET_TO_PED(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeHackerTruckPed), TRUE)
									SET_ENTITY_VISIBLE(NET_TO_PED(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeHackerTruckPed), FALSE)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeHackerTruckPed), TRUE)
									SET_MODEL_AS_NO_LONGER_NEEDED(GET_MOC_PED_MODEL())
									
									SET_PED_CONFIG_FLAG(NET_TO_PED(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeHackerTruckPed), PCF_RunFromFiresAndExplosions, FALSE)
									SET_PED_CONFIG_FLAG(NET_TO_PED(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeHackerTruckPed), PCF_DontActivateRagdollFromExplosions, TRUE)
									SET_PED_CONFIG_FLAG(NET_TO_PED(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeHackerTruckPed), PCF_DisableExplosionReactions, TRUE)
									SET_ENTITY_PROOFS(NET_TO_PED(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeHackerTruckPed), TRUE, TRUE, TRUE, TRUE, TRUE)
									
									PED_INDEX pedMoc
									pedMoc = NET_TO_PED(GlobalplayerBD[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost].netID_FakeHackerTruckPed)
						
									IF DOES_ENTITY_EXIST(pedMoc)
									AND NOT IS_ENTITY_DEAD(pedMoc)
									
										SET_ENTITY_VISIBLE(NET_TO_PED(GlobalplayerBD[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost].netID_FakeHackerTruckPed), FALSE)
									
										INT iBone = GET_ENTITY_BONE_INDEX_BY_NAME(pedMoc, "chassis_dummy")
										
										IF NOT IS_ENTITY_ATTACHED(pedMoc)
											ATTACH_ENTITY_TO_ENTITY(pedMoc, GET_HACKER_TRUCK_VEHICLE_INDEX(), iBone, <<0,1,2.5>>, <<0,0,0>>)
											SET_ENTITY_COLLISION(NET_TO_PED(GlobalplayerBD[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost].netID_FakeHackerTruckPed), FALSE)
											SET_ENTITY_COMPLETELY_DISABLE_COLLISION(NET_TO_PED(GlobalplayerBD[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost].netID_FakeHackerTruckPed), FALSE)
											//PRINTLN("    ->     [HCAKER_TRUCK_WANTED] MAINTAIN_FAKE_AVENGER_PED, ATTACH_ENTITY_TO_ENTITY ")
										ENDIF
										
										IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
											SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeHackerTruckPed), rgFM_AiHatedByCopsAndMercs)
										ELSE
											SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeHackerTruckPed), rgFM_AiLikePlyrLikeCops)
										ENDIF
										
									ELSE
										PRINTLN("MAINTAIN_FAKE_HACKER_TRUCK_PED , DOES_ENTITY_EXIST, FALSE ")
									ENDIF
									
									PRINTLN("MAINTAIN_FAKE_HACKER_TRUCK_PED , DONE ")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost = NATIVE_TO_INT(PLAYER_ID())
					AND (NOT IS_ANY_HACEKR_TRUCK_PARTICIPANT_USING_MISSILES() OR 
						 (IS_ENTITY_ALIVE(NET_TO_PED(GlobalplayerBD[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost].netID_FakeHackerTruckPed)) AND NOT IS_ENTITY_ATTACHED(NET_TO_PED(GlobalplayerBD[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost].netID_FakeHackerTruckPed))))
						// Maintain the ped				
						IF TAKE_CONTROL_OF_NET_ID(GlobalplayerBD[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost].netID_FakeHackerTruckPed)
						
							IF SHOULD_HACKER_TRUCK_PED_EXIST()
							OR IS_ANY_HACEKR_TRUCK_PARTICIPANT_USING_MISSILES()
							
								PED_INDEX pedMoc
								pedMoc = NET_TO_PED(GlobalplayerBD[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost].netID_FakeHackerTruckPed)
					
								IF DOES_ENTITY_EXIST(pedMoc)
								AND NOT IS_ENTITY_DEAD(pedMoc)
								
									SET_ENTITY_VISIBLE(NET_TO_PED(GlobalplayerBD[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost].netID_FakeHackerTruckPed), FALSE)
								
									INT iBone = GET_ENTITY_BONE_INDEX_BY_NAME(pedMoc, "chassis_dummy")
									
									IF IS_ENTITY_ALIVE(GET_HACKER_TRUCK_VEHICLE_INDEX())
										IF NOT IS_ENTITY_ATTACHED(pedMoc)
											ATTACH_ENTITY_TO_ENTITY(pedMoc, GET_HACKER_TRUCK_VEHICLE_INDEX(), iBone, <<0,1,2.5>>, <<0,0,0>>)
											SET_ENTITY_COLLISION(NET_TO_PED(GlobalplayerBD[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost].netID_FakeHackerTruckPed), FALSE)
											SET_ENTITY_COMPLETELY_DISABLE_COLLISION(NET_TO_PED(GlobalplayerBD[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost].netID_FakeHackerTruckPed), FALSE)
											//PRINTLN("    ->     [HCAKER_TRUCK_WANTED] MAINTAIN_FAKE_AVENGER_PED, ATTACH_ENTITY_TO_ENTITY ")
										ENDIF
									ENDIF
									
									IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
										SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeHackerTruckPed), rgFM_AiHatedByCopsAndMercs)
									ELSE
										SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeHackerTruckPed), rgFM_AiLikePlyrLikeCops)
									ENDIF
									
								ELSE
									PRINTLN("MAINTAIN_FAKE_HACKER_TRUCK_PED , DOES_ENTITY_EXIST, FALSE ")
								ENDIF
							ELSE
								IF DOES_ENTITY_EXIST(MPGlobals.RemoteFakeHackerTruckPed[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost])
									//PRINTLN("    ->     [HCAKER_TRUCK_WANTED] MAINTAIN_FAKE_AVENGER_PED, DELETE_NET_ID ")
									SET_ENTITY_AS_NET_MISSION_ENTITY(GlobalplayerBD[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost].netID_FakeHackerTruckPed, MPGlobals.RemoteFakeHackerTruckPed[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost], FALSE, TRUE)
									DELETE_PED(MPGlobals.RemoteFakeHackerTruckPed[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost])
									DELETE_NET_ID(GlobalplayerBD[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost].netID_FakeHackerTruckPed)
									IF bRegisteredPed
										IF GET_NUM_CREATED_MISSION_PEDS() > 0
											RESERVE_NETWORK_MISSION_PEDS(GET_NUM_CREATED_MISSION_PEDS() - 1) // For fake Avenger wanted ped 
										ENDIF
										bRegisteredPed = FALSE
									ENDIF
								ENDIF	
							ENDIF
						ELSE
							PRINTLN("MAINTAIN_FAKE_HACKER_TRUCK_PED , MAINTAIN_CONTROL_OF_NETWORK_ID ")
						ENDIF
					ENDIF
				ENDIF
			ENDIF	
		ENDIF	
	// If host changes, delete the ped
	ELSE
		IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NETWORK_PLAYER_ID_TO_INT()].netID_FakeHackerTruckPed)
			IF TAKE_CONTROL_OF_NET_ID(GlobalplayerBD[NETWORK_PLAYER_ID_TO_INT()].netID_FakeHackerTruckPed)
				IF DOES_ENTITY_EXIST(MPGlobals.RemoteFakeHackerTruckPed[NETWORK_PLAYER_ID_TO_INT()])
					SET_ENTITY_AS_NET_MISSION_ENTITY(GlobalplayerBD[NETWORK_PLAYER_ID_TO_INT()].netID_FakeHackerTruckPed, MPGlobals.RemoteFakeHackerTruckPed[NETWORK_PLAYER_ID_TO_INT()], FALSE, TRUE)
					PRINTLN("MAINTAIN_FAKE_HACKER_TRUCK_PED , HOST CHANGE, DELETE_NET_ID  ")
					DELETE_PED(MPGlobals.RemoteFakeHackerTruckPed[NETWORK_PLAYER_ID_TO_INT()])
					DELETE_NET_ID(GlobalplayerBD[NETWORK_PLAYER_ID_TO_INT()].netID_FakeHackerTruckPed)
					IF bRegisteredPed
						IF GET_NUM_CREATED_MISSION_PEDS() > 0
							RESERVE_NETWORK_MISSION_PEDS(GET_NUM_CREATED_MISSION_PEDS() - 1) 
						ENDIF
						bRegisteredPed = FALSE
					ENDIF
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
ENDPROC

#IF FEATURE_HEIST_ISLAND
FUNC BOOL SHOULD_SET_THIS_HEIST_ISLAND_VEHICLE_RADIO(MODEL_NAMES vehicleModel)
	SWITCH vehicleModel
		CASE AVISA
		CASE KOSATKA
		CASE DINGHY4
		CASE SEASPARROW2	
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_HEIST_ISLAND_VEHICLE_RADIO()
	IF !g_sMPTunables.bFORCE_ISLAND_HEIST_VEH_RADIO
		EXIT
	ENDIF
	
	IF !g_sMPTunables.IH_RADIO_ENABLE_KULT_FM
		EXIT
	ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		EXIT
	ENDIF	
	
	IF GET_CURRENT_GAMEMODE() != GAMEMODE_FM
		EXIT
	ENDIF	
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_SET_ISLAND_HEIST_VEH_RADIO)
			VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF IS_ENTITY_ALIVE(tempVeh)
			AND IS_VEHICLE_DRIVEABLE(tempVeh)
				IF GET_PED_IN_VEHICLE_SEAT(tempVeh, VS_DRIVER) = PLAYER_PED_ID()
					IF SHOULD_SET_THIS_HEIST_ISLAND_VEHICLE_RADIO(GET_ENTITY_MODEL(tempVeh))
						IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
							START_VEHICLE_AND_TURN_ON_RADIO(tempVeh, "RADIO_34_DLC_HEI4_KULT")
							SET_BIT(iLocalBS, LOCAL_BS_SET_ISLAND_HEIST_VEH_RADIO)
							PRINTLN("MAINTAIN_HEIST_ISLAND_VEHICLE_RADIO set radio")
						ENDIF
					ENDIF	
				ENDIF	
			ENDIF	
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBS, LOCAL_BS_SET_ISLAND_HEIST_VEH_RADIO)
			CLEAR_BIT(iLocalBS, LOCAL_BS_SET_ISLAND_HEIST_VEH_RADIO)
			PRINTLN("MAINTAIN_HEIST_ISLAND_VEHICLE_RADIO set radio false")
		ENDIF	
	ENDIF
ENDPROC

FUNC VEHICLE_INDEX GET_SUBMARINE_VEHICLE_INDEX()
	VEHICLE_INDEX submarineVeh
	IF !NETWORK_IS_ACTIVITY_SESSION()

		IF globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner != INVALID_PLAYER_INDEX()
		AND IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
			IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner)].netID_SubmarineV) 
				submarineVeh = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner)].netID_SubmarineV)
			ENDIF
		ELSE

			VEHICLE_INDEX vehIn
			PLAYER_INDEX playerOwner
			
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				
					vehIn = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				
					IF IS_VEHICLE_A_PERSONAL_SUBMARINE(vehIn) 
					
						playerOwner = GET_OWNER_OF_SMPL_INTERIOR_SUBMARINE(vehIn)
						
						IF IS_NET_PLAYER_OK(playerOwner)
						
							IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(playerOwner)].netID_SubmarineV) 
								submarineVeh = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(playerOwner)].netID_SubmarineV)
							ENDIF
						ENDIF
					ENDIF
				
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN submarineVeh
ENDFUNC

FUNC BOOL IS_ANY_SUBMARINE_PARTICIPANT_USING_PERISCOPE_OR_GUIDED_MISSILE()
	
	IF PLAYER_ID() = INVALID_PLAYER_INDEX()
		RETURN FALSE
	ENDIF	
	
	IF NOT IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
		RETURN FALSE
	ENDIF	
	
	IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = INVALID_PLAYER_INDEX()
		RETURN FALSE
	ENDIF
	
	INT i
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))

			PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			
			IF IS_NET_PLAYER_OK(playerID)
				IF IS_PLAYER_IN_SUBMARINE(playerID)
					
					IF GlobalPlayerBD[NATIVE_TO_INT(playerID)].SimpleInteriorBD.propertyOwner = GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner
						IF IS_PLAYER_USING_SUBMARINE_PERISCOPE(playerID)
						OR IS_PLAYER_USING_DRONE(playerID)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_SUBMARINE_PED_EXIST()
	IF !NETWORK_IS_ACTIVITY_SESSION()
		IF DOES_ENTITY_EXIST(GET_SUBMARINE_VEHICLE_INDEX())
		AND NOT IS_ENTITY_DEAD(GET_SUBMARINE_VEHICLE_INDEX())
		AND IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
		AND IS_NET_PLAYER_OK(PLAYER_ID()) 
		AND (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0 OR IS_ANY_SUBMARINE_PARTICIPANT_USING_PERISCOPE_OR_GUIDED_MISSILE())
			RETURN TRUE
		ENDIF
	ELSE

	ENDIF

	RETURN FALSE
ENDFUNC

PROC CLEANUP_FAKE_SUBMARINE_PED(INT iStaggerPlayer)

	IF NOT IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, iStaggerPlayer), FALSE)	
	OR NOT NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_FakeSubmarinePed)
	OR (NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_FakeSubmarinePed) AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_FakeSubmarinePed))

		IF DOES_ENTITY_EXIST(MPGlobals.RemoteFakeSubmarinePed[iStaggerPlayer])
			IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(MPGlobals.RemoteFakeSubmarinePed[iStaggerPlayer], FALSE)
				NET_PRINT("CLEANUP_FAKE_SUBMARINE_PED - remove ped exists for old player ") NET_PRINT_INT(iStaggerPlayer) NET_NL()
				
				NET_PRINT("CLEANUP_FAKE_SUBMARINE_PED - IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, iStaggerPlayer), FALSE) = ")
				NET_PRINT_BOOL(IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, iStaggerPlayer), FALSE)) NET_NL()
				NET_PRINT("CLEANUP_FAKE_SUBMARINE_PED - NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_FakeSubmarinePed) = ")
				NET_PRINT_BOOL(NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_FakeSubmarinePed)) NET_NL()
				NET_PRINT("CLEANUP_FAKE_SUBMARINE_PED - (NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_FakeSubmarinePed) AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_FakeSubmarinePed)) = ")
				NET_PRINT_BOOL((NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_FakeSubmarinePed) AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_FakeSubmarinePed))) NET_NL()
				
				IF NETWORK_HAS_CONTROL_OF_ENTITY(MPGlobals.RemoteFakeSubmarinePed[iStaggerPlayer])
							
					IF NOT IS_ENTITY_DEAD(MPGlobals.RemoteFakeSubmarinePed[iStaggerPlayer])
						DETACH_ENTITY(MPGlobals.RemoteFakeSubmarinePed[iStaggerPlayer])
						VECTOR vPedPos = GET_ENTITY_COORDS(MPGlobals.RemoteFakeSubmarinePed[iStaggerPlayer])
						vPedPos.z = ( vPedPos.z - 100 )
						SET_ENTITY_COORDS(MPGlobals.RemoteFakeSubmarinePed[iStaggerPlayer], vPedPos)
						PRINTLN("CLEANUP_FAKE_SUBMARINE_PED, vPedPos = ", vPedPos)
					ENDIF
					
					IF NOT IS_ENTITY_A_MISSION_ENTITY(MPGlobals.RemoteFakeSubmarinePed[iStaggerPlayer])
						SET_ENTITY_AS_NET_MISSION_ENTITY(GlobalplayerBD[iStaggerPlayer].netID_FakeSubmarinePed, MPGlobals.RemoteFakeSubmarinePed[iStaggerPlayer], FALSE, TRUE)
						DELETE_PED(MPGlobals.RemoteFakeSubmarinePed[iStaggerPlayer])
						PRINTLN("CLEANUP_FAKE_SUBMARINE_PED, DELETE_PED 1 ")
					ELSE
						DELETE_PED(MPGlobals.RemoteFakeSubmarinePed[iStaggerPlayer])
						PRINTLN("CLEANUP_FAKE_SUBMARINE_PED, DELETE_PED 2 ")
					ENDIF
				ENDIF
			ELSE
				NET_PRINT("CLEANUP_FAKE_SUBMARINE_PED - remove ped exists entity not belong to this script ") NET_PRINT_INT(iStaggerPlayer) NET_NL()
				IF NOT IS_ENTITY_A_MISSION_ENTITY(MPGlobals.RemoteFakeSubmarinePed[iStaggerPlayer])
					SET_ENTITY_AS_NET_MISSION_ENTITY(GlobalplayerBD[iStaggerPlayer].netID_FakeSubmarinePed, MPGlobals.RemoteFakeSubmarinePed[iStaggerPlayer], FALSE, TRUE)
					NET_PRINT("CLEANUP_FAKE_SUBMARINE_PED - remove ped exists entity grab this script ") NET_PRINT_INT(iStaggerPlayer) NET_NL()
				ENDIF
			ENDIF		
		ENDIF
	ELSE	
		IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_FakeSubmarinePed)	
			PRINTLN("CLEANUP_FAKE_SUBMARINE_PED, 3 ")
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_FakeSubmarinePed)
				PRINTLN("CLEANUP_FAKE_SUBMARINE_PED, 4 ")
				IF NOT (MPGlobals.RemoteFakeSubmarinePed[iStaggerPlayer] = NET_TO_PED(GlobalplayerBD[iStaggerPlayer].netID_FakeSubmarinePed))
					MPGlobals.RemoteFakeSubmarinePed[iStaggerPlayer] = NET_TO_PED(GlobalplayerBD[iStaggerPlayer].netID_FakeSubmarinePed)
					PRINTLN("CLEANUP_FAKE_SUBMARINE_PED - updating RemoteFakeSubmarinePed for player ", iStaggerPlayer, " RemoteFakeSubmarinePed[1] = ", NATIVE_TO_INT(MPGlobals.RemoteFakeSubmarinePed[iStaggerPlayer]))
				ENDIF
			ENDIF
		ENDIF
	ENDIF	

ENDPROC

PROC SET_ALL_FAKE_PEDS_LOCALLY_INVISIBLE()
	IF IS_BIT_SET(iLocalBS, LOCAL_BS_SHOULD_ALLOW_EXPENSIVE_PROCESSING)
		EXIT
	ENDIF
	
	INT index
	FOR index = 0 TO (NUM_NETWORK_PLAYERS-1)
		IF IS_ENTITY_ALIVE(MPGlobals.RemoteFakeSubmarinePed[index])
			IF NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(MPGlobals.RemoteFakeSubmarinePed[index])
				IF NETWORK_HAS_CONTROL_OF_ENTITY(MPGlobals.RemoteFakeSubmarinePed[index])
					SET_ENTITY_LOCALLY_INVISIBLE(MPGlobals.RemoteFakeSubmarinePed[index])
				ENDIF
			ENDIF	
		ENDIF
		IF IS_ENTITY_ALIVE(MPGlobals.RemoteFakeAvengerPed[index])
			IF NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(MPGlobals.RemoteFakeAvengerPed[index])
				IF NETWORK_HAS_CONTROL_OF_ENTITY(MPGlobals.RemoteFakeAvengerPed[index])
					SET_ENTITY_LOCALLY_INVISIBLE(MPGlobals.RemoteFakeAvengerPed[index])
				ENDIF
			ENDIF	
		ENDIF
		IF IS_ENTITY_ALIVE(MPGlobals.RemoteFakeHackerTruckPed[index])
			IF NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(MPGlobals.RemoteFakeHackerTruckPed[index])
				IF NETWORK_HAS_CONTROL_OF_ENTITY(MPGlobals.RemoteFakeHackerTruckPed[index])
					SET_ENTITY_LOCALLY_INVISIBLE(MPGlobals.RemoteFakeHackerTruckPed[index])
				ENDIF
			ENDIF	
		ENDIF
		IF IS_ENTITY_ALIVE(MPGlobals.RemoteFakeMocPed[index])
			IF NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(MPGlobals.RemoteFakeMocPed[index])
				IF NETWORK_HAS_CONTROL_OF_ENTITY(MPGlobals.RemoteFakeMocPed[index])
					SET_ENTITY_LOCALLY_INVISIBLE(MPGlobals.RemoteFakeMocPed[index])
				ENDIF
			ENDIF	
		ENDIF
	ENDFOR
ENDPROC

PROC MAINTAIN_FAKE_SUBMARINE_PED()
	
	IF !IS_BIT_SET(iLocalBS, LOCAL_BS_SHOULD_ALLOW_EXPENSIVE_PROCESSING)
		EXIT
	ENDIF
	
	IF g_OwnerOfSubmarinePropertyIAmIn != INVALID_PLAYER_INDEX()
	AND IS_PLAYER_USING_SUBMARINE_PERISCOPE(PLAYER_ID())
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost != -1
			PED_INDEX pedToSet = MPGlobals.RemoteFakeSubmarinePed[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost]
			IF DOES_ENTITY_EXIST(pedToSet)
			AND NOT IS_ENTITY_DEAD(pedToSet)
				
				IF NETWORK_HAS_CONTROL_OF_ENTITY(pedToSet)
					IF IS_ENTITY_ALIVE(GET_SUBMARINE_VEHICLE_INDEX())

						SET_ENTITY_LOCALLY_INVISIBLE(pedToSet)

						INT iBone = GET_ENTITY_BONE_INDEX_BY_NAME(pedToSet, "chassis_dummy")
								
						ATTACH_ENTITY_TO_ENTITY(pedToSet, GET_SUBMARINE_VEHICLE_INDEX(), iBone, <<0, 28, 9>>, <<0, 0, 0>>, DEFAULT, DEFAULT, DEFAULT, TRUE, DEFAULT, TRUE)
						SET_ENTITY_COMPLETELY_DISABLE_COLLISION(pedToSet, FALSE)
					ENDIF	
				ELSE
					NETWORK_REQUEST_CONTROL_OF_ENTITY(pedToSet)
				ENDIF
			ENDIF
		ENDIF	
	ENDIF	
	
	CLEAR_BIT(iCloseTruck, iRemoteVehicleStagger)
	
	VEHICLE_INDEX submarineVeh 
	
	IF !NETWORK_IS_ACTIVITY_SESSION()
		submarineVeh = MPGlobals.RemoteSubmarineV[iRemoteVehicleStagger]
	ELSE
		submarineVeh = GET_SUBMARINE_VEHICLE_INDEX()
	ENDIF
	
	IF DOES_ENTITY_EXIST(submarineVeh)
	AND NOT IS_ENTITY_DEAD(submarineVeh)
	
		VECTOR vPos = GET_ENTITY_COORDS(submarineVeh)
		
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		
			VECTOR vplayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	
			IF VDIST2(vPos, vplayerPos) < 10000.00
				SET_BIT(iCloseTruck, iRemoteVehicleStagger)
				vCloseTruck[iRemoteVehicleStagger] = vPos
				
				PRINTLN("MAINTAIN_FAKE_SUBMARINE_PED, vCloseHcakerTruck( iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])", " vCloseHcakerTruck[iRemoteVehicleStagger] = ", vCloseTruck[iRemoteVehicleStagger])
			ENDIF
		ENDIF
	
	ENDIF
	
	CONST_INT ciNEARBY_PEDS 10
	PED_INDEX pedArray[ciNEARBY_PEDS]
	
	INT iLoop2, iLoopCount
	PED_INDEX pedToCheck
	
	IF DOES_ENTITY_EXIST(MPGlobals.RemoteFakeSubmarinePed[iRemoteVehicleStagger])
		IF NOT IS_ENTITY_DEAD(MPGlobals.RemoteFakeSubmarinePed[iRemoteVehicleStagger])
			IF NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(MPGlobals.RemoteFakeSubmarinePed[iRemoteVehicleStagger])
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(PED_TO_NET(MPGlobals.RemoteFakeSubmarinePed[iRemoteVehicleStagger]))
					SET_ENTITY_LOCALLY_INVISIBLE(MPGlobals.RemoteFakeSubmarinePed[iRemoteVehicleStagger])
					PRINTLN("MAINTAIN_FAKE_SUBMARINE_PED, SET_ENTITY_LOCALLY_INVISIBLE  ")
				ENDIF	
			ELSE
			
			ENDIF
		ENDIF	
	ELSE
		//IF IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
			IF IS_ENTITY_ALIVE(GET_SUBMARINE_VEHICLE_INDEX())
				VECTOR vPedCooordtoCheck = GET_ENTITY_COORDS(GET_SUBMARINE_VEHICLE_INDEX())
				IF GET_CLOSEST_PED(vPedCooordtoCheck, 30.00, TRUE, TRUE, pedToCheck, FALSE, TRUE) 
					IF DOES_ENTITY_EXIST(pedToCheck)
						IF GET_ENTITY_MODEL(pedToCheck) = GET_MOC_PED_MODEL()
							SET_ENTITY_LOCALLY_INVISIBLE(pedToCheck)
							PRINTLN("    ->     [SUBMARINE_WANTED], MPGlobals.RemoteFakeSubmarinePed doesn't exist found nearest ped SET_ENTITY_LOCALLY_INVISIBLE  ")
						ENDIF
					ENDIF	
				ENDIF
			ENDIF	
		//ENDIF	
	ENDIF

	IF IS_BIT_SET(iCloseTruck, iRemoteVehicleStagger)
		//PRINTLN("    ->     [SUBMARINE_WANTED], FIX_FOR_3704938, IS_BIT_SET(iCloseTruck - iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])")
		IF GET_CLOSEST_PED(vCloseTruck[iRemoteVehicleStagger], 15.00, TRUE, TRUE, pedToCheck, FALSE, TRUE) 
			//PRINTLN("    ->     [SUBMARINE_WANTED], FIX_FOR_3704938, GET_CLOSEST_PED - iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])")
			IF DOES_ENTITY_EXIST(pedToCheck)
				//PRINTLN("    ->     [SUBMARINE_WANTED], FIX_FOR_3704938, DOES_ENTITY_EXIST - iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])")
				IF GET_ENTITY_MODEL(pedToCheck) = GET_MOC_PED_MODEL()
					SET_ENTITY_LOCALLY_INVISIBLE(pedToCheck)
					//PRINTLN("    ->     [SUBMARINE_WANTED],  SET_ENTITY_LOCALLY_INVISIBLE(pedToCheck) - iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])")
					iLoopCount = GET_PED_NEARBY_PEDS(pedToCheck, pedArray)
					//PRINTLN("    ->     [SUBMARINE_WANTED], FIX_FOR_3704938, iLoopCount = ", iLoopCount, "])")
					REPEAT iLoopCount iLoop2
						IF pedArray[iLoop2] != pedToCheck
							IF DOES_ENTITY_EXIST(pedArray[iLoop2])
								IF GET_ENTITY_MODEL(pedArray[iLoop2]) = GET_MOC_PED_MODEL()
									SET_ENTITY_LOCALLY_INVISIBLE(pedArray[iLoop2])
									//PRINTLN("    ->     [SUBMARINE_WANTED], FIX_FOR_3704938, SET_ENTITY_LOCALLY_INVISIBLE(pedArray[", iLoop2, "]) - iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])")
									IF GET_PED_RELATIONSHIP_GROUP_HASH(pedArray[iLoop2])  = rgFM_AiHatedByCopsAndMercs
										IF NOT IS_ENTITY_DEAD(pedArray[iLoop2])
											IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(pedArray[iLoop2])
												IF NETWORK_HAS_CONTROL_OF_ENTITY(pedArray[iLoop2])
													SET_ENTITY_AS_MISSION_ENTITY(pedArray[iLoop2],FALSE,TRUE)
													PRINTLN("MAINTAIN_FAKE_SUBMARINE_PED, DELETE_PED1(pedArray[", iLoop2, "]) - iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])")
													CLEAR_AREA_OF_PEDS(GET_ENTITY_COORDS(pedArray[iLoop2]), 1.0)
													IF NETWORK_HAS_CONTROL_OF_ENTITY(pedArray[iLoop2])
														DELETE_PED(pedArray[iLoop2])
													ELSE
														NETWORK_REQUEST_CONTROL_OF_ENTITY(pedArray[iLoop2])
													ENDIF
												ELSE
	//													NETWORK_REQUEST_CONTROL_OF_ENTITY(pedArray[iLoop2])
													PRINTLN("MAINTAIN_FAKE_SUBMARINE_PED  NETWORK_HAS_CONTROL_OF_ENTITY, FALSE[", iLoop2, "]) - iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])")
												ENDIF
											ELSE
												PRINTLN("MAINTAIN_FAKE_SUBMARINE_PED  DELETE_PED2(pedArray[", iLoop2, "]) - iRemoteVehicleStagger = ", iRemoteVehicleStagger, "])")
												IF NETWORK_HAS_CONTROL_OF_ENTITY(pedArray[iLoop2])
													CLEAR_AREA_OF_PEDS(GET_ENTITY_COORDS(pedArray[iLoop2]), 1.0)
													DELETE_PED(pedArray[iLoop2])
												ELSE
													NETWORK_REQUEST_CONTROL_OF_ENTITY(pedArray[iLoop2])
												ENDIF	
											ENDIF
										ENDIF	
									ENDIF
								ELSE
									PRINTLN("MAINTAIN_FAKE_SUBMARINE_PED, pedArray GET_ENTITY_MODEL = ", iLoop2, "])")
								ENDIF
							ELSE
								PRINTLN("MAINTAIN_FAKE_SUBMARINE_PED, pedArray DOES_ENTITY_EXIST = ", iLoop2, "])")
							ENDIF
						ELSE
							PRINTLN("MAINTAIN_FAKE_SUBMARINE_PED, pedArray SAME_PED = ", iLoop2, "])")
						ENDIF
					ENDREPEAT
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	
	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost = NATIVE_TO_INT(PLAYER_ID())
	OR IS_PLAYER_USING_SUBMARINE_PERISCOPE(PLAYER_ID())
	
		// Create the ped
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost  != -1
			
			IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost = NATIVE_TO_INT(PLAYER_ID())
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost].netID_FakeSubmarinePed)
					IF SHOULD_SUBMARINE_PED_EXIST()
						REQUEST_MODEL(GET_MOC_PED_MODEL())
						
						IF HAS_MODEL_LOADED(GET_MOC_PED_MODEL())
							IF NOT bRegisteredPed
								IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_OBJECTS()+ 1, FALSE, TRUE)
									RESERVE_NETWORK_MISSION_PEDS(GET_NUM_CREATED_MISSION_PEDS() + 1) // For fake Avenger wanted ped 
									bRegisteredPed = TRUE
								ENDIF
							ELSE
								IF CAN_REGISTER_MISSION_PEDS(1)

									CREATE_NET_PED(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeSubmarinePed, PEDTYPE_MISSION, GET_MOC_PED_MODEL(), GET_ENTITY_COORDS(GET_SUBMARINE_VEHICLE_INDEX()), GET_ENTITY_HEADING(GET_SUBMARINE_VEHICLE_INDEX()), FALSE)
									SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeSubmarinePed, PLAYER_ID(), TRUE)
									//SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeSubmarinePed, TRUE)
									
									SET_ENTITY_INVINCIBLE(NET_TO_PED(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeSubmarinePed), TRUE)
									SET_ENTITY_VISIBLE(NET_TO_PED(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeSubmarinePed), FALSE)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeSubmarinePed), TRUE)
									SET_MODEL_AS_NO_LONGER_NEEDED(GET_MOC_PED_MODEL())
									
									SET_PED_CONFIG_FLAG(NET_TO_PED(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeSubmarinePed), PCF_RunFromFiresAndExplosions, FALSE)
									SET_PED_CONFIG_FLAG(NET_TO_PED(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeSubmarinePed), PCF_DontActivateRagdollFromExplosions, TRUE)
									SET_PED_CONFIG_FLAG(NET_TO_PED(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeSubmarinePed), PCF_DisableExplosionReactions, TRUE)
									SET_ENTITY_PROOFS(NET_TO_PED(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeSubmarinePed), TRUE, TRUE, TRUE, TRUE, TRUE)
									
									PED_INDEX pedMoc
									pedMoc = NET_TO_PED(GlobalplayerBD[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost].netID_FakeSubmarinePed)
						
									IF DOES_ENTITY_EXIST(pedMoc)
									AND NOT IS_ENTITY_DEAD(pedMoc)
									
										SET_ENTITY_VISIBLE(NET_TO_PED(GlobalplayerBD[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost].netID_FakeSubmarinePed), FALSE)
									
										INT iBone = GET_ENTITY_BONE_INDEX_BY_NAME(pedMoc, "chassis_dummy")
										
										IF NOT IS_ENTITY_ATTACHED(pedMoc)
											ATTACH_ENTITY_TO_ENTITY(pedMoc, GET_SUBMARINE_VEHICLE_INDEX(), iBone, <<0,28,9>>, <<0,0,0>>)
											SET_ENTITY_COLLISION(NET_TO_PED(GlobalplayerBD[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost].netID_FakeSubmarinePed), FALSE)
											SET_ENTITY_COMPLETELY_DISABLE_COLLISION(NET_TO_PED(GlobalplayerBD[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost].netID_FakeSubmarinePed), FALSE)
											PRINTLN("    ->     [SUBMARINE_WANTED] MAINTAIN_FAKE_AVENGER_PED, ATTACH_ENTITY_TO_ENTITY ")
										ENDIF
										
										IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
											SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeSubmarinePed), rgFM_AiHatedByCopsAndMercs)
										ELSE
											SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeSubmarinePed), rgFM_AiLikePlyrLikeCops)
										ENDIF
										
									ELSE
										PRINTLN("MAINTAIN_FAKE_SUBMARINE_PED , DOES_ENTITY_EXIST, FALSE ")
									ENDIF
									
									PRINTLN("MAINTAIN_FAKE_SUBMARINE_PED , DONE ")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost = NATIVE_TO_INT(PLAYER_ID())
					AND (IS_ANY_SUBMARINE_PARTICIPANT_USING_PERISCOPE_OR_GUIDED_MISSILE()
						OR (IS_ENTITY_ALIVE(NET_TO_PED(GlobalplayerBD[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost].netID_FakeSubmarinePed)) AND NOT IS_ENTITY_ATTACHED(NET_TO_PED(GlobalplayerBD[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost].netID_FakeSubmarinePed))))
						// Maintain the ped				
						IF TAKE_CONTROL_OF_NET_ID(GlobalplayerBD[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost].netID_FakeSubmarinePed)
						
							IF SHOULD_SUBMARINE_PED_EXIST()
							OR IS_ANY_SUBMARINE_PARTICIPANT_USING_PERISCOPE_OR_GUIDED_MISSILE()
							
								PED_INDEX pedMoc
								pedMoc = NET_TO_PED(GlobalplayerBD[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost].netID_FakeSubmarinePed)
					
								IF DOES_ENTITY_EXIST(pedMoc)
								AND NOT IS_ENTITY_DEAD(pedMoc)
								
									SET_ENTITY_VISIBLE(NET_TO_PED(GlobalplayerBD[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost].netID_FakeSubmarinePed), FALSE)
								
									INT iBone = GET_ENTITY_BONE_INDEX_BY_NAME(pedMoc, "chassis_dummy")
									
									IF IS_ENTITY_ALIVE(GET_SUBMARINE_VEHICLE_INDEX())
										IF NOT IS_ENTITY_ATTACHED(pedMoc)
											ATTACH_ENTITY_TO_ENTITY(pedMoc, GET_SUBMARINE_VEHICLE_INDEX(), iBone, <<0,28,9>>, <<0,0,0>>)
											SET_ENTITY_COLLISION(NET_TO_PED(GlobalplayerBD[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost].netID_FakeSubmarinePed), FALSE)
											SET_ENTITY_COMPLETELY_DISABLE_COLLISION(NET_TO_PED(GlobalplayerBD[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost].netID_FakeSubmarinePed), FALSE)
											PRINTLN("    ->     [SUBMARINE_WANTED] MAINTAIN_FAKE_AVENGER_PED, ATTACH_ENTITY_TO_ENTITY ")
										ENDIF
									ENDIF
									
									IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
										SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeSubmarinePed), rgFM_AiHatedByCopsAndMercs)
									ELSE
										SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_FakeSubmarinePed), rgFM_AiLikePlyrLikeCops)
									ENDIF
									
								ELSE
									PRINTLN("MAINTAIN_FAKE_SUBMARINE_PED , DOES_ENTITY_EXIST, FALSE ")
								ENDIF
							ELSE
								IF DOES_ENTITY_EXIST(MPGlobals.RemoteFakeSubmarinePed[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost])
									//PRINTLN("    ->     [SUBMARINE_WANTED] MAINTAIN_FAKE_AVENGER_PED, DELETE_NET_ID ")
									SET_ENTITY_AS_NET_MISSION_ENTITY(GlobalplayerBD[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost].netID_FakeSubmarinePed, MPGlobals.RemoteFakeSubmarinePed[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost], FALSE, TRUE)
									DELETE_PED(MPGlobals.RemoteFakeSubmarinePed[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost])
									DELETE_NET_ID(GlobalplayerBD[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost].netID_FakeSubmarinePed)
									IF bRegisteredPed
										IF GET_NUM_CREATED_MISSION_PEDS() > 0
											RESERVE_NETWORK_MISSION_PEDS(GET_NUM_CREATED_MISSION_PEDS() - 1) // For fake Avenger wanted ped 
										ENDIF
										bRegisteredPed = FALSE
									ENDIF
								ENDIF	
							ENDIF
						ELSE
							PRINTLN("MAINTAIN_FAKE_SUBMARINE_PED , MAINTAIN_CONTROL_OF_NETWORK_ID ")
						ENDIF
					ENDIF
				ENDIF
			ENDIF	
		ENDIF	
	ELSE
		IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NETWORK_PLAYER_ID_TO_INT()].netID_FakeSubmarinePed)
			IF TAKE_CONTROL_OF_NET_ID(GlobalplayerBD[NETWORK_PLAYER_ID_TO_INT()].netID_FakeSubmarinePed)
				IF DOES_ENTITY_EXIST(MPGlobals.RemoteFakeSubmarinePed[NETWORK_PLAYER_ID_TO_INT()])
					SET_ENTITY_AS_NET_MISSION_ENTITY(GlobalplayerBD[NETWORK_PLAYER_ID_TO_INT()].netID_FakeSubmarinePed, MPGlobals.RemoteFakeSubmarinePed[NETWORK_PLAYER_ID_TO_INT()], FALSE, TRUE)
					PRINTLN("MAINTAIN_FAKE_SUBMARINE_PED , HOST CHANGE, DELETE_NET_ID  ")
					DELETE_PED(MPGlobals.RemoteFakeSubmarinePed[NETWORK_PLAYER_ID_TO_INT()])
					DELETE_NET_ID(GlobalplayerBD[NETWORK_PLAYER_ID_TO_INT()].netID_FakeSubmarinePed)
					IF bRegisteredPed
						IF GET_NUM_CREATED_MISSION_PEDS() > 0
							RESERVE_NETWORK_MISSION_PEDS(GET_NUM_CREATED_MISSION_PEDS() - 1) 
						ENDIF
						bRegisteredPed = FALSE
					ENDIF
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
ENDPROC
#ENDIF

FUNC BOOL IS_ANY_PLAYER_EXITING_HOLD_OF_AVENGER(PLAYER_INDEX pAvengerOwner, VEHICLE_INDEX avengerVeh, PLAYER_INDEX pedToCheck)

	IF IS_NET_PLAYER_OK(pedToCheck)
		IF pedToCheck != PLAYER_ID()
		AND !IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID()) 
			IF IS_PLAYER_IN_ARMORY_AIRCRAFT(pedToCheck)
				IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(pedToCheck)].propertyDetails.iArmourAircraftModBS, PROPERTY_BROADCAST_ARMOUR_AIRCRAFT_CONCEAL_PILOT_SEAT_TRANS)
				OR GET_SCRIPT_TASK_STATUS(GET_PLAYER_PED(pedToCheck), SCRIPT_TASK_ENTER_VEHICLE) = WAITING_TO_START_TASK
				OR GET_SCRIPT_TASK_STATUS(GET_PLAYER_PED(pedToCheck), SCRIPT_TASK_ENTER_VEHICLE) = PERFORMING_TASK
					IF GlobalPlayerBD[NATIVE_TO_INT(pedToCheck)].SimpleInteriorBD.propertyOwner != INVALID_PLAYER_INDEX()
						IF GlobalPlayerBD[NATIVE_TO_INT(pedToCheck)].SimpleInteriorBD.propertyOwner = pAvengerOwner
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF	
			ELSE
				IF DOES_ENTITY_EXIST(avengerVeh)
				AND NOT IS_ENTITY_DEAD(avengerVeh)
					IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(pedToCheck), avengerVeh)
						IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(pedToCheck)].propertyDetails.iArmourAircraftModBS, PROPERTY_BROADCAST_ARMOUR_AIRCRAFT_COCKPIT_SEAT_SHUFFLE)
						OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(pedToCheck)].propertyDetails.iArmourAircraftModBS, PROPERTY_BROADCAST_ARMOUR_AIRCRAFT_CONCEAL_PILOT_SEAT_TRANS)
						OR GET_SCRIPT_TASK_STATUS(GET_PLAYER_PED(pedToCheck), SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT) = WAITING_TO_START_TASK
						OR GET_SCRIPT_TASK_STATUS(GET_PLAYER_PED(pedToCheck), SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT) = PERFORMING_TASK
						OR GET_PED_RESET_FLAG(GET_PLAYER_PED(pedToCheck), PRF_IsSeatShuffling)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF	
			ENDIF
		ENDIF	
	ENDIF	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANY_PLAYER_EXITING_HOLD_OF_CREATOR_AVENGER(PLAYER_INDEX pAvengerOwner, VEHICLE_INDEX avengerVeh, PLAYER_INDEX pedToCheck)

	IF IS_NET_PLAYER_OK(pedToCheck)
		IF pedToCheck != PLAYER_ID()
		AND !IS_PLAYER_IN_CREATOR_AIRCRAFT(PLAYER_ID()) 
			IF IS_PLAYER_IN_CREATOR_AIRCRAFT(pedToCheck)
				IF  IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(pedToCheck)].propertyDetails.iArmourAircraftModBS, PROPERTY_BROADCAST_ARMOUR_AIRCRAFT_CONCEAL_PILOT_SEAT_TRANS)
				OR GET_SCRIPT_TASK_STATUS(GET_PLAYER_PED(pedToCheck), SCRIPT_TASK_ENTER_VEHICLE) = WAITING_TO_START_TASK
				OR GET_SCRIPT_TASK_STATUS(GET_PLAYER_PED(pedToCheck), SCRIPT_TASK_ENTER_VEHICLE) = PERFORMING_TASK
					IF GlobalPlayerBD[NATIVE_TO_INT(pedToCheck)].SimpleInteriorBD.propertyOwner != INVALID_PLAYER_INDEX()
						IF GlobalPlayerBD[NATIVE_TO_INT(pedToCheck)].SimpleInteriorBD.propertyOwner = pAvengerOwner
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF	
			ELSE
				IF DOES_ENTITY_EXIST(avengerVeh)
				AND NOT IS_ENTITY_DEAD(avengerVeh)
					IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(pedToCheck), avengerVeh)
						IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(pedToCheck)].propertyDetails.iArmourAircraftModBS, PROPERTY_BROADCAST_ARMOUR_AIRCRAFT_COCKPIT_SEAT_SHUFFLE)
						OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(pedToCheck)].propertyDetails.iArmourAircraftModBS, PROPERTY_BROADCAST_ARMOUR_AIRCRAFT_CONCEAL_PILOT_SEAT_TRANS)
						OR GET_SCRIPT_TASK_STATUS(GET_PLAYER_PED(pedToCheck), SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT) = WAITING_TO_START_TASK
						OR GET_SCRIPT_TASK_STATUS(GET_PLAYER_PED(pedToCheck), SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT) = PERFORMING_TASK
						OR GET_PED_RESET_FLAG(GET_PLAYER_PED(pedToCheck), PRF_IsSeatShuffling)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDIF	
	
	RETURN FALSE
ENDFUNC

PLAYER_INDEX pedEnteringAvenger
FUNC BOOL CAN_PLAYER_EXIT_TO_PILOT_SEAT(PLAYER_INDEX pedToCheck)

	IF (IS_NET_PLAYER_OK(pedToCheck)
	AND pedToCheck != PLAYER_ID())
		IF NOT IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(pedToCheck)
			IF DOES_ENTITY_EXIST(g_viAvengerRemoteVehIndex)
				IF GET_VEHICLE_PED_IS_ENTERING(GET_PLAYER_PED(pedToCheck)) = g_viAvengerRemoteVehIndex
				OR INT_TO_NATIVE(VEHICLE_INDEX, GET_VEHICLE_PED_IS_TRYING_TO_ENTER(GET_PLAYER_PED(pedToCheck))) = g_viAvengerRemoteVehIndex
					pedEnteringAvenger = pedToCheck
					RETURN FALSE
				ENDIF
			ELSE
				PRINTLN("PERVENT_PLAYER_FROM_GETTING_STUCK_IN_AVENGER_MODEL - g_viAvengerRemoteVehIndex doesn't exist")
			ENDIF	
		ENDIF
	ENDIF
	
	IF (IS_NET_PLAYER_OK(pedEnteringAvenger)
	AND pedEnteringAvenger != PLAYER_ID())
		IF NOT IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(pedEnteringAvenger)
			IF DOES_ENTITY_EXIST(g_viAvengerRemoteVehIndex)
				IF GET_VEHICLE_PED_IS_ENTERING(GET_PLAYER_PED(pedEnteringAvenger)) = g_viAvengerRemoteVehIndex
				OR INT_TO_NATIVE(VEHICLE_INDEX, GET_VEHICLE_PED_IS_TRYING_TO_ENTER(GET_PLAYER_PED(pedEnteringAvenger))) = g_viAvengerRemoteVehIndex
					RETURN FALSE
				ENDIF
			ELSE
				PRINTLN("PERVENT_PLAYER_FROM_GETTING_STUCK_IN_AVENGER_MODEL - g_viAvengerRemoteVehIndex doesn't exist")
			ENDIF	
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CAN_PLAYER_EXIT_TO_CREATOR_AVENGER_PILOT_SEAT(PLAYER_INDEX pedToCheck)

	IF IS_NET_PLAYER_OK(pedToCheck)
	AND pedToCheck != PLAYER_ID()
		IF NOT IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(pedToCheck)
			IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehCreatorAircraft)
				IF GET_VEHICLE_PED_IS_ENTERING(GET_PLAYER_PED(pedToCheck)) = MPGlobalsAmbience.vehCreatorAircraft
				OR INT_TO_NATIVE(VEHICLE_INDEX, GET_VEHICLE_PED_IS_TRYING_TO_ENTER(GET_PLAYER_PED(pedToCheck))) = MPGlobalsAmbience.vehCreatorAircraft
					pedEnteringAvenger = pedToCheck
					RETURN FALSE
				ENDIF
			ELSE
				PRINTLN("PERVENT_PLAYER_FROM_GETTING_STUCK_IN_AVENGER_MODEL - MPGlobalsAmbience.vehCreatorAircraft doesn't exist")
			ENDIF
		ENDIF
	ENDIF
	
	IF (IS_NET_PLAYER_OK(pedEnteringAvenger)
	AND pedEnteringAvenger != PLAYER_ID())
		IF NOT IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(pedEnteringAvenger)
			IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehCreatorAircraft)
				IF GET_VEHICLE_PED_IS_ENTERING(GET_PLAYER_PED(pedEnteringAvenger)) = MPGlobalsAmbience.vehCreatorAircraft
				OR INT_TO_NATIVE(VEHICLE_INDEX, GET_VEHICLE_PED_IS_TRYING_TO_ENTER(GET_PLAYER_PED(pedEnteringAvenger))) = MPGlobalsAmbience.vehCreatorAircraft
					RETURN FALSE
				ENDIF
			ELSE
				PRINTLN("PERVENT_PLAYER_FROM_GETTING_STUCK_IN_AVENGER_MODEL - g_viAvengerRemoteVehIndex doesn't exist")
			ENDIF	
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_ANY_PLAYER_ENTERING_AVENGER(VEHICLE_INDEX avengerVeh, PLAYER_INDEX pedToCheck)

	IF IS_NET_PLAYER_OK(pedToCheck)
		IF NOT IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(pedToCheck)
			IF GET_DISTANCE_BETWEEN_ENTITIES(avengerVeh, GET_PLAYER_PED(pedToCheck)) < 5
				IF GET_VEHICLE_PED_IS_ENTERING(GET_PLAYER_PED(pedToCheck)) = avengerVeh
				OR INT_TO_NATIVE(VEHICLE_INDEX, GET_VEHICLE_PED_IS_TRYING_TO_ENTER(GET_PLAYER_PED(pedToCheck))) = avengerVeh
					RETURN TRUE
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

BOOL BlockExit, bBlockEnterToHold

PROC BLOCK_AVENGER_HOLD_ENTRY_FROM_COCKPIT(VEHICLE_INDEX avengerVeh, PLAYER_INDEX pedToCheck)
	IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), avengerVeh)
		IF IS_ANY_PLAYER_ENTERING_AVENGER(avengerVeh, pedToCheck)
			g_bPlayerViewingTurretHud = TRUE
			bBlockEnterToHold = TRUE	
		ELSE
			IF bBlockEnterToHold
				g_bPlayerViewingTurretHud = FALSE
				bBlockEnterToHold = FALSE	
			ENDIF
		ENDIF
	ELSE
		IF bBlockEnterToHold
			g_bPlayerViewingTurretHud = FALSE
			bBlockEnterToHold = FALSE	
		ENDIF
	ENDIF
ENDPROC


VECTOR vPlayerEntryCoords
BOOL bGetPedCoords = FALSE

PROC PERVENT_PLAYER_FROM_GETTING_STUCK_IN_AVENGER_MODEL()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF NOT NETWORK_IS_ACTIVITY_SESSION()
			VEHICLE_INDEX nearbyVehs[1]
			PLAYER_INDEX pedToCheck = INT_TO_PLAYERINDEX(iRemoteVehicleStagger)
			IF GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), nearbyVehs) > 0	
				IF DOES_ENTITY_EXIST(nearbyVehs[0])
				AND IS_ENTITY_A_VEHICLE(nearbyVehs[0])
					IF NOT IS_ENTITY_DEAD(nearbyVehs[0])
						IF IS_VEHICLE_A_PERSONAL_ARMORY_AIRCRAFT(nearbyVehs[0])
							IF IS_ANY_PLAYER_EXITING_HOLD_OF_AVENGER(GET_OWNER_OF_PERSONAL_ARMORY_AIRCRAFT(nearbyVehs[0], FALSE, pedToCheck), nearbyVehs[0], pedToCheck)
								IF GET_VEHICLE_PED_IS_TRYING_TO_ENTER(PLAYER_PED_ID()) = NATIVE_TO_INT(nearbyVehs[0])
								OR GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID()) = nearbyVehs[0]
									IF NOT bGetPedCoords
										vPlayerEntryCoords = GET_PLAYER_COORDS(PLAYER_ID())
										bGetPedCoords = TRUE
									ENDIF
									TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(),0, ECF_WARP_PED)
									NET_WARP_TO_COORD(vPlayerEntryCoords, 0)
								ELSE
									bGetPedCoords = FALSE
								ENDIF
								DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
							ENDIF
							BLOCK_AVENGER_HOLD_ENTRY_FROM_COCKPIT(nearbyVehs[0], pedToCheck)
						ENDIF
					ENDIF
				ENDIF	
			ENDIF	
			
			IF IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<506.976685,4750.366211,-69.995972>>, <<509.373657,4750.375488,-67.495972>>, 2.750000)
					IF NOT CAN_PLAYER_EXIT_TO_PILOT_SEAT(pedToCheck)
						IF NOT IS_SIMPLE_INTERIOR_EXIT_BLOCKED_WITH_BG_SCRIPT()
							BLOCK_SIMPLE_INTERIOR_EXIT_WITH_BG_SCRIPT(TRUE)
							BlockExit = TRUE
							PRINTLN("PERVENT_PLAYER_FROM_GETTING_STUCK_IN_AVENGER_MODEL - BlockExit = TRUE")
						ENDIF	
					ELSE
						IF BlockExit
							BLOCK_SIMPLE_INTERIOR_EXIT_WITH_BG_SCRIPT(FALSE)
							BlockExit = FALSE
							PRINTLN("PERVENT_PLAYER_FROM_GETTING_STUCK_IN_AVENGER_MODEL - BlockExit = FALSE 1")
						ENDIF	
					ENDIF
				ELSE
					IF BlockExit
						BLOCK_SIMPLE_INTERIOR_EXIT_WITH_BG_SCRIPT(FALSE)
						BlockExit = FALSE
						PRINTLN("PERVENT_PLAYER_FROM_GETTING_STUCK_IN_AVENGER_MODEL - BlockExit = FALSE 2")
					ENDIF	
				ENDIF	
			ELSE
				IF BlockExit
					BLOCK_SIMPLE_INTERIOR_EXIT_WITH_BG_SCRIPT(FALSE)
					BlockExit = FALSE
					PRINTLN("PERVENT_PLAYER_FROM_GETTING_STUCK_IN_AVENGER_MODEL - BlockExit = FALSE 3")
				ENDIF
			ENDIF
		
		ELSE
			VEHICLE_INDEX nearbyVehs[1]
			PLAYER_INDEX pedToCheck = INT_TO_PLAYERINDEX(iRemoteVehicleStagger)
			IF GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), nearbyVehs) > 0
				IF DOES_ENTITY_EXIST(nearbyVehs[0])
				AND IS_ENTITY_A_VEHICLE(nearbyVehs[0])
					IF NOT IS_ENTITY_DEAD(nearbyVehs[0])
						IF DECOR_IS_REGISTERED_AS_TYPE("Creator_Trailer", DECOR_TYPE_INT)
							IF IS_VEHICLE_MODEL(nearbyVehs[0], AVENGER)
								IF IS_ANY_PLAYER_EXITING_HOLD_OF_CREATOR_AVENGER(GET_OWNER_OF_CREATOR_TRAILER(nearbyVehs[0], FALSE, pedToCheck), nearbyVehs[0], pedToCheck)
									IF GET_VEHICLE_PED_IS_TRYING_TO_ENTER(PLAYER_PED_ID()) = NATIVE_TO_INT(nearbyVehs[0])
									OR GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID()) = nearbyVehs[0]
										IF NOT bGetPedCoords
											vPlayerEntryCoords = GET_PLAYER_COORDS(PLAYER_ID())
											bGetPedCoords = TRUE
										ENDIF
										TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(),0, ECF_WARP_PED)
										NET_WARP_TO_COORD(vPlayerEntryCoords, 0)
									ENDIF
									DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
								ENDIF
								BLOCK_AVENGER_HOLD_ENTRY_FROM_COCKPIT(nearbyVehs[0], pedToCheck)
							ENDIF	
						ENDIF
					ENDIF
				ENDIF	
			ENDIF	
			
			IF IS_PLAYER_IN_CREATOR_AIRCRAFT(PLAYER_ID())
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<506.976685,4750.366211,-69.995972>>, <<509.373657,4750.375488,-67.495972>>, 2.750000)
					IF NOT CAN_PLAYER_EXIT_TO_CREATOR_AVENGER_PILOT_SEAT(pedToCheck)
						IF NOT IS_SIMPLE_INTERIOR_EXIT_BLOCKED_WITH_BG_SCRIPT()
							BLOCK_SIMPLE_INTERIOR_EXIT_WITH_BG_SCRIPT(TRUE)
							BlockExit = TRUE
							PRINTLN("PERVENT_PLAYER_FROM_GETTING_STUCK_IN_AVENGER_MODEL - BlockExit = TRUE")
						ENDIF	
					ELSE
						IF BlockExit
							BLOCK_SIMPLE_INTERIOR_EXIT_WITH_BG_SCRIPT(FALSE)
							BlockExit = FALSE
							PRINTLN("PERVENT_PLAYER_FROM_GETTING_STUCK_IN_AVENGER_MODEL - BlockExit = FALSE 1")
						ENDIF	
					ENDIF
				ELSE
					IF BlockExit
						BLOCK_SIMPLE_INTERIOR_EXIT_WITH_BG_SCRIPT(FALSE)
						BlockExit = FALSE
						PRINTLN("PERVENT_PLAYER_FROM_GETTING_STUCK_IN_AVENGER_MODEL - BlockExit = FALSE 2")
					ENDIF	
				ENDIF	
			ELSE
				IF BlockExit
					BLOCK_SIMPLE_INTERIOR_EXIT_WITH_BG_SCRIPT(FALSE)
					BlockExit = FALSE
					PRINTLN("PERVENT_PLAYER_FROM_GETTING_STUCK_IN_AVENGER_MODEL - BlockExit = FALSE 3")
				ENDIF
			ENDIF
			
		ENDIF
	ELSE
		bGetPedCoords = FALSE
		IF BlockExit
			BLOCK_SIMPLE_INTERIOR_EXIT_WITH_BG_SCRIPT(FALSE)
			BlockExit = FALSE
			PRINTLN("PERVENT_PLAYER_FROM_GETTING_STUCK_IN_AVENGER_MODEL - BlockExit = FALSE 4")
		ENDIF
		IF bBlockEnterToHold
			g_bPlayerViewingTurretHud = FALSE
			bBlockEnterToHold = FALSE
			PRINTLN("PERVENT_PLAYER_FROM_GETTING_STUCK_IN_AVENGER_MODEL - bBlockEnterToHold = FALSE 4")
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_ASSIGN_HACKER_TRUCK_TO_THIS_SCRIPT()
	INT iInstance
	STRING scriptName 
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF SHOULD_HACKER_TRUCK_ASSIGN_TO_MAIN_SCRIPT()
			IF IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER)
				VEHICLE_INDEX newVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF IS_VEHICLE_DRIVEABLE(newVeh)
					IF IS_VEHICLE_A_PERSONAL_HACKER_TRUCK(newVeh)
					AND IS_HACKER_TRUCK_PURCHASED()
						IF NETWORK_GET_ENTITY_IS_NETWORKED(newVeh)
							IF NETWORK_HAS_CONTROL_OF_ENTITY(newVeh)
								IF CAN_REGISTER_MISSION_VEHICLES(1)
									scriptName = GET_ENTITY_SCRIPT(newVeh,iInstance)
									PRINTLN("[HACKER_TRUCK_VEH] MAINTAIN_ASSIGN_HACKER_TRUCK_TO_THIS_SCRIPT: script name = ",scriptName)
									IF NOT IS_STRING_NULL_OR_EMPTY(scriptName)
										IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(scriptName)) <= 0
											IF IS_ENTITY_A_MISSION_ENTITY(newVeh)
												PRINTLN("[HACKER_TRUCK_VEH] MAINTAIN_ASSIGN_HACKER_TRUCK_TO_THIS_SCRIPT - waiting for entity to not be a mission entity as script that created it is cleaning up")
											ELSE
												PRINTLN("[HACKER_TRUCK_VEH] MAINTAIN_ASSIGN_HACKER_TRUCK_TO_THIS_SCRIPT: NOT a mission Entity")
											ENDIF
										ELSE
											PRINTLN("[HACKER_TRUCK_VEH] MAINTAIN_ASSIGN_HACKER_TRUCK_TO_THIS_SCRIPT: num scripts running = ",GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(scriptName)))
										ENDIF
									ENDIF
									
									IF NOT IS_ENTITY_A_MISSION_ENTITY(newVeh)
										SET_ENTITY_AS_MISSION_ENTITY(newVeh, FALSE, TRUE)
										PRINTLN("[HACKER_TRUCK_VEH] MAINTAIN_ASSIGN_HACKER_TRUCK_TO_THIS_SCRIPT - not a mission entity, setting as one.")
									ENDIF
									
									IF IS_ENTITY_A_MISSION_ENTITY(newVeh)
										IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(newVeh)						
											SET_ENTITY_AS_MISSION_ENTITY(newVeh, FALSE, TRUE)
											PRINTLN("[HACKER_TRUCK_VEH] MAINTAIN_ASSIGN_HACKER_TRUCK_TO_THIS_SCRIPT - waiting for vehicle to belong to this script.") 							
										ELSE
											MPGlobalsAmbience.vehHackerTruck = newVeh
											GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV = VEH_TO_NET(newVeh)
											SET_HACKER_TRUCK_IS_IN_BUSINESS_HUB(FALSE)
											SET_HACKER_TRUCK_ASSIGN_TO_MAIN_SCRIPT(FALSE)
											PRINTLN("[HACKER_TRUCK_VEH] MAINTAIN_ASSIGN_HACKER_TRUCK_TO_THIS_SCRIPT - truck is belong to this script.") 		
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						PRINTLN("[HACKER_TRUCK_VEH] MAINTAIN_ASSIGN_HACKER_TRUCK_TO_THIS_SCRIPT - player is not in hacker truck.") 							
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDIF	
ENDPROC

PROC UPDATE_HACKER_TRUCK_I_AM_IN_VAR()
	IF IS_BIT_SET(iLocalBS, LOCAL_BS_SHOULD_ALLOW_EXPENSIVE_PROCESSING)
		BOOL bFoundHackerTruckIamIn = FALSE
		
		IF NATIVE_TO_INT(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner) != -1
			IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner)].netID_HackertruckV)
				IF (g_viHackerTruckIamIn = NULL)
					PRINTLN("UPDATE_HACKER_TRUCK_I_AM_IN_VAR - found hacker truck I am in, owned by, ", GET_PLAYER_NAME(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner))
				ENDIF
				
				g_viHackerTruckIamIn = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner)].netID_HackertruckV)
				bFoundHackerTruckIamIn = TRUE
			ENDIF
		ENDIF
		IF NOT bFoundHackerTruckIamIn
			IF !(g_viHackerTruckIamIn = NULL)
				PRINTLN("UPDATE_HACKER_TRUCK_I_AM_IN_VAR - lost hacker truck I am in")
			ENDIF
			g_viHackerTruckIamIn = NULL
		ENDIF
	ENDIF	
ENDPROC

#IF FEATURE_DLC_2_2022
PROC MAINTAIN_ASSIGN_ACID_LAB_TO_THIS_SCRIPT()
	INT iInstance
	STRING scriptName 
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF SHOULD_ACID_LAB_ASSIGN_TO_MAIN_SCRIPT()
			IF IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER)
				VEHICLE_INDEX newVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF IS_VEHICLE_DRIVEABLE(newVeh)
					IF IS_VEHICLE_A_PERSONAL_ACID_LAB(newVeh)
					AND IS_PLAYER_ACID_LAB_PURCHASED(PLAYER_ID())
						IF NETWORK_GET_ENTITY_IS_NETWORKED(newVeh)
							IF NETWORK_HAS_CONTROL_OF_ENTITY(newVeh)
								IF CAN_REGISTER_MISSION_VEHICLES(1)
									scriptName = GET_ENTITY_SCRIPT(newVeh,iInstance)
									PRINTLN("[ACID_LAB_VEH] MAINTAIN_ASSIGN_ACID_LAB_TO_THIS_SCRIPT: script name = ",scriptName)
									IF NOT IS_STRING_NULL_OR_EMPTY(scriptName)
										IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(scriptName)) <= 0
											IF IS_ENTITY_A_MISSION_ENTITY(newVeh)
												PRINTLN("[ACID_LAB_VEH] MAINTAIN_ASSIGN_ACID_LAB_TO_THIS_SCRIPT - waiting for entity to not be a mission entity as script that created it is cleaning up")
											ELSE
												PRINTLN("[ACID_LAB_VEH] MAINTAIN_ASSIGN_ACID_LAB_TO_THIS_SCRIPT: NOT a mission Entity")
											ENDIF
										ELSE
											PRINTLN("[ACID_LAB_VEH] MAINTAIN_ASSIGN_ACID_LAB_TO_THIS_SCRIPT: num scripts running = ",GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(scriptName)))
										ENDIF
									ENDIF
									
									IF NOT IS_ENTITY_A_MISSION_ENTITY(newVeh)
										SET_ENTITY_AS_MISSION_ENTITY(newVeh, FALSE, TRUE)
										PRINTLN("[ACID_LAB_VEH] MAINTAIN_ASSIGN_ACID_LAB_TO_THIS_SCRIPT - not a mission entity, setting as one.")
									ENDIF
									
									IF IS_ENTITY_A_MISSION_ENTITY(newVeh)
										IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(newVeh)						
											SET_ENTITY_AS_MISSION_ENTITY(newVeh, FALSE, TRUE)
											PRINTLN("[ACID_LAB_VEH] MAINTAIN_ASSIGN_ACID_LAB_TO_THIS_SCRIPT - waiting for vehicle to belong to this script.") 							
										ELSE
											MPGlobalsAmbience.vehAcidLab = newVeh
											GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV = VEH_TO_NET(newVeh)
											SET_ACID_LAB_IN_JUGGALO_HIDEOUT(FALSE)
											SET_ACID_LAB_ASSIGN_TO_MAIN_SCRIPT(FALSE)
											PRINTLN("[ACID_LAB_VEH] MAINTAIN_ASSIGN_ACID_LAB_TO_THIS_SCRIPT - truck is belong to this script.") 		
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						PRINTLN("[ACID_LAB_VEH] MAINTAIN_ASSIGN_ACID_LAB_TO_THIS_SCRIPT - player is not in acid lab.") 							
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDIF	
ENDPROC

PROC UPDATE_ACID_LAB_I_AM_IN_VAR()
	IF IS_BIT_SET(iLocalBS, LOCAL_BS_SHOULD_ALLOW_EXPENSIVE_PROCESSING)
		BOOL bFoundAcidLabIamIn = FALSE
		
		IF NATIVE_TO_INT(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner) != -1
			IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner)].netID_AcidLabV)
				IF (g_viAcidLabIamIn = NULL)
					PRINTLN("UPDATE_ACID_LAB_I_AM_IN_VAR - found AcidLab I am in, owned by, ", GET_PLAYER_NAME(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner))
				ENDIF
				
				g_viAcidLabIamIn = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner)].netID_AcidLabV)
				bFoundAcidLabIamIn = TRUE
			ENDIF
		ENDIF
		IF NOT bFoundAcidLabIamIn
			IF !(g_viAcidLabIamIn = NULL)
				PRINTLN("UPDATE_ACID_LAB_I_AM_IN_VAR - lost AcidLab I am in")
			ENDIF
			g_viAcidLabIamIn = NULL
		ENDIF
	ENDIF	
ENDPROC
#ENDIF

#IF FEATURE_HEIST_ISLAND
PROC UPDATE_SUBMARINE_I_AM_IN_VAR()
	IF IS_BIT_SET(iLocalBS, LOCAL_BS_SHOULD_ALLOW_EXPENSIVE_PROCESSING)
		BOOL bFoundSubmarineIamIn = FALSE
		
		IF NATIVE_TO_INT(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner) != -1
			IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner)].netID_SubmarineV)
				IF (g_viSubmarineIamIn = NULL)
					PRINTLN("UPDATE_SUBMARINE_I_AM_IN_VAR - found submarine I am in, owned by, ", GET_PLAYER_NAME(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner))
				ENDIF
				
				g_viSubmarineIamIn = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner)].netID_SubmarineV)
				bFoundSubmarineIamIn = TRUE
			ENDIF
		ENDIF
		IF NOT bFoundSubmarineIamIn
			IF !(g_viSubmarineIamIn = NULL)
				PRINTLN("UPDATE_SUBMARINE_I_AM_IN_VAR - lost submarine I am in")
			ENDIF
			g_viSubmarineIamIn = NULL
		ENDIF
	ENDIF	
ENDPROC

PROC MAINTAIN_SUBMARINE_SURFACE_REQUEST()
	IF SHOULD_SURFACE_SUBMARINE()
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			VEHICLE_INDEX submarineVeh = g_vehSubmarine[NATIVE_TO_INT(PLAYER_ID())]
			IF IS_ENTITY_ALIVE(submarineVeh)
				IF TAKE_CONTROL_OF_ENTITY(submarineVeh)
					CLEAR_PRIMARY_VEHICLE_TASK(submarineVeh)
					SET_DISABLE_SUPERDUMMY(submarineVeh, FALSE)
					FORCE_SUBMARINE_NEUTRAL_BUOYANCY(submarineVeh, 0)

					SET_SURFACE_SUBMARINE(FALSE)
					PRINTLN("MAINTAIN_SUBMARINE_SURFACE_REQUEST done")
				ELSE
					PRINTLN("MAINTAIN_SUBMARINE_SURFACE_REQUEST can't take control")
				ENDIF	
			ELSE
				SET_SURFACE_SUBMARINE(FALSE)
			ENDIF
		ELSE
			SET_SURFACE_SUBMARINE(FALSE)
		ENDIF
	ENDIF
ENDPROC
#ENDIF

PROC UPDATE_MAIN_BLIMP3_MODS()
	IF SHOULD_UPDATE_MAIN_BLIMP3_MODS(PLAYER_ID())
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV)
			IF NOT IS_ENTITY_DEAD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV))
				IF GET_ENTITY_MODEL(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV)) = BLIMP3
					IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV))
						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV)		
	//						INT iSaveSlot
	//						MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(DISPLAY_SLOT_BLIMP3_HELI  , iSaveSlot)
	//						
	//						IF iSaveSlot > 0 
	//							VEHICLE_INDEX Blimp3Index = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV)
	//							MP_SAVE_VEHICLE_APPLY_SAVED_DETAILS_TO_VEHICLE(Blimp3Index,iSaveSlot)
	//						ENDIF
							
							SET_COMMON_PROPERTIES_FOR_PEGASUS_VEHICLE()
							SET_UPDATE_MAIN_BLIMP3_MODS(FALSE)
						ELSE
							NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

PROC PROCESS_DRONE_COOL_DOWN()
	IF PLAYER_ID() != INVALID_PLAYER_INDEX()
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iHackerTruckModBS, PROPERTY_BROADCAST_HACKER_TRUCK_DRONE_IN_COOL_DOWN)
			IF !HAS_NET_TIMER_STARTED(g_sDroneCoolDownTimer)
				START_NET_TIMER(g_sDroneCoolDownTimer)
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.droneSeatCooldownTimer = g_sDroneCoolDownTimer.Timer
			ELSE
				IF HAS_NET_TIMER_EXPIRED(g_sDroneCoolDownTimer, GET_DRONE_COOL_DOWN_TIMER())
					SET_DRONE_IN_COOL_DOWN(FALSE)
					IF  GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(player_id()) != FMMC_TYPE_FMBB_INFILTRATION
						OVERRIDE_DRONE_COOL_DOWN_TIMER(-1)
					ENDIF	
					RESET_NET_TIMER(g_sDroneCoolDownTimer)
				ENDIF
			ENDIF	
		ENDIF
	ENDIF	
ENDPROC

#IF FEATURE_HEIST_ISLAND
PROC PROCESS_SUBMARINE_GUIDED_MISSILE_COOL_DOWN()
	IF PLAYER_ID() != INVALID_PLAYER_INDEX()
		IF IS_LOCAL_PLAYER_SUBMARINE_GUIDED_MISSILE_ONE_IN_COOL_DOWN()
			IF !HAS_NET_TIMER_STARTED(g_sSubmarineMissileOneCoolDownTimer)
				START_NET_TIMER(g_sSubmarineMissileOneCoolDownTimer)
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sSubmarinePropertyData.GuidedMissileSeatOneCooldownTimer = g_sSubmarineMissileOneCoolDownTimer.Timer
			ELSE
				IF HAS_NET_TIMER_EXPIRED(g_sSubmarineMissileOneCoolDownTimer, GET_SUBMARINE_GUIDED_MISSILE_SEAT_ONE_COOL_DOWN_TIMER())
					SET_SUBMARINE_GUIDED_MISSILE_ONE_IN_COOL_DOWN(FALSE)
					RESET_NET_TIMER(g_sSubmarineMissileOneCoolDownTimer)
				ENDIF
			ENDIF	
		ENDIF
		
		IF IS_LOCAL_PLAYER_SUBMARINE_GUIDED_MISSILE_TWO_IN_COOL_DOWN()
			IF !HAS_NET_TIMER_STARTED(g_sSubmarineMissileTwoCoolDownTimer)
				START_NET_TIMER(g_sSubmarineMissileTwoCoolDownTimer)
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sSubmarinePropertyData.GuidedMissileSeatTwoCooldownTimer = g_sSubmarineMissileTwoCoolDownTimer.Timer
			ELSE
				IF HAS_NET_TIMER_EXPIRED(g_sSubmarineMissileTwoCoolDownTimer, GET_SUBMARINE_GUIDED_MISSILE_SEAT_TWO_COOL_DOWN_TIMER())
					SET_SUBMARINE_GUIDED_MISSILE_TWO_IN_COOL_DOWN(FALSE)
					RESET_NET_TIMER(g_sSubmarineMissileTwoCoolDownTimer)
				ENDIF
			ENDIF	
		ENDIF
	ENDIF	
ENDPROC
#ENDIF

PROC PROCESS_DRONE_TARGET_CREATION()
	IF GB_IS_PLAYER_CRITICAL_TO_THIS_JOB(PLAYER_ID(), ENUM_TO_INT(FMMC_TYPE_FMBB_JEWEL_STORE_GRAB))
		IF PLAYER_ID() != INVALID_PLAYER_INDEX()
			IF SHOULD_CREATE_DRONE_TARGET(PLAYER_ID())
				MODEL_NAMES droneProp = INT_TO_ENUM(MODEL_NAMES, HASH("ba_prop_Battle_SecPanel"))	
				OBJECT_INDEX placedPropTarget = GET_CLOSEST_OBJECT_OF_TYPE(g_sDroneGlobals.vDroneTargetCoord, 2 ,droneProp, FALSE, FALSE, FALSE)
				IF DOES_ENTITY_EXIST(placedPropTarget)
					SET_ENTITY_VISIBLE(placedPropTarget, FALSE)
					FREEZE_ENTITY_POSITION(placedPropTarget, TRUE)
					SET_ENTITY_COLLISION(placedPropTarget, FALSE)
					SET_ENTITY_COMPLETELY_DISABLE_COLLISION(placedPropTarget, FALSE)
				ENDIF
				
				MODEL_NAMES droneDamageProp = INT_TO_ENUM(MODEL_NAMES, HASH("ba_prop_Battle_SecPanel_Dam"))
				IF REQUEST_LOAD_MODEL(droneDamageProp)
					IF NOT DOES_ENTITY_EXIST(droneTargetObjectDamaged)
						IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE, RESERVATION_LOCAL_ONLY) + 1, FALSE, TRUE)
							droneTargetObjectDamaged = CREATE_OBJECT(droneDamageProp,  <<-631.0264, -227.670, 38.4782>>, TRUE, FALSE, TRUE)
							SET_ENTITY_COORDS_NO_OFFSET(droneTargetObjectDamaged,  <<-631.0264, -227.670, 38.4782>>)
							SET_ENTITY_HEADING(droneTargetObjectDamaged, g_sDroneGlobals.fDroneTargetHeading)
							FREEZE_ENTITY_POSITION(droneTargetObjectDamaged, TRUE)
							SET_ENTITY_VISIBLE(droneTargetObjectDamaged, TRUE)
							SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(OBJ_TO_NET(droneTargetObjectDamaged), PLAYER_ID(), TRUE)
							SET_CREATE_DRONE_TARGET(FALSE)
							PRINTLN("[AM_MP_DRONE] - PROCESS_DRONE_TARGET_CREATION - model swaped")
						ENDIF
					ENDIF	
				ENDIF	
			ENDIF
		ENDIF
	ELSE
		IF DOES_ENTITY_EXIST(droneTargetObjectDamaged)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(droneTargetObjectDamaged)
				DELETE_OBJECT(droneTargetObjectDamaged)
				PRINTLN("[AM_MP_DRONE] - PROCESS_DRONE_TARGET_CREATION - DELETE_OBJECT swaped model")
			ELSE
				NETWORK_REQUEST_CONTROL_OF_ENTITY(droneTargetObjectDamaged)
			ENDIF
		ENDIF
		SET_CREATE_DRONE_TARGET(FALSE)
	ENDIF
ENDPROC

PROC MAINTAIN_VEHICLE_NAME_WARNING_HELP()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF !IS_BIT_SET(iLocalBS, LOCAL_BS_DISPLAY_VEHICLE_NAME_WARNING)
			IF IS_LOCAL_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR()
				IF GET_SIMPLE_INTERIOR_TYPE(GET_SIMPLE_INTERIOR_PLAYER_IS_WALKING_IN_OR_OUT_FROM(PLAYER_ID())) = SIMPLE_INTERIOR_TYPE_ARENA_GARAGE
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						INT iSaveSlot = CURRENT_SAVED_VEHICLE_SLOT()
						INT iDisplaySlot 
						MPSV_GET_DISPLAY_SLOT_FROM_SAVE_SLOT(iSaveSlot, iDisplaySlot)
						TEXT_LABEL_63 tl63VehicleName = GET_ARENA_VEHICLE_NAME(-1, iDisplaySlot)
						IF !IS_STRING_NULL_OR_EMPTY(tl63VehicleName)
							SET_BIT(iLocalBS, LOCAL_BS_DISPLAY_VEHICLE_NAME_WARNING)
						ELSE
							PRINTLN("MAINTAIN_VEHICLE_NAME_WARNING_HELP vehicle doesn't have name or null")
						ENDIF
					ENDIF	
				ENDIF	
			ENDIF	
		ELSE
			IF IS_SCREEN_FADED_IN()
				IF !IS_LOCAL_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR()
					INT iStat = GET_PACKED_STAT_INT(PACKED_MP_INT_ARENA_VEHICLE_NAME_HELP_COUNT)
					IF iStat < 4
						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						AND NOT IS_HELP_MESSAGE_ON_SCREEN()
							PRINT_HELP("VEH_NAME_WARN")
							iStat++
							SET_PACKED_STAT_INT(PACKED_MP_INT_ARENA_VEHICLE_NAME_HELP_COUNT, iStat)
							CLEAR_BIT(iLocalBS, LOCAL_BS_DISPLAY_VEHICLE_NAME_WARNING)
							PRINTLN("MAINTAIN_VEHICLE_NAME_WARNING_HELP - settin stat to: ", iStat)
						ENDIF
					ELSE
						CLEAR_BIT(iLocalBS, LOCAL_BS_DISPLAY_VEHICLE_NAME_WARNING)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		CLEAR_BIT(iLocalBS, LOCAL_BS_DISPLAY_VEHICLE_NAME_WARNING)
	ENDIF
ENDPROC

#IF FEATURE_CASINO
FUNC BOOL MAINTAIN_LAUNCHING_VEHICLE_REWARD_SCRIPT()
	
	IF NOT g_bEnableVehRewardScriptInIsland
	AND IS_PLAYER_ON_HEIST_ISLAND(PLAYER_ID())
		RETURN FALSE
	ENDIF	
	
	//Make sure the car mod script is not started too early after being killed
	//Make sure the car mod script cleanup flag is false
	IF g_bCleanUpVehRewardScript = TRUE
		IF NOT HAS_NET_TIMER_STARTED(sVehRewardScriptRunTimer)
			START_NET_TIMER(sVehRewardScriptRunTimer)
		ELSE
			IF HAS_NET_TIMER_EXPIRED(sVehRewardScriptRunTimer, 8000)
				g_bCleanUpVehRewardScript = FALSE
				RESET_NET_TIMER(sVehRewardScriptRunTimer)
				#IF IS_DEBUG_BUILD
				PRINTLN("MAINTAIN_LAUNCHING_VEHICLE_REWARD_SCRIPT - Setting g_bCleanUpVehRewardScript To FALSE")
				#ENDIF
			ENDIF	
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_VEHICLE_REWARD_SCRIPT_READY)
		sVehRewardScriptName = "AM_MP_VEHICLE_REWARD"
		REQUEST_SCRIPT(sVehRewardScriptName)
		IF HAS_SCRIPT_LOADED(sVehRewardScriptName)
		AND NOT IS_THREAD_ACTIVE(vehRewardThread)
		AND !g_bCleanUpVehRewardScript

			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(sVehRewardScriptName)) < 1
				IF NOT NETWORK_IS_SCRIPT_ACTIVE(sVehRewardScriptName, -1, TRUE)
					INT iStackSize = DEFAULT_STACK_SIZE
					
					// Running larger stack size for debug since we have some widgets which uses big structs 
					#IF IS_DEBUG_BUILD
						iStackSize = FRIEND_STACK_SIZE
					#ENDIF
					
					vehRewardThread = START_NEW_SCRIPT_WITH_NAME_HASH(GET_HASH_KEY(sVehRewardScriptName), iStackSize)
					SET_SCRIPT_AS_NO_LONGER_NEEDED(sVehRewardScriptName)
					SET_BIT(iLocalBS, LOCAL_BS_VEHICLE_REWARD_SCRIPT_READY)
					PRINTLN("MAINTAIN_LAUNCHING_VEHICLE_REWARD_SCRIPT - TRUE")
					RETURN TRUE
				ELSE
					#IF IS_DEBUG_BUILD
						PRINTLN("MAINTAIN_LAUNCHING_VEHICLE_REWARD_SCRIPT: Unable to start shop script for personal_car_mod_shop - last instance still active")
					#ENDIF					
				ENDIF
			ELSE
				PRINTLN("MAINTAIN_LAUNCHING_VEHICLE_REWARD_SCRIPT - carmod is already running")
				RETURN TRUE 
			ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
			CLEAN_UP_VEHICLE_REWARD_SCRIPT()
			TERMINATE_THREAD(vehRewardThread)
		ENDIF
		#ENDIF
	
	RETURN TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("MAINTAIN_LAUNCHING_VEHICLE_REWARD_SCRIPT - return false")
	#ENDIF
	RETURN FALSE
ENDFUNC
#ENDIF

PROC MAINTAIN_VEHICLE_CONTROLS_ONCE_PHONE_ON_SCREEN()
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_PHONE_ONSCREEN()
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				OR IS_DISABLED_CONTROL_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				OR IS_CONTROL_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
					DISABLE_VEH_ATTACK_CONTROLS()
				ENDIF
			ENDIF
		ENDIF	
	ENDIF	
ENDPROC

/// PURPOSE:
///    Performs all every-frame processing for the local player
PROC CLIENT_PROCESSING()
	IF NOT IS_PLAYER_IN_CORONA()
		MAINTAIN_CLIENT_EVENT_GLOBALS()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CLIENT_EVENT_GLOBALS")
		#ENDIF	
		#ENDIF	
		
		MAINTAIN_CLIENT_SPAWN_POINT_STAGGER()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CLIENT_SPAWN_POINT_STAGGER")
		#ENDIF	
		#ENDIF	
		
		MAINTAINT_CLIENT_REQUEST_SPAWNING()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAINT_CLIENT_REQUEST_SPAWNING")
		#ENDIF	
		#ENDIF	
		
		MAINTAIN_CLIENT_REQUEST_FAIL()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CLIENT_REQUEST_FAIL")
		#ENDIF	
		#ENDIF	
		
		MAINTAIN_CLIENT_PHONECALL_REACTIONS()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CLIENT_PHONECALL_REACTIONS")
		#ENDIF	
		#ENDIF	
		
		MAINTAIN_CLIENT_RESTORE_LAST_VEHICLE()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CLIENT_RESTORE_LAST_VEHICLE")
		#ENDIF	
		#ENDIF
		
		MAINTAIN_CLIENT_BOOKING()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CLIENT_BOOKING")
		#ENDIF	
		#ENDIF
		
		MAINTAIN_CLIENT_PHONE_CALLS()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CLIENT_PHONE_CALLS")
		#ENDIF	
		#ENDIF
		
		MAINTAIN_CLIENT_TEXT_MESSAGES()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CLIENT_TEXT_MESSAGES")
		#ENDIF	
		#ENDIF
		
		MAINTAIN_CLIENT_SPAWNING_TIMER()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CLIENT_SPAWNING_TIMER")
		#ENDIF	
		#ENDIF
		
		MAINTAIN_CLIENT_CONVERT_TO_PEGASUS_VEHICLE()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CLIENT_CONVERT_TO_PEGASUS_VEHICLE")
		#ENDIF	
		#ENDIF
		
		MAINTAIN_CLIENT_CONVERT_TO_TRUCK_VEHICLE()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CLIENT_CONVERT_TO_TRUCK_VEHICLE")
		#ENDIF	
		#ENDIF
		
		MAINTAIN_CREATION_OF_OFFICE_HELICOPTER()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CREATION_OF_OFFICE_HELICOPTER")
		#ENDIF	
		#ENDIF
		
		MAINTAIN_MOVE_PEGASUS_VEH_NEARBY_REQUESTS()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MOVE_PEGASUS_VEH_NEARBY_REQUESTS")
		#ENDIF	
		#ENDIF
		
		MAINTAIN_CLIENT_CONVERT_TO_HACKER_TRUCK_VEHICLE()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CLIENT_CONVERT_TO_HACKER_TRUCK_VEHICLE")
		#ENDIF	
		#ENDIF
		
		#IF FEATURE_DLC_2_2022
		MAINTAIN_CLIENT_CONVERT_TO_ACID_LAB_VEHICLE()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CLIENT_CONVERT_TO_ACID_LAB_VEHICLE")
		#ENDIF	
		#ENDIF
		#ENDIF
		
		#IF FEATURE_HEIST_ISLAND
		MAINTAIN_CLIENT_CONVERT_TO_SUBMARINE_VEHICLE()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CLIENT_CONVERT_TO_SUBMARINE_VEHICLE")
		#ENDIF	
		#ENDIF
		#ENDIF
		
		
		#IF FEATURE_HEIST_ISLAND
		MAINTAIN_CLIENT_CONVERT_TO_SUBMARINE_DINGHY_VEHICLE()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CLIENT_CONVERT_TO_SUBMARINE_DINGHY_VEHICLE")
		#ENDIF	
		#ENDIF
		#ENDIF
		
		#IF FEATURE_DLC_2_2022
		MAINTAIN_CLIENT_CONVERT_TO_SUPPORT_BIKE_VEHICLE()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CLIENT_CONVERT_TO_SUBMARINE_DINGHY_VEHICLE")
		#ENDIF	
		#ENDIF
		#ENDIF
		
		MAINTAIN_CLIENT_CONVERT_TO_AVENGER_VEHICLE()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CLIENT_CONVERT_TO_AVENGER_VEHICLE")
		#ENDIF	
		#ENDIF		
		
		MAINTAIN_ENTER_AVENGER_INTERIOR_FROM_PILOT_SEAT()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ENTER_AVENGER_INTERIOR_FROM_PILOT_SEAT")
		#ENDIF	
		#ENDIF	
		
		MAINTAIN_WARP_INTO_VEHICLE_FROM_DEFUNCT_BASE()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_WARP_INTO_VEHICLE_FROM_DEFUNCT_BASE")
		#ENDIF	
		#ENDIF	
		
	ENDIF
		
	// We need to monitor certain vehicle door states to ensure they are closed when we want them to be
	MAINTAIN_LUXE_VEH_DOOR_STATE()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_LUXE_VEH_DOOR_STATE")
	#ENDIF	
	#ENDIF
		
	
	MAINTAIN_ARMORY_TRUCK_CAMERA_SHAKE()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ARMORY_TRUCK_CAMERA_SHAKE")
	#ENDIF	
	#ENDIF
	
	GRAB_MOC_ID_FOR_WANTED_LEVELS()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("GRAB_MOC_ID_FOR_WANTED_LEVELS")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_SPAWN_TRUCK_IN_BUNKER()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SPAWN_TRUCK_IN_BUNKER")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_TRUCK_TRAILER_LOCATION()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_TRUCK_TRAILER_LOCATION")
	#ENDIF	
	#ENDIF
	
	//MAINTAIN_BUNKER_TRUCK_ENTRY()
	SLOW_DOWN_TRUCK_TO_BUNKER_ROOM()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("SLOW_DOWN_TRUCK_TO_BUNKER_ROOM")
	#ENDIF	
	#ENDIF
	
	DONT_ALLOW_TRUCK_TO_MOVE_WHEN_OWNER_IS_ENTERING_WITH_VEHICLE()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("DONT_ALLOW_TRUCK_TO_MOVE_WHEN_OWNER_IS_ENTERING_WITH_VEHICLE")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_TRUCK_RENOVATION()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_TRUCK_RENOVATION")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_ARMORY_TRUCK_TURRET_ACCESS()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ARMORY_TRUCK_TURRET_ACCESS")
	#ENDIF	
	#ENDIF
	
	HANDLE_CONCEALING_PEGASUS_VEHICLE()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("HANDLE_CONCEALING_PEGASUS_VEHICLE")
	#ENDIF	
	#ENDIF
	
	UPDATE_HIDE_PERSONAL_TRUCK()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_HIDE_PERSONAL_TRUCK")
	#ENDIF	
	#ENDIF
	
	TAKE_CONTROL_OF_TRUCK()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("TAKE_CONTROL_OF_TRUCK")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_TRUCK_UNDER_MAP()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_TRUCK_UNDER_MAP")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_MOVING_OUT_OF_BUNKER()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MOVING_OUT_OF_BUNKER")
	#ENDIF	
	#ENDIF
	
	CLEANUP_TRUCK_AFTER_JOIN_CEO()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("CLEANUP_TRUCK_AFTER_JOIN_CEO")
	#ENDIF	
	#ENDIF
	
	CLEANUP_TRUCK_AFTER_TRAILER_IS_DESTROYED()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("CLEANUP_TRUCK_AFTER_TRAILER_IS_DESTROYED")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_MOVING_INTO_BUNKER()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MOVING_INTO_BUNKER")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_KICK_PLAYERS_OUT_OF_CREATOR_TRAILER()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_KICK_PLAYERS_OUT_OF_CREATOR_TRAILER")
	#ENDIF	
	#ENDIF
	
	UPDATE_TRUCK_TRAILER_I_AM_IN_VAR()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_TRUCK_TRAILER_I_AM_IN_VAR")
	#ENDIF	
	#ENDIF
	
	UPDATE_MAIN_TRUCK_TRAILER_MODS()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_MAIN_TRUCK_TRAILER_MODS")
	#ENDIF	
	#ENDIF
	
	#IF FEATURE_DLC_2_2022
	UPDATE_MAIN_HIDEOUT_VEHICLES_MODS()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_MAIN_HIDEOUT_VEHICLES_MODS")
	#ENDIF	
	#ENDIF
	#ENDIF
	
	MAINTAIN_VEHICLE_TURRET_POSITIONS()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_VEHICLE_TURRET_POSITIONS")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_FAKE_MOC_PED()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_FAKE_MOC_PED")
	#ENDIF	
	#ENDIF
	
	
	MAINTAIN_AVENGER()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_AVENGER")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_SPAWN_AVENGER_IN_DEFUNCT_BASE()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SPAWN_AVENGER_IN_DEFUNCT_BASE")
	#ENDIF	
	#ENDIF
	
	CLEANUP_AVENGER_AFTER_JOIN_CEO()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("CLEANUP_AVENGER_AFTER_JOIN_CEO")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_MOVING_INTO_DEFUNCT_BASE()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MOVING_INTO_DEFUNCT_BASE")
	#ENDIF	
	#ENDIF
	
	UPDATE_MAIN_AVENGER_MODS()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_MAIN_AVENGER_MODS")
	#ENDIF	
	#ENDIF
	
	UPDATE_AVENGER_I_AM_IN_VAR()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_AVENGER_I_AM_IN_VAR")
	#ENDIF	
	#ENDIF
	
	HANDLE_AVENGER_SOUNDS()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("HANDLE_AVENGER_SOUNDS")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_AVENGER_CAMERA_SHAKE()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_AVENGER_CAMERA_SHAKE")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_FAKE_AVENGER_PED()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_FAKE_AVENGER_PED")
	#ENDIF	
	#ENDIF
		
	UPDATE_AVENGER_REMOTE_VEHICLE_INDEX()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_AVENGER_REMOTE_VEHICLE_INDEX")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_ENTITIES_FOR_ORBITAL_CANNON()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ENTITIES_FOR_ORBITAL_CANNON")
	#ENDIF	
	#ENDIF
	
	UPDATE_HACKER_TRUCK_REMOTE_VEHICLE_INDEX()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_HACKER_TRUCK_REMOTE_VEHICLE_INDEX")
	#ENDIF	
	#ENDIF
	
	PERVENT_PLAYER_FROM_GETTING_STUCK_IN_AVENGER_MODEL()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("PERVENT_PLAYER_FROM_GETTING_STUCK_IN_AVENGER_MODEL")
	#ENDIF	
	#ENDIF	
	
	MAINTAIN_SPAWN_HACKERTRUCK_IN_BUSINESS_HUB()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SPAWN_HACKERTRUCK_IN_BUSINESS_HUB")
	#ENDIF	
	#ENDIF
	
	#IF FEATURE_DLC_2_2022
	
	UPDATE_ACID_LAB_REMOTE_VEHICLE_INDEX()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_ACID_LAB_REMOTE_VEHICLE_INDEX")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_SPAWN_ACIDLAB_IN_JUGGALO_HIDEOUT()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SPAWN_ACIDLAB_IN_JUGGALO_HIDEOUT")
	#ENDIF	
	#ENDIF
	
	CLEANUP_ACIDLAB_AFTER_JOIN_CEO()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("CLEANUP_ACIDLAB_AFTER_JOIN_CEO")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_ASSIGN_ACID_LAB_TO_THIS_SCRIPT()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ASSIGN_ACID_LAB_TO_THIS_SCRIPT")
	#ENDIF	
	#ENDIF
	
	#ENDIF
	
	CLEANUP_HACKERTRUCK_AFTER_JOIN_CEO()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("CLEANUP_HACKERTRUCK_AFTER_JOIN_CEO")
	#ENDIF	
	#ENDIF	
	
	MAINTAIN_ASSIGN_HACKER_TRUCK_TO_THIS_SCRIPT()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ASSIGN_HACKER_TRUCK_TO_THIS_SCRIPT")
	#ENDIF	
	#ENDIF
	
	UPDATE_HACKER_TRUCK_I_AM_IN_VAR()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_HACKER_TRUCK_I_AM_IN_VAR")
	#ENDIF	
	#ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	UPDATE_SUBMARINE_I_AM_IN_VAR()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_SUBMARINE_I_AM_IN_VAR")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_SUBMARINE_SURFACE_REQUEST()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SUBMARINE_SURFACE_REQUEST")
	#ENDIF
	#ENDIF
	
	MAINTAIN_HACKER_TRUCK_COMBO_SCANNER(g_hackerTruckScanner)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_HACKER_TRUCK_COMBO_SCANNER")
	#ENDIF	
	#ENDIF

	UPDATE_MAIN_BLIMP3_MODS()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_MAIN_BLIMP3_MODS")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_PARTY_BUS_SIREN()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PARTY_BUS_SIREN")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_TRASH_MASTER_CRUSHER()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_TRASH_MASTER_CRUSHER")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_FAKE_HACKER_TRUCK_PED()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_FAKE_HACKER_TRUCK_PED")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_HEIST_ISLAND_VEHICLE_RADIO()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_HEIST_ISLAND_VEHICLE_RADIO")
	#ENDIF	
	#ENDIF
	
	CLEAR_KICK_PLAYER_HACKER_TRUCK_FLAG()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("CLEAR_KICK_PLAYER_HACKER_TRUCK_FLAG")
	#ENDIF	
	#ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	CLEAR_KICK_PLAYER_SUBMARINE_FLAG()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("CLEAR_KICK_PLAYER_SUBMARINE_FLAG")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_WATER_CALMING_AROUND_SUBMARINE()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_WATER_CALMING_AROUND_SUBMARINE")
	#ENDIF	
	#ENDIF
	
	CLEAR_KICK_OUT_FOR_RENOVATE_HACKER_TRUCK_FLAG()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("CLEAR_KICK_OUT_FOR_RENOVATE_HACKER_TRUCK_FLAG")
	#ENDIF	
	#ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	MAINTAIN_FAKE_SUBMARINE_PED()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_FAKE_SUBMARINE_PED")
	#ENDIF	
	#ENDIF
	
	SET_ALL_FAKE_PEDS_LOCALLY_INVISIBLE()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("SET_ALL_FAKE_PEDS_LOCALLY_INVISIBLE")
	#ENDIF	
	#ENDIF
	
	CLEAR_EMPTY_SUB_FLAG()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("CLEAR_EMPTY_SUB_FLAG")
	#ENDIF
	#ENDIF
	
	CLEAR_EMPTY_SUB_HELM_FLAG()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("CLEAR_EMPTY_SUB_HELM_FLAG")
	#ENDIF
	#ENDIF
	#ENDIF // FEATURE_HEIST_ISLAND
	
	CLEAR_EMPTY_HACKER_TRUCK_FLAG()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("CLEAR_EMPTY_HACKER_TRUCK_FLAG")
	#ENDIF	
	#ENDIF
	
	CLEANUP_HACKER_TRUCK_AFTER_IT_IS_DESTROYED()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("CLEANUP_HACKER_TRUCK_AFTER_IT_IS_DESTROYED")
	#ENDIF	
	#ENDIF
	
	#IF FEATURE_DLC_2_2022
	UPDATE_ACID_LAB_I_AM_IN_VAR()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_ACID_LAB_I_AM_IN_VAR")
	#ENDIF	
	#ENDIF
	
	CLEAR_KICK_PLAYER_ACID_LAB_FLAG()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("CLEAR_KICK_PLAYER_ACID_LAB_FLAG")
	#ENDIF	
	#ENDIF
	
	CLEAR_EMPTY_ACID_LAB_FLAG()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("CLEAR_EMPTY_ACID_LAB_FLAG")
	#ENDIF	
	#ENDIF
	
	CLEANUP_ACID_LAB_AFTER_IT_IS_DESTROYED()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("CLEANUP_ACID_LAB_AFTER_IT_IS_DESTROYED")
	#ENDIF	
	#ENDIF
	#ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	CLEANUP_SUBMARINE_AFTER_IT_IS_DESTROYED()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("CLEANUP_SUBMARINE_AFTER_IT_IS_DESTROYED")
	#ENDIF	
	#ENDIF
	#ENDIF
	
	MAINTAIN_HACKER_TRUCK_NETWORK_ENTITIES()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_HACKER_TRUCK_NETWORK_ENTITIES")
	#ENDIF	
	#ENDIF
	
	#IF FEATURE_DLC_2_2022
	MAINTAIN_ACID_LAB_NETWORK_ENTITIES()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ACID_LAB_NETWORK_ENTITIES")
	#ENDIF	
	#ENDIF
	#ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	MAINTAIN_SUBMARINE_NETWORK_ENTITIES()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SUBMARINE_NETWORK_ENTITIES")
	#ENDIF	
	#ENDIF
	#ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	MAINTAIN_SUBMARINE_DINGHY_NETWORK_ENTITIES()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SUBMARINE_DINGHY_NETWORK_ENTITIES")
	#ENDIF	
	#ENDIF
	#ENDIF
	
	#IF FEATURE_DLC_2_2022
	MAINTAIN_SUPPORT_BIKE_NETWORK_ENTITIES()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SUPPORT_BIKE_NETWORK_ENTITIES")
	#ENDIF	
	#ENDIF
	#ENDIF
	
	VEHICLE_EXISTENCE_EVENT_EVENTS()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("VEHICLE_EXISTENCE_EVENT_EVENTS")
	#ENDIF	
	#ENDIF
	
	PROCESS_DRONE_COOL_DOWN()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("PROCESS_DRONE_COOL_DOWN")
	#ENDIF	
	#ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	PROCESS_SUBMARINE_GUIDED_MISSILE_COOL_DOWN()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("PROCESS_SUBMARINE_GUIDED_MISSILE_COOL_DOWN")
	#ENDIF	
	#ENDIF
	#ENDIF
	
	PROCESS_DRONE_TARGET_CREATION()
	PROCESS_ENTERING_HACKER_TRUCK_CAB_CHECK()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("PROCESS_ENTERING_HACKER_TRUCK_CAB_CHECK")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_HACKER_TRUCK_AND_SUBMARINE_CAMERA_SHAKE()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_HACKER_TRUCK_AND_SUBMARINE_CAMERA_SHAKE")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_ENTER_HACKER_TRUCK_INTERIOR_FROM_CAB_SEAT()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ENTER_HACKER_TRUCK_INTERIOR_FROM_CAB_SEAT")
	#ENDIF	
	#ENDIF
	
	BROADCAST_LOCAL_ARMORY_VEHICLES_EXIT_SPAWN_POINT()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("BROADCAST_LOCAL_ARMORY_VEHICLES_EXIT_SPAWN_POINT")
	#ENDIF	
	#ENDIF
	
//	MAINTAIN_HACKER_TRUCK_TURRET_POSITIONS()
//	#IF IS_DEBUG_BUILD
//	#IF SCRIPT_PROFILER_ACTIVE
//	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_HACKER_TRUCK_TURRET_POSITIONS")
//	#ENDIF	
//	#ENDIF
	
	MAINTAIN_HACKER_TRUCK_LOCATION()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_HACKER_TRUCK_LOCATION")
	#ENDIF	
	#ENDIF
	
	#IF FEATURE_DLC_2_2022
	PROCESS_ENTERING_ACID_LAB_CAB_CHECK()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("PROCESS_ENTERING_ACID_LAB_CAB_CHECK")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_ACID_LAB_LOCATION()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ACID_LAB_LOCATION")
	#ENDIF	
	#ENDIF
	#ENDIF
	
	MAINTAIN_VEHICLE_NAME_WARNING_HELP()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_VEHICLE_NAME_WARNING_HELP")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_LAUNCHING_VEHICLE_REWARD_SCRIPT()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_LAUNCHING_VEHICLE_REWARD_SCRIPT")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_VEHICLE_CONTROLS_ONCE_PHONE_ON_SCREEN()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_VEHICLE_CONTROLS_ONCE_PHONE_ON_SCREEN")
	#ENDIF	
	#ENDIF
ENDPROC

FUNC BOOL IS_VEHICLE_ANOTHER_PLAYERS_PEGASUS(VEHICLE_INDEX vehIndex)
	IF DECOR_IS_REGISTERED_AS_TYPE("CreatedByPegasus", DECOR_TYPE_BOOL)
	AND DECOR_EXIST_ON(vehIndex, "CreatedByPegasus")
	AND DECOR_GET_BOOL(vehIndex, "CreatedByPegasus")
		NET_PRINT("IS_VEHICLE_ANOTHER_PLAYERS_PEGASUS - this is a pegasus vehicle") NET_NL()
		IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV)
		AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV)
			IF NOT ( NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PegV) = vehIndex)
				NET_PRINT("IS_VEHICLE_ANOTHER_PLAYERS_PEGASUS - TRUE 1") NET_NL()
				RETURN(TRUE)
			ELSE
				NET_PRINT("IS_VEHICLE_ANOTHER_PLAYERS_PEGASUS - this is my pegasus vehicle") NET_NL()
			ENDIF
		ELSE
			NET_PRINT("IS_VEHICLE_ANOTHER_PLAYERS_PEGASUS - TRUE 2") NET_NL()
			RETURN(TRUE)
		ENDIF
	ENDIF

	NET_PRINT("IS_VEHICLE_ANOTHER_PLAYERS_PEGASUS - FALSE") NET_NL()
	RETURN(FALSE)
ENDFUNC



FUNC BOOL IS_VEHICLE_ANOTHER_PLAYERS_TRUCK(VEHICLE_INDEX vehIndex)
	IF DECOR_IS_REGISTERED_AS_TYPE("CreatedByPegasus", DECOR_TYPE_BOOL)
	AND DECOR_EXIST_ON(vehIndex, "CreatedByPegasus")
	AND DECOR_GET_BOOL(vehIndex, "CreatedByPegasus")
		NET_PRINT("IS_VEHICLE_ANOTHER_PLAYERS_TRUCK - this is a pegasus vehicle") NET_NL()
		IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV)
		AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV)
			IF NOT ( NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV) = vehIndex)
				NET_PRINT("IS_VEHICLE_ANOTHER_PLAYERS_TRUCK - TRUE 1") NET_NL()
				RETURN(TRUE)
			ELSE
				NET_PRINT("IS_VEHICLE_ANOTHER_PLAYERS_TRUCK - this is my pegasus vehicle") NET_NL()
			ENDIF
		ELSE
			NET_PRINT("IS_VEHICLE_ANOTHER_PLAYERS_TRUCK - TRUE 2") NET_NL()
			RETURN(TRUE)
		ENDIF
	ENDIF

	NET_PRINT("IS_VEHICLE_ANOTHER_PLAYERS_TRUCK - FALSE") NET_NL()
	RETURN(FALSE)
ENDFUNC

PROC UPDATE_ONE_ON_ONE_DM_VEHICLE_DUPING()
	
	VEHICLE_INDEX VehicleID
	
	IF (g_bFM_ON_VEH_DEATHMATCH)
		IF NOT (bHasDoneDMCheck)
			IF (bLocalPlayerInsideAnotherPlayersPegasus)
				bStartedDMInAnotherPlayersPegasus = TRUE	
				StoredPegVehicleID = INT_TO_NATIVE(VEHICLE_INDEX, -1)
				NET_PRINT("UPDATE_ONE_ON_ONE_DM_VEHICLE_DUPING - player has started a 1on1dm in another players pegasus.") NET_NL()
			ELSE
				NET_PRINT("UPDATE_ONE_ON_ONE_DM_VEHICLE_DUPING - player has NOT started a 1on1dm in another players pegasus.") NET_NL()	
			ENDIF
			bHasDoneDMCheck = TRUE
		ENDIF
	ELSE
		IF (bHasDoneDMCheck)
			// just finished a dm
			IF (bStartedDMInAnotherPlayersPegasus)
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					VEHICLE_INDEX vehIndex
					vehIndex = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF NOT (StoredPegVehicleID = vehIndex)
						CLEANUP_REMOTE_MP_PEG_SAVED_VEHICLE(vehIndex)	
						NET_PRINT("UPDATE_ONE_ON_ONE_DM_VEHICLE_DUPING - cleaning up other players pegasus vehicle - inside it.")
						StoredPegVehicleID = vehIndex
						timeEmptyPegVehicle = GET_NETWORK_TIME()
					ELSE
						IF GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), timeEmptyPegVehicle) > 2000
							NET_PRINT("UPDATE_ONE_ON_ONE_DM_VEHICLE_DUPING - timed out waiting for vehicle to be empty.")
							StoredPegVehicleID = NULL
						ENDIF
					ENDIF
				ELSE
					IF DOES_ENTITY_EXIST(StoredPegVehicleID)
						CLEANUP_REMOTE_MP_PEG_SAVED_VEHICLE(StoredPegVehicleID)
						NET_PRINT("UPDATE_ONE_ON_ONE_DM_VEHICLE_DUPING - cleaning up other players pegasus vehicle - outside it.") NET_NL()	
					ELSE
						bHasDoneDMCheck = FALSE
						bStartedDMInAnotherPlayersPegasus = FALSE
						bLocalPlayerInsideAnotherPlayersPegasus = FALSE	
						IF (StoredPegVehicleID = NULL)
							MPGlobalsAmbience.bPrintPegasusReclaimTicker = TRUE				
						ENDIF
						NET_PRINT("UPDATE_ONE_ON_ONE_DM_VEHICLE_DUPING - clearing flags 2.") NET_NL()
					ENDIF	
				ENDIF			
			ENDIF
		ELSE
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				
				IF (bLocalPlayerInsideAnotherPlayersPegasus)
				
					// if player is accepting or challenging for 1on1 dont do anything
					IF IS_BIT_SET(g_sImpromptuVars.iBitSet, ciIMPROMPTU_ACTIVE_REQUEST_AWAITING_REPLY)
					OR IS_BIT_SET(g_sImpromptuVars.iBitSet, ciIMPROMPTU_REQUEST_ACCEPTED) 
					OR IS_BIT_SET(g_sImpromptuVars.iBitSet, ciBOSSVBOSS_REQUEST_ACCEPTED)
						NET_PRINT("UPDATE_ONE_ON_ONE_DM_VEHICLE_DUPING - starting or requesting 1 on 1 dm.") NET_NL()
					ELSE
				
						// are we not longer in a vehicle
						IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							bLocalPlayerInsideAnotherPlayersPegasus = FALSE
							StoredPegVehicleID = INT_TO_NATIVE(VEHICLE_INDEX, -1)
							NET_PRINT("UPDATE_ONE_ON_ONE_DM_VEHICLE_DUPING - player is no longer in any vehicle.") NET_NL()
						ELSE
							// are we no longer in the same vehicle?
							VehicleID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())						
							IF NOT (StoredPegVehicleID = VehicleID)
								NET_PRINT("UPDATE_ONE_ON_ONE_DM_VEHICLE_DUPING - player is a different vehicle.") NET_NL()
								// this vehicle is not a pegasus?
								IF NOT IS_VEHICLE_ANOTHER_PLAYERS_PEGASUS(VehicleID)
									bLocalPlayerInsideAnotherPlayersPegasus = FALSE
									NET_PRINT("UPDATE_ONE_ON_ONE_DM_VEHICLE_DUPING - new vehicle is not a pegasus vehicle.") NET_NL()
								ELSE
									NET_PRINT("UPDATE_ONE_ON_ONE_DM_VEHICLE_DUPING - new vehicle is a pegasus vehicle.") NET_NL()	
								ENDIF
								StoredPegVehicleID = VehicleID
							ENDIF
						ENDIF
						
					ENDIF
				ELSE
					// are we in a vehicle
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())	
						VehicleID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						// are we in a different vehicle from last time we checked?
						IF NOT (VehicleID = StoredPegVehicleID)
							NET_PRINT("UPDATE_ONE_ON_ONE_DM_VEHICLE_DUPING - player is a different vehicle(2).") NET_NL()
							IF IS_VEHICLE_ANOTHER_PLAYERS_PEGASUS(VehicleID)
								bLocalPlayerInsideAnotherPlayersPegasus = TRUE	
								NET_PRINT("UPDATE_ONE_ON_ONE_DM_VEHICLE_DUPING - player has got into another players pegasus.") NET_NL()	
							ELSE
								NET_PRINT("UPDATE_ONE_ON_ONE_DM_VEHICLE_DUPING - vehicle is not a pegasus vehicle.") NET_NL()		
							ENDIF
							StoredPegVehicleID = VehicleID
						ENDIF
					ENDIF
				ENDIF

			ENDIF
		ENDIF
	ENDIF
	
ENDPROC


/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MISSION PROCS                  //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////


FUNC BOOL SHOULD_BYPASS_CREATED_BY_THIS_SCRIPT_CHECK(VEHICLE_INDEX viTargetVehicle)
	IF DECOR_IS_REGISTERED_AS_TYPE("EnableVehLuxeActs", DECOR_TYPE_INT)
	AND DECOR_EXIST_ON(viTargetVehicle,"EnableVehLuxeActs")
	OR IS_PLAYER_IN_LUX_HELI_VEHICLE(GET_ENTITY_MODEL(viTargetVehicle))
	OR IS_PLAYER_IN_LUX_JET_VEHICLE(GET_ENTITY_MODEL(viTargetVehicle))
	AND NOT NETWORK_IS_ACTIVITY_SESSION()
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Register interest in getting a luxe instance ID. When the server acknowledges
///    this will return an ID. Relies on SERVER_CHECK_LUXE_VEH_ID_REQUESTS succeeding.
/// RETURNS:
///    INT - New ID when successful, -1 otherwise.
FUNC INT CLIENT_REQUEST_LUXE_INSTANCE_ID(VEHICLE_INDEX viTargetVehicle)

	// First trigger the handshake. The client needs to pass the details of the vehicle that they're getting into
	// to the server, so that the server can calculate an instance ID based on those details.
	IF playerBD[NATIVE_TO_INT(PLAYER_ID())].playerWantsLuxeVehID = FALSE
		IF NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(viTargetVehicle)
		OR SHOULD_BYPASS_CREATED_BY_THIS_SCRIPT_CHECK(viTargetVehicle)
			
			INT niTemp = NETWORK_ENTITY_GET_OBJECT_ID(viTargetVehicle)
			//NETWORK_INDEX niTemp = NETWORK_GET_NETWORK_ID_FROM_ENTITY(viTargetVehicle)
			playerBD[NATIVE_TO_INT(PLAYER_ID())].playerWantsLuxeVehID = TRUE	
			playerBD[NATIVE_TO_INT(PLAYER_ID())].objIDRequestingVehicle = niTemp
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "[HS]CLIENT_REQUEST_LUXE_INSTANCE_ID: Initialising instance ID request. Loc idx: ", NATIVE_TO_INT(viTargetVehicle), ", network idx: ", niTemp)
		ENDIF
	ENDIF
	
	// When the server acknowledges that the request is valid, it will set the target of the instance ID data to the
	// original requestor from above.
	IF serverBD.LuxeVehIDPlayerRequest = PLAYER_ID()
	AND ServerBD.luxeVehInstanceID >= 0
	AND ServerBD.luxeVehInstanceID < NUM_NETWORK_PLAYERS
		IF ServerBD.objIDStoredVehicles[ServerBD.luxeVehInstanceID] != -1
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "[HS]CLIENT_REQUEST_LUXE_INSTANCE_ID: Collecting available luxeVehInstanceID: ", ServerBD.luxeVehInstanceID)
			playerBD[NATIVE_TO_INT(PLAYER_ID())].playerHasTakenLuxeVehID = TRUE
			playerBD[NATIVE_TO_INT(PLAYER_ID())].playerWantsLuxeVehID = FALSE
			RETURN ServerBD.luxeVehInstanceID
		ELSE
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "[HS]CLIENT_REQUEST_LUXE_INSTANCE_ID: niStoredVehicles has invalid entry for instance ID, value: ", ServerBD.objIDStoredVehicles[ServerBD.luxeVehInstanceID], ", instanceID: ", ServerBD.luxeVehInstanceID)
		ENDIF
	ELSE
		IF ServerBD.LuxeVehIDPlayerRequest = INT_TO_NATIVE(PLAYER_INDEX, -1)
			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "[HS]CLIENT_REQUEST_LUXE_INSTANCE_ID: Server has not picked anyone yet")
		ELIF (ServerBD.luxeVehInstanceID < 0 OR ServerBD.luxeVehInstanceID >= NUM_NETWORK_PLAYERS)
			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "[HS]CLIENT_REQUEST_LUXE_INSTANCE_ID: luxeVehInstanceID is invalid, value: ", ServerBD.luxeVehInstanceID)
		ELSE
			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "[HS]CLIENT_REQUEST_LUXE_INSTANCE_ID: Unknown failure. Instance ID: ", ServerBD.luxeVehInstanceID, ", serverBD.LuxeVehIDPlayerRequest: ", GET_PLAYER_NAME(serverBD.LuxeVehIDPlayerRequest))
		ENDIF
	ENDIF
	
	RETURN -1
ENDFUNC


/// PURPOSE:
///    Monitor luxe instance ID requests. When a client has a request active, take the network ID from them, and store it
///    in the serverBD array of luxe vehicles. The index of this vehicle in the serverBD array is the instance ID of the script
///    associated with that vehicle.
PROC SERVER_CHECK_LUXE_INSTANCE_REQUESTS()

	INT iPlayerCount, index
	BOOL bBypassSave

	IF ServerBD.LuxeVehIDPlayerRequest = INT_TO_NATIVE(PLAYER_INDEX, -1)
	
		REPEAT NUM_NETWORK_PLAYERS iPlayerCount
			IF playerBD[iPlayerCount].playerWantsLuxeVehID = TRUE
			
				IF playerBD[iPlayerCount].objIDRequestingVehicle != -1
				
					ServerBD.LuxeVehIDPlayerRequest = INT_TO_PLAYERINDEX(iPlayerCount)
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "[HS]SERVER_CHECK_LUXE_INSTANCE_REQUESTS: Processing request for player: [", GET_PLAYER_NAME(ServerBD.LuxeVehIDPlayerRequest), ", ", NATIVE_TO_INT(ServerBD.LuxeVehIDPlayerRequest), "].")
					
					// Check if the client's vehicle already exists before adding it anew.
					FOR index = 0 TO (NUM_NETWORK_PLAYERS-1)
						IF ServerBD.objIDStoredVehicles[index] = playerBD[iPlayerCount].objIDRequestingVehicle
							ServerBD.luxeVehInstanceID = index
							CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "[HS]SERVER_CHECK_LUXE_INSTANCE_REQUESTS: Requested vehicle: ",playerBD[iPlayerCount].objIDRequestingVehicle," already has instance ID: ", index, ", setting bBypassSave to TRUE.")
							index = (NUM_NETWORK_PLAYERS-1)
							bBypassSave = TRUE
						ENDIF
					ENDFOR
		
					IF NOT bBypassSave
						// vehicle does not already exist, find space for it in the serverBD vehicle array and store the index.
						FOR index = 0 TO (NUM_NETWORK_PLAYERS-1)
							IF ServerBD.objIDStoredVehicles[index] = -1
								ServerBD.objIDStoredVehicles[index] = playerBD[iPlayerCount].objIDRequestingVehicle
								ServerBD.luxeVehInstanceID = index
								CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "[HS]SERVER_CHECK_LUXE_INSTANCE_REQUESTS: Saving requested vehicle: ",playerBD[iPlayerCount].objIDRequestingVehicle,", in server array slot (instance ID): ", index)
								index = (NUM_NETWORK_PLAYERS-1)
							ENDIF
						ENDFOR
					ENDIF

					iPlayerCount = NUM_NETWORK_PLAYERS
					bBypassSave = FALSE
					
				ELSE
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "[HS]SERVER_CHECK_LUXE_INSTANCE_REQUESTS: Invalid requesting vehicle index, value: ", playerBD[iPlayerCount].objIDRequestingVehicle)
				ENDIF	
				
			ENDIF
		ENDREPEAT
		
	ELSE
		// TODO: Look into making this more robust.
	
		IF playerBD[NATIVE_TO_INT(ServerBD.LuxeVehIDPlayerRequest)].playerHasTakenLuxeVehID = TRUE
		AND playerBD[NATIVE_TO_INT(ServerBD.LuxeVehIDPlayerRequest)].playerWantsLuxeVehID = FALSE
			
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "[HS]SERVER_CHECK_LUXE_INSTANCE_REQUESTS: Resetting saved instance request target (LuxeVehIDPlayerRequest).")
			ServerBD.LuxeVehIDPlayerRequest = INT_TO_NATIVE(PLAYER_INDEX, -1)
		
		ELIF playerBD[NATIVE_TO_INT(ServerBD.LuxeVehIDPlayerRequest)].playerHasTakenLuxeVehID = FALSE
		AND playerBD[NATIVE_TO_INT(ServerBD.LuxeVehIDPlayerRequest)].playerWantsLuxeVehID = FALSE
			
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "[HS]SERVER_CHECK_LUXE_INSTANCE_REQUESTS: Timing error occured: playerHasTakenLuxeVehID AND playerWantsLuxeVehID are both FALSE, resetting server state.")
			ServerBD.LuxeVehIDPlayerRequest = INT_TO_NATIVE(PLAYER_INDEX, -1)
		
		ELIF NATIVE_TO_INT(ServerBD.LuxeVehIDPlayerRequest) >= 0
		
			IF NOT IS_NET_PLAYER_OK(ServerBD.LuxeVehIDPlayerRequest, FALSE)
				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "[HS]SERVER_CHECK_LUXE_INSTANCE_REQUESTS: Not IS_NET_PLAYER_OK, clean up requesting player (LuxeVehIDPlayerRequest).")
				ServerBD.LuxeVehIDPlayerRequest = INT_TO_NATIVE(PLAYER_INDEX, -1)
			ENDIF
			
		ENDIF

	ENDIF

ENDPROC


/// PURPOSE:
///    Monitor active luxe script instances. If an instance is orphaned from its parent script, this method will attempt to find
///    the parent script again 3 times (3 strikes). Once 3 strikes have been reached, the instance is terminated and returned
///    to the instance pool for re-use.
PROC SERVER_MONITOR_LUXE_VEHICLE_INSTANCES()
	
	IF NOT HAS_NET_TIMER_STARTED(timeLuxeInstanceMonitor)
		START_NET_TIMER(timeLuxeInstanceMonitor)
	ENDIF
	
	IF HAS_NET_TIMER_EXPIRED(timeLuxeInstanceMonitor, ciLUXE_CLEANUP_TIME_OUT)
		
		INT index, subIndex
		BOOL bBypassCleanup
		
		FOR index = 0 TO (NUM_NETWORK_PLAYERS-1)
		
			IF ServerBD.objIDStoredVehicles[index] != -1

				IF NOT NETWORK_IS_SCRIPT_ACTIVE("luxe_veh_activity", index, FALSE)
				
					FOR subIndex = 0 TO (ciLUXE_INSTANCE_RECYCLE_ATTEMPTS-1)
						IF NOT bBypassCleanup
							IF NOT IS_BIT_SET(iLuxeInstanceCheckBS[index], subIndex)
								CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "[HS]SERVER_MONITOR_LUXE_VEHICLE_INSTANCES: Instance ID: ",index,", is on inactivity strike #: ", subIndex, ", inactivity bitset: ", iLuxeInstanceCheckBS[index])
								SET_BIT(iLuxeInstanceCheckBS[index], subIndex)
								bBypassCleanup = TRUE
							ENDIF
						ENDIF
					ENDFOR
					
					IF NOT bBypassCleanup
						CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "[HS]SERVER_MONITOR_LUXE_VEHICLE_INSTANCES: Instance ID: ",index,", has reached 3 strikes, recycling ID.")
						ServerBD.objIDStoredVehicles[index] = -1
						
						FOR subIndex = 0 TO (ciLUXE_INSTANCE_RECYCLE_ATTEMPTS-1)
							CLEAR_BIT(iLuxeInstanceCheckBS[index], subIndex)
						ENDFOR
					ENDIF
					
					bBypassCleanup = FALSE
					
				ELSE
					CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "[HS]SERVER_MONITOR_LUXE_VEHICLE_INSTANCES: Server vehicle index: ",index,", is marked as active and script is running.")
					iLuxeInstanceCheckBS[index] = 0 // Reset the number of strikes.
				ENDIF
			
			ENDIF
			
		ENDFOR
		
		RESET_NET_TIMER(timeLuxeInstanceMonitor)
		
	ENDIF

ENDPROC


/// PURPOSE:
///    Clear the client data that is used during the init and load for luxe scripts.
PROC CLIENT_CLEAN_LOCAL_LUXE_DATA()
	luxeActState.vehModel = INT_TO_ENUM(MODEL_NAMES, -1)
	playerBD[NATIVE_TO_INT(PLAYER_ID())].playerHasTakenLuxeVehID = FALSE
	playerBD[NATIVE_TO_INT(PLAYER_ID())].playerWantsLuxeVehID = FALSE
	playerBD[NATIVE_TO_INT(PLAYER_ID())].objIDRequestingVehicle = -1
	luxeActState.iLuxeVehInstanceID = -1
	luxeActState.iParticipantAttempts = 0
	CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "[HS]CLIENT_CLEAN_LOCAL_LUXE_DATA: Cleaned local client luxe data.")
ENDPROC



/// PURPOSE:
///    Maintain server reservation info
PROC SERVER_MAINTAIN_RESERVATIONS()
	
ENDPROC

/// PURPOSE:
///    Maintain client reservation info
PROC CLIENT_MAINTAIN_RESERVATIONS()
	
ENDPROC



/// PURPOSE:
///    Returns if the vehicle spawn script should terminate immediately
/// RETURNS:
///    true/false
FUNC BOOL SHOULD_VEHICLE_SPAWN_TERMINATE()
	
	//PRINTLN("=== VEH SPAWN CLIENT ===  SHOULD_VEHICLE_SPAWN_TERMINATE called...")
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
	OR DID_I_JOIN_MISSION_AS_SPECTATOR()
		PRINTLN("=== VEH SPAWN CLIENT ===  Player is SCTV or joined mission as spectator.")
		RETURN TRUE
	ENDIF
	
	IF NOT (CAN_PLAYER_USE_REQUEST_DURING_CURRENT_AMBIENT(REQUEST_LESTER_LOCATE_BOAT)	//If we cannot do anything with vehicle spawn
			AND CAN_PLAYER_USE_REQUEST_DURING_CURRENT_JOB(REQUEST_LESTER_LOCATE_BOAT))
	AND NOT (CAN_PLAYER_USE_REQUEST_DURING_CURRENT_AMBIENT(REQUEST_PEGASUS)
			AND CAN_PLAYER_USE_REQUEST_DURING_CURRENT_JOB(REQUEST_PEGASUS))
		PRINTLN("=== VEH SPAWN CLIENT ===  Nothing we can do with this script.")
		RETURN TRUE
	ENDIF
	
	IF NOT HAS_PLAYER_COMPLETED_RACE_AND_DM_TUTORIAL(PLAYER_ID())
	//OR NETWORK_IS_IN_TUTORIAL_SESSION() - don't want the script to end if in tutorial session due to duping see 1787796
	OR IS_LOCAL_PLAYER_IN_TUTORIAL_SESSION_FOR_INTRO()
		PRINTLN("=== VEH SPAWN CLIENT ===  Tutorial session or race not completed.")
		RETURN TRUE
	ENDIF
	
	IF (MPGlobalsAmbience.bCleanupPegasusVehicle)
		IF IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_BLIP_CREATED_FOR_SPAWN_POINT)
		OR IS_BIT_SET(bookedVehicle.iBitset, BOOK_BS_DELIVERY_COUNTDOWN_SET)
			PRINTLN("=== VEH SPAWN CLIENT ===  MPGlobalsAmbience.bCleanupPegasusVehicle = TRUE, but BOOK_BS_BLIP_CREATED_FOR_SPAWN_POINT or BOOK_BS_DELIVERY_COUNTDOWN_SET is set.")
			MPGlobalsAmbience.bCleanupPegasusVehicle = FALSE
			MPGlobalsAmbience.bForceDeletePegasusVehicle = FALSE
		ELSE

			PRINTLN("=== VEH SPAWN CLIENT ===  MPGlobalsAmbience.bCleanupPegasusVehicle = TRUE")
			RETURN(TRUE)
		ENDIF	
	ENDIF

	RETURN FALSE
ENDFUNC

BOOL bDoLaunchedScriptCheck



FUNC BOOL ManageStuckCheckForTrailer(VEHICLE_INDEX VehicleID)
	INT iPlayerInt
	INT iCount = 0
	VEHICLE_INDEX TrailerID
	
	IF DOES_ENTITY_EXIST(VehicleID)
		IF NOT IS_ENTITY_DEAD(VehicleID)
		
			// does it already have a stuck check? if so, should we remove?
			IF DOES_VEHICLE_HAVE_STUCK_VEHICLE_CHECK(VehicleID)
				
				// if more than 200m away remove
				IF NOT (VDIST2(GET_ENTITY_COORDS(VehicleID), GET_PLAYER_COORDS(PLAYER_ID())) > 40000.0)
					REMOVE_VEHICLE_STUCK_CHECK(VehicleID)
					PRINTLN("[MOC_STUCK] ManageStuckCheckForTrailer - removing stuck check, more than 200m away. ")
				ENDIF
			
			ELSE
			
				// should we add a stuck check?
				IF NETWORK_HAS_CONTROL_OF_ENTITY(VehicleID)
					IF (VDIST2(GET_ENTITY_COORDS(VehicleID), GET_PLAYER_COORDS(PLAYER_ID())) < 40000.0)
					
						PRINTLN("[MOC_STUCK] ManageStuckCheckForTrailer - try to add, near enough... ")
					
						//  count number of existing stuck checks
						REPEAT NUM_NETWORK_PLAYERS iPlayerInt
							IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iPlayerInt].netID_TruckV[1])
								TrailerID = NET_TO_VEH(GlobalplayerBD[iPlayerInt].netID_TruckV[1])
								IF DOES_ENTITY_EXIST(TrailerID)
									IF NOT IS_ENTITY_DEAD(TrailerID)
										IF DOES_VEHICLE_HAVE_STUCK_VEHICLE_CHECK(TrailerID)
											iCount++
										ENDIF
									ENDIF			
								ENDIF
							ENDIF
						ENDREPEAT
						
						PRINTLN("[MOC_STUCK] ManageStuckCheckForTrailer - count = ", iCount)
						
						// can we add?
						IF (iCount < 14)
							IF DOES_ENTITY_EXIST(VehicleID)
								IF NOT IS_ENTITY_DEAD(VehicleID)
									ADD_VEHICLE_STUCK_CHECK_WITH_WARP(VehicleID, 2.0, 500, FALSE, FALSE, FALSE, -1) 
									iCount++
									PRINTLN("[MOC_STUCK] ManageStuckCheckForTrailer - Adding. iCount = ", iCount)
									RETURN TRUE
								ENDIF
							ENDIF
						ELSE
							PRINTLN("[MOC_STUCK] ManageStuckCheckForTrailer - cant add, count = ", iCount)
						ENDIF
					
					ELSE
						// too far away to add
						//PRINTLN("[MOC_STUCK] ManageStuckCheckForTrailer - too far away. ")
					ENDIF	
				ELSE
					//PRINTLN("[MOC_STUCK] ManageStuckCheckForTrailer - not in network control. ")
				ENDIF
				
			ENDIF
			
		ENDIF			
	ENDIF	

	RETURN FALSE
	
	
ENDFUNC


FUNC BOOL IsMobileControlCentreStuck(INT iPlayerInt)

	VEHICLE_INDEX TrailerID
	ENTITY_INDEX eiAttached
	VEHICLE_INDEX viAttachedVehicle
	
	IF NOT IS_PLAYER_IN_BUNKER(PLAYER_ID())
		IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iPlayerInt].netID_TruckV[1])
			TrailerID = NET_TO_VEH(GlobalplayerBD[iPlayerInt].netID_TruckV[1])
			IF NOT IS_ENTITY_DEAD(TrailerID)
			
				// dont allow cargobob to grab
				IF NETWORK_HAS_CONTROL_OF_ENTITY(TrailerID)
				AND IS_ENTITY_ATTACHED(TrailerID)
					eiAttached = GET_ENTITY_ATTACHED_TO(TrailerID)
					IF IS_ENTITY_A_VEHICLE(eiAttached)
						viAttachedVehicle = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(eiAttached)
						IF IS_ENTITY_CARGOBOB(viAttachedVehicle)
							PRINTLN("[MOC_STUCK]  IsMobileControlCentreStuck - detaching from cargobob.")
							DETACH_VEHICLE_FROM_CARGOBOB(viAttachedVehicle, TrailerID)
						ENDIF
					ENDIF
				ENDIF			
			
//				IF IS_ENTITY_SUBMERGED_IN_WATER(TrailerID, FALSE, 0.25)
//					PRINTLN("[MOC_STUCK] IsMobileControlCentreStuck - true, in water, iPlayerInt ", iPlayerInt)
//					RETURN TRUE
//				ENDIF
				
				IF DOES_VEHICLE_HAVE_STUCK_VEHICLE_CHECK(TrailerID)
					
					IF IS_VEHICLE_STUCK_TIMER_UP(TrailerID, VEH_STUCK_ON_ROOF, 5000)
						PRINTLN("[MOC_STUCK]  IsMobileControlCentreStuck - true, stuck on roof, iPlayerInt ", iPlayerInt)
						RETURN TRUE
					ENDIF
				
					IF IS_VEHICLE_STUCK_TIMER_UP(TrailerID, VEH_STUCK_ON_SIDE, 5000)
						PRINTLN("[MOC_STUCK]  IsMobileControlCentreStuck - true, stuck on side, iPlayerInt ", iPlayerInt)
						RETURN TRUE
					ENDIF

					IF IS_VEHICLE_STUCK_TIMER_UP(TrailerID, VEH_STUCK_HUNG_UP, 5000)
						PRINTLN("[MOC_STUCK]  IsMobileControlCentreStuck - true, hung up, iPlayerInt ", iPlayerInt)
						RETURN TRUE
					ENDIF
					
					IF IS_VEHICLE_STUCK_TIMER_UP(TrailerID, VEH_STUCK_JAMMED, 5000)
						PRINTLN("[MOC_STUCK]  IsMobileControlCentreStuck - true, jammed, iPlayerInt ", iPlayerInt)
						RETURN TRUE
					ENDIF
				
				ENDIF
					
				//PRINTLN("[MOC_STUCK]  IsMobileControlCentreStuck - ManageStuckCheckForTrailer - iPlayerInt ", iPlayerInt)	
				IF ManageStuckCheckForTrailer(TrailerID)
					PRINTLN("[MOC_STUCK]  IsMobileControlCentreStuck - ManageStuckCheckForTrailer - added for iPlayerInt ", iPlayerInt)	
				ENDIF
					
			ELSE
				
				PRINTLN("[MOC_STUCK]  IsMobileControlCentreStuck - trailer is dead. ", iPlayerInt)
		
			ENDIF
		ELSE
			//PRINTLN("[MOC_STUCK]  IsMobileControlCentreStuck - net trailer does not exist.  ", iPlayerInt)	
		ENDIF
	ENDIF

	VEHICLE_INDEX TruckID
	IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iPlayerInt].netID_TruckV[0])
		TruckID = NET_TO_VEH(GlobalplayerBD[iPlayerInt].netID_TruckV[0])
		IF DOES_ENTITY_EXIST(TruckID)
		AND IS_ENTITY_A_VEHICLE(TruckID)
		AND IS_VEHICLE_DRIVEABLE(TruckID)
		AND IS_VEHICLE_ATTACHED_TO_TRAILER(TruckID)
		
			// dont allow cargobob to grab
			IF NETWORK_HAS_CONTROL_OF_ENTITY(TruckID)
			AND IS_ENTITY_ATTACHED(TruckID)
			AND IS_VEHICLE_ATTACHED_TO_TRAILER(TruckID)
				eiAttached = GET_ENTITY_ATTACHED_TO(TruckID)
				IF IS_ENTITY_A_VEHICLE(eiAttached)
					viAttachedVehicle = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(eiAttached)
					IF IS_ENTITY_CARGOBOB(viAttachedVehicle)
						PRINTLN("[MOC_STUCK]  UPDATE_MOBILE_CONTROL_CENTRE_STUCK_CHECKS - detaching truck from cargobob.")
						DETACH_VEHICLE_FROM_CARGOBOB(viAttachedVehicle, TruckID)
					ENDIF
				ENDIF
			ENDIF
		
//			IF IS_ENTITY_IN_DEEP_WATER(TruckID)
//				PRINTLN("[MOC_STUCK]  IsMobileControlCentreStuck - true, truck in deep water")
//				RETURN TRUE
//			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF (bForceMobileIsStuck)
			RETURN TRUE
		ENDIF
	#ENDIF
	
	
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_PLAYER_INSIDE_MY_ARMOUR_TRUCK(PLAYER_INDEX PlayerID)
	IF IS_PLAYER_IN_ARMORY_TRUCK(PlayerID)		
		IF (GET_OWNER_OF_ARMORY_TRUCK_THAT_PLAYER_IS_INSIDE(PlayerID) = PLAYER_ID())	
			PRINTLN("[MOC_STUCK] IS_PLAYER_INSIDE_MY_ARMOUR_TRUCK - player ", NATIVE_TO_INT(PlayerID), " is inside my truck")
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANYONE_INSIDE_MY_TRAILER()
	INT i
	PLAYER_INDEX PlayerID
	REPEAT NUM_NETWORK_PLAYERS i
		PlayerID = INT_TO_NATIVE(PLAYER_INDEX, i)
		IF IS_PLAYER_INSIDE_MY_ARMOUR_TRUCK(PlayerID)
			PRINTLN("[MOC_STUCK] IS_ANYONE_INSIDE_MY_TRAILER - player ", NATIVE_TO_INT(PlayerID), " is inside my truck")
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC


BOOL bShouldWarpTruck
INT iTrailerWarpState
INT iTrailerQuickWarpState
INT iHackerTruckQuickWarpState
TIME_DATATYPE timeTruckWarp
VECTOR vTruckWarpCoords
FLOAT fTruckWarpHeading
INT iStuckStagger
TIME_DATATYPE timeTruckLeaveEvent

PROC RequestControlOfMOC()
	VEHICLE_INDEX TruckID
	IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])
		TruckID = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])
	ENDIF
	VEHICLE_INDEX TrailerID
	IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
		TrailerID = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
	ENDIF	
	
	// request control
	IF DOES_ENTITY_EXIST(TruckID)
	AND NOT NETWORK_HAS_CONTROL_OF_ENTITY(TruckID)
		NETWORK_REQUEST_CONTROL_OF_ENTITY(TruckID)
	ENDIF
	IF DOES_ENTITY_EXIST(TrailerID)
	AND NOT NETWORK_HAS_CONTROL_OF_ENTITY(TrailerID)
		NETWORK_REQUEST_CONTROL_OF_ENTITY(TrailerID)
	ENDIF	
ENDPROC

PROC KeepEveryoneOutMyTruck()
	
	VEHICLE_INDEX TruckID
	IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])
		TruckID = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])
	ENDIF
	
	IF (ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), timeTruckLeaveEvent)) > 1500)
	
		// kick out trailer
		IF IS_ANYONE_INSIDE_MY_TRAILER()
			IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS,  BS_SIMPLE_INTERIOR_GLOBAL_DATA_KICK_PLAYERS_OUT_OF_ARMORY_TRUCK_INTERIOR) 	
				PRINTLN("[MOC_STUCK]  KeepEveryoneOutMyTruck - someone is inside truck")
				SET_KICK_OUT_PLAYER_FROM_ARMORY_TRUCK(TRUE)	
			ENDIF
		ENDIF
		
		IF MPGlobalsAmbience.bMobileCommandCentreShouldLeave = FALSE
			MPGlobalsAmbience.bMobileCommandCentreShouldLeave = TRUE
			PRINTLN("[MOC_STUCK]  KeepEveryoneOutMyTruck - MPGlobalsAmbience.bMobileCommandCentreShouldLeave = TRUE")
		ENDIF
		
		// kick out truck - if attached.
		IF DOES_ENTITY_EXIST(TruckID)
		AND IS_VEHICLE_DRIVEABLE(TruckID)
		AND IS_VEHICLE_ATTACHED_TO_TRAILER(TruckID)
			PLAYER_INDEX PlayerID
			INT i
			REPEAT NUM_NETWORK_PLAYERS i
				PlayerID = INT_TO_PLAYERINDEX(i)
				IF IS_NET_PLAYER_OK(PlayerID, FALSE)
					IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(PlayerID), TruckID)
						BROADCAST_LEAVE_VEHICLE(SPECIFIC_PLAYER(PlayerID), TRUE, 0, 0, FALSE, FALSE)
						timeTruckLeaveEvent = GET_NETWORK_TIME()
					ENDIF
				ENDIF
			ENDREPEAT						
			
		ENDIF
		
		// keep cab locked
		IF DOES_ENTITY_EXIST(TruckID)
		AND NETWORK_HAS_CONTROL_OF_ENTITY(TruckID)
		AND IS_VEHICLE_DRIVEABLE(TruckID)
			SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(TruckID, TRUE)	
		ENDIF
		
	ELSE
		PRINTLN("[MOC_STUCK]  KeepEveryoneOutMyTruck - waiting for timeTruckLeaveEvent to expire, time: ", ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), timeTruckLeaveEvent)))
	ENDIF
ENDPROC

FUNC BOOL IS_ANY_PLAYER_TOUCHING_VEHICLE(VEHICLE_INDEX VehicleID)
	INT iPlayer
	PLAYER_INDEX PlayerID
	PED_INDEX PedID
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		PlayerID = INT_TO_NATIVE(PLAYER_INDEX, iPlayer)
		IF IS_NET_PLAYER_OK(PlayerID, FALSE)
			PedID = GET_PLAYER_PED(PlayerID)
			IF DOES_ENTITY_EXIST(PedID)
				IF IS_ENTITY_TOUCHING_ENTITY(PedID, VehicleID)
					PRINTLN("[MOC_STUCK]  IS_ANY_PLAYER_TOUCHING_VEHICLE - iPlayer = ", iPlayer)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

PROC UPDATE_MOBILE_CONTROL_CENTRE_STUCK_CHECKS()

	INT iDiff
	INT i

	VEHICLE_INDEX TruckID
	IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])
		TruckID = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])
	ENDIF
	VEHICLE_INDEX TrailerID
	IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
		TrailerID = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
	ENDIF


	IF IsMobileControlCentreStuck(iStuckStagger)
		PRINTLN("[MOC_STUCK]  UPDATE_MOBILE_CONTROL_CENTRE_STUCK_CHECKS - detected stuck mobile command center. iStuckStagger ", iStuckStagger)		
		BROADCAST_GENERAL_EVENT(GENERAL_EVENT_MOBILE_COMMAND_CENTRE_STUCK, SPECIFIC_PLAYER(INT_TO_NATIVE(PLAYER_INDEX, iStuckStagger)))
	ENDIF
	iStuckStagger++	
	IF (iStuckStagger >= NUM_NETWORK_PLAYERS)
		iStuckStagger = 0
	ENDIF
	
	IF NOT IS_PLAYER_IN_BUNKER(PLAYER_ID())
	
		//PRINTLN("[MOC_STUCK]  UPDATE_MOBILE_CONTROL_CENTRE_STUCK_CHECKS - vehicle is driveable.")

		// is stuck?
		IF (iTrailerWarpState = 0)
			IF (MPGlobalsAmbience.bMobileCommandCentreIsStuck)						
				PRINTLN("[MOC_STUCK]  UPDATE_MOBILE_CONTROL_CENTRE_STUCK_CHECKS - my trailer is stuck")
				timeTruckWarp = GET_NETWORK_TIME_ACCURATE()
				timeTruckLeaveEvent = GET_NETWORK_TIME_ACCURATE()
				
				RequestControlOfMOC()
				
				// broadcast event to print help to all players who were inside truck or cab
				
				PLAYER_INDEX PlayerID
				INT iPlayerFlags
				REPEAT NUM_NETWORK_PLAYERS i
					PlayerID = INT_TO_PLAYERINDEX(i)
					IF IS_NET_PLAYER_OK(PlayerID, FALSE)
						IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(PlayerID), TruckID)
						OR IS_PLAYER_INSIDE_MY_ARMOUR_TRUCK(PlayerID)
							SET_BIT( iPlayerFlags , i)
						ENDIF
					ENDIF
				ENDREPEAT
				
				BROADCAST_GENERAL_EVENT(GENERAL_EVENT_MOVING_COMMAND_CENTRE, iPlayerFlags)
				
				iTrailerWarpState++
			ENDIF
		ENDIF
	

		// wait for everyone to be out
		IF (iTrailerWarpState = 1)
			
			RequestControlOfMOC()
			KeepEveryoneOutMyTruck()
			
			IF NOT IS_ANYONE_INSIDE_MY_TRAILER()

				iDiff = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), timeTruckWarp))
				PRINTLN("[MOC_STUCK]  UPDATE_MOBILE_CONTROL_CENTRE_STUCK_CHECKS - waiting for everyone to be kicked out. iDiff = ", iDiff)
				IF (iDiff > 10000)
				
					IF IS_ANY_PLAYER_TOUCHING_VEHICLE(TrailerID)
						PRINTLN("[MOC_STUCK]  UPDATE_MOBILE_CONTROL_CENTRE_STUCK_CHECKS - IS_ANY_PLAYER_TOUCHING_VEHICLE = TRUE")
					ELSE
					
						vTruckWarpCoords = GET_ENTITY_COORDS(TrailerID, FALSE)
						PRINTLN("[MOC_STUCK]  UPDATE_MOBILE_CONTROL_CENTRE_STUCK_CHECKS - vTruckWarpCoords = ", vTruckWarpCoords)
											
						IF IS_VEHICLE_DRIVEABLE(TruckID)
							IF IS_VEHICLE_ATTACHED_TO_TRAILER(TruckID)
								bShouldWarpTruck = TRUE
								iTrailerQuickWarpState = 0
								PRINTLN("[MOC_STUCK]  UPDATE_MOBILE_CONTROL_CENTRE_STUCK_CHECKS - setting bShouldWarpTruck")
							ENDIF
						ENDIF		
						
						iTrailerWarpState++
					
					ENDIF
					
				ELSE
					
					// display help
					
				
				ENDIF
			ENDIF
		ENDIF
		
		// get safe spawn position
		IF (iTrailerWarpState = 2)
		
			RequestControlOfMOC()
			KeepEveryoneOutMyTruck()
		
			VEHICLE_SPAWN_LOCATION_PARAMS Params
			Params.fMinDistFromCoords = 20.0
			Params.fMaxDistance = 250.0
			Params.bConsiderHighways = TRUE
			Params.bCheckEntityArea = TRUE
			Params.bCheckOwnVisibility = FALSE
			Params.bIsForPV = TRUE
			Params.bIgnoreCustomNodesForArea = TRUE	
			
			#IF IS_DEBUG_BUILD
			IF NOT IS_ENTITY_DEAD(TrailerID)
				IF IS_ENTITY_ATTACHED(TrailerID)
					PRINTLN("[MOC_STUCK]  UPDATE_MOBILE_CONTROL_CENTRE_STUCK_CHECKS - trailer is attached")
				ENDIF
			ENDIF
			#ENDIF
			
			IF NOT (bShouldWarpTruck)
				IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(vTruckWarpCoords, <<0.0, 0.0, 0.0>>, TRAILERLARGE, FALSE, vTruckWarpCoords, fTruckWarpHeading, Params)
					vTruckWarpCoords.z += 2.0						
					iTrailerWarpState++
					PRINTLN("[MOC_STUCK]  UPDATE_MOBILE_CONTROL_CENTRE_STUCK_CHECKS - warping trailer only to ", vTruckWarpCoords, ", ", fTruckWarpHeading)
				ENDIF
			ELSE
				IF IS_VEHICLE_DRIVEABLE(TruckID)
				AND IS_VEHICLE_ATTACHED_TO_TRAILER(TruckID)	
					
					ADD_CUSTOM_NODES_FOR_ALL_TRUCK_SPAWNS()
					Params.fMaxDistance= 9999.0	
				
					IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(vTruckWarpCoords, <<0.0, 0.0, 0.0>>, COMBINED_MODEL_ID(CM_AMROUR_TRUCK_PHANTOM), FALSE, vTruckWarpCoords, fTruckWarpHeading, Params)
						
						CLEAR_CUSTOM_VEHICLE_NODES()
						
						iTrailerWarpState++
						PRINTLN("[MOC_STUCK]  UPDATE_MOBILE_CONTROL_CENTRE_STUCK_CHECKS - warping truck and trailer to ", vTruckWarpCoords, ", ", fTruckWarpHeading)						
					ENDIF
				ELSE
					CLEANUP_HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS()
					bShouldWarpTruck = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		// warp to safe spawn position
		IF (iTrailerWarpState = 3)
			
			RequestControlOfMOC()
			KeepEveryoneOutMyTruck()
			
			IF NOT IS_ENTITY_DEAD(TrailerID)
				IF NOT (bShouldWarpTruck)
				
					IF NETWORK_HAS_CONTROL_OF_ENTITY(TrailerID)
						
						IF NOT IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(TrailerID)
						
							SET_TRUCK_LOCK_STATE(TrailerID) // restore lock state
							
							IF NOT IS_ENTITY_DEAD(TruckID)
								IF NETWORK_HAS_CONTROL_OF_ENTITY(TruckID)
									SET_TRUCK_LOCK_STATE(TruckID) // restore lock state
								ENDIF
							ENDIF							
							
							SET_ENTITY_ROTATION(TrailerID, <<0.0, 0.0, 0.0>>)
							SET_ENTITY_HEADING(TrailerID, fTruckWarpHeading)
							SET_ENTITY_COORDS(TrailerID, vTruckWarpCoords)	
							IF SET_VEHICLE_ON_GROUND_PROPERLY(TrailerID)
								PRINTLN("[MOC_STUCK]  UPDATE_MOBILE_CONTROL_CENTRE_STUCK_CHECKS - managed to set trailer on ground properly ", vTruckWarpCoords, ", ", fTruckWarpHeading)						
							ELSE
								PRINTLN("[MOC_STUCK]  UPDATE_MOBILE_CONTROL_CENTRE_STUCK_CHECKS - didn't managed to set trailer on ground properly ", vTruckWarpCoords, ", ", fTruckWarpHeading)	
							ENDIF
							iTrailerWarpState++
							
						ELSE
							DETACH_ENTITY(TrailerID, FALSE)
							PRINTLN("[MOC_STUCK]  UPDATE_MOBILE_CONTROL_CENTRE_STUCK_CHECKS - trying to warp trailer, but its attached to something.")	
						ENDIF
					ELSE
						PRINTLN("[MOC_STUCK]  UPDATE_MOBILE_CONTROL_CENTRE_STUCK_CHECKS - trying to warp trailer, but not got control.")	
					ENDIF
				ELSE
					

					IF DoTrailerQuickWarp(iTrailerQuickWarpState, vTruckWarpCoords, fTruckWarpHeading, TruckID)
						
						IF NOT IS_ENTITY_DEAD(TruckID)
							IF NETWORK_HAS_CONTROL_OF_ENTITY(TruckID)
								SET_TRUCK_LOCK_STATE(TruckID) // restore lock state
							ENDIF
						ENDIF
						
						IF NETWORK_HAS_CONTROL_OF_ENTITY(TrailerID)
							SET_TRUCK_LOCK_STATE(TrailerID) // restore lock state
						ENDIF
						
						PRINTLN("[MOC_STUCK]  UPDATE_MOBILE_CONTROL_CENTRE_STUCK_CHECKS - DoTrailerQuickWarp finished.")
						iTrailerWarpState++
					
					ELSE
						PRINTLN("[MOC_STUCK]  UPDATE_MOBILE_CONTROL_CENTRE_STUCK_CHECKS - waiting for DoTrailerQuickWarp().")
					ENDIF
								
				ENDIF
			ELSE
				CLEANUP_MP_SAVED_TRUCK(FALSE, FALSE, TRUE, FALSE, TRUE, FALSE, FALSE) 
				PRINTLN("[MOC_STUCK]  UPDATE_MOBILE_CONTROL_CENTRE_STUCK_CHECKS - trailer is dead")	
				iTrailerWarpState++
			ENDIF
				
		ENDIF
		
		// cleanup
		IF (iTrailerWarpState = 4)
		
			RequestControlOfMOC()
		
			IF NOT IS_ENTITY_DEAD(TrailerID)
				IF DOES_VEHICLE_HAVE_STUCK_VEHICLE_CHECK(TrailerID)				
				AND NETWORK_HAS_CONTROL_OF_ENTITY(TrailerID)
					RESET_VEHICLE_STUCK_TIMER(TrailerID, VEH_STUCK_ON_ROOF)
					RESET_VEHICLE_STUCK_TIMER(TrailerID, VEH_STUCK_ON_SIDE)
					RESET_VEHICLE_STUCK_TIMER(TrailerID, VEH_STUCK_HUNG_UP)
					RESET_VEHICLE_STUCK_TIMER(TrailerID, VEH_STUCK_JAMMED)
				ENDIF
				MPGlobalsAmbience.bMobileCommandCentreMovedHelp = TRUE
			ENDIF
			
			iTrailerWarpState = 0
			bShouldWarpTruck = FALSE
			MPGlobalsAmbience.bMobileCommandCentreIsStuck = FALSE
			MPGlobalsAmbience.bMobileCommandCentreShouldLeave = FALSE
			
			PRINTLN("[MOC_STUCK]  UPDATE_MOBILE_CONTROL_CENTRE_STUCK_CHECKS - cleaned up.")
		ENDIF
	
	ELSE
		IF (MPGlobalsAmbience.bMobileCommandCentreIsStuck)			
			PRINTLN("[MOC_STUCK]  UPDATE_MOBILE_CONTROL_CENTRE_STUCK_CHECKS - in bunker, resetting.")			
			iTrailerWarpState = 0
			bShouldWarpTruck = FALSE
			MPGlobalsAmbience.bMobileCommandCentreIsStuck = FALSE	
			MPGlobalsAmbience.bMobileCommandCentreMovedHelp = FALSE
			MPGlobalsAmbience.bMobileCommandCentreMovingHelp = FALSE
			MPGlobalsAmbience.bMobileCommandCentreShouldLeave = FALSE
		ENDIF
	ENDIF
	
	// print help saying that we are moving.
	IF NOT (MPGlobalsAmbience.bMobileCommandCentreMovedHelp)
		IF (MPGlobalsAmbience.bMobileCommandCentreMovingHelp)
			IF SAFE_TO_PRINT_PV_HELP()
				PRINT_HELP("MOVE_MOC", 10000)
				MPGlobalsAmbience.bMobileCommandCentreMovingHelp = FALSE 
				PRINTLN("[MOC_STUCK] printing help message ")
			ELSE	
				PRINTLN("[MOC_STUCK] waiting to print help message ")
			ENDIF
		ENDIF
	ELSE
		IF SAFE_TO_PRINT_PV_HELP()
			PRINT_HELP("MOVED_MOC", 7000)
			MPGlobalsAmbience.bMobileCommandCentreMovedHelp = FALSE 
			PRINTLN("[MOC_STUCK] printing help message (finished) ")
		ELSE	
			PRINTLN("[MOC_STUCK] waiting to print help message (finisheD) ")
		ENDIF	
	ENDIF
	

ENDPROC

FUNC BOOL ManageStuckCheckForHackerTruck(VEHICLE_INDEX VehicleID)
	INT iPlayerInt
	INT iCount = 0
	VEHICLE_INDEX TrailerID
	
	IF DOES_ENTITY_EXIST(VehicleID)
		IF NOT IS_ENTITY_DEAD(VehicleID)
		
			// does it already have a stuck check? if so, should we remove?
			IF DOES_VEHICLE_HAVE_STUCK_VEHICLE_CHECK(VehicleID)
				
				// if more than 200m away remove
				IF NOT (VDIST2(GET_ENTITY_COORDS(VehicleID), GET_PLAYER_COORDS(PLAYER_ID())) > 40000.0)
					REMOVE_VEHICLE_STUCK_CHECK(VehicleID)
					PRINTLN("[MOC_STUCK] ManageStuckCheckForHackerTruck - removing stuck check, more than 200m away. ")
				ENDIF
			
			ELSE
			
				// should we add a stuck check?
				IF NETWORK_HAS_CONTROL_OF_ENTITY(VehicleID)
					IF (VDIST2(GET_ENTITY_COORDS(VehicleID), GET_PLAYER_COORDS(PLAYER_ID())) < 40000.0)
					
						PRINTLN("[MOC_STUCK] ManageStuckCheckForHackerTruck - try to add, near enough... ")
					
						//  count number of existing stuck checks
						REPEAT NUM_NETWORK_PLAYERS iPlayerInt
							IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iPlayerInt].netID_HackertruckV)
								TrailerID = NET_TO_VEH(GlobalplayerBD[iPlayerInt].netID_HackertruckV)
								IF DOES_ENTITY_EXIST(TrailerID)
									IF NOT IS_ENTITY_DEAD(TrailerID)
										IF DOES_VEHICLE_HAVE_STUCK_VEHICLE_CHECK(TrailerID)
											iCount++
										ENDIF
									ENDIF			
								ENDIF
							ENDIF
						ENDREPEAT
						
						PRINTLN("[MOC_STUCK] ManageStuckCheckForHackerTruck - count = ", iCount)
						
						// can we add?
						IF (iCount < 14)
							IF DOES_ENTITY_EXIST(VehicleID)
								IF NOT IS_ENTITY_DEAD(VehicleID)
									ADD_VEHICLE_STUCK_CHECK_WITH_WARP(VehicleID, 2.0, 500, FALSE, FALSE, FALSE, -1) 
									iCount++
									PRINTLN("[MOC_STUCK] ManageStuckCheckForHackerTruck - Adding. iCount = ", iCount)
									RETURN TRUE
								ENDIF
							ENDIF
						ELSE
							PRINTLN("[MOC_STUCK] ManageStuckCheckForHackerTruck - cant add, count = ", iCount)
						ENDIF
					
					ELSE
						// too far away to add
						//PRINTLN("[MOC_STUCK] ManageStuckCheckForTrailer - too far away. ")
					ENDIF	
				ELSE
					//PRINTLN("[MOC_STUCK] ManageStuckCheckForTrailer - not in network control. ")
				ENDIF
				
			ENDIF
			
		ENDIF			
	ENDIF	

	RETURN FALSE
	
	
ENDFUNC


FUNC BOOL IsHackerTruckStuck(INT iPlayerInt)

	VEHICLE_INDEX TrailerID
	ENTITY_INDEX eiAttached
	VEHICLE_INDEX viAttachedVehicle
	
	IF NOT IS_PLAYER_IN_BUSINESS_HUB(PLAYER_ID())
		IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iPlayerInt].netID_HackertruckV)
			TrailerID = NET_TO_VEH(GlobalplayerBD[iPlayerInt].netID_HackertruckV)
			IF NOT IS_ENTITY_DEAD(TrailerID)
			
				// dont allow cargobob to grab
				IF NETWORK_HAS_CONTROL_OF_ENTITY(TrailerID)
				AND IS_ENTITY_ATTACHED(TrailerID)
					eiAttached = GET_ENTITY_ATTACHED_TO(TrailerID)
					IF IS_ENTITY_A_VEHICLE(eiAttached)
						viAttachedVehicle = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(eiAttached)
						IF IS_ENTITY_CARGOBOB(viAttachedVehicle)
							PRINTLN("[MOC_STUCK]  IsHackerTruckStuck - detaching from cargobob.")
							DETACH_VEHICLE_FROM_ANY_CARGOBOB(TrailerID)
						ENDIF
					ENDIF
				ENDIF			

				IF DOES_VEHICLE_HAVE_STUCK_VEHICLE_CHECK(TrailerID)
					
					IF IS_VEHICLE_STUCK_TIMER_UP(TrailerID, VEH_STUCK_ON_ROOF, 5000)
						PRINTLN("[MOC_STUCK]  IsHackerTruckStuck - true, stuck on roof, iPlayerInt ", iPlayerInt)
						RETURN TRUE
					ENDIF
				
					IF IS_VEHICLE_STUCK_TIMER_UP(TrailerID, VEH_STUCK_ON_SIDE, 5000)
						PRINTLN("[MOC_STUCK]  IsHackerTruckStuck - true, stuck on side, iPlayerInt ", iPlayerInt)
						RETURN TRUE
					ENDIF

					IF IS_VEHICLE_STUCK_TIMER_UP(TrailerID, VEH_STUCK_HUNG_UP, 5000)
						PRINTLN("[MOC_STUCK]  IsHackerTruckStuck - true, hung up, iPlayerInt ", iPlayerInt)
						RETURN TRUE
					ENDIF
					
					IF IS_VEHICLE_STUCK_TIMER_UP(TrailerID, VEH_STUCK_JAMMED, 60000)
						PRINTLN("[MOC_STUCK]  IsHackerTruckStuck - true, jammed, iPlayerInt ", iPlayerInt)
						RETURN TRUE
					ENDIF
				
				ENDIF
					
				//PRINTLN("[MOC_STUCK]  IsMobileControlCentreStuck - ManageStuckCheckForTrailer - iPlayerInt ", iPlayerInt)	
				IF ManageStuckCheckForHackerTruck(TrailerID)
					PRINTLN("[MOC_STUCK]  IsHackerTruckStuck - ManageStuckCheckForHackerTruck - added for iPlayerInt ", iPlayerInt)	
				ENDIF
					
			ELSE
				
				PRINTLN("[MOC_STUCK]  IsHackerTruckStuck - trailer is dead. ", iPlayerInt)
		
			ENDIF
		ELSE
			//PRINTLN("[MOC_STUCK]  IsMobileControlCentreStuck - net trailer does not exist.  ", iPlayerInt)	
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF (bForceMobileIsStuck)
			RETURN TRUE
		ENDIF
	#ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_PLAYER_INSIDE_MY_HACKER_TRUCK(PLAYER_INDEX PlayerID)
	IF IS_PLAYER_IN_HACKER_TRUCK(PlayerID)		
		IF (GET_OWNER_OF_ARMORY_TRUCK_THAT_PLAYER_IS_INSIDE(PlayerID) = PLAYER_ID())	
			PRINTLN("[MOC_STUCK] IS_PLAYER_INSIDE_MY_ARMOUR_TRUCK - player ", NATIVE_TO_INT(PlayerID), " is inside my hacker truck")
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANYONE_INSIDE_MY_HACKER_TRUCK()
	INT i
	PLAYER_INDEX PlayerID
	REPEAT NUM_NETWORK_PLAYERS i
		PlayerID = INT_TO_NATIVE(PLAYER_INDEX, i)
		IF IS_PLAYER_INSIDE_MY_HACKER_TRUCK(PlayerID)
			PRINTLN("[MOC_STUCK] IS_ANYONE_INSIDE_MY_HACKER_TRUCK - player ", NATIVE_TO_INT(PlayerID), " is inside my truck")
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC


BOOL bShouldWarpHackerTruck
INT iHackerTruckWarpState
TIME_DATATYPE timeHackerTruckWarp
VECTOR vHackerTruckWarpCoords
FLOAT fHackerTruckWarpHeading
INT iHackerTruckStuckStagger
TIME_DATATYPE timeHackerTruckLeaveEvent

//Request warp nearby vars
INT iHackerTruckWarpNearbyState

#IF FEATURE_DLC_2_2022
INT iAcidLabWarpState
INT iAcidLabWarpNearbyState
VECTOR vAcidLabWarpCoords
FLOAT fAcidLabWarpHeading
INT iAcidLabStuckStagger
TIME_DATATYPE timeAcidLabWarp
TIME_DATATYPE timeAcidLabLeaveEvent
BOOL bShouldWarpAcidLab
//INT iAcidLabQuickWarpState
#ENDIF

PROC RequestControlOfHackerTruck()
	VEHICLE_INDEX TruckID
	IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV)
		TruckID = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV)
	ENDIF

	// request control
	IF DOES_ENTITY_EXIST(TruckID)
	AND NOT NETWORK_HAS_CONTROL_OF_ENTITY(TruckID)
		NETWORK_REQUEST_CONTROL_OF_ENTITY(TruckID)
	ENDIF	
ENDPROC

PROC KeepEveryoneOutMyHackerTruck()
	
	VEHICLE_INDEX TruckID
	IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV)
		TruckID = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV)
	ENDIF
	
	IF (ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), timeHackerTruckLeaveEvent)) > 1500)
	
		// kick out trailer
		IF IS_ANYONE_INSIDE_MY_HACKER_TRUCK()
			IF NOT HAS_OWNER_KICKED_OUT_PLAYER_FROM_HACKER_TRUCK(PLAYER_ID())	
				PRINTLN("[MOC_STUCK]  KeepEveryoneOutMyHackerTruck - someone is inside truck")
				SET_KICK_OUT_PLAYER_FROM_HACKER_TRUCK(TRUE)	
			ENDIF
		ENDIF
		
		IF MPGlobalsAmbience.bHackerTruckShouldLeave = FALSE
			MPGlobalsAmbience.bHackerTruckShouldLeave = TRUE
			PRINTLN("[MOC_STUCK]  KeepEveryoneOutMyHackerTruck - MPGlobalsAmbience.bHackerTruckShouldLeave = TRUE")
		ENDIF
		
		// kick out truck - if attached.
		IF DOES_ENTITY_EXIST(TruckID)
		AND IS_VEHICLE_DRIVEABLE(TruckID)
			PLAYER_INDEX PlayerID
			INT i
			REPEAT NUM_NETWORK_PLAYERS i
				PlayerID = INT_TO_PLAYERINDEX(i)
				IF IS_NET_PLAYER_OK(PlayerID, FALSE)
					IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(PlayerID), TruckID)
						BROADCAST_LEAVE_VEHICLE(SPECIFIC_PLAYER(PlayerID), TRUE, 0, 0, FALSE, FALSE)
						timeHackerTruckLeaveEvent = GET_NETWORK_TIME()
					ENDIF
				ENDIF
			ENDREPEAT						
			
		ENDIF
		
		// keep cab locked
		IF DOES_ENTITY_EXIST(TruckID)
		AND NETWORK_HAS_CONTROL_OF_ENTITY(TruckID)
		AND IS_VEHICLE_DRIVEABLE(TruckID)
			SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(TruckID, TRUE)	
		ENDIF
		
	ELSE
		PRINTLN("[MOC_STUCK]  KeepEveryoneOutMyHackerTruck - waiting for timeHackerTruckLeaveEvent to expire, time: ", ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), timeHackerTruckLeaveEvent)))
	ENDIF
ENDPROC

#IF FEATURE_DLC_2_2022
FUNC BOOL IS_PLAYER_INSIDE_MY_ACID_LAB(PLAYER_INDEX PlayerID)
	IF IS_PLAYER_IN_ACID_LAB(PlayerID)		
		IF (GET_OWNER_OF_ACID_LAB_THAT_PLAYER_IS_INSIDE(PlayerID) = PLAYER_ID())	
			PRINTLN("[ACID_LAB_STUCK] IS_PLAYER_INSIDE_MY_ACID_LAB - player ", NATIVE_TO_INT(PlayerID), " is inside my acid lab")
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANYONE_INSIDE_MY_ACID_LAB()
	INT i
	PLAYER_INDEX PlayerID
	REPEAT NUM_NETWORK_PLAYERS i
		PlayerID = INT_TO_NATIVE(PLAYER_INDEX, i)
		IF IS_PLAYER_INSIDE_MY_ACID_LAB(PlayerID)
			PRINTLN("[ACID_LAB_STUCK] IS_ANYONE_INSIDE_MY_ACID_LAB - player ", NATIVE_TO_INT(PlayerID), " is inside my acid lab")
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL ManageStuckCheckForAcidLab(VEHICLE_INDEX VehicleID)
	INT iPlayerInt
	INT iCount = 0
	VEHICLE_INDEX TrailerID
	
	IF DOES_ENTITY_EXIST(VehicleID)
		IF NOT IS_ENTITY_DEAD(VehicleID)
		
			// does it already have a stuck check? if so, should we remove?
			IF DOES_VEHICLE_HAVE_STUCK_VEHICLE_CHECK(VehicleID)
				
				// if more than 200m away remove
				IF NOT (VDIST2(GET_ENTITY_COORDS(VehicleID), GET_PLAYER_COORDS(PLAYER_ID())) > 40000.0)
					REMOVE_VEHICLE_STUCK_CHECK(VehicleID)
					PRINTLN("[ACID_LAB_STUCK] ManageStuckCheckForAcidLab - removing stuck check, more than 200m away. ")
				ENDIF
			
			ELSE
			
				// should we add a stuck check?
				IF NETWORK_HAS_CONTROL_OF_ENTITY(VehicleID)
					IF (VDIST2(GET_ENTITY_COORDS(VehicleID), GET_PLAYER_COORDS(PLAYER_ID())) < 40000.0)
					
						PRINTLN("[ACID_LAB_STUCK] ManageStuckCheckForAcidLab - try to add, near enough... ")
					
						//  count number of existing stuck checks
						REPEAT NUM_NETWORK_PLAYERS iPlayerInt
							IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iPlayerInt].netID_AcidLabV)
								TrailerID = NET_TO_VEH(GlobalplayerBD[iPlayerInt].netID_AcidLabV)
								IF DOES_ENTITY_EXIST(TrailerID)
									IF NOT IS_ENTITY_DEAD(TrailerID)
										IF DOES_VEHICLE_HAVE_STUCK_VEHICLE_CHECK(TrailerID)
											iCount++
										ENDIF
									ENDIF			
								ENDIF
							ENDIF
						ENDREPEAT
						
						PRINTLN("[ACID_LAB_STUCK] ManageStuckCheckForAcidLab - count = ", iCount)
						
						// can we add?
						IF (iCount < 14)
							IF DOES_ENTITY_EXIST(VehicleID)
								IF NOT IS_ENTITY_DEAD(VehicleID)
									ADD_VEHICLE_STUCK_CHECK_WITH_WARP(VehicleID, 2.0, 500, FALSE, FALSE, FALSE, -1) 
									iCount++
									PRINTLN("[ACID_LAB_STUCK] ManageStuckCheckForAcidLab - Adding. iCount = ", iCount)
									RETURN TRUE
								ENDIF
							ENDIF
						ELSE
							PRINTLN("[ACID_LAB_STUCK] ManageStuckCheckForAcidLab - cant add, count = ", iCount)
						ENDIF
					
					ELSE
						// too far away to add
						//PRINTLN("[ACID_LAB_STUCK] ManageStuckCheckForAcidLab - too far away. ")
					ENDIF	
				ELSE
					//PRINTLN("[ACID_LAB_STUCK] ManageStuckCheckForAcidLab - not in network control. ")
				ENDIF
				
			ENDIF
			
		ENDIF			
	ENDIF	

	RETURN FALSE
	
	
ENDFUNC

FUNC BOOL IsAcidLabStuck(INT iPlayerInt)

	VEHICLE_INDEX TrailerID
	ENTITY_INDEX eiAttached
	VEHICLE_INDEX viAttachedVehicle
	
	IF NOT IS_PLAYER_IN_JUGGALO_HIDEOUT_PROPERTY(PLAYER_ID())
		IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iPlayerInt].netID_AcidLabV)
			TrailerID = NET_TO_VEH(GlobalplayerBD[iPlayerInt].netID_AcidLabV)
			IF NOT IS_ENTITY_DEAD(TrailerID)
			
				// dont allow cargobob to grab
				IF NETWORK_HAS_CONTROL_OF_ENTITY(TrailerID)
				AND IS_ENTITY_ATTACHED(TrailerID)
					eiAttached = GET_ENTITY_ATTACHED_TO(TrailerID)
					IF IS_ENTITY_A_VEHICLE(eiAttached)
						viAttachedVehicle = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(eiAttached)
						IF IS_ENTITY_CARGOBOB(viAttachedVehicle)
							PRINTLN("[ACID_LAB_STUCK]  IsAcidLabStuck - detaching from cargobob.")
							DETACH_VEHICLE_FROM_ANY_CARGOBOB(TrailerID)
						ENDIF
					ENDIF
				ENDIF			

				IF DOES_VEHICLE_HAVE_STUCK_VEHICLE_CHECK(TrailerID)
					
					IF IS_VEHICLE_STUCK_TIMER_UP(TrailerID, VEH_STUCK_ON_ROOF, 5000)
						PRINTLN("[ACID_LAB_STUCK]  IsAcidLabStuck - true, stuck on roof, iPlayerInt ", iPlayerInt)
						RETURN TRUE
					ENDIF
				
					IF IS_VEHICLE_STUCK_TIMER_UP(TrailerID, VEH_STUCK_ON_SIDE, 5000)
						PRINTLN("[ACID_LAB_STUCK]  IsAcidLabStuck - true, stuck on side, iPlayerInt ", iPlayerInt)
						RETURN TRUE
					ENDIF

					IF IS_VEHICLE_STUCK_TIMER_UP(TrailerID, VEH_STUCK_HUNG_UP, 5000)
						PRINTLN("[ACID_LAB_STUCK]  IsAcidLabStuck - true, hung up, iPlayerInt ", iPlayerInt)
						RETURN TRUE
					ENDIF
					
					IF IS_VEHICLE_STUCK_TIMER_UP(TrailerID, VEH_STUCK_JAMMED, 60000)
						PRINTLN("[ACID_LAB_STUCK]  IsAcidLabStuck - true, jammed, iPlayerInt ", iPlayerInt)
						RETURN TRUE
					ENDIF
				
				ENDIF
					
				//PRINTLN("[MOC_STUCK]  IsMobileControlCentreStuck - ManageStuckCheckForTrailer - iPlayerInt ", iPlayerInt)	
				IF ManageStuckCheckForAcidLab(TrailerID)
					PRINTLN("[ACID_LAB_STUCK]  IsAcidLabStuck - ManageStuckCheckForAcidLab - added for iPlayerInt ", iPlayerInt)	
				ENDIF
					
			ELSE
				PRINTLN("[ACID_LAB_STUCK]  IsAcidLabStuck - trailer is dead. ", iPlayerInt)
			ENDIF
		ELSE
			//PRINTLN("[MOC_STUCK]  IsMobileControlCentreStuck - net trailer does not exist.  ", iPlayerInt)	
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF (bForceMobileIsStuck)
			RETURN TRUE
		ENDIF
	#ENDIF
	
	RETURN FALSE

ENDFUNC

PROC RequestControlOfAcidLab()
	VEHICLE_INDEX TruckID
	IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV)
		TruckID = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV)
	ENDIF

	// request control
	IF DOES_ENTITY_EXIST(TruckID)
	AND NOT NETWORK_HAS_CONTROL_OF_ENTITY(TruckID)
		NETWORK_REQUEST_CONTROL_OF_ENTITY(TruckID)
	ENDIF	
ENDPROC

PROC KeepEveryoneOutMyAcidLab()
	
	VEHICLE_INDEX TruckID
	IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV)
		TruckID = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV)
	ENDIF
	
	IF (ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), timeAcidLabLeaveEvent)) > 1500)
	
		// kick out trailer
		IF IS_ANYONE_INSIDE_MY_ACID_LAB()
			IF NOT HAS_OWNER_KICKED_OUT_PLAYER_FROM_ACID_LAB(PLAYER_ID())	
				PRINTLN("[ACID_LAB_STUCK]  KeepEveryoneOutMyAcidLab - someone is inside truck")
				SET_KICK_OUT_PLAYER_FROM_ACID_LAB(TRUE)	
			ENDIF
		ENDIF
		
		IF MPGlobalsAmbience.bAcidLabShouldLeave = FALSE
			MPGlobalsAmbience.bAcidLabShouldLeave = TRUE
			PRINTLN("[ACID_LAB_STUCK]  KeepEveryoneOutMyAcidLab - MPGlobalsAmbience.bAcidLabShouldLeave = TRUE")
		ENDIF
		
		// kick out truck - if attached.
		IF DOES_ENTITY_EXIST(TruckID)
		AND IS_VEHICLE_DRIVEABLE(TruckID)
			PLAYER_INDEX PlayerID
			INT i
			REPEAT NUM_NETWORK_PLAYERS i
				PlayerID = INT_TO_PLAYERINDEX(i)
				IF IS_NET_PLAYER_OK(PlayerID, FALSE)
					IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(PlayerID), TruckID)
						BROADCAST_LEAVE_VEHICLE(SPECIFIC_PLAYER(PlayerID), TRUE, 0, 0, FALSE, FALSE)
						timeAcidLabLeaveEvent = GET_NETWORK_TIME()
					ENDIF
				ENDIF
			ENDREPEAT						
			
		ENDIF
		
		// keep cab locked
		IF DOES_ENTITY_EXIST(TruckID)
		AND NETWORK_HAS_CONTROL_OF_ENTITY(TruckID)
		AND IS_VEHICLE_DRIVEABLE(TruckID)
			SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(TruckID, TRUE)	
		ENDIF
		
	ELSE
		PRINTLN("[ACID_LAB_STUCK]  KeepEveryoneOutMyAcidLab - waiting for timeAcidLabLeaveEvent to expire, time: ", ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), timeAcidLabLeaveEvent)))
	ENDIF
ENDPROC
#ENDIF

PROC UPDATE_HACKER_TRUCK_STUCK_CHECKS()

	INT iDiff
	INT i

	VEHICLE_INDEX TruckID
	IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV)
		TruckID = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV)
		IF NOT IS_ENTITY_DEAD(TruckID)
			IF DOES_VEHICLE_HAVE_STUCK_VEHICLE_CHECK(TruckID)				
			AND NETWORK_HAS_CONTROL_OF_ENTITY(TruckID)
			AND GET_ENTITY_SPEED(TruckID) > 1.0
				RESET_VEHICLE_STUCK_TIMER(TruckID, VEH_STUCK_ON_ROOF)
				RESET_VEHICLE_STUCK_TIMER(TruckID, VEH_STUCK_ON_SIDE)
				RESET_VEHICLE_STUCK_TIMER(TruckID, VEH_STUCK_HUNG_UP)
				RESET_VEHICLE_STUCK_TIMER(TruckID, VEH_STUCK_JAMMED)
			ENDIF
		ENDIF
	ENDIF

	IF IsHackerTruckStuck(iHackerTruckStuckStagger)
	AND NOT IS_MP_SAVED_HACKERTRUCK_BEING_CLEANED_UP()
		PRINTLN("[MOC_STUCK]  UPDATE_HACKER_TRUCK_STUCK_CHECKS - detected stuck mobile command center. iHackerTruckStuckStagger ", iHackerTruckStuckStagger)		
		BROADCAST_GENERAL_EVENT(GENERAL_EVENT_MOVING_HACKER_TRUCK_STUCK, SPECIFIC_PLAYER(INT_TO_NATIVE(PLAYER_INDEX, iHackerTruckStuckStagger)))
	ENDIF
	iHackerTruckStuckStagger++	
	IF (iHackerTruckStuckStagger >= NUM_NETWORK_PLAYERS)
		iHackerTruckStuckStagger = 0
	ENDIF
	
	IF NOT IS_PLAYER_IN_BUSINESS_HUB(PLAYER_ID())
	AND iHackerTruckWarpNearbyState = 0
	
		//PRINTLN("[MOC_STUCK]  UPDATE_HACKER_TRUCK_STUCK_CHECKS - vehicle is driveable.")

		// is stuck?
		IF (iHackerTruckWarpState = 0)
			IF (MPGlobalsAmbience.bHackerTruckIsStuck)
				PRINTLN("[MOC_STUCK]  UPDATE_HACKER_TRUCK_STUCK_CHECKS - my trailer is stuck")
				timeHackerTruckWarp = GET_NETWORK_TIME_ACCURATE()
				timeHackerTruckLeaveEvent = GET_NETWORK_TIME_ACCURATE()
				
				RequestControlOfHackerTruck()
				
				// broadcast event to print help to all players who were inside truck or cab
				
				PLAYER_INDEX PlayerID
				INT iPlayerFlags
				REPEAT NUM_NETWORK_PLAYERS i
					PlayerID = INT_TO_PLAYERINDEX(i)
					IF IS_NET_PLAYER_OK(PlayerID, FALSE)
						IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(PlayerID), TruckID)
						OR IS_PLAYER_INSIDE_MY_HACKER_TRUCK(PlayerID)
							SET_BIT( iPlayerFlags , i)
						ENDIF
					ENDIF
				ENDREPEAT
				
				BROADCAST_GENERAL_EVENT(GENERAL_EVENT_MOVING_HACKER_TRUCK, iPlayerFlags)
				iHackerTruckWarpState++
			ENDIF
		ENDIF
	

		// wait for everyone to be out
		IF (iHackerTruckWarpState = 1)

			RequestControlOfHackerTruck()
			KeepEveryoneOutMyHackerTruck()
			
			IF NOT IS_ANYONE_INSIDE_MY_HACKER_TRUCK()
			AND DOES_ENTITY_EXIST(TruckID)
			AND IS_VEHICLE_EMPTY(TruckID, TRUE, TRUE, FALSE, FALSE, FALSE, TRUE, TRUE)
			
				iDiff = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), timeHackerTruckWarp))
				PRINTLN("[MOC_STUCK]  UPDATE_HACKER_TRUCK_STUCK_CHECKS - waiting for everyone to be kicked out. iDiff = ", iDiff)
				IF (iDiff > 10000)
				
					IF IS_ANY_PLAYER_TOUCHING_VEHICLE(TruckID)
						PRINTLN("[MOC_STUCK]  UPDATE_HACKER_TRUCK_STUCK_CHECKS - IS_ANY_PLAYER_TOUCHING_VEHICLE = TRUE")
					ELSE
					
						vHackerTruckWarpCoords = GET_ENTITY_COORDS(TruckID, FALSE)
						PRINTLN("[MOC_STUCK]  UPDATE_HACKER_TRUCK_STUCK_CHECKS - vHackerTruckWarpCoords = ", vHackerTruckWarpCoords)
											
						IF IS_VEHICLE_DRIVEABLE(TruckID)
							//IF IS_VEHICLE_ATTACHED_TO_TRAILER(TruckID)
								bShouldWarpHackerTruck = TRUE
								iHackerTruckQuickWarpState = 0
								PRINTLN("[MOC_STUCK]  UPDATE_HACKER_TRUCK_STUCK_CHECKS - setting bShouldWarpHackerTruck")
							//ENDIF
						ENDIF		
						
						iHackerTruckWarpState++
					ENDIF
					
				ELSE
					
					// display help
					
				
				ENDIF
			ENDIF
		ENDIF
		
		// get safe spawn position
		IF (iHackerTruckWarpState = 2)
		
			RequestControlOfHackerTruck()
			KeepEveryoneOutMyTruck()
		
			VEHICLE_SPAWN_LOCATION_PARAMS Params
			Params.fMinDistFromCoords = 20.0
			Params.fMaxDistance = 250.0
			Params.bConsiderHighways = TRUE
			Params.bCheckEntityArea = TRUE
			Params.bCheckOwnVisibility = FALSE
			Params.bIsForPV = TRUE
			Params.bIgnoreCustomNodesForArea = TRUE	
			
			IF NOT (bShouldWarpHackerTruck)
				IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(vHackerTruckWarpCoords, <<0.0, 0.0, 0.0>>, TERBYTE, FALSE, vHackerTruckWarpCoords, fHackerTruckWarpHeading, Params)
					vHackerTruckWarpCoords.z += 2.0						
					iHackerTruckWarpState++
					PRINTLN("[MOC_STUCK]  UPDATE_HACKER_TRUCK_STUCK_CHECKS - warping trailer only to ", vHackerTruckWarpCoords, ", ", fHackerTruckWarpHeading)
				ENDIF
			ELSE
				IF IS_VEHICLE_DRIVEABLE(TruckID)
				//AND IS_VEHICLE_ATTACHED_TO_TRAILER(TruckID)	
					
					ADD_CUSTOM_NODES_FOR_ALL_TRUCK_SPAWNS()
					Params.fMaxDistance= 9999.0	
				
					IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(vHackerTruckWarpCoords, <<0.0, 0.0, 0.0>>, TERBYTE, FALSE, vHackerTruckWarpCoords, fHackerTruckWarpHeading, Params)
						
						CLEAR_CUSTOM_VEHICLE_NODES()
						
						iHackerTruckWarpState++
						PRINTLN("[MOC_STUCK]  UPDATE_HACKER_TRUCK_STUCK_CHECKS - warping truck and trailer to ", vHackerTruckWarpCoords, ", ", fHackerTruckWarpHeading)						
					ENDIF
				ELSE
					CLEANUP_HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS()
					bShouldWarpHackerTruck = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		// warp to safe spawn position
		IF (iHackerTruckWarpState = 3)
			
			RequestControlOfHackerTruck()
			KeepEveryoneOutMyHackerTruck()
			
			IF NOT IS_ENTITY_DEAD(TruckID)
				IF NOT (bShouldWarpHackerTruck)
				
					IF NETWORK_HAS_CONTROL_OF_ENTITY(TruckID)
						
						IF NOT IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(TruckID)

							SET_ENTITY_ROTATION(TruckID, <<0.0, 0.0, 0.0>>)
							SET_ENTITY_HEADING(TruckID, fHackerTruckWarpHeading)
							SET_ENTITY_COORDS(TruckID, vHackerTruckWarpCoords)	
							IF SET_VEHICLE_ON_GROUND_PROPERLY(TruckID)
								PRINTLN("[MOC_STUCK]  UPDATE_HACKER_TRUCK_STUCK_CHECKS - managed to set trailer on ground properly ", vHackerTruckWarpCoords, ", ", fHackerTruckWarpHeading)						
							ELSE
								PRINTLN("[MOC_STUCK]  UPDATE_HACKER_TRUCK_STUCK_CHECKS - didn't managed to set trailer on ground properly ", vHackerTruckWarpCoords, ", ", fHackerTruckWarpHeading)	
							ENDIF
							
							SET_HACKER_TRUCK_LOCK_STATE(TruckID) // restore lock state
							
							iHackerTruckWarpState++
							
						ELSE
							DETACH_ENTITY(TruckID, FALSE)
							PRINTLN("[MOC_STUCK]  UPDATE_HACKER_TRUCK_STUCK_CHECKS - trying to warp trailer, but its attached to something.")	
						ENDIF
					ELSE
						PRINTLN("[MOC_STUCK]  UPDATE_HACKER_TRUCK_STUCK_CHECKS - trying to warp trailer, but not got control.")	
					ENDIF
				ELSE
					
					IF DoTrailerQuickWarp(iHackerTruckQuickWarpState, vHackerTruckWarpCoords, fHackerTruckWarpHeading, TruckID)
						
						IF NOT IS_ENTITY_DEAD(TruckID)
							IF NETWORK_HAS_CONTROL_OF_ENTITY(TruckID)
								SET_HACKER_TRUCK_LOCK_STATE(TruckID) // restore lock state
							ENDIF
						ENDIF

						PRINTLN("[MOC_STUCK]  UPDATE_MOBILE_CONTROL_CENTRE_STUCK_CHECKS - DoTrailerQuickWarp finished.")
						iHackerTruckWarpState++
					
					ELSE
						PRINTLN("[MOC_STUCK]  UPDATE_MOBILE_CONTROL_CENTRE_STUCK_CHECKS - waiting for DoTrailerQuickWarp().")
					ENDIF		
				ENDIF
			ELSE
				CLEANUP_MP_SAVED_HACKERTRUCK(FALSE, FALSE, TRUE, FALSE, TRUE, FALSE, FALSE) 
				PRINTLN("[MOC_STUCK]  UPDATE_HACKER_TRUCK_STUCK_CHECKS - trailer is dead")	
				iHackerTruckWarpState++
			ENDIF
				
		ENDIF
		
		// cleanup
		IF (iHackerTruckWarpState = 4)
		
			RequestControlOfHackerTruck()
		
			IF NOT IS_ENTITY_DEAD(TruckID)
				IF DOES_VEHICLE_HAVE_STUCK_VEHICLE_CHECK(TruckID)				
				AND NETWORK_HAS_CONTROL_OF_ENTITY(TruckID)
					RESET_VEHICLE_STUCK_TIMER(TruckID, VEH_STUCK_ON_ROOF)
					RESET_VEHICLE_STUCK_TIMER(TruckID, VEH_STUCK_ON_SIDE)
					RESET_VEHICLE_STUCK_TIMER(TruckID, VEH_STUCK_HUNG_UP)
					RESET_VEHICLE_STUCK_TIMER(TruckID, VEH_STUCK_JAMMED)
				ENDIF
				MPGlobalsAmbience.bHackerTruckMovedHelp = TRUE
			ENDIF
			
			iHackerTruckWarpState = 0
			bShouldWarpHackerTruck = FALSE
			MPGlobalsAmbience.bHackerTruckIsStuck = FALSE
			MPGlobalsAmbience.bHackerTruckShouldLeave = FALSE
			
			PRINTLN("[MOC_STUCK]  UPDATE_HACKER_TRUCK_STUCK_CHECKS - cleaned up.")
		ENDIF
	
	ELSE
		IF (MPGlobalsAmbience.bHackerTruckIsStuck)			
			PRINTLN("[MOC_STUCK]  UPDATE_HACKER_TRUCK_STUCK_CHECKS - in bunker, resetting.")			
			iHackerTruckWarpState = 0
			bShouldWarpHackerTruck = FALSE
			MPGlobalsAmbience.bHackerTruckIsStuck = FALSE	
			MPGlobalsAmbience.bHackerTruckMovedHelp = FALSE
			MPGlobalsAmbience.bHackerTruckMovingHelp = FALSE
			MPGlobalsAmbience.bHackerTruckShouldLeave = FALSE
		ENDIF
	ENDIF
	
	// print help saying that we are moving.
	IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS4, BS_SIMPLE_INTERIOR_GLOBAL_BS_FOUR_REMOTE_STORED_HACKER_TRUCK_RETURNED_TO_GARAGE)
		IF NOT (MPGlobalsAmbience.bHackerTruckMovedHelp)
			IF (MPGlobalsAmbience.bHackerTruckMovingHelp)
				IF SAFE_TO_PRINT_PV_HELP()
				AND NOT IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
				AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
					PRINT_HELP("MOVE_HACKER", 10000)
					MPGlobalsAmbience.bHackerTruckMovingHelp = FALSE 
					PRINTLN("[MOC_STUCK] printing help message ")
				ELSE	
					PRINTLN("[MOC_STUCK] waiting to print help message ")
				ENDIF
			ENDIF
		ELSE
			IF SAFE_TO_PRINT_PV_HELP()
			AND NOT IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
			AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
				PRINT_HELP("MOVED_HACKER", 7000)
				MPGlobalsAmbience.bHackerTruckMovedHelp = FALSE 
				PRINTLN("[MOC_STUCK] printing help message (finished) ")
			ELSE	
				PRINTLN("[MOC_STUCK] waiting to print help message (finisheD) ")
			ENDIF	
		ENDIF
	ELSE
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MOVED_HACKER")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MOVE_HACKER")
			CLEAR_HELP()
			MPGlobalsAmbience.bHackerTruckMovingHelp = FALSE
		ENDIF
	ENDIF

ENDPROC

PROC UPDATE_WARP_HACKER_TRUCK_NEARBY()

	VEHICLE_INDEX TruckID
	IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV)
		TruckID = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV)
	ENDIF
	
	IF HAS_PERSONAL_HACKER_TRUCK_BEEN_REQUESTED_TO_WARP_NEAR()
	AND iHackerTruckWarpState = 0
		// Do we need to move it?
		IF (iHackerTruckWarpNearbyState = 0)
			PRINTLN("[TERRORBYTE_WARP]  UPDATE_WARP_HACKER_TRUCK_NEARBY - my trailer is stuck")
			RequestControlOfHackerTruck()
			iHackerTruckWarpNearbyState++
		ENDIF	

		// wait for control of the vehicle
		IF (iHackerTruckWarpNearbyState = 1)

			RequestControlOfHackerTruck()
			
			IF DOES_ENTITY_EXIST(TruckID)
			AND IS_VEHICLE_EMPTY(TruckID, TRUE, TRUE, FALSE, FALSE, FALSE, TRUE, TRUE)
			AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV)
				NETWORK_FADE_OUT_ENTITY(TruckID, TRUE, TRUE)
				vHackerTruckWarpCoords = MPGlobals.VehicleData.vOverrideSecondaryPVWarpRequestCoord
				PRINTLN("[TERRORBYTE_WARP]  UPDATE_WARP_HACKER_TRUCK_NEARBY - Warp requested near = ", vHackerTruckWarpCoords)
				
				iHackerTruckWarpNearbyState++
			ENDIF
		ENDIF
		
		// get safe spawn position
		IF (iHackerTruckWarpNearbyState = 2)
		
			RequestControlOfHackerTruck()
		
			VEHICLE_SPAWN_LOCATION_PARAMS Params
			Params.fMinDistFromCoords = 20.0
			Params.fMaxDistance = 150.0
			Params.bConsiderHighways = TRUE
			Params.bCheckEntityArea = TRUE
			Params.bCheckOwnVisibility = FALSE
			Params.bIsForPV = TRUE
			Params.bConsiderOnlyActiveNodes = TRUE
			
			IF IS_VEHICLE_DRIVEABLE(TruckID)
				Params.fMaxDistance= 9999.0	
			
				IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(vHackerTruckWarpCoords, <<0.0, 0.0, 0.0>>, TERBYTE, FALSE, vHackerTruckWarpCoords, fHackerTruckWarpHeading, Params)					
					iHackerTruckWarpNearbyState++
					PRINTLN("[TERRORBYTE_WARP]  UPDATE_WARP_HACKER_TRUCK_NEARBY - warping truck and trailer to ", vHackerTruckWarpCoords, ", ", fHackerTruckWarpHeading)						
				ENDIF
			ELSE
				CLEANUP_MP_SAVED_HACKERTRUCK(FALSE, FALSE, TRUE, FALSE, TRUE, FALSE, FALSE) 
				PRINTLN("[TERRORBYTE_WARP]  UPDATE_WARP_HACKER_TRUCK_NEARBY - truck is not driveable")	
				iHackerTruckWarpNearbyState += 2
				CLEANUP_HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS()
			ENDIF
		ENDIF
		
		// warp to safe spawn position
		IF (iHackerTruckWarpNearbyState = 3)
			
			RequestControlOfHackerTruck()
			
			IF NOT IS_ENTITY_DEAD(TruckID)
				
				IF NETWORK_HAS_CONTROL_OF_ENTITY(TruckID)
					
					IF NOT IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(TruckID)

						SET_ENTITY_ROTATION(TruckID, <<0.0, 0.0, 0.0>>)
						SET_ENTITY_HEADING(TruckID, fHackerTruckWarpHeading)
						SET_ENTITY_COORDS(TruckID, vHackerTruckWarpCoords)	
						IF SET_VEHICLE_ON_GROUND_PROPERLY(TruckID)
							PRINTLN("[TERRORBYTE_WARP]  UPDATE_WARP_HACKER_TRUCK_NEARBY - managed to set trailer on ground properly ", vHackerTruckWarpCoords, ", ", fHackerTruckWarpHeading)						
						ELSE
							PRINTLN("[TERRORBYTE_WARP]  UPDATE_WARP_HACKER_TRUCK_NEARBY - didn't managed to set trailer on ground properly ", vHackerTruckWarpCoords, ", ", fHackerTruckWarpHeading)	
						ENDIF
						
						NETWORK_FADE_IN_ENTITY(TruckID, TRUE, TRUE)
						SET_HACKER_TRUCK_LOCK_STATE(TruckID) // restore lock state
						
						iHackerTruckWarpNearbyState++
						
					ELSE
						DETACH_ENTITY(TruckID, FALSE)
						PRINTLN("[TERRORBYTE_WARP]  UPDATE_WARP_HACKER_TRUCK_NEARBY - trying to warp trailer, but its attached to something.")	
					ENDIF
				ELSE
					PRINTLN("[TERRORBYTE_WARP]  UPDATE_WARP_HACKER_TRUCK_NEARBY - trying to warp trailer, but not got control.")	
				ENDIF
			ELSE
				CLEANUP_MP_SAVED_HACKERTRUCK(FALSE, FALSE, TRUE, FALSE, TRUE, FALSE, FALSE) 
				PRINTLN("[TERRORBYTE_WARP]  UPDATE_WARP_HACKER_TRUCK_NEARBY - trailer is dead")	
				iHackerTruckWarpNearbyState++
			ENDIF
				
		ENDIF
		
		// cleanup
		IF (iHackerTruckWarpNearbyState = 4)
			iHackerTruckWarpNearbyState = 0
			CLEAR_REQUEST_TO_WARP_PERSONAL_HACKER_TRUCK_NEAR()
			PRINTLN("[TERRORBYTE_WARP]  UPDATE_WARP_HACKER_TRUCK_NEARBY - cleaned up.")
		ENDIF
	ENDIF	

ENDPROC

#IF FEATURE_DLC_2_2022
PROC UPDATE_ACID_LAB_STUCK_CHECKS()

	INT iDiff
	INT i

	VEHICLE_INDEX TruckID
	IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV)
		TruckID = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV)
		IF NOT IS_ENTITY_DEAD(TruckID)
			IF DOES_VEHICLE_HAVE_STUCK_VEHICLE_CHECK(TruckID)				
			AND NETWORK_HAS_CONTROL_OF_ENTITY(TruckID)
			AND GET_ENTITY_SPEED(TruckID) > 1.0
				RESET_VEHICLE_STUCK_TIMER(TruckID, VEH_STUCK_ON_ROOF)
				RESET_VEHICLE_STUCK_TIMER(TruckID, VEH_STUCK_ON_SIDE)
				RESET_VEHICLE_STUCK_TIMER(TruckID, VEH_STUCK_HUNG_UP)
				RESET_VEHICLE_STUCK_TIMER(TruckID, VEH_STUCK_JAMMED)
			ENDIF
		ENDIF
	ENDIF

	IF IsAcidLabStuck(iAcidLabStuckStagger)
	AND NOT IS_MP_SAVED_ACIDLAB_BEING_CLEANED_UP()
		PRINTLN("[ACID_LAB_STUCK]  UPDATE_ACID_LAB_STUCK_CHECKS - detected stuck acid lab. iAcidLabStuckStagger ", iAcidLabStuckStagger)		
		BROADCAST_GENERAL_EVENT(GENERAL_EVENT_MOVING_ACID_LAB_STUCK, SPECIFIC_PLAYER(INT_TO_NATIVE(PLAYER_INDEX, iAcidLabStuckStagger)))
	ENDIF
	
	iAcidLabStuckStagger++	
	IF (iAcidLabStuckStagger >= NUM_NETWORK_PLAYERS)
		iAcidLabStuckStagger = 0
	ENDIF
	
	IF NOT IS_PLAYER_IN_JUGGALO_HIDEOUT_PROPERTY(PLAYER_ID())
	AND iAcidLabWarpNearbyState = 0
	
		//PRINTLN("[MOC_STUCK]  UPDATE_HACKER_TRUCK_STUCK_CHECKS - vehicle is driveable.")

		// is stuck?
		IF (iAcidLabWarpState = 0)
			IF (MPGlobalsAmbience.bAcidLabIsStuck)
				PRINTLN("[ACID_LAB_STUCK]  UPDATE_ACID_LAB_STUCK_CHECKS - my trailer is stuck")
				timeAcidLabWarp = GET_NETWORK_TIME_ACCURATE()
				timeAcidLabLeaveEvent = GET_NETWORK_TIME_ACCURATE()
				
				RequestControlOfAcidLab()
				
				// broadcast event to print help to all players who were inside truck or cab
				
				PLAYER_INDEX PlayerID
				INT iPlayerFlags
				REPEAT NUM_NETWORK_PLAYERS i
					PlayerID = INT_TO_PLAYERINDEX(i)
					IF IS_NET_PLAYER_OK(PlayerID, FALSE)
						IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(PlayerID), TruckID)
						OR IS_PLAYER_INSIDE_MY_ACID_LAB(PlayerID)
							SET_BIT(iPlayerFlags , i)
						ENDIF
					ENDIF
				ENDREPEAT
				
				BROADCAST_GENERAL_EVENT(GENERAL_EVENT_MOVING_ACID_LAB, iPlayerFlags)
				iAcidLabWarpState++
			ENDIF
		ENDIF
	

		// wait for everyone to be out
		IF (iAcidLabWarpState = 1)

			RequestControlOfAcidLab()
			KeepEveryoneOutMyAcidLab()
			
			IF NOT IS_ANYONE_INSIDE_MY_ACID_LAB()
			AND DOES_ENTITY_EXIST(TruckID)
			AND IS_VEHICLE_EMPTY(TruckID, TRUE, TRUE, FALSE, FALSE, FALSE, TRUE, TRUE)
			
				iDiff = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), timeAcidLabWarp))
				PRINTLN("[ACID_LAB_STUCK]  UPDATE_ACID_LAB_STUCK_CHECKS - waiting for everyone to be kicked out. iDiff = ", iDiff)
				IF (iDiff > 10000)
				
					IF IS_ANY_PLAYER_TOUCHING_VEHICLE(TruckID)
						PRINTLN("[ACID_LAB_STUCK]  UPDATE_ACID_LAB_STUCK_CHECKS - IS_ANY_PLAYER_TOUCHING_VEHICLE = TRUE")
					ELSE
					
						vAcidLabWarpCoords = GET_ENTITY_COORDS(TruckID, FALSE)
						PRINTLN("[ACID_LAB_STUCK]  UPDATE_ACID_LAB_STUCK_CHECKS - vAcidLabWarpCoords = ", vAcidLabWarpCoords)
											
						IF IS_VEHICLE_DRIVEABLE(TruckID)
							//IF IS_VEHICLE_ATTACHED_TO_TRAILER(TruckID)
								bShouldWarpAcidLab = TRUE
								//iAcidLabQuickWarpState = 0
								//PRINTLN("[ACID_LAB_STUCK]  UPDATE_ACID_LAB_STUCK_CHECKS - setting iAcidLabQuickWarpState")
							//ENDIF
						ENDIF		
						
						iAcidLabWarpState++
					ENDIF
					
				ELSE
					// display help 
				ENDIF
			ENDIF
		ENDIF
		
		// get safe spawn position
		IF (iAcidLabWarpState = 2)
		
			RequestControlOfAcidLab()
			KeepEveryoneOutMyAcidLab()
		
			VEHICLE_SPAWN_LOCATION_PARAMS Params
			Params.fMinDistFromCoords = 20.0
			Params.fMaxDistance = 250.0
			Params.bConsiderHighways = TRUE
			Params.bCheckEntityArea = TRUE
			Params.bCheckOwnVisibility = FALSE
			Params.bIsForPV = TRUE
			Params.bIgnoreCustomNodesForArea = TRUE	
			
			IF NOT (bShouldWarpAcidLab)
				IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(vAcidLabWarpCoords, <<0.0, 0.0, 0.0>>, GET_ACID_LAB_MODEL(), FALSE, vAcidLabWarpCoords, fAcidLabWarpHeading, Params)
					vAcidLabWarpCoords.z += 2.0						
					iAcidLabWarpState++
					PRINTLN("[ACID_LAB_STUCK]  UPDATE_ACID_LAB_STUCK_CHECKS - warping trailer only to ", vAcidLabWarpCoords, ", ", fAcidLabWarpHeading)
				ENDIF
			ELSE
				IF IS_VEHICLE_DRIVEABLE(TruckID)
				//AND IS_VEHICLE_ATTACHED_TO_TRAILER(TruckID)	
					
					ADD_CUSTOM_NODES_FOR_ALL_TRUCK_SPAWNS()
					Params.fMaxDistance= 9999.0	
				
					IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(vAcidLabWarpCoords, <<0.0, 0.0, 0.0>>, GET_ACID_LAB_MODEL(), FALSE, vAcidLabWarpCoords, fAcidLabWarpHeading, Params)
						CLEAR_CUSTOM_VEHICLE_NODES()
						iAcidLabWarpState++
						PRINTLN("[ACID_LAB_STUCK]  UPDATE_ACID_LAB_STUCK_CHECKS - warping truck and trailer to ", vAcidLabWarpCoords, ", ", fAcidLabWarpHeading)						
					ENDIF
				ELSE
					CLEANUP_HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS()
					bShouldWarpAcidLab = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		// warp to safe spawn position
		IF (iAcidLabWarpState = 3)
			
			RequestControlOfAcidLab()
			KeepEveryoneOutMyAcidLab()
			
			IF NOT IS_ENTITY_DEAD(TruckID)
				IF NOT (bShouldWarpAcidLab)
				
					IF NETWORK_HAS_CONTROL_OF_ENTITY(TruckID)
						
						IF NOT IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(TruckID)

							SET_ENTITY_ROTATION(TruckID, <<0.0, 0.0, 0.0>>)
							SET_ENTITY_HEADING(TruckID, fAcidLabWarpHeading)
							SET_ENTITY_COORDS(TruckID, vAcidLabWarpCoords)	
							IF SET_VEHICLE_ON_GROUND_PROPERLY(TruckID)
								PRINTLN("[ACID_LAB_STUCK]  UPDATE_ACID_LAB_STUCK_CHECKS - managed to set trailer on ground properly ", vAcidLabWarpCoords, ", ", fAcidLabWarpHeading)						
							ELSE
								PRINTLN("[ACID_LAB_STUCK]  UPDATE_ACID_LAB_STUCK_CHECKS - didn't managed to set trailer on ground properly ", vAcidLabWarpCoords, ", ", fAcidLabWarpHeading)	
							ENDIF
							
							SET_ACID_LAB_LOCK_STATE(TruckID) // restore lock state
							iAcidLabWarpState++
							
						ELSE
							DETACH_ENTITY(TruckID, FALSE)
							PRINTLN("[ACID_LAB_STUCK]  UPDATE_ACID_LAB_STUCK_CHECKS - trying to warp trailer, but its attached to something.")	
						ENDIF
					ELSE
						PRINTLN("[ACID_LAB_STUCK]  UPDATE_ACID_LAB_STUCK_CHECKS - trying to warp trailer, but not got control.")	
					ENDIF
				ELSE
					
//					IF DoTrailerQuickWarp(iHackerTruckQuickWarpState, vHackerTruckWarpCoords, fHackerTruckWarpHeading, TruckID)
//						
//						IF NOT IS_ENTITY_DEAD(TruckID)
//							IF NETWORK_HAS_CONTROL_OF_ENTITY(TruckID)
//								SET_HACKER_TRUCK_LOCK_STATE(TruckID) // restore lock state
//							ENDIF
//						ENDIF
//
//						PRINTLN("[MOC_STUCK]  UPDATE_MOBILE_CONTROL_CENTRE_STUCK_CHECKS - DoTrailerQuickWarp finished.")
//						iHackerTruckWarpState++
//					
//					ELSE
//						PRINTLN("[MOC_STUCK]  UPDATE_MOBILE_CONTROL_CENTRE_STUCK_CHECKS - waiting for DoTrailerQuickWarp().")
//					ENDIF		
				ENDIF
			ELSE
				CLEANUP_MP_SAVED_ACIDLAB(FALSE, FALSE, TRUE, FALSE, TRUE, FALSE, FALSE) 
				PRINTLN("[ACID_LAB_STUCK]  UPDATE_ACID_LAB_STUCK_CHECKS - trailer is dead")	
				iAcidLabWarpState++
			ENDIF
				
		ENDIF
		
		// cleanup
		IF (iAcidLabWarpState = 4)
		
			RequestControlOfAcidLab()
		
			IF NOT IS_ENTITY_DEAD(TruckID)
				IF DOES_VEHICLE_HAVE_STUCK_VEHICLE_CHECK(TruckID)				
				AND NETWORK_HAS_CONTROL_OF_ENTITY(TruckID)
					RESET_VEHICLE_STUCK_TIMER(TruckID, VEH_STUCK_ON_ROOF)
					RESET_VEHICLE_STUCK_TIMER(TruckID, VEH_STUCK_ON_SIDE)
					RESET_VEHICLE_STUCK_TIMER(TruckID, VEH_STUCK_HUNG_UP)
					RESET_VEHICLE_STUCK_TIMER(TruckID, VEH_STUCK_JAMMED)
				ENDIF
				MPGlobalsAmbience.bAcidLabMovedHelp = TRUE
			ENDIF
			
			iAcidLabWarpState = 0
			bShouldWarpAcidLab = FALSE
			MPGlobalsAmbience.bAcidLabIsStuck = FALSE
			MPGlobalsAmbience.bAcidLabShouldLeave = FALSE
			
			PRINTLN("[ACID_LAB_STUCK]  UPDATE_ACID_LAB_STUCK_CHECKS - cleaned up.")
		ENDIF
	
	ELSE
		IF (MPGlobalsAmbience.bAcidLabIsStuck)			
			PRINTLN("[ACID_LAB_STUCK]  UPDATE_ACID_LAB_STUCK_CHECKS - in bunker, resetting.")			
			iAcidLabWarpState = 0
			bShouldWarpAcidLab = FALSE
			MPGlobalsAmbience.bAcidLabIsStuck = FALSE	
			MPGlobalsAmbience.bAcidLabMovedHelp = FALSE
			MPGlobalsAmbience.bAcidLabMovingHelp = FALSE
			MPGlobalsAmbience.bAcidLabShouldLeave = FALSE
		ENDIF
	ENDIF
	
	// print help saying that we are moving.
	IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSSeven, BS_SIMPLE_INTERIOR_GLOBAL_BS_SEVEN_REMOTE_STORED_ACID_LAB_RETURNED_TO_GARAGE)
		IF NOT (MPGlobalsAmbience.bAcidLabMovedHelp)
			IF (MPGlobalsAmbience.bAcidLabMovingHelp)
				IF SAFE_TO_PRINT_PV_HELP()
				AND NOT IS_PLAYER_IN_ACID_LAB(PLAYER_ID())
				AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
					PRINT_HELP("MOVE_ACIDLAB", 10000)
					MPGlobalsAmbience.bAcidLabMovingHelp = FALSE 
					PRINTLN("[ACID_LAB_STUCK] printing help message ")
				ELSE	
					PRINTLN("[ACID_LAB_STUCK] waiting to print help message ")
				ENDIF
			ENDIF
		ELSE
			IF SAFE_TO_PRINT_PV_HELP()
			AND NOT IS_PLAYER_IN_ACID_LAB(PLAYER_ID())
			AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
				PRINT_HELP("MOVED_ACIDLAB", 7000)
				MPGlobalsAmbience.bAcidLabMovedHelp = FALSE 
				PRINTLN("[ACID_LAB_STUCK] printing help message (finished) ")
			ELSE	
				PRINTLN("[ACID_LAB_STUCK] waiting to print help message (finisheD) ")
			ENDIF	
		ENDIF
	ELSE
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MOVED_ACIDLAB")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MOVE_ACIDLAB")
			CLEAR_HELP()
			MPGlobalsAmbience.bAcidLabMovingHelp = FALSE
		ENDIF
	ENDIF

ENDPROC

PROC UPDATE_WARP_ACID_LAB_NEARBY()

	VEHICLE_INDEX TruckID
	IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV)
		TruckID = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV)
	ENDIF
	
	IF HAS_PERSONAL_ACID_LAB_BEEN_REQUESTED_TO_WARP_NEAR()
	AND iAcidLabWarpState = 0
		// Do we need to move it?
		IF (iAcidLabWarpNearbyState = 0)
			PRINTLN("[ACID_LAB_WARP]  UPDATE_WARP_ACID_LAB_NEARBY - my trailer is stuck")
			RequestControlOfAcidLab()
			iAcidLabWarpNearbyState++
		ENDIF	

		// wait for control of the vehicle
		IF (iAcidLabWarpNearbyState = 1)

			RequestControlOfAcidLab()
			
			IF DOES_ENTITY_EXIST(TruckID)
			AND IS_VEHICLE_EMPTY(TruckID, TRUE, TRUE, FALSE, FALSE, FALSE, TRUE, TRUE)
			AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV)
				NETWORK_FADE_OUT_ENTITY(TruckID, TRUE, TRUE)
				vAcidLabWarpCoords = MPGlobals.VehicleData.vOverrideSecondaryPVWarpRequestCoord
				PRINTLN("[ACID_LAB_WARP]  UPDATE_WARP_ACID_LAB_NEARBY - Warp requested near = ", vAcidLabWarpCoords)
				
				iAcidLabWarpNearbyState++
			ENDIF
		ENDIF
		
		// get safe spawn position
		IF (iAcidLabWarpNearbyState = 2)
		
			RequestControlOfAcidLab()
		
			VEHICLE_SPAWN_LOCATION_PARAMS Params
			Params.fMinDistFromCoords = 20.0
			Params.fMaxDistance = 150.0
			Params.bConsiderHighways = TRUE
			Params.bCheckEntityArea = TRUE
			Params.bCheckOwnVisibility = FALSE
			Params.bIsForPV = TRUE
			Params.bConsiderOnlyActiveNodes = TRUE
			
			IF IS_VEHICLE_DRIVEABLE(TruckID)
				Params.fMaxDistance= 9999.0	
			
				IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(vAcidLabWarpCoords, <<0.0, 0.0, 0.0>>, GET_ACID_LAB_MODEL(), FALSE, vAcidLabWarpCoords, fAcidLabWarpHeading, Params)					
					iAcidLabWarpNearbyState++
					PRINTLN("[ACID_LAB_WARP]  UPDATE_WARP_ACID_LAB_NEARBY - warping truck to ", vAcidLabWarpCoords, ", ", fAcidLabWarpHeading)						
				ENDIF
			ELSE
				CLEANUP_MP_SAVED_ACIDLAB(FALSE, FALSE, TRUE, FALSE, TRUE, FALSE, FALSE) 
				PRINTLN("[ACID_LAB_WARP]  UPDATE_WARP_ACID_LAB_NEARBY - truck is not driveable")	
				iAcidLabWarpNearbyState += 2
				CLEANUP_HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS()
			ENDIF
		ENDIF
		
		// warp to safe spawn position
		IF (iAcidLabWarpNearbyState = 3)
			
			RequestControlOfAcidLab()
			
			IF NOT IS_ENTITY_DEAD(TruckID)
				
				IF NETWORK_HAS_CONTROL_OF_ENTITY(TruckID)
					
					IF NOT IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(TruckID)

						SET_ENTITY_ROTATION(TruckID, <<0.0, 0.0, 0.0>>)
						SET_ENTITY_HEADING(TruckID, fAcidLabWarpHeading)
						SET_ENTITY_COORDS(TruckID, vAcidLabWarpCoords)	
						IF SET_VEHICLE_ON_GROUND_PROPERLY(TruckID)
							PRINTLN("[ACID_LAB_WARP]  UPDATE_WARP_ACID_LAB_NEARBY - managed to set trailer on ground properly ", vAcidLabWarpCoords, ", ", fAcidLabWarpHeading)						
						ELSE
							PRINTLN("[ACID_LAB_WARP]  UPDATE_WARP_ACID_LAB_NEARBY - didn't managed to set trailer on ground properly ", vAcidLabWarpCoords, ", ", fAcidLabWarpHeading)	
						ENDIF
						
						NETWORK_FADE_IN_ENTITY(TruckID, TRUE, TRUE)
						SET_ACID_LAB_LOCK_STATE(TruckID) // restore lock state
						
						iAcidLabWarpNearbyState++
						
					ELSE
						DETACH_ENTITY(TruckID, FALSE)
						PRINTLN("[ACID_LAB_WARP]  UPDATE_WARP_ACID_LAB_NEARBY - trying to warp trailer, but its attached to something.")	
					ENDIF
				ELSE
					PRINTLN("[ACID_LAB_WARP]  UPDATE_WARP_ACID_LAB_NEARBY - trying to warp trailer, but not got control.")	
				ENDIF
			ELSE
				CLEANUP_MP_SAVED_ACIDLAB(FALSE, FALSE, TRUE, FALSE, TRUE, FALSE, FALSE) 
				PRINTLN("[ACID_LAB_WARP]  UPDATE_WARP_ACID_LAB_NEARBY - trailer is dead")	
				iAcidLabWarpNearbyState++
			ENDIF
				
		ENDIF
		
		// cleanup
		IF (iAcidLabWarpNearbyState = 4)
			iAcidLabWarpNearbyState = 0
			CLEAR_REQUEST_TO_WARP_PERSONAL_ACID_LAB_NEAR()
			PRINTLN("[ACID_LAB_WARP]  UPDATE_WARP_ACID_LAB_NEARBY - cleaned up.")
		ENDIF
	ENDIF	

ENDPROC
#ENDIF

#IF FEATURE_HEIST_ISLAND

BOOL bSubmarineIsDiving
INT iDiveStartTime
BOOL bStartedFadeOut
PED_INDEX DummyDriverId

PROC CREATE_SUB_DUMMY_DRIVER()
	VEHICLE_INDEX subId = GET_SUBMARINE_VEHICLE(PLAYER_ID())
	// create a dummy driver
	IF NOT DOES_ENTITY_EXIST(DummyDriverId)
		REQUEST_MODEL(GET_PLAYER_MODEL_FOR_TEAM(GET_STAT_CHARACTER_TEAM()))
		DummyDriverId = CREATE_PED(PEDTYPE_CIVMALE, GET_PLAYER_MODEL_FOR_TEAM(GET_STAT_CHARACTER_TEAM()), GET_ENTITY_COORDS(SubId), 0.0, TRUE, FALSE)
		WarpPlayerIntoCar(DummyDriverId, SubId, VS_DRIVER, FALSE)
		PRINTLN("[SUBMARINE_HELM] CREATE_SUB_DUMMY_DRIVER - created dummy driver ")
	ENDIF		
ENDPROC

PROC DELETE_SUB_DUMMY_DRIVER()
	IF DOES_ENTITY_EXIST(DummyDriverId)
		IF NOT IS_ENTITY_DEAD(DummyDriverId)
			CLEAR_DEFAULT_PRIMARY_TASK(DummyDriverId)
		ENDIF
		DELETE_PED(DummyDriverId)
		PRINTLN("[SUBMARINE_HELM] DELETE_SUB_DUMMY_DRIVER - deleting dummy driver ")
	ENDIF
ENDPROC

PROC TASK_SUBMARINE_DIVE(FLOAT fDepth)

	VEHICLE_INDEX submarineVeh = GET_SUBMARINE_VEHICLE(PLAYER_ID())
	NETWORK_INDEX subNetId = GET_SUBMARINE_NET_ID(PLAYER_ID())

	// have we already been given the task? check if we lost it
	IF (bSubmarineIsDiving)
		IF DOES_ENTITY_EXIST(submarineVeh)
		AND NOT IS_ENTITY_DEAD(submarineVeh)
		
			SET_BOAT_ANCHOR(submarineVeh, FALSE) // call each frame
	
			IF NOT (GET_ACTIVE_VEHICLE_MISSION_TYPE(submarineVeh) = MISSION_GOTO)
				PRINTLN("TASK_SUBMARINE_DIVE - lost task")
				bSubmarineIsDiving = FALSE	
			ENDIF
			
			// fade out when sufficiently under water
			IF ((GET_GAME_TIMER() - iDiveStartTime) > 7000)
				IF NOT (bStartedFadeOut)
					PRINTLN("TASK_SUBMARINE_DIVE -  start fading out")
					NETWORK_FADE_OUT_ENTITY(submarineVeh, FALSE, TRUE)
					bStartedFadeOut = TRUE
				ENDIF
			ENDIF
			
		ELSE
			PRINTLN("TASK_SUBMARINE_DIVE - entity dead or not exist")
			bSubmarineIsDiving = FALSE
		ENDIF
	ENDIF
	

	IF NOT (bSubmarineIsDiving)
		
		IF DOES_ENTITY_EXIST(submarineVeh)
		AND NOT IS_ENTITY_DEAD(submarineVeh)
		
			SET_NETWORK_ID_CAN_MIGRATE(subNetId, FALSE)
			
			VECTOR vTargetCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(submarineVeh, <<0.0, 500.0, fDepth>>)
		
			SET_BOAT_ANCHOR(submarineVeh, FALSE)
			CLEAR_PRIMARY_VEHICLE_TASK(submarineVeh)	
			FORCE_SUBMARINE_NEUTRAL_BUOYANCY(submarineVeh, 0)
			TASK_SUBMARINE_GOTO_AND_STOP(NULL , submarineVeh, vTargetCoords, TRUE)
			SET_VEHICLE_DONT_TERMINATE_TASK_WHEN_ACHIEVED(submarineVeh)
			SET_DISABLE_SUPERDUMMY(submarineVeh, TRUE)
			PRINTLN("[SUBMARINE_HELM] TASK_SUBMARINE_DIVE - depth ", fDepth)
			
			bStartedFadeOut = FALSE
			bSubmarineIsDiving = TRUE
			iDiveStartTime = GET_GAME_TIMER()
		
		ENDIF
			
		
	ENDIF
ENDPROC

PROC DEACTIVATE_SUBMARINE_DIVE()
	IF (bSubmarineIsDiving)
	
		VEHICLE_INDEX submarineVeh = GET_SUBMARINE_VEHICLE(PLAYER_ID())
		NETWORK_INDEX subNetId = GET_SUBMARINE_NET_ID(PLAYER_ID())	
		
		DELETE_SUB_DUMMY_DRIVER()
		
		IF DOES_ENTITY_EXIST(submarineVeh)
			SET_BOAT_ANCHOR(submarineVeh, FALSE)
			CLEAR_PRIMARY_VEHICLE_TASK(submarineVeh)		
			SET_DISABLE_SUPERDUMMY(submarineVeh, FALSE)
			FORCE_SUBMARINE_NEUTRAL_BUOYANCY(submarineVeh, 0)
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(submarineVeh, FALSE)
			NETWORK_FADE_IN_ENTITY(submarineVeh, TRUE, FALSE)
			SET_NETWORK_ID_CAN_MIGRATE(subNetId, TRUE)		
			PRINTLN("[SUBMARINE_HELM] DEACTIVATE_SUBMARINE_DIVE")			
		ENDIF
		
		bStartedFadeOut = FALSE
		bSubmarineIsDiving = FALSE
	ENDIF
ENDPROC

FUNC BOOL TakeControlOfSub()
	VEHICLE_INDEX SubId = GET_SUBMARINE_VEHICLE(PLAYER_ID())
	NETWORK_REQUEST_CONTROL_OF_ENTITY(SubId)
	IF NETWORK_HAS_CONTROL_OF_ENTITY(SubId)	
		
		IF NOT IS_ENTITY_A_MISSION_ENTITY(SubId)
			SET_ENTITY_AS_MISSION_ENTITY(SubId,FALSE,TRUE)
			PRINTLN("TakeControlOfSub - setting as mission entity") 
		ENDIF
	
		IF IS_ENTITY_A_MISSION_ENTITY(SubId)
			IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(SubId)						
				SET_ENTITY_AS_MISSION_ENTITY(SubId,FALSE,TRUE)
				PRINTLN("TakeControlOfSub - setting as mission entity for this script") 
			ENDIF
			
			IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(SubId)
				RETURN TRUE	
			ENDIF
		ENDIF
	ELSE
		PRINTLN("TakeControlOfSub - waiting for control") 	
	ENDIF
	
	RETURN FALSE
ENDFUNC

BOOL bHasStartedForceDive
INT iWarpSubmarineGameTime
INT iSubWarpAttempt
VECTOR vAdjustedSubWarpCoords
PROC MAINTAIN_WARP_SUBMARINE_TO_COORDS()
	
	VEHICLE_INDEX SubId
	//NETWORK_INDEX NetId
	INT iElapsedTime
	VECTOR vPos
	VECTOR vVel
	VECTOR vOutToSea
	FLOAT fRotateAngle
	FLOAT fDist
	
	IF (MPGlobalsAmbience.iWarpSubmarineState > 0)
		
		PRINTLN("MAINTAIN_WARP_SUBMARINE_TO_COORDS - active...") 
				
		SubId = GET_SUBMARINE_VEHICLE(PLAYER_ID())
		//NetId = GET_SUBMARINE_NET_ID(PLAYER_ID())
		
		IF DOES_ENTITY_EXIST(SubId)
		AND NOT IS_ENTITY_DEAD(SubId)
			IF NETWORK_GET_ENTITY_IS_NETWORKED(SubId)
			

				iElapsedTime = (GET_GAME_TIMER() - iWarpSubmarineGameTime)
								
	
				// start warp
				IF (MPGlobalsAmbience.iWarpSubmarineState = 1)				
					IF TakeControlOfSub()
					
						// move further out to sea if this is a subsequent attempt
						IF (iSubWarpAttempt > 0)
							vOutToSea = <<MPGlobalsAmbience.vSubmarineWarpPos.x, MPGlobalsAmbience.vSubmarineWarpPos.y, 0.0>>
							vOutToSea /= VMAG(vOutToSea)									
							// in case 2 players get stuck trying to warp to same place fan them out according to player id
							fRotateAngle = -45.0 + (NATIVE_TO_INT(PLAYER_ID()) * 90.0/NUM_NETWORK_PLAYERS)
							RotateVec(vOutToSea, <<0,0, fRotateAngle >>)
							PRINTLN("MAINTAIN_WARP_SUBMARINE_TO_COORDS - fRotateAngle = ", fRotateAngle, ", player id = ", NATIVE_TO_INT(PLAYER_ID()))
							vOutToSea *= (iSubWarpAttempt * SUB_OUT_TO_SEA_DIST)
						ELSE
							vOutToSea = <<0,0,0>>
						ENDIF
						
						vAdjustedSubWarpCoords = MPGlobalsAmbience.vSubmarineWarpPos + vOutToSea									
						
						FREEZE_ENTITY_POSITION(SubId, FALSE)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(SubId, TRUE)
						SET_CLEAR_FREEZE_WAITING_ON_COLLISION_ONCE_PLAYER_ENTERS(SubId, TRUE)	
						SET_BOAT_ANCHOR(SubId, FALSE)	
						CLEAR_AREA_OF_PEDS(vAdjustedSubWarpCoords, 50.0)
						CLEAR_AREA_OF_VEHICLES(vAdjustedSubWarpCoords, 50.0)	
						SET_ENTITY_COORDS_NO_OFFSET(SubId, vAdjustedSubWarpCoords)
						SET_ENTITY_HEADING(SubId, MPGlobalsAmbience.fSubmarineWarpHeading)
						
						// switch on respot timer, allows us some time to seperate any subs
						//CREATE_SUB_DUMMY_DRIVER()
						//SET_NETWORK_VEHICLE_RESPOT_TIMER(NetId, 5000, TRUE, TRUE)
															
						iWarpSubmarineGameTime = GET_GAME_TIMER()
						MPGlobalsAmbience.iWarpSubmarineState++																	
						PRINTLN("MAINTAIN_WARP_SUBMARINE_TO_COORDS - teleported sub, warp pos = ", MPGlobalsAmbience.vSubmarineWarpPos, " iSubWarpAttempt = ", iSubWarpAttempt, " adjusted warp coords = ", vAdjustedSubWarpCoords)

					ENDIF				
				ENDIF
					
					
				// wait for warp to 'happen' - dont request control for this part, as relinquish control seems to perform the position update
				IF (MPGlobalsAmbience.iWarpSubmarineState = 2)
					
					iElapsedTime = GET_GAME_TIMER() - iWarpSubmarineGameTime
					
					PRINTLN("MAINTAIN_WARP_SUBMARINE_TO_COORDS - waiting for warp, NETWORK_HAS_CONTROL_OF_ENTITY = ", NETWORK_HAS_CONTROL_OF_ENTITY(SubId))
					
					vPos = GET_ENTITY_COORDS(SubId, FALSE)
					vVel = GET_ENTITY_VELOCITY(SubId)
					fDist = VDIST(<<vAdjustedSubWarpCoords.x, vAdjustedSubWarpCoords.y, 0.0>>, <<vPos.x, vPos.y, 0.0>>)
					
					PRINTLN("MAINTAIN_WARP_SUBMARINE_TO_COORDS - sub pos = ", vPos, ", iElapsedTime = ", iElapsedTime, " vel = ", vVel, " fDist = ", fDist)																											
																							
					// wait for some time to elapse then check if the sub warped ok
					IF (iElapsedTime > SUB_WARP_WAIT_TIME)	// takes about 3 secs to resolve the collision, we'll give 4	
					OR (VMAG(vVel) > 0.0)
					OR (fDist > 0.0)
						IF (iSubWarpAttempt < 5)
						AND (fDist > SUB_OUT_TO_SEA_DIST)												
							iSubWarpAttempt++
							MPGlobalsAmbience.iWarpSubmarineState = 1	
							PRINTLN("MAINTAIN_WARP_SUBMARINE_TO_COORDS - warp never happened, trying again, attempt ", iSubWarpAttempt)
						ELSE
					
							PRINTLN("MAINTAIN_WARP_SUBMARINE_TO_COORDS - warp has happened")
							MPGlobalsAmbience.iWarpSubmarineState++
						ENDIF						
					ENDIF
					
				ENDIF
				
					
				// we're done tidy up
				IF (MPGlobalsAmbience.iWarpSubmarineState = 3)
					
					IF TakeControlOfSub()
						SET_BOAT_ANCHOR(SubId, FALSE)																														
						IF NOT (bSubmarineIsDiving)
							SET_ENTITY_VISIBLE(SubId, TRUE)
						ENDIF				
						DEACTIVATE_SUBMARINE_DIVE()					
						//DELETE_SUB_DUMMY_DRIVER()						
						PRINTLN("MAINTAIN_WARP_SUBMARINE_TO_COORDS - warp complete")
						MPGlobalsAmbience.iWarpSubmarineState = 0								
					ENDIF			

				ENDIF			
							
						
						
			ELSE
				PRINTLN("MAINTAIN_WARP_SUBMARINE_TO_COORDS - submarine is not networked") 	
				MPGlobalsAmbience.iWarpSubmarineState = 0	
			ENDIF
		ELSE
			PRINTLN("MAINTAIN_WARP_SUBMARINE_TO_COORDS - submarine does not exit") 	
			MPGlobalsAmbience.iWarpSubmarineState = 0
		ENDIF

		
	ELSE
	
		// reset stuff
		iSubWarpAttempt = 0
		//DELETE_SUB_DUMMY_DRIVER()
		

		IF (MPGlobalsAmbience.bForceDiveSubmarine)
		
			PRINTLN("MAINTAIN_WARP_SUBMARINE_TO_COORDS - force dive is active") 
		
			SubId = GET_SUBMARINE_VEHICLE(PLAYER_ID())
			IF DOES_ENTITY_EXIST(SubId)
			AND NOT IS_ENTITY_DEAD(SubId)
				IF NETWORK_GET_ENTITY_IS_NETWORKED(SubId)
					NETWORK_REQUEST_CONTROL_OF_ENTITY(SubId)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(SubId)						
						
						IF NOT IS_ENTITY_A_MISSION_ENTITY(SubId)
							SET_ENTITY_AS_MISSION_ENTITY(SubId,FALSE,TRUE)
							PRINTLN("MAINTAIN_WARP_SUBMARINE_TO_COORDS - force dive, setting as mission entity") 
						ENDIF
					
						IF IS_ENTITY_A_MISSION_ENTITY(SubId)
						AND NOT IS_ENTITY_DEAD(SubId)
						
							IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(SubId)						
								SET_ENTITY_AS_MISSION_ENTITY(SubId,FALSE,TRUE)
								PRINTLN("MAINTAIN_WARP_SUBMARINE_TO_COORDS - force dive, setting as mission entity for this script") 
							ENDIF
							
							IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(SubId)		
																						
								TASK_SUBMARINE_DIVE(MPGlobalsAmbience.fForceDiveDepth)
								bHasStartedForceDive = TRUE						
							
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF


			MPGlobalsAmbience.bForceDiveSubmarine = FALSE // set back to false, meaning FORCE_DIVE_SUBMARINE will need to be called each frame
			
		ELSE
			IF (bHasStartedForceDive)	
					
				SubId = GET_SUBMARINE_VEHICLE(PLAYER_ID())
				IF DOES_ENTITY_EXIST(SubId)
				AND NOT IS_ENTITY_DEAD(SubId)
					IF NETWORK_GET_ENTITY_IS_NETWORKED(SubId)
						NETWORK_REQUEST_CONTROL_OF_ENTITY(SubId)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(SubId)	
							
							IF NOT IS_ENTITY_A_MISSION_ENTITY(SubId)
								SET_ENTITY_AS_MISSION_ENTITY(SubId,FALSE,TRUE)
								PRINTLN("MAINTAIN_WARP_SUBMARINE_TO_COORDS - force dive, setting as mission entity") 
							ENDIF
						
							IF IS_ENTITY_A_MISSION_ENTITY(SubId)
							AND NOT IS_ENTITY_DEAD(SubId)
							
								IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(SubId)						
									SET_ENTITY_AS_MISSION_ENTITY(SubId,FALSE,TRUE)
									PRINTLN("MAINTAIN_WARP_SUBMARINE_TO_COORDS - force dive, setting as mission entity for this script") 
								ENDIF
								
								IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(SubId)										
									bHasStartedForceDive = FALSE
									DEACTIVATE_SUBMARINE_DIVE()
									PRINTLN("MAINTAIN_WARP_SUBMARINE_TO_COORDS - clearing bHasStartedForceDive") 
								ENDIF
							ENDIF
						ENDIF
					ELSE
						PRINTLN("MAINTAIN_WARP_SUBMARINE_TO_COORDS - clearing bHasStartedForceDive - not networked") 
						DEACTIVATE_SUBMARINE_DIVE()
						bHasStartedForceDive = FALSE
					ENDIF
				ELSE
					PRINTLN("MAINTAIN_WARP_SUBMARINE_TO_COORDS - clearing bHasStartedForceDive - does not exist / dead") 
					DEACTIVATE_SUBMARINE_DIVE()
					bHasStartedForceDive = FALSE	
				ENDIF
		 
			ENDIF
		ENDIF
	
	ENDIF
	

ENDPROC

PROC MAINTAIN_SUBMARINE_FIRST_TIME_REVEAL()
	
	#IF IS_DEBUG_BUILD
	IF bFMSubmarineDebug_ResetFirstReveal
		CLEAR_BIT(iLocalBS, LOCAL_BS_SUBMARINE_FIRST_REVEAL)
	ENDIF
	#ENDIF
	
	IF IS_BIT_SET(iLocalBS, LOCAL_BS_SUBMARINE_FIRST_REVEAL)
		EXIT
	ENDIF	
	
	VEHICLE_INDEX SubVeh = GET_SUBMARINE_VEHICLE(PLAYER_ID())	
	VECTOR vPos
	
	IF NOT DOES_ENTITY_EXIST(SubVeh)
		EXIT
	ENDIF	
	
	#IF IS_DEBUG_BUILD
	IF bFMSubmarineDebug_ResetFirstReveal
		bFMSubmarineDebug_ResetFirstReveal = FALSE
		CLEAR_BIT(iLocalBS, LOCAL_BS_SUBMARINE_FIRST_REVEAL)	
		FLOAT fHeading
		IF DOES_ENTITY_EXIST(SubVeh)
		AND NOT IS_ENTITY_DEAD(SubVeh)
			vPos = GET_ENTITY_COORDS(SubVeh, FALSE)
			fHeading = GET_ENTITY_HEADING(SubVeh)
		ENDIF
		vPos.z = SUBMARINE_SPAWN_DEPTH
		REQUEST_WARP_SUBMARINE(vPos, fHeading)
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_KOSATKA_FIRST_REVEAL, FALSE)
	ENDIF
	
	IF (bFMSub_Destroy)
		SubVeh = GET_SUBMARINE_VEHICLE(PLAYER_ID())		
		IF DOES_ENTITY_EXIST(SubVeh)
		AND NOT IS_ENTITY_DEAD(SubVeh)
			IF NETWORK_GET_ENTITY_IS_NETWORKED(SubVeh)
				NETWORK_REQUEST_CONTROL_OF_ENTITY(SubVeh)
			ENDIF	
			IF NETWORK_HAS_CONTROL_OF_ENTITY(SubVeh)
				NETWORK_EXPLODE_VEHICLE(SubVeh)
				bFMSub_Destroy = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF (bFMSub_TestNewCollisionDetection)
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)	
			
		IF DOES_ENTITY_EXIST(SubVeh)
		AND NOT IS_ENTITY_DEAD(SubVeh)
			
			FLOAT fHeading = GET_ENTITY_HEADING(SubVeh)
			vPos = GET_ENTITY_COORDS(SubVeh)
			VECTOR vCoords1, vCoords2
			FLOAT fWidth		
			
			// create dummy sub
			IF NOT DOES_ENTITY_EXIST(DummySub)
				REQUEST_MODEL(KOSATKA)
				IF HAS_MODEL_LOADED(KOSATKA)
					vDummySubPos = vPos
					vDummySubPos.z += 40.0
					fDummySubHeading = fHeading
					DummySub = CREATE_VEHICLE(KOSATKA, vDummySubPos,fDummySubHeading, FALSE, FALSE)
					SET_ENTITY_COLLISION(DummySub, FALSE)
					SET_ENTITY_INVINCIBLE(DummySub, TRUE)
					FREEZE_ENTITY_POSITION(DummySub, TRUE)
				ENDIF
			ENDIF
		
			// update dummy sub
			IF DOES_ENTITY_EXIST(DummySub)				
				SET_ENTITY_COORDS_NO_OFFSET(DummySub, vDummySubPos)
				SET_ENTITY_HEADING(DummySub, fDummySubHeading)							
			ENDIF
			
			BOOL bMainSubCollides = DoVehicleBoundsOverlap(vPos, fHeading, KOSATKA, vDummySubPos, fDummySubHeading, KOSATKA)
			BOOL bDummySubCollides = DoVehicleBoundsOverlap(vDummySubPos, fDummySubHeading, KOSATKA,vPos, fHeading, KOSATKA)
			
			
			// draw angled areas and result
			GetAngledAreaForModel(vPos, fHeading, KOSATKA, vCoords1, vCoords2, fWidth)
			IF (bMainSubCollides)
				DRAW_DEBUG_ANGLED_AREA(vCoords1, vCoords2, fWidth, 255,15,15,128)
			ELSE
				DRAW_DEBUG_ANGLED_AREA(vCoords1, vCoords2, fWidth, 15,200,200,128)
			ENDIF
			
			GetAngledAreaForModel(vDummySubPos, fDummySubHeading, KOSATKA, vCoords1, vCoords2, fWidth)
			IF (bDummySubCollides)
				DRAW_DEBUG_ANGLED_AREA(vCoords1, vCoords2, fWidth, 255,15,15,128)
			ELSE
				DRAW_DEBUG_ANGLED_AREA(vCoords1, vCoords2, fWidth, 15,200,200,128)
			ENDIF
		
		ENDIF
	ELSE
		IF DOES_ENTITY_EXIST(DummySub)
			DELETE_VEHICLE(DummySub)
		ENDIF	
	ENDIF
	
	#ENDIF
	
	IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_SUBMARINE_FIRST_REVEAL)
		IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_KOSATKA_FIRST_REVEAL)
			SET_BIT(iLocalBS, LOCAL_BS_SUBMARINE_FIRST_REVEAL) //already shown once
		ELSE
			//Hold the submarine at spawn depth until the player gets near
			IF DOES_ENTITY_EXIST(SubVeh)									
				IF IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
				OR IS_TRANSITION_ACTIVE()
					PRINTLN("[MAINTAIN_SUBMARINE_FIRST_TIME_REVEAL] Player in submarine, ignoring first reveal...")
									
					IF NETWORK_GET_ENTITY_IS_NETWORKED(SubVeh)
						NETWORK_REQUEST_CONTROL_OF_ENTITY(SubVeh)
					ENDIF
					
					IF NETWORK_HAS_CONTROL_OF_ENTITY(SubVeh)
						FREEZE_ENTITY_POSITION(SubVeh, FALSE)
						SET_BIT(iLocalBS, LOCAL_BS_SUBMARINE_FIRST_REVEAL)
						PRINTLN("[MAINTAIN_SUBMARINE_FIRST_TIME_REVEAL] unfrozen")
					ENDIF
				
				ELIF GET_DISTANCE_BETWEEN_ENTITIES(SubVeh, PLAYER_PED_ID()) < fRevealDist 
				
					IF NETWORK_GET_ENTITY_IS_NETWORKED(SubVeh)
						NETWORK_REQUEST_CONTROL_OF_ENTITY(SubVeh)
					ENDIF		
				
				
					IF IS_GAMEPLAY_HINT_ACTIVE()
						PRINTLN("[MAINTAIN_SUBMARINE_FIRST_TIME_REVEAL] Stopping gameplay hint...")
						STOP_GAMEPLAY_HINT()
					ELSE
						IF NETWORK_HAS_CONTROL_OF_ENTITY(SubVeh)
						
							FREEZE_ENTITY_POSITION(SubVeh, FALSE)
							FORCE_SUBMARINE_NEUTRAL_BUOYANCY(SubVeh, 0)
							
							vPos = GET_ENTITY_COORDS(SubVeh, FALSE)
							
							PRINTLN("[MAINTAIN_SUBMARINE_FIRST_TIME_REVEAL] vPos = ", vPos)
							
							IF (vPos.z > fRevealDepth)							

								PRINTLN("[MAINTAIN_SUBMARINE_FIRST_TIME_REVEAL] Revealing...")
																							
								SET_GAMEPLAY_ENTITY_HINT(SubVeh, vRevealOffset, DEFAULT, DEFAULT, DEFAULT, DEFAULT, INT_TO_ENUM(HINT_TYPE, iRevealHintType))
								
								PLAY_SOUND_FROM_COORD(-1, "Surface", GET_ENTITY_COORDS(SubVeh), "Submarine_First_Time_Surface", TRUE, CEIL(fRevealDist))
			
								SCRIPT_LOOK_FLAG LookFlags = SLF_WHILE_NOT_IN_FOV	//SLF_DEFAULT
								SCRIPT_LOOK_PRIORITY priority = SLF_LOOKAT_HIGH		//SLF_LOOKAT_MEDIUM
								TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), SubVeh, DEFAULT_DWELL_TIME, LookFlags, priority)																					
								
								SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_KOSATKA_FIRST_REVEAL, TRUE)
								SET_BIT(iLocalBS, LOCAL_BS_SUBMARINE_FIRST_REVEAL)
								
							ENDIF
						ELSE
							PRINTLN("[MAINTAIN_SUBMARINE_FIRST_TIME_REVEAL] waiting to take control")	
						ENDIF
					ENDIF
				ELSE					
					IF NETWORK_GET_ENTITY_IS_NETWORKED(SubVeh)
						NETWORK_REQUEST_CONTROL_OF_ENTITY(SubVeh)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(SubVeh)
							PRINTLN("[MAINTAIN_SUBMARINE_FIRST_TIME_REVEAL] Holding submarine underwater for first reveal...")
							FORCE_SUBMARINE_NEUTRAL_BUOYANCY(SubVeh, 5000)
							FREEZE_ENTITY_POSITION(SubVeh, TRUE)
						ELSE
							PRINTLN("[MAINTAIN_SUBMARINE_FIRST_TIME_REVEAL] waiting to take control (2)")	
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

#ENDIF

PROC MAINTAIN_PARTY_BUS2_SCALEFORM()
	INT i
	BOOL bPlayerHasPartyBus2
	MODEL_NAMES thePropModel
	//PLAYER_INDEX thePlayer 
	REPEAT NUM_NETWORK_PLAYERS i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[i].netID_PegV)
			IF GET_ENTITY_MODEL(NET_TO_ENT(GlobalplayerBD[i].netID_PegV)) = PBUS2
				bPlayerHasPartyBus2 = TRUE
				thePropModel = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("ba_prop_battle_pbus_screen"))
				IF NOT IS_BIT_SET(partyBusScaleform.iLoadBS,PARTY_BUS_SCALEFORM_BS_REQUESTED_MOVIE)
					partyBusScaleform.theMovie = REQUEST_SCALEFORM_MOVIE("PARTY_BUS")
					PRINTLN("MAINTAIN_PARTY_BUS2_SCALEFORM: requested PARTY_BUS movie")
					SET_BIT(partyBusScaleform.iLoadBS,PARTY_BUS_SCALEFORM_BS_REQUESTED_MOVIE)
					
				ENDIF
				IF NOT IS_BIT_SET(partyBusScaleform.iLoadBS,PARTY_BUS_SCALEFORM_BS_REGISTERED_RENDER_TARGET)
					IF NOT IS_NAMED_RENDERTARGET_REGISTERED("PBus_Screen")
						REGISTER_NAMED_RENDERTARGET("PBus_Screen")
						IF NOT IS_NAMED_RENDERTARGET_LINKED(thePropModel)
							LINK_NAMED_RENDERTARGET(thePropModel)
							IF partyBusScaleform.iRenderTargeID = -1
								partyBusScaleform.iRenderTargeID = GET_NAMED_RENDERTARGET_RENDER_ID("PBus_Screen")
								PRINTLN("MAINTAIN_PARTY_BUS2_SCALEFORM: 1 Setting RT:", partyBusScaleform.iRenderTargeID)
							ENDIF
							SET_BIT(partyBusScaleform.iLoadBS,PARTY_BUS_SCALEFORM_BS_REGISTERED_RENDER_TARGET)
							PRINTLN("MAINTAIN_PARTY_BUS2_SCALEFORM: 1 linked and registered PBus_Screen")
						ENDIF
					ELSE
						IF NOT IS_NAMED_RENDERTARGET_LINKED(thePropModel)
							LINK_NAMED_RENDERTARGET(thePropModel)
							IF partyBusScaleform.iRenderTargeID = -1
								partyBusScaleform.iRenderTargeID = GET_NAMED_RENDERTARGET_RENDER_ID("PBus_Screen")
								PRINTLN("MAINTAIN_PARTY_BUS2_SCALEFORM: 2 Setting RT:", partyBusScaleform.iRenderTargeID)
							ENDIF
							SET_BIT(partyBusScaleform.iLoadBS,PARTY_BUS_SCALEFORM_BS_REGISTERED_RENDER_TARGET)
							PRINTLN("MAINTAIN_PARTY_BUS2_SCALEFORM: 2 linked and registered PBus_Screen")
						ENDIF
					ENDIF
				ENDIF
										
				SET_BIT(partyBusScaleform.iLoadBS,PARTY_BUS_SCALEFORM_BS_REQUESTED_MODEL)
				IF REQUEST_LOAD_MODEL(thePropModel)
					//REPEAT NUM_NETWORK_PLAYERS i
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[i].netID_PegV)
							IF GET_ENTITY_MODEL(NET_TO_ENT(GlobalplayerBD[i].netID_PegV)) = PBUS2
								IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(GlobalplayerBD[i].netID_PegV))
									IF NOT DOES_ENTITY_EXIST(partyBusScaleform.theProp[i])
									AND IS_BIT_SET(partyBusScaleform.iLoadBS,PARTY_BUS_SCALEFORM_BS_REGISTERED_RENDER_TARGET)
										partyBusScaleform.theProp[i] = CREATE_OBJECT(thePropModel,GET_ENTITY_COORDS(NET_TO_ENT(GlobalplayerBD[i].netID_PegV)) + <<0,0,-50>>,FALSE,FALSE)
										ATTACH_ENTITY_TO_ENTITY(partyBusScaleform.theProp[i],NET_TO_ENT(GlobalplayerBD[i].netID_PegV),0,<<0,0,0>>,<<0,0,0>>)
										PRINTLN("MAINTAIN_PARTY_BUS2_SCALEFORM: created and attached prop to vehicle #",i)
									ENDIF
								ENDIF
							ELSE
								IF DOES_ENTITY_EXIST(partyBusScaleform.theProp[i])
									DELETE_OBJECT(partyBusScaleform.theProp[i])
									PRINTLN("MAINTAIN_PARTY_BUS2_SCALEFORM: 1 deleting prop #",i)
								ENDIF
							ENDIF
						ELSE
							IF DOES_ENTITY_EXIST(partyBusScaleform.theProp[i])
								DELETE_OBJECT(partyBusScaleform.theProp[i])
								PRINTLN("MAINTAIN_PARTY_BUS2_SCALEFORM: 2 deleting prop #",i)
							ENDIF
						ENDIF
					//ENDREPEAT
				ENDIF
				IF IS_BIT_SET(partyBusScaleform.iLoadBS,PARTY_BUS_SCALEFORM_BS_REGISTERED_RENDER_TARGET)
				AND IS_BIT_SET(partyBusScaleform.iLoadBS,PARTY_BUS_SCALEFORM_BS_REQUESTED_MOVIE)
					IF HAS_SCALEFORM_MOVIE_LOADED(partyBusScaleform.theMovie)
						SET_SCALEFORM_MOVIE_TO_USE_LARGE_RT(partyBusScaleform.theMovie, TRUE) 
						SET_TEXT_RENDER_ID(partyBusScaleform.iRenderTargeID)
						SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
						SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
						#IF IS_DEBUG_BUILD
						IF db_bTestScaleform
							BEGIN_SCALEFORM_MOVIE_METHOD(partyBusScaleform.theMovie, "SET_TEST_CARD")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
							END_SCALEFORM_MOVIE_METHOD()
							DRAW_SCALEFORM_MOVIE(partyBusScaleform.theMovie,db_CentreX,db_CentreY,db_Width,db_Height,255,255,255,255)
						ELSE
						#ENDIF
							DRAW_SCALEFORM_MOVIE(partyBusScaleform.theMovie,0.4,0.045,0.8,0.09,255,255,255,255)
						#IF IS_DEBUG_BUILD
						ENDIF
						#ENDIF
						SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	ENDREPEAT
	IF !bPlayerHasPartyBus2
		IF IS_BIT_SET(partyBusScaleform.iLoadBS,PARTY_BUS_SCALEFORM_BS_REQUESTED_MOVIE)
			SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(partyBusScaleform.theMovie)
			CLEAR_BIT(partyBusScaleform.iLoadBS,PARTY_BUS_SCALEFORM_BS_REQUESTED_MOVIE)
			PRINTLN("MAINTAIN_PARTY_BUS2_SCALEFORM: releasing PARTY_BUS movie")
		ENDIF
		IF IS_BIT_SET(partyBusScaleform.iLoadBS,PARTY_BUS_SCALEFORM_BS_REQUESTED_MODEL)
			SET_MODEL_AS_NO_LONGER_NEEDED( INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("ba_prop_battle_pbus_screen")))
			PRINTLN("MAINTAIN_PARTY_BUS2_SCALEFORM: releasing prop model")
			CLEAR_BIT(partyBusScaleform.iLoadBS,PARTY_BUS_SCALEFORM_BS_REQUESTED_MODEL)
		ENDIF
		//REPEAT NUM_NETWORK_PLAYERS i
		IF iRemoteVehicleStagger != -1	
			IF DOES_ENTITY_EXIST(partyBusScaleform.theProp[iRemoteVehicleStagger])
				DELETE_OBJECT(partyBusScaleform.theProp[iRemoteVehicleStagger])
				PRINTLN("MAINTAIN_PARTY_BUS2_SCALEFORM: 3 deleting prop #",iRemoteVehicleStagger)
			ENDIF
		ENDIF	
		//ENDREPEAT
		partyBusScaleform.iRenderTargeID = -1
		IF IS_NAMED_RENDERTARGET_REGISTERED("PBus_Screen")
			RELEASE_NAMED_RENDERTARGET("PBus_Screen")
			PRINTLN("MAINTAIN_PARTY_BUS2_SCALEFORM: releasing render target")
			CLEAR_BIT(partyBusScaleform.iLoadBS,PARTY_BUS_SCALEFORM_BS_REGISTERED_RENDER_TARGET)
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_CONCEAL_PLAYER_USING_RC_VEHICLE()
	PLAYER_INDEX playerToCheck = INT_TO_PLAYERINDEX(iRemoteVehicleStagger)
	
	IF PLAYER_ID() != playerToCheck
	AND IS_NET_PLAYER_OK(playerToCheck)
		IF IS_PLAYER_USING_RC_PERSONAL_VEHICLE(playerToCheck)
			IF NOT NETWORK_IS_PLAYER_CONCEALED(playerToCheck)
				NETWORK_CONCEAL_PLAYER(playerToCheck, TRUE)
				SET_BIT(iPersonalVehicleRemoteControlConceal, iRemoteVehicleStagger)
			ENDIF
		ELSE	
			IF IS_BIT_SET(iPersonalVehicleRemoteControlConceal, iRemoteVehicleStagger)
				IF NETWORK_IS_PLAYER_CONCEALED(playerToCheck)
					NETWORK_CONCEAL_PLAYER(playerToCheck, FALSE)
				ENDIF
				
				CLEAR_BIT(iPersonalVehicleRemoteControlConceal, iRemoteVehicleStagger)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                      //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT(MP_MISSION_DATA missionScriptArgs)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === STARTING SCRIPT")
		#ENDIF
		
		// Carry out all the initial game starting duties. 
		IF NOT PROCESS_PRE_GAME(missionScriptArgs)
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === FAILED TO RECEIVE INITIAL NETWORK BROADCAST")
			#ENDIF
			SCRIPT_CLEANUP()
		ENDIF
		
		// Setup widgets. Make sure this is done after PROCESS_PRE_GAME()
		#IF IS_DEBUG_BUILD
			CREATE_WIDGETS()
		#ENDIF
		
		SCRIPT_INITIALISE()
		
	ENDIF
	
	// Main loop
	WHILE TRUE

		MP_LOOP_WAIT_ZERO() 
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_START_OF_FRAME() 
		#ENDIF
		#ENDIF			
		
		// If we have a match end event, bail
		IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		OR SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		OR SHOULD_VEHICLE_SPAWN_TERMINATE()
			IF NOT IS_ON_IMPROMPTU_DEATHMATCH_GLOBAL_SET()
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")
				#ENDIF
				SCRIPT_CLEANUP()
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("cleanup checks")
		#ENDIF	
		#ENDIF	
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
			timeNet = GET_NETWORK_TIME()
			IF GET_FRAME_COUNT() % 30 = 0
				IF SHOULD_ALLOW_EXPENSIVE_PROCESSING()
					SET_BIT(iLocalBS, LOCAL_BS_SHOULD_ALLOW_EXPENSIVE_PROCESSING)
				ELSE
					CLEAR_BIT(iLocalBS, LOCAL_BS_SHOULD_ALLOW_EXPENSIVE_PROCESSING)
				ENDIF
			ENDIF	
		ENDIF

		// Deal with the debug
		#IF IS_DEBUG_BUILD
			UPDATE_WIDGETS()
			
			IF NOT bDebugClientHostDeclaration
			OR playerLastRecordedHost <> NETWORK_GET_HOST_OF_SCRIPT("AM_VEHICLE_SPAWN")
				IF NATIVE_TO_INT(NETWORK_GET_HOST_OF_SCRIPT("AM_VEHICLE_SPAWN")) <> -1
					playerLastRecordedHost = NETWORK_GET_HOST_OF_SCRIPT("AM_VEHICLE_SPAWN")
					IF NETWORK_IS_PLAYER_ACTIVE(playerLastRecordedHost)
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === CLIENT RECOGNISES HOST AS ", GET_PLAYER_NAME(playerLastRecordedHost))
						#ENDIF
					ENDIF
					bDebugClientHostDeclaration = TRUE
				ENDIF
			ENDIF
			
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("debug")
		#ENDIF	
		#ENDIF		
	
		// -----------------------------------
		// Process the game logic.....
		
		CLIENT_MAINTAIN_RESERVATIONS()
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("CLIENT_MAINTAIN_RESERVATIONS")
		#ENDIF	
		#ENDIF		
		
		IF NOT IS_PLAYER_SPECTATING(PLAYER_ID())
			CLEAR_REMOTE_PEGASUS_VEHICLE(iRemoteVehicleStagger)						
			CLEAR_REMOTE_TRUCK_VEHICLE(iRemoteVehicleStagger)
			CLEANUP_FAKE_MOC_PED(iRemoteVehicleStagger)
			CLEAR_REMOTE_AVENGER_VEHICLE(iRemoteVehicleStagger)
			CLEANUP_AVENGER_MOC_PED(iRemoteVehicleStagger)	
			CLEANUP_FAKE_HCAKER_TRUCK_PED(iRemoteVehicleStagger)
			CLEAR_REMOTE_HACKER_TRUCK_VEHICLE(iRemoteVehicleStagger)
			#IF FEATURE_DLC_2_2022
			CLEAR_REMOTE_ACID_LAB_VEHICLE(iRemoteVehicleStagger)
			#ENDIF
			#IF FEATURE_HEIST_ISLAND
			MAINTAIN_REMOTE_SUBMARINE_VEHICLE(iRemoteVehicleStagger)
			MAINTAIN_REMOTE_SUBMARINE_DINGHY_VEHICLE(iRemoteVehicleStagger)
			CLEANUP_FAKE_SUBMARINE_PED(iRemoteVehicleStagger)
			#ENDIF
			#IF FEATURE_DLC_2_2022
			MAINTAIN_REMOTE_SUPPORT_BIKE_VEHICLE(iRemoteVehicleStagger)
			#ENDIF
			iRemoteVehicleStagger += 1
			IF (iRemoteVehicleStagger >= NUM_NETWORK_PLAYERS)
				iRemoteVehicleStagger = 0
			ENDIF
		ENDIF
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("clear remote stuff")
		#ENDIF	
		#ENDIF		
		
		UPDATE_ONE_ON_ONE_DM_VEHICLE_DUPING()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("UPDATE_ONE_ON_ONE_DM_VEHICLE_DUPING")
		#ENDIF	
		#ENDIF		
			
		MAINTAIN_CONCEAL_PLAYER_USING_RC_VEHICLE()		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CONCEAL_PLAYER_USING_RC_VEHICLE")
		#ENDIF	
		#ENDIF	
		
		SWITCH GET_CLIENT_MISSION_STATE(NETWORK_PLAYER_ID_TO_INT())
			
			// Wait until the server gives the all go before moving on
			CASE GAME_STATE_INI
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					playerBD[NETWORK_PLAYER_ID_TO_INT()].iGameState = GAME_STATE_RUNNING
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === playerBD[", NETWORK_PLAYER_ID_TO_INT(), "].iGameState = GAME_STATE_RUNNING")
					#ENDIF

				// Look for the server say the mission has ended
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[NETWORK_PLAYER_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === playerBD[", NETWORK_PLAYER_ID_TO_INT(), "].iGameState = GAME_STATE_TERMINATE_DELAY 1")
					#ENDIF
				ENDIF
				
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("GAME_STATE_INI")
				#ENDIF	
				#ENDIF				
			BREAK
			
			// Main gameplay state.
			CASE GAME_STATE_RUNNING
				
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("GAME_STATE_RUNNING start..")
				#ENDIF	
				#ENDIF				
				
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					MAINTAIN_PARTY_BUS2_SCALEFORM()
					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PARTY_BUS2_SCALEFORM")
					#ENDIF	
					#ENDIF						
					
					
//					IS_PLAYER_IN_SYNC_ACTIVITY(luxeActState)
					SWITCH luxActGenerationState
						CASE CHECK_PLAYER_IS_IN_LUXE_ACTIVITY_VEH
							IF IS_PLAYER_IN_OR_TRYING_TO_ENTER_LUXE_VEH(luxeActState, NULL, bTempLuxeBool)
							AND NOT MPGlobalsAmbience.g_bForceFromVehicle
								CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CHECK_PLAYER_IS_IN_LUXE_ACTIVITY_VEH: luxActGenerationState")
								SetGenActStage(luxActGenerationState, REQUEST_INSTANCE_ID)
								playerBD[NATIVE_TO_INT(PLAYER_ID())].objIDRequestingVehicle = -1
							ENDIF
						BREAK
						
						CASE REQUEST_INSTANCE_ID
							IF IS_PLAYER_IN_OR_TRYING_TO_ENTER_LUXE_VEH(luxeActState, NULL, bTempLuxeBool)
							AND NOT MPGlobalsAmbience.g_bForceFromVehicle
								luxeActState.iLuxeVehInstanceID = CLIENT_REQUEST_LUXE_INSTANCE_ID(luxeActState.vehicleIndex)
								IF luxeActState.iLuxeVehInstanceID >= 0
									CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "[HS]AM_VEHICLE_SPAWN:  recieved instance id from client: luxeActState.iLuxeVehInstanceID: ", luxeActState.iLuxeVehInstanceID)
									SetGenActStage(luxActGenerationState, LAUNCH_LUX_VEH_SCRIPT)
								ENDIF
							ELSE
								SetGenActStage(luxActGenerationState, CHECK_FOR_LUX_VEH_RESET)
							ENDIF
						BREAK
						
						CASE LAUNCH_LUX_VEH_SCRIPT
							IF NOT NETWORK_IS_SCRIPT_ACTIVE("luxe_veh_activity", luxeActState.iLuxeVehInstanceID, TRUE)
								CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "[HS]AM_VEHICLE_SPAWN: Requesting script: luxe_veh_activity")
								REQUEST_SCRIPT("luxe_veh_activity")
								
								IF IS_BIT_SET(g_iLuxeBootVerifyBS, luxeActState.iLuxeVehInstanceID)
									CLEAR_BIT(g_iLuxeBootVerifyBS, luxeActState.iLuxeVehInstanceID)
									CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "[HS]AM_VEHICLE_SPAWN: Emergency resetting boot bit index: ", luxeActState.iLuxeVehInstanceID)
								ENDIF

								IF HAS_SCRIPT_LOADED("luxe_veh_activity")
									CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "[HS]AM_VEHICLE_SPAWN: HAS_SCRIPT_LOADED = TRUE for script: luxe_veh_activity")
								    START_NEW_SCRIPT_WITH_ARGS("luxe_veh_activity", luxeActState, SIZE_OF(luxeActState), DEFAULT_STACK_SIZE)
					//				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "MAINTAIN_LUXE_VEH_ACTIVITIES: Launching script with niVehNetID: ", NATIVE_TO_INT(luxeActStruct.niVehNetID))
								    SET_SCRIPT_AS_NO_LONGER_NEEDED("luxe_veh_activity")
									bDoLaunchedScriptCheck = FALSE
	//								luxActGenerationState = CHECK_FOR_LUX_VEH_RESET
									g_iLuxeLatestScriptInstanceID = luxeActState.iLuxeVehInstanceID
									SetGenActStage(luxActGenerationState, VERIFY_LAUNCH_LUX_VEH_SCRIPT)
								ENDIF
							ELSE
								bDoLaunchedScriptCheck = TRUE
								CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "[HS]AM_VEHICLE_SPAWN: NETWORK_IS_SCRIPT_ACTIVE - bDoLaunchedScriptCheck = TRUE: NETWORK_IS_SCRIPT_ACTIVE(\"luxe_veh_activity\", ", luxeActState.iLuxeVehInstanceID, ", TRUE)")
								g_iLuxeLatestScriptInstanceID = luxeActState.iLuxeVehInstanceID
								SetGenActStage(luxActGenerationState, VERIFY_LAUNCH_LUX_VEH_SCRIPT)
							ENDIF
						BREAK
						
						CASE VERIFY_LAUNCH_LUX_VEH_SCRIPT
							IF bDoLaunchedScriptCheck
							AND NOT NETWORK_IS_SCRIPT_ACTIVE("luxe_veh_activity", luxeActState.iLuxeVehInstanceID, TRUE)
								CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "[HS]AM_VEHICLE_SPAWN: VERIFY_LAUNCH_LUX_VEH_SCRIPT - bDoLaunchedScriptCheck = TRUE: NETWORK_IS_SCRIPT_ACTIVE(\"luxe_veh_activity\", ", luxeActState.iLuxeVehInstanceID, ", TRUE)")
							
								SetGenActStage(luxActGenerationState, LAUNCH_LUX_VEH_SCRIPT)
								
							ELSE
						
								CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "[HS]AM_VEHICLE_SPAWN: Attempting to verify boot state. InstanceID: ",luxeActState.iLuxeVehInstanceID,", verify bitset: ", g_iLuxeBootVerifyBS)
							
								IF IS_BIT_SET(g_iLuxeBootVerifyBS, luxeActState.iLuxeVehInstanceID)
									CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "[HS]AM_VEHICLE_SPAWN: Boot verification successful, moving to CHECK_FOR_LUX_VEH_RESET.")
									SetGenActStage(luxActGenerationState, CHECK_FOR_LUX_VEH_RESET)
								ELSE
									CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "[HS]AM_VEHICLE_SPAWN: Still waiting for boot verification.")
								ENDIF
							ENDIF
							
						BREAK
						
						CASE CHECK_FOR_LUX_VEH_RESET
							IF NOT IS_PLAYER_IN_OR_TRYING_TO_ENTER_LUXE_VEH(luxeActState, luxeActState.vehicleIndex, bTempLuxeBool)
//							IF NOT NETWORK_IS_PLAYER_A_PARTICIPANT_ON_SCRIPT(PLAYER_ID(),"luxe_veh_activity",luxeActState.iLuxeVehInstanceID)
								
								CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "[HS]AM_VEHICLE_SPAWN: Player is not in a Luxe vehicle, resetting local data and moving to restart.")
								CLIENT_CLEAN_LOCAL_LUXE_DATA()
								SetGenActStage(luxActGenerationState, CHECK_PLAYER_IS_IN_LUXE_ACTIVITY_VEH)
	
							ELSE
							
								IF luxeActState.iLuxeVehInstanceID >= 0
									IF NOT NETWORK_IS_PLAYER_A_PARTICIPANT_ON_SCRIPT(PLAYER_ID(),"luxe_veh_activity",luxeActState.iLuxeVehInstanceID)
										CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "[HS]AM_VEHICLE_SPAWN: player is NOT a participant of the Luxe script, but is in a luxe vehicle. Instance ID: ", luxeActState.iLuxeVehInstanceID, ", Attempts: ", luxeActState.iParticipantAttempts)
										
										IF luxeActState.iParticipantAttempts > 15
											CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "[HS]AM_VEHICLE_SPAWN: Moving to cleanup/re-init luxe script.")
											CLIENT_CLEAN_LOCAL_LUXE_DATA()
											SetGenActStage(luxActGenerationState, CHECK_PLAYER_IS_IN_LUXE_ACTIVITY_VEH)
										ENDIF
										
										luxeActState.iParticipantAttempts++
									ENDIF
								ELSE
									CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "[HS]AM_VEHICLE_SPAWN: ERROR! Player is in a Luxe vehicle, but I cannot find an associated script.")
									CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "[HS]AM_VEHICLE_SPAWN: ERROR! Moving to cleanup/re-init luxe script.")
									CLIENT_CLEAN_LOCAL_LUXE_DATA()
									SetGenActStage(luxActGenerationState, CHECK_PLAYER_IS_IN_LUXE_ACTIVITY_VEH)
								ENDIF
							ENDIF

						BREAK
					ENDSWITCH

					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("LUXE_ACTIVITY_VEH")
					#ENDIF	
					#ENDIF	
					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
					OPEN_SCRIPT_PROFILE_MARKER_GROUP("CLIENT_PROCESSING")
					#ENDIF
					#ENDIF					
					CLIENT_PROCESSING()
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
					CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
					#ENDIF	
					#ENDIF	
				
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_INI
					SCRIPT_TIDY_UP()
				
					playerBD[NETWORK_PLAYER_ID_TO_INT()].iGameState = GAME_STATE_INI
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === playerBD[", NETWORK_PLAYER_ID_TO_INT(), "].iGameState = GAME_STATE_INI")
					#ENDIF
					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("SCRIPT_TIDY_UP")
					#ENDIF	
					#ENDIF						
				
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[NETWORK_PLAYER_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === playerBD[", NETWORK_PLAYER_ID_TO_INT(), "].iGameState = GAME_STATE_TERMINATE_DELAY 2")
					#ENDIF
									
				ENDIF
			BREAK
			
			CASE GAME_STATE_TERMINATE_DELAY
			
				SET_MISSION_FINISHED(serverBD.MissionTerminateDelayTimer)
		      	IF IS_MISSION_READY_TO_CLEANUP(serverBD.MissionTerminateDelayTimer)
		            playerBD[NETWORK_PLAYER_ID_TO_INT()].iGameState = GAME_STATE_END
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === playerBD[", NETWORK_PLAYER_ID_TO_INT(), "].iGameState = GAME_STATE_TERMINATE_DELAY 3")
					#ENDIF
				ENDIF
				
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("GAME_STATE_TERMINATE_DELAY")
				#ENDIF	
				#ENDIF				
			BREAK
			
			// The game stage the local player is placed when we want him to leave a mission
			CASE GAME_STATE_LEAVE
				
				playerBD[NETWORK_PLAYER_ID_TO_INT()].iGameState = GAME_STATE_END
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === playerBD[", NETWORK_PLAYER_ID_TO_INT(), "].iGameState = GAME_STATE_END 2")
				#ENDIF
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("GAME_STATE_LEAVE")
				#ENDIF	
				#ENDIF				
			FALLTHRU
			
			//Cleans up then terminates the mission
			CASE GAME_STATE_END
			
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SCRIPT CLEANUP")
				#ENDIF
				
				SCRIPT_CLEANUP()
				
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("GAME_STATE_END")
				#ENDIF	
				#ENDIF					
			BREAK

		ENDSWITCH
		
		UPDATE_MOBILE_CONTROL_CENTRE_STUCK_CHECKS()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("UPDATE_MOBILE_CONTROL_CENTRE_STUCK_CHECKS")
		#ENDIF	
		#ENDIF	
				
		UPDATE_HACKER_TRUCK_STUCK_CHECKS()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("UPDATE_HACKER_TRUCK_STUCK_CHECKS")
		#ENDIF	
		#ENDIF	
		
		UPDATE_WARP_HACKER_TRUCK_NEARBY()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("UPDATE_WARP_HACKER_TRUCK_NEARBY")
		#ENDIF	
		#ENDIF
		
		#IF FEATURE_DLC_2_2022
		UPDATE_ACID_LAB_STUCK_CHECKS()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("UPDATE_ACID_LAB_STUCK_CHECKS")
		#ENDIF	
		#ENDIF
		
		UPDATE_WARP_ACID_LAB_NEARBY()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("UPDATE_WARP_ACID_LAB_NEARBY")
		#ENDIF	
		#ENDIF
		#ENDIF
		
		#IF FEATURE_HEIST_ISLAND
		MAINTAIN_WARP_SUBMARINE_TO_COORDS()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_WARP_SUBMARINE_TO_COORDS")
		#ENDIF	
		
		MAINTAIN_SUBMARINE_FIRST_TIME_REVEAL()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SUBMARINE_FIRST_TIME_REVEAL")
		#ENDIF		
		
		
		#ENDIF
		
		
		
		#ENDIF
		
		// -----------------------------------
		// Process server game logic
		IF NETWORK_IS_GAME_IN_PROGRESS()
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			
				#IF IS_DEBUG_BUILD
					IF NOT bDebugServerDeclaration
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN SERVER === I AM THE HOST OF AM_VEHICLE_SPAWN")
						#ENDIF
						bDebugServerDeclaration = TRUE
					ENDIF
				#ENDIF
			
				SERVER_MAINTAIN_RESERVATIONS()
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("SERVER_MAINTAIN_RESERVATIONS")
				#ENDIF	
				#ENDIF	
				
				SWITCH GET_SERVER_MISSION_STATE()
					
					CASE GAME_STATE_INI
						
						SERVER_FORCE_CLEAR_ALL_SPAWN_POINTS()
						
						SERVER_CLEAR_ALL_PLAYER_VEHICLE_SPAWN_REQUESTS()
						
						serverBD.iServerGameState = GAME_STATE_RUNNING
						
						serverBD.LuxeVehIDPlayerRequest = INT_TO_NATIVE(PLAYER_INDEX, -1)
						INT index
						FOR index = 0 TO (NUM_NETWORK_PLAYERS-1)
							ServerBD.objIDStoredVehicles[index] = -1
						ENDFOR
						
						#IF IS_DEBUG_BUILD
						#IF SCRIPT_PROFILER_ACTIVE
						ADD_SCRIPT_PROFILE_MARKER("server GAME_STATE_INI")
						#ENDIF	
						#ENDIF
					BREAK
					
					// Look for game end conditions
					CASE GAME_STATE_RUNNING
						
						SERVER_PROCESSING()
						#IF IS_DEBUG_BUILD
						#IF SCRIPT_PROFILER_ACTIVE
						ADD_SCRIPT_PROFILE_MARKER("SERVER_PROCESSING")
						#ENDIF	
						#ENDIF
						
						SERVER_CHECK_LUXE_INSTANCE_REQUESTS()
						#IF IS_DEBUG_BUILD
						#IF SCRIPT_PROFILER_ACTIVE
						ADD_SCRIPT_PROFILE_MARKER("SERVER_CHECK_LUXE_INSTANCE_REQUESTS")
						#ENDIF	
						#ENDIF
						
						SERVER_MONITOR_LUXE_VEHICLE_INSTANCES()
						#IF IS_DEBUG_BUILD
						#IF SCRIPT_PROFILER_ACTIVE
						ADD_SCRIPT_PROFILE_MARKER("SERVER_MONITOR_LUXE_VEHICLE_INSTANCES")
						#ENDIF	
						#ENDIF
						
					BREAK
					
					CASE GAME_STATE_END
					BREAK
					
				ENDSWITCH
				
				IF GET_SERVER_MISSION_STATE() < GAME_STATE_END
					IF HAVE_MISSION_END_CONDITIONS_BEEN_MET()
						serverBD.iServerGameState = GAME_STATE_END
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN SERVER === serverBD.iServerGameState = GAME_STATE_END 4")
						#ENDIF
					ENDIF
				ENDIF
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("HAVE_MISSION_END_CONDITIONS_BEEN_MET")
				#ENDIF	
				#ENDIF				
			
			#IF IS_DEBUG_BUILD
			ELSE
				IF bDebugServerDeclaration
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN SERVER === I AM NO LONGER THE HOST OF AM_VEHICLE_SPAWN")
					#ENDIF
					bDebugServerDeclaration = FALSE
				ENDIF
			#ENDIF
			
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_END_OF_FRAME() 
		#ENDIF
		#ENDIF			
	ENDWHILE
	
ENDSCRIPT
