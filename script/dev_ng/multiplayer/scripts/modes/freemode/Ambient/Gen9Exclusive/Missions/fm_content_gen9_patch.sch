USING "globals.sch"
USING "rage_builtins.sch"

#IF ENABLE_CONTENT
// ****************************************************************************************************************
// ****************************************************************************************************************
// 		PATCH HEADER
//		Description: Contains deprecated definitions for functions that have been changed in future releases.
//		Purpose: To prevent introducing regression errors to previously released content.
// ****************************************************************************************************************
// ****************************************************************************************************************
// 		Add functions HERE:

#IF MAX_NUM_PATROLS
#IF NOT DEFINED(GET_PATROL_SCENARIO)
FUNC STRING GET_PATROL_SCENARIO(INT iPatrol, INT iNode)
	INT iScenario = data.Patrol.Patrols[iPatrol].Node[iNode].iScenarioIndex
	IF iScenario != -1
		#IF MAX_NUM_SCENARIOS
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] GET_PATROL_SCENARIO - Using Gen9 release version.")
		CONVERT_TEXT_LABEL_TO_STRING(data.Scenario[iScenario].sName)
		#ENDIF
	ENDIF
	
	RETURN ""
ENDFUNC
#ENDIF
#ENDIF

#IF MAX_NUM_MISSION_ENTITIES
#IF NOT DEFINED(PERFORM_DELIVERED_CARRIER_VEHICLE_DELETE_ON_END)
FUNC BOOL PERFORM_DELIVERED_CARRIER_VEHICLE_DELETE_ON_END()
	RETURN TRUE
ENDFUNC
#ENDIF
#ENDIF

#IF MAX_NUM_PEDS
FUNC BOOL CAN_PED_USE_VEHICLE_ATTACK(INT iPed, PED_INDEX pedId)
	UNUSED_PARAMETER(pedId)
	
	IF IS_PED_DATA_BIT_SET(iPed, ePEDDATABITSET_ALLOW_BUZZARD_CHAINGUN_USE)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC
#ENDIF

// ****************************************************************************************************************
// 		Include header for next patch HERE:
		USING "fm_content_summer2022_patch.sch"
// ****************************************************************************************************************
#ENDIF
