//////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_DISTRACT_COPS.sc														//
// Description: Players have to stay in an area with a Wanted Level for a set time		//
// Written by:  Ryan Baker																//
// Date: 06/08/2014																		//
//////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_event.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"

USING "net_mission.sch"
//
USING "net_scoring_common.sch"
USING "net_ambience.sch"

//USING "help_at_location.sch"
USING "commands_object.sch"
USING "net_xp_animation.sch"

USING "net_wait_zero.sch"

#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF

USING "net_gang_boss.sch"

// Game States. 
CONST_INT GAME_STATE_INI 				0
CONST_INT GAME_STATE_RUNNING			1
CONST_INT GAME_STATE_LEAVE				2
CONST_INT GAME_STATE_TERMINATE_DELAY	3
CONST_INT GAME_STATE_END				4

ENUM DISTRACT_STAGE_ENUM
	eAD_IDLE,
	eAD_IN_AREA,
	eAD_CLEANUP
ENDENUM

CONST_INT FAIL_TO_START_DISTRACTION_TIME	480000	//8mins
CONST_INT DISTRACTION_LENGTH				300000	//5mins
CONST_INT DISTRACTION_CHECKPOINT_LENGTH		30000	//30sec
CONST_INT DISTRACTION_RADIUS				200
CONST_INT LEAVE_WARNING_RADIUS 				180
CONST_INT CHECKPOINT_BASIC_XP				50
CONST_INT CASH_PER_CHECKPOINT				500

//Server Bitset Data
CONST_INT biS_IsAnyPlayerInArea		0
CONST_INT biS_SendFailTextMessage	1

// The server broadcast data.
// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData
	INT iServerGameState
		
	INT iServerBitSet
		
	VECTOR vDistractLocation
	FLOAT fDistractionRadius
	FLOAT fLeaveWarningRadius
		
	DISTRACT_STAGE_ENUM eDistractStage = eAD_IDLE
	
	SCRIPT_TIMER FailToStartDistractionTimer
	
	SCRIPT_TIMER DistractionCompletedTimer
	INT iDistractionTime = DISTRACTION_LENGTH
	
	SCRIPT_TIMER DistractionCheckpointTimer
	INT iDistractionCheckpointTime = DISTRACTION_CHECKPOINT_LENGTH
	
	INT iMinimumWantedLevel = 2
	INT iCheckpoint
	
	SCRIPT_TIMER MissionTerminateDelayTimer
ENDSTRUCT
ServerBroadcastData serverBD


CONST_INT biP_InArea		0

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	INT iGameState
	
	INT iPlayerBitSet
	
	INT iRewardCounter
	
	DISTRACT_STAGE_ENUM eDistractStage = eAD_IDLE
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

///// MISSION VARIABLES /////
INT iBoolsBitSet
CONST_INT biLeaveWarningHelpDone		0
CONST_INT biEndRewardGiven				1
CONST_INT biSpawnInArea					2
CONST_INT biEnteredArea					3
CONST_INT biStartTrackingAward			4
CONST_INT biStopTrackingAward			5
CONST_INT biHelpTextDone				6
CONST_INT iStartedCopKillsTracking		7


BLIP_INDEX AreaBlip

SCRIPT_TIMER iRewardTimer
CONST_INT REWARD_DELAY	30000

SCRIPT_TIMER iWantedLevelDelayTimer
CONST_INT WANTED_LEVEL_DELAY	10000

INT iStaggeredParticipant
BOOL bPlayerInArea

// Widgets & Debug.
#IF IS_DEBUG_BUILD
	BOOL bHostEndMissionNow
	BOOL bWarpToArea
#ENDIF

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MAIN PROCS                            ////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Helper function to get the servers game/mission state
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC

FUNC BOOL SHOULD_HIDE_LOCAL_UI()
//	Removed content, setting to hide
//	IF HANDLE_DEPRECATED_HIDE_BLIP(ciPI_HIDE_MENU_DEPRECATED_AMB_DISTRACT_COPS)
//		RETURN TRUE
//	ENDIF
	
	RETURN TRUE
ENDFUNC


//PURPOSE: Set the player's spawn area
PROC SET_PLAYER_SPAWN_AREA()
	IF NOT IS_BIT_SET(iBoolsBitSet, biSpawnInArea)
		//SET_MISSION_SPAWN_SPHERE(serverBD.vDistractLocation, serverBD.fLeaveWarningRadius, DEFAULT, TRUE, TRUE)
		
		SET_MISSION_SPAWN_SPHERE(serverBD.vDistractLocation, (serverBD.fLeaveWarningRadius*1.5), DEFAULT, DEFAULT, TRUE)
		SET_MISSION_SPAWN_OCCLUSION_SPHERE(serverBD.vDistractLocation, serverBD.fLeaveWarningRadius, DEFAULT, TRUE)
		SET_PLAYER_WILL_SPAWN_FACING_COORDS(serverBD.vDistractLocation, TRUE, FALSE)
		
		SET_BIT(iBoolsBitSet, biSpawnInArea)
		PRINTLN("     ---------->     DISTRACT COPS - SET_PLAYER_SPAWN_AREA - LOCATION = ", serverBD.vDistractLocation, " RADIUS = ", serverBD.fLeaveWarningRadius)
	ENDIF
ENDPROC

//PURPOSE: Clear the player's spawn area
PROC CLEAR_PLAYER_SPAWN_AREA()
	IF IS_BIT_SET(iBoolsBitSet, biSpawnInArea)
		CLEAR_SPAWN_AREA()
		CLEAR_BIT(iBoolsBitSet, biSpawnInArea)
		NET_PRINT_TIME() NET_PRINT("     ---------->     DISTRACT COPS - CLEAR_PLAYER_SPAWN_AREA - DONE    <----------     ") NET_NL()
	ENDIF
ENDPROC

//Cleanup Mission Data
PROC SCRIPT_CLEANUP()
	
	IF DOES_BLIP_EXIST(AreaBlip)
		REMOVE_BLIP(AreaBlip)
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_SendFailTextMessage)
		Send_MP_Txtmsg_From_CharSheetID(CHAR_LESTER, "DSC_FAIL")
		NET_PRINT_TIME() NET_PRINT("     ---------->     DISTRACT COPS -  SEND FAIL TEXT MESSAGE      <----------     ") NET_NL()
	ENDIF
	
	CLEAR_PLAYER_SPAWN_AREA()
	SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_DISTRACT_COPS, FALSE)
	
	SET_BIT(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_PLAYER_IN_DISTRACT_COPS_AREA)
	PRINTLN("     ---------->     DISTRACT COPS -  iNGABI_PLAYER_IN_DISTRACT_COPS_AREA SET")
	
	NET_PRINT_TIME() NET_PRINT("     ---------->     DISTRACT COPS -  CLEANUP MISSION      <----------     ") NET_NL()
	#IF IS_DEBUG_BUILD NET_LOG("SCRIPT_CLEANUP")	#ENDIF
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

//PURPOSE: Returns a Random location to do the Distract at.
FUNC VECTOR GET_RANDOM_DISTRACT_AREA()
	SWITCH GET_RANDOM_INT_IN_RANGE(0, 10)
		CASE 0		RETURN <<1440, -2135, 60>>	//OIL FIELD
		CASE 1		RETURN <<233, -1725, 30>>	//GHETTO
		CASE 2		RETURN <<-403, -1207, 38>>	//SPAGEHTTI JUNCTION
		CASE 3		RETURN <<-1060, -1028, 3>>	//CANAL HOUSING
		CASE 4		RETURN <<-2152, -329, 14>>	//EXIT OUT OF TOWN
		CASE 5		RETURN <<-1651, 214, 61>>	//CAMPUS
		CASE 6		RETURN <<-527, 662, 142>>	//HILLS HOUSES
		CASE 7		RETURN <<-27, -747, 45>>	//FINANCIAL DISTRICT
		CASE 8		RETURN <<1292, -649, 68>>	//HOUSING ESTATE
		CASE 9		RETURN <<978, -3143, 6>>	//SHIPPING YARD
	ENDSWITCH
	
	RETURN <<978, -3143, 6>>
ENDFUNC

//PURPOSE: Grabs Random Vector for Distract Location
PROC GET_DISTRACT_LOCATION()
	serverBD.vDistractLocation = GET_RANDOM_DISTRACT_AREA()
	serverBD.fDistractionRadius = DISTRACTION_RADIUS
	serverBD.fLeaveWarningRadius = LEAVE_WARNING_RADIUS
	PRINTLN("     ---------->     DISTRACT COPS -  GET_DISTRACT_LOCATION - vDistractLocation = ", serverBD.vDistractLocation)
ENDPROC

//Do necessary pre game start ini.
PROC PROCESS_PRE_GAME( MP_MISSION_DATA missionScriptArgs )
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission) ,  missionScriptArgs)	//NUM_NETWORK_PLAYERS, missionScriptArgs)	//
	//NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, PARTICIPANT_ID_TO_INT())  
	
	//Reserve Entities
	//RESERVE_NETWORK_MISSION_PEDS(1)

	// This makes sure the net script is active, waits untill it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		#IF IS_DEBUG_BUILD NET_LOG("DISTRACT COPS -  FAILED TO RECEIVE INITIAL NETWORK BROADCAST")	#ENDIF
		NET_PRINT_TIME() NET_PRINT("     ---------->     DISTRACT COPS -  MISSION END - FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP D     <----------     ") NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		// This script will not be paused if another script calls PAUSE_GAME
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			GET_DISTRACT_LOCATION()
		ENDIF
		
		SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_DISTRACT_COPS, TRUE)
		
		playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_INI
		NET_PRINT_TIME() NET_PRINT("     ---------->     DISTRACT COPS -  PRE_GAME DONE     <----------     ") NET_NL()
	ELSE
		#IF IS_DEBUG_BUILD NET_LOG("DISTRACT COPS -  NETWORK_IS_GAME_IN_PROGRESS = FALSE")	#ENDIF
		NET_PRINT_TIME() NET_PRINT("     ---------->     DISTRACT COPS -  MISSION END - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP Q     <----------     ") NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
ENDPROC

//Player leaves area
FUNC BOOL MISSION_END_CHECK()
	
	IF serverBD.eDistractStage = eAD_CLEANUP
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Server only function that checks to see if the game mode should now end. For this test script, it if time has passed.
FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET()
	
	#IF IS_DEBUG_BUILD
		IF bHostEndMissionNow
			RETURN TRUE
		ENDIF
	#ENDIF	
	
	IF MISSION_END_CHECK()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


//Helper function to get a clients game/mission state
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iGameState
ENDFUNC

#IF IS_DEBUG_BUILD
PROC CREATE_WIDGETS()
	INT iPlayer
	TEXT_LABEL_15 tl63

	START_WIDGET_GROUP("DISTRACT COPS")  
		ADD_WIDGET_INT_READ_ONLY("iCheckpoint", serverBD.iCheckpoint)
		ADD_WIDGET_INT_READ_ONLY("iMinimumWantedLevel", serverBD.iMinimumWantedLevel)
		ADD_WIDGET_BOOL("Warp to Area", bWarpToArea)
				
		// Gameplay debug, sever only.
		START_WIDGET_GROUP("Server Only Gameplay") 
			ADD_WIDGET_BOOL("End mission", bHostEndMissionNow)			
		STOP_WIDGET_GROUP()		
		
		// Data about the server
		START_WIDGET_GROUP("Server BD") 
			ADD_WIDGET_INT_SLIDER("S. Game state", serverBD.iServerGameState,-1, HIGHEST_INT,1)
		STOP_WIDGET_GROUP()	

		// Data about the clients. * = You.
		START_WIDGET_GROUP("Client BD")  				
			REPEAT NUM_NETWORK_PLAYERS iPlayer
				tl63 = "Player "
				tl63 += iPlayer
				IF iPlayer = PARTICIPANT_ID_TO_INT()
					tl63 += "*"
				ENDIF
				START_WIDGET_GROUP(tl63)
					ADD_WIDGET_INT_SLIDER("Game state", playerBD[iPlayer].iGameState,-1, HIGHEST_INT,1)
				STOP_WIDGET_GROUP()
			ENDREPEAT				
		STOP_WIDGET_GROUP()	
		
	STOP_WIDGET_GROUP()
ENDPROC		

PROC UPDATE_WIDGETS()
	//Warp player to Distract Area
	IF bWarpToArea = TRUE
		IF NET_WARP_TO_COORD(serverBD.vDistractLocation, 0, FALSE, FALSE)
			NET_PRINT_TIME() NET_PRINT("     ---------->     DISTRACT COPS -  UPDATE_WIDGETS - bWarpToArea DONE    <----------     ") NET_NL()
			bWarpToArea = FALSE
		ELSE
			NET_PRINT_TIME() NET_PRINT("     ---------->     DISTRACT COPS -  UPDATE_WIDGETS - bWarpToArea IN PROGRESS    <----------     ") NET_NL()
		ENDIF
	ENDIF
ENDPROC

#ENDIF

//PURPOSE: Adds an Area Blip for the Distraction Location
PROC ADD_AREA_BLIP()
	IF DOES_BLIP_EXIST(AreaBlip)
		IF SHOULD_HIDE_LOCAL_UI()
		OR ARE_TEMP_HIDDEN_BLIPS_ACTIVE()
			SET_BLIP_DISPLAY(AreaBlip, DISPLAY_NOTHING)
		ELSE
			SET_BLIP_DISPLAY(AreaBlip, DISPLAY_BOTH)
		ENDIF
	ELSE
		AreaBlip = ADD_BLIP_FOR_RADIUS(serverBD.vDistractLocation, serverBD.fDistractionRadius)
		SET_BLIP_COLOUR(AreaBlip, BLIP_COLOUR_PURPLE)
		SET_BLIP_ALPHA(AreaBlip, 220)
		SHOW_HEIGHT_ON_BLIP(AreaBlip, FALSE)
		SET_BLIP_NAME_FROM_TEXT_FILE(AreaBlip, "DCP_BLIPN")
		IF SHOULD_HIDE_LOCAL_UI()
			SET_BLIP_DISPLAY(AreaBlip, DISPLAY_NOTHING)
		ENDIF
		NET_PRINT_TIME() NET_PRINT("     ---------->     DISTRACT COPS -  AREA BLIP ADDED ") NET_NL()
	ENDIF
ENDPROC


//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  ENTITY CREATION                  /////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

//PURPOSE: Runs through the max num participents and does relevant checks
PROC MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
	//Staggered Checks
	IF serverBD.iServerGameState = GAME_STATE_RUNNING
		//REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iStaggeredParticipant
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iStaggeredParticipant))
				PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iStaggeredParticipant))
				//PED_INDEX PlayerPedId = GET_PLAYER_PED(PlayerId)
				
				//** CHECKS THAT DON'T NEED THE PLAYER TO BE ALIVE **//
				
				//** CHECKS THAT REQUIRE THE PLAYER TO BE ALIVE **//
				IF IS_NET_PLAYER_OK(PlayerId)
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_IsAnyPlayerInArea)
						IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_InArea)
							bPlayerInArea = TRUE
							SET_BIT(serverBD.iServerBitSet, biS_IsAnyPlayerInArea)
							PRINTLN("     ---------->     DISTRACT COPS - biS_IsAnyPlayerInArea SET - PLAYER IN AREA = ", GET_PLAYER_NAME(PlayerId))
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		//ENDREPEAT
	ENDIF
	
	iStaggeredParticipant++
	
	//Reset Staggered Loop
	IF iStaggeredParticipant = NETWORK_GET_MAX_NUM_PARTICIPANTS()
		IF bPlayerInArea = FALSE
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_IsAnyPlayerInArea)
				CLEAR_BIT(serverBD.iServerBitSet, biS_IsAnyPlayerInArea)
				PRINTLN("     ---------->     DISTRACT COPS - biS_IsAnyPlayerInArea CLEARED - NO LONGER ANY PLAYER IN THE AREA")
			ENDIF
		ELSE
			bPlayerInArea = FALSE
		ENDIF
		iStaggeredParticipant = 0
		//NET_PRINT_TIME() NET_PRINT(" ---> DISTRACT COPS -  MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - iStaggeredParticipant RESET") NET_NL()
	ENDIF
ENDPROC

//PURPOSE: Cleans up variables when leaving the Area
PROC LEAVE_AREA_CLEANUP()
	RESET_NET_TIMER(iRewardTimer)
	RESET_NET_TIMER(iWantedLevelDelayTimer)
	CLEAR_PLAYER_SPAWN_AREA()
	CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InArea)
	CLEAR_BIT(iBoolsBitSet, biEnteredArea)
	
	SET_BIT(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_PLAYER_IN_DISTRACT_COPS_AREA)
	PRINTLN("     ---------->     DISTRACT COPS -  iNGABI_PLAYER_IN_DISTRACT_COPS_AREA SET")
	
	IF NOT IS_BIT_SET(iBoolsBitSet, biStopTrackingAward)
		SET_BIT(iBoolsBitSet, biStopTrackingAward)
		PRINTLN("     ---------->     DISTRACT COPS - LEAVE_AREA_CLEANUP - biStopTrackingAward SET - PLAYER LEFT AREA")
	ENDIF
	
	PRINTLN("     ---------->     DISTRACT COPS - LEAVE_AREA_CLEANUP - DONE")
ENDPROC

//PURPOSE: Checks if the player is leaving or has left the main Area
PROC LEAVE_AREA_CHECKS()
	//LEFT AREA
	IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), serverBD.vDistractLocation, <<serverBD.fDistractionRadius, serverBD.fDistractionRadius, 1000>>)
		LEAVE_AREA_CLEANUP()
		playerBD[PARTICIPANT_ID_TO_INT()].eDistractStage = eAD_IDLE
		NET_PRINT_TIME() NET_PRINT("     ---------->     DISTRACT COPS - PLAYER LEFT DISTRACT AREA - PLAYER STAGE = eAD_IDLE - FROM IN AREA    <----------     ") NET_NL()
	
	//WARNING AREA
	ELIF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), serverBD.vDistractLocation, <<serverBD.fLeaveWarningRadius, serverBD.fLeaveWarningRadius, 950>>)
		IF NOT IS_BIT_SET(iBoolsBitSet, biLeaveWarningHelpDone)
		AND IS_BIT_SET(iBoolsBitSet, biEnteredArea)
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			AND NOT IS_CUSTOM_MENU_ON_SCREEN()
				IF !SHOULD_HIDE_LOCAL_UI()
					PRINT_HELP("DCP_LEAVE")	//You are leaving the area for distracting the Cops.
				ENDIF
				SET_BIT(iBoolsBitSet, biLeaveWarningHelpDone)
				PRINTLN("     ---------->     DISTRACT COPS - biLeaveWarningHelpDone SET")
			ENDIF
		ENDIF
	
	//IN AREA
	ELSE
		IF NOT IS_BIT_SET(iBoolsBitSet, biEnteredArea)
			SET_BIT(iBoolsBitSet, biEnteredArea)
			PRINTLN("     ---------->     DISTRACT COPS - biEnteredArea SET")
		ENDIF
		IF IS_BIT_SET(iBoolsBitSet, biLeaveWarningHelpDone)
			CLEAR_BIT(iBoolsBitSet, biLeaveWarningHelpDone)
			PRINTLN("     ---------->     DISTRACT COPS - biLeaveWarningHelpDone CLEARED")
		ENDIF
	ENDIF
	
ENDPROC

//PURPOSE: Makes sure the player has the correct wanted level.
PROC MAINTAIN_WANTED_LEVEL()
	IF HAS_NET_TIMER_EXPIRED(iWantedLevelDelayTimer, WANTED_LEVEL_DELAY)
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < serverBD.iMinimumWantedLevel
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), serverBD.iMinimumWantedLevel)
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			PRINTLN("     ---------->     DISTRACT COPS - MAINTAIN_WANTED_LEVEL - SET PLAYER WANTED LEVEL TO ", serverBD.iMinimumWantedLevel)
		ENDIF
		UPDATE_WANTED_POSITION_THIS_FRAME(PLAYER_ID())
	ENDIF
ENDPROC

//PURPOSE: Controls giving the player XP over time.
PROC GIVE_CHECKPOINT_REWARDS()
	IF HAS_NET_TIMER_EXPIRED(iRewardTimer, REWARD_DELAY)
		
		//Increment Stat
		SWITCH serverBD.iMinimumWantedLevel
			CASE 2	INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_TIMES_COMPLETE_DISTCOPS1, 1)	BREAK
			CASE 3	INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_TIMES_COMPLETE_DISTCOPS2, 1)	BREAK
			CASE 4	INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_TIMES_COMPLETE_DISTCOPS3, 1)	BREAK
			CASE 5	INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_TIMES_COMPLETE_DISTCOPS4, 1)	BREAK
		ENDSWITCH
		
		//Give XP
		GIVE_LOCAL_PLAYER_FM_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "XPT_DSC", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_DISTRACT_COPS, g_sMPTunables.iXP_Reward_Event_Distract_Cops*serverBD.iMinimumWantedLevel)
		playerBD[PARTICIPANT_ID_TO_INT()].iRewardCounter++
		RESET_NET_TIMER(iRewardTimer)
		PRINTLN("     ---------->     DISTRACT COPS - GIVE_CHECKPOINT_REWARDS - CHECKPOINT REWARD GIVEN - iRewardCounter = ", playerBD[PARTICIPANT_ID_TO_INT()].iRewardCounter)
		PRINTLN("     ---------->     DISTRACT COPS - GIVE_CHECKPOINT_REWARDS - CHECKPOINT REWARD GIVEN - iXP_Reward_Event_Distract_Cops = ", g_sMPTunables.iXP_Reward_Event_Distract_Cops)
		PRINTLN("     ---------->     DISTRACT COPS - GIVE_CHECKPOINT_REWARDS - CHECKPOINT REWARD GIVEN - XP = ", g_sMPTunables.iXP_Reward_Event_Distract_Cops*serverBD.iMinimumWantedLevel)
	ENDIF
ENDPROC

//PURPOSE: Gives the player end Job rewards
PROC GIVE_END_REWARDS()
	IF NOT IS_BIT_SET(iBoolsBitSet, biEndRewardGiven)
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InArea)
		AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) >= (serverBD.iMinimumWantedLevel-1)
		AND playerBD[PARTICIPANT_ID_TO_INT()].iRewardCounter < 10
			playerBD[PARTICIPANT_ID_TO_INT()].iRewardCounter++ //ADD ONE FOR THE END
			PRINTLN("     ---------->     DISTRACT COPS - GIVE_END_REWARDS - IN AREA - ADD ONE - iRewardCounter = ", playerBD[PARTICIPANT_ID_TO_INT()].iRewardCounter)
		ENDIF
		
		PRINTLN("     ---------->     DISTRACT COPS - GIVE_END_REWARDS - iRewardCounter = ", playerBD[PARTICIPANT_ID_TO_INT()].iRewardCounter)
		PRINTLN("     ---------->     DISTRACT COPS - GIVE_END_REWARDS - icash_Reward_Event_Distract_Cops = ", g_sMPTunables.icash_Reward_Event_Distract_Cops)
		
		IF playerBD[PARTICIPANT_ID_TO_INT()].iRewardCounter > 0
			INT iCash = (playerBD[PARTICIPANT_ID_TO_INT()].iRewardCounter*g_sMPTunables.icash_Reward_Event_Distract_Cops)
			g_i_cashForEndEventShard = iCash
			
			PRINTLN("     ---------->     DISTRACT COPS - GIVE_END_REWARDS - [MAGNATE_GANG_BOSS] - Cash before boss cut: $", iCash)
			GB_HANDLE_GANG_BOSS_CUT(iCash)
			PRINTLN("     ---------->     DISTRACT COPS - GIVE_END_REWARDS - [MAGNATE_GANG_BOSS] - Cash after boss cut: $", iCash)
			
			IF iCash > 0
				GIVE_LOCAL_PLAYER_FM_CASH_END_OF_MISSION(iCash)
				AMBIENT_JOB_DATA AJdata
				PRINTLN("     ---------->     DISTRACT COPS - GIVE_END_REWARDS - CASH GIVEN = $", iCash)
				
				IF USE_SERVER_TRANSACTIONS()
					INT iTransactionID
					TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_AMBIENT_JOB_DISTRACT_COPS, iCash, iTransactionID)
				ELSE
					NETWORK_EARN_FROM_AMBIENT_JOB(iCash, "AM_DISTRACT_COPS", AJdata)
				ENDIF
			ENDIF
			//Text Message
			IF GET_RANDOM_BOOL()
				Send_MP_Txtmsg_From_CharSheetID(CHAR_LESTER, "DSC_PASS0")
			ELSE
				Send_MP_Txtmsg_From_CharSheetID(CHAR_LESTER, "DSC_PASS1")
			ENDIF
			
			SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_GENERIC_MIDSIZED, iCash, "DSC_CASH", "DSC_BIGM", HUD_COLOUR_WHITE) //Distract Cops - $~1~
			
			//INCREMENT AWARD STAT
			IF IS_BIT_SET(iBoolsBitSet, biStartTrackingAward)
				IF NOT IS_BIT_SET(iBoolsBitSet, biStopTrackingAward)
					INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_ODISTRACTCOPSNOEATH, 1)
					PRINTLN("     ---------->     DISTRACT COPS - GIVE_END_REWARDS - MP_AWARD_ODISTRACTCOPSNOEATH +1")
				ENDIF
			ENDIF
		ENDIF
				
		SET_BIT(iBoolsBitSet, biEndRewardGiven)
		PRINTLN("     ---------->     DISTRACT COPS - GIVE_END_REWARDS - DONE")
	ENDIF
ENDPROC

//PURPOSE: Displays the remaining time for this job.
PROC DRAW_TIMER()
	IF GET_CURRENT_LANGUAGE() = LANGUAGE_RUSSIAN
	OR GET_CURRENT_LANGUAGE() = LANGUAGE_KOREAN
	OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE
	OR GET_CURRENT_LANGUAGE() = LANGUAGE_JAPANESE
	OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE_SIMPLIFIED
		SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
	ELSE
		SET_MIDDLE_TITLE_POSITION_HUD_THIS_FRAME()
	ENDIF
	
	IF (serverBD.iDistractionTime-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.DistractionCompletedTimer)) >= 0
		DRAW_GENERIC_TIMER((serverBD.iDistractionTime-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.DistractionCompletedTimer)), "DCP_TIMER", 0, TIMER_STYLE_USEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_DONTCARE)
	ELSE
		DRAW_GENERIC_TIMER(0, "DCP_TIMER", 0, TIMER_STYLE_USEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_DONTCARE)
	ENDIF
ENDPROC

//PURPOSE: Tracks the player spending the full time in the area to get an award
PROC MAINTAIN_IN_AREA_FULL_TIME_AWARD()
	IF NOT IS_BIT_SET(iBoolsBitSet, biStopTrackingAward)
		IF NOT IS_BIT_SET(iBoolsBitSet, biStartTrackingAward)
			IF serverBD.iCheckpoint = 0
				SET_BIT(iBoolsBitSet, biStartTrackingAward)
				PRINTLN("     ---------->     DISTRACT COPS - MAINTAIN_IN_AREA_FULL_TIME_AWARD - biStartTrackingAward SET")
			ELSE
				SET_BIT(iBoolsBitSet, biStopTrackingAward)
				PRINTLN("     ---------->     DISTRACT COPS - MAINTAIN_IN_AREA_FULL_TIME_AWARD - biStopTrackingAward SET - PAST CHECKPOINT 0")
			ENDIF
		ELSE
			IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
				SET_BIT(iBoolsBitSet, biStopTrackingAward)
				PRINTLN("     ---------->     DISTRACT COPS - MAINTAIN_IN_AREA_FULL_TIME_AWARD - biStopTrackingAward SET - PLAYER DEAD")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Deals with timer, rewards, wanted level, etc.
PROC PROCESS_INSIDE_AREA()
	MAINTAIN_WANTED_LEVEL()
	GIVE_CHECKPOINT_REWARDS()
	DRAW_TIMER()
	MAINTAIN_IN_AREA_FULL_TIME_AWARD()
ENDPROC

//PURPOSE: Returns the current Min wanted level Based on the Checkpoint.
FUNC INT GET_WANTED_LEVEL_BASED_ON_CHECKPOINT()
	SWITCH serverBD.iCheckpoint
		CASE 0
		CASE 1
			RETURN 2
		CASE 2
		CASE 3
		CASE 4
		CASE 5
			RETURN 3
		CASE 6
		CASE 7
		CASE 8
			RETURN 4
		CASE 9
			RETURN 5
	ENDSWITCH
	
	RETURN serverBD.iMinimumWantedLevel
ENDFUNC

//PURPOSE: Controls the total time and increments the Wanted Level and ends the mission accordingly.
PROC MAINTAIN_DISTRACT_COP_TIME_SERVER()
	IF NOT HAS_NET_TIMER_EXPIRED(serverBD.DistractionCompletedTimer, serverBD.iDistractionTime)
		
		//Every 30sec increment a checkpoint
		IF HAS_NET_TIMER_EXPIRED(serverBD.DistractionCheckpointTimer, serverBD.iDistractionCheckpointTime)
			serverBD.iCheckpoint++
			serverBD.iMinimumWantedLevel = GET_WANTED_LEVEL_BASED_ON_CHECKPOINT()
			
			RESET_NET_TIMER(serverBD.DistractionCheckpointTimer)
			PRINTLN("     ---------->     DISTRACT COPS - MAINTAIN_DISTRACT_COP_TIME_SERVER - CHECKPOINT REACHED - CHECKPOINT = ", serverBD.iCheckpoint, " MIN WANTED LEVEL = ", serverBD.iMinimumWantedLevel)
		ENDIF
	
	//Move to End when timer is up
	ELSE
		serverBD.eDistractStage = eAD_CLEANUP
		NET_PRINT_TIME() NET_PRINT("     ---------->     DISTRACT COPS - SERVER STAGE = eAD_CLEANUP - FROM IN AREA   <----------     ") NET_NL()
	ENDIF
ENDPROC

PROC CONTROL_HELP_TEXT()
	IF NOT IS_BIT_SET(iBoolsBitSet, biHelpTextDone)
		IF HAS_PLAYER_BEEN_IN_SESSION_THIS_LONG(CRATE_DROP_DISPLAY_DELAY)
		AND GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
		AND serverBD.eDistractStage <= eAD_IN_AREA
		AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		AND NOT IS_PAUSE_MENU_ACTIVE()
		AND IS_NET_PLAYER_OK(PLAYER_ID())
		AND NOT IS_CUSTOM_MENU_ON_SCREEN()
		AND serverBD.iMinimumWantedLevel != 5	//TOO LATE 
			IF !SHOULD_HIDE_LOCAL_UI()
				PRINT_HELP("DCP_HELP1")	//Help Lester's contact by distracting the cops in the area marked with a purple circle.
			ENDIF
			NET_PRINT_TIME() NET_PRINT("     ---------->     DISTRACT COPS -  HELP TEXT DONE    <----------     ") NET_NL()
			SET_BIT(iBoolsBitSet, biHelpTextDone)
		ENDIF
	ENDIF
ENDPROC

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MISSION PROCS                  //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

//PURPOSE: Process the JOYRIDER stages for the Client
PROC PROCESS_DISTRACT_COPS_CLIENT()
	
	CONTROL_HELP_TEXT()
	
	IF DOES_BLIP_EXIST(AreaBlip)
		IF SHOULD_HIDE_LOCAL_UI()
		OR ARE_TEMP_HIDDEN_BLIPS_ACTIVE()
			SET_BLIP_DISPLAY(AreaBlip, DISPLAY_NOTHING)
		ELSE
			SET_BLIP_DISPLAY(AreaBlip, DISPLAY_BOTH)
		ENDIF
	ENDIF
	
	SWITCH playerBD[PARTICIPANT_ID_TO_INT()].eDistractStage
		CASE eAD_IDLE
			//In Area Check
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), serverBD.vDistractLocation, <<serverBD.fDistractionRadius, serverBD.fDistractionRadius, 1000>>)
				SET_PLAYER_SPAWN_AREA()
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InArea)
				SET_BIT(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_PLAYER_IN_DISTRACT_COPS_AREA)
				PRINTLN("     ---------->     DISTRACT COPS -  iNGABI_PLAYER_IN_DISTRACT_COPS_AREA SET")
				playerBD[PARTICIPANT_ID_TO_INT()].eDistractStage = eAD_IN_AREA
				NET_PRINT_TIME() NET_PRINT("     ---------->     DISTRACT COPS -  PLAYER STAGE = eAD_IN_AREA - FROM IDLE    <----------     ") NET_NL()
			ENDIF
			
			//Cleanup Check
			IF serverBD.eDistractStage > eAD_IN_AREA
				playerBD[PARTICIPANT_ID_TO_INT()].eDistractStage = eAD_CLEANUP
				NET_PRINT_TIME() NET_PRINT("     ---------->     DISTRACT COPS -  PLAYER STAGE = eAD_CLEANUP - FROM IDLE    <----------     ") NET_NL()
			ENDIF
		BREAK
		
		CASE eAD_IN_AREA
			
			LEAVE_AREA_CHECKS()
			PROCESS_INSIDE_AREA()
			
			IF serverBD.eDistractStage > eAD_IN_AREA
				playerBD[PARTICIPANT_ID_TO_INT()].eDistractStage = eAD_CLEANUP
				NET_PRINT_TIME() NET_PRINT("     ---------->     DISTRACT COPS -  PLAYER STAGE = eAD_CLEANUP - FROM IN AREA   <----------     ") NET_NL()
			ENDIF
		BREAK
						
		CASE eAD_CLEANUP
			GIVE_END_REWARDS()
		BREAK
	ENDSWITCH
ENDPROC

//PURPOSE: Process the JOYRIDER stages for the Server
PROC PROCESS_DISTRACT_COPS_SERVER()
	SWITCH serverBD.eDistractStage
		CASE eAD_IDLE
			//Move on once a player is in the Area
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_IsAnyPlayerInArea)
				serverBD.eDistractStage = eAD_IN_AREA
				NET_PRINT_TIME() NET_PRINT("     ---------->     DISTRACT COPS - SERVER STAGE = eAD_IN_AREA - FROM IDLE   <----------     ") NET_NL()
			//End if nobody arrives
			ELSE
				IF HAS_NET_TIMER_EXPIRED(serverBD.FailToStartDistractionTimer, FAIL_TO_START_DISTRACTION_TIME)
					//Send Ticker
					SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
					TickerEventData.TickerEvent = TICKER_EVENT_DISTRACT_COPS_NEVER_STARTED
					BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT())
					
					SET_BIT(serverBD.iServerBitSet, biS_SendFailTextMessage)
					
					NET_PRINT_TIME() NET_PRINT("     ---------->     DISTRACT COPS -  MISSION END - NOBODY REACHED THE DISTRACT AREA     <----------     ") NET_NL()
					serverBD.eDistractStage = eAD_CLEANUP
					NET_PRINT_TIME() NET_PRINT("     ---------->     DISTRACT COPS - SERVER STAGE = eAD_CLEANUP - FROM IDLE   <----------     ") NET_NL()
				ENDIF
			ENDIF
		BREAK
		
		CASE eAD_IN_AREA
			MAINTAIN_DISTRACT_COP_TIME_SERVER()
		BREAK
				
		CASE eAD_CLEANUP
			
		BREAK
	ENDSWITCH
ENDPROC

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                      //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT(MP_MISSION_DATA missionScriptArgs)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
		#IF IS_DEBUG_BUILD	
			NET_PRINT_TIME() NET_PRINT("MP: Starting mission ") NET_NL()
		#ENDIF
		
		// Planery out all the initial game starting duties. 
		PROCESS_PRE_GAME(missionScriptArgs)	
		
		// Setup widgets. Make sure this is done after PROCESS_PRE_GAME()
		#IF IS_DEBUG_BUILD
			CREATE_WIDGETS()
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD NET_LOG("DISTRACT COPS -  NETWORK_IS_GAME_IN_PROGRESS = FALSE")	#ENDIF
		NET_PRINT_TIME() NET_PRINT("     ---------->     DISTRACT COPS -  MISSION END - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP P     <----------     ") NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
	
	// Main loop
	WHILE TRUE

		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
				
		// If we have a match end event, bail
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		OR SHOULD_MP_AMBIENT_SCRIPT_TERMINATE(MPAM_TYPE_DISTRACT_COPS)
			#IF IS_DEBUG_BUILD NET_LOG("SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")	#ENDIF
			NET_PRINT_TIME() NET_PRINT("     ---------->     DISTRACT COPS -  MISSION END - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE - SCRIPT CLEANUP B     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		
		//Check if player is in a tutorial session and end
		IF NETWORK_IS_IN_TUTORIAL_SESSION()
			#IF IS_DEBUG_BUILD NET_LOG("PLAYER IN TUT SESSION")	#ENDIF
			NET_PRINT_TIME() NET_PRINT("     ---------->     DISTRACT COPS -  MISSION END - PLAYER IN TUTORIAL SESSION - SCRIPT CLEANUP C     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		
		// Deal with the debug
		#IF IS_DEBUG_BUILD		
			UPDATE_WIDGETS()
		#ENDIF		
	
		// If we need to leave the mission, make sure we just straight to the end stage and terminate the script - DON'T NEED FOR AMBIENT MISSION
		/*IF SHOULD_PLAYER_LEAVE_MP_MISSION() 
			#IF IS_DEBUG_BUILD
			NET_PRINT_TIME() NET_PRINT( GET_MP_MISSION_NAME(thisMission), " Leave mission = TRUE \n")
			#ENDIF
			playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_LEAVE
			#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_LEAVE")	#ENDIF
		ENDIF*/
		
		// -----------------------------------
		// Process your game logic.....
		SWITCH GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT())
			
			// Wait untill the server gives the all go before moving on
			CASE GAME_STATE_INI
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
				
					ADD_AREA_BLIP()
					
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING
					NET_PRINT_TIME() NET_PRINT("     ---------->     DISTRACT COPS -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_RUNNING")	#ENDIF
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					NET_PRINT_TIME() NET_PRINT("     ---------->     DISTRACT COPS -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 1    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY")	#ENDIF
				ENDIF
			BREAK
			
			// Main gameplay state.
			CASE GAME_STATE_RUNNING
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					PROCESS_DISTRACT_COPS_CLIENT()
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					GIVE_END_REWARDS()
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					NET_PRINT_TIME() NET_PRINT("     ---------->     DISTRACT COPS -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 2   <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY")	#ENDIF
				ENDIF
			BREAK
			
			CASE GAME_STATE_TERMINATE_DELAY 
				SET_MISSION_FINISHED(serverBD.MissionTerminateDelayTimer)
		      	IF IS_MISSION_READY_TO_CLEANUP(serverBD.MissionTerminateDelayTimer)
		            playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					NET_PRINT_TIME() NET_PRINT("     ---------->     DISTRACT COPS -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 3    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 3")	#ENDIF
		      	ENDIF
			BREAK
			
			// The game stage the local player is placed when we want him to leave a mission
			CASE GAME_STATE_LEAVE
				playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
				NET_PRINT_TIME() NET_PRINT("     ---------->     DISTRACT COPS -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 2    <----------     ") NET_NL()
				#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 2")	#ENDIF
			FALLTHRU
			
			//Cleans up then terminates the mission
			CASE GAME_STATE_END
				NET_PRINT_TIME() NET_PRINT("     ---------->     DISTRACT COPS -  SCRIPT CLEANUP A     <----------     ") NET_NL()
				SCRIPT_CLEANUP()
			BREAK

		ENDSWITCH
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			
			SWITCH GET_SERVER_MISSION_STATE()
				
				// For CnC group missions, we may want to wait for all players to have launched the script
				CASE GAME_STATE_INI
					serverBD.iServerGameState = GAME_STATE_RUNNING
					NET_PRINT_TIME() NET_PRINT("     ---------->     DISTRACT COPS -  serverBD.iServerGameState = GAME_STATE_RUNNING    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_RUNNING")	#ENDIF
				BREAK
				
				// Look for game end conditions
				CASE GAME_STATE_RUNNING
					
					MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
					PROCESS_DISTRACT_COPS_SERVER()
					
					#IF IS_DEBUG_BUILD
						IF bHostEndMissionNow
						OR MPGlobalsAmbience.bKillActiveEvent
						MPGlobalsAmbience.bKillActiveEvent = FALSE
							serverBD.iServerGameState = GAME_STATE_END
							NET_PRINT_TIME() NET_PRINT("     ---------->     DISTRACT COPS -  serverBD.iServerGameState = GAME_STATE_END 1    <----------     ") NET_NL()
							#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 1")	#ENDIF
						ENDIF
					#ENDIF	
					
					IF HAVE_MISSION_END_CONDITIONS_BEEN_MET()
						serverBD.iServerGameState = GAME_STATE_END
						NET_PRINT_TIME() NET_PRINT("     ---------->     DISTRACT COPS -  serverBD.iServerGameState = GAME_STATE_END 4    <----------     ") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 4")	#ENDIF
					ENDIF
					
				BREAK
				
				CASE GAME_STATE_END
				BREAK
				
			ENDSWITCH
			
		ENDIF
		
	ENDWHILE
	
ENDSCRIPT
