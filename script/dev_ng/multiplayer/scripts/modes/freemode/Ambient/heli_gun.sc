
// ----------------------------------------------------- \\
// ----------------------------------------------------- \\
//                                                       \\
// SCRIPT:                                               \\
//                                                       \\
// net_heli_gun.sc                                 		 \\
//                                                       \\
// ----------------------------------------------------- \\
//                                                       \\
// DESCRIPTION:                                          \\
//                                                       \\
// Runs the heli gun that heli passengers can fire.      \\
//                                                       \\
// ----------------------------------------------------- \\
//                                                       \\
// AUTHOR:                                               \\
//                                                       \\
// William.Kennedy@RockstarNorth.com                     \\
// 														 \\	
// HACKER:												 \\
// 														 \\
// Philip.O'Duffy@RockstarNorth.com						 \\	
// ----------------------------------------------------- \\
// ----------------------------------------------------- \\



/// PURPOSE: Headers.
USING "globals.sch"
USING "net_include.sch"
USING "net_scoring_common.sch"
USING "net_mission.sch"
USING "commands_water.sch"
USING "net_heli_gun_launching.sch"
USING "net_realty_armory_aircraft.sch"
USING "net_activity_creator_components.sch"

CONST_INT AVENGER_TURRET_COUNT 3
CONST_INT IAA_TURRET_COUNT 4
CONST_INT ENABLE_ENHANCED_HUD_FEATURES 0
CONST_INT MAX_HELIHUD_COORD_COUNT 1
CONST_INT MAX_SCANNABLE_PEDS 1
CONST_INT FIRE_ROCKET_COOLDOWN_TIME 2000
CONST_INT FIRE_AOC_TURRET_COOLDOWN 	500
CONST_INT FIRE_TRUCK_TURRET_COOLDOWN_TIME 1000
CONST_INT AT_TURRET_HELP_TIME_TO_CHECK	7500 

TWEAK_FLOAT cfVOLATOL_T1_PITCH_MIN 6.50
TWEAK_FLOAT cfVOLATOL_T1_PITCH_MAX -70.0
TWEAK_FLOAT cfVOLATOL_T2_PITCH_MIN 50.0
TWEAK_FLOAT cfVOLATOL_T2_PITCH_MAX 5.0
TWEAK_FLOAT cfVOLATOL_TURRET_DYNAMIC_FOV_MIN	27.9
TWEAK_FLOAT cfVOLATOL_TURRET_STATIC_FOV_MIN		10.0
TWEAK_FLOAT cfVOLATOL_TURRET_DYNAMIC_FOV_HEIGHT 40.0
CONST_FLOAT	cfVOLATOL_VEH_HEIGHT 2.0

TWEAK_FLOAT cfAKULA_T0_PITCH_MAX 	-70.000
TWEAK_FLOAT cfAKULA_T0_PITCH_MIN 	10.00

TWEAK_FLOAT cfAKULA_T1_PITCH_MIN 	16.50
TWEAK_FLOAT cfAKULA_T1_PITCH_MAX 	-70.000
TWEAK_FLOAT cfAKULA_T1_YAW_MIN 		-180.0
TWEAK_FLOAT cfAKULA_T1_YAW_MAX 		180.0
TWEAK_FLOAT cfAKULA_T1_YAW_ARC 170.0

TWEAK_FLOAT cfAVENGER_T0_YAW_MIN	-100.0 //does nothing?
TWEAK_FLOAT cfAVENGER_T0_YAW_MAX	100.0	
TWEAK_FLOAT cfAVENGER_T0_PITCH_MIN	15.0 //0.0 //48.0
TWEAK_FLOAT cfAVENGER_T0_PITCH_MAX	-80.0 

TWEAK_FLOAT cfAVENGER_T1_YAW_MIN	-100.0
TWEAK_FLOAT cfAVENGER_T1_YAW_MAX	100.0	
TWEAK_FLOAT cfAVENGER_T1_PITCH_MIN	80.0	
TWEAK_FLOAT cfAVENGER_T1_PITCH_MAX	-20.0 //-45.0

TWEAK_FLOAT cfAVENGER_T2_YAW_MIN	-100.0
TWEAK_FLOAT cfAVENGER_T2_YAW_MAX	100.0
TWEAK_FLOAT cfAVENGER_T2_PITCH_MIN	15.0	//0.0 //48.0
TWEAK_FLOAT cfAVENGER_T2_PITCH_MAX	-80.0 	//-80.0



//TWEAK_FLOAT cfRAY_TRACE_DISTANCE	15.0

USING "helicopterHUD.sch"

USING "net_wait_zero.sch"
USING "rc_helper_functions.sch"

#IF IS_DEBUG_BUILD 
USING "net_debug.sch"
USING "profiler.sch"
#ENDIF

ENUM eGAME_STATE
	eGAMESTATE_INI = 0,
	eGAMESTATE_RUNNING,
	eGAMESTATE_LEAVE,
	eGAMESTATE_END
ENDENUM

ENUM eHELI_GUN_STATE
	eHELIGUNSTATE_OFF = 0,
	eHELIGUNSTATE_LAUNCHING,
	eHELIGUNSTATE_ON
ENDENUM

structHelicopterHUDVars sKevCamVars

SCRIPT_TIMER tdNetTimer
SCRIPT_TIMER stRocketCoolDownTimerAlt

SCRIPT_TIMER tHelpTimer
BOOL bSetWantedMultiplierForTruckTurret

MODEL_NAMES eHeliModel
MODEL_NAMES eRocketModel
BOOL bShownTurnOnHeliGunHelp
BOOL bWeaponsDisabledForPassive = FALSE
BOOL bShownRocketsInUseHelp
BOOL bShownOnMissionHelp
BOOL bPilotAndAllowed
BOOL bIAmCoPilot
BOOL bIAmTurretPilot1
BOOL bIAmTurretPilot2
INT iCannotMoveHelpDisplayCount
INT iBusyHelpDisplayCount
CONST_INT CANNOT_MOVE_HELP_DISPLAY_COUNT_MAX 1
CONST_INT PLAYER_BUSY_HELP_DISPLAY_COUNT_MAX 1
BOOL bPilotExists
BOOL bPilotDead
BOOL bCoPilotExists
BOOL bCoPilotDead
BOOL bDisabledRockets
BOOL bHeliDriveable 
BOOL bSetSniperAudioFlag

FLOAT fDeadzoneMag = 0.10

PED_INDEX TurretPilot1Ped
BOOL bTurretPilot1Exists
BOOL bTurretPilot1Dead
PLAYER_INDEX PlayerIdTurretPilot1

PED_INDEX TurretPilot2Ped 
BOOL bTurretPilot2Exists
BOOL bTurretPilot2Dead
PLAYER_INDEX PlayerIdTurretPilot2 
SCRIPT_TIMER stBombInputTimer
SCRIPT_TIMER stBombCamExitTimer
BOOL bVehicleSeatCamInputAccept
BOOL bVehicleSeatShuffleInputAccept
VEHICLE_SEAT vsSeatToShuffleTo = VS_ANY_PASSENGER
//STRING sScaleFormMovie
BOOL bStillShuffling
INT iPreviousPedConfigFlag
CONST_INT PPCF_BS_PREVENT_SHUFFLE_HAS_BEEN_MODIFIED 	0
CONST_INT PPCF_BS_PREVENT_SHUFFLE_ENABLED		1

// Some variables to track audio stuff //
INTERIOR_TURRET_AUDIO_SCENE m_ePreviousAudioScene 	= ITAS_INVALID
TURRET_FIRE_SOUND_SET 		m_eTurretFireSounds	 	= TFSS_INVALID

VECTOR vLerpValue = << 0.1, 0.1, 0.1 >>
FLOAT fThreshold = 0.0

INT iUseHeliHelpSwitch
INT iAudioLevelFlag

#IF IS_DEBUG_BUILD
BOOL bCustomOffset = FALSE
VECTOR vCannonOffset = <<-2.6829, 9.5693, 1.7578>>
VECTOR db_vCamOffset = <<-1, -1, -1>>
FLOAT m_fDebugVehDistFromGround 
BOOL m_bDebugCantGetGround
#ENDIF

VEHICLE_INDEX 	viHeli
MODEL_NAMES 	modHeliModel
PED_INDEX 		pilotPed, coPilotPed
PLAYER_INDEX 	PlayerIdPilot
PLAYER_INDEX 	PlayerIdCoPilot
BLIP_INDEX 		ArrowStrokeBlipID
BLIP_INDEX 		ArrowStrokeBlipBg

SCRIPT_TIMER sFadeDelayTimer

// ****************
// BROADCAST DATA.
// ****************

// The server broadcast data.
// Everyone can read this data, only the server can update it. 
STRUCT ServerBroadcastData

	eGAME_STATE eGameState
	
ENDSTRUCT
ServerBroadcastData serverBD

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it. 
STRUCT PlayerBroadcastData
	
	eGAME_STATE eGameState
	eHELI_GUN_STATE eHeliGunState
ENDSTRUCT
CONST_INT PLAYER_COUNT_MAX 2
PlayerBroadcastData playerBD[PLAYER_COUNT_MAX]
#IF IS_DEBUG_BUILD

FUNC STRING GET_GAME_STATE_NAME(eGAME_STATE eState)
	
	SWITCH eState
		CASE eGAMESTATE_INI			RETURN "INI"
		CASE eGAMESTATE_RUNNING		RETURN "RUNNING"
		CASE eGAMESTATE_LEAVE		RETURN "LEAVE"
		CASE eGAMESTATE_END			RETURN "END"
	ENDSWITCH
	
	RETURN "NOT_IN_SWITCH"
	
ENDFUNC

FUNC STRING GET_HELI_GUN_STATE_NAME(eHELI_GUN_STATE eState)
	
	SWITCH eState
		CASE eHELIGUNSTATE_OFF			RETURN "OFF"
		CASE eHELIGUNSTATE_LAUNCHING	RETURN "LAUNCHING"
		CASE eHELIGUNSTATE_ON			RETURN "ON"
	ENDSWITCH
	
	RETURN "NOT_IN_SWITCH"
	
ENDFUNC

#ENDIF

FUNC BOOL HELI_EXISTS_WITH_MODEL(MODEL_NAMES model)
	RETURN modHeliModel = model AND model != DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

PROC SET_GAME_STATE(eGAME_STATE eState)
	IF PARTICIPANT_ID_TO_INT() != -1
		#IF IS_DEBUG_BUILD
			IF (eState != playerBD[PARTICIPANT_ID_TO_INT()].eGameState)
				CDEBUG1LN(DEBUG_NET_GUN_TURRET, " - setting game state to ", GET_GAME_STATE_NAME(eState))NET_NL()
			ENDIF
		#ENDIF
	
	
		playerBD[PARTICIPANT_ID_TO_INT()].eGameState = eState
	ENDIF
	
ENDPROC

PROC SET_HELI_GUN_STATE(eHELI_GUN_STATE eState)
	IF PARTICIPANT_ID_TO_INT() != -1
		#IF IS_DEBUG_BUILD
			IF (eState != playerBD[PARTICIPANT_ID_TO_INT()].eHeliGunState)
				CDEBUG1LN(DEBUG_NET_GUN_TURRET, " SET_HELI_GUN_STATE - setting heli gun state to ", GET_HELI_GUN_STATE_NAME(eState))NET_NL()
			ENDIF
		#ENDIF
	
	
		playerBD[PARTICIPANT_ID_TO_INT()].eHeliGunState = eState
	ENDIF
	
ENDPROC


TWEAK_FLOAT fBombushkaTurretPanFactor  0.002
#IF IS_DEBUG_BUILD

WIDGET_GROUP_ID widgetsId
INT iCreateWidgetsStage
BOOL bCreateWidgets
BOOL bTerminateScript
TEXT_WIDGET_ID twID_gunState
BOOL bAllowDriverAccess = FALSE
BOOL bDoHeliTaggingExtraDebug

BOOL bAutoPilotRun
BOOL bAPClearedTasks = TRUE
INT iAPTarget = 0
INT iAPTargetMax = 3//13
BOOL bAPFirstTarget = TRUE

FUNC VECTOR GET_TARGET(INT iTarget)
	
	SWITCH iTarget
		CASE 0 RETURN <<-706.2826, -1383.9208, 501.7660>> 	
		CASE 1 RETURN <<101.2933, -1075.0054, 364.9690>> 	
		DEFAULT RETURN <<-345.5414, -504.2520, 453.2757>>		
	ENDSWITCH
	//SWITCH iTarget + 1
//		CASE 1 RETURN <<275.7955, -347.2136, 384.1318>>
//		CASE 2 RETURN <<94.1090, 273.3078, 394.0559>>
//		CASE 3 RETURN <<-141.7547, 480.0696, 413.0086>>
//		CASE 4 RETURN <<-341.4733, 516.1962, 398.8824>>
//		CASE 5 RETURN <<-531.5126, 397.0917, 393.8857>>
//		CASE 6 RETURN <<-586.7456, 245.2967, 388.8610>>
//		CASE 7 RETURN <<-598.4315, -160.1694, 386.5756>>
//		CASE 8 RETURN <<-594.2236, -447.8876, 420.6947>>
//		CASE 9 RETURN <<-500.3179, -592.7109, 440.4191>>
//		CASE 10 RETURN <<-376.6170, -675.9476, 446.8520>>
//		CASE 11 RETURN <<-30.1925, -795.8780, 43.2252>>
//		CASE 12 RETURN <<178.9994, -801.6310, 459.9987>>
//		DEFAULT RETURN <<339.4850, -734.5038, 445.2837>>
//	ENDSWITCH
ENDFUNC

//returns true if the target has changed
FUNC BOOL HAS_TARGET_CHANGED(VEHICLE_INDEX viVeh, VECTOR& vTarget)
	vTarget = GET_TARGET(iAPTarget)
	IF IS_ENTITY_AT_COORD( viVeh, vTarget, <<150,150,150>>)
		iAPTarget = ( iAPTarget + 1 ) % 3
		vTarget = GET_TARGET(iAPTarget)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC UPDATE_WIDGETS()
	
	IF bTerminateScript
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, " - widget bTerminateScript = TRUE.")
	    SET_GAME_STATE(eGAMESTATE_END)
	ENDIF
	
	IF PARTICIPANT_ID_TO_INT() != -1
		SET_CONTENTS_OF_TEXT_WIDGET(twID_gunState, GET_HELI_GUN_STATE_NAME(playerBD[PARTICIPANT_ID_TO_INT()].eHeliGunState))
	ENDIF
	
	IF bAutoPilotRun
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX vehID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF DOES_ENTITY_EXIST(vehID)
			AND IS_VEHICLE_DRIVEABLE(vehID)
			AND NETWORK_HAS_CONTROL_OF_ENTITY(vehID)
				//Example TASK_PLANE_MISSION(PLAYER_PED_ID(), vehPlane, NULL, NULL, <<vGoToPos.X, vGoToPos.Y, vPlaneCoords.Z>>, MISSION_GOTO, 50.0, 0.1, -1, 30, 20)
				IF NOT IS_ENTITY_IN_AIR(vehID)
					SET_ENTITY_COORDS(vehID, GET_TARGET(iAPTargetMax)/*GET_ENTITY_COORDS(vehID)+ <<0.0, 0.0, 450.0>>*/)
					//SET_ENTITY_ROTATION(vehID, <<0,0,0>>)
					SET_ENTITY_HEADING(vehID, 300) 
				ENDIF
				
				
				VECTOR vTarget
				IF HAS_TARGET_CHANGED(vehID ,vTarget)
				OR bAPFirstTarget
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					TASK_PLANE_MISSION(PLAYER_PED_ID(), vehID, NULL, NULL, vTarget, MISSION_GOTO, 50.0, 0.1, -1, 30, 20)
					bAPFirstTarget = FALSE
					bAPClearedTasks = FALSE
				ENDIF
//				VECTOR vPlaneForward = GET_ENTITY_FORWARD_VECTOR(vehID)
//				
//				VECTOR vAPTarget = GET_ENTITY_COORDS(vehID) + ROTATE_VECTOR_ABOUT_Z((<<100,100,100>> * vPlaneForward), 5 )
//				
//				IF bAPClearedTasks
//					TASK_PLANE_MISSION(PLAYER_PED_ID(), vehID, NULL, NULL, vAPTarget, MISSION_GOTO, 50.0, 0.1, -1, 30, 20)
//					bAPClearedTasks = FALSE
//				ENDIF
			ENDIF
		ENDIF
	ELIF NOT bAPClearedTasks
		bAPClearedTasks= TRUE
		CLEAR_PED_TASKS(PLAYER_PED_ID())
	ENDIF
ENDPROC

PROC MAINTAIN_WIDGETS()
	
	SWITCH iCreateWidgetsStage
		
		CASE 0

			widgetsId = START_WIDGET_GROUP("Heli Gun")
			ADD_WIDGET_BOOL("Create Heli Gun Widgets", bCreateWidgets)
			STOP_WIDGET_GROUP()

			iCreateWidgetsStage++

		BREAK

		CASE 1

			IF bCreateWidgets

				DELETE_WIDGET_GROUP(widgetsId)
				widgetsId = START_WIDGET_GROUP("Heli Gun")
					ADD_WIDGET_INT_SLIDER("CamRotationSpeed = ", g_BombushkaTurretCamRotSpeedMultiplier, -1, 15, 1)
					ADD_WIDGET_INT_SLIDER("CamZoomSpeed = ", g_BombushkaTurretCamZoomSpeedMultiplier, -1, 15, 1)
					ADD_WIDGET_BOOL("Terminate Script", bTerminateScript)
					ADD_WIDGET_BOOL("g_bTerminateHeliGunScript", g_bTerminateHeliGunScript)
					twID_gunState = ADD_TEXT_WIDGET("State")
					ADD_WIDGET_BOOL("bAllowDriverAccess", bAllowDriverAccess)
					ADD_WIDGET_BOOL("bDoHeliTaggingExtraDebug", bDoHeliTaggingExtraDebug)
					ADD_WIDGET_VECTOR_SLIDER("vLerpValue", vLerpValue, -10000.0, 10000, 0.000001)
					ADD_WIDGET_FLOAT_SLIDER("fThreshold", fThreshold, -10000.0, 10000.0, 0.000001)
					#IF SCRIPT_PROFILER_ACTIVE 
						CREATE_SCRIPT_PROFILER_WIDGET() 
					#ENDIF
					
					ADD_WIDGET_BOOL("Enable Custom Offset", bCustomOffset)
					IF bCustomOffset
						ADD_WIDGET_VECTOR_SLIDER("Cannon Offset", vCannonOffset, -10000.0, 10000.0, 0.1)
					ENDIF
					ADD_WIDGET_VECTOR_SLIDER("Camera Offset", db_vCamOffset, -10000.0, 10000.0, 0.1)		
					
					ADD_WIDGET_FLOAT_SLIDER("fDeadzone", fDeadzoneMag,0, 1.0, 0.01)
					ADD_WIDGET_FLOAT_READ_ONLY("chopperPanFactor", sKevCamVars.chopperPanFactor)
					
					ADD_WIDGET_FLOAT_SLIDER("Bombushka pan factor", fBombushkaTurretPanFactor, 0, 1, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("Max FOV", sKevCamVars.maxFOV, 0, 120, 1)
					ADD_WIDGET_FLOAT_SLIDER("Min FOV", sKevCamVars.minFOV, 0, 120, 1)
					
					ADD_WIDGET_BOOL("Run auto-pilot", bAutoPilotRun)
					ADD_WIDGET_INT_READ_ONLY("Target idx", iAPTarget)
					
					ADD_WIDGET_INT_READ_ONLY("g_iInteriorTurretSeat", g_iInteriorTurretSeat)
					
					START_WIDGET_GROUP("Volatol turrets")
						ADD_WIDGET_FLOAT_SLIDER("cfVOLATOL_NOSE_CAM_X", cfVOLATOL_NOSE_CAM_X, -15.0, 15.0, 0.01)
						ADD_WIDGET_FLOAT_SLIDER("cfVOLATOL_NOSE_CAM_Y", cfVOLATOL_NOSE_CAM_Y, -15.0, 15.0, 0.01)
						ADD_WIDGET_FLOAT_SLIDER("cfVOLATOL_NOSE_CAM_Z", cfVOLATOL_NOSE_CAM_Z, -15.0, 15.0, 0.01)
						
						ADD_WIDGET_FLOAT_SLIDER("cfVOLATOL_ROOF_CAM_X", cfVOLATOL_ROOF_CAM_X, -15.0, 15.0, 0.01)
						ADD_WIDGET_FLOAT_SLIDER("cfVOLATOL_ROOF_CAM_Y", cfVOLATOL_ROOF_CAM_Y, -15.0, 15.0, 0.01)
						ADD_WIDGET_FLOAT_SLIDER("cfVOLATOL_ROOF_CAM_Z", cfVOLATOL_ROOF_CAM_Z, -15.0, 15.0, 0.01)
						
						ADD_WIDGET_FLOAT_SLIDER("cfVOLATOL_T1_PITCH_MIN", cfVOLATOL_T1_PITCH_MIN, -180, 180, 1)
						ADD_WIDGET_FLOAT_SLIDER("cfVOLATOL_T1_PITCH_MAX", cfVOLATOL_T1_PITCH_MAX, -180, 180, 1)
						ADD_WIDGET_FLOAT_SLIDER("cfVOLATOL_T2_PITCH_MIN", cfVOLATOL_T2_PITCH_MIN, -180, 180, 1)
						ADD_WIDGET_FLOAT_SLIDER("cfVOLATOL_T2_PITCH_MAX", cfVOLATOL_T2_PITCH_MAX, -180, 180, 1)
						
						ADD_WIDGET_FLOAT_SLIDER("cfVOLATOL_TURRET_DYNAMIC_FOV_HEIGHT", cfVOLATOL_TURRET_DYNAMIC_FOV_HEIGHT, 0, 400, 1)
						ADD_WIDGET_FLOAT_SLIDER("cfVOLATOL_TURRET_DYNAMIC_FOV_MIN", cfVOLATOL_TURRET_DYNAMIC_FOV_MIN, 0, 28.0, 1)
						ADD_WIDGET_FLOAT_READ_ONLY("Dist from ground", m_fDebugVehDistFromGround)
						ADD_WIDGET_BOOL("Can't get ground coord? (RO)", m_bDebugCantGetGround)
						
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("Akula")
						START_WIDGET_GROUP("Copilot gun")
							ADD_WIDGET_FLOAT_SLIDER("cfAKULA_T0_PITCH_MAX", cfAKULA_T0_PITCH_MAX, -180, 180, 1)
							ADD_WIDGET_FLOAT_SLIDER("cfAKULA_T0_PITCH_MIN", cfAKULA_T0_PITCH_MIN, -180, 180, 1)
						STOP_WIDGET_GROUP()
						START_WIDGET_GROUP("Rear turrets")
							ADD_WIDGET_FLOAT_SLIDER("pos.x", cfAKULA_T1_X, -20.0, 20.0, 0.01)
							ADD_WIDGET_FLOAT_SLIDER("pos.y", cfAKULA_T1_Y, -20.0, 20.0, 0.01)
							ADD_WIDGET_FLOAT_SLIDER("pos.z", cfAKULA_T1_Z, -20.0, 20.0, 0.01)
							ADD_WIDGET_FLOAT_SLIDER("rot.x", cfAKULA_T1_RX, -180.0, 180.0, 1.0)
							ADD_WIDGET_FLOAT_SLIDER("rot.Y", cfAKULA_T1_RY, -180.0, 180.0, 1.0)
							ADD_WIDGET_FLOAT_SLIDER("rot.Z", cfAKULA_T1_RZ, -180.0, 180.0, 1.0)
							ADD_WIDGET_FLOAT_SLIDER("pitch.max", cfAKULA_T1_PITCH_MAX, -180, 180, 1)
							ADD_WIDGET_FLOAT_SLIDER("pitch.min", cfAKULA_T1_PITCH_MIN, -180, 180, 1)
							ADD_WIDGET_FLOAT_SLIDER("yaw.max", cfAKULA_T1_YAW_MAX, -180, 180, 1)
							ADD_WIDGET_FLOAT_SLIDER("yaw.min", cfAKULA_T1_YAW_MIN, -180, 180, 1)
							ADD_WIDGET_FLOAT_SLIDER("yaw override", cfAKULA_T1_YAW_ARC, -360.0, 360.0, 1)
						STOP_WIDGET_GROUP()
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("Avenger turrets")
						START_WIDGET_GROUP("Turret 0")
							ADD_WIDGET_FLOAT_SLIDER("pos.x", cfAVENGER_T0_X, -20.0, 20.0, 0.01)
							ADD_WIDGET_FLOAT_SLIDER("pos.y", cfAVENGER_T0_Y, -20.0, 20.0, 0.01)
							ADD_WIDGET_FLOAT_SLIDER("pos.z", cfAVENGER_T0_Z, -20.0, 20.0, 0.01)
							ADD_WIDGET_FLOAT_SLIDER("rot.x", cfAVENGER_T0_RX, -180.0, 180.0, 1.0)
							ADD_WIDGET_FLOAT_SLIDER("rot.Y", cfAVENGER_T0_RY, -180.0, 180.0, 1.0)
							ADD_WIDGET_FLOAT_SLIDER("rot.Z", cfAVENGER_T0_RZ, -180.0, 180.0, 1.0)
							ADD_WIDGET_FLOAT_SLIDER("pitch.min", cfAVENGER_T0_PITCH_MIN, -180.0, 180.0, 1.0)
							ADD_WIDGET_FLOAT_SLIDER("pitch.max", cfAVENGER_T0_PITCH_MAX, -180.0, 180.0, 1.0)
							ADD_WIDGET_FLOAT_SLIDER("yaw.min", cfAVENGER_T0_YAW_MIN, -180.0, 180.0, 1.0)
							ADD_WIDGET_FLOAT_SLIDER("yaw.max", cfAVENGER_T0_YAW_MAX, -180.0, 180.0, 1.0)
						STOP_WIDGET_GROUP()
						START_WIDGET_GROUP("Turret 1")
							ADD_WIDGET_FLOAT_SLIDER("pos.x", cfAVENGER_T1_X, -20.0, 20.0, 0.01)
							ADD_WIDGET_FLOAT_SLIDER("pos.y", cfAVENGER_T1_Y, -20.0, 20.0, 0.01)
							ADD_WIDGET_FLOAT_SLIDER("pos.z", cfAVENGER_T1_Z, -20.0, 20.0, 0.01)
							ADD_WIDGET_FLOAT_SLIDER("rot.x", cfAVENGER_T1_RX, -180.0, 180.0, 1.0)
							ADD_WIDGET_FLOAT_SLIDER("rot.y", cfAVENGER_T1_RY, -180.0, 180.0, 1.0)
							ADD_WIDGET_FLOAT_SLIDER("rot.z", cfAVENGER_T1_RZ, -180.0, 180.0, 1.0)
							ADD_WIDGET_FLOAT_SLIDER("pitch.min", cfAVENGER_T1_PITCH_MIN, -180.0, 180.0, 1.0)
							ADD_WIDGET_FLOAT_SLIDER("pitch.max", cfAVENGER_T1_PITCH_MAX, -180.0, 180.0, 1.0)
							ADD_WIDGET_FLOAT_SLIDER("yaw.min", cfAVENGER_T1_YAW_MIN, -180.0, 180.0, 1.0)
							ADD_WIDGET_FLOAT_SLIDER("yaw.max", cfAVENGER_T1_YAW_MAX, -180.0, 180.0, 1.0)
						STOP_WIDGET_GROUP()
						START_WIDGET_GROUP("Turret 2")
							ADD_WIDGET_FLOAT_SLIDER("pos.x", cfAVENGER_T2_X, -20.0, 20.0, 0.01)
							ADD_WIDGET_FLOAT_SLIDER("pos.y", cfAVENGER_T2_Y, -20.0, 20.0, 0.01)
							ADD_WIDGET_FLOAT_SLIDER("pos.z", cfAVENGER_T2_Z, -20.0, 20.0, 0.01)
							ADD_WIDGET_FLOAT_SLIDER("rot.x", cfAVENGER_T2_RX, -180.0, 180.0, 1.0)
							ADD_WIDGET_FLOAT_SLIDER("rot.y", cfAVENGER_T2_RY, -180.0, 180.0, 1.0)
							ADD_WIDGET_FLOAT_SLIDER("rot.z", cfAVENGER_T2_RZ, -180.0, 180.0, 1.0)
							ADD_WIDGET_FLOAT_SLIDER("pitch.min", cfAVENGER_T2_PITCH_MIN, -180.0, 180.0, 1.0)
							ADD_WIDGET_FLOAT_SLIDER("pitch.max", cfAVENGER_T2_PITCH_MAX, -180.0, 180.0, 1.0)
							ADD_WIDGET_FLOAT_SLIDER("yaw.min", cfAVENGER_T2_YAW_MIN, -180.0, 180.0, 1.0)
							ADD_WIDGET_FLOAT_SLIDER("yaw.max", cfAVENGER_T2_YAW_MAX, -180.0, 180.0, 1.0)
						STOP_WIDGET_GROUP()
					STOP_WIDGET_GROUP()
					START_WIDGET_GROUP("Misc")
						ADD_WIDGET_BOOL("Draw Yaw and Pitch", sKevCamVars.bDebugDrawRelRot)
					STOP_WIDGET_GROUP()
				STOP_WIDGET_GROUP()

				iCreateWidgetsStage++

			ENDIF

		BREAK

		CASE 2

			UPDATE_WIDGETS()
			//MAINTAIN_HELIHUD_WIDGETS(sKevCamVars)
		BREAK

	ENDSWITCH
	
	
//	IF NOT ARE_VECTORS_EQUAL(db_vCamOffset, <<-1, -1, -1>>)
//		
//		IF NOT ARE_VECTORS_EQUAL(db_vCamOffset, sKevCamVars.vTurretCamOffset)
//			sKevCamVars.vTurretCamOffset = db_vCamOffset
//			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "HUD.vTurretCamOffset = ", sKevCamVars.vTurretCamOffset)
//			ATTACH_CAM_TO_ENTITY(sKevCamVars.camChopper,sKevCamVars.camEntity,sKevCamVars.vTurretCamOffset+<<0,0,-1.5>>,TRUE)
//		ENDIF
//	ENDIF
	

ENDPROC

#ENDIF

FUNC BOOL IS_CO_PILOT_CAM_ON()
	IF bCoPilotExists
		IF NOT bCoPilotDead
			INT iPlayerIdx = NATIVE_TO_INT(PlayerIdCoPilot)
			IF iPlayerIdx <> -1
				PRINTLN( "[OCH] Co-pilot participant ID: ",iPlayerIdx , " Heli gun cam on?: ", IS_BIT_SET(GlobalplayerBD_FM[iPlayerIdx].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bInHeliGunCam)  )
				RETURN IS_BIT_SET(GlobalplayerBD_FM[iPlayerIdx].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bInHeliGunCam)//GlobalplayerBD_FM[iPlayerIdx].bInHeliGunCam
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_TURRET1_CAM_ON()	
	IF bTurretPilot1Exists
		IF NOT bTurretPilot1Dead
			INT iPlayerIdx = NATIVE_TO_INT(PlayerIdTurretPilot1)
			IF iPlayerIdx <> -1
				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[OCH] Front turret participant ID: ",iPlayerIdx , " Heli gun cam on?: ", IS_BIT_SET(GlobalplayerBD_FM[iPlayerIdx].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bInHeliGunCam)  )
				RETURN IS_BIT_SET(GlobalplayerBD_FM[iPlayerIdx].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bInHeliGunCam)//GlobalplayerBD_FM[iPlayerIdx].bInHeliGunCam
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_TURRET2_CAM_ON()	
	IF bTurretPilot2Exists
		IF NOT bTurretPilot2Dead
			INT iPlayerIdx = NATIVE_TO_INT(PlayerIdTurretPilot2)
			IF iPlayerIdx <> -1
				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[OCH] Rear turret participant ID: ",iPlayerIdx , " Heli gun cam on?: ", IS_BIT_SET(GlobalplayerBD_FM[iPlayerIdx].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bInHeliGunCam)  )
				RETURN IS_BIT_SET(GlobalplayerBD_FM[iPlayerIdx].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bInHeliGunCam)// GlobalplayerBD_FM[iPlayerIdx].bInHeliGunCam
			ENDIF
		ENDIF
	ENDIF	
	RETURN FALSE
ENDFUNC


FUNC BOOL SHOULD_GO_STRAIGHT_INTO_CAMERA()
	
	IF NOT MPGlobalsAmbience.bGoneStraightIntoValkyrieFpModeforThisHeli
		IF (MPGlobalsAmbience.lastValkyrieForStraightIntoFpMode = viHeli)
			MPGlobalsAmbience.bGoneStraightIntoValkyrieFpModeforThisHeli = TRUE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC


/// PURPOSE:
///    Used to check when the player is outside the hanger and a bunch of conditions are 
///    met that mean it is completely safe to display help text.
FUNC BOOL IS_SAFE_TO_DISPLAY_HELP()
	RETURN 	NOT bShownTurnOnHeliGunHelp
			AND NOT IS_PLAYER_IN_HANGAR(PLAYER_ID())
			AND NOT IS_SCREEN_FADING_IN()
			AND NOT IS_SCREEN_FADED_OUT()
			AND NOT IS_SCREEN_FADING_OUT()
			AND IS_SCREEN_FADED_IN() 
			AND NOT IS_RADAR_HIDDEN()
			AND IS_MINIMAP_RENDERING()
			AND NOT IS_CUSTOM_MENU_ON_SCREEN()
			AND NOT IS_PAUSE_MENU_ACTIVE()
			AND NOT NETWORK_IS_IN_MP_CUTSCENE()
			AND NOT IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
			AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
ENDFUNC



FUNC BOOL IS_PLAYER_SHUFFLING_IN_VEHICLE()
//	IF eHeliModel = INT_TO_ENUM(MODEL_NAMES, HASH("BOMBUSHKA"))
	IF IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(viHeli)
		RETURN bStillShuffling
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

ENUM TURRET_CAM_CHANGE_DIR
	FORWARDS
	,BACKWARDS
ENDENUM


FUNC BOOL IS_IAA_TURRET_CAM_AVAILABLE(INT iTurretCamID)
	// loop through other players in aircraft to see if they're using the turret seat with GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iTurretSeat != g_iInteriorTurretSeat
	IF iTurretCamID = -1
		RETURN FALSE
	ENDIF
	INT iPlayerInteriorInstance = globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iInstance 
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		PLAYER_INDEX playerID
		playerID = INT_TO_PLAYERINDEX(i)
		
		IF IS_NET_PLAYER_OK(playerID)
		AND playerID != PLAYER_ID()
			IF globalPlayerBD[NATIVE_TO_INT(playerID)].SimpleInteriorBD.iInstance = iPlayerInteriorInstance // If we're in the same interior
				IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iTurretSeat = iTurretCamID
					CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "IS_AVENGER_TURRET_CAM_AVAILABLE: TurretCam not available, already in use by ", GET_PLAYER_NAME(playerID))
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN TRUE
ENDFUNC


FUNC INT NEXT_FREE_IAA_SEAT(TURRET_CAM_CHANGE_DIR eDirection)
	
//	CDEBUG1LN(DEBUG_NET_GUN_TURRET, "NEXT_FREE_SEAT: g_iInteriorTurretSeat = ", g_iInteriorTurretSeat)

	IF IS_ENTITY_DEAD(viHeli) // I don't want the next avaliable seat loop infinitley if no seats are available because the vehicle isn't alive	
		CWARNINGLN(DEBUG_NET_ACTIVITY_CREATOR, "NEXT_FREE_IAA_SEAT: viHeli is dead")
		RETURN -1
	ENDIF
	
		
	INT iReturnSeat
	IF eDirection = FORWARDS
		iReturnSeat = (g_iInteriorTurretSeat % IAA_TURRET_COUNT ) + 1 //1 -> AVENGER_TURRET_COUNT

		WHILE NOT IS_IAA_TURRET_CAM_AVAILABLE(iReturnSeat)

			iReturnSeat = (iReturnSeat % IAA_TURRET_COUNT ) + 1 //1 -> AVENGER_TURRET_COUNT
		ENDWHILE

		RETURN iReturnSeat
	ELSE	
		iReturnSeat = PICK_INT(g_iInteriorTurretSeat = 1, IAA_TURRET_COUNT, g_iInteriorTurretSeat - 1)

		WHILE NOT IS_IAA_TURRET_CAM_AVAILABLE(iReturnSeat)


			iReturnSeat = PICK_INT(iReturnSeat <= 1, IAA_TURRET_COUNT , iReturnSeat - 1)
		ENDWHILE
//		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "NEXT_FREE_SEAT: BACKWARDS: Recaculated: iReturnSeat = ", iReturnSeat , ", current seat = ", g_iInteriorTurretSeat)
		RETURN iReturnSeat
	ENDIF
ENDFUNC

FUNC BOOL IS_AVENGER_TURRET_CAM_AVAILABLE(INT iTurretCamID)
	// loop through other players in aircraft to see if they're using the turret seat with GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iTurretSeat != g_iInteriorTurretSeat
	IF iTurretCamID = -1
		RETURN FALSE
	ENDIF
	INT iPlayerInteriorInstance = globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iInstance 
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		PLAYER_INDEX playerID
		playerID = INT_TO_PLAYERINDEX(i)
		
		IF IS_NET_PLAYER_OK(playerID)
		AND playerID != PLAYER_ID()
			IF globalPlayerBD[NATIVE_TO_INT(playerID)].SimpleInteriorBD.iInstance = iPlayerInteriorInstance // If we're in the same interior
				IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iTurretSeat = iTurretCamID
					CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "IS_AVENGER_TURRET_CAM_AVAILABLE: TurretCam not available, already in use by ", GET_PLAYER_NAME(playerID))
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN TRUE
ENDFUNC



FUNC BOOL IS_TURRET_SEAT_AVAILABLE(INT iTurretSeat)
	SWITCH iTurretSeat
		CASE 1
			
			IF IS_AVENGER_TURRET_STATION_A_AVAILABLE()
			AND IS_AVENGER_TURRET_CAM_AVAILABLE(iTurretSeat)
				RETURN TRUE
			ENDIF
		BREAK

		CASE 2
			IF IS_AVENGER_TURRET_STATION_B_AVAILABLE()
			AND IS_AVENGER_TURRET_CAM_AVAILABLE(iTurretSeat)
				RETURN TRUE
			ENDIF
		BREAK

		CASE 3
			IF IS_AVENGER_TURRET_STATION_C_AVAILABLE()
			AND IS_AVENGER_TURRET_CAM_AVAILABLE(iTurretSeat)
				RETURN TRUE
			ENDIF
		BREAK
		DEFAULT
			CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "IS_TURRET_SEAT_AVAILABLE: DEFAULT CASE: iTurretSeat = ", iTurretSeat)
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC


FUNC INT NEXT_FREE_SEAT(TURRET_CAM_CHANGE_DIR eDirection)
	
//	CDEBUG1LN(DEBUG_NET_GUN_TURRET, "NEXT_FREE_SEAT: g_iInteriorTurretSeat = ", g_iInteriorTurretSeat)
	
	IF IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
		IF IS_ENTITY_DEAD( g_viAvengerIamIn)
			RETURN -1
		ENDIF
	ELIF IS_PLAYER_IN_CREATOR_AIRCRAFT(PLAYER_ID())
		IF IS_ENTITY_DEAD( MPGlobalsAmbience.vehCreatorAircraft) // I don't want the next avaliable seat loop infinitley if no seats are available because the vehicle isn't alive
			RETURN -1
		ENDIF
	ENDIF
		
	INT iReturnSeat
	IF eDirection = FORWARDS
		iReturnSeat = (g_iInteriorTurretSeat % AVENGER_TURRET_COUNT) + 1 //1 -> AVENGER_TURRET_COUNT
//		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "NEXT_FREE_SEAT: FORWARDS: iReturnSeat = ", iReturnSeat )
		WHILE NOT IS_TURRET_SEAT_AVAILABLE(iReturnSeat)
//			DEBUG_PRINTCALLSTACK()
//			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "NEXT_FREE_SEAT: FORWARDS: iReturnSeat = ", iReturnSeat, ", Not available, recalculating")
			iReturnSeat = (iReturnSeat % AVENGER_TURRET_COUNT) + 1 //1 -> AVENGER_TURRET_COUNT
		ENDWHILE
//		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "NEXT_FREE_SEAT: FORWARDS: Recaculated: iReturnSeat = ", iReturnSeat )
		RETURN iReturnSeat
	ELSE	
		iReturnSeat = PICK_INT(g_iInteriorTurretSeat = 1, AVENGER_TURRET_COUNT, g_iInteriorTurretSeat - 1)
//		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "NEXT_FREE_SEAT: BACKWARDS: iReturnSeat = ", iReturnSeat )
		WHILE NOT IS_TURRET_SEAT_AVAILABLE(iReturnSeat)
//			DEBUG_PRINTCALLSTACK()
//			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "NEXT_FREE_SEAT: BACKWARDS: iReturnSeat = ", iReturnSeat, ", Not available, recalculating")
			iReturnSeat = PICK_INT(iReturnSeat <= 1, AVENGER_TURRET_COUNT, iReturnSeat - 1)
		ENDWHILE
//		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "NEXT_FREE_SEAT: BACKWARDS: Recaculated: iReturnSeat = ", iReturnSeat )
		RETURN iReturnSeat
	ENDIF
ENDFUNC


FUNC STRING GET_TURRET_INSTRUCTION_HELP_STRING(VEHICLE_INDEX veh)

	IF g_iShouldLaunchTruckTurret != -1
		IF IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(g_ownerOfArmoryTruckPropertyIAmIn)
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciALLOW_MOC_ENTRY_THIS_MISSION)
			RETURN "HUNTGUN_T_3"
		ELSE
			RETURN "HUNTGUN_T_2b"
		ENDIF
	ENDIF
	
	BOOL bBackwards
	BOOL bForwards
	
	IF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
		INT iNextBackSeat = NEXT_FREE_IAA_SEAT(BACKWARDS)
		
		IF iNextBackSeat != g_iInteriorTurretSeat
			bBackwards = IS_IAA_TURRET_CAM_AVAILABLE(iNextBackSeat)
			IF bBackwards
				SWITCH iNextBackSeat
					CASE 1
						IF NOT IS_IAA_TURRET_CAM_AVAILABLE(1)
							bBackwards = FALSE
						ENDIF
					BREAK
					CASE 2
						IF NOT IS_IAA_TURRET_CAM_AVAILABLE(2)
							bBackwards = FALSE
						ENDIF
					BREAK
					CASE 3
						IF NOT IS_IAA_TURRET_CAM_AVAILABLE(3)
							bBackwards = FALSE
						ENDIF
					BREAK
					CASE 4
						IF NOT IS_IAA_TURRET_CAM_AVAILABLE(4)
							bBackwards = FALSE
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		ELSE
			bBackwards = FALSE
		ENDIF
		
		INT iNextForwardSeat = NEXT_FREE_IAA_SEAT(FORWARDS)
		IF iNextForwardSeat != g_iInteriorTurretSeat
			bForwards = IS_IAA_TURRET_CAM_AVAILABLE(iNextForwardSeat)
			IF bForwards 
				SWITCH iNextForwardSeat
					CASE 1
						IF NOT IS_IAA_TURRET_CAM_AVAILABLE(1)
							bForwards = FALSE
						ENDIF
					BREAK
					CASE 2
						IF NOT IS_IAA_TURRET_CAM_AVAILABLE(2)
							bForwards = FALSE
						ENDIF
					BREAK
					CASE 3
						IF NOT IS_IAA_TURRET_CAM_AVAILABLE(3)
							bForwards = FALSE
						ENDIF
					BREAK
					CASE 4
						IF NOT IS_IAA_TURRET_CAM_AVAILABLE(4)
							bForwards = FALSE
						ENDIF
					BREAK
				ENDSWITCH
			ELSE
				bForwards = FALSE
			ENDIF
		ELSE
			bForwards = FALSE
		ENDIF
		
		SWITCH g_iInteriorTurretSeat
			CASE 1
				IF bBackwards
				AND bForwards
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_TURRET_INSTRUCTION_HELP_STRING: IAA_T_2_OSM1")
					RETURN "IAA_T_2_OSM1"
				ELIF bForwards
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_TURRET_INSTRUCTION_HELP_STRING: IAA_T_2_OSL1")
					RETURN "IAA_T_2_OSL1"
				ELIF bBackwards
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_TURRET_INSTRUCTION_HELP_STRING: IAA_T_2_OSR1")
					RETURN "IAA_T_2_OSR1"
				ELSE
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_TURRET_INSTRUCTION_HELP_STRING: IAA_T_2_OSN1")
					RETURN "IAA_T_2_OSN1"
				ENDIF
			BREAK
			
			CASE 2
				IF bBackwards
				AND bForwards
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_TURRET_INSTRUCTION_HELP_STRING: IAA_T_2_OSM2")
					RETURN "IAA_T_2_OSM2"
				ELIF bForwards
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_TURRET_INSTRUCTION_HELP_STRING: IAA_T_2_OSL2")
					RETURN "IAA_T_2_OSL2"
				ELIF bBackwards
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_TURRET_INSTRUCTION_HELP_STRING: IAA_T_2_OSR2")
					RETURN "IAA_T_2_OSR2"
				ELSE
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_TURRET_INSTRUCTION_HELP_STRING: IAA_T_2_OSN2")
					RETURN "IAA_T_2_OSN2"
				ENDIF
			BREAK
			
			CASE 3
				IF bBackwards
				AND bForwards
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_TURRET_INSTRUCTION_HELP_STRING: IAA_T_2_OSM3")
					RETURN "IAA_T_2_OSM3"
				ELIF bForwards
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_TURRET_INSTRUCTION_HELP_STRING: IAA_T_2_OSL3")
					RETURN "IAA_T_2_OSL3"
				ELIF bBackwards
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_TURRET_INSTRUCTION_HELP_STRING: IAA_T_2_OSR3")
					RETURN "IAA_T_2_OSR3"
				ELSE
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_TURRET_INSTRUCTION_HELP_STRING: IAA_T_2_OSN3")
					RETURN "IAA_T_2_OSN3"
				ENDIF
			BREAK
			
			CASE 4
				IF bBackwards
				AND bForwards
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_TURRET_INSTRUCTION_HELP_STRING: IAA_T_2_OSM4")
					RETURN "IAA_T_2_OSM4"
				ELIF bForwards
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_TURRET_INSTRUCTION_HELP_STRING: IAA_T_2_OSL4")
					RETURN "IAA_T_2_OSL4"
				ELIF bBackwards
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_TURRET_INSTRUCTION_HELP_STRING: IAA_T_2_OSR4")
					RETURN "IAA_T_2_OSR4"
				ELSE
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_TURRET_INSTRUCTION_HELP_STRING: IAA_T_2_OSN4")
					RETURN "IAA_T_2_OSN4"
				ENDIF
			BREAK
		ENDSWITCH	
//		RETURN "IAAGUN_1"
	ENDIF
	
	bForwards = FALSE
	bBackwards = FALSE
	
	IF IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
	OR IS_PLAYER_IN_CREATOR_AIRCRAFT(PLAYER_ID())
		
		INT iNextBackSeat = NEXT_FREE_SEAT(BACKWARDS)
		
		IF iNextBackSeat != g_iInteriorTurretSeat
			bBackwards = IS_AVENGER_TURRET_CAM_AVAILABLE(iNextBackSeat)
			IF bBackwards
				SWITCH iNextBackSeat
					CASE 1
						IF NOT IS_AVENGER_TURRET_STATION_A_AVAILABLE()
							bBackwards = FALSE
						ENDIF
					BREAK
					CASE 2
						IF NOT IS_AVENGER_TURRET_STATION_B_AVAILABLE()
							bBackwards = FALSE
						ENDIF
					BREAK
					CASE 3
						IF NOT IS_AVENGER_TURRET_STATION_C_AVAILABLE()
							bBackwards = FALSE
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		ELSE
			bBackwards = FALSE
		ENDIF
		
		INT iNextForwardSeat = NEXT_FREE_SEAT(FORWARDS)
		IF iNextForwardSeat != g_iInteriorTurretSeat
			bForwards = IS_AVENGER_TURRET_CAM_AVAILABLE(iNextForwardSeat)
			IF bForwards 
				SWITCH NEXT_FREE_SEAT(FORWARDS)
					CASE 1
						IF NOT IS_AVENGER_TURRET_STATION_A_AVAILABLE()
							bForwards = FALSE
						ENDIF
					BREAK
					CASE 2
						IF NOT IS_AVENGER_TURRET_STATION_B_AVAILABLE()
							bForwards = FALSE
						ENDIF
					BREAK
					CASE 3
						IF NOT IS_AVENGER_TURRET_STATION_C_AVAILABLE()
							bForwards = FALSE
						ENDIF
					BREAK
				ENDSWITCH
			ELSE
				bForwards = FALSE
			ENDIF
		ELSE
			bForwards = FALSE
		ENDIF
		
		SWITCH g_iInteriorTurretSeat
			CASE 1
				IF bBackwards
				AND bForwards
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_TURRET_INSTRUCTION_HELP_STRING: HUNTGUN_T_2_OSM")
					RETURN "HUNTGUN_T_2_OSM1"
				ELIF bForwards
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_TURRET_INSTRUCTION_HELP_STRING: HUNTGUN_T_2_OSL")
					RETURN "HUNTGUN_T_2_OSL1"
				ELIF bBackwards
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_TURRET_INSTRUCTION_HELP_STRING: HUNTGUN_T_2_OSR")
					RETURN "HUNTGUN_T_2_OSR1"
				ELSE
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_TURRET_INSTRUCTION_HELP_STRING: HUNTGUN_T_2_OSN")
					RETURN "HUNTGUN_T_2_OSN1"
				ENDIF
			BREAK
			
			CASE 2
				IF bBackwards
				AND bForwards
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_TURRET_INSTRUCTION_HELP_STRING: HUNTGUN_T_2_OSM")
					RETURN "HUNTGUN_T_2_OSM3"
				ELIF bForwards
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_TURRET_INSTRUCTION_HELP_STRING: HUNTGUN_T_2_OSL")
					RETURN "HUNTGUN_T_2_OSL3"
				ELIF bBackwards
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_TURRET_INSTRUCTION_HELP_STRING: HUNTGUN_T_2_OSR")
					RETURN "HUNTGUN_T_2_OSR3"
				ELSE
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_TURRET_INSTRUCTION_HELP_STRING: HUNTGUN_T_2_OSN")
					RETURN "HUNTGUN_T_2_OSN3"
				ENDIF
			BREAK
			
			CASE 3
				IF bBackwards
				AND bForwards
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_TURRET_INSTRUCTION_HELP_STRING: HUNTGUN_T_2_OSM")
					RETURN "HUNTGUN_T_2_OSM2"
				ELIF bForwards
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_TURRET_INSTRUCTION_HELP_STRING: HUNTGUN_T_2_OSL")
					RETURN "HUNTGUN_T_2_OSL2"
				ELIF bBackwards
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_TURRET_INSTRUCTION_HELP_STRING: HUNTGUN_T_2_OSR")
					RETURN "HUNTGUN_T_2_OSR2"
				ELSE
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_TURRET_INSTRUCTION_HELP_STRING: HUNTGUN_T_2_OSN")
					RETURN "HUNTGUN_T_2_OSN2"
				ENDIF
			BREAK
		ENDSWITCH	
	ENDIF
	
	IF DOES_ENTITY_EXIST(veh)
		SWITCH GET_ENTITY_MODEL(veh)
			CASE BOMBUSHKA
				RETURN "BOMBGUN_T_2c"
			BREAK
			CASE RHINO
				RETURN "BOMBGUN_T_2c"
			BREAK
			CASE AKULA
				RETURN "AKULAGUN_P2"
			BREAK
		ENDSWITCH
	ENDIF
	RETURN ""
ENDFUNC


PROC CLEAR_TURN_ON_HELI_HELP()	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HUNTGUN_1")
		CLEAR_HELP()
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HUNTGUN_1b")
		CLEAR_HELP()
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HUNTGUN_1c")
		CLEAR_HELP()
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HUNTGUN_3")
		CLEAR_HELP()
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HUNTGUN_3b")
		CLEAR_HELP()
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HUNTGUN_3c")
		CLEAR_HELP()
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BOMBGUN_1c")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BOMBGUN_1c1")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BOMBGUN_1c2")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BOMBGUN_2c")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BOMBGUN_2c1")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BOMBGUN_2c2")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BOMBGUN_3c")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BOMBGUN_3c1")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BOMBGUN_3c2")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BOMBGUN_FULL")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BOMBGUN_BUSY")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AKULAGUN_2")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AKULAGUN_1")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("VOLGUN_1c")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("VOLGUN_1c1")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("VOLGUN_1c2")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("VOLGUN_2c")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("VOLGUN_2c1")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("VOLGUN_2c2")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("VOLGUN_3c")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("VOLGUN_3c1")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("VOLGUN_3c2")
		CLEAR_HELP()
	ENDIF
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HUNTGUNH_1c")
		CLEAR_HELP()
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_TURRET_INSTRUCTION_HELP_STRING(viHeli))
		CLEAR_HELP()
	ENDIF
	
ENDPROC


FUNC BOOL IS_SEAT_SUITABLE_FOR_TURRET_CAM()
	IF GET_ENTITY_MODEL(viHeli) = VOLATOL
	AND bIAmCoPilot
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC


FUNC BOOL CAN_TURRET_PED_LAUNCH_TURRET_HUD()
	IF g_iShouldLaunchTruckTurret != -1
	OR g_iInteriorTurretSeat != -1
		IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "enter")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "enter_left")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "exit")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "exit_left")
		OR IsPedPerformingTask(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD)
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "CAN_TURRET_PED_LAUNCH_TURRET_HUD: returning false, player is playing enter anim")
			RETURN FALSE
		ENDIF
		
		IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "base")
		AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "computer_enter")
		AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "computer_exit")
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "CAN_TURRET_PED_LAUNCH_TURRET_HUD: returning false, player is not in the seat")
			RETURN FALSE
		ENDIF
		
		IF SHOULD_BAIL_FROM_TURRET()
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "CAN_TURRET_PED_LAUNCH_TURRET_HUD: returning false, SHOULD_BAIL_FROM_TURRET ")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
		IF gPlayerCreatorActivity = ACT_CREATOR_IAA_TURRET_SEAT
			IF g_eIAAGunCamerasEnabled != IAA_ALLOW_GUN_CAM
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_BLOCK_TURRET()
	
	IF NOT IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(PLAYER_ID())
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BHH_LEFTRANGE")
			RETURN TRUE
		ENDIF
		
		IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_FMBB_SECURITY_VAN
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL SHOULD_CAM_TURN_ON()
//	CDEBUG1LN(DEBUG_NET_GUN_TURRET, "SHOULD_CAM_TURN_ON")
	BOOL bAcceptButtonPressed
	//BOOL bgoStraightIn
//	DISPLAYER_DEBUG_CREATOR_INFO()
	IF NOT bIAmCoPilot
	AND NOT bIAmTurretPilot1
	AND NOT bIAmTurretPilot2
	AND NOT bPilotAndAllowed
	AND IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(viHeli)
		RETURN FALSE
	ENDIF
	
	IF SHOULD_BLOCK_TURRET()
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_HANGAR( PLAYER_ID())
		RETURN FALSE
	ENDIF
	
//	IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
//		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "SHOULD_CAM_TURN_ON: IS_PLAYER_IN_DEFUNCT_BASE = TRUE")
//	ELSE
//		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "SHOULD_CAM_TURN_ON: IS_PLAYER_IN_DEFUNCT_BASE = FALSE")
//	ENDIF
	IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
	OR SHOULD_BAIL_FROM_TURRET()
		
		RETURN FALSE
	ELSE
		
	ENDIF
	IF g_bMissionSeatSwapping
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "SHOULD_CAM_TURN_ON returning FALSE as g_bMissionSeatSwapping is set")
		RETURN FALSE
	ENDIF
	
	SCRIPTTASKSTATUS eTaskStatus 
	eTaskStatus = GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT)
	IF eTaskStatus = PERFORMING_TASK
	OR eTaskStatus = WAITING_TO_START_TASK
	OR GET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_IsSeatShuffling) 
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "SHOULD_CAM_TURN_ON returning FALSE - Player is shuffling!")
		RETURN FALSE  
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TUR_GR")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TUR_WATER")
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "SHOULD_CAM_TURN_ON returning FALSE - camera is underwater or underground!")
		RETURN FALSE
	ENDIF

	
	IF NOT IS_PAUSE_MENU_ACTIVE()
	AND NOT IS_CUSTOM_MENU_ON_SCREEN()
	AND NOT IS_PHONE_ONSCREEN()
	AND NOT g_bMissionEnding
	AND NOT g_bBikerInSellHeli
		IF IS_PLAYER_SHUFFLING_IN_VEHICLE()
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
			AND iBusyHelpDisplayCount < PLAYER_BUSY_HELP_DISPLAY_COUNT_MAX
				CLEAR_TURN_ON_HELI_HELP()
				//HELP TEXT "You cannot move to turret camera while moving to another seat."
				PRINT_HELP("BOMBGUN_BUSY") 
				iBusyHelpDisplayCount++	
			ENDIF
		ELSE
			CONTROL_ACTION caInput
			caInput = INPUT_CONTEXT
			CONTROL_TYPE ctInput
			ctInput = PLAYER_CONTROL
	//		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "g_iShouldLaunchTruckTurret = ", g_iShouldLaunchTruckTurret )
			IF g_iShouldLaunchTruckTurret != -1
			OR g_iInteriorTurretSeat != -1
				ctInput = FRONTEND_CONTROL
				caInput = INPUT_CELLPHONE_SELECT
	//			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "g_iShouldLaunchTruckTurret != -1")
			ELSE
			
			ENDIF
			
			//-- 3641496 - Looks like g_viGunRunTruckTailerIamIn gets wiped when you enter the bunker, invalidating vHeli
			//-- Need to update vheli to the 'new' g_viGunRunTruckTailerIamIn, if it becomes valid again.
			IF g_iShouldLaunchTruckTurret != -1
				IF NOT DOES_ENTITY_EXIST(viHeli)
					IF DOES_ENTITY_EXIST(g_viGunRunTruckTailerIamIn)
					
						viHeli = g_viGunRunTruckTailerIamIn
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(viHeli)
				IF NOT IS_ENTITY_DEAD(viHeli)
					IF NOT IS_VEHICLE_SEAT_FREE(viHeli, VS_DRIVER)
						pilotPed = GET_PED_IN_VEHICLE_SEAT(viHeli, VS_DRIVER)
					ENDIF
				ENDIF
			ENDIF
							
			IF IS_CONTROL_JUST_PRESSED(ctInput, caInput)
				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "SHOULD_CAM_TURN_ON: START_NET_TIMER stBombInputTimer")
				START_NET_TIMER(stBombInputTimer,TRUE)
			ENDIF
			
			IF IS_CONTROL_JUST_RELEASED(ctInput, caInput)
				IF HAS_NET_TIMER_STARTED(stBombInputTimer)
					IF HAS_NET_TIMER_EXPIRED(stBombInputTimer, 500, TRUE)
						IF bVehicleSeatShuffleInputAccept = FALSE
							RESET_NET_TIMER(stBombInputTimer)
							CDEBUG1LN(DEBUG_NET_GUN_TURRET, "SHOULD_CAM_TURN_ON: IS_CONTROL_JUST_RELEASED: HAS_NET_TIMER_EXPIRED: TRUE")
							CDEBUG1LN(DEBUG_NET_GUN_TURRET, "bVehicleSeatShuffleInputAccept = TRUE")
							bVehicleSeatShuffleInputAccept = TRUE
						ENDIF
					ELSE
						IF bVehicleSeatCamInputAccept = FALSE
							IF IS_SEAT_SUITABLE_FOR_TURRET_CAM()
								RESET_NET_TIMER(stBombInputTimer)
								CDEBUG1LN(DEBUG_NET_GUN_TURRET, "SHOULD_CAM_TURN_ON: IS_CONTROL_JUST_RELEASED: HAS_NET_TIMER_EXPIRED: FALSE")
								CDEBUG1LN(DEBUG_NET_GUN_TURRET, "bVehicleSeatCamInputAccept = TRUE")
								bVehicleSeatCamInputAccept = TRUE
							ELSE
								RESET_NET_TIMER(stBombInputTimer)
								CDEBUG1LN(DEBUG_NET_GUN_TURRET, "SHOULD_CAM_TURN_ON: IS_CONTROL_JUST_RELEASED: HAS_NET_TIMER_EXPIRED: FALSE, seat unsuitable for cam, so resetting timer")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF bVehicleSeatShuffleInputAccept = FALSE
					IF HAS_NET_TIMER_STARTED(stBombInputTimer)
						IF HAS_NET_TIMER_EXPIRED(stBombInputTimer, 500, TRUE)
							RESET_NET_TIMER(stBombInputTimer)
							CDEBUG1LN(DEBUG_NET_GUN_TURRET, "SHOULD_CAM_TURN_ON: IS_CONTROL_JUST_RELEASED FALSE: HAS_NET_TIMER_EXPIRED: TRUE")
							CDEBUG1LN(DEBUG_NET_GUN_TURRET, "bVehicleSeatShuffleInputAccept = TRUE")
							bVehicleSeatShuffleInputAccept = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT SHOULD_BLOCK_TURRET()			
				IF (IS_CONTROL_JUST_RELEASED(ctInput, caInput)
					AND NOT(pilotPed = PLAYER_PED_ID() AND eHeliModel = HUNTER)
					AND NOT IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(viHeli))
				OR (IS_DISABLED_CONTROL_JUST_PRESSED(ctInput, caInput) AND g_iShouldLaunchTruckTurret != -1)
				OR (IS_DISABLED_CONTROL_JUST_PRESSED(ctInput, caInput) AND g_iInteriorTurretSeat  != -1)
				OR bVehicleSeatCamInputAccept	
					bVehicleSeatCamInputAccept = FALSE		
					CDEBUG1LN(DEBUG_NET_GUN_TURRET, " - IS_CONTROL_JUST_PRESSED TRUE.")
					IF CAN_TURRET_PED_LAUNCH_TURRET_HUD()
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciDISABLE_VALKYRIE_FRONT_GUN)
						AND NOT g_OfficeHeliDockData.bDoingCutscene
							IF (IS_VEHICLE_DRIVEABLE(viHeli) AND NOT IS_ENTITY_IN_DEEP_WATER(viHeli))
								bAcceptButtonPressed = TRUE
								CDEBUG1LN(DEBUG_NET_GUN_TURRET, " - INPUT_CONTEXT pressed.")
								CDEBUG1LN(DEBUG_NET_GUN_TURRET, "g_bPlayerViewingTurretHud = TRUE")
								g_bPlayerViewingTurretHud = TRUE
								GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bPlayerUsingTurretCam = TRUE
							ELSE
								IF NOT IS_VEHICLE_DRIVEABLE(viHeli) 
								AND GET_ENTITY_MODEL(viHeli) = AVENGER
									bAcceptButtonPressed = TRUE
									CDEBUG1LN(DEBUG_NET_GUN_TURRET, " - INPUT_CONTEXT pressed.")
									CDEBUG1LN(DEBUG_NET_GUN_TURRET, "g_bPlayerViewingTurretHud = TRUE")
									g_bPlayerViewingTurretHud = TRUE
									GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bPlayerUsingTurretCam = TRUE
								ENDIF
								IF NOT IS_VEHICLE_DRIVEABLE(viHeli)
								AND GET_ENTITY_MODEL(viHeli) != AVENGER
									CDEBUG1LN(DEBUG_NET_GUN_TURRET, "SHOULD_CAM_TURN_ON: viHeli = ", GET_ENTITY_MODEL(viHeli))
									CDEBUG1LN(DEBUG_NET_GUN_TURRET, "SHOULD_CAM_TURN_ON: IS_VEHICLE_DRIVEABLE = FALSE")
								ENDIF
							ENDIF
						ELSE
							CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[JJT] SHOULD_CAM_TURN_ON - Returning FALSE due to ciDISABLE_VALKYRIE_FRONT_GUN.")
							RETURN FALSE
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_NET_GUN_TURRET, "CAN_TURRET_PED_LAUNCH_TURRET_HUD = FALSE")
					ENDIF
	//			ELIF SHOULD_GO_STRAIGHT_INTO_CAMERA()
	//				//bgoStraightIn = TRUE // 2238104 - No longer going straight in to gun cam.
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
	OR IS_PLAYER_IN_CREATOR_AIRCRAFT(PLAYER_ID())
	OR DOES_INTERIOR_USE_TURRETS()
//		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "SHOULD_CAM_TURN_ON: IS_PLAYER_IN_CREATOR_TRAILER: returning ", bAcceptButtonPressed )
		RETURN bAcceptButtonPressed 
	ENDIF
	
	IF bIAmCoPilot
	OR bIAmTurretPilot1
	OR bIAmTurretPilot2
	OR bPilotAndAllowed
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "SHOULD_CAM_TURN_ON: bIAmCoPilot/TurretPilot1/TurretPilot2/bPilotAndAllowed")
		IF bAcceptButtonPressed
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "SHOULD_CAM_TURN_ON: returning TRUE from bAcceptButtonPressed")
		ELSE
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "SHOULD_CAM_TURN_ON: returning FALSE from bAcceptButtonPressed")
		ENDIF
		
		RETURN (bAcceptButtonPressed) //OR bgoStraightIn)	
	ELSE
		IF IS_CO_PILOT_CAM_ON()
		#IF IS_DEBUG_BUILD
		OR bAllowDriverAccess
		#ENDIF
			IF bAcceptButtonPressed
				IF eHeliModel != HUNTER
					
					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "SHOULD_CAM_TURN_ON: model not a hunter returning whatever")
					RETURN bAcceptButtonPressed
				ELSE
					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "SHOULD_CAM_TURN_ON: model a hunter returning FALSE")
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC



PROC MAINTAIN_TURN_ON_HELI_GUN_HELP()
	
	IF NOT IS_SAFE_TO_DISPLAY_HELP()
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "MAINTAIN_TURN_ON_HELI_GUN_HELP: IS_SAFE_TO_DISPLAY_HELP = FALSE EXITING")
		EXIT
	ENDIF
	
//	CDEBUG1LN(DEBUG_NET_GUN_TURRET, "MAINTAIN_TURN_ON_HELI_GUN_HELP")
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciDISABLE_VALKYRIE_FRONT_GUN)
	OR g_OfficeHeliDockData.bDoingCutscene
	OR g_bBikerInSellHeli
		EXIT
	ENDIF
	
	IF SHOULD_BLOCK_TURRET()
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "MAINTAIN_TURN_ON_HELI_GUN_HELP: SHOULD_BLOCK_TURRET = TRUE")
		EXIT
	ENDIF
	
//	IF NOT SHOULD_CAM_TURN_ON() //added for url:bugstar:4245814 - WVM - Cover Blown - Unable to use MOC turrets during mission.
//		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "MAINTAIN_TURN_ON_HELI_GUN_HELP: SHOULD_CAM_TURN_ON = FALSE EXITING")
//		EXIT
//	ENDIF

	BOOL bDelayHelpForAmbient
//	CDEBUG1LN(DEBUG_NET_GUN_TURRET, "MAINTAIN_TURN_ON_HELI_GUN_HELP: ")
	IF viHeli != MPGlobalsAmbience.viHeliILastShowedHeliGunCamHelp
	OR g_iShouldLaunchTruckTurret != -1
	OR IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(viHeli)
	OR g_iInteriorTurretSeat != -1
//		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "bShownTurnOnHeliGunHelp = ", bShownTurnOnHeliGunHelp)
		IF NOT bShownTurnOnHeliGunHelp
		OR g_iShouldLaunchTruckTurret != -1
			
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				
				IF bIAmCoPilot
				OR bIAmTurretPilot1
				OR bIAmTurretPilot2
					IF eHeliModel = BUZZARD
					OR eHeliModel = SAVAGE
							IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_KILL_LIST)
								IF NOT IS_LOCAL_PLAYER_DOING_KILL_LIST()	
									bDelayHelpForAmbient = TRUE
								ENDIF
							ENDIF
						IF NOT bDelayHelpForAmbient
							PRINT_HELP("HUNTGUN_1")
						ENDIF
					ELIF eHeliModel = VALKYRIE
					
							IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_KILL_LIST)
								IF NOT IS_LOCAL_PLAYER_DOING_KILL_LIST()	
									bDelayHelpForAmbient = TRUE
								ENDIF
							ENDIF
						
						IF NOT bDelayHelpForAmbient
							PRINT_HELP("HUNTGUN_1c")
						ENDIF
					ELIF eHeliModel = HUNTER
							IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_KILL_LIST)
								IF NOT IS_LOCAL_PLAYER_DOING_KILL_LIST()	
									bDelayHelpForAmbient = TRUE
								ENDIF
							ENDIF
						
						IF NOT bDelayHelpForAmbient
							PRINT_HELP("HUNTGUNH_1c")
						ENDIF
					ELIF g_iShouldLaunchTruckTurret != -1
//						CDEBUG1LN(DEBUG_NET_GUN_TURRET, "MAINTAIN_TURN_ON_HELI_GUN_HELP: setting ")
//						PRINT_HELP("HUNTGUN_T_1")
//						bDelayHelpForAmbient = TRUE
						
					ELIF IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(viHeli)
						IF NOT IS_PLAYER_IN_HANGAR(PLAYER_ID())
							IF eHeliModel = AKULA 
								IF bIAmCoPilot
									PRINT_HELP("AKULAGUN_1") // You are the co-pilot of an Akula. ~n~Press ~INPUT_CONTEXT~ to enter the gun camera. 
								ELIF bIAmTurretPilot1 // nose turret
									PRINT_HELP("AKULAGUN_2") // You are a passenger of an Akula. ~n~Press ~INPUT_CONTEXT~ to enter the passenger camera. . 
								ELIF bIAmTurretPilot2 // 
									PRINT_HELP("AKULAGUN_2") // You are a passenger of an Akula. ~n~Press ~INPUT_CONTEXT~ to enter the passenger camera. . 
								ENDIF
//							ELIF 
							ELIF eHeliModel = VOLATOL
								
								IF bIAmTurretPilot1 // nose turret
									IF NOT bTurretPilot2Exists // top turret
										PRINT_HELP("VOLGUN_1c2") // You are sitting in the Nose Turret seat of a Volatol. ~n~Press ~INPUT_CONTEXT~ to enter the gun camera. ~n~Hold ~INPUT_CONTEXT~ to move to the Top Turret seat.
									ELSE
										PRINT_HELP("VOLGUN_1c")
									ENDIF
								ELIF bIAmTurretPilot2 // top turret
									IF NOT bTurretPilot1Exists 
										PRINT_HELP("VOLGUN_2c1") // You are sitting in the Top Turret seat of a Volatol. ~n~Press ~INPUT_CONTEXT~ to enter the gun camera. ~n~Hold ~INPUT_CONTEXT~ to move to the Nose Turret seat.
									ELSE
										PRINT_HELP("VOLGUN_2c")
									ENDIF
								ENDIF
							ELSE
								
								IF bIAmCoPilot
								 	IF NOT bTurretPilot1Exists // top turret
										PRINT_HELP("BOMBGUN_1c1") // You are the co-pilot of a Bombushka. ~n~Press ~INPUT_CONTEXT~ to enter the gun camera. ~n~Hold ~INPUT_CONTEXT~ to move to the Nose Turret.
									ELIF NOT bTurretPilot2Exists 
										PRINT_HELP("BOMBGUN_1c2") // You are the co-pilot of a Bombushka. ~n~Press ~INPUT_CONTEXT~ to enter the gun camera. ~n~Hold ~INPUT_CONTEXT~ to move to the Rear Turret.
									ELSE
										PRINT_HELP("BOMBGUN_1c")
									ENDIF

								ELIF bIAmTurretPilot1 // nose turret
									IF NOT bTurretPilot2Exists 
										PRINT_HELP("BOMBGUN_2c2") //You are sitting in the Nose Turret seat of a Bombushka. ~n~Press ~INPUT_CONTEXT~ to enter the gun camera. ~n~Hold ~INPUT_CONTEXT~ to move to the Rear Turret.
									ELIF NOT bCoPilotExists 
										PRINT_HELP("BOMBGUN_2c1") //You are sitting in the Nose Turret seat of a Bombushka. Press ~INPUT_CONTEXT~ to enter the gun camera. ~n~Hold ~INPUT_CONTEXT~ to move to the Top Turret.
									ELSE
										PRINT_HELP("BOMBGUN_2c")
									ENDIF
									
								ELIF bIAmTurretPilot2 // rear turret
									IF NOT bCoPilotExists 
										PRINT_HELP("BOMBGUN_3c1") //You are sitting in the Rear Turret seat of a Bombushka. ~n~Press ~INPUT_CONTEXT~ to enter the gun camera. ~n~Hold ~INPUT_CONTEXT~ to move to the Top Turret.
									ELIF NOT bTurretPilot1Exists 
										PRINT_HELP("BOMBGUN_3c2") //You are sitting in the Rear Turret seat of a Bombushka. ~n~Press ~INPUT_CONTEXT~ to enter the gun camera. ~n~Hold ~INPUT_CONTEXT~ to move to the Nose Turret.
									ELSE
										PRINT_HELP("BOMBGUN_3c") // You are sitting in the Rear Turret seat of a Bombushka. ~n~Press ~INPUT_CONTEXT~ to enter the gun camera. 
									ENDIF
								ENDIF
							ENDIF
						ELSE
							//This is unreachable code and the BOMBGUN_HAN text is no longer in the game.
							//PRINT_HELP("BOMBGUN_HAN") //You cannot use the Bombushka gun turret while in a Hangar.
						ENDIF
					ELSE
						PRINT_HELP("HUNTGUN_1b")
					ENDIF
					bShownTurnOnHeliGunHelp =TRUE
				ELIF bPilotAndAllowed
					IF NOT g_heligunMissionHelpSeen
						IF GB_GET_TUNER_ROBBERY_PREP_PLAYER_IS_ON(PLAYER_ID()) = TRV_VAULT_KEY_CODES
							PRINT_HELP("TR_HT_VKCHCAM")
						ELSE
							PRINT_HELP("TR_HT_HCAM")
						ENDIF
						bShownTurnOnHeliGunHelp = TRUE
						g_heligunMissionHelpSeen = TRUE
					ENDIF
				ELSE
					IF IS_CO_PILOT_CAM_ON()
					AND NOT g_OfficeHeliDockData.bShowLandingPrompt
					AND NOT g_bBikerInSellHeli
					AND eHeliModel != HUNTER
					AND NOT IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(viHeli)
						IF eHeliModel = BUZZARD
						OR eHeliModel = SAVAGE
							PRINT_HELP("HUNTGUN_3")
						ELIF eHeliModel = VALKYRIE
							PRINT_HELP("HUNTGUN_3c")
						ELSE
							PRINT_HELP("HUNTGUN_3b")
						ENDIF
						bShownTurnOnHeliGunHelp = TRUE
					ENDIF
				ENDIF
			ELSE
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("VEX_EYEHLPe")
					bShownTurnOnHeliGunHelp = TRUE
				ENDIF
			ENDIF
		ELSE
			IF IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID())
			OR IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					IF NOT bShownOnMissionHelp
						IF !bIAmCoPilot
							IF DISPLAY_ON_JOB_HELI_GUN_RESTRICTIONS_HELP()
								bShownOnMissionHelp = TRUE
								MPGlobalsAmbience.viHeliILastShowedHeliGunCamHelp = viHeli
							ENDIF
						ELSE
							bShownOnMissionHelp = TRUE
							MPGlobalsAmbience.viHeliILastShowedHeliGunCamHelp = viHeli
						ENDIF
					ENDIF
				ENDIF
			ELSE
				bShownOnMissionHelp = TRUE
				MPGlobalsAmbience.viHeliILastShowedHeliGunCamHelp = viHeli
			ENDIF
		ENDIF
	ELSE
//		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "in here = ")
		bShownTurnOnHeliGunHelp = TRUE
		bShownOnMissionHelp = TRUE
	ENDIF
ENDPROC

FUNC BOOL CAN_INTERIOR_TURRET_HELP_BE_DISPLAYED()
	IF IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
	OR IS_PLAYER_IN_CREATOR_AIRCRAFT(PLAYER_ID())
	OR IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
		RETURN TRUE
	ENDIF
	CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CAN_INTERIOR_TURRET_HELP_BE_DISPLAYED: HAS_NET_TIMER_EXPIRED")
	RETURN NOT HAS_NET_TIMER_EXPIRED(tHelpTimer, AT_TURRET_HELP_TIME_TO_CHECK)
ENDFUNC


PROC MAINTAIN_HELI_GUN_OPERATIONAL_INSTRUCTIONS_HELP()
	SWITCH iUseHeliHelpSwitch
		CASE 0
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			AND NOT (HAS_THIS_ADDITIONAL_TEXT_LOADED("MC_PLAY", MISSION_TEXT_SLOT) AND IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SUBJOB_HELP_7")) //url:bugstar:4226207
			
				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "Help text is not already being displayed")
				IF bIAmCoPilot
				OR bPilotAndAllowed
					IF eHeliModel = BUZZARD
					OR eHeliModel = SAVAGE
						PRINT_HELP("HUNTGUN_2")
					ELIF eHeliModel = VALKYRIE
						PRINT_HELP("HUNTGUN_2c")
					ELIF eHeliModel = HUNTER
						PRINT_HELP("HUNTGUN_2c")
					ELIF IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(viHeli)
						PRINT_HELP(GET_TURRET_INSTRUCTION_HELP_STRING(viHeli))
					ELIF g_iShouldLaunchTruckTurret != -1
					OR g_iInteriorTurretSeat != -1
						PRINT_HELP(GET_TURRET_INSTRUCTION_HELP_STRING(viHeli))
					ELSE
						PRINT_HELP("HUNTGUN_2b")
					ENDIF
				ELIF bIAmTurretPilot1
					IF GET_ENTITY_MODEL(viHeli) = BOMBUSHKA
						PRINT_HELP("BOMBGUN_T_2b")
					ENDIF
					IF GET_ENTITY_MODEL(viHeli) = AKULA
						PRINT_HELP("AKULAGUN_P1")
					ENDIF
					IF GET_ENTITY_MODEL(viHeli) = VOLATOL
						PRINT_HELP("VOLGUN_T_2b")
					ENDIF
				ELIF bIAmTurretPilot2
					IF GET_ENTITY_MODEL(viHeli) = BOMBUSHKA
						PRINT_HELP("BOMBGUN_T_2d")
					ENDIF
					IF GET_ENTITY_MODEL(viHeli) = AKULA
						PRINT_HELP("AKULAGUN_P1")
					ENDIF
					IF GET_ENTITY_MODEL(viHeli) = VOLATOL
						PRINT_HELP("VOLGUN_T_2c")
					ENDIF
				ELSE
					IF NOT IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(viHeli)
						IF eHeliModel = BUZZARD
						OR eHeliModel = SAVAGE
							PRINT_HELP("HUNTGUN_4")
						ELIF eHeliModel = VALKYRIE
						OR eHeliModel = HUNTER
							PRINT_HELP("HUNTGUN_4c")
						ELIF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
							PRINT_HELP("IAAGUN_1")
						ELSE
							PRINT_HELP("HUNTGUN_4b")
						ENDIF
					ENDIF
				ENDIF
				iUseHeliHelpSwitch++
			ELSE
//				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "Help text is already being displayed")
			ENDIF
		BREAK
		CASE 1
			// Tagging messages
			IF ( IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HUNTGUN_2")
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HUNTGUN_4")
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HUNTGUN_2b")
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HUNTGUN_4b") )
			AND g_iInteriorTurretSeat = -1 
				iUseHeliHelpSwitch++
			ENDIF
			
			IF g_iShouldLaunchTruckTurret != -1
			OR g_iInteriorTurretSeat != -1
				IF IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(g_ownerOfArmoryTruckPropertyIAmIn)
				AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciALLOW_MOC_ENTRY_THIS_MISSION)
					PRINT_HELP(GET_TURRET_INSTRUCTION_HELP_STRING(viHeli))
				ELSE
					IF CAN_INTERIOR_TURRET_HELP_BE_DISPLAYED()
						PRINT_HELP(GET_TURRET_INSTRUCTION_HELP_STRING(viHeli))
						IF NOT HAS_NET_TIMER_STARTED(tHelpTimer)
							START_NET_TIMER(tHelpTimer)
						ENDIF
					ELSE
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_TURRET_INSTRUCTION_HELP_STRING(viHeli))
							CLEAR_HELP()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				IF bIAmCoPilot
				OR bPilotAndAllowed
						//-- Skip if player is doing urban warfare (B* 2411626)
						IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_KILL_LIST)
							IF NOT IS_LOCAL_PLAYER_DOING_KILL_LIST()			
								IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
									PRINT_HELP("HUNTGUN_6_KM")
								ELSE
									PRINT_HELP("HUNTGUN_6")
								ENDIF
								
							ENDIF
						ENDIF
					
				ENDIF
				iUseHeliHelpSwitch++
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC


PROC CLEAR_INSTRUCTIONAL_HELP()
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HUNTGUN_2")
		CLEAR_HELP()
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HUNTGUN_2b")
		CLEAR_HELP()
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HUNTGUN_2c")
		CLEAR_HELP()
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HUNTGUN_4")
		CLEAR_HELP()
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HUNTGUN_4b")
		CLEAR_HELP()
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HUNTGUN_4c")
		CLEAR_HELP()
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HUNTGUN_5")
		CLEAR_HELP()
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HUNTGUN_6")
		CLEAR_HELP()
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AKULAGUN_P1")
		CLEAR_HELP()
	ENDIF
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AKULAGUN_P2")
		CLEAR_HELP()
	ENDIF
	
	IF IS_PC_VERSION()
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HUNTGUN_5_KM")
			CLEAR_HELP()
		ENDIF

		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HUNTGUN_6_KM")
			CLEAR_HELP()
		ENDIF
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BOMBGUN_T_2b")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BOMBGUN_T_2c")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BOMBGUN_T_2d")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("VOLGUN_T_2b")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("VOLGUN_T_2c")
		CLEAR_HELP()
	ENDIF
ENDPROC

PROC ADD_HELI_HELMET_FOR_GUNNER()
	IF bIAmCoPilot
		IF NOT IS_PED_WEARING_A_MASK(PLAYER_PED_ID())
			GIVE_PED_HELMET(PLAYER_PED_ID(), TRUE, PV_FLAG_PILOT_HELMET)		
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_HELI_HELMET_FOR_GUNNER()
	IF bIAmCoPilot
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "REMOVE_HELI_HELMET_FOR_GUNNER()")
		REMOVE_PED_HELMET(PLAYER_PED_ID(), FALSE)
	ENDIF
ENDPROC


PROC HIDE_HELI_TURRETS_LOCALLY(BOOL bHide)
	IF  DOES_ENTITY_EXIST(viHeli)
	AND NOT IS_ENTITY_DEAD(viHeli)
		MODEL_NAMES model = GET_ENTITY_MODEL(viHeli)
		SWITCH model
			CASE AVENGER
				SET_TURRET_HIDDEN(viHeli, 0, bHide)
				SET_TURRET_HIDDEN(viHeli, 1, bHide)
				SET_TURRET_HIDDEN(viHeli, 2, bHide)
				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "HIDE_HELI_TURRETS_LOCALLY : ", bHide)
			BREAK
		ENDSWITCH	
	ELSE
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "HIDE_HELI_TURRETS_LOCALLY : Can't set this value to ", bHide," because entity doesn't exist")
	ENDIF
ENDPROC

PROC CAM_CLEANUP()
	CDEBUG1LN(DEBUG_NET_GUN_TURRET, "CAM_CLEANUP")
	VEHICLE_INDEX veh 
	HIDE_HELI_TURRETS_LOCALLY(FALSE)
	CLEAR_INSTRUCTIONAL_HELP()
	CLEAR_TURN_ON_HELI_HELP()
	
//	CLEAR_ADDITIONAL_TEXT(ODDJOB_TEXT_SLOT, TRUE)
	g_bTerminateHeliGunScript = FALSE
		
	CLEAR_INSTRUCTIONAL_HELP()
	
	SET_CHOPPER_HUD_ACTIVE(sKevCamVars, veh,FALSE, viHeli, TRUE)
	g_heligunCamActive = FALSE
	//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bInHeliGunCam = FALSE
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bInHeliGunCam	)
		
	IF GET_ENTITY_MODEL(viHeli) = VOLATOL
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "RESET_PARTICLE_FX_OVERRIDE(muz_xm_volatol_twinmg)")
		RESET_PARTICLE_FX_OVERRIDE("muz_xm_volatol_twinmg")
	ENDIF

	IF g_iShouldLaunchTruckTurret != -1
	OR IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(viHeli)
	OR g_iInteriorTurretSeat != -1
		IF NOT IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(viHeli)
		AND NETWORK_HAS_CONTROL_OF_ENTITY(viHeli)
			SET_SHOULD_RESET_TURRET_IN_SCRIPTED_CAMERAS(viHeli, TRUE)
		ENDIF

	ENDIF
	
	IF g_b_EnableFocusDisconnectHeliCam
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			IF NOT IS_ENTITY_FOCUS(PLAYER_PED_ID())
				CLEAR_FOCUS()
				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[HELICAMSTREAM] CAM_CLEANUP: CLEAR_FOCUS() put it back to the player ")
			ENDIF
		ENDIF
	ENDIF
	
	STOP_AUDIO_SCENE("CAR_2_HELI_FILTERING")
	
	IF iAudioLevelFlag != 99
	AND iAudioLevelFlag > 0
		STOP_AUDIO_SCENE("MP_HELI_CAM_FILTERING")
		iAudioLevelFlag = 0
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "CAM_CLEANUP: STOP_AUDIO_SCENE(MP_HELI_CAM_FILTERING) iAudioLevelFlag ", iAudioLevelFlag)
	ENDIF
	
ENDPROC	


PROC INIT_TURRET_AUDIO()
	m_eTurretFireSounds = TFSS_INVALID
	IF IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID()) 
	OR IS_PLAYER_IN_CREATOR_TRAILER(PLAYER_ID())
		m_eTurretFireSounds = TFSS_MOC
	ELIF IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
	OR IS_PLAYER_IN_CREATOR_AIRCRAFT(PLAYER_ID())
		m_eTurretFireSounds = TFSS_AOC
	ELIF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
		m_eTurretFireSounds = TFSS_IAA
	ELIF GET_ENTITY_MODEL(viHeli) = BUZZARD
	OR GET_ENTITY_MODEL(viHeli) = SAVAGE
		m_eTurretFireSounds = TFSS_ROCKETS
	ENDIF
ENDPROC

PROC CLEANUP_TURRET_AUDIO()
	IF m_ePreviousAudioScene != ITAS_INVALID
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[heli_gun] Stopping audio scene ", GET_INTERIOR_TURRET_AUDIO_SCENE_NAME(m_ePreviousAudioScene))
		STOP_AUDIO_SCENE(GET_INTERIOR_TURRET_AUDIO_SCENE_NAME(m_ePreviousAudioScene))
	ENDIF	
	
	IF sKevCamVars.iSceneSoundId != -1
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[heli_gun] Stopping and releasing sound with ID : ", sKevCamVars.iSceneSoundId)
		STOP_SOUND(sKevCamVars.iSceneSoundId)
		RELEASE_SOUND_ID(sKevCamVars.iSceneSoundId)
		sKevCamVars.iSceneSoundId = -1
	ENDIF
	
	IF iAudioLevelFlag != 99
	AND iAudioLevelFlag > 0
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[heli_gun] CLEANUP_TURRET_AUDIO - Stopping audio scene : MP_HELI_CAM_FILTERING")
		STOP_AUDIO_SCENE("MP_HELI_CAM_FILTERING")
		iAudioLevelFlag = 99
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintains the audio scenes for first-person-turret seat
///    and camrea-view for various turret types. 
PROC MAINTAIN_TURRET_AUDIO()
	// Audio scene //
	INTERIOR_TURRET_AUDIO_SCENE eAudioScene = ITAS_INVALID
	BOOL bInAnyTurretCamView = IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bInHeliGunCam	)//GlobalplayerBD_FM[NATIVE_TO_INT(GET_PLAYER_INDEX())].bInHeliGunCam
	
	IF IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
	OR IS_PLAYER_IN_CREATOR_TRAILER(PLAYER_ID())
		IF bInAnyTurretCamView
			eAudioScene = ITAS_MOC_CAM_VIEW
		ELIF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
			eAudioScene = ITAS_MOC_SEAT_FP
		ENDIF
	ELIF IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
	OR IS_PLAYER_IN_CREATOR_AIRCRAFT(PLAYER_ID())
		IF bInAnyTurretCamView
			eAudioScene = ITAS_AOC_CAM_VIEW
		ELIF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
			eAudioScene = ITAS_AOC_SEAT_FP
		ENDIF
	ELIF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
		eAudioScene = ITAS_IAA_CAM_VIEW
	ENDIF
	
	IF eAudioScene != m_ePreviousAudioScene
		IF m_ePreviousAudioScene != ITAS_INVALID
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[heli_gun] Stopping audio scene ", GET_INTERIOR_TURRET_AUDIO_SCENE_NAME(m_ePreviousAudioScene))
			STOP_AUDIO_SCENE(GET_INTERIOR_TURRET_AUDIO_SCENE_NAME(m_ePreviousAudioScene))
		ENDIF	
		IF eAudioScene != ITAS_INVALID
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[heli_gun] Starting audio scene ", GET_INTERIOR_TURRET_AUDIO_SCENE_NAME(eAudioScene))
			START_AUDIO_SCENE(GET_INTERIOR_TURRET_AUDIO_SCENE_NAME(eAudioScene))
		ENDIF
	ENDIF
	m_ePreviousAudioScene = eAudioScene
	
	// Turret audio //
	IF bInAnyTurretCamView
		IF sKevCamVars.iSceneSoundId = -1
			sKevCamVars.iSceneSoundId = GET_SOUND_ID()
			IF sKevCamVars.iSceneSoundId != -1
				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[heli_gun] Starting sound ", GET_TURRET_FIRE_SOUNDS_ELEMENT(m_eTurretFireSounds, TFSK_HUM), " from ", GET_TURRET_FIRE_SOUNDS_NAME(m_eTurretFireSounds), " with id ", sKevCamVars.iSceneSoundId)
				PLAY_SOUND_FRONTEND(sKevCamVars.iSceneSoundId, GET_TURRET_FIRE_SOUNDS_ELEMENT(m_eTurretFireSounds, TFSK_HUM), GET_TURRET_FIRE_SOUNDS_NAME(m_eTurretFireSounds))
			ELSE 
				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[heli_gun] Failed to retrieve a valid sound ID from GET_SOUND_ID(). Cannot play turret sounds")
			ENDIF	
		ENDIF
	ELSE
		IF sKevCamVars.iSceneSoundId != -1
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[heli_gun] Stopping and releasing sound with ID : ", sKevCamVars.iSceneSoundId)
			STOP_SOUND(sKevCamVars.iSceneSoundId)
			RELEASE_SOUND_ID(sKevCamVars.iSceneSoundId)
			sKevCamVars.iSceneSoundId = -1
		ENDIF
	ENDIF
ENDPROC

BOOL bPCCOntrolsSetup

PROC SETUP_PC_CONTROLS()

	IF IS_PC_VERSION()
		IF bPCCOntrolsSetup = FALSE
			INIT_PC_SCRIPTED_CONTROLS("Turret")
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ControlScheme: SETUP_PC_CONTROLS Turret")
			bPCCOntrolsSetup = TRUE
		ENDIF
	ENDIF
ENDPROC


PROC CLEANUP_PC_CONTROLS()

	IF IS_PC_VERSION()
		IF bPCCOntrolsSetup = TRUE
			SHUTDOWN_PC_SCRIPTED_CONTROLS()
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ControlScheme: SHUTDOWN_PC_SCRIPTED_CONTROLS")
			bPCCOntrolsSetup = FALSE
		ENDIF
	ENDIF
ENDPROC

ENUM CAM_MOD
	CM_NORMAL,
	CM_NIGHT,
	CM_THERMAL
ENDENUM

FUNC STRING GET_DEBUG_NAME_FOR_FILTER(CAM_MOD camMod)
	SWITCH camMod
		CASE CM_NORMAL RETURN "CM_NORMAL"
		CASE CM_NIGHT RETURN "CM_NIGHT"
		CASE CM_THERMAL RETURN "CM_THERMAL"
	ENDSWITCH
	RETURN ""
ENDFUNC
CAM_MOD camMod = CM_NORMAL

PROC CLEANUP_THERMAL_CAM()
	IF camMod != CM_NORMAL
		SET_NIGHTVISION(FALSE) 
		IF GET_USINGSEETHROUGH()
			SEETHROUGH_SET_MAX_THICKNESS(g_fSeethrough_cached_max_thickness)
			g_fSeethrough_cached_max_thickness = -1.0
			SET_SEETHROUGH(FALSE)
		ENDIF 
		 camMod = CM_NORMAL
		 CDEBUG1LN(DEBUG_NET_GUN_TURRET, "MAINTAIN_THERMAL_CAM_LOGIC: reseting camera mod now that player camera isn't set to on.")
	ENDIF
ENDPROC

/// PURPOSE:
///    Script cleanup.
PROC SCRIPT_CLEANUP()

	HIDE_HELI_TURRETS_LOCALLY(FALSE)
	
	CLEANUP_PC_CONTROLS()
	CLEANUP_THERMAL_CAM()
	
	IF IS_BIT_SET( iPreviousPedConfigFlag, PPCF_BS_PREVENT_SHUFFLE_HAS_BEEN_MODIFIED )
		SET_PED_CONFIG_FLAG( PLAYER_PED_ID(), PCF_PreventAutoShuffleToDriversSeat, IS_BIT_SET(iPreviousPedConfigFlag, PPCF_BS_PREVENT_SHUFFLE_ENABLED ) )
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "SCRIPT_CLEANUP: Reseting PCF_PreventAutoShuffleToDriversSeat to ", IS_BIT_SET(iPreviousPedConfigFlag, PPCF_BS_PREVENT_SHUFFLE_ENABLED ))
	ENDIF
	
	VEHICLE_INDEX veh 
	//url:bugstar:4199336
	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bPlayerUsingTurretCam 
		UNLOCK_MINIMAP_POSITION()
	ENDIF
	
	g_bPlayerViewingTurretHud = FALSE
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bPlayerUsingTurretCam = FALSE
	CDEBUG1LN(DEBUG_NET_GUN_TURRET, "SCRIPT_CLEANUP: g_bPlayerViewingTurretHud = FALSE")
	CLEAR_INSTRUCTIONAL_HELP()
	CLEAR_TURN_ON_HELI_HELP()
	
//	CLEAR_ADDITIONAL_TEXT(ODDJOB_TEXT_SLOT, TRUE)
	g_bTerminateHeliGunScript = FALSE
	
	IF DOES_ENTITY_EXIST(viHeli)
		IF (pilotPed = PLAYER_PED_ID())
		AND NETWORK_HAS_CONTROL_OF_ENTITY(viHeli)
			DISABLE_VEHICLE_WEAPON(FALSE, WEAPONTYPE_VEHICLE_SPACE_ROCKET, viHeli, PLAYER_PED_ID())
		ENDIF
	ENDIF
	g_bBombuskaExitCamTimerOn = FALSE
	
	CLEAR_INSTRUCTIONAL_HELP()
	
	REMOVE_HELI_HELMET_FOR_GUNNER()
	
	IF DOES_BLIP_EXIST(ArrowStrokeBlipID)
		REMOVE_BLIP(ArrowStrokeBlipID)
	ENDIF
	IF DOES_BLIP_EXIST(ArrowStrokeBlipBg)
		REMOVE_BLIP(ArrowStrokeBlipBg)
	ENDIF
	IF DOES_BLIP_EXIST(GET_MAIN_PLAYER_BLIP_ID())
		SET_BLIP_ALPHA(GET_MAIN_PLAYER_BLIP_ID(), 255)
	ENDIF
	
	SET_CHOPPER_HUD_ACTIVE(sKevCamVars, veh,FALSE, viHeli, TRUE)
	g_heligunCamActive = FALSE	
	g_bActiveInGunTurret = FALSE
	g_bUsingTurretHeliCam = FALSE
	
	//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bInHeliGunCam = FALSE
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bInHeliGunCam	)
	
	IF g_iShouldLaunchTruckTurret != -1
	OR IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(viHeli)
	OR g_iInteriorTurretSeat != -1
		UNLOCK_MINIMAP_POSITION()
		g_iShouldLaunchTruckTurret = -1
		g_iVehicleTurretSeat = -1
		g_iInteriorTurretSeat = -1
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_HELI_GUN: g_iShouldLaunchTruckTurret = -1")
	ENDIF
	
	IF bSetWantedMultiplierForTruckTurret = TRUE
		SET_WANTED_LEVEL_MULTIPLIER(1.0)
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_HELI_GUN: SET_WANTED_LEVEL_MULTIPLIER(1.0)")
	ENDIF
	
	STOP_AUDIO_SCENE("CAR_2_HELI_FILTERING")
	CLEANUP_TURRET_AUDIO()
	
//	#IF FEATURE_GUNRUNNING
//	CDEBUG1LN(DEBUG_NET_GUN_TURRET, "Heli_gun: SCRIPT_CLEANUP: Reseting g_iShouldLaunchTruckTurret")
//	playerBD[PARTICIPANT_ID_TO_INT()].eHeliGunState = eHELIGUNSTATE_OFF
//	g_iShouldLaunchTruckTurret = -1
//	g_SimpleInteriorData.bSuppressConcealCheck = FALSE
//	#ENDIF
	
	RESET_NET_TIMER(tHelpTimer)
		
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()  // call this to terminate your script.
ENDPROC	


/// PURPOSE:
///    Do necessary pre game start ini.
PROC PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission),  missionScriptArgs )
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	// This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	// Save the heli model and Id I am in.
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
					eHeliModel = GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
					viHeli = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_PRE_GAME: eHeliModel = ", GET_MODEL_NAME_FOR_DEBUG(eHeliModel))
					eRocketModel = INT_TO_ENUM(MODEL_NAMES, HASH("w_lr_rpg_rocket"))
				
					IF ENUM_TO_INT(eRocketModel) != 0
						REQUEST_MODEL(eRocketModel)
//						CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_PRE_GAME - Requesting heli model: ", GET_MODEL_NAME_FOR_DEBUG(eRocketModel))
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
//	IF IS_BIT_SET(missionScriptArgs.iTruckBitSet, TRUCK_BS_IS_TRUCK_TURRET)
	IF g_iShouldLaunchTruckTurret != -1
	OR g_iInteriorTurretSeat != -1
		
		IF IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
		OR IS_PLAYER_IN_CREATOR_TRAILER(PLAYER_ID())
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciALLOW_MOC_ENTRY_THIS_MISSION)
				IF viHeli != MPGlobalsAmbience.vehCreatorTrailer 
					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_PRE_GAME: setting viHeli = MPGlobalsAmbience.vehCreatorTrailer ")
					viHeli = MPGlobalsAmbience.vehCreatorTrailer 
				ENDIF
			ELSE
				IF viHeli != g_viGunRunTruckTailerIamIn
					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_PRE_GAME: setting viHeli = g_viGunRunTruckTailer")

					viHeli = g_viGunRunTruckTailerIamIn
				ENDIF
			ENDIF
		ELIF IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())

				
				viHeli = g_viAvengerIamIn

				IF DOES_ENTITY_EXIST(viHeli)
					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_PRE_GAME: setting viHeli = GET_AVENGER_VEHICLE_INDEX_THAT_I_AM_IN")

//					viHeli = GET_ARMORY_AIRCRAFT_VEH_INDEX(viHeli)
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_PRE_GAME: setting viHeli = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(viHeli)))
				ELSE
					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_PRE_GAME: GET_AVENGER_VEHICLE_INDEX_THAT_I_AM_IN != aircraft")
				ENDIF
		ELIF IS_PLAYER_IN_CREATOR_AIRCRAFT(PLAYER_ID())

				viHeli = MPGlobalsAmbience.vehCreatorAircraft
			
				IF DOES_ENTITY_EXIST(viHeli)
					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_PRE_GAME: setting viHeli = creator aircraft")

//					viHeli = GET_ARMORY_AIRCRAFT_VEH_INDEX(viHeli)
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_PRE_GAME: setting viHeli = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(viHeli)))
				ELSE
					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_PRE_GAME: MPGlobalsAmbience.vehCreatorAircraft != aircraft")
				ENDIF
		ELIF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
		OR IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
			
			viHeli = g_viDefunctBastTurretVeh//NET_TO_VEH(GlobalServerBD.niVehTurretAII)
//			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_PRE_GAME: in defunct base setting viHeli = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(viHeli)))
		ELSE
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_PRE_GAME: g_iShouldLaunchTruckTurret, can't tell which vehicle player is in ")
		ENDIF
		
		IF IS_PLAYER_IN_CREATOR_AIRCRAFT(PLAYER_ID())
		OR IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
			sKevCamVars.maxFOV = 40.0
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_PRE_GAME: sKevCamVars.maxFOV = 40.0")
		ELSE
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_PRE_GAME: sKevCamVars.maxFOV != 40.0")
		ENDIF
		
	ELSE
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_PRE_GAME: g_iShouldLaunchTruckTurret= -1 ")
	ENDIF
	#IF IS_DEBUG_BUILD
	sKevCamVars.vTurretCamOffset = <<-1,-1,-1>>
	
	
	#ENDIF
	
	// KGM: Wait for the first network broadcast before moving on - call this as the last instruction in pre-game
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, " - Failed to receive initial network broadcast. Cleaning up.")
		SCRIPT_CLEANUP()
	ENDIF
	
	SET_GAME_STATE(eGAMESTATE_INI)
	
ENDPROC

FUNC INT GET_VEHICLE_PLAYER_SEAT()
	IF bIAmCoPilot
		RETURN 0
	ELIF bIAmTurretPilot1
	OR bPilotAndAllowed
		RETURN 1
	ELIF bIAmTurretPilot2
		RETURN 2
	ENDIF
	RETURN -1
ENDFUNC

PROC MAINTAIN_START_OF_FRAME_DATA()
	
	PED_INDEX pedId = PLAYER_PED_ID()
	modHeliModel = DUMMY_MODEL_FOR_SCRIPT
	bPilotExists = FALSE
	bPilotDead = TRUE
	bPilotAndAllowed = FALSE
	bTurretPilot1Dead = TRUE
	bTurretPilot2Dead = TRUE
	bCoPilotExists = FALSE
	bCoPilotDead = TRUE
	pilotPed = INT_TO_NATIVE(PED_INDEX, -1)
	coPilotPed = INT_TO_NATIVE(PED_INDEX, -1)
	PlayerIdPilot = INVALID_PLAYER_INDEX()
	PlayerIdCoPilot = INVALID_PLAYER_INDEX()
	bIAmCoPilot = FALSE
	g_heliGunCoPlayerId = INVALID_PLAYER_INDEX()
	bHeliDriveable = FALSE
	TurretPilot1Ped = INT_TO_NATIVE(PED_INDEX, -1)
	bTurretPilot1Exists = FALSE
	PlayerIdTurretPilot1  = INVALID_PLAYER_INDEX()
	TurretPilot2Ped = INT_TO_NATIVE(PED_INDEX, -1)
	bTurretPilot2Exists = FALSE
	PlayerIdTurretPilot2  = INVALID_PLAYER_INDEX()
	
	
	IF DOES_ENTITY_EXIST(pedId)
		IF NOT IS_ENTITY_DEAD(pedId)
		
			//This if statement has been removed so that the flags still get updated
			//while the player is leaving the aircraft and is still in control of the vehicle
			//so that they can still make weapon ammo changes.
			//
			//IF IS_PED_SITTING_IN_ANY_VEHICLE(pedId)
				
				IF DOES_ENTITY_EXIST(viHeli)
				AND IS_VEHICLE_DRIVEABLE(viHeli)
					modHeliModel = GET_ENTITY_MODEL(viHeli)
					bHeliDriveable = TRUE

					IF NOT IS_VEHICLE_SEAT_FREE(viHeli, VS_FRONT_RIGHT)
						coPilotPed = GET_PED_IN_VEHICLE_SEAT(viHeli, VS_FRONT_RIGHT)
						bCoPilotExists = DOES_ENTITY_EXIST(coPilotPed)
						IF bCoPilotExists
							bCoPilotDead = IS_ENTITY_DEAD(coPilotPed)
							IF IS_PED_SITTING_IN_VEHICLE_SEAT(coPilotPed, viHeli, VS_FRONT_RIGHT)
							AND IS_PED_IN_VEHICLE(coPilotPed, viHeli)
							
								IF IS_PED_A_PLAYER(coPilotPed)
									PlayerIdCoPilot = NETWORK_GET_PLAYER_INDEX_FROM_PED(coPilotPed)
									//IF NETWORK_IS_PLAYER_A_PARTICIPANT(PlayerIdCoPilot)
										//coPilotParticipantId = NETWORK_GET_PARTICIPANT_INDEX(PlayerIdCoPilot)
									//ENDIF
									IF PlayerIdCoPilot = PLAYER_ID()
										CDEBUG1LN(DEBUG_NET_GUN_TURRET, "MAINTAIN_START_OF_FRAME_DATA: Player is coPilot")
										bIAmCoPilot = TRUE
										IF IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(viHeli)
											CDEBUG1LN(DEBUG_NET_GUN_TURRET, "MAINTAIN_START_OF_FRAME_DATA: Player is coPilot, g_iVehicleTurretSeat = 0")
											g_iVehicleTurretSeat = 0
										ENDIF
									ELSE
										bIAmCoPilot = FALSE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						coPilotPed = INT_TO_NATIVE(PED_INDEX, -1)
						bCoPilotExists = FALSE
						bIAmCoPilot = FALSE
					ENDIF
					
					IF NOT IS_VEHICLE_SEAT_FREE(viHeli, VS_DRIVER)
						pilotPed = GET_PED_IN_VEHICLE_SEAT(viHeli, VS_DRIVER)
						bPilotExists = DOES_ENTITY_EXIST(pilotPed)
						IF bPilotExists
							bPilotDead = IS_ENTITY_DEAD(pilotPed)
							IF IS_PED_A_PLAYER(pilotPed)
								PlayerIdPilot = NETWORK_GET_PLAYER_INDEX_FROM_PED(pilotPed)
								
								IF SHOULD_ALLOW_PILOT_TO_USE_HELI_CAM()
								AND (NOT bCoPilotExists OR bCoPilotDead OR NOT IS_PED_A_PLAYER(coPilotPed))
									bPilotAndAllowed = TRUE
								ENDIF
								
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(viHeli)
						IF NOT IS_VEHICLE_SEAT_FREE(viHeli, VS_BACK_LEFT)
							TurretPilot1Ped = GET_PED_IN_VEHICLE_SEAT(viHeli, VS_BACK_LEFT)
							bTurretPilot1Exists = DOES_ENTITY_EXIST(TurretPilot1Ped)
							IF bTurretPilot1Exists
								bTurretPilot1Dead = IS_ENTITY_DEAD(TurretPilot1Ped)
								IF IS_PED_SITTING_IN_VEHICLE_SEAT(TurretPilot1Ped, viHeli, VS_BACK_LEFT)
								AND IS_PED_IN_VEHICLE(TurretPilot1Ped, viHeli)
								
									IF IS_PED_A_PLAYER(TurretPilot1Ped)
										PlayerIdTurretPilot1 = NETWORK_GET_PLAYER_INDEX_FROM_PED(TurretPilot1Ped)
										IF PlayerIdTurretPilot1 = PLAYER_ID()
											CDEBUG1LN(DEBUG_NET_GUN_TURRET, "MAINTAIN_START_OF_FRAME_DATA: Player is PlayerIdTurretPilot1, g_iVehicleTurretSeat = 1")
											
	
											g_iVehicleTurretSeat = 1
											bIAmTurretPilot1 = TRUE
										ELSE
											bIAmTurretPilot1 = FALSE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							TurretPilot1Ped = INT_TO_NATIVE(PED_INDEX, -1)
							bTurretPilot1Exists = FALSE
							bIAmTurretPilot1 = FALSE
						ENDIF
						
						IF NOT IS_VEHICLE_SEAT_FREE(viHeli, VS_BACK_RIGHT)
							TurretPilot2Ped = GET_PED_IN_VEHICLE_SEAT(viHeli, VS_BACK_RIGHT)
							bTurretPilot2Exists = DOES_ENTITY_EXIST(TurretPilot2Ped)
							IF bTurretPilot2Exists
								bTurretPilot2Dead = IS_ENTITY_DEAD(TurretPilot2Ped)
								IF IS_PED_SITTING_IN_VEHICLE_SEAT(TurretPilot2Ped, viHeli, VS_BACK_RIGHT)
								AND IS_PED_IN_VEHICLE(TurretPilot2Ped, viHeli)
									
									IF IS_PED_A_PLAYER(TurretPilot2Ped)
										CDEBUG1LN(DEBUG_NET_GUN_TURRET, "MAINTAIN_START_OF_FRAME_DATA: Player is TurretPilot2Ped, g_iVehicleTurretSeat = 2")
										PlayerIdTurretPilot2 = NETWORK_GET_PLAYER_INDEX_FROM_PED(TurretPilot2Ped)
										IF PlayerIdTurretPilot2 = PLAYER_ID()
											g_iVehicleTurretSeat = 2
											bIAmTurretPilot2 = TRUE
										ELSE
											bIAmTurretPilot2 = FALSE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							bIAmTurretPilot2 = FALSE
							bTurretPilot2Exists = FALSE
							TurretPilot2Ped = INT_TO_NATIVE(PED_INDEX, -1)
						ENDIF
					ENDIF
					
					IF PlayerIdCoPilot = PLAYER_ID()
						IF NOT bPilotDead
							g_heliGunCoPlayerId = PlayerIdPilot
						ENDIF
					ELSE
						IF NOT bCoPilotDead
							g_heliGunCoPlayerId = PlayerIdCoPilot
						ENDIF
					ENDIF
				ENDIF
			//ENDIF
		ENDIF
	ENDIF
	
	IF g_iShouldLaunchTruckTurret != -1
//		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "MAINTAIN_START_OF_FRAME_DATA: setting truck data")

			bCoPilotDead = FALSE
			
			PlayerIdCoPilot = PLAYER_ID()
//			IF NETWORK_IS_PLAYER_A_PARTICIPANT(PlayerIdCoPilot)
//				coPilotParticipantId = NETWORK_GET_PARTICIPANT_INDEX(PlayerIdCoPilot)
//			ENDIF
			IF PlayerIdCoPilot = PLAYER_ID()
				bIAmCoPilot = TRUE
			ENDIF
	ENDIF
	
ENDPROC

FUNC INT GET_SIDE_TO_SHOOT_ROCKET_FROM()
	
	VECTOR vChopperFVector
	VECTOR vCamFVector
	VECTOR vCamRotation
	FLOAT fChopperATan
	FLOAT fCamATan
	FLOAT fResult
	
	vCamRotation = GET_CAM_ROT(sKevCamVars.camChopper)
	vCamFVector = <<-SIN(vCamRotation.z) * COS(vCamRotation.x), COS(vCamRotation.z) * COS(vCamRotation.x), SIN(vCamRotation.x)>>
	vChopperFVector = GET_ENTITY_FORWARD_VECTOR(sKevCamVars.vehchopper)
	
	CDEBUG1LN(DEBUG_NET_GUN_TURRET, " - GET_SIDE_TO_SHOOT_ROCKET_FROM - vCamRotation = ", vCamRotation)
	CDEBUG1LN(DEBUG_NET_GUN_TURRET, " - GET_SIDE_TO_SHOOT_ROCKET_FROM - vCamFVector = ", vCamFVector)
	CDEBUG1LN(DEBUG_NET_GUN_TURRET, " - GET_SIDE_TO_SHOOT_ROCKET_FROM - vChopperFVector = ", vChopperFVector)
	
	fCamATan = ATAN2(vCamFVector.y, vCamFVector.x)
	fChopperATan = ATAN2(vChopperFVector.y, vChopperFVector.x)
	
	IF fCamATan < -3.14
		fCamATan = -3.14
	ENDIF
	
	IF fCamATan > 3.14
		fCamATan = 3.14
	ENDIF
	
	IF fChopperATan < -3.14
		fChopperATan = -3.14
	ENDIF
	
	IF fChopperATan > 3.14
		fChopperATan = 3.14
	ENDIF
	
//	CDEBUG1LN(DEBUG_NET_GUN_TURRET, " - GET_SIDE_TO_SHOOT_ROCKET_FROM - fChopperATan = ", fChopperATan)
//	CDEBUG1LN(DEBUG_NET_GUN_TURRET, " - GET_SIDE_TO_SHOOT_ROCKET_FROM - fCamATan = ", fCamATan)
//	
//	fCamATan = ABSF(fCamATan)
//	fChopperATan = ABSF(fChopperATan)
	
	CDEBUG1LN(DEBUG_NET_GUN_TURRET, " - GET_SIDE_TO_SHOOT_ROCKET_FROM - ABSF(fChopperATan) = ", fChopperATan)
	CDEBUG1LN(DEBUG_NET_GUN_TURRET, " - GET_SIDE_TO_SHOOT_ROCKET_FROM - ABSF(fCamATan) = ", fCamATan)
	
	fResult = fCamATan - fChopperATan
	
	CDEBUG1LN(DEBUG_NET_GUN_TURRET, " - GET_SIDE_TO_SHOOT_ROCKET_FROM - fResult = ", fResult)
	
	IF fResult <= 0
		RETURN 0
	ENDIF
	
	RETURN 1
	
ENDFUNC


FUNC VECTOR GET_ROCKET_OFFSET()
	
	VEHICLE_INDEX veh
	VECTOR vOffset
		
	BOOL bTemp
	IF IS_MODEL_VALID_HELI_GUN_MODEL(viHeli, bTemp)
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_ROCKET_OFFSET: eHeliModel = ", GET_MODEL_NAME_FOR_DEBUG(eHeliModel))
	ENDIF
	
//	CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_ROCKET_OFFSET: g_iShouldLaunchTruckTurret = ", g_iShouldLaunchTruckTurret)

	SWITCH eHeliModel
		CASE BUZZARD
		CASE SAVAGE
			SWITCH GET_SIDE_TO_SHOOT_ROCKET_FROM()
				CASE 0 vOffset = << 1.59, 0.415, -0.43 >> BREAK // Right pod.
				CASE 1 vOffset = << -1.59, 0.415, -0.43 >> BREAK // Left pod.
			ENDSWITCH
		BREAK
		CASE VALKYRIE
			SWITCH GET_SIDE_TO_SHOOT_ROCKET_FROM()
				CASE 0 vOffset = << 2.89, 1.215, -0.43 >> BREAK // Right pod.
				CASE 1 vOffset = << -2.89, 1.215, -0.43 >> BREAK // Left pod.
			ENDSWITCH
		BREAK
		CASE HUNTER
			SWITCH GET_SIDE_TO_SHOOT_ROCKET_FROM()
				CASE 0 vOffset = << 2.89, 1.215, -0.43 >> BREAK // Right pod.
				CASE 1 vOffset = << -2.89, 1.215, -0.43 >> BREAK // Left pod.
			ENDSWITCH
//			vOffset = <<0.0014, 5.9031, 0.5528>>
		BREAK
		
	ENDSWITCH
	
	
	IF IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(viHeli)
		SWITCH GET_VEHICLE_PLAYER_SEAT()
			CASE 0  vOffset = <<0.0122, 8.7349, 0.7239>> BREAK // nose
			CASE 1  vOffset = <<0.0082, 1.1879, 5.2393>> BREAK // top
			CASE 2  vOffset = <<-0.0083, -22.7956, 4.2180>> BREAK // butt
		ENDSWITCH
	ENDIF
	
	IF g_iInteriorTurretSeat != -1
		IF GET_ENTITY_MODEL(viHeli) = BOMBUSHKA 
			SWITCH g_iInteriorTurretSeat 
				CASE 0  vOffset = <<0.0122, 8.7349, 0.7239>> BREAK // nose
				CASE 1  vOffset = <<0.0082, 1.1879, 5.2393>> BREAK // top
				CASE 2  vOffset = <<-0.0083, -22.7956, 4.2180>> BREAK // butt
			ENDSWITCH
		ENDIF
		IF GET_ENTITY_MODEL(viHeli) = VOLATOL 
			SWITCH g_iInteriorTurretSeat 
				CASE 0  vOffset = <<0.0122, 8.7349, 0.7239>> BREAK // nose
				CASE 1  vOffset = <<0.0082, 1.1879, 5.2393>> BREAK // top
				CASE 2  vOffset = <<-0.0083, -22.7956, 4.2180>> BREAK // butt
			ENDSWITCH
		ENDIF
	ENDIF

	IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
	OR IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
		VECTOR vAdderPos
		vAdderPos = <<2065.8484, 2967.3196, 45.2947>>
		VECTOR vTurretPos1 
		vTurretPos1 = <<2049.6121, 2947.6570, 49.5560>>
		VECTOR vTurretPos2 
		vTurretPos2 = <<2045.0913, 2943.2578, 49.4991>>
		VECTOR vTurretPos3 
		vTurretPos3 = <<2040.3645, 2952.7544, 49.5688>>
		VECTOR vTurretPos4 
		vTurretPos4 = <<2049.3848, 2953.9709, 49.9635>>
		SWITCH g_iInteriorTurretSeat
			CASE 1
				vOffset = vTurretPos1 - vAdderPos
			BREAK 
			CASE 2
				vOffset = vTurretPos2 - vAdderPos
			BREAK
			CASE 3
				vOffset = vTurretPos3 - vAdderPos				
			BREAK
			CASE 4
				vOffset = vTurretPos4 - vAdderPos
			BREAK
		ENDSWITCH
	ENDIF
	
	IF g_iShouldLaunchTruckTurret != -1
		SWITCH g_iShouldLaunchTruckTurret
			CASE 1
				vOffset = <<0.0, 9.0, 0.920>>
			BREAK 
			CASE 2
				vOffset = <<-2.400, -8.0, 1.140>>
			BREAK
			CASE 3
				vOffset = <<2.400, -8.0, 1.140>>
			BREAK
			CASE 4
				vOffset = <<-1.3829, 9.3693, 1.7578>> + <<1.3666, 0.2053, 0>>
			BREAK
		ENDSWITCH
		
		#IF IS_DEBUG_BUILD
		IF bCustomOffset	
			vOffset = vCannonOffset
		ENDIF
		#ENDIF
		
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		AND DOES_ENTITY_EXIST(viHeli)
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			
				#IF IS_DEBUG_BUILD
				VECTOR vTemp = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viHeli, vOffset)
				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_ROCKET_OFFSET: Truck offset = ", vTemp)
				#ENDIF
			
				RETURN GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viHeli, vOffset)
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_ROCKET_OFFSET: Either player or viHeli doesn't exist")
		ENDIF
	ENDIF

	
	CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_ROCKET_OFFSET: vOffset = ", vOffset)
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())		
				veh = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_ROCKET_OFFSET: GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS from veh")
				RETURN GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh, vOffset)
			ELIF DOES_INTERIOR_USE_SCRIPTED_WEAPONS()
				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_ROCKET_OFFSET: GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS from viHeli")
				RETURN GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viHeli, vOffset)
			ENDIF
		ENDIF
	ENDIF
	
	RETURN << 0.0, 0.0, 0.0 >>
	
ENDFUNC


FUNC WEAPON_TYPE GET_SCRIPT_TURRET_WEAPON_TYPE(VEHICLE_INDEX veh)
	MODEL_NAMES eVehModel
	eVehModel = GET_ENTITY_MODEL(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(veh))
	CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_SCRIPT_TURRET_WEAPON_TYPE: veh = ", GET_MODEL_NAME_FOR_DEBUG(eVehModel))
	IF IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
	OR IS_PLAYER_IN_CREATOR_AIRCRAFT(PLAYER_ID())
		RETURN WEAPONTYPE_DLC_VEHICLE_AVENGER_CANNON
	ENDIF
	SWITCH eVehModel
		CASE SAVAGE
		CASE BUZZARD
			RETURN WEAPONTYPE_PASSENGER_ROCKET
		BREAK 
		CASE AVENGER
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_SCRIPT_TURRET_WEAPON_TYPE: WEAPONTYPE_DLC_VEHICLE_MOBILEOPS_CANNON")
			RETURN WEAPONTYPE_DLC_VEHICLE_MOBILEOPS_CANNON
		BREAK
	ENDSWITCH
	
	IF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
		RETURN WEAPONTYPE_DLC_VEHICLE_MOBILEOPS_CANNON
	ENDIF
	
	RETURN  WEAPONTYPE_DLC_VEHICLE_MOBILEOPS_CANNON
ENDFUNC


FUNC FLOAT LERP_LIMIT(FLOAT fXMin, FLOAT fXMax, FLOAT fYMin, FLOAT fYMax, FLOAT fYCurrent)
	fYCurrent = (fYCurrent - fYMin) / (fYMax - fYMin)
	RETURN LERP_FLOAT(fXmin, fXMax, fYCurrent)
ENDFUNC

FUNC BOOL CAN_TURRET_CAM_FIRE_AT_THIS_ANGLE()
	//I wish this wasn't the way :) very painful to work out and has to be adjusted if turrets move...
	FLOAT fABSz = ABSF(sKevCamVars.vAttachedCamRotationOffset.z)
	BOOL bCanShoot = TRUE
	SWITCH GET_ENTITY_MODEL(viHeli)
		CASE AVENGER
			SWITCH g_iInteriorTurretSeat
				CASE 1 //Nose turret
					IF GET_VEHICLE_FLIGHT_NOZZLE_POSITION(viHeli) >= 0.4	
						//Rotor engines
						IF (fABSz > 122.6 AND fABSz < 131.3 AND sKevCamVars.vAttachedCamRotationOffset.x > 7.87) 
							bCanShoot = FALSE
						//Hull
						ELIF (fABSz > 140.5 AND fABSz < 159.5 AND sKevCamVars.vAttachedCamRotationOffset.x  > LERP_LIMIT(6.0, 15.0, -159.5, -140.5, -fABSz))
							bCanShoot = FALSE
						ENDIF
					ENDIF
					IF bCanShoot
					AND GET_VEHICLE_HAS_LANDING_GEAR(viHeli)
						LANDING_GEAR_STATE eLandingGearState
						eLandingGearState = GET_LANDING_GEAR_STATE(viHeli)
						IF eLandingGearState = LGS_LOCKED_DOWN
						OR eLandingGearState = LGS_DEPLOYING
						OR eLandingGearState = LGS_RETRACTING
							IF sKevCamVars.vAttachedCamRotationOffset.x > -13.0
								IF sKevCamVars.vAttachedCamRotationOffset.x > -3.7
									//Wheel axel, wheels and wheel bay doors
									bCanShoot = NOT (fABSz > 157.0)
								ELSE
									//Wheels
									bCanShoot = NOT (fABSz > 164 AND fABSz < 177.0)
								ENDIF
							ENDIF
						ELSE
							bCanShoot = NOT (fABSz > 140.5 AND sKevCamVars.vAttachedCamRotationOffset.x  > LERP_LIMIT(0.5, 15.0, -180, -140.5, -fABSz))					
						ENDIF
					ENDIF
				BREAK
				CASE 2 //Top turret
					IF GET_VEHICLE_FLIGHT_NOZZLE_POSITION(viHeli) >= 0.4
						//Front wings
						bCanShoot = NOT (fABSz > 111.2 AND fABSz < 125.8 AND sKevCamVars.vAttachedCamRotationOffset.x < 20.0)
					ELSE
						//@@ Add this when feeling insane
					ENDIF
					IF bCanShoot
						//Rear fins
						bCanShoot = NOT (fABSz > 20.0 AND fABSz < 27.6 AND sKevCamVars.vAttachedCamRotationOffset.x < 10.9)
					ENDIF
					IF bCanShoot
						//Rear wing
						bCanShoot = NOT (fABSz < 28 AND sKevCamVars.vAttachedCamRotationOffset.x < -8.5 AND sKevCamVars.vAttachedCamRotationOffset.x > -12.0)
					ENDIF
					IF bCanShoot
						//Front wing
						bCanShoot = NOT (fABSz < 28 AND sKevCamVars.vAttachedCamRotationOffset.x < -8.5 AND sKevCamVars.vAttachedCamRotationOffset.x > -12.0)
					ENDIF
					//Rear hull
					IF bCanShoot
						IF fABSz < LERP_LIMIT(10.6, 15.6, 13.23, 20, -sKevCamVars.vAttachedCamRotationOffset.x) AND (sKevCamVars.vAttachedCamRotationOffset.x < -13.23)
							bCanShoot = FALSE
						//Front wing / hull
						ELIF (fABSz > 108.0) 
						AND (sKevCamVars.vAttachedCamRotationOffset.x >= -LERP_LIMIT(3.8, 38, 108, 180, fABSz))
						AND (sKevCamVars.vAttachedCamRotationOffset.x < -LERP_LIMIT(1.3, 7, 125, 180, fABSz))
							bCanShoot = FALSE
						ENDIF
					ENDIF
				BREAK
				CASE 3 //Rear turret
					//Front wings
					IF GET_VEHICLE_FLIGHT_NOZZLE_POSITION(viHeli) >= 0.4
						bCanShoot = NOT (fABSz > 141.3 AND fABSz < 147.7 AND sKevCamVars.vAttachedCamRotationOffset.x > -3.7)
					ELSE
						//@@ Add this when feeling insane
					ENDIF
					IF bCanShoot
						//Hull
						bCanShoot = NOT (sKevCamVars.vAttachedCamRotationOffset.x > -18.0
										AND fABSz > LERP_LIMIT(148, 170, -4.5, 17.5, -sKevCamVars.vAttachedCamRotationOffset.x))
					ENDIF
					IF bCanShoot
						//Fins on rear wing
						bCanShoot = NOT (fABSz > 59.4 AND fABSz < 106.0 AND sKevCamVars.vAttachedCamRotationOffset.x > LERP_LIMIT(5.5, 11.8, -106.0, -59.4, -fABSz))
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	IF NOT bCanShoot
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "CAN_TURRET_CAM_FIRE_AT_THIS_ANGLE: FALSE Yaw == ", sKevCamVars.vAttachedCamRotationOffset.z, " Pitch == ", sKevCamVars.vAttachedCamRotationOffset.x)
	ENDIF
	#ENDIF
	RETURN bCanShoot
ENDFUNC

/// PURPOSE:
///    Processes firing the machine gun.
PROC PROCESS_ROCKETS()
	
//	IF HELI_MODEL_CAN_SHOOT_ROCKETS_FROM_HELI_CAM()
	
	IF VEH_MODEL_CAN_SCRIPT_SHOOT_FROM_VEH_CAM(viHeli)
	OR DOES_INTERIOR_USE_SCRIPTED_WEAPONS()
	AND NOT IS_PAUSE_MENU_ACTIVE()
		
		IF NOT IS_MP_PASSIVE_MODE_ENABLED()
			
			MODEL_NAMES eModelToLoad
		 	IF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
				eModelToLoad = INT_TO_ENUM(MODEL_NAMES, HASH("w_ex_vehiclemissile_3"))
			ELSE
				
				IF IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
				OR IS_PLAYER_IN_CREATOR_TRAILER(PLAYER_ID())
				OR IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
				OR IS_PLAYER_IN_CREATOR_AIRCRAFT(PLAYER_ID())
					eModelToLoad  = INT_TO_ENUM(MODEL_NAMES, HASH("w_lr_rpg_rocket"))
				ELSE
					eModelToLoad = GET_WEAPONTYPE_MODEL(GET_SCRIPT_TURRET_WEAPON_TYPE(viHeli))	
				ENDIF
			ENDIF
//			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_ROCKETS: requesting model: ", GET_WEAPONTYPE_MODEL(GET_SCRIPT_TURRET_WEAPON_TYPE(viHeli)))
			REQUEST_MODEL(eModelToLoad)
			
			
	
			IF HAS_MODEL_LOADED(eModelToLoad)
				#IF IS_DEBUG_BUILD
				IF HAS_MODEL_LOADED(eModelToLoad)
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_ROCKETS: HAS_MODEL_LOADED: TRUE for Model = ", eModelToLoad)			
				ENDIF
				#ENDIF
				IF ((IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RT) OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RT)) AND NOT bWeaponsDisabledForPassive AND NOT HAS_NET_TIMER_STARTED(MPGlobals.stRocketCoolDownTimer)	)
					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_ROCKETS: IS_CONTROL_PRESSED: INPUT_ATTACK #1, g_iShouldLaunchTruckTurret: ",g_iShouldLaunchTruckTurret)
					IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
							IF DOES_CAM_EXIST(sKevCamVars.camChopper)
								IF IS_CAM_ACTIVE(sKevCamVars.camChopper)
									INT iDamage = 250
									
									IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MISSION)
										INT iVeh = -1
										
										IF DECOR_IS_REGISTERED_AS_TYPE("MC_EntityID", DECOR_TYPE_INT)
											IF DECOR_EXIST_ON(viHeli,"MC_EntityID")
												iveh = DECOR_GET_INT(viHeli,"MC_EntityID")
											ENDIF
										ENDIF
										
										IF iVeh != -1
										AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehWeaponDamage != -1
											iDamage = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehWeaponDamage
										ENDIF
									ENDIF
									
									VEHICLE_INDEX vehToIgnore = viHeli
									

									IF DOES_VEHICLE_USE_SCRIPTED_WEAPONS(viHeli, g_iVehicleTurretSeat)
									OR DOES_INTERIOR_USE_SCRIPTED_WEAPONS()
										IF CAN_TURRET_CAM_FIRE_AT_THIS_ANGLE()
											CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_ROCKETS: IS_CONTROL_PRESSED: g_iInteriorTurretSeat fire logic")
											

											IF NOT IS_ENTITY_DEAD(sKevCamVars.camEntity)
												CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_ROCKETS: Setting ignore to cab: ", NATIVE_TO_INT(sKevCamVars.camEntity))
												vehToIgnore =  GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sKevCamVars.camEntity)
											ENDIF

											CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_ROCKETS: IS_CONTROL_PRESSED: script fire logic")
											
											VECTOR vRocketOffset = GET_ROCKET_OFFSET()
											
											
											CDEBUG1LN(DEBUG_NET_GUN_TURRET, "SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW")
	//										IF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
	//											VECTOR vCamHeading = GET_CAM_ROT(GET_RENDERING_CAM())
	//											vRocketOffset = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vRocketOffset, vCamHeading.z, <<0.5, 0, 0>>)
	//										ENDIF
											
											CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_ROCKETS: Fire shell from: ",vRocketOffset,", to ground: ",sKevCamVars.vTargetedGround)
											
											IF IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
											OR IS_PLAYER_IN_CREATOR_AIRCRAFT(PLAYER_ID())
												vRocketOffset = GET_FINAL_RENDERED_CAM_COORD()
												VECTOR vRot = GET_FINAL_RENDERED_CAM_ROT()
												VECTOR vDir = <<-SIN(vRot.z) * COS(vRot.x), COS(vRot.z) * COS(vRot.x), SIN(vRot.x)>>
												VECTOR vOffset = <<10.0, 10.0, 10.0>>
												VECTOR vEndPoint = vRocketOffset + (vDir*vOffset)
												SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vRocketOffset, 
												vEndPoint, iDamage, TRUE, GET_SCRIPT_TURRET_WEAPON_TYPE(viHeli), 
												PLAYER_PED_ID(), DEFAULT, DEFAULT, DEFAULT, vehToIgnore, DEFAULT, DEFAULT, 
												DEFAULT, DEFAULT, DEFAULT, TRUE)

											ELSE										
												SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vRocketOffset, 
												sKevCamVars.vTargetedGround, iDamage, TRUE, GET_SCRIPT_TURRET_WEAPON_TYPE(viHeli), 
												PLAYER_PED_ID(), DEFAULT, DEFAULT, DEFAULT, vehToIgnore, DEFAULT, DEFAULT, 
												DEFAULT, DEFAULT, DEFAULT, TRUE)
											ENDIF		
											

											START_NET_TIMER(MPGlobals.stRocketCoolDownTimer)
												

											IF m_eTurretFireSounds != TFSS_INVALID
												CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[heli_gun] Sound ", GET_TURRET_FIRE_SOUNDS_ELEMENT(m_eTurretFireSounds, TFSK_FIRE), " playing from ",GET_TURRET_FIRE_SOUNDS_NAME(m_eTurretFireSounds))
												PLAY_SOUND_FROM_ENTITY(-1, GET_TURRET_FIRE_SOUNDS_ELEMENT(m_eTurretFireSounds, TFSK_FIRE), viHeli, GET_TURRET_FIRE_SOUNDS_NAME(m_eTurretFireSounds), TRUE, GET_FIRE_RANGE_FOR_SOUND(m_eTurretFireSounds, TFSK_FIRE))
												
												IF m_eTurretFireSounds = TFSS_ROCKETS //B* 4233394 - Can't hear rocket launch sound locally
													PLAY_SOUND_FRONTEND(-1, GET_TURRET_FIRE_SOUNDS_ELEMENT(m_eTurretFireSounds, TFSK_FIRE))
												ENDIF
											ELSE
												CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[heli_gun] Sound is invalid playing from ",GET_TURRET_FIRE_SOUNDS_NAME(m_eTurretFireSounds))
											ENDIF
										ENDIF
									ELSE
										IF NOT DOES_VEHICLE_USE_SCRIPTED_WEAPONS(viHeli, g_iVehicleTurretSeat)
											CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_ROCKETS: DOES_VEHICLE_USE_SCRIPTED_WEAPONS = FALSE")
										ENDIF
									ENDIF	
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					IF NOT ((IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RT) OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RT)) AND NOT bWeaponsDisabledForPassive AND NOT HAS_NET_TIMER_STARTED(MPGlobals.stRocketCoolDownTimer)	)
						IF HAS_NET_TIMER_STARTED(MPGlobals.stRocketCoolDownTimer)
//							CDEBUG1LN(DEBUG_NET_GUN_TURRET, "here stRocketCoolDownTimer started")
						ELSE
//							CDEBUG1LN(DEBUG_NET_GUN_TURRET, "here stRocketCoolDownTimer not started")
						ENDIF
						
					ENDIF
					
					IF bWeaponsDisabledForPassive
						CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_ROCKETS: bWeaponsDisabledForPassive = TRUE")
					ENDIF
					#ENDIF
				ENDIF
			ENDIF
			
			// Alternative Firing
			IF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
				IF HAS_MODEL_LOADED(eModelToLoad)
					#IF IS_DEBUG_BUILD
					IF HAS_MODEL_LOADED(eModelToLoad)
	//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_ROCKETS: HAS_MODEL_LOADED: TRUE for Model = ", eModelToLoad)			
					ENDIF
					#ENDIF
					
					IF ((IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_LT) OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_LT)) AND NOT HAS_NET_TIMER_STARTED(stRocketCoolDownTimerAlt))
						CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_ROCKETS: IS_CONTROL_PRESSED: INPUT_ATTACK #1, g_iShouldLaunchTruckTurret: ",g_iShouldLaunchTruckTurret)
						IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
							IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								IF DOES_CAM_EXIST(sKevCamVars.camChopper)
									IF IS_CAM_ACTIVE(sKevCamVars.camChopper)
										INT iDamage = 250
										
										IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MISSION)
											INT iVeh = -1
											
											IF DECOR_IS_REGISTERED_AS_TYPE("MC_EntityID", DECOR_TYPE_INT)
												IF DECOR_EXIST_ON(viHeli,"MC_EntityID")
													iveh = DECOR_GET_INT(viHeli,"MC_EntityID")
												ENDIF
											ENDIF
											
											IF iVeh != -1
											AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehWeaponDamage != -1
												iDamage = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehWeaponDamage
											ENDIF
										ENDIF
										
										VEHICLE_INDEX vehToIgnore = viHeli
										

										IF DOES_VEHICLE_USE_SCRIPTED_WEAPONS(viHeli, g_iVehicleTurretSeat)
										OR DOES_INTERIOR_USE_SCRIPTED_WEAPONS()
											CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_ROCKETS: IS_CONTROL_PRESSED: g_iInteriorTurretSeat fire logic")
											

											IF NOT IS_ENTITY_DEAD(sKevCamVars.camEntity)
												CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_ROCKETS: Setting ignore to cab: ", NATIVE_TO_INT(sKevCamVars.camEntity))
												vehToIgnore =  GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sKevCamVars.camEntity)
											ENDIF

											CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_ROCKETS: IS_CONTROL_PRESSED: script fire logic")
											
											VECTOR vRocketOffset = GET_ROCKET_OFFSET()
											
											
											CDEBUG1LN(DEBUG_NET_GUN_TURRET, "SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW")
											IF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
												VECTOR vCamHeading = GET_CAM_ROT(GET_RENDERING_CAM())
												vRocketOffset = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vRocketOffset, vCamHeading.z, <<-0.5, 0, 0>>)
											ENDIF
											CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_ROCKETS: Fire ALT shell from: ",vRocketOffset,", to ground: ",sKevCamVars.vTargetedGround)
											
																				
											SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vRocketOffset, 
												sKevCamVars.vTargetedGround, iDamage, TRUE, WEAPONTYPE_VEHICLE_PLAYER_BULLET, 
												PLAYER_PED_ID(), DEFAULT, DEFAULT, DEFAULT, vehToIgnore, DEFAULT, DEFAULT, 
												DEFAULT, DEFAULT, DEFAULT, TRUE)
												
													
											
											START_NET_TIMER(stRocketCoolDownTimerAlt) // Allows player to use left trigger as well as right trigger for turrets on IAA mission. seperate cool down timer is needed for alt fire
											CDEBUG1LN(DEBUG_NET_GUN_TURRET, "stRocketCoolDownTimerAlt set")

											IF m_eTurretFireSounds != TFSS_INVALID
												CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[heli_gun] Sound ", GET_TURRET_FIRE_SOUNDS_ELEMENT(m_eTurretFireSounds, TFSK_MACHINEGUN), " playing from ",GET_TURRET_FIRE_SOUNDS_NAME(m_eTurretFireSounds))
												PLAY_SOUND_FROM_ENTITY(-1, GET_TURRET_FIRE_SOUNDS_ELEMENT(m_eTurretFireSounds, TFSK_MACHINEGUN), viHeli, GET_TURRET_FIRE_SOUNDS_NAME(m_eTurretFireSounds), TRUE, 120)
											ELSE
												CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[heli_gun] Sound is invalid playing from ",GET_TURRET_FIRE_SOUNDS_NAME(m_eTurretFireSounds))
											ENDIF
										ELSE
											IF NOT DOES_VEHICLE_USE_SCRIPTED_WEAPONS(viHeli, g_iVehicleTurretSeat)
												CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_ROCKETS: DOES_VEHICLE_USE_SCRIPTED_WEAPONS = FALSE")
											ENDIF
										ENDIF									
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						
						IF NOT ((IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_AIM) OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_AIM)) AND IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE) AND NOT HAS_NET_TIMER_STARTED(stRocketCoolDownTimerAlt))
							IF HAS_NET_TIMER_STARTED(stRocketCoolDownTimerAlt)
//								CDEBUG1LN(DEBUG_NET_GUN_TURRET, "here stRocketCoolDownTimerAlt started")
							ELSE
//								CDEBUG1LN(DEBUG_NET_GUN_TURRET, "here stRocketCoolDownTimerAlt not started")
							ENDIF
						ENDIF

					ENDIF
				ENDIF
			ENDIF
			IF HAS_NET_TIMER_STARTED(MPGlobals.stRocketCoolDownTimer)
			OR HAS_NET_TIMER_STARTED(stRocketCoolDownTimerAlt)
				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_ROCKETS: turret net timer still cooling down")

				
				#IF IS_DEBUG_BUILD
				
				IF NOT HAS_MODEL_LOADED(eModelToLoad)
					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_ROCKETS: model not loaded: ", GET_MODEL_NAME_FOR_DEBUG(eModelToLoad))
				ENDIF
				#ENDIF
				
				IF DOES_VEHICLE_USE_SCRIPTED_WEAPONS(viHeli, g_iVehicleTurretSeat)
				OR DOES_INTERIOR_USE_SCRIPTED_WEAPONS()
					IF IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
					OR IS_PLAYER_IN_CREATOR_TRAILER(PLAYER_ID())
						IF HAS_NET_TIMER_EXPIRED(MPGlobals.stRocketCoolDownTimer, FIRE_TRUCK_TURRET_COOLDOWN_TIME)
							CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_ROCKETS: timer stuff: RESET_NET_TIMER: FIRE_TRUCK_TURRET_COOLDOWN_TIME")
							RESET_NET_TIMER(MPGlobals.stRocketCoolDownTimer)
							SCRIPT_TIMER stTemp
							MPGlobals.stRocketCoolDownTimer = stTemp
						ENDIF
					ELIF IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
					OR IS_PLAYER_IN_CREATOR_AIRCRAFT(PLAYER_ID())
						IF HAS_NET_TIMER_EXPIRED(MPGlobals.stRocketCoolDownTimer, FIRE_AOC_TURRET_COOLDOWN)
							CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_ROCKETS: timer stuff: RESET_NET_TIMER: FIRE_AOC_TURRET_COOLDOWN")
							RESET_NET_TIMER(MPGlobals.stRocketCoolDownTimer)
							SCRIPT_TIMER stTemp
							MPGlobals.stRocketCoolDownTimer = stTemp
						ENDIF
					ELIF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
						IF HAS_NET_TIMER_STARTED(MPGlobals.stRocketCoolDownTimer)
							IF HAS_NET_TIMER_EXPIRED(MPGlobals.stRocketCoolDownTimer, 500)
								CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_ROCKETS: timer stuff: RESET_NET_TIMER: 5000")
								RESET_NET_TIMER(MPGlobals.stRocketCoolDownTimer)
								SCRIPT_TIMER stTemp1
								MPGlobals.stRocketCoolDownTimer = stTemp1
							ELSE
	//							CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_ROCKETS: timer stuff not over yet")
							ENDIF
						ENDIF

						IF HAS_NET_TIMER_STARTED(stRocketCoolDownTimerAlt)
							IF HAS_NET_TIMER_EXPIRED(stRocketCoolDownTimerAlt, 50)
								CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_ROCKETS: timer stuff alt: RESET_NET_TIMER: 5000")
								RESET_NET_TIMER(stRocketCoolDownTimerAlt)
								SCRIPT_TIMER stTemp2
								stRocketCoolDownTimerAlt = stTemp2
							ELSE
	//							CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_ROCKETS: timer stuff alt not over yet")
							ENDIF
						ENDIF
					ELSE
						IF HAS_NET_TIMER_EXPIRED(MPGlobals.stRocketCoolDownTimer, FIRE_ROCKET_COOLDOWN_TIME)
							CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_ROCKETS: timer stuff: RESET_NET_TIMER: FIRE_ROCKET_COOLDOWN_TIME")
							RESET_NET_TIMER(MPGlobals.stRocketCoolDownTimer)
							SCRIPT_TIMER stTemp
							MPGlobals.stRocketCoolDownTimer = stTemp
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_ROCKETS: IS_MP_PASSIVE_MODE_ENABLED = TRUE")
		ENDIF
	ELSE
//		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_ROCKETS: in here ")
	ENDIF
	
ENDPROC

FUNC VECTOR GET_INTERPED_VECTOR(VECTOR vTargetRot, VECTOR vCurrentRot)
	
	VECTOR vNewRot
	float fAngDiff
	
	//need to cater for the z rot transitioning from -180 to +180 and vice versa
	fAngDiff = vTargetRot.z - vCurrentRot.z
	
	IF ABSF(fAngDiff) > 180
		IF fAngDiff > 0
			fAngDiff -= 360.0
		ELSE
			fAngDiff += 360.0
		ENDIF
		
		vCurrentRot.z = vTargetRot.z - fAngDiff
		
	ENDIF
	
	vNewRot = vCurrentRot + ((vTargetRot - vCurrentRot) * vLerpValue)
	
	IF ABSF(vNewRot.x - vTargetRot.x) < fThreshold
		vNewRot = vTargetRot
	ENDIF
	
	IF ABSF(vNewRot.y - vTargetRot.y) < fThreshold
		vNewRot = vTargetRot
	ENDIF
	
	IF ABSF(vNewRot.z - vTargetRot.z) < fThreshold
		vNewRot = vTargetRot
	ENDIF
	
	RETURN vNewRot
	
ENDFUNC

FUNC FLOAT GET_INTERPED_FLOAT(FLOAT fTargetRot, FLOAT fCurrentRot)
	
	FLOAT fNewRot
	
	fNewRot = fCurrentRot + ((fTargetRot - fCurrentRot) * vLerpValue.x)
	
	IF ABSF(fNewRot - fTargetRot) < fThreshold
		fNewRot = fTargetRot
	ENDIF
	
	RETURN fNewRot
	
ENDFUNC

INT iSynchedCamStage

PROC PROCESS_SYNCHING_PILOT_CAMERA_WITH_COPILOT_CAMERA()
	
	VECTOR vCoPilotCamRot
	FLOAT fCoPilotCamFov
	
	IF bCoPilotExists
		IF NOT bCoPilotDead
		AND NATIVE_TO_INT(PlayerIdCoPilot) <> -1
			// Get copilot cam values.
			vCoPilotCamRot = GET_FINAL_RENDERED_REMOTE_PLAYER_CAM_ROT(PlayerIdCoPilot)
			fCoPilotCamFov = GET_FINAL_RENDERED_REMOTE_PLAYER_CAM_FOV(PlayerIdCoPilot)
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_SYNCHING_PILOT_CAMERA_WITH_COPILOT_CAMERA()")
			SWITCH iSynchedCamStage
				
				CASE 0
					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_SYNCHING_PILOT_CAMERA_WITH_COPILOT_CAMERA() 0")
					// Set pilot cam.
					IF DOES_CAM_EXIST(sKevCamVars.camChopper)
						sKevCamVars.vCamRot = vCoPilotCamRot
						sKevCamVars.fFOV = fCoPilotCamFov
						SET_CAM_ROT(sKevCamVars.camChopper, sKevCamVars.vCamRot)
						SET_CAM_FOV(sKevCamVars.camChopper, sKevCamVars.fFOV)
						
						iSynchedCamStage++
					ENDIF
					
				BREAK
				
				CASE 1
					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_SYNCHING_PILOT_CAMERA_WITH_COPILOT_CAMERA() 1")
					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_SYNCHING_PILOT_CAMERA_WITH_COPILOT_CAMERA() 1, vCoPilotCamRot = ", vCoPilotCamRot)
					// Interpolation to stop jerky updating.
					vCoPilotCamRot = GET_INTERPED_VECTOR(vCoPilotCamRot, sKevCamVars.vCamRot)
					fCoPilotCamFov = GET_INTERPED_FLOAT(fCoPilotCamFov, sKevCamVars.fFOV)
					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_SYNCHING_PILOT_CAMERA_WITH_COPILOT_CAMERA() 1, GET_INTERPED_VECTOR vCoPilotCamRot = ", vCoPilotCamRot)
					// Set pilot cam.
					IF DOES_CAM_EXIST(sKevCamVars.camChopper)
						sKevCamVars.vCamRot = vCoPilotCamRot
						sKevCamVars.fFOV = fCoPilotCamFov
						SET_CAM_ROT(sKevCamVars.camChopper, sKevCamVars.vCamRot)
						SET_CAM_FOV(sKevCamVars.camChopper, sKevCamVars.fFOV)
					ENDIF
					
				BREAK
				
			ENDSWITCH
			
		ENDIF
	ENDIF
	
ENDPROC

//PROC DISPLAYER_DEBUG_CREATOR_INFO()
//	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
//		INT iStrOffset = 0, iColumn = 0, iRow = 0
//		INT iRed, iGreen, iBlue, iAlpha
//		TEXT_LABEL_63 str
//		str += "g_bIAAGunCamerasEnabled: "
//		IF g_bIAAGunCamerasEnabled
//			str += "TRUE"	
//		ELSE
//			str += "FALSE"	
//		ENDIF
//		
//		DRAW_DEBUG_TEXT_2D(str, <<0.3,0.2,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)+(<<0.0,0.017,0>>*TO_FLOAT(iRow))), iRed, iGreen, iBlue, iAlpha)
//		IF g_bIAAGunCamerasEnabled = FALSE
//			CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "g_bIAAGunCamerasEnabled = FALSE")
//		ELSE
//			CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "g_bIAAGunCamerasEnabled = TRUE")
//		ENDIF
//ENDPROC

PROC DEFAULT_SHUFFLE_LOGIC()
	IF NOT IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(viHeli)
	OR GET_ENTITY_MODEL(viHeli) = AKULA
		bVehicleSeatShuffleInputAccept = FALSE
		EXIT
	ENDIF
	
	IF bVehicleSeatShuffleInputAccept
		CLEAR_TURN_ON_HELI_HELP()
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "MAINTAIN_VEHICLE_SEAT_SHUFFLE")
		bVehicleSeatShuffleInputAccept = FALSE
		IF bIAmCoPilot
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "bIAmCoPilot")
			IF IS_VEHICLE_SEAT_FREE(viHeli, VS_BACK_LEFT)
				TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(PLAYER_PED_ID(), viHeli)
				vsSeatToShuffleTo = VS_BACK_LEFT
			ELIF IS_VEHICLE_SEAT_FREE(viHeli, VS_BACK_RIGHT)
				TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(PLAYER_PED_ID(), viHeli, TRUE)
				vsSeatToShuffleTo = VS_BACK_RIGHT
			ELIF iCannotMoveHelpDisplayCount < CANNOT_MOVE_HELP_DISPLAY_COUNT_MAX
				PRINT_HELP( "BOMBGUN_FULL" )
				iCannotMoveHelpDisplayCount++
			ENDIF

		ELIF bIAmTurretPilot1 // nose turret
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "bIAmTurretPilot1")
			IF IS_VEHICLE_SEAT_FREE(viHeli, VS_BACK_RIGHT)
				TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(PLAYER_PED_ID(), viHeli)
				vsSeatToShuffleTo = VS_BACK_RIGHT
			ELIF IS_VEHICLE_SEAT_FREE(viHeli, VS_FRONT_RIGHT)
//				PRINT_HELP("BOMBGUN_2c1") //You are sitting in the Nose Turret seat of a Bombushka. Press ~INPUT_CONTEXT~ to enter the gun camera. ~n~Hold ~INPUT_CONTEXT~ to move to the Top Turret.
				TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(PLAYER_PED_ID(), viHeli, TRUE)
				vsSeatToShuffleTo = VS_FRONT_RIGHT
			ELIF iCannotMoveHelpDisplayCount < CANNOT_MOVE_HELP_DISPLAY_COUNT_MAX
				//no seats available
				PRINT_HELP( "BOMBGUN_FULL" )
				iCannotMoveHelpDisplayCount++
			ENDIF
			
		ELIF bIAmTurretPilot2 // rear turret
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "bIAmTurretPilot2")
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "bCoPilotExists = ",bCoPilotExists)
			IF IS_VEHICLE_SEAT_FREE(viHeli, VS_FRONT_RIGHT)
//				PRINT_HELP("BOMBGUN_3c1") //You are sitting in the Rear Turret seat of a Bombushka. Press ~INPUT_CONTEXT~ to enter the gun camera. ~n~Hold ~INPUT_CONTEXT~ to move to the Top Turret.
				TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(PLAYER_PED_ID(), viHeli)
				vsSeatToShuffleTo = VS_FRONT_RIGHT
			ELIF IS_VEHICLE_SEAT_FREE(viHeli, VS_BACK_LEFT)
//				PRINT_HELP("BOMBGUN_3c2") //You are sitting in the Rear Turret seat of a Bombushka. Press ~INPUT_CONTEXT~ to enter the gun camera. ~n~Hold ~INPUT_CONTEXT~ to move to the Nose Turret.
				TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(PLAYER_PED_ID(), viHeli, TRUE)
				vsSeatToShuffleTo = VS_BACK_LEFT
			ELIF iCannotMoveHelpDisplayCount < CANNOT_MOVE_HELP_DISPLAY_COUNT_MAX
				//no seats available
				PRINT_HELP( "BOMBGUN_FULL" )
				iCannotMoveHelpDisplayCount++
			ENDIF
		ENDIF
		//	CLEAR_TURN_ON_HELI_HELP()	
	ENDIF
	IF vsSeatToShuffleTo != VS_ANY_PASSENGER
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "vsSeatToShuffleTo = ", ENUM_TO_INT(vsSeatToShuffleTo))
		IF GET_PED_IN_VEHICLE_SEAT(viHeli, vsSeatToShuffleTo) = PLAYER_PED_ID()
			bStillShuffling = FALSE
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "bStillShuffling = FALSE")
			vsSeatToShuffleTo = VS_ANY_PASSENGER
			bShownTurnOnHeliGunHelp = FALSE
		ELSE
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "bStillShuffling = TRUE")
			bStillShuffling = TRUE
			IF NOT IS_VEHICLE_SEAT_FREE(viHeli, vsSeatToShuffleTo)
				SWITCH vsSeatToShuffleTo
					CASE VS_FRONT_RIGHT
						PRINT_HELP("BOMBGUN_1o", 1000)
					BREAK
					CASE VS_BACK_LEFT
						PRINT_HELP("BOMBGUN_2o", 1000)
					BREAK
					CASE VS_BACK_RIGHT
						PRINT_HELP("BOMBGUN_3o", 1000)
					BREAK
				ENDSWITCH
				bStillShuffling = FALSE
				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "bStillShuffling = FALSE")
				vsSeatToShuffleTo = VS_ANY_PASSENGER
				bShownTurnOnHeliGunHelp = FALSE
				
			ELSE
				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "bStillShuffling = TRUE, and seat trying to get to is free")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC VOLATOL_SEAT_SHUFFLE_LOGIC()
	IF NOT IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(viHeli)
		bVehicleSeatShuffleInputAccept = FALSE
		EXIT
	ENDIF
	
	IF bVehicleSeatShuffleInputAccept
		CLEAR_TURN_ON_HELI_HELP()
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "VOLATOL_SEAT_SHUFFLE_LOGIC")
		bVehicleSeatShuffleInputAccept = FALSE
//		IF bIAmCoPilot
//			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "bIAmCoPilot")
//			IF IS_VEHICLE_SEAT_FREE(viHeli, VS_BACK_LEFT)
//				TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(PLAYER_PED_ID(), viHeli)
//				vsSeatToShuffleTo = VS_BACK_LEFT
//			ELIF IS_VEHICLE_SEAT_FREE(viHeli, VS_BACK_RIGHT)
//				TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(PLAYER_PED_ID(), viHeli, TRUE)
//				vsSeatToShuffleTo = VS_BACK_RIGHT
//			ELIF iCannotMoveHelpDisplayCount < CANNOT_MOVE_HELP_DISPLAY_COUNT_MAX
//				PRINT_HELP( "BOMBGUN_FULL" )
//				iCannotMoveHelpDisplayCount++
//			ENDIF

		IF bIAmTurretPilot1 // nose turret
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "bIAmTurretPilot1")
			IF IS_VEHICLE_SEAT_FREE(viHeli, VS_BACK_RIGHT)
				TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(PLAYER_PED_ID(), viHeli)
				vsSeatToShuffleTo = VS_BACK_RIGHT
			ELIF iCannotMoveHelpDisplayCount < CANNOT_MOVE_HELP_DISPLAY_COUNT_MAX
				//no seats available
				PRINT_HELP( "BOMBGUN_FULL" )
				iCannotMoveHelpDisplayCount++
			ENDIF
			
		ELIF bIAmTurretPilot2 // rear turret
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "bIAmTurretPilot2")
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "bCoPilotExists = ",bCoPilotExists)
			IF IS_VEHICLE_SEAT_FREE(viHeli, VS_BACK_LEFT)
				TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(PLAYER_PED_ID(), viHeli, TRUE)
				vsSeatToShuffleTo = VS_BACK_LEFT
			ELIF iCannotMoveHelpDisplayCount < CANNOT_MOVE_HELP_DISPLAY_COUNT_MAX
				//no seats available
				PRINT_HELP( "BOMBGUN_FULL" )
				iCannotMoveHelpDisplayCount++
			ENDIF
		ENDIF
	ENDIF
	IF vsSeatToShuffleTo != VS_ANY_PASSENGER
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "vsSeatToShuffleTo = ", ENUM_TO_INT(vsSeatToShuffleTo))
		IF GET_PED_IN_VEHICLE_SEAT(viHeli, vsSeatToShuffleTo) = PLAYER_PED_ID()
			bStillShuffling = FALSE
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "bStillShuffling = FALSE")
			vsSeatToShuffleTo = VS_ANY_PASSENGER
			bShownTurnOnHeliGunHelp = FALSE
		ELSE
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "bStillShuffling = TRUE")
			bStillShuffling = TRUE
			IF NOT IS_VEHICLE_SEAT_FREE(viHeli, vsSeatToShuffleTo)
				SWITCH vsSeatToShuffleTo
//					CASE VS_FRONT_RIGHT
//						PRINT_HELP("BOMBGUN_1o", 1000)
//					BREAK
					CASE VS_BACK_LEFT
						PRINT_HELP("BOMBGUN_2o", 1000)
					BREAK
					CASE VS_BACK_RIGHT
						PRINT_HELP("BOMBGUN_1o", 1000)
					BREAK
				ENDSWITCH
				bStillShuffling = FALSE
				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "bStillShuffling = FALSE")
				vsSeatToShuffleTo = VS_ANY_PASSENGER
				bShownTurnOnHeliGunHelp = FALSE
				
			ELSE
				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "bStillShuffling = TRUE, and seat trying to get to is free")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_VEHICLE_SEAT_SHUFFLE()
	IF GET_ENTITY_MODEL(viHeli) = VOLATOL
		VOLATOL_SEAT_SHUFFLE_LOGIC()
	ELSE
		DEFAULT_SHUFFLE_LOGIC()
	ENDIF
ENDPROC

/// PURPOSE:
///    Calculates a score for how far a screen coord is from the center of the screen by adding together the distances from the center on the x and y axes. 
/// PARAMS:
///    x - x component of screen coord to check.
///    y - y component of screen coord to check.
/// RETURNS:
///    FLOAT - score indicating how close to the center of the screen the coord passed as argument is. 
FUNC FLOAT CALCULATE_NEAR_MIDDLE_OF_SCREEN_SCORE(FLOAT x, FLOAT y, FLOAT &fXScore, FLOAT &fYScore)
	
	FLOAT fDistanceFromCentreX
	FLOAT fDistanceFromCentreY
	
	IF x <= 0.5
		fDistanceFromCentreX = 0.5 - x
	ELSE
		fDistanceFromCentreX = x - 0.5
	ENDIF

	IF y <= 0.5
		fDistanceFromCentreY = 0.5 - y
	ELSE
		fDistanceFromCentreY = y - 0.5
	ENDIF
	
	fXScore = fDistanceFromCentreX
	fYScore = fDistanceFromCentreY
	
	RETURN (fDistanceFromCentreX + fDistanceFromCentreY)

ENDFUNC

func bool IS_POINT_IN_SCREEN_AREA_HELI_GUN(float sx, float sy, float x1, float y1, float x2, float y2)

	if sX>= x1 and sX<=x2
		if sY>=y1  and sy<=y2
			return TRUE
		ENDIF
	ENDIF

	return FALSE
	
ENDFUNC

PROC SET_TAG_PLAYER_PROMPT_LABEL_TEXT_PROPERTIES(TEXT_PLACEMENT &eTP, TEXT_STYLE &eTextStyle, FLOAT fX, FLOAT fY)
	
	eTP.x = fX
	eTP.y = fY
	eTextStyle.aFont = FONT_STANDARD
	eTextStyle.XScale = 0.25
	eTextStyle.YScale = 0.25
	eTextStyle.drop = DROPSTYLE_OUTLINEONLY
	GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(HUD_COLOUR_WHITE), eTextStyle.r, eTextStyle.g, eTextStyle.b, eTextStyle.a)	
ENDPROC

FUNC BOOL SHOULD_BLOCK_TAG_ABILITY()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_CarnageBarFilled_PassRule_CarnageFull)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_CarnageBarFilled_PassRule_HeliFootageFull)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC 

PROC DRAW_TAG_PLAYER_PROMPT(FLOAT fX, FLOAT fY)
	
	IF SHOULD_BLOCK_TAG_ABILITY()
		EXIT
	ENDIF	
	
	TEXT_PLACEMENT eTP
	TEXT_STYLE eTextStyle
	
	SET_TAG_PLAYER_PROMPT_LABEL_TEXT_PROPERTIES(eTP, eTextStyle, fX, fY)
	
	IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
		//DRAW_TEXT(eTP, eTextStyle, "HUNTGUN_5_KM") //removed as per bug 1595716
		PRINT_HELP("HUNTGUN_5_KM")
	ELSE
		//DRAW_TEXT(eTP, eTextStyle, "HUNTGUN_5") //removed as per bug 1595716
		PRINT_HELP("HUNTGUN_5")
	ENDIF
									
ENDPROC

PROC MAINTAIN_TAGGING()
	
	IF SHOULD_BLOCK_TAG_ABILITY()
		EXIT
	ENDIF
	
	INT i
	PED_INDEX pedId
	PLAYER_INDEX playerId
	VECTOR vCoords, vClosestPlayerCoords
	FLOAT fMiddleScreenScore
	FLOAT fClosestMiddlePedScore = 99999.0
	INT iClosestToMiddlePed = (-1)
	FLOAT fScreenX, fScreenY, fTempX, fTempY
	BOOL bTagged
	
	// Get player closest to the middle of the screen.
	REPEAT NUM_NETWORK_PLAYERS i
		
		bTagged = IS_BIT_SET(g_iHeliGunTaggedPlayers, i)
		
		playerId = INT_TO_NATIVE(PLAYER_INDEX, i)
		IF playerId != PLAYER_ID()
			#IF IS_DEBUG_BUILD IF bDoHeliTaggingExtraDebug PRINTLN("[Heli Tagging] - player ", i, ", is not local player.") ENDIF #ENDIF
			IF IS_NET_PLAYER_OK(playerId)
				#IF IS_DEBUG_BUILD IF bDoHeliTaggingExtraDebug PRINTLN("[Heli Tagging] - player ", i, ", is is net ok.") ENDIF #ENDIF
				pedId = GET_PLAYER_PED(playerId)
				vCoords = GET_ENTITY_COORDS(pedId)
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vCoords) <= 150.0
					#IF IS_DEBUG_BUILD IF bDoHeliTaggingExtraDebug PRINTLN("[Heli Tagging] - player ", i, ", is <= 150.0m away.") ENDIF #ENDIF
					IF CAN_I_SEE_PLAYER(playerId)
						#IF IS_DEBUG_BUILD IF bDoHeliTaggingExtraDebug PRINTLN("[Heli Tagging] - player ", i, ", CAN_I_SEE_PLAYER = TRUE.") ENDIF #ENDIF
						IF GET_SCREEN_COORD_FROM_WORLD_COORD(vCoords, fScreenX, fScreenY)
							#IF IS_DEBUG_BUILD IF bDoHeliTaggingExtraDebug PRINTLN("[Heli Tagging] - player ", i, ", GET_SCREEN_COORD_FROM_WORLD_COORD = TRUE.") ENDIF #ENDIF
							IF NOT bTagged
								#IF IS_DEBUG_BUILD IF bDoHeliTaggingExtraDebug PRINTLN("[Heli Tagging] - player ", i, ", bTagged = TRUE.") ENDIF #ENDIF
								IF IS_POINT_IN_SCREEN_AREA_HELI_GUN(fScreenX, fScreenY, 0.4,0.4,0.6,0.6)
									#IF IS_DEBUG_BUILD IF bDoHeliTaggingExtraDebug PRINTLN("[Heli Tagging] - player ", i, ", IS_POINT_IN_SCREEN_AREA_HELI_GUN(fScreenX, fScreenY, 0.4,0.4,0.6,0.6) = TRUE.") ENDIF #ENDIF
									fMiddleScreenScore = CALCULATE_NEAR_MIDDLE_OF_SCREEN_SCORE(fScreenX, fScreenY, fTempX, fTempY)
									IF fMiddleScreenScore < fClosestMiddlePedScore
										fClosestMiddlePedScore = fMiddleScreenScore
										iClosestToMiddlePed = i
										vClosestPlayerCoords = vCoords
									ENDIF
								ELSE
									#IF IS_DEBUG_BUILD IF bDoHeliTaggingExtraDebug PRINTLN("[Heli Tagging] - player ", i, ", IS_POINT_IN_SCREEN_AREA_HELI_GUN(fScreenX, fScreenY, 0.4,0.4,0.6,0.6) = FALSE.") ENDIF #ENDIF
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD IF bDoHeliTaggingExtraDebug PRINTLN("[Heli Tagging] - player ", i, ", bTagged = FALSE.") ENDIF #ENDIF
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD IF bDoHeliTaggingExtraDebug PRINTLN("[Heli Tagging] - player ", i, ", GET_SCREEN_COORD_FROM_WORLD_COORD = FALSE.") ENDIF #ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD IF bDoHeliTaggingExtraDebug PRINTLN("[Heli Tagging] - player ", i, ", CAN_I_SEE_PLAYER = FALSE.") ENDIF #ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD IF bDoHeliTaggingExtraDebug PRINTLN("[Heli Tagging] - player ", i, ", is > 150.0m away.") ENDIF #ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF iClosestToMiddlePed != (-1)		
		#IF IS_DEBUG_BUILD IF bDoHeliTaggingExtraDebug PRINTLN("[Heli Tagging] - iClosestToMiddlePed ", iClosestToMiddlePed) ENDIF #ENDIF
		IF GET_SCREEN_COORD_FROM_WORLD_COORD(vClosestPlayerCoords + << 0.0, 0.0, -1.0>>, fScreenX, fScreenY)
			#IF IS_DEBUG_BUILD IF bDoHeliTaggingExtraDebug PRINTLN("[Heli Tagging] - GET_SCREEN_COORD_FROM_WORLD_COORD = TRUE.") ENDIF #ENDIF
			DRAW_TAG_PLAYER_PROMPT(fScreenX, fScreenY)
			IF NOT IS_PAUSE_MENU_ACTIVE()	
				#IF IS_DEBUG_BUILD IF bDoHeliTaggingExtraDebug PRINTLN("[Heli Tagging] - IS_PAUSE_MENU_ACTIVE = TRUE.") ENDIF #ENDIF

				BOOL bIsTagPressed = FALSE
				IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
					bIsTagPressed = IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_AIM)
				ELSE
					bIsTagPressed = IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				ENDIF
				
				IF bIsTagPressed
					#IF IS_DEBUG_BUILD IF bDoHeliTaggingExtraDebug PRINTLN("[Heli Tagging] - INPUT_FRONTEND_ACCEPT/INPUT_ATTACK just pressed.") ENDIF #ENDIF
					SET_BIT(g_iHeliGunTaggedPlayers, iClosestToMiddlePed)
					PRINTLN("[Heli Tagging] - player pressed X, tagging player ", iClosestToMiddlePed)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC ORIENTATE_CAM_TO_FACE_SAME_DIRECTION_AS_CHOPPER()
	CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ORIENTATE_CAM_TO_FACE_SAME_DIRECTION_AS_CHOPPER: ")
	VECTOR vNewCoords
	
	IF bHeliDriveable
	OR g_iShouldLaunchTruckTurret != -1
	OR g_iInteriorTurretSeat != -1 
	OR IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(viHeli)
		
		IF g_iShouldLaunchTruckTurret != -1
		
			VECTOR vTempOffset
			SWITCH g_iShouldLaunchTruckTurret
				CASE 1 // Front
					vTempOffset = <<0.0, 50.0000, 1.0>>
				BREAK 
				CASE 2 // Rear
					vTempOffset = <<0.0, -50.0000, 1.0>>
				BREAK
				CASE 3 // Rear
					vTempOffset = <<0.0, -50.0000, 1.0>>
				BREAK
			ENDSWITCH

			vNewCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viHeli, vTempOffset)
			
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ORIENTATE_CAM_TO_FACE_SAME_DIRECTION_AS_CHOPPER: vTempOffset = ", vTempOffset, ", vNewCoords = ", vNewCoords)
		ELIF g_iInteriorTurretSeat != -1
			VECTOR vTempOffset
			SWITCH g_iShouldLaunchTruckTurret
				CASE 1 // Front
					vTempOffset = <<0.0, 50.0000, 1.0>>
				BREAK 
				CASE 2 // Rear
					vTempOffset = <<0.0, -50.0000, 1.0>>
				BREAK
				CASE 3 // Rear
					vTempOffset = <<0.0, -50.0000, 1.0>>
				BREAK
			ENDSWITCH

			vNewCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viHeli, vTempOffset)
			
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ORIENTATE_CAM_TO_FACE_SAME_DIRECTION_AS_CHOPPER: g_iInteriorTurretSeat = ", g_iInteriorTurretSeat,", vTempOffset = ", vTempOffset, ", vNewCoords = ", vNewCoords)
		ELSE
		
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ORIENTATE_CAM_TO_FACE_SAME_DIRECTION_AS_CHOPPER: g_iShouldLaunchTruckTurret = -1")
			vNewCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viHeli, << 0.0, 20.0, -1.0 >>)
		
		ENDIF
		
		RUN_HELICOPTER_HUD(sKevCamVars)
		ControlChopperCamera(sKevCamVars)
		POINT_CHOPPER_CAM_AT_COORD(sKevCamVars, vNewCoords)
		SET_CAM_CONTROLS_MINI_MAP_HEADING(sKevCamVars.camChopper, TRUE)
		
			IF g_iShouldLaunchTruckTurret != -1
			OR IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(viHeli)
			OR g_iInteriorTurretSeat != -1
				EXIT // Quit early, no need to continue here for turrets as the rotation cannot be calculated until next frame.
			ENDIF
		
		sKevCamVars.vCamRot = GET_ENTITY_ROTATION(viHeli) - <<3,0,0>>
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ORIENTATE_CAM_TO_FACE_SAME_DIRECTION_AS_CHOPPER: Set camera rotation to: ", sKevCamVars.vCamRot)
	ELSE
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ORIENTATE_CAM_TO_FACE_SAME_DIRECTION_AS_CHOPPER: bHeliDriveable = FALSE")
	ENDIF
	
ENDPROC

PROC CONTROL_CHOPPER_AUDIO_LEVEL()
	
	IF bHeliDriveable
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viHeli)
			SWITCH iAudioLevelFlag
				CASE 0
					IF IS_HELIHUD_ACTIVE(sKevCamVars)
						IF START_AUDIO_SCENE("MP_HELI_CAM_FILTERING")
							iAudioLevelFlag++
						ENDIF
					ENDIF
				BREAK
				CASE 1					
					SET_AUDIO_SCENE_VARIABLE("MP_HELI_CAM_FILTERING","HeliFiltering",GET_ENTITY_SPEED(viHeli))
				BREAK
			ENDSWITCH
		ELSE
			IF iAudioLevelFlag != 99
			AND iAudioLevelFlag > 0
				STOP_AUDIO_SCENE("MP_HELI_CAM_FILTERING")
				iAudioLevelFlag = 99
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_HELI_VISIBILITY()
	
	IF DOES_ENTITY_EXIST(sKevCamVars.camEntity)
		IF NOT IS_ENTITY_DEAD(sKevCamVars.camEntity)
		AND NOT( NOT IS_ENTITY_DEAD(g_viGunRunTruckTailer)
				AND GET_ENTITY_MODEL(sKevCamVars.camEntity) = GET_ENTITY_MODEL(g_viGunRunTruckTailer)) 
		AND NOT IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(viHeli)
			IF GET_ENTITY_MODEL(sKevCamVars.camEntity) != VALKYRIE
			AND GET_ENTITY_MODEL(sKevCamVars.camEntity) != SAVAGE
			AND GET_ENTITY_MODEL(sKevCamVars.camEntity) != HUNTER
			AND NOT IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
			AND NOT IS_PLAYER_IN_CREATOR_AIRCRAFT(PLAYER_ID())
				SET_ENTITY_LOCALLY_INVISIBLE(sKevCamVars.camEntity)
				IF NOT bPilotDead
					SET_PLAYER_INVISIBLE_LOCALLY(PlayerIdPilot)
				ENDIF
				IF NOT bCoPilotDead
					SET_PLAYER_INVISIBLE_LOCALLY(PlayerIdCoPilot)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC LIMIT_CAM_PITCH_WHILE_GROUNDED()
	//Don't need this for Akula passenger cameras.
	IF GET_ENTITY_MODEL(viHeli) = AKULA
	AND (bIAmTurretPilot1 
	OR bIAmTurretPilot2)
		EXIT
	ENDIF

	IF IS_VEHICLE_DRIVEABLE(viHeli)
		IF !IS_ENTITY_IN_AIR(viHeli)
			IF IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(viHeli)
				IF IS_TURRET1_CAM_ON()
					IF bIAmTurretPilot1
						IF sKevCamVars.bLimitCamWhileGrounded = FALSE
							CDEBUG1LN(DEBUG_NET_GUN_TURRET, "LIMIT_CAM_PITCH_WHILE_GROUNDED: sKevCamVars.bLimitCamWhileGrounded = TRUE	")
							sKevCamVars.bLimitCamWhileGrounded = TRUE	
						ENDIF
					ENDIF
				ELIF sKevCamVars.bLimitCamWhileGrounded = TRUE	
					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "LIMIT_CAM_PITCH_WHILE_GROUNDED: sKevCamVars.bLimitCamWhileGrounded = FALSE	")
//					SET_THIRD_PERSON_CAM_RELATIVE_PITCH_LIMITS_THIS_UPDATE(MAX_CAMERA_PITCH_GROUNDED, MIN_CAMERA_PITCH)
					sKevCamVars.bLimitCamWhileGrounded = FALSE
				ENDIF
			ELSE

				IF IS_CO_PILOT_CAM_ON()
					sKevCamVars.bLimitCamWhileGrounded = TRUE	
				ELSE
					SET_THIRD_PERSON_CAM_RELATIVE_PITCH_LIMITS_THIS_UPDATE(MAX_CAMERA_PITCH_GROUNDED, MIN_CAMERA_PITCH)
					sKevCamVars.bLimitCamWhileGrounded = FALSE
				ENDIF
			ENDIF
		ELSE
			sKevCamVars.bLimitCamWhileGrounded = FALSE	
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_ROCKETS_DISABLED_HELP_TEXT()
	
	//B*url:bugstar:4219474
	SWITCH eHeliModel
		CASE AKULA
		EXIT
	ENDSWITCH
		
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciDISABLE_VALKYRIE_FRONT_GUN)
	OR g_OfficeHeliDockData.bDoingCutscene
		EXIT
	ENDIF

	IF (PlayerIdPilot = PLAYER_ID())
	AND eHeliModel != INT_TO_ENUM(MODEL_NAMES, HASH("BOMBUSHKA"))
		IF HELI_MODEL_CAN_SHOOT_ROCKETS_FROM_HELI_CAM(viHeli)
			IF bDisabledRockets
			AND NOT bShownRocketsInUseHelp
			AND (IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
			OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON))
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					// PILOT CANT CHANGE WEAPON DUE TO CO_PILOT HAVING CONTROL HELP TEXT
					PRINT_HELP("HUNTGUN_8")
					bShownRocketsInUseHelp = TRUE
					CPRINTLN(DEBUG_COLIN, "HANDLE_ROCKETS_DISABLED_HELP_TEXT - Showing Rockets in Use Help")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


SCRIPT_TIMER stTurretUpdateTimer

FUNC INT GET_TURRET_INDEX_FROM_SCRIPT_INDEX()
	
	IF IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID()) 
	OR IS_PLAYER_IN_CREATOR_TRAILER(PLAYER_ID())
		SWITCH g_iShouldLaunchTruckTurret
			DEFAULT RETURN -1
			CASE 1 RETURN 0
			CASE 2 RETURN 2
			CASE 3 RETURN 1
		ENDSWITCH
	ELIF IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
	OR IS_PLAYER_IN_CREATOR_AIRCRAFT(PLAYER_ID())
		SWITCH g_iInteriorTurretSeat
			DEFAULT RETURN -1
			CASE 1 RETURN 0
			CASE 2 RETURN 1
			CASE 3 RETURN 2
		ENDSWITCH
	ENDIF
	RETURN -1
ENDFUNC

PROC MAINTAIN_TURRET_NETWORK_DATA()
//	IF g_iShouldLaunchTruckTurret = -1
//	AND eHeliModel != INT_TO_ENUM(MODEL_NAMES, HASH("BOMBUSHKA"))
	IF NOT DOES_INTERIOR_USE_SCRIPTED_WEAPONS()
		EXIT
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(stTurretUpdateTimer)
		START_NET_TIMER(stTurretUpdateTimer)
		
	ENDIF
	
	IF NOT HAS_NET_TIMER_EXPIRED(stTurretUpdateTimer, 1000)
		EXIT
	ENDIF
	
	RESET_NET_TIMER(stTurretUpdateTimer)
	START_NET_TIMER(stTurretUpdateTimer)
	
	INT arrayIndex = GET_TURRET_INDEX_FROM_SCRIPT_INDEX()
	int turretIndex
	
	IF arrayIndex < 0
	OR arrayIndex > 3
		EXIT
	ENDIF

	
	IF NOT IS_VECTOR_ZERO(sKevCamVars.vTargetedGround)
	
		IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bTurretInUse, arrayIndex)
			SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bTurretInUse, arrayIndex)
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "MAINTAIN_TURRET_NETWORK_DATA: [MAINTAIN_TURRETS] - Setting turret ID ", g_iInteriorTurretSeat, " (index: ", arrayIndex,") in use.")
		ENDIF
		IF sKevCamVars.vTargetedGround.x > 9999.00
			sKevCamVars.vTargetedGround.x = 9999.00
		ENDIF
		IF sKevCamVars.vTargetedGround.y > 9999.00
			sKevCamVars.vTargetedGround.y = 9999.00
		ENDIF
		IF sKevCamVars.vTargetedGround.z > 9999.00
			sKevCamVars.vTargetedGround.z = 9999.00
		ENDIF
		
		IF NOT IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
		AND NOT IS_PLAYER_IN_CREATOR_AIRCRAFT(PLAYER_ID())
			IF NOT ARE_VECTORS_ALMOST_EQUAL(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].vTurretRayTrace[arrayIndex], sKevCamVars.vTargetedGround) 
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].vTurretRayTrace[arrayIndex] = sKevCamVars.vTargetedGround
			ENDIF
		ELSE
			
			VECTOR vRocketOffset 
			vRocketOffset = GET_FINAL_RENDERED_CAM_COORD()
			VECTOR vRot = GET_FINAL_RENDERED_CAM_ROT()
			VECTOR vDir = <<-SIN(vRot.z) * COS(vRot.x), COS(vRot.z) * COS(vRot.x), SIN(vRot.x)>>
			VECTOR vOffset = <<10.0, 10.0, 10.0>>
			VECTOR vEndPoint = vRocketOffset + (vDir*vOffset)
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].vTurretRayTrace[arrayIndex] = vEndPoint
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "MAINTAIN_TURRET_NETWORK_DATA: [MAINTAIN_TURRETS] - GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].vTurretRayTrace[", arrayIndex, "] ", GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].vTurretRayTrace[arrayIndex])
		
			IF NOT ARE_VECTORS_ALMOST_EQUAL(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].vTurretRayTrace[arrayIndex], vEndPoint) 
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].vTurretRayTrace[arrayIndex] = vEndPoint
			ENDIF
		ENDIF
		
	ENDIF
	
	// GUNRUN_TOTAL_MOC_TURRETS
	REPEAT 3 turretIndex
		IF arrayIndex != turretIndex
			IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bTurretInUse, turretIndex)
				CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bTurretInUse, turretIndex)
				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "MAINTAIN_TURRET_NETWORK_DATA: [MAINTAIN_TURRETS] - Un-setting turret ID ", g_iInteriorTurretSeat, " (index: ", turretIndex,") in use.")
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

FUNC VECTOR EULER_TO_DIRECTION_VECTOR(VECTOR vEuler)
	VECTOR vResult
	vResult.x = Cos(vEuler.x)
	vResult.y = Cos(vEuler.z)
	vResult.z = Sin(vEuler.x)
	vResult.y *= vResult.x
	vResult.x *= -Sin(vEuler.z)
	RETURN vResult
ENDFUNC

PROC MAINTAIN_TURRET_RESET()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX vehID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF DOES_ENTITY_EXIST(vehID)
		AND IS_VEHICLE_DRIVEABLE(vehID)
		AND IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(vehID)
		AND NETWORK_HAS_CONTROL_OF_ENTITY(vehID)
			SET_SHOULD_RESET_TURRET_IN_SCRIPTED_CAMERAS(vehID, FALSE)
		ENDIF
	ENDIF
ENDPROC
PROC MAINTAIN_CINEMATIC_CAM_BLOCKING()
	IF HAS_NET_TIMER_STARTED(stBombCamExitTimer)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
		g_bBombuskaExitCamTimerOn = TRUE
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "MAINTAIN_CINEMATIC_CAM_BLOCKING: blocking cin cam")
		IF HAS_NET_TIMER_EXPIRED(stBombCamExitTimer, 1000, TRUE)
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "MAINTAIN_CINEMATIC_CAM_BLOCKING: Reseting net timer, timer expired")
			RESET_NET_TIMER(stBombCamExitTimer)
			g_bBombuskaExitCamTimerOn = FALSE
		ENDIF 
	ENDIF
ENDPROC

PROC DISABLE_VEH_ATTACK_INPUT()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_UD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON) 
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)	
ENDPROC

PROC RUN_VEHICLE_FOCUS_LOGIC()
	IF DOES_INTERIOR_USE_TURRETS()
		IF (playerBD[PARTICIPANT_ID_TO_INT()].eHeliGunState = eHELIGUNSTATE_LAUNCHING
		OR playerBD[PARTICIPANT_ID_TO_INT()].eHeliGunState = eHELIGUNSTATE_ON
		OR g_iInteriorTurretSeat != -1)
		AND DOES_CAM_EXIST(sKevCamVars.camChopper)
		AND IS_CAM_RENDERING(sKevCamVars.camChopper)
			IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
				SET_FOCUS_POS_AND_VEL(GET_CAM_COORD(sKevCamVars.camChopper), GET_CAM_ROT(sKevCamVars.camChopper))
//				USE_SCRIPT_CAM_FOR_AMBIENT_POPULATION_ORIGIN_THIS_FRAME(TRUE, TRUE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC



DEBUGONLY FUNC STRING TURRET_CAM_CHANGE_DIR_TO_STRING(TURRET_CAM_CHANGE_DIR eDirection)
	SWITCH eDirection
		CASE FORWARDS 	RETURN "FORWARDS"
		CASE BACKWARDS 	RETURN "BACKWARDS"
	ENDSWITCH
	RETURN "INVALID DIR"
ENDFUNC





//FUNC INT NEXT_FREE_SEAT(TURRET_CAM_CHANGE_DIR eDirection)
//	INT iReturnSeat
//	IF eDirection = FORWARDS
//		iReturnSeat = (g_iInteriorTurretSeat+1) % AVENGER_TURRET_COUNT + 1
//		INT iSeatCalAttemps
//		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "NEXT_FREE_SEAT: FORWARDS: iReturnSeat = ", iReturnSeat )
//		WHILE NOT IS_TURRET_SEAT_AVAILABLE(iReturnSeat)
//		AND iSeatCalAttemps < 3
//			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "NEXT_FREE_SEAT: FORWARDS: iReturnSeat = ", iReturnSeat, ", Not available, recalculating")
//			iReturnSeat = (iReturnSeat+1) % AVENGER_TURRET_COUNT + 1
//			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "NEXT_FREE_SEAT: FORWARDS: Recaculated: iReturnSeat = ", iReturnSeat )
//			iSeatCalAttemps++
//		ENDWHILE
//		IF iSeatCalAttemps = 3
//			RETURN -1
//		ENDIF
//		RETURN iReturnSeat
//	ELSE	
//		iReturnSeat = PICK_INT(g_iInteriorTurretSeat = 1, AVENGER_TURRET_COUNT, g_iInteriorTurretSeat - 1)
//		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "NEXT_FREE_SEAT: FORWARDS: iReturnSeat = ", iReturnSeat )
//		INT iSeatCalAttemps
//		WHILE NOT IS_TURRET_SEAT_AVAILABLE(iReturnSeat)
//		AND iSeatCalAttemps < 3
//			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "NEXT_FREE_SEAT: FORWARDS: iReturnSeat = ", iReturnSeat, ", Not available, recalculating")
//			iReturnSeat = PICK_INT(iReturnSeat = 1, AVENGER_TURRET_COUNT, iReturnSeat - 1)
//			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "NEXT_FREE_SEAT: FORWARDS: Recaculated: iReturnSeat = ", iReturnSeat )
//		ENDWHILE
//		IF iSeatCalAttemps = 3
//			RETURN -1
//		ENDIF
//		RETURN iReturnSeat
//	ENDIF
//ENDFUNC


PROC CHANGE_SEAT_CAM(INT iSeatIndex)
	CDEBUG1LN(DEBUG_NET_GUN_TURRET, "CHANGE_SEAT_CAM : g_iInteriorTurretSeat (previous) = ", g_iInteriorTurretSeat, ", (new) = ", iSeatIndex)
	g_iInteriorTurretSeat = iSeatIndex
	sKevCamVars.bIsCamAttached = FALSE
	RESET_NET_TIMER(tHelpTimer)
ENDPROC


PROC HANDLE_AVENGER_TURRET_CAM_SWITCH()
	INT iNextSeat =-1
	IF g_iInteriorTurretSeat != -1
	AND (IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
	OR IS_PLAYER_IN_CREATOR_AIRCRAFT(PLAYER_ID()))
//	OR IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
		IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_LB)
			iNextSeat = NEXT_FREE_SEAT(FORWARDS)
			sKevCamVars.vAttachedCamRotationOffset = <<0,0,0>>
		ELIF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RB)
			iNextSeat = NEXT_FREE_SEAT(BACKWARDS)
			sKevCamVars.vAttachedCamRotationOffset = <<0,0,0>>
		ENDIF
	ENDIF
	
	IF IS_AVENGER_TURRET_CAM_AVAILABLE(iNextSeat)
		CHANGE_SEAT_CAM(iNextSeat)
	ENDIF
ENDPROC

PROC HANDLE_IAA_TURRET_CAM_SWITCH()
	INT iNextSeat =-1
	IF g_iInteriorTurretSeat != -1
	AND IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
		IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_LB)
			iNextSeat = NEXT_FREE_IAA_SEAT(BACKWARDS)
			sKevCamVars.vAttachedCamRotationOffset = <<0,0,0>>
		ELIF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RB)
			iNextSeat = NEXT_FREE_IAA_SEAT(FORWARDS)
			sKevCamVars.vAttachedCamRotationOffset = <<0,0,0>>
		ENDIF
	ENDIF
	
	IF IS_IAA_TURRET_CAM_AVAILABLE(iNextSeat)
		CHANGE_SEAT_CAM(iNextSeat)
	ENDIF
ENDPROC



BOOL bCamFilterChanged
PROC HANDLE_AVENGER_TURRET_CAM_MOD_SWITCH()
	IF GET_ENTITY_MODEL(viHeli) = AKULA
	AND (bIAmTurretPilot1 OR bIAmTurretPilot2)
		IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_LB)

			IF ENUM_TO_INT(camMod) = 2
				camMod = INT_TO_ENUM(CAM_MOD, 0)
			ELSE
				camMod = INT_TO_ENUM(CAM_MOD, ENUM_TO_INT(camMod)+1)
			ENDIF
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "camMod = ", GET_DEBUG_NAME_FOR_FILTER(camMod))
			bCamFilterChanged = FALSE
		ENDIF
		IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RB)

			IF ENUM_TO_INT(camMod) = 0		
				camMod = INT_TO_ENUM(CAM_MOD, 2)
			ELSE
				camMod = INT_TO_ENUM(CAM_MOD, ENUM_TO_INT(camMod)-1)

			ENDIF
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "camMod = ", GET_DEBUG_NAME_FOR_FILTER(camMod))
			bCamFilterChanged = FALSE
		ENDIF
	ENDIF
ENDPROC



PROC MAINTAIN_THERMAL_CAM_LOGIC()
	HANDLE_AVENGER_TURRET_CAM_MOD_SWITCH()
	IF GET_ENTITY_MODEL(viHeli) = AKULA
	AND (bIAmTurretPilot1 OR bIAmTurretPilot2)
		IF playerBD[PARTICIPANT_ID_TO_INT()].eHeliGunState = eHELIGUNSTATE_ON
			IF bCamFilterChanged = FALSE
				SWITCH camMod
					CASE CM_NORMAL
						//normal
						SET_NIGHTVISION(FALSE) 
						IF GET_USINGSEETHROUGH()
							SEETHROUGH_SET_MAX_THICKNESS(g_fSeethrough_cached_max_thickness)
							g_fSeethrough_cached_max_thickness = -1.0
							SET_SEETHROUGH(FALSE)
						ENDIF 
					BREAK
					CASE CM_NIGHT
						//night
						SET_NIGHTVISION(TRUE) 
						IF GET_USINGSEETHROUGH()
							SEETHROUGH_SET_MAX_THICKNESS(g_fSeethrough_cached_max_thickness)
							g_fSeethrough_cached_max_thickness = -1.0
							SET_SEETHROUGH(FALSE)
						ENDIF 
					BREAK
					CASE CM_THERMAL
						SET_NIGHTVISION(FALSE) 
						SET_SEETHROUGH (TRUE ) 
						g_fSeethrough_cached_max_thickness = SEETHROUGH_GET_MAX_THICKNESS() 
						SEETHROUGH_SET_MAX_THICKNESS(fCONST_SEETHROUGH_CACHED_DEFAULT_THICKNESS) 
					BREAK
				ENDSWITCH
				bCamFilterChanged = TRUE
			ENDIF
		ELSE
			IF camMod != CM_NORMAL
				SET_NIGHTVISION(FALSE) 
				IF GET_USINGSEETHROUGH()
					SEETHROUGH_SET_MAX_THICKNESS(g_fSeethrough_cached_max_thickness)
					g_fSeethrough_cached_max_thickness = -1.0
					SET_SEETHROUGH(FALSE)
				ENDIF 
				 camMod = CM_NORMAL
				 CDEBUG1LN(DEBUG_NET_GUN_TURRET, "MAINTAIN_THERMAL_CAM_LOGIC: reseting camera mod now that player camera isn't set to on.")
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

FUNC BOOL IS_SUITABLE_FOR_SNIPER_AUDIO()
	IF IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
	OR IS_PLAYER_IN_CREATOR_AIRCRAFT(PLAYER_ID())
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_DISABLED_CONTROLS_FOR_AKULA()
	IF GET_ENTITY_MODEL(viHeli) = AKULA
	OR GET_ENTITY_MODEL(viHeli) = PHANTOM3 //@@ MOC2
		IF (bIAmTurretPilot1
		OR bIAmTurretPilot2)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_LR)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_UD)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_AIM)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_ATTACK)
		ENDIF
	ENDIF
ENDPROC


PROC PROCESS_HELI_GUN()
	
	#IF IS_DEBUG_BUILD
        #IF SCRIPT_PROFILER_ACTIVE 
        OPEN_SCRIPT_PROFILE_MARKER_GROUP("PROCESS_HELI_GUN")
        #ENDIF
   	#ENDIF
	HANDLE_AVENGER_TURRET_CAM_SWITCH()
	HANDLE_IAA_TURRET_CAM_SWITCH()
	RUN_VEHICLE_FOCUS_LOGIC()
	MAINTAIN_DISABLED_CONTROLS_FOR_AKULA()
	
	IF IS_VEHICLE_DRIVEABLE(viHeli)
	AND IS_ENTITY_A_GHOST(viHeli)
	AND NOT (FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID()) = FMMC_TYPE_KILL_LIST)
	AND ( bIAmCoPilot OR bIAmTurretPilot1 OR bIAmTurretPilot2 OR g_iInteriorTurretSeat != -1 OR bPilotAndAllowed)
		DISABLE_VEH_ATTACK_INPUT()
		bWeaponsDisabledForPassive = TRUE
	ELSE
		bWeaponsDisabledForPassive = FALSE
	ENDIF	
	g_bUsingTurretHeliCam = FALSE
	MAINTAIN_TURRET_RESET()
	MAINTAIN_CINEMATIC_CAM_BLOCKING()
	
	IF IS_HELIHUD_ACTIVE(sKevCamVars)
		IF NOT bSetSniperAudioFlag
		AND IS_SUITABLE_FOR_SNIPER_AUDIO()
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ForceSniperAudio should be true")
			SET_AUDIO_FLAG("ForceSniperAudio", TRUE)
			bSetSniperAudioFlag = TRUE
		ENDIF
	ELSE
		IF bSetSniperAudioFlag
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ForceSniperAudio should be false")
			SET_AUDIO_FLAG("ForceSniperAudio", FALSE)
			bSetSniperAudioFlag = FALSE
		ENDIF
	ENDIF
	
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iRuleOptionsBitSet,ciALLOW_HELI_CAM)
		
		IF (PlayerIdPilot = PLAYER_ID())
			IF NOT bDisabledRockets
				DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_VEHICLE_SPACE_ROCKET, viHeli, PLAYER_PED_ID())
				bDisabledRockets = TRUE
				CDEBUG1LN(DEBUG_NET_GUN_TURRET, " - disabled rockets.")
			ENDIF
		ENDIF
		
	ELSE

		IF IS_CO_PILOT_CAM_ON()
			IF (PlayerIdPilot = PLAYER_ID())
				IF NOT bDisabledRockets
				AND eHeliModel != HUNTER
					DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_VEHICLE_SPACE_ROCKET, viHeli, PLAYER_PED_ID())
					bDisabledRockets = TRUE
					CDEBUG1LN(DEBUG_NET_GUN_TURRET, " - disabled rockets.")
				ENDIF
			ENDIF
		ELSE
			IF (PlayerIdPilot = PLAYER_ID())
				IF bDisabledRockets
					DISABLE_VEHICLE_WEAPON(FALSE, WEAPONTYPE_VEHICLE_SPACE_ROCKET, viHeli, PLAYER_PED_ID())
					bDisabledRockets = FALSE
					CDEBUG1LN(DEBUG_NET_GUN_TURRET, " - enabled rockets.")
				ENDIF
				 
			ENDIF
		ENDIF
		
	ENDIF
	
	LIMIT_CAM_PITCH_WHILE_GROUNDED()
	
	HANDLE_ROCKETS_DISABLED_HELP_TEXT()
	MAINTAIN_THERMAL_CAM_LOGIC()
//	CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_HELI_GUN: eHeliGunState: ", GET_HELI_GUN_STATE_NAME(playerBD[PARTICIPANT_ID_TO_INT()].eHeliGunState))

	VECTOR vPos
	FLOAT newFov
	
	SWITCH playerBD[PARTICIPANT_ID_TO_INT()].eHeliGunState
	
		CASE eHELIGUNSTATE_OFF
			CLEANUP_PC_CONTROLS()
//			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "eHELIGUNSTATE_OFF")
			MAINTAIN_TURN_ON_HELI_GUN_HELP()
			MAINTAIN_VEHICLE_SEAT_SHUFFLE()
			//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bInHeliGunCam = FALSE
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bInHeliGunCam	)
			g_bUsingTurretHeliCam = FALSE
			IF SHOULD_CAM_TURN_ON()
				SET_HELI_GUN_STATE(eHELIGUNSTATE_LAUNCHING)
			ELIF bIAmCoPilot
			OR bIAmTurretPilot1
			OR bIAmTurretPilot2
			OR bPilotAndAllowed
				IF DOES_VEHICLE_USE_VEHICLE_WEAPONS_CODE(viHeli)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_LR)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_UD)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_AIM)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_ATTACK)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON) 
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
				ENDIF
			ENDIF
			
				RESET_NET_TIMER(tHelpTimer)
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER( "Heli gun - eHELIGUNSTATE_OFF")
			#ENDIF
			#ENDIF	
		BREAK
		
		CASE eHELIGUNSTATE_LAUNCHING
			//Clear non-turret-cam help.
			CLEAR_TURN_ON_HELI_HELP()
			HIDE_HELI_TURRETS_LOCALLY(TRUE)
			
			IF g_iInteriorTurretSeat != -1
				RESET_NET_TIMER(tHelpTimer)
				START_NET_TIMER(tHelpTimer)
			ENDIF
			
			IF g_iShouldLaunchTruckTurret != -1
			OR g_iInteriorTurretSeat != -1
				
				DO_SCREEN_FADE_OUT(250)
			
				mov = REQUEST_SCALEFORM_MOVIE(GET_SCALEFORM_MOVIE_TURRET_HUD())
				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_HELI_GUN: Using turret specific UI: turret_cam.")
			
				IF sKevCamVars.bLimitCamWhileGrounded
					sKevCamVars.bLimitCamWhileGrounded = FALSE
				ENDIF
				
				IF IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
				OR IS_PLAYER_IN_CREATOR_TRAILER(PLAYER_ID())
					//B* 3643339
					IF GET_MAX_WANTED_LEVEL() > 0
						SET_WANTED_LEVEL_MULTIPLIER(0.65)
						bSetWantedMultiplierForTruckTurret = TRUE
						CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_HELI_GUN: SET_WANTED_LEVEL_MULTIPLIER(0.65)")
					ENDIF
				ENDIF
				
				IF IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
				OR IS_PLAYER_IN_CREATOR_AIRCRAFT(PLAYER_ID())
					//B* 4080957
					IF GET_MAX_WANTED_LEVEL() > 0
						SET_WANTED_LEVEL_MULTIPLIER(g_sMPTunables.FH2_AVENGER_WANTED_LEVEL_MULTIPLIER_ON_TURRETS)
						bSetWantedMultiplierForTruckTurret = TRUE
						CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_HELI_GUN: SET_WANTED_LEVEL_MULTIPLIER = ", g_sMPTunables.FH2_AVENGER_WANTED_LEVEL_MULTIPLIER_ON_TURRETS)
					ENDIF
				ENDIF
				
			ELIF IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(viHeli)
			
				mov = REQUEST_SCALEFORM_MOVIE(GET_SCALEFORM_MOVIE_TURRET_HUD())
//				IF NETWORK_HAS_CONTROL_OF_ENTITY(viHeli)
//					SET_SHOULD_RESET_TURRET_IN_SCRIPTED_CAMERAS(viHeli, FALSE)
//				ENDIF
			ELSE
			
			mov = REQUEST_SCALEFORM_MOVIE(GET_SCALEFORM_MOVIE_TURRET_HUD())
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_HELI_GUN: Using turret specific UI: heli_cam.")
			
			ENDIF
			
			REQUEST_STREAMED_TEXTURE_DICT("helicopterhud")
			DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
//			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PreventAutoShuffleToDriversSeat, TRUE)
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "CHOPPER_INIT(sKevCamVars): ", CHOPPER_INIT(sKevCamVars), ", HAS_SCALEFORM_MOVIE_LOADED(mov): ", HAS_SCALEFORM_MOVIE_LOADED(mov), ", HAS_STREAMED_TEXTURE_DICT_LOADED(helicopterhud): ", HAS_STREAMED_TEXTURE_DICT_LOADED("helicopterhud"))
			IF CHOPPER_INIT(sKevCamVars)
			AND HAS_SCALEFORM_MOVIE_LOADED(mov)
			AND HAS_STREAMED_TEXTURE_DICT_LOADED("helicopterhud")
				
				
				IF g_b_EnableFocusDisconnectHeliCam
					IF IS_NEW_LOAD_SCENE_ACTIVE() = FALSE
					OR IS_NEW_LOAD_SCENE_LOADED() 
						NEW_LOAD_SCENE_STOP()
						CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[HELICAMSTREAM] PROCESS_HELI_GUN: NEW_LOAD_SCENE_STOP ")
					ENDIF
					IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
						IF IS_ENTITY_FOCUS(PLAYER_PED_ID())
							CLEAR_FOCUS()
							
							CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[HELICAMSTREAM] PROCESS_HELI_GUN: CLEAR_FOCUS from player ")
						ENDIF
					ENDIF
				ENDIF
				
				
				sKevCamVars.camEntity = CONVERT_TO_ENTITY_INDEX(viHeli)
				sKevCamVars.isMultiplayerMode = TRUE
				//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bInHeliGunCam = TRUE
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bInHeliGunCam	)
				SET_CHOPPER_HUD_ACTIVE(sKevCamVars, viHeli, TRUE, viHeli, TRUE)
				g_heligunCamActive = TRUE
				ORIENTATE_CAM_TO_FACE_SAME_DIRECTION_AS_CHOPPER()	
				SET_ENTITY_ALWAYS_PRERENDER(viHeli, TRUE)
				CDEBUG1LN(DEBUG_NET_GUN_TURRET, " - called SET_ENTITY_ALWAYS_PRERENDER on heli.")
				CDEBUG1LN(DEBUG_NET_GUN_TURRET, " - initialised cam vars.")
				SET_HELI_GUN_STATE(eHELIGUNSTATE_ON)
				
			
				IF GET_ENTITY_MODEL(viHeli) = VOLATOL
					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "SET_PARTICLE_FX_OVERRIDE(muz_xm_volatol_twinmg, scr_xm_volatol_turret_camera)")
					SET_PARTICLE_FX_OVERRIDE("muz_xm_volatol_twinmg", "scr_xm_volatol_turret_camera")
				ENDIF
				
				//trying to prevent single frame jump when pilot switches to cam
				IF NOT bIAmCoPilot
				AND NOT bIAmTurretPilot1
				AND NOT bIAmTurretPilot2
				AND NOT bPilotAndAllowed
				AND g_iInteriorTurretSeat = -1
					g_heligunWatchingOnly = TRUE
					sKevCamVars.mpCamFudge=true
					PROCESS_SYNCHING_PILOT_CAMERA_WITH_COPILOT_CAMERA()
				ELSE
					g_heligunWatchingOnly = FALSE
				ENDIF
				
				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "sKevCamVars.mpCamFudge = ", sKevCamVars.mpCamFudge)
										
				IF NOT (g_iShouldLaunchTruckTurret != -1
				OR g_iInteriorTurretSeat != -1)
					MAINTAIN_HELI_VISIBILITY()
				ENDIF
				
			ENDIF
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER( "Heli gun - eHELIGUNSTATE_LAUNCHING")
			#ENDIF
			#ENDIF	
			
			INIT_TURRET_AUDIO()
		BREAK
		
		CASE eHELIGUNSTATE_ON
			IF g_iShouldLaunchTruckTurret != -1
			OR g_iInteriorTurretSeat != -1
			OR IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(viHeli)
				SETUP_PC_CONTROLS()
			ENDIF
			
			//INIT_PLAYER_SCANNER(m_playerScanner, sKevCamVars.camChopper)
			//MAINTAIN_PLAYER_SCANNER(m_playerScanner)	
			IF GET_FRAME_COUNT() % 30 = 0 
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].vTurretCamPosition = GET_FINAL_RENDERED_CAM_COORD()
			ENDIF	
			eRocketModel = INT_TO_ENUM(MODEL_NAMES, HASH("w_lr_rpg_rocket"))
			
			IF ENUM_TO_INT(eRocketModel) != 0
				REQUEST_MODEL(eRocketModel)
//				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_HELI_GUN - Requesting heli model: ", GET_MODEL_NAME_FOR_DEBUG(eRocketModel))
			ELSE
//				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_HELI_GUN - Heli model: ", GET_MODEL_NAME_FOR_DEBUG(eRocketModel), " loaded.")
			ENDIF
			
			IF g_iShouldLaunchTruckTurret != -1
			OR IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(viHeli)
			OR g_iInteriorTurretSeat != -1
				ACTIVATE_INTERIOR_GROUPS_USING_CAMERA()
			
				SET_HUD_TO_SHOW_FOR_HELI(sKevCamVars, FALSE)
			
				// Do this the frame after we've set the camera rotation on purpose.
				IF sKevCamVars.bCamRotSet AND sKevCamVars.bDelayRenderScriptCams 
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					sKevCamVars.bDelayRenderScriptCams = FALSE
					SET_TIMECYCLE_MODIFIER("eyeinthesky")
					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_HELI_GUN: Started rendering script cameras & timecycle modifiers.")
					
					IF DOES_INTERIOR_USE_TURRETS()
						START_NET_TIMER(sFadeDelayTimer)
					ENDIF
					
					EXIT
				ENDIF
			
				IF NOT sKevCamVars.bCamRotSet
					sKevCamVars.vCamRot = GET_CAM_ROT(sKevCamVars.camChopper)
					sKevCamVars.bCamRotSet = TRUE
					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_HELI_GUN: Set initial camera rotation to: ", sKevCamVars.vCamRot)
					EXIT
				ENDIF
				
				IF sKevCamVars.bDelayRenderScriptCams
					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_HELI_GUN: EXIT - Waiting for script camera delay.")
					EXIT
				ELSE
					IF NOT g_bEndScreenSuppressFadeIn
					OR IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
						IF IS_SCREEN_FADED_OUT()
							
							IF NOT DOES_INTERIOR_USE_TURRETS()
								CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_HELI_GUN: Fade in camera.")
								DO_SCREEN_FADE_IN(250)	
							ELSE
								IF IS_ENTITY_ALIVE(viHeli)
									IF HAS_NET_TIMER_STARTED(sFadeDelayTimer)
									AND HAS_NET_TIMER_EXPIRED(sFadeDelayTimer, 3000)
										RESET_NET_TIMER(sFadeDelayTimer)
										
										DO_SCREEN_FADE_IN(250)
										CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_HELI_GUN: entity in focus: Fade in camera.")
									ELSE
										CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_HELI_GUN: entity not faded in")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PROCESS_HELI_GUN: Not fading in camera - g_bEndScreenSuppressFadeIn")
					ENDIF
				ENDIF
			ENDIF
		
	
			IF IS_PAUSE_MENU_ACTIVE_EX()	
				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "eHELIGUNSTATE_ON: Pause menu active: SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(mov)")
				SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(mov)
			ELSE
				IF NOT HAS_SCALEFORM_MOVIE_LOADED(mov)
					
					mov = REQUEST_SCALEFORM_MOVIE(GET_SCALEFORM_MOVIE_TURRET_HUD())
					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "eHELIGUNSTATE_ON: Pause menu not active: REQUEST_SCALEFORM_MOVIE()")
				ENDIF
			ENDIF
	
//			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PreventAutoShuffleToDriversSeat, TRUE)
			DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			SET_HUD_TO_SHOW_FOR_HELI(sKevCamVars, TRUE)
			
			
			sKevCamVars.isMultiplayerMode = TRUE
			
			IF GET_ENTITY_MODEL(viHeli) = AVENGER

				IF DOES_CAM_EXIST(sKevCamVars.camChopper)
					
					newFov = sKevCamVars.fFOV
					newFov += 5
					
					SET_CAM_FOV(sKevCamVars.camChopper, newFov)
					
				ENDIF		
				SUPPRESS_HD_MAP_STREAMING_THIS_FRAME()			
			ENDIF

			IF VEH_MODEL_CAN_SCRIPT_SHOOT_FROM_VEH_CAM(viHeli)
			OR DOES_INTERIOR_USE_SCRIPTED_WEAPONS()
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_PASSENGER_AIM)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_PASSENGER_ATTACK)
//				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "eHELIGUNSTATE_ON: disabling INPUT_VEH_PASSENGER_ATTACK becuase vehicle should use script turret command")
			ELSE
//				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "eHELIGUNSTATE_ON: not disabling INPUT_VEH_PASSENGER_ATTACK becuase vehicle should use code turret command")
			ENDIF
			
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_PASSENGER_AIM)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_AIM)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_ATTACK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_CIN_CAM)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SPECIAL)
//			IF eHeliModel != HUNTER
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
//			ENDIF
			DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_LR)
			DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_UD)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
			IF bIAmCoPilot
			OR bIAmTurretPilot1
			OR bIAmTurretPilot2
			OR bPilotAndAllowed
				DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_VEH_RADIO_WHEEL)
			ENDIF
			
			MAINTAIN_HELI_GUN_OPERATIONAL_INSTRUCTIONS_HELP()
			CONTROL_CHOPPER_AUDIO_LEVEL()
			
			MAINTAIN_HELI_VISIBILITY()
			
			#IF IS_DEBUG_BUILD
		          #IF SCRIPT_PROFILER_ACTIVE 
		          OPEN_SCRIPT_PROFILE_MARKER_GROUP("heli gun stuff")
		          #ENDIF
		   	#ENDIF
			
			IF g_iShouldLaunchTruckTurret != -1
			
			
				IF !g_bActiveInGunTurret
					g_bActiveInGunTurret = TRUE
					PRINTLN("[MAINTAIN_TURRETS] - g_bActiveInGunTurret = TRUE")
				ENDIF
				// This means virtually nothing, it was just more dangerous to remove than to bypass.
				// It can be set to anything as long as it's not 0.
				sKevCamVars.fMaxCameraYaw = 50.0	
				IF g_iShouldLaunchTruckTurret = 1
					// Forward turret has a more restrictive traverse than the rear turrets.
					RUN_HELICOPTER_HUD(sKevCamVars, FALSE, 30, -12, -50, 50) 
				ELIF g_iShouldLaunchTruckTurret = 2
					// Side turrets have variable traverse depending on the side.
					RUN_HELICOPTER_HUD(sKevCamVars, FALSE, 30, -55, -85, 140) 
				ELIF g_iShouldLaunchTruckTurret = 3
					// Side turrets have variable traverse depending on the side.
					RUN_HELICOPTER_HUD(sKevCamVars, FALSE, 30, -55, -140, 85) 
				ELSE
					RUN_HELICOPTER_HUD(sKevCamVars)
				ENDIF		
				//vPos = GET_PLAYER_PERCEIVED_COORDS(MPGlobals.LocalPlayerID)
				vPos = GET_FINAL_RENDERED_CAM_COORD()
				IF IS_PAUSE_MENU_ACTIVE()
				OR IS_PAUSE_MENU_ACTIVE_EX()
				OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
				OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
				OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
					SET_FAKE_PAUSEMAP_PLAYER_POSITION_THIS_FRAME(vPos.x,vPos.y)
					PRINTLN("[MAINTAIN_TURRETS] - SET_FAKE_PAUSEMAP_PLAYER_POSITION_THIS_FRAME(", vPos.x, ",", vPos.y, ")")	
					IF IS_PAUSE_MENU_ACTIVE()
					OR IS_PAUSE_MENU_ACTIVE_EX()
						IF DOES_BLIP_EXIST(GET_MAIN_PLAYER_BLIP_ID())
							SET_BLIP_ALPHA(GET_MAIN_PLAYER_BLIP_ID(), 255)
						ENDIF
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(GET_MAIN_PLAYER_BLIP_ID())
						SET_BLIP_ALPHA(GET_MAIN_PLAYER_BLIP_ID(), 0)
					ENDIF
				ENDIF
				LOCK_MINIMAP_POSITION(vPos.x,vPos.y)
				SET_RADAR_AS_EXTERIOR_THIS_FRAME()
				HIDE_MINIMAP_INTERIOR_MAP_THIS_FRAME()
				SET_RADAR_ZOOM(0)
				//PRINTLN("[MAINTAIN_TURRETS] - BLIP")
				VECTOR vRot			
				vRot = GET_FINAL_RENDERED_CAM_ROT(EULER_XYZ)
				vRot = EULER_TO_DIRECTION_VECTOR(vRot)
				IF NOT DOES_BLIP_EXIST(ArrowStrokeBlipID)
					ArrowStrokeBlipBg = CREATE_BLIP_FOR_COORD(vPos)
					SET_BLIP_SPRITE(ArrowStrokeBlipBg, RADAR_TRACE_CENTRE_STROKE)
					SHOW_HEIGHT_ON_BLIP(ArrowStrokeBlipBg, FALSE)	
					SET_BLIP_DISPLAY(ArrowStrokeBlipBg, DISPLAY_BOTH)
					SET_BLIP_COLOUR(ArrowStrokeBlipBg,GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_BLACK))	
					SET_BLIP_SHOW_CONE(ArrowStrokeBlipBg, TRUE)
					SET_BLIP_SCALE(ArrowStrokeBlipBg, 0.54)	
					SET_BLIP_PRIORITY(ArrowStrokeBlipBg, INT_TO_ENUM(BLIP_PRIORITY, (ENUM_TO_INT(BLIPPRIORITY_OVER_CENTRE_BLIP)+1)))
					ArrowStrokeBlipID = CREATE_BLIP_FOR_COORD(vPos)
					SET_BLIP_SPRITE(ArrowStrokeBlipID, RADAR_TRACE_CENTRE_STROKE)
					SHOW_HEIGHT_ON_BLIP(ArrowStrokeBlipID, FALSE)	
					SET_BLIP_DISPLAY(ArrowStrokeBlipID, DISPLAY_BOTH)
					SET_BLIP_COLOUR(ArrowStrokeBlipID,GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_GREEN))
					SET_BLIP_SHOW_CONE(ArrowStrokeBlipID, TRUE)
					SET_BLIP_SCALE(ArrowStrokeBlipID, 0.44)	
					SET_BLIP_PRIORITY(ArrowStrokeBlipID, INT_TO_ENUM(BLIP_PRIORITY, (ENUM_TO_INT(BLIPPRIORITY_OVER_CENTRE_BLIP)+1)))
					SET_BLIP_HIDDEN_ON_LEGEND(ArrowStrokeBlipID, TRUE)
					SET_BLIP_ROTATION(ArrowStrokeBlipID, ROUND(GET_HEADING_FROM_VECTOR_2D (vRot.x, vRot.y)))
					
					SET_BLIP_HIDDEN_ON_LEGEND(ArrowStrokeBlipBg, TRUE)
					SET_BLIP_ROTATION(ArrowStrokeBlipBg, ROUND(GET_HEADING_FROM_VECTOR_2D (vRot.x, vRot.y)))
				ELSE
					SET_BLIP_COORDS(ArrowStrokeBlipID, vPos)
					SET_BLIP_ROTATION(ArrowStrokeBlipID, ROUND(GET_HEADING_FROM_VECTOR_2D (vRot.x, vRot.y)))
					SET_BLIP_DISPLAY(ArrowStrokeBlipID, DISPLAY_RADAR_ONLY)
					SET_BLIP_COORDS(ArrowStrokeBlipBg, vPos)
					SET_BLIP_ROTATION(ArrowStrokeBlipBg, ROUND(GET_HEADING_FROM_VECTOR_2D (vRot.x, vRot.y)))
					SET_BLIP_DISPLAY(ArrowStrokeBlipBg, DISPLAY_RADAR_ONLY)
				ENDIF
			ELIF IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(viHeli)
				sKevCamVars.chopperPanFactor = fBombushkaTurretPanFactor
				IF GET_ENTITY_MODEL(viHeli) = AKULA
					SWITCH GET_VEHICLE_PLAYER_SEAT()
						CASE 0
							// Nose	
							sKevCamVars.fMaxCameraYaw = 180.0
							RUN_HELICOPTER_HUD(sKevCamVars, FALSE, cfAKULA_T0_PITCH_MIN, cfAKULA_T0_PITCH_MAX, -100.0, 100.0, fDeadzoneMag) 
						BREAK
							CASE 1
							// Passenger left
							sKevCamVars.fMaxCameraYaw = cfAKULA_T1_YAW_ARC
							RUN_HELICOPTER_HUD(sKevCamVars, FALSE, cfAKULA_T1_PITCH_MIN, cfAKULA_T1_PITCH_MAX, cfAKULA_T1_YAW_MIN, cfAKULA_T1_YAW_MAX, fDeadzoneMag) 
						BREAK
						CASE 2
							sKevCamVars.fMaxCameraYaw = cfAKULA_T1_YAW_ARC
							RUN_HELICOPTER_HUD(sKevCamVars, FALSE, cfAKULA_T1_PITCH_MIN, cfAKULA_T1_PITCH_MAX, cfAKULA_T1_YAW_MIN, cfAKULA_T1_YAW_MAX, fDeadzoneMag) 
						BREAK
						DEFAULT
							// Top
							sKevCamVars.fMaxCameraYaw = 360.0
							RUN_HELICOPTER_HUD(sKevCamVars, FALSE, 54, -12, -50, 50, fDeadzoneMag) 
						BREAK
					ENDSWITCH
				ELIF GET_ENTITY_MODEL(viHeli) = VOLATOL
					//Dynamically alter FOV as volatol reaches the ground
					FLOAT fGrounZ
					VECTOR vPlaneCoords
					FLOAT fZDiff	
					FLOAT fCurFOVAlpha
					IF NOT GET_GROUND_Z_FOR_3D_COORD(GET_ENTITY_COORDS(viHeli), fGrounZ, TRUE)
						//If failed to grab ground dist use max height -> fov will be normal
						fGrounZ = -1000
					#IF IS_DEBUG_BUILD
						m_bDebugCantGetGround = TRUE
					ELSE	
						m_bDebugCantGetGround = FALSE
					#ENDIF
					ENDIF			
					vPlaneCoords = GET_ENTITY_COORDS(viHeli)
					fZDiff = vPlaneCoords.z - fGrounZ
					//Make a note of relative FOV
					fCurFOVAlpha = CLAMP((sKevCamVars.fFOV - sKevCamVars.minFOV) / (sKevCamVars.maxFOV  - sKevCamVars.minFOV), 0, 1) 
					//Adjust min FOV
					sKevCamVars.minFOV = LERP_FLOAT(cfVOLATOL_TURRET_STATIC_FOV_MIN, cfVOLATOL_TURRET_DYNAMIC_FOV_MIN, //...
													1.0 - CLAMP((fZDiff - cfVOLATOL_VEH_HEIGHT)/ cfVOLATOL_TURRET_DYNAMIC_FOV_HEIGHT, 0, 1))
					//Adjust current FOV based on new min/max to
					sKevCamVars.fFOV = LERP_FLOAT(sKevCamVars.minFOV, sKevCamVars.maxFOV, fCurFOVAlpha)
					
					#IF IS_DEBUG_BUILD
					m_fDebugVehDistFromGround = fZDiff - cfVOLATOL_VEH_HEIGHT
					#ENDIF
					SWITCH GET_VEHICLE_PLAYER_SEAT()
						CASE 1
							// Nose	
							sKevCamVars.fMaxCameraYaw = 360.0
							RUN_HELICOPTER_HUD(sKevCamVars, FALSE, cfVOLATOL_T1_PITCH_MIN, cfVOLATOL_T1_PITCH_MAX, -100.0, 100.0, fDeadzoneMag)
						BREAK
						CASE 2
							// Top
							sKevCamVars.fMaxCameraYaw =  360.0
							RUN_HELICOPTER_HUD(sKevCamVars, FALSE, cfVOLATOL_T2_PITCH_MIN, cfVOLATOL_T2_PITCH_MAX, -100.0, 100.0, fDeadzoneMag) 
						BREAK
						DEFAULT
							// Top
							sKevCamVars.fMaxCameraYaw = 360.0
							RUN_HELICOPTER_HUD(sKevCamVars, FALSE, 54, -12, -50, 50, fDeadzoneMag) 
						BREAK
					ENDSWITCH
				ELSE
					SWITCH GET_VEHICLE_PLAYER_SEAT()
						CASE 1
							// Nose	
							sKevCamVars.fMaxCameraYaw = 360.0
							RUN_HELICOPTER_HUD(sKevCamVars, FALSE, 13, -80, -50, 50, fDeadzoneMag) 
						BREAK
						CASE 0
							// Top
							sKevCamVars.fMaxCameraYaw = 360.0
							RUN_HELICOPTER_HUD(sKevCamVars, FALSE, 54, -12, -50, 50, fDeadzoneMag) 
						BREAK
						CASE 2
							// REAR
							sKevCamVars.fMaxCameraYaw = 70.0
							RUN_HELICOPTER_HUD(sKevCamVars, FALSE, 48.0, -45.0, -100.0, 100.0, fDeadzoneMag) 
						BREAK
						DEFAULT
							// Top
							sKevCamVars.fMaxCameraYaw = 360.0
							RUN_HELICOPTER_HUD(sKevCamVars, FALSE, 54, -12, -50, 50, fDeadzoneMag) 
						BREAK
					ENDSWITCH
				ENDIF
			ELIF g_iInteriorTurretSeat != -1
				sKevCamVars.chopperPanFactor = fBombushkaTurretPanFactor
				
				IF NOT IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
					IF !g_bActiveInGunTurret
						g_bActiveInGunTurret = TRUE
						PRINTLN("[MAINTAIN_TURRETS] - g_bActiveInGunTurret = TRUE")
					ENDIF
				ENDIF
				
				IF  IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
				OR IS_PLAYER_IN_CREATOR_AIRCRAFT(PLAYER_ID()) 
					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "here: g_iInteriorTurretSeat = ", g_iInteriorTurretSeat)
					SWITCH g_iInteriorTurretSeat
					
						CASE 1
							// Nose	
							sKevCamVars.fMaxCameraYaw = 360.0
							RUN_HELICOPTER_HUD(sKevCamVars, FALSE, cfAVENGER_T0_PITCH_MIN, cfAVENGER_T0_PITCH_MAX, -100.0, 100.0, fDeadzoneMag)
						BREAK
						CASE 2
							// Top
							sKevCamVars.fMaxCameraYaw =  360.0
							RUN_HELICOPTER_HUD(sKevCamVars, FALSE, cfAVENGER_T1_PITCH_MIN, cfAVENGER_T1_PITCH_MAX, -100.0, 100.0, fDeadzoneMag) 
						BREAK
						CASE 3
							// Rear
							sKevCamVars.fMaxCameraYaw =  360.0
							RUN_HELICOPTER_HUD(sKevCamVars, FALSE, cfAVENGER_T2_PITCH_MIN, cfAVENGER_T2_PITCH_MAX, -100.0, 100.0, fDeadzoneMag) 
						BREAK
						DEFAULT
							// Top
							sKevCamVars.fMaxCameraYaw = 360.0
							RUN_HELICOPTER_HUD(sKevCamVars, FALSE, 54, -80, -50, 50, fDeadzoneMag) 
						BREAK
					ENDSWITCH
				ELIF NOT IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
				AND NOT IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
					SWITCH g_iInteriorTurretSeat
						CASE 1
							// Nose	
							sKevCamVars.fMaxCameraYaw = 360.0
							RUN_HELICOPTER_HUD(sKevCamVars, FALSE, 13, -80, -50, 50, fDeadzoneMag) 
						BREAK
						CASE 0
							// Top
							sKevCamVars.fMaxCameraYaw = 360.0
							RUN_HELICOPTER_HUD(sKevCamVars, FALSE, 54, -80, -50, 50, fDeadzoneMag) 
						BREAK
						CASE 2
							// REAR
							sKevCamVars.fMaxCameraYaw = 70.0
							RUN_HELICOPTER_HUD(sKevCamVars, FALSE, 48.0, -80, -100.0, 100.0, fDeadzoneMag) 
						BREAK
						DEFAULT
							// Top
							sKevCamVars.fMaxCameraYaw = 360.0
							RUN_HELICOPTER_HUD(sKevCamVars, FALSE, 54, -80, -50, 50, fDeadzoneMag) 
						BREAK
					ENDSWITCH
				ELIF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
					SWITCH g_iInteriorTurretSeat
						CASE 1
							// Nose	
							sKevCamVars.fMaxCameraYaw = 360.0
							RUN_HELICOPTER_HUD(sKevCamVars, FALSE, 88, -80, -100, 100, fDeadzoneMag) 
						BREAK
						CASE 0
							// Top
							sKevCamVars.fMaxCameraYaw = 360.0
							RUN_HELICOPTER_HUD(sKevCamVars, FALSE, 88, -80, -100, 100, fDeadzoneMag) 
						BREAK
						CASE 2
							// REAR
							sKevCamVars.fMaxCameraYaw = 360.0
							RUN_HELICOPTER_HUD(sKevCamVars, FALSE, 88, -80, -100, 100, fDeadzoneMag) 
						BREAK
						DEFAULT
							// Top
							sKevCamVars.fMaxCameraYaw = 360.0
							RUN_HELICOPTER_HUD(sKevCamVars, FALSE, 88, -80, -100, 100, fDeadzoneMag) 
						BREAK
					ENDSWITCH
				ELSE
					SWITCH g_iInteriorTurretSeat
						CASE 1
							// Nose	
							sKevCamVars.fMaxCameraYaw = 360.0
							RUN_HELICOPTER_HUD(sKevCamVars, FALSE, 13, -80, -50, 50, fDeadzoneMag) 
						BREAK
						CASE 0
							// Top
							sKevCamVars.fMaxCameraYaw = 360.0
							RUN_HELICOPTER_HUD(sKevCamVars, FALSE, 13, -80, -50, 50, fDeadzoneMag) 
						BREAK
						CASE 2
							// REAR
							sKevCamVars.fMaxCameraYaw = 360.0
							RUN_HELICOPTER_HUD(sKevCamVars, FALSE, 13, -80, -50, 50, fDeadzoneMag) 
						BREAK
						DEFAULT
							// Top
							sKevCamVars.fMaxCameraYaw = 360.0
							RUN_HELICOPTER_HUD(sKevCamVars, FALSE, 13, -80, -50, 50, fDeadzoneMag) 
						BREAK
					ENDSWITCH
				ENDIF
				
				vPos = GET_FINAL_RENDERED_CAM_COORD()
				IF IS_PAUSE_MENU_ACTIVE()
				OR IS_PAUSE_MENU_ACTIVE_EX()
				OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
				OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
				OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
					SET_FAKE_PAUSEMAP_PLAYER_POSITION_THIS_FRAME(vPos.x,vPos.y)
					PRINTLN("[MAINTAIN_TURRETS] - SET_FAKE_PAUSEMAP_PLAYER_POSITION_THIS_FRAME(", vPos.x, ",", vPos.y, ")")	
					IF IS_PAUSE_MENU_ACTIVE()
					OR IS_PAUSE_MENU_ACTIVE_EX()
						IF DOES_BLIP_EXIST(GET_MAIN_PLAYER_BLIP_ID())
							SET_BLIP_ALPHA(GET_MAIN_PLAYER_BLIP_ID(), 255)
						ENDIF
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(GET_MAIN_PLAYER_BLIP_ID())
						SET_BLIP_ALPHA(GET_MAIN_PLAYER_BLIP_ID(), 0)
					ENDIF
				ENDIF
				LOCK_MINIMAP_POSITION(vPos.x,vPos.y)
//				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "mini map position, vPos = ", vPos)
				SET_RADAR_AS_EXTERIOR_THIS_FRAME()
				HIDE_MINIMAP_INTERIOR_MAP_THIS_FRAME()
				SET_RADAR_ZOOM(0)
				VECTOR vRot			
				vRot = GET_FINAL_RENDERED_CAM_ROT(EULER_XYZ)
				vRot = EULER_TO_DIRECTION_VECTOR(vRot)
				IF NOT DOES_BLIP_EXIST(ArrowStrokeBlipID)
					ArrowStrokeBlipBg = CREATE_BLIP_FOR_COORD(vPos)
					SET_BLIP_SPRITE(ArrowStrokeBlipBg, RADAR_TRACE_CENTRE_STROKE)
					SHOW_HEIGHT_ON_BLIP(ArrowStrokeBlipBg, FALSE)	
					SET_BLIP_DISPLAY(ArrowStrokeBlipBg, DISPLAY_BOTH)
					SET_BLIP_COLOUR(ArrowStrokeBlipBg,GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_BLACK))	
					SET_BLIP_SHOW_CONE(ArrowStrokeBlipBg, TRUE)
					SET_BLIP_SCALE(ArrowStrokeBlipBg, 0.54)	
					SET_BLIP_PRIORITY(ArrowStrokeBlipBg, INT_TO_ENUM(BLIP_PRIORITY, (ENUM_TO_INT(BLIPPRIORITY_OVER_CENTRE_BLIP)+1)))
					ArrowStrokeBlipID = CREATE_BLIP_FOR_COORD(vPos)
					SET_BLIP_SPRITE(ArrowStrokeBlipID, RADAR_TRACE_CENTRE_STROKE)
					SHOW_HEIGHT_ON_BLIP(ArrowStrokeBlipID, FALSE)	
					SET_BLIP_DISPLAY(ArrowStrokeBlipID, DISPLAY_BOTH)
					SET_BLIP_COLOUR(ArrowStrokeBlipID,GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_GREEN))
					SET_BLIP_SHOW_CONE(ArrowStrokeBlipID, TRUE)
					SET_BLIP_SCALE(ArrowStrokeBlipID, 0.44)	
					SET_BLIP_PRIORITY(ArrowStrokeBlipID, INT_TO_ENUM(BLIP_PRIORITY, (ENUM_TO_INT(BLIPPRIORITY_OVER_CENTRE_BLIP)+1)))
					SET_BLIP_HIDDEN_ON_LEGEND(ArrowStrokeBlipID, TRUE)
					SET_BLIP_ROTATION(ArrowStrokeBlipID, ROUND(GET_HEADING_FROM_VECTOR_2D (vRot.x, vRot.y)))
					
					SET_BLIP_HIDDEN_ON_LEGEND(ArrowStrokeBlipBg, TRUE)
					SET_BLIP_ROTATION(ArrowStrokeBlipBg, ROUND(GET_HEADING_FROM_VECTOR_2D (vRot.x, vRot.y)))
				ELSE
					SET_BLIP_COORDS(ArrowStrokeBlipID, vPos)
					SET_BLIP_ROTATION(ArrowStrokeBlipID, ROUND(GET_HEADING_FROM_VECTOR_2D (vRot.x, vRot.y)))
					SET_BLIP_DISPLAY(ArrowStrokeBlipID, DISPLAY_RADAR_ONLY)
					SET_BLIP_COORDS(ArrowStrokeBlipBg, vPos)
					SET_BLIP_ROTATION(ArrowStrokeBlipBg, ROUND(GET_HEADING_FROM_VECTOR_2D (vRot.x, vRot.y)))
					SET_BLIP_DISPLAY(ArrowStrokeBlipBg, DISPLAY_RADAR_ONLY)
				ENDIF
			ELSE
			
			sKevCamVars.fMaxCameraYaw = 90.0
			RUN_HELICOPTER_HUD(sKevCamVars)
			
			ENDIF
			
			MAINTAIN_TURRET_NETWORK_DATA()
			
			
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER( "Heli gun - RUN_HELICOPTER_HUD (Kev)")
			#ENDIF
			#ENDIF	
			
			sKevCamVars.bDisableScanner = TRUE
			
			IF sKevCamVars.bDeactivateChopperCam

				IF g_iInteriorTurretSeat != -1
					SET_HELI_GUN_STATE(eHELIGUNSTATE_OFF)
					sKevCamVars.bDeactivateChopperCam = FALSE
					//url:bugstar:4199336
					IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bPlayerUsingTurretCam 
						UNLOCK_MINIMAP_POSITION()
					ENDIF
					
					g_bPlayerViewingTurretHud = FALSE
					GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bPlayerUsingTurretCam = FALSE
					CAM_CLEANUP()
				ELSE
					SCRIPT_CLEANUP()
				ENDIF 
			ENDIF

			
			
			IF NOT bIAmCoPilot
			AND NOT bIAmTurretPilot1
			AND NOT bIAmTurretPilot2
			AND NOT bPilotAndAllowed
			AND eHeliModel != HUNTER
			AND eHeliModel != AKULA
			AND g_iInteriorTurretSeat = -1
				g_heligunWatchingOnly = TRUE
				sKevCamVars.mpCamFudge=true 
				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "eHELIGUNSTATE_ON: sKevCamVars.mpCamFudge=true")
				PROCESS_SYNCHING_PILOT_CAMERA_WITH_COPILOT_CAMERA()
			ELSE
				g_heligunWatchingOnly = FALSE
			ENDIF
			
			
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER( "Heli gun - ControlChopperCamera (Kev)")
			#ENDIF
			#ENDIF
			
//			displayChopperHud(sKevCamVars)
//			MAINTAIN_PLACEHOLDER_HELI_GUN_HUD()
			IF bIAmCoPilot
			OR bIAmTurretPilot1
			OR bIAmTurretPilot2
			OR DOES_INTERIOR_USE_SCRIPTED_WEAPONS()
			OR bPilotAndAllowed
				PROCESS_ROCKETS()
				IF eHeliModel != INT_TO_ENUM(MODEL_NAMES, HASH("BOMBUSHKA"))
					MAINTAIN_TAGGING()
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				IF bAllowDriverAccess
					MAINTAIN_TAGGING()
				ENDIF
			#ENDIF
			ENDIF
			
			IF bPCCOntrolsSetup
				IF IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(viHeli)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)")
				ENDIF
			ENDIF
									
//			IF MPGlobals.stRocketCoolDownTimer.bInitialisedTimer
//				IF bCanFireNonPlayerRocket
//					#IF FEATURE_HEIST_PLANNING
//					PLAY_SOUND_FROM_COORD(-1, "Prison_Finale_Buzzard_Rocket", GET_ROCKET_OFFSET(), "DLC_HEISTS_GENERIC_SOUNDS", TRUE, 50, DEFAULT )
//					#ENDIF
//					bCanFireNonPlayerRocket = FALSE
//				ENDIF
//			ELSE
//				bCanFireNonPlayerRocket = TRUE
//			ENDIF
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER( "Heli gun - PROCESS_ROCKETS (Will)")
			#ENDIF
			#ENDIF	
			
			IF (NOT IS_PAUSE_MENU_ACTIVE()
			AND NOT IS_CUSTOM_MENU_ON_SCREEN()
			AND NOT IS_PHONE_ONSCREEN()
			AND NOT IS_WARNING_MESSAGE_ACTIVE())
			OR (g_iShouldLaunchTruckTurret != -1
			AND IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(g_ownerOfArmoryTruckPropertyIAmIn)
			AND NOT NETWORK_IS_ACTIVITY_SESSION())
			OR (g_iInteriorTurretSeat  != -1
			AND IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(g_ownerOfArmoryTruckPropertyIAmIn)
			AND NOT NETWORK_IS_ACTIVITY_SESSION())
			
				CONTROL_ACTION caInput
				caInput = INPUT_CONTEXT
				CONTROL_TYPE ctInput
				ctInput = PLAYER_CONTROL
				IF g_iShouldLaunchTruckTurret != -1
				OR g_iInteriorTurretSeat != -1
					ctInput = FRONTEND_CONTROL
					caInput = INPUT_SCRIPT_RRIGHT
				ENDIF
				IF IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(viHeli)
					ctInput = FRONTEND_CONTROL
					caInput = INPUT_SCRIPT_RRIGHT
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
				ENDIF
				IF IS_CONTROL_JUST_PRESSED(ctInput, caInput)
				OR NOT IS_VEHICLE_DRIVEABLE(viHeli) 
				OR IS_ENTITY_IN_DEEP_WATER(viHeli)
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HUNTGUN_T_3")
				OR SHOULD_BAIL_FROM_TURRET()
					#IF IS_DEBUG_BUILD
						IF IS_CONTROL_JUST_PRESSED(ctInput, caInput)
							CDEBUG1LN(DEBUG_NET_GUN_TURRET, "eHELIGUNSTATE_ON: Bailing: IS_CONTROL_JUST_PRESSED, ctInput = ", ctInput, ", caInput = ", caInput)
						ENDIF
						IF NOT IS_VEHICLE_DRIVEABLE(viHeli) 
							CDEBUG1LN(DEBUG_NET_GUN_TURRET, "eHELIGUNSTATE_ON: Bailing: IS_VEHICLE_DRIVEABLE")
						ENDIF
						IF IS_ENTITY_IN_DEEP_WATER(viHeli)
							CDEBUG1LN(DEBUG_NET_GUN_TURRET, "eHELIGUNSTATE_ON: Bailing: IS_ENTITY_IN_DEEP_WATER")
						ENDIF 
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HUNTGUN_T_3")
							CDEBUG1LN(DEBUG_NET_GUN_TURRET, "eHELIGUNSTATE_ON: Bailing: IS_THIS_HELP_MESSAGE_BEING_DISPLAYED")
						ENDIF
						
						IF SHOULD_BAIL_FROM_TURRET()
							CDEBUG1LN(DEBUG_NET_GUN_TURRET, "eHELIGUNSTATE_ON: Bailing: SHOULD_BAIL_FROM_TURRET")
						ENDIF
					#ENDIF
					//Enable help text to come show again
					bShownTurnOnHeliGunHelp = FALSE
					
					IF IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(viHeli)
						CDEBUG1LN(DEBUG_NET_GUN_TURRET, "NET TIMER STARTED: stBombCamExitTimer")
						START_NET_TIMER(stBombCamExitTimer, TRUE)
						g_bBombuskaExitCamTimerOn = TRUE
					ENDIF
					
					IF DOES_BLIP_EXIST(ArrowStrokeBlipID)
						REMOVE_BLIP(ArrowStrokeBlipID)
					ENDIF
					IF DOES_BLIP_EXIST(ArrowStrokeBlipBg)
						REMOVE_BLIP(ArrowStrokeBlipBg)
					ENDIF
					IF DOES_BLIP_EXIST(GET_MAIN_PLAYER_BLIP_ID())
						SET_BLIP_ALPHA(GET_MAIN_PLAYER_BLIP_ID(), 255)
					ENDIF
				//OR IS_CELLPHONE_CAMERA_IN_USE() 
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-7)
					IF g_iShouldLaunchTruckTurret != -1
					OR g_iInteriorTurretSeat != -1
						g_bActiveInGunTurret = FALSE
						PRINTLN("[MAINTAIN_TURRETS] - g_bActiveInGunTurret = FALSE")
						CAM_CLEANUP()
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_TURRET_INSTRUCTION_HELP_STRING(viHeli))
//						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HUNTGUN_T_3")
							CDEBUG1LN(DEBUG_NET_GUN_TURRET, "Help text being cleared")
							CLEAR_HELP(TRUE)
						ENDIF
						DO_SCREEN_FADE_OUT(0)
						CDEBUG1LN(DEBUG_NET_GUN_TURRET, "eHELIGUNSTATE_ON: DO_SCREEN_FADE_OUT")
						g_bPlayerViewingTurretHud = FALSE
						GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bPlayerUsingTurretCam = FALSE
						g_bTurretFadeOut = TRUE
						CDEBUG1LN(DEBUG_NET_GUN_TURRET, "g_bPlayerViewingTurretHud = FALSE")
						SET_HELI_GUN_STATE(eHELIGUNSTATE_OFF)
						vPos = GET_PLAYER_COORDS(PLAYER_ID())
						IF NOT IS_VECTOR_ZERO(vPos)
							SET_FAKE_PAUSEMAP_PLAYER_POSITION_THIS_FRAME(vPos.x,vPos.y)
							//LOCK_MINIMAP_POSITION(vPos.x,vPos.y)
						ENDIF
						UNLOCK_MINIMAP_POSITION()
					ELIF IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(viHeli)
						SET_HELI_GUN_STATE(eHELIGUNSTATE_OFF)
						CAM_CLEANUP()
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
					ELSE
						SET_GAME_STATE(eGAMESTATE_END)		
					ENDIF
					g_SimpleInteriorData.bSuppressConcealCheck = FALSE
				ENDIF
			ELSE
				
			ENDIF
			
			#IF IS_DEBUG_BUILD
		          #IF SCRIPT_PROFILER_ACTIVE 
		          CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
		          #ENDIF
		   	#ENDIF
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER( "Heli gun - eHELIGUNSTATE_ON")
			#ENDIF
			#ENDIF
			
		BREAK
		
	ENDSWITCH
	
	MAINTAIN_TURRET_AUDIO()
	
	#IF IS_DEBUG_BUILD
	      #IF SCRIPT_PROFILER_ACTIVE 
	      CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
	      #ENDIF
	#ENDIF
			
ENDPROC

FUNC BOOL SHOULD_SCRIPT_CLEANUP()
	
	IF HELI_EXISTS_WITH_MODEL( HUNTER )
	AND IS_ENTITY_ALIVE( pilotPed )
	AND pilotPed = PLAYER_PED_ID()
	AND NOT IS_ENTITY_ALIVE( coPilotPed )
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, " SHOULD_SCRIPT_CLEANUP - I am hunter pilot and the co-pilot doesn't exist or is dead.")
	   	RETURN TRUE
	ENDIF
		
 // If we have a match end event, bail.
	IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, " SHOULD_SCRIPT_CLEANUP - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE returning TRUE.")
	   	RETURN TRUE
	ENDIF
	IF SHOULD_PLAYER_LEAVE_MP_MISSION() 
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, " SHOULD_SCRIPT_CLEANUP - SHOULD_PLAYER_LEAVE_MP_MISSION returning TRUE.")
	    RETURN TRUE
	ENDIF
	IF g_bFailedMission
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, " SHOULD_SCRIPT_CLEANUP - g_bFailedMission = TRUE.")
	    RETURN TRUE
	ENDIF
	IF NOT IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
		IF g_bMissionEnding
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, " SHOULD_SCRIPT_CLEANUP - g_bMissionEnding = TRUE.")
		    RETURN TRUE
		ENDIF
	ENDIF
	IF g_bTerminateHeliGunScript
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, " SHOULD_SCRIPT_CLEANUP - g_bTerminateHeliGunScript = TRUE.")
	    RETURN TRUE
	ENDIF
	IF g_b_OnLeaderboard
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, " SHOULD_SCRIPT_CLEANUP - g_b_OnLeaderboard = TRUE.")
	    RETURN TRUE
	ENDIF
	IF g_b_onNewJobVoteScreen
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, " SHOULD_SCRIPT_CLEANUP - g_b_onNewJobVoteScreen = TRUE.")
	    RETURN TRUE
	ENDIF
	IF g_bCelebrationScreenIsActive 
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, " SHOULD_SCRIPT_CLEANUP - g_bCelebrationScreenIsActive = TRUE.")
	    RETURN TRUE
	ENDIF
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, " SHOULD_SCRIPT_CLEANUP - NETWORK_IS_GAME_IN_PROGRESS = FALSE.")
	    SCRIPT_CLEANUP()
	ENDIF
	IF IS_TRANSITION_ACTIVE()
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, " SHOULD_SCRIPT_CLEANUP - IS_TRANSITION_ACTIVE = FALSE.")
	    RETURN TRUE
	ENDIF
	IF NETWORK_IS_PLAYER_IN_MP_CUTSCENE(PLAYER_ID())
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, " SHOULD_SCRIPT_CLEANUP - NETWORK_IS_PLAYER_IN_MP_CUTSCENE = TRUE.")
	    RETURN TRUE
	ENDIF
	IF IS_MP_PASSIVE_MODE_ENABLED()
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, " SHOULD_SCRIPT_CLEANUP - IS_MP_PASSIVE_MODE_ENABLED = TRUE.")
	    RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
	OR IS_PLAYER_IN_CREATOR_TRAILER(PLAYER_ID())
	
		IF g_iShouldLaunchTruckTurret = -1
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, " SHOULD_SCRIPT_CLEANUP -  - IS_PLAYER_IN_ARMORY_TRUCK = TRUE && Not sitting in seat.")
            RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ELSE
//			CDEBUG1LN(DEBUG_NET_GUN_TURRET, " - IS_PLAYER_IN_ARMORY_TRUCK AND IS_PLAYER_IN_CREATOR_TRAILER= FALSE")
	ENDIF
	
	IF IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
	OR IS_PLAYER_IN_CREATOR_AIRCRAFT(PLAYER_ID())
		IF g_iInteriorTurretSeat = -1
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, " SHOULD_SCRIPT_CLEANUP -  - IS_PLAYER_IN_ARMORY_AIRCRAFT = TRUE && g_iInteriorTurretSeat = -1, not sitting down.")
            RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF DOES_INTERIOR_USE_TURRETS()
		IF g_iInteriorTurretSeat = -1
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, " SHOULD_SCRIPT_CLEANUP -  - DOES_INTERIOR_USE_TURRETS = TRUE && g_iInteriorTurretSeat = -1, not sitting down.")
            RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

// *********************************** \\
// *********************************** \\
//                                     \\
//				 MAIN LOOP             \\
//                                     \\
// *********************************** \\
// *********************************** \\

/// PURPOSE:
///    Main loop.
/// PARAMS:
///    HordeMissionData - mission data.
SCRIPT(MP_MISSION_DATA HordeMissionData)

    // Carry out all the initial game starting duties. 
    IF NETWORK_IS_GAME_IN_PROGRESS()
        PROCESS_PRE_GAME(HordeMissionData)      
    ENDIF
	
	
    // Main while loop.
    WHILE TRUE
        
        // One wait to rule them all. This can be the ONLY wait from here on in...
        MP_LOOP_WAIT_ZERO()
       		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, " heli_gun - SCRIPT update : SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE() = true ")
			SCRIPT_CLEANUP()
		ENDIF
		
		// Initiate script profiler.
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_START_OF_FRAME()
		#ENDIF
		#ENDIF
		
		// Process security van client logic.
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER( "Heli gun - top wrapper (ignore this)")
		#ENDIF
		#ENDIF
				
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_DrawDebugBombushka")
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
		ENDIF 
		#ENDIF
		
		// So we only call certain commands once per frame.
		MAINTAIN_START_OF_FRAME_DATA()
		//Relies on some start of frame data. SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE() is exclusively checked earlier in this update loop.
		IF SHOULD_SCRIPT_CLEANUP()
			SCRIPT_CLEANUP() 
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER( "Heli gun - GAME_STATE_END checks")
		#ENDIF
		#ENDIF	
					
        // Deal with the debug
        #IF IS_DEBUG_BUILD
            MAINTAIN_WIDGETS()
        #ENDIF
       	
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER( "Heli gun - MAINTAIN_WIDGETS")
		#ENDIF
		#ENDIF
		
        // -----------------------------------
        // Process client game logic    
        REINIT_NET_TIMER(tdNetTimer)
    	
		// url:bugstar:3574343
		IF IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
			SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WANTED_STARS)
		ENDIF
		
		// -----------------------------------
		// Process your game logic.....
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
		AND PARTICIPANT_ID_TO_INT() != -1
						
			SWITCH playerBD[PARTICIPANT_ID_TO_INT()].eGameState
							
				// Wait until the server gives the all go before moving on.
				CASE eGAMESTATE_INI		
					//ADD_HELI_HELMET_FOR_GUNNER()
					
					SET_GAME_STATE(eGAMESTATE_RUNNING)
				BREAK
				
				// Main gameplay state.
				CASE eGAMESTATE_RUNNING			
					
					//Disable player auto shuffle while they're using a turret cam.
					IF IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(viHeli)
					AND NOT IS_PED_INJURED( PLAYER_PED_ID() )
					AND GET_PED_CONFIG_FLAG( PLAYER_PED_ID(), PCF_PreventAutoShuffleToDriversSeat ) != IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bInHeliGunCam	)
						//If we haven't set this flag as having been changed...
						IF NOT IS_BIT_SET( iPreviousPedConfigFlag, PPCF_BS_PREVENT_SHUFFLE_HAS_BEEN_MODIFIED )
							//...set that we have set it
							SET_BIT( iPreviousPedConfigFlag, PPCF_BS_PREVENT_SHUFFLE_HAS_BEEN_MODIFIED )
							//If the previous PCF was true...
							IF GET_PED_CONFIG_FLAG( PLAYER_PED_ID(), PCF_PreventAutoShuffleToDriversSeat )
								//...Save that info, else it will be saved as having been false.
								SET_BIT( iPreviousPedConfigFlag, PPCF_BS_PREVENT_SHUFFLE_ENABLED )			
							ENDIF
						ENDIF
						
						IF NOT g_bBlockSeatShufflePlacedVeh
							SET_PED_CONFIG_FLAG( PLAYER_PED_ID(), PCF_PreventAutoShuffleToDriversSeat, IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bInHeliGunCam	) ) 
							PRINTLN( "[OCH] Setting pcf: PCF_PreventAutoShuffleToTurretSeat = GlobalPlayerBroadcastDataFM_BS_bInHeliGunCam	) = ", IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bInHeliGunCam	) )
						ENDIF
					ENDIF
					
					// Process client logic.
	                PROCESS_HELI_GUN()
				
	            	
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER( "Heli gun - PROCESS_HELI_GUN")
					#ENDIF
					#ENDIF
					
					g_B_Disable_HELI_CAM_Shooting_This_Frame = FALSE
					
					// Look for the server say the mission has ended.
					IF serverBd.eGameState >= eGAMESTATE_END
						SET_GAME_STATE(eGAMESTATE_END)			
					ENDIF	
					
				BREAK	

				CASE eGAMESTATE_LEAVE
						
				BREAK
					
				CASE eGAMESTATE_END				
					SCRIPT_CLEANUP()
				BREAK
				
				DEFAULT 
	                #IF IS_DEBUG_BUILD
	                    SCRIPT_ASSERT("Heli gun: Problem in SWITCH eGameState") 
	                #ENDIF
	            BREAK
					
			ENDSWITCH
		ENDIF
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			
			SWITCH serverBd.eGameState
				
				CASE eGAMESTATE_INI	
					
					serverBd.eGameState = eGAMESTATE_RUNNING
					
				BREAK
				
				CASE eGAMESTATE_RUNNING		
					
				BREAK
				
				CASE eGAMESTATE_LEAVE
					
				BREAK
				
				CASE eGAMESTATE_END			
					SCRIPT_CLEANUP()
				BREAK	
				
				DEFAULT 
	                #IF IS_DEBUG_BUILD
	                    SCRIPT_ASSERT("Heli gun: Problem in SWITCH eGameState") 
	                #ENDIF
	            BREAK
		
			ENDSWITCH

		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			SCRIPT_PROFILER_END_OF_FRAME()
		#ENDIF
		#ENDIF
    ENDWHILE
	
ENDSCRIPT

