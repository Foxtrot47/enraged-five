
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name: gb_gunrunning_delivery.sc       																	//
// Description: Main script handling delivery cutscene.														//
// Written by:  Tymon																						//
// Date:  		31/01/2017																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "net_include.sch"
USING "net_wait_zero.sch"
USING "net_simple_cutscene.sch"

CONST_FLOAT CI_WAIT_UNTIL_DELIVERY_DISAPLYED 2.0
CONST_INT CI_FAILSAFE_VEHICLE_FADE 7
CONST_INT CI_STREAM_FAILSAFE_TIME 7
CONST_INT CI_DELAY_BEFORE_CUTSCENE 2
CONST_INT CI_STREAMING_FADEOUT_TIME 250
CONST_INT CI_STREAMING_FADEIN_TIME 500
CONST_INT CI_HALT_VEHICLE_TIME 1
CONST_FLOAT CF_HALT_VEHICLE_DISTANCE 0.0

USING "net_freemode_delivery_private.sch"
USING "gb_gunrunning_cutscene_move_weapons.sch"
USING "gb_gunrunning_cutscene_phantom.sch"
USING "gb_gunrunning_cutscene_follow_leader.sch"
USING "net_freemode_delivery_helpers.sch"
USING "GB_GUNRUNNING_CUTSCENE_BUNKER_VEHICLE.sch"
USING "GB_GUNRUNNING_CUTSCENE_BUNKER_FOOT.sch"
USING "GB_GUNRUNNING_CUTSCENE_BUNKER_HELI.sch"
USING "GB_GUNRUNNING_SPAWN_DATA.sch"
USING "GB_GUNRUNNING_CUTSCENE_HILL_CLIMB.sch"
USING "GB_GUNRUNNING_DELIVERY_HEADER.sch"
USING "GB_GUNRUNNING_CUTSCENE_ROUGH_TERRAIN.sch"
USING "GB_GUNRUNNING_CUTSCENE_AMBUSHED.sch"
USING "gb_gunrunning_cutscene_bunker_cargobob_vehicle.sch"

STRUCT ServerBroadcastData
	INT iBS
	BOOL bCutsceneWillStart
	BOOL bStartCutscene
	
	BOOL bSpawnPointsReady
	VECTOR vSpawnPoints[32]
	FLOAT fSpawnHeadings[32]
	INT iCurrentGeneratedSpawnPoint
	INT iProcessedSpawnPoints
ENDSTRUCT
ServerBroadcastData serverBD

ENUM DELIVERY_SCRIPT_STATE
	DELIVERY_STATE_INITIAL,
	DELIVERY_STATE_LOADING,
	DELIVERY_STATE_READY,
	DELIVERY_STATE_STREAMING,
	DELIVERY_STATE_SKIP_CUTSCENE,  //url:bugstar:3578852
	DELIVERY_STATE_PLAYING_CUTSCENE,
	DELIVERY_STATE_SKIP_CUTSCENE_WAIT_WARP,  //url:bugstar:3578852
	DELIVERY_STATE_INTERIOR_WARP,
	DELIVERY_STATE_WARP,
	DELIVERY_STATE_SCENE_FAILED,
	DELIVERY_STATE_FINISHED_CUTSCENE
ENDENUM

ENUM DELIVERY_FADE_AND_MOVE_ENTITIES_STATE
	MES_FADE_OUT_ENTITIES,
	MES_WARP_VEHICLE,
	MES_WARP_PV,
	MES_WARP_PEGASUS_VEH,
	MES_WARP_PLAYER,
	MES_DELETE_VEHICLE,
	MES_FADE_IN_VEHICLE,
	MES_WARP_BOSS_VEH,
	MES_AWAIT_BOSS_VEH_FADE_IN,
	MES_DONE
ENDENUM

STRUCT PlayerBroadcastData
	INT iBS
	DELIVERY_SCRIPT_STATE eState
	VEHICLE_SEAT vehicleSeat
ENDSTRUCT

PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

FREEMODE_DELIVERY_CUTSCENE_DATA cutsceneData

GUNRUNNING_DELIVERY_SCRIPT_DATA scriptData
DELIVERY_FADE_AND_MOVE_ENTITIES_STATE eFadeAndMoveState
structtimer stCutsceneStartDelay
structtimer stStreamFailSafe
structtimer stVehicleFadeFailSafe
structtimer stWaitDeliveryMessage

BOOL bPhantomDeliveryViaCargobob = FALSE
//Model we save out when warping some vehicles away from the bunker
STRUCT GUNRUN_MOVE_AFTER_BUNKER_DELIVERY_DATA
	PLAYER_INDEX 	piBossVehOwner
	MODEL_NAMES 	eVehModel 				= DUMMY_MODEL_FOR_SCRIPT
	SCRIPT_TIMER 	stFadeInBossVehTimer
	INT iPegasusVehOwner
	VEHICLE_INDEX vehVehicleToTmove
ENDSTRUCT

GUNRUN_MOVE_AFTER_BUNKER_DELIVERY_DATA thisSceneMoveEntityStruct

#IF IS_DEBUG_BUILD
STRUCT GUNRUNNING_DELIVERY_CUTSCENE_DEBUG_DATA
	BOOL bCamProbing
	BOOL bAddSpawnPoints
	BOOL bKillThisScript
	BOOL bTriggerCutscene
ENDSTRUCT
GUNRUNNING_DELIVERY_CUTSCENE_DEBUG_DATA debugData
#ENDIF

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ PROCEDURES  ╞═══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL BUILD_CUTSCENE_INTERFACE(FREEMODE_DELIVERY_CUTSCENES eCutscene)
	SWITCH eCutscene
		CASE GUNRUNNING_CUTSCENE_HILL_CLIMB
			scriptData.cutsceneInterface.procConstructor = &CONSTRUCT_HILL_CLIMB_CUTSCENE
			scriptData.cutsceneInterface.funcLoader = &HAS_HILL_CLIMB_CUTSCENE_INITIALISED
			scriptData.cutsceneInterface.procStart = &START_HILL_CLIMB_CUTSCENE
			scriptData.cutsceneInterface.procMaintain = &MAINTAIN_HILL_CLIMB_CUTSCENE
			scriptData.cutsceneInterface.procDestructor = &DESTRUCT_HILL_CLIMB_CUTSCENE
			#IF IS_DEBUG_BUILD
			scriptData.cutsceneInterface.procMaintainDebug = &MAINTAIN_HILL_CLIMB_CUTSCENE_DEBUG
			#ENDIF
			
			RETURN TRUE
		BREAK
		CASE GUNRUNNING_CUTSCENE_MOVE_WEAPONS
			scriptData.cutsceneInterface.procConstructor = &CONSTRUCT_MOVE_WEAPONS_CUTSCENE
			scriptData.cutsceneInterface.funcLoader = &HAS_MOVE_WEAPONS_CUTSCENE_INITIALISED
			scriptData.cutsceneInterface.procStart = &START_MOVE_WEAPONS_CUTSCENE
			scriptData.cutsceneInterface.procMaintain = &MAINTAIN_MOVE_WEAPONS_CUTSCENE
			scriptData.cutsceneInterface.procDestructor = &DESTRUCT_MOVE_WEAPONS_CUTSCENE
			#IF IS_DEBUG_BUILD
			scriptData.cutsceneInterface.procMaintainDebug = &MAINTAIN_MOVE_WEAPONS_CUTSCENE_DEBUG
			#ENDIF
			
			RETURN TRUE
		BREAK
		CASE GUNRUNNING_CUTSCENE_PHANTOM
			scriptData.cutsceneInterface.procConstructor = &CONSTRUCT_PHANTOM_CUTSCENE
			scriptData.cutsceneInterface.funcLoader = &HAS_PHANTOM_CUTSCENE_INITIALISED
			scriptData.cutsceneInterface.procStart = &START_PHANTOM_CUTSCENE
			scriptData.cutsceneInterface.procMaintain = &MAINTAIN_PHANTOM_CUTSCENE
			scriptData.cutsceneInterface.procDestructor = &DESTRUCT_PHANTOM_CUTSCENE
			#IF IS_DEBUG_BUILD
			scriptData.cutsceneInterface.procMaintainDebug = &MAINTAIN_PHANTOM_CUTSCENE_DEBUG
			#ENDIF
			
			RETURN TRUE
		BREAK
		CASE GUNRUNNING_CUTSCENE_FOLLOW_LEADER
			scriptData.cutsceneInterface.procConstructor = &CONSTRUCT_FOLLOW_THE_LEADER_CUTSCENE
			scriptData.cutsceneInterface.funcLoader = &HAS_FOLLOW_THE_LEADER_CUTSCENE_INITIALISED
			scriptData.cutsceneInterface.procStart = &START_FOLLOW_THE_LEADER_CUTSCENE
			scriptData.cutsceneInterface.procMaintain = &MAINTAIN_FOLLOW_THE_LEADER_CUTSCENE
			scriptData.cutsceneInterface.procDestructor = &DESTRUCT_FOLLOW_THE_LEADER_CUTSCENE
			#IF IS_DEBUG_BUILD
			scriptData.cutsceneInterface.procMaintainDebug = &MAINTAIN_FOLLOW_THE_LEADER_CUTSCENE_DEBUG
			#ENDIF
			
			RETURN TRUE
		BREAK
		CASE GUNRUNNING_CUTSCENE_BUNKER_VEHICLE
			scriptData.cutsceneInterface.procConstructor = &CONSTRUCT_BUNKER_DELIVERY_VEHICLE_CUTSCENE
			scriptData.cutsceneInterface.funcLoader = &HAS_BUNKER_DELIVERY_VEHICLE_CUTSCENE_INITIALISED
			scriptData.cutsceneInterface.procStart = &START_BUNKER_DELIVERY_VEHICLE_CUTSCENE
			scriptData.cutsceneInterface.procMaintain = &MAINTAIN_BUNKER_DELIVERY_VEHICLE_CUTSCENE
			scriptData.cutsceneInterface.procDestructor = &DESTRUCT_BUNKER_DELIVERY_VEHICLE_CUTSCENE
			#IF IS_DEBUG_BUILD
			scriptData.cutsceneInterface.procMaintainDebug = &MAINTAIN_BUNKER_DELIVERY_VEHICLE_CUTSCENE_DEBUG
			#ENDIF
			
			RETURN TRUE
		BREAK
		CASE GUNRUNNING_CUTSCENE_BUNKER_FOOT
			scriptData.cutsceneInterface.procConstructor = &CONSTRUCT_BUNKER_DELIVERY_FOOT_CUTSCENE
			scriptData.cutsceneInterface.funcLoader = &HAS_BUNKER_DELIVERY_FOOT_CUTSCENE_INITIALISED
			scriptData.cutsceneInterface.procStart = &START_BUNKER_DELIVERY_FOOT_CUTSCENE
			scriptData.cutsceneInterface.procMaintain = &MAINTAIN_BUNKER_DELIVERY_FOOT_CUTSCENE
			scriptData.cutsceneInterface.procDestructor = &DESTRUCT_BUNKER_DELIVERY_FOOT_CUTSCENE
			#IF IS_DEBUG_BUILD
			scriptData.cutsceneInterface.procMaintainDebug = &MAINTAIN_BUNKER_DELIVERY_FOOT_CUTSCENE_DEBUG
			#ENDIF
			
			RETURN TRUE
		BREAK
		CASE GUNRUNNING_CUTSCENE_BUNKER_HELI
			scriptData.cutsceneInterface.procConstructor = &CONSTRUCT_BUNKER_DELIVERY_HELI_CUTSCENE
			scriptData.cutsceneInterface.funcLoader = &HAS_BUNKER_DELIVERY_HELI_CUTSCENE_INITIALISED
			scriptData.cutsceneInterface.procStart = &START_BUNKER_DELIVERY_HELI_CUTSCENE
			scriptData.cutsceneInterface.procMaintain = &MAINTAIN_BUNKER_DELIVERY_HELI_CUTSCENE
			scriptData.cutsceneInterface.procDestructor = &DESTRUCT_BUNKER_DELIVERY_HELI_CUTSCENE
			#IF IS_DEBUG_BUILD
			scriptData.cutsceneInterface.procMaintainDebug = &MAINTAIN_BUNKER_DELIVERY_HELI_CUTSCENE_DEBUG
			#ENDIF
			
			RETURN TRUE
		BREAK
		CASE GUNRUNNING_CUTSCENE_BUNKER_CARGOBOB_ATTACHED
			scriptData.cutsceneInterface.procConstructor = &CONSTRUCT_BUNKER_DELIVERY_CARGOBOB_ATTACHED_CUTSCENE
			scriptData.cutsceneInterface.funcLoader = &HAS_BUNKER_DELIVERY_CARGOBOB_ATTACHED_CUTSCENE_INITIALISED
			scriptData.cutsceneInterface.procStart = &START_BUNKER_DELIVERY_CARGOBOB_ATTACHED_CUTSCENE
			scriptData.cutsceneInterface.procMaintain = &MAINTAIN_BUNKER_DELIVERY_CARGOBOB_ATTACHED_CUTSCENE
			scriptData.cutsceneInterface.procDestructor = &DESTRUCT_BUNKER_DELIVERY_CARGOBOB_ATTACHED_CUTSCENE
			#IF IS_DEBUG_BUILD
			scriptData.cutsceneInterface.procMaintainDebug = &MAINTAIN_BUNKER_DELIVERY_CARGOBOB_ATTACHED_CUTSCENE_DEBUG
			#ENDIF
			
			RETURN TRUE
		BREAK
		CASE GUNRUNNING_CUTSCENE_ROUGH_TERRAIN
			scriptData.cutsceneInterface.procConstructor = &CONSTRUCT_ROUGH_TERRAIN_CUTSCENE
			scriptData.cutsceneInterface.funcLoader = &HAS_ROUGH_TERRAIN_CUTSCENE_INITIALISED
			scriptData.cutsceneInterface.procStart = &START_ROUGH_TERRAIN_CUTSCENE
			scriptData.cutsceneInterface.procMaintain = &MAINTAIN_ROUGH_TERRAIN_CUTSCENE
			scriptData.cutsceneInterface.procDestructor = &DESTRUCT_ROUGH_TERRAIN_CUTSCENE
			#IF IS_DEBUG_BUILD
			scriptData.cutsceneInterface.procMaintainDebug = &MAINTAIN_ROUGH_TERRAIN_CUTSCENE_DEBUG
			#ENDIF
			
			RETURN TRUE
		BREAK
		CASE GUNRUNNING_CUTSCENE_AMBUSHED
			scriptData.cutsceneInterface.procConstructor = &CONSTRUCT_AMBUSHED_CUTSCENE
			scriptData.cutsceneInterface.funcLoader = &HAS_AMBUSHED_CUTSCENE_INITIALISED
			scriptData.cutsceneInterface.procStart = &START_AMBUSHED_CUTSCENE
			scriptData.cutsceneInterface.procMaintain = &MAINTAIN_AMBUSHED_CUTSCENE
			scriptData.cutsceneInterface.procDestructor = &DESTRUCT_AMBUSHED_CUTSCENE
			#IF IS_DEBUG_BUILD
			scriptData.cutsceneInterface.procMaintainDebug = &MAINTAIN_AMBUSHED_CUTSCENE_DEBUG
			#ENDIF
			
		RETURN TRUE
	ENDSWITCH
	
	PRINTLN("[GUNRUNNING_CUTSCENE] BUILD_CUTSCENE_INTERFACE - Couldn't build the interface for cutscene!")
	ASSERTLN("[GUNRUNNING_CUTSCENE] BUILD_CUTSCENE_INTERFACE - Couldn't build the interface for cutscene!")
	
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
FUNC STRING GET_DELIVERY_SCRIPT_STATE_STRING(DELIVERY_SCRIPT_STATE eState)
	SWITCH eState
		CASE DELIVERY_STATE_INITIAL 			RETURN "DELIVERY_STATE_INITIAL"
		CASE DELIVERY_STATE_LOADING 			RETURN "DELIVERY_STATE_LOADING"
		CASE DELIVERY_STATE_STREAMING			RETURN "DELIVERY_STATE_STREAMING"
		CASE DELIVERY_STATE_READY 				RETURN "DELIVERY_STATE_READY"
		CASE DELIVERY_STATE_PLAYING_CUTSCENE 	RETURN "DELIVERY_STATE_PLAYING_CUTSCENE"
		CASE DELIVERY_STATE_INTERIOR_WARP		RETURN "DELIVERY_STATE_INTERIOR_WARP"
		CASE DELIVERY_STATE_WARP				RETURN "DELIVERY_STATE_WARP"
		CASE DELIVERY_STATE_FINISHED_CUTSCENE 	RETURN "DELIVERY_STATE_FINISHED_CUTSCENE"
		CASE DELIVERY_STATE_SCENE_FAILED		RETURN "DELIVERY_STATE_SCENE_FAILED"
		CASE DELIVERY_STATE_SKIP_CUTSCENE		RETURN "DELIVERY_STATE_SKIP_CUTSCENE"
		CASE DELIVERY_STATE_SKIP_CUTSCENE_WAIT_WARP RETURN "DELIVERY_STATE_SKIP_CUTSCENE_WAIT_WARP"
	ENDSWITCH
	
	RETURN "*** INVALID ***"
ENDFUNC
#ENDIF

PROC SET_DELIVERY_SCRIPT_STATE(DELIVERY_SCRIPT_STATE eState)
	IF playerBD[PARTICIPANT_ID_TO_INT()].eState != eState
		playerBD[PARTICIPANT_ID_TO_INT()].eState = eState
		PRINTLN("[GUNRUNNING_CUTSCENE] SET_DELIVERY_SCRIPT_STATE - New state: ", GET_DELIVERY_SCRIPT_STATE_STRING(eState))
	ENDIF
ENDPROC

FUNC DELIVERY_SCRIPT_STATE GET_DELIVERY_SCRIPT_STATE()
	RETURN playerBD[PARTICIPANT_ID_TO_INT()].eState
ENDFUNC

/// PURPOSE:
///    Call after assets are created, set assets as not ready
///    Create assets under the map, freeze. hide/ wait for streaming , reposition.
/// PARAMS:
///    bAssetsReady - are assets ready to be shown/ everything streamed in ?
PROC TOGGLE_SCRIPT_ASSETS_READY(BOOL bAssetsReady)
	PRINTLN("[GUNRUNNING_CUTSCENE] TOGGLE_SCRIPT_ASSETS_READY new State: ", bAssetsReady)
	
	INT i
	REPEAT COUNT_OF(cutsceneData.pedActors) i
		IF DOES_ENTITY_EXIST(cutsceneData.pedActors[i])
			SET_ENTITY_VISIBLE(cutsceneData.pedActors[i], bAssetsReady)
			IF NOT IS_PED_IN_ANY_VEHICLE(cutsceneData.pedActors[i])
				SET_ENTITY_COLLISION(cutsceneData.pedActors[i], bAssetsReady)
				FREEZE_ENTITY_POSITION(cutsceneData.pedActors[i], NOT bAssetsReady)
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT COUNT_OF(cutsceneData.vehActors) i
		IF DOES_ENTITY_EXIST(cutsceneData.vehActors[i])
			SET_ENTITY_VISIBLE(cutsceneData.vehActors[i], bAssetsReady)
			SET_ENTITY_COLLISION(cutsceneData.vehActors[i], bAssetsReady)
			
			IF bAssetsReady 
				SWITCH scriptData.launchData.eCutscene
					CASE GUNRUNNING_CUTSCENE_BUNKER_VEHICLE
					//CASE GUNRUNNING_CUTSCENE_BUNKER_HELI
					CASE GUNRUNNING_CUTSCENE_BUNKER_FOOT
					CASE GUNRUNNING_CUTSCENE_HILL_CLIMB
					CASE GUNRUNNING_CUTSCENE_PHANTOM
						//Unfreeze only for this type
						//SET_VEHICLE_GRAVITY(cutsceneData.vehActors[i], bAssetsReady)
						FREEZE_ENTITY_POSITION(cutsceneData.vehActors[i], NOT bAssetsReady)
					BREAK
				ENDSWITCH
			ELSE
				//SET_VEHICLE_GRAVITY(cutsceneData.vehActors[i], bAssetsReady)
				IF NOT IS_ENTITY_ATTACHED(cutsceneData.vehActors[i])
					FREEZE_ENTITY_POSITION(cutsceneData.vehActors[i], NOT bAssetsReady)
				ENDIF
			ENDIF

		ENDIF
	ENDREPEAT
	
	REPEAT COUNT_OF(cutsceneData.pedPlayerActors) i
		IF DOES_ENTITY_EXIST(cutsceneData.pedPlayerActors[i])
			SET_ENTITY_VISIBLE(cutsceneData.pedPlayerActors[i], bAssetsReady)
			IF NOT IS_PED_IN_ANY_VEHICLE(cutsceneData.pedPlayerActors[i])
				SET_ENTITY_COLLISION(cutsceneData.pedPlayerActors[i], bAssetsReady)
				FREEZE_ENTITY_POSITION(cutsceneData.pedPlayerActors[i], NOT bAssetsReady)
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT COUNT_OF(cutsceneData.objProps) i
		IF DOES_ENTITY_EXIST(cutsceneData.objProps[i])
			SET_ENTITY_VISIBLE(cutsceneData.objProps[i], bAssetsReady)
			IF NOT IS_ENTITY_ATTACHED(cutsceneData.objProps[i])
			AND NOT IS_ENTITY_STATIC(cutsceneData.objProps[i])
			SET_ENTITY_COLLISION(cutsceneData.objProps[i], bAssetsReady)
			FREEZE_ENTITY_POSITION(cutsceneData.objProps[i], NOT bAssetsReady)
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC SCRIPT_CLEANUP()
	PRINTLN("[GUNRUNNING_CUTSCENE] SCRIPT_CLEANUP")
	
	VEHICLE_SET_JET_WASH_FORCE_ENABLED(TRUE)
		
	//Interior script toggles no loading off when inside.
	IF NOT GUNNRUNNING_DROPOFF_IS_PLAYER_BUNKER(g_FreemodeDeliveryData.eDropoffForDeliveryScript)
		IF IS_NO_LOADING_SCREEN_ACTIVE(scriptData)
			PRINTLN("[GUNRUNNING_CUTSCENE] no loading screen was active, disabling.")
			TOGGLE_NO_LOADING_SCREEN(scriptData, FALSE)
		ELSE
			PRINTLN("[GUNRUNNING_CUTSCENE] no loading screen was never active.")
		ENDIF
	ENDIF
	
	INT i
	REPEAT COUNT_OF(cutsceneData.pedActors) i
		IF DOES_ENTITY_EXIST(cutsceneData.pedActors[i])
			DELETE_PED(cutsceneData.pedActors[i])
		ENDIF
	ENDREPEAT
	
	REPEAT COUNT_OF(cutsceneData.vehActors) i
		IF DOES_ENTITY_EXIST(cutsceneData.vehActors[i])
			DELETE_VEHICLE(cutsceneData.vehActors[i])
		ENDIF
	ENDREPEAT
	
	REPEAT COUNT_OF(cutsceneData.pedPlayerActors) i
		IF DOES_ENTITY_EXIST(cutsceneData.pedPlayerActors[i])
			DELETE_PED(cutsceneData.pedPlayerActors[i])
		ENDIF
	ENDREPEAT
	
	REPEAT COUNT_OF(cutsceneData.objProps) i
		IF DOES_ENTITY_EXIST(cutsceneData.objProps[i])
			DELETE_OBJECT(cutsceneData.objProps[i])
		ENDIF
	ENDREPEAT
	
	IF NOT IS_PLAYER_CONTROL_ON(GET_PLAYER_INDEX())
		SET_PLAYER_CONTROL(GET_PLAYER_INDEX(), TRUE)
		PRINTLN("[GUNRUNNING_CUTSCENE] SCRIPT_INITIALISE - player controlls are not on on cleanup, reenable")
	ENDIF
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		CLEAR_RAGDOLL_BLOCKING_FLAGS(PLAYER_PED_ID(), RBF_BULLET_IMPACT)
		CLEAR_RAGDOLL_BLOCKING_FLAGS(PLAYER_PED_ID(), RBF_VEHICLE_IMPACT)
		CLEAR_RAGDOLL_BLOCKING_FLAGS(PLAYER_PED_ID(), RBF_FIRE)
		CLEAR_RAGDOLL_BLOCKING_FLAGS(PLAYER_PED_ID(), RBF_ELECTROCUTION)
		CLEAR_RAGDOLL_BLOCKING_FLAGS(PLAYER_PED_ID(), RBF_PLAYER_IMPACT)
		CLEAR_RAGDOLL_BLOCKING_FLAGS(PLAYER_PED_ID(), RBF_EXPLOSION)
		CLEAR_RAGDOLL_BLOCKING_FLAGS(PLAYER_PED_ID(), RBF_IMPACT_OBJECT)
		CLEAR_RAGDOLL_BLOCKING_FLAGS(PLAYER_PED_ID(), RBF_MELEE)
		CLEAR_RAGDOLL_BLOCKING_FLAGS(PLAYER_PED_ID(), RBF_RUBBER_BULLET)
		CLEAR_RAGDOLL_BLOCKING_FLAGS(PLAYER_PED_ID(), RBF_FALLING)
		CLEAR_RAGDOLL_BLOCKING_FLAGS(PLAYER_PED_ID(), RBF_DROWNING)
		CLEAR_RAGDOLL_BLOCKING_FLAGS(PLAYER_PED_ID(), RBF_ALLOW_BLOCK_DEAD_PED)
		CLEAR_RAGDOLL_BLOCKING_FLAGS(PLAYER_PED_ID(), RBF_PLAYER_BUMP)
		CLEAR_RAGDOLL_BLOCKING_FLAGS(PLAYER_PED_ID(), RBF_PLAYER_RAGDOLL_BUMP)
		CLEAR_RAGDOLL_BLOCKING_FLAGS(PLAYER_PED_ID(), RBF_PED_RAGDOLL_BUMP)
		CLEAR_RAGDOLL_BLOCKING_FLAGS(PLAYER_PED_ID(), RBF_VEHICLE_GRAB)
		
		SET_PED_CAN_RAGDOLL(PLAYER_PED_ID(), TRUE)
	ENDIF
	
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
	ENDIF
	
	CLEAR_FOCUS()
	g_FreemodeDeliveryData.bDeliveryScriptAllowDeliveryShard = FALSE
	USE_CUSTOM_SPAWN_POINTS(FALSE)
	SET_PLAYER_RESPAWN_IN_VEHICLE(FALSE)
	g_eGunrunningDropoffID = FREEMODE_DELIVERY_DROPOFF_INVALID
	TERMINATE_THIS_FREEMODE_DELIVERY_SCRIPT()
ENDPROC

FUNC BOOL IS_DROPOFF_OK_FOR_SPAWNING()
	IF g_eGunrunningDropoffID >= GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_1
	AND g_eGunrunningDropoffID <= GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_30
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SET_GUNRUNNING_SPAWN_LOCATION()
	IF NOT IS_DROPOFF_OK_FOR_SPAWNING()
		g_eGunrunningSpawnLocation = SPAWN_LOCATION_NEAR_CURRENT_POSITION
	ELSE
		g_eGunrunningSpawnLocation = SPAWN_LOCATION_CUSTOM_SPAWN_POINTS
	ENDIF
ENDPROC

PROC SCRIPT_INITIALISE(FREEMODE_DELIVERY_SCRIPT_LAUNCH_DATA launchData)

	scriptData.launchData = launchData
	PRINTLN("[GUNRUNNING_CUTSCENE] SCRIPT_INITIALISE - Launching for dropoff ", FREEMODE_DELIVERY_GET_DROPOFF_DEBUG_NAME(scriptData.launchData.eDropoffID), " (", ENUM_TO_INT(scriptData.launchData.eDropoffID), ")")
	PRINTLN("[GUNRUNNING_CUTSCENE] SCRIPT_INITIALISE - Launching for deliverable: ")
	FREEMODE_DELIVERY_DEBUG_PRINT_DELIVERABLE_ID(launchData.deliverableID, "SCRIPT_INITIALISE")
	PRINTLN("[GUNRUNNING_CUTSCENE] SCRIPT_INITIALISE - Player deliverer: ", GET_PLAYER_NAME(launchData.pDeliverer))
	PRINTLN("[GUNRUNNING_CUTSCENE] SCRIPT_INITIALISE - Cutscene: ", ENUM_TO_INT(launchData.eCutscene))
	
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, launchData.iInstance)
	
	// This makes sure the net script is active, waits untill it is.
	IF NOT HANDLE_NET_SCRIPT_INITIALISATION(DEFAULT, DEFAULT, TRUE)
		PRINTLN("[GUNRUNNING_CUTSCENE] SCRIPT_INITIALISE - HANDLE_NET_SCRIPT_INITIALISATION returned FALSE - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF

	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	IF NOT Wait_For_First_Network_Broadcast()
		PRINTLN("[GUNRUNNING_CUTSCENE] SCRIPT_INITIALISE - FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		PRINTLN("[GUNRUNNING_CUTSCENE] SCRIPT_INITIALISE - NETWORK_IS_GAME_IN_PROGRESS returned FALSE - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
	PED_INDEX pedPlayer = GET_PLAYER_PED(PLAYER_ID())
	
	IF NOT IS_ENTITY_DEAD(pedPlayer)
		IF IS_PED_IN_ANY_VEHICLE(pedPlayer)
		OR IS_PED_IN_ANY_HELI(pedPlayer)
		OR IS_PED_IN_ANY_BOAT(pedPlayer)
		OR IS_PED_IN_ANY_PLANE(pedPlayer)
			scriptData.bDeliveryStartedInVehicle = TRUE
			scriptData.vehDeliveryStartedIn = GET_VEHICLE_PED_IS_IN(pedPlayer)
			PRINTLN("[GUNRUNNING_CUTSCENE] SCRIPT_INITIALISE - bDeliveryStartedInVehicle - TRUE.")
		ELSE
			scriptData.vehDeliveryStartedIn = NULL
			scriptData.bDeliveryStartedInVehicle = FALSE
			PRINTLN("[GUNRUNNING_CUTSCENE] SCRIPT_INITIALISE - bDeliveryStartedInVehicle - FALSE.")
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(pedPlayer)
	AND IS_PED_IN_ANY_VEHICLE(pedPlayer)
	AND _FREEMODE_DELIVERY_ARE_WE_DELIVERING_IN_VALID_PV(PLAYER_ID(), GET_VEHICLE_PED_IS_IN(pedPlayer))
		scriptData.vehDelivered = GET_VEHICLE_PED_IS_IN(pedPlayer)	
		PRINTLN("[GUNRUNNING_CUTSCENE] SCRIPT_INITIALISE - _FREEMODE_DELIVERY_ARE_WE_DELIVERING_IN_VALID_PV.")
	ELSE
		PRINTLN("[GUNRUNNING_CUTSCENE] SCRIPT_INITIALISE - This is delivery on foot.")
		scriptData.vehDelivered = NULL
		IF IS_PED_IN_ANY_VEHICLE(pedPlayer)
		AND NOT _FREEMODE_DELIVERY_ARE_WE_DELIVERING_IN_VALID_PV(PLAYER_ID(), GET_VEHICLE_PED_IS_IN(pedPlayer))
			scriptData.bSpecialOnFootScene 			= TRUE
			scriptData.launchData.bDeliveryByProxy 	= FALSE
			scriptData.launchData.bDeliveryInCar 	= FALSE
			scriptData.launchData.bDeliveryOnFoot 	= TRUE
			scriptData.vehDelivered 				= GET_VEHICLE_PED_IS_IN(pedPlayer)	
			PRINTLN("[GUNRUNNING_CUTSCENE] SCRIPT_INITIALISE - This is a special on foot delivery.")
		ENDIF
	ENDIF
	
	IF NOT BUILD_CUTSCENE_INTERFACE(scriptData.launchData.eCutscene)
		PRINTLN("[GUNRUNNING_CUTSCENE] SCRIPT_INITIALISE - BUILD_CUTSCENE_INTERFACE returned FALSE - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
	scriptData.vBasePos = FREEMODE_DELIVERY_DROPOFF_GET_IN_CAR_COORDS(scriptData.launchData.eDropoffID)
	cutsceneData.vBasePos = scriptData.vBasePos
	cutsceneData.eDropoffID = scriptData.launchData.eDropoffID
	cutsceneData.vehDropOff = scriptData.vehDelivered
	
	g_eGunrunningDropoffID = scriptData.launchData.eDropoffID
	
	SET_GUNRUNNING_SPAWN_LOCATION()
	
	VECTOR vLocateSizeMultiplier = <<1.0, 1.0, 1.0>>

	//Default carmod stats
	INT iMod
	REPEAT COUNT_OF(cutsceneData.iVehicleModVariation) iMod
		cutsceneData.iVehicleModVariation[iMod] = -1
	ENDREPEAT
	
	vLocateSizeMultiplier = GUNRUNNING_DROPOFF_GET_LOCATE_SIZE_MULTIPLIER(INT_TO_ENUM(GUNRUN_VARIATION, GB_GET_CONTRABAND_VARIATION_PLAYER_IS_ON(PLAYER_ID())))
	
	IF IS_PED_IN_A_CARGOBOB_WITH_VEHICLE_ATTACHED(pedPlayer)
	OR IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].freemodeDelivery.iBS, BS_FREEMODE_DELIVERY_GLOBAL_PLAYER_BD_CARGOB_DROP)
		scriptData.launchData.bDeliveryViaCargobob 	= TRUE
		vLocateSizeMultiplier = <<vLocateSizeMultiplier.x, vLocateSizeMultiplier.y, 12.0>>
	ENDIF
	
	FREEMODE_DELIVERY_GET_IN_CAR_ACTIVATION_LOCATE(scriptData.launchData.eDropoffID, scriptData.vActivationLocateMin, scriptData.vActivationLocateMax, vLocateSizeMultiplier)
	PRINTLN("[GUNRUNNING_CUTSCENE] SCRIPT_INITIALISE - Got in car activation locate: ", scriptData.vActivationLocateMin, " - ", scriptData.vActivationLocateMax)
	
	IF NOT CALL scriptData.cutsceneInterface.procConstructor(scriptData.cutscene, cutsceneData, scriptData.vehDelivered)
		PRINTLN("[GUNRUNNING_CUTSCENE] SCRIPT_INITIALISE - Cutscene constructor returned FALSE - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
	PRINTLN("[GUNRUNNING_CUTSCENE] SCRIPT_INITIALISE - INITIALISED")
	
ENDPROC

/// PURPOSE:
///    Checks if every player in a vehicle is running the same script.
/// RETURNS:
///    Returns true if every player is running this script, else false.
///    
FUNC BOOL IS_EVERYONE_IN_THIS_VEHICLE_RUNNING_THIS_SCRIPT(VEHICLE_INDEX vehIndex #IF IS_DEBUG_BUILD , BOOL bPrintDebug #ENDIF)
	IF IS_ENTITY_DEAD(vehIndex)
		RETURN FALSE
	ENDIF
	
	VEHICLE_SEAT eSeat
	PLAYER_INDEX playerIndex
	PED_INDEX pedIndex
	
	INT iSeatCount, iSeat
	iSeatCount = GET_VEHICLE_MODEL_NUMBER_OF_SEATS(GET_ENTITY_MODEL(vehIndex))
	
	#IF IS_DEBUG_BUILD
		IF bPrintDebug
			PRINTLN("[GUNRUNNING_CUTSCENE] IS_EVERYONE_IN_THIS_VEHICLE_RUNNING_THIS_SCRIPT - Calling this for vehicle ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(vehIndex)))
		ENDIF
	#ENDIF

	REPEAT iSeatCount iSeat
		eSeat = INT_TO_ENUM(VEHICLE_SEAT, iSeat - 1)
		
		IF NOT IS_VEHICLE_SEAT_FREE(vehIndex, eSeat)
			pedIndex = GET_PED_IN_VEHICLE_SEAT(vehIndex, eSeat)
			
			IF NOT IS_ENTITY_DEAD(pedIndex) AND IS_PED_A_PLAYER(pedIndex)
				playerIndex = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedIndex)
				
				#IF IS_DEBUG_BUILD
					IF bPrintDebug
						PRINTLN("[GUNRUNNING_CUTSCENE] IS_EVERYONE_IN_THIS_VEHICLE_RUNNING_THIS_SCRIPT - Player: ", GET_PLAYER_NAME(playerIndex), " is in the vehicle.")
					ENDIF
				#ENDIF
				
				IF playerIndex != INVALID_PLAYER_INDEX() AND IS_NET_PLAYER_OK(playerIndex)
					// Check if the player is not a participant of this script.
					IF NOT NETWORK_IS_PLAYER_A_PARTICIPANT(playerIndex)
						#IF IS_DEBUG_BUILD
							IF bPrintDebug
								PRINTLN("[GUNRUNNING_CUTSCENE] IS_EVERYONE_IN_THIS_VEHICLE_RUNNING_THIS_SCRIPT - Player: ", GET_PLAYER_NAME(playerIndex), " is not running script 'GB_GUNRUNNING_DELIVERY'.")
							ENDIF
						#ENDIF
						RETURN FALSE
					ELSE
						IF playerBD[NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(playerIndex))].eState != DELIVERY_STATE_READY
							#IF IS_DEBUG_BUILD
								IF bPrintDebug	
									PRINTLN("[GUNRUNNING_CUTSCENE] IS_EVERYONE_IN_THIS_VEHICLE_RUNNING_THIS_SCRIPT - Player: ", GET_PLAYER_NAME(playerIndex), " is running 'GB_GUNRUNNING_DELIVERY' but they aren't ready.")
								ENDIF
							#ENDIF
							RETURN FALSE
						ELSE
							#IF IS_DEBUG_BUILD
								IF bPrintDebug
									PRINTLN("[GUNRUNNING_CUTSCENE] IS_EVERYONE_IN_THIS_VEHICLE_RUNNING_THIS_SCRIPT - Player: ", GET_PLAYER_NAME(playerIndex), " is running script 'GB_GUNRUNNING_DELIVERY' and is loaded.")
								ENDIF
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT	

	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Returns true if any player in the same vehicle as the local player is wanted
FUNC BOOL IS_ANY_PLAYER_IN_VEH_WANTED()
	VEHICLE_INDEX veh
	PLAYER_INDEX player
	INT iPlayersInVeh, i
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
	AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)
			iPlayersInVeh = ALL_PLAYERS_IN_VEHICLE(veh, FALSE)
			
			IF iPlayersInVeh > 0
				REPEAT NUM_NETWORK_PLAYERS i
					player = INT_TO_PLAYERINDEX(i)
					
					IF IS_BIT_SET(iPlayersInVeh, i)
					AND IS_NET_PLAYER_OK(player)
						IF GET_PLAYER_WANTED_LEVEL(player) > 0
							RETURN TRUE
						ENDIF
					ENDIF
					
				ENDREPEAT
			ENDIF
		ENDIF
	ENDIF
	
	RETURN (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
ENDFUNC

/// PURPOSE:
///    Checks if we should block the delivery because of a wanted level
/// PARAMS:
///    bInVeh - Is the player in a vehicle
/// RETURNS:
///    True if we should not allow delivery
FUNC BOOL SHOULD_BLOCK_DELIVERY_FOR_WANTED_LEVEL(BOOL bInVeh)
	IF GB_GET_CONTRABAND_VARIATION_PLAYER_IS_ON(PLAYER_ID()) = ENUM_TO_INT(GRV_SELL_PHANTOM)
	OR GB_GET_CONTRABAND_VARIATION_PLAYER_IS_ON(PLAYER_ID()) = ENUM_TO_INT(GRV_STEAL_MINIGUNS)
	OR GB_GET_CONTRABAND_VARIATION_PLAYER_IS_ON(PLAYER_ID()) = ENUM_TO_INT(GRV_STEAL_RHINO)
	OR GB_GET_CONTRABAND_VARIATION_PLAYER_IS_ON(PLAYER_ID()) = ENUM_TO_INT(GRV_STEAL_MINIGUNS)
		RETURN FALSE
	ENDIF

	IF bInVeh
		RETURN IS_ANY_PLAYER_IN_VEH_WANTED()
	ENDIF
	
	RETURN (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
ENDFUNC

FUNC BOOL SHOULD_BLOCK_HELI_DELIVERY()
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND IS_PED_IN_ANY_HELI(PLAYER_PED_ID())	
		VEHICLE_INDEX vehHeli = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)
		
		IF NOT IS_ENTITY_ALIVE(vehHeli)
			PRINTLN("[GUNRUNNING_CUTSCENE] SHOULD_BLOCK_HELI_DELIVERY heli is not alive!")
			RETURN TRUE
		ENDIF
		
		VECTOR vVelocity = GET_ENTITY_VELOCITY(PLAYER_PED_ID())
		
		IF DOES_ENTITY_EXIST(GET_ENTITY_ATTACHED_TO_CARGOBOB(vehHeli))
			PRINTLN("[GUNRUNNING_CUTSCENE] SHOULD_BLOCK_HELI_DELIVERY - VMAG(vVelocity): ", VMAG(vVelocity))
			IF VMAG(vVelocity) >= 8.0
				RETURN TRUE
			ENDIF
		ELSE
			IF (vVelocity.x > 3.0 OR vVelocity.x < -3.0)
			OR (vVelocity.y > 3.0 OR vVelocity.y < -3.0)
			OR (vVelocity.z > 0.5 OR vVelocity.z < -0.5)
				#IF IS_DEBUG_BUILD
					IF (GET_FRAME_COUNT() % 10) = 0
						PRINTLN("[GUNRUNNING_CUTSCENE] SHOULD_BLOCK_HELI_DELIVERY - velocity not acceptable for delivery via heli: ", vVelocity)
					ENDIF
				#ENDIF
				
				RETURN TRUE
			ELIF IS_ENTITY_IN_AIR(vehHeli)
				#IF IS_DEBUG_BUILD
					IF (GET_FRAME_COUNT() % 10) = 0
						PRINTLN("[GUNRUNNING_CUTSCENE] SHOULD_BLOCK_HELI_DELIVERY - heli is airborne")
					ENDIF
				#ENDIF
				
				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_VEH_ACCEPTABLE(VEHICLE_INDEX vehicle)
	
	IF NOT DOES_ENTITY_EXIST(vehicle)
		RETURN FALSE
	ENDIF	
	
	MODEL_NAMES mVeh = GET_ENTITY_MODEL(vehicle)
	
	IF mVeh = THRUSTER
	OR mVeh = CHERNOBOG
	OR mVeh = KHANJALI
	OR mVeh = RIOT2
	or mVeh = VOLATOL
		RETURN FALSE
	ENDIF
	
	IF IS_THIS_MODEL_ONLY_ALLOWED_IN_ARENA_GARAGE(mVeh)
		RETURN FALSE
	ENDIF
	
	IF IS_VEHICLE_A_PERSONAL_VEHICLE(vehicle)
		IF IS_THIS_MODEL_A_SPECIAL_VEHICLE(mVeh)
		AND mVeh != TRAILERSMALL2
			PRINTLN("[GUNRUNNING_CUTSCENE] IS_THIS_VEH_ACCEPTABLE = FALSE - IS_THIS_MODEL_A_SPECIAL_VEHICLE")
			RETURN FALSE
		ENDIF
		
		IF GET_OWNER_OF_PERSONAL_VEHICLE(vehicle) = scriptData.launchData.pOwner
			RETURN !IS_THIS_MODEL_ALLOWED_IN_HANGAR(GET_ENTITY_MODEL(thisSceneMoveEntityStruct.vehVehicleToTmove))
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_CUTSCENE_BE_STARTED()
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
	AND NOT serverBD.bCutsceneWillStart
	
		#IF IS_DEBUG_BUILD
		IF debugData.bTriggerCutscene
			serverBD.bCutsceneWillStart = TRUE
			serverBD.bStartCutscene = TRUE
			debugData.bTriggerCutscene = FALSE
		ENDIF
		#ENDIF
	
		BOOl bAllowAreaCheck = FALSE
		ENTITY_INDEX deliveryVia
		
		IF scriptData.launchData.bDeliveryByProxy
		OR scriptData.launchData.bDeliveryInCar			
			BOOL bDeliverableByProxy = _FREEMODE_DELIVERY_CAN_DELIVERABLE_TYPE_BE_DELIVERED_BY_PROXY(FREEMODE_DELIVERY_GET_DELIVERABLE_TYPE(scriptData.launchData.deliverableID))
			IF DOES_ENTITY_EXIST(scriptData.vehDelivered)
				IF NOT IS_ENTITY_DEAD(scriptData.vehDelivered)
				AND NOT SHOULD_BLOCK_DELIVERY_FOR_WANTED_LEVEL(TRUE)
				AND NOT SHOULD_BLOCK_HELI_DELIVERY()
				AND NOT IS_PLAYER_INITIALISING_RCBANDITO(PLAYER_ID())
				AND NOT IS_PLAYER_INITIALISING_RC_TANK(PLAYER_ID())
					IF (bDeliverableByProxy AND NOT IS_ANYONE_TRYING_TO_GET_IN_OR_OUT_OF_THIS_VEHICLE(scriptData.vehDelivered) AND IS_EVERYONE_IN_THIS_VEHICLE_RUNNING_THIS_SCRIPT(scriptData.vehDelivered #IF IS_DEBUG_BUILD , FALSE #ENDIF))
					OR NOT bDeliverableByProxy
						deliveryVia = scriptData.vehDelivered
						
						// Handle trailer delivery by cargobob
						IF IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].freemodeDelivery.iBS, BS_FREEMODE_DELIVERY_GLOBAL_PLAYER_BD_CARGOB_DROP)
							PRINTLN("[GUNRUNNING_CUTSCENE] SHOULD_CUTSCENE_BE_STARTED - BS_FREEMODE_DELIVERY_GLOBAL_PLAYER_BD_CARGOB_DROP ")
							ENTITY_INDEX vehAttached = GET_VEHICLE_ATTACHED_TO_CARGOBOB(scriptData.vehDelivered)
							IF DOES_ENTITY_EXIST(vehAttached)
								IF NOT IS_ENTITY_DEAD(vehAttached)
									deliveryVia = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(vehAttached)
									PRINTLN("[GUNRUNNING_CUTSCENE] SHOULD_CUTSCENE_BE_STARTED - Delivery vehicle is a trailer attached to Cargobob")
								ENDIF
							ENDIF
						ENDIF
						
						bAllowAreaCheck = TRUE
					ENDIF
				ELSE
					PRINTLN("[GUNRUNNING_CUTSCENE] SHOULD_CUTSCENE_BE_STARTED - Veh is not allowed to do delivery! Dead? ", IS_ENTITY_DEAD(scriptData.vehDelivered), " wanted? ", SHOULD_BLOCK_DELIVERY_FOR_WANTED_LEVEL(TRUE))
				ENDIF
			ENDIF
		ELIF scriptData.launchData.bDeliveryOnFoot
			deliveryVia = PLAYER_PED_ID()
			IF DOES_ENTITY_EXIST(deliveryVia)
			AND NOT SHOULD_BLOCK_DELIVERY_FOR_WANTED_LEVEL(FALSE)
			AND NOT IS_PED_FALLING(PLAYER_PED_ID())
			AND NOT IS_PED_LANDING(PLAYER_PED_ID())
			AND NOT IS_PED_IN_PARACHUTE_FREE_FALL(PLAYER_PED_ID())
				bAllowAreaCheck = TRUE
			ENDIF
		ENDIF

		IF bAllowAreaCheck
			IF FREEMODE_DELIVERY_IS_DELIVERABLE_VALID_FOR_TARGET_DROPOFF(GET_DELIVERABLE_SERVER_INDEX(scriptData.launchData.deliverableID), scriptData.launchData.eDropoffID)
				IF IS_ENTITY_IN_AREA(deliveryVia, scriptData.vActivationLocateMin, scriptData.vActivationLocateMax)
					IF scriptData.launchData.bDeliveryOnFoot
					OR IS_THIS_VEH_ACCEPTABLE(scriptData.vehDelivered)
						PRINTLN("[GUNRUNNING_CUTSCENE] SHOULD_CUTSCENE_BE_STARTED - Cutscene will start!")
						serverBD.bCutsceneWillStart = TRUE
					ELSE
						PRINTLN("[GUNRUNNING_CUTSCENE] SHOULD_CUTSCENE_BE_STARTED - Cutscene will start!")
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GR_NO_PA_DEL")
							PRINT_HELP("GR_NO_PA_DEL")
						ENDIF
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[GUNRUNNING_CUTSCENE] SHOULD_CUTSCENE_BE_STARTED - deliverable is not valid for this dropoff!")
			ENDIF
		ENDIF
	ENDIF
	
	IF serverBD.bCutsceneWillStart
		serverBD.bStartCutscene = TRUE
	ENDIF
	
	RETURN serverBD.bStartCutscene
ENDFUNC

FUNC BOOL HAS_EVERYONE_STARTED_DELIVERY_CUTSCENE()
	INT i
	PARTICIPANT_INDEX participant
	REPEAT NUM_NETWORK_PLAYERS i
		participant = INT_TO_PARTICIPANTINDEX(i)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(participant)
			IF ENUM_TO_INT(playerBD[i].eState) < ENUM_TO_INT(DELIVERY_STATE_PLAYING_CUTSCENE)
				PRINTLN("[GUNRUNNING_CUTSCENE] HAS_EVERYONE_STARTED_DELIVERY_CUTSCENE - ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(participant)), " hasn't yet started the cutscene. state: ", GET_DELIVERY_SCRIPT_STATE_STRING(playerBD[i].eState))
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC BOOL HAS_EVERYONE_FADED_SCREEN_FOR_CUTSCENE_SKIP()
	VEHICLE_INDEX vehicleDelivery
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		vehicleDelivery = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	ELSE
		PRINTLN("[GUNRUNNING_CUTSCENE] HAS_EVERYONE_FADED_SCREEN_FOR_CUTSCENE_SKIP - not inside a veh , returning true")
		RETURN TRUE
	ENDIF
	
	INT iParticipants
	INT iReadyForSkip
	
	PLAYER_INDEX playerID
	PED_INDEX pedID

	INT i
	PARTICIPANT_INDEX participant
	REPEAT NUM_NETWORK_PLAYERS i
		participant = INT_TO_PARTICIPANTINDEX(i)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(participant)
			playerID = NETWORK_GET_PLAYER_INDEX(participant)
			pedID = GET_PLAYER_PED(playerID)
			IF IS_PED_IN_ANY_VEHICLE(pedID)
			AND GET_VEHICLE_PED_IS_IN(pedID) = vehicleDelivery
				iParticipants++
				IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBS, BS_DELIVERY_SCRIPT_PLAYER_BD_SKIP_CUTSCENE)
					iReadyForSkip++
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF iReadyForSkip = iParticipants
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC REQUEST_ALL_THE_ASSETS()
	REQUEST_MODEL(MP_M_FREEMODE_01)
	REQUEST_MODEL(MP_F_FREEMODE_01)
	REQUEST_MODEL(prop_golf_ball)

	INT i
	REPEAT COUNT_OF(cutsceneData.models) i
		IF cutsceneData.models[i] != DUMMY_MODEL_FOR_SCRIPT
			REQUEST_MODEL(cutsceneData.models[i])
			PRINTLN("[GUNRUNNING_CUTSCENE] REQUEST_ALL_THE_ASSETS - Requested model: ", GET_MODEL_NAME_FOR_DEBUG(cutsceneData.models[i]))
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL HAVE_ALL_THE_ASSETS_LOADED()
	IF NOT HAS_MODEL_LOADED(MP_M_FREEMODE_01)
	OR NOT HAS_MODEL_LOADED(MP_F_FREEMODE_01)
	OR NOT HAS_MODEL_LOADED(prop_golf_ball)
		RETURN FALSE
	ENDIF
	
	IF NOT GUNRUNNING_DELIVRY_LOAD_DELIVRY_VEHICLE_CRATE(scriptData)
		RETURN FALSE
	ENDIF
	
	INT i
	REPEAT COUNT_OF(cutsceneData.models) i
		IF cutsceneData.models[i] != DUMMY_MODEL_FOR_SCRIPT
			IF NOT HAS_MODEL_LOADED(cutsceneData.models[i])
				PRINTLN("[GUNRUNNING_CUTSCENE] HAVE_ALL_THE_ASSETS_LOADED - Waiting for model to load: ", GET_MODEL_NAME_FOR_DEBUG(cutsceneData.models[i]))
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

PROC _GET_SINGLE_SPAWN_POINT(INT iIndex, VECTOR vCentre, FLOAT fDist, FLOAT fBaseDist, FLOAT &fHeading, VECTOR &vSpawnPoint)
	iIndex = iIndex + 1 // Start with point on the side of the car
	
	INT iIntervals = 8
	FLOAT fAngleStep = 360.0 / TO_FLOAT(iIntervals)
	INT iCurrentCircle = FLOOR(TO_FLOAT(iIndex) / TO_FLOAT(iIntervals))
	FLOAT fCurrentLength = iCurrentCircle * fDist + fBaseDist + ((iIndex % 2) * (fDist * 0.5))
	INT iCurrentInterval = iIndex % iIntervals
	FLOAT fAngleOffset = fAngleStep * 0.5 * (iCurrentCircle % 2)
	
	fHeading = fAngleStep * iCurrentInterval + fAngleOffset
	vSpawnPoint = vCentre + GET_HEADING_AS_VECTOR(fHeading) * fCurrentLength
ENDPROC

//FUNC PLAYER_INDEX _GET_PLAYER_IN_CACHED_SEAT(VEHICLE_SEAT vs)
//	INT i
//	REPEAT NETWORK_GET_NUM_PARTICIPANTS() i
//		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
//			IF playerBD[i].vehicleSeat = vs
//				RETURN NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
//			ENDIF
//		ENDIF
//	ENDREPEAT
//	
//	/*
//	#IF IS_DEBUG_BUILD
//	// To test how walking away works in cutscene create full set of peds if in debug
//	IF GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].IEDeliveryBD.db_bAllowAllVehicles
//		RETURN scriptData.pDeliverer
//	ENDIF
//	#ENDIF
//	*/
//	
//	PRINTLN("[GUNRUNNING_CUTSCENE] _GET_PLAYER_IN_CACHED_SEAT - We dont have player for this cached seat: ", GET_VEHICLE_SEAT_FOR_DEBUG(vs))
//	
//	// Fallbacks, just in case - worst case scenario we have one ped in the cutscene
//	//IF vs = VS_DRIVER
//		//PRINTLN("[GUNRUNNING_CUTSCENE] _GET_PLAYER_IN_CACHED_SEAT - Using fallback for vs_driver, pdeliverer: ", GET_PLAYER_NAME(scriptData.pDeliverer))
//		//RETURN scriptData.pDeliverer
//	//ENDIF
//	
//	RETURN INVALID_PLAYER_INDEX()
//ENDFUNC

//PROC _CREATE_PLAYER_PED_ACTORS()
//	
//	INT i, j
//	PARTICIPANT_INDEX participantID
//	PLAYER_INDEX playerID
//	PED_INDEX pedID
//	
//	PRINTLN("[GUNRUNNING_CUTSCENE] _CREATE_PLAYER_PED_ACTORS - number of participants: ", NETWORK_GET_NUM_PARTICIPANTS(), " my participant ID = ", NATIVE_TO_INT(PARTICIPANT_ID()))
//	
//	REPEAT NUM_NETWORK_PLAYERS i
//		participantID = INT_TO_PARTICIPANTINDEX(i)
//		
//		IF NETWORK_IS_PARTICIPANT_ACTIVE(participantID)
//			playerID = NETWORK_GET_PLAYER_INDEX(participantID)
//			pedID = GET_PLAYER_PED(playerID)
//			
//			IF DOES_ENTITY_EXIST(pedID)
//			AND NOT IS_ENTITY_DEAD(pedID)
//				PRINTLN("[GUNRUNNING_CUTSCENE] _CREATE_PLAYER_PED_ACTORS - creating an actor for script participant: ", NATIVE_TO_INT(participantID), " player name: ", GET_PLAYER_NAME(playerID))
//				IF CUTSCENE_HELP_CLONE_PLAYER(cutsceneData.pedPlayerActors[j], playerID)
//					SET_ENTITY_COORDS(cutsceneData.pedPlayerActors[j], <<10.000, 10.000, 10.000>>)
//					#IF IS_DEBUG_BUILD
//						VECTOR vCoords 
//						vCoords = GET_ENTITY_COORDS(cutsceneData.pedPlayerActors[j])
//						PRINTLN("[GUNRUNNING_CUTSCENE] _CREATE_PLAYER_PED_ACTORS - SET PLAYER CLONE COORDS: ", vCoords)
//					#ENDIF
//				ENDIF
//				//CREATE_DELIVERY_CUTSCENE_PED_PLAYER_ACTOR(cutsceneData.pedPlayerActors[j], pedID, <<0.000, 0.000, 10.000>>)
//				j = j + 1
//			ENDIF
//		ENDIF
//		
//		IF j = 6
//			PRINTLN("[GUNRUNNING_CUTSCENE] _CREATE_PLAYER_PED_ACTORS - created the max number of perticipants")
//			EXIT
//		ENDIF
//	ENDREPEAT
//ENDPROC


/// PURPOSE:
///    Run by server to generate spawn point for use by every participant
///    
PROC MAINTAIN_GENERATING_CUSTOM_SPAWN_POINTS()
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		EXIT
	ENDIF
	
	IF serverBD.bSpawnPointsReady
		EXIT
	ENDIF
	
	IF serverBD.iCurrentGeneratedSpawnPoint < COUNT_OF(serverBD.vSpawnPoints)
	AND serverBD.iProcessedSpawnPoints < COUNT_OF(serverBD.vSpawnPoints) * 2
		
		IF NATIVE_TO_INT(scriptData.spawnPointShapetest) = 0
			
			_GET_SINGLE_SPAWN_POINT(serverBD.iProcessedSpawnPoints, scriptData.vBasePos, 2.0, 4.0, scriptData.fCurrentSpawnHeading, scriptData.vCurrentSpawnPoint)
			scriptData.vCurrentSpawnPoint.Z += 5.0
			
			PRINTLN("[GUNRUNNING_CUTSCENE][SERVER] MAINTAIN_GENERATING_CUSTOM_SPAWN_POINTS - Processing point ", serverBD.iProcessedSpawnPoints) 
			
			IF NOT GET_GROUND_Z_FOR_3D_COORD(scriptData.vCurrentSpawnPoint, scriptData.vCurrentSpawnPoint.Z)
				serverBD.iProcessedSpawnPoints += 1
				PRINTLN("[GUNRUNNING_CUTSCENE][SERVER] MAINTAIN_GENERATING_CUSTOM_SPAWN_POINTS - Didn't pass ground Z test, can't use it!")
				EXIT
			ENDIF
			
			IF ABSF(scriptData.vCurrentSpawnPoint.Z - scriptData.vBasePos.Z) > 3.0
				serverBD.iProcessedSpawnPoints += 1
				PRINTLN("[GUNRUNNING_CUTSCENE][SERVER] MAINTAIN_GENERATING_CUSTOM_SPAWN_POINTS - Vertical distance between spawn point and base is too big. Skipping.")
				EXIT
			ENDIF
			
			FLOAT fAboveZ
			IF GET_GROUND_Z_FOR_3D_COORD(scriptData.vCurrentSpawnPoint + <<0.0, 0.0, 10.0>>, fAboveZ)
				IF ABSF(scriptData.vCurrentSpawnPoint.Z - fAboveZ) > 1.0
					serverBD.iProcessedSpawnPoints += 1
					PRINTLN("[GUNRUNNING_CUTSCENE][SERVER] MAINTAIN_GENERATING_CUSTOM_SPAWN_POINTS - There's something above this point, skipping.")
					EXIT
				ENDIF
			ENDIF
			
			IF IS_POINT_ON_ROAD(scriptData.vCurrentSpawnPoint, scriptData.vehDelivered)
				serverBD.iProcessedSpawnPoints += 1
				PRINTLN("[GUNRUNNING_CUTSCENE][SERVER] MAINTAIN_GENERATING_CUSTOM_SPAWN_POINTS - Point is on the road, skipping.")
				EXIT
			ENDIF
			
			IF IS_POINT_IN_ANGLED_AREA(scriptData.vCurrentSpawnPoint, g_vGunrunSpawnBlock1, g_vGunrunSpawnBlock2, g_fGunrunSpawnBlockWidth)
				serverBD.iProcessedSpawnPoints += 1
				PRINTLN("[GUNRUNNING_CUTSCENE][SERVER] MAINTAIN_GENERATING_CUSTOM_SPAWN_POINTS - Point is in blocked area, skipping.")
				EXIT
			ENDIF
			
			scriptData.spawnPointShapetest = START_SHAPE_TEST_CAPSULE(scriptData.vCurrentSpawnPoint + <<0.0, 0.0, 1.0>>, scriptData.vCurrentSpawnPoint + <<0.0, 0.0, 1.5>>, 0.25)
			scriptData.iSpawnPointTestStage = 0
			
			PRINTLN("[GUNRUNNING_CUTSCENE][SERVER] MAINTAIN_GENERATING_CUSTOM_SPAWN_POINTS - Started shapetest for it: ", scriptData.vCurrentSpawnPoint, ", ", scriptData.fCurrentSpawnHeading)
		ELSE
			
			INT iTestResult
			VECTOR vTestHitPoint, vTestHitNormal
			ENTITY_INDEX entityTestHit
			
			SHAPETEST_STATUS status = GET_SHAPE_TEST_RESULT(scriptData.spawnPointShapetest, iTestResult, vTestHitPoint, vTestHitNormal, entityTestHit)
			
			IF status = SHAPETEST_STATUS_RESULTS_READY
				PRINTLN("[GUNRUNNING_CUTSCENE][SERVER] MAINTAIN_GENERATING_CUSTOM_SPAWN_POINTS - Shapetest results ready for ", scriptData.vCurrentSpawnPoint)
				IF iTestResult = 0
					
					/*
					IF scriptData.iSpawnPointTestStage = 0
						// General sphere test passed, go up and down see if we hit anything and compare normals
						scriptData.spawnPointShapetest = START_SHAPE_TEST_LOS_PROBE(scriptData.vCurrentSpawnPoint + <<0.0, 0.0, 0.5>>, scriptData.vCurrentSpawnPoint + <<0.0, 0.0, 1.5>>, 0.25)
						scriptData.iSpawnPointTestStage = 0
						
					//ELIF scriptData.iSpawnPointTestStage = 1
					
					ELIF scriptData.iSpawnPointTestStage = 1
					*/
						serverBD.vSpawnPoints[serverBD.iCurrentGeneratedSpawnPoint] = scriptData.vCurrentSpawnPoint
						serverBD.fSpawnHeadings[serverBD.iCurrentGeneratedSpawnPoint] = scriptData.fCurrentSpawnHeading
						
						serverBD.iCurrentGeneratedSpawnPoint += 1
						scriptData.spawnPointShapetest = INT_TO_NATIVE(SHAPETEST_INDEX , 0)
						serverBD.iProcessedSpawnPoints += 1
						PRINTLN("[GUNRUNNING_CUTSCENE][SERVER] MAINTAIN_GENERATING_CUSTOM_SPAWN_POINTS - Didn't hit anything, good! Saving as point number ", serverBD.iCurrentGeneratedSpawnPoint)
					/*
					ENDIF
					*/
					
				ELSE
					PRINTLN("[GUNRUNNING_CUTSCENE][SERVER] MAINTAIN_GENERATING_CUSTOM_SPAWN_POINTS - Hit something, not good. Moving on.")
					scriptData.spawnPointShapetest = INT_TO_NATIVE(SHAPETEST_INDEX , 0)
					serverBD.iProcessedSpawnPoints += 1
				ENDIF
				
			ELIF status = SHAPETEST_STATUS_NONEXISTENT
				scriptData.spawnPointShapetest = INT_TO_NATIVE(SHAPETEST_INDEX , 0)
				PRINTLN("[GUNRUNNING_CUTSCENE][SERVER] MAINTAIN_GENERATING_CUSTOM_SPAWN_POINTS - Shapetest nonexistent, something went wrong, gonna try again.")
			ENDIF
			
		ENDIF
	ELSE
		serverBD.bSpawnPointsReady = TRUE
		PRINTLN("[GUNRUNNING_CUTSCENE][SERVER] MAINTAIN_GENERATING_CUSTOM_SPAWN_POINTS - Computed all spawn points. Count of valid points = ", serverBD.iCurrentGeneratedSpawnPoint)
		
		#IF IS_DEBUG_BUILD
			IF serverBD.iCurrentGeneratedSpawnPoint <= 0
				CASSERTLN(DEBUG_AMBIENT, "[GUNRUNNING_CUTSCENE] MAINTAIN_GENERATING_CUSTOM_SPAWN_POINTS couldn't find any valid spawn points!")
			ENDIF
		#ENDIF
	ENDIF
ENDPROC

PROC START_DELIVERY_CUTSCENE()
	//remove vfx and ambient enities
	CUTSCENE_HELP_PREPARE_AREA_FOR_CUTSCENE(scriptData.vBasePos, 15.0)
	
	IF GUNNRUNNING_DROPOFF_IS_PLAYER_BUNKER(g_FreemodeDeliveryData.eDropoffForDeliveryScript)
		TRANSFORM_STRUCT bunkerTransform
		bunkerTransform = BUNKER_CUTSCENE_GET_PROP_WORLD_COORD(GET_SIMPLE_INTERIOR_FROM_GUNRUNNING_DROPOFF(g_FreemodeDeliveryData.eDropoffForDeliveryScript), BUNKER_CA_FOOT_ENTRY)
		SET_FOCUS_POS_AND_VEL(bunkerTransform.Position, <<0.0, 0.0, 0.0>>)
		PRINTLN("[GUNRUNNING_CUTSCENE] START_DELIVERY_CUTSCENE - Bunker entry focus was set to position: ", bunkerTransform.Position)
	ENDIF
	
	IF cutsceneData.bFailedToFindValidCamPos
		PRINTLN("[GUNRUNNING_CUTSCENE] START_DELIVERY_CUTSCENE - Cutscene failed to find a valid camera position.")
		SET_DELIVERY_SCRIPT_STATE(DELIVERY_STATE_SCENE_FAILED)
		EXIT
	ENDIF
	
	IF GUNNRUNNING_DROPOFF_IS_PLAYER_BUNKER(scriptData.launchData.eDropoffID)
		SET_PLAYER_DATA_DOING_INTERIOR_WARP_FROM_CUTSCENE(TRUE)
	ENDIF
	
	g_FreemodeDeliveryData.bDeliveryScriptTriggeredCutScene = TRUE
	GB_SET_GLOBAL_CLIENT_BIT1(eGB_GLOBAL_CLIENT_BITSET_1_DOING_DELIVERY_CUTSCENE)
	
	IF GUNNRUNNING_DROPOFF_IS_PLAYER_BUNKER(g_FreemodeDeliveryData.eDropoffForDeliveryScript)
		SIMPLE_CUTSCENE_START(scriptData.cutscene, SCS_DO_NOT_RETURN_PLAYER_CONTROL)
	ELSE
		SIMPLE_CUTSCENE_START(scriptData.cutscene)
	ENDIF
ENDPROC

PROC MAINTAIN_TERMINATE_CHECKS()
	IF IS_ENTITY_DEAD(PLAYER_PED_ID())
		PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_TERMINATE_CHECKS - Cleaned up player died.")
		SCRIPT_CLEANUP()
	ENDIF
	
	IF scriptData.bDeliveryStartedInVehicle
//		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//			IF NOT IS_VEHICLE_ON_ALL_WHEELS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
//				SCRIPT_CLEANUP()
//				PRINTLN("[GUNRUNNING_CUTSCENE] SHOULD_CUTSCENE_BE_STARTED - do not start, player is in a vehicle that is not steady on ground.")
//			ENDIF
//		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(scriptData.vehDeliveryStartedIn)
		OR NOT IS_VEHICLE_DRIVEABLE(scriptData.vehDeliveryStartedIn)
			PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_TERMINATE_CHECKS - Cleaned up because vehDeliveryStartedIn was destroyed, DOES_ENTITY_EXIST: ", DOES_ENTITY_EXIST(scriptData.vehDeliveryStartedIn), " IS_VEHICLE_DRIVEABLE: ", IS_VEHICLE_DRIVEABLE(scriptData.vehDeliveryStartedIn))
			SCRIPT_CLEANUP()
		ELSE
			PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_TERMINATE_CHECKS - DELIVERY VEHICLE ALIVE.")
		ENDIF
	ELSE
		PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_TERMINATE_CHECKS - NOT IN A VEHICLE STARTED.")
	ENDIF
		
	IF NOT serverBD.bCutsceneWillStart
		IF GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].freemodeDelivery.eCurrentActiveDropoff != g_FreemodeDeliveryData.eDropoffForDeliveryScript
			PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_TERMINATE_CHECKS - Current active dropoff has changed.")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF VDIST(GET_PLAYER_COORDS(PLAYER_ID()), scriptData.vBasePos) > 1000.0
			#IF IS_DEBUG_BUILD
				VECTOR vPlayerPos = GET_PLAYER_COORDS(PLAYER_ID())
				VECTOR vbasePos = scriptData.vBasePos
				PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_TERMINATE_CHECKS - Local player moved too far. vPlayerPos: ", vPlayerPos, " vbasePos: ", vbasePos)
			#ENDIF
			
			SCRIPT_CLEANUP()
		ENDIF
		
		IF NOT scriptData.bSpecialOnFootScene
			IF scriptData.launchData.bDeliveryByProxy != g_FreemodeDeliveryData.bDeliveringViaProxy
				PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_TERMINATE_CHECKS - No longer delivering by proxy.")
				SCRIPT_CLEANUP()
			ENDIF
			
			IF scriptData.launchData.bDeliveryOnFoot != g_FreemodeDeliveryData.bDeliveringOnFoot
				PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_TERMINATE_CHECKS - No longer dleivering on foot.")
				SCRIPT_CLEANUP()
			ENDIF
			
			IF scriptData.launchData.bDeliveryInCar != g_FreemodeDeliveryData.bDeliveringInCar		
				PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_TERMINATE_CHECKS - No longer delivering in car.")
				SCRIPT_CLEANUP()
			ENDIF
			
			IF IS_PED_TRYING_TO_ENTER_OR_EXIT_VEHICLE(PLAYER_PED_ID(), FALSE)
				PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_TERMINATE_CHECKS - Entering or exiting a vehicle.")
				SCRIPT_CLEANUP()
			ENDIF
			
			IF scriptData.launchData.bDeliveryInCar 
				IF IS_VEHICLE_SEAT_FREE(scriptData.vehDelivered, DEFAULT, TRUE)
					PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_TERMINATE_CHECKS - driver no longer present in delivery vehicle.")
					SCRIPT_CLEANUP()
				ENDIF
				
				IF _FREEMODE_DELIVERY_ARE_WE_DELIVERING_WITH_A_PLAYER_FROM_ANOTHER_GANG(scriptData.vehDelivered, PLAYER_ID())
					PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_TERMINATE_CHECKS - a member of another gang has entered the vehicle.")
					SCRIPT_CLEANUP()
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].freemodeDelivery.iBS, BS_FREEMODE_DELIVERY_GLOBAL_PLAYER_BD_CARGOB_DROP)
				IF DOES_ENTITY_EXIST(scriptData.vehDelivered)
					IF IS_VEHICLE_CARGOBOB(scriptData.vehDelivered)	
						IF NOT IS_VEHICLE_SEAT_FREE(scriptData.vehDelivered)
							ENTITY_INDEX entityAttachedToCargobob
							entityAttachedToCargobob = GET_ENTITY_ATTACHED_TO_CARGOBOB(scriptData.vehDelivered)
							
							IF NOT DOES_ENTITY_EXIST(entityAttachedToCargobob)
								CLEAR_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].freemodeDelivery.iBS, BS_FREEMODE_DELIVERY_GLOBAL_PLAYER_BD_CARGOB_DROP)
								PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_TERMINATE_CHECKS - nothing is attached to the cargobob.")
								SCRIPT_CLEANUP()
							ENDIF
						ELSE
							PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_TERMINATE_CHECKS - no one is in cargobob driver seat.")
							SCRIPT_CLEANUP()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT FREEMODE_DELIVERY_IS_DELIVERABLE_VALID(scriptData.launchData.deliverableID)
		#IF IS_DEBUG_BUILD
		AND NOT g_FreemodeDeliveryData.d_bAllDropoffsAcceptEverything
		#ENDIF
			PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_TERMINATE_CHECKS - Deliverable is invalid.")
			SCRIPT_CLEANUP()
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_SPAWN_AFTER_CUTSCENE()
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBS, BS_DELIVERY_SCRIPT_PLAYER_BD_WARP_DONE)
	OR IS_ENTITY_DEAD(PLAYER_PED_ID())
	OR IS_PLAYER_CONTROL_ON(PLAYER_ID())
		EXIT
	ENDIF

	IF SHOULD_DROPOFF_USE_PRESET_SPAWN_COORDS(scriptData.launchData.eDropoffID)
		PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_SPAWN_AFTER_CUTSCENE - Getting predefined spawn data.")
		
		VECTOR vSpawnCoords
		FLOAT fSpawnHeading, fWeight

		USE_CUSTOM_SPAWN_POINTS(TRUE, FALSE, TRUE, 0.02, 2, DEFAULT, DEFAULT, 0.02)
		
		INT i
		REPEAT GET_CUSTOM_SPAWN_POINT_COUNT(scriptData.launchData.eDropoffID) i
			fWeight = GET_RANDOM_FLOAT_IN_RANGE(0.0, 1.0)
			GET_PREDEFINED_DROPOFF_SPAWN_DATA(scriptData.launchData.eDropoffID, i, vSpawnCoords, fSpawnHeading)
			ADD_CUSTOM_SPAWN_POINT(vSpawnCoords, fSpawnHeading, fWeight #IF IS_DEBUG_BUILD,0.2 #ENDIF)
			PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_SPAWN_AFTER_CUTSCENE - Added preset spawn point id ", i, " which is ", vSpawnCoords, ", ", fSpawnHeading)
		ENDREPEAT
	ELSE
		PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_SPAWN_AFTER_CUTSCENE - Getting generated spawn data.")
		
		IF NOT (g_SpawnData.bUseCustomSpawnPoints)
			IF serverBD.bSpawnPointsReady
				USE_CUSTOM_SPAWN_POINTS(TRUE, FALSE, TRUE, 0.02, 2, DEFAULT, DEFAULT, 0.02)
				
				INT i
				FLOAT fWeight
				REPEAT ServerBD.iCurrentGeneratedSpawnPoint i
					fWeight = GET_RANDOM_FLOAT_IN_RANGE(0.0, 1.0)
					ADD_CUSTOM_SPAWN_POINT(serverBD.vSpawnPoints[i], serverBD.fSpawnHeadings[i], fWeight #IF IS_DEBUG_BUILD,0.2 #ENDIF)
					PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_SPAWN_AFTER_CUTSCENE - Added spawn point id ", i, " which is ", serverBD.vSpawnPoints[i], ", ", serverBD.fSpawnHeadings[i])
				ENDREPEAT
			ELSE
				PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_SPAWN_AFTER_CUTSCENE - Waiting for server to compute spawn points.")
			ENDIF
		ENDIF
	ENDIF

	BOOL bSpawnInVehicle = IS_DROPOFF_A_HILL_CLIMB_DROPOFF(scriptData.launchData.eDropoffID)

	IF g_SpawnData.bUseCustomSpawnPoints
		
		IF bSpawnInVehicle
			MODEL_NAMES eModel = SANCHEZ
			REQUEST_MODEL(eModel)

			IF HAS_MODEL_LOADED(eModel)
			AND NOT scriptData.bSetWarpInVehicle
				SET_PLAYER_RESPAWN_IN_VEHICLE(TRUE, eModel, FALSE, FALSE, FALSE, DEFAULT, DEFAULT, DEFAULT, TRUE, TRUE)
				scriptData.bSetWarpInVehicle = TRUE
				PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_SPAWN_AFTER_CUTSCENE - SET_PLAYER_RESPAWN_IN_VEHICLE = TRUE")
			ENDIF
			
			IF scriptData.bSetWarpInVehicle
				IF WARP_TO_SPAWN_LOCATION(g_eGunrunningSpawnLocation, FALSE, TRUE, FALSE, FALSE)
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBS, BS_DELIVERY_SCRIPT_PLAYER_BD_WARP_DONE)
					SET_PLAYER_RESPAWN_IN_VEHICLE(FALSE)
					CLEAR_CUSTOM_SPAWN_POINTS()
					USE_CUSTOM_SPAWN_POINTS(FALSE)
					scriptData.bSetWarpInVehicle = FALSE
					PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_SPAWN_AFTER_CUTSCENE - Warp into vehicle finished.")
				ENDIF
			ELSE
				PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_SPAWN_AFTER_CUTSCENE - Waiting to set player respawn in vehicle.")
			ENDIF
		ELSE
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			ENDIF
		
			IF WARP_TO_SPAWN_LOCATION(g_eGunrunningSpawnLocation, FALSE, FALSE, FALSE, TRUE)
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBS, BS_DELIVERY_SCRIPT_PLAYER_BD_WARP_DONE)
				CLEAR_CUSTOM_SPAWN_POINTS()
				USE_CUSTOM_SPAWN_POINTS(FALSE)
				PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_SPAWN_AFTER_CUTSCENE - Warp finished.")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Vehicle seat is saved in BD and that's what's used to create peds in cutscene.
PROC MAINTAIN_CACHING_VEHICLE_SEAT()
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
	AND NOT IS_ENTITY_DEAD(scriptData.vehDelivered)
	AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), scriptData.vehDelivered)
		VEHICLE_SEAT vsTmp = GET_SEAT_PED_IS_IN_WITH_SAFE_DEFAULT(PLAYER_PED_ID())
		IF vsTmp != VS_ANY_PASSENGER
			IF vsTmp != playerBD[PARTICIPANT_ID_TO_INT()].vehicleSeat
				playerBD[PARTICIPANT_ID_TO_INT()].vehicleSeat = vsTmp
				PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_CACHING_VEHICLE_SEAT - Setting our seat to ", GET_VEHICLE_SEAT_FOR_DEBUG(vsTmp))
			ENDIF
			cutsceneData.iLocalPedVehSeatIndex = (ENUM_TO_INT(vsTmp) + 1)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_PALYER_CONTROLS()
	IF GET_DELIVERY_SCRIPT_STATE() > DELIVERY_STATE_READY
	AND GET_DELIVERY_SCRIPT_STATE() < DELIVERY_STATE_FINISHED_CUTSCENE
		SET_PLAYER_CONTROL(GET_PLAYER_INDEX(), FALSE)
	ENDIF
ENDPROC

PROC MAINTAIN_VEHICLE_HALT()
	IF GET_DELIVERY_SCRIPT_STATE() >= DELIVERY_STATE_READY
		IF serverBD.bCutsceneWillStart
			IF DOES_ENTITY_EXIST(scriptData.vehDelivered)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(scriptData.vehDelivered)
					scriptData.bDeliveryVehHalted = BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(scriptData.vehDelivered, DEFAULT_VEH_STOPPING_DISTANCE, 8, 0.5)
				ELSE
					IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
						NETWORK_REQUEST_CONTROL_OF_ENTITY(scriptData.vehDelivered)
					ELSE
						IF ABSF(GET_ENTITY_SPEED(scriptData.vehDelivered)) <= 0.5
							scriptData.bDeliveryVehHalted = TRUE
						ENDIF
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_VEHICLE_HALT - delivery vehicle does not exist, setting halted to true.")
				scriptData.bDeliveryVehHalted = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_STREAMING_FINISHED()
	INT i
	
	REPEAT COUNT_OF(cutsceneData.pedPlayerActors) i
		IF DOES_ENTITY_EXIST(cutsceneData.pedPlayerActors[i]) AND NOT IS_ENTITY_DEAD(cutsceneData.pedPlayerActors[i])
			IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(cutsceneData.pedPlayerActors[i])
				#IF IS_DEBUG_BUILD
					PRINTLN("[GUNRUNNING_CUTSCENE] HAS_STREAMING_FINISHED, MAINTAIN_CACHING_VEHICLE_SEAT - Waiting for pedPlayerActor[", i, "] to load.")
				#ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF DOES_ENTITY_EXIST(cutsceneData.vehActors[GUNRUNNING_DELIVERY_VEH_ARRAY_DELIVERY_CLONE])		
		IF NOT GUNRUNNING_DELIVRY_HAS_DELIVERY_VEHICLE_STREAMED(cutsceneData)
			PRINTLN("[GUNRUNNING_CUTSCENE] HAS_STREAMING_FINISHED - Vehicle mods not streamed.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	SWITCH scriptData.launchData.eCutscene
		CASE GUNRUNNING_CUTSCENE_BUNKER_CARGOBOB_ATTACHED
		CASE GUNRUNNING_CUTSCENE_BUNKER_FOOT
		CASE GUNRUNNING_CUTSCENE_BUNKER_HELI
		CASE GUNRUNNING_CUTSCENE_BUNKER_VEHICLE
			IF BUNKER_CUTSCENE_BUNKERDOOR_STREAM_LOADED(scriptData.iBS, BS_LOAD_BUNKER_ENTRY_AUDIO_STREAM)
				PRINTLN("[GUNRUNNING_CUTSCENE] HAS_STREAMING_FINISHED - Bunker door audio stream loaded.")
			ELSE
				RETURN FALSE
				PRINTLN("[GUNRUNNING_CUTSCENE] HAS_STREAMING_FINISHED - Bunker door audio stream not loaded.")
			ENDIF
		BREAK
	ENDSWITCH
	
	
	PRINTLN("[GUNRUNNING_CUTSCENE] HAS_STREAMING_FINISHED - Streaming finished.")
	RETURN TRUE
ENDFUNC

FUNC BOOL READY_TO_START_CUTSCENE()
	IF scriptData.launchData.bDeliveryViaCargobob
		IF GET_TIMER_IN_SECONDS_SAFE(stCutsceneStartDelay) >= 3
			RETURN TRUE
		ENDIF
	ELIF (scriptData.launchData.bDeliveryByProxy OR scriptData.launchData.bDeliveryInCar)
		IF scriptData.bDeliveryVehHalted
		AND GET_TIMER_IN_SECONDS_SAFE(stCutsceneStartDelay) >= CI_DELAY_BEFORE_CUTSCENE
			RETURN TRUE
		ENDIF
	ELIF scriptData.launchData.bDeliveryOnFoot
		IF GET_TIMER_IN_SECONDS_SAFE(stCutsceneStartDelay) >= 1
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
FUNC STRING DEBUG_GET_FADE_AND_MOVE_ENTITIES_STATE_NAME(DELIVERY_FADE_AND_MOVE_ENTITIES_STATE eState)
	SWITCH eState		
		CASE MES_FADE_OUT_ENTITIES		RETURN "MES_FADE_OUT_ENTITIES"
		CASE MES_WARP_VEHICLE			RETURN "MES_WARP_VEHICLE"
		CASE MES_WARP_PLAYER			RETURN "MES_WARP_PLAYER"
		CASE MES_DELETE_VEHICLE			RETURN "MES_DELETE_VEHICLE"
		CASE MES_FADE_IN_VEHICLE		RETURN "MES_FADE_IN_VEHICLE"
		CASE MES_DONE					RETURN "MES_DONE"
		CASE MES_WARP_PV				RETURN "MES_WARP_PV"
		CASE MES_WARP_PEGASUS_VEH		RETURN "MES_WARP_PEGASUS_VEH"
		CASE MES_WARP_BOSS_VEH 			RETURN "MES_WARP_BOSS_VEH"
		CASE MES_AWAIT_BOSS_VEH_FADE_IN RETURN "MES_AWAIT_BOSS_VEH_FADE_IN"
	ENDSWITCH
	
	RETURN "***unknown***"
ENDFUNC
#ENDIF

PROC SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(DELIVERY_FADE_AND_MOVE_ENTITIES_STATE eNewState)
	IF eFadeAndMoveState != eNewState
		PRINTLN("[GUNRUNNING_CUTSCENE] SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE new state = ",DEBUG_GET_FADE_AND_MOVE_ENTITIES_STATE_NAME(eNewState), " from: ", DEBUG_GET_FADE_AND_MOVE_ENTITIES_STATE_NAME(eFadeAndMoveState))
		eFadeAndMoveState = eNewState
	ENDIF
ENDPROC

FUNC BOOL IS_VEHICLE_TO_MOVE_TOWING_PERSONAL_AA_TRAILER(VEHICLE_INDEX theVeh, VEHICLE_INDEX &towedVehicle)
	towedVehicle = NULL
	IF IS_VEHICLE_ATTACHED_TO_TRAILER(theVeh)
		GET_VEHICLE_TRAILER_VEHICLE(theVeh,towedVehicle)
		IF IS_VEHICLE_A_PERSONAL_VEHICLE(towedVehicle)
		AND GET_ENTITY_MODEL(towedVehicle) = TRAILERSMALL2
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_FADE_AND_MOVE_ON_DELIVERY()

	VECTOR vPlayerPos
	BOOL bVehEntityGrabbed = FALSE
	
	IF NOT IS_ENTITY_ALIVE(PLAYER_PED_ID())
		PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - Player is dead")
		EXIT
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND IS_SVM_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
		PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - Delivering in an SVM vehicle")
		SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_GLOBAL_DATA_DONT_BRING_VEHICLE_INSIDE)
		SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(MES_DONE)
		EXIT
	ENDIF
	
	//Set our vehicle alpha to maintain local visibility
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		thisSceneMoveEntityStruct.vehVehicleToTmove = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())		
		RESET_ENTITY_ALPHA(thisSceneMoveEntityStruct.vehVehicleToTmove)
		RESET_ENTITY_ALPHA(PLAYER_PED_ID())
	ENDIF
	
	//Only start the fade once we are in a position to stream assets before starting the scene
	IF NOT IS_BIT_SET(scriptData.iBS, BS_DELIVERY_FADE_AND_HIDE_ENTITIES)
	OR IS_BIT_SET(scriptData.iBS, BS_PREVENT_FADE_FOR_MISSION_VEHICLES)
	OR (scriptData.bSpecialOnFootScene AND (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND NOT IS_VEHICLE_EMPTY(thisSceneMoveEntityStruct.vehVehicleToTmove, FALSE, TRUE, TRUE)))
		//Nothing to do just yet
		EXIT
	ENDIF
	
	//Only move the player once the scene has started
	IF GET_DELIVERY_SCRIPT_STATE() < DELIVERY_STATE_PLAYING_CUTSCENE
	AND eFadeAndMoveState != MES_FADE_OUT_ENTITIES
		PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - entities are fading out. Waiting on cutscene to start")
		EXIT
	ENDIF
	
	SWITCH eFadeAndMoveState
		
		CASE MES_FADE_OUT_ENTITIES
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				FORCE_PLAYER_LOCALLY_VISIBLE_FOR_FADE()
				NETWORK_FADE_OUT_ENTITY(PLAYER_PED_ID(), TRUE, TRUE)
			ENDIF
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				
				IF IS_ENTITY_ALIVE(thisSceneMoveEntityStruct.vehVehicleToTmove)
				
					//Save out the model for layer use
					IF thisSceneMoveEntityStruct.eVehModel = DUMMY_MODEL_FOR_SCRIPT
						thisSceneMoveEntityStruct.eVehModel = GET_ENTITY_MODEL(thisSceneMoveEntityStruct.vehVehicleToTmove)
					ENDIF
					
					VEHICLE_INDEX towedVehicle
					IF IS_VEHICLE_TO_MOVE_TOWING_PERSONAL_AA_TRAILER(thisSceneMoveEntityStruct.vehVehicleToTmove, towedVehicle)	
						VECTOR vSpawnLocation
						FLOAT fSpawnHeading
						VEHICLE_SPAWN_LOCATION_PARAMS vehicleSpawnLocationParams
						
						vehicleSpawnLocationParams.fMinDistFromCoords 		= 20
						vehicleSpawnLocationParams.fMaxDistance 			= 150
						vehicleSpawnLocationParams.bConsiderHighways 		= TRUE
						vehicleSpawnLocationParams.bCheckEntityArea 		= TRUE
						vehicleSpawnLocationParams.bCheckOwnVisibility 		= FALSE
						vehicleSpawnLocationParams.bConsiderOnlyActiveNodes = TRUE
						
						IF NOT HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(GET_ENTITY_COORDS(towedVehicle), <<0.0, 0.0, 0.0>>, GET_ENTITY_MODEL(towedVehicle), TRUE, vSpawnLocation, fSpawnHeading, vehicleSpawnLocationParams)
							PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - towing AA, not warped...")
							EXIT
						ELSE
							DETACH_VEHICLE_FROM_TRAILER(thisSceneMoveEntityStruct.vehVehicleToTmove)
							
							SET_ENTITY_COORDS_NO_OFFSET(towedVehicle, vSpawnLocation)
							SET_ENTITY_HEADING(towedVehicle, fSpawnHeading)
							
							PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - towing AA, warped to ", vSpawnLocation, "!")
						ENDIF
					ENDIF
					
					IF IS_VEHICLE_ARMORY_TRUCK(thisSceneMoveEntityStruct.vehVehicleToTmove)
					
						SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(MES_DONE)
						PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - we are in an MOC")
						EXIT
				
					ELIF NOT IS_PED_SITTING_IN_VEHICLE_SEAT(PLAYER_PED_ID(), thisSceneMoveEntityStruct.vehVehicleToTmove, VS_DRIVER)
						
						IF NOT IS_VEHICLE_A_PERSONAL_VEHICLE(thisSceneMoveEntityStruct.vehVehicleToTmove)
						OR GET_OWNER_OF_PERSONAL_VEHICLE(thisSceneMoveEntityStruct.vehVehicleToTmove) != scriptData.launchData.pOwner
							PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - Setting BS2_SIMPLE_INTERIOR_GLOBAL_DATA_DONT_BRING_VEHICLE_INSIDE.")
							SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_GLOBAL_DATA_DONT_BRING_VEHICLE_INSIDE)
						ENDIF
						
						SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(MES_DONE)
						PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - in a vehicle but not the driver.")
						EXIT
						
					ELIF GB_IS_PLAYER_DRIVING_BOSS_LIMO(PLAYER_ID())
						
						GB_SET_GLOBAL_CLIENT_BIT1(eGB_GLOBAL_CLIENT_BITSET_1_LOCAL_REQUEST_BOSS_LIMO_FADE_OUT)						
						SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_GLOBAL_DATA_DONT_BRING_VEHICLE_INSIDE)
						SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(MES_WARP_BOSS_VEH)
						thisSceneMoveEntityStruct.piBossVehOwner = GB_GET_LOCAL_PLAYER_GANG_BOSS()
						PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - Requesting boss limo be moved elsewhere. Owner is: ", GET_PLAYER_NAME(thisSceneMoveEntityStruct.piBossVehOwner))
						EXIT
						
					ELIF IS_THIS_VEHICLE_A_PEGASUS_VEHICLE(thisSceneMoveEntityStruct.vehVehicleToTmove)
						thisSceneMoveEntityStruct.iPegasusVehOwner = GET_OWNER_OF_PEGASUS_VEHICLE(thisSceneMoveEntityStruct.vehVehicleToTmove)
						SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_GLOBAL_DATA_DONT_BRING_VEHICLE_INSIDE)
						SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(MES_WARP_PEGASUS_VEH)
						PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - Moving to stage Requesting pegasus vehicle warp")
						EXIT
					ELIF NOT IS_VEHICLE_A_PERSONAL_VEHICLE(thisSceneMoveEntityStruct.vehVehicleToTmove)
						IF NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(thisSceneMoveEntityStruct.vehVehicleToTmove)
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(VEH_TO_NET(thisSceneMoveEntityStruct.vehVehicleToTmove))
								NETWORK_FADE_OUT_ENTITY(thisSceneMoveEntityStruct.vehVehicleToTmove, TRUE, TRUE)
								RESET_ENTITY_ALPHA(thisSceneMoveEntityStruct.vehVehicleToTmove)
								bVehEntityGrabbed = TRUE
							ELSE
								PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - MES_FADE_OUT_ENTITIES - Requesting control of netID")
								NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(VEH_TO_NET(thisSceneMoveEntityStruct.vehVehicleToTmove))
							ENDIF
						ELSE
							IF IS_ENTITY_A_MISSION_ENTITY(thisSceneMoveEntityStruct.vehVehicleToTmove)
							AND NOT IS_BIT_SET(scriptData.iBS, BS_GRABBING_CONTROL_OF_AMBIENT_VEH)
								SET_BIT(scriptData.iBS, BS_PREVENT_FADE_FOR_MISSION_VEHICLES)
								SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_GLOBAL_DATA_DONT_BRING_VEHICLE_INSIDE)
							ELSE
								SET_BIT(scriptData.iBS, BS_GRABBING_CONTROL_OF_AMBIENT_VEH)
								BOOL bScriptHostObject
								
								IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
									bScriptHostObject = TRUE
								ELSE
									bScriptHostObject = FALSE
								ENDIF
								
								SET_ENTITY_AS_MISSION_ENTITY(thisSceneMoveEntityStruct.vehVehicleToTmove,bScriptHostObject,TRUE)
							ENDIF
						ENDIF
						
						PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - Requesting non mission vehicle be moved elsewhere")
						SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_GLOBAL_DATA_DONT_BRING_VEHICLE_INSIDE)
					
					ELSE
						IF GET_OWNER_OF_PERSONAL_VEHICLE(thisSceneMoveEntityStruct.vehVehicleToTmove) != scriptData.launchData.pOwner
							PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - Requesting personal vehicle be moved elsewhere")
							bVehEntityGrabbed = TRUE
						ELSE
							IF IS_THIS_MODEL_ALLOWED_IN_HANGAR(GET_ENTITY_MODEL(thisSceneMoveEntityStruct.vehVehicleToTmove))
								PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - Requesting personal aircraft be moved elsewhere")
								SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_GLOBAL_DATA_DONT_BRING_VEHICLE_INSIDE)
								SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(MES_WARP_PV)
							ELSE
								SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(MES_DONE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF bVehEntityGrabbed
					IF IS_THIS_VEHICLE_A_PEGASUS_VEHICLE(thisSceneMoveEntityStruct.vehVehicleToTmove)
						SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(MES_WARP_PEGASUS_VEH)
					ELIF IS_VEHICLE_A_PERSONAL_VEHICLE(thisSceneMoveEntityStruct.vehVehicleToTmove)
						SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(MES_WARP_PV)
					ELSE
						SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(MES_WARP_VEHICLE)
					ENDIF
				ENDIF
			ELSE
				SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(MES_WARP_PLAYER) 
			ENDIF
		BREAK
		
		CASE MES_WARP_VEHICLE
						
			IF IS_ENTITY_ALIVE(thisSceneMoveEntityStruct.vehVehicleToTmove)
				IF NOT NETWORK_IS_ENTITY_FADING(thisSceneMoveEntityStruct.vehVehicleToTmove)
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(VEH_TO_NET(thisSceneMoveEntityStruct.vehVehicleToTmove))
										
						VECTOR vSpawnLocation
						FLOAT fSpawnHeading
						VEHICLE_SPAWN_LOCATION_PARAMS vehicleSpawnLocationParams
						
						vehicleSpawnLocationParams.fMinDistFromCoords 		= 20
						vehicleSpawnLocationParams.fMaxDistance 			= 150
						vehicleSpawnLocationParams.bConsiderHighways 		= TRUE
						vehicleSpawnLocationParams.bCheckEntityArea 		= TRUE
						vehicleSpawnLocationParams.bCheckOwnVisibility 		= FALSE
						vehicleSpawnLocationParams.bConsiderOnlyActiveNodes = TRUE
						
						IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(GET_ENTITY_COORDS(thisSceneMoveEntityStruct.vehVehicleToTmove), <<0.0, 0.0, 0.0>>, thisSceneMoveEntityStruct.eVehModel, TRUE, vSpawnLocation, fSpawnHeading, vehicleSpawnLocationParams)									  
							
							NETWORK_FADE_IN_ENTITY(thisSceneMoveEntityStruct.vehVehicleToTmove, TRUE)
							SET_ENTITY_COORDS_NO_OFFSET(thisSceneMoveEntityStruct.vehVehicleToTmove, vSpawnLocation)
							SET_ENTITY_HEADING(thisSceneMoveEntityStruct.vehVehicleToTmove, fSpawnHeading)
							SET_VEHICLE_ON_GROUND_PROPERLY(thisSceneMoveEntityStruct.vehVehicleToTmove)
							SET_VEHICLE_ENGINE_ON(thisSceneMoveEntityStruct.vehVehicleToTmove, FALSE, TRUE)
							
							IF IS_THIS_MODEL_A_HELI(thisSceneMoveEntityStruct.eVehModel)
								SET_HELI_BLADES_SPEED(thisSceneMoveEntityStruct.vehVehicleToTmove, 0.0)
							ENDIF
							
							PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - Moved ambient vehicle!")
							SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(MES_DONE)
							
						ENDIF
					ELSE
						PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - MES_WARP_VEHICLE - requesting control of amient vehicle")
						NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(VEH_TO_NET(thisSceneMoveEntityStruct.vehVehicleToTmove))					
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - MES_WARP_VEHICLE - vhicle no longer exists")
			ENDIF
		BREAK
		
		CASE MES_WARP_BOSS_VEH
			IF NOT GB_IS_GLOBAL_CLIENT_BIT1_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_1_LOCAL_REQUEST_BOSS_LIMO_FADE_OUT)
				
				IF thisSceneMoveEntityStruct.piBossVehOwner != INVALID_PLAYER_INDEX()
				AND NOT NETWORK_IS_ENTITY_FADING(MPGlobalsAmbience.sMagnateGangBossData.bossLimo[NATIVE_TO_INT(thisSceneMoveEntityStruct.piBossVehOwner)])
					PRINTLN("[GUNRUNNING_CUTSCENE][MAGNATE_GANG_BOSS][BOSS_LIMO] GB_REQUEST_CREATE_GANG_BOSS_LIMO - Sending request event.")
					IF IS_NET_PLAYER_OK(GB_GET_LOCAL_PLAYER_GANG_BOSS(),FALSE)
						GB_REQUEST_CREATE_GANG_BOSS_LIMO(thisSceneMoveEntityStruct.eVehModel, DEFAULT, TRUE)
						PRINTLN("[GUNRUNNING_CUTSCENE][MAGNATE_GANG_BOSS][BOSS_LIMO] GB_REQUEST_CREATE_GANG_BOSS_LIMO - Sending event to boss - ",GET_PLAYER_NAME(thisSceneMoveEntityStruct.piBossVehOwner))
					ENDIF
					
					SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(MES_AWAIT_BOSS_VEH_FADE_IN)
				ENDIF
			ENDIF
		BREAK
		
		CASE MES_AWAIT_BOSS_VEH_FADE_IN
			IF NOT HAS_NET_TIMER_STARTED(thisSceneMoveEntityStruct.stFadeInBossVehTimer)
				PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - Broadcasting fade in request!")
				BROADCAST_REQUEST_BOSS_LIMO_FADE_IN(thisSceneMoveEntityStruct.piBossVehOwner)
				START_NET_TIMER(thisSceneMoveEntityStruct.stFadeInBossVehTimer)
			ELIF HAS_NET_TIMER_EXPIRED(thisSceneMoveEntityStruct.stFadeInBossVehTimer, 3000)
				PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - Resending fade in event")
				RESET_NET_TIMER(thisSceneMoveEntityStruct.stFadeInBossVehTimer)
			ENDIF	
			
			IF NETWORK_IS_ENTITY_FADING(MPGlobalsAmbience.sMagnateGangBossData.bossLimo[NATIVE_TO_INT(thisSceneMoveEntityStruct.piBossVehOwner)])
				PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_FADE_AND_MOVE_ON_DELIVERY vehicle is now fading in!")
				RESET_NET_TIMER(thisSceneMoveEntityStruct.stFadeInBossVehTimer)
				SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(MES_DONE)
			ENDIF
		BREAK
		
		CASE MES_WARP_PLAYER
			IF NOT NETWORK_IS_ENTITY_FADING(PLAYER_PED_ID())
				vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
				vPlayerPos.z -= 10.0
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vPlayerPos)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(MES_DONE)
			ENDIF
		BREAK
		
		CASE MES_WARP_PEGASUS_VEH
			MPGlobalsAmbience.iRequestFadeOutPegasusVeh = thisSceneMoveEntityStruct.iPegasusVehOwner
			SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(MES_DONE)
			PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - Requesting pegasus vehicle warp with player ID: ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(MPGlobalsAmbience.iRequestFadeOutPegasusVeh)))
		BREAK
		
		CASE MES_WARP_PV
			BROADCAST_REQUEST_PLAYER_WARP_PV_NEAR_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_OWNER_OF_PERSONAL_VEHICLE(thisSceneMoveEntityStruct.vehVehicleToTmove))
			PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - Requesting Move other players PV")
			SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(MES_DONE)
		BREAK
		
		CASE MES_DONE
			//Do nothing
		BREAK
		
	ENDSWITCH
ENDPROC

FUNC BOOL ANY_OF_THE_GUNRUNNING_DELIVERY_MODELS(VEHICLE_INDEX delVehicle)
	INT iDeliveryModel
	MODEL_NAMES vehModel
	vehModel = GET_ENTITY_MODEL(delVehicle)
	FOR iDeliveryModel = GRV_SETUP TO GRV_CRASH_SITE_2
		IF GET_GUNRUN_ENTITY_MODEL(INT_TO_ENUM(GUNRUN_VARIATION, iDeliveryModel)) = vehModel
		OR (vehModel = POUNDER AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GUNRUNNING_DEFEND ) 
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_DELIVERY_VEHICLE_BLOCKED_FROM_CUTSCENE(VEHICLE_INDEX delVehicle)
	IF NOT DOES_ENTITY_EXIST(delVehicle)
		RETURN FALSE
	ENDIF
	
	IF IS_BIG_VEHICLE(delVehicle)
	AND NOT ANY_OF_THE_GUNRUNNING_DELIVERY_MODELS(delVehicle)
	AND NOT IS_VEHICLE_ARMORY_TRUCK(delVehicle)
		PRINTLN("[GUNRUNNING_CUTSCENE] IS_DELIVERY_VEHICLE_BLOCKED_FROM_CUTSCENE - Vehicle is big. Model: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(delVehicle)))
		RETURN TRUE
	ENDIF
	
	SWITCH GET_ENTITY_MODEL(delVehicle)
		CASE DUMP RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns true if we should send the ok to abandon veh event instead of the ok to cleanup event
FUNC BOOL SHOULD_SEND_ABANDON_VEH_EVENT()
	SWITCH GUNRUNNING_GET_MISSION_VARIATION_FROM_DROPOFF(g_FreemodeDeliveryData.eDropoffForDeliveryScript)
		CASE GRV_SELL_FOLLOW_THE_LEADER
		CASE GRV_SELL_ROUGH_TERRAIN
		CASE GRV_SELL_AMBUSHED
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_NIGHT_VISION_BE_DISABLED()
	IF GUNNRUNNING_DROPOFF_IS_PLAYER_BUNKER(g_FreemodeDeliveryData.eDropoffForDeliveryScript)
		PRINTLN("[GUNRUNNING_CUTSCENE]] SHOULD_NIGHT_VISION_BE_DISABLED - Disable NV due to bunker entry.")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_NIGHVISION_DISABLE()
	IF scriptData.cutscene.bPlaying
		NIGHTVISION_GR_DISABLE_CHECK(scriptData)
	ENDIF
	
	IF GET_DELIVERY_SCRIPT_STATE() > DELIVERY_STATE_PLAYING_CUTSCENE
		NIGHTVISION_GR_RESTORE(scriptData)
	ENDIF
ENDPROC

PROC MAINTAIN_CUTSCENE_DURATION_EVENTS()
	IF scriptData.cutscene.bPlaying
		IF scriptData.cutscene.iScenesCount > 0
			IF scriptData.cutscene.iCurrentScene = scriptData.cutscene.iScenesCount - 1
				//Allow delivery shard display
				INT lastSceneMsLeft
				lastSceneMsLeft = scriptData.cutscene.sScenes[scriptData.cutscene.iCurrentScene].iDuration - scriptData.cutscene.iCurrentSceneElapsedTime
				
				IF lastSceneMsLeft <= 1700
					IF NOT g_FreemodeDeliveryData.bDeliveryScriptAllowDeliveryShard
						PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_SCREEN_FADE_ON_CUTSCENE_END - allow delivery shard to display")
						g_FreemodeDeliveryData.bDeliveryScriptAllowDeliveryShard = TRUE
					ENDIF
				ENDIF
				
				//Before cutscene finishes start fade out.
				IF lastSceneMsLeft <= 300
					IF NOT IS_SCREEN_FADING_OUT()
					AND NOT IS_SCREEN_FADED_OUT()
						PRINTLN("[GUNRUNNING_CUTSCENE] MAINTAIN_SCREEN_FADE_ON_CUTSCENE_END - Fade out screen scene ending, scriptData.cutscene.iCurrentScene: ", scriptData.cutscene.iCurrentScene, 
						" scriptData.cutscene.iScenesCount: ", scriptData.cutscene.iScenesCount,
						" currentSceneTime: ", scriptData.cutscene.iCurrentSceneTimer, 
						" Duration: ", scriptData.cutscene.sScenes[scriptData.cutscene.iCurrentScene].iDuration)
						IF NOT bPhantomDeliveryViaCargobob
							DO_SCREEN_FADE_OUT(250)
						ENDIF
					ELSE
						PRINTLN("[GUNRUNNING_CUTSCENE] DELIVERY_STATE_PLAYING_CUTSCENE - Screenis already fading. currentSceneTime: ", scriptData.cutscene.iCurrentSceneElapsedTime, " Duration: ", scriptData.cutscene.sScenes[scriptData.cutscene.iCurrentScene].iDuration)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_LOCAL_PLAYER_DELIVERING_TO_BUNKER_IN_OWNERS_MOC()
	IF GUNNRUNNING_DROPOFF_IS_PLAYER_BUNKER(g_FreemodeDeliveryData.eDropoffForDeliveryScript)
	AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF IS_VEHICLE_A_PERSONAL_TRUCK(veh)
		AND GET_OWNER_OF_ARMORY_TRUCK(veh) = scriptData.launchData.pOwner
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_MOVE_DELIVERY_VEHICLE_UNDER_MAP()

	IF scriptData.bSpecialOnFootScene
		PRINTLN("[GUNRUNNING_CUTSCENE] SHOULD_MOVE_DELIVERY_VEHICLE_UNDER_MAP - No we are doing bSpecialOnFootScene")
		RETURN FALSE
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF IS_VEHICLE_A_PERSONAL_VEHICLE(scriptData.vehDelivered)
		AND GET_OWNER_OF_PERSONAL_VEHICLE(thisSceneMoveEntityStruct.vehVehicleToTmove) = scriptData.launchData.pOwner
			IF IS_THIS_MODEL_ALLOWED_IN_HANGAR(GET_ENTITY_MODEL(thisSceneMoveEntityStruct.vehVehicleToTmove))
				PRINTLN("[GUNRUNNING_CUTSCENE] SHOULD_MOVE_DELIVERY_VEHICLE_UNDER_MAP - No IS_THIS_MODEL_ALLOWED_IN_HANGAR = TRUE")
				RETURN FALSE
			ENDIF
			
			RETURN TRUE
		ENDIF
		
		IF IS_LOCAL_PLAYER_DELIVERING_TO_BUNKER_IN_OWNERS_MOC()
			IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
			AND PLAYER_ID() = scriptData.launchData.pOwner
			OR GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) != VS_DRIVER
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC RUN_MAIN_CLIENT_LOGIC()
	MAINTAIN_CACHING_VEHICLE_SEAT()
	MAINTAIN_PALYER_CONTROLS()
	MAINTAIN_VEHICLE_HALT()
	MAINTAIN_FADE_AND_MOVE_ON_DELIVERY()
	
	IF GET_DELIVERY_SCRIPT_STATE() = DELIVERY_STATE_INITIAL
		REQUEST_ALL_THE_ASSETS()
		SET_DELIVERY_SCRIPT_STATE(DELIVERY_STATE_LOADING)
	ENDIF

	IF GET_DELIVERY_SCRIPT_STATE() = DELIVERY_STATE_LOADING
		IF HAVE_ALL_THE_ASSETS_LOADED()
		AND (CALL scriptData.cutsceneInterface.funcLoader(scriptData.cutscene, cutsceneData))
			SET_DELIVERY_SCRIPT_STATE(DELIVERY_STATE_READY)
		ENDIF
	ENDIF
	
	//WAIT UNTIL DELIVERY IS VALID, CREATE ASSETS
	IF GET_DELIVERY_SCRIPT_STATE() = DELIVERY_STATE_READY
		MAINTAIN_TERMINATE_CHECKS()
		
		#IF IS_DEBUG_BUILD
		IF debugData.bCamProbing
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			cutsceneData.bCamProbing = TRUE
			CALL scriptData.cutsceneInterface.funcLoader(scriptData.cutscene, cutsceneData)
		ENDIF
		#ENDIF
		
		IF SHOULD_CUTSCENE_BE_STARTED()
		#IF IS_DEBUG_BUILD
		AND NOT debugData.bCamProbing
		#ENDIF
			BOOL bVehicleReady = FALSE
			PRINTLN("[GUNRUNNING_CUTSCENE]] DELIVERY_STATE_READY - Cutscene start is ok.")
			IF NOT IS_ENTITY_DEAD(scriptData.vehDelivered)
				cutsceneData.fBaseHeading = GET_ENTITY_HEADING(scriptData.vehDelivered)
			ENDIF
			
			IF GUNNRUNNING_DROPOFF_IS_PLAYER_BUNKER(g_FreemodeDeliveryData.eDropoffForDeliveryScript)
				SET_BIT(scriptData.iBS, BS_DELIVERY_FADE_AND_HIDE_ENTITIES)
			ENDIF
			
			//Only check for incompatible entries on vehicle entry, other missions have compatible vehicles.
			IF scriptData.launchData.eCutscene = GUNRUNNING_CUTSCENE_BUNKER_VEHICLE
				IF DOES_ENTITY_EXIST(scriptData.vehDelivered)
					IF IS_DELIVERY_VEHICLE_BLOCKED_FROM_CUTSCENE(scriptData.vehDelivered)
						PRINTLN("[GUNRUNNING_CUTSCENE]] DELIVERY_STATE_READY - Delivery vehicle is blocked for cutscene.")
						SET_DELIVERY_SCRIPT_STATE(DELIVERY_STATE_SKIP_CUTSCENE)
						EXIT
					ELSE
						PRINTLN("[GUNRUNNING_CUTSCENE]] DELIVERY_STATE_READY - Delivery vehicle is ok for cutscene.")
					ENDIF
				ELSE
					PRINTLN("[GUNRUNNING_CUTSCENE]] DELIVERY_STATE_READY - Not in a vehicle not checking if veh is ok for cutscene.")
				ENDIF
			ENDIF
			
			//Clone Delivery vehicle in 1 place...
			SWITCH scriptData.launchData.eCutscene
				//Missions that dont require palyers to be in the cloned vehicle.
				CASE GUNRUNNING_CUTSCENE_BUNKER_FOOT
					//Dont create a vehicle clone
					
					IF scriptData.bSpecialOnFootScene							
						bVehicleReady = GUNRUNNING_DELIVRY_CREATE_DELIVERY_VEHICLE_CLONE(cutsceneData)
					ENDIF
				BREAK
				
				CASE GUNRUNNING_CUTSCENE_HILL_CLIMB
				CASE GUNRUNNING_CUTSCENE_PHANTOM
					bVehicleReady = GUNRUNNING_DELIVRY_CREATE_DELIVERY_VEHICLE_CLONE(cutsceneData, FALSE)
				BREAK
				
				DEFAULT
					bVehicleReady = GUNRUNNING_DELIVRY_CREATE_DELIVERY_VEHICLE_CLONE(cutsceneData)
				BREAK
			ENDSWITCH
			
			IF bVehicleReady
				GUNRUNNING_DELIVRY_CREATE_DELIVERY_VEHICLE_CRATE(scriptData, cutsceneData)
				
				CALL scriptData.cutsceneInterface.procStart(scriptData.cutscene, cutsceneData)
				TOGGLE_SCRIPT_ASSETS_READY(FALSE)
				//Timer for min time before jumping to cutscene
				IF NOT IS_TIMER_STARTED(stCutsceneStartDelay)
					START_TIMER_NOW(stCutsceneStartDelay)
				ENDIF
				
				g_FreemodeDeliveryData.bDeliveryScriptStreamingAssets = TRUE
				SET_DELIVERY_SCRIPT_STATE(DELIVERY_STATE_STREAMING)
			ELIF scriptData.launchData.bDeliveryOnFoot
			AND NOT scriptData.bSpecialOnFootScene
				CALL scriptData.cutsceneInterface.procStart(scriptData.cutscene, cutsceneData)
				TOGGLE_SCRIPT_ASSETS_READY(FALSE)
				//Timer for min time before jumping to cutscene
				IF NOT IS_TIMER_STARTED(stCutsceneStartDelay)
					START_TIMER_NOW(stCutsceneStartDelay)
				ENDIF
				
				g_FreemodeDeliveryData.bDeliveryScriptStreamingAssets = TRUE
				SET_DELIVERY_SCRIPT_STATE(DELIVERY_STATE_STREAMING)
			ENDIF
		ENDIF
	ENDIF
	
	IF GET_DELIVERY_SCRIPT_STATE() = DELIVERY_STATE_STREAMING	
		MAINTAIN_TERMINATE_CHECKS()
		
		IF NOT IS_TIMER_STARTED(stStreamFailSafe)
			START_TIMER_NOW(stStreamFailSafe)
		ENDIF
	
		IF (HAS_STREAMING_FINISHED() OR GET_TIMER_IN_SECONDS_SAFE(stStreamFailSafe) >= CI_STREAM_FAILSAFE_TIME)
		AND READY_TO_START_CUTSCENE()
			IF IS_TIMER_STARTED(stCutsceneStartDelay)
				CANCEL_TIMER(stCutsceneStartDelay)
			ENDIF
			PRINTLN("[GUNRUNNING_CUTSCENE] RUN_MAIN_CLIENT_LOGIC - Streaming finished for the delivery cutscene. Streaming finished: ", HAS_STREAMING_FINISHED(), " Timer? ", GET_TIMER_IN_SECONDS_SAFE(stStreamFailSafe) >= CI_STREAM_FAILSAFE_TIME)
			SET_DELIVERY_SCRIPT_STATE(DELIVERY_STATE_PLAYING_CUTSCENE)
			TOGGLE_NO_LOADING_SCREEN(scriptData, TRUE)			
			START_DELIVERY_CUTSCENE()
			
			IF scriptData.launchData.eCutscene = GUNRUNNING_CUTSCENE_BUNKER_VEHICLE
			OR scriptData.launchData.eCutscene = GUNRUNNING_CUTSCENE_BUNKER_FOOT
				BUNKER_DOOR_IPL_TOGGLE_FORCE_HIDE(TRUE, TRUE)
			ENDIF
			
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
			ENDIF
			
			g_FreemodeDeliveryData.bDeliveryScriptStreamingAssets = FALSE
		ELSE
			PRINTLN("[GUNRUNNING_CUTSCENE] RUN_MAIN_CLIENT_LOGIC - Stuck waiting for assets to load. streaming finished: ", HAS_STREAMING_FINISHED(), " Ready? ", READY_TO_START_CUTSCENE())
		ENDIF
	ENDIF
	
	IF GET_DELIVERY_SCRIPT_STATE() = DELIVERY_STATE_PLAYING_CUTSCENE
		IF scriptData.cutscene.bPlaying
			IF NOT IS_BIT_SET(scriptData.iBS, BS_DELIVERY_SCRIPT_UNFREEZE_ASSETS)
				TOGGLE_SCRIPT_ASSETS_READY(TRUE)
				SET_BIT(scriptData.iBS, BS_DELIVERY_SCRIPT_UNFREEZE_ASSETS)
			ENDIF
		ENDIF
		
		//If bunker delivery do not return controlls interior script will handle this, otherwise player control is returned and loading screen is set to be hiden, which disabled the delivery shard.
		SIMPLE_CUTSCENE_MAINTAIN(scriptData.cutscene)
		CUTSCE_HELP_MAINTAIN_PED_ARRAY_FREEMODE_CUTSCENE(cutsceneData.pedPlayerActors)
		CALL scriptData.cutsceneInterface.procMaintain(scriptData.cutscene, cutsceneData)
		MAINTAIN_CUTSCENE_DURATION_EVENTS()
				
		//Mission finished.
		IF HAS_EVERYONE_STARTED_DELIVERY_CUTSCENE()
		OR NOT scriptData.cutscene.bPlaying
			IF NOT scriptData.bRepositionPlayer
				IF NOT IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].freemodeDelivery.iBS, BS_FREEMODE_DELIVERY_GLOBAL_PLAYER_BD_CARGOB_DROP)
					IF SHOULD_MOVE_DELIVERY_VEHICLE_UNDER_MAP()
						IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
							SET_ENTITY_COORDS_NO_OFFSET(scriptData.vehDelivered, GET_ENTITY_COORDS(scriptData.vehDelivered) - <<0.0, 0.0, 15.0>>)
							FREEZE_ENTITY_POSITION(scriptData.vehDelivered, TRUE)
							PRINTLN("[GUNRUNNING_CUTSCENE] DELIVERY_STATE_PLAYING_CUTSCENE - Vehicle repositioned under map, when everyone is playing and still in the vehicle(about to be deleted.")
						ENDIF
					ELSE
						SET_ENTITY_COORDS_NO_OFFSET(PLAYER_PED_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()) - <<0.0, 0.0, 15.0>>)
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
						PRINTLN("[GUNRUNNING_CUTSCENE] DELIVERY_STATE_PLAYING_CUTSCENE - Player repositioned under map, when everyone is playing and still in the vehicle(about to be deleted.")
					ENDIF
				ELSE
					PRINTLN("[GUNRUNNING_CUTSCENE] DELIVERY_STATE_PLAYING_CUTSCENE - Player not repositioned for cargobob delivery. setting bPhantomDeliveryViaCargobob = TRUE")
					bPhantomDeliveryViaCargobob = TRUE
				ENDIF
				
				scriptData.bRepositionPlayer = TRUE
			ENDIF
			
			IF NOT scriptData.bDoneFmDeliveryBroadcast
				FREEMODE_DELIVERY_START_DELIVERY(g_FreemodeDeliveryData.eDropoffForDeliveryScript, GB_GET_LOCAL_PLAYER_GANG_BOSS(), FALSE, FALSE)
				PRINTLN("[GUNRUNNING_CUTSCENE] DELIVERY_STATE_PLAYING_CUTSCENE - FREEMODE_DELIVERY_START_DELIVERY")
				IF FREEMODE_DELIVERY_IS_DELIVERABLE_VALID(FREEMODE_DELIVERY_GET_DELIVERABLE_PLAYER_POSSESSES(PLAYER_ID()))					
					
					IF SHOULD_SEND_ABANDON_VEH_EVENT()
						PRINTLN("[GUNRUNNING_CUTSCENE] DELIVERY_STATE_PLAYING_CUTSCENE - FREEMODE_DELIVERY_IS_DELIVERABLE_VALID - Sending ok to kick event")
						BROADCAST_OK_TO_KICK_PLAYERS_FROM_DELIVERABLE_ENTITY(FREEMODE_DELIVERY_GET_DELIVERABLE_PLAYER_POSSESSES(PLAYER_ID()))
					ELSE
						PRINTLN("[GUNRUNNING_CUTSCENE] DELIVERY_STATE_PLAYING_CUTSCENE - FREEMODE_DELIVERY_IS_DELIVERABLE_VALID - Sending ok to cleanup event")
						BROADCAST_OK_TO_CLEANUP_DELIVERABLE_ENTITY(FREEMODE_DELIVERY_GET_DELIVERABLE_PLAYER_POSSESSES(PLAYER_ID()))
					ENDIF
					
				ENDIF
				scriptData.bDoneFmDeliveryBroadcast = TRUE
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			IF GET_FRAME_COUNT() % 30 = 0
			PRINTLN("[GUNRUNNING_CUTSCENE] DELIVERY_STATE_PLAYING_CUTSCENE - EveryoneStarted? ", HAS_EVERYONE_STARTED_DELIVERY_CUTSCENE(), " cutscenePlaying? ", scriptData.cutscene.bPlaying)
			ENDIF
			#ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(scriptData.iBS, BS_DELIVERY_SCRIPT_STARTED_DELIVERY)
			IF HAS_EVERYONE_STARTED_DELIVERY_CUTSCENE()
				PRINTLN("[GUNRUNNING_CUTSCENE] DELIVERY_STATE_PLAYING_CUTSCENE - Everyone started delivery cutscene, set BS_DELIVERY_SCRIPT_STARTED_DELIVERY")
				SET_BIT(scriptData.iBS, BS_DELIVERY_SCRIPT_STARTED_DELIVERY)
			ENDIF
		ENDIF
		
		IF NOT scriptData.cutscene.bPlaying
			SET_DELIVERY_SCRIPT_STATE(DELIVERY_STATE_WARP)
		ENDIF
	ENDIF
	
	IF GET_DELIVERY_SCRIPT_STATE() = DELIVERY_STATE_SKIP_CUTSCENE
		IF NOT IS_TIMER_STARTED(stWaitDeliveryMessage)
			START_TIMER_NOW(stWaitDeliveryMessage)
		ENDIF
		
		IF NOT scriptData.bDoneFmDeliveryBroadcast
			PRINTLN("[GUNRUNNING_CUTSCENE] DELIVERY_STATE_SKIP_CUTSCENE - bDoneFmDeliveryBroadcast.")
			FREEMODE_DELIVERY_START_DELIVERY(g_FreemodeDeliveryData.eDropoffForDeliveryScript, GB_GET_LOCAL_PLAYER_GANG_BOSS(), FALSE, FALSE)
			scriptData.bDoneFmDeliveryBroadcast = TRUE
		ENDIF
		
		IF GET_TIMER_IN_SECONDS_SAFE(stWaitDeliveryMessage) >= CI_WAIT_UNTIL_DELIVERY_DISAPLYED
			IF NOT IS_SCREEN_FADING_OUT()
			AND NOT IS_SCREEN_FADED_OUT()
				PRINTLN("[GUNRUNNING_CUTSCENE] DELIVERY_STATE_SKIP_CUTSCENE - Fading screen out, skipping cutscene.")
				DO_SCREEN_FADE_OUT(250)
			ELSE
				PRINTLN("[GUNRUNNING_CUTSCENE] DELIVERY_STATE_SKIP_CUTSCENE - Screen is already fading for cutscene skip.")
			ENDIF
			
			IF NOT IS_SCREEN_FADING_OUT()
			AND IS_SCREEN_FADED_OUT()
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBS, BS_DELIVERY_SCRIPT_PLAYER_BD_SKIP_CUTSCENE)
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBS, BS_DELIVERY_SCRIPT_PLAYER_BD_SKIP_CUTSCENE)	
				ENDIF
			ENDIF
			
			IF HAS_EVERYONE_FADED_SCREEN_FOR_CUTSCENE_SKIP()
				PRINTLN("[GUNRUNNING_CUTSCENE] DELIVERY_STATE_SKIP_CUTSCENE - everyone is ok for cutscene skip.")
				

				
				IF NOT IS_BIT_SET(scriptData.iBS, BS_DELIVERY_SCRIPT_STARTED_DELIVERY)
					PRINTLN("[GUNRUNNING_CUTSCENE] DELIVERY_STATE_SKIP_CUTSCENE - FREEMODE_DELIVERY_START_DELIVERY")
					IF FREEMODE_DELIVERY_IS_DELIVERABLE_VALID(FREEMODE_DELIVERY_GET_DELIVERABLE_PLAYER_POSSESSES(PLAYER_ID()))
						IF SHOULD_SEND_ABANDON_VEH_EVENT()
							PRINTLN("[GUNRUNNING_CUTSCENE] DELIVERY_STATE_PLAYING_CUTSCENE - FREEMODE_DELIVERY_IS_DELIVERABLE_VALID - Sending ok to kick event")
							BROADCAST_OK_TO_KICK_PLAYERS_FROM_DELIVERABLE_ENTITY(FREEMODE_DELIVERY_GET_DELIVERABLE_PLAYER_POSSESSES(PLAYER_ID()))
						ELSE
							PRINTLN("[GUNRUNNING_CUTSCENE] DELIVERY_STATE_PLAYING_CUTSCENE - FREEMODE_DELIVERY_IS_DELIVERABLE_VALID - Sending ok to cleanup event")
							BROADCAST_OK_TO_CLEANUP_DELIVERABLE_ENTITY(FREEMODE_DELIVERY_GET_DELIVERABLE_PLAYER_POSSESSES(PLAYER_ID()))
						ENDIF
					ENDIF				
				
					PRINTLN("[GUNRUNNING_CUTSCENE] DELIVERY_STATE_SKIP_CUTSCENE - Everyone started skip cutscene, set BS_DELIVERY_SCRIPT_STARTED_DELIVERY")
					SET_BIT(scriptData.iBS, BS_DELIVERY_SCRIPT_STARTED_DELIVERY)
				ENDIF
				
				IF NOT IS_PLAYER_DATA_DOING_INTERIOR_WARP_FROM_CUTSCENE()
					SET_PLAYER_DATA_DOING_INTERIOR_WARP_FROM_CUTSCENE(TRUE)
				ENDIF
				
				SET_DELIVERY_SCRIPT_STATE(DELIVERY_STATE_SKIP_CUTSCENE_WAIT_WARP)
			ELSE
				PRINTLN("[GUNRUNNING_CUTSCENE] DELIVERY_STATE_SKIP_CUTSCENE - not everyone is ready for skip yet.")
			ENDIF
		ENDIF
	ENDIF
	
	IF GET_DELIVERY_SCRIPT_STATE() = DELIVERY_STATE_SKIP_CUTSCENE_WAIT_WARP
		IF NOT IS_TIMER_STARTED(stVehicleFadeFailSafe)
			PRINTLN("[GUNRUNNING_CUTSCENE] DELIVERY_STATE_SKIP_CUTSCENE - started fail safe timer")
			START_TIMER_NOW(stVehicleFadeFailSafe)
		ENDIF
				
		IF eFadeAndMoveState = MES_DONE
		OR GET_TIMER_IN_SECONDS_SAFE(stVehicleFadeFailSafe) >= CI_FAILSAFE_VEHICLE_FADE
			PRINTLN("[GUNRUNNING_CUTSCENE] DELIVERY_STATE_SKIP_CUTSCENE - eFadeAndMoveState: ", ENUM_TO_INT(eFadeAndMoveState), " stVehicleFadeFailSafe: ", GET_TIMER_IN_SECONDS_SAFE(stVehicleFadeFailSafe))
			SET_DELIVERY_SCRIPT_STATE(DELIVERY_STATE_FINISHED_CUTSCENE)
		ENDIF
	ENDIF
	
	IF GET_DELIVERY_SCRIPT_STATE() = DELIVERY_STATE_SCENE_FAILED
	
		IF NOT HAS_EVERYONE_STARTED_DELIVERY_CUTSCENE()
			PRINTLN("[GUNRUNNING_CUTSCENE] RUN_MAIN_CLIENT_LOGIC - DELIVERY_STATE_SCENE_FAILED waiting on all players to either start or fail scene!")
			EXIT
		ENDIF
		
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
		
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_VEHICLE) != PERFORMING_TASK
				PRINTLN("[GUNRUNNING_CUTSCENE] RUN_MAIN_CLIENT_LOGIC - DELIVERY_STATE_SCENE_FAILED - Tasking ped to leave vehicle.")				
				FREEMODE_DELIVERY_START_DELIVERY(g_FreemodeDeliveryData.eDropoffForDeliveryScript, GB_GET_LOCAL_PLAYER_GANG_BOSS(), FALSE, FALSE)
				TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
			ENDIF
			
			SET_BIT(scriptData.iBS, BS_SCENE_FAILED_IN_VEHICLE)
			EXIT
		ELIF IS_BIT_SET(scriptData.iBS, BS_SCENE_FAILED_IN_VEHICLE)
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_VEHICLE) = PERFORMING_TASK
			OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_VEHICLE) = WAITING_TO_START_TASK
				PRINTLN("[GUNRUNNING_CUTSCENE] RUN_MAIN_CLIENT_LOGIC - DELIVERY_STATE_SCENE_FAILED waiting on ped leaving vehicle")
				//Wait
				EXIT
			ENDIF
		ENDIF

		PRINTLN("[GUNRUNNING_CUTSCENE] RUN_MAIN_CLIENT_LOGIC - DELIVERY_STATE_SCENE_FAILED player is not in a vehicle - moving on. task status: ", GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_VEHICLE))
		
		IF NOT IS_BIT_SET(scriptData.iBS, BS_SCENE_FAILED_IN_VEHICLE)
		
			IF SHOULD_SEND_ABANDON_VEH_EVENT()
				PRINTLN("[GUNRUNNING_CUTSCENE] DELIVERY_STATE_PLAYING_CUTSCENE - FREEMODE_DELIVERY_IS_DELIVERABLE_VALID - Sending ok to kick event")
				BROADCAST_OK_TO_KICK_PLAYERS_FROM_DELIVERABLE_ENTITY(FREEMODE_DELIVERY_GET_DELIVERABLE_PLAYER_POSSESSES(PLAYER_ID()))
			ELSE
				PRINTLN("[GUNRUNNING_CUTSCENE] DELIVERY_STATE_PLAYING_CUTSCENE - FREEMODE_DELIVERY_IS_DELIVERABLE_VALID - Sending ok to cleanup event")
				BROADCAST_OK_TO_CLEANUP_DELIVERABLE_ENTITY(FREEMODE_DELIVERY_GET_DELIVERABLE_PLAYER_POSSESSES(PLAYER_ID()))
			ENDIF
			PRINTLN("[GUNRUNNING_CUTSCENE] RUN_MAIN_CLIENT_LOGIC - Scene failed on foot, Set as Delivered.")
		ELIF FREEMODE_DELIVERY_IS_DELIVERABLE_VALID(scriptData.launchData.deliverableID)
		AND NOT GUNNRUNNING_DROPOFF_IS_PLAYER_BUNKER(g_FreemodeDeliveryData.eDropoffForDeliveryScript)
			PRINTLN("[GUNRUNNING_CUTSCENE] RUN_MAIN_CLIENT_LOGIC - Scene failed int vehicle, cleanup deliverable.")
			BROADCAST_OK_TO_CLEANUP_DELIVERABLE_ENTITY(scriptData.launchData.deliverableID)
		ENDIF
		
		SET_DELIVERY_SCRIPT_STATE(DELIVERY_STATE_FINISHED_CUTSCENE)
	ENDIF
	
	// NOTE: Spawn warp moved to own state to allow players to spawn on vehicles 
	// which cannot take place during the cutscene.
	IF GET_DELIVERY_SCRIPT_STATE() = DELIVERY_STATE_WARP
		
		IF GUNNRUNNING_DROPOFF_IS_PLAYER_BUNKER(g_FreemodeDeliveryData.eDropoffForDeliveryScript)
		OR bPhantomDeliveryViaCargobob
			//No need for warping for bunker dropoffs as this will be handeled by a warp into the bunker
			PRINTLN("[GUNRUNNING_CUTSCENE] DELIVERY_STATE_PLAYING_CUTSCENE - Delivery point is a Bunker or bPhantomDeliveryViaCargobob? ", bPhantomDeliveryViaCargobob)
			SET_DELIVERY_SCRIPT_STATE(DELIVERY_STATE_FINISHED_CUTSCENE)
			EXIT
		ENDIF
		
		MAINTAIN_SPAWN_AFTER_CUTSCENE()
		
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBS, BS_DELIVERY_SCRIPT_PLAYER_BD_WARP_DONE)
			IF NOT scriptData.cutscene.bPlaying
				IF NOT IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_OUT(250)
				ENDIF
			ENDIF
		ELSE
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(250)
			ENDIF
		
			SET_DELIVERY_SCRIPT_STATE(DELIVERY_STATE_FINISHED_CUTSCENE)	
		ENDIF
	ENDIF
		
	IF GET_DELIVERY_SCRIPT_STATE() = DELIVERY_STATE_FINISHED_CUTSCENE
		PRINTLN("[BLKSCR] - [GUNRUNNING_CUTSCENE] RUN_MAIN_CLIENT_LOGIC - Cutscene finished.")
		CALL scriptData.cutsceneInterface.procDestructor(scriptData.cutscene, cutsceneData)
		
		IF GUNNRUNNING_DROPOFF_IS_PLAYER_BUNKER(g_FreemodeDeliveryData.eDropoffForDeliveryScript)
			DELIVERY_TRIGGER_ENTRY_TO_SIMPLE_INTERIOR(g_FreemodeDeliveryData.eDropoffForDeliveryScript, ScriptData.launchData.pOwner)			
			SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_PREVENT_WARP_PV_NEARBY)
			SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_PREVENT_BIG_MESSAGE_DISABLE)			
			FREEMODE_DELIVERY_SCRIPT_PREVENT_DELAY(TRUE)
			
			IF IS_LOCAL_PLAYER_DELIVERING_TO_BUNKER_IN_OWNERS_MOC()
				PRINTLN("[BLKSCR] - [GUNRUNNING_CUTSCENE] RUN_MAIN_CLIENT_LOGIC - Cutscene finished - Starting bunker entry in .")
				SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_BUNKER_FINISHED_DELIVERY_MISSION_USING_MOC)
			ENDIF
			
			PRINTLN("[BLKSCR] - [GUNRUNNING_CUTSCENE] RUN_MAIN_CLIENT_LOGIC - Cutscene finished - Starting bunker entry.")
		ENDIF
		
		IF bPhantomDeliveryViaCargobob
			CLEAR_PED_TASKS(PLAYER_PED_ID())
		ENDIF
		
		SCRIPT_CLEANUP()	
	ENDIF
	
	MAINTAIN_NIGHVISION_DISABLE()
ENDPROC

PROC RUN_MAIN_SERVER_LOGIC()
	MAINTAIN_GENERATING_CUSTOM_SPAWN_POINTS()
ENDPROC

#IF IS_DEBUG_BUILD
PROC CREATE_DEBUG_WIDGETS()
	START_WIDGET_GROUP("GB_GUNRUNNING_DELIVERY")
		ADD_WIDGET_BOOL("Cam Probing", debugData.bCamProbing)
		ADD_WIDGET_BOOL("Create spawn points", debugData.bAddSpawnPoints)
		ADD_WIDGET_BOOL("Kill script", debugData.bKillThisScript)
		ADD_WIDGET_BOOL("Start cutscene", debugData.bTriggerCutscene)
		ADD_WIDGET_BOOL("Force use backup Cam", cutsceneData.bForceUseBackupCam)
	STOP_WIDGET_GROUP()
ENDPROC

PROC UPDATE_DEBUG_WIDGETS()
	CALL scriptData.cutsceneInterface.procMaintainDebug(scriptData.cutscene, cutsceneData)
	
	INT i
	IF debugData.bAddSpawnPoints
		IF serverBD.bSpawnPointsReady
			
			USE_CUSTOM_SPAWN_POINTS(TRUE)
			
			REPEAT COUNT_OF(serverBD.vSpawnPoints) i
				ADD_CUSTOM_SPAWN_POINT(serverBD.vSpawnPoints[i], serverBD.fSpawnHeadings[i], 0.0 #IF IS_DEBUG_BUILD,0.2 #ENDIF)
			ENDREPEAT
			
			debugData.bAddSpawnPoints = FALSE
		ENDIF
	ENDIF
	
	IF IS_DEBUG_KEY_JUST_RELEASED(KEY_F9, KEYBOARD_MODIFIER_CTRL, "killing script")
		debugData.bKillThisScript = TRUE
	ENDIF
	
	IF IS_DEBUG_KEY_JUST_RELEASED(KEY_F10, KEYBOARD_MODIFIER_CTRL, "start cutscene")
		debugData.bTriggerCutscene = TRUE
	ENDIF
	
	IF debugData.bKillThisScript
		SCRIPT_CLEANUP()
	ENDIF
ENDPROC
#ENDIF


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                      //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT  (FREEMODE_DELIVERY_SCRIPT_LAUNCH_DATA launchData) 
	
	
		IF NETWORK_IS_GAME_IN_PROGRESS() 
			SCRIPT_INITIALISE(launchData)
			#IF IS_DEBUG_BUILD
				CREATE_DEBUG_WIDGETS()
			#ENDIF
		ELSE
			SCRIPT_CLEANUP()
		ENDIF
		
		WHILE TRUE

			MP_LOOP_WAIT_ZERO()
					
			IF SHOULD_THIS_FREEMODE_DELIVERY_SCRIPT_TERMINATE()
				PRINTLN("[GUNRUNNING_CUTSCENE] SHOULD_THIS_DELIVERY_SCRIPT_TERMINATE returns TRUE")
				SCRIPT_CLEANUP()
			ENDIF
			
			RUN_MAIN_CLIENT_LOGIC()
			
			// -----------------------------------
			// Process server game logic		
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				RUN_MAIN_SERVER_LOGIC()
			ENDIF
			
			#IF IS_DEBUG_BUILD
				UPDATE_DEBUG_WIDGETS()
			#ENDIF
			
		ENDWHILE
	

ENDSCRIPT
