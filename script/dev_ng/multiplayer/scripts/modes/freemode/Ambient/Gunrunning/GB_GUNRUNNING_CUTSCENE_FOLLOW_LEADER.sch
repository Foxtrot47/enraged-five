//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name: gb_gunrunning_cutscene_FOLLOW_THE_LEADER.sc																//
// Description: Script defining single cutscene for move weapons sell mission.								//
//				This script always needs to define:															//
//																											//
//				CONSTRUCT_[NAME]_CUTSCENE																	//
//				HAS_[NAME]_CUTSCENE_INITIALISED																//
//				START_[NAME]_CUTSCENE																		//
//				MAINTAIN_[NAME]_CUTSCENE																	//
//				DESTRUCT_[NAME]_CUTSCENE																	//
//				MAINTAIN_[NAME]_CUTSCENE_DEBUG																//
//																											//
// Written by:  Tymon																						//
// Date:  		07/02/2017																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "net_simple_cutscene.sch"

USING "net_simple_cutscene_interior.sch"
USING "net_freemode_delivery_common.sch"

// Vectors
CONST_INT FOLLOW_THE_LEADER_VEC_CAM_POINTS_COUNT		18

// Ints
CONST_INT FOLLOW_THE_LEADER_INT_SHAPETEST_ID			0

// Floats
CONST_INT FOLLOW_THE_LEADER_FLOAT_CAM_DIST				0
CONST_INT FOLLOW_THE_LEADER_FLOAT_CAM_HEIGHT			1
CONST_INT FOLLOW_THE_LEADER_FLOAT_TARGET_HEADING		2
CONST_INT FOLLOW_THE_LEADER_FLOAT_DOOR_OPEN				3

// BS
CONST_INT FOLLOW_THE_LEADER_BS_OK_CAM_POINTS				0
CONST_INT FOLLOW_THE_LEADER_BS_CUTSCENE						1
CONST_INT BS_FOLLOW_THE_LEADER_CUTSCENE_TASKED_LEAVE		0 // Peds were tasked to leave
CONST_INT BS_FOLLOW_THE_LEADER_CUTSCENE_TASKED_GOTO			1 // Peds tasked to go to coords while closing the back door of insurgent
CONST_INT BS_FOLLOW_THE_LEADER_CUTSCENE_BACK_DOOR_CLOSED	2 // Back door of insurgent closed
CONST_INT BS_FOLLOW_THE_LEADER_CUTSCENE_PED_MOVED			3 // Ped moved to the back of insurgent
CONST_INT BS_FOLLOW_THE_LEADER_CUTSCENE_GROUND_VEHICLE		4 // Ped moved to the back of insurgent

// Models
CONST_INT FOLLOW_THE_LEADER_MODEL_INSURGENT			0
CONST_INT FOLLOW_THE_LEADER_MODEL_CRATE				1

// Vehicles
//CONST_INT FOLLOW_THE_LEADER_VEH_DELIVERED			0
CONST_INT FOLLOW_THE_LEADER_VEH_CLONE				0

// Props
CONST_INT FOLLOW_THE_LEADER_PROP_CRATE				0

// Shapetests
CONST_INT FOLLOW_THE_LEADER_SHAPETEST_CAMERA		0

FUNC BOOL CONSTRUCT_FOLLOW_THE_LEADER_CUTSCENE(SIMPLE_CUTSCENE &cutscene, FREEMODE_DELIVERY_CUTSCENE_DATA &data, ENTITY_INDEX deliveredEntity = NULL)
	UNUSED_PARAMETER(deliveredEntity)
	UNUSED_PARAMETER(cutscene)
	
	data.fPool[FOLLOW_THE_LEADER_FLOAT_CAM_HEIGHT] = 2.0
	data.fPool[FOLLOW_THE_LEADER_FLOAT_CAM_DIST] = 6.0
	data.fPool[FOLLOW_THE_LEADER_FLOAT_DOOR_OPEN] = 0.0
	
	data.models[FOLLOW_THE_LEADER_MODEL_INSURGENT] = INSURGENT2
	data.models[FOLLOW_THE_LEADER_MODEL_CRATE] = PROP_MIL_CRATE_02
	
	RETURN TRUE
ENDFUNC

FUNC BOOL _HAVE_FOLLOW_THE_LEADER_CAMERA_PROBES_FINISHED(SIMPLE_CUTSCENE &cutscene, FREEMODE_DELIVERY_CUTSCENE_DATA &data)
	UNUSED_PARAMETER(cutscene)
	
	IF data.iPool[FOLLOW_THE_LEADER_INT_SHAPETEST_ID] = FOLLOW_THE_LEADER_VEC_CAM_POINTS_COUNT
		// We're good when we finished shapetest for all the points
		RETURN TRUE
	ENDIF

	VECTOR vBasePos = data.vBasePos
	
	IF NATIVE_TO_INT(data.shapetests[FOLLOW_THE_LEADER_SHAPETEST_CAMERA]) = 0
		IF IS_VECTOR_ZERO(data.vPool[data.iPool[FOLLOW_THE_LEADER_INT_SHAPETEST_ID]])
			FLOAT fAngleStep = 360.0 / FOLLOW_THE_LEADER_VEC_CAM_POINTS_COUNT
			data.vPool[data.iPool[FOLLOW_THE_LEADER_INT_SHAPETEST_ID]].X = vBasePos.X + COS(fAngleStep * data.iPool[FOLLOW_THE_LEADER_INT_SHAPETEST_ID]) * data.fPool[FOLLOW_THE_LEADER_FLOAT_CAM_DIST]
			data.vPool[data.iPool[FOLLOW_THE_LEADER_INT_SHAPETEST_ID]].Y = vBasePos.Y + SIN(fAngleStep * data.iPool[FOLLOW_THE_LEADER_INT_SHAPETEST_ID]) * data.fPool[FOLLOW_THE_LEADER_FLOAT_CAM_DIST]
			data.vPool[data.iPool[FOLLOW_THE_LEADER_INT_SHAPETEST_ID]].Z = vBasePos.Z + 40.0 // gotta be above the ground or else get_ground fails
			
			FLOAT fGroundZ
			
			IF GET_GROUND_Z_FOR_3D_COORD(data.vPool[data.iPool[FOLLOW_THE_LEADER_INT_SHAPETEST_ID]], fGroundZ)
				data.vPool[data.iPool[FOLLOW_THE_LEADER_INT_SHAPETEST_ID]].Z = fGroundZ + data.fPool[FOLLOW_THE_LEADER_FLOAT_CAM_HEIGHT]
			ELSE
				// Skip that point
				data.iPool[FOLLOW_THE_LEADER_INT_SHAPETEST_ID] += 1
				PRINTLN("[GUNRUNNING_CUTSCENE] _HAVE_FOLLOW_THE_LEADER_CAMERA_PROBES_FINISHED - Couldn't get ground Z for current point! Skipping point.")
				RETURN FALSE
			ENDIF
			
			// Skip point if the difference between original location and new ground Z is too big (this eliminates points that end up on top of buildings)
			IF ABSF(data.vPool[data.iPool[FOLLOW_THE_LEADER_INT_SHAPETEST_ID]].Z - vBasePos.Z) > 4.0
				data.iPool[FOLLOW_THE_LEADER_INT_SHAPETEST_ID] += 1
				PRINTLN("[GUNRUNNING_CUTSCENE] _HAVE_FOLLOW_THE_LEADER_CAMERA_PROBES_FINISHED - Vertical distance between point and the base is too much! Skipping point.")
				RETURN FALSE
			ENDIF
			
			PRINTLN("[GUNRUNNING_CUTSCENE] _HAVE_FOLLOW_THE_LEADER_CAMERA_PROBES_FINISHED - Got new point number ", data.iPool[FOLLOW_THE_LEADER_INT_SHAPETEST_ID], ": ", data.vPool[data.iPool[FOLLOW_THE_LEADER_INT_SHAPETEST_ID]])
		ENDIF
		
		VECTOR fShapetestFrom = data.vPool[data.iPool[FOLLOW_THE_LEADER_INT_SHAPETEST_ID]]
		VECTOR fShapetestTo = vBasePos + <<0.0, 0.0, 0.1>>
		
		data.shapetests[FOLLOW_THE_LEADER_SHAPETEST_CAMERA] = START_SHAPE_TEST_LOS_PROBE(fShapetestFrom, fShapetestTo, SCRIPT_INCLUDE_ALL)
		
		IF NATIVE_TO_INT(data.shapetests[FOLLOW_THE_LEADER_SHAPETEST_CAMERA]) != 0
			PRINTLN("[GUNRUNNING_CUTSCENE] _HAVE_FOLLOW_THE_LEADER_CAMERA_PROBES_FINISHED - Started shapetest from ", fShapetestFrom, " to ", fShapetestTo)
		ELSE
			PRINTLN("[GUNRUNNING_CUTSCENE] _HAVE_FOLLOW_THE_LEADER_CAMERA_PROBES_FINISHED - Something went wrong when starting the shapetest, will try again next frame.")
		ENDIF
	ELSE
		INT iHitSomething
		VECTOR vHitPos, vHitNormal
		ENTITY_INDEX entityHit
		
		SHAPETEST_STATUS status = GET_SHAPE_TEST_RESULT(data.shapetests[FOLLOW_THE_LEADER_SHAPETEST_CAMERA], iHitSomething, vHitPos, vHitNormal, entityHit)
		
		IF status = SHAPETEST_STATUS_RESULTS_READY
			IF iHitSomething = 0
				// We didn't hit anything so the point is good!
				SET_BIT(data.iBSPool[FOLLOW_THE_LEADER_BS_OK_CAM_POINTS], data.iPool[FOLLOW_THE_LEADER_INT_SHAPETEST_ID])
				PRINTLN("[GUNRUNNING_CUTSCENE] _HAVE_FOLLOW_THE_LEADER_CAMERA_PROBES_FINISHED - Found a good point for camera start: ", data.iPool[FOLLOW_THE_LEADER_INT_SHAPETEST_ID])
			ENDIF
			
			data.iPool[FOLLOW_THE_LEADER_INT_SHAPETEST_ID] += 1
			data.shapetests[FOLLOW_THE_LEADER_SHAPETEST_CAMERA] = INT_TO_NATIVE(SHAPETEST_INDEX, 0)
			
		ELIF status = SHAPETEST_STATUS_NONEXISTENT
			data.shapetests[FOLLOW_THE_LEADER_SHAPETEST_CAMERA] = INT_TO_NATIVE(SHAPETEST_INDEX, 0)
			PRINTLN("[GUNRUNNING_CUTSCENE] _HAVE_FOLLOW_THE_LEADER_CAMERA_PROBES_FINISHED - Shapetest result returned non-existent, something went wrong, resetting.")
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/*
FUNC BOOL _HAVE_FOLLOW_THE_LEADER_INSURGENT_BOUND_TEST_FINISHED(SIMPLE_CUTSCENE &cutscene, FREEMODE_DELIVERY_CUTSCENE_DATA &data)
	VECTOR vModelMin, vModelMax
	GET_MODEL_DIMENSIONS(data.models[FOLLOW_THE_LEADER_MODEL_INSURGENT], vModelMin, vModelMax)
	vModelMax = vModelMax + <<0.0, 0.0, 1.5>> // Extension at the back to have the car driving in
	
	VECTOR vBasePos = data.vBasePos
	
	
	//GET_VEHICLE_
	START_SHAPE_TEST_BOX
ENDFUNC
*/

FUNC BOOL HAS_FOLLOW_THE_LEADER_CUTSCENE_INITIALISED(SIMPLE_CUTSCENE &cutscene, FREEMODE_DELIVERY_CUTSCENE_DATA &data)
	IF _HAVE_FOLLOW_THE_LEADER_CAMERA_PROBES_FINISHED(cutscene, data)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC GUNRUN_GET_FOLLOW_LEADER_BACKUP_DELIVERY_CAM_DETAILS(FREEMODE_DELIVERY_DROPOFFS eDropoffID, VECTOR &vCamPosition, VECTOR &vCamRotation, FLOAT &fCamFOV)
	SWITCH eDropoffID
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_1 // 71
			vCamPosition = <<-240.6769, 2091.1418, 149.0416>>
			vCamRotation = <<-16.8195, -0.0000, -148.7795>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_2
			vCamPosition = <<2744.2717, 2848.4612, 48.6057>>
			vCamRotation = <<-20.6121, -0.0000, -94.4168>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_3
			vCamPosition = <<3010.8225, 3487.0269, 82.9643>>
			vCamRotation = <<-13.8797, -0.0000, 121.4941>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_4
			vCamPosition = <<-2689.4912, 3469.6660, 28.9000>>
			vCamRotation = <<-18.3763, -0.0000, -113.0179>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_5
			vCamPosition = <<1474.6542, 3575.4480, 43.0862>>
			vCamRotation = <<-11.6285, -0.0000, -72.3609>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_6
			vCamPosition = <<-2407.2778, 4267.5542, 18.1345>>
			vCamRotation = <<-3.5704, -0.0000, -59.8728>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_7
			vCamPosition = <<2486.1841, 4319.1792, 50.5665>>
			vCamRotation = <<-11.5855, -0.0000, -141.3174>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_8
			vCamPosition = <<-1840.3151, 4395.2861, 60.4985>>
			vCamRotation = <<-12.9521, -0.0000, 4.2585>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_9
			vCamPosition = <<-1553.7913, 4511.6431, 30.3840>>
			vCamRotation = <<-9.6026, -0.0000, 161.3975>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_10//80
			vCamPosition = <<-422.6879, 6384.7979, 24.4980>>
			vCamRotation = <<-18.8113, -0.0000, -113.7943>>
			fCamFOV = 50.0
		BREAK		
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_1 // 81
			vCamPosition = <<1174.9204, -3225.9519, 14.6373>>
			vCamRotation = <<-18.8228, -0.0000, -139.8902>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_2
			vCamPosition = <<199.9098, -3106.5261, 17.8524>>
			vCamRotation = <<-25.3981, -0.0000, -150.1158>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_3
			vCamPosition = <<290.8995, -2870.5347, 17.6846>>
			vCamRotation = <<-33.5881, 0.0000, -57.4045>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_4
			vCamPosition = <<1654.1420, -2368.0044, 104.5084>>
			vCamRotation = <<-24.8413, -0.0000, 96.3044>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_5
			vCamPosition = <<868.8862, -2458.9985, 38.3830>>
			vCamRotation = <<-13.7167, 0.0000, 43.0259>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_6
			vCamPosition = <<-1205.8557, -2389.3716, 24.8211>>
			vCamRotation = <<-21.0412, -0.0000, 103.3599>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_7
			vCamPosition = <<920.9332, -1751.7904, 37.9878>>
			vCamRotation = <<-13.5384, -0.0000, 39.2778>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_8
			vCamPosition = <<1751.1338, -1553.6910, 124.7459>>
			vCamRotation = <<-17.4720, -0.0000, 40.2432>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_9
			vCamPosition = <<502.0954, -1412.3597, 37.8341>>
			vCamRotation = <<-20.9308, -0.0000, 40.9399>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_10 //90
			vCamPosition = <<-133.8458, -1363.0621, 38.3665>>
			vCamRotation = <<-17.6259, 0.0000, 50.5273>>
			fCamFOV = 50.0
		BREAK
		
		DEFAULT
			CASSERTLN(DEBUG_AMBIENT, "GUNRUN_GET_FOLLOW_LEADER_BACKUP_DELIVERY_CAM_DETAILS passed non follow the leader mission dropoff!")
		
	ENDSWITCH
	
	PRINTLN("[GUNRUNNING_CUTSCENE] GUNRUN_GET_FOLLOW_LEADER_BACKUP_DELIVERY_CAM_DETAILS - Returning position: ", vCamPosition, " rotation: ", vCamRotation, " FOV: ", fCamFOV)
ENDPROC

PROC _CREATE_FOLLOW_THE_LEADER_SCENES(SIMPLE_CUTSCENE &cutscene, FREEMODE_DELIVERY_CUTSCENE_DATA &data)
	FLOAT fTargetHeading = data.fBaseHeading + 90.0 //data.fPool[FOLLOW_THE_LEADER_FLOAT_TARGET_HEADING]
	FLOAT fAngleStep = 360.0 / FOLLOW_THE_LEADER_VEC_CAM_POINTS_COUNT
	FLOAT fCamFOVStart = 50.0
	INT iClosestAngleID = FLOOR(fTargetHeading / fAngleStep)
	
	PRINTLN("[GUNRUNNING_CUTSCENE] _CREATE_FOLLOW_THE_LEADER_SCENES - Got target camera heading ", fTargetHeading)
	PRINTLN("[GUNRUNNING_CUTSCENE] _CREATE_FOLLOW_THE_LEADER_SCENES - Closest to target point is ", iClosestAngleID)
	
	INT iCounter
	INT iPointToUse = -1
	
	// Go both ways and find the starting camera point that matches target heading
	WHILE iCounter < FOLLOW_THE_LEADER_VEC_CAM_POINTS_COUNT
		IF iClosestAngleID + iCounter < FOLLOW_THE_LEADER_VEC_CAM_POINTS_COUNT
			IF IS_BIT_SET(data.iBSPool[FOLLOW_THE_LEADER_BS_OK_CAM_POINTS], iClosestAngleID + iCounter)
				iPointToUse = iClosestAngleID + iCounter
				BREAKLOOP
			ENDIF
		ENDIF
		
		IF iCounter > 0 AND iClosestAngleID - iCounter >= 0
			IF IS_BIT_SET(data.iBSPool[FOLLOW_THE_LEADER_BS_OK_CAM_POINTS], iClosestAngleID - iCounter)
				iPointToUse = iClosestAngleID - iCounter
				BREAKLOOP
			ENDIF
		ENDIF
			
		iCounter += 1
	ENDWHILE
	
	IF iPointToUse = -1
	#IF IS_DEBUG_BUILD
	OR data.bForceUseBackupCam
	#ENDIF
		PRINTLN("[GUNRUNNING_CUTSCENE] _CREATE_FOLLOW_THE_LEADER_SCENES - Coulnd't find any good point to put the camera in! Using backup camera")
		data.bFailedToFindValidCamPos = TRUE
		iPointToUse = 0
	ELSE
		PRINTLN("[GUNRUNNING_CUTSCENE] _CREATE_FOLLOW_THE_LEADER_SCENES - Found the closest point to the desired heading, it's ", iPointToUse)
	ENDIF
	
	VECTOR vCutscenePos = data.vBasePos
	//FLOAT fCutsceneHeading = data.fBaseHeading
	//VECTOR vCutsceneDir = GET_HEADING_AS_VECTOR(fCutsceneHeading)
	
	VECTOR vCamStartPos = data.vPool[iPointToUse]
	
	SIMPLE_CUTSCENE_CREATE(cutscene, "FOLLOW_THE_LEADER")
	
	VECTOR vLookAtDir = (vCutscenePos + <<0.0, 0.0, 1.5>>) - vCamStartPos
	vLookAtDir = NORMALISE_VECTOR(vLookAtDir)
	
	VECTOR vCamRot = _GET_CAM_ROT_FROM_DIR(vLookAtDir)
	
	IF data.bFailedToFindValidCamPos
		GUNRUN_GET_FOLLOW_LEADER_BACKUP_DELIVERY_CAM_DETAILS(data.eDropoffID, vCamStartPos, vCamRot, fCamFOVStart)
		data.bFailedToFindValidCamPos = FALSE
	ENDIF
	
	SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 3500, "scene1", vCamStartPos, vCamRot, fCamFOVStart, vCamStartPos + <<0.0, 0.0, 0.0>>, vCamRot, 35.0, 0.2)
	SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 5000, "scene2", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 60.0, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 60.0, 0.2, 0, 0, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL) // This scene is gonna get filled in dynamically
	SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 400, "scene2", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 60.0, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 60.0, 0.2, 0, 0)
ENDPROC

PROC _CREATE_FOLLOW_THE_LEADER_ASSETS(SIMPLE_CUTSCENE &cutscene, FREEMODE_DELIVERY_CUTSCENE_DATA &data)
	UNUSED_PARAMETER(cutscene)
	UNUSED_PARAMETER(data)
//	IF DOES_ENTITY_EXIST(data.vehDropOff)
//		IF CREATE_VEHICLE_CLONE(data.vehActors[FOLLOW_THE_LEADER_VEH_CLONE], data.vehDropOff, GET_ENTITY_COORDS(data.vehDropOff), GET_ENTITY_HEADING(data.vehDropOff), FALSE, FALSE)
//			PRINTLN("[GUNRUNNING_CUTSCENE] _CREATE_FOLLOW_LEADER_SCENES - Player Vehicle clone was created succesfully.")
//			SET_VEHICLE_CAN_BREAK(data.vehActors[FOLLOW_THE_LEADER_VEH_CLONE], FALSE)
//			FREEZE_ENTITY_POSITION(data.vehActors[FOLLOW_THE_LEADER_VEH_CLONE], TRUE)
//			//Create Delivery Prop
//			data.objProps[FOLLOW_THE_LEADER_PROP_CRATE] = CREATE_OBJECT(data.models[FOLLOW_THE_LEADER_MODEL_CRATE], <<10.0, 10.0, 10.0>>, FALSE, FALSE, TRUE)
//			SET_ENTITY_COLLISION(data.objProps[FOLLOW_THE_LEADER_PROP_CRATE], FALSE)
//			ATTACH_ENTITY_TO_ENTITY(data.objProps[FOLLOW_THE_LEADER_PROP_CRATE], data.vehActors[FOLLOW_THE_LEADER_VEH_CLONE], 
//			GET_ENTITY_BONE_INDEX_BY_NAME(data.vehActors[FOLLOW_THE_LEADER_VEH_CLONE], "chassis_dummy"), <<0.0, -2.2, 0.813>>, <<0.0, 0.0, 90.0>>, TRUE, FALSE, TRUE)
//		ELSE
//			PRINTLN("[GUNRUNNING_CUTSCENE] _CREATE_FOLLOW_LEADER_SCENES - Unable to create Player vehicle clone.")
//		ENDIF
//	ELSE
//		PRINTLN("[GUNRUNNING_CUTSCENE] _CREATE_FOLLOW_LEADER_SCENES - Player is not in a vehicle")
//	ENDIF
//	
//	PRINTLN("[GUNRUNNING_CUTSCENE] _CREATE_FOLLOW_LEADER_SCENES - Created insurgent at ", data.vBasePos, " with heading ", data.fBaseHeading)
//	
//	IF NOT IS_ENTITY_DEAD(data.vehActors[FOLLOW_THE_LEADER_VEH_CLONE])
//		INT iSeat
//		
//		INT iPedIndex
//		PED_INDEX pedInSeat
//		FOR iSeat = VS_DRIVER TO VS_EXTRA_RIGHT_3
//			iPedIndex = iSeat + 1
//			IF NOT IS_VEHICLE_SEAT_FREE(data.vehDropOff, INT_TO_ENUM(VEHICLE_SEAT, iSeat))
//				pedInSeat = GET_PED_IN_VEHICLE_SEAT(data.vehDropOff, INT_TO_ENUM(VEHICLE_SEAT, iSeat))
//				IF DOES_ENTITY_EXIST(pedInSeat)
//					data.pedPlayerActors[iPedIndex] = CLONE_PED(pedInSeat, FALSE, FALSE)
//					SET_PED_INTO_VEHICLE(data.pedPlayerActors[iPedIndex], data.vehActors[MOVE_WEAPONS_VEH_CLONE], INT_TO_ENUM(VEHICLE_SEAT, iSeat))
//				ENDIF
//			ENDIF
//		ENDFOR
//	ENDIF
//
//	IF DOES_ENTITY_EXIST(data.vehActors[FOLLOW_THE_LEADER_VEH_CLONE])
//		#IF IS_DEBUG_BUILD
//		VECTOR vCoord 
//		vCoord = GET_ENTITY_COORDS(data.vehActors[FOLLOW_THE_LEADER_VEH_CLONE])
//		PRINTLN( "[GUNRUNNING_CUTSCENE] _CREATE_FOLLOW_LEADER_SCENES - SET VEHICLE ON GROUND - TRY, Coords: ", vCoord)
//		#ENDIF
//		IF SET_VEHICLE_ON_GROUND_PROPERLY(data.vehActors[FOLLOW_THE_LEADER_VEH_CLONE])
//			#IF IS_DEBUG_BUILD
//			vCoord = GET_ENTITY_COORDS(data.vehActors[FOLLOW_THE_LEADER_VEH_CLONE])
//			PRINTLN( "[GUNRUNNING_CUTSCENE] _CREATE_FOLLOW_LEADER_SCENES - SET VEHICLE ON GROUND - SUCCCESS, Coords: ", vCoord)
//			#ENDIF
//			FREEZE_ENTITY_POSITION(data.vehActors[FOLLOW_THE_LEADER_VEH_CLONE], TRUE)
//			SET_ENTITY_COLLISION(data.vehActors[FOLLOW_THE_LEADER_VEH_CLONE], FALSE)
//		ENDIF
//	ENDIF
ENDPROC

PROC START_FOLLOW_THE_LEADER_CUTSCENE(SIMPLE_CUTSCENE &cutscene, FREEMODE_DELIVERY_CUTSCENE_DATA &data)
	_CREATE_FOLLOW_THE_LEADER_SCENES(cutscene, data)
	
	IF data.bFailedToFindValidCamPos
		EXIT
	ENDIF
	
	_CREATE_FOLLOW_THE_LEADER_ASSETS(cutscene, data)
	
	// Compute camera position for last shot showing the camera leaving insurgent from the back door
	//Create scene cams before cutscene starts.
	#IF IS_DEBUG_BUILD
	VECTOR vCoord
	vCoord = GET_ENTITY_COORDS(data.vehActors[FOLLOW_THE_LEADER_VEH_CLONE])
	PRINTLN( "MAINTAIN_FOLLOW_THE_LEADER_CUTSCENE - Camera loaded: SceneVehCoords: ",  vCoord)
	#ENDIF
	cutscene.sScenes[1].vCamCoordsStart = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(data.vehActors[FOLLOW_THE_LEADER_VEH_CLONE], <<0.0, -3.5, 0.8>>)
	cutscene.sScenes[1].vCamCoordsFinish = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(data.vehActors[FOLLOW_THE_LEADER_VEH_CLONE], <<0.0, -4.5, 0.8>>)
	
	VECTOR vCamLookAt = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(data.vehActors[FOLLOW_THE_LEADER_VEH_CLONE], <<0.0, 0.0, 1.0>>)
	
	cutscene.sScenes[1].vCamRotStart = _GET_CAM_ROT_FROM_DIR(NORMALISE_VECTOR(vCamLookAt - cutscene.sScenes[1].vCamCoordsStart))
	cutscene.sScenes[1].vCamRotStart.Y = -GET_ENTITY_ROLL(data.vehActors[FOLLOW_THE_LEADER_VEH_CLONE])
	cutscene.sScenes[1].vCamRotFinish = cutscene.sScenes[1].vCamRotStart
	
	SET_VEHICLE_DOOR_OPEN(data.vehActors[FOLLOW_THE_LEADER_VEH_CLONE], SC_DOOR_BOOT, FALSE, TRUE)
			
	cutscene.sScenes[2].vCamCoordsStart = cutscene.sScenes[1].vCamCoordsFinish
	cutscene.sScenes[2].vCamCoordsFinish = cutscene.sScenes[1].vCamCoordsFinish
	cutscene.sScenes[2].vCamRotStart = cutscene.sScenes[1].vCamRotStart
	cutscene.sScenes[2].vCamRotFinish = cutscene.sScenes[1].vCamRotStart
ENDPROC

PROC MAINTAIN_FOLLOW_THE_LEADER_CUTSCENE(SIMPLE_CUTSCENE &cutscene, FREEMODE_DELIVERY_CUTSCENE_DATA &data)
	IF NOT DOES_ENTITY_EXIST(data.vehActors[FOLLOW_THE_LEADER_VEH_CLONE])
	OR IS_ENTITY_DEAD(data.vehActors[FOLLOW_THE_LEADER_VEH_CLONE])
	OR NOT DOES_ENTITY_EXIST(data.pedPlayerActors[0])
	OR IS_ENTITY_DEAD(data.pedPlayerActors[0])
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(data.iBSPool[FOLLOW_THE_LEADER_BS_CUTSCENE], BS_FOLLOW_THE_LEADER_CUTSCENE_TASKED_LEAVE)
		IF cutscene.iTotalElapsedTime > 500
			TASK_LEAVE_ANY_VEHICLE(data.pedPlayerActors[0], 0)
			SET_BIT(data.iBSPool[FOLLOW_THE_LEADER_BS_CUTSCENE], BS_FOLLOW_THE_LEADER_CUTSCENE_TASKED_LEAVE)
		ENDIF
	ENDIF
	
	IF SIMPLE_CUTSCENE_IS_SCENE_RUNNING(cutscene, 1)
		// Move ped to the back of the insurgent
		IF NOT IS_BIT_SET(data.iBSPool[FOLLOW_THE_LEADER_BS_CUTSCENE], BS_FOLLOW_THE_LEADER_CUTSCENE_PED_MOVED)
			VECTOR vPedPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(data.vehActors[FOLLOW_THE_LEADER_VEH_CLONE], <<-2.0, -3.75, 0.0>>)
			VECTOR vPedDir = NORMALISE_VECTOR(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(data.vehActors[FOLLOW_THE_LEADER_VEH_CLONE], <<0.0, -3.75, 0.0>>) - vPedPos)
			FLOAT fPedHeading = GET_HEADING_FROM_VECTOR_2D(vPedDir.X, vPedDir.Y)
		
			SET_ENTITY_COORDS(data.pedPlayerActors[0], vPedPos)
			SET_ENTITY_HEADING(data.pedPlayerActors[0], fPedHeading)
			
			SET_BIT(data.iBSPool[FOLLOW_THE_LEADER_BS_CUTSCENE], BS_FOLLOW_THE_LEADER_CUTSCENE_PED_MOVED)
		ENDIF
		
		// Task ped with walking
		IF NOT IS_BIT_SET(data.iBSPool[FOLLOW_THE_LEADER_BS_CUTSCENE], BS_FOLLOW_THE_LEADER_CUTSCENE_TASKED_GOTO)
		AND cutscene.iCurrentSceneElapsedTime > 2000
			TASK_GO_STRAIGHT_TO_COORD(data.pedPlayerActors[0], GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(data.vehActors[FOLLOW_THE_LEADER_VEH_CLONE], <<0.6, -3.75, 0.0>>), PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, GET_ENTITY_HEADING(data.vehActors[FOLLOW_THE_LEADER_VEH_CLONE]))
			TASK_LOOK_AT_COORD(data.pedPlayerActors[0], GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(data.vehActors[FOLLOW_THE_LEADER_VEH_CLONE], <<0.0, 0.0, 1.0>>), 3000)
			SET_BIT(data.iBSPool[FOLLOW_THE_LEADER_BS_CUTSCENE], BS_FOLLOW_THE_LEADER_CUTSCENE_TASKED_GOTO)
		ENDIF
		
		// Close the door
		IF NOT IS_BIT_SET(data.iBSPool[FOLLOW_THE_LEADER_BS_CUTSCENE], BS_FOLLOW_THE_LEADER_CUTSCENE_BACK_DOOR_CLOSED)
		AND cutscene.iCurrentSceneElapsedTime > 3000
			data.fPool[FOLLOW_THE_LEADER_FLOAT_DOOR_OPEN] += 1.4 * GET_FRAME_TIME()
			data.fPool[FOLLOW_THE_LEADER_FLOAT_DOOR_OPEN] = CLAMP(data.fPool[FOLLOW_THE_LEADER_FLOAT_DOOR_OPEN], 0.0, 1.0)	
			SET_VEHICLE_DOOR_CONTROL(data.vehActors[FOLLOW_THE_LEADER_VEH_CLONE], SC_DOOR_BOOT, DT_DOOR_NO_RESET, 1 - data.fPool[FOLLOW_THE_LEADER_FLOAT_DOOR_OPEN])
			IF data.fPool[FOLLOW_THE_LEADER_FLOAT_DOOR_OPEN] >= 1.0
				SET_BIT(data.iBSPool[FOLLOW_THE_LEADER_BS_CUTSCENE], BS_FOLLOW_THE_LEADER_CUTSCENE_BACK_DOOR_CLOSED)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DESTRUCT_FOLLOW_THE_LEADER_CUTSCENE(SIMPLE_CUTSCENE &cutscene, FREEMODE_DELIVERY_CUTSCENE_DATA &data)
	UNUSED_PARAMETER(cutscene)
	UNUSED_PARAMETER(data)
ENDPROC

#IF IS_DEBUG_BUILD
PROC MAINTAIN_FOLLOW_THE_LEADER_CUTSCENE_DEBUG(SIMPLE_CUTSCENE &cutscene, FREEMODE_DELIVERY_CUTSCENE_DATA &data)
	UNUSED_PARAMETER(cutscene)
	UNUSED_PARAMETER(data)
	
	INT i
	REPEAT FOLLOW_THE_LEADER_VEC_CAM_POINTS_COUNT i
		IF IS_BIT_SET(data.iBSPool[FOLLOW_THE_LEADER_BS_OK_CAM_POINTS], i)
			DRAW_DEBUG_SPHERE(data.vPool[i], 0.1, 0, 255, 0)
		ELSE
			DRAW_DEBUG_SPHERE(data.vPool[i], 0.1, 255, 0, 0)
		ENDIF
	ENDREPEAT
ENDPROC
#ENDIF

