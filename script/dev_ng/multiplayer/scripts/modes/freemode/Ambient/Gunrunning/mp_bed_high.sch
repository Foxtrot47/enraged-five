// Includes
USING "commands_ped.sch"
USING "context_control_public.sch"
USING "freemode_header.sch"
//USING "rgeneral_include.sch"
USING "net_spawn_activities.sch"
USING "net_mission_trigger_public.sch"


// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	ob_mp_bed_med.sc
//		DESCRIPTION		:	Handles bed activity in mid-level multiplayer safehouses
//
// *****************************************************************************************
// *****************************************************************************************#


STRUCT BedStructData
	VECTOR vScenePos
	VECTOR vSceneRot
	VECTOR vBoundingBoxA
	VECTOR vBoundingBoxB
	BOOL bEnterRightBedSide 
	INT iScriptInstanceID
	FLOAT fWidth
	FLOAT fTargetRadius
ENDSTRUCT

// Enums
ENUM ACTIVITY_STATE
	AS_LOAD_ASSETS = 0,
	AS_RUN_ACTIVITY,
	AS_END
ENDENUM
ACTIVITY_STATE eBedState = AS_LOAD_ASSETS

ENUM TRIGGER_STATE
	TS_PLAYER_OUT_OF_RANGE = 0,
	TS_WAIT_FOR_PLAYER,
	TS_RUN_ENTRY_ANIM,
	TS_CHECK_ENTRY_ANIM,
	TS_RUN_IDLE_ANIM,
	TS_CHECK_IDLE_ANIM,
	TS_WAIT_FOR_INPUT,
	TS_RUN_EXIT_ANIM,
	TS_CHECK_EXIT_ANIM,
	TS_RESET
ENDENUM

FUNC STRING GetStageString(TRIGGER_STATE state)
	SWITCH state
	   CASE TS_PLAYER_OUT_OF_RANGE RETURN "TS_PLAYER_OUT_OF_RANGE"
		CASE TS_WAIT_FOR_PLAYER	RETURN "TS_WAIT_FOR_PLAYER"
		CASE TS_RUN_ENTRY_ANIM RETURN "TS_RUN_ENTRY_ANIM"
		CASE TS_CHECK_ENTRY_ANIM RETURN "TS_CHECK_ENTRY_ANIM"
		CASE  TS_RUN_IDLE_ANIM RETURN "TS_RUN_IDLE_ANIM"
		CASE TS_CHECK_IDLE_ANIM RETURN "TS_CHECK_IDLE_ANIM"
		CASE  TS_WAIT_FOR_INPUT RETURN "TS_WAIT_FOR_INPUT"
		CASE TS_RUN_EXIT_ANIM RETURN "TS_RUN_EXIT_ANIM"
		CASE TS_CHECK_EXIT_ANIM RETURN "TS_CHECK_EXIT_ANIM"
		CASE TS_RESET RETURN "TS_RESET"
	ENDSWITCH
	RETURN ""
ENDFUNC

//STRUCT PlayerBroadcastData
//	INT iBedID = -1
//	INT iAnimVar = -1
//ENDSTRUCT

//PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

TRIGGER_STATE eTriggerState = TS_PLAYER_OUT_OF_RANGE
INT iPlayerStateBitSet

CONST_INT BIT_INDEX_WEARING_HELMET   	0
CONST_INT BIT_INDEX_DISABLED_PIM		1

PED_COMP_NAME_ENUM eStoredMask 
BOOL bSwappedChemSuit = FALSE
INT iStoredHatTex
INT iStoredHat

FLOAT	AREA_WIDTH

// Variables
BOOL 			bSetUpAnimDict		= FALSE
BOOL			bSleeping			= FALSE
BOOL 			bInTransition		= FALSE

CAMERA_INDEX  	mCam

FLOAT 			fTriggerHead


INT 			iContextIntentionMain = NEW_CONTEXT_INTENTION
INT 			iContextIntentionRight = NEW_CONTEXT_INTENTION
INT 			iContextIntentionWH = NEW_CONTEXT_INTENTION
INT				iLocalScene
INT 			iScene
INT				iActivityID			= ci_APT_ACT_BED_HI

//NAVDATA			mNavStruct
BOOL bInitOffsets

STRING 			sAnimDictionary 		= "mp_bedmid"
//STRING 			sEnterAnim			= "f_getin_l_bighouse"
//STRING 			sIdleAnim 			= "f_sleep_l_loop_bighouse"
//STRING			sExitAnim			= "f_getout_l_bighouse"

//VECTOR 			vBedPos

//VECTOR			vCamPos, vCamHead
//FLOAT			fCamFov				= 45.0
//
//VECTOR			vExitCamPos, vExitCamHead
//FLOAT			fExitCamFov			= 50.0

//VECTOR 			vPlayerPos

VECTOR			vTriggerAreaA, vTriggerAreaB
VECTOR 			vTriggerPos


//FLOAT			fTriggerHead
BOOL bRightBedSide

VECTOR 			vScenePos
VECTOR 			vSceneHead

BOOL bDrunkWakeUp = FALSE


#IF IS_DEBUG_BUILD
VECTOR vTempDebugVector
#ENDIF

//VECTOR vYachtBed1 = <<-1565.0925, -4089.3171, 5.7800>>
//VECTOR vYachtBed2 = <<-1872.8811, -4088.8394, 5.7800>>
//VECTOR vYachtBed3 = <<-1593.2943, -4080.7271, 5.7800>>
// PC CONTROL FOR GETTING OUT OF BED
BOOL bPCCOntrolsSetup = FALSE



FUNC STRING GET_ENTER_ANIM(BOOL bRightSide = FALSE)
	STRING sReturnString = "f_getin_l_bighouse"

	
	IF bRightSide = FALSE
		sReturnString = "f_getin_l_bighouse"
	ELSE
		sReturnString = "f_getin_r_bighouse"
	ENDIF
		
	CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_ENTER_ANIM: ", sReturnString)
	RETURN sReturnString
	
ENDFUNC

FUNC STRING GET_EXIT_ANIM(BOOL bRightSide = FALSE)
	STRING sReturnString = "f_getout_l_bighouse"

	
	IF bRightSide = FALSE
		sReturnString = "f_getout_l_bighouse"
	ELSE
		sReturnString = "f_getout_r_bighouse"
	ENDIF
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_EXIT_ANIM: ", sReturnString)
	RETURN sReturnString
	
ENDFUNC

FUNC STRING GET_IDLE_ANIM(BOOL bRightSide = FALSE)
	STRING sReturnString = "f_sleep_l_loop_bighouse"

	IF bRightSide = FALSE
		sReturnString = "f_sleep_l_loop_bighouse"
	ELSE
		sReturnString = "f_sleep_r_loop_bighouse"
	ENDIF
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_IDLE_ANIM: ", sReturnString)
	RETURN sReturnString
	
ENDFUNC

PROC SETUP_PC_CONTROLS()

	IF IS_PC_VERSION()
		IF bPCCOntrolsSetup = FALSE
			INIT_PC_SCRIPTED_CONTROLS("SAFEHOUSE ACTIVITY")
			bPCCOntrolsSetup = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_PC_CONTROLS()

	IF IS_PC_VERSION()
		IF bPCCOntrolsSetup = TRUE
			SHUTDOWN_PC_SCRIPTED_CONTROLS()
			bPCCOntrolsSetup = FALSE
		ENDIF
	ENDIF
ENDPROC

//FUNC BOOL IS_THIS_NEW_BUSINESS_INTERIOR()
//	SWITCH iCurProperty
//		CASE PROPERTY_BUS_HIGH_APT_1
//		CASE PROPERTY_BUS_HIGH_APT_2
//		CASE PROPERTY_BUS_HIGH_APT_3
//		CASE PROPERTY_BUS_HIGH_APT_4
//		CASE PROPERTY_BUS_HIGH_APT_5
//			RETURN TRUE
//		BREAK
//	ENDSWITCH
//	RETURN FALSE	
//ENDFUNC

FUNC BOOL CAN_PLAYER_USE_BED()

	PLAYER_INDEX playerIndex
	INT i
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
		
			playerIndex = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			
			IF IS_NET_PLAYER_OK(playerIndex, FALSE)
			AND playerIndex != PLAYER_ID()
				IF ARE_PLAYERS_IN_SAME_PROPERTY(PLAYER_ID(), playerIndex, TRUE)
				AND NOT IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(playerIndex)
				
					// TODO: Implement function which gets bed Id at players location 
					// and check if any others bed Id matches.
				
//					IF playerBD[NATIVE_TO_INT(playerIndex)].iBedID != -1
//						CDEBUG1LN(DEBUG_SAFEHOUSE, "OB_MP_BED_HIGH - CAN_PLAYER_USE_BED: Cannot use this bed as another player is using it.")
//						RETURN FALSE
//					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

	CDEBUG2LN(DEBUG_SAFEHOUSE, "OB_MP_BED_HIGH - CAN_PLAYER_USE_BED: Can use bed.")
	RETURN TRUE
ENDFUNC

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Cleans up assets for bed script script
PROC CLEANUP_BED_ACTIVITY()
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_BED_ACTIVITY")
//	SET_LOCAL_PLAYER_INVISIBLE_LOCALLY  
//	SET_PLAYER_INVISIBLE_LOCALLY
	g_showerGlobals.bPauseSkyCamForSpawn = FALSE
	CDEBUG1LN(DEBUG_SAFEHOUSE, "MP Bed Script Cleaning up...")
	
	// Clear the context intention
	IF iContextIntentionMain > -1
		CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_BED_ACTIVITY: RELEASE_CONTEXT_INTENTION(iContextIntentionMain)")
		RELEASE_CONTEXT_INTENTION(iContextIntentionMain)
	ELSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_BED_ACTIVITY: iContextIntentionMain: ", iContextIntentionMain)
	ENDIF
	
	IF iContextIntentionRight > -1
		CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_BED_ACTIVITY: RELEASE_CONTEXT_INTENTION(iContextIntentionRight)")
		RELEASE_CONTEXT_INTENTION(iContextIntentionRight)
	ELSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_BED_ACTIVITY: iContextIntentionMain: ", iContextIntentionRight)
	ENDIF
	
	IF bSwappedChemSuit = TRUE
		SWAP_CHEM_SUIT_HOOD_FOR_ALTERNATE(PLAYER_PED_ID())
		
		bSwappedChemSuit = FALSE
	ENDIF
	
	IF eStoredMask != PROPS_HEAD_NONE
		SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_BERD, eStoredMask, FALSE)
	ENDIF
	
	IF IS_BIT_SET(iPlayerStateBitSet, BIT_INDEX_WEARING_HELMET)
		SET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD, iStoredHat, iStoredHatTex)
		
		CLEAR_BIT(iPlayerStateBitSet, BIT_INDEX_WEARING_HELMET)
	ENDIF
	
	PRINTLN("POD: BED: REAPPLYING MASK")
	
	IF IS_BIT_SET(iPlayerStateBitSet, BIT_INDEX_DISABLED_PIM)
		ENABLE_INTERACTION_MENU()
		
		CLEAR_BIT(iPlayerStateBitSet, BIT_INDEX_DISABLED_PIM)
	ENDIF
	
	SET_PLAYER_USING_BED_ACTIVITY(FALSE)
	
	RELEASE_CONTEXT_INTENTION(iContextIntentionWH)
	// Clear any text
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_BED_IN")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_BED_OUT")
		CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_BED_ACTIVITY: CLEAR_HELP")
		CLEAR_HELP()
	ELSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_BED_ACTIVITY: Help text not detected")
	ENDIF
	
	CLEANUP_PC_CONTROLS()
	
	// If the script has ended with the player sleeping (going into PSN store etc), get the ped out of bed.
	IF bSleeping
//	//AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
//		IF NOT IS_BIT_SET(iPropertyAllExitBitset, ALL_EXIT_STREET)
//		AND NOT IS_BIT_SET(iPropertyAllExitBitset, ALL_EXIT_GARAGE)
//			
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_BED_ACTIVITY: TASK_PLAY_ANIM_ADVANCED")
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_BED_ACTIVITY: vScenePos: ", vScenePos, ", vSceneHead: ", vSceneHead)
//			TASK_PLAY_ANIM_ADVANCED(PLAYER_PED_ID(), sAnimDictionary, sExitAnim, vScenePos, vSceneHead, REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT)
//		ELSE
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_BED_ACTIVITY: CLEARING EXIT BITS")
//			CLEAR_BIT(iPropertyAllExitBitset, ALL_EXIT_STREET)
//			CLEAR_BIT(iPropertyAllExitBitset, ALL_EXIT_GARAGE)
//		ENDIF
	ENDIF
		
	// Unload audio
	RELEASE_AMBIENT_AUDIO_BANK()
		
	// Remove camera
	IF DOES_CAM_EXIST(mCam)
		DESTROY_CAM(mCam)
	ENDIF
	
		// Remove animation dictionary
	IF bSetupAnimDict
		REMOVE_ANIM_DICT(sAnimDictionary)
	ENDIF
	g_showerGlobals.bPauseSkyCamForSpawn = FALSE
//	IF bSafehouseSetControlOff
//	AND NOT IS_PLAYER_SPECTATING(PLAYER_ID())
//	AND NOT IS_NEW_LOAD_SCENE_ACTIVE()
//	AND IS_SKYSWOOP_AT_GROUND()
//		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//	ENDIF						

	// End script
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Functions
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Requests and loads the correct animation dictionary
FUNC BOOL HAS_ANIM_DICT_LOADED_FOR_BED()
		
	// Load animations
	REQUEST_ANIM_DICT(sAnimDictionary)
	IF NOT HAS_ANIM_DICT_LOADED(sAnimDictionary)
		RETURN FALSE
	ENDIF
	bSetupAnimDict = TRUE
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Check the player is alone in the trigger area
FUNC BOOL IS_PLAYER_ALONE_IN_BED_AREA(VECTOR vTriggerArea1, VECTOR vTriggerArea2)

	PED_INDEX mPed[8]
	INT iPeds = GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), mPed)
	
	IF iPeds > 0
		INT i
		FOR i = 0 TO 7
			IF NOT IS_PED_INJURED(mPed[i])
				IF IS_ENTITY_IN_ANGLED_AREA(mPed[i], vTriggerArea1, vTriggerArea2, AREA_WIDTH)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDFOR
	
	ENDIF
	
	RETURN TRUE
	
ENDFUNC


FUNC INT GENERATE_ANIM_VAR_INT(INT iExculdeInt = -1) 
	INT iRandInt 
	iRandInt = GET_RANDOM_INT_IN_RANGE(1, 5)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_ANIM_VAR_INT: iExculdeInt = ", iExculdeInt)
	
	WHILE (iRandInt = iExculdeInt)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "iRandInt was unsuitable: ", iRandInt, ", so regenerating")
		iRandInt = GET_RANDOM_INT_IN_RANGE(1, 5)
	ENDWHILE
	
	#IF FEATURE_HEIST_ISLAND
	IF IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
		IF iRandInt = 3
			iRandInt = 4 // Skipping variation 3 on submarine
		ENDIF
	ENDIF
	#ENDIF
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_ANIM_VAR_INT returning AnimVar: ", iRandInt)
	RETURN iRandInt
ENDFUNC

FUNC STRING GET_RIGHT_BEDSIDE_ANIM_DICT(INT iVar)
	
	SWITCH iVar
		CASE 1
			RETURN "anim@mp_bedmid@right_var_01"
		BREAK
		CASE 2
			RETURN "anim@mp_bedmid@right_var_02"
		BREAK
		CASE 3
			RETURN "anim@mp_bedmid@right_var_03"
		BREAK
		CASE 4
			RETURN "anim@mp_bedmid@right_var_04"
		BREAK
	ENDSWITCH
	RETURN "anim@mp_bedmid@right_var_01"
ENDFUNC

FUNC STRING GET_LEFT_BEDSIDE_ANIM_DICT(INT iVar)
	
	SWITCH iVar
		CASE 1
			RETURN "anim@mp_bedmid@left_var_01"
		BREAK
		CASE 2
			RETURN "anim@mp_bedmid@left_var_02"
		BREAK
		CASE 3
			RETURN "anim@mp_bedmid@left_var_03"
		BREAK
		CASE 4
			RETURN "anim@mp_bedmid@left_var_04"
		BREAK
	ENDSWITCH
	RETURN "anim@mp_bedmid@left_var_01"
ENDFUNC

FUNC STRING GET_ANIM_DICT(INT iVar, BOOL bRightSide = TRUE)
	STRING sReturn
	IF bRightSide 
		sReturn = GET_RIGHT_BEDSIDE_ANIM_DICT(iVar)
	ELSE
		sReturn = GET_LEFT_BEDSIDE_ANIM_DICT(iVar)
	ENDIF
	RETURN sReturn
ENDFUNC


FUNC BOOL IS_PROPERTY_A_VEHICLE_WAREHOUSE()
	
	SIMPLE_INTERIORS eSimpleInterior = GET_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN()

	IF eSimpleInterior > SIMPLE_INTERIOR_INVALID
	AND eSimpleInterior < SIMPLE_INTERIOR_END
		// Check if the local player is in a vehicle warehouse.
		IF GET_SIMPLE_INTERIOR_TYPE(eSimpleInterior) = SIMPLE_INTERIOR_TYPE_IE_GARAGE
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

PROC ASSIGN_ANIM_VARIATION()
	PLAYER_INDEX playerToCheck
	INT i
	BOOL bFoundPlayerInBed = FALSE
	IF NOT bFoundPlayerInBed
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
				playerToCheck = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
				IF IS_NET_PLAYER_OK(playerToCheck, FALSE)
				AND playerToCheck != PLAYER_ID()
					IF NOT IS_PLAYER_IN_YACHT_PROPERTY(PLAYER_ID())
						IF ARE_PLAYERS_IN_SAME_PROPERTY(PLAYER_ID(), playerToCheck, TRUE)
						AND NOT IS_PLAYER_ENTERING_PROPERTY(playerToCheck)
//							IF playerBD[NATIVE_TO_INT(playerToCheck)].iBedID = playerBD[NATIVE_TO_INT(PLAYER_ID())].iBedID
//							AND playerBD[NATIVE_TO_INT(PLAYER_ID())].iBedID != -1
								bFoundPlayerInBed = TRUE
//								playerBD[NATIVE_TO_INT(PLAYER_ID())].iAnimVar = GENERATE_ANIM_VAR_INT(playerBD[NATIVE_TO_INT(playerToCheck)].iAnimVar)
//								#IF IS_DEBUG_BUILD
//								CDEBUG1LN(DEBUG_SAFEHOUSE, "ASSIGN_ANIM_VARIATION: Other player: ", GET_PLAYER_NAME(playerToCheck)," is playering " , playerBD[NATIVE_TO_INT(playerToCheck)].iAnimVar, " so setting player ", GET_PLAYER_NAME(PLAYER_ID()), " has been assigned var ", playerBD[NATIVE_TO_INT(PLAYER_ID())].iAnimVar)
//								#ENDIF
//							ELSE
//								#IF IS_DEBUG_BUILD
//								CDEBUG1LN(DEBUG_SAFEHOUSE, "ASSIGN_ANIM_VARIATION: ", GET_PLAYER_NAME(playerToCheck), " is not in the same bed as local player, is in bed = ", playerBD[NATIVE_TO_INT(playerToCheck)].iBedID)
//								#ENDIF
//							ENDIF
						ENDIF
					ELSE
						IF GET_YACHT_PLAYER_IS_ON(playerToCheck) > -1
							IF GET_YACHT_PLAYER_IS_ON(PLAYER_ID()) = GET_YACHT_PLAYER_IS_ON(playerToCheck)
//								IF playerBD[NATIVE_TO_INT(playerToCheck)].iBedID = playerBD[NATIVE_TO_INT(PLAYER_ID())].iBedID
//								AND playerBD[NATIVE_TO_INT(PLAYER_ID())].iBedID != -1
									bFoundPlayerInBed = TRUE
//									playerBD[NATIVE_TO_INT(PLAYER_ID())].iAnimVar = GENERATE_ANIM_VAR_INT(playerBD[NATIVE_TO_INT(playerToCheck)].iAnimVar)
									#IF IS_DEBUG_BUILD
//									CDEBUG1LN(DEBUG_SAFEHOUSE, "ASSIGN_ANIM_VARIATION: Other player: ", GET_PLAYER_NAME(playerToCheck)," is playering " , playerBD[NATIVE_TO_INT(playerToCheck)].iAnimVar, " so setting player ", GET_PLAYER_NAME(PLAYER_ID()), " has been assigned var ", playerBD[NATIVE_TO_INT(PLAYER_ID())].iAnimVar)
									#ENDIF
//								ELSE
//									#IF IS_DEBUG_BUILD
//									CDEBUG1LN(DEBUG_SAFEHOUSE, "ASSIGN_ANIM_VARIATION: ", GET_PLAYER_NAME(playerToCheck), " is not in the same bed as local player, is in bed = ", playerBD[NATIVE_TO_INT(playerToCheck)].iBedID)
//									#ENDIF
//								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
								CDEBUG2LN(DEBUG_SAFEHOUSE, "ASSIGN_ANIM_VARIATION: ", GET_PLAYER_NAME(playerToCheck), " is not in the same yacht as ", GET_PLAYER_NAME(playerToCheck))
								#ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF	
			ENDIF
		ENDREPEAT
	ENDIF
	
	// No one was found in the same bed
//	IF NOT bFoundPlayerInBed
//		playerBD[NATIVE_TO_INT(PLAYER_ID())].iAnimVar = GENERATE_ANIM_VAR_INT()
//		IF IS_PROPERTY_A_VEHICLE_WAREHOUSE()
//			playerBD[NATIVE_TO_INT(PLAYER_ID())].iAnimVar = GENERATE_ANIM_VAR_INT(1) // Var 1 will task the ped to walk into a position in the wall
//		ENDIF
//	ENDIF
ENDPROC

PROC SetBedStage(TRIGGER_STATE nextState)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "SetStage: setting Stage from ", GetStageString(eTriggerState), ", to: ", GetStageString(nextState))
	eTriggerState = nextState
ENDPROC

PROC HID_HUD_AND_TICKER()
//	DISABLE_SELECTOR_THIS_FRAME()
//	DISABLE_ALL_MP_HUD_THIS_FRAME()
//	HIDE_HUD_AND_RADAR_THIS_FRAME()
//	THEFEED_HIDE_THIS_FRAME()
ENDPROC

BOOL bLeftAreaNearBed = FALSE
FUNC BOOL IS_PED_IN_RANGE_FOR_CONTEXT(BedStructData &bedStruct)
	BOOL bReturn = FALSE
	
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), bedStruct.vBoundingBoxA, bedStruct.vBoundingBoxB, bedStruct.fWidth)
		IF bLeftAreaNearBed
			bReturn = TRUE
		ENDIF
	ELSE
		IF bLeftAreaNearBed = FALSE
			bLeftAreaNearBed = TRUE
			CDEBUG3LN(DEBUG_SAFEHOUSE, "IS_PED_IN_RANGE_FOR_CONTEXT: Ped has left the area near the bed, we're now safe to trigger the context when they come back in range")
		ENDIF
	ENDIF

	RETURN bReturn
ENDFUNC

/// PURPOSE:
///    Updates sofa activity
PROC UPDATE_BED_ACTIVITY(BedStructData &bedStruct)

	FLOAT fRadius = 0.6 // Size of the ped capsule during sync scene.

	IF IS_PROPERTY_A_BUNKER()
	OR IS_PROPERTY_A_HANGAR()
	OR IS_PROPERTY_A_DEFUNCT_BASE()
	OR IS_PROPERTY_A_NIGHTCLUB()
	OR IS_PROPERTY_AN_ARENA_GARAGE()
	OR IS_PROPERTY_A_CASINO_APARTMENT()
	OR IS_PROPERTY_AN_ARCADE()
	OR IS_PROPERTY_A_SUBMARINE()
	OR IS_PROPERTY_A_AUTO_SHOP()
	#IF FEATURE_FIXER
	OR IS_PROPERTY_A_FIXER_HQ()
	#ENDIF
		IF IS_BIT_SET(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_RUNNING_SPAWN_ACTIVITY)
		OR IS_BIT_SET(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_SPAWNING_IN_SIMPLE_INTERIOR)
			// Dont show prompts when spawn activity is handled by warehouse script
			CDEBUG1LN(DEBUG_SAFEHOUSE, "UPDATE_BED_ACTIVITY: Performing bed spawn activity in simple interior, exiting procedure.")
			EXIT
		ENDIF
	ENDIF
	
	SWITCH eTriggerState
		
		CASE TS_PLAYER_OUT_OF_RANGE
						
			IF iPersonalAptActivity = iActivityID //ci_APT_ACT_BED_HI
				iPersonalAptActivity = ci_APT_ACT_IDLE
			ENDIF
			
			CDEBUG3LN(DEBUG_SAFEHOUSE, "UPDATE_BED_ACTIVITY: g_SpawnData.iDrunkRespawnYachtState = ", g_SpawnData.iDrunkRespawnYachtState )
			CDEBUG3LN(DEBUG_SAFEHOUSE, "UPDATE_BED_ACTIVITY: g_SpawnData.bPassedOutDrunk  = ", g_SpawnData.bPassedOutDrunk )
			
			IF GET_SPAWN_ACTIVITY() = SPAWN_ACTIVITY_BED
			#IF FEATURE_HEIST_ISLAND
			AND NOT IS_PROPERTY_A_SUBMARINE()
			#ENDIF
				IF (g_SpawnData.bPassedOutDrunk and g_SpawnData.iDrunkRespawnYachtState = 2)
				or !g_SpawnData.bPassedOutDrunk
					
					IF g_SpawnData.iDrunkRespawnYachtState = 2
						bDrunkWakeUp = TRUE
					ENDIF
					
					vSceneHead = bedStruct.vSceneRot
					vScenePos = bedStruct.vScenePos
//					vTriggerPos = vTriggerPos1
//					fTriggerHead = fTriggerHead1
					bRightBedSide = bedStruct.bEnterRightBedSide
					
					STRING sSpawnAnimDict
					sSpawnAnimDict = sAnimDictionary					
					sSpawnAnimDict = GET_RIGHT_BEDSIDE_ANIM_DICT(1)

					eStoredMask = GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_BERD)
					CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "UPDATE_BED_ACTIVITY: TS_PLAYER_OUT_OF_RANGE: FINALIZE_HEAD_BLEND")
					FINALIZE_HEAD_BLEND(PLAYER_PED_ID())
					iScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vScenePos, vSceneHead, EULER_YXZ, FALSE, TRUE)
					
					STRING sIdleAnim
					sIdleAnim = GET_IDLE_ANIM(bedStruct.bEnterRightBedSide)
					
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iScene, sSpawnAnimDict, sIdleAnim, NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON)				
					CDEBUG1LN(DEBUG_SAFEHOUSE, "UPDATE_BED_ACTIVITY: TS_PLAYER_OUT_OF_RANGE: running sync scene")
					NETWORK_START_SYNCHRONISED_SCENE(iScene)
					
					//g_showerGlobals.bPauseSkyCamForSpawn = FALSE
					//CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "UPDATE_BED_ACTIVITY: TS_PLAYER_OUT_OF_RANGE: g_showerGlobals.bPauseSkyCamForSpawn = FALSE")
					
					SetBedStage(TS_RUN_EXIT_ANIM)
				ENDIF
			ELSE
							
//				IF MP_IS_APARTMENT_ACTIVITY_FREE_AND_PLAYER_IS_AVAILABLE(iActivityID) //(ci_APT_ACT_BED_HI)	
//					IF IS_TRIGGER_AREA_OK(vTriggerPos, vTriggerSize)
					IF NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
					AND NOT IS_PLAYER_ON_PAUSE_MENU(PLAYER_ID())
					#IF FEATURE_GTAO_MEMBERSHIP
					AND NOT SHOULD_LAUNCH_MEMBERSHIP_PAGE_BROWSER()
					#ENDIF
					AND NOT IS_PED_RUNNING(PLAYER_PED_ID())
					AND NOT IS_PED_SPRINTING(PLAYER_PED_ID())
					AND NOT NETWORK_IS_IN_MP_CUTSCENE()
					AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
					AND NOT IS_PLAYER_USING_BED_ACTIVITY()
						IF IS_PED_IN_RANGE_FOR_CONTEXT(bedStruct)
						AND IS_PLAYER_ALONE_IN_BED_AREA(bedStruct.vBoundingBoxB, bedStruct.vBoundingBoxA)
							IF NOT g_bRadioActTriggered
								IF iContextIntentionMain = NEW_CONTEXT_INTENTION
									REGISTER_CONTEXT_INTENTION(iContextIntentionMain, CP_MEDIUM_PRIORITY, "SA_BED_IN")
								ENDIF
								
								IF HAS_CONTEXT_BUTTON_TRIGGERED(iContextIntentionMain)
									SET_PLAYER_USING_BED_ACTIVITY(TRUE)
									sAnimDictionary = GET_ANIM_DICT(GENERATE_ANIM_VAR_INT(), bRightBedSide)								
									SetBedStage(TS_WAIT_FOR_PLAYER)
								ENDIF
							ELSE
								CDEBUG1LN(DEBUG_SAFEHOUSE, "Tried to trigger bed but player as already activated radio, so disregarding")
							ENDIF	
						ELSE
							RELEASE_CONTEXT_INTENTION(iContextIntentionMain)
						ENDIF
						
					ELSE
						RELEASE_CONTEXT_INTENTION(iContextIntentionMain)
						
						#IF IS_DEBUG_BUILD
						
//						IF NOT IS_TRIGGER_AREA_OK(vTriggerPos, vTriggerSize)
//							CDEBUG2LN(DEBUG_SAFEHOUSE, "CDM: ob_mp_bed_high - trigger area not ok, Trigger area = ", (vTriggerPos))
//						ENDIF
						IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vTriggerAreaA, vTriggerAreaB, AREA_WIDTH)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "CDM: ob_mp_bed_high - player not in trigger area. vTriggerAreaA.", vTriggerAreaA, "vTriggerAreaB", vTriggerAreaB)
						ENDIF
						IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) != GET_INTERIOR_AT_COORDS(vTriggerAreaA)
							CDEBUG2LN(DEBUG_SAFEHOUSE, "CDM: ob_mp_bed_high - interior does not match, Player = ", NATIVE_TO_INT(GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())), ", Brain Object = )")//, NATIVE_TO_INT(GET_INTERIOR_FROM_ENTITY(mTrigger)))
						ENDIF
						#ENDIF
					ENDIF
//				ELSE
//					#IF IS_DEBUG_BUILD
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "CDM: bed - ", iActivityID ," not free!")
//					#ENDIF
//				ENDIF
			ENDIF
			
		BREAK
		
		CASE TS_WAIT_FOR_PLAYER
				
			IF CAN_PLAYER_START_CUTSCENE()	
			AND HAS_ANIM_DICT_LOADED_FOR_BED()
			AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vTriggerAreaB, vTriggerAreaA, AREA_WIDTH)
				
				eStoredMask = GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_BERD)
				
				IF IS_ITEM_A_HELMET(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_PROPS, GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, ENUM_TO_INT(ANCHOR_HEAD)))
					iStoredHat = GET_PED_PROP_INDEX( PLAYER_PED_ID(), ANCHOR_HEAD)
					iStoredHatTex = GET_PED_PROP_TEXTURE_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD)
					SET_BIT(iPlayerStateBitSet, BIT_INDEX_WEARING_HELMET)
				ENDIF			
				
				IF IS_PED_WEARING_HELMET(PLAYER_PED_ID())			
					// If the player is already wearing a helmet whilst getting 
					// into bed, then keep it on.
					SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_HasHelmet, TRUE)
					SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontTakeOffHelmet, TRUE)
					
					#IF IS_DEBUG_BUILD
						CDEBUG1LN(DEBUG_SAFEHOUSE, "TS_WAIT_FOR_PLAYER - Setting PED_CONFIG_FLAG: PCF_DontTakeOffHelmet - TRUE")
					#ENDIF
				ENDIF
				
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH] - mid level bed TRIGGERED...")
				
				RELEASE_CONTEXT_INTENTION(iContextIntentionMain)
				DISABLE_INTERACTION_MENU()				
				
				SET_BIT(iPlayerStateBitSet, BIT_INDEX_DISABLED_PIM)
				
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_BED_IN")
					CLEAR_HELP()
				ENDIF
				
				iPersonalAptActivity = iActivityID// ci_APT_ACT_BED_HI
				
				CLEAR_AREA_OF_PROJECTILES(vTriggerPos, 3.0)
				
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
//				bSafehouseSetControlOff = TRUE
				
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)	
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)

				SETUP_PC_CONTROLS()
				
				CDEBUG1LN(DEBUG_SAFEHOUSE, "TS_WAIT_FOR_PLAYER: High Bed: bInitOffsets: fTriggerHead = ", fTriggerHead)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "TS_WAIT_FOR_PLAYER: High Bed: bInitOffsets: vTriggerAreaA = ", vTriggerAreaA)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "TS_WAIT_FOR_PLAYER: High Bed: bInitOffsets: vTriggerAreaB = ", vTriggerAreaB)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "TS_WAIT_FOR_PLAYER: High Bed: bInitOffsets: vTriggerPos = ", vTriggerPos)
				
				//mNavStruct.m_fSlideToCoordHeading = fTriggerHead		
				vTriggerPos = GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictionary, GET_ENTER_ANIM(bRightBedSide), vScenePos, vSceneHead)  
				VECTOR vTempRotation 
				vTempRotation = GET_ANIM_INITIAL_OFFSET_ROTATION(sAnimDictionary, GET_ENTER_ANIM(bRightBedSide), vScenePos, vSceneHead)  
				fTriggerHead = vTempRotation.Z
				
				CDEBUG1LN(DEBUG_SAFEHOUSE, "TS_WAIT_FOR_PLAYER: vTriggerPos = ", vTriggerPos)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "fTriggerHead: ", fTriggerHead)
				
				CDEBUG1LN(DEBUG_SAFEHOUSE, "vScenePos: ", vScenePos)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "vSceneHead: ", vSceneHead)
				
//				VECTOR vTempPos 
//				vTempPos = vTriggerPos - vScenePos
//				FLOAT fTempFloat 
//				fTempFloat = fTriggerHead - vTempRotation.Z
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "vTriggerPos - vScenePos: ", vTempPos)
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "fTriggerHead - vSceneHead: ", fTempFloat)

				CDEBUG1LN(DEBUG_SAFEHOUSE, "TS_WAIT_FOR_PLAYER: vTriggerPos = ", vTriggerPos)
				TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), vTriggerPos, PEDMOVEBLENDRATIO_WALK, 5000, fTriggerHead, bedStruct.fTargetRadius)
//				TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), vTriggerPos, PEDMOVEBLENDRATIO_WALK, 60000, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, fTriggerHead)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH] High level Bed - TS_SEAT_PLAYER")
				
				IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
					SET_PED_DESIRED_HEADING(PLAYER_PED_ID(), GET_HEADING_BETWEEN_VECTORS_2D(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerAreaA))
					PRINTLN("POD: SET_PED_DESIRED_HEADING: ", GET_HEADING_BETWEEN_VECTORS_2D(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerAreaA))
					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_AIMING)
					//SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_ForcePedToStrafe, TRUE)
				ENDIF
				
				SetBedStage(TS_RUN_ENTRY_ANIM)
			ELSE		
				#IF IS_DEBUG_BUILD
				IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vTriggerAreaB, vTriggerAreaA, AREA_WIDTH)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "TS_WAIT_FOR_PLAYER: player isn't in either angled area anymore, therefore going to TS_PLAYER_OUT_OF_RANGE")
					SET_PLAYER_USING_BED_ACTIVITY(FALSE)
					SetBedStage(TS_PLAYER_OUT_OF_RANGE)
				ENDIF
				#ENDIF
			ENDIF
		BREAK
		
		CASE TS_RUN_ENTRY_ANIM
			HID_HUD_AND_TICKER()
//			DISABLE_SELECTOR_THIS_FRAME()
////			DISABLE_ALL_MP_HUD_THIS_FRAME()
////			HIDE_HUD_AND_RADAR_THIS_FRAME()
//			THEFEED_HIDE_THIS_FRAME()
			
			SET_PED_CAPSULE(PLAYER_PED_ID(), fRadius)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[SAFEHOUSE][BED] RUN_BED_LOGIC - SET_PED_CAPSULE to ", fRadius, " this frame.")
			
			IF IS_PED_WEARING_HAZ_HOOD_UP(PLAYER_PED_ID())
				SWAP_CHEM_SUIT_HOOD_FOR_ALTERNATE(PLAYER_PED_ID())
				bSwappedChemSuit = TRUE
			ENDIF
			
			SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_BERD, BERD_FMM_0_0, FALSE)

			REMOVE_MP_HEIST_GEAR(PLAYER_PED_ID(), HEIST_GEAR_NIGHTVISION)
			IF IS_PED_WEARING_PILOT_SUIT(PLAYER_PED_ID(), PED_COMP_TEETH)
				PRINTLN("bed: removing pilot suit")
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TEETH, 0, 0)
			ENDIF
//			// Initialise camera
//			mCam = CREATE_CAMERA()
//			
//			IF DOES_CAM_EXIST(mCam)
//				SET_CAM_ACTIVE(mCam, TRUE)
//				RENDER_SCRIPT_CAMS(TRUE, FALSE)
//				SET_CAM_PARAMS(mCam, vCamPos, vCamHead, fCamFov)
//				SHAKE_CAM(mCam, "HAND_SHAKE", DEFAULT_CAM_SHAKE)
//			ENDIF
			IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
				SET_PED_DESIRED_HEADING(PLAYER_PED_ID(), GET_HEADING_BETWEEN_VECTORS_2D(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerAreaA))
				//SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_ForcePedToStrafe, TRUE)
			ENDIF
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> PERFORMING_TASK
//			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = FINISHED_TASK
				iScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vScenePos, vSceneHead, DEFAULT, TRUE)
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iScene, sAnimDictionary, GET_ENTER_ANIM( bRightBedSide ), SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT)/*|SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON)*/
				 
				NETWORK_START_SYNCHRONISED_SCENE(iScene)
							
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH] High level Bed - TS_CHECK_ENTRY_ANIM")
				SetBedStage(TS_CHECK_ENTRY_ANIM)
				
			ENDIF
			
		BREAK
		
		CASE TS_CHECK_ENTRY_ANIM	
			HID_HUD_AND_TICKER()
//			DISABLE_SELECTOR_THIS_FRAME()
//			DISABLE_ALL_MP_HUD_THIS_FRAME()
//			HIDE_HUD_AND_RADAR_THIS_FRAME()
//			THEFEED_HIDE_THIS_FRAME()
			
			iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iScene)
			                  
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH] High level Bed - TS_RUN_IDLE_ANIM")	
				bSleeping = TRUE
				SetBedStage(TS_RUN_IDLE_ANIM)
			ENDIF
		
		BREAK
		
		CASE TS_RUN_IDLE_ANIM

			#IF IS_DEBUG_BUILD
				PRINTLN("POD: BED: in TS_RUN_IDLE_ANIM	")
			#ENDIF
			
			HID_HUD_AND_TICKER()
//			DISABLE_SELECTOR_THIS_FRAME()
//			DISABLE_ALL_MP_HUD_THIS_FRAME()
//			HIDE_HUD_AND_RADAR_THIS_FRAME()
//			THEFEED_HIDE_THIS_FRAME()

			SET_PED_CAPSULE(PLAYER_PED_ID(), fRadius)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[SAFEHOUSE][BED] RUN_BED_LOGIC - SET_PED_CAPSULE to ", fRadius, " this frame.")
			
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
			OR GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene) > 0.9
				
				bSleeping = TRUE
				
				iScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vScenePos, vSceneHead, EULER_YXZ, FALSE, TRUE)
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iScene, sAnimDictionary, GET_IDLE_ANIM( bRightBedSide ), NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT)/*|SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON)*/
								
				NETWORK_START_SYNCHRONISED_SCENE(iScene)
								
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH] High level Bed - TS_CHECK_IDLE_ANIM")
				SetBedStage(TS_CHECK_IDLE_ANIM)
			ENDIF
			
		BREAK
		
		CASE TS_CHECK_IDLE_ANIM
		
			#IF IS_DEBUG_BUILD
				PRINTLN("POD: BED: in TS_CHECK_IDLE_ANIM	")
			#ENDIF
			HID_HUD_AND_TICKER()
//			DISABLE_SELECTOR_THIS_FRAME()
//			DISABLE_ALL_MP_HUD_THIS_FRAME()
//			HIDE_HUD_AND_RADAR_THIS_FRAME()
//			THEFEED_HIDE_THIS_FRAME()
			
			iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iScene)
			                  
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
				
				SET_SYNCHRONIZED_SCENE_LOOPED(iLocalScene, TRUE)				
			
				/*
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_BED_OUT")
					PRINT_HELP_FOREVER("SA_BED_OUT")
				ENDIF
				*/
				
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH] High level Bed - TS_WAIT_FOR_INPUT")		
				SetBedStage(TS_WAIT_FOR_INPUT)
			ENDIF
			
		BREAK
				
		CASE TS_WAIT_FOR_INPUT
		
			#IF IS_DEBUG_BUILD
				PRINTLN("POD: BED: in TS_WAIT_FOR_INPUT	")
			#ENDIF
			
			IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
			AND NOT IS_NEW_LOAD_SCENE_ACTIVE()
			AND NOT IS_PLAYER_TELEPORT_ACTIVE()
			AND NOT IS_PLAYER_DOING_WARP_TO_SPAWN_LOCATION(PLAYER_ID())
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ENDIF
			
//			HID_HUD_AND_TICKER()
//			DISABLE_SELECTOR_THIS_FRAME()
//			DISABLE_ALL_MP_HUD_THIS_FRAME()
//			HIDE_HUD_AND_RADAR_THIS_FRAME()
//			THEFEED_HIDE_THIS_FRAME()

			SET_PED_CAPSULE(PLAYER_PED_ID(), fRadius)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[SAFEHOUSE][BED] RUN_BED_LOGIC - SET_PED_CAPSULE to ", fRadius, " this frame.")
			
			IF NOT IS_SELECTOR_ONSCREEN()
			AND NOT IS_CELLPHONE_CAMERA_IN_USE()
			AND NOT IS_INTERACTION_MENU_OPEN()
			AND NOT IS_BROWSER_OPEN()
			AND NOT NETWORK_IS_IN_MP_CUTSCENE()
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_BED_OUT")
					PRINT_HELP_FOREVER("SA_BED_OUT")
				ENDIF
			ELSE
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_BED_OUT")
					CLEAR_HELP()
				ENDIF
			ENDIF
					
			IF NOT IS_PLAYER_ON_PAUSE_MENU(PLAYER_ID())
			#IF FEATURE_GTAO_MEMBERSHIP
			AND NOT SHOULD_LAUNCH_MEMBERSHIP_PAGE_BROWSER()
			#ENDIF
			AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RUP)
			OR IS_PLAYER_IN_CORONA()
//				HIDE_HUD_AND_RADAR_THIS_FRAME()
//				THEFEED_HIDE_THIS_FRAME()
				HID_HUD_AND_TICKER()
				
				bSleeping = FALSE
				
				// Clear the help message
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_BED_OUT")
					CLEAR_HELP()
				ENDIF
							
				iScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vScenePos, vSceneHead)
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iScene, sAnimDictionary, GET_EXIT_ANIM( bRightBedSide ), REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_ON_ABORT_STOP_SCENE)/*|SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON)*/
			                  
				NETWORK_START_SYNCHRONISED_SCENE(iScene)
			
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH] High level Bed - TS_RUN_EXIT_ANIM")
				
				CLEANUP_PC_CONTROLS()
								
				SetBedStage(TS_CHECK_EXIT_ANIM)
			ENDIF
		BREAK
		
		CASE TS_RUN_EXIT_ANIM
		
			SET_PED_CAPSULE(PLAYER_PED_ID(), fRadius)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[SAFEHOUSE][BED] RUN_BED_LOGIC - SET_PED_CAPSULE to ", fRadius, " this frame.")
		
			#IF IS_DEBUG_BUILD
			PRINTLN("POD: BED: TS_RUN_EXIT_ANIM: IS_SKYSWOOP_IN_SKY: ", IS_SKYSWOOP_IN_SKY())		
			#ENDIF
			IF NOT IS_SKYSWOOP_IN_SKY()
			AND IS_SKYSWOOP_AT_GROUND() 
			
				IF IS_SCREEN_FADED_OUT()
				AND NOT IS_SCREEN_FADING_IN()
				#IF FEATURE_SHORTEN_SKY_HANG
				AND NOT (g_bSkySwoopDownDidFadeOut)
				#ENDIF
					DO_SCREEN_FADE_IN(1000)
				ENDIF					
	
				bInTransition = TRUE
				
				STRING sSpawnAnimDict
				sSpawnAnimDict = sAnimDictionary
				
				IF GET_SPAWN_ACTIVITY() = SPAWN_ACTIVITY_BED
					sSpawnAnimDict = GET_ANIM_DICT(1, bRightBedSide)
				ENDIF
						
				iScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vScenePos, vSceneHead)
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iScene, sSpawnAnimDict, GET_EXIT_ANIM( bRightBedSide ), INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT)/*|SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON)*/

				PRINTLN("POD: BED: TS_RUN_EXIT_ANIM: Dict: ", sAnimDictionary, " ANIM: ", GET_EXIT_ANIM(bRightBedSide))
				
				NETWORK_START_SYNCHRONISED_SCENE(iScene)
				SetBedStage(TS_CHECK_EXIT_ANIM)
			ENDIF
		BREAK
		
		CASE TS_CHECK_EXIT_ANIM
		
			#IF IS_DEBUG_BUILD
				PRINTLN("POD: BED: in TS_CHECK_EXIT_ANIM")
			#ENDIF
			
			IF NOT bInTransition
				HID_HUD_AND_TICKER()
//				DISABLE_SELECTOR_THIS_FRAME()
//				DISABLE_ALL_MP_HUD_THIS_FRAME()
//				HIDE_HUD_AND_RADAR_THIS_FRAME()
//				THEFEED_HIDE_THIS_FRAME()
			ENDIF
			
			iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iScene)
			                  
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
				
				SET_SPAWN_ACTIVITY_OBJECT_SCRIPT_AS_READY(SPAWN_ACTIVITY_BED)
				
				IF bDrunkWakeUp
					SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
				ENDIF
				
				g_showerGlobals.bPauseSkyCamForSpawn = FALSE
				CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "UPDATE_BED_ACTIVITY: TS_CHECK_EXIT_ANIM: g_showerGlobals.bPauseSkyCamForSpawn = FALSE")
				
//				 Change the camera angle - non-shake
//				IF DOES_CAM_EXIST(mCam)
//					SET_CAM_PARAMS(mCam, vExitCamPos, vExitCamHead, fExitCamFov)
//				ENDIF
				
				IF g_SimpleInteriorData.eSimpleInteriorIDToSpawnInDrunk != SIMPLE_INTERIOR_INVALID
					g_SimpleInteriorData.eSimpleInteriorIDToSpawnInDrunk = SIMPLE_INTERIOR_INVALID
					PRINTLN("UPDATE_BED_ACTIVITY - g_SimpleInteriorData.eSimpleInteriorIDToSpawnInDrunk = SIMPLE_INTERIOR_INVALID")
				ENDIF
				
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH] High level Bed - TS_RESET")		
				SetBedStage(TS_RESET)
			ENDIF
			
		BREAK
		
		CASE TS_RESET
			
//			DISABLE_SELECTOR_THIS_FRAME()
//			HIDE_HUD_AND_RADAR_THIS_FRAME()
			HID_HUD_AND_TICKER()
			SET_SPAWN_ACTIVITY(SPAWN_ACTIVITY_NOTHING)
			
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
			OR HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("BREAK_OUT"))
						
			
				IF bDrunkWakeUp
				AND NOT IS_NEW_LOAD_SCENE_ACTIVE()
				AND NOT IS_PLAYER_TELEPORT_ACTIVE()
				AND NOT IS_PLAYER_DOING_WARP_TO_SPAWN_LOCATION(PLAYER_ID())
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					bDrunkWakeUp = FALSE
				ENDIF
				
				IF HAS_ANIM_EVENT_FIRED (PLAYER_PED_ID(), HASH("BREAK_OUT"))
					CDEBUG1LN(DEBUG_SAFEHOUSE, "TS_RESET: Finishing anim early as BREAK_OUT anim event fired")
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "TS_RESET: Finishing anim finished at phase: ", GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene))
						NETWORK_STOP_SYNCHRONISED_SCENE(iScene)
					ENDIF
					
				ENDIF
				
//				playerBD[NATIVE_TO_INT(PLAYER_ID())].iAnimVar = -1
//				playerBD[NATIVE_TO_INT(PLAYER_ID())].iBedID = -1
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "TS_RESET: reseting iAnimVar to -1")
				
				IF bSwappedChemSuit = TRUE
					SWAP_CHEM_SUIT_HOOD_FOR_ALTERNATE(PLAYER_PED_ID())
					bSwappedChemSuit = FALSE					
				ENDIF
				SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_BERD, eStoredMask, FALSE)
				
				IF IS_BIT_SET(iPlayerStateBitSet, BIT_INDEX_WEARING_HELMET)
					SET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD, iStoredHat, iStoredHatTex)
					CLEAR_BIT(iPlayerStateBitSet, BIT_INDEX_WEARING_HELMET)
				ENDIF
				PRINTLN("POD: BED: REAPPLYING MASK")
				
				ENABLE_INTERACTION_MENU()
				
				CLEAR_BIT(iPlayerStateBitSet, BIT_INDEX_DISABLED_PIM)
				
				iPersonalAptActivity = ci_APT_ACT_IDLE
				
//				IF bSafehouseSetControlOff
				
				IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
				AND NOT IS_PLAYER_TELEPORT_ACTIVE()
				AND NOT IS_PLAYER_DOING_WARP_TO_SPAWN_LOCATION(PLAYER_ID())
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				ENDIF
					
//					bSafehouseSetControlOff = FALSE
//				ENDIF
				FINALIZE_HEAD_BLEND(PLAYER_PED_ID())
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				
				RELEASE_CONTEXT_INTENTION(iContextIntentionMain)
				RELEASE_CONTEXT_INTENTION(iContextIntentionRight)
				RELEASE_CONTEXT_INTENTION(iContextIntentionWH)
				
				SET_PLAYER_USING_BED_ACTIVITY(FALSE)
				
				SetBedStage(TS_PLAYER_OUT_OF_RANGE)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "TS_RESET: going to TS_PLAYER_OUT_OF_RANGE")
			ENDIF
			
		BREAK
					
	ENDSWITCH
ENDPROC

PROC PROCESS_PRE_GAME_BED(BedStructData &bedStruct)
	IF NETWORK_IS_GAME_IN_PROGRESS()
		
		NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, bedStruct.iScriptInstanceID)
		
		// This makes sure the net script is active, waits until it is.
		HANDLE_NET_SCRIPT_INITIALISATION()
		
//		NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
		
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[Safehouse] High Bed: Ready for network launch...")
		
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
		
	ENDIF	
ENDPROC

PROC BED_SCRIPT_LAUNCH_LOGIC(BedStructData &bedStruct)
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), bedStruct.vBoundingBoxA, bedStruct.vBoundingBoxB, bedStruct.fWidth)
		IF NOT NETWORK_IS_SCRIPT_ACTIVE("mp_bed_high", bedStruct.iScriptInstanceID, TRUE)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[HS]AM_VEHICLE_SPAWN: Requesting script: mp_bed_high")
			REQUEST_SCRIPT("mp_bed_high")
			
			IF HAS_SCRIPT_LOADED("mp_bed_high")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "BED_SCRIPT_LAUNCH_LOGIC: HAS_SCRIPT_LOADED = TRUE for script: luxe_veh_activity")
			    START_NEW_SCRIPT_WITH_ARGS("mp_bed_high", bedStruct, SIZE_OF(bedStruct), DEFAULT_STACK_SIZE)
			    SET_SCRIPT_AS_NO_LONGER_NEEDED("mp_bed_high")

			ENDIF
		ELSE
			CDEBUG2LN(DEBUG_SAFEHOUSE, "BED_SCRIPT_LAUNCH_LOGIC: NETWORK_IS_SCRIPT_ACTIVE: mp_bed_high = TRUE")
			
		ENDIF
//	ELSE
//		CDEBUG2LN(DEBUG_SAFEHOUSE, "BED_SCRIPT_LAUNCH_LOGIC: NETWORK_IS_SCRIPT_ACTIVE: mp_bed_high = TRUE, bedStruct.vBoundingBoxA: ", bedStruct.vBoundingBoxA, ", bedStruct.vBoundingBoxB: ", bedStruct.vBoundingBoxB)
	ENDIF
ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================
PROC RUN_BED_LOGIC(BedStructData &bedStruct)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND (NOT IS_PLAYER_SWITCH_IN_PROGRESS() OR GET_SPAWN_ACTIVITY() = SPAWN_ACTIVITY_BED)
	AND NOT SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
	AND NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_USING_MP_RADIO)
	
		IF NOT bInitOffsets
		
			vTriggerAreaA 	= bedStruct.vBoundingBoxA
			vTriggerAreaB 	= bedStruct.vBoundingBoxB
			vScenePos 		= bedStruct.vScenePos
			vSceneHead		= bedStruct.vSceneRot
			AREA_WIDTH 		= bedStruct.fWidth
			bRightBedSide 	= bedStruct.bEnterRightBedSide
			bInitOffsets 	= TRUE				
			
			CDEBUG1LN(DEBUG_SAFEHOUSE, "High Bed: bInitOffsets: fTriggerHead = ", fTriggerHead)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "High Bed: bInitOffsets: vTriggerAreaA = ", vTriggerAreaA)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "High Bed: bInitOffsets: vTriggerAreaB = ", vTriggerAreaB)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "High Bed: bInitOffsets: vTriggerPos = ", vTriggerPos)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "High Bed: bInitOffsets: vScenePos = ", vScenePos)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "High Bed: bInitOffsets: vSceneHead = ", vSceneHead)
			
			IF NOT ARE_VECTORS_EQUAL(vTriggerAreaA, <<0, 0, 0>>)
				bInitOffsets = TRUE	
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "Bed activity retrieved origin coords for bed coords, so waiting till bedPropertyStruct is referencing the correct data structure")
			ENDIF
			
		ELSE
			IF GET_DISTANCE_BETWEEN_COORDS(vTriggerAreaA, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) < 5//SOFA_TRIGGER_DIST					
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "[Safehouse] High Bed: < SOFA_TRIGGER_DIST: eBedState: ", eBedState)
				SWITCH eBedState				
					CASE AS_LOAD_ASSETS		
						IF HAS_ANIM_DICT_LOADED_FOR_BED()
							CDEBUG1LN(DEBUG_SAFEHOUSE, "[MP safehouse] High Bed: Assets loaded for activity...")
							eBedState = AS_RUN_ACTIVITY
							
							IF GET_SPAWN_ACTIVITY() != SPAWN_ACTIVITY_BED
								bLeftAreaNearBed = TRUE
							ENDIF
						ENDIF
					BREAK
					CASE AS_RUN_ACTIVITY
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							UPDATE_BED_ACTIVITY(bedStruct)
						ENDIF
					BREAK				
					CASE AS_END

					BREAK
				ENDSWITCH
			ELSE
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "[Safehouse] High Bed: > SOFA_TRIGGER_DIST: ", GET_DISTANCE_BETWEEN_COORDS(vTriggerPos, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)))
				IF GET_DISTANCE_BETWEEN_COORDS(vTriggerAreaA, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) > SOFA_TRIGGER_DIST+3
					#IF IS_DEBUG_BUILD
					vTempDebugVector = GET_ENTITY_COORDS(PLAYER_PED_ID())
					VECTOR vTempDebugVectorObjectCoords = vTriggerAreaA
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[MP safehouse] High Bed: Player has moved far enough away, 1, object: ",vTempDebugVectorObjectCoords, " player:", vTempDebugVector)
					FLOAT fTempFloat  = GET_DISTANCE_BETWEEN_COORDS(vTriggerAreaA, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) 
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[MP safehouse] High Bed: Player has moved far enough away, distance from object: ",fTempFloat)
					#ENDIF
					
					CLEANUP_BED_ACTIVITY()
				ENDIF
			ENDIF
		ENDIF

		IF GET_DISTANCE_BETWEEN_COORDS(vTriggerAreaA, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) > SOFA_TRIGGER_DIST+3
			#IF IS_DEBUG_BUILD
			vTempDebugVector = GET_ENTITY_COORDS(PLAYER_PED_ID())
			VECTOR vTempDebugVectorObjectCoords = vTriggerAreaA
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[MP safehouse] High Bed: Player has moved far enough away, 2, object: ",vTempDebugVectorObjectCoords, " player:", vTempDebugVector)
			FLOAT fTempFloat  = GET_DISTANCE_BETWEEN_COORDS(vTriggerAreaA, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) 
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[MP safehouse] High Bed: Player has moved far enough away, distance from object: ",fTempFloat)
			#ENDIF
			
			CLEANUP_BED_ACTIVITY()
		ENDIF
	ELSE
		CLEANUP_BED_ACTIVITY()
	ENDIF
ENDPROC
