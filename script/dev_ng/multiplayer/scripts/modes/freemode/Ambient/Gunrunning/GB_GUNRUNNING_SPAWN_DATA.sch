USING "globals.sch"


PROC GET_PREDEFINED_DROPOFF_SPAWN_DATA(FREEMODE_DELIVERY_DROPOFFS eDropoffID, INT iPointIndex, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)

	SWITCH eDropoffID
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_1
			SWITCH iPointIndex
				CASE 0
				    vSpawnPoint 	= <<17.5349, 6573.1323, 31.7158>>
				    fSpawnHeading 	= 240.1994    
				BREAK
				CASE 1
				    vSpawnPoint 	= <<14.8945, 6575.8086, 31.7158>> 
				    fSpawnHeading 	= 238.9994
				BREAK
				CASE 2
				    vSpawnPoint 	= <<20.6651, 6570.3550, 30.6121>> 
				    fSpawnHeading 	= 240.1994
				BREAK
				CASE 3
				    vSpawnPoint 	= <<26.0199, 6574.6753, 30.5131>>
				    fSpawnHeading 	= 240.1994 
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_2
			SWITCH iPointIndex
				CASE 0
				    vSpawnPoint 	= <<412.8073, 6622.1787, 27.0666>> 
				    fSpawnHeading 	= 237.9986
				BREAK
				CASE 1
				    vSpawnPoint 	= <<414.6512, 6624.5527, 27.0842>> 
				    fSpawnHeading 	= 222.3986
				BREAK
				CASE 2
				    vSpawnPoint 	= <<418.1367, 6622.2842, 26.7583>>
				    fSpawnHeading 	= 211.1986	
				BREAK
				CASE 3
				    vSpawnPoint 	= <<417.6506, 6616.3989, 26.4138>> 
				    fSpawnHeading 	= 206.9986 	
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_3
			SWITCH iPointIndex 
				CASE 0
				    vSpawnPoint 	= <<8.8103, 6525.5127, 30.4204>> 
				    fSpawnHeading 	= 55.5983
				BREAK
				CASE 1
				    vSpawnPoint 	= <<7.4254, 6527.8735, 30.4543>>
				    fSpawnHeading 	= 66.5983
				BREAK
				CASE 2
				    vSpawnPoint 	= <<11.1684, 6527.2412, 30.3547>> 
				    fSpawnHeading 	= 66.5983
				BREAK
				CASE 3
				    vSpawnPoint 	= <<5.9900, 6525.4712, 30.4680>> 
				    fSpawnHeading 	= 59.7982
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_4
			SWITCH iPointIndex 
				CASE 0
				    vSpawnPoint 	= <<-195.3279, 6463.1318, 29.8997>> 
				    fSpawnHeading 	= 251.7979
				BREAK
				CASE 1
				    vSpawnPoint 	= <<-198.0305, 6457.6387, 29.9715>> 
				    fSpawnHeading 	= 247.5979
				BREAK
				CASE 2
				    vSpawnPoint 	= <<-193.2977, 6464.5513, 29.6835>> 
				    fSpawnHeading 	= 251.7979
				BREAK
				CASE 3
				    vSpawnPoint 	= <<-199.5536, 6455.8496, 30.0525>> 
				    fSpawnHeading 	= 238.9979
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_5
			SWITCH iPointIndex 
				CASE 0
				    vSpawnPoint 	= <<-222.3729, 6431.6401, 30.2021>> 
				    fSpawnHeading 	= 244.9979
				BREAK
				CASE 1
				    vSpawnPoint 	= <<-220.4680, 6433.8340, 30.1975>> 
				    fSpawnHeading 	= 244.9979
				BREAK
				CASE 2
				    vSpawnPoint 	= <<-220.5028, 6429.0884, 30.2131>> 
				    fSpawnHeading 	= 244.9979
				BREAK
				CASE 3
				    vSpawnPoint 	= <<-217.9140, 6432.0356, 30.1977>> 
				    fSpawnHeading 	= 239.3978
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_6
			SWITCH iPointIndex 
				CASE 0
				    vSpawnPoint 	= <<-132.9801, 6334.5225, 30.4461>> 
				    fSpawnHeading 	= 157.1977
				BREAK
				CASE 1
				    vSpawnPoint 	= <<-139.9169, 6336.3477, 30.5915>> 
				    fSpawnHeading 	= 138.9975
				BREAK
				CASE 2
				    vSpawnPoint 	= <<-135.0320, 6333.2202, 30.4676>> 
				    fSpawnHeading 	= 157.1977
				BREAK
				CASE 3
				    vSpawnPoint 	= <<-134.7707, 6330.5181, 30.5974>> 
				    fSpawnHeading 	= 157.1977
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_7
			SWITCH iPointIndex 
				CASE 0
				    vSpawnPoint 	= <<-346.9642, 6333.3760, 28.9891>> 
				    fSpawnHeading 	= 240.7971
				BREAK
				CASE 1
				    vSpawnPoint 	= <<-349.7834, 6330.6763, 28.9422>> 
				    fSpawnHeading 	= 236.5970
				BREAK
				CASE 2
				    vSpawnPoint 	= <<-352.4390, 6328.4951, 28.8999>> 
				    fSpawnHeading 	= 232.3970
				BREAK
				CASE 3
				    vSpawnPoint 	= <<-355.0284, 6326.3994, 28.8577>> 
				    fSpawnHeading 	= 216.1970
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_8
			SWITCH iPointIndex 
				CASE 0
				    vSpawnPoint 	= <<-318.6661, 6324.6636, 30.0188>> 
				    fSpawnHeading 	= 52.5967
				BREAK
				CASE 1
				    vSpawnPoint 	= <<-323.9018, 6319.0083, 30.0690>> 
				    fSpawnHeading 	= 63.7967
				BREAK
				CASE 2
				    vSpawnPoint 	= <<-321.7896, 6326.2471, 29.6390>> 
				    fSpawnHeading 	= 45.5966
				BREAK
				CASE 3
				    vSpawnPoint 	= <<-325.8750, 6321.8823, 29.5869>> 
				    fSpawnHeading 	= 79.7966
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_9
			SWITCH iPointIndex 
				CASE 0
				    vSpawnPoint 	= <<-424.5072, 6257.6011, 29.5337>> 
				    fSpawnHeading 	= 269.5963
				BREAK
				CASE 1
				    vSpawnPoint 	= <<-420.7737, 6256.8682, 29.5840>> 
				    fSpawnHeading 	= 269.5963
				BREAK
				CASE 2
				    vSpawnPoint 	= <<-423.2110, 6263.8442, 29.4838>> 
				    fSpawnHeading 	= 245.7961
				BREAK
				CASE 3
				    vSpawnPoint 	= <<-419.1255, 6261.5391, 29.4641>> 
				    fSpawnHeading 	= 245.7961
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_10
			SWITCH iPointIndex 
				CASE 0
				    vSpawnPoint 	= <<-252.8151, 6218.2642, 30.4892>> 
				    fSpawnHeading 	= 153.5960
				BREAK
				CASE 1
				    vSpawnPoint 	= <<-256.2903, 6214.3511, 30.4892>> 
				    fSpawnHeading 	= 150.5959
				BREAK
				CASE 2
				    vSpawnPoint 	= <<-264.4771, 6217.4136, 30.5053>> 
				    fSpawnHeading 	= 150.5959
				BREAK
				CASE 3
				    vSpawnPoint 	= <<-266.9532, 6214.9858, 30.5313>> 
				    fSpawnHeading 	= 152.5959
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_11
			SWITCH iPointIndex 
				CASE 0
				    vSpawnPoint 	= <<-265.1024, 6176.5068, 30.3858>> 
				    fSpawnHeading 	= 336.5955
				BREAK
				CASE 1
				    vSpawnPoint 	= <<-270.5427, 6178.7651, 30.4002>> 
				    fSpawnHeading 	= 332.3954
				BREAK
				CASE 2
				    vSpawnPoint 	= <<-268.2294, 6182.5654, 30.3956>>
				    fSpawnHeading 	= 339.5952
				BREAK
				CASE 3
				    vSpawnPoint 	= <<-263.3365, 6178.2749, 30.3859>>
				    fSpawnHeading 	= 315.5951
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_12
			SWITCH iPointIndex 
				CASE 0
				    vSpawnPoint 	= <<-425.9655, 6128.2617, 30.4779>> 
				    fSpawnHeading 	= 240.9948
				BREAK
				CASE 1
				    vSpawnPoint 	= <<-423.1857, 6131.0171, 30.4763>>
				    fSpawnHeading 	= 243.7947
				BREAK
				CASE 2
				    vSpawnPoint 	= <<-419.5018, 6129.0229, 30.2606>> 
				    fSpawnHeading 	= 243.7947
				BREAK
				CASE 3
				    vSpawnPoint 	= <<-422.8078, 6125.8784, 30.4005>> 
				    fSpawnHeading 	= 228.1947
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_13
			SWITCH iPointIndex
				CASE 0
				    vSpawnPoint 	= <<2234.6362, 5165.2373, 57.6865>>
				    fSpawnHeading 	= 101.9944
				BREAK
				CASE 1
				    vSpawnPoint 	= <<2235.6367, 5161.1538, 57.0695>> 
				    fSpawnHeading 	= 132.5943
				BREAK
				CASE 2
				    vSpawnPoint 	= <<2232.0330, 5163.0420, 57.1329>> 
				    fSpawnHeading 	= 119.3942
				BREAK
				CASE 3
				    vSpawnPoint 	= <<2232.3408, 5159.1475, 56.6485>> 
				    fSpawnHeading 	= 119.3942
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_14
			SWITCH iPointIndex 
				CASE 0
				    vSpawnPoint 	= <<2025.6923, 4974.1875, 40.1593>> 
				    fSpawnHeading 	= 239.5937
				BREAK
				CASE 1
				    vSpawnPoint 	= <<2022.1621, 4970.0488, 40.2599>> 
				    fSpawnHeading 	= 239.5937
				BREAK
				CASE 2
				    vSpawnPoint 	= <<2030.2384, 4976.5542, 40.0578>> 
				    fSpawnHeading 	= 267.9936
				BREAK
				CASE 3
				    vSpawnPoint 	= <<2028.3578, 4973.9263, 40.1077>> 
				    fSpawnHeading 	= 254.1935
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	SWITCH eDropoffID
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_15
			SWITCH iPointIndex 
				CASE 0
				    vSpawnPoint 	= <<2314.8618, 4746.6094, 35.6502>> 
				    fSpawnHeading 	= 200.3996
				BREAK
				CASE 1
				    vSpawnPoint 	= <<2318.9041, 4745.3813, 35.4422>>
				    fSpawnHeading 	= 200.3996
				BREAK
				CASE 2
				    vSpawnPoint 	= <<2320.0222, 4749.6230, 35.3766>> 
				    fSpawnHeading 	= 200.3996
				BREAK
				CASE 3
				    vSpawnPoint 	= <<2316.3103, 4749.3418, 35.6638>> 
				    fSpawnHeading 	= 200.3996
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_16
			SWITCH iPointIndex 
				CASE 0
				    vSpawnPoint 	= <<-1355.2542, 4830.6431, 135.3954>>
				    fSpawnHeading 	= 154.1996
				BREAK
				CASE 1
				    vSpawnPoint 	= <<-1358.7560, 4831.7363, 134.8787>>
				    fSpawnHeading 	= 154.1996
				BREAK
				CASE 2
				    vSpawnPoint 	= <<-1352.2186, 4832.2393, 136.0066>> 
				    fSpawnHeading 	= 154.1996
				BREAK
				CASE 3
				    vSpawnPoint 	= <<-1355.4888, 4834.4502, 135.9585>> 
				    fSpawnHeading 	= 154.1996
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_17
			SWITCH iPointIndex 
				CASE 0
				    vSpawnPoint 	= <<1684.5769, 4645.4961, 42.7361>> 
				    fSpawnHeading 	= 268.1993
				BREAK
				CASE 1
				    vSpawnPoint 	= <<1687.5667, 4645.3115, 42.5452>>
				    fSpawnHeading 	= 268.1993
				BREAK
				CASE 2
				    vSpawnPoint 	= <<1686.1591, 4649.3979, 42.4877>>
				    fSpawnHeading 	= 268.1993
				BREAK
				CASE 3
				    vSpawnPoint 	= <<1688.7515, 4648.2363, 42.4730>>
				    fSpawnHeading 	= 268.1993
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_18
			SWITCH iPointIndex 
				CASE 0
				    vSpawnPoint 	= <<1700.1821, 4745.1973, 40.9769>> 
				    fSpawnHeading 	= 98.5990
				BREAK
				CASE 1
				    vSpawnPoint 	= <<1698.2633, 4748.4063, 40.9859>> 
				    fSpawnHeading 	= 98.5990
				BREAK
				CASE 2
				    vSpawnPoint 	= <<1694.4590, 4746.6611, 40.9979>> 
				    fSpawnHeading 	= 98.5990
				BREAK
				CASE 3
				    vSpawnPoint 	= <<1696.3158, 4743.8862, 41.0162>> 
				    fSpawnHeading 	= 98.5990
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_19
			SWITCH iPointIndex 
				CASE 0
				    vSpawnPoint 	= <<2935.3838, 4476.0742, 46.6891>> 
				    fSpawnHeading 	= 218.9987
				BREAK
				CASE 1
				    vSpawnPoint 	= <<2937.8411, 4477.7490, 46.6067>> 
				    fSpawnHeading 	= 218.9987
				BREAK
				CASE 2
				    vSpawnPoint 	= <<2939.1301, 4475.5474, 46.3159>>
				    fSpawnHeading 	= 218.9987
				BREAK
				CASE 3
				    vSpawnPoint 	= <<2936.4009, 4473.4536, 46.3906>>
				    fSpawnHeading 	= 218.9987
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_20
			SWITCH iPointIndex 
				CASE 0
				    vSpawnPoint 	= <<1876.1440, 3752.3984, 31.9640>> 
				    fSpawnHeading 	= 218.1987
				BREAK
				CASE 1
				    vSpawnPoint 	= <<1879.0182, 3752.5776, 31.9339>>
				    fSpawnHeading 	= 218.1987
				BREAK
				CASE 2
				    vSpawnPoint 	= <<1876.8099, 3749.3838, 31.8909>>
				    fSpawnHeading 	= 218.1987
				BREAK
				CASE 3
				    vSpawnPoint 	= <<1879.5748, 3749.9265, 31.7246>>
				    fSpawnHeading 	= 218.1987
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_21
			SWITCH iPointIndex 
				CASE 0
				    vSpawnPoint 	= <<1899.7926, 3705.0969, 31.7522>>
				    fSpawnHeading 	= 218.1987
				BREAK
				CASE 1
				    vSpawnPoint 	= <<1895.0872, 3702.2561, 31.8466>>
				    fSpawnHeading 	= 218.1987
				BREAK
				CASE 2
				    vSpawnPoint 	= <<1898.1145, 3700.9785, 31.7997>>
				    fSpawnHeading 	= 218.1987
				BREAK
				CASE 3
				    vSpawnPoint 	= <<1901.6954, 3702.2905, 31.7391>>
				    fSpawnHeading 	= 218.1987
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_22
			SWITCH iPointIndex 
				CASE 0
				    vSpawnPoint 	= <<916.1896, 3642.5642, 31.6467>> 
				    fSpawnHeading 	= 183.1988
				BREAK
				CASE 1
				    vSpawnPoint 	= <<918.9680, 3642.2605, 31.6279>>
				    fSpawnHeading 	= 183.1988
				BREAK
				CASE 2
				    vSpawnPoint 	= <<919.8309, 3640.4534, 31.3778>>
				    fSpawnHeading 	= 183.1988
				BREAK
				CASE 3
				    vSpawnPoint 	= <<917.0494, 3640.3521, 31.3650>>
				    fSpawnHeading 	= 183.1988
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_23
			SWITCH iPointIndex 
				CASE 0
				    vSpawnPoint 	= <<1365.5244, 3587.1074, 33.9438>>
				    fSpawnHeading 	= 217.3988
				BREAK
				CASE 1
				    vSpawnPoint 	= <<1369.2686, 3590.1165, 33.8985>>
				    fSpawnHeading 	= 217.3988
				BREAK
				CASE 2
				    vSpawnPoint 	= <<1370.7280, 3587.2690, 33.9985>>
				    fSpawnHeading 	= 217.3988
				BREAK
				CASE 3
				    vSpawnPoint 	= <<1368.3104, 3585.3569, 34.0098>>
				    fSpawnHeading 	= 217.3988
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_24
			SWITCH iPointIndex 
				CASE 0
				    vSpawnPoint 	= <<422.7554, 3553.9094, 32.2386>> 
				    fSpawnHeading 	= 91.9986
				BREAK
				CASE 1
				    vSpawnPoint 	= <<423.1900, 3558.2954, 32.2386>>
				    fSpawnHeading 	= 91.9986
				BREAK
				CASE 2
				    vSpawnPoint 	= <<426.0304, 3556.7034, 32.2386>>
				    fSpawnHeading 	= 91.9986
				BREAK
				CASE 3
					vSpawnPoint		= <<425.6619, 3553.3105, 32.2386>>
					fSpawnHeading	= 91.9986
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_25
			SWITCH iPointIndex 
				CASE 0
				    vSpawnPoint 	= <<-2243.9653, 3477.1235, 29.3219>> 
				    fSpawnHeading 	= 115.9984
				BREAK
				CASE 1
				    vSpawnPoint 	= <<-2245.1677, 3474.2588, 29.3730>>
				    fSpawnHeading 	= 115.9984
				BREAK
				CASE 2
				    vSpawnPoint 	= <<-2242.9785, 3472.5774, 29.3580>>
				    fSpawnHeading 	= 115.9984
				BREAK
				CASE 3
				    vSpawnPoint 	= <<-2241.5627, 3475.5352, 29.3601>>
				    fSpawnHeading 	= 115.9984
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_26
			SWITCH iPointIndex 
				CASE 0
				    vSpawnPoint 	= <<1205.3422, 2701.2014, 36.9853>>
				    fSpawnHeading 	= 176.3981
				BREAK
				CASE 1
				    vSpawnPoint 	= <<1208.7668, 2700.7422, 36.9993>>
				    fSpawnHeading 	= 176.3981
				BREAK
				CASE 2
				    vSpawnPoint 	= <<1208.2073, 2697.6377, 36.9403>>
				    fSpawnHeading 	= 176.3981
				BREAK
				CASE 3
				    vSpawnPoint 	= <<1205.3074, 2697.8628, 36.9607>>
				    fSpawnHeading 	= 176.3981
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_27
			SWITCH iPointIndex 
				CASE 0
				    vSpawnPoint 	= <<989.6898, 2673.8418, 38.8820>>
				    fSpawnHeading 	= 5.3977
				BREAK
				CASE 1
				    vSpawnPoint 	= <<986.5496, 2673.7385, 38.8256>>
				    fSpawnHeading 	= 5.3977
				BREAK
				CASE 2
				    vSpawnPoint 	= <<989.5935, 2676.6897, 38.7354>>
				    fSpawnHeading 	= 5.3977
				BREAK
				CASE 3
				    vSpawnPoint 	= <<986.3177, 2676.5840, 38.7799>>
				    fSpawnHeading 	= 5.3977
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_28
			SWITCH iPointIndex 
				CASE 0
				    vSpawnPoint 	= <<2847.7900, 1478.3876, 23.5599>>
				    fSpawnHeading 	= 79.1975
				BREAK
				CASE 1
				    vSpawnPoint 	= <<2850.9910, 1477.3756, 23.5596>>
				    fSpawnHeading 	= 79.1975
				BREAK
				CASE 2
				    vSpawnPoint 	= <<2849.3857, 1474.4191, 23.5554>>
				    fSpawnHeading 	= 79.1975
				BREAK
				CASE 3
				    vSpawnPoint 	= <<2846.3313, 1475.4043, 23.5554>>
				    fSpawnHeading 	= 79.1975
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_29
			SWITCH iPointIndex 
				CASE 0
				    vSpawnPoint 	= <<1772.5625, 3661.9585, 33.3027>>
				    fSpawnHeading 	= 28.2000
				BREAK
				CASE 1
				    vSpawnPoint 	= <<1769.9290, 3661.9863, 33.3072>>
				    fSpawnHeading 	= 28.2000
				BREAK
				CASE 2
				    vSpawnPoint 	= <<1773.2869, 3664.2185, 33.2887>>
				    fSpawnHeading 	= 28.2000
				BREAK
				CASE 3
				    vSpawnPoint 	= <<1770.4105, 3664.7598, 33.2771>>
				    fSpawnHeading 	= 28.2000
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_30
			SWITCH iPointIndex 
				CASE 0
				    vSpawnPoint 	= <<-260.2494, 6259.3652, 30.4328>>
				    fSpawnHeading 	= 28.2000
				BREAK
				CASE 1
				    vSpawnPoint 	= <<-260.1661, 6262.1753, 30.4094>>
				    fSpawnHeading 	= 28.2000
				BREAK
				CASE 2
				    vSpawnPoint 	= <<-257.4483, 6260.7710, 30.4437>>
				    fSpawnHeading 	= 28.2000
				BREAK
				CASE 3
				    vSpawnPoint 	= <<-257.9496, 6263.7437, 30.4144>>
				    fSpawnHeading 	= 28.2000
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH	

	SWITCH eDropoffID		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_1
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<-1995.2019, 454.5807, 101.1281>>
					fSpawnHeading	= 295.9997
				BREAK
				CASE 1
					vSpawnPoint 	= <<-1996.0759, 459.7257, 101.5876>>
					fSpawnHeading	= 295.9997
				BREAK
				CASE 2	
					vSpawnPoint 	= <<-1992.2562, 453.1313, 100.9065>>
					fSpawnHeading	= 295.9997
				BREAK
				CASE 3
					vSpawnPoint 	= <<-1994.0605, 461.8541, 101.6874>>
					fSpawnHeading	= 295.9997
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_2
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<-181.2569, 300.2538, 95.7347>>
					fSpawnHeading	= 183.1993
				BREAK
				CASE 1
					vSpawnPoint 	= <<-185.0248, 300.0963, 95.7095>> 
					fSpawnHeading	= 183.1993
				BREAK
				CASE 2	
					vSpawnPoint 	= <<-185.1412, 296.0942, 95.0675>>
					fSpawnHeading	= 183.1993
				BREAK
				CASE 3
					vSpawnPoint 	= <<-181.4765, 296.0129, 95.0546>>
					fSpawnHeading	= 183.1993
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_3
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<-131.9714, 201.2229, 90.2297>>
					fSpawnHeading	= 278.1991
				BREAK
				CASE 1
					vSpawnPoint 	= <<-131.1041, 205.3411, 91.1239>>
					fSpawnHeading	= 278.1991
				BREAK
				CASE 2	
					vSpawnPoint 	= <<-126.0736, 205.8745, 91.2943>>
					fSpawnHeading	= 278.1991
				BREAK
				CASE 3
					vSpawnPoint 	= <<-127.0113, 201.5501, 90.3986>> 
					fSpawnHeading	= 278.1991
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_4
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<-1924.7725, 189.2633, 83.2921>>
					fSpawnHeading	= 316.1989
				BREAK
				CASE 1
					vSpawnPoint 	= <<-1926.6370, 192.3639, 83.4390>>
					fSpawnHeading	= 316.1989
				BREAK
				CASE 2	
					vSpawnPoint 	= <<-1921.8235, 188.7411, 83.1094>>
					fSpawnHeading	= 316.1989
				BREAK
				CASE 3
					vSpawnPoint 	= <<-1925.9991, 195.6414, 83.4114>>
					fSpawnHeading	= 316.1989
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_5
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<337.9270, 37.1557, 88.5231>>
					fSpawnHeading	= 66.3987
				BREAK
				CASE 1
					vSpawnPoint 	= <<336.1073, 40.1279, 88.9758>>
					fSpawnHeading	= 66.3987
				BREAK
				CASE 2	
					vSpawnPoint 	= <<340.2810, 40.3901, 89.3334>>
					fSpawnHeading	= 66.3987
				BREAK
				CASE 3
					vSpawnPoint 	= <<338.4041, 43.5040, 89.7926>> 
					fSpawnHeading	= 66.3987
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_6
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<177.2463, -152.6417, 55.3300>>
					fSpawnHeading	= 73.5986
				BREAK
				CASE 1
					vSpawnPoint 	= <<179.4564, -148.1428, 56.0887>>
					fSpawnHeading	= 73.5986
				BREAK
				CASE 2	
					vSpawnPoint 	= <<176.2766, -146.9760, 56.0387>>
					fSpawnHeading	= 73.5986
				BREAK
				CASE 3
					vSpawnPoint 	= <<173.7142, -150.4158, 55.4614>> 
					fSpawnHeading	= 73.5986
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_7
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<-1511.9407, -202.7063, 51.8724>>
					fSpawnHeading	= 129.7984
				BREAK
				CASE 1
					vSpawnPoint 	= <<-1515.3580, -200.2663, 52.4878>>
					fSpawnHeading	= 129.7984
				BREAK
				CASE 2	
					vSpawnPoint 	= <<-1515.4022, -204.5276, 52.3675>>
					fSpawnHeading	= 129.7984
				BREAK
				CASE 3
					vSpawnPoint 	= <<-1519.0421, -201.9936, 52.7673>>
					fSpawnHeading	= 129.7984
				BREAK
			ENDSWITCH
		BREAK		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_8
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<935.5638, -254.4030, 66.8777>>
					fSpawnHeading	= 149.7981
				BREAK
				CASE 1
					vSpawnPoint 	= <<938.3046, -256.5606, 66.6640>>
					fSpawnHeading	= 149.7981
				BREAK
				CASE 2	
					vSpawnPoint 	= <<933.1868, -257.1711, 66.8951>>
					fSpawnHeading	= 149.7981
				BREAK
				CASE 3
					vSpawnPoint 	= <<936.9108, -259.2471, 66.6384>>
					fSpawnHeading	= 149.7981
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_9
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<-543.1403, -36.2037, 42.2845>>
					fSpawnHeading	= 71.9978
				BREAK
				CASE 1
					vSpawnPoint 	= <<-543.9888, -40.2859, 41.9586>>
					fSpawnHeading	= 71.9978
				BREAK
				CASE 2	
					vSpawnPoint 	= <<-546.8793, -35.5579, 42.1723>>
					fSpawnHeading	= 71.9978
				BREAK
				CASE 3
					vSpawnPoint 	= <<-547.8883, -39.4730, 41.8568>>
					fSpawnHeading	= 71.9978
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_10
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<-1293.3419, -403.8072, 35.0446>>
					fSpawnHeading	= 308.3975
				BREAK
				CASE 1
					vSpawnPoint 	= <<-1294.1025, -400.1062, 35.1418>>
					fSpawnHeading	= 308.3975
				BREAK
				CASE 2	
					vSpawnPoint 	= <<-1290.1134, -402.1434, 34.9497>>
					fSpawnHeading	= 308.3975
				BREAK
				CASE 3
					vSpawnPoint 	= <<-1290.7780, -398.3760, 35.1410>>
					fSpawnHeading	= 308.3975
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_11
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint		= <<909.5392, -491.2570, 58.0202>> 
					fSpawnHeading	= 205.3971
				BREAK
				CASE 1
					vSpawnPoint		= <<908.9469, -494.6134, 57.5490>>
					fSpawnHeading	= 205.3971
				BREAK
				CASE 2
					vSpawnPoint		= <<911.3970, -496.5866, 57.6449>>
					fSpawnHeading	= 205.3971
				BREAK
				CASE 3
					vSpawnPoint		= <<914.6880, -494.9266, 57.8814>>
					fSpawnHeading	= 205.3971
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_12
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint		= <<1250.4475, -579.7961, 68.1448>> 
					fSpawnHeading	= 287.7967
				BREAK
				CASE 1
					vSpawnPoint		= <<1250.1046, -576.4091, 68.1629>>
					fSpawnHeading	= 287.7967
				BREAK
				CASE 2
					vSpawnPoint		= <<1253.3912, -581.5173, 68.0956>>
					fSpawnHeading	= 287.7967
				BREAK
				CASE 3
					vSpawnPoint		= <<1252.6133, -574.8839, 68.0420>>
					fSpawnHeading	= 287.7967
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_13
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint		= <<-1267.5220, -611.5441, 26.0968>> 
					fSpawnHeading	= 267.7996
				BREAK
				CASE 1
					vSpawnPoint		= <<-1264.9601, -609.5386, 26.1319>> 
					fSpawnHeading	= 267.7996
				BREAK
				CASE 2
					vSpawnPoint		= <<-1264.8472, -612.9615, 26.1041>>
					fSpawnHeading	= 267.7996
				BREAK
				CASE 3
					vSpawnPoint		= <<-1262.2869, -610.7207, 26.1430>>
					fSpawnHeading	= 267.7996
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_14
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint		= <<324.1917, -692.1303, 28.3166>>
					fSpawnHeading	= 244.7994
				BREAK
				CASE 1
					vSpawnPoint		= <<326.5067, -689.1725, 28.3205>>
					fSpawnHeading	= 244.7994
				BREAK
				CASE 2
					vSpawnPoint		= <<328.4621, -691.6176, 28.3159>>
					fSpawnHeading	= 244.7994
				BREAK
				CASE 3
					vSpawnPoint		= <<326.8571, -694.4362, 28.3135>>
					fSpawnHeading	= 244.7994
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_15
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint		= <<925.1204, -628.9852, 56.8682>>
					fSpawnHeading	= 329.9988
				BREAK
				CASE 1
					vSpawnPoint		= <<928.2182, -631.1860, 56.8647>>
					fSpawnHeading	= 329.9988
				BREAK
				CASE 2
					vSpawnPoint		= <<927.7733, -626.0200, 56.7620>>
					fSpawnHeading	= 329.9988
				BREAK
				CASE 3
					vSpawnPoint		= <<931.4572, -625.9988, 56.6196>>
					fSpawnHeading	= 329.9988
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_16
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint		= <<-1368.1387, -787.0344, 18.3512>>
					fSpawnHeading	= 149.7982
				BREAK
				CASE 1
					vSpawnPoint		= <<-1366.6638, -790.6207, 18.3394>>
					fSpawnHeading	= 149.7982
				BREAK
				CASE 2
					vSpawnPoint		= <<-1370.6704, -789.7843, 18.3280>>
					fSpawnHeading	= 149.7982
				BREAK
				CASE 3
					vSpawnPoint		= <<-1368.2505, -792.6863, 18.3250>>
					fSpawnHeading	= 149.7982
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_17
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint		= <<-617.7515, -774.4829, 24.3407>> 
					fSpawnHeading	= 6.9977
				BREAK
				CASE 1
					vSpawnPoint		= <<-614.6607, -774.0724, 24.3209>>
					fSpawnHeading	= 6.9977
				BREAK
				CASE 2
					vSpawnPoint		= <<-618.2723, -771.1660, 24.4534>>
					fSpawnHeading	= 6.9977
				BREAK
				CASE 3
					vSpawnPoint		= <<-615.1741, -770.1447, 24.4229>>
					fSpawnHeading	= 6.9977
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_18
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint		= <<-1376.0793, -894.8463, 12.3229>> 
					fSpawnHeading	= 48.9976
				BREAK
				CASE 1
					vSpawnPoint		= <<-1377.3660, -898.5443, 12.0424>>
					fSpawnHeading	= 48.9976
				BREAK
				CASE 2
					vSpawnPoint		= <<-1378.6206, -894.2716, 12.2401>>
					fSpawnHeading	= 48.9976
				BREAK
				CASE 3
					vSpawnPoint		= <<-1379.5294, -897.3811, 11.9825>>
					fSpawnHeading	= 48.9976
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_19
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint		= <<-1447.0873, -878.5676, 9.7464>> 
					fSpawnHeading	= 159.9973
				BREAK
				CASE 1
					vSpawnPoint		= <<-1444.0660, -880.5381, 9.7828>>
					fSpawnHeading	= 159.9973
				BREAK
				CASE 2
					vSpawnPoint		= <<-1448.5057, -881.1882, 9.7595>>
					fSpawnHeading	= 159.9973
				BREAK
				CASE 3
					vSpawnPoint		= <<-1445.4635, -883.2283, 9.8216>>
					fSpawnHeading	= 159.9973
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_20
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint		= <<447.8857, -943.3335, 27.6278>> 
					fSpawnHeading	= 170.9972
				BREAK
				CASE 1
					vSpawnPoint		= <<450.8445, -944.4548, 27.5244>>
					fSpawnHeading	= 170.9972
				BREAK
				CASE 2
					vSpawnPoint		= <<450.3604, -946.5624, 27.5522>>
					fSpawnHeading	= 170.9972
				BREAK
				CASE 3
					vSpawnPoint		= <<446.3271, -946.0310, 27.7024>>
					fSpawnHeading	= 170.9972
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_21
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint		= <<1142.0441, -975.2816, 45.5088>>
					fSpawnHeading	= 269.5970
				BREAK
				CASE 1
					vSpawnPoint		= <<1142.0645, -971.8673, 45.7278>>
					fSpawnHeading	= 269.5970
				BREAK
				CASE 2
					vSpawnPoint		= <<1145.6801, -972.4183, 45.6699>>
					fSpawnHeading	= 269.5970
				BREAK
				CASE 3
					vSpawnPoint		= <<1145.5840, -975.7992, 45.4613>>
					fSpawnHeading	= 269.5970
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_22
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint		= <<-1335.3749, -1023.2935, 6.7139>> 
					fSpawnHeading	= 54.3967
				BREAK
				CASE 1
					vSpawnPoint		= <<-1335.2377, -1019.6651, 6.7484>>
					fSpawnHeading	= 54.3967
				BREAK
				CASE 2
					vSpawnPoint		= <<-1337.5175, -1020.8102, 6.7944>>
					fSpawnHeading	= 54.3967
				BREAK
				CASE 3
					vSpawnPoint		= <<-1337.4792, -1018.4900, 6.8198>>
					fSpawnHeading	= 54.3967
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_23
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint		= <<-593.9991, -1083.1636, 21.3297>> 
					fSpawnHeading	= 266.1964
				BREAK
				CASE 1
					vSpawnPoint		= <<-590.8708, -1086.1857, 21.3297>>
					fSpawnHeading	= 266.1964
				BREAK
				CASE 2
					vSpawnPoint		= <<-589.8027, -1082.1525, 21.3297>>
					fSpawnHeading	= 266.1964
				BREAK
				CASE 3
					vSpawnPoint		= <<-586.3967, -1086.4921, 21.3297>>
					fSpawnHeading	= 266.1964
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_24
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint		= <<-1248.7383, -1221.0515, 5.8872>>
					fSpawnHeading	= 131.9997
				BREAK
				CASE 1
					vSpawnPoint		= <<-1252.4053, -1221.7817, 5.7938>>
					fSpawnHeading	= 131.9997
				BREAK
				CASE 2
					vSpawnPoint		= <<-1250.2954, -1224.0452, 5.7095>>
					fSpawnHeading	= 131.9997
				BREAK
				CASE 3
					vSpawnPoint		= <<-1254.0385, -1224.2892, 5.2533>>
					fSpawnHeading	= 131.9997
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_25
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint		= <<-1109.8026, -1516.1025, 3.4447>> 
					fSpawnHeading	= 123.9995
				BREAK
				CASE 1
					vSpawnPoint		= <<-1111.5428, -1513.0828, 3.4509>>
					fSpawnHeading	= 123.9995
				BREAK
				CASE 2
					vSpawnPoint		= <<-1114.6108, -1515.0959, 3.4018>>
					fSpawnHeading	= 123.9995
				BREAK
				CASE 3
					vSpawnPoint		= <<-1112.7791, -1518.4091, 3.3962>>
					fSpawnHeading	= 123.9995
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_26
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint		= <<1309.1708, -1648.9971, 50.7958>> 
					fSpawnHeading	= 33.7993
				BREAK
				CASE 1
					vSpawnPoint		= <<1311.2843, -1646.2000, 50.9453>>
					fSpawnHeading	= 33.7993
				BREAK
				CASE 2
					vSpawnPoint		= <<1307.3323, -1647.0403, 50.7247>>
					fSpawnHeading	= 33.7993
				BREAK
				CASE 3
					vSpawnPoint		= <<1309.3942, -1644.3422, 50.9163>>
					fSpawnHeading	= 33.7993
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_27
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint		= <<254.4520, -1715.0374, 28.2051>> 
					fSpawnHeading	= 75.7992
				BREAK
				CASE 1
					vSpawnPoint		= <<256.6550, -1712.4402, 28.2124>>
					fSpawnHeading	= 75.7992
				BREAK
				CASE 2
					vSpawnPoint		= <<254.1898, -1711.3239, 28.0655>>
					fSpawnHeading	= 75.7992
				BREAK
				CASE 3
					vSpawnPoint		= <<251.4979, -1714.2472, 28.1155>>
					fSpawnHeading	= 75.7992
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_28
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint		= <<949.2336, -2009.6714, 29.1833>> 
					fSpawnHeading	= 83.9991
				BREAK
				CASE 1
					vSpawnPoint		= <<948.9877, -2003.7523, 29.1404>>
					fSpawnHeading	= 83.9991
				BREAK
				CASE 2
					vSpawnPoint		= <<949.9338, -1997.6852, 29.1538>>
					fSpawnHeading	= 83.9991
				BREAK
				CASE 3
					vSpawnPoint		= <<951.1229, -1991.9243, 29.1799>>
					fSpawnHeading	= 83.9991
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_29
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint		= <<1009.0292, -2368.5796, 29.5195>>
					fSpawnHeading	= 261.3984
				BREAK
				CASE 1
					vSpawnPoint		= <<1009.2556, -2365.2183, 29.5096>>
					fSpawnHeading	= 261.3984
				BREAK
				CASE 2
					vSpawnPoint		= <<1012.6377, -2365.4888, 29.5096>>
					fSpawnHeading	= 261.3984
				BREAK
				CASE 3
					vSpawnPoint		= <<1012.5215, -2368.4741, 29.5177>>
					fSpawnHeading	= 261.3984
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_30
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint		= <<-703.8530, -2461.9846, 12.8285>> 
					fSpawnHeading	= 70.5981
				BREAK
				CASE 1
					vSpawnPoint		= <<-706.1159, -2463.7012, 12.8285>>
					fSpawnHeading	= 70.5981
				BREAK
				CASE 2
					vSpawnPoint		= <<-706.3795, -2460.6003, 12.8271>>
					fSpawnHeading	= 70.5981
				BREAK
				CASE 3
					vSpawnPoint		= <<-709.3088, -2461.9670, 12.8285>>
					fSpawnHeading	= 70.5981
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
		
	SWITCH eDropoffID		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_1
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<-239.8092, 2059.9397, 138.3452>>
					fSpawnHeading	= 24.3911
				BREAK
				CASE 1
					vSpawnPoint 	= <<-242.0186, 2059.1018, 138.3981>>
					fSpawnHeading	= 22.3911
				BREAK
				CASE 2	
					vSpawnPoint 	= <<-244.5391, 2058.7390, 138.3553>>
					fSpawnHeading	= 22.3911
				BREAK
				CASE 3
					vSpawnPoint 	= <<-246.7880, 2058.1184, 138.3157>>
					fSpawnHeading	= 17.3910
				BREAK
				CASE 4
					vSpawnPoint 	= <<-248.0152, 2069.2358, 138.5611>>
					fSpawnHeading	= 213.3909
				BREAK
				CASE 5
					vSpawnPoint 	= <<-250.2329, 2068.5591, 138.5097>>
					fSpawnHeading	= 209.1909
				BREAK
				CASE 6
					vSpawnPoint 	= <<-252.9919, 2067.7771, 138.5097>>
					fSpawnHeading	= 209.1909
				BREAK
				CASE 7
					vSpawnPoint 	= <<-255.4840, 2067.4187, 138.5100>>
					fSpawnHeading	= 209.1909
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_2
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<2765.7502, 2831.3706, 35.2633>>
					fSpawnHeading	= 34.3907
				BREAK
				CASE 1
					vSpawnPoint 	= <<2763.8525, 2830.5208, 35.2886>>
					fSpawnHeading	= 34.3907
				BREAK
				CASE 2	
					vSpawnPoint 	= <<2762.1494, 2829.3301, 35.2987>>
					fSpawnHeading	=  33.1907
				BREAK
				CASE 3
					vSpawnPoint 	= <<2760.4075, 2828.3096, 35.3181>>
					fSpawnHeading	=  33.1907
				BREAK
				CASE 4
					vSpawnPoint 	= <<2765.0308, 2834.0103, 35.2890>>
					fSpawnHeading	= 41.7906
				BREAK
				CASE 5
					vSpawnPoint 	= <<2762.9463, 2833.1233, 35.3258>>
					fSpawnHeading	= 25.5906
				BREAK
				CASE 6
					vSpawnPoint 	= <<2761.2000, 2831.8035, 35.3523>>
					fSpawnHeading	= 25.5906
				BREAK
				CASE 7
					vSpawnPoint 	= <<2759.3525, 2830.7092, 35.3803>>
					fSpawnHeading	= 25.5906
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_3
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<2994.6135, 3480.1956, 70.3818>>
					fSpawnHeading	= 268.7986
				BREAK
				CASE 1
					vSpawnPoint 	= <<2994.0471, 3484.8970, 70.3817>>
					fSpawnHeading	= 261.9996
				BREAK
				CASE 2	
					vSpawnPoint 	= <<2992.8569, 3482.5920, 70.3817>>
					fSpawnHeading	= 268.1986
				BREAK
				CASE 3
					vSpawnPoint 	= <<2993.2749, 3477.7866, 70.3818>>
					fSpawnHeading	= 268.1986
				BREAK
				CASE 4
					vSpawnPoint 	= <<2987.5771, 3488.0452, 70.3819>>
					fSpawnHeading	= 344.7994
				BREAK
				CASE 5
					vSpawnPoint 	= <<2985.9561, 3490.2009, 70.3819>>
					fSpawnHeading	= 344.7994
				BREAK
				CASE 6
					vSpawnPoint 	= <<2983.5830, 3488.4573, 70.3819>>
					fSpawnHeading	= 344.7994
				BREAK
				CASE 7
					vSpawnPoint 	= <<2981.7300, 3491.1001, 70.3819>>
					fSpawnHeading	= 344.7994
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_4
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<-2655.6240, 3463.4727, 13.5531>>
					fSpawnHeading	= 365.3982
				BREAK
				CASE 1
					vSpawnPoint 	= <<-2658.7771, 3464.2903, 13.4602>>
					fSpawnHeading	= 365.3982
				BREAK
				CASE 2	
					vSpawnPoint 	= <<-2652.9893, 3466.0242, 13.5323>>
					fSpawnHeading	= 358.1980
				BREAK
				CASE 3
					vSpawnPoint 	= <<-2657.1042, 3467.1689, 13.3498>>
					fSpawnHeading	= 353.1978
				BREAK
				CASE 4
					vSpawnPoint 	= <<-2673.9207, 3463.2053, 13.2321>>
					fSpawnHeading	= 353.1978
				BREAK
				CASE 5
					vSpawnPoint 	= <<-2678.0991, 3463.3445, 13.1388>>
					fSpawnHeading	= 360.3977
				BREAK
				CASE 6
					vSpawnPoint 	= <<-2676.0950, 3466.0803, 13.0674>>
					fSpawnHeading	= 360.3977
				BREAK
				CASE 7
					vSpawnPoint 	= <<-2680.7227, 3465.9805, 12.8825>>
					fSpawnHeading	= 360.3977
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_5
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<1490.3108, 3567.1851, 34.1604>>
					fSpawnHeading	= 112.1975
				BREAK
				CASE 1
					vSpawnPoint 	= <<1490.6128, 3564.6824, 34.2336>>
					fSpawnHeading	= 112.1975
				BREAK
				CASE 2	
					vSpawnPoint 	= <<1492.5616, 3563.0459, 34.2289>>
					fSpawnHeading	= 112.1975
				BREAK
				CASE 3
					vSpawnPoint 	= <<1493.1681, 3560.3757, 34.2993>>
					fSpawnHeading	= 112.1975
				BREAK
				CASE 4
					vSpawnPoint 	= <<1485.1941, 3591.4915, 34.3504>>
					fSpawnHeading	= 104.9974
				BREAK
				CASE 5
					vSpawnPoint 	= <<1483.3914, 3593.5486, 34.3593>>
					fSpawnHeading	= 104.9974
				BREAK
				CASE 6
					vSpawnPoint 	= <<1482.6871, 3595.9612, 34.2956>>
					fSpawnHeading	= 104.9974
				BREAK
				CASE 7
					vSpawnPoint 	= <<1480.6106, 3597.6843, 34.2640>>
					fSpawnHeading	= 1123.1973
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_6
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<-2369.5991, 4292.9849, 8.6590>>
					fSpawnHeading	= 47.5971
				BREAK
				CASE 1
					vSpawnPoint 	= <<-2368.3420, 4295.4033, 8.4331>>
					fSpawnHeading	= 47.5971
				BREAK
				CASE 2	
					vSpawnPoint 	= <<-2366.9187, 4297.2656, 8.4466>>
					fSpawnHeading	= 47.5971
				BREAK
				CASE 3
					vSpawnPoint 	= <<-2365.9460, 4299.8540, 8.2329>>
					fSpawnHeading	= 47.5971
				BREAK
				CASE 4
					vSpawnPoint 	= <<-2368.5034, 4301.8540, 7.7919>>
					fSpawnHeading	= 47.5971
				BREAK
				CASE 5
					vSpawnPoint 	= <<-2370.2493, 4299.3389, 7.9781>>
					fSpawnHeading	= 47.5971
				BREAK
				CASE 6
					vSpawnPoint 	= <<-2371.4290, 4297.0684, 8.1561>>
					fSpawnHeading	= 60.5971
				BREAK
				CASE 7
					vSpawnPoint 	= <<-2372.7461, 4294.6831, 8.3161>>
					fSpawnHeading	= 60.5971
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_7
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<2494.2007, 4299.6646, 38.1688>>
					fSpawnHeading	= 352.5968
				BREAK
				CASE 1
					vSpawnPoint 	= <<2491.9946, 4300.4028, 38.0832>>
					fSpawnHeading	= 346.9966
				BREAK
				CASE 2	
					vSpawnPoint 	= <<2489.8699, 4300.9038, 38.0059>>
					fSpawnHeading	= 340.9963
				BREAK
				CASE 3
					vSpawnPoint 	= <<2487.4395, 4301.5269, 37.9177>>
					fSpawnHeading	= 343.9958
				BREAK
				CASE 4 
					vSpawnPoint 	= <<2506.8625, 4286.6187, 38.1724>>
					fSpawnHeading	= 343.9958
				BREAK
				CASE 5
					vSpawnPoint 	= <<2509.0750, 4286.2900, 38.1921>>
					fSpawnHeading	= 343.9958
				BREAK
				CASE 6
					vSpawnPoint 	= <<2511.0920, 4286.2729, 38.2012>>
					fSpawnHeading	= 343.9958
				BREAK
				CASE 7
					vSpawnPoint 	= <<2513.5771, 4285.5068, 38.2107>>
					fSpawnHeading	= 343.9958
				BREAK
			ENDSWITCH
		BREAK		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_8
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<-1841.1995, 4414.7998, 48.4769>>
					fSpawnHeading	= 155.5953
				BREAK
				CASE 1
					vSpawnPoint 	= <<-1839.0634, 4412.8091, 48.7605>>
					fSpawnHeading	= 143.7951
				BREAK
				CASE 2	
					vSpawnPoint 	= <<-1841.4969, 4411.3394, 49.0692>>
					fSpawnHeading	= 143.7951
				BREAK
				CASE 3
					vSpawnPoint 	= <<-1838.9121, 4408.9941, 49.1208>>
					fSpawnHeading	= 134.5950
				BREAK
				CASE 4
					vSpawnPoint 	= <<-1849.3041, 4402.7681, 49.1779>>
					fSpawnHeading	= 334.5943
				BREAK
				CASE 5 
					vSpawnPoint 	= <<-1851.4114, 4404.1157, 49.0271>>
					fSpawnHeading	= 334.5943
				BREAK
				CASE 6
					vSpawnPoint 	= <<-1853.5602, 4405.4375, 48.9189>>
					fSpawnHeading	= 334.5943
				BREAK
				CASE 7
					vSpawnPoint 	= <<-1855.9065, 4406.7310, 48.7319>>
					fSpawnHeading	= 336.5942
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_9
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<-1568.3062, 4509.5566, 19.4424>>
					fSpawnHeading	= 272.9926
				BREAK
				CASE 1
					vSpawnPoint 	= <<-1568.3857, 4507.0601, 19.8718>>
					fSpawnHeading	= 272.9926
				BREAK
				CASE 2	
					vSpawnPoint 	= <<-1567.4714, 4504.5811, 20.0082>>
					fSpawnHeading	= 272.9926
				BREAK
				CASE 3
					vSpawnPoint 	= <<-1566.8716, 4501.9487, 20.1819>>
					fSpawnHeading	= 272.9926
				BREAK
				CASE 4
					vSpawnPoint 	= <<-1571.8341, 4508.3633, 19.6603>>
					fSpawnHeading	= 271.3924
				BREAK
				CASE 5
					vSpawnPoint 	= <<-1568.5364, 4513.2402, 18.9730>>
					fSpawnHeading	= 271.3924
				BREAK
				CASE 6
					vSpawnPoint 	= <<-1572.3900, 4494.7378, 20.8722>>
					fSpawnHeading	= 277.5921
				BREAK
				CASE 7
					vSpawnPoint 	= <<-1572.3982, 4489.5542, 21.3130>>
					fSpawnHeading	= 297.7920
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_10
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<-397.5512, 6383.4624, 13.1920>>
					fSpawnHeading	= 20.3916
				BREAK
				CASE 1
					vSpawnPoint 	= <<-399.9127, 6382.1235, 13.1150>>
					fSpawnHeading	= 20.3916
				BREAK
				CASE 2	
					vSpawnPoint 	= <<-402.4652, 6381.0249, 12.9592>>
					fSpawnHeading	= 20.3916
				BREAK
				CASE 3
					vSpawnPoint 	= <<-405.1915, 6379.7080, 12.9470>>
					fSpawnHeading	= 20.3916
				BREAK
				CASE 4
					vSpawnPoint 	= <<-376.3020, 6388.5366, 13.1121>>
					fSpawnHeading	= 20.3916
				BREAK
				CASE 5
					vSpawnPoint 	= <<-375.8965, 6391.9561, 13.0446>>
					fSpawnHeading	= 20.3916
				BREAK
				CASE 6
					vSpawnPoint 	= <<-379.3679, 6390.1455, 13.0727>>
					fSpawnHeading	= 20.3916
				BREAK
				CASE 7
					vSpawnPoint 	= <<-379.9874, 6387.0864, 13.1564>>
					fSpawnHeading	= 20.3916
				BREAK
			ENDSWITCH
		BREAK

		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_1
			SWITCH iPointIndex 
				CASE 0
				    vSpawnPoint 	= <<1197.3662, -3243.5964, 4.8361>>
				    fSpawnHeading 	= 3.7994
				BREAK
				CASE 1
				    vSpawnPoint 	= <<1201.1088, -3243.1396, 4.8596>>
				    fSpawnHeading 	= 3.7994
				BREAK
				CASE 2
				    vSpawnPoint 	= <<1201.0468, -3238.5645, 5.0288>>
				    fSpawnHeading 	= 3.7994
				BREAK
				CASE 3
				    vSpawnPoint 	= <<1197.5703, -3238.7986, 5.0288>>
				    fSpawnHeading 	= 358.3994
				BREAK
				CASE 4
				    vSpawnPoint 	= <<1186.3480, -3250.2014, 5.0288>>
				    fSpawnHeading 	= 69.7996
				BREAK
				CASE 5
				    vSpawnPoint 	= <<1186.3184, -3252.8633, 5.0288>>
				    fSpawnHeading 	= 72.7996
				BREAK
				CASE 6
				    vSpawnPoint 	= <<1185.8265, -3255.9255, 5.0288>>
				    fSpawnHeading 	= 74.7995
				BREAK
				CASE 7
				    vSpawnPoint 	= <<1204.3333, -3242.7854, 4.8778>>
				    fSpawnHeading 	= 358.3994
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_2
			SWITCH iPointIndex 
				CASE 0
				    vSpawnPoint 	= <<233.1918, -3120.1926, 4.7902>>
				    fSpawnHeading 	= 357.1993
				BREAK
				CASE 1
				    vSpawnPoint 	= <<230.3353, -3120.0562, 4.7902>>
				    fSpawnHeading 	= 357.1993
				BREAK
				CASE 2
				    vSpawnPoint 	= <<227.4524, -3120.1345, 4.7902>>
				    fSpawnHeading 	= 357.1993
				BREAK
				CASE 3
				    vSpawnPoint 	= <<224.2509, -3120.0701, 4.7902>>
				    fSpawnHeading 	= 357.1993
				BREAK
				CASE 4
				    vSpawnPoint 	= <<233.1299, -3116.7510, 4.7903>>
				    fSpawnHeading 	= 357.1993
				BREAK
				CASE 5
				    vSpawnPoint 	= <<230.1146, -3116.7417, 4.7903>>
				    fSpawnHeading 	= 357.1993
				BREAK
				CASE 6
				    vSpawnPoint 	= <<227.3014, -3117.0527, 4.7903>>
				    fSpawnHeading 	= 357.1993
				BREAK
				CASE 7
				    vSpawnPoint 	= <<223.9951, -3117.2344, 4.7903>>
				    fSpawnHeading 	= 357.1993
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_3
			SWITCH iPointIndex 
				CASE 0
				    vSpawnPoint 	= <<308.8590, -2873.0002, 5.0083>>
				    fSpawnHeading 	= 83.7991
				BREAK
				CASE 1
				    vSpawnPoint 	= <<308.8752, -2876.1394, 5.0056>>
				    fSpawnHeading 	= 84.9991
				BREAK
				CASE 2
				    vSpawnPoint 	= <<294.4750, -2882.2217, 5.0020>>
				    fSpawnHeading 	= 271.7987
				BREAK
				CASE 3
				    vSpawnPoint 	= <<294.6525, -2880.0400, 5.0043>>
				    fSpawnHeading 	= 263.3985
				BREAK
				CASE 4
				    vSpawnPoint 	= <<309.7551, -2889.1951, 4.9982>>
				    fSpawnHeading 	= 91.5980
				BREAK
				CASE 5
				    vSpawnPoint 	= <<309.5560, -2891.8486, 4.9981>>
				    fSpawnHeading 	= 91.5980
				BREAK
				CASE 6
				    vSpawnPoint 	= <<309.5738, -2894.1792, 4.9990>>
				    fSpawnHeading 	= 87.5980
				BREAK
				CASE 7
				    vSpawnPoint 	= <<295.7810, -2875.5862, 5.0084>>
				    fSpawnHeading 	= 269.7974
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_4
			SWITCH iPointIndex 
				CASE 0
				    vSpawnPoint 	= <<1636.8746, -2354.2878, 93.7334>>
				    fSpawnHeading 	= 296.7984
				BREAK
				CASE 1
				    vSpawnPoint 	= <<1636.1016, -2352.3372, 93.6793>>
				    fSpawnHeading 	= 288.9982
				BREAK
				CASE 2
				    vSpawnPoint 	= <<1635.5677, -2350.3184, 93.6766>>
				    fSpawnHeading 	= 290.1982
				BREAK
				CASE 3
				    vSpawnPoint 	= <<1637.6235, -2356.4426, 93.7008>>
				    fSpawnHeading 	= 285.1980
				BREAK
				CASE 4
				    vSpawnPoint 	= <<1641.5140, -2362.4448, 93.5406>>
				    fSpawnHeading 	= 24.3979
				BREAK
				CASE 5
				    vSpawnPoint 	= <<1643.3954, -2361.4060, 93.6880>>
				    fSpawnHeading 	= 24.3979
				BREAK
				CASE 6
				    vSpawnPoint 	= <<1645.4235, -2360.4038, 93.8702>>
				    fSpawnHeading 	= 24.3979
				BREAK
				CASE 7
				    vSpawnPoint 	= <<1647.3636, -2359.2268, 94.0564>>
				    fSpawnHeading 	= 24.3979
				BREAK
			ENDSWITCH
		BREAK	
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_5
			SWITCH iPointIndex 
				CASE 0
				    vSpawnPoint 	= <<862.7340, -2433.2141, 27.1510>>
				    fSpawnHeading 	= 171.1971
				BREAK
				CASE 1
				    vSpawnPoint 	= <<865.0858, -2433.5630, 27.1738>>
				    fSpawnHeading 	= 171.1971
				BREAK
				CASE 2
				    vSpawnPoint 	= <<867.4045, -2433.8057, 27.1962>>
				    fSpawnHeading 	= 171.1971
				BREAK
				CASE 3
				    vSpawnPoint 	= <<869.9240, -2434.1653, 27.2064>>
				    fSpawnHeading 	= 164.1970
				BREAK
				CASE 4
				    vSpawnPoint 	= <<841.7037, -2431.1838, 26.7920>>
				    fSpawnHeading 	= 170.9968
				BREAK
				CASE 5
				    vSpawnPoint 	= <<844.3810, -2431.4016, 26.9363>>
				    fSpawnHeading 	= 168.9968
				BREAK
				CASE 6
				    vSpawnPoint 	= <<832.0388, -2441.9163, 25.1012>>
				    fSpawnHeading 	= 173.1967
				BREAK
				CASE 7
				    vSpawnPoint 	= <<834.7183, -2442.5454, 25.3971>>
				    fSpawnHeading 	= 173.1967
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_6
			SWITCH iPointIndex 
				CASE 0
				    vSpawnPoint 	= <<-1217.9016, -2381.5642, 12.9451>>
				    fSpawnHeading 	= 236.5964
				BREAK
				CASE 1
				    vSpawnPoint 	= <<-1218.8424, -2383.4778, 12.9452>>
				    fSpawnHeading 	= 236.7963
				BREAK
				CASE 2
				    vSpawnPoint 	= <<-1216.5819, -2379.5093, 12.9451>>
				    fSpawnHeading 	= 238.7964
				BREAK
				CASE 3
				    vSpawnPoint 	= <<-1215.3207, -2377.5054, 12.9451>>
				    fSpawnHeading 	= 236.7963
				BREAK
				CASE 4
				    vSpawnPoint 	= <<-1224.3518, -2390.1724, 12.9452>>
				    fSpawnHeading 	= 239.7963
				BREAK
				CASE 5
				    vSpawnPoint 	= <<-1222.8337, -2388.1614, 12.9451>>
				    fSpawnHeading 	= 236.5963
				BREAK
				CASE 6
				    vSpawnPoint 	= <<-1235.8232, -2407.5283, 12.9452>>
				    fSpawnHeading 	= 244.1963
				BREAK
				CASE 7
				    vSpawnPoint 	= <<-1234.4468, -2405.3401, 12.9452>>
				    fSpawnHeading 	= 244.1963
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_7
			SWITCH iPointIndex 
				CASE 0
				    vSpawnPoint 	= <<915.7257, -1741.7209, 29.6908>>
				    fSpawnHeading 	= 158.3962
				BREAK
				CASE 1
				    vSpawnPoint 	= <<919.4307, -1743.1262, 29.7956>>
				    fSpawnHeading 	= 162.5961
				BREAK
				CASE 2
				    vSpawnPoint 	= <<920.5720, -1746.4564, 29.8444>>
				    fSpawnHeading 	= 158.5960
				BREAK
				CASE 3
				    vSpawnPoint 	= <<916.5859, -1745.0791, 29.7417>>
				    fSpawnHeading 	= 158.5960
				BREAK
				CASE 4
				    vSpawnPoint 	= <<894.6024, -1735.2214, 29.3399>>
				    fSpawnHeading 	= 181.5958
				BREAK
				CASE 5
				    vSpawnPoint 	= <<897.0215, -1735.4695, 29.3698>>
				    fSpawnHeading 	= 175.3958
				BREAK
				CASE 6
				    vSpawnPoint 	= <<915.4925, -1738.0023, 29.6544>>
				    fSpawnHeading 	= 152.3955
				BREAK
				CASE 7
				    vSpawnPoint 	= <<918.0347, -1739.4360, 29.6996>>
				    fSpawnHeading 	= 163.3954
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_8
			SWITCH iPointIndex 
				CASE 0
				    vSpawnPoint 	= <<1710.7242, -1560.2673, 111.6274>>
				    fSpawnHeading 	= 161.9952
				BREAK
				CASE 1
				    vSpawnPoint 	= <<1712.8064, -1560.2811, 111.6311>>
				    fSpawnHeading 	= 159.1948
				BREAK
				CASE 2
				    vSpawnPoint 	= <<1714.4633, -1561.6298, 111.6321>>
				    fSpawnHeading 	= 159.1948
				BREAK
				CASE 3
				    vSpawnPoint 	= <<1716.4321, -1561.4622, 111.6359>>
				    fSpawnHeading 	= 159.1948
				BREAK
				CASE 4
				    vSpawnPoint 	= <<1725.3038, -1556.6415, 111.6554>>
				    fSpawnHeading 	= 249.7944
				BREAK
				CASE 5
				    vSpawnPoint 	= <<1726.9696, -1555.2720, 111.6582>>
				    fSpawnHeading 	= 249.7944
				BREAK
				CASE 6
				    vSpawnPoint 	= <<1726.7292, -1553.2471, 111.6608>>
				    fSpawnHeading 	= 245.7943
				BREAK
				CASE 7
				    vSpawnPoint 	= <<1725.9482, -1558.7065, 111.6529>>
				    fSpawnHeading 	= 245.7943
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_9
			SWITCH iPointIndex 
				CASE 0
				    vSpawnPoint 	= <<500.2138, -1406.6166, 28.2998>>
				    fSpawnHeading 	= 91.5941
				BREAK
				CASE 1
				    vSpawnPoint 	= <<500.0005, -1409.0240, 28.2968>>
				    fSpawnHeading 	= 85.9940
				BREAK
				CASE 2
				    vSpawnPoint 	= <<482.4195, -1404.0125, 28.2756>>
				    fSpawnHeading 	= 258.1935
				BREAK
				CASE 3
				    vSpawnPoint 	= <<482.9327, -1401.5416, 28.2890>>
				    fSpawnHeading 	= 258.1935
				BREAK
				CASE 4
				    vSpawnPoint 	= <<489.1647, -1385.7111, 28.3110>>
				    fSpawnHeading 	= 176.9934
				BREAK
				CASE 5
				    vSpawnPoint 	= <<491.3342, -1388.2578, 28.3720>>
				    fSpawnHeading 	= 173.9933
				BREAK
				CASE 6
				    vSpawnPoint 	= <<489.0717, -1390.1459, 28.3097>>
				    fSpawnHeading 	= 173.9933
				BREAK
				CASE 7
				    vSpawnPoint 	= <<491.2379, -1392.7699, 28.3589>>
				    fSpawnHeading 	= 173.9933
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_10		
			SWITCH iPointIndex 
				CASE 0
				    vSpawnPoint 	= <<-149.9420, -1360.1604, 28.6196>>
				    fSpawnHeading 	= 297.1925
				BREAK
				CASE 1
				    vSpawnPoint 	= <<-150.7215, -1358.2743, 28.7026>>
				    fSpawnHeading 	= 283.9924
				BREAK
				CASE 2
				    vSpawnPoint 	= <<-150.8821, -1356.1034, 28.7524>>
				    fSpawnHeading 	= 268.3923
				BREAK
				CASE 3
				    vSpawnPoint 	= <<-150.8625, -1353.7942, 28.7727>>
				    fSpawnHeading 	= 268.3923
				BREAK
				CASE 4
				    vSpawnPoint 	= <<-133.3586, -1345.4371, 28.6417>>
				    fSpawnHeading 	= 142.7920
				BREAK
				CASE 5
				    vSpawnPoint 	= <<-131.7090, -1347.0692, 28.5189>>
				    fSpawnHeading 	= 135.5920
				BREAK
				CASE 6
				    vSpawnPoint 	= <<-154.7681, -1363.0498, 28.6425>>
				    fSpawnHeading 	= 299.5917
				BREAK
				CASE 7
				    vSpawnPoint 	= <<-155.5251, -1361.0588, 28.7210>>
				    fSpawnHeading 	= 281.3916
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH

	SWITCH eDropoffID
		CASE GUNRUNNING_DROPOFF_PHANTOM_COUNTRY_1
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<-2863.7339, 3501.0291, 8.3341>>
					fSpawnHeading	= 288.3989
				BREAK
				CASE 1
					vSpawnPoint 	= <<-2863.4944, 3503.4839, 8.0000>>
					fSpawnHeading	= 289.5989
				BREAK
				CASE 2
					vSpawnPoint 	= <<-2865.7524, 3504.8853, 7.9170>>
					fSpawnHeading	= 290.7989
				BREAK
				CASE 3
					vSpawnPoint 	= <<-2867.1985, 3507.7234, 7.6155>>
					fSpawnHeading	= 306.3987
				BREAK
				CASE 4
					vSpawnPoint 	= <<-2877.5681, 3511.4519, 7.3036>>
					fSpawnHeading	= 341.5979
				BREAK
				CASE 5
					vSpawnPoint 	= <<-2879.7366, 3513.1919, 7.2770>>
					fSpawnHeading	= 348.3977
				BREAK
				CASE 6
					vSpawnPoint 	= <<-2882.4751, 3513.8784, 7.2993>>
					fSpawnHeading	= 353.9977
				BREAK
				CASE 7
					vSpawnPoint 	= <<-2885.0586, 3513.4653, 7.3914>>
					fSpawnHeading	= 344.9973
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_PHANTOM_COUNTRY_2
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<1857.4663, 1824.9127, 63.8040>>
					fSpawnHeading	= 313.9965
				BREAK
				CASE 1
					vSpawnPoint 	= <<1857.1476, 1828.3701, 63.7479>>
					fSpawnHeading	= 313.9965
				BREAK
				CASE 2
					vSpawnPoint 	= <<1855.1067, 1830.7969, 63.9052>>
					fSpawnHeading	= 309.1963
				BREAK
				CASE 3
					vSpawnPoint 	= <<1852.6200, 1832.1237, 64.1362>>
					fSpawnHeading	= 307.1963
				BREAK
				CASE 4
					vSpawnPoint 	= <<1859.6808, 1804.1836, 65.1696>>
					fSpawnHeading	= 261.3957
				BREAK
				CASE 5
					vSpawnPoint 	= <<1861.4379, 1805.8765, 64.6999>>
					fSpawnHeading	= 255.7955
				BREAK
				CASE 6
					vSpawnPoint 	= <<1860.5437, 1808.5172, 64.6330>>
					fSpawnHeading	= 255.7955
				BREAK
				CASE 7
					vSpawnPoint 	= <<1861.5178, 1811.0271, 64.3307>>
					fSpawnHeading	= 258.7954
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_PHANTOM_COUNTRY_3
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<2064.3628, 4659.6953, 40.1043>>
					fSpawnHeading	= 43.3951
				BREAK
				CASE 1
					vSpawnPoint 	= <<2060.9192, 4658.8003, 40.0484>>
					fSpawnHeading	= 43.3951
				BREAK
				CASE 2
					vSpawnPoint 	= <<2060.5352, 4656.3862, 40.0140>>
					fSpawnHeading	= 43.3951
				BREAK
				CASE 3
					vSpawnPoint 	= <<2064.8647, 4662.8555, 40.0804>>
					fSpawnHeading	= 49.7950
				BREAK
				CASE 4
					vSpawnPoint 	= <<2060.2383, 4641.8413, 39.8278>>
					fSpawnHeading	= 49.7950
				BREAK
				CASE 5
					vSpawnPoint 	= <<2057.4946, 4641.1963, 39.7606>>
					fSpawnHeading	= 49.7950
				BREAK
				CASE 6
					vSpawnPoint 	= <<2056.9810, 4638.5054, 39.8439>>
					fSpawnHeading	= 49.7950
				BREAK
				CASE 7
					vSpawnPoint 	= <<2054.3494, 4637.0889, 39.7052>>
					fSpawnHeading	= 47.7950
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_PHANTOM_COUNTRY_4
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<2783.2063, 4728.0767, 44.9791>>
					fSpawnHeading	= 139.5946
				BREAK
				CASE 1
					vSpawnPoint 	= <<2784.3892, 4725.3784, 44.9900>>
					fSpawnHeading	= 139.5946
				BREAK
				CASE 2
					vSpawnPoint 	= <<2786.7334, 4724.4849, 45.0271>>
					fSpawnHeading	= 139.5946
				BREAK
				CASE 3
					vSpawnPoint 	= <<2789.2988, 4723.4531, 45.0699>>
					fSpawnHeading	= 139.5946
				BREAK
				CASE 4
					vSpawnPoint 	= <<2811.1174, 4724.4893, 45.4228>>
					fSpawnHeading	= 154.5944
				BREAK
				CASE 5
					vSpawnPoint 	= <<2809.5129, 4727.4155, 45.4013>>
					fSpawnHeading	= 154.5944
				BREAK
				CASE 6
					vSpawnPoint 	= <<2806.5950, 4727.6089, 45.3709>>
					fSpawnHeading	= 158.5943
				BREAK
				CASE 7
					vSpawnPoint 	= <<2804.6167, 4729.6245, 45.3589>>
					fSpawnHeading	= 158.5943
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_PHANTOM_COUNTRY_5
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<2733.0037, 1708.4890, 23.5816>>
					fSpawnHeading	= 166.9941
				BREAK
				CASE 1
					vSpawnPoint 	= <<2735.4321, 1707.5354, 23.5535>>
					fSpawnHeading	= 169.5940
				BREAK
				CASE 2
					vSpawnPoint 	= <<2737.4937, 1708.1571, 23.5722>>
					fSpawnHeading	= 177.5939
				BREAK
				CASE 3
					vSpawnPoint 	= <<2739.7554, 1707.4045, 23.5616>>
					fSpawnHeading	= 177.5939
				BREAK
				CASE 4
					vSpawnPoint 	= <<2784.5955, 1708.5583, 23.5872>>
					fSpawnHeading	= 185.3932
				BREAK
				CASE 5
					vSpawnPoint 	= <<2786.9836, 1708.4647, 23.5816>>
					fSpawnHeading	= 185.3932
				BREAK
				CASE 6
					vSpawnPoint 	= <<2789.2078, 1707.7313, 23.5526>>
					fSpawnHeading	= 184.1932
				BREAK
				CASE 7
					vSpawnPoint 	= <<2791.3137, 1708.4169, 23.5778>>
					fSpawnHeading	= 177.9932
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_PHANTOM_COUNTRY_6
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<-1530.2471, 2868.9348, 30.1862>>
					fSpawnHeading	= 306.7927
				BREAK
				CASE 1
					vSpawnPoint 	= <<-1527.5690, 2867.9792, 30.2224>>
					fSpawnHeading	= 306.7927
				BREAK
				CASE 2
					vSpawnPoint 	= <<-1526.4832, 2865.6270, 30.2213>>
					fSpawnHeading	= 306.7927
				BREAK
				CASE 3
					vSpawnPoint 	= <<-1526.3011, 2863.0066, 30.2057>>
					fSpawnHeading	= 306.7927
				BREAK
				CASE 4
					vSpawnPoint 	= <<-1538.9948, 2874.4128, 30.0755>>
					fSpawnHeading	= 349.7921
				BREAK
				CASE 5
					vSpawnPoint 	= <<-1541.1294, 2875.6921, 30.0697>>
					fSpawnHeading	= 353.7920
				BREAK
				CASE 6
					vSpawnPoint 	= <<-1543.7698, 2875.4783, 30.0821>>
					fSpawnHeading	= 353.7920
				BREAK
				CASE 7 
					vSpawnPoint 	= <<-1546.2577, 2875.6296, 30.0918>>
					fSpawnHeading	= 352.5919
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_PHANTOM_COUNTRY_7
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<-3115.3618, 1293.6069, 19.5672>>
					fSpawnHeading	= 303.9917
				BREAK
				CASE 1
					vSpawnPoint 	= <<-3115.1877, 1295.9266, 19.5531>>
					fSpawnHeading	= 304.5916
				BREAK
				CASE 2
					vSpawnPoint 	= <<-3117.0439, 1297.2444, 19.6683>>
					fSpawnHeading	= 306.3916
				BREAK
				CASE 3
					vSpawnPoint 	= <<-3114.0115, 1292.2383, 19.4718>>
					fSpawnHeading	= 303.1911
				BREAK
				CASE 4
					vSpawnPoint 	= <<-3122.3306, 1262.8240, 19.6253>>
					fSpawnHeading	= 233.9905
				BREAK
				CASE 5
					vSpawnPoint 	= <<-3120.1545, 1263.6908, 19.5731>>
					fSpawnHeading	= 233.9905
				BREAK
				CASE 6
					vSpawnPoint 	= <<-3119.8208, 1265.7959, 19.6634>>
					fSpawnHeading	= 236.9905
				BREAK
				CASE 7
					vSpawnPoint 	= <<-3121.7168, 1258.9780, 19.3942>>
					fSpawnHeading	= 236.9905
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_PHANTOM_COUNTRY_8
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<-510.1558, 6325.3354, 10.1306>>
					fSpawnHeading	= 236.9905
				BREAK
				CASE 1
					vSpawnPoint 	= <<-507.9496, 6326.5737, 10.0822>>
					fSpawnHeading	= 236.9905
				BREAK
				CASE 2
					vSpawnPoint 	= <<-507.4629, 6328.6563, 10.0553>>
					fSpawnHeading	= 229.3904
				BREAK
				CASE 3
					vSpawnPoint 	= <<-504.8620, 6329.6655, 10.0119>>
					fSpawnHeading	= 231.1904
				BREAK
				CASE 4
					vSpawnPoint 	= <<-521.1659, 6309.3950, 9.8781>>
					fSpawnHeading	= 255.3903
				BREAK
				CASE 5
					vSpawnPoint 	= <<-522.5894, 6305.4326, 9.8454>>
					fSpawnHeading	= 260.1901
				BREAK
				CASE 6
					vSpawnPoint 	= <<-523.9474, 6301.9976, 9.7077>>
					fSpawnHeading	= 260.1901
				BREAK
				CASE 7
					vSpawnPoint 	= <<-523.6025, 6298.8145, 9.4764>>
					fSpawnHeading	= 260.1901
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_PHANTOM_COUNTRY_9
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<811.7854, 6514.5679, 22.4369>>
					fSpawnHeading	= 179.7994
				BREAK
				CASE 1
					vSpawnPoint 	= <<813.7658, 6513.4585, 22.3588>>
					fSpawnHeading	= 175.5993
				BREAK
				CASE 2
					vSpawnPoint 	= <<816.1437, 6514.3701, 22.1967>>
					fSpawnHeading	= 175.5993
				BREAK
				CASE 3
					vSpawnPoint 	= <<818.6289, 6513.7656, 22.1592>>
					fSpawnHeading	= 175.5993
				BREAK
				CASE 4
					vSpawnPoint 	= <<825.0697, 6513.9287, 21.9763>>
					fSpawnHeading	= 176.7993
				BREAK
				CASE 5
					vSpawnPoint 	= <<827.0150, 6512.7715, 21.9052>>
					fSpawnHeading	= 176.7993
				BREAK
				CASE 6
					vSpawnPoint 	= <<829.5932, 6512.0186, 21.8051>>
					fSpawnHeading	= 176.7993
				BREAK
				CASE 7
					vSpawnPoint 	= <<831.7493, 6512.7920, 21.7500>>
					fSpawnHeading	= 176.7993
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_PHANTOM_COUNTRY_10
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<970.1619, 4414.3130, 46.0788>>
					fSpawnHeading	= 7.1986
				BREAK
				CASE 1
					vSpawnPoint 	= <<967.8749, 4416.6440, 46.2423>>
					fSpawnHeading	= 7.1986
				BREAK
				CASE 2
					vSpawnPoint 	= <<965.9127, 4415.1006, 46.3406>>
					fSpawnHeading	= 2.9986
				BREAK
				CASE 3
					vSpawnPoint 	= <<963.4276, 4416.7358, 46.4660>>
					fSpawnHeading	= 2.9986
				BREAK
				CASE 4
					vSpawnPoint 	= <<954.2126, 4420.6602, 46.9861>>
					fSpawnHeading	= 345.1982
				BREAK
				CASE 5
					vSpawnPoint 	= <<952.0507, 4420.9722, 47.1055>>
					fSpawnHeading	= 345.1982
				BREAK
				CASE 6
					vSpawnPoint 	= <<950.0118, 4422.0532, 47.2178>>
					fSpawnHeading	= 345.1982
				BREAK
				CASE 7
					vSpawnPoint 	= <<956.4673, 4418.7368, 46.8541>>
					fSpawnHeading	= 345.1982
				BREAK
			ENDSWITCH
		BREAK

		CASE GUNRUNNING_DROPOFF_PHANTOM_CITY_1
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<-749.9396, -2224.8936, 4.7978>>
					fSpawnHeading	= 0.0000
				BREAK
				CASE 1
					vSpawnPoint 	= <<-751.9151, -2225.4553, 4.7920>>
					fSpawnHeading	= 353.9999
				BREAK
				CASE 2
					vSpawnPoint 	= <<-754.1807, -2224.8328, 4.7860>>
					fSpawnHeading	= 360.5998
				BREAK
				CASE 3
					vSpawnPoint 	= <<-756.0544, -2225.2703, 4.7860>>
					fSpawnHeading	= 359.3997
				BREAK
				CASE 4
					vSpawnPoint 	= <<-758.2713, -2224.4490, 4.7866>>
					fSpawnHeading	= 1.7997
				BREAK
				CASE 5
					vSpawnPoint 	= <<-760.1992, -2225.0894, 4.7860>>
					fSpawnHeading	= -0.2003
				BREAK
				CASE 6
					vSpawnPoint 	= <<-762.1611, -2224.3828, 4.7871>>
					fSpawnHeading	= -0.2003
				BREAK
				CASE 7
					vSpawnPoint 	= <<-764.0490, -2225.2249, 4.7860>>
					fSpawnHeading	= 2.3995
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_PHANTOM_CITY_2
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<-336.9368, -2572.0891, 5.0202>>
					fSpawnHeading	= 314.5988
				BREAK
				CASE 1
					vSpawnPoint 	= <<-338.4848, -2570.7100, 5.0082>>
					fSpawnHeading	= 314.5988
				BREAK
				CASE 2
					vSpawnPoint 	= <<-335.4114, -2573.8262, 5.0082>>
					fSpawnHeading	= 314.5988
				BREAK
				CASE 3
					vSpawnPoint 	= <<-333.1522, -2575.2808, 5.0085>>
					fSpawnHeading	= 314.5988
				BREAK
				CASE 4
					vSpawnPoint 	= <<-333.5012, -2569.0134, 5.0066>>
					fSpawnHeading	= 314.5988
				BREAK
				CASE 5
					vSpawnPoint 	= <<-335.4506, -2567.7212, 5.0060>>
					fSpawnHeading	= 314.5988
				BREAK
				CASE 6
					vSpawnPoint 	= <<-332.6393, -2571.0269, 5.0060>>
					fSpawnHeading	= 314.5988
				BREAK
				CASE 7
					vSpawnPoint 	= <<-330.3058, -2572.1860, 5.0069>>
					fSpawnHeading	= 314.5988
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_PHANTOM_CITY_3
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<121.0204, -2819.9907, 5.0002>>
					fSpawnHeading	= 256.7982
				BREAK
				CASE 1
					vSpawnPoint 	= <<121.3664, -2822.4758, 5.0423>>
					fSpawnHeading	= 256.7982
				BREAK
				CASE 2
					vSpawnPoint 	= <<120.3422, -2824.9692, 5.0002>>
					fSpawnHeading	= 256.7982
				BREAK
				CASE 3
					vSpawnPoint 	= <<120.8707, -2827.7471, 5.0002>>
					fSpawnHeading	= 260.9980
				BREAK
				CASE 4
					vSpawnPoint 	= <<115.2697, -2831.5994, 5.0000>>
					fSpawnHeading	= 268.1980
				BREAK
				CASE 5
					vSpawnPoint 	= <<115.8834, -2834.0308, 5.0000>>
					fSpawnHeading	= 267.1979
				BREAK
				CASE 6
					vSpawnPoint 	= <<114.7470, -2836.3184, 5.0000>>
					fSpawnHeading	= 267.1979
				BREAK
				CASE 7
					vSpawnPoint 	= <<115.7083, -2838.4868, 5.0000>>
					fSpawnHeading	= 274.3979
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_PHANTOM_CITY_4
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<1139.8101, -3259.1240, 4.9007>>
					fSpawnHeading	= 4.3975
				BREAK
				CASE 1
					vSpawnPoint 	= <<1137.6697, -3257.9121, 4.9007>>
					fSpawnHeading	= 3.1975
				BREAK
				CASE 2
					vSpawnPoint 	= <<1135.8846, -3259.2715, 4.9006>>
					fSpawnHeading	= 358.9976
				BREAK
				CASE 3
					vSpawnPoint 	= <<1134.0117, -3257.9355, 4.9006>>
					fSpawnHeading	= 358.9976
				BREAK
				CASE 4
					vSpawnPoint 	= <<1151.0505, -3259.5747, 4.9008>>
					fSpawnHeading	= 356.5974
				BREAK
				CASE 5
					vSpawnPoint 	= <<1149.0919, -3258.1167, 4.9008>>
					fSpawnHeading	= 356.5974
				BREAK
				CASE 6
					vSpawnPoint 	= <<1146.9697, -3259.3330, 4.9008>>
					fSpawnHeading	= 356.5974
				BREAK
				CASE 7
					vSpawnPoint 	= <<1144.7407, -3258.2271, 4.9008>>
					fSpawnHeading	= 360.7973
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_PHANTOM_CITY_5
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<791.5980, -2498.9595, 20.1933>>
					fSpawnHeading	= 88.7968
				BREAK
				CASE 1
					vSpawnPoint 	= <<792.7791, -2501.0833, 20.3249>>
					fSpawnHeading	= 85.7968
				BREAK
				CASE 2
					vSpawnPoint 	= <<790.7320, -2503.1616, 20.1484>>
					fSpawnHeading	= 85.7968
				BREAK
				CASE 3
					vSpawnPoint 	= <<791.9630, -2505.2209, 20.2916>>
					fSpawnHeading	= 80.3968
				BREAK
				CASE 4
					vSpawnPoint 	= <<817.6115, -2488.8337, 22.3010>>
					fSpawnHeading	= 4.7967
				BREAK
				CASE 5
					vSpawnPoint 	= <<815.3525, -2487.2705, 22.0523>>
					fSpawnHeading	= 4.7967
				BREAK
				CASE 6
					vSpawnPoint 	= <<813.1287, -2488.5820, 21.9835>>
					fSpawnHeading	= 359.3967
				BREAK
				CASE 7
					vSpawnPoint 	= <<810.8846, -2487.3738, 21.7046>>
					fSpawnHeading	= 350.3965
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_PHANTOM_CITY_6
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<-49.5821, -2435.1758, 5.0010>>
					fSpawnHeading	= 332.1958
				BREAK
				CASE 1
					vSpawnPoint 	= <<-51.8241, -2434.5093, 5.0009>>
					fSpawnHeading	= 332.1958
				BREAK
				CASE 2
					vSpawnPoint 	= <<-52.9491, -2432.4651, 5.0006>>
					fSpawnHeading	= 326.5957
				BREAK
				CASE 3
					vSpawnPoint 	= <<-55.2325, -2431.7637, 5.0006>>
					fSpawnHeading	= 326.5957
				BREAK
				CASE 4
					vSpawnPoint 	= <<-40.6483, -2439.1577, 5.0013>>
					fSpawnHeading	= 300.9953
				BREAK
				CASE 5
					vSpawnPoint 	= <<-41.6158, -2437.0156, 5.0011>>
					fSpawnHeading	= 300.9953
				BREAK
				CASE 6
					vSpawnPoint 	= <<-42.6932, -2434.7224, 5.0009>>
					fSpawnHeading	= 300.9953
				BREAK
				CASE 7
					vSpawnPoint 	= <<-43.6750, -2432.7810, 5.0008>>
					fSpawnHeading	= 300.9953
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_PHANTOM_CITY_7
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<-966.8149, -3516.1165, 13.1465>>
					fSpawnHeading	= 322.1951
				BREAK
				CASE 1
					vSpawnPoint 	= <<-968.8292, -3514.2917, 13.1465>>
					fSpawnHeading	= 322.1951
				BREAK
				CASE 2
					vSpawnPoint 	= <<-971.1616, -3512.9656, 13.1465>>
					fSpawnHeading	= 322.1951
				BREAK
				CASE 3
					vSpawnPoint 	= <<-973.0881, -3511.6550, 13.1465>>
					fSpawnHeading	= 322.1951
				BREAK
				CASE 4
					vSpawnPoint 	= <<-944.8167, -3509.5095, 13.1466>>
					fSpawnHeading	= 330.9948
				BREAK
				CASE 5
					vSpawnPoint 	= <<-946.6290, -3508.0825, 13.1466>>
					fSpawnHeading	= 325.7946
				BREAK
				CASE 6
					vSpawnPoint 	= <<-948.7563, -3507.1094, 13.1466>>
					fSpawnHeading	= 325.7946
				BREAK
				CASE 7
					vSpawnPoint 	= <<-950.7985, -3505.7195, 13.1465>>
					fSpawnHeading	= 336.5943
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_PHANTOM_CITY_8
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<1463.6593, -2424.9846, 65.4594>>
					fSpawnHeading	= 73.1939
				BREAK
				CASE 1
					vSpawnPoint 	= <<1464.4661, -2422.6924, 65.5676>>
					fSpawnHeading	= 73.1939
				BREAK
				CASE 2
					vSpawnPoint 	= <<1463.1770, -2427.0339, 65.2831>>
					fSpawnHeading	= 73.1939
				BREAK
				CASE 3
					vSpawnPoint 	= <<1461.9559, -2429.0205, 65.1292>>
					fSpawnHeading	= 73.1939
				BREAK
				CASE 4
					vSpawnPoint 	= <<1461.4030, -2431.5435, 64.9206>>
					fSpawnHeading	= 73.1939
				BREAK
				CASE 5
					vSpawnPoint 	= <<1460.7990, -2433.9277, 64.7493>>
					fSpawnHeading	= 73.1939
				BREAK
				CASE 6
					vSpawnPoint 	= <<1459.3258, -2435.9966, 64.4889>>
					fSpawnHeading	= 73.1939
				BREAK
				CASE 7
					vSpawnPoint 	= <<1459.2849, -2438.5547, 64.2416>>
					fSpawnHeading	= 73.1939
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_PHANTOM_CITY_9
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<803.4257, -2945.9907, 5.0207>>
					fSpawnHeading	= 1.3937
				BREAK
				CASE 1
					vSpawnPoint 	= <<800.5339, -2946.5286, 5.0207>>
					fSpawnHeading	= 0.1937
				BREAK
				CASE 2
					vSpawnPoint 	= <<798.1046, -2946.1182, 5.0207>>
					fSpawnHeading	= 0.1937
				BREAK
				CASE 3
					vSpawnPoint 	= <<795.6312, -2946.4983, 5.0207>>
					fSpawnHeading	= 359.5937
				BREAK
				CASE 4
					vSpawnPoint 	= <<795.5524, -2932.0459, 4.8538>>
					fSpawnHeading	= 150.7933
				BREAK
				CASE 5
					vSpawnPoint 	= <<793.9752, -2928.9956, 4.8364>>
					fSpawnHeading	= 153.7932
				BREAK
				CASE 6
					vSpawnPoint 	= <<790.9120, -2927.6042, 4.8253>>
					fSpawnHeading	= 153.7932
				BREAK
				CASE 7
					vSpawnPoint 	= <<787.7563, -2927.4885, 4.8185>>
					fSpawnHeading	= 153.7932
				BREAK
			ENDSWITCH
		BREAK
		CASE GUNRUNNING_DROPOFF_PHANTOM_CITY_10
			SWITCH iPointIndex
				CASE 0
					vSpawnPoint 	= <<-1170.9305, -2903.8428, 12.9452>>
					fSpawnHeading	= 308.3925
				BREAK
				CASE 1
					vSpawnPoint 	= <<-1171.0830, -2901.0146, 12.9453>>
					fSpawnHeading	= 308.3925
				BREAK
				CASE 2
					vSpawnPoint 	= <<-1173.0746, -2899.1501, 12.9453>>
					fSpawnHeading	= 308.3925
				BREAK
				CASE 3
					vSpawnPoint 	= <<-1174.7642, -2897.1572, 12.9453>>
					fSpawnHeading	= 308.3925
				BREAK
				CASE 4
					vSpawnPoint 	= <<-1179.3281, -2888.2910, 12.9454>>
					fSpawnHeading	= 328.5916
				BREAK
				CASE 5
					vSpawnPoint 	= <<-1180.9073, -2886.3528, 12.9454>>
					fSpawnHeading	= 328.5916
				BREAK
				CASE 6
					vSpawnPoint 	= <<-1182.9829, -2885.3523, 12.9454>>
					fSpawnHeading	= 328.5916
				BREAK
				CASE 7
					vSpawnPoint 	= <<-1185.3142, -2884.4329, 12.9454>>
					fSpawnHeading	= 331.5915
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

