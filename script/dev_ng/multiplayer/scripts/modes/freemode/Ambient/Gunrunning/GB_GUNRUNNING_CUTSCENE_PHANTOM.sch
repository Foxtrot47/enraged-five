//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name: gb_gunrunning_cutscene_phantom.sc																	//
// Description: Script defining single cutscene for move weapons sell mission.								//
//				This script always needs to define:															//
//																											//
//				CONSTRUCT_[NAME]_CUTSCENE																	//
//				HAS_[NAME]_CUTSCENE_INITIALISED																//
//				START_[NAME]_CUTSCENE																		//
//				MAINTAIN_[NAME]_CUTSCENE																	//
//				DESTRUCT_[NAME]_CUTSCENE																	//
//				MAINTAIN_[NAME]_CUTSCENE_DEBUG																//
//																											//
// Written by:  Tymon																						//
// Date:  		14/02/2017																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "net_simple_cutscene.sch"


USING "net_freemode_delivery_common.sch"

// Vectors
CONST_INT PHANTOM_VEC_INITIAL_LOOK_AT_DIR			0
CONST_INT PHANTOM_VEC_CARGOBOB_DIR					1
CONST_INT PHANTOM_VEC_CARGOBOB_START				2
CONST_INT PHANTOM_VEC_CARGOBOB_END					3
CONST_INT PHANTOM_VEC_CARGOBOB_TRACKING				4

// Floats
CONST_INT PHANTOM_FLOAT_MAX_TRACKING_DIST			0
CONST_INT PHANTOM_FLOAT_FOV_TIME_FACTOR				1
CONST_INT PHANTOM_FLOAT_CARGOBOB_FOLLOW_SPEED		2

// BS
CONST_INT PHANTOM_BS_CUTSCENE						0
	CONST_INT BS_PHANTOM_CUTSCENE_CARGOBOB_FROZEN			0
	CONST_INT BS_PHANTOM_CUTSCENE_HOOK_DEPLOYED				1
	CONST_INT BS_PHANTOM_CUTSCENE_CARGOBOB_SET_FOR_PICKUP 	2
	CONST_INT BS_PHANTOM_CUTSCENE_CARGOBOB2_REPOSITION		3
	CONST_INT BS_PHANTOM_CUTSCENE_CARGOBOB_LAND				4

// Models
CONST_INT PHANTOM_MODEL_TRUCK					0
CONST_INT PHANTOM_MODEL_CARGOBOB				1
CONST_INT PHANTOM_MODEL_TRAILER					2
CONST_INT PHANTOM_MODEL_PILOT					3

// Vehicles
CONST_INT PHANTOM_VEH_TRUCK						0
CONST_INT PHANTOM_VEH_TRAILER					1
CONST_INT PHANTOM_VEH_CARGOBOB					2
CONST_INT PHANTOM_VEH_CARGOBOB2					3

// Peds
CONST_INT PHANTOM_PED_PILOT						0
CONST_INT PHANTOM_PED_PILOT2					1

FUNC BOOL CONSTRUCT_PHANTOM_CUTSCENE(SIMPLE_CUTSCENE &cutscene, FREEMODE_DELIVERY_CUTSCENE_DATA &data, ENTITY_INDEX deliveredEntity = NULL)
	UNUSED_PARAMETER(cutscene)
	UNUSED_PARAMETER(deliveredEntity)
	data.models[PHANTOM_MODEL_TRUCK] = PHANTOM2
	data.models[PHANTOM_MODEL_CARGOBOB] = CARGOBOB
	data.models[PHANTOM_MODEL_TRAILER] = TRAILERS
	data.models[PHANTOM_MODEL_PILOT] = S_M_Y_PILOT_01
	
	data.fPool[PHANTOM_FLOAT_FOV_TIME_FACTOR] = -1.0
	
	VECTOR vCamStart = data.vBasePos + <<0.0, 0.0, 200.0>> // Starting position for camera up in the sky
	VECTOR vLookAtPoint = <<0.0, 0.0, 200.0>> // Always looking towards the center of the map as it provides more interesting scenery
	data.vPool[PHANTOM_VEC_INITIAL_LOOK_AT_DIR] = NORMALISE_VECTOR(vLookAtPoint - vCamStart)
	VECTOR vCamRotStart = _GET_CAM_ROT_FROM_DIR(data.vPool[PHANTOM_VEC_INITIAL_LOOK_AT_DIR])
	
	SIMPLE_CUTSCENE_CREATE(cutscene, "phantom_delivery")
	SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 10000, "scene1", vCamStart, vCamRotStart, 40.0, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 0.0, 0.1)
	SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 6000, "scene2", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 15.0, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 15.0, 0.1) // This scene coords will be filled in on the fly
	
	RETURN TRUE
ENDFUNC

FUNC BOOL HAS_PHANTOM_CUTSCENE_INITIALISED(SIMPLE_CUTSCENE &cutscene, FREEMODE_DELIVERY_CUTSCENE_DATA &data)
	UNUSED_PARAMETER(cutscene)
	UNUSED_PARAMETER(data)
	RETURN TRUE
ENDFUNC

PROC START_PHANTOM_CUTSCENE(SIMPLE_CUTSCENE &cutscene, FREEMODE_DELIVERY_CUTSCENE_DATA &data)
	UNUSED_PARAMETER(cutscene)

	VECTOR vCreationCoords = data.vBasePos
	FLOAT fCreationHeading = data.fBaseHeading
	
	IF DOES_ENTITY_EXIST(data.vehActors[PHANTOM_VEH_TRUCK])
		CDEBUG1LN(DEBUG_SHOOTRANGE, "PHANTOM_VEH_TRUCK vehicle: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(data.vehActors[PHANTOM_VEH_TRUCK])))
		IF GET_ENTITY_MODEL(data.vehActors[PHANTOM_VEH_TRUCK]) <> PHANTOM2
			CDEBUG1LN(DEBUG_SHOOTRANGE, "PHANTOM_VEH_TRUCK delete, not a truck.")
			DELETE_VEHICLE(data.vehActors[PHANTOM_VEH_TRUCK])
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_SHOOTRANGE, "PHANTOM_VEH_TRUCK NOPE")
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(data.vehActors[PHANTOM_VEH_TRAILER])
		PRINTLN("[GUNRUNNING_CUTSCENE] START_PHANTOM_CUTSCENE - trailer does not exist, creating one.")
		data.vehActors[PHANTOM_VEH_TRAILER] = CREATE_VEHICLE(data.models[PHANTOM_MODEL_TRAILER], vCreationCoords, GET_ENTITY_HEADING(data.vehActors[PHANTOM_VEH_TRUCK]), FALSE, FALSE)
	ENDIF
			
	IF DOES_ENTITY_EXIST(data.vehActors[PHANTOM_VEH_TRAILER])
		SET_TRAILER_LEGS_RAISED(data.vehActors[PHANTOM_VEH_TRAILER])
		//If truck cab exists, make sure trailer is detached.
		IF DOES_ENTITY_EXIST(data.vehActors[PHANTOM_VEH_TRUCK])
		AND NOT IS_ENTITY_DEAD(data.vehActors[PHANTOM_VEH_TRUCK])
			SET_ENTITY_NO_COLLISION_ENTITY(data.vehActors[PHANTOM_VEH_TRUCK], data.vehActors[PHANTOM_VEH_TRAILER], FALSE)
			
			IF IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(data.vehActors[PHANTOM_VEH_TRAILER])
				PRINTLN("[GUNRUNNING_CUTSCENE] START_PHANTOM_CUTSCENE - trailer was attached to something detaching.")
				DETACH_ENTITY(data.vehActors[PHANTOM_VEH_TRAILER])
			ENDIF
			
			SET_ENTITY_COORDS_NO_OFFSET(data.vehActors[PHANTOM_VEH_TRAILER], GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(data.vehActors[PHANTOM_VEH_TRUCK], << 0.111684, -8.4309, 1.53631 >>))
			SET_ENTITY_HEADING(data.vehActors[PHANTOM_VEH_TRAILER], GET_ENTITY_HEADING(data.vehActors[PHANTOM_VEH_TRUCK]))
		ELSE
			#IF IS_DEBUG_BUILD
			VECTOR vPrevCoord
				vPrevCoord = GET_ENTITY_COORDS(data.vehActors[PHANTOM_VEH_TRAILER])
			#ENDIF
			
			VECTOR vNewCoords
			vNewCoords = GET_GROUND_COORD_ADJUSTED_FOR_MODEL_DIMENSIONS(GET_ENTITY_MODEL(data.vehActors[PHANTOM_VEH_TRAILER]), vCreationCoords)
			
			SET_ENTITY_COORDS_NO_OFFSET(data.vehActors[PHANTOM_VEH_TRAILER], vNewCoords)
			SET_ENTITY_ROTATION(data.vehActors[PHANTOM_VEH_TRAILER], <<0.0, 0.0, GET_ENTITY_HEADING(data.vehActors[PHANTOM_VEH_TRAILER])>>)
		
			CDEBUG1LN(DEBUG_SHOOTRANGE, "Truck cab does not exist, currentCoord: ", vPrevCoord, " vNewCoords: ", vNewCoords)
		ENDIF
		
		vCreationCoords = GET_ENTITY_COORDS(data.vehActors[PHANTOM_VEH_TRAILER])
		fCreationHeading = GET_ENTITY_HEADING(data.vehActors[PHANTOM_VEH_TRAILER])
	ENDIF		
		
		
	//IF DOES_ENTITY_EXIST(data.vehActors[PHANTOM_VEH_TRUCK])
	//AND NOT IS_ENTITY_DEAD(data.vehActors[PHANTOM_VEH_TRUCK])
	
		IF DOES_ENTITY_EXIST(data.vehActors[PHANTOM_VEH_TRAILER])
		AND NOT IS_ENTITY_DEAD(data.vehActors[PHANTOM_VEH_TRAILER])

			VECTOR vCamForward = ROTATE_VECTOR_ABOUT_Z_ORTHO(GET_HEADING_AS_VECTOR(fCreationHeading), ROTSTEP_180)
			vCamForward = ROTATE_VECTOR_ABOUT_Z(vCamForward, fCreationHeading)
			
			VECTOR vTruckDir = GET_ENTITY_FORWARD_VECTOR(data.vehActors[PHANTOM_VEH_TRAILER])
			
			// Second scene starts looking frome above top down on the truck and trailer being picked up by the cargobob
			cutscene.sScenes[1].vCamCoordsStart = vCreationCoords + <<0.0, 0.0, 50.0>> + vTruckDir * 2.0 + ROTATE_VECTOR_ABOUT_Z_ORTHO(vCamForward, ROTSTEP_NEG_90) * 5.0
			
			VECTOR vCamLookAt = cutscene.sScenes[1].vCamCoordsStart + vCamForward * 2.0
			vCamLookAt.Z = vCreationCoords.Z
			
			VECTOR vCamLookAtDir = NORMALISE_VECTOR(vCamLookAt - cutscene.sScenes[1].vCamCoordsStart)
			
			cutscene.sScenes[1].vCamCoordsFinish = cutscene.sScenes[1].vCamCoordsStart + ROTATE_VECTOR_ABOUT_Z_ORTHO(vCamForward, ROTSTEP_90) * 1.0
			cutscene.sScenes[1].vCamRotStart = _GET_CAM_ROT_FROM_DIR(vCamLookAtDir)
			cutscene.sScenes[1].vCamRotFinish = cutscene.sScenes[1].vCamRotStart
			
		ENDIF
	//ENDIF
	
	data.vPool[PHANTOM_VEC_CARGOBOB_START] = cutscene.sScenes[0].vCamCoordsStart + data.vPool[PHANTOM_VEC_INITIAL_LOOK_AT_DIR] * 30.0 + ROTATE_VECTOR_ABOUT_Z_ORTHO(data.vPool[PHANTOM_VEC_INITIAL_LOOK_AT_DIR], ROTSTEP_NEG_90) * 28.0
	data.vPool[PHANTOM_VEC_CARGOBOB_DIR] = ROTATE_VECTOR_ABOUT_Z_ORTHO(data.vPool[PHANTOM_VEC_INITIAL_LOOK_AT_DIR], ROTSTEP_90)
	FLOAT fCargobobHeading = GET_HEADING_FROM_VECTOR_2D(data.vPool[PHANTOM_VEC_CARGOBOB_DIR].X, data.vPool[PHANTOM_VEC_CARGOBOB_DIR].Y) + 10.0
	data.vPool[PHANTOM_VEC_CARGOBOB_DIR] = GET_HEADING_AS_VECTOR(fCargobobHeading)
	data.vPool[PHANTOM_VEC_CARGOBOB_END] = vCreationCoords + data.vPool[PHANTOM_VEC_INITIAL_LOOK_AT_DIR] * 34.0 + ROTATE_VECTOR_ABOUT_Z_ORTHO(data.vPool[PHANTOM_VEC_INITIAL_LOOK_AT_DIR], ROTSTEP_NEG_90) * 5.0
	
	data.vPool[PHANTOM_VEC_CARGOBOB_TRACKING] = cutscene.sScenes[0].vCamCoordsStart + data.vPool[PHANTOM_VEC_INITIAL_LOOK_AT_DIR] * 30.0
	data.fPool[PHANTOM_FLOAT_MAX_TRACKING_DIST] = VDIST(data.vPool[PHANTOM_VEC_CARGOBOB_TRACKING], data.vPool[PHANTOM_VEC_CARGOBOB_START])
	
	data.vehActors[PHANTOM_VEH_CARGOBOB] = CREATE_VEHICLE(data.models[PHANTOM_MODEL_CARGOBOB], data.vPool[PHANTOM_VEC_CARGOBOB_START], fCargobobHeading, FALSE, FALSE)
	data.pedActors[PHANTOM_PED_PILOT] = CREATE_PED(PEDTYPE_CIVMALE, data.models[PHANTOM_MODEL_PILOT], data.vPool[PHANTOM_VEC_CARGOBOB_START], 0.0, FALSE, FALSE)
	
	IF DOES_ENTITY_EXIST(data.vehActors[PHANTOM_VEH_CARGOBOB])
	AND NOT IS_ENTITY_DEAD(data.vehActors[PHANTOM_VEH_CARGOBOB])
	AND DOES_ENTITY_EXIST(data.pedActors[PHANTOM_PED_PILOT])
	AND NOT IS_ENTITY_DEAD(data.pedActors[PHANTOM_PED_PILOT])
		SET_PED_INTO_VEHICLE(data.pedActors[PHANTOM_PED_PILOT], data.vehActors[PHANTOM_VEH_CARGOBOB])
		SET_HELI_BLADES_FULL_SPEED(data.vehActors[PHANTOM_VEH_CARGOBOB])
		CREATE_PICK_UP_ROPE_FOR_CARGOBOB(data.vehActors[PHANTOM_VEH_CARGOBOB])
		SET_VEHICLE_FORWARD_SPEED(data.vehActors[PHANTOM_VEH_CARGOBOB], 5.0)
		SET_VEHICLE_DISABLE_TOWING(data.vehActors[PHANTOM_VEH_CARGOBOB], TRUE)
		
		INT iHeight
		
		FLOAT fGround
		IF GET_GROUND_Z_FOR_3D_COORD(GET_ENTITY_COORDS(data.vehActors[PHANTOM_VEH_CARGOBOB]), fGround) 
			iHeight = ROUND(ABSF(cutscene.sScenes[0].vCamCoordsStart.z - fGround))
			CDEBUG1LN(DEBUG_SHOOTRANGE, " USING HEIGHT GROUND")
		ELSE
			iHeight = ROUND(GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), data.vehActors[PHANTOM_VEH_CARGOBOB]))
			CDEBUG1LN(DEBUG_SHOOTRANGE, " USING HEIGHT ELSE")
		ENDIF
		
		TASK_HELI_MISSION(data.pedActors[PHANTOM_PED_PILOT], data.vehActors[PHANTOM_VEH_CARGOBOB], NULL, NULL, data.vPool[PHANTOM_VEC_CARGOBOB_END], MISSION_GOTO, 20.0, 1.0, fCargobobHeading, iHeight, iHeight-5)
		SET_HELI_TURBULENCE_SCALAR(data.vehActors[PHANTOM_VEH_CARGOBOB], 0.3)
	ENDIF
	
	// Create second cargobob that will fly off with the trailer
	data.vehActors[PHANTOM_VEH_CARGOBOB2] = CREATE_VEHICLE(data.models[PHANTOM_MODEL_CARGOBOB], <<0.0, 0.0, -30.0>>, GET_ENTITY_HEADING(data.vehActors[PHANTOM_VEH_TRAILER]) + 10.0, FALSE, FALSE)
	data.pedActors[PHANTOM_PED_PILOT2] = CREATE_PED(PEDTYPE_CIVMALE, data.models[PHANTOM_MODEL_PILOT], data.vPool[PHANTOM_VEC_CARGOBOB_START], 0.0, FALSE, FALSE)
	
	IF DOES_ENTITY_EXIST(data.vehActors[PHANTOM_VEH_CARGOBOB2])
	AND NOT IS_ENTITY_DEAD(data.vehActors[PHANTOM_VEH_CARGOBOB2])
	AND DOES_ENTITY_EXIST(data.pedActors[PHANTOM_PED_PILOT2])
	AND NOT IS_ENTITY_DEAD(data.pedActors[PHANTOM_PED_PILOT2])
		SET_PED_INTO_VEHICLE(data.pedActors[PHANTOM_PED_PILOT2], data.vehActors[PHANTOM_VEH_CARGOBOB2])
		SET_HELI_BLADES_FULL_SPEED(data.vehActors[PHANTOM_VEH_CARGOBOB2])
		CREATE_PICK_UP_ROPE_FOR_CARGOBOB(data.vehActors[PHANTOM_VEH_CARGOBOB2])
		SET_PICKUP_ROPE_LENGTH_FOR_CARGOBOB(data.vehActors[PHANTOM_VEH_CARGOBOB2], 2.0, 2.0, TRUE)
		SET_HELI_TURBULENCE_SCALAR(data.vehActors[PHANTOM_VEH_CARGOBOB2], 0.0)
		SET_VEHICLE_DISABLE_TOWING(data.vehActors[PHANTOM_VEH_CARGOBOB2], TRUE)
		//TASK_HELI_MISSION(data.pedActors[PHANTOM_PED_PILOT2], data.vehActors[PHANTOM_VEH_CARGOBOB2], NULL, NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(data.vehActors[PHANTOM_VEH_TRAILER], <<0.0, -2.0, 4.0>>), MISSION_STOP, 40.0, 1.0, GET_ENTITY_HEADING(data.vehActors[PHANTOM_VEH_TRUCK]) + 10.0, 0, 0)
	ENDIF
	
	IF DOES_ENTITY_EXIST(data.vehDropOff)
		IF IS_VEHICLE_CARGOBOB(data.vehDropOff)	
			//task driver of the cargobob to fly above the bunker
			IF NOT IS_VEHICLE_SEAT_FREE(data.vehDropOff)
				PED_INDEX cargobobPilot
				cargobobPilot = GET_PED_IN_VEHICLE_SEAT(data.vehDropOff)
				IF DOES_ENTITY_EXIST(cargobobPilot)
					IF cargobobPilot = PLAYER_PED_ID()
						TASK_HELI_MISSION(PLAYER_PED_ID(), data.vehDropOff, NULL, NULL, (FREEMODE_DELIVERY_DROPOFF_GET_IN_CAR_COORDS(data.eDropoffID) + <<0.0, 0.0, 20.0>>), MISSION_GOTO, 30.0, 0.0, GET_ENTITY_HEADING(data.vehDropOff), 20, 20)
					ELSE
						PRINTLN("[GUNRUNNING_CUTSCENE] _CREATE_BUNKER_DELIVERY_VEHICLE_SCENES - not the driver.")
					ENDIF
				ELSE
					PRINTLN("[GUNRUNNING_CUTSCENE] _CREATE_BUNKER_DELIVERY_VEHICLE_SCENES - did not task as there is noone in driver seat.")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_PHANTOM_CUTSCENE(SIMPLE_CUTSCENE &cutscene, FREEMODE_DELIVERY_CUTSCENE_DATA &data)
	UNUSED_PARAMETER(cutscene)
	UNUSED_PARAMETER(data)
	
	IF NOT DOES_ENTITY_EXIST(data.vehActors[PHANTOM_VEH_CARGOBOB])
	OR IS_ENTITY_DEAD(data.vehActors[PHANTOM_VEH_CARGOBOB])
	OR NOT DOES_ENTITY_EXIST(data.vehActors[PHANTOM_VEH_CARGOBOB2])
	OR IS_ENTITY_DEAD(data.vehActors[PHANTOM_VEH_CARGOBOB2])
	OR NOT DOES_ENTITY_EXIST(data.pedActors[PHANTOM_PED_PILOT])
	OR IS_ENTITY_DEAD(data.pedActors[PHANTOM_PED_PILOT])
	OR NOT DOES_ENTITY_EXIST(data.pedActors[PHANTOM_PED_PILOT2])
	OR IS_ENTITY_DEAD(data.pedActors[PHANTOM_PED_PILOT2])
	OR NOT DOES_ENTITY_EXIST(data.vehActors[PHANTOM_VEH_TRAILER])
	OR IS_ENTITY_DEAD(data.vehActors[PHANTOM_VEH_TRAILER])
	//OR NOT DOES_ENTITY_EXIST(data.vehActors[PHANTOM_VEH_TRUCK])
	//OR IS_ENTITY_DEAD(data.vehActors[PHANTOM_VEH_TRUCK])
		EXIT
	ENDIF
	
	FLOAT fTimeFactor
	
	SET_HELI_BLADES_FULL_SPEED(data.vehActors[PHANTOM_VEH_CARGOBOB2])
	
	IF SIMPLE_CUTSCENE_IS_SCENE_RUNNING(cutscene, 0)
		//REposition cargobob2 for 2nd scene due to hook ...
		IF NOT IS_BIT_SET(data.iBSPool[PHANTOM_BS_CUTSCENE], BS_PHANTOM_CUTSCENE_CARGOBOB2_REPOSITION)
			IF DOES_ENTITY_EXIST(data.vehActors[PHANTOM_VEH_CARGOBOB2])
			AND DOES_ENTITY_EXIST(data.vehActors[PHANTOM_VEH_TRAILER])
				SET_ENTITY_COORDS_NO_OFFSET(data.vehActors[PHANTOM_VEH_CARGOBOB2], GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(data.vehActors[PHANTOM_VEH_TRAILER], <<0.0, -2.0, 5.0>>))
				SET_ENTITY_HEADING(data.vehActors[PHANTOM_VEH_CARGOBOB2], GET_ENTITY_HEADING(data.vehActors[PHANTOM_VEH_TRAILER]) + 10.0)
			ENDIF
			SET_BIT(data.iBSPool[PHANTOM_BS_CUTSCENE], BS_PHANTOM_CUTSCENE_CARGOBOB2_REPOSITION)
		ENDIF
		
		fTimeFactor = FMIN(1.0, TO_FLOAT(cutscene.iCurrentSceneElapsedTime) / TO_FLOAT(cutscene.sScenes[0].iDuration - 1500))
		// This is the vector on which we'll move cargobob tracking point
		VECTOR vTrack = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(data.vehActors[PHANTOM_VEH_CARGOBOB], <<0.0, 0.0, 0.0>>) - data.vPool[PHANTOM_VEC_CARGOBOB_TRACKING]
		vTrack.Z = vTrack.Z * 0.25
		// Move the tracking point by small amount and make it dependent on time and distance to cargobob to get nice natural and smooth camera movement
		data.vPool[PHANTOM_VEC_CARGOBOB_TRACKING] = data.vPool[PHANTOM_VEC_CARGOBOB_TRACKING] + vTrack * GET_FRAME_TIME() * (1.0 - FMIN(1.0, VMAG(vTrack) / data.fPool[PHANTOM_FLOAT_MAX_TRACKING_DIST])) * fTimeFactor
		
		CDEBUG1LN(DEBUG_SHOOTRANGE, "data.vPool[PHANTOM_VEC_CARGOBOB_TRACKING]: ", data.vPool[PHANTOM_VEC_CARGOBOB_TRACKING])
		IF cutscene.iCurrentSceneElapsedTime >= 5000
			IF NOT IS_BIT_SET(data.iBSPool[PHANTOM_BS_CUTSCENE], BS_PHANTOM_CUTSCENE_CARGOBOB_LAND) 
				TASK_HELI_MISSION(data.pedActors[PHANTOM_PED_PILOT], data.vehActors[PHANTOM_VEH_CARGOBOB], NULL, NULL, GET_ENTITY_COORDS(data.vehActors[PHANTOM_VEH_CARGOBOB]) - <<0.0, 0.0 ,50.0>>, MISSION_GOTO, 10.0, 1.0, GET_ENTITY_HEADING(data.vehActors[PHANTOM_VEH_CARGOBOB]), 0, 0)
				STOP_CAM_POINTING(cutscene.camera)
				SET_BIT(data.iBSPool[PHANTOM_BS_CUTSCENE], BS_PHANTOM_CUTSCENE_CARGOBOB_LAND)
			ENDIF

			data.vPool[PHANTOM_VEC_CARGOBOB_TRACKING].z =data.fPool[PHANTOM_FLOAT_CARGOBOB_FOLLOW_SPEED]
		ELSE
			data.fPool[PHANTOM_FLOAT_CARGOBOB_FOLLOW_SPEED] = data.vPool[PHANTOM_VEC_CARGOBOB_TRACKING].z
		ENDIF

		POINT_CAM_AT_COORD(cutscene.camera, data.vPool[PHANTOM_VEC_CARGOBOB_TRACKING])
		
		IF NOT IS_BIT_SET(data.iBSPool[PHANTOM_BS_CUTSCENE], BS_PHANTOM_CUTSCENE_CARGOBOB_LAND)
			FLOAT fFovStartPhase = 0.0
			IF fTimeFactor >= fFovStartPhase
				SET_CAM_FOV(cutscene.camera, cutscene.sScenes[0].fCamFovStart - 10.0 * (fTimeFactor - fFovStartPhase) / (1.0 - fFovStartPhase))
			ENDIF
		ENDIF
		
		DRAW_DEBUG_SPHERE(data.vPool[PHANTOM_VEC_CARGOBOB_TRACKING], 0.1, 255, 0, 0, 255)
	ENDIF
	
	IF cutscene.iCurrentScene = 1
	AND NOT SIMPLE_CUTSCENE_IS_SCENE_RUNNING(cutscene, 1)
		STOP_CAM_POINTING(cutscene.camera)
	ENDIF
		
	IF SIMPLE_CUTSCENE_IS_SCENE_RUNNING(cutscene, 1)

		IF IS_ENTITY_VISIBLE(data.vehActors[PHANTOM_VEH_CARGOBOB])
			FREEZE_ENTITY_POSITION(data.vehActors[PHANTOM_VEH_CARGOBOB], TRUE)
			SET_ENTITY_VISIBLE(data.vehActors[PHANTOM_VEH_CARGOBOB], FALSE)
		ENDIF
		
		VECTOR vNewCoord
		vNewCoord = GET_ENTITY_COORDS(data.vehActors[PHANTOM_VEH_CARGOBOB2])
		vNewCoord = vNewCoord + <<0.0, 0.0, 1.0>> * 1.45 * GET_FRAME_TIME()
		SET_ENTITY_COORDS_NO_OFFSET(data.vehActors[PHANTOM_VEH_CARGOBOB2], vNewCoord)
		
		IF NOT IS_BIT_SET(data.iBSPool[PHANTOM_BS_CUTSCENE], BS_PHANTOM_CUTSCENE_CARGOBOB_FROZEN)
			SET_CARGOBOB_FORCE_DONT_DETACH_VEHICLE(data.vehActors[PHANTOM_VEH_CARGOBOB2], TRUE)
			ATTACH_ENTITY_TO_CARGOBOB(data.vehActors[PHANTOM_VEH_CARGOBOB2], data.vehActors[PHANTOM_VEH_TRAILER], 0, <<0.0, 0.0, 3.0>>)
			SET_BIT(data.iBSPool[PHANTOM_BS_CUTSCENE], BS_PHANTOM_CUTSCENE_CARGOBOB_FROZEN)
		ENDIF
//		IF NOT IS_BIT_SET(data.iBSPool[PHANTOM_BS_CUTSCENE], BS_PHANTOM_CUTSCENE_CARGOBOB_FROZEN)
//		AND cutscene.iCurrentSceneElapsedTime > 600
//			SET_CARGOBOB_FORCE_DONT_DETACH_VEHICLE(data.vehActors[PHANTOM_VEH_CARGOBOB2], TRUE)
//			SET_VEHICLE_GRAVITY(data.vehActors[PHANTOM_VEH_CARGOBOB2], FALSE)
//			ATTACH_ENTITY_TO_CARGOBOB(data.vehActors[PHANTOM_VEH_CARGOBOB2], data.vehActors[PHANTOM_VEH_TRAILER], 0, <<0.0, 0.0, 3.0>>)
//			//VECTOR vTruckDir = GET_HEADING_AS_VECTOR(GET_ENTITY_HEADING(data.vehActors[PHANTOM_VEH_TRUCK]))
//			VECTOR vDestCoords = GET_ENTITY_COORDS(data.vehActors[PHANTOM_VEH_CARGOBOB2]) + <<0.0, 0.0, 200.0>> //ROTATE_VECTOR_ABOUT_Z_ORTHO(vTruckDir, ROTSTEP_NEG_90) * 100.0 + data.vBasePos + <<0.0, 0.0, 200.0>>
//			TASK_HELI_MISSION(data.pedActors[PHANTOM_PED_PILOT2], data.vehActors[PHANTOM_VEH_CARGOBOB2], NULL, NULL, vDestCoords, MISSION_GOTO, 3.0, 0.0, GET_ENTITY_HEADING(data.vehActors[PHANTOM_VEH_CARGOBOB2]), 40, 40)
//			
//			
//			
//			SET_BIT(data.iBSPool[PHANTOM_BS_CUTSCENE], BS_PHANTOM_CUTSCENE_CARGOBOB_FROZEN)
//		ENDIF
	ENDIF
ENDPROC

PROC DESTRUCT_PHANTOM_CUTSCENE(SIMPLE_CUTSCENE &cutscene, FREEMODE_DELIVERY_CUTSCENE_DATA &data)
	UNUSED_PARAMETER(cutscene)
	UNUSED_PARAMETER(data)
ENDPROC

#IF IS_DEBUG_BUILD
PROC MAINTAIN_PHANTOM_CUTSCENE_DEBUG(SIMPLE_CUTSCENE &cutscene, FREEMODE_DELIVERY_CUTSCENE_DATA &data)
	UNUSED_PARAMETER(cutscene)
	UNUSED_PARAMETER(data)
ENDPROC
#ENDIF

