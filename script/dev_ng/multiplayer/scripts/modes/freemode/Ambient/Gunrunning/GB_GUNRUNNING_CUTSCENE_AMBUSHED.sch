//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name: gb_gunrunning_cutscene_AMBUSHED.sc																//
// Description: Script defining single cutscene for move weapons sell mission.								//
//				This script always needs to define:															//
//																											//
//				CONSTRUCT_[NAME]_CUTSCENE																	//
//				HAS_[NAME]_CUTSCENE_INITIALISED																//
//				START_[NAME]_CUTSCENE																		//
//				MAINTAIN_[NAME]_CUTSCENE																	//
//				DESTRUCT_[NAME]_CUTSCENE																	//
//				MAINTAIN_[NAME]_CUTSCENE_DEBUG																//
//																											//
// Written by:  Tymon																						//
// Date:  		07/02/2017																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "net_simple_cutscene.sch"

USING "net_simple_cutscene_interior.sch"
USING "net_freemode_delivery_common.sch"

// Vectors
CONST_INT AMBUSHED_VEC_CAM_POINTS_COUNT		18

// Ints
CONST_INT AMBUSHED_INT_SHAPETEST_ID			0

// Floats
CONST_INT AMBUSHED_FLOAT_CAM_DIST				0
CONST_INT AMBUSHED_FLOAT_CAM_HEIGHT			1
CONST_INT AMBUSHED_FLOAT_TARGET_HEADING		2
CONST_INT AMBUSHED_FLOAT_DOOR_OPEN				3

// BS
CONST_INT AMBUSHED_BS_OK_CAM_POINTS				0
CONST_INT AMBUSHED_BS_CUTSCENE						1
CONST_INT BS_AMBUSHED_CUTSCENE_TASKED_LEAVE		0 // Peds were tasked to leave
CONST_INT BS_AMBUSHED_CUTSCENE_TASKED_GOTO			1 // Peds tasked to go to coords while closing the back door of insurgent
CONST_INT BS_AMBUSHED_CUTSCENE_BACK_DOOR_CLOSED	2 // Back door of insurgent closed
CONST_INT BS_AMBUSHED_CUTSCENE_PED_MOVED			3 // Ped moved to the back of insurgent
CONST_INT BS_AMBUSHED_CUTSCENE_GROUND_VEHICLE		4 // Ped moved to the back of insurgent

// Models
CONST_INT AMBUSHED_MODEL_INSURGENT			0
CONST_INT AMBUSHED_MODEL_CRATE				1

// Vehicles
//CONST_INT AMBUSHED_VEH_DELIVERED			0
CONST_INT AMBUSHED_VEH_CLONE				0

// Props
CONST_INT AMBUSHED_PROP_CRATE				0

// Shapetests
CONST_INT AMBUSHED_SHAPETEST_CAMERA		0

FUNC BOOL CONSTRUCT_AMBUSHED_CUTSCENE(SIMPLE_CUTSCENE &cutscene, FREEMODE_DELIVERY_CUTSCENE_DATA &data, ENTITY_INDEX deliveredEntity = NULL)
	UNUSED_PARAMETER(deliveredEntity)
	UNUSED_PARAMETER(cutscene)
	
	data.fPool[AMBUSHED_FLOAT_CAM_HEIGHT] = 2.0
	data.fPool[AMBUSHED_FLOAT_CAM_DIST] = 6.0
	data.fPool[AMBUSHED_FLOAT_DOOR_OPEN] = 0.0
	
	data.models[AMBUSHED_MODEL_INSURGENT] = INSURGENT2
	data.models[AMBUSHED_MODEL_CRATE] = PROP_MIL_CRATE_02
	
	RETURN TRUE
ENDFUNC

FUNC BOOL _HAVE_AMBUSHED_CAMERA_PROBES_FINISHED(SIMPLE_CUTSCENE &cutscene, FREEMODE_DELIVERY_CUTSCENE_DATA &data)
	UNUSED_PARAMETER(cutscene)
	
	IF data.iPool[AMBUSHED_INT_SHAPETEST_ID] = AMBUSHED_VEC_CAM_POINTS_COUNT
		// We're good when we finished shapetest for all the points
		#IF IS_DEBUG_BUILD
		IF data.bCamProbing
			data.iPool[AMBUSHED_INT_SHAPETEST_ID] = 0
			
			INT iPool
			REPEAT AMBUSHED_VEC_CAM_POINTS_COUNT iPool
				data.vPool[iPool] = <<0.0, 0.0, 0.0>>
			ENDREPEAT
		ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF

	VECTOR vBasePos = data.vBasePos
	
	IF NATIVE_TO_INT(data.shapetests[AMBUSHED_SHAPETEST_CAMERA]) = 0
		IF IS_VECTOR_ZERO(data.vPool[data.iPool[AMBUSHED_INT_SHAPETEST_ID]])
			FLOAT fAngleStep = 360.0 / AMBUSHED_VEC_CAM_POINTS_COUNT
			data.vPool[data.iPool[AMBUSHED_INT_SHAPETEST_ID]].X = vBasePos.X + COS(fAngleStep * data.iPool[AMBUSHED_INT_SHAPETEST_ID]) * data.fPool[AMBUSHED_FLOAT_CAM_DIST]
			data.vPool[data.iPool[AMBUSHED_INT_SHAPETEST_ID]].Y = vBasePos.Y + SIN(fAngleStep * data.iPool[AMBUSHED_INT_SHAPETEST_ID]) * data.fPool[AMBUSHED_FLOAT_CAM_DIST]
			data.vPool[data.iPool[AMBUSHED_INT_SHAPETEST_ID]].Z = vBasePos.Z + 40.0 // gotta be above the ground or else get_ground fails
			
			FLOAT fGroundZ
			
			IF GET_GROUND_Z_FOR_3D_COORD(data.vPool[data.iPool[AMBUSHED_INT_SHAPETEST_ID]], fGroundZ)
				data.vPool[data.iPool[AMBUSHED_INT_SHAPETEST_ID]].Z = fGroundZ + data.fPool[AMBUSHED_FLOAT_CAM_HEIGHT]
			ELSE
				// Skip that point
				data.iPool[AMBUSHED_INT_SHAPETEST_ID] += 1
				CDEBUG1LN(DEBUG_SHOOTRANGE, "[GUNRUNNING_CUTSCENE] _HAVE_AMBUSHED_CAMERA_PROBES_FINISHED - Couldn't get ground Z for current point! Skipping point.")
				RETURN FALSE
			ENDIF
			
			// Skip point if the difference between original location and new ground Z is too big (this eliminates points that end up on top of buildings)
			IF ABSF(data.vPool[data.iPool[AMBUSHED_INT_SHAPETEST_ID]].Z - vBasePos.Z) > 4.0
				data.iPool[AMBUSHED_INT_SHAPETEST_ID] += 1
				CDEBUG1LN(DEBUG_SHOOTRANGE, "[GUNRUNNING_CUTSCENE] _HAVE_AMBUSHED_CAMERA_PROBES_FINISHED - Vertical distance between point and the base is too much! Skipping point.")
				RETURN FALSE
			ENDIF
			
			CDEBUG1LN(DEBUG_SHOOTRANGE, "[GUNRUNNING_CUTSCENE] _HAVE_AMBUSHED_CAMERA_PROBES_FINISHED - Got new point number ", data.iPool[AMBUSHED_INT_SHAPETEST_ID], ": ", data.vPool[data.iPool[AMBUSHED_INT_SHAPETEST_ID]])
		ENDIF
		
		VECTOR fShapetestFrom = data.vPool[data.iPool[AMBUSHED_INT_SHAPETEST_ID]]
		VECTOR fShapetestTo = vBasePos + <<0.0, 0.0, 0.3>>

		data.shapetests[AMBUSHED_SHAPETEST_CAMERA] = START_SHAPE_TEST_LOS_PROBE(fShapetestFrom, fShapetestTo, SCRIPT_INCLUDE_ALL)
		DRAW_DEBUG_LINE(fShapetestFrom, fShapetestTo)
		IF NATIVE_TO_INT(data.shapetests[AMBUSHED_SHAPETEST_CAMERA]) != 0
			CDEBUG1LN(DEBUG_SHOOTRANGE, "[GUNRUNNING_CUTSCENE] _HAVE_AMBUSHED_CAMERA_PROBES_FINISHED - Started shapetest from ", fShapetestFrom, " to ", fShapetestTo)
		ELSE
			CDEBUG1LN(DEBUG_SHOOTRANGE, "[GUNRUNNING_CUTSCENE] _HAVE_AMBUSHED_CAMERA_PROBES_FINISHED - Something went wrong when starting the shapetest, will try again next frame.")
		ENDIF
	ELSE
		INT iHitSomething
		VECTOR vHitPos, vHitNormal
		ENTITY_INDEX entityHit
		
		SHAPETEST_STATUS status = GET_SHAPE_TEST_RESULT(data.shapetests[AMBUSHED_SHAPETEST_CAMERA], iHitSomething, vHitPos, vHitNormal, entityHit)
		IF status = SHAPETEST_STATUS_RESULTS_READY
			IF iHitSomething = 0
				// We didn't hit anything so the point is good!
				SET_BIT(data.iBSPool[AMBUSHED_BS_OK_CAM_POINTS], data.iPool[AMBUSHED_INT_SHAPETEST_ID])
				CDEBUG1LN(DEBUG_SHOOTRANGE, "[GUNRUNNING_CUTSCENE] _HAVE_AMBUSHED_CAMERA_PROBES_FINISHED - Found a good point for camera start: ", data.iPool[AMBUSHED_INT_SHAPETEST_ID])
			ELSE
				DRAW_DEBUG_SPHERE(vHitPos, 0.1, 125, 125, 0)
			ENDIF
			
			data.iPool[AMBUSHED_INT_SHAPETEST_ID] += 1
			data.shapetests[AMBUSHED_SHAPETEST_CAMERA] = INT_TO_NATIVE(SHAPETEST_INDEX, 0)
			
		ELIF status = SHAPETEST_STATUS_NONEXISTENT
			data.shapetests[AMBUSHED_SHAPETEST_CAMERA] = INT_TO_NATIVE(SHAPETEST_INDEX, 0)
			CDEBUG1LN(DEBUG_SHOOTRANGE, "[GUNRUNNING_CUTSCENE] _HAVE_AMBUSHED_CAMERA_PROBES_FINISHED - Shapetest result returned non-existent, something went wrong, resetting.")
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_AMBUSHED_CUTSCENE_INITIALISED(SIMPLE_CUTSCENE &cutscene, FREEMODE_DELIVERY_CUTSCENE_DATA &data)
	UNUSED_PARAMETER(cutscene)
	UNUSED_PARAMETER(data)
//	IF _HAVE_AMBUSHED_CAMERA_PROBES_FINISHED(cutscene, data)
//			CDEBUG1LN(DEBUG_SHOOTRANGE, "[GUNRUNNING_CUTSCENE] _HAVE_AMBUSHED_CAMERA_PROBES_FINISHED - TRUE")
//		RETURN TRUE
//	ENDIF
	data.bFailedToFindValidCamPos = FALSE
	RETURN TRUE
ENDFUNC

PROC GUNRUN_GET_AMBUSHED_BACKUP_DELIVERY_CAM_DETAILS(FREEMODE_DELIVERY_DROPOFFS eDropoffID, VECTOR &vCamPosition, VECTOR &vCamRotation, FLOAT &fCamFOV)
	SWITCH eDropoffID
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_1 // 71
			vCamPosition = <<-240.6769, 2091.1418, 149.0416>>
			vCamRotation = <<-16.8195, -0.0000, -148.7795>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_2
			vCamPosition = <<2744.2717, 2848.4612, 48.6057>>
			vCamRotation = <<-20.6121, -0.0000, -94.4168>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_3
			vCamPosition = <<3010.8225, 3487.0269, 82.9643>>
			vCamRotation = <<-13.8797, -0.0000, 121.4941>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_4
			vCamPosition = <<-2689.4912, 3469.6660, 28.9000>>
			vCamRotation = <<-18.3763, -0.0000, -113.0179>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_5
			vCamPosition = <<1474.6542, 3575.4480, 43.0862>>
			vCamRotation = <<-11.6285, -0.0000, -72.3609>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_6
			vCamPosition = <<-2407.2778, 4267.5542, 18.1345>>
			vCamRotation = <<-3.5704, -0.0000, -59.8728>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_7
			vCamPosition = <<2486.1841, 4319.1792, 50.5665>>
			vCamRotation = <<-11.5855, -0.0000, -141.3174>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_8
			vCamPosition = <<-1840.3151, 4395.2861, 60.4985>>
			vCamRotation = <<-12.9521, -0.0000, 4.2585>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_9
			vCamPosition = <<-1553.7913, 4511.6431, 30.3840>>
			vCamRotation = <<-9.6026, -0.0000, 161.3975>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_10//80
			vCamPosition = <<-422.6879, 6384.7979, 24.4980>>
			vCamRotation = <<-18.8113, -0.0000, -113.7943>>
			fCamFOV = 50.0
		BREAK		
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_1 // 81
			vCamPosition = <<1174.9204, -3225.9519, 14.6373>>
			vCamRotation = <<-18.8228, -0.0000, -139.8902>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_2
			vCamPosition = <<199.9098, -3106.5261, 17.8524>>
			vCamRotation = <<-25.3981, -0.0000, -150.1158>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_3
			vCamPosition = <<290.8995, -2870.5347, 17.6846>>
			vCamRotation = <<-33.5881, 0.0000, -57.4045>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_4
			vCamPosition = <<1654.1420, -2368.0044, 104.5084>>
			vCamRotation = <<-24.8413, -0.0000, 96.3044>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_5
			vCamPosition = <<868.8862, -2458.9985, 38.3830>>
			vCamRotation = <<-13.7167, 0.0000, 43.0259>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_6
			vCamPosition = <<-1205.8557, -2389.3716, 24.8211>>
			vCamRotation = <<-21.0412, -0.0000, 103.3599>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_7
			vCamPosition = <<920.9332, -1751.7904, 37.9878>>
			vCamRotation = <<-13.5384, -0.0000, 39.2778>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_8
			vCamPosition = <<1751.1338, -1553.6910, 124.7459>>
			vCamRotation = <<-17.4720, -0.0000, 40.2432>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_9
			vCamPosition = <<502.0954, -1412.3597, 37.8341>>
			vCamRotation = <<-20.9308, -0.0000, 40.9399>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_10 //90
			vCamPosition = <<-133.8458, -1363.0621, 38.3665>>
			vCamRotation = <<-17.6259, 0.0000, 50.5273>>
			fCamFOV = 50.0
		BREAK
		
		DEFAULT
			CASSERTLN(DEBUG_AMBIENT, "GUNRUN_GET_FOLLOW_LEADER_BACKUP_DELIVERY_CAM_DETAILS passed non follow the leader mission dropoff!")
		
	ENDSWITCH
	
	CDEBUG1LN(DEBUG_SHOOTRANGE, "[GUNRUNNING_CUTSCENE] GUNRUN_GET_FOLLOW_LEADER_BACKUP_DELIVERY_CAM_DETAILS - Returning position: ", vCamPosition, " rotation: ", vCamRotation, " FOV: ", fCamFOV)
ENDPROC

PROC _CREATE_AMBUSHED_SCENES(SIMPLE_CUTSCENE &cutscene, FREEMODE_DELIVERY_CUTSCENE_DATA &data)
	FLOAT fTargetHeading = data.fBaseHeading + 90.0 //data.fPool[AMBUSHED_FLOAT_TARGET_HEADING]
	FLOAT fAngleStep = 360.0 / AMBUSHED_VEC_CAM_POINTS_COUNT
	FLOAT fCamFOVStart = 60.0
	INT iClosestAngleID = FLOOR(fTargetHeading / fAngleStep)
	
	CDEBUG1LN(DEBUG_SHOOTRANGE, "[GUNRUNNING_CUTSCENE] _CREATE_AMBUSHED_SCENES - Got target camera heading ", fTargetHeading)
	CDEBUG1LN(DEBUG_SHOOTRANGE, "[GUNRUNNING_CUTSCENE] _CREATE_AMBUSHED_SCENES - Closest to target point is ", iClosestAngleID)
	
	INT iCounter
	INT iPointToUse = -1
	
	// Go both ways and find the starting camera point that matches target heading
	WHILE iCounter < AMBUSHED_VEC_CAM_POINTS_COUNT
		IF iClosestAngleID + iCounter < AMBUSHED_VEC_CAM_POINTS_COUNT
			IF IS_BIT_SET(data.iBSPool[AMBUSHED_BS_OK_CAM_POINTS], iClosestAngleID + iCounter)
				iPointToUse = iClosestAngleID + iCounter
				BREAKLOOP
			ENDIF
		ENDIF
		
		IF iCounter > 0 AND iClosestAngleID - iCounter >= 0
			IF IS_BIT_SET(data.iBSPool[AMBUSHED_BS_OK_CAM_POINTS], iClosestAngleID - iCounter)
				iPointToUse = iClosestAngleID - iCounter
				BREAKLOOP
			ENDIF
		ENDIF
			
		iCounter += 1
	ENDWHILE
	
	IF iPointToUse = -1
	#IF IS_DEBUG_BUILD
	OR data.bForceUseBackupCam
	#ENDIF
		
		CDEBUG1LN(DEBUG_SHOOTRANGE, "[GUNRUNNING_CUTSCENE] _CREATE_AMBUSHED_SCENES - Coulnd't find any good point to put the camera in! Using backup camera")
		//data.bFailedToFindValidCamPos = TRUE
		iPointToUse = 0
	ELSE
		CDEBUG1LN(DEBUG_SHOOTRANGE, "[GUNRUNNING_CUTSCENE] _CREATE_AMBUSHED_SCENES - Found the closest point to the desired heading, it's ", iPointToUse)
	ENDIF
	
	VECTOR vCutscenePos = data.vBasePos
	//FLOAT fCutsceneHeading = data.fBaseHeading
	//VECTOR vCutsceneDir = GET_HEADING_AS_VECTOR(fCutsceneHeading)
	
	VECTOR vCamStartPos = data.vPool[iPointToUse]
	
	//SIMPLE_CUTSCENE_CREATE(cutscene, "AMBUSHED")
	
	VECTOR vLookAtDir = (vCutscenePos + <<0.0, 0.0, 1.5>>) - vCamStartPos
	vLookAtDir = NORMALISE_VECTOR(vLookAtDir)
	
	VECTOR vCamRot = _GET_CAM_ROT_FROM_DIR(vLookAtDir)
	
	IF data.bFailedToFindValidCamPos
		//GUNRUN_GET_FOLLOW_LEADER_BACKUP_DELIVERY_CAM_DETAILS(data.eDropoffID, vCamStartPos, vCamRot, fCamFOVStart)
		//data.bFailedToFindValidCamPos = FALSE
	ENDIF
	
	SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 4500, "scene1", vCamStartPos, vCamRot, fCamFOVStart, vCamStartPos + <<0.0, 0.0, 0.0>>, vCamRot, fCamFOVStart, 0.2)
ENDPROC

PROC _CREATE_AMBUSHED_ASSETS(SIMPLE_CUTSCENE &cutscene, FREEMODE_DELIVERY_CUTSCENE_DATA &data)
	UNUSED_PARAMETER(cutscene)
	UNUSED_PARAMETER(data)
ENDPROC

PROC START_AMBUSHED_CUTSCENE(SIMPLE_CUTSCENE &cutscene, FREEMODE_DELIVERY_CUTSCENE_DATA &data)
	_CREATE_AMBUSHED_SCENES(cutscene, data)
	
	IF data.bFailedToFindValidCamPos
		CDEBUG1LN(DEBUG_SHOOTRANGE, "[GUNRUNNING_CUTSCENE] _HAVE_AMBUSHED_CAMERA_PROBES_FINISHED - NO VALID CAM POINTS")
		EXIT
	ENDIF
	
	_CREATE_AMBUSHED_ASSETS(cutscene, data)
	
	// Compute camera position for last shot showing the camera leaving insurgent from the back door
	//Create scene cams before cutscene starts.
	#IF IS_DEBUG_BUILD
	VECTOR vCoord
	vCoord = GET_ENTITY_COORDS(data.vehActors[AMBUSHED_VEH_CLONE])
	CDEBUG1LN(DEBUG_SHOOTRANGE,  "MAINTAIN_AMBUSHED_CUTSCENE - Camera loaded: SceneVehCoords: ",  vCoord)
	#ENDIF
	
//	VECTOR vCamDir
//	vCamDir = GET_ENTITY_FORWARD_VECTOR(data.vehActors[AMBUSHED_VEH_CLONE])
//	ROTATE_VECTOR_FMMC(vCamDir, <<0.0, 70.0, 0.0>>)
//	
//	VECTOR vVehCoord
//	vVehCoord = GET_ENTITY_COORDS(data.vehActors[AMBUSHED_VEH_CLONE])
//	
//	cutscene.sScenes[0].vCamCoordsStart = vVehCoord + vCamDir * 4.5
//	cutscene.sScenes[0].vCamCoordsFinish = vVehCoord + vCamDir * 5.5
//	
//	cutscene.sScenes[0].vCamRotStart = _GET_CAM_ROT_FROM_DIR(NORMALISE_VECTOR(vVehCoord - cutscene.sScenes[0].vCamCoordsStart))
//	cutscene.sScenes[0].vCamRotFinish = cutscene.sScenes[0].vCamRotStart
	
	
	cutscene.sScenes[0].vCamCoordsStart = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(data.vehActors[FOLLOW_THE_LEADER_VEH_CLONE], <<-1.000, 5.0, 3.0>>)
	cutscene.sScenes[0].vCamCoordsFinish = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(data.vehActors[FOLLOW_THE_LEADER_VEH_CLONE], <<-1.000, 5.5, 3.5>>)
	
	VECTOR vCamLookAt = NORMALISE_VECTOR(GET_ENTITY_COORDS(data.vehActors[FOLLOW_THE_LEADER_VEH_CLONE]) - cutscene.sScenes[0].vCamCoordsStart)
	
	cutscene.sScenes[0].vCamRotStart = _GET_CAM_ROT_FROM_DIR(vCamLookAt)
	//cutscene.sScenes[0].vCamRotStart.Y = -GET_ENTITY_ROLL(data.vehActors[FOLLOW_THE_LEADER_VEH_CLONE])
	cutscene.sScenes[0].vCamRotFinish = cutscene.sScenes[0].vCamRotStart
ENDPROC

PROC MAINTAIN_AMBUSHED_CUTSCENE(SIMPLE_CUTSCENE &cutscene, FREEMODE_DELIVERY_CUTSCENE_DATA &data)
	IF NOT DOES_ENTITY_EXIST(data.vehActors[AMBUSHED_VEH_CLONE])
	OR IS_ENTITY_DEAD(data.vehActors[AMBUSHED_VEH_CLONE])
	OR NOT DOES_ENTITY_EXIST(data.pedPlayerActors[0])
	OR IS_ENTITY_DEAD(data.pedPlayerActors[0])
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(data.iBSPool[AMBUSHED_BS_CUTSCENE], BS_AMBUSHED_CUTSCENE_TASKED_LEAVE)
		IF cutscene.iTotalElapsedTime > 500
			TASK_LEAVE_ANY_VEHICLE(data.pedPlayerActors[0], 0)
			SET_BIT(data.iBSPool[AMBUSHED_BS_CUTSCENE], BS_AMBUSHED_CUTSCENE_TASKED_LEAVE)
		ENDIF
	ENDIF
	
	IF SIMPLE_CUTSCENE_IS_SCENE_RUNNING(cutscene, 1)

	ENDIF
ENDPROC

PROC DESTRUCT_AMBUSHED_CUTSCENE(SIMPLE_CUTSCENE &cutscene, FREEMODE_DELIVERY_CUTSCENE_DATA &data)
	UNUSED_PARAMETER(cutscene)
	UNUSED_PARAMETER(data)
ENDPROC

#IF IS_DEBUG_BUILD
PROC MAINTAIN_AMBUSHED_CUTSCENE_DEBUG(SIMPLE_CUTSCENE &cutscene, FREEMODE_DELIVERY_CUTSCENE_DATA &data)
	UNUSED_PARAMETER(cutscene)
	UNUSED_PARAMETER(data)
	
	INT i
	REPEAT AMBUSHED_VEC_CAM_POINTS_COUNT i
		IF IS_BIT_SET(data.iBSPool[AMBUSHED_BS_OK_CAM_POINTS], i)
			DRAW_DEBUG_SPHERE(data.vPool[i], 0.1, 0, 255, 0)
		ELSE
			DRAW_DEBUG_SPHERE(data.vPool[i], 0.1, 255, 0, 0)
		ENDIF
	ENDREPEAT
ENDPROC
#ENDIF

