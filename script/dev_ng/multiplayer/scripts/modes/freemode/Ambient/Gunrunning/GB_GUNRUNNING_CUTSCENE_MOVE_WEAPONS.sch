//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name: gb_gunrunning_cutscene_move_weapons.sc																//
// Description: Script defining single cutscene for move weapons sell mission.								//
//				This script always needs to define:															//
//																											//
//				CONSTRUCT_[NAME]_CUTSCENE																	//
//				HAS_[NAME]_CUTSCENE_INITIALISED																//
//				START_[NAME]_CUTSCENE																		//
//				MAINTAIN_[NAME]_CUTSCENE																	//
//				DESTRUCT_[NAME]_CUTSCENE																	//
//				MAINTAIN_[NAME]_CUTSCENE_DEBUG																//
//																											//
// Written by:  Tymon																						//
// Date:  		07/02/2017																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "net_simple_cutscene.sch"


USING "net_freemode_delivery_common.sch"
USING "net_simple_cutscene_interior.sch"
// Vectors
CONST_INT MOVE_WEAPONS_VEC_CAM_POINTS_COUNT		18

// Ints
CONST_INT MOVE_WEAPONS_INT_SHAPETEST_ID			0

// Floats
CONST_INT MOVE_WEAPONS_FLOAT_CAM_DIST			0
CONST_INT MOVE_WEAPONS_FLOAT_CAM_HEIGHT			1
CONST_INT MOVE_WEAPONS_FLOAT_TARGET_HEADING		2

// BS
CONST_INT MOVE_WEAPONS_BS_OK_CAM_POINTS					0
CONST_INT MOVE_WEAPONS_BS_CUTSCENE						1
	CONST_INT BS_MOVE_WEAPONS_CUTSCENE_TASKED_LEAVE			0 // Peds were tasked to leave
	CONST_INT BS_MOVE_WEAPONS_CUTSCENE_TASKED_GOTO			1 // Peds tasked to go to coords while closing the back door of insurgent
	CONST_INT BS_MOVE_WEAPONS_CUTSCENE_BACK_DOOR_CLOSED		2 // Back door of insurgent closed
	CONST_INT BS_MOVE_WEAPONS_CUTSCENE_PED_MOVED			3 // Ped moved to the back of insurgent

// Models
CONST_INT MOVE_WEAPONS_MODEL_INSURGENT			0
CONST_INT MOVE_WEAPONS_MODEL_CRATE				1

// Vehicles
//CONST_INT MOVE_WEAPONS_VEH_DELIVERED			0
CONST_INT MOVE_WEAPONS_VEH_CLONE				0

// Props
CONST_INT MOVE_WEAPONS_PROP_CRATE				0

// Shapetests
CONST_INT MOVE_WEAPON_SHAPETEST_CAMERA			0

FUNC BOOL CONSTRUCT_MOVE_WEAPONS_CUTSCENE(SIMPLE_CUTSCENE &cutscene, FREEMODE_DELIVERY_CUTSCENE_DATA &data, ENTITY_INDEX deliveredEntity = NULL)
	UNUSED_PARAMETER(deliveredEntity)
	UNUSED_PARAMETER(cutscene)
	
	data.fPool[MOVE_WEAPONS_FLOAT_CAM_HEIGHT] = 2.0
	data.fPool[MOVE_WEAPONS_FLOAT_CAM_DIST] = 6.0
	
	data.models[MOVE_WEAPONS_MODEL_INSURGENT] = INSURGENT2
	data.models[MOVE_WEAPONS_MODEL_CRATE] = PROP_MIL_CRATE_02
	
	RETURN TRUE
ENDFUNC

FUNC BOOL _HAVE_MOVE_WEAPONS_CAMERA_PROBES_FINISHED(SIMPLE_CUTSCENE &cutscene, FREEMODE_DELIVERY_CUTSCENE_DATA &data)
	UNUSED_PARAMETER(cutscene)
	
	IF data.iPool[MOVE_WEAPONS_INT_SHAPETEST_ID] = MOVE_WEAPONS_VEC_CAM_POINTS_COUNT
		// We're good when we finished shapetest for all the points
		RETURN TRUE
	ENDIF
	
	VECTOR vBasePos = data.vBasePos
	
	IF NATIVE_TO_INT(data.shapetests[MOVE_WEAPON_SHAPETEST_CAMERA]) = 0
		IF IS_VECTOR_ZERO(data.vPool[data.iPool[MOVE_WEAPONS_INT_SHAPETEST_ID]])
			FLOAT fAngleStep = 360.0 / MOVE_WEAPONS_VEC_CAM_POINTS_COUNT
			data.vPool[data.iPool[MOVE_WEAPONS_INT_SHAPETEST_ID]].X = vBasePos.X + COS(fAngleStep * data.iPool[MOVE_WEAPONS_INT_SHAPETEST_ID]) * data.fPool[MOVE_WEAPONS_FLOAT_CAM_DIST]
			data.vPool[data.iPool[MOVE_WEAPONS_INT_SHAPETEST_ID]].Y = vBasePos.Y + SIN(fAngleStep * data.iPool[MOVE_WEAPONS_INT_SHAPETEST_ID]) * data.fPool[MOVE_WEAPONS_FLOAT_CAM_DIST]
			data.vPool[data.iPool[MOVE_WEAPONS_INT_SHAPETEST_ID]].Z = vBasePos.Z + 40.0 // gotta be above the ground or else get_ground fails
			
			FLOAT fGroundZ
			
			IF GET_GROUND_Z_FOR_3D_COORD(data.vPool[data.iPool[MOVE_WEAPONS_INT_SHAPETEST_ID]], fGroundZ)
				data.vPool[data.iPool[MOVE_WEAPONS_INT_SHAPETEST_ID]].Z = fGroundZ + data.fPool[MOVE_WEAPONS_FLOAT_CAM_HEIGHT]
			ELSE
				// Skip that point
				data.iPool[MOVE_WEAPONS_INT_SHAPETEST_ID] += 1
				PRINTLN("[GUNRUNNING_CUTSCENE] _HAVE_MOVE_WEAPONS_CAMERA_PROBES_FINISHED - Couldn't get ground Z for current point! Skipping point.")
				RETURN FALSE
			ENDIF
			
			// Skip point if the difference between original location and new ground Z is too big (this eliminates points that end up on top of buildings)
			IF ABSF(data.vPool[data.iPool[MOVE_WEAPONS_INT_SHAPETEST_ID]].Z - vBasePos.Z) > 4.0
				data.iPool[MOVE_WEAPONS_INT_SHAPETEST_ID] += 1
				PRINTLN("[GUNRUNNING_CUTSCENE] _HAVE_MOVE_WEAPONS_CAMERA_PROBES_FINISHED - Vertical distance between point and the base is too much! Skipping point.")
				RETURN FALSE
			ENDIF
			
			PRINTLN("[GUNRUNNING_CUTSCENE] _HAVE_MOVE_WEAPONS_CAMERA_PROBES_FINISHED - Got new point number ", data.iPool[MOVE_WEAPONS_INT_SHAPETEST_ID], ": ", data.vPool[data.iPool[MOVE_WEAPONS_INT_SHAPETEST_ID]])
		ENDIF
		
		VECTOR fShapetestFrom = data.vPool[data.iPool[MOVE_WEAPONS_INT_SHAPETEST_ID]]
		VECTOR fShapetestTo = vBasePos + <<0.0, 0.0, 0.1>>
		
		data.shapetests[MOVE_WEAPON_SHAPETEST_CAMERA] = START_SHAPE_TEST_LOS_PROBE(fShapetestFrom, fShapetestTo, SCRIPT_INCLUDE_ALL)
		
		IF NATIVE_TO_INT(data.shapetests[MOVE_WEAPON_SHAPETEST_CAMERA]) != 0
			PRINTLN("[GUNRUNNING_CUTSCENE] _HAVE_MOVE_WEAPONS_CAMERA_PROBES_FINISHED - Started shapetest from ", fShapetestFrom, " to ", fShapetestTo)
		ELSE
			PRINTLN("[GUNRUNNING_CUTSCENE] _HAVE_MOVE_WEAPONS_CAMERA_PROBES_FINISHED - Something went wrong when starting the shapetest, will try again next frame.")
		ENDIF
	ELSE
		INT iHitSomething
		VECTOR vHitPos, vHitNormal
		ENTITY_INDEX entityHit
		
		SHAPETEST_STATUS status = GET_SHAPE_TEST_RESULT(data.shapetests[MOVE_WEAPON_SHAPETEST_CAMERA], iHitSomething, vHitPos, vHitNormal, entityHit)
		
		IF status = SHAPETEST_STATUS_RESULTS_READY
			IF iHitSomething = 0
				// We didn't hit anything so the point is good!
				SET_BIT(data.iBSPool[MOVE_WEAPONS_BS_OK_CAM_POINTS], data.iPool[MOVE_WEAPONS_INT_SHAPETEST_ID])
				PRINTLN("[GUNRUNNING_CUTSCENE] _HAVE_MOVE_WEAPONS_CAMERA_PROBES_FINISHED - Found a good point for camera start: ", data.iPool[MOVE_WEAPONS_INT_SHAPETEST_ID])
			ENDIF
			
			data.iPool[MOVE_WEAPONS_INT_SHAPETEST_ID] += 1
			data.shapetests[MOVE_WEAPON_SHAPETEST_CAMERA] = INT_TO_NATIVE(SHAPETEST_INDEX, 0)
			
		ELIF status = SHAPETEST_STATUS_NONEXISTENT
			data.shapetests[MOVE_WEAPON_SHAPETEST_CAMERA] = INT_TO_NATIVE(SHAPETEST_INDEX, 0)
			PRINTLN("[GUNRUNNING_CUTSCENE] _HAVE_MOVE_WEAPONS_CAMERA_PROBES_FINISHED - Shapetest result returned non-existent, something went wrong, resetting.")
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/*
FUNC BOOL _HAVE_MOVE_WEAPONS_INSURGENT_BOUND_TEST_FINISHED(SIMPLE_CUTSCENE &cutscene, FREEMODE_DELIVERY_CUTSCENE_DATA &data)
	VECTOR vModelMin, vModelMax
	GET_MODEL_DIMENSIONS(data.models[MOVE_WEAPONS_MODEL_INSURGENT], vModelMin, vModelMax)
	vModelMax = vModelMax + <<0.0, 0.0, 1.5>> // Extension at the back to have the car driving in
	
	VECTOR vBasePos = data.vBasePos
	
	
	//GET_VEHICLE_
	START_SHAPE_TEST_BOX
ENDFUNC
*/

FUNC BOOL HAS_MOVE_WEAPONS_CUTSCENE_INITIALISED(SIMPLE_CUTSCENE &cutscene, FREEMODE_DELIVERY_CUTSCENE_DATA &data)
	IF _HAVE_MOVE_WEAPONS_CAMERA_PROBES_FINISHED(cutscene, data)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC GUNRUN_GET_MOVE_WEAPONS_BACKUP_DELIVERY_CAM_DETAILS(FREEMODE_DELIVERY_DROPOFFS eDropoffID, VECTOR &vCamPosition, VECTOR &vCamRotation, FLOAT &fCamFOV)
	SWITCH eDropoffID
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_1 // 11
			vCamPosition = <<36.5735, 6569.5371, 42.5394>>
			vCamRotation = <<-27.5351, 0.0000, 79.8661>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_2
			vCamPosition = <<391.5988, 6634.0352, 37.7136>>
			vCamRotation = <<-11.5787, -0.0000, -140.4141>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_3
			vCamPosition = <<14.1324, 6532.0190, 36.5357>>
			vCamRotation = <<-17.9602, -0.0000, -173.6631>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_4
			vCamPosition = <<-186.9730, 6459.9214, 35.8558>>
			vCamRotation = <<-14.7841, 0.0000, 92.6616>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_5
			vCamPosition = <<-227.0445, 6421.6177, 39.5059>>
			vCamRotation = <<-21.4372, -0.0000, 2.8946>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_6
			vCamPosition = <<-147.9945, 6327.4141, 37.3003>>
			vCamRotation = <<-9.1472, -0.0000, -67.4204>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_7
			vCamPosition = <<-353.9754, 6307.7661, 38.3755>>
			vCamRotation = <<-5.8748, 0.0000, -0.0867>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_8
			vCamPosition = <<-321.1798, 6331.2935, 38.0054>>
			vCamRotation = <<-9.9697, -0.0000, -176.8878>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_9
			vCamPosition = <<-419.0103, 6267.8086, 38.0995>>
			vCamRotation = <<-19.7330, -0.0000, 112.2946>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_10
			vCamPosition = <<-238.5225, 6220.9175, 42.9345>>
			vCamRotation = <<-17.1616, -0.0000, 101.1774>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_11
			vCamPosition = <<-261.8289, 6176.5835, 39.8118>>
			vCamRotation = <<-21.5566, -0.0000, 87.3085>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_12
			vCamPosition = <<-429.9159, 6120.5913, 38.0592>>
			vCamRotation = <<-8.0689, -0.0000, 1.2842>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_13
			vCamPosition = <<2238.2500, 5192.8799, 68.9847>>
			vCamRotation = <<-17.1347, -0.0000, -177.6114>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_14
			vCamPosition = <<2012.8134, 4991.0952, 50.3863>>
			vCamRotation = <<-12.5201, -0.0000, -164.8657>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_15
			vCamPosition = <<2328.4185, 4774.3115, 46.8227>>
			vCamRotation = <<-8.5230, 0.0000, 112.6437>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_16
			vCamPosition = <<-1325.5497, 4840.7090, 151.8916>>
			vCamRotation = <<-20.5373, -0.0000, 100.5541>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_17
			vCamPosition = <<1683.4298, 4639.3247, 51.3936>>
			vCamRotation = <<-26.0178, -0.0000, 32.7382>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_18
			vCamPosition = <<1697.9600, 4754.1479, 49.5384>>
			vCamRotation = <<-15.6878, -0.0000, -134.2903>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_19
			vCamPosition = <<2955.3367, 4480.4546, 56.8123>>
			vCamRotation = <<-14.7383, -0.0000, 85.3989>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_20
			vCamPosition = <<1895.5820, 3748.6909, 41.0289>>
			vCamRotation = <<-13.3639, -0.0000, 65.1558>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_21
			vCamPosition = <<1891.5513, 3694.4092, 39.9596>>
			vCamRotation = <<-12.3222, 0.0000, 3.3238>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_22
			vCamPosition = <<940.1895, 3657.3345, 40.7142>>
			vCamRotation = <<-12.5643, -0.0000, 134.9185>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_23
			vCamPosition = <<1378.8119, 3610.5881, 41.3637>>
			vCamRotation = <<-6.7997, 0.0000, 139.5238>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_24
			vCamPosition = <<408.8880, 3545.9143, 43.4946>>
			vCamRotation = <<-10.4153, -0.0000, -65.2960>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_25
			vCamPosition = <<-2258.5166, 3460.0271, 38.7876>>
			vCamRotation = <<-9.0854, -0.0000, -74.9960>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_26
			vCamPosition = <<1226.5999, 2725.5920, 48.6466>>
			vCamRotation = <<-13.3576, -0.0000, 128.5186>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_27
			vCamPosition = <<1019.2131, 2646.1067, 51.6826>>
			vCamRotation = <<-11.8433, 0.0000, 51.7107>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_28
			vCamPosition = <<2872.0234, 1452.8049, 34.4343>>
			vCamRotation = <<-16.0402, 0.0000, 40.4119>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_29
			vCamPosition = <<1768.2223, 3681.3582, 46.5404>>
			vCamRotation = <<-11.9767, -0.0000, 170.4576>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_30 // 40
			vCamPosition = <<-261.3971, 6269.9038, 41.2678>>
			vCamRotation = <<-18.1448, -0.0000, -159.7883>>
			fCamFOV = 50.0
		BREAK		
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_1 // 41
			vCamPosition = <<-1983.1538, 461.9846, 110.4829>>
			vCamRotation = <<-15.9895, 4.5633, 132.9430>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_2
			vCamPosition = <<-203.2005, 333.1274, 108.3379>>
			vCamRotation = <<-15.3950, -0.0000, -136.7291>>
			fCamFOV = 45.0000
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_3
			vCamPosition = <<-128.0132, 209.1882, 104.1048>>
			vCamRotation = <<-25.6896, -0.0000, 129.0972>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_4
			vCamPosition = <<-1924.5431, 200.6054, 92.7748>>
			vCamRotation =  <<-15.9948, -0.0000, 160.5087>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_5
			vCamPosition = <<326.4579, 42.3446, 101.0316>>
			vCamRotation = <<-23.9594, -0.0000, -146.6969>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_6
			vCamPosition = <<211.6117, -156.0833, 66.6939>>
			vCamRotation = <<-23.5380, -0.0000, 108.3673>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_7
			vCamPosition = <<-1498.7937, -180.7409, 66.2593>>
			vCamRotation = <<-25.0164, 0.0000, -177.6287>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_8
			vCamPosition = <<939.9900, -268.3495, 76.6870>>
			vCamRotation = <<-13.7468, 0.0000, 11.4198>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_9
			vCamPosition = <<-515.9101, -40.3301, 49.7454>>
			vCamRotation = <<-15.1874, -0.0000, 93.7082>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_10
			vCamPosition = <<-1288.9530, -393.0926, 45.2944>>
			vCamRotation = <<-28.5592, -0.0000, 156.9409>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_11
			vCamPosition = <<933.2134, -497.2465, 69.5297>>
			vCamRotation = <<-15.4811, -0.0000, 81.6522>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_12
			vCamPosition = <<1257.5093, -593.2073, 77.2474>>
			vCamRotation = <<-9.3488, 0.0000, 57.9059>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_13
			vCamPosition = <<-1270.2802, -632.5280, 38.3999>>
			vCamRotation = <<-12.1498, -0.0000, -5.5992>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_14
			vCamPosition = <<340.5221, -704.1046, 40.3619>>
			vCamRotation = <<-14.8558, -0.0000, 25.3365>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_15
			vCamPosition = <<932.6509, -624.2589, 69.7337>>
			vCamRotation = <<-27.1982, 0.0000, 114.7252>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_16
			vCamPosition = <<-1386.5905, -795.2264, 34.5398>>
			vCamRotation = <<-21.5419, -0.0000, -73.9359>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_17
			vCamPosition = <<-628.8514, -755.0581, 35.4730>>
			vCamRotation = <<-12.8563, -0.0000, -139.2738>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_18
			vCamPosition = <<-1390.4717, -885.2930, 20.0036>>
			vCamRotation = <<-6.8589, -0.0000, -100.8430>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_19
			vCamPosition = <<-1465.0829, -891.1269, 20.3783>>
			vCamRotation = <<-4.7419, -0.0000, -72.9520>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_20
			vCamPosition = <<459.0200, -951.9058, 37.4426>>
			vCamRotation = <<-18.5120, -0.0000, 45.9026>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_21
			vCamPosition = <<1164.1083, -972.2504, 59.5149>>
			vCamRotation = <<-15.1506, 0.0000, 110.4453>>
			fCamFOV = 44.9999
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_22
			vCamPosition = <<-1339.6560, -1054.5687, 20.0319>>
			vCamRotation = <<-16.4292, -1.0441, -10.4908>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_23
			vCamPosition = <<-612.7360, -1070.2891, 35.4428>>
			vCamRotation = <<-18.2388, -0.0111, -134.6484>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_24
			vCamPosition = <<-1266.3715, -1197.4802, 16.6082>>
			vCamRotation = <<-13.3919, -0.0000, -137.7864>>
			fCamFOV = 45.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_25
			vCamPosition = <<-1079.0101, -1484.6761, 15.3639>>
			vCamRotation = <<-23.2661, 0.1792, 161.8475>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_26
			vCamPosition = <<1294.1271, -1645.9863, 63.8202>>
			vCamRotation = <<-13.3082, -0.0000, -109.6770>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_27
			vCamPosition = <<253.3068, -1704.2894, 37.2320>>
			vCamRotation = <<-16.2038, -0.0000, -162.0453>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_28
			vCamPosition = <<931.1226, -2014.4263, 38.6039>>
			vCamRotation = <<-8.4750, -0.0000, -55.4055>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_29
			vCamPosition = <<1025.2697, -2336.0598, 43.1810>>
			vCamRotation = <<-10.2937, -0.0000, 126.6524>>
			fCamFOV = 50.0
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_30 // 70
			vCamPosition = <<-718.0296, -2470.5393, 26.5757>>
			vCamRotation = <<-11.2395, -0.0000, -70.4007>>
			fCamFOV = 50.0
		BREAK
				
		DEFAULT
			CASSERTLN(DEBUG_AMBIENT, "GUNRUN_GET_MOVE_WEAPONS_BACKUP_DELIVERY_CAM_DETAILS passed non follow the leader mission dropoff!")
		
	ENDSWITCH
	
	PRINTLN("[GUNRUNNING_CUTSCENE] GUNRUN_GET_MOVE_WEAPONS_BACKUP_DELIVERY_CAM_DETAILS - Returning position: ", vCamPosition, " rotation: ", vCamRotation, " FOV: ", fCamFOV)
ENDPROC

PROC _CREATE_MOVE_WEAPONS_SCENES(SIMPLE_CUTSCENE &cutscene, FREEMODE_DELIVERY_CUTSCENE_DATA &data)
	FLOAT fTargetHeading = data.fBaseHeading + 90.0 //data.fPool[MOVE_WEAPONS_FLOAT_TARGET_HEADING]
	FLOAT fAngleStep = 360.0 / MOVE_WEAPONS_VEC_CAM_POINTS_COUNT
	FLOAT fCamFOVStart = 50.0
	INT iClosestAngleID = FLOOR(fTargetHeading / fAngleStep)
	
	PRINTLN("[GUNRUNNING_CUTSCENE] _CREATE_MOVE_WEAPONS_SCENES - Got target camera heading ", fTargetHeading)
	PRINTLN("[GUNRUNNING_CUTSCENE] _CREATE_MOVE_WEAPONS_SCENES - Closest to target point is ", iClosestAngleID)
	
	INT iCounter
	INT iPointToUse = -1
	
	// Go both ways and find the starting camera point that matches target heading
	WHILE iCounter < MOVE_WEAPONS_VEC_CAM_POINTS_COUNT
		IF iClosestAngleID + iCounter < MOVE_WEAPONS_VEC_CAM_POINTS_COUNT
			IF IS_BIT_SET(data.iBSPool[MOVE_WEAPONS_BS_OK_CAM_POINTS], iClosestAngleID + iCounter)
				iPointToUse = iClosestAngleID + iCounter
				BREAKLOOP
			ENDIF
		ENDIF
		
		IF iCounter > 0 AND iClosestAngleID - iCounter >= 0
			IF IS_BIT_SET(data.iBSPool[MOVE_WEAPONS_BS_OK_CAM_POINTS], iClosestAngleID - iCounter)
				iPointToUse = iClosestAngleID - iCounter
				BREAKLOOP
			ENDIF
		ENDIF
			
		iCounter += 1
	ENDWHILE
	
	IF iPointToUse = -1
	#IF IS_DEBUG_BUILD
	OR data.bForceUseBackupCam
	#ENDIF
		PRINTLN("[GUNRUNNING_CUTSCENE] _CREATE_MOVE_WEAPONS_SCENES - Coulnd't find any good point to put the camera in!")
		data.bFailedToFindValidCamPos = TRUE	
		iPointToUse = 0
	ELSE
		PRINTLN("[GUNRUNNING_CUTSCENE] _CREATE_MOVE_WEAPONS_SCENES - Found the closest point to the desired heading, it's ", iPointToUse)
	ENDIF
	
	VECTOR vCutscenePos = GET_ENTITY_COORDS(data.vehDropOff)//data.vBasePos
	//FLOAT fCutsceneHeading = data.fBaseHeading
	//VECTOR vCutsceneDir = GET_HEADING_AS_VECTOR(fCutsceneHeading)
	
	VECTOR vCamStartPos = data.vPool[iPointToUse]
	
	SIMPLE_CUTSCENE_CREATE(cutscene, "move_weapons")
	
	VECTOR vLookAtDir = (vCutscenePos + <<0.0, 0.0, 1.5>>) - vCamStartPos
	vLookAtDir = NORMALISE_VECTOR(vLookAtDir)
	
	VECTOR vCamRot = _GET_CAM_ROT_FROM_DIR(vLookAtDir)
	
	IF data.bFailedToFindValidCamPos
		GUNRUN_GET_MOVE_WEAPONS_BACKUP_DELIVERY_CAM_DETAILS(data.eDropoffID, vCamStartPos, vCamRot, fCamFOVStart)
		data.bFailedToFindValidCamPos = FALSE
	ENDIF
	
	SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 3500, "scene1", vCamStartPos, vCamRot, fCamFOVStart, vCamStartPos + <<0.0, 0.0, 0.0>>, vCamRot, 35.0, 0.2)
	SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 5000, "scene2", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 60.0, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 60.0, 0.2, 0, 0, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL) // This scene is gonna get filled in dynamically
	SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 400, "scene2", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 60.0, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 60.0, 0.2, 0, 0)
ENDPROC

PROC _CREATE_MOVE_WEAPONS_ASSETS(SIMPLE_CUTSCENE &cutscene, FREEMODE_DELIVERY_CUTSCENE_DATA &data)
	UNUSED_PARAMETER(cutscene)
	UNUSED_PARAMETER(data)

ENDPROC

PROC START_MOVE_WEAPONS_CUTSCENE(SIMPLE_CUTSCENE &cutscene, FREEMODE_DELIVERY_CUTSCENE_DATA &data)
	_CREATE_MOVE_WEAPONS_SCENES(cutscene, data)

	IF data.bFailedToFindValidCamPos
		EXIT
	ENDIF
	
	_CREATE_MOVE_WEAPONS_ASSETS(cutscene, data)
	
	// Compute camera position for last shot showing the camera leaving insurgent from the back door
	cutscene.sScenes[1].vCamCoordsStart = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(data.vehActors[MOVE_WEAPONS_VEH_CLONE], <<0.0, -3.5, 0.8>>)
	cutscene.sScenes[1].vCamCoordsFinish = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(data.vehActors[MOVE_WEAPONS_VEH_CLONE], <<0.0, -4.5, 0.8>>)
	
	VECTOR vCamLookAt = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(data.vehActors[MOVE_WEAPONS_VEH_CLONE], <<0.0, 0.0, 1.0>>)
	
	cutscene.sScenes[1].vCamRotStart = _GET_CAM_ROT_FROM_DIR(NORMALISE_VECTOR(vCamLookAt - cutscene.sScenes[1].vCamCoordsStart))
	cutscene.sScenes[1].vCamRotStart.Y = -GET_ENTITY_ROLL(data.vehActors[MOVE_WEAPONS_VEH_CLONE])
	cutscene.sScenes[1].vCamRotFinish = cutscene.sScenes[1].vCamRotStart
			
	cutscene.sScenes[2].vCamCoordsStart = cutscene.sScenes[1].vCamCoordsFinish
	cutscene.sScenes[2].vCamCoordsFinish = cutscene.sScenes[1].vCamCoordsFinish
	cutscene.sScenes[2].vCamRotStart = cutscene.sScenes[1].vCamRotStart
	cutscene.sScenes[2].vCamRotFinish = cutscene.sScenes[1].vCamRotStart
ENDPROC

PROC MAINTAIN_MOVE_WEAPONS_CUTSCENE(SIMPLE_CUTSCENE &cutscene, FREEMODE_DELIVERY_CUTSCENE_DATA &data)
	IF NOT DOES_ENTITY_EXIST(data.vehActors[MOVE_WEAPONS_VEH_CLONE])
	OR IS_ENTITY_DEAD(data.vehActors[MOVE_WEAPONS_VEH_CLONE])
	OR NOT DOES_ENTITY_EXIST(data.pedPlayerActors[0])
	OR IS_ENTITY_DEAD(data.pedPlayerActors[0])
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(data.iBSPool[MOVE_WEAPONS_BS_CUTSCENE], BS_MOVE_WEAPONS_CUTSCENE_TASKED_LEAVE)
		IF cutscene.iTotalElapsedTime > 500
			INT iPed
			REPEAT COUNT_OF(data.pedPlayerActors) iPed
				IF DOES_ENTITY_EXIST(data.pedPlayerActors[iPed])
					IF IS_PED_IN_ANY_VEHICLE(data.pedPlayerActors[iPed])
						//Leave the gunner in the seat, collision disabled on vehicle will fall, if enabled will jsut stand on top of truck..
						IF GET_SEAT_PED_IS_IN(data.pedPlayerActors[iPed]) <> INT_TO_ENUM(VEHICLE_SEAT, VS_INSURGENT_TURRET)
							TASK_LEAVE_ANY_VEHICLE(data.pedPlayerActors[iPed], 0, ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)
						ELSE
							SET_VEHICLE_DOOR_CONTROL(data.vehActors[MOVE_WEAPONS_VEH_CLONE], SC_DOOR_BOOT, DT_DOOR_NO_RESET, 1.0)
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
			SET_BIT(data.iBSPool[MOVE_WEAPONS_BS_CUTSCENE], BS_MOVE_WEAPONS_CUTSCENE_TASKED_LEAVE)
		ENDIF
	ENDIF
ENDPROC

PROC DESTRUCT_MOVE_WEAPONS_CUTSCENE(SIMPLE_CUTSCENE &cutscene, FREEMODE_DELIVERY_CUTSCENE_DATA &data)
	UNUSED_PARAMETER(cutscene)
	UNUSED_PARAMETER(data)
ENDPROC

#IF IS_DEBUG_BUILD
PROC MAINTAIN_MOVE_WEAPONS_CUTSCENE_DEBUG(SIMPLE_CUTSCENE &cutscene, FREEMODE_DELIVERY_CUTSCENE_DATA &data)
	UNUSED_PARAMETER(cutscene)
	UNUSED_PARAMETER(data)
	
	INT i
	REPEAT MOVE_WEAPONS_VEC_CAM_POINTS_COUNT i
		IF IS_BIT_SET(data.iBSPool[MOVE_WEAPONS_BS_OK_CAM_POINTS], i)
			DRAW_DEBUG_SPHERE(data.vPool[i], 0.1, 0, 255, 0)
		ELSE
			DRAW_DEBUG_SPHERE(data.vPool[i], 0.1, 255, 0, 0)
		ENDIF
	ENDREPEAT
ENDPROC
#ENDIF

