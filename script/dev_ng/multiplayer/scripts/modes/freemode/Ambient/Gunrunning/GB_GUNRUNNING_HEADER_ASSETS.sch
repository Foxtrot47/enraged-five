//Need to include for delivery cutscenes.
USING "globals.sch"

FUNC MODEL_NAMES GET_CRATE_MODEL_FOR_VEHICLE(MODEL_NAMES vehModel)

	SWITCH(vehModel)
		CASE POLICET
		CASE SPEEDO
		CASE PARADISE
			RETURN Prop_box_wood04a
		
		CASE BOXVILLE
		CASE SURFER
		
			RETURN prop_box_wood03a
			
		CASE insurgent
		CASE insurgent2
		CASE insurgent3
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_GR_rsply_crate04a")) //prop_mil_crate_02
			
		CASE RUMPO3
		CASE JOURNEY
			RETURN prop_box_wood01a
			
		CASE YOUGA2
		CASE BURRITO
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_GR_rsply_crate04a")) //prop_mil_crate_02
			
		CASE DLOADER
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_GR_rsply_crate04b")) //prop_mil_crate_01
		
		CASE MARSHALL	
			RETURN hei_prop_carrier_crate_01a  //p_secret_weapon_02
			
		CASE RIOT
			RETURN prop_box_ammo03a_set
			
	ENDSWITCH
	
	RETURN Prop_box_wood04a

ENDFUNC

FUNC VECTOR GET_OFFSET_FOR_CRATE_VEHICLE(MODEL_NAMES vehModel)

	SWITCH(vehModel)
		CASE PARADISE 	RETURN <<0.0, -1.25, -0.2>> 
		CASE RUMPO3 	RETURN <<0.0, -1.8, -0.4>> 
		CASE JOURNEY 	RETURN <<0.55, -2.73, 0.4>>
		CASE SURFER		RETURN <<0.0, 0.0, -0.3>>
		CASE STOCKADE	RETURN <<0.0, -2.0, 0.45>>
		CASE DLOADER	RETURN <<0.0, -1.4, 0.375>>
		CASE YOUGA2		RETURN <<0.0, -1.4, -0.2>>
		CASE MARSHALL	RETURN <<0.0, -2.44, 1.19>>
		CASE BURRITO	RETURN <<0.0, -0.85, -0.3>>
		
		CASE insurgent	
		CASE insurgent2	
		CASE insurgent3
			RETURN <<0.0, -2.2, 0.25>>
			
	ENDSWITCH

	RETURN <<0.0, -1.25, -0.1>>

ENDFUNC

FUNC VECTOR GET_ROTATION_FOR_CRATE_VEHICLE(MODEL_NAMES vehModel)

	SWITCH(vehModel)
		CASE YOUGA2		
		CASE insurgent	
		CASE insurgent2	
		CASE MARSHALL
		CASE insurgent3
			RETURN <<0.0, 0.0, 0.0>>
	ENDSWITCH

	RETURN <<0.0, 0.0, 90.0>>

ENDFUNC

FUNC BOOL ATTACH_CRATE_TO_VEHICLE(OBJECT_INDEX objCrate, VEHICLE_INDEX vehID, BOOL battachPhyiscallyOverrideInverseMass = FALSE)

	UNUSED_PARAMETER(battachPhyiscallyOverrideInverseMass)
	
	IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(objCrate, vehID)
		STRING sBone = "chassis_dummy"
//		IF battachPhyiscallyOverrideInverseMass
//			ATTACH_ENTITY_TO_ENTITY_PHYSICALLY_OVERRIDE_INVERSE_MASS(objCrate, vehID, -1, GET_ENTITY_BONE_INDEX_BY_NAME(vehID, sBone), GET_OFFSET_FOR_CRATE_VEHICLE(GET_ENTITY_MODEL(vehID)), << 0.0, 0.0, 0.0 >>, GET_ROTATION_FOR_CRATE_VEHICLE(GET_ENTITY_MODEL(vehID)), -1, TRUE, FALSE, FALSE, TRUE, DEFAULT, 0.0, 0.0)
//			PRINTLN("ATTACH_CRATE_TO_VEHICLE: Attaching crate to vehicle using ATTACH_ENTITY_TO_ENTITY_PHYSICALLY_OVERRIDE_INVERSE_MASS. Bone = ", sBone)
//		ELSE
			ATTACH_ENTITY_TO_ENTITY(objCrate, vehID, GET_ENTITY_BONE_INDEX_BY_NAME(vehID, sBone), GET_OFFSET_FOR_CRATE_VEHICLE(GET_ENTITY_MODEL(vehID)), GET_ROTATION_FOR_CRATE_VEHICLE(GET_ENTITY_MODEL(vehID)), TRUE, FALSE, TRUE)
			PRINTLN("ATTACH_CRATE_TO_VEHICLE: Attaching crate to vehicle using ATTACH_ENTITY_TO_ENTITY. Bone = ", sBone)
			//PRINTLN("[GUNRUN] - [", GET_CURRENT_VARIATION_STRING(), "] - ATTACH_CRATE_TO_VEHICLE: Attaching crate ", iGunrunEntity, " to vehicle ", NATIVE_TO_INT(vehID), "; attached to bone: ", sBone)
//		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_DISABLE_COLLISIONS_FOR_VEHICLE_CRATE(INT iGunrunEntity, VEHICLE_INDEX vehID)

	UNUSED_PARAMETER(iGunrunEntity)

	IF GET_ENTITY_MODEL(vehID) = INSURGENT2
	OR GET_ENTITY_MODEL(vehID) = BURRITO
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

