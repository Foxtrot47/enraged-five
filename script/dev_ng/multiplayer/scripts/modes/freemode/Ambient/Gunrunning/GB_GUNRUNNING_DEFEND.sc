//////////////////////////////////////////////////////////////////////////////////////////
// Name:        GB_CONTABAND_DEFEND.sc													//
// Description: Defend Contraband business												//
// Written by:  David Watson															//
// Date: 02/02/2016																		//
//////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "net_mission.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_event.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"

//
USING "net_scoring_common.sch"
USING "net_ambience.sch"

//USING "help_at_location.sch"
USING "commands_object.sch"

USING "net_xp_animation.sch"

USING "net_wait_zero.sch"

USING "net_simple_interior_warehouse.sch"

USING "net_gang_boss.sch"
USING "DM_Leaderboard.sch"
USING "am_common_ui.sch"

USING "net_simple_interior.sch"

USING "GB_GUNRUNNING_DEFEND_HEADER.sch"

USING "net_gang_boss_gang_chase.sch"
USING "net_gang_boss_packages.sch"
USING "net_gang_boss_spectate.sch"

USING "hacking_public.sch"
USING "net_freemode_delivery_common.sch"
USING "net_freemode_delivery_public.sch"
USING "net_realty_biker_warehouse.sch"
#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF




// Game States. 
CONST_INT GAME_STATE_INI 				0
CONST_INT GAME_STATE_RUNNING			1
CONST_INT GAME_STATE_LEAVE				2
CONST_INT GAME_STATE_TERMINATE_DELAY	3
CONST_INT GAME_STATE_END				4


ENUM GB_GUNRUNNING_DEFEND_STAGE_ENUM
	eAD_TARGETS,
	eAD_ATTACK,
	eAD_OFF_MISSION,
	eAD_CLEANUP
ENDENUM





CONST_INT MAX_TARGETS		30
CONST_INT MAX_SPAWN_LOC		15
CONST_INT MAX_AMBIENT_VEH	4
CONST_INT MAX_ENEMY_VEH		6
CONST_INT MAX_PLAYER_VEH	1
CONST_INT MAX_SCEN_BLOCKING	2
CONST_INT MAX_FRIENDLY_PEDS	2
CONST_INT MAX_ANGLED_AREA_CHECKS	3
CONST_INT MAX_BOMBS			3
CONST_INT MAX_VALKYRIE_VARIATIONS	3
CONST_INT MAX_DEFUSAL_VARIATIONS	4

CONST_INT MAX_DEFEND_PROPS	20
CONST_INT MAX_DEFEND_PACKAGES	2
CONST_INT TIME_EXPOLDE_VEH		30000
CONST_INT HACKING_IP_CONNECT			0
CONST_INT HACKING_BRUTEFORCE			1
CONST_INT HACKING_NEW_MG				2
CONST_INT MAX_NUM_HACKING_MG			3

CONST_FLOAT bombHelpTextRange 700.0
CONST_FLOAT	 actionAreaRange 70.0



CONST_INT TIME_FORCE_RIVALS_JOIN	600000

CONST_INT SERVER_BITSET_ARRAY_SIZE	2

//Server Bitset Data
CONST_INT biS_SomeoneNearActionArea		0
CONST_INT biS_AllInitalTargetsKilled	1
CONST_INT biS_AllPlayersFinished		2
CONST_INT biS_DurationExpired			3
CONST_INT biS_VehDelivered				4
CONST_INT biS_BossLaunchedQuit			5
CONST_INT biS_VehSetOff					6
CONST_INT biS_AllBombsDefused			7
CONST_INT biS_ContrabandWrecked			8
CONST_INT biS_ContrabandLost			9
CONST_INT biS_InVanAtLeastOnce			10
CONST_INT biS_ContraVehClearOfAction	11
CONST_INT biS_BlockedScenarios			12
CONST_INT biS_ApproachedWarehouse		13
CONST_INT biS_FindNewVehSpawnCoords		14
CONST_INT biS_TimeoutExpired			15
CONST_INT biS_ContrabandRemovedAtEnd	16
CONST_INT biS_AllPackagesDelivered		17
CONST_INT biS_AllFinishedWarp			18
CONST_INT biS_AllTargetsKilled			19
CONST_INT biS_EnemiesAlerted			20
CONST_INT biS_EnemyPilotEnteredHeli		21
//CONST_INT biS_HeloTextSent				22
CONST_INT biS_HeliAboutToEscape			22
//CONST_INT biS_HeloDestroyedTextSent		24
//CONST_INT biS_HeloShotDown				25
CONST_INT biS_PilotTimerUp				23
CONST_INT biS_ValkyrieDestroyed         24
CONST_INT biS_MissionStarted            25
CONST_INT biS_WithinBombTextRange       26
CONST_INT biS_BombTimerExpired 			27
CONST_INT biS_BombTriggered				28
CONST_INT biS_FirstBombDefusalInitiated	29
CONST_INT biS_BombTimerStarted			30
CONST_INT biS_CleanupHeli				31
CONST_INT biS_GangChaseDelayTimerUp	    32

//CONST_INT biS_HelpIntroTextSent         29
//CONST_INT biS_HelpStealthTextSent       30
//CONST_INT biS_StealthStageStarted       31
//CONST_INT biS_GangChaseStarted	        32
//CONST_INT biS_GangChaseTextSent	        28

//CONST_INT biS_StealthTextDisplaying     33
//CONST_INT biS_SpookTextSent    			34	



CONST_INT PED_AI_STATE_CREATED				0
CONST_INT PED_AI_STATE_TOUR_IN_CAR			1
CONST_INT PED_AI_STATE_FLEE_IN_CAR			2
CONST_INT PED_AI_STATE_LEAVE_CAR			3
CONST_INT PED_AI_STATE_FLEE_ON_FOOT			4
CONST_INT PED_AI_STATE_TOUR_ON_FOOT			5
CONST_INT PED_AI_STATE_DEFEND_AREA			6
CONST_INT PED_AI_STATE_DEFEND_IN_CAR		7
CONST_INT PED_AI_STATE_WAIT_FOR_APPROACH	8
CONST_INT PED_AI_STATE_ESCORT				9
CONST_INT PED_AI_STATE_TOUR_IN_CAR_AS_PASS	10
CONST_INT PED_AI_STATE_PLAY_SCENARIO		11
CONST_INT PED_AI_STATE_COWER				12
CONST_INT PED_AI_STATE_HOVER_IN_HELI		13
CONST_INT PED_AI_STATE_GOTO_COORD_IN_HELI	14
CONST_INT PED_AI_STATE_CLEANUP				15
CONST_INT PED_AI_STATE_GOTO_COORD_ON_FOOT	16
CONST_INT PED_AI_STATE_DELETE				17
CONST_INT PED_AI_STATE_GOTO_COORD_IN_CAR	18
CONST_INT PED_AI_STATE_HELI_ATTACK			19
CONST_INT PED_AI_STATE_ENTER_CAR			20
CONST_INT PED_AI_STATE_FINISHED				99

CONST_INT ENEMY_PED_BIT_CREATE_AS_DRIVER	0
CONST_INT ENEMY_PED_BIT_DEFEND_START_LOC	1
CONST_INT ENEMY_PED_BIT_CREATE_AS_FPASS		2
CONST_INT ENEMY_PED_BIT_CREATE_ON_FOOT		3
CONST_INT ENEMY_PED_BIT_ESCORT				4
CONST_INT ENEMY_PED_BIT_CREATE_AS_RLPASS	5
CONST_INT ENEMY_PED_BIT_CREATE_AS_RRPASS	6
CONST_INT ENEMY_PED_BIT_HAS_SCENARIO		7
CONST_INT ENEMY_PED_BIT_USE_DEFENSIVE_AREA	8
CONST_INT ENEMY_PED_BIT_COWER_WHEN_SPOOKED	9
CONST_INT ENEMY_PED_BIT_IS_COP				10
CONST_INT ENEMY_PED_BIT_BLIP_AS_TARGET		11
CONST_INT ENEMY_PED_BIT_DO_NOT_AUTO_SPOOK	12
CONST_INT ENEMY_PED_BIT_HAS_MARKER			13
CONST_INT ENEMY_PED_BIT_SPOOKED_BY_AREA		14
CONST_INT ENEMY_PED_HELI_PILOT				15
CONST_INT ENEMY_PED_HELI_PASS				16
CONST_INT ENEMY_PED_CUSTOM_VARIATION		17
CONST_INT ENEMY_PED_DRIVE_TO_COORD			18
CONST_INT ENEMY_PED_REACHED_DRIVE_TO_COORD	19
CONST_INT ENEMY_PED_BIT_IS_VEHICLE_GUNNER	20
CONST_INT ENEMY_PED_BIT_VISION_CONE			21
CONST_INT ENEMY_PED_ENTERED_HELI			22
CONST_INT ENEMY_PED_DELAYED_REACTION_FINISHED 23
CONST_INT ENEMY_IS_AMBUSH_PED				24

CONST_INT ENEMY_VEH_BIT_GOT_SPAWN_COORDS		0
CONST_INT ENEMY_VEH_BIT_ATTACKED_BY_PLAYER		1
CONST_INT ENEMY_VEH_BIT_DO_NEARBY_PLAYER_CHECK	2
CONST_INT ENEMY_VEH_BIT_PLAYER_NEARBY			3
CONST_INT ENEMY_VEH_BIT_USE_EXACT_COORDS		4
CONST_INT ENEMY_VEH_BIT_RIOT_VAN_DAMAGED		5
CONST_INT ENEMY_VEH_BIT_OPEN_REAR_RIGHT_DOOR	6
CONST_INT ENEMY_VEH_BIT_OPEN_REAR_LEFT_DOOR		7
CONST_INT ENEMY_VEH_BIT_PREVENT_OPEN_BY_EXPLOSION		8


CONST_INT PLAYER_VEH_BIT_TRIED_CLEAR_AREA	0
CONST_INT PLAYER_VEH_BIT_GOT_SPAWN_COORDS	1
CONST_INT PLAYER_VEH_BIT_FLASH_BLIP			2
CONST_INT PLAYER_VEH_BIT_PREVENT_OPEN_BY_EXPLOSION 3
CONST_INT PLAYER_VEH_BIT_OPEN_REAR_RIGHT_DOOR 4
CONST_INT PLAYER_VEH_BIT_OPEN_REAR_LEFT_DOOR 5


CONST_INT PROP_BIT_ATTACH_TO_VEH			0
CONST_INT PROP_BIT_BOMB_DISARMED_PROP		1

CONST_INT CRATE_BS_ANIM_PLAYING				0
CONST_INT CRATE_BS_ANIM_STARTED				1
CONST_INT CRATE_BS_ANIM_DONE				2


//CONST_INT GLOBAL_MISSION_TIME 1800000//290000//
//SCRIPT_TIMER pilotTimer
CONST_INT pilotTimerThreshold 300000
SCRIPT_TIMER dropoffSendEventTimer
CONST_INT dropoffSendEventTimerThreshold 5000
SCRIPT_TIMER bombTimer
//CONST_INT bombTimerThreshold 180000// 33000//30000 /*5000*///180000
FLOAT bombTimerTickingThreshold = 30000 /*5000*///180000
INT intensityThreshold = 1000
FLOAT tempThreshold
//INT inumPropsDeleted
SCRIPT_TIMER bombSoundIntensityTimer
INT iAttempt = 0
FLOAT fClearAreaRadius = 40.0

//SCRIPT_TIMER stealthFlashBlipTimer
CONST_INT stealthFlashBlipTimerThreshold 3000

CONST_INT gangChaseDelay 15000
INT hardcodeBusinessToDefend = 0
FLOAT fDistanceFromBunker = 500
FLOAT fDistanceFromContrabandVehicle = 250

CONST_INT PEDS_PER_WAVE 5
INT currentNumberOfPedsSpawnedByWave
CONST_INT MAX_PED_WAVE_SPAWN_POINTS 30
//sound variables
INT iStartBombSound = -1
INT iTickBombSound = -1
INT iDisarmBombSound = -1
INT iArmedBombSound = -1

CONST_INT MAX_PACKAGES 1
CONST_INT MAX_CUSTOM_SPAWN_POINTS 8

SCRIPT_TIMER stGarageTimer, stGarageSyncDelay

FLOAT iTickIntensity

//CONST_INT pedAccuracy 15
VECTOR globalPedSpawnWaveVector =  <<0,0,0>>

FLOAT fHeliCruiseSpeed = 30.0
//SCRIPT_TIMER fGangChaseDelayTimer
//FLOAT fGangChaseDelayTimerThreshold 15000
//
//start_net_timer
//has_net_timer_started
//has net timer expired(timer,ms e.g. 5s = 5000
//reset net timer.

STRUCT structTarget
	INT iTargetType
	NETWORK_INDEX niPed
	NETWORK_INDEX niVeh
	
	MODEL_NAMES mVeh
	MODEL_NAMES mPed
	
	VECTOR vSpawnCoord
	FLOAT fSpawnHead
	INT iServerTargetBitset
	INT iPedState
	INT iGotoCoordCount
	SCRIPT_TIMER stReactionDelayTimer
	INT iReactionDelay = 0

ENDSTRUCT


STRUCT structDefendSpawnCoords
	VECTOR vLoc
	FLOAT fHead
	BOOL bUsed
ENDSTRUCT

STRUCT_GANG_CHASE_LOCAL gangChaseLocalVar

// The server broadcast data.
// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData

	INT iServerGameState
	INT iServerBitSet[SERVER_BITSET_ARRAY_SIZE]
	SCRIPT_TIMER gangChaseTimer
	SCRIPT_TIMER pilotTimer
	
	//bitset for defusal waves
	//ambush stuff
	INT iCanSpawnDefusalWave
	INT iCurrentAmbushPedToSpawn = -1
	
	structTarget Targets[MAX_TARGETS]
	//structTarget AmbushTargets[MAX_TARGETS]
	INT iGlobalBombCount = 0
	structDefendSpawnCoords pedSpawnCoords[MAX_SPAWN_LOC]
	structDefendSpawnCoords ambientVehSpawnCoords[MAX_AMBIENT_VEH]
	structDefendSpawnCoords enemyVehSpawnCoords[MAX_ENEMY_VEH]
	structDefendSpawnCoords propSpawnCoords[MAX_DEFEND_PROPS]
	structDefendSpawnCoords packageSpawnCoords[MAX_DEFEND_PACKAGES] 
	INT iGangVariation = -1
	INT iVariation = -1
//	INT iWarehouseToUse = -1
	INT iBusinessToDefend = -1
	INT iStartingContraband = -1
	INT currentAmbushWaveTally
	NETWORK_INDEX niVehAmbient[MAX_AMBIENT_VEH]
	NETWORK_INDEX niVeh[MAX_ENEMY_VEH]
	NETWORK_INDEX niPlayerVeh[MAX_PLAYER_VEH]
	NETWORK_INDEX niProp[MAX_DEFEND_PROPS]
	
	INT iEnemyVehBitset[MAX_ENEMY_VEH]
	INT iPlayerVehBitset[MAX_PLAYER_VEH]
	INT iServerPropBitset[MAX_DEFEND_PROPS]
	INT iTargetCreatedBitSet
	INT iTargetDriveFastBitSet
	INT iTargetVehDamagedBitSet
	INT iTargetAimedAtBitSet
	INT iTargetNearbyShotsBitset
	INT iTargetVehStuckBitSet
	INT iTargetSpookedByAreaCheck
	INT iTargetKilledBitset
	INT iTargetKilledByRivalBitset
	INT iLockDoorsBitset
	INT iVehClearAreaBitset
	INT iPropClearAreaBitset
	INT iPedClearAreaBitset
	INT iNumTargetsKilled = 0
	GB_GUNRUNNING_DEFEND_STAGE_ENUM eStage = eAD_TARGETS
	INT iPartDrivingContraVehicle = -1
	INT iPlayerDrivingContraVehicle = -1
	INT iBossPartWhoLaunched = -1
	INT iBossPlayerWhoLaunched = -1
	INT iPlayerKilledBoss = -1
	INT iPlayerDeliveredVeh = -1
	INT iEnemyPedSpookedServer
	INT iTimeBeforeLoseContra = 0
	INT iPedWavePedLocationUsedBitset

	SCRIPT_TIMER MissionTerminateDelayTimer
	SCRIPT_TIMER timeDuration
	SCRIPT_TIMER timeSpawnAmbVeh
	SCRIPT_TIMER timeStartToLoseContra
	SCRIPT_TIMER timeTimeout
	SCRIPT_TIMER timeKillFailsafe[MAX_TARGETS]
	
	INT iPlayerCanDefuseBomb[MAX_BOMBS]
	INT iBombDefusedByPlayer[MAX_BOMBS]
	SCENARIO_BLOCKING_INDEX scenWarehouse[MAX_SCEN_BLOCKING]
	STRUCT_COLLECT_GB_PACKAGES_SERVER packagesServer
	INT iMinPlayers = -1
	INT iNumberOfPackages = -1
	INT iMatchId1
	INT iMatchId2
//	INT iTrafficReductionIndex
	INT iMeterValueWhenInitialAllKilled = -1
	INT iMeterValueWhenReachedArea = -1
	FLOAT fAmountOfProductToLose = -1.0
	INT iCrateBS[MAX_GANG_BOSS_GB_PACKAGES]
	
	#IF IS_DEBUG_BUILD
	BOOL bAllowWith2Players = FALSE
	BOOL bSomeoneSPassed
	#ENDIF
	
	STRUCT_GANG_CHASE_SERVER gangChaseServer
	INT iOrganisationSizeOnLaunch
	NETWORK_INDEX pilotPed
	
	FREEMODE_DELIVERABLE_ID deliverableGunrunIds[MAX_NUM_GUNRUN_ENTITIES]
	
	//Freemode delivery mission ID
	FM_DELIVERY_MISSION_ID sMissionID
	
	#IF IS_DEBUG_BUILD
		INT missionTimerOverride = -1
		//INT iValkyrieSelection = -1
		//INT iDefusalSelection = -1
		
	#ENDIF
ENDSTRUCT
ServerBroadcastData serverBD
STRUCT_GANG_CHASE_OPTIONS gangChaseOptions


CONST_INT biP_MissionStartExpired		0
CONST_INT biP_VehDelivered				1
CONST_INT biP_InVeh						2
CONST_INT biP_Finished					3
CONST_INT biP_ApproachedVan				4
CONST_INT biP_CanSeeVan					5
CONST_INT biP_VanWrecked				6
CONST_INT biP_JustCompHack				7
CONST_INT biP_ApproachedWarehouse		8
CONST_INT biP_HudOkForSCTV				9
CONST_INT biP_ContrabandRemovedAtEnd	10
CONST_INT biP_ContraVehClearOfAction	11
CONST_INT biP_NearACtionArea			12
CONST_INT biP_MoveOffMission			13
CONST_INT biP_InEnemyTriggerArea		14
CONST_INT biP_WithinObjectiveRange		15
CONST_INT biP_CloseEnoughToSendBombInfoText	16
CONST_INT biP_BombTimerExpired		17
CONST_INT biP_BombTimerStarted		18
CONST_INT biP_CleanupHeli		19
CONST_INT biP_ValkyrieWrecked	20



// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	INT iGameState
	
	INT iPlayerBitSet
	
	INT iPlayerAimAtTargetBitSet
	INT iPlayerTargetVehStuckBitSet
	INT iPlayerShotsNearTargetBitSet
	INT iPlayerTargetVehDamagedBitSet
	INT iPlayerTargetVehNearbyBitSet
	INT iPlayerKilledTargetBitset
	INT iPlayerTargetSpookedByAreaCheck
	INT iNonPartPlayerKilledTargetBitset
	INT iNonPlayerKilledTargetBitset


	INT iPackagesICanSeeBitset
	INT iNumCompletedHackingGames
	INT iEnemyPedSpookedClient
	INT iEnemyPedReachedInCarGoto
	
	INT iWantToDefuseBomb
	INT iDefusedBomb
	GB_GUNRUNNING_DEFEND_STAGE_ENUM eStage = eAD_TARGETS
	INT iMyBossPart = -1
	INT iMyDamagedRiotVanCount
	INT iCrateBS[MAX_GANG_BOSS_GB_PACKAGES]
	
	//to trigger wave of enemies
	INT iStartedDefusingBomb
	
	
	#IF IS_DEBUG_BUILD
	INT iPDebugBitset
	#ENDIF
ENDSTRUCT
PlayerBroadcastData playerBD[GB_MAX_GANG_SIZE_INCLUDING_BOSS]

///// MISSION VARIABLES /////
INT iBoolsBitSet
CONST_INT biHelp1Done							0
CONST_INT biL_DoneIntroShard					1
CONST_INT biL_DoneIntroHelp						2
CONST_INT biL_WaitForShard						3
CONST_INT biL_KilledWhileHack					4
CONST_INT biL_BlippedRival						5
CONST_INT biL_DoneEndTelemetry					6
CONST_INT biL_ResetBlipForShard					7
CONST_INT biL_DoneLoseContraHlp					8
CONST_INT biL_ResetPlayerBlips					9
CONST_INT biL_DefuseBomb						10
CONST_INT biL_DoneOpeningTxt					11
CONST_INT biL_StartedAlarm						12
CONST_INT biL_DisabledVehLockon					13
CONST_INT biL_SetupSpawnCoord 					14
CONST_INT biL_ShouldResetWanted					15
CONST_INT biL_ResetWanted						16
CONST_INT biL_SetupTelemetry					17
CONST_INT biL_ResetAllPlayerBlips 				18
CONST_INT biL_DoneIntroPhonecall				19
CONST_INT biL_AddedPhoneContact					20
CONST_INT biL_DeliverablesSetup					21
CONST_INT biL_DropoffSetup						22
CONST_INT biL_HelpIntroTextSent					23
CONST_INT biL_SpookTextSent						24
CONST_INT biL_HelpStealthTextSent				25
CONST_INT biL_StealthTextDisplaying				26
CONST_INT biL_StealthTextFlashBlip				27
CONST_INT biL_PackageFlashBlip					28
CONST_INT biL_HelpHeloDestroyTextSent			29
CONST_INT biL_GangChaseStarted	        		30
CONST_INT biL_StealthStageStarted	        	31

INT iBoolsBitSet2
CONST_INT biL2_FlashedVehBlip					0
CONST_INT biL2_IntroCallOnGoing					1
CONST_INT biL2_IntroCallFinished				2
CONST_INT biL2_HeloShotDown						3
CONST_INT biL2_HeloTextSent						4
CONST_INT biL2_HeloDestroyedTextSent			5
CONST_INT biL2_GangChaseTextSent				6
CONST_INT biL2_BombHelpTextSent					7
CONST_INT biL2_BombInfoTextSent					8
CONST_INT biL2_VehicleUnlocked					9
CONST_INT biL2_BombInstructionHelpTextSent		10
CONST_INT biL2_BombInstructionHelpTextCleared	11
CONST_INT biL2_StartBombSoundPlayed				12
CONST_INT biL2_EndBombSoundPlayed				13
CONST_INT biL2_bomb1Defused						14
CONST_INT biL2_bomb2Defused						15
CONST_INT biL2_bomb3Defused						16
CONST_INT biL2_bombsArmedSoundPlayed			17
CONST_INT biL2_wantedLevelSet					18
CONST_INT biL2_ExplodedTruck					19
CONST_INT biL2_RadarZoomed						20
CONST_INT biL2_InPedSpookArea					21
CONST_INT biL2_DoneMidPhonecall					22
CONST_INT biL2_MidCallOnGoing					23
CONST_INT biL2_MidCallFinished					24
CONST_INT biL2_DidMusicCue0						25
CONST_INT biL2_DidMusicCue1						26
CONST_INT biL2_DidMusicCue2						27
CONST_INT biL2_DidMusicCue3						28
CONST_INT biL2_DidMusicCue4						29
CONST_INT biL2_DidMusicCue5						30
CONST_INT biL2_DidMusicCue6						31

INT iBoolsBitSet3

CONST_INT biL3_BombTimerHelpTextSent			0
CONST_INT biL3_MaintainDropOffTriggered			1





INT iMusicBitset
CONST_INT biMusic_Init					0
CONST_INT biMusic_TriggerStart			1
CONST_INT biMusic_Reset					2
CONST_INT biMusic_ArriveAtWarehouse		3
CONST_INT biMusic_AllTargetsKilled		4
CONST_INT biMusic_30sStarted			5
CONST_INT biMusic_BackupComing			6
CONST_INT biMusic_MissionPassed			7
CONST_INT biMusic_ShouldStop30s			8
CONST_INT biMusic_Started30sCountdown	9
CONST_INT biMusic_StoppedCountdownMusic	10
CONST_INT biMusic_PrepareKill30s		11
CONST_INT biMusic_RestoreRadio			12
CONST_INT biMusic_DoneFadeInRadio		13
CONST_INT biMusic_DoneStart30s			14
CONST_INT biMusic_DonePre30sStop		15
CONST_INT biMusic_AllInitialKilled		16
CONST_INT biMusic_MissionFailed			17
CONST_INT biMusic_DrivingMissionVan		18
CONST_INT biMusic_GotPackage			19
CONST_INT biMusic_GetawaySpooked		20
CONST_INT biMusic_LostCops				21

CONST_INT biTxtMsg_DoGangChaseTxt		0
CONST_INT biTxtMsg_DoRiotVanText		1

CONST_INT biTxtMsg_DoneGangChaseTxt		0
CONST_INT biTxtMsg_DoneRiotVanText		1


//INT iDoTextMsgBitset
//INT iDoneTextMsgBitset

//INT iInitPlayerVehBlips
INT iStaggeredParticipant

INT iLocalPartDrivingContraVehicle = -2
INT iGangCount
//--Relationship groups

REL_GROUP_HASH relDefendPlayer
REL_GROUP_HASH relMyFmGroup


STRUCT_LOCAL_GB_PACKAGE_DATA localPackages

INT iAlarmSoundID


//INT iMeterMax, iMeterCurrent

// Contraband transaction
CONTRABAND_TRANSACTION_STATE eResult

GB_STRUCT_BOSS_END_UI gbBossEndUi

AI_BLIP_STRUCT PedBlipData[MAX_TARGETS]
BLIP_INDEX blipTarget[MAX_TARGETS]
BLIP_INDEX blipDropOff
BLIP_INDEX blipAction
BLIP_INDEX blipWarehouse
BLIP_INDEX blipWarehouseDropOff
BLIP_INDEX blipVeh[MAX_PLAYER_VEH]
BLIP_INDEX blipBomb[MAX_BOMBS]


GB_COUNTDOWN_MUSIC_STRUCT cmStruct

INT iNumValidParts

INT iLocalTargetGotoCount[MAX_TARGETS]


//HUD_COLOURS hcMyGang = HUD_COLOUR_PURE_WHITE
//BOOL bDoneDistanceCheck

S_HACKING_DATA hackGUNRUNNING_DEFEND
INT iHackingProg = -1
INT iMyHackingMG = -1
INT iHackSpeed

FLARE_STRUCT sFlare

// Widgets & Debug.
#IF IS_DEBUG_BUILD
	BOOL bHostEndMissionNow
	BOOL bWdShowDistToVan
	BOOL bWdDoOnScreenDebug
	BOOL bWdBlipAllDrivers
	FLOAT fWdVanDist
	CONST_INT biPDebug_PressedS		0
	CONST_INT biPDebug_PressedF		1
	CONST_INT biPDebug_PressedJ		2
#ENDIF

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MAIN PROCS                            ////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF IS_DEBUG_BUILD
FUNC STRING GET_SERVER_TARGET_BIT_NAME(INT iBit)
	SWITCH iBit
		CASE ENEMY_PED_BIT_CREATE_AS_DRIVER				RETURN "ENEMY_PED_BIT_CREATE_AS_DRIVER"
		CASE ENEMY_PED_BIT_DEFEND_START_LOC				RETURN "ENEMY_PED_BIT_DEFEND_START_LOC"
		CASE ENEMY_PED_BIT_CREATE_AS_FPASS				RETURN "ENEMY_PED_BIT_CREATE_AS_FPASS"
		CASE ENEMY_PED_BIT_CREATE_ON_FOOT				RETURN "ENEMY_PED_BIT_CREATE_ON_FOOT"
		CASE ENEMY_PED_BIT_CREATE_AS_RLPASS				RETURN "ENEMY_PED_BIT_CREATE_AS_RLPASS"
		CASE ENEMY_PED_BIT_CREATE_AS_RRPASS				RETURN "ENEMY_PED_BIT_CREATE_AS_RRPASS"
		CASE ENEMY_PED_BIT_HAS_SCENARIO					RETURN "ENEMY_PED_BIT_HAS_SCENARIO"
		CASE ENEMY_PED_BIT_USE_DEFENSIVE_AREA			RETURN "ENEMY_PED_BIT_USE_DEFENSIVE_AREA"
		CASE ENEMY_PED_BIT_COWER_WHEN_SPOOKED			RETURN "ENEMY_PED_BIT_COWER_WHEN_SPOOKED"
		CASE ENEMY_PED_BIT_IS_COP						RETURN "ENEMY_PED_BIT_IS_COP"
		CASE ENEMY_PED_BIT_DO_NOT_AUTO_SPOOK			RETURN "ENEMY_PED_BIT_DO_NOT_AUTO_SPOOK"
		CASE ENEMY_PED_BIT_BLIP_AS_TARGET				RETURN "ENEMY_PED_BIT_BLIP_AS_TARGET"
		CASE ENEMY_PED_BIT_HAS_MARKER					RETURN "ENEMY_PED_BIT_HAS_MARKER"
		CASE ENEMY_PED_BIT_SPOOKED_BY_AREA				RETURN "ENEMY_PED_BIT_SPOOKED_BY_AREA"
		CASE ENEMY_PED_HELI_PILOT						RETURN "ENEMY_PED_HELI_PILOT"
		CASE ENEMY_PED_HELI_PASS						RETURN "ENEMY_PED_HELI_PASS"
		CASE ENEMY_PED_CUSTOM_VARIATION					RETURN "ENEMY_PED_CUSTOM_VARIATION"
		CASE ENEMY_PED_DRIVE_TO_COORD					RETURN "ENEMY_PED_DRIVE_TO_COORD"
		CASE ENEMY_PED_REACHED_DRIVE_TO_COORD			RETURN "ENEMY_PED_REACHED_DRIVE_TO_COORD"
		CASE ENEMY_PED_BIT_IS_VEHICLE_GUNNER			RETURN "ENEMY_PED_BIT_IS_VEHICLE_GUNNER"
		CASE ENEMY_PED_BIT_VISION_CONE					RETURN "ENEMY_PED_BIT_VISION_CONE"
	ENDSWITCH
	
	RETURN "INVALID"
ENDFUNC

FUNC STRING GET_SERVER_ENEMY_VEH_BIT_NAME(INT iBit)
	SWITCH iBit
		CASE ENEMY_VEH_BIT_GOT_SPAWN_COORDS				RETURN "ENEMY_VEH_BIT_GOT_SPAWN_COORDS"
		CASE ENEMY_VEH_BIT_ATTACKED_BY_PLAYER			RETURN "ENEMY_VEH_BIT_ATTACKED_BY_PLAYER"
		CASE ENEMY_VEH_BIT_DO_NEARBY_PLAYER_CHECK		RETURN "ENEMY_VEH_BIT_DO_NEARBY_PLAYER_CHECK"
		CASE ENEMY_VEH_BIT_PLAYER_NEARBY				RETURN "ENEMY_VEH_BIT_PLAYER_NEARBY"
		CASE ENEMY_VEH_BIT_USE_EXACT_COORDS				RETURN "ENEMY_VEH_BIT_USE_EXACT_COORDS"
		CASE ENEMY_VEH_BIT_RIOT_VAN_DAMAGED				RETURN "ENEMY_VEH_BIT_RIOT_VAN_DAMAGED"
		CASE ENEMY_VEH_BIT_OPEN_REAR_RIGHT_DOOR			RETURN "ENEMY_VEH_BIT_OPEN_REAR_RIGHT_DOOR"
		CASE ENEMY_VEH_BIT_OPEN_REAR_LEFT_DOOR			RETURN "ENEMY_VEH_BIT_OPEN_REAR_LEFT_DOOR"
		CASE ENEMY_VEH_BIT_PREVENT_OPEN_BY_EXPLOSION	RETURN "ENEMY_VEH_BIT_PREVENT_OPEN_BY_EXPLOSION"
	ENDSWITCH
	
	RETURN "INVALID"
ENDFUNC

FUNC STRING GET_SERVER_PLAYER_VEH_BIT_NAME(INT iBit)
	SWITCH iBit
		CASE PLAYER_VEH_BIT_TRIED_CLEAR_AREA				RETURN "PLAYER_VEH_BIT_TRIED_CLEAR_AREA"
		CASE PLAYER_VEH_BIT_GOT_SPAWN_COORDS				RETURN "PLAYER_VEH_BIT_GOT_SPAWN_COORDS"
		CASE PLAYER_VEH_BIT_FLASH_BLIP						RETURN "PLAYER_VEH_BIT_FLASH_BLIP" 
	ENDSWITCH
	
	RETURN "INVALID"
ENDFUNC

FUNC STRING GET_SERVER_PROP_BIT_NAME(INT iBit)
	SWITCH iBit
		CASE PROP_BIT_ATTACH_TO_VEH							RETURN "PROP_BIT_ATTACH_TO_VEH"
		CASE PROP_BIT_BOMB_DISARMED_PROP					RETURN "PROP_BIT_BOMB_DISARMED_PROP"
		
	ENDSWITCH
	
	RETURN "INVALID"
ENDFUNC
PROC DISPLAY_ON_SCREEN_STRING_AND_NUMBER(FLOAT fX, FLOAT fY, STRING sDisplay, INT iDisplay)
	INT iR, iG, iB, iA
	GET_HUD_COLOUR(HUD_COLOUR_RED, iR, iG, iB, iA)
	
	SET_TEXT_COLOUR(iR, iG, iB, iA)
	SET_TEXT_SCALE(0.0000, 0.5)
	DISPLAY_TEXT_WITH_LITERAL_STRING(fX, fY, "STRING", sDisplay)
	
	SET_TEXT_COLOUR(iR, iG, iB, iA)
	SET_TEXT_SCALE(0.0000, 0.5)
	DISPLAY_TEXT_WITH_NUMBER((fX + 0.2), fY, "NUMBER", iDisplay)	
ENDPROC
#ENDIF

FUNC STRING GET_SERVER_BIT_NAME(INT iBitset)
		SWITCH iBitset
			CASE biS_SomeoneNearActionArea		RETURN "biS_SomeoneNearActionArea"
			CASE biS_AllInitalTargetsKilled		RETURN "biS_AllInitalTargetsKilled"
			CASE biS_AllPlayersFinished		    RETURN "biS_AllPlayersFinished"
			CASE biS_DurationExpired			RETURN "biS_DurationExpired"
			CASE biS_VehDelivered				RETURN "biS_VehDelivered"
			CASE biS_BossLaunchedQuit			RETURN "biS_BossLaunchedQuit"
			CASE biS_VehSetOff					RETURN "biS_VehSetOff"
			CASE biS_AllBombsDefused			RETURN "biS_AllBombsDefused"
			CASE biS_ContrabandWrecked			RETURN "biS_ContrabandWrecked"
			CASE biS_ContrabandLost				RETURN "biS_ContrabandLost"
			CASE biS_InVanAtLeastOnce			RETURN "biS_InVanAtLeastOnce"
			CASE biS_ContraVehClearOfAction		RETURN "biS_ContraVehClearOfAction"
			CASE biS_BlockedScenarios			RETURN "biS_BlockedScenarios"
			CASE biS_ApproachedWarehouse		RETURN "biS_ApproachedWarehouse"
			CASE biS_FindNewVehSpawnCoords		RETURN "biS_FindNewVehSpawnCoords"
			CASE biS_TimeoutExpired				RETURN "biS_TimeoutExpired"
			CASE biS_ContrabandRemovedAtEnd		RETURN "biS_ContrabandRemovedAtEnd"
			CASE biS_AllPackagesDelivered		RETURN "biS_AllPackagesDelivered"
			CASE biS_AllFinishedWarp			RETURN "biS_AllFinishedWarp"
			CASE biS_AllTargetsKilled			RETURN "biS_AllTargetsKilled"
			CASE biS_EnemiesAlerted				RETURN "biS_EnemiesAlerted"
			CASE biS_EnemyPilotEnteredHeli		RETURN "biS_EnemyPilotEnteredHeli"
			//CASE biS_HeloTextSent				RETURN "biS_HeloTextSent"
			CASE biS_HeliAboutToEscape			RETURN "biS_HeliAboutToEscape"
			//CASE biS_HeloDestroyedTextSent		RETURN "biS_HeloDestroyedTextSent"
			//CASE biS_HeloShotDown				RETURN "biS_HeloShotDown"
			CASE biS_PilotTimerUp				RETURN "biS_PilotTimerUp"
			CASE biS_ValkyrieDestroyed          RETURN "biS_ValkyrieDestroyed"
			CASE biS_MissionStarted             RETURN "biS_MissionStarted"
			CASE biS_BombTimerExpired           RETURN "biS_BombTimerExpired"
			CASE biS_FirstBombDefusalInitiated  RETURN "biS_FirstBombDefusalInitiated"
			CASE biS_BombTimerStarted			RETURN "biS_BombTimerStarted"
			CASE biS_CleanupHeli				RETURN "biS_CleanupHeli"
			CASE biS_GangChaseDelayTimerUp      RETURN "biS_GangChaseDelayTimerUp"
			
			
			
			//CASE biS_GangChaseStarted			RETURN "biS_GangChaseStarted"
		//	CASE biS_GangChaseTextSent			RETURN "biS_GangChaseTextSent"			
			
		ENDSWITCH
		PRINTLN("JS-GET_SERVER_BIT_NAME: ", iBitset)
	RETURN "***INVALID*** "
ENDFUNC
// Get
FUNC BOOL IS_SERVER_BIT_SET(INT bitSet)
        INT iBitSet = (ENUM_TO_INT(bitSet) / 32)
        INT iBitVal = (ENUM_TO_INT(bitSet) % 32)
        RETURN IS_BIT_SET(serverBD.iServerBitSet[iBitSet], iBitVal)
ENDFUNC



// Set
PROC SET_SERVER_BIT(INT bitSet)

        INT iBitSet = (ENUM_TO_INT(bitSet) / 32)
        INT iBitVal = (ENUM_TO_INT(bitSet) % 32) 
        #IF IS_DEBUG_BUILD
        IF NOT IS_BIT_SET(serverBD.iServerBitSet[iBitSet], iBitVal)
                STRING strTemp = GET_SERVER_BIT_NAME(bitSet)
                PRINTLN("[GUNRUN] -  SET_SERVER_BIT - ", strTemp)
        ENDIF
        #ENDIF
        IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
                #IF IS_DEBUG_BUILD
                SCRIPT_ASSERT("[GUNRUN] calling SET_SERVER_BIT on a non host.")
                PRINTLN("")
                #ENDIF
                EXIT
        ENDIF
        SET_BIT(serverBD.iServerBitSet[iBitSet], iBitVal) 
ENDPROC

// Clear
PROC CLEAR_SERVER_BIT(INT bitSet #IF IS_DEBUG_BUILD #ENDIF)
        INT iBitSet = (ENUM_TO_INT(bitSet) / 32)
        INT iBitVal = (ENUM_TO_INT(bitSet) % 32)
        #IF IS_DEBUG_BUILD
        IF IS_BIT_SET(serverBD.iServerBitSet[iBitSet], iBitVal)
                STRING strTemp = GET_SERVER_BIT_NAME(bitSet)
                PRINTLN("[GUNRUN] - CLEAR_SERVER_BIT - ", strTemp)
        ENDIF
        #ENDIF
        IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
                #IF IS_DEBUG_BUILD
                SCRIPT_ASSERT("[GUNRUN] calling CLEAR_SERVER_BIT on a non host.")
                PRINTLN("")
                #ENDIF
                EXIT
        ENDIF
        CLEAR_BIT(serverBD.iServerBitSet[iBitSet], iBitVal)
ENDPROC




PROC SET_SERVER_PROP_BIT(INT iProp, INT iBit)
	IF NOT IS_BIT_SET(serverBD.iServerPropBitset[iProp], iBit)
		SET_BIT(serverBD.iServerPropBitset[iProp], iBit)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [SET_SERVER_PROP_BIT] Set bit ", iBit, " = ", GET_SERVER_PROP_BIT_NAME(iBit))
		#ENDIF
	ENDIF
ENDPROC

PROC CLEAR_SERVER_PROP_BIT(INT iProp, INT iBit)
	IF IS_BIT_SET(serverBD.iServerPropBitset[iProp], iBit)
		CLEAR_BIT(serverBD.iServerPropBitset[iProp], iBit)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [CLEAR_SERVER_PROP_BIT] Clear bit ", iBit, " = ", GET_SERVER_PROP_BIT_NAME(iBit))
		#ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_SERVER_PROP_BIT_SET(INT iProp, INT iBit)
	RETURN IS_BIT_SET(serverBD.iServerPropBitset[iProp], iBit)
ENDFUNC

PROC SET_SERVER_TARGET_BIT(INT iTarget, INT iBit)
	IF NOT IS_BIT_SET(serverBD.Targets[iTarget].iServerTargetBitset, iBit)
		SET_BIT(serverBD.Targets[iTarget].iServerTargetBitset, iBit)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [SET_SERVER_TARGET_BIT] Set bit ", iBit, " = ", GET_SERVER_TARGET_BIT_NAME(iBit))
		#ENDIF
	ENDIF
ENDPROC

PROC CLEAR_SERVER_TARGET_BIT(INT iTarget, INT iBit)
	IF IS_BIT_SET(serverBD.Targets[iTarget].iServerTargetBitset, iBit)
		CLEAR_BIT(serverBD.Targets[iTarget].iServerTargetBitset, iBit)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [CLEAR_SERVER_TARGET_BIT] Clear bit ", iBit, " = ", GET_SERVER_TARGET_BIT_NAME(iBit))
		#ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_SERVER_TARGET_BIT_SET(INT iTarget, INT iBit)
	RETURN IS_BIT_SET(serverBD.Targets[iTarget].iServerTargetBitset, iBit)
ENDFUNC


PROC SET_SERVER_ENEMY_VEH_BIT(INT iTarget, INT iBit)
	IF NOT IS_BIT_SET(serverBD.iEnemyVehBitset[iTarget], iBit)
		SET_BIT(serverBD.iEnemyVehBitset[iTarget], iBit)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [SET_SERVER_ENEMY_VEH_BIT] Set bit ", iBit, " = ", GET_SERVER_ENEMY_VEH_BIT_NAME(iBit), " veh ", iTarget)
		#ENDIF
	ENDIF
ENDPROC

PROC CLEAR_SERVER_ENEMY_VEH_BIT(INT iTarget, INT iBit)
	IF IS_BIT_SET(serverBD.iEnemyVehBitset[iTarget], iBit)
		CLEAR_BIT(serverBD.iEnemyVehBitset[iTarget], iBit)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [CLEAR_SERVER_ENEMY_VEH_BIT] Clear bit ", iBit, " = ", GET_SERVER_ENEMY_VEH_BIT_NAME(iBit))
		#ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_SERVER_ENEMY_VEH_BIT_SET(INT iTarget, INT iBit)
	RETURN IS_BIT_SET(serverBD.iEnemyVehBitset[iTarget], iBit)
ENDFUNC


PROC SET_SERVER_PLAYER_VEH_BIT(INT iTarget, INT iBit)
	IF NOT IS_BIT_SET(serverBD.iPlayerVehBitset[iTarget], iBit)
		SET_BIT(serverBD.iPlayerVehBitset[iTarget], iBit)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [SET_SERVER_PLAYER_VEH_BIT] Set bit ", iBit, " = ", GET_SERVER_PLAYER_VEH_BIT_NAME(iBit))
		#ENDIF
	ENDIF
ENDPROC

PROC CLEAR_SERVER_PLAYER_VEH_BIT(INT iTarget, INT iBit)
	IF IS_BIT_SET(serverBD.iPlayerVehBitset[iTarget], iBit)
		CLEAR_BIT(serverBD.iPlayerVehBitset[iTarget], iBit)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [CLEAR_SERVER_PLAYER_VEH_BIT] Clear bit ", iBit, " = ", GET_SERVER_PLAYER_VEH_BIT_NAME(iBit))
		#ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_SERVER_PLAYER_VEH_BIT_SET(INT iTarget, INT iBit)
	RETURN IS_BIT_SET(serverBD.iPlayerVehBitset[iTarget], iBit)
ENDFUNC


FUNC BOOL DOES_ENEMY_PED_NI_EXIST(INT iPed)
	RETURN (NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[iPed].niPed))
ENDFUNC

FUNC NETWORK_INDEX GET_ENEMY_PED_NI(INT iPed) 
	RETURN serverBD.Targets[iPed].niPed
ENDFUNC

FUNC PED_INDEX GET_ENEMY_PED(INT iPed)
	RETURN NET_TO_PED(GET_ENEMY_PED_NI(iPed))
ENDFUNC

FUNC BOOL DOES_ENEMY_VEH_NI_EXIST(INT iVeh)
	RETURN (NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[iVeh]))
ENDFUNC

FUNC NETWORK_INDEX GET_ENEMY_VEH_NI(INT iVeh) 
	RETURN serverBD.niVeh[iVeh]
ENDFUNC

FUNC VEHICLE_INDEX GET_ENEMY_VEH(INT iVeh)
	RETURN (NET_TO_VEH(GET_ENEMY_VEH_NI(iVeh)))
ENDFUNC

FUNC BOOL DOES_PLAYER_VEH_NI_EXIST(INT iVeh)
	RETURN (NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPlayerVeh[iVeh]))
ENDFUNC

FUNC NETWORK_INDEX GET_PLAYER_VEH_NI(INT iVeh) 
	RETURN serverBD.niPlayerVeh[iVeh]
ENDFUNC

FUNC VEHICLE_INDEX GET_PLAYER_VEH(INT iVeh)
	RETURN (NET_TO_VEH(GET_PLAYER_VEH_NI(iVeh)))
ENDFUNC


///  This function will convert the bombs back to their original prop ID's.  
FUNC INT GB_GUNRUNNING_DEFEND_CONVERT_BOMB_ARRAY_TO_PROP_ARRAY(INT inputArray)
	SWITCH inputArray
		CASE 0 RETURN 6
		CASE 1 RETURN 7
		CASE 2 RETURN 8	
		CASE 3 RETURN 9	
		CASE 4 RETURN 10	
		CASE 5 RETURN 11
		
	ENDSWITCH
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Location of the corresponding 'bomb' for the Defusal variation
/// PARAMS:
///    iBusiness - 
///    iBomb - 
///    vLocation - 
PROC GET_BOMB_LOCATION_DETAILS(INT iBusiness, INT iBomb, VECTOR &bombLocation) 
UNUSED_PARAMETER(iBusiness)
	//SWITCH iBusiness
		//CASE 0
			iBomb = GB_GUNRUNNING_DEFEND_CONVERT_BOMB_ARRAY_TO_PROP_ARRAY(iBomb)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niProp[iBomb])
				bombLocation =	GET_ENTITY_COORDS(NET_TO_ENT(serverBD.niProp[iBomb]))
			ENDIF
		//BREAK
	//ENDSWITCH
ENDPROC


PROC GET_BOMB_PROP(INT iBusiness, INT iBomb, NETWORK_INDEX &bombProp) 
UNUSED_PARAMETER(iBusiness)
	//SWITCH iBusiness
		//CASE 0
			iBomb = GB_GUNRUNNING_DEFEND_CONVERT_BOMB_ARRAY_TO_PROP_ARRAY(iBomb)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niProp[iBomb])
				bombProp =  serverBD.niProp[iBomb]
			ENDIF
		//BREAK
	//ENDSWITCH
ENDPROC

FUNC BOOL IS_LOCAL_PLAYER_NEAR_ANY_BOMB(INT iBusiness, INT &iBombToDefuse, INT iNumberOfBombs = 3)
	IF IS_PED_INJURED(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	INT i
	VECTOR vLoc
	VECTOR playerLoc
	REPEAT iNumberOfBombs i
		GET_BOMB_LOCATION_DETAILS(iBusiness, i, vLoc)
		playerLoc = GET_ENTITY_COORDS(PLAYER_PED_ID())
		PRINTLN("JS-111 - PED LOC : ", playerLoc , " BOMB LOC : ", vloc)
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vLoc) <= 4.0
			iBombToDefuse = i
			PRINTLN("JS-111 NEAR A BOMB")
			RETURN TRUE
		ENDIF
		
	ENDREPEAT
	PRINTLN("JS-111 NOT NEAR A BOMB 2")
	RETURN FALSE
ENDFUNC

//--Tunables

/// PURPOSE:
///    Time limit for the variation, if applicable
/// RETURNS:
///    
FUNC INT GET_GB_GB_GUNRUNNING_DEFEND_DURATION()
	SWITCH serverBD.iVariation
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL	RETURN g_sMPTunables.IGR_DEFEND_DISARM_BOMBS_MISSION_TIME   //g_sMPTunables.iEXEC_DEFEND_TIMER_UNDER_ATTACK
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE	RETURN g_sMPTunables.IGR_DEFEND_VALKYRIE_MISSION_TIME       //g_sMPTunables.iEXEC_DEFEND_TIMER_UNDER_ATTACK
	ENDSWITCH
	RETURN g_sMPTunables.IGR_DEFEND_DISARM_BOMBS_MISSION_TIME
	//RETURN GLOBAL_MISSION_TIME// g_sMPTunables.iEXEC_DEFEND_TIMER_UNDER_ATTACK
ENDFUNC

/// PURPOSE:
///    The length of time before the mode expires, if the player(s) choose to ignore playing the mode
/// RETURNS:
///    
FUNC INT GET_GB_GUNRUNNING_DEFEND_TIMEOUT()
	#IF IS_DEBUG_BUILD
	IF g_iDebugBikerTimeOut != 0
		RETURN g_iDebugBikerTimeOut
	ENDIF
	IF serverbd.missionTimerOverride != -1
		RETURN serverbd.missionTimerOverride 
	ENDIF
	#ENDIF
	
	RETURN GET_GB_GB_GUNRUNNING_DEFEND_DURATION()
	
ENDFUNC

/// PURPOSE:
///    Some variations don't display the time remaining until, for example, the last 5 minutes.
/// RETURNS:
///    How much time should be remaining before displaying the timer on the HUD
FUNC INT GET_GB_GUNRUNNING_DISPLAY_TIMER_TIME()

	RETURN 60000 * 5
ENDFUNC

/// PURPOSE:
///    Time before the contrabvand vehicle will set off, if no player approaches the vehicle beforehand
/// RETURNS:
///    
FUNC INT GET_GB_GUNRUNNING_DEFEND_MAX_TIME_BEFORE_VEHICLE_SETS_OFF()	
	RETURN 60000
ENDFUNC

/// PURPOSE:
///    Time until the player starts to lose content from their business
/// RETURNS:
///    
FUNC INT GET_GB_GUNRUNNING_DEFEND_TIME_BEFORE_START_TO_LOSE_CONTRABAND()
//	RETURN 0
//	RETURN 60000
	SWITCH serverBD.iVariation
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL		RETURN ServerBD.iTimeBeforeLoseContra
		
	ENDSWITCH
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Tunable that limits how much (as a percentage) content the player can lose from their busines if they fail the mission
/// RETURNS:
///    
FUNC FLOAT GET_MAX_LOST_CONTRABAND_PERCENTAGE_FOR_VARIATION()

		RETURN (TO_FLOAT( g_sMPTunables.iEXEC_DEFEND_CONTRABAND_LOST_CAP_UNDER_ATTACK) * 0.01) // Value needs to be between 0 and 1

	
ENDFUNC

/// PURPOSE:
///   	Se the difficulty (max wanted level, AI ped accuracy etc) for the variatio. Based on the values used for the Biker DLC pack
/// RETURNS:
///    
FUNC INT GET_GB_GUNRUNNING_DEFEND_DIFFICULTY()
	INT iDifficulty
	
	SWITCH serverBD.iVariation
		

		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
			iDifficulty = GB_BIKER_CLUBHOUSE_MISSION_DIFFICULTY_EASY
		BREAK
	ENDSWITCH
	
	RETURN iDifficulty
ENDFUNC

FUNC INT GET_MAX_WANTED_LEVEL_FROM_DIFFICULTY()
	INT iDifficulty = GET_GB_GUNRUNNING_DEFEND_DIFFICULTY()
	
	IF iDifficulty = GB_BIKER_CLUBHOUSE_MISSION_DIFFICULTY_HARD
		RETURN g_sMPTunables.iBIKER_DEFEND_HARD_WANTED_CAP
	ELIF iDifficulty = GB_BIKER_CLUBHOUSE_MISSION_DIFFICULTY_MEDIUM
		RETURN g_sMPTunables.iBIKER_DEFEND_MEDIUM_WANTED_CAP 
	ENDIF
	
	RETURN g_sMPTunables.iBIKER_DEFEND_EASY_WANTED_CAP 
ENDFUNC

/// PURPOSE:
///    Where the stolen product needs to be returned to. Will use functions provided by the Technical MP team
/// RETURNS:
///    
FUNC VECTOR GET_DROP_OFF_LOCATION()
	SWITCH serverBD.iVariation
		
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
		FACTORY_ID tempFact
		tempFact =	GET_OWNED_BUNKER(GB_GET_LOCAL_PLAYER_GANG_BOSS())
		//TODO: change  to get_bunker_corona_coords
		RETURN GET_BUNKER_COORDS (tempFact)
//		FREEMODE_DELIVERY_DROPOFFS tempDropoff
//		tempDropoff = GET_FREEMODE_DELIVERY_DROPOFF_FROM_FACTORY_ID(tempFact)
//		IF tempDropoff != FREEMODE_DELIVERY_DROPOFF_INVALID
//			RETURN FREEMODE_DELIVERY_DROPOFF_GET_IN_CAR_COORDS(tempDropoff)
//		ENDIF
		//BREAK
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
		FACTORY_ID tempFact2
		tempFact2 =	GET_OWNED_BUNKER(GB_GET_LOCAL_PLAYER_GANG_BOSS())
	//	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [GET_DROP_OFF_LOCATION] returning ", tempFact)
		RETURN GET_BUNKER_COORDS(tempFact2)
		//GET_FACTORY_COORDS
		
			//RETURN <<2984.8069, 3489.8923, 70.3818>>
	ENDSWITCH
//	RETURN GET_FACTORY_DROP_OFF_COORDS(serverBD.iBusinessToDefend) //GET_FACTORY_DROP_OFF_COORDS(serverBD.iBusinessToDefend)

	RETURN <<0.0, 0.0, 0.0 >>
ENDFUNC
FUNC VECTOR GET_CORONA_LOCATION()
	//IF entering in a vehicle
	//IF entering on foot (player picked up the package)
	VECTOR coronaLocation
	IF DOES_PART_HAVE_ANY_GB_PACKAGE(PARTICIPANT_ID_TO_INT())
		  coronaLocation = FREEMODE_DELIVERY_DROPOFF_GET_ON_FOOT_COORDS(GET_FREEMODE_DELIVERY_DROPOFF_FROM_FACTORY_ID(GET_OWNED_BUNKER(GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID()))))
	ELSE
		  coronaLocation = FREEMODE_DELIVERY_DROPOFF_GET_IN_CAR_COORDS(GET_FREEMODE_DELIVERY_DROPOFF_FROM_FACTORY_ID(GET_OWNED_BUNKER(GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID()))))
	ENDIF
	
	RETURN coronaLocation
ENDFUNC

FUNC VECTOR GET_CONTRA_DEFEND_WAREHOUSE_LOCATION()
	SWITCH serverBD.iBusinessToDefend
		CASE 0 RETURN GET_DROP_OFF_LOCATION() //<<416.2551, 6519.0869, 26.7145>>


	ENDSWITCH
	
	RETURN GET_DROP_OFF_LOCATION()
ENDFUNC

FUNC FLOAT GET_HEALTH_MULTIPLIER()
	RETURN 2.0
ENDFUNC

/// PURPOSE:
///    Set the amount of time that needs to expire after the mission starts before the player will start to lose content from their business
PROC SET_TIME_BEFORE_LOSE_CONTRABAND()
	/*
		AS a quick test, an as-the-crow flies distance of ~2800m (across the city) took about 1min 45s to drive
	*/
	
	IF serverBD.iTimeBeforeLoseContra = 0	
		VECTOR vLoc = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
		VECTOR vWarehouseLoc = GET_CONTRA_DEFEND_WAREHOUSE_LOCATION()
		FLOAT fDistToWarehouse = GET_DISTANCE_BETWEEN_COORDS(vLoc, vWarehouseLoc, FALSE)
		
		IF fDistToWarehouse > 2500
			serverBD.iTimeBeforeLoseContra = g_sMPTunables.iEXEC_DEFEND_GRACE_TIMER_TIER1 //120000
		ELIF fDistToWarehouse > 2000
			serverBD.iTimeBeforeLoseContra = g_sMPTunables.iEXEC_DEFEND_GRACE_TIMER_TIER2 // 96000
		ELIF fDistToWarehouse > 1500
			serverBD.iTimeBeforeLoseContra = g_sMPTunables.iEXEC_DEFEND_GRACE_TIMER_TIER3 //72000
		ELIF fDistToWarehouse > 1000
			serverBD.iTimeBeforeLoseContra = g_sMPTunables.iEXEC_DEFEND_GRACE_TIMER_TIER4 // 60000
		ELSE
			serverBD.iTimeBeforeLoseContra = g_sMPTunables.iEXEC_DEFEND_GRACE_TIMER_TIER5 //45000
		ENDIF
		
	//	serverBD.iTimeBeforeLoseContra = 1000
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [SET_TIME_BEFORE_LOSE_CONTRABAND] serverBD.iTimeBeforeLoseContra = ",serverBD.iTimeBeforeLoseContra, " Dist to warehouse = ", fDistToWarehouse, " Warehouse loc = ", vWarehouseLoc, " Player loc = ", vLoc)
	ENDIF
	
ENDPROC
/// PURPOSE:
///    Sets bits on all enemies that are used for both the Valkyrie and Disarm Bombs variations of Gunrunning Defend Mission.
///    
PROC SET_SERVER_TARGET_GUNRUNNING_DEFEND_BITS()

			SET_SERVER_TARGET_BIT(0, ENEMY_PED_BIT_CREATE_ON_FOOT)
			SET_SERVER_TARGET_BIT(1, ENEMY_PED_BIT_CREATE_ON_FOOT)
			SET_SERVER_TARGET_BIT(2, ENEMY_PED_BIT_CREATE_ON_FOOT)
			SET_SERVER_TARGET_BIT(3, ENEMY_PED_BIT_CREATE_ON_FOOT)
			SET_SERVER_TARGET_BIT(4, ENEMY_PED_BIT_CREATE_ON_FOOT)
			SET_SERVER_TARGET_BIT(5, ENEMY_PED_BIT_CREATE_ON_FOOT)
			SET_SERVER_TARGET_BIT(6, ENEMY_PED_BIT_CREATE_ON_FOOT)
			SET_SERVER_TARGET_BIT(7, ENEMY_PED_BIT_CREATE_ON_FOOT)
			SET_SERVER_TARGET_BIT(8, ENEMY_PED_BIT_CREATE_ON_FOOT)
			SET_SERVER_TARGET_BIT(9, ENEMY_PED_BIT_CREATE_ON_FOOT)
			SET_SERVER_TARGET_BIT(10, ENEMY_PED_BIT_CREATE_ON_FOOT)
			SET_SERVER_TARGET_BIT(11, ENEMY_PED_BIT_CREATE_ON_FOOT)
			
			SET_SERVER_TARGET_BIT(0, ENEMY_PED_BIT_HAS_SCENARIO)
			SET_SERVER_TARGET_BIT(1, ENEMY_PED_BIT_HAS_SCENARIO)
			SET_SERVER_TARGET_BIT(2, ENEMY_PED_BIT_HAS_SCENARIO)
			SET_SERVER_TARGET_BIT(3, ENEMY_PED_BIT_HAS_SCENARIO)
			SET_SERVER_TARGET_BIT(4, ENEMY_PED_BIT_HAS_SCENARIO)
			SET_SERVER_TARGET_BIT(5, ENEMY_PED_BIT_HAS_SCENARIO)
			SET_SERVER_TARGET_BIT(6, ENEMY_PED_BIT_HAS_SCENARIO)
			SET_SERVER_TARGET_BIT(7, ENEMY_PED_BIT_HAS_SCENARIO)
			SET_SERVER_TARGET_BIT(8, ENEMY_PED_BIT_HAS_SCENARIO)
			SET_SERVER_TARGET_BIT(9, ENEMY_PED_BIT_HAS_SCENARIO)
			SET_SERVER_TARGET_BIT(10, ENEMY_PED_BIT_HAS_SCENARIO)
			SET_SERVER_TARGET_BIT(11, ENEMY_PED_BIT_HAS_SCENARIO)
			
		
			
			SET_SERVER_TARGET_BIT(0, ENEMY_PED_BIT_VISION_CONE)
			SET_SERVER_TARGET_BIT(1, ENEMY_PED_BIT_VISION_CONE)
			SET_SERVER_TARGET_BIT(2, ENEMY_PED_BIT_VISION_CONE)
			SET_SERVER_TARGET_BIT(3, ENEMY_PED_BIT_VISION_CONE)
			SET_SERVER_TARGET_BIT(4, ENEMY_PED_BIT_VISION_CONE)
			SET_SERVER_TARGET_BIT(5, ENEMY_PED_BIT_VISION_CONE)
			SET_SERVER_TARGET_BIT(6, ENEMY_PED_BIT_VISION_CONE)
			SET_SERVER_TARGET_BIT(7, ENEMY_PED_BIT_VISION_CONE)
			SET_SERVER_TARGET_BIT(8, ENEMY_PED_BIT_VISION_CONE)
			SET_SERVER_TARGET_BIT(9, ENEMY_PED_BIT_VISION_CONE)
			SET_SERVER_TARGET_BIT(10, ENEMY_PED_BIT_VISION_CONE)
			SET_SERVER_TARGET_BIT(11, ENEMY_PED_BIT_VISION_CONE)
			
			
			SWITCH serverBD.iVariation
				CASE GB_GUNRUNNING_DEFEND_DEFUSAL
				CASE GB_GUNRUNNING_DEFEND_VALKYRIE
					SET_SERVER_TARGET_BIT(0, ENEMY_PED_BIT_USE_DEFENSIVE_AREA)
					SET_SERVER_TARGET_BIT(1, ENEMY_PED_BIT_USE_DEFENSIVE_AREA)
					SET_SERVER_TARGET_BIT(2, ENEMY_PED_BIT_USE_DEFENSIVE_AREA)
					SET_SERVER_TARGET_BIT(3, ENEMY_PED_BIT_USE_DEFENSIVE_AREA)
					SET_SERVER_TARGET_BIT(4, ENEMY_PED_BIT_USE_DEFENSIVE_AREA)
					SET_SERVER_TARGET_BIT(5, ENEMY_PED_BIT_USE_DEFENSIVE_AREA)
					SET_SERVER_TARGET_BIT(6, ENEMY_PED_BIT_USE_DEFENSIVE_AREA)
					SET_SERVER_TARGET_BIT(7, ENEMY_PED_BIT_USE_DEFENSIVE_AREA)
					SET_SERVER_TARGET_BIT(8, ENEMY_PED_BIT_USE_DEFENSIVE_AREA)
					SET_SERVER_TARGET_BIT(9, ENEMY_PED_BIT_USE_DEFENSIVE_AREA)
					SET_SERVER_TARGET_BIT(10, ENEMY_PED_BIT_USE_DEFENSIVE_AREA)
					SET_SERVER_TARGET_BIT(11, ENEMY_PED_BIT_USE_DEFENSIVE_AREA)
				BREAK
			ENDSWITCH
ENDPROC	
/// PURPOSE:
///   Defend-Valkyrie- Gets a random target to be the pilot, instead of having the same ped be the pilot every time.
FUNC INT GB_GUNRUNNING_DEFEND_CHOOSE_RANDOM_PILOT()
	RETURN GET_RANDOM_INT_IN_RANGE(0,3)
ENDFUNC

FUNC FLOAT GB_GUNRUNNING_DEFEND_GET_DIST_BETWEEN_PLAYER_AND_CONTRABAND_VEHICLE(PED_INDEX playerPed)
FLOAT fDistanceBetween = 0

fDistanceBetween = VDIST2(GET_ENTITY_COORDS(playerPed), GET_GB_GUNRUNNING_DEFEND_CONTRABAND_VEHICLE_LOCATION(serverBD.iVariation, serverBD.iBusinessToDefend))
RETURN fDistanceBetween
ENDFUNC

FUNC FLOAT GB_GUNRUNNING_DEFEND_GET_DIST_BETWEEN_PLAYER_AND_BUNKER(PED_INDEX playerPed)
FACTORY_ID tempFact
tempFact =	GET_OWNED_BUNKER(GB_GET_LOCAL_PLAYER_GANG_BOSS())
FLOAT fDistanceBetween = 0

fDistanceBetween = VDIST2(GET_ENTITY_COORDS(playerPed), GET_BUNKER_COORDS(tempFact))

RETURN fDistanceBetween
ENDFUNC

FUNC BOOL GB_GUNRUNNING_DEFEND_ARE_ALL_PLAYERS_AWAY_FROM_BUNKER_AND_CONTRABAND_VEHICLE()
	INT iSlot
	REPEAT GB_GET_MAX_GANG_GOONS() iSlot
        PLAYER_INDEX gangMember = GB_GET_GANG_GOON_AT_INDEX(GB_GET_LOCAL_PLAYER_GANG_BOSS(), iSlot)
        IF gangMember != INVALID_PLAYER_INDEX()   //There is a valid player in this slot
			IF IS_NET_PLAYER_OK(gangMember)
			 	//Get this player's coords and check their distance from the Defend Mission location and bunker
				IF GB_GUNRUNNING_DEFEND_GET_DIST_BETWEEN_PLAYER_AND_BUNKER(GET_PLAYER_PED(gangMember))  < fDistanceFromBunker * fDistanceFromBunker
				AND GB_GUNRUNNING_DEFEND_GET_DIST_BETWEEN_PLAYER_AND_CONTRABAND_VEHICLE(GET_PLAYER_PED(gangMember)) < fDistanceFromContrabandVehicle * fDistanceFromContrabandVehicle
				RETURN FALSE
				ENDIF
			ENDIF
             
        ENDIF
    ENDREPEAT
	
	RETURN TRUE
ENDFUNC
PROC GB_GUNRUNNING_DEFEND_SETUP_CUSTOM_SPAWN_POINTS()
INT i
VECTOR vPos
FLOAT fHeading
	REPEAT MAX_CUSTOM_SPAWN_POINTS i
		GET_GB_GUNRUNNING_DEFEND_CUSTOM_SPAWN_COORDS(serverBD.iVariation,serverBD.iBusinessToDefend,i,vPos,fHeading)
		ADD_CUSTOM_SPAWN_POINT(vPos, fHeading)
	ENDREPEAT
ENDPROC

PROC GB_GUNRUNNING_DEFEND_CHOOSE_MISSION_VARIATION()
	 INT iLocation  = 0

	 //BOOL isLocationOk = FALSE
	// BOOL isLocationOk = FALSE
	 	//if set in the widget, read it from here.
	 	IF GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION2() != -1
			iLocation = GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION2() 
		ENDIF
	SWITCH serverbd.iVariation 
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
			iLocation = GET_RANDOM_INT_IN_RANGE(0, MAX_DEFUSAL_VARIATIONS)
		BREAK
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			iLocation = GET_RANDOM_INT_IN_RANGE(1, MAX_VALKYRIE_VARIATIONS)
		BREAK
	ENDSWITCH

	IF iAttempt < 100 // and if one of the players is <250m away from the 
	AND !GB_GUNRUNNING_DEFEND_ARE_ALL_PLAYERS_AWAY_FROM_BUNKER_AND_CONTRABAND_VEHICLE()
		
		iAttempt++
		GB_GUNRUNNING_DEFEND_CHOOSE_MISSION_VARIATION()
		                                

	ELSE
		serverBD.iBusinessToDefend = iLocation
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [GB_GUNRUNNING_DEFEND_CHOOSE_MISSION_VARIATION] Set serverBD.iBusinessToDefend = ", serverBD.iBusinessToDefend)
		iAttempt = 0
		//serverBD.iBusinessToDefend = 0
	ENDIF


ENDPROC
/// PURPOSE:
///    Figure out which variation to launch
/// RETURNS:
///    
FUNC INT GET_GB_DEFEND_VARIATION()

	
	
	IF serverBD.iVariation != -1
		RETURN serverBD.iVariation
	ENDIF
	

	
	#IF IS_DEBUG_BUILD
		IF MPGlobalsAmbience.iDefendContrabandVar != -1
//			SWITCH MPGlobalsAmbience.iDefendContrabandVar	
//				CASE 0
//					serverBD.iVariation = GB_GUNRUNNING_DEFEND_TEST
//				BREAK
//				
//				CASE 1
//					serverBD.iVariation = GB_GUNRUNNING_DEFEND_STOLEN_VAN 
//	
//				BREAK
//			ENDSWITCH
			serverBD.iVariation = GB_GUNRUNNING_DEFEND_DEFUSAL
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [GET_GB_DEFEND_VARIATION] - FIGURING OUT VARIATION.... FROM DEBUG serverBD.iVariation = ", serverBD.iVariation, " MPGlobalsAmbience.iDefendContrabandVar = ", MPGlobalsAmbience.iDefendContrabandVar)
		ENDIF
	#ENDIF
	
	IF serverBD.iVariation = -1
		serverBD.iVariation = GET_RANDOM_INT_IN_RANGE(0,GB_GUNRUNNING_DEFEND_MAX_VARIATIONS)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [GET_GB_DEFEND_VARIATION] Variation not set, picked random ",serverBD.iVariation)
//		
	ENDIF
	
	IF g_sMPTunables.BGR_DEFEND_VALKYRIE_DISABLE_VALKYRIE    
		serverBD.iVariation = GB_GUNRUNNING_DEFEND_DEFUSAL
	ELIF g_sMPTunables.BGR_DEFEND_DISARM_BOMBS_DISABLE_DISARM_BOMBS  
		serverBD.iVariation = GB_GUNRUNNING_DEFEND_VALKYRIE
	ENDIF

	

	
	
	// This stores the variation to launch when chosen from the 'm' debug menu
	INT iVar = GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION() 
	
	IF iVar != -1
	AND iVar < GB_GUNRUNNING_DEFEND_MAX_VARIATIONS
		serverBD.iVariation = iVar
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [GET_GB_DEFEND_VARIATION] Set serverBD.iVariation = ", serverBD.iVariation, " from GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION = ")
	ENDIF
	
	IF serverBD.iVariation = 0
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [GET_GB_DEFEND_VARIATION]  serverBD.iVariation = ", serverBD.iVariation, " = VALKYRIE = ")
	ELIF serverBD.iVariation = 1
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [GET_GB_DEFEND_VARIATION]  serverBD.iVariation = ", serverBD.iVariation, " = DEFUSAL = ")
		
	ENDIF

	//hardcoded valkyrie variation.
	//-- WIll need to store in server BD which business we are defending
	
		
	
	
//	IF GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION2() != -1
//	PRINTLN("JS-CALLED1: ", GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION2())
//		serverBD.iBusinessToDefend = GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION2()
//	ELSE
//	PRINTLN("JS-CALLED2")
//		serverBD.iBusinessToDefend = GET_RANDOM_INT_IN_RANGE(0,3)
//	ENDIF

	//TODO: SETUP NON-HARDCODED WAY OF SELECTING MISSIONS.
	serverBD.iBusinessToDefend = hardcodeBusinessToDefend
	GB_GUNRUNNING_DEFEND_CHOOSE_MISSION_VARIATION()
	#IF IS_DEBUG_BUILD
		//SWITCH serverBD.iVariation
			//CASE GB_GUNRUNNING_DEFEND_VALKYRIE
				IF GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION2() != -1
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     JS-DDD 0-  iWidgetMissionSubvariation = ", GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION2()  )
					serverBD.iBusinessToDefend = GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION2() 
				ENDIF
		//	BREAK
//			CASE GB_GUNRUNNING_DEFEND_DEFUSAL
//				IF MPGlobalsAmbience.sMagnateGangBossData.iWidgetMissionSubvariation != -1
//						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     JS-DDD 0-  iWidgetMissionSubvariation = ", GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION2()  )
//					serverBD.iBusinessToDefend = GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION2() 
//				ENDIF
//			BREAK
		//ENDSWITCH
		
	#ENDIF

	
	//INT iNumPeds
	INT i
	
	//-- Set some flags on each enemy ped which will determine that AI ped's behaviour for the variation.
	
	SWITCH serverBD.iVariation
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
		//	serverBD.iWavesRemaining = GET_NUMBER_OF_WAVES_FOR_GB_GUNRUNNING_DEFEND(serverBD.iVariation, serverBD.iBusinessToDefend)
			//setting the bomb props to attach to the vehicl
			SET_SERVER_PROP_BIT(6, PROP_BIT_ATTACH_TO_VEH)
			SET_SERVER_PROP_BIT(7, PROP_BIT_ATTACH_TO_VEH)
			SET_SERVER_PROP_BIT(8, PROP_BIT_ATTACH_TO_VEH)
			SET_SERVER_PROP_BIT(9, PROP_BIT_ATTACH_TO_VEH)
			SET_SERVER_PROP_BIT(10, PROP_BIT_ATTACH_TO_VEH)
			SET_SERVER_PROP_BIT(11, PROP_BIT_ATTACH_TO_VEH)
			SET_SERVER_PROP_BIT(9, PROP_BIT_BOMB_DISARMED_PROP)
			SET_SERVER_PROP_BIT(10, PROP_BIT_BOMB_DISARMED_PROP)
			SET_SERVER_PROP_BIT(11, PROP_BIT_BOMB_DISARMED_PROP)
			
			
			//iNumPeds = GET_GB_GUNRUNNING_DEFEND_NUMBER_OF_INITIAL_PEDS_FOR_VARIATION(serverBD.iVariation, serverBD.iBusinessToDefend)
			REPEAT MAX_TARGETS i
			
				SET_SERVER_TARGET_BIT(i, ENEMY_PED_BIT_DEFEND_START_LOC)  
				SET_SERVER_TARGET_BIT(i, ENEMY_PED_BIT_CREATE_ON_FOOT)
			ENDREPEAT
			
			i = 0
			REPEAT MAX_BOMBS i
				serverBD.iBombDefusedByPlayer[i] = -1
				serverBD.iPlayerCanDefuseBomb[i] = -1
			ENDREPEAT
			
			i = 0
			REPEAT GET_GB_GUNRUNNING_DEFEND_NUMBER_OF_INITIAL_ENEMY_VEHS_FOR_VARIATION(serverBD.iVariation, serverBD.iBusinessToDefend) i
				SET_SERVER_ENEMY_VEH_BIT(i, ENEMY_VEH_BIT_USE_EXACT_COORDS)
			ENDREPEAT
			
			SET_SERVER_TARGET_GUNRUNNING_DEFEND_BITS()
			//-- lock rear doors on bomb lorry
			SET_SERVER_PLAYER_VEH_BIT(0, PLAYER_VEH_BIT_PREVENT_OPEN_BY_EXPLOSION)
			SET_SERVER_PLAYER_VEH_BIT(0, PLAYER_VEH_BIT_OPEN_REAR_RIGHT_DOOR)
			SET_SERVER_PLAYER_VEH_BIT(0, PLAYER_VEH_BIT_OPEN_REAR_LEFT_DOOR)
			PRINTLN("JS-PREVENTOPEN")
		BREAK
		
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			SET_SERVER_PROP_BIT(7, PROP_BIT_ATTACH_TO_VEH)
			
			
			i = 0
			REPEAT GET_GB_GUNRUNNING_DEFEND_NUMBER_OF_INITIAL_ENEMY_VEHS_FOR_VARIATION(serverBD.iVariation, serverBD.iBusinessToDefend) i
				SET_SERVER_ENEMY_VEH_BIT(i, ENEMY_VEH_BIT_USE_EXACT_COORDS)
			ENDREPEAT
			
			SET_SERVER_TARGET_GUNRUNNING_DEFEND_BITS()
			//switch
			//case 0
			//
			//TODO: Randomise pilot
			SET_SERVER_TARGET_BIT(GB_GUNRUNNING_DEFEND_CHOOSE_RANDOM_PILOT(), ENEMY_PED_HELI_PILOT)
		BREAK
		
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	VECTOR vLoc = GET_CONTRA_DEFEND_WAREHOUSE_LOCATION()
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [GET_GB_DEFEND_VARIATION] - FIGURING OUT VARIATION.... serverBD.iVariation = ", serverBD.iVariation, " serverBD.iBusinessToDefend = ", ENUM_TO_INT(serverBD.iBusinessToDefend), " serverBD.iStartingContraband = ", serverBD.iStartingContraband, " Warehouse location = ", vLoc, " serverBD.iNumberOfPackages = ", serverBD.iNumberOfPackages)
	#ENDIF
	
	RETURN serverBD.iVariation
ENDFUNC



FUNC INT GET_NUMBER_OF_PACKAGES_MY_GANG_DELIVERED()
	INT i
	INT iPackages = serverBD.iNumberOfPackages //GET_GB_GUNRUNNING_DEFEND_NUMBER_OF_PACKAGES_FOR_VARIATION(serverBD.iVariation, serverBD.iBusinessToDefend)
	INT iPlayer
	INT iMyTeamDeliveredCount
	PLAYER_INDEX player
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] [GET_NUMBER_OF_PACKAGES_MY_GANG_DELIVERED]......")
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] [GET_NUMBER_OF_PACKAGES_MY_GANG_DELIVERED]		This many packages: ", iPackages)

	
	REPEAT iPackages i
		IF HAS_GB_PACKAGE_BEEN_DELIVERED(serverBD.packagesServer, i, iPlayer)
			player = INT_TO_PLAYERINDEX(iPlayer)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] [GET_NUMBER_OF_PACKAGES_MY_GANG_DELIVERED]			Package ", i, " was delivered by ", GET_PLAYER_NAME(player))
			IF player = PLAYER_ID()
				iMyTeamDeliveredCount++
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] [GET_NUMBER_OF_PACKAGES_MY_GANG_DELIVERED]				I delivered! iMyTeamDeliveredCount = ", iMyTeamDeliveredCount)
			ELIF GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG(PLAYER_ID(), player)
				iMyTeamDeliveredCount++
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] [GET_NUMBER_OF_PACKAGES_MY_GANG_DELIVERED]				on my team! iMyTeamDeliveredCount = ", iMyTeamDeliveredCount)
			ELSE
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] [GET_NUMBER_OF_PACKAGES_MY_GANG_DELIVERED]				NOT on my team! iMyTeamDeliveredCount = ", iMyTeamDeliveredCount)
			ENDIF
		ENDIF
	ENDREPEAT
	
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] [GET_NUMBER_OF_PACKAGES_MY_GANG_DELIVERED] 		Total delivered by my team = ", iMyTeamDeliveredCount)
	
	RETURN iMyTeamDeliveredCount
	
	
ENDFUNC

FUNC BOOL SHOULD_SHUT_DOWN_BUSINESS()
//	IF serverBD.iVariation = GB_GUNRUNNING_DEFEND_GLOBAL
//		IF IS_BIT_SET(serverBD.iServerBitSet, biS_TimeoutExpired)
//		OR IS_BIT_SET(serverBD.iServerBitSet, biS_ContrabandWrecked)
//		OR IS_BIT_SET(serverBD.iServerBitSet, biS_AllKilledByAi)
//			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] [SHOULD_SHUT_DOWN_BUSINESS] TRUE - GB_GUNRUNNING_DEFEND_GLOBAL")
//			RETURN TRUE
//		ENDIF
//	ELIF serverBD.iVariation = GB_GUNRUNNING_DEFEND_COKE
//		IF IS_BIT_SET(serverBD.iServerBitSet, biS_TimeoutExpired) 
//			IF GET_NUMBER_OF_PACKAGES_MY_GANG_DELIVERED() = 0
//				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] [SHOULD_SHUT_DOWN_BUSINESS] TRUE - CRACK - biS_TimeoutExpired")
//				RETURN TRUE
//			ENDIF
//		ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_AllKilledByAi)
//			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] [SHOULD_SHUT_DOWN_BUSINESS] TRUE - CRACK - biS_AllKilledByAi")
//			RETURN TRUE
//		ENDIF
//	ENDIF
//	
//	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] [SHOULD_SHUT_DOWN_BUSINESS] FALSE")
//	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns the perscentage of product to remove from the business, expressed as a value between 0.0 and 1.0
/// RETURNS:
///    
FUNC FLOAT GET_BUSINESS_STOCK_AMOUNT_TO_LOSE()
	RETURN serverBD.fAmountOfProductToLose
ENDFUNC


PROC REMOVE_DEFEND_CONTRABAND_FROM_WAREHOUSE(FLOAT fPercent)
//	SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ContrabandRemovedAtEnd)
//	UNUSED_PARAMETER(fpercent)
	//ILLICIT_GOODS_MISSION_DATA eData = GB_GET_PLAYER_ILLICIT_GOOD_MISSION_DATA()
	
	FACTORY_ID ownedBunker =  GET_OWNED_BUNKER(PLAYER_ID())
	INT iAmountInWarehouse =  GET_PRODUCT_TOTAL_FOR_FACTORY(PLAYER_ID(),ownedBunker)
	INT iAmountToRemove
	IF iAmountInWarehouse > 0
		//IF NOT SHOULD_SHUT_DOWN_BUSINESS()
			iAmountToRemove = ROUND(TO_FLOAT(iAmountInWarehouse) * fPercent)
			// GET PRODUCT TOTAL
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [REMOVE_DEFEND_CONTRABAND_FROM_WAREHOUSE] iAmountInWarehouse = ", iAmountInWarehouse, " iAmountToRemove = ", iAmountToRemove, " fPercent = ", fPercent)

			
			REMOVE_PRODUCT_FROM_FACTORY(ownedBunker, iAmountToRemove, 0, FALSE, REMOVE_CONTRA_ATTACKED, eResult)

			//REMOVE_CONTRABAND_FROM_WAREHOUSE(iWarheouseToUse, iAmountToRemove, 0, REMOVE_CONTRA_ATTACKED, eResult)
		//ELSE
		//	RESET_FACTORY_TO_JUST_PURCHASED_STATE(ownedBunker)
		//	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [REMOVE_DEFEND_CONTRABAND_FROM_WAREHOUSE] NOT REMOVING AS SHOULD_SHUT_DOWN_BUSINESS ")
		//	SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ContrabandRemovedAtEnd)
		//ENDIF
	ELSE
		
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [REMOVE_DEFEND_CONTRABAND_FROM_WAREHOUSE] NOT REMOVING AS iAmountInWarehouse = ", iAmountInWarehouse, "FACTORY ID: ", ownedBunker )
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ContrabandRemovedAtEnd)
	ENDIF
ENDPROC

PROC HANDLE_LOST_CONTRABAND()
		FLOAT fPercentToRemove
	//	IF HAS_NET_TIMER_STARTED(serverBD.timeDuration)
	//	OR IS_BIT_SET(serverBD.iServerBitSet, biS_ContrabandWrecked)
			fPercentToremove = GET_BUSINESS_STOCK_AMOUNT_TO_LOSE()
			

			fPercentToRemove *= g_sMPTunables.fbiker_defend_product_taken_on_fail
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [REMOVE_DEFEND_CONTRABAND_FROM_WAREHOUSE] Adjusting fPercentToRemove as g_sMPTunables.fbiker_defend_product_taken_on_fail = ", g_sMPTunables.fbiker_defend_product_taken_on_fail, " new fPercentToRemove = ", fPercentToRemove)
			
			
			REMOVE_DEFEND_CONTRABAND_FROM_WAREHOUSE(fPercentToremove)
			
	//	ELSE
	//		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [HANDLE_LOST_CONTRABAND] Not removing contraband as timeDuration not started")
	//	ENDIF
	
ENDPROC

FUNC INT GET_GB_DEFEND_GANG_VARIATION()
	IF serverBD.iGangVariation != -1
		RETURN serverBD.iGangVariation
	ENDIF
	
	serverBD.iGangVariation = GET_RANDOM_INT_IN_RANGE(0, 3)
	
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND USING serverBD.iGangVariation = ", serverBD.iGangVariation)
	
	RETURN serverBD.iGangVariation
ENDFUNC






FUNC BOOL HAS_MISSION_END_CONDITION_BEEN_MET()

	//RETURN ( IS_SERVER_BIT_SET( biS_AllTargetsKilled)	// For varuations where players have to just kill AI peds 
	RETURN (IS_SERVER_BIT_SET(biS_DurationExpired)			// Mission time limit expired (DEFUSAL)
	OR IS_SERVER_BIT_SET(biS_VehDelivered)				// Contraband vehicle successfully returned to the business
	OR IS_SERVER_BIT_SET(biS_BossLaunchedQuit)			// The launch boss has quit the session
	OR IS_SERVER_BIT_SET(biS_ContrabandWrecked)			// The contraband vehicle / package was destroyed before being returned
	OR IS_SERVER_BIT_SET(biS_ContrabandLost)			// Lost track of the contaband	
	OR IS_SERVER_BIT_SET(biS_AllPackagesDelivered)		// The contraband package(s) were sucessfully returned to the business
	OR IS_SERVER_BIT_SET(biS_TimeoutExpired))			// The mission timeout expired. (VALKYRIE)
	//OR IS_SERVER_BIT_SET(biS_AllBombsDefused))			// All the bombs were successfuly defused
ENDFUNC

PROC CLEANUP_DEFEND_CONTRABAND_SPAWNING()
	IF IS_BIT_SET(iBoolsBitSet, biL_SetupSpawnCoord)
//		CLEAR_PLAYER_NEXT_RESPAWN_LOCATION()
//		CLEAR_SPECIFIC_SPAWN_LOCATION_FROM_THIS_SCRIPT()
		
		CLEAR_CUSTOM_SPAWN_POINTS()
		USE_CUSTOM_SPAWN_POINTS(FALSE)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [CLEANUP_DEFEND_CONTRABAND_SPAWNING] CALLED")
		
		CLEAR_BIT(iBoolsBitSet, biL_SetupSpawnCoord)
	ENDIF
ENDPROC
//



/// PURPOSE:
///    hand custom respawn points for players
PROC MAINTAIN_DEFEND_CONTRABAND_PLAYER_SPAWNING()
	
	IF NOT DOES_VARIATION_USE_CUSTOM_SPAWN_POINTS(serverBD.iVariation)
		EXIT
	ENDIF

	IF HAS_MISSION_END_CONDITION_BEEN_MET()
		EXIT
	ENDIF
	
	//-- DOn't use custom spanw points if in an interior
	IF IS_BIT_SET(iBoolsBitSet, biL_SetupSpawnCoord)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) != NULL
				CLEANUP_DEFEND_CONTRABAND_SPAWNING()
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_DEFEND_CONTRABAND_PLAYER_SPAWNING] CLEANUP_DEFEND_CONTRABAND_SPAWNING as in interior")
			ENDIF
		ENDIF
	ENDIF
	
	//-- Only use custom spawn points when near the business
	IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), GET_GB_GUNRUNNING_DEFEND_CONTRABAND_VEHICLE_LOCATION(serverBD.iVariation, serverBD.iBusinessToDefend)) <= 100
	AND IS_SERVER_BIT_SET(biS_EnemiesAlerted)
	//within 100m
		IF NOT IS_BIT_SET(iBoolsBitSet, biL_SetupSpawnCoord)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = NULL
					USE_CUSTOM_SPAWN_POINTS(TRUE)
					GB_GUNRUNNING_DEFEND_SETUP_CUSTOM_SPAWN_POINTS()
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_DEFEND_CONTRABAND_PLAYER_SPAWNING]- CUSTOM SPAWN POINTS SETUP ")
					
					SET_BIT(iBoolsBitSet, biL_SetupSpawnCoord)
				ENDIF
			ENDIF
			
		ENDIF
	ELSE
			CLEANUP_DEFEND_CONTRABAND_SPAWNING()
		
	ENDIF
ENDPROC


FUNC PLAYER_INDEX GET_PLAYER_DRIVING_CONTRABAND_VEHICLE()
	PLAYER_INDEX playerDriving = INVALID_PLAYER_INDEX()
	IF serverBD.iPlayerDrivingContraVehicle > -1
		IF NETWORK_IS_PLAYER_ACTIVE(INT_TO_PLAYERINDEX(serverBD.iPlayerDrivingContraVehicle))
			playerDriving = INT_TO_PLAYERINDEX(serverBD.iPlayerDrivingContraVehicle)
		ENDIF
	ENDIF
	
	RETURN playerDriving
ENDFUNC

FUNC PLAYER_INDEX GET_LAST_PLAYER_DRIVING_CONTRABAND_VEHICLE()
	PLAYER_INDEX playerDriving = INVALID_PLAYER_INDEX()
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  [GET_LAST_PLAYER_DRIVING_CONTRABAND_VEHICLE] Called iLocalPartDrivingContraVehicle = ", iLocalPartDrivingContraVehicle)
	IF iLocalPartDrivingContraVehicle < 0
		RETURN playerDriving
	ENDIF
	
	PLAYER_INDEX player = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iLocalPartDrivingContraVehicle))
	IF NETWORK_IS_PLAYER_ACTIVE(player)
		playerDriving = player
	ENDIF
	
	RETURN playerDriving
ENDFUNC

/// PURPOSE:
///    The player we are specating as a Participnat int
/// RETURNS:
///    
FUNC INT GET_MY_SPEC_TARGET_AS_PARTICIPANT_INT()
	INT iPart = -1
	PLAYER_INDEX playerSPec = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget
	PARTICIPANT_INDEX partSpec
	
	IF playerSPec != INVALID_PLAYER_INDEX()
		IF NETWORK_IS_PLAYER_ACTIVE(playerSPec)
			IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerSPec)
				partSpec = NETWORK_GET_PARTICIPANT_INDEX(playerSPec)
				iPart = NATIVE_TO_INT(partSpec)
			ENDIF
		ENDIF
	ENDIF
	
	RETURN iPart
ENDFUNC

/// PURPOSE:
///    The player we are spectating as a Player Index
/// RETURNS:
///    
FUNC PLAYER_INDEX GET_MY_GBTV_SPEC_TARGET_AS_PLAYER()
	PLAYER_INDEX playerSpec = INVALID_PLAYER_INDEX()
	PED_INDEX specTargetPed = GET_SPECTATOR_SELECTED_PED()
	IF specTargetPed != NULL
		IF IS_PED_A_PLAYER(specTargetPed)
			playerSpec = NETWORK_GET_PLAYER_INDEX_FROM_PED(specTargetPed)
		ENDIF
	ENDIF
	
	RETURN playerSpec
ENDFUNC



FUNC BOOL SHOULD_LOCAL_PLAYER_SEE_GB_GB_GUNRUNNING_DEFEND_HUD()
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
		IF NOT MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI 
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [SHOULD_LOCAL_PLAYER_SEE_GB_GB_GUNRUNNING_DEFEND_HUD] FALSE - GB_IS_IT_SAFE_TO_DISPLAY_BOSS_MISSION_UI")
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HudOkForSCTV)
				CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HudOkForSCTV)
			ENDIF
			RETURN FALSE
		ENDIF
		
		IF GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - SHOULD_LOCAL_PLAYER_SEE_GB_GB_GUNRUNNING_DEFEND_HUD - FALSE AS GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE")
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HudOkForSCTV)
				CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HudOkForSCTV)
			ENDIF
			RETURN FALSE
		ENDIF
		
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HudOkForSCTV)
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HudOkForSCTV)
		ENDIF
		
	ELSE
		//-- SCTV player
		INT iPart = GET_MY_SPEC_TARGET_AS_PARTICIPANT_INT()
		IF iPart = -1
			RETURN FALSE
		ENDIF
		
		IF NOT IS_BIT_SET(playerBD[iPart].iPlayerBitSet, biP_HudOkForSCTV)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC



/// PURPOSE:
///    Store my boss's participant ID in playerBD, so I don't have to calculate it every time
FUNC INT GET_MY_BOSS_PARTICPANT_ID()
	INT iPart = PARTICIPANT_ID_TO_INT()
	PLAYER_INDEX player = PLAYER_ID()
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
		iPart = GET_MY_SPEC_TARGET_AS_PARTICIPANT_INT()
		player = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget
	ENDIF
	IF iPart > -1
		IF playerBD[iPart].iMyBossPart != -1
			//-- Already stored
			RETURN playerBD[iPart].iMyBossPart
		ENDIF
		
		PLAYER_INDEX playerBoss
		
		IF GB_IS_PLAYER_BOSS_OF_A_GANG(player)
			//-- I'm a a Boss, store my participant ID
			playerBD[iPart].iMyBossPart = iPart
			IF iPart = serverBD.iBossPartWhoLaunched	
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [GET_MY_BOSS_PARTICPANT_ID] SETTING MYSELF CRITICAL AS I'M THE BOSS WHO LAUNCHED")
				GB_SET_LOCAL_PLAYER_CRITICAL_TO_JOB(TRUE)
			ENDIF
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [GET_MY_BOSS_PARTICPANT_ID] SETTING iMyBossPart TO ", PARTICIPANT_ID_TO_INT(), " (MYSELF) AS I'M A BOSS")
		ELSE
			//-- I'm not a boss
			IF GB_IS_PLAYER_MEMBER_OF_A_GANG(player)
				playerBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
				IF playerBoss != INVALID_PLAYER_INDEX()
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [GET_MY_BOSS_PARTICPANT_ID] THINK MY BOSS IS ", GET_PLAYER_NAME(playerBoss))
					IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerBoss)
						playerBD[iPart].iMyBossPart = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(playerBoss))
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [GET_MY_BOSS_PARTICPANT_ID] SETTING iMyBossPart TO ", playerBD[PARTICIPANT_ID_TO_INT()].iMyBossPart, " WHO IS PLAYER ",GET_PLAYER_NAME(playerBoss))
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		RETURN playerBD[iPart].iMyBossPart
	ENDIF
	
	RETURN -1
	
ENDFUNC

PROC INIT_MY_GANG_COLOUR()
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [INIT_MY_GANG_COLOUR] CALLED")
//	INT iTempGangID = GET_GANG_ID_FOR_PLAYER(PLAYER_ID())
//	hcMyGang = GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID())	
ENDPROC

/// PURPOSE:
///    Determine if it was my boss that launched the assault Boss Work
FUNC BOOL DID_MY_BOSS_LAUNCH_GB_GB_GUNRUNNING_DEFEND()
	INT iMyBossPart = GET_MY_BOSS_PARTICPANT_ID()
	IF iMyBossPart > -1
		IF serverBD.iBossPartWhoLaunched = iMyBossPart
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Determine if the model specified is classed as a cop. Useful for determining if a cop has been killed by the players
/// PARAMS:
///    mn - 
/// RETURNS:
///    
FUNC BOOL IS_MODEL_AMBIENT_COP(MODEL_NAMES mn)
	SWITCH mn
		CASE S_M_Y_COP_01
		CASE S_F_Y_COP_01
		CASE S_M_Y_SWAT_01
		CASE S_M_M_FIBOFFICE_01
		CASE S_M_Y_SHERIFF_01
		CASE S_F_Y_SHERIFF_01
		CASE S_M_Y_Ranger_01
		CASE S_F_Y_Ranger_01
	//	CASE S_M_Y_BlackOps_01 Ignore mercs
		CASE S_M_M_ARMOURED_01
		CASE S_M_Y_Pilot_01
		CASE S_M_M_FIBSEC_01
			RETURN TRUE
		BREAK
		
	ENDSWITCH
	RETURN FALSE
ENDFUNC

 
PROC PROCESS_GB_GUNRUNNING_DEFEND_DAMAGE_EVENT(INT iCount)

	STRUCT_ENTITY_DAMAGE_EVENT sei 

	PED_INDEX pedKiller
	PED_INDEX pedTemp

	PLAYER_INDEX playerKiller
	
	
//	SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerData
	


	INT iTarget

	
	// Grab the event data.
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sei, SIZE_OF(sei))
	
		/// Determine if any enemy vehicle has been damaged by anyone
		REPEAT MAX_ENEMY_VEH itarget
			IF DOES_ENTITY_EXIST(sei.VictimIndex)				
				IF IS_ENTITY_A_VEHICLE(sei.VictimIndex)		
					IF NOT IS_BIT_SET(serverBD.iTargetVehDamagedBitSet, iTarget)
						IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetVehDamagedBitSet, iTarget)
							IF DOES_ENEMY_VEH_NI_EXIST(itarget)
								IF DOES_ENTITY_EXIST(GET_ENEMY_VEH(itarget))						
									IF GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sei.VictimIndex) = GET_ENEMY_VEH(itarget)
										IF DOES_ENTITY_EXIST(sei.DamagerIndex)
											IF IS_ENTITY_A_PED(sei.DamagerIndex)							
												IF GET_PED_INDEX_FROM_ENTITY_INDEX(sei.DamagerIndex) = PLAYER_PED_ID()
													IF GET_MY_BOSS_PARTICPANT_ID() = serverBD.iBossPartWhoLaunched
														SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetVehDamagedBitSet, iTarget)
														CPRINTLN(DEBUG_NET_MAGNATE,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_GB_GUNRUNNING_DEFEND_DAMAGE_EVENT] I DAMAGED THIS TARGET VEHICLE = ", iTarget)
													ENDIF
												ELSE
													IF IS_PED_A_PLAYER(GET_PED_INDEX_FROM_ENTITY_INDEX(sei.DamagerIndex))
														PLAYER_INDEX playerId = NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_INDEX_FROM_ENTITY_INDEX(sei.DamagerIndex))
														IF NOT NETWORK_IS_PLAYER_A_PARTICIPANT(playerId)
															SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetVehDamagedBitSet, iTarget)
															CPRINTLN(DEBUG_NET_MAGNATE,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_GB_GUNRUNNING_DEFEND_DAMAGE_EVENT] NON-PARTICIPANT ", GET_PLAYER_NAME(playerId), " DAMAGED THE TARGET VEHICLE = ", iTarget, ". SETTING THE BIT LOCALLY.")
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
										
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		
		
		
		
		
		iTarget = 0
		
		//-- Determine if any AI enemy has been damaged / killed by the players
		REPEAT MAX_TARGETS iTarget
			IF NOT IS_BIT_SET(serverBD.iTargetKilledBitset, iTarget)
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerKilledTargetBitset, iTarget)
					IF DOES_ENTITY_EXIST(sei.VictimIndex)				

						IF IS_ENTITY_A_PED(sei.VictimIndex)
							pedTemp = GET_PED_INDEX_FROM_ENTITY_INDEX(sei.VictimIndex)
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[iTarget].niPed)
								IF pedTemp = NET_TO_PED(serverBD.Targets[iTarget].niPed)
									IF DOES_ENTITY_EXIST(sei.DamagerIndex)
										IF IS_ENTITY_A_PED(sei.DamagerIndex)	
											pedKiller = GET_PED_INDEX_FROM_ENTITY_INDEX(sei.DamagerIndex)				
											IF IS_PED_A_PLAYER(pedKiller)
												playerKiller = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedKiller)
												CPRINTLN(DEBUG_NET_MAGNATE,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_GB_GUNRUNNING_DEFEND_DAMAGE_EVENT] THIS PLAYER ", GET_PLAYER_NAME(playerKiller), " HAS DAMAGED TARGET PED ", iTarget)
												IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerKiller)
													CPRINTLN(DEBUG_NET_MAGNATE,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_GB_GUNRUNNING_DEFEND_DAMAGE_EVENT] THIS PLAYER ", GET_PLAYER_NAME(playerKiller), " HAS DAMAGED TARGET PED ", iTarget, " AND IS A PART")
													
													IF sei.VictimDestroyed
														CPRINTLN(DEBUG_NET_MAGNATE,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_GB_GUNRUNNING_DEFEND_DAMAGE_EVENT] THIS PLAYER ", GET_PLAYER_NAME(playerKiller), " HAS KILLED TARGET PED ", iTarget)
														IF playerKiller = PLAYER_ID()
															CPRINTLN(DEBUG_NET_MAGNATE,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_GB_GUNRUNNING_DEFEND_DAMAGE_EVENT] I HAVE KILLED TARGET PED ", iTarget)
															SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerKilledTargetBitset, iTarget)
															
															
															//	TickerData.TickerEvent = TICKER_EVENT_GB_DEFEND_KILLED
															//	TickerData.playerID = PLAYER_ID()
															//	BROADCAST_TICKER_EVENT(TickerData,ALL_PLAYERS_ON_SCRIPT(FALSE))
															
														ENDIF
													ELSE
														//Did the ped take a bullet and survive? Set all to spooked.
														SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iEnemyPedSpookedClient, iTarget)
													ENDIF
												ELSE 
													IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iNonPartPlayerKilledTargetBitset, iTarget)
														IF sei.VictimDestroyed
															CPRINTLN(DEBUG_NET_MAGNATE,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_GB_GUNRUNNING_DEFEND_DAMAGE_EVENT] THIS NON-PART PLAYER ", GET_PLAYER_NAME(playerKiller), " HAS KILLED TARGET PED ", iTarget, " WILL SET iNonPartPlayerKilledTargetBitset")
															SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iNonPartPlayerKilledTargetBitset, iTarget)
															
														ENDIF
													ENDIF
												ENDIF
											ELSE
												IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iNonPlayerKilledTargetBitset, iTarget)
													CPRINTLN(DEBUG_NET_MAGNATE,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_GB_GUNRUNNING_DEFEND_DAMAGE_EVENT] THINK THIS TARGET ", iTarget, " HAS BEEN DAMAGED BY A NON-PLAYER!")
													IF sei.VictimDestroyed
														CPRINTLN(DEBUG_NET_MAGNATE,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_GB_GUNRUNNING_DEFEND_DAMAGE_EVENT] THINK THIS TARGET ", iTarget, " HAS BEEN KILLED BY A NON-PLAYER! WILL SET iNonPlayerKilledTargetBitset")
														SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iNonPlayerKilledTargetBitset, iTarget)
													ENDIF
												ENDIF
											ENDIF
										ELSE
											CPRINTLN(DEBUG_NET_MAGNATE,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_GB_GUNRUNNING_DEFEND_DAMAGE_EVENT] THINK THIS TARGET ", iTarget, " HAS BEEN DAMAGED BY SOMETHING OTHER THAN A PED!")
											IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iNonPlayerKilledTargetBitset, iTarget)
												CPRINTLN(DEBUG_NET_MAGNATE,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_GB_GUNRUNNING_DEFEND_DAMAGE_EVENT] THINK THIS TARGET ", iTarget, " HAS BEEN DAMAGED BY A NON-PED!")
												IF sei.VictimDestroyed
													CPRINTLN(DEBUG_NET_MAGNATE,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_GB_GUNRUNNING_DEFEND_DAMAGE_EVENT] THINK THIS TARGET ", iTarget, " HAS BEEN KILLED BY A NON-PED! WILL SET iNonPlayerKilledTargetBitset")
													SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iNonPlayerKilledTargetBitset, iTarget)
												ENDIF
											ENDIF
										ENDIF
									ENDIF					
								ENDIF
							ENDIF
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		
		
		
		
		
		
		
		//-- Wanted rating if a cop is killed / damaged
		IF NOT IS_BIT_SET(iBoolsBitSet, biL_ShouldResetWanted)

			IF DOES_ENTITY_EXIST(sei.VictimIndex)	
				IF IS_ENTITY_A_PED(sei.VictimIndex)
					pedTemp = GET_PED_INDEX_FROM_ENTITY_INDEX(sei.VictimIndex)
					
					IF IS_MODEL_AMBIENT_COP(GET_ENTITY_MODEL(pedTemp))
						SET_BIT(iBoolsBitSet, biL_ShouldResetWanted)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_GB_GUNRUNNING_DEFEND_DAMAGE_EVENT] Set biL_ShouldResetWanted as I attacked a cop!")
					ENDIF
				ELIF IS_ENTITY_A_VEHICLE(sei.VictimIndex)
					IF IS_MODEL_POLICE_VEHICLE(GET_ENTITY_MODEL(sei.VictimIndex))
						IF !IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sei.VictimIndex))
							IF NOT IS_ANY_PLAYER_IN_VEHICLE(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sei.VictimIndex))
								SET_BIT(iBoolsBitSet, biL_ShouldResetWanted)
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_GB_GUNRUNNING_DEFEND_DAMAGE_EVENT] Set biL_ShouldResetWanted as I attacked a cop vehicle!")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
	
	
	
ENDPROC

PROC PROCESS_EVENTS()
	
	INT iCount
	EVENT_NAMES ThisScriptEvent
	STRUCT_EVENT_COMMON_DETAILS Details
	SCRIPT_EVENT_DATA_OK_TO_CLEANUP_GUNRUN_ENTITY sFadeOutData
	
	//process the events
	
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount
		
		ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)
		
		SWITCH ThisScriptEvent
			
			CASE EVENT_NETWORK_DAMAGE_ENTITY
				

				PROCESS_GB_GUNRUNNING_DEFEND_DAMAGE_EVENT(iCount)

				
				
			BREAK
			
			CASE EVENT_NETWORK_SCRIPT_EVENT
				
				GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Details, SIZE_OF(Details))
				
				SWITCH Details.Type	
			
					CASE SCRIPT_EVENT_OK_TO_CLEANUP_DELIVERABLE_ENTITY
					
						IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sFadeOutData, SIZE_OF(sFadeOutData))
						
						
							SET_BIT(playerbd[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_CleanupHeli)
							
							PRINTLN("     ---------->     GB_GUNRUNNING_DEFEND - SCRIPT_EVENT_OK_TO_CLEANUP_DELIVERABLE_ENTITY, biP_CleanupHeli ")
						
						ENDIF
					
					BREAK
				ENDSWITCH
			BREAK
			
		ENDSWITCH
		
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
	//DISPLAY_ON_SCREEN_STRING_AND_NUMBER(0.15, 0.15, "Hit", playerBD[PARTICIPANT_ID_TO_INT()].iMyDamagedRiotVanCount)
	#ENDIF
ENDPROC



PROC STOP_30S_COUNTDOWN_AUDIO()
//	EXIT
	
	
	//-- From spreadsheet
	//--  //depot/gta5/docs/Audio/Interactive_Music/DLC/Apartments_Magnate.xlsx
	IF IS_BIT_SET(iMusicBitset, biMusic_ShouldStop30s)
		IF IS_BIT_SET(iMusicBitset, biMusic_Started30sCountdown)
			IF NOT IS_BIT_SET(iMusicBitset, biMusic_StoppedCountdownMusic)
			
				//-- Restore radio control 
				IF IS_BIT_SET(iMusicBitset, biMusic_PrepareKill30s)	
					IF NOT IS_BIT_SET(iMusicBitset, biMusic_RestoreRadio)
						SET_USER_RADIO_CONTROL_ENABLED(TRUE)
						SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [STOP_30S_COUNTDOWN_AUDIO] SSET_USER_RADIO_CONTROL_ENABLED(TRUE)")
					ENDIF
				ENDIF
				
				IF NOT IS_BIT_SET(iMusicBitset, biMusic_DonePre30sStop)
					IF TRIGGER_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
						SET_BIT(iMusicBitset, biMusic_DonePre30sStop)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [STOP_30S_COUNTDOWN_AUDIO] SET biMusic_DonePre30sStop")
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(iMusicBitset, biMusic_DonePre30sStop)
					IF NOT IS_BIT_SET(iMusicBitset, biMusic_DoneFadeInRadio)
						IF TRIGGER_MUSIC_EVENT("APT_FADE_IN_RADIO")
							SET_BIT(iMusicBitset, biMusic_DoneFadeInRadio)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [STOP_30S_COUNTDOWN_AUDIO] SET biMusic_DoneFadeInRadio")
						ENDIF
					ENDIF
					
					IF IS_BIT_SET(iMusicBitset, biMusic_DoneFadeInRadio)
						SET_BIT(iMusicBitset, biMusic_StoppedCountdownMusic)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [STOP_30S_COUNTDOWN_AUDIO] SET biMusic_StoppedCountdownMusic")
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL ARE_ALL_GB_GUNRUNNING_DEFEND_ENEMY_PED_DEAD()
	INT i
	INT iNumPeds = GET_GB_GUNRUNNING_DEFEND_NUMBER_OF_INITIAL_PEDS_FOR_VARIATION(serverBD.iVariation, serverBD.iBusinessToDefend)
	INT iNumKilled
	
	REPEAT iNumPeds i
		IF DOES_ENEMY_PED_NI_EXIST(i)
			IF IS_PED_INJURED(GET_ENEMY_PED(i))
				iNumKilled++
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN iNumKilled = iNumPeds
	
	
ENDFUNC

/// PURPOSE:
///    There will be different music for different stages of the mission. Audio department will have the details for each music event
PROC MAINTAIN_MUSIC_EVENTS_FOR_VALKYRIE_VARIATION()
//	IF NOT IS_BIT_SET(iMusicBitset, biMusic_MissionPassed )
//	AND NOT IS_BIT_SET(iMusicBitset, biMusic_MissionFailed ) 
//		//-- Mission start
//		IF NOT IS_BIT_SET(iMusicBitset, biMusic_TriggerStart)
//			IF TRIGGER_MUSIC_EVENT("WEED_DEFEND_SHOOTOUT_START")
//				SET_BIT(iMusicBitset, biMusic_TriggerStart)
//				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_MUSIC] Done biMusic_TriggerStart")
//			ENDIF
//		ENDIF
//		
//		//-- Got back to business location
//		IF NOT IS_BIT_SET(iMusicBitset, biMusic_ArriveAtWarehouse )
//			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ApproachedWarehouse)
//				IF TRIGGER_MUSIC_EVENT("WEED_DEFEND_SHOOTOUT_FIGHT")
//					SET_BIT(iMusicBitset, biMusic_ArriveAtWarehouse )
//					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_MUSIC] Done biMusic_ArriveAtWarehouse ")
//				ENDIF
//			ENDIF
//		ENDIF
//		
//		//-- Initial enemies killed
////		IF NOT IS_BIT_SET(iMusicBitset, biMusic_AllInitialKilled )
////			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ApproachedWarehouse)
////				IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllInitalTargetsKilled)
////					IF TRIGGER_MUSIC_EVENT("WEED_DEFEND_SHOOTOUT_PREPARE")
////						SET_BIT(iMusicBitset, biMusic_AllInitialKilled )
////						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_MUSIC] Done biMusic_ArriveAtWarehouse ")
////					ENDIF
////				ENDIF
////			ENDIF
////		ENDIF
////		
////		
//	ENDIF
//	
//	//-- Pass
//	IF IS_SERVER_BIT_SET(biS_AllInitalTargetsKilled)
//		IF NOT IS_BIT_SET(iMusicBitset, biMusic_MissionPassed )
//		
//			IF IS_SERVER_BIT_SET(biS_AllTargetsKilled)
//			//	IF TRIGGER_MUSIC_EVENT("BIKER_MP_MUSIC_STOP")
//			//		SET_BIT(iMusicBitset, biMusic_MissionPassed )
//			//		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_MUSIC] Done biMusic_MissionPassed ")
//			//	ENDIF
//			ENDIF
//		ENDIF
//	ELSE
//		IF NOT IS_BIT_SET(iMusicBitset, biMusic_MissionFailed )
//			IF HAS_MISSION_END_CONDITION_BEEN_MET()
//
//				IF TRIGGER_MUSIC_EVENT("BIKER_MP_MUSIC_FAIL")
//					SET_BIT(iMusicBitset, biMusic_MissionFailed )
//					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_MUSIC] Done biMusic_MissionFailed ")
//				ENDIF
//				
//			ENDIF
//		ENDIF
//	ENDIF
ENDPROC
	
	

PROC MAINTAIN_GB_GUNRUNNING_DEFEND_MUSIC()
	//EXIT
	
	
	STOP_30S_COUNTDOWN_AUDIO()
	
	IF IS_BIT_SET(iMusicBitset, biMusic_Started30sCountdown)
		//-- Don't do anything here if we're doing the 30s countdown audio
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(iMusicBitset, biMusic_Init)
		SET_AUDIO_FLAG("DisableFlightMusic", TRUE) 
		SET_AUDIO_FLAG("WantedMusicDisabled", TRUE)
		
		SET_BIT(iMusicBitset, biMusic_Init)
		
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_CONTRA_DEFEND - [MAINTAIN_GB_CONTRA_DEFEND_MUSIC] SET biMusic_Init")
	ENDIF
		
	SWITCH serverBD.iVariation
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			MAINTAIN_MUSIC_EVENTS_FOR_VALKYRIE_VARIATION()
		BREAK
		
		//TODO: no music for defusal?
		
		
	ENDSWITCH
	
	
	IF HAS_NET_TIMER_STARTED(serverBD.timeTimeout)
		GB_MAINTAIN_COUNTDOWN_MUSIC(GET_GB_GUNRUNNING_DEFEND_TIMEOUT() - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeTimeout), HAS_MISSION_END_CONDITION_BEEN_MET(), cmStruct)
	ENDIF
	                         
ENDPROC



/// PURPOSE:
///    Countdown audio for the last 30 seconds of the mission duration
/// PARAMS:
///    iTime - 
PROC MAINTAIN_30S_COUNTDOWN_MUSIC(INT iTime)
	//EXIT
	
	
	//-- From spreadsheet
	//--  //depot/gta5/docs/Audio/Interactive_Music/DLC/Apartments_Magnate.xlsx
	
	IF IS_BIT_SET(iMusicBitset, biMusic_ShouldStop30s)
		STOP_30S_COUNTDOWN_AUDIO()
	//	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_30S_COUNTDOWN_MUSIC] EXIT AS biMusic_ShouldStop30s")
		EXIT 
	ENDIF
	
	IF NOT IS_BIT_SET(iMusicBitset, biMusic_DonePre30sStop)
		IF iTime <= 35000
			IF TRIGGER_MUSIC_EVENT("APT_PRE_COUNTDOWN_STOP")
				SET_BIT(iMusicBitset, biMusic_DonePre30sStop)
				SET_BIT(iMusicBitset, biMusic_Started30sCountdown)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_30S_COUNTDOWN_MUSIC] SET biMusic_DonePre30sStop")
			ELSE
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_30S_COUNTDOWN_MUSIC] TRYING TO TRIGGER APT_PRE_COUNTDOWN_STOP")
			ENDIF
		ENDIF
	ENDIF
	
	
	IF IS_BIT_SET(iMusicBitset, biMusic_Started30sCountdown)
		IF iTime <= 30000
			IF NOT IS_BIT_SET(iMusicBitset, biMusic_PrepareKill30s)
				IF PREPARE_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
					SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE)
					SET_USER_RADIO_CONTROL_ENABLED(FALSE) 
					SET_BIT(iMusicBitset, biMusic_PrepareKill30s)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_30S_COUNTDOWN_MUSIC] SET biMusic_PrepareKill30s")
				ELSE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_30S_COUNTDOWN_MUSIC] TRYING TO PREPARE APT_COUNTDOWN_30S_KILL")
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(iMusicBitset, biMusic_DoneStart30s)
				IF TRIGGER_MUSIC_EVENT("APT_COUNTDOWN_30S")
					SET_BIT(iMusicBitset, biMusic_DoneStart30s)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_30S_COUNTDOWN_MUSIC] SET biMusic_DoneStart30s")
				ELSE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_30S_COUNTDOWN_MUSIC] TRYING TO TRIGGER APT_COUNTDOWN_30S")
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(iMusicBitset, biMusic_DoneStart30s)
				IF iTime <= 27000
					IF NOT IS_BIT_SET(iMusicBitset, biMusic_RestoreRadio)
						SET_USER_RADIO_CONTROL_ENABLED(TRUE)
						SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
						SET_BIT(iMusicBitset, biMusic_RestoreRadio) 
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_30S_COUNTDOWN_MUSIC] SET biMusic_RestoreRadio")
					ENDIF
					
					IF iTime <= 500 
						IF TRIGGER_MUSIC_EVENT("APT_FADE_IN_RADIO")
							CANCEL_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_30S_COUNTDOWN_MUSIC] TRIGGERED APT_FADE_IN_RADIO")
							CLEAR_BIT(iMusicBitset, biMusic_Started30sCountdown)
						ELSE
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_30S_COUNTDOWN_MUSIC] TRYING TO TRIGGER APT_FADE_IN_RADIO")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC



//Helper function to get the servers game/mission state
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC


///	PURPOSE: Need specific relationship groups for players taking part. Don't want AI to attack non-mission players
PROC SETUP_GB_DEFEND_GROUPS()
	PRINTLN("     ---------->     KILL LIST - SETUP_GB_DEFEND_GROUPS")
	
	ADD_RELATIONSHIP_GROUP("relDefendPlayer", relDefendPlayer)
//	ADD_RELATIONSHIP_GROUP("relBikerDefendAi", relBikerDefendAi)
	RELATIONSHIP_TYPE relWithCops = GET_RELATIONSHIP_BETWEEN_GROUPS(GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()), RELGROUPHASH_COP)
	
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		
		//Like Player
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rgFM_Team[i], relDefendPlayer)	
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relDefendPlayer, rgFM_Team[i])
		
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rgFM_Team[i], relBikerDefendAi)	
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relBikerDefendAi, rgFM_Team[i])
		
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relDefendPlayer, RELGROUPHASH_COP)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_COP, relDefendPlayer)
		
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relDefendPlayer, rgFM_AiAmbientGangMerc[5])
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rgFM_AiAmbientGangMerc[5], relDefendPlayer)
		
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relDefendPlayer, rgFM_AiLike)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rgFM_AiLike, relDefendPlayer)
		
	ENDREPEAT
	
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(relWithCops, relDefendPlayer, RELGROUPHASH_COP)
	SET_RELATIONSHIP_BETWEEN_GROUPS(relWithCops, RELGROUPHASH_COP, relDefendPlayer)
		
	//Hate UW players
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relDefendPlayer, relBikerDefendAi)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relBikerDefendAi, relDefendPlayer)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relDefendPlayer, rgFM_AiHate)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rgFM_AiHate, relDefendPlayer)
	// Like Cops
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rgFM_Team[i], rgFM_AiLikePlyrLikeCops)
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rgFM_AiLikePlyrLikeCops, rgFM_Team[i])
//	//Dislike Player Like Cops
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE, rgFM_Team[i], rgFM_AiDislikePlyrLikeCops)
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE, rgFM_AiDislikePlyrLikeCops, rgFM_Team[i])
//	//Hate Player Like Cops
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rgFM_Team[i], rgFM_AiHatePlyrLikeCops)
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rgFM_AiHatePlyrLikeCops, rgFM_Team[i])
	
	//SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rgFM_Team[i], rgFM_AiHatedByCopsAndMercs)
	

	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_AMBIENT_GANG_CULT, relBikerDefendAi)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE , RELGROUPHASH_AMBIENT_GANG_CULT, relDefendPlayer)

	
	
	//-- Cops
	SET_AMBIENT_COP_REL_TO_THIS_RELGROUP(ACQUAINTANCE_TYPE_PED_LIKE, relBikerDefendAi)
	
	//**AMBIENT GANGS
	SET_AMBIENT_GANG_LIKES_THIS_RELGROUP(relBikerDefendAi)
	SET_AMBIENT_GANG_LIKES_THIS_RELGROUP(relDefendPlayer)
ENDPROC




PROC REMOVE_UW_REL_GROUPS()
	//REMOVE_RELATIONSHIP_GROUP(relDefendAI)
	REMOVE_RELATIONSHIP_GROUP(relDefendPlayer)
ENDPROC

PROC CLEANUP_ASSETS()
	INT i
	
	REPEAT MAX_TARGETS i
		IF DOES_BLIP_EXIST(blipTarget[i])
			REMOVE_BLIP(blipTarget[i])
		ENDIF
	ENDREPEAT
	
	IF DOES_BLIP_EXIST(blipVeh[0])
		REMOVE_BLIP(blipVeh[0])
	ENDIF
	
	IF DOES_BLIP_EXIST(blipAction)
		REMOVE_BLIP(blipAction)
	ENDIF
	
	IF DOES_BLIP_EXIST(blipDropOff)
		REMOVE_BLIP(blipDropOff)
	ENDIF
	
	IF DOES_BLIP_EXIST(blipWarehouse)
		REMOVE_BLIP(blipWarehouse)
	ENDIF
	IF DOES_BLIP_EXIST(blipWarehouseDropOff)
		REMOVE_BLIP(blipWarehouseDropOff)
	ENDIF
	
	
	
		
	IF IS_BIT_SET(iBoolsBitSet, biL_StartedAlarm)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [CLEANUP_ASSETS] stopping alarm")
		
		STOP_SOUND(iAlarmSoundID)
		
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("ALARM_BELL_02")
		
		CLEAR_BIT(iBoolsBitSet, biL_StartedAlarm)
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DCONTRA_HLP1")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DCONTRA_HLP2")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DCONTRA_HLPVCR")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DCONTRA_HLPCR")
		CLEAR_HELP()
	ENDIF
	
	CLEANUP_DEFEND_CONTRABAND_SPAWNING()
ENDPROC

//PROC RESET_ALL_DEFEND_PLAYER_BLIPS()
//	IF NOT IS_STOLEN_VAN_VARIATION()
//		EXIT
//	ENDIF
//	
//	INT i
//	IF NOT IS_BIT_SET(iBoolsBitSet, biL_ResetPlayerBlips)
//		
//		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [RESET_ALL_DEFEND_PLAYER_BLIPS] SET biL_ResetPlayerBlips")
//		SET_BIT(iBoolsBitSet, biL_ResetPlayerBlips)
//		
//		REPEAT NUM_NETWORK_PLAYERS i
//			PLAYER_INDEX PlayerId = INT_TO_PLAYERINDEX(i)
//			IF NETWORK_IS_PLAYER_ACTIVE(PlayerId)
//				IF PlayerId != PLAYER_ID()
//
//						SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(PlayerId, RADAR_TRACE_CONTRABAND, FALSE)
//						
//						SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(PlayerId, GET_BLIP_COLOUR_FROM_HUD_COLOUR(hcMyGang) , FALSE)
//						
//					
//				ENDIF
//			ENDIF
//		ENDREPEAT
//	ENDIF
//ENDPROC

//FUNC BLIP_SPRITE GET_CONTRABAND_VEHICLE_BLIP()
//	RETURN RADAR_TRACE_PACKAGE
//ENDFUNC



PROC MAINTAIN_INITIALISE_DELIVERABLE_IDS()
	//called every frame

	FREEMODE_DELIVERY_DROPOFFS eDropoff[1]
	IF !IS_BIT_SET(iBoolsBitSet, biL_DropoffSetup)
			
		eDropoff[0] = GET_GUNRUNNING_DROPOFF_SIMPLE_INTERIOR(GET_SIMPLE_INTERIOR_ID_FROM_FACTORY_ID(GET_OWNED_BUNKER(GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID()))))
		IF !HAS_NET_TIMER_STARTED(dropoffSendEventTimer)
		START_NET_TIMER(dropoffSendEventTimer)
			//sends an event
			FREEMODE_DELIVERY_SET_DROPOFF_ACTIVE(eDropoff[0], serverBD.sMissionID)
		ENDIF
		IF HAS_NET_TIMER_EXPIRED(dropoffSendEventTimer, dropoffSendEventTimerThreshold)
			RESET_NET_TIMER(dropoffSendEventTimer)
		ENDIF
		
		IF 	FREEMODE_DELIVERY_IS_DROPOFF_ACTIVE_FOR_PLAYER(eDropoff[0], GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID()))
			SET_BIT(iBoolsBitSet, biL_DropoffSetup)
		ENDIF
	ENDIF
	
	IF !IS_BIT_SET(iBoolsBitSet, biL_DeliverablesSetup)
	//AND serverBD.sGunrunEntities.eType != eGUNRUNENTITYTYPE_UNSET	
		eDropoff[0] = FREEMODE_DELIVERY_DROPOFF_INVALID
		INT iGunrunEntity
		BOOL bComplete = TRUE
		
		IF NETWORK_GET_HOST_OF_THIS_SCRIPT() = PARTICIPANT_ID()
			IF NOT FREEMODE_DELIVERY_CREATE_DELIVERABLE(serverBD.deliverableGunrunIds[iGunrunEntity], GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID()), iGunrunEntity, GUNRUNNING_DELIVERABLE_CRATE, serverBD.sMissionID, 1 , eDropoff[0])
				bComplete = FALSE
			ENDIF
		ELSE
			IF serverBD.deliverableGunrunIds[0].iIndex = -1
				bComplete = FALSE
			ENDIF
		ENDIF
			
		IF bComplete
			SET_BIT(iBoolsBitSet, biL_DeliverablesSetup)
		ENDIF
	ELIF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		eDropoff[0] = GET_GUNRUNNING_DROPOFF_SIMPLE_INTERIOR(GET_SIMPLE_INTERIOR_ID_FROM_FACTORY_ID(GET_OWNED_BUNKER(GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID()))))
		FREEMODE_DELIVERY_LINK_DELIVERABLES_TO_DROPOFFS(GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID()), serverBD.sMissionID, eDropoff)
	ENDIF
ENDPROC
PROC RESET_ALL_PLAYER_VEH_BLIPS()
	INT i
	
	IF NOT IS_BIT_SET(iBoolsBitSet, biL_ResetAllPlayerBlips)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [RESET_ALL_PLAYER_VEH_BLIPS] Called")
		
		REPEAT NUM_NETWORK_PLAYERS i
			PLAYER_INDEX PlayerId = INT_TO_PLAYERINDEX(i)
			IF NETWORK_IS_PLAYER_ACTIVE(PlayerId)
				IF PlayerId != PLAYER_ID()
					

					SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(PlayerId, GET_GUNRUN_ENTITY_BLIP_SPRITE(FALSE), FALSE)
					SET_PLAYER_BLIP_AS_LONG_RANGE(PlayerId, FALSE)
					FORCE_BLIP_PLAYER(PlayerId, FALSE, FALSE)
					
					SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(PlayerId, BLIP_COLOUR_BLUE, FALSE)
				ENDIF
			ENDIF
		ENDREPEAT
		
		SET_BIT(iBoolsBitSet, biL_ResetAllPlayerBlips)
	ENDIF
ENDPROC
/// PURPOSE:
///    Setting telemetry data specific to the defend missions before the mission ends
PROC GB_GUNRUNNING_DEFEND_SET_DEFEND_TELEMETRY_DATA()
	//g_sGUNRUN_Telemetry_data.m_playerrole =
	//g_sGUNRUN_Telemetry_data.m_Result =
	//g_sGUNRUN_Telemetry_data.m_endingreason = 
	//	g_sGUNRUN_Telemetry_data.m_timetakentocomplete = 
	//g_sGUNRUN_Telemetry_data.m_cashearned = 0
	//g_sGUNRUN_Telemetry_data.m_rpearned = 0
	
	//   (serverBD.iOrganisationSizeOnLaunch - (GB_GET_NUM_GOONS_IN_LOCAL_GANG()+1))  
	g_sGUNRUN_Telemetry_data.m_playersleftinprogress = (serverBD.iOrganisationSizeOnLaunch - (GB_GET_NUM_GOONS_IN_LOCAL_GANG()+1)) 
	PRINTLN("JS-ZZZ- serverBD.iOrganisationSizeOnLaunch = ", serverBD.iOrganisationSizeOnLaunch)
	PRINTLN("JS-ZZZ- GB_GET_NUM_GOONS_IN_LOCAL_GANG() = ", GB_GET_NUM_GOONS_IN_LOCAL_GANG())
	PRINTLN("JS-ZZZ- g_sGUNRUN_Telemetry_data.m_playersleftinprogress = ", g_sGUNRUN_Telemetry_data.m_playersleftinprogress)
	
	FLOAT fPercentToRemove
	FACTORY_ID ownedBunker =  GET_OWNED_BUNKER(PLAYER_ID())
	INT iAmountInWarehouse =  GET_PRODUCT_TOTAL_FOR_FACTORY(PLAYER_ID(),ownedBunker)
	INT iAmountToRemove
	fPercentToremove = GET_BUSINESS_STOCK_AMOUNT_TO_LOSE()
	fPercentToRemove *= g_sMPTunables.fbiker_defend_product_taken_on_fail
	PRINTLN("JS-ZZZ1- fPercentToremove = ", fPercentToremove)
	iAmountToRemove  =  ROUND(TO_FLOAT(iAmountInWarehouse) * fPercentToRemove)
	PRINTLN("JS-ZZZ2- iAmountToRemove = ", iAmountToRemove)
	g_sGUNRUN_Telemetry_data.m_SuppliesOwned  = GET_MATERIALS_TOTAL_FOR_FACTORY(PLAYER_ID(),GET_OWNED_BUNKER(PLAYER_ID()))
	g_sGUNRUN_Telemetry_data.m_ProductsOwned = iAmountInWarehouse - iAmountToRemove
	PRINTLN("JS-ZZZ- g_sGUNRUN_Telemetry_data.m_SuppliesOwned = ", g_sGUNRUN_Telemetry_data.m_SuppliesOwned)
	PRINTLN("JS-ZZZ- g_sGUNRUN_Telemetry_data.m_ProductsOwned = ", g_sGUNRUN_Telemetry_data.m_ProductsOwned)
	
	
ENDPROC

PROC GB_GUNRUNNING_DEFEND_SET_FINAL_DEFEND_TELEMETRY_DATA()
	
	
ENDPROC

PROC CLEANUP_BOMB_SFX()
	IF iStartBombSound != -1
		STOP_SOUND(iStartBombSound)
		RELEASE_SOUND_ID(iStartBombSound)
		iStartBombSound = -1
	ENDIF
	IF iTickBombSound != -1
		STOP_SOUND(iTickBombSound)
		RELEASE_SOUND_ID(iTickBombSound)
		iTickBombSound = -1
	ENDIF
ENDPROC

//Cleanup Mission Data
PROC SCRIPT_CLEANUP()
	GB_GUNRUNNING_DEFEND_SET_FINAL_DEFEND_TELEMETRY_DATA()
	FREEMODE_DELIVERY_CLEAR_LP_DELIVERABLE_POSSESSION_FOR_MISSION(serverBD.sMissionID)
	FREEMODE_DELIVERY_SET_ALL_DROPOFFS_DISABLED(FALSE)
	SET_AUDIO_FLAG("DisableFlightMusic", TRUE)
	SET_AUDIO_FLAG("WantedMusicDisabled", TRUE)
	CLEANUP_BOMB_SFX()
	CLEANUP_GANG_CHASE_CLIENT()
	CLEAR_LOCAL_PLAYER_IS_VALID_GANG_CHASE_TARGET()
	SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY(FALSE)
	IF iArmedBombSound != -1
		STOP_SOUND(iArmedBombSound)
		RELEASE_SOUND_ID(iArmedBombSound)
		iArmedBombSound = -1
	ENDIF
	IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
	//TODO: GET a mission pass bool
		RESET_DEFEND_MISSION_TIMERS(GET_OWNED_BUNKER(PLAYER_ID()), TRUE)
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
		IF serverBD.bSomeoneSPassed
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
	ENDIF
		
	MPGlobalsAmbience.iDefendContrabandVar = -1
	#ENDIF
	
	FREEMODE_DELIVERY_REQUEST_DELIVERABLE_CLEANUP(serverBD.deliverableGunrunIds[0])
	
	
	IF IS_BIT_SET(iBoolsBitSet2,biL2_RadarZoomed)
		SET_RADAR_ZOOM_PRECISE(0)
		SET_LOCAL_PLAYER_AS_USING_MISSION_SPECIFIC_ZOOM(FALSE)
		CLEAR_BIT(iBoolsBitSet2,biL2_RadarZoomed)
	ENDIF
	
	IF !IS_BIT_SET(iBoolsBitSet2,biL2_DidMusicCue6)	
	AND !IS_BIT_SET(iBoolsBitSet2,biL2_DidMusicCue5)
		IF IS_BIT_SET(iBoolsBitSet2,biL2_DidMusicCue0)	
		OR IS_BIT_SET(iBoolsBitSet2,biL2_DidMusicCue1)	
		OR IS_BIT_SET(iBoolsBitSet2,biL2_DidMusicCue2)	
		OR IS_BIT_SET(iBoolsBitSet2,biL2_DidMusicCue3)	
		OR IS_BIT_SET(iBoolsBitSet2,biL2_DidMusicCue4)	
			//GR_MP_MUSIC_FAIL	Please trigger if the player leaves the session, fails the mission or ungracefully exits the script.
			TRIGGER_MUSIC_EVENT("GR_MP_MUSIC_STOP")
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GUNRUNNING_DEFEND_AUDIO_MUSIC_TRIGGERS] SCRIPT_CLEANUP")
			SET_BIT(iBoolsBitSet2,biL2_DidMusicCue6)
		ENDIF
	ENDIF
	
	RESET_SPECTATOR_CAN_QUIT()
	
	IF serverBD.iBossPlayerWhoLaunched = NATIVE_TO_INT(PLAYER_ID())
		IF NOT HAS_MISSION_END_CONDITION_BEEN_MET()
	//		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - CLEANUP MISSION I'm boss player who launched, but no end condition met")
		//	SET_LOCAL_PLAYER_QUIT_GB_BIKER_DEFEND(ENUM_TO_INT(serverBD.iBusinessToDefend), (serverBD.iVariation = GB_GUNRUNNING_DEFEND_GLOBAL))
			
	//		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  CLEANUP MISSION - Player quit....")
	//		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  CLEANUP MISSION - 			GB_SHOULD_CLOSE_BUSINESS_DUE_TO_LOCAL_PLAYER_QUITTING_DEFEND() = ", GB_SHOULD_CLOSE_BUSINESS_DUE_TO_LOCAL_PLAYER_QUITTING_DEFEND())
	//		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  CLEANUP MISSION - 			GB_SHOULD_LOSE_PRODUCT_DUE_TO_LOCAL_PLAYER_QUITTING_DEFEND() = ", GB_SHOULD_LOSE_PRODUCT_DUE_TO_LOCAL_PLAYER_QUITTING_DEFEND())
	//		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  CLEANUP MISSION - 			GB_DID_LOCAL_PLAYER_QUIT_ANY_BIKER_DEFEND() = ", GB_DID_LOCAL_PLAYER_QUIT_ANY_BIKER_DEFEND())
	//		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  CLEANUP MISSION - 			GB_DID_LOCAL_PLAYER_QUIT_BIKER_GLOBAL_DEFEND() = ", GB_DID_LOCAL_PLAYER_QUIT_BIKER_GLOBAL_DEFEND())
	//		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  CLEANUP MISSION - 			GB_GET_FACTORY_LOCAL_PLAYER_QUIT_DEFENDING() = ", ENUM_TO_INT(GB_GET_FACTORY_LOCAL_PLAYER_QUIT_DEFENDING()))
		ENDIF
	ENDIF
	
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
//		g_sBIK_Telemetry_data.m_TypeOfBusinesS = 
//		g_sBIK_Telemetry_data.m_Location = 
		
		IF IS_BIT_SET(iBoolsBitSet, biL_SetupTelemetry)
			IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneEndTelemetry)
				IF NETWORK_IS_GAME_IN_PROGRESS()
					IF DID_MY_BOSS_LAUNCH_GB_GB_GUNRUNNING_DEFEND()
						GB_GUNRUNNING_DEFEND_SET_DEFEND_TELEMETRY_DATA()
						GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LEFT, INT_TO_ENUM(FACTORY_ID, serverBD.iBusinessToDefend))
						SET_BIT(iBoolsBitSet, biL_DoneEndTelemetry)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  CLEANUP MISSION - TELEMETRY SETTING GB_TELEMETRY_END_LEFT AS NOTHING SET YET")
					ELSE
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  CLEANUP MISSION - DIDN'T SEND TELEMETRY AS DID_MY_BOSS_LAUNCH_GB_ASSAULT / DID_LOCAL_PLAYER_JOIN_AS_RIVAL")
					ENDIF
				ENDIF
			ENDIF

		ELSE
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  CLEANUP - NOT SENDING END PLAYSTATS AS biL_SetupTelemetry not set") 
		ENDIF
	ENDIF
//	SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_GB_GUNRUNNING_DEFEND, FALSE)
//	SET_FREEMODE_FLOW_COMMS_DISABLED(FALSE)

	GB_STOP_COUNTDOWN_MUSIC(cmStruct)
	
	CLEANUP_GB_PACKAGE_CLIENT(localPackages, serverBD.packagesServer, HAS_MISSION_END_CONDITION_BEEN_MET())

	
	CLEANUP_DEFEND_CONTRABAND_SPAWNING()

	
	IF GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()) = relDefendPlayer
		IF NETWORK_IS_GAME_IN_PROGRESS()
			SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), relMyFmGroup)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  CLEANUP MISSION - RESTORED MY FM REL GROUP     <----------     ") NET_NL()
		ELSE
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  CLEANUP MISSION - Not RESTORING MY FM REL GROUP as NETWORK_IS_GAME_IN_PROGRESS    <----------     ")
		ENDIF
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			INT iNumBlockingAreas = GET_GB_GUNRUNNING_DEFEND_NUMBER_OF_SCENARIO_BLOCKING_AREAS(serverBD.iVariation, serverBD.iBusinessToDefend)
			INT iNum
			REPEAT iNumBlockingAreas iNum
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  CLEANUP MISSION REMOVE_SCENARIO_BLOCKING_AREA ", iNum)
				REMOVE_SCENARIO_BLOCKING_AREA(serverBD.scenWarehouse[iNum], TRUE)
			ENDREPEAT
			
		
		ENDIF
		
		RESET_ALL_PLAYER_VEH_BLIPS()
	ENDIF
	
	REMOVE_UW_REL_GROUPS()
	
	IF serverBD.iBossPlayerWhoLaunched != -1
		IF IS_BIT_SET(iBoolsBitSet, biL_BlippedRival)
			IF NETWORK_IS_PLAYER_ACTIVE(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
				
				SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), RADAR_TRACE_TEMP_4, FALSE)
				SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), BLIP_COLOUR_RED, FALSE)
				FORCE_BLIP_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), FALSE, FALSE)
				SET_PLAYER_BLIP_AS_LONG_RANGE(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), FALSE) 
				SET_FIXED_BLIP_SCALE_FOR_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), g_sMPTunables.fgangboss_Job_blip_scale, FALSE)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  CLEANUP MISSION - biL_BlippedRival WAS SET, RESET BLIP")
			ENDIF
		ENDIF
	ENDIF
	
	IF GB_IS_GLOBAL_NON_BD_BIT0_SET(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_DPAD_DOWN_LB)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  CLEANUP MISSION - CLEAR eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_DPAD_DOWN_LB")
		GB_CLEAR_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_DPAD_DOWN_LB)
	ENDIF
	
	//-- Reset music
	IF NOT IS_BIT_SET(iMusicBitset, biMusic_MissionPassed )
	AND NOT IS_BIT_SET(iMusicBitset, biMusic_MissionFailed )
		//TRIGGER_MUSIC_EVENT("BIKER_MP_MUSIC_FAIL")
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - CLEANUP MISSION TRIGGER_MUSIC_EVENT(BIKER_MP_MUSIC_FAIL)")
	ENDIF
	
	IF IS_BIT_SET(iMusicBitset, biMusic_Init)
		IF NOT IS_BIT_SET(iMusicBitset, biMusic_Reset)
			SET_AUDIO_FLAG("DisableFlightMusic", FALSE) 
			SET_AUDIO_FLAG("WantedMusicDisabled", FALSE)
			
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  CLEANUP MISSION - SET biMusic_Reset")
		ENDIF
	ENDIF
	
	IF IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
		SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY(FALSE)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  CLEANUP MISSION  SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY = FALSE")
	ENDIF
	
	SET_BIT(iMusicBitset, biMusic_ShouldStop30s)
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  CLEANUP MISSION - SET biMusic_ShouldStop30s")
	STOP_30S_COUNTDOWN_AUDIO()
	SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(TRUE)
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  CLEANUP MISSION - SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(TRUE)")
	GB_SET_LOCAL_PLAYER_CRITICAL_TO_JOB(FALSE)
	
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
		
//	REMOVE_TRAFFIC_REDUCTION_ZONE(serverBD.iTrafficReductionIndex)
	
	
	GB_COMMON_BOSS_MISSION_CLEANUP()
	
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
		SET_MAX_WANTED_LEVEL(5)
	ENDIF
	
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  CLEANUP MISSION      <----------     ") NET_NL()
	#IF IS_DEBUG_BUILD NET_LOG("SCRIPT_CLEANUP")	#ENDIF
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC





//PURPOSE: Returns the Cash reward for completing the objective
FUNC INT GET_CASH_REWARD()
	RETURN g_sMPTunables.iUrbanWarfareCashReward
ENDFUNC

//Do necessary pre game start ini.
PROC PROCESS_PRE_GAME( MP_MISSION_DATA missionScriptArgs )
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  																						 ")
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - ********************************************************************************************")
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - *																							*")
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - *						MISSION START														*")
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - *																							*")
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - ********************************************************************************************")
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  																					     ")

	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission) ,  missionScriptArgs)	//NUM_NETWORK_PLAYERS, missionScriptArgs)	//
	//NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, PARTICIPANT_ID_TO_INT())  
	
	//Reserve Entities
	RESERVE_NETWORK_MISSION_PEDS(MAX_TARGETS + 1)
	RESERVE_NETWORK_MISSION_VEHICLES(MAX_AMBIENT_VEH + MAX_GANG_CHASE_PER_TARGET)
	RESERVE_NETWORK_MISSION_OBJECTS(MAX_DEFEND_PROPS)
	//RESERVE_NETWORK_MISSION_OBJECTS(MAX_PACKAGES)
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	SET_AUDIO_FLAG("DisableFlightMusic", TRUE)
	SET_AUDIO_FLAG("WantedMusicDisabled", TRUE)

	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		#IF IS_DEBUG_BUILD NET_LOG("BIKER_CONTRA_DEFEND -  FAILED TO RECEIVE INITIAL NETWORK BROADCAST")	#ENDIF
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  MISSION END - FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP D     <----------     ") NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		// This script will not be paused if another script calls PAUSE_GAME
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			serverbd.iOrganisationSizeOnLaunch = GB_GET_NUM_GOONS_IN_LOCAL_GANG() + 1
		ENDIF
		
	//	SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_GB_GUNRUNNING_DEFEND, TRUE)
		
	//	g_sMPTunables.bBIKER_DEFEND_POLICE_RAID_DISABLE_SPECTATOR = TRUE
		
		SETUP_GB_DEFEND_GROUPS()
		
	//	CLEAR_LOCAL_PLAYER_QUIT_GB_GUNRUNNING_DEFEND()
		
		GB_COMMON_BOSS_MISSION_PREGAME()		
		
		ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
		
		
		
		playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_INI
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - TUNABLES...")
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - TUNABLES... g_sMPTunables.iBIKER_DEFEND_EASY_ENEMY_ACCURACY   	           			= ", g_sMPTunables.iBIKER_DEFEND_EASY_ENEMY_ACCURACY   )
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - TUNABLES... g_sMPTunables.iBIKER_DEFEND_MEDIUM_ENEMY_ACCURACY                		= ", g_sMPTunables.iBIKER_DEFEND_MEDIUM_ENEMY_ACCURACY )
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - TUNABLES... g_sMPTunables.iBIKER_DEFEND_HARD_ENEMY_ACCURACY   	           			= ", g_sMPTunables.iBIKER_DEFEND_HARD_ENEMY_ACCURACY   )
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - TUNABLES... g_sMPTunables.fBIKER_DEFEND_EASY_ENEMY_HEALTH                    		= ", g_sMPTunables.fBIKER_DEFEND_EASY_ENEMY_HEALTH     )
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - TUNABLES... g_sMPTunables.fBIKER_DEFEND_MEDIUM_ENEMY_HEALTH   	           			= ", g_sMPTunables.fBIKER_DEFEND_MEDIUM_ENEMY_HEALTH   )
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - TUNABLES... g_sMPTunables.fBIKER_DEFEND_HARD_ENEMY_HEALTH   	           			= ", g_sMPTunables.fBIKER_DEFEND_HARD_ENEMY_HEALTH   )
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - TUNABLES... g_sMPTunables.iBIKER_DEFEND_EASY_ENEMY_FIRE_RATE  	           			= ", g_sMPTunables.iBIKER_DEFEND_EASY_ENEMY_FIRE_RATE  )
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - TUNABLES... g_sMPTunables.iBIKER_DEFEND_MEDIUM_ENEMY_FIRE_RATE               		= ", g_sMPTunables.iBIKER_DEFEND_MEDIUM_ENEMY_FIRE_RATE)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - TUNABLES... g_sMPTunables.iBIKER_DEFEND_HARD_ENEMY_FIRE_RATE  	           			= ", g_sMPTunables.iBIKER_DEFEND_HARD_ENEMY_FIRE_RATE  )
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - TUNABLES... g_sMPTunables.iBIKER_DEFEND_EASY_WANTED_CAP       	           			= ", g_sMPTunables.iBIKER_DEFEND_EASY_WANTED_CAP       )
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - TUNABLES... g_sMPTunables.iBIKER_DEFEND_MEDIUM_WANTED_CAP     	           			= ", g_sMPTunables.iBIKER_DEFEND_MEDIUM_WANTED_CAP     )
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - TUNABLES... g_sMPTunables.iBIKER_DEFEND_HARD_WANTED_CAP       	           			= ", g_sMPTunables.iBIKER_DEFEND_HARD_WANTED_CAP       )

										
	
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND")
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  PRE_GAME DONE     <----------     ") NET_NL()
	ELSE
		#IF IS_DEBUG_BUILD NET_LOG("BIKER_CONTRA_DEFEND -  NETWORK_IS_GAME_IN_PROGRESS = FALSE")	#ENDIF
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  MISSION END - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP Q     <----------     ") NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
ENDPROC

PROC MOVE_TO_EXPLODE_VEHICLE_STAGE()

ENDPROC				

FUNC BOOL ARE_ALL_PEDS_CLEANED_UP()
	
	
	RETURN TRUE
ENDFUNC

FUNC BOOL MISSION_END_CHECK()
	RETURN FALSE
ENDFUNC	

//Server only function that checks to see if the game mode should now end. 
FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET()
	
	#IF IS_DEBUG_BUILD
		IF bHostEndMissionNow
			RETURN TRUE
		ENDIF
		
		IF ServerBD.bSomeoneSPassed
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [HAVE_MISSION_END_CONDITIONS_BEEN_MET] TRUE AS bSomeoneSPassed")
			RETURN TRUE
		ENDIF
		
	#ENDIF	
	
	IF MISSION_END_CHECK()
		RETURN TRUE
	ENDIF
	
	IF IS_SERVER_BIT_SET( biS_AllPlayersFinished)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC




//Helper function to get a clients game/mission state
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iGameState
ENDFUNC

#IF IS_DEBUG_BUILD
PROC CREATE_WIDGETS()
	INT iPlayer
	TEXT_LABEL_15 tl63

	START_WIDGET_GROUP("GB_GB_GUNRUNNING_DEFEND")  
		
		
		
		// Gameplay debug, sever only.
		START_WIDGET_GROUP("Server Only Gameplay") 

			ADD_WIDGET_BOOL("End mission", bHostEndMissionNow)			
		STOP_WIDGET_GROUP()		
		
		// Data about the server
		START_WIDGET_GROUP("Server BD") 
			ADD_WIDGET_INT_SLIDER("S. Game state", serverBD.iServerGameState,-1, HIGHEST_INT,1)

		STOP_WIDGET_GROUP()	

		// Data about the clients. * = You.
		START_WIDGET_GROUP("Client BD")  				
			REPEAT GB_MAX_GANG_SIZE_INCLUDING_BOSS iPlayer
				tl63 = "Player "
				tl63 += iPlayer
				IF iPlayer = PARTICIPANT_ID_TO_INT()
					tl63 += "*"
				ENDIF
				START_WIDGET_GROUP(tl63)
					ADD_WIDGET_INT_SLIDER("Game state", playerBD[iPlayer].iGameState,-1, HIGHEST_INT,1)
				STOP_WIDGET_GROUP()
			ENDREPEAT				
		STOP_WIDGET_GROUP()	
		ADD_WIDGET_BOOL("Blip all drivers", bWdBlipAllDrivers)
		ADD_WIDGET_BOOL("Vehicle on screen?", bWdDoOnScreenDebug)
		ADD_WIDGET_BOOL("Dist To Van?", bWdShowDistToVan)
		ADD_WIDGET_BOOL("Entity Spawn debug", g_SpawnData.bShowAdvancedSpew)
		ADD_WIDGET_FLOAT_SLIDER("Dist", fWdVanDist, 0.0, 10000.0, 0.1)
		ADD_WIDGET_INT_SLIDER("Mission Duration (HOST ONLY)",serverbd.missionTimerOverride,-1, HIGHEST_INT,1)
		//ADD_WIDGET_INT_SLIDER("Valkyrie Mission Location (HOST ONLY)",serverbd.iValkyrieSelection,0,2,1)
		//ADD_WIDGET_INT_SLIDER("Defusal Mission Location (HOST ONLY)",serverbd.iDefusalSelection,0,3,1)
		
		
		
	STOP_WIDGET_GROUP()
ENDPROC		

BLIP_INDEX blipWdDrivers[MAX_ENEMY_VEH]
PROC UPDATE_WIDGETS()
	
	IF bWdShowDistToVan
//		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[0].niVeh)
//			IF IS_NET_VEHICLE_DRIVEABLE(serverBD.Targets[0].niVeh)
//				fWdVanDist = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), NET_TO_VEH(serverBD.Targets[0].niVeh), FALSE)
//			ENDIF
//		ENDIF
	ENDIF
	
	INT iDriver
	INT i
	VECTOR vLoc
	IF bWdBlipAllDrivers 
		REPEAT MAX_ENEMY_VEH i
		//	iDriver = GET_GB_DEFEND_DRIVER_OF_ENEMY_VEH(serverBD.iVariation, serverBD.iBusinessToDefend, i)
			IF NOT DOES_BLIP_EXIST(blipWdDrivers[i])
				IF DOES_ENEMY_PED_NI_EXIST(iDriver)
					vLoc = GET_ENTITY_COORDS(GET_ENEMY_PED(iDriver), FALSE)
					IF NOT IS_PED_INJURED(GET_ENEMY_PED(iDriver))
						blipWdDrivers[i] = ADD_BLIP_FOR_ENTITY(GET_ENEMY_PED(iDriver))
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND [bWdBlipAllDrivers] Added blip for ped ", iDriver, " at coord ", vLoc, " - driver of veh ", i)
					ELSE
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND [bWdBlipAllDrivers] Failed to add blip (injured) for ped ", iDriver, " at coord ", vLoc, " - driver of veh ", i)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

VECTOR vWarpCoordsForJskip
FUNC BOOL HANDLE_J_SKIP()

	INT iVeh
	VEHICLE_INDEX vehDefend

	VECTOR vDimensions = << 20.0, 20.0, 20.0>>
	VECTOR vVehCoords
	
	IF NOT IS_VECTOR_ZERO(vWarpCoordsForJskip)
	AND NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vWarpCoordsForJskip, vDimensions)
	
		J_SKIP_MP(vWarpCoordsForJskip, vDimensions)
		
		RETURN TRUE
	ELSE
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPlayerVeh[iVeh])
			IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niPlayerVeh[iVeh])
			
				vehDefend = NET_TO_VEH(serverBD.niPlayerVeh[iVeh])
				vVehCoords = GET_ENTITY_COORDS(vehDefend)
		
				IF IS_VEHICLE_SEAT_FREE(vehDefend, VS_DRIVER)
					// Driver
					TASK_ENTER_VEHICLE(PLAYER_PED_ID(), vehDefend, -1, VS_DRIVER, PEDMOVEBLENDRATIO_RUN, ECF_WARP_PED)
					
					RETURN TRUE
				ELSE
					IF IS_VEHICLE_SEAT_FREE(vehDefend, VS_FRONT_RIGHT)
						// Passenger
						TASK_ENTER_VEHICLE(PLAYER_PED_ID(), vehDefend, -1, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_RUN, ECF_WARP_PED)
						
						RETURN TRUE
					ELSE
						// Nearby
						SET_ENTITY_COORDS(PLAYER_PED_ID(), vVehCoords)
						
						RETURN TRUE
					ENDIF
				ENDIF	
			ENDIF		
		ENDIF
	ENDIF

//	IF ARE_VECTORS_EQUAL(vWarpCoordsForJskip, << 0.0, 0.0, 0.0>>)
//		vWarpCoordsForJskip = GET_DROP_OFF_LOCATION()
//		
//		
//		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [HANDLE_J_SKIP] WARPING TO ", vWarpCoordsForJskip)
//		
//		IF NOT ARE_VECTORS_EQUAL(vWarpCoordsForJskip, << 0.0, 0.0, 0.0>>)
//			J_SKIP_MP(vWarpCoordsForJskip, << 20.0, 20.0, 20.0>>)
//		ENDIF
//		
//	ENDIF
//	
//	IF NOT ARE_VECTORS_EQUAL(vWarpCoordsForJskip, << 0.0, 0.0, 0.0>>)
//		
//		MAINTAIN_J_SKIP_WARP()
//		IF NOT g_bDoJSkip
//			vWarpCoordsForJskip = << 0.0, 0.0, 0.0 >>
//			RETURN TRUE
//		ELSE
//			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [HANDLE_J_SKIP] WAITING FOR g_bDoJSkip")
//		ENDIF
//	
//	ENDIF

	RETURN FALSE
ENDFUNC


PROC MAINTAIN_DEBUG_KEYS()
	SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerData
	
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPDebugBitset,biPDebug_PressedS)
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S) 
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPDebugBitset, biPDebug_PressedS)
			
			TickerData.TickerEvent = TICKER_EVENT_PLAYER_CHEATED_SPASS
			TickerData.playerID = PLAYER_ID()
			BROADCAST_TICKER_EVENT(TickerData, ALL_PLAYERS_ON_SCRIPT(TRUE))
			
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_DEBUG_KEYS] I S-PASSED ")
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPDebugBitset,biPDebug_PressedF)
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F) 
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPDebugBitset, biPDebug_PressedF)
			
			TickerData.TickerEvent = TICKER_EVENT_PLAYER_CHEATED_FFAIL
			TickerData.playerID = PLAYER_ID()
			BROADCAST_TICKER_EVENT(TickerData, ALL_PLAYERS_ON_SCRIPT(TRUE))
			
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_DEBUG_KEYS] I F-FAILED ")
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPDebugBitset, biPDebug_PressedJ)
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J) 
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPDebugBitset, biPDebug_PressedJ)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_DEBUG_KEYS] I J-SKIPPED ")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPDebugBitset, biPDebug_PressedJ)
		IF HANDLE_J_SKIP()
			CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPDebugBitset, biPDebug_PressedJ)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_DEBUG_KEYS] I J-SKIP FINISHED ")
		ENDIF
	ENDIF
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD1) 
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPlayerVeh[0])
			IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niPlayerVeh[0])
				NETWORK_EXPLODE_HELI(NET_TO_VEH(serverBD.niPlayerVeh[0]))
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_DEBUG_KEYS] NETWORK_EXPLODE_HELI")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

#ENDIF

FUNC STRING GET_VEHICLE_TYPE_STRING()
	RETURN "" //GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(serverBD.VehModel)
ENDFUNC

FUNC STRING GET_VEH_BLIP_STRING()
//	IF serverBD.VehModel = RHINO
//		RETURN "ABLIP_TANK"			//"~BLIP_TEMP_6~"
//	ELIF IS_THIS_MODEL_A_PLANE(serverBD.VehModel)
//		RETURN "ABLIP_PLANE"		//"~BLIP_TEMP_6~"
//	ENDIF
//	
//	RETURN "ABLIP_HELI"				//"~BLIP_TEMP_6~"*/
	RETURN ""
ENDFUNC

//PURPOSE: Adds a blip to the Veh
PROC ADD_VEH_BLIP()

ENDPROC

PROC REMOVE_VEHICLE_BLIP()

ENDPROC

PROC INITIALISE_PED_REACTION_TIMES()
      INT iPed
      REPEAT MAX_TARGETS iPed
	  	IF serverBD.Targets[iPed].iReactionDelay = 0
            serverBD.Targets[iPed].iReactionDelay = GET_RANDOM_INT_IN_RANGE(0, 1000)
            PRINTLN("[GUNRUN] - [VALKYRIE DEFEND] - INITIALISE_PED_REACTION_TIMES - Ped #", iPed, "'s reaction delay set to ", serverBD.Targets[iPed].iReactionDelay, "ms.")
		ENDIF
      ENDREPEAT
ENDPROC

FUNC BOOL HAS_PEDS_REACTION_DELAY_EXPIRED(INT iPed)
      IF HAS_NET_TIMER_EXPIRED(serverBD.Targets[iPed].stReactionDelayTimer, serverBD.Targets[iPed].iReactionDelay)
            RETURN TRUE
      ENDIF
      RETURN FALSE
ENDFUNC





//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  ENTITY CREATION                  /////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////
///    			VEHICLE   				///
///////////////////////////////////////////
//PURPOSE: Create the Veh
FUNC BOOL CREATE_VEH()

	
	RETURN TRUE
ENDFUNC

//PURPOSE: Creates the Veh and Driver that will deliver the Vehicle
FUNC BOOL CREATE_MAIN_VEHICLE()

	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Determine if the participant iPart thinks any of the targets is stuck / damaged / being aimed at etc.
PROC SERVER_PART_PROCESS_TARGETS(INT iPart)
	
	#IF IS_DEBUG_BUILD
	PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart))
	#ENDIF
	INT iTarget
	
	REPEAT MAX_TARGETS iTarget
		
//		IF NOT IS_BIT_SET(serverBD.iEnemyPedSpookedServer, iTarget)
//			IF IS_BIT_SET(playerBD[iPart].iEnemyPedSpookedClient, iTarget)
//				SET_BIT(serverBD.iEnemyPedSpookedServer, iTarget)
//				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] Server thinks this player ", GET_PLAYER_NAME(PlayerId), " thinks this target is spooked ", iTarget)
//			ENDIF
//		ENDIF
		
		IF NOT IS_BIT_SET(ServerBD.iTargetAimedAtBitSet, iTarget)
			IF IS_BIT_SET(playerBD[iPart].iPlayerAimAtTargetBitSet, iTarget)
				SET_BIT(ServerBD.iTargetAimedAtBitSet, iTarget)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_CONTRA_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] Server thinks this player ", GET_PLAYER_NAME(PlayerId), " is aiming at target ", iTarget)
			ENDIF
		ENDIF
		
//		IF NOT IS_BIT_SET(ServerBD.iTargetVehStuckBitSet, iTarget)
//			IF IS_BIT_SET(playerBD[iPart].iPlayerTargetVehStuckBitSet, iTarget)
//				SET_BIT(ServerBD.iTargetVehStuckBitSet, iTarget)
//				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_CONTRA_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] Server thinks this player ", GET_PLAYER_NAME(PlayerId), " thinks this target is stuck target ", iTarget)
//			ENDIF
//		ENDIF
		
		IF NOT IS_BIT_SET(ServerBD.iTargetNearbyShotsBitset, iTarget)
			IF IS_BIT_SET(playerBD[iPart].iPlayerShotsNearTargetBitSet, iTarget)
				SET_BIT(ServerBD.iTargetNearbyShotsBitset, iTarget)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_CONTRA_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] Server thinks this player ", GET_PLAYER_NAME(PlayerId), " detected shots near target ", iTarget)
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(ServerBD.iTargetSpookedByAreaCheck, iTarget)
			IF IS_BIT_SET(playerBD[iPart].iPlayerTargetSpookedByAreaCheck, iTarget)
				SET_BIT(ServerBD.iTargetSpookedByAreaCheck, iTarget)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_CONTRA_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] Server thinks this player ", GET_PLAYER_NAME(PlayerId), " detectedtarget spooked by area check ", iTarget)
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(serverBD.iTargetKilledBitset, iTarget)
			IF IS_BIT_SET(playerBD[iPart].iPlayerKilledTargetBitset, iTarget)
				SET_BIT(serverBD.iTargetKilledBitset, iTarget)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] Server thinks this player ", GET_PLAYER_NAME(PlayerId), " killed target ", iTarget)
			ELIF IS_BIT_SET(playerBD[iPart].iNonPartPlayerKilledTargetBitset, iTarget) 
				IF NOT IS_BIT_SET(serverBD.iTargetKilledByRivalBitset, iTarget)
					SET_BIT(serverBD.iTargetKilledByRivalBitset, iTarget)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] Server thinks this player ", GET_PLAYER_NAME(PlayerId), " detected target ", iTarget, " killed by non-player part")
				ENDIF
			ELIF IS_BIT_SET(playerBD[iPart].iNonPlayerKilledTargetBitset, iTarget) 
				SET_BIT(serverBD.iTargetKilledBitset, iTarget)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] Server thinks this player ", GET_PLAYER_NAME(PlayerId), " detected target ", iTarget, " killed by something other than a player")
			
			ENDIF
		ENDIF
		
		IF NOT IS_SERVER_TARGET_BIT_SET(iTarget, ENEMY_PED_REACHED_DRIVE_TO_COORD)
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iEnemyPedReachedInCarGoto,iTarget)
				SET_SERVER_TARGET_BIT(iTarget, ENEMY_PED_REACHED_DRIVE_TO_COORD)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] Server thinks this player ", GET_PLAYER_NAME(PlayerId), " detected target ", iTarget, " reached goto")
			ENDIF
		ENDIF
	ENDREPEAT
	
	iTarget = 0
	REPEAT MAX_ENEMY_VEH iTarget
		IF NOT IS_SERVER_ENEMY_VEH_BIT_SET(iTarget, ENEMY_VEH_BIT_ATTACKED_BY_PLAYER)
			IF IS_BIT_SET(playerBD[iPart].iPlayerTargetVehDamagedBitSet, iTarget)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] Server thinks this player ", GET_PLAYER_NAME(PlayerId), " detected this veh being attacked ", iTarget)
				SET_SERVER_ENEMY_VEH_BIT(iTarget, ENEMY_VEH_BIT_ATTACKED_BY_PLAYER)
			ENDIF
		ENDIF
		
		IF IS_SERVER_ENEMY_VEH_BIT_SET(iTarget, ENEMY_VEH_BIT_DO_NEARBY_PLAYER_CHECK)
			IF NOT IS_SERVER_ENEMY_VEH_BIT_SET(iTarget, ENEMY_VEH_BIT_PLAYER_NEARBY)
				IF IS_BIT_SET(playerBD[iPart].iPlayerTargetVehNearbyBitSet, iTarget)
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] Server thinks this player ", GET_PLAYER_NAME(PlayerId), " is nearby this enemy veh ", iTarget)
					SET_SERVER_ENEMY_VEH_BIT(iTarget, ENEMY_VEH_BIT_PLAYER_NEARBY)
				ENDIF
			ENDIF
		ENDIF
		
		
	ENDREPEAT
	

	
ENDPROC

/// PURPOSE:
///    Lock the doors on the each of the enemy vehicle so particpant iPart can't enter
/// PARAMS:
///    iPart - 
PROC MAINTAIN_LOCK_TARGET_DOORS_FOR_PART(INT iPart)

	
	INT i
	IF NOT IS_BIT_SET(serverBD.iLockDoorsBitset, iPart)
		REPEAT MAX_TARGETS i
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[i].niVeh)
				IF IS_NET_VEHICLE_DRIVEABLE(serverBD.Targets[i].niVeh)
					SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.Targets[i].niVeh), 
							NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart)), TRUE)
				
					SET_BIT(serverBD.iLockDoorsBitset, iPart)
				ENDIF
			ELSE
				SET_BIT(serverBD.iLockDoorsBitset, iPart)
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

/// PURPOSE:
///    Timeout to handle edge cases where an AI ped is killed somehow, but nobody claims the kill
PROC MAINTAIN_TARGETS_KILLED_FAILSAFE()

	
	INT i
	REPEAT MAX_TARGETS i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[i].niPed)
			IF NOT IS_BIT_SET(serverBD.iTargetKilledBitset, i)
				IF NOT HAS_NET_TIMER_STARTED(serverBD.timeKillFailsafe[i])
					IF IS_NET_PED_INJURED(serverBD.Targets[i].niPed)
						START_NET_TIMER(serverBD.timeKillFailsafe[i])
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_TARGETS_KILLED_FAILSAFE] started fail-safe timer for target ", i )
					ENDIF
				ELSE
					IF HAS_NET_TIMER_EXPIRED(serverBD.timeKillFailsafe[i], 10000)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_TARGETS_KILLED_FAILSAFE] Failsafe timer expired! Ped ", i)
						SET_BIT(serverBD.iTargetKilledBitset, i)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC MAINTAIN_MAX_PLAYER_CHECKS_CLIENT()
	INT iPlayer

	INT iTempNumValidParts

	INT iPart
	PLAYER_INDEX player
	PARTICIPANT_INDEX part
	
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		player = INT_TO_PLAYERINDEX(iPlayer)
		IF NETWORK_IS_PLAYER_ACTIVE(player)
			IF NETWORK_IS_PLAYER_A_PARTICIPANT(player)
				part = NETWORK_GET_PARTICIPANT_INDEX(player)
				iPart = NATIVE_TO_INT(part)
				IF NOT IS_PLAYER_SCTV(player)
					iTempNumValidParts++
					
					IF iPart = 0 ENDIF
					IF player != PLAYER_ID()
						
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF iNumValidParts != iTempNumValidParts
		iNumValidParts = iTempNumValidParts
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MAX_PLAYER_CHECKS_CLIENT] iNumValidParts updated to ", iNumValidParts)
	ENDIF
	
	
ENDPROC

PROC SEND_PACKAGE_COLLECTED_TICKER(PLAYER_INDEX playerCollect)
	SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
	TickerEventData.TickerEvent = TICKER_EVENT_GUNRUN_COLLECT
	TickerEventData.playerID = playerCollect
	INT iPlayers = ALL_PLAYERS_ON_SCRIPT()
	BROADCAST_TICKER_EVENT(TickerEventData, iPlayers)
ENDPROC

PROC SEND_PACKAGE_DROPPED_TICKER(PLAYER_INDEX playrerDropped)
	SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
	TickerEventData.TickerEvent = TICKER_EVENT_GUNRUN_DROP
	TickerEventData.playerID = playrerDropped
	INT iPlayers = ALL_PLAYERS_ON_SCRIPT()
	BROADCAST_TICKER_EVENT(TickerEventData, iPlayers)
ENDPROC

PROC SEND_PACKAGE_DELIVERED_TICKER(PLAYER_INDEX playerDelivered)
	SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
	TickerEventData.TickerEvent = TICKER_EVENT_GUNRUN_DELIVER
	TickerEventData.playerID = playerDelivered
	INT iPlayers = ALL_PLAYERS_ON_SCRIPT()
	BROADCAST_TICKER_EVENT(TickerEventData, iPlayers)
ENDPROC
//PURPOSE: Runs through the max num participents and does relevant checks
PROC MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
	INT iNumTargetsKilled = 0
	INT iTarget = 0
//	INT iTempPartDriving = -1
	BOOL bSomeoneNotFinished = FALSE
//	BOOL bAllFinishedWarp = TRUE
//	INT iTempKilledByGangChase
	//INT iFriendly
	INT itempPackageHolder[MAX_GANG_BOSS_GB_PACKAGES]
//	BOOL bSomeoneCanSeePackage[MAX_GANG_BOSS_GB_PACKAGES]
	INT iTempPartDriving = -1

	INT iNumBombsDefused = 0
	INT i
	REPEAT MAX_GANG_BOSS_GB_PACKAGES i
		itempPackageHolder[i] = -1
	//	bSomeoneCanSeePackage[i] = FALSE
	ENDREPEAT
	
	BOOL bTempSOmeoneNearActionArea
	//For now we only need to do this check once the Veh is destroyed.
	IF serverBD.iServerGameState != GAME_STATE_END
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iStaggeredParticipant
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iStaggeredParticipant))
			//DOES_ENTITY_EXIST
				PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iStaggeredParticipant))
			//	PED_INDEX playerPedID =  GET_PLAYER_PED(PlayerId)
				IF NOT IS_PLAYER_SCTV(PlayerId)
					//PED_INDEX PlayerPedId = GET_PLAYER_PED(PlayerId)
					
					//** CHECKS THAT DON'T NEED THE PLAYER TO BE ALIVE **//
					
					
					//** CHECKS THAT REQUIRE THE PLAYER TO BE ALIVE **//
					IF IS_NET_PLAYER_OK(PlayerId)
						

					ENDIF
					
					IF NOT IS_SERVER_BIT_SET(biS_CleanupHeli)
						IF IS_BIT_SET(playerbd[iStaggeredParticipant].iPlayerBitSet, biP_CleanupHeli)
							PRINTLN("     ---------->     GB_GUNRUNNING_DEFEND - SCRIPT_EVENT_OK_TO_CLEANUP_DELIVERABLE_ENTITY, SET_SERVER_BIT(biS_CleanupHeli) ")
							SET_SERVER_BIT(biS_CleanupHeli)
						ENDIF
					ENDIF					
					
					IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
						//-- Lock enemy vehicle doors for each part
						MAINTAIN_LOCK_TARGET_DOORS_FOR_PART(iStaggeredParticipant)
						
						SERVER_PART_PROCESS_TARGETS(iStaggeredParticipant)
						
						IF IS_BIT_SET( playerbd[iStaggeredParticipant].iPlayerBitSet, biP_InEnemyTriggerArea)
						//OR IS_SERVER_BIT_SET( biS_ContrabandWrecked)
						OR IS_SERVER_BIT_SET( biS_ValkyrieDestroyed)
						OR playerbd[iStaggeredParticipant].iEnemyPedSpookedClient != 0
						OR serverBD.iTargetSpookedByAreaCheck != 0
							PRINTLN("JS-ENEMIESALERTED BIT SET")
							//SETTING CUSTOM SERVER BIT to kick off enemy
							SET_SERVER_BIT(biS_EnemiesAlerted)	
							//START_NET_TIMER(stReactionDelayTimer)
						ENDIF
						
						IF IS_BIT_SET( playerbd[iStaggeredParticipant].iPlayerBitSet, biP_BombTimerExpired)
								PRINTLN("JS-BOMB TIMER EXPIRED BIT SET")
							SET_SERVER_BIT(biS_BombTimerExpired)	
						ENDIF
						IF IS_BIT_SET( playerbd[iStaggeredParticipant].iPlayerBitSet, biP_BombTimerStarted)
								PRINTLN("JS-BOMB TIMER STARTED BIT SET")
							SET_SERVER_BIT(biS_BombTimerStarted)	
						ENDIF
						
					//--Within 50m of the objective vehicle?		
				//	VECTOR vLoc
				//	vLoc = GET_DROP_OFF_LOCATION()
							
						IF NOT IS_SERVER_BIT_SET(biS_WithinBombTextRange)
							IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_CloseEnoughToSendBombInfoText)
								SET_SERVER_BIT(biS_WithinBombTextRange)
							ENDIF
						ENDIF
						
					
						
						
						//-- Approached warehouse?
						IF NOT IS_SERVER_BIT_SET(biS_ApproachedWarehouse)
							IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_ApproachedWarehouse)
								SET_SERVER_BIT(biS_ApproachedWarehouse)
								
								//-- STore time so we can calcluate product to lose
								IF serverBD.iMeterValueWhenReachedArea = -1
									IF HAS_NET_TIMER_STARTED(serverBD.timeDuration)
										serverBD.iMeterValueWhenReachedArea = GET_GB_GB_GUNRUNNING_DEFEND_DURATION()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeDuration)
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] biS_ApproachedWarehouse set, Timer started, Set serverBD.iMeterValueWhenReachedArea = ", serverBD.iMeterValueWhenReachedArea)
									ELSE
										serverBD.iMeterValueWhenReachedArea = GET_GB_GB_GUNRUNNING_DEFEND_DURATION()
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] biS_ApproachedWarehouse set, Timer NOT started, Set serverBD.iMeterValueWhenReachedArea = ", serverBD.iMeterValueWhenReachedArea)
									ENDIF
								ENDIF
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SERVER THINKS THIS PLAYER APPROACHED WAREHOUSE", GET_PLAYER_NAME(PlayerID))
							ENDIF
						ENDIF
						
						//--Someone Near Action area?
						IF NOT bTempSOmeoneNearActionArea
							IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_NearActionArea)
								bTempSOmeoneNearActionArea = TRUE
							ENDIF
						ENDIF
					//	FLOAT tempFloat
						//tempFloat = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(playerPedID, GET_GB_GUNRUNNING_DEFEND_CONTRABAND_VEHICLE_LOCATION(serverBD.iVariation, serverBD.iBusinessToDefend))
						//set bit if player is within range of the objective.
						//IF tempFloat <= actionAreaRange
//						IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_GB_GUNRUNNING_DEFEND_CONTRABAND_VEHICLE_LOCATION(serverBD.iVariation, serverBD.iBusinessToDefend)) <= actionAreaRange * actionAreaRange
//							
//							IF !IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_NearActionArea)
//								SET_BIT(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_NearActionArea)
//							ENDIF
//							PRINTLN("JS- SET BIT, biP_NearActionArea")
//						ENDIF
						//--Someone want to defuse bomb?
						i = 0
						IF serverBD.iVariation = GB_GUNRUNNING_DEFEND_DEFUSAL
							REPEAT MAX_BOMBS i
								IF serverBD.iBombDefusedByPlayer[i] = -1
								 	IF serverBD.iPlayerCanDefuseBomb[i] = -1
									 	IF IS_BIT_SET(playerBD[iStaggeredParticipant].iWantToDefuseBomb, i)
											serverBD.iPlayerCanDefuseBomb[i] = NATIVE_TO_INT(PlayerId)
											CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] server says bomb ", i, " can be defused by player ", GET_PLAYER_NAME(PlayerID))
										ENDIF
									ELSE
										IF IS_BIT_SET(playerBD[iStaggeredParticipant].iDefusedBomb, i)
											serverBD.iGlobalBombCount++
											serverBD.iBombDefusedByPlayer[i] = NATIVE_TO_INT(PlayerId)
											CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] server thinks bomb ", i, " was defused by player ", GET_PLAYER_NAME(PlayerID))
										ElIF serverBD.iPlayerCanDefuseBomb[i] = NATIVE_TO_INT(PlayerId)
										AND !IS_BIT_SET(playerBD[iStaggeredParticipant].iWantToDefuseBomb, i)
										AND !IS_BIT_SET(playerBD[iStaggeredParticipant].iStartedDefusingBomb,i)
											CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] server player quit hacking ", i, " was defused by player ", GET_PLAYER_NAME(PlayerID))
											serverBD.iPlayerCanDefuseBomb[i] = -1
										ENDIF
									ENDIF
								ENDIF
								
								//set server bit for sending a wave of enemies when there's a bomb being defused.
								IF !IS_BIT_SET(serverbd.iCanSpawnDefusalWave, i)
									IF IS_BIT_SET(playerBD[iStaggeredParticipant].iStartedDefusingBomb,i)
										SET_BIT(serverbd.iCanSpawnDefusalWave, i)
										SET_SERVER_BIT(biS_FirstBombDefusalInitiated)
										PRINTLN("JS-SERVER BIT SET iCanSpawnDefusalWave: ",i)
									ENDIF
								ENDIF
								
							ENDREPEAT
						ENDIF
						
						//-- Count kills by gang Chasem
						
//						IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllInitalTargetsKilled)
//							IF playerBD[iStaggeredParticipant].iKilledByGangChaseCount > 0
//								iTempKilledByGangChase += playerBD[iStaggeredParticipant].iKilledByGangChaseCount
//							ENDIF
//						ENDIF
						
						
						//-- Packages
						i = 0
						IF serverBD.iVariation = GB_GUNRUNNING_DEFEND_VALKYRIE
							REPEAT MAX_GANG_BOSS_GB_PACKAGES i
								IF GET_SERVER_MISSION_STATE() >= GAME_STATE_RUNNING
									IF itempPackageHolder[i] = -1
										IF serverBD.packagesServer.iDeliveredByPlayer[i] = -1
											IF DOES_PART_HAVE_GB_PACKAGE(iStaggeredParticipant, i)
												
												itempPackageHolder[i] = iStaggeredParticipant
											ENDIF
										ENDIF
									ENDIF
									
									IF serverBD.packagesServer.iDeliveredByPlayer[i] = -1
										IF HAS_PART_DELIVERED_GB_PACKAGE(iStaggeredParticipant, i)
											serverBD.packagesServer.iDeliveredByPlayer[i] = NATIVE_TO_INT(PlayerId)
											CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] Server thinks this player ", GET_PLAYER_NAME(playerID), " has delivered package ", i)
										ENDIF
									ENDIF
									
									//-- For syncing the animation of the parahute descending / landing
									IF NOT IS_BIT_SET(serverBD.iCrateBS[i], CRATE_BS_ANIM_STARTED)
										IF IS_BIT_SET(playerBD[iStaggeredParticipant].iCrateBS[i], CRATE_BS_ANIM_STARTED)
											SET_BIT(serverBD.iCrateBS[i], CRATE_BS_ANIM_STARTED)
											PRINTLN("[VEH_EXPORT] participant ", iStaggeredParticipant, " CRATE_BS_ANIM_STARTED ", i, ". PlayerId = ", GET_PLAYER_NAME(PlayerId))
										ENDIF
									ENDIF
									
									IF NOT IS_BIT_SET(serverBD.iCrateBS[i], CRATE_BS_ANIM_PLAYING)
										IF IS_BIT_SET(playerBD[iStaggeredParticipant].iCrateBS[i], CRATE_BS_ANIM_PLAYING)
											SET_BIT(serverBD.iCrateBS[i], CRATE_BS_ANIM_PLAYING)
											PRINTLN("[VEH_EXPORT] participant ", iStaggeredParticipant, " CRATE_BS_ANIM_PLAYING ", i, ". PlayerId = ", GET_PLAYER_NAME(PlayerId))
										ENDIF
									ENDIF
									
									IF NOT IS_BIT_SET(serverBD.iCrateBS[i], CRATE_BS_ANIM_DONE)
										IF IS_BIT_SET(playerBD[iStaggeredParticipant].iCrateBS[i], CRATE_BS_ANIM_DONE)
											SET_BIT(serverBD.iCrateBS[i], CRATE_BS_ANIM_STARTED)
											PRINTLN("[VEH_EXPORT] participant ", iStaggeredParticipant, " CRATE_BS_ANIM_DONE ", i, ". PlayerId = ", GET_PLAYER_NAME(PlayerId))
										ENDIF
									ENDIF
								ENDIF
							ENDREPEAT
						ENDIF
						IF serverBD.iVariation = GB_GUNRUNNING_DEFEND_VALKYRIE
							//-- Van wrecked?
							IF NOT IS_SERVER_BIT_SET(biS_ValkyrieDestroyed)
								IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_ValkyrieWrecked)
									SET_SERVER_BIT(biS_ValkyrieDestroyed)
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_CONTRA_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SERVER THINKS THIS PLAYER THINKS THE VALKYRIE IS DESTROYED ", GET_PLAYER_NAME(PlayerID))
								ENDIF
							ENDIF
						ENDIF
						IF serverBD.iVariation = GB_GUNRUNNING_DEFEND_VALKYRIE
						OR serverBD.iVariation = GB_GUNRUNNING_DEFEND_DEFUSAL
							//-- Van wrecked?
							IF NOT IS_SERVER_BIT_SET(biS_ContrabandWrecked)
								IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_VanWrecked)
									SET_SERVER_BIT(biS_ContrabandWrecked)
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_CONTRA_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SERVER THINKS THIS PLAYER THINKS THE VAN IS WRECKED ", GET_PLAYER_NAME(PlayerID))
								ENDIF
							ENDIF

							
							//-- Contraband vehicle recovered?
							IF iTempPartDriving = -1
								IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_InVeh)
									iTempPartDriving = iStaggeredParticipant
								ENDIF
							ENDIF
							
							//-- Contraband vehicle delivered?
							IF NOT IS_SERVER_BIT_SET(biS_VehDelivered)
								IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_VehDelivered)
									SET_SERVER_BIT(biS_VehDelivered)
									serverBD.iPlayerDeliveredVeh = serverBD.iPlayerDrivingContraVehicle
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SERVER THINKS THIS PLAYER DELIVERED VEHICLE ", GET_PLAYER_NAME(PlayerId), " serverBD.iPlayerDeliveredVeh = ", serverBD.iPlayerDeliveredVeh, " WHO IS PLAYER ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(serverBD.iPlayerDeliveredVeh)))
								ENDIF
							ENDIF
							
							
						ENDIF
						

						
						//-- Everyone finished?
						IF NOT IS_SERVER_BIT_SET(biS_AllPlayersFinished)
							IF NOT bSomeoneNotFinished
								IF HAS_MISSION_END_CONDITION_BEEN_MET()
									IF NOT IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_Finished)
										bSomeoneNotFinished = TRUE
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SERVER THINKS THIS PLAYER NOT FINISHED ", GET_PLAYER_NAME(PlayerId))
									ENDIF
								ELSE
									bSomeoneNotFinished = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						//-- Has boss transfered to warehouse?
						IF NOT IS_SERVER_BIT_SET(biS_ContrabandRemovedAtEnd)
							IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_ContrabandRemovedAtEnd)
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] BOSS TRANSFERED CONTRABAND")
								SET_SERVER_BIT(biS_ContrabandRemovedAtEnd)
							ENDIF
						ENDIF
					ENDIF	
					
					#IF IS_DEBUG_BUILD
					//-- Debug s-skip
					IF NOT IS_SERVER_BIT_SET(biS_AllTargetsKilled)
					AND NOT IS_SERVER_BIT_SET(biS_VehDelivered) 
						IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPDebugBitset, biPDebug_PressedS)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SERVER THINKS THIS PLAYER S-PASSED ", GET_PLAYER_NAME(PlayerId))

							SET_SERVER_BIT(biS_AllTargetsKilled) 
							
							ServerBD.bSomeoneSPassed = TRUE
						ENDIF
					ENDIF
					
					//-- Debug F-Fail
					IF NOT IS_SERVER_BIT_SET(biS_DurationExpired)
					AND NOT IS_SERVER_BIT_SET(biS_ContrabandLost) 
						IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPDebugBitset,biPDebug_PressedF)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SERVER THINKS THIS PLAYER F-FAILED ", GET_PLAYER_NAME(PlayerId))

							SET_SERVER_BIT(biS_TimeoutExpired) 
							
						ENDIF
					ENDIF
					
					#ENDIF
				ENDIF
			ELSE
				//-- Part not active
				
				//-- CHeck for Boss who launched the job quiting the session
				IF NOT IS_SERVER_BIT_SET(biS_BossLaunchedQuit)
					IF serverBD.iBossPartWhoLaunched > -1
						IF iStaggeredParticipant = serverBD.iBossPartWhoLaunched
							SET_SERVER_BIT(biS_BossLaunchedQuit)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SERVER THINKS BOSS WHO LAUNCHED WORK QUIT SESSION erverBD.iBossPartWhoLaunched = ", serverBD.iBossPartWhoLaunched)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF GET_SERVER_MISSION_STATE() > GAME_STATE_INI
			MAINTAIN_TARGETS_KILLED_FAILSAFE()
			
			REPEAT MAX_TARGETS iTarget
				IF IS_BIT_SET(serverBD.iTargetKilledBitset, iTarget)
					iNumTargetsKilled++
				ENDIF
			ENDREPEAT
			
			//-- Number of targets killed
			IF serverBD.iNumTargetsKilled != iNumTargetsKilled
				serverBD.iNumTargetsKilled = iNumTargetsKilled
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] serverBD.iNumTargetsKilled UPDATED TO ", serverBD.iNumTargetsKilled)
			ENDIF
			
			
			//-- All targets killed?
			INT iNumPeds = GET_GB_GUNRUNNING_DEFEND_NUMBER_OF_INITIAL_PEDS_FOR_VARIATION(serverBD.iVariation, serverbd.iBusinessToDefend)
			IF NOT IS_SERVER_BIT_SET(biS_AllInitalTargetsKilled)
				IF iNumTargetsKilled >= iNumPeds
					SET_SERVER_BIT(biS_AllInitalTargetsKilled)
					
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SET biS_AllInitalTargetsKilled AS iNumTargetsKilled = ", iNumTargetsKilled, " GET_NUMBER_OF_ENEMIES_FOR_VARIATION = ", iNumPeds)
					
				ENDIF
			ENDIF
			
			//-- All bombs defused?
			IF serverBD.iVariation = GB_GUNRUNNING_DEFEND_DEFUSAL
				IF NOT IS_SERVER_BIT_SET(biS_AllBombsDefused)
					i = 0
					iNumBombsDefused = 0
					REPEAT MAX_BOMBS i
						IF serverBD.iBombDefusedByPlayer[i] > -1
							iNumBombsDefused++
						ENDIF
					ENDREPEAT
					IF iNumBombsDefused >= GET_GUNRUNNING_DEFEND_NUMBER_OF_BOMBS_FOR_BUSINESS()
						SET_SERVER_BIT(biS_AllBombsDefused)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] Set biS_AllBombsDefused, iNumBombsDefused = ", iNumBombsDefused, " >= GET_GUNRUNNING_DEFEND_NUMBER_OF_BOMBS_FOR_BUSINESS() = ", GET_GUNRUNNING_DEFEND_NUMBER_OF_BOMBS_FOR_BUSINESS())
					ENDIF
				ENDIF
			ENDIF
			

			
		//	PLAYER_INDEX playerDriving
			
			//-- Who's driving contraband vehicle
			IF serverBD.iVariation = GB_GUNRUNNING_DEFEND_VALKYRIE
			OR serverBD.iVariation = GB_GUNRUNNING_DEFEND_DEFUSAL
				PLAYER_INDEX playerDriving
				IF NOT HAS_MISSION_END_CONDITION_BEEN_MET()
					IF serverBD.iPartDrivingContraVehicle != iTempPartDriving
						serverBD.iPartDrivingContraVehicle = iTempPartDriving
						IF serverBD.iPartDrivingContraVehicle > -1
							IF NOT IS_SERVER_BIT_SET(biS_InVanAtLeastOnce)
								SET_SERVER_BIT( biS_InVanAtLeastOnce)
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] Set biS_InVanAtLeastOnce")
							ENDIF
							IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(serverBD.iPartDrivingContraVehicle))
								playerDriving = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.iPartDrivingContraVehicle))	
								serverBD.iPlayerDrivingContraVehicle = NATIVE_TO_INT(playerDriving)
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] serverBD.iPartDrivingContraVehicle now ", serverBD.iPartDrivingContraVehicle, " who is player ", GET_PLAYER_NAME(playerDriving))	
							ENDIF
						ELSE
							serverBD.iPlayerDrivingContraVehicle = -1
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] serverBD.iPartDrivingContraVehicle now ", serverBD.iPartDrivingContraVehicle)
						ENDIF 
					ENDIF
				ENDIF
			ENDIF
			
			
			//-- someone still near action area: DEFUSAL?
			IF bTempSOmeoneNearActionArea
				IF !IS_SERVER_BIT_SET(biS_SomeoneNearActionArea)
					SET_SERVER_BIT(biS_SomeoneNearActionArea)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] Set biS_SomeoneNearActionArea") 
				ENDIF
			ENDIF
			
			
			//-- someone still near action area: VALKYRIE?
			IF serverbd.iVariation = GB_GUNRUNNING_DEFEND_VALKYRIE
				IF IS_SERVER_BIT_SET(biS_ValkyrieDestroyed)
					IF !IS_SERVER_BIT_SET(biS_SomeoneNearActionArea)
						SET_SERVER_BIT(biS_SomeoneNearActionArea)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] Set biS_SomeoneNearActionArea") 
					ENDIF
				ENDIF
			ENDIF
			
			//-- Track who is holding (or delivered) each package
			i = 0
			INT iPlayer
			PLAYER_INDEX playerTemp
			IF serverBD.iVariation = GB_GUNRUNNING_DEFEND_VALKYRIE
				REPEAT MAX_PACKAGES i
					IF serverBD.packagesServer.iCurrentpackageHolder[i] != itempPackageHolder[i]
						//-- Tickers on update
						IF GET_SERVER_MISSION_STATE() > GAME_STATE_INI
							IF IS_BIT_SET(serverBD.packagesServer.iServerPackageBitset, SERVER_GB_PACKAGE_ALLOW_COLLECT)
								IF itempPackageHolder[i] = -1
									IF serverBD.packagesServer.iCurrentpackageHolder[i] > -1
										IF NOT HAS_GB_PACKAGE_BEEN_DELIVERED(serverBD.packagesServer, i, iPlayer)
										//	IF serverBD.eEndReason = eENDREASON_NO_REASON_YET
												playerTemp = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.packagesServer.iCurrentpackageHolder[i]))
												IF playerTemp != INVALID_PLAYER_INDEX()
													CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER]  SEND_PACKAGE_DROPPED_TICKER Player - ", GET_PLAYER_NAME(playerTemp))
													SEND_PACKAGE_DROPPED_TICKER(playerTemp)
												ENDIF
										//	ENDIF
										ELSE
											CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER]  SEND_PACKAGE_DELIVERED_TICKER Player - ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iPlayer)))
											SEND_PACKAGE_DELIVERED_TICKER(INT_TO_PLAYERINDEX(iPlayer))
										ENDIF
									ENDIF
								ELSE
									IF itempPackageHolder[i] > -1
										playerTemp =  NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(itempPackageHolder[i]))
										IF playerTemp != INVALID_PLAYER_INDEX()
											CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SEND_PACKAGE_COLLECTED_TICKER Player - ", GET_PLAYER_NAME(playerTemp))
											CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SEND_PACKAGE_COLLECTED_TICKER Playertemp Value =  ", NATIVE_TO_INT( playerTemp) )
											SEND_PACKAGE_COLLECTED_TICKER(playerTemp)
										ENDIF
											
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						serverBD.packagesServer.iCurrentpackageHolder[i] = itempPackageHolder[i]
						
						#IF IS_DEBUG_BUILD
							playerTemp = GET_GB_PACKAGE_HOLDER_FROM_SERVER_AS_PLAYER(serverBD.packagesServer, i)
							IF playerTemp != INVALID_PLAYER_INDEX()
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] iCurrentpackageHolder[", i, "] now part ", serverBD.packagesServer.iCurrentpackageHolder[i], " who is player ", GET_PLAYER_NAME(playerTemp))
							ELSE
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] iCurrentpackageHolder[", i, "] now ", serverBD.packagesServer.iCurrentpackageHolder[i])
							ENDIF
						#ENDIF
					ENDIF
					

				ENDREPEAT
			ENDIF
			

			
			
			
			
			//--All finished?
			IF NOT IS_SERVER_BIT_SET(biS_AllPlayersFinished)
				IF NOT bSomeoneNotFinished
					SET_SERVER_BIT(biS_AllPlayersFinished)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SET biS_AllPlayersFinished")
				ENDIF
			ENDIF
		ENDIF
		//--Someone quit lobby whilst defusing?
		IF GB_GET_NUM_GOONS_IN_LOCAL_GANG() != iGangCount
			iGangCount = GB_GET_NUM_GOONS_IN_LOCAL_GANG()
			REPEAT MAX_BOMBS i
				IF serverBD.iPlayerCanDefuseBomb[i] != -1
					IF !NETWORK_IS_PLAYER_ACTIVE(INT_TO_PLAYERINDEX(serverBD.iPlayerCanDefuseBomb[i]))
						serverBD.iPlayerCanDefuseBomb[i] = -1
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] serverBD.iPlayerCanDefuseBomb[i] = -1")
					ENDIF
				ENDIF	
			ENDREPEAT
		ENDIF	
	ENDIF
ENDPROC


PROC DISABLE_PLAYER_MISSION_VEHICLE_CONTROLS_THIS_FRAME(BOOL bDisableExit = TRUE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HANDBRAKE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_UD)
	IF bDisableExit
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
	ENDIF
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ROLL_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_PITCH_UD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_YAW_LEFT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_YAW_LEFT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_UP)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_DOWN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_UNDERCARRIAGE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_NEXT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_PREV_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_TARGET_LEFT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_TARGET_RIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_VERTICAL_FLIGHT_MODE)
ENDPROC

PROC STOP_PLAYER_LEAVING_VEHICLE_THIS_FRAME()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
	ENDIF
ENDPROC

PROC HANDLE_END_SHARD_FOR_BOMB_DEFUSAL()
//	STRING sOrganization
//	HUD_COLOURS hclPlayer
//	sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(PLAYER_ID())
//	hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID())	
//	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] [HANDLE_END_SHARD_FOR_DEFUSAL] sOrganization = ", sOrganization)
	
	
	IF IS_SERVER_BIT_SET(biS_AllPackagesDelivered)
	OR IS_SERVER_BIT_SET(biS_VehDelivered)
		//SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "DGUN_DBUSI", "DGUN_VLKSHA", sOrganization, hcMyGang)
		SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "DGUN_DBUSI", "DGUN_VLKSHA")
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] [HANDLE_END_SHARD_FOR_DEFUSAL] biS_VehDelivered / biS_AllPackagesDelivered set")
	ELIF IS_SERVER_BIT_SET(biS_DurationExpired)
	OR IS_SERVER_BIT_SET(biS_TimeoutExpired)
		//SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_FAIL, "DGUN_DBUSIO", "DGUN_FAIL", sOrganization, hcMyGang)
		SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_FAIL, "DGUN_DBUSIO", "DGUN_FAIL")
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] [HANDLE_END_SHARD_FOR_DEFUSAL] biS_DurationExpired set")
	ELIF IS_SERVER_BIT_SET(biS_ContrabandWrecked) 
		//SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_FAIL, "DGUN_DBUSIO", "DGUN_FAIL", sOrganization, hcMyGang)
		SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_FAIL, "DGUN_DBUSIO", "DGUN_FAIL")
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] [HANDLE_END_SHARD_FOR_DEFUSAL] biS_ContrabandWrecked set")
	ELIF IS_SERVER_BIT_SET(biS_ContrabandLost) 
		//SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_FAIL, "DGUN_DBUSIO", "DGUN_FAIL", sOrganization, hcMyGang) 
		SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_FAIL, "DGUN_DBUSIO", "DGUN_FAIL")
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] [HANDLE_END_SHARD_FOR_DEFUSAL] biS_ContrabandLost set")
	ENDIF
	//STRING sOrganization
	//HUD_COLOURS hclPlayer
	//sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(PLAYER_ID())
	//hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID())	
	//CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] [HANDLE_END_SHARD_FOR_BOMB_DEFUSAL] sOrganization = ", sOrganization)
	
	
//	IF IS_SERVER_BIT_SET(biS_AllBombsDefused)
//		SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "DBR_DEFEND", "DGUN_DEFSHA", sOrganization, hclPlayer) 
//		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] [HANDLE_END_SHARD_FOR_BOMB_DEFUSAL] biS_VehDelivered set")
//	ELIF IS_SERVER_BIT_SET(biS_DurationExpired)
//	OR IS_SERVER_BIT_SET(biS_TimeoutExpired)
//		SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_FAIL, "BIGM_BK_DEFOVR", "DBR_VANFSH", sOrganization, hclPlayer) 
//		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] [HANDLE_END_SHARD_FOR_BOMB_DEFUSAL] biS_DurationExpired set")
//	ENDIF
ENDPROC

PROC HANDLE_END_SHARD_FOR_VALKYRIE()
//	STRING sOrganization
//	HUD_COLOURS hclPlayer
//	sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(PLAYER_ID())
//	hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID())	
//	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] [HANDLE_END_SHARD_FOR_VALKYRIE] sOrganization = ", sOrganization)
	
	
	IF IS_SERVER_BIT_SET(biS_AllPackagesDelivered)
	OR IS_SERVER_BIT_SET(biS_VehDelivered)
		//SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "DGUN_DBUSI", "DGUN_VLKSHA", sOrganization, hcMyGang)
		SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "DGUN_DBUSI", "DGUN_VLKSHA")
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] [HANDLE_END_SHARD_FOR_VALKYRIE] biS_VehDelivered / biS_AllPackagesDelivered set")
	ELIF IS_SERVER_BIT_SET(biS_DurationExpired)
	OR IS_SERVER_BIT_SET(biS_TimeoutExpired)
		//SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_FAIL, "DGUN_DBUSIO", "DGUN_FAIL", sOrganization, hcMyGang) 
		SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_FAIL, "DGUN_DBUSIO", "DGUN_FAIL")
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] [HANDLE_END_SHARD_FOR_VALKYRIE] biS_DurationExpired set")
	ELIF IS_SERVER_BIT_SET(biS_ContrabandWrecked) 
		//SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_FAIL, "DGUN_DBUSIO", "DGUN_FAIL", sOrganization, hcMyGang) 
		SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_FAIL, "DGUN_DBUSIO", "DGUN_FAIL")
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] [HANDLE_END_SHARD_FOR_VALKYRIE] biS_ContrabandWrecked set")
	ELIF IS_SERVER_BIT_SET(biS_ContrabandLost) 
		//SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_FAIL, "DGUN_DBUSIO", "DGUN_FAIL", sOrganization, hcMyGang) 
		SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_FAIL, "DGUN_DBUSIO", "DGUN_FAIL")
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] [HANDLE_END_SHARD_FOR_VALKYRIE] biS_ContrabandLost set")
	ENDIF
ENDPROC

PROC HANDLE_END_SHARD_FOR_GUNRUNNING_DEFEND_VARIATION()
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] [HANDLE_END_SHARD_FOR_GUNRUNNING_DEFEND_VARIATION] Called,  variation ", serverBD.iVariation)
	SWITCH serverBD.iVariation
		
		
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
			HANDLE_END_SHARD_FOR_BOMB_DEFUSAL()
		BREAK
		
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			HANDLE_END_SHARD_FOR_VALKYRIE()
		BREAK
	ENDSWITCH
ENDPROC

PROC REMOVE_BOMB_BLIPS()
	INT i
	REPEAT MAX_BOMBS i
		IF DOES_BLIP_EXIST(blipBomb[i])
			REMOVE_BLIP(blipBomb[i])
		ENDIF
	ENDREPEAT
ENDPROC

PROC MAINTAIN_BOMB_CLEANUP()
	
INT i
	FOR i = 9 TO 11
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.niProp[i])
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverbd.niProp[i])
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GUNRUNNING_DEFEND [MAINTAIN_BOMB_CLEANUP] Prop  ",i," Deleted" )
				DELETE_NET_ID(serverbd.niProp[i])
			ENDIF
		ENDIF
	ENDFOR


ENDPROC

//PURPOSE: Checks to see if this is the end and the player should be rewarded.
PROC CHECK_FOR_REWARD()
//	BOOL bEventCompleted
	GANG_BOSS_MANAGE_REWARDS_DATA gbRewards
//	PLAYER_INDEX playerVeh = INVALID_PLAYER_INDEX()
//	PLAYER_INDEX playerKiller = INVALID_PLAYER_INDEX()
//	PLAYER_INDEX playerMyBoss = INVALID_PLAYER_INDEX()
//	STRING sOrganization
//	HUD_COLOURS hclPlayer
	
	INT iPart = PARTICIPANT_ID_TO_INT()
	PLAYER_INDEX player= PLAYER_ID()
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
		iPart = GET_MY_SPEC_TARGET_AS_PARTICIPANT_INT()
		player = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget
	ENDIF
	
	
	
	IF NOT IS_BIT_SET(playerBD[iPart].iPlayerBitSet, biP_Finished)
		IF NOT IS_BIT_SET(iBoolsBitSet, biL_WaitForShard)
			IF IS_SERVER_BIT_SET(biS_AllTargetsKilled)
//				//-- All targets killed
//				IF iPart = serverBD.iBossPartWhoLaunched
//				OR GET_MY_BOSS_PARTICPANT_ID() = serverBD.iBossPartWhoLaunched
//					//-- I'm the boss who launched event
//					// GB_SET_LOCAL_BIKER_CALL_BIT(eGB_LOCAL_BIKER_CALL_BITSET_NOTIFICATION_DEFEND_PASS)
//					IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_MINIMAL
//						sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(player)
//						hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(player)
//						SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "DBR_DEFEND", "BIGM_SHOOTS", sOrganization, hclPlayer) 
//					ENDIF
//					
//					IF player = PLAYER_ID()
//						IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneEndTelemetry)
//							GB_SET_COMMON_TELEMETRY_DATA_ON_END(TRUE, GB_TELEMETRY_END_WON, serverBD.iBusinessToDefend)
//							SET_BIT(iBoolsBitSet, biL_DoneEndTelemetry)
//						ENDIF
//						GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GUNRUNNING_DEFEND, TRUE, gbRewards)
//						CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
//					ENDIF
//					// RESET_GLOBAL_DEFEND_TIMERS(TRUE)
//					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] MY GANG LAUNCHED AND ALL TARGETS WERE KILLED")
//				ELIF DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
//
//					
//				ENDIF
//				
//			
//				SET_BIT(iBoolsBitSet, biL_WaitForShard)	
//			ELIF IS_SERVER_BIT_SET(biS_AllBombsDefused)
//				//-- all bombs defused
//				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] biS_AllBombsDefused")
//				IF GB_GET_PLAYER_UI_LEVEL(player) >= GB_UI_LEVEL_MINIMAL
//					HANDLE_END_SHARD_FOR_GUNRUNNING_DEFEND_VARIATION()
//				ENDIF
//								
//				IF player = PLAYER_ID()
//					IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneEndTelemetry)
//					//	GB_SET_COMMON_TELEMETRY_DATA_ON_END(TRUE, GB_TELEMETRY_END_WON, serverBD.iBusinessToDefend)
//						SET_BIT(iBoolsBitSet, biL_DoneEndTelemetry)
//					ENDIF
//					GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GUNRUNNING_DEFEND, TRUE, gbRewards)
//					CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
//				ENDIF
//				
//				SET_BIT(iBoolsBitSet, biL_WaitForShard)	

			ELIF IS_SERVER_BIT_SET(biS_AllPackagesDelivered)
		
					//-- all packages delivered
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] biS_AllPackagesDelivered")
					IF GB_GET_PLAYER_UI_LEVEL(player) >= GB_UI_LEVEL_MINIMAL
						GB_GUNRUN_SET_PASS_FAIL_CALL_DATA(GRV_INVALID,TRUE,TRUE,TRUE)
						HANDLE_END_SHARD_FOR_GUNRUNNING_DEFEND_VARIATION()
					PRINTLN("JS-ALLPACKAGESDELIVERED")
					ENDIF
									
					IF player = PLAYER_ID()
						IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneEndTelemetry)
							GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GUNRUNNING_DEFEND, TRUE, gbRewards)
							GB_GUNRUNNING_DEFEND_SET_DEFEND_TELEMETRY_DATA()		
							GB_SET_COMMON_TELEMETRY_DATA_ON_END(TRUE, GB_TELEMETRY_END_WON, INT_TO_ENUM(FACTORY_ID, serverBD.iBusinessToDefend))
							SET_BIT(iBoolsBitSet, biL_DoneEndTelemetry)
						ENDIF
						CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
					ENDIF
					

					
					SET_BIT(iBoolsBitSet, biL_WaitForShard)	

			ELIF IS_SERVER_BIT_SET(biS_VehDelivered)
				//-- Contraband vehicle delivered
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] biS_VehDelivered")
				IF GB_GET_PLAYER_UI_LEVEL(player) >= GB_UI_LEVEL_MINIMAL
					HANDLE_END_SHARD_FOR_GUNRUNNING_DEFEND_VARIATION()
					GB_GUNRUN_SET_PASS_FAIL_CALL_DATA(GRV_INVALID, TRUE,FALSE,TRUE)
				ENDIF
								
				IF player = PLAYER_ID()
					IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneEndTelemetry)
						GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GUNRUNNING_DEFEND, TRUE, gbRewards)
						GB_GUNRUNNING_DEFEND_SET_DEFEND_TELEMETRY_DATA()
						GB_SET_COMMON_TELEMETRY_DATA_ON_END(TRUE, GB_TELEMETRY_END_WON, INT_TO_ENUM(FACTORY_ID, serverBD.iBusinessToDefend))
						SET_BIT(iBoolsBitSet, biL_DoneEndTelemetry)
					ENDIF
					CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
				ENDIF
				

				
				SET_BIT(iBoolsBitSet, biL_WaitForShard)	
			ELIF IS_SERVER_BIT_SET(biS_DurationExpired)
				//-- Mission duration expired
				#IF IS_DEBUG_BUILD
				INT numberStillAlive = MAX_TARGETS - serverBD.iNumTargetsKilled
				#ENDIF
				
				IF PARTICIPANT_ID_TO_INT() = serverBD.iBossPartWhoLaunched
				OR GET_MY_BOSS_PARTICPANT_ID() = serverBD.iBossPartWhoLaunched
					IF GB_GET_PLAYER_UI_LEVEL(player) >= GB_UI_LEVEL_MINIMAL
						HANDLE_END_SHARD_FOR_GUNRUNNING_DEFEND_VARIATION()
						GB_GUNRUN_SET_PASS_FAIL_CALL_DATA(GRV_INVALID,FALSE,FALSE,TRUE)
					ENDIF
				
				
					IF player = PLAYER_ID()
						IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneEndTelemetry)
							GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GUNRUNNING_DEFEND, FALSE, gbRewards)
							GB_GUNRUNNING_DEFEND_SET_DEFEND_TELEMETRY_DATA()
							GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST, INT_TO_ENUM(FACTORY_ID, serverBD.iBusinessToDefend))
							SET_BIT(iBoolsBitSet, biL_DoneEndTelemetry)
						ENDIF
					ENDIF
					
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] MY GANG LAUNCHED AND TIME EXPIRED. THIS MANY TARGETS STILL ALIVE numberStillAlive = ", numberStillAlive)		
				ELSE
				ENDIF
				SET_BIT(iBoolsBitSet, biL_WaitForShard)
			ELIF IS_SERVER_BIT_SET(biS_ContrabandLost)
				//-- Lost sight of the contraband vehicle
				IF SHOULD_LOCAL_PLAYER_SEE_GB_GB_GUNRUNNING_DEFEND_HUD()

					HANDLE_END_SHARD_FOR_GUNRUNNING_DEFEND_VARIATION()
					GB_GUNRUN_SET_PASS_FAIL_CALL_DATA(GRV_INVALID,FALSE,FALSE,TRUE)
				ENDIF
				
				IF player = PLAYER_ID()
					IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneEndTelemetry)
						GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GUNRUNNING_DEFEND, FALSE, gbRewards)
						GB_GUNRUNNING_DEFEND_SET_DEFEND_TELEMETRY_DATA()
						GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST, INT_TO_ENUM(FACTORY_ID, serverBD.iBusinessToDefend))
						SET_BIT(iBoolsBitSet, biL_DoneEndTelemetry)
					ENDIF
				ENDIF
				
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] FAILED AS biS_ContrabandLost")
				
			//	iMeterMax = GET_GB_GB_GUNRUNNING_DEFEND_DURATION()
			//	iMeterCurrent =  serverBD.iMeterValueForContrabandLossAmount
				
				SET_BIT(iBoolsBitSet, biL_WaitForShard)
			ELIF IS_SERVER_BIT_SET(biS_ContrabandWrecked)
				//--Contraband vehilce destroyed
				IF SHOULD_LOCAL_PLAYER_SEE_GB_GB_GUNRUNNING_DEFEND_HUD()
					IF GB_GET_PLAYER_UI_LEVEL(player) >= GB_UI_LEVEL_MINIMAL
						
						HANDLE_END_SHARD_FOR_GUNRUNNING_DEFEND_VARIATION()
						GB_GUNRUN_SET_PASS_FAIL_CALL_DATA(GRV_INVALID,FALSE,FALSE,TRUE)
					ENDIF
				
				ENDIF
				
				IF player = PLAYER_ID()
					IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneEndTelemetry)
						GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GUNRUNNING_DEFEND, FALSE, gbRewards)
						GB_GUNRUNNING_DEFEND_SET_DEFEND_TELEMETRY_DATA()
						GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST, INT_TO_ENUM(FACTORY_ID, serverBD.iBusinessToDefend))
						SET_BIT(iBoolsBitSet, biL_DoneEndTelemetry)
					ENDIF
				ENDIF
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] FAILED AS biS_ContrabandWrecked")
				

				
				SET_BIT(iBoolsBitSet, biL_WaitForShard)
			ELIF IS_SERVER_BIT_SET(biS_TimeoutExpired)
				//-- 30 minute timeout expired
				IF SHOULD_LOCAL_PLAYER_SEE_GB_GB_GUNRUNNING_DEFEND_HUD()
					IF GB_GET_PLAYER_UI_LEVEL(player) >= GB_UI_LEVEL_MINIMAL
						//IF serverBD.iVariation = GB_GUNRUNNING_DEFEND_DEFUSAL
							//sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(player)
							//hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID())
							// ~a~ ~s~failed to kill all the targets.
							HANDLE_END_SHARD_FOR_GUNRUNNING_DEFEND_VARIATION()
							//SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_OVER, "BIGM_BK_DEFOVR", "BIGM_BKSTOLPR", sOrganization, hclPlayer) 
							GB_GUNRUN_SET_PASS_FAIL_CALL_DATA(GRV_INVALID,FALSE,FALSE,TRUE)
							// RESET_GLOBAL_DEFEND_TIMERS(FALSE)
							 GB_SET_LOCAL_BIKER_CALL_BIT(eGB_LOCAL_BIKER_CALL_BITSET_NOTIFICATION_DEFEND_FAIL)
						//ELSE
						//	HANDLE_END_SHARD_FOR_BIKER_DEFEND_VARIATION()
						//ENDIF
					ENDIF
					
				ENDIF
				
				IF player = PLAYER_ID()
					IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneEndTelemetry)
						GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GUNRUNNING_DEFEND, FALSE, gbRewards)
						GB_GUNRUNNING_DEFEND_SET_DEFEND_TELEMETRY_DATA()
						GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST, INT_TO_ENUM(FACTORY_ID, serverBD.iBusinessToDefend))
						SET_BIT(iBoolsBitSet, biL_DoneEndTelemetry)
					ENDIF
				ENDIF
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] FAILED AS biS_TimeoutExpired")
				

				
				SET_BIT(iBoolsBitSet, biL_WaitForShard) 
//			ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_AllKilledByAi)
//				//-- All players killed by AI
//				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] biS_AllKilledByAi")
//				IF GB_GET_PLAYER_UI_LEVEL(player) >= GB_UI_LEVEL_MINIMAL
//					HANDLE_END_SHARD_FOR_BIKER_DEFEND_VARIATION()
//				ENDIF
//								
//				IF player = PLAYER_ID()
//					IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneEndTelemetry)
//						GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST, serverBD.iBusinessToDefend)
//						SET_BIT(iBoolsBitSet, biL_DoneEndTelemetry)
//					ENDIF
//					GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GUNRUNNING_DEFEND, FALSE, gbRewards)
//					
//				ENDIF
//
//				
//				SET_BIT(iBoolsBitSet, biL_WaitForShard)	
			ELIF IS_SERVER_BIT_SET(biS_BossLaunchedQuit)
				//-- Boss who launched quit
				
				IF SHOULD_LOCAL_PLAYER_SEE_GB_GB_GUNRUNNING_DEFEND_HUD()
					IF DID_MY_BOSS_LAUNCH_GB_GB_GUNRUNNING_DEFEND()
						IF GB_GET_PLAYER_UI_LEVEL(player) >= GB_UI_LEVEL_MINIMAL
						//	SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_SGHTBQ") // Your Boss quit the session
						ENDIF
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] biS_BossLaunchedQuit AND MY BOSS QUIT")
					ELSE
						IF GB_GET_PLAYER_UI_LEVEL(player) >= GB_UI_LEVEL_MINIMAL
							SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_HUNTRBQ") // The rival Boss quit the session
						ENDIF
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] biS_BossLaunchedQuit AND RIVAL BOSS")
					ENDIF
				ELSE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] biS_BossLaunchedQuit BUT NOT SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD")
				ENDIF
				
				IF Player = PLAYER_ID()
					IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneEndTelemetry)
						GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GUNRUNNING_DEFEND, FALSE, gbRewards)
						GB_GUNRUNNING_DEFEND_SET_DEFEND_TELEMETRY_DATA()
						GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_BOSS_LEFT, INT_TO_ENUM(FACTORY_ID, serverBD.iBusinessToDefend))
						SET_BIT(iBoolsBitSet, biL_DoneEndTelemetry)
					ENDIF
				ENDIF
				
				SET_BIT(iBoolsBitSet, biL_WaitForShard)
			//	bEventCompleted = TRUE
			
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(iBoolsBitSet, biL_WaitForShard)
			Clear_Any_Objective_Text_From_This_Script()
			REMOVE_BOMB_BLIPS()
			//-- Boss needs to remove any lost contraband from their business. 
			//-- Amount to remove will depend on the variation, and how the player's org perfromed during the mission
			IF PARTICIPANT_ID_TO_INT() = serverBD.iBossPartWhoLaunched
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] I'm boss who launched ")
				IF GET_BUSINESS_STOCK_AMOUNT_TO_LOSE() > -1.0
					IF NOT IS_SERVER_BIT_SET(biS_ContrabandRemovedAtEnd)			
					AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ContrabandRemovedAtEnd)
						IF IS_SERVER_BIT_SET(biS_TimeoutExpired)
						OR IS_SERVER_BIT_SET(biS_DurationExpired)
						OR IS_SERVER_BIT_SET(biS_ContrabandWrecked)
						OR IS_SERVER_BIT_SET(biS_ContrabandLost)
						OR HAS_NET_TIMER_STARTED(serverBD.timeDuration)
						OR IS_SERVER_BIT_SET(biS_BossLaunchedQuit)
						OR IS_SERVER_BIT_SET(biS_AllPackagesDelivered)
//						OR (serverBD.iVariation = GB_GUNRUNNING_DEFEND_FAKE_ID
//						AND IS_BIT_SET(serverBD.iServerBitSet, biS_ContrabandLost))
							HANDLE_LOST_CONTRABAND()
						ELSE
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] No contraband to be removed, setting biP_ContrabandRemovedAtEnd")
							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ContrabandRemovedAtEnd)
							

						ENDIF
						
						IF eResult = CONTRABAND_TRANSACTION_STATE_SUCCESS
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] Transaction successful, setting biP_ContrabandRemovedAtEnd")
							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ContrabandRemovedAtEnd)
						ENDIF
						
						IF eResult = CONTRABAND_TRANSACTION_STATE_FAILED
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] Transaction failed, setting biP_ContrabandRemovedAtEnd")
							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ContrabandRemovedAtEnd)
						ENDIF
						
						//-- For now, until we know what we're going to lose in biker defend missions
						//SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ContrabandRemovedAtEnd)
					ENDIF
				ELSE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] WAiting for GET_BUSINESS_STOCK_AMOUNT_TO_LOSE")
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(iMusicBitset, biMusic_ShouldStop30s)
				
				CLEANUP_ASSETS()
				MAINTAIN_BOMB_CLEANUP()
				CLEANUP_GB_PACKAGE_CLIENT(localPackages, serverBD.packagesServer, HAS_MISSION_END_CONDITION_BEEN_MET())
				
				RESET_ALL_PLAYER_VEH_BLIPS()
				
				SET_BIT(iMusicBitset, biMusic_ShouldStop30s)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] SET biL_ShouldStop30s ")
			ENDIF
			
			//-- If a a remote player has been given a custom blip (becuase the yheld a package, for example), reset here
			IF NOT IS_BIT_SET(iBoolsBitSet, biL_ResetBlipForShard)
				IF serverBD.iBossPlayerWhoLaunched != -1
					IF IS_BIT_SET(iBoolsBitSet, biL_BlippedRival)
						IF NETWORK_IS_PLAYER_ACTIVE(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
							
							SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), RADAR_TRACE_TEMP_4, FALSE)
							SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), BLIP_COLOUR_RED, FALSE)
							FORCE_BLIP_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), FALSE, FALSE)
							SET_PLAYER_BLIP_AS_LONG_RANGE(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), FALSE) 
							SET_FIXED_BLIP_SCALE_FOR_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), g_sMPTunables.fgangboss_Job_blip_scale, FALSE)
							SET_BIT (iBoolsBitSet, biL_ResetBlipForShard)
							
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  [CHECK_FOR_REWARD] - biL_BlippedRival WAS SET, RESET BLIP")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//-- Wait for end shard to finish displaying before cleanuing up mission
			IF GB_MAINTAIN_BOSS_END_UI(gbBossEndUi, FALSE)
			AND (IS_SERVER_BIT_SET(biS_ContrabandRemovedAtEnd)
			OR IS_SERVER_BIT_SET(biS_BossLaunchedQuit))
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_REWARD] SET biP_Finished ")
				IF iPart = PARTICIPANT_ID_TO_INT()
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Finished)
				ENDIF
			
			
			ENDIF
		ENDIF
	ENDIF
ENDPROC







/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MISSION PROCS                  //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////






/// PURPOSE:
///    Handle the mission duration (server side)
PROC MAINTAIN_MISSION_DURATION_SERVER()

	
	IF NOT HAS_NET_TIMER_STARTED(serverBD.timeStartToLoseContra)
		START_NET_TIMER(serverBD.timeStartToLoseContra)
		PRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] [MAINTAIN_MISSION_DURATION_SERVER] STARTED timeStartToLoseContra")
		EXIT
	ELSE
		IF NOT HAS_NET_TIMER_EXPIRED(serverBD.timeStartToLoseContra, GET_GB_GUNRUNNING_DEFEND_TIME_BEFORE_START_TO_LOSE_CONTRABAND())
			IF IS_SERVER_BIT_SET(biS_AllInitalTargetsKilled)
				IF serverBD.iMeterValueWhenInitialAllKilled = -1
					serverBD.iMeterValueWhenInitialAllKilled = GET_GB_GB_GUNRUNNING_DEFEND_DURATION()
				ENDIF
			ENDIF
		//	PRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] [MAINTAIN_MISSION_DURATION_SERVER]- Timer Expired")
			
			EXIT
		ENDIF
	ENDIF
	
//	INT iTimeToUse
	SWITCH serverBD.iVariation
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
//			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_UseReducedDurationTime)
//				iTimeToUse = GET_GB_GB_GUNRUNNING_DEFEND_DURATION()
//			ELSE
//				IF serverBD.iNewServerDuration >= 0
//					iTimeToUse = serverBD.iNewServerDuration
//				ENDIF
//			ENDIF
			
			IF NOT IS_SERVER_BIT_SET(biS_AllInitalTargetsKilled)
				IF NOT HAS_NET_TIMER_STARTED(serverBD.timeDuration)
					START_NET_TIMER(serverBD.timeDuration)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MISSION_DURATION_SERVER] [MAINTAIN_MISSION_DURATION_SERVER] STARTED timeDuration")
				ELSE
					IF HAS_NET_TIMER_EXPIRED(serverBD.timeDuration, GET_GB_GB_GUNRUNNING_DEFEND_DURATION())
						SET_SERVER_BIT(biS_DurationExpired)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_MISSION_DURATION_SERVER] [MAINTAIN_MISSION_DURATION_SERVER] SET biS_DurationExpired")
					ENDIF
				ENDIF
			
			ENDIF
		BREAK
		
	
	ENDSWITCH
	
				
ENDPROC
PROC MAINTAIN_BOMB_BLOWUP()
	IF IS_SERVER_BIT_SET(biS_BombTimerExpired)
		IF !IS_BIT_SET(iBoolsBitSet2,biL2_ExplodedTruck)
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niPlayerVeh[0])
			AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.niPlayerVeh[0]))
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niPlayerVeh[0])
				OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.niPlayerVeh[0]) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
					IF TAKE_CONTROL_OF_NET_ID(serverBD.niPlayerVeh[0]) 
						NETWORK_EXPLODE_VEHICLE(NET_TO_VEH(serverbd.niPlayerVeh[0]))
						SET_BIT(iBoolsBitSet2,biL2_ExplodedTruck)
						PRINTLN("JS- MAINTAIN_BOMB_BLOWUP - NETWORK_EXPLODE_VEHICLE")
					ENDIF
				ENDIF
			ELSE
				SET_BIT(iBoolsBitSet2,biL2_ExplodedTruck)
				PRINTLN("JS- NOT NETWORK_DOES_NETWORK_ID_EXIST")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_CONTRABAND_DOOR_LOCK()
 IF !IS_BIT_SET(iBoolsBitSet2, biL2_VehicleUnlocked)
	IF IS_SERVER_BIT_SET(biS_AllBombsDefused)
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niPlayerVeh[0])
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niPlayerVeh[0])
			OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.niPlayerVeh[0]) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
				IF TAKE_CONTROL_OF_NET_ID(serverBD.niPlayerVeh[0]) 
					IF  GET_VEHICLE_DOOR_LOCK_STATUS(NET_TO_VEH(serverBD.niPlayerVeh[0])) != VEHICLELOCK_UNLOCKED
						SET_VEHICLE_DOORS_LOCKED(NET_TO_VEH(serverBD.niPlayerVeh[0]), VEHICLELOCK_UNLOCKED)
						SET_BIT(iBoolsBitSet2, biL2_VehicleUnlocked)
					ENDIF
					PRINTLN("JS- UNLOCK THE LORRY")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
 ENDIF
	
ENDPROC

PROC MAINTAIN_MISSION_TIMEOUT_SERVER()
	IF NOT HAS_NET_TIMER_STARTED(serverBD.timeTimeout)
		START_NET_TIMER(serverBD.timeTimeout)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  [MAINTAIN_MISSION_TIMEOUT_SERVER] STARTED timtimeTimeouteDuration")
	ELSE
		IF HAS_NET_TIMER_EXPIRED(serverBD.timeTimeout, GET_GB_GUNRUNNING_DEFEND_TIMEOUT())
			
					
			SET_SERVER_BIT(biS_TimeoutExpired)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  [MAINTAIN_MISSION_TIMEOUT_SERVER] SET biS_TimeoutExpired")
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_CONTRABAND_VEHICLE_SET_OFF()

ENDPROC
PROC MAINTAIN_GUNRUNNING_DEFEND_AUDIO_MUSIC_TRIGGERS()
	SWITCH serverbd.iVariation
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL

			IF !HAS_MISSION_END_CONDITION_BEEN_MET()
				IF !IS_BIT_SET(iBoolsBitSet2,biL2_DidMusicCue0)
					IF IS_BIT_SET(iBoolsBitSet2, biL2_IntroCallFinished)
						//GR_DELIVERING_START	Trigger event when player is told to Go to LOCATION
						TRIGGER_MUSIC_EVENT("GR_DELIVERING_START")
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GUNRUNNING_DEFEND_AUDIO_MUSIC_TRIGGERS] Music Triggered 0 =  told to Go to LOCATION")
						SET_BIT(iBoolsBitSet2,biL2_DidMusicCue0)
					ENDIF
				ELSE 
					IF !IS_BIT_SET(iBoolsBitSet2,biL2_DidMusicCue1)
						IF IS_SERVER_BIT_SET(biS_SomeoneNearActionArea)
							//GR_RSUP_STEAL_RHINO_DELIVER	Trigger event when player is told to defuse the bombs
							TRIGGER_MUSIC_EVENT("GR_RSUP_STEAL_RHINO_DELIVER")
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GUNRUNNING_DEFEND_AUDIO_MUSIC_TRIGGERS] Music Triggered 1 = told to defuse the bombs")
							SET_BIT(iBoolsBitSet2,biL2_DidMusicCue1)
						ENDIF
					ELSE
						IF !IS_BIT_SET(iBoolsBitSet2,biL2_DidMusicCue2)
							IF IS_SERVER_BIT_SET(biS_EnemiesAlerted)
								//GR_RSUP_STEAL_RAILGUNS_FIGHT	Trigger event when player enters area near truck and enemies attack
								TRIGGER_MUSIC_EVENT("GR_RSUP_STEAL_RAILGUNS_FIGHT")
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GUNRUNNING_DEFEND_AUDIO_MUSIC_TRIGGERS] Music Triggered 2 =  player enters area near truck and enemies attack")
								SET_BIT(iBoolsBitSet2,biL2_DidMusicCue2)		
							ENDIF
						ELSE
							IF !IS_BIT_SET(iBoolsBitSet2,biL2_DidMusicCue3)
								IF IS_SERVER_BIT_SET(biS_AllBombsDefused)
									//GR_RSUP_STEAL_RAILGUNS_DELIVER	Trigger event when player is told to retrieve the pounder containing the weapons
									TRIGGER_MUSIC_EVENT("GR_RSUP_STEAL_RAILGUNS_DELIVER")
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GUNRUNNING_DEFEND_AUDIO_MUSIC_TRIGGERS] Music Triggered 3 = told to retrieve the pounder containing the weapons")
									SET_BIT(iBoolsBitSet2,biL2_DidMusicCue3)
								ENDIF
							ELSE
								IF !IS_BIT_SET(iBoolsBitSet2,biL2_DidMusicCue4)
									IF IS_BIT_SET(iBoolsBitSet,biL_GangChaseStarted)
										//GR_RSUP_STEAL_RAILGUNS_DELIVER_2	Trigger event when player is told to return the weapons to the bunker
										TRIGGER_MUSIC_EVENT("GR_RSUP_STEAL_RAILGUNS_DELIVER_2")
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GUNRUNNING_DEFEND_AUDIO_MUSIC_TRIGGERS] Music Triggered 4 = told to return the weapons to the bunker")
										SET_BIT(iBoolsBitSet2,biL2_DidMusicCue4)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			
		BREAK
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
		
			IF !HAS_MISSION_END_CONDITION_BEEN_MET()
				IF !IS_BIT_SET(iBoolsBitSet2,biL2_DidMusicCue0)
					IF IS_BIT_SET(iBoolsBitSet2, biL2_IntroCallFinished)
						//GR_DELIVERING_START	Trigger when told to go to location
						TRIGGER_MUSIC_EVENT("GR_DELIVERING_START")
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GUNRUNNING_DEFEND_AUDIO_MUSIC_TRIGGERS] Music Triggered 0 = told to go to location")
						SET_BIT(iBoolsBitSet2,biL2_DidMusicCue0)
					ENDIF
				ELSE
					IF !IS_BIT_SET(iBoolsBitSet2,biL2_DidMusicCue1)
						IF IS_SERVER_BIT_SET(biS_EnemiesAlerted)
							//GR_RSUP_AMBUSHED_FIGHT	Trigger when told to destroy the Valkyrie and recover the weapons
							TRIGGER_MUSIC_EVENT("GR_RSUP_AMBUSHED_FIGHT")
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GUNRUNNING_DEFEND_AUDIO_MUSIC_TRIGGERS] Music Triggered 1 = told to destroy the Valkyrie and recover the weapons")
							SET_BIT(iBoolsBitSet2,biL2_DidMusicCue1)
						ENDIF
					ELSE
						IF !IS_BIT_SET(iBoolsBitSet2,biL2_DidMusicCue2)
							IF IS_BIT_SET(iBoolsBitSet2,biL2_HeloShotDown)
								//GR_RSUP_MINIGUNS_FLATBEDS	Trigger when told to recover the stolen weapons
								TRIGGER_MUSIC_EVENT("GR_RSUP_MINIGUNS_FLATBEDS")
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GUNRUNNING_DEFEND_AUDIO_MUSIC_TRIGGERS] Music Triggered 2 = told to recover the stolen weapons")
								SET_BIT(iBoolsBitSet2,biL2_DidMusicCue2)
							ENDIF
						ELSE
							IF !IS_BIT_SET(iBoolsBitSet2,biL2_DidMusicCue3)
								IF IS_BIT_SET(iBoolsBitSet,biL_GangChaseStarted)
									//GR_RSUP_MINIGUNS_DELIVER	Trigger event when told to return the weapons to the bunker
									TRIGGER_MUSIC_EVENT("GR_RSUP_MINIGUNS_DELIVER")
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GUNRUNNING_DEFEND_AUDIO_MUSIC_TRIGGERS] Music Triggered 3 = told to return the weapons to the bunker")
									SET_BIT(iBoolsBitSet2,biL2_DidMusicCue3)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	//For both missions
	IF !IS_BIT_SET(iBoolsBitSet2,biL2_DidMusicCue5)
				IF IS_SERVER_BIT_SET(biS_AllPackagesDelivered)
				OR IS_SERVER_BIT_SET(biS_VehDelivered)
					//GR_MP_MUSIC_STOP	Trigger event on successful drop off/completion to stop the music
					TRIGGER_MUSIC_EVENT("GR_MP_MUSIC_STOP")
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GUNRUNNING_DEFEND_AUDIO_MUSIC_TRIGGERS] Music Triggered 5 = on successful drop off")
					SET_BIT(iBoolsBitSet2,biL2_DidMusicCue5)
				ENDIF
	ENDIF

	IF !IS_BIT_SET(iBoolsBitSet2,biL2_DidMusicCue6)	
		IF IS_SERVER_BIT_SET(biS_TimeoutExpired)
		OR IS_SERVER_BIT_SET(biS_DurationExpired)
		OR IS_SERVER_BIT_SET(biS_BossLaunchedQuit)
		OR IS_SERVER_BIT_SET(biS_ContrabandWrecked)
		OR IS_SERVER_BIT_SET(biS_ContrabandLost)
			//GR_MP_MUSIC_FAIL	Please trigger if the player leaves the session, fails the mission or ungracefully exits the script.
			TRIGGER_MUSIC_EVENT("GR_MP_MUSIC_FAIL")
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GUNRUNNING_DEFEND_AUDIO_MUSIC_TRIGGERS] Music Triggered 6 = player leaves the session, fails the mission or ungracefully exits the script")
			SET_BIT(iBoolsBitSet2,biL2_DidMusicCue6)
		ENDIF
	ENDIF
ENDPROC

/// Some helper functions for accessing who is disabling / has disabled each bomb

FUNC BOOL IS_ANYONE_DEFUSING_BOMB(INT iBomb)
	RETURN (serverBD.iPlayerCanDefuseBomb[iBomb] > -1)
ENDFUNC

FUNC BOOL HAS_BOMB_BEEN_DEFUSED(INT iBomb)
	RETURN (serverBD.iBombDefusedByPlayer[iBomb] > -1)
ENDFUNC

FUNC PLAYER_INDEX GET_PLAYER_WHO_DEFUSED_BOMB(INT iBomb)
	PLAYER_INDEX playerDefuse = INVALID_PLAYER_INDEX()
	IF HAS_BOMB_BEEN_DEFUSED(iBomb)
		playerDefuse = INT_TO_PLAYERINDEX(serverBD.iBombDefusedByPlayer[iBomb])
	ENDIF
	
	RETURN playerDefuse
ENDFUNC

FUNC PLAYER_INDEX GET_PLAYER_DEFUSING_BOMB(INT iBomb)
	PLAYER_INDEX playerDefuse = INVALID_PLAYER_INDEX()
	IF IS_ANYONE_DEFUSING_BOMB(iBomb)
		playerDefuse = INT_TO_PLAYERINDEX(serverBD.iPlayerCanDefuseBomb[iBomb])
	ENDIF
	
	RETURN playerDefuse
ENDFUNC

FUNC INT GET_NUMBER_OF_DEFUSED_BOMBS()
	INT iDefused
	INT i
	REPEAT MAX_BOMBS i
		IF HAS_BOMB_BEEN_DEFUSED(i)
			iDefused++
		ENDIF
	ENDREPEAT
	
	RETURN iDefused
			
ENDFUNC

PROC MAINTAIN_MANAGE_GB_DEFEND_BOMB_PROPS()
	IF serverbd.iVariation != GB_GUNRUNNING_DEFEND_DEFUSAL
		EXIT
	ENDIF
	CPRINTLN(DEBUG_NET_MAGNATE, "JS-AAA1")
	INT i
	REPEAT MAX_BOMBS i
		
		IF HAS_BOMB_BEEN_DEFUSED(i)
		
		
			INT tempInt = i+ 6
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.niProp[tempInt])
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverbd.niProp[tempInt])
					
					//inumPropsDeleted++
					INT tempInt2 = tempInt + MAX_BOMBS// + inumPropsDeleted
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.niProp[tempInt2])
						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverbd.niProp[tempInt2])
							SET_ENTITY_VISIBLE(NET_TO_ENT(serverbd.niProp[tempInt2]),TRUE)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -MAINTAIN_MANAGE_GB_DEFEND_BOMB_PROPS- PROP MADE VISIBLE: ", tempInt2) NET_NL()
						ELSE
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -MAINTAIN_MANAGE_GB_DEFEND_BOMB_PROPS- NO CONTROL OVER PROP: ", tempInt2) NET_NL()
						ENDIF
					ELSE
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -MAINTAIN_MANAGE_GB_DEFEND_BOMB_PROPS- DOES NOT EXIST, PROP: ", tempInt2) NET_NL()
					ENDIF
					
					DELETE_NET_ID(serverbd.niProp[tempInt])
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -MAINTAIN_MANAGE_GB_DEFEND_BOMB_PROPS- PROP DELETED: ", tempInt) NET_NL()
				ENDIF
			ENDIF
		
			
		ELIF IS_SERVER_BIT_SET(biS_BombTimerExpired)
			INT tempInt = i+ 6
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.niProp[tempInt])
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverbd.niProp[tempInt])
					
					//inumPropsDeleted++
					INT tempInt2 = tempInt + MAX_BOMBS// + inumPropsDeleted
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.niProp[tempInt2])
						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverbd.niProp[tempInt2])
							DELETE_NET_ID(serverbd.niProp[tempInt2])
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -MAINTAIN_MANAGE_GB_DEFEND_BOMB_PROPS- DELETED PROP because biS_BombTimerExpired : ", tempInt2) NET_NL()
						ELSE
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -MAINTAIN_MANAGE_GB_DEFEND_BOMB_PROPS- NO CONTROL OVER PROP: ", tempInt2) NET_NL()
						ENDIF
					ELSE
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -MAINTAIN_MANAGE_GB_DEFEND_BOMB_PROPS- DOES NOT EXIST, PROP: ", tempInt2) NET_NL()
					ENDIF
					
					DELETE_NET_ID(serverbd.niProp[tempInt])
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -MAINTAIN_MANAGE_GB_DEFEND_BOMB_PROPS- DELETED PROP because biS_BombTimerExpired: ", tempInt) NET_NL()
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL IS_ANY_BOMB_NOT_BEING_DEFUSED()
	INT i
	REPEAT GET_GUNRUNNING_DEFEND_NUMBER_OF_BOMBS_FOR_BUSINESS() i
		IF NOT HAS_BOMB_BEEN_DEFUSED(i)
			IF NOT IS_ANYONE_DEFUSING_BOMB(i)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC PLAYER_INDEX GET_ANY_PLAYER_DEFUSING_A_BOMB()
	
	INT i
	REPEAT GET_GUNRUNNING_DEFEND_NUMBER_OF_BOMBS_FOR_BUSINESS() i
		IF NOT HAS_BOMB_BEEN_DEFUSED(i)
			IF IS_ANYONE_DEFUSING_BOMB(i)
				RETURN GET_PLAYER_DEFUSING_BOMB(i)
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN INVALID_PLAYER_INDEX()
ENDFUNC

FUNC BOOL IS_PLAYER_DEFUSING_A_BOMB(PLAYER_INDEX playerDefuse)
	INT i
	REPEAT GET_GUNRUNNING_DEFEND_NUMBER_OF_BOMBS_FOR_BUSINESS() i
		IF NOT HAS_BOMB_BEEN_DEFUSED(i)
			IF IS_ANYONE_DEFUSING_BOMB(i)
				IF GET_PLAYER_DEFUSING_BOMB(i) = playerDefuse
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC


PROC SETUP_BLOCKING_ZONES()

//	VECTOR vDropOff = GET_DROP_OFF_LOCATION()
//	IF NOT IS_VECTOR_ZERO(vDropOff)
//		ADD_TRAFFIC_REDUCTION_ZONE(serverBD.iTrafficReductionIndex,vDropOff)
//	ENDIF

ENDPROC

PROC DRAW_KILLS_BAR()
	
//	DRAW_GENERIC_BIG_DOUBLE_NUMBER((serverBD.iKillGoal-serverBD.iTotalKills), serverBD.iKillGoal, "GHO_KILLB")
ENDPROC

/// PURPOSE:
///    Draw the hud elemnts tracking whether or not each package has been delivered or not. Only needed if there's > 1 package
PROC MAINTAIN_GB_GUNRUNNING_DEFEND_PACKAGE_HUD()

	
	INT iNumberOfPackages = serverBD.iNumberOfPackages
	INT i
	INT iPlayerDel
	
	IF iNumberOfPackages <= 1
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_GB_GUNRUNNING_DEFEND_PACKAGE_HUD]  -NumPackages <= 1")
		EXIT
	ENDIF
	
	HUD_COLOURS eBoxColour[MAX_GANG_BOSS_GB_PACKAGES]
	HUD_COLOURS eCrossColour[MAX_GANG_BOSS_GB_PACKAGES]
	
	
	REPEAT iNumberOfPackages i
		IF HAS_GB_PACKAGE_BEEN_DELIVERED(serverBD.packagesServer, i, iPlayerDel)
			eBoxColour[i] = HUD_COLOUR_GREEN 
		ELSE
			eBoxColour[i] = HUD_COLOUR_GREYLIGHT 
		ENDIF
	ENDREPEAT
	
	DRAW_GENERIC_ELIMINATION(	iNumberOfPackages, localPackages.tl5HudTitle, DEFAULT, 
								TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, eBoxColour[0], eBoxColour[1], eBoxColour[2], eBoxColour[3],
								DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 
								FALSE, FALSE, FALSE,
								FALSE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, eCrossColour[0], eCrossColour[1], eCrossColour[2], eCrossColour[3])
	
	
ENDPROC

/// PURPOSE:
///    Maintin the on-screen HUD for each mode
PROC MAINTAIN_BOTTOM_RIGHT_HUD()
		
	IF HAS_MISSION_END_CONDITION_BEEN_MET()
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_BOTTOM_RIGHT_HUD]  -MISSION END MET")
		EXIT
	ENDIF
	

	
	IF NOT DID_MY_BOSS_LAUNCH_GB_GB_GUNRUNNING_DEFEND()
		EXIT
	ENDIF
	
	
	
	IF NOT SHOULD_LOCAL_PLAYER_SEE_GB_GB_GUNRUNNING_DEFEND_HUD()
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(iBoolsBitSet2, biL2_IntroCallFinished)
	//	EXIT
	ENDIF
	
	PLAYER_INDEX player = PLAYER_ID()
//	INT iPart = PARTICIPANT_ID_TO_INT()
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
	//	iPart = GET_MY_SPEC_TARGET_AS_PARTICIPANT_INT()
		player = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget
	ENDIF
	
	GB_UI_LEVEL myUiLevel = GB_GET_PLAYER_UI_LEVEL(player)
	
	IF myUiLevel < GB_UI_LEVEL_FULL	
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_BOTTOM_RIGHT_HUD] EXIT AS myUiLevel = ", ENUM_TO_INT(myUiLevel))
		EXIT
	ENDIF
	
	
	
//	INT iEnemiesRemaining = GET_GB_GUNRUNNING_DEFEND_NUMBER_OF_INITIAL_PEDS_FOR_VARIATION(serverBD.iVariation, serverbd.iBusinessToDefend) - serverBD.iNumTargetsKilled
	
	
	HUD_COLOURS timeColour
	
	
//	INT iMeterMaxValue = GET_GB_GB_GUNRUNNING_DEFEND_DURATION()
//	INT iMeterCurrentValue
	INT iTimeRemaining
//	INT iTimeDiff

	//INT iTotalBombs 
	//INT iBombsDefused
	SWITCH ServerBD.iVariation
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
			IF IS_SERVER_BIT_SET(biS_SomeoneNearActionArea)
				IF IS_SERVER_BIT_SET(biS_EnemiesAlerted)
					IF !IS_SERVER_BIT_SET(biS_AllBombsDefused)
					//Bomb timer
						IF !HAS_NET_TIMER_EXPIRED(bombTimer,g_sMPTunables.IGR_DEFEND_DISARM_BOMBS_BOMB_DISARM_TIME)
								IF !IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_BombTimerStarted)
									SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_BombTimerStarted)
								ENDIF
								iTimeRemaining = (g_sMPTunables.IGR_DEFEND_DISARM_BOMBS_BOMB_DISARM_TIME - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(bombTimer))
								IF iTimeRemaining > g_sMPTunables.IGR_DEFEND_DISARM_BOMBS_BOMB_DISARM_TIME/10
									timeColour = HUD_COLOUR_WHITE
								ELSE
									timeColour = HUD_COLOUR_RED
								ENDIF
								
								SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
							
								IF iTimeRemaining > 0
									DRAW_GENERIC_TIMER(iTimeRemaining, "DGUN_HUD3", DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, timeColour)
								ELSE
									
									DRAW_GENERIC_TIMER(0, "SCONTRA_TIMER", DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, timeColour)
								ENDIF						
						ELSE
							//Timer expired. Blow up the lorry.
							IF !IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_BombTimerExpired)
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_BombTimerExpired)
							ENDIF
						
						
						ENDIF
						
						//draw bomb count
						DRAW_GENERIC_BIG_DOUBLE_NUMBER(serverBD.iGlobalBombCount,MAX_BOMBS,"DGUN_BOMBCOUNT")
						
						
					ENDIF
				ENDIF
			ENDIF
			//Mission timer.
			IF HAS_NET_TIMER_STARTED(serverBD.timeTimeout)
					iTimeRemaining = (GET_GB_GUNRUNNING_DEFEND_TIMEOUT() - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeTimeout))
					
					IF iTimeRemaining <= GET_GB_GUNRUNNING_DISPLAY_TIMER_TIME()
						IF iTimeRemaining > 30000
							timeColour = HUD_COLOUR_WHITE
						ELSE
							timeColour = HUD_COLOUR_RED
						ENDIF
						
						SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
					
						IF iTimeRemaining > 0
							DRAW_GENERIC_TIMER(iTimeRemaining, "SCONTRA_TIMER", DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, timeColour)
						ELSE
							DRAW_GENERIC_TIMER(0, "SCONTRA_TIMER", DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, timeColour)
						ENDIF
					ENDIF
			ENDIF
		BREAK
		
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			//Mission timer.
			IF HAS_NET_TIMER_STARTED(serverBD.timeTimeout)
				iTimeRemaining = (GET_GB_GUNRUNNING_DEFEND_TIMEOUT() - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeTimeout))
				
				IF iTimeRemaining <= GET_GB_GUNRUNNING_DISPLAY_TIMER_TIME()
					IF iTimeRemaining > 30000
						timeColour = HUD_COLOUR_WHITE
					ELSE
						timeColour = HUD_COLOUR_RED
					ENDIF
					
					SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
				
					IF iTimeRemaining > 0
						DRAW_GENERIC_TIMER(iTimeRemaining, "SCONTRA_TIMER", DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, timeColour)
					ELSE
						DRAW_GENERIC_TIMER(0, "SCONTRA_TIMER", DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, timeColour)
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC


PROC ADD_BLIP_FOR_PACKAGE()
ENDPROC


PROC ADD_BLIP_FOR_DROP_OFF()//BOOL bSetGps = TRUE)
//	IF NOT DOES_BLIP_EXIST(blipDropOff)
//		VECTOR vDropOff = GET_DROP_OFF_LOCATION()
//		blipDropOff = ADD_BLIP_FOR_COORD(vDropOff)
//		IF bSetGps
//			SET_BLIP_ROUTE(blipDropOff, TRUE)
//		ENDIF
//		
//		SET_BLIP_NAME_FROM_TEXT_FILE(blipDropOff, "BUNK_QUICK_GPS")
//		
//		#IF IS_DEBUG_BUILD
//		vWarpCoordsForJskip = vDropOff
//		#ENDIF
//		
//		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [ADD_BLIP_FOR_DROP_OFF] ADD DROP OFF BLIP AT COORDS ", vDropOff) 
//	ENDIF
ENDPROC

PROC ADD_BLIP_FOR_CONTRABAND_VEHICLE(INT iVeh, INT iColour, BOOL bSetGps = TRUE)
	TEXT_LABEL_15 tl15Name
	IF NOT DOES_BLIP_EXIST(blipVeh[iVeh])
		IF  NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niPlayerVeh[iVeh])
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPlayerVeh[iVeh])
				IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niPlayerVeh[iVeh])
					blipVeh[iVeh] = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(serverBD.niPlayerVeh[iVeh]))
					
					SET_BLIP_PRIORITY(blipVeh[iVeh], BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
				//	SET_BLIP_SPRITE(blipveh, RADAR_TRACE_CONTRABAND)
					SET_BLIP_SPRITE(blipVeh[iVeh], GET_GUNRUN_ENTITY_BLIP_SPRITE(FALSE))
					SET_BLIP_COLOUR(blipVeh[iVeh], iColour)
					IF bSetGps
						SET_BLIP_ROUTE(blipVeh[iVeh], TRUE)
					ENDIF
					
					IF IS_SERVER_PLAYER_VEH_BIT_SET(iVeh, PLAYER_VEH_BIT_FLASH_BLIP)
						IF NOT IS_BIT_SET(iBoolsBitSet2, biL2_FlashedVehBlip)
							SET_BLIP_FLASHES(blipVeh[iVeh], TRUE)
							SET_BLIP_FLASH_TIMER(blipVeh[iVeh], 7000)
							SET_BIT(iBoolsBitSet2, biL2_FlashedVehBlip)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND ADD_BLIP_FOR_CONTRABAND_VEHICLE Flashing blip for veh ", iVeh)
						ENDIF
					ENDIF
					
					tl15Name = GET_GB_GUNRUNNING_DEFEND_VEHICLE_BLIP_NAME(serverBD.iVariation, serverBD.iBusinessToDefend)
					IF NOT IS_STRING_NULL_OR_EMPTY(tl15Name)
						SET_BLIP_NAME_FROM_TEXT_FILE(blipVeh[iVeh], tl15Name)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND ADD_BLIP_FOR_CONTRABAND_VEHICLE ADDED BLIP Name = ", tl15Name, " for veh ", iVeh)
					ELSE
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND ADD_BLIP_FOR_CONTRABAND_VEHICLE ADDED BLIP for veh ", iVeh)
					ENDIF
					#IF IS_DEBUG_BUILD
					DEBUG_PRINTCALLSTACK()
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    The player's max wanted level (for example) depends upon how close the pllayer is to the mission 'action area'
PROC MAINTAIN_DISTANCE_CHECKS_FOR_VARIATION()
	IF NOT DID_MY_BOSS_LAUNCH_GB_GB_GUNRUNNING_DEFEND()

		EXIT
	ENDIF
	IF !IS_BIT_SET(iBoolsBitSet2,biL2_wantedLevelSet)
		SET_MAX_WANTED_LEVEL(0)
		SET_BIT(iBoolsBitSet2,biL2_wantedLevelSet)
	ENDIF
	
	
	IF IS_BIT_SET(iBoolsBitSet,biL_ShouldResetWanted)
		//set to whatever the difficulty is.
		SET_MAX_WANTED_LEVEL(3)
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(),3)
		CLEAR_BIT(iBoolsBitSet,biL_ShouldResetWanted)
	ENDIF
	
	
//	VECTOR vAction
////	FLOAT fDist2
////	INT iClosest
//	INT iUncollected
//	FLOAT fDistance = 500.0
//	FLOAT fCleanupDist = 750.0
////	BOOL bGiveInitialWanted
//	
//	PLAYER_INDEX playerWithPackage
//	IF NOT HAS_MISSION_END_CONDITION_BEEN_MET()
//		IF serverBD.iBossPlayerWhoLaunched > -1
//			IF GET_MAX_WANTED_LEVEL() > GET_MAX_WANTED_LEVEL_FROM_DIFFICULTY()
//				SET_MAX_WANTED_LEVEL(GET_MAX_WANTED_LEVEL_FROM_DIFFICULTY())
//				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_DISTANCE_CHECKS_FOR_VARIATION] SET_MAX_WANTED_LEVEL - ", GET_MAX_WANTED_LEVEL_FROM_DIFFICULTY())
//			ENDIF
//		ENDIF
//		
//	ENDIF
//	
//	SWITCH serverBD.iVariation
//		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
//			//-- 'Action' area is the business to defend
//			vAction = GET_DROP_OFF_LOCATION()
//		BREAK
//		
//		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
//			//-- 'Action area is either....
//			IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niPlayerVeh[0])
//				//-- ...the location of the not destroyed Valkyrie, or
//				vAction = GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niPlayerVeh[0]))
//			ELSE
//				IF IS_ANY_GB_PACKAGE_UNCOLLECTED(serverBD.packagesServer, iUncollected)
//					//-- ... the location of the dropped package, or
//					vACtion = GET_ENTITY_COORDS(GET_GB_PACKAGE_OBJ(serverBD.packagesServer, iUncollected))
//				ELSE
//					//-- ... the location of the player holding the package
//					playerWithPackage = GET_GB_PACKAGE_HOLDER_FROM_SERVER_AS_PLAYER(serverBD.packagesServer, 0)
//					IF playerWithPackage != INVALID_PLAYER_INDEX()
//						vACtion = GET_ENTITY_COORDS(GET_PLAYER_PED(playerWithPackage), FALSE)
//					ENDIF
//				ENDIF
//			ENDIF
//		BREAK
//		
//	ENDSWITCH
//	
//
////	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_DISTANCE_CHECKS_FOR_VARIATION] vAction = ",vAction, " fDist2 = ", fDist2)
//	
//	IF NOT ARE_VECTORS_EQUAL(vAction, <<0.0, 0.0, 0.0>>)
//		GB_MAINTAIN_BOSS_WORK_DISTANT_CHECKS(FMMC_TYPE_GUNRUNNING_DEFEND, vAction, bDoneDistanceCheck, fDistance, fCleanupDist)
//	ENDIF
ENDPROC



/// PURPOSE:
///    Initial phone call text label for the variation
/// RETURNS:
///    
FUNC STRING GET_OPENING_DEFEND_MISSION_CALL()
	BOOL bFemale = !IS_OFFICE_PA_MALE(TRUE)
	INT iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
	
	 
	//Defend Mission - Warehouse Under Attack Opening Call

	IF bFemale
		SWITCH iRand
			CASE 0 RETURN "EXCAL_ATTAF"
			CASE 1 RETURN "EXCAL_ATTAF2"
			DEFAULT RETURN "EXCAL_ATTAF3"
		ENDSWITCH
	ELSE
		SWITCH iRand
			CASE 0 RETURN "EXCAL_ATTAM"
			CASE 1 RETURN "EXCAL_ATTAM2"
			DEFAULT RETURN "EXCAL_ATTAM3"
		ENDSWITCH
	ENDIF
	
	
	
	RETURN ""
ENDFUNC

INT iBossPhoncallProg
STRING sMyPhonecall = ""
structPedsForConversation sSpeech
FUNC BOOL DO_BOSS_INTRO_PHONECALL()
	enumCharacterList charPA = GET_OFFICE_PA_CHAR(TRUE)
	SWITCH iBossPhoncallProg
		CASE 0
			sMyPhonecall = GET_OPENING_DEFEND_MISSION_CALL()
			IF IS_OFFICE_PA_MALE(TRUE)
				ADD_PED_FOR_DIALOGUE(sSpeech, 3, NULL, "EXECPA_MALE")
			ELSE
				ADD_PED_FOR_DIALOGUE(sSpeech, 2, NULL, "EXECPA_FEMALE")
			ENDIF
			iBossPhoncallProg++
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [DO_BOSS_INTRO_PHONECALL] sMyPhonecall = ", sMyPhonecall, " iBossPhoncallProg = ", iBossPhoncallProg) 
		BREAK
		
		CASE 1
			INT phonecallModifiers
			IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE,DEFAULT, FALSE)
				SET_BIT(phonecallModifiers, MP_COMMS_MODIFIER_FORCE_PLAYER_TO_ANSWER)
				IF Request_MP_Comms_Message(sSpeech, charPA, "EXCALAU", sMyPhonecall, phonecallModifiers) 
					
					iBossPhoncallProg++
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [DO_BOSS_INTRO_PHONECALL] iBossPhoncallProg = ", iBossPhoncallProg)
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC
/// PURPOSE:
///    Checks if text messages need to be sent, and sends them accordingly.
PROC MAINTAIN_GB_GUNRUNNING_DEFEND_TEXT_MESSAGES()
	
//	GB_UI_LEVEL myUiLevel
//	TEXT_LABEL_15 tl15Txt	
//	IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneOpeningTxt)
//		myUiLevel = GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())
//		IF myUiLevel >= GB_UI_LEVEL_MINIMAL
//			IF NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GB_START_OF_JOB)	
//				tl15Txt = GET_GB_GUNRUNNING_DEFEND_OPENING_TEXT_MSG(serverBD.iVariation, serverBD.iBusinessToDefend)
//				IF NOT IS_STRING_NULL_OR_EMPTY(tl15Txt)
//					IF Request_MP_Comms_Txtmsg(GET_MY_BUSINESS_PHONE_CONTACT() ,tl15Txt, DEFAULT, DEFAULT)
//						SET_BIT(iBoolsBitSet, biL_DoneOpeningTxt)
//						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_GB_GUNRUNNING_DEFEND_TEXT_MESSAGES] Done opening text - label = ", tl15txt)
//					ENDIF
//				ELSE
//					SET_BIT(iBoolsBitSet, biL_DoneOpeningTxt)
//					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_GB_GUNRUNNING_DEFEND_TEXT_MESSAGES] Not doing opening txt msg as IS_STRING_NULL_OR_EMPTY")
//				
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
	IF IS_SERVER_BIT_SET(biS_EnemyPilotEnteredHeli)
	AND !IS_BIT_SET(iBoolsBitSet2,biL2_HeloTextSent)
		SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(GET_GUNRUNNING_CONTACT(), "GR_HELO", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
		SET_BIT(iBoolsBitSet2, biL2_HeloTextSent)
		PRINTLN("JS-HELOTEXTSENT")
	ENDIF
	
	IF IS_BIT_SET(iBoolsBitSet2, biL2_HeloShotDown)
	AND !IS_BIT_SET(iBoolsBitSet2,biL2_HeloDestroyedTextSent)
		SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(GET_GUNRUNNING_CONTACT(), "GR_HELODESTROYED", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
		SET_BIT(iBoolsBitSet2,biL2_HeloDestroyedTextSent)
		PRINTLN("JS-HELODESTROYEDTEXTSENT")
	ENDIF
	
	
	IF IS_BIT_SET(iBoolsBitSet,biL_GangChaseStarted)
		IF !IS_BIT_SET(iBoolsBitSet2,biL2_GangChaseTextSent)	
			IF serverbd.iVariation = GB_GUNRUNNING_DEFEND_VALKYRIE
				SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(GET_GUNRUNNING_CONTACT(), "GR_GANGCHASEV", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
			ELIF serverbd.iVariation = GB_GUNRUNNING_DEFEND_DEFUSAL
				SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(GET_GUNRUNNING_CONTACT(), "GR_GANGCHASEB", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
			ENDIF
			SET_BIT(iBoolsBitSet2,biL2_GangChaseTextSent)
			PRINTLN("JS-GANGCHASESTARTEDTEXTSENT")
		ENDIF
	ENDIF
	
	SWITCH serverBD.iVariation
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
		//TODO:check a server bit set
		IF IS_SERVER_BIT_SET(biS_WithinBombTextRange)
			IF !IS_BIT_SET(iBoolsBitSet2,biL2_BombInfoTextSent)
			//	SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(GET_GUNRUNNING_CONTACT(), "GR_BOMBINFO", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
			ENDIF
			//SET_BIT(iBoolsBitSet2,biL2_BombInfoTextSent)
			//PRINTLN("JS-BOMBINFOTEXTSENT")
		ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC TEXT_LABEL_15 GET_GB_GUNRUNNING_DEFEND_INTRO_PHONE_CALL                                                                                                                                               ()
	TEXT_LABEL_15 tl15Name
	
	INT iRand
	iRand = GET_RANDOM_INT_IN_RANGE(0,3)
	
	SWITCH serverBD.iVariation
	
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			SWITCH iRand
				CASE 0
					tl15Name = "GNRC_DVEO1"	
				BREAK
				CASE 1
					tl15Name = "GNRC_DVEO2"	
				BREAK
				CASE 2
					tl15Name = "GNRC_DVEO3"	
				BREAK
			ENDSWITCH	
		BREAK
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
			SWITCH iRand
				CASE 0
					tl15Name = "GNRC_DDBO1"	
				BREAK
				CASE 1
					tl15Name = "GNRC_DDBO2"	
				BREAK
				CASE 2
					tl15Name = "GNRC_DDBO3"	
				BREAK
			ENDSWITCH	
		BREAK
		
	ENDSWITCH
	RETURN tl15Name
ENDFUNC

FUNC TEXT_LABEL_15 GET_GB_GUNRUNNING_DEFEND_MID_PHONE_CALL                                                                                                                                               ()
	TEXT_LABEL_15 tl15Name
	
	INT iRand
	iRand = GET_RANDOM_INT_IN_RANGE(0,3)
	
	SWITCH serverBD.iVariation
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
			SWITCH iRand
				CASE 0
					tl15Name = "GNRC_DDBUC"	
				BREAK
				CASE 1
					tl15Name = "GNRC_DDBUC2"	
				BREAK
				CASE 2
					tl15Name = "GNRC_DDBUC3"	
				BREAK
			ENDSWITCH	
		BREAK
		
	ENDSWITCH
	RETURN tl15Name
ENDFUNC


TEXT_LABEL_15 tl15TxtIntro
SCRIPT_TIMER timePhonecall
PROC MAINTAIN_GUNRUNNING_DEFEND_INTRO_PHONE_CALLS()
	GB_UI_LEVEL myUiLevel
	
	INT phonecallModifiers
	
	IF NOT IS_BIT_SET(iBoolsBitSet, biL_AddedPhoneContact)
		ADD_PED_FOR_DIALOGUE(sSpeech, 2, NULL, "AGENT14")
		SET_BIT(iBoolsBitSet, biL_AddedPhoneContact)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_BIKER_DEFEND_INTRO_PHONE_CALLS] Set biL_AddedPhoneContact")
	ENDIF
	//IF NOT IS_BIT_SET(iBoolsBitSet, biL_NearVehicleDefusal)
	//ENDIF
	IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneIntroPhonecall)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			myUiLevel = GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())
			IF myUiLevel >= GB_UI_LEVEL_MINIMAL
				IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
				//IF NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GB_START_OF_JOB)	
					IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE,DEFAULT, FALSE)
						IF IS_STRING_NULL_OR_EMPTY(tl15TxtIntro)
							tl15TxtIntro = GET_GB_GUNRUNNING_DEFEND_INTRO_PHONE_CALL()
						ENDIF
						IF NOT IS_STRING_NULL_OR_EMPTY(tl15TxtIntro)
							SET_BIT(phonecallModifiers, MP_COMMS_MODIFIER_FORCE_PLAYER_TO_ANSWER)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_BIKER_DEFEND_INTRO_PHONE_CALLS] Doing intro call with label ", tl15TxtIntro)
							IF Request_MP_Comms_Message(sSpeech, GET_GUNRUNNING_CONTACT(),  "GNRCAUD", tl15TxtIntro, phonecallModifiers)
																							//----->>>change this for near defusal truck.			
								SET_BIT(iBoolsBitSet, biL_DoneIntroPhonecall)
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_BIKER_DEFEND_INTRO_PHONE_CALLS] Done intro call")
							ELSE
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_BIKER_DEFEND_INTRO_PHONE_CALLS] Request_MP_Comms_Message(sSpeech, GET_MY_BUSINESS_PHONE_CONTACT()")
							ENDIF
							
							IF NOT HAS_NET_TIMER_STARTED(timePhonecall)
								START_NET_TIMER(timePhonecall)
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_BIKER_DEFEND_INTRO_PHONE_CALLS] Start timePhonecall")
							ENDIF
						ELSE
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_BIKER_DEFEND_INTRO_PHONE_CALLS] IF NOT IS_STRING_NULL_OR_EMPTY(tl15Txt) ")
						ENDIF
					ELSE
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_BIKER_DEFEND_INTRO_PHONE_CALLS]  IS_OK_TO_PRINT_FREEMODE_HELP(FALSE,DEFAULT, FALSE)")
					ENDIF
				//ELSE
					//CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_BIKER_DEFEND_INTRO_PHONE_CALLS]  GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG)")
				//ENDIF
				ELIF !IS_BIT_SET(iBoolsBitSet2, biL2_IntroCallFinished)
					SET_BIT(iBoolsBitSet2, biL2_IntroCallFinished)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_BIKER_DEFEND_INTRO_PHONE_CALLS]  IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GB_START_OF_JOB) - SET_BIT(iBoolsBitSet2, biL2_IntroCallFinished)")
				ENDIF
			ELSE
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_BIKER_DEFEND_INTRO_PHONE_CALLS] IF myUiLevel >= GB_UI_LEVEL_MINIMAL ")
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(iBoolsBitSet2, biL2_IntroCallOnGoing)
			IF IS_CELLPHONE_CONVERSATION_PLAYING() 
				SET_BIT(iBoolsBitSet2, biL2_IntroCallOnGoing)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_BIKER_DEFEND_INTRO_PHONE_CALLS] Set biL2_IntroCallOnGoing")
			ELSE
				IF NOT Did_MP_Comms_Complete_Successfully()
				AND NOT Is_MP_Comms_Still_Playing()
					CLEAR_BIT(iBoolsBitSet, biL_DoneIntroPhonecall)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_BIKER_DEFEND_INTRO_PHONE_CALLS]  Clear biL_DoneIntroPhonecall as Did_MP_Comms_Complete_Successfully / Is_MP_Comms_Still_Playing")
				ELSE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_BIKER_DEFEND_INTRO_PHONE_CALLS] Did_MP_Comms_Complete_Successfully / Is_MP_Comms_Still_Playing")
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(iBoolsBitSet2, biL2_IntroCallFinished)
				IF NOT IS_CELLPHONE_CONVERSATION_PLAYING() 
					IF NOT IS_PHONE_ONSCREEN()
						SET_BIT(iBoolsBitSet2, biL2_IntroCallFinished)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_BIKER_DEFEND_INTRO_PHONE_CALLS] Set biL2_IntroCallFinished")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(iBoolsBitSet2, biL2_IntroCallFinished)
		IF HAS_NET_TIMER_STARTED(timePhonecall)
			IF HAS_NET_TIMER_EXPIRED(timePhonecall, 15000)
				SET_BIT(iBoolsBitSet2, biL2_IntroCallFinished)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_BIKER_DEFEND_INTRO_PHONE_CALLS] timePhonecall expired - set biL2_IntroCallFinished")
			ENDIF
		ENDIF
	ENDIF
ENDPROC
TEXT_LABEL_15 tl15TxtMid
SCRIPT_TIMER timeMidBombPhonecall
PROC MAINTAIN_GUNRUNNING_DEFEND_MIDDLE_PHONE_CALLS()

	 IF serverbd.iVariation != GB_GUNRUNNING_DEFEND_DEFUSAL
	 	EXIT
	 ENDIF
	GB_UI_LEVEL myUiLevel
	
	INT phonecallModifiers
	
	IF NOT IS_BIT_SET(iBoolsBitSet, biL_AddedPhoneContact)
		ADD_PED_FOR_DIALOGUE(sSpeech, 2, NULL, "AGENT14")
		SET_BIT(iBoolsBitSet, biL_AddedPhoneContact)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_BIKER_DEFEND_MIDDLE_PHONE_CALLS] Set biL_AddedPhoneContact")
	ENDIF
	//ADD BIT TO TRIGGER PHONE CALL
//IF !IS_BIT_SET(iBoolsBitSet2, biL2_MidCallFinished)
	IF IS_CONVERSATION_STATUS_FREE()
		//CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_BIKER_DEFEND_MIDDLE_PHONE_CALLS] 0 ")
		IF IS_SERVER_BIT_SET(biS_WithinBombTextRange)
			//CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_BIKER_DEFEND_MIDDLE_PHONE_CALLS] 1 ")
			IF NOT IS_BIT_SET(iBoolsBitSet2, biL2_DoneMidPhonecall)
			//	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_BIKER_DEFEND_MIDDLE_PHONE_CALLS] 2 ")
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				//	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_BIKER_DEFEND_MIDDLE_PHONE_CALLS] 3 ")
					myUiLevel = GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())
					IF myUiLevel >= GB_UI_LEVEL_MINIMAL
					//	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_BIKER_DEFEND_MIDDLE_PHONE_CALLS] 4 ")
						//IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
						//IF NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GB_START_OF_JOB)	
							IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE,DEFAULT, FALSE)
							//	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_BIKER_DEFEND_MIDDLE_PHONE_CALLS] 5 ")
								IF IS_STRING_NULL_OR_EMPTY(tl15TxtMid)
								//TODO : Random string
									tl15TxtMid = GET_GB_GUNRUNNING_DEFEND_MID_PHONE_CALL()
								ENDIF
								IF NOT IS_STRING_NULL_OR_EMPTY(tl15TxtMid)
									SET_BIT(phonecallModifiers, MP_COMMS_MODIFIER_FORCE_PLAYER_TO_ANSWER)
							//		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_BIKER_DEFEND_MIDDLE_PHONE_CALLS] Doing intro call with label ", tl15TxtMid)
									IF Request_MP_Comms_Message(sSpeech, GET_GUNRUNNING_CONTACT(),  "GNRCAUD", tl15TxtMid, phonecallModifiers)
																											
										SET_BIT(iBoolsBitSet2, biL2_DoneMidPhonecall)
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_BIKER_DEFEND_MIDDLE_PHONE_CALLS] Done mid call")
									ELSE
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_BIKER_DEFEND_MIDDLE_PHONE_CALLS] Request_MP_Comms_Message(sSpeech, GET_MY_BUSINESS_PHONE_CONTACT()")
									ENDIF
									
									IF NOT HAS_NET_TIMER_STARTED(timeMidBombPhonecall)
										START_NET_TIMER(timeMidBombPhonecall)
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_BIKER_DEFEND_MIDDLE_PHONE_CALLS] Start timePhonecall")
									ENDIF
								ELSE
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_BIKER_DEFEND_MIDDLE_PHONE_CALLS] IF NOT IS_STRING_NULL_OR_EMPTY(tl15Txt) ")
								ENDIF
							ELSE
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_BIKER_DEFEND_MIDDLE_PHONE_CALLS]  IS_OK_TO_PRINT_FREEMODE_HELP(FALSE,DEFAULT, FALSE)")
							ENDIF
						//ELSE
							//CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_BIKER_DEFEND_INTRO_PHONE_CALLS]  GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG)")
						//ENDIF
		//				ELIF !IS_BIT_SET(iBoolsBitSet2, biL2_IntroCallFinished)
		//					SET_BIT(iBoolsBitSet2, biL2_IntroCallFinished)
		//					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_BIKER_DEFEND_INTRO_PHONE_CALLS]  IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GB_START_OF_JOB) - SET_BIT(iBoolsBitSet2, biL2_IntroCallFinished)")
		//				ENDIF
					ELSE
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_BIKER_DEFEND_MIDDLE_PHONE_CALLS] IF myUiLevel >= GB_UI_LEVEL_MINIMAL ")
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(iBoolsBitSet2, biL2_MidCallOnGoing)
					IF IS_CELLPHONE_CONVERSATION_PLAYING() 
						SET_BIT(iBoolsBitSet2, biL2_MidCallOnGoing)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_BIKER_DEFEND_MIDDLE_PHONE_CALLS] Set biL2_MidCallOnGoing")
					ELSE
						IF NOT Did_MP_Comms_Complete_Successfully()
						AND NOT Is_MP_Comms_Still_Playing()
							CLEAR_BIT(iBoolsBitSet2, biL2_DoneMidPhonecall)
						//	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_BIKER_DEFEND_MIDDLE_PHONE_CALLS]  Clear biL_DoneMidPhonecall as Did_MP_Comms_Complete_Successfully / Is_MP_Comms_Still_Playing")
						ELSE
						///	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_BIKER_DEFEND_MIDDLE_PHONE_CALLS] Did_MP_Comms_Complete_Successfully / Is_MP_Comms_Still_Playing")
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_BIT_SET(iBoolsBitSet2, biL2_MidCallFinished)
						IF NOT IS_CELLPHONE_CONVERSATION_PLAYING() 
							IF NOT IS_PHONE_ONSCREEN()
								SET_BIT(iBoolsBitSet2, biL2_MidCallFinished)
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_BIKER_DEFEND_MIDDLE_PHONE_CALLS] Set biL2_MidCallFinished")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
//ENDIF
	
	IF NOT IS_BIT_SET(iBoolsBitSet2, biL2_MidCallFinished)
		IF HAS_NET_TIMER_STARTED(timeMidBombPhonecall)
			IF HAS_NET_TIMER_EXPIRED(timeMidBombPhonecall, 12000)
				SET_BIT(iBoolsBitSet2, biL2_MidCallFinished)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_BIKER_DEFEND_MIDDLE_PHONE_CALLS] timePhonecall expired - set biL2_MidCallFinished")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_GB_GUNRUNNING_DEFEND_INTRO_SHARD()
	IF NOT DID_MY_BOSS_LAUNCH_GB_GB_GUNRUNNING_DEFEND()

		EXIT
	ENDIF
	
	GB_UI_LEVEL myUiLevel
	
	IF NOT IS_BIT_SET(iBoolsBitSet, biL_SetupTelemetry)
		myUiLevel = GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())
		IF myUiLevel >= GB_UI_LEVEL_MINIMAL
			SET_BIT(iBoolsBitSet, biL_SetupTelemetry)
			GB_SET_COMMON_TELEMETRY_DATA_ON_START(serverBD.iVariation, INT_TO_ENUM(FACTORY_ID, serverBD.iBusinessToDefend))
			
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_GB_GUNRUNNING_DEFEND_INTRO_SHARD] Set biL_SetupTelemetry")
		ENDIF
	ENDIF
	
	IF NOT SHOULD_LOCAL_PLAYER_SEE_GB_GB_GUNRUNNING_DEFEND_HUD()
		EXIT
	ENDIF
	
	
	
	
	INT iPart = PARTICIPANT_ID_TO_INT()
	PLAYER_INDEX player = PLAYER_ID()
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
		iPart = GET_MY_SPEC_TARGET_AS_PARTICIPANT_INT()
		player = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget
	ENDIF
	
//	STRING sOrganization
//	HUD_COLOURS hclPlayer
	
	IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneIntroShard)
		myUiLevel = GB_GET_PLAYER_UI_LEVEL(player)
		IF myUiLevel >= GB_UI_LEVEL_MINIMAL
			IF iPart = serverBD.iBossPartWhoLaunched
				//-- I'm the boss who launched event
				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_START_OF_JOB, "DGUN_DBUSI", "DGUN_RECWEP") // Defend your business
				SET_BIT(iBoolsBitSet, biL_DoneIntroShard)

				
				
				IF IS_BIT_SET(iBoolsBitSet, biL_DoneIntroShard)
					IF player = PLAYER_ID()
						GB_SET_LOCAL_PLAYER_CRITICAL_TO_JOB(TRUE)
					ENDIF
					
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_GB_GUNRUNNING_DEFEND_INTRO_SHARD] I LAUNCHED WORK")
				ENDIF
			
			ELIF GET_MY_BOSS_PARTICPANT_ID() = serverBD.iBossPartWhoLaunched

				//-- I'm the boss who launched event
				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_START_OF_JOB, "DGUN_DBUSI", "DGUN_RECWEP") // Defend your business
				SET_BIT(iBoolsBitSet, biL_DoneIntroShard)
				
				IF player = PLAYER_ID()
					GB_SET_LOCAL_PLAYER_CRITICAL_TO_JOB(TRUE)
				ENDIF

				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_GB_GUNRUNNING_DEFEND_INTRO_SHARD] I'M ON GANG WHO LAUNCHED WORK")
				
			ELSE
				SET_BIT(iBoolsBitSet, biL_DoneIntroShard)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_GB_GUNRUNNING_DEFEND_INTRO_SHARD] I'M A RIVAL")

			ENDIF
		ELSE
			SET_BIT(iBoolsBitSet, biL_DoneIntroShard)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_GB_GUNRUNNING_DEFEND_INTRO_SHARD] NOT DOING SHARD BECAUSE myUiLevel = ", ENUM_TO_INT(myUiLevel))
		ENDIF

		
	ENDIF
ENDPROC

//-- Some helper functions for determining who is in the contraband vehicle
FUNC BOOL ARE_ANY_ENEMY_PEDS_STILL_IN_CONTRABAND_VEHICLE()
	VEHICLE_INDEX veh
	PED_INDEX ped
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[0].niVeh)
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.Targets[0].niVeh)
			veh = NET_TO_VEH(serverBD.Targets[0].niVeh)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[0].niPed)
				IF NOT IS_NET_PED_INJURED(serverBD.Targets[0].niPed)
					ped = NET_TO_PED(serverBD.Targets[0].niPed)
					IF IS_PED_IN_VEHICLE(ped, veh)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[1].niPed)
				IF NOT IS_NET_PED_INJURED(serverBD.Targets[1].niPed)
					ped = NET_TO_PED(serverBD.Targets[1].niPed)
					IF IS_PED_IN_VEHICLE(ped, veh)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_CONTRABAND_VEHICLE_HAVE_AI_DRIVER()
	PED_INDEX pedTemp
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[0].niVeh)
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.Targets[0].niVeh)
			pedTemp = GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(serverBD.Targets[0].niVeh))
			IF pedTemp != NULL
				IF NOT IS_PED_INJURED(pedTemp)
					IF NOT IS_PED_A_PLAYER(pedTemp)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC PLAYER_INDEX GET_PLAYER_DRIVING_CONTRA_VEHICLE()
	PLAYER_INDEX playerTemp = INVALID_PLAYER_INDEX()
	IF serverBD.iPlayerDrivingContraVehicle > -1
		playerTemp = INT_TO_PLAYERINDEX(serverBD.iPlayerDrivingContraVehicle)
	ENDIF
	
	RETURN playerTemp
ENDFUNC

PROC MAINTAIN_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT()

	
//	INT iTempGangID
//	HUD_COLOURS hcTempGang
//	PLAYER_INDEX playerTemp
	
	PLAYER_INDEX player = PLAYER_ID()
	//INT iPart = PARTICIPANT_ID_TO_INT()
	
	IF !IS_BIT_SET(iBoolsBitSet, biL_DoneIntroShard)
	OR !IS_BIT_SET(iBoolsBitSet2, biL2_IntroCallFinished)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT]  -INTRO CALL/SHARD NOT FINISHED")
		EXIT
	ENDIF
	
	
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
	//	iPart = GET_MY_SPEC_TARGET_AS_PARTICIPANT_INT()
		player = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget
	ENDIF
	
	
//	STRING sOrganization

	IF player = INVALID_PLAYER_INDEX()
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT] Exit - player = INVALID_PLAYER_INDEX()")
		EXIT
	ENDIF
	
	IF NOT NETWORK_IS_PLAYER_ACTIVE(player)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT] Exit - NETWORK_IS_PLAYER_ACTIVE(player)")
		EXIT
	ENDIF
	
	
	
	GB_UI_LEVEL myUiLevel = GB_GET_PLAYER_UI_LEVEL(player)
	
	IF NOT SHOULD_LOCAL_PLAYER_SEE_GB_GB_GUNRUNNING_DEFEND_HUD()
		IF Is_This_The_Current_Objective_Text("DCONTRA_OBJ")
			Clear_Any_Objective_Text_From_This_Script()
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT] CLEAR DCONTRA_OBJ AS SHOULD_LOCAL_PLAYER_SEE_GB_GB_GUNRUNNING_DEFEND_HUD")
		ENDIF
		
		IF Is_This_The_Current_Objective_Text("DGUN_ODEF1")
			Clear_Any_Objective_Text_From_This_Script()
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT] CLEAR DGUN_ODEF1 AS SHOULD_LOCAL_PLAYER_SEE_GB_GB_GUNRUNNING_DEFEND_HUD")
		ENDIF
		
		IF Is_This_The_Current_Objective_Text("DGUN_ODEF")
			Clear_Any_Objective_Text_From_This_Script()
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT] CLEAR DGUN_ODEF AS SHOULD_LOCAL_PLAYER_SEE_GB_GB_GUNRUNNING_DEFEND_HUD")
		ENDIF
		EXIT
	ENDIF
	
	IF myUiLevel < GB_UI_LEVEL_FULL
		IF Is_This_The_Current_Objective_Text("DGUN_ODEF1")
			Clear_Any_Objective_Text_From_This_Script()
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT] CLEAR DGUN_ODEF1 AS myUiLevel = ", ENUM_TO_INT(myUiLevel))
		ENDIF
		
		IF Is_This_The_Current_Objective_Text("DGUN_ODEF")
			Clear_Any_Objective_Text_From_This_Script()
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT] CLEAR DGUN_ODEF AS myUiLevel = ", ENUM_TO_INT(myUiLevel))
		ENDIF
		
		IF Is_This_The_Current_Objective_Text("DCONTRA_OBJ")
			Clear_Any_Objective_Text_From_This_Script()
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT] CLEAR DCONTRA_OBJ AS myUiLevel = ", ENUM_TO_INT(myUiLevel))
		ENDIF
		EXIT
	ENDIF
	
	IF HAS_MISSION_END_CONDITION_BEEN_MET()
		EXIT
	ENDIF
	
//	INT iUncollected
	//HUD_COLOURS hclPlayer
	TEXT_LABEL_15 tl15Obj
	TEXT_LABEL_15 tl15Sub
	PLAYER_INDEX playerDriving
	INT iTotalBombs 
	INT iBombsDefused
	INT iUncollected
	BOOL bOneRemaining
	//FLOAT tempFloat
	PLAYER_INDEX playerDefuse
	SWITCH serverBD.iVariation
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
		//Go to the action area.
			IF !IS_SERVER_BIT_SET(biS_SomeoneNearActionArea)
			AND !IS_SERVER_BIT_SET(biS_EnemiesAlerted)
			//PRINTLN("JS- variation = ", serverbd.iVariation, " location = ", serverbd.iBusinessToDefend , " JS-TEMPFLOAT = ", tempFloat)
		
		//	IF !IS_SERVER_BIT_SET(biS_ApproachedWarehouse)
			//AND !IS_BIT_SET(playerBD[iPart].iPlayerBitSet, biP_ApproachedWarehouse)
				tl15Obj = GET_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT_FOR_APPROACH_ACTION(serverBD.iVariation, serverBD.iBusinessToDefend)
				IF NOT IS_STRING_NULL_OR_EMPTY(tl15Obj)
					IF !IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WithinObjectiveRange)
						IF NOT Is_This_The_Current_Objective_Text(tl15Obj)
							Print_Objective_Text(tl15Obj)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT] Told to go to action area tl15Obj = ", tl15Obj)
						ENDIF
					ENDIF
				ENDIF
			ELSE
		
//			IF !IS_SERVER_BIT_SET(biS_AllInitalTargetsKilled)
//					IF iPart = serverBD.iBossPartWhoLaunched
//						//-- I'm the boss who launched event
//						IF NOT Is_This_The_Current_Objective_Text("DCONTRA_OBJ")
//							Print_Objective_Text("DCONTRA_OBJ") // Kill the ~r~targets.
//							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT] DONE DCONTRA_OBJ - I LAUNCHED")
//						ENDIF
//					ELIF GET_MY_BOSS_PARTICPANT_ID() = serverBD.iBossPartWhoLaunched
//						//-- My boss launched the works
//
//						IF NOT Is_This_The_Current_Objective_Text("DCONTRA_OBJ")
//							Print_Objective_Text("DCONTRA_OBJ") // Kill the ~r~targets.
//							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT] DONE DCONTRA_OBJ - MY BOSS LAUNCHED")
//						ENDIF
//					ENDIF
//				ELSE
					iTotalBombs 	= GET_GUNRUNNING_DEFEND_NUMBER_OF_BOMBS_FOR_BUSINESS()
					iBombsDefused 	= GET_NUMBER_OF_DEFUSED_BOMBS()
					bOneRemaining	= (iTotalBombs - iBombsDefused) <= 1
					
					IF NOT IS_PLAYER_DEFUSING_A_BOMB(PLAYER_ID())
						IF IS_ANY_BOMB_NOT_BEING_DEFUSED()
							IF iBombsDefused < iTotalBombs
								IF bOneRemaining
									IF NOT Is_This_The_Current_Objective_Text("DGUN_ODEF1")
										Print_Objective_Text("DGUN_ODEF1") // Defuse the last ~r~bomb.
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT] DONE DGUN_ODEF1 iTotalBombs = ", iTotalBombs, " iBombsDefused = ", iBombsDefused, " bOneRemaining = ", bOneRemaining)
									ENDIF
								ELSE
									IF NOT Is_This_The_Current_Objective_Text("DGUN_ODEF")
										Print_Objective_Text("DGUN_ODEF") // Defuse the ~r~bombs.
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT] DONE DGUN_ODEF iTotalBombs = ", iTotalBombs, " iBombsDefused = ", iBombsDefused, " bOneRemaining = ", bOneRemaining)
									ENDIF
								ENDIF
							ELSE
								IF	!IS_SERVER_BIT_SET(biS_AllBombsDefused)
									Clear_Any_Objective_Text_From_This_Script()
								ENDIF
							ENDIF
						ELSE
							playerDefuse = GET_ANY_PLAYER_DEFUSING_A_BOMB()
							IF playerDefuse != INVALID_PLAYER_INDEX()
								IF NOT Is_This_The_Current_Objective_Text("DGUN_PRDEF")
									Print_Objective_Text_With_Player_Name("DGUN_PRDEF", playerDefuse) // Protect <C>~a~</C> ~s~while they defuse a bomb.
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT] Told to protect player ", GET_PLAYER_NAME(playerDefuse))
								ENDIF
							ELSE
								IF	!IS_SERVER_BIT_SET(biS_AllBombsDefused)
									Clear_Any_Objective_Text_From_This_Script()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF IS_SERVER_BIT_SET(biS_AllBombsDefused)
						IF NOT IS_BIT_SET(serverBD.packagesServer.iServerPackageBitset, SERVER_GB_PACKAGE_ALLOW_COLLECT)
							IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InVeh)
								//-- I'm in possession of the package
									IF !_FREEMODE_DELIVERY_DOES_PLAYER_POSSESS_VALID_DELIVERABLE(PLAYER_ID()) 
										FREEMODE_DELIVERY_SET_LOCAL_PLAYER_IN_POSSESSION_OF_DELIVERABLE(serverBD.deliverableGunrunIds[0])
									ENDIF
								IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
									IF NOT Is_This_The_Current_Objective_Text("DGUN_DELTSM")
										print_objective_text("DGUN_DELTSM") //
									ENDIF
										
									IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ApproachedWarehouse)
									OR IS_SERVER_BIT_SET(biS_ApproachedWarehouse)
										//Activate gang chase
										IF IS_SERVER_BIT_SET(biS_GangChaseDelayTimerUp)
											//SET_LOCAL_PLAYER_IS_VALID_GANG_CHASE_TARGET()
										ENDIF	
									ENDIF
									IF !IS_BIT_SET(iBoolsBitSet,biL_GangChaseStarted)
										IF IS_SERVER_BIT_SET(biS_GangChaseDelayTimerUp)
											SET_LOCAL_PLAYER_IS_VALID_GANG_CHASE_TARGET()
											SET_BIT(iBoolsBitSet,biL_GangChaseStarted)
										ENDIF
									ENDIF
									//	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT] Told to return the vehicle")
								ELSE
									IF NOT Is_This_The_Current_Objective_Text("DBR_LCOPS")
										print_objective_text("DBR_LCOPS")
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT] Told to lose cops")
									ENDIF
								ENDIF
								
							
							ELIF serverBD.iPartDrivingContraVehicle = -1
								IF _FREEMODE_DELIVERY_DOES_PLAYER_POSSESS_VALID_DELIVERABLE(PLAYER_ID()) 
									FREEMODE_DELIVERY_CLEAR_LP_DELIVERABLE_POSSESSION_FOR_MISSION(serverBD.sMissionID)
								ENDIF
								
								tl15Sub = GET_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT_VEHICLE_BLIP_NAME(serverBD.iVariation, serverBD.iBusinessToDefend)
								//-- Nobody driving vehicle
								//"Retrieve the Pounder containing the product"
								IF DOES_BLIP_EXIST(blipWarehouse)
									REMOVE_BLIP(blipWarehouse)
								ENDIF
								ADD_BLIP_FOR_CONTRABAND_VEHICLE(0, BLIP_COLOUR_BLUE, FALSE)
								
								IF NOT Is_This_The_Current_Objective_Text("DGUN_RTRVPROD")
									IF !IS_BIT_SET(iBoolsBitSet,biL_StealthStageStarted)
										SET_BIT(iBoolsBitSet,biL_StealthStageStarted)
									ENDIF
									Print_Objective_Text_With_Coloured_Text_Label("DGUN_RTRVPROD", tl15Sub, HUD_COLOUR_BLUE)
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT] Done retreive vehicle text tl15Sub = ", tl15Sub)
								ENDIF
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WithinObjectiveRange)
							ELSE
								playerDriving = GET_PLAYER_DRIVING_CONTRABAND_VEHICLE()
								IF playerDriving != INVALID_PLAYER_INDEX()
									tl15Sub = GET_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT_VEHICLE_BLIP_NAME(serverBD.iVariation, serverBD.iBusinessToDefend)
									IF NOT Is_This_The_Current_Objective_Text("DGUN_HDELPK")
										Print_Objective_Text_With_Coloured_Text_Label("DGUN_HDELPK", tl15Sub, HUD_COLOUR_BLUE)
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT] Told to help deliver vehicle, player driving = ", GET_PLAYER_NAME(playerDriving))
									ENDIF
								ENDIF
							ENDIF
						ELSE
						//SET_BIT(serverBD.iServerBitSet, biS_ValkyrieDestroyed)
							PRINTLN("JS-VALKYRIE IN OR DESTROYED BIT SET")
							//-- Valkyrie destroyed or entered
							IF DOES_PART_HAVE_ANY_GB_PACKAGE(PARTICIPANT_ID_TO_INT())
									IF !_FREEMODE_DELIVERY_DOES_PLAYER_POSSESS_VALID_DELIVERABLE(PLAYER_ID()) 
										FREEMODE_DELIVERY_SET_LOCAL_PLAYER_IN_POSSESSION_OF_DELIVERABLE(serverBD.deliverableGunrunIds[0])
									ENDIF
									IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
									//Return the product to the business.
										//Activate gang chase
										
										IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ApproachedWarehouse)
										OR IS_SERVER_BIT_SET(biS_ApproachedWarehouse)
											IF IS_SERVER_BIT_SET(biS_GangChaseDelayTimerUp)
												//SET_LOCAL_PLAYER_IS_VALID_GANG_CHASE_TARGET()
											ENDIF
										ENDIF
										PRINTLN("JS-GANGCHASESTARTED")
										//cant set server bits on client
										//SET_SERVER_BIT(biS_GangChaseStarted)
										IF !IS_BIT_SET(iBoolsBitSet,biL_GangChaseStarted)
											IF IS_SERVER_BIT_SET(biS_GangChaseDelayTimerUp)
												SET_LOCAL_PLAYER_IS_VALID_GANG_CHASE_TARGET()
												SET_BIT(iBoolsBitSet,biL_GangChaseStarted)
											ENDIF
										ENDIF
									
										IF NOT Is_This_The_Current_Objective_Text("DGUN_DELTSM")
											print_objective_text("DGUN_DELTSM")
											CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT] Told to return goods as I have them")
										ENDIF
									ELSE
										IF NOT Is_This_The_Current_Objective_Text("DBR_LCOPS")
											print_objective_text("DBR_LCOPS")
											CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT] Told to lose cops")
										ENDIF
									ENDIF
								//Pick up the product.
							ELIF IS_ANY_GB_PACKAGE_UNCOLLECTED(serverBD.packagesServer, iUncollected)
								PRINTLN("JS-ANYPACKAGEUNCOLLECTED")
								
								IF _FREEMODE_DELIVERY_DOES_PLAYER_POSSESS_VALID_DELIVERABLE(PLAYER_ID()) 
									FREEMODE_DELIVERY_CLEAR_LP_DELIVERABLE_POSSESSION_FOR_MISSION(serverBD.sMissionID)
								ENDIF
								IF NOT Is_This_The_Current_Objective_Text("DGUN_RETPRO")
									Print_Objective_Text_With_Coloured_Text_Label("DGUN_RETPRO", "DDGUN_PROD", HUD_COLOUR_GREEN)
									//cant set server bits on client
									IF !IS_BIT_SET(iBoolsBitSet2,biL2_HeloShotDown)
									 	SET_BIT(iBoolsBitSet2, biL2_HeloShotDown)
									ENDIF
								
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT] Told to retrieve goods as one has been dropped")
								ENDIF
								
							ELSE
							PRINTLN("JS-ALL PACKAGE HOLDERS IN GANG?")
								IF ARE_ALL_GB_PACKAGE_HOLDERS_IN_MY_GANG(serverBD.packagesServer)
									IF NOT Is_This_The_Current_Objective_Text("DGUN_HDELPK")
									//	hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(player)
										Print_Objective_Text_With_Coloured_Text_Label("DGUN_HDELPK", "DDGUN_PROD", HUD_COLOUR_GREEN)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				//ENDIF
			ENDIF
		BREAK
		
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
		
		 IF	IS_NET_VEHICLE_DRIVEABLE(serverBD.niPlayerVeh[0])
		 ENDIF
		 //use central coord, so that way you dont even need to check if the heli is alive.
		 //TODO:
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), GET_GB_GUNRUNNING_DEFEND_CONTRABAND_VEHICLE_LOCATION(serverBD.iVariation, serverBD.iBusinessToDefend)) >= 100
			AND ! IS_SERVER_BIT_SET(biS_EnemiesAlerted)
			AND ! IS_BIT_SET(serverBD.packagesServer.iServerPackageBitset, SERVER_GB_PACKAGE_ALLOW_COLLECT)
				PRINTLN("JS-PLAYER WITHIN 100M of the objective vehicle")	
			
				tl15Obj = GET_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT_FOR_APPROACH_ACTION(serverBD.iVariation, serverBD.iBusinessToDefend)
				IF NOT IS_STRING_NULL_OR_EMPTY(tl15Obj)
					IF !IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WithinObjectiveRange)
						IF NOT Is_This_The_Current_Objective_Text(tl15Obj)
							Print_Objective_Text(tl15Obj)
							//Go to the action area.
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT] Told to go to action area tl15Obj = ", tl15Obj)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(serverBD.packagesServer.iServerPackageBitset, SERVER_GB_PACKAGE_ALLOW_COLLECT)
					PRINTLN("JS-000 1")	
					IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InVeh)
						PRINTLN("JS-000 2")	
						//-- I'm in possession of the package
						IF !_FREEMODE_DELIVERY_DOES_PLAYER_POSSESS_VALID_DELIVERABLE(PLAYER_ID()) 
							FREEMODE_DELIVERY_SET_LOCAL_PLAYER_IN_POSSESSION_OF_DELIVERABLE(serverBD.deliverableGunrunIds[0])
						ENDIF
						IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
							IF NOT Is_This_The_Current_Objective_Text("DGUN_DELTSM")
								print_objective_text("DGUN_DELTSM") //
							ENDIF
								
							IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ApproachedWarehouse)
							OR IS_SERVER_BIT_SET(biS_ApproachedWarehouse)
								//Activate gang chase
								IF IS_SERVER_BIT_SET(biS_GangChaseDelayTimerUp)
									SET_LOCAL_PLAYER_IS_VALID_GANG_CHASE_TARGET()
								ENDIF	
							ENDIF
							IF !IS_BIT_SET(iBoolsBitSet,biL_GangChaseStarted)
								IF IS_SERVER_BIT_SET(biS_GangChaseDelayTimerUp)
									SET_BIT(iBoolsBitSet,biL_GangChaseStarted)
								ENDIF
							ENDIF
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT] Told to return the vehicle")
						ELSE
							IF NOT Is_This_The_Current_Objective_Text("DBR_LCOPS")
								print_objective_text("DBR_LCOPS")
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT] Told to lose cops")
							ENDIF
						ENDIF
						
					
					ELIF serverBD.iPartDrivingContraVehicle = -1
						IF _FREEMODE_DELIVERY_DOES_PLAYER_POSSESS_VALID_DELIVERABLE(PLAYER_ID()) 
							FREEMODE_DELIVERY_CLEAR_LP_DELIVERABLE_POSSESSION_FOR_MISSION(serverBD.sMissionID)
						ENDIF
						tl15Sub = GET_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT_VEHICLE_BLIP_NAME(serverBD.iVariation, serverBD.iBusinessToDefend)
						//-- Nobody driving vehicle
						//"Retrieve the Valkyrie containing the product"
						PRINTLN("JS-000 3")	
						IF DOES_BLIP_EXIST(blipWarehouse)
							REMOVE_BLIP(blipWarehouse)
						ENDIF
						ADD_BLIP_FOR_CONTRABAND_VEHICLE(0, BLIP_COLOUR_BLUE, FALSE)
					//	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT] Done retreive vehicle text tl15Sub = ", tl15Sub)
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WithinObjectiveRange)
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.pilotPed)
							PRINTLN("JS-000 4")	
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPlayerVeh[0])
								PRINTLN("JS-000 5")	
								IF IS_PED_SITTING_IN_VEHICLE_SEAT(NET_TO_PED(serverbd.pilotPed), NET_TO_VEH(serverBD.niPlayerVeh[0]),VS_DRIVER)
									IF NOT Is_This_The_Current_Objective_Text("DGUN_RTRVPROD2")
										//cant set server bits on client
										PRINTLN("JS-000 6")	
										IF !IS_BIT_SET(iBoolsBitSet,biL_StealthStageStarted)
											SET_BIT(iBoolsBitSet,biL_StealthStageStarted)
										ENDIF
										Print_Objective_Text_With_Coloured_Text_Label("DGUN_RTRVPROD2", tl15Sub, HUD_COLOUR_RED)
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT] Done destroy vehicle text tl15Sub = ", tl15Sub)
									ENDIF
								ELSE
									IF NOT Is_This_The_Current_Objective_Text("DGUN_RTRVPROD")
										//cant set server bits on client
										PRINTLN("JS-000 7")	
										IF !IS_BIT_SET(iBoolsBitSet,biL_StealthStageStarted)
											SET_BIT(iBoolsBitSet,biL_StealthStageStarted)
										ENDIF
										Print_Objective_Text_With_Coloured_Text_Label("DGUN_RTRVPROD", tl15Sub, HUD_COLOUR_BLUE)
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT] Done retreive vehicle text tl15Sub = ", tl15Sub)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
					ELSE
						playerDriving = GET_PLAYER_DRIVING_CONTRABAND_VEHICLE()
						IF playerDriving != INVALID_PLAYER_INDEX()
							tl15Sub = GET_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT_VEHICLE_BLIP_NAME(serverBD.iVariation, serverBD.iBusinessToDefend)
							IF NOT Is_This_The_Current_Objective_Text("DGUN_HDELPK")
								Print_Objective_Text_With_Coloured_Text_Label("DGUN_HDELPK", tl15Sub, HUD_COLOUR_BLUE)
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT] Told to help deliver vehicle, player driving = ", GET_PLAYER_NAME(playerDriving))
							ENDIF
						ENDIF
					ENDIF
				ELSE
				//SET_BIT(serverBD.iServerBitSet, biS_ValkyrieDestroyed)
					PRINTLN("JS-VALKYRIE IN OR DESTROYED BIT SET")
					//-- Valkyrie destroyed or entered
					IF DOES_PART_HAVE_ANY_GB_PACKAGE(PARTICIPANT_ID_TO_INT())
						IF !_FREEMODE_DELIVERY_DOES_PLAYER_POSSESS_VALID_DELIVERABLE(PLAYER_ID()) 
							FREEMODE_DELIVERY_SET_LOCAL_PLAYER_IN_POSSESSION_OF_DELIVERABLE(serverBD.deliverableGunrunIds[0])
						ENDIF
						IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
						//Return the product to the business.
							//Activate gang chase
							
							IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ApproachedWarehouse)
							OR IS_SERVER_BIT_SET(biS_ApproachedWarehouse)
								IF IS_SERVER_BIT_SET(biS_GangChaseDelayTimerUp)
									SET_LOCAL_PLAYER_IS_VALID_GANG_CHASE_TARGET()
								ENDIF
							ENDIF
							PRINTLN("JS-GANGCHASESTARTED")
							//cant set server bits on client
							//SET_SERVER_BIT(biS_GangChaseStarted)
							IF !IS_BIT_SET(iBoolsBitSet,biL_GangChaseStarted)
								IF IS_SERVER_BIT_SET(biS_GangChaseDelayTimerUp)
									SET_BIT(iBoolsBitSet,biL_GangChaseStarted)
								ENDIF
							ENDIF
						
							IF NOT Is_This_The_Current_Objective_Text("DGUN_DELTSM")
								print_objective_text("DGUN_DELTSM")
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT] Told to return goods as I have them")
							ENDIF
						ELSE
							IF NOT Is_This_The_Current_Objective_Text("DBR_LCOPS")
								print_objective_text("DBR_LCOPS")
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT] Told to lose cops")
							ENDIF
						ENDIF
						//Pick up the product.
					ELIF IS_ANY_GB_PACKAGE_UNCOLLECTED(serverBD.packagesServer, iUncollected)
						IF _FREEMODE_DELIVERY_DOES_PLAYER_POSSESS_VALID_DELIVERABLE(PLAYER_ID()) 
							FREEMODE_DELIVERY_CLEAR_LP_DELIVERABLE_POSSESSION_FOR_MISSION(serverBD.sMissionID)
						ENDIF
						PRINTLN("JS-ANYPACKAGEUNCOLLECTED")
						IF NOT Is_This_The_Current_Objective_Text("DGUN_RETPRO")
							Print_Objective_Text_With_Coloured_Text_Label("DGUN_RETPRO", "DDGUN_PROD", HUD_COLOUR_GREEN)
							//cant set server bits on client
							IF !IS_BIT_SET(iBoolsBitSet2,biL2_HeloShotDown)
							 	SET_BIT(iBoolsBitSet2, biL2_HeloShotDown)
							ENDIF
						
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT] Told to retrieve goods as one has been dropped")
						ENDIF
						
					ELSE
						PRINTLN("JS-ALL PACKAGE HOLDERS IN GANG?")
						IF ARE_ALL_GB_PACKAGE_HOLDERS_IN_MY_GANG(serverBD.packagesServer)
							IF NOT Is_This_The_Current_Objective_Text("DGUN_HDELPK")
							//	hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(player)
								Print_Objective_Text_With_Coloured_Text_Label("DGUN_HDELPK", "DDGUN_PROD", HUD_COLOUR_GREEN)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
		
		BREAK
		DEFAULT
		PRINTLN("JS- serverBD.iVariation = ", serverBD.iVariation, " from GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION = ")
		ASSERTLN("Debug widget used to change variation. Turn off this widget and use the M menu exclusively.")
		BREAK
		
	ENDSWITCH
	
ENDPROC

/// PURPOSE:
///    Determine whether or not the mission-end location (generally the business being defended) shoul;d be blipped
PROC MAINTAIN_GB_DEFEND_DROP_OFF()

	IF !IS_SERVER_BIT_SET(biS_ApproachedWarehouse)
	AND !IS_SERVER_BIT_SET(biS_EnemiesAlerted)
	AND !IS_SERVER_BIT_SET(biS_PilotTimerUp)
		EXIT
	ENDIF	
	
	VECTOR vMarkerSize = <<3.0, 3.0, 2.5 >>
	VECTOR vDropOff = GET_DROP_OFF_LOCATION()
	VECTOR vCoronaLocation = GET_CORONA_LOCATION()
	
	INT iR, iG, iB, iA
	
	PLAYER_INDEX playerDriving
	IF !IS_BIT_SET(iBoolsBitSet3,biL3_MaintainDropOffTriggered)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_DEFEND_DROP_OFF] First call ")
	ENDIF
	BOOL bShowMarker = FALSE
	BOOL bAddBlip
	SWITCH serverBD.iVariation
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
			IF NOT HAS_MISSION_END_CONDITION_BEEN_MET()
				IF NOT IS_BIT_SET(serverBD.packagesServer.iServerPackageBitset, SERVER_GB_PACKAGE_ALLOW_COLLECT)
					//TODO: may be stopping eveeryone from seeing the corona
					IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InVeh)
						IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
							bAddBlip = TRUE
							bShowMarker = TRUE
						ENDIF
					ELSE
						playerDriving = GET_PLAYER_DRIVING_CONTRABAND_VEHICLE()
						IF playerDriving != INVALID_PLAYER_INDEX()
							bAddBlip = TRUE
							bShowMarker = TRUE
						ENDIF
					ENDIF
				ELSE
				
					IF DOES_PART_HAVE_ANY_GB_PACKAGE(PARTICIPANT_ID_TO_INT())
						IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
							bAddBlip = TRUE
							bShowMarker = TRUE
						ENDIF
					ELSE
						IF ARE_ALL_GB_PACKAGE_HOLDERS_IN_MY_GANG(serverBD.packagesServer)
							bAddBlip = TRUE
							bShowMarker = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE				
			IF NOT HAS_MISSION_END_CONDITION_BEEN_MET()
				IF NOT IS_BIT_SET(serverBD.packagesServer.iServerPackageBitset, SERVER_GB_PACKAGE_ALLOW_COLLECT)
					IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InVeh)
						IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
							bAddBlip = TRUE
							bShowMarker = TRUE
						ENDIF
					ELSE
						playerDriving = GET_PLAYER_DRIVING_CONTRABAND_VEHICLE()
						IF playerDriving != INVALID_PLAYER_INDEX()
							bAddBlip = TRUE
							bShowMarker = TRUE
						ENDIF
					ENDIF
				ELSE
				
					IF DOES_PART_HAVE_ANY_GB_PACKAGE(PARTICIPANT_ID_TO_INT())
						IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
							bAddBlip = TRUE
							bShowMarker = TRUE
						ENDIF
					ELSE
						IF ARE_ALL_GB_PACKAGE_HOLDERS_IN_MY_GANG(serverBD.packagesServer)
							bAddBlip = TRUE
							bShowMarker = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
			
		//BREAK
		
//		CASE GB_GUNRUNNING_DEFEND_FAKE_ID
//			IF NOT HAS_MISSION_END_CONDITION_BEEN_MET()
//				IF DOES_PART_HAVE_ANY_GB_PACKAGE(PARTICIPANT_ID_TO_INT())
//					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
//						bAddBlip = TRUE
//						bShowMarker = TRUE
//					ENDIF
//				ELSE
//					IF ARE_ALL_GB_PACKAGE_HOLDERS_IN_MY_GANG(serverBD.packagesServer)
//						bAddBlip = TRUE
//						bShowMarker = TRUE
//					ENDIF
//				ENDIF
//			ENDIF
//		BREAK
		
		
	//ENDSWITCH 
	
	// Already being done in MAINTAIN_GB_GUNRUNNING_DEFEND_CONTRABAND_VEHICLE
	//NOW being done here.
	//UNUSED_PARAMETER(bAddBlip)
	IF bAddBlip
		//IF !IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WithinObjectiveRange)
			IF NOT DOES_BLIP_EXIST(blipWarehouseDropOff)
				blipWarehouseDropOff = ADD_BLIP_FOR_COORD(vDropOff)
				SET_BLIP_ROUTE(blipWarehouseDropOff, TRUE)
				SET_BLIP_NAME_FROM_TEXT_FILE(blipWarehouseDropOff, "BUNK_QUICK_GPS")
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_DEFEND_DROP_OFF] Added blip at coord ", vDropOff)
			ENDIF
		//ENDIF
	ELSE
		IF DOES_BLIP_EXIST(blipWarehouseDropOff)
			REMOVE_BLIP(blipWarehouseDropOff)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_DEFEND_DROP_OFF] removed blip")
		ENDIF
	ENDIF
//	IF bShowMarker
//		PRINTLN("JS-MARKERTRUE: vector = ", vDropOff)
//	ELSE
//		PRINTLN("JS-MARKERFALSE: vector = ", vDropOff)
//	ENDIF
	
	IF bShowMarker
		GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
		DRAW_MARKER(	MARKER_CYLINDER,
						vCoronaLocation,
						<<0,0,0>>,
						<<0,0,0>>,
						vMarkerSize,
						iR,
						iG,
						iB,
						150)
	ENDIF
ENDPROC

PROC MAINTAIN_GB_GUNRUNNING_DEFEND_HELP_TEXT()

	
	IF !IS_BIT_SET(iBoolsBitSet, biL_DoneIntroShard)
	OR !IS_BIT_SET(iBoolsBitSet2, biL2_IntroCallFinished)
	
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_HELP_TEXT] -INTRO CALL/SHARD NOT FINISHED")    
		EXIT
	ENDIF

	IF NOT SHOULD_LOCAL_PLAYER_SEE_GB_GB_GUNRUNNING_DEFEND_HUD()
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DCONTRA_HLP1")
			CLEAR_HELP()
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_HELP_TEXT] CLEAR DCONTRA_HLP1 AS SHOULD_LOCAL_PLAYER_SEE_GB_GUNRUNNING_DEFEND_HUD")
		ENDIF
		

		EXIT
	ENDIF
	
	GB_UI_LEVEL myUiLevel = GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())
	IF myUiLevel < GB_UI_LEVEL_MINIMAL
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DCONTRA_HLP1")
			CLEAR_HELP()
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_HELP_TEXT] CLEAR DCONTRA_HLP1 AS myUiLevel = ", ENUM_TO_INT(myUiLevel))
		ENDIF
		EXIT
	ENDIF
	
	IF HAS_MISSION_END_CONDITION_BEEN_MET()
		EXIT
	ENDIF
	
	
	
	
	IF NOT IS_BIT_SET(iBoolsBitSet2, biL2_IntroCallFinished)
	//	EXIT
	ENDIF
//	VECTOR vLoc
//	FLOAT fHead
//	TEXT_LABEL_15 tl15Help
//	INT iNumberOfBombs
//	INT iLocalNearbyBomb

		IF IS_SERVER_BIT_SET(biS_MissionStarted)
			AND !IS_BIT_SET(iBoolsBitSet,biL_HelpIntroTextSent)
					PRINT_HELP_NO_SOUND("GR_HELPINTRO")
				SET_BIT(iBoolsBitSet,biL_HelpIntroTextSent)
				PRINTLN("JS-HELPINTROTEXTSENT")
		ENDIF

	SWITCH serverBD.iVariation
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
			IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneIntroHelp)
				IF NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GB_START_OF_JOB)	
					IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
						IF PARTICIPANT_ID_TO_INT() = serverBD.iBossPartWhoLaunched

						//	PRINT_HELP_NO_SOUND("BK_DEFSH_HLP2") // Kill the targets before the time expires.
							
							GB_SET_GANG_BOSS_HELP_BACKGROUND()
							SET_BIT(iBoolsBitSet, biL_DoneIntroHelp)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_HELP_TEXT] DONE INTRO HELP - I LAUNCHED")
						ELIF GET_MY_BOSS_PARTICPANT_ID() = serverBD.iBossPartWhoLaunched

							//PRINT_HELP_NO_SOUND("BK_DEFSH_HLP2") // Kill the targets before the time expires.
							
							GB_SET_GANG_BOSS_HELP_BACKGROUND()
							SET_BIT(iBoolsBitSet, biL_DoneIntroHelp)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_HELP_TEXT] DONE INTRO HELP - MY BOSS LAUNCHED")
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneLoseContraHlp)

						IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
							IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
							//	PRINT_HELP_NO_SOUND("BK_DEFSH_HLPVCR") // You will lose contraband while there are still enemies attacking your warehouse.
							ELSE
							//	PRINT_HELP_NO_SOUND("BK_DEFSH_HLPCR") // Your organization will lose contraband while there are still enenmies attacking your warehouse.
							ENDIF
							GB_SET_GANG_BOSS_HELP_BACKGROUND()
							SET_BIT(iBoolsBitSet, biL_DoneLoseContraHlp)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_HELP_TEXT] DONE LOSE CONTRABAND HELP")
						ENDIF
					

				ENDIF	
				//IF IS_SERVER_BIT_SET(biS_ApproachedWarehouse)
				//ENDIF
				//if in the action area
				
				//print DGUN_HELPBOMB
			ENDIF
			
			//check if bomb instructions are being displayed.
//			 iNumberOfBombs = GET_GUNRUNNING_DEFEND_NUMBER_OF_BOMBS_FOR_BUSINESS()
//			 
//			IF IS_LOCAL_PLAYER_NEAR_ANY_BOMB(serverbd.iBusinessToDefend, iLocalNearbyBomb, iNumberOfBombs)
//				IF !IS_BIT_SET(iBoolsBitSet2,biL2_BombInstructionHelpTextSent)
//					PRINT_HELP_NO_SOUND("DGUN_HDEF")
//					CLEAR_BIT(iBoolsBitSet2,biL2_BombInstructionHelpTextCleared)
//			 		SET_BIT(iBoolsBitSet2,biL2_BombInstructionHelpTextSent)
//				ENDIF	
//			ELIF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DGUN_HDEF") 
//				IF !IS_BIT_SET(iBoolsBitSet2,biL2_BombInstructionHelpTextCleared)
//					CLEAR_ALL_HELP_MESSAGES()
//					CLEAR_BIT(iBoolsBitSet2,biL2_BombInstructionHelpTextSent)
//			 		SET_BIT(iBoolsBitSet2,biL2_BombInstructionHelpTextCleared)
//				ENDIF
//			ENDIF
//			
//			IF IS_SERVER_BIT_SET(biS_ApproachedWarehouse)
//				AND !IS_BIT_SET(iBoolsBitSet2,biL2_BombHelpTextSent)
//						PRINT_HELP_NO_SOUND("DGUN_HELPBOMB")
//					SET_BIT(iBoolsBitSet2,biL2_BombHelpTextSent)
//					PRINTLN("JS-BOMBHELPTEXTSENT")
//			ENDIF
	IF IS_SERVER_BIT_SET(biS_SomeoneNearActionArea)
		IF IS_SERVER_BIT_SET(biS_EnemiesAlerted)
			IF !IS_SERVER_BIT_SET(biS_AllBombsDefused)
				AND !IS_BIT_SET(iBoolsBitSet3,biL3_BombTimerHelpTextSent)
					PRINT_HELP_NO_SOUND("DGUN_HELPTIMER")
				SET_BIT(iBoolsBitSet3,biL3_BombTimerHelpTextSent)
			ENDIF
		ENDIF
	ENDIF

		BREAK
		
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			PRINTLN("JS-CASE VALKYRIE HELP MESSAGES")
			
	//		IF IS_SERVER_BIT_SET(biS_MissionStarted)
	//		AND !IS_SERVER_BIT_SET(biS_HelpIntroTextSent)
	//			SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(GET_GUNRUNNING_CONTACT(), "GR_HELPINTRO", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
	//			SET_SERVER_BIT(biS_HelpIntroTextSent)
	//			PRINTLN("JS-HELPINTROTEXTSENT")
	//		ENDIF
		
			
			IF IS_BIT_SET(iBoolsBitSet,biL_StealthStageStarted)
			AND !IS_BIT_SET(iBoolsBitSet, biL_HelpStealthTextSent)
				PRINT_HELP_NO_SOUND("GR_HELPSTEALTH")
				SET_BIT(iBoolsBitSet, biL_HelpStealthTextSent)
				PRINTLN("JS-HELPSTEALTHTEXTSENT")
			ENDIF
			
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED( "GR_HELPSTEALTH")
				SET_BIT(iBoolsBitSet, biL_StealthTextDisplaying)
			ELSE		
				IF IS_BIT_SET(iBoolsBitSet, biL_HelpStealthTextSent)
					CLEAR_BIT(iBoolsBitSet, biL_StealthTextDisplaying)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(iBoolsBitSet,biL_HelpStealthTextSent)
				IF !IS_BIT_SET(iBoolsBitSet,biL_StealthTextDisplaying)
				AND !IS_BIT_SET(iBoolsBitSet,biL_SpookTextSent)
					IF !IS_BIT_SET(serverBD.packagesServer.iServerPackageBitset, SERVER_GB_PACKAGE_ALLOW_COLLECT)
						PRINT_HELP_NO_SOUND("GR_SPOOKTEXT") 
					ENDIF
					SET_BIT(iBoolsBitSet,biL_SpookTextSent)
					PRINTLN("JS-SPOOKTEXTSENT")
				ENDIF
			ENDIF

		
			
			
			IF IS_BIT_SET(iBoolsBitSet2, biL2_HeloShotDown)
			AND !IS_BIT_SET(iBoolsBitSet,biL_HelpHeloDestroyTextSent)
					PRINT_HELP_NO_SOUND("DGUN_HELPDEST")
				SET_BIT(iBoolsBitSet,biL_HelpHeloDestroyTextSent)
				PRINTLN("JS-HELPINTROTEXTSENT")
			ENDIF
			
			
			
		BREAK
	ENDSWITCH
	
ENDPROC

PROC MAINTAIN_MARKER_FOR_CONTRABAND_VEHICLE(INT iVeh)
	INT r, g, b, a

	IF HAVE_MISSION_END_CONDITIONS_BEEN_MET()
		EXIT
	ENDIF

	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[iVeh].niVeh)
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.Targets[iVeh].niVeh)
			IF DOES_BLIP_EXIST(blipVeh[0])
				IF serverBD.iPlayerDrivingContraVehicle > -1
					GET_HUD_COLOUR(HUD_COLOUR_BLUE, r, g, b, a)
				ELSE
					IF iLocalPartDrivingContraVehicle = -2
						GET_HUD_COLOUR(HUD_COLOUR_RED, r, g, b, a)
					ELSE
						GET_HUD_COLOUR(HUD_COLOUR_BLUE, r, g, b, a)
					ENDIF
				ENDIF
				
				FM_EVENTDRAW_MARKER_ABOVE_VEHICLE(NET_TO_VEH(serverBD.Targets[iVeh].niVeh), r, g, b)
			ENDIF
		ENDIF
	ENDIF
ENDPROC





/// PURPOSE:
///    Listen for the player driving the vehicle entering the drop off 
PROC CHECK_FOR_LOCAL_PLAYER_DELIVERING_VEHICLE()
	PLAYER_INDEX playerVeh = GET_PLAYER_DRIVING_CONTRABAND_VEHICLE()
	//VECTOR vLocation = GET_DROP_OFF_LOCATION()
//	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_LOCAL_PLAYER_DELIVERING_VEHICLE]- playerVeh = ",NATIVE_TO_INT( playerVeh))
//	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_LOCAL_PLAYER_DELIVERING_VEHICLE]- bDeliveryScriptTriggeredCutScene = ",g_FreemodeDeliveryData.bDeliveryScriptTriggeredCutScene)
	
	IF playerVeh = PLAYER_ID()
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_VehDelivered)
			IF g_FreemodeDeliveryData.bDeliveryScriptTriggeredCutScene
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_VehDelivered)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_LOCAL_PLAYER_DELIVERING_VEHICLE] SET biP_VehDelivered")
			ENDIF
		ENDIF
	ENDIF	
	
//	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPlayerVeh[0])
//		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niPlayerVeh[0])
//			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_VehDelivered)
//				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_LOCAL_PLAYER_DELIVERING_VEHICLE] Not delivered")
//				IF playerVeh = PLAYER_ID()
//					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_LOCAL_PLAYER_DELIVERING_VEHICLE] I'm driving")
//					//	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_LOCAL_PLAYER_DELIVERING_VEHICLE]- playerVeh != PLAYER_ID()")
//					IF NOT IS_PED_INJURED(GET_PLAYER_PED(playerVeh))
//						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_LOCAL_PLAYER_DELIVERING_VEHICLE] Not injured")
//						IF g_FreemodeDeliveryData.bDeliveryScriptTriggeredCutScene
//							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_LOCAL_PLAYER_DELIVERING_VEHICLE] Global set")
//							EMPTY_AND_LOCK_VEHICLE(NET_TO_VEH(serverBD.niPlayerVeh[0]))
//							
//							IF NETWORK_HAS_CONTROL_OF_ENTITY(NET_TO_VEH(serverBD.niPlayerVeh[0]))
//								SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(NET_TO_VEH(serverBD.niPlayerVeh[0]), TRUE)
//								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_LOCAL_PLAYER_DELIVERING_VEHICLE] I HAVE LOCKED THE VEHICLE")
//							ELSE	
//								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_LOCAL_PLAYER_DELIVERING_VEHICLE] I DON'T HAVE CONTROL!")
//							ENDIF
//							
//							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_VehDelivered)
//							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [CHECK_FOR_LOCAL_PLAYER_DELIVERING_VEHICLE] SET biP_VehDelivered")
//							
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
	
ENDPROC



/// PURPOSE:
///    The 'Action Area' is wherever the main body of the mission takes place.
PROC MAINTAIN_PLAYER_APPROACH_ACTION_AREA()

	IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
		IF !IS_BIT_SET(iBoolsBitSet2, biL2_IntroCallFinished)
		OR !IS_BIT_SET(iBoolsBitSet2, biL_DoneIntroShard)
			CPRINTLN(DEBUG_NET_MAGNATE,  "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_PLAYER_APPROACH_ACTION_AREA]- INTRO CALL/SHARD NOT FINISHED ")
			EXIT
		ENDIF
	ENDIF
	
	IF HAS_MISSION_END_CONDITION_BEEN_MET()
		EXIT
	ENDIF

	
	VECTOR vLoc
	//FLOAT tempFloat
	FLOAT fHead
//	FLOAT fDist2
	TEXT_LABEL_15 tl15Name
	
	SWITCH serverBD.iVariation
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
			INIT_GB_GUNRUNNING_DEFEND_CONTRABAND_VEH_SPAWN_COORDS(GB_GUNRUNNING_DEFEND_DEFUSAL, serverBD.iBusinessToDefend, 0, vLoc, fHead)
			//check if player is within 300m of the objective.
			 IF !IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_CloseEnoughToSendBombInfoText)
			 	IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(),FALSE),GET_GB_GUNRUNNING_DEFEND_CONTRABAND_VEHICLE_LOCATION(serverbd.iVariation,serverbd.iBusinessToDefend)) <= bombHelpTextRange * bombHelpTextRange
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_CloseEnoughToSendBombInfoText)
				ENDIF
			 ENDIF
		BREAK
		
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			INIT_GB_GUNRUNNING_DEFEND_CONTRABAND_VEH_SPAWN_COORDS(GB_GUNRUNNING_DEFEND_VALKYRIE, serverBD.iBusinessToDefend, 0, vLoc, fHead)
		BREAK
	ENDSWITCH
	
	IF !ARE_VECTORS_EQUAL(vLoc, << 0.0, 0.0, 0.0 >>)
		IF NOT IS_SERVER_BIT_SET(biS_ApproachedWarehouse)
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ApproachedWarehouse)
				IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InEnemyTriggerArea)
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ApproachedWarehouse)
					CPRINTLN(DEBUG_NET_MAGNATE,  "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_PLAYER_APPROACH_ACTION_AREA] SET biP_ApproachedWarehouse, serverBD.iBusinessToDefend = ", ENUM_TO_INT(serverBD.iBusinessToDefend), " vLoc = ", vLoc)
					vLoc = << 0.0, 0.0, 0.0 >>
					
				ENDIF
			ELSE
				vLoc = << 0.0, 0.0, 0.0 >>
			ENDIF
		ELSE
			vLoc = << 0.0, 0.0, 0.0 >>
		ENDIF
	ENDIF
		//tempFloat = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), GET_GB_GUNRUNNING_DEFEND_CONTRABAND_VEHICLE_LOCATION(serverBD.iVariation, serverBD.iBusinessToDefend))
		//IF tempFloat >= 100
			//ENDIF
			//
	IF !IS_SERVER_BIT_SET(biS_SomeoneNearActionArea)
		IF !ARE_VECTORS_EQUAL(vLoc, << 0.0, 0.0, 0.0 >>)
		AND !IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WithinObjectiveRange)
			IF NOT DOES_BLIP_EXIST(blipWarehouse) 
			
				#IF IS_DEBUG_BUILD
				vWarpCoordsForJskip = vLoc
				#ENDIF
				
				blipWarehouse = ADD_BLIP_FOR_COORD(vLoc) // (GET_DROP_OFF_LOCATION())
				SET_BLIP_ROUTE(blipWarehouse, TRUE)
				
				tl15Name = GET_NAME_FOR_ACTION_AREA_BLIP(serverBD.iVariation, serverBD.iBusinessToDefend)
				IF NOT IS_STRING_NULL_OR_EMPTY(tl15Name)
					SET_BLIP_NAME_FROM_TEXT_FILE(blipWarehouse, tl15Name)
				ENDIF
				CPRINTLN(DEBUG_NET_MAGNATE,  "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_PLAYER_APPROACH_ACTION_AREA] ADDED blipWarehouse at loc ", vLoc)
			ENDIF
		ENDIF
	ELSE
		IF !IS_SERVER_BIT_SET(biS_InVanAtLeastOnce)
		AND !IS_BIT_SET(serverBD.packagesServer.iServerPackageBitset, SERVER_GB_PACKAGE_ALLOW_COLLECT)
			IF DOES_BLIP_EXIST(blipWarehouse)
				REMOVE_BLIP(blipWarehouse)
				CPRINTLN(DEBUG_NET_MAGNATE,  "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_PLAYER_APPROACH_ACTION_AREA] REMOVED blipWarehouse")
			ENDIF
		ENDIF
		
		IF DOES_BLIP_EXIST(blipWarehouse)
				REMOVE_BLIP(blipWarehouse)
				CPRINTLN(DEBUG_NET_MAGNATE,  "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_PLAYER_APPROACH_ACTION_AREA] REMOVED blipWarehouse")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL CAN_LOCK_ONTO_CONTRABAND_VEHICLE_WITH_HOMING_MISSILE()
	SWITCH serverBD.iVariation
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			RETURN TRUE
		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Client-side processing of who is driving the contraband vehicle 
PROC MAINTAIN_GB_GUNRUNNING_DEFEND_CONTRABAND_VEHICLE()

	
	VEHICLE_INDEX veh
	VECTOR vLoc
	FLOAT fHead
//	IF serverBD.iVariation != GB_GUNRUNNING_DEFEND_VALKYRIE
//		EXIT
//	ENDIF

	
	//-- Set can't be locked-on to
	IF !CAN_LOCK_ONTO_CONTRABAND_VEHICLE_WITH_HOMING_MISSILE()
		IF NOT IS_BIT_SET(iBoolsBitSet, biL_DisabledVehLockon)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPlayerVeh[0])
				IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niPlayerVeh[0])
					SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON(NET_TO_VEH(serverBD.niPlayerVeh[0]), FALSE)
					SET_BIT(iBoolsBitSet, biL_DisabledVehLockon)
					CPRINTLN(DEBUG_NET_MAGNATE,  "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_CONTRABAND_VEHICLE] SET biL_DisabledVehLockon")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//-- Not drivable?
	//IF serverBD.iVariation != GB_GUNRUNNING_DEFEND_VALKYRIE
	IF serverBD.iVariation = GB_GUNRUNNING_DEFEND_DEFUSAL
	//OR serverBD.iVariation = GB_GUNRUNNING_DEFEND_VALKYRIE
		IF NOT IS_SERVER_BIT_SET(biS_ContrabandWrecked)
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_VanWrecked)
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPlayerVeh[0])
					IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.niPlayerVeh[0])
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_VanWrecked)
						CPRINTLN(DEBUG_NET_MAGNATE,  "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_CONTRABAND_VEHICLE] SET biP_VanWrecked")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	//--Not drivable? Valkyrie version
	IF serverBD.iVariation = GB_GUNRUNNING_DEFEND_VALKYRIE
		IF NOT IS_SERVER_BIT_SET(biS_ValkyrieDestroyed)
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ValkyrieWrecked)
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPlayerVeh[0])
					IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.niPlayerVeh[0])
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ValkyrieWrecked)
						CPRINTLN(DEBUG_NET_MAGNATE,  "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_CONTRABAND_VEHICLE] SET biP_ValkyrieWrecked")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//-- Approached vehicle?
	IF NOT IS_SERVER_BIT_SET(biS_VehSetOff)
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ApproachedVan)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPlayerVeh[0])
				vLoc = GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niPlayerVeh[0]), FALSE)
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vLoc) < bombHelpTextRange * bombHelpTextRange
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ApproachedVan)
					CPRINTLN(DEBUG_NET_MAGNATE,  "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_CONTRABAND_VEHICLE] SET biP_ApproachedVan")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//-- Can see vehicle?
//	IF iLocalPartDrivingContraVehicle = -2
//		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPlayerVeh[0])
//			IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niPlayerVeh[0])
//				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_CanSeeVan)
//					IF CAN_I_SEE_CONTRABAND_VEHICLE(NET_TO_VEH(serverBD.niPlayerVeh[0]))
//						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_CanSeeVan)
//						CPRINTLN(DEBUG_NET_MAGNATE,  "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_CONTRABAND_VEHICLE] SET biP_CanSeeVan")
//					ENDIF
//				ELSE
//					IF NOT CAN_I_SEE_CONTRABAND_VEHICLE(NET_TO_VEH(serverBD.niPlayerVeh[0]))
//						CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_CanSeeVan)
//						CPRINTLN(DEBUG_NET_MAGNATE,  "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_CONTRABAND_VEHICLE] CLEAR biP_CanSeeVan")
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF				
//	ENDIF
	
	//-- Am i I driving the vehicle? Need to let server know
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_VehDelivered)
	AND NOT IS_SERVER_BIT_SET(biS_VehDelivered)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPlayerVeh[0])
			IF !g_FreemodeDeliveryData.bDeliveryScriptTriggeredCutScene
				veh = NET_TO_VEH(serverBD.niPlayerVeh[0])
				IF IS_VEHICLE_DRIVEABLE(veh)
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) = veh
								IF GET_PED_IN_VEHICLE_SEAT(veh) = PLAYER_PED_ID()
									IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InVeh)
										SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InVeh)
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_CONTRABAND_VEHICLE] SET biP_InVeh")
									ENDIF
								ELSE
									IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InVeh)
										CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InVeh)
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_CONTRABAND_VEHICLE] CLEAR biP_InVeh as GET_PED_IN_VEHICLE_SEAT")
									ENDIF
								ENDIF
							ELSE
								IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InVeh)
									CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InVeh)
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_CONTRABAND_VEHICLE] CLEAR biP_InVeh as GET_VEHICLE_PED_IS_IN")
								ENDIF
							ENDIF
						ELSE
							IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InVeh)
								CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InVeh)
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_CONTRABAND_VEHICLE] CLEAR biP_InVeh as IS_PED_SITTING_IN_ANY_VEHICLE")
							ENDIF
						ENDIF
					ELSE
						IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InVeh)
							CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InVeh)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_CONTRABAND_VEHICLE] CLEAR biP_InVeh as IS_PED_INJURED")
						ENDIF
					ENDIF
				ELSE
					IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InVeh)
						CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InVeh)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_CONTRABAND_VEHICLE] CLEAR biP_InVeh as not driveable")
					ENDIF
				ENDIF
			ENDIF
			
			VECTOR vVehLoc
			FLOAT fDist2
			//-- Track if the the contraband vehicle has been moved far from its starting position
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ContraVehClearOfAction)
				IF NOT IS_SERVER_BIT_SET(biS_ContraVehClearOfAction)
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPlayerVeh[0])
						IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niPlayerVeh[0])
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								vVehLoc = GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niPlayerVeh[0]))
								INIT_GB_GUNRUNNING_DEFEND_CONTRABAND_VEH_SPAWN_COORDS(serverBD.iVariation, serverBD.iBusinessToDefend, 0, vLoc, fHead)
								fDist2 = VDIST2(vLoc, vVehLoc)
								IF fDist2 >= 200.0 * 200.0
									SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ContraVehClearOfAction)
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_CONTRABAND_VEHICLE] Set biP_ContraVehClearOfAction fDist2 = ", fDist2, " start loc = ", vLoc, " current loc = ", vVehLoc)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	PLAYER_INDEX playerDriving = INVALID_PLAYER_INDEX()
	PLAYER_INDEX playerBoss = INVALID_PLAYER_INDEX()
	IF iLocalPartDrivingContraVehicle != serverBD.iPartDrivingContraVehicle
		PRINTLN("JS-SSS1")
		IF !HAS_MISSION_END_CONDITION_BEEN_MET()
			PRINTLN("JS-SSS2")
		//	IF NOT ARE_ANY_ENEMY_PEDS_STILL_IN_CONTRABAND_VEHICLE()
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPlayerVeh[0])
			PRINTLN("JS-SSS3")
				IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niPlayerVeh[0])
					PRINTLN("JS-SSS4")
					IF IS_SERVER_BIT_SET(biS_ApproachedWarehouse)
					OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ApproachedWarehouse)
					OR IS_SERVER_BIT_SET(biS_EnemiesAlerted)
						PRINTLN("JS-SSS5")
						IF DOES_BLIP_EXIST(blipVeh[0])
						PRINTLN("JS-SSS6")
							REMOVE_BLIP(blipVeh[0])
						ENDIF
							
						IF serverBD.iPartDrivingContraVehicle = PARTICIPANT_ID_TO_INT()
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_CONTRABAND_VEHICLE] ADDING DROPOFF BLIP - I'M DRIVING")

							IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
								ADD_BLIP_FOR_DROP_OFF()
							ELSE
								IF DOES_BLIP_EXIST(blipDropOff)
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_CONTRABAND_VEHICLE] REMOVING DROPOFF BLIP AS I'VE GOT A WANTED RATING")
									REMOVE_BLIP(blipDropOff)
								ENDIF
							ENDIF
							
						ELIF serverBD.iPartDrivingContraVehicle > -1
							
							playerDriving = GET_PLAYER_DRIVING_CONTRABAND_VEHICLE()
							IF playerDriving != INVALID_PLAYER_INDEX()
								IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
									playerBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
									IF playerBoss != INVALID_PLAYER_INDEX()
										IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerDriving, playerBoss)
											CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_CONTRABAND_VEHICLE] ADDING CUSTOM PLAYER BLIP - THIS PLAYER IS DRIVING ", GET_PLAYER_NAME(playerDriving))
											SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(playerDriving, GET_GUNRUN_ENTITY_BLIP_SPRITE(FALSE), TRUE)
											SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(playerDriving, BLIP_COLOUR_BLUE, TRUE)
											SET_PLAYER_BLIP_AS_LONG_RANGE(playerDriving, TRUE)
											FORCE_BLIP_PLAYER(playerDriving, TRUE, TRUE)
											SET_FIXED_BLIP_SCALE_FOR_PLAYER(playerDriving,0.8, TRUE)
											ADD_BLIP_FOR_DROP_OFF()
											CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_CONTRABAND_VEHICLE] ADDING DROPOFF BLIP - THIS PLAYER IS DRIVING ", GET_PLAYER_NAME(playerDriving))
										ENDIF
									ENDIF
								ENDIF
								
//								IF serverBD.iVariation = GB_GUNRUNNING_DEFEND_METH
//									PRINT_TICKER_WITH_PLAYER_NAME("DBR_TCK_MTH", playerDriving) // ~a~ took the contraband vehicle.
//								ELIF serverBD.iVariation = GB_GUNRUNNING_DEFEND_GLOBAL
//									PRINT_TICKER_WITH_PLAYER_NAME("DBR_TCK_GBL", playerDriving)
//								ENDIF
							ENDIF
						ELIF serverBD.iPartDrivingContraVehicle = -1
							IF DOES_BLIP_EXIST(blipDropOff)
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_CONTRABAND_VEHICLE] REMOVING DROPOFF BLIP AS serverBD.iPartDrivingContraVehicle = -1")
								REMOVE_BLIP(blipDropOff)
							ENDIF
							
							playerDriving = GET_LAST_PLAYER_DRIVING_CONTRABAND_VEHICLE()
							IF playerDriving != INVALID_PLAYER_INDEX()
								SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(playerDriving, GET_GUNRUN_ENTITY_BLIP_SPRITE(FALSE), FALSE)
								SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(playerDriving, BLIP_COLOUR_BLUE, FALSE)
								SET_PLAYER_BLIP_AS_LONG_RANGE(playerDriving, FALSE)
								FORCE_BLIP_PLAYER(playerDriving, FALSE, FALSE)
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_CONTRABAND_VEHICLE] CLEARING CUSTOM BLIP FOR PLAYER ", GET_PLAYER_NAME(playerDriving), " AS serverBD.iPartDrivingContraVehicle = -1")
							ENDIF
							
							IF serverBD.iVariation = GB_GUNRUNNING_DEFEND_VALKYRIE
								ADD_BLIP_FOR_CONTRABAND_VEHICLE(0, BLIP_COLOUR_BLUE, FALSE)
							ENDIF
							IF serverBD.iVariation = GB_GUNRUNNING_DEFEND_DEFUSAL
								IF IS_SERVER_BIT_SET(biS_AllBombsDefused)
									ADD_BLIP_FOR_CONTRABAND_VEHICLE(0, BLIP_COLOUR_BlUE, FALSE)
								ENDIF
								
							ENDIF
						ENDIF
						
						iLocalPartDrivingContraVehicle = serverBD.iPartDrivingContraVehicle
						
						
						#IF IS_DEBUG_BUILD
							PLAYER_INDEX playerTemp
							PLAYER_INDEX playerHost = NETWORK_GET_PLAYER_INDEX(NETWORK_GET_HOST_OF_THIS_SCRIPT())
							IF iLocalPartDrivingContraVehicle > -1
								playerTemp = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iLocalPartDrivingContraVehicle))
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_CONTRABAND_VEHICLE] iLocalPartDrivingContraVehicle now ", iLocalPartDrivingContraVehicle, " who is player ", GET_PLAYER_NAME(playerTemp), " HOST: ", GET_PLAYER_NAME(playerHost))
							ELSE
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_CONTRABAND_VEHICLE] iLocalPartDrivingContraVehicle now ", iLocalPartDrivingContraVehicle, " HOST: ", GET_PLAYER_NAME(playerHost))
							ENDIF
						#ENDIF
					ENDIF
				ELSE
//					IF DOES_PART_HAVE_ANY_GB_PACKAGE(PARTICIPANT_ID_TO_INT())
//						IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
//							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_CONTRABAND_VEHICLE] PACKAGE PICKED UP ")
//							ADD_BLIP_FOR_DROP_OFF()
//						ENDIF
//					ENDIF
					playerDriving = GET_LAST_PLAYER_DRIVING_CONTRABAND_VEHICLE()
					IF playerDriving != INVALID_PLAYER_INDEX()
						SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(playerDriving, GET_GUNRUN_ENTITY_BLIP_SPRITE(FALSE), FALSE)
						SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(playerDriving, BLIP_COLOUR_BLUE, FALSE)
						SET_PLAYER_BLIP_AS_LONG_RANGE(playerDriving, FALSE)
						FORCE_BLIP_PLAYER(playerDriving, FALSE, FALSE)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_CONTRABAND_VEHICLE] CLEARING CUSTOM BLIP FOR PLAYER ", GET_PLAYER_NAME(playerDriving), " AS Veh not driveable")
					ENDIF
					iLocalPartDrivingContraVehicle = serverBD.iPartDrivingContraVehicle
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPlayerVeh[0])
		IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.niPlayerVeh[0])
			IF DOES_BLIP_EXIST(blipVeh[0])
				REMOVE_BLIP(blipVeh[0])
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_GB_GUNRUNNING_DEFEND_CONTRABAND_VEHICLE] removed blip as veh not driveable")
			ENDIF
		ENDIF
	ENDIF
	
	//MAINTAIN_CONTRABAND_VEHICLE_BLIP_ALPHA_CLIENT()
	
	MAINTAIN_MARKER_FOR_CONTRABAND_VEHICLE(0)
	
	CHECK_FOR_LOCAL_PLAYER_DELIVERING_VEHICLE()
	
ENDPROC
 

/// PURPOSE:
///    Draws a marker above the package when it has been dropped
PROC MAINTAIN_PACKAGE_MARKER()

ENDPROC



PROC MAINTAIN_PLAYER_COLLECT_PACKAGES()
	
	
ENDPROC

PROC MAINTAIN_TICKERS()

ENDPROC

PROC MAINTAIN_WAREHOUSE_ALARM()
	EXIT
//	IF NOT IS_BIT_SET(iBoolsBitSet, biL_StartedAlarm)
//		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//			VECTOR vCoords = GET_WAREHOUSE_BLIP_COORDS(GET_SIMPLE_INTERIOR_ID_FROM_WAREHOUSE_ID(serverBD.iWarehouseToUse + 1))
//			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vCoords) <= 200
//				iAlarmSoundID = GET_SOUND_ID()
//				PLAY_SOUND_FROM_COORD(iAlarmSoundID,"Bell_02",vCoords,"ALARMS_SOUNDSET",FALSE)
//				SET_BIT(iBoolsBitSet, biL_StartedAlarm)
//				
//				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_WAREHOUSE_ALARM] Started alarm at coords ", vCoords)
//			ENDIF
//		ENDIF
//	ENDIF
ENDPROC





FUNC BOOL ROTATION_IS_TOO_FAR(INT iExportEntity)
	FLOAT fRot = GET_ENTITY_PITCH(NET_TO_OBJ(serverBD.packagesServer.niPackage[iExportEntity]))
	
	IF fRot > 10
	OR fRot < -10
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - ROTATION_IS_TOO_FAR - PITCH")
		RETURN TRUE
	ENDIF
	
	fRot = GET_ENTITY_ROLL(NET_TO_OBJ(serverBD.packagesServer.niPackage[iExportEntity]))
	IF fRot > 10
	OR fRot < -10
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - ROTATION_IS_TOO_FAR - ROLL")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Plays an anim when the package lands
FUNC BOOL SHOULD_LAND_ANIM_START(INT iExportEntity)
      IF HAS_NET_TIMER_EXPIRED(serverBD.packagesServer.InAirCheckDelayTimer[iExportEntity], 3000)
        IF NOT IS_ENTITY_IN_AIR(NET_TO_OBJ(serverBD.packagesServer.niPackage[iExportEntity]))
            PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.packagesServer.niPackage[iExportEntity]), FALSE)
            CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - CONTROL_CRATE_LANDING - SHOULD_LAND_ANIM_START - TRUE - NOT IS_ENTITY_IN_AIR iExportEntity = ",iExportEntity)
            RETURN TRUE
        ENDIF
		

      ENDIF
      IF IS_ENTITY_IN_WATER(NET_TO_OBJ(serverBD.packagesServer.niPackage[iExportEntity]))
           PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.packagesServer.niPackage[iExportEntity]), FALSE)
           CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - CONTROL_CRATE_LANDING - SHOULD_LAND_ANIM_START - TRUE - IS_ENTITY_IN_WATER iExportEntity = ",iExportEntity)
           RETURN TRUE
      ENDIF
      IF ROTATION_IS_TOO_FAR(iExportEntity)
            PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.packagesServer.niPackage[iExportEntity]), FALSE)
            CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - CONTROL_CRATE_LANDING - SHOULD_LAND_ANIM_START - TRUE - ROTATION_IS_TOO_FAR iExportEntity = ",iExportEntity)
            RETURN TRUE
      ENDIF
      
      RETURN FALSE
ENDFUNC

////PURPOSE: Plays an anim when the package lands - ALSO DOES DELETION AND TIMEOUT CHECKS
PROC CONTROL_CRATE_LANDING(INT iExportEntity)

	IF NOT HAS_ANIM_DICT_LOADED("P_cargo_chute_S")
		PRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - CONTROL_CRATE_LANDING Exit as anim dict not loaded")
		EXIT
	ENDIF
	 
      VECTOR vCratePos
      FLOAT vCrateGroundZ
      
      //CRATE CHECKS
      IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.packagesServer.niPackage[iExportEntity])
      AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.packagesServer.niPackage[iExportEntity])      
	  		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.packagesServer.niChute[iExportEntity])
			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.packagesServer.niChute[iExportEntity])  
		  		 //CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - CONTROL_CRATE_LANDING - Package exists - ExportEntity: ",iExportEntity)
										
	            IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.packagesServer.niPackage[iExportEntity])
	            OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.packagesServer.niPackage[iExportEntity]) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
					//Extra check to fix assert bug 3510436
					 IF TAKE_CONTROL_OF_NET_ID(serverBD.packagesServer.niPackage[iExportEntity])
	                 AND TAKE_CONTROL_OF_NET_ID(serverBD.packagesServer.niChute[iExportEntity])
					 	//CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - CONTROL_CRATE_LANDING - Own package - ExportEntity: ",iExportEntity)
						APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(NET_TO_ENT(serverBD.packagesServer.niPackage[iExportEntity]), APPLY_TYPE_FORCE, <<0,0,10>>, 0, FALSE, TRUE)
	                  
	                  //CHUTE CHECKS
	                  IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.packagesServer.niChute[iExportEntity])
					  AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.packagesServer.niChute[iExportEntity])  
					  		// CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - CONTROL_CRATE_LANDING - Chute exists - ExportEntity: ",iExportEntity)
	                        
	                        IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.packagesServer.niChute[iExportEntity])
	                        OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.packagesServer.niChute[iExportEntity]) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
	                               //CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - CONTROL_CRATE_LANDING - Own chute - ExportEntity: ",iExportEntity)
								  
	                              IF TAKE_CONTROL_OF_NET_ID(serverBD.packagesServer.niPackage[iExportEntity])
	                              AND TAKE_CONTROL_OF_NET_ID(serverBD.packagesServer.niChute[iExportEntity])
	                                     //CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - CONTROL_CRATE_LANDING - Taken control - ExportEntity: ",iExportEntity)
										

										
	                                    
	                                    //Delete Chute when Crumple is done
	                                    IF NOT IS_BIT_SET(serverBD.iCrateBS[iExportEntity], CRATE_BS_ANIM_DONE)
	                                    AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iCrateBS[iExportEntity], CRATE_BS_ANIM_DONE)
	                                          IF IS_BIT_SET(serverBD.iCrateBS[iExportEntity], CRATE_BS_ANIM_PLAYING)
	                                          OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iCrateBS[iExportEntity], CRATE_BS_ANIM_PLAYING)
	                                                //IF HAS_ENTITY_ANIM_FINISHED(NET_TO_OBJ(serverBD.packagesServer.niChute[iExportEntity]), "P_cargo_chute_S", "P_cargo_chute_S_crumple")
	                                                IF NOT IS_ENTITY_PLAYING_ANIM(NET_TO_OBJ(serverBD.packagesServer.niChute[iExportEntity]), "P_cargo_chute_S", "P_cargo_chute_S_crumple")
	                                                      SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iCrateBS[iExportEntity], CRATE_BS_ANIM_DONE)
	                                                      //SET_BIT(serverBD.iCrateBS[iExportEntity], CRATE_BS_ANIM_DONE)
	                                                      DELETE_NET_ID(serverBD.packagesServer.niChute[iExportEntity])
	                                                      PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.packagesServer.niPackage[iExportEntity]), FALSE)
	                                                      CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - CONTROL_CRATE_LANDING - PREVENT_COLLECTION_OF_PORTABLE_PICKUP - FALSE - B - ExportEntity: ",iExportEntity)
	                                                      CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - CONTROL_CRATE_LANDING - LAND ANIM DONE - CHUTE DELETED - ExportEntity: ",iExportEntity) 
	                                                ENDIF
	                                          ENDIF
	                                    ENDIF
	                                    //Check Crumple is playing
	                                    IF NOT IS_BIT_SET(serverBD.iCrateBS[iExportEntity], CRATE_BS_ANIM_PLAYING)
	                                    AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iCrateBS[iExportEntity], CRATE_BS_ANIM_PLAYING)
	                                          IF IS_BIT_SET(serverBD.iCrateBS[iExportEntity], CRATE_BS_ANIM_STARTED)
	                                          OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iCrateBS[iExportEntity], CRATE_BS_ANIM_STARTED)
	                                                IF IS_ENTITY_PLAYING_ANIM(NET_TO_OBJ(serverBD.packagesServer.niChute[iExportEntity]), "P_cargo_chute_S", "P_cargo_chute_S_crumple")
	                                                      SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iCrateBS[iExportEntity], CRATE_BS_ANIM_PLAYING)
	                                                      //SET_BIT(serverBD.iCrateBS[iExportEntity], CRATE_BS_ANIM_STARTED)
	                                                      CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - CONTROL_CRATE_LANDING - LAND ANIM - P_cargo_chute_S_crumple - PLAYING - ExportEntity: ",iExportEntity)
	                                                ENDIF
	                                          ENDIF
	                                    ENDIF
	                                    //Start Crumple
	                                    IF NOT IS_BIT_SET(serverBD.iCrateBS[iExportEntity], CRATE_BS_ANIM_STARTED)
	                                    AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iCrateBS[iExportEntity], CRATE_BS_ANIM_STARTED)
											  CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - CONTROL_CRATE_LANDING - Waiting to start land anim - ExportEntity: ",iExportEntity)
	                                          IF SHOULD_LAND_ANIM_START(iExportEntity)
	                                                PLAY_ENTITY_ANIM(NET_TO_OBJ(serverBD.packagesServer.niChute[iExportEntity]), "P_cargo_chute_S_crumple", "P_cargo_chute_S", INSTANT_BLEND_IN, FALSE, FALSE) //TRUE)
													PLAY_SOUND_FROM_ENTITY(-1,"Parachute_Land",NET_TO_OBJ(serverBD.packagesServer.niChute[iExportEntity]),"DLC_Exec_Air_Drop_Sounds",TRUE)
	                                                SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iCrateBS[iExportEntity], CRATE_BS_ANIM_STARTED)
	                                                //SET_BIT(serverBD.iCrateBS[iExportEntity], CRATE_BS_ANIM_STARTED)
	                                                CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - CONTROL_CRATE_LANDING - LAND ANIM STARTED - P_cargo_chute_S_crumple - STARTED - ExportEntity: ",iExportEntity)
	                                                CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - CONTROL_CRATE_LANDING - NETID = ") NET_PRINT_INT(NATIVE_TO_INT(serverBD.packagesServer.niChute[iExportEntity]))
	                                          ELSE
	                                                SET_DAMPING(NET_TO_OBJ(serverBD.packagesServer.niPackage[iExportEntity]), PHYSICS_DAMPING_LINEAR_V2, 0.0245)
	                                          ENDIF
	                                    ENDIF
	                              ENDIF
	                        ENDIF
	                  ENDIF
	                  
	                  //Check Crate is above ground
	                  vCratePos = GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.packagesServer.niPackage[iExportEntity]))
	                  IF GET_GROUND_Z_FOR_3D_COORD(vCratePos, vCrateGroundZ) 
	                        IF vCratePos.z < vCrateGroundZ
	                        AND NOT IS_ENTITY_IN_WATER(NET_TO_OBJ(serverBD.packagesServer.niPackage[iExportEntity]))
	                              IF TAKE_CONTROL_OF_NET_ID(serverBD.packagesServer.niPackage[iExportEntity])
	                                    SET_ENTITY_COORDS(NET_TO_OBJ(serverBD.packagesServer.niPackage[iExportEntity]), <<vCratePos.x, vCratePos.y, vCrateGroundZ>>)
	                                    CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - CONTROL_CRATE_LANDING - NORMAL CRATE BELOW GROUND - WARP BACK UP - ExportEntity: ",iExportEntity) 
	                              ENDIF
	                        ENDIF
	                  ENDIF
	                  
					ENDIF
	           ENDIF 
			ENDIF
      ENDIF
ENDPROC

/// PURPOSE:
///    Client-side process of who is carrying / has delivered each package.
PROC MAINTAIN_GB_DEFEND_PACKAGES_CLIENT()


	INT iR, iG, iB, iA
	INT i
	INT iPlayerDel
	PLAYER_INDEX playerPackage
	TEXT_LABEL_15 tl15Name	
			
//	IF NOT IS_BIT_SET(iBoolsBitSet2, biL2_IntroCallFinished)
//		#IF IS_DEBUG_BUILD
//		IF GET_FRAME_COUNT() % 100 = 0
//			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_GB_DEFEND_PACKAGES_CLIENT] Waiting for intro call")
//		ENDIF
//		#ENDIF
//		EXIT
//	ENDIF
	
	SWITCH serverBD.iVariation
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			IF NOT HAS_MISSION_END_CONDITION_BEEN_MET()
				tl15Name = GET_NAME_FOR_DEFEND_PICKUP_BLIP(serverBD.iVariation)
				SET_GB_PACKAGE_BLIP_NAME(localPackages, tl15Name)
				tl15Name = GET_NAME_FOR_DEFEND_PACKAGE_HUD_TITLE(serverBD.iVariation)
				SET_GB_PACKAGE_BLIP_HUD_TITLE(localPackages, tl15Name)
							
				//add parameter to check for delivery							
				MAINTAIN_LOCAL_PLAYER_WITH_GB_PACKAGES(localPackages, serverBD.packagesServer, FALSE,TRUE)
				//check if player has delivered deliverable
				// if it's 
				MAINTAIN_GB_PACKAGE_BLIPS(localPackages, serverBD.packagesServer)
				
				
				//-- Marker over dropped packages
				IF IS_BIT_SET(serverBD.packagesServer.iServerPackageBitset, SERVER_GB_PACKAGE_ALLOW_COLLECT)
					REPEAT MAX_GANG_BOSS_GB_PACKAGES i
						IF NOT DOES_PART_HAVE_ANY_GB_PACKAGE(PARTICIPANT_ID_TO_INT())
						
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GET_GB_PACKAGE_NI(serverBD.packagesServer, i))
								IF NOT IS_ANYONE_HOLDING_GB_PACKAGE(serverBD.packagesServer, i, playerPackage)
								AND NOT HAS_GB_PACKAGE_BEEN_DELIVERED(serverBD.packagesServer, i, iPlayerDel)
								AND NOT HAS_PART_DELIVERED_GB_PACKAGE(PARTICIPANT_ID_TO_INT(), i)
								AND NOT DOES_PART_HAVE_GB_PACKAGE(PARTICIPANT_ID_TO_INT(), i)
								
								
									IF IS_GB_PACKAGE_ATTACHED_TO_ANY_PED(serverBD.packagesServer, i)
										GET_HUD_COLOUR(HUD_COLOUR_RED, iR, iG, iB, iA)
									ELSE
										GET_HUD_COLOUR(HUD_COLOUR_GREEN, iR, iG, iB, iA)
									ENDIF
									
									IF NOT IS_GB_PACKAGE_ATTACHED_TO_ANY_PED(serverBD.packagesServer, i)
										DRAW_MARKER(MARKER_ARROW, GET_ENTITY_COORDS(GET_GB_PACKAGE_OBJ(serverBD.packagesServer, i))+<<0,0,2.0>>, <<0,0,0>>, <<180,0,0>>, <<0.5,0.5,0.5>>, iR, iG, iB, 100, TRUE, TRUE)
									ENDIF
								
										
									
								ENDIF
							ENDIF
						ENDIF
						
						CONTROL_CRATE_LANDING(i)
					ENDREPEAT
					
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	
ENDPROC
/// PURPOSE:
///    Sets the corresponding trigger area for the player, depending on where they are.
///    e.g.If a player is in a heli, expand the enemy trigger area.
FUNC BOOL IS_ENEMY_TRIGGER_AREA_VALID(INT iVar, INT iLocation, INT iAngled)
	
	SWITCH iVar
	CASE GB_GUNRUNNING_DEFEND_DEFUSAL
			SWITCH iLocation
				CASE 0
					SWITCH iAngled
						CASE 0
						//heli trigger area
							IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
							PRINTLN("JS-IN HELI TRIGGER AREA")
								RETURN TRUE
							ENDIF
							
						RETURN FALSE
						
						CASE 1
						 //for all peds
						 PRINTLN("JS-IN PED TRIGGER AREA")
						//RETURN TRUE
						RETURN FALSE
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH iAngled
						CASE 0
						//heli trigger area
							IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
								RETURN TRUE
							ENDIF
						BREAK
						CASE 1
						 //for all peds
								//RETURN TRUE
								RETURN FALSE
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iAngled
						CASE 0
						//heli trigger area
							IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
								RETURN TRUE
							ENDIF
						BREAK
						CASE 1
						 //for all peds
								//RETURN TRUE
								RETURN FALSE
					ENDSWITCH
				BREAK
				CASE 3
				SWITCH iAngled
						CASE 0
						//heli trigger area
							IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
								RETURN TRUE
							ENDIF
						BREAK
						CASE 1
						//for CASE 5 ped
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[1].niPed)
								IF !IS_PED_INJURED(NET_TO_PED(serverbd.Targets[1].niPed))
									//RETURN TRUE
									RETURN FALSE
								ENDIF
							ENDIF
						BREAK
						CASE 2
						 //for all peds
								//RETURN TRUE
								RETURN FALSE
				ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			SWITCH iLocation
				CASE 0
					SWITCH iAngled
						CASE 0
							IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
								RETURN TRUE
							ENDIF
						BREAK
						CASE 1
							//IF //ped 2 alive
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[1].niPed)
								IF !IS_PED_INJURED(NET_TO_PED(serverbd.Targets[1].niPed))
									//RETURN TRUE
									RETURN FALSE
								ENDIF
							ENDIF
						BREAK
						CASE 2
							 //for all peds
								//RETURN TRUE
								RETURN FALSE
							
						//BREAK
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH iAngled
						CASE 0
							IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
								RETURN TRUE
							ENDIF
						BREAK
						CASE 1
						//ped 2 alive
								//RETURN TRUE
								RETURN FALSE
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iAngled
						CASE 0
							IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
								RETURN TRUE
							ENDIF
						BREAK
						CASE 1
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[6].niPed)
								IF !IS_PED_INJURED(NET_TO_PED(serverbd.Targets[6].niPed))
									//RETURN TRUE
									RETURN FALSE
								ENDIF
							ENDIF
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[7].niPed)
								IF !IS_PED_INJURED(NET_TO_PED(serverbd.Targets[7].niPed))
									//RETURN TRUE
									RETURN FALSE
								ENDIF
							ENDIF
						BREAK
						CASE 2
							//IF //for all peds
								//RETURN TRUE
								RETURN FALSE
							//ENDIF
						
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC
/// PURPOSE:
///    Client-side process that handles a player entering an enemy trigger area.
///    e.g.If a player has spooked the enemies, set them all off.
PROC MAINTAIN_PLAYER_TRIGGER_AREAS()
	VECTOR vMin, vMax
	INT iAngled
	FLOAT fWidth
	
	//-- Spooked by player entering area?
	//IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetSpookedByAreaCheck, i)
	//if not custom bit
			
	BOOL bInArea = IS_BIT_SET(playerbd[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InEnemyTriggerArea)
	BOOL bClearFlag = TRUE
	IF !bInArea
	OR serverBD.iVariation = GB_GUNRUNNING_DEFEND_VALKYRIE
		REPEAT MAX_ANGLED_AREA_CHECKS iAngled
			IF IS_ENEMY_TRIGGER_AREA_VALID(serverBD.iVariation, serverBD.iBusinessToDefend, iAngled)
				//PRINTLN("JS- TRIGGER AREA VALID")
				GET_GB_DEFEND_ANGLED_AREA_FOR_SPOOKED_CHECK(serverBD.iVariation, serverBD.iBusinessToDefend, iAngled, vMin, vMax, fWidth)
				//PRINTLN("JS-VALID vMin: ",vMin,"vMax: ",vMax,"width: ",fWidth)
				IF NOT ARE_VECTORS_EQUAL(vMin, << 0.0, 0.0, 0.0 >>)
					//PRINTLN("JS- VECTORS NOT EQUAL")
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						//PRINTLN("JS-PLAYER NOT INJURED")
						IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), vMin, vMax, fWidth)
							//PRINTLN("JS-PLAYER IN AREA")
							IF !IS_BIT_SET(iBoolsBitSet2,biL2_InPedSpookArea)
								SET_BIT(iBoolsBitSet2,biL2_InPedSpookArea)
								PRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_CONTRA_DEFEND - MAINTAIN_PLAYER_TRIGGER_AREAS - SET_BIT(iBoolsBitSet2,biL2_InPedSpookArea)")
							ENDIF
							bClearFlag = FALSE
							IF !bInArea
								SET_BIT(playerbd[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InEnemyTriggerArea)
								PRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_CONTRA_DEFEND - [PROCESS_TARGET_PED_BODY] I spooked ped due to angled-area check vMin = ", vMin)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF

	IF bClearFlag
	AND IS_BIT_SET(iBoolsBitSet2,biL2_InPedSpookArea)
		CLEAR_BIT(iBoolsBitSet2,biL2_InPedSpookArea)
		PRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_CONTRA_DEFEND - MAINTAIN_PLAYER_TRIGGER_AREAS - CLEAR_BIT(iBoolsBitSet2,biL2_InPedSpookArea)")
	ENDIF

ENDPROC

PROC MAINTAIN_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
	INT i
	VECTOR vPlayerLoc
	VECTOR vEntityLoc
	FLOAT fQualDist2 = 10000.0
	FLOAT fDist2 = 0.0
	IF NOT GB_IS_PLAYER_A_QUALIFYING_PARTICIPANT(PLAYER_ID())
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		
				vPlayerLoc = GET_ENTITY_COORDS(PLAYER_PED_ID())
				SWITCH serverBD.iVariation
					
					
					CASE GB_GUNRUNNING_DEFEND_DEFUSAL
						//-- Near business
						
						vPlayerLoc = GET_ENTITY_COORDS(PLAYER_PED_ID())
						
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.niPlayerVeh[0])
							vEntityLoc = GET_ENTITY_COORDS(NET_TO_VEH(serverbd.niPlayerVeh[0]))
						ENDIF
						
						// Check within range of pounder
						fDist2 = VDIST2(vPlayerLoc, vEntityLoc)
						
						IF fDist2 < fQualDist2
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_SET_PLAYER_AS_QUALIFYING_PARTICIPANT] fDist2 = ", fDist2, " <= fQualDist2 = ", fQualDist2) 
							GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
						ENDIF
					BREAK
					
					CASE GB_GUNRUNNING_DEFEND_VALKYRIE
					
						IF IS_BIT_SET(serverBD.packagesServer.iServerPackageBitset, SERVER_GB_PACKAGE_ALLOW_COLLECT)
							// Check within range of package
							fDist2 = GET_DISTANCE2_TO_CLOSEST_GB_PACKAGE(serverBD.packagesServer, i)
						ELSE
							vPlayerLoc = GET_ENTITY_COORDS(PLAYER_PED_ID())
							IF DOES_ENTITY_EXIST(NET_TO_VEH(serverbd.niPlayerVeh[0]))
							AND NOT IS_ENTITY_DEAD(NET_TO_VEH(serverbd.niPlayerVeh[0]))
		
								IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.niPlayerVeh[0])
									vEntityLoc = GET_ENTITY_COORDS(NET_TO_VEH(serverbd.niPlayerVeh[0]))
								ENDIF
								// Check within range of valkyrie
								fDist2 = VDIST2(vPlayerLoc, vEntityLoc)
							ENDIF
						ENDIF
						
						IF fDist2 < fQualDist2
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_SET_PLAYER_AS_QUALIFYING_PARTICIPANT] fDist2 = ", fDist2, " <= fQualDist2 = ", fQualDist2) 
							GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
						ENDIF
					BREAK
					
				ENDSWITCH
				//GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT
			
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Players may need to be moved to an 'off mission' state if they do something that invalidates they're participation in the mission
/// PARAMS:
///    serverPackages - 
/// RETURNS:
///    
FUNC BOOL SHOULD_MOVE_OFF_MISSION(STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackages)
	BOOl bShouldMoveToIdleState
	IF SHOULD_FM_EVENT_DROP_TO_IDLE_STATE()
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [SHOULD_MOVE_OFF_MISSION] TRUE AS SHOULD_FM_EVENT_DROP_TO_IDLE_STATE")
	//	SET_LOCAL_BIT0(eLOCALBITSET0_MOVED_OFF_DUE_TO_DROP_TO_IDLE_STATE_CHECK)
		bShouldMoveToIdleState = TRUE
	ELIF AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE() 
		PRINTLN("[BIKER1]     ---------->  [RESCUE_CONTACT] [SHOULD_MOVE_OFF_MISSION] TRUE AS AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE")
		bShouldMoveToIdleState = TRUE
	ELIF GET_PLAYER_CORONA_STATUS(PLAYER_ID()) > CORONA_STATUS_IDLE
		PRINTLN("[BIKER1]     ---------->  [RESCUE_CONTACT] [SHOULD_MOVE_OFF_MISSION] TRUE AS GET_PLAYER_CORONA_STATUS")
		bShouldMoveToIdleState = TRUE
	ELIF GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
		PRINTLN("[BIKER1]     ---------->  [RESCUE_CONTACT] [SHOULD_MOVE_OFF_MISSION] TRUE AS GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE")
		bShouldMoveToIdleState = TRUE
	ELIF GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE()
		PRINTLN("[BIKER1]     ---------->  [RESCUE_CONTACT] [SHOULD_MOVE_OFF_MISSION] TRUE AS B_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE")
		bShouldMoveToIdleState = TRUE
	ENDIF
	
	INT i
	
	//-- If the player is moving off-mission, they need to drop any package they were holding
	IF bShouldMoveToIdleState
		REPEAT serverPackages.options.iNumberOfPackages i
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GET_GB_PACKAGE_NI(serverPackages, i))
				IF IS_ENTITY_ATTACHED_TO_ENTITY(GET_GB_PACKAGE_OBJ(serverPackages, i), PLAYER_PED_ID())
					CLEAR_LOCAL_PART_HAS_GB_PACKAGE(i)
					PREVENT_COLLECTION_OF_PORTABLE_PICKUP(GET_GB_PACKAGE_OBJ(serverPackages, i), TRUE, TRUE)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND PREVENT_COLLECTION_OF_PORTABLE_PICKUP(TRUE) 8")
					DETACH_PORTABLE_PICKUP_FROM_PED(GET_GB_PACKAGE_OBJ(serverPackages, i))
					SET_ENTITY_VISIBLE(GET_GB_PACKAGE_OBJ(serverPackages, i), TRUE)
					HIDE_PORTABLE_PICKUP_WHEN_DETACHED(GET_GB_PACKAGE_OBJ(serverPackages, i), FALSE)
					
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [SHOULD_MOVE_OFF_MISSION] TRUE AS SHOULD_MOVE_OFF_MISSION")

				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN bShouldMoveToIdleState
ENDFUNC

PROC MAINTAIN_PLAYER_MOVE_OFF_MISSION()
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_MoveOffMission)
		IF SHOULD_MOVE_OFF_MISSION(serverBD.packagesServer)
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_MoveOffMission)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_PLAYER_MOVE_OFF_MISSION] Set biP_MoveOffMission")
		ENDIF
	ELSE
		IF NOT SHOULD_MOVE_OFF_MISSION(serverBD.packagesServer)
			CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_MoveOffMission)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_PLAYER_MOVE_OFF_MISSION] Clear biP_MoveOffMission")
		ENDIF
	ENDIF
ENDPROC


FUNC VECTOR GET_GROUND_FLARE_COORDS()
	SWITCH serverBD.iVariation
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			SWITCH serverBD.iBusinessToDefend
				CASE 0	RETURN <<1705.3650, 3321.8279, 40.2490>>
				CASE 1	RETURN <<1371.2214, -1856.4077, 56.2579>>
				CASE 2	RETURN <<747.6510, -1838.4570, 28.3950>>
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN << 0.0, 0.0, 0.0 >>
					
ENDFUNC

FUNC BOOL LOAD_INTERACT_PARTICLE_FX()
	REQUEST_PTFX_ASSET()

	IF HAS_PTFX_ASSET_LOADED()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEANUP_FLARE()
	IF DOES_PARTICLE_FX_LOOPED_EXIST(sFlare.ptfxID1)
		STOP_PARTICLE_FX_LOOPED(sFlare.ptfxID1)
	ENDIF 
	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(sFlare.ptfxID2)
		STOP_PARTICLE_FX_LOOPED(sFlare.ptfxID2)
	ENDIF
	
	IF DOES_ENTITY_EXIST(sFlare.flareObject)
		DELETE_OBJECT(sFlare.flareObject)
	ENDIF
ENDPROC

PROC MAINTAIN_GROUND_FLARES()

	IF IS_VECTOR_ZERO(GET_GROUND_FLARE_COORDS())
		EXIT
	ENDIF

	IF LOAD_INTERACT_PARTICLE_FX()
		INT iR, iG, iB, iA
		IF NOT DOES_ENTITY_EXIST(sFlare.flareObject)
		OR NOT DOES_PARTICLE_FX_LOOPED_EXIST(sFlare.ptfxID1)
		OR NOT DOES_PARTICLE_FX_LOOPED_EXIST(sFlare.ptfxID2)
			VECTOR vFlareLocation = GET_GROUND_FLARE_COORDS()
			
			IF NOT DOES_ENTITY_EXIST(sFlare.flareObject)
				sFlare.model = PROP_FLARE_01
				sFlare.flareObject = CREATE_OBJECT_NO_OFFSET(sFlare.model, vFlareLocation, FALSE, FALSE)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_GROUND_FLARES] - Flare object created at ", vFlareLocation)
			ENDIF
			
			IF DOES_ENTITY_EXIST(sFlare.flareObject)
		
				IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(sFlare.ptfxID1)
					sFlare.ptfxID1 = START_PARTICLE_FX_LOOPED_AT_COORD("scr_gr_def_flare", vFlareLocation, <<0.0, 0.0, 0.0>>,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) 
					IF sFlare.ptfxID1 != null
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_GROUND_FLARES] - Flare fizzle effect started.")
					ENDIF
				ENDIF
				IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(sFlare.ptfxID2)
					sFlare.ptfxID2 = START_PARTICLE_FX_LOOPED_AT_COORD("scr_gr_def_package_flare", vFlareLocation, <<0.0, 0.0, 0.0>>,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)  
					IF sFlare.ptfxID2 != null
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_GROUND_FLARES] - Flare smoke effect started.")
					ENDIF
				ENDIF
				
				IF sFlare.ptfxID2 != null
					IF DOES_PARTICLE_FX_LOOPED_EXIST(sFlare.ptfxID2)
						GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(HUD_COLOUR_YELLOW), iR, iG, iB, iA)
						SET_PARTICLE_FX_LOOPED_COLOUR(sFlare.ptfxID2, TO_FLOAT(iR)/255, TO_FLOAT(iG)/255, TO_FLOAT(iB)/255, TRUE)
					ENDIF
				ENDIF

			ENDIF

		ENDIF
	ENDIF

ENDPROC

PROC CLEANUP_HACKING_SOUNDS()
	INT iLoop
	REPEAT MAX_SOUNDS iLoop
		IF sHacking[iLoop] > 0
			PRINTLN(" CLEANUP_HACKING_SOUNDS, sHacking[iLoop] != -1")
			IF NOT HAS_SOUND_FINISHED(sHacking[iLoop])
				STOP_SOUND(sHacking[iLoop])
				PRINTLN(" CLEANUP_HACKING_SOUNDS STOPPING SOUND sHacking[HackingInt]")
				sHacking[iLoop] = -1 
			ENDIF
		ENDIF
	ENDREPEAT
	IF hackGUNRUNNING_DEFEND.i_hacking_sound > 0
		PRINTLN(" CLEANUP_HACKING_SOUNDS, hackGUNRUNNING_DEFEND.i_hacking_sound != -1")
		IF NOT HAS_SOUND_FINISHED(hackGUNRUNNING_DEFEND.i_hacking_sound)
			PRINTLN("JS- CLEANUP_HACKING_SOUNDS - hackGUNRUNNING_DEFEND.i_hacking_sound = ", hackGUNRUNNING_DEFEND.i_hacking_sound)
			STOP_SOUND(hackGUNRUNNING_DEFEND.i_hacking_sound)
			RELEASE_SOUND_ID(hackGUNRUNNING_DEFEND.i_hacking_sound)
			hackGUNRUNNING_DEFEND.i_hacking_sound = -1
		ENDIF
	ENDIF
	REPEAT COUNT_OF(iNewMiniGameSoundID) iLoop
		IF iNewMiniGameSoundID[iLoop] > 0
			PRINTLN(" CLEANUP_HACKING_SOUNDS, iNewMiniGameSoundID[iLoop] != -1")
			IF NOT HAS_SOUND_FINISHED(iNewMiniGameSoundID[iLoop])
				PRINTLN("JS- CLEANUP_HACKING_SOUNDS - iNewMiniGameSoundID[iLoop] = ", iNewMiniGameSoundID[iLoop])
				STOP_SOUND(iNewMiniGameSoundID[iLoop])
				RELEASE_SOUND_ID(iNewMiniGameSoundID[iLoop])
				iNewMiniGameSoundID[iLoop] = -1
			ENDIF
		ENDIF
	ENDREPEAT
	iNewMiniGameSoundBitSet = 0
ENDPROC

/// PURPOSE:
///    Control the minigames used for the 'bomb defusal' variation
/// PARAMS:
///    iBombToDefuse - 
PROC DO_GUNRUNNING_DEFEND_HACKING(INT iBombToDefuse)
	
	BOOL bHackingComplete
	
	IF NOT IS_BIT_SET(iBoolsBitSet,biL_KilledWhileHack)
		IF iHackingProg >= 0
			IF IS_PED_INJURED(PLAYER_PED_ID())
				CLEAR_BIT(hackGUNRUNNING_DEFEND.bsHacking,BS_IS_HACKING)
				iBombToDefuse = -1
				
				SET_BIT(iBoolsBitSet,biL_KilledWhileHack)
				CPRINTLN(DEBUG_NET_MAGNATE, "     -----AAA")
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GUNRUNNING_DEFEND - [DO_GUNRUNNING_DEFEND_HACKING] SET biL_KilledWhileHack iHackingProg = ", iHackingProg, " iMyHackingMG = ", iMyHackingMG, " iBombToDefuse = ", iBombToDefuse)
				iHackingProg = -1
			ENDIF
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
				//	IF iHackingProg <= 2
						//--2679608
						DISABLE_PLAYER_MISSION_VEHICLE_CONTROLS_THIS_FRAME()
				//	ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ELSE
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			RESET_NEW_HACKING_MINIGAME_DATA(hackGUNRUNNING_DEFEND)
			CLEAR_BIT(iBoolsBitSet, biL_DefuseBomb)
			FORCE_QUIT_FAIL_HACKING_MINIGAME(hackGUNRUNNING_DEFEND)
			CLEAR_BIT(iBoolsBitSet,biL_KilledWhileHack)
			playerBD[PARTICIPANT_ID_TO_INT()].iWantToDefuseBomb = 0
			playerBD[PARTICIPANT_ID_TO_INT()].iStartedDefusingBomb = 0
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GUNRUNNING_DEFEND - [DO_GUNRUNNING_DEFEND_HACKING] CLEAR biL_KilledWhileHack iHackingProg = ", iHackingProg, " iMyHackingMG = ", iMyHackingMG, " iBombToDefuse = ", iBombToDefuse)
		ENDIF
	ENDIF
	
	SWITCH iHackingProg
		
		
		CASE 0
			REQUEST_HACKING_MINI_GAME()
			REQUEST_ADDITIONAL_TEXT("HACK", MINIGAME_TEXT_SLOT)
			
			iHackingProg++
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GUNRUNNING_DEFEND - [DO_GUNRUNNING_DEFEND_HACKING] iHackingProg = ", iHackingProg, " iMyHackingMG = ",iMyHackingMG, " iBombToDefuse = ", iBombToDefuse)
		BREAK
		
		CASE 1
			IF HAVE_HACKING_ASSETS_LOADED()
			AND HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT)
				SET_BIT(hackGUNRUNNING_DEFEND.bsHacking,BS_IS_HACKING)
				SET_BIT(hackGUNRUNNING_DEFEND.iBS2Hacking, BS2_LAUNCH_MG_IMMEDIATIALY)
				
				Clear_Any_Objective_Text_From_This_Script()
				
				iHackSpeed = 10
			//	iMyHackingMG = HACKING_BRUTEFORCE //HACKING_NEW_MG
				IF iMyHackingMG = -1
					iMyHackingMG = GET_RANDOM_INT_IN_RANGE(0,MAX_NUM_HACKING_MG)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GUNRUNNING_DEFEND - [DO_GUNRUNNING_DEFEND_HACKING]  iMyHackingMG = ", iMyHackingMG, " iBombToDefuse = ",iBombToDefuse)
				ENDIF
				
				IF iMyHackingMG = HACKING_NEW_MG
					hackGUNRUNNING_DEFEND.bTestHacking = TRUE // For new hacking minigame
				ELIF iMyHackingMG = HACKING_BRUTEFORCE
					hackGUNRUNNING_DEFEND.bTestHacking = FALSE
					iHackSpeed = 77
				ELIF iMyHackingMG = HACKING_IP_CONNECT
					iHackSpeed = 50
					hackGUNRUNNING_DEFEND.bTestHacking = FALSE
				ENDIF
				
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_ALLOW_PLAYER_DAMAGE | NSPC_CAN_BE_TARGETTED)
				
				iHackingProg++
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GUNRUNNING_DEFEND - [DO_GUNRUNNING_DEFEND_HACKING] iHackingProg = ", iHackingProg, " iBombToDefuse = ", iBombToDefuse)
			ELSE
				IF NOT HAVE_HACKING_ASSETS_LOADED()
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GUNRUNNING_DEFEND - [DO_GUNRUNNING_DEFEND_HACKING] WAITING FOR REQUEST_HACKING_MINI_GAME iBombToDefuse = ", iBombToDefuse)
				ENDIF
				IF NOT HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GUNRUNNING_DEFEND - [DO_GUNRUNNING_DEFEND_HACKING] WAITING FOR HAS_ADDITIONAL_TEXT_LOADED iBombToDefuse = ", iBombToDefuse)
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
			
			IF NOT GB_IS_GLOBAL_NON_BD_BIT0_SET(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_DPAD_DOWN_LB)
				GB_SET_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_DPAD_DOWN_LB)
			ENDIF
			
			IF iMyHackingMG = HACKING_IP_CONNECT
				RUN_HACKING_MINIGAME_WITH_PARAMETERS(hackGUNRUNNING_DEFEND, 
									 				DEFAULT,			// INT iStartLives = 5, 
													DEFAULT,			// INT iMaxLives = 5, 
													iHackSpeed,			// INT iStartingSpeed = 10, 
													DEFAULT,			// INT iSpeedDecayRate = 10, 
													DEFAULT,			// INT iBruteForceColumns = 8, 
													DEFAULT,			// INT iChanceOfRedHerrings = 10,
													DEFAULT,			// BOOL bDoTutorial = FALSE, 
													TRUE,				// BOOL bJustHackConnect = FALSE, 
													DEFAULT,			// BOOL bJustBruteForce = FALSE,  
													DEFAULT)			// BOOL bRandomPassword = TRUE, 
																		// BOOL bHackConnectBeforeBrute = TRUE, 
																		// BOOL bExitButton = FALSE, 
																		// BOOL bDoDownloadOpen = FALSE, 
																		// INT iTotalTime = 60000 
																		// #IF USE_TU_CHANGES , BOOL bMP = TRUE #ENDIF )
																	
																	
			

				IF HAS_PLAYER_BEAT_HACK_CONNECT(hackGUNRUNNING_DEFEND, TRUE)
				#IF IS_DEBUG_BUILD
				OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_DOWN)
				#ENDIF
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GUNRUNNING_DEFEND - [DO_GUNRUNNING_DEFEND_HACKING] I COMPLETED IP CONNECT MG iBombToDefuse = ", iBombToDefuse)
					bHackingComplete = TRUE
				ENDIF
			ELIF iMyHackingMG = HACKING_BRUTEFORCE
				RUN_HACKING_MINIGAME_WITH_PARAMETERS(hackGUNRUNNING_DEFEND, 
									 				DEFAULT,			// INT iStartLives = 5, 
													DEFAULT,			// INT iMaxLives = 5, 
													iHackSpeed,			// INT iStartingSpeed = 10, 
													DEFAULT,			// INT iSpeedDecayRate = 10, 
													DEFAULT,			// INT iBruteForceColumns = 8, 
													DEFAULT,			// INT iChanceOfRedHerrings = 10,
													DEFAULT,			// BOOL bDoTutorial = FALSE, 
													DEFAULT,			// BOOL bJustHackConnect = FALSE, 
													TRUE,				// BOOL bJustBruteForce = FALSE,  
													DEFAULT)			// BOOL bRandomPassword = TRUE, 
																		// BOOL bHackConnectBeforeBrute = TRUE, 
																		// BOOL bExitButton = FALSE, 
																		// BOOL bDoDownloadOpen = FALSE, 
																		// INT iTotalTime = 60000 
																		// #IF USE_TU_CHANGES , BOOL bMP = TRU
		
				IF HAS_PLAYER_BEAT_BRUTEFORCE(hackGUNRUNNING_DEFEND, TRUE)
				#IF IS_DEBUG_BUILD
				OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_DOWN)
				#ENDIF
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GUNRUNNING_DEFEND - [DO_GUNRUNNING_DEFEND_HACKING] I COMPLETED BRUTE FORCE MG iBombToDefuse = ",iBombToDefuse)
					bHackingComplete = TRUE
				ENDIF
			ELIF iMyHackingMG = HACKING_NEW_MG
				RUN_HACKING_MINIGAME_WITH_PARAMETERS(hackGUNRUNNING_DEFEND, 
									 				DEFAULT,			// INT iStartLives = 5, 
													DEFAULT,			// INT iMaxLives = 5, 
													iHackSpeed,			// INT iStartingSpeed = 10, 
													DEFAULT,			// INT iSpeedDecayRate = 10, 
													DEFAULT,			// INT iBruteForceColumns = 8, 
													DEFAULT,			// INT iChanceOfRedHerrings = 10,
													DEFAULT,			// BOOL bDoTutorial = FALSE, 
													DEFAULT,			// BOOL bJustHackConnect = FALSE, 
													DEFAULT,				// BOOL bJustBruteForce = FALSE,  
													DEFAULT)			// BOOL bRandomPassword = TRUE, 
																		// BOOL bHackConnectBeforeBrute = TRUE, 
																		// BOOL bExitButton = FALSE, 
																		// BOOL bDoDownloadOpen = FALSE, 
																		// INT iTotalTime = 60000 
																		// #IF USE_TU_CHANGES , BOOL bMP = TRU
																		
				IF HAS_PLAYER_BEAT_NEW_HACKING(hackGUNRUNNING_DEFEND, TRUE)
				#IF IS_DEBUG_BUILD
				OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_DOWN)
				#ENDIF
					bHackingComplete = TRUE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GUNRUNNING_DEFEND - [DO_GUNRUNNING_DEFEND_HACKING] I COMPLETED NEW HACKING MG iBombToDefuse = ",iBombToDefuse)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(hackGUNRUNNING_DEFEND.iBS2Hacking, BS2_QUIT_MG_IMMEDIATIALY)
				CLEAR_BIT(hackGUNRUNNING_DEFEND.bsHacking,BS_IS_HACKING)
				
				RESET_NEW_HACKING_MINIGAME_DATA(hackGUNRUNNING_DEFEND)
				
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GUNRUNNING_DEFEND - [DO_GUNRUNNING_DEFEND_HACKING] QUIT! iMyHackingMG = ",iMyHackingMG, " iBombToDefuse = ",iBombToDefuse)
				
				IF GB_IS_GLOBAL_NON_BD_BIT0_SET(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_DPAD_DOWN_LB)
					GB_CLEAR_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_DPAD_DOWN_LB)
				ENDIF
				
				iBombToDefuse = -1
				//Resetting all bits when the player quits out of the mini-game.
				CLEAR_BIT(iBoolsBitSet, biL_DefuseBomb)
				playerBD[PARTICIPANT_ID_TO_INT()].iWantToDefuseBomb = 0
				playerBD[PARTICIPANT_ID_TO_INT()].iStartedDefusingBomb = 0
				
				CLEANUP_HACKING_SOUNDS()
				iHackingProg = -1
			ELIF bHackingComplete
				CLEAR_BIT(hackGUNRUNNING_DEFEND.bsHacking,BS_IS_HACKING)
				
				RESET_NEW_HACKING_MINIGAME_DATA(hackGUNRUNNING_DEFEND)
				playerBD[PARTICIPANT_ID_TO_INT()].iNumCompletedHackingGames++
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_JustCompHack)
				
				iMyHackingMG++
				IF iMyHackingMG >= MAX_NUM_HACKING_MG
					iMyHackingMG = HACKING_IP_CONNECT
				ENDIF
				
				IF GB_IS_GLOBAL_NON_BD_BIT0_SET(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_DPAD_DOWN_LB)
					GB_CLEAR_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_DPAD_DOWN_LB)
				ENDIF
				
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GUNRUNNING_DEFEND - [DO_GUNRUNNING_DEFEND_HACKING] SUCCESS! iNumCompletedHackingGames = ", playerBD[PARTICIPANT_ID_TO_INT()].iNumCompletedHackingGames, " iMyHackingMG = ",iMyHackingMG, " iBombToDefuse = ", iBombToDefuse)
				
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iDefusedBomb, iBombToDefuse)
				iDisarmBombSound = GET_SOUND_ID()
				INT iBomb 
				iBomb = GB_GUNRUNNING_DEFEND_CONVERT_BOMB_ARRAY_TO_PROP_ARRAY(iBombToDefuse)
				IF iBomb != 0
					PLAY_SOUND_FROM_ENTITY(iDisarmBombSound, "Bomb_Disarmed" , NET_TO_ENT(serverBD.niProp[iBomb]),"DLC_GR_Disarm_Bombs_Sounds")
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GUNRUNNING_DEFEND - [DO_GUNRUNNING_DEFEND_HACKING] PLAY_SOUND_FROM_ENTITY(iDisarmBombSound, \"Bomb_Disarmed\" , NET_TO_ENT(serverBD.niProp[", iBomb, "]),\"DLC_GR_Disarm_Bombs_Sounds\")")
				ENDIF
				iBombToDefuse = -1
				CLEAR_BIT(iBoolsBitSet, biL_DefuseBomb)
				
				CLEANUP_HACKING_SOUNDS()
				iHackingProg++
			ELIF IS_SERVER_BIT_SET (biS_BombTimerExpired)
				CLEAR_BIT(hackGUNRUNNING_DEFEND.bsHacking,BS_IS_HACKING)
				
				RESET_NEW_HACKING_MINIGAME_DATA(hackGUNRUNNING_DEFEND)
				
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GUNRUNNING_DEFEND - [DO_GUNRUNNING_DEFEND_HACKING] biS_BombTimerExpired")
				
				IF GB_IS_GLOBAL_NON_BD_BIT0_SET(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_DPAD_DOWN_LB)
					GB_CLEAR_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_DPAD_DOWN_LB)
				ENDIF
				
				iBombToDefuse = -1
				//Resetting all bits when the player quits out of the mini-game.
				CLEAR_BIT(iBoolsBitSet, biL_DefuseBomb)
				playerBD[PARTICIPANT_ID_TO_INT()].iWantToDefuseBomb = 0
				playerBD[PARTICIPANT_ID_TO_INT()].iStartedDefusingBomb = 0
				
				CLEANUP_HACKING_SOUNDS()
				iHackingProg = -1
			
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC





INT iBombDefusalContext
INT iDefuseBombProg
INT iNearbyBomb
INT iBombToDefuse = -1
SCRIPT_TIMER timeNearBomb

FUNC BOOL IS_LOCAL_PLAYER_OK_TO_DEFUSE_BOMBS()
	IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE, DEFAULT, FALSE)
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_BOMB_BLIPS()
	INT iNumBombs = GET_GUNRUNNING_DEFEND_NUMBER_OF_BOMBS_FOR_BUSINESS()
	INT i
	//VECTOR vLoc
	NETWORK_INDEX bombProp
	IF IS_SERVER_BIT_SET(biS_SomeoneNearActionArea)
		REPEAT iNumBombs i
			IF NOT HAS_BOMB_BEEN_DEFUSED(i)
				IF NOT DOES_BLIP_EXIST(blipBomb[i])
					//GET_BOMB_LOCATION_DETAILS(0, i, vLoc)
					//blipBomb[i] = ADD_BLIP_FOR_COORD(vLoc)
					GET_BOMB_PROP(0,i,bombProp)
					//if the prop is not a 'disarmed bomb' prop, add a blip
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(bombProp)
						IF !IS_SERVER_PROP_BIT_SET(i,PROP_BIT_BOMB_DISARMED_PROP)
							blipBomb[i] = ADD_BLIP_FOR_ENTITY(NET_TO_ENT(bombProp))
							SET_BLIP_COLOUR(blipBomb[i], BLIP_COLOUR_GREEN)
							SET_BLIP_SCALE(blipBomb[i], 0.8)
							SET_BLIP_NAME_FROM_TEXT_FILE(blipBomb[i], "GR_BOMBBLIP")
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GUNRUNNING_DEFEND [MAINTAIN_BOMB_DEFUSAL_CLIENT] Added blip for bomb ", i)
						ENDIF
					ENDIF
					
				
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(blipBomb[i])
					REMOVE_BLIP(blipBomb[i])
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GUNRUNNING_DEFEND [MAINTAIN_BOMB_DEFUSAL_CLIENT] REMOVED blip for bomb ", i)
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

/// PURPOSE:
///    Track whether or not the local player is defusing, or can defuse, each of the bombs
PROC MAINTAIN_BOMB_DEFUSAL_CLIENT()
	
	IF serverBD.iVariation != GB_GUNRUNNING_DEFEND_DEFUSAL
		//CPRINTLN(DEBUG_NET_MAGNATE,  "     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_BOMB_DEFUSAL_CLIENT]- VARIATION IS NOT DEFUSAL ")
		EXIT
	ENDIF
	MAINTAIN_BOMB_BLIPS()
	
	INT iNumberOfBombs = GET_GUNRUNNING_DEFEND_NUMBER_OF_BOMBS_FOR_BUSINESS()
	PLAYER_INDEX playerDefuse = INVALID_PLAYER_INDEX()
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GUNRUNNING_DEFEND [MAINTAIN_BOMB_DEFUSAL_CLIENT] iDefuseBombProg =  ",iDefuseBombProg)
	
	SWITCH iDefuseBombProg
		CASE 0
		//registering if the player wants to defuse the bomb
			IF NOT IS_PLAYER_DEFUSING_A_BOMB(PLAYER_ID())
			   PRINTLN("JS-111 -1")
				IF IS_LOCAL_PLAYER_NEAR_ANY_BOMB(serverbd.iBusinessToDefend, iNearbyBomb, iNumberOfBombs)
				 PRINTLN("JS-111 0")
					IF IS_LOCAL_PLAYER_OK_TO_DEFUSE_BOMBS()
					PRINTLN("JS-111 1")
						IF NOT IS_ANYONE_DEFUSING_BOMB(iNearbyBomb)
							PRINTLN("JS-111 2")
							IF NOT HAS_BOMB_BEEN_DEFUSED(iNearbyBomb)
								PRINTLN("JS-111 3")
								IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iDefusedBomb, iNearbyBomb)
									PRINTLN("JS-111 4")
									IF !HAS_NET_TIMER_STARTED(timeNearBomb)
										PRINTLN("JS-111 5")
										START_NET_TIMER(timeNearBomb)
									ELSE
										IF HAS_NET_TIMER_EXPIRED(timeNearBomb, 500)
											PRINTLN("JS-111 6")
											
											
										//	IF !IS_BIT_SET(iBoolsBitSet2,biL2_BombInstructionHelpTextSent)
												iBombDefusalContext = NEW_CONTEXT_INTENTION
												REGISTER_CONTEXT_INTENTION(iBombDefusalContext, CP_MAXIMUM_PRIORITY, "DGUN_HDEF")
												//PRINT_HELP_NO_SOUND("DGUN_HDEF")
												PRINTLN("JS-111 SET BOMB TEXT BOOL")
												//CLEAR_BIT(iBoolsBitSet2,biL2_BombInstructionHelpTextCleared)
										 		SET_BIT(iBoolsBitSet2,biL2_BombInstructionHelpTextSent)
												RESET_NET_TIMER(timeNearBomb)
												iDefuseBombProg++
												CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GUNRUNNING_DEFEND [MAINTAIN_BOMB_DEFUSAL_CLIENT] iNearbyBomb = ", iNearbyBomb, " iDefuseBombProg = ", iDefuseBombProg)
											//ENDIF	
										ENDIF
									ENDIF
								ELSE
									IF HAS_NET_TIMER_STARTED(timeNearBomb)
										PRINTLN("JS-111 RESET BOMB TEXT BOOL 0")
										CLEAR_BIT(iBoolsBitSet2,biL2_BombInstructionHelpTextSent)
										RESET_NET_TIMER(timeNearBomb)
										//iDefuseBombProg = 0
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GUNRUNNING_DEFEND [MAINTAIN_BOMB_DEFUSAL_CLIENT] Reset timeNearBomb as iDefusedBomb")
									ENDIF
								ENDIF
							ELSE
								IF HAS_NET_TIMER_STARTED(timeNearBomb)
									PRINTLN("JS-111 RESET BOMB TEXT BOOL 1")
									CLEAR_BIT(iBoolsBitSet2,biL2_BombInstructionHelpTextSent)
									RESET_NET_TIMER(timeNearBomb)
									//iDefuseBombProg = 0
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GUNRUNNING_DEFEND [MAINTAIN_BOMB_DEFUSAL_CLIENT] Reset timeNearBomb as HAS_BOMB_BEEN_DEFUSED")
								ENDIF
							ENDIF
						ELSE
							IF HAS_NET_TIMER_STARTED(timeNearBomb)
								PRINTLN("JS-111 RESET BOMB TEXT BOOL 2")
								CLEAR_BIT(iBoolsBitSet2,biL2_BombInstructionHelpTextSent)
								RESET_NET_TIMER(timeNearBomb)
								//iDefuseBombProg = 0
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GUNRUNNING_DEFEND [MAINTAIN_BOMB_DEFUSAL_CLIENT] Reset timeNearBomb as IS_ANYONE_DEFUSING_BOMB")
							ENDIF
						ENDIF
					ELSE
						IF HAS_NET_TIMER_STARTED(timeNearBomb)
							PRINTLN("JS-111 RESET BOMB TEXT BOOL 3")
							CLEAR_BIT(iBoolsBitSet2,biL2_BombInstructionHelpTextSent)
							RESET_NET_TIMER(timeNearBomb)
						//	iDefuseBombProg = 0
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GUNRUNNING_DEFEND [MAINTAIN_BOMB_DEFUSAL_CLIENT] Reset timeNearBomb as not IS_LOCAL_PLAYER_OK_TO_DEFUSE_BOMBS")
						ENDIF
					ENDIF
				ELSE
					
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GUNRUNNING_DEFEND [MAINTAIN_BOMB_DEFUSAL_CLIENT] NEAR BOMB BITSET CLEARED")
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DGUN_HDEF")
						CLEAR_HELP()
					ENDIF
					
					IF HAS_NET_TIMER_STARTED(timeNearBomb)
						PRINTLN("JS-111 RESET BOMB TEXT BOOL 4")
						CLEAR_BIT(iBoolsBitSet2,biL2_BombInstructionHelpTextSent)
						RESET_NET_TIMER(timeNearBomb)
						//iDefuseBombProg = 0
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GUNRUNNING_DEFEND [MAINTAIN_BOMB_DEFUSAL_CLIENT] Reset timeNearBomb as not IS_LOCAL_PLAYER_NEAR_ANY_BOMB")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
		//checks if the player actually clicks the button
			IF IS_LOCAL_PLAYER_NEAR_ANY_BOMB(serverbd.iBusinessToDefend, iNearbyBomb, iNumberOfBombs)
			 	PRINTLN("JS-111 7")
				IF NOT IS_ANYONE_DEFUSING_BOMB(iNearbyBomb)
						PRINTLN("JS-111 8")
					IF iBombDefusalContext != (-1)
							PRINTLN("JS-111 9")
						//IF IS_CONTEXT_INTENTION_HELP_DISPLAYING(iBombDefusalContext)
						IF HAS_CONTEXT_BUTTON_TRIGGERED(iBombDefusalContext)
								PRINTLN("JS-111 10")
							//-- Player has to be near a bomb, and press the "context" button (dpad right)
							//-- to defuse a bomb, as long as no one else is defusing
							RELEASE_CONTEXT_INTENTION(iBombDefusalContext)
							iDefuseBombProg++
							
							//-- Need to ask the server if I can defuse the bomb. Can only have 1 player defusing each bomb
							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iWantToDefuseBomb, iNearbyBomb)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GUNRUNNING_DEFEND [MAINTAIN_BOMB_DEFUSAL_CLIENT] - Context button triggered iNearbyBomb = ",iNearbyBomb, " iDefuseBombProg = ",iDefuseBombProg)
						ENDIF
					ENDIF
				ELSE
					iDefuseBombProg = 0
					iNearbyBomb = -1
					//playerDefuse = GET_PLAYER_DEFUSING_BOMB(iNearbyBomb)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GUNRUNNING_DEFEND [MAINTAIN_BOMB_DEFUSAL_CLIENT] - Resetting, bomb = ", "iDefuseBombProg = ",iDefuseBombProg)
					RELEASE_CONTEXT_INTENTION(iBombDefusalContext)
				ENDIF
			ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DGUN_HDEF")
						CLEAR_HELP()
					ENDIF
				iDefuseBombProg = 0
				iNearbyBomb = -1
				CLEAR_BIT(iBoolsBitSet2,biL2_BombInstructionHelpTextSent)
				RELEASE_CONTEXT_INTENTION(iBombDefusalContext)
			ENDIF
		BREAK
		
		CASE 2
			//-- set bit to start hacking
			IF IS_ANYONE_DEFUSING_BOMB(iNearbyBomb)
				playerDefuse = GET_PLAYER_DEFUSING_BOMB(iNearbyBomb)
				IF playerDefuse = PLAYER_ID()
					//-- Server says I can defuse the bomb
					iBombToDefuse = iNearbyBomb
					iHackingProg = 0
					SET_BIT(iBoolsBitSet, biL_DefuseBomb)
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iStartedDefusingBomb,iBombToDefuse)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GUNRUNNING_DEFEND [MAINTAIN_BOMB_DEFUSAL_CLIENT] Server says I can defuse bomb ", iNearbyBomb, " will launch hacking game...")
				ELSE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GUNRUNNING_DEFEND [MAINTAIN_BOMB_DEFUSAL_CLIENT] Server says bomb ", iNearbyBomb, " is being defused by ", GET_PLAYER_NAME(playerDefuse), " wil reset...")
				ENDIF
				
				RELEASE_CONTEXT_INTENTION(iBombDefusalContext)
				CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iWantToDefuseBomb, iNearbyBomb)
				CLEAR_BIT(iBoolsBitSet2,biL2_BombInstructionHelpTextSent)
				iDefuseBombProg = 0
			ENDIF
		BREAK
	ENDSWITCH
	
	
	//starts hacking
	IF IS_BIT_SET(iBoolsBitSet, biL_DefuseBomb)
		DO_GUNRUNNING_DEFEND_HACKING(iBombToDefuse)
	ENDIF
	
	
ENDPROC

/// PURPOSE:
///    Checks to see how far the player is from certain things, and to do certain things if they are withing range.
PROC MAINTAIN_GB_GUNRUNNING_DEFEND_PLAYER_DISTANCE_CHECKS()


 SWITCH serverBD.iVariation
 	CASE GB_GUNRUNNING_DEFEND_DEFUSAL
	
		IF serverBD.iServerGameState != GAME_STATE_END
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_GB_GUNRUNNING_DEFEND_CONTRABAND_VEHICLE_LOCATION(serverBD.iVariation, serverBD.iBusinessToDefend)) <= actionAreaRange * actionAreaRange
									
				IF !IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_NearActionArea)
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_NearActionArea)
				ENDIF
				PRINTLN("JS- SET BIT, biP_NearActionArea")
			ENDIF
		ENDIF
	
	BREAK
 ENDSWITCH
ENDPROC
/// PURPOSE:
///    Produces a ticking sound from the contraband vehicle when the bomb is about to blow up
PROC MAINTAIN_GB_GUNRUNNING_DEFEND_BOMB_TICKING_SOUND(NETWORK_INDEX iVehicle)
	//pass in the vehicle

	CONST_INT iSoundIntervalThreshold 1000
	//FLOAT iSoundIntervalTimer = 0.0
	IF !IS_SERVER_BIT_SET(biS_AllBombsDefused)
	//g_sMPTunables.FGR_DEFEND_VALKYRIE_TAKEN_ON_FAILB_PERCENTAGE                         

		//when bomb has 30s to go, call first sound
		IF !IS_BIT_SET(iBoolsBitSet2 , biL2_StartBombSoundPlayed)
			IF IS_SERVER_BIT_SET(biS_BombTimerStarted)
				IF HAS_NET_TIMER_EXPIRED(bombTimer, (g_sMPTunables.IGR_DEFEND_DISARM_BOMBS_BOMB_DISARM_TIME - FLOOR(bombTimerTickingThreshold)))
					START_NET_TIMER(bombSoundIntensityTimer)
					PRINTLN("JS- MAINTAIN_GB_GUNRUNNING_DEFEND_BOMB_TICKING_SOUND - START TICKING SOUND")
					iStartBombSound = GET_SOUND_ID()
					iTickBombSound = GET_SOUND_ID()
					PLAY_SOUND_FROM_ENTITY(iStartBombSound, "Exploding_Entity_Start",NET_TO_ENT(iVehicle),"MP_MISSION_COUNTDOWN_SOUNDSET")
					PLAY_SOUND_FROM_ENTITY(iTickBombSound, "Exploding_Entity_Loop" , NET_TO_ENT(iVehicle),"MP_MISSION_COUNTDOWN_SOUNDSET")
					SET_BIT(iBoolsBitSet2 , biL2_StartBombSoundPlayed)
				ENDIF
			ENDIF
		ELSE
			IF !IS_SERVER_BIT_SET (biS_BombTimerExpired)
				IF HAS_NET_TIMER_EXPIRED(bombSoundIntensityTimer, intensityThreshold)
					tempThreshold +=1000
					PRINTLN("JS- MAINTAIN_GB_GUNRUNNING_DEFEND_BOMB_TICKING_SOUND - tempThreshold = ", tempThreshold)
					iTickIntensity = tempThreshold/bombTimerTickingThreshold
					PRINTLN("JS- MAINTAIN_GB_GUNRUNNING_DEFEND_BOMB_TICKING_SOUND - bombTimerTickingThreshold = ", bombTimerTickingThreshold)
					PRINTLN("JS- MAINTAIN_GB_GUNRUNNING_DEFEND_BOMB_TICKING_SOUND - iTickIntensity = ", iTickIntensity)
					IF iTickBombSound != -1
						SET_VARIABLE_ON_SOUND(iTickBombSound,"Ctrl", iTickIntensity)
					ENDIF
					RESET_NET_TIMER(bombSoundIntensityTimer)
				ENDIF
			ENDIF
	
			//IF IS_SERVER_BIT_SET(biS_AllBombsDefused)
			
			//ENDIF
		ENDIF
	ELSE
		IF iStartBombSound > 0
			IF !HAS_SOUND_FINISHED(iStartBombSound)
				PRINTLN("JS- MAINTAIN_GB_GUNRUNNING_DEFEND_BOMB_TICKING_SOUND - iStartBombSound = ", iStartBombSound)
				STOP_SOUND(iStartBombSound)
				RELEASE_SOUND_ID(iStartBombSound)
				iStartBombSound = -1
			ENDIF
		ENDIF
		IF iTickBombSound > 0
			IF !HAS_SOUND_FINISHED(iTickBombSound)
				PRINTLN("JS- MAINTAIN_GB_GUNRUNNING_DEFEND_BOMB_TICKING_SOUND - iTickBombSound = ", iTickBombSound)
				STOP_SOUND(iTickBombSound)
				RELEASE_SOUND_ID(iTickBombSound)
				iTickBombSound = -1
			ENDIF
		ENDIF
	ENDIF



	IF IS_SERVER_BIT_SET (biS_BombTimerExpired)
		IF !IS_BIT_SET(iBoolsBitSet2, biL2_EndBombSoundPlayed)
			IF iStartBombSound != -1
				PRINTLN("JS- MAINTAIN_GB_GUNRUNNING_DEFEND_BOMB_TICKING_SOUND - iStartBombSound = ", iStartBombSound)
				STOP_SOUND(iStartBombSound)
				RELEASE_SOUND_ID(iStartBombSound)
				iStartBombSound = -1
			ENDIF
			IF iTickBombSound != -1
				PRINTLN("JS- MAINTAIN_GB_GUNRUNNING_DEFEND_BOMB_TICKING_SOUND - iTickBombSound = ", iTickBombSound)
				STOP_SOUND(iTickBombSound)
				RELEASE_SOUND_ID(iTickBombSound)
				iTickBombSound = -1
			ENDIF
			iTickBombSound = GET_SOUND_ID()
			PLAY_SOUND_FROM_ENTITY(iTickBombSound, "Exploding_Entity_Stop" , NET_TO_ENT(iVehicle),"MP_MISSION_COUNTDOWN_SOUNDSET")
			SET_BIT(iBoolsBitSet2, biL2_EndBombSoundPlayed)
		ENDIF
	ENDIF

	//Please trigger this sound from the truck using PLAY_SOUND_FROM_ENTITY()  
	//Soundset - "MP_MISSION_COUNTDOWN_SOUNDSET"  
	//Sound Names: Countdown Start = "Exploding_Entity_Start" - to play when the countdown timer hits 30s  
	//Countdown Loop = "Exploding_Entity_Loop" - this needs to be started when the timer reaches 30 seconds 
	//remaining and stopped when the timer stops. 
	//Please pass in the variable "Ctrl" and use a 0-1 value to ramp up the intensity of the beeps
	//as the timer reaches its limit. One instance of the sounds should be triggered locally for each player
	//in the session.
	//Countdown Stop = "Exploding_Entity_Stop" - to play when the countdown timer stops 

ENDPROC


PROC MAINTAIN_GB_GUNRUNNING_DEFEND_BOMBS_ARMED_SOUND(NETWORK_INDEX iVehicle)

	IF IS_SERVER_BIT_SET(biS_ApproachedWarehouse)
		IF !IS_BIT_SET(iBoolsBitSet2,biL2_bombsArmedSoundPlayed)
			iArmedBombSound = GET_SOUND_ID()
			PLAY_SOUND_FROM_ENTITY(iArmedBombSound, "Bomb_Armed" , NET_TO_ENT(iVehicle),"DLC_GR_Disarm_Bombs_Sounds")
			SET_BIT(iBoolsBitSet2,biL2_bombsArmedSoundPlayed)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_GB_GUNRUNNING_DEFEND_SFX()

	MAINTAIN_GB_GUNRUNNING_DEFEND_BOMBS_ARMED_SOUND(serverbd.niPlayerVeh[0])
	MAINTAIN_GB_GUNRUNNING_DEFEND_BOMB_TICKING_SOUND(serverbd.niPlayerVeh[0])
	
ENDPROC

FUNC FLOAT GET_ZOOM_PERCENTAGE()
	RETURN 60.0
ENDFUNC

PROC MAINTAIN_RADAR_ZOOM()
	IF serverBD.iVariation = GB_GUNRUNNING_DEFEND_VALKYRIE
	AND IS_NET_PLAYER_OK(PLAYER_ID())
	AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
	AND NOT IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
		IF IS_BIT_SET(iBoolsBitSet2,biL2_InPedSpookArea)
		OR GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), GET_GB_GUNRUNNING_DEFEND_CONTRABAND_VEHICLE_LOCATION(serverBD.iVariation, serverBD.iBusinessToDefend)) <=50
			IF GET_DPADDOWN_ACTIVATION_STATE() <> DPADDOWN_SECOND
			AND GET_PROFILE_SETTING(PROFILE_DIAPLAY_BIG_RADAR) != 1
				SET_RADAR_ZOOM_PRECISE(GET_ZOOM_PERCENTAGE())
				SET_LOCAL_PLAYER_AS_USING_MISSION_SPECIFIC_ZOOM(TRUE)
				SET_BIT(iBoolsBitSet2,biL2_RadarZoomed)
			ENDIF
		ELSE
			IF IS_BIT_SET(iBoolsBitSet2,biL2_RadarZoomed)
				SET_RADAR_ZOOM_PRECISE(0)
				SET_LOCAL_PLAYER_AS_USING_MISSION_SPECIFIC_ZOOM(FALSE)
				CLEAR_BIT(iBoolsBitSet2,biL2_RadarZoomed)
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(iBoolsBitSet2,biL2_RadarZoomed)
			SET_RADAR_ZOOM_PRECISE(0)
			SET_LOCAL_PLAYER_AS_USING_MISSION_SPECIFIC_ZOOM(FALSE)
			CLEAR_BIT(iBoolsBitSet2,biL2_RadarZoomed)
		ENDIF
	ENDIF
ENDPROC
PROC MAINTAIN_ENEMY_PILOT_IN_SEAT_CHECK()
	IF serverBD.iVariation = GB_GUNRUNNING_DEFEND_VALKYRIE
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.pilotPed)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPlayerVeh[0])
				IF IS_PED_SITTING_IN_VEHICLE_SEAT(NET_TO_PED(serverbd.pilotPed), NET_TO_VEH(serverBD.niPlayerVeh[0]),VS_DRIVER)
					//IF IS_PED_SITTING_IN_VEHICLE_SEAT(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niPlayerVeh[0]),VS_ANY_PASSENGER)
					//OR IS_PED_SITTING_IN_VEHICLE_SEAT(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niPlayerVeh[0]),vs_)
					IF  IS_PED_SITTING_IN_THIS_VEHICLE(PLAYER_PED_ID(),NET_TO_VEH(serverBD.niPlayerVeh[0]))		
						IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) != VS_DRIVER 
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_ENEMY_PILOT_IN_SEAT_CHECK] enemy ped in pilot seat! kick out the player from any other seat ")
							TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niPlayerVeh[0]))
						ENDIF
					ENDIF
					IF DOES_BLIP_EXIST(blipVeh[0])
						SET_BLIP_COLOUR(blipVeh[0], BLIP_COLOUR_RED)
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(blipVeh[0])
						SET_BLIP_COLOUR(blipVeh[0], BLIP_COLOUR_BLUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
			
			
	ENDIF
ENDPROC

PROC MAINTAIN_HANDLE_OBJECTIVE_IN_GARAGE()
	HANDLE_OBJECTIVE_IN_GARAGE(serverBD.packagesServer.niPackage[0], stGarageTimer, stGarageSyncDelay, FALSE)
	
	IF DOES_PART_HAVE_ANY_GB_PACKAGE(PARTICIPANT_ID_TO_INT())
		SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY(TRUE)
	ELSE
		SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY(FALSE)
	ENDIF
	
	
ENDPROC


/// PURPOSE:
///    All the per-frame client-side maintain procedures for the mission
PROC MAINTAIN_GB_GUNRUNNING_DEFEND_TARGETS_CLIENT()

	MAINTAIN_GB_GUNRUNNING_DEFEND_PLAYER_DISTANCE_CHECKS()
	
	MAINTAIN_GB_GUNRUNNING_DEFEND_INTRO_SHARD()
	
	MAINTAIN_GB_GUNRUNNING_DEFEND_TEXT_MESSAGES()
	
	MAINTAIN_GUNRUNNING_DEFEND_INTRO_PHONE_CALLS()
	
	MAINTAIN_GUNRUNNING_DEFEND_MIDDLE_PHONE_CALLS()
	
	MAINTAIN_PLAYER_APPROACH_ACTION_AREA()
	
	MAINTAIN_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()

	MAINTAIN_RADAR_ZOOM()
	
	MAINTAIN_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT()
	
	MAINTAIN_GB_GUNRUNNING_DEFEND_HELP_TEXT()
	
	MAINTAIN_GB_GUNRUNNING_DEFEND_CONTRABAND_VEHICLE()
	
	MAINTAIN_DEFEND_CONTRABAND_PLAYER_SPAWNING()
	
	MAINTAIN_BOTTOM_RIGHT_HUD()
	
	MAINTAIN_TICKERS()
	
	MAINTAIN_GB_DEFEND_DROP_OFF()
	
	MAINTAIN_GB_DEFEND_PACKAGES_CLIENT()
	
	MAINTAIN_PLAYER_MOVE_OFF_MISSION()
	
	MAINTAIN_ENEMY_PILOT_IN_SEAT_CHECK()
	
	MAINTAIN_BOMB_DEFUSAL_CLIENT()
	
	MAINTAIN_GROUND_FLARES()
	
	MAINTAIN_CONTRABAND_DOOR_LOCK()
	
	MAINTAIN_GB_GUNRUNNING_DEFEND_SFX()
	
	MAINTAIN_MANAGE_GB_DEFEND_BOMB_PROPS()
	
	MAINTAIN_GUNRUNNING_DEFEND_AUDIO_MUSIC_TRIGGERS()
	
	MAINTAIN_HANDLE_OBJECTIVE_IN_GARAGE()

ENDPROC

PROC MAINTAIN_HELI_FLASHING_BLIP()
//TODO:
//	blip flash for when the care package DROPS

	//for when the heli is about to escape
	IF IS_SERVER_BIT_SET(biS_HeliAboutToEscape)
		IF DOES_BLIP_EXIST(blipVeh[0])
			SET_BLIP_FLASHES(blipVeh[0], TRUE)		
			SET_BLIP_FLASH_INTERVAL(blipVeh[0] , BLIP_FLASHING_TIME)
			PRINTLN("JS-FLASHBLIP")
		ENDIF
	ENDIF
	//for when the objective updates
	IF IS_BIT_SET(iBoolsBitSet, biL_StealthStageStarted)
		IF DOES_BLIP_EXIST(blipVeh[0])
			IF !IS_BIT_SET(iBoolsBitSet, biL_StealthTextFlashBlip)
				SET_BLIP_FLASH_TIMER(blipVeh[0], stealthFlashBlipTimerThreshold)
				SET_BLIP_FLASH_INTERVAL(blipVeh[0], BLIP_FLASHING_TIME)
				PRINTLN("JS-FLASHOBJUPDATEBLIP")
				SET_BIT(iBoolsBitSet,biL_StealthTextFlashBlip)
			ENDIF
		ENDIF
	ENDIF
	//for when the care package DROPS
	IF IS_BIT_SET(iBoolsBitSet2, bil2_HeloShotDown)
		PRINTLN("JS-SHOTDOWN")
		IF DOES_BLIP_EXIST(localPackages.blipPackage[0])
			IF !IS_BIT_SET(iBoolsBitSet, biL_PackageFlashBlip)
				SET_BLIP_FLASH_TIMER(localPackages.blipPackage[0], stealthFlashBlipTimerThreshold)
				SET_BLIP_FLASH_INTERVAL(localPackages.blipPackage[0], BLIP_FLASHING_TIME)
				PRINTLN("JS-FLASHCAREPACKAGEBLIP")
				SET_BIT(iBoolsBitSet,biL_PackageFlashBlip)
				
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

//PURPOSE: Process the GB_GUNRUNNING_DEFEND stages for the Client
PROC PROCESS_GB_GUNRUNNING_DEFEND_CLIENT()
	IF IS_PLAYER_SCTV(PLAYER_ID())
		EXIT
	ENDIF
	
	SWITCH playerBD[PARTICIPANT_ID_TO_INT()].eStage		
		CASE eAD_TARGETS
		
			IF serverbd.iVariation = GB_GUNRUNNING_DEFEND_VALKYRIE
				gangChaseOptions.iTotalNumberOfWaves = 3
				gangChaseOptions.iNumPedsInEachVeh = 1
				gangChaseOptions.iNumVehInEachWave = 1
				gangChaseOptions.mGangChaseVeh = BUZZARD
				gangChaseOptions.mGangChasePed = S_M_Y_BLACKOPS_02
				gangChaseOptions.vDropffCoord = GET_DROP_OFF_LOCATION()
				gangChaseOptions.iPedAccuracy = 40
				
			ELIF serverbd.iVariation = GB_GUNRUNNING_DEFEND_DEFUSAL
				gangChaseOptions.iTotalNumberOfWaves = 4
				gangChaseOptions.iNumPedsInEachVeh = 2
				gangChaseOptions.iNumVehInEachWave = 2
				gangChaseOptions.mGangChasePed = MP_G_M_PROS_01
				gangChaseOptions.mGangChaseVeh = FUGITIVE
				gangChaseOptions.vDropffCoord = GET_DROP_OFF_LOCATION()
				
				MAINTAIN_BOMB_BLOWUP()
			ENDIF
			
			SET_GANG_CHASE_OPTION(gangChaseOptions, GANG_CHASE_OPTIONS_ALWAYS_CHASE_PLAYER)
			MAINTAIN_GANG_CHASE_CLIENT(gangChaseLocalVar, serverBD.gangChaseServer)
			MAINTAIN_INITIALISE_DELIVERABLE_IDS()
			MAINTAIN_GB_GUNRUNNING_DEFEND_TARGETS_CLIENT()
			MAINTAIN_PLAYER_TRIGGER_AREAS()
			MAINTAIN_HELI_FLASHING_BLIP()
			CHECK_FOR_REWARD()
			IF serverBD.eStage = eAD_OFF_MISSION
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_OFF_MISSION
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  PLAYER STAGE eAD_TARGETS -> eAD_OFF_MISSION AS SERVER SAYS SO   <----------     ") NET_NL()
			ELIF serverBD.eStage = eAD_CLEANUP
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CLEANUP
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  PLAYER STAGE eAD_TARGETS -> eAD_CLEANUP AS SERVER SAYS SO   <----------     ") NET_NL()
			ENDIF
		BREAK
		
		
		CASE eAD_OFF_MISSION
			
			
			CHECK_FOR_REWARD()
			
			IF serverBD.eStage = eAD_CLEANUP
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CLEANUP
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  PLAYER STAGE eAD_OFF_MISSION -> eAD_CLEANUP AS SERVER SAYS SO   <----------     ") NET_NL()
			ENDIF
			

		BREAK
		
		CASE eAD_CLEANUP
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  SCRIPT CLEANUP B     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		BREAK
		
	ENDSWITCH
ENDPROC









PROC MAINTAIN_TARGET_BLIPS()

ENDPROC




PROC MAINTAIN_PED_SET_OFF_CHECKS(PED_INDEX ped, INT iPed)
	IF NOT IS_NET_PED_INJURED(serverbd.Targets[iPed].niPed)
		IF IS_PED_SITTING_IN_ANY_VEHICLE(NET_TO_PED(serverbd.Targets[iPed].niPed))
			IF IS_BIT_SET(serverBD.iTargetVehDamagedBitSet, iPed)

				SET_BIT(serverBD.iTargetDriveFastBitSet, iPed)
				
				ped = NET_TO_PED(serverbd.Targets[iPed].niPed)
				IF IS_PED_DRIVING_ANY_VEHICLE(ped)
					serverBD.Targets[iPed].iPedState = PED_AI_STATE_FLEE_IN_CAR
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] -> TARGET_PED_FLEE_IN_CAR as biS_SomeoneDamagedVeh PED = ", iPed)
				ELSE
					serverBD.Targets[iPed].iPedState = PED_AI_STATE_DEFEND_IN_CAR
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] -> PED_AI_STATE_DEFEND_IN_CAR as biS_SomeoneDamagedVeh PED = ", iPed)
				
				ENDIF

			ELIF IS_BIT_SET(serverBD.iTargetAimedAtBitSet, iPed)
				SET_BIT(serverBD.iTargetDriveFastBitSet, iPed)
				ped = NET_TO_PED(serverbd.Targets[iPed].niPed)
				IF IS_PED_DRIVING_ANY_VEHICLE(ped)
					serverBD.Targets[iPed].iPedState = PED_AI_STATE_FLEE_IN_CAR
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] -> TARGET_PED_FLEE_IN_CAR as biS_SomeoneAimingAtTarget PED = ", iPed)
				ELSE
					serverBD.Targets[iPed].iPedState = PED_AI_STATE_DEFEND_IN_CAR
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] -> PED_AI_STATE_DEFEND_IN_CAR as biS_SomeoneAimingAtTarget PED = ", iPed)
				
				ENDIF
			ELIF IS_BIT_SET(serverBD.iTargetNearbyShotsBitset, iPed)
				SET_BIT(serverBD.iTargetDriveFastBitSet, iPed)
				
				ped = NET_TO_PED(serverbd.Targets[iPed].niPed)
				IF IS_PED_DRIVING_ANY_VEHICLE(ped)
					serverBD.Targets[iPed].iPedState = PED_AI_STATE_FLEE_IN_CAR
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] -> PED_AI_STATE_FLEE_IN_CAR as bullets near target PED = ", iPed)
				ELSE
					serverBD.Targets[iPed].iPedState = PED_AI_STATE_DEFEND_IN_CAR
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] -> PED_AI_STATE_DEFEND_IN_CAR  as bullets near target PED = ", iPed)
				
				ENDIF

			ELSE
				// Added to handle non-script players killing the driver
			//	MAINTAIN_PASSENGER_PED_EXIT(iPed)
			ENDIF
		ELSE
			IF serverBD.Targets[iPed].iPedState != PED_AI_STATE_DEFEND_IN_CAR
				serverBD.Targets[iPed].iPedState = PED_AI_STATE_DEFEND_IN_CAR
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] -> PED_AI_STATE_DEFEND_IN_CAR  as not in a car PED = ", iPed)
			ENDIF	
		ENDIF
	ENDIF
ENDPROC
FUNC VECTOR GB_GUNRUNNING_DEFEND_GET_RANDOM_PED_SPAWN_COORD(VECTOR vCentralCoord)

INT iXCoord, iYCoord
	VECTOR vNewVector
	INT iMaxDist = 25
	FLOAT fTempZ
	IF IS_VECTOR_ZERO(globalPedSpawnWaveVector)
		// Get a random number between -iMaxDist and iMaxDist to get the x coord
		iXCoord = GET_RANDOM_INT_IN_RANGE((iMaxDist - (iMaxDist*2)), iMaxDist)
		
		// Get a random number between the rest of the value between xCoord and iMaxDist for the y coord
		iYCoord = GET_RANDOM_INT_IN_RANGE((iMaxDist - iXCoord), iMaxDist)
		
		// Should this go positive or negative on y from the x coord?
		iYCoord = PICK_INT(GET_RANDOM_BOOL(), iYCoord, (iYCoord - (iYCoord*2)))
		
		// Now get the new vector based on the grabbed coord offsets from the central coord
		vNewVector = <<vCentralCoord.x+iXCoord, vCentralCoord.y+iYCoord, 0.0>>
		globalPedSpawnWaveVector = vNewVector
	ENDIF
		 
		//vNewVector.z = GET_APPROX_HEIGHT_FOR_POINT(vNewVector.x, vNewVector.y)
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [GB_GUNRUNNING_DEFEND_GET_RANDOM_PED_SPAWN_COORD] globalPedSpawnWaveVector = ", globalPedSpawnWaveVector)
	IF GET_GROUND_Z_FOR_3D_COORD(globalPedSpawnWaveVector+<<0,0,100>>, fTempZ)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [GB_GUNRUNNING_DEFEND_GET_RANDOM_PED_SPAWN_COORD] 0")
		globalPedSpawnWaveVector.z = fTempZ
	ELSE
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [GB_GUNRUNNING_DEFEND_GET_RANDOM_PED_SPAWN_COORD] 1")
		globalPedSpawnWaveVector = <<0,0,0>>
	ENDIF
	
	RETURN globalPedSpawnWaveVector

ENDFUNC



/// PURPOSE:
///    Sever side processing of each AI ped. Determines what each ped needs to do next
PROC PROCESS_TARGET_PED_BRAIN()
//	EXIT

	IF HAS_MISSION_END_CONDITION_BEEN_MET()
	//	EXIT
	ENDIF
	

	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		INT i
		VECTOR vGoto
	//	FLOAT fDist2
		FLOAT f2dDist
		VEHICLE_INDEX veh
		PED_INDEX ped
		INT iCurrentGoto
		REPEAT MAX_TARGETS i
			SWITCH serverBD.Targets[i].iPedState
				CASE PED_AI_STATE_CREATED
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niPed)
						
					//	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_DOWN)
					//	IF serverBD.iEnemyPedSpookedServer != 0 
							IF IS_SERVER_TARGET_BIT_SET(i, ENEMY_PED_BIT_HAS_SCENARIO)
								serverBD.Targets[i].iPedState = PED_AI_STATE_PLAY_SCENARIO
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] TARGET_PED_CREATED -> ENEMY_PED_BIT_HAS_SCENARIO PED = ", i)
							ELIF IS_SERVER_TARGET_BIT_SET(i, ENEMY_PED_BIT_DEFEND_START_LOC)
					 			serverBD.Targets[i].iPedState = PED_AI_STATE_DEFEND_AREA
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] TARGET_PED_CREATED -> ENEMY_PED_BIT_DEFEND_START_LOC PED = ", i)
							ELIF IS_SERVER_TARGET_BIT_SET(i, ENEMY_PED_BIT_IS_VEHICLE_GUNNER)
								serverBD.Targets[i].iPedState = PED_AI_STATE_DEFEND_IN_CAR
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] TARGET_PED_CREATED -> ENEMY_PED_BIT_IS_VEHICLE_GUNNER PED = ", i)
							ELIF IS_SERVER_TARGET_BIT_SET(i, ENEMY_PED_HELI_PILOT)
								serverBD.Targets[i].iPedState = PED_AI_STATE_PLAY_SCENARIO
								serverBD.pilotPed = serverBD.Targets[i].niPed
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] TARGET_PED_CREATED -> ENEMY_PED_HELI_PILOT PED = ", i)								
							ENDIF
								
//							ELIF IS_SERVER_TARGET_BIT_SET(i, ENEMY_PED_BIT_CREATE_AS_DRIVER)
//								IF IS_SERVER_TARGET_BIT_SET(i, ENEMY_PED_BIT_ESCORT)
//									serverBD.Targets[i].iPedState = PED_AI_STATE_ESCORT
//									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] TARGET_PED_CREATED -> PED_AI_STATE_ESCORT PED = ", i)
//								ELSE
//									IF IS_SERVER_TARGET_BIT_SET(i, ENEMY_PED_HELI_PILOT)
//										serverBD.Targets[i].iPedState = PED_AI_STATE_PLAY_SCENARIO
//										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] TARGET_PED_CREATED -> PED_AI_STATE_PLAY_SCENARIO PED = ", i)								
//									ELIF IS_SERVER_TARGET_BIT_SET(i, ENEMY_PED_DRIVE_TO_COORD)
//										serverBD.Targets[i].iPedState = PED_AI_STATE_GOTO_COORD_IN_CAR
//										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] TARGET_PED_CREATED -> PED_AI_STATE_GOTO_COORD_IN_CAR PED = ", i)
//									ELSE
//										serverBD.Targets[i].iPedState = PED_AI_STATE_TOUR_IN_CAR
//										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] TARGET_PED_CREATED -> PED_AI_STATE_TOUR_IN_CAR PED = ", i)
//									ENDIF
//								ENDIF
//						
//							ELIF IS_SERVER_TARGET_BIT_SET(i, ENEMY_PED_BIT_CREATE_AS_FPASS)
//								serverBD.Targets[i].iPedState = PED_AI_STATE_TOUR_IN_CAR_AS_PASS
//								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] TARGET_PED_CREATED -> PED_AI_STATE_TOUR_IN_CAR_AS_PASS PED = ", i)
//							ELIF IS_SERVER_TARGET_BIT_SET(i, ENEMY_PED_BIT_CREATE_AS_RLPASS)
//								IF IS_SERVER_TARGET_BIT_SET(i, ENEMY_PED_HELI_PASS) 
//									serverBD.Targets[i].iPedState = PED_AI_STATE_HOVER_IN_HELI
//									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] TARGET_PED_CREATED -> PED_AI_STATE_HOVER_IN_HELI PED = ", i)
//								ELSE
//									serverBD.Targets[i].iPedState = PED_AI_STATE_TOUR_IN_CAR_AS_PASS
//									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] TARGET_PED_CREATED -> PED_AI_STATE_TOUR_IN_CAR_AS_PASS PED = ", i)
//								ENDIF
//							ELIF IS_SERVER_TARGET_BIT_SET(i, ENEMY_PED_BIT_CREATE_AS_RRPASS)
//								IF IS_SERVER_TARGET_BIT_SET(i, ENEMY_PED_HELI_PASS) 
//									serverBD.Targets[i].iPedState = PED_AI_STATE_HOVER_IN_HELI
//									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] TARGET_PED_CREATED -> PED_AI_STATE_HOVER_IN_HELI PED = ", i)
//								ELSE
//									serverBD.Targets[i].iPedState = PED_AI_STATE_TOUR_IN_CAR_AS_PASS
//									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] TARGET_PED_CREATED -> PED_AI_STATE_TOUR_IN_CAR_AS_PASS PED = ", i)
//								ENDIF
//							
//							ENDIF
					//	ENDIF
					//	ENDIF
						
						
					ENDIF
				BREAK
				
				CASE PED_AI_STATE_HOVER_IN_HELI
					IF serverBD.iTargetAimedAtBitSet != 0
					OR serverBD.iTargetNearbyShotsBitset != 0
					OR serverBD.iTargetKilledBitset != 0
					OR serverBD.iTargetSpookedByAreaCheck != 0
					OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ApproachedWarehouse)
					OR IS_SERVER_BIT_SET(biS_ApproachedWarehouse)
						IF IS_SERVER_TARGET_BIT_SET(i, ENEMY_PED_HELI_PASS) 
							serverBD.Targets[i].iPedState = PED_AI_STATE_DEFEND_IN_CAR
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] PED_AI_STATE_HOVER_IN_HELI -> PED_AI_STATE_DEFEND_IN_CAR PED = ", i)
						ELSE
							serverBD.Targets[i].iPedState = PED_AI_STATE_HELI_ATTACK
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] PED_AI_STATE_HOVER_IN_HELI -> PED_AI_STATE_HELI_ATTACK PED = ", i)
						ENDIF
							
					ENDIF
				BREAK
				
				CASE PED_AI_STATE_GOTO_COORD_IN_HELI
					IF HAS_MISSION_END_CONDITION_BEEN_MET()
						serverBD.Targets[i].iPedState = PED_AI_STATE_CLEANUP
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] PED_AI_STATE_GOTO_COORD_IN_HELI -> PED_AI_STATE_CLEANUP PED = ", i)
					ELSE
						
						
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niPed)
							ped = GET_ENEMY_PED(i)
							
							IF NOT IS_PED_INJURED(GET_ENEMY_PED(i))
								IF IS_PED_SITTING_IN_ANY_VEHICLE(GET_ENEMY_PED(i))
									veh = GET_VEHICLE_PED_IS_IN(ped)
									iCurrentGoto = serverBD.Targets[i].iGotoCoordCount
									vGoto = GET_NTH_HELI_GOTO_COORD(serverBD.iVariation, serverBD.iBusinessToDefend, iCurrentGoto)
									
									//-- Check 2d distance to destination (i.e. ignore height)
									f2dDist = GET_DISTANCE_BETWEEN_COORDS(vGoto, GET_ENTITY_COORDS(veh, FALSE), FALSE)
									FLOAT fNextPointRadius
									
									IF serverBD.Targets[i].iGotoCoordCount = GET_LAST_HELI_GOTO_IN_LOCATION(serverBD.iVariation, serverBD.iBusinessToDefend)
										fNextPointRadius = 2.0
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] PED_AI_STATE_GOTO_COORD_IN_HELI Set fNextPointRadius = ",fNextPointRadius)
									ELSE
										fNextPointRadius = 20.0
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] PED_AI_STATE_GOTO_COORD_IN_HELI Set fNextPointRadius = ",fNextPointRadius)
									ENDIF
									
									IF f2dDist < fNextPointRadius
										serverBD.Targets[i].iGotoCoordCount++
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] PED_AI_STATE_GOTO_COORD_IN_HELI Ped ", i, " updated goto count now ", serverBD.Targets[i].iGotoCoordCount)
										
										//-- Update the ped's goto coord if they've reached their current goto
										IF serverBD.Targets[i].iGotoCoordCount = GET_LAST_HELI_GOTO_IN_LOCATION(serverBD.iVariation, serverBD.iBusinessToDefend)
											//set server bit, heading towards last location	 
											SET_SERVER_BIT(biS_HeliAboutToEscape)
											CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] PED_AI_STATE_GOTO_COORD_IN_HELI Set biS_HeliAboutToEscape")
											
										ENDIF
										//
										IF serverBD.Targets[i].iGotoCoordCount > GET_GB_GUNRUNNING_DEFEND_NUMBER_OF_HELI_GOTO_POINTS(serverBD.iVariation, serverBD.iBusinessToDefend)
											IF SHOULD_FAIL_WHEN_HELI_REACHES_END_OF_GOTO_ROUTE(serverBD.iVariation)
												IF NOT IS_SERVER_BIT_SET(biS_ContrabandLost)
													SET_SERVER_BIT(biS_ContrabandLost)
													CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] PED_AI_STATE_GOTO_COORD_IN_HELI Set biS_ContrabandLost")
												ENDIF
											ENDIF
										ENDIF
									ELSE
										
										
								//		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] PED_AI_STATE_GOTO_COORD_IN_HELI Ped ", i, " 2D dist ", f2dDist, " vGoto " , vGoto, " GotoCount ", iCurrentGoto)
										
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
									
					ENDIF
				BREAK
				
				CASE PED_AI_STATE_HELI_ATTACK
				PRINTLN("JS-HELI ATTACK")
					IF HAS_MISSION_END_CONDITION_BEEN_MET()
						serverBD.Targets[i].iPedState = PED_AI_STATE_CLEANUP
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] PED_AI_STATE_HELI_ATTACK -> PED_AI_STATE_CLEANUP PED = ", i)
					ELSE
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niPed)
							IF NOT IS_PED_INJURED(GET_ENEMY_PED(i))
								IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(GET_ENEMY_PED(i))
									serverBD.Targets[i].iPedState = PED_AI_STATE_DEFEND_IN_CAR
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] PED_AI_STATE_HELI_ATTACK -> PED_AI_STATE_DEFEND_IN_CAR as no longer in veh PED = ", i)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE PED_AI_STATE_PLAY_SCENARIO
					IF NOT IS_SERVER_TARGET_BIT_SET(i, ENEMY_PED_BIT_DO_NOT_AUTO_SPOOK)
						IF IS_SERVER_BIT_SET(biS_ApproachedWarehouse)
						OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ApproachedWarehouse)
						OR IS_SERVER_BIT_SET(biS_EnemiesAlerted)
						OR IS_SERVER_BIT_SET(biS_PilotTimerUp)
						
						   //delayed reaction time up
							IF HAS_PEDS_REACTION_DELAY_EXPIRED(i)
								IF NOT IS_SERVER_TARGET_BIT_SET(i, ENEMY_PED_BIT_COWER_WHEN_SPOOKED)
									IF NOT IS_SERVER_TARGET_BIT_SET(i, ENEMY_PED_HELI_PILOT)
										//attack players
										PRINTLN("JS-ATTACK")
										serverBD.Targets[i].iPedState = PED_AI_STATE_DEFEND_AREA
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] PED_AI_STATE_PLAY_SCENARIO -> PED_AI_STATE_DEFEND_AREA PED = ", i, " as biS_ApproachedWarehouse")
									ELSE
										serverBD.Targets[i].iPedState = PED_AI_STATE_ENTER_CAR
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] PED_AI_STATE_PLAY_SCENARIO -> PED_AI_STATE_ENTER_CAR PED = ", i, " as biS_ApproachedWarehouse")
									ENDIF
								ELSE
									serverBD.Targets[i].iPedState = PED_AI_STATE_COWER
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] PED_AI_STATE_PLAY_SCENARIO -> PED_AI_STATE_COWER PED = ", i, " as biS_ApproachedWarehouse")
								ENDIF
							ENDIF
						ELSE
							IF serverBD.iTargetKilledByRivalBitset != 0
								//shot by rival, not people on mission
								serverBD.Targets[i].iPedState = PED_AI_STATE_DEFEND_AREA
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] shot by rival, all peds should defend area and not fly away.")
							ENDIF
						ENDIF
					ELSE
						IF serverBD.iTargetAimedAtBitSet != 0
						OR serverBD.iTargetNearbyShotsBitset != 0
						OR serverBD.iTargetKilledBitset != 0
						OR serverBD.iTargetSpookedByAreaCheck != 0
							IF NOT IS_SERVER_TARGET_BIT_SET(i, ENEMY_PED_BIT_COWER_WHEN_SPOOKED)
								IF NOT IS_SERVER_TARGET_BIT_SET(i, ENEMY_PED_HELI_PILOT)	
									serverBD.Targets[i].iPedState = PED_AI_STATE_DEFEND_AREA
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] PED_AI_STATE_PLAY_SCENARIO -> PED_AI_STATE_DEFEND_AREA PED = ", i)
								ELSE
									serverBD.Targets[i].iPedState = PED_AI_STATE_ENTER_CAR
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] PED_AI_STATE_PLAY_SCENARIO -> PED_AI_STATE_ENTER_CAR PED = ", i, " as biS_ApproachedWarehouse")
								ENDIF
							ELSE
								serverBD.Targets[i].iPedState = PED_AI_STATE_COWER
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] PED_AI_STATE_PLAY_SCENARIO -> PED_AI_STATE_COWER PED = ", i)
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE PED_AI_STATE_WAIT_FOR_APPROACH
					IF IS_SERVER_BIT_SET(biS_VehSetOff)
						serverBD.Targets[i].iPedState = PED_AI_STATE_TOUR_IN_CAR
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] PED_AI_STATE_WAIT_FOR_APPROACH -> PED_AI_STATE_TOUR_IN_CAR PED = ", i)
					ELSE
						MAINTAIN_PED_SET_OFF_CHECKS(ped, i)
					ENDIF
				BREAK
				
				
				CASE PED_AI_STATE_TOUR_IN_CAR
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niPed)
						IF NOT IS_PED_INJURED(GET_ENEMY_PED(i))
							IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(GET_ENEMY_PED(i))
								serverBD.Targets[i].iPedState = PED_AI_STATE_DEFEND_IN_CAR
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] PED_AI_STATE_TOUR_IN_CAR -> PED_AI_STATE_DEFEND_IN_CAR as IS_ANY_PED_ESCORTING_VEH_IN_ATTACK_STATE PED = ", i)
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE PED_AI_STATE_GOTO_COORD_IN_CAR
				PRINTLN("JS-GOTO IN CAR")
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niPed)
						IF NOT IS_PED_INJURED(GET_ENEMY_PED(i))
							IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(GET_ENEMY_PED(i))
								serverBD.Targets[i].iPedState = PED_AI_STATE_DEFEND_IN_CAR
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] PED_AI_STATE_GOTO_COORD_IN_CAR -> PED_AI_STATE_DEFEND_IN_CAR as IS_ANY_PED_ESCORTING_VEH_IN_ATTACK_STATE PED = ", i)
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE PED_AI_STATE_TOUR_IN_CAR_AS_PASS
					
				BREAK
				
				CASE PED_AI_STATE_ESCORT
					
				BREAK
				
				CASE PED_AI_STATE_FLEE_IN_CAR

				BREAK
				
				CASE PED_AI_STATE_LEAVE_CAR
					IF DOES_ENEMY_PED_NI_EXIST(i)
						IF NOT IS_PED_INJURED(GET_ENEMY_PED(i))
							IF NOT IS_PED_IN_ANY_VEHICLE(GET_ENEMY_PED(i))
								serverBD.Targets[i].iPedState = PED_AI_STATE_DEFEND_IN_CAR
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] PED_AI_STATE_LEAVE_CAR -> PED_AI_STATE_DEFEND_IN_CAR ")
							ENDIF
						ENDIF
					ENDIF						
				BREAK
				
				CASE PED_AI_STATE_ENTER_CAR
					IF DOES_ENEMY_PED_NI_EXIST(i)
						IF NOT IS_PED_INJURED(GET_ENEMY_PED(i))
							IF IS_PED_IN_ANY_VEHICLE(GET_ENEMY_PED(i))
								IF IS_SERVER_TARGET_BIT_SET(i, ENEMY_PED_HELI_PILOT)
									//SET BIT FOR TEXT MESSAGE
									PRINTLN("JS-PILOTENTEREDHELI")
									serverBD.pilotPed = serverBD.Targets[i].niPed
									SET_SERVER_BIT(biS_EnemyPilotEnteredHeli)
									serverBD.Targets[i].iPedState = PED_AI_STATE_GOTO_COORD_IN_HELI
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] PED_AI_STATE_ENTER_CAR -> PED_AI_STATE_GOTO_COORD_IN_HELI ")
								
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE PED_AI_STATE_TOUR_ON_FOOT
					
				BREAK
				
				CASE PED_AI_STATE_FLEE_ON_FOOT
				BREAK
				
				CASE PED_AI_STATE_DEFEND_AREA
				
				 	IF IS_SERVER_TARGET_BIT_SET(i, ENEMY_PED_HELI_PILOT)
				 		IF IS_SERVER_BIT_SET(biS_EnemiesAlerted)
							serverBD.Targets[i].iPedState = PED_AI_STATE_ENTER_CAR
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] PILOT WAS FIGHTING BUT NOW GETTING IN HELI:  PED = ", i)
							
						ENDIF
				 	ENDIF
				BREAK
				
				CASE PED_AI_STATE_DEFEND_IN_CAR
				//	MAINTAIN_PASSENGER_PED_EXIT(i)
					PRINTLN("JS-DEFEND IN CAR")
					IF IS_SERVER_TARGET_BIT_SET(i, ENEMY_PED_BIT_CREATE_AS_RRPASS)
					OR IS_SERVER_TARGET_BIT_SET(i, ENEMY_PED_BIT_CREATE_AS_RLPASS)
						IF HAS_MISSION_END_CONDITION_BEEN_MET()
							serverBD.Targets[i].iPedState = PED_AI_STATE_CLEANUP
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BRAIN] PED_AI_STATE_DEFEND_IN_CAR -> PED_AI_STATE_CLEANUP PED = ", i)
						ENDIF
					ENDIF
				BREAK
				
				CASE PED_AI_STATE_CLEANUP
					NETWORK_INDEX ni
					IF DOES_ENEMY_PED_NI_EXIST(i)
						ni = GET_ENEMY_PED_NI(i)
						CLEANUP_NET_ID(ni)
					ENDIF
					
				BREAK
				
			ENDSWITCH
		ENDREPEAT
	ENDIF
ENDPROC


//PURPOSE:checks to see if the vehicle is stuck or undrivable
FUNC BOOL GB_DEFEND_VEHICLE_STUCK_CHECKS(VEHICLE_INDEX veh)
	
//	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[iTarget].niVeh)
		IF IS_VEHICLE_DRIVEABLE(veh)

			IF IS_VEHICLE_STUCK_TIMER_UP(veh, VEH_STUCK_ON_ROOF, ROOF_TIME)
			OR IS_VEHICLE_STUCK_TIMER_UP(veh, VEH_STUCK_ON_SIDE, SIDE_TIME)
			//	PRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     MTA - [PROCESS_TARGET_PED_BODY] [GB_DEFEND_VEHICLE_STUCK_CHECKS] I think this vehicle is stuck ", iTarget)
				
				RETURN TRUE
				
			ENDIF 
		ELSE
			RETURN TRUE
		ENDIF 
//	ENDIF 
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Determine the closest on-mission player to the AI ped specified
/// PARAMS:
///    iTarget - 
/// RETURNS:
///    
FUNC PLAYER_INDEX GET_CLOSEST_ATTACKING_PLAYER_TO_TARGET(INT iTarget)
	PLAYER_INDEX playerClosest = INVALID_PLAYER_INDEX()
	FLOAT fClosestDist = 9999999999.0
	FLOAT fDist = 0.0
	INT i
	
	VECTOR vTargetCoors = GET_ENTITY_COORDS(NET_TO_PED(serverBD.Targets[iTarget].niPed), FALSE)
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			PED_INDEX PlayerPedId = GET_PLAYER_PED(PlayerId)
			IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PlayerID)

				IF GB_GET_THIS_PLAYER_GANG_BOSS(PlayerID) = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.iBossPartWhoLaunched))
					IF NOT IS_PED_INJURED(PlayerPedid)
						fDist = VDIST2(GET_ENTITY_COORDS(PlayerPedID), vTargetCoors)	
						IF fDist < fClosestDist
							fClosestDist = fDist
							playerClosest = playerId
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		IF playerClosest != INVALID_PLAYER_INDEX()
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND GET_CLOSEST_ATTACKING_PLAYER_TO_TARGET playerClosest = ", GET_PLAYER_NAME(playerClosest), " Target = ", iTarget)
		ELSE
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND GET_CLOSEST_ATTACKING_PLAYER_TO_TARGET playerClosest = INVALID_PLAYER_INDEX Target = ", iTarget)
		ENDIF
	#ENDIF
	
	RETURN playerClosest
ENDFUNC

FUNC PED_INDEX GET_PED_TO_FLEE(INT iTargetFlee)
	PED_INDEX pedToFlee
	PLAYER_INDEX playerToFlee = GET_CLOSEST_ATTACKING_PLAYER_TO_TARGET(iTargetFlee)
	IF playerToFlee != INVALID_PLAYER_INDEX()
		pedToFlee = GET_PLAYER_PED(playerToFlee)
	ENDIF
	
	RETURN pedToFlee
ENDFUNC

PROC MAINTAIN_MARKER_FOR_TARGET(INT iTarget)
	IF NOT SHOULD_LOCAL_PLAYER_SEE_GB_GB_GUNRUNNING_DEFEND_HUD()
		EXIT
	ENDIF
	
	IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) < GB_UI_LEVEL_BACKGROUND
		EXIT
	ENDIF
	
	IF NOT DID_MY_BOSS_LAUNCH_GB_GB_GUNRUNNING_DEFEND()
		EXIT
	ENDIF
	
	IF NOT IS_SERVER_TARGET_BIT_SET(iTarget, ENEMY_PED_BIT_HAS_MARKER)
		EXIT
	ENDIF
	
	INT r, g, b, a
	VECTOR vCoord
	
	GET_HUD_COLOUR(HUD_COLOUR_RED, r, g, b, a)

	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[iTarget].niPed)
		IF NOT IS_NET_PED_INJURED(serverBD.Targets[iTarget].niPed)
			IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(NET_TO_PED(serverBD.Targets[iTarget].niPed))
				vCoord = GET_ENTITY_COORDS(NET_TO_PED(serverBD.Targets[iTarget].niPed))
				DRAW_MARKER(MARKER_ARROW, vCoord+<<0,0,2>>, <<0,0,0>>, <<180,0,0>>, <<0.5,0.5,0.5>>, r, g, b, 100, TRUE, TRUE)
			ELSE
				VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(NET_TO_PED(serverBD.Targets[iTarget].niPed))
				FM_EVENTDRAW_MARKER_ABOVE_VEHICLE(veh, r, g, b)
			ENDIF
		ENDIF
	ENDIF
		
ENDPROC

INT iActiveDamageTrackingBitset
INT iStaggeredDamageTrackCount
/// PURPOSE:
///    Activate damage tracking on on-foot peds . Every player needs to turn it on. Needed so that damage events register properly
PROC MAINTAIN_ACTIVATE_DAMAGE_TRACKING_CLIENT()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[iStaggeredDamageTrackCount].niPed)
		IF NOT IS_NET_PED_INJURED(serverBD.Targets[iStaggeredDamageTrackCount].niPed)
			IF NOT IS_BIT_SET(iActiveDamageTrackingBitset, iStaggeredDamageTrackCount)
				ACTIVATE_DAMAGE_TRACKER_ON_NETWORK_ID(serverBD.Targets[iStaggeredDamageTrackCount].niPed, TRUE)
				SET_BIT(iActiveDamageTrackingBitset, iStaggeredDamageTrackCount)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [MAINTAIN_ACTIVATE_DAMAGE_TRACKING_CLIENT] Turned on damage tracking for target ", iStaggeredDamageTrackCount)
			ENDIF
		ENDIF
	ENDIF
	
	iStaggeredDamageTrackCount++
	
	IF iStaggeredDamageTrackCount = MAX_TARGETS
		iStaggeredDamageTrackCount = 0
	ENDIF
ENDPROC

/// PURPOSE:
///    For 'stealth' variations, draw a 'vision cone' on the minimap for  the ped specified
/// PARAMS:
///    iPed - 
PROC MAINTAIN_AI_PED_VISION_CONE(INT iPed)
	IF NOT IS_SERVER_TARGET_BIT_SET(iPed, ENEMY_PED_BIT_VISION_CONE)
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_AI_PED_VISION_CONE] ENEMY_PED_BIT_VISION_CONE BIT NOT SET")
		EXIT
	ENDIF
	
	PED_INDEX ped
	
	IF DOES_ENEMY_PED_NI_EXIST(iPed)
		ped = GET_ENEMY_PED(iPed)
		IF NOT IS_PED_INJURED(ped)
			IF NOT DOES_BLIP_EXIST(blipTarget[iPed])
				blipTarget[iPed] = ADD_BLIP_FOR_ENTITY(ped)
				SET_BLIP_COLOUR(blipTarget[iPed], BLIP_COLOUR_RED)
				SET_BLIP_ROUTE(blipTarget[iPed], FALSE)
				SET_BLIP_PRIORITY(blipTarget[iPed], BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
				SET_BLIP_SCALE(blipTarget[iPed], 0.5)
				SET_BLIP_SHOW_CONE(blipTarget[iPed], TRUE)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_AI_PED_VISION_CONE] Added blip for ped ", iPed)
			ELSE
				IF IS_BIT_SET(ServerBD.iTargetSpookedByAreaCheck, iPed)
					SET_BLIP_SHOW_CONE(blipTarget[iPed], FALSE)
				ENDIF
				
				IF IS_PED_IN_ANY_VEHICLE(ped)
					IF NOT IS_SERVER_TARGET_BIT_SET(iPed, ENEMY_PED_BIT_IS_VEHICLE_GUNNER)
						SET_BLIP_SHOW_CONE(blipTarget[iPed], FALSE)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(blipTarget[iPed])
				REMOVE_BLIP(blipTarget[iPed])
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    For 'stealth' variations, determine of the AI ped specified has seen the local player
/// PARAMS:
///    pedId - 
/// RETURNS:
///    
FUNC BOOL HAS_PED_PERCEIVED_PLAYER(PED_INDEX pedId)
	IF !IS_PED_INJURED(PLAYER_PED_ID())
		IF !IS_PED_INJURED(pedId)
			IF NOT IS_PED_PERFORMING_STEALTH_KILL(PLAYER_PED_ID()) // url:bugstar:3001524
				IF IS_TARGET_PED_IN_PERCEPTION_AREA(pedId, PLAYER_PED_ID())// 45.0, 60.0, 3.5, 4.0) // These values are halved in code - overriding to help sort url:bugstar:3001524
				AND HAS_ENTITY_CLEAR_LOS_TO_ENTITY(pedId, PLAYER_PED_ID(), SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Determine if the AI ped specified has been alerted, or 'spooked' by the local player
/// PARAMS:
///    index - 
PROC SPOOKED_CHECK_FOR_ENEMY_PED(INT index)
	IF NOT DOES_ENEMY_PED_NI_EXIST(index)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iEnemyPedSpookedClient, index)
		EXIT
	ENDIF
	

	//VECTOR vLoc = GET_DROP_OFF_LOCATION()
	IF NOT IS_PED_INJURED(GET_ENEMY_PED(index))
		IF NOT IS_SERVER_TARGET_BIT_SET(index, ENEMY_PED_BIT_VISION_CONE)
//			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vLoc) < 75.0 * 75.0
//			//OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ApproachedWarehouse)
//			 
//				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iEnemyPedSpookedClient, index)
//				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [SPOOKED_CHECK_FOR_ENEMY_PED] Spooked ped ", index, " - HAS_PLAYER_APPROACHED_ACTION_AREA")
//			ELSE
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					//IF IS_PED_CURRENT_WEAPON_SILENCED 
					//IF IS_PLAYER_AIMING_AT_PED(GET_ENEMY_PED(index)) 
						//am i within a certain range of the ped
						//if i have a sniper rifle
						//SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iEnemyPedSpookedClient, index)
						//CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [SPOOKED_CHECK_FOR_ENEMY_PED] Spooked ped ", index, " - IS_PLAYER_AIMING_AT_PED")
					IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(GET_ENEMY_PED(index)), 1.0)
				
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iEnemyPedSpookedClient, index)
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [SPOOKED_CHECK_FOR_ENEMY_PED] Spooked ped ", index, " - IS_BULLET_IN_AREA")
					ENDIF
				ENDIF
			//ENDIF
		ELSE
			//-- 'stealth' variation
			
			//Did the ped take a bullet and survive?
			//IF //IS_PED_INJURED(GET_ENEMY_PED(index))
				//	SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iEnemyPedSpookedClient, index)
				//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [SPOOKED_CHECK_FOR_ENEMY_PED] Spooked ped ", index, " - PED GOT SHOT")
			//ENDIF
			
			//Did the ped detect a bullet in the area
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF !IS_PED_CURRENT_WEAPON_SILENCED(PLAYER_PED_ID())
					IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(GET_ENEMY_PED(index)), 3.0)
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iEnemyPedSpookedClient, index)
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [SPOOKED_CHECK_FOR_ENEMY_PED] Spooked ped ", index, " - IS_BULLET_IN_AREA")
					ENDIF
				ENDIF
			ENDIF
			
			//Did the ped percieve the player?
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetSpookedByAreaCheck, index)
				IF HAS_PED_PERCEIVED_PLAYER(GET_ENEMY_PED(index))
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetSpookedByAreaCheck, index)
					PRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [SPOOKED_CHECK_FOR_ENEMY_PED] iPlayerTargetSpookedByAreaCheck - HAS_PED_PERCEIVED_PLAYER - ", index)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//-- Helper functions for attaching crate props to vehicles (in the back of a a van, for example)
FUNC VECTOR GET_CRATE_ATTACH_OFFSET_FOR_MODEL(MODEL_NAMES modelVeh, INT propID)
	SWITCH modelVeh
		//for the bomb props
		CASE POUNDER
			SWITCH propID
			CASE 0 	RETURN <<1.4,0.0,0.5>>
			CASE 1  RETURN <<-1.4,-5.0,0.5>>
			CASE 2 	RETURN <<0.2,-8.1,0.5>>
			CASE 3 	RETURN <<1.4,0.0,0.5>>
			CASE 4  RETURN <<-1.4,-5.0,0.5>>
			CASE 5 	RETURN <<0.2,-8.1,0.5>>
			ENDSWITCH
		BREAK
		CASE GBURRITO
		CASE GBURRITO2
			RETURN <<0.0,-1.0,-0.3>>
			
		CASE BODHI2
			RETURN <<0.0,-2.0,-0.1>>
		
		CASE rumpo3
			RETURN <<-0.17, -2.3, 0.1>>
			
		CASE VALKYRIE2
			SWITCH propID
			// Y: + = move all crates towards front. - = move towards back
				CASE 0 RETURN <<0.0,1.38,0.0>>
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN <<0.0,-1.0,-0.3>>
ENDFUNC

FUNC VECTOR GET_CRATE_ATTACH_ROTATION_FOR_MODEL(MODEL_NAMES modelVeh, INT propID)
	SWITCH modelVeh
		//for the bomb props
		CASE POUNDER
			SWITCH propID
			CASE 0 	RETURN <<0.0,90.0,0.0>>
			CASE 1  RETURN <<0.0,90.0,180.0>>
			CASE 2 	RETURN <<90.0,0.0,0.0>>
			CASE 3 	RETURN <<0.0,90.0,0.0>>
			CASE 4  RETURN <<0.0,90.0,180.0>>
			CASE 5 	RETURN <<90.0,0.0,0.0>>
			ENDSWITCH
		BREAK
		CASE rumpo3
			RETURN <<90, 0, 180>>
		
		CASE VALKYRIE2
			SWITCH propID
			//X = tilt towards tail end. Y = roll either side. Z = 
				CASE 0	RETURN <<175.0,180.0,180.0>>
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN <<0.0,0.0,90.0>>
ENDFUNC


FUNC STRING GET_BONE_TO_ATTACH_CRATE_TO_FOR_MODEL(MODEL_NAMES modelVeh)
	SWITCH modelVeh

		CASE rumpo3
			RETURN "chassis_dummy" // "PROP_MB_CRATE_01A_SET"
	ENDSWITCH
	
	RETURN "chassis_dummy"
ENDFUNC
/// PURPOSE:
/// Using the position of the array, checks to see if the prop is a bomb
FUNC BOOL GB_GUNRUNNING_DEFEND_IS_PROP_ATTACHABLE(INT inputArray, INT iVariation)
	SWITCH iVariation
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			SWITCH inputArray
				CASE 7
				RETURN TRUE
			ENDSWITCH
		BREAK
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
			SWITCH inputArray
				CASE 6 
				CASE 7 
				CASE 8 
				CASE 9
				CASE 10 
				CASE 11
				
				RETURN TRUE
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

///: So there are only 3 bomb props, but nine props total.
///  We want to grab the 3 bomb props out of the array and set them to a specific position.
///  This function will correctly assign each bomb prop an ID, so they can be positioned accordingly.  
FUNC INT GB_GUNRUNNING_DEFEND_CONVERT_PROP_ARRAY_TO_ATTACHABLE_ARRAY(INT inputArray , INT iVariation)
	SWITCH iVariation
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			SWITCH inputArray
				CASE 7 RETURN 0
			ENDSWITCH
		BREAK
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL	
			SWITCH inputArray
				CASE 6 RETURN 0
				CASE 7 RETURN 1
				CASE 8 RETURN 2	
				CASE 9 RETURN 3	
				CASE 10 RETURN 4	
				CASE 11 RETURN 5	
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN 0
ENDFUNC
PROC MAINTAIN_ATTACH_PROP_TO_VEHICLE(INT iProp, VEHICLE_INDEX veh, INT propID)
//	EXIt
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niProp[iProp])	
//		IF DOES_ENEMY_VEH_NI_EXIST(iVeh)
//			veh = GET_ENEMY_VEH(iVeh)
			IF IS_VEHICLE_DRIVEABLE(veh)
				IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(NET_TO_OBJ(serverBD.niProp[iProp]),veh)
					IF MAINTAIN_OWNERSHIP_OF_NETWORK_ID(serverBD.niProp[iProp])
						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niProp[iProp])
							IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(veh)
								NETWORK_REQUEST_CONTROL_OF_ENTITY(veh)
							ENDIF
							IF GB_GUNRUNNING_DEFEND_IS_PROP_ATTACHABLE(iProp, serverbd.iVariation)
								propID = GB_GUNRUNNING_DEFEND_CONVERT_PROP_ARRAY_TO_ATTACHABLE_ARRAY(iProp, serverbd.iVariation)
							ENDIF
							PRINTLN("JS-propID = ",propID)
							IF NETWORK_HAS_CONTROL_OF_ENTITY(veh)
								STRING sBone = GET_BONE_TO_ATTACH_CRATE_TO_FOR_MODEL(GET_ENTITY_MODEL(veh))
								VECTOR vAttachOff = GET_CRATE_ATTACH_OFFSET_FOR_MODEL(GET_ENTITY_MODEL(veh),propID)
								VECTOR vAttachRot = GET_CRATE_ATTACH_ROTATION_FOR_MODEL(GET_ENTITY_MODEL(veh),propID)
								PRINTLN("JS-vAttachOff = ",vAttachOff, " JS-vAttachRot = ",vAttachRot )
								ATTACH_ENTITY_TO_ENTITY(NET_TO_OBJ(serverBD.niProp[iProp]), veh, GET_ENTITY_BONE_INDEX_BY_NAME(veh, sBone), vAttachOff, vAttachRot, FALSE, FALSE, FALSE)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND  [MAINTAIN_ATTACH_PROP_TO_VEHICLE] Attached!")
								VECTOR v = GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.niProp[iProp]))
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND  [MAINTAIN_ATTACH_PROP_TO_VEHICLE] CRATE COORDS: ",v)
								
							ENDIF
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF
//		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Client side-processing of each AI ped. Get each AI ped to do what the should be doing, based on what server says (see PROCESS_TARGET_PED_BRAIN)
PROC PROCESS_TARGET_PED_BODY()

	IF HAS_MISSION_END_CONDITION_BEEN_MET()
	//	EXIT
	ENDIF
	
	
	VECTOR vLoc
	INT i
	
	VECTOR vMin, vMax
	FLOAT fDefendWidth
	
//	PED_INDEX pedToFlee
	SCRIPTTASKSTATUS status
	PED_INDEX ped
	INT iMyVehicle

	TEXT_LABEL_63 tl63Scen
	VECTOR vDefensive
	VECTOR vGoto
//	VECTOR vHeliCreate
	//FLOAT fDefensiveSphere
	FLOAT fHead
	INT iFlightHeight
	INT iMinFlightHeight
	VEHICLE_INDEX veh
	NETWORK_INDEX ni
	VEHICLE_INDEX vehToUse
	
		
	REPEAT MAX_TARGETS i
		IF NOT HAS_MISSION_END_CONDITION_BEEN_MET()
			//-- Aiming at target?
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerAimAtTargetBitSet, i)
				IF NOT IS_BIT_SET(serverBD.iTargetAimedAtBitSet, i)
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niPed)
						IF DOES_ENTITY_EXIST(NET_TO_PED(serverbd.Targets[i].niPed))
						AND NOT IS_ENTITY_DEAD(NET_TO_PED(serverbd.Targets[i].niPed))
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								IF GET_MY_BOSS_PARTICPANT_ID() = serverBD.iBossPartWhoLaunched
									IF IS_PLAYER_AIMING_AT_PED(NET_TO_PED(serverbd.Targets[i].niPed)) 
										SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerAimAtTargetBitSet, i)
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_CONTRA_DEFEND - [PROCESS_TARGET_PED_BODY] SET iPlayerAimAtTargetBitSet AS I AM AIMING AT PED = ", i)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//-- Target veh stuck?
	//		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetVehStuckBitSet, i)
	//			IF NOT IS_BIT_SET(serverBD.iTargetVehStuckBitSet, i)
	//				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niVeh)
	//					IF GB_DEFEND_VEHICLE_STUCK_CHECKS(i)
	//						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetVehStuckBitSet, i)
	//						PRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_CONTRA_DEFEND - [PROCESS_TARGET_PED_BODY] I think this vehicle is stuck ", i)
	//					ENDIF
	//				ENDIF
	//			ENDIF
	//		ENDIF
			
			//-- Bullets near target?
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerShotsNearTargetBitSet, i)
				IF NOT IS_BIT_SET(serverBD.iTargetNearbyShotsBitset, i)
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niPed)
						IF NOT IS_NET_PED_INJURED(serverbd.Targets[i].niPed)
							IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(NET_TO_PED(serverbd.Targets[i].niPed)), 10.0)
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerShotsNearTargetBitSet, i)
								PRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_CONTRA_DEFEND - [PROCESS_TARGET_PED_BODY] I think shots are near this target ", i)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//INT iAngled
			
//			//-- Spooked by player entering area?
//			//IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetSpookedByAreaCheck, i)
//			//if not custom bit
//				REPEAT MAX_ANGLED_AREA_CHECKS iAngled
//					IF IS_DEFEND_ANGLED_AREA_VALID(serverBD.iVariation, serverBD.iBusinessToDefend, iAngled)
//						GET_GB_DEFEND_ANGLED_AREA_FOR_SPOOKED_CHECK(serverBD.iVariation, serverBD.iBusinessToDefend, iAngled, vMin, vMax, fWidth)
//						IF NOT ARE_VECTORS_EQUAL(vMin, << 0.0, 0.0, 0.0 >>)
//							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//								IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), vMin, vMax, fWidth)
//									//SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetSpookedByAreaCheck, i)
//									//set custom bit here
//									PRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_CONTRA_DEFEND - [PROCESS_TARGET_PED_BODY] I spooked ped ", i, " due to angled-area check vMin = ", vMin)
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDREPEAT
			//ENDIF
		ENDIF
		BOOL bForce = !HAS_MISSION_END_CONDITION_BEEN_MET()
		IF SHOULD_LOCAL_PLAYER_SEE_GB_GB_GUNRUNNING_DEFEND_HUD()
			SPOOKED_CHECK_FOR_ENEMY_PED(i)
			
			//-- Blip for AI ped
			SWITCH serverBD.iVariation
				CASE GB_GUNRUNNING_DEFEND_DEFUSAL
				//PRINTLN("JS-BBB0")
					//IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WithinObjectiveRange)
					IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_NearActionArea)
					//IF IS_SERVER_BIT_SET(biS_SomeoneNearActionArea
					//IF IS_SERVER_BIT_SET(biS_ApproachedWarehouse)
						//IF !IS_SERVER_TARGET_BIT_SET(i, ENEMY_PED_BIT_VISION_CONE)
						
							UPDATE_ENEMY_NET_PED_BLIP( serverbd.Targets[i].niPed, PedBlipData[i], DEFAULT, DEFAULT, bForce, DEFAULT)
						//ELSE					
							// MAINTAIN_AI_PED_VISION_CONE(i)
						//ENDIF
					ENDIF
				BREAK
				
				CASE GB_GUNRUNNING_DEFEND_VALKYRIE
					IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WithinObjectiveRange)
					//IF IS_SERVER_BIT_SET(biS_ApproachedWarehouse)
						PRINTLN("JS-enemiesalerted: ",IS_SERVER_BIT_SET(biS_EnemiesAlerted))
					
						IF IS_SERVER_TARGET_BIT_SET(i, ENEMY_PED_BIT_VISION_CONE)
						AND !IS_SERVER_BIT_SET(biS_EnemiesAlerted)
							MAINTAIN_AI_PED_VISION_CONE(i)
						ELSE	
							IF DOES_BLIP_EXIST(blipTarget[i])
								REMOVE_BLIP(blipTarget[i])
							ENDIF							
							UPDATE_ENEMY_NET_PED_BLIP( serverbd.Targets[i].niPed, PedBlipData[i], DEFAULT, DEFAULT, bForce, DEFAULT)
						ENDIF
				
					ELSE
						IF IS_SERVER_TARGET_BIT_SET(i, ENEMY_PED_BIT_VISION_CONE)
						AND IS_SERVER_BIT_SET(biS_EnemiesAlerted)						
							UPDATE_ENEMY_NET_PED_BLIP( serverbd.Targets[i].niPed, PedBlipData[i], DEFAULT, DEFAULT, bForce, DEFAULT)
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH

			
		ENDIF
		
		SWITCH serverBD.iVariation
			CASE GB_GUNRUNNING_DEFEND_VALKYRIE
				IF IS_SERVER_TARGET_BIT_SET(i, ENEMY_PED_HELI_PILOT)
					vehToUse = NET_TO_VEH(serverBD.niPlayerVeh[0])
					INIT_GB_GUNRUNNING_DEFEND_CONTRABAND_VEH_SPAWN_COORDS(GB_GUNRUNNING_DEFEND_VALKYRIE, serverBD.iBusinessToDefend, 0, vLoc, fHead)
				ENDIF
			BREAK
		ENDSWITCH
		
		//-- Marker
		MAINTAIN_MARKER_FOR_TARGET(i)
		
		
		SWITCH serverBD.Targets[i].iPedState
			CASE PED_AI_STATE_PLAY_SCENARIO
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niPed)
					IF DOES_ENTITY_EXIST(GET_ENEMY_PED(i))
					AND NOT IS_ENTITY_DEAD(GET_ENEMY_PED(i))

						status = GET_SCRIPT_TASK_STATUS(GET_ENEMY_PED(i), SCRIPT_TASK_START_SCENARIO_IN_PLACE)
						IF status != PERFORMING_TASK
						AND status != WAITING_TO_START_TASK
							IF MAINTAIN_OWNERSHIP_OF_NETWORK_ID(serverbd.Targets[i].niPed)
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverbd.Targets[i].niPed)
									tl63Scen = GET_GB_GUNRUNNING_DEFEND_PED_SCENARIO(serverBD.iVariation, serverBD.iBusinessToDefend, i)
									IF NOT IS_STRING_NULL_OR_EMPTY(tl63Scen)
										TASK_START_SCENARIO_IN_PLACE(GET_ENEMY_PED(i), tl63Scen)
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [PROCESS_TARGET_PED_BODY] I have given enemy ", i, " TASK_START_SCENARIO_IN_PLACE with scenario ", tl63Scen)
									ELSE
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [PROCESS_TARGET_PED_BODY] I cannot give given enemy a scenario ", i, " scenario empty!")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE PED_AI_STATE_GOTO_COORD_IN_HELI
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niPed)
					IF DOES_ENTITY_EXIST(GET_ENEMY_PED(i))
					AND NOT IS_ENTITY_DEAD(GET_ENEMY_PED(i))
						IF IS_PED_IN_ANY_VEHICLE(GET_ENEMY_PED(i))
							veh = GET_VEHICLE_PED_IS_IN(GET_ENEMY_PED(i))
							
							status = GET_SCRIPT_TASK_STATUS(GET_ENEMY_PED(i), SCRIPT_TASK_VEHICLE_MISSION)
							IF (status != PERFORMING_TASK
							AND status != WAITING_TO_START_TASK)
							OR iLocalTargetGotoCount[i] != serverBD.Targets[i].iGotoCoordCount
								IF MAINTAIN_OWNERSHIP_OF_NETWORK_ID(serverbd.Targets[i].niPed)
									IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverbd.Targets[i].niPed)
										vGoto = GET_NTH_HELI_GOTO_COORD(serverBD.ivariation, serverBD.iBusinessToDefend, serverBD.Targets[i].iGotoCoordCount)
									
//										 IF serverBD.Targets[i].iGotoCoordCount = GET_LAST_HELI_GOTO_IN_LOCATION()
//										 //set server bit, heading towards last location	 
//											SET_BIT(iServerBitSet, biS_HeliAboutToEscape)
//											PRINTLN("JS-HELI ABOUT TO ESCAPE")
//										 ENDIF
										//slow the heli down until it gets to the first node
										IF serverBD.Targets[i].iGotoCoordCount < 1
											fHeliCruiseSpeed = 10
										ELSE
											fHeliCruiseSpeed = 30
										ENDIF
										
										IF NOT ARE_VECTORS_EQUAL(vGoto, << 0.0, 0.0, 0.0 >>)
											iFlightHeight 		= ROUND(vGoto.z) + 20
											iMinFlightHeight 	= ROUND(vGoto.z) - 20
											TASK_HELI_MISSION(GET_ENEMY_PED(i), 
																veh,
																NULL,
																NULL,
																vGoto,
																MISSION_GOTO,
																fHeliCruiseSpeed, 20.0, -1, iFlightHeight, iMinFlightHeight, -1, 
																HF_MaintainHeightAboveTerrain | HF_StartEngineImmediately)
											
											IF iLocalTargetGotoCount[i] != serverBD.Targets[i].iGotoCoordCount
												iLocalTargetGotoCount[i] = serverBD.Targets[i].iGotoCoordCount
												CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [PROCESS_TARGET_PED_BODY] Given ped ", i, " TASK_VEHICLE_MISSION_COORS_TARGET to coord ", vGoto, " iFlightHeight = ", iFlightHeight, " iMinFlightHeight = ", iMinFlightHeight)
												CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [PROCESS_TARGET_PED_BODY] Given ped ", i, " TASK_VEHICLE_MISSION_COORS_TARGET updated iLocalTargetGotoCount = ", iLocalTargetGotoCount[i])
											ELSE
												CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [PROCESS_TARGET_PED_BODY] Given ped ", i, " TASK_VEHICLE_MISSION_COORS_TARGET to coord ", vGoto, " iFlightHeight = ", iFlightHeight, " iMinFlightHeight = ", iMinFlightHeight)
											ENDIF
										ELSE
											CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [PROCESS_TARGET_PED_BODY] Given ped ", i, " Goto is at origin! serverBD.Targets[i].iGotoCoordCount = ", serverBD.Targets[i].iGotoCoordCount)
										ENDIF
										
									ENDIF
								ELSE
									//-- I don't have control, but keep local goto count in sync with server
									IF iLocalTargetGotoCount[i] != serverBD.Targets[i].iGotoCoordCount
										iLocalTargetGotoCount[i] = serverBD.Targets[i].iGotoCoordCount		
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [PROCESS_TARGET_PED_BODY] PED_AI_STATE_GOTO_COORD_IN_HELI - I don't have cintrol of ped ", i, " but updated iLocalTargetGotoCount to ", iLocalTargetGotoCount[i])
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
//					
				
			BREAK
			
			CASE PED_AI_STATE_HELI_ATTACK
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niPed)
					IF NOT IS_NET_PED_INJURED(serverbd.Targets[i].niPed)
						status = GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.Targets[i].niPed), SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED)
						IF status != PERFORMING_TASK
						AND status != WAITING_TO_START_TASK
							IF MAINTAIN_OWNERSHIP_OF_NETWORK_ID(serverBD.Targets[i].niPed)
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverbd.Targets[i].niPed)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(serverBD.Targets[i].niPed), FALSE)
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(NET_TO_PED(serverBD.Targets[i].niPed), 299 )
									
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BODY] I've given TASK_COMBAT_HATED_TARGETS_AROUND_PED PED = ", i)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE PED_AI_STATE_TOUR_IN_CAR
			
					iMyVehicle = GET_GB_GUNRUNNING_DEFEND_VEH_PED_SHOULD_USE(serverBD.iVariation, serverBD.iBusinessToDefend, i)
					IF iMyVehicle >= 0

						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[iMyVehicle])
							ni = serverBD.niVeh[iMyVehicle]
							veh = NET_TO_VEH(serverBD.niVeh[iMyVehicle])
						ENDIF
						
						
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(ni)
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niPed)
								IF IS_NET_VEHICLE_DRIVEABLE(ni)
									IF NOT IS_NET_PED_INJURED(serverbd.Targets[i].niPed)
										IF IS_PED_SITTING_IN_VEHICLE(NET_TO_PED(serverbd.Targets[i].niPed), veh)
											ped = NET_TO_PED(serverbd.Targets[i].niPed)
											IF IS_PED_DRIVING_ANY_VEHICLE(ped)
												IF MAINTAIN_OWNERSHIP_OF_NETWORK_ID(serverbd.Targets[i].niPed)
													IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverbd.Targets[i].niPed)
														IF (GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverbd.Targets[i].niPed), SCRIPT_TASK_VEHICLE_DRIVE_WANDER) != PERFORMING_TASK
														AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverbd.Targets[i].niPed), SCRIPT_TASK_VEHICLE_DRIVE_WANDER) != WAITING_TO_START_TASK)
															TASK_VEHICLE_DRIVE_WANDER(NET_TO_PED(serverbd.Targets[i].niPed), veh, 12.0, DRIVINGMODE_STOPFORCARS)
															
															CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BODY] I've given ped TASK_VEHICLE_DRIVE_WANDER PED = ", i, " veh = ", iMyVehicle)
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BODY] iMyVehicle < 0 Ped ", i) 
					ENDIF
			//	ENDIF
			BREAK
			
			CASE PED_AI_STATE_GOTO_COORD_IN_CAR
				
				iMyVehicle = GET_GB_GUNRUNNING_DEFEND_VEH_PED_SHOULD_USE(serverBD.iVariation, serverBD.iBusinessToDefend, i)
				IF iMyVehicle >= 0

					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[iMyVehicle])
						ni = serverBD.niVeh[iMyVehicle]
						veh = NET_TO_VEH(serverBD.niVeh[iMyVehicle])
					ENDIF
					
					
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(ni)
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niPed)
							IF IS_NET_VEHICLE_DRIVEABLE(ni)
								IF NOT IS_NET_PED_INJURED(serverbd.Targets[i].niPed)
									IF IS_PED_SITTING_IN_VEHICLE(NET_TO_PED(serverbd.Targets[i].niPed), veh)
										ped = NET_TO_PED(serverbd.Targets[i].niPed)
										
								//		GET_INITIAL_GOTO_FOR_GLOBAL_DEFEND_VEHICLE(serverBD.iBusinessToDefend, vGoto)
										IF NOT ARE_VECTORS_EQUAL(vGoto, << 0.0, 0.0, 0.0 >>)
											IF IS_PED_DRIVING_ANY_VEHICLE(ped)
												IF MAINTAIN_OWNERSHIP_OF_NETWORK_ID(serverbd.Targets[i].niPed)
													IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverbd.Targets[i].niPed)
														IF (GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverbd.Targets[i].niPed), SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD) != PERFORMING_TASK
														AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverbd.Targets[i].niPed), SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD) != WAITING_TO_START_TASK)
															TASK_VEHICLE_DRIVE_TO_COORD(ped, veh, vGoto, 15.0, DRIVINGSTYLE_NORMAL, DUMMY_MODEL_FOR_SCRIPT, DRIVINGMODE_STOPFORCARS, 5.0, 5.0)
															CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BODY] GIVEN PED ", i, " TASK_VEHICLE_DRIVE_TO_COORD to coord ", vGoto)
														ENDIF
													ENDIF
												ENDIF
											ENDIF
											
											IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iEnemyPedReachedInCarGoto, i)
												IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(NET_TO_PED(serverBD.Targets[i].niPed), vGoto) < 20.0
													SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iEnemyPedReachedInCarGoto, i)
													CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BODY] Set iEnemyPedReachedInCarGoto ped ", i)
												ENDIF
											ENDIF
										ELSE
											CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BODY] Can't GIVE PED ", i, " TASK_VEHICLE_DRIVE_TO_COORD as goto is 0!")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			BREAK
			
			
			CASE PED_AI_STATE_ESCORT
					
			BREAK
			
			CASE PED_AI_STATE_FLEE_IN_CAR
				ni = serverbd.Targets[i].niPed
				
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(ni)
					ped = NET_TO_PED(ni)
					IF NOT IS_NET_PED_INJURED(ni)
						IF MAINTAIN_OWNERSHIP_OF_NETWORK_ID(ni)
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(ni)
								status = GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_SMART_FLEE_POINT)
								IF status != PERFORMING_TASK
								AND status != WAITING_TO_START_TASK
									IF NOT ARE_VECTORS_EQUAL(vLoc, << 0.0, 0.0, 0.0>>)
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped, TRUE)
										SET_PED_COMBAT_ATTRIBUTES(ped, CA_ALWAYS_FIGHT, FALSE)
										SET_PED_COMBAT_ATTRIBUTES(ped, CA_ALWAYS_FLEE, TRUE)
										SET_PED_FLEE_ATTRIBUTES(ped, FA_DISABLE_COWER,TRUE)
										SET_PED_FLEE_ATTRIBUTES(ped,  FA_COWER_INSTEAD_OF_FLEE,FALSE)
										TASK_SMART_FLEE_COORD(ped, vLoc, 10000.0, 999999)
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BODY] Given TASK_SMART_FLEE_COORD to ped ", i, " coord ", vloc)
									ELSE
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BODY] Want to flee be coord is origin! Ped ", i)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
							
				
			BREAK
			
			CASE PED_AI_STATE_LEAVE_CAR
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niPed)
					IF NOT IS_NET_PED_INJURED(serverbd.Targets[i].niPed)
						IF MAINTAIN_OWNERSHIP_OF_NETWORK_ID(serverbd.Targets[i].niPed)
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverbd.Targets[i].niPed)
								status = GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.Targets[i].niPed), SCRIPT_TASK_LEAVE_ANY_VEHICLE)
								IF status != PERFORMING_TASK
								AND status != WAITING_TO_START_TASK
							
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(serverBD.Targets[i].niPed), TRUE)
									TASK_LEAVE_ANY_VEHICLE(NET_TO_PED(serverBD.Targets[i].niPed))
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BODY] I've given TASK_LEAVE_ANY_VEHICLE PED = ", i)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE PED_AI_STATE_ENTER_CAR
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niPed)
					IF NOT IS_NET_PED_INJURED(serverbd.Targets[i].niPed)
						IF MAINTAIN_OWNERSHIP_OF_NETWORK_ID(serverbd.Targets[i].niPed)
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverbd.Targets[i].niPed)
								IF IS_VEHICLE_DRIVEABLE(vehToUse)
									IF NOT IS_PED_IN_ANY_VEHICLE(NET_TO_PED(serverbd.Targets[i].niPed))
										IF GET_PED_IN_VEHICLE_SEAT(vehToUse) = NULL
											status = GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.Targets[i].niPed), SCRIPT_TASK_ENTER_VEHICLE)
											IF status != PERFORMING_TASK
											AND status != WAITING_TO_START_TASK
												PRINTLN("JS-PILOTENTEREDHELI")
												//SET_BIT(serverbd.iServerBitSet,biS_EnemyPilotEnteredHeli)
												//SET_BIT(iServerTargetBitset,ENEMY_PED_ENTERED_HELI)
												SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(serverBD.Targets[i].niPed), TRUE)
												TASK_ENTER_VEHICLE(NET_TO_PED(serverbd.Targets[i].niPed), vehToUse, -1, VS_DRIVER)
												CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BODY] I've given TASK_ENTER_VEHICLE PED = ", i)
											ENDIF
										ENDIF
									ENDIF
								ELSE
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BODY] Can't give ped TASK_ENTER_VEHICLE as veh not driveable PED = ", i)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE PED_AI_STATE_TOUR_ON_FOOT
				
			BREAK
				
			CASE PED_AI_STATE_FLEE_ON_FOOT
				
			BREAK
			
			CASE PED_AI_STATE_DEFEND_IN_CAR
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niPed)
					IF NOT IS_NET_PED_INJURED(serverbd.Targets[i].niPed)
						status = GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.Targets[i].niPed), SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED)
						IF status != PERFORMING_TASK
						AND status != WAITING_TO_START_TASK
							IF MAINTAIN_OWNERSHIP_OF_NETWORK_ID(serverBD.Targets[i].niPed)
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverbd.Targets[i].niPed)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(serverBD.Targets[i].niPed), FALSE)
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(NET_TO_PED(serverBD.Targets[i].niPed), 299 )
									
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BODY] I've given TASK_COMBAT_HATED_TARGETS_AROUND_PED PED = ", i)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE PED_AI_STATE_DEFEND_AREA
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niPed)
					IF NOT IS_NET_PED_INJURED(serverbd.Targets[i].niPed)
						//status = GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverbd.Targets[i].niPed), SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA)
						IF ( GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverbd.Targets[i].niPed), SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA) != PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverbd.Targets[i].niPed), SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA) != WAITING_TO_START_TASK)
							IF MAINTAIN_OWNERSHIP_OF_NETWORK_ID(serverbd.Targets[i].niPed)
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverbd.Targets[i].niPed)
									 IF ! IS_SERVER_TARGET_BIT_SET(i, ENEMY_IS_AMBUSH_PED)
									 
										SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(NET_TO_PED(serverbd.Targets[i].niPed))
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(serverbd.Targets[i].niPed), TRUE)
			
										IF IS_SERVER_TARGET_BIT_SET(i, ENEMY_PED_BIT_USE_DEFENSIVE_AREA)
											INIT_GB_GUNRUNNING_DEFEND_PED_DEFENSIVE_COORDS(serverBD.iVariation, serverBD.iBusinessToDefend, i, vMin,vMax , fDefendWidth)
											////SET_PED_SPHERE_DEFENSIVE_AREA( GET_ENEMY_PED(i), vDefensive, fDefensiveSphere)
											///    		   commented out for testing
											//SET_PED_ANGLED_DEFENSIVE_AREA(GET_ENEMY_PED(i),vMin,vMax,fDefendWidth)
										ENDIF
										IF ARE_VECTORS_EQUAL(vMin, << 0.0, 0.0, 0.0 >>)
										//backup function if future added peds are not setout to have a defensive area
											vLoc = GET_CONTRA_DEFEND_WAREHOUSE_LOCATION()
										ELSE
											vLoc = vDefensive
										ENDIF
										
										TASK_COMBAT_HATED_TARGETS_IN_AREA(NET_TO_PED(serverbd.Targets[i].niPed), vMin, 250)
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BODY] I HAVE GIVEN TASK_COMBAT_HATED_TARGETS_IN_AREA TO PED ", i, " Loc = ", vLoc, " vMin = ", vMin)
										
										SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverbd.Targets[i].niPed), CA_PERMIT_CHARGE_BEYOND_DEFENSIVE_AREA, FALSE) 
										SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverbd.Targets[i].niPed), CA_SWITCH_TO_ADVANCE_IF_CANT_FIND_COVER, TRUE)
										SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverbd.Targets[i].niPed), CA_SWITCH_TO_DEFENSIVE_IF_IN_COVER, TRUE)
										SET_PED_COMBAT_MOVEMENT(NET_TO_PED(serverbd.Targets[i].niPed), CM_DEFENSIVE)
										SET_PED_SPHERE_DEFENSIVE_AREA(NET_TO_PED(serverbd.Targets[i].niPed), vMin, 250)
										
									ELSE
										
										IF !IS_ENTITY_IN_AIR(GET_ENEMY_PED(i))
											VECTOR tempCoords
											tempCoords = GET_ENTITY_COORDS(GET_ENEMY_PED(i))
											
											SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(GET_ENEMY_PED(i))
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(serverbd.Targets[i].niPed), FALSE)
											//last digit corresponds to the area in which they can move.
											///    		   commented out for testing
											//SET_PED_SPHERE_DEFENSIVE_AREA( GET_ENEMY_PED(i), tempCoords, 20)
											TASK_COMBAT_HATED_TARGETS_IN_AREA(NET_TO_PED(serverbd.Targets[i].niPed), tempCoords, 250)
					
										ENDIF
										
										
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF	
			BREAK
			
			CASE PED_AI_STATE_COWER
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niPed)
					IF DOES_ENTITY_EXIST(GET_ENEMY_PED(i))
					AND NOT IS_ENTITY_DEAD(GET_ENEMY_PED(i))
						IF MAINTAIN_OWNERSHIP_OF_NETWORK_ID(serverbd.Targets[i].niPed)
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverbd.Targets[i].niPed)
								status = GET_SCRIPT_TASK_STATUS(GET_ENEMY_PED(i), SCRIPT_TASK_COWER)
								IF status != PERFORMING_TASK
								AND status != WAITING_TO_START_TASK 
									SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(GET_ENEMY_PED(i))
									TASK_COWER(GET_ENEMY_PED(i))
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BODY] I have given enemy ", i, " TASK_COWER")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDREPEAT
	
	
	
	
	//--Enemy veh checks
	i = 0
	REPEAT MAX_ENEMY_VEH i
		IF IS_SERVER_ENEMY_VEH_BIT_SET(i, ENEMY_VEH_BIT_DO_NEARBY_PLAYER_CHECK)
			IF NOT IS_SERVER_ENEMY_VEH_BIT_SET(i, ENEMY_VEH_BIT_PLAYER_NEARBY)
				IF DOES_ENEMY_VEH_NI_EXIST(i)
					IF IS_VEHICLE_DRIVEABLE(GET_ENEMY_VEH(i))
						IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetVehNearbyBitSet, i)
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(GET_ENEMY_VEH(i))) < 50.0 * 50.0
									SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetVehNearbyBitSet, i)
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [PROCESS_TARGET_PED_BODY] I think I am nearby enemy veh ", i)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		
		
	ENDREPEAT
	
	//-- Attach care package to the rear of / inside contraband vehicle
	IF DOES_GB_GUNRUNNING_DEFEND_VARIATION_REQUIRE_CRATES_IN_BACK_OF_VAN(serverBD.iVariation)
		i = 0
		INT iNumProps = GET_GB_GUNRUNNING_DEFEND_NUMBER_OF_INITIAL_PROPS_FOR_VARIATION(serverBD.iVariation, serverBD.iBusinessToDefend)
	//	INT iVehToAttachTo
		INT iObjectToAttach
		VEHICLE_INDEX vehToAttach
		
		REPEAT iNumProps i
			IF IS_SERVER_PROP_BIT_SET(i, PROP_BIT_ATTACH_TO_VEH)
				iObjectToAttach = i
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPlayerVeh[0])
					vehToAttach = NET_TO_VEH(serverBD.niPlayerVeh[0])
					MAINTAIN_ATTACH_PROP_TO_VEHICLE(iObjectToAttach, vehToAttach, iNumProps)
					
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	
	MAINTAIN_ACTIVATE_DAMAGE_TRACKING_CLIENT()
ENDPROC



PROC INIT_SPAWN_COORDS()
	IF NOT ARE_VECTORS_EQUAL(serverBD.pedSpawnCoords[0].vLoc, <<0.0, 0.0, 0.0>>)
		EXIT
	ENDIF
	
	GET_GB_DEFEND_GANG_VARIATION()
	
	INT iVariation = GET_GB_DEFEND_VARIATION()
	INT iBusiness = serverBD.iBusinessToDefend
	
	INT iNumPeds 		= GET_GB_GUNRUNNING_DEFEND_NUMBER_OF_INITIAL_PEDS_FOR_VARIATION(iVariation, iBusiness)
	INT iNumAmbVehs 	= GET_GB_GUNRUNNING_DEFEND_NUMBER_OF_INITIAL_AMBIENT_VEHS_FOR_VARIATION(iVariation, iBusiness)
//	INT iNumEnemyVehs	= GET_GB_GUNRUNNING_DEFEND_NUMBER_OF_INITIAL_ENEMY_VEHS_FOR_VARIATION(iVariation, iBusiness)
	INT iNumProps 		= GET_GB_GUNRUNNING_DEFEND_NUMBER_OF_INITIAL_PROPS_FOR_VARIATION(iVariation, iBusiness)
	INT iNumPackages	= serverBD.iNumberOfPackages //GET_GB_GUNRUNNING_DEFEND_NUMBER_OF_PACKAGES_FOR_VARIATION(iVariation, iBusiness)
	
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [INIT_SPAWN_COORDS] - iVariation = ", iVariation, " Business = ", ENUM_TO_INT(iBusiness), " iNumPeds = ",iNumPeds, " iNumAmbVehs = ",iNumAmbVehs, " iNumProps = ", iNumProps)
	
	INT i
	REPEAT iNumPeds i
		INIT_GB_GUNRUNNING_DEFEND_PED_SPAWN_COORDS(iVariation, iBusiness, i, serverBD.Targets[i].vSpawnCoord, serverBD.Targets[i].fSpawnHead)
	ENDREPEAT
	
	i = 0
	REPEAT iNumAmbVehs i
		INIT_GB_GUNRUNNING_DEFEND_AMBIENT_VEH_SPAWN_COORDS(iVariation, iBusiness, i, serverBD.ambientVehSpawnCoords[i].vLoc, serverBD.ambientVehSpawnCoords[i].fHead)
	ENDREPEAT
	
//	i = 0
//	REPEAT iNumEnemyVehs i
//		INIT_GB_GUNRUNNING_DEFEND_ENEMY_VEH_SPAWN_COORDS(iVariation, iBusiness, i, serverBD.enemyVehSpawnCoords[i].vLoc, serverBD.enemyVehSpawnCoords[i].fHead)
//	ENDREPEAT
	
	i = 0
	REPEAT iNumProps i
		INIT_GB_GUNRUNNING_DEFEND_PROP_SPAWN_COORDS(iVariation, iBusiness, i, serverBD.propSpawnCoords[i].vLoc, serverBD.propSpawnCoords[i].fHead)
	ENDREPEAT
	
	i = 0
	REPEAT iNumPackages i
		serverBD.packageSpawnCoords[i].vLoc = GET_GB_GUNRUNNING_DEFEND_PACKAGE_SPAWN_COORDS(iVariation, iBusiness, i)
	ENDREPEAT
ENDPROC

PROC BLOCK_WAREHOUSE_SCENARIOS()

	IF IS_SERVER_BIT_SET(biS_BlockedScenarios)
		EXIT
	ENDIF
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		EXIT
	ENDIF
	
	INT iNumBlockingAreas = GET_GB_GUNRUNNING_DEFEND_NUMBER_OF_SCENARIO_BLOCKING_AREAS(serverBD.iVariation, serverBD.iBusinessToDefend)
	INT i
	VECTOR vMin
	VECTOR vMax
	
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [BLOCK_WAREHOUSE_SCENARIOS] This many scenario blocking areas: ", iNumBlockingAreas)
	IF iNumBlockingAreas > 0
		REPEAT iNumBlockingAreas i
			GET_GB_DEFEND_SCENARIO_BLOCKING_AREA_DETAILS(serverBD.iVariation, serverBD.iBusinessToDefend, i, vMin, vMax)
			IF NOT ARE_VECTORS_EQUAL(vMin, << 0.0, 0.0, 0.0 >>)
				serverBD.scenWarehouse[i] = ADD_SCENARIO_BLOCKING_AREA(vMin, vMax, TRUE)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - [BLOCK_WAREHOUSE_SCENARIOS] Added scenario blocking area ", i, " vMin = ", vMin, " vMax = ", vMax)
			ENDIF
		ENDREPEAT
	ENDIF
	
	SET_SERVER_BIT(biS_BlockedScenarios)
ENDPROC




FUNC BOOL GET_GB_DEFEND_PED_SPAWN_COORD(INT iTarget, VECTOR &vSpawn, FLOAT &fSpawn)
	
	INT i = iTarget // GET_RANDOM_INT_IN_RANGE(0, GET_NUMBER_OF_ENEMIES_FOR_VARIATION())
	BOOL bTryClearArea
	IF NOT ARE_VECTORS_EQUAL(serverBD.pedSpawnCoords[i].vLoc, << 0.0, 0.0, 0.0 >>)
		IF NOT serverBD.pedSpawnCoords[i].bUsed
			IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(	serverBD.pedSpawnCoords[i].vLoc, // VECTOR vPoint, 
																1.0, 						 // FLOAT fVehRadius = 6.0, 
																DEFAULT, 						 // FLOAT fPedRadius = 1.0, 
																DEFAULT,						 // FLOAT fObjRadius = 1.0,
																DEFAULT,						 // FLOAT fViewRadius = 5.0,
																FALSE,						 // BOOL bCheckThisPlayerSight = TRUE,
																FALSE,						 // BOOL bDoAnyPlayerSeePointCheck = TRUE,
																DEFAULT,						 // BOOL bCheckVisibilityForOwnTeam = TRUE,
																DEFAULT,						 // FLOAT fVisibleDistance = 120.0,
																DEFAULT, 						 // BOOL bIgnoreLocalPlayerChecks=FALSE, 
																DEFAULT,						 // INT iTeamToIgnore=-1,
																DEFAULT,							 // BOOL bDoFireCheck=TRUE,
																DEFAULT)						//	FLOAT fPlayerRadius = 0.0
				vSpawn = serverBD.pedSpawnCoords[i].vLoc
				fSpawn = serverBD.pedSpawnCoords[i].fHead
				serverBD.pedSpawnCoords[i].bUsed = TRUE
				
				RETURN TRUE
			ELSE
				
				
				IF IS_ANY_VEHICLE_NEAR_POINT( serverBD.pedSpawnCoords[i].vLoc, 1.0 )
					bTryClearArea = TRUE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [GET_GB_DEFEND_PED_SPAWN_COORD] FAILING ON IS_POINT_OK_FOR_NET_ENTITY_CREATION (IS_ANY_VEHICLE_NEAR_POINT) WILL TRY CLEAR AREA iTarget = ", iTarget, " Loc = ", serverBD.pedSpawnCoords[i].vLoc)
				ELIF IS_ANY_PED_NEAR_POINT( <<serverBD.pedSpawnCoords[i].vLoc.x, serverBD.pedSpawnCoords[i].vLoc.y, serverBD.pedSpawnCoords[i].vLoc.z + 1.0>>, 1.0) 
					OR IS_ANY_PED_NEAR_POINT( serverBD.pedSpawnCoords[i].vLoc, 1.0 )
					bTryClearArea = TRUE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [GET_GB_DEFEND_PED_SPAWN_COORD] FAILING ON IS_POINT_OK_FOR_NET_ENTITY_CREATION (IS_ANY_VEHICLE_NEAR_POINT) WILL TRY CLEAR AREA iTarget = ", iTarget, " Loc = ", serverBD.pedSpawnCoords[i].vLoc)
				ELSE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [GET_GB_DEFEND_PED_SPAWN_COORD] FAILING ON IS_POINT_OK_FOR_NET_ENTITY_CREATION iTarget = ", iTarget, " Loc = ", serverBD.pedSpawnCoords[i].vLoc)
				ENDIF
				
				IF bTryClearArea
					IF NOT IS_BIT_SET(serverBD.iPedClearAreaBitset, i)
						//CLEAR_AREA(serverBD.pedSpawnCoords[i].vLoc, 5.0, TRUE, DEFAULT, DEFAULT, TRUE)
						SET_BIT(serverBD.iPedClearAreaBitset, i)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [GET_GB_DEFEND_PED_SPAWN_COORD] SET_BIT(serverBD.iPedClearAreaBitset, ", i, ")")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	RETURN FALSE
	
	
ENDFUNC

/// PURPOSE:
///    Sets the difficulty modifiers for enemy peds in the mode. Based on number of members in the launch gang.
PROC GB_GUNRUNNING_DEFEND_SET_PED_ATTRIBUTES_FOR_DIFFICULTY(PED_INDEX &ped, INT iForceDifficulty = -1)
	FLOAT fHealthModifier = 1.0
	
	INT iHealth

	INT iDifficulty
	
	IF NOT IS_PED_INJURED(ped)
		IF iForceDifficulty = -1
			iDifficulty = GB_BIKER_GET_CURRENT_CLUBHOUSE_MISSION_DIFFICULTY()
		ELSE
			iDifficulty = iForceDifficulty
		ENDIF
		
		IF iDifficulty >= GB_BIKER_CLUBHOUSE_MISSION_DIFFICULTY_EASY
			SWITCH iDifficulty
				CASE GB_BIKER_CLUBHOUSE_MISSION_DIFFICULTY_EASY
					//-- Easy
					SET_PED_ACCURACY(ped, g_sMPTunables.iBIKER_DEFEND_EASY_ENEMY_ACCURACY      )
					SET_PED_SHOOT_RATE(ped, g_sMPTunables.iBIKER_DEFEND_EASY_ENEMY_FIRE_RATE  )
					SET_PED_COMBAT_ATTRIBUTES(ped, CA_USE_PROXIMITY_FIRING_RATE, FALSE)
					SET_PED_COMBAT_ABILITY(ped, CAL_POOR)
					fHealthModifier = g_sMPTunables.fBIKER_DEFEND_EASY_ENEMY_HEALTH  // 0.6
					PRINTLN("[BIKER1] [GB_GUNRUNNING_DEFEND_SET_PED_ATTRIBUTES_FOR_DIFFICULTY] - Easy")
				BREAK
				
				CASE GB_BIKER_CLUBHOUSE_MISSION_DIFFICULTY_MEDIUM
					//-- Medium
					SET_PED_ACCURACY(ped, g_sMPTunables.iBIKER_DEFEND_MEDIUM_ENEMY_ACCURACY )
					SET_PED_SHOOT_RATE(ped, g_sMPTunables.iBIKER_DEFEND_MEDIUM_ENEMY_FIRE_RATE  )
					SET_PED_COMBAT_ATTRIBUTES(ped, CA_USE_PROXIMITY_FIRING_RATE, FALSE)
					fHealthModifier = g_sMPTunables.fBIKER_DEFEND_MEDIUM_ENEMY_HEALTH // 0.8
					PRINTLN("[BIKER1] [GB_GUNRUNNING_DEFEND_SET_PED_ATTRIBUTES_FOR_DIFFICULTY] - Medium")
				BREAK
				
				CASE GB_BIKER_CLUBHOUSE_MISSION_DIFFICULTY_HARD
			
					//-- Hard
					SET_PED_ACCURACY(ped, g_sMPTunables.iBIKER_DEFEND_HARD_ENEMY_ACCURACY )
					SET_PED_SHOOT_RATE(ped, g_sMPTunables.iBIKER_DEFEND_HARD_ENEMY_FIRE_RATE  )
					fHealthModifier = g_sMPTunables.fBIKER_DEFEND_HARD_ENEMY_HEALTH
					PRINTLN("[BIKER1] [GB_GUNRUNNING_DEFEND_SET_PED_ATTRIBUTES_FOR_DIFFICULTY] - Hard")
				BREAK
			ENDSWITCH
			
			iHealth = ROUND(100.0 + (100.0 * fHealthModifier))
			IF GET_ENTITY_HEALTH(ped) > iHealth
				SET_ENTITY_MAX_HEALTH(ped, iHealth)
				SET_ENTITY_HEALTH(ped, iHealth)
			ENDIF
			
			PRINTLN("[BIKER1] [GB_GUNRUNNING_DEFEND_SET_PED_ATTRIBUTES_FOR_DIFFICULTY] Ped given health ", iHealth)
		ELSE
			PRINTLN("[BIKER1] [GB_BIKER_SET_PED_ATTRIBUTES_FOR_DIFFICULTY] iDifficulty < GB_BIKER_CLUBHOUSE_MISSION_DIFFICULTY_EASY = ", iDifficulty)
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		PRINTLN("[BIKER1] [GB_BIKER_SET_PED_ATTRIBUTES_FOR_DIFFICULTY] ped is injured, call stack...")
		DEBUG_PRINTCALLSTACK()
		NET_SCRIPT_ASSERT("GB_BIKER_SET_PED_ATTRIBUTES_FOR_DIFFICULTY - ped is injured!")
		#ENDIF
	ENDIF
ENDPROC
/// PURPOSE:
///    Common initialization settings for AI peds
/// PARAMS:
///    iTarget - 
PROC SETUP_TARGET(INT iTarget)
//	INT iHealth
//	FLOAT fHealth
//	FLOAT fHealthMultiplier = GET_HEALTH_MULTIPLIER()
	PED_INDEX ped = GET_ENEMY_PED(iTarget)
	
	WEAPON_TYPE weapon
	
	SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(ped, TRUE)
						
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped, TRUE)
	
	
	IF NOT IS_SERVER_TARGET_BIT_SET(iTarget, ENEMY_PED_BIT_IS_COP)
		SET_PED_RELATIONSHIP_GROUP_HASH(ped,rgFM_AiHate)
	ELSE
		SET_PED_AS_COP(ped, TRUE)
		SET_PED_WILL_ONLY_ATTACK_WANTED_PLAYER(ped, TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(ped,rgFM_AiHatePlyrLikeCops)
	ENDIF
	
	SET_PED_DIES_WHEN_INJURED(ped,TRUE)
	
	IF IS_SERVER_TARGET_BIT_SET(iTarget, ENEMY_IS_AMBUSH_PED)
		weapon = WEAPONTYPE_PISTOL
	ELSE
		weapon = GET_WEAPON_FOR_GUNRUNNING_DEFEND_PED(iTarget, serverBD.iVariation, serverBD.iBusinessToDefend)
	ENDIF
	
	SET_PED_CONFIG_FLAG(ped, PCF_DisableGoToWritheWhenInjured, TRUE)

	GIVE_WEAPON_TO_PED(ped, weapon, 9999999, TRUE)

	
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND [SETUP_TARGET] iTarget = ", iTarget, "GIVE_WEAPON_TO_PED weapon  - ", ENUM_TO_INT(weapon))
	
	SET_PED_COMBAT_MOVEMENT(ped, CM_WILLADVANCE)
	SET_PED_CONFIG_FLAG(ped, PCF_DontInfluenceWantedLevel, TRUE)
	SET_PED_HIGHLY_PERCEPTIVE(ped, TRUE)
	//SET_PED_COMBAT_ATTRIBUTES(ped, CA_SWITCH_TO_ADVANCE_IF_CANT_FIND_COVER, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(ped, CA_AGGRESSIVE, TRUE)
	//SET_PED_COMBAT_ATTRIBUTES(ped, CA_SWITCH_TO_DEFENSIVE_IF_IN_COVER, TRUE)
	SET_PED_TARGET_LOSS_RESPONSE(ped, TLR_NEVER_LOSE_TARGET)
	
	
	SET_PED_CONFIG_FLAG(ped, PCF_DisableLadderClimbing, TRUE)
	
	IF IS_SERVER_TARGET_BIT_SET(iTarget,ENEMY_PED_BIT_CREATE_AS_DRIVER)
	OR IS_SERVER_TARGET_BIT_SET(iTarget,ENEMY_PED_BIT_CREATE_AS_FPASS)
	OR IS_SERVER_TARGET_BIT_SET(iTarget,ENEMY_PED_BIT_IS_VEHICLE_GUNNER)
		SET_PED_COMBAT_ATTRIBUTES(ped, CA_LEAVE_VEHICLES, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(ped, CA_DO_DRIVEBYS, TRUE)
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND [SETUP_TARGET] iTarget = ", iTarget, " CA_DO_DRIVEBYS" )
	ENDIF
	
	SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(ped, KNOCKOFFVEHICLE_HARD)
	IF !IS_SERVER_TARGET_BIT_SET(iTarget, ENEMY_IS_AMBUSH_PED)
		IF IS_SERVER_TARGET_BIT_SET(iTarget, ENEMY_PED_BIT_DEFEND_START_LOC)
			SET_PED_SPHERE_DEFENSIVE_AREA( ped, serverbd.Targets[iTarget].vSpawnCoord, 7.0)
		ENDIF
	ENDIF
	
	
	IF IS_SERVER_TARGET_BIT_SET(iTarget, ENEMY_PED_BIT_VISION_CONE)
		SET_PED_SEEING_RANGE(ped, 10.0)
		SET_PED_HEARING_RANGE(ped, 10.0)
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND [SETUP_TARGET] iTarget = ", iTarget, " ENEMY_PED_BIT_VISION_CONE")
	ENDIF
	
	//-- Default is 60
	//SET_PED_ACCURACY(ped, 90)
	
	INT iDifficulty = GET_GB_GUNRUNNING_DEFEND_DIFFICULTY()
	
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND [SETUP_TARGET] iTarget = ", iTarget, " iDifficulty = " , iDifficulty)
	GB_GUNRUNNING_DEFEND_SET_PED_ATTRIBUTES_FOR_DIFFICULTY(ped, iDifficulty)
	SET_PED_KEEP_TASK(ped, TRUE)
						
ENDPROC

/// PURPOSE:
///    Create an AI ped (on foot) with the array index specified
/// PARAMS:
///    iTarget - 
/// RETURNS:
///    
FUNC BOOL CREATE_TARGET(INT iTarget)
	//MODEL_NAMES mVehToUse
	MODEL_NAMES mPedTToUSe = GET_GB_GUNRUNNING_DEFEND_PED_MODEL(serverBD.iVariation, serverBD.iBusinessToDefend, iTarget)
	VECTOR vSpawn
	FLOAT fSpawn
	

			
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[iTarget].niPed)
		PRINTLN("JS-CCC1:  ", iTarget)
		IF CAN_REGISTER_MISSION_PEDS(1)
		PRINTLN("JS-CCC2:  ",  iTarget)
		//	mPedTToUSe = serverbd.Targets[iTarget].mPed
			
			IF REQUEST_LOAD_MODEL(mPedTToUSe)
				IF ARE_VECTORS_EQUAL(serverbd.Targets[iTarget].vSpawnCoord, << 0.0, 0.0, 0.0>>)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [CREATE_TARGET] TRYING TO FIND SPAWN COORD iTarget = ", iTarget)
					IF GET_GB_DEFEND_PED_SPAWN_COORD(iTarget, vSpawn, fSpawn)
						serverbd.Targets[iTarget].vSpawnCoord = vSpawn
						serverbd.Targets[iTarget].fSpawnHead = fSpawn
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [CREATE_TARGET] GOT SPAWN COORD iTarget = ", iTarget, " vSpawnCoord = ", vSpawn, " fSpawnHead = ", fSpawn)
					ENDIF
				ELSE

					
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - CREATE_TARGET  GOT SPAWN COORDS iTargetNum = ", iTarget, " vSpawn = ", serverbd.Targets[iTarget].vSpawnCoord, " fSpawn = ",serverbd.Targets[iTarget].fSpawnHead)
					
					
					IF CREATE_NET_PED(serverbd.Targets[iTarget].niPed, PEDTYPE_CRIMINAL, mPedTToUSe, serverbd.Targets[iTarget].vSpawnCoord, serverbd.Targets[iTarget].fSpawnHead)
						
						IF IS_SERVER_TARGET_BIT_SET(iTarget, ENEMY_PED_CUSTOM_VARIATION)
						//	SET_GB_GUNRUNNING_DEFEND_PED_COMPONENT_VARIATION(serverBD.iVariation, serverBD.iBusinessToDefend, iTarget)
						ENDIF
						SETUP_TARGET(iTarget)
						
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - CREATE_TARGET  CREATED PED FOR TARGET iTargetNum = ", iTarget)
						SWITCH serverBD.iVariation
							CASE GB_GUNRUNNING_DEFEND_VALKYRIE
								SET_PED_ACCURACY(NET_TO_PED(serverbd.Targets[iTarget].niPed),g_sMPTunables.IGR_DEFEND_VALKYRIE_ENEMY_ACCURACY)
							BREAK
							CASE GB_GUNRUNNING_DEFEND_DEFUSAL
								SET_PED_ACCURACY(NET_TO_PED(serverbd.Targets[iTarget].niPed),g_sMPTunables.IGR_DEFEND_DISARM_BOMBS_ENEMY_ACCURACY)
							BREAK
						ENDSWITCH
						
						
						IF IS_SERVER_TARGET_BIT_SET(iTarget, ENEMY_PED_HELI_PILOT)
							serverBD.pilotPed = serverBD.Targets[iTarget].niPed
						ENDIF
															
						SET_BIT(serverBD.iTargetCreatedBitSet, iTarget)
						
						#IF IS_DEBUG_BUILD
							TEXT_LABEL_15 tl15Ped
							tl15Ped = "Ped "
							tl15Ped += iTarget
							SET_PED_NAME_DEBUG(NET_TO_PED(serverbd.Targets[iTarget].niPed), tl15Ped)
						#ENDIF
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - CREATE_TARGET  IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[iTarget].niPed) iTargetNum = ", iTarget)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC
/// PURPOSE:
///    Responsible for choosing a random location to spawn a ped at. If the location has already been used, it will choose another one.
/// PARAMS:
///    spawnLocation - 
///    spawnHeading - 
PROC CHOOSE_RANDOM_SPAWN_POSITION( VECTOR &spawnLocation, FLOAT &spawnHeading)
	INT iSpawnLocationID
	iSpawnLocationID = GET_RANDOM_INT_IN_RANGE(0, MAX_PED_WAVE_SPAWN_POINTS)
	IF !IS_BIT_SET(serverBD.iPedWavePedLocationUsedBitset, iSpawnLocationID)
		GB_GUNRUNNING_DEFEND_GET_PED_WAVE_SPAWN_COORD(serverBD.iBusinessToDefend, iSpawnLocationID, spawnLocation, spawnHeading)
		SET_BIT(serverBD.iPedWavePedLocationUsedBitset, iSpawnLocationID)
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND [CHOOSE_RANDOM_SPAWN_POSITION] Spawn location valid! iSpawnLocationID = ", iSpawnLocationID)
		
	ELSE
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND [CHOOSE_RANDOM_SPAWN_POSITION] Spawn location already in use. iSpawnLocationID = ", iSpawnLocationID)
		CHOOSE_RANDOM_SPAWN_POSITION(spawnLocation, spawnHeading)
	ENDIF
	 
ENDPROC

/// PURPOSE:
///    Responsible for creating a wave of peds on each first defusal attempt of each bomb.
/// PARAMS:
///    
PROC MAINTAIN_SPAWN_PED_WAVE( MODEL_NAMES iPedModel)


	// if no first attempt of each bomb defusal has been made, exit.
	//otherwise, spawn a wave of peds.
	IF !IS_SERVER_BIT_SET(biS_FirstBombDefusalInitiated)
	//	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_SPAWN_PED_WAVE] biS_FirstBombDefusalInitiated BIT NOT SET")
		EXIT
	ENDIF
	
	
	PRINTLN("JS-MAINTAIN_SPAWN_PED_WAVE SUCCESSFUL")
	
	INT i
	VECTOR spawnLocation
	FLOAT spawnHeading
	int multiplier = serverbd.currentAmbushWaveTally *PEDS_PER_WAVE
	i = GET_GB_GUNRUNNING_DEFEND_NUMBER_OF_INITIAL_PEDS_FOR_VARIATION(serverBD.iVariation, serverBD.iBusinessToDefend) +  multiplier + currentNumberOfPedsSpawnedByWave
	//INT iOffset = GET_GB_GUNRUNNING_DEFEND_NUMBER_OF_INITIAL_PEDS_FOR_VARIATION(serverBD.iVariation, serverBD.iBusinessToDefend)
	//FOR i = iOffset + multiplier TO (iOffset + PEDS_PER_WAVE + multiplier  - 1)
		PRINTLN("JS-START SPAWN PED ", i)
		IF serverBd.iCurrentAmbushPedToSpawn = -1
		OR serverBd.iCurrentAmbushPedToSpawn = i
			PRINTLN("JS-BBB1")
//					//we're making this one:
			serverBd.iCurrentAmbushPedToSpawn = i
			
			// grab a place to spawn
			CHOOSE_RANDOM_SPAWN_POSITION(spawnLocation, spawnHeading)
			
			
//					
			//spawnLocation = GB_GUNRUNNING_DEFEND_GET_RANDOM_PED_SPAWN_COORD(GET_GB_GUNRUNNING_DEFEND_CONTRABAND_VEHICLE_LOCATION(serverbd.iVariation,serverbd.iBusinessToDefend))
			IF !IS_VECTOR_ZERO(spawnLocation)
				
				
				IF REQUEST_LOAD_MODEL(iPedModel)
					
//					// if spawn point is ok
					IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(spawnLocation, // VECTOR vPoint, 
																1.0, 						 // FLOAT fVehRadius = 6.0, 
																DEFAULT, 						 // FLOAT fPedRadius = 1.0, 
																DEFAULT,						 // FLOAT fObjRadius = 1.0,
																DEFAULT,						 // FLOAT fViewRadius = 5.0,
																FALSE,						 // BOOL bCheckThisPlayerSight = TRUE,
																FALSE,						 // BOOL bDoAnyPlayerSeePointCheck = TRUE,
																DEFAULT,						 // BOOL bCheckVisibilityForOwnTeam = TRUE,
																DEFAULT,						 // FLOAT fVisibleDistance = 120.0,
																DEFAULT, 						 // BOOL bIgnoreLocalPlayerChecks=FALSE, 
																DEFAULT,						 // INT iTeamToIgnore=-1,
																DEFAULT,							 // BOOL bDoFireCheck=TRUE,
																DEFAULT)						//	FLOAT fPlayerRadius = 0.0)

						serverbd.Targets[i].vSpawnCoord = spawnLocation
						serverbd.Targets[i].fSpawnHead = spawnHeading
						
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_SPAWN_PED_WAVE] spawnLocation = ", serverbd.Targets[i].vSpawnCoord)
						SET_SERVER_TARGET_BIT(i,ENEMY_IS_AMBUSH_PED)
						IF	CREATE_TARGET(i)
							PRINTLN("JS-BBB2")
							globalPedSpawnWaveVector = <<0,0,0>>
							currentNumberOfPedsSpawnedByWave++
						//iterator currentpedsmadeinwave
							serverBd.iCurrentAmbushPedToSpawn = -1
						ELSE
							PRINTLN("JS-BBB3")
						ENDIF
					ELSE
						PRINTLN("JS-BBB4")
						globalPedSpawnWaveVector = <<0,0,0>>
					ENDIF
				ENDIF
			ELSE
				
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_SPAWN_PED_WAVE] 1")
			ENDIF
		ENDIF
		//set a bit to tell the ped brain to pick up 
	//ENDFOR
	IF currentNumberOfPedsSpawnedByWave = PEDS_PER_WAVE
		//only clear and up the tally when when all 5 peds have spawned
		CLEAR_SERVER_BIT(biS_FirstBombDefusalInitiated)
		serverbd.currentAmbushWaveTally++
		currentNumberOfPedsSpawnedByWave = 0
	ENDIF

ENDPROC                                








/// PURPOSE:
///    Create an AI ped in the vehicle / seat specified
/// PARAMS:
///    iTarget - 
///    iEnemyVehicle - 
///    seatToUse - 
/// RETURNS:
///    
FUNC BOOL CREATE_TARGET_INSIDE_VEHICLE(INT iTarget, INT iEnemyVehicle, VEHICLE_SEAT seatToUse = VS_DRIVER)
	//MODEL_NAMES mVehToUse
	MODEL_NAMES mPedTToUSe = GET_GB_GUNRUNNING_DEFEND_PED_MODEL(serverBD.iVariation, serverBD.iBusinessToDefend, iTarget)

	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [CREATE_TARGET_INSIDE_VEHICLE] Called with iTarget = ", iTarget, " iEnemyVehicle = ", iEnemyVehicle, " seatToUse = ", ENUM_TO_INT(seatToUse))
	IF iEnemyVehicle > -1
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[iEnemyVehicle])		
			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[iTarget].niPed)
				IF CAN_REGISTER_MISSION_PEDS(1)
				//	mPedTToUSe = serverbd.Targets[iTarget].mPed
					
					IF REQUEST_LOAD_MODEL(mPedTToUSe)

						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - CREATE_TARGET  GOT SPAWN COORDS iTargetNum = ", iTarget, " vSpawn = ", serverbd.Targets[iTarget].vSpawnCoord, " fSpawn = ",serverbd.Targets[iTarget].fSpawnHead)
						
						
				//		IF CREATE_NET_PED(serverbd.Targets[iTarget].niPed, PEDTYPE_CRIMINAL, mPedTToUSe, serverbd.Targets[iTarget].vSpawnCoord, serverbd.Targets[iTarget].fSpawnHead)
						IF CREATE_NET_PED_IN_VEHICLE(serverbd.Targets[iTarget].niPed, GET_ENEMY_VEH_NI(iEnemyVehicle), PEDTYPE_CRIMINAL, mPedTToUSe, seatToUse)
							
							IF IS_SERVER_TARGET_BIT_SET(iTarget, ENEMY_PED_CUSTOM_VARIATION)
						//		SET_GB_GUNRUNNING_DEFEND_PED_COMPONENT_VARIATION(serverBD.iVariation, serverBD.iBusinessToDefend, iTarget)
							ENDIF
						
							SETUP_TARGET(iTarget)
							
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - CREATE_TARGET_INSIDE_VEHICLE ped = ", iTarget, " Veh = ", iEnemyVehicle)
							
							

																
							SET_BIT(serverBD.iTargetCreatedBitSet, iTarget)
							
							#IF IS_DEBUG_BUILD
								TEXT_LABEL_15 tl15Ped
								tl15Ped = "Ped "
								tl15Ped += iTarget
								SET_PED_NAME_DEBUG(NET_TO_PED(serverbd.Targets[iTarget].niPed), tl15Ped)
							#ENDIF
							
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF
		ELSE
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [CREATE_TARGET_INSIDE_VEHICLE] Waiting for enemy veh ", iEnemyVehicle)
		ENDIF
	ELSE
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - [CREATE_TARGET_INSIDE_VEHICLE] iEnemyVehicle <= -1 iEnemyVehicle = ", iEnemyVehicle)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_VECTOR_INFRONT_OF_COORDS_WITH_HEADING(VECTOR vStart, FLOAT fHeading, FLOAT fDistance = 2.0)

	    VECTOR vEnd 
        
        //end vector - pointing north 
        vEnd    = <<0.0, 1.0, 0.0>> 
        
        //point it in the same direction as the entity
        ROTATE_VECTOR_ABOUT_Z(vEnd, fHeading ) 

        //Make the normilised roted vector 300 times larger 
		
        vEnd.x *= fDistance 
        vEnd.y *= fDistance 
        vEnd.z *= fDistance 
        
        //add it on to the start vector to get the end vector coordinates 
        vEnd += vStart 
        
        RETURN vEnd 

ENDFUNC

PROC SET_GB_DEFEND_VEHICLE_COLOURS(VEHICLE_INDEX veh)
	MODEL_NAMES mVeh = GET_ENTITY_MODEL(veh)
	SWITCH mveh
		CASE SPEEDO	 SET_VEHICLE_COLOUR_COMBINATION(veh, 0) 	BREAK 
	ENDSWITCH

ENDPROC


/// PURPOSE:
///    Creates a vehicle with the model specified
/// PARAMS:
///    iVeh - 
///    modelToUse - 
///    bLockRearDoors - 
///    bUseExactCoords - 
/// RETURNS:
///    
FUNC BOOL CREATE_ENEMY_VEHICLE(INT iVeh, MODEL_NAMES modelToUse, BOOL bLockRearDoors = FALSE, BOOL bUseExactCoords = FALSE)
	VECTOR vSpawn
	FLOAT fHead
	
	///	If bUseExactCoords, then the coordinate to create the vehicle will already have been stored in serverBD.enemyVehSpawnCoords[iVeh].vLoc
	/// Alternatively, if it doesn't need to be exact coords, then need to find a suitable spawn location near the initial coords
	
	

	IF NOT DOES_ENEMY_VEH_NI_EXIST(iVeh)
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND [CREATE_ENEMY_VEHICLE] Called with iVeh = ", iVeh, " modelToUse = ", ENUM_TO_INT(modelToUse), " bLockRearDoors = ", bLockRearDoors, " bUseExactCoords = ", bUseExactCoords)
		IF CAN_REGISTER_MISSION_VEHICLES(1)
			IF REQUEST_LOAD_MODEL(modelToUse)
				IF NOT IS_SERVER_ENEMY_VEH_BIT_SET(iVeh, ENEMY_VEH_BIT_GOT_SPAWN_COORDS)
				AND NOT bUseExactCoords
					//-- Not suing exact coords, need to find spawn coords to use
					INIT_GB_GUNRUNNING_DEFEND_ENEMY_VEH_SPAWN_COORDS(serverBD.iVariation, serverBD.iBusinessToDefend, iVeh, vSpawn, fHead)
					
					VEHICLE_SPAWN_LOCATION_PARAMS Params
					Params.fMinDistFromCoords = 0.0 
					Params.bConsiderHighways= FALSE
					Params.fMaxDistance= 50.0
					Params.bConsiderOnlyActiveNodes= TRUE
					Params.bAvoidSpawningInExclusionZones= TRUE
					Params.bCheckEntityArea = TRUE
					Params.bUseExactCoordsIfPossible = TRUE
					VECTOR vSpawnHeading
					vSpawnHeading = GET_VECTOR_INFRONT_OF_COORDS_WITH_HEADING(vSpawn, fHead, 20.0)
					
					IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(vSpawn, 
																vSpawnHeading, 
																modelToUse, 
																FALSE, 
																serverbd.enemyVehSpawnCoords[iVeh].vLoc, 
																serverbd.enemyVehSpawnCoords[iVeh].fHead, 
																Params)
																
						
						SET_SERVER_ENEMY_VEH_BIT(iVeh, ENEMY_VEH_BIT_GOT_SPAWN_COORDS)
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND [CREATE_ENEMY_VEHICLE] Veh ", iVeh, " LOOKING FOR  SPAWN COORDS... FOUND LOC ", serverbd.enemyVehSpawnCoords[iVeh].vLoc ," HEADING ",serverbd.enemyVehSpawnCoords[iVeh].fHead, " vSpawnHeading = ", vSpawnHeading) 
					ELSE
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND [CREATE_ENEMY_VEHICLE] Veh ", iVeh, " waiting for spawn coords ")
					ENDIF
				ELSE
					//-- Using exact coords, store them in the server BD
					IF bUseExactCoords
						INIT_GB_GUNRUNNING_DEFEND_ENEMY_VEH_SPAWN_COORDS(serverBD.iVariation, serverBD.iBusinessToDefend, iVeh, vSpawn, fHead)
						serverBD.enemyVehSpawnCoords[iVeh].vLoc = vSpawn
					 	serverBD.enemyVehSpawnCoords[iVeh].fHead = fHead
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND [CREATE_ENEMY_VEHICLE] Veh ", iVeh, " using exact coords ", serverBD.enemyVehSpawnCoords[iVeh].vLoc, " and heading ", serverBD.enemyVehSpawnCoords[iVeh].fHead)
					ENDIF
				ENDIF
				
				IF IS_SERVER_ENEMY_VEH_BIT_SET(iVeh, ENEMY_VEH_BIT_GOT_SPAWN_COORDS)
				OR bUseExactCoords
					vSpawn 	= serverBD.enemyVehSpawnCoords[iVeh].vLoc
					fHead 	= serverBD.enemyVehSpawnCoords[iVeh].fHead
					IF NOT IS_BIT_SET(serverBD.iVehClearAreaBitset, iVeh)
						//CLEAR_AREA(vSpawn, 5.0, TRUE, DEFAULT, DEFAULT, TRUE)
						SET_BIT(serverBD.iVehClearAreaBitset, iVeh)
					ENDIF
					
					IF CREATE_NET_VEHICLE(serverBD.niVeh[iVeh], 
											modelToUse, 
											vSpawn,
											fHead)
									
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND [CREATE_ENEMY_VEHICLE] CREATED VEH ", iVeh, " AT LOC ", vSpawn, " Heading ", fHead)
						
						SET_ENTITY_CAN_ONLY_BE_DAMAGED_BY_SCRIPT_PARTICIPANTS(NET_TO_PED(serverBD.niVeh[iVeh]), TRUE)
						
						SET_GB_DEFEND_VEHICLE_COLOURS(NET_TO_VEH(serverBD.niVeh[iVeh]))
						
						SET_VEHICLE_TYRES_CAN_BURST(NET_TO_VEH(serverBD.niVeh[iVeh]), FALSE)
						SET_VEHICLE_AUTOMATICALLY_ATTACHES(NET_TO_VEH(serverBD.niVeh[iVeh]), FALSE)
						
					//    SET_VEHICLE_HAS_STRONG_AXLES(NET_TO_VEH(serverBD.niVeh[iVeh]), TRUE)
					  //  SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(NET_TO_VEH(serverBD.niVeh[iVeh]), FALSE)
						//SET_VEHICLE_STRONG(NET_TO_VEH(serverBD.niVeh[iVeh]), TRUE)
						//SET_VEHICLE_DAMAGE_SCALE(NET_TO_VEH(serverBD.niVeh[iVeh]), 0.08)
					//	SET_ENTITY_HEALTH(NET_TO_VEH(serverBD.niVeh[iVeh]), g_sMPTunables.iexec_contraband_buy_health)
						
						SET_VEHICLE_DOORS_LOCKED_FOR_NON_SCRIPT_PLAYERS(NET_TO_VEH(serverBD.niVeh[iVeh]), TRUE)
						SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.niVeh[iVeh]), TRUE)
						SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(NET_TO_VEH(serverBD.niVeh[iVeh]), TRUE) 
						
						SET_ALLOW_RAMMING_SOOP_OR_RAMP(NET_TO_VEH(serverBD.niVeh[iVeh]), FALSE) 
						
						IF IS_SERVER_ENEMY_VEH_BIT_SET(iVeh, ENEMY_VEH_BIT_OPEN_REAR_RIGHT_DOOR)
							SET_VEHICLE_DOOR_OPEN(NET_TO_VEH(serverBD.niVeh[iVeh]), SC_DOOR_REAR_RIGHT,FALSE, TRUE)
							SET_VEHICLE_DOORS_LOCKED(NET_TO_VEH(serverBD.niVeh[iVeh]), VEHICLELOCK_LOCKED)
							SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(NET_TO_VEH(serverBD.niVeh[iVeh]), SC_DOOR_REAR_LEFT, FALSE)
							SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(NET_TO_VEH(serverBD.niVeh[iVeh]), SC_DOOR_REAR_RIGHT, FALSE)
							PRINTLN("JS-PREVENT REAR RIGHT")
						ENDIF
						
						IF IS_SERVER_ENEMY_VEH_BIT_SET(iVeh, ENEMY_VEH_BIT_OPEN_REAR_LEFT_DOOR)
							SET_VEHICLE_DOOR_OPEN(NET_TO_VEH(serverBD.niVeh[iVeh]), SC_DOOR_REAR_LEFT,FALSE, TRUE)
							PRINTLN("JS-PREVENT REAR LEFT")
						ENDIF
						IF IS_SERVER_ENEMY_VEH_BIT_SET(iVeh, ENEMY_VEH_BIT_PREVENT_OPEN_BY_EXPLOSION)
							SET_OPEN_REAR_DOORS_ON_EXPLOSION(NET_TO_VEH(serverBD.niVeh[iVeh]), FALSE)
							
							PRINTLN("JS-PREVENT BLOW OPEN")
						ENDIF
						
						
						
						IF bLockRearDoors
							SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(NET_TO_VEH(serverBD.niVeh[iVeh]), ENUM_TO_INT(SC_DOOR_REAR_LEFT), VEHICLELOCK_LOCKED)
							SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(NET_TO_VEH(serverBD.niVeh[iVeh]), ENUM_TO_INT(SC_DOOR_REAR_RIGHT), VEHICLELOCK_LOCKED)
						ENDIF
						
						#IF IS_DEBUG_BUILD
						
							TEXT_LABEL_15 tl15Veh
							tl15Veh = "E Veh "
							tl15Veh += iVeh
							SET_VEHICLE_NAME_DEBUG(NET_TO_VEH(serverBD.niVeh[iVeh]), tl15Veh)
						
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF NOT DOES_ENEMY_VEH_NI_EXIST(iVeh)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC


/// PURPOSE:
///    Create the contraband vehicle to be recovered by the player's org.
/// PARAMS:
///    iVeh - 
///    modelToUse - 
///    bSkipSpawnCheck - 
/// RETURNS:
///    
FUNC BOOL CREATE_GB_GUNRUNNING_CONTRABAND_VEHICLE_AT_EXACT_COORDS(INT iVeh, MODEL_NAMES modelToUse, BOOL bSkipSpawnCheck = FALSE)
	VECTOR vSpawn
	FLOAT fHead
	BOOL bTryClearArea
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPlayerVeh[iVeh])
		IF CAN_REGISTER_MISSION_VEHICLES(1)
			IF REQUEST_LOAD_MODEL(modelToUse)
				INIT_GB_GUNRUNNING_DEFEND_CONTRABAND_VEH_SPAWN_COORDS(serverBD.iVariation, serverBD.iBusinessToDefend, iVeh, vSpawn, fHead)
				
				IF bSKipSpawnCheck
				OR IS_POINT_OK_FOR_NET_ENTITY_CREATION(	vSpawn, // VECTOR vPoint, 
														DEFAULT, 						 // FLOAT fVehRadius = 6.0, 
														DEFAULT, 						 // FLOAT fPedRadius = 1.0, 
														DEFAULT,						 // FLOAT fObjRadius = 1.0,
														DEFAULT,						 // FLOAT fViewRadius = 5.0,
														FALSE,						 	// BOOL bCheckThisPlayerSight = TRUE,
														FALSE,						 	// BOOL bDoAnyPlayerSeePointCheck = TRUE,
														DEFAULT,						 // BOOL bCheckVisibilityForOwnTeam = TRUE,
														DEFAULT,						 // FLOAT fVisibleDistance = 120.0,
														DEFAULT, 						 // BOOL bIgnoreLocalPlayerChecks=FALSE, 
														DEFAULT,						 // INT iTeamToIgnore=-1,
														DEFAULT,							 // BOOL bDoFireCheck=TRUE,
														DEFAULT)						//	FLOAT fPlayerRadius = 0.0
														
					IF CREATE_NET_VEHICLE(serverBD.niPlayerVeh[iVeh], 
															modelToUse, 
															vSpawn,
															fHead)
						
						
						SET_VEHICLE_TYRES_CAN_BURST(NET_TO_VEH(serverBD.niPlayerVeh[iVeh]), FALSE)
						SET_VEHICLE_AUTOMATICALLY_ATTACHES(NET_TO_VEH(serverBD.niPlayerVeh[iVeh]), FALSE)
						
//					    SET_VEHICLE_HAS_STRONG_AXLES(NET_TO_VEH(serverBD.niPlayerVeh[iVeh]), TRUE)
//					    SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(NET_TO_VEH(serverBD.niPlayerVeh[iVeh]), FALSE)
						SET_VEHICLE_STRONG(NET_TO_VEH(serverBD.niPlayerVeh[iVeh]), TRUE)
						SET_VEHICLE_DAMAGE_SCALE(NET_TO_VEH(serverBD.niPlayerVeh[iVeh]),1.0)
//						SET_ENTITY_HEALTH(NET_TO_VEH(serverBD.niPlayerVeh[iVeh]), g_sMPTunables.iexec_contraband_buy_health)
						SET_VEHICLE_DISABLE_TOWING(NET_TO_VEH(serverBD.niPlayerVeh[iVeh]), TRUE) 
						SET_VEHICLE_DOORS_LOCKED_FOR_NON_SCRIPT_PLAYERS(NET_TO_VEH(serverBD.niPlayerVeh[iVeh]), TRUE)
						SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.niPlayerVeh[iVeh]), TRUE)
						SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(NET_TO_VEH(serverBD.niPlayerVeh[iVeh]), TRUE) 
						
						SET_ALLOW_RAMMING_SOOP_OR_RAMP(NET_TO_VEH(serverBD.niPlayerVeh[iVeh]), FALSE) 
						IF serverBD.iVariation != GB_GUNRUNNING_DEFEND_VALKYRIE
							SET_VEHICLE_DOORS_LOCKED(NET_TO_VEH(serverBD.niPlayerVeh[iVeh]), VEHICLELOCK_LOCKED)
						ENDIF
						//set_vehi
						//SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(NET_TO_VEH(serverBD.niPlayerVeh[iVeh]),TRUE)
						IF IS_SERVER_PLAYER_VEH_BIT_SET(iVeh, PLAYER_VEH_BIT_OPEN_REAR_RIGHT_DOOR)
							SET_VEHICLE_DOOR_SHUT(NET_TO_VEH(serverBD.niPlayerVeh[iVeh]), SC_DOOR_REAR_RIGHT,TRUE)
							PRINTLN("JS-PREVENT REAR RIGHT PLAYER")
						ENDIF
						
						IF IS_SERVER_PLAYER_VEH_BIT_SET(iVeh, PLAYER_VEH_BIT_OPEN_REAR_LEFT_DOOR)
							SET_VEHICLE_DOOR_SHUT(NET_TO_VEH(serverBD.niPlayerVeh[iVeh]), SC_DOOR_REAR_LEFT,TRUE)
							PRINTLN("JS-PREVENT REAR LEFT PLAYER")
						ENDIF
						IF IS_SERVER_PLAYER_VEH_BIT_SET(iVeh, PLAYER_VEH_BIT_PREVENT_OPEN_BY_EXPLOSION)
							SET_OPEN_REAR_DOORS_ON_EXPLOSION(NET_TO_VEH(serverBD.niPlayerVeh[iVeh]), FALSE)
							PRINTLN("JS-PREVENT BLOW OPEN PLAYER")
						ENDIF
						#IF IS_DEBUG_BUILD
						
							TEXT_LABEL_15 tl15Veh
							tl15Veh = "P Veh "
							tl15Veh += iVeh
							SET_VEHICLE_NAME_DEBUG(NET_TO_VEH(serverBD.niPlayerVeh[iVeh]), tl15Veh)
							
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [CREATE_GB_GUNRUNNING_CONTRABAND_VEHICLE_AT_EXACT_COORDS] Created veh ", iVeh, " at coords ", vSpawn)
						#ENDIF
					ENDIF
				ELSE
					//-- Try clearing the area if we couldn't create
					
					IF NOT IS_SERVER_PLAYER_VEH_BIT_SET(iVeh, PLAYER_VEH_BIT_TRIED_CLEAR_AREA)
						IF IS_ANY_VEHICLE_NEAR_POINT( vSpawn, 6.0 )
							bTryClearArea = TRUE
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [CREATE_GB_GUNRUNNING_CONTRABAND_VEHICLE_AT_EXACT_COORDS]  FAILING ON IS_POINT_OK_FOR_NET_ENTITY_CREATION (IS_ANY_VEHICLE_NEAR_POINT) WILL TRY CLEAR AREA veh = ", iVeh, " Loc = ", vSpawn)
						ELIF IS_ANY_PED_NEAR_POINT( <<vSpawn.x, vSpawn.y, vSpawn.z + 1.0>>, 1.0) 
							OR IS_ANY_PED_NEAR_POINT( vSpawn, 1.0 )
							bTryClearArea = TRUE
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [CREATE_GB_GUNRUNNING_CONTRABAND_VEHICLE_AT_EXACT_COORDS]  FAILING ON IS_POINT_OK_FOR_NET_ENTITY_CREATION (IS_ANY_VEHICLE_NEAR_POINT) WILL TRY CLEAR AREA veh = ", iVeh, " Loc = ", vSpawn)
						ELSE
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [CREATE_GB_GUNRUNNING_CONTRABAND_VEHICLE_AT_EXACT_COORDS]  FAILING ON IS_POINT_OK_FOR_NET_ENTITY_CREATION Veh = ", iVeh, " Loc = ", vSpawn)
						ENDIF
						
						IF bTryClearArea
							CLEAR_AREA(vSpawn, fClearAreaRadius, TRUE, DEFAULT, DEFAULT, TRUE)
							SET_SERVER_PLAYER_VEH_BIT(iVeh, PLAYER_VEH_BIT_TRIED_CLEAR_AREA)
							
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [CREATE_GB_GUNRUNNING_CONTRABAND_VEHICLE_AT_EXACT_COORDS] Set ENEMY_VEH_BIT_TRIED_CLEAR_AREA at coord ", vspawn, " veh ", iVeh)
						ENDIF
					ELSE
					//	PRINTLN("[BIKER1]     ---------->  [BAD_DEAL_EXTRA_DEBUG] - [CREATE_BAD_DEAL_ENEMY_VEHICLE] Failing veh, ",index, " loc, ", vspawn, " already cleared area")
						
						#IF IS_DEBUG_BUILD
						g_SpawnData.bShowAdvancedSpew = TRUE
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


PROC CLEANUP_ALL_TARGET_PEDS()
	INT i
	INT iVariation = serverBD.iVariation

	INT iNumPeds = GET_GB_GUNRUNNING_DEFEND_NUMBER_OF_INITIAL_PEDS_FOR_VARIATION(iVariation, serverBD.iBusinessToDefend)
	
	REPEAT iNumPeds i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niPed)
			CLEANUP_NET_ID(serverbd.Targets[i].niPed)
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL ARE_ALL_TARGET_PEDS_CLEANED_UP()
	INT i
	INT iVariation = serverBD.iVariation
//	INT iBusiness = serverBD.iWarehouseToUse
	INT iNumPeds = GET_GB_GUNRUNNING_DEFEND_NUMBER_OF_INITIAL_PEDS_FOR_VARIATION(iVariation, serverBD.iBusinessToDefend)
	
	REPEAT iNumPeds i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niPed)
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND [ARE_ALL_TARGET_PEDS_CLEANED_UP] False as target ", i, " still  exists")
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC


/// PURPOSE:
///    Create all the entites for the variation
/// RETURNS:
///    
FUNC BOOL CREATE_ALL_TARGETS()
	INT i
	
	INT iVariation = serverBD.iVariation

	INT iNumPeds 		= GET_GB_GUNRUNNING_DEFEND_NUMBER_OF_INITIAL_PEDS_FOR_VARIATION(iVariation, serverBD.iBusinessToDefend)
	INT iNumEnemyVehs	= GET_GB_GUNRUNNING_DEFEND_NUMBER_OF_INITIAL_ENEMY_VEHS_FOR_VARIATION(iVariation, serverBD.iBusinessToDefend)
	INT iNumPlayerVehs 	= GET_GB_GUNRUNNING_DEFEND_NUMBER_OF_INITIAL_CONTRABAND_VEHS_FOR_VARIATION(iVariation, serverBD.iBusinessToDefend) 
	
	i = 0
	INT index
	MODEL_NAMES mVeh
	REPEAT iNumEnemyVehs i
		index = i
		mVeh = GET_GB_GUNRUNNING_DEFEND_ENEMY_VEH_MODEL(iVariation, serverBD.iBusinessToDefend, index)
		IF CREATE_ENEMY_VEHICLE(index, mVeh, DEFAULT, IS_SERVER_ENEMY_VEH_BIT_SET(index, ENEMY_VEH_BIT_USE_EXACT_COORDS))
		ENDIF
	ENDREPEAT
	i = 0
	REPEAT iNumPeds i
		IF IS_SERVER_TARGET_BIT_SET(i, ENEMY_PED_BIT_CREATE_ON_FOOT)
			CREATE_TARGET(i)
		ELIF IS_SERVER_TARGET_BIT_SET(i, ENEMY_PED_BIT_CREATE_AS_DRIVER) 
			CREATE_TARGET_INSIDE_VEHICLE(i, 
										GET_GB_GUNRUNNING_DEFEND_VEH_PED_SHOULD_USE(serverBD.iVariation, serverBD.iBusinessToDefend, i))
										
		ELIF IS_SERVER_TARGET_BIT_SET(i, ENEMY_PED_BIT_CREATE_AS_FPASS) 
			CREATE_TARGET_INSIDE_VEHICLE(i, 
										GET_GB_GUNRUNNING_DEFEND_VEH_PED_SHOULD_USE(serverBD.iVariation, serverBD.iBusinessToDefend, i),
										VS_FRONT_RIGHT)
		ELIF IS_SERVER_TARGET_BIT_SET(i, ENEMY_PED_BIT_CREATE_AS_RLPASS) 
			CREATE_TARGET_INSIDE_VEHICLE(i, 
										GET_GB_GUNRUNNING_DEFEND_VEH_PED_SHOULD_USE(serverBD.iVariation, serverBD.iBusinessToDefend, i),
										VS_BACK_LEFT)
		ELIF IS_SERVER_TARGET_BIT_SET(i, ENEMY_PED_BIT_CREATE_AS_RRPASS) 
			CREATE_TARGET_INSIDE_VEHICLE(i, 
										GET_GB_GUNRUNNING_DEFEND_VEH_PED_SHOULD_USE(serverBD.iVariation, serverBD.iBusinessToDefend, i),
										VS_BACK_RIGHT)
		ELIF IS_SERVER_TARGET_BIT_SET(i, ENEMY_PED_BIT_IS_VEHICLE_GUNNER)
			CREATE_TARGET_INSIDE_VEHICLE(i, 
										GET_GB_GUNRUNNING_DEFEND_VEH_PED_SHOULD_USE(serverBD.iVariation, serverBD.iBusinessToDefend, i),
										VS_BACK_LEFT)
		ELSE
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - CREATE_ALL_TARGETS Not creating target ped ", i, " because no spawn bit set")
		ENDIF
	ENDREPEAT
	
	i = 0
	REPEAT iNumPlayerVehs i
		CREATE_GB_GUNRUNNING_CONTRABAND_VEHICLE_AT_EXACT_COORDS(i, 
											GET_GB_GUNRUNNING_DEFEND_PLAYER_VEH_MODEL(serverBD.iVariation, serverBD.iBusinessToDefend, i),  
											TRUE)		
	ENDREPEAT
	
	
	i = 0
	REPEAT iNumEnemyVehs i
		index = i
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.niVeh[index])
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - CREATE_ALL_TARGETS THIS VEH DOESN'T EXIST ", index, " iNumEnemyVehs = ", iNumEnemyVehs)
			RETURN FALSE
		ENDIF
		
	ENDREPEAT
	
	i = 0
	REPEAT iNumPeds i
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niPed)
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - CREATE_ALL_TARGETS THIS TARGET DOESN'T EXIST ", i, " iNumPeds = ", iNumPeds)
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	i = 0
	REPEAT iNumPlayerVehs i
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPlayerVeh[i])
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND - CREATE_ALL_TARGETS THIS  player VEH DOESN'T EXIST ", i, " iNumPlayerVehs = ", iNumPlayerVehs)
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	INT iNumPackages = serverBD.iNumberOfPackages
	i = 0
	
	INT iPedToAttachTo
	//-- Packages - some variations may require the package to start attached to an AI ped, other variations
	//-- will require the package to spawn on the map somewhere
	
	REPEAT iNumPackages i
		iPedToAttachTo = GET_GB_GUNRUNNING_DEFEND_PED_TO_ATTACH_PACKAGE_TO(serverBD.iVariation, serverBD.iBusinessToDefend, i)
		IF iPedToAttachTo > -1
			IF NOT CREATE_GB_PACKAGE(serverBD.packagesServer, localPackages, serverBD.packageSpawnCoords[i].vLoc,  i, TRUE, serverBD.Targets[iPedToAttachTo].niPed)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - Waiting for package ", i, " iNumPackages = ", iNumPackages, " ped to attach = ", iPedToAttachTo)
				RETURN FALSE
			ENDIF
		ELSE
			IF NOT CREATE_GB_PACKAGE(serverBD.packagesServer, localPackages, serverBD.packageSpawnCoords[i].vLoc,  i, FALSE, NULL, TRUE)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - Waiting for package ", i, " iNumPackages = ", iNumPackages, " ped to attach = ", iPedToAttachTo)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	

	
	//-- Need to load the parachute falling anims and models
	IF serverBD.iVariation = GB_GUNRUNNING_DEFEND_VALKYRIE
		REQUEST_ANIM_DICT("P_cargo_chute_S")
		REQUEST_MODEL( GET_GB_GUNRUNNING_DEFEND_PACKAGE_MODEL(GB_GUNRUNNING_DEFEND_VALKYRIE, serverBD.iBusinessToDefend))
		REQUEST_MODEL(P_CARGO_CHUTE_S)
		
	ENDIF
	
//	IF serverBD.iVariation = GB_GUNRUNNING_DEFEND_FAKE_ID
//		SET_BIT(serverBD.packagesServer.iServerPackageBitset, SERVER_GB_PACKAGE_ALLOW_COLLECT)
//	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    The ambient vehicles (i.e. vehicles that aren't mission critical) don't necassarily have to be created at the start
///    as its not vital that they are created. We don't have to delay the start of the mission waiting for them to be created. 
///    Can just create them any time after the missions starts, as long as nobody has got close to the spawn location
PROC MAINTAIN_CREATE_GB_DEFEND_AMBIENT_VEHICLES()
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		EXIT
	ENDIF
	

	
	INT i
	INT iNumAmbVeh = GET_GB_GUNRUNNING_DEFEND_NUMBER_OF_INITIAL_AMBIENT_VEHS_FOR_VARIATION(serverBD.iVariation, serverBD.iBusinessToDefend)
	IF serverBD.iTargetKilledBitset != 0
		//-- Don't bother creating if a target ped has already been killed - player must be nearby
		
		#IF IS_DEBUG_BUILD
		REPEAT iNumAmbVeh i
			IF NOT ARE_VECTORS_EQUAL(serverBD.ambientVehSpawnCoords[i].vLoc, << 0.0, 0.0, 0.0>>)
				IF NOT ARE_VECTORS_EQUAL(serverBD.ambientVehSpawnCoords[i].vLoc, << 0.0, 0.0, 0.0>>)
					IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVehAmbient[i])
						PRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_CREATE_GB_DEFEND_AMBIENT_VEHICLES] NOT CREATING AMB VEH ", i, " AT COORDS ", serverBD.ambientVehSpawnCoords[i].vLoc, " because serverBD.iTargetKilledBitset != 0")
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		#ENDIF
		
		EXIT
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(serverBD.timeSpawnAmbVeh)
		START_NET_TIMER(serverBD.timeSpawnAmbVeh)
	ENDIF
	
	INT iVariation = GET_GB_DEFEND_VARIATION()
	INT iBusiness = serverBD.iBusinessToDefend
	
	MODEL_NAMES mAmbeientVeh  
	i = 0
	BOOL bTryClearArea
	IF NOT HAS_NET_TIMER_EXPIRED(serverBD.timeSpawnAmbVeh, 15000)
		REPEAT iNumAmbVeh i
			bTryClearArea = FALSE
			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVehAmbient[i])
				IF CAN_REGISTER_MISSION_VEHICLES(1)
					mAmbeientVeh = GET_GB_GUNRUNNING_DEFEND_ENEMY_VEH_MODEL(iVariation, iBusiness, i)
					IF REQUEST_LOAD_MODEL(mAmbeientVeh)
						IF NOT ARE_VECTORS_EQUAL(serverBD.ambientVehSpawnCoords[i].vLoc, << 0.0, 0.0, 0.0>>)
							IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(	serverBD.ambientVehSpawnCoords[i].vLoc)
							OR (i > 0 AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVehAmbient[i-1]))
								IF NOT IS_BIT_SET(serverBD.iVehClearAreaBitset, i)
									//CLEAR_AREA(serverBD.ambientVehSpawnCoords[i].vLoc, 5.0, TRUE, DEFAULT, DEFAULT, TRUE)
									SET_BIT(serverBD.iVehClearAreaBitset, i)
								ENDIF
				
								IF CREATE_NET_VEHICLE(serverBD.niVehAmbient[i], 
												mAmbeientVeh, 
												serverBD.ambientVehSpawnCoords[i].vLoc,
												serverBD.ambientVehSpawnCoords[i].fHead)
												IF serverbd.iVariation = GB_GUNRUNNING_DEFEND_DEFUSAL
													SET_VEHICLE_COLOURS(NET_TO_VEH(serverBD.niVehAmbient[i]), 0,0)
												ENDIF
												
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_CREATE_GB_DEFEND_AMBIENT_VEHICLES] CREATED AMBIENT VEH ", i, " AT LOC ", serverBD.ambientVehSpawnCoords[i].vLoc)
										
										SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(NET_TO_VEH(serverBD.niVehAmbient[i]), TRUE)
										
										SET_ALLOW_RAMMING_SOOP_OR_RAMP(NET_TO_VEH(serverBD.niVehAmbient[i]), FALSE)
										
								//		SET_GB_DEFEND_VEHICLE_COLOURS(NET_TO_VEH(serverBD.niVehAmbient[i]))
										#IF IS_DEBUG_BUILD
										TEXT_LABEL_15 tl15Veh
										tl15Veh = "Veh "
										tl15Veh += i
										
										SET_VEHICLE_NAME_DEBUG(NET_TO_VEH(serverBD.niVehAmbient[i]), tl15Veh)
									#ENDIF
								ENDIF
							ELSE
								
								IF IS_ANY_VEHICLE_NEAR_POINT( serverBD.ambientVehSpawnCoords[i].vLoc, 6.00 )	
									bTryClearArea = TRUE
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_CREATE_GB_DEFEND_AMBIENT_VEHICLES] FAILING ON IS_POINT_OK_FOR_NET_ENTITY_CREATION (IS_ANY_VEHICLE_NEAR_POINT) i =", i)
								ELIF IS_ANY_PED_NEAR_POINT( <<serverBD.ambientVehSpawnCoords[i].vLoc.x, serverBD.ambientVehSpawnCoords[i].vLoc.y, serverBD.ambientVehSpawnCoords[i].vLoc.z + 1.0>>, 1.0) 
								OR IS_ANY_PED_NEAR_POINT( serverBD.ambientVehSpawnCoords[i].vLoc, 1.0 )
									bTryClearArea = TRUE
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_CREATE_GB_DEFEND_AMBIENT_VEHICLES] FAILING ON IS_POINT_OK_FOR_NET_ENTITY_CREATION (IS_ANY_PED_NEAR_POINT) i =", i)
								ELSE
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_CREATE_GB_DEFEND_AMBIENT_VEHICLES] FAILING ON IS_POINT_OK_FOR_NET_ENTITY_CREATION (NOT DUE TO NEARBY PEDS OR CARS)i =", i)
								ENDIF
								
								IF bTryClearArea = TRUE
									IF NOT IS_BIT_SET(serverBD.iVehClearAreaBitset, i)
										//CLEAR_AREA(serverBD.ambientVehSpawnCoords[i].vLoc, 6.0, TRUE, DEFAULT, DEFAULT, TRUE)
										SET_BIT(serverBD.iVehClearAreaBitset, i)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
								
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

/// PURPOSE:
///    Create the props (crates etc) for the variation
PROC MAINTAIN_CREATE_GB_DEFEND_PROPS()
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		EXIT
	ENDIF

	INT i
	INT iNumProps = GET_GB_GUNRUNNING_DEFEND_NUMBER_OF_INITIAL_PROPS_FOR_VARIATION(serverBD.iVariation, serverBD.iBusinessToDefend)
	INT iVariation = serverBD.iVariation
	INT iBusiness = serverBD.iBusinessToDefend
	
	IF serverBD.iTargetKilledBitset != 0
		//-- Don't bother creating if a target ped has already been killed - player must be nearby
		
		#IF IS_DEBUG_BUILD
		REPEAT iNumProps i
			IF NOT ARE_VECTORS_EQUAL(serverBD.propSpawnCoords[i].vLoc, << 0.0, 0.0, 0.0>>)
				IF NOT ARE_VECTORS_EQUAL(serverBD.propSpawnCoords[i].vLoc, << 0.0, 0.0, 0.0>>)
					IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niProp[i])
						IF GET_FRAME_COUNT() % 500 = 0
							PRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_CREATE_GB_DEFEND_PROPS] NOT CREATING PROP ", i, " AT COORDS ", serverBD.propSpawnCoords[i].vLoc, " because serverBD.iTargetKilledBitset != 0")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		#ENDIF
		
		EXIT
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(serverBD.timeSpawnAmbVeh)
		START_NET_TIMER(serverBD.timeSpawnAmbVeh)
	ENDIF
	
	
	
	MODEL_NAMES mProp  
	i = 0
	BOOL bTryClearArea
	IF NOT HAS_NET_TIMER_EXPIRED(serverBD.timeSpawnAmbVeh, 15000)
		REPEAT iNumProps i
			bTryClearArea = FALSE
			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niProp[i])
				IF CAN_REGISTER_MISSION_OBJECTS(1)
					mProp = GET_GB_GUNRUNNING_DEFEND_PROP_MODEL(iVariation, iBusiness, i)
					IF REQUEST_LOAD_MODEL(mProp)
						IF NOT ARE_VECTORS_EQUAL(serverBD.propSpawnCoords[i].vLoc, << 0.0, 0.0, 0.0>>)
							IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(	serverBD.propSpawnCoords[i].vLoc)
							OR (i > 0 AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niProp[i-1]))
							
								IF NOT IS_BIT_SET(serverBD.iPropClearAreaBitset, i)
									IF mProp != INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_adv_hdsec"))
										//CLEAR_AREA(serverBD.propSpawnCoords[i].vLoc, 5.0, TRUE, DEFAULT, DEFAULT, TRUE)
									ENDIF
									SET_BIT(serverBD.iPropClearAreaBitset, i)
								ENDIF

								IF CREATE_NET_OBJ(serverBD.niProp[i], 
												mProp, 
												serverBD.propSpawnCoords[i].vLoc
												)
										
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_CREATE_GB_DEFEND_PROPS] CREATED PROP ", i, " AT LOC ", serverBD.propSpawnCoords[i].vLoc)
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_CREATE_GB_DEFEND_PROPS] CREATED PROP MODEL NAME ", mProp, " AT LOC ", serverBD.propSpawnCoords[i].vLoc)
									
									SET_ENTITY_ROTATION(NET_TO_OBJ(serverBD.niProp[i]), << 0.0, 0.0, serverBD.propSpawnCoords[i].fHead >>)
									
										IF IS_SERVER_PROP_BIT_SET(i, PROP_BIT_ATTACH_TO_VEH)
											IF !NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niProp[i])
												FREEZE_ENTITY_POSITION(NET_TO_OBJ(serverBD.niProp[i]), TRUE)
												SET_ENTITY_LOD_DIST(NET_TO_OBJ(serverBD.niProp[i]), 100)
												SET_ENTITY_VISIBLE(NET_TO_OBJ(serverBD.niProp[i]),TRUE)
												SET_ENTITY_INVINCIBLE(NET_TO_OBJ(serverBD.niProp[i]),TRUE)
												SET_ENTITY_PROOFS(NET_TO_OBJ(serverBD.niProp[i]),TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
												CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_CREATE_GB_DEFEND_PROPS] PROP ", i, " setup for attach")
											ENDIF
										ENDIF
										//
										IF GET_ENTITY_MODEL(NET_TO_OBJ(serverBD.niProp[i])) = prop_generator_03b
											FREEZE_ENTITY_POSITION(NET_TO_OBJ(serverBD.niProp[i]), TRUE)
										ENDIF
									IF IS_SERVER_PROP_BIT_SET(i, PROP_BIT_BOMB_DISARMED_PROP)
										SET_ENTITY_VISIBLE(NET_TO_OBJ(serverBD.niProp[i]),FALSE)
									ENDIF
								ENDIF
							ELSE
								
								IF IS_ANY_VEHICLE_NEAR_POINT( serverBD.propSpawnCoords[i].vLoc, 6.00 )	
									bTryClearArea = TRUE
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_CREATE_GB_DEFEND_PROPS] FAILING ON IS_POINT_OK_FOR_NET_ENTITY_CREATION (IS_ANY_VEHICLE_NEAR_POINT) i =", i)
								ELIF IS_ANY_PED_NEAR_POINT( <<serverBD.propSpawnCoords[i].vLoc.x, serverBD.propSpawnCoords[i].vLoc.y, serverBD.propSpawnCoords[i].vLoc.z + 1.0>>, 1.0) 
								OR IS_ANY_PED_NEAR_POINT( serverBD.propSpawnCoords[i].vLoc, 1.0 )
									bTryClearArea = TRUE
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_CREATE_GB_DEFEND_PROPS] FAILING ON IS_POINT_OK_FOR_NET_ENTITY_CREATION (IS_ANY_PED_NEAR_POINT) i =", i)
								ELSE
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_CREATE_GB_DEFEND_PROPS] FAILING ON IS_POINT_OK_FOR_NET_ENTITY_CREATION (NOT DUE TO NEARBY PEDS OR CARS)i =", i)
								ENDIF
								
								IF bTryClearArea = TRUE
									IF NOT IS_BIT_SET(serverBD.iPropClearAreaBitset, i)
										//CLEAR_AREA(serverBD.propSpawnCoords[i].vLoc, 6.0, TRUE, DEFAULT, DEFAULT, TRUE)
										SET_BIT(serverBD.iPropClearAreaBitset, i)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_CREATE_GB_DEFEND_PROPS] CANT LOAD MODEL : ", i)
					ENDIF
				ENDIF
								
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

//FUNC VECTOR CONVERT_ROTATION_TO_DIRECTION_VECTOR(VECTOR vRot)
//	RETURN <<-SIN(vRot.z) * COS(vRot.x), COS(vRot.z) * COS(vRot.x), SIN(vRot.x)>>
//ENDFUNC


/// PURPOSE:
///    Server side tracking of who is holding and who has delivered each package. Also controls when players are allowed to 
///    first collect a package
PROC MAINTAIN_GB_GUNRUNNING_DEFEND_PACKAGES_SERVER()
	

	MODEL_NAMES mPackage
	
	STRUCT_COLLECT_GB_PACKAGE_OPTIONS packageOptions
	IF serverBD.iVariation = GB_GUNRUNNING_DEFEND_VALKYRIE
		mPackage = GET_GB_GUNRUNNING_DEFEND_PACKAGE_MODEL(GB_GUNRUNNING_DEFEND_VALKYRIE, serverBD.iBusinessToDefend)
	//ELIF serverBD.iVariation = GB_GUNRUNNING_DEFEND_DEFUSAL
		//mPackage = GET_GB_GUNRUNNING_DEFEND_PACKAGE_MODEL(GB_GUNRUNNING_DEFEND_DEFUSAL, serverBD.iBusinessToDefend)
	ENDIF
	
	
	IF serverBD.iVariation = GB_GUNRUNNING_DEFEND_VALKYRIE
	//OR serverBD.iVariation = GB_GUNRUNNING_DEFEND_DEFUSAL
		
		
		IF serverBD.packagesServer.options.mPackage != mPackage
			serverBD.packagesServer.options.mPackage = mPackage
		ENDIF
		
		
		
		IF NOT IS_BIT_SET(serverBD.packagesServer.iServerPackageBitset, SERVER_GB_PACKAGE_ALLOW_COLLECT)
			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.packagesServer.niPackage[0])
				IF ARE_VECTORS_EQUAL(serverBD.packageSpawnCoords[0].vLoc, << 0.0, 0.0, 0.0>>)
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPlayerVeh[0])
						IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.niPlayerVeh[0])
							serverBD.iNumberOfPackages = 1
							serverBD.packageSpawnCoords[0].vLoc = GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niPlayerVeh[0]))
							serverBD.packageSpawnCoords[0].vLoc.z += 1.0
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_GB_GUNRUNNING_DEFEND_PACKAGES_SERVER] Got package spawn coords for package 0 = ", serverBD.packageSpawnCoords[0].vLoc)
						ENDIF
					ENDIF
				ENDIF
						
				IF NOT ARE_VECTORS_EQUAL(serverBD.packageSpawnCoords[0].vLoc, << 0.0, 0.0, 0.0>>)
					REQUEST_ANIM_DICT("P_cargo_chute_S")
					IF CREATE_GB_PACKAGE_AS_DROPPED_CRATE(serverBD.packagesServer, serverBD.packageSpawnCoords[0].vLoc, 0)
						SET_BIT(serverBD.packagesServer.iServerPackageBitset, SERVER_GB_PACKAGE_ALLOW_COLLECT)
						IF NOT IS_SERVER_BIT_SET(biS_ApproachedWarehouse)
							SET_SERVER_BIT(biS_ApproachedWarehouse)
						ENDIF
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_GB_GUNRUNNING_DEFEND_PACKAGES_SERVER] Set SERVER_GB_PACKAGE_ALLOW_COLLECT")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF serverBD.iNumberOfPackages > 0
			packageOptions.iNumberOfPackages 					= serverBD.iNumberOfPackages
			packageOptions.spritePackage						= RADAR_TRACE_SUPPLIES
			packageOptions.vDropOff								= GET_DROP_OFF_LOCATION()
			packageOptions.iBlipColourFriendlyPlayerWithPackage	= BLIP_COLOUR_GREEN
			packageOptions.bDisableGps							= TRUE 
			
			MAINTAIN_COLLECT_GB_PACKAGE_SERVER(serverBD.packagesServer, packageOptions)
			
			IF NOT IS_SERVER_BIT_SET(biS_AllPackagesDelivered)
				IF IS_BIT_SET(serverBD.packagesServer.iServerPackageBitset, SERVER_GB_PACKAGE_ALLOW_COLLECT)
					IF HAVE_ALL_GB_PACKAGES_BEEN_DELIVERED(serverBD.packagesServer)
						SET_SERVER_BIT(biS_AllPackagesDelivered)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [MAINTAIN_GB_GUNRUNNING_DEFEND_PACKAGES_SERVER] SET biS_AllPackagesDelivered")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
	ENDIF
ENDPROC

/// PURPOSE:
///    Calculate the perscentage of product to remove from the business, expressed as a value between 0.0 and 1.0
/// RETURNS:
///    
PROC CALCULATE_AMOUNT_OF_PRODUCT_TO_LOSE()
	
	
//	FLOAT fMaxValue = TO_FLOAT(GET_GB_GB_GUNRUNNING_DEFEND_DURATION())
//	FLOAT fCurrentValue
	
//	INT iNumGangDelivered
//	INT iTotalNumberOfPackages
//	INT iTimerValueWhenReachedArea
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF serverBD.fAmountOfProductToLose = -1.0
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [CHECK_FOR_REWARD] [CALCULATE_AMOUNT_OF_PRODUCT_TO_LOSE] Called......  variation = ", serverBD.iVariation)
			
			IF IS_SERVER_BIT_SET(biS_TimeoutExpired)
			OR IS_SERVER_BIT_SET(biS_DurationExpired)
				//-- Lose 100% because mission 30 min timeout expired
				SWITCH serverBD.iVariation
					CASE GB_GUNRUNNING_DEFEND_VALKYRIE
						serverBD.fAmountOfProductToLose = g_sMPTunables.FGR_DEFEND_VALKYRIE_TAKEN_ON_FAILB_PERCENTAGE                         
					BREAK
					CASE GB_GUNRUNNING_DEFEND_DEFUSAL
						serverBD.fAmountOfProductToLose = g_sMPTunables.FGR_DEFEND_DISARM_BOMBS_TAKEN_ON_FAIL                                 
					BREAK
				ENDSWITCH
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [CHECK_FOR_REWARD] [CALCULATE_AMOUNT_OF_PRODUCT_TO_LOSE] serverBD.fAmountOfProductToLose = 1.0 because biS_TimeoutExpired or biS_DurationExpired is set")
				EXIT
			ENDIF
			
			IF IS_SERVER_BIT_SET(biS_BossLaunchedQuit)
				//-- Lose 100% because the boss quit the mission
				SWITCH serverBD.iVariation
					CASE GB_GUNRUNNING_DEFEND_VALKYRIE
						serverBD.fAmountOfProductToLose = g_sMPTunables.FGR_DEFEND_VALKYRIE_TAKEN_ON_FAILB_PERCENTAGE                         
					BREAK
					CASE GB_GUNRUNNING_DEFEND_DEFUSAL
						serverBD.fAmountOfProductToLose = g_sMPTunables.FGR_DEFEND_DISARM_BOMBS_TAKEN_ON_FAIL                                 
					BREAK
				ENDSWITCH
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [CHECK_FOR_REWARD] [CALCULATE_AMOUNT_OF_PRODUCT_TO_LOSE] serverBD.fAmountOfProductToLose = 1.0 because biS_BossLaunchedQuit is set")
				EXIT
			ENDIF
			
			IF IS_SERVER_BIT_SET(biS_ContrabandWrecked)
				//-- Lose 100% because the contraband got wrecked
				SWITCH serverBD.iVariation
					CASE GB_GUNRUNNING_DEFEND_VALKYRIE
						serverBD.fAmountOfProductToLose = g_sMPTunables.FGR_DEFEND_VALKYRIE_TAKEN_ON_FAILB_PERCENTAGE                         
					BREAK
					CASE GB_GUNRUNNING_DEFEND_DEFUSAL
						serverBD.fAmountOfProductToLose = g_sMPTunables.FGR_DEFEND_DISARM_BOMBS_TAKEN_ON_FAIL                                 
					BREAK
				ENDSWITCH
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [CHECK_FOR_REWARD] [CALCULATE_AMOUNT_OF_PRODUCT_TO_LOSE] serverBD.fAmountOfProductToLose = 1.0 because biS_ContrabandWrecked is set")
				EXIT
			ENDIF
			
			IF IS_SERVER_BIT_SET(biS_ContrabandLost)
				//-- Lose 100% because the contraband was lost
				SWITCH serverBD.iVariation
					CASE GB_GUNRUNNING_DEFEND_VALKYRIE
						serverBD.fAmountOfProductToLose = g_sMPTunables.FGR_DEFEND_VALKYRIE_TAKEN_ON_FAILB_PERCENTAGE                         
					BREAK
					CASE GB_GUNRUNNING_DEFEND_DEFUSAL
						serverBD.fAmountOfProductToLose = g_sMPTunables.FGR_DEFEND_DISARM_BOMBS_TAKEN_ON_FAIL                                 
					BREAK
				ENDSWITCH
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [CHECK_FOR_REWARD] [CALCULATE_AMOUNT_OF_PRODUCT_TO_LOSE] serverBD.fAmountOfProductToLose = 1.0 because biS_ContrabandLost is set")
				EXIT
			ENDIF
			
			
			IF IS_SERVER_BIT_SET(biS_VehDelivered)
				//-- Lose 0% because the player delivered the vehicle
				serverBD.fAmountOfProductToLose = 0.0
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [CHECK_FOR_REWARD] [CALCULATE_AMOUNT_OF_PRODUCT_TO_LOSE] serverBD.fAmountOfProductToLose = 1.0 because biS_VehDelivered is set")
				EXIT
			ENDIF
			
			SWITCH serverBD.iVariation
				CASE GB_GUNRUNNING_DEFEND_VALKYRIE
				
					IF IS_SERVER_BIT_SET(biS_AllPackagesDelivered)
						//-- Lose 50% because the player only managed to return the care package
						serverBD.fAmountOfProductToLose = g_sMPTunables.FGR_DEFEND_VALKYRIE_TAKEN_ON_PARTIAL_FAIL
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [CHECK_FOR_REWARD] [CALCULATE_AMOUNT_OF_PRODUCT_TO_LOSE] serverBD.fAmountOfProductToLose = 0.5 because biS_AllPackagesDelivered is set")
						EXIT
					ENDIF
//					fCurrentValue = TO_FLOAT(serverBD.iMeterValueWhenInitialAllKilled - (60000 * serverBD.iKilledByGangChaseCount))
//					IF IS_BIT_SET(serverBD.iServerBitSet, biS_DurationExpired)
//					OR fCurrentValue <= 0
//						//-- Got killed too many times by gang chase
//						serverBD.fAmountOfProductToLose = 1.0
//						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [CHECK_FOR_REWARD] [CALCULATE_AMOUNT_OF_PRODUCT_TO_LOSE] -GB_GUNRUNNING_DEFEND_DEFUSAL- serverBD.fAmountOfProductToLose = 1.0 because  biS_DurationExpired is set")
//					ELSE
//						serverBD.fAmountOfProductToLose = 1 - (fCurrentValue / fMaxValue)
//						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [CHECK_FOR_REWARD] [CALCULATE_AMOUNT_OF_PRODUCT_TO_LOSE] -GB_GUNRUNNING_DEFEND_DEFUSAL- serverBD.fAmountOfProductToLose = ", serverBD.fAmountOfProductToLose)
//						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [CHECK_FOR_REWARD] [CALCULATE_AMOUNT_OF_PRODUCT_TO_LOSE] -GB_GUNRUNNING_DEFEND_DEFUSAL- fCurrentValue = ", fCurrentValue)
//						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [CHECK_FOR_REWARD] [CALCULATE_AMOUNT_OF_PRODUCT_TO_LOSE] -GB_GUNRUNNING_DEFEND_DEFUSAL- fMaxValue = ", fMaxValue)
//						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [CHECK_FOR_REWARD] [CALCULATE_AMOUNT_OF_PRODUCT_TO_LOSE] -GB_GUNRUNNING_DEFEND_DEFUSAL- serverBD.iKilledByGangChaseCoun = ", serverBD.iKilledByGangChaseCount)
//						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [CHECK_FOR_REWARD] [CALCULATE_AMOUNT_OF_PRODUCT_TO_LOSE] -GB_GUNRUNNING_DEFEND_DEFUSAL- serverBD.iMeterValueWhenInitialAllKilled  = ", serverBD.iMeterValueWhenInitialAllKilled )
//					ENDIF
//TODO: Depending on how you lose, lose a certain amount:
//50% or 100%
					//serverBD.fAmountOfProductToLose = 0.5
					
				BREAK
				
			
			ENDSWITCH
			
			
		ENDIF
	ENDIF
ENDPROC

INT iDeleteHeliStage
PROC SERVER_CLEANUP_HELI()
	IF IS_SERVER_BIT_SET(biS_CleanupHeli)	
	AND IS_SERVER_BIT_SET(biS_VehDelivered)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPlayerVeh[0])
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niPlayerVeh[0])
				BOOL bHasControl
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niPlayerVeh[0])
					PRINTLN("     ---------->     GB_GUNRUNNING_DEFEND - SCRIPT_EVENT_OK_TO_CLEANUP_DELIVERABLE_ENTITY, bHasControl =1 TRUE ")
					bHasControl = TRUE
				ELSE
					IF TAKE_CONTROL_OF_NET_ID(serverBD.niPlayerVeh[0])
						PRINTLN("     ---------->     GB_GUNRUNNING_DEFEND - SCRIPT_EVENT_OK_TO_CLEANUP_DELIVERABLE_ENTITY, bHasControl =2 TRUE ")
						bHasControl = TRUE
					ENDIF
				ENDIF
				
				IF bHasControl
					SWITCH iDeleteHeliStage
						CASE 0
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPlayerVeh[0])
								NETWORK_FADE_OUT_ENTITY(NET_TO_ENT(serverBD.niPlayerVeh[0]), TRUE, TRUE)
								PRINTLN("     ---------->     GB_GUNRUNNING_DEFEND - SCRIPT_EVENT_OK_TO_CLEANUP_DELIVERABLE_ENTITY, NETWORK_FADE_OUT_ENTITY ")
							ENDIF
							
							iDeleteHeliStage++
						BREAK
						
						CASE 1
							IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.niPlayerVeh[0]))
							AND NOT IS_ENTITY_VISIBLE(NET_TO_ENT(serverBD.niPlayerVeh[0]))
								PRINTLN("     ---------->     GB_GUNRUNNING_DEFEND - SCRIPT_EVENT_OK_TO_CLEANUP_DELIVERABLE_ENTITY, DELETE_NET_ID ")
								DELETE_NET_ID(serverBD.niPlayerVeh[0])
							ENDIF
						BREAK
				
					ENDSWITCH
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Process the stages for the Server
PROC PROCESS_GB_GUNRUNNING_DEFEND_SERVER()
	
//300000

	IF !IS_SERVER_BIT_SET(biS_MissionStarted)
		SET_SERVER_BIT(biS_MissionStarted)
	ENDIF

	IF NOT HAS_NET_TIMER_STARTED(serverBD.pilotTimer)
		START_NET_TIMER(serverBD.pilotTimer)
		PRINTLN("JS-PILOT TIME UP, START_NET_TIMER(serverBD.pilotTimer) ")
	ELSE
		IF HAS_NET_TIMER_EXPIRED(serverBD.pilotTimer, pilotTimerThreshold)
			SET_SERVER_BIT(biS_PilotTimerUp)
			PRINTLN("JS-PILOT TIME UP, biS_PilotTimerUp")
		ENDIF
	ENDIF
	
	
	IF NOT HAS_NET_TIMER_STARTED(serverBD.gangChaseTimer)
		SWITCH serverBD.iVariation
			CASE GB_GUNRUNNING_DEFEND_VALKYRIE
				IF serverBD.packagesServer.iCurrentpackageHolder[0] != -1
					START_NET_TIMER(serverBD.gangChaseTimer)
					PRINTLN("JS-DELAY TIME UP, START_NET_TIMER(serverBD.gangChaseTimer)-package picked up ")
				ENDIF
			BREAK
		ENDSWITCH
		
		IF serverBD.iPartDrivingContraVehicle != -1
			START_NET_TIMER(serverBD.gangChaseTimer)
			PRINTLN("JS-DELAY TIME UP, START_NET_TIMER(serverBD.gangChaseTimer) got in vehicle ")
		ENDIF
	ELSE
		IF HAS_NET_TIMER_EXPIRED(serverBD.gangChaseTimer, gangChaseDelay)
			SET_SERVER_BIT(biS_GangChaseDelayTimerUp)
			PRINTLN("JS-DELAY TIME UP, biS_PilotTimerUp")
		ENDIF
	ENDIF
	
//start_net_timer
//has_net_timer_started
//has net timer expired(timer,ms e.g. 5s = 5000
//reset net timer.

	MAINTAIN_GANG_CHASE_SERVER(serverBD.gangChaseServer, gangChaseOptions)
	//new variables from chris
//	gangChaseOptions.mGangChasePedCustom[0] = INT_TO_ENUM(MODEL_NAMES, HASH("g_m_importexport_01"))
//	gangChaseOptions.weaponGangChasePed = WEAPONTYPE_PISTOL
//    gangChaseOptions.iPedAccuracy = 5          
//    gangChaseOptions.niVehToAttack = serverBD.niDropOffVeh[iVeh]            
//    gangChaseOptions.vDropffCoord = vDROP_COORDS(serverBD.iDrop)

	

	
	SWITCH serverBD.eStage
		CASE eAD_TARGETS
			
			
	
			SWITCH serverbd.iVariation
				CASE GB_GUNRUNNING_DEFEND_DEFUSAL
					// if  we need to create an ambush on the defuser
					IF IS_BIT_SET(serverbd.iCanSpawnDefusalWave, 0)
					OR IS_BIT_SET(serverbd.iCanSpawnDefusalWave, 1)
					OR IS_BIT_SET(serverbd.iCanSpawnDefusalWave, 2)
						MAINTAIN_SPAWN_PED_WAVE( MP_G_M_PROS_01)
					ENDIF
				BREAK
			ENDSWITCH
			MAINTAIN_MISSION_DURATION_SERVER()
			
			MAINTAIN_MISSION_TIMEOUT_SERVER()
			

			
			IF HAS_MISSION_END_CONDITION_BEEN_MET()
				CALCULATE_AMOUNT_OF_PRODUCT_TO_LOSE()
			ENDIF
			
			IF IS_SERVER_BIT_SET( biS_AllTargetsKilled)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [PROCESS_GB_GUNRUNNING_DEFEND_SERVER] serverBD.eStage eAD_TARGETS -> eAD_OFF_MISSION AS biS_AllTargetsKilled")
				serverBD.eStage = eAD_OFF_MISSION 
			ELIF IS_SERVER_BIT_SET(biS_DurationExpired)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [PROCESS_GB_GUNRUNNING_DEFEND_SERVER] serverBD.eStage eAD_TARGETS -> eAD_OFF_MISSION AS biS_DurationExpired")
				serverBD.eStage = eAD_OFF_MISSION 
			ELIF IS_SERVER_BIT_SET(biS_VehDelivered)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [PROCESS_GB_GUNRUNNING_DEFEND_SERVER] serverBD.eStage eAD_TARGETS -> eAD_OFF_MISSION AS biS_VehDelivered")
				serverBD.eStage = eAD_OFF_MISSION 
			ENDIF
		BREAK

		
		CASE eAD_OFF_MISSION
		BREAK

		CASE eAD_CLEANUP
		BREAK
	ENDSWITCH
ENDPROC

PROC SOUND_INIT()
	INT iLoop
	REPEAT MAX_SOUNDS iLoop
		sHacking[iLoop] = -1 
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND [SOUND_INIT] ")
	ENDREPEAT
ENDPROC

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                      //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT(MP_MISSION_DATA missionScriptArgs)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
		#IF IS_DEBUG_BUILD	
			CPRINTLN(DEBUG_NET_MAGNATE, "MP: Starting mission ") NET_NL()
		#ENDIF
		
		// Vehry out all the initial game starting duties. 
		PROCESS_PRE_GAME(missionScriptArgs)	
		
		// Setup widgets. Make sure this is done after PROCESS_PRE_GAME()
		#IF IS_DEBUG_BUILD
			CREATE_WIDGETS()
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD NET_LOG("BIKER_CONTRA_DEFEND -  NETWORK_IS_GAME_IN_PROGRESS = FALSE")	#ENDIF
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  MISSION END - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP P     <----------     ") NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
	
	FREEMODE_DELIVERY_MISSION_INIT(serverBD.sMissionID)
	
	// Main loop
	WHILE TRUE

		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
				
		// If we have a match end event, bail
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			#IF IS_DEBUG_BUILD NET_LOG("SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")	#ENDIF
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  MISSION END - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE - SCRIPT CLEANUP B     <----------     ") NET_NL()
			SCRIPT_CLEANUP()

		ENDIF

		
		// Deal with the debug
		#IF IS_DEBUG_BUILD		
			UPDATE_WIDGETS()
			
			MAINTAIN_DEBUG_KEYS()
		#ENDIF		
	
		// If we need to leave the mission, make sure we just straight to the end stage and terminate the script - DON'T NEED FOR AMBIENT MISSION
		/*IF SHOULD_PLAYER_LEAVE_MP_MISSION() 
			#IF IS_DEBUG_BUILD
			NET_PRINT_TIME() NET_PRINT( GET_MP_MISSION_NAME(thisMission), " Leave mission = TRUE \n")
			#ENDIF
			playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_LEAVE
			#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_LEAVE")	#ENDIF
		ENDIF*/
		
		g_sGb_Telemetry_data.sdata.m_match1 = serverBD.iMatchId1
		g_sGb_Telemetry_data.sdata.m_match2 = serverBD.iMatchId2
		
		MAINTAIN_MAX_PLAYER_CHECKS_CLIENT()
		
		// -----------------------------------
		// Process your game logic.....
		SWITCH GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT())
			
			// Wait untill the server gives the all go before moving on
			CASE GAME_STATE_INI
				IF REQUEST_SCRIPT_AUDIO_BANK("ALARM_BELL_02")
					IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING

						GET_MY_BOSS_PARTICPANT_ID()
						
						
						IF DID_MY_BOSS_LAUNCH_GB_GB_GUNRUNNING_DEFEND()
							GB_COMMON_BOSS_MISSION_SETUP(FMMC_TYPE_GUNRUNNING_DEFEND,DEFAULT,GET_GB_DEFEND_VARIATION())

							

							relMyFmGroup = GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID())
						//	SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), relDefendPlayer)
							
							
							INIT_MY_GANG_COLOUR()
							SOUND_INIT()
							
							
						//	GB_SET_CONTRABAND_VARIATION_LOCAL_PLAYER_IS_ON(serverBD.iVariation)
							
						//	GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
							
							DISABLE_LOCAL_PLAYER_GB_PACKAGE_HUD()
							
							//iLocalPackageBlipAlpha[0] = -1
							//iLocalPackageBlipAlpha[1] = -1
							
							IF serverBD.iVariation = GB_GUNRUNNING_DEFEND_VALKYRIE
								REQUEST_ANIM_DICT("P_cargo_chute_S")
									
							ENDIF
							
							
						ELSE
							GB_COMMON_BOSS_MISSION_SETUP(FMMC_TYPE_GUNRUNNING_DEFEND, FALSE,GET_GB_DEFEND_VARIATION())
							
							
						ENDIF
						

						
						playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING    <----------     ") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_RUNNING")	#ENDIF

					ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
						playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 1    <----------     ") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY")	#ENDIF
					ENDIF
				ELSE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND Waiting for REQUEST_SCRIPT_AUDIO_BANK")
				ENDIF
				
			//	CHECK_FOR_REWARD()
			BREAK
			
			// Main gameplay state.
			CASE GAME_STATE_RUNNING
				IF GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION()
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_LEAVE
				ENDIF
			
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					PROCESS_TARGET_PED_BODY()

					PROCESS_GB_GUNRUNNING_DEFEND_CLIENT()

					PROCESS_EVENTS()
					
					IF DID_MY_BOSS_LAUNCH_GB_GB_GUNRUNNING_DEFEND()

						DRAW_CHALLENGE_DPAD_LBD(g_GBLeaderboardStruct.challengeLbdStruct, g_GBLeaderboardStruct.siDpadMovie, SUB_BOSS, g_GBLeaderboardStruct.dpadVariables, 
												g_GBLeaderboardStruct.fmDpadStruct)
					ENDIF
					
					
					MAINTAIN_DISTANCE_CHECKS_FOR_VARIATION()
					
					
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 2   <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY")	#ENDIF
				ENDIF
				
				MAINTAIN_GB_GUNRUNNING_DEFEND_MUSIC()
			BREAK
			
			CASE GAME_STATE_TERMINATE_DELAY 
				SET_MISSION_FINISHED(serverBD.MissionTerminateDelayTimer)
		      	IF IS_MISSION_READY_TO_CLEANUP(serverBD.MissionTerminateDelayTimer)
		            playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 3    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 3")	#ENDIF
		      	ENDIF
			BREAK
			
			// The game stage the local player is placed when we want him to leave a mission
			CASE GAME_STATE_LEAVE
				//LEAVE_MY_MP_MISSION(thisMission)
				playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 2    <----------     ") NET_NL()
				#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 2")	#ENDIF
			FALLTHRU
			
			//Cleans up then terminates the mission
			CASE GAME_STATE_END
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  SCRIPT CLEANUP A     <----------     ") NET_NL()
				SCRIPT_CLEANUP()
			BREAK

		ENDSWITCH
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			MAINTAIN_GB_GUNRUNNING_DEFEND_PACKAGES_SERVER()
			
			SWITCH GET_SERVER_MISSION_STATE()
				
				// For CnC group missions, we may want to wait for all players to have launched the script
				CASE GAME_STATE_INI
					IF GET_GB_DEFEND_VARIATION() > -1	
						IF serverBD.iBossPlayerWhoLaunched = -1
							serverBD.iBossPartWhoLaunched = PARTICIPANT_ID_TO_INT()
							serverBD.iBossPlayerWhoLaunched = NATIVE_TO_INT(PLAYER_ID())
						ENDIF
						
						INIT_SPAWN_COORDS()
						
						SET_TIME_BEFORE_LOSE_CONTRABAND()
						
						BLOCK_WAREHOUSE_SCENARIOS()
						
						
						MAINTAIN_CREATE_GB_DEFEND_AMBIENT_VEHICLES()
						
						MAINTAIN_CREATE_GB_DEFEND_PROPS()
						
						INITIALISE_PED_REACTION_TIMES()
						
						
						IF CREATE_ALL_TARGETS()
							PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBD.iMatchId1, serverBD.iMatchId2)
							SETUP_BLOCKING_ZONES()
							serverBD.iServerGameState = GAME_STATE_RUNNING
							
							GB_COMMON_BOSS_MISSION_SETUP(FMMC_TYPE_GUNRUNNING_DEFEND,DEFAULT,GET_GB_DEFEND_VARIATION())
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  serverBD.iServerGameState = GAME_STATE_RUNNING    <---------- ")
							#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_RUNNING")	#ENDIF
						ELSE
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND - WAITING FOR CREATE_ALL_TARGETS")
						ENDIF
					ENDIF
					
					MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
				BREAK
				
				// Look for game end conditions
				CASE GAME_STATE_RUNNING
					
					MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
					
					MAINTAIN_CREATE_GB_DEFEND_AMBIENT_VEHICLES()
					
					MAINTAIN_CREATE_GB_DEFEND_PROPS()
					
					PROCESS_TARGET_PED_BRAIN()
					
					PROCESS_GB_GUNRUNNING_DEFEND_SERVER()
					
					SERVER_CLEANUP_HELI()
							
					#IF IS_DEBUG_BUILD
						IF bHostEndMissionNow
						OR MPGlobalsAmbience.bKillActiveEvent
							MPGlobalsAmbience.bKillActiveEvent = FALSE
							serverBD.iServerGameState = GAME_STATE_END
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  serverBD.iServerGameState = GAME_STATE_END 1    <----------     ") NET_NL()
							#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 1")	#ENDIF
						ENDIF
					#ENDIF	
					
					IF HAVE_MISSION_END_CONDITIONS_BEEN_MET()
						serverBD.iServerGameState = GAME_STATE_END
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  serverBD.iServerGameState = GAME_STATE_END 4    <----------     ") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 4")	#ENDIF
					ELIF GB_SHOULD_KILL_ACTIVE_BOSS_MISSION() 
						serverBD.iServerGameState = GAME_STATE_END
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_GUNRUNNING_DEFEND -  serverBD.iServerGameState = GAME_STATE_END 4  AS GB_SHOULD_KILL_ACTIVE_BOSS_MISSION  <----------     ") NET_NL()
					ENDIF
					
				BREAK
				
				CASE GAME_STATE_END
				BREAK
				
			ENDSWITCH
			
		ENDIF
		
	ENDWHILE
	
	
ENDSCRIPT
