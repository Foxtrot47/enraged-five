
//-- Dave W - Header for Gunrunning Defend missions. 
//-- Julian S -Development of Header.

//----------------------
//	INCLUDES
//----------------------
USING "globals.sch"
USING "net_prints.sch"
USING "net_gang_boss_common.sch"
USING "net_include.sch"



STRUCT FLARE_STRUCT
	OBJECT_INDEX		flareObject
	PTFX_ID 			ptfxID1
	PTFX_ID 			ptfxID2
	MODEL_NAMES 		model
	INT 				iR
	INT 				iG
	INT 				iB
	INT 				iA
	FLOAT 				fR
	FLOAT 				fG
	FLOAT 				fB
ENDSTRUCT

FUNC MODEL_NAMES GET_GB_GUNRUNNING_DEFEND_PED_MODEL(INT iVariation, INT iLocation, INT iPed)
	UNUSED_PARAMETER(iLocation)
	UNUSED_PARAMETER(iPed)
	
	
	SWITCH iVariation
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
			//SWITCH iPed
			//	CASE 0	RETURN mp_g_m_pros_01
				//DEFAULT
			RETURN mp_g_m_pros_01
			//ENDSWITCH
		//BREAK
		
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
		RETURN S_M_Y_BLACKOPS_02
//			SWITCH iPed
//				CASE 0			RETURN S_M_Y_BLACKOPS_02	// Pilot
//				CASE 1			RETURN S_M_Y_BLACKOPS_02
//				CASE 2			RETURN S_M_Y_BLACKOPS_02
//				CASE 3			RETURN S_M_Y_BLACKOPS_02
//				CASE 4			RETURN S_M_Y_BLACKOPS_02
//				CASE 5			RETURN S_M_Y_BLACKOPS_02
//				CASE 6			RETURN S_M_Y_BLACKOPS_02
//				CASE 7			RETURN S_M_Y_BLACKOPS_02
//				CASE 8			RETURN S_M_Y_BLACKOPS_02
//				CASE 9			RETURN S_M_Y_BLACKOPS_02
//				CASE 10			RETURN S_M_Y_BLACKOPS_02
//				CASE 11			RETURN S_M_Y_BLACKOPS_02
//
//			ENDSWITCH
		//BREAK
		
	ENDSWITCH
	

	RETURN mp_g_m_pros_01
ENDFUNC

/// PURPOSE:
///    This vehicle model for the vehicle that hs to be retrieved (if any)
/// PARAMS:
///    iVariation - 
///    iLocation - 
///    iVeh - 
/// RETURNS:
///    
FUNC MODEL_NAMES GET_GB_GUNRUNNING_DEFEND_PLAYER_VEH_MODEL(INT iVariation, INT iLocation, INT iVeh)
	UNUSED_PARAMETER(iLocation)
	UNUSED_PARAMETER(iVeh)
	
	SWITCH iVariation
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
		RETURN POUNDER	
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
		RETURN VALKYRIE2
//			SWITCH iLocation
//				CASE 0
//					SWITCH iVeh
//						CASE 0
//							RETURN VALKYRIE
//					ENDSWITCH
//				BREAK
//				CASE 1
//					SWITCH iVeh
//						CASE 0
//							RETURN VALKYRIE
//					ENDSWITCH
//				BREAK
//				CASE 2
//					SWITCH iVeh
//						CASE 0
//							RETURN VALKYRIE
//					ENDSWITCH
//				BREAK
//			ENDSWITCH
		//BREAK
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC


FUNC MODEL_NAMES GET_GB_GUNRUNNING_DEFEND_ENEMY_VEH_MODEL(INT iVariation, INT iLocation, INT iVeh)
	//CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_GUNRUNNING_DEFEND [CREATE_ENEMY_VEHICLE] GET_GB_GUNRUNNING_DEFEND_ENEMY_VEH_MODEL iVariation = ", iVariation, " factoryId = ", ENUM_TO_INT(factoryId)," iVeh = ", iVeh) 
	UNUSED_PARAMETER(iLocation)
	
	SWITCH iVariation
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
		SWITCH iLocation
				CASE 0
					SWITCH iVeh
						CASE 0	RETURN SPEEDO
						CASE 1	RETURN SPEEDO
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH iVeh
						CASE 0	RETURN SPEEDO
						CASE 1	RETURN SPEEDO
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iVeh
						CASE 0	RETURN SPEEDO
						CASE 1	RETURN SPEEDO
					ENDSWITCH
				BREAK
				CASE 3
					SWITCH iVeh
						CASE 0	RETURN SPEEDO
						CASE 1	RETURN SPEEDO
					ENDSWITCH
				BREAK
			ENDSWITCH	
		BREAK
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			SWITCH iLocation
				CASE 0
					SWITCH iVeh
						CASE 0	RETURN MESA3
						CASE 1	RETURN INSURGENT
						CASE 2	RETURN MULE3
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH iVeh
						CASE 0	RETURN MESA3
						CASE 1	RETURN INSURGENT
						CASE 2	RETURN MULE3
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iVeh
						CASE 0	RETURN MESA3
						CASE 1	RETURN INSURGENT
						CASE 2	RETURN MULE3
					ENDSWITCH
				BREAK
			ENDSWITCH
			
		BREAK
		
		
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

//
//
//FUNC BOOL IS_PROP_BOMB(INT iProp)
////
//	SWITCH iProp
//		CASE 6 
//		CASE 7
//		CASE 8
//		
//			RETURN TRUE
//		
//	ENDSWITCH
//
//	RETURN FALSE
//ENDFUNC

FUNC MODEL_NAMES GET_GB_GUNRUNNING_DEFEND_PROP_MODEL(INT iVariation, INT iLocation, INT iProp)
	SWITCH iVariation
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
			SWITCH iLocation
				CASE 0
					SWITCH iProp
						CASE 0		RETURN prop_box_wood04a
						CASE 1		RETURN prop_box_wood04a
						CASE 2		RETURN prop_box_wood04a
						CASE 3		RETURN prop_generator_03b
						CASE 4		RETURN prop_generator_03b
						CASE 5		RETURN prop_generator_03b
						CASE 6		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_adv_hdsec"))
						CASE 7		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_adv_hdsec"))
						CASE 8		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_adv_hdsec"))
						CASE 9		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_hdsec_deactive"))
						CASE 10		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_hdsec_deactive"))
						CASE 11		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_hdsec_deactive"))
						
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH iProp
						CASE 0		RETURN prop_box_wood04a
						CASE 1		RETURN prop_box_wood04a
						CASE 2		RETURN prop_box_wood04a
						CASE 3		RETURN prop_generator_03b
						CASE 4		RETURN prop_generator_03b
						CASE 5		RETURN prop_generator_03b
						CASE 6		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_adv_hdsec"))
						CASE 7		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_adv_hdsec"))
						CASE 8		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_adv_hdsec"))
						CASE 9		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_hdsec_deactive"))
						CASE 10		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_hdsec_deactive"))
						CASE 11		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_hdsec_deactive"))
						
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iProp
						CASE 0		RETURN prop_box_wood04a
						CASE 1		RETURN prop_box_wood04a
						CASE 2		RETURN prop_box_wood04a
						CASE 3		RETURN prop_generator_03b
						CASE 4		RETURN prop_generator_03b
						CASE 5		RETURN prop_generator_03b
						CASE 6		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_adv_hdsec"))
						CASE 7		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_adv_hdsec"))
						CASE 8		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_adv_hdsec"))
						CASE 9		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_hdsec_deactive"))
						CASE 10		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_hdsec_deactive"))
						CASE 11		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_hdsec_deactive"))
						
					ENDSWITCH
				BREAK
				CASE 3
					SWITCH iProp
						CASE 0		RETURN prop_box_wood04a
						CASE 1		RETURN prop_box_wood04a
						CASE 2		RETURN prop_box_wood04a
						CASE 3		RETURN prop_generator_03b
						CASE 4		RETURN prop_generator_03b
						CASE 5		RETURN prop_generator_03b
						CASE 6		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_adv_hdsec"))
						CASE 7		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_adv_hdsec"))
						CASE 8		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_adv_hdsec"))
						CASE 9		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_hdsec_deactive"))
						CASE 10		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_hdsec_deactive"))
						CASE 11		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_hdsec_deactive"))
						
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			SWITCH iLocation
				CASE 0
					SWITCH iProp
						CASE 0			RETURN prop_box_wood04a
						CASE 1			RETURN prop_box_wood04a
						CASE 2			RETURN prop_box_wood04a
						CASE 3			RETURN prop_flare_01
						CASE 4			RETURN prop_generator_03b
						CASE 5			RETURN prop_generator_03b
						CASE 6			RETURN prop_generator_03b
						CASE 7			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01a_set"))
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH iProp
						CASE 0			RETURN prop_box_wood04a
						CASE 1			RETURN prop_box_wood04a
						CASE 2			RETURN prop_box_wood04a
						CASE 3			RETURN prop_generator_03b
						CASE 4			RETURN prop_generator_03b
						CASE 5			RETURN prop_generator_03b
						CASE 6			RETURN prop_flare_01
						CASE 7			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01a_set"))
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iProp
						CASE 0			RETURN prop_generator_03b
						CASE 1			RETURN prop_generator_03b
						CASE 2			RETURN prop_generator_03b
						CASE 3			RETURN prop_box_wood04a
						CASE 4			RETURN prop_box_wood04a
						CASE 5			RETURN prop_flare_01
						CASE 6			RETURN prop_box_wood04a
						CASE 7			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01a_set"))
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC MODEL_NAMES GET_GB_GUNRUNNING_DEFEND_PACKAGE_MODEL(INT iVariation, INT iLocation)
	UNUSED_PARAMETER(iLocation)
	SWITCH iVariation
			
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			
			RETURN EX_PROP_ADV_CASE_SM
	//		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("bkr_Prop_Duffel_Bag_01a"))
	ENDSWITCH
			
	RETURN DUMMY_MODEL_FOR_SCRIPT
	
	//RETURN prop_drug_package//
ENDFUNC


FUNC INT GET_GB_GUNRUNNING_DEFEND_NUMBER_OF_HELI_GOTO_POINTS(INT iVariation, INT iLocation)
	SWITCH iVariation
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			SWITCH iLocation
				CASE 0	RETURN 6
				CASE 1	RETURN 7
				CASE 2	RETURN 7
				
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC BOOL SHOULD_FAIL_WHEN_HELI_REACHES_END_OF_GOTO_ROUTE(INT iVariation)
	SWITCH iVariation
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_LAST_HELI_GOTO_IN_LOCATION(INT iVariation, INT iLocation)
	SWITCH iVariation
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			SWITCH iLocation
				CASE 0	RETURN 6
				CASE 1	RETURN 7
				CASE 2	RETURN 7
			ENDSWITCH
		BREAK
	
	ENDSWITCH
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Get goto coordinate for a helicopter. Idea being it will fly to point iGoto = 0, then iGoto = 1 etc
/// PARAMS:
///    iVariation - 
///    iGoto - 
///    iLocation - 
/// RETURNS:
///    
FUNC VECTOR GET_NTH_HELI_GOTO_COORD(INT iVariation, INT iLocation, INT iGoto)

	SWITCH iVariation
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			SWITCH iLocation
				CASE 0
					SWITCH iGoto
						CASE 0			RETURN <<1695.1899, 3315.9221, 91.2720>>
						CASE 1			RETURN <<1320.6150, 1927.6160, 142.4050>>
						CASE 2			RETURN <<1096.1689, 553.2840, 183.7230>>
						CASE 3			RETURN <<777.1940, -730.3070, 76.6590>>
						CASE 4			RETURN <<826.3270, -1752.4860, 78.5320>>
						CASE 5			RETURN <<645.7070, -2697.1960, 78.5320>>
						CASE 6			RETURN <<477.4920, -3371.3081, 76.1620>>

					ENDSWITCH
				BREAK
				CASE 1
					SWITCH iGoto
						CASE 0			RETURN <<1366.7299, -1860.5139, 133.2140>>
						CASE 1			RETURN <<1080.3696, -1403.7255, 135.7079>>
						CASE 2			RETURN <<1021.9543, -612.2437, 135.7079>>
						CASE 3			RETURN <<1208.4791, 483.2035, 167.2980>>
						CASE 4			RETURN <<1635.0889, 1202.5670, 167.6163>>
						CASE 5			RETURN <<2052.1907, 2606.0520, 170.5815>>
						CASE 6			RETURN <<2777.6443, 3227.4846, 149.9717>>
						CASE 7			RETURN <<3570.7710, 3665.0352, 91.6934>>
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iGoto
						CASE 0			RETURN <<741.9750, -1845.0510, 76.3140>>
						CASE 1			RETURN <<1155.8440, -2075.6531, 72.6000>>
						CASE 2			RETURN <<1815.1899, -1465.1080, 184.5570>>
						CASE 3			RETURN <<2360.0911, -334.3410, 155.7670>>
						CASE 4			RETURN <<2628.5220, 1573.6331, 81.4450>>
						CASE 5			RETURN <<2690.1370, 3038.3879, 97.0820>>
						CASE 6			RETURN <<3241.9951, 3580.9260, 139.6940>>
						CASE 7			RETURN <<3568.3140, 3663.1760, 84.7430>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN << 0.0, 0.0, 0.0>>
ENDFUNC
/// PURPOSE:
///    List of all the possible spawn locations for peds who spawn in the bomb defusal waves.
/// PARAMS:
///    iLocation - 
///    iSpawnCoord - 
///    vLoc - 
///    fHeading - 
PROC  GB_GUNRUNNING_DEFEND_GET_PED_WAVE_SPAWN_COORD( INT iLocation,INT iSpawnCoord, VECTOR &vLoc, FLOAT &fHeading)
	SWITCH iLocation
				CASE 0
					SWITCH iSpawnCoord
					
						CASE 0  	vLoc = <<1055.4705, -1995.8763, 30.0166>>		fHeading = 351.0000 BREAK
						CASE 1  	vLoc = <<1117.3994, -1983.1510, 29.9457>>		fHeading = 95.9999	BREAK
						CASE 2		vLoc = <<1083.7450, -1973.9280, 30.0150>>		fHeading = 341.2000	BREAK
						CASE 3		vLoc = <<1098.7550, -1984.1820, 30.0150>>		fHeading = 15.4000	BREAK
						CASE 4		vLoc = <<1108.7490, -1983.4840, 30.0260>>		fHeading = 98.7990	BREAK
						CASE 5		vLoc = <<1060.8290, -1998.6140, 30.0160>>		fHeading = 98.7990	BREAK
						CASE 6		vLoc = <<1042.4060, -1970.3690, 33.9680>>		fHeading = 302.7990	BREAK
						CASE 7		vLoc = <<1049.9220, -1958.5280, 30.0320>>		fHeading = 260.7990	BREAK
						CASE 8		vLoc = <<1046.5530, -1957.3190, 34.1390>>		fHeading = 260.7990	BREAK
						CASE 9		vLoc = <<1054.7970, -1953.6860, 31.1000>>		fHeading = 260.7990	BREAK
						CASE 10		vLoc = <<1056.2390, -1944.5031, 35.3250>>		fHeading = 260.7990	BREAK
						CASE 11		vLoc = <<1082.8669, -1947.0940, 30.1140>>		fHeading = 126.9980	BREAK
						CASE 12		vLoc = <<1124.4351, -1990.6910, 30.0150>>		fHeading = 61.9980	BREAK
						CASE 13		vLoc = <<1110.8380, -1970.2111, 30.0140>>		fHeading = 199.7980	BREAK
						CASE 14		vLoc = <<1075.8080, -1973.6689, 46.9270>>		fHeading = 4.9970	BREAK
						CASE 15		vLoc = <<1084.7531, -1949.2570, 39.7390>>		fHeading = 112.7970	BREAK
						CASE 16		vLoc = <<1036.0869, -1976.2251, 30.0445>>		fHeading = 284.9967	BREAK
						CASE 17		vLoc = <<1101.3580, -1977.8051, 35.3010>>		fHeading = 59.5970	BREAK
						CASE 18		vLoc = <<1037.3340, -1974.5620, 30.0450>>		fHeading = 257.1970	BREAK
						CASE 19		vLoc = <<1029.2090, -1957.4490, 30.0430>>		fHeading = 257.1970	BREAK
						CASE 20		vLoc = <<1057.1710, -1924.9270, 35.3260>>		fHeading = 207.9970	BREAK
						CASE 21		vLoc = <<1095.1530, -1945.8250, 30.4800>>		fHeading = 78.9960	BREAK
						CASE 22		vLoc = <<1104.8140, -1961.6071, 39.3920>>		fHeading = 111.7960	BREAK
						CASE 23 	vLoc = <<1052.4370, -1952.8850, 39.0280>>		fHeading = 265.5960	BREAK
						CASE 24		vLoc = <<1046.3662, -1993.3947, 30.0447>>		fHeading = 293.3969	BREAK
						CASE 25		vLoc = <<1034.9624, -1969.4718, 30.0438>>		fHeading = 314.5968	BREAK
						CASE 26		vLoc = <<1088.3370, -1980.5537, 49.8624>>		fHeading = 49.7967	BREAK
						CASE 27		vLoc = <<1091.7250, -1943.3732, 30.7072>>		fHeading = 94.1965	BREAK	
						CASE 28		vLoc = <<1070.5052, -1914.3690, 30.5570>>		fHeading = 159.1963	BREAK	
						CASE 29		vLoc = <<1042.0769, -1991.4810, 30.0192>>		fHeading = 321.3989	BREAK														
					ENDSWITCH														
				BREAK	
				CASE 1
					SWITCH iSpawnCoord
						CASE 0  	vLoc = <<223.8943, -1872.4429, 25.8715>>		fHeading = 136.5995	BREAK
						CASE 1  	vLoc = <<233.5436, -1874.8145, 25.4799>>		fHeading = 143.7995	BREAK
						CASE 2		vLoc = <<224.8463, -1885.0547, 24.7187>>		fHeading = 347.7993	BREAK
						CASE 3		vLoc = <<215.3888, -1862.0089, 29.8679>>		fHeading = 72.3990	BREAK
						CASE 4		vLoc = <<224.0119, -1849.1362, 25.9600>>		fHeading = 155.7985	BREAK
						CASE 5		vLoc = <<229.6837, -1846.2457, 25.8657>>		fHeading = 120.9982	BREAK
						CASE 6		vLoc = <<181.8554, -1840.3456, 27.0975>>		fHeading = 134.1981	BREAK
						CASE 7		vLoc = <<182.4445, -1837.0227, 27.1026>>		fHeading = 152.1979	BREAK
						CASE 8		vLoc = <<179.4690, -1832.0035, 27.1166>>		fHeading = 150.9978	BREAK
						CASE 9		vLoc = <<174.4112, -1824.6622, 27.2750>>		fHeading = 178.1977	BREAK
						CASE 10		vLoc = <<162.9509, -1809.8105, 27.7446>>		fHeading = 164.9975	BREAK
						CASE 11		vLoc = <<173.9211, -1805.9800, 28.0737>>		fHeading = 170.5974	BREAK
						CASE 12		vLoc = <<183.2023, -1813.5142, 28.0026>>		fHeading = 159.5974	BREAK
						CASE 13		vLoc = <<194.7354, -1839.0178, 31.2540>>		fHeading = 168.5973	BREAK
						CASE 14		vLoc = <<194.8095, -1831.2981, 33.5520>>		fHeading = 173.5972 BREAK
						CASE 15		vLoc = <<216.5745, -1863.3549, 33.8676>>		fHeading = 80.5969	BREAK
						CASE 16		vLoc = <<215.0836, -1855.6116, 30.2272>>		fHeading = 93.7968	BREAK
						CASE 17		vLoc = <<233.5953, -1877.8741, 25.2969>>		fHeading = 89.5968	BREAK
						CASE 18		vLoc = <<228.0450, -1885.5577, 24.7885>>		fHeading = 52.3967	BREAK
						CASE 19		vLoc = <<209.8642, -1834.1383, 30.9008>>		fHeading = 165.1964	BREAK
						CASE 20		vLoc = <<151.3684, -1812.2551, 27.1353>>		fHeading = 240.7961	BREAK
						CASE 21		vLoc = <<157.5625, -1803.7252, 27.7590>>		fHeading = 213.9962	BREAK
						CASE 22		vLoc = <<212.8072, -1844.2081, 30.5683>>		fHeading = 144.7960	BREAK
						CASE 23 	vLoc = <<163.3574, -1831.6115, 26.5229>>		fHeading = 237.1957	BREAK
						CASE 24		vLoc = <<159.2808, -1836.9426, 26.4316>>		fHeading = 235.9957	BREAK
						CASE 25		vLoc = <<226.8004, -1843.6375, 25.9307>>		fHeading = 177.1956	BREAK
						CASE 26		vLoc = <<157.4909, -1816.9467, 27.1418>>		fHeading = 232.3954	BREAK
						CASE 27		vLoc = <<146.3948, -1816.0411, 26.8111>>		fHeading = 282.9952	BREAK	
						CASE 28		vLoc = <<174.1512, -1815.2272, 27.7846>>		fHeading = 167.3948	BREAK	
						CASE 29		vLoc = <<167.8879, -1815.2568, 27.6257>>		fHeading = 176.3947	BREAK	
					ENDSWITCH														
				BREAK	
				CASE 2
					SWITCH iSpawnCoord
						CASE 0  	vLoc = <<-108.1577, -2503.1670, 13.6445>>		fHeading = 188.7996	BREAK
						CASE 1  	vLoc = <<-124.4544, -2526.1563, 13.6445>>		fHeading = 222.5996	BREAK
						CASE 2		vLoc = <<-131.2542, -2535.6240, 11.0446>>		fHeading = 246.5995	BREAK
						CASE 3		vLoc = <<-139.7364, -2532.9717, 5.0007>>		fHeading = 246.5995	BREAK
						CASE 4		vLoc = <<-147.1421, -2525.9287, 5.1000>>		fHeading = 231.3995	BREAK
						CASE 5		vLoc = <<-110.1184, -2509.6934, 3.8278>>		fHeading = 242.3994	BREAK
						CASE 6		vLoc = <<-111.5171, -2492.4038, 5.0091>>		fHeading = 250.9992	BREAK
						CASE 7		vLoc = <<-105.6160, -2496.7188, 5.0054>>		fHeading = 250.9992	BREAK
						CASE 8		vLoc = <<-95.4160, -2490.6016, 5.0186>>			fHeading = 168.1990	BREAK
						CASE 9		vLoc = <<-108.7311, -2483.6619, 5.0265>>		fHeading = 195.7988	BREAK
						CASE 10		vLoc = <<-62.7724, -2509.5637, 10.3494>>		fHeading = 112.9986	BREAK
						CASE 11		vLoc = <<-77.9648, -2534.8142, 5.0100>>			fHeading = 76.3984	BREAK
						CASE 12		vLoc = <<-60.2275, -2523.2292, 5.1609>>			fHeading = 106.9983	BREAK
						CASE 13		vLoc = <<-146.5238, -2531.6978, 5.0009>>		fHeading = 241.9981	BREAK
						CASE 14		vLoc = <<-56.1022, -2501.8606, 5.1609>>			fHeading = 114.5979 BREAK
						CASE 15		vLoc = <<-77.9708, -2488.7092, 5.0232>>			fHeading = 165.1976	BREAK
						CASE 16		vLoc = <<-98.0264, -2490.6272, 10.6453>>		fHeading = 165.1976	BREAK
						CASE 17		vLoc = <<-86.4462, -2498.5332, 13.4656>>		fHeading = 153.1974	BREAK
						CASE 18		vLoc = <<-121.4051, -2499.3521, 13.6445>>		fHeading = 229.1971	BREAK
						CASE 19		vLoc = <<-126.2097, -2505.1729, 13.6445>>		fHeading = 238.1971	BREAK
						CASE 20		vLoc = <<-137.8986, -2521.0337, 11.0446>>		fHeading = 238.1971	BREAK
						CASE 21		vLoc = <<-62.7017, -2532.7412, 5.0127>>			fHeading = 58.1967	BREAK
						CASE 22		vLoc = <<-49.0633, -2530.0093, 5.1540>>			fHeading = 85.3966	BREAK
						CASE 23 	vLoc = <<-154.0171, -2525.7400, 5.0050>>		fHeading = 239.9962	BREAK
						CASE 24		vLoc = <<-113.3242, -2482.5635, 5.0263>>		fHeading = 234.3963	BREAK
						CASE 25		vLoc = <<-92.1973, -2486.0969, 5.0197>>			fHeading = 153.3960	BREAK
						CASE 26		vLoc = <<-105.0870, -2480.1489, 5.0228>>		fHeading = 156.3960	BREAK
						CASE 27		vLoc = <<-51.5454, -2506.0295, 6.4012>>			fHeading = 62.7957	BREAK	
						CASE 28		vLoc = <<-63.3921, -2545.3018, 5.0100>>			fHeading = 62.7957	BREAK	
						CASE 29		vLoc = <<-63.3405, -2520.3455, 6.4002>>			fHeading = 104.7956	BREAK	
					ENDSWITCH														
				BREAK	
				CASE 3
					SWITCH iSpawnCoord
						CASE 0  	vLoc = <<751.7123, -531.2252, 39.9524>>		fHeading = 130.7997		BREAK
						CASE 1  	vLoc = <<738.7371, -575.9060, 33.7772>>		fHeading = 80.1995		BREAK
						CASE 2		vLoc = <<741.0178, -568.4597, 33.7772>>		fHeading = 66.9995		BREAK
						CASE 3		vLoc = <<744.3707, -556.3380, 33.7772>>		fHeading = 87.1994		BREAK
						CASE 4		vLoc = <<753.6755, -525.5611, 26.2189>>		fHeading = 84.1994		BREAK
						CASE 5		vLoc = <<748.2568, -528.6512, 26.7782>>		fHeading = 84.1994		BREAK
						CASE 6		vLoc = <<759.2032, -507.8281, 26.8752>>		fHeading = 130.3993		BREAK
						CASE 7		vLoc = <<751.4592, -537.2648, 40.8336>>		fHeading = 128.5993		BREAK
						CASE 8		vLoc = <<746.9885, -541.8518, 33.7772>>		fHeading = 99.1991		BREAK
						CASE 9		vLoc = <<747.5743, -583.4346, 26.1191>>		fHeading = 91.9991		BREAK
						CASE 10		vLoc = <<738.1207, -578.9072, 26.1012>>		fHeading = 74.3990		BREAK
						CASE 11		vLoc = <<757.5797, -520.5892, 26.6085>>		fHeading = 113.3985		BREAK
						CASE 12		vLoc = <<702.1577, -589.6456, 24.7053>>		fHeading = 325.1983		BREAK
						CASE 13		vLoc = <<713.7491, -592.7561, 26.0064>>		fHeading = 349.1982		BREAK
						CASE 14		vLoc = <<732.3572, -586.2111, 26.0538>>		fHeading = 44.3979		BREAK
						CASE 15		vLoc = <<724.5908, -573.8358, 34.5614>>		fHeading = 357.3978		BREAK
						CASE 16		vLoc = <<717.2817, -570.5662, 34.5583>>		fHeading = 345.1972		BREAK
						CASE 17		vLoc = <<698.3436, -561.2944, 36.3998>>		fHeading = 313.7969		BREAK
						CASE 18		vLoc = <<765.7123, -502.6945, 27.1826>>		fHeading = 135.5966		BREAK
						CASE 19		vLoc = <<753.4877, -528.0688, 37.9419>>		fHeading = 2.3964		BREAK
						CASE 20		vLoc = <<713.2164, -605.7006, 26.1300>>		fHeading = 349.1963		BREAK
						CASE 21		vLoc = <<732.4710, -597.0988, 26.5775>>		fHeading = 27.5961		BREAK
						CASE 22		vLoc = <<751.6623, -540.7039, 40.8336>>		fHeading = 109.5959		BREAK
						CASE 23 	vLoc = <<698.4994, -598.7482, 24.6356>>		fHeading = 327.9953		BREAK
						CASE 24		vLoc = <<709.1746, -566.9232, 35.0930>>		fHeading = 323.9951		BREAK
						CASE 25		vLoc = <<692.4679, -612.0090, 24.5906>>		fHeading = 326.9950		BREAK
						CASE 26		vLoc = <<740.0320, -500.5943, 24.8953>>		fHeading = 231.5945		BREAK
						CASE 27		vLoc = <<752.7185, -490.9646, 25.4253>>		fHeading = 174.3943		BREAK	
						CASE 28		vLoc = <<690.1321, -565.8355, 24.2646>>		fHeading = 261.3941		BREAK	
						CASE 29		vLoc = <<683.7603, -581.3099, 24.4894>>		fHeading = 279.5942		BREAK	
					ENDSWITCH														
				BREAK																					
	ENDSWITCH																		
ENDPROC																				


/// PURPOSE:
///    Spawn locations for enemy peds
/// PARAMS:
///    iVariation - 
///    iLocation - 
///    iPed - 
///    vLoc - 
///    fHeading - 
PROC INIT_GB_GUNRUNNING_DEFEND_PED_SPAWN_COORDS(INT iVariation, INT iLocation, INT iPed, VECTOR &vLoc, FLOAT &fHeading)

	SWITCH iVariation
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
			SWITCH iLocation
				CASE 0
					SWITCH iPed
						CASE 0	vLoc = <<1082.4810, -1949.4000, 30.0140>> 		fHeading = 53.7990		BREAK
						CASE 1	vLoc = <<1076.6689, -1963.3290, 30.0140>> 		fHeading = 148.7980		BREAK
						CASE 2	vLoc = <<1071.1550, -1967.7090, 30.0150>> 		fHeading = 107.9960		BREAK
						CASE 3	vLoc = <<1069.8920, -1968.2469, 30.0150>> 		fHeading = 287.9960		BREAK
						CASE 4	vLoc = <<1050.1680, -1968.8810, 30.0150>> 		fHeading = 46.7940		BREAK
						CASE 5	vLoc = <<1050.7469, -1957.2330, 30.0420>> 		fHeading = 182.5970		BREAK
						CASE 6	vLoc = <<1056.6300, -1961.6660, 30.0140>> 		fHeading = 143.9960		BREAK
						CASE 7	vLoc = <<1056.0020, -1963.1140, 30.0150>> 		fHeading = 330.5950		BREAK
						CASE 8	vLoc = <<1059.4550, -1957.0770, 30.0140>> 		fHeading = 355.7950		BREAK
						CASE 9	vLoc = <<1074.5880, -1954.3621, 30.0140>> 		fHeading = 180.3990		BREAK
						CASE 10	vLoc = <<1074.8440, -1955.7820, 30.0140>> 		fHeading = 9.5990		BREAK
						CASE 11	vLoc = <<1081.0980, -1959.0649, 31.8210>> 		fHeading = 59.3940		BREAK
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH iPed
						CASE 0	vLoc = <<196.7590, -1854.8890, 26.1900>> 		fHeading = 352.5980	BREAK
						CASE 1	vLoc = <<210.8220, -1852.9200, 30.2270>> 		fHeading = 147.1980	BREAK
						CASE 2	vLoc = <<213.5420, -1863.3770, 29.8680>> 		fHeading = 73.7980	BREAK
						CASE 3	vLoc = <<187.4180, -1848.2220, 26.2140>> 		fHeading = 70.3950	BREAK
						CASE 4	vLoc = <<198.4610, -1850.0260, 26.2030>> 		fHeading = 25.1990	BREAK
						CASE 5	vLoc = <<197.1970, -1849.0190, 26.2020>> 		fHeading = 230.9980	BREAK
						CASE 6	vLoc = <<203.5770, -1857.9850, 26.1990>> 		fHeading = 276.9970	BREAK
						CASE 7	vLoc = <<204.9200, -1857.5070, 26.2020>> 		fHeading = 100.9970	BREAK
						CASE 8	vLoc = <<201.0030, -1846.9091, 26.7820>> 		fHeading = 135.7970	BREAK
						CASE 9	vLoc = <<195.8670, -1861.3330, 25.9920>> 		fHeading = 211.7960	BREAK
						CASE 10	vLoc = <<196.6820, -1862.4510, 25.9560>> 		fHeading = 22.7950	BREAK
						CASE 11	vLoc = <<196.9060, -1861.3101, 25.9860>> 		fHeading = 115.7950	BREAK
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iPed
						CASE 0	vLoc = <<-121.2050, -2524.0391, 10.1630>>			fHeading = 244.3940	BREAK
						CASE 1	vLoc = <<-126.7190, -2530.6399, 5.1010>>			fHeading = 238.7940	BREAK
						CASE 2	vLoc = <<-124.1130, -2539.8530, 5.0000>>			fHeading = 320.3930	BREAK
						CASE 3	vLoc = <<-120.0400, -2534.8440, 5.0000>>			fHeading = 349.3930	BREAK
						CASE 4	vLoc = <<-119.9240, -2533.2261, 5.0000>>			fHeading = 177.7920	BREAK
						CASE 5	vLoc = <<-111.3660, -2535.0920, 5.0000>>			fHeading = 98.3920	BREAK
						CASE 6	vLoc = <<-112.8180, -2535.8931, 5.0000>>			fHeading = 299.7910	BREAK
						CASE 7	vLoc = <<-112.5500, -2534.6240, 5.0000>>			fHeading = 229.5910	BREAK
						CASE 8	vLoc = <<-113.4970, -2525.2930, 5.0000>>			fHeading = 64.9910	BREAK
						CASE 9	vLoc = <<-114.7680, -2524.7080, 5.0000>>			fHeading = 244.3910	BREAK
						CASE 10	vLoc = <<-116.4690, -2516.1521, 5.1010>>			fHeading = 236.1910	BREAK
						CASE 11	vLoc = <<-107.1980, -2524.3630, 5.0000>>			fHeading = 146.9890	BREAK
					ENDSWITCH
				BREAK
				CASE 3
					SWITCH iPed
						CASE 0	vLoc = <<735.6310, -550.3410, 25.7800>> 		fHeading = 189.3980	BREAK
						CASE 1	vLoc = <<736.1140, -551.6280, 25.8290>> 		fHeading = 14.5980	BREAK
						CASE 2	vLoc = <<736.3860, -543.4420, 25.7600>> 		fHeading = 345.9980	BREAK
						CASE 3	vLoc = <<744.7210, -538.1230, 26.7690>> 		fHeading = 70.9950	BREAK
						CASE 4	vLoc = <<743.8870, -540.0100, 26.7670>> 		fHeading = 70.9950	BREAK
						CASE 5	vLoc = <<719.4100, -568.9080, 25.1870>> 		fHeading = 189.7950	BREAK
						CASE 6	vLoc = <<733.7160, -536.9570, 25.8750>> 		fHeading = 320.7990	BREAK
						CASE 7	vLoc = <<734.6300, -536.0410, 25.8990>> 		fHeading = 138.9990	BREAK
						CASE 8	vLoc = <<724.1070, -554.2700, 25.4520>> 		fHeading = 65.7970	BREAK
						CASE 9	vLoc = <<723.4240, -553.4530, 25.4670>> 		fHeading = 209.7970	BREAK
						CASE 10	vLoc = <<722.9350, -554.5440, 25.4410>> 		fHeading = 292.5970	BREAK
						CASE 11	vLoc = <<729.4890, -559.3530, 25.5280>> 		fHeading = 302.7980	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			SWITCH iLocation
				CASE 0
					SWITCH iPed 
						CASE 0	vLoc = <<1699.7650, 3308.6780, 40.1650>> 		fHeading = 224.1990		BREAK // pilot
						CASE 1	vLoc = <<1691.7390, 3314.0950, 40.2890>> 		fHeading = 211.0000		BREAK 
						CASE 2	vLoc = <<1704.3270, 3298.0559, 40.1480>> 		fHeading = 218.5990		BREAK
						CASE 3	vLoc = <<1700.2698, 3286.5830, 47.9222>> 		fHeading = 213.5996		BREAK
						CASE 4	vLoc = <<1683.0032, 3298.3372, 40.0738>>		fHeading = 87.5995		BREAK
						CASE 5  vLoc = <<1699.6694, 3295.8501, 47.9142>>		fHeading = 35.3990		BREAK
						CASE 6  vLoc = <<1691.0790, 3308.6350, 40.1460>>		fHeading = 196.3980		BREAK
						CASE 7  vLoc = <<1696.8397, 3300.8809, 40.1483>>		fHeading = 333.9979		BREAK
						CASE 8	vLoc = <<1697.4421, 3302.2461, 40.1493>>		fHeading = 150.3984		BREAK
						CASE 9  vLoc = <<1697.5602, 3317.8213, 40.3237>>		fHeading = 152.5993		BREAK
						CASE 10 vLoc = <<1710.4750, 3304.4390, 40.1700>>		fHeading = 234.5990		BREAK
						CASE 11 vLoc = <<1711.6030, 3303.7080, 40.1770>>		fHeading = 62.3990		BREAK
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH iPed 
						CASE 0	vLoc = <<1352.3663, -1858.5142, 55.6553>> 		fHeading = 286.7989 BREAK // pilot
						CASE 1	vLoc = <<1354.2169, -1858.0988, 55.6580>> 		fHeading = 101.5978 BREAK 
						CASE 2	vLoc = <<1369.0750, -1860.7610, 55.8510>> 		fHeading = 139.5980 BREAK
						CASE 3	vLoc = <<1366.4797, -1864.8062, 55.6665>> 		fHeading = 137.7980 BREAK
						CASE 4	vLoc = <<1365.2183, -1866.1210, 55.6698>>		fHeading = 317.1970 BREAK
						CASE 5  vLoc = <<1357.8060, -1866.4510, 55.6080>>		fHeading = 323.7960 BREAK
						CASE 6  vLoc = <<1349.7251, -1845.4690, 56.1100>>		fHeading = 33.7966 BREAK
						CASE 7  vLoc = <<1363.4950, -1851.6740, 56.1080>>		fHeading = 22.5990 BREAK
						CASE 8	vLoc = <<1369.9540, -1875.7070, 55.9660>>		fHeading = 44.9960 BREAK
						CASE 9  vLoc = <<1370.7280, -1847.6410, 58.5280>>		fHeading = 96.9960 BREAK
						CASE 10 vLoc = <<1373.9900, -1860.5800, 56.2820>>		fHeading = 181.9960 BREAK
						CASE 11 vLoc = <<1374.0410, -1861.8110, 56.2800>>		fHeading = 10.7950 BREAK
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iPed 
						CASE 0	vLoc = <<747.3670, -1851.6071, 28.2920>> 		fHeading = 174.9990	BREAK // pilot
						CASE 1	vLoc = <<755.7421, -1842.6259, 28.2916>> 		fHeading = 82.3999	BREAK 
						CASE 2	vLoc = <<737.1559, -1851.9757, 28.2916>> 		fHeading = 58.3986	BREAK
						CASE 3	vLoc = <<746.2255, -1858.0168, 28.2916>> 		fHeading = 211.5981	BREAK
						CASE 4	vLoc = <<753.7130, -1852.0422, 28.2916>>		fHeading = 195.3977	BREAK
						CASE 5  vLoc = <<735.6103, -1851.1403, 28.2916>>		fHeading = 232.3980	BREAK
						CASE 6  vLoc = <<730.2368, -1873.9905, 28.2915>>		fHeading = 1.3979	BREAK
						CASE 7  vLoc = <<750.5330, -1870.4969, 28.2920>>		fHeading = 246.3990	BREAK
						CASE 8	vLoc = <<750.8630, -1874.0420, 28.2920>>		fHeading = 267.9980	BREAK
						CASE 9  vLoc = <<740.4280, -1843.7000, 28.2920>>		fHeading = 282.5990	BREAK
						CASE 10 vLoc = <<747.7684, -1830.8374, 28.2916>>		fHeading = 267.5980	BREAK
						CASE 11 vLoc = <<747.1207, -1859.3408, 28.2918>>		fHeading = 39.3984	BREAK				
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC




/// PURPOSE:
///    Scenario which each enemy ped should play prior to being spooked
/// PARAMS:
///    iVariation - 
///    iLocation - 
///    iPed - 
/// RETURNS:
///    
FUNC TEXT_LABEL_63 GET_GB_GUNRUNNING_DEFEND_PED_SCENARIO(INT iVariation,INT iLocation, INT iPed)
	TEXT_LABEL_63 tl63Scen
	SWITCH iVariation
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
			SWITCH iLocation
				CASE 0 
					SWITCH iPed
						CASE 0	tl63Scen = "WORLD_HUMAN_GUARD_STAND"			BREAK
						CASE 1	tl63Scen = "WORLD_HUMAN_WELDING"				BREAK
						CASE 2 	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"		BREAK
						CASE 3 	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"		BREAK
						CASE 4	tl63Scen = "WORLD_HUMAN_CLIPBOARD"				BREAK
						CASE 5 	tl63Scen = "WORLD_HUMAN_GUARD_STAND"			BREAK
						CASE 6 	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"		BREAK 
						CASE 7 	tl63Scen = "WORLD_HUMAN_STAND_MOBILE"			BREAK 
						CASE 8 	tl63Scen = "WORLD_HUMAN_GUARD_STAND"			BREAK 
						CASE 9 	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"		BREAK 
						CASE 10 tl63Scen = "CODE_HUMAN_MEDIC_TIME_OF_DEATH"		BREAK 
						CASE 11 tl63Scen = "WORLD_HUMAN_STAND_IMPATIENT"		BREAK 
					ENDSWITCH
				BREAK
				CASE 1 
					SWITCH iPed
						CASE 0	tl63Scen = "WORLD_HUMAN_WELDING"			BREAK
						CASE 1	tl63Scen = "WORLD_HUMAN_BINOCULARS"		BREAK
						CASE 2 	tl63Scen = "WORLD_HUMAN_STAND_IMPATIENT"	BREAK
						CASE 3 	tl63Scen = "WORLD_HUMAN_GUARD_STAND"		BREAK
						CASE 4	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"	BREAK
						CASE 5 	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"	BREAK
						CASE 6 	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"	BREAK 
						CASE 7 	tl63Scen = "WORLD_HUMAN_STAND_MOBILE"	BREAK 
						CASE 8 	tl63Scen = "WORLD_HUMAN_GUARD_STAND"		BREAK 
						CASE 9 	tl63Scen = "CODE_HUMAN_MEDIC_TIME_OF_DEATH"	BREAK 
						CASE 10 tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"	BREAK 
						CASE 11 tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"	BREAK 
					ENDSWITCH
				BREAK
				CASE 2 
					SWITCH iPed
						CASE 0	tl63Scen = "WORLD_HUMAN_STAND_MOBILE"		BREAK
						CASE 1	tl63Scen = "WORLD_HUMAN_GUARD_STAND"			BREAK
						CASE 2 	tl63Scen = "WORLD_HUMAN_WELDING"				BREAK
						CASE 3 	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"		BREAK
						CASE 4	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"		BREAK
						CASE 5 	tl63Scen = "CODE_HUMAN_MEDIC_TIME_OF_DEATH"	BREAK
						CASE 6 	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"		BREAK 
						CASE 7 	tl63Scen = "WORLD_HUMAN_STAND_IMPATIENT"	BREAK 
						CASE 8 	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"		BREAK 
						CASE 9 	tl63Scen = "WORLD_HUMAN_STAND_MOBILE"		BREAK 
						CASE 10 tl63Scen = "WORLD_HUMAN_GUARD_STAND"			BREAK 
						CASE 11 tl63Scen = "WORLD_HUMAN_CLIPBOARD"			BREAK 
					ENDSWITCH
				BREAK
				CASE 3 
					SWITCH iPed
						CASE 0	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"	BREAK
						CASE 1	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"	BREAK
						CASE 2 	tl63Scen = "WORLD_HUMAN_WELDING"			BREAK
						CASE 3 	tl63Scen = "WORLD_HUMAN_STAND_MOBILE"	BREAK
						CASE 4	tl63Scen = "WORLD_HUMAN_GUARD_STAND"		BREAK
						CASE 5 	tl63Scen = "WORLD_HUMAN_GUARD_STAND"		BREAK
						CASE 6 	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"	BREAK 
						CASE 7 	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"	BREAK 
						CASE 8 	tl63Scen = "CODE_HUMAN_MEDIC_TIME_OF_DEATH"	BREAK 
						CASE 9 	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"	BREAK 
						CASE 10 tl63Scen = "WORLD_HUMAN_STAND_IMPATIENT"	BREAK 
						CASE 11 tl63Scen = "WORLD_HUMAN_CLIPBOARD"		BREAK 
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			SWITCH iLocation
				CASE 0 
					SWITCH iPed
						CASE 0	tl63Scen = "WORLD_HUMAN_CLIPBOARD"				BREAK
						CASE 1	tl63Scen = "CODE_HUMAN_MEDIC_KNEEL"				BREAK
						CASE 2 	tl63Scen = "WORLD_HUMAN_GUARD_STAND"			BREAK
						CASE 3 	tl63Scen = "WORLD_HUMAN_BINOCULARS"				BREAK
						CASE 4	tl63Scen = "WORLD_HUMAN_SMOKING"				BREAK
						CASE 5 	tl63Scen = "WORLD_HUMAN_BINOCULARS"				BREAK
						CASE 6 	tl63Scen = "WORLD_HUMAN_WINDOW_SHOP_BROWSE"		BREAK 
						CASE 7	tl63Scen = "CODE_HUMAN_MEDIC_TIME_OF_DEATH"		BREAK
						CASE 8	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"		BREAK
						CASE 9 	tl63Scen = "WORLD_HUMAN_WELDING"				BREAK
						CASE 10	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"		BREAK
						CASE 11	tl63Scen = "WORLD_HUMAN_SMOKING"				BREAK
					ENDSWITCH
				BREAK
				CASE 1 
					SWITCH iPed
						CASE 0	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"		BREAK
						CASE 1	tl63Scen = "WORLD_HUMAN_STAND_MOBILE"		BREAK
						CASE 2 	tl63Scen = "WORLD_HUMAN_WELDING"		BREAK
						CASE 3 	tl63Scen = "WORLD_HUMAN_SMOKING"		BREAK
						CASE 4	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"		BREAK
						CASE 5 	tl63Scen = "WORLD_HUMAN_CLIPBOARD"		BREAK
						CASE 6 	tl63Scen = "WORLD_HUMAN_SMOKING"		BREAK 
						CASE 7	tl63Scen = "WORLD_HUMAN_CLIPBOARD"		BREAK
						CASE 8	tl63Scen = "WORLD_HUMAN_GUARD_STAND"		BREAK
						CASE 9 	tl63Scen = "WORLD_HUMAN_SMOKING"		BREAK
						CASE 10	tl63Scen = "CODE_HUMAN_MEDIC_TIME_OF_DEATH"		BREAK
						CASE 11	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"		BREAK
					ENDSWITCH
				BREAK
				CASE 2 
					SWITCH iPed
						CASE 0	tl63Scen = "CODE_HUMAN_MEDIC_KNEEL"		BREAK
						CASE 1	tl63Scen = "WORLD_HUMAN_GUARD_STAND"		BREAK
						CASE 2 	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"		BREAK
						CASE 3 	tl63Scen = "WORLD_HUMAN_STAND_MOBILE"		BREAK
						CASE 4	tl63Scen = "WORLD_HUMAN_CLIPBOARD"		BREAK
						CASE 5 	tl63Scen = "WORLD_HUMAN_SMOKING"		BREAK
						CASE 6 	tl63Scen = "WORLD_HUMAN_SMOKING"		BREAK 
						CASE 7	tl63Scen = "WORLD_HUMAN_GUARD_STAND"		BREAK
						CASE 8	tl63Scen = "WORLD_HUMAN_GUARD_STAND"		BREAK
						CASE 9 	tl63Scen = "WORLD_HUMAN_WELDING"		BREAK
						CASE 10	tl63Scen = "WORLD_HUMAN_CLIPBOARD"		BREAK
						CASE 11	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"		BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
				
				
	
	RETURN tl63Scen
ENDFUNC

/// PURPOSE:
///    Returns the cooresponding vehicle array index for the ped specified. i.e. the vehicle that the ped should be in 
/// PARAMS:
///    iVariation - 
///    iLocation - 
///    iPed - 
/// RETURNS:
///    
FUNC INT GET_GB_GUNRUNNING_DEFEND_VEH_PED_SHOULD_USE(INT iVariation, INT iLocation, INT iPed)
	UNUSED_PARAMETER(iLocation)
	UNUSED_PARAMETER(iped)
	SWITCH iVariation
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
//			SWITCH iPed
//				CASE 1		RETURN 0	// Technical gunner
//			ENDSWITCH
		BREAK
	ENDSWITCH

//	
	RETURN -1
ENDFUNC



/// PURPOSE:
///    Spawn coords for vehicles which aren't used by anybody (Ai ped or otherwise)
/// PARAMS:
///    iVariation - 
///    iLocation - 
///    iVeh - 
///    vLoc - 
///    fHeading - 
PROC INIT_GB_GUNRUNNING_DEFEND_AMBIENT_VEH_SPAWN_COORDS(INT iVariation, INT iLocation, INT iVeh, VECTOR &vLoc, FLOAT &fHeading)
UNUSED_PARAMETER(iVariation)
UNUSED_PARAMETER(iLocation)
UNUSED_PARAMETER(iVeh)
UNUSED_PARAMETER(vLoc)
UNUSED_PARAMETER(fHeading)
//	SWITCH iVariation
//		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
//			SWITCH iLocation
//				CASE 0
//					SWITCH iVeh
//						//CASE 0	vLoc =  <<1073.8510, -1963.8030, 30.0140>>		fHeading = 55.3980	BREAK
//						CASE 0	vLoc =  <<1052.4570, -1963.5430, 30.0140>>		fHeading = 16.2000	BREAK
//						CASE 1	vLoc =	<<1074.0090, -1949.6680, 30.0140>>      fHeading = 302.3990	BREAK
//					ENDSWITCH
//				BREAK
//			ENDSWITCH 	
//			
//		BREAK
//		
//	ENDSWITCH
ENDPROC


/// PURPOSE:
///    The spawn coords for the vehicle(s) that has to be retrieved
/// PARAMS:
///    iVariation - 
///    iLocation - 
///    iVeh - 
///    vLoc - 
///    fHeading - 
PROC INIT_GB_GUNRUNNING_DEFEND_CONTRABAND_VEH_SPAWN_COORDS(INT iVariation, INT iLocation, INT iVeh, VECTOR &vLoc, FLOAT &fHeading)
	SWITCH iVariation
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
			SWITCH iLocation
				CASE 0
					SWITCH iVeh
						CASE 0	vLoc =  <<1073.8510, -1963.8030, 30.0140>>  		fHeading =  55.3980	BREAK 
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH iVeh
						CASE 0	vLoc =  <<200.8890, -1853.8630, 26.2020>>  		fHeading =  258.7990 BREAK 
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iVeh
						CASE 0	vLoc =  <<-123.3800, -2538.0920, 5.0000>>  		fHeading =  54.8000	BREAK 
					ENDSWITCH
				BREAK
				CASE 3
					SWITCH iVeh
						CASE 0	vLoc =   <<737.9270, -538.5040, 25.8530>>  		fHeading = 163.0000	BREAK 
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			SWITCH iLocation
				CASE 0
					SWITCH iVeh
						CASE 0	vLoc =  <<1695.8260, 3316.6440, 40.3990>>  		fHeading =  78.6000	BREAK 
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH iVeh
						CASE 0	vLoc =  <<1366.5656, -1860.1763, 55.8010>>  		fHeading =  39.3999	BREAK 
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iVeh
						CASE 0	vLoc =   <<742.5110, -1844.6060, 28.2920>>  		fHeading =  210.6000	BREAK 
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC VECTOR GET_GB_GUNRUNNING_DEFEND_CONTRABAND_VEHICLE_LOCATION(INT iVariation, INT iLocation)
	 
	SWITCH iVariation
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
			SWITCH iLocation
				CASE 0 RETURN  <<1073.8510, -1963.8030, 30.0140>>  	
		
				BREAK
				CASE 1 RETURN  <<200.8890, -1853.8630, 26.2020>>		
				
				BREAK
				CASE 2 RETURN  <<-123.3800, -2538.0920, 5.0000>>  		
					
				BREAK
				CASE 3 RETURN  <<737.9270, -538.5040, 25.8530>>		
					
				BREAK
			ENDSWITCH
		BREAK
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			SWITCH iLocation
				CASE 0 RETURN  <<1695.8260, 3316.6440, 40.3990>>  	
		
				BREAK
				CASE 1 RETURN  <<1366.5656, -1860.1763, 55.8010>>  		
				
				BREAK
				CASE 2 RETURN  <<742.5110, -1844.6060, 28.2920>>  		
					
				BREAK
		
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN <<0,0,0>>
ENDFUNC



PROC INIT_GB_GUNRUNNING_DEFEND_ENEMY_VEH_SPAWN_COORDS(INT iVariation, INT iLocation, INT iVeh, VECTOR &vLoc, FLOAT &fHeading)


	SWITCH iVariation
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
			SWITCH iLocation
				CASE 0
					SWITCH iVeh 
						CASE 0	vLoc =  <<1052.4570, -1963.5430, 30.0140>>  	fHeading = 16.2000	BREAK // Speedo
						CASE 1	vLoc =  <<1074.0090, -1949.6680, 30.0140>>  	fHeading = 302.3990	BREAK // Speedo
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH iVeh 
						CASE 0	vLoc =  <<203.0820, -1865.3000, 25.7910>>  		fHeading = 34.0000	BREAK // Speedo
						CASE 1	vLoc =  <<192.3930, -1846.0780, 26.2040>>   	fHeading = 319.5980	BREAK // Speedo
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iVeh 
						CASE 0	vLoc =  <<-108.4950, -2527.6450, 5.0000>> 		fHeading = 160.1910	BREAK // Speedo
						CASE 1	vLoc =  <<-117.0820, -2529.2500, 5.0000>>  		fHeading = 89.5970	BREAK // Speedo
					ENDSWITCH
				BREAK
				CASE 3
					SWITCH iVeh 
						CASE 0	vLoc =  <<732.2670, -557.4190, 25.6910>> 		fHeading = 302.7980	BREAK // Speedo
						CASE 1	vLoc =  <<727.4380, -549.4210, 25.5690>> 		fHeading = 347.9980	BREAK // Speedo
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			SWITCH iLocation
				CASE 0
					SWITCH iVeh 
						CASE 0	vLoc =  <<1701.7290, 3306.3010, 40.1720>>  		fHeading = 219.8000	BREAK // Mesa
						CASE 1	vLoc =  <<1692.2150, 3305.7019, 40.1460>>   	fHeading = 198.0000	BREAK // Mesa
						CASE 2	vLoc = 	<<1685.1694, 3300.1729, 40.1150>>		fHeading = 167.3998	BREAK // MULE
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH iVeh 
						CASE 0	vLoc =  <<1356.0850, -1853.6110, 55.9780>>  	fHeading = 168.9990	BREAK // Mesa
						CASE 1	vLoc =  <<1359.7950, -1863.8010, 55.6020>>   	fHeading = 323.7980	BREAK // Mesa
						CASE 2	vLoc = 	<<1348.9030, -1848.6030, 56.1530>>		fHeading = 316.5990	BREAK // MULE
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iVeh 
						CASE 0	vLoc =  <<748.9900, -1856.4580, 28.2920>> 		fHeading = 254.6000	BREAK // Mesa
						CASE 1	vLoc =  <<739.0040, -1854.3700, 28.2920>>  		fHeading = 149.0000	BREAK // Mesa
						CASE 2	vLoc = 	<<744.5185, -1825.6472, 28.2916>>		fHeading = 20.5976	BREAK // MULE
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	
	

ENDPROC

PROC INIT_GB_GUNRUNNING_DEFEND_PROP_SPAWN_COORDS(INT iVariation, INT iLocation, INT iProp, VECTOR &vLoc, FLOAT &fHeading)
	SWITCH iVariation
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
			SWITCH iLocation
				CASE 0
					SWITCH iProp
						CASE 0	vLoc = <<1072.2650, -1955.2400, 30.0170>> 		fHeading = 270.6000 BREAK
						CASE 1	vLoc = <<1067.7780, -1965.8120, 30.0170>> 		fHeading = 216.2000 BREAK
						CASE 2	vLoc = <<1059.3240, -1955.8790, 30.0170>> 		fHeading = 359.3990 BREAK
						CASE 3	vLoc = <<1045.3480, -1965.2400, 30.0740>> 		fHeading = 292.9930 BREAK
						CASE 4	vLoc = <<1067.3910, -1974.4320, 30.0740>> 		fHeading = 337.1940 BREAK
						CASE 5	vLoc = <<1073.0660, -1944.1340, 30.0760>> 		fHeading = 181.9940 BREAK
						CASE 6	vLoc = <<1073.0660, -1944.1340, 30.0760>> 		fHeading = 181.9940 BREAK						
						CASE 7	vLoc = <<1073.0660, -1944.1340, 30.0760>> 		fHeading = 181.9940 BREAK						
						CASE 8	vLoc = <<1073.0660, -1944.1340, 30.0760>> 		fHeading = 181.9940 BREAK		
						CASE 9	vLoc = <<1073.0660, -1944.1340, 30.0760>> 		fHeading = 181.9940 BREAK						
						CASE 10	vLoc = <<1073.0660, -1944.1340, 30.0760>> 		fHeading = 181.9940 BREAK						
						CASE 11	vLoc = <<1073.0660, -1944.1340, 30.0760>> 		fHeading = 181.9940 BREAK						
						
						
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH iProp
						CASE 0	vLoc = <<186.2520, -1847.7780, 26.2570>> 		fHeading = 71.7950 BREAK
						CASE 1	vLoc = <<194.1250, -1860.5070, 26.0200>> 		fHeading = 80.1990 BREAK
						CASE 2	vLoc = <<205.9340, -1859.6050, 26.1730>> 		fHeading = 229.3990 BREAK
						CASE 3	vLoc = <<187.7610, -1855.2469, 26.1760>> 		fHeading = 258.7990 BREAK
						CASE 4	vLoc = <<211.8240, -1860.6219, 26.1910>> 		fHeading = 48.7950 BREAK
						CASE 5	vLoc = <<198.3620, -1864.0780, 25.9540>> 		fHeading = 344.5950 BREAK						
						CASE 6	vLoc = <<198.3620, -1864.0780, 25.9540>> 		fHeading = 344.5950 BREAK						
						CASE 7	vLoc = <<198.3620, -1864.0780, 25.9540>> 		fHeading = 344.5950 BREAK						
						CASE 8	vLoc = <<198.3620, -1864.0780, 25.9540>> 		fHeading = 344.5950 BREAK
						CASE 9	vLoc = <<198.3620, -1864.0780, 25.9540>> 		fHeading = 344.5950 BREAK						
						CASE 10	vLoc = <<198.3620, -1864.0780, 25.9540>> 		fHeading = 344.5950 BREAK						
						CASE 11	vLoc = <<198.3620, -1864.0780, 25.9540>> 		fHeading = 344.5950 BREAK						
						
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iProp
						CASE 0	vLoc = <<-112.0350, -2523.3660, 5.0030>>		fHeading = 294.3940 BREAK
						CASE 1	vLoc = <<-109.9480, -2533.8220, 5.0030>>		fHeading = 128.1900 BREAK
						CASE 2	vLoc = <<-117.5950, -2534.5320, 5.0030>>		fHeading = 250.9940 BREAK
						CASE 3	vLoc = <<-131.0880, -2540.1299, 5.0590>>		fHeading = 277.4000 BREAK
						CASE 4	vLoc = <<-109.1530, -2517.6079, 5.0590>>		fHeading = 146.9990 BREAK
						CASE 5	vLoc = <<-94.7510, -2531.6340, 5.0640>>			fHeading = 88.1990  BREAK						
						CASE 6	vLoc = <<-94.7510, -2531.6340, 5.0640>>			fHeading = 88.1990  BREAK						
						CASE 7	vLoc = <<-94.7510, -2531.6340, 5.0640>>			fHeading = 88.1990  BREAK						
						CASE 8	vLoc = <<-94.7510, -2531.6340, 5.0640>>			fHeading = 88.1990  BREAK
						CASE 9	vLoc = <<-94.7510, -2531.6340, 5.0640>>			fHeading = 88.1990  BREAK						
						CASE 10	vLoc = <<-94.7510, -2531.6340, 5.0640>>			fHeading = 88.1990  BREAK						
						CASE 11	vLoc = <<-94.7510, -2531.6340, 5.0640>>			fHeading = 88.1990  BREAK						
						
					ENDSWITCH
				BREAK
				CASE 3
					SWITCH iProp
						CASE 0	vLoc = <<719.3600, -570.1590, 25.2140>> 		fHeading = 356.1960 BREAK
						CASE 1	vLoc = <<733.9700, -552.6940, 25.7090>> 		fHeading = 301.5980 BREAK
						CASE 2	vLoc = <<722.0230, -557.6940, 25.3700>> 		fHeading = 185.5970 BREAK
						CASE 3	vLoc = <<743.8700, -529.8410, 26.1310>> 		fHeading = 170.2000 BREAK
						CASE 4	vLoc = <<718.5590, -560.7000, 25.3490>> 		fHeading = 287.3990 BREAK
						CASE 5	vLoc = <<727.2430, -572.2770, 25.9050>> 		fHeading = 5.7990   BREAK						
						CASE 6	vLoc = <<727.2430, -572.2770, 25.9050>> 		fHeading = 5.7990   BREAK						
						CASE 7	vLoc = <<727.2430, -572.2770, 25.9050>> 		fHeading = 5.7990   BREAK						
						CASE 8	vLoc = <<727.2430, -572.2770, 25.9050>> 		fHeading = 5.7990   BREAK						
						CASE 9	vLoc = <<727.2430, -572.2770, 25.9050>> 		fHeading = 5.7990   BREAK						
						CASE 10	vLoc = <<727.2430, -572.2770, 25.9050>> 		fHeading = 5.7990   BREAK						
						CASE 11	vLoc = <<727.2430, -572.2770, 25.9050>> 		fHeading = 5.7990   BREAK						
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			SWITCH iLocation
				CASE 0
					SWITCH iProp
						CASE 0	vLoc = <<1692.8230, 3313.1841, 40.1490>> 		fHeading = 213.6000	BREAK
						CASE 1	vLoc = <<1698.0610, 3307.4241, 40.1630>> 		fHeading = 163.0000	BREAK
						CASE 2	vLoc = <<1704.8910, 3297.1360, 40.1510>> 		fHeading = 31.6000	BREAK
						CASE 3	vLoc = <<1705.3650, 3321.8279, 40.2490>> 		fHeading = 178.1960	BREAK
						CASE 4	vLoc = <<1703.0380, 3323.7080, 40.2370>> 		fHeading = 145.4000	BREAK
						CASE 5	vLoc = <<1698.6810, 3295.4851, 40.2070>> 		fHeading = 4.8000	BREAK
						CASE 6	vLoc = <<1679.9070, 3314.8840, 40.3960>> 		fHeading = 250.1990	BREAK
						CASE 7	vLoc = <<1686.4620, 3313.0696, 40.4114>> 		fHeading = 250.1990	BREAK
					ENDSWITCH
				BREAK
				
				CASE 1
					SWITCH iProp
						CASE 0	vLoc = <<1354.0420, -1860.6350, 55.5210>>		fHeading =159.9960	BREAK
						CASE 1	vLoc = <<1363.0320, -1850.0310, 56.1980>>		fHeading =189.5990	BREAK
						CASE 2	vLoc = <<1364.4340, -1869.5750, 55.7010>> 		fHeading =199.5980	BREAK
						CASE 3	vLoc = <<1362.2430, -1876.9871, 56.0520>>		fHeading =349.3950	BREAK
						CASE 4	vLoc = <<1365.8210, -1844.8010, 56.3370>>		fHeading =172.3950	BREAK
						CASE 5	vLoc = <<1344.2111, -1854.1219, 56.1660>> 		fHeading =249.7950  BREAK
						CASE 6	vLoc = <<1371.2214, -1856.4077, 56.2579>>		fHeading =0.0000	BREAK
						CASE 7	vLoc = <<1694.4135, 3316.9587, 42.0240>>		fHeading = 250.1990	BREAK
					ENDSWITCH
				BREAK
				
				CASE 2
					SWITCH iProp
						CASE 0	vLoc = <<732.0740, -1846.5780, 28.3510>> 		fHeading = 259.3960	BREAK
						CASE 1	vLoc = <<733.5160, -1864.9940, 28.3510>> 		fHeading = 304.7970	BREAK
						CASE 2	vLoc = <<747.3100, -1862.0220, 28.3520>> 		fHeading = 22.9970	BREAK
						CASE 3	vLoc = <<746.8900, -1853.2860, 28.2940>> 		fHeading = 180.4000	BREAK
						CASE 4	vLoc = <<738.5860, -1846.7720, 28.2940>> 		fHeading = 120.3990	BREAK
						CASE 5	vLoc = <<747.6510, -1838.4570, 28.3950>> 		fHeading = 0.0000	BREAK
						CASE 6	vLoc = <<752.3300, -1870.6990, 28.2940>> 		fHeading = 282.0000	BREAK
						CASE 7	vLoc = <<743.2522, -1845.8781, 30.2916>> 		fHeading = 250.1990	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC


PROC GET_GB_GUNRUNNING_DEFEND_CUSTOM_SPAWN_COORDS(INT iVariation, INT iLocation, INT iSpawnID, VECTOR &vLoc, FLOAT &fHeading)
SWITCH iVariation
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
			SWITCH iLocation
				CASE 0
					SWITCH iSpawnID
						CASE 0	vLoc = <<1064.5813, -1907.2212, 30.0934>> 		fHeading = 179.3996 BREAK
						CASE 1	vLoc = <<1059.5532, -1906.8505, 30.0164>> 		fHeading = 179.3996 BREAK
						CASE 2	vLoc = <<1062.1067, -1902.8069, 30.0378>> 		fHeading = 179.3996 BREAK
						CASE 3	vLoc = <<1064.5898, -1899.3472, 30.2061>> 		fHeading = 179.3996 BREAK
						CASE 4	vLoc = <<1059.5922, -1899.6389, 30.0417>> 		fHeading = 179.3996 BREAK
						CASE 5	vLoc = <<1062.1000, -1896.2556, 30.0632>> 		fHeading = 179.3996 BREAK
						CASE 6	vLoc = <<1064.8038, -1892.5697, 30.4167>> 		fHeading = 179.3996 BREAK						
						CASE 7	vLoc = <<1059.1752, -1893.0912, 29.7882>> 		fHeading = 179.3996 BREAK													
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH iSpawnID
						CASE 0	vLoc = <<155.6465, -1806.2797, 27.5321>> 		fHeading = 229.7995 BREAK
						CASE 1	vLoc = <<152.3038, -1809.9589, 27.2610>> 		fHeading = 221.3996 BREAK
						CASE 2	vLoc = <<178.0284, -1803.2389, 28.1735>> 		fHeading = 161.3996 BREAK
						CASE 3	vLoc = <<182.4805, -1807.2704, 28.1118>> 		fHeading = 146.3994 BREAK
						CASE 4	vLoc = <<229.2251, -1883.3702, 24.9078>> 		fHeading = 53.9991  BREAK
						CASE 5	vLoc = <<232.6127, -1879.9161, 25.1697>> 		fHeading = 62.9990  BREAK						
						CASE 6	vLoc = <<237.7819, -1873.7811, 25.5284>> 		fHeading = 124.9989 BREAK						
						CASE 7	vLoc = <<225.8228, -1888.5154, 24.5700>> 		fHeading = 2.9987   BREAK	
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iSpawnID
						CASE 0	vLoc = <<-54.7719, -2537.4563, 5.0100>> 		fHeading = 65.0000  BREAK
						CASE 1	vLoc = <<-53.2291, -2532.0007, 5.0100>> 		fHeading = 78.1999  BREAK
						CASE 2	vLoc = <<-49.7084, -2500.0859, 5.0100>> 		fHeading = 103.3997 BREAK
						CASE 3	vLoc = <<-52.1248, -2495.7148, 5.0100>> 		fHeading = 118.9996 BREAK
						CASE 4	vLoc = <<-66.1814, -2484.7856, 5.0396>> 		fHeading = 142.3994 BREAK
						CASE 5	vLoc = <<-60.0652, -2488.2822, 5.0293>> 		fHeading = 136.3993 BREAK						
						CASE 6	vLoc = <<-60.4108, -2495.0161, 5.0205>> 		fHeading = 136.3993 BREAK						
						CASE 7	vLoc = <<-48.9202, -2535.4888, 5.0100>> 		fHeading = 65.0000  BREAK		
						
					ENDSWITCH
				BREAK
				CASE 3
					SWITCH iSpawnID
						CASE 0	vLoc = <<757.3538, -610.1703, 27.7424>> 		fHeading = 75.1999 BREAK
						CASE 1	vLoc = <<757.8469, -606.3697, 27.9403>> 		fHeading = 75.1999 BREAK
						CASE 2	vLoc = <<754.5437, -607.8561, 27.7143>> 		fHeading = 75.1999 BREAK
						CASE 3	vLoc = <<752.7327, -610.7994, 27.6183>> 		fHeading = 62.1998 BREAK
						CASE 4	vLoc = <<753.6967, -604.8429, 27.6692>> 		fHeading = 62.1998 BREAK
						CASE 5	vLoc = <<751.5939, -607.4882, 27.5981>> 		fHeading = 62.1998 BREAK						
						CASE 6	vLoc = <<748.5320, -610.0411, 27.5027>> 		fHeading = 62.1998 BREAK						
						CASE 7	vLoc = <<748.1827, -605.6660, 27.5097>> 		fHeading = 62.1998 BREAK						
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			SWITCH iLocation
				CASE 0
					SWITCH iSpawnID
						CASE 0	vLoc = <<1743.7032, 3285.1711, 40.0873>> 		fHeading = 62.9999	BREAK
						CASE 1	vLoc = <<1738.8485, 3277.9067, 40.1074>> 		fHeading = 51.9999	BREAK
						CASE 2	vLoc = <<1681.5533, 3255.4016, 39.8105>> 		fHeading = 321.5995	BREAK
						CASE 3	vLoc = <<1676.4358, 3259.9502, 39.5910>> 		fHeading = 321.5995	BREAK
						CASE 4	vLoc = <<1654.2811, 3282.4429, 39.1613>> 		fHeading = 305.9993	BREAK
						CASE 5	vLoc = <<1649.4785, 3294.0420, 39.4557>> 		fHeading = 279.1991 BREAK
						CASE 6	vLoc = <<1653.5433, 3339.6448, 38.6600>> 		fHeading = 229.1988	BREAK
						CASE 7	vLoc = <<1664.2559, 3349.7668, 38.6951>> 		fHeading = 213.9988	BREAK
					ENDSWITCH
				BREAK
				
				CASE 1
					SWITCH iSpawnID
						CASE 0	vLoc = <<1350.2419, -1806.0681, 56.9415>>	fHeading =188.9995	BREAK
						CASE 1	vLoc = <<1358.9518, -1803.6178, 57.9564>>	fHeading =175.7995	BREAK
						CASE 2	vLoc = <<1349.0831, -1907.7535, 52.5809>>	fHeading =357.1992	BREAK
						CASE 3	vLoc = <<1356.2181, -1909.0057, 52.7028>>	fHeading =8.1991	BREAK
						CASE 4	vLoc = <<1357.8640, -1793.1045, 58.6428>>	fHeading =178.9988	BREAK
						CASE 5	vLoc = <<1348.3741, -1794.9983, 57.2378>>	fHeading =197.1988  BREAK
						CASE 6	vLoc = <<1343.6953, -1921.5613, 50.5250>>	fHeading =342.7985  BREAK
						CASE 7	vLoc = <<1351.4014, -1922.8488, 50.7572>>	fHeading =359.1983	BREAK
					ENDSWITCH
				BREAK
				
				CASE 2
					SWITCH iSpawnID
  						CASE 0	vLoc = <<767.4472, -1876.9119, 28.2210>> 		fHeading = 75.5999	BREAK
						CASE 1	vLoc = <<770.5578, -1873.9382, 28.2681>> 		fHeading = 91.1999	BREAK
						CASE 2	vLoc = <<772.3266, -1877.3075, 28.2817>> 		fHeading = 67.1998	BREAK
						CASE 3	vLoc = <<776.9196, -1874.3225, 28.2980>> 		fHeading = 79.1997	BREAK
						CASE 4	vLoc = <<774.9619, -1881.1442, 28.2945>> 		fHeading = 66.1997	BREAK
						CASE 5	vLoc = <<772.7117, -1885.1566, 28.2915>> 		fHeading = 30.9996  BREAK
						CASE 6	vLoc = <<779.3302, -1878.7858, 28.2671>> 		fHeading = 73.3995	BREAK
						CASE 7	vLoc = <<778.3873, -1884.8042, 28.2211>> 		fHeading = 47.9994	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC 



FUNC VECTOR GET_GB_GUNRUNNING_DEFEND_PACKAGE_SPAWN_COORDS(INT iVariation, INT iLocation, INT iPackage)
	UNUSED_PARAMETER(iPackage)
	SWITCH iVariation
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
			SWITCH iLocation
				CASE 0
//					SWITCH iPackage
//						CASE 0		RETURN <<-111.3476, 6419.3032, 40.4354>> 
//						CASE 1		RETURN <<-170.4088, 6213.2241, 40.1815>>
//					ENDSWITCH
				BREAK
				
				
			ENDSWITCH
		BREAK
		
		
	ENDSWITCH
	
	RETURN << 0.0, 0.0, 0.0 >>
ENDFUNC

FUNC INT GET_GB_GUNRUNNING_DEFEND_NUMBER_OF_INITIAL_PEDS_FOR_VARIATION(INT iVariation, INT iLocation)
	UNUSED_PARAMETER(iLocation)
	SWITCH iVariation
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
			RETURN 12
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			RETURN 12
	ENDSWITCH
	

	
	RETURN 0
ENDFUNC

FUNC INT GET_GUNRUNNING_DEFEND_NUMBER_OF_BOMBS_FOR_BUSINESS()
	RETURN 3
ENDFUNC

FUNC INT GET_GB_GUNRUNNING_DEFEND_NUMBER_OF_INITIAL_AMBIENT_VEHS_FOR_VARIATION(INT iVariation, INT iLocation)
	UNUSED_PARAMETER(iVariation)
	UNUSED_PARAMETER(iLocation)

//	SWITCH iVariation
//		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
//			SWITCH iLocation
//				CASE 0		RETURN 0
//			ENDSWITCH
//		BREAK
//		
//	ENDSWITCH
//
	RETURN 0	
ENDFUNC

FUNC INT GET_GB_GUNRUNNING_DEFEND_NUMBER_OF_INITIAL_ENEMY_VEHS_FOR_VARIATION(INT iVariation, INT iLocation)
	
	SWITCH iVariation
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
			SWITCH iLocation
				CASE 0	RETURN 2
				CASE 1	RETURN 2
				CASE 2	RETURN 2
				CASE 3	RETURN 2
			ENDSWITCH
		BREAK
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			SWITCH iLocation
				CASE 0	RETURN 3
				CASE 1	RETURN 3
				CASE 2	RETURN 3
			ENDSWITCH
		BREAK
	ENDSWITCH
	


	RETURN 0	
ENDFUNC

FUNC INT GET_GB_GUNRUNNING_DEFEND_NUMBER_OF_INITIAL_PROPS_FOR_VARIATION(INT iVariation, INT iLocation)

	
	SWITCH iVariation
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
			SWITCH iLocation	
				CASE 0	RETURN 12
				CASE 1	RETURN 12
				CASE 2	RETURN 12
				CASE 3	RETURN 12
			ENDSWITCH
		BREAK
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			SWITCH iLocation	
				CASE 0	RETURN 8
				CASE 1	RETURN 8
				CASE 2	RETURN 8
			ENDSWITCH
		BREAK
	ENDSWITCH
	


	RETURN 0	
ENDFUNC

FUNC INT GET_GB_GUNRUNNING_DEFEND_NUMBER_OF_INITIAL_CONTRABAND_VEHS_FOR_VARIATION(INT iVariation, INT iLocation)
	UNUSED_PARAMETER(iLocation)
	SWITCH iVariation
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
			RETURN 1
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			RETURN 1
	ENDSWITCH

//	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Determine which AI ped (if any) rthe specified package should be attached to when created
/// PARAMS:
///    iVariation - 
///    iLocation - 
///    iPackage - 
/// RETURNS:
///    
FUNC INT GET_GB_GUNRUNNING_DEFEND_PED_TO_ATTACH_PACKAGE_TO(INT iVariation, INT iLocation, INT iPackage)
	UNUSED_PARAMETER(iPackage)
	SWITCH iVariation

		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			SWITCH iLocation
				CASE 0	
				
//					SWITCH iPackage
//					//	CASE 0 RETURN 0
//						
//					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
	ENDSWITCH

	RETURN -1	
ENDFUNC



FUNC INT GET_GB_GUNRUNNING_DEFEND_NUMBER_OF_PACKAGES_FOR_VARIATION(INT iVariation, INT iLocation)

	SWITCH iVariation
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
			SWITCH iLocation
			//	CASE 0	RETURN 0
				
			ENDSWITCH
		BREAK
		
	
	ENDSWITCH
	
	RETURN 0
ENDFUNC


/// PURPOSE:
///    Custom respawn points for the variation
/// PARAMS:
///    iVar - 
///    iLocation - 
PROC SETUP_GB_GUNRUNNING_DEFEND_CUSTOM_SPAWNS(INT iVar, INT iLocation)
	
	
	SWITCH iVar
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
			SWITCH iLocation
				CASE 0	
//					ADD_CUSTOM_SPAWN_POINT( <<446.4323, 6503.0796, 28.3633>>	,	179.3996)
//					ADD_CUSTOM_SPAWN_POINT( <<445.7750, 6510.7295, 28.0500>>	,	360.9990)
//					ADD_CUSTOM_SPAWN_POINT( <<424.6255, 6459.0332, 27.7887>>	,  	52.1989)
//					ADD_CUSTOM_SPAWN_POINT( <<449.2216, 6460.0342, 28.1724>>	,	325.1988)
//					ADD_CUSTOM_SPAWN_POINT( <<439.9934, 6449.7568, 28.1983>>	,	133.1982)
//					ADD_CUSTOM_SPAWN_POINT( <<336.6075, 6482.7090, 28.4298>>	,  	335.9978)
//					ADD_CUSTOM_SPAWN_POINT( <<335.6669, 6460.5923, 29.4889>>	,  	229.9972)
//					ADD_CUSTOM_SPAWN_POINT( <<422.2368, 6460.2861, 27.9069>>	, 	45.5966)
				BREAK
				
				
			ENDSWITCH
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC BOOL DOES_VARIATION_USE_CUSTOM_SPAWN_POINTS(INT iVar)
	SWITCH iVar
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC WEAPON_TYPE GET_WEAPON_FOR_GUNRUNNING_DEFEND_PED(INT iPed, INT iVar, INT iLocation)
	
	
	SWITCH iVar
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
			SWITCH iLocation
				CASE 0
					SWITCH iPed
						CASE 0	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 1	RETURN WEAPONTYPE_PISTOL
						CASE 2	RETURN WEAPONTYPE_PISTOL
						CASE 3	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 4	RETURN WEAPONTYPE_PISTOL
						CASE 5	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 6	RETURN WEAPONTYPE_PISTOL
						CASE 7	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 8	RETURN WEAPONTYPE_PISTOL
						CASE 9	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 10	RETURN WEAPONTYPE_PISTOL
						CASE 11	RETURN WEAPONTYPE_PISTOL
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH iPed
						CASE 0	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 1	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 2	RETURN WEAPONTYPE_PISTOL
						CASE 3	RETURN WEAPONTYPE_PISTOL
						CASE 4	RETURN WEAPONTYPE_PISTOL
						CASE 5	RETURN WEAPONTYPE_PISTOL
						CASE 6	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 7	RETURN WEAPONTYPE_PISTOL
						CASE 8	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 9	RETURN WEAPONTYPE_PISTOL
						CASE 10	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 11	RETURN WEAPONTYPE_PISTOL
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iPed
						CASE 0	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 1	RETURN WEAPONTYPE_PISTOL
						CASE 2	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 3	RETURN WEAPONTYPE_PISTOL
						CASE 4	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 5	RETURN WEAPONTYPE_PISTOL
						CASE 6	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 7	RETURN WEAPONTYPE_PISTOL
						CASE 8	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 9	RETURN WEAPONTYPE_PISTOL
						CASE 10	RETURN WEAPONTYPE_PISTOL
						CASE 11	RETURN WEAPONTYPE_PISTOL
					ENDSWITCH
				BREAK
				CASE 3
					SWITCH iPed
						CASE 0	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 1	RETURN WEAPONTYPE_PISTOL
						CASE 2	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 3	RETURN WEAPONTYPE_PISTOL
						CASE 4	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 5	RETURN WEAPONTYPE_PISTOL
						CASE 6	RETURN WEAPONTYPE_PISTOL
						CASE 7	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 8	RETURN WEAPONTYPE_PISTOL
						CASE 9	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 10	RETURN WEAPONTYPE_PISTOL
						CASE 11	RETURN WEAPONTYPE_PISTOL
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			SWITCH iLocation
				CASE 0
					SWITCH iPed
						CASE 0	RETURN WEAPONTYPE_PISTOL
						CASE 1	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 2	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 3	RETURN WEAPONTYPE_SNIPERRIFLE
						CASE 4	RETURN WEAPONTYPE_PISTOL
						CASE 5	RETURN WEAPONTYPE_SNIPERRIFLE
						CASE 6	RETURN WEAPONTYPE_PISTOL
						CASE 7	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 8	RETURN WEAPONTYPE_PISTOL
						CASE 9	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 10	RETURN WEAPONTYPE_PISTOL
						CASE 11	RETURN WEAPONTYPE_CARBINERIFLE
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH iPed
						CASE 0	RETURN WEAPONTYPE_PISTOL
						CASE 1	RETURN WEAPONTYPE_PISTOL
						CASE 2	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 3	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 4	RETURN WEAPONTYPE_PISTOL
						CASE 5	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 6	RETURN WEAPONTYPE_PISTOL
						CASE 7	RETURN WEAPONTYPE_PISTOL
						CASE 8	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 9	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 10	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 11	RETURN WEAPONTYPE_PISTOL
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iPed
						CASE 0	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 1	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 2	RETURN WEAPONTYPE_PISTOL
						CASE 3	RETURN WEAPONTYPE_PISTOL
						CASE 4	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 5	RETURN WEAPONTYPE_PISTOL
						CASE 6	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 7	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 8	RETURN WEAPONTYPE_PISTOL
						CASE 9	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 10	RETURN WEAPONTYPE_PISTOL
						CASE 11	RETURN WEAPONTYPE_CARBINERIFLE
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN WEAPONTYPE_MICROSMG	
ENDFUNC

/// PURPOSE:
///    Pause menu name for the blip for the specified package
/// PARAMS:
///    iVar - 
/// RETURNS:
///    
FUNC TEXT_LABEL_15 GET_NAME_FOR_DEFEND_PICKUP_BLIP(INT iVar)
	TEXT_LABEL_15 tl15Name
	
	SWITCH iVar
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			tl15Name = "GR_GOODSa" // Weapons
		BREAK

	ENDSWITCH
	
	RETURN tl15Name
ENDFUNC

/// PURPOSE:
///    Text label for the 'title' of the hud element corresponding to the number of packages collected. E.g. Product: 0/3
/// PARAMS:
///    iVar - 
/// RETURNS:
///    
FUNC TEXT_LABEL_15 GET_NAME_FOR_DEFEND_PACKAGE_HUD_TITLE(INT iVar)
	TEXT_LABEL_15 tl15Name
	
	SWITCH iVar
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			tl15Name = "GR_GOODSa" // Weapons
		BREAK
		

	ENDSWITCH
	
	RETURN tl15Name
ENDFUNC

/// PURPOSE:
///    Pause menu name for the blip corresponding to the initial goto , or 'action area', for the variation 
/// PARAMS:
///    iVar - 
///    iLocation - 
/// RETURNS:
///    
FUNC TEXT_LABEL_15 GET_NAME_FOR_ACTION_AREA_BLIP(INT iVar, INT iLocation)
	//UNUSED_PARAMETER(iLocation)
	TEXT_LABEL_15 tl15Obj
	
	SWITCH iVar
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			SWITCH iLocation
				CASE 0 
					tl15Obj = "DGUNV_AREA1" // Sandy Shores Airfield
				BREAK
				CASE 1 
					tl15Obj = "DGUNV_AREA2" // Labor Place
				BREAK
				CASE 2 
					tl15Obj = "DGUNV_AREA3" // Popular Street
				BREAK
			ENDSWITCH
		BREAK
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
			SWITCH iLocation
				CASE 0 
					tl15Obj = "DGUNB_AREA1" //  Labor Place
				BREAK
				CASE 1 
					tl15Obj = "DGUNB_AREA2" //  Brouge Avenue
				BREAK
				CASE 2 
					tl15Obj = "DGUNB_AREA3" // Elysian Island
				BREAK
				CASE 3 
					tl15Obj = "DGUNB_AREA4" // Popular Street
				BREAK
			ENDSWITCH
		BREAK
		
		
		
		
	ENDSWITCH
	
	RETURN tl15Obj
ENDFUNC

/// PURPOSE:
///    Pause menu name for the blip corresponding to the vehicle to be retieved
/// PARAMS:
///    iVar - 
///    iLocation - 
/// RETURNS:
///    
FUNC TEXT_LABEL_15 GET_GB_GUNRUNNING_DEFEND_VEHICLE_BLIP_NAME(INT iVar, INT iLocation)
	TEXT_LABEL_15 tl15Veh
	UNUSED_PARAMETER(iLocation)
		SWITCH iVar
			CASE GB_GUNRUNNING_DEFEND_DEFUSAL
				tl15Veh = "DGUN_BLIPPOU"	// Pounder
			BREAK
			CASE GB_GUNRUNNING_DEFEND_VALKYRIE
				tl15Veh = "DGUN_BLIPVLK"		// Valkyrie
			BREAK
				
		ENDSWITCH
	RETURN tl15Veh
ENDFUNC

FUNC TEXT_LABEL_15 GET_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT_VEHICLE_BLIP_NAME(INT iVar, INT iLocation)
	TEXT_LABEL_15 tl15Veh
	UNUSED_PARAMETER(iLocation)
		SWITCH iVar
			CASE GB_GUNRUNNING_DEFEND_DEFUSAL
				tl15Veh = "DGUN_OBJPOU"	// Pounder
			BREAK
			CASE GB_GUNRUNNING_DEFEND_VALKYRIE
				tl15Veh = "DGUN_OBJVLK"		// Valkyrie
			BREAK
				
		ENDSWITCH
	RETURN tl15Veh
ENDFUNC

/// PURPOSE:
///    The objective text to display when the mission launches.
/// PARAMS:
///    iVar - 
///    iLocation - 
/// RETURNS:
///    
FUNC TEXT_LABEL_15 GET_GB_GUNRUNNING_DEFEND_OBJECTIVE_TEXT_FOR_APPROACH_ACTION(INT iVar, INT iLocation)
	TEXT_LABEL_15 tl15Obj
	
	
	SWITCH iVar
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
			SWITCH iLocation
				CASE 0	tl15Obj = "DGUNB_GOTOV1"// Go to  Labor Place.
				BREAK
				CASE 1 tl15Obj = "DGUNB_GOTOV2"	// Go to Brouge Avenue.
				BREAK
				CASE 2 tl15Obj = "DGUNB_GOTOV3"	// Go to Elysian Island.
				BREAK
				CASE 3 tl15Obj = "DGUNB_GOTOV4"	// Go to Popular Street.
				BREAK
			ENDSWITCH
		BREAK
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			SWITCH iLocation
				CASE 0	tl15Obj = "DGUN_GOTOV1"	// Go to Sandy Shores Airfield
				
				BREAK
				CASE 1 tl15Obj = "DGUN_GOTOV2"	// Go to Labor Place.
				
				BREAK
				CASE 2 tl15Obj = "DGUN_GOTOV3"	// Go to Popular Street.
				
				BREAK
				
			ENDSWITCH
		BREAK
	ENDSWITCH

//	
	RETURN tl15Obj
	
ENDFUNC


FUNC TEXT_LABEL_15 GET_GB_GUNRUNNING_DEFEND_OPENING_TEXT_MSG(INT iVar, INT iLocation)
	TEXT_LABEL_15 tl15Obj
	UNUSED_PARAMETER(iLocation)
	SWITCH iVar
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			tl15Obj = "DBR_TXTMTH1"
		BREAK

	ENDSWITCH
	
	RETURN tl15Obj
	
ENDFUNC



FUNC INT GET_GB_GUNRUNNING_DEFEND_NUMBER_OF_SCENARIO_BLOCKING_AREAS(INT iVar, INT iLocation)
	SWITCH iVar
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
			SWITCH iLocation
				CASE 0
					//RETURN 1
				BREAK
				DEFAULT
					RETURN 1
			ENDSWITCH
		BREAK
		
		
	ENDSWITCH
	
	RETURN 0
ENDFUNC


PROC GET_GB_DEFEND_SCENARIO_BLOCKING_AREA_DETAILS(INT iVar, INT iLocation, INT iScenBlock, VECTOR &vMin, VECTOR &vMax)
	SWITCH iVar
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
			SWITCH iLocation
				CASE 0
					SWITCH iScenBlock
						CASE 0
							vMin = <<2982.944092,3488.139404,73.381813>> - <<18.437500,30.875000,3.000000>>
							vMin.z -= 2.0
							
							vMax = <<2982.944092,3488.139404,73.381813>> + <<18.437500,30.875000,3.000000>>
							vMin.z += 2.0
						BREAK

						
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Define an angled area which will spook the corresponding ped if the player enter .
/// PARAMS:
///    iVar - 
///    iLocation - 
///    iAngled - 
///    vMin - 
///    vMax - 
///    fDefendWidth - 
PROC GET_GB_DEFEND_ANGLED_AREA_FOR_SPOOKED_CHECK(INT iVar, INT iLocation, INT iAngled, VECTOR &vMin, VECTOR &vMax, FLOAT &fDefendWidth)
	//UNUSED_PARAMETER(iAngled)
	//UNUSED_PARAMETER(vMin)
	//UNUSED_PARAMETER(vMax)
	//UNUSED_PARAMETER(fDefendWidth)
	
	//add parameter for PED, pass in i, can do SWITCH on i
	//outside this function, check if player in heli, then if it is, return the 1st area, would not move into switch
	//else if 
	SWITCH iVar
	CASE GB_GUNRUNNING_DEFEND_DEFUSAL
			SWITCH iLocation
				CASE 0
					SWITCH iAngled
						CASE 0
							vMin = <<1030.219360,-1926.544678,5.078026>>
							vMax =  <<1108.106201,-1984.468506,130.026550>>
							fDefendWidth = 150.000000
						BREAK
						CASE 1
							vMin = <<1087.047241,-1940.190308,26.567997>>
							vMax = <<1055.058838,-1982.814087,87.426285>>
							fDefendWidth =  60.000000
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 1
					SWITCH iAngled
						CASE 0
							vMin =<<224.251160,-1891.744141,-0.639847>>
							vMax =  <<131.385605,-1819.175537,126.210320>>
							fDefendWidth = 120.000000
						BREAK
						CASE 1
							vMin = <<202.652985,-1845.678955,25.914598>>
							vMax = <<190.382416,-1859.310425,66.011948>>
							fDefendWidth = 60.000000
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 2
					SWITCH iAngled
							CASE 0
								vMin =<<-78.530479,-2500.336670,-19.975250>>
								vMax = <<-128.034348,-2565.068848,105.014008>>
								fDefendWidth =  120.000000
							BREAK
							CASE 1
								vMin =  <<-120.178505,-2552.199463,0.364465>>
								vMax =  <<-92.397049,-2511.549805,30.000000>>
								fDefendWidth =  50.000000
							BREAK
					ENDSWITCH
				BREAK
				CASE 3
					SWITCH iAngled
							CASE 0
								vMin =<<759.052124,-562.936707,6.596148>>
								vMax =  <<662.191895,-527.747681,114.133789>>
								fDefendWidth =  120.000000
							BREAK
							CASE 1
								vMin =<<719.477051,-568.799011,20.187748>>
								vMax = <<720.140137,-591.920471,32.444752>>
								fDefendWidth =  20.000000
							BREAK
							CASE 2
								vMin = <<725.791870,-544.324829,21.453226>>
								vMax = <<739.429199,-553.218567,58.777245>>
								fDefendWidth =  50.000000
							BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			SWITCH iLocation
				CASE 0
					SWITCH iAngled
						CASE 0
							vMin = <<1713.811279,3255.872559,15.088112>>
							vMax = <<1678.597168,3363.158203,138.892212>>
							fDefendWidth = 120.000000
						BREAK
						CASE 1
							vMin = <<1704.579956,3297.624023,35.977470>>
							vMax = <<1715.241211,3282.717285,93.153465>>
							fDefendWidth = 20.000000
						BREAK
						CASE 2
							vMin = <<1715.327393,3311.437988,38.740585>>
							vMax = <<1681.860107,3301.489502,90.098663>>
							fDefendWidth = 40.000000
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 1
					SWITCH iAngled
						CASE 0
							vMin = <<1354.264282,-1797.796265,52.821709>>
							vMax = <<1357.332642,-1924.000366,132.079590>>
							fDefendWidth = 140.000000
						BREAK
						CASE 1
							vMin = <<1357.933960,-1833.940796,53.560246>>
							vMax = <<1361.171997,-1877.688477,65.987007>>
							fDefendWidth = 45.000000
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 2
					SWITCH iAngled
							CASE 0
								vMin =<<724.251587,-1894.992432,3.443504>>
								vMax =  <<733.220337,-1745.509888,128.405731>>
								fDefendWidth =  150.000000
							BREAK
							CASE 1
								vMin =  <<749.634216,-1872.848511,18.291552>>
								vMax = <<768.672913,-1874.328857,68.246521>>
								fDefendWidth =  10.000000
							BREAK
							CASE 2
								vMin = <<741.637207,-1880.003296,25.166248>>
								vMax = <<745.383362,-1822.859619,88.291611>>
								fDefendWidth = 40.000000
							BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
		
ENDPROC

/// PURPOSE:
///    Defensive area (and size) for each enemy ped
/// PARAMS:
///    iVariation - 
///    iLocation - 
///    iPed - 
///    vLoc - 
///    fSize - 
PROC INIT_GB_GUNRUNNING_DEFEND_PED_DEFENSIVE_COORDS(INT iVariation, INT iLocation, INT iPed, VECTOR &vMin, VECTOR &vMax, FLOAT &fDefendWidth)

//if it is the valkyrie mission type, then set angled defensive area
UNUSED_PARAMETER(iPed)
	//FLOAT fDefaultSize
	SWITCH iVariation
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
			SWITCH iLocation
				CASE 0 
				vMin = 	<<1087.047241,-1940.190308,26.567997>>
				vMax =  <<1055.058838,-1982.814087,87.426285>>
				fDefendWidth =  60.000000
				BREAK
				CASE 1 
				vMin = 	<<202.652985,-1845.678955,25.914598>>
				vMax =  <<190.382416,-1859.310425,66.011948>>
				fDefendWidth =  60.000000
				BREAK
				CASE 2 
				vMin = 	<<-120.178505,-2552.199463,0.364465>>
				vMax =  <<-92.397049,-2511.549805,30.000000>>
				fDefendWidth =  50.000000
				BREAK
				CASE 3 
				vMin = 	<<725.791870,-544.324829,21.453226>>
				vMax =  <<739.429199,-553.218567,58.777245>>
				fDefendWidth =  50.000000
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			SWITCH iLocation
				CASE 0 
				vMin = 	<<1715.327393,3311.437988,38.740585>>
				vMax =   <<1681.860107,3301.489502,90.098663>>
				fDefendWidth =  40.000000
				BREAK
				CASE 1
				vMin = 	<<1357.933960,-1833.940796,53.560246>> 
				vMax =  <<1361.171997,-1877.688477,65.987007>>
				fDefendWidth =  45.000000
				BREAK
				CASE 2
				vMin = 	<<741.637207,-1880.003296,25.166248>>
				vMax =  <<745.383362,-1822.859619,88.291611>>
				fDefendWidth =  40
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
		
ENDPROC
//PROC INIT_GB_GUNRUNNING_DEFEND_CREATE_BOMBS_FOR_VEHICLE(NETWORK_INDEX iVeh)
//MODEL_NAMES bombModel =INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_adv_hdsec"))  //GET_CRATE_MODEL_FOR_VEHICLE(GET_ENTITY_MODEL(vehID))
//
//	IF bombModel != DUMMY_MODEL_FOR_SCRIPT
//		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.vehCrate[iGunrunEntity])
//			IF CAN_REGISTER_MISSION_OBJECTS( 1 )
//				REQUEST_MODEL(bombModel)
//				IF HAS_MODEL_LOADED(bombModel)
//					IF CREATE_NET_OBJ(serverBD.vehCrate[iGunrunEntity],bombModel, GET_ENTITY_COORDS(vehID)+<<0.0,0.0,5.0>>)
//						//SET_ENTITY_LOD_DIST(NET_TO_OBJ(serverBD.vehCrate[iGunrunEntity]), 100)
//						//SET_ENTITY_VISIBLE(NET_TO_OBJ(serverBD.vehCrate[iGunrunEntity]),TRUE)
//						//SET_ENTITY_INVINCIBLE(NET_TO_OBJ(serverBD.vehCrate[iGunrunEntity]),TRUE)
//						SET_ENTITY_DYNAMIC(NET_TO_OBJ(serverBD.vehCrate[iGunrunEntity]),FALSE)
//						//SET_ENTITY_PROOFS(NET_TO_OBJ(serverBD.vehCrate[iGunrunEntity]),TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
//						IF SHOULD_DISABLE_COLLISIONS_FOR_VEHICLE_CRATE(iGunrunEntity, vehID)
//							SET_ENTITY_COLLISION(NET_TO_OBJ(serverBD.vehCrate[iGunrunEntity]), FALSE)
//						ENDIF
//						ATTACH_CRATE_TO_VEHICLE(iGunrunEntity, vehID)
//						SET_MODEL_AS_NO_LONGER_NEEDED(bombModel)
//					ENDIF
//				ENDIF
//			ENDIF
//		ELSE
//			PRINTLN("[GUNRUN_DEFUSAL] - [", GET_CURRENT_VARIATION_STRING(), "] - bomb exists. CARRY ON")
//		ENDIF
//	ELSE
//		PRINTLN("[GUNRUN_DEFUSAL] - [", GET_CURRENT_VARIATION_STRING(), "] - NO VEHICLE BOMBS. return false")
//	ENDIF
//ENDPROC	


/// PURPOSE:
///    Determine if we need to attach a prop to inside / rea of a vehicle for the current variation
/// PARAMS:
///    iVariation - 
///    iLocation - 
/// RETURNS:
///    
FUNC BOOL DOES_GB_GUNRUNNING_DEFEND_VARIATION_REQUIRE_CRATES_IN_BACK_OF_VAN(INT iVariation)
	SWITCH iVariation
		CASE GB_GUNRUNNING_DEFEND_DEFUSAL
			RETURN TRUE
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC



/// PURPOSE:
///    Determine which index of the niPlayerVeh[] array corrsponds to the vehicle we want to attach a prop to
/// PARAMS:
///    iVariation - 
///    iLocation - 
///    iCrate - 
/// RETURNS:
///    
FUNC INT GET_GB_GUNRUNNING_DEFEND_VEHICLE_TO_ATTACH_CRATE_TO(INT iVariation, INT iLocation, INT iCrate)
	SWITCH iVariation
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			SWITCH iLocation
				CASE 0
					SWITCH iCrate 
						CASE 0	RETURN 0
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH	
	RETURN -1
ENDFUNC

/// PURPOSE:
///    Determine which element of the niProp[] array corresponds to the iNthCrateToAttach prop we want to attach to a vehicle
/// PARAMS:
///    iVariation - 
///    iLocation - 
///    iNthCrateToAttach - 
/// RETURNS:
///    
FUNC INT GET_GB_GUNRUNNING_DEFEND_CRATE_TO_ATTACH_PROP_INDEX(INT iVariation, INT iLocation, INT iNthCrateToAttach = 0)
	SWITCH iVariation
		CASE GB_GUNRUNNING_DEFEND_VALKYRIE
			SWITCH iLocation
				CASE 0
					RETURN iNthCrateToAttach
			ENDSWITCH
		BREAK
		
	ENDSWITCH
	
	RETURN 0
ENDFUNC
